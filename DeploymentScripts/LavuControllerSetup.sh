#! /bin/bash
#LavuControllerSetup.sh

# UPDATE SYSTEM
sudo apt-get update
sudo apt-get -y upgrade
# INSTALL NEEDED APPS
sudo apt-get -y install lighttpd libfcgi php5-cgi php5-cli php5-curl php5-mysqlnd unzip screen
# Preset Mysql password
echo 'mysql-server-5.5 mysql-server/root_password password lavuPassword!' | sudo debconf-set-selections
echo 'mysql-server-5.5 mysql-server/root_password_again password lavuPassword!' | sudo debconf-set-selections
sudo apt-get -y install mysql-server-5.5
# wget LavuController.zip from LavuController.zip
echo "Getting LavuController.zip"
wget http://lavukart.lavu/LavuController.zip >/dev/null
# extract LavuController to web directory
echo "Unzipping LavuController.zip"
sudo rm -rf /var/www/*
sudo unzip LavuController.zip -d /var/www/ >/dev/null
sudo chmod 0777 -R /var/www
echo "Finished Unzipping LavuController.zip"
# Set up mysql Tables
sudo mysql -u root -plavuPassword! < /var/www/pi_print/Create.sql
sudo mysql -u root -plavuPassword! < /var/www/pi_swipe/Create.sql
#Create convience alias for php/apache logs
sudo  grep -v 'server.reject-expect-100-with-417' /etc/lighttpd/lighttpd.conf | sudo tee /etc/lighttpd/lighttpd.conf
echo 'server.reject-expect-100-with-417 = "disable"' | sudo tee -a /etc/lighttpd/lighttpd.conf
sudo lighty-enable-mod fastcgi
sudo lighty-enable-mod fastcgi-php
sudo /etc/init.d/lighttpd force-reload
sudo crontab /var/www/crontab
sudo chmod 0755 -R /var/log/lighttpd
alias phplogs='tail -f /var/log/lighttpd/error.log'
sudo reboot
