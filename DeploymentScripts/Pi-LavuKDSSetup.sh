#! /bin/bash
#LavuKDS.sh

MYUSER=$(whoami)
TEST1="root"
UBUNTU=$(uname -a | sed "s/.*\(ubuntu\|Ubuntu\).*/ubuntu/")
DEBIAN=$(uname -a | sed "s/.*\(debian\).*/\1/")

if [ "$MYUSER" == "root" ];
then
	echo "KDS Install Script must be run by a non-root user"
	exit
fi

# UPDATE SYSTEM
sudo apt-get update
#sudo apt-get -y upgrade
# INSTALL NEEDED APPS
sudo apt-get -y install midori unclutter xorg matchbox-window-manager rungetty curl screen

#GET USER AND SETUP AUTO LOGIN (Ubuntu / Debian Specific)

#if [ "$DEBIAN" == "debian" ];
#then
	echo "8:2345:respawn:/sbin/rungetty tty8 --autologin $MYUSER" | sudo tee -a /etc/inittab
#fi

if [ "$UBUNTU" == "ubuntu" ];
then
cat > ~/tty8.conf <<tty8DOC
# tty8 - getty
#
# This service maintains a getty on tty8 from the point the system is
# started until it is shut down again.
start on runlevel [23]
stop on runlevel [!23]

respawn
exec /sbin/rungetty tty8 --autologin $MYUSER
tty8DOC
echo 'manual' | sudo tee /etc/init/lightdm.override
sudo cp ~/tty8.conf /etc/init/
fi

cat > ~/lavu-scanner <<"ScannerDOC"
#! /usr/bin/perl
use strict;
use warnings;
use feature qw(say);

my $command = <<'ESCAPEME';
ping -b -i 1000 -w 2 255.255.255.255 | grep from | sed 's/64 bytes from \(.*\)\:.*/\1/'
ESCAPEME
my $stuff = `$command`;
my @addresses = split(/\n/,$stuff);
foreach my $server (@addresses){
	#print "Checking " . $server . "\n";
	my @response = `curl -s -m 2 http://$server/LOCAL/KDS.PHP`; #try to load the page
	my $resp = $response[3];
	if (defined $resp){
		chomp ($resp);
		if ($resp =~ m/Lavu Local KDS/i){
			gotServer($server);
		}
	}
}

sub gotServer{
	my $server = shift;
	say $server;
	`grep -v lavulls /etc/hosts | sudo tee /etc/hosts`;
	`echo "$server lavulls" | sudo tee -a /etc/hosts`;
	`killall midori`;
}
ScannerDOC

# SET UP AUTO-START XORG
cat > ~/.bash_login <<"BASHLOGINDOC"
# ~/.bash_login: executed by bash(1) for login shells.

# include .profile if it exists
if [ -f "${HOME}/.profile" ] && [ -r "${HOME}/.profile" ]; then
source "${HOME}/.profile"
fi

# if were not root and were logged in on tty8, we assume a rungetty autologin and start xorg
if [ ! -z "${UID:-}" ] && [ "$UID" != "0" ] && [ -z "${DISPLAY}" ] && [ ! -z "${SHLVL:-}" ] && [ "$SHLVL" == "1" ]; then
if [ "$(tty)" == "/dev/tty8" ]; then
trap "chvt 1; logout" INT TERM EXIT
chvt 8
while true; do
echo "starting xorg"
startx
echo "sleeping 2 seconds"
sleep 2
done
fi
fi
BASHLOGINDOC

# SET SCRIPTS TO AUTO RUN WHEN X11 STARTS
cat > ~/.xinitrc <<"XINITRCDOC321"
# TURN OFF SCREEENBLANKING
xset -display :0 s off s 0 s noblank -dpms &
# TURN OFF CURSOR
unclutter -display :0 -noevents -grab &
# LAUNCH MIDORI LOOPER
midori-repeat &
# START WINDOW MANAGER
matchbox-window-manager
XINITRCDOC321

# AUTO RESTART MIDORI
cat > ~/midori-repeat <<"MIDORIDOC"
while true; do
	midori -a http://lavulls/local/kds.php -e Fullscreen
done
MIDORIDOC

# MAKE ~/midori-repeat EXECUTABLE
chmod +x ~/midori-repeat
chmod +x ~/lavu-scanner
# MAKE ~/midori-repeat RUN FROM CONSOLE
sudo cp ~/midori-repeat /usr/bin/
sudo cp ~/lavu-scanner /usr/bin/
echo '@daily killall midori' | crontab
echo '@reboot lavu-scanner' | sudo crontab

sync
sudo reboot