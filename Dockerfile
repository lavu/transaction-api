ARG DOCKERHUB_HOST="629914876889.dkr.ecr.us-east-1.amazonaws.com"
ARG PHP_IMAGE_VERSION="0.0.4"
ARG INVENTORY_VERSION

FROM ${DOCKERHUB_HOST}/inventory20:${INVENTORY_VERSION} as inventory

FROM ${DOCKERHUB_HOST}/lavu-php-run:${PHP_IMAGE_VERSION}

ARG APP_VERSION
ENV APP_VERSION=${APP_VERSION}

RUN groupadd -g 504 poslavu && useradd -u 504 -g 504 -m poslavu

RUN mkdir -p /home/poslavu/public_html/admin/cp/areas/inventory 

COPY --chown=poslavu prod/public_html /home/poslavu/public_html
COPY --chown=poslavu prod/private_html /home/poslavu/private_html

COPY --from=inventory --chown=poslavu /app/build /home/poslavu/public_html/admin/cp/areas/inventory/build

COPY --chown=poslavu DevOps /home/poslavu/DevOps
COPY --chown=poslavu External/secrets /home/poslavu/secrets

# Copy git commit number
COPY .git/refs /tmp/refs
COPY .git/HEAD /tmp/HEAD

# TAINT THE BUILD FROM HERE
# HARDCODED release.txt for https://jira.lavu.com/browse/OPS-541
# AND https://jira.lavu.com/browse/LP-8887
RUN echo 3.8.8 > /home/poslavu/public_html/admin/release.txt && \
    echo `cat /tmp/$(cat /tmp/HEAD | cut -d' ' -f2)` > /home/poslavu/public_html/admin/real-release.txt

# Copy config files
RUN cp /home/poslavu/DevOps/dockerfiles/config_files/install-monitoring.sh /var/tmp/install-monitoring.sh && \
      cp /home/poslavu/DevOps/dockerfiles/config_files/LogCopier.sh /var/tmp/LogCopier.sh && \
      cp /home/poslavu/DevOps/dockerfiles/config_files/wkhtmltopdf /var/tmp/wkhtmltopdf && \
      cp /home/poslavu/DevOps/dockerfiles/config_files/crontab /var/tmp/crontab && \
      cp /home/poslavu/DevOps/dockerfiles/repos/centos-base.release /etc/yum.repos.d/CentOS-Base.repo && \
      cp /home/poslavu/DevOps/dockerfiles/repos/epel.key /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7 && \
      cp /home/poslavu/DevOps/dockerfiles/httpd/httpd.conf /etc/httpd/conf/ && \
      cp /home/poslavu/DevOps/dockerfiles/httpd/php.conf /etc/httpd/conf.d/ && \
      cp /home/poslavu/DevOps/dockerfiles/httpd/sites.conf /etc/httpd/conf.d/ && \
      cp /home/poslavu/DevOps/dockerfiles/php/php.ini /etc/php.ini && \
      cp /home/poslavu/DevOps/dockerfiles/php/z-memcached.ini /etc/php.d/z-memcached.ini && \
      cp /home/poslavu/DevOps/dockerfiles/postfix/main.cf /etc/postfix/ && \
      cp /home/poslavu/DevOps/dockerfiles/postfix/master.cf /etc/postfix/ && \
      cp /home/poslavu/secrets/config_files/sasl_passwd /etc/postfix/ && \
      rm -rf /home/poslavu/DevOps 

ADD DevOps/dockerfiles/build_app.sh /root/build_app.sh
ADD DevOps/dockerfiles/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

RUN cp /home/poslavu/secrets/config_files/myinfo.php /home/poslavu/private_html/myinfo.php && \
      cp /home/poslavu/secrets/config_files/ZuoraConfig.php /home/poslavu/private_html/ZuoraConfig.php && \
      cp /home/poslavu/secrets/config_files/salesforceCredentials.php /home/poslavu/private_html/salesforceCredentials.php && \
      chown -R poslavu:apache /home/poslavu && chmod go+rx /home/poslavu && rm -rf /home/poslavu/secrets

ENV NR_INSTALL_SILENT 1
ENV NR_INSTALL_KEY "d9b43766dc7c03e3e9ccea722ae43a59284f7e9d"
ENV NR_APP_NAME "Lavu Backend Docker (PHP)"
ENV NR_STATUS "false"

# Add ASSET_VERSION variable https://jira.lavu.com/browse/LP-9354
#RUN export ASSET_VERSION=`cat /home/poslavu/public_html/admin/real-release.txt` && \
RUN export ASSET_VERSION="develop" && \
      ln -s /home/poslavu/public_html/admin/cp/styles /home/poslavu/public_html/admin/cp/styles/$ASSET_VERSION && \
      ln -s /home/poslavu/public_html/admin/cp/scripts /home/poslavu/public_html/admin/cp/scripts/$ASSET_VERSION && \
      chown poslavu:apache /home/poslavu/public_html/admin/cp/scripts/$ASSET_VERSION && \
      chown poslavu:apache /home/poslavu/public_html/admin/cp/styles/$ASSET_VERSION

#RUN echo export ASSET_VERSION=`cat /home/poslavu/public_html/admin/real-release.txt` > /etc/profile.d/poslavuenv.sh && \
#   chmod 0755 /etc/profile.d/poslavuenv.sh
# Move to external variable later: ENV ASSET_VERSION
ENV ASSET_VERSION "develop"

# Project variables
ENV INGRESS_HOSTNAMES "devcp.aws.devposlavu.com dev-cp-01.aws.devposlavu.com"
ENV CP3_URL "https://devcp3.aws.devposlavu.com"
ENV HUBQL_URL "https://dev-hubql-01.aws.devposlavu.com"
ENV IS_CP3_ENV "false"
ENV CP3_SESSION_EXPIRE_DAYS "30"
ENV PAYMENTAPI_URL "https://devpaymentapi.aws.devposlavu.com"

# Infra variables
ENV DATABASE_HOST "devdb.cbayhmoffsfj.us-east-1.rds.amazonaws.com"
ENV DATABASE_USERNAME "root"
ENV DATABASE_PASSWORD ""

# Redis
ENV REDIS_HOST "redis-silver-penguin-backend.default.svc.cluster.local"
ENV REDIS_SENTINEL_PORT "26379"
ENV REDIS_PASSWORD ""
ENV REDIS_LANGUAGE_DICTIONARY 0
ENV ENABLED_REDIS 1

# Memcached
ENV MEMCACHED_HOST "memc-silver-penguin-backend.default.svc.cluster.local"

# Environment
ENV ENVIRONMENT "development"

# PayPal
ENV PAYPAL_APP_ID ""
ENV PAYPAL_SECRET ""
ENV PAYPAL_MERCHANT_ID ""
ENV PAYPAL_URL ""
ENV PAYPAL_REFRESH_TOKEN_URL "https://dev-mobile-01.aws.devposlavu.com/paypal/refresh/php-encrypted"
ENV PAYPAL_ENV "sandbox"
ENV PAYPAL_ONBOARDING_URL "https://www.sandbox.paypal.com/"

#Bolt
ENV BOLT_API_KEY "ZCb8pPkXcZDVO0CIngLSFrBJgA/BYyUZIHT8zaj3MPg="


# Square
ENV SQUARE_APP_ID ""
ENV SQUARE_SECRET ""
ENV SQUARE_ACCESS_TOKEN ""
ENV SQUARE_LOCATION_ID ""

# PAPI
ENV PAPI_SECRET "7ejD3didj34"
ENV ENCRYPTION_KEY "mustbe32charslong123456789012345"
ENV PAPI_API "https://devpaymentapi.aws.devposlavu.com"

# DUDE BREW FTP CREDENTIALS
ENV DUDE_FTP_SERVER "ftp.vianetplc.com"
ENV DUDE_FTP_USER "FTP.TheDudes"
ENV DUDE_FTP_PWD "b9#4pL-9q:ve"
ENV DUDE_ENABLED 1

# S3 Variables
ENV AWS_END_POINT "https://fvf7uuzhq6.execute-api.us-east-1.amazonaws.com/dev/images"
ENV S3IMAGEURL "https://s3.amazonaws.com/dms-primary.development.lavu.com/"

# Mount files
ENV IMAGES_MOUNT_PATH "fs-88235368.efs.us-east-1.amazonaws.com"

# Change PHP Errors output
ENV PHP_ERROR_REPORTING "E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE & ~E_WARNING"

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]

#POS OLM variables
ENV OLM_ONLINE_FREQUENCY "60" 
ENV OLM_OFFLINE_FREQUENCY "60"
ENV OLM_HEALTH_CHECK_URI "hello.php?json=1"

# Manage Item Availability
ENV REDIS_SOCKET_IP "dev-redis-cluster-001.jceu0g.0001.use1.cache.amazonaws.com"
ENV REDIS_SOCKET_PORT "6379"
ENV REDIS_SOCKET_PASSWORD ""
ENV REDIS_SOCKET_ENABLED "1"
ENV KRAKEN_URL "https://dev-kraken-socketio.aws.devposlavu.com"

# Card Connect
ENV CARDCONNECT_URL "https://lavu-uat.cardconnect.com"

# hype lab offline mode
ENV HYPE_APP_IDENTIFIER "878f7238"
ENV HYPE_MODE "development"
ENV HYPE_ACCESS_TOKEN "37faf69861317e7e"

# BE Circut Breaker variables
ENV CB_PAPI_FAILURETHRESHOLD "5"
ENV CB_PAPI_RETRYTIMEPERIOD "20"
ENV CB_TGATE_FAILURETHRESHOLD "5"
ENV CB_TGATE_RETRYTIMEPERIOD "20"

# Secure Unsecure Protocol variable
ENV URI_SCHEME "https"
