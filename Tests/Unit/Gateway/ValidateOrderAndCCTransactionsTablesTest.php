<?php
/**
 *  Developer: Leif Guillermo
 *  Date 9/9/2016
 */
require_once(dirname(__FILE__). '/../../../prod/public_html/admin/lib/gateway_lib/ValidateOrderAndCCTransactionsTables.php');
require_once(dirname(__FILE__) . '/../../../../FrameworksRepo/delivery/vendor/autoload.php');

use PHPUnit\Framework\TestCase;

class ValidateOrderAndCCTransactionsTablesTest extends TestCase
{


	private function setUpValidatorForCheckReturnAmountAgainstOrdersTable($returnAmountToBeTransacted, $ordersRowTotalCollected)
	{
		$validator = new ValidateOrderAndCCTransactionsTables(null, null, null);
		$returnValue = $validator->checkReturnAmountAgainstOrdersTable($returnAmountToBeTransacted, $ordersRowTotalCollected);
		return $returnValue;
	}

	/**
	 * @test
	 * @dataProvider dataProviderForTestCheckReturnAmountAgainstOrdersTableReturnsTrue
	 */
	public function testCheckReturnAmountAgainstOrdersTableReturnsTrue($returnAmountToBeTransacted, $ordersRowTotalCollected)
	{
		$returnValue = $this->setUpValidatorForCheckReturnAmountAgainstOrdersTable($returnAmountToBeTransacted, $ordersRowTotalCollected);
		$this->assertEquals(true, $returnValue);
	}

	/*
	 * First returned parameter should be less than or equal to second returned parameter
	 * Both parameters must be greater than or equal to 0.00
	 * */
	public function dataProviderForTestCheckReturnAmountAgainstOrdersTableReturnsTrue()
	{
		return [
			["0.00", "0.00"],
			["0.00", "0.01"],
			["0.00", "1.00"],
			["0.50", "1.00"],
			["0.01", "0.01"],
			["1.00", "1.00"]
		];
	}

	/**
	 * @test
	 * @dataProvider dataProviderForTestCheckReturnAmountAgainstOrdersTableDoesNotReturnTrue
	 */
	public function testCheckReturnAmountAgainstOrdersTableDoesNotReturnTrue($returnAmountToBeTransacted, $ordersRowTotalCollected)
	{
		$returnValue = $this->setUpValidatorForCheckReturnAmountAgainstOrdersTable($returnAmountToBeTransacted, $ordersRowTotalCollected);
		$this->assertNotEquals(true, $returnValue);
	}

	public function dataProviderForTestCheckReturnAmountAgainstOrdersTableDoesNotReturnTrue()
	{
		return [
			["0.02", "0.01"],   //return is greater than amount-left
			["0.02", "-0.01"],  //return is greater than amount-left and amount left is negative
			["-1.00", "2.00"]   //return is negative, amount-left is acceptable.
		];
	}


	private function setUpValidatorForCompareCCTransactionSumAgainstOrdersTableTotal($ccTransactionSum, $orders_row_total_collected)
	{
		$validator = new ValidateOrderAndCCTransactionsTables(null, null, null);
		$returnValue = $validator->compareCCTransactionSumAgainstOrdersTableTotal($ccTransactionSum, $orders_row_total_collected);
		return $returnValue;
	}

	/**
	 * @test
	 * @dataProvider dataProviderForTestCompareCCTransactionSumAgainstOrdersTableTotalReturnsTrue
	 */
	public function testCompareCCTransactionSumAgainstOrdersTableTotalReturnsTrue($ccTransactionSum, $orders_row_total_collected)
	{
		$returnValue = $this->setUpValidatorForCompareCCTransactionSumAgainstOrdersTableTotal($ccTransactionSum, $orders_row_total_collected);
		$this->assertEquals(true, $returnValue);
	}

	/*
	 * Returns values that match.
	 * */
	public function dataProviderForTestCompareCCTransactionSumAgainstOrdersTableTotalReturnsTrue()
	{
		return [
			["0.00", "0.00"], //Both 0
			["0.01", "0.01"], //1 in the cents part, 0 for dollars
			["1.00", "1.00"], //1 in the dollars part, 0 for cents
			["1.23", "1.23"], //All digits non-0
			["91.23", "91.23"], //Test 10's place
			["991.23", "991.23"], //Test 100's place
			["9991.23", "9991.23"] //Test 1,000's place
		];
	}

	/**
	 * @test
	 * @dataProvider dataProviderForTestCompareCCTransactionSumAgainstOrdersTableTotalDoesNotReturnTrue
	 */
	public function testCompareCCTransactionSumAgainstOrdersTableTotalDoesNotReturnTrue($ccTransactionSum, $orders_row_total_collected)
	{
		$returnValue = $this->setUpValidatorForCompareCCTransactionSumAgainstOrdersTableTotal($ccTransactionSum, $orders_row_total_collected);
		$this->assertNotEquals(true, $returnValue);
	}

	/*
	 * Returns values that do not match.
	 * */
	public function dataProviderForTestCompareCCTransactionSumAgainstOrdersTableTotalDoesNotReturnTrue()
	{
		return [
			["0.00", "0.01"],    //Difference of one cent
			["0.01", "0.00"],    //Difference of one cent, opposite order
			["90.01", "0.01"],   //Difference where one param contains 10's place digit
			["0.01", "90.01"],   //Difference where other param contains 10's place digit
			["1.00", "2.00"],    //Difference only in 1's place
			["2.00", "1.00"],    //Difference only in 1's place, other order
			["1.00", "-1.00"],   //One positive, One negative
			["-1.00", "1.00"]    //One positive, One negative, other order
		];
	}

	/**
	 * @test
	 * @dataProvider dataProviderForTestSumSaleVoidAndRefundTransactionsFromCCTransactionsTable
	 */
	public function testSumSaleVoidAndRefundTransactionsFromCCTransactionsTable($rows, $expectedValue)
	{
		$method = new ReflectionMethod('ValidateOrderAndCCTransactionsTables', 'sumSaleVoidAndRefundTransactionsFromCCTransactionsTable');
		$method->setAccessible(true);
		$returnValue = $method->invokeArgs(new ValidateOrderAndCCTransactionsTables(), array($rows));
		$this->assertEquals($expectedValue, $returnValue);
	}

	public function dataProviderForTestSumSaleVoidAndRefundTransactionsFromCCTransactionsTable()
	{
		$row1 = array();
		$row1['voided'] = 0;
		$row1['action'] = 'Sale';
		$row1['total_collected'] = '10.00';
		$row2 = array();
		$row2['voided'] = 0;
		$row2['action'] = 'Void';
		$row2['total_collected'] = '3.00';
		$row3 = array();
		$row3['voided'] = 0;
		$row3['action'] = 'Refund';
		$row3['total_collected'] = '2.00';
		$row4 = array();
		$row4['voided'] = 1;
		$row4['action'] = 'Sale';
		$row4['total_collected'] = '2.00';

		$rows1 = [$row1];
		$rows2 = [$row2];
		$rows3 = [$row3];
		$rows4 = [$row4];
		$rows5 = [$row1 ,$row2, $row3, $row4];
		return [
			[$rows1, 10.00],
			[$rows2, -3.00],
			[$rows3, -2.00],
			[$rows4, 0.00],
			[$rows5, 5.00]
		];
	}

	public function dataProviderForTestCheckPNREFAgainstTransactionIDs()
	{

		$row1 = array();
		$row1['action'] = 'Sale';
		$row1['transaction_id'] = "abc"; //transaction id should match void
		$row1['internal_id'] = "def"; //internal id should match return

		$row2 = array();
		$row2['action'] = 'Return';
		$row2['return_pnref'] = "def";

		$row3 = array();
		$row3['action'] = 'Void';
		$row3['void_pnref'] = "abc";

		$rows1 = [$row1, $row2, $row3];

		$action1 = 'Void';
		$pnref_field1 = 'void_pnref';
		$id_field1 = 'transaction_id';

		$action2 = 'Return';
		$pnref_field2 = 'return_pnref';
		$id_field2 = 'internal_id';

		return [
			[$rows1, $action1, $pnref_field1, $id_field1, true],
			[$rows1, $action2, $pnref_field2, $id_field2, true]
		];
	}

	/**
	 * @test
	 * @dataProvider dataProviderForTestCheckPNREFAgainstTransactionIDs
	 */
	public function testCheckPNREFAgainstTransactionIDs($rows, $action, $pnref_field, $id_field, $expectedValue)
	{
		$method = new ReflectionMethod('ValidateOrderAndCCTransactionsTables', 'checkPNREFAgainstTransactionIDs');
		$method->setAccessible(true);
		$returnValue = $method->invokeArgs(new ValidateOrderAndCCTransactionsTables(), array($rows, $action, $pnref_field, $id_field));
		$this->assertEquals($expectedValue, $returnValue);

	}

	/**
	 * @test
	 * @dataProvider dataProviderForTestCheckPostedAuthCodeForExistenceInCCTransactionsTable
	 */
	public function testCheckPostedAuthCodeForExistenceInCCTransactionsTable($rows, $authCode, $expectedValue)
	{
		$method = new ReflectionMethod('ValidateOrderAndCCTransactionsTables', 'checkPostedAuthCodeForExistenceInCCTransactionsTable');
		$method->setAccessible(true);
		$returnValue = $method->invokeArgs(new ValidateOrderAndCCTransactionsTables(), array($rows, $authCode));
		$this->assertEquals($expectedValue, $returnValue);
	}

	public function dataProviderForTestCheckPostedAuthCodeForExistenceInCCTransactionsTable()
	{
		$row1['auth_code'] = 'abc';
		$row2['auth_code'] = 'def';
		$row3['auth_code'] = 'ghi';

		$rows1 = [$row1,$row2,$row3];

		$authCode1 = 'abc';
		$authCode2 = 'def';
		$authCode3 = 'ghi';

		return[
			[$rows1,$authCode1,true],
			[$rows1,$authCode2,true],
			[$rows1,$authCode3,true]
		];
	}
}
