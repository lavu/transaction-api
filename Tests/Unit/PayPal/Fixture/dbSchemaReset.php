<?php

require_once __DIR__.'/commonFunctions.php';

$conn = getConnection();
/**
 *  Create the query to initialize the auth_tokens database.
 */
$newTableSQL = 'CREATE TABLE `auth_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dataname` text,
  `extension_id` int(11) DEFAULT NULL,
  `token` text NOT NULL,
  `_deleted` int(1) DEFAULT \'0\',
  `token_type` text,
  PRIMARY KEY (`id`),
  KEY `extension_id` (`extension_id`),
  CONSTRAINT `auth_tokens_ibfk_1` FOREIGN KEY (`extension_id`) REFERENCES `extensions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;';

/**
 *  Drop the current auth_tokens table so the database is clear to add a new auth_tokens table.
 */
mysqli_query($conn, 'DROP TABLE auth_tokens');

/**
 * Add the new auth_tokens table.
 */
mysqli_query($conn, $newTableSQL);

/**
 *  Insert the various essential auth tokens for paypal.
 *  A Username token, Signature token, and Password token are generated.
 *  Tokens are created for Sandbox and Live environments.
 *  Each environment token gets an associated Internal Token, and App ID Token.
 */
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_SANDBOX_INTERNAL", "17", "EL8PZ0opdygRgPnD62LIFcWNbp5n2Ja6IVVJPhT39tI4c_3EF2UmYtglO2GoCJhXL8Md0QnrbMow30A9", 0, "secret")');
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_SANDBOX_APPID", "17", "Abaw7iES8yTHJmRtd1NN8CsEbhHa-4oDIHom1JuZAvpGuPsXmBoRJR4fUCrhXixNR9wVipVLsNmDF50r", 0, "client_id")');
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_PAYPALEC_SANDBOX", "17", "H6L7L5JHPP9ZA", 0, "merchantID")');
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_PAYPALEC_LIVE", "17", "RZ4UKYZV6QB6W", 0, "merchantID")');
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_PAYPAL_APPID", "17", "AQj3dWY1jLWd_dnxVz3tecg4EEnmRnh2W5E1bQzY2Xe2ACmt_3yX-7RCSq-kYDIKdqem16pRoBJmcDB0", 0, "client_id")');
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_PAYPAL_INTERNAL", "17", "EEcfQlFw89pPeMiucPCSaAt5vqWFebWelXW4FMWvqCYvwX0c2Z509v3q9i-xcVLPpwtHVBrQEiY7sIXJ", 0, "secret")');
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_API_UN_SANDBOX", "17", "Dev-facilitator_api1.lavu.com", 0, "API")');
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_API_PW_SANDBOX", "17", "MV3749AQVCZV6S6Y", 0, "API")');
mysqli_query($conn, 'INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES '.
                  '("POSLAVU_API_SIG_SANDBOX", "17", "AFcWxV21C7fd0v3bYYYRCpSSRl31A7B5eYt-ItXgSXfy6tbgAw-4yjTF", 0, "API")');
