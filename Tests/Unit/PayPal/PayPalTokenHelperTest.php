<?php

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */

class PayPalTokenHelperTest extends PHPUnit_Framework_TestCase
{
    private $_helper;
    private static $_conn;

    public static function setUpBeforeClass()
    {
        require __DIR__.'/Fixture/dbSchemaReset.php';
        self::$_conn = getConnection();

        require_once __DIR__.'/../../../prod/public_html/admin/components/payment_extensions/PayPal/PayPalTokenHelper.php';
    }

    public static function tearDownAfterClass()
    {
        include __DIR__.'/Fixture/dbSchemaReset.php';
    }

    public function setUp()
    {
        $this->_helper = new PayPalTokenHelper();
    }

    public function test_getRefreshToken_should_return_token_from_db()
    {
        $sql = "SELECT dataname, token_type, token FROM auth_tokens";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_helper->getRefreshToken($result[0], $result[1]);

        $this->assertEquals($result[2], $output);
    }

    public function test_getRefreshToken_should_return_error_if_no_record_found()
    {
        $result = $this->_helper->getRefreshToken("database", "type");

        $this->assertEquals("No token retrieved", $result['Error']);
    }

    public function test_insertRefreshToken_should_insert_a_new_record_and_return_insert_result()
    {
        $dataname = "POSLAVU_API_SIG_SANDBOX2";
        $output = $this->_helper->insertRefreshToken($dataname, "AFcWxV21C7fd0v3bYYYRCpSSRl31A7B5eYt-ItXgSXfy6tbgAw-4yjTF", "API");

        $sql = 'SELECT * FROM auth_tokens WHERE dataname = "'.$dataname.'"';
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertTrue(count($result) > 0);
    }

    public function test_insertRefreshToken_should_update_record_if_dataname_exists()
    {
        $fakeToken = "abcdefghijklmnopqrstuvwxyz";

        $sql = "SELECT dataname, token_type, token FROM auth_tokens ORDER BY id";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->_helper->insertRefreshToken($result[0], $fakeToken, $result[1]);

        $sql = 'SELECT dataname, token_type, token FROM auth_tokens WHERE dataname = "'.$result[0].'" ORDER BY id';
        $result2 = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[0], $result2[0]);
        $this->assertEquals($result[1], $result2[1]);
        $this->assertNotEquals($result[2], $result2[2]);
        $this->assertEquals($fakeToken, $result2[2]);

    }

    public function test_insertRefreshToken_should_return_error_if_token_is_not_provided()
    {
        $result = $this->_helper->insertRefreshToken("database", "", "type");

        $this->assertEquals("Error: No valid token", $result);
    }
}
