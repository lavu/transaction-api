<?php


class PayPalCoreTest extends PHPUnit_Framework_TestCase
{

    public static function setUpBeforeClass()
    {
        require __DIR__.'/Fixture/dbSchemaReset.php';
        require_once __DIR__.'/../../../prod/public_html/admin/components/payment_extensions/PayPal/PayPalCore.php';
    }

    public function test_instantiating_new_instance_should_set_up_correct_mode_sandbox()
    {
        $obj = new PayPalCore("sandbox");
        $this->assertEquals("Abaw7iES8yTHJmRtd1NN8CsEbhHa-4oDIHom1JuZAvpGuPsXmBoRJR4fUCrhXixNR9wVipVLsNmDF50r", $obj->lavu_client_id);
        $this->assertEquals("EL8PZ0opdygRgPnD62LIFcWNbp5n2Ja6IVVJPhT39tI4c_3EF2UmYtglO2GoCJhXL8Md0QnrbMow30A9", $obj->lavu_secret);
        $this->assertEquals("H6L7L5JHPP9ZA", $obj->lavu_merchantID);
        $this->assertEquals("https://www.sandbox.paypal.com/", $obj->PayPalEndpointURL);
    }

    public function test_instantiating_new_instance_should_set_up_correct_mode_live()
    {
        $obj = new PayPalCore();
        $this->assertEquals("AQj3dWY1jLWd_dnxVz3tecg4EEnmRnh2W5E1bQzY2Xe2ACmt_3yX-7RCSq-kYDIKdqem16pRoBJmcDB0", $obj->lavu_client_id);
        $this->assertEquals("EEcfQlFw89pPeMiucPCSaAt5vqWFebWelXW4FMWvqCYvwX0c2Z509v3q9i-xcVLPpwtHVBrQEiY7sIXJ", $obj->lavu_secret);
        $this->assertEquals("RZ4UKYZV6QB6W", $obj->lavu_merchantID);
        $this->assertEquals("https://www.paypal.com/", $obj->PayPalEndpointURL);
    }
}
