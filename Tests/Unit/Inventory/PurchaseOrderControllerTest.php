<?php

/**
 * Created For Lavu
 * User: Ruben
 * Date: 2/1/16
 */
/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class PurchaseOrderControllerTest extends PHPUnit_Framework_TestCase
{

    private static $_conn;
    private $_controller;

    public static function setUpBeforeClass()
    {
        date_default_timezone_set('America/Denver');
        global $dataname;
        $dataname = "ll-lavudemochr";

        global $loc_id;
        $loc_id = 1;

        require_once (__DIR__ . "/Fixture/SetupDefaultDB.php");
        self::$_conn = getConnection();
        require_once(__DIR__ . '/../../../prod/public_html/admin/components/API/Inventory/Controllers/PurchaseOrderController.php');
    }

    public function setUp()
    {
        $this->_controller = new PurchaseOrderController();
    }

    public function test_add_PurchaseOrder_should_add_new_records()
    {
        $output = $this->_controller->routeCommand("purchaseorder", "add", '[{"vendorID":"1"},{"vendorID":"2"}]');

        $sql = "SELECT COUNT(*) FROM inventory_purchaseorder WHERE order_date = '".$output[0]['order_date']."' AND received = '0' AND _deleted = 0 AND id > 2";
        $result = mysqli_query(self::$_conn, $sql)->fetch_all();

        $this->assertEquals($result[0][0], count($output));
    }

    public function test_get_purchaseOrder_should_return_all_records_that_have_not_been_deleted()
    {
        $output = $this->_controller->routeCommand("purchaseorder", "get");

        $sql = "SELECT COUNT(*) FROM inventory_purchaseorder WHERE _deleted = 0";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[0][0], count($output));
    }

    public function test_receive_PurchaseOrders_should_update_record_with_time_date_and_received_flag()
    {
        $output = $this->_controller->routeCommand("purchaseorder", "receive", '[{"id":"2"}]');

        $sql = "SELECT receive_date, received FROM inventory_purchaseorder WHERE _deleted = 0 AND id = '2'";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals(date("Y-m-d H:i:s"), $result[0]);
        $this->assertEquals(1, $result[1]);
    }

    public function test_add_purchase_order_items_should_add_new_records()
    {
        $output = $this->_controller->routeCommand("item", "add", '[{"purchaseOrderID":"2", "vendorItemID":"1", "unitID":"2", "quantityOrdered":"200", "SKU":"10001"}, {"purchaseOrderID":"2", "vendorItemID":"1", "unitID":"2", "quantityOrdered":"300", "SKU":"10001"}]');

        $sql = "SELECT COUNT(*) FROM purchaseorder_items WHERE purchaseOrderID = '2'";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[0], count($output));
    }

    public function test_get_purchase_order_items_should_return_specific_record()
    {
        $output = $this->_controller->routeCommand("item", "get", '{"id":"1"}');

        $this->assertEquals(1, count($output));
        $this->assertEquals(1, $output[0]['id']);
    }

    public function test_get_purchase_order_items_should_return_all_record()
    {
        $output = $this->_controller->routeCommand("item", "get");

        $sql = "SELECT COUNT(*) FROM purchaseorder_items WHERE _deleted = 0";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[0], count($output));
    }

    public function test_update_purchase_order_item_should_change_field_values_for_changed_properties()
    {
        $sql = "SELECT * FROM purchaseorder_items WHERE id = '1'";
        $oldRecord = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("item", "update", '[{"id":"1", "purchaseOrderID":"1", "vendorItemID":"2", "unitID":"2", "quantityOrdered":"400", "cost":"300.50", "quantityReceived":"200", "SKU":"15551"}]');

        $newRecord = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($oldRecord[0], $newRecord[0]);
        $this->assertEquals($oldRecord[1], $newRecord[1]);
        $this->assertEquals($oldRecord[2], $newRecord[2]);
        $this->assertEquals($oldRecord[3], $newRecord[3]);
        $this->assertNotEquals($oldRecord[4], $newRecord[4]);
        $this->assertNotEquals($oldRecord[5], $newRecord[5]);
        $this->assertNotEquals($oldRecord[6], $newRecord[6]);
        $this->assertEquals($oldRecord[7], $newRecord[7]);
        $this->assertEquals($oldRecord[8], $newRecord[8]);

    }

    public function test_controller_should_return_error_if_object_is_missing()
    {
        $output = $this->_controller->routeCommand("","get");
        $this->assertEquals("No valid command", $output['ERROR']);
    }

    public function test_controller_should_return_error_if_method_is_missing()
    {
        $output = $this->_controller->routeCommand("item","");
        $this->assertEquals("No valid command", $output['ERROR']);
    }

    public function test_controller_should_return_error_if_object_is_not_found()
    {
        $output = $this->_controller->routeCommand("apple","get");
        $this->assertEquals("No valid command 2", $output['ERROR']);
    }

    public function test_controller_should_return_error_if_method_is_not_found()
    {
        $output = $this->_controller->routeCommand("item","put");
        $this->assertEquals("No valid command 2", $output['ERROR']);
    }

}
