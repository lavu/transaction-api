<?php
/**
 * Created For Lavu
 * User: Ruben
 * Date: 1/27/16
 */

@require_once (dirname(__FILE__) . '/../../../../prod/private_html/Inventory/InventoryTableCreation.php');
@require_once (dirname(__FILE__) . '/../../../../prod/public_html/admin/components/API/Helpers/queryHelper.php');

function getConnection()
{
    $servername = "localhost";
    $username = "root";
    $password = "password";
    $dbname = "poslavu_ll-lavudemochr_db";

    return mysqli_connect($servername,$username,$password,$dbname);
}

function getLiquidId($conn)
{
    $sql = "SELECT id FROM unit_category WHERE name = 'Liquid'";
    return mysqli_query($conn, $sql)->fetch_object()->id;
}

$conn = getConnection();

if(!$conn)
{
    die("Error connecting to database");
}

//Unit Category

$sql = "INSERT INTO unit_category (name) VALUES ('Solid')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to category");
}

$sql = "INSERT INTO unit_category (name) VALUES ('Liquid')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to category");
}

$sql = "INSERT INTO unit_category (name) VALUES ('General')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to category");
}

$sql = "INSERT INTO unit_category (name) VALUES ('Custom')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to category");
}

$liquidId = getLiquidId($conn);

$sql = "SELECT id FROM unit_category WHERE name = 'General'";
$generalId = mysqli_query($conn, $sql)->fetch_object()->id;


//Inventory Unit

$sql = "INSERT INTO inventory_unit (name, symbol, categoryID, conversion) VALUES ('Bottle', 'btl', {$generalId}, 1)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to Unit");
}

$sql = "INSERT INTO inventory_unit (name, symbol, categoryID, conversion) VALUES ('Each', 'ea', {$generalId}, 1)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to Unit");
}

$sql = "INSERT INTO inventory_unit (name, symbol, categoryID, conversion) VALUES ('6Pack', '6pk', {$generalId}, ". 1/6 .")";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to Unit");
}

$sql = "INSERT INTO inventory_unit (name, symbol, categoryID, conversion) VALUES ('Ounce', 'oz', {$liquidId}, 1)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to Unit");
}


//Inventory Categories

$sql = "INSERT INTO inventory_categories (name, description, loc_id) VALUES ('Pizza Toppings', 'All toppings that go on top of a pizza', '1')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to inventory categories");
}


//Inventory Items

$sql = "INSERT INTO inventory_items (name, categoryID, unitID, quantity, lowQuantity, parQuantity, loc_id) VALUES ('Pepperoni', '1', '2', '100.0', '101.0', '100.0', '1')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to inventory item");
}

$sql = "INSERT INTO inventory_items (name, categoryID, unitID, quantity, lowQuantity, parQuantity, loc_id) VALUES ('Green Chile', '1', '2', '200.0', '201.0', '100.0', '1')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to inventory item");
}

$sql = "INSERT INTO inventory_items (name, categoryID, unitID, quantity, lowQuantity, parQuantity, loc_id) VALUES ('Sausage', '1', '2', '200.0', '201.0', '100.0', '1')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to inventory item");
}



//Inventory Audit

$sql = "INSERT INTO inventory_audit (inventoryItemID, oldCount, newCount, userID, userNotes, loc_id) VALUES ('1', '200.0', '100.0', '2', 'Dropped 100 worth of pep', '1')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to inventory audit");
}


//Vendors

$sql = "INSERT INTO vendors (name, contactName, contactPhone, contactEmail, loc_id) VALUES ('VendorsRus', 'Trisha', '555-555-5555', 'trisha@vendorsrus.com', '1')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to vendor");
}

$sql = "INSERT INTO vendors (name, contactName, contactPhone, contactEmail, loc_id) VALUES ('Rubens Market', 'Ruben', '555-555-5555', 'ruben@rm.com', '1')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to vendor");
}

$sql = "INSERT INTO vendors (name, contactName, contactPhone, contactEmail, loc_id) VALUES ('Joes Fast Food', 'joe', '555-555-5555', 'joe@jff.com', '1')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add Item to vendor");
}

//Vendor Items

$sql = "INSERT INTO vendor_items (inventoryItemID, vendorID, unitID, minOrderQuantity, averageCost, SKU) VALUES ('1', '1', '2', '200', '109.56', '10001')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to vendor items");
}

$sql = "INSERT INTO vendor_items (inventoryItemID, vendorID, unitID, minOrderQuantity, averageCost, SKU) VALUES ('1', '2', '2', '150', '57.50', '15551')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to vendor items");
}

//Inventory Purchase order

$sql = "INSERT INTO inventory_purchaseorder (vendorID, receive_date, received, loc_id, _deleted) VALUES ('2', '".date("Y-m-d H:i:s")."', 1, 1, 0)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to inventory purchase order");
}

$sql = "INSERT INTO inventory_purchaseorder (vendorID, loc_id, _deleted) VALUES ('1', 1, 0)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to inventory purchase order");
}

//Purchase order items

$sql = "INSERT INTO purchaseorder_items (purchaseOrderID, vendorItemID, unitID, quantityOrdered, SKU, _deleted) VALUES ('1', '2', '2', '200', '15551', '0')";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to purchase order items");
}


//86 Menu Items

$sql = "SELECT * FROM menu_items ORDER BY id";
$menuItemID = mysqli_query($conn, $sql)->fetch_row();
$menuItemID = $menuItemID[0];
$sql = "INSERT INTO menuitem_86 (menuItemID, inventoryItemID, quantityUsed, unitID) VALUES ({$menuItemID}, 1, 150, 2)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to 86 menu items");
}

$sql = "SELECT * FROM menu_items WHERE id != {$menuItemID} ORDER BY id";
$menuItemID = mysqli_query($conn, $sql)->fetch_row();
$menuItemID = $menuItemID[0];
$sql = "INSERT INTO menuitem_86 (menuItemID, inventoryItemID, quantityUsed, unitID, reservation) VALUES ({$menuItemID}, 3, 150, 2, 2)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to 86 menu items");
}

//86 Modifier

$sql = "SELECT id FROM modifiers ORDER BY id";
$modifierID = mysqli_query($conn, $sql)->fetch_row();
$modifierID = $modifierID[0];
$sql = "INSERT INTO modifier_86 (modifierID, inventoryItemID, quantityUsed, unitID) VALUES ({$modifierID}, 1, 150, 2)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to 86 modifiers");
}

$sql = "SELECT id FROM modifiers WHERE id != {$modifierID} ORDER BY id";
$modifierID = mysqli_query($conn, $sql)->fetch_row();
$modifierID = $modifierID[0];
$sql = "INSERT INTO modifier_86 (modifierID, inventoryItemID, quantityUsed, unitID, reservation) VALUES ({$modifierID}, 3, 150, 2, 2)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to 86 modifiers");
}


//86 Forced Modifier

$sql = "SELECT id FROM forced_modifiers ORDER BY id";
$modifierID = mysqli_query($conn, $sql)->fetch_row();
$modifierID = $modifierID[0];
$sql = "INSERT INTO forced_modifier_86 (forced_modifierID, inventoryItemID, quantityUsed, unitID) VALUES ({$modifierID}, 1, 150, 2)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to 86 modifiers");
}

$sql = "SELECT id FROM forced_modifiers WHERE id != {$modifierID} ORDER BY id";
$modifierID = mysqli_query($conn, $sql)->fetch_row();
$modifierID = $modifierID[0];
$sql = "INSERT INTO forced_modifier_86 (forced_modifierID, inventoryItemID, quantityUsed, unitID, reservation) VALUES ({$modifierID}, 3, 150, 2, 2)";
if(false === mysqli_query($conn, $sql))
{
    die("Unable to add record to 86 modifiers");
}