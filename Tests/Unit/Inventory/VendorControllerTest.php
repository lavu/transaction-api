<?php

/**
 * Created For Lavu
 * User: Ruben
 * Date: 1/29/16
 */
/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */

class VendorControllerTest extends PHPUnit_Framework_TestCase
{
    private static $_conn;
    private $_controller;

    public static function setUpBeforeClass()
    {
        global $dataname;
        $dataname = "ll-lavudemochr";

        global $loc_id;
        $loc_id = 1;

        require_once (__DIR__ . "/Fixture/SetupDefaultDB.php");
        self::$_conn = getConnection();

        require_once(__DIR__ . '/../../../prod/public_html/admin/components/API/Inventory/Controllers/VendorController.php');
    }

    public function setUp()
    {
        $this->_controller = new VendorController();
    }

    public function test_vendor_get_should_return_all_vendor_records()
    {
        $output = $this->_controller->routeCommand('vendor', 'get');

        $sql = "SELECT COUNT(*) FROM vendors WHERE _deleted = '0'";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[0],count($output));
    }


    public function test_vendor_add_should_add_new_records_and_return_new_records()
    {
        $output = $this->_controller->routeCommand('vendor', 'add', '[{"name":"VendorPro", "contactEmail":"ben@vendorpro.com", "contactPhone":"555-555-5555", "contactName":"Ben"}, '.
                                                                     '{"name":"VendorSelect", "contactEmail":"joe@vendorselect.com", "contactPhone":"555-555-5555", "contactName":"joe"}]');

        $this->assertEquals(2, count($output));
    }

    public function test_vendor_update_should_update_specific_record()
    {
        $sql = "SELECT id, name, contactEmail, contactPhone, contactName FROM vendors ORDER BY id ASC";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand('vendor', 'update', '[{"id":"'.$result[0].'", "name":"'.$result[1].'", "contactEmail":"'.$result[2].'", "contactPhone":"'.$result[3].'", "contactName":"MyName"}]');

        $sql = "SELECT id, name, contactEmail, contactPhone, contactName FROM vendors WHERE id = {$result[0]} ORDER BY id ASC";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[4], $output[0]['contactName']);
    }

    public function test_vendor_items_get_should_return_specific_or_all_records()
    {
        $output = $this->_controller->routeCommand('item', 'get', '{"id":"1"}');
        $this->assertEquals("10001", $output[0]['SKU']);

        $output = $this->_controller->routeCommand('item', 'get', '{"inventoryItemID":"1", "vendorID":"1"}');
        $this->assertEquals(1, count($output));

        $output = $this->_controller->routeCommand('item', 'get', '{"vendorID":"1"}');
        $this->assertEquals(1, count($output));

        $output = $this->_controller->routeCommand('item', 'get', '{"unitID":"2", "SKU":"15551"}');
        $this->assertEquals(1, count($output));

        $output = $this->_controller->routeCommand('item', 'get', '{"SKU":"10001"}');
        $this->assertEquals(1, count($output));
    }

    public function test_vendor_item_add_should_add_a_new_records()
    {
        $output = $this->_controller->routeCommand('item', 'add', '[{"inventoryItemID":"1", "vendorID":"2", "unitID":"2", "minOrderQuantity":"100", "SKU":"10002"},'.
                                                                   '{"inventoryItemID":"1", "vendorID":"3", "unitID":"2", "minOrderQuantity":"25", "SKU":"10003"}]');

        $sql = "SELECT * FROM vendor_items WHERE inventoryItemID = 1";
        $result = mysqli_query(self::$_conn, $sql)->fetch_all();

        $this->assertTrue(is_numeric($output[0]['id']));
        $this->assertTrue(is_numeric($output[1]['id']));
        $this->assertEquals(4, count($result));
    }

    public function test_vendor_item_update_should_update_records()
    {
        $output = $this->_controller->routeCommand('item', 'update', '[{"id":"1", "inventoryItemID":"1", "vendorID":"1", "unitID":"2", "minOrderQuantity":"75", "SKU":"12351"}]');

        $sql = "SELECT * FROM vendor_items WHERE id = 1";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($output[0]['minOrderQuantity'], $result[4]);
        $this->assertEquals($output[0]['SKU'], $result[6]);
    }

    public function test_vendor_controller_should_return_error_if_object_is_not_set()
    {
        $output = $this->_controller->routeCommand('', 'update');

        $this->assertEquals("No valid object", $output['ERROR']);
    }

    public function test_vendor_controller_should_return_error_if_method_is_not_set()
    {
        $output = $this->_controller->routeCommand('vendor', '');

        $this->assertEquals("No valid method", $output['ERROR']);
    }

    public function test_vender_add_should_return_error_if_property_is_not_set()
    {
        $output = $this->_controller->routeCommand('vendor', 'add', '[{"names":"VendorPro", "contactEmails":"ben@vendorpro.com", "contactName":"Ben"}]');

        $this->assertEquals("Vendor Add property not set", $output['ERROR']);
    }

    public function test_vendor_update_should_return_error_if_property_is_not_set()
    {
        $output = $this->_controller->routeCommand('vendor', 'update', '[{"id":"1", "name":"1", "contactPhone":"555-505-5534", "contactName":"MyName"}]');

        $this->assertEquals("Vendor update property not set", $output['ERROR']);
    }

    public function test_vendor_item_update_should_return_error_if_property_not_set()
    {
        $output = $this->_controller->routeCommand('item', 'update', '[{"id":"1", "inventoryItemID":"1", "unitID":"2", "minOrderQuantity":"75", "SKU":"12351"}]');

        $this->assertEquals('Error vendor items property not set', $output['ERROR']);
    }
}
