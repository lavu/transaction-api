<?php

/**
 * Created For Lavu
 * User: Ruben
 * Date: 1/27/16
 */
/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class InventoryManagementControllerTest extends PHPUnit_Framework_TestCase
{
    private $_controller;
    private static $_conn;

    public static function setUpBeforeClass()
    {
        global $dataname;
        $dataname = "ll-lavudemochr";

        global $loc_id;
        $loc_id = 1;

        require_once (__DIR__ . "/Fixture/SetupDefaultDB.php");
        self::$_conn = getConnection();

        require_once(dirname(__FILE__) . '/../../../prod/public_html/admin/components/API/Inventory/Controllers/InventoryManagementController.php');
    }

    public function setUp()
    {
        $this->_controller = new InventoryManagementController();
    }

    public function test_get_category_should_return_all_categories()
    {
        $output = $this->_controller->routeCommand("category", "get");
        $sql = "SELECT COUNT(*) FROM inventory_categories";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();
        $this->assertEquals($result[0],count($output));
    }

    public function test_add_category_should_add_a_record_to_the_database_and_return_the_new_record()
    {
        $output = $this->_controller->routeCommand("category", "add", '{"name":"Bread", "description":"All bread types here"}');
        $this->assertTrue(count($output) > 0);
        $this->assertEquals("Bread", $output[0]["name"]);
    }

    public function test_add_2_items_should_add_a_item_to_inventory_and_return_new_record()
    {
        $output = $this->_controller->routeCommand("item", "add", '[{"name":"Olives", "categoryID":"1", "unitID":"2", "quantity":"75.0", "lowQuantity":"15.0", "parQuantity":"75.0"}, '.
            '{"name":"Cheese", "categoryID":"1", "unitID":"2", "quantity":"300.0", "lowQuantity":"100.0", "parQuantity":"500.0"}]');
        $this->assertTrue(count($output) == 2);
        $this->assertEquals("Olives", $output[0]["name"]);
        $this->assertEquals("Cheese", $output[1]["name"]);
    }

    public function test_get_items_should_return_all_records_from_inventory_items_table()
    {
        $output = $this->_controller->routeCommand("item", "get");
        $sql = "SELECT COUNT(*) FROM inventory_items";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();
        $this->assertEquals($result[0],count($output));
    }

    public function test_update_item_should_update_record_and_return_success_true()
    {
        $output = $this->_controller->routeCommand("item", "update", '[{"name":"Pepperoni", "categoryID":"1", "unitID":"2", "quantity":"98.001", "lowQuantity":"25.0", "parQuantity":"100.0", "id":"1"}]');
        $this->assertEquals("true", $output['SUCCESS']);
        $sql = "SELECT quantity FROM inventory_items WHERE id = 1";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();
        $this->assertEquals("98.00100", $result[0]);
    }

    public function test_add_audit_should_add_a_new_record_and_return_success_true()
    {
        $output = $this->_controller->routeCommand("audit", "add", '{"inventoryItemID":"1", "newCount":"50.0", "userID":"2", "userNotes":"Test Change count"}');
        $this->assertEquals("true", $output['SUCCESS']);
        $sql = "SELECT * FROM inventory_audit WHERE newCount = '50.00000'";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();
        $this->assertEquals("50", $result[3]);
    }

    public function test_get_audit_with_no_json_should_return_all_audit_records()
    {
        $output = $this->_controller->routeCommand("audit", "get", '');
        $sql = "SELECT COUNT(*) FROM inventory_audit";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();
        $this->assertEquals($result[0],count($output));
    }

    public function test_get_audit_with_inventoryItemID_should_return_specific_record()
    {
        $output = $this->_controller->routeCommand("audit", "get", '{"inventoryItemID":"1"}');

        $sql = "SELECT count(*) FROM inventory_audit WHERE inventoryItemID = 1";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[0], count($output));
    }

    public function test_get_audit_with_userID_should_return_specific_record()
    {
        $output = $this->_controller->routeCommand("audit", "get", '{"userID":"2"}');

        $sql = "SELECT count(*) FROM inventory_audit WHERE userID = 2";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[0], count($output));
    }

    public function test_error_returned_when_no_method_found()
    {
        $output = $this->_controller->routeCommand();
        $this->assertEquals("No valid Command", $output["ERROR"]);

        $output = $this->_controller->routeCommand("pie", "get");
        $this->assertEquals("No valid Command", $output["ERROR"]);

        $output = $this->_controller->routeCommand("item", "put");
        $this->assertEquals("No valid Command", $output["ERROR"]);

        $output = $this->_controller->routeCommand("category", "put");
        $this->assertEquals("No valid Command", $output["ERROR"]);

        $output = $this->_controller->routeCommand("audit", "put");
        $this->assertEquals("No valid Command", $output["ERROR"]);
    }

    public function test_updateItem_returns_error_when_json_is_not_correct_or_blank()
    {
        $output = $this->_controller->routeCommand("item", "update");
        $this->assertEquals("Json parser error", $output["ERROR"]);

        $output = $this->_controller->routeCommand("item", "update", "{name:foo}");
        $this->assertEquals("Json parser error", $output["ERROR"]);
    }

    public function test_updateItem_returns_error_when_property_not_set()
    {
        $output = $this->_controller->routeCommand("item", "update", '[{"name":"Pepperoni", "categoryIDs":"1", "unitID":"2", "quantity":"98.001", "lowQuantity":"25.0", "parQuantity":"100.0", "id":"1"}]');
        $this->assertEquals("Json property not set", $output["ERROR"]);
    }

    public function test_addItem_returns_error_when_property_not_set()
    {
        $output = $this->_controller->routeCommand("item", "add", '[{"name":"Olives", "categoryID":"1", "unitID":"2", "quantitys":"75.0", "lowQuantity":"15.0", "parQuantity":"75.0"},');
        $this->assertEquals("Json property not set", $output["ERROR"]);
    }

    public function test_addCategory_returns_error_when_property_not_set()
    {
        $output = $this->_controller->routeCommand("category", "add", '{"Name":"Bread", "description":"All bread types here"}');
        $this->assertEquals("Json property not set", $output["ERROR"]);
    }

    public function test_addAudit_returns_error_when_property_not_set()
    {
        $output = $this->_controller->routeCommand("audit", "add", '{"inventoryItemID":"1", "newCount":"50.0", "usersID":"2", "userNotes":"Test Change count"}');
        $this->assertEquals("Json property not set", $output["ERROR"]);
    }
}
