<?php

/**
 * Created For Lavu
 * User: Ruben
 * Date: 2/2/16
 */

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class myClass extends stdClass
{
    public function execute($arg)
    {

    }
}

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class _86CountControllerTest extends PHPUnit_Framework_TestCase
{
    private static $_conn;
    private $_controller;

    public static function setUpBeforeClass()
    {
        global $dataname;
        $dataname = "ll-lavudemochr";

        global $loc_id;
        $loc_id = 1;

        require_once (__DIR__ . "/Fixture/SetupDefaultDB.php");
        self::$_conn = getConnection();
        require_once(__DIR__ . '/../../../prod/public_html/admin/components/API/Inventory/Controllers/86CountController.php');
    }

    public function setUpClean()
    {
        $this->_controller = new _86CountController();
    }

    public function setUpWithMock()
    {
        $queryHelper = $this->getMockBuilder('queryHelper')->getMock();
        $queryHelper->method('executeQuery')->willReturn(false);

        $queryHelper->method('prepareQuery')->willReturnCallback(function ($querystring)
        {
            $queryPrepare = $this->getMockBuilder('myClass')->getMock();
            $queryPrepare->method("execute")->withAnyParameters()->willReturn(false);
            return $queryPrepare;
        });

        $this->_controller = new _86CountController($queryHelper);
    }

    public function test_controller_should_return_error_if_object_is_missing()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("", "get");

        $this->assertEquals("No valid Command", $output['ERROR']);
    }

    public function test_controller_should_return_error_if_method_is_missing()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("menuitem", "");

        $this->assertEquals("No valid Command", $output['ERROR']);
    }

    public function test_controller_should_return_error_if_object_is_misspelled_or_not_supported()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("item", "get");

        $this->assertEquals("No valid Command", $output['ERROR']);
    }

    public function test_controller_should_return_error_if_method_is_misspelled_or_not_supported()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("menuitem", "put");

        $this->assertEquals("No valid Command", $output['ERROR']);
    }

    public function test_get_menu_item_should_return_all_records()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("menuitem", 'get');

        $sql = "SELECT COUNT(*) FROM menuitem_86";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals($result[0], count($output));
    }

    public function test_out_of_stock_should_return_all_records_that_meet_out_of_stock_requirements()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("menuitem", "outofstock");

        $this->assertTrue(count($output) > 0);
    }

    public function test_low_Stock_Menu_Items_should_return_menu_items_that_are_under_low_quantity_fore_each_item()
    {
        $this->setUpClean();
        $sql = "UPDATE inventory_items SET lowQuantity = 101, quantity = 100 WHERE id = 1";
        mysqli_query(self::$_conn, $sql);

        $output = $this->_controller->routeCommand("menuitem", "lowstock");

        $this->assertTrue(count($output) > 0);
    }

    public function test_add_should_add_new_record_in_menuitem_86_and_return_new_record()
    {
        $this->setUpClean();
        $sql = "SELECT id FROM menu_items WHERE id NOT IN (474,476)";
        $menuItemID = mysqli_query(self::$_conn, $sql)->fetch_row();
        $menuItemID = $menuItemID[0];

        $output = $this->_controller->routeCommand("menuitem", "add", '[{"menuItemID":"'.$menuItemID.'", "inventoryItemID":"2", "criticalItem":"1", "quantityUsed":"200", "unitID":"2"}]');

        $sql = "SELECT * FROM menuitem_86 WHERE id = {$output[0]['id']}";
        $result = mysqli_query(self::$_conn, $sql)->fetch_all();

        $this->assertTrue(count($result) > 0);
    }

    public function test_update_should_update_specific_menu_item_86_record()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("menuitem", "update", '[{"quantityUsed":"200", "id":"1"}]');

        $sql = "SELECT quantityUsed FROM menuitem_86 WHERE id = 1";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals("200", $result[0]);
        $this->assertEquals("Items updated", $output["SUCCESS"]);
    }

    public function test_reserve_should_add_to_the_item_reserved_and_return_success()
    {
        $this->setUpClean();
        $sql = "SELECT reservation FROM menuitem_86 WHERE id = 1";
        $old = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("menuitem","reserve",'[{"menuItemID":"474"}]');

        $new = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertNotEquals($old[0], $new[0]);
        $this->assertTrue($new[0] > $old[0]);
        $this->assertEquals("Items updated", $output['SUCCESS']);
    }

    public function test_reserve_should_return_error_if_menuItemID_is_not_found()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("menuitem", "reserve", '[{"id":"474"}]');
        $this->assertEquals("Property not set", $output["ERROR"]);
    }

    public function test_close_menu_item_should_decrease_reservation_and_quantity_in_inventory()
    {
        $this->setUpClean();
        $sql = "SELECT menuItemID, inventoryItemID, reservation FROM menuitem_86 WHERE reservation > 0 ORDER BY reservation DESC";
        $menuItem = mysqli_query(self::$_conn, $sql)->fetch_row();
        $sql = "SELECT quantity FROM inventory_items WHERE id = {$menuItem[1]}";
        $quantity = mysqli_query(self::$_conn, $sql)->fetch_row();
        $quantity = $quantity[0];

        $output = $this->_controller->routeCommand("menuitem", "close", '[{"menuItemID":"'.$menuItem[0].'"}]');

        $sql = "SELECT reservation FROM menuitem_86 WHERE menuItemID = ". $menuItem[0];
        $menuItemNew = mysqli_query(self::$_conn, $sql)->fetch_row();
        $sql = "SELECT quantity FROM inventory_items WHERE id = {$menuItem[1]}";
        $quantityNew = mysqli_query(self::$_conn, $sql)->fetch_row();
        $quantityNew = $quantityNew[0];

        $this->assertTrue($menuItem[2] > $menuItemNew[0]);
        $this->assertTrue($quantity > $quantityNew);
        $this->assertTrue(isset($output['SUCCESS']));
        $this->assertEquals("Items updated", $output['SUCCESS']);
    }

    public function test_cancel_menu_item_should_decrease_reservation()
    {
        $this->setUpClean();
        $sql = "SELECT menuItemID, reservation, id FROM menuitem_86 WHERE reservation != 0 ORDER BY reservation DESC";
        $menuItem = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("menuitem", "cancel", '[{"menuItemID":"'.$menuItem[0].'"}]');

        $sql = "SELECT reservation FROM menuitem_86 WHERE id = {$menuItem[2]}";
        $menuItemNew = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals("Items updated",$output['SUCCESS']);
        $this->assertTrue($menuItem[1] > $menuItemNew[0]);
    }

    public function test_get_modifier_should_return_all_modifiers()
    {
        $this->setUpClean();
        $sql = "SELECT count(*) FROM modifier_86 WHERE _deleted = 0";
        $count = mysqli_query(self::$_conn, $sql)->fetch_row()[0];

        $output = $this->_controller->routeCommand("modifier", 'get');

        $this->assertEquals($count, count($output));
    }

    public function test_out_of_stock_modifier_should_return_all_records_that_meet_out_of_stock_requirements()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("modifier", "outofstock");

        $this->assertTrue(count($output) > 0);
    }

    public function test_low_stock_should_return_all_records_that_are_now_in_quantity()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("modifier", "lowstock");

        $this->assertTrue(count($output) > 0);
    }

    public function test_add_modifier_should_add_new_record_in_modifier_86_and_return_new_record()
    {
        $this->setUpClean();
        $sql = "SELECT id FROM modifiers WHERE id NOT IN (SELECT modifierID FROM modifier_86)";
        $modifierID = mysqli_query(self::$_conn, $sql)->fetch_row()[0];

        $output = $this->_controller->routeCommand("modifier", "add", '[{"modifierID":"'.$modifierID.'", "inventoryItemID":"2", "criticalItem":"1", "quantityUsed":"200", "unitID":"2"}]');

        $sql = "SELECT * FROM modifier_86 WHERE id = {$output[0]['id']}";
        $result = mysqli_query(self::$_conn, $sql)->fetch_all();

        $this->assertTrue(count($result) > 0);
    }

    public function test_update_modifier_should_update_specific_modifier_86_record()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("modifier", "update", '[{"quantityUsed":"200", "id":"1"}]');

        $sql = "SELECT quantityUsed FROM modifier_86 WHERE id = 1";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals("200", $result[0]);
        $this->assertEquals("Items updated", $output["SUCCESS"]);
    }

    public function test_reserve_should_add_to_the_modifier_reserved_and_return_success()
    {
        $this->setUpClean();
        $sql = "SELECT reservation, modifierID FROM modifier_86 WHERE id = 1";
        $return = mysqli_query(self::$_conn, $sql)->fetch_row();
        $modifierID = $return[1];
        $reservation = $return[0];

        $output = $this->_controller->routeCommand("modifier","reserve",'[{"modifierID":"'.$modifierID.'"}]');

        $return = mysqli_query(self::$_conn, $sql)->fetch_row();
        $reservationNew = $return[0];

        $this->assertNotEquals($reservation, $reservationNew);
        $this->assertTrue($reservationNew > $reservation);
        $this->assertEquals("Items updated", $output['SUCCESS']);
    }

    public function test_close_modifier_should_decrease_reservation_and_quantity_in_inventory()
    {
        $this->setUpClean();
        $sql = "SELECT modifierID, inventoryItemID, reservation FROM modifier_86 WHERE reservation > 0 ORDER BY reservation DESC";
        $modifierItem = mysqli_query(self::$_conn, $sql)->fetch_row();
        $sql = "SELECT quantity FROM inventory_items WHERE id = {$modifierItem[1]}";
        $quantity = mysqli_query(self::$_conn, $sql)->fetch_row()[0];

        $output = $this->_controller->routeCommand("modifier", "close", '[{"modifierID":"'.$modifierItem[0].'"}]');

        $sql = "SELECT reservation FROM modifier_86 WHERE modifierID = ". $modifierItem[0];
        $modifierItemNew = mysqli_query(self::$_conn, $sql)->fetch_row();
        $sql = "SELECT quantity FROM inventory_items WHERE id = {$modifierItem[1]}";
        $quantityNew = mysqli_query(self::$_conn, $sql)->fetch_row()[0];

        $this->assertTrue($modifierItem[2] > $modifierItemNew[0]);
        $this->assertTrue($quantity > $quantityNew);
        $this->assertTrue(isset($output['SUCCESS']));
        $this->assertEquals("Items updated", $output['SUCCESS']);
    }

    public function test_cancel_modifier_should_decrease_reservation()
    {
        $this->setUpClean();
        $sql = "SELECT modifierID, reservation, id FROM modifier_86 WHERE reservation != 0 ORDER BY reservation DESC";
        $modifier = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("modifier", "cancel", '[{"modifierID":"'.$modifier[0].'"}]');

        $sql = "SELECT reservation FROM modifier_86 WHERE id = {$modifier[2]}";
        $modifierNew = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals("Items updated",$output['SUCCESS']);
        $this->assertTrue($modifier[1] > $modifierNew[0]);
    }

    public function test_get_forced_modifier_should_return_all_forced_modifiers()
    {
        $this->setUpClean();
        $sql = "SELECT count(*) FROM forced_modifier_86 WHERE _deleted = 0";
        $count = mysqli_query(self::$_conn, $sql)->fetch_row()[0];

        $output = $this->_controller->routeCommand("forcedmodifier", 'get');

        $this->assertEquals($count, count($output));
    }

    public function test_out_of_stock_forced_modifier_should_return_all_records_that_meet_out_of_stock_requirements()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("forcedmodifier", "outofstock");

        $this->assertTrue(count($output) > 0);
    }

    public function test_low_stock_forced_modifier_should_return_all_records_that_are_now_in_quantity()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("forcedmodifier", "lowstock");

        $this->assertTrue(count($output) > 0);
    }

    public function test_low_stock_forced_modifier_should_return_error_when_there_is_a_error_with_query()
    {
        $this->setUpWithMock();
        $output = $this->_controller->routeCommand("forcedmodifier", "lowstock");

        $this->assertEquals("Error getting low stock Forced Modifiers", $output["ERROR"]);
    }

    public function test_add_forced_modifier_should_add_new_record_in_forced_modifier_86_and_return_new_record()
    {
        $this->setUpClean();
        $sql = "SELECT id FROM forced_modifiers WHERE id NOT IN (SELECT forced_modifierID FROM forced_modifier_86)";
        $modifierID = mysqli_query(self::$_conn, $sql)->fetch_row()[0];

        $output = $this->_controller->routeCommand("forcedmodifier", "add", '[{"forced_modifierID":"'.$modifierID.'", "inventoryItemID":"2", "criticalItem":"1", "quantityUsed":"200", "unitID":"2"}]');

        $sql = "SELECT * FROM forced_modifier_86 WHERE id = {$output[0]['id']}";
        $result = mysqli_query(self::$_conn, $sql)->fetch_all();

        $this->assertTrue(count($result) > 0);
    }

    public function test_update_forced_modifier_should_update_specific_forced_modifier_86_record()
    {
        $this->setUpClean();
        $output = $this->_controller->routeCommand("forcedmodifier", "update", '[{"quantityUsed":"200", "id":"1"}]');

        $sql = "SELECT quantityUsed FROM forced_modifier_86 WHERE id = 1";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals("200", $result[0]);
        $this->assertEquals("Items updated", $output["SUCCESS"]);
    }

    public function test_update_forced_modifier_should_return_error_if_query_is_not_correct()
    {
        $this->setUpWithMock();
        $output = $this->_controller->routeCommand("forcedmodifier", "update", '[{"quantityUsed":"200", "id":"1"}]');

        $this->assertEquals("SQL ERROR", $output["ERROR"]);
    }

    public function test_reserve_forced_modifier_should_add_to_the_forced_modifier_reserved_and_return_success()
    {
        $this->setUpClean();
        $sql = "SELECT reservation, forced_modifierID FROM forced_modifier_86 WHERE id = 1";
        $return = mysqli_query(self::$_conn, $sql)->fetch_row();
        $modifierID = $return[1];
        $reservation = $return[0];

        $output = $this->_controller->routeCommand("forcedmodifier","reserve",'[{"forced_modifierID":"'.$modifierID.'"}]');

        $return = mysqli_query(self::$_conn, $sql)->fetch_row();
        $reservationNew = $return[0];

        $this->assertNotEquals($reservation, $reservationNew);
        $this->assertTrue($reservationNew > $reservation);
        $this->assertEquals("Items updated", $output['SUCCESS']);
    }

    public function test_close_forced_modifier_should_decrease_reservation_and_quantity_in_inventory()
    {
        $this->setUpClean();
        $sql = "SELECT forced_modifierID, inventoryItemID, reservation FROM forced_modifier_86 WHERE reservation > 0 ORDER BY reservation DESC";
        $modifierItem = mysqli_query(self::$_conn, $sql)->fetch_row();
        $sql = "SELECT quantity FROM inventory_items WHERE id = {$modifierItem[1]}";
        $quantity = mysqli_query(self::$_conn, $sql)->fetch_row()[0];

        $output = $this->_controller->routeCommand("forcedmodifier", "close", '[{"forced_modifierID":"'.$modifierItem[0].'"}]');

        $sql = "SELECT reservation FROM forced_modifier_86 WHERE forced_modifierID = ". $modifierItem[0];
        $modifierItemNew = mysqli_query(self::$_conn, $sql)->fetch_row();
        $sql = "SELECT quantity FROM inventory_items WHERE id = {$modifierItem[1]}";
        $quantityNew = mysqli_query(self::$_conn, $sql)->fetch_row()[0];

        $this->assertTrue($modifierItem[2] > $modifierItemNew[0]);
        $this->assertTrue($quantity > $quantityNew);
        $this->assertTrue(isset($output['SUCCESS']));
        $this->assertEquals("Items updated", $output['SUCCESS']);
    }

    public function test_cancel_forced_modifier_should_decrease_reservation()
    {
        $this->setUpClean();
        $sql = "SELECT forced_modifierID, reservation, id FROM forced_modifier_86 WHERE reservation != 0 ORDER BY reservation DESC";
        $modifier = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("forcedmodifier", "cancel", '[{"forced_modifierID":"'.$modifier[0].'"}]');

        $sql = "SELECT reservation FROM forced_modifier_86 WHERE id = {$modifier[2]}";
        $modifierNew = mysqli_query(self::$_conn, $sql)->fetch_row();

        $this->assertEquals("Items updated",$output['SUCCESS']);
        $this->assertTrue($modifier[1] > $modifierNew[0]);
    }

}
