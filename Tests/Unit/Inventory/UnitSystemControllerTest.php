<?php

/**
 * Created For Lavu
 * User: Ruben
 * Date: 1/26/16
 */

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class UnitSystemControllerTest extends PHPUnit_Framework_TestCase
{
    public static $_liquidId = -1;
    private $_controller;

    public static function setUpBeforeClass()
    {
        global $dataname;
        $dataname = "ll-lavudemochr";

        global $loc_id;
        $loc_id = 1;

        require_once (__DIR__ . "/Fixture/SetupDefaultDB.php");
        self::$_liquidId = getLiquidId(getConnection());

        require_once(dirname(__FILE__) . '/../../../prod/public_html/admin/components/API/Inventory/Controllers/UnitSystemController.php');
    }

    public function setUp()
    {
        $this->_controller = new UnitSystemController();
    }

    public function test_addCategory_should_add_category_to_the_database_and_return_the_new_added_record()
    {
        $output = $this->_controller->routeCommand("category", "add", "[{\"name\": \"NewCategory\"}]");
        $this->assertEquals("NewCategory", $output[0]['name']);
        $this->assertTrue(isset($output[0]['id']));
    }

    public function test_getUnitCategory_should_return_all_category_records()
    {
        $output = $this->_controller->routeCommand("category", "get");
        $this->assertTrue(count($output) >= 4);
    }

    public function test_addUnit_should_add_unit_to_the_database_and_return_record()
    {
        $output = $this->_controller->routeCommand("unit", "add", "[{\"name\":\"Cup\", \"symbol\":\"c\", \"categoryID\":\"". self::$_liquidId . "\", \"conversion\":\"" . 1/8 . "\"}]");
        $this->assertEquals("Cup", $output[0]['name']);
        $this->assertTrue(isset($output[0]['id']));
    }

    public function test_getUnit_with_NO_json_should_return_records_of_all_units()
    {
        $output = $this->_controller->routeCommand("unit", "get");
        $this->assertTrue(count($output) >= 4);
    }

    public function test_getUnit_with_json_should_return_specific_records()
    {
        $output = $this->_controller->routeCommand("unit", "get", '[{"id":"2"},{"id":"4"}]');
        $this->assertEquals("Each", $output[0][0]['name']);
        $this->assertEquals("Ounce", $output[1][0]['name']);
    }

    public function test_controller_should_return_error_if_method_and_or_object_is_not_provided()
    {
        $expectedErrorString = "Unit System: No valid Command";
        $output = $this->_controller->routeCommand();
        $this->assertEquals($expectedErrorString, $output["ERROR"]);
        $output = $this->_controller->routeCommand("unit");
        $this->assertEquals($expectedErrorString, $output["ERROR"]);
    }

    public function test_controller_should_return_error_if_object_is_not_supported()
    {
        $expectedErrorString = "Unit System: No valid object";
        $output = $this->_controller->routeCommand("thunder", "get");
        $this->assertEquals($expectedErrorString, $output["ERROR"]);
    }

    public function test_controller_should_return_error_if_method_in_unit_is_not_supported()
    {
        $expectedErrorString = "Unit System - unit: Invalid command";
        $output = $this->_controller->routeCommand("unit", "put");
        $this->assertEquals($expectedErrorString, $output["ERROR"]);
    }

    public function test_controller_should_return_error_if_method_in_category_is_not_supported()
    {
        $expectedErrorString = "Unit System - category: Invalid command";
        $output = $this->_controller->routeCommand("category", "delete");
        $this->assertEquals($expectedErrorString, $output["ERROR"]);
    }

    public function test_addCategory_should_return_error_if_category_json_name_for_add_is_not_provided()
    {
        $expectedErrorString = "Unit category add must have a name";
        $output = $this->_controller->routeCommand("category", "add", "[{\"id\": \"1\"}]");
        $this->assertEquals($expectedErrorString, $output["ERROR"]);
    }

    public function test_addUnit_should_return_error_if_one_of_the_required_fields_is_not_provided()
    {
        $expectedErrorString = "One of the required fields for Unit add is not set";
        $output = $this->_controller->routeCommand("unit", "add", "[{\"name\":\"BigCup\", \"icon\":\"c\", \"categoryID\":\"". self::$_liquidId . "\", \"conversion\":\"" . 1/8 . "\"}]");
        $this->assertEquals($expectedErrorString, $output["ERROR"]);
    }

    public function test_addUnit_should_return_error_if_json_is_not_provided()
    {
        $expectedErrorString = "No Units Added, JSON Error";
        $output = $this->_controller->routeCommand("unit", "add", "");
        $this->assertEquals($expectedErrorString, $output["ERROR"]);
    }
}
