<?php

/**
 * Created For Lavu
 * User: Ruben
 * Date: 2/1/16
 */

/**
 * @backupGlobals disabled
 * @backupStaticAttributes disabled
 */
class MenuControllerTest extends PHPUnit_Framework_TestCase
{

    private static $_conn;
    private $_controller;

    public static function setUpBeforeClass()
    {
        global $dataname;
        $dataname = "ll-lavudemochr";

        global $loc_id;
        $loc_id = 1;

        require_once (__DIR__ . "/Fixture/SetupDefaultDB.php");
        self::$_conn = getConnection();
        require_once(__DIR__ . '/../../../prod/public_html/admin/components/API/Inventory/Controllers/MenuController.php');
    }

    public function setUp()
    {
        $this->_controller = new MenuController();
    }

    public function test_get_items_should_return_all_items_that_are_part_of_a_item_category()
    {
        $sql = "SELECT COUNT(*) FROM menu_items WHERE _deleted = '0' AND category_id IN ( SELECT id FROM menu_categories WHERE _deleted = 0)";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("menuitem", "get");

        $this->assertEquals($result[0], count($output));
    }

    public function test_get_modifiers_should_return_all_modifiers_that_are_part_of_a_modifier_category()
    {
        $sql = "SELECT COUNT(*) FROM modifiers WHERE _deleted = '0' AND category IN ( SELECT id FROM modifier_categories WHERE _deleted = 0)";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("modifier", "get");

        $this->assertEquals($result[0], count($output));
    }

    public function test_get_item_category_should_return_all_category_that_have_not_been_deleted()
    {
        $sql = "SELECT COUNT(*) FROM menu_categories WHERE _deleted = 0";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("menucategory", "get");

        $this->assertEquals($result[0], count($output));
    }

    public function test_get_modifier_category_should_return_all_category_that_have_not_been_deleted()
    {
        $sql = "SELECT COUNT(*) FROM modifier_categories WHERE _deleted = 0";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("modifiercategory", "get");

        $this->assertEquals($result[0], count($output));
    }

    public function test_get_forced_modifiers_should_return_all_forced_modifiers_that_are_members_of_forced_modifiers_list()
    {
        $sql = "SELECT COUNT(*) FROM forced_modifiers WHERE _deleted='0' AND list_id IN ( SELECT id FROM forced_modifier_lists WHERE _deleted = '0')";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("forcedmodifier", "get");

        $this->assertEquals($result[0], count($output));
    }

    public function test_get_forced_modifier_lists_should_return_all_forced_modifier_lists_that_are_have_not_been_deleted()
    {
        $sql = "SELECT COUNT(*) FROM forced_modifier_lists WHERE _deleted='0'";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("forcedmodifierlist", "get");

        $this->assertEquals($result[0], count($output));
    }

    public function test_get_forced_modifier_groups_should_return_all_forced_modifier_groups_that_are_have_not_been_deleted()
    {
        $sql = "SELECT COUNT(*) FROM forced_modifier_groups WHERE _deleted='0'";
        $result = mysqli_query(self::$_conn, $sql)->fetch_row();

        $output = $this->_controller->routeCommand("forcedmodifiergroup", "get");

        $this->assertEquals($result[0], count($output));
    }

    public function test_controller_returns_error_if_no_object_is_given()
    {
        $output = $this->_controller->routeCommand("", "get");
        $this->assertEquals("No valid Command", $output['ERROR']);
    }

    public function test_controller_returns_error_if_no_method_is_given()
    {
        $output = $this->_controller->routeCommand("forcedmodifiergroup", "");
        $this->assertEquals("No valid Command", $output['ERROR']);
    }

    public function test_controller_returns_error_if_object_is_mistyped()
    {
        $output = $this->_controller->routeCommand("menuitems", "get");
        $this->assertEquals("No valid Command 2", $output['ERROR']);
    }

    public function test_controller_returns_error_if_method_is_mistyped()
    {
        $output = $this->_controller->routeCommand("menuitem", "gets");
        $this->assertEquals("No valid Command 2", $output['ERROR']);

        $output = $this->_controller->routeCommand("modifier", "gets");
        $this->assertEquals("No valid Command 2", $output['ERROR']);

        $output = $this->_controller->routeCommand("menucategory", "gets");
        $this->assertEquals("No valid Command 2", $output['ERROR']);

        $output = $this->_controller->routeCommand("modifiercategory", "gets");
        $this->assertEquals("No valid Command 2", $output['ERROR']);

        $output = $this->_controller->routeCommand("forcedmodifier", "gets");
        $this->assertEquals("No valid Command 2", $output['ERROR']);

        $output = $this->_controller->routeCommand("forcedmodifierlist", "gets");
        $this->assertEquals("No valid Command 2", $output['ERROR']);

        $output = $this->_controller->routeCommand("forcedmodifiergroup", "gets");
        $this->assertEquals("No valid Command 2", $output['ERROR']);
    }



}
