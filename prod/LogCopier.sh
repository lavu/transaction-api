#!/bin/bash
# For details related to Lavu's logging system, refer to http://wiki.lavu/Server_Logs
REMOTESERVER=lavu@gman.lavu.com:
REMOTELOGPATH=./logdumps2/$HOSTNAME
LOCALLOGS=/home/poslavu/logs
rsync -lrtzPW --exclude 'debug*' --size-only -e ssh $LOCALLOGS $REMOTESERVER$REMOTELOGPATH
if [ 0 -eq $? ];
then
        logger "ERROR: Backup of logs to $REMOTESERVER succeeded"
else
        logger "ERROR: Backup of logs to $REMOTESERVER failed"
fi
