<?php
	session_start();
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	$maindb = "poslavu_MAIN_db";
	mlavu_connect_db();
		
	function get_local_load_level() // for the load level
	{
		$lockfname = "/home/poslavu/private_html/loadlevel.txt";
		$fp = fopen($lockfname,"r");
		$lockout_status = fread($fp,filesize($lockfname));
		fclose($fp);
		if($lockout_status!="" && is_numeric($lockout_status))
		{
			return $lockout_status;
		}
		else return "";
	}
	
	function read_vars_from_file($filename)
	{
		$vars = array();
		$fp = fopen($filename,"r");
		$readstr = fread($fp,filesize($filename));
		fclose($fp);
		
		$strparts = explode("&",$readstr);
		for($i=0; $i<count($strparts); $i++)
		{
			$lineparts = explode("=",$strparts[$i]);
			if(count($lineparts) > 1)
			{
				$linekey = urldecode(trim($lineparts[0]));
				$lineval = urldecode(trim($lineparts[1]));
				$vars[$linekey] = $lineval;
			}
		}
		
		return $vars;	
	}
	
	function write_vars_to_file($filename,$vars)
	{
		$fstr = "";
		foreach($vars as $vkey => $vval)
		{
			if($fstr!="") $fstr .= "&";
			$fstr .= urlencode($vkey) . "=" . urlencode($vval);
		}
		
		$fp = fopen($filename,"w");
		fwrite($fp,$fstr);
		fclose($fp);
	}
	
	function write_result_to_file($str)
	{
		$fstr = "";
		$fstr .= "---------------------------------\n";
		$fstr .= "Datetime: " . date("Y-m-d H:i:s") . "\n";
		$fstr .= $str . "\n";
		
		$fp = fopen("/home/poslavu/private_html/tools/data/sig_size_results.txt","a");
		fwrite($fp,$fstr);
		fclose($fp);
	}
	
	$resize_signatures = true;
	$batch_found = false;
	$rcount = 0;
	
	function set_run_status($set_status)
	{
		write_vars_to_file("/home/poslavu/private_html/tools/data/running_sizer.txt",array("running"=>$set_status));
	}
	
	function get_run_status()
	{
		$vars = read_vars_from_file("/home/poslavu/private_html/tools/data/running_sizer.txt");
		if(isset($vars['running']) && $vars['running']=="1") return true;
		else return false;
	}
	
	if(get_run_status())
	{
		echo "Sizer already running\n";
		exit();
	}
	set_run_status("1");
	
	while(!$batch_found)
	{
		$rcount++;
		if($rcount > 100)
		{
			$batch_found = true;
		}
		
		$rstr = "";
		$sig_filter = "";//"oldwest_cafe_%";//(isset($_GET['sig_filter']))?$_GET['sig_filter']:"";
		$start = 0;//(isset($_GET['start']))?$_GET['start']:"0";
		$span = 1;
		
		$rows = 0;
		$cols = 0;
		$maxcols = 6;
		
		$start_sig = 0;//(isset($_GET['start_sig']))?$_GET['start_sig']:"0";
		$sig_span = 250;
		$max_sig_span = 2500;
		$auto_continue_sigs = false;
		
		$position_filename = "/home/poslavu/private_html/tools/data/last_sig_size.txt";
		
		if(is_file($position_filename))
		{
			$rvars = read_vars_from_file($position_filename);
			$start = $rvars['start'];
			$start_sig = $rvars['start_sig'];
			
			if($start=="") $start = 0;
			if($start_sig=="") $start_sig = 0;
			$start_sig = $start_sig * 1;
			$start = $start * 1;
		}
				
		$current_load_level = get_local_load_level();
		
		//echo "Resize signatures:<br><br>";
		$rstr .= "Current load level: " . $current_load_level . "\n";
		
		if($current_load_level > 14)
		{
			$rstr .= "Load level to high to continue\n";
			set_run_status("0");
			
			/*$next_url = "/sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start&start_sig=$start_sig&sig_filter=$sig_filter";
			echo "<script language='javascript'>";
			echo "function continue_tool() { window.location = '$next_url'; } ";
			echo "setTimeout('continue_tool()',6000); ";
			echo "</script>";*/
			exit();
		}
		
		$min_ftime = time() - (60 * 60 * 24 * 90);
		//echo "Min Datetime for Signatures: " . date("m/d/Y",$min_ftime) . "<br><br>";
		
		$sig_r_query = "";
		if($sig_filter!="")
		{
			$sig_r_query = "where `data_name` LIKE '[1]' ";
		}
		
		//echo "select `data_name` from poslavu_MAIN_db.restaurants ".str_replace("[1]",$sig_filter,$sig_r_query)."order by id asc limit $start,$span<br>";
		// where img_disk_space > 20
		$r_query = mlavu_query("select `data_name` from poslavu_MAIN_db.restaurants ".$sig_r_query."order by id asc limit $start,$span", $sig_filter);
		//$r_query = mlavu_query("select `data_name` from poslavu_MAIN_db.restaurants order by id asc limit $start,$span");
		while($r_read = mysqli_fetch_assoc($r_query))
		{
			$dataname = $r_read['data_name'];
			if(trim($dataname)!="")
			{
				$rstr .= $dataname . "\n";
				$dataname_dir = "/mnt/poslavu-files/images/".$dataname."/signatures/";
				
				$ftotal = 0;
				$ftotal_processed = 0;
				$ltotal = 0;
				$ctotal = 0;
				$stotal = 0;
				if(is_dir($dataname_dir))
				{
					$batch_found = true;
					$before_count = $start_sig;
					
					//echo "ssh poslavu@10.182.103.248 mkdir /home/poslavu/deleted_signatures/$dataname<br>";
					//echo exec("ssh poslavu@10.182.103.248 mkdir /home/poslavu/deleted_signatures/$dataname");
					
					//echo "<table><tr>";
					
					$continue = true;
					$dp = opendir($dataname_dir);
					while($continue)
					{
						if($fname = readdir($dp))
						{
							$entire_fname = $dataname_dir . $fname;
							
							if(strpos($fname,".jpg"))
							{
								if($before_count > 0)
								{
									$before_count--;
								}
								else
								{
									//$dest_fname = "/home/poslavu/deleted_signatures/".$dataname."/$fname";
									
									//rename($entire_fname,$dest_fname);
									//echo $entire_fname . " - " . date("m/d/Y",$ftime) ."<br>";
									//exec("scp poslavu@10.182.103.248:$entire_fname $dest_fname");
									
									$img_size = getimagesize($entire_fname);
									if(is_array($img_size) && count($img_size) > 1)
									{
										$ftime = filemtime($entire_fname);
										if($ftime < $min_ftime)
										{
											$set_width = 128;
											$set_height = 24;
										}
										else
										{
											$set_width = 256;
											$set_height = 47;
										}
										
										if($img_size[0] > $set_width)
										{
											$ltotal++;
											$convert_cmd = "convert '$entire_fname' -thumbnail '".$set_width."x".$set_height.">' '$entire_fname'";
											//echo $convert_cmd . "<br>";
											exec($convert_cmd);
											
											$rstr .= "converting $entire_fname (" . $img_size[0] . "x" . $img_size[1]." to " . $set_width . "x" . $set_height . ")\n";
											//echo "<td>".$img_size[0]."x".$img_size[1]."<br><img src='/images/$dataname/signatures/$fname' /></td>";
											$cols++;
											if($cols==$maxcols) 
											{
												$cols = 0;
												//echo "</tr><tr>";
											}
											$ftotal_processed++;
										}
										else 
										{
											$rstr .= "....img already small enough: $set_width x $set_height - $fname (" . date("m/d/Y h:i:s a",$ftime) . ")\n";
											$stotal++;
										}
									}
									else 
									{
										$rstr .= "...cannot determine image size\n";
										$ctotal++;
									}
									$ftotal++;
								}
							}
						}
						else $continue = false;
						
						if($ftotal_processed >= $sig_span || $ftotal >= $max_sig_span) $continue = false;
					}
					
					//echo "</tr></table>";
					
					if($ftotal > 0)
						$auto_continue_sigs = true;
					//if($ltotal > $ftotal / 10 || $ltotal > 0)
					//	$auto_continue_sigs = true;
					$rstr .= "Processed signatures " . ($start_sig) . " - " . ($start_sig + $ftotal) . " for $dataname ($ltotal large - $ftotal checked - $stotal small - $ctotal errors)\n";
					//echo "Signature count: started at $start_sig $ftotal records ($ltotal large)<br>";
				}
				else $rstr .= "No dir\n";
			}
		}

		$start_next_sig = $start_sig * 1 + $ftotal * 1;
		//echo "<br><br><input type='button' value='Next Signatures >>' onclick='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start&start_sig=$start_next_sig&sig_filter=$sig_filter\"' />";
		
		if(isset($ftotal) && $ftotal < 1)
		{
			$start_next = $start * 1 + $span * 1;
			$start_next_sig = 0;
		}
		else $start_next = $start * 1;
		
		write_result_to_file($rstr);
		write_vars_to_file($position_filename,array("start"=>$start_next,"start_sig"=>$start_next_sig));
		set_run_status("0");

		/*echo "<br><br><input type='button' value='Next >>' onclick='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start_next&sig_filter=$sig_filter\"' />";

		if($auto_continue_sigs)
		{
			$next_url = "index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start&start_sig=$start_next_sig&sig_filter=$sig_filter";		
		}
		else if($ftotal < $sig_span - ($sig_span / 4))
		{
			$next_url = "index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start_next&sig_filter=$sig_filter";
		}
		else
		{
			$next_url = "";
		}
		
		if($next_url!="")
		{
			echo "<script language='javascript'>";
			echo "function continue_tool() { window.location = '$next_url'; } ";
			echo "setTimeout('continue_tool()',2000); ";
			echo "</script>";
		}*/
	}
?>