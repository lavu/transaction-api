<?php
	ini_set("memory_limit","256M");
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	
	function verify_dir($dir)
	{
		if(!is_dir($dir))
		{
			mkdir($dir,0755);
		}
		return (is_dir($dir));
	}

	function backup_orders_for_account($dataname,$min_date,$max_date,$filename)
	{
		$dbname = "poslavu_" . $dataname . "_db";
		//lavu_connect_shard_db($dataname);
		
		$record = "";
		$record_header = "";
		$other_headers = array();
		$other_tables = array("order_contents","cc_transactions","split_check_details");
		for($i=0; $i<count($other_tables); $i++)
		{
			$tablename = $other_tables[$i];
			$other_records[$tablename] = "";
			$other_headers[$tablename] = "";
		}		
		
		$order_query = mlavu_query("select * from `[3]`.`orders` where (`opened`>='[1]' and `opened`<='[2]') or (`reopened_datetime`>='[1]' and `reopened_datetime`<='[2]') or (`closed`>='[1]' and `closed`<='[2]') or (`reclosed_datetime`>='[1]' and `reclosed_datetime`<='[2]')",$min_date,$max_date,$dbname);
		while($order_read = mysqli_fetch_assoc($order_query))
		{
			$order_id = $order_read['order_id'];
			
			$record_str = "";
			$record_header_str = "";
			foreach($order_read as $key => $val)
			{
				if($record_str!="") $record_str .= "&";
				$record_str .= urlencode($val);
				if($record_header=="")
				{
					if($record_header_str!="") $record_header_str .= "&";
					$record_header_str .= urlencode($key);
				}
			}
			if($record_str != "")
			{
				$record .= "[order]" . $record_str . "\n";
			}
			if($record_header_str != "")
			{
				$record_header = $record_header_str . "\n";
			}
			
			for($i=0; $i<count($other_tables); $i++)
			{
				$tablename = $other_tables[$i];
				$other_query = mlavu_query("select * from `[1]`.`[2]` where `order_id`='[3]'",$dbname,$tablename,$order_id);
				while($other_read = mysqli_fetch_assoc($other_query))
				{
					$other_str = "";
					$other_header_str = "";
					foreach($other_read as $key => $val)
					{
						if($other_str!="") $other_str .= "&";
						$other_str .= urlencode($val);
						if($other_headers[$tablename]=="")
						{
							if($other_header_str!="") $other_header_str .= "&";
							$other_header_str .= urlencode($key);
						}
					}
					if($other_str != "")
					{
						$record .= "[" . $tablename . "]" . $other_str . "\n";
					}
					if($other_header_str != "")
					{
						$other_headers[$tablename] = $other_header_str .= "\n";
					}
				}
			}
		}
		
		$writestr = "";
		$writestr .= "[order_header]" . $record_header;
		for($i=0; $i<count($other_tables); $i++)
		{
			$tablename = $other_tables[$i];
			if(isset($other_headers[$tablename]) && $other_headers[$tablename] != "") 
			{
				$writestr .= "[" . $tablename . "_header]" . $other_headers[$tablename];
			}
		}
		$writestr .= $record . "\n";
		
		$fp = fopen($filename,"w");
		fwrite($fp,$writestr);
		fclose($fp);
	}
	
	function create_backup_for_account($dataname)
	{
		$dbname = "poslavu_" . $dataname . "_db";
		//lavu_connect_shard_db($dataname);
		
		$table_list = array("locations",
							"tables",
							"users",
							"menu_items",
							"menu_categories",
							"menu_groups",
							"super_groups",
							"forced_modifiers",
							"forced_modifier_lists",
							"forced_modifier_groups",
							"discount_types",
							"discount_groups",
							"modifiers",
							"modifier_categories",
							"payment_types");
		
		$first_char = substr($dataname,0,1);
		$backup_path = dirname(__FILE__) . "/backup_data/" . date("Y-m-d");
		if(verify_dir($backup_path))
		{
			$backup_dn_path = $backup_path . "/" . $first_char;
			if(verify_dir($backup_dn_path))
			{
				$backup_whole_path = $backup_dn_path . "/" . $dataname;
				if(verify_dir($backup_whole_path))
				{
					for($i=0; $i<count($table_list); $i++)
					{
						$tablename = $table_list[$i];
						$dumpfile = $backup_whole_path . "/" . $tablename . ".sql";
						if(!is_file($dumpfile . ".gz"))
						{
							echo "dumping $dbname.$tablename to $dumpfile\n";
							exec("mysqldump -u " . my_poslavu_username() . " -p" . my_poslavu_password() . " -h " . my_poslavu_host() . " $dbname $tablename > $dumpfile");
							if(is_file($dumpfile))
							{
								exec("gzip -f $dumpfile");
							}
						}
					}
					
					$orderfile = $backup_whole_path . "/orders.sql";
					$min_date = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 1,date("Y")));
					$max_date = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") + 1,date("Y")));
					if(!is_file($orderfile . ".gz"))
					{
						backup_orders_for_account($dataname,$min_date,$max_date,$orderfile);
						if(is_file($orderfile))
						{
							exec("gzip -f $orderfile");
						}
					}
				}
			}
		}
	}
	
	if($argc < 2)
	{
		echo "Missing Arguments\n";
		exit();
	}
	$range_start = $argv[1];
	if($argc > 2) $range_end = $argv[2];
	else $range_end = "";
	
	if($range_start == "all")
	{
		$limit_str = "";
		$show_range = "All";
	}
	else
	{
		if(is_numeric($range_start) && is_numeric($range_end) && $range_end!="" && $range_end > 0)
		{
			$limit_str = " limit $range_start,$range_end";
			$show_range = "$range_start - $range_end";
		}
		else
		{
			echo speak("Invalid Range")."\n";
			exit();
		}
	}
	
	echo "Backup started (Range: $show_range)...\n";
	$count = 0;
	$min_activity = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 1,date("Y")));
	$site_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `last_activity` > '[1]' and (`disabled`='0' or `disabled`='') order by id asc" . $limit_str,$min_activity);
	while($site_read = mysqli_fetch_assoc($site_query))
	{
		$dataname = $site_read['data_name'];
		if($dataname!="")
		{
			$count++;
			echo "\n------------------ $count:$dataname --------------------\n";
			create_backup_for_account($dataname);
		}
	}
	echo "Done!\n\n";
?>
