#!/usr/bin/php
//<?php

function cleanTables($conn){
    $sql = "DROP TABLE"
            . " purchaseorder_items,"
            . " vendor_items,"
            . " inventory_purchaseorder,"
            . " vendors,"
            . " inventory_audit,"
            . " menuitem_86,"
            . " modifier_86,"
            . " forced_modifier_86,"
            . " inventory_items,"
            . " inventory_categories,"
            . " inventory_unit,"
            . " unit_category;";
    
    
    if(mysql_query($sql, $conn)=== TRUE){
        echo ("Tables dropped successfully\n");
        return true;
    }
    else{
        echo "Error dropping tables: " . mysql_error() . "\n";
    }      
}

function createInventoryItemsTable($conn){
    $sql = "CREATE TABLE inventory_items ("
            . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
            . "name TEXT NOT NULL,"
            . "categoryID INT(11) NOT NULL,"
            . "unitID INT(11) NOT NULL,"
            . "quantity DOUBLE(14,5) NOT NULL,"
            . "lowQuantity DOUBLE(14,5)  NOT NULL,"
            . "parQuantity DOUBLE(14,5)  NOT NULL,"
            . "loc_id int(11) NOT NULL,"
            . "_deleted INT(1) default '0',"
            . "CONSTRAINT FOREIGN KEY (`categoryID`) REFERENCES `inventory_categories`(`id`) ON UPDATE CASCADE,"
            . "CONSTRAINT FOREIGN KEY (`unitID`) REFERENCES `inventory_unit`(`id`) ON UPDATE CASCADE"            
            . ")ENGINE = InnoDB";
    //echo $sql;
    if(mysql_query($sql, $conn)=== TRUE){
        echo ("Inventory Items Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating inventory_items: " . mysql_error() . "\n";
    }              
}

function createVendorsTable($conn){
    $sql = "CREATE TABLE vendors ("
            . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
            . "name TEXT NOT NULL,"
            . "contactName TEXT,"
            . "contactPhone TEXT,"
            . "contactEmail TEXT,"
            . "loc_id int(11) NOT NULL,"
            . "_deleted INT(1) default '0'"         
            . ")ENGINE = InnoDB";
    
    if(mysql_query($sql, $conn)=== TRUE){
        echo ("Vendors Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating vendors: " . mysql_error() . "\n";
    }   
}

function createVendorItemsTable($conn){
    $sql = "CREATE TABLE vendor_items ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "inventoryItemID INT(11) NOT NULL,"
        . "vendorID INT(11) NOT NULL,"
        . "unitID INT(11) NOT NULL,"
        . "minOrderQuantity INT(11) NOT NULL," 
        . "averageCost DOUBLE(14,2),"            
        . "SKU VARCHAR(255),"
        . "_deleted INT(1) default '0',"
        . "CONSTRAINT FOREIGN KEY (`inventoryItemID`) REFERENCES `inventory_items`(`id`) ON UPDATE CASCADE, "
        . "CONSTRAINT FOREIGN KEY (`vendorID`) REFERENCES `vendors`(`id`) ON UPDATE CASCADE, "
        . "CONSTRAINT FOREIGN KEY (`unitID`) REFERENCES `inventory_unit`(`id`) ON UPDATE CASCADE"         
            . ")ENGINE = InnoDB";
    //echo $sql;
    
    if(mysql_query($sql, $conn)=== TRUE){
        echo ("Vendor Items Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating vendor_items: " . mysql_error() . "\n";
    }    
}

function createInventoryCategoriesTable($conn){
    $sql = "CREATE TABLE inventory_categories ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "name TEXT NOT NULL,"
        . "description TEXT NOT NULL,"
        . "loc_id int(11) NOT NULL,"
        . "_deleted INT(1) default '0'"         
            . ")ENGINE = InnoDB";
    // echo $sql;
    
    if(mysql_query($sql, $conn)=== TRUE){
        echo ("Inventory Category Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating inventory_category: " . mysql_error() . "\n";
    }    
}

function createInventoryPurchaseOrderTable($conn){
    $sql = "CREATE TABLE inventory_purchaseorder ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "vendorID INT(11) NOT NULL,"
        . "order_date TIMESTAMP NOT NULL,"
        . "receive_date TIMESTAMP NULL DEFAULT NULL,"
        . "received INT(1) default '0',"
        . "loc_id int(11) NOT NULL,"
        . "_deleted INT(1) default '0',"
        . "CONSTRAINT FOREIGN KEY (`vendorID`) REFERENCES `vendors`(`id`) ON UPDATE CASCADE"         
            . ")ENGINE = InnoDB";
    //echo $sql;
    
    if(mysql_query($sql, $conn)=== TRUE){
        echo ("Vendor Items Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating vendor_items: " . mysql_error() . "\n";
    }    
}

function createPurchaseOrderItemsTable($conn){
    $sql = "CREATE TABLE purchaseorder_items ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "purchaseOrderID INT(11) NOT NULL,"
        . "vendorItemID INT(11) NOT NULL,"
        . "unitID INT(11) NOT NULL,"
        . "quantityOrdered INT(11) NOT NULL,"
        . "cost DOUBLE(10,2),"
        . "quantityReceived INT(11),"
        . "SKU VARCHAR(255),"
        . "_deleted INT(1) default '0',"
        . "CONSTRAINT FOREIGN KEY (`vendorItemID`) REFERENCES `vendor_items`(`id`) ON UPDATE CASCADE"         
            . ")ENGINE = InnoDB";
    
    //echo $sql;
    if(mysql_query($sql, $conn) === TRUE){
        echo ("Purchase Order Items Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating purchaseorder_items: " . mysql_error() . "\n";
    }    
}
function createInventoryUnitTable($conn){
    $sql = "CREATE TABLE inventory_unit ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "name TEXT NOT NULL,"
        . "symbol VARCHAR(255),"
        . "categoryID INT(11) NOT NULL,"
        . "conversion DOUBLE(14,7),"
        . "_deleted INT(1) default '0',"
        . "CONSTRAINT FOREIGN KEY (`categoryID`) REFERENCES `unit_category`(`id`) ON UPDATE CASCADE"         
            . ")ENGINE = InnoDB";
    
    //echo $sql;
    if(mysql_query($sql, $conn) === TRUE){
        echo ("Inventory Unit Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating inventory_unit: " . mysql_error() . "\n";
    }    
}
function createUnitCategoryTable($conn){
    $sql = "CREATE TABLE unit_category ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "name TEXT NOT NULL,"
        . "_deleted INT(1) default '0'"         
            . ")ENGINE = InnoDB";
    
    //echo $sql;
    if(mysql_query($sql, $conn) === TRUE){
        echo ("Unit Category Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating unit_conversion: " . mysql_error() . "\n";
    }   
}
function create86MenuItemsTable($conn){
    $sql = "CREATE TABLE menuitem_86 ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "menuItemID INT(11),"
        . "inventoryItemID INT(11),"
        . "quantityUsed INT(11) NOT NULL,"
        . "criticalItem INT(1) default '0',"
        . "unitID INT(11) NOT NULL,"
        . "reservation INT(11) default '0',"
        . "_deleted INT(1) default'0',"
        . "CONSTRAINT FOREIGN KEY (`menuItemID`) references `menu_items`(`id`) ON UPDATE CASCADE, "
        . "CONSTRAINT FOREIGN KEY (`inventoryItemID`) references `inventory_items`(`id`) ON UPDATE CASCADE, "  
        . "CONSTRAINT FOREIGN KEY (`unitID`) REFERENCES `inventory_unit`(`id`) ON UPDATE CASCADE"                
            . ")ENGINE = InnoDB";
    
    //echo $sql;
    if(mysql_query($sql, $conn) === TRUE){
        echo ("Menu Item 86 Count Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating menuitem_86: " . mysql_error() . "\n";
    }  
}
function create86ModifiersTable($conn){
    $sql = "CREATE TABLE modifier_86 ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "modifierID INT(11),"
        . "inventoryItemID INT(11),"
        . "quantityUsed INT(11) default '0',"
        . "reservation INT(11) default '0',"
        . "criticalItem INT(1) default '0',"
        . "unitID INT(11) NOT NULL,"
        . "_deleted INT(1) default'0',"
        . "CONSTRAINT FOREIGN KEY (`modifierID`) REFERENCES `modifiers`(`id`) ON UPDATE CASCADE, "
        . "CONSTRAINT FOREIGN KEY (`inventoryItemID`) REFERENCES `inventory_items`(`id`) ON UPDATE CASCADE, "
        . "CONSTRAINT FOREIGN KEY (`unitID`) REFERENCES `inventory_unit`(`id`) ON UPDATE CASCADE"         
            . ")ENGINE = InnoDB";
    
        //echo $sql;
        if(mysql_query($sql, $conn)=== TRUE){
            echo ("Modifier 86 Count Table created successfully\n");
            return true;
        }
        else{
            echo "Error creating modifier_86: " . mysql_error() . "\n";
        }  
    }
    
    function create86ForcedModifiersTable($conn){
    $sql = "CREATE TABLE forced_modifier_86 ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,"
        . "forced_modifierID INT(11),"
        . "inventoryItemID INT(11),"
        . "quantityUsed INT(11) default '0',"
        . "reservation INT(11) default '0',"
        . "criticalItem INT(1) default '0',"
        . "unitID INT(11) NOT NULL,"
        . "_deleted INT(1) default'0',"
        . "CONSTRAINT FOREIGN KEY (`forced_modifierID`) REFERENCES `forced_modifiers`(`id`) ON UPDATE CASCADE, "
        . "CONSTRAINT FOREIGN KEY (`inventoryItemID`) REFERENCES `inventory_items`(`id`) ON UPDATE CASCADE, "
        . "CONSTRAINT FOREIGN KEY (`unitID`) REFERENCES `inventory_unit`(`id`) ON UPDATE CASCADE"         
            . ")ENGINE = InnoDB";
    
    //echo $sql;
    if(mysql_query($sql, $conn)=== TRUE){
        echo ("Forced Modifier 86 Count Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating modifier_86: " . mysql_error() . "\n";
    }  
}
function createInventoryAuditTable($conn){
    $sql = "CREATE TABLE inventory_audit ("
        . "id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY," 
        . "inventoryItemID INT(11) NOT NULL,"
        . "oldCount INT(11) NOT NULL,"
        . "newCount INT(11) NOT NULL,"
        . "userID INT(11) NOT NULL,"
        . "userNotes TEXT NOT NULL,"
        . "loc_id int(11) NOT NULL,"
        . "datetime TIMESTAMP default CURRENT_TIMESTAMP,"
        . "_deleted INT(1) default'0',"
        . "CONSTRAINT FOREIGN KEY (`inventoryItemID`) REFERENCES `inventory_items`(`id`) ON UPDATE CASCADE, "
        . "CONSTRAINT FOREIGN KEY (`userID`) REFERENCES `users`(`id`) ON UPDATE CASCADE"         
            . ")ENGINE = InnoDB";
    
    //echo $sql;
    if(mysql_query($sql, $conn)=== TRUE){
        echo ("Inventory Audit Table created successfully\n");
        return true;
    }
    else{
        echo "Error creating inventory_audit: " . mysql_error() . "\n";
    }  
}

$servername = "localhost";//"demo.lavu.com";
$username = "root";//"lavu_demo";
$password = "password";//"asdf089adsfados4ojg;alkjlv334ljk6kljh346k";
$dbname = "poslavu_ll-lavudemochr_db";

$conn = mysqli_connect($servername, $username, $password);
if(FALSE === $conn){
    die("connection failed: " . mysql_error() . "\n");
}
$dbselected = mysql_select_db($dbname);
if(FALSE === $dbSelected){
    die("Couldn't select DB " . mysql_error() . "\n");
}

    //Drops the tables
    cleanTables($conn);
    //creates the tables
    createVendorsTable($conn);
    createInventoryCategoriesTable($conn);
    createUnitCategoryTable($conn);
    createInventoryUnitTable($conn);
    createInventoryItemsTable($conn);
    createVendorItemsTable($conn);
    createInventoryPurchaseOrderTable($conn);
    createPurchaseOrderItemsTable($conn);
    create86MenuItemsTable($conn);
    create86ModifiersTable($conn);
    create86ForcedModifiersTable($conn);
    createInventoryAuditTable($conn);


mysqli_close($conn);