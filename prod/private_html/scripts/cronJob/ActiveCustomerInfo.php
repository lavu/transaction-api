<?php
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once("/home/poslavu/public_html/inc/billing/zuora/ZuoraIntegration.php");

//To check Replica DB connection.
$obj = new DBConnector();
$replicaConn = $obj->getLink('readreplica');
if (empty($replicaConn)) {
	error_log('Active Customer Report: The replica db is currently unavailable due to maintenance.');
	exit;
}

$oldTimezone = date_default_timezone_get();
$serverTimezone = "America/Denver";
date_default_timezone_set($serverTimezone);
$toDate = date('Y-m-d', strtotime('today - 1 day'));
$fromDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($toDate)). '- 30 days'));
$dateRange = array('fromDate' => $fromDate, 'toDate' => $toDate);

mrpt_query("TRUNCATE TABLE `poslavu_MAIN_db`.`active_customer_details`");

// Get list of existing/non-archived dataname
$existingDatanameList = getExistingDatanames();

$restaurantQuery = "SELECT `data_name`, `created`, `chain_id`, `modules`, `is_new_control_panel_active` AS 'cp', `id` AS 'restId', `last_activity` FROM `poslavu_MAIN_db`.`restaurants` where `disabled` = '0' AND `reseller` = '0' AND `dev` = '0' AND `demo` = '0' AND `test` = '0' AND `email` not like '%lavu.com' AND `last_activity` BETWEEN DATE_FORMAT('[fromDate]', '%Y-%m-%d') AND DATE_FORMAT('[toDate]', '%Y-%m-%d') order by `last_activity` DESC";
$custInfoQuery = mrpt_query($restaurantQuery, $dateRange);

$count = 1;
$customerInfo = array();
$insertKeyList = array();
$insertKeys = '';

if (mysqli_num_rows($custInfoQuery) > 0) {
	while ($customerData = mysqli_fetch_assoc($custInfoQuery)) {
		$restId = $customerData['restId'];
		$dataName = $customerData['data_name'];
		$posDataName = "poslavu_".$dataName."_db";
		#checks for the existence/archived dataname
		if (!in_array($posDataName, $existingDatanameList)) {
			continue;
		}
		$updateCustomerInfo = array();
		$activeCustomerData = array();
		$deviceInfo = getDeviceInfo($restId);
		$locationData = getLocationInfo($dataName);
		$locId = $locationData['locId'];
		$monetarySymbol = $locationData['monitary_symbol'];
		$configDetails = getConfigDetails($dataName);

		//Active customer info.
		$updateCustomerInfo['restaurant_id'] = $restId;
		$updateCustomerInfo['location_id'] = $locId;
		$updateCustomerInfo['data_name'] = $dataName;

		$activeCustomerData['gateway'] = $locationData['gateway'];
		$activeCustomerData['business_country'] = $configDetails['business_country'];
		$activeCustomerData['use_language_pack'] = $locationData['use_language_pack'];
		$activeCustomerData['start_date'] = $customerData['created'];
		$activeCustomerData['default_service_type'] = $locationData['tab_view'];
		$activeCustomerData['active_user_count'] = getUserCount($dataName);
		$activeCustomerData['terminal_count'] = (isset($deviceInfo['terminalCount'])) ? $deviceInfo['terminalCount'] : 0;
		$activeCustomerData['ipod_count'] = (isset($deviceInfo['ipodCount'])) ? $deviceInfo['ipodCount'] : 0;
		$activeCustomerData['premium_kds_features'] = (isset($deviceInfo['premiumKDSfeatures']) && $deviceInfo['premiumKDSfeatures'] > 0) ? 'Yes' : 'No';
		$activeCustomerData['zuora_payment'] = getZuoraPaymentInfo($dataName, $restId, $dateRange);
		$activeCustomerData['average_order_amount'] =  getAvgOrderSale($dataName, $locId, $dateRange, $monetarySymbol);
		$activeCustomerData['cc_sales_ratio'] = getCCOrderSaleRatio($dataName, $locId, $dateRange);
		$activeCustomerData['chain_status'] = ($customerData['chain_id'] != '') ? 'Yes' : 'No';
		$activeCustomerData['cp_statis'] = ($customerData['cp'] == 1) ? 'CP4' : 'Legacy';
		$activeCustomerData['ltg_status'] = $configDetails['use_lavu_togo_2'];
		$activeCustomerData['kiosk_status'] = (isset($deviceInfo['kiosk']) && $deviceInfo['kiosk'] > 0) ? 'Yes' : 'No';
		$activeCustomerData['inventory2_status'] = ($configDetails['inventory_status'] == 'Completed_Ready') ? 'Yes' : 'No';
		$activeCustomerData['inventory1_status'] = ($configDetails['inventory_status'] == 'Legacy') ? 'Yes' : 'No';
		$activeCustomerData['scheduling_status'] = checkUserScheduling($dataName, $locId, $dateRange);
		$activeCustomerData['timecards_status'] = checkTimeCards($dataName, $locId, $dateRange);
		$activeCustomerData['combo_status'] = $configDetails['disable_combo_bulider'];
		$activeCustomerData['pizza_status'] = $configDetails['pizza_mods'];
		$activeCustomerData['lavu_loyalty_status'] = checkModuleStatus($customerData['modules'], 'lavuLoyalty');
		$activeCustomerData['lpl_status'] = $configDetails['pepper_enabled'];
		$activeCustomerData['lavu_gift_status'] = checkModuleStatus($customerData['modules'], 'lavuGift');

		$updateCustomerInfo['data'] = json_encode($activeCustomerData, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
		$updateCustomerInfo['created_date'] = date('Y-m-d H:i:s');

		$customerInfo[] = "('".implode("', '", $updateCustomerInfo)."')";

		if (empty($insertKeyList)) {
			$insertKeyList = array_keys($updateCustomerInfo);
			$insertKeys = "`".implode("`, `", $insertKeyList)."`";
		}
		$count++;
		if ($count > 1000) {
			$insertValues = implode(", ", $customerInfo);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`active_customer_details` ($insertKeys) VALUES $insertValues");
			$customerInfo = array();
			$count = 1;
		}
	}

	if (!empty($customerInfo)) {
		$insertValues = implode(", ", $customerInfo);
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`active_customer_details` ($insertKeys) VALUES $insertValues");
	}
}

/**
 * To get all non-archived datanames.
 *
 * @return array of non-archived dataname
 */
function getExistingDatanames() {
    $schemaList = array();
    $schemaObj = mrpt_query("SELECT SCHEMA_NAME as dataname FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME LIKE 'poslavu%'");
    while($row = mysqli_fetch_assoc($schemaObj)){
        $schemaList[] = $row['dataname'];
    }
    return $schemaList;
}

/**
* This function is used to check module status.
*
* @param string $moduleInfo.
* @param string $moduleName.
*
* @return string $dataUsageReport.
*/
function checkModuleStatus($moduleInfo, $moduleName) {
	$moduleDataList = array(
		'lavuLoyalty' => 'extensions.payment.lavu.+loyalty',
		'lavuGift' => 'extensions.payment.lavu.+gift'
	);
	$moduleStatus = 'No';
	if (strpos($moduleInfo, $moduleDataList[$moduleName]) !== false) {
	    $moduleStatus = 'Yes';
	}

	return $moduleStatus;
}

/**
* This function is used to get details from payment_status table.
*
* @param int $restId.
*
* @return array $deviceInfo.
*/
function getDeviceInfo($restId) {
	$deviceQuery = mrpt_query("SELECT SUM(custom_max_ipads) AS 'terminalCount', SUM(custom_max_ipods) AS 'ipodCount', SUM(custom_max_kds) AS 'premiumKDSfeatures', SUM(custom_max_kiosk) AS 'kiosk' FROM `poslavu_MAIN_db`.`payment_status` WHERE `restaurantid` = '[1]'", $restId);
	$deviceInfo = array();
	if (mysqli_num_rows($deviceQuery) > 0) {
		$deviceInfo = mysqli_fetch_assoc($deviceQuery);
	}
	return $deviceInfo;
}

/**
* This function is used to get active user count .
*
* @param string $dataName.
*
* @return int $userCount.
*/
function getUserCount($dataName) {
	$dataName = 'poslavu_'.$dataName.'_db';
	$userCntQuery = mrpt_query("SELECT count(`id`) AS 'userCount' FROM [1].`users` WHERE `active` = '1' AND `_deleted` = '0' AND `lavu_admin` = '0'", $dataName);
	$userCount = 0;
	if (mysqli_num_rows($userCntQuery) > 0) {
		$userCntInfo = mysqli_fetch_assoc($userCntQuery);
		$userCount = $userCntInfo['userCount'];
	}
	return $userCount;
}

/**
* This function is used to get gateway, language pack, tab view and monetary symbol .
*
* @param string $dataName.
*
* @return array $locationInfo.
*/
function getLocationInfo($dataName) {
	lavu_query("SET NAMES 'utf8'");
	$dataName = 'poslavu_'.$dataName.'_db';
	$locationQuery = mrpt_query("SELECT `id` AS 'locId', `gateway`, `use_language_pack`, `tab_view`, `monitary_symbol` FROM [1].`locations`", $dataName);
	$locationInfo['tab_view'] = 'NULL';
	$locationInfo['gateway'] = 'NULL';
	$locationInfo['use_language_pack'] = 'NULL';
	$locationInfo['monitary_symbol'] = '';
	if (mysqli_num_rows($locationQuery) > 0) {
		$locationInfo = mysqli_fetch_assoc($locationQuery);
		$tabView = 'NULL';
		if ($locationInfo['tab_view'] == 1) {
			$tabView = 'Tabs';
		} else if ($locationInfo['tab_view'] == 2) {
			$tabView = 'Quick Serve';
		} else if ($locationInfo['tab_view'] == 3) {
			$tabView = 'Tables';
		}
		if (!isset($locationInfo['gateway']) || $locationInfo['gateway'] == '') {
			$locationInfo['gateway'] = 'NULL';
		}
		$locationInfo['tab_view'] = $tabView;
		$locationInfo['use_language_pack'] = getLanguagePackName($locationInfo['use_language_pack']);
	}
	return $locationInfo;
}

/**
* This function is used to get language pack name .
*
* @param int $packId.
*
* @return string $languagePackName.
*/
function getLanguagePackName($packId) {
	$languagePackQuery = mrpt_query("SELECT `english_name` FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` = '[1]'", $packId);
	$languagePackName = 'English';
	if (mysqli_num_rows($languagePackQuery) > 0) {
		$languagePackInfo = mysqli_fetch_assoc($languagePackQuery);
		$languagePackName = $languagePackInfo['english_name'];
	}
	return $languagePackName;
}

/**
* This function is used to get ConfigDetails .
*
* @param string $dataName.
*
* @return array $configInfo.
*/
function getConfigDetails($dataName) {
	$configInfo = array();
	$dataName = 'poslavu_'.$dataName.'_db';
	$configQuery = mrpt_query("SELECT `setting`, `value`, `value2` FROM [1].config WHERE `type` = 'location_config_setting' AND `setting` IN('business_country', 'use_lavu_togo_2', 'disable_combo_bulider', 'pepper_enabled')", $dataName);
		$configInfo['business_country'] = 'NULL';
		$configInfo['use_lavu_togo_2'] = 'No';
		$configInfo['disable_combo_bulider'] = 'No';
		$configInfo['pepper_enabled'] = 'No';
	if (mysqli_num_rows($configQuery) > 0) {
		while ($configData = mysqli_fetch_assoc($configQuery)) {
			if ($configData['setting'] == 'business_country') {
				$country = $configData['value2'];
				$bCountry = 'US';
				if ($country == '') {
					$bCountry = 'NULL';
				} else if ($country != 'US') {
					$bCountry = 'Intl';
				}
				$configInfo['business_country'] = $bCountry;
			} else if ($configData['setting'] == 'use_lavu_togo_2') {
				$configInfo['use_lavu_togo_2'] = ($configData['value'] == 1) ? 'Yes':'No';
			} else if ($configData['setting'] == 'disable_combo_bulider') {
				$configInfo['disable_combo_bulider'] = ($configData['value'] == 1) ? 'Yes':'No';
			} else if ($configData['setting'] == 'pepper_enabled') {
				$configInfo['pepper_enabled'] = ($configData['value'] == 1) ? 'Yes':'No';
			}
		}
	}

	//check pizza mode
	$pizzaModeQuery = mrpt_query("SELECT `value` FROM [1].config WHERE `type` = 'setting' AND `setting` = 'pizza_mods'", $dataName);
	$pizzaModes = (mysqli_num_rows($pizzaModeQuery)) ? 'Yes' : 'No';
	$configInfo['pizza_mods'] = $pizzaModes;

	//check inventory status
	$inventoryModeQuery = mrpt_query("SELECT `value` FROM [1].config WHERE `type` = 'inventory_setting' AND `setting` = 'INVENTORY_MIGRATION_STATUS'", $dataName);

	$inventoryStatus = 'No';
	if (mysqli_num_rows($inventoryModeQuery) > 0) {
		$inventoryInfo = mysqli_fetch_assoc($inventoryModeQuery);
		$inventoryStatus = $inventoryInfo['value'];
	}
	$configInfo['inventory_status'] = $inventoryStatus;

	return $configInfo;
}

/**
* This function is used to get user scheduling status .
*
* @param string $dataName.
* @param int $locId.
* @param array $dateRange.
*
* @return string $scheduling Yes/No.
*/
function checkUserScheduling($dataName, $locId, $dateRange) {
	$dataName = 'poslavu_'.$dataName.'_db';
	$toDate =  $dateRange['toDate'];
	$fromDate = $dateRange['fromDate'];
	$schedulingQuery = mrpt_query("SELECT `date` FROM [1].`user_schedules` WHERE `loc_id`= '[2]' AND `date` BETWEEN DATE_FORMAT('[3]', '%m/%d/%y') AND DATE_FORMAT('[4]', '%m/%d/%y')", $dataName, $locId, $fromDate, $toDate);
	$scheduling = (mysqli_num_rows($schedulingQuery)) ? 'Yes' : 'No';
	return $scheduling;
}

/**
* This function is used to get Timecard status .
*
* @param string $dataName.
* @param int $locId.
* @param array $dateRange.
*
* @return string $timeCards Yes/No.
*/
function checkTimeCards($dataName, $locId, $dateRange) {
	$dataName = 'poslavu_'.$dataName.'_db';
	$toDate =  $dateRange['toDate'];
	$fromDate = $dateRange['fromDate'];
	$timecardQuery = mrpt_query("SELECT `time` FROM [1].`clock_punches` WHERE `location_id`= '[2]' AND `time` BETWEEN DATE_FORMAT('[3]', '%Y-%m-%d') AND DATE_FORMAT('[4]', '%Y-%m-%d')", $dataName, $locId, $fromDate, $toDate);
	$timeCards = (mysqli_num_rows($timecardQuery)) ? 'Yes' : 'No';
	return $timeCards;	
}

/**
* This function is used to get Payment details from Zuora API .
*
* @param string $dataName.
* @param int $restId.
* @param array $dateRange.
*
* @return string $saasPayment.
*/
function getZuoraPaymentInfo($dataName, $restId, $dateRange) {
	$saasPayment = 'No Account';
	$signupQuery = mrpt_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `restaurantid`='[1]' OR `dataname`='[2]'", $restId, $dataName);
	if (mysqli_num_rows($signupQuery)) {
		$signupRead = mysqli_fetch_assoc($signupQuery);
		$zAccountId = $signupRead['zAccountId'];
		if ($zAccountId != '') {
			$zInt = new ZuoraIntegration();
			$getAccountInfo = $zInt->getAccount(array('zAccountId' => $zAccountId));
			$currencyCode = $getAccountInfo->Currency;
			$monetarySymbol = getMonetarySymbol($currencyCode);
			$getPaymentList = $zInt->getPaymentsBetweenDates(array('zAccountId' => $zAccountId, 'toDate' => $dateRange['toDate'], 'fromDate' => $dateRange['fromDate']));
			foreach ($getPaymentList as $paymentList) {
				$totalAmount += $paymentList->Amount;
			}
          $saasPayment = $monetarySymbol.sprintf('%0.2f', $totalAmount);
		}
	}

	return $saasPayment;
}

/**
* This function is used to get monetary_symbol with currency code .
*
* @param string $currencyCode.
*
* @return string $monetarySymbol.
*/
function getMonetarySymbol($currencyCode) {
	$currencyQuery = mrpt_query("SELECT `monetary_symbol` FROM `poslavu_MAIN_db`.`country_region` WHERE `alphabetic_code` ='[1]'", $currencyCode);
	$monetarySymbol = '';
	if (mysqli_num_rows($currencyQuery) > 0) {
		$currencyInfo = mysqli_fetch_assoc($currencyQuery);
		$monetarySymbol = $currencyInfo['monetary_symbol'];
	}
	return $monetarySymbol;
}

/**
* This function is used to get Average order sales of last 3 months .
*
* @param string $dataName.
* @param int $locId
* @param array $dataRange
* @param string $monetarySymbol
*
* @return float $avgSale.
*/
function getAvgOrderSale($dataName, $locId, $dateRange, $monetarySymbol) {
	$dataName = 'poslavu_'.$dataName.'_db';
	$toDate = $dateRange['toDate'];
	$fromDate = date('Y-m-d', strtotime(date('Y-m-d', strtotime($toDate)). '- 3 month'));
	$orderSaleQuery = mrpt_query("SELECT if(count(distinct ioid )>count( distinct order_id ), count( distinct ioid ), count( distinct order_id )) AS orderCount, if(count(*) > 0, SUM(total), '0.00') AS total, if(count(*) > 0, SUM(discount + idiscount_amount ), '0.00') AS discount FROM [1].orders WHERE closed BETWEEN DATE_FORMAT('[3]', '%Y-%m-%d') AND DATE_FORMAT('[4]', '%Y-%m-%d') AND void = '0' AND location_id = '[2]'", $dataName, $locId, $fromDate, $toDate);
	$avgSale = 0;

	if (mysqli_num_rows($orderSaleQuery) > 0) {
		$saleData = mysqli_fetch_assoc($orderSaleQuery);
		$avgSale = ($saleData['orderCount']) ? ($saleData['total'] - $saleData['discount']) / $saleData['orderCount'] : 0;
	}
	$avgSale = $monetarySymbol.sprintf('%0.2f', $avgSale);
	return $avgSale;
}

/**
* This function is used to get credit card order sale ratio .
*
* @param string $dataName.
* @param int $locId
* @param array $dataRange
*
* @return float $ccSaleRatio.
*/
function getCCOrderSaleRatio($dataName, $locId, $dateRange) {
	$dataName = 'poslavu_'.$dataName.'_db';
	$toDate = $dateRange['toDate'];
	$fromDate = $dateRange['fromDate'];
	$orderSaleQuery = mrpt_query("SELECT if(count(distinct ioid )>count( distinct order_id ), count( distinct ioid ), count( distinct order_id )) AS orderCount, if(count(*) > 0, SUM(total), '0.00') AS total, if(count(*) > 0, SUM(discount + idiscount_amount ), '0.00') AS discount FROM [1].orders WHERE closed BETWEEN DATE_FORMAT('[3]', '%Y-%m-%d') AND DATE_FORMAT('[4]', '%Y-%m-%d') AND void = '0' AND location_id = '[2]'", $dataName, $locId, $fromDate, $toDate);
	if (mysqli_num_rows($orderSaleQuery) > 0) {
		$saleData = mysqli_fetch_assoc($orderSaleQuery);
		$totalCollected = $saleData['total'] - $saleData['discount'];
	}

	$ccOrderSaleQuery = mrpt_query("SELECT  if(count(*) > 0, SUM(amount), '0.00') AS total FROM [1].cc_transactions AS cc LEFT JOIN [1].`payment_types` AS pt ON cc.`pay_type_id` = pt.`id` WHERE cc.`datetime` BETWEEN DATE_FORMAT('[3]', '%Y-%m-%d') AND DATE_FORMAT('[4]', '%Y-%m-%d') AND cc.`loc_id` = '[2]' AND cc.`pay_type_id` = '2' OR pt.`special` = 'native_extension:paypal' OR pt.`special` = 'payment_extension:82' AND cc.`action` != 'Void'  AND cc.`action` != 'Refund' AND cc.`voided` = '0'", $dataName, $locId, $fromDate, $toDate);

	if (mysqli_num_rows($ccOrderSaleQuery) > 0) {
		$ccSaleData = mysqli_fetch_assoc($ccOrderSaleQuery);
		$totalCardPayment = $ccSaleData['total'];
	}
    $ccSaleRatioInfo = ($totalCollected) ? ($totalCardPayment / $totalCollected)*100 : 0;
    $ccSaleRatio = sprintf('%0.2f', $ccSaleRatioInfo);
	return $ccSaleRatio;
}

?>