<?php
/**
TODO:
lavu_query need to replace by mlavu_query and also need to remove hardcoded dataname used in query statement. There are tables need to create in main db and since for QA cannot update prod main db for new tables hence created those tables under location database poslavu_demo_shuja_ind_db. Post QA is done and prior to prod deployment, will replace below lavu_query with mlavu_query
*/
set_time_limit(900);
ignore_user_abort (true);
require_once ("/home/poslavu/public_html/admin/lib/gateway_functions.php");
require_once ("/home/poslavu/public_html/admin/lib/square/squareReconciliation.php");

/*
 Use of Parameter
 -d = data name
 -i = id
 -n = job count number
 */
$options = getopt("d:i:n:");
$data_name = $options['d'];
$id = $options['i'];
$number = $options['n'];
$jobcount = ($number) ? $number : 10;
class SquareReconciliate {
	private static $locationdb;
	/*
	*	get records and starts the process reconciliation
	*/
	static function doProcess($jobcount, $id = '', $data_name = '')
	{
        $SquareReconcile = new SquareReconcile();

		$customWhereClause = '';
		if ($data_name != '') {
			$customWhereClause = ' AND data_name = "' . $data_name . '"';
		}
		if ($id != '') {
			$customWhereClause .= ' AND id = ' . $id;
		}
		$queryResult = mlavu_query("SELECT id,`data_name`,`start_date`,`end_date`, `transaction_id`, `location_id`, `user_id`, `transaction_type` FROM gateway_sync_request WHERE gateway='square' AND status='pending'" . $customWhereClause . " ORDER BY id ASC limit $jobcount");
		if(mysqli_num_rows($queryResult) > 0)
		{
			while ( $record = mysqli_fetch_assoc ( $queryResult ) ) {
                $SquareReconcile->updateRecordStatus('inprogress', $record['id']);
				self::processReconciliate($record);
			}
		}
		else
		{
			error_log("No records to run");
		}
	}
	/*
	 * it takes the record and process reconciliation request and get response
	 */
	private static function processReconciliate($arr){

		//error_log("tester");
		$SquareReconcile = new SquareReconcile();
		$response = $SquareReconcile->processReconcile($arr);
		if($response == 'done')
		{
            $SquareReconcile->updateRecordStatus('done', $arr['id']);
            if($arr['transaction_type'] == 'deposit')
            {
            	$SquareReconcile->updateDepositStatus($arr);
            }
            
		} else if ($response == 'pending') {
			$SquareReconcile->updateRecordStatus('pending', $arr['id']);
		}
		else
		{
            $SquareReconcile->updateRecordStatus('fail', $arr['id']);
		}
	}

}

SquareReconciliate::doProcess($jobcount, $id, $data_name);
