<?php
/**
 * This script is used to update cc_transaction.card_type of those records which has card_type is unknown for Paypal transaction.
 */
ini_set("display_errors", 0);
error_reporting(0);
//Just voiding notice on PayPalCore.php 
$_SERVER['HTTP_HOST'] = $_SERVER['INGRESS_HOSTNAMES'];
require_once(dirname(__FILE__) . "/../../public_html/admin/cp/resources/core_functions.php");
require_once(dirname(__FILE__) . "/../../public_html/admin/components/payment_extensions/PayPal/PayPalPaymentHandler.php");
$options = getopt("d:t:");
$dataName = '';
// If we hit PayPal api continuously with same access token, They blocked and throwing error like "Too many requests.
// Blocked due to rate limiting.". I checked if sleep time is less than 3 then throwing same error. So kept default 3
// and make it parameter input. If we get same error then we can increase sleep time from 3 to 5 from Parameter.
$time = 3;
$dataNames = array();
if (isset($options['d'])) {
    $dataName = $options['d'];
}
if (isset($options['t'])) {
    $time = $options['t'];
}
if ($dataName) {
    $dataNames = explode(',', $dataName);
} else {
    $schemaQuery = "SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT IN (  'information_schema', 
                                                                                                                            'mysql', 
                                                                                                                            'performance_schema', 
                                                                                                                            'lavutogo',
                                                                                                                            'test',
                                                                                                                            'poslavu_MAINREST_db', 
                                                                                                                            'poslavu_MAIN_db')";
    $schemaResult = mlavu_query($schemaQuery);
    if (mysqli_num_rows($schemaResult) > 0) {
        while ($dataBaseRecord = mysqli_fetch_row($schemaResult)){
            $dataNames[] = trim(trim($dataBaseRecord[0], '_db'), 'poslavu_');
        }
    }
}

$filename = "paypalUnknownCardType-" . date('Y-m-d-H-i-s') . ".txt";
$file = '/home/poslavu/private_html/scripts/data/' . $filename ;
if (!empty($dataNames)) {
    $coreObj = new PayPalCore();
    foreach ($dataNames as $dataname) {
        $logStr = '';
        $dataname = trim($dataname);
        echo $echoMessage = 'Getting records for "' . $dataname . "\"\n";
        $logStr = $echoMessage;
        $table = '`poslavu_'.$dataname.'_db`.`cc_transactions`';
        $query = "SELECT `id`, `record_no` FROM " . $table . " WHERE `card_type` = 'unknown' AND `gateway` = 'PayPal' ORDER BY ID DESC";
        $result = mlavu_query($query);
        if (!$result) {
            $logStr .= 'Database Query Error ' . mlavu_dberror() . "\n";
            echo $echoMessage = 'Database Query Error ' . mlavu_dberror() . "\n";
            file_put_contents($file, $echoMessage, FILE_APPEND);
            continue;
        }
        $i = 0;
        $records = mysqli_num_rows($result);
        echo $echoMessage = 'Total records found ' . $records . "\n";
        $logStr .= $echoMessage;
        file_put_contents($file, $logStr, FILE_APPEND);
        if ($records > 0) {
            $payPalPaymentObj = new PayPalPaymentHandler($dataname);
            while($row = mysqli_fetch_assoc($result)){
                $logStr = '';
                $invoiceId = $row['record_no'];
                $id = $row['id'];
                if ($invoiceId == '') {
                    echo $echoMessage = 'Invoice id is empty in lavu database for id ' . $id . "\n";
                    $logStr .= $echoMessage;
                    file_put_contents($file, $logStr, FILE_APPEND);
                    continue;
                }
                sleep($time);
                echo $echoMessage = "\n\nProcessing for invoice id " . $invoiceId;
                file_put_contents($file, $echoMessage, FILE_APPEND);
                $transactionDetails = getPayPalTransactionDetail($dataname, $invoiceId, $payPalPaymentObj, $coreObj);
                $updateResult = 0;
                if (!empty($transactionDetails)) {
                    $echoMessage = "\nGetting response for invoice id " . $invoiceId . '== Response :- ' . json_encode($transactionDetails);
                    file_put_contents($file, $echoMessage, FILE_APPEND);
                    if (isset($transactionDetails['debug_id'])) {
                        echo $echoMessage = "\nThe specified resource does not exist for invoice id " . $invoiceId;
                        file_put_contents($file, $echoMessage, FILE_APPEND);
                        continue;
                    }
                    $cardType = getCardType($transactionDetails);
                    $query = "UPDATE " . $table . " set `card_type` = '" . $cardType . "' WHERE `id` = " . $id;
                    $updateResult = mlavu_query($query);
                }
                if ($updateResult) {
                    echo $echoMessage = "\nCard Type updated for invoice id " . $invoiceId;
                    $logStr .= $echoMessage;
                    $i++;
                }
                file_put_contents($file, $logStr, FILE_APPEND);
            }
        }
        $logStr = $i . " Record(s) updated out of " . $records  . " for " . $dataname . "\n\n";
        echo $logStr;
        file_put_contents($file, $logStr, FILE_APPEND);
    }
}

/**
 * This function is used to get PayPal transaction details
 * @param string $data_name location name
 * @param string $invoice_id transaction invoice id
 * @param object $payPalPaymentObj PayPalPaymentHandler class object
 * @param object $coreObj PayPalCore class object
 *
 * @return array $response transaction details
 */
function getPayPalTransactionDetail($data_name, $invoice_id, $payPalPaymentObj, $coreObj) {
    $url = 'v2/invoicing/invoices/'.$invoice_id;
    $response = $payPalPaymentObj->callPayPalAPI($coreObj->PayPalEndpointURL.$url);
    return json_decode($response, 1);
}

/**
 * This function is used to parse PayPal response and return Card Type
 * @param array $transactionDetails Transaction details
 *
 * @return string $cardType Card Type
 */
function getCardType($transactionDetails) {
    $cardType = 'unknown';
    if (!empty($transactionDetails) && !isset($transactionDetails['debug_id'])) {
        $additionalData = json_decode($transactionDetails['detail']['additional_data'], 1);
        $type = $additionalData['payment']['type'];
        switch ($type) {
            case 'A':
                $cardType = 'Amex';
                break;
            case 'D':
                $cardType = 'DISCOVER';
                break;
            case 'M':
                $cardType = 'MASTERCARD';
                break;
            case 'V':
                $cardType = 'VISA';
                break;
            default:
                $cardType = $cardType;
        }
    }
    return $cardType;
}

