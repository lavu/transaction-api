<?php
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
ini_set("display_errors",0);
error_reporting(0);
#To get Business/Company settings information
function getBusinessInfo($data_name) {
    $configSettings = array(
        'business_country' => '',
        'business_city' => '',
        'business_state' => '',
        'business_address' => '',
        'business_zip' => ''
    );
    $database = "poslavu_".$data_name."_db";

    #checks for the existence of a particular database
    if(!databaseExists($database)){
        return array();
    }
    $query = "select setting,value,value2 from ".$database.".config where `type` = 'location_config_setting' AND `_deleted` = 0 and setting in ('business_address', 'business_city', 'business_state', 'business_zip', 'business_country', 'vertical_selected')";
    
    $queryObj = mlavu_query($query);

    if(!empty($queryObj)){   
        while($configData = mysqli_fetch_assoc($queryObj)){
            $configSettings[$configData['setting']] = $configData['value'];
        }
    }
    #Check all CP prompt form fields should have value 
    foreach($configSettings as $key=>$value){
        if($value == '')
            return array();
    }
    return $prompCheck?array():$configSettings;
}

#Find all active customers or dataname from restaurants
$query = "select id, company_name, data_name from `restaurants` where `data_name` <> '' AND `data_name` <> 'main' AND `disabled` = 0";
$result = mlavu_query($query);

$delimiter = "\t";
$filename = "CP_Prompt_Submitted_" . date('Y-m-d') . ".csv";
$file = fopen('php://memory', 'w');
$csvData = '';
$csvData .= '\n Dataname, Business Name, Business Address, City, State, Zip / Postal Code, Country, Service Styles';

while($row = mysqli_fetch_assoc($result)){
    $businessSettings = getBusinessInfo($row['data_name']);
    if(!empty($businessSettings)){
        $businessSettings['business_name'] = str_replace(","," ", $row['company_name']);
        $businessSettings['business_country'] = $businessSettings['business_country']?$businessSettings['business_country']:"NA";
        
        if(strpos($businessSettings['vertical_selected'], '_/_')) {
            $businessSettings['vertical_selected'] = str_replace("_/_"," ", $businessSettings['vertical_selected']);
        }
        if(strpos($businessSettings['vertical_selected'], '_')) {
            $businessSettings['vertical_selected'] = str_replace("_"," ", $businessSettings['vertical_selected']);
        }
        if(strpos($businessSettings['business_address'], ',')) {
            $businessSettings['business_address'] = str_replace(","," | ", $businessSettings['business_address']);
        }
        $businessSettings['vertical_selected'] = str_replace("|"," | ", $businessSettings['vertical_selected']);

        $csvData .= "\n".$row['data_name'].",".$businessSettings['business_name'].",".$businessSettings['business_address'].",".$businessSettings['business_city'].",". $businessSettings['business_state'].",".$businessSettings['business_zip'].",". $businessSettings['business_country'].",". ucwords($businessSettings['vertical_selected']);
    }
}

$file = '/home/poslavu/private_html/scripts/data/'.$filename ;
file_put_contents($file, $csvData);
?>

