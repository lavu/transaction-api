<?php
/*
    This script will check mysql user and password of given location, If mysql credential does not exist then create mysql user and password
    and generate csv file for those dataname
*/
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once( "/home/poslavu/public_html/inc/usermgr.php");
ini_set("display_errors",0);
error_reporting(0);

$opts = getopt(null, ["datanames:", "from:", "end:"]);
$filename = "ltgInventoryAffectedLocation_" . date('YmdHis') . ".csv";
$csvData = 'Dataname';

$datanames = '';
if (isset($opts['datanames']) && $opts['datanames'] != '') {
    $datanames = implode('", "', explode(',', $opts['datanames']));
}
if (isset($opts['from']) && $opts['from'] != '') {
    $fromCreatedDate = date('Y-m-d', strtotime($opts['from'])) . ' 00:00:00';
}
if (isset($opts['end']) && $opts['end'] != '') {
    $endCreatedDate = date('Y-m-d', strtotime($opts['end'])) . ' 23:59:59';
}
#Find all enabled restaurant
$locationSql = "SELECT id, data_name FROM `restaurants` WHERE `data_name` <> '' AND `data_name` <> 'main' AND `disabled` = 0";
if ($datanames != '') {
    $locationSql .= ' AND data_name IN ("' . $datanames . '")';
}

if ($fromCreatedDate && $endCreatedDate) {
    $locationSql .= ' AND created BETWEEN "' . $fromCreatedDate . '" AND "' . $endCreatedDate . '"';
}
$result = mlavu_query($locationSql);

while($row = mysqli_fetch_assoc($result)) {
    #checks for the existence of a particular dataname
    $restaurantId = $row['id'];
    $dataname = $row['data_name'];
    $userDB = "poslavu_" . $dataname . "_db";
    if (!databaseExists($userDB)) {
        continue;
    }
    echo "\n======== Checking mysql credential for location => " . $dataname . ' ========';
    $dbConnection = true;
    $mainRestTableName = isset($dataname[0]) && ord($dataname[0]) >= ord('a') && ord($dataname[0]) <= ord('z') ? 'rest_'.$dataname[0] : 'rest_OTHER';
    $clientResult = mlavu_query("SELECT concat('usr_pl_', `companyid`) AS user, AES_DECRYPT(`data_access`,'lavupass') AS password FROM `poslavu_MAINREST_db`.`[1]` WHERE `data_name`='[2]'", $mainRestTableName, $dataname);
    // Check mysql user and password, If record does not exist then insert record in `poslavu_MAINREST_db`.`$tablename`.
    // Insert record into `poslavu_MAINREST_db`.`$tablename` and then get user, AES_DECRYPT password. Use this user and password to connect
    // mysql db, If not able to connect then create mysql user with same user and password.
    if (mysqli_num_rows($clientResult) == 0) {
        $restaurantResult = mlavu_query("SELECT `data_access`, `id`, `last_activity`, `jkey`, `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='" . $dataname . "'");
        $restaurantRow = mysqli_fetch_assoc($restaurantResult);
        $mainRestDBQuery = "INSERT INTO `poslavu_MAINREST_db`.`".$mainRestTableName."` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) VALUES ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')";
        mlavu_query($mainRestDBQuery, $restaurantRow);
        $clientResult = mlavu_query("SELECT concat('usr_pl_', `companyid`) AS user, AES_DECRYPT(`data_access`,'lavupass') AS password FROM `poslavu_MAINREST_db`.`[1]` WHERE `data_name`='[2]'", $mainRestTableName, $dataname);
        $clientRow = mysqli_fetch_assoc($clientResult);
        $mysqlUser = $clientRow['user'];
        $mysqlPassword = $clientRow['password'];
        $dbConnection = false;
    } else {
        $clientRow = mysqli_fetch_assoc($clientResult);
        $mysqlUser = $clientRow['user'];
        $mysqlPassword = $clientRow['password'];
        $clientConnection = mysqli_connect(my_poslavu_host(), $mysqlUser, $mysqlPassword);
        if (empty($clientConnection)) {
            $dbConnection = false;
        } else {
            // Close the connection If client connection created
            mysqli_close($clientConnection);
        }
    }
    if (!$dbConnection) {
        grantAccessToTenantDB($userDB, $mysqlUser, $mysqlPassword);
        $csvData .= "\n" . $dataname;
    }
    $msg = ($dbConnection) ? 'Already Exist' : 'Generated';
    echo "\n======== Checked for location =>" . $dataname . ", mysql credential " . $msg . ' ========';
}
mlavu_close_db();
if (!is_dir("/home/poslavu/private_html/scripts/data")) {
    mkdir("/home/poslavu/private_html/scripts/data/", 0775);
}
$file = '/home/poslavu/private_html/scripts/data/'.$filename ;
file_put_contents($file, $csvData);

/**
 * This function is used to create mysql user if not exist and grant prevelege mysql access
 * @param string $userDB mysql database name
 * @param string $mysqlUser mysql user
 * @param string $mysqlPassword mysql password
 * @return boolean true/false
 */
function grantAccessToTenantDB($userDB, $mysqlUser, $mysqlPassword)
{
    if (!db_user_exists($mysqlUser)) {
        create_db_user($mysqlUser, $mysqlPassword);
    }
    return grant_db_user($mysqlUser, $userDB);
}