<?php

require_once("models.php");
require("/home/poslavu/private_html/myinfo.php");


/**
 * Class CtuitIntegration
 */
class CtuitIntegration
{
    public $mConn;
    /**
     * @var SimpleXMLElement
     */
    private $xml;
    private $pos_db_name;
    private $credentials;
    /**
     * @var string FTP Host to upload the final XML.
     */
    private $FTP_HOST = "ctuit.brickftp.com";
    /**
     * @var string
     */
    private $reportDate;
    /**
     * @var SimpleXMLElement
     */
    private $configuration;
    private $salesDetail;
    private $paidInOuts;
    private $deposits;
    private $laborDetail;
    private $discountTypes;
    private $revenueCenters;
    private $mealPeriods;
    private $splitCheckDetailsInfo;
    private $orderModeName;
    /**
     * @var ConfigurationCategories
     */
    private $configuration_categories;

    function __construct()
    {
        date_default_timezone_set("America/Denver");
        $this->mConn = new mysqli(my_poslavu_host(), my_poslavu_username(), my_poslavu_password());
        if ($this->mConn === false) {
            die("Couldn't connect to database host");
        }

        $this->configuration_categories = new ConfigurationCategories();
    }

    /**
     * Makes and return the the generated XML report.
     *
     * @param $args
     * @return string XML string of the whole report.
     */
    public function getXml($args)
    {
        if (empty($args['dataname']) || empty($args['date'])) {
            error_log('Missing required arguments');
            die;
        }
        $this->dataname = $args['dataname'];
        $this->reportDate = $args['date'];
        $this->pos_db_name = 'poslavu_' . $this->dataname . '_db';
        $this->createXmlObj($args);
        $this->getCategories();
        $this->getUsers($args);
        $this->getPaymentTypes($args);
        $this->getDiscountTypes($args);
        $this->getRevenueCenters($args);
        $this->getMealPeriods($args);
        $this->getTaxRates($args);
        $this->getOrders($args, $discountTypes, $splitCheckDetailsInfo);
        $this->getPaidInOuts($args);
        $this->getDeposits($args);
        $this->getLaborDetail($args);
        $this->getScheduleDetail($args);
        return $this->getXmlForXmlObj($this->xml);
    }

    function createXmlObj($args)
    {

        $sql = "SELECT token FROM `poslavu_MAIN_db`.`auth_tokens` where dataname='$this->dataname' and  token_type='api_compeat' and _deleted = 0";
        $result = mysqli_query($this->mConn, $sql);
        $authToken = mysqli_fetch_assoc($result);
        $this->credentials = json_decode($authToken["token"], 1);


        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        if (!$this->credentials) {
            error_log("DESERIALIZATION OF JSON FAILED IN " . __FILE__);
            return;
        }

        $this->xml = new SimpleXMLElement('<PollData/>');
        $this->xml->addAttribute('CompanyID', $this->credentials["api"]["company_id"]);
        $this->xml->addAttribute('LocationID', $this->credentials["api"]["location_id"]);
        $pollDateRaw = strtotime($args['date']);
        $pollDate1 = date("m/d/Y", $pollDateRaw);
        $this->xml->addAttribute('PollDate', $pollDate1);
        $this->configuration = $this->xml->addChild('Configuration');
    }

    private function getCategories()
    {
        $this->_getMenuCategories();
        $this->_getModifierCategories();
        $this->_getForcedModifierList();

        $categories = $this->configuration->addChild("Categories");
        foreach ($this->configuration_categories->getCategories() as $category) {
            if (!$category->getItems()) {
                continue;
            }
            $categoryNode = $categories->addChild("Category");
            $categoryNode->addAttribute("Name", $category->getName());

            $itemsNode = $categoryNode->addChild("Items");
            foreach ($category->getItems() as $category_item) {
                $itemNode = $itemsNode->addChild("Item");
                $itemNode->addAttribute("Number", $category_item->getNumber());
                $itemNode->addAttribute("Name", $category_item->getName());
            }
        }
    }

    /**
     * Get all the Modifier Categories.
     */
    function _getModifierCategories()
    {
        $sql = "SELECT id, title FROM `$this->pos_db_name`.`modifier_categories` where _deleted = 0";
        $result = mysqli_query($this->mConn, $sql);

        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        while ($modifierCategory = mysqli_fetch_assoc($result)) {
            $category_id = $modifierCategory['id'];

            $this->configuration_categories->addCategory(new Category(
                    $category_id,
                    $modifierCategory['title'],
                    $this->_getModifierItems($category_id)
                )
            );
        }
    }

    /**
     * Getting list of Modifier Items.
     *
     * @param $modifierCategoryID int Modifier Category ID.
     * @return CategoryItem[]
     *
     * @author Alireza Savand
     */
    private function _getModifierItems($modifierCategoryID)
    {
        $sql = "SELECT id, cost, title, category FROM `$this->pos_db_name`.`modifiers` WHERE category = $modifierCategoryID and _deleted = 0";
        $result = mysqli_query($this->mConn, $sql);
        /**
         * @var CategoryItem[]
         */
        $items = array();

        if ($result == false) {
            die(mysqli_error($this->mConn));
        }

        while ($modifier = mysqli_fetch_assoc($result)) {
            $items[] = new CategoryItem($modifier['title'], $modifier['id'], $modifier['cost']);
        }

        return $items;
    }

    /**
     * Gets Forced Modifiers List
     *
     * @author Alireza Savand
     */
    private function _getForcedModifierList()
    {
        $sql = "SELECT id, title FROM `$this->pos_db_name`.forced_modifier_lists WHERE _deleted=0";
        $result = mysqli_query($this->mConn, $sql);

        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        while ($modifierCategory = mysqli_fetch_assoc($result)) {
            $category_id = $modifierCategory['id'];

            $this->configuration_categories->addCategory(new Category(
                    $category_id,
                    $modifierCategory['title'],
                    $this->_getForcedModifiers($category_id)
                )
            );
        }
    }

    /**
     * Get Forced Modifiers by the given Modifier List ID.
     *
     * @param $forcedModifierListID int Forced Modifier List ID to get all the
     * @return CategoryItem[]
     */
    private function _getForcedModifiers($forcedModifierListID)
    {
        $sql = "SELECT title, id, cost FROM `$this->pos_db_name`.forced_modifiers WHERE list_id=$forcedModifierListID and _deleted = 0";
        $result = mysqli_query($this->mConn, $sql);
        /**
         * @var CategoryItem[]
         */
        $items = array();

        if ($result == false) {
            die(mysqli_error($this->mConn));
        }

        while ($modifier = mysqli_fetch_assoc($result)) {
            $items[] = new CategoryItem($modifier['title'], $modifier['id'], $modifier['cost'], "forced");
        }

        return $items;
    }

    private function _getMenuCategories()
    {
        $sql = "SELECT id, name FROM `$this->pos_db_name`.`menu_categories` where _deleted = 0";
        $result = mysqli_query($this->mConn, $sql);

        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        while ($menuCategory = mysqli_fetch_assoc($result)) {
            $category_id = $menuCategory['id'];

            $this->configuration_categories->addCategory(new Category(
                    $category_id,
                    $menuCategory['name'],
                    $this->_getMenuItems($category_id)
                )
            );
        }
    }

    /**
     * Getting all the items for the given CategoryID.
     *
     * @param $categoryID int Menu Category to get all the items for.
     * @return CategoryItem[]
     */
    private function _getMenuItems($categoryID)
    {
        $sql = "SELECT id, name, price FROM `$this->pos_db_name`.`menu_items` where category_id = $categoryID";
        $result = mysqli_query($this->mConn, $sql);

        if ($result == false) {
            die(mysqli_error($this->mConn));
        }

        /**
         * @var CategoryItem[]
         */
        $items = array();

        while ($modifier = mysqli_fetch_assoc($result)) {
            $items[] = new CategoryItem($modifier['name'], $modifier['id'], $modifier['price']);
        }

        return $items;
    }

    function getUsers($args)
    {
        $sql = "SELECT id as 'userID', username as 'username', f_name as 'firstName', l_name as 'lastName', email as 'userEmail' FROM `$this->pos_db_name`.`users` WHERE _deleted = 0";
        $result = mysqli_query($this->mConn, $sql);

        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        $employees = $this->configuration->addChild('Employees');
        while ($userInfo = mysqli_fetch_assoc($result)) {
            $employee = $employees->addChild('Employee');
            $employee->addAttribute('Number', $userInfo['userID']);
            $employee->addAttribute('FirstName', $userInfo['firstName']);
            $employee->addAttribute('LastName', $userInfo['lastName']);
            if ($userInfo['userEmail'] != '') {
                $email = $employee->addChild('Email', $userInfo['userEmail']);
            }
        }
    }

    function getPaymentTypes($args)
    {
        $sql = "SELECT id as 'paymentTypeID', type as 'paymentTypeName' FROM `$this->pos_db_name`.`payment_types` WHERE _deleted = 0";
        $result = mysqli_query($this->mConn, $sql);

        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        $tenders = $this->configuration->addChild('Tenders');
        while ($paymentTypeInfo = mysqli_fetch_assoc($result)) {

            switch ($paymentTypeInfo['paymentTypeName']) {
                case 'Cash':
                    $paymentType = 'Cash';
                    break;
                case 'Card':
                    $paymentType = 'Non-cash';
                    break;
                case 'Gift Certificate':
                    $paymentType = 'GiftCard';
                    break;
                case 'Gift Card':
                    $paymentType = 'GiftCard';
                    break;
                default:
                    $paymentType = 'House Account';
            }

            $tender = $tenders->addChild('Tender');
            $tender->addAttribute('Number', $paymentTypeInfo['paymentTypeID']);
            $tender->addAttribute('Name', $paymentTypeInfo['paymentTypeName']);
            $tender->addAttribute('Type', $paymentType);
        }
    }

    function getTaxRates($args)
    {
        $sqlTaxRates = "SELECT id as 'taxRateID', title as 'taxRateName', calc as 'taxRate' from `$this->pos_db_name`.`tax_rates`";
        $result5 = mysqli_query($this->mConn, $sqlTaxRates);
        if ($result5 === false) die(mysqli_error($this->mConn));
        $taxes = $this->configuration->addChild('Taxes');
        while ($taxRateInfo = mysqli_fetch_assoc($result5)) {
            $tax = $taxes->addChild('Tax');
            $tax->addAttribute('Number', $taxRateInfo['taxRateID']);
            $tax->addAttribute('Name', $taxRateInfo['taxRateName']);
            $rate = $tax->addChild('Rate', $taxRateInfo['taxRate']);
        }
    }

    function getDiscountTypes($args)
    {
        $sql = "SELECT id, title, calc FROM `$this->pos_db_name`.`discount_types` WHERE _deleted = 0";
        $result = mysqli_query($this->mConn, $sql);

        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        while ($discountTypeInfo = mysqli_fetch_assoc($result)) {
            $discountTypes[$discountTypeInfo['id']] = $discountTypeInfo['title'];
            $discountCalcs[$discountTypeInfo['id']] = $discountTypeInfo['calc'];
        }

        $this->discountTypes = $discountTypes;
    }

    function getRevenueCenters($args)
    {
        $sqlRevenueCenters = "SELECT * FROM `$this->pos_db_name`.`revenue_centers` WHERE _deleted = 0";
        $resultRC = mysqli_query($this->mConn, $sqlRevenueCenters);
        if ($resultRC === false) die(mysqli_error($this->mConn));
        while ($revenueCenterInfo = mysqli_fetch_assoc($resultRC)) {
            $revenueCenters[$revenueCenterInfo['id']] = $revenueCenterInfo['name'];
        }
        $this->revenueCenters = $revenueCenters;
    }

    function getMealPeriods($args)
    {
        $sqlMealPeriods = "SELECT * FROM `$this->pos_db_name`.`meal_period_types` WHERE _deleted = 0";
        $resultMP = mysqli_query($this->mConn, $sqlMealPeriods);
        if ($resultMP === false) die(mysqli_error($this->mConn));
        while ($mealPeriodInfo = mysqli_fetch_assoc($resultMP)) {
            $mealPeriods[$mealPeriodInfo['id']] = $mealPeriodInfo['name'];
        }
        $this->mealPeriods = $mealPeriods;
    }


    function getOrders($args, $discountTypes, $splitCheckDetailsInfo)
    {
        // <SalesDetail>
        $sqlOrders = "SELECT * FROM `$this->pos_db_name`.`orders` where closed like '{$args['date']}%' and void = 0 and deposit_status = 0 and order_id not like 'Paid%' and order_id not like '777%' and void = 0";
        $result6 = mysqli_query($this->mConn, $sqlOrders);

        if ($result6 === false) {
            die(mysqli_error($this->mConn));
        }

        $this->salesDetail = $this->xml->addChild('SalesDetail');
        $checks = $this->salesDetail->addChild('Checks');
        while ($orderInfo = mysqli_fetch_assoc($result6)) {
            $orderIDRaw = $orderInfo['order_id'];
            $orderID = str_replace("-", "", $orderIDRaw);
            $openedTimeRaw = strtotime($orderInfo['opened']);
            $openedTime = date("m/d/Y H:i:s", $openedTimeRaw);
            $closedTimeRaw = strtotime($orderInfo['closed']);
            $closedTime = date("m/d/Y H:i:s", $closedTimeRaw);
            $sqlSplitCheckDetails = "SELECT * FROM `$this->pos_db_name`.`split_check_details` where order_id = '{$orderInfo['order_id']}'";
            $result7 = mysqli_query($this->mConn, $sqlSplitCheckDetails);

            if ($result7 === false) {
                die(mysqli_error($this->mConn));
            }

            $check = $splitCheckDetailsInfo['check'];
            $check = $checks->addChild('Check');
            $check->addAttribute('Number', $orderInfo['id']);
            $checkCloseTime = $check->addChild('CloseTime', $closedTime);
            if ($orderInfo['meal_period_typeid'] != '') {
                $checkDaypartName = $check->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
            }
            $checkEmployeeNumber = $check->addChild('EmployeeNumber', $orderInfo['server_id']);
            $guestCount = $check->addChild('GuestCount', $orderInfo['guests']);
            $opened = $check->addChild('OpenTime', $openedTime);

            $_orderIDRaw = substr($orderIDRaw, 0, 1);
            switch ($_orderIDRaw) {
                case 1:
                    $orderModeName = $check->addChild('OrderModeName', 'Lavu POS');
                    break;
                case 2:
                    $orderModeName = $check->addChild('OrderModeName', 'Online');
                    break;
                case 7:
                    $orderModeName = $check->addChild('OrderModeName', 'API');
                    break;
                case 8:
                    $orderModeName = $check->addChild('OrderModeName', 'LLS');
                    break;

            }

            $check->addChild('RevenueCenterName', $this->revenueCenters[$orderInfo['revenue_center_id']]);
            $check->addChild('TableName', $orderInfo['tablename']);

            // If there's a discount on the check, the <Comps> node should be added.
            if ($orderInfo['discount'] != '0.00') {
                $comps = $check->addChild('Comps');
                $comp = $comps->addChild('Comp');
                $comp->addAttribute('Amount', $orderInfo['discount']);
                $_compName = $this->discountTypes[$orderInfo['discount_id']];

                // On some check level discount `discount_id` can be blank/empty
                // instead we'll find `discount_type` which is either `p`, `pp` or `d`.
                if (empty($_compName) && $_compName != "") {
                    $_compName = $orderInfo['discount_type'];

                    if ($_compName == 'p' || $_compName = 'pp') {
                        $_compName = 'Percentage';
                    } else if ($_compName == 'd') {
                        $_compName = 'Dollar';
                    }
                }

                if (empty($_compName)) {
                    $_compName = 'Discount';
                }

                $comp->addChild('CompName', $_compName);
                $comp->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
                $comp->addChild('OrderModeName', $this->orderModeName);
                $comp->addChild('RevenueCenterName', $this->revenueCenters[$orderInfo['revenue_center_id']]);
            }
            $this->orderModeName = $orderModeName;
            $this->getOrderContents($args, $orderInfo, $check);

            $customerInfoRaw = $orderInfo['original_id'];
            $customerInfoParts = explode("|o|", $customerInfoRaw);
            $customerInfoJSON = $customerInfoParts[5];
            $customerInfo = json_decode($customerInfoJSON, 1);
            if ($customerInfoRaw != '') {
                $customers = $check->addChild('Customers');
                $customer = $customers->addChild('Customer');
                $customer->addAttribute('FirstName', $customerInfo['f_name']);
                $customer->addAttribute('LastName', $customerInfo['l_name']);
                $customer->addAttribute('Street', $customerInfo['field3']);
                $customer->addAttribute('City', $customerInfo['field4']);
                $customer->addAttribute('StateAbbreviation', $customerInfo['field5']);
                $customer->addAttribute('PostalCode', $customerInfo['field6']);
                $customerCompanyName = $customer->addChild('CompanyName', $customerInfo['field8']);
                $customerEmail = $customer->addChild('Email', $customerInfo['field1']);
                $customerPhone = $customer->addChild('HomePhone', $customerInfo['field2']);
            }
            $this->getPayments($args, $orderInfo, $check);
            $this->getVoids($args, $orderInfo, $check);
            $this->getRefunds($args, $orderInfo, $check);
        }
    }

    /**
     * @param $args array
     * @param $orderInfo array
     * @param $check SimpleXMLElement
     */
    function getOrderContents($args, $orderInfo, &$check)
    {
        $sqlOrderContents = "SELECT * from `$this->pos_db_name`.`order_contents` where order_id = '{$orderInfo['order_id']}' AND void=0";
        $result8 = mysqli_query($this->mConn, $sqlOrderContents);

        if ($result8 === false) {
            die(mysqli_error($this->mConn));
        }

        $this->itemSales = $check->addChild('ItemSales');

        $dayPartName = $check->xpath("DaypartName")[0];
        $revenueCenterName = $check->xpath("RevenueCenterName")[0];

        while ($orderContentsInfo = mysqli_fetch_assoc($result8)) {
            $itemSale = $this->itemSales->addChild('ItemSale');
            $itemSale->addAttribute('Quantity', $orderContentsInfo['quantity']);
            $itemSale->addAttribute('GrossAmount', $orderContentsInfo['subtotal']);
            $itemSale->addChild("DaypartName", $dayPartName);
            $itemSale->addChild("RevenueCenterName", $revenueCenterName);
            if ($mealPeriods[$orderInfo['meal_period_typeid']] != '') {
                $itemSaleDaypartName = $itemSale->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
            }
            $itemSale->addChild('ItemNumber', $orderContentsInfo['item_id']);
            $itemSale->addChild('OrderModeName', $this->orderModeName);

//            if ($revenueCenters[$orderInfo['revenue_center_id']] != '') {
//                $itemSaleRevenueCenterName = $itemSale->addChild('RevenueCenterName', $this->revenueCenters[$orderInfo['revenue_center_id']]);
//            }

            if ($orderContentsInfo['tax_exempt'] == 1) {
                $itemSaleIsTaxable = $itemSale->addChild('IsTaxable', 'False');
            }
            $itemSaleTimeRaw = strtotime($orderContentsInfo['device_time']);
            $itemSaleTime1 = date("m/d/Y H:i:s", $itemSaleTimeRaw);
            $itemSale->addChild('Time', $itemSaleTime1);
            $sqlModifiersUsed = "SELECT * from `$this->pos_db_name`.`modifiers_used` where order_id = '{$orderInfo['order_id']}' and ioid='{$orderContentsInfo['ioid']}' and icid='{$orderContentsInfo['icid']}'";
            $result9 = mysqli_query($this->mConn, $sqlModifiersUsed);

            if ($result9 === false) {
                die(mysqli_error($this->mConn));
            }

            if (mysqli_num_rows($result9) >= 1) {
                $itemSaleModifiers = $itemSale->addChild('Modifiers');
                while ($modifiersUsedInfo = mysqli_fetch_assoc($result9)) {
                    $modifiersItemSales = $itemSaleModifiers->addChild('ItemSale');
                    $modifiersItemSales->addAttribute('Quantity', $modifiersUsedInfo['qty']);
                    $modifiersItemSales->addAttribute('GrossAmount', $modifiersUsedInfo['cost']);
                    $modifiersItemSales->addChild("DaypartName", $dayPartName);
                    $modifiersItemSales->addChild("RevenueCenterName", $revenueCenterName);

//                    if ($mealPeriods[$orderInfo['meal_period_typeid']] != '') {
//                        $modifiersItemSalesDaypartName = $modifiersItemSales->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
//                    }
                    //
                    if ($modifiersUsedInfo['type'] == 'forced') {
                        $itemNumber = "100" . $modifiersUsedInfo['mod_id'];
                    } else {
                        $itemNumber = $modifiersUsedInfo['mod_id'];
                    }
                    $modifiersItemNumber = $modifiersItemSales->addChild('ItemNumber', $itemNumber);
                    // $modifiersType = $modifiersItemSales->addChild('ModifierType', $modifiersUsedInfo['type']);
                    // $modifiersName = $modifiersItemSales->addChild('ModifierName', $modifiersUsedInfo['title']);
                    $modifiersOrderModeName = $modifiersItemSales->addChild('OrderModeName', $this->orderModeName);
//                    if ($revenueCenters[$orderInfo['revenue_center_id']] != '') {
//                        $modifiersRevenueCenterName = $modifiersItemSales->addChild('RevenueCenterName', $this->revenueCenters[$orderInfo['revenue_center_id']]);
//                    }
                }
            }
            $taxRateRaw = $orderContentsInfo['apply_taxrate'];
            $taxRateParts = explode("::", $taxRateRaw);
            $taxRateID = $taxRateParts[0];

            if ($orderContentsInfo['itax_rate'] != '' && $orderContentsInfo['itax_rate'] != '0.000000') {
                $itemSaleInclusiveTaxes = $itemSale->addChild('InclusiveTaxes');
                $itemSaleInclusiveTax = $itemSaleInclusiveTaxes->addChild('InclusiveTax');
                $itemSaleInclusiveTax->addAttribute('Amount', $orderContentsInfo['itax']);
                $itemSaleInclusiveTax->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
                $itemSaleInclusiveTax->addChild('OrderModeName', $this->orderModeName);
                $itemSaleInclusiveTax->addChild('RevenueCenterName', $this->revenueCenters[$orderInfo['revenue_center_id']]);
                $itemSaleInclusiveTax->addChild('TaxNumber', $taxRateID);
            }
            if ($orderContentsInfo['idiscount_amount'] != '') {
                $itemSaleComps = $itemSale->addChild('Comps');
                $itemSaleComp = $itemSaleComps->addChild('Comp');
                $discountAmount = round((double)$orderContentsInfo['idiscount_amount'], 2, PHP_ROUND_HALF_DOWN);

                if ($orderContentsInfo['discount_amount']) {
                    $discountAmount += round((double)$orderContentsInfo['discount_amount'], 2, PHP_ROUND_HALF_DOWN);
                }

                $itemSaleComp->addAttribute('Amount', $discountAmount);
                $itemSaleComp->addChild('CompName', $this->discountTypes[$orderContentsInfo['idiscount_id']]);
                $itemSaleComp->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
                $itemSaleComp->addChild('OrderModeName', $this->orderModeName);
                $itemSaleComp->addChild('RevenueCenterName', $this->revenueCenters[$orderInfo['revenue_center_id']]);

                $notes = $orderContentsInfo['idiscount_info'];
                if (empty($notes) && $notes != "") {
                    $itemSaleComp->addChild('Notes', $notes);
                }

            }
        }
        $exclusiveTaxes = $check->addChild('ExclusiveTaxes');
        $exclusiveTax = $exclusiveTaxes->addChild("ExclusiveTax");

        $exclusiveTax->addAttribute("Amount", $orderInfo["tax"]);
    }

    function getPayments($args, $orderInfo, &$check)
    {
        $this->payments = $check->addChild('Payments');
        $sqlPayments = "SELECT datetime, total_collected, server_id, card_desc, card_desc, pay_type_id, tip_amount, tip_amount FROM `$this->pos_db_name`.`cc_transactions` where order_id = '{$orderInfo['order_id']}' and `action` = 'Sale'";
        $result10 = mysqli_query($this->mConn, $sqlPayments);

        if ($result10 === false) {
            die(mysqli_error($this->mConn));
        }

        while ($paymentInfo = mysqli_fetch_assoc($result10)) {
            $payment = $this->payments->addChild('Payment');
            $paymentTimeRaw = strtotime($paymentInfo['datetime']);
            $paymentTime = date("m/d/Y H:i:s", $paymentTimeRaw);
            $payment->addAttribute('BaseAmount', $paymentInfo['total_collected']);
            if ($mealPeriods[$orderInfo['meal_period_typeid']] != '') {
                $paymentDaypartName = $payment->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
            }
            $paymentEmployeeNumber = $payment->addChild('EmployeeNumber', $paymentInfo['server_id']);
            $paymentOrderModeName = $payment->addChild('OrderModeName', $this->orderModeName);
            if ($paymentInfo['card_desc'] != '') {
                $paymentTenderIdentity = $payment->addChild('TenderIdentity', $paymentInfo['card_desc']);
            }
            $paymentTenderNumber = $payment->addChild('TenderNumber', $paymentInfo['pay_type_id']);
            $paymentTime = $payment->addChild('Time', $paymentTime);
            if ($paymentInfo['tip_amount'] != '') {
                $paymentTip = $payment->addChild('Tip', $paymentInfo['tip_amount']);
            }
        }
    }

    /**
     * Gets the Order/Check Voids.
     * @param $args
     * @param $orderInfo
     * @param $check SimpleXMLElement
     */
    function getVoids($args, $orderInfo, &$check)
    {
        $sql = "SELECT item_id, total_with_tax from `$this->pos_db_name`.`order_contents` where order_id = '{$orderInfo['order_id']}' and void = 1";
        $voidResult = mysqli_query($this->mConn, $sql);

        if ($voidResult === false) {
            die(mysqli_error($this->mConn));
        }

        if (mysqli_num_rows($voidResult) >= 1) {
            $voids = $check->addChild('Voids');
            while ($voidedOrderContentsInfo = mysqli_fetch_assoc($voidResult)) {
                $void = $voids->addChild('Void');
                $itemId = $voidedOrderContentsInfo['item_id'];
                $void->addAttribute('Amount', $voidedOrderContentsInfo['total_with_tax']);
                $void->addAttribute('Quantity', '1');
                $void->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
                $void->addChild('ItemNumber', $itemId);
                $void->addChild('OrderModeName', $this->orderModeName);

                // Getting VoidName
                $voidItemResult = mysqli_query($this->mConn, "SELECT id, name FROM `$this->pos_db_name`.`menu_items` WHERE id=$itemId LIMIT 1");
                if ($voidItemResult === false) {
                    die(mysqli_error($this->mConn));
                }
                $voidItem = mysqli_fetch_assoc($voidItemResult);
                $void->addChild("VoidName", str_replace("&", "&amp;", $voidItem["name"]));
            }
        }
    }

    function getRefunds($args, $orderInfo, &$check)
    {
        $sql = "SELECT total_collected, server_id, total_collected FROM `$this->pos_db_name`.`cc_transactions` where order_id = '{$orderInfo['order_id']}' and `action` = 'Refund'";
        $result15 = mysqli_query($this->mConn, $sql);

        if ($result15 === false) {
            die(mysqli_error($this->mConn));
        }

        if (mysqli_num_rows($result15) >= 1) {
            $refunds = $check->addChild('Refunds');
            while ($refundsInfo = mysqli_fetch_assoc($result15)) {
                $refund = $refunds->addChild('Refund');
                $refund->addAttribute('Amount', $refundsInfo['total_collected']);
                $refund->addAttribute('Quantity', 1);
                $refundDaypartName = $refund->addChild('DaypartName', $this->mealPeriods[$orderInfo['meal_period_typeid']]);
                $refundEmployeeNumber = $refund->addChild('EmployeeNumber', $refundsInfo['server_id']);
                $erfundOrderModeName = $refund->addChild('OrderModeName', $this->orderModeName);
                // $refundItemNumber = $refund->addChild('ItemNumber'); // Since we track refunds on the payment level, we will not have item numbers
                // $refundName = $refund->addChild('RefundName');
                // $refundTaxAmount = $refund->addChild('TaxAmount');
                // $refundTaxNumber = $refund->addChild('TaxNumber');

                // This section was added because CTUIT doesn't account for the refund amount provided in the <Refunds> section
                $itemSale = $this->itemSales->addChild('ItemSale');
                $itemSale->addAttribute('Quantity', '0');
                $itemSale->addAttribute('GrossAmount', '-' . $refundsInfo['total_collected']);
                $itemSaleOrderModeName = $itemSale->addChild('OrderModeName', $this->orderModeName);
            }
        }
    }

    function getPaidInOuts($args)
    {
        $sql = "SELECT order_id, amount, server_id, info, datetime FROM `$this->pos_db_name`.`cc_transactions` where order_id like 'Paid%' and `datetime` like '{$args['date']}%'";
        $result = mysqli_query($this->mConn, $sql);
        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        if (mysqli_num_rows($result) >= 1) {
            $this->paidInOuts = $this->salesDetail->addChild('PaidInOuts');
            while ($paidInOutsInfo = mysqli_fetch_assoc($result)) {
                $paidInOut = $this->paidInOuts->addChild('PaidInOut');
                if ($paidInOutsInfo['order_id'] == 'Paid In') {
                    $paidInOut->addAttribute('Amount', $paidInOutsInfo['amount']);
                }
                if ($paidInOutsInfo['order_id'] == 'Paid Out') {
                    $paidInOut->addAttribute('Amount', '-' . $paidInOutsInfo['amount']);
                }
                $paidInOut->addAttribute('AffectsCash', 'True');
                $paidInOutEmployeeNumber = $paidInOut->addChild('EmployeeNumber', $paidInOutsInfo['server_id']);
                $paidInOutName = $paidInOut->addChild('PaidInOutName', $paidInOutsInfo['info']);
                $paidInOutTimeRaw = strtotime($paidInOutsInfo['datetime']);
                $paidInOutTimeFormatted = date("m/d/Y H:i:s", $paidInOutTimeRaw);
                $paidInOutTime = $paidInOut->addChild('Time', $paidInOutTimeFormatted);
            }
        }
    }

    function getDeposits($args)
    {
        $sql = "SELECT closed, deposit_amount, server_id FROM `$this->pos_db_name`.`orders` WHERE closed like '{$args['date']}%' and void = 0 and deposit_status != 0 and order_id not like 'Paid%' and order_id not like '777%'";
        $result = mysqli_query($this->mConn, $sql);

        if ($result === false) {
            die(mysqli_error($this->mConn));
        }

        if (mysqli_num_rows($result) >= 1) {
            $deposits = $this->salesDetail->addChild('Deposits');
            while ($depositsInfo = mysqli_fetch_assoc($result)) {
                $depositDateRaw = strtotime($depositsInfo['closed']);
                $depositDate = date("m/d/Y", $depositDateRaw);
                $depositTime = date("m/d/Y H:i:s", $depositDateRaw);
                $deposit = $deposits->addChild('Deposit');
                $deposit->addAttribute('DateOfBusiness', $depositDate);
                $deposit->addAttribute('Amount', $depositsInfo['deposit_amount']);
                $depositEmployeeNumber = $deposit->addChild('EmployeeNumber', $depositsInfo['server_id']);
                $depositTime = $deposit->addChild('Time', $depositTime);
            }
        }
    }

    function getLaborDetail($args)
    {
        $sqlEmpClasses = "SELECT id, title from `$this->pos_db_name`.`emp_classes` where _deleted = 0";
        $result12 = mysqli_query($this->mConn, $sqlEmpClasses);
        if ($result12 === false) {
            die(mysqli_error($this->mConn));
        }

        while ($empClassesInfo = mysqli_fetch_assoc($result12)) {
            $empClassArray[$empClassesInfo['id']] = $empClassesInfo['title'];
        }

        $this->empClassArray = $empClassArray;
        $sqlClockPunches = "SELECT * from `$this->pos_db_name`.`clock_punches` where `time_out` between ('{$args['date']}%' - INTERVAL 21 DAY) and '{$args['date']}%' and _deleted = 0 and punched_out = 1 and break = 0";
        $result13 = mysqli_query($this->mConn, $sqlClockPunches);

        if ($result13 === false) {
            die(mysqli_error($this->mConn));
        }
        $shiftNumber = 1;

        if (mysqli_num_rows($result13) >= 1) {
            $laborDetail = $this->xml->addChild('LaborDetail');
            $shifts = $laborDetail->addChild('Shifts');
        }

        $shiftInfo = array();
        while ($clockPunchesInfo = mysqli_fetch_assoc($result13)) {
            $dailyShiftKey = $clockPunchesInfo['server_id'] . $args['date'];
            $shiftInfo[$dailyShiftKey] += 1;
            $shiftNumber = $shiftInfo[$dailyShiftKey];
            $clockInTimeRaw = strtotime($clockPunchesInfo['time']);
            $clockInDate = date("m/d/Y", $clockInTimeRaw);
            $clockInTime = date("m/d/Y H:i:s", $clockInTimeRaw);
            $clockOutTimeRaw = strtotime($clockPunchesInfo['time_out']);
            $clockOutTime = date("m/d/Y H:i:s", $clockOutTimeRaw);
            $shift = $shifts->addChild('Shift');
            $shift->addAttribute('ShiftNumber', $shiftNumber);
            $shift->addAttribute('ClockInDate', $clockInDate);
            $shift->addAttribute('ClockInTime', $clockInTime);
            $shiftEmployeeNumber = $shift->addChild('EmployeeNumber', $clockPunchesInfo['server_id']);
            $shiftClockOutTime = $shift->addChild('ClockOutTime', $clockOutTime);
            if ($empClassArray[$clockPunchesInfo['role_id']] != '') {
                $shiftJobName = $shift->addChild('JobName', $empClassArray[$clockPunchesInfo['role_id']]);
            }
            if ($clockPunchesInfo['payrate'] != '') {
                $shiftPayRate = $shift->addChild('Payrate', $clockPunchesInfo['payrate']);
            }
        }
    }

    function getScheduleDetail($args)
    {
        $sql = "SELECT start_datetime, end_datetime, user_id, data1 from `$this->pos_db_name`.`user_schedules` where start_datetime like '{$args['date']}%'";
        $result14 = mysqli_query($this->mConn, $sql);

        if ($result14 === false) {
            die(mysqli_error($this->mConn));
        }

        if (mysqli_num_rows($result14) >= 1) {
            $scheduleDetail = $this->xml->addChild('ScheduleDetail');
            $scheduledShifts = $scheduleDetail->addChild('ScheduledShifts');
        }
        while ($userSchedulesInfo = mysqli_fetch_assoc($result14)) {
            $startDatetimeRaw = strtotime($userSchedulesInfo['start_datetime']);
            $startDate = date("m/d/Y", $startDatetimeRaw);
            $startTime = date("m/d/Y H:i:s", $startDatetimeRaw);
            $endDatetimeRaw = strtotime($userSchedulesInfo['end_datetime']);
            $endDatetime = date("m/d/Y H:i:s", $endDatetimeRaw);
            $scheduledShift = $scheduledShifts->addChild('ScheduledShift');
            $scheduledShift->addAttribute('ClockInDate', $startDate);
            $scheduledShift->addAttribute('ClockInTime', $startTime);
            $scheduledShift->addAttribute('ClockOutTime', $endDatetime);
            $scheduledShiftEmployeeNumber = $scheduledShift->addChild('EmployeeNumber', $userSchedulesInfo['user_id']);
            if ($userSchedulesInfo['data1'] != '') {
                $scheduledShiftJobName = $scheduledShift->addChild('JobName', $empClassArray[$userSchedulesInfo['data1']]);
            }
        }
    }

    function getXmlForXmlObj($xml)
    {
        // Pretty Print the XML
        $dom = new DOMDocument("1.0");
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $dom->loadXML($xml->asXML());
        return $dom->saveXML();
    }

    /**
     * Send Report File to Compeat FTP Server.
     *
     * This function should be called once the CtuitIntegration instance
     * has been initialized and to be called directly.
     *
     * The first usage of this function is by calling it form the CLI.
     * How to invoke the function has been documented in README.md file.
     *
     * @param $args array Arguments passed from CLI or any external call.
     */
    function sendFTP($args)
    {
        $xmlContent = $this->getXml($args);

        $locationID = $this->credentials["ftp"]["location_id"];
        $ftpUsername = $this->credentials["ftp"]["username"];
        $ftpPassword = $this->credentials["ftp"]["password"];
        $userDirectory = explode("_", $ftpUsername)[0];

        // Compeat cannot process file names with dashes `-`, we're removing
        // the dashes from the report file name that looks
        // like YYYY-MM-DD to YYYYMMDD
        $reportFileName = str_replace("-", "", $locationID . "_$this->reportDate.xml");

        $ftpConnection = ftp_ssl_connect($this->FTP_HOST, 21) or die("Could not connect to FTP Server $this->FTP_HOST");
        ftp_login($ftpConnection, $ftpUsername, $ftpPassword);
        ftp_pasv($ftpConnection, true);

        $stream = fopen('php://memory', 'r+');
        fwrite($stream, $xmlContent);
        rewind($stream);

        ftp_chdir($ftpConnection, "/$userDirectory");
        if (ftp_fput($ftpConnection, $reportFileName, $stream, FTP_ASCII)) {
            echo "File $reportFileName has been uploaded successfully.\n";
        } else {
            echo "There was a problem while uploading $reportFileName\n";
            print_r(error_get_last());
        }

        fclose($stream);
        ftp_close($ftpConnection);
    }

    /**
     * Send Report Result in format of XML to Compeat via their API.
     *
     * The `Import` resource API endpoint documentation can be
     * found at {@see https://api.ctuit.com/API/swagger/ui/index.html#/Import}
     *
     * This function should be called once the CtuitIntegration instance
     * has been initialized and to be called directly.
     *
     * The first usage of this function is by calling it form the CLI.
     * How to invoke the function has been documented in README.md file.
     *
     * @param $args array Arguments passed from CLI or any external call.
     *
     * @throws Exception when sending to the API fails.
     */
    function sendAPI ($args)
    {
        $xml = $this->getXml($args);
        /**
         * @var string Location ID to be used in API endpoint.
         */
        $locationID = $this->credentials["api"]["location_id"];
        /**
         * @var string Company ID to be used in API endpoint.
         */
        $companyID = $this->credentials["api"]["company_id"];
        /**
         * @var string API User to include in the HTTP Header as `X-UserID`.
         */
        $apiUser = $this->credentials["api"]["api_user"];
        /**
         * @var string API Token to include in the HTTP Header as `X-UserAuthToken`.
         */
        $apiToken = $this->credentials["api"]["api_token"];

        $url = "https://api.ctuit.com/api/Import/$locationID/$companyID/$this->reportDate";

        print_r("Sending to Import Endpoint $url\n");

        $ch = curl_init();
        $optArray = array(
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: text/xml",
                "X-UserID: $apiUser",
                "X-UserAuthToken: $apiToken"
            ),
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_RETURNTRANSFER => true,
            CURLINFO_HEADER_OUT => true,
        );
        curl_setopt_array($ch, $optArray);

        /**
         * @var string holds the response after the API call.
         * However a SUCCESS message from `Import` resource, doesn't return
         * any content because it's a HTTP Status Code 204.
         *
         * In case of failure, the data of $response will be used to print
         * for debugging purposes.
         */
        $response = curl_exec($ch);
        /**
         * @var array Holds all the information of the curl object {@see $info}.
         *
         * In case of failure, the data of $info will be used to print
         * for debugging purposes.
         */
        $info = curl_getinfo($ch);

        curl_close($ch);

        if ($info["http_code"] == 204) {
            print_r("SUCCESS: Report sent to Compeat API successfully\n!");
        } else {
            throw new Exception(
                "FAILURE: Could not send the report to Compeat API.\n" .
                "Following HTTP request details may be helpful (ALSO SENSITIVE, SHARE CAREFULLY!!!)\n\n" .
                "PHP CURL INFO:\n" .
                "$info" .
                "\n\n" .
                "Request Response:\n" .
                "$response\n"
            );
        }
    }
}
/**
 * This part of the code will be executed only if the script is being called via CLI.
 *
 * The command will lookup the database for all the data names that have
 * compeat API credentials and will send the generated report for each
 * one of them in sequence.
 */
if (php_sapi_name() == "cli") {
    error_reporting(1);

    if (sizeof($argv) > 2) {
        die("Usage: DATE[OPTIONAL]\n");
    }

    /**
     * @var int Max number of attempts when sending to the API fails.
     */
    $maxAttempts = 2;

    if (isset($argv[1])) {
        $date = $argv[1];
    } else {
        $date = (string)date("Y-m-d", strtotime("yesterday"));
    }

    $mConn = new mysqli(my_poslavu_host(), my_poslavu_username(), my_poslavu_password());
    $sql = "SELECT dataname FROM `poslavu_MAIN_db`.`auth_tokens` WHERE token_type='api_compeat' and _deleted = 0";
    $result = mysqli_query($mConn, $sql);

    if ($result === false) {
        die(mysqli_error($mConn));
    }

    while ($authToken = mysqli_fetch_assoc($result)) {
        /**
         * @var CtuitIntegration
         */
        $ctuit = new CtuitIntegration();
        /**
         * @var string Data name to run the report for.
         */
        $dataName = $authToken["dataname"];
        /**
         * @var array Arguments to generate the report for.
         */
        $args = array(
            'dataname' => $dataName,
            'date' => $date
        );

        print "Data name: $dataName\n";
        print "Date of reporting: $date\n";

        for ($i = 0; $i <= $maxAttempts; $i++) {
            try {
                $ctuit->sendAPI($args);
                break;
            } catch (Exception $e) {
                print "Failed on attempt $i of $maxAttempts to run the report " .
                    "with error: \n\n$e\n";

                if ($i == 2) {
                    echo "Max attempts reached, we are not going to try anymore" .
                        "Date name '$dataName' will not be send now.\n" .
                        "CLI executed with: \n";
                    print_r($argv);
                    echo "\n";
                } else {
                    echo "Trying 1 more time...\n";
                }
            }
        }
    }
}
