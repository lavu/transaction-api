<?php

/**
 * Class CategoryItem
 */
class CategoryItem
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $number;
    /**
     * @var string
     */
    private $cost;

    /**
     * @var string
     */
    private $type;

    /**
     * CategoryItem constructor.
     *
     * @param string $name
     * @param string $number
     * @param string $cost
     * @param string|null $type
     */
    public function __construct($name, $number, $cost, $type = null)
    {
        $this->name = $name;
        $this->number = $number;
        $this->cost = $cost;

        if (!is_null($type)) {
            $this->type = $type;
            $this->number = $this->getLookupIdx();
        }

    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getLookupIdx()
    {
        return  '100' . $this->getNumber();
    }
}

/**
 * Class Category
 */
class Category
{
    /**
     * @var int
     */
    private $id;

    /**
     * Category Name.
     *
     * @var string
     */
    private $name;

    /**
     * Category Items.
     *
     * @var CategoryItem[]
     */
    private $items;

    /**
     * Configuration Categories
     *
     * @var ConfigurationCategories
     */
    private $conf;

    /**
     * Category constructor.
     *
     * @param int $id
     * @param string $name
     * @param CategoryItem[] $items
     */
    public function __construct($id, $name, array $items)
    {
        $this->id = $id;
        $this->name = $name;
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return CategoryItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return ConfigurationCategories
     */
    public function getConf()
    {
        return $this->conf;
    }

    /**
     * @param ConfigurationCategories $conf
     */
    public function setConf($conf)
    {
        $this->conf = $conf;
    }
}

/**
 * Class ConfigurationCategories
 */
class ConfigurationCategories
{
    /**
     * @var Category[]
     */
    private $categories;
    /**
     * @var string[]
     */
    private $category_names;

    /**
     * Return all the Categories.
     *
     * @return Category[]
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Add a category to the Category list.
     *
     * @param Category $category
     */
    public function addCategory(Category $category)
    {
        $category_name = $category->getName();
        if (in_array($category_name, $this->category_names)) {
            $category_name .= ":" . $category->getId();
            $category->setName($category_name);
        }

        $this->category_names[] = $category_name;
        $this->categories[] = $category;
    }
}