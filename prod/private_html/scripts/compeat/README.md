CtuitIntegration
=================

### Running via CLI

The following command will generate the report and upload the the API server.

```bash
# Usage: DATE[OPTIONAL]
$ php ctuitintegration.php 2019-02-13

# Date time is optional and if omitted it will be set to yesterday date
$ php ctuitintegration.php
```

The command will lookup the database for all the data names that have 
compeat API credentials and will send the generated report for each
one of them in sequence.

You can now find the file in the API server of the customer provided.


### API Credentials

The script requires to have the API credentials in the `auth_tokens` table.
Running the following SQL should insert the required credentials.

Note: replace the values with correct information:

```sql
INSERT INTO 
  auth_tokens (dataname,extension_id,token,token_type,_deleted) 
VALUES ("DATA-NAME",NULL,"{\"ftp\":{\"username\":\"FTP-USERNAME\",\"password\":\"FTP-PASS",\"company_id\":\"3106\",\"location_id\":\"0410\"},\"api\":{\"company_id\":32,\"location_id\":1,\"api_user\":API-USER,\"api_token\":\"API-TOKEN\"}}","api_compeat",0)
```

Filling up the API credentials is necessary because the CLI sends data
via the API credentials.

Please keep in mind the structure of the token should be as follow:

```json
{
  "ftp": {},
  "api": {}
}
```

or:

```json
{
  "api": {}
}
```

In anycase, the token should be a JSON data having separate key-value for
`api` and `ftp`.
