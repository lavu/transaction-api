<?php
/**
This script creates csv file under scripts/data folder
and it contains list of active / in-active locations based on
various validations.
*/

//get latest exchange rate for base currency USD from external api
$data=file_get_contents('https://api.exchangeratesapi.io/latest?base=USD');
$currencyRate=json_decode($data, true);

require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
ini_set("display_errors",0);
error_reporting(0);
#This function will check active omnivore customer in CP
function checkCpLogin($data_name,$date) {
    $database = "poslavu_".$data_name."_db";
    $username = str_replace("_",".",$data_name);
    $response = array("first_login"=>"No","user_name_exist"=>"No","user_name"=>"-", "user_login"=>"-","user_first_login_ts"=>"-");
    #check action log including data/username as empty
	//USE LOC_ID
    $actionLogSql1 = mlavu_query("SELECT count(*) as total FROM ".$database.".admin_action_log WHERE  DATE(`server_time`) >='".$date."'  AND `data` NOT LIKE 'POS%' AND detail1 IN ('Access level: 3','Access level: 4')");
    $actionLogData = mysqli_fetch_assoc($actionLogSql1);
    if($actionLogData['total']>0){
        #check action log excluding data/username as empty
        $actionLogSql2 = mlavu_query("SELECT `data` AS username, `user` AS userlogin, server_time as `timestamp` FROM ".$database.".admin_action_log WHERE  DATE(`server_time`) >='".$date."'  AND `data` NOT LIKE 'POS%' AND detail1 IN ('Access level: 3','Access level: 4') AND `data`<>'' ORDER BY server_time LIMIT 0,1");
        $result = mysqli_fetch_assoc($actionLogSql2);
        if(!empty($result)){
            $response['first_login'] = "Yes";
            $response['user_name_exist'] = "Yes";
            $response['user_name'] = $result['username'];
            $response['user_login'] = $result['userlogin'];
            $response['user_first_login_ts'] = $result['timestamp'];
        }
    }
    return $response;
}

function checkPosLogin($dataName, $date) {
    $database = "poslavu_".$dataName."_db";
    $response = array("first_login"=>"No","user_name_exist"=>"No","username"=>"-", "user_first_login_ts"=>"-");
        #check action log excluding data/username as empty
        $actionLogSql2 = mlavu_query("SELECT `user_id`, time as `timestamp` FROM ".$database.".action_log WHERE  DATE(`time`) >='".$date."' AND `action` = 'User Logged In' AND user <>'' ORDER BY time LIMIT 0,1");
        $result = mysqli_fetch_assoc($actionLogSql2);
        if(!empty($result)){
            $response['first_login'] = "Yes";
            $response['user_name_exist'] = "Yes";
            $response['user_first_login_ts'] = $result['timestamp'];
            $response['user_login'] = '';
            $response['user_name'] = '';
            //get user full name
            if($result['user_id']) {
              $queryObj = mlavu_query('SELECT username, CONCAT(f_name, " ", l_name) as full_name FROM '.$database.'.users WHERE id='.$result['user_id']);
              $result = mysqli_fetch_assoc($queryObj);
              if(!empty($result)){
                $response['user_name'] = $result['full_name'];
                $response['user_login'] = $result['username'];
              }
            }

        }

    return $response;
}

#This function will check account status(trial or subscribed) of omnivore customer
function isTrialAccount($dataname){
    $onTrial = 0;
    $result = mlavu_query("SELECT `created`, `license_applied`, `trial_end`, `hosting_taken`, `annual_agreement`, `tos_date`, `proposed_billing_change` FROM `poslavu_MAIN_db`.`restaurants` r LEFT JOIN `poslavu_MAIN_db`.`payment_status` ps on r.`data_name` = ps.`dataname` WHERE `dataname`='".$dataname."'");

	if ($result === false){
        return  $onTrial;
    }

    $acct_info = mysqli_fetch_assoc($result);

    // Fallback to restaurant create date if trial_end date isn't specified
    $date_to_use = empty($acct_info['trial_end']) ? 'created' : 'trial_end';

    // Obtain the date portion of the trial end date
    list($trial_end_date, $trial_end_time) = explode(" ", $acct_info[$date_to_use]);

    // If we're using the restaurant created date, we need to add 14 days to it.
    if ($date_to_use == 'created'){
        $trial_end_date = date( 'Y-m-d', strtotime($trial_end_date . '+14 days') );
    }


    $current_ts = time();
    $trial_end_ts = strtotime($trial_end_date);

    $days_remaining = floor( ($trial_end_ts - $current_ts) / 60 / 60 / 24 ) + 1;

    if($days_remaining > 0)
        return 1;
    else
        return 0;
}

#To check last 30 days transaction
function hasThirtyDaysTransaction($data_name, $locationId) {

    $database = "poslavu_".$data_name."_db";
    $extraCond = ($locationId)?'AND loc_id='.$locationId:'';
    #We are counting 30 days from first transaction
    $query = "SELECT DATE_ADD(DATE(datetime), INTERVAL 30 DAY) AS active_date FROM ".$database.".`cc_transactions` WHERE action='Sale' AND datetime <> '' $extraCond  ORDER BY datetime limit 0,1";
    $queryObj = mlavu_query($query);
    if ($queryObj === false){
        return 0;
    }
    $result = mysqli_fetch_assoc($queryObj);

    if($result['active_date'] != "" && $result['active_date'] <= date("Y-m-d")){
        return 1;
    }else{
        return 0;
    }
}
#To check average transaction of last 7 days.
function checkWeeklyTransaction($data_name, $locationId){
    global $currencyRate;
    $database = "poslavu_".$data_name."_db";
    $startDate = date( 'Y-m-d', strtotime(date("Y-m-d") . '-27 days') );
    $curDate = date( 'Y-m-d' );
    if($locationId){
	$extraCond1='AND location='.$locationId;
    	$extraCond2='AND loc_id='.$locationId;
    }
    //get all currency exchange effective post startDate
    $queryObj = mlavu_query('SELECT value from '.$database.'.config WHERE setting = "primary_currency_code" '.$extraCond1);
    $result = mysqli_fetch_assoc($queryObj);
    $localCurrency = $result['value'];
    /*$baseCurrency = 'USD';
    if ($localCurrency) {
      $baseRate = $currencyRate[rates][$localCurrency];
      $baseCurrency = ($baseRate) ? 'USD':$localCurrency;
    } else {
      $localCurrency = 'USD';
    }*/

    $queryObj = mlavu_query("SELECT  SUM(FORMAT(amount,2)) as amount FROM ".$database.".cc_transactions WHERE DATE(datetime) >= '$startDate' AND action = 'Sale' AND datetime <> '' $extraCond2");
    $totalAmount = 0;

	  $res = mysqli_fetch_assoc($queryObj);
    $totalAmount = $res['amount'];
	  $formatAmount = number_format($totalAmount,2,'.', ',');
    return array('amount'=>$totalAmount, 'format_amount'=>$formatAmount, 'local_currency'=>$localCurrency);

}

/**
 * To fetch api access key and token for locationId=1
 * @param string $dataname, int $locationId with default value 1
 * @return array $result (if data found) else '' (if data not found)
 */
function getLocationApiKeys($dataName, $locationId) {
    $db = "poslavu_".$dataName."_db";
    $where = $locationId ? " WHERE `id`=$locationId" : "";
    $tokenQuery = mlavu_query("select AES_DECRYPT(`api_token`,'lavutokenapikey') as `api_token`, AES_DECRYPT(`api_key`,'lavuapikey') as `api_key` from ".$db.".`locations` $where");
    if(mysqli_num_rows($tokenQuery))
    {
        $result = mysqli_fetch_assoc($tokenQuery);
        return $result;
    }
    return '';
}

/**
 * To read address from sfdc (SalesForce) 
 * @return array of address from csv file
 */
function getSfdcAddress() {
    $addressList = array();
    $file = fopen("data/sfdc_address_list.csv","r");
    $skipHeader = fgetcsv($file);
    while($csvData = fgetcsv($file)){
        #check address should not be empty
        if($csvData[1] != "" && $csvData[2] != "" && $csvData[3] != "" && $csvData[4] != "" && $csvData[5] != ""){
             if(!preg_match('/[a-zA-Z]/',$csvData[1]) || !preg_match('/[a-zA-Z]/',$csvData[2]) || !preg_match('/[a-zA-Z]/',$csvData[3]) || !preg_match('/[a-zA-Z]/',$csvData[4]) || !preg_match('/\d/',$csvData[5])){
                continue;   
            }
	    #Take first address if there are more address in csv for one dataname
            if (!isset($addressList[$csvData[0]])) {
                $addressList[$csvData[0]]=$csvData; 
            }
        }
        
    }
    return $addressList;
}
/**
 * To get all non-archived dataname
 * @return array of non-archived dataname
 */
function getExistingDatanames() {
    $schemaList = array();
    $schemaObj = mlavu_query("SELECT SCHEMA_NAME as dataname FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME LIKE 'poslavu%'");
    while($row = mysqli_fetch_assoc($schemaObj)){
        $schemaList[] = $row['dataname'];
        
    }
    return $schemaList;
}

function compressStr($str){
	$str=strtolower(preg_replace('/\s+/', '', $str));
	return $str;
}

function getStackLocations($dataName){
    $result = mlavu_query("select * from `restaurant_locations` where `dataname`='[1]'",$dataName);
    $locations = array();
    if(mysqli_num_rows($result))
    {
        while($row = mysqli_fetch_assoc($result)){
		    $locations[$row['locationid']] = array('title'=>$row['title']);
	    }
    }
	
    return $locations;
}


function buildCsv($row, $sfdcAddressList, $stateCodes, $lastReportData){
	global $counter;
    #get CP user details
    $cpLogin = checkCpLogin($row['dataname'], "2019-06-06");

    #get POS user details
    $posLogin = checkPosLogin($row['dataname'], "2019-06-06");
    
    
    #check last 30 days transaction
    $hasThirtyDays = (hasThirtyDaysTransaction($row['dataname'], $row['extra']['location_id']))? 'Yes':'No';

    #check last 28 days total transaction
    $weeklyTransaction = checkWeeklyTransaction($row['dataname'], $row['extra']['location_id']);
    $weeklyStatus = "No";
    $localCurrency = $weeklyTransaction['local_currency'];

    # check acount status
    $acountStatus = "Existing";
    if(!empty($lastReportData) && !isset($lastReportData[$row['dataname']])){
        $acountStatus = "New";
    }

      //echo "----amount: ".$weeklyTransaction."\n";
    if($weeklyTransaction['amount'] >= 10000){
        $weeklyStatus = "Yes";
    }

    $userFirstLoginExist = ($cpLogin['first_login'] == "Yes" || $posLogin['first_login'] == 'Yes') ? 'Yes':'No';
    $userNameExist = ($cpLogin['user_name_exist'] == "Yes" || $posLogin['user_name_exist'] == 'Yes') ? 'Yes':'No';

    $active = "No";
    if($row['extra']['is_production'] == 'Yes' && $hasThirtyDays == 'Yes' && $weeklyStatus == 'Yes' && ($cpLogin['first_login'] == "Yes" || $posLogin['first_login'] == 'Yes')){
        $active = "Yes";
    }

    $keys = getLocationApiKeys($row['dataname'], $row['extra']['location_id']);
    $apiKey = ($keys['api_key'])? $keys['api_key']:'-';
    $apiToken = ($keys['api_token'])? $keys['api_token']:'-';
    
    #customer complete physical address
    $configSettings = array(
        'business_country' => '',
        'business_city' => '',
        'business_state' => '',
        'business_address' => '',
        'business_zip' => ''
    );
   
    $daterange = date('Y-m-d', strtotime(date("Y-m-d") . '-28 days'))." To ".date("Y-m-d");

    if ($sfdcAddressList[$row['dataname']]) {
        $configSettings['business_address'] = $sfdcAddressList[$row['dataname']][1];
        $configSettings['business_city'] = $sfdcAddressList[$row['dataname']][2];
        $configSettings['business_state'] = $sfdcAddressList[$row['dataname']][3];
        $configSettings['business_country'] = $sfdcAddressList[$row['dataname']][4];
        $configSettings['business_zip'] = $sfdcAddressList[$row['dataname']][5];
    } else {
    #find business address of location from config
    $confiSql = "SELECT setting,value,value2 FROM ".$row['extra']['database'].".config WHERE type = 'location_config_setting' and _deleted = 0 and setting in ('business_name', 'business_country', 'business_address', 'business_city', 'business_state', 'business_zip')";
        $confiObj = mlavu_query($confiSql);

        while($configData = mysqli_fetch_assoc($confiObj)){
            $configSettings[$configData['setting']] = $configData['value'];
        }

        $configSettings['business_country'] = $configSettings['business_country'] ? $configSettings['business_country'] : "-";
        $configSettings['business_address'] = $configSettings['business_address'] ? $configSettings['business_address'] : "-";
        $configSettings['business_city'] = $configSettings['business_city'] ? $configSettings['business_city'] : "-";
        $configSettings['business_state'] = $configSettings['business_state'] ? $configSettings['business_state'] : "-";
        $configSettings['business_zip'] = $configSettings['business_zip'] ? $configSettings['business_zip'] : "-";

    }

    //get business state replaced with state code
    $stateCode = $stateCodes[compressStr($configSettings['business_state'])];
    if ($stateCode) {
        $configSettings['business_state'] = $stateCode;
    }

    //Replace country keyword 'United States' with 'US'
    $countryCode = array('usa','unitedstatesofamerica','unitedstates','usa','u.s.a.','u.s','americansamoa','americanland');
    if (in_array(compressStr($configSettings['business_country']),$countryCode)) {
        $configSettings['business_country'] = 'US';
    }

    echo $row['dataname']." (".$row['extra']['title'].")...:$active:...".++$counter."\n";

   $csvData = "\n\"".$row['dataname']."\",\"".$row['extra']['title']."\",".$active.",\"".$cpLogin['user_name']."\",\"".$cpLogin['user_login']."\",".$cpLogin['user_first_login_ts'].",\"".$posLogin['user_name']."\",\"".$posLogin['user_login']."\",".$posLogin['user_first_login_ts'].",\"".$configSettings['business_address']."\",\"".$configSettings['business_city']."\",\"".$configSettings['business_state']."\",\"".$configSettings['business_country']."\",".$configSettings['business_zip'].",\"".$apiKey."\",\"".$apiToken."\",\"".$weeklyTransaction['format_amount']."\",".$localCurrency.",".$daterange.",".$isProduction.", ".$hasThirtyDays.",".$weeklyStatus.",".$userFirstLoginExist.",".$userNameExist.",".$row['created'].",".$acountStatus;

   return $csvData;

}
#Read last generated report
$lastReportData = array();
$lastReportFileName =  'data/'.trim(file_get_contents('/home/poslavu/private_html/scripts/omnivore_report_log.txt'));
//$lastReportFileName = 'data/active_omnivore_customers_v3_2019-09-04_05.52.28.csv'; #hard code for now. latter will comment this line and uncomment above one
if(isset($lastReportFileName) && $lastReportFileName != ""){
    $lastReportFile = fopen($lastReportFileName,"r");
    $csvData = fgetcsv($lastReportFile);# skip header part
    while($csvData = fgetcsv($lastReportFile)){
        $lastReportData[$csvData[0]]=$csvData;
    }
    fclose($lastReportFile);
}


#Find all Omnivor customers
//$locationSql = "SELECT DISTINCT(opa.user_id) AS user_id, ca.dataname FROM oauth2_provider_accesstoken opa INNER JOIN  `customer_accounts` ca ON opa.user_id=ca.`id`";

$locationSql = "select id, company_name, data_name as dataname, created from `restaurants` where `data_name` <> '' AND `data_name` <> 'main' AND `disabled` = 0 AND demo <> 1 AND test=0 AND dev = 0  order by created DESC";
//$locationSql = "select id, company_name, data_name as dataname from `restaurants` where `data_name` <> '' AND `data_name` <> 'main' AND `disabled` = 0 AND demo <> 1 AND test=0 AND dev = 0 AND data_name IN ('hop_nuts_brewi2')";

$locationObj = mlavu_query($locationSql);

$delimiter = "\t";
$filename = "active_omnivore_customers_" . date('Y-m-d_H.i.s') . ".csv";
$file = fopen('php://memory', 'w');
$csvData = '';

//build array for US state codes
$stateCodes = array();
$f=fopen('us-state-code.csv', 'r');
fgetcsv($f);
while ($data = fgetcsv($f)) {
	$stateCodes[compressStr($data[0])] = $data[1];
}
$sfdcAddressList = getSfdcAddress();

// Get list of existing/non-archived dataname
$existingDatanameList = getExistingDatanames();

$data .= 'Dataname, Title, Omnivore Enabled, CP User Name, CP User Login, CP Login (EST), POS User Name, POS User Login, POS Login (EST), Business Address, Business City, Business State, Business Country, Postal Code, API Key, API Token, 28 Days Total (>=$10000), Local Currency, Date Range For 28 Days Total, Is Production, Meets 30 Days Requirement, Meets 28 Days Total Requirement, User First Login, User Name, Date Created, Status';

$counter=0;
while($row = mysqli_fetch_assoc($locationObj)){
    $database = "poslavu_".$row['dataname']."_db";
    #checks for the existence/archived dataname
    if(!in_array($database,$existingDatanameList)){
        continue;
    }
  
	//get chain locations
    $locations = getStackLocations($row['dataname']);
    
    #check if is trial
    $isProduction = (isTrialAccount($row['dataname']))? 'No':'Yes';
    $row['extra']['is_production'] = $isProduction;
    $row['extra']['database'] = $database;

    if($locations){
    	foreach ($locations as $locationId => $list) {
            $row['extra']['location_id'] = $locationId;
            $row['extra']['title'] = $list['title'];
            $data .= buildCsv($row,$sfdcAddressList,$stateCodes,$lastReportData);
        }//END foreach
    }else{
            $row['extra']['title'] = $row['company_name'];
            $row['extra']['location_id'] = "";
            $data .= buildCsv($row,$sfdcAddressList,$stateCodes,$lastReportData);
    }

}
//echo $csvData;
//exit;
$file = '/home/poslavu/private_html/scripts/data/'.$filename ;
$logfile = '/home/poslavu/private_html/scripts/omnivore_report_log.txt';
file_put_contents($file, $data);
file_put_contents($logfile," $filename"); #write last report file name in logfile


?>