<?php
$filePath = __DIR__.'/../../../';
require_once $filePath .'public_html/admin/cp/reports_v2/emailReports.php';

class GenerateRport extends EmailReports {
    private $processingQueries;
    private $chunckSize;
    private $offest = 0;
    private $queryStatus = [];
    private $progessStatus = [];
    private $recordId = false;

    function __construct($token = false) {
        parent::__construct($token);
        $this->testQueries();
        $this->decodeQueries();
        $this->chunckSize = (int) $this->limit;
        $this->deleteRecordById();
    }

    private function deleteRecordById() {
        $table = $this->getTableName();
        $query = 'DELETE FROM [0] WHERE `process_id` = "[1]" AND `token` = "[2]"';
        return mlavu_query($query, [$table, $this->id, $this->token]);
    }

    public function getCurrentOffset() {
        return $this->offest;
    }

    public function getChunkSize() {
        return $this->chunckSize;
    }

    public function getDecodedQueries() {
        return $this->processingQueries;
    }

    private function nextSet() {
        $this->offest = (int) ($this->offest + $this->chunckSize);
    }

    private function tokenError($id) {
        $this->logError('Token not found');
        if (isset($id) && !empty($id)) {
            $this->update(['status' => self::$_STATUS_ERROR], false, $id);
        }
    }

    private function setError($methodName, $message) {
        $this->logError('\n\n'.$methodName.'() : '.$message);
        $this->update(['status' => self::$_STATUS_ERROR], $this->token);
        exit;
    }

    private function setStatusToProcessing() {
        $this->update(['status' => self::$_STATUS_PROCESSING], $this->token);
    }

    private function decodeQueries() {
        try {
            $this->processingQueries = !is_array($this->queries) ? json_decode($this->queries, true) : $this->queries;
        } catch (Exception $e) {
            $this->setError(__METHOD__, 'Cannot Decode Queries!');
        }
    }

    public function testQueries() {
        if (empty($this->queries)) {
            $this->setError(__METHOD__, 'Nothing to Processess!');
        }
        return true;
    }

    private function allDone() {
        $this->update(
            [
                'status' => self::$_STATUS_DONE, 
                'no_of_record_processed' => $this->noOfRecordProcessed,
            ], $this->token
        );
        $this->sendMail();
    }

    private function insertRecord($data) {
        $values = [
            $this->token,
            $this->id,
            $data
        ];
        $status = mlavu_query('INSERT INTO '.$this->getTableName().' (`id`, `token`, `process_id`, `file`) VALUES (null, "[0]", "[1]", "[2]")', $values);
        if ($status) {
            $this->recordId = mlavu_insert_id();
        } else {
            $this->setError(__METHOD__, 'Failed to insert into reports');
        }

        return $status;
    }

    private function appendRecord($data) {
        $status = mlavu_query('UPDATE '.$this->getTableName().' SET `file` = CONCAT(`file`, "[0]") WHERE `id` = "[1]" AND `token` = "[2]"', [$data, $this->recordId, $this->token]);
        if (!$status) {
            $this->setError(__METHOD__, 'Failed to update records');
        }
        return $status;
    }

    private function saveData($data) {
        if ($this->recordId === false) {
            $this->insertRecord($data);
        } else {
            $this->appendRecord($data);
        }
    }

    private function resetOffset() {
        $this->offest = 0;
    }

    private function generatReport($dataname) {
        $this->setQueryProcessing($dataname);
        $data = $this->fetch($this->processingQueries[$dataname], $this->getLocationName($dataname));
        if (empty($data) || $data === false) {
            $this->setQueryDone($dataname);
            $this->resetOffset();
            return true;
        } else {
            $this->saveData($data);
            sleep(1);
            $this->nextSet();
            $this->generatReport($dataname);
        }
    }

    private function setQueryProcessing($dataname) {
        if (!isset($this->queryStatus[$dataname]['status'])) {
            $this->queryStatus[$dataname]['status'] = self::$_STATUS_PROCESSING;
        }
        $this->queryStatus[$dataname]['offset'] = $this->getCurrentOffset();
        $this->queryStatus[$dataname]['chucksize'] = $this->getChunkSize();
        $this->progessStatus[$dataname] = self::$_STATUS_PROCESSING;
    }

    private function setQueryDone($dataname) {
        $this->queryStatus[$dataname]['status'] = self::$_STATUS_DONE;
    }

    private function fetch($query, $locationName, $type = 'main') {
        $dataString = "";
        switch($type) {
            case self::$_QUERY_TYPE:
            default: 
                $preparedQuery = $query." LIMIT [0], [1]";
                $currentOffset = $this->getCurrentOffset();
                $result = mlavu_query($preparedQuery, [$currentOffset, $this->getChunkSize()]);
                $headerSet = false;
                while($row  = mysqli_fetch_assoc($result)) {
                    if($currentOffset < 1 && !$headerSet) {
                        if ($this->noOfRecordProcessed > 0) {
                            $dataString .= "\n\n";
                        }
                        $dataString .= $locationName." \n";
                        $dataString .= $this->createHeader(array_keys($row));
                        $headerSet = true;
                    }
                    $dataString .= implode(',', $row) ." \n";
                    $this->noOfRecordProcessed++;
                }
            break;
        }
        return $dataString;
    }

    private function createHeader($headers) {
        foreach ($headers as $key => $header) {
            $headers[$key] = $this->createTitle($header);
        }
        return implode(',', $headers). " \r\n";
    }

    private function createTitle($string, $replace = ' ') {
        $string = str_replace(' ', $replace, $string); // Replaces all spaces with hyphens.
        return ucwords(preg_replace('/[^A-Za-z0-9\-.]/', $replace, $string)); // Removes special chars.
    }

    private function getLocationName($dataname) {
        $query = 'SELECT title FROM restaurant_locations where dataname = "[0]"';
        $result = mlavu_query($query, [$dataname]);
        $data = mysqli_fetch_assoc($result);
        return isset($data['title']) ? $data['title'] : '';
    }

    public function process() {
        $this->setStatusToProcessing();
        $queries = $this->getDecodedQueries();
        foreach ($queries as $dataname => $query) {
            if ($dataname != 'dates') {
                $this->generatReport($dataname);
            }
        }
        $this->allDone();
    }
}

?>