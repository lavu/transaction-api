<?php
    $_REQUEST['cc'] = '';
    $filePath = __DIR__.'/../../../';
    require_once $filePath ."public_html/admin/lib/info_inc.php";
    require_once $filePath ."private_html/scripts/reports/process.php";
    $numberOfProcessAllowed = 10;
    if(isset($argv[1]) && !empty($argv[1])){
        $numberOfProcessAllowed = (int) $argv[1];
    }
    $reportUnderProcess = mlavu_query('SELECT COUNT(*) as processing FROM `proccess_reports_to_email` WHERE status = "[0]"', ['processing']);
    $processingCount = mysqli_fetch_assoc($reportUnderProcess);
    $processingCount = isset($processingCount['processing']) ? $processingCount['processing'] : 0;

    if (($numberOfProcessAllowed - $processingCount) >= 0 ) {
        $processQuery = 'SELECT id, token FROM `proccess_reports_to_email` WHERE `status` = "pending" LIMIT [0]';
        $pendingReportResult = mlavu_query($processQuery, [$numberOfProcessAllowed - $processingCount]);

        $ids = array();
        $result = array();
        while ($rec = mysqli_fetch_assoc($pendingReportResult)) {
            $ids[]=$rec['id'];
            $result[]=$rec;
        }

       if (count($ids) > 0) {
        mlavu_query('UPDATE proccess_reports_to_email SET status="processing" WHERE id IN ([1])', implode(',', $ids));
       }

        foreach ($result as $data) {
            $token = isset($data['token']) && !empty($data['token']) ? $data['token'] : '';
            $id = isset($data['id']) && !empty($data['id']) ? $data['id'] : false;
            if (strlen($token) < 1) {
                if($id !== false) {
                    $reportGenerator = new GenerateRport();
                    $reportGenerator->tokenError($id);
                }
                continue;
            } else {
                $reportGenerator = new GenerateRport($token);
                $reportGenerator->process();
            }
        }
    }
?>
