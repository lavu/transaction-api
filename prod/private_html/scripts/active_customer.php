<?php
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
ini_set("display_errors",0);
error_reporting(0);

#Find all active customers or dataname from restaurants
$startDate = date( 'Y-m-d', strtotime(date("Y-m-d") . '-1 days') );
$locationSql = "SELECT id, company_name, data_name, last_activity FROM `restaurants` WHERE `data_name` <> '' AND `data_name` <> 'main' AND DATE(last_activity) >='$startDate' AND `disabled` = 0 AND demo <> 1 AND test=0 AND dev = 0 ";

$result = mlavu_query($locationSql);

$delimiter = "\t";
$filename = "active_customer_" . date('Y-m-d') . ".csv";
$file = fopen('php://memory', 'w');
$csvData = '';
$csvData .= 'Dataname, Customer Name, Last Activity';

while($row = mysqli_fetch_assoc($result)){
    #checks for the existence of a particular database
    $database = "poslavu_".$row['data_name']."_db";
    if(!databaseExists($database)){
        continue;
    }
    $row['company_name'] = str_replace(","," ", $row['company_name']);

    $csvData .= "\n".$row['data_name'].",".$row['company_name'].",".$row['last_activity'];
}
if(!is_dir("/home/poslavu/private_html/scripts/data")){
    mkdir("/home/poslavu/private_html/scripts/data/", 0775);
}
$file = '/home/poslavu/private_html/scripts/data/'.$filename ;
file_put_contents($file, $csvData);
?>

