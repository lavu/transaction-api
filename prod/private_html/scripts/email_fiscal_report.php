<?php
ini_set("memory_limit","384M");
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

$info_query = mlavu_query("select * from `fiscal_csv_email_info` where status=0 limit 1");
if(mysqli_num_rows($info_query)>0)
{
	while($req_data = mysqli_fetch_assoc($info_query))
	{
		$info = $req_data['req_info'];
		$info = json_decode($info,true);
		$update_tbl_id = $req_data['id'];
		$location = $req_data['location'];
		$report_type =  $req_data['report_type'];
		$link =  $req_data['request_domain'];
		if($req_data['multi_report']>0)
		{
			$data_name = '';
			$chain_company_id = explode(',',$req_data['chain_company_id']);
			$get_data_arr = array();
			foreach($chain_company_id as $company_id)
			{
				$data_name = connect_to_chain_database( $company_id );
				$dataname= 'poslavu_'.$data_name.'_db';
				$sql = 'SELECT COUNT(*) AS `exists` FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMATA.SCHEMA_NAME="'.$dataname.'"';
				$query = mlavu_query($sql);
				$row = mysqli_fetch_assoc($query);
				$dbExists = $row['exists'];
				if($dbExists>0)
				{
					//echo $data_name.'<br>';
					$info['foldername'] =  $data_name;
					if($info['report_name']=='fiscal_accounting' || $info['report_name']=='fiscal_daily_accounting')
					{
						$get_record_arr = fiscal_accounting_report($info);
						if(!empty($get_record_arr))
						{
							$get_data_arr = array_merge($get_data_arr,$get_record_arr);
						}
					}
					else
					{
						$get_record_arr = fiscal_reconciliation_report($info);
						if(!empty($get_record_arr))
						{
							$get_data_arr = array_merge($get_data_arr,$get_record_arr);
						}
					}
				}
				
			}
		}
		else 
		{
			if($info['report_name']=='fiscal_accounting' || $info['report_name']=='fiscal_daily_accounting')
			{
				$get_data_arr = fiscal_accounting_report($info);
			}
			else
			{
				$get_data_arr = fiscal_reconciliation_report($info);
			}
		}
		
		createCSV($get_data_arr,$location,$update_tbl_id,$report_type,$link);
	}
}


function fiscal_reconciliation_report($vars)
{
	$dataname = 'poslavu_'.$vars['foldername'].'_db';
	$query = "select locations.title as 'Location Name', fiscal_reports.register as 'Register', fiscal_reports.pv_number as 'P. V. No', 
	fiscal_reports.report_type as 'Report Type', fiscal_reports.first_invoice as 'First Invoice', 
	fiscal_reports.last_invoice as 'Last Invoice', fiscal_reports.report_id as 'Z Report ID', 
	fiscal_reports.fiscal_day as 'Date (AAMMDD)', fiscal_reports.net_total as 'Net Total', fiscal_reports.tax as 'Tax', 
	fiscal_reports.total as Total, fiscal_reports.refund_net as 'Refund Net', fiscal_reports.refund_tax as 'Refunded Tax', 
	fiscal_reports.refund_total as 'Refund Total', fiscal_reports.untax_total as 'Untaxed Total', 
	fiscal_reports.untax_refund as 'Untaxed Refund Total' from $dataname.`fiscal_reports`
	LEFT JOIN $dataname.`config` ON fiscal_reports.register = config.value2
	LEFT JOIN $dataname.`locations` ON config.location = locations.id
	where fiscal_reports.datetime >= '".$vars['start_datetime']."' and fiscal_reports.datetime <= '".$vars['end_datetime']."'
			and fiscal_reports.report_id != '' and report_type IN ('TiqueFacturaA', 'TiqueFacturaB','ReciboB')
			order by fiscal_reports.report_id asc";
	$exec_query = mlavu_query($query);
	while($report_read = mysqli_fetch_assoc($exec_query)) {
		foreach( $report_read as $k => $v ) {
			if( report_value_is_number( $v ) && substr($v,0,7) != '[money]'){
				if($k == "First Invoice" || $k == "Last Invoice" || $k == "Z Report ID" || $k == "P. V. No"){
					$report_read[$k] = "{$v}";
				}
				else{
					$report_read[$k] = "[number]{$v}";
				}
			}else if(report_value_is_date($v)){
				$report_read[$k] = "[date]{$v}";
			}
		}

		$report_results[] = $report_read;
	}
	return $report_results;
}

function fiscal_accounting_report($vars)
{
	$dataname = 'poslavu_'.$vars['foldername'].'_db';

	if(strpos($vars['report_name'],"daily")!==false)
		$report_group_by = "day";
		else
			$report_group_by = "record";

			$fiscal_ts_start = strtotime($vars['start_datetime']) - (60 * 60 * 24 * 2);
			$fiscal_ts_end = strtotime($vars['end_datetime']) + (60 * 60 * 24 * 2);
			$fvars = array();
			$fvars['min_fiscal_datetime'] = date("Y-m-d",$fiscal_ts_start);
			$fvars['max_fiscal_datetime'] = date("Y-m-d",$fiscal_ts_end);
			$fiscal_spans = array();
			$fiscal_query = mlavu_query("select * from `$dataname`.`fiscal_reports` where `datetime`>='[min_fiscal_datetime]' and `datetime`<='[max_fiscal_datetime]'",$fvars);
			while($fiscal_read = mysqli_fetch_assoc($fiscal_query))
			{
				$register = $fiscal_read['register'];
				$first_invoice = $fiscal_read['first_invoice'];
				$last_invoice = $fiscal_read['last_invoice'];
				$invoice_report_id = $fiscal_read['report_id'];
				$report_type = $fiscal_read['report_type'];
					
				if(!isset($fiscal_spans["RT".$report_type]["R".$register]))
				{
					$fiscal_spans["RT".$report_type]["R".$register] = array();
				}

				if(is_numeric($first_invoice) && is_numeric($last_invoice))
				{
					$invoice_span = $last_invoice * 1 - $first_invoice * 1;
					if($invoice_span >=0 && $invoice_span <= 5000)
					{
						for($n=$first_invoice * 1; $n<=$last_invoice * 1; $n++)
						{
							$fiscal_spans["RT".$report_type]["R".$register]["I".($n * 1)] = $invoice_report_id;
						}
					}
				}
			}
			$startts = time();
				
			$query = "select `locations`.`title` as 'location_name',
			orders.closed as `schedule`,
			LEFT(orders.closed,10) as fiscal_date,
			LEFT(orders.closed,10) as `commercial_date`,
			'z_location' as z_location,
			split_check_details.print_info AS fiscal_type,
			orders.order_id as order_id,
			split_check_details.print_info as fiscal_txn_id,
			IF(truncate(' ',`med_customers`.field10) = '', `med_customers`.field10, '') AS cuit_local_id,
			payment_types.type as payment_type,
			cc_transactions.card_desc as last_four,
			if(count(*) > 0, concat('[money]',format(sum( IF(cc_transactions.action='Refund', -1*cc_transactions.total_collected, cc_transactions.total_collected) ),2)), '[money]0.00') as total_payment,
			`payment_types`.`type` as `group_col_payment_types_type`
			from `$dataname`.`orders`
			LEFT JOIN `$dataname`.`cc_transactions` on `orders`.`order_id` = `cc_transactions`.`order_id`
			LEFT JOIN `$dataname`.`payment_types` ON `cc_transactions`.`pay_type_id` = `payment_types`.`id` AND (`cc_transactions`.`loc_id` = `payment_types`.`loc_id` OR '0' = `payment_types`.`loc_id` )
			LEFT JOIN `$dataname`.`locations` on `orders`.`location_id`=`locations`.`id`
			LEFT JOIN `$dataname`.`split_check_details` on `orders`.`order_id`=`split_check_details`.`order_id` AND `split_check_details`.`check`=1
			LEFT JOIN `$dataname`.`med_customers` ON `med_customers`.id = SUBSTRING_INDEX(`orders`.original_id, '|', 1)
			WHERE orders.closed >= '[start_datetime]'
			AND orders.closed < '[end_datetime]'
			AND orders.void = '0'
			AND orders.location_id = '1'
			group by orders.id order by fiscal_date desc;";

			$order_id_list = "";

			$rows = array();
			$report_query = mlavu_query($query,$vars);
			while($report_read = mysqli_fetch_assoc($report_query))
			{
				if($order_id_list!="") $order_id_list .= ",";
				$escaped_order_id = str_replace(urlencode("-"),"-",urlencode($report_read['order_id']));
				$order_id_list .= "'".$escaped_order_id."'";
					
				if(isset($report_read['fiscal_type'])){
					$fiscal_type = json_decode($report_read["fiscal_type"]);
					$report_read['fiscal_type'] = $fiscal_type->receipt_type;
				}
				else{
					$report_read['fiscal_type'] = "";
				}
					
				if(isset($report_read['fiscal_txn_id']))
				{
					$fiscalJsonArray = json_decode($report_read['fiscal_txn_id']);
					if(isset($fiscalJsonArray->transaction_id)){
						$report_read['fiscal_txn_id']=$fiscalJsonArray->transaction_id;
					}
				}
				$rows[] = $report_read;
			}

			$order_data = array();
			$categories = array();

			$elapsed = time() - $startts;
			if (!empty($order_id_list))
			{
				$query = "select `orders`.`total` as `order_total`,
				concat('[money]',`orders`.`subtotal`- `orders`.`itax`) as `order_sale`,
				concat('[money]',`orders`.`discount` + `orders`.`idiscount_amount`) as `order_discount`,
				concat('[money]',`orders`.`subtotal` - (`orders`.`discount` + `orders`.`idiscount_amount` + `orders`.`itax`)) as `order_taxable`,
				concat('[money]',`orders`.`tax` + `orders`.`itax`) as `order_tax`,
				`orders`.`register_name` as `register_name`,
				`menu_categories`.`name` as `category_name`,
				`menu_categories`.`id` as `category_id`,
				`order_contents`.`tax_subtotal1` as `cost`,
				`order_contents`.`order_id` as `order_id`,
				`order_contents`.`item_id` as `item_id`
				from `$dataname`.`order_contents`
				left join `$dataname`.`menu_categories` on `order_contents`.`category_id`=`menu_categories`.`id`
				left join `$dataname`.`orders` on `order_contents`.`order_id`=`orders`.`order_id`
				where `orders`.`order_id` IN ($order_id_list) AND `order_contents`.`quantity`*1 > 0";

				$content_query = mlavu_query($query);
				while($content_read = mysqli_fetch_assoc($content_query))
				{
					$order_id = $content_read['order_id'];
					$category_id = $content_read['category_id'];
					$category_name = trim($content_read['category_name']);
					$cost = $content_read['cost'];
					if(!isset($order_data[$order_id][$category_id]))
						$order_data[$order_id][$category_id] = 0;
						$order_data[$order_id][$category_id] += $cost;
						$categories[$category_id] = $category_name;
						$order_data[$order_id]['info'] = $content_read;
						$order_data[$order_id]['register_name'] = $content_read['register_name'];
				}
			}

			// Make sure all orders have information for all categories found
			foreach($order_data as $order_id => $order_arr)
			{
				foreach($categories as $category_id => $category_name)
				{
					if(!isset($order_data[$order_id][$category_id]))
						$order_data[$order_id][$category_id] = 0;
				}
			}

			$processed_order_ids = array();

			for($i=0; $i<count($rows); $i++)
			{
				$order_id = $rows[$i]['order_id'];
				$already_processed = in_array($order_id, $processed_order_ids);

				$rows[$i]['order_sale'] = $order_data[$order_id]['info']['order_sale'];
				$rows[$i]['order_discount'] = $order_data[$order_id]['info']['order_discount'];
				$rows[$i]['order_taxable'] = $order_data[$order_id]['info']['order_taxable'];
				$rows[$i]['order_tax'] = $order_data[$order_id]['info']['order_tax'];
				foreach($categories as $category_id => $category_name)
				{
					$cost = $order_data[$order_id][$category_id];
					$category_name = $categories[$category_id];

					if ($already_processed)
					{
						$rows[$i][$category_name] = "[money]0";
					}
					else
					{
						if (isset($rows[$i][$category_name])) // i.e., 2 categories are named 'Pinta Patagonia'
						{
							$previous_cost = str_replace("[money]", "", $rows[$i][$category_name]);
							$cost += $previous_cost;
						}
						$rows[$i][$category_name] = "[money]".$cost;
					}
				}
				$order_arr = $order_data[$order_id];
				$fiscal_txn_id = $rows[$i]['fiscal_txn_id'];
				$fiscal_type = $rows[$i]['fiscal_type'];
				if(isset($order_arr['register_name']) && trim($order_arr['register_name'])!="" && trim($fiscal_txn_id)!="")
				{
					$rows[$i]['z_location'] = "No ZReport";
					$register_name = trim($order_arr['register_name']);
					if(isset($fiscal_spans["RT".$fiscal_type]["R".$register_name]["I".($fiscal_txn_id * 1)]))
					{
						$rows[$i]['z_location'] = "#" . $fiscal_spans["RT".$fiscal_type]["R".$register_name]["I".($fiscal_txn_id * 1)];
					}
				}
				$processed_order_ids[] = $order_id;
			}

			if($report_group_by=="day")
			{
				$ommitcols = array("P. V. No","order_id", "fiscal_txn_id", "cuit_local_id", "payment_type", "last_four", "total_tip");
				$sumcols = array("total_payment", "total_cash", "total_card", "order_sale", "order_discount", "order_taxable", "order_tax");
				foreach($categories as $category_id => $category_name) $sumcols[] = $category_name;

				for($i=0; $i<count($rows); $i++)
				{
					$rows[$i]['total_cash'] = 0;
					$rows[$i]['total_card'] = 0;
					$payment_type = strtolower(trim($rows[$i]['payment_type']));

					if($payment_type=="cash") $rows[$i]['total_cash'] = $rows[$i]['total_payment'];
					else if($payment_type=="card") $rows[$i]['total_card'] = $rows[$i]['total_payment'];
				}
				for($i=0; $i<count($rows); $i++)
				{
					$newrow = array();
					foreach($rows[$i] as $col => $val)
					{
						if($col=="total_payment") // put cash and card before total
						{
							$newrow['order_sale'] = $rows[$i]['order_sale'];
							$newrow['order_discount'] = $rows[$i]['order_discount'];
							$newrow['order_taxable'] = $rows[$i]['order_taxable'];
							$newrow['order_tax'] = $rows[$i]['order_tax'];
						}
						if($col!="total_cash"&&$col!="total_card")
						{
							$newrow[$col] = $rows[$i][$col];
						}
						if($col=="total_payment") // put cash and card before total
						{
							$newrow['total_cash'] = $rows[$i]['total_cash'];
							$newrow['total_card'] = $rows[$i]['total_card'];
						}
					}
					$rows[$i] = $newrow;
				}


				$lastday = "";
				$dayrows = array();
				for($i=0; $i<count($rows); $i++)
				{
					$nextday = $rows[$i]['fiscal_date'];
					if($nextday!=$lastday || $i==0)
					{
						$newrow = array();
						foreach($rows[$i] as $col => $val)
						{
							if(!in_array($col,$ommitcols))
							{
								$newrow[$col] = $val;
							}
						}
						$dayrows[] = $newrow;
					}
					else
					{
						$crow = count($dayrows) - 1;
						foreach($dayrows[$crow] as $col => $val)
						{
							if(in_array($col,$sumcols))
							{
								$prefix = "";
								$current_val = $dayrows[$crow][$col];
								$add_val = $rows[$i][$col];
								if(substr($current_val,0,7)=="[money]"||substr($add_val,0,7)=="[money]") $prefix = "[money]";
								if(substr($current_val,0,8)=="[number]"||substr($add_val,0,8)=="[number]") $prefix = "[number]";
								if($prefix!="")
								{
									$current_val = str_replace($prefix,"",$current_val);
									$add_val = str_replace($prefix,"",$add_val);
								}
								$current_val = $current_val * 1 + $add_val * 1;

								if($prefix!="") $current_val = $prefix . $current_val;
								$dayrows[$crow][$col] = $current_val;
							}
						}
					}
					$lastday = $nextday;
				}
				$rows = $dayrows;
			}
			else
			{
				for($i=0; $i<count($rows); $i++)
				{
					$newrow = array();
					foreach($rows[$i] as $col => $val)
					{
						if($col=="total_payment") // put taxable before total
						{
							$newrow['order_sale'] = $rows[$i]['order_sale'];
							$newrow['order_discount'] = $rows[$i]['order_discount'];
							$newrow['order_taxable'] = $rows[$i]['order_taxable'];
							$newrow['order_tax'] = $rows[$i]['order_tax'];
						}
						if($col!="total_cash"&&$col!="total_card")
						{
							$newrow[$col] = $rows[$i][$col];
						}
					}
					if(empty($newrow["total_payment"])){
						$newrow["total_payment"]="[money]0";
					}
					if(empty($newrow['order_sale'])){
						$newrow['order_sale']="[money]0";
					}
					if(empty($newrow['order_discount'])){
						$newrow['order_discount']="[money]0";
					}
					if(empty($newrow['order_taxable'])){
						$newrow['order_taxable']="[money]0";
					}
					if(empty($newrow['order_tax'])){
						$newrow['order_tax']="[money]0";
					}
					$rows[$i] = $newrow;
				}
			}

			$translated_phrases = array();
			$auto_translate = array("schedule","fiscal_date","commercial_date","z_location","fiscal_type","order_id","fiscal_txn_id","cuit_local_id","payment_type","last_four","order_sale","order_discount","order_taxable","order_tax","total_payment");
			for($n=0; $n<count($auto_translate); $n++)
			{
				$phrase = $auto_translate[$n];
				$human_phrase = trim(ucwords(str_replace("_"," ",$phrase)));
				$translated_human_phrase = speak($human_phrase);
				if($translated_human_phrase != $human_phrase)
				{
					$translated_phrase = str_replace(" ","_",trim(str_replace(".","",strtolower($translated_human_phrase))));
					$translated_phrases[$phrase] = $translated_phrase;
				}
			}
			for($i=0; $i<count($rows); $i++)
			{
				$newrow = array();
				foreach($rows[$i] as $col => $val)
				{
					if(isset($translated_phrases[$col])) $newrow[$translated_phrases[$col]] = $val;
					else $newrow[$col] = $val;
				}
				$rows[$i] = $newrow;
			}

			return $rows;
}


function report_value_is_number($n)
{
	if( substr($n,0,8) == '[number]' )
	{
		return true;
	}
	$n = str_replace(".", "", str_replace(",", "", $n));
	return is_numeric( $n );
}

function report_value_is_date($n)
{
	if (preg_match("/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/", $n))
	{
		return true;
	}else if(preg_match("/\d{2}:\d{2}:\d{2}/", $n)){
		return true;
	} else if (preg_match("/\d{4}-\d{1}/", $n)){
		return true;
	} else {
		return false;
	}
	return $n;
}

function format_currency($monitary_amount, $dont_use_monetary_symbol = false, $monitary_symbol = "",$location) {

	$query = "select * from  `poslavu_".$location."_db`.`locations`";
	$content_query = mlavu_query($query);
	$location_info = mysqli_fetch_assoc($content_query);
	$needToAddMinus = "";
	$monitary_amount = str_replace(array('[money]','[number]'),array('',''),$monitary_amount);
	$number = $monitary_amount;

	$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
	if($location_info['decimal_char'] != ""){
		$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
		$thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char']==".") ? "," : '.';
	}

	$thousands_sep = ",";
	if( !empty($thousands_char) ) {
		$thousands_sep = $thousands_char;
	}

	if(strpos($number,"-") !== false){
		$find_minus = explode("-", $number);
		$needToAddMinus = "-";
	}

	if($monitary_symbol === ""){
		$monitary_symbol = isset($location_info['monitary_symbol']) ? htmlspecialchars($location_info['monitary_symbol']) : '';
	}
	$left_or_right = isset($location_info['left_or_right']) ? $location_info['left_or_right'] : 'left';

	$left_side = "";
	$right_side = "";

	if (!$dont_use_monetary_symbol) {
		if ($left_or_right == 'left') {
			$left_side = $monitary_symbol;
		} else {
			$right_side = $monitary_symbol;
		}
	}

	$output = $left_side.number_format($number, $precision, $decimal_char, $thousands_sep).$right_side;
	return $output;
}

function format_array($input_array,$location)
{
	$query = "select `monitary_symbol` from  `poslavu_".$location."_db`.`locations`";
	$content_query = mlavu_query($query);
	$content_read = mysqli_fetch_assoc($content_query);
	if(!empty($input_array))
	{
		$keys = array_keys($input_array);
		for($i = 0; $i < count($input_array); $i++)
		{
			foreach($input_array[$keys[$i]] as $nkey=>$nval)
			{
				if(strpos($nval,'[money]')!== false)
				{
					$data = format_currency($nval,'','',$location);
					$formatted_array[$nkey] = $data;
				}
				else if(strpos($nval,'[number]')!== false)
				{
					$data = format_currency($nval,'','',$location);
					$formatted_array[$nkey] = $data;
				}
				else{
					if($nkey=='fiscal_txn_id' || $nkey=='First Invoice' || $nkey=='Last Invoice')
					{
						if(is_numeric($nval))
						{
							$formatted_array[$nkey] = '#'.$nval;
						}
						else {
							$formatted_array[$nkey] = $nval;
						}
					}
					else{
						$formatted_array[$nkey] = $nval;
					}
				}
			}
			$final_arr[$i] = $formatted_array;
		}
	}
	return $final_arr;
}



function createCSV($report_results,$location,$updt_tbl_id,$report_type,$link)
{
	$data = format_array($report_results,$location);
	$records = $data;
	$query_main = mlavu_query("select created_date, report_type from fiscal_csv_email_info where id='$updt_tbl_id'");
	$main_data = mysqli_fetch_assoc($query_main);
	$created_date = str_replace(' ','_',$main_data['created_date']);
	$filename = str_replace(' ','_',$main_data['report_type']).'_'.$created_date;
	$maindir = '/home/poslavu/public_html/admin/cp/abi_fiscal/';
	$maindir = '/home/poslavu/public_html/admin/images/fiscal_reports/';
	if (!file_exists($maindir))
	{
			
		mkdir($maindir, 0777, true);
	}
	$datafolder = $maindir.$location;
	if (!file_exists($datafolder))
	{
			
		mkdir($datafolder, 0777, true);
	}
	$cdt = date('Y-m-d h:i:s');
	$token = md5(uniqid(rand(), true));
	$expiry_date = date('Y-m-d', strtotime($cdt. ' + 2 days'));
	$filename = $filename.'.csv';
	$fp = fopen($datafolder.'/'.$filename, 'w');

	$i = 0;
	foreach ($records as $record) {

		if($i === 0) {
			fputcsv($fp, array_keys($record));
		}

		if(fputcsv($fp, array_values($record)))
			$i++;
	}
	

	$update_query = mlavu_query("update fiscal_csv_email_info set
			`access_token` = '$token',
			`expiry_date` = '$expiry_date',
			`status` = 1,
			`update_date` = '$cdt',
			csv_file_name = '$filename'
			where id =$updt_tbl_id
			");
	$query = "select `email` from  `poslavu_".$location."_db`.`locations`";
	$content_query = mlavu_query($query);
	$content_read = mysqli_fetch_assoc($content_query);

	$path = $link.'/cp/download.php?token='.$token;
	$to = $content_read['email'];

	$subject = $report_type.' Report';

	$headers = "From: admin@lavu.com\r\n";
	$headers .= "Reply-To:admin@lavu.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	$message = '<p>Please Click on the below link to download the report:<br>
				   <a href="'.$path.'">Download Report</a><br>
				This link will be valid for two days.
				</p>';

	mail($to, $subject, $message, $headers);

}

function connect_to_chain_database( $company_id ) 
{
	$rest_info_query_result = mlavu_query( "SELECT `data_name`, `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $company_id );
	if( !$rest_info_query_result || !mysqli_num_rows($rest_info_query_result)){
		return false;
	}
	
	$rest_info = mysqli_fetch_assoc( $rest_info_query_result );
	$rdb = $rest_info['data_name'];
	return $rdb;
}
?>