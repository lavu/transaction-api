<?php
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
ini_set("display_errors",0);
error_reporting(0);

#Find all active customers or dataname from restaurants
$startDate = date( 'Y-m-d', strtotime(date("Y-m-d") . '-1 days') );
$locationSql = "SELECT data_name, company_name FROM `restaurants` WHERE `data_name` <> '' AND `data_name` <> 'main' AND `disabled` = 0 AND demo <> 1 AND test=0 AND dev = 0";

$result = mlavu_query($locationSql);

$delimiter = "\t";
$filename = "paypal_customer_" . date('Y-m-d') . ".csv";
$file = fopen('php://memory', 'w');
$csvData = '';
$csvData .= 'Dataname, Customer Name, Integration Data 1';

$counter=0;
while($row = mysqli_fetch_assoc($result)){
    #checks for the existence of a particular database
    $database = "poslavu_".$row['data_name']."_db";
    if(!databaseExists($database)){
        continue;
    }

    $obj = mlavu_query('SELECT AES_DECRYPT(integration1, "AIH3476ag7gaGAn84nbaZZZ48hah") as integration1 FROM '.$database.'.locations WHERE gateway = "PayPal" ORDER BY id DESC LIMIT 1');
    $r = mysqli_fetch_assoc($obj);
    if (!$r) {
      continue;
    }

    echo "$database...".++$counter."\n";

    $integrationData = $r['integration1'];

    $row['company_name'] = str_replace(","," ", $row['company_name']);

    $csvData .= "\n".$row['data_name'].",".$row['company_name'].",".$integrationData;
}
if(!is_dir("/home/poslavu/private_html/scripts/data")){
    mkdir("/home/poslavu/private_html/scripts/data/", 0775);
}
$file = '/home/poslavu/private_html/scripts/data/'.$filename ;
file_put_contents($file, $csvData);
?>
