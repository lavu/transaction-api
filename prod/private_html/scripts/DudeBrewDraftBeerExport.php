<?php

/**
*  Developer : Dhruvpalsinh D Chudasama
*  Date      : 01-07-2019
*  Purpose   : It is a script that will pull data from all of Dudes Brewing Co. locations.The scrip shall only care about the sales
*			   associated with the menu group of Draft Beer.Pour is the nutrition type.
*/

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

/**
 * Class DudeBrewExport
 */
class DudeBrewExport
{
    /**
     * To Get the Request from Terminal or without it proceed to generate CSV and Transfer it via FTP.
     * @param
     * @return
     */
	public static function doProcess()
	{
		$filename = '';
		$file = '';
		$msg = '';
		$msg = "DUDE BREW : ";
		//if initiated from cron
		$resCsvGen = self::generateCsv();
		//Check Staus of Generated CSV
		if ($resCsvGen != false) {
		//Get File Names & Full File Path
		$filename = $resCsvGen[0];
		$file = $resCsvGen[1];
		//Check FTP Status
		$isFtp = self::sendFTP($filename, $file);
		if ($isFtp) {
		$msg .= "FTP Transfer Successful..";
		} else {
		$msg .= "FTP Transfer UnSuccessful..!!";
		}
		} else {
			$msg .= "There is Some Issues in Generation of CSV!";
		}
		error_log($msg);
	}

	/**
     * It will generate CSV as per start date & end date.
     * @param String $startdate, String $enddate (optional)
     * @return Boolean if fails OR Array if succeeded
     */
	public static function generateCsv($startdate = '', $enddate = '')
	{
		$isCsvGenearated = false;
		//Pacific Timezone
		date_default_timezone_set('America/Los_Angeles');
		$currentDate = date('Y-m-d');
		$cronDataDate = date('Y-m-d',strtotime("-1 days"));
		$filename = "Dudes_Brew_Co_" . date('Y-m-d_H-i-s') . ".csv";
		$file = fopen('php://memory', 'w');
		$csvDataRow = '';
		//CSV Headers
		$csvDataRow .= 'Site ID, Timestamp, Product Code, Product Description, Volume(oz)';
		$dataNameAry = array();
		$orderdataAry = array();
		$menuitemAry = array();
		$groupitemary = array();
		$nutritionAry = array('Pour', 'pour');
		$groupitemary = array();
		$locationid = '';
		$chainId = ''; 
		$start = '';
		$end = '';
		//Get Datanames
		$queryResult = mlavu_query("SELECT * from restaurants where chain_id =(SELECT chain_id from restaurants where data_name='dudes_santa_cl')");
		while($row = mysqli_fetch_assoc($queryResult)) {
			$dataNameAry[] = $row['data_name'];
		}
		//Length of Datanames array
		$len = count($dataNameAry);
		//Get Draft Beer Data Across All The Locations
		for($d=0;$d<$len;$d++) {
			$orderdataAry = array();
			//Dataname
			$data_name = '';
			$siteid = '';
			$data_name = 'poslavu_'.$dataNameAry[$d].'_db';
			$siteid = $dataNameAry[$d];
			if (!databaseExists($data_name)) {
				continue;
			}
			//Get Start Date & End Date
			$start = $cronDataDate." 05:00:00";
			$end = $currentDate." 04:59:59";

			if ($startdate != '' && $enddate != '') {
				$start = $startdate." 05:00:00";
				$end = $enddate." 04:59:59";
			}

			$itemidAry = array();
			$modidAry = array();

			//Get Item Ids,Mod Ids
			$queryItem = "SELECT ordc.item_id, ms.mod_id FROM `$data_name`.`orders` as ords LEFT JOIN `$data_name`.`order_contents` as ordc ON
					ords.order_id = ordc.order_id LEFT JOIN  `$data_name`.`modifiers_used` as ms ON ms.icid = ordc.icid WHERE
					ords.opened >= '[1]' AND ords.opened <= '[2]' AND ms.type = 'forced' ";
			$exequeryItem = mlavu_query($queryItem, $start, $end);
			if (mysqli_num_rows ( $exequeryItem )) {
				while($rowItem = mysqli_fetch_assoc($exequeryItem))
				{
					if (!in_array($rowItem['item_id'], $itemidAry) && $rowItem['item_id'] != null) {
						$itemidAry[] = $rowItem['item_id'];
					}
					if (!in_array($rowItem['mod_id'], $modidAry) && $rowItem['mod_id'] != null) {
						$modidAry[] = $rowItem['mod_id'];
					}	
				}
			}
			//Get UPC DATA
			$upcData = array();
			$queryUpc = "SELECT `UPC`, `id` FROM `[1]`.`menu_items` WHERE `id` IN ('".implode("','",$itemidAry)."')";
			$queryUpcInfo = mlavu_query($queryUpc, $data_name);
			if (mysqli_num_rows ( $queryUpcInfo )) {
				while($rowupc = mysqli_fetch_assoc($queryUpcInfo))
				{
					$upcData[$rowupc['id']] = $rowupc['UPC'];
				}
			}
			//Get Nutrition Data
			$nutriData = array();
			$queryNut = "SELECT `nutrition`, `id` FROM `[1]`.`forced_modifiers` WHERE `id` IN ('".implode("','", $modidAry)."') ";
			$queryexeNut = mlavu_query($queryNut, $data_name);
			if (mysqli_num_rows ( $queryexeNut )) {
				while($rownuti = mysqli_fetch_assoc($queryexeNut))
				{
					$nutriData[$rownuti['id']] = $rownuti['nutrition'];
				}
			}
			//Get Order Details Based On Opened Date
			$queryOrd = "SELECT ms.order_id, ordc.item, ordc.item_id, ms.mod_id, ms.qty, ms.type, ms.list_id, ords.closed, ordc.device_time FROM
					`$data_name`.`orders` as ords LEFT JOIN  `$data_name`.`order_contents` as ordc ON ords.order_id = ordc.order_id LEFT JOIN
					`$data_name`.`modifiers_used` as ms ON ms.icid = ordc.icid WHERE ords.opened >= '[1]' AND ords.opened <= '[2]' AND ms.type =
					'forced' ";
			$orderQuery = mlavu_query($queryOrd, $start, $end);
			$orderrow = '';
			if (mysqli_num_rows ( $orderQuery )) {
				while($orderrow = mysqli_fetch_assoc($orderQuery))
				{
					$quantity = '';
					$quantity = $orderrow['qty'];
					for($i = 0; $i<$quantity; $i++) {
					$orderdata = array();
					$orderdata['site'] = $siteid;
					$orderdata['UPC'] = $upcData[$orderrow['item_id']];
					$orderdata['timestamp'] = date("d/m/Y H:i:s", strtotime($orderrow['device_time']));
					$orderdata['order_id'] = $orderrow['order_id'];
					$orderdata['item'] = $orderrow['item'];
					$orderdata['item_id'] = $orderrow['item_id'];
					$orderdata['mod_id'] = $orderrow['mod_id'];
					$orderdata['type'] = $orderrow['type'];
					$orderdata['mod_list_id'] = $orderrow['list_id'];
					if ($nutriData[$orderrow['mod_id']] != '') {
					$nutritionall = explode(',', $nutriData[$orderrow['mod_id']]);
					$nutriCount = count($nutritionall);
					for($j = 0; $j < $nutriCount; $j++) {
						$nutrition = '';
						$nutritionname = '';
						$nutrition = explode(' - ', $nutritionall[$j]);
						$nutritionname = $nutrition[0];
						$nutritionqty = $nutrition[1];
						if (in_array($nutritionname, $nutritionAry)) {
							$orderdata['nutrition'] = $nutritionname;
							$orderdata['qty'] = $nutritionqty;
							array_push($orderdataAry, $orderdata);
						}
					}
					}
					}
			    }
			}
			$rowgroup = '';
			//Get menu items for only Draft beer menu group
			$menudataQuery = mlavu_query("SELECT mi.id FROM `$data_name`.`menu_groups` as mg
							 LEFT JOIN `$data_name`.`menu_categories` as mc ON mc.`group_id`=mg.id
							 LEFT JOIN `$data_name`.`menu_items` as mi ON mi.`category_id` = mc.id
							 WHERE mg.`group_name`='Draft Beer' AND mc._deleted=0 AND mi._deleted=0");
			if (mysqli_num_rows ( $menudataQuery )) {
			while($rowgroup = mysqli_fetch_assoc($menudataQuery))
			{
				$grp = array();
				$grp['item_id'] = $rowgroup['id'];
				array_push($groupitemary, $grp);
			}
		    }
		    $result_array = array();
		    //Filter the Draft Beer Menu Group Orders using nutrition Pour type
		    if (!empty($orderdataAry) && !empty($groupitemary)) {
		    $result_array = array_intersect_assoc($orderdataAry, $groupitemary);
			}
			$orderdataAry = array();
		    foreach ($result_array as $key => $value) {
			$item = '';
			$item = trim('"'.$value['item'].'"');
			$csvDataRow .= "\n".$value['site'].",".$value['timestamp'].",".$value['UPC'].",".$item.",".$value['qty'];
		    }

		}
		//Crreate Dir Data If Not Found
		if (!is_dir(__DIR__."/data")) {
			mkdir(__DIR__."/data", 0775);
		}
		//File Name
		$file = __DIR__.'/data/'.$filename;
		//Save Generatedd CSV File
		$isSaved = file_put_contents($file, $csvDataRow);
		//Check Is File Saved
		if ($isSaved) {
			return array($filename, $file);
		} else {
			return false;
		}
	}

	/**
     * It will grab Csv and Sent it to the FTP Server
     * @param String $filename String $file
     * @return Boolean $isSent
     */
	public static function sendFTP($filename, $file) {
	//Connection Settings
	$isSent = false;
	$ftp_server = getenv('DUDE_FTP_SERVER'); // Address of FTP server.
	$ftp_user_name = getenv('DUDE_FTP_USER'); // Username
	$ftp_user_pass = getenv('DUDE_FTP_PWD'); // Password
	$ftp_user_pass = str_replace(':', '$', $ftp_user_pass);
	$ftpLoginMsg = "";
	$ftpUploadMsg = "";
	$ftpLoginMsg = "DUDE BREW : ";
	$ftpUploadMsg = $ftpLoginMsg;
	if($ftp_server != '' && $ftp_user_name != '' && $ftp_user_pass != '') {
	//FTP Connect Details
	$connId = ftp_connect($ftp_server, 21) or die(error_log('DUDE BREW : Couldnt connect to '.$ftp_server));
	//FTP Login
	$login_result = ftp_login($connId, $ftp_user_name, $ftp_user_pass) or die(error_log('DUDE BREW : You do not have access to this ftp server'));   // login with username and password, or give invalid user message

	if ((!$connId) || (!$login_result)) {  // check connection
    // wont ever hit this, b/c of the die call on ftp_login
	$ftpLoginMsg .= "FTP connection has failed!, Attempted to connect to ".$ftp_server." for user ".$ftp_user_name;
	} else {
	$ftpLoginMsg .= "Connected to DUDE BREW FTP SERVER : ".$ftp_server. " for user ".$ftp_user_name;
	}
	error_log($ftpLoginMsg);

	ftp_pasv($connId, true);
	$upload = ftp_put($connId, $filename, $file, FTP_ASCII);  // upload the file
	if (!$upload) {  // check upload status
		$ftpUploadMsg .= "FTP upload of ".$filename." has failed!";
	} else {
		$isSent = true;
		$ftpUploadMsg .= "Uploading ".$filename." Completed Successfully!";
	}
	error_log($ftpUploadMsg);
	ftp_close($connId); // close the FTP stream
	} else {
		error_log($ftpLoginMsg."Credentials Not Found..!!");
	}

	return $isSent;
	}

}

//Feature Flag DUDE_ENABLED
$isEnabled = '';
$isEnabled = getenv('DUDE_ENABLED');
$iniMsg = "DUDE BREW : ";
//EST Timezone
date_default_timezone_set('America/New_York');
//Check DUDE_ENABLED flag is Active or Inactive in Environment Configuration
if ($isEnabled) {
	error_log($iniMsg."Cron Initiated At -> ".date('Y-m-d H:i:s'));
	DudeBrewExport::doProcess();
} else {
	error_log($iniMsg. "Cron Not Initiated At -> ".date('Y-m-d H:i:s')." As DUDE ENABLED Flag Is Disabled");
}

?>