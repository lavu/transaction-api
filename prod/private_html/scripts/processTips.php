<?php
set_time_limit(900);
ignore_user_abort (true);

if($argv[1]=='' || $argv[1] != 'c045e90e4077b3233a6f6e3c892e9feb'){
echo "Unauthorized Access.";
exit;
}

//OM including this file for more functions
require_once ("/home/poslavu/public_html/admin/lib/gateway_functions.php");
require_once ("/home/poslavu/public_html/admin/lib/gateway_lib/paypal_func.php");

class processTips {
	public static function getUnprocessedTips($status = "pending") {
		$query = "select id, dataname, order_id, transaction_info,response, status, created_date, updated_date from `cc_tip_transactions` where status='[1]' order by created_date asc limit 10";
		$queryResult = mlavu_query ( $query, $status );
		$noOfTipsCount = mysqli_num_rows ( $queryResult );
		if ($noOfTipsCount > 0) {
			while ( $tipRecord = mysqli_fetch_assoc ( $queryResult ) ) {
				if (isset ( $tipRecord ['dataname'] )) {
					$updateQuery = "Update cc_tip_transactions set status='processing' where id = '[1]' and order_id = '[2]'";
					$updateQueryResult = mlavu_query ( $query, $tipRecord ['id'], $tipRecord ['order_id'] );
					$decodeTransInfo = json_decode ( $tipRecord ['transaction_info'] );
					$response = self::performTipTransaction ( $tipRecord ['dataname'], ( array ) $decodeTransInfo->process_info, ( array ) $decodeTransInfo->location_info, ( array ) $decodeTransInfo->transaction_info, $decodeTransInfo->tip_amount, $decodeTransInfo->last_tip );
					self::updateDBRecord ( $tipRecord ['id'], $response, "done" );
				}
			}
		} else {
			error_log ( "No Transaction found!" );
		}
	}
	
	private static function updateDBRecord($id, $response, $status) {
		$dateUpdated = date ( "Y-m-d H:i:s" );
		$query = "update `cc_tip_transactions` set  `response`='[1]', status='[2]', updated_date = '[3]' where id = '[4]'";
		$queryResult = mlavu_query ( $query, $response, $status, $dateUpdated,  $id);
	}
	
	private static function getPayPalMerchantEmail($locationDb) {
		$paypal_email = "";
		$enc_query = mlavu_query ( "select AES_DECRYPT(`integration1`,'[1]') as `integration1` from $locationDb.locations ", integration_keystr () );
		if (mysqli_num_rows ( $enc_query )) {
			$enc_read = mysqli_fetch_assoc ( $enc_query );
			$paypal_email = $enc_read ['integration1'];
		}
		return $paypal_email;
	}
	
	private static function performTipTransaction($dataName, $process_info, $location_info, $transaction_info, $tipAmount, $lastTip) {
		$order_id = $process_info ["order_id"];
		$transtype = "Adjustment";
		$transaction_info ['transtype'] = $transtype;
		$process_info = array_merge( $process_info, $transaction_info );
		
		$locationDb = "poslavu_" . $dataName . "_db";
		
		$responseText = "";
		
		$transaction_vars = generate_transaction_vars ( $process_info, $location_info, $process_info ['card_number'], getCardTypeFromNumber ( $process_info ['card_number'] ), "Sale", "", $process_info ['set_pay_type'], $process_info ['set_pay_type_id'], $process_info ['card_amount'] );
		
		if (! isset ( $dataName )) {
			$responseText = "0|Merchant Information Unavailable, please try processing via the PayPal button.";
			error_log ( $responseText );
			return $responseText;
		}
		
		$_REQUEST ['dataname'] = $dataName;
		
		$paypal_email = self::getPayPalMerchantEmail ( $locationDb );
		
		if (trim ( $paypal_email ) == "" || strpos ( $paypal_email, "@" ) === false || strpos ( $paypal_email, "." ) === false) {
			$responseText = "Incomplete Setup";
			error_log ( "Incomplete Setup:==>" . $dataName );
			return $responseText;
		}
		
		// set Transaction ID
		$cleanupRequired = false;
		$txnid = isset ( $process_info ['authcode'] ) ? $process_info ['authcode'] : (isset ( $process_info ['pnref'] ) ? $process_info ['pnref'] : "");
		$record_no = "";
		$authcode = "";
		
		// getting the transaction row if it exists, generating the row if it does not exist and we need to create it
		$transactionRow = mlavu_query ( "select * from $locationDb.cc_transactions where `order_id`='[1]' and (`preauth_id`='[2]' OR `transaction_id`='[2]' OR `auth_code`='[2]') and `check`='[3]' order by id desc limit 1", $order_id, $txnid, $process_info ['check'] );
		$transactionArray = mysqli_fetch_assoc ( $transactionRow );
		$txn_system_id = isset ( $transactionArray ['id'] ) ? $transactionArray ['id'] : "";
		$preauthID = isset ( $transactionArray ['preauth_id'] ) ? $transactionArray ['preauth_id'] : "";
		$authcode = isset ( $transactionArray ['auth_code'] ) ? $transactionArray ['auth_code'] : "";
		$txnid = isset ( $transactionArray ['transaction_id'] ) ? $transactionArray ['transaction_id'] : $txnid;
		$process_info ['transtype'] = $transaction_info ['transtype'];

		if (isset($transactionArray['api']) && $transactionArray['api'] == 'papi') {
			require_once (__DIR__."/../../public_html/inc/papi/papiApi.php");
			$papiRequest['transtype'] = $transtype;
			$papiRequest['tipAmount'] = $tipAmount;
			$papiRequest['locationDb'] = $locationDb;
			$papiRequest['authcode'] = $authcode;
			$papiRequest['txnid'] = $txnid;
			$args['dataname'] = $dataName;
			$args['locationid'] = $transactionArray ['loc_id'];
			$papiApiObj = new papiApi($args);
			$response = $papiApiObj->papiTransaction($transactionArray, $papiRequest);
			$message = $response;
			$rspvars = json_decode ( $response, true );
			if (isset($rspvars['status']) && $rspvars['status'] == 'C') {
				$record_no = $rspvars['gateway']['data']['invoiceId'];
				$ReturnID = $rspvars['id'];
				paypal_update_transaction_record ( $process_info, $location_info, $ReturnID, $txnid, $txn_system_id, "" );
				mlavu_query ( "UPDATE $locationDb.`cc_transactions` SET `preauth_id`='[1]',`tip_amount` = '[2]',`auth` = 0, `ref_data` = 'Success|Processed', `temp_data` = '[3]', `record_no` = '[5]' WHERE `id` = '[4]'", $ReturnID, $tipAmount, $message, $txn_system_id, $record_no );
			} else {
				mlavu_query ( "UPDATE $locationDb.`cc_transactions` SET `temp_data` = '[1]',tip_amount = '[2]' WHERE `id` = '[3]'", $message."|Processed", $lastTip, $txn_system_id );
			}
		} else {
			$paypal_email = self::getPayPalMerchantEmail ( $locationDb );
			if (trim ( $paypal_email ) == "" || strpos ( $paypal_email, "@" ) === false || strpos ( $paypal_email, "." ) === false) {
				$responseText = "Incomplete Setup";
				error_log ( "Incomplete Setup:==>" . $dataName );
				return $responseText;
			}
			switch ($transtype) {
				case "Adjustment" :
				case "PreAuthCapture" :
					$captureID = empty ( $authcode ) ? (empty ( $txnid ) ? "" : $txnid) : $authcode;
					if (empty ( $captureID )) {
						return "0|Unable to retrieve transaction identifier";
					}
					$base_amount = $transactionArray ['amount'];
					$card_amount = str_replace ( ",", "", number_format ( $base_amount * 1 + $tipAmount * 1, 2 ) );
					$fields = '{"amount":{' . '"currency":"USD",' . '"total":[amount]},' . '"is_final_capture":"false"}';
					$fields = str_replace ( "[amount]", $card_amount, $fields );
					$curlURL = "v1/payments/authorization/$captureID/capture";
					break;
			default :
				return "0|Unsupported Transaction Type";
			}
			// Perform the transaction
			$core = new PayPalCore ();
			$PayPalPaymentHandler = new PayPalPaymentHandler ( $dataName );
			$response = $PayPalPaymentHandler->makePayment ( $fields, $core->PayPalEndpointURL . $curlURL );
			$rspvars = json_decode ( $response, true );
			$txn = (isset ( $rspvars ['transactionNumber'] )) ? $rspvars ['transactionNumber'] : "";
			$rcode = (isset ( $rspvars ['responseCode'] )) ? $rspvars ['responseCode'] : "";
			$message = (isset ( $rspvars ['message'] )) ? $rspvars ['message'] : "No Message";
			$state = (isset ( $rspvars ['state'] )) ? $rspvars ['state'] : "";
			$ReturnID = empty ( $rspvars ['id'] ) ? "" : $rspvars ['id'];
			$errorType = empty ( $rspvars ['errorType'] ) ? "" : $rspvars ['errorType'];
			$rspstr = "No Recognized Response";
			$approved = true;
			$rspstr = "Approved";
			/*
			* transactionNumber = auth_code
			* id = refund_pnref or void_pnref
			* When Adjusting or capturing, the id will populate the preauth_id field
			*/
			if ($rcode === "0000") {
				paypal_update_transaction_record ( $process_info, $location_info, $txn, $txn, $txn_system_id, "" );
			}
			else if ($state === "completed" && ($transtype === "Adjustment" || $transtype === "PreAuthCapture")) {
				paypal_update_transaction_record ( $process_info, $location_info, $ReturnID, $txnid, $txn_system_id, "" );
				mlavu_query ( "update $locationDb.cc_transactions set `preauth_id`='[1]',`tip_amount`='[2]',`auth`=0, `temp_data`='Success|Processed', `ref_data`='[3]' where `id` = '[4]'", $ReturnID, $tipAmount, $message, $txn_system_id );
				$message='Success';
			}
			else {
				$approved = false;
				switch ($errorType) {
					case "invalid_cvv" :
						$message = "Invalid CVV, double check manual entry.";
						break;
					case "service_error" :
						$message = "PayPal Server Error.  If this problem persists, please contact PayPal Support";
						break;
					case "pay/bad_card" :
						$message = "Invalid Card Data.  Please retry.";
						break;
					default :
						$message = $rspvars ['message'];
						break;
				}
				$rspstr = "Declined\n$message";
				if ($cleanupRequired && $transtype === "Sale" || $transtype === "Auth") {
					mlavu_query ( "DELETE FROM $locationDb.cc_transactions WHERE `id` = '[1]'", $txn_system_id );
				}
				else{
					mlavu_query ( "update $locationDb.cc_transactions set `temp_data`='[1]',tip_amount='[2]' where `id` = '[3]'", $message."|Processed", $lastTip, $txn_system_id );
				}
			}
		}
		return $message;
	}

    public static function format_paypal_gateway_response($rvars, $process_info)
    {
        $lastModTS = (empty($process_info['last_mod_ts'])) ? "" : $process_info['last_mod_ts'];
        $rtn = array();
        if(isset($rvars['approved']) && $rvars['approved'] === true)
        {
            $rtn[] = "1"; // 0
            $rtn[] = $rvars['txnid']; //transaction ID
            $rtn[] = $rvars['last_four'];
            $rtn[] = $rvars['message'];;
            $rtn[] = $rvars['txnid'];
            $rtn[] = getCardTypeFromNumber($process_info['card_number']);
            $rtn[] = "";
            $rtn[] = $process_info['card_amount'];
            $rtn[] = $process_info['order_id'];
            $rtn[] = "";
            $rtn[] = ""; // 10
            $rtn[] = "";
            $rtn[] = "";
            $rtn[] = "";
            $rtn[] = $lastModTS;
        }
        else
        {
            $rtn[] = "0";
            $rtn[] = $rvars['message'];
            $rtn[] = $rvars['txnid'];
            $rtn[] = $rvars['order_id'];
            $rtn[] = "";
            $rtn[] = (isset($rvars['title']))?$rvars['title']:"Unable to Process";
        }

        return implode("|",$rtn);
    }
}

processTips::getUnprocessedTips ();
