<?php

/**
 *  Developer : Dhruvpalsinh D Chudasama
*  Date      : 10-01-2018
*  Purpose   : It is cron script,which will generate cvs from 1.0 to 4.0 & 2.0 to 4.0 in background
*/

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

class InventoryMigrationCron
{

	public static function initiateMigration()
	{
		$isCLI = ( php_sapi_name() == 'cli' );
		if($isCLI)
		{
			$tdataname = getopt("d:t:");
			
			if(count($tdataname) > 0)
			{
				$dataname = '';
				$version = '';
				if(isset($tdataname['d']))
				{
					$dataname = $tdataname['d'];
				}	
				if(isset($tdataname['t']))
				{
					$version = $tdataname['t'];
				}	
				
				if($dataname != '' && $version != '')
				{
					$vary = array('v1','v2');
					
					if(in_array($version,$vary))
					{
						self::runMigration($dataname,$version);
						
						echo "\n message1 : Please check generated csv in private_html/scripts location \n \n";
					}
					else
					{
						echo "\n message1 : Version Type ".$version." Does Not Matched \n";
						echo "\n \n message2 : Please Provide Arguments Data Name And Version Type Like : -d data_name -t version_type Where data_name values should be like 'demo_sajeed_di' & version_type values should be v1,v2.Note : v1 - Migrate from 1.0 to 4.0,v2 - Migrate from 2.0 to 4.0 \n\n";
						echo "\n message3 : Sample Run : php inventoryMigration.php -d demo_sajeed_di -t v1 \n \n";
					}	
				}
				else
				{
					echo "\n message1 : Please Provide Arguments Data Name & Version Type Every Time \n";
					echo "\n \n message2 : Please Provide Arguments Data Name And Version Type Like : -d data_name -t version_type Where data_name values should be like 'demo_sajeed_di' & version_type values should be v1,v2.Note : v1 - Migrate from 1.0 to 4.0,v2 - Migrate from 2.0 to 4.0 \n\n";
					echo "\n message3 : Sample Run : php inventoryMigration.php -d demo_sajeed_di -t v1 \n \n";
				}	
			}
			else
			{
				echo "\n \n message1 : Please Provide Arguments Data Name And Version Type Like : -d data_name -t version_type Where data_name values should be like 'demo_sajeed_di' & version_type values should be v1,v2.Note : v1 - Migrate from 1.0 to 4.0,v2 - Migrate from 2.0 to 4.0 \n\n";
				echo "\n message2 : Sample Run : php inventoryMigration.php -d demo_sajeed_di -t v1 \n \n";
			}	
		}
		else 
		{
			$status = 'pending';
			$getDataname = mlavu_query("select dataname, type and status FROM `poslavu_MAIN_db`.`inventorymigratecron` WHERE `status` ='[1]'",$status);
			
			while($row = mysqli_fetch_assoc($getDataname))
			{
				self::runMigration($row['dataname'],$row['type']);
			}
		}	
	}

	public static function runMigration($dataname,$type)
	{
		spl_autoload_register(function ($class_name) {
			include $class_name . '.php';
		});
		
			$v1 = 'v1';
			$v2 = 'v2';
			
			if(strcasecmp($type,$v1) == 0)
			{
				$m1t4 = new inventoryMigration_V1T4($dataname);
			}
			if(strcasecmp($type,$v2) == 0)
			{
				$m2t4 = new inventoryMigration_V2T4($dataname);
			}
	}
}

InventoryMigrationCron::initiateMigration();

?>