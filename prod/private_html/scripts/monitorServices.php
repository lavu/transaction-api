<?php
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
Class MonitorServices {
	
	private function get_processtips() {
		$query = mlavu_query("select count(*) as `pendingTransaction`  from `cc_tip_transactions` where status='pending' and `created_date` >= DATE_SUB(NOW(), INTERVAL 6 Hour)");
		$rows = mysqli_fetch_assoc($query);
		$returnArr = array();
		$data = array();
		$pendingTransactions = $rows['pendingTransaction'];
		$reponse = self::uniqueResponses();
		$lastsuccessQuery = mlavu_query("select `created_date` from `cc_tip_transactions` where `status` = 'done' and `response` = 'Success' order by id desc limit 1");
		$successData = mysqli_fetch_assoc($lastsuccessQuery);
		$getDateTime = self::getTimelastPending();
		$lastSuccessTime = $successData['created_date'];
		$first_date = new DateTime($getDateTime);
		$second_date = new DateTime($lastSuccessTime);
		$interval = $first_date->diff($second_date);
		$interval->format('%d days %h hours %i minutes');
		if ($pendingTransactions == 0) {
			$status = 'up';
			$data['pending'] = $pendingTransactions;
			$data['last_successful_transaction'] = $lastSuccessTime;
			$data['last24Hrs'] = $reponse;
		} else {
			if (($interval->format('%h') > 1 || $interval->format('%i') > 10)) {
				$status = 'down';
				$data['last_successful_transaction'] = $lastSuccessTime;
				$data['pending'] = $pendingTransactions;
				
			} else {
				$status = 'up';
				$data['pending'] = $pendingTransactions;
				$data['last_successful_transaction'] = $lastSuccessTime;
				$data['last24Hrs'] = $reponse;
				
			}
		}
		$returnArr = array (
					"status" => $status,
					"data" => $data
		);
		
		return $returnArr;
	}
	
	private function uniqueResponses() {
		$uniqueResponseQuery = mlavu_query("select response, count(*) as 'responseCount' from `cc_tip_transactions` where `created_date` >= (NOW() - INTERVAL 24 Hour)  group by response");
		while ($responseData = mysqli_fetch_assoc($uniqueResponseQuery)) {
			$reponse [] = $responseData;
		}
		return $reponse;
	}
	
	private function getTimelastPending() {
		$uniqueResponseQuery = mlavu_query("select `created_date` from `cc_tip_transactions` where `status`='pending' and `created_date` >= (NOW() - INTERVAL 24 Hour)  order by id asc limit 1");
		$responseData = mysqli_fetch_assoc($uniqueResponseQuery);
		return $responseData['created_date'];;
	}
	
	private function get_lavutogostatus() {
		$todayDt = date('Y-m-d');
		$query = mlavu_query("select count(*) as `orderCount`  from `lavutogo`.`orders` where DATE(`created_at`)='$todayDt'");
		if (!empty($query)) {
			$rows = mysqli_fetch_assoc($query);
			$status = 'up';
			$data['order_count'] = $rows['orderCount'];
		} else {
			$status = 'down';
			$data['order_count'] = 0;
		}
		
		$returnArr = array (
				"status" => $status,
				"data" => $data
		);
		return $returnArr;
	}
	
	private function get_lavutogostatusbydn($dataName) {
		$dbName = 'poslavu_'.$dataName.'_db';
		$todayDt = date('Y-m-d');
		$query = mlavu_query("select count(*) as `orderCount`  from `$dbName`.`orders` where DATE(`closed`)='$todayDt'");
		if (!empty($query)) {
			$rows = mysqli_fetch_assoc($query);
			$status = 'up';
			$data['order_count'] = $rows['orderCount'];
		} else {
			$status = 'down';
			$data['order_count'] = 0;
		}
	
		$returnArr = array (
				"status" => $status,
				"data" => $data
		);
		return $returnArr;
	}
	
	private function get_adminhealth() {
		$services = array();
		$serviceStatus = array();
		$status = 'up';
		$services[] = array("port" => "80", "service" => "Apache", "ip" => "localhost") ;
		foreach ($services  as $service) {
			$fp = @fsockopen($service['ip'], $service['port'], $errno, $errstr, $timeout);
			if (!$fp) {
				$status = 'down';
				$serviceStatus[$service['service']] = 'Offline';
			} else {
				$serviceStatus[$service['service']] = 'Online';
				fclose($fp);
			}
		}
		$serviceStatus['MYSQL'] = self::checkMysqlConnection();
		$disks[] = array("path" => getcwd()) ;
		$diskSpace = self::get_disk_free_status($disks);
		$data['services'] = $serviceStatus;
		$data['diskSpace'] = $diskSpace;
		$returnArr = array (
				"status" => $status,
				"data" => $data
		);
		return $returnArr;
	}
	
	private function getSymbolByQuantity($bytes) {
		$symbol = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB');
		$exp = floor(log($bytes)/log(1024));
		return sprintf('%.2f '.$symbol[$exp], ($bytes/pow(1024, floor($exp))));
	}
	
	private function get_disk_free_status($disks) {
		$dataArr = array();
		$max = 5;
		foreach($disks as $disk){
			if(strlen($disk["name"]) > $max)
				$max = strlen($disk["name"]);
		}
		$number_length = 0;	
		foreach($disks as $disk){
			$disk_space = disk_total_space($disk["path"]);
			$disk_free = disk_free_space($disk["path"]);
			$disk_free_precent = round($disk_free*1.0 / $disk_space*100, 2);
			$dataArr['disk_space'] =  str_pad(self::getSymbolByQuantity($disk_space),  $number_length, " ");
			$dataArr['disk_free']  =  str_pad(self::getSymbolByQuantity($disk_free),  $number_length, " ");
			$dataArr['disk_free_precent'] =  str_pad($disk_free_precent,  $number_length, " ");
		}
		return $dataArr;
	}
	
	private function checkMysqlConnection() {
		require_once('/home/poslavu/private_html/myinfo.php');
		$clientConnection = mysqli_connect(my_poslavu_host(), my_poslavu_username(), my_poslavu_password());
		if(empty($clientConnection)) {
			$status = 'Offline';
		} else {
			$status = 'Online';
		}
		return $status;
	}
	
}