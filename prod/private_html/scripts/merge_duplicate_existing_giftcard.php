<?php
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');

ini_set("display_errors",0);
error_reporting(0);

#Find all active restaurants chains
$chainListObj = mlavu_query("SELECT id as chainid, primary_restaurantid FROM `restaurant_chains` WHERE `_deleted` = 0 ORDER BY id DESC");

while($row = mysqli_fetch_assoc($chainListObj) ) {
    $chainid = $row['chainid'];
    $chainedRestaurants =get_restaurants_by_chain($chainid );	#get all chained restaurants
    
    $chainedDataname = array();
    $existingChainDataname = array();
    foreach($chainedRestaurants as $restaurants ) {
        if($row['primary_restaurantid'] == $restaurants['id']) {
            $existingChainDataname[] = $restaurants['data_name'];   #get primary restaurants from chain
            continue;
        } 
        $chainedDataname[] = $restaurants['data_name']; # get all non-primary restaurants from chain
    }
   
    #find duplicate gift card and merge if its exist
    foreach($chainedDataname as $dataname ){
        addRemoveChainedGiftCard($chainid, $dataname, $existingChainDataname );
        $existingChainDataname[] = $dataname;   
    }
}
?>
