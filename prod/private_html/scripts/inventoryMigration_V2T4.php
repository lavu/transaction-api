<?php

/**
 * Developer : Dhruvpalsinh D Chudasama
* Date       : 09-01-2018
* Purpose    : To Migrate Data to Csv From V 2.0 to 4.0
*/

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

class inventoryMigration_V2T4
{
	private $data_namemain;

	public function __construct($dataname)
	{
		$data_namemain = 'poslavu_'.$dataname.'_db';

		$tname = array('VENDOR2.0T4.0','ITEM2.0T4.0');

		for($t=0;$t<count($tname);$t++)
		{
			if($tname[$t] == 'VENDOR2.0T4.0')
			{
				//$vendor2t4 = array('name','address','blank','city','stateProvinceRegion','zipcode','country','blank','blank','blank','blank','blank','blank','blank','blank','blank','contactName','contactPhone','contactEmail','blank','blank','','','','','','','id','createdDate');
				$vendor2t4 = array('name','address','blank','city','stateProvinceRegion','zipcode','country','blank','blank','blank','blank','blank','blank','blank','blank','blank','contactName','contactPhone','contactEmail','blank','blank');
				$queryResult = mlavu_query("select name,address,city,stateProvinceRegion,zipcode,country,city,contactPhone,contactEmail,contactFax,contactName,createdDate,id FROM ".$data_namemain.".vendors");

				$this->exportDataV24($dataname,$queryResult,$tname[$t],$vendor2t4);
			}
			else if($tname[$t] == 'ITEM2.0T4.0')
			{
				$item2 = array('Name','Blank','storageLocationName','quantityOnHand','Blank','Blank','Blank','Blank','recentPurchaseUnit','primaryVendor','Blank','Blank','Blank','Blank','Blank','categoryName','Blank','Blank','Blank','lowQuantity','Blank');

				//$queryResult = mlavu_query("select name, storageLocation,quantityOnHand,(SELECT name FROM ".$data_namemain.".unit_category where id=ii.recentPurchaseUnitID) as recentPurchaseUnitID,(SELECT name FROM ".$data_namemain.".vendors where id=ii.primaryVendorID) as primaryVendorID,categoryID,lowQuantity FROM ".$data_namemain.".inventory_items as ii");
				$queryResult = mlavu_query("select name,(SELECT name FROM ".$data_namemain.".storage_locations WHERE id=ii.storageLocation) as storageLocation,quantityOnHand,(SELECT name FROM ".$data_namemain.".standard_units_measure where id=ii.recentPurchaseUnitID) as recentPurchaseUnitID,(SELECT name FROM ".$data_namemain.".vendors where id=ii.primaryVendorID) as primaryVendorID,(SELECT name FROM ".$data_namemain.".inventory_categories where id=ii.categoryID) as categoryID,lowQuantity FROM ".$data_namemain.".inventory_items as ii");

				$this->exportDataV24($dataname,$queryResult,$tname[$t],$item2);
			}
		}
	}

	private function exportDataV24($dataname,$query,$table,$columns)
	{
		$filename = $dataname.'_'.$table.'.csv';
		$path = __DIR__ . DIRECTORY_SEPARATOR.$filename;
		$output = '';
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$filename);
		$output = fopen($path, "w");
		fputcsv($output,$columns);
		while($row = mysqli_fetch_assoc($query))
		{
			if($table == 'VENDOR2.0T4.0')
			{
				//fputcsv($output, array($row['name'],$row['address'],'',$row['city'],$row['stateProvinceRegion'],$row['zipcode'],$row['country'],'','','','','','','','','',$row['contactName'],$row['contactPhone'],$row['contactEmail'],'','','','','','','','',$row['id'],$row['createdDate']));
				fputcsv($output, array($row['name'],$row['address'],'',$row['city'],$row['stateProvinceRegion'],$row['zipcode'],$row['country'],'','','','','','','','','',$row['contactName'],$row['contactPhone'],$row['contactEmail'],'',''));
			}
			else if($table == 'ITEM2.0T4.0')
			{
				fputcsv($output, array($row['name'],'',$row['storageLocation'],$row['quantityOnHand'],'','','','',$row['recentPurchaseUnitID'],$row['primaryVendorID'],'','','','','',$row['categoryID'],'','','',$row['lowQuantity'],''));
			}
		}
		fclose($output);
	}
}

?>