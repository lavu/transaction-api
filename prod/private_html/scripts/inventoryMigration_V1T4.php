<?php

/**
 * Developer : Dhruvpalsinh D Chudasama
* Date       :  09-01-2018
* Purpose    : To Migrate Data to Csv From V 1.0 to 4.0
*/

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

class inventoryMigration_V1T4
{
	private $data_namemain;

	public function __construct($dataname)
	{
		$data_namemain = 'poslavu_'.$dataname.'_db';

		$tname = array('ITEM1.0T4.0','VENDOR1.0T4.0','INVENTORYITEM1.0T4.0');

		for($t=0;$t<count($tname);$t++)
		{
			if($tname[$t] == 'ITEM1.0T4.0')
			{
				$item1 = array('Ingredient','Blank','Blank','Quantity','Blank','Blank','Blank','Blank','Unit','Blank','Blank','Blank','Blank','Blank','Blank','List Name','Blank','Blank','Par','Low Alert Level','Blank');

				$queryResult = mlavu_query("SELECT title,qty,unit,low FROM ".$data_namemain.".ingredients");

				$this->exportDataV14($dataname,$queryResult,$tname[$t],$item1);
			}
			else if($tname[$t] == 'VENDOR1.0T4.0')
			{
				$vendor1 = array('Vendor name', 'Address line 1','Address line 2','City','State','Zip','Country','Billing Address line 1','Billing Address 2','City','State','Zip','Country','Vendor phone','Vendor email','Vendor fax','Representative name','Representative phone','Representative email','Delivery days (seperated by comma)','Lead time');
				
				$queryResult = mlavu_query("select
				 name,city,stateProvinceRegion,zipcode,country,contactPhone,contactEmail,contactFax,contactName FROM ".$data_namemain.".vendors");
				
				$this->exportDataV14($dataname,$queryResult,$tname[$t],$vendor1);
			}
			else if($tname[$t] == 'INVENTORYITEM1.0T4.0')
			{
				$invitem1 = array('Item name','Status','Storage location 1 name','Storage location 1 qty (Primary)','Storage location 2 name','Storage location 2 qty','Storage location 3 name','Storage location 3 qty','Storage Unit of Measure','Vendor 1 name (Primary)','Vendor 1 sku','Vendor 2 name','Vendor 2 sku','Vendor 3 name','Vendor 3 sku','Category','Tags(comma seperate multiple)','UPC','PAR','Low stock alert','Shelf life (in days)');
				
				$queryResult = mlavu_query("SELECT name,(SELECT name FROM ".$data_namemain.".storage_locations where id=ii.storageLocation) as storage_location,(SELECT name FROM ".$data_namemain.".vendors where id=ii.primaryVendorID) as vendor,(SELECT SKU FROM ".$data_namemain.".vendor_items where vendorID=ii.primaryVendorID AND inventoryItemID=ii.id) as SKU,(SELECT name FROM ".$data_namemain.".inventory_categories where id=ii.categoryID) as category,lowQuantity FROM ".$data_namemain.".inventory_items as ii");
				
				$this->exportDataV14($dataname,$queryResult,$tname[$t],$invitem1);
			}	
		}
	}

	private function exportDataV14($dataname,$query,$table,$columns)
	{
		$filename = $dataname.'_'.$table.'.csv';
		$path = __DIR__ . DIRECTORY_SEPARATOR.$filename;
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$filename);
		$output = fopen($path, "w");
		fputcsv($output,$columns);
		while($row = mysqli_fetch_assoc($query))
		{
			if($table == 'ITEM1.0T4.0')
			{
				fputcsv($output, array($row['title'],'','',$row['qty'],'','','','',$row['unit'],'','','','','','','','','','',$row['low'],''));
			}
			else if($table == 'VENDOR1.0T4.0')
			{
				fputcsv($output, array($row['name'],'','',$row['city'],$row['stateProvinceRegion'],$row['zipcode'],$row['country'],'','',$row['city'],$row['stateProvinceRegion'],$row['zipcode'],$row['country'],$row['contactPhone'],$row['contactEmail'],$row['contactFax'],$row['contactName'],'','','',''));
			}
			else if($table == 'INVENTORYITEM1.0T4.0')
			{
				fputcsv($output, array($row['name'],'',$row['storage_location'],'','','','','','',$row['vendor'],$row['SKU'],'','','','',$row['category'],'','','',$row['lowQuantity'],''));
			}	
		}
		fclose($output);
	}
}

//$m14 = new inventoryMigration_V1T4('demo_sajeed_di');

?>