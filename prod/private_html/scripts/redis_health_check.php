<?php
/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 03/01/19
 * Time: 12:05 PM
 */

/**
 * This script is related to LP-8600.
 * It will check redis server availability, Check all sentinel server which has been configured into myinfo.php.
 * Get all alive redis sentinel server ips and update poslavu_MAIN_db.main_config table.
*/

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
require_once('/home/poslavu/public_html/admin/lib/redis/Sentinel.php');

class RedisHealthCheck {
    public static function healthCheck() {
        $redisHosts = explode(',', my_poslavu_redis_host());
        if (!empty($redisHosts)) {
            $aliveRedisIps = array();
            $sentinel = new \Jenner\RedisSentinel\Sentinel();
            foreach ($redisHosts as $redisHost) {
                $beat = $sentinel->connect($redisHost, my_poslavu_redis_port());
                if ($beat) {
                    $aliveRedisIps[] = $redisHost;
                }
            }
            if (!empty($aliveRedisIps)) {
                $aliveRedisIpsStr = implode(",", $aliveRedisIps);
                $setting = 'ALIVE_REDIS_SERVER';
                $queryResult = mlavu_query("SELECT id FROM main_config WHERE setting = '[1]'", $setting);
                if (mysqli_num_rows($queryResult) > 0) {
                    mlavu_query("UPDATE `main_config` set value = '[1]' WHERE `setting` = '[2]'", $aliveRedisIpsStr, $setting);
                } else {
                    mlavu_query("INSERT INTO main_config (id, setting, value, _deleted) VALUES(null, '[1]', '[2]', '0')", $setting, $aliveRedisIpsStr);
                }
            }
        }          
    }
}

// Call health check method
RedisHealthCheck::healthCheck();