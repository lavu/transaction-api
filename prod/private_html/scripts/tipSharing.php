<?php
require_once ("/home/poslavu/public_html/admin/lib/info_inc.php");
$serverId = $argv['1'];
$locationId = $argv['2'];
$dataName = 'poslavu_'.$argv['3'].'_db';
$locQuery = mlavu_query("select * from $dataName.locations where id='".$locationId."'");
$location_info = mysqli_fetch_assoc($locQuery);
/**
* tip_sharing table allow only 3 decimal values, if more then 3 decimal values are available, when insertion time it automatically rounded to 3 decimal places.
* later when compare with tip sharing values it will get amount mismatch issue.
* For calculation purpose we are tacking rounding value upto 3 decimal places.
*/
$decimalPlaces = ($location_info['disable_decimal'] > 3) ? 3 : $location_info['disable_decimal'];
$timezone = $location_info["timezone"];
$deviceTime = date('Y-m-d H:i:s');
if (! empty($timezone)) {
	$deviceTime = DateTimeForTimeZone($timezone);
}
/*
 * Updating emp_classes and its setting from emp_classes_temp
 */
$ids = '';
$empIds = mlavu_query("select `id` from $dataName.`emp_classes` where `_deleted` = 0 ");
while ($empid = mysqli_fetch_assoc($empIds)) {
	$ids .= $empid['id'].",";
}
$ids = rtrim($ids, ",");
$today_timestamp = date("Y-m-d");
if (! empty($timezone)) {
	$today_times = DateTimeForTimeZone($timezone);
	$today_times = explode(' ',$today_times);
	$today_timestamp = $today_times[0];
}
$empInfo_query = mlavu_query("select * from $dataName.`emp_classes_temp` where `_deleted` = 0 AND `class_updated_date` ='".$today_timestamp."' and `emp_class_id` in ($ids)");

while ($empData = mysqli_fetch_assoc($empInfo_query)) {
	$update_emp_class = "UPDATE $dataName.`emp_classes` SET `title` = '".$empData['title']."' ,`type` = '".$empData['type']."', `_deleted` = '".$empData['_deleted']."',`tipout_rules` = '".$empData['tipout_rules']."' ,`tiprefund_rules` = '".$empData['tiprefund_rules']."', `refund_type` = '".$empData['refund_type']."', `enable_tip_sharing` = '".$empData['enable_tip_sharing']."',`tip_sharing_rule` = '".$empData['tip_sharing_rule']."' ,`include_server_owes` = '".$empData['include_server_owes']."', `tip_sharing_period` = '".$empData['tip_sharing_period']."', `tipout_break` = '".$empData['tipout_break']."' where `id` = '".$empData['emp_class_id']."'";
	mlavu_query($update_emp_class);
		
	$update_emp_temp_class = "UPDATE $dataName.`emp_classes_temp` SET `_deleted` = 1 where `emp_class_id` = '".$empData['emp_class_id']."' AND `class_updated_date`  = '".$today_timestamp."'";
	mlavu_query($update_emp_temp_class);
}

/*
 * End of updating emp_classes
 */
$classIds = array();
if(is_numeric($serverId)) {
	$getUserRoleQuery = mlavu_query("SELECT `role_id` from $dataName.`users` WHERE `id` = '[1]'", $serverId);
	$userRoleInfo = mysqli_fetch_assoc($getUserRoleQuery);
	$userClasses = explode(",", $userRoleInfo['role_id']);
	foreach($userClasses as $classInfo) {
		$userClass = explode("|", $classInfo);
		$classIds[] = $userClass[0];
	}
	$classIdList = implode(",", $classIds);
	$get_server_info = mlavu_query( "SELECT `emp_classes`.`id` AS `class_id`, `emp_classes`.`title` AS `class_title`, `emp_classes`.`tipout_rules` AS `tipout_rules`, `emp_classes`.`enable_tip_sharing` AS `enable_sharing`, `emp_classes`.`tip_sharing_rule` AS `tip_sharing_rule`, `emp_classes`.`tip_sharing_period` AS `time_for_tip_sharing`,`emp_classes`.`tiprefund_rules` as `tiprefund_rules`,`emp_classes`.`refund_type` as `refund_type`  FROM $dataName.`emp_classes` LEFT JOIN $dataName.`clock_punches` on  `clock_punches`.`role_id` = `emp_classes`.`id`  where `clock_punches`.`server_id`= ".$serverId." AND `clock_punches`.`punched_out`= 0 AND `clock_punches`.`_deleted`= 0 AND `emp_classes`.`id` in (".$classIdList.") group by `emp_classes`.`id`");
}
else if ($serverId == 'all') {
	$get_server_info = mlavu_query("SELECT `emp_classes`.`id` AS `class_id`, `emp_classes`.`title` AS `class_title`, `emp_classes`.`tipout_rules` AS `tipout_rules`, `emp_classes`.`enable_tip_sharing` AS `enable_sharing`, `emp_classes`.`tip_sharing_rule` AS `tip_sharing_rule`, `emp_classes`.`tip_sharing_period` AS `time_for_tip_sharing`,`tiprefund_rules`,`refund_type` FROM $dataName.`emp_classes`");
}

if (mysqli_num_rows($get_server_info) > 0) {
	while ($server_info = mysqli_fetch_assoc($get_server_info)) {
		$total_tip_all_users = 0; 
			$device_time = (isset($deviceTime))?$deviceTime:date("Y-m-d H:i:s");
			if ($location_info['timezone'] != "") { 
				$device_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
			}
			$class_id = $server_info['class_id'];
			$time_for_tip_sharing = $server_info['time_for_tip_sharing'];
			$date_info = get_use_date_end_date($location_info, $device_time);
			$class_name = $server_info['class_title'];
			$tip_sharing_rule = $server_info['tip_sharing_rule'];
		
		if ($server_info['enable_sharing'] == 1) {
			$userdetail = array();
			if (! empty($timezone)) {
				$todayDate  = DateTimeForTimeZone($timezone);
			}
			$createdDate = $today_timestamp;
			$cashvalues = $cardvalues = 0;
			$tip=0;
			$time_shift = explode("|", $time_for_tip_sharing);
			if ($time_shift[0] != "ByShift") {
				$total_tip_all_users = getTotalTips($date_info, $locationId, $class_id, $dataName);
				$total_tip_all_users = $total_tip_all_users + tipsCashData($date_info, $locationId, $class_id, $dataName);
			}
			$uquery = mlavu_query("SELECT `users`.`id`, `users`.`f_name`,  `users`.`l_name`, `clock_punches`.`time` FROM $dataName.`users` LEFT JOIN $dataName.`clock_punches` on  `clock_punches`.`server_id` = `users`.`id` WHERE `clock_punches`.`role_id` = '".$class_id."' AND `clock_punches`.`punched_out`= 0 AND `users`.`_deleted` = 0 AND DATE_FORMAT(`clock_punches`.`time`, '%Y-%m-%d') = '$today_timestamp' group by `users`.`id`");
			$ucount = mysqli_num_rows($uquery);
			$sales = $uid = 0;

			if ($ucount > 0 ) {
				$resultData = array();
				$totalAmount = 0;
				$userCountNum = 0;
				//loop start here
				while ($result = mysqli_fetch_assoc($uquery)) {
					$uid = $result['id'];
					$dateHrs = $today_timestamp." 00:00:00";
					$tomDateHrs = date('Y-m-d',strtotime($today_timestamp . "+1 days"))." 00:00:00";
					$hrsCurrentUser = getUserTime($locationId, $uid, $dateHrs, $tomDateHrs, $todayDate, $dataName, $class_id, $decimalPlaces);
					if ($tip_sharing_rule == "Evenly") {
						$userQuery = mlavu_query("SELECT `users`.`id`, `users`.`f_name`,  `users`.`l_name`, `clock_punches`.`time` FROM $dataName.`users` LEFT JOIN $dataName.`clock_punches` on  `clock_punches`.`server_id` = `users`.`id` WHERE `clock_punches`.`role_id` = '".$class_id."' AND `users`.`_deleted` = 0 AND `clock_punches`.`punched_out`= 0 AND DATE_FORMAT(`clock_punches`.`time`, '%Y-%m-%d') = '$today_timestamp' group by `users`.`id`");

						if ($time_shift[0] == "ByShift") {
							$userCount = mysqli_num_rows($userQuery);
						} else {
							$userCount = mysqli_num_rows($userQuery);
						}
						$amount =  round($total_tip_all_users/$userCount, $decimalPlaces);
						$result['amount'] = $amount;
						$totalAmount += $amount;
					} else {
						$hrs = 0;
						$userHours = 0;
						if ($time_shift[0] == "ByShift") {
							$serverCond = implode(',', $filterUserArr);
						} else {
							$serverCond = "SELECT `users`.`id` FROM $dataName.`users` LEFT JOIN $dataName.`clock_punches` on  `clock_punches`.`server_id` = `users`.`id` WHERE `clock_punches`.`role_id` = '".$class_id."'	AND `users`.`_deleted` = 0 AND `clock_punches`.`punched_out`= 0 group by `users`.`id`";
						}
						$userQuery = mlavu_query("SELECT `clock_punches`.`time`,`clock_punches`.`hours`,`clock_punches`.`time_out` FROM  $dataName.`clock_punches`  WHERE `server_id` in ($serverCond) AND `clock_punches`.`time` >= '".$date_info[2]."' AND `clock_punches`.`time` < '".$date_info[3]."' AND `clock_punches`.`punched_out`= 0");
						$userCount = mysqli_num_rows($userQuery);
						if ($userCount > 0) {
							while ($rowTime = mysqli_fetch_assoc($userQuery)) {
								if ($rowTime['time_out'] != "") {
									$hrs += round($rowTime['hours'], $decimalPlaces);
								} else {
									$inTime2 = $rowTime['time'];
									$inTime2= strtotime($inTime2);
									$todayAll= strtotime($todayDate);
									$userHours = round(abs($todayAll-$inTime2)/60/60, $decimalPlaces);
									$hrs += $userHours;
								}
							}
						}

						$perHoursAmount = round($total_tip_all_users/$hrs, $decimalPlaces);
						$amount = round($perHoursAmount * $hrsCurrentUser, $decimalPlaces);
						$result['amount'] = $amount;
						$totalAmount += $amount;
						}

					$resultData[] = $result;
					$userCountNum++;
				}

				$tipMismatchAmount = round($total_tip_all_users - $totalAmount, $decimalPlaces);
				//To distribute tip mismatch amount.
				$distributedValue = getDistributedValue($tipMismatchAmount, $userCountNum, $decimalPlaces);

				foreach ($resultData as $key => $resultInfo) {
					$uid = $resultInfo['id'];
					$server_name_user = $resultInfo['f_name']." ".$resultInfo['l_name'];
					$date_hrs = $today_timestamp." 00:00:00";
					$tom_Date_hrs = date('Y-m-d',strtotime($today_timestamp . "+1 days"))." 00:00:00";
					$week_end_date = "";
					$created_where = " AND created_date='".$createdDate."' ";
					$weekly_where = "";
					$weekly_where_update = "";

					if ($time_shift[0] == "ByShift") {
						$time_punch_query = mlavu_query("select `server_id`, `time`, `hours`, `time_out` from $dataName.`clock_punches` where `location_id` ='".$locationId."' AND `server_id` = '".$uid."' AND `role_id` = '".$class_id."' AND `punched_out`= 0 AND `time` >= '".$date_hrs."' AND `time` < '".$tom_Date_hrs."' order by `time` asc limit 1 ");
						$time_punch_data = mysqli_fetch_assoc($time_punch_query);
						$clockin_time = $time_punch_data['time'];
						$server_user_id = $time_punch_data['server_id'];
						$punch_time = explode(" ", $clockin_time);
						$shift_change_time = date("H:i", strtotime($time_shift[1]));
						$punch_time = date("H:i", strtotime($punch_time[1]));
						$userArr = array();
						$getUserClassQuery = mlavu_query("SELECT `users`.`id` FROM $dataName.`users` LEFT JOIN $dataName.`clock_punches` on  `clock_punches`.`server_id` = `users`.`id` WHERE `clock_punches`.`role_id` = '".$class_id."' AND `clock_punches`.`punched_out`= 0 AND `users`.`_deleted` = 0 group by `users`.`id`");
						//$getUserClassQuery = mlavu_query("SELECT `id` FROM $dataName.`users` WHERE `employee_class` = '".$class_id."' AND `_deleted` = 0");
						while ( $getAllUserClass = mysqli_fetch_assoc($getUserClassQuery)) {
							$userArr [] = $getAllUserClass['id'];
						}
						$getAllUser = implode(',',$userArr);


						if (strtotime($punch_time) < strtotime($shift_change_time)  ) { //clock in before shift
							$timeClause = " AND DATE(`time`) = '".$today_timestamp."'";
							$havingTime = " HAVING minTime < '".$today_timestamp." ".$shift_change_time."'";
							$uid = $server_user_id;
						}
						if (strtotime($punch_time) >= strtotime($shift_change_time) ) { //clock in on or after shift
							$uid = $server_user_id;
							$timeClause = " AND `time` >= '".$today_timestamp." ".$shift_change_time."'";
							$havingTime = " HAVING minTime >= '".$today_timestamp." ".$shift_change_time."'";
						}

						$userQuery = mlavu_query("select `server_id`,`time`,`hours`,`time_out`, min(`time`) as minTime from $dataName.`clock_punches` where `location_id` ='$locationId'  $timeClause AND `punched_out` = 0 AND `server_id` in ($getAllUser) GROUP BY server_id $havingTime");
						$filterUserCount = mysqli_num_rows($userQuery);
						$filterUserArr = array();
						if ($filterUserCount > 0) {
							while ( $filterUser = mysqli_fetch_assoc($userQuery)) {
								$filterUserArr[] = $filterUser['server_id'];
							}
						}
						$total_tip_all_users = getTotalTips($date_info, $locationId, $class_id, $dataName, $filterUserArr);
						$total_tip_all_users = $total_tip_all_users + tipsCashData($date_info, $locationId, $class_id, $dataName, $filterUserArr);

					}
						if ($uid != "" ) {
							$total_tip_array = getUserTips($date_info, $locationId, $uid, $dataName, $class_id);
							$card_tip = $total_tip_array['cardtip'];
							$cash_tip = $total_tip_array['cashtip'];
							$total_tip = $card_tip + $cash_tip;
							$tip_out = getTotalTipout($uid, $createdDate, $locationId, $class_id, $dataName);
							$hrs_current_user = getUserTime($locationId, $uid, $date_hrs, $tom_Date_hrs, $todayDate, $dataName, $class_id, $decimalPlaces);
							$hours_worked = $hrs_current_user;
							$tip_in = $total_tip - $tip_out;
							$amount = $resultInfo['amount'] + $distributedValue[$key];

							$sale_query = mlavu_query("select ( SUM(`orders`.`subtotal`) - SUM(`orders`.`discount`) ) as 'Sales' from $dataName.`orders` LEFT JOIN $dataName.`clock_punches` as cp ON `cp`.`server_id`= `orders`.`server_id` where `orders`.`location_id` =".$locationId." AND `orders`.`server_id` =".$uid." AND `orders`.`closed` >= '".$date_info[2]."' AND `orders`.`closed` < '".$date_info[3]."' AND `orders`.`closed` >= `cp`.`time` AND `orders`.`closed` < if(`cp`.`time_out` <> '', `cp`.`time_out`, '".$date_info[3]."') AND `cp`.`time` >= '".$date_info[2]."' AND `cp`.`time` < '".$date_info[3]."'AND `orders`.`void` = '0' AND `cp`.`role_id`=".$class_id." AND `cp`.`punched_out` = 0");
							$sale_data = mysqli_fetch_assoc($sale_query);
							$sales=$sale_data['Sales'];
							$tip_due  = $amount + $tip_out;
							$tip_pool = $tip_due - $tip_in;
							if ($time_shift[0] == 'Weekly') {
								$createdDateWeekly = " AND created_date<'".$createdDate."' ";
								$weekly_where = " AND `week_end_date` >= '".$createdDate."' ";
								$weekly_where_update = " AND `week_end_date` >= '".$createdDate."' ";
								$day = $time_shift[1];
								$getPrevdayDataQuery = mlavu_query("select `id`,`time_for_tip_sharing`, `week_end_date`, `tip_due`, `sales`, `card_tip`, 
		`cash_tip`, `total_tip`, `tip_due`,`tip_pool`,`hours_worked` from $dataName.tip_sharing where location_id='".$locationId."' AND emp_class_id='".$class_id."' AND server_id='".$uid."' $createdDateWeekly $weekly_where ORDER BY `id` DESC LIMIT 1");
								if ( mysqli_num_rows($getPrevdayDataQuery) > 0 ) {
									$getPrevdayData = mysqli_fetch_assoc($getPrevdayDataQuery);
									$week_end_date = $getPrevdayData['week_end_date'];
									$hours_worked += $getPrevdayData['hours_worked'];
									$sales += $getPrevdayData['sales'];
									$card_tip += $getPrevdayData['card_tip'];
									$cash_tip += $getPrevdayData['cash_tip'];
									$total_tip += $getPrevdayData['total_tip'];
									$tip_out += $getPrevdayData['tip_out'];
									$tip_in += $getPrevdayData['tip_in'];
									$tip_due += $getPrevdayData['tip_due'];
									$tip_pool += $getPrevdayData['tip_pool'];
								} else {
									$today_day = strtolower(date('l'));
									if (strtolower($day) == $today_day) {
									$week_end_date = date('Y-m-d', strtotime($deviceTime. ' + 6 days'));
									} else {
										$week_start_date = date('Y-m-d',strtotime('last '.$day));
										$week_end_date = date('Y-m-d', strtotime($week_start_date. ' + 6 days'));
									}
								}
								insertIntoSharingLog($createdDate, $locationId, $uid, $class_id, $day, $dataName);
							}
							$select_query_sharing = mlavu_query("select `id`,`time_for_tip_sharing` from $dataName.tip_sharing where location_id='".$locationId."' AND emp_class_id='".$class_id."' AND server_id='".$uid."' $created_where");
							$row_count = mysqli_num_rows($select_query_sharing);
							$result_data = mysqli_fetch_assoc($select_query_sharing);
							$time_for_sharing = explode("|",$result_data['time_for_tip_sharing']);
							$current_time_sharing = explode("|",$time_for_tip_sharing);
							if ($time_for_sharing[0] == "Weekly" && $time_for_sharing[0] != $current_time_sharing[0]) {
								//$week_end_date = date("Y-m-d", strtotime('tomorrow'));
							}
							if ($row_count > 0) {
								$updatedDate = $deviceTime;
								mlavu_query("UPDATE $dataName.`tip_sharing` SET `tip_sharing_rule` = '".$tip_sharing_rule."', `time_for_tip_sharing` = '".$time_for_tip_sharing."', `hours_worked` = '".$hours_worked."', `sales` = '".$sales."',`card_tip` = '".$card_tip."', `cash_tip` = '".$cash_tip."',`total_tip` = '".$total_tip."', `tip_out` = '".$tip_out."', `tip_in` = '".$tip_in."', `tip_due` = '".$tip_due."', `tip_pool` = '".$tip_pool."',`updated_date` = '".$updatedDate."', `emp_class_name`='[1]'  WHERE  `location_id` = '".$locationId."' AND `emp_class_id`='".$class_id."' AND `server_id` = '".$uid."' AND (`created_date` = '".$createdDate."' $weekly_where_update) ", $class_name);
							} else {
								mlavu_query("INSERT INTO $dataName.`tip_sharing` (`location_id`, `server_id`, `server_name`, `emp_class_id`,`emp_class_name`, `tip_sharing_rule`,`time_for_tip_sharing`, `hours_worked`, `sales`, `card_tip`, `cash_tip`,`total_tip`, `tip_out`, `tip_in`, `tip_due`, `tip_pool`, `created_date`, `updated_date`, `week_end_date`) VALUES ('".$locationId."', '".$uid."', '[1]', '".$class_id."', '[2]', '".$tip_sharing_rule."', '".$time_for_tip_sharing."', '".$hours_worked."', '".$sales."', '".$card_tip."', '".$cash_tip."', '".$total_tip."', '".$tip_out."','".$tip_in."', '".$tip_due."', '".$tip_pool."', '".$createdDate."', '".$createdDate."', '".$week_end_date."') ", $server_name_user, $class_name);
							}
						}
					}
				}
		}
	}
}