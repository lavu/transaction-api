<?php

	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/admin/distro/beta/distro_user.php");
	require_once("/home/poslavu/public_html/admin/distro/distro_data_functions.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/json.php");
	mlavu_connect_db();

	// set up some values
	// certified min is 10/year (roughly 2.5=3 a quarter)
	// preferred min is 35/year (roughly 8.75=9 a quarter)
	// super min is 60/year (15 a quarter)
	$a_levels = array(
		array('level'=>'uncertified', 'min'=>0, 'max'=>2, 'distros'=>array(), 'data'=>array(), 'new_commission'=>20),
		array('level'=>'certified', 'min'=>3, 'max'=>8, 'distros'=>array(), 'data'=>array(), 'new_commission'=>20),
		array('level'=>'preferred', 'min'=>9, 'max'=>14, 'distros'=>array(), 'data'=>array(), 'new_commission'=>35),
		array('level'=>'super', 'min'=>15, 'max'=>10000, 'distros'=>array(), 'data'=>array(), 'new_commission'=>50)
	);
	$a_quarters = array("01-01", "04-01", "07-01", "10-01");

	// get all commissions/settings for all distributors
	$distro_query = mlavu_query("SELECT `username`,`distro_license_com`,`special_json` FROM `poslavu_MAIN_db`.`resellers` WHERE `username`!=''");
	$a_distros = array();
	while ($row = mysqli_fetch_assoc($distro_query)) {
		$a_distros[$row['username']] = $row;
		$o_settings = (strlen($row['special_json']) > 0) ? LavuJson::json_decode($row['special_json'], FALSE) : new stdClass();
		$a_distros[$row['username']]['settings'] = $o_settings;
	}

	// determine the current quarter
	$today = date("Y-m-d");
	$i_today = strtotime($today." 00:00:00");
	$s_previous_quarter = date("Y")."-".$a_quarters[3]." 00:00:00";
	$s_current_quarter = date("Y")."-".$a_quarters[0]." 00:00:00";
	$s_next_quarter = date("Y")."-".$a_quarters[1]." 00:00:00";
	foreach($a_quarters as $s_quarter) {
		$s_quarter = date("Y")."-".$s_quarter." 00:00:00";
		$i_date = strtotime($s_quarter);
		$s_next_quarter = $s_quarter;
		if ($i_today <= $i_date)
			break;
		$s_previous_quarter = $s_current_quarter;
		$s_current_quarter = $s_quarter;
	}

	// get the current commissions (and only the current commissions)
	$o_current_commissions = new stdClass();
	foreach($a_distros as $s_username=>$a_distro) {
		$o_current_commissions->$s_username = $a_distro['distro_license_com'];
	}

	// get all of distributors for each level
	foreach($a_levels as $k=>$a_level) {
		$i_min = $a_level['min'];
		$i_max = $a_level['max'];
		$a_levels[$k]['data'] = get_distributor_licenses_sold_since($s_previous_quarter, $i_min, $i_max, $s_current_quarter);
		foreach($a_levels[$k]['data'] as $a_data) {
			$a_levels[$k]['distros'][] = array_merge($a_distros[$a_data['resellername']], array('count'=>$a_data['count']));
		}
	}

	// dump the old values
	$today_full = date("Y-m-d H:i:s");
	$s_file_contents = file_get_contents("/home/poslavu/private_html/old_distro_commissions.txt");
	$o_old_values = ($s_file_contents == "") ? new stdClass() : LavuJson::json_decode($s_file_contents, FALSE);
	$o_old_values->$today = $o_current_commissions;
	file_put_contents("/home/poslavu/private_html/old_distro_commissions.txt", LavuJson::json_encode($o_old_values));

	// set the new values
	$s_username = "";
	$s_reason_for_changing = "updated quarterly as according to the reseller agreement";
	foreach($a_levels as $k=>$a_level) {
		foreach($a_level['distros'] as $k2=>$a_distro) {
			$user = new distro_user();
			$s_distro = $a_distro['username'];
			$user->set_leads_accounts($s_distro, '1');
			if ($user->set_all_info($s_distro) !== FALSE && $user->use_custom_commission() == 0) {
				$a_levels[$k]['distros'][$k2]['changed'] = '';
				$user->update_commission($a_level['new_commission'], $s_username, $s_reason_for_changing);
			} else {
				$a_levels[$k]['distros'][$k2]['changed'] = '(commission not changed)';
			}
		}
	}

	// email ben, rafael, and gibbs
	$a_echo_dates = array(explode(" ",$s_previous_quarter), explode(" ",$s_current_quarter));
	$s_email = "Distributor commissions have been updated based on sales during the period between ";
	$s_email .= $a_echo_dates[0][0]." and ".$a_echo_dates[1][0].".";
	$s_email .= " Please review these changes, as described below:\n\n";
	foreach($a_levels as $k=>$a_level) {
		$s_email .= "\n".ucfirst($a_level['level'])."\n\n";
		foreach($a_level['distros'] as $a_distro) {
			$s_distro = $a_distro['username'];
			$s_changed = $a_distro['changed'];
			$s_email .= $s_distro." ".$s_changed."\n";
		}
	}
	mail("benjamin@poslavu.com, rt@poslavu.com, ag@poslavu.com", "Distributor Commission Updated", $s_email);

?>