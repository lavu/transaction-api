<?php

// For details related to Lavu's logging system, refer to http://wiki.lavu/Server_Logs

function clear_all_logs_for_dataname($company_dataname, $root_dir)
{
	if (empty($company_dataname))
	{
		return;
	}

	$cdir = $root_dir.$company_dataname;
	if (!is_dir($cdir))
	{
		return;
	}

	$logdir_fp = opendir($cdir);
	$flist = array();

	while ($read_logfile = readdir($logdir_fp))
	{
		if ($read_logfile=="." || $read_logfile=="..")
		{
			continue;
		}

		// Keep logs around that include details about location configuration changes
		if (strpos($read_logfile, "form_change") === FALSE)
		{
			$flist[] = $read_logfile;
		}
	}

	sort($flist);

	for ($i = 0; $i < count($flist); $i++)
	{
		if ($flist[$i]=="." || $flist[$i]=="..")
		{
			continue;
		}

		$log_fullpath = $cdir."/".$flist[$i];
		if (is_file($log_fullpath))
		{
			echo $log_fullpath . "\n";
			unlink($log_fullpath);
		}
		else if (is_dir($log_fullpath))
		{
			$subdir_fp = opendir($log_fullpath);
			while ($read_subfile = readdir($subdir_fp))
			{
				if ($read_subfile!="." && $read_subfile!="..")
				{
					$sub_fullpath = $log_fullpath."/".$read_subfile;
					if (is_file($sub_fullpath))
					{
						echo $sub_fullpath . "\n";
						unlink($sub_fullpath);
					}
				}
			}
		}
	}
}

function clear_logs_for_dataname($company_dataname, $root_dir, $operational_ttl, $debug_ttl)
{
	if (empty($company_dataname))
	{
		return " NOT FOUND";
	}

	$cdir = $root_dir.$company_dataname;

	// Only consider files in these subdirectories
	$subdirectories = array(
		"",
		"/debug",
		"/debug/gateway_debug",
		"/operational"
	);

	$earliest_operational_date	= date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $operational_ttl), date("Y")));
	$earliest_debug_date		= date("Y-m-d", mktime(0, 0, 0, date("m"), (date("d") - $debug_ttl), date("Y")));

	$str = "\n\nClearing Logs for ".$company_dataname." Before ".$earliest_operational_date." (debug before ".$earliest_debug_date."):";
	$str .= "\ndir: ".$cdir;

	if (!is_dir($cdir))
	{
		$str .= " NOT FOUND";
	}
	else
	{
		$flist = array();

		foreach ($subdirectories as $sub_dir)
		{
			$logdir_fp = opendir($cdir."/".$sub_dir);
			while ($read_logfile = readdir($logdir_fp))
			{
				// Keep logs around that include details about location configuration changes
				if (strpos($read_logfile, "form_change") === FALSE)
				{
					$flist[] = $sub_dir."/".$read_logfile;
				}
			}
		}

		sort($flist);

		for ($i = 0; $i < count($flist); $i++)
		{
			$file_path	= $flist[$i];
			$full_path	= $cdir.$file_path;
			$path_parts	= explode("/", $file_path);
			$file_name	= $path_parts[(count($path_parts) - 1)];
			$keep_time 	= ($operational_ttl * 86400); // TTL days x 86400 seconds in a day

			if (is_dir($full_path))
			{
				continue;
			}

			if ($file_name=="mysql.log" || substr($file_name, 0, 12)=="auto_correct")
			{
				$keep_time = (3 * 86400); // 3 days
			}
			else if (substr($file_path, 0, 7) == "/debug/")
			{
				$keep_time = ($debug_ttl * 86400);
			}

			$file_mod_ts	= filemtime($full_path);
			$minimum_ts		= (time() - $keep_time - 86400); // Additional day of margin
			$keep_log		= ($file_mod_ts >= $minimum_ts);

			if ($keep_log)
			{
				$phrase = "Keeping";
			}
			else
			{
				$phrase = "Removing";
				unlink($full_path);
			}

			$str .= "\n".$phrase." - ".$file_path;
		}
	}

	return $str;
}

require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");

$maindb = "poslavu_MAIN_db";
mlavu_connect_db();

$opts = getopt("c:");

echo "\nprocessing:\n";

$logstr = "Clearing logs on ".date("m/d/Y h:i a");

// Keep other operational logs on individual containers for 30 days
$operational_ttl = 30;

// Keep keep all debuyg logs on individual containers for 7 days
$debug_ttl = 7;

$limit_deletions = (isset($opts['c']) && $opts['c']=="1");

if ($limit_deletions)
{
	echo "\nClearing operational logs older than ".$operational_ttl." days";
	echo "\nClearing debug logs older than ".$debug_ttl." days\n\n";
}
else
{
	echo "\nClearing all logs\n\n";
}

$base_log_path = "/home/poslavu/logs/company/";

$company_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` != '' ORDER BY `data_name` ASC");
if (mysqli_num_rows($company_query) > 0)
{
	while ($company_read = mysqli_fetch_assoc($company_query))
	{
		$data_name = $company_read['data_name'];
		$can_delete = FALSE;

		if (strpos($company_read['notes'], "AUTO-DELETED") !== FALSE)
		{
			$can_delete = TRUE;
		}
		else if (is_dir($base_log_path.$data_name))
		{
			$can_delete = TRUE;
		}

		if (!$can_delete)
		{
			continue;
		}

		if ($limit_deletions)
		{
			$logstr .= clear_logs_for_dataname($data_name, $base_log_path, $operational_ttl, $debug_ttl);
		}
		else
		{
			clear_all_logs_for_dataname($data_name, $base_log_path);
		}
	}
}

echo "\ndone!\n";