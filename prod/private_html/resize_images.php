<?php
	ini_set("display_errors","1");
	
	function image_size($filename)
	{
		list($width, $height) = getimagesize($filename);
		$img_info = array();
		$img_info['width'] = $width;
		$img_info['height'] = $height;
		return $img_info;
	}
	
	function s_setup_dir($rdir)
	{
		if(!is_dir($rdir))
		{
			mkdir($rdir,0755);
		}
		if(is_dir($rdir))
		{
			$result = chown($rdir, "apache");
			echo $rdir;
			if($result) 
			{
				echo " (success)";
				$fp = fopen($rdir . "/.htaccess","w");
				fwrite($fp," ");
				fclose($fp);
			}
			else echo " (failed)";
			echo "\n";
		}
	}
	
	function clean_htfiles($dir)
	{
		$fp = opendir($dir);
		while($fname = readdir($fp))
		{
			if($fname!="."&&$fname!=".."&&$fname!="engine")
			{
				//echo "<br>" . $fname;
				//unlink("$dir/$fname/.htaccess");
				if(is_dir("$dir/$fname"))
				{
					clean_htfiles("$dir/$fname");
				}
				else if($fname!="index.php"&&$fname!="index.htm"&&$fname!="index.html"&&$fname!=".htaccess")
				{
					chown("$dir/$fname","apache");
					chgrp("$dir/$fname","apache"); 
				}
			}
		}
		echo "\n$dir";
		//unlink("$dir/.htaccess");
		if($fw = fopen("$dir/.htaccess","w"))
		{
			fwrite($fw," ");
			fclose($fw);
		}
		if($fi = fopen("$dir/index.php","w"))
		{
			fwrite($fi," ");
			fclose($fi);
		}
		chown($dir,"apache");
		chgrp($dir,"apache"); 
	}
	
	function resize_images($dir)
	{
		echo "\nprocessing images for $dir";
		$fp = opendir($dir);
		if($fp)
		{
			if(!is_dir("$dir/full"))
			{
				mkdir("$dir/full",0755);
				echo "\ncreated $dir/full";
			}
			while($fname = readdir($fp))
			{
				if($fname!="."&&$fname!=".."&&$fname!="engine")
				{
					if(is_dir("$dir/$fname"))
					{
					}
					else if($fname!="index.php"&&$fname!="index.htm"&&$fname!="index.html"&&$fname!=".htaccess")
					{
						// copy into full directory
						if(!is_file("$dir/full/$fname"))
						{
							copy("$dir/$fname","$dir/full/$fname");
							echo "\ncopied file to $dir/full/$fname";
						}
						// resize here
						if(is_file("$dir/full/$fname"))
						{
							$set_width = 120;
							$set_height = 80;
							
							$full_file = "$dir/full/$fname";
							$thumb_file = "$dir/$fname";
							
							$create_thumb = true;
							if(is_file("$dir/$fname"))
							{
								$img_size = image_size("$dir/$fname");
								//echo "\nexists: $dir/$fname - " . $img_size['width'] . " x " . $img_size['height'];
								if($img_size['width'] > $set_width || $img_size['height'] > $set_height)
								  $create_thumb = true; else $create_thumb = false;
	
							}
							if($create_thumb)
							{
								exec("convert '$full_file' -resize '".$set_width."x".$set_height.">' '$thumb_file'");
								echo "\ncreated thumbnail for $dir/$fname";
							}
						}
					}
				}
			}
		}
	}
	
	if($argc > 1)
	{
		$option = $argv[1];
		if($option=="dirs")
		{
			if($argc > 2)
			{
				$area = $argv[2];
				if($area=="items" || $area=="categories")
				{
					$bdir = "/home/poslavu/public_html/admin/images";
					$drlist = opendir($bdir);
					while($dr = readdir($drlist))
					{
						$rdir = $bdir . "/" . $dr;
						if(is_dir($rdir))
						{
							s_setup_dir($rdir . "/$area/full");
							s_setup_dir($rdir . "/$area/main");
						}
					}
				}
				else if($area=="lavukart")
				{
					$bdir = "/home/poslavu/public_html/admin/components/lavukart/companies";
					clean_htfiles($bdir);
				}
			}
		}
		else if($option=="all")
		{
		  //$res_query = mysql_query();
		}
		else
		{
			resize_images("/home/poslavu/public_html/admin/images/$option/items");
			resize_images("/home/poslavu/public_html/admin/images/$option/categories");
		}
	}
?>

