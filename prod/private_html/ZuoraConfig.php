<?php

class ZuoraConfig
{
	const SANDBOX           = true;
	const DEVELOPER_EMAIL   = 'team.zuora@lavu.com';

	// Zuora SOAP API
	const API_USERNAME         = 'apiuser@lavu.com';
	const API_PASSWORD         = 'BaliLavu88';
	const API_ENDPOINT         = 'https://apisandbox.zuora.com/apps/services/a/81.0';
	const API_DEBUG            = true;
	const API_WSDL             = '/home/poslavu/public_html/inc/billing/zuora/wsdl/zuora.a.81.0.wsdl';
	const API_QUERY_BATCH_SIZE = 100;

	// Hosted Payment Method (HPM)
	const HPM_PAGE_URL            = 'https://apisandbox.zuora.com/apps/PublicHostedPageLite.do';
	const HPM_JS_INC              = 'https://apisandboxstatic.zuora.com/Resources/libs/hosted/1.3.0/zuora-min.js';
	const HPM_DIGITALSIG_ENDPOINT = 'https://apisandbox-api.zuora.com/rest/v1/rsa-signatures';
	const HPM_TENANTID            = '14464';
	const HPM_PAYMENTGATEWAY_NAME = 'Authorize.net Sandbox';

	// HPMs must be created for subdomain.domain combo (https required) with which users might access CP / Payment Link
	const HPM_PAGEID_PAYMENT_LINK__register_tom_poslavu_com = '2c92c0f856bc85290156c270acfb3af1';
	const HPM_PAGEID_PAYMENT_LINK__bugtest_poslavu_com      = '2c92c0f85847a3e1015850f2a0035a1f';
	const HPM_PAGEID_PAYMENT_LINK__qatest_poslavu_com       = '2c92c0f95847b088015850f4041037db';
	const HPM_PAGEID_CP_BILLING__tom_poslavu_com            = '2c92c0f956bc8fb40156d10b5538569d';
	const HPM_PAGEID_CP_BILLING__bugtest_poslavu_com        = '2c92c0f85847a3e1015850d108df7f31';
	const HPM_PAGEID_CP_BILLING__qatest_poslavu_com         = '2c92c0f95847b069015850d62e102297';

	const GOLIVE_DATE    = '2016-09-12';
	const GOLIVE_DATE_TS = 1473631200;

	const LAVU_START_DATE   = '2010-01-01';
	const TWENTY_YEARS_DATE = '2030-01-01';

}


/**
* Sources of HPM values:
*
* HPM_PAGE_URL  Zuora UI -> Payment Settings -> Setup Hosted Page -> <Page Name link> -> Implementation Details section -> Hosted Page URL
* HPM_JS_INC  https://knowledgecenter.zuora.com/CA_Commerce/T_Hosted_Commerce_Pages/B_Payment_Pages_2.0/H_Integrate_Payment_Pages_2.0
* HPM_DIGITALSIG_ENDPOINT - https://knowledgecenter.zuora.com/CA_Commerce/T_Hosted_Commerce_Pages/B_Payment_Pages_2.0/F_Generate_the_Digital_Signature_for_Payment_Pages_2.0
* HPM_TENANTID - Zuora UI -> Personal Settings -> Manage Your Profile
* HPM_PAGEID_* - Zuora UI -> Payment Settings -> Setup Hosted Page -> <Show Page Id link>
* HPM_PAYMENTGATEWAY_NAME - Zuora UI -> Payment Settings -> Payment Gateways -> <Name>
*
*/
