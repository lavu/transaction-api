<?php

	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	$maindb = "poslavu_MAIN_db";
	mlavu_connect_db();
	
	$check_paths = array();
	$check_paths[] = array('menu_categories', '/categories');
	$check_paths[] = array('menu_categories', '/categories/full');
	$check_paths[] = array('menu_categories', '/categories/main');
	$check_paths[] = array('menu_items', '/items');
	$check_paths[] = array('menu_items', '/items/full');
	$check_paths[] = array('menu_items', '/items/main');
	
	$ci_array = array("categories", "items");
	$default_datanames = array("DEFAULT", "default_coffee", "defaultbar", "default_pizza_", "default_restau");
	$default_files = array();
	foreach ($default_datanames as $dflt_dn) {
		foreach ($ci_array as $ci) {
			$path = "/mnt/poslavu-files/images/".$dflt_dn."/".$ci;
			if (is_dir($path)) {
				if ($dir = opendir($path)) {
					while (false !== ($file = readdir($dir))) {
						if (!in_array($file, $default_files) && is_file($path."/".$file)) $default_files[] = $file;
					}
				}
			}
		}
	}
	
	echo "\n\n".print_r($default_files, true)."\n\n";

	$fp = fopen("/home/poslavu/private_html/images_cleared.txt","w");

	if (count($default_files) > 0) {
	
		echo "\nprocessing:\n";
		$logstr = "Clearing unused default images on " . date("m/d/Y h:i a");
		$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `last_activity` > '[1]' order by `data_name` asc", date("Y-m-d H:i:s", (time() - 1209600))); // check accounts active within 14 days
		if (mysqli_num_rows($company_query)) {
			while($company_read = mysqli_fetch_assoc($company_query))
			{
				echo ".";
				if($company_read['data_name']!="" && !in_array($company_read['data_name'], $default_datanames))
				{
					$logstr .= "\n\n\n\nClearing images for ".$company_read['data_name'].":";
					$root_path = "/mnt/poslavu-files/images/".$company_read['data_name'];
					foreach ($check_paths as $chkpth) {
						$full_path = $root_path.$chkpth[1];
						$logstr .= "\n\ndir: ".$full_path;
						if (is_dir($full_path)) {
							if ($dir = opendir($full_path)) {
								$file_list = array();
								while (false !== ($file = readdir($dir))) {
									if (is_file($full_path."/".$file) && $file!=".htaccess") $file_list[] = $file;
								}
								
								foreach ($file_list as $file) {
									$keeping = true;
									if (in_array($file, $default_files)) {
										$check_menu = mlavu_query("SELECT `id` FROM `poslavu_".$company_read['data_name']."_db`.`".$chkpth[0]."` WHERE `image` = '[1]' AND `_deleted` != '1'", $file);
										if (mysqli_num_rows($check_menu) < 1) {
											$keeping = false;
											$logstr .= "\ndeleting ".$file;
											unlink($full_path."/".$file);
										}
									}
									if ($keeping) $logstr .= "\nkeeping ".$file;
								}
								
							} else $logstr .= " FAILED TO OPEN";
	
						} else {
						
							if (!is_dir($root_path)) {
								mkdir($root_path);
								chmod($root_path, 0775);
								chown($root_path, "apache");
								chgrp($root_path, "apache");
							}
							
							foreach ($ci_array as $ci) {
								if (!is_dir($root_path."/".$ci)) {
									mkdir($root_path."/".$ci);
									chmod($root_path."/".$ci, 0775);
									chown($root_path."/".$ci, "apache");
									chgrp($root_path."/".$ci, "apache");
								}
							}
						
							echo "\n".$full_path;
						
							mkdir($full_path);
							chmod($full_path, 0775);
							chown($full_path, "apache");
							chgrp($full_path, "apache");
						}
					}
					
					fwrite($fp, $logstr);
					$logstr = "";
				}
			}
		}
		echo "\ndone!\n";

	} else {
	
		echo "\nfailed to build default file list\n";
		fwrite($fp, "\nfailed to build default file list on " . date("m/d/Y h:i a")."\n");
		
	}
	
	fclose($fp);

?>