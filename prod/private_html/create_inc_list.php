<?php
	require_once("/home/poslavu/private_html/myinfo.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	
	mlavu_connect_db();
	
	function get_date_parts($dt)
	{
		$dt_parts = explode(" ",$dt);
		if(count($dt_parts) > 1)
		{
			$d_parts = explode("-",$dt_parts[0]);
			$t_parts = explode(":",$dt_parts[1]);
			
			return array("year"=>$d_parts[0],"month"=>$d_parts[1],"day"=>$d_parts[2],"hour"=>$t_parts[0],"mins"=>$t_parts[1],"secs"=>$t_parts[2]);
		}
		else return false;
	}
	
	$nt = get_date_parts(date("Y-m-d H:i:s"));
	$min_last_connect = date("Y-m-d H:i:s",mktime(0,0,0,$nt['month'],$nt['day'] - 5,$nt['year']));
	
	$str = "";
	$str .= "order deny,allow\n";
	$str .= "deny from all\n";
	$str_strict = $str;
	$first3 = array();
	
	$inc_query = mlavu_query("select distinct(`ipaddress`), `last_connect`, `first_connect` from `poslavu_MAIN_db`.`ap_connects` where `last_connect` > '[1]' order by `count` desc",$min_last_connect);
	while($inc_read = mysqli_fetch_assoc($inc_query))
	{
		$lct = get_date_parts($inc_read['last_connect']);
		$latest_first_connect_allowed = date("Y-m-d H:i:s",mktime(0,0,0,$lct['month'],$lct['day'] - 1,$lct['year']));
		
		$str .= "allow from " . $inc_read['ipaddress'] . "\n";
		if($inc_read['first_connect'] <= $latest_first_connect_allowed)
		{
			$str_strict .= "allow from " . $inc_read['ipaddress'] . "\n";
		}
		
		$ip_parts = explode(".",$inc_read['ipaddress']);
		if(count($ip_parts) > 2)
		{
			$f3 = $ip_parts[0] . "." . $ip_parts[1] . "." . $ip_parts[2];
			if(!isset($first3[$f3])) $first3[$f3] = 1;
			else $first3[$f3]++;
		}
	}
	
	$str_first3 = "";
	foreach($first3 as $key => $val)
	{
		$str_first3 .= $key . ".0\n";// . " x " . $val . "\n";
	}
	
	$fname = "/home/poslavu/private_html/inclist.txt";
	$fp = fopen($fname,"w");
	fwrite($fp,$str);
	fclose($fp);

	$fname = "/home/poslavu/private_html/inclist_strict.txt";
	$fp = fopen($fname,"w");
	fwrite($fp,$str_strict);
	fclose($fp);

	$fname = "/home/poslavu/private_html/inc_first3.txt";
	$fp = fopen($fname,"w");
	fwrite($fp,$str_first3);
	fclose($fp);
?>
