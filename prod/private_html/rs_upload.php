<?php
require_once("/home/poslavu/private_html/myinfo.php");

function rs_move_uploaded_file($tmp_filename, $new_filename, $basename = "", $imagick = false, $allowed=false, $dataname="", $isPublic=true)
{
	$dataname = $dataname ? "poslavu_".$dataname."_db" : admin_info("database");
	$fparts = explode(".",$new_filename);
	if(count($fparts) < 2)
		$ext = "";
	else
		$ext = strtolower($fparts[count($fparts)-1]);
	
	if($allowed)
	{
		$allow_upload = false;
		for($i=0; $i<count($allowed); $i++)
		{
			if($allowed[$i]==$ext)
			$allow_upload = true;
		}
	}
	else
	{
		$allow_upload = true;
		if($ext=="php" || $ext=="") $allow_upload = false;
	}
	if($allow_upload)
	{
		//---------------------------------------------------------------------------------
		// upload file here to AWS here, we first request the URI which will be use to upload
		// the image and send the right parameters to set the access rights
		//---------------------------------------------------------------------------------
		$handle = curl_init();
		$urlAWS = $_SERVER['AWS_END_POINT'];
		$properties = array();
		$properties["access"] = $isPublic ? "public" : "private";
		$properties["category"] = "menu";
		$properties["type"] = "image";
		$arrayPost = array();
		$arrayPost["properties"] = $properties;
		$payLoad = json_encode($arrayPost);
		curl_setopt($handle, CURLOPT_URL, $urlAWS);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($handle, CURLOPT_POSTFIELDS, $payLoad);
		$awsResponse = json_decode(curl_exec($handle), true);
		curl_close($handle);
		//---------------------------------------------------------------------------------
		// once we got the AWS URL we create a new CURL to actually upload the file to AWS
		// servers and log it into a new table which will from now on keep track of all 
		// uploads
		//---------------------------------------------------------------------------------
		if($imagick)
		{
			$imageContent = $tmp_filename->getImageBlob();
			$imageFormat = $tmp_filename->getImageFormat();
		}
		else{
			$imageFile = fopen($tmp_filename, "rb") or die("Unable to open file!");
			$imageContent = fread($imageFile, filesize($tmp_filename));
			fclose($imageFile);
			$imageFormat = getimagesize($tmp_filename)['mime'];
		}

		$uploadURL = $awsResponse['uri'];
		$handleUpload = curl_init();
		curl_setopt($handleUpload, CURLOPT_URL, $uploadURL);
		curl_setopt($handleUpload, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($handleUpload, CURLOPT_HTTPHEADER, array('Content-Type:'.$imageFormat,'x-amz-tagging: '. http_build_query($properties)));
		curl_setopt($handleUpload, CURLOPT_POSTFIELDS, $imageContent);
		$awsUploadResponse = json_decode(curl_exec($handleUpload), true);
		curl_close($handleUpload);
		mlavu_query("INSERT INTO " . $dataname .".file_uploads (name, storage_key, access_level, category, type, uuid, path) VALUES ('". $basename . "', '" . $awsResponse['storage_key']."', '".$properties["access"]."', '".$properties["category"]."', '".$properties["type"]."', '".$awsResponse['uuid']."', '".$urlAWS."')");
		$result = $awsResponse['storage_key'];
		// this is the actual instruction which creates the uploaded file, it can be removed after we fully move to S3
		if (file_exists($tmp_filename)) {
			if (!move_uploaded_file($tmp_filename, $new_filename)) {
				error_log('Unable to move file from' . $tmp_filename . ' to ' . $new_filename . '. S3 ref:' . $result);
			}
		}
	}
	else
	{
		$result = false;
	}
	return $result;
}
?>
