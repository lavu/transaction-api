<?php


use Phinx\Migration\AbstractMigration;

class Lp5839 extends AbstractMigration
{
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if ($this->hasTable('config') && !$this->table('config')->hasColumn('value17') ) {
				$this->execute("ALTER TABLE `config` ADD COLUMN `value17` varchar(255) NOT NULL DEFAULT '0'");
			}

			if ($this->hasTable('cc_transactions') && !$this->table('cc_transactions')->hasColumn('drawer') ) {
				$this->execute("ALTER TABLE `cc_transactions` ADD COLUMN `drawer` enum('0','1','2') NOT NULL DEFAULT '0'");
			}

			if ($this->hasTable('cash_data') && !$this->table('cash_data')->hasColumn('drawer') ) {
				$this->execute("ALTER TABLE `cash_data` ADD COLUMN `drawer` enum('0','1','2') NOT NULL DEFAULT '0'");
			}

			// add table
			$exists = $this->hasTable('server_cash_drawer');
			if ($exists == FALSE) {
				$table = $this->table('server_cash_drawer');
				$table
					->addColumn('printer', 'string', ['limit' => 100, 'null' => false, 'default'=>''])
					->addColumn('drawer', 'enum', ['values' => ['0', '1', '2'], 'null' => false, 'default'=>'0'])
					->addColumn('server_id', 'integer', [ 'limit' => 11, 'null' => false, 'default'=>0])
					->addColumn('assigned_by', 'integer', [ 'limit' => 11, 'null' => false, 'default'=>0])
					->addColumn('created_date', 'datetime', [ 'null' => false, 'default'=>'CURRENT_TIMESTAMP'])
					->addColumn('created_by', 'integer', ['limit' => 11, 'null' => false, 'default'=>0])
					->addColumn('updated_date', 'datetime', [ 'null' => false, 'update'=>'CURRENT_TIMESTAMP', 'default'=>'CURRENT_TIMESTAMP'])
					->addColumn('updated_by', 'integer', ['limit' => 11, 'null' => false, 'default'=>0])
					->addColumn('_deleted', 'integer', ['limit' => 1, 'null' => false, 'default'=>0])
					->addIndex(['printer','server_id','drawer'], ['unique' => true])
					->addIndex(['printer'],['name' => 'idx_printer'])
					->addIndex(['drawer'],['name' => 'idx_drawer'])
					->create();
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if ($this->hasTable('config') && $this->table('config')->hasColumn('value17') ) {
				$this->execute('ALTER TABLE `config` DROP COLUMN `value17`');
			}

			if ($this->hasTable('cc_transactions') && $this->table('cc_transactions')->hasColumn('drawer') ) {
				$this->execute("ALTER TABLE `cc_transactions` DROP COLUMN `drawer`");
			}

			if ($this->hasTable('cash_data') && $this->table('cash_data')->hasColumn('drawer') ) {
				$this->execute("ALTER TABLE `cash_data` DROP COLUMN `drawer`");
			}

			if ($this->hasTable('server_cash_drawer') ) {
				$this->dropTable('server_cash_drawer');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}

	}
}
