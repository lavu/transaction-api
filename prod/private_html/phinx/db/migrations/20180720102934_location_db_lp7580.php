<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp7580 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('locations') == true && $this->table('locations')->hasColumn('cuit_no') == false) {
   			$this->execute("ALTER TABLE `locations` ADD COLUMN `cuit_no` VARCHAR(100) NOT NULL");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('locations') == true && $this->table('locations')->hasColumn('cuit_no') == true) {
    			$this->execute("ALTER TABLE `locations` DROP COLUMN `cuit_no`");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
