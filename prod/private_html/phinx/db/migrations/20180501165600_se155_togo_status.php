<?php


use Phinx\Migration\AbstractMigration;

class Se155TogoStatus extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
      if ($this->hasTable('order_contents') == true && $this->table('order_contents')->hasColumn('togo_status') == false) {
            $this->execute("ALTER TABLE `order_contents` ADD COLUMN `togo_status` CHAR(1) NOT NULL");
      }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
      if ($this->hasTable('order_contents') == true && $this->table('order_contents')->hasColumn('togo_status') == true) {
            $this->execute("ALTER TABLE `order_contents` DROP COLUMN `togo_status`");
      }
    }
}