<?php


use Phinx\Migration\AbstractMigration;

class Cp32048RemoveValidatorFromCustomFieldsType extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('custom_field_types') && $this->table('custom_field_types')->hasColumn('validator')) {
                $this->execute("ALTER TABLE `custom_field_types` DROP COLUMN `validator`");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('custom_field_types') && !$this->table('custom_field_types')->hasColumn('validator')) {
                $this->execute("ALTER TABLE `custom_field_types` ADD COLUMN `validator` TEXT");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
