<?php


use Phinx\Migration\AbstractMigration;

class Cp32138RemoveUniqueConstraintForCustomFields extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            $tables = ['custom_fields', 'custom_fields_metadata'];

            foreach ($tables as $table) {
                if ($this->hasTable($table)) {
                    $row = $this->fetchRow('SHOW INDEX FROM `'.$table.'` WHERE Key_name="label";');

                    if ($row['Key_name']) {
                        $this->execute("ALTER TABLE `".$table."` DROP INDEX label;");
                    }
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            $tables = ['custom_fields', 'custom_fields_metadata'];

            foreach ($tables as $table) {
                if ($this->hasTable($table)) {
                    $row = $this->fetchRow('SHOW INDEX FROM `'.$table.'` WHERE Key_name="label";');

                    if (!$row['Key_name']) {
                        $this->execute("CREATE UNIQUE INDEX label ON `".$table."` (label);");
                    }
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
