<?php


use Phinx\Migration\AbstractMigration;

class TruncateAuditLogLp9975 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
                /* Write migration code here */
                if ($this->hasTable('audit_log')) {
                        $this->execute("TRUNCATE table audit_log");
                }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }


    /**
     * down() Method to rollback.
     * Rollback might not required for this migration. This has been discussed with Nick.
     */
    public function down()
    {
        $this->status = true;
        try {
                /* Write rollback code here */
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
