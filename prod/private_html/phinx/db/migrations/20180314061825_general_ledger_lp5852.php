<?php


use Phinx\Migration\AbstractMigration;

class GeneralLedgerLp5852 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $exists = $this->hasTable('general_ledger_settings');
        if ($exists == FALSE) {
            $table = $this->table('general_ledger_settings');
            $table
            ->addColumn('lineitem_name', 'string', ['limit' => 100, 'null' => false])
            ->addColumn('ref_id', 'string', ['limit' => 100, 'null' => false])
            ->addColumn('account_number', 'string', [ 'limit' => 100, 'null' => false])
            ->addColumn('description', 'string', [ 'limit' => 200, 'null' => false])
            ->addColumn('position', 'integer', [ 'limit' => 5,'null' => false ])
            ->addColumn('_deleted', 'integer', ['limit' => 1, 'null' => false, 'default'=>0])
            ->addColumn('created_date', 'datetime', [ 'null' => false ])
            ->addColumn('updated_date', 'datetime', [ 'default'=>'0000-00-00 00:00:00' ])
            ->addColumn('type', 'string', ['limit' => 100, 'null' => false])
            ->create();
        }
        
        
        // INSERT INTO `general_ledger_settings`
        if ($this->hasTable ( 'general_ledger_settings' )) {
            $rows = [
                       [ 
                            'lineitem_name' => 'Cash Payments',
                            'ref_id' => '',
                            'account_number' => '56895222',
                            'description' => 'Cash Clearing',
                            'position' => '1',
                            '_deleted' => '0',
                            'created_date' => '2018-03-07 07:10:00',
                            'updated_date' => '',
                            'type' => ''
                       ],
                       [
                           'lineitem_name' => 'Credit Card Sales',
                           'ref_id' => '',
                           'account_number' => '56895233',
                           'description' => 'Credit Cards Receivable',
                           'position' => '2',
                           '_deleted' => '0',
                           'created_date' => '2018-03-07 07:10:00',
                           'updated_date' => '',
                           'type' => ''
                       ],
                        [
                            'lineitem_name' => 'Deposits',
                            'ref_id' => '',
                            'account_number' => '56895244',
                            'description' => 'Deposits Deferred',
                            'position' => '3',
                            '_deleted' => '0',
                            'created_date' => '2018-03-07 07:10:00',
                            'updated_date' => '',
                            'type' => ''
                       ],
                       [
                            'lineitem_name' => 'Sales Tax',
                            'ref_id' => '',
                            'account_number' => '56895255',
                            'description' => 'Sales Tax payable',
                            'position' => '4',
                            '_deleted' => '0',
                            'created_date' => '2018-03-07 07:10:00',
                            'updated_date' => '',
                            'type' => ''
                       ],
                       [
                            'lineitem_name' => 'Tips',
                            'ref_id' => '',
                            'account_number' => '56895266',
                            'description' => 'Service Income - tips',
                            'position' => '2',
                            '_deleted' => '0',
                            'created_date' => '2018-03-07 07:10:00',
                            'updated_date' => '',
                            'type' => ''
                       ],
                       [
                            'lineitem_name' => 'Discounts',
                            'ref_id' => '',
                            'account_number' => '568952777',
                            'description' => 'Promos/Discounts',
                            'position' => '3',
                            '_deleted' => '0',
                            'created_date' => '2018-03-07 07:10:00',
                            'updated_date' => '',
                            'type' => ''
                       ],
                       [
                            'lineitem_name' => 'Reconciliation',
                            'ref_id' => '',
                            'account_number' => '568952888',
                            'description' => 'Cash Over/Short',
                            'position' => '4',
                            '_deleted' => '0',
                            'created_date' => '2018-03-07 07:10:00',
                            'updated_date' => '',
                            'type' => ''
                       ]
                ];
             $table = $this->table ( 'general_ledger_settings' );
             $table->insert ( $rows )->saveData ();
           }
        
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $tableExists = $this->hasTable('general_ledger_settings');
        if ($tableExists == true) {
            $this->dropTable('general_ledger_settings');
        }
    }
}
