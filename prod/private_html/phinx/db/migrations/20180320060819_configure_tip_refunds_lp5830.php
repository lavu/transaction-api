<?php


use Phinx\Migration\AbstractMigration;

class ConfigureTipRefundsLp5830 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if($this->hasTable('emp_classes')) {
            
            if ( $this->table('emp_classes')->hasColumn('tiprefund_rules') == false ) {
                $this->execute("ALTER TABLE `emp_classes` ADD COLUMN `tiprefund_rules` VARCHAR(40) NOT NULL");
            }
            
            if ($this->table('emp_classes')->hasColumn('refund_type') == false) {
                $this->execute("ALTER TABLE `emp_classes` ADD COLUMN `refund_type` VARCHAR(40) NOT NULL");
            }
        }
        
        if($this->hasTable('tipouts')) {
            
            if ( $this->table('tipouts')->hasColumn('sales_tipout') == false ) {
                $this->execute("ALTER TABLE `tipouts` ADD COLUMN `sales_tipout` VARCHAR(100) NOT NULL");
            }
            
            if ($this->table('tipouts')->hasColumn('supergroups_used') == false) {
                $this->execute("ALTER TABLE `tipouts` ADD COLUMN `supergroups_used` VARCHAR(100) NOT NULL");
            }
            
            if ($this->table('tipouts')->hasColumn('emp_class_id') == false) {
                $this->execute("ALTER TABLE `tipouts` ADD COLUMN `emp_class_id` INT(11) NOT NULL");
            }
            
        }
        
        if (!$this->hasTable('tipout_refunds')) {
            $this->execute("CREATE TABLE `tipout_refunds` ( `id` int(11) NOT NULL AUTO_INCREMENT, 
                                                            `amount` varchar(255) NOT NULL, 
                                                            `refund_percentage` varchar(255) NOT NULL, 
                                                            `created_date` varchar(255) NOT NULL,
                                                            `loc_id` varchar(100) NOT NULL,
                                                            `emp_class_id` INT(11) NOT NULL,
                                                            PRIMARY KEY (`id`) ) ENGINE=InnoDB");
        }
        
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if($this->hasTable('emp_classes')) {
            
            if($this->table('emp_classes')->hasColumn('tiprefund_rules')) {
                $this->execute('ALTER TABLE `emp_classes` DROP COLUMN `tiprefund_rules`');
            }
            
            if($this->table('emp_classes')->hasColumn('refund_type')) {
                $this->execute('ALTER TABLE `emp_classes` DROP COLUMN `refund_type`');
            }
            
        }
        
        if($this->hasTable('tipouts')) {
            
            if($this->table('tipouts')->hasColumn('sales_tipout')) {
                $this->execute('ALTER TABLE `tipouts` DROP COLUMN `sales_tipout`');
            }
            
            if($this->table('tipouts')->hasColumn('supergroups_used')) {
                $this->execute('ALTER TABLE `tipouts` DROP COLUMN `supergroups_used`');
            }
            
            if($this->table('tipouts')->hasColumn('emp_class_id')) {
                $this->execute('ALTER TABLE `tipouts` DROP COLUMN `emp_class_id`');
            }
            
        }
        
        if ($this->hasTable('tipout_refunds')) {
            $this->execute('DROP TABLE `tipout_refunds`');
        }
    }
}
