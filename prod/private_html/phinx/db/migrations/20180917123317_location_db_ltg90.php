<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLtg90 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ( $this->hasTable('config') == true ) {
    			$locationInfoArr = $this->fetchAll("SELECT `id` FROM `locations` where _disabled=0");
    			if ( count($locationInfoArr) > 0 ) {
    				foreach ( $locationInfoArr as $key => $location ) {
    					$array_data=array("ltg_use_opt_mods","max_advance_days","minimum_notice_time","enable_togo_polling","togo_order_polling_interval","ltg_phone_carrier","ltg_phone_number","ltg_email","ltg_dine_in_option","lls_fetch_api_orders");
    					foreach ($array_data as $data) {
    						$data_2 = $data."_2";
    						$row_2 = $this->fetchRow("SELECT `value` FROM `config` WHERE `setting` = '$data_2'  AND _deleted=0");
    						if($row_2 == false){
				    			$row = $this->fetchRow("SELECT `value` FROM `config` WHERE `setting` = '$data'  AND _deleted=0");
				    			$this->execute("INSERT INTO `config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`, `value15`, `value16`, `value17`) VALUES (".$location['id'].", '".$data_2."', '".$row['value']."', '', '', '', '', 'location_config_setting', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0', '0')");
    						}
    					}
    				}
   				}
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ( $this->hasTable('config') == true ) {
    			$array_data=array("ltg_use_opt_mods","max_advance_days","minimum_notice_time","enable_togo_polling","togo_order_polling_interval","ltg_phone_carrier","ltg_phone_number","ltg_email","ltg_dine_in_option","lls_fetch_api_orders");
    			foreach ($array_data as $data) {
	    			$data_2 = $data."_2";
	    			$row = $this->fetchRow("SELECT `value` FROM `config` WHERE `setting` = '".$data_2."'  AND _deleted=0");
	    			if ($row == true) {
	    				$this->execute("DELETE FROM `config` WHERE setting = '".$data_2."' ");
	    			}
    			}
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
