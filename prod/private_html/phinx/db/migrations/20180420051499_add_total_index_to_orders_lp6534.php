<?php


use Phinx\Migration\AbstractMigration;

class AddTotalIndexToOrdersLp6534 extends AbstractMigration {
  /**
  * up() Method to migrate.
  */
  public function up() {
      if ($this->hasTable('orders')) {
        $indexPresent = $this->getAdapter()->hasIndexByName('orders', 'idx_total') == 1 ? true : false;
        if (!$indexPresent) {
          $table = $this->table('orders');
          $table->addIndex(['total'], ['name' => 'idx_total']);
          $table->save();
        }
      }
    }

  /**
  * down() Method to rollback.
  */
  public function down() {
    if ($this->hasTable('orders')) {
      $indexPresent = $this->getAdapter()->hasIndexByName('orders', 'idx_total') == 1 ? true : false;
      // if the index is present we're able to delete it
      if ($indexPresent) {
        $table = $this->table('orders');
        $table->removeIndex(['total'], ['name' => 'idx_total']);
        $table->save();
      }
    }
  }
}