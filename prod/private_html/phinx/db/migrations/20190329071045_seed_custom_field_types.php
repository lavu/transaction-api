<?php


use Phinx\Migration\AbstractMigration;

class SeedCustomFieldTypes extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            $this->execute("SELECT id INTO @gen_field_source from poslavu_MAIN_db.field_source_types WHERE code = 'GEN';");
            $this->execute("
                INSERT INTO custom_field_types (field_source_type_id, label, name, validator, code)
                VALUES
                    (@gen_field_source, 'Text', 'Text', '^[-\w\s]+$', 'TEXT'),
                    (@gen_field_source, 'Numeric', 'Numeric', '^[+-]?([0-9]*[.])?[0-9]+$', 'NUM'),
                    (@gen_field_source, 'Email', 'Email', '^[^\s@]+@[^\s@]+\.[^\s@]+$', 'EMAIL'),
                    (@gen_field_source, 'Name', 'Name', '(^|\s)[a-z]', 'NAME'),
                    (@gen_field_source, 'Phone', 'Phone', '^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$', 'PHONE');

            ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            $this->execute("DELETE FROM custom_field_types WHERE code IN ('TEXT', 'NUM', 'EMAIL', 'PHONE', 'NAME');");
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
