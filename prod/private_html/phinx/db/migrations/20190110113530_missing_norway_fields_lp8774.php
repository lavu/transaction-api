<?php


use Phinx\Migration\AbstractMigration;

class MissingNorwayFieldsLp8774 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		/* We are trying to add fields as per LP-1913 SQL script */
    	    if ($this->hasTable('tax_rates') == true && $this->table('tax_rates')->hasColumn('norway_vat_code') == false) {
    		    $this->execute("ALTER TABLE `tax_rates` ADD COLUMN `norway_vat_code` VARCHAR(11) DEFAULT '0'");
    		}
    		
    		if ($this->hasTable('menu_categories') == true && $this->table('menu_categories')->hasColumn('norway_menu_mapping') == false) {
    		    $this->execute("ALTER TABLE `menu_categories` ADD COLUMN `norway_menu_mapping` VARCHAR(100) DEFAULT '04999' NOT NULL");
    		}
    		
    		if ($this->hasTable('orders') == true && $this->table('orders')->hasColumn('signature') == false) {
    		    $this->execute("ALTER TABLE `orders` ADD COLUMN `signature` VARCHAR(2000) DEFAULT '' NOT NULL");
    		}
    		
    		if ($this->hasTable('orders') == true && $this->table('orders')->hasColumn('key_version') == false) {
    		    $this->execute("ALTER TABLE `orders` ADD COLUMN `key_version` VARCHAR(5) DEFAULT '1' NOT NULL");
    		}
    		
    		if ($this->hasTable('split_check_details') == true && $this->table('split_check_details')->hasColumn('check_has_printed') == false) {
    		    $this->execute("ALTER TABLE `split_check_details` ADD COLUMN `check_has_printed` TINYINT(4) DEFAULT 0 NOT NULL");
    		}
    		
    		if ($this->hasTable('action_log') == true && $this->table('action_log')->hasColumn('signature') == false) {
    		    $this->execute("ALTER TABLE `action_log` ADD COLUMN `signature` VARCHAR(2000) DEFAULT '' NOT NULL");
    		}
    		
    		if ($this->hasTable('action_log') == true && $this->table('action_log')->hasColumn('key_version') == false) {
    		    $this->execute("ALTER TABLE `action_log` ADD COLUMN `key_version` VARCHAR(5) DEFAULT '1' NOT NULL");
    		}
    		
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    	    
    	    if ($this->hasTable('tax_rates') == true && $this->table('tax_rates')->hasColumn('norway_vat_code') == true) {
    	        $this->execute("ALTER TABLE `tax_rates` DROP COLUMN `norway_vat_code`");
    	    }
    	    
    	    if ($this->hasTable('menu_categories') == true && $this->table('menu_categories')->hasColumn('norway_menu_mapping') == true) {
    	        $this->execute("ALTER TABLE `menu_categories` DROP COLUMN `norway_menu_mapping`");
    	    }
    	    
    	    if ($this->hasTable('orders') == true && $this->table('orders')->hasColumn('signature') == true) {
    	        $this->execute("ALTER TABLE `orders` DROP COLUMN `signature`");
    	    }
    	    
    	    if ($this->hasTable('orders') == true && $this->table('orders')->hasColumn('key_version') == true) {
    	        $this->execute("ALTER TABLE `orders` DROP COLUMN `key_version`");
    	    }
    	    
    	    if ($this->hasTable('split_check_details') == true && $this->table('split_check_details')->hasColumn('check_has_printed') == true) {
    	        $this->execute("ALTER TABLE `split_check_details` DROP COLUMN `check_has_printed`");
    	    }
    	    
    	    if ($this->hasTable('action_log') == true && $this->table('action_log')->hasColumn('signature') == true) {
    	        $this->execute("ALTER TABLE `action_log` DROP COLUMN `signature`");
    	    }
    	    
    	    if ($this->hasTable('action_log') == true && $this->table('action_log')->hasColumn('key_version') == true) {
    	        $this->execute("ALTER TABLE `action_log` DROP COLUMN `key_version`");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
