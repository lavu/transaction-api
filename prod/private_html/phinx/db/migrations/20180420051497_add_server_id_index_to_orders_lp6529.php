<?php


use Phinx\Migration\AbstractMigration;

//    add_server_id_index_to_orders_lp6529
class AddServerIdIndexToOrdersLp6529 extends AbstractMigration {
  /**
  * up() Method to migrate.
  */
  public function up() {
      if ($this->hasTable('orders')) {
        $indexPresent = $this->getAdapter()->hasIndexByName('orders', 'idx_server_id') == 1 ? true : false;
        if (!$indexPresent) {
          $table = $this->table('orders');
          $table->addIndex(['server_id'], ['name' => 'idx_server_id']);
          $table->save();
        }
      }
    }

  /**
  * down() Method to rollback.
  */
  public function down() {
    if ($this->hasTable('orders')) {
      $indexPresent = $this->getAdapter()->hasIndexByName('orders', 'idx_server_id') == 1 ? true : false;
      // if the index is present we're able to delete it
      if ($indexPresent) {
        $table = $this->table('orders');
        $table->removeIndex(['server_id'], ['name' => 'idx_server_id']);
        $table->save();
      }
    }
  }
}