<?php


use Phinx\Migration\AbstractMigration;

class CreateCustomFieldsMetadata extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            $this->execute("
                CREATE TABLE IF NOT EXISTS custom_fields_metadata (
                    id INT (11) unsigned NOT NULL AUTO_INCREMENT,
                    custom_field_type_id INT (11) unsigned NOT NULL,
                    custom_field_id INT (11) unsigned NOT NULL,

                    label VARCHAR(255) NOT NULL UNIQUE,
                    placeholder VARCHAR(255) NOT NULL,
                    is_required tinyint(4) NOT NULL DEFAULT 0,
                    sort INT(11) NOT NULL,

                    status char(1) NOT NULL DEFAULT 'A',
                    created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                    PRIMARY KEY (id),
                    INDEX custom_fields_status (status),

                    CONSTRAINT fk_custom_fields_metadata_fields_id
                        FOREIGN KEY (custom_field_id)
                        REFERENCES custom_fields (id)
                        ON DELETE CASCADE
                        ON UPDATE CASCADE,

                    CONSTRAINT fk_custom_fields_metadata_types_id
                        FOREIGN KEY (custom_field_type_id)
                        REFERENCES custom_field_types (id)
                        ON UPDATE CASCADE

                ) ENGINE=InnoDB;
            ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            $this->execute("DROP TABLE IF EXISTS custom_fields_metadata;");
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
