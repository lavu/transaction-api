<?php


use Phinx\Migration\AbstractMigration;

class CreateCcTransactionsExtendTableLp10420 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            if($this->hasTable("cc_transactions_ext") == false) {
                $this->execute("CREATE TABLE IF NOT EXISTS `cc_transactions_ext` (
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `transaction_id` varchar(255) NOT NULL,
                                `primary_currency_code` varchar(10) NOT NULL,
                                `secondary_currency_code` varchar(10) NOT NULL,
                                `secondary_paid_amount` decimal(30,5) NOT NULL DEFAULT '0.00000',
                                `exchange_rate` float(5) NOT NULL,
                                `_deleted` tinyint(1) NOT NULL DEFAULT '0',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `unq_transaction_id` (`transaction_id`),
                                CONSTRAINT `transaction_id_ibfk_1` FOREIGN KEY (`transaction_id`) REFERENCES `cc_transactions` (`transaction_id`)
                                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            if($this->hasTable("cc_transactions_ext") == true){
                $this->execute("DROP TABLE `cc_transactions_ext`");
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
