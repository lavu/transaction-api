<?php


use Phinx\Migration\AbstractMigration;

class AddNewTableSecondaryExchangeRatesLp6409 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if($this->hasTable('secondary_exchange_rates')==false){
            $this->execute('CREATE TABLE `secondary_exchange_rates` ( `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                                                      `primary_restaurantid` int(11) unsigned NOT NULL,
                                                                      `from_currency_id` int(11) unsigned NOT NULL,
                                                                      `to_currency_id` int(11) unsigned NOT NULL, 
                                                                      `from_currency_code` varchar(255) NOT NULL, 
                                                                      `to_currency_code` varchar(255) NOT NULL, 
                                                                      `rate` float(10,3) unsigned NOT NULL, 
                                                                      `effective_date` timestamp NOT NULL, 
                                                                      `custom` int(1) unsigned NOT NULL, 
                                                                      `_deleted` int(1) unsigned NOT NULL,
                                                                      PRIMARY KEY (`id`) ) ENGINE=InnoDB CHARSET=utf8');
        }

    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if($this->hasTable('secondary_exchange_rates') == true){
            $this->execute('DROP TABLE `secondary_exchange_rates`');
        }
    }
}
