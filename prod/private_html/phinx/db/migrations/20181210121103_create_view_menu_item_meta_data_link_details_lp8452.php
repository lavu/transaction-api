<?php


use Phinx\Migration\AbstractMigration;

class CreateViewMenuItemMetaDataLinkDetailsLp8452 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ( $this->hasTable('menu_item_meta_data_link') == true ) {
    		    $view = "create view `menu_item_meta_data_link_details` as 
                    select `m_mmd`.`id` as `meta_data_id`,
                    `m_mmd`.`code` as `meta_data_code`,
                    `m_mmd`.`name` as `meta_data_name`,
                    `m_mmd`.`description` as `meta_data_description`,
                    `l_mimdl`.`menu_item_id` as `menu_item_id`,
                    `l_mimdl`.`value` as `meta_data_link_value`,
                    `l_mimdl`.`_deleted` as `meta_data_link_deleted`
                    from `".$this->getOptions()['name']."`.`menu_item_meta_data_link` as `l_mimdl`
                    inner join `poslavu_MAIN_db`.`menu_meta_data` as `m_mmd` on `l_mimdl`.`meta_data_id` = `m_mmd`.`id`
                    where `l_mimdl`.`_deleted` <> '1'";
    		    
    		    $this->execute($view);
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$this->execute("drop view if exists menu_item_meta_data_link_details");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
