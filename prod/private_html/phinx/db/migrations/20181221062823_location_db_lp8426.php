<?php

use Phinx\Migration\AbstractMigration;

class LocationDbLp8426 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		/* Write migration code here */
            if($this->hasTable('orders')){
                        $action = (!$this->table('orders')->hasColumn('invoice_number'))?'ADD':'MODIFY';
                        $this->execute('ALTER TABLE orders '.$action.' COLUMN invoice_number VARCHAR(50) NOT NULL DEFAULT \'\'');
            }
            if($this->hasTable('split_check_details')){
                $action = (!$this->table('orders')->hasColumn('invoice_number'))?'ADD':'MODIFY';
                $this->execute('ALTER TABLE split_check_details '.$action.' COLUMN invoice_number VARCHAR(50) NOT NULL DEFAULT \'\'');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            /* Write rollback code here */
            if($this->hasTable('orders') && $this->table('orders')->hasColumn('invoice_number')){
                    $this->execute('ALTER TABLE orders MODIFY COLUMN invoice_number VARCHAR(50) NOT NULL DEFAULT \'\'');
            }
            if($this->hasTable('split_check_details') && $this->table('split_check_details')->hasColumn('invoice_number')){
                    $this->execute('ALTER TABLE split_check_details MODIFY COLUMN invoice_number VARCHAR(50) NOT NULL DEFAULT \'\'');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
