<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp7922 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
	$this->execute("SET FOREIGN_KEY_CHECKS = 0");
	if ( $this->hasTable('order_contents') == true && $this->table('order_contents')->hasColumn('ordertype_id') == true) {
	    try	{
		$this->execute("ALTER TABLE `order_contents` DROP FOREIGN KEY `FK_OCOrderTagsId`");
		$this->execute("ALTER TABLE `order_contents` MODIFY `ordertype_id` varchar(255) NOT NULL");
	    } catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	    }
	} else {
	    try {
		$this->execute("ALTER TABLE `order_contents` ADD COLUMN `ordertype_id` varchar(255) NOT NULL");
	    } catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	    }
	}

	if ( $this->hasTable('modifiers_used') == true && $this->table('modifiers_used')->hasColumn('ordertype_id') == true) {
	    try {
		$this->execute("ALTER TABLE `modifiers_used` DROP FOREIGN KEY `FK_MUOrderTagsId`");
		$this->execute("ALTER TABLE `modifiers_used` MODIFY `ordertype_id` varchar(255) NOT NULL");
	    } catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	    }
	} else {
	    try {
		$this->execute("ALTER TABLE `modifiers_used` ADD COLUMN `ordertype_id` varchar(255) NOT NULL");
	    } catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	    }
	}
	$this->execute("SET FOREIGN_KEY_CHECKS = 1");
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
		$this->execute("SET FOREIGN_KEY_CHECKS = 0");
    		if ( $this->hasTable('order_contents') == true && $this->table('order_contents')->hasColumn('ordertype_id') == true) {
		    $this->execute('ALTER TABLE order_contents DROP COLUMN `ordertype_id`');
    		}
		if ( $this->hasTable('modifiers_used') == true && $this->table('modifiers_used')->hasColumn('ordertype_id') == true) {
		    $this->execute('ALTER TABLE modifiers_used DROP COLUMN `ordertype_id`');
		}
		$this->execute("SET FOREIGN_KEY_CHECKS = 1");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
