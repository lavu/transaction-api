<?php


use Phinx\Migration\AbstractMigration;

class PickupSeqTableLp6865 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ( $this->hasTable('pickup_seq') == false ) {
            $this->execute("CREATE TABLE `pickup_seq` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `server_id` varchar(255) NOT NULL,
                              `order_id` varchar(255) NOT NULL,
                              `pickup_num` int(11) NOT NULL DEFAULT '0',
                              `enddate` datetime NOT NULL,
                              `created_date` datetime NOT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `uniq_order_id` (`order_id`),
                              KEY `idx_server_id` (`server_id`),
                              KEY `idx_enddate` (`enddate`)
                            ) ENGINE=InnoDB CHARSET=utf8");
        }

    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ( $this->hasTable('pickup_seq') == true ) {
            $this->execute('DROP TABLE `pickup_seq`');
        }
    }
}