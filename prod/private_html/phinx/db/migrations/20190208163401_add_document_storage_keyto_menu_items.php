<?php


use Phinx\Migration\AbstractMigration;

class AddDocumentStorageKeytoMenuItems extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_items') && !$this->table('menu_items')->hasColumn('storage_key')) {
                $this->execute("ALTER TABLE `menu_items`
                                ADD COLUMN `storage_key`
                                VARCHAR(200)");
                                
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }


    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_items') && $this->table('menu_items')->hasColumn('storage_key')) {
                $this->execute("ALTER TABLE `menu_items` DROP COLUMN `storage_key`");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
