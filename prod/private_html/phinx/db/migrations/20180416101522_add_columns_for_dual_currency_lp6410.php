<?php


use Phinx\Migration\AbstractMigration;

class AddColumnsForDualCurrencyLp6410 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable('split_check_details') == true && $this->table('split_check_details')->hasColumn('secondary_currency_total') == false) {
            $this->execute("ALTER TABLE `split_check_details` ADD COLUMN `secondary_currency_total` varchar(10) NOT NULL");
        }
        
        if ( $this->hasTable('orders') == true
            && $this->table('orders')->hasColumn('secondary_currency_code') == false
            && $this->table('orders')->hasColumn('secondary_currency_code_id') == false
            && $this->table('orders')->hasColumn('secondary_monitary_symbol') == false
            && $this->table('orders')->hasColumn('exchange_rate_id') == false
            && $this->table('orders')->hasColumn('exchange_rate_value') == false
            && $this->table('orders')->hasColumn('secondary_currency_receipt_label') == false
            )
        {
            $this->execute("ALTER TABLE `orders` ADD COLUMN `secondary_currency_code` varchar(50) NOT NULL,
                                                 ADD COLUMN `secondary_currency_code_id` int(11) NOT NULL,
                                                 ADD COLUMN `secondary_monitary_symbol` varchar(10) NOT NULL,
                                                 ADD COLUMN `exchange_rate_id` int(11) NOT NULL,
                                                 ADD COLUMN `exchange_rate_value` varchar(10) NOT NULL,
                                                 ADD COLUMN `secondary_currency_receipt_label` varchar(255) NOT NULL");
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        
        if ($this->hasTable('split_check_details') == true && $this->table('split_check_details')->hasColumn('secondary_currency_total') == true) {
            $this->execute("ALTER TABLE `split_check_details` DROP COLUMN `secondary_currency_total`");
        }
        
        if ( $this->hasTable('orders') == true
            && $this->table('orders')->hasColumn('secondary_currency_code') == true
            && $this->table('orders')->hasColumn('secondary_currency_code_id') == true
            && $this->table('orders')->hasColumn('secondary_monitary_symbol') == true
            && $this->table('orders')->hasColumn('exchange_rate_id') == true
            && $this->table('orders')->hasColumn('exchange_rate_value') == true
            && $this->table('orders')->hasColumn('secondary_currency_receipt_label') == true
            )
        {
            $this->execute("ALTER TABLE `orders` DROP `secondary_currency_code`,
                                                 DROP `secondary_currency_code_id`,
                                                 DROP `secondary_monitary_symbol`,
                                                 DROP `exchange_rate_id`,
                                                 DROP `exchange_rate_value`,
                                                 DROP `secondary_currency_receipt_label`");
        }
    }
}
