<?php


use Phinx\Migration\AbstractMigration;

class AddMenuItemIndexLp10215 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_items') && $this->table('menu_items')->hasColumn('track_86_count')) {
                $rowData = $this->fetchRow("SHOW INDEX FROM `menu_items` WHERE Key_name = 'ix_track_86_count'");
                if (!isset($rowData['Key_name'])) {
                    $this->execute("ALTER TABLE `menu_items` ADD INDEX `ix_track_86_count` (`track_86_count`)");
                }
            }
            if ($this->hasTable('alternate_payment_totals') && $this->table('alternate_payment_totals')->hasColumn('ioid')) {
                $rowData = $this->fetchRow("SHOW INDEX FROM `alternate_payment_totals` WHERE Key_name = 'ix_ioid'");
                if (!isset($rowData['Key_name'])) {
                    $this->execute("ALTER TABLE `alternate_payment_totals` ADD INDEX `ix_ioid` (`ioid`)");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_items') && $this->table('menu_items')->hasColumn('track_86_count')) {
                $rowData = $this->fetchRow("SHOW INDEX FROM `menu_items` WHERE Key_name = 'ix_track_86_count' ");
                if (isset($rowData['Key_name'])) {
                    $this->execute("ALTER TABLE `menu_items` DROP INDEX `ix_track_86_count`");
                }
            }
            if ($this->hasTable('alternate_payment_totals') && $this->table('alternate_payment_totals')->hasColumn('ioid')) {
                $rowData = $this->fetchRow("SHOW INDEX FROM `alternate_payment_totals` WHERE Key_name = 'ix_ioid' ");
                if (isset($rowData['Key_name'])) {
                    $this->execute("ALTER TABLE `alternate_payment_totals` DROP INDEX `ix_ioid` ");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
