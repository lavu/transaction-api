<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp5183 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('payment_gateway_logs') == false ) {
               $this->execute("CREATE TABLE `payment_gateway_logs` (
                  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                  `order_id` varchar(255) DEFAULT NULL,
                  `check` int(11) NOT NULL,
                  `loc_id` int(11) NOT NULL,
                  `amount` decimal(30,5) NOT NULL DEFAULT '0.00000',
                  `request_data` text COMMENT 'payload sent to the payment gateway',
                  `response_data` text COMMENT 'response from payment gateway',
                  `pay_type` varchar(255) DEFAULT NULL COMMENT 'paytype - sale, auth',
                  `request_timestamp` timestamp NULL DEFAULT NULL COMMENT 'time when request was sent',
                  `response_timestamp` timestamp NULL DEFAULT NULL COMMENT 'time when response recieved',
                  `payment_status` int(11) NOT NULL COMMENT '0- Process, 1-Approved, 2- Declined, Failed ...',
                  `process_status` int(11) NOT NULL COMMENT '1.Pos - BE,2.BE - Gateway,3.Gateway - BE,4.BE - Pos,5.Pos - BE(acknowledge)',
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
           }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if($this->hasTable('payment_gateway_logs') == true) {
                $this->execute('DROP TABLE `payment_gateway_logs`');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
