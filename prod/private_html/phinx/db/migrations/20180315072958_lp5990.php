<?php
use Phinx\Migration\AbstractMigration;

class Lp5990 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	if ($this->hasTable('config')) {
		//To delete fields, if already exits.
    	$this->execute("DELETE FROM `config` WHERE setting in('print_pickup_preference', 'print_pickup_ordertype')");
       // inserting only one rows
    	$configDetails = [
			    			[
			    					'location' => 1,'setting' => 'print_pickup_preference','value' => '0','value2' => '','value3' => '','value4' => '','value_long' => '','type' => 'location_config_setting','value5' => '','value6' => '','value7' => '','value8' => '','_deleted' => '0','value9' => '','value10' => '','value11' => '','value12' => '','value13' => '','value14' => '','value_long2' => '','value15' => ''
			    			],
			    			[
			    					'location' => 1,'setting' => 'print_pickup_ordertype','value' => 'Quick Serve,Tables,Tabs,Kiosk,LTG','value2' => '','value3' => '','value4' => '','value_long' => '','type' => 'location_config_setting','value5' => '','value6' => '','value7' => '','value8' => '','_deleted' => '0','value9' => '','value10' => '','value11' => '','value12' => '','value13' => '','value14' => '','value_long2' => '','value15' => ''
			    	
			    			]
    	
    					];
                $table = $this->table ( 'config' );
                $table->insert ( $configDetails )->saveData ();
       }
     }
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        // Rollback from 'config' table;
		if ($this->hasTable('config')) {
		$this->execute("DELETE FROM `config` WHERE setting in('print_pickup_preference', 'print_pickup_ordertype')");
		}

    }
    
}