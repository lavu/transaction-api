<?php


use Phinx\Migration\AbstractMigration;

class SeedNameToCustomFields extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */

    public function up()
    {
    	$this->status = true;
    	try {
            $this->execute("
                INSERT INTO custom_fields(location_id, label, sort, is_default)
                VALUES (1, 'Your information', 1, 1);
            ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            /* Write rollback code here */
            $this->execute("
                DELETE FROM custom_fields WHERE label = 'Your information';
            ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
