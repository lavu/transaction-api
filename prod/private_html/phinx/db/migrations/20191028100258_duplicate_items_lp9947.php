<?php


use Phinx\Migration\AbstractMigration;

class DuplicateItemsLp9947 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {

    		if ($this->hasTable('order_contents') && $this->table('order_contents')->hasColumn('order_id') && $this->table('order_contents')->hasColumn('ioid') && $this->table('order_contents')->hasColumn('icid')) {
                $rowData = $this->fetchRow("SHOW INDEX FROM `order_contents` WHERE Key_name = 'ix_unique_icid'");
                if (!isset($rowData['Key_name'])) {
                    $this->execute("ALTER IGNORE TABLE `order_contents` ADD UNIQUE KEY `ix_unique_icid` (`order_id`, `ioid`, `icid`)");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('order_contents') && $this->table('order_contents')->hasColumn('order_id') && $this->table('order_contents')->hasColumn('ioid') && $this->table('order_contents')->hasColumn('icid')) {
                $rowData = $this->fetchRow("SHOW INDEX FROM `order_contents` WHERE Key_name = 'ix_unique_icid'");
                if (isset($rowData['Key_name'])) {
                    $this->execute("ALTER TABLE `order_contents` DROP INDEX `ix_unique_icid`");;
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
