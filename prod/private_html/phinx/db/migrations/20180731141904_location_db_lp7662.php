<?php
use Phinx\Migration\AbstractMigration;

class LocationDbLp7662 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('users') == true && $this->table('users')->hasColumn('settings') == false) {
			$this->execute("ALTER TABLE `users` ADD COLUMN `settings` LONGTEXT NOT NULL CHECK (JSON_VALID(`settings`))");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('users') == true && $this->table('users')->hasColumn('settings') == true) {
    			$this->execute("ALTER TABLE `users` DROP COLUMN `settings`");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
