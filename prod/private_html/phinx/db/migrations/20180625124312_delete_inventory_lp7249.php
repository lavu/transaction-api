<?php


use Phinx\Migration\AbstractMigration;

class DeleteInventoryLp7249 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
		    if ($this->hasTable('inventory_items_backup')==false) {
			    $this->execute('CREATE TABLE `inventory_items_backup` (
				  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `inventoryItemID` int(11) unsigned DEFAULT NULL,
				  `name` varchar(255) DEFAULT NULL,
				  `categoryID` int(11) unsigned DEFAULT NULL,
				  `salesUnitID` int(11) unsigned DEFAULT NULL,
				  `recentPurchaseQuantity` double unsigned DEFAULT NULL,
				  `recentPurchaseUnitID` int(11) unsigned DEFAULT NULL,
				  `quantityOnHand` double unsigned DEFAULT NULL,
				  `recentPurchaseCost` double unsigned DEFAULT NULL,
				  `lowQuantity` double unsigned NOT NULL DEFAULT 0,
				  `reOrderQuantity` double unsigned NOT NULL DEFAULT 0,
				  `criticalItem` int(11) unsigned DEFAULT NULL,
				  `_deleted` int(11) unsigned NOT NULL DEFAULT 0,
				  `primaryVendorID` int(11) unsigned DEFAULT NULL,
				  `storageLocation` int(11) unsigned DEFAULT NULL,
				  `inventoryOverage` double unsigned DEFAULT 0,
				  `backup_by` varchar(150) NOT NULL DEFAULT \'\',
				  `backup_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  KEY `FK` (`categoryID`,`salesUnitID`,`recentPurchaseUnitID`,`primaryVendorID`,`storageLocation`)
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;');
		    }

		    if ($this->hasTable('inventory_audit_backup')==false) {
			    $this->execute('CREATE TABLE `inventory_audit_backup` (
				  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `inventoryItemID` int(11) unsigned DEFAULT NULL,
				  `action` varchar(255) NOT NULL DEFAULT \'\',
				  `quantity` int(11) unsigned DEFAULT NULL,
				  `unitID` int(11) unsigned DEFAULT NULL,
				  `cost` double unsigned DEFAULT NULL,
				  `userID` int(11) unsigned DEFAULT NULL,
				  `userNotes` varchar(255) NOT NULL DEFAULT \'\',
				  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  `transferItemID` int(11) unsigned DEFAULT 0,
				  `_deleted` int(11) unsigned NOT NULL DEFAULT 0,
				  `backup_by` varchar(150) NOT NULL DEFAULT \'\',
				  `backup_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  KEY `FK` (`inventoryItemID`,`unitID`)
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;');
		    }

		    if ($this->hasTable('menuitem_86_backup')==false) {
			    $this->execute('CREATE TABLE `menuitem_86_backup` (
				  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `menuItemID` int(11) unsigned DEFAULT NULL,
				  `inventoryItemID` int(11) unsigned DEFAULT NULL,
				  `quantityUsed` double unsigned DEFAULT 0,
				  `criticalItem` int(11) unsigned DEFAULT NULL,
				  `unitID` int(11) unsigned DEFAULT NULL,
				  `reservation` double unsigned DEFAULT NULL,
				  `_deleted` int(11) unsigned NOT NULL DEFAULT 0,
				  `backup_by` varchar(150) NOT NULL DEFAULT \'\',
				  `backup_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  KEY `FK` (`inventoryItemID`,`unitID`)
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;');
		    }

		    if ($this->hasTable('forced_modifier_86_backup')==false) {
			    $this->execute('CREATE TABLE `forced_modifier_86_backup` (
				  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `forced_modifierID` int(11) unsigned DEFAULT NULL,
				  `inventoryItemID` int(11) unsigned DEFAULT NULL,
				  `quantityUsed` double unsigned DEFAULT NULL,
				  `reservation` int(11) DEFAULT NULL,
				  `criticalItem` int(11) unsigned DEFAULT NULL,
				  `unitID` int(11) unsigned DEFAULT NULL,
				  `_deleted` int(11) unsigned NOT NULL DEFAULT 0,
				  `backup_by` varchar(150) NOT NULL DEFAULT \'\',
				  `backup_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  KEY `FK` (`inventoryItemID`,`unitID`)
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;');
		    }

		    if ($this->hasTable('modifier_86_backup')==false) {
			    $this->execute('CREATE TABLE `modifier_86_backup` (
				  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
				  `modifierID` int(11) unsigned DEFAULT NULL,
				  `inventoryItemID` int(11) unsigned DEFAULT NULL,
				  `quantityUsed` double unsigned DEFAULT NULL,
				  `reservation` int(11) DEFAULT NULL,
				  `criticalItem` int(11) unsigned DEFAULT NULL,
				  `unitID` int(11) unsigned DEFAULT NULL,
				  `_deleted` int(11) unsigned NOT NULL DEFAULT 0,
				  `backup_by` varchar(150) NOT NULL DEFAULT \'\',
				  `backup_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  KEY `FK` (`inventoryItemID`,`unitID`)
				) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;');
		    }
    		/* Write migration code here */
    	}
    	catch (PDOException $exception) {
            $this->status = false;

            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
		    if($this->hasTable('inventory_items_backup')){
			    $this->execute('DROP TABLE inventory_items_backup');
		    }

		    if($this->hasTable('inventory_audit_backup')){
			    $this->execute('DROP TABLE inventory_audit_backup');
		    }
		    if($this->hasTable('menuitem_86_backup')){
			    $this->execute('DROP TABLE menuitem_86_backup');
		    }

		    if($this->hasTable('forced_modifier_86_backup')){
			    $this->execute('DROP TABLE forced_modifier_86_backup');
		    }
		    if($this->hasTable('modifier_86_backup')){
			    $this->execute('DROP TABLE modifier_86_backup');
		    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
