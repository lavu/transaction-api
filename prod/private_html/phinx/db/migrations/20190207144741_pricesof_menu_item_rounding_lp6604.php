<?php


use Phinx\Migration\AbstractMigration;

class PricesofMenuItemRoundingLp6604 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {

			$menuTable = 'menu_items';
			if ($this->hasTable($menuTable) == true && $this->table($menuTable)->hasColumn('price') == true) {
				$this->execute('ALTER TABLE '.$menuTable.' MODIFY COLUMN `price` DECIMAL(30, 5) NOT NULL DEFAULT 0');
			}

			$orderContentsTable = 'order_contents';
			if ($this->hasTable($orderContentsTable) == true && $this->table($orderContentsTable)->hasColumn('price') == true && $this->table($orderContentsTable)->hasColumn('modify_price') == true && $this->table($orderContentsTable)->hasColumn('forced_modifiers_price') == true) {
				$this->execute('ALTER TABLE '.$orderContentsTable.' MODIFY COLUMN `price` VARCHAR(30) NOT NULL, MODIFY COLUMN `modify_price` VARCHAR(30) NOT NULL, MODIFY COLUMN `forced_modifiers_price` VARCHAR(30) NOT NULL');
			}

			$ordersTable = 'orders';
			if ($this->hasTable($ordersTable) == true && $this->table($ordersTable)->hasColumn('subtotal') == true && $this->table($ordersTable)->hasColumn('tax') == true && $this->table($ordersTable)->hasColumn('total') == true && $this->table($ordersTable)->hasColumn('gratuity') == true && $this->table($ordersTable)->hasColumn('card_gratuity') == true && $this->table($ordersTable)->hasColumn('cash_paid') == true && $this->table($ordersTable)->hasColumn('card_paid') == true && $this->table($ordersTable)->hasColumn('gift_certificate') == true&& $this->table($ordersTable)->hasColumn('change_amount') == true ) {
				$this->execute('ALTER TABLE '.$ordersTable.' MODIFY COLUMN `subtotal` VARCHAR(30) NOT NULL DEFAULT 0, MODIFY COLUMN `tax` VARCHAR(30) NOT NULL DEFAULT 0, MODIFY COLUMN `total` VARCHAR(30) NOT NULL DEFAULT 0, MODIFY COLUMN `gratuity` VARCHAR(30) NOT NULL, MODIFY COLUMN `card_gratuity` VARCHAR(30) NOT NULL, MODIFY COLUMN `cash_paid` VARCHAR(30) NOT NULL DEFAULT "",MODIFY COLUMN `card_paid` VARCHAR(30) NOT NULL DEFAULT "", MODIFY COLUMN `gift_certificate` VARCHAR(30) NOT NULL, MODIFY COLUMN `change_amount` VARCHAR(30) NOT NULL DEFAULT ""');
			}

			$splitCheckTable = 'split_check_details';
			if ($this->hasTable($splitCheckTable) == true && $this->table($splitCheckTable)->hasColumn('subtotal') == true && $this->table($splitCheckTable)->hasColumn('discount') == true && $this->table($splitCheckTable)->hasColumn('tax') == true && $this->table($splitCheckTable)->hasColumn('gratuity') == true &&$this->table($splitCheckTable)->hasColumn('cash') == true && $this->table($splitCheckTable)->hasColumn('card') == true && $this->table($splitCheckTable)->hasColumn('gift_certificate') == true && $this->table($splitCheckTable)->hasColumn('refund') == true && $this->table($splitCheckTable)->hasColumn('remaining') == true && $this->table($splitCheckTable)->hasColumn('check_total') == true && $this->table($splitCheckTable)->hasColumn('change') == true && $this->table($splitCheckTable)->hasColumn('refund_cc') == true && $this->table($splitCheckTable)->hasColumn('secondary_currency_total') == true) {

				$this->execute('ALTER TABLE '.$splitCheckTable.' MODIFY COLUMN `subtotal` VARCHAR(30) NOT NULL, MODIFY COLUMN `discount` VARCHAR(30) NOT NULL, MODIFY COLUMN `tax` VARCHAR(30) NOT NULL, MODIFY COLUMN `gratuity` VARCHAR(30) NOT NULL, MODIFY COLUMN `cash` VARCHAR(30) NOT NULL, MODIFY COLUMN `card` VARCHAR(30) NOT NULL, MODIFY COLUMN `gift_certificate` VARCHAR(30) NOT NULL, MODIFY COLUMN `refund` VARCHAR(30) NOT NULL, MODIFY COLUMN `remaining` VARCHAR(30) NOT NULL, MODIFY COLUMN `check_total` VARCHAR(30) NOT NULL, MODIFY COLUMN `change` VARCHAR(30) NOT NULL, MODIFY COLUMN `refund_cc` VARCHAR(30) NOT NULL, MODIFY COLUMN `secondary_currency_total` VARCHAR(30) NOT NULL');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {

			$menuTable = 'menu_items';
			if ($this->hasTable($menuTable) == true && $this->table($menuTable)->hasColumn('price') == true) {
				$this->execute('ALTER TABLE '.$menuTable.' MODIFY COLUMN `price` FLOAT NOT NULL DEFAULT 0');
			}

			$orderContentsTable = 'order_contents';
			if ($this->hasTable($orderContentsTable) == true && $this->table($orderContentsTable)->hasColumn('price') == true && $this->table($orderContentsTable)->hasColumn('modify_price') == true && $this->table($orderContentsTable)->hasColumn('forced_modifiers_price') == true) {
				$this->execute('ALTER TABLE '.$orderContentsTable.' MODIFY COLUMN `price` VARCHAR(10) NOT NULL, MODIFY COLUMN `modify_price` VARCHAR(10) NOT NULL, MODIFY COLUMN `forced_modifiers_price` VARCHAR(10) NOT NULL');
			}

			$ordersTable = 'orders';
			if ($this->hasTable($ordersTable) == true && $this->table($ordersTable)->hasColumn('subtotal') == true && $this->table($ordersTable)->hasColumn('tax') == true && $this->table($ordersTable)->hasColumn('total') == true && $this->table($ordersTable)->hasColumn('gratuity') == true && $this->table($ordersTable)->hasColumn('card_gratuity') == true && $this->table($ordersTable)->hasColumn('cash_paid') == true && $this->table($ordersTable)->hasColumn('card_paid') == true && $this->table($ordersTable)->hasColumn('gift_certificate') == true&& $this->table($ordersTable)->hasColumn('change_amount') == true ) {
				$this->execute('ALTER TABLE '.$ordersTable.' MODIFY COLUMN `subtotal` VARCHAR(10) NOT NULL DEFAULT 0, MODIFY COLUMN `tax` VARCHAR(10) NOT NULL DEFAULT 0, MODIFY COLUMN `total` VARCHAR(10) NOT NULL DEFAULT 0, MODIFY COLUMN `gratuity` VARCHAR(10) NOT NULL, MODIFY COLUMN `card_gratuity` VARCHAR(10) NOT NULL, MODIFY COLUMN `cash_paid` VARCHAR(10) NOT NULL DEFAULT "", MODIFY COLUMN `card_paid` VARCHAR(10) NOT NULL DEFAULT "", MODIFY COLUMN `gift_certificate` VARCHAR(10) NOT NULL, MODIFY COLUMN `change_amount` VARCHAR(10) NOT NULL DEFAULT ""');
			}

			$splitCheckTable = 'split_check_details';
			if ($this->hasTable($splitCheckTable) == true && $this->table($splitCheckTable)->hasColumn('subtotal') == true && $this->table($splitCheckTable)->hasColumn('discount') == true && $this->table($splitCheckTable)->hasColumn('tax') == true && $this->table($splitCheckTable)->hasColumn('gratuity') == true &&$this->table($splitCheckTable)->hasColumn('cash') == true && $this->table($splitCheckTable)->hasColumn('card') == true && $this->table($splitCheckTable)->hasColumn('gift_certificate') == true && $this->table($splitCheckTable)->hasColumn('refund') == true && $this->table($splitCheckTable)->hasColumn('remaining') == true && $this->table($splitCheckTable)->hasColumn('check_total') == true && $this->table($splitCheckTable)->hasColumn('change') == true && $this->table($splitCheckTable)->hasColumn('refund_cc') == true && $this->table($splitCheckTable)->hasColumn('secondary_currency_total') == true) {
				$this->execute('ALTER TABLE '.$splitCheckTable.' MODIFY COLUMN `subtotal` VARCHAR(10) NOT NULL, MODIFY COLUMN `discount` VARCHAR(10) NOT NULL, MODIFY COLUMN `tax` VARCHAR(10) NOT NULL, MODIFY COLUMN `gratuity` VARCHAR(10) NOT NULL, MODIFY COLUMN `cash` VARCHAR(10) NOT NULL, MODIFY COLUMN `card` VARCHAR(10) NOT NULL, MODIFY COLUMN `gift_certificate` VARCHAR(10) NOT NULL, MODIFY COLUMN `refund` VARCHAR(10) NOT NULL, MODIFY COLUMN `remaining` VARCHAR(10) NOT NULL, MODIFY COLUMN `check_total` VARCHAR(10) NOT NULL, MODIFY COLUMN `change` VARCHAR(10) NOT NULL, MODIFY COLUMN `refund_cc` VARCHAR(10) NOT NULL, MODIFY COLUMN `secondary_currency_total` VARCHAR(10) NOT NULL');
			}

		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}