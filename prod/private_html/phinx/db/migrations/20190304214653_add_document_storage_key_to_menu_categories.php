<?php


use Phinx\Migration\AbstractMigration;

class AddDocumentStorageKeyToMenuCategories extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_categories') && !$this->table('menu_categories')->hasColumn('storage_key')) {
                $this->execute("ALTER TABLE `menu_categories`
                                ADD COLUMN `storage_key`
                                VARCHAR(200)");
                                
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }

    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_categories') && $this->table('menu_categories')->hasColumn('storage_key')) {
                $this->execute("ALTER TABLE `menu_categories` DROP COLUMN `storage_key`");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }

    }
}
