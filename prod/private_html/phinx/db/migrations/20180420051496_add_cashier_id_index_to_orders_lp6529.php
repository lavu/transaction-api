<?php


use Phinx\Migration\AbstractMigration;

//    add_cashier_id_index_to_orders_lp6529
class AddCashierIdIndexToOrdersLp6529 extends AbstractMigration {
  /**
  * up() Method to migrate.
  */
  public function up() {
      if ($this->hasTable('orders')) {
        $indexPresent = $this->getAdapter()->hasIndexByName('orders', 'idx_cashier_id') == 1 ? true : false;
        if (!$indexPresent) {
          $table = $this->table('orders');
          $table->addIndex(['cashier_id'], ['name' => 'idx_cashier_id']);
          $table->save();
        }
      }
    }

  /**
  * down() Method to rollback.
  */
  public function down() {
    if ($this->hasTable('orders')) {
      $indexPresent = $this->getAdapter()->hasIndexByName('orders', 'idx_cashier_id') == 1 ? true : false;
      // if the index is present we're able to delete it
      if ($indexPresent) {
        $table = $this->table('orders');
        $table->removeIndex(['cashier_id'], ['name' => 'idx_cashier_id']);
        $table->save();
      }
    }
  }
}