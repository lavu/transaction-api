<?php


use Phinx\Migration\AbstractMigration;

class ComboBuilderTaxProfileIdLp5977 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $tableExists = $this->hasTable('order_contents');
        $table = $this->table('order_contents');
        $columnExists = $table->hasColumn('combo_tax_profile_id');
        
        if ($tableExists == true && $columnExists == false) {
            $this->execute('ALTER TABLE `order_contents` ADD COLUMN `combo_tax_profile_id` VARCHAR(10) NOT NULL');
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $tableExists = $this->hasTable('order_contents');
        $table = $this->table('order_contents');
        $columnExists = $table->hasColumn('combo_tax_profile_id');
        
        if ($tableExists && $columnExists) {
            $this->execute('ALTER TABLE `order_contents` DROP COLUMN `combo_tax_profile_id`');
        }
    }
}
