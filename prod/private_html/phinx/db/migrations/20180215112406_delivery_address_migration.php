<?php


use Phinx\Migration\AbstractMigration;

class DeliveryAddressMigration extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $ordersTableExists = $this->hasTable('orders');
        if ($ordersTableExists == true && $this->table('orders')->hasColumn('delivery_address') == false) {
            $this->execute('ALTER TABLE `orders` ADD COLUMN `delivery_address` varchar(500) NOT NULL');
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $ordersTableExists = $this->hasTable('orders');
        if ($ordersTableExists == true && $this->table('orders')->hasColumn('delivery_address') == true) {
            $this->execute('ALTER TABLE `orders` DROP COLUMN `delivery_address` ');
        }
    }
}
