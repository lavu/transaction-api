<?php


use Phinx\Migration\AbstractMigration;

class LavuGiftLp4860 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable('forced_modifiers')) {
            $row = $this->fetchRow("SELECT * FROM `forced_modifiers` WHERE `extra2` ='lavugift' AND `extra5`='0x0x0x0|120x154x800x460'");
            if ($row == false) {
                $this->execute("update `forced_modifiers` set `extra5`='0x0x0x0|120x154x800x460' where `extra2` ='lavugift' AND `extra5`='0x0x0x0|353x154x318x460'");
            }
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable('forced_modifiers')) {
            $row = $this->fetchRow("SELECT * FROM `forced_modifiers` WHERE `extra2` ='lavugift' AND `extra5`='0x0x0x0|353x154x318x460'");
            if ($row == false) {
                $this->execute("update `forced_modifiers` set `extra5`='0x0x0x0|353x154x318x460' where `extra2` ='lavugift' AND `extra5`='0x0x0x0|120x154x800x460'");
            }
        }
    }
}
