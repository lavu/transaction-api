<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp8639 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		/* Write migration code here */
		$table = $this->table('action_log');
		if ($table && !$table->hasColumn('code')) {
			$this->execute('ALTER TABLE action_log ADD COLUMN code VARCHAR(100) , ADD COLUMN ref_data TEXT NULL');
		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		/* Write rollback code here */
		$table = $this->table('action_log');
		if ($table && $table->hasColumn('code')) {
			$this->execute('ALTER TABLE action_log DROP COLUMN code, DROP COLUMN ref_data');
		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
