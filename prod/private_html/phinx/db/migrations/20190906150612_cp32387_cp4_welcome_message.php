<?php


use Phinx\Migration\AbstractMigration;

class Cp32387Cp4WelcomeMessage extends AbstractMigration
{
    public $status;
    public $tableName = 'users';

    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable($this->tableName) && !$this->table($this->tableName)->hasColumn('viewed_welcome_message')) {
                $this->execute('ALTER TABLE '.$this->tableName.' ADD COLUMN viewed_welcome_message tinyint(1) DEFAULT 0');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable($this->tableName)) {
                if ($this->table($this->tableName)->hasColumn('viewed_welcome_message')) {
                    $this->execute('ALTER TABLE '. $this->tableName . ' DROP COLUMN viewed_welcome_message');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
