<?php


use Phinx\Migration\AbstractMigration;

class PendingLipTransactionsCheckLp9238 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('lip_order_transaction')) {
                if (!$this->table('lip_order_transaction')->hasColumn('amount')) {
                    $this->execute("ALTER TABLE lip_order_transaction ADD COLUMN amount decimal(30, 5) NOT NULL DEFAULT '0.00000'");
                }
                if (!$this->table('lip_order_transaction')->hasColumn('terminal_id')) {
                    $this->execute("ALTER TABLE lip_order_transaction ADD COLUMN terminal_id varchar(255) NOT NULL DEFAULT ''");
                }
                $this->execute("ALTER TABLE `lip_order_transaction` ADD INDEX `idx_order_id` (`order_id`), ADD INDEX `idx_transaction_id` (`transaction_id`)");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('lip_order_transaction')) {
                if ($this->table('lip_order_transaction')->hasColumn('amount')) {
                    $this->execute('ALTER TABLE `lip_order_transaction` DROP COLUMN `amount`');
                }
                if (!$this->table('lip_order_transaction')->hasColumn('terminal_id')) {
                    $this->execute('ALTER TABLE `lip_order_transaction` DROP COLUMN  `terminal_id`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
