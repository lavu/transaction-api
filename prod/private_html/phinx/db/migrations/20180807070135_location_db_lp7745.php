<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp7745 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    	    
    	    if ($this->hasTable('order_tags')) {
    	        $row = $this->fetchRow("SELECT `id` FROM `order_tags` WHERE LOWER(`name`) = 'dine in'");
    	        if (!isset($row['id'])) {
    	            $dineInNameRow = $this->fetchRow("SELECT `value` FROM `config` WHERE `setting` = 'dine_in_order_type_name' AND `type` = 'location_config_setting'");
    	            $dineInActiveRow = $this->fetchRow("SELECT `value` FROM `config` WHERE `setting` = 'dine_in_order_type_active' AND `type` = 'location_config_setting'");
    	            $dineInTaxRow = $this->fetchRow("SELECT `value` FROM `config` WHERE `setting` = 'dine_in_order_type_tax_exempt' AND `type` = 'location_config_setting'");
    	            if (isset($dineInNameRow['value']) && isset($dineInActiveRow['value']) && isset($dineInTaxRow['value'])) {
    	                $this->execute("INSERT INTO `order_tags` (`name`, `active`, `_deleted`, `tax_exempt`) VALUES('".$dineInNameRow['value']."', ".$dineInActiveRow['value'].", 0, ".$dineInTaxRow['value'].")");
    	                $this->execute("DELETE FROM `config` WHERE `setting` = 'dine_in_order_type_name' AND `type` = 'location_config_setting'");
    	                $this->execute("DELETE FROM `config` WHERE `setting` = 'dine_in_order_type_active' AND `type` = 'location_config_setting'");
    	                $this->execute("DELETE FROM `config` WHERE `setting` = 'dine_in_order_type_tax_exempt' AND `type` = 'location_config_setting'");
    	            }
    	            else {
    	                $this->execute("INSERT INTO `order_tags` ( `name`, `active`, `_deleted`, `tax_exempt`) VALUES('Dine In', 1, 0, 0)");
    	            }
    	            $this->execute("UPDATE `orders` SET `togo_status` = (SELECT `id` FROM `order_tags` WHERE LOWER(`name`)='dine in') WHERE `togo_status` = '0' ");
    	        }
    	    }
    	    
    	    if ($this->hasTable('config') && $this->hasTable('order_tags')) {
    			$quickServeConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_quick_serve'");
    			if (isset($quickServeConfig['value'])) {
    				$chkQuickServe = $this->fetchRow("SELECT count(*) AS `cnt` FROM `order_tags` WHERE `id` = '".$quickServeConfig['value']."' ");
    				if ($chkQuickServe['cnt'] == 0) {
    					$this->execute("UPDATE `config` SET `value2` = '".$quickServeConfig['value']."', `value` = (SELECT `id` FROM `order_tags` WHERE LOWER(`name`)='dine in') WHERE `setting` = 'order_tag_default_quick_serve' ");
    				}
    			}
    
    			$tableConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_table'");
    			if (isset($tableConfig['value'])) {
    				$chkTable = $this->fetchRow("SELECT count(*) AS `cnt` FROM `order_tags` WHERE `id` = '".$tableConfig['value']."' ");
    				if ($chkTable['cnt'] == 0) {
    					$this->execute("UPDATE `config` SET `value2` = '".$tableConfig['value']."', `value` = (SELECT `id` FROM `order_tags` WHERE LOWER(`name`)='dine in') WHERE `setting` = 'order_tag_default_table' ");
    				}
    			}
    
    			$tabConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_tab'");
    			if (isset($tabConfig['value'])) {
    				$chkTab = $this->fetchRow("SELECT count(*) AS `cnt` FROM `order_tags` WHERE `id` = '".$tabConfig['value']."' ");
    				if ($chkTab['cnt'] == 0) {
    					$this->execute("UPDATE `config` SET `value2` = '".$tabConfig['value']."', `value` = (SELECT `id` FROM `order_tags` WHERE LOWER(`name`)='dine in') WHERE `setting` = 'order_tag_default_tab' ");
    				}
    			}
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    	    $row = $this->fetchRow("SELECT `id`, `name`, `active`, `tax_exempt`, `_deleted` FROM `order_tags` WHERE LOWER(`name`) = 'dine in'");
    	    if (isset($row['name'])) {
    	        if ( $this->hasTable('config') == true ) {
    	            $locationInfoArr = $this->fetchAll("SELECT `id` FROM `locations` where `_disabled`=0");
    	            if ( count($locationInfoArr) > 0 ) {
    	                foreach ( $locationInfoArr as $key => $location ) {
    	                    $this->execute("INSERT INTO `config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`, `value15`, `value16`, `value17`) VALUES (".$location['id'].", 'dine_in_order_type_name', '".$row['name']."', '', '', '', '', 'location_config_setting', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0', '0')");
    	                    $this->execute("INSERT INTO `config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`, `value15`, `value16`, `value17`) VALUES (".$location['id'].", 'dine_in_order_type_active', '".$row['active']."', '', '', '', '', 'location_config_setting', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0', '0')");
    	                    $this->execute("INSERT INTO `config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`, `value15`, `value16`, `value17`) VALUES (".$location['id'].", 'dine_in_order_type_tax_exempt', '".$row['tax_exempt']."', '', '', '', '', 'location_config_setting', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0', '0')");
    	                }
    	            }
    	        }
		$this->execute("UPDATE `orders` SET `togo_status` = '0' WHERE `togo_status` = '".$row['id']."' ");
		$this->execute("SET FOREIGN_KEY_CHECKS = 0");
		$this->execute("UPDATE `forced_modifiers` SET `_deleted` = 1 WHERE `ordertype_id` = (SELECT `id` FROM `order_tags` WHERE LOWER(`name`)='dine in')");
		$this->execute("DELETE FROM `order_tags` WHERE LOWER(`name`) = 'dine in'");
		$this->execute("SET FOREIGN_KEY_CHECKS = 1");
    	    }
    	    
    	    if ($this->hasTable('config')) {
    			$quickServeConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_quick_serve'");
    			if (isset($quickServeConfig['value2'])) {
    				$this->execute("UPDATE `config` SET `value` = '".$quickServeConfig['value2']."', `value2` = '' WHERE `setting`='order_tag_default_quick_serve'");
    			}
    			
    			$tableConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_table'");
    			if (isset($tableConfig['value2'])) {
    				$this->execute("UPDATE `config` SET `value` = '".$tableConfig['value2']."', `value2` = '' WHERE `setting`='order_tag_default_table'");
    			}
    			
    			$tabConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_tab'");
    			if (isset($tabConfig['value2'])) {
    				$this->execute("UPDATE `config` SET `value` = '".$tabConfig['value2']."', `value2` = '' WHERE `setting`='order_tag_default_tab'");
    			}
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
