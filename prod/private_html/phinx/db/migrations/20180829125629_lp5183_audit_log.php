<?php


use Phinx\Migration\AbstractMigration;

class Lp5183AuditLog extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('audit_log') == false) {
    			$this->execute("CREATE TABLE `audit_log` (
                                   `id` varchar(100) NOT NULL,
                                   `id_type` varchar(100) NOT NULL,
                                   `table_name` varchar(100) NOT NULL,
                                   `info` text NOT NULL,
                                   `updated_for` varchar(50) NOT NULL,
                                   `updated_by` varchar(50) NOT NULL,
                                   `created_date` datetime NOT NULL,
                                   KEY `auditIndex` (`id`,`id_type`,`table_name`)
                                   ) ENGINE=InnoDB CHARSET=utf8");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            if ($this->hasTable('audit_log') == true) {
                $this->execute("DROP TABLE `audit_log`");
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
