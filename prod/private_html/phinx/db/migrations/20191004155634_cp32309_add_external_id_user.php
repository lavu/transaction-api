<?php


use Phinx\Migration\AbstractMigration;

class Cp32309AddExternalIdUser extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if($this->hasTable('users') && !$this->table('users')->hasColumn('external_id')) {
                $this->table('users')->addColumn('external_id', 'string', ['default' => ''])->save();
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if($this->hasTable('users') && $this->table('users')->hasColumn('external_id')) {
                $this->table('users')->removeColumn('external_id')->save();
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
