<?php


use Phinx\Migration\AbstractMigration;

class LocationLp8208 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if ($this->hasTable('reports') == false ) {
				$this->execute("CREATE TABLE `reports` (
				   `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
				   `token` VARCHAR(100) COMMENT 'Token to download',
				   `process_id` INT NOT NULL COMMENT 'Process id referes to proccess_reports_to_email from main db.',
				   `file` LONGBLOB NOT NULL COMMENT 'Report data comma separated format for csv',
				   `created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Time at which the file was created',
				   `modified_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date time of the execution was completed',
				   KEY `process_id` (`process_id`),
				   KEY `token` (`token`)
				) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;");
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if($this->hasTable('reports') == true) {
				$this->execute('DROP TABLE `reports`');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
