<?php


use Phinx\Migration\AbstractMigration;

class CreateCustomFields extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            $this->execute("
                CREATE TABLE IF NOT EXISTS custom_fields (
                    id INT (11) unsigned NOT NULL AUTO_INCREMENT,

                    location_id INT(11) NOT NULL,
                    is_default tinyint NOT NULL DEFAULT 0,
                    label VARCHAR(255) NOT NULL UNIQUE,
                    sort INT(11) NOT NULL,

                    status char(1) NOT NULL DEFAULT 'A',
                    created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                    PRIMARY KEY (id),
                    INDEX custom_fields_status (status),
                    INDEX custom_fields_label (label),

                    CONSTRAINT fk_custom_fields_location_id
                        FOREIGN KEY (location_id)
                        REFERENCES locations (id)
                        ON UPDATE CASCADE

                ) ENGINE=InnoDB;
            ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            $this->execute("DROP TABLE IF EXISTS custom_fields;");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
