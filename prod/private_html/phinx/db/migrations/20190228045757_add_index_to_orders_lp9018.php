<?php


use Phinx\Migration\AbstractMigration;

class AddIndexToOrdersLp9018 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
    	try {
            if ($this->hasTable("orders")) {
                $row = $this->fetchRow('SHOW INDEX FROM orders WHERE Key_name="togo_time_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `orders` ADD INDEX `togo_time_idx` (`togo_time`)");
                }
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            /* Write rollback code here */
            if ($this->hasTable("orders")) {
                $row = $this->fetchRow('SHOW INDEX FROM orders WHERE Key_name="togo_time_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `orders` DROP INDEX `togo_time_idx`");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
