<?php


use Phinx\Migration\AbstractMigration;

class AddStorageKeysToLocations extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('locations') && !$this->table('locations')->hasColumn('storage_key_receipt') && !$this->table('locations')->hasColumn('storage_key_logo') && !$this->table('locations')->hasColumn('storage_key_kiosk')&& $this->hasTable('file_uploads') && $this->table('file_uploads')->hasColumn('storage_key')) {
                $tableUploads = $this->table('file_uploads');
                $tableUploads->addIndex(['storage_key'], ['unique' => true])->save();
                $tableUploads->changeColumn('storage_key', 'string', ['encoding' => 'latin1', 'collation' => 'latin1_swedish_ci'])->update();

                $tableLocations = $this->table('locations');
                $tableLocations
                    ->addColumn('storage_key_receipt', 'string', ['limit' => 200, 'null'=>true])
                    ->addColumn('storage_key_logo', 'string', ['limit' => 200, 'null'=>true])
                    ->addColumn('storage_key_kiosk', 'string', ['limit' => 200, 'null'=>true])->save();
                $tableLocations
                    ->addForeignKey('storage_key_receipt', 'file_uploads', 'storage_key', ['delete'=> 'RESTRICT', 'update'=> 'CASCADE'])
                    ->addForeignKey('storage_key_logo', 'file_uploads', 'storage_key', ['delete'=> 'RESTRICT', 'update'=> 'CASCADE'])
                    ->addForeignKey('storage_key_kiosk', 'file_uploads', 'storage_key', ['delete'=> 'RESTRICT', 'update'=> 'CASCADE'])
                    ->save();
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }

    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('locations') && $this->table('locations')->hasColumn('storage_key_receipt') && $this->table('locations')->hasColumn('storage_key_logo') && $this->table('locations')->hasColumn('storage_key_kiosk')&& $this->hasTable('file_uploads')) {
                $tableLocations = $this->table('locations');
                $tableLocations
                ->dropForeignKey('storage_key_receipt')
                ->dropForeignKey('storage_key_logo')
                ->dropForeignKey('storage_key_kiosk')
                ->removeColumn('storage_key_receipt')
                ->removeColumn('storage_key_logo')
                ->removeColumn('storage_key_kiosk')
                ->save();
                $tableUploads = $this->table('file_uploads');
                $tableUploads->removeIndex(['storage_key'])->save();
                $tableUploads->changeColumn('storage_key', 'string', array('encoding' => 'UTF8','collation' => 'utf8_general_ci'))->update();
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
