<?php


use Phinx\Migration\AbstractMigration;

class DropIndexLp8266 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    	    $rowData = $this->fetchRow("SHOW INDEX FROM `orders` WHERE Key_name = 'location_id'");
    	    if (isset($rowData['Key_name']) && $rowData['Key_name']=='location_id') {
    	        $this->execute("ALTER TABLE `orders` DROP KEY `location_id`");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$rowData = $this->fetchRow("SHOW INDEX FROM `orders` WHERE Key_name = 'location_id'");
    		if (!isset($rowData['Key_name'])) {
    	        $this->execute("ALTER TABLE `orders` ADD INDEX `location_id` (`location_id`)");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
