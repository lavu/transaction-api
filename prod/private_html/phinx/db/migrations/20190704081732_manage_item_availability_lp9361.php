<?php


use Phinx\Migration\AbstractMigration;

class ManageItemAvailabilityLp9361 extends AbstractMigration
{
 public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            if ($this->hasTable('menu_items') == true && $this->table('menu_items')->hasColumn('unavailable_until') == false) {
                $this->execute("ALTER TABLE `menu_items` ADD COLUMN `unavailable_until` varchar(20) NOT NULL DEFAULT '0'  COMMENT 'available:0, soldout:timestamp'");
            }
            if ($this->hasTable('modifiers') == true && $this->table('modifiers')->hasColumn('unavailable_until') == false) {
                 $this->execute("ALTER TABLE `modifiers` ADD COLUMN `unavailable_until` varchar(20) NOT NULL DEFAULT '0'  COMMENT 'available:0, soldout:timestamp'");
            }
            if ($this->hasTable('forced_modifiers') == true && $this->table('forced_modifiers')->hasColumn('unavailable_until') == false) {
                $this->execute("ALTER TABLE `forced_modifiers` ADD COLUMN `unavailable_until` varchar(20) NOT NULL DEFAULT '0'  COMMENT 'available:0, soldout:timestamp'");
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            if($this->hasTable('menu_items') == true && $this->table('menu_items')->hasColumn('unavailable_until') == true) {
                $this->execute("ALTER TABLE `menu_items` DROP COLUMN `unavailable_until`");
            }
            if($this->hasTable('modifiers') == true && $this->table('modifiers')->hasColumn('unavailable_until') == true) {
                $this->execute("ALTER TABLE `modifiers` DROP COLUMN `unavailable_until`");
            }
            if($this->hasTable('forced_modifiers') == true && $this->table('forced_modifiers')->hasColumn('unavailable_until') == true) {
                $this->execute("ALTER TABLE `forced_modifiers` DROP COLUMN `unavailable_until`");
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
