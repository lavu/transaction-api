<?php


use Phinx\Migration\AbstractMigration;

class AddIsNewControlPanelActiveToLocation extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('locations') == true && $this->table('locations')->hasColumn('is_new_control_panel_active') == false) {
                $this->execute("ALTER TABLE `locations`
                                ADD COLUMN `is_new_control_panel_active`
                                boolean default false");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('locations') == true && $this->table('locations')->hasColumn('is_new_control_panel_active') == true) {
    			$this->execute("ALTER TABLE `locations`
                                DROP COLUMN `is_new_control_panel_active`");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
