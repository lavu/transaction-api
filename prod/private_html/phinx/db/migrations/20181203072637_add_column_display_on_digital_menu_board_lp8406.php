<?php


use Phinx\Migration\AbstractMigration;

class AddColumnDisplayOnDigitalMenuBoardLp8406 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_categories') == true && $this->table('menu_categories')->hasColumn('display_on_digital_menu_board') == false) {
                $this->execute("ALTER TABLE `menu_categories` ADD COLUMN `display_on_digital_menu_board` tinyint(1) NOT NULL");
    		}
    		
    		if ($this->hasTable('menu_items') == true && $this->table('menu_items')->hasColumn('display_on_digital_menu_board') == false) {
    		    $this->execute("ALTER TABLE `menu_items` ADD COLUMN `display_on_digital_menu_board` tinyint(1) NOT NULL");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_categories') == true && $this->table('menu_categories')->hasColumn('display_on_digital_menu_board') == true) {
                $this->execute("ALTER TABLE `menu_categories` DROP COLUMN `display_on_digital_menu_board`");
    		}
    		
    		if ($this->hasTable('menu_items') == true && $this->table('menu_items')->hasColumn('display_on_digital_menu_board') == true) {
    		    $this->execute("ALTER TABLE `menu_items` DROP COLUMN `display_on_digital_menu_board`");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
