<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLtg307 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		$this->execute("UPDATE  `config` SET `value` = '' WHERE  `setting` = 'ltg_on_embed_typform_feedback' ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$this->execute("UPDATE `config` SET `value` = '<a class=\"typeform-share button\" href=\"https://areeyalila.typeform.com/to/GAxpqr\" style = \"text-decoration: none; color: #000;\" data-mode=\"popup\" data-submit-close-delay=\"5\" target=\"_blank\">Leave Feed Back</a> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id=\"typef_orm_share\", b=\"https://embed.typeform.com/\"; if(!gi.call(d,id)){ js=ce.call(d,\"script\"); js.id=id; js.src=b+\"embed.js\"; q=gt.call(d,\"script\")[0]; q.parentNode.insertBefore(js,q) } })() </script>' WHERE  `setting` = 'ltg_on_embed_typform_feedback'");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
