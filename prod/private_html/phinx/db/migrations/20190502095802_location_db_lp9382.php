<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp9382 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('forced_modifier_lists') == true && $this->table('forced_modifier_lists')->hasColumn('type_option') == false) {
                $this->execute("ALTER TABLE `forced_modifier_lists` ADD COLUMN `type_option` INT(11) NOT NULL  COMMENT 'To capture check list/choice option'");
                $this->execute("UPDATE `forced_modifier_lists` set `type_option` = 1 where `type` = 'choice'");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if($this->hasTable('forced_modifier_lists') == true && $this->table('forced_modifier_lists')->hasColumn('type_option') == true) {
                $this->execute("ALTER TABLE `forced_modifier_lists` DROP COLUMN `type_option`");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
