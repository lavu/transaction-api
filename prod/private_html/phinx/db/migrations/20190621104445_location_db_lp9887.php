<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp9887 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		/* Write migration code here */
            if ($this->hasTable('config') == true  && $this->hasTable('locations') == true ) {
                $locationInfoArr = $this->fetchAll("SELECT `id` FROM `locations` where `_disabled` = 0");
                if ( count($locationInfoArr) > 0 ) {
                    foreach ( $locationInfoArr as $key => $location ) {
                        $row = $this->fetchRow("SELECT `id`, `value` FROM `config` WHERE `setting` = 'offline_comms_enabled' AND `location`=".$location['id']." AND `_deleted` = 0");
                        if ($row == false) {
                           $this->execute("INSERT INTO `config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`, `value15`, `value16`, `value17`) VALUES (".$location['id'].", 'offline_comms_enabled', '0', '', '', '', '', 'location_config_setting', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0', '0')");
                        }
                    }
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('config')) {
            $this->execute("DELETE FROM `config` WHERE setting = 'offline_comms_enabled'");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
