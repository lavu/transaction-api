<?php


use Phinx\Migration\AbstractMigration;

class AddMonitorySymbolLp6406 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ( $this->hasTable('locations') == true && $this->table('locations')->hasColumn('secondary_monitary_symbol') == false) {
            $this->execute("ALTER TABLE `locations` ADD COLUMN `secondary_monitary_symbol` varchar(25) NOT NULL AFTER `monitary_symbol`");
        }
        
        if ( $this->hasTable('locations') == true && $this->table('locations')->hasColumn('secondary_currency_receipt_label') == false) {
            $this->execute("ALTER TABLE `locations` ADD COLUMN `secondary_currency_receipt_label` varchar(100) NOT NULL AFTER `secondary_monitary_symbol`");
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ( $this->hasTable('locations') == true && $this->table('locations')->hasColumn('secondary_monitary_symbol') == true ) {
            $this->execute("ALTER TABLE `locations` DROP COLUMN `secondary_monitary_symbol`");
        }
        
        if ( $this->hasTable('locations') == true && $this->table('locations')->hasColumn('secondary_currency_receipt_label') == true ) {
            $this->execute("ALTER TABLE `locations` DROP COLUMN `secondary_currency_receipt_label`");
        }
    }
}
