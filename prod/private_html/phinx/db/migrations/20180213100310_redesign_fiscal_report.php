<?php


use Phinx\Migration\AbstractMigration;

class RedesignFiscalReport extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable('fiscal_reports')) {
            if (!$this->table('fiscal_reports')->hasColumn('CUIT')) {
                $this->execute("ALTER TABLE `fiscal_reports` ADD COLUMN `CUIT` varchar(255) NOT NULL");
            }
        }
        
        if ($this->hasTable('fiscal_reports')) {
            if (!$this->table('fiscal_reports')->hasColumn('client_name')) {
                $this->execute("ALTER TABLE `fiscal_reports` ADD COLUMN `client_name` varchar(255) NOT NULL");
            }
        }
        
        if ($this->hasTable('fiscal_transaction_log')) {
            if (!$this->table('fiscal_transaction_log')->hasColumn('item_void_amount')) {
                $this->execute("ALTER TABLE `fiscal_transaction_log` ADD COLUMN `item_void_amount` float(7,2) NOT NULL");
            }
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable('fiscal_reports')) {
            if ($this->table('fiscal_reports')->hasColumn('CUIT')) {
                $this->execute("ALTER TABLE `fiscal_reports` DROP COLUMN `CUIT`");
            }
        }
        
        if ($this->hasTable('fiscal_reports')) {
            if ($this->table('fiscal_reports')->hasColumn('client_name')) {
                $this->execute("ALTER TABLE `fiscal_reports` DROP COLUMN `client_name`");
            }
        }
        
        if ($this->hasTable('fiscal_transaction_log')) {
            if (!$this->table('fiscal_transaction_log')->hasColumn('item_void_amount')) {
                $this->execute("ALTER TABLE `fiscal_transaction_log` DROP COLUMN `item_void_amount`");
            }
        }
        
    }
}
