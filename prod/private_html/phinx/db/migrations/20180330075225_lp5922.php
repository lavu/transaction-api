<?php


use Phinx\Migration\AbstractMigration;

class Lp5922 extends AbstractMigration
{
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		//HAVE REMOVED FK CHECK AND DROPPED FK.
		// change bank migrations
		if ($this->hasTable('change_bank'))
		{
			if ($this->table('change_bank')->hasColumn('user_id'))
			{
				if ($this->table('change_bank')->hasForeignKey('user_id'))
				{
					$this->execute('ALTER TABLE change_bank DROP FOREIGN KEY `change_bank_ibfk_1`');
				}
				$getIndexNames = $this->fetchRow("SELECT distinct INDEX_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.STATISTICS
WHERE TABLE_NAME='change_bank' AND index_name IN ('idx_user_id','user_id') AND table_schema=schema()");
				if($getIndexNames['INDEX_NAME'] != ""){
					$this->execute("ALTER TABLE change_bank DROP INDEX `". $getIndexNames['INDEX_NAME'] ."`");
				}
				$this->execute('ALTER TABLE change_bank ADD INDEX `idx_user_id` (user_id)');
			}
		}
		// bank deposit migrations
		if ($this->hasTable('bank_deposit'))
		{
			if ($this->table('bank_deposit')->hasColumn('user_id'))
			{
				if ($this->table('bank_deposit')->hasForeignKey('user_id'))
				{
					$this->execute('ALTER TABLE bank_deposit DROP FOREIGN KEY `bank_deposit_ibfk_1`');
				}
				$getIndexNames = $this->fetchRow("SELECT distinct INDEX_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.STATISTICS
WHERE TABLE_NAME='bank_deposit' AND index_name IN ('idx_user_id','user_id') AND table_schema=schema()");
				if($getIndexNames['INDEX_NAME'] != ""){
					$this->execute("ALTER TABLE bank_deposit DROP INDEX `". $getIndexNames['INDEX_NAME'] ."`");
				}
				$this->execute('ALTER TABLE bank_deposit ADD INDEX `idx_user_id` (user_id)');
			}
		}
	}

	//TO KEEP BACKUP FOR INITIAL COPY OF up()
	public function up_bkp()
	{
		// change bank migrations
		if ($this->hasTable('change_bank'))
		{
			if ($this->table('change_bank')->hasColumn('user_id'))
			{
				$fkExists = $this->table('change_bank')->hasForeignKey('user_id');
				if ($fkExists) {
					$this->execute('ALTER TABLE change_bank DROP FOREIGN KEY `change_bank_ibfk_1`');
				}
				if($this->table('change_bank')->hasIndex('user_id'))
				{
					$getIndexNames = $this->fetchRow("SELECT distinct INDEX_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.STATISTICS
WHERE TABLE_NAME='change_bank' AND index_name IN ('idx_user_id','user_id') AND table_schema=schema()");
					if($getIndexNames['INDEX_NAME'] != ""){
						$this->execute("ALTER TABLE change_bank DROP INDEX `". $getIndexNames['INDEX_NAME'] ."`");
					}
				}	
				$this->execute('ALTER TABLE change_bank ADD CONSTRAINT `change_bank_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)');
				$this->execute('ALTER TABLE change_bank ADD INDEX `idx_user_id` (user_id)');
			}
		}
		// bank deposit migrations
		if ($this->hasTable('bank_deposit'))
		{
			if ($this->table('bank_deposit')->hasColumn('user_id'))
			{
				$fkExists = $this->table('bank_deposit')->hasForeignKey('user_id');
				if ($fkExists)
				{
					$this->execute('ALTER TABLE bank_deposit DROP FOREIGN KEY `bank_deposit_ibfk_1`');
				}
				if($this->table('bank_deposit')->hasIndex('user_id'))
				{
					$getIndexNames = $this->fetchRow("SELECT distinct INDEX_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.STATISTICS
WHERE TABLE_NAME='bank_deposit' AND index_name IN ('idx_user_id','user_id') AND table_schema=schema()");
					if($getIndexNames['INDEX_NAME'] != ""){
						$this->execute("ALTER TABLE bank_deposit DROP INDEX `". $getIndexNames['INDEX_NAME'] ."`");
					}
				}
				$this->execute('ALTER TABLE bank_deposit ADD CONSTRAINT `bank_deposit_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)');
				$this->execute('ALTER TABLE bank_deposit ADD INDEX `idx_user_id` (user_id)');
			}
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{

	}
}
