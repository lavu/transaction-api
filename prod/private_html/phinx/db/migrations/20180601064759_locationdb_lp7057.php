<?php


use Phinx\Migration\AbstractMigration;

class LocationdbLp7057 extends AbstractMigration
{
    public $status;
    public $tblName = 'fiscal_receipt_invoice';
 
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
        
    	try {
    		if ( $this->hasTable($this->tblName) && !$this->table($this->tblName)->hasColumn('pre_post_fix')) {
                    $this->execute('ALTER TABLE '.$this->tblName.' ADD COLUMN pre_post_fix VARCHAR(50) NOT NULL DEFAULT \'\'');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ( $this->hasTable($this->tblName) && $this->table($this->tblName)->hasColumn('pre_post_fix')) {
                    $this->execute('ALTER TABLE '.$this->tblName.' DROP COLUMN pre_post_fix');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
