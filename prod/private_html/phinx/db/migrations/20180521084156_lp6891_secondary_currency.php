<?php


use Phinx\Migration\AbstractMigration;

class Lp6891SecondaryCurrency extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    	    if ($this->hasTable('secondary_exchange_rates') == true && $this->table('secondary_exchange_rates')->hasColumn('rate') == true) {
    	        $this->execute("ALTER TABLE `secondary_exchange_rates` MODIFY COLUMN `rate` decimal(22,16) unsigned NOT NULL");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    	    if ($this->hasTable('secondary_exchange_rates') == true && $this->table('secondary_exchange_rates')->hasColumn('rate') == true) {
    	        $this->execute("ALTER TABLE `secondary_exchange_rates` MODIFY COLUMN `rate` float(10,3) unsigned NOT NULL");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
