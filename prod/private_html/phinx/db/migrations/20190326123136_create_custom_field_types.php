<?php


use Phinx\Migration\AbstractMigration;

class CreateCustomFieldTypes extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            $this->execute("
                CREATE TABLE IF NOT EXISTS custom_field_types (
                    id INT (11) unsigned NOT NULL AUTO_INCREMENT,
                    field_source_type_id INT (11) unsigned NOT NULL,
                    label VARCHAR(255) NOT NULL,
                    name VARCHAR(255) NOT NULL UNIQUE,
                    validator TEXT,

                    code VARCHAR(5) NOT NULL,
                    status char(1) NOT NULL DEFAULT 'A',
                    created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                    PRIMARY KEY (id),
                    INDEX custom_field_types_status (status),
                    UNIQUE INDEX custom_field_types_code (code),

                    CONSTRAINT fk_field_types_source_id
                        FOREIGN KEY (field_source_type_id)
                        REFERENCES poslavu_MAIN_db.field_source_types (id)
                        ON UPDATE CASCADE
                ) ENGINE=InnoDB;"
            );
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$this->execute("DROP TABLE IF EXISTS custom_field_types;");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
