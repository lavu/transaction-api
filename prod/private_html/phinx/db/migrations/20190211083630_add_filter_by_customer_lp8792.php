<?php


use Phinx\Migration\AbstractMigration;

class AddFilterByCustomerLp8792 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	if ($this->hasTable('orders')) {
            if(!$this->table('orders')->hasColumn('customer_id')){
                $this->execute("ALTER TABLE `orders` ADD COLUMN `customer_id` int(11) DEFAULT NULL, ADD INDEX `idx_customer_id` (`customer_id`)");
            }
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	if($this->hasTable('orders')){
            if($this->table('orders')->hasColumn('customer_id')){
                $this->execute("ALTER TABLE `orders` DROP COLUMN `customer_id`");
            }
        }
    }
}
