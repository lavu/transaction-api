<?php

use Phinx\Migration\AbstractMigration;

class AddFileUploadsTable extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if (!$this->hasTable('file_uploads')) {
                $fileUploads = $this->table('file_uploads');
                $fileUploads->addColumn('storage_key', 'string', ['limit' => 200])
                    ->addColumn('name', 'string', ['limit' => 200])
                    ->addColumn('_deleted', 'integer', ['default' => 0])
                    ->addColumn('disabled', 'integer', ['default' => 0])
                    ->addColumn('type', 'string', ['limit' => 50])
                    ->addColumn('category', 'string', ['limit' => 50])
                    ->addColumn('access_level', 'string', ['limit' => 50])
                    ->addColumn('uuid', 'string', ['limit' => 50])
                    ->addColumn('path', 'string', ['limit' => 200])
                    ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                    ->addColumn('updated_at', 'timestamp', ['null' => true, 'update' => 'CURRENT_TIMESTAMP'])
                    ->save();
            }
        }
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('file_uploads')) {
                $this->dropTable('file_uploads');
            }
        }
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
