<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp7610 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		$row = $this->fetchRow('SELECT `id` FROM `config` WHERE `setting` = "ltg_on_embed_typform_feedback"');
    		if (!isset($row['id'])) {
    			$this->execute("INSERT INTO `config` (	`location`, 
    								`setting`, 
    								`value`,
    								`value2`, 
    								`value3`,
								`value4`,
								`value_long`, 
								`type`, 
								`value5`, 
								`value6`, 
								`value7`, 
							    	`value8`, 
							    	`_deleted`, 
							   	`value9`, 
							    	`value10`, 
							    	`value11`, 
							    	`value12`, 
							    	`value13`, 
							    	`value14`, 
    								`value_long2`,
   								`value15`) 
    								 VALUES ( '1',
								 	  'ltg_on_embed_typform_feedback',
								   	  '<a class=\"typeform-share button\" href=\"https://areeyalila.typeform.com/to/GAxpqr\" style = \"text-decoration: none; color: #000;\" data-mode=\"popup\" data-submit-close-delay=\"5\" target=\"_blank\">Leave Feedback</a> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id=\"typef_orm_share\", b=\"https://embed.typeform.com/\"; if(!gi.call(d,id)){ js=ce.call(d,\"script\"); js.id=id; js.src=b+\"embed.js\"; q=gt.call(d,\"script\")[0]; q.parentNode.insertBefore(js,q) } })() </script>',
								          '', '', '', '', 'location_config_setting', '', '', '', '', 0, '', '', '', '', '', '', '', ''
    						)"
    					);
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$row = $this->fetchRow('SELECT `id` FROM `config` WHERE `setting` = "ltg_on_embed_typform_feedback"');
    		if (isset($row['id'])) {
    			$this->execute("DELETE FROM `config` WHERE  `setting` = 'ltg_on_embed_typform_feedback' ");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
