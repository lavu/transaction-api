<?php


use Phinx\Migration\AbstractMigration;

class IndexForLocationQueryOptimizationLp11134 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable("clock_punches")) {
                $row = $this->fetchRow('SHOW INDEX FROM clock_punches WHERE Key_name="server_id_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `clock_punches` ADD INDEX `server_id_idx` (`server_id`)");
                }

                $row = $this->fetchRow('SHOW INDEX FROM clock_punches WHERE Key_name="location_id_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `clock_punches` ADD INDEX `location_id_idx` (`location_id`)");
                }

                $row = $this->fetchRow('SHOW INDEX FROM clock_punches WHERE Key_name="punched_out_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `clock_punches` ADD INDEX `punched_out_idx` (`punched_out`)");
                }

                $row = $this->fetchRow('SHOW INDEX FROM clock_punches WHERE Key_name="_deleted_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `clock_punches` ADD INDEX `_deleted_idx` (`_deleted`)");
                }
            }

            if ($this->hasTable("users")) {
                $row = $this->fetchRow('SHOW INDEX FROM users WHERE Key_name="username_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `users` ADD INDEX `username_idx` (`username`)");
                }

                $row = $this->fetchRow('SHOW INDEX FROM users WHERE Key_name="password_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `users` ADD INDEX `password_idx` (`password`)");
                }

                $row = $this->fetchRow('SHOW INDEX FROM users WHERE Key_name="loc_id_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `users` ADD INDEX `loc_id_idx` (`loc_id`)");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable("clock_punches")) {
                $row = $this->fetchRow('SHOW INDEX FROM clock_punches WHERE Key_name="server_id_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `clock_punches` DROP INDEX `server_id_idx`");
                }

                $row = $this->fetchRow('SHOW INDEX FROM clock_punches WHERE Key_name="location_id_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `clock_punches` DROP INDEX `location_id_idx`");
                }

                $row = $this->fetchRow('SHOW INDEX FROM clock_punches WHERE Key_name="punched_out_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `clock_punches` DROP INDEX `punched_out_idx`");
                }

                $row = $this->fetchRow('SHOW INDEX FROM clock_punches WHERE Key_name="_deleted_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `clock_punches` DROP INDEX `_deleted_idx`");
                }
            }

            if ($this->hasTable("users")) {
                $row = $this->fetchRow('SHOW INDEX FROM users WHERE Key_name="username_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `users` DROP INDEX `username_idx`");
                }

                $row = $this->fetchRow('SHOW INDEX FROM users WHERE Key_name="password_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `users` DROP INDEX `password_idx`");
                }

                $row = $this->fetchRow('SHOW INDEX FROM users WHERE Key_name="loc_id_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `users` DROP INDEX `loc_id_idx`");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
