<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp7324 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ( $this->hasTable('menu_categories') == true && $this->table('menu_categories')->hasColumn('allow_ordertype_tax_exempt') == false) {
			$this->execute("ALTER TABLE `menu_categories` ADD COLUMN `allow_ordertype_tax_exempt` TINYINT(1) DEFAULT 1");
    		}
    		if ( $this->hasTable('menu_items') == true && $this->table('menu_items')->hasColumn('allow_ordertype_tax_exempt') == false) {
			$this->execute("ALTER TABLE `menu_items` ADD COLUMN `allow_ordertype_tax_exempt` VARCHAR(10) NOT NULL DEFAULT 'Default'");
    		}
    		if ( $this->hasTable('forced_modifiers') == true && $this->table('forced_modifiers')->hasColumn('ordertype_id') == false) {
    			$this->execute("ALTER TABLE `forced_modifiers` ADD COLUMN `ordertype_id` INT(11)");
    			$this->execute("ALTER TABLE `forced_modifiers` ADD CONSTRAINT FK_OrderTagsId FOREIGN KEY (`ordertype_id`) REFERENCES `order_tags`(`id`)");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_categories') == true && $this->table('menu_categories')->hasColumn('allow_ordertype_tax_exempt') == true) {
    			$this->execute("ALTER TABLE `menu_categories` DROP COLUMN `allow_ordertype_tax_exempt`");
    		}
    		if ($this->hasTable('menu_items') == true && $this->table('menu_items')->hasColumn('allow_ordertype_tax_exempt') == true) {
    			$this->execute("ALTER TABLE `menu_items` DROP COLUMN `allow_ordertype_tax_exempt`");
    		}
    		if ( $this->hasTable('forced_modifiers') == true && $this->table('forced_modifiers')->hasColumn('ordertype_id') == true) {
    			if ($this->table('forced_modifiers')->hasForeignKey('ordertype_id'))
    			{
    				$this->execute('ALTER TABLE forced_modifiers DROP FOREIGN KEY `FK_OrderTagsId`');
    			}
    			$this->execute("ALTER TABLE `forced_modifiers` DROP COLUMN `ordertype_id`");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
