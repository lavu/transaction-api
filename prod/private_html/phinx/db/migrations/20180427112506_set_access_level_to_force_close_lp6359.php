<?php
use Phinx\Migration\AbstractMigration;

class SetAccessLevelToForceCloseLp6359 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up() {
        if ( $this->hasTable('config') == true ) {
            $locationInfoArr = $this->fetchAll("SELECT `id` FROM `locations` where _disabled=0");
            if ( count($locationInfoArr) > 0 ) {
                foreach ( $locationInfoArr as $key => $location ) {
                    $row = $this->fetchRow("SELECT `value` FROM `config` WHERE `setting` ='level_to_close_order' AND `location`=".$location['id']." AND _deleted=0");
                    if ($row == false) {
                        $this->execute("INSERT INTO `config` (`location`, `setting`, `value`, `value2`, `value3`, `value4`, `value_long`, `type`, `value5`, `value6`, `value7`, `value8`, `_deleted`, `value9`, `value10`, `value11`, `value12`, `value13`, `value14`, `value_long2`, `value15`, `value16`, `value17`) VALUES (".$location['id'].", 'level_to_close_order', '2', '', '', '', '', 'location_config_setting', '', '', '', '', 0, '', '', '', '', '', '', '', '', '0', '0')");
                    }
                    else {
                        $rowCheckCurrentValue = $this->fetchRow("SELECT `value` FROM `config` WHERE `location`=".$location['id']." AND `setting` ='level_to_close_order' and ( `value` ='1' OR `value`='') AND _deleted = 0");
                        if ($rowCheckCurrentValue == true) {
                            $this->execute("Update `config` set value='2', _deleted=0 WHERE `location`=".$location['id']." AND `setting` ='level_to_close_order' and ( `value` ='1' OR `value`='')");
                        }
                    }
                }
            }
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down() {
        /* Nothing to Rollback this is a one way Query to set value */
    }
}