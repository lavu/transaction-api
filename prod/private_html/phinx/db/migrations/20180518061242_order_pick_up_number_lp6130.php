<?php


use Phinx\Migration\AbstractMigration;

class OrderPickUpNumberLp6130 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('pickup_seq') == true && $this->table('pickup_seq')->hasColumn('startdate') == false) {
				$this->execute("ALTER TABLE `pickup_seq` ADD COLUMN `startdate` DATETIME DEFAULT CURRENT_TIMESTAMP");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		
    		if ($this->hasTable('pickup_seq') == true && $this->table('pickup_seq')->hasColumn('startdate') == true) {
           		 $this->execute("ALTER TABLE `pickup_seq` DROP COLUMN `startdate`");
       		 }
        
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
