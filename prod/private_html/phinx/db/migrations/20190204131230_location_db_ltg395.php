<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLtg395 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		$lavuToGoName = 'Lavu To Go';
    		if ($this->hasTable('action_log')) {
    			$this->execute("UPDATE `action_log` SET `user` = '".$lavuToGoName."' WHERE `user` = 'OLO'");
    		}
    		if ($this->hasTable('orders')) {
    			$this->execute("UPDATE `orders` SET `server` = '".$lavuToGoName."', `register` = '".$lavuToGoName."', `register_name` = '".$lavuToGoName."', `last_mod_register_name` = '".$lavuToGoName."', `tablename` = '".$lavuToGoName." UNPAID' WHERE `server` = 'OLO'");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
		//We have changed orders and action_log table Lavu To Go data from 'OLO' to 'Lavu To Go'. OLO name is inserted into orders and action log tables by mistakenly. So rollback is not required here.
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
