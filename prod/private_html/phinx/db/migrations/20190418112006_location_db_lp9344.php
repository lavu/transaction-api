<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp9344 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('cc_transactions') == true && $this->table('cc_transactions')->hasColumn('card_holder_name') == false) {
                $this->execute("ALTER TABLE `cc_transactions` ADD COLUMN `card_holder_name` VARCHAR(255) NOT NULL DEFAULT '' COMMENT 'to capture card holder name'");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('cc_transactions') == true && $this->table('cc_transactions')->hasColumn('card_holder_name') == true) {
               $this->execute("ALTER TABLE `cc_transactions` DROP COLUMN `card_holder_name`");
           }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
