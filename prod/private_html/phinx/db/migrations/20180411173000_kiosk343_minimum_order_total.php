<?php


use Phinx\Migration\AbstractMigration;

class Kiosk343MinimumOrderTotal extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
      if ($this->hasTable('kiosk_settings')) {
        $row = $this->fetchRow("SELECT * FROM `kiosk_settings` WHERE `setting` = 'kiosk_minimum_order_total'");
        if ($row == false) {
            $this->execute("INSERT INTO `kiosk_settings` (`location`, `setting`, `value`, `_deleted`, `_created_time`, `_created_user`, `_updated_time`, `_updated_user`)
                VALUES
                (1, 'kiosk_minimum_order_total', '0', 0, NOW(), 0, '0000-00-00 00:00:00', 0)");
        }
      }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
      if ($this->hasTable('kiosk_settings')) {
          $row = $this->fetchRow("SELECT * FROM `kiosk_settings` WHERE `setting` = 'kiosk_minimum_order_total'");
          if ($row == true) {
              $this->execute("DELETE FROM `kiosk_settings` WHERE `setting` = 'kiosk_minimum_order_total'");
          }
      }
    }
}