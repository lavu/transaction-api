<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp8009 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if ($this->hasTable('audit_log') == false) {
				$this->execute("CREATE TABLE `audit_log` (
    								`id` varchar(100) NOT NULL,
    								`id_type` varchar(100) NOT NULL,
    								`table_name` varchar(100) NOT NULL,
    								`info` text NOT NULL,
    								`updated_for` varchar(50) NOT NULL,
    								`updated_by` varchar(50) NOT NULL,
    								`created_date` datetime NOT NULL,
    								KEY `auditIndex` (`id`,`id_type`,`table_name`)
    								) ENGINE=InnoDB CHARSET=utf8");
			}
			if ($this->hasTable('menu_items') == true  && $this->hasTable('combo_items') == true ) {
				$queryData=$this->fetchAll("select DISTINCT(MI.id) as menu_id from menu_items as MI left join combo_items as CI on MI.id=CI.combo_menu_id where CI.`_deleted`=0 and MI.combo=1 and MI.`_deleted`=1");
				foreach($queryData AS $queryItem) {
					$currDate = date('Y-m-d H:i:s');
					$comboContentID = $queryItem['menu_id'];
					$actionData = ['id'=>$comboContentID, 'id_type'=>'id', 'table_name'=>'combo_items', 'info'=>$comboContentID, 'updated_for'=>'lp8009', 'updated_by'=>'migration', 'created_date'=>$currDate];
					$auditTable = $this->table( 'audit_log' );
					$auditTable->insert($actionData)->saveData();
					$this->execute("update `combo_items`  SET `_deleted`= 1  where `combo_menu_id` ='".$comboContentID."'");
				}
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if ( $this->hasTable( 'audit_log' ) ) {
				$auditLog_queryData = $this->fetchAll("select info from `audit_log` where `updated_for`='lp8009' and `updated_by`='migration'");
				foreach ($auditLog_queryData as $auditData) {
					$id = $auditData['info'];
					if ($this->hasTable( 'order_contents' )) {
						$this->execute("update `combo_items`  SET `_deleted`= 0 where `combo_menu_id`='".$id."'");
					}
				}
				$this->execute("DROP TABLE `audit_log`");
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
