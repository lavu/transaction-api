<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp7817 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
		if ($this->hasTable('config') && $this->hasTable('order_tags')) {
			$row = $this->fetchRow("SELECT max(`id`) as maxId FROM `order_tags`");
			if (isset($row['maxId'])) {
				$quickServeConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_quick_serve'");
				$defaultQuickServe = trim($quickServeConfig['value']);
				if (isset($quickServeConfig['value2']) && $quickServeConfig['value2'] == "0" && $defaultQuickServe=="") {
				    $this->execute("UPDATE `config` SET `value3` = `value` WHERE `setting`='order_tag_default_quick_serve'");
					$this->execute("UPDATE `config` SET `value` = '".$row['maxId']."' WHERE `setting`='order_tag_default_quick_serve'");
					$this->execute("UPDATE `orders` SET `togo_status` = '".$row['maxId']."' WHERE `togo_status` = '0' OR `togo_status` IS NULL OR `togo_status` = ''");
				}

				$tableConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_table'");
				$defaultTable = trim($tableConfig['value']);
				if (isset($tableConfig['value2']) && $tableConfig['value2'] == "0" && $defaultTable == "") {
				    $this->execute("UPDATE `config` SET `value3` = `value` WHERE `setting`='order_tag_default_table'");
				    $this->execute("UPDATE `config` SET `value` = '".$row['maxId']."' WHERE `setting`='order_tag_default_table'");
				}

				$tabConfig = $this->fetchRow("SELECT `value`, `value2` FROM `config` WHERE `setting`='order_tag_default_tab'");
				$defaultTab = trim($tabConfig['value']);
				if (isset($tabConfig['value2']) && $tabConfig['value2'] == "0" && $defaultTab=="") {
				    $this->execute("UPDATE `config` SET `value3` = `value` WHERE `setting`='order_tag_default_tab'");
					$this->execute("UPDATE `config` SET `value` = '".$row['maxId']." 'WHERE `setting`='order_tag_default_tab'");
				}
			}
		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    	    if ($this->hasTable('config') && $this->hasTable('order_tags')) {
    	        $row = $this->fetchRow("SELECT max(`id`) as maxId FROM `order_tags`");
    	        if (isset($row['maxId'])) {
    	            $quickServeConfig = $this->fetchRow("SELECT `value`, `value2`, `value3` FROM `config` WHERE `setting`='order_tag_default_quick_serve'");
    	            
    	            $defaultQuickServe = trim($quickServeConfig['value3']);
    	            if (isset($quickServeConfig['value2']) && $quickServeConfig['value2'] == "0" && $defaultQuickServe=="") {
    	                $this->execute("UPDATE `config` SET `value` = `value3` WHERE `setting`='order_tag_default_quick_serve'");
    	                $this->execute("UPDATE `orders` SET `togo_status` = '0' WHERE `togo_status` = '".$row['maxId']."'");
    	            }
    	            
    	            $tableConfig = $this->fetchRow("SELECT `value`, `value2`, `value3` FROM `config` WHERE `setting`='order_tag_default_table'");
    	            $defaultTable = trim($tableConfig['value3']);
    	            if (isset($tableConfig['value2']) && $tableConfig['value2'] == "0" && $defaultTable=="") {
    	                $this->execute("UPDATE `config` SET `value` = `value3` WHERE `setting`='order_tag_default_table'");
    	            }
    	            
    	            $tabConfig = $this->fetchRow("SELECT `value`, `value2`, `value3` FROM `config` WHERE `setting`='order_tag_default_tab'");
    	            $defaultTab = trim($tabConfig['value3']);
    	            if (isset($tabConfig['value2']) && $tabConfig['value2'] == "0" && $defaultTab =="") {
    	                $this->execute("UPDATE `config` SET `value` = `value3` WHERE `setting`='order_tag_default_tab'");
    	            }
    	        }
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
