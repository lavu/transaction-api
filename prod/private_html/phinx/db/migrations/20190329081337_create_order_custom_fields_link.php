<?php


use Phinx\Migration\AbstractMigration;

class CreateOrderCustomFieldsLink extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            $this->execute("
                CREATE TABLE IF NOT EXISTS order_custom_fields_link (
                    id INT(11) unsigned NOT NULL AUTO_INCREMENT,
                    order_id VARCHAR(255) NOT NULL,
                    custom_fields_metadata_id INT(11) unsigned NOT NULL,
                    value text,

                    status char(1) NOT NULL DEFAULT 'A',
                    created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                    PRIMARY KEY (id),
                    INDEX ord_cus_field_link_status(status),

                    CONSTRAINT fk_ord_custom_field_meta_id
                        FOREIGN KEY (custom_fields_metadata_id)
                        REFERENCES custom_fields_metadata (id)
                        ON UPDATE CASCADE,

                    CONSTRAINT fk_ord_custom_field_order_id
                        FOREIGN KEY (order_id)
                        REFERENCES orders (order_id)
                        ON UPDATE CASCADE


                ) ENGINE=InnoDB;
            ");
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            $this->execute("DROP TABLE IF EXISTS order_custom_fields_link;");
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
