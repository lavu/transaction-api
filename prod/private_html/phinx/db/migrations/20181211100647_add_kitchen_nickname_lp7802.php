<?php


use Phinx\Migration\AbstractMigration;

class AddKitchenNicknameLp7802 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_items') == true && $this->table('menu_items')->hasColumn('kitchen_nickname') == false) {
    		    $this->execute("ALTER TABLE `menu_items` ADD COLUMN `kitchen_nickname` VARCHAR(100) NULL, ADD INDEX (kitchen_nickname)  COMMENT 'nickname for menuItem'");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('menu_items') == true && $this->table('menu_items')->hasColumn('kitchen_nickname') == true) {
               $this->execute("ALTER TABLE `menu_items` DROP COLUMN `kitchen_nickname`, DROP INDEX kitchen_nickname");
           }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
