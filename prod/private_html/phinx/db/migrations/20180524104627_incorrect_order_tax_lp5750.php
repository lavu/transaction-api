<?php


use Phinx\Migration\AbstractMigration;

class IncorrectOrderTaxLp5750 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {

			if( $_SERVER['PHINX_DBNAME'] == 'poslavu_caf_livingston_db' ) {

				if ( !$this->hasTable('audit_log') ) {
					throw new PDOException("Table 'audit_log' doesn't exist under db: ".$_SERVER['PHINX_DBNAME'], 1);
				}

				if ($this->hasTable( 'order_contents' )) {
					$startDate = "2017-06-01 00:00:00";
					$endDate = date('Y-m-d H:i:s');
						
					$oc_queryData = $this->fetchAll("select `oc`.*  from `order_contents` as `oc` LEFT JOIN `split_check_details` as `scd` ON `oc`.`order_id`=`scd`.`order_id` where `scd`.`check` >1 and ROUND(`oc`.`itax`,6) != ROUND(`oc`.`tax1`,6) and  `oc`.`device_time` BETWEEN '".$startDate."' and '".$endDate."'");
					foreach($oc_queryData as $ocInfo)
					{
						//backup of order_contents record
						if ($this->hasTable( 'audit_log' )) {

							$currDate = date('Y-m-d H:i:s');
							$orderContentID = $ocInfo['id'];
							$splitData = $ocInfo['split_details'];
							$splitInfo = json_decode($splitData, true);
							$itax_amount = 0;
								
							if (is_array($splitInfo)) {
								foreach($splitInfo as $splitResult){
									$itax = $splitResult['itax_amount'];
									$itax_amount = $itax + $itax_amount;
								}
							}
								
							$ocInfoData = json_encode($ocInfo);
							$ocData = ['id'=>$orderContentID, 'id_type'=>'id', 'table_name'=>'order_contents', 'info'=>$ocInfoData, 'updated_for'=>'lp5750', 'updated_by'=>'migration', 'created_date'=>$currDate];
							$auditTable = $this->table( 'audit_log' );
							$auditTable->insert($ocData)->saveData();
								
							//To update itax.
							$this->execute("update `order_contents` as `oc` SET `oc`.`itax` = IF($itax_amount=0 or (ROUND(`oc`.`itax_rate`,3) = ROUND(`oc`.`tax_rate1`,3)),`oc`.`tax1`,ROUND($itax_amount,6)), `oc`.`tax1` = IF(ROUND(`oc`.`tax1`,3) < 0,0,`oc`.`tax1`) where `oc`.`id`='".$orderContentID."'");
						}
					}
				}
			} else {
				throw new PDOException("Will not execute for: ".$_SERVER['PHINX_DBNAME'], 1);
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {

			if ( $_SERVER['PHINX_DBNAME'] == 'poslavu_caf_livingston_db' ) {

				if ( $this->hasTable( 'audit_log' ) ) {

					$auditLog_queryData = $this->fetchAll("select info from `audit_log` where `updated_for`='lp5750' and `updated_by`='migration'");

					foreach ($auditLog_queryData as $auditData) {
						$auditLog = json_decode($auditData['info']);
						$id = $auditLog->id;
						$itax = $auditLog->itax;

						if ($this->hasTable( 'order_contents' )) {
							//update old itax.
							$this->execute("update `order_contents` as `oc` SET `oc`.`itax`='".$itax."' where `oc`.`id`='".$id."'");
						}
					}
					//delete audit_log table records.
					$this->execute( "DELETE FROM `audit_log` WHERE `updated_for`='lp5750' and `updated_by`='migration'");
				} else {
					throw new PDOException("Table 'audit_log' doesn't exist under db: poslavu_caf_livingston_db ", 1);
				}
			} else {
				throw new PDOException("Will not execute for: ".$_SERVER['PHINX_DBNAME'], 1);
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
