<?php

use Phinx\Migration\AbstractMigration;

class CheckStatusLp5829 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $tableExists = $this->hasTable('lip_order_transaction');
        if ($tableExists == FALSE) {
            $table = $this->table('lip_order_transaction');
            $table
            ->addColumn('order_id', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('check', 'integer', [ 'limit' => 1, 'null' => false])
            ->addColumn('transaction_id', 'string', [ 'limit' => 50, 'null' => false])
            ->addColumn('created_date', 'datetime', [ 'null' => false ])
            ->create();
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $tableExists = $this->hasTable('lip_order_transaction');
        if ($tableExists == true) {
            $this->dropTable('lip_order_transaction');
        }
    }
}