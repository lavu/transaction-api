<?php


use Phinx\Migration\AbstractMigration;

class SeedNameToCustomFieldsMetadata extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            /* Write migration code here */
            $this->execute("SELECT id INTO @name_field_type from custom_field_types WHERE code = 'NAME';");
            $this->execute("SELECT id INTO @label_cfield from custom_fields WHERE label = 'Your information';");
            $this->execute("
                INSERT INTO custom_fields_metadata (custom_field_type_id, custom_field_id, label, placeholder, is_required, sort)
                VALUES
                    (@name_field_type, @label_cfield, 'Your information', 'Enter your name', 1, 1);
            ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            /* Write rollback code here */
            $this->execute("
                DELETE FROM custom_fields_metadata WHERE label = 'Your information';
            ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
