<?php


use Phinx\Migration\AbstractMigration;

class CreateViewNutritionInfoLinkDetailsLp8453 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    	    if ( $this->hasTable('nutrition_info_link') == true && $this->hasTable('nutrition_info') == true ) {
    		    $view = "create view `nutrition_info_link_details` as 
                    select `l_nil`.`source_table` as `nutrition_info_link_source_table`, 
                    `l_nil`.`source_table_id` as `nutrition_info_link_source_table_id`, 
                    `l_ni`.`nutrition_info_link_id` as `nutrition_info_link_id`, 
                    `l_ni`.`value` as `nutrition_info_value`, 
                    `l_ni`.`units` as `nutrition_info_units`, 
                    `l_ni`.`sort_order` as `nutrition_info_sort_order`, 
                    `l_ni`.`_deleted` as `nutrition_info_deleted`, 
                    `m_snt`.`id` as `nutrition_type_id` , 
                    `m_snt`.`code` as `nutrition_type_code`,
                    `m_snt`.`label` as `nutrition_type_label`
                    from `".$this->getOptions()['name']."`.`nutrition_info_link` as `l_nil`
                    inner join `".$this->getOptions()['name']."`.`nutrition_info` as `l_ni` on `l_nil`.`id` = `l_ni`.`nutrition_info_link_id`
                    inner join `poslavu_MAIN_db`.`standard_nutrition_types` as `m_snt` on `l_ni`.`standard_nutrition_type_id` = `m_snt`.`id` 
                    where `l_ni`.`_deleted` <> '1'";
    		    
    		    $this->execute($view);
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    	    $this->execute("drop view if exists nutrition_info_link_details");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
