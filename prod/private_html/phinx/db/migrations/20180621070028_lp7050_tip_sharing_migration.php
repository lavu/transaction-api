<?php


use Phinx\Migration\AbstractMigration;

class Lp7050TipSharingMigration extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		 if ( $this->hasTable('emp_classes') == true
    		     && $this->table('emp_classes')->hasColumn('enable_tip_sharing') == false
    		     && $this->table('emp_classes')->hasColumn('tip_sharing_rule') == false
    		     && $this->table('emp_classes')->hasColumn('include_server_owes') == false
    		     && $this->table('emp_classes')->hasColumn('tip_sharing_period') == false
    		     )
    		 {
    		     $this->execute("ALTER TABLE `emp_classes` ADD COLUMN `enable_tip_sharing` tinyint(1) NOT NULL,
                                                 ADD COLUMN `tip_sharing_rule` varchar(30),
                                                 ADD COLUMN `include_server_owes` tinyint(1),
                                                 ADD COLUMN `tip_sharing_period` varchar(30)");
    		 }
    		 //LP-7276
    		 if ($this->hasTable('tip_sharing') == false) {
    		     $this->execute('CREATE TABLE `tip_sharing` ( `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                                                      `location_id` int(11) NOT NULL,
                                                                      `server_id` int(11) NOT NULL,
                                                                      `server_name` varchar(255),
                                                                      `emp_class_id` int(11) unsigned NOT NULL,
                                                                      `emp_class_name` varchar(255),
                                                                      `tip_sharing_rule` varchar(20),
                                                                      `time_for_tip_sharing` varchar(20),
                                                                      `hours_worked` float(7,3),
                                                                      `sales` decimal(7,2),
                                                                      `card_tip` float(7,3),
                                                                      `cash_tip` float(7,3),
                                                                      `total_tip` float(7,3),
                                                                      `tip_out` float(7,3),
                                                                      `tip_in` float(7,3),
                                                                      `tip_due` float(7,3),
                                                                      `tip_pool` decimal(7,3),
                                                                      `created_date` date NOT NULL,
                                                                      `updated_date` date NOT NULL,
                                                                      `week_end_date` date,
                                                                      PRIMARY KEY (`id`),
                                                                      FOREIGN KEY (`location_id`) REFERENCES `locations`(`id`),
                                                                      FOREIGN KEY (`server_id`) REFERENCES `users`(`id`),
                                                                      FOREIGN KEY (`emp_class_id`) REFERENCES `emp_classes`(`id`) ) ENGINE=InnoDB CHARSET=utf8;');
    		 }
    		 if ($this->hasTable('tip_sharing_log') == false) {
    		     $this->execute('CREATE TABLE `tip_sharing_log` ( `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                                                      `location_id` int(11) NOT NULL,
                                                                      `server_id` int(11) NOT NULL,
                                                                      `server_name` varchar(255),
                                                                      `emp_class_id` int(11) unsigned NOT NULL,
                                                                      `emp_class_name` varchar(255),
                                                                      `tip_sharing_rule` varchar(20),
                                                                      `time_for_tip_sharing` varchar(20),
                                                                      `hours_worked` float(7,3),
                                                                      `sales` decimal(7,2),
                                                                      `card_tip` float(7,3),
                                                                      `cash_tip` float(7,3),
                                                                      `total_tip` float(7,3),
                                                                      `tip_out` float(7,3),
                                                                      `tip_in` float(7,3),
                                                                      `tip_due` float(7,3),
                                                                      `tip_pool` decimal(7,3),
                                                                      `created_date` date NOT NULL,
                                                                      `week_end_date` date,
                                                                      PRIMARY KEY (`id`) ) ENGINE=InnoDB CHARSET=utf8');
    		 }
    		 //lP-7280
    		 if ($this->hasTable('emp_classes_temp') == false) {
    		     $this->execute('CREATE TABLE `emp_classes_temp` ( `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                                              `title` varchar(255),
                                                              `type` varchar(255),
                                                              `_deleted` int(11),
                                                              `tipout_rules` text,
                                                              `tiprefund_rules` varchar(40),
                                                              `refund_type` varchar(40),
                                                              `enable_tip_sharing` varchar(100),
                                                              `tip_sharing_rule` varchar(100),
                                                              `include_server_owes` varchar(100),
                                                              `tip_sharing_period` varchar(100),
                                                              `emp_class_id` int(11) unsigned,
                                                              `class_updated_date` date NOT NULL,
                                                              PRIMARY KEY (`id`),
                                                              FOREIGN KEY (`emp_class_id`) REFERENCES `emp_classes`(`id`)
                                                            ) ENGINE=InnoDB CHARSET=utf8');
    		 }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		 if ( $this->hasTable('emp_classes') == true
    		     && $this->table('emp_classes')->hasColumn('enable_tip_sharing') == true
    		     && $this->table('emp_classes')->hasColumn('tip_sharing_rule') == true
    		     && $this->table('emp_classes')->hasColumn('include_server_owes') == true
    		     && $this->table('emp_classes')->hasColumn('tip_sharing_period') == true
    		     )
    		 {
    		     $this->execute("ALTER TABLE `emp_classes` DROP `enable_tip_sharing`,
                                                      DROP `tip_sharing_rule`,
                                                      DROP `include_server_owes`,
                                                      DROP `tip_sharing_period` ");
    		 }
    		 //LP-7276
    		 if ($this->hasTable('tip_sharing') == true) {
    		     $this->execute('DROP TABLE `tip_sharing` ');
    		 }
    		 if ($this->hasTable('tip_sharing_log') == true) {
    		     $this->execute('DROP TABLE `tip_sharing_log`');
    		 }
    		 //lP-7280
    		 if($this->hasTable('emp_classes_temp') == true){
    		     $this->execute('DROP TABLE `emp_classes_temp`');
    		 }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
