<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp9721 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('config') == true  && $this->hasTable('locations') == true && $this->hasTable('audit_log') == true ) {
                $locationInfoArr = $this->fetchAll("SELECT `id` FROM `locations` where `_disabled` = 0");
                if ( count($locationInfoArr) > 0 ) {
                    foreach ( $locationInfoArr as $key => $location ) {
                        $row = $this->fetchRow("SELECT `id`, `value` FROM `config` WHERE `setting` = 'use_lavu_togo' AND `location`=".$location['id']." AND `_deleted` = 0");
                        if (!empty($row)){
                            $currDate = date('Y-m-d H:i:s');
                            $actionData = ['id'=>$row['id'], 'id_type'=>'id', 'table_name'=>'config', 'info'=>$row['value'], 'updated_for'=>'lp9721', 'updated_by'=>'migration', 'created_date'=>$currDate];
                            $auditTable = $this->table( 'audit_log' );
                            $auditTable->insert($actionData)->saveData();
                            $this->execute("Update `config` set value='0', `_deleted`=1 WHERE `location`=".$location['id']." AND `setting` ='use_lavu_togo' AND `_deleted` = 0 ");
                        }
                    }
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ( $this->hasTable( 'audit_log' ) ) {
                $auditLog_queryData = $this->fetchAll("select id,info from `audit_log` where `updated_for`='lp9721' and `updated_by`='migration'");
                if ( count($auditLog_queryData) > 0 ){
                    foreach ($auditLog_queryData as $auditData) {
                        $id = $auditData['id'];
                        $data = $auditData['info'];
                        if ($this->hasTable( 'config' )) {
                            $this->execute("update `config` SET `value` = '".$data."', `_deleted`= 0 where `id`='".$id."'");
                        }
                    }
                }
                $this->execute("DELETE FROM `audit_log` WHERE updated_for = 'lp9721' and `updated_by`='migration'");
            }
    		
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
