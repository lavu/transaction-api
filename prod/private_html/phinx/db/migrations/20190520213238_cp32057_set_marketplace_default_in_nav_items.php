<?php


use Phinx\Migration\AbstractMigration;

class Cp32057SetMarketplaceDefaultInNavItems extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		$this->execute("UPDATE `users` SET `access_white_list` = CONCAT(`access_white_list`, IF(`access_white_list` = '', 'marketplace', ',marketplace')) WHERE `access_level` = '3' AND `restricted_user` = 1 AND `access_white_list` NOT LIKE '%marketplace%'");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$this->execute("UPDATE `users` SET `access_white_list` = REPLACE(`access_white_list`, IF(`access_white_list` = 'marketplace', 'marketplace', ',marketplace'), '') WHERE `access_level` = '3' AND `restricted_user` = 1 AND `access_white_list` LIKE '%marketplace%'");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
