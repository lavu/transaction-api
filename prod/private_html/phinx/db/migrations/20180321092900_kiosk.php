<?php
use Phinx\Migration\AbstractMigration;

class Kiosk extends AbstractMigration
{
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if (!$this->hasTable('nutrition_info_link')) {
				$this->execute("
				  CREATE TABLE `nutrition_info_link` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`source_table` varchar(20) NOT NULL,
					`source_table_id` int(11) NOT NULL,
					`_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
					`_created_user` int(11) NOT NULL,
				  PRIMARY KEY (`id`)
				  ) ENGINE=InnoDB DEFAULT CHARSET=latin1
				");
			}

			if (!$this->hasTable('nutrition_info')) {
				$this->execute("
				CREATE TABLE `nutrition_info` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `nutrition_info_link_id` int(11) NOT NULL,
				  `standard_nutrition_type_id` int(11) NOT NULL,
				  `value` float NOT NULL,
				  `units` varchar(50) NOT NULL,
				  `is_custom` tinyint(4) NOT NULL,
				  `sort_order` tinyint(4) NOT NULL,
				  `_deleted` tinyint(4) NOT NULL,
				  `_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  `_created_user` int(11) NOT NULL,
				  `_updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
				  `_updated_user` int(11) NOT NULL,
				  PRIMARY KEY (`id`),
				  UNIQUE KEY `unq_nutrition_link_id` (`nutrition_info_link_id`,`standard_nutrition_type_id`),
				  KEY `nutrition_info_link_id` (`nutrition_info_link_id`),
				  CONSTRAINT `nutrition_info_ibfk_1` FOREIGN KEY (`nutrition_info_link_id`) REFERENCES `nutrition_info_link` (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1
				");
			}

			if (!$this->hasTable('menu_item_meta_data_link')) {
				$this->execute("
				CREATE TABLE `menu_item_meta_data_link` (
				  `menu_item_id` int(11) NOT NULL,
				  `meta_data_id` int(11) NOT NULL,
				  `value` tinyint(4) NOT NULL,
				  `_deleted` tinyint(4) NOT NULL,
				  PRIMARY KEY (`menu_item_id`,`meta_data_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1
				");
			}

			if (!$this->hasTable('kiosk_settings')) {
				$this->execute("
				CREATE TABLE `kiosk_settings` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `location` int(11) NOT NULL,
				  `setting` varchar(255) NOT NULL,
				  `value` text NOT NULL,
				  `_deleted` tinyint(4) NOT NULL,
				  `_created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				  `_created_user` int(11) NOT NULL,
				  `_updated_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
				  `_updated_user` int(11) NOT NULL,
				  PRIMARY KEY (`id`),
				  KEY `setting` (`setting`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1
				");
			}

			if ($this->hasTable('menu_items')) {
				if (!$this->table('menu_items')->hasColumn('kiosk_display') ) {
					$this->execute("ALTER TABLE `menu_items` ADD COLUMN `kiosk_display` tinyint(4) NOT NULL DEFAULT '1' AFTER `ltg_display`");
				}
			}

			if ($this->hasTable('menu_items')) {
				if (!$this->table('menu_items')->hasColumn('dietary_info') ) {
					$this->execute("ALTER TABLE `menu_items` ADD COLUMN `dietary_info` tinyint(4) NOT NULL DEFAULT '1' AFTER `nutrition`");
				}
			}

			if ($this->hasTable('menu_categories')) {
				if (!$this->table('menu_categories')->hasColumn('kiosk_display') ) {
					$this->execute("ALTER TABLE `menu_categories` ADD COLUMN `kiosk_display` tinyint(4) NOT NULL DEFAULT '1' AFTER `ltg_display`");
				}
			}

			if ($this->hasTable('kiosk_settings')) {
				$row = $this->fetchRow("SELECT * FROM `kiosk_settings` WHERE `setting` = 'kiosk_theme'");
				if ($row == false) {
					$this->execute("INSERT INTO `kiosk_settings` (`location`, `setting`, `value`, `_deleted`, `_created_time`, `_created_user`, `_updated_time`, `_updated_user`)
						VALUES
						(1, 'kiosk_theme', 'AECD37', 0, '2018-02-07 19:13:22', 0, '0000-00-00 00:00:00', 0)");
				}
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if ($this->hasTable('nutrition_info')) {
				$this->execute('DROP TABLE `nutrition_info`');
			}

			if ($this->hasTable('nutrition_info_link')) {
				$this->execute('DROP TABLE `nutrition_info_link`');
			}

			if ($this->hasTable('menu_item_meta_data_link')) {
				$this->execute('DROP TABLE `menu_item_meta_data_link`');
			}

			if ($this->hasTable('kiosk_settings')) {
				$this->execute('DROP TABLE `kiosk_settings`');
			}

			if ($this->hasTable('menu_items')) {
				if ($this->table('menu_items')->hasColumn('kiosk_display')) {
					$this->execute('ALTER TABLE `menu_items` DROP COLUMN `kiosk_display`');
				}
			}

			if ($this->hasTable('menu_items')) {
				if ($this->table('menu_items')->hasColumn('dietary_info')) {
					$this->execute('ALTER TABLE `menu_items` DROP COLUMN `dietary_info`');
				}
			}

			if ($this->hasTable('menu_categories')) {
				if ($this->table('menu_categories')->hasColumn('kiosk_display')) {
					$this->execute('ALTER TABLE `menu_categories` DROP COLUMN `kiosk_display`');
				}
			}

			if ($this->hasTable('kiosk_settings')) {
				$row = $this->fetchRow("SELECT * FROM `kiosk_settings` WHERE `setting` = 'kiosk_theme'");
				if ($row == true) {
					$this->execute("DELETE FROM `kiosk_settings` WHERE `setting` = 'kiosk_theme'");
				}
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}