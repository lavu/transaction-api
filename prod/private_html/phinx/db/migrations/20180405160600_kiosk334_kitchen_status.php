<?php


use Phinx\Migration\AbstractMigration;

class Kiosk334KitchenStatus extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
      if ($this->hasTable('orders')) {
          if (!$this->table('orders')->hasColumn('kitchen_status')) {
              $this->execute("ALTER TABLE `orders` ADD COLUMN `kitchen_status` varchar(20) NOT NULL");
          }
      }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable('orders')) {
            if ($this->table('orders')->hasColumn('kitchen_status')) {
                $this->execute('ALTER TABLE `orders` DROP COLUMN `kitchen_status`');
            }
        }
    }
}