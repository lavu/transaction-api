<?php


use Phinx\Migration\AbstractMigration;

class Lp9723AddIndexAlternatePaymentTotals extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            /* Write migration code here */
            if ($this->hasTable('alternate_payment_totals')) {
                $row = $this->fetchRow('SHOW INDEX FROM alternate_payment_totals WHERE Key_name="idx_order_id"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE alternate_payment_totals ADD INDEX `idx_order_id` (order_id)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            /* Write rollback code here */
            if ($this->hasTable('alternate_payment_totals')) {
                $row = $this->fetchRow('SHOW INDEX FROM alternate_payment_totals WHERE Key_name="idx_order_id"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE alternate_payment_totals DROP INDEX `idx_order_id`');
                }
            }            
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
