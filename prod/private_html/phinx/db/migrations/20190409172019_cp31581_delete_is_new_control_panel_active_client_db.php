<?php


use Phinx\Migration\AbstractMigration;

class Cp31581DeleteIsNewControlPanelActiveClientDb extends AbstractMigration
{
	public $status;

	public $locTable = 'locations';
	public $activeFlag = 'is_new_control_panel_active';
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if ($this->hasTable($this->locTable) && $this->table($this->locTable)->hasColumn($this->activeFlag)) {
				$this->execute('ALTER TABLE `'.$this->locTable.'`
												DROP COLUMN `'.$this->activeFlag.'`');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if ($this->hasTable($this->locTable) && !$this->table($this->locTable)->hasColumn($this->activeFlag)) {
				$this->execute('ALTER TABLE `'.$this->locTable.'`
												ADD COLUMN `'.$this->activeFlag.'`
												boolean default false');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
