<?php


use Phinx\Migration\AbstractMigration;

class AddStorageKeysToCcTransactions extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('cc_transactions') && !$this->table('cc_transactions')->hasColumn('storage_key_signature')) {
                $table = $this->table('cc_transactions');
                $table
                ->addColumn('storage_key_signature', 'string', ['limit' => 200, 'null'=>true])->save();
                $table
                ->addForeignKey('storage_key_signature', 'file_uploads', 'storage_key', ['delete'=> 'RESTRICT', 'update'=> 'CASCADE'])->save();
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }

    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('cc_transactions') && $this->table('cc_transactions')->hasColumn('storage_key_signature')) {
                $table = $this->table('cc_transactions');
                $table
                ->dropForeignKey('storage_key_signature')
                ->removeColumn('storage_key_signature')
                ->save();
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
