<?php


use Phinx\Migration\AbstractMigration;

class LocationDbLp8210 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
	$this->status = true;
	try {
		if ($this->hasTable('order_contents') == true && $this->table('order_contents')->hasColumn('ordertag_id') == false) {
			$this->execute("ALTER TABLE `order_contents` ADD COLUMN `ordertag_id` INT(11) COMMENT 'captures the order tag id from POS', ADD CONSTRAINT FK_OCOrderTagId FOREIGN KEY (`ordertag_id`) REFERENCES `order_tags`(`id`)");
    		}
		if ($this->hasTable('modifiers_used') == true && $this->table('modifiers_used')->hasColumn('ordertag_id') == false) {
			$this->execute("ALTER TABLE `modifiers_used` ADD COLUMN `ordertag_id` INT(11) COMMENT 'captures the order tag id from POS', ADD CONSTRAINT FK_MUOrderTagId FOREIGN KEY (`ordertag_id`) REFERENCES `order_tags`(`id`)");
    		}
    	}
	catch (PDOException $exception) {
		$this->status = false;
		$this->logException($this->getName(), $exception->getMessage());
	}
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
	$this->execute("SET FOREIGN_KEY_CHECKS = 0");
    	try {
    		if ($this->hasTable('order_contents') == true && $this->table('order_contents')->hasColumn('ordertag_id') == true) {
			$this->execute("ALTER TABLE `order_contents` DROP FOREIGN KEY `FK_OCOrderTagId`, DROP COLUMN `ordertag_id`");
    		}
    		if ($this->hasTable('modifiers_used') == true && $this->table('modifiers_used')->hasColumn('ordertag_id') == true) {
			$this->execute("ALTER TABLE `modifiers_used` DROP FOREIGN KEY `FK_MUOrderTagId`, DROP COLUMN `ordertag_id`");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
	$this->execute("SET FOREIGN_KEY_CHECKS = 1");
    }
}
