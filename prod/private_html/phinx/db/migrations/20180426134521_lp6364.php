<?php
use Phinx\Migration\AbstractMigration;

class Lp6364 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $propsAutoInc=array('signed' => false, 'limit' =>11, 'identity' => true);
        $propsInt=array('limit' => 11, 'null' => false, 'default' => 0);
        $propsString=array('limit' => 255, 'null' => false, 'default' => '');
        $propsDT=array('default' => 'CURRENT_TIMESTAMP');
        $propsTinyInt=array('limit' => 1, 'null' => false, 'default'=>0);
        $propsText=array('null' => false, 'default'=>'');
        $propsFK=array('delete' => 'NO_ACTION', 'update' => 'NO ACTION');

        $tblName='fiscal_receipt_tpl';
        if(!$this->hasTable($tblName)){
            $table = $this->table($tblName, ['id' => false, 'primary_key' => 'id']);
            $table
            ->addColumn('id', 'integer', $propsAutoInc)
            ->addColumn('fiscal_receipt_tpl_field_id','integer',$propsInt)
            ->addColumn('value','string',$propsString)
            ->addColumn('label','string',$propsString)
            ->addColumn('display','integer',$propsTinyInt)
            ->addColumn('fiscal_receipt_tpl_id','integer',$propsInt)
            ->addColumn('fiscal_receipt_section_id','integer',$propsInt)
            ->addColumn('created_date', 'datetime', $propsDT)
            ->addColumn('updated_date', 'datetime', ['null' => false])
            ->addColumn('created_by', 'integer', $propsInt)
            ->addColumn('updated_by', 'integer', $propsInt)
            ->addColumn('_deleted', 'integer', $propsTinyInt)
            ->create();
        }

        $tblName='fiscal_receipt_invoice';
        if(!$this->hasTable($tblName)){
            $table = $this->table($tblName, ['id' => false, 'primary_key' => 'fiscal_receipt_invoice_id']);
            $table
            ->addColumn('fiscal_receipt_invoice_id', 'integer', $propsAutoInc)
            ->addColumn('server_id','string',$propsString)
            ->addColumn('order_id','string',$propsString)
            ->addColumn('invoice_number','integer',$propsInt)
            ->addColumn('created_date', 'datetime', $propsDT)
            ->addColumn('created_by', 'integer', $propsInt)
            ->addColumn('_deleted', 'integer', $propsTinyInt)
            ->create();
        }

        if($this->hasTable('orders')){
            if(!$this->table('orders')->hasColumn('invoice_number')){
                $this->execute('ALTER TABLE orders ADD COLUMN invoice_number VARCHAR(50) NOT NULL DEFAULT \'\'');
            }
        }

        if($this->hasTable('split_check_details')){
            if(!$this->table('split_check_details')->hasColumn('invoice_number')){
                $this->execute('ALTER TABLE split_check_details ADD COLUMN invoice_number VARCHAR(50) NOT NULL DEFAULT \'\'');
            }
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if($this->hasTable('fiscal_receipt_tpl')){
            $this->execute('DROP TABLE fiscal_receipt_tpl');
        }

        if($this->hasTable('fiscal_receipt_invoice')){
            $this->execute('DROP TABLE fiscal_receipt_invoice');
        }

        if($this->hasTable('orders')){
            if($this->table('orders')->hasColumn('invoice_number')){
                $this->execute('ALTER TABLE orders DROP COLUMN invoice_number');
            }
        }

        if($this->hasTable('split_check_details')){
            if($this->table('split_check_details')->hasColumn('invoice_number')){
                $this->execute('ALTER TABLE split_check_details DROP COLUMN invoice_number');
            }
        }

    }
}