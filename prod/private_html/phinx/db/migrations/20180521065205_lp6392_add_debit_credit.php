<?php


use Phinx\Migration\AbstractMigration;

class Lp6392AddDebitCredit extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('general_ledger_settings') == true && $this->table('general_ledger_settings')->hasColumn('transaction_type') ==false) {
    			$this->execute("ALTER TABLE `general_ledger_settings` ADD COLUMN `transaction_type` varchar(10) NOT NULL");
    			$this->execute("update `general_ledger_settings` set transaction_type='debit' where lineitem_name  in ('Cash Payments','Credit Card Sales','Discounts')");
    			$this->execute("update `general_ledger_settings` set transaction_type='credit' where lineitem_name in ('Sales Tax','Tips')");
    		}
    	}
    	catch (PDOException $exception) {
    		$this->status = false;
    		$this->logException($this->getName(), $exception->getMessage());
    	}
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('general_ledger_settings') == true && $this->table('general_ledger_settings')->hasColumn('transaction_type') ==true) {
    			$this->execute("ALTER TABLE `general_ledger_settings` DROP COLUMN `transaction_type`");
    		}
    	}
    	catch (PDOException $exception) {
    		$this->status = false;
    		$this->logException($this->getName(), $exception->getMessage());
    	}
    }
}
