<?php


use Phinx\Migration\AbstractMigration;

class PapiApiPaypalLp8826 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('cc_transactions') == true && $this->table('cc_transactions')->hasColumn('api') == false) {
                $this->execute("ALTER TABLE `cc_transactions`
                                ADD COLUMN `api`
                                varchar(20) DEFAULT 'lavu', ADD INDEX `idx_api` (`api`)");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ( $this->hasTable('cc_transactions') == true && $this->table('cc_transactions')->hasColumn('api') == true ) {
                $this->execute("ALTER TABLE `cc_transactions` DROP COLUMN `api`");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
