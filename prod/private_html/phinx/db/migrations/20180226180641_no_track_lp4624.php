<?php


use Phinx\Migration\AbstractMigration;

class NoTrackLp4624 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if (!$this->table('inventory_items')->hasColumn('inventoryOverage')) {
            $this->execute("ALTER TABLE inventory_items ADD COLUMN `inventoryOverage` double unsigned DEFAULT '0'");
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->table('inventory_items')->hasColumn('inventoryOverage')) {
            $this->execute("ALTER TABLE inventory_items DROP COLUMN `inventoryOverage`");
        }
    }
}
