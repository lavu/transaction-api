<?php


use Phinx\Migration\AbstractMigration;

class AddCommentToData1UserSchedule extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('user_schedules')) {
                $this->table('user_schedules')->engine='InnoDB';

                if ($this->table('user_schedules')->hasColumn('data1')) {
                    $this->table('user_schedules')->changeColumn('data1', 'string', array('comment' => 'This field is used to store the `id` of the `emp_classes` table to identify which class the shift belongs to'))->update();
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('user_schedules')) {
                $this->table('user_schedules')->engine='InnoDB';

                if ($this->table('user_schedules')->hasColumn('data1')) {
                    $this->table('user_schedules')->changeColumn('data1', 'string', array('comment' => ''))->update();
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
