<?php


use Phinx\Migration\AbstractMigration;

class Lp7272TipoutBreadownMigration extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('emp_classes') == true && $this->table('emp_classes')->hasColumn('tipout_break') == false) {
                $this->execute("ALTER TABLE `emp_classes` ADD COLUMN `tipout_break` varchar(255) NOT NULL");
                $this->execute("ALTER TABLE `emp_classes_temp` ADD COLUMN `tipout_break` varchar(255) NOT NULL");
            }
    	    
            if ( $this->hasTable('tipouts') == true
    	        && $this->table('tipouts')->hasColumn('breakdown_type') == false
    	        && $this->table('tipouts')->hasColumn('breakdown_detail') == false
    	        ) {
    	            $this->execute("ALTER TABLE `tipouts` ADD COLUMN `breakdown_type` varchar(100) NOT NULL,
                                                  ADD COLUMN `breakdown_detail` text NOT NULL");
    	        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('emp_classes') == true && $this->table('emp_classes')->hasColumn('tipout_break') == true) {
                $this->execute("ALTER TABLE `emp_classes` DROP COLUMN `tipout_break` ");
                $this->execute("ALTER TABLE `emp_classes_temp` DROP COLUMN `tipout_break` ");
            }
    	    
            if ( $this->hasTable('tipouts') == true
    	        && $this->table('tipouts')->hasColumn('breakdown_type') == true
    	        && $this->table('tipouts')->hasColumn('breakdown_detail') == true
    	        ) {
    	            $this->execute("ALTER TABLE `tipouts` DROP COLUMN `breakdown_type`,
                                                  DROP COLUMN `breakdown_detail`");
    	        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}