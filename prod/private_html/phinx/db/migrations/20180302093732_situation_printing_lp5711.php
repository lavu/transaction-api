<?php


use Phinx\Migration\AbstractMigration;

class SituationPrintingLp5711 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable('config')) {
            if (!$this->table('config')->hasColumn('value16')) {
                $this->execute("ALTER TABLE `config` ADD COLUMN `value16` varchar(255) NOT NULL DEFAULT '0'");
            }
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $configTableExists = $this->hasTable('config');
        if ($this->table('config')->hasColumn('value16')) {
            $this->execute('ALTER TABLE `config` DROP COLUMN `value16`');
        }
    }
}
