<?php


use Phinx\Migration\AbstractMigration;

class UpdateCustomDictionaryLp10020 extends AbstractMigration
{
    public $status;
    public $tableName = 'custom_dictionary';
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		/* Write migration code here */
        if ($this->hasTable($this->tableName) && !$this->table($this->tableName)->hasColumn('tag')) {
          $this->execute('ALTER TABLE '.$this->tableName.' ADD COLUMN tag VARCHAR(50) NOT NULL DEFAULT \'\'');
          $this->execute('ALTER TABLE '.$this->tableName.' ADD INDEX `idx_tag` (tag)');
        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
        //Rollback is not required for this migration as it might result to fatal error for custom_language.php for column tag
        /* Write migration code here */
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
