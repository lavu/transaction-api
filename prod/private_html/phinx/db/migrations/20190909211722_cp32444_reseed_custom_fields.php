<?php


use Phinx\Migration\AbstractMigration;

class Cp32444ReseedCustomFields extends AbstractMigration
{
    public $status;

    private $tables = [
        'custom_fields',
        'custom_field_types',
        'custom_fields_metadata'
    ];

    private $queries = [
        "custom_fields" => "INSERT INTO custom_fields(id, location_id, label, sort, is_default, created_at, updated_at)
                VALUES (1, 1, 'Your information', 1, 1, {now}, {now});",
        "custom_field_types" => "
            SELECT id INTO @gen_field_source from poslavu_MAIN_db.field_source_types WHERE code = 'GEN';
            INSERT INTO custom_field_types (id, field_source_type_id, label, name, code, created_at, updated_at)
                VALUES
                    (1, @gen_field_source, 'Text', 'Text', 'TEXT', {now}, {now}),
                    (2, @gen_field_source, 'Numeric', 'Numeric', 'NUM', {now}, {now}),
                    (3, @gen_field_source, 'Email', 'Email', 'EMAIL', {now}, {now}),
                    (4, @gen_field_source, 'Name', 'Name', 'NAME', {now}, {now}),
                    (5, @gen_field_source, 'Phone', 'Phone', 'PHONE', {now}, {now});
        ",
        "custom_fields_metadata" => "
            SELECT id INTO @name_field_type from custom_field_types WHERE code = 'NAME';
            SELECT id INTO @label_cfield from custom_fields WHERE label = 'Your information';
            INSERT INTO custom_fields_metadata (id, custom_field_type_id, custom_field_id, label, placeholder, is_required, sort, created_at, updated_at)
                VALUES
                    (1, @name_field_type, @label_cfield, 'Your information', 'Enter your name', 1, 1, {now}, {now});
        ",
    ];
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            foreach ($this->tables as $table) {
                if ($this->hasTable($table)) {
                    $existentFields = $this->fetchAll("SELECT `id` FROM `".$table."`");
                    $currentTime = (new Datetime())->format('Y-m-d H:i:s');

                    if (count($existentFields) === 0) {
                        $callback = "trim";
                        $currentQueries = array_filter(array_map($callback, explode(";", $this->queries[$table])));

                        foreach ($currentQueries as $q) {
                            $this->execute(str_replace("{now}", "'".$currentTime."'", $q));
                        }
                    }
                }
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     * 
     * To can detect which DN’s had the data added by this migration. We will look
     * at the `created_at` time of the records on the given and compare it to the `start_time`
     * of the migration record on `phinxlog`. If ALL records on the given table have
     * the same creation time and are similar (with a 2 seconds difference at most) to
     * the creation time of the phinxlog record we can safely assume that the given
     * DN was reseeded by the current migration
     */
    public function down()
    {
        $this->status = true;
        try {
            $migrationTable = $this->getOptions()['default_migration_table'];
            $migrationRow = $this->fetchRow("SELECT * FROM `".$migrationTable."` WHERE `migration_name` = '".get_class($this)."'");
            $minStartTime = DateTime::createFromFormat('Y-m-d H:i:s', $migrationRow['start_time'])->sub(new DateInterval('PT2S'))->format('Y-m-d H:i:s');


            foreach ($this->tables as $table) {
                if ($this->hasTable($table)) {
                    $fieldsCreatedWithMigration = $this->fetchAll("SELECT * FROM `".$table."` WHERE `created_at` >= '".$minStartTime."' AND `created_at` <= '".$migrationRow['start_time']."'");
                    $allFieldsCount = $this->fetchRow("SELECT count(*) AS count FROM `".$table."`")['count'];

                    if (count($fieldsCreatedWithMigration) == $allFieldsCount) {
                        $this->execute("DELETE FROM ".$table);
                    }
                }
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
