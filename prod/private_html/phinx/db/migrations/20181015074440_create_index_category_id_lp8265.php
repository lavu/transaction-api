<?php


use Phinx\Migration\AbstractMigration;

class CreateIndexCategoryIdLp8265 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    	    
    	    $rowData = $this->fetchRow("SHOW INDEX FROM `menu_items` WHERE Key_name = 'category_id'");
    	    
    	    if(!isset($rowData['Key_name'])) {
    	        $this->execute("ALTER TABLE `menu_items` ADD INDEX `category_id` (`category_id`)");
    	    }
    	    
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		
    	    $rowData = $this->fetchRow("SHOW INDEX FROM `menu_items` WHERE Key_name = 'category_id'");
    	    
    	    if (isset($rowData['Key_name']) && $rowData['Key_name']=='category_id') {
    	        $this->execute("ALTER TABLE `menu_items` DROP KEY `category_id`");
    	    }
    	    
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
