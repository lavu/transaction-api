<?php


use Phinx\Migration\AbstractMigration;

class AddColumnServerIdLp6356 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable('tipout_refunds') == true && $this->table('tipout_refunds')->hasColumn('server_id') == false) {
            $this->execute("ALTER TABLE `tipout_refunds` ADD COLUMN `server_id` int(11) NOT NULL");
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable('tipout_refunds') == true && $this->table('tipout_refunds')->hasColumn('server_id') == true) {
            $this->execute("ALTER TABLE `tipout_refunds` DROP COLUMN `server_id`");
        }
    }
}
