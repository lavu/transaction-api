<?php


use Phinx\Migration\AbstractMigration;

class MapDttDevicesintheControlPanelLp8081 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if ($this->hasTable('dtt_pairs') == false ) {
				$this->execute('CREATE TABLE IF NOT EXISTS `dtt_pairs` (
								`id` int(11) NOT NULL AUTO_INCREMENT,
								`config_id` int(11) NOT NULL,
								`device_uuid` varchar(255) NOT NULL DEFAULT "",
								`created_by` int(11) NOT NULL DEFAULT 0 ,
								`created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
								 PRIMARY KEY (`id`),
								 CONSTRAINT `fk_config_dtt_pairs_congig_id` FOREIGN KEY (`config_id`) REFERENCES `config` (`id`),
								 UNIQUE KEY `device_uuid` (`device_uuid`,`config_id`)
								) ENGINE=InnoDB CHARSET=utf8');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if($this->hasTable('dtt_pairs') == true) {
				$this->execute('DROP TABLE `dtt_pairs`');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}