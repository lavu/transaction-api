<?php


use Phinx\Migration\AbstractMigration;

class NewMigrationLp4140 extends AbstractMigration
{
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			// add table
			$exists = $this->hasTable('change_bank');
			if ($exists == FALSE) {
				$table = $this->table('change_bank');
				$table
					->addColumn('amount', 'float', ['limit' => [11, 2], 'null' => false])
					->addColumn('user_id', 'integer', ['limit' => 11, 'null' => false])
					->addColumn('required_amount', 'float', ['limit' => [11, 2], 'null' => false])
					->addColumn('created_date', 'datetime', ['null' => false])
					->addIndex(['user_id'], ['unique' => true, 'name' => 'user_id'])
					->addForeignKey('user_id', 'users', 'id', ['constraint' => 'change_bank_ibfk_1'])
					->create();
			}

			$bdexists = $this->hasTable('bank_deposit');
			if ($bdexists == FALSE) {
				$bdtable = $this->table('bank_deposit');
				$bdtable
					->addColumn('amount', 'float', ['limit' => [11, 2], 'null' => false])
					->addColumn('user_id', 'integer', ['limit' => 11, 'null' => false])
					->addColumn('required_deposit', 'float', ['limit' => [11, 2], 'null' => false])
					->addColumn('created_date', 'datetime', ['null' => false])
					->addIndex(['user_id'], ['unique' => true, 'name' => 'user_id'])
					->addForeignKey('user_id', 'users', 'id', ['constraint' => 'bank_deposit_ibfk_1'])
					->create();
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}

	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			$changeBankExists = $this->hasTable('change_bank');
			if ($changeBankExists == true) {
				$this->dropTable('change_bank');
			}

			$bankDepositExists = $this->hasTable('bank_deposit');
			if ($bankDepositExists == true) {
				$this->dropTable('bank_deposit');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}