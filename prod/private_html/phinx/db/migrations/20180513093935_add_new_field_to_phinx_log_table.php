<?php
use Phinx\Migration\AbstractMigration;

class AddNewFieldToPhinxLogTable extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    	    if ($this->table('phinxlog')->hasColumn('release') == false) {
    	        $this->execute("ALTER TABLE `phinxlog` ADD COLUMN `release` varchar(100) NOT NULL");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
