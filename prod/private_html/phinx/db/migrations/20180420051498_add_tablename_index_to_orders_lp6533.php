<?php


use Phinx\Migration\AbstractMigration;


class AddTablenameIndexToOrdersLp6533 extends AbstractMigration {
  /**
  * up() Method to migrate.
  */
  public function up() {
      if ($this->hasTable('orders')) {
        $indexPresent = $this->getAdapter()->hasIndexByName('orders', 'idx_tablename') == 1 ? true : false;
        if (!$indexPresent) {
          $table = $this->table('orders');
          $table->addIndex(['tablename'], ['name' => 'idx_tablename']);
          $table->save();
        }
      }
    }

  /**
  * down() Method to rollback.
  */
  public function down() {
    if ($this->hasTable('orders')) {
      $indexPresent = $this->getAdapter()->hasIndexByName('orders', 'idx_tablename') == 1 ? true : false;
      // if the index is present we're able to delete it
      if ($indexPresent) {
        $table = $this->table('orders');
        $table->removeIndex(['tablename'], ['name' => 'idx_tablename']);
        $table->save();
      }
    }
  }
}