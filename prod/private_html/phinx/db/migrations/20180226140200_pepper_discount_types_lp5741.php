<?php


use Phinx\Migration\AbstractMigration;

class PepperDiscountTypesLp5741 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable('discount_types')) {
            if (!$this->table('discount_types')->hasColumn('token')) {
                $this->execute("ALTER TABLE `discount_types` ADD COLUMN `token` VARCHAR(40) NOT NULL");
            }
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable('discount_types')) {
            if ($this->table('discount_types')->hasColumn('token')) {
                $this->execute('ALTER TABLE `discount_types` DROP COLUMN `token`');
            }
        }
    }
}
