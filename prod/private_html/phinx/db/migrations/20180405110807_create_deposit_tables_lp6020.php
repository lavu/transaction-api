<?php


use Phinx\Migration\AbstractMigration;

class CreateDepositTablesLp6020 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if($this->hasTable('deposit_customer_info')==false){
            $this->execute('CREATE TABLE IF NOT EXISTS `deposit_customer_info` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `first_name` varchar(250) NOT NULL,
                    `last_name` varchar(250) NOT NULL,
                    `email` varchar(100) NOT NULL,
                    `phone` varchar(20) NOT NULL,
                    `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
                    `status` int(1),
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB CHARSET=utf8');
        }
        
        if($this->hasTable('deposit_information')==false){
            $this->execute('CREATE TABLE IF NOT EXISTS `deposit_information` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `customer_id` int(11),
                            `transaction_id` varchar(255),
                            `reason` varchar (255),
                            `amount` varchar(30),
                            `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
                            `refund_amount` varchar(30) DEFAULT NULL,
                            `refund_date` datetime DEFAULT NULL,
                            `refund_reason` varchar (255) DEFAULT NULL,
                            `deposit_type` varchar(15),
                            `pay_type` varchar(15),
                            `pay_status` int(1),
                            `transaction_status` int(1) DEFAULT 0,
                            PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB CHARSET=utf8');
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if($this->hasTable('deposit_information')==true){
            $this->execute('DROP TABLE `deposit_information`');
        }
        
        if($this->hasTable('deposit_customer_info')==true){
            $this->execute('DROP TABLE `deposit_customer_info`');
        }
    }
}
