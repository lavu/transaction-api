<?php

use Phinx\Migration\AbstractMigration;

class Lp7128TipSharingReport extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable ( 'reports_v2' )) {
    		    $rows = [
                    'selection_column' => '',
                    'special_code' => '//custom:get_tip_sharing',
                    'selection' => 'Preset Date Ranges',
                    'table' => 'tip_sharing',
                    'title' => 'Tip Sharing',
                    'name' => 'tip_sharing',
                    'orderby' => '',
                    'select' => '',
                    'groupby' => '',
                    'where' => '',
                    'join' => '',
                    '_order' => NULL,
                    '_deleted' => 1,
                    'required_modules' => '',
                    'custom_sister_list' => ''
                ];
    		    $table = $this->table ( 'reports_v2' );
    		    $table->insert ( $rows )->saveData ();
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable( 'reports_v2' )) {
    		    $this->execute( "DELETE FROM `reports_v2` WHERE `title` = 'Tip Sharing' ");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
