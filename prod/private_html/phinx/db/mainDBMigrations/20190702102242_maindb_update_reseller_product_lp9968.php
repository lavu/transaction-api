<?php


use Phinx\Migration\AbstractMigration;

class MaindbUpdateResellerProductLp9968 extends AbstractMigration
{
  public $status;
  public $bkpTable="reseller_products_bkp";
  //public $newCategories=array('Payments', 'POS Bundles');

  /**
   * up() Method to migrate.
   */
  public function up()
  {
      $this->status = true;

    try {
          /* Write migration code here */
          //take table backup
          if ($this->hasTable('reseller_products')) {
              if (!$this->hasTable($this->bkpTable)) {
                  $this->execute('CREATE TABLE IF NOT EXISTS '.$this->bkpTable.' AS SELECT * FROM reseller_products');
              } else {
                  $this->execute('TRUNCATE TABLE '.$this->bkpTable);
                  $this->execute('INSERT INTO '.$this->bkpTable.' (reseller_product_id, name, description, info, category_id, created_date, update_date, _deleted) SELECT reseller_product_id, name, description, info, category_id, created_date, update_date, _deleted FROM reseller_products');
              }

              $result1=$this->fetchAll('SELECT count(1) as cnt FROM reseller_products');
              $result2=$this->fetchAll('SELECT count(1) as cnt FROM '.$this->bkpTable);

              if ($result1[0]['cnt'] != $result2[0]['cnt']) {
                  throw new Exception("Migration failed, Backup table reseller_products_bkp does not match count with reseller_products");
              } else {
                  $this->execute('TRUNCATE TABLE reseller_products');
              }
          }

          $result=$this->fetchAll('SELECT reseller_product_category_id, name FROM reseller_product_category');
          foreach ($result as $rec) {
              $categories[$rec['name']] = $rec['reseller_product_category_id'];
          }

          $categories['Card Readers: Cable'] = $categories['Card Readers and Cables'];
          $categories['Cash Drawer: Accessory'] = $categories['Cash Drawer and Accessory'];
          $categories['Computer: Accessory'] = $categories['Computer and Accessory'];
          $categories['Printer'] = $categories['Printers'];

          //parse CSV
          $fp=fopen("/home/poslavu/private_html/phinx/reseller-products-latest.csv","r");

          fgetcsv($fp,1000,",");
          $count=0;
          while (($r=fgetcsv($fp,1000,","))!=FALSE) {
              $count++;
              $r[3]=str_replace(',','',$r[3]);
              $r[4]=str_replace(',','',$r[4]);
              $resellerCost=($r[3]=='(blank)')?'':$r[3];
              $salePrice=($r[4]=='(blank)')?'':$r[4];
              $info='^^'.$resellerCost.'^'.$salePrice;
              $prodName=addslashes($r[0]);
              $prodDesc=addslashes($r[1]);
              $catId = $categories[trim($r[2])];

              $q='INSERT INTO reseller_products(name,description,info,category_id,created_date,_deleted) VALUES(
              "'.$prodName.'","'.$prodDesc.'","'.$info.'",'.$catId.',NOW(),0);';
              $this->execute($q);
          }
    }
    catch (PDOException $exception) {
          $this->status = false;
          $this->logException($this->getName(), $exception->getMessage());
      }
  }

  /**
   * down() Method to rollback.
   */
  public function down()
  {
    $this->status = true;
    try {
          /* Write rollback code here */
          if ($this->hasTable('reseller_products')) {
              if ($this->hasTable($this->bkpTable)) {
                  $this->execute('TRUNCATE TABLE reseller_products');
                  $this->execute('INSERT INTO reseller_products (reseller_product_id, name, description, info, category_id, created_date, update_date, _deleted) SELECT reseller_product_id, name, description, info, category_id, created_date, update_date, _deleted FROM '.$this->bkpTable);
              }

              $result1=$this->fetchAll('SELECT count(1) as cnt FROM reseller_products');
              $result2=$this->fetchAll('SELECT count(1) as cnt FROM '.$this->bkpTable);

              if ($result1[0]['cnt'] != $result2[0]['cnt']) {
                  throw new Exception("Migration failed, Backup table reseller_products_bkp does not match count with reseller_products");
              }

          }
    }
    catch (PDOException $exception) {
          $this->status = false;
          $this->logException($this->getName(), $exception->getMessage());
      }
  }
}
