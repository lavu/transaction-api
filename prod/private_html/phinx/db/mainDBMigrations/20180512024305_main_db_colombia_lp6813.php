<?php


use Phinx\Migration\AbstractMigration;

class MainDbColombiaLp6813 extends AbstractMigration
{
    public $region="Colombia";

   /**
     * up() Method to migrate.
     */
    public function up()
    {

        $curTime=time();

        $propsAutoInc=array('signed' => false, 'limit' =>11, 'identity' => true);
        $propsInt=array('limit' => 11, 'null' => false, 'default' => 0);
        $propsUnsignedInt=array('signed' => false, 'limit' => 11, 'null' => false, 'default' => 0);
        $propsString=array('limit' => 255, 'null' => false, 'default' => '');
        $propsDT=array('default' => 'CURRENT_TIMESTAMP');
        $propsTinyInt=array('limit' => 1, 'null' => false, 'default'=>0);
        $propsText=array('null' => false, 'default'=>'');
        $propsFK=array('delete' => 'NO_ACTION', 'update' => 'NO ACTION');

    	if($this->hasTable('fiscal_receipt_tpl')){

                $result=$this->fetchAll('SELECT fiscal_receipt_tpl_id FROM fiscal_receipt_tpl WHERE title="'.$this->region.'"');
                $tplId=$result[0]['fiscal_receipt_tpl_id'];

                $tblName='fiscal_receipt_tpl_fields';
                if($this->hasTable($tblName)){

                    $table=$this->table($tblName);  
                    if(!$table->hasColumn('allow_edit')){
                        $this->execute('ALTER TABLE '.$tblName.' ADD COLUMN allow_edit smallint NOT NULL DEFAULT 1');
                    }

                    //FIRST THING TO REMOVE ALL FIELDS FOR THIS TPL
                    $this->execute('DELETE FROM '.$tblName.' WHERE fiscal_receipt_tpl_id='.$tplId);

                    $fields=array();
                    $sections=array();

                    $result=$this->fetchAll('SELECT fiscal_receipt_section_id,title FROM fiscal_receipt_sections WHERE title NOT IN ("Location Information")');
                    foreach($result as $row){
                        $sections[$row[1]]=$row[0];
                    }

                    $result=$this->fetchAll('SELECT fiscal_receipt_field_id,title FROM fiscal_receipt_fields');
                    foreach($result as $row){
                        $fields[$row[1]]=$row[0];
                    }

                    if($this->hasTable('fiscal_receipt_tpl_sections')){
			             $seqs=array('Header'=>1,'General Order Information'=>2,'Customer Information'=>3,'Order Information'=>4,'Footer'=>5);
                         $this->execute('DELETE FROM fiscal_receipt_tpl_sections WHERE fiscal_receipt_tpl_id='.$tplId);
                         foreach($sections as $key=>$val){
                            $allowEdit=1;
                            if(in_array($key,array('Customer Information','Order Information','Location Information','Footer'))){
                                $allowEdit=0;
                            }
                            $this->execute('INSERT INTO fiscal_receipt_tpl_sections (fiscal_receipt_tpl_id, fiscal_receipt_section_id, allow_edit, seq) VALUES ('.$tplId.','.$val.','.$allowEdit.','.$seqs[$key].')');
                        }
                    }

                    //ADD REQUIRED FIELDS TO THIS TPL
                    $data=array(
                            1=> array(
                                'field_id'=>$fields['company_name'], 'label'=>'Business Name','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L","type":"text","size":25}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>"business_name"),

                                array('field_id'=>$fields['text_input'], 'label'=>'NIT','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L","type":"text","size":25}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>"nit"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Regimen','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L","type":"text","size":25}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>"regimen"),

                                array('field_id'=>$fields['company_address'], 'label'=>'Business Address','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L","type":"textarea","size":"1-14"}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>""),

                                array('field_id'=>$fields['text_input'], 'label'=>'City/Country','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L"},"type":"text","size":25}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>"city_state_country"),

                                array('field_id'=>$fields['telephone'], 'label'=>'Telephone','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L","type":"text","size":25}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>""),

                                array('field_id'=>$fields['text_input'], 'label'=>'Resolution','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L","type":"text","size":25}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>"resolucion"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Resolution Number/Date','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L","type":"text","size":25}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>"resolucion_number"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Rango','display'=>'1','allow_hide'=>'1','required'=>'1', 'format'=>'{"pos":"L","type":"text","size":25}', 'show_label'=>'0', 'type'=>'field', 'section_id'=>$sections['Header'],"macros"=>"rango"),

                                array('field_id'=>$fields['div'], 'label'=>'div','display'=>'1','allow_hide'=>'0','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'0', 'type'=>'', 'section_id'=>$sections['Header'],"macros"=>"","allow_edit"=>0),

                                array('field_id'=>$fields['invoice_number'], 'label'=>'Factura de Venta','display'=>'1','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'', 'section_id'=>$sections['General Order Information'],"macros"=>""),

                                array('field_id'=>$fields['order_date'], 'label'=>'Fecha','display'=>'1','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'', 'section_id'=>$sections['General Order Information'],"macros"=>""),

                                array('field_id'=>$fields['order_timestamp'], 'label'=>'Hora','display'=>'1','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'', 'section_id'=>$sections['General Order Information'],"macros"=>""),

                                array('field_id'=>$fields['table_name'], 'label'=>'Mesa','display'=>'1','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'', 'section_id'=>$sections['General Order Information'],"macros"=>"new_table_name"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Order #','display'=>'1','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'system', 'section_id'=>$sections['General Order Information'],"macros"=>"fiscal_order_id"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Check','display'=>'0','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'system', 'section_id'=>$sections['General Order Information'],"macros"=>"fiscal_check"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Guests','display'=>'0','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'system', 'section_id'=>$sections['General Order Information'],"macros"=>"new_guest_count"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Server Name','display'=>'0','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'system', 'section_id'=>$sections['General Order Information'],"macros"=>"fiscal_server"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Cashier Name','display'=>'0','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'system', 'section_id'=>$sections['General Order Information'],"macros"=>"fiscal_cashier"),

                                array('field_id'=>$fields['text_input'], 'label'=>'Register Name','display'=>'0','allow_hide'=>'1','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'1', 'type'=>'system', 'section_id'=>$sections['General Order Information'],"macros"=>"fiscal_register"),                                

                                array('field_id'=>$fields['div'], 'label'=>'div','display'=>'1','allow_hide'=>'0','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'0', 'type'=>'', 'section_id'=>$sections['General Order Information'],"macros"=>"","allow_edit"=>0),

                                array('field_id'=>$fields['customer_info'], 'label'=>'Customer Info','display'=>'1','allow_hide'=>'0','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'0', 'type'=>'', 'section_id'=>$sections['Customer Information'],"macros"=>"","allow_edit"=>0),
                                
                                array('field_id'=>$fields['div'], 'label'=>'','display'=>'1','allow_hide'=>'0','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'0', 'type'=>'', 'section_id'=>$sections['Customer Information'],"macros"=>"","allow_edit"=>0),

                                array('field_id'=>$fields['items_totals_payments'], 'label'=>'Order Info','display'=>'1','allow_hide'=>'0','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'0', 'type'=>'', 'section_id'=>$sections['Order Information'],"macros"=>"","allow_edit"=>0),

                                array('field_id'=>$fields['div'], 'label'=>'div','display'=>'1','allow_hide'=>'0','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'0', 'type'=>'', 'section_id'=>$sections['Order Information'],"macros"=>"","allow_edit"=>0),

				                array('field_id'=>$fields['lavu_footer'], 'label'=>'Footer','display'=>'1','allow_hide'=>'0','required'=>'0', 'format'=>'{"pos":"L"}', 'show_label'=>'0', 'type'=>'', 'section_id'=>$sections['Footer'],"macros"=>"","allow_edit"=>0),

                        );

                    $this->execute('DELETE FROM '.$tblName.' WHERE fiscal_receipt_tpl_id='.$tplId);

                    foreach($data as $seq=>$v){
                        $this->execute('INSERT INTO '.$tblName.' (`fiscal_receipt_field_id`, `fiscal_receipt_tpl_id`, `label`, `display`, `allow_hide`, `seq`, `required`, `format`, `show_label`, `created_date`, `updated_date`, `created_by`, `updated_by`, `_deleted`, `type`, `fiscal_receipt_section_id`,`macros`,`allow_edit`) VALUES
                            ('.$v['field_id'].',
                            '.$tplId.',
                            "'.$v['label'].'",
                            "'.$v['display'].'",
                            "'.$v['allow_hide'].'",
                            '.$seq.',
                            "'.$v['required'].'",
                            "'.addslashes($v['format']).'",
                            '.$v['show_label'].',
                            '.time().',
                            '.time().',
                            0,
                            0,
                            "0",
                            "'.$v['type'].'",
                            '.$v['section_id'].',
                            "'.$v['macros'].'",
                            '.(isset($v['allow_edit'])?$v['allow_edit']:1).'
                            )'
                        );
                    }
                }
            }// END If check for fiscal_receipt_tpl
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {

	if ($this->hasTable('fiscal_receipt_tpl_fields')) {
        	$this->execute('TRUNCATE TABLE fiscal_receipt_tpl_fields');

        	if ($this->table('fiscal_receipt_tpl_fields')->hasColumn('allow_edit')) {
            		$this->execute('ALTER TABLE fiscal_receipt_tpl_fields DROP COLUMN allow_edit');
        	}
	}


    }
}
