<?php


use Phinx\Migration\AbstractMigration;

class FiscalReportLp6109 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable ( 'reports_v2' )) {
            $row = $this->fetchRow('SELECT * FROM `reports_v2` WHERE `title` = "Fiscal Reconciliation" ');
            if($row == true){
                $this->execute('UPDATE `reports_v2` SET `where` = "#zreport_dates and report_type IN (\'TiqueFacturaA\', \'TiqueFacturaB\',\'ReciboB\',\'TiqueNotaCreditoB\',\'TiqueNotaCreditoA\') and config._deleted != 1" WHERE `reports_v2`.`title` = "Fiscal Reconciliation" ');
            }
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable ( 'reports_v2' )) {
            $row = $this->fetchRow('SELECT * FROM `reports_v2` WHERE `title` = "Fiscal Reconciliation" ');
            if($row == true){
                $this->execute('UPDATE `reports_v2` SET `where` = "#zreport_dates and report_type IN (\'TiqueFacturaA\', \'TiqueFacturaB\',\'ReciboB\',\'TiqueNotaCreditoB\',\'TiqueNotaCreditoA\') " WHERE `reports_v2`.`title` = "Fiscal Reconciliation" ');
            }
        }
    }
}
