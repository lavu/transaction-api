<?php


use Phinx\Migration\AbstractMigration;

class AddIndexCcTipTransactionsLp11405 extends AbstractMigration
{
    public $status;
    public $indexes = array('dataname'=>'idx_dataname', 'order_id'=>'idx_order_id');
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('cc_tip_transactions') && $this->table('cc_tip_transactions')->hasColumn('dataname') && $this->table('cc_tip_transactions')->hasColumn('order_id')) {
                $rows = $this->fetchAll('SHOW INDEX FROM cc_tip_transactions');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (!in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'ADD INDEX `' . $index . '` (`' . $column . '`)';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE cc_tip_transactions ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('cc_tip_transactions') && $this->table('cc_tip_transactions')->hasColumn('dataname') && $this->table('cc_tip_transactions')->hasColumn('order_id')) {
                $rows = $this->fetchAll('SHOW INDEX FROM cc_tip_transactions');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'DROP INDEX `' . $index . '`';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE cc_tip_transactions ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
