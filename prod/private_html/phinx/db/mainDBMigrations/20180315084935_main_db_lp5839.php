<?php


use Phinx\Migration\AbstractMigration;

class MainDbLp5839 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	if ($this->hasTable ( 'supported_printers' )) {
    		if(!$this->table('supported_printers')->hasColumn('drawer_code_2')){
    			$this->execute("ALTER TABLE `supported_printers` ADD COLUMN `drawer_code_2` VARCHAR(255) NOT NULL DEFAULT '' AFTER drawer_code");
    		}
		    // INSERT INTO [POSLAVU MAIN DB].`supported_printers`
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Thermal (Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Impact (Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Impact (Basic, Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Thermal (Basic, Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Thermal #2 (Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson TM-T20 (Dual)"' );

		    // inserting multiple row
		    $multipleRow = [
			    [
				    'name' => 'Epson Thermal (Dual)',
				    'drawer_code' => '27,112,48,40,120',
				    'drawer_code_2' => '27,112,49,40,120',
				    'cutter_code' => '29,86,1',
				    'newline' => '10',
				    'job_start' => '',
				    'job_end' => '10,10,10,10',
				    'tab' => '9',
				    'bold' => '',
				    'underline' => '',
				    'font_size' => '',
				    'id' => '',
				    'emphasize' => '27,33,8',
				    'standard_font' => '27,33,0',
				    'print_red' => '',
				    'print_black' => '',
				    'double_height' => '27,33,16',
				    'line_spacing' => '27,51',
				    'get_status' => '29,97,126',
				    'bit_graphics_8' => '',
				    'bit_graphics_24' => '',
				    'raster_start' => '',
				    'raster_end' => '',
				    'brand' => 'Epson',
				    'asb' => 1,
				    'status_bytes' => 4,
				    'status_bits' => 16,
				    'offline_bit' => -1,
				    'overflow_bit' => -1,
				    'cover_open_bit' => -1,
				    'no_paper_bit' => -1,
				    'paper_low_bit' => 22,
				    'etb_byte' => -1,
				    'reset_etb' => '',
				    'update_etb' => '',
				    '_order' => 1,
				    '_deleted' => 0
			    ],
			    [
				    'name' => 'Epson Impact (Dual)',
				    'drawer_code' => '27,112,48,40,120',
				    'drawer_code_2' => '27,112,49,40,120',
				    'cutter_code' => '29,86,1',
				    'newline' => '10',
				    'job_start' => '',
				    'job_end' => '',
				    'tab' => '9',
				    'bold' => '',
				    'underline' => '',
				    'font_size' => '',
				    'id' => '',
				    'emphasize' => '27,33,8',
				    'standard_font' => '27,33,0',
				    'print_red' => '27,114,49',
				    'print_black' => '27,114,48',
				    'double_height' => '27,33,16',
				    'line_spacing' => '27,51',
				    'get_status' => '29,97,126',
				    'bit_graphics_8' => '',
				    'bit_graphics_24' => '',
				    'raster_start' => '',
				    'raster_end' => '',
				    'brand' => 'Epson',
				    'asb' => 1,
				    'status_bytes' => 4,
				    'status_bits' => 16,
				    'offline_bit' => -1,
				    'overflow_bit' => -1,
				    'cover_open_bit' => -1,
				    'no_paper_bit' => -1,
				    'paper_low_bit' => 22,
				    'etb_byte' => -1,
				    'reset_etb' => '',
				    'update_etb' => '',
				    '_order' => 2,
				    '_deleted' => 0
			    ],
			    [
				    'name' => 'Epson Impact (Basic, Dual)',
				    'drawer_code' => '27,112,48,40,120',
				    'drawer_code_2' => '27,112,49,40,120',
				    'cutter_code' => '29,86,1',
				    'newline' => '10',
				    'job_start' => '',
				    'job_end' => '10,10,10,10',
				    'tab' => '9',
				    'bold' => '',
				    'underline' => '',
				    'font_size' => '',
				    'id' => '',
				    'emphasize' => '',
				    'standard_font' => '',
				    'print_red' => '',
				    'print_black' => '',
				    'double_height' => '',
				    'line_spacing' => '',
				    'get_status' => '29,97,126',
				    'bit_graphics_8' => '',
				    'bit_graphics_24' => '',
				    'raster_start' => '',
				    'raster_end' => '',
				    'brand' => 'Epson',
				    'asb' => 1,
				    'status_bytes' => 4,
				    'status_bits' => 16,
				    'offline_bit' => -1,
				    'overflow_bit' => -1,
				    'cover_open_bit' => -1,
				    'no_paper_bit' => -1,
				    'paper_low_bit' => 22,
				    'etb_byte' => -1,
				    'reset_etb' => '',
				    'update_etb' => '',
				    '_order' => 3,
				    '_deleted' => 0
			    ],
			    [
				    'name' => 'Epson Thermal (Basic, Dual)',
				    'drawer_code' => '27,112,48,40,120',
				    'drawer_code_2' => '27,112,49,40,120',
				    'cutter_code' => '29,86,1',
				    'newline' => '10',
				    'job_start' => '',
				    'job_end' => '10,10,10,10',
				    'tab' => '9',
				    'bold' => '',
				    'underline' => '',
				    'font_size' => '',
				    'id' => '',
				    'emphasize' => '',
				    'standard_font' => '',
				    'print_red' => '',
				    'print_black' => '',
				    'double_height' => '',
				    'line_spacing' => '',
				    'get_status' => '',
				    'bit_graphics_8' => '',
				    'bit_graphics_24' => '',
				    'raster_start' => '',
				    'raster_end' => '',
				    'brand' => 'Epson',
				    'asb' => 1,
				    'status_bytes' => 4,
				    'status_bits' => 16,
				    'offline_bit' => -1,
				    'overflow_bit' => -1,
				    'cover_open_bit' => -1,
				    'no_paper_bit' => -1,
				    'paper_low_bit' => 22,
				    'etb_byte' => -1,
				    'reset_etb' => '',
				    'update_etb' => '',
				    '_order' => 4,
				    '_deleted' => 0
			    ]
		    ];
		    $table = $this->table ( 'supported_printers' );
		    $table->insert ( $multipleRow )->saveData ();
    	}
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	if ($this->hasTable ( 'supported_printers' )) {
    		if($this->table('supported_printers')->hasColumn('drawer_code_2')){
		    	$this->execute ("ALTER TABLE `supported_printers` DROP COLUMN `drawer_code_2`");
		    }
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Thermal (Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Impact (Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Impact (Basic, Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Thermal (Basic, Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson Thermal #2 (Dual)"' );
		    $this->execute ( 'DELETE FROM `supported_printers` WHERE name="Epson TM-T20 (Dual)"' );
    	}
    }
}
