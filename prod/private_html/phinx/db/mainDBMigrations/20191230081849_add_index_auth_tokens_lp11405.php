<?php


use Phinx\Migration\AbstractMigration;

class AddIndexAuthTokensLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('auth_tokens') && $this->table('auth_tokens')->hasColumn('dataname')) {
                $this->execute('ALTER TABLE auth_tokens MODIFY dataname varchar(255)');
                $row = $this->fetchRow('SHOW INDEX FROM auth_tokens WHERE Key_name = "idx_dataname"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE auth_tokens ADD INDEX `idx_dataname` (dataname)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('auth_tokens') && $this->table('auth_tokens')->hasColumn('dataname')) {
                $row = $this->fetchRow('SHOW INDEX FROM auth_tokens WHERE Key_name = "idx_dataname"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE auth_tokens DROP INDEX `idx_dataname`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
