<?php


use Phinx\Migration\AbstractMigration;

class Ltg34 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('payment_status')) {
				if (!$this->table('payment_status')->hasColumn('lavutogo_payment')) {
					$this->execute("ALTER TABLE payment_status ADD COLUMN lavutogo_payment INT(1) NOT NULL COMMENT 'This field used for LTG Status and which billing type is associated with location (Monthly/Anual)'");
				}
			}
			if ($this->hasTable('extensions') == true) {
				$lavutogoquery = $this->fetchRow("SELECT `id`, `title` FROM `extensions` WHERE `title`='Lavu To Go'");
				if (!isset($lavutogoquery['title'])) {
					$this->execute("INSERT INTO `extensions` (`title`, `thumbnail`, `short_description`, `description`, `module`, `group_ids`, `cost`, `cost_type`, `dev`, `prerequisites`, `_deleted`, `enabled_description`, `native`, `code_word`, `extra_info`) VALUES
							('Lavu To Go',
    						'LavuToGoLogo_2.png',
							'Lavu To Go is an online ordering system that is easy to use for both your restaurant staff and your customers. For staff that means easy training and set-up; for your customers that means an intuitive, user-friendly interface for placing orders. With online ordering, patrons of your restaurant can order from any mobile device and even prepay. Orders go directly to the kitchen, improving order accuracy and faster turnaround. Customers experience shorter lines and wait times.<br/><br/>With Lavu To Go, you will experience:<br/><br/>Increased to-go and delivery sales <br/>Higher order volume <br/>Increased efficiency <br/> Reduced labor costs.',
							'<table border=\'0\'><tr><td><div style=\'float:left;\' ><form action=\'\' method=\'post\'><div style=\'text-align:center;font-size:18px;\'> <b>Enable Lavu To Go </b> </div><div style=\'text-align:center;margin-top: 15px;\'> <img src=\'areas/extensions/images/LavuToGoLogo_2.png\' style=\'width:100px;\' />  </div><div style=\'text-align:center;font-size:14px;margin-top: 15px;\'> <b> Get started with Lavu To Go. Choose your package below. </b> </div> <div style=\'margin-top: 15px;\'><div style=\'margin: 15px auto;height: 30px;border: 1px solid #3CA5D9;border-radius: 4px;width: 80%;\' id=\'r1d1\'><div style=\'width: 65%;float:left;font-size:16px;margin-top: 4px;\'> &nbsp; &nbsp;<input type=\'radio\' name=\'pay_type\' id=\'pay_type\' value=\'75\' onclick=\'radioactive()\' checked> &nbsp Billed Monthly</div><div style=\'width: 35%;float: left;background: #3CA5D9;height: 30px;font-size:20px;\' id=\'r1d3\'>  <div style=\'color:#FFF;margin-top:6px;font-size: 15px;\' id=\'r1s1\'> &nbsp $75  <span style=\'font-size: 11px;\'> /month </span></div></div></div><div style=\'margin: 15px auto;height: 30px;border: 1px solid #ccc;border-radius: 4px;width: 80%;\' id=\'r2d1\'><div style=\'width: 65%;float:left;font-size:16px;margin-top: 4px;\'> &nbsp; &nbsp;<input type=\'radio\' name=\'pay_type\' id=\'pay_type2\' value=\'799\' onclick=\'radioactive()\'> &nbsp Billed Annually &nbsp <span style=\'background:#99BC09;color:#FFFFFF;font-size:10px;padding:5px;border-radius:4px;\'> <b style=\'padding:6px;\'>BEST DEAL </b></span></div><div style=\'width: 35%;float: left;background: #ccc;height: 30px;font-size:20px;\' id=\'r2d3\'><div style=\'color:#23163F;margin-top:6px;font-size: 15px;\' id=\'r2s1\'> &nbsp $799 <span style=\'font-size: 11px;\'> /annual ($66.58/month) </span> </div></div></div></div>  <div style=\'clear:both\'></div><div style=\'text-align:center;font-size:14px;margin-top: 15px;\'> <b> Please enter the first name, last name and and date of birth.</b> </div><div style=\'text-align:center;font-size:14px;margin-top: 15px;\'> <div style=\'float:left;padding:5px;\'> <input type=\'text\' placeholder=\' First Name\' name=\'fname\' id=\'fname\' onkeyup=\'butactive()\' style=\'background-color: #E6E6E6;width: 200px;height: 30px;border: 2px solid #CDCDCD;\'></div><div style=\'float:left;padding:5px;\'> <input type=\'text\' placeholder=\' Last Name\' name=\'lname\' id=\'lname\' onkeyup=\'butactive()\' style=\'background-color: #E6E6E6;width: 200px;height: 30px;border: 2px solid #CDCDCD;\'></div><div style=\'float:left;padding:5px;\'> <input type=\'text\' placeholder=\' DOB MM/YYYY\' name=\'dob\' onkeyup=\'butactive()\' id=\'dob\'  style=\'background-color: #E6E6E6;width: 200px;height: 30px;border: 2px solid #CDCDCD;\'></div></div><div style=\'clear:both\'></div><div style=\'text-align:center;font-size:14px;margin-top: 15px;\'><input type=\'button\' value=\'Submit\' disabled=\'disabled\' onclick=\'enableLavuToGoCheck()\' id=\'submit\'  style=\'background: #ccc;width: 149px;height: 42px;color: #fff;border: 1px solid #ccc;\'/> </div></form><div style=\'text-align:center;font-size:12px;color:#9C9C9C;margin-top: 15px;\'>By clicking Submit, you confirm that you accept the <a href=\'https://www.lavu.com/terms-of-service\' target=\'_blank\' style=\'    text-decoration: none;\'>Terms and Conditions </a> and that your membership will automatically </div><div style=\'text-align:center;font-size:12px;color:#9C9C9C;\'>renew on the basis of the plan you selected and your credit card will automatically be charged the applicable </div><div style=\'text-align:center;font-size:12px;color:#9C9C9C;\'>subscription fee. </div></div></td></tr></table>',
    						'extensions.ordering.lavutogo',
    						'5', '', '', 0, '', 0, '', 0, 'lavutogo', '')");
				}
				 
			}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('payment_status')) {
				if ($this->table('payment_status')->hasColumn('lavutogo_payment')) {
					$this->execute('ALTER TABLE `payment_status` DROP COLUMN `lavutogo_payment`');
				}
			}
			if ($this->hasTable('extensions') == true) {
				$lavutogoquery = $this->fetchRow("SELECT `id`, `title` FROM `extensions` WHERE `title`='Lavu To Go' ");
				if (isset($lavutogoquery['title'])) {
					$this->execute( "DELETE FROM `extensions` WHERE `title`='Lavu To Go' ");
				}
			}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
