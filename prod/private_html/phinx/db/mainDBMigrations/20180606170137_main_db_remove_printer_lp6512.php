<?php


use Phinx\Migration\AbstractMigration;

class MainDbRemovePrinterLp6512 extends AbstractMigration
{
    public $status;
    public $tblName='supported_printers';
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable($this->tblName)){
                $this->execute('DELETE FROM '.$this->tblName.' WHERE name IN ("Epson Thermal #2 (Dual)","Epson TM-T20 (Dual)") AND brand="Epson"');
                $this->execute('UPDATE '.$this->tblName.' SET name="Epson Impact Basic (Dual)" WHERE name="Epson Impact (Basic, Dual)" AND brand="Epson"');
                $this->execute('UPDATE '.$this->tblName.' SET name="Epson Thermal Basic (Dual)" WHERE name="Epson Thermal (Basic, Dual)" AND brand="Epson"');
            }    		/* Write migration code here */
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {

            if ($this->hasTable($this->tblName)){
                $this->execute("INSERT INTO ".$this->tblName." (`name`, `drawer_code`, `drawer_code_2`, `cutter_code`, `newline`, `job_start`, `job_end`, `tab`, `bold`, `underline`, `font_size`, `emphasize`, `standard_font`, `print_red`, `print_black`, `double_height`, `line_spacing`, `get_status`, `bit_graphics_8`, `bit_graphics_24`, `raster_start`, `raster_end`, `brand`, `asb`, `status_bytes`, `status_bits`, `offline_bit`, `overflow_bit`, `cover_open_bit`, `no_paper_bit`, `paper_low_bit`, `etb_byte`, `reset_etb`, `update_etb`, `_order`, `_deleted`) VALUES
('Epson Thermal #2 (Dual)', '27,112,48,40,120', '27,112,49,40,120', '10,10,10,10,10,29,86,1', '10', '', '', '9', '', '', '', '27,33,8', '27,33,0', '', '', '27,33,16', '27,51', '29,97,126', '', '', '', '', 'Epson', 1, 4, 16, -1, -1, -1, -1, 22, -1, '', '', 5, 0),
('Epson TM-T20 (Dual)', '27,112,48,40,120', '27,112,49,40,120', '29,86,1', '10', '', '10,10,10,10', '9', '', '', '', '27,33,8', '27,33,0', '', '', '27,33,16', '27,51', '29,97,126', '', '', '', '', 'Epson', 1, 4, 16, -1, -1, -1, -1, 22, -1, '', '', 6, 0)");

                $this->execute('UPDATE '.$this->tblName.' SET name="Epson Impact (Basic, Dual)" WHERE name="Epson Impact Basic (Dual)" AND brand="Epson"');
                $this->execute('UPDATE '.$this->tblName.' SET name="Epson Thermal (Basic, Dual)" WHERE name="Epson Thermal Basic (Dual)" AND brand="Epson"');
            }
    		/* Write rollback code here */
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
