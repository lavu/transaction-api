<?php

use Phinx\Migration\AbstractMigration;

class MainDbSuperGroupPaymentType extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable ( 'reports_v2' )) {
            $this->execute("UPDATE `reports_v2`  SET `select` = \"sister('#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales\')[_sep_]#sum_contents_quantity[_sep_]#order_count[_sep_]#gross_contents[_sep_]#sum_contents_discount[_sep_]#net_contents[_sep_]#order_tax[_sep_]#order_included_tax[_sep_]drill_down(\'@menu_category_name,@super_group,@contents_name,@order_server,@order_cashier,@rev_center,@order_register,@meal_period,@order_register,@order_tag,@order_id,@content_options,@closed_by_hour,@closed_by_day,@closed_by_weekday,@closed_by_week_for_year,@closed_by_month,@closed_by_year,@closed_actual_date_hour,@item_added_by_hour,@item_added_by_day,@item_added_by_weekday,@item_added_by_week_for_year,@item_added_by_month,@item_added_by_year,@item_added_actual_date_hour,@order_reg_group,@order_time,@combo_sales,@payment_type_name') as filters\" WHERE `id`=31");
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable ( 'reports_v2' )) {
            $this->execute("UPDATE `reports_v2`  SET `select` = \"sister('#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales\')[_sep_]#sum_contents_quantity[_sep_]#order_count[_sep_]#gross_contents[_sep_]#sum_contents_discount[_sep_]#net_contents[_sep_]#order_tax[_sep_]#order_included_tax[_sep_]drill_down(\'@menu_category_name,@super_group,@contents_name,@order_server,@order_cashier,@rev_center,@order_register,@meal_period,@order_register,@order_tag,@order_id,@content_options,@closed_by_hour,@closed_by_day,@closed_by_weekday,@closed_by_week_for_year,@closed_by_month,@closed_by_year,@closed_actual_date_hour,@item_added_by_hour,@item_added_by_day,@item_added_by_weekday,@item_added_by_week_for_year,@item_added_by_month,@item_added_by_year,@item_added_actual_date_hour,@order_reg_group,@order_time,@combo_sales') as filters\" WHERE `id`=31");
        }

    }
}
