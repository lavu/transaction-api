<?php


use Phinx\Migration\AbstractMigration;

class AddIndexResellerBillingLogLp11405 extends AbstractMigration
{
    public $status;
    public $indexes = array('resellername'=>'idx_resellername', 'datetime'=>'idx_datetime');
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reseller_billing_log') && $this->table('reseller_billing_log')->hasColumn('resellername') && $this->table('reseller_billing_log')->hasColumn('datetime')) {
                $rows = $this->fetchAll('SHOW INDEX FROM reseller_billing_log');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (!in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'ADD INDEX `' . $index . '` (`' . $column . '`)';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE reseller_billing_log ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reseller_billing_log') && $this->table('reseller_billing_log')->hasColumn('resellername') && $this->table('reseller_billing_log')->hasColumn('datetime')) {
                $rows = $this->fetchAll('SHOW INDEX FROM reseller_billing_log');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'DROP INDEX `' . $index . '`';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE reseller_billing_log ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
