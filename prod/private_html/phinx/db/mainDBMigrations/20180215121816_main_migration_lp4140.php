<?php


use Phinx\Migration\AbstractMigration;

class MainMigrationLp4140 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {   	
    	//DELETE FROM poslavu_MAIN_db.`reports_v2` WHERE title="Change Bank" AND name="changeBankReport";
    	if ($this->hasTable( 'reports_v2' )) {
    		$this->execute( "DELETE FROM `reports_v2` WHERE `title`IN ('Change Bank','Bank Deposit')");
    	
    		// To displaying change bank & Bank Deposit report tab in v2 reports:
    		$rows = [ [
    						'selection_column' => '','special_code' => '','selection' => 'Preset Date Ranges','table' => 'users','title' => 'Change Bank','name' => 'changeBankReport','orderby' => 'change_bank.created_date asc','select' => '#dateTime[_sep_]#changeBankAmount[_sep_]#firstName[_sep_]#lastName[_sep_]#userName','groupby' => '','where' => '#changeBankReportDates','join' => 'users.id=change_bank.user_id','_order' => NULL,'_deleted' => 0,'required_modules' => NULL,'custom_sister_list' => ''
    				],
    				[
    						'selection_column' => '','special_code' => '','selection' => 'Preset Date Ranges','table' => 'users','title' => 'Bank Deposit','name' => 'depositBankReport','orderby' => 'bank_deposit.created_date desc','select' => '#depositDateTime[_sep_]#depositAmount[_sep_]#depositFirstName[_sep_]#depositLastName[_sep_]#depositUserName[_sep_]#lavuCalculatedAmount','groupby' => '','where' => '#depositBankReportDates','join' => 'users.id=bank_deposit.user_id','_order' => NULL,'_deleted' => 0,'required_modules' => NULL,'custom_sister_list' => ''
    				]
    		];
    		$table = $this->table( 'reports_v2' );
    		$table->insert( $rows )->saveData();
    	}
    	// To displaying change bank report tab in V1 Reports:
    	if ($this->hasTable( 'reports' )) {
    		$this->execute( "DELETE FROM `reports` WHERE `title`IN ('Change Bank','Bank Deposit')");
    		$reportsRows = [    				
							['title' => 'Change Bank','tables' => '|change_bank|','dateid' => '','timeid' => '','groupid' => '','group_operation' => '','orderby' => 'change_bank.created_date asc','order_operation' => '','date_select_type' => 'Day Range','display' => '1','unified' => '','component_package' => 0,'locid' => '','report_type' => 'Regular','repeat_foreach' => '','categoryid' => '3','restrict' => 0,'extra_functions' => '','required_modules' => '','display_notes' => ''    						
    						],
    						[
    						'title' => 'Bank Deposit','tables' => '|bank_deposit|','dateid' => '','timeid' => '','groupid' => '','group_operation' => '','orderby' => 'bank_deposit.created_date','order_operation' => '','date_select_type' => 'Day Range','display' => '1','unified' => '','component_package' => 0,'locid' => '','report_type' => 'Regular','categoryid' => '3','restrict' => 0,'extra_functions' => '','required_modules' => '','display_notes' => ''
    						]
    					];

    		$reportTable = $this->table( 'reports' );
    		$reportTable->insert( $reportsRows )->saveData();
    	}

    		if ($this->hasTable( 'report_parts' )) {
    			//delete parts for Change Bank
    			$this->execute( "DELETE FROM `report_parts` WHERE `reportid`= (select id from `reports` where `title`='Change Bank')  ");
    			$changeBankId = $this->fetchAll( "select id from `reports` where `title`='Change Bank' ");    			
    			//insert data
    			$reportPartsRow = [
    								[
    								'reportid' => $changeBankId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'change_bank','fieldname' => 'created_date','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'Date/Time','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'center'
			    					],
	    							[
	    							'reportid' => $changeBankId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'users','fieldname' => 'f_name','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'First Name','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
	    							],
    								[
    								'reportid' => $changeBankId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'users','fieldname' => 'l_name','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'Last Name','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
    								],
    								[
    								'reportid' => $changeBankId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'users','fieldname' => 'username','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'Username','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
    								],
    								[
    								'reportid' => $changeBankId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'change_bank','fieldname' => 'amount','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'Change Bank Amount','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
	    							],
	    							[
	    							'reportid' => $changeBankId[0]['id'],'type' => 'join','prop1' => 'users','prop2' => 'id','prop3' => '','prop4' => '','tablename' => 'change_bank','fieldname' => 'user_id','operation' => '','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => '','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
	    							]
    						];
    			$reportPartsTable = $this->table( 'report_parts' );
    			$reportPartsTable->insert($reportPartsRow)->saveData();    			
    			
    			//delete parts for Bank Deposit
    			$this->execute( "DELETE FROM `report_parts` WHERE `reportid`= (select id from `reports` where `title`='Bank Deposit')  ");
    			$bankDepositId = $this->fetchAll( "select id from `reports` where `title`='Bank Deposit' ");
    			//insert data
    			$reportPartsRow2 = [
			    					[
			    							'reportid' => $bankDepositId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'bank_deposit','fieldname' => 'created_date','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171115135011','label' => 'Date/Time','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'center'
			    					],
			    					[
			    							'reportid' => $bankDepositId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'bank_deposit','fieldname' => 'amount','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'Entered Deposit Amount','branch' => 1,'_order' => 0,'id' => 0,'monetary' => 0,'align' => 'left'
			    					],
			    					[
			    							'reportid' => $bankDepositId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'users','fieldname' => 'f_name','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'First Name','branch' => 1,'_order' => 0,'id' => 0,'monetary' => 0,'align' => 'left'
			    					],
			    					[
			    							'reportid' => $bankDepositId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'users','fieldname' => 'l_name','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'Last Name','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
			    					],
			    					[
			    							'reportid' => $bankDepositId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'users','fieldname' => 'username','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'Username','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
			    					],
			    					[
			    							'reportid' => $bankDepositId[0]['id'],'type' => 'select','prop1' => '','prop2' => '','prop3' => '','prop4' => '','tablename' => 'bank_deposit','fieldname' => 'required_deposit','operation' => 'custom','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => 'Lavu Calculated Deposit Amount','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
			    					],
			    					[
			    							'reportid' => $bankDepositId[0]['id'],'type' => 'join','prop1' => 'users','prop2' => 'id','prop3' => '','prop4' => '','tablename' => 'bank_deposit','fieldname' => 'user_id','operation' => '','opargs' => '','filter' => '','hide' => 0,'sum' => 0,'graph' => 0,'updatets' => '20171112135011','label' => '','branch' => 1,'_order' => 0,'id' => '0','monetary' => 0,'align' => 'left'
			    					]
    			];
    			$reportPartsTable = $this->table( 'report_parts' );
    			$reportPartsTable->insert($reportPartsRow2)->saveData();    			    			    			
    		}    		
    }    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	if ($this->hasTable( 'report_parts' )) {    		
    		//delete parts for Bank Deposit
    		$this->execute( "DELETE FROM `report_parts` WHERE `reportid`= (select id from `reports` where `title`='Bank Deposit')  ");
    		//delete from reports_parts for Change Bank
    		$this->execute( "DELETE FROM `report_parts` WHERE `reportid`= (select id from `reports` where `title`='Change Bank')  ");
    	}
    	// remove records
    	if ($this->hasTable( 'reports' )) {
    		$this->execute( "DELETE FROM `reports` WHERE `title`IN ('Change Bank','Bank Deposit')");
    	}
    }
}
