<?php

use Phinx\Migration\AbstractMigration;

class MainDbActiveCustomersReportLp11118 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
      $this->status = true;
      try {
        /**
             * Purpose:- We will store active customers historical data.
             */
             if (!$this->hasTable('active_customer_details')) {
                $this->execute("
                CREATE TABLE `active_customer_details` (
                  `restaurant_id` int(11) NOT NULL UNIQUE,
                  `location_id` int(11) NOT NULL,
                  `data_name` varchar(100) NOT NULL,
                  `data` text NOT NULL COMMENT 'active customer historical data',
                  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                   KEY `active_customer_idx` (`restaurant_id`, `data_name`)
                ) ENGINE = InnoDB CHARSET = utf8
            ");
            }
      }
      catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
      $this->status = true;
      try {
        if ($this->hasTable('active_customer_details')) {
                $this->execute('DROP TABLE `active_customer_details`');
            }
      }
      catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}