<?php


use Phinx\Migration\AbstractMigration;

class CreateFieldSourceTypes extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            $this->execute("
                CREATE TABLE IF NOT EXISTS field_source_types (
                    id INT(11) unsigned NOT NULL UNIQUE AUTO_INCREMENT,
                    name VARCHAR(100) NOT NULL UNIQUE,
                    code VARCHAR(5) NOT NULL UNIQUE,
                    status char(1) NOT NULL DEFAULT 'A',

                    created_at datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    updated_at datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

                    PRIMARY KEY (id)

                ) ENGINE=InnoDB;
            ");
    	} catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$this->execute("DROP TABLE IF EXISTS field_source_types;");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
