<?php


use Phinx\Migration\AbstractMigration;

class MaindbCompeatIntegrationLp8896 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		/* Write migration code here */
            $result=$this->fetchAll('SELECT dataname FROM auth_tokens WHERE dataname="noble__co_" AND token_type="ftp_compeat"');
            $dataname=$result[0]['dataname'];
            if (!$dataname) {
                $this->execute('INSERT INTO auth_tokens (dataname, token, token_type) VALUES ("noble__co_","{\"username\":\"JohnnysDiner_FTP\",\"password\":\"johNd1n84&8*13\",\"company_id\":\"3106\",\"location_id\":\"0410\"}","ftp_compeat")');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            /* Write rollback code here */
            $this->execute('DELETE FROM auth_tokens WHERE dataname="noble__co_" AND token_type="ftp_compeat"');
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
