<?php


use Phinx\Migration\AbstractMigration;

class AddIndexChainReportingGroupsLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('chain_reporting_groups') && $this->table('chain_reporting_groups')->hasColumn('chainid')) {
                $row = $this->fetchRow('SHOW INDEX FROM chain_reporting_groups WHERE Key_name = "idx_chainid"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE chain_reporting_groups ADD INDEX `idx_chainid` (chainid)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('chain_reporting_groups') && $this->table('chain_reporting_groups')->hasColumn('chainid')) {
                $row = $this->fetchRow('SHOW INDEX FROM chain_reporting_groups WHERE Key_name = "idx_chainid"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE chain_reporting_groups DROP INDEX `idx_chainid`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
