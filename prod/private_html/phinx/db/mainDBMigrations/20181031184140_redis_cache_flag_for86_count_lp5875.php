<?php


use Phinx\Migration\AbstractMigration;

class RedisCacheFlagFor86CountLp5875 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
		    /* Write migration code here */
		    if ($this->hasTable('main_config')) {
			    $this->execute("
                    INSERT INTO `main_config` (`id`, `setting`, `value`, `_deleted`)
                    VALUES
                        (null, 'ENABLED_REDIS', '1', '0')
                ");
		    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
		    /* Write rollback code here */
		    if ($this->hasTable('main_config')) {
			    $this->execute("
                    DELETE FROM `main_config` WHERE `setting` = 'ENABLED_REDIS'
                ");
		    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
