<?php


use Phinx\Migration\AbstractMigration;

class AddIndexEmployeeTimeOffLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('employeeTimeOff') && $this->table('employeeTimeOff')->hasColumn('username')) {
                $row = $this->fetchRow('SHOW INDEX FROM employeeTimeOff WHERE Key_name = "idx_username"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE employeeTimeOff ADD INDEX `idx_username` (`username`)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('employeeTimeOff') && $this->table('employeeTimeOff')->hasColumn('username')) {
                $row = $this->fetchRow('SHOW INDEX FROM employeeTimeOff WHERE Key_name = "idx_username"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE employeeTimeOff DROP INDEX `idx_username`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
