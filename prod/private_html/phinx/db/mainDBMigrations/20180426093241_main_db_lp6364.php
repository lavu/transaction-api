<?php
use Phinx\Migration\AbstractMigration;

class MainDbLp6364 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {

        $curTime=time();

        $propsAutoInc=array('signed' => false, 'limit' =>11, 'identity' => true);
        $propsInt=array('limit' => 11, 'null' => false, 'default' => 0);
        $propsUnsignedInt=array('signed' => false, 'limit' => 11, 'null' => false, 'default' => 0);
        $propsString=array('limit' => 255, 'null' => false, 'default' => '');
        $propsDT=array('default' => 'CURRENT_TIMESTAMP');
        $propsTinyInt=array('limit' => 1, 'null' => false, 'default'=>0);
        $propsText=array('null' => false, 'default'=>'');
        $propsFK=array('delete' => 'NO_ACTION', 'update' => 'NO ACTION');

        $tblName='fiscal_receipt_sections';
        if(!$this->hasTable($tblName)){
            $table = $this->table($tblName, ['id' => false, 'primary_key' => 'fiscal_receipt_section_id']);
            $table
                ->addColumn('fiscal_receipt_section_id', 'integer', $propsAutoInc)
                ->addColumn('title','string',$propsString)
                ->addColumn('created_date', 'datetime', $propsDT)
                ->addColumn('updated_date', 'datetime', ['null' => false])
                ->addColumn('created_by', 'integer', $propsInt)
                ->addColumn('updated_by', 'integer', $propsInt)
                ->addColumn('_deleted', 'integer', $propsTinyInt)
                ->create();
	    $this->execute("INSERT INTO `fiscal_receipt_sections` (`title`, `created_date`, `updated_date`, `created_by`, `updated_by`, `_deleted`) VALUES
('Header','".$curTime."' , '0000-00-00 00:00:00', 0, 0, '0'),
('Footer', '".$curTime."', '0000-00-00 00:00:00', 0, 0, '0'),
('Location Information', '".$curTime."', '0000-00-00 00:00:00', 0, 0, '0'),
('General Order Information', '".$curTime."', '0000-00-00 00:00:00', 0, 0, '0'),
('Customer Information', '".$curTime."', '0000-00-00 00:00:00', 0, 0, '0'),
('Order Information', '".$curTime."', '0000-00-00 00:00:00', 0, 0, '0');
");
        }

        $tblName='fiscal_receipt_tpl_sections';
        if(!$this->hasTable($tblName)){
            $table = $this->table($tblName, ['id' => false, 'primary_key' => 'fiscal_receipt_tpl_section_id']);
            $table
                ->addColumn('fiscal_receipt_tpl_section_id', 'integer', $propsAutoInc)
                ->addColumn('fiscal_receipt_tpl_id','integer',$propsInt)
                ->addColumn('fiscal_receipt_section_id','integer',$propsInt)
                ->addColumn('allow_edit','integer',$propsTinyInt)
                ->addColumn('seq','integer',$propsInt)
                ->addColumn('created_date', 'datetime', $propsDT)
                ->addColumn('updated_date', 'datetime', ['null' => false])
                ->addColumn('created_by', 'integer', $propsInt)
                ->addColumn('updated_by', 'integer', $propsInt)
                ->addColumn('_deleted', 'integer', $propsTinyInt)
                ->create();
        }

        $tblName='fiscal_receipt_tpl_region';
        if(!$this->hasTable($tblName)){
            $table = $this->table($tblName, ['id' => false, 'primary_key' => 'fiscal_receipt_tpl_region_id']);
            $table
                ->addColumn('fiscal_receipt_tpl_region_id', 'integer', $propsAutoInc)
                ->addColumn('region', 'string',$propsString)
                ->addColumn('country', 'string', $propsString)
                ->addColumn('country_id', 'integer', $propsInt)
                ->addColumn('created_date', 'datetime', $propsDT)
                ->addColumn('updated_date', 'datetime', ['null' => false])
                ->addColumn('created_by', 'integer', $propsInt)
                ->addColumn('updated_by', 'integer', $propsInt)
                ->addColumn('_deleted', 'integer', $propsTinyInt)
                ->addIndex(['region'],['name' => 'idx_region'])
                ->create();
    
	    $this->execute("INSERT INTO `fiscal_receipt_tpl_region` (`fiscal_receipt_tpl_region_id`, `region`, `country`, `country_id`, `created_date`, `updated_date`, `created_by`, `updated_by`, `_deleted`) VALUES
(1, 'Colombia', 'Colombia', 0, '".$curTime."', '".$curTime."', 0, 0, '0'),
(2, 'Paraguay', 'Paraguay', 0, '".$curTime."', '".$curTime."', 0, 0, '0')");
        }
        
        $tblName='fiscal_receipt_tpl';
        if(!$this->hasTable($tblName)){
        	$table = $this->table($tblName, ['id' => false, 'primary_key' => 'fiscal_receipt_tpl_id']);
        	$table
        	->addColumn('fiscal_receipt_tpl_id', 'integer', $propsAutoInc)
        	->addColumn('title', 'string',$propsString)
        	->addColumn('fiscal_receipt_tpl_region_id', 'integer', $propsInt)
        	->addColumn('format','text', $propsText)
        	->addColumn('created_date', 'datetime', $propsDT)
        	->addColumn('updated_date', 'datetime', ['null' => false])
        	->addColumn('created_by', 'integer', $propsInt)
        	->addColumn('updated_by', 'integer', $propsInt)
        	->addColumn('_deleted', 'integer', $propsTinyInt)
        	->create();
       
	$this->execute("INSERT INTO `fiscal_receipt_tpl` (`fiscal_receipt_tpl_id`, `title`, `fiscal_receipt_tpl_region_id`, `format`, `created_date`, `updated_date`, `created_by`, `updated_by`, `_deleted`) VALUES
(1, 'Colombia', 1, '', '".$curTime."', NULL, NULL, NULL, '0'),
(2, 'Paraguay', 2, '', '".$curTime."', NULL, NULL, NULL, '0')
");

 
        }

        $tblName='fiscal_receipt_fields';
        if(!$this->hasTable($tblName)){
            $table = $this->table($tblName, ['id' => false, 'primary_key' => 'fiscal_receipt_field_id']);
            $table
                ->addColumn('fiscal_receipt_field_id', 'integer', $propsAutoInc)
                ->addColumn('title', 'string',$propsString)
                ->addColumn('label','string',$propsString)
                ->addColumn('description','string', $propsString)
                ->addColumn('type','enum',['values' => ['field', 'section', 'div', 'system', 'img'], 'default' => 'field'])
                ->addColumn('format','text', $propsText)
                ->addColumn('status','integer', $propsTinyInt)
                ->addColumn('created_date', 'datetime', $propsDT)
                ->addColumn('updated_date', 'datetime', ['null' => false])
                ->addColumn('created_by', 'integer', $propsInt)
                ->addColumn('updated_by', 'integer', $propsInt)
                ->addColumn('_deleted', 'integer', $propsTinyInt)
                ->addIndex(['title'],['name' => 'idx_title'])
                ->create();

            $this->execute("INSERT INTO ".$tblName." (`title`, `label`, `description`, `type`, `format`, `status`, `created_date`, `updated_date`, `created_by`, `updated_by`, `_deleted`) VALUES 
                ('company_name', 'Company Name', '', 'system', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:00', '2018-04-20 11:57:00', 0, 0, '0'),
                ('company_type', 'Company Type', '', 'system', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:00', '2018-04-20 11:57:00', 0, 0, '0'),                
                ('text_input', 'Text Input', '', 'field', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:00', '2018-04-20 11:57:00', 0, 0, '0'),                
                ('logo', 'Logo', '', 'img', '{\"type\":\"img\",\"size\":\"\"}', '1', '2018-04-20 11:57:02', '2018-04-20 11:57:02', 0, 0, '0'),
                ('order_tag', 'Order Tag', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:02', '2018-04-20 11:57:02', 0, 0, '0'),
                ('order_id', 'Order ID', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:02', '2018-04-20 11:57:02', 0, 0, '0'),
                ('check', 'Check No', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:02', '2018-04-20 11:57:02', 0, 0, '0'),
                ('table_name', 'Table Name', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:03', '2018-04-20 11:57:03', 0, 0, '0'),
                ('guest_count', 'Guest Count', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:03', '2018-04-20 11:57:03', 0, 0, '0'),
                ('server_name', 'Server Name', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:04', '2018-04-20 11:57:04', 0, 0, '0'),
                ('cashier_name', 'Cashier Name', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:04', '2018-04-20 11:57:04', 0, 0, '0'),
                ('register', 'Register', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:04', '2018-04-20 11:57:04', 0, 0, '0'),
                ('time_stamp', 'Timestamp', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:05', '2018-04-20 11:57:05', 0, 0, '0'),
                ('customer_info', 'Customer Info', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:05', '2018-04-20 11:57:05', 0, 0, '0'),
                ('order_tag', 'Order Tag', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:05', '2018-04-20 11:57:05', 0, 0, '0'),
                ('items_totals_payments', 'Order Info', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:06', '2018-04-20 11:57:06', 0, 0, '0'),
                ('location_name', 'Location Name', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:06', '2018-04-20 11:57:06', 0, 0, '0'),
                ('country', 'Country', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:07', '2018-04-20 11:57:07', 0, 0, '0'),
                ('telephone', 'Phone', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:07', '2018-04-20 11:57:07', 0, 0, '0'),
                ('email', 'Email', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:07', '2018-04-20 11:57:07', 0, 0, '0'),
                ('website', 'Website', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:08', '2018-04-20 11:57:08', 0, 0, '0'),
                ('manager', 'Manager', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:08', '2018-04-20 11:57:08', 0, 0, '0'),
                ('serial_no', 'Serial No', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:08', '2018-04-20 11:57:08', 0, 0, '0'),
                ('loyaltree_qr_code', 'LoyalTree QR Code', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:09', '2018-04-20 11:57:09', 0, 0, '0'),
                ('special_query_results', 'Special Query Results', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:09', '2018-04-20 11:57:09', 0, 0, '0'),
                ('check_gratuity_lines', 'Check Gratuity Lines', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:09', '2018-04-20 11:57:09', 0, 0, '0'),
                ('tax_id', 'Tax ID', '', 'field', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:10', '2018-04-20 11:57:10', 0, 0, '0'),
                ('company_address', 'Company Address', '', 'field', '{\"type\":\"textarea\",\"size\":\"5-40\"}', '1', '2018-04-20 11:57:10', '2018-04-20 11:57:10', 0, 0, '0'),
                ('address', 'Address', '', 'field', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:10', '2018-04-20 11:57:10', 0, 0, '0'),
                ('manager', 'Manager', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:11', '2018-04-20 11:57:11', 0, 0, '0'),
                ('city', 'City', '', 'field', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:11', '2018-04-20 11:57:11', 0, 0, '0'),
                ('state', 'State', '', 'system', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:11', '2018-04-20 11:57:11', 0, 0, '0'),
                ('invoice_number', 'Invoice Number', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:13', '2018-04-20 11:57:13', 0, 0, '0'),
                ('order_date', 'Order Date', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:13', '2018-04-20 11:57:13', 0, 0, '0'),
                ('order_timestamp', 'Order Timestamp', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:13', '2018-04-20 11:57:13', 0, 0, '0'),
                ('receipt_copy_original', 'Receipt Copy Original', '', 'field', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:13', '2018-04-20 11:57:13', 0, 0, '0'),
                ('receipt_copy_duplicate', 'Receipt Copy Duplicate', '', 'field', '{\"type\":\"text\",\"size\":\"25\"}', '1', '2018-04-20 11:57:13', '2018-04-20 11:57:13', 0, 0, '0'),
                ('lavu_footer', 'Footer', '', 'system', '{\"type\":\"\",\"size\":\"\"}', '1', '2018-04-20 11:57:13', '2018-04-20 11:57:13', 0, 0, '0'),
                ('div', '', '', 'div', '{\"type\":\"div\",\"size\":\"\"}', '1', '2018-04-20 13:58:29', '2018-04-20 13:58:29', 0, 0, '0')"
            );


        }

        $tblName='fiscal_receipt_tpl_fields';
        if(!$this->hasTable($tblName) && $this->hasTable('fiscal_receipt_tpl') && $this->hasTable('fiscal_receipt_sections')){
            $table = $this->table($tblName, ['id' => false, 'primary_key' => 'fiscal_receipt_tpl_field_id']);
            $table
                ->addColumn('fiscal_receipt_tpl_field_id', 'integer', $propsAutoInc)
                ->addColumn('fiscal_receipt_field_id', 'integer', $propsUnsignedInt)
                ->addColumn('fiscal_receipt_tpl_id', 'integer', $propsUnsignedInt)
                ->addColumn('label','string',$propsString)
                ->addColumn('display', 'integer', $propsTinyInt)
                ->addColumn('allow_hide', 'integer', $propsTinyInt)
                ->addColumn('seq', 'integer', $propsInt)
                ->addColumn('required', 'integer', $propsTinyInt)
                ->addColumn('format','text', $propsText)
                ->addColumn('show_label', 'integer', $propsTinyInt)
                ->addColumn('type','enum',['values' => ['field', 'section', 'div', 'system', 'img'], 'default' => 'field'])
                ->addColumn('macros','string',$propsString)
                ->addColumn('fiscal_receipt_section_id', 'integer', $propsInt)
                ->addColumn('created_date', 'datetime', $propsDT)
                ->addColumn('updated_date', 'datetime', ['null' => false])
                ->addColumn('created_by', 'integer', $propsInt)
                ->addColumn('updated_by', 'integer', $propsInt)
                ->addColumn('_deleted', 'integer', $propsTinyInt)
                ->create();


    		$this->execute('ALTER TABLE `fiscal_receipt_tpl_fields` ADD CONSTRAINT `fk_fiscal_receipt_field_id` FOREIGN KEY (`fiscal_receipt_field_id`) REFERENCES `fiscal_receipt_fields`(`fiscal_receipt_field_id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
    		$this->execute('ALTER TABLE `fiscal_receipt_tpl_fields` ADD CONSTRAINT `fk_fiscal_receipt_tpl_id` FOREIGN KEY (`fiscal_receipt_tpl_id`) REFERENCES `fiscal_receipt_tpl`(`fiscal_receipt_tpl_id`) ON DELETE RESTRICT ON UPDATE RESTRICT');

        }

    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {

        $this->execute('SET FOREIGN_KEY_CHECKS=0');

        if($this->hasTable('fiscal_receipt_tpl_fields')){
            $this->execute('DROP TABLE fiscal_receipt_tpl_fields');
        }

        if($this->hasTable('fiscal_receipt_tpl_sections')){
            $this->execute('DROP TABLE fiscal_receipt_tpl_sections');
        }

        if($this->hasTable('fiscal_receipt_fields')){
            $this->execute('DROP TABLE fiscal_receipt_fields');
        }

        if($this->hasTable('fiscal_receipt_tpl')){
            $this->execute('DROP TABLE fiscal_receipt_tpl');
        }

        if($this->hasTable('fiscal_receipt_sections')){
            $this->execute('DROP TABLE fiscal_receipt_sections');
        }
        
        if($this->hasTable('fiscal_receipt_tpl_region')){
        	$this->execute('DROP TABLE fiscal_receipt_tpl_region');
        }

        $this->execute('SET FOREIGN_KEY_CHECKS=1');

    }
}



