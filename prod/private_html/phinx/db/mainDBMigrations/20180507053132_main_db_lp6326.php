<?php


use Phinx\Migration\AbstractMigration;

class MainDbLp6326 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {

	if($this->hasTable('extensions')){
		$this->execute('UPDATE extensions SET short_description=\'Sacoa is a fully integrated payment method now available for Lavu POS. <br/><br/> Sacoa is a worldwide supplier of revenue management systems for the amusement, entertainment, and leisure industries. Lavu\\\'s integration with Sacoa involves the Sacoa PlayCard, which is a debit card system for tokens and redemptions. <br/><br/>With Sacoa and Lavu, you can better manage all revenue streams in your business, accept Sacoa as a method of payment in Lavu, and use the PlayCards as a debit card through out your location. This even includes earning points at various games or attractions to redeem for prizes.<br/><br/><div style="position:relative">  <div style="position:absolute; width:350px; top:5px; left:50%; margin-left:-175px">   <label for="sacoa_username" style="width: 65px;display: inline-block;">Username</label><input id="sacoa_username" type="text" style="width:75%"> </div> <div></div><div style="position:absolute; width:350px; top:30px; left:50%; margin-left:-175px"><label for="sacoa_password" style="width: 65px;display: inline-block;">Password</label><input id="sacoa_password" type="password" style="width:75%"><label for="sacoa_url" style="width: 65px;display: inline-block;">Service URL</label><input id="sacoa_url" type="text" style="width:75%"></div> </div><br/><br/><br/><br/><br/><br/>\' WHERE title="Sacoa Playcard"');
	}

    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {

	if($this->hasTable('extensions')){
		$this->execute('UPDATE extensions SET short_description=\'Sacoa is a fully integrated payment method now available for Lavu POS. <br/><br/> Sacoa is a worldwide supplier of revenue management systems for the amusement, entertainment, and leisure industries. Lavu\\\'s integration with Sacoa involves the Sacoa PlayCard, which is a debit card system for tokens and redemptions. <br/><br/>With Sacoa and Lavu, you can better manage all revenue streams in your business, accept Sacoa as a method of payment in Lavu, and use the PlayCards as a debit card through out your location. This even includes earning points at various games or attractions to redeem for prizes.<br/><br/><div style="position:relative">  <div style="position:absolute; width:350px; top:5px; left:50%; margin-left:-175px">   <label for="sacoa_username" style="width: 65px;display: inline-block;">Username</label><input id="sacoa_username" type="text" style="width:75%"> </div> <div></div><div style="position:absolute; width:350px; top:30px; left:50%; margin-left:-175px"><label for="sacoa_password" style="width: 65px;display: inline-block;">Password</label><input id="sacoa_password" type="password" style="width:75%"></div></div><br/><br/><br/>\' WHERE title="Sacoa Playcard"');
	}

    }
}
