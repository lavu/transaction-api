<?php


use Phinx\Migration\AbstractMigration;

class MainDbLp5081 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
	    if ($this->hasTable ( 'language_dictionary' )) {
		    // INSERT INTO [POSLAVU MAIN DB].`language_dictionary`
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DEFAULT_1"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_1"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_2"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_3"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_4"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_5"' );

		    $datetime = date('Y-m-d H:i:s');
		    // inserting multiple row
		    $multipleRow = [
			    [
				    'pack_id' => '1',
				    'word_or_phrase' => 'Message does not exist for tag',
				    'english_id' => '',
				    'used_by' => '',
				    'created_date' => $datetime,
				    'modified_date' => $datetime,
				    'tag' => 'ERR_DEFAULT_1'
			    ],
			    [
				    'pack_id' => '1',
				    'word_or_phrase' => 'Assign a server to Cash Drawer',
				    'english_id' => '',
				    'used_by' => 'app',
				    'created_date' => $datetime,
				    'modified_date' => $datetime,
				    'tag' => 'ERR_DCD_1'
			    ],
			    [
				    'pack_id' => '1',
				    'word_or_phrase' => 'Till Out permission denied {1} User assigned to this cash drawer will be able to till out',
				    'english_id' => '',
				    'used_by' => 'app',
				    'created_date' => $datetime,
				    'modified_date' => $datetime,
				    'tag' => 'ERR_DCD_2'
			    ],
			    [
				    'pack_id' => '1',
				    'word_or_phrase' => 'Please assign a server to cash drawer',
				    'english_id' => '',
				    'used_by' => 'app',
				    'created_date' => $datetime,
				    'modified_date' => $datetime,
				    'tag' => 'ERR_DCD_3'
			    ],
			    [
				    'pack_id' => '1',
				    'word_or_phrase' => 'Please select a cash drawer',
				    'english_id' => '',
				    'used_by' => 'app',
				    'created_date' => $datetime,
				    'modified_date' => $datetime,
				    'tag' => 'ERR_DCD_4'
			    ],
			    [
				    'pack_id' => '1',
				    'word_or_phrase' => 'Assign a different user {1} {2} is assigned to cash drawer {3}',
				    'english_id' => '',
				    'used_by' => 'app',
				    'created_date' => $datetime,
				    'modified_date' => $datetime,
				    'tag' => 'ERR_DCD_5'
			    ]
		    ];
		    $table = $this->table ( 'language_dictionary' );
		    $table->insert ( $multipleRow )->saveData ();
	    }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
	    if ($this->hasTable ( 'language_dictionary' )) {
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DEFAULT_1"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_1"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_2"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_3"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_4"' );
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_5"' );
	    }
    }
}
