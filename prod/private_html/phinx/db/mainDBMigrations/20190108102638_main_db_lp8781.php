<?php


use Phinx\Migration\AbstractMigration;

class MainDbLp8781 extends AbstractMigration
{
	public $status;

	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			$englishWord1 = "Provisional receipt - NOT A PURCHASE RECEIPT";
			$englishWord2 = "Order cannot be closed because the receipt was unable to be printed. Once a receipt has been printed for this order, the order will close.";

			$translationWord1 = "ForelÃ¸pig kvittering - IKKJE KVITTERING FOR KJÃ˜P";
			$translationWord2 = "Bestillingen kan ikke lukkes fordi kvitteringen ikke kunne skrives ut. Nar en kvittering er skrevet ut for denne bestillingen, lukkes bestillingen.";

			$getEngId = $this->fetchRow('SELECT `id` FROM `language_packs` WHERE `language` = "English" AND `english_name` = "English" AND `_deleted` = 0');
			if (isset($getEngId['id'])) {
				$this->execute("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `translation`, `_deleted`, `english_id`, `used_by`, `created_date`, `modified_date`, `backtrace`, `tag`) VALUES ('".$getEngId['id']."', '".$englishWord1."', '', 0, 0 , '', '', '', '', ''), ('".$getEngId['id']."', '".$englishWord2."', '', 0, 0 ,'', '', '', '', '')");
				$getNorwId = $this->fetchRow('SELECT `id` FROM `language_packs` WHERE `language` = "Norsk" AND `english_name` = "Norwegian" AND `_deleted` = 0');
				if (isset($getNorwId['id'])) {
					$getLastEngIdWord1 = $this->fetchRow("SELECT `id` FROM language_dictionary WHERE `pack_id` = '".$getEngId['id']."' AND `word_or_phrase` = '".$englishWord1."'");
					$getLastEngIdWord2 = $this->fetchRow("SELECT `id` FROM language_dictionary WHERE `pack_id` = '".$getEngId['id']."' AND `word_or_phrase` = '".$englishWord2."'");

					if (isset($getLastEngIdWord1) && isset($getLastEngIdWord2)) {
						$this->execute("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `translation`, `_deleted`, `english_id`, `used_by`, `created_date`, `modified_date`, `backtrace`, `tag`) VALUES ('".$getNorwId['id']."', '".$englishWord1."', '".$translationWord1."', 0, '".$getLastEngIdWord1['id']."' ,'', '', '', '', ''), ('".$getNorwId['id']."', '".$englishWord2."', '".$translationWord2."', 0, '".$getLastEngIdWord2['id']."' ,'', '', '', '', '')");

						$this->execute("UPDATE `language_dictionary` SET `english_id` = '".$getLastEngIdWord1['id']."' WHERE `word_or_phrase` = '".$englishWord1."'");
						$this->execute("UPDATE `language_dictionary` SET `english_id` = '".$getLastEngIdWord2['id']."' WHERE `word_or_phrase` = '".$englishWord2."'");
					}
				}
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			$englishWord1 = "Provisional receipt - NOT A PURCHASE RECEIPT";
			$englishWord2 = "Order cannot be closed because the receipt was unable to be printed. Once a receipt has been printed for this order, the order will close.";

			$getEngId = $this->fetchRow('SELECT `id` FROM `language_packs` WHERE `language` = "English" AND `english_name` = "English" AND `_deleted` = 0');
			if (isset($getEngId['id'])) {
				$this->execute("DELETE FROM `language_dictionary` WHERE `pack_id` = '".$getEngId['id']."' AND `word_or_phrase` IN('".$englishWord1."', '".$englishWord2."')");
			}
			$getNorwId = $this->fetchRow('SELECT `id` FROM `language_packs` WHERE `language` = "Norsk" AND `english_name` = "Norwegian" AND `_deleted` = 0');
			if (isset($getNorwId['id'])) {
				$this->execute("DELETE FROM `language_dictionary` WHERE `pack_id` = '".$getNorwId['id']."' AND `word_or_phrase` IN('".$englishWord1."', '".$englishWord2."')");
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
