<?php


use Phinx\Migration\AbstractMigration;

class MainDbKiosk extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if (!$this->hasTable('menu_meta_data')) {
            $this->execute("
				CREATE TABLE `menu_meta_data` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `code` varchar(50) NOT NULL,
				  `name` varchar(50) NOT NULL,
				  `description` varchar(50) NOT NULL,
				  `icon_name` varchar(50) NOT NULL,
				  PRIMARY KEY (`id`)
				)
            ");
        }

        if ($this->hasTable('menu_meta_data')) {
            $rows = $this->fetchAll("SELECT * FROM `menu_meta_data` WHERE `id` IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14)");
            if ($rows == false) {
                $this->execute("
                    INSERT INTO `menu_meta_data` (`id`, `code`, `name`, `description`, `icon_name`)
                    VALUES
                        (1, 'dairy', 'Contains Dairy', 'Item contains dairy', 'dairy'),
                        (2, 'no_dairy', 'Dairy Free', 'Item does not contain dairy', 'noDairy'),
                        (3, 'egg', 'Egg', 'Item contains egg', 'egg'),
                        (4, 'no_egg', 'No Egg', 'Item does not contain egg', 'noEgg'),
                        (5, 'gluten', 'Gluten', 'Item contains gluten', 'gluten'),
                        (6, 'gluten_free', 'Gluten Free', 'Item does not contain gluten', 'glutenFree'),
                        (7, 'shellfish', 'Shellfish', 'Item contains shellfish', 'shellfish'),
                        (8, 'no_shellfish', 'No Shellfish', 'Item does not contain shellfish', 'noShellfish'),
                        (9, 'soy', 'Soy', 'Item contains soy', 'soy'),
                        (10, 'no_soy', 'No Soy', 'Item does not contain soy', 'noSoy'),
                        (11, 'nuts', 'Nuts', 'Item contains nuts', 'nuts'),
                        (12, 'nut_free', 'Nut Free', 'Item does not contain nuts', 'nutFree'),
                        (13, 'vegan', 'Vegan', 'Item is vegan', 'vegan'),
                        (14, 'heart', 'Heart Healthy', 'Item is heart healthy', 'heart')
                ");
            }
        }

        if (!$this->hasTable('standard_nutrition_types')) {
            $this->execute("
				CREATE TABLE `standard_nutrition_types` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `code` varchar(50) NOT NULL,
				  `label` varchar(255) NOT NULL,
				  `units` varchar(255) NOT NULL,
				  PRIMARY KEY (`id`)
				)
            ");
        }

        if ($this->hasTable('standard_nutrition_types')) {
            $rows = $this->fetchAll("SELECT * FROM `standard_nutrition_types` WHERE `id` IN (1,2,3,4,5,6,7,8,9,10,11)");
            if ($rows == false) {
                $this->execute("
                    INSERT INTO `standard_nutrition_types` (`id`, `code`, `label`, `units`)
                    VALUES
                        (1, 'serving_size', 'Serving Size', 'g|mg'),
                        (2, 'total_fat', 'Total Fat', 'g|mg'),
                        (3, 'saturated_fat', 'Saturated Fat', 'g|mg'),
                        (4, 'trans_fat', 'Trans Fat', 'g|mg'),
                        (5, 'sodium', 'Sodium', 'g|mg'),
                        (6, 'dietary_fiber', 'Dietary Fiber', 'g|mg'),
                        (7, 'total_sugar', 'Total Sugar', 'g|mg'),
                        (8, 'protein', 'Protein', 'g|mg'),
                        (9, 'carbohydrates', 'Carbohydrates', 'g|mg'),
                        (10, 'calories', 'Calories', 'g|mg'),
                        (11, 'calories_from_fat', 'Calories from Fat', 'g|mg')
                ");
            }
        }

        if ($this->hasTable('payment_status')) {
            if (!$this->table('payment_status')->hasColumn('custom_max_kiosk')) {
                $this->execute("ALTER TABLE `payment_status` ADD COLUMN `custom_max_kiosk` int(11) NOT NULL AFTER `custom_max_kds`");
            }
        }

        if ($this->hasTable('printer_models')) {
            if (!$this->table('printer_models')->hasColumn('printer_series')) {
                $this->execute("ALTER TABLE `printer_models` ADD COLUMN `printer_series` varchar(255) NOT NULL AFTER `model`");
            }
        }

        if ($this->hasTable('printer_models')) {
            if ($this->table('printer_models')->hasColumn('printer_series')) {
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_P60' WHERE `model` = 'TM-P60'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_P60II' WHERE `model` = 'TM-P60II'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_P80' WHERE `model` = 'TM-P80'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_T20' WHERE `model` = 'TM-T20' OR `model` = 'TM-T20II'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_T70' WHERE `model` = 'TM-T70' OR `model` = 'TM-T70II'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_T81' WHERE `model` = 'TM-T81II'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_T82' WHERE `model` = 'TM-T82' OR `model` = 'TM-T82II'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_T88' WHERE `model` = 'TM-T88V' OR `model` = 'TM-T88V-i'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_T90' WHERE `model` = 'TM-T90II'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_U220' WHERE `model` = 'TM-U220'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_M30' WHERE `model` = 'TM-m30'");
                $this->execute("UPDATE `printer_models` SET `printer_series` = 'TM_P20' WHERE `model` = 'TM-P20'");
            }
        }

        if ($this->hasTable('extensions')) {
            $row = $this->fetchRow("SELECT * FROM `extensions` WHERE `title` = 'Lavu Kiosk'");
            if ($row == false) {
                $this->execute("
                    INSERT INTO `extensions` (`id`, `title`, `thumbnail`, `short_description`, `description`, `module`, `group_ids`, `cost`, `cost_type`, `dev`, `prerequisites`, `_deleted`, `enabled_description`, `native`, `code_word`, `extra_info`)
                    VALUES
                        (25, 'Lavu Kiosk', '', '<div>Lavu Kiosk is Lavu\'s brand new Kiosk platform that takes the tablet out of your staff\'s hands and puts it into your customers.Orders that are paid for by your customers are automatically sent to the assigned kitchen printers.<br><br></div>\n\n \n\n<div>Streamlining this process allows your staff to spend less time on the POS application and more time helping your customers.<br><br></div>\n\nSelect the number of devices and click below to enable Lavu Kiosk on your account.<br><br>\n\n<div>\n  <select id=\"kiosk_devices\" type=\"text\" style=\"width:10%\" required>\n  <option value=\"0\"></option>\n <option value =\"1\">1</option>\n   <option value =\"2\">2</option>\n   <option value =\"3\">3</option>\n   <option value =\"4\">4</option>\n   <option value =\"5\">5</option>\n   <option value =\"6\">6</option>\n   <option value =\"7\">7</option>\n   <option value =\"8\">8</option>\n   <option value =\"9\">9</option>\n   <option value =\"10\">10</option>\n</select>\n<label for=\"kiosk_devices\" style=\"display: inline-block;\"?>Device(s)</label>\n</div>\n', '', 'extensions.ordering.kiosk', '5', '', '', 1, '', 0, 'Kiosk has been enabled.', 0, '', '')
                ");
            }
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable('menu_meta_data')) {
            $this->execute('DROP TABLE `menu_meta_data`');
        }

        if ($this->hasTable('standard_nutrition_types')) {
            $this->execute('DROP TABLE `standard_nutrition_types`');
        }

        if ($this->hasTable('payment_status')) {
            if ($this->table('payment_status')->hasColumn('custom_max_kiosk')) {
                $this->execute('ALTER TABLE `payment_status` DROP COLUMN `custom_max_kiosk`');
            }
        }

        if ($this->hasTable('printer_models')) {
            if ($this->table('printer_models')->hasColumn('printer_series')) {
                $this->execute('ALTER TABLE `printer_models` DROP COLUMN `printer_series`');
            }
        }
    }
}