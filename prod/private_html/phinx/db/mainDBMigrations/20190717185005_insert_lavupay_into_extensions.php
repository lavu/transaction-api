<?php


use Phinx\Migration\AbstractMigration;

class InsertLavupayIntoExtensions extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('extensions')) {
                $row = $this->fetchRow("SELECT * FROM `extensions` WHERE `code_word` = 'lavupay'");
                if ($row == false) {
                    $this->execute("
                        INSERT INTO `extensions` (`title`, `thumbnail`, `short_description`, `description`, `module`, `group_ids`, `cost`, `cost_type`, `dev`, `prerequisites`, `_deleted`, `enabled_description`, `native`, `code_word`, `extra_info`)
                        VALUES
                        ('LavuPay', '', 'LavuPay', '', 'extensions.payment.lavupay', '1', '', '', 1, '', '0',  'LavuPay has been enabled.', 0,  'lavupay', '')
                    ");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('extensions')) {
                $row = $this->execute("DELETE FROM `extensions` WHERE `code_word` = 'lavupay'");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}