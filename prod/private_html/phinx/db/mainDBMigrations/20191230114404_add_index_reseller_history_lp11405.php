<?php


use Phinx\Migration\AbstractMigration;

class AddIndexResellerHistoryLp11405 extends AbstractMigration
{
    public $status;

    public $indexes = array('reseller_id'=>'idx_reseller_id');
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('reseller_history') && $this->table('reseller_history')->hasColumn('reseller_id')) {
                $rows = $this->fetchAll('SHOW INDEX FROM reseller_history');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (!in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'ADD INDEX `' . $index . '` (`' . $column . '`)';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE reseller_history ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('reseller_history') && $this->table('reseller_history')->hasColumn('reseller_id')) {
                $rows = $this->fetchAll('SHOW INDEX FROM reseller_history');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'DROP INDEX `' . $index . '`';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE reseller_history ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
