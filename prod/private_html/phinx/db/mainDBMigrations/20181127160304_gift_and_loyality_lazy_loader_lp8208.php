<?php


use Phinx\Migration\AbstractMigration;

class GiftAndLoyalityLazyLoaderLp8208 extends AbstractMigration
{
    public $status;
    public $tableMainConfig = 'main_config';
    public $tableProcessReports = 'proccess_reports_to_email';
    public $paginationSettingName = 'lavu_gift_and_loyality_pagination';
    public $paginationConfiguration = [
        "limit" => 10000,
        "noOfRowPerPage" => 5,
    ];
    
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            
            // insert pagination configuration 
    		if ($this->hasTable($this->tableMainConfig)){
                $this->execute('INSERT INTO '.$this->tableMainConfig.' (`id`, `setting`, `value`, `_deleted`) VALUES (null, \''.$this->paginationSettingName.'\', \''.json_encode($this->paginationConfiguration).'\', 0)');
            }

            if (!$this->hasTable($this->tableProcessReports)) {
                $table = $this->tableProcessReports;
                $this->execute("CREATE TABLE `".$this->tableProcessReports."` (
                    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    `dataname` VARCHAR(100) COMMENT 'Dataname is used to get company name for restaurants table',
                    `report_id` INT COMMENT 'Link reportv2 table to get report basic details such as title & report type',
                    `queries` TEXT COMMENT 'An array of queries encoded into JSON sting to run mutliple queries, which will help to run multi location queries',
                    `email` VARCHAR(255) COMMENT 'Can consist of multiple email in case if user is sending the same report data to multiple email, this is only applicable when the status is in pending state',
                    `limit` INT COMMENT 'Chuck size will executing the queries',
                    `status` ENUM('pending', 'processing', 'completed', 'error') COMMENT 'This defines the state of the report getting prepaid for report',
                    `no_of_record_processed` INT COMMENT 'Provides you the status of number of records processed',
                    `token` VARCHAR(100) COMMENT 'Token to download',
                    `file_path` VARCHAR(255) COMMENT 'Path from where the file can be set to download',
                    `domain` VARCHAR(100) COMMENT 'Request Domain',
                    `created_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP COMMENT 'Time at which the request was raised',
                    `modified_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date time of the execution was completed',
                    KEY `report_id` (`report_id`),
                    KEY `dataname` (`dataname`),
                    KEY `token` (`token`)
                ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {

            // remove pagination configuration
    		if ($this->hasTable($this->tableMainConfig)){ 
                $this->execute('DELETE FROM '.$this->tableMainConfig.' WHERE setting= \''.$this->paginationSettingName.'\'');
            }
            if($this->hasTable($this->tableProcessReports)) {
                $table = $this->tableProcessReports;
                $this->execute('DROP TABLE `'.$table.'`');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}