<?php


use Phinx\Migration\AbstractMigration;

class MainDbLp1754 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable ( 'report_parts' )) {
    			
    			$row = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `reportid` = "131" and  `type` = "join" and `prop1` = "split_check_details" ');
    			if (!isset($row['id'])) {
    				$this->execute("INSERT INTO `report_parts` (`reportid`, `type`, `prop1`, `prop2`, `prop3`, `prop4`, `tablename`, `fieldname`, `operation`, `opargs`, `filter`, `hide`, `sum`, `graph`, `updatets`, `label`, `branch`, `_order`,  `monetary`, `align`) VALUES  ('131', 'join', 'split_check_details', 'order_id', '(SELECT `order_id`, sum(`gratuity`) AS `sgratuity`, sum(IF (cash > \"0.00\", gratuity, \"0\")) AS sag_cash, sum(IF (card > \"0.00\", gratuity, \"0\")) AS sag_card  FROM ' ,' `split_check_details`  GROUP BY `order_id`)', 'orders', 'order_id', '', '', '', 0, 0, '', '', '', '', 0, 0, '')");
    				$row1 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `reportid` = "131" AND `fieldname` = "ag_cash" ');
    				if (isset($row1['id'])) {
    					$this->execute('UPDATE `report_parts` SET `opargs` = " sum(if(`orders`.`no_of_checks`>1, `split_check_details`.`sag_cash`, * ))" where `reportid` = "131" AND `fieldname` = "ag_cash" ');
    				}
    				$row2 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `reportid` = "131" AND `fieldname` = "ag_card" ');
    				if (isset($row2['id'])) {
    					$this->execute('UPDATE `report_parts` SET `opargs` = " sum(if(`orders`.`no_of_checks`>1, `split_check_details`.`sag_card`, * ))" where `reportid` = "131" AND `fieldname` = "ag_card" ');
    				}
    			}
    			
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$row = $this->fetchRow('SELECT * FROM `report_parts` WHERE `reportid` = "131" and  `type` = "join" and `prop1` = "split_check_details" ');
    		if (isset($row['id'])) {
    			$this->execute('DELETE FROM `report_parts` WHERE  `reportid` = "131" and  `type` = "join" and `prop1` = "split_check_details"');
    			$row1 = $this->fetchRow('SELECT * FROM `report_parts` WHERE `reportid` = "131" AND `fieldname` = "ag_cash" ');
    			if (isset($row1['id'])) {
    				$this->execute('UPDATE `report_parts` SET `opargs` = "sum(*)" where  `reportid` = "131" AND `fieldname` = "ag_cash"');
    			}
    			$row2 = $this->fetchRow('SELECT * FROM `report_parts` WHERE `reportid` = "131" AND `fieldname` = "ag_card" ');
    			if (isset($row2['id'])) {
    				$this->execute('UPDATE `report_parts` SET `opargs` = "sum(*)" where  `reportid` = "131" AND `fieldname` = "ag_card"');
    			}
    		}
    		
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
