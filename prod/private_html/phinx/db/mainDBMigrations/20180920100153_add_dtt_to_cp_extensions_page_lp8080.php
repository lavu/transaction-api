<?php


use Phinx\Migration\AbstractMigration;

class AddDttToCpExtensionsPageLp8080 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if ($this->hasTable ( 'extensions' )) {
				$this->execute("INSERT INTO `extensions` (`title`, `thumbnail`, `short_description`, `description`, `module`, `group_ids`, `cost`, `cost_type`,
`dev`, `prerequisites`, `_deleted`, `enabled_description`, `native`, `code_word`, `extra_info`) VALUES ('DTT', 'DTT-reLogo-Dark.png',
'Simplify your loss prevention routine with the DTT solution, designed specifically for the restaurant industry.<br>
<br>Understand whats happening at each of your registers with DTT POS integration. Text overlay on video shows exactly what items guests are ordering and how
much the employee is charging. Immediately connect suspicious transactions to individual employees and payment stations to fully identify any discrepancies.',
'<table><tr><td>Simplify your loss prevention routine with the DTT solution, </br>designed specifically for the restaurant industry.</td></tr><tr>
<td>Understand whats happening at each of your registers</br> with DTT POS integration. Text overlay on video shows </br>exactly what items guests are ordering and
how much </br>the employee is charging. Immediately connect suspicious</br>  transactions to individual employees and payment stations</br> to fully identify any
 discrepancies.</td></tr></table>', 'extensions.reporting.dtt', 3, '', '', 1, '', 0, 'DTT Service is enabled.', 0,
'dtt', '')");
			}

		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if ($this->hasTable ( 'extensions' )) {
				$this->execute('DELETE FROM `extensions` WHERE  `title` = "DTT" and  `module` = "extensions.reporting.dtt" and `code_word` = "dtt"');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
