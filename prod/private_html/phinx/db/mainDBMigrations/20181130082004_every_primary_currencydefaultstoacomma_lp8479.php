<?php


use Phinx\Migration\AbstractMigration;

class EveryPrimaryCurrencydefaultstoacommaLp8479 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			$tableName = 'country_region';
			if ($this->hasTable($tableName)) {

				if (!$this->table($tableName)->hasColumn('country_code')) {
					$this->execute("ALTER TABLE `$tableName` ADD COLUMN `country_code` varchar(5) NOT NULL AFTER `name`, ADD INDEX (`country_code`)");

					//list of countries and country codes.
					$countyCodeList = ["AFGHANISTAN" => "AF", "ALBANIA" => "AL", "ANTARCTICA" => "AQ", "ALGERIA" => "DZ", "AMERICAN SAMOA" => "AS", "ANDORRA" => "AD", "ANGOLA" => "AO", "ANTIGUA AND BARBUDA" => "AG", "AZERBAIJAN" => "AZ", "ARGENTINA" => "AR", "AUSTRALIA" => "AU", "AUSTRIA" => "AT", "BAHAMAS" => "BS", "BAHRAIN" => "BH", "BANGLADESH" => "BD", "ARMENIA" => "AM", "BARBADOS" => "BB", "BELGIUM" => "BE", "BERMUDA" => "BM", "BHUTAN" => "BT", "BOLIVIA (PLURINATIONAL STATE OF)" => "BO", "BOSNIA AND HERZEGOVINA" => "BA", "BOTSWANA" => "BW", "BOUVET ISLAND" => "BV", "BRAZIL" => "BR", "BELIZE" => "BZ", "BRITISH INDIAN OCEAN TERRITORY (THE)" => "IO", "SOLOMON ISLANDS" => "SB", "VIRGIN ISLANDS (BRITISH)" => "VG", "BRUNEI DARUSSALAM" => "BN", "BULGARIA" => "BG", "MYANMAR" => "MM", "BURUNDI" => "BI", "BELARUS" => "BY", "CAMBODIA" => "KH", "CAMEROON" => "CM", "CANADA" => "CA", "CABO VERDE" => "CV", "CAYMAN ISLANDS (THE)" => "KY", "CENTRAL AFRICAN REPUBLIC (THE)" => "CF", "SRI LANKA" => "LK", "CHAD" => "TD", "CHILE" => "CL", "CHINA" => "CN", "TAIWAN (PROVINCE OF CHINA)" => "TW", "CHRISTMAS ISLAND" => "CX", "COCOS (KEELING) ISLANDS (THE)" => "CC", "COLOMBIA" => "CO", "COMOROS (THE)" => "KM", "MAYOTTE" => "YT", "CONGO (THE)" => "CG", "CONGO (THE DEMOCRATIC REPUBLIC OF THE)" => "CD", "COOK ISLANDS (THE)" => "CK", "COSTA RICA" => "CR", "CROATIA" => "HR", "CUBA" => "CU", "CYPRUS" => "CY", "CZECH REPUBLIC (THE)" => "CZ", "BENIN" => "BJ", "DENMARK" => "DK", "DOMINICA" => "DM", "DOMINICAN REPUBLIC" => "DO", "ECUADOR" => "EC", "EL SALVADOR" => "SV", "EQUATORIAL GUINEA" => "GQ", "ETHIOPIA" => "ET", "EUROPEAN UNION" => "EU", "ERITREA" => "ER", "ESTONIA" => "EE", "FAROE ISLANDS (THE)" => "FO", "FALKLAND ISLANDS (THE) [MALVINAS]" => "FK", "SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS" => "GS", "FIJI" => "FJ", "FINLAND" => "FI", "ÅLAND ISLANDS" => "AX", "FRANCE" => "FR", "FRENCH GUIANA" => "GF", "FRENCH POLYNESIA" => "PF", "FRENCH SOUTHERN TERRITORIES (THE)" => "TF", "DJIBOUTI" => "DJ", "GABON" => "GA", "GEORGIA" => "GE", "GAMBIA" => "GM", "PALESTINE, STATE OF" => "PS", "GERMANY" => "DE", "GHANA" => "GH", "GIBRALTAR" => "GI", "KIRIBATI" => "KI", "GREECE" => "GR", "GREENLAND" => "GL", "GRENADA" => "GD", "GUADELOUPE" => "GP", "GUAM" => "GU", "GUATEMALA" => "GT", "GUINEA" => "GN", "GUYANA" => "GY", "HAITI" => "HT", "HEARD ISLAND AND McDONALD ISLANDS" => "HM", "HOLY SEE (THE)" => "VA", "HONDURAS" => "HN", "HONG KONG" => "HK", "HUNGARY" => "HU", "ICELAND" => "IS", "INDIA" => "IN", "INDONESIA" => "ID", "IRAN (ISLAMIC REPUBLIC OF)" => "IR", "IRAQ" => "IQ", "IRELAND" => "IE", "ISRAEL" => "IL", "ITALY" => "IT", "CÔTE D'IVOIRE" => "CI", "JAMAICA" => "JM", "JAPAN" => "JP", "KAZAKHSTAN" => "KZ", "JORDAN" => "JO", "KENYA" => "KE", "KOREA (THE DEMOCRATIC PEOPLE’S REPUBLIC OF)" => "KP", "THE REPUBLIC OF KOREA" => "KR", "KUWAIT" => "KW", "KYRGYZSTAN" => "KG", "PEOPLE’S DEMOCRATIC REPUBLIC LAOS" => "LA", "LEBANON" => "LB", "LESOTHO" => "LS", "LATVIA" => "LV", "LIBERIA" => "LR", "LIBYA" => "LY", "LIECHTENSTEIN" => "LI", "LITHUANIA" => "LT", "LUXEMBOURG" => "LU", "MACAO" => "MO", "MADAGASCAR" => "MG", "MALAWI" => "MW", "MALAYSIA" => "MY", "MALDIVES" => "MV", "MALI" => "ML", "MALTA" => "MT", "MARTINIQUE" => "MQ", "MAURITANIA" => "MR", "MAURITIUS" => "MU", "MEXICO" => "MX", "MONACO" => "MC", "MONGOLIA" => "MN", "MOLDOVA (THE REPUBLIC OF)" => "MD", "MONTENEGRO" => "ME", "MONTSERRAT" => "MS", "MOROCCO" => "MA", "MOZAMBIQUE" => "MZ", "OMAN" => "OM", "NAMIBIA" => "NA", "NAURU" => "NR", "NEPAL" => "NP", "NETHERLANDS (THE)" => "NL", "CURAÇAO" => "CW", "ARUBA" => "AW", "SINT MAARTEN (DUTCH PART)" => "SX", "BONAIRE" => "BQ", "NEW CALEDONIA" => "NC", "VANUATU" => "VU", "NEW ZEALAND" => "NZ", "NICARAGUA" => "NI", "NIGER (THE)" => "NE", "NIGERIA" => "NG", "NIUE" => "NU", "NORFOLK ISLAND" => "NF", "NORWAY" => "NO", "NORTHERN MARIANA ISLANDS (THE)" => "MP", "UNITED STATES MINOR OUTLYING ISLANDS (THE)" => "UM", "MICRONESIA (FEDERATED STATES OF)" => "FM", "MARSHALL ISLANDS (THE)" => "MH", "PALAU" => "PW", "PAKISTAN" => "PK", "PANAMA" => "PA", "PAPUA NEW GUINEA" => "PG", "PARAGUAY" => "PY", "PERU" => "PE", "PHILIPPINES (THE)" => "PH", "PITCAIRN" => "PN", "POLAND" => "PL", "PORTUGAL" => "PT", "GUINEA-BISSAU" => "GW", "TIMOR-LESTE" => "TL", "PUERTO RICO" => "PR", "QATAR" => "QA", "RÉUNION" => "RE", "ROMANIA" => "RO", "RUSSIAN FEDERATION" => "RU", "RWANDA" => "RW", "SAINT BARTHÉLEMY" => "BL", "SAINT HELENA" => "SH", "SAINT KITTS AND NEVIS" => "KN", "ANGUILLA" => "AI", "SAINT LUCIA" => "LC", "SAINT MARTIN (FRENCH PART)" => "MF", "SAINT PIERRE AND MIQUELON" => "PM", "SAINT VINCENT AND THE GRENADINES" => "VC", "SAN MARINO" => "SM", "SAO TOME AND PRINCIPE" => "ST", "SAUDI ARABIA" => "SA", "SENEGAL" => "SN", "SERBIA" => "RS", "SEYCHELLES" => "SC", "SIERRA LEONE" => "SL", "SINGAPORE" => "SG", "SLOVAKIA" => "SK", "VIET NAM" => "VN", "SLOVENIA" => "SI", "SOMALIA" => "SO", "SOUTH AFRICA" => "ZA", "ZIMBABWE" => "ZW", "SPAIN" => "ES", "SOUTH SUDAN" => "SS", "SUDAN (THE)" => "SD", "WESTERN SAHARA" => "EH", "SURINAME" => "SR", "SVALBARD AND JAN MAYEN" => "SJ", "SWAZILAND" => "SZ", "SWEDEN" => "SE", "SWITZERLAND" => "CH", "SYRIAN ARAB REPUBLIC" => "SY", "TAJIKISTAN" => "TJ", "THAILAND" => "TH", "TOGO" => "TG", "TOKELAU" => "TK", "TONGA" => "TO", "TRINIDAD AND TOBAGO" => "TT", "UNITED ARAB EMIRATES" => "AE", "TUNISIA" => "TN", "TURKEY" => "TR", "TURKMENISTAN" => "TM", "TURKS AND CAICOS ISLANDS (THE)" => "TC", "TUVALU" => "TV", "UGANDA" => "UG", "UKRAINE" => "UA", "MACEDONIA (THE FORMER YUGOSLAV REPUBLIC OF)" => "MK", "EGYPT" => "EG", "UNITED KINGDOM OF GREAT BRITAIN AND NORTHERN IRELAND (THE)" => "GB", "GUERNSEY" => "GG", "JERSEY" => "JE", "ISLE OF MAN" => "IM", "TANZANIA" => "TZ", "UNITED STATES OF AMERICA (THE)" => "US", "VIRGIN ISLANDS (U.S.)" => "VI", "BURKINA FASO" => "BF", "URUGUAY" => "UY", "UZBEKISTAN" => "UZ", "VENEZUELA (BOLIVARIAN REPUBLIC OF)" => "VE", "WALLIS AND FUTUNA" => "WF", "SAMOA" => "WS", "YEMEN" => "YE", "ZAMBIA" => "ZM"];

					foreach ($countyCodeList as $countryName => $countryCode) {
						$this->execute('update `'.$tableName.'` set `country_code` = "'.$countryCode.'" where name ="'.$countryName.'"');
					}
				}

				if ($this->table($tableName)->hasColumn('currency_separator')) {

					//list of country code,currency code and decimal separators.
					$decimalSeparatorsList = ['AS' => ['USD',  '.'], 'AI' => ['XCD',  '.'], 'AG' => ['XCD',  '.'], 'AU' => ['AUD',  '.'], 'BS' => ['BSD',  '.'], 'BD' => ['BDT',  '.'], 'BB' => ['BBD',  '.'], 'BZ' => ['BZD',  '.'], 'BM' => ['BMD', '.'], 'BT' => ['INR',  '.'], 'BQ' => ['USD',  '.'], 'BW' => ['BWP',  '.'], 'IO' => ['USD',  '.'], 'BN' => ['BND',  '.'], 'CV' => ['CVE',  '.'], 'KH' => ['KHR',  '.'], 'KY' => ['KYD',  '.'], 'CN' => ['CNY',  '.'], 'CX' => ['AUD',  '.'], 'CC' => ['AUD',  '.'], 'CO' => ['COU',  '.'], 'CK' => ['NZD',  '.'], 'CU' => ['CUC',  '.'], 'DJ' => ['DJF',  '.'], 'DM' => ['XCD',  '.'], 'DO' => ['DOP',  '.'], 'EG' => ['EGP',  '.'], 'SV' => ['SVC',  '.'], 'SV'=>['USD',  '.'], 'ER' => ['ERN',  '.'], 'ET' => ['ETB',  '.'], 'FK' => ['FKP',  '.'], 'FJ' => ['FJD',  '.'], 'GH' => ['GHS',  '.'], 'GI' => ['GIP',  '.'], 'GD' => ['XCD',  '.'], 'GU' => ['USD',  '.'], 'GT' => ['GTQ',  '.'], 'GG' => ['GBP',  '.'], 'GY' => ['GYD',  '.'], 'HT' => ['USD',  '.'], 'HM' => ['AUD',  '.'], 'HN' => ['HNL',  '.'], 'HK' => ['HKD',  '.'], 'IN' => ['INR',  '.'], 'IE' => ['EUR',  '.'], 'IM' => ['GBP',  '.'], 'IL' => ['ILS',  '.'], 'FK' => ['FKP',  '.'], 'FJ' => ['FJD',  '.'], 'JM' => ['JMD',  '.'], 'JP' => ['JPY',  '.'], 'JE' => ['GBP',  '.'], 'JO' => ['JOD',  '.'], 'KE' => ['KES',  '.'], 'KI' => ['AUD',  '.'], 'KP' => ['KPW',  '.'], 'LS' =>['ZAR', '.'], 'LR' => ['LRD',  '.'], 'LI' => ['CHF',  '.'], 'MY' => ['MYR',  '.'], 'MT' => ['EUR',  '.'], 'MH' => ['USD',  '.'], 'MU' => ['MUR',  '.'], 'MX' => ['MXN',  '.'], 'FM' => ['USD',  '.'], 'MS' => ['XCD',  '.'], 'MM' => ['MMK',  '.'], 'NA' => ['ZAR',  '.'], 'NR' => ['AUD',  '.'], 'NP' => ['NPR',  '.'], 'NZ' => ['NZD',  '.'], 'NI' => ['NIO',  '.'], 'NG' => ['NGN',  '.'], 'NU' => ['NZD',  '.'], 'NF' => ['AUD',  '.'], 'MP' => ['USD',  '.'], 'PK' => ['PKR',  '.'], 'PW' => ['USD',  '.'], 'PA' => ['PAB',  '.'], 'PA' => ['USD',  '.'], 'PH' => ['PHP',  '.'], 'PN' => ['NZD',  '.'], 'PR' => ['USD',  '.'], 'SH' => ['SHP',  '.'], 'KN' => ['XCD',  '.'], 'LC' => ['XCD',  '.'], 'VC' => ['XCD',  '.'], 'WS' => ['WST',  '.'], 'SG' => ['SGD',  '.'], 'SB' => ['SBD',  '.'], 'SO' => ['SOS',  '.'], 'LK' => ['LKR',  '.'], 'SR' => ['SRD',  '.'], 'SZ' => ['SZL',  '.'], 'CH' => ['CHF',  '.'], 'TW' => ['TWD',  '.'], 'TZ' => ['TZS',  '.'], 'TH' => ['THB',  '.'], 'TL' => ['USD',  '.'], 'TK' => ['NZD',  '.'], 'TO' => ['TOP',  '.'], 'TT' => ['TTD',  '.'], 'TC' => ['USD',  '.'], 'TV' => ['AUD',  '.'], 'UG' => ['UGX',  '.'], 'GB' =>['GBP',  '.'], 'UM' => ['USD',  '.'], 'US' => ['USD',  '.'], 'VN' => ['VND',  '.'], 'VG' => ['USD',  '.'], 'VI' => ['USD',  '.'], 'ZM' => ['ZMW',  '.'], 'ZW' => ['ZWL',  '.'], 'BT' => ['BTN',  ''], 'GM' => ['GMD',  ''], 'HT' => ['HTG', ''], 'PF' => ['XPF',  ''], 'LA' => ['LAK',  ''], 'LS' => ['LSL',  ''], 'MO' => ['MOP',  ''], 'MG' => ['MGA',  ''], 'MW' => ['MWK',  ''], 'MV' => ['MVR',  ''], 'ML' => ['XOF',  ''], 'MR' => ['MRO',  ''], 'NC' => ['XPF',  ''], 'NE' => ['XOF',  ''], 'PG' => ['PGK',  ''], 'ST' => ['STD',  ''], 'SC' => ['SCR',  ''], 'SL' => ['SLL',  ''], 'SS' => ['SSP',  ''], 'SY' => ['SYP',  ''], 'TJ' => ['TJS',  ''], 'TM' => ['TMT',  ''], 'WF' => ['XPF',  ''], 'EH' => ['MAD',  '']];

					foreach ($decimalSeparatorsList as $countryCode => $countryInfo) {
						$currencyCode = $countryInfo[0];
						$decimalSeparator = $countryInfo[1];

						$this->execute('update `'.$tableName.'` SET `currency_separator` = "'.$decimalSeparator.'" where `country_code` ="'.$countryCode.'" and `alphabetic_code` ="'.$currencyCode.'"');
					}
				}
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		$this->logException($this->getName(), 'Rollback is not required for this migration');
	}
}