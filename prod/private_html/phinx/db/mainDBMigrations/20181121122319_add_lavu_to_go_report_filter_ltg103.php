<?php


use Phinx\Migration\AbstractMigration;

class AddLavuToGoReportFilterLtg103 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable ( 'reports_v2' )) {
                // inserting one new row titled LTG Order
                $rows = [
                    'selection_column' => '',
                    'special_code' => '',
                    'selection' => 'Preset Date Ranges',
                    'table' => 'orders',
                    'title' => 'LavuToGo Orders',
                    'name' => 'lavuToGoOrders',
                    'orderby' => '#order_id',
                    'select' => 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#kiosk_order,#lavu_togo_order,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#sum_contents_quantity[_sep_]#order_count[_sep_]#gross_contents[_sep_]#sum_contents_discount[_sep_]#net_contents[_sep_]#order_tax[_sep_]#order_included_tax[_sep_]#device_name[_sep_]drill_down(\'@menu_category_name,@super_group,@contents_name,@order_server,@order_cashier,@rev_center,@order_register,@meal_period,@order_register,@order_tag,@order_id,@kiosk_order,@lavu_togo_order,@content_options,@closed_by_hour,@closed_by_day,@closed_by_weekday,@closed_by_week_for_year,@closed_by_month,@closed_by_year,@closed_actual_date_hour,@item_added_by_hour,@item_added_by_day,@item_added_by_weekday,@item_added_by_week_for_year,@item_added_by_month,@item_added_by_year,@item_added_actual_date_hour,@order_reg_group,@order_time,@combo_sales,@overpaid_order,@underpaid_order,@payment_type_name\') as filters',
                    'groupby' => '',
                    'where' => 'sister(\'#orders_placed_active,#orders_placed_void\')[_sep_]orders.location_id=\'[locationid]\'[_sep_]#lavu_togo_orders',
                    'join' => '',
                    '_order' => NULL,
                    '_deleted' => 1,
                    'required_modules' => NULL,
                    'custom_sister_list' => ''
                ];
                $table = $this->table ( 'reports_v2' );
                $table->insert ( $rows )->saveData ();
                
                // updating Sales row
                $salesStmt = $this->query("SELECT `reports_v2`.`select` FROM `reports_v2` where `reports_v2`.`title` = 'Sales' ");    		
                if($salesStmt == true){
                    $this->execute("UPDATE `reports_v2` SET `select` = 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#kiosk_order,#lavu_togo_order,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#sum_contents_quantity[_sep_]#order_count[_sep_]#gross_contents[_sep_]#sum_contents_discount[_sep_]#net_contents[_sep_]#order_tax[_sep_]#order_included_tax[_sep_]drill_down(\'@menu_category_name,@super_group,@contents_name,@order_server,@order_cashier,@rev_center,@order_register,@meal_period,@order_register,@order_tag,@order_id,@kiosk_order,@lavu_togo_order,@content_options,@closed_by_hour,@closed_by_day,@closed_by_weekday,@closed_by_week_for_year,@closed_by_month,@closed_by_year,@closed_actual_date_hour,@item_added_by_hour,@item_added_by_day,@item_added_by_weekday,@item_added_by_week_for_year,@item_added_by_month,@item_added_by_year,@item_added_actual_date_hour,@order_reg_group,@order_time,@combo_sales,@overpaid_order,@underpaid_order,@payment_type_name\') as filters' WHERE `reports_v2`.`title` = 'Sales' ");
                }
                // updating Underpaid Orders row
                $underpaidStmt = $this->query("SELECT `reports_v2`.`select` FROM `reports_v2` where `reports_v2`.`title` = 'Underpaid Orders' ");
                if($underpaidStmt == true){
                    $this->execute("UPDATE `reports_v2` SET `select` = 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#kiosk_order,#lavu_togo_order,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\'[_sep_]#server[_sep_]#closer_by_name[_sep_]#underpaid_amount)' where `reports_v2`.`title` = 'Underpaid Orders' ");
                }
                // updating Overpaid Order row
                $overpaidStmt = $this->query("SELECT `reports_v2`.`select` FROM `reports_v2` where `reports_v2`.`title` = 'Overpaid Order' ");
                if($overpaidStmt == true){
                    $this->execute("UPDATE `reports_v2` SET `select` = 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#kiosk_order,#lavu_togo_order,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#server[_sep_]#closer_by_name[_sep_]#overpaid_amount' WHERE `reports_v2`.`title` = 'Overpaid Order' ");
                }
                // updating Order Time row
                $orderTimeStmt = $this->query("SELECT `reports_v2`.`select` FROM `reports_v2` where `reports_v2`.`title` = 'Order Time' ");
                if($orderTimeStmt == true){
                    $this->execute("UPDATE `reports_v2` SET `select` = 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#kiosk_order,#lavu_togo_order,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#opened[_sep_]#closed[_sep_]#time_elapsed' WHERE `reports_v2`.`title` = 'Order Time' ");
                }
                // updating Kiosk Order row
                $orderTimeStmt = $this->query("SELECT `reports_v2`.`select` FROM `reports_v2` where `reports_v2`.`title` = 'Kiosk Order' ");
                if($orderTimeStmt == true){
                    $this->execute("UPDATE `reports_v2` SET `select` = 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#kiosk_order,#lavu_togo_order,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#opened[_sep_]#closed[_sep_]#time_elapsed' WHERE `reports_v2`.`title` = 'Kiosk Order' ");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    	    if ($this->hasTable ( 'reports_v2' )) {
    	        $this->execute ( "DELETE FROM `reports_v2` WHERE `title` = 'LavuToGo Orders' ");
    	        $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',#lavu_togo_order', '') WHERE `title` = 'Sales' ");
    	        $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',@lavu_togo_order', '') WHERE `title` = 'Sales' ");
    	        $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',#lavu_togo_order', '') WHERE `title` = 'Underpaid Orders' ");
    	        $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',#lavu_togo_order', '') WHERE `title` = 'Overpaid Order' ");
    	        $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',#lavu_togo_order', '') WHERE `title` = 'Order Time' ");
    	        $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',#lavu_togo_order', '') WHERE `title` = 'Kiosk Order' ");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}