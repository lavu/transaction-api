<?php


use Phinx\Migration\AbstractMigration;

class Lp819V2ReportLoyalty extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reports_v2') == true) {
    		    $this->execute("UPDATE `reports_v2`  SET `selection` = 'Preset Date Ranges', `where` = \"#lavu_gift_active[_sep_]((dataname='[dataname]' and dataname!='') or (chainid='[chainid]' and chainid!=''))\" where `title` = 'Lavu Gift and Loyalty' and `name` = 'lavu_gift_and_loyalty' ");
        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reports_v2') == true) {
    		    $this->execute("UPDATE `reports_v2`  SET `selection` = '', `where` = \"(dataname='[dataname]' and dataname!='') or (chainid='[chainid]' and chainid!='')\" where `title` = 'Lavu Gift and Loyalty' and `name` = 'lavu_gift_and_loyalty' ");
        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
