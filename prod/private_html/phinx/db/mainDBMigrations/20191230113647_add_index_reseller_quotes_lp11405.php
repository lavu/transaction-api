<?php


use Phinx\Migration\AbstractMigration;

class AddIndexResellerQuotesLp11405 extends AbstractMigration
{
    public $status;
    public $indexes = array('dataname'=>'idx_dataname');
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('reseller_quotes') && $this->table('reseller_quotes')->hasColumn('dataname')) {
                $rows = $this->fetchAll('SHOW INDEX FROM reseller_quotes');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (!in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'ADD INDEX `' . $index . '` (`' . $column . '`)';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE reseller_quotes ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('reseller_quotes') && $this->table('reseller_quotes')->hasColumn('dataname')) {
                $rows = $this->fetchAll('SHOW INDEX FROM reseller_quotes');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'DROP INDEX `' . $index . '`';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE reseller_quotes ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
