<?php


use Phinx\Migration\AbstractMigration;

class AddIndexResellerLicensesLp11405 extends AbstractMigration
{
    public $status;
    public $indexes = array('resellername'=>'idx_resellername', 'purchased'=>'idx_purchased');
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reseller_licenses') && $this->table('reseller_licenses')->hasColumn('resellername') && $this->table('reseller_licenses')->hasColumn('purchased')) {
                $rows = $this->fetchAll('SHOW INDEX FROM reseller_licenses');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (!in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'ADD INDEX `' . $index . '` (`' . $column . '`)';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE reseller_licenses ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reseller_licenses') && $this->table('reseller_licenses')->hasColumn('resellername') && $this->table('reseller_licenses')->hasColumn('purchased')) {
                $rows = $this->fetchAll('SHOW INDEX FROM reseller_licenses');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'DROP INDEX `' . $index . '`';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE reseller_licenses ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
