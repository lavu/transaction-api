<?php


use Phinx\Migration\AbstractMigration;

class AddLavugiftcardDeletedColumnLp6726 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('lavu_gift_cards')) {
                if (!$this->table('lavu_gift_cards')->hasColumn('deleted')) {
                    $this->execute("ALTER TABLE lavu_gift_cards ADD COLUMN deleted tinyint(1) NOT NULL DEFAULT 0 ");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('lavu_gift_cards')) {
                if ($this->table('lavu_gift_cards')->hasColumn('deleted')) {
                    $this->execute('ALTER TABLE `lavu_gift_cards` DROP COLUMN `deleted`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
