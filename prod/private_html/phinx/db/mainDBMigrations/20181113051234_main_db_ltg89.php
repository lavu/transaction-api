<?php


use Phinx\Migration\AbstractMigration;

class MainDbLtg89 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
			if ($this->hasTable('restaurant_locations') && $this->table('restaurant_locations')->hasColumn('lavu_togo_name') && $this->hasTable( 'audit_log')) {
				$this->execute("ALTER  TABLE restaurant_locations MODIFY COLUMN lavu_togo_name varchar(225) DEFAULT  NULL");
				$this->execute("UPDATE `restaurant_locations` SET `lavu_togo_name` = NULL where `lavu_togo_name` = ''");
				$all_ltg_names_data = $this->fetchAll("SELECT lavu_togo_name FROM restaurant_locations");
				foreach ($all_ltg_names_data as $all_ltg_name) {
					$all_ltg_names[] = $all_ltg_name['lavu_togo_name'];
				}
				$ltg_duplicate_names_list = $this->fetchAll("SELECT dataname, lavu_togo_name, count(lavu_togo_name) as count_nu  FROM restaurant_locations  where lavu_togo_name IS NOT NULL  GROUP BY lavu_togo_name HAVING count(lavu_togo_name) > 1");
				$lavutogo_names = "";
				$poslavu_datanames ="";
				$duplicate_dataname_count = array();
				foreach ($ltg_duplicate_names_list as $ltg_names) {
					$duplicate_dataname_count[$ltg_names['lavu_togo_name']] = $ltg_names['count_nu'];
					$lavutogo_names .= "'".$ltg_names['lavu_togo_name']."',";
				}
				$lavutogo_names = trim($lavutogo_names, ",");
				$ltg_duplicate_list = $this->fetchAll('Select id, dataname, lavu_togo_name , locationid from  restaurant_locations where lavu_togo_name in ('.$lavutogo_names.')');
				foreach ($ltg_duplicate_list as $ltg_duplicate_db) {
					$poslavu_datanames .=  "'poslavu_".$ltg_duplicate_db['dataname']."_db',";
				}
				$poslavu_datanames = trim($poslavu_datanames, ",");
				$check_database = $this->fetchAll('SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME  in ('.$poslavu_datanames.')');
				$list_database = array();
				foreach ($check_database as $databases) {
					$list_database [] = $databases['SCHEMA_NAME'];
				}
				foreach ($ltg_duplicate_list as $ltg_data) {
					$ltg_name = $ltg_data['lavu_togo_name'];
					$dataname = "poslavu_".$ltg_data['dataname']."_db";
					$restaurant_location_id = $ltg_data['id'];
					$location_id = $ltg_data['locationid'];
					if (!in_array($dataname, $list_database)) {
						$this->execute('UPDATE `restaurant_locations` SET `lavu_togo_name` = NULL where id ="'.$restaurant_location_id.'"');
						$this->insertAudit($restaurant_location_id, $ltg_data , 'no');
					} else {
						$countrow = $duplicate_dataname_count [$ltg_name];
						if ($countrow > 1 ) {
							$new_name = $this->validateNewLavutogoname($ltg_name, $countrow, $all_ltg_names);
							$all_ltg_names [] = $new_name;
							$this->execute('UPDATE `restaurant_locations` SET `lavu_togo_name` = "'.$new_name.'" where id ="'.$restaurant_location_id.'"');
							$this->execute('UPDATE  `'.$dataname.'`.`config` SET `value` = "'.$new_name.'" where `location` = "'.$location_id.'" and `setting` ="lavu_togo_name" ');
							$this->insertAudit($restaurant_location_id, $ltg_data , 'yes');
							$duplicate_dataname_count [$ltg_name] = $countrow-1;
						}
					}
				}
				$this->execute("ALTER TABLE restaurant_locations ADD CONSTRAINT uq_lavu_togo_name UNIQUE (lavu_togo_name)");
			}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('restaurant_locations') && $this->table('restaurant_locations')->hasColumn('lavu_togo_name') && $this->hasTable( 'audit_log')) {
    			$this->execute("ALTER TABLE `restaurant_locations` drop INDEX uq_lavu_togo_name");
    			$this->execute("ALTER TABLE restaurant_locations MODIFY COLUMN lavu_togo_name varchar(255) DEFAULT '' NOT NULL");
    			$auditLog_queryData = $this->fetchAll("select id, info from `audit_log` where `updated_for`='ltg89' and `updated_by`='migration'");
    			foreach ($auditLog_queryData as $auditData) {
    				$auditLog = json_decode($auditData['info']);
    				$id = $auditLog->id;
    				$lavu_togo_name = $auditLog->lavu_togo_name;
    				$dataname = "poslavu_".$auditLog->dataname."_db";
    				$location_id = $auditLog->locationid;
    				$database_status = $auditLog->database_status;
    				$this->execute("update `restaurant_locations`  SET `lavu_togo_name`='".$lavu_togo_name."' where `id`='".$id."'");
    				if ($database_status == 'yes') {
    					$this->execute('UPDATE  `'.$dataname.'`.`config` SET `value` = "'.$lavu_togo_name.'" where `location` = "'.$location_id.'" and `setting` ="lavu_togo_name" ');
    				}
    			}
    			$this->execute( "DELETE FROM `audit_log` WHERE `updated_for`='ltg89' and `updated_by`='migration'");
				
			}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
    
    /**
     * validate New Lavu togo name.
     */
    public function validateNewLavutogoname($ltg_name, $number, $all_ltg_names)
    {
    	$new_ltg_name = $ltg_name."_".$number;
    	if (in_array($new_ltg_name, $all_ltg_names)) {
    		$this->validateNewLavutogoname($new_ltg_name, $number, $all_ltg_names);
    	} else {
    		return $new_ltg_name;
    	}
    }

    /**
     * Insert Data Audit Log  Table.
     */
    public function insertAudit($restaurant_location_id, $ltg_data , $dabase_status) {
    	$ltg_data['database_status'] = $dabase_status;
    	$ocInfoData = json_encode($ltg_data);
    	$currDate = date('Y-m-d H:i:s');
    	$ocData = ['id'=>$restaurant_location_id, 'id_type'=>'id', 'table_name'=>'restaurant_locations', 'info'=>$ocInfoData, 'updated_for'=>'ltg89', 'updated_by'=>'migration', 'created_date'=>$currDate];
    	$auditTable = $this->table( 'audit_log' );
    	$auditTable->insert($ocData)->saveData();
    }
}
