<?php


use Phinx\Migration\AbstractMigration;

class SquareFunctionalityLp6039 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    	    if ($this->table('gateway_sync_request')->hasColumn('transaction_type') == false) {
    	        $this->execute("ALTER TABLE `gateway_sync_request` ADD COLUMN `transaction_type` varchar(100) NOT NULL");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    	    if ($this->table('gateway_sync_request')->hasColumn('transaction_type') == true) {
    	        $this->execute("ALTER TABLE `gateway_sync_request` DROP COLUMN `transaction_type`");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
