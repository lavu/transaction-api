<?php


use Phinx\Migration\AbstractMigration;

class AddIndexReservationsLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reservations') && $this->table('reservations')->hasColumn('email')) {
                $row = $this->fetchRow('SHOW INDEX FROM reservations WHERE Key_name = "idx_email"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE reservations ADD INDEX `idx_email` (`email`)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reservations') && $this->table('reservations')->hasColumn('email')) {
                $row = $this->fetchRow('SHOW INDEX FROM reservations WHERE Key_name = "idx_email"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE reservations DROP INDEX `idx_email`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
