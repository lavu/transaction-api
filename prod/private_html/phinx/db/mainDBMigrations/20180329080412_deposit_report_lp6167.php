<?php


use Phinx\Migration\AbstractMigration;

class DepositReportLp6167 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        if ($this->hasTable( 'reports_v2' )) {
            $this->execute( "DELETE FROM `reports_v2` WHERE `title` IN ('Customer Deposits')");
            // To displaying change bank & Bank Deposit report tab in v2 reports:
            $insertReportRecord = array(
                        'selection_column' => '',
                        'special_code' => '//custom:get_deposit_detail',
                        'selection' => 'Preset Date Ranges',
                        'table' => 'deposit_customer_info',
                        'title' => 'Customer Deposits',
                        'name' => 'deposit_detail',
                        'orderby' => '',
                        'select' => '',
                        'groupby' => '',
                        'where' => '',
                        'join' => '',
                        '_order' => NULL,
                        '_deleted' => 0,
                        'required_modules' => NULL,
                        'custom_sister_list' => ''
                   );
            $table = $this->table( 'reports_v2' );
            $table->insert( $insertReportRecord )->saveData();
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        if ($this->hasTable ( 'reports_v2' )) {
            $this->execute ( "DELETE FROM `reports_v2` WHERE `title` IN ('Customer Deposits')" );
        }
    }
}
