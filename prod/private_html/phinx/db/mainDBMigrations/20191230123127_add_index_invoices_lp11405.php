<?php


use Phinx\Migration\AbstractMigration;

class AddIndexInvoicesLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('invoices') && $this->table('invoices')->hasColumn('due_date')) {
                $row = $this->fetchRow('SHOW INDEX FROM invoices WHERE Key_name = "idx_due_date"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE invoices ADD INDEX `idx_due_date` (`due_date`)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('invoices') && $this->table('invoices')->hasColumn('due_date')) {
                $row = $this->fetchRow('SHOW INDEX FROM invoices WHERE Key_name = "idx_due_date"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE invoices DROP INDEX `idx_due_date`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
