<?php


use Phinx\Migration\AbstractMigration;

class AddIndexGatewaySyncRequestLp11405 extends AbstractMigration
{
    public $status;
    public $indexes = array('gateway'=>'idx_gateway', 'data_name'=>'idx_data_name');
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('gateway_sync_request') && $this->table('gateway_sync_request')->hasColumn('gateway') && $this->table('gateway_sync_request')->hasColumn('data_name')) {
                $rows = $this->fetchAll('SHOW INDEX FROM gateway_sync_request');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (!in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'ADD INDEX `' . $index . '` (`' . $column . '`)';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE gateway_sync_request ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('gateway_sync_request') && $this->table('gateway_sync_request')->hasColumn('gateway') && $this->table('gateway_sync_request')->hasColumn('data_name')) {
                $rows = $this->fetchAll('SHOW INDEX FROM gateway_sync_request');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'DROP INDEX `' . $index . '`';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE gateway_sync_request ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
