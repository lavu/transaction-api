<?php


use Phinx\Migration\AbstractMigration;

class RedesignFiscalReportMainDatabase extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        // INSERT INTO [POSLAVU MAIN DB].`reports_v2`
        if ($this->hasTable ( 'reports_v2' )) {
            
            // inserting only one row
            $singleRow = [
                'selection_column' => '',
                'special_code' => '//custom:fiscal_reconciliation_report',
                'selection' => 'Preset Date Ranges',
                'table' => 'fiscal_reports',
                'title' => 'Fiscal Reconciliation v2',
                'name' => 'fiscal_reconciliation_v2',
                'orderby' => '',
                'select' => '',
                'groupby' => '',
                'where' => '',
                'join' => '',
                '_order' => 0,
                '_deleted' => '1',
                'required_modules' => 'fiscal.+printing',
                'custom_sister_list' => 'FiscalZReconciliation,fiscal_accounting,fiscal_daily_accounting,fiscal_accounting_v2,fiscal_reconciliation_v2'
            ];
            $table = $this->table ( 'reports_v2' );
            $table->insert ( $singleRow )->saveData ();
            
            
            //update table [POSLAVU MAIN DB].`reports_v2`
            $this->execute("UPDATE `reports_v2`  SET `custom_sister_list`='FiscalZReconciliation,fiscal_accounting,fiscal_daily_accounting,fiscal_accounting_v2,fiscal_reconciliation_v2' WHERE id=44");
            $this->execute("UPDATE `reports_v2`  SET `custom_sister_list`='FiscalZReconciliation,fiscal_accounting,fiscal_daily_accounting,fiscal_accounting_v2,fiscal_reconciliation_v2' WHERE id=45");
            $this->execute("UPDATE `reports_v2`  SET `custom_sister_list`='FiscalZReconciliation,fiscal_accounting,fiscal_daily_accounting,fiscal_accounting_v2,fiscal_reconciliation_v2' WHERE id=59");
            
        }
        

        
        
        if ($this->hasTable ( 'language_dictionary' ))
        {
            // inserting multiple rows
            $rows = [
                [
                    'pack_id' => 2,
                    'word_or_phrase' => 'Number Of FC NC',
                    'translation' => 'Numero de FC/NC',
                    '_deleted' => 0,
                    'english_id' => 0,
                    'used_by' => 'cp',
                    'created_date' => date ( "Y-m-d H:i:s", time () ) . "Z",
                    'modified_date' => '',
                    'backtrace' => '',
                    'tag' => ''
                ],
                [
                    'pack_id' => 2,
                    'word_or_phrase' => 'Iva Percentage',
                    'translation' => 'Porcentaje de Iva',
                    '_deleted' => 0,
                    'english_id' => 0,
                    'used_by' => 'cp',
                    'created_date' => date ( "Y-m-d H:i:s", time () ) . "Z",
                    'modified_date' => '',
                    'backtrace' => '',
                    'tag' => ''
                ],
                [
                    'pack_id' => 2,
                    'word_or_phrase' => 'Taxable Net Sale',
                    'translation' => 'Venta Neta Gravada',
                    '_deleted' => 0,
                    'english_id' => 0,
                    'used_by' => 'cp',
                    'created_date' => date ( "Y-m-d H:i:s", time () ) . "Z",
                    'modified_date' => '',
                    'backtrace' => '',
                    'tag' => ''
                ],
                [
                    'pack_id' => 2,
                    'word_or_phrase' => 'Client',
                    'translation' => 'cliente',
                    '_deleted' => 0,
                    'english_id' => 0,
                    'used_by' => 'cp',
                    'created_date' => date ( "Y-m-d H:i:s", time () ) . "Z",
                    'modified_date' => '',
                    'backtrace' => '',
                    'tag' => ''
                ]
            ];
            
            $this->insert ( 'language_dictionary', $rows );
            
            
            $this->execute("UPDATE `language_dictionary`  SET `word_or_phrase` = 'Number Of Z', `translation` = 'Numero de Z' WHERE `pack_id`= 2 and `word_or_phrase` = 'Number of Z' ");
            $this->execute("UPDATE `language_dictionary`  SET `word_or_phrase` = 'Total Sales', `translation` = 'Venta Total' WHERE `pack_id`= 2 and `word_or_phrase` = 'Total Sales' ");
        }
        
        
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        
        // Rollback from 'reports_v2' table;
        if ($this->hasTable ( 'reports_v2' )) {
            $this->execute ( 'DELETE FROM `reports_v2` WHERE name="fiscal_reconciliation_v2"' );
            $this->execute("UPDATE `reports_v2`  SET `custom_sister_list`='FiscalZReconciliation,fiscal_accounting,fiscal_daily_accounting,fiscal_accounting_v2' WHERE id=44");
            $this->execute("UPDATE `reports_v2`  SET `custom_sister_list`='FiscalZReconciliation,fiscal_accounting,fiscal_daily_accounting,fiscal_accounting_v2' WHERE id=45");
            $this->execute("UPDATE `reports_v2`  SET `custom_sister_list`='FiscalZReconciliation,fiscal_accounting,fiscal_daily_accounting,fiscal_accounting_v2' WHERE id=59");
        }
        
        if ($this->hasTable('language_dictionary')) {
            $this->execute('DELETE FROM `language_dictionary` WHERE `word_or_phrase` IN ("Number Of FC NC","Iva Percentage","Taxable Net Sale","Client") ');
            $this->execute("UPDATE `language_dictionary`  SET `word_or_phrase` = 'Number of Z', `translation` = '' WHERE `pack_id`= 2 and `word_or_phrase` = 'Number Of Z' ");
            $this->execute("UPDATE `language_dictionary`  SET `word_or_phrase` = 'Total Sales', `translation` = 'Venta Bruta Sin Impuestos' WHERE `pack_id`= 2 and `word_or_phrase` = 'Total Sales' ");
        }
        
        
    }
}
