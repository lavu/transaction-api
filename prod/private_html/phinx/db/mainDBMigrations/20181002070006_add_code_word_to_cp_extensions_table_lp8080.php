<?php


use Phinx\Migration\AbstractMigration;

class AddCodeWordToCpExtensionsTableLp8080 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if ($this->hasTable('extensions') == true) {
				if ($this->table('extensions')->hasColumn('code_word')) {
					$codeWordList = array(1=>'mercury_paypal', 2=>'mercury_wells_fargo', 3=>'loyaltree', 4=>'quickbooks', 5=>'pizza_creator', 6=>'customer_management', 7=>'delivery', 8=>'routing', 9=>'mercury_mobile_wallets', 10=>'lavu_fit', 11=>'lavu_give', 12=>'lavu_kart', 13=>'guest_metrics', 14=>'self_serve_mode', 15=>'moneris', 16=>'lavu_gift', 18=>'lavu_loyalty', 19=>'loyaltree_white_label', 23=>'square', 25=>'lavu_kiosk', 27=>'sacoa_playcard');

					foreach ($codeWordList as $extId => $codeWord) {
						$this->execute("UPDATE `extensions` SET `code_word` = '".$codeWord."' WHERE `id` ='".$extId."'");
					}
					$this->execute('ALTER TABLE extensions CHANGE COLUMN code_word code_word VARCHAR(255) NOT NULL, ADD UNIQUE codeword (code_word)');
				}
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if ($this->hasTable('extensions') == true) {
				if ($this->table('extensions')->hasColumn('code_word')) {
					$this->execute('ALTER TABLE extensions DROP INDEX codeword, CHANGE COLUMN code_word code_word VARCHAR(255) NOT NULL DEFAULT ""');

					$codeWordList = array(1=>'mercury_paypal', 2=>'mercury_wells_fargo', 3=>'loyaltree', 4=>'quickbooks', 5=>'pizza_creator', 6=>'customer_management', 7=>'delivery', 8=>'routing', 9=>'mercury_mobile_wallets', 10=>'lavu_fit', 11=>'lavu_give', 12=>'lavu_kart', 13=>'guest_metrics', 14=>'self_serve_mode', 15=>'moneris', 16=>'lavu_gift', 18=>'lavu_loyalty', 19=>'loyaltree_white_label', 23=>'square', 25=>'lavu_kiosk', 27=>'sacoa_playcard');
					$codeWordKeyList = array_keys($codeWordList);
					foreach ($codeWordKeyList as $extId) {
						$this->execute("UPDATE `extensions` SET `code_word` = '' WHERE `id` ='".$extId."'");
					}
					 
				}
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
