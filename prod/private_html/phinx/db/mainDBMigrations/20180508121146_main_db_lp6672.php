<?php


use Phinx\Migration\AbstractMigration;

class MainDbLp6672 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
	    if ($this->hasTable ( 'language_dictionary' )) {
		    // INSERT INTO [POSLAVU MAIN DB].`language_dictionary`
		    $this->execute ( 'DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_6"' );

		    $datetime = date('Y-m-d H:i:s');
		    // inserting multiple row
		    $multipleRow = [
			    [
				    'pack_id' => '1',
				    'word_or_phrase' => 'Assign a different drawer {1} drawer {2} already assigned to {3}',
				    'english_id' => '',
				    'used_by' => '',
				    'created_date' => $datetime,
				    'modified_date' => $datetime,
				    'tag' => 'ERR_DCD_6'
			    ]
		    ];
		    $table = $this->table ( 'language_dictionary' );
		    $table->insert ( $multipleRow )->saveData ();
	    }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
	    if ($this->hasTable ( 'language_dictionary' )) {
		    // INSERT INTO [POSLAVU MAIN DB].`language_dictionary`
		    $this->execute('DELETE FROM `language_dictionary` WHERE `tag`="ERR_DCD_6"');
	    }
    }
}
