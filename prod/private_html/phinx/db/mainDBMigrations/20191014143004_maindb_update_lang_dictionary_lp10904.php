<?php


use Phinx\Migration\AbstractMigration;

class MaindbUpdateLangDictionaryLp10904 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            /* Write migration code here */
            if ($this->hasTable('language_dictionary')) {
                //Update Phrase "Lavu 4.0 is here! Please visit the App Store and download Lavu 4.0. If you have any questions please call 1-855-767-5288 or email support@lavu.com" with "Please be aware that we will be removing Lavu POS 3 from the iPad App Store as of 1/31/2020. Please upgrade to Lavu POS 4 as soon as possible to take advantage of our new and enhanced platform.
                $wordPhrase = "Please be aware that we will be removing Lavu POS 3 from the iPad App Store as of 1/31/2020. Please upgrade to Lavu POS 4 as soon as possible to take advantage of our new and enhanced platform.";
                $updatePhrase = "UPDATE `language_dictionary` SET `word_or_phrase` = '".$wordPhrase."' WHERE `tag` = 'POS4_UPGRADE_NOTIFICATION'";
                $this->execute($updatePhrase);
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            /* Write rollback code here */
            if ($this->hasTable('language_dictionary')) {
            $oldWordPhrase = "Lavu 4.0 is here! Please visit the App Store and download Lavu 4.0. If you have any questions please call 1-855-767-5288 or email support@lavu.com.";
            $this->execute("UPDATE `language_dictionary` SET `word_or_phrase` = '".$oldWordPhrase."' WHERE `tag` = 'POS4_UPGRADE_NOTIFICATION'");
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
