<?php


use Phinx\Migration\AbstractMigration;

class RedisFor86CountLp5875 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
		    /**
		     * Purpose:- We will store configuration which could be applicable on application level
		     */
		    if (!$this->hasTable('main_config')) {
			    $this->execute("
				CREATE TABLE `main_config` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `setting` varchar(255) NOT NULL,
				  `value` varchar(500) NOT NULL,
				  `_deleted` tinyint(4) NOT NULL,
				  PRIMARY KEY (`id`)
				)
            ");
		    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
		    if ($this->hasTable('main_config')) {
			    $this->execute('DROP TABLE `main_config`');
		    }
    		/* Write rollback code here */
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
