<?php


use Phinx\Migration\AbstractMigration;

class SeedFieldSourceTypes extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            $this->execute("
                INSERT INTO field_source_types (name, code)
                VALUES
                    ('General', 'GEN'),
                    ('Kiosk', 'KIOSK'),
                    ('Lavu 2 Go', 'LA2GO')
            ");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		$this->execute("DELETE FROM field_source_types WHERE code IN ('GEN', 'KIOSK', 'LA2GO')");
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
