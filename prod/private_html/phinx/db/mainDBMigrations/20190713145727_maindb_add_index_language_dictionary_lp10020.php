<?php


use Phinx\Migration\AbstractMigration;

class MaindbAddIndexLanguageDictionaryLp10020 extends AbstractMigration
{
    public $status;
    public $tableName = 'language_dictionary';
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
        if ($this->hasTable($this->tableName)) {
          $this->execute('ALTER TABLE '.$this->tableName.' ADD INDEX `idx_tag` (tag)');
        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
        if ($this->hasTable($this->tableName)) {
          $this->execute('ALTER TABLE '.$this->tableName.' DROP INDEX idx_tag');
        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
