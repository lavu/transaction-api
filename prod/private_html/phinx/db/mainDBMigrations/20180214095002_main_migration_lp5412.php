<?php


use Phinx\Migration\AbstractMigration;

class MainMigrationLp5412 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	// INSERT INTO [POSLAVU MAIN DB].`extensions`
    	if ($this->hasTable ( 'reports_v2' )) {
    		$this->execute ( "DELETE FROM `reports_v2` WHERE title IN ('Overpaid Order','Underpaid Orders')" );
    		
    		// inserting multiple rows
    		$rows = [
    					[
		    				'selection_column' => '',
		    				'special_code' => '',
		    				'selection' => 'Preset Date Ranges',
		    				'table' => 'orders',
		    				'title' => 'Overpaid Order',
		    				'name' => 'overpaidOrders',
		    				'orderby' => '#order_id',
	    					'select' => 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#server[_sep_]#closer_by_name[_sep_]#overpaid_amount',
		    				'groupby' => 0,
		    				'where' => 'sister(\'#orders_placed_active,#orders_placed_void\')[_sep_]orders.location_id=\'[locationid]\'[_sep_]#overpaid_orders',
		    				'join' => '',
		    				'_order' => NULL,
		    				'_deleted' => 1,
		    				'required_modules' => NULL,
		    				'custom_sister_list' => ''
    					],
    					[
    						'selection_column' => '',
    						'special_code' => '',
    						'selection' => 'Preset Date Ranges',
    						'table' => 'orders',
    						'title' => 'Underpaid Orders',
    						'name' => 'underpaidOrders',
    						'orderby' => '#order_id',
    						'select' => 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#server[_sep_]#closer_by_name[_sep_]#underpaid_amount',
    						'groupby' => 0,
    						'where' => 'sister(\'#orders_placed_active,#orders_placed_void\')[_sep_]orders.location_id=\'[locationid]\'[_sep_]#underpaid_orders',
    						'join' => '',
    						'_order' => NULL,
    						'_deleted' => 1,
    						'required_modules' => NULL,
    						'custom_sister_list' => ''
    							
    							
    					]
    				
    		];
    		$table = $this->table ( 'reports_v2' );
    		$table->insert ( $rows )->saveData ();
    		
    		$stmt = $this->query("SELECT `reports_v2`.`select` FROM `reports_v2` where `reports_v2`.`title` = 'Sales' ");    		
    		if($stmt == true){
    			// first remove code
    			$updateSister = $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',#overpaid_order,#underpaid_order', '')	WHERE `title` = 'Sales' ");
    			$updateSister = $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',@overpaid_order,@underpaid_order', '')	WHERE `title` = 'Sales' ");
    			// Second add code
    			$updateSister = $this->execute(" UPDATE `reports_v2` SET `select` = 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#sum_contents_quantity[_sep_]#order_count[_sep_]#gross_contents[_sep_]#sum_contents_discount[_sep_]#net_contents[_sep_]#order_tax[_sep_]#order_included_tax[_sep_]drill_down( \' @menu_category_name,@super_group,@contents_name,@order_server,@order_cashier,@rev_center,@order_register,@meal_period,@order_register,@order_tag,@order_id,@content_options,@closed_by_hour,@closed_by_day,@closed_by_weekday,@closed_by_week_for_year,@closed_by_month,@closed_by_year,@closed_actual_date_hour,@item_added_by_hour,@item_added_by_day,@item_added_by_weekday,@item_added_by_week_for_year,@item_added_by_month,@item_added_by_year,@item_added_actual_date_hour,@order_reg_group,@order_time,@combo_sales,@overpaid_order,@underpaid_order\') as filters' WHERE `reports_v2`.`title` = 'Sales' ");
    		}
    	}
    	

    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	if ($this->hasTable ( 'reports_v2' )) {
    		$this->execute ( "DELETE FROM `reports_v2` WHERE `title` IN ('Overpaid Order','Underpaid Orders')" );
    		
    		
    		$updateSister = $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',#overpaid_order,#underpaid_order', '')	WHERE `title` = 'Sales' ");
			$updateSister = $this->execute("UPDATE `reports_v2` SET `select` = REPLACE(`select`, ',@overpaid_order,@underpaid_order', '')	WHERE `title` = 'Sales' ");

    	}
    	
    	
    	

    }
}
