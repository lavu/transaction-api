<?php


use Phinx\Migration\AbstractMigration;

class MaindbUpdateLangDictionaryLp9926 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		/* Write migration code here */
        if ($this->hasTable('language_dictionary')) {
          //insert for engligh pack_id and get id
          $phrase = 'Lavu 4.0 is here! Please visit the App Store and download Lavu 4.0. If you have any questions please call 1-855-767-5288 or email support@lavu.com.';
          $this->execute('INSERT INTO language_dictionary (pack_id, word_or_phrase, translation, _deleted, english_id, used_by,created_date, modified_date, backtrace, tag) VALUES (1, "'.$phrase.'", "",0, 0,"",NOW(),NOW(),"","POS4_UPGRADE_NOTIFICATION")');

          //get id of english pack phrase
          $data = $this->fetchRow('SELECT id FROM language_dictionary WHERE tag="POS4_UPGRADE_NOTIFICATION" AND pack_id=1');
          $englishId = $data['id'];

          //get all distinct pack_id except english
          $data = $this->fetchAll('SELECT id FROM language_packs WHERE id <> 1');

          //insert for remaining pack_id
          foreach($data as $ids){
            $packId=$ids[0];
            $this->execute('INSERT INTO language_dictionary (pack_id, word_or_phrase, _deleted, english_id, created_date, tag,translation,backtrace,used_by,modified_date) VALUES ('.$packId.', "'.$phrase.'", 0,'.$englishId.',NOW(),"POS4_UPGRADE_NOTIFICATION","","","",NOW())');
          }
        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		/* Write rollback code here */
        if ($this->hasTable('language_dictionary')) {
          $this->execute('DELETE FROM language_dictionary WHERE tag="POS4_UPGRADE_NOTIFICATION"');
        }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
