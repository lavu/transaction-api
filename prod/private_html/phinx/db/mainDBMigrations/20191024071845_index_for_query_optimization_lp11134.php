<?php


use Phinx\Migration\AbstractMigration;

class IndexForQueryOptimizationLp11134 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable("restaurants")) {
                $row = $this->fetchRow('SHOW INDEX FROM restaurants WHERE Key_name="chain_id_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `restaurants` ADD INDEX `chain_id_idx` (`chain_id`)");
                }
            }
            if ($this->hasTable("tos_agreements")) {
                $row = $this->fetchRow('SHOW INDEX FROM tos_agreements WHERE Key_name="username_idx"');
                if (!$row['Key_name']) {
                    $this->execute("ALTER TABLE `tos_agreements` ADD INDEX `username_idx` (`username`)");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable("restaurants")) {
                $row = $this->fetchRow('SHOW INDEX FROM restaurants WHERE Key_name="chain_id_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `restaurants` DROP INDEX `chain_id_idx`");
                }
            }
            if ($this->hasTable("tos_agreements")) {
                $row = $this->fetchRow('SHOW INDEX FROM tos_agreements WHERE Key_name="username_idx"');
                if ($row['Key_name']) {
                    $this->execute("ALTER TABLE `tos_agreements` DROP INDEX `username_idx`");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
