<?php


use Phinx\Migration\AbstractMigration;

class AddIndexRestaurantChainsLp11405 extends AbstractMigration
{
    public $status;
    public $indexes = array('name'=>'idx_name', 'primary_restaurantid'=>'idx_primary_restaurantid');
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('restaurant_chains') && $this->table('restaurant_chains')->hasColumn('name') && $this->table('restaurant_chains')->hasColumn('primary_restaurantid')) {
                $rows = $this->fetchAll('SHOW INDEX FROM restaurant_chains');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (!in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'ADD INDEX `' . $index . '` (`' . $column . '`)';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE restaurant_chains ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('restaurant_chains') && $this->table('restaurant_chains')->hasColumn('name') && $this->table('restaurant_chains')->hasColumn('primary_restaurantid')) {
                $rows = $this->fetchAll('SHOW INDEX FROM restaurant_chains');
                $existingIndexes = array();
                foreach ($rows as $row) {
                    $existingIndexes[] = $row['Key_name'];
                }
                $customIndexArr = array();
                foreach ($this->indexes as $column => $index) {
                    if (in_array($index, $existingIndexes)) {
                        $customIndexArr[] = 'DROP INDEX `' . $index . '`';
                    }
                }
                $addIndexStr = implode(', ', $customIndexArr);
                if (!empty($customIndexArr)) {
                    $this->execute('ALTER TABLE restaurant_chains ' . $addIndexStr);
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
