<?php


use Phinx\Migration\AbstractMigration;

class AddTranslatedMsgsLp9040 extends AbstractMigration
{
    public $status;
    public $msgs;
    public $packId;
    public $insertStmt;
    public $usedBy;
    public $datetime;
    public $languageName;
    public $auditInsertStmt;
    
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	$this->msgs = array(
    	    "Account details updated successfully" => "Los detalles de la cuenta se actualizaron correctamente.",
    	    "Account preference updated successfully" => "La preferencia de la cuenta se actualizó correctamente.",
    	    "Address Name already exists" => "La dirección ya existe.",
    	    "Address deleted successfully" => "La dirección se eliminó correctamente.",
    	    "Address details added successfully" => "Los detalles de la dirección se agregaron correctamente.",
    	    "Address details updated successfully" => "Los detalles de la dirección se actualizaron correctamente.",
    	    "Address not found" => "No se encontró la dirección.",
    	    "Confirm New Password is required" => "Se requiere la confirmación de la contraseña nueva.",
    	    "Confirm Password is required" => "Se requiere la confirmación de la contraseña.",
    	    "Current Password and New Password shouldnot match" => "La contraseña actual y la contraseña nueva no deben coincidir.",
    	    "Current Password is required" => "Se requiere la contraseña actual.",
    	    "Customer Address is required" => "Se requiere la dirección del cliente.",
    	    "Dataname is required" => "Se requiere el nombre del dato.",
    	    "Email Address already exists" => "La dirección de correo electrónico ya existe.",
    	    "Email Address is required" => "Se requiere la dirección de correo electrónico.",
    	    "First Name is required" => "Se requiere el nombre.",
    	    "Incorrect Email Address" => "Dirección de correo electrónico incorrecta",
    	    "Incorrect Password" => "Contraseña incorrecta",
    	    "Insufficient Lavu Gift Card Amount" => "Monto insuficiente de la tarjeta de regalo de Lavu",
    	    "Invalid Address details" => "Detalles de dirección no válidos",
    	    "Invalid Award Points" => "Puntos de recompensa no válidos",
    	    "Invalid Current Password" => "Contraseña actual no válida",
    	    "Invalid Delivery Fee" => "Tarifa de envío no válida",
    	    "Invalid Discount Amount" => "Monto de descuento no válido",
    	    "Invalid Discount Id" => "ID de descuento no válida",
    	    "Invalid Discount Value" => "Valor de descuento no válido",
    	    "Invalid Email Address or Password" => "Contraseña o dirección de correo electrónico no válida",
    	    "Invalid Email Address" => "Dirección de correo electrónico no válida",
    	    "Invalid Lavu Gift Card Amount" => "Monto no válido de la tarjeta de regalo de Lavu",
    	    "Invalid Order Amount" => "Monto de pedido no válido",
    	    "Invalid Order Id" => "ID de pedido no válida",
    	    "Invalid Order Type" => "Tipo de pedido no válido",
    	    "Invalid Pay Type" => "Tipo de pago no válido",
    	    "Invalid Tip Amount" => "Monto de propina no válido",
    	    "Invalid Tip Type" => "Tipo de propina no válido",
    	    "Invalid password format" => "Formato de contraseña no válido",
    	    "Item doesnot exists" => "El artículo no existe.",
    	    "Item exists" => "El artículo existe.",
    	    "Last Name is required" => "Se requiere el apellido.",
    	    "Lavu Card No is required" => "Se requiere el n.° de tarjeta de Lavu.",
    	    "Lavu Gift Card doesnot exists" => "La tarjeta de regalo de Lavu no existe.",
    	    "Lavu Gift Card is expired" => "La tarjeta de regalo de Lavu expiró.",
    	    "Lavu Gift Card is inactive" => "La tarjeta de regalo de Lavu está inactiva.",
    	    "Lavu Loyalty Number doesnot exists" => "El número de fidelidad de Lavu no existe.",
    	    "Lavu Loyalty Number is expired" => "El número de fidelidad de Lavu expiró.",
    	    "Lavu Loyalty Number is inactive" => "El número de fidelidad de Lavu está inactivo.",
    	    "Login Successful" => "Se inició sesión correctamente.",
    	    "Loyalty Discount Id is required" => "Se requiere la ID de descuento de fidelidad.",
    	    "Loyalty Points Discount Id is required" => "Se requiere la ID de descuento de puntos de fidelidad.",
    	    "Mail sent successfully to your registered Email Address" => "El correo se envió correctamente a su dirección de correo electrónico registrada.",
    	    "Meal Plan Payment Type is required" => "Se requiere el tipo de pago del plan de comida.",
    	    "Meal Plan failed" => "Se produjo un error con el plan de comida.",
    	    "New Password and Confirm New Password doesnot match" => "La contraseña nueva y la confirmación de la contraseña nueva no coinciden.",
    	    "New Password is required" => "Se requiere la contraseña nueva.",
    	    "No change in account details since last update" => "No se aplicaron cambios a los detalles de la cuenta desde la última actualización.",
    	    "No change in address details since last update" => "No se aplicaron cambios a los detalles de la dirección desde la última actualización.",
    	    "One or more of the items on this order are no longer available" => "Uno o más de los artículos de este pedido ya no están disponibles.",
    	    "Order Amount is required" => "Se requiere el monto del pedido.",
    	    "Order Amount should be greater than Minimum Delivery Amount" => "El monto del pedido debe ser superior al monto mínimo del envío.",
    	    "Order Id is required" => "Se requiere la ID del pedido.",
    	    "Order Items Details are required" => "Se requieren los detalles de los artículos del pedido.",
    	    "Order Type is required" => "Se requiere el tipo de pedido.",
    	    "Order is closed" => "El pedido está cerrado.",
    	    "Password and Confrim Password doesnot match" => "La contraseña y la confirmación de la contraseña no coinciden.",
    	    "Password is required" => "Se requiere la contraseña.",
    	    "Password reset link is expired" => "El enlace de restablecimiento de contraseña expiró.",
    	    "PayPal auth failed at server" => "Se produjo un error de servidor en la autenticación de PayPal.",
    	    "Payment Method Nonce is required" => "Se requiere el valor para la ocasión del método de pago.",
    	    "Registered Successfully" => "El registro se realizó correctamente.",
    	    "Some error occured while sending email" => "Se produjo un error al enviar el correo electrónico.",
    	    "Some error occured. Please try again" => "Se produjo un error. Inténtalo nuevamente.",
    	    "Some items are out of stock" => "Algunos artículos no cuentan con existencias.",
    	    "Telephone Number is required" => "Se requiere el número de teléfono.",
    	    "To process a payment with PayPal, the minimum order amount is $1" => "Para procesar un pago con PayPal, el monto mínimo del pedido es $1.",
    	    "Token Id is required" => "Se requiere la ID del token.",
    	    "User Id is required" => "Se requiere la ID del usuario.",
    	    "User details not found" => "No se encontraron los detalles del usuario.",
    	    "User doesnot have Meal Plan" => "El usuario no tiene plan de comida.",
    	    "Your funds have been successfully added" => "Tus fondos se han agregado correctamente.",
    	    "Your order failed due to" => "Se produjo un error con tu pedido debido a",
    	    "Your payment failed due to" => "Se produjo un error con tu pago debido a",
    	    "Your transaction has failed due to an internal server error for Order" => "Se produjo un error con tu transacción debido a un error de servidor interno para el pedido.",
    	    "Zero order amount. So cannot proceed to Lavu Gift Card" => "No se puede proceder a la tarjeta de regalo de Lavu ya que el monto del pedido equivale a cero.",
    	    "Zero order amount. So cannot proceed to Loyalty Points" => "No se puede proceder a los puntos de fidelidad ya que el monto del pedido equivale a cero.",
    	    "already exists" => "ya existe",
    	    "is not a valid email address" => "no es una dirección de correo electrónico válida"
    	);
    	$this->languageName = "Spanish (Mexico)";
    	$this->insertStmt = "INSERT INTO language_dictionary (pack_id, word_or_phrase, translation, _deleted, english_id, used_by, created_date) ";
    	$this->usedBy = "cp";
    	$this->auditInsertStmt = "INSERT INTO audit_log (id, id_type, table_name, info, updated_for, updated_by, created_date) ";
    	
    	
    	try {
    	    $checkLanguagePack = $this->fetchRow("SELECT `id` FROM `language_packs` where `english_name` = '".$this->languageName."' AND `_deleted` = 0");
    	    
    	    if(isset($checkLanguagePack['id'])) {
    	        $this->packId = $checkLanguagePack['id'];
    	    }
    	    
    	    if($this->hasTable('language_dictionary') && $this->packId != "") {
    	        
    	        foreach($this->msgs as $k => $v) {
    	            $ts = time();
    	            $utc_tz = new DateTimeZone("UTC");
    	            $datetime = new DateTime(NULL, $utc_tz);
    	            $datetime->setTimestamp($ts);
    	            $datetime_str = $datetime->format("Y-m-d H:i:s")."Z";
    	            $this->datetime = $datetime_str;
    	            
    	            // check if the word already exists in table
    	            $checkWordEntry = $this->fetchRow("SELECT `id` FROM `language_dictionary` where `word_or_phrase` = '".$k."' AND `english_id` = 0 AND `_deleted` = 0");
    	            
    	            if(isset($checkWordEntry['id'])) {
    	                // if word exists, check for the translation entry using language packid and if the entry exists update the translation or insert translation if not exists
    	                $checkTranslationEntry = $this->fetchRow("SELECT `id`, `translation` FROM `language_dictionary` where `word_or_phrase` = '".$k."' AND `pack_id` = '".$this->packId."' AND `english_id` = '".$checkWordEntry['id']."' AND `_deleted` = 0");
    	                
    	                if(isset($checkTranslationEntry['id'])) {
    	                    // backup record to audit_log table
    	                    $info = '{"id":"'.$checkTranslationEntry['id'].'","translation":"'.$checkTranslationEntry['translation'].'"}';
    	                    $this->execute($this->auditInsertStmt . " values ('".$checkTranslationEntry['id']."', 'id', 'language_dictionary', '".$info."', 'lp9040-update', 'migration', '".$this->datetime."')");
    	                    
    	                    // update record in language_dictionary
    	                    $this->execute("UPDATE `language_dictionary` set translation = CONVERT(_latin1'".$v."' USING utf8), modified_date = '".$this->datetime."' where `word_or_phrase` = '".$k."' AND `pack_id` = '".$this->packId."' AND `english_id` = '".$checkWordEntry['id']."' AND `_deleted` = 0");
    	                }
    	                else {
    	                    // insert record in language dictionary
    	                    $insertSuccess = $this->execute($this->insertStmt . " values ('".$this->packId."', '".$k."', CONVERT(_latin1'".$v."' USING utf8), '0', '".$checkWordEntry['id']."', '".$this->usedBy."', '".$this->datetime."')");
    	                    
    	                    if($insertSuccess) {
    	                        $checkNewWordTranslationEntry = $this->fetchRow("SELECT `id` FROM `language_dictionary` where `pack_id` = '".$this->packId."' AND `word_or_phrase` = '".$k."' AND `translation` = CONVERT(_latin1'".$v."' USING utf8) AND `english_id` = '".$checkWordEntry['id']."' AND `_deleted` = 0");
    	                        
    	                        if(isset($checkNewWordTranslationEntry['id'])) {
    	                            // add record to audit log table
    	                            $info = '{"id":"'.$checkNewWordTranslationEntry['id'].'","pack_id":"'.$this->packId.'","word_or_phrase":"'.$k.'","translation":"'.$v.'","english_id":"'.$checkWordEntry['id'].'"}';
    	                            $this->execute($this->auditInsertStmt . " values ('".$checkNewWordTranslationEntry['id']."', 'id', 'language_dictionary', CONVERT(_latin1'".$info."' USING utf8), 'lp9040-insert', 'migration', '".$this->datetime."')");
    	                        }
    	                    }
    	                    else {
    	                        throw new Exception("Migration Failed: Record not found");
    	                    }
    	                }
    	            }
    	            else {
    	                // if word doesnot exists, insert word entry for english packid, get the insert id and then insert word entry and translation for the other language packid
    	                $insertSuccess = $this->execute($this->insertStmt . " values ('1', '".$k."', '', '0', '0', '".$this->usedBy."', '".$this->datetime."')");
    	                
    	                if($insertSuccess) {
    	                    $checkNewWordEntry = $this->fetchRow("SELECT `id` FROM `language_dictionary` WHERE `word_or_phrase` = '".$k."' AND `english_id` = 0 AND `_deleted` = 0");
    	                    
    	                    if(isset($checkNewWordEntry['id'])) {
    	                        // add record to audit log table
    	                        $info = '{"id":"'.$checkNewWordEntry['id'].'","pack_id":"1","word_or_phrase":"'.$k.'","english_id":"0"}';
    	                        $this->execute($this->auditInsertStmt . " values ('".$checkNewWordEntry['id']."', 'id', 'language_dictionary', '".$info."', 'lp9040-insert', 'migration', '".$this->datetime."')");
    	                        
    	                        $insertSuccess = $this->execute($this->insertStmt . " values ('".$this->packId."', '".$k."', CONVERT(_latin1'".$v."' USING utf8), '0', '".$checkNewWordEntry['id']."', '".$this->usedBy."', '".$this->datetime."')");
    	                        
    	                        if($insertSuccess) {
    	                            $checkNewWordTranslationEntry = $this->fetchRow("SELECT `id` FROM `language_dictionary` where `pack_id` = '".$this->packId."' AND `word_or_phrase` = '".$k."' AND `translation` = CONVERT(_latin1'".$v."' USING utf8) AND `english_id` = '".$checkNewWordEntry['id']."' AND `_deleted` = 0");
    	                            
    	                            if(isset($checkNewWordTranslationEntry['id'])) {
    	                                // add record to audit log table
    	                                $info = '{"id":"'.$checkNewWordTranslationEntry['id'].'","pack_id":"'.$this->packId.'","word_or_phrase":"'.$k.'","translation":"'.$v.'","english_id":"'.$checkNewWordEntry['id'].'"}';
    	                                $this->execute($this->auditInsertStmt . " values ('".$checkNewWordTranslationEntry['id']."', 'id', 'language_dictionary', CONVERT(_latin1'".$info."' USING utf8), 'lp9040-insert', 'migration', '".$this->datetime."')");
    	                            }
    	                        }
    	                        else {
    	                            throw new Exception("Migration Failed: Record not found");
    	                        }
    	                    }
    	                    else {
    	                        throw new Exception("Migration Failed: Record not found");
    	                    }
    	                }
    	                else {
    	                    throw new Exception("Migration Failed: Failed to insert language dictionary record");
    	                }
    	            }
    	        }
    	    }
    	    else {
    	        throw new Exception("Migration Failed: Table `language_dictionary` does not exists or Language Pack ID not found");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    	    if($this->hasTable('audit_log')) {
    	        
    	        $result = $this->fetchAll("SELECT `id`, `info`, `updated_for` FROM `audit_log` where `updated_for` IN ('lp9040-insert', 'lp9040-update')");
    	        
    	        if(count($result) > 0) {
    	            
    	            foreach($result as $row){
    	                if(substr($row['updated_for'], -7) == "-insert") {
    	                    
    	                    // delete row from language dictionary table
    	                    $this->execute("DELETE FROM language_dictionary where id = '".$row['id']."'");
    	                    
    	                    // delete row from audit log table
    	                    $this->execute("DELETE FROM audit_log where id = '".$row['id']."' AND  updated_for = '".$row['updated_for']."'");
    	                }
    	                else if(substr($row['updated_for'], -7) == "-update") {
    	                    
    	                    $infoArr = json_decode($row['info'], TRUE);
    	                    $updateParams = array();
    	                    
    	                    foreach($infoArr as $key => $val) {
    	                        
    	                        if($key != "id") {
    	                            $updateParams[] = ' '.$key.'="'.$val.'" ';
    	                        }
    	                    }
    	                    
    	                    // rollback details of language dictionary table
    	                    $this->execute("UPDATE language_dictionary SET ".implode(', ', $updateParams)." WHERE id = '".$row['id']."'");
    	                    
    	                    // delete row from audit log table
    	                    $this->execute("DELETE FROM audit_log where id = '".$row['id']."' AND  updated_for = '".$row['updated_for']."'");
    	                }
    	            }
    	        }
    	    }
    	    else {
    	        throw new Exception("Rollback Failed: Table `audit_log` does not exists");
    	    }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
