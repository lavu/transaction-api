<?php
use Phinx\Migration\AbstractMigration;
class UpdateTableSideMaxIpadsLp12039 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            if ($this->hasTable('payment_status') && $this->table('payment_status')->hasColumn('tableside_max_ipads')) {
                #create a backup table to store/restore original value of custom_max_ipods for the respected dataname
                if (!$this->hasTable('tableside_ipads_migration_backup')) {
                    $this->execute("CREATE TABLE `tableside_ipads_migration_backup`(
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `dataname` varchar(255) NOT NULL,
                            `max_ipods`  int(11) NOT NULL,
                            `created_date` datetime NOT NULL,
                            PRIMARY KEY (`id`)
                        )ENGINE=InnoDB;
                    ");
                }
                #get active dataname list
                $existingDatanameList = array();
                $activeDatanames = $this->fetchAll("SELECT SCHEMA_NAME as dataname FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME LIKE 'poslavu%'");
                foreach ($activeDatanames as $activeDataname) {
                    $existingDatanameList[] = $activeDataname['dataname'];
                }
                #get location list
                $locationSql = "SELECT data_name AS dataname FROM `restaurants` WHERE `data_name` <> '' AND `data_name` <> 'main' AND `disabled` = 0";
                $locations = $this->fetchAll($locationSql);
                foreach ($locations as $location) {
                    $database = "poslavu_".$location['dataname']."_db";
                    if (!in_array($database,$existingDatanameList)) {
                        continue;
                    }
                    #check device table exist or not
                    $deviceTable = $this->fetchRow("SELECT COUNT(*) as count FROM information_schema. tables WHERE table_schema = '".$database."' AND table_name = 'devices'");
                    if ($deviceTable['count']) {                            
                        #get count of tableside, iphone and ipods
                        $devices = $this->fetchAll( "SELECT if(model like 'iPhone%', 'iphone', if(model like 'iPod%', 'ipod', if(model like 'iPad%', 'tableside', 'NA'))) AS device_model, count(distinct(UUID)) as device_count FROM `$database`.`devices`  WHERE lavu_admin = 0 AND (model LIKE 'iPhone%' OR model LIKE 'iPod%' OR (model LIKE 'iPad%' AND usage_type = 'tableside')) AND device_time >= (CURDATE() - INTERVAL 30 DAY ) GROUP BY model");
                        $deviceList = array(
                                        'tableside'=>'0',
                                        'iphone'   =>'0',
                                        'ipod'     =>'0',
                                    );
                        foreach ($devices as $device) {
                            $deviceList[$device['device_model']] = $device['device_count'];
                        }
                        if ($deviceList['tableside'] == 0 && $deviceList['iphone'] == 0 && $deviceList['ipod'] == 0) {
                            continue;
                        }
                        $createdDate = date("Y-m-d H:i:s");
                        $tablesideCount = $deviceList['tableside'];
                        $customMaxIpodTotal  = ($deviceList['iphone'] + $deviceList['ipod']);
                        #take backup of original custom_max_ipods count before updating new value for the respective dataname
                        $originalMaxIpods = $this->fetchRow("SELECT custom_max_ipods FROM `payment_status` WHERE dataname='".$location['dataname']."'");
                        $maxIpodCount = $originalMaxIpods['custom_max_ipods'] ? $originalMaxIpods['custom_max_ipods'] : 0;
                        $this->execute('INSERT into tableside_ipads_migration_backup VALUES(null, "'.$location['dataname'].'", "'.$maxIpodCount.'", "'.$createdDate.'")');
                        #update payment status
                        $this->execute("UPDATE payment_status SET tableside_max_ipads='".$tablesideCount."', custom_max_ipods = '".$customMaxIpodTotal."' WHERE dataname='".$location['dataname']."'");  
                        #check config table exist or not
                        $configTable = $this->fetchRow("SELECT COUNT(*) as count FROM information_schema. tables WHERE table_schema = '".$database."' AND table_name = 'config'");
                        #update location config
                        if ($configTable['count']) {
                            $this->execute("UPDATE `$database`.`config` SET value2='".$customMaxIpodTotal."', value4 = '".$tablesideCount."' WHERE `type` = 'system_setting' AND `setting` = 'package_info'"); 
                        }                             
                    }
                }                
             }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            if ($this->hasTable('payment_status') && $this->table('payment_status')->hasColumn('tableside_max_ipads')) {
                #get all ipods original count from backup table tableside_ipads_migration_backup
                if ($this->hasTable('tableside_ipads_migration_backup')) {
                    #get active dataname list
                    $existingDatanameList = array();
                    $activeDatanames = $this->fetchAll("SELECT SCHEMA_NAME as dataname FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME LIKE 'poslavu%'");
                    foreach ($activeDatanames as $activeDataname) {
                        $existingDatanameList[] = $activeDataname['dataname'];
                    }
                    #Read backup table
                    $locations = $this->fetchAll("SELECT * FROM tableside_ipads_migration_backup");
                    foreach ($locations as $location) {
                        $database = "poslavu_".$location['dataname']."_db";
                        if (!in_array($database,$existingDatanameList)) {
                            continue;
                        }
                        #restore payment_status table
                        $this->execute("UPDATE payment_status SET tableside_max_ipads='99', custom_max_ipods = '".$location['max_ipods']."' WHERE dataname='".$location['dataname']."'");
                        #check existance of location's config table
                        $configTable = $this->fetchRow("SELECT COUNT(*) as count FROM information_schema. tables WHERE table_schema = '".$database."' AND table_name = 'config'");
                        #restore location config
                        if ($configTable['count']) {
                            $this->execute("UPDATE `$database`.`config` SET value2='".$location['max_ipods']."', value4 = '99' WHERE `type` = 'system_setting' AND `setting` = 'package_info'"); 
                        }
                    }
                    $this->execute ( "DROP table `tableside_ipads_migration_backup`" );
                }                
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
