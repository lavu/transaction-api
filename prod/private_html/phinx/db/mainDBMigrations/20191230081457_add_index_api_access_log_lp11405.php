<?php


use Phinx\Migration\AbstractMigration;

class AddIndexApiAccessLogLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('api_access_log') && $this->table('api_access_log')->hasColumn('dataname')) {
                $row = $this->fetchRow('SHOW INDEX FROM api_access_log WHERE Key_name = "idx_dataname"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE api_access_log ADD INDEX `idx_dataname` (dataname)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('api_access_log') && $this->table('api_access_log')->hasColumn('dataname')) {
                $row = $this->fetchRow('SHOW INDEX FROM api_access_log WHERE Key_name = "idx_dataname"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE api_access_log DROP INDEX `idx_dataname`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
