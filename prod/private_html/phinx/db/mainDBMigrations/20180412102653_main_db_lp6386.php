<?php


use Phinx\Migration\AbstractMigration;

class MainDbLp6386 extends AbstractMigration
{
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		if ($this->hasTable('gateway_auth_tokens')) {
			$this->execute("DELETE FROM gateway_auth_tokens WHERE dataname='POSLAVU_QA_SQUARE_CREDENTIALS'");
			$this->execute("INSERT INTO gateway_auth_tokens (dataname, extension_id, merchant_id, token, token_expiration_date, permission_granted, location_name, location_id, token_generated_date, extra_info, _deleted) VALUES ('POSLAVU_QA_SQUARE_CREDENTIALS', 23, 'sq0idp-1kf2L4IXK9uDGTdje0P1Qg', 'sq0atp-KpbbXfcGrrXH-Jkm_fzOpw', '2017-10-21T12:26:24Z', 1, '', '1S0D29H0D8DEY', '2018-03-18 00:05:02', '', 0)");
		}

		if ($this->hasTable('auth_tokens')) {
			$this->execute("DELETE FROM auth_tokens WHERE dataname='POSLAVU_QA_SQUARE_SECRET_KEY'");
			$this->execute("INSERT INTO auth_tokens (dataname, extension_id, token, _deleted, token_type) VALUES ('POSLAVU_QA_SQUARE_SECRET_KEY', 23, 'sq0csp-Rme3-2hQoCXuPaZdPO7gg_7H9-CSi7qPs9hw_xkHgYQ', 0, 'secret')");

		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		if ($this->hasTable('gateway_auth_tokens')) {
			$this->execute("DELETE FROM gateway_auth_tokens WHERE dataname='POSLAVU_QA_SQUARE_CREDENTIALS'");
		}

		if ($this->hasTable('auth_tokens')) {
			$this->execute("DELETE FROM auth_tokens WHERE dataname='POSLAVU_QA_SQUARE_SECRET_KEY'");
		}
	}
}
