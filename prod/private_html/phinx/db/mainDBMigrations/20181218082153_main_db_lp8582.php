<?php


use Phinx\Migration\AbstractMigration;

class MainDbLp8582 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
		$getReportId = $this->fetchRow("SELECT `id` FROM `reports` WHERE `title` = 'Server Sales w/Tips'");
		if (isset($getReportId['id'])) {
				$this->execute("ALTER TABLE `report_parts` MODIFY COLUMN `prop3` TEXT NOT NULL COMMENT 'This query is modifying the column definition due to insufficient char length'");
				$this->execute('UPDATE `report_parts` SET `prop3` = "(SELECT `order_id`, SUM(IF(`split_check_details`.`cash` > 0, IF(CAST(`split_check_details`.`cash` AS DECIMAL(10,2)) >= CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), CAST(`split_check_details`.`cash` AS DECIMAL(10,2))), \"0.00\")) AS sag_cash, SUM(IF(CAST(`split_check_details`.`cash` AS DECIMAL(10,2)) > CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), \"0.00\", IF(CAST(`split_check_details`.`card` AS DECIMAL(10,2)) = 0, \"0.00\", IF(CAST(`split_check_details`.`alt_paid` + `split_check_details`.`gift_certificate` AS DECIMAL(10,2)) < CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), IF((`split_check_details`.`cash`) > 0, IF(CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)) > CAST(`split_check_details`.`cash` AS DECIMAL(10,2)), CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)) - CAST(`split_check_details`.`cash` AS DECIMAL(10,2)), \"0.00\"), IF((`split_check_details`.`card`) > 0, IF(CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)) > CAST(`split_check_details`.`card` AS DECIMAL(10,2)), CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)) - CAST(`split_check_details`.`card` AS DECIMAL(10,2)), `split_check_details`.`gratuity`), \"0.00\")), IF(CAST((`split_check_details`.`cash` + `split_check_details`.`card`) AS DECIMAL(10,2)) = 0, \"0.00\", IF(CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)) >= CAST((`split_check_details`.`cash` + `split_check_details`.`card`) AS DECIMAL(10,2)), CAST(`split_check_details`.`card` AS DECIMAL(10,2)), IF(CAST(`split_check_details`.`alt_paid` + `split_check_details`.`gift_certificate` AS DECIMAL(10,2)) > 0, IF(CAST(`split_check_details`.`cash` AS DECIMAL(10,2)) > 0, IF(CAST(`split_check_details`.`card` AS DECIMAL(10,2)) < CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), IF(CAST(`split_check_details`.`cash` AS DECIMAL(10,2)) < CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), CAST(`split_check_details`.`gratuity` - `split_check_details`.`cash` AS DECIMAL(10,2)), CAST(`split_check_details`.`gratuity` - `split_check_details`.`card` AS DECIMAL(10,2))), CAST(`split_check_details`.`gratuity` - `split_check_details`.`cash` AS DECIMAL(10,2))), CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2))), \"0.00\"))))))) as sag_card, SUM(IF((`split_check_details`.`cash` + `split_check_details`.`card`) < `split_check_details`.`gratuity`, CAST(`split_check_details`.`gratuity` - `split_check_details`.`cash` - `split_check_details`.`card` AS DECIMAL(10,2)), IF(CAST(`split_check_details`.`alt_paid` + `split_check_details`.`gift_certificate` AS DECIMAL(10,2)) <= CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), IF(CAST(`split_check_details`.`cash` AS DECIMAL(10,2)) > 0, IF(CAST((`split_check_details`.`cash` + `split_check_details`.`card`) AS DECIMAL(10,2)) <= CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), CAST(`split_check_details`.`gratuity` - (`split_check_details`.`alt_paid` + `split_check_details`.`gift_certificate`) AS DECIMAL(10,2)), \"0.00\"), IF(CAST(`split_check_details`.`card` AS DECIMAL(10,2)) > 0, IF(CAST((`split_check_details`.`cash` + `split_check_details`.`card`) AS DECIMAL(10,2)) <= CAST(`split_check_details`.`gratuity` AS DECIMAL(10,2)), CAST(`split_check_details`.`gratuity` - (`split_check_details`.`alt_paid` + `split_check_details`.`gift_certificate`) AS DECIMAL(10,2)), \"0.00\"), \"0.00\")), \"0.00\"))) AS sag_other FROM " WHERE `type` = "join" AND `prop1` = "split_check_details" AND `reportid` = '.$getReportId['id']);
				$row1 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `fieldname` = "ag_cash" AND `reportid` = '.$getReportId['id']);
				if (isset($row1['id'])) {
					$this->execute('UPDATE `report_parts` SET `opargs` = "SUM(`split_check_details`.`sag_cash`)" WHERE `fieldname` = "ag_cash" AND `reportid` = '.$getReportId['id']);
				}
				$row2 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `fieldname` = "ag_card" AND `reportid` = '.$getReportId['id']);
				if (isset($row2['id'])) {
					$this->execute('UPDATE `report_parts` SET `opargs` = "SUM(`split_check_details`.`sag_card`)" WHERE `fieldname` = "ag_card" AND `reportid` = '.$getReportId['id']);
				}
				$row3 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `fieldname` = "ag_other" AND `reportid` = '.$getReportId['id']);
				if (!isset($row3['id'])) {
					$updatets = date("YmdHis");
					$this->execute("INSERT INTO `report_parts` (`reportid`, `type`, `prop1`, `prop2`, `prop3`, `prop4`, `tablename`, `fieldname`, `operation`, `opargs`, `filter`, `hide`, `sum`, `graph`, `updatets`, `label`, `branch`, `_order`,  `monetary`, `align`) VALUES ('".$getReportId['id']."', 'select', '', '', '' ,'', 'orders', 'ag_other', 'custom', 'SUM(`split_check_details`.`sag_other`)', '', 0, 1, 0, '".$updatets."', 'AG Other', '0', 16, 1, 'left')");
				}
				$row4 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `label` = "Total Card Tips" AND `reportid` = '.$getReportId['id']);
				if (isset($row4['id'])) {
					$this->execute('UPDATE `report_parts` SET `opargs` = "SUM(`orders`.`card_gratuity` + `split_check_details`.`sag_card`)" WHERE `label` = "Total Card Tips" AND `reportid` = '.$getReportId['id']);
				}
		}
	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
		$getReportId = $this->fetchRow("SELECT `id` FROM `reports` WHERE `title` = 'Server Sales w/Tips'");
		if (isset($getReportId['id'])) {
				$this->execute('UPDATE `report_parts` SET `prop3` = "(SELECT `order_id`, SUM(`gratuity`) AS `sgratuity`, SUM(IF (cash > \"0.00\", gratuity, \"0\")) AS sag_cash, SUM(IF (card > \"0.00\", gratuity, \"0\")) AS sag_card  FROM " WHERE `type` = "join" AND `prop1` = "split_check_details" AND `reportid` = '.$getReportId['id']);
				$this->execute("ALTER TABLE `report_parts` MODIFY COLUMN `prop3` VARCHAR(255) NOT NULL");
				$row1 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `fieldname` = "ag_cash" AND `reportid` = '.$getReportId['id']);
				if (isset($row1['id'])) {
					$this->execute('UPDATE `report_parts` SET `opargs` = "SUM(IF(`orders`.`no_of_checks`>1, `split_check_details`.`sag_cash`, * ))" WHERE `fieldname` = "ag_cash" AND `reportid` = '.$getReportId['id']);
				}
				$row2 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `fieldname` = "ag_card" AND `reportid` = '.$getReportId['id']);
				if (isset($row2['id'])) {
					$this->execute('UPDATE `report_parts` SET `opargs` = "SUM(IF(`orders`.`no_of_checks`>1, `split_check_details`.`sag_card`, * ))" WHERE `fieldname` = "ag_card" AND `reportid` = '.$getReportId['id']);
				}
				$row3 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `fieldname` = "ag_other" AND `reportid` = '.$getReportId['id']);
				if (isset($row3['id'])) {
					$this->execute('DELETE FROM `report_parts` WHERE `fieldname` = "ag_other" AND `reportid` = '.$getReportId['id']);
				}
				$row4 = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `label` = "Total Card Tips" AND `reportid` = '.$getReportId['id']);
				if (isset($row4['id'])) {
					$this->execute('UPDATE `report_parts` SET `opargs` = "SUM(`orders`.`card_gratuity` + `orders`.`ag_card`)" WHERE `label` = "Total Card Tips" AND `reportid` = '.$getReportId['id']);
				}
		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
