<?php


use Phinx\Migration\AbstractMigration;

class MainDbAddLanguageDictionaryForNorwayLp8855 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			$englishWord = "The two receipt maximum per order or check has already been reached. Lavu is unable to print another copy. Closing order.";
			$translationWord = "De to kvitteringsnivaene per ordre eller sjekk er allerede nadd. Lavu kan ikke skrive ut en annen kopi. Avsluttende rekkefolge.";
			$getEngId = $this->fetchRow('SELECT `id` FROM `language_packs` WHERE `language` = "English" AND `english_name` = "English" AND `_deleted` = 0');
			if (isset($getEngId['id'])) {
				$this->execute("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `translation`, `_deleted`, `english_id`, `used_by`, `created_date`, `modified_date`, `backtrace`, `tag`) VALUES ('".$getEngId['id']."', '".$englishWord."', '', 0, 0 ,'', '', '', '', '')");
				$getNorwId = $this->fetchRow('SELECT `id` FROM `language_packs` WHERE `language` = "Norsk" AND `english_name` = "Norwegian" AND `_deleted` = 0');
				if (isset($getNorwId['id'])) {
					$getLastEngId = $this->fetchRow("SELECT `id` FROM language_dictionary WHERE `pack_id` = '".$getEngId['id']."' AND `word_or_phrase` = '".$englishWord."'");
					if (isset($getLastEngId)) {
						$this->execute("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `translation`, `_deleted`, `english_id`, `used_by`, `created_date`, `modified_date`, `backtrace`, `tag`) VALUES ('".$getNorwId['id']."', '".$englishWord."', '".$translationWord."', 0, '".$getLastEngId['id']."' ,'', '', '', '', '')");
						$this->execute("UPDATE `language_dictionary` SET `english_id` = '".$getLastEngId['id']."' WHERE `word_or_phrase` = '".$englishWord."'");
					}
				}
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			$englishWord = "The two receipt maximum per order or check has already been reached. Lavu is unable to print another copy. Closing order.";
			$getEngId = $this->fetchRow('SELECT `id` FROM `language_packs` WHERE `language` = "English" AND `english_name` = "English" AND `_deleted` = 0');
			if (isset($getEngId['id'])) {
				$this->execute("DELETE FROM `language_dictionary` WHERE `pack_id` = '".$getEngId['id']."' AND `word_or_phrase` = '".$englishWord."'");
			}
			$getNorwId = $this->fetchRow('SELECT `id` FROM `language_packs` WHERE `language` = "Norsk" AND `english_name` = "Norwegian" AND `_deleted` = 0');
			if (isset($getNorwId['id'])) {
				$this->execute("DELETE FROM `language_dictionary` WHERE `pack_id` = '".$getNorwId['id']."' AND `word_or_phrase` = '".$englishWord."'");
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
