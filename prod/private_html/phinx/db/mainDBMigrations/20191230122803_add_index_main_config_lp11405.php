<?php


use Phinx\Migration\AbstractMigration;

class AddIndexMainConfigLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('main_config') && $this->table('main_config')->hasColumn('setting')) {
                $row = $this->fetchRow('SHOW INDEX FROM main_config WHERE Key_name = "idx_setting"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE main_config ADD INDEX `idx_setting` (`setting`)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('main_config') && $this->table('main_config')->hasColumn('setting')) {
                $row = $this->fetchRow('SHOW INDEX FROM main_config WHERE Key_name = "idx_setting"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE main_config DROP INDEX `idx_setting`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
