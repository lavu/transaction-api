<?php
use Phinx\Migration\AbstractMigration;

class GlReportV2Lp5867 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        // INSERT INTO [POSLAVU MAIN DB].`reports_v2`
        if ($this->hasTable ( 'reports_v2' )) {
           
            $rows = [
                    'selection_column' => '',
                    'special_code' => '//custom:general_ledger',
                    'selection' => 'Preset Date Ranges',
                    'table' => 'orders',
                    'title' => 'General Ledger',
                    'name' => 'general_ledger',
                    'orderby' => '',
                    'select' => '',
                    'groupby' => '',
                    'where' => '',
                    'join' => '',
                    '_order' => NULL,
                    '_deleted' => 0,
                    'required_modules' => '',
                    'custom_sister_list' => 'general_ledger'
                ];
            $table = $this->table ( 'reports_v2' );
            $table->insert ( $rows )->saveData ();
        }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
        // remove records
        if ($this->hasTable( 'reports_v2' )) {
            $this->execute( "DELETE FROM `reports_v2` WHERE `title` = 'General Ledger' ");
        }
    }
}