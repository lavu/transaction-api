<?php


use Phinx\Migration\AbstractMigration;

class AddIndexPaymentProfileLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('payment_profile') && $this->table('payment_profile')->hasColumn('subscriptionid')) {
                $row = $this->fetchRow('SHOW INDEX FROM payment_profile WHERE Key_name = "idx_subscriptionid"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE payment_profile ADD INDEX `idx_subscriptionid` (`subscriptionid`)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('payment_profile') && $this->table('payment_profile')->hasColumn('subscriptionid')) {
                $row = $this->fetchRow('SHOW INDEX FROM payment_profile WHERE Key_name = "idx_subscriptionid"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE payment_profile DROP INDEX `idx_subscriptionid`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
