<?php

use Phinx\Migration\AbstractMigration;

class TablesideIpadsLimitLp10857 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('payment_status')) {
                if (!$this->table('payment_status')->hasColumn('tableside_max_ipads')) {
                    $this->execute("ALTER TABLE `payment_status` ADD COLUMN `tableside_max_ipads` int(11) NOT NULL AFTER `custom_max_ipads`");
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('payment_status')) {
                if ($this->table('payment_status')->hasColumn('tableside_max_ipads')) {
                    $this->execute('ALTER TABLE `payment_status` DROP COLUMN `tableside_max_ipads`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
