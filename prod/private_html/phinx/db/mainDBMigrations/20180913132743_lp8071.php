<?php


use Phinx\Migration\AbstractMigration;

class Lp8071 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reports_v2')) {
    			$this->execute("UPDATE `reports_v2` SET `_deleted` = 0 WHERE `title` ='Tip Sharing'");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reports_v2')) {
    			$this->execute("UPDATE `reports_v2` SET `_deleted` = 1 WHERE `title` ='Tip Sharing'");
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
