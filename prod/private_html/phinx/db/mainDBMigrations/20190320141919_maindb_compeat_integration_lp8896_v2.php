<?php


use Phinx\Migration\AbstractMigration;

class MaindbCompeatIntegrationLp8896V2 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
		//Have to delete existing rec by calling down(). As below query had been already executed in previous script
            	//20190214023843_maindb_compeat_integration_lp8896.php
		$this->down();
    		/* Write migration code here */
            $result=$this->fetchAll('SELECT dataname FROM auth_tokens WHERE dataname="noble__co_" AND token_type="ftp_compeat"');
            $dataname=$result[0]['dataname'];
            if (!$dataname) {
                $this->execute('INSERT INTO auth_tokens (dataname, extension_id, token, `_deleted`, token_type) VALUES (\'noble__co_\', null, \'{"ftp":{"username":"JohnnysDiner_FTP","password":"johNd1n84&8*13","company_id":"3106","location_id":"0410"},"api":{"company_id":32,"location_id":1,"api_user":606366,"api_token":"1c7fcdbf-24cf-4609-8bb9-37721d3ef735"}}\', 0, \'api_compeat\');');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            /* Write rollback code here */
            $this->execute('DELETE FROM auth_tokens WHERE dataname="noble__co_" AND token_type="ftp_compeat"');
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
