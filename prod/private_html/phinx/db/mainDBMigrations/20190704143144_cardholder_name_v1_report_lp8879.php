<?php


use Phinx\Migration\AbstractMigration;

class CardholderNameV1ReportLp8879 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable ( 'report_parts' )) {
    			
    			$row = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `reportid` = "38" and  `label` = "Info" and `fieldname` = "card_desc" ');
    			if (isset($row['id'])) {
    				$this->execute('UPDATE `report_parts` SET `opargs` = "IF(`cc_transactions`.`info` = \'\',IF(`cc_transactions`.`card_holder_name`= \'\' ,IF(`cc_transactions`.`swipe_grade` = \'\',`cc_transactions`.`card_desc`, concat(IF(`cc_transactions`.`swipe_grade` = \'H\',\'Manual entry\',\'Invalid name\'),IF(`cc_transactions`.`card_desc`=\'\', \'\', \'-\'), `cc_transactions`.`card_desc`)), concat(`cc_transactions`.`card_holder_name`, IF(`cc_transactions`.`card_desc`=\'\', \'\', \'-\'),`cc_transactions`.`card_desc`)), concat(`cc_transactions`.`info`, IF(`cc_transactions`.`card_desc` = SUBSTRING(`cc_transactions`.`info`, -4), \'\', concat(\' - \', `cc_transactions`.`card_desc`))))" WHERE `reportid` = "38" and  `label` = "Info" and `fieldname` = "card_desc"');
    				
    			}
    			
    		}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable ( 'report_parts' )) {
                $row = $this->fetchRow('SELECT `id` FROM `report_parts` WHERE `reportid` = "38" and  `label` = "Info" and `fieldname` = "card_desc" ');
                if (isset($row['id'])) {
                    $this->execute("UPDATE `report_parts` SET `opargs` = 'IF(`cc_transactions`.`info` = \'\', *, concat(`cc_transactions`.`info`, IF(`cc_transactions`.`card_desc` = SUBSTRING(`cc_transactions`.`info`, -4), \'\', concat(\' - \', `cc_transactions`.`card_desc`))))' WHERE `reportid` = '38' and  `label` = 'Info' and `fieldname` = 'card_desc'");
                }
            }
    		
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
