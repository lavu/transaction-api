<?php


use Phinx\Migration\AbstractMigration;

class AddIndexReportsLp11405 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reports') && $this->table('reports')->hasColumn('title')) {
                $row = $this->fetchRow('SHOW INDEX FROM reports WHERE Key_name = "idx_title"');
                if (!$row['Key_name']) {
                    $this->execute('ALTER TABLE reports ADD INDEX `idx_title` (`title`)');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('reports') && $this->table('reports')->hasColumn('title')) {
                $row = $this->fetchRow('SHOW INDEX FROM reports WHERE Key_name = "idx_title"');
                if ($row['Key_name']) {
                    $this->execute('ALTER TABLE reports DROP INDEX `idx_title`');
                }
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
