<?php


use Phinx\Migration\AbstractMigration;

class DeleteGiftCardsLp7066 extends AbstractMigration
{
	public $status;
	/**
	 * up() Method to migrate.
	 */
	public function up()
	{
		$this->status = true;
		try {
			if(!$this->hasTable('lavu_gift_cards_backup')){
				 
				$this->execute("CREATE TABLE `lavu_gift_cards_backup` (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
							  `chainid` varchar(255) NOT NULL DEFAULT '',
							  `dataname` varchar(255) NOT NULL DEFAULT '',
							  `name` varchar(255) NOT NULL DEFAULT '',
							  `balance` varchar(255) NOT NULL DEFAULT '',
							  `expires` varchar(255) NOT NULL DEFAULT '',
							  `created_datetime` varchar(255) NOT NULL DEFAULT '',
							  `history` text NOT NULL,
							  `lavu_gift_id` int(11) NOT NULL,
							  `points` varchar(255) NOT NULL DEFAULT '',
							  `inactive` int(11) NOT NULL,
							  `created_ts` int(11) NOT NULL,
							  `expires_ts` int(11) NOT NULL,
							  PRIMARY KEY (`id`),
							  KEY `chainid` (`chainid`),
							  KEY `dataname` (`dataname`),
							  KEY `name` (`name`)
                            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1");
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}

	/**
	 * down() Method to rollback.
	 */
	public function down()
	{
		$this->status = true;
		try {
			if ($this->hasTable('lavu_gift_cards_backup')) {
				$this->execute('DROP TABLE `lavu_gift_cards_backup`');
			}
		}
		catch (PDOException $exception) {
			$this->status = false;
			$this->logException($this->getName(), $exception->getMessage());
		}
	}
}
