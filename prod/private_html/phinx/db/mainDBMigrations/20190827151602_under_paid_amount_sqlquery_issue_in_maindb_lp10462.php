<?php


use Phinx\Migration\AbstractMigration;

class UnderPaidAmountSqlqueryIssueInMaindbLp10462 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;
        try {
            if ($this->hasTable ( 'reports_v2' )) {
                //updating Underpaid Orders row.
               $underpaidStmt = $this->query("SELECT `reports_v2`.`select` FROM `reports_v2` where `reports_v2`.`title` = 'Underpaid Orders' ");
                if($underpaidStmt == true) {
                    $this->execute("UPDATE `reports_v2` SET `select` = 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#kiosk_order,#lavu_togo_order,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\')[_sep_]#server[_sep_]#closer_by_name[_sep_]#underpaid_amount' where `reports_v2`.`title` = 'Underpaid Orders' ");
                } else {
                    throw new Exception("Migration Failed: Row with `title` value `Underpaid Orders` does not exist in table `reports_v2`");
                }
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
        $this->status = true;
        try {
            if ($this->hasTable ( 'reports_v2' )) {
                //updating Underpaid Orders row with existing data.
                $underpaidStmt = $this->query("SELECT `reports_v2`.`select` FROM `reports_v2` where `reports_v2`.`title` = 'Underpaid Orders' ");
                if($underpaidStmt == true) {
                    $this->execute("UPDATE `reports_v2` SET `select` = 'sister(\'#menu_category_name,#super_group,#contents_name,#order_server,#order_cashier,#rev_center,#meal_period,#order_register,#order_tag,#order_id,#kiosk_order,#lavu_togo_order,#content_options,#closed_by_weekday,#item_added_by_hour,#order_reg_group,#order_time,#combo_sales,#overpaid_order,#underpaid_order\'[_sep_]#server[_sep_]#closer_by_name[_sep_]#underpaid_amount)' where `reports_v2`.`title` = 'Underpaid Orders' ");
                }
            }
        }
        catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
