<?php


use Phinx\Migration\AbstractMigration;

class SquareReconciliationAutomateLp5712 extends AbstractMigration
{
    /**
     * up() Method to migrate.
     */
    public function up()
    {
	    if ($this->hasTable('gateway_sync_request')) {
		    if (!$this->table('gateway_sync_request')->hasColumn('location_id')) {
			    $this->execute("ALTER TABLE gateway_sync_request ADD COLUMN location_id varchar(200) NOT NULL AFTER `transaction_id`");
		    }
	    }
    }
    
    /**
     * down() Method to rollback.
     */
    public function down()
    {
	    if ($this->hasTable('gateway_sync_request')) {
		    if ($this->table('gateway_sync_request')->hasColumn('location_id')) {
			    $this->execute('ALTER TABLE `gateway_sync_request` DROP COLUMN `location_id`');
		    }
	    }
    }
}
