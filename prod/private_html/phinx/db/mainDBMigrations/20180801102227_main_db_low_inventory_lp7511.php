<?php


use Phinx\Migration\AbstractMigration;

class MainDbLowInventoryLp7511 extends AbstractMigration
{
    public $status;
    /**
     * up() Method to migrate.
     */
    public function up()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('reports_v2') == true && $this->table('reports_v2')->hasColumn('selection') == true) {
                $this->execute("UPDATE `reports_v2` SET `selection`='Preset Date Ranges' WHERE `id`=51");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            if ($this->hasTable('reports_v2') == true && $this->table('reports_v2')->hasColumn('selection') == true) {
                $this->execute("UPDATE `reports_v2` SET `selection`='' WHERE `id`=51");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
