<?php

use Phinx\Migration\AbstractMigration;

class MainDbResellerProductListLp8877 extends AbstractMigration
{
    public $status;
    public $bkpTable="reseller_products_bkp_lp8877";
    public $newCategories=array('Payments', 'POS Bundles');
    public $csvFile = '/home/poslavu/private_html/phinx/data/reseller-prods-lp8877.csv';

    /**
     * up() Method to migrate.
     */
    public function up()
    {
        $this->status = true;

    	try {

            //If csv file for new prod list is missing, throw exception
            if (!file_exists($this->csvFile)) {
                throw New Exception('Migration Failed: CSV file `'.$this->csvFile.'` is missing');
            }

            /* Write migration code here */
            //take table backup
            if ($this->hasTable('reseller_products')) {
                if (!$this->hasTable($this->bkpTable)) {
                    $this->execute('CREATE TABLE IF NOT EXISTS '.$this->bkpTable.' AS SELECT * FROM reseller_products');
                } else {
                    $this->execute('TRUNCATE TABLE '.$this->bkpTable);
                    $this->execute('INSERT INTO '.$this->bkpTable.' (reseller_product_id, name, description, info, category_id, created_date, update_date, _deleted) SELECT reseller_product_id, name, description, info, category_id, created_date, update_date, _deleted FROM reseller_products');
                }

                $result1=$this->fetchAll('SELECT count(1) as cnt FROM reseller_products');
                $result2=$this->fetchAll('SELECT count(1) as cnt FROM '.$this->bkpTable);

                if ($result1[0]['cnt'] != $result2[0]['cnt']) {
                    throw new Exception("Migration failed, Backup table reseller_products_bkp does not match count with reseller_products");
                } else {
                    $this->execute('TRUNCATE TABLE reseller_products');
                }
            } else {
                throw new Exception("Migration failed: table 'reseller_products' does not exist");
            }

            foreach ($this->newCategories as $val) {
                $inCategories[] = '"'.$val.'"';
            }
            $categories = array();
            $result=$this->fetchAll('SELECT reseller_product_category_id, name FROM reseller_product_category WHERE name IN ('.implode(',',$inCategories).')');
            foreach($result as $row) {
                $newCategoryExist[]=$row['name'];
            }

            foreach($this->newCategories as $val) {
                if(!in_array($val, $newCategoryExist)) {
                    $this->execute('INSERT INTO reseller_product_category VALUES ("","'.$val.'",NOW(),NOW(),0)');
                }
            }

            $result=$this->fetchAll('SELECT reseller_product_category_id, name FROM reseller_product_category');
            foreach ($result as $rec) {
                $categories[$rec['name']] = $rec['reseller_product_category_id'];
            }

            $categories['Card Readers: Cable'] = $categories['Card Readers and Cables'];
            $categories['Cash Drawer: Accessory'] = $categories['Cash Drawer and Accessory'];
            $categories['Computer: Accessory'] = $categories['Computer and Accessory'];
            $categories['Printer'] = $categories['Printers'];

            //parse CSV
            $fp=fopen($this->csvFile,'r');

            fgetcsv($fp,1000,",");
            $count=0;
            while (($r=fgetcsv($fp,1000,","))!=FALSE) {
                $count++;
                $r[3]=str_replace(',','',$r[3]);
                $r[4]=str_replace(',','',$r[4]);
                $resellerCost=($r[3]=='(blank)')?'':$r[3];
                $salePrice=($r[4]=='(blank)')?'':$r[4];
                $info='^^'.$resellerCost.'^'.$salePrice;
                $prodName=addslashes($r[0]);
                $prodDesc=addslashes($r[1]);
                $catId = $categories[trim($r[2])];

                $q='INSERT INTO reseller_products(name,description,info,category_id,created_date,_deleted) VALUES(
                "'.$prodName.'","'.$prodDesc.'","'.$info.'",'.$catId.',NOW(),0);';
                $this->execute($q);
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
            /* Write rollback code here */
            if ($this->hasTable('reseller_products')) {
                if ($this->hasTable($this->bkpTable)) {
                    $this->execute('TRUNCATE TABLE reseller_products');
                    $this->execute('INSERT INTO reseller_products (reseller_product_id, name, description, info, category_id, created_date, update_date, _deleted) SELECT reseller_product_id, name, description, info, category_id, created_date, update_date, _deleted FROM '.$this->bkpTable);
                }

                $result1=$this->fetchAll('SELECT count(1) as cnt FROM reseller_products');
                $result2=$this->fetchAll('SELECT count(1) as cnt FROM '.$this->bkpTable);

                if ($result1[0]['cnt'] != $result2[0]['cnt']) {
                    throw new Exception("Migration failed, Backup table reseller_products_bkp does not match count with reseller_products");
                }

                foreach ($this->newCategories as $val) {
                    $inCategories[] = '"'.$val.'"';
                }

                $this->execute('DELETE FROM reseller_product_category WHERE name IN ('.implode(',',$inCategories).')');
            } else {
                throw new Exception("Migration failed: table 'reseller_products' does not exist");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
