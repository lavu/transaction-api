<?php


use Phinx\Migration\AbstractMigration;

class MoveIsNewControlPanelActiveToMainDbCp31308 extends AbstractMigration
{
		public $status;
		
		public $locTable = 'restaurants';
		public $activeFlag = 'is_new_control_panel_active';
    /**
     * up() Method to migrate.
     */

    public function up()
    {
			$this->status = true;

    	try {
    		/**
		     * Purpose:- We will store isNewControlPanelActive flag in main_db for fastest access
		     */
				if ($this->hasTable($this->locTable) && !$this->table($this->locTable)->hasColumn($this->activeFlag)) {
					$this->execute('ALTER TABLE `'.$this->locTable.'`
													ADD COLUMN `'.$this->activeFlag.'`
													boolean default false');
				}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
				if ($this->hasTable($this->locTable) && $this->table($this->locTable)->hasColumn($this->activeFlag)) {
					$this->execute('ALTER TABLE `'.$this->locTable.'`
													DROP COLUMN `'.$this->activeFlag.'`');
				}
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}
