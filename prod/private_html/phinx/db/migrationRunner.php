<?php
if((isset($argv[1])&&$argv[1]=='help')||count($argv)<3){
        showHelpMessage();
        exit(0);
}

$command = isset($argv[1]) ? strtolower(trim($argv[1])) : "";
$envToDeploy = isset($argv[2]) ? trim($argv[2]) : "";
$target = isset($argv[3]) ? trim($argv[3]) :NULL;
$csvpath = isset($argv[4]) ? trim($argv[4]) :NULL;

if( $command!="" && $command=="rollback" && is_null($target)){
    showRollBackMessage();
    exit(0);
}

if($command!="" && $envToDeploy!=""){
    require("/home/poslavu/private_html/phinx/vendor/autoload.php");
    set_time_limit(0);
    $connection = mysqli_connect(my_poslavu_host(), my_poslavu_username(), my_poslavu_password()) or die ("Database Connection error");
    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    if($csvpath !=NULL){
        	$flag = true;
        	if (($handle = fopen($csvpath, "r")) !== FALSE) {
        	    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        	        if($flag) { $flag = false; continue; } // remove the first row of the csv file (the first row may contain tile of the column)
        	        $num = count($data);
        	        for ($c=0; $c < $num; $c++) {
                        //THIS IS TO AVOID EXECUTING QUERIES FOR MAIN DB JUST IN CASE IF MAIN DB WAS SPECIFIED IN CSV LIST
                        if(in_array(strtolower($data[$c]), array('main'))) {
                            continue;
                        }
        	            $dblist[] = "'poslavu_".trim($data[$c]). "_db'";
        	        }
        	    }
            fclose($handle);
        	}
        	$list = implode (array_values($dblist), ",");
        	$databases = mysqli_query($connection, "SELECT schema_name FROM information_schema.schemata WHERE schema_name IN ($list) ");
    }
    else{
         $databases = mysqli_query($connection, "SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT IN (  'information_schema', 
         'reports_main_db',
                                                                                                                            'mysql', 
                                                                                                                            'performance_schema', 
                                                                                                                            'lavutogo',
                                                                                                                            'test',
                                                                                                                            'poslavu_MAINREST_db', 
                                                                                                                            'poslavu_MAIN_db') ");
    }
    
    while ($dataBaseRecord = mysqli_fetch_row($databases)){
        $app = new \Phinx\Console\PhinxApplication();
        $_SERVER['PHINX_DBNAME'] = $dataBaseRecord[0];
        
        if($target!=null && $command == "migrate"){
            $databaseWithPhinxLogTable="`".$dataBaseRecord[0]."`.`phinxlog`";
            $checkTargetExist = mysqli_query($connection, "SELECT version FROM ".$databaseWithPhinxLogTable." WHERE version=".$target);
            if(mysqli_num_rows($checkTargetExist)==1){
                showNoMigrationApplicationMessage($target, $dataBaseRecord[0]);
                continue;
            }
        }
        
        $wrap = new \Phinx\Wrapper\TextWrapper($app, array(
            'parser' => 'yaml'
        ));
        
        switch($command){
            case "migrate":
                try{
                    $response = $wrap->getMigrate($envToDeploy,$target);
                }
                catch (Exception $e) {
                    showExceptionMessage($dataBaseRecord[0], $e->getMessage());
                }
                break;
            case "rollback":
                try{
                    $response = $wrap->getRollback($envToDeploy,$target);
                }
                catch (Exception $e) {
                    showExceptionMessage($dataBaseRecord[0], $e->getMessage());
                }
                break;
        }

        print_r($response);
        unset($wrap);
        unset($app);
        unset($response);
    }
    mysqli_close($connection);
}

function showHelpMessage(){
    echo "===============================================================================================\n\r";
    echo "Command Example: php db/migrationRunner.php <COMMAND> <ENVIRONMENT> <MIGRATION> <CSV_FILE_PATH>\n\r";
    echo "Example: php db/migrationRunner.php migrate development 123456 /home/poslavu/test.csv\n\r";
    echo "Example: php db/migrationRunner.php rollback development 123456 /home/poslavu/test.csv\n\r";
    echo "===============================================================================================\n\r";
}

function showRollBackMessage(){
    echo "==============================================================================================\n\r";
    echo "Command Example: php db/migrationRunner.php rollback <ENVIRONMENT> <MIGRATION> <CSV_FILE_PATH>\n\r";
    echo "Example: php db/migrationRunner.php rollback development 123456 /home/poslavu/test.csv\n\r";
    echo "==============================================================================================\n\r";
}


function showNoMigrationApplicationMessage($target, $dataBaseRecord){
    echo "============================================================================================\n\r";
    echo "Migration -> $target is already applied on Database -> ".$dataBaseRecord."\n\r";
    echo "============================================================================================\n\r\n\r";
}

function showExceptionMessage($dataName, $exceptionMessage){
    echo "============================================================================================\n\r";
    echo "Migration failed for $dataName due to exception ".$exceptionMessage."\n\r";
    echo "============================================================================================\n\r\n\r";
}
?>