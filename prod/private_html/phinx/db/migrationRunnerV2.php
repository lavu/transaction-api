<?php
if((isset($argv[1])&&$argv[1]=='help')||count($argv)<3){
        showHelpMessage();
        exit(0);
}

$command = isset($argv[1]) ? strtolower(trim($argv[1])) : "";
$envToDeploy = isset($argv[2]) ? trim($argv[2]) : "";
$target = isset($argv[3]) ? trim($argv[3]) :NULL;
$arg4 = isset($argv[4]) ? trim($argv[4]) :NULL;
$dbsToSelectArr = explode(",",$arg4);

//LP-8869
require("/home/poslavu/private_html/phinx/vendor/autoload.php");
use Symfony\Component\Yaml\Yaml;

if ( count ($dbsToSelectArr) == 0 ) {
    showInputParamsError();
    exit(0);
}

if( $command!="" && $command=="rollback" && is_null($target)){
    showRollBackMessage();
    exit(0);
}

if($command!="" && $envToDeploy!=""){
    set_time_limit(0);
    //LP-8869
    $yml = Yaml::parseFile('/home/poslavu/private_html/phinx/phinx.yml');
    $db = $yml['environments'][$envToDeploy];
    $connection = mysqli_connect($db['host'], $db['user'], $db['pass']) or die ("Database Connection error");

    if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    foreach ( $dbsToSelectArr as $key => $dbInitial ) {
        $databases = mysqli_query($connection, "SELECT schema_name
                                                   FROM information_schema.schemata
                                                   WHERE schema_name NOT IN (  'information_schema',
                                                                                'mysql',
                                                                                'reports_main_db',
                                                                                'performance_schema',
                                                                                'lavutogo',
                                                                                'test',
                                                                                'poslavu_MAINREST_db',
                                                                                'poslavu_MAIN_db')
                                                        AND schema_name LIKE \"poslavu_{$dbInitial}%\" order by schema_name ASC");
        if ( mysqli_num_rows($databases) > 0 ) {
            while ($dataBaseRecord = mysqli_fetch_row($databases)){
                $app = new \Phinx\Console\PhinxApplication();
                $_SERVER['PHINX_DBNAME'] = $dataBaseRecord[0];

                if($target!=null && $command == "migrate"){
                    $databaseWithPhinxLogTable="`".$dataBaseRecord[0]."`.`phinxlog`";
                    $checkTargetExist = mysqli_query($connection, "SELECT version FROM ".$databaseWithPhinxLogTable." WHERE version=".$target);
                    if(mysqli_num_rows($checkTargetExist)==1){
                        showNoMigrationApplicationMessage($target, $dataBaseRecord[0]);
                        continue;
                    }
                }

                $wrap = new \Phinx\Wrapper\TextWrapper($app, array(
                    'parser' => 'yaml'
                ));


                switch($command){
                    case "migrate":
                        try{
                            $response = $wrap->getMigrate($envToDeploy,$target);
                        }
                        catch (Exception $e) {
                            showExceptionMessage($dataBaseRecord[0], $e->getMessage());
                        }
                        break;
                    case "rollback":
                        try{
                            $response = $wrap->getRollback($envToDeploy,$target);
                        }
                        catch (Exception $e) {
                            showExceptionMessage($dataBaseRecord[0], $e->getMessage());
                        }
                        break;
                }

                print_r($response);
                unset($wrap);
                unset($app);
                unset($response);
            }
        } else {
            echo "===============================================================================================\n\r";
            echo "There is no dataname found to start with : {$dbInitial}\n\r";
            echo "===============================================================================================\n\r";
        }
    }
    mysqli_close($connection);
}

function showHelpMessage(){
    echo "===============================================================================================\n\r";
    echo "Command Example: php db/migrationRunner.php <COMMAND> <ENVIRONMENT> <MIGRATION> <CSV_FILE_PATH>\n\r";
    echo "Example: php db/migrationRunner.php migrate development 123456 /home/poslavu/test.csv\n\r";
    echo "Example: php db/migrationRunner.php rollback development 123456 /home/poslavu/test.csv\n\r";
    echo "===============================================================================================\n\r";
}

function showRollBackMessage(){
    echo "==============================================================================================\n\r";
    echo "Command Example: php db/migrationRunner.php rollback <ENVIRONMENT> <MIGRATION> <CSV_FILE_PATH>\n\r";
    echo "Example: php db/migrationRunner.php rollback development 123456 /home/poslavu/test.csv\n\r";
    echo "==============================================================================================\n\r";
}


function showNoMigrationApplicationMessage($target, $dataBaseRecord){
    echo "============================================================================================\n\r";
    echo "Migration -> $target is already applied on Database -> ".$dataBaseRecord."\n\r";
    echo "============================================================================================\n\r\n\r";
}

function showExceptionMessage($dataName, $exceptionMessage){
    echo "============================================================================================\n\r";
    echo "Migration failed for $dataName due to exception ".$exceptionMessage."\n\r";
    echo "============================================================================================\n\r\n\r";
}

function showInputParamsError(){
    echo "============================================================================================\n\r";
    echo "Migration Input Params Error\n\r";
    echo "============================================================================================\n\r\n\r";
}
?>