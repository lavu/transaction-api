<?php
use Phinx\Migration\AbstractMigration;

class UsersDeliverAddress extends AbstractMigration
{
    public $status;
    /**
	 * up() Method to migrate.
	 */
    public function up()
    {
    	$this->status = true;
    	try {
    		if (!$this->hasTable ( 'users_address' )) {
                $this->execute("CREATE TABLE `users_address` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `user_id` int(11) NOT NULL,
                  `name` varchar(100) DEFAULT NULL,
                  `address_1` text,
                  `address_2` text,
                  `country` varchar(100) DEFAULT NULL,
                  `state` varchar(100) DEFAULT NULL,
                  `city` varchar(100) DEFAULT NULL,
                  `zipcode` varchar(100) DEFAULT NULL,
                  `is_primary` enum('0','1') DEFAULT '0',
                  `created_date` datetime DEFAULT NULL,
                  `last_modified_date` datetime DEFAULT NULL,
                  `_deleted` enum('0','1') DEFAULT '0',
                  PRIMARY KEY (`id`),
                  KEY `users_delivery_address_user_id_IDX` (`user_id`) USING BTREE
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1
                ");
            }
            if ($this->hasTable ( 'users_address' ) && $this->hasTable ( 'users_info') ) {
                 $this->execute("INSERT INTO `users_address` (user_id,address_1, state, city, zipcode, _deleted) ( SELECT user_id, address, state, city, zipcode,_deleted FROM `users_info` WHERE `user_id` IN ( SELECT id FROM `users`) )");
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }

    /**
     * down() Method to rollback.
     */
    public function down()
    {
    	$this->status = true;
    	try {
    		if ($this->hasTable('users_address')) {
                $this->execute('DROP TABLE `users_address`');
            }
    	}
    	catch (PDOException $exception) {
            $this->status = false;
            $this->logException($this->getName(), $exception->getMessage());
        }
    }
}