<?php
require_once ("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
ini_set("display_errors",0);
error_reporting(0);

#To get missing migration of location's DB
function checkMissingMigrations($data_name = "MAIN") {
    $database = "poslavu_".$data_name."_db";
    $csvData = "";

    if($data_name == "MAIN"){
        $directory = '/home/poslavu/private_html/phinx/db/mainDBMigrations';
    }else{
        $directory = '/home/poslavu/private_html/phinx/db/migrations';
    }
   
    $migrationFiles = array();
    $allMigrationFiles = array();
     
    #get all migration script files
    $migrationScript = array_diff(scandir($directory), array('..', '.'));
    foreach($migrationScript as $filename){
        $scriptfile = explode("_",$filename);
        if(count($scriptfile)==1)
            continue;
            
        $migrationFiles[] = $scriptfile[0];
        $allMigrationFiles[$scriptfile[0]] = $filename;
    }
  
    #get all existing migration from phinxlog
    $phinxLogMigrations = array();
    $queryObj = mlavu_query("SELECT version FROM ".$database.".phinxlog");
    while($row = mysqli_fetch_assoc($queryObj)){
        $phinxLogMigrations[] = $row['version'];
    }

    #get missing migration
    $missingMigrations = array_diff($migrationFiles,$phinxLogMigrations);
    if(!empty($missingMigrations)){
        foreach($missingMigrations as $key=>$version){
            $csvData .= "\n".$data_name.",".$allMigrationFiles[$version];
        }
    }
    return $csvData;
}

#Find all active customers or dataname from restaurants
$query = "SELECT id, company_name, data_name FROM `restaurants` WHERE  `data_name` <> '' AND `data_name` <> 'main' AND `disabled` = 0";
$result = mlavu_query($query);

$delimiter = "\t";
$filename = "missing_migration_list_" .getenv('ENVIRONMENT')."_". date('Y-m-d') . ".csv";
$file = fopen('php://memory', 'w');
$csvData = '';
$csvData .= 'Dataname, Migration Name';
$csvData .= checkMissingMigrations(); #get missing migration of main db
while($row = mysqli_fetch_assoc($result)){
    $database = "poslavu_".$row['data_name']."_db";
    if(!databaseExists($database)){
        continue;
    }
    $csvData .= checkMissingMigrations($row['data_name']); #get missing migration of location's db
}

if(!is_dir("/home/poslavu/public_html/admin/phinxdata")){
    mkdir("/home/poslavu/public_html/admin/phinxdata", 0775);
}

$file = '/home/poslavu/public_html/admin/phinxdata/'.$filename ;
file_put_contents($file, $csvData);
echo "*** download this file from: https://".$_SERVER['INGRESS_HOSTNAMES']."/phinxdata/".$filename."  ***";
?>

