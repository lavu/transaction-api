#!/bin/bash
# Our custom function
runMigration() {
  php /home/poslavu/private_html/phinx/db/migrationRunnerV2.php $1 $2 $3 $4 
  sleep 1
}
# For loop 3 times
for i in {1..3}
do
	if [ $i -eq 1 ]; then
		runMigration $1 $2 $3 "0,1,2,3,4,5,6,7,8,9,\_" & # Run migrations in the background
	elif [ $i -eq 2 ]; then
		runMigration $1 $2 $3 "a,b,c,d,e,f,g,h,i,j,k,l,m" & # Run migrations in the background
	elif [ $i -eq 3 ]; then
		runMigration $1 $2 $3 "n,o,p,q,r,s,t,u,v,w,x,y,z" & # Run migrations in the background
	else
        	echo "Somthing went wrong!!"
	fi
done

## would wait until those are completed
## before displaying all done message
wait
echo "All migrations done!"