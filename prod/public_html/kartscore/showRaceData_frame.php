<?php

	ini_set("display_errors",1);
	
	session_start();
	/* First Thing to do is connect to the DB */
	date_default_timezone_set('America/New_York');

	require_once(__DIR__."/connect_to_racer_data.php");

	/*
		determine which data set to display
	*/
	$viewType='All Time Champs';
	$location=$_GET['dataname'];
	function initialize(){
		global $viewType;
		global $location;
		$lastMon = date('Y-m-d', strtotime('last monday'));
		$data='';
		
		if( $location==''){
			echo "An error has occurred. No location was passed";
			exit;
		}
		
		if(isset($_GET['best_of'])){
			$viewType="Best Race Times of ". date("F");;
			$time_start = microtime(true);
			$data=getBestOf($location);
			$time_end = microtime(true);
			$time = $time_end - $time_start;
			//echo"Time to get the data: ". $time;
		}else if(isset($_GET['best_of_week'])){
			$data=getBestOfWeek($location);	
			$viewType="Best Race Times This Week (starting ".$lastMon.")";
		}else if(isset($_GET['racer_data'])){
			$viewType="Choose Racer";
			$data=getRacers($_GET['racer_data'], $location);
		}else if(isset($_GET['racer_data_specific'])){
			$viewType="Individual Stats";
			$data=get_individual_stats($_GET['racer_data_specific'], $location);
		}else if(isset($_GET['kids_race'])){
			$viewType="Best Kids Race Times This Month";
			$data=getkidsRace($location);	
		}else if(isset($_GET['kids_race_week'])){
			$viewType="Best Kids Race Times This Week (starting ".$lastMon.")";
			$data=getkidsRaceWeek($location);	
		}else{
			$data=getBestOf($location);
			$viewType="Best Race Times This Month";
		}
		//$time_start = microtime(true);
		$html= displayData($data, $location, $viewType);
		//$time_end = microtime(true);
		//$time = $time_end - $time_start;
		//echo "DISPLAY DATA TOOK: ". $time;
		return $html;
	}
	
	function displayErrorMessage(){
		echo "error";
	}
	function getBestOfWeek($location){
		$results=array();
		
		$lastMon  = date('Y-m-d', strtotime('last monday'));
		$dataname = mysqli_real_escape_string($location);
		$lastMon  = mysqli_real_escape_string($lastMon);

		$query= $conn->query("read","select * from `races` where `date` >='".$lastMon."' and `dataname`='".$dataname."'  order by `ms_end`*1 ");

		//echo "The number of results is: ". mysqli_num_rows($query);
		return perform_query($query);
	}
	function getBestOf($location){
	
		$dataname= mysqli_real_escape_string($location);
		$monthNumber= date('m');
		$query= $conn->query("read","select * from `races` where `dataname`='$dataname' and YEAR(`date`) = YEAR(CURDATE()) AND MONTH(`date`) = '$monthNumber' order by `ms_end`*1 asc ");
		
		return perform_query($query);
	}
	function getkidsRace($location){
	
		$results=array();
		$dataname= mysqli_real_escape_string($location);
		$monthNumber= date('m');
		$query= $conn->query("read","select * from `races` where `dataname`='$dataname' and YEAR(`date`) = YEAR(CURDATE()) AND MONTH(`date`) = '$monthNumber' and (`type` LIKE '%Youth%' OR `type` LIKE '%kid%' OR `type` LIKE '%Junior%') order by `ms_end`*1 asc ");
		return perform_query($query);
	}
	function getkidsRaceWeek($location){
		$dataname= mysqli_real_escape_string($location);
		$lastMon = date('Y-m-d', strtotime('last monday'));

		$query= $conn->query("read","select * from `races` where `date` >='".$lastMon."' and `dataname`='".$dataname."' and (`type` LIKE '%Youth%' OR `type` LIKE '%kid%' OR `type` LIKE '%Junior%') order by `ms_end`*1 ");
		return perform_query($query);
	}
	function get_individual_stats($id, $location){
		
		$dataname= mysqli_real_escape_string($location);
		$monthNumber= date('m');
		$query= $conn->query("read","select * from `races` where `dataname`='$dataname' and YEAR(`date`) = YEAR(CURDATE()) order by `ms_end`*1 asc ");
		$lap_times=array();
		$sorted=false;
		if( mysqli_num_rows($query)){
			while ($read= mysqli_fetch_assoc($query)){
			
				$race_times= json_decode($read['race_info']);
				foreach($race_times as $cust_id=>$lap_time){
					if( ($lap_time * 1) > 10.0 && $cust_id== $id)
						$lap_times[$lap_time]=  array("cust_id"=>$cust_id, "event_id"=>$read['eventid'],"date"=>$read['date'],"type"=>$read['type'], "lap_time"=>$lap_time);
				}

			}
			//echo print_r($lap_times,1);
			uksort($lap_times, "cmp");
			return $lap_times;

		}else{
			debugging("No Races returned \n" );
			return false;
		}	
		
	}
	
	function getRacerInfo($cust_id, $location){

		$results=array();
		$dataname= mysqli_real_escape_string($location);
		$cust_id = mysqli_real_escape_string($cust_id);
		$query=$conn->query("read","SELECT * FROM `kart_customers` WHERE `customer_id` = '".$cust_id."' AND `dataname`='".$dataname."' limit 1");	
		//echo "SELECT * FROM `kart_customers` WHERE `customer_id` = '".$cust_id."' AND `dataname`='".$dataname."' limit 1 <br>";		
		if( mysqli_num_rows($query))
			return mysqli_fetch_assoc($query);
		else
			return "error: no user found with cust id: ". $cust_id. "  and dataname: ".$dataname;
	}
	function getRacers($searchTerm, $location){
		$results=array();
		$searchTerm= mysqli_real_escape_string($searchTerm);
		$dataname= mysqli_real_escape_string($location);
		$query = $conn->query("read","SELECT * FROM `kart_customers` WHERE `dataname` = '".$location."' AND (`email` LIKE '%".$searchTerm."%' OR `racer_name` LIKE '%".$searchTerm."%' OR CONCAT (`f_name`, ' ', `l_name`) LIKE '%".$searchTerm."%')  limit 100");

		while ($read= mysqli_fetch_assoc($query)){
			$id= mysqli_real_escape_string($read['customer_id']);
			
			$read['cust_id']= $id;
			$results[]= $read;	
		}
		//echo print_r($results,1);
		return $results;
	}	
	function perform_query($query){
		$lap_times=array();
		$sorted=false;
		if( mysqli_num_rows($query)){
			while ($read= mysqli_fetch_assoc($query)){
			
				$race_times= json_decode($read['race_info']);
				foreach($race_times as $cust_id=>$lap_time){
					if( ($lap_time * 1) > 10.0)
						$lap_times[$lap_time]=  array("cust_id"=>$cust_id, "event_id"=>$read['eventid'],"date"=>$read['date'],"type"=>$read['type'], "lap_time"=>$lap_time);
				}

			}
			uksort($lap_times, "cmp");
			return $lap_times;

		}else{
			debugging("No Races returned \n" );
			return false;
		}	

	}
	function cmp($a, $b){
		if($a==$b){
			return 0;
		}
	    return ($a < $b) ? -1 : 1;	
	}
	function displayData($data, $location, $viewType){
		$html='';
		$counter=0;
		$cust_id_array=array();
		
		if( $data=='error')
			return 'An Error has occurred. Please try again later. ';
		foreach ($data as $info){
			
			if($counter > 99){
				//echo "Time to get racer info: ". $total_get_racer_infoTime;
				return $html;			
			}
			if( $viewType != "Individual Stats"){
				if(isset($cust_id_array[$info['cust_id']])){
					continue;
				}else
					$cust_id_array[$info['cust_id']]=1;
				
				
	
			}
			$racerInfo= getRacerInfo($info['cust_id'], $location);
			if ($viewType != "Choose Racer") {
				$html.='
					<tr class="dataRow" onclick ="window.location=\'getRaceSheet.php?eventid='.$info['event_id'].'&trackid=[trackid]&mode=results&loc_id=1&location='.$location.'&racer_name='.$racerInfo['racer_name'].'&originalid='.$info['cust_id'].'\' ">
						<td>
							'.++$counter.'
						</td>
						<td>	
							'.$racerInfo['racer_name'].'
						</td>
						<td>	
							'.$racerInfo['f_name'].'
						</td>
						<td>	
							'.$racerInfo['l_name'].'
						</td>
						<td>	
							'.$info['type'].'
						</td>
						<td>	
							'.$info['date'].'
						</td>
						<td>	
							'.round(($info['lap_time']*1),3).'
						</td>
						
					</tr>
				';
			} else {
				$html.='
					<tr class="dataRow" onclick ="window.location=\'showRaceData.php?dataname='.$location.'&racer_data_specific='.$info['cust_id'].'\'">
						<td>	
							'.$info['racer_name'].'
						</td>
						<td>	
							'.$info['f_name'].'
						</td>
						<td>	
							'.$info['l_name'].'
						</td>
					</tr>
				';
			}
		}
		
		return $html;
	}
	function debugging($var){
		echo "<pre><div class='debugging'  style='color:white; '>". print_r($var,true)."</div></pre>"; 
	}
	function quicksort($array) {
	    if(count($array) < 2) return $array;
	 
	    $left = $right = array();
	 
	    reset($array);
	    $pivot_key = key($array);
	    $pivot = array_shift($array);
	 
	    foreach($array as $k => $v) {
	        if($v < $pivot)
	            $left[$k] = $v;
	        else
	            $right[$k] = $v;
	    }
	 
	    return array_merge(quicksort($left), array($pivot_key => $pivot), quicksort($right));
	}
	
	
?>

<?php $html= initialize();?>
<html>
	<head>
		<title>
			P2R Race Data
		</title>
	
		<link rel="stylesheet" type="text/css" href="./css/new_styles.css">
	</head>
	
	
	<body>
		<div id= 'bodyContent'>
			<div id= 'header'>
				
					
				
				</img>
			
			</div>
			<div id='viewType'>Top Times</div>
			<div id='divider'>
	
				<span id='race_types'>
					<?php if(isset($_GET['best_of'])){
	
						}else if(isset($_GET['best_of_week'])){
						
						}else if(isset($_GET['racer_data'])){
				
						}else if(isset($_GET['racer_data_specific'])){
					
						}else if(isset($_GET['kids_race'])){
		
						}else if(isset($_GET['kids_race_week'])){
	
						}else{
				
						}
					?>
				<a class='links <?php echo isset($_GET['best_of_week']) ? "selected" : false; ?>' href='http://scores.polepositionraceway.com/showRaceData_frame.php?best_of_week&dataname=<?php echo $_GET['dataname'];?>'>   &nbsp;&nbsp; Adult Weekly  &nbsp;&nbsp;</a>
				<span class='divider'> &nbsp; </span>		
				<a class='links  <?php echo isset($_GET['best_of']) ? "selected" : false; ?>' href='http://scores.polepositionraceway.com/showRaceData_frame.php?best_of&dataname=<?php echo $_GET['dataname'];?>'>        &nbsp;&nbsp; Adult Monthly &nbsp;&nbsp;</a>
				<span class='divider'> &nbsp; </span>
				<a class='links  <?php echo isset($_GET['kids_race']) ? "selected" : false; ?>' href='http://scores.polepositionraceway.com/showRaceData_frame.php?kids_race&dataname=<?php echo $_GET['dataname'];?>'>      &nbsp;&nbsp; Kid Weekly    &nbsp;&nbsp;</a>
				<span class='divider'> &nbsp; </span>
				<a class='links  <?php echo isset($_GET['kids_race_week']) ? "selected" : false; ?>' href='http://scores.polepositionraceway.com/showRaceData_frame.php?kids_race_week&dataname=<?php echo $_GET['dataname'];?>'> &nbsp;&nbsp; Kid Monthly   &nbsp;&nbsp;</a>
				</span>
				<span id= "search">
				<input type="text" id="searchTerm" placeholder="Racer name/email"> 
				<input class='search_btn' type= "submit" value="Search" onclick= "
					var text= document.getElementById('searchTerm').value; 
					window.location='http://scores.polepositionraceway.com/showRaceData.php?dataname=<?php echo $location;?>&racer_data='+text+'' ">
				</span>
			</div>
			
			
			<table id='race_data' border='1'>
				
				<?php
					if ($viewType != "Choose Racer") {
						echo "<tr id='header_data'>\n";
						echo "					<th> Place		 </th>\n";
						echo "					<th> Racer Name  </th>\n";
						echo "					<th> First Name  </th>\n";
						echo "					<th> Last Name   </th>\n";
						echo "					<th> Race Type   </th>\n";
						echo "					<th> Race Date   </th>\n";
						echo "					<th> Time 		 </th>\n";
						echo "				</tr>\n";
					} else {
						echo "<tr id='header_data'>\n";
						echo "					<th> Racer Name  </th>\n";
						echo "					<th> First Name  </th>\n";
						echo "					<th> Last Name   </th>\n";
						echo "				</tr>\n";
					}
				?>
					<?php echo $html;?>
					
			</table>
		</div>
	</body>

</html>
