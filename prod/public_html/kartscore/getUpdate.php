<?php

	/********************************
		
		Hey! you weirdo! lookin at this code! 
		
		be careful! 
		
		this gets called from a cURL request
		therefore if you output anythign eg. ECHO you will overwrite the last transfer time
		when you do that you mess everything up. so watch yourself. 
		
	*********************************/
	date_default_timezone_set('America/New_York');
	if(isset($_GET['debugging'])){
	
		ini_set("log_errors", 1);
		ini_set("error_log", "/var/log/httpd/getUpdate_error_log");
	}
	
	$postData=array();
	$dataname=$_POST['dataname'];
	$lastTime='';
	//$dataname= mysqli_real_escape_string($dataname);
	if(count($_POST)==0){
		echo "error : No post variables sent. ";
		exit;
	}else if(!isset($_POST['dataname'])){
		echo "error : Dataname not sent. ";	
		error_log("error : Dataname not sent.");
		exit();	
	}else if( !isset($_POST['json']) || $_POST['json']=='' ){
		error_log('json not sent.');
		exit();	
	
	}else
		$postData=json_decode($_POST['json']);
	
	//error_log(print_r($postData,true));
	//error_log("json". print_r($_POST['json'],true));

	require_once(__DIR__."/connect_to_racer_data.php");

	foreach ($postData as $dataSet){

		handleRaceDetails($dataSet->race_details);
		
		handleRacerDetails($dataSet->details);
		
		$dataSet->event->details= $dataSet->details;
		
		handleEventData($dataSet->event);
		
		$lastTime= max(date("Y-m-d H:i:s", strtotime($dataSet->event->ms_end)), date("Y-m-d H:i:s", strtotime($lastTime)));

	}	
	
	mysqli_close($con);	
	//This is really the return value. just fyi 
	echo $lastTime;
	
	function handleRacerDetails($data){  //This function does the insert for lk_customers
		 
		foreach($data as $racerData){
			$racer_details=$racerData->racer_details;
			
			if(is_object($racer_details) && $racer_details->racer_name!='')
				createQueryParts($racerData->racer_details, "lk_customers");
		}
	}
	function handleRaceDetails($data){ //This function does the insert for the race details: lk_race_results;
		//if( isset($_GET['debugging']))
			 //error_log("This came from handleRaceDetails:  \n ".print_r($data, true), 0);

		//echo " Race Details: " . print_r($data, true);
		foreach($data as $lapData)
			createQueryParts($lapData, "lk_race_results");
	
	}
	function handleEventData($data){ 	//This function does the insert into lk_events

		$raceDetails=array();
		foreach($data->details as $details){			//The reason for this is because we need to save the lap data in one cell. 
			
			$individualRaceDetails['number']=$details->number;
			$individualRaceDetails['laps']=$details->laps;
			
			$raceDetails[$details->custid]=$individualRaceDetails;
		}
		$data->details=json_encode($raceDetails);
	
		createQueryParts($data, "lk_events");
	}
	
	function createQueryParts($dataSet, $table){
		
		global $dataname;
		$raceInfoKeysString='';		
		$raceInfoDataString='';
		
		foreach($dataSet as $key => $data){
			
			$escapedKey = mysqli_real_escape_string($key);
			$raceInfoKeysString.="`".$escapedKey."`, ";
			$raceInfoDataString.="'".$data."', ";
		
		}
		
		if(isset($dataSet->id) && $table=='lk_race_results'){
			$id=mysqli_real_escape_string($dataSet->id);
			$conn->query("write","delete from `lk_race_scores` where `id` = '".$id."' and `dataname`='".$dataname."'");
				
		}
		
		$raceInfoKeysString.='`dataname`';//the reason i do this is so that i dont have to strip off the last comma
		$raceInfoDataString.="'".$_POST['dataname']."'";
		$raceInfoQuery="INSERT INTO	`".$table."` (".$raceInfoKeysString.") VALUES (".$raceInfoDataString.")";
		
		$conn->query("write",$raceInfoQuery);
		
	}
		
?>
