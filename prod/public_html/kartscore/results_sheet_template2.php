
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kart Lavu : Print Results</title>
<style type="text/css">
<!--
.titletext {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: 700;
	font-size: 16px;
	color: #000;
}

.pagetext {
	font-family:  Arial, Helvetica, sans-serif;
	font-weight: 500;
	font-size: 13px;
	color: #666;
}
.pagetext_red {
	font-family:  Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 13px;
	color: #ee2d32;
}
.pagetext_blue {
	font-family:  Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 13px;
	color: #3653a4;
}

.helptext {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: 700;
	font-size: 11px;
	color: #000;
}
.titlesub {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: 500;
	font-size: 14px;
	font-style:italic;
	color: #666;
}

-->
</style>
</head>

<body>
<!--startsheet-->
<table width="1020" height="660" border="0" align="center" cellpadding="0" cellspacing="0" id="pagetext">
  <tr>
    <td height="51" colspan="2" align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="960" height="555" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="460" align="left" valign="top"><table width="460" height="555" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="132" align="left" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td align="left" valign="top"><table width="460" height="139" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="60" align="left" valign="middle"  class="titletext">&nbsp;</td>
                  <td width="400" align="left" valign="middle"  class="titletext"><table width="400" border="0" cellpadding="0" cellspacing="0" class="pagetext">
                    <tr>
                      <td align="left" class="titletext">Driver Name</td>
					  <td class="titletext">[fullname] <font style='font-size:10px'>([racer name])</font></td>
                      <td width="142" rowspan="3"><img src="images/resultsheet/number[placed].png" width="142" height="50"></td>
                    </tr>
                    <tr>
                      <td width="100" align="right">Date:</td>
                      <td>[date]</td>
                    </tr>
                    <tr>
                      <td align="right">Visits:</td>
                      <td>[visits]</td>
                    </tr>
                    <tr>
                      <td align="right">Rank:</td>
                      <td colspan="2">[rank]</td>
                    </tr>
                    <tr>
                      <td align="right">Race Name:</td>
                      <td colspan="2">[race name]</td>
                    </tr>
                    <tr>
                      <td align="right">Race Format:</td>
                      <td colspan="2">[race format]</td>
                    </tr>
                    <tr>
                      <td align="right">Track Name:</td>
                      <td colspan="2">[track name]</td>
                    </tr>
                  </table></td>
                </tr>
                </table></td>
            </tr>
            <tr>
              <td height="156" align="left" valign="top"><table width="460" height="156" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="22" colspan="4" align="left" valign="bottom" background="resize_image.php?image=images/resultsheet/flagline.png&ratio=R" class="titletext"><table width="400" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="60"></td>
                      <td align="left" valign="bottom" class="titletext">Race Results</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="60" align="left" valign="top"></td>
                  <td width="196" align="left" valign="top"><table width="196" border="0" cellpadding="0" cellspacing="0" class="pagetext" >
                    <tr>
                      <td width="12" align="left" valign="middle" class="pagetext_red">[p1]</td>
                      <td width="148" align="left" valign="middle" class="pagetext_red">[racer1]</td>
                      <td width="36" align="center" valign="middle" class="pagetext_red">[rtime1]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle" class="pagetext_blue">[p2]</td>
                      <td align="left" valign="middle" class="pagetext_blue">[racer2]</td>
                      <td align="center" valign="middle" class="pagetext_blue">[rtime2]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p3]</td>
                      <td align="left" valign="middle">[racer3]</td>
                      <td align="center" valign="middle">[rtime3]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p4]</td>
                      <td align="left" valign="middle">[racer4]</td>
                      <td align="center" valign="middle">[rtime4]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p5]</td>
                      <td align="left" valign="middle">[racer5]</td>
                      <td align="center" valign="middle">[rtime5]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p6]</td>
                      <td align="left" valign="middle">[racer6]</td>
                      <td align="center" valign="middle">[rtime6]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p7]</td>
                      <td align="left" valign="middle">[racer7]</td>
                      <td align="center" valign="middle">[rtime7]</td>
                    </tr>
                  </table></td>
                  <td width="8" align="left" valign="top"></td>
                  <td width="196" align="left" valign="top"><table width="196" border="0" cellpadding="0" cellspacing="0" class="pagetext" >
                    <tr>
                      <td width="16">[p8]</td>
                      <td width="144" align="left" valign="middle">[racer8]</td>
                      <td width="36" align="center" valign="middle">[rtime8]</td>
                    </tr>
                    <tr>
                      <td width="16">[p9]</td>
                      <td align="left" valign="middle">[racer9]</td>
                      <td width="36" align="center" valign="middle">[rtime9]</td>
                    </tr>
                    <tr>
                      <td width="16">[p10]</td>
                      <td align="left" valign="middle">[racer10]</td>
                      <td width="36" align="center" valign="middle">[rtime10]</td>
                    </tr>
                    <tr>
                      <td width="16">[p11]</td>
                      <td align="left" valign="middle">[racer11]</td>
                      <td width="36" align="center" valign="middle">[rtime11]</td>
                    </tr>
                    <tr>
                      <td width="16">[p12]</td>
                      <td align="left" valign="middle">[racer12]</td>
                      <td width="36" align="center" valign="middle">[rtime12]</td>
                    </tr>
                    <tr>
                      <td width="16">[p13]</td>
                      <td align="left" valign="middle">[racer13]</td>
                      <td width="36" align="center" valign="middle">[rtime13]</td>
                    </tr>
                    <tr>
                      <td width="16">[p14]</td>
                      <td align="left" valign="middle">[racer14]</td>
                      <td width="36" align="center" valign="middle">[rtime14]</td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="128" align="left" valign="top"><table width="460" height="128" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="22" colspan="4" align="left" valign="bottom" background="resize_image.php?image=images/resultsheet/flagline.png&ratio=R" class="titletext"><table width="400" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="60">&nbsp;</td>
                      <td class="titletext">Best Lap This Week</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="60" align="left" valign="top">&nbsp;</td>
                  <td width="196" align="left" valign="middle"><table width="196" border="0" cellpadding="0" cellspacing="0" class="pagetext">
                    <tr>
                      <td width="20">[bl1]</td>
                      <td>[blracer1]</td>
                      <td align="right" valign="middle">[bltime1]</td>
                    </tr>
                    <tr>
                      <td width="20">[bl2]</td>
                      <td>[blracer2]</td>
                      <td align="right" valign="middle">[bltime2]</td>
                    </tr>
                    <tr>
                      <td width="20">[bl3]</td>
                      <td>[blracer3]</td>
                      <td align="right" valign="middle">[bltime3]</td>
                    </tr>
                    <tr>
                      <td width="20">[bl4]</td>
                      <td>[blracer4]</td>
                      <td align="right" valign="middle">[bltime4]</td>
                    </tr>
                    <tr>
                      <td width="20">[bl5]</td>
                      <td>[blracer5]</td>
                      <td align="right" valign="middle">[bltime5]</td>
                    </tr>
                    </table></td>
                  <td width="8" align="left" valign="top"></td>
                  <td width="196" align="center" valign="middle" background=""><img src="images/resultsheet/speedsheet.png" width="174" height="80"></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
          <td width="5">&nbsp;</td>
          <td width="495" align="left" valign="top"><table width="495" height="555" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="317" align="left" valign="top"><img src="[chart_new]" width="500" height="330"></td>
              </tr>
            <tr>
              <td height="131" align="left" valign="top"><table width="495" height="139" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="30" colspan="4" align="left" valign="middle" class="titletext"><table width="495" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="20">&nbsp;</td>
                      <td align="center" class="titlesub">Race Information</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="20" align="left" valign="top"></td>
                  <td width="196" align="left" valign="top"><table width="196" border="0" cellpadding="2" cellspacing="1" class="pagetext" bgcolor="#b1b1b1">
                    <tr>
                      <td width="28" height="23" bgcolor="#6d6e71">&nbsp;</td>
                      <td align="center" bgcolor="#FFFFFF">Amateur</td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF">[ama_count]</td>
                    </tr>
                    <tr>
                      <td height="23" bgcolor="#b2b3b6">&nbsp;</td>
                      <td align="center" bgcolor="#FFFFFF">Intermediate</td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF">[int_count]</td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="23">&nbsp;</td>
                      <td align="center">Expert</td>
                      <td align="center" valign="middle">[exp_count]</td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="23" colspan="2" align="center">Total Races</td>
                      <td align="center" valign="middle">[total_races]</td>
                    </tr>
                  </table></td>
                  <td width="8" align="left" valign="top"></td>
                  <td width="271" align="left" valign="top"><table width="252" border="0" cellpadding="2" cellspacing="1" class="pagetext" bgcolor="#b1b1b1">
                    <tr>
                      <td width="119" height="23" align="center" bgcolor="#FFFFFF">Speed</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_1]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_2]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_3]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_4]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_5]</td>
                    </tr>
                    <tr>
                      <td height="23" align="center" bgcolor="#FFFFFF">Consistancy</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_1]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_2]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_3]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_4]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_5]</td>
                    </tr>
                    <tr>
                      <td height="23" align="center" bgcolor="#FFFFFF">Passing</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_1]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_2]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_3]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_4]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_5]</td>
                    </tr>
                    <tr>
                      <td height="23" align="center" bgcolor="#FFFFFF">Betterment</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_1]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_2]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_3]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_4]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_5]</td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
              </tr>
            <tr>
              <td height="99" align="center" valign="middle"><table width="460" height="73
            " border="0" cellpadding="8" cellspacing="0" background="resize_image.php?image=images/resultsheet/tipbox.png&ratio=R" class="pagetext">
                <tr>
                  <td align="left" valign="middle"><p class="helptext">[tip]</p></td>
                  </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
    <td width="60">&nbsp;</td>
  </tr>
  <tr>
    <td height="54" colspan="2" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--endsheet-->
</body>
</html>
