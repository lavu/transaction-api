<?php
	ini_set("display_errors",1);
	
	import_customers_main();
	
	function import_customers_main()
	{
		require_once(__DIR__."/connect_to_racer_data.php");

		$dataname = mysqli_real_escape_string($_GET['dataname']);
		$f_name = NULL;
		$l_name = NULL;
		$racer_name = NULL;
		$original_id = NULL;
		
		// test that the dataname is good
		if (!is_dataname_good($dataname)) {
			echo "Bad dataname " . $dataname . "\n";
			return;
		}
		
		// for each customer, try to find their original id
		$customers = $conn->query("read","SELECT `f_name`,`l_name`,`racer_name`,`id` FROM `racer_data`.`lk_customers` WHERE `dataname` = 'p2r_".$dataname."'");
		$successes = 0;
		if ($customers) {
			while ($customer = mysqli_fetch_assoc($customers)) {
				$f_name = $customer['f_name'];
				$l_name = $customer['l_name'];
				$racer_name = $customer['racer_name'];
				$f_name = mysqli_real_escape_string($f_name);
				$l_name = mysqli_real_escape_string($l_name);
				$racer_name = mysqli_real_escape_string($racer_name);
				$original_id = mysqli_real_escape_string(get_original_id($dataname, $f_name, $l_name, $racer_name));
				if ($f_name == "Ryan") { echo "..." . $f_name . " " . $l_name . "<br />\n"; }
				if ($original_id === NULL) {
					echo "Failed to find original id for " . $f_name . " " . $l_name . " (" . $racer_name . ")<br />\n";
				} else {
					$result = $conn->query("write","UPDATE `racer_data`.`lk_customers` SET `original_id` = '".$original_id."' WHERE `id` = '".$customer['id']."'");
					if ($conn->affectedRows() == 0) {
						echo "Failed to update original id for " . $f_name . " " . $l_name . " (" . $racer_name . ")<br />\n";
					} else {
						$successes++;
						echo $successes . " ";
					}
				}
			}
		}
		echo "finished";
	}
	
	function is_dataname_good($dataname) {
		$dataname_query = $conn->query("read","SELECT * FROM `import_customers`.`lk_customers_".$dataname."` LIMIT 1");
		if ($dataname_query)
			if (mysqli_num_rows($dataname_query))
				return TRUE;
		return FALSE;
	}
	
	function get_original_id($dataname, $f_name, $l_name, $racer_name) {
		$racer_query = $conn->query("read","SELECT `id` FROM `import_customers`.`lk_customers_".$dataname."` WHERE `f_name`='".$f_name."' AND `l_name`='".$l_name."' AND `racer_name`='".$racer_name."' LIMIT 1");
		
		if ($racer_query) {
			if (mysqli_num_rows($racer_query)) {
				$row = mysqli_fetch_assoc($racer_query);
				return $row['id'];
			}
		}
		return NULL;
	}
?>
