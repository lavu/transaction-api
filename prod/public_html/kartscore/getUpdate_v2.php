<?php
	/*
		JSON errors 
		0 = JSON_ERROR_NONE
		1 = JSON_ERROR_DEPTH
		2 = JSON_ERROR_STATE_MISMATCH
		3 = JSON_ERROR_CTRL_CHAR
		4 = JSON_ERROR_SYNTAX
		5 = JSON_ERROR_UTF8
	*/

	date_default_timezone_set('America/New_York');
	if(isset($_GET['debugging'])){
		ini_set("log_errors", 1);
		//ini_set("error_log", "/var/log/httpd/getUpdate_error_log");
		ini_set("display_errors",1);
	}
	
	$postData=array();	
	$lastTime='';
	if( isset($_GET['useTestData']))
		$_POST['json']= file_get_contents("./testData.txt");
	else if(!isset($_POST['json']) || $_POST['json']==''){
		echo"JSON not sent. ";
		error_log('json not sent.');
		exit();	
	}
	$string= utf8_encode($_POST['json']);
	$postData=json_decode($string,1);

	$good=false;
error_log("Inside getUpdate_v2.");
error_log("Post Data length:".strlen(json_encode($postData)));
	require_once(__DIR__."/connect_to_racer_data.php");

	if(!empty($postData)){
		
		foreach($postData as $post){
			//echo print_r($post,true). "\n";

			$write_result= writeRaceInfo($post['event_info']);
			if($write_result=="success"){
				//echo "Successfully wrote race information\n";
				$cust_return=writeCustomerInfo($post['customers']);
				
				if( $cust_return=="success"){
					$good=true;	
					//echo "Successfully wrote customer information\n";
				}else
					echo "The cust return is: ".$cust_return."\n";
				
			}else
				echo "The write result is: ".$write_result."\n";		
		}
	}else{
		$good=false;
		echo "Json_decode failed. \n ".json_last_error().".\n The json was:\n ". $_POST['json'];		
	}
	if($good)
		echo "success";
		
	mysqli_close($con);
	function writeRaceInfo($eventInfo){
		$return_val='';

		$check_query_str = "SELECT * FROM `races` where `eventid`='[1]' AND `dataname`='[2]' ";
		$check_query = mlavu_query($check_query_str,$eventInfo['eventid'],$eventInfo['dataname']);
		if (!mysqli_num_rows($check_query)){	//in other words, if this event doesnt yet exists. 
		//if(true){
			$keys            = array_keys($eventInfo);
			$valuesRaw       = array_values($eventInfo);
			$valuesSanitized = array();
			foreach($valuesRaw as $currRawValue){
			     if(is_array($currRawValue)){
			        $sanitizedValue = "'".json_encode($currRawValue)."'";
			     }else{
				$sanitizedValue = "'".str_replace("'","\\'",$currRawValue)."'";
			     }
			     $valuesSanitized[] = $sanitizedValue;
			}

			$query_string = "INSERT INTO `racer_data`.`races` (". implode(",", $keys). " ) VALUES (". implode(",", $valuesSanitized).");";

			//$conn->query("write",$query_string);
			mlavu_query($query_string);
			if (lavu_dberror())
				$return_val= lavu_dberror();
			else
				$return_val= "success";
		}else
			$return_val= "success";
			
		return $return_val;
	}


	function writeCustomerInfo($customer_info){

		$return_val='';

		foreach($customer_info as $customer){

			if(!isset($customer['customer_id'])){
				echo "No Customer id for customer: ". print_r($customer, true);
error_log("Missing customer id");
				continue;
			}	


/*
			//$check_query = mlavu_query("SELECT * FROM `racer_data`.`kart_customers` where `customer_id`='".mysqli_real_escape_string($customer->customer_id)."' AND `dataname`='".mysqli_real_escape_string($customer->dataname)."' ");
			//if (!mysqli_num_rows($check_query)){	//in other words, if this event doesnt yet exists. 
*/

			if(true){

				$keys            = array_keys($customer);
				$valuesRaw       = array_values($customer);
				$valuesSanitized = array();
				foreach($valuesRaw as $currRawValue){
			     	     if(is_array($currRawValue)){
			                  $sanitizedValue = "'".json_encode($currRawValue)."'";
			             }else{
				          $sanitizedValue = "'".str_replace("'","\\'",$currRawValue)."'";
			             }
			             $valuesSanitized[] = $sanitizedValue;
				}

				$query_string = "INSERT INTO `racer_data`.`kart_customers` (". implode(",", $keys). " ) VALUES (". implode(",", $valuesSanitized).");";
				mlavu_query($query_string);

error_log("Insert query:".$query_string);
		
				if (lavu_dberror()){
					return lavu_dberror();
				}
			}	
		}
		return "success";
	}		
?>
