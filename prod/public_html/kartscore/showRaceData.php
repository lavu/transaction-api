<?php
	ini_set("display_errors",1);

	session_start();
	/* First Thing to do is connect to the DB */
	date_default_timezone_set('America/New_York');

	require_once(__DIR__."/connect_to_racer_data.php");

	/*
		determine which data set to display
	*/
	$viewType='All Time Champs';
	$location=$_GET['dataname'];
	$type ="best_of";
	$track_options = '';
	$selected_track='%';
	$selected_type='%';
	if(isset($_GET['selected_track'])){
		$selected_track= mysqli_real_escape_string($conn, $_GET['selected_track']);
	}
	if(isset($_GET['selected_type'])){
		$selected_type= mysqli_real_escape_string($conn, $_GET['selected_type']);	
	}
	
	function initialize(){
		global $viewType;
		global $location;
		global $selected_track;
		global $selected_type;
		global $conn;

		$lastMon = date('Y-m-d', strtotime('last monday'));
		$data='';
		
		if( $location==''){
			echo "An error has occurred. No location was passed";
			exit;
		}
		
		if(isset($_GET['best_of'])){
			$type ="best_of";
			$viewType="Best Race Times of ". date("F");;
			$time_start = microtime(true);
			$data=getBestOf($location);
			$time_end = microtime(true);
			$time = $time_end - $time_start;
			//echo"Time to get the data: ". $time;
		}else if(isset($_GET['best_of_week'])){
			$type="best_of_week";
			$data=getBestOfWeek($location);	
			$viewType="Best Race Times This Week (starting ".$lastMon.")";
		}else if(isset($_GET['racer_data'])){
			$viewType="Choose Racer";
			$type="racer_data";
			$data=getRacers($_GET['racer_data'], $location);
		}else if(isset($_GET['racer_data_specific'])){
			$type= "racer_data_specific";
			$viewType="Individual Stats";
			$data=get_individual_stats($_GET['racer_data_specific'], $location);
		}else if(isset($_GET['kids_race'])){
			$type= "kids_race";
			$viewType="Best Kids Race Times This Month";
			$data=getkidsRace($location);	
		}else if(isset($_GET['kids_race_week'])){
			$type="kids_race_week";
			$viewType="Best Kids Race Times This Week (starting ".$lastMon.")";
			$data=getkidsRaceWeek($location);	
		}else{
			$type = "best_of";
			$data=getBestOf($location);
			$viewType="Best Race Times This Month";
		}
		echo "<script type ='text/javascript'> var type='$type'; var dataname ='$location'; var track ='$selected_track'; var race_type ='$selected_type';</script>";
		//$time_start = microtime(true);
		$html= displayData($data, $location, $viewType);
		//$time_end = microtime(true);
		//$time = $time_end - $time_start;
		//echo "DISPLAY DATA TOOK: ". $time;
		return $html;
	}
	
	function displayErrorMessage(){
		echo "error";
	}
	function getBestOfWeek($location){

		global $selected_track;
		global $selected_type;
		global $conn;

		$results=array();
		$lastMon  = date('Y-m-d', strtotime('last monday'));
		$dataname = mysqli_real_escape_string($conn, $location);
		$lastMon  = mysqli_real_escape_string($conn, $lastMon);
		
		
		//echo "select * from `race_data`.`races` where `date` >'".$lastMon."' and `dataname`='".$dataname."' and `track` like '".$selected_track."' and `scores_page_title` like '".$selected_type."' order by `ms_end`*1 ";

		$queryStr = "select * from `racer_data`.`races` where `date` >'".$lastMon."' and `dataname`='".$dataname."' and `track` like '".$selected_track."' and `scor\
es_page_title` like '".$selected_type."' order by `ms_end`*1 ";



		$query= mlavu_query($queryStr);

		//echo print_r(perform_query($query),1);
		return perform_query($query);
	}
	function getBestOf($location){
		global $conn;
		global $selected_track;
		global $selected_type;
		$dataname= str_replace("'","\\'",$location);
		$monthNumber= date('m');

$queryStr = "select * from `racer_data`.`races` where `dataname`='$dataname' and YEAR(`date`) = YEAR(CURDATE()) AND MONTH(`date`) = '$monthNumber' and `track` like '".$selected_track."' and `scores_page_title` like '".$selected_type."' order by `ms_end`*1 asc ";

error_log("Query str:".$queryStr);
		$query= mlavu_query($queryStr);
		//echo "select * from `races` where `dataname`='$dataname' and YEAR(`date`) = YEAR(CURDATE()) AND MONTH(`date`) = '$monthNumber' and `track` like '".$selected_track."' and `scores_page_title` like '".$selected_type."' order by `ms_end`*1 asc";
		return perform_query($query);
	}
	function getkidsRace($location){
		global $conn;
		global $selected_track;
		global $selected_type;
		$results=array();
		$dataname= mysqli_real_escape_string($conn, $location);
		$monthNumber= date('m');
		$query= mlavu_query("select * from `racer_data`.`races` where `dataname`='$dataname' and YEAR(`date`) = YEAR(CURDATE()) AND MONTH(`date`) = '$monthNumber' and (`type` LIKE '%Youth%' OR `type` LIKE '%kid%' OR `type` LIKE '%Junior%') and `track` like '".$selected_track."' and `scores_page_title` like '".$selected_type."' order by `ms_end`*1 asc ");
		return perform_query($query);
	}
	function getkidsRaceWeek($location){
		global $conn;
		global $selected_track;
		global $selected_type;
		$dataname= mysqli_real_escape_string($conn, $location);
		$lastMon = date('Y-m-d', strtotime('last monday'));
		
		$query= mlavu_query("select * from `racer_data`.`races` where `date` >='".$lastMon."' and `dataname`='".$dataname."' and (`type` LIKE '%Youth%' OR `type` LIKE '%kid%' OR `type` LIKE '%Junior%') and `track` like '".$selected_track."' and `scores_page_title` like '".$selected_type."' order by `ms_end`*1 ");
		return perform_query($query);
	}
	function get_individual_stats($id, $location){
		global $conn;
		$dataname= mysqli_real_escape_string($conn, $location);
		$monthNumber= date('m');
		$queryStr = "select * from `racer_data`.`races` where `dataname`='$dataname' and YEAR(`date`) = YEAR(CURDATE()) order by DATE(`date`) asc ";

		$query= mlavu_query($queryStr);
		$lap_times=array();
		$sorted=false;
		if( mysqli_num_rows($query)){
			while ($read= mysqli_fetch_assoc($query)){
				
				$race_times= json_decode($read['race_info']);
				foreach($race_times as $cust_id=>$lap_time){
					if( ($lap_time * 1) > 10.0 && $cust_id== $id)
						$lap_times[$lap_time]=  array("cust_id"=>$cust_id, "event_id"=>$read['eventid'],"date"=>$read['date'],"type"=>$read['type'],"track"=>$read['track'], "lap_time"=>$lap_time);
				}
				
			}
			//echo print_r($lap_times,1);
			uksort($lap_times, "cmp");
			return $lap_times;

		}else{
			debugging("No Races returned \n" );
			return false;
		}	
		
	}
	
	function getRacerInfo($cust_id, $location){
		global $conn;
		$results=array();
		$dataname= $location;
		$cust_id = $cust_id;
		$query=mlavu_query("SELECT * FROM `racer_data`.`kart_customers` WHERE `customer_id` = '".$cust_id."' AND `dataname`='".$dataname."' limit 1");	
		//echo "SELECT * FROM `kart_customers` WHERE `customer_id` = '".$cust_id."' AND `dataname`='".$dataname."' limit 1 <br>";		
		if( mysqli_num_rows($query))
			return mysqli_fetch_assoc($query);
		else
			return "error: no user found with cust id: ". $cust_id. "  and dataname: ".$dataname;
	}
	function getRacers($searchTerm, $location){
		global $conn;
		$results=array();
		$searchTerm= mysqli_real_escape_string($conn, $searchTerm);
		$searchTerm= preg_replace('/[^A-Za-z0-9\-]/', '', $searchTerm);
		echo "the search term is ". $searchTerm;  
		$dataname = str_replace("'","\\'",$location);
		$query = mlavu_query("SELECT * FROM `racer_data`.`kart_customers` WHERE `dataname` = '".$location."' AND (`email` LIKE '%".$searchTerm."%' OR `racer_name` LIKE '%".$searchTerm."%' OR CONCAT (`f_name`, ' ', `l_name`) LIKE '%".$searchTerm."%')  limit 100");

		while ($read= mysqli_fetch_assoc($query)){
			$id= mysqli_real_escape_string($conn, $read['customer_id']);
			
			$read['cust_id']= $id;
			$results[]= $read;	
		}
		//echo print_r($results,1);
		return $results;
	}	
	function perform_query($query){
		global $conn;
		$lap_times=array();
		$sorted=false;
		if( mysqli_num_rows($query)){
			while ($read= mysqli_fetch_assoc($query)){
			
				$race_times= json_decode($read['race_info']);
				//error_log("THE race times are: ".print_r($race_times,1));
				if( $race_times){
					foreach($race_times as $cust_id=>$lap_time){
						if( ($lap_time * 1) > 10.0)
							$lap_times[$lap_time]=  array("cust_id"=>$cust_id, "event_id"=>$read['eventid'],"date"=>$read['date'],"type"=>$read['type'], "lap_time"=>$lap_time, "track"=>$read['track']);
					}
				}

			}
			uksort($lap_times, "cmp");
			return $lap_times;

		}else{
			debugging("No Races returned \n" );
			return false;
		}	

	}
	function cmp($a, $b){
		if($a==$b){
			return 0;
		}
	    return ($a < $b) ? -1 : 1;	
	}
	function get_tracks(){
		global $conn;
		global $location; 
		global $selected_track;
		if($selected_track=='%')
			$options = "<option value = '%'> All Tracks </option>";
		else
			$options = "<option value = '%' selected> All Tracks </option>";
			
		$dn = mysqli_real_escape_string($conn, $location);
		$query = mlavu_query("select DISTINCT(`track`) from `racer_data`.`races` where `dataname`='$dn'");
		while($res = mysqli_fetch_assoc($query)){
			if($res['track']!= '' && $res['track']!='0'){
				if($res['track'] == $selected_track)
					$options.="<option value = '". $res['track']. "' selected>".$res['track']."</option>";
				else
					$options.="<option value = '". $res['track']. "'>".$res['track']."</option>";
			}
		}
		return $options;
	}
	function get_types(){
		global $conn;
		global $location; 
		global $selected_track;
		global $selected_type;
		if($selected_track=='%')
			$options = "<option value = '%'> All Race Types </option>";
		else
			$options = "<option value = '%' selected> All Race Types </option>";
			
		$dn = mysqli_real_escape_string($conn, $location);
		$query = mlavu_query("select DISTINCT(`scores_page_title`), `type` from `racer_data`.`races` where `dataname`='$dn'");
		while($res = mysqli_fetch_assoc($query)){
			if($res['scores_page_title']!= '' && $res['scores_page_title']!='0'){
				if($res['type'] == $selected_type)
					$options.="<option value = '". $res['scores_page_title']. "' selected>".$res['scores_page_title']."</option>";
				else
					$options.="<option value = '". $res['scores_page_title']. "'>".$res['scores_page_title']."</option>";
			}
		}
		return $options;
	}
	function displayData($data, $location, $viewType){
		global $conn;
		$html='';
		$counter=0;
		$cust_id_array=array();
		
		if( $data=='error' || !$data)
			return 'An Error has occurred. Please try again later. ';
		//echo "the data is: ". print_r($data,1);
		foreach ($data as $info){
			//if($info['racer_name']== 'JohnSki'){
			//	echo print_r($info,1);
			//}
			if($counter > 99){
				//echo "Time to get racer info: ". $total_get_racer_infoTime;
				return $html;			
			}
			if( $viewType != "Individual Stats"){
				if(isset($cust_id_array[$info['cust_id']])){
					continue;
				}else
					$cust_id_array[$info['cust_id']]=1;
			}
			$racerInfo= getRacerInfo($info['cust_id'], $location);
			//if($racerInfo['racer_name']== 'JohnSki'){
			//	echo print_r($info,1);
			//}
			if ($viewType != "Choose Racer") {
				$html.='
					<tr class="dataRow" onclick ="window.location=\'getRaceSheet.php?eventid='.$info['event_id'].'&trackid=[trackid]&mode=results&loc_id=1&location='.$location.'&racer_name='.$racerInfo['racer_name'].'&originalid='.$info['cust_id'].'\' ">
						<td>
							'.++$counter.'
						</td>
						<td>	
							'.$racerInfo['racer_name'].'
						</td>
						<td>	
							'.$racerInfo['f_name'].'
						</td>
						<td>	
							'.$racerInfo['l_name'].'
						</td>
						<td>	
							'.$info['type'].'
						</td>
						<td class= "track_name">
							'.$info['track'].'
						</td>
						<td>	
							'.$info['date'].'
						</td>
						<td>	
							'.round(($info['lap_time']*1),3).'
						</td>
						
					</tr>
				';
			} else {
				$html.='
					<tr class="dataRow" onclick ="window.location=\'showRaceData.php?dataname='.$location.'&racer_data_specific='.$info['cust_id'].'\'">
						<td>	
							'.$info['racer_name'].'
						</td>
						<td>	
							'.$info['f_name'].'
						</td>
						<td>	
							'.$info['l_name'].'
						</td>
					</tr>
				';
			}
		}
		
		return $html;
	}
	function debugging($var){
		echo "<pre><div class='debugging'  style='color:white; '>". print_r($var,true)."</div></pre>"; 
	}
	function quicksort($array) {
	    if(count($array) < 2) return $array;
	 
	    $left = $right = array();
	 
	    reset($array);
	    $pivot_key = key($array);
	    $pivot = array_shift($array);
	 
	    foreach($array as $k => $v) {
	        if($v < $pivot)
	            $left[$k] = $v;
	        else
	            $right[$k] = $v;
	    }
	 
	    return array_merge(quicksort($left), array($pivot_key => $pivot), quicksort($right));
	}
?>

<?php $html= initialize();?>
<html>
	<head>
		<title>
			P2R Race Data
		</title>
	
		<link rel="stylesheet" type="text/css" href="./css/styles.css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47775601-1', 'polepositionraceway.com');
  ga('send', 'pageview');

</script>
	<body>
		
		<div id= 'bodyContent'>
			<div id= 'header'>
				
				<img src = "./images/logo.jpeg" style="float:left;" alt="" > </img>
				<img src = "./images/search.jpg" style="float:left;" alt=""> 
					<div id= "search">
						<input type="text" id="searchTerm" placeholder="Racer name/email"> 
						<input type= "submit" value="Search" onclick= "
							var text= document.getElementById('searchTerm').value; 
							window.location='http://kartscore.poslavu.com/showRaceData.php?dataname=<?php echo $location;?>&racer_data='+text+'' ">
					</div>
					
				</img>
			
			</div>
			<div id='divider'>
				<img src = "./images/divider.jpeg" style="float:left;" alt="" />
					<div id='viewType'>
						<?php echo $viewType; ?>
						<?php if(!isset($_GET['hide_selects'])){ 
							
						?>

						<div id ='track_selector'>
							<select name ='track_select' onchange='load_with_track(this)'>
								
								<?php echo get_tracks();?>
							</select>
						
						</div>
						<!--<div id ='type_selector'>
							<select name ='type_select' onchange='load_with_type(this)'>
								<?php echo get_types();?>
							</select>
						</div>-->
						<?php
						}
						?>
					</div>
			</div>
			
			
			<table id='race_data' border='1'>
				
				<?php
					if ($viewType != "Choose Racer") {
						echo "<tr id='header_data'>\n";
						echo "					<th> Place		 </th>\n";
						echo "					<th> Racer Name  </th>\n";
						echo "					<th> First Name  </th>\n";
						echo "					<th> Last Name   </th>\n";
						echo "					<th> Race Type   </th>\n";
						echo "					<th> Track       </th>\n";
						echo "					<th> Race Date   </th>\n";
						echo "					<th> Time 		 </th>\n";
						echo "				</tr>\n";
					} else {
						echo "<tr id='header_data'>\n";
						echo "					<th> Racer Name  </th>\n";
						echo "					<th> First Name  </th>\n";
						echo "					<th> Last Name   </th>\n";
						echo "				</tr>\n";
					}
				?>
					<?php echo $html;?>
					
			</table>
		</div>
	</body>
	<script type='text/javascript'>
		function load_with_track(elem){
			window.location = "http://kartscore.poslavu.com/showRaceData.php?dataname="+dataname+"&"+type +"&selected_track="+elem.value+"&selected_type="+race_type;
		}
		function load_with_type(elem){
			window.location = "http://kartscore.poslavu.com/showRaceData.php?dataname="+dataname+"&"+type +"&selected_type="+elem.value+"&selected_track="+track;
		}
	</script>
</html>
