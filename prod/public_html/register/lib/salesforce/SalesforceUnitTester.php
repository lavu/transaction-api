<?php

class SalesforceUnitTester extends PHPUnit_Framework_TestCase{

    protected $sforce;

    public static function setUpBeforeClass () {
        require 'SalesforceHandler.php';
        require '/home/poslavu/private_html/salesforceCredentials.php';
        $GLOBALS['salesforce'] = SalesforceHandler::create(SFORCE_UNAME,SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
    }

    public function setUp(){
        global $salesforce;
        $this->sforce = &$salesforce;
    }

	public function testConnect() {
        $expected_returned = array(
            "username",
            "password",
            "client_id",
            "client_secret",
            "id",
            "instance",
            "signature",
            "access_token",
            "token_timestamp",
            "version",
            "version_label",
            "version_url"
        );
        foreach( $expected_returned as $expected ) {
            $val = $this->sforce->get($expected);
            echo "checking for \"{$expected}\"\nvalue:$val\n\n";
            $this->assertTrue( !empty( $val ) );
        }
	}

    public function testGetResources() {
        $resources = $this->sforce->get_all_resources();
        //print_r( $resources );
        $this->assertNotEmpty( $resources );
        $this->assertTrue( is_array( $resources ) );
    }

    public function testGetSObjects() {
        $sobjects = $this->sforce->get_all_sobjects();
        //print_r( $sobjects );
        $this->assertNotEmpty( $sobjects );
        $this->assertTrue( is_array( $sobjects ) );
    }

    public function testLeadMetadata () {
        $meta = $this->sforce->get_lead_metadata();
        echo "=== Leads Object Metadata ===\n";
        //print_r( $meta );
        $this->assertNotEmpty( $meta );
        $this->assertTrue( is_array( $meta ) );
        // list all the possible field in a lead just for fun
        $this->assertNotEmpty( $meta['fields'] );
//        foreach( $meta['fields'] as $field ) {
//            echo "Name:{$field['name']} // Label:{$field['label']} // Type:{$field['type']}\n";
//        }
    }

    public function testInsertNGetLead() {
        $test_lead = array(
            "Salutation" => "Mrs",
            "FirstName" => "Johnny",
            "LastName" => "Cupcakes",
            "Company" => "DooDoo Cupcakes Co.",
            "Street" => "163 Brownies Ave NE",
            "City" => "Brownsville",
            "State" => "TX",
            "PostalCode" => 78522,
            "Country" => "US",
            "Email" => "carlosa@poslavu.com",
            "Description" => <<<TXT
This is a test, please do not panic and ignore this message
TXT
            ,"LeadSource" => "WEbSignup"
        );
        $insert_response = $this->sforce->insert_lead( $test_lead );
        // print_r( $insert_response );
        $this->assertNotEmpty( $insert_response );
        $this->assertNotEmpty( $insert_response['success'] );
        $lead_sforce_id = $insert_response['id'];
        $this->assertNotEmpty( $lead_sforce_id );
        // get the same lead back
        $retrieved_lead = $this->sforce->get_lead_by_id( $lead_sforce_id );
        // print_r( $retrieved_lead );
        $this->assertNotEmpty( $retrieved_lead );
        $this->assertEquals( $retrieved_lead['Id'], $lead_sforce_id );
        foreach( $test_lead as $test_fields => $value ) {
            $this->assertEquals( $retrieved_lead[$test_fields], $value );
        }
        // now let's delete it
        //echo "== Deleting Johnny Cupcakes Lead ==\n";
        $delete_response = $this->sforce->delete_lead_by_id( $lead_sforce_id );
        // print_r( $delete_response );
        $this->assertNull( $delete_response );
        // Now we should try to get the lead and but it is not there
        $retrieved_lead = $this->sforce->get_lead_by_id( $lead_sforce_id );
        print_r( $retrieved_lead );
        $this->assertNotEmpty( $retrieved_lead );
        $this->assertEquals( $retrieved_lead[0]['errorCode'], "NOT_FOUND" );
        $this->assertArrayNotHasKey( "Id",  $retrieved_lead );
        $this->assertArrayNotHasKey( "Name", $retrieved_lead );
    }

    public function testGetNUpdate () {
        // get one of the dummy leads that are on by default in the
        // developer account
        $retrieved_lead_id = $this->sforce->get_leads_id_by_field("Email", "david@blues.com");
        echo "== Retrieved Lead ==\n";
        $this->assertNotNull( $retrieved_lead_id );
        print_r( $retrieved_lead_id );
        $lead_id = $retrieved_lead_id['records'][0]['Id'];
        echo "== Lead Id : $lead_id\n";
        $retrieved_lead = $this->sforce->get_lead_by_id( $lead_id );
        echo "== retrieved lead\n";
        print_r( $retrieved_lead );
        $lead_first = $retrieved_lead['FirstName'];
        $lead_last = $retrieved_lead['LastName'];
        $this->assertNotNull( $lead_id );
        $this->assertNotNull( $lead_first );
        $this->assertNotNull( $lead_last );
        // let's change the name now
        $new_name = array(
            "FirstName" => "David",
            "LastName" => "Blues"
        );
        $update_response = $this->sforce->update_lead_by_id($lead_id, $new_name );
        $this->assertNull( $update_response );
        // let's see if it worked
        $lead_again = $this->sforce->get_lead_by_id( $lead_id );
        $this->assertEquals( $new_name['FirstName'],  $lead_again['FirstName'] );
        $this->assertEquals( $new_name['LastName'],  $lead_again['LastName'] );
        // Turn it back to what it was
        $old_values = array( "FirstName" => $lead_first, "LastName" => $lead_last );
        $update_response = $this->sforce->update_lead_by_id($lead_id, $old_values );
        $this->assertNull( $update_response );
        // let's see if it worked
        $lead_again = $this->sforce->get_lead_by_id( $lead_id );
        $this->assertEquals( $lead_again['FirstName'],  $lead_first );
        $this->assertEquals( $lead_again['LastName'],  $lead_last );
    }

    public function testKillNRevive() {
        // let's create a test lead just for shits and giggles
        $test_lead = array(
            "FirstName" => "Timmy",
            "LastName" => "McTurdies",
            "Company" => "McTurdies Inc.",
            "Street" => "163 Brownies Ave NE",
            "City" => "Brownsville",
            "State" => "TX",
            "PostalCode" => 78522,
            "Country" => "US",
            "Email" => "electpasa@gmail.com",
            "Description" => <<<TXT
McTurdies, the best peanut butter sandwiches in the southwest
TXT
        ,"LeadSource" => "SalesforceUnitTesting"
        );
        $insert_response = $this->sforce->insert_lead( $test_lead );
        // make sure to get all the old properties before killing
        $old_instance_properties = $this->sforce->get_all();
        // now let's kill it
        $this->sforce = null;
        // .. and revive
        $this->sforce = SalesforceHandler::revive( $old_instance_properties );
        // Now let's do something to make sure it works
        $this->sforce->delete_lead_by_id( $insert_response['id'] );
        // Let's create a new lead just for shits and giggles
        $new_lead_response = $this->sforce->insert_lead( array(
            "FirstName" => "Testie",
            "LastName" => "McTest",
            "Company" => "Test Co.",
            "Street" => "163 Testing Grounds",
            "City" => "Testville",
            "State" => "TX",
            "PostalCode" => 78522,
            "Country" => "US",
            "Email" => "test@test.com",
            "Description" => <<<TXT
This is another test, if you see me, it's all good
TXT
        ,"LeadSource" => "SalesforceUnitTesting"
        ) );
        print_r( $new_lead_response );
        $this->assertArrayHasKey( "id", $new_lead_response );
    }

    public function testUpsert() {
        $test_lead = array(
            "FirstName" => "Upsert",
            "LastName" => "Testing",
            "Company" => "Upsert & Co.",
            "Street" => "Upserting Ave APT 111",
            "State" => "GA",
            "City" => "Georgia",
            "PostalCode" => 30004,
            "Country" => "US",
            "Email" => "upsert@upsert.com",
            "Description" => "Testing the Upserting function",
            "LeadSource" => "SalesforceUnitTesting"
        );
        $upsert_response = $this->sforce->upsert_lead_by_external_id( 666, $test_lead );
        print_r( $upsert_response );
        $this->assertArrayHasKey(  "id", $upsert_response );
        //let's get it back to see if it worked
        $lead_id  = $upsert_response['id'];
        $lead  = $this->sforce->get_lead_by_id( $lead_id );
        foreach( $lead as $k=>$v ){
            if ( !empty( $test_lead[$k] ) ) {
                $this->assertEquals( $test_lead[$k], $v );
            }
        }
        // now let's change a couple of things
        $test_lead['Email'] = "electpaisa@hotmail.com";
        $test_lead['LastName'] = "Checking";
        $upsert_response = $this->sforce->upsert_lead_by_external_id( 666,  $test_lead );
        print_r( $upsert_response );
        $this->assertArrayHasKey(  "success", $upsert_response );
        $this->assertArrayHasKey(  "updated", $upsert_response );
        //let's get it back to see if it worked
        $lead  = $this->sforce->get_lead_by_id( $lead_id );
        foreach( $lead as $k=>$v ){
            if ( !empty( $test_lead[$k] ) ) {
                $this->assertEquals( $test_lead[$k], $v );
            }
        }
    }
}