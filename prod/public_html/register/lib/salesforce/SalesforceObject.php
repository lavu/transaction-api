<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/17/14
 * Time: 06:04 PM
 */

require_once "SalesforceHandler.php";
require_once "SalesforceInterface.php";

class SalesforceObject extends SalesforceHandler implements SalesforceInterface {

    protected $_credentials;
    protected $_object_name;
    protected $_metadata;

    public function get_by_id($id)
    {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $headers = array(
            "Authorization: Bearer {$access_token}"
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/$id";
        return json_decode( self::do_curl( $url , null, "GET", $headers ), true);
    }

    public function delete_by_id($id)
    {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $headers = array(
            "Authorization: Bearer {$access_token}"
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        $method = "DELETE";
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/$id";
        return json_decode( self::do_curl( $url, null, $method, $headers ), true);
    }

    public function update_by_id($id, $data)
    {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/$id";
        $method = "PATCH";
//        $method = "POST";
//        $url .= "?_HttpMethod=PATCH";
        $json_data = json_encode( $data );
        $headers = array(
            "Authorization: Bearer {$access_token}",
            "Content-Type: application/json",
            'Content-Length: ' . strlen($json_data),
            "X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        $response = self::do_curl( $url, $json_data, $method, $headers, $http_code );
        if ( empty( $response ) ) {
            if ( $http_code == 204 ) {
                return array("success" => true, "updated" => true );
            } else {
                return array("success" => false, "httpCode" => $http_code );
            }
        }
        return json_decode( $response, true );
    }

    public function get_by_field($field, $value)
    {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        // check if th field exists
        if ( empty( $this->_metadata ) ) {
            $this->get_meta();
        }
        foreach( $this->_metadata['fields']  as $field_def ) {
            if ( $field_def['name'] === $field ) {
                $field_meta = $field_def;
                break;
            }
        }
        if ( empty( $field_meta ) ){
            $err_msg = get_called_class() . "::" . __FUNCTION__ . "::Field '$field' doesn't exists";
            throw new SalesforceHandlerException( $err_msg );
        }
        if ( $field_meta["type"] === 'string' ) {
            $query_string = urlencode( "SELECT Id FROM {$this->_object_name} WHERE $field LIKE '$value'" );
        } elseif ( $field_meta["type"] === 'int' ) {
            $query_string = urlencode( "SELECT Id FROM {$this->_object_name} WHERE $field=$value" );
        } else {
            $query_string = urlencode( "SELECT Id FROM {$this->_object_name} WHERE $field='$value'" );
        }
        $url = "{$instance}{$version_url}/query/?q=$query_string";
        $method = "GET";
        $no_data = null;
        $headers = array(
            "Authorization: Bearer {$access_token}"
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        return json_decode( self::do_curl( $url, $no_data, $method, $headers ), true);
    }

    public function query( $query ) {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        // just pass the query verbatim
        $url = "{$instance}{$version_url}/query/?q=" . urlencode( $query );
        $method = "GET";
        $no_data = null;
        $headers = array(
            "Authorization: Bearer {$access_token}"
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        return json_decode( self::do_curl( $url, $no_data, $method, $headers ), true);
    }

    public function get_by_external_id( $field, $external_id ) {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/$field/$external_id";
        $method = "GET";
        $headers = array(
            "Authorization: Bearer {$access_token}",
            "X-PrettyPrint:1"
        );
        $response = self::do_curl( $url, null, $method, $headers, $http_code );
        return json_decode( $response, true );
    }

    public function upsert($field, $external_id, $data)
    {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/$field/$external_id";
        $method = "PATCH";
        $json_data = json_encode( $data );
        $headers = array(
            "Authorization: Bearer {$access_token}",
            "Content-Type: application/json",
            "Content-Length: ". strlen($json_data),
            "X-PrettyPrint:1"
        );
        $response = self::do_curl( $url, $json_data, $method, $headers, $http_code );
        if ( empty( $response ) ) {
            if ( $http_code == 204 ) {
                return array("success" => true, "updated" => true );
            } else {
                return array("success" => false, "httpCode" => $http_code );
            }
        }
        return json_decode( $response, true );
    }

    public function insert($data_array)
    {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/";
        $method = "POST";
        $json_data = json_encode( $data_array );
        $headers = array(
            "Authorization: Bearer {$access_token}",
            "Content-Type: application/json",
            'Content-Length: ' . strlen($json_data)
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        return json_decode( self::do_curl($url, $json_data, $method, $headers ) ,true);
    }

    public function get_meta()
    {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/describe";
        $method = "GET";
        $no_data = null;
        $headers = array(
            "Authorization: Bearer {$access_token}"
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        return json_decode( self::do_curl($url, $no_data,$method, $headers ) ,true);
    }

    public function get_property($property_name, $id)
    {
        $property_val = '';

        if ( empty($property_name) || empty($id) ) {
            return $property_val;
        }

        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/{$id}?fields={$property_name}";
        $method = "GET";
        $no_data = null;
        $headers = array(
            "Authorization: Bearer {$access_token}"
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );

        $response_json = self::do_curl( $url, $no_data, $method, $headers );

        $property_vals = json_decode( $response_json, true );
        $property_val  = isset( $property_vals[$property_name] ) ? $property_vals[$property_name] : '';

        return $property_val;
    }

    public function get_properties($property_names, $id)
    {
        $property_vals = array();

        if ( !is_array($property_names) || empty($id) ) {
            return $property_vals;
        }

        $property_names = implode(',', $property_names);

        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        $url = "{$instance}{$version_url}/sobjects/{$this->_object_name}/{$id}?fields={$property_names}";
        $method = "GET";
        $no_data = null;
        $headers = array(
            "Authorization: Bearer {$access_token}"
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );

        $response_json = self::do_curl( $url, $no_data, $method, $headers );

        $properties = json_decode( $response_json, true );

        return $properties;
    }
}