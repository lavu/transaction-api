<?php

require_once "SalesforceLogin.php";
require_once "SalesforceObject.php";

class SalesforceQuoteDocument extends SalesforceObject {

    public function __construct ( SalesforceLogin $credentials, $meta=null ){
        $this->_object_name = 'QuoteDocument';
        $this->_credentials = $credentials;
        $this->_metadata = empty( $meta ) ? $this->get_meta() : $meta;
    }
}