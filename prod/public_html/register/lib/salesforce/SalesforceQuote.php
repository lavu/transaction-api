<?php

require_once "SalesforceLogin.php";
require_once "SalesforceObject.php";

class SalesforceQuote extends SalesforceObject {

    public function __construct ( SalesforceLogin $credentials, $meta=null ){
        $this->_object_name = 'Quote';
        $this->_credentials = $credentials;
        $this->_metadata = empty( $meta ) ? $this->get_meta() : $meta;
    }

    public function getQuoteDocuments($quote_id)
    {
        $access_token = $this->_credentials->get_var("access_token");
        $version_url = $this->_credentials->get_var("version_url");
        $instance = $this->_credentials->get_var("instance");
        // check if th field exists
        if ( empty( $this->_metadata ) ) {
            $this->get_meta();
        }
        $soql_escaped = urlencode( "SELECT Id, Name, Discount, GrandTotal FROM QuoteDocument WHERE QuoteId = '{$quote_id}'" );
        $url = "{$instance}{$version_url}/query/?q={$soql_escaped}";
        $method = "GET";
        $no_data = null;
        $headers = array(
            "Authorization: Bearer {$access_token}"
            //,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );

        $results = array('done' => true, 'totalSize' => 0, 'records' => array(), 'error' => '');
        try {
        	$results = json_decode(self::do_curl( $url, $no_data, $method, $headers ), true);
        }
		catch ( Exception $e ) {
			$results['error'] = $e->getMessage();
		}        

        return $results;
    }

}