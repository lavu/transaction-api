<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/17/14
 * Time: 4:47 PM
 */

class SalesforceLoginUnitTester extends PHPUnit_Framework_TestCase {

    protected $sforce;

    public static function setUpBeforeClass () {
        require 'SalesforceLead.php';
        require '/home/poslavu/private_html/salesforceCredentials.php';
        $GLOBALS['credentials'] = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
    }

    public function setUp(){
        $this->sforce = $GLOBALS['credentials'];
    }

    public function testConnect() {
        $expected_returned = array(
            "username",
            "password",
            "client_id",
            "client_secret",
            "id",
            "instance",
            "signature",
            "access_token",
            "token_timestamp",
            "version",
            "version_label",
            "version_url"
        );
        foreach( $expected_returned as $expected ) {
            $val = $this->sforce->get_var($expected);
            echo "checking for \"{$expected}\"\nvalue:$val\n\n";
            $this->assertTrue( !empty( $val ) );
        }
    }

    public function testGetResources() {
        $resources = $this->sforce->get_all_resources();
        //print_r( $resources );
        $this->assertNotEmpty( $resources );
        $this->assertTrue( is_array( $resources ) );
    }

    public function testGetSObjects() {
        $sobjects = $this->sforce->get_all_sobjects();
        //print_r( $sobjects );
        $this->assertNotEmpty( $sobjects );
        $this->assertTrue( is_array( $sobjects ) );
    }
} 