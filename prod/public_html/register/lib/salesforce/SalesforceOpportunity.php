<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/17/14
 * Time: 11:33 AM
 */

require_once "SalesforceLogin.php";
require_once "SalesforceObject.php";

class SalesforceOpportunity extends SalesforceObject {

    public function __construct ( SalesforceLogin $credentials, $meta=null ){
        $this->_object_name = "Opportunity";
        $this->_credentials = $credentials;
        $this->_metadata = empty( $meta ) ? $this->get_meta() : $meta;
    }
}