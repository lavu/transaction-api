<?php

function html_email_template( $content ) {
    return <<<HTML
<!doctype html>
<html>
    <head><meta charset="UTF-8"></head>
    <body style="font-family:sans-serif;color:#2b2b2b;background-color:#d4d4d4;padding:0;margin:0;">
        <div style="width:600px;padding:20px;margin:0 auto;">$content</div>
    </body>
</html>
HTML;
}

function array_to_table( $arr ) {
    $html_buffer = "<table><tr><th>Field</th><th>Value</th></tr>";
    foreach( $arr as $key => $value ) {
        $html_buffer .= "<tr>$key<td></td><td>$value</td></tr>";
    }
    return $html_buffer . "</table>";
}


// Get the lavuquery library
require '/home/poslavu/public_html/admin/cp/resources/lavuquery.php';
// get all leads in the time frame (mlavu_query takes care of the connection
// to db internally
$now = time();
$lead_handler = null;
$mysql_timestamp_format = "Y-m-d H:i:s";
$fifteen_min_ago = date( $mysql_timestamp_format, $now - (15 * 60));
$day_ago = date( $mysql_timestamp_format, $now - (24 * 60 * 60) );
$mysql_tstamp = date("Y-m-d H:i:s", $now);
$zero_time = "0000-00-00 00:00:00";
$query = <<<SQL
SELECT IF(`lead_source` LIKE '%distro portal%', 'opportunity', 'lead') AS `create`,
    `id` as 'lead_id',
    `created`,
    `data_name`,
    `distro_code`,
    `company_name`,
    `firstname`,
    `lastname`,
    `address`,
    `city`,
    `email`,
    `phone`,
    `state`,
    `zip`,
    `country`,
    `notes`,
    `lead_source`,
    `ipaddress`,
    `entry_url`,
    `referring_site`,
    `utm_campaign`,
    `utm_source`,
    `utm_medium`,
    `signupid`,
    `salesforce_id`,
    `salesforce_lead_id`,
    `salesforce_sent`
FROM `reseller_leads` WHERE
    `created` > '$day_ago'
AND
    `salesforce_sent`='$zero_time'
SQL;
$results = mlavu_query( $query );
if ( mysqli_num_rows( $results ) ){
    require_once "/home/poslavu/public_html/register/lib/utilities.php";
    require_once "/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php";
    require_once "/home/poslavu/public_html/admin/sa_cp/billing/procareas/SalesforceIntegration.php";
    require_once "/home/poslavu/private_html/salesforceCredentials.php";
    require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceLead.php";
    require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceAccount.php";
    require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceContact.php";
    require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceOpportunity.php";
    $package_container = new package_container();
    $fname = "/home/poslavu/private_html/salesforce_leads_log.txt";
    $fp = fopen($fname,"a");
    $leads_ids = array();
    $token_file_path = '/tmp/salesforce_token.txt';
    $file_exists = file_exists( $token_file_path );
    $file_not_empty = $file_exists && filesize($token_file_path);
    $is_token_fresh = $file_not_empty && ( time()  < ( filemtime( $token_file_path ) + 60 * 60 * 2 ) );
    $token = $is_token_fresh ? unserialize( file_get_contents($token_file_path ) ) : false;
    if ( !empty ( $token ) && !empty( $token['access_token'] ) ) {
        $credentials = new SalesforceLogin( $token );
    } else {
        require_once '/home/poslavu/private_html/salesforceCredentials.php';
        $credentials = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
        file_put_contents( $token_file_path, serialize($credentials->get_all() ) );
    }
    $account_meta_file = '/home/poslavu/private_html/salesforceAccountMeta.txt';
    $account_meta = file_exists( $account_meta_file ) ? file_get_contents($account_meta_file ) : null;
    $account_meta = @unserialize( $account_meta );
    $contact_meta_file = '/home/poslavu/private_html/salesforceContactMeta.txt';
    $contact_meta = file_exists( $contact_meta_file  ) ? file_get_contents( $contact_meta_file ) : null;
    $contact_meta = @unserialize( $contact_meta );
    $opportunity_meta_file = '/home/poslavu/private_html/salesforceOpportunityMeta.txt';
    $opportunity_meta = file_exists( $opportunity_meta_file ) ? file_get_contents( $opportunity_meta_file ) : null;
    $opportunity_meta = @unserialize( $opportunity_meta );
    $account_handler = new SalesforceAccount( $credentials, $account_meta );
    $contact_handler = new SalesforceContact( $credentials, $contact_meta );
    $opportunity_handler = new SalesforceOpportunity( $credentials, $opportunity_meta );
    $html_buffer = "";
    $all_rows = array();
    while ( $row = mysqli_fetch_assoc( $results ) ) {
        $all_rows[] = $row;
    }
    mysqli_free_result($results);
    foreach ($all_rows as $curr_row ){
        //print_r( $curr_row );  //debug

        $curr_row['stage'] = "Prospecting";

        if ( !empty( $curr_row['data_name'] ) ) {
            $curr_row['stage'] = "Trial - Reseller";
            $curr_row = array_merge( SalesforceIntegration::getAccountInfo($curr_row['data_name']), $curr_row );
            $signups_data = mlavu_query(<<<SQL
select `package`, `restaurantid`, `salesforce_id` from `signups` where `dataname`!='' and `dataname`='[data_name]' order by `signup_timestamp` desc limit 1
SQL
, $curr_row);
            if ( $signups_data ) {
                $package_info = mysqli_fetch_assoc( $signups_data );
                mysqli_free_result($signups_data);
            }
            $package = $package_container->get_package_by_attribute('level', $package_info['package']);
            if ( !empty( $package ) ) {
                $curr_row['level'] = $package->get_plain_name();
                $curr_row['lead_license_type'] = $package->get_plain_name();
            }
            if ( !empty( $package_info['restaurantid'] ) ) {
                $curr_row['restaurantid'] = $package_info['restaurantid'];
            }
            if ( strpos( strtolower($curr_row['lead_source']), 'nocc') !== false ) {
                $curr_row['stage'] = "Trial - No CC";
            }
        }
        if ( !empty( $curr_row['distro_code'] ) ) {
            $distro_account_response = $account_handler->query("SELECT Id FROM Account WHERE Distro_Username_Reseller__c='{$curr_row['distro_code']}' AND Type='Reseller'" );
            if ( !empty( $distro_account_response['records'] ) ) {
                echo "\n\nDistro found\n\n";
                print_r( $distro_account_response['records'] );
                $distro_account_id = $distro_account_response['records'][0]['Id'];
                $curr_row["ParentId"] = $distro_account_id;
            }
        }
        if ( preg_match('/_contact/',  $curr_row['lead_source']) ) {
            try {
                if ( empty( $lead_handler ) ){
                    $lead_handler = new SalesforceLead( $credentials );
                }
                $response = $lead_handler->upsert('Reseller_Lead_ID__c', $curr_row['lead_id'], array(
                    //TODO
                    "Address__c" => $curr_row['address'],
                    "FirstName" => empty( $curr_row['firstname'] ) ? "N/A": $curr_row['firstname'],
                    "LastName" => empty( $curr_row['lastname'] ) ? "N/A" : $curr_row['lastname'],
                    "Phone" => empty( $curr_row['phone'] ) ? "N/A" : $curr_row['phone'],
                    "Country__c" => $curr_row['country'],
                    "Company" => empty( $curr_row['company_name'] ) ? "N/A" : $curr_row['company_name'],
                    "Email" => empty( $curr_row['email'] ) ? "N/A" : $curr_row['email'],
                    "Description__c" => $curr_row['notes'],
                    "Description__c" => $curr_row['notes'],
                    "Zip_Postal_Code__c" => $curr_row['zip'],
                    "State_Provence__c" => $curr_row['state'],
                    "IP_Address__c" => $curr_row['ipaddress'],
                ));
            } catch( SalesforceHandlerException $e ) {
                mail("tom@lavu.com", "Salesforce Error", $e->getMessage() ."\n\n". print_r( $data_array, true ));
                $response['success'] = false;
            }
        } else {
            $response = send_to_salesforce($curr_row);
        }
        if ( !empty( $response['success'] ) ) {
            $logstr = "------------------------\nSent To Salesforce:\n";
            $logstr .= $mysql_tstamp . "\n";
            foreach($curr_row as $k => $v) {
                $logstr .= $k . "=" . $v . "\n";
            }
            $html_buffer .= "<h2 style=\"text-align:center\">{$mysql_tstamp}</h2>";
            $html_buffer .= "<h2 style=\"text-align:center\">Lead #{$curr_row['lead_id']}</h2>";
            $html_buffer .= "<h3 style=\"text-align:center\">Account Information</h3>";
            $html_buffer .= array_to_table( $curr_row );
            fwrite($fp,$logstr);
            $leads_ids[] = $curr_row['lead_id'];

            if ( $curr_row['create'] == 'lead' && !empty($response['salesforce_lead_id']) ) {
                mlavu_query( "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `salesforce_lead_id` = '[salesforce_lead_id]' WHERE `id` = '[lead_id]' AND (`salesforce_lead_id` = '' OR `salesforce_lead_id` IS NULL)", array('salesforce_lead_id' => $response['salesforce_lead_id'], 'lead_id' => $curr_row['lead_id']) );
                if ( ConnectionHub::getConn('poslavu')->affectedRows() === 0 ) {
                    $msg = __FILE__ ." could not set Salesforce Lead ID (". $response['salesforce_lead_id'] .") on `reseller_leads` for dataname=". $curr_row['data_name'] .": ". mlavu_dberror();
                    error_log($msg);
                    mail('tom@lavu.com', 'Error Setting Salesforce Lead IDs', $msg);
                }
            }
            // Record the Salesforce IDs on the `reseller_leads` table, if it's not already already set.
            else if ( $curr_row['create'] != 'lead' && empty($curr_row['salesforce_id']) ) {
                mlavu_query( "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `salesforce_id` = '[salesforce_id]' WHERE `id` = '[lead_id]' AND (`salesforce_id` = '' OR `salesforce_id` is null)", array('salesforce_id' => $response['id'], 'lead_id' => $curr_row['lead_id']) );
                if ( ConnectionHub::getConn('poslavu')->affectedRows() === 0 ) {
                    $msg = __FILE__ ." could not set Salesforce ID (". $response['id'] .") or Salesforce Opportunity ID (". $response['opportunity_id'] .") on `reseller_leads` for dataname=". $curr_row['data_name'] .": ". mlavu_dberror();
                    error_log($msg);
                    mail('tom@lavu.com', 'Error Setting Salesforce IDs', $msg);
                }
            }

            // Record the Salesforce ID and Salesforce Opportunity ID on the `signups` table, if they're not already already set.
            if ( $curr_row['create'] != 'lead' && empty($package_info['salesforce_id']) && (!empty($curr_row['data_name']) || !empty($curr_row['signupid'])) ) {
                list($lookupfield, $lookupfieldkey) = empty($curr_row['data_name']) ? array('id', 'signupid') : array('dataname', 'data_name');
                mlavu_query( "UPDATE `poslavu_MAIN_db`.`signups` SET `salesforce_id` = '[salesforce_id]', `salesforce_opportunity_id` = '[salesforce_opportunity_id]' WHERE `{$lookupfield}` = '[{$lookupfield}]' AND (`salesforce_id` = '' OR `salesforce_id` is null)", array('salesforce_id' => $response['id'], 'salesforce_opportunity_id' => $response['opportunity_id'], $lookupfield => $curr_row[$lookupfieldkey]) );
                if ( ConnectionHub::getConn('poslavu')->affectedRows() === 0 ) {
                    $msg = __FILE__ ." could not set Salesforce ID (". $response['id'] .") and Salesforce Opportunity ID (". $response['opportunity_id'] .") on `signups` for {$lookupfield}=". $curr_row[$lookupfieldkey] .": ". mlavu_dberror();
                    error_log($msg);
                    mail('tom@lavu.com', 'Error Setting Salesforce IDs', $msg);
                }
            }
        }
        // Mark record as errored so it doesn't keep getting processed over and over, creating duplicate emails
        // (Once the error is fixed, the record can just have its sent_to_salesforce reset to "0000-00-00 00:00:00" and its salesforce_lead_id blanked out)
        else {
            mlavu_query( "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `salesforce_lead_id` = 'error', `salesforce_sent` = '{$mysql_tstamp}' WHERE `id` = '[lead_id]' AND (`salesforce_lead_id` = '' OR `salesforce_lead_id` IS NULL)", $curr_row );
            if ( ConnectionHub::getConn('poslavu')->affectedRows() === 0 ) {
                $msg = __FILE__ ." could not set Salesforce Lead ID (". $response['salesforce_lead_id'] .") on `reseller_leads` for dataname=". $curr_row['data_name'] .": ". mlavu_dberror();
                error_log($msg);
                mail('tom@lavu.com', 'Error Setting Salesforce Lead IDs', $msg);
            }
        }
    }
    require_once "/home/poslavu/public_html/register/mailer.php";
    $ids_list = join(',', $leads_ids );
    $hostname = php_uname('n');  // because this script typically runs from the command-line and $_SERVER isn't available
    send_multipart_email( "noreply@poslavu.com", "Lead Sent to Salesforce ({$hostname})", html_email_template($html_buffer), "", "tom@lavu.com" );
    $update_query = <<<SQL
UPDATE `reseller_leads` SET `salesforce_sent`='$mysql_tstamp' WHERE id IN ( $ids_list )
SQL;
        mlavu_query( $update_query );
    
    fclose($fp);
}
