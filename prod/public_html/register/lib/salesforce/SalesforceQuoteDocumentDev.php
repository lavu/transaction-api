<?php

$start_time = microtime(true);

require_once('/home/poslavu/private_html/salesforceCredentials.php');
require_once('SalesforceLogin.php');
require_once('SalesforceQuote.php');

define( 'DEV', 1 );

$quoteId = '0Q0F0000000jonf';

$sfLogin = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
$sfQuote = new SalesforceQuote($sfLogin);

echo "quoteId={$quoteId} sfQuoteDocuments=". print_r($sfQuote->getQuoteDocuments($quoteId), true) ."\n";

$end_time = microtime(true);
$time_elapsed = $end_time - $start_time;
echo "script execution time: {$time_elapsed} s\n";

exit;