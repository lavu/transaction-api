<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/17/14
 * Time: 11:52 AM
 */

class SalesforceContactUnitTester extends PHPUnit_Framework_TestCase {
    protected $sforce;

    public static function setUpBeforeClass () {
        //require 'SalesforceContact.php';
        //require 'SalesforceAccount.php';
        require 'SalesforceOpportunity.php';
        require '/home/poslavu/private_html/salesforceCredentials.php';
        //$GLOBALS['contact_handler'] = SalesforceContact::create(SFORCE_UNAME,SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
        //$GLOBALS['account_handler'] = SalesforceAccount::create(SFORCE_UNAME,SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
        $GLOBALS['opportunity_handler'] = SalesforceOpportunity::create(SFORCE_UNAME,SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
    }

    public function setUp(){
        //$this->contactHandler = &$GLOBALS['contact_handler'];
        //$this->accountHandler = &$GLOBALS['account_handler'];
        $this->opportunityHandler = &$GLOBALS['opportunity_handler'];
    }

//    public function testConnect() {
//        $expected_returned = array(
//            "username",
//            "password",
//            "client_id",
//            "client_secret",
//            "id",
//            "instance",
//            "signature",
//            "access_token",
//            "token_timestamp",
//            "version",
//            "version_label",
//            "version_url"
//        );
//        foreach( $expected_returned as $expected ) {
//            $val = $this->sforce->get_var($expected);
//            echo "checking for \"{$expected}\"\nvalue:$val\n\n";
//            $this->assertTrue( !empty( $val ) );
//        }
//    }

//    public function testGetResources() {
//        $resources = $this->sforce->get_all_resources();
//        // print_r( $resources );
//        $this->assertNotEmpty( $resources );
//        $this->assertTrue( is_array( $resources ) );
//    }
//
//    public function testGetSObjects() {
//        $sobjects = $this->sforce->get_all_sobjects();
//        echo "== SObjects ==\n\n";
//        print_r( $sobjects );
//        $this->assertNotEmpty( $sobjects );
//        $this->assertTrue( is_array( $sobjects ) );
//    }

    public function testContactMetadata () {
        $meta = $this->opportunityHandler->get_meta();
        echo "=== Opportunity Object Metadata ===\n\n";
        print_r( $meta );
        $this->assertNotEmpty( $meta );
        $this->assertTrue( is_array( $meta ) );
        // list all the possible field in a Contact just for fun
        $this->assertNotEmpty( $meta['fields'] );
        foreach( $meta['fields'] as $field ) {
            echo "Name:{$field['name']} // Label:{$field['label']} // Type:{$field['type']}\n";
        }
    }

//    public function testInsertNGetContact() {
//        $test_account = array(
//            "Name" => "DooDoo Cupcakes Co.",
//            "BillingStreet" => "163 Brownies Ave NE",
//            "BillingCity" => "Brownsville",
//            "BillingState" => "TX",
//            "BillingPostalCode" => 78522,
//            "BillingCountry" => "US",
//            "Phone" => "5057159482",
//            "Description" => <<<TXT
//This is a test, please do not panic and ignore this message
//TXT
//        );
//        $test_contact = array(
//            "Salutation" => "Dr.",
//            "FirstName" => "Johnny",
//            "LastName" => "Cupcakes",
//            "Phone" => $test_account['Phone'],
//            "Email" => "johnny@cupcakes.crap",
//            "LeadSource" => "WebSignupContact",
//            "MailingStreet" => $test_account['BillingStreet'],
//            "MailingCity" => $test_account['BillingCity'],
//            "MailingState" => $test_account['BillingState'],
//            "MailingPostalCode" => $test_account['BillingPostalCode'],
//            "MailingCountry" => $test_account['BillingCountry'],
//        );
//        $insert_response = $this->accountHandler->insert( $test_account );
//        echo "== Insert Account Response ==\n\n";
//        print_r( $insert_response );
//        $this->assertNotEmpty( $insert_response );
//        $this->assertNotEmpty( $insert_response['success'] );
//        $account_sforce_id = $insert_response['id'];
//        $this->assertNotEmpty( $account_sforce_id );
//        // get the same Account back
//        $retrieved_account = $this->accountHandler->get_by_id( $account_sforce_id );
//        echo "== Retrieved Account ==\n\n";
//        print_r( $retrieved_account );
//        $this->assertNotEmpty( $retrieved_account );
//        $this->assertEquals( $retrieved_account['Id'], $account_sforce_id );
//        foreach( $test_account as $test_fields => $value ) {
//            $this->assertEquals( $retrieved_account[$test_fields], $value );
//        }
//        // Now let's create a Contact for this account
//        echo "== Insert Contact Response ==\n\n";
//        $test_contact['AccountId'] =  $account_sforce_id;
//        $contact_insert_response = $this->contactHandler->insert( $test_contact );
//        print_r( $contact_insert_response );
//        //now let's delete it
//        $delete_response = $this->accountHandler->delete_by_id( $account_sforce_id );
//        echo "== Delete Response ==\n\n";
//        print_r( $delete_response );
//        $this->assertNull( $delete_response );
//        // Now we should try to get the account and but it is not there
//        $retrieved_account = $this->accountHandler->get_by_id( $account_sforce_id );
//        echo "== Retrieved Contact After Delete ==\n\n";
//        print_r( $retrieved_account );
//        $this->assertNotEmpty( $retrieved_account );
//        $this->assertEquals( $retrieved_account[0]['errorCode'], "NOT_FOUND" );
//        $this->assertArrayNotHasKey( "Id",  $retrieved_account );
//        $this->assertArrayNotHasKey( "Name", $retrieved_account );
//    }

//    public function testGetNUpdate () {
//        // get one of the dummy accounts that are on by default in the
//        // developer account
//        $retrieved_account_id = $this->sforce->get_by_field("Name", "University of Arizona");
//        echo "== Retrieved account ==\n\n";
//        $this->assertNotNull( $retrieved_account_id );
//        print_r( $retrieved_account_id );
//        $account_id = $retrieved_account_id['records'][0]['Id'];
//        echo "== account Id : $account_id\n\n";
//        $retrieved_account = $this->sforce->get_by_id( $account_id );
//        echo "== retrieved account\n";
//        print_r( $retrieved_account );
//        $account_phone = $retrieved_account['Phone'];
//        $account_fax = $retrieved_account['Fax'];
//        $this->assertNotNull( $account_id );
//        $this->assertNotNull( $account_phone );
//        $this->assertNotNull( $account_fax );
//        // let's change the name now
//        $new_contact = array(
//            "Phone" => "5057159482",
//            "Fax" => "5057159482"
//        );
//        $update_response = $this->sforce->update_by_id($account_id, $new_contact );
//        $this->assertNull( $update_response );
//        // let's see if it worked
//        $account_again = $this->sforce->get_by_id( $account_id );
//        $this->assertEquals( $new_contact['Phone'],  $account_again['Phone'] );
//        $this->assertEquals( $new_contact['Fax'],  $account_again['Fax'] );
//        // Turn it back to what it was
//        $old_values = array( "Phone" => $account_phone, "Fax" => $account_fax );
//        $update_response = $this->sforce->update_by_id($account_id, $old_values );
//        $this->assertNull( $update_response );
//        // let's see if it worked
//        $account_again = $this->sforce->get_by_id( $account_id );
//        $this->assertEquals( $account_again['Phone'],  $account_phone );
//        $this->assertEquals( $account_again['Fax'],  $account_fax );
//    }
//
//    public function testKillNRevive() {
//        // let's create a test account just for shits and giggles
//        $test_account = array(
//            "Name" => "McTurdies Inc.",
//            "BillingStreet" => "163 Brownies Ave NE",
//            "BillingCity" => "Brownsville",
//            "BillingState" => "TX",
//            "BillingPostalCode" => 78522,
//            "BillingCountry" => "US",
//            "Description" => <<<TXT
//McTurdies, the best peanut butter sandwiches in the southwest
//TXT
//        );
//        $insert_response = $this->sforce->insert( $test_account );
//        // make sure to get all the old properties before killing
//        $old_instance_properties = $this->sforce->get_all();
//        // now let's kill it
//        $this->sforce = null;
//        // .. and revive
//        $this->sforce = Salesforceaccount::revive( $old_instance_properties );
//        // Now let's do something to make sure it works
//        $this->sforce->delete_by_id( $insert_response['id'] );
//        // Let's create a new account just for shits and giggles
//        $new_account_response = $this->sforce->insert( array(
//            "Name" => "Test Co.",
//            "BillingStreet" => "163 Testing Grounds",
//            "BillingCity" => "Testville",
//            "BillingState" => "TX",
//            "BillingPostalCode" => 78522,
//            "BillingCountry" => "US",
//            "Description" => <<<TXT
//This is another test, if you see me, it's all good
//TXT
//        ) );
//        print_r( $new_account_response );
//        $this->assertArrayHasKey( "id", $new_account_response );
//    }
//
//    public function testUpsert() {
//        $test_account = array(
//            "Name" => "Upsert & Co.",
//            "BillingStreet" => "Upserting Ave APT 111",
//            "BillingState" => "GA",
//            "BillingCity" => "Georgia",
//            "BillingPostalCode" => 30004,
//            "BillingCountry" => "US",
//            "Description" => "Testing the Upserting function"
//        );
//        $upsert_response = $this->sforce->upsert( 666, $test_account );
//        echo "=== Upsert Response ===\n\n";
//        print_r( $upsert_response );
//        $this->assertTrue( $upsert_response['success']);
//        if ( empty( $upsert_response['updated'] ) ){
//            $this->assertArrayHasKey(  "id", $upsert_response );
//            $account_id  = $upsert_response['id'];
//            $account  = $this->sforce->get_by_id( $account_id );
//            foreach( $account as $k=>$v ){
//                if ( !empty( $test_account[$k] ) ) {
//                    $this->assertEquals( $test_account[$k], $v );
//                }
//            }
//            // now let's change a couple of things
//            $test_account['BillingStreet'] = "123 Test Rd";
//            $test_account['BillingState'] = "AL";
//            $test_account['BillingCity'] = "Mobile";
//            $test_account['BillingPostalCode'] = "36601";
//            $upsert_response = $this->sforce->upsert_by( 666,  $test_account );
//            print_r( $upsert_response );
//            $this->assertArrayHasKey(  "success", $upsert_response );
//            $this->assertArrayHasKey(  "updated", $upsert_response );
//            //let's get it back to see if it worked
//            $account  = $this->sforce->get_by_id( $account_id );
//            foreach( $account as $k=>$v ){
//                if ( !empty( $test_account[$k] ) ) {
//                    $this->assertEquals( $test_account[$k], $v );
//                }
//            }
//        } else {
//            $this->assertArrayHasKey(  "success", $upsert_response );
//            $this->assertArrayHasKey(  "updated", $upsert_response );
//        }
//        //let's get it back to see if it worked
//    }
}