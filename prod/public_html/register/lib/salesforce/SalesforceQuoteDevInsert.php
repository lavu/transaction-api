<?php

require_once('/home/poslavu/private_html/salesforceCredentialsDev.php');
require_once('SalesforceLogin.php');
require_once('SalesforceQuote.php');

define( 'DEV', 1 );

$sfOpportunityId = '006c000000DnvRpAAJ';

$sfLogin = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
//echo json_encode( $sfLogin->get_all_sobjects(), JSON_PRETTY_PRINT ) ."\n";

$sfQuote = new SalesforceQuote( $sfLogin );
//echo json_encode($sfQuote->get_meta(), JSON_PRETTY_PRINT) . "\n";

//echo "sfQuotePropertyName=". $sfQuote->get_property( $sfQuotePropertyName, $sfQuoteExternalId ) ."\n";

/*
$properties = array(
	'Name',
	'Id',
	'Subtotal',
	'TotalPrice',
	'ShippingCountry',
	'Tax',
	'GrandTotal',
	'LineItemCount',
	'BillingCountry',
);
echo "sfQuoteProperties=". print_r( $sfQuote->get_properties( $properties, $sfQuoteExternalId ), true ) ."\n";
*/

$quote_data = array(
	'Name' => 'Lavu Test Quote (Shipping Address Edition)',
	'Status' => 'Needs Review',
	'Reseller_Quote_ID__c' => '123456',
	'OpportunityId' => $sfOpportunityId,
	'BillingName' => 'Lavu Test Quote (Address Edition)',
	'BillingStreet' => '116 Central Ave SW, Suite 300',
	'BillingCity' => 'Albuquerque',
	'BillingStateCode' => 'NM',
	'BillingPostalCode' => '87102',
	'BillingCountryCode' => 'US',
/*	'ShippingName' => 'Lavu Test Quote (Address Edition)',
	'ShippingStreet' => '116 Central Ave SW, Suite 300',
	'ShippingCity' => 'Albuquerque',
	'ShippingStateCode' => 'NM',
	'ShippingPostalCode' => '87102',
	'ShippingCountryCode' => 'US',
*/	'Same_as_Billing__c' => true,
);

$result = $sfQuote->insert( $quote_data );

echo 'result='. print_r($result, true) ."\n";

exit;