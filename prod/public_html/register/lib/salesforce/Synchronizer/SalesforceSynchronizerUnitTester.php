<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 1/29/15
 * Time: 12:39 PM
 */

class SalesforceSynchronizerUnitTester extends PHPUnit_Framework_TestCase {
    protected $synchro;

    public static function setUpBeforeClass(){
        require dirname(__FILE__) . '/SalesforceSynchronizer.php';
    }

    public function setUp(){
        $this->synchro = new SalesforceSynchronizer();
        file_put_contents('html-log.html', <<<HTML
<!doctype html>
<html>
<head>
<title>Salesforce Synchronizer </title>
<style>
html,body{
    margin:0;
    padding:0;
    font-family:"Fira Mono Medium", "Monaco", monospace;
    font-size:14px;
    background-color:#f2f2f4;
    line-height:1.5rem;
}
#console{
    padding:1.5rem;
    white-space: pre;
    max-width:640px;
    margin:0 auto;
}
</style>
</head><body><div id="console">
HTML
       );
    }

    private static function to_log( $str ) {
        file_put_contents('html-log.html', "$str\n", FILE_APPEND);
    }

    public function testGetRows() {
        $rows = $this->synchro->get_rows();
        self::to_log("Rows:\n". print_r( $rows, true ) );
        $this->assertNotEmpty( $rows );
    }

    public function testRun() {
        $this->assertTrue( $this->synchro->run() );
    }


    public static function tearDownAfterClass()
    {
        self::to_log("</div></body></html>");
    }

}