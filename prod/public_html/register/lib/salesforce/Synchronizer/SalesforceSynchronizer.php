<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 1/29/15
 * Time: 12:09 PM
 */
$file_dir = dirname(__FILE__);

require_once "$file_dir/../../../../admin/cp/resources/lavuquery.php";
require_once "$file_dir/../SalesforceAccount.php";
require_once "$file_dir/../SalesforceOpportunity.php";
require_once "$file_dir/../SalesforceContact.php";
require_once "$file_dir/../../utilities.php";
require_once "$file_dir/../../../../admin/sa_cp/billing/procareas/SalesforceIntegration.php";

class SalesforceSynchronizer {

    protected $rows;
    protected $rows_ids;
    protected $salesforce_final_stages = array(
        "Cancelled",
        "Closed Won",
        "Trial - Reseller",
        "Cancelled - Trial"
    );
    protected function load_rows () {
        $query = <<<SQL
SELECT
    `id`,
    `createtime`,
    `updatetime`,
    `dataname`,
    `event`,
    `details`,
    `processed`
FROM `billing_salesforce_events` WHERE `event` = 'stage_change' AND `processed` = ''
SQL;
        $results = mlavu_query( $query );
        $this->rows = array();
        while ( $row = mysqli_fetch_assoc( $results ) ) {
            $this->rows[] = $row;
            $this->rows_ids[] = $row['id'];
        }
    }

    public function get_rows() {
        return $this->rows;
    }

    public function __construct() {
        $this->load_rows();
    }

    public function there_is_stuff_to_do() {
        return !empty( $this->rows );
    }

    public function mark_rows_as_in_progress() {
        if ( empty( $this->rows ) ) {return;}
        $ids = array();
        foreach( $this->rows as $row ){
            $ids[] = $row['id'];
        }
        $ids = join(',', $ids);
        $query = <<<SQL
UPDATE `billing_salesforce_events` SET `processed`='in_progress' WHERE `id` in ( $ids )
SQL;
        mlavu_query( $query );
        $error = mlavu_dberror();
        if ( empty( $error ) ) {
            return true;
        } else {
            throw new Exception( __CLASS__ . "::mysql error='$error'--".
                __FILE__ . ":L" . __LINE__ );
            return false;
        }
    }

    private function connect_to_salesforce(){
        $this->login_credentials = new SalesforceLogin(
            SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET
        );
    }

    public function run(){
        if ( !$this->there_is_stuff_to_do() ) {return 0;}
        $this->mark_rows_as_in_progress();
        //$this->connect_to_salesforce();
        $successful_events_ids = array();
        $failure_events_ids = array();
        foreach( $this->rows as $row ) {
            if ( empty( $row['dataname'] ) ){
                $this->mark_rows_as_unprocessed($this->rows_ids);
                throw new Exception(__CLASS__. "::row doesn't have a dataname --" .
                __FILE__ . ':L'. __LINE__ );
            }
            $query = <<<SQL
SELECT * from `reseller_leads` WHERE `data_name` = '{$row['dataname']}'
SQL;
            try {
            $account_info = SalesforceIntegration::getAccountInfo( $row['dataname'] );
            } catch ( Exception $e ) {
                error_log(print_r($e));
                continue;
            }
            $results = mlavu_query( $query );
            $lead = null;
            if ( mysqli_num_rows( $results ) ) {
                //$reseller_leads = array();
                $lead = mysqli_fetch_assoc( $results );
            } else {
                // let's try getting it from the signups row
                $query = <<<SQL
SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname` = '[1]'
SQL;
                $results = mlavu_query( $query, $row['dataname']);
                if ( mysqli_num_rows( $results ) ) {
                    $lead = mysqli_fetch_assoc( $results );
                    unset($lead['id']);  // send_to_salesforce() expects id to be reseller_lead ID
                } else {
                    // if we cannot find it in signups either, then we just throw the Exception and called it a day
                    //$this->mark_rows_as_unprocessed($this->rows_ids);
                    throw new Exception(__CLASS__. <<<LOG
::can't find a corresponding `signups` table entry by dataname --
LOG
                        . __FILE__ . ':L'. __LINE__ );
                }
            }
            $lead['stage'] = $row['details'];
            if ( in_array($lead['stage'], $this->salesforce_final_stages )  ) {
                $lead['closeDate'] = date( 'r',strtotime($row['updatetime']) );
            }
            $went_well = send_to_salesforce(array_merge($lead, $account_info ) );
            if ( $went_well['success'] ) {
                if ( !empty( $went_well['id'] ) ) {
                    $query = <<<SQL
UPDATE `poslavu_MAIN_db`.`signups` SET `salesforce_id`='[1]' WHERE `dataname` LIKE '[2]'
SQL;
                    mlavu_query( $query, $went_well['id'], $row['dataname'] );
                    $query = <<<SQL
UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `salesforce_id`='[1]' WHERE `data_name` LIKE '[2]'
SQL;
                    mlavu_query( $query, $went_well['id'], $row['dataname'] );
                }
                $successful_events_ids[] = $row['id'];
            }
            else {
                $failure_events_ids[] = $row['id'];
            }
        }
        $this->mark_rows_as_unprocessed( $failure_events_ids );
        $this->mark_rows_as_completed( $successful_events_ids );
        return true;
    }

    protected function mark_rows_as_completed( $ids_array ) {
        if ( empty( $ids_array) ) return null;
        $ids = join(',', $ids_array );
        $timestamp = date('Y-m-d H:m:s');
        $query = <<<SQL
UPDATE `billing_salesforce_events` SET `processed`='$timestamp' WHERE `id` in ( $ids )
SQL;
        mlavu_query( $query );
        $error = mlavu_dberror();
        if ( empty( $error ) ) {
            return true;
        } else {
            throw new Exception( __CLASS__ . "::mysql error='$error'||$query--".
                __FILE__ . ":L" . __LINE__ );
            return false;
        }
    }

    protected function mark_rows_as_unprocessed( $ids_array ) {
        if ( empty( $ids_array) ) return null;
        $ids = join(',', $ids_array );
        $query = <<<SQL
UPDATE `billing_salesforce_events` SET `processed`='error' WHERE `id` in ( $ids )
SQL;
        mlavu_query( $query );
        $error = mlavu_dberror();
        if ( empty( $error ) ) {
            return true;
        } else {
            throw new Exception( __CLASS__ . "::mysql error='$error'--".
                __FILE__ . ":L" . __LINE__ );
            return false;
        }
    }
}