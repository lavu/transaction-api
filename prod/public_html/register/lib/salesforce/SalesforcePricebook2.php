<?php

require_once "SalesforceLogin.php";
require_once "SalesforceObject.php";

class SalesforcePricebook2 extends SalesforceObject {

    public function __construct( SalesforceLogin $credentials, $meta=null )
    {
        $this->_object_name = 'Pricebook2';
        $this->_credentials = $credentials;
        $this->_metadata = empty( $meta ) ? $this->get_meta() : $meta;
    }
}