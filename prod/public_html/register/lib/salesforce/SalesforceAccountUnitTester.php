<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/13/14
 * Time: 3:41 PM
 */

class SalesforceAccountUnitTester extends PHPUnit_Framework_TestCase {
    protected $sforce;

    public static function setUpBeforeClass () {
        require 'SalesforceAccount.php';
        require '/home/poslavu/private_html/salesforceCredentials.php';
        $GLOBALS['credentials'] = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
    }

    public function setUp(){
        $this->sforce = new SalesforceAccount( $GLOBALS['credentials'] );
    }

    public function testAccountMetadata () {
        $meta = $this->sforce->get_meta();
        echo "=== Accounts Object Metadata ===\n\n";
        print_r( $meta );
        $this->assertNotEmpty( $meta );
        $this->assertTrue( is_array( $meta ) );
        // list all the possible field in a Account just for fun
        $this->assertNotEmpty( $meta['fields'] );
        //foreach( $meta['fields'] as $field ) {
        //    echo "Name:{$field['name']} // Label:{$field['label']} // Type:{$field['type']}\n";
        //}
    }

    public function testInsertNGetAccount() {
        $test_Account = array(
            "Name" => "DooDoo Cupcakes Co.",
            "Address__c" => "163 Brownies Ave NE",
            "City__c" => "Brownsville",
            "State_Provence__c" => "TX",
            "Zip_Postal_Code__c" => 78522,
            "Country__c" => "US",
            "Description__c" => <<<TXT
This is a test, please do not panic and ignore this message
TXT
        );
        $insert_response = $this->sforce->insert( $test_Account );
        echo "== Insert Response ==\n\n";
        print_r( $insert_response );
        $this->assertNotEmpty( $insert_response );
        $this->assertNotEmpty( $insert_response['success'] );
        $Account_sforce_id = $insert_response['id'];
        $this->assertNotEmpty( $Account_sforce_id );
        // get the same Account back
        $retrieved_Account = $this->sforce->get_by_id( $Account_sforce_id );
        echo "== Retrieved Account ==\n\n";
        print_r( $retrieved_Account );
        $this->assertNotEmpty( $retrieved_Account );
        $this->assertEquals( $retrieved_Account['Id'], $Account_sforce_id );
        foreach( $test_Account as $test_fields => $value ) {
            $this->assertEquals( $retrieved_Account[$test_fields], $value );
        }
        // now let's delete it
        echo "== Deleting Johnny Cupcakes Account ==\n";
        $delete_response = $this->sforce->delete_by_id( $Account_sforce_id );
        echo "== Delete Response ==\n\n";
        print_r( $delete_response );
        $this->assertNull( $delete_response );
        // Now we should try to get the Account and but it is not there
        $retrieved_Account = $this->sforce->get_by_id( $Account_sforce_id );
        echo "== Retrieved Account After Delete ==\n\n";
        print_r( $retrieved_Account );
        $this->assertNotEmpty( $retrieved_Account );
        $this->assertEquals( $retrieved_Account[0]['errorCode'], "NOT_FOUND" );
        $this->assertArrayNotHasKey( "Id",  $retrieved_Account );
        $this->assertArrayNotHasKey( "Name", $retrieved_Account );
    }

    public function testUpsert() {
        $test_account = array(
            "Name" => "Upsert & Co.",
            "Address__c" => "Upserting Ave APT 111",
            "State_Provence__c" => "GA",
            "City__c" => "Georgia",
            "Zip_Postal_Code__c" => 30004,
            "Country__c" => "US",
            "Description__c" => "Testing the Upserting function",
            "Phone" => 5057159482
        );
        $upsert_response = $this->sforce->upsert( "Lead_ID__c", 666, $test_account );
        echo "=== Upsert Response ===\n\n";
        print_r( $upsert_response );
        $this->assertTrue( $upsert_response['success']);
        if ( empty( $upsert_response['updated'] ) ){
            $this->assertArrayHasKey(  "id", $upsert_response );
            $account_id  = $upsert_response['id'];
            $account  = $this->sforce->get_by_id( $account_id );
            foreach( $account as $k=>$v ){
                if ( !empty( $test_account[$k] ) ) {
                    $this->assertEquals( $test_account[$k], $v );
                }
            }
            // now let's change a couple of things
            $test_account['Address__c'] = "123 Test Rd";
            $test_account['State_Provence__c'] = "AL";
            $test_account['City__c'] = "Mobile";
            $test_account['Zip_Postal_Code__c'] = "36601";
            $upsert_response = $this->sforce->upsert( "Lead_ID__c", 666,  $test_account );
            print_r( $upsert_response );
            $this->assertArrayHasKey(  "success", $upsert_response );
            $this->assertArrayHasKey(  "updated", $upsert_response );
            //let's get it back to see if it worked
            $account  = $this->sforce->get_by_id( $account_id );
            foreach( $account as $k=>$v ){
                if ( !empty( $test_account[$k] ) ) {
                    $this->assertEquals( $test_account[$k], $v );
                }
            }
        } else {
            $this->assertArrayHasKey(  "success", $upsert_response );
            $this->assertArrayHasKey(  "updated", $upsert_response );
        }
        //let's get it back to see if it worked
    }

    public function testGetNUpdate () {
        // get one of the dummy accounts that are on by default in the
        // developer account
        $retrieved_account_id = $this->sforce->get_by_field("Name", "Upsert & Co.");
        echo "== Retrieved account ==\n\n";
        $this->assertNotNull( $retrieved_account_id );
        print_r( $retrieved_account_id );
        $account_id = $retrieved_account_id['records'][0]['Id'];
        echo "== account Id : $account_id\n\n";
        $retrieved_account = $this->sforce->get_by_id( $account_id );
        echo "== retrieved account\n";
        print_r( $retrieved_account );
        $account_phone = $retrieved_account['Phone'];
        $this->assertNotNull( $account_id );
        $this->assertNotNull( $account_phone );
        // let's change the name now
        $new_contact = array(
            "Phone" => "5057159482"
        );
        $update_response = $this->sforce->update_by_id($account_id, $new_contact );
        $this->assertNull( $update_response );
        // let's see if it worked
        $account_again = $this->sforce->get_by_id( $account_id );
        $this->assertEquals( $new_contact['Phone'],  $account_again['Phone'] );
        // Turn it back to what it was
        $old_values = array( "Phone" => $account_phone );
        $update_response = $this->sforce->update_by_id($account_id, $old_values );
        $this->assertNull( $update_response );
        // let's see if it worked
        $account_again = $this->sforce->get_by_id( $account_id );
        $this->assertEquals( $account_again['Phone'],  $account_phone );
    }
}