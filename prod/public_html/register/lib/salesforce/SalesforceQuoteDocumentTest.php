<?php
require 'SalesforceQuoteDocument.php';
require '/home/poslavu/private_html/salesforceCredentials.php';

$credentials = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
$meta_data_file = '/home/poslavu/private_html/SalesforceQuoteDocument.txt';
$meta_data = file_exists( $meta_data_file ) ? @unserialize( file_get_contents( $meta_data_file ) ) : null;
$handler = new SalesforceQuoteDocument( $credentials, $meta_data );

$quote_documents_results = $handler->get_by_field('QuoteId', '0Q0F0000000jgfn');

echo print_r( $quote_documents_results, true) ."\n";

foreach ( $quote_documents_results['records'] as $quote_document_info ) {
	$id = $quote_document_info['Id'];
	$quote_document = $handler->get_by_id($id);
	echo "id={$id} ". print_r($quote_document, true) ."\n";
}