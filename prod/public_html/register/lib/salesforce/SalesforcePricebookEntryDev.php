<?php

define('DEV', 0);  // must be before require_once()'s

$salesforceCredentialsFile = DEV ? '/home/poslavu/private_html/salesforceCredentialsDev.php' : '/home/poslavu/private_html/salesforceCredentialsProd.php';

require_once($salesforceCredentialsFile);
require_once('SalesforceLogin.php');
require_once('SalesforcePricebookEntry.php');

// Make the connection
$sfLogin = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
$sfPricebookEntry = new SalesforcePricebookEntry($sfLogin);

$pricebook2Id = DEV ? '01s560000000QdHAAU' : '01sF0000001tfrqIAA';
echo "DEV=". DEV ." pricebook2Id={$pricebook2Id}\n";  //debug

$pricebookEntries = $sfPricebookEntry->query("SELECT Name, Id, Product2Id, UnitPrice, UseStandardPrice, IsDeleted, IsActive FROM PricebookEntry WHERE Pricebook2Id = '{$pricebook2Id}' AND IsActive = true");
echo json_encode($pricebookEntries, JSON_PRETTY_PRINT) ."\n";

exit;