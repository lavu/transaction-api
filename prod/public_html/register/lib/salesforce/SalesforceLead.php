<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/12/14
 * Time: 12:33 PM
 */

require_once "SalesforceLogin.php";
require_once "SalesforceObject.php";

class SalesforceLead extends SalesforceObject {

    public function __construct ( SalesforceLogin $credentials ){
        $this->_object_name = "Lead";
        $this->_credentials = $credentials;
        $this->_metadata = $this->get_meta();
    }

}