<?php

require_once('/home/poslavu/private_html/salesforceCredentialsDev.php');
require_once('SalesforceLogin.php');
require_once('SalesforceQuote.php');

define( 'DEV', 1 );

$sfOpportunityId = '006c000000DnvRpAAJ';
$sfQuoteId = '0Q0c0000000XZog';

$sfLogin = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
//echo json_encode( $sfLogin->get_all_sobjects(), JSON_PRETTY_PRINT ) ."\n";

$sfQuoteLineItem = new SalesforceQuoteLineItem( $sfLogin );
//echo json_encode($sfQuote->get_meta(), JSON_PRETTY_PRINT) . "\n";

//echo "sfQuotePropertyName=". $sfQuote->get_property( $sfQuotePropertyName, $sfQuoteExternalId ) ."\n";

/*
$properties = array(
	'Name',
	'Id',
	'Subtotal',
	'TotalPrice',
	'ShippingCountry',
	'Tax',
	'GrandTotal',
	'LineItemCount',
	'BillingCountry',
);
echo "sfQuoteProperties=". print_r( $sfQuote->get_properties( $properties, $sfQuoteExternalId ), true ) ."\n";
*/

$quote_data = array(
	'Product2Id' => 'Lavu Test Quote (SalesforceQuoteDev Edition)',
	'Status' => 'Needs Review',
	'Reseller_Quote_ID__c' => '123',
	'OpportunityId' => $sfOpportunityId,
);

$result = $sfQuoteLineItem->insert( $quote_data );

echo 'result='. print_r($result, true) ."\n";

exit;