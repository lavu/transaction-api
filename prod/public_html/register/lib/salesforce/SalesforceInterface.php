<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/12/14
 * Time: 12:36 PM
 */

interface SalesforceInterface {
    public function get_by_id( $id );
    public function delete_by_id( $id );
    public function update_by_id( $id, $data );
    public function get_by_field( $field, $value );
    public function upsert( $field, $external_id, $data );
    public function insert( $data_array );
    public function get_meta();
    public function get_property($property_name, $id);
    public function get_properties($property_names, $id);
}