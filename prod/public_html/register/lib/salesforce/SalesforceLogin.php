<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/17/14
 * Time: 3:38 PM
 */

require_once "SalesforceHandler.php";

class SalesforceLogin extends SalesforceHandler{

    protected $_password;
    protected $_client_id;
    protected $_client_secret;
    protected $_username;
    protected $_sforce_id;
    protected $_instance;
    protected $_signature;
    protected $_token_timestamp;
    protected $_api_version_url;
    protected $_api_version;
    protected $_api_version_label;
    protected $_access_token;

    public function __construct () {
        $first_arg = func_get_arg( 0 );
        $num_args = func_num_args();
        if ( $num_args === 1 && is_array( $first_arg )){
            foreach( $first_arg as $property => $value ) {
                $this->set_var( $property, $value );
            }
            // check if the token is still fresh
            $is_token_fresh = (time() * 1000) < ( $this->_token_timestamp + 60 * 60 * 2 * 1000);
            if ( !$is_token_fresh ) {
                $this->acquire_access_token();
            }
            return;
        }
        if ( $num_args !== 4 ) {
            $err_msg = __CLASS__ . "::Incorrect number of arguments in the constructor";
            throw new SalesforceHandlerException( $err_msg );
        } else {
            list( $username, $password, $client_id, $client_secret ) = func_get_args();
            $this->_username = $username;
            $this->_password = $password;
            $this->_client_id = $client_id;
            $this->_client_secret = $client_secret;
            $this->acquire_access_token();
            // by default, use the latest API version
            $all_versions = $this->get_all_versions();
            $latest_version = array_pop($all_versions);
            $this->_api_version_label = $latest_version['label'];
            $this->_api_version_url = $latest_version['url'];
            $this->_api_version = $latest_version['version'];
        }
    }

    protected function acquire_access_token () {
        $url  = "https://login.salesforce.com/services/oauth2/token";
        $method = "POST";
        $data = array(
            "grant_type" => "password",
            "client_id" =>  $this->_client_id,
            "client_secret" => $this->_client_secret,
            "username" => $this->_username,
            "password" => $this->_password
        );
        $response = json_decode( self::do_curl( $url, $data, $method ), true );
        $this->_sforce_id = $response['id'];
        $this->_instance = $response['instance_url'];
        $this->_signature = $response['signature'];
        $this->_access_token = $response['access_token'];
        $this->_token_timestamp = $response['issued_at'];
        return true;
    }

    public function refresh() {
        $url  = "https://login.salesforce.com/services/oauth2/token";
        $method = "POST";
        $data = array(
            "grant_type" => "refresh_token",
            "client_id" =>  $this->_client_id,
            "client_secret" => $this->_client_secret,
            "refresh_token" => $this->_access_token
        );
        $response = json_decode( self::do_curl( $url, $data, $method ), true );
        $this->_sforce_id = $response['id'];
        $this->_instance = $response['instance_url'];
        $this->_signature = $response['signature'];
        $this->_access_token = $response['access_token'];
        $this->_token_timestamp = $response['issued_at'];
        return true;
    }

    public function get_all_versions(){
        $url = "{$this->_instance}/services/data/";
        return json_decode( self::do_curl( $url ), true );
    }

    public function get_all_resources() {
        $url = "{$this->_instance}{$this->_api_version_url}";
        $method = "POST";
        $no_data = null;
        $headers = array(
            "Authorization: Bearer {$this->_access_token}"
        ,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        return json_decode( self::do_curl($url, $no_data, $method, $headers ) ,true);
    }

    public function get_all_sobjects() {
        $url = "{$this->_instance}{$this->_api_version_url}/sobjects";
        $method = "GET";
        $no_data = null;
        $headers = array(
            "Authorization: Bearer {$this->_access_token}"
            ,"X-PrettyPrint:1" // if you want the json to be printed formatted
        );
        return json_decode( self::do_curl($url,  $no_data, $method, $headers ) ,true);
    }

    public function get_var( $key ) {
        if ( !is_string( $key ) ) {
            $err_msg = "function \"" . __FUNCTION__ .
                "\" only accepts strings as arguments";
            throw new SalesforceHandlerException( $err_msg );
        }
        switch( $key ) {
            case "username":
                return $this->_username;
            case "password":
                return $this->_password;
            case "client_id":
                return $this->_client_id;
            case "client_secret":
                return $this->_client_secret;
            case "id":
                return $this->_sforce_id;
            case "instance":
                return $this->_instance;
            case "signature":
                return $this->_signature;
            case "access_token" :
                return $this->_access_token;
            case "token_timestamp":
                return $this->_token_timestamp;
            case "version":
                return $this->_api_version;
            case "version_label":
                return $this->_api_version_label;
            case "version_url":
                return $this->_api_version_url;
            case "refresh_token":
                return $this->_refresh_token;
            default:
                $err_msg = "attribute does not exists";
                throw new SalesforceHandlerException( $err_msg );
        }
    }

    public function set_var( $key, $value ) {
        if ( !is_string( $key ) ) {
            $err_msg = "function \"" . __FUNCTION__ .
                "\" only accepts strings as arguments";
            throw new SalesforceHandlerException( $err_msg );
        }
        switch( $key ) {
            case "username":
                $this->_username = $value;
                break;
            case "password":
                $this->_password = $value;
                break;
            case "client_id":
                $this->_client_id = $value;
                break;
            case "client_secret":
                $this->_client_secret = $value;
                break;
            case "id":
                $this->_sforce_id = $value;
                break;
            case "instance":
                $this->_instance = $value;
                break;
            case "signature":
                $this->_signature = $value;
                break;
            case "access_token" :
                $this->_access_token = $value;
                break;
            case "token_timestamp":
                $this->_token_timestamp = $value;
                break;
            case "version":
                $this->_api_version = $value;
                break;
            case "version_label":
                $this->_api_version_label = $value;
                break;
            case "version_url":
                $this->_api_version_url = $value;
                break;
            case "refresh_token":
                $this->_refresh_token = $value;
                break;
            default:
                $err_msg = "attribute does not exists";
                throw new SalesforceHandlerException( $err_msg );
        }
    }

    public function get_all() {
        return array(
            "username"=>$this->_username,
            "password"=>$this->_password,
            "client_id"=>$this->_client_id,
            "client_secret"=>$this->_client_secret,
            "id"=>$this->_sforce_id,
            "instance"=>$this->_instance,
            "signature"=>$this->_signature,
            "access_token" =>$this->_access_token,
            "token_timestamp"=>$this->_token_timestamp,
            "version"=>$this->_api_version,
            "version_label"=>$this->_api_version_label,
            "version_url"=>$this->_api_version_url
        );
    }
} 
