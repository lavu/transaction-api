<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/12/14
 * Time: 12:47 PM
 */

require_once "SalesforceHandlerException.php";
require_once "SalesforceInterface.php";

abstract class SalesforceHandler{

    protected static function do_curl( $url,  $data=array(),  $method="GET", $extra_headers=array(), &$http_code=null ) {
        $curl_opts = array( CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 0,            // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false
        );
        if ( $method !== "POST" && $method !== "GET" ){
            $curl_opts[CURLOPT_CUSTOMREQUEST] = $method;
        }
        if ( !empty( $extra_headers ) ) {
            $curl_opts[CURLOPT_HTTPHEADER] = $extra_headers;
        }
        if ( $method === "GET" && !empty( $data ) ) {
            $url = $url . "?" . http_build_query( $data );
        }
        $ch = curl_init( $url );
        if ( $method === "POST" ) {
            $curl_opts[CURLOPT_POST] = true;
        }
        if ( $method !== "GET" && $method !== "HEAD" ) {
            if ( !empty( $data ) ) {
                $curl_opts[CURLOPT_POSTFIELDS] = $data;
            }
        }
        curl_setopt_array( $ch, $curl_opts );
        $result = curl_exec( $ch );
        $http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        if ( $http_code == 400 ) {
            $err_msg = __CLASS__ . "::" . __METHOD__ . "() $url returned $http_code response\n$result";
            error_log($err_msg);
            throw new SalesforceHandlerException( $err_msg );
        }
        return $result;
    }

}