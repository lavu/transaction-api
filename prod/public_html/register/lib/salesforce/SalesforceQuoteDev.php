<?php

require_once('/home/poslavu/private_html/salesforceCredentials.php');
require_once('SalesforceLogin.php');
require_once('SalesforceQuote.php');

define( 'DEV', 1 );

$sfQuoteExternalId = '0Q0e0000000L4INCA0';  // TO DO
$sfQuotePropertyName = 'GrandTotal';

$sfLogin = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
//echo json_encode( $sfLogin->get_all_sobjects(), JSON_PRETTY_PRINT ) ."\n";

$sfQuote = new SalesforceQuote( $sfLogin );
//echo json_encode($sfQuote->get_meta(), JSON_PRETTY_PRINT) . "\n";

//echo "sfQuotePropertyName=". $sfQuote->get_property( $sfQuotePropertyName, $sfQuoteExternalId ) ."\n";

$properties = array(
	'Name',
	'Id',
	'Subtotal',
	'TotalPrice',
	'ShippingCountry',
	'Tax',
	'GrandTotal',
	'LineItemCount',
	'BillingCountry',
);
echo "sfQuoteProperties=". print_r( $sfQuote->get_properties( $properties, $sfQuoteExternalId ), true ) ."\n";

exit;