<?php

require_once "SalesforceLogin.php";
require_once "SalesforceObject.php";
require_once "SalesforceProduct2.php";

class SalesforceQuoteLineItem extends SalesforceObject
{
	private $ids;

    public function __construct( $credentials, $meta = null )
    {
        $this->_object_name = 'QuoteLineItem';
        $this->_credentials = $credentials;
        $this->_metadata = empty( $meta ) ? $this->get_meta() : $meta;
    }

    public function getIds()
    {
    	return $this->ids;
    }

    public function getQuoteLineItemsIds( $quoteId )
    {
        $this->ids = array();

        $response = $this->get_by_field('QuoteId', $quoteId);

        foreach ( $response['records'] as $i => $quoteLineItem ) {
        	$this->ids[ $quoteLineItem['Id'] ] = array();
        }

        return $this->ids;
    }

    public function getQuoteLineItems( $quoteId, $credentials )
    {
    	$fieldNames = array('Product2Id', 'ListPrice', 'UnitPrice', 'Quantity', 'Subtotal', 'Discount', 'TotalPrice', 'LineNumber');

    	$sfProduct = new SalesforceProduct2( $credentials );

    	foreach ( $this->getQuoteLineItemsIds( $quoteId ) as $quoteLineItemId => $empty_array )
    	{
    		$properties = $this->get_properties($fieldNames, $quoteLineItemId);

    		// Delete unneeded stuff returned in the properties array.
    		unset( $properties['attributes'] );

    		// The name of our QuoteLineItem product isn't contained in the entity itself
    		// so we have to look it up using the Product info.
			$properties['ProductName'] = $sfProduct->get_property( 'Name', $properties['Product2Id'] );

    		$this->ids[$quoteLineItemId] = $properties;    		
    	}

    	return $this->ids;
    }
}