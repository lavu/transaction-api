<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/13/14
 * Time: 3:30 PM
 */

require_once "SalesforceLogin.php";
require_once "SalesforceObject.php";

class SalesforceAccount extends SalesforceObject {

    public function __construct ( SalesforceLogin $credentials, $meta=null ){
        $this->_object_name = "Account";
        $this->_credentials = $credentials;
        $this->_metadata = empty( $meta ) ? $this->get_meta() : $meta;
    }
}