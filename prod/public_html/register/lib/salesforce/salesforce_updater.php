 <?php

echo "Running process to check for Salesforce data update events...\n";

require_once "/home/poslavu/private_html/salesforceCredentials.php";
require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceLead.php";
require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceAccount.php";
require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceContact.php";
require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceOpportunity.php";
require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceQuote.php";
require_once "/home/poslavu/public_html/admin/cp/resources/core_functions.php";

// Setup the Salesforce REST API classes

$token_file_path = '/tmp/salesforce_token.txt';
$file_exists = file_exists( $token_file_path );
$file_not_empty = $file_exists && filesize($token_file_path);
$is_token_fresh = $file_not_empty && ( time()  < ( filemtime( $token_file_path ) + 60 * 60 * 2 ) );
$token = $is_token_fresh ? unserialize( file_get_contents($token_file_path ) ) : false;
if ( !empty ( $token ) && !empty( $token['access_token'] ) ) {
    $credentials = new SalesforceLogin( $token );
} else {
    $credentials = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
    file_put_contents( $token_file_path, serialize($credentials->get_all() ) );
}

$account_meta_file = '/home/poslavu/private_html/salesforceAccountMeta.txt';
$account_meta = file_exists( $account_meta_file ) ? file_get_contents($account_meta_file ) : null;
$account_meta = @unserialize( $account_meta );
$account_handler = new SalesforceAccount( $credentials, $account_meta );

$contact_meta_file = '/home/poslavu/private_html/salesforceContactMeta.txt';
$contact_meta = file_exists( $contact_meta_file  ) ? file_get_contents( $contact_meta_file ) : null;
$contact_meta = @unserialize( $contact_meta );
$contact_handler = new SalesforceContact( $credentials, $contact_meta );

$opportunity_meta_file = '/home/poslavu/private_html/salesforceOpportunityMeta.txt';
$opportunity_meta = file_exists( $opportunity_meta_file ) ? file_get_contents( $opportunity_meta_file ) : null;
$opportunity_meta = @unserialize( $opportunity_meta );
$opportunity_handler = new SalesforceOpportunity( $credentials, $opportunity_meta );

$quote_meta_file = '/home/poslavu/private_html/salesforceQuoteMeta.txt';
$quote_meta = file_exists( $quote_meta_file ) ? file_get_contents( $quote_meta_file ) : null;
$quote_meta = @unserialize( $quote_meta );
$quote_handler = new SalesforceQuote( $credentials, $quote_meta );


// Query the Salesforce integration table for unprocessed events

$processed_timestamp = date('Y-m-d H:i:s');

// Check if there are already any "in_progress" rows before proceeding because, if so, then there's probably another process running.
echo "...checking for existing in_progress event(s) from another process\n";
$result = mlavu_query( "SELECT * FROM `poslavu_MAIN_db`.`billing_salesforce_events` WHERE `event` IN('update_account', 'update_opportunity', 'update_quote') AND `processed` = 'in_progress'" );
if ($result === false) die( __FILE__ ." got ". mlavu_dberror() ."\n" );

// Exit if "in_progress" rows are detected, otherwise mark all of the unprocessed rows as "in_progress" so we can start cranking on them.
if ( mysqli_num_rows($result) > 0 ) {
	die(__FILE__ ." exiting because ". mysqli_num_rows($result) ." in_progress event(s) detected from another process\n");
}
else {
	echo "...flagging unprocessed event(s) as in_progress\n";
	$initial_update_result = mlavu_query( "UPDATE `poslavu_MAIN_db`.`billing_salesforce_events` SET `processed` = 'in_progress' WHERE `event` IN('update_account', 'update_opportunity', 'update_quote') AND `processed` = ''" );
	if ($initial_update_result === false) die( __FILE__ ." got ". mlavu_dberror() ."\n" );
}

// Run the same initial query to get all of the "in_progress" rows we just marked.
echo "...querying for flagged unprocessed event(s)\n";
$result = mlavu_query( "SELECT * FROM `poslavu_MAIN_db`.`billing_salesforce_events` WHERE `event` IN('update_account', 'update_opportunity', 'update_quote') AND `processed` = 'in_progress'" );
if ($result === false) die( __FILE__ ." got ". mlavu_dberror() ."\n" );

echo "Processing ". mysqli_num_rows($result) ." unprocessed event(s)...\n";

// Process the unprocessed rows

$processed_ids = array();
$updated_ids = array();
$errored_ids = array();

while ( $salesforce_event = mysqli_fetch_assoc($result) )
{

	echo "...processing ". $salesforce_event['event'] ." event for ". $salesforce_event['dataname'] ." from ". $salesforce_event['createtime'] ."\n";
	$processed_ids[] = $salesforce_event['id'];

	// Get signup record.
	echo "...looking up signup record\n";
	$signup_result = mlavu_query( "SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname` = '[dataname]'", array('dataname' => $salesforce_event['dataname']) );
	if ($signup_result === false) die( __FILE__ ." got ". mlavu_dberror() );
	$signup = mysqli_fetch_assoc($signup_result);
	//echo print_r($signup, true) ."\n";  //debug

	// Process Salesforce event.
	try {
		switch ( $salesforce_event['event'] ) {
			case 'update_account':
				if ( empty($signup['salesforce_id']) )  {
					$error_msg = "...skipping due to blank `signups`.`salesforce_id` (". $signup['salesforce_id'] .")";
					echo $error_msg . "\n";
					$errored_ids[$salesforce_event['id']] = $error_msg;
					continue;
				}
				else {
					$response = $account_handler->update_by_id( $signup['salesforce_id'], json_decode( $salesforce_event['details'], true ) );
				}
				break;
			case 'update_opportunity':
				if ( empty($signup['salesforce_opportunity_id']) )  {
					$error_msg = "...skipping due to blank `signups`.`salesforce_opportunity_id` (". $signup['salesforce_opportunity_id'] .")";
					echo $error_msg ."\n";
					$errored_ids[$salesforce_event['id']] = $error_msg;
					continue;
				}
				else {
					$response = $opportunity_handler->update_by_id( $signup['salesforce_opportunity_id'], json_decode( $salesforce_event['details'], true ) );
				}
				break;
			case 'update_quote':
				$update_fields = json_decode($salesforce_event['details'], true);
				$salesforce_quote_id = $update_fields['QuoteId'];
				if ( empty($salesforce_quote_id) )  {
					$error_msg = "...skipping due to blank QuoteId in event JSON for dataname={$salesforce_event['dataname']}";
					echo $error_msg ."\n";
					$errored_ids[$salesforce_event['id']] = $error_msg;
					continue;
				}
				else {
					unset($update_fields['QuoteId']);
					$response = $quote_handler->update_by_id( $salesforce_quote_id, $update_fields );
				}
				break;
			default:
				throw new Exception('Unrecognized Salesforce event type "'. $salesforce_event['event'] .'"');
				break;
		}
	}
	catch (Exception $e) {
		$error_msg = $e->getMessage();
		error_log( $error_msg );
		$errored_ids[$salesforce_event['id']] = $error_msg;
		continue;
	}

	$updated_ids[] = $salesforce_event['id'];

	echo print_r($response, true);  //debug
}

echo "processed_ids: ". print_r($processed_ids, true);  //debug
echo "updated_ids: ". print_r($updated_ids, true);  //debug
echo "errored_ids: ". print_r($errored_ids, true);  //debug


// Mark any processed rows as processed

if ( count($updated_ids) > 0 ) {
	$updated_ids_list = implode(', ', $updated_ids);
	$processed_update_result = mlavu_query( "UPDATE `poslavu_MAIN_db`.`billing_salesforce_events` SET `processed` = '[processed_timestamp]', `updatetime` = '[processed_timestamp]' WHERE `event` IN('update_account', 'update_opportunity', 'update_quote') AND `processed` = 'in_progress' AND `id` IN([ids_list])", array('processed_timestamp' => $processed_timestamp, 'ids_list' => $updated_ids_list ) );
	if ($processed_update_result === false) die( __FILE__ ." got ". mlavu_dberror() ." so couldn't update these `billing_salesforce_events` rows as processed: {$updated_ids_list}" );
}


// Mark any errored rows as failed so they don't get processed again the next time this script runs and send an email for manual intervention.

if ( count($errored_ids) > 0 ) {
	$errored_ids_list = implode(', ', array_keys($errored_ids));
	mail('tom@lavu.com', "ERROR'd events from ". __FILE__, "Could not process these `billing_salesforce_events` rows: ". print_r($errored_ids_list, true));
	$errored_update_result = mlavu_query( "UPDATE `poslavu_MAIN_db`.`billing_salesforce_events` SET `processed` = 'ERROR', `updatetime` = '[processed_timestamp]' WHERE `event` IN('update_account', 'update_opportunity', 'update_quote') AND `processed` = 'in_progress' AND `id` IN([ids_list])", array('processed_timestamp' => $processed_timestamp, 'ids_list' => $errored_ids_list ) );
	if ($errored_update_result === false) die( __FILE__ ." got ". mlavu_dberror() ." so couldn't update these `billing_salesforce_events` rows as errored: {$errored_ids_list}" );
}
