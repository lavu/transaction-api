<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/14/14
 * Time: 5:47 PM
 */

require_once "SalesforceLogin.php";
require_once "SalesforceObject.php";

class SalesforceContact extends SalesforceObject{

    public function __construct ( SalesforceLogin $credentials ){
        $this->_object_name = "Contact";
        $this->_credentials = $credentials;
        $this->_metadata = empty( $meta ) ? $this->get_meta() : $meta;
    }
}