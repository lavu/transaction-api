<?php
/**
 * Create the billing profiles and payment_hostlink for an account
 * @param  integer $p_signupid The id of the restaurant's signup row
 * @param  string $hostlink_type One of 'hosting', 'license', 'Hosting', or 'License'
 * @param  integer $subscriptionid The subscription id returned by Authorize.net
 * @param  string $subscription_start A datetime string of when the Authorize.net subscription starts
 * @param  double $subscription_amount The amount that is charged to the customer every time the subscription gets triggered (once a month for hosting, once for license)
 * @param  string $first_name The name of the user that entered the credit card information
 * @param  string $last_name The name of the user that entered the credit card information
 * @param  string $card_number The digits in the credit card number
 * @param  string $package The package level of the restuarant
 * @return none                         N/A
 */
function insert_signup_hostlink($p_signupid, $hostlink_type, $subscriptionid, $subscription_start, $subscription_amount, $first_name = "", $last_name = "", $card_number = "", $package = "")
{
    $hlink_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_hostlink` where `profileid`='[1]'", $subscriptionid);
    if (mysqli_num_rows($hlink_query)) {
    } else {
        mlavu_query("insert into `poslavu_MAIN_db`.`payment_hostlink` (`signupid`,`profileid`,`profiletype`,`profilestart`,`profileamount`) values ('[signupid]','[profileid]','[profiletype]','[profilestart]','[profileamount]')", array('signupid' => $p_signupid, 'profileid' => $subscriptionid, 'profiletype' => $hostlink_type, 'profilestart' => $subscription_start, 'profileamount' => $subscription_amount));
    }

    $bprof_query = mlavu_query("select * from `poslavu_MAIN_db`.`billing_profiles` where `subscriptionid`='[1]'", $subscriptionid);
    if (mysqli_num_rows($bprof_query)) {
    } else {
        $last_four = substr($card_number, strlen($card_number) - 4);

        $vars = array();
        $vars['subscriptionid'] = $subscriptionid;
        $vars['type'] = $hostlink_type;
        $vars['start_date'] = $subscription_start;
        $vars['last_name'] = $last_name;
        $vars['first_name'] = $first_name;
        $vars['last_four'] = $last_four;
        $vars['amount'] = $subscription_amount;
        $vars['package'] = $package;
        $vars['restaurantid'] = "";
        $vars['dataname'] = "";
        $vars['signupid'] = $p_signupid;
        $vars['last_pay_date'] = "";
        $vars['active'] = "1";
        mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_profiles` (`subscriptionid`, `type`, `start_date`, `last_name`, `first_name`, `last_four`, `amount`, `package`, `restaurantid`, `dataname`, `signupid`, `last_pay_date`, `active`) VALUES ('[subscriptionid]', '[type]', '[start_date]', '[last_name]', '[first_name]', '[last_four]', '[amount]', '[package]', '[restaurantid]', '[dataname]', '[signupid]', '[last_pay_date]', '[active]')", $vars);
    }
}

/**
 * Attach the given information to a billing profile
 */
function connect_billing_profile($subscriptionid, $dataname, $restaurantid) {
    if ( !empty( $subscriptionid ) ) {
        mlavu_query("UPDATE `poslavu_MAIN_db`.`billing_profiles` set `dataname`='[1]', `restaurantid`='[2]' where `subscriptionid`='[3]'", $dataname, $restaurantid, $subscriptionid);
    }
}

/**
 * Confirms that the restaurant shard in poslavu_MAINREST_db exists.
 * @param  string $data_name The restaurant's dataname
 * @return string            One of:
 *     $firstLetterOfDataname.": ".$data_name." exists"
 *     $firstLetterOfDataname.": ".$data_name." inserting <font color='green'>success</font>"
 *     $firstLetterOfDataname.": ".$data_name." inserting <font color='red'>failed</font>"
 */
function confirm_restaurant_shard($data_name)
{
    $str = "";
    $letter = substr($data_name, 0, 1);
    if (ord($letter) >= ord("a") && ord($letter) <= ord("z"))
        $letter = $letter;
    else
        $letter = "OTHER";
    $tablename = "rest_" . $letter;

    $mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'", $data_name);
    if (mysqli_num_rows($mainrest_query)) {
        $str .= $letter . ": " . $data_name . " exists";
    } else {
        $str .= $letter . ": " . $data_name . " inserting";

        $rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'", $data_name);
        if (mysqli_num_rows($rest_query)) {
            $rest_read = mysqli_fetch_assoc($rest_query);

            $success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')", $rest_read);
            if ($success) $str .= " <font color='green'>success</font>";
            else $str .= " <font color='red'>failed</font>";
        }
    }
    return $str;
}

/**
 * Creates a poslavu account.
 * Should only be called after the signups row has been created.
 * Redirects to the approved.php page and the equivelant translated pages.
 * @param  integer $rowid The id of the restaurant's row from the signups table
 * @param  string $company The company name of the restaurant, used to derive the dataname, not the dataname
 * @param  string $email The email address used during account creation
 * @param  string $phone The phone number used during account creation
 * @param  string $firstname The first name of the primary user used during account creation
 * @param  string $lastname The last name of the primary user used during account creation
 * @param  string $address The mailing address used during account creation
 * @param  string $city The city used during account creation
 * @param  string $state The state used during account creation
 * @param  string $zip The zip code used during account creation
 * @param  string $use_default_db THIS SHOULD ALWAYS BE "MAIN"
 * @return array                   returns an array as one of the following
 *     array('success'=>FALSE)
 *     array('success'=>TRUE, 'dataname'=>dataname)
 */
function execute_poslavu_creation($rowid, $company, $email, $phone, $firstname, $lastname, $address = "", $city = "", $state = "", $zip = "", $use_default_db = "", $locale="en")
{
    global $maindb;
    require_once(option('root_dir') . "/create_account.php");
    global $data;
    $post_ref = &$_POST;
    $data_ref = &$data;
    $transport = empty( $data ) ? $post_ref : $data_ref;
    $result = create_new_poslavu_account($company, $email, $phone, $firstname, $lastname, $address, $city, $state, $zip, $use_default_db, $locale);
    $success = $result[0];
    if ($success) {
        $username = $result[1];
        $password = $result[2];
        $dataname = $result[3];

        $credvars = array();
        $credvars['username'] = $username;
        $credvars['password'] = $password;
        $credvars['dataname'] = $dataname;
        $credvars['rowid'] = $rowid;
        $credvars['restaurantid'] = $result[4];
        $credvars['reseller_leads_rowid'] = (isset($transport['reseller_leads_rowid'])) ? $transport['reseller_leads_rowid'] : '';
        mlavu_query("update `poslavu_MAIN_db`.`signups` set `username`='[username]', `dataname`='[dataname]', `restaurantid`=[restaurantid], `password`=AES_ENCRYPT('[password]','password') where `id`='[rowid]'", $credvars);
        confirm_restaurant_shard($dataname);

        $signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `id`='[1]'", $rowid);
        if (mysqli_num_rows($signup_query)) {
            $signup_read = mysqli_fetch_assoc($signup_query);
            $rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'", $dataname);
            if (mysqli_num_rows($rest_query)) {
                $rest_read = mysqli_fetch_assoc($rest_query);

                if ($credvars['reseller_leads_rowid'] != '') {
                    mlavu_query("UPDATE `$maindb`.`reseller_leads` SET `data_name`='[dataname]' WHERE `id`='[reseller_leads_rowid]'", $credvars);
//                    if (ConnectionHub::getConn('poslavu')->affectedRows()) {
//                        mail("sales@poslavu.com", "New Lead Dataname", "A new lead with company name (" . $company . ") and data name (" . $credvars['dataname'] . ") was created. Please assign them to a distributor at https://admin.poslavu.com/distro/beta/index.php#leads_tab.", "from:noreply@poslavu.com");
//                        mail("distributors@poslavu.com", "New Lead Dataname", "A new lead with company name (" . $company . ") and data name (" . $credvars['dataname'] . ") was created. Please assign them to a distributor at https://admin.poslavu.com/distro/beta/index.php#leads_tab.", "from:noreply@poslavu.com");
//                    }
                }

                if (!isset($transport['vf_signup'])) {
                    connect_billing_profile($signup_read['arb_license_id'], $dataname, $rest_read['id']);
                    connect_billing_profile($signup_read['arb_hosting_id'], $dataname, $rest_read['id']);
                } else if (!isset($transport['vf_skip'])) {
                    $toks = Array('key' => 'integration_keystr()',
                        'dname' => $dataname,
                        'iname' => 'integration',
                        'int1' => $transport['vf_username'], //vf_api_secret
                        'int2' => $transport['vf_password'], //vf_password
                        'int3' => $transport['vf_client_id'], //vf_api_token
                        'int4' => $transport['vf_api_token'], //vf_client_id
                        'int5' => $transport['vf_api_secret']); //vf_username

                    $qstr = "update `poslavu_[dname]_db`.`locations` set `[iname]1`= AES_ENCRYPT('[int1]','[key]'), `[iname]2`= AES_ENCRYPT('[int2]','[key]'), `[iname]3`= AES_ENCRYPT('[int3]','[key]'), `[iname]4`= AES_ENCRYPT('[int4]','[key]'), `[iname]5`= AES_ENCRYPT('[int5]','[key]'),  `integrateCC`='1', `gateway`='SailPay'";
                    mlavu_query($qstr, $toks);
                }
            }
        }

// redirect to translated approved pages
        global $s_dev;
        if (!isset($s_dev)) {
            $s_dev = "";
        }

        return array(
            'success' => TRUE,
            'dataname' => $dataname,
            'rowid_plus_offset'=>$rowid+12500
        );
    } else {

        return array('success' => FALSE);
    }
}

/**
 * Validates and proesses the supplied promo code.
 * Will modify the values of the vars array accordingly.
 * @param  string $promo_code Promo code supplied by the user
 * @param  array $vars Array containing 'amount' key to be discounted for valid promo codes
 * @param  string $package Package level (used to look up package display name)
 * @param  string $signup_rowid Table ID for poslavu_MAIN_db.signups
 * @return bool                    returns boolean indicating whether code was accepted
 */
function process_promo_code($promo_code, &$vars, $package, $signup_rowid)
{
    global $o_package_container;

    $retvars = array(
        'code_accepted' => false,
        'promo_name' => '',
    );

    $package_name = strtolower($o_package_container->get_printed_name_by_attribute('level', $package));

    if (DEV) error_log("DEBUG: Before - vars[amount]={$vars['amount']} package={$package} package_name={$package_name}"); //debug

// NRA 2014 mailer promo
    if (preg_match('/nra\s*14/i', $promo_code) || preg_match('/14\s*nra/i', $promo_code)) // alexis said she's like to accept a few alternate forms of this promo code.
    {
        $retvars['code_accepted'] = true;

        $license_discount = 0;
        switch ($package_name) {
            case 'silver': // TO DO : make sure these are the right values for $vars['package']
                $license_discount = 100;
                break;
            case 'gold':
                $license_discount = 200;
                break;
            case 'pro':
                $license_discount = 400;
                break;
            default:
                break;
        }
        $retvars['code_accepted'] = true;
        $retvars['promo_name'] = 'nra14';
        $retvars['license_discount'] = $license_discount;

        $vars['amount'] = intval($vars['amount']) - $license_discount;
    } // Mercury 25% off license promo
    else if (preg_match('/^[mM]/', $promo_code)) {
// Don't query to see if the promo code exists in the database, just try updating it as taken by this user and, if it exists, the update will work.

        $promo_code_update_sql = "UPDATE `poslavu_MAIN_db`.`promo_codes` SET `claimed_date` = now(), `signupid` = '[signup_rowid]' WHERE `claimed_date` = '0000-00-00' and `promo_name` like 'mercury%' and `promo_code` = '[promo_code]'";
        mlavu_query($promo_code_update_sql, array('promo_code' => $promo_code, 'signup_rowid' => $signup_rowid));
        $affected_rows = ConnectionHub::getConn('poslavu')->affectedRows();
        if (DEV) error_log("DEBUG: promo_code_update_sql={$promo_code_update_sql} promo_code={$promo_code} affected_rows={$affected_rows}"); //debug
        if (DEV) error_log("DEBUG: mlavu_dberror=" . mlavu_dberror()); //debug
        if ($affected_rows === 1) {
            $retvars['code_accepted'] = true;
            $retvars['promo_name'] = 'mercury25off';
            $retvars['license_discount'] = intval($vars['amount']) * .25;

            $vars['amount'] = intval($vars['amount']) - (intval($vars['amount']) * .25);
        }
    } // BlueStar Activation Codes
    else if (preg_match('/^..[bBlLUueE].[1238][uUcC]./', $promo_code)) {
// Don't query to see if the promo code exists in the database, just try updating it as taken by this user and, if it exists, the update will work.

        $promo_code_update_sql = "UPDATE `poslavu_MAIN_db`.`promo_codes` SET `claimed_date` = now(), `signupid` = '[signup_rowid]' WHERE `claimed_date` = '0000-00-00' and `promo_name` like 'bluestar%' and `promo_code` = '[promo_code]'";
        mlavu_query($promo_code_update_sql, array('promo_code' => $promo_code, 'signup_rowid' => $signup_rowid));
        $affected_rows = ConnectionHub::getConn('poslavu')->affectedRows();
        if (DEV) error_log("DEBUG: promo_code_update_sql={$promo_code_update_sql} promo_code={$promo_code} affected_rows={$affected_rows}"); //debug
        if (DEV) error_log("DEBUG: mlavu_dberror=" . mlavu_dberror()); //debug
        if ($affected_rows === 1) {
            $retvars['code_accepted'] = true;
            $retvars['promo_name'] = 'bluestar_activation_code';
            $retvars['license_discount'] = intval($vars['amount']);

            $vars['amount'] = 0;
        }
    }

    if (DEV) error_log("DEBUG: After - code_accepted={$retvars['code_accepted']} vars[amount]={$vars['amount']}"); //debug

    return $retvars;
}

class AuthAccountExtractedFunctions
{

    /**
     * This is the old code to get the information necessary to create an account.
     * @param  array $a_immutables An array of variables passed in that aren't changed within this function, including
     *     $packages, $package, $mname
     * @param  array $package_info Set to $packages[$package]
     * @param  array $fields An array of arrays that are fields the user is meant to fill in
     * @param  boolean $show_form Always set to FALSE
     * @param  string $content Used to append the $formcode to the output
     * @param  array $validate Javascript code to validate the form return values
     * @param  string $formcode The html used to create the form
     * @param  string $insert_cols Used for an sql insert statement
     * @param  string $insert_vals Used for an sql insert statement
     * @param  string $update_cols Used for an sql update statement
     * @return none                  N/A
     */
    public static function getFormCode($a_immutables, &$package_info, &$fields, &$show_form, &$content, &$validate, &$formcode, &$insert_cols, &$insert_vals, &$update_cols)
    {

        $packages = $a_immutables['packages'];
        $package = $a_immutables['package'];
        $mname = $a_immutables['mname'];

        $package_info = $packages[$package];
        $fields = array();

        $fields[] = array("Company Name", "text", "company", true);
        $fields[] = array("First Name", "text", "firstname", true);
        $fields[] = array("Last Name", "text", "lastname", true);
        $fields[] = array("Email Address", "email", "email", true);
        $fields[] = array("Phone", "phone", "phone", true);
        $fields[] = array("Promo Code", "text", "promo", false);

        $fields[] = array("Address", "text", "address", false);
        $fields[] = array("State", "text", "state", false);
        $fields[] = array("Zip", "text", "zip", false);
        $fields[] = array("Country", "text", "country", false);
        $fields[] = array("City", "text", "city", false);

        $fields[] = array("Package", "set", "package", false, $package);
        $fields[] = array("Default Menu", "set", "default_menu", false, $mname);
        $fields[] = array("Status", "set", "status", false, "new");
        $fields[] = array("Date", "set", "date", false, date("Y-m-d"));
        $fields[] = array("Time", "set", "time", false, date("H:i:s"));

        $fields[] = array("Ip Address", "set", "ipaddress", false, $_SERVER['REMOTE_ADDR']);
        $fields[] = array("Session Id", "set", "sessionid", false, session_id());
        $fields[] = array("Credit Card Info", "title");
        $fields[] = array("Card Number", "card_number", "card_number");
        $fields[] = array("Card Expiration", "card_expiration", "card_expiration");
        $fields[] = array("CVV Code", "card_code", "card_code");
        $fields[] = array("Referral", "source", "source");

        $show_form = false;
        $content .= "<div style='position:relative'>";

        $validate = array();
        $formcode = "";
        $insert_cols = "";
        $insert_vals = "";
        $update_cols = "";

        for ($i = 0; $i < count($fields); $i++) {
            $title = $fields[$i][0];
            $type = $fields[$i][1];
            $name = (isset($fields[$i][2])) ? $fields[$i][2] : "";
            $required = (isset($fields[$i][3])) ? $fields[$i][3] : false;
            $props = (isset($fields[$i][4])) ? $fields[$i][4] : array();
            global $data;
            $post_ref = &$_POST;
            $data_ref = &$data;
            $transport = empty( $data ) ? $post_ref : $data_ref;
            $value = (isset($transport[$name])) ? $transport[$name] : "";

            $include_in_db = false;

            $formcode .= "<tr>";
            if ($type == "title") {
                $formcode .= "<td colspan='2'>&nbsp;</td><td class='tabhead' align='center'>" . strtoupper($title) . "</td>";
            } else if ($type == "submit") {
            } else if ($type == "set") {
                $include_in_db = true;
                $value = $props;
                $formcode .= "<input type='hidden' name='$name' value=\"" . str_replace("\"", "&quot;", $props) . "\">";
            } else {
                $include_in_db = true;

                $formcode .= "<td align='right' class='tabtext'>";
                $formcode .= strtoupper($title);

                if ($type == "card_code") {
                    $include_in_db = false;
                    $formcode .= " <a style='cursor:pointer; color:#888888' onclick='document.getElementById(\"cvv\").style.visibility = \"visible\"'>(What is this?)</a>";
                } else if ($type == "card_expiration") {
                    $formcode .= " <font color='#888888'>(mmYY)</font>";
                }

                $formcode .= "</td><td>";
                if ($required) $formcode .= "<font color='#444477' style='font-size:14px'>*</font>";
                else $formcode .= "&nbsp;";
                $formcode .= "</td>";
                $formcode .= "<td>";
                if ($type == "select") {
                    $formcode .= "<select name='$name' style='width:240px'>";
                    for ($n = 0; $n < count($props); $n++) {
                        $prop = $props[$n];
                        $formcode .= "<option value='$prop'>$prop</option>";
                    }
                    $formcode .= "</select>";
                } else {
                    $formcode .= "<input type='text' name='$name' value='' style='width:240px'>";
                }
                if ($type == "card_number") {
                    if (strlen($value) > 4) {
                        $value = substr($value, strlen($value) - 4);
                    }
                }
                $formcode .= "</td>";
                if ($required) {
                    $validate[] = "if(document.form1.$name.value=='') alert('Please provide a value for \"$title\"'); ";
                }
                if ($type == "email") {
                    $validate[] = "if(document.form1.$name.value!='' && (document.form1.$name.value.indexOf('@') < 1 || document.form1.$name.value.indexOf('.') < 1)) alert('Please provide a valid email address'); ";
                }
            }
            if ($include_in_db) {
                if ($insert_cols != "") $insert_cols .= ",";
                $insert_cols .= "`$name`";
                if ($insert_vals != "") $insert_vals .= ",";
                $insert_vals .= "'" . mysqli_real_escape_string(str_replace("'", "''", $value)) . "'";
                if ($update_cols != "") $update_cols .= ",";
                $update_cols .= "`$name`='" . mysqli_real_escape_string(str_replace("'", "''", $value)) . "'";
            }
            $formcode .= "</tr>";
        }
        $formcode .= "</table>";
        $formcode .= "</form>";
        if (strpos($update_cols, "`ipaddress`") === FALSE) {
            $update_cols .= ",`ipaddress`='" . $_SERVER["REMOTE_ADDR"] . "'";
            $insert_cols .= ",`ipaddress`";
            $insert_vals .= ",'" . $_SERVER["REMOTE_ADDR"] . "'";
        }
    }

    /**
     * Tracks the success or failure of credit card authorization from authAccount
     * @param  string $license_auth_result should be "Approved" upon success or anything else on failure
     * @return none                        N/A
     */
    public static function trackAuthAccountTransactions($license_auth_result, $card_number, $card_expiration, $card_code)
    {
        $a_tracking = DB::getAllInTable("tracking", array("ipaddress" => $_SERVER['REMOTE_ADDR'], "date" => date("Y-m-d"), "description" => "auth_account"), TRUE, array("whereclause" => "WHERE `ipaddress`='[ipaddress]' AND `date`>='[date]' AND `description`='[description]'"));
        $s_date = date("Y-m-d H:i:s");
        $i_successes = ($license_auth_result == "Approved") ? 1 : 0;
        $i_failures = ($license_auth_result == "Approved") ? 0 : 1;
        $card_number_last_four = substr($card_number, -4);
        $s_successes = ($license_auth_result == "Approved") ? str_replace(array('"', ","), "", "{$card_number_last_four}") : "";
        $s_failures = ($license_auth_result == "Approved") ? "" : str_replace(array('"', ","), "", "{$card_number_last_four}");
        if (count($a_tracking) > 0) {
            $i_successes = (int)$a_tracking[0]['success_count'] + $i_successes;
            $i_failures = (int)$a_tracking[0]['fail_count'] + $i_failures;
            $a_successes = ($s_successes != "") ? array($s_successes) : array();
            $a_prev_successes = ($a_tracking[0]['successes'] != "") ? explode(",", $a_tracking[0]['successes']) : array();
            $s_successes = implode(",", array_merge($a_successes, $a_prev_successes));
            $a_failures = ($s_failures != "") ? array($s_failures) : array();
            $a_prev_failures = ($a_tracking[0]['failures'] != "") ? explode(",", $a_tracking[0]['failures']) : array();
            $s_failures = implode(",", array_merge($a_failures, $a_prev_failures));
        }
        $a_update_vars = array("ipaddress" => $_SERVER['REMOTE_ADDR'], "description" => "auth_account", "date" => date("Y-m-d H:i:s"), "success_count" => $i_successes, "fail_count" => $i_failures, "successes" => $s_successes, "failures" => $s_failures);
        if (count($a_tracking) > 0) {
            $s_updateclause = DB::arrayToUpdateClause($a_update_vars);
            mlavu_query("UPDATE `poslavu_MAIN_db`.`tracking` {$s_updateclause} WHERE `id`='[id]'", array_merge(array('id' => $a_tracking[0]['id']), $a_update_vars));
        } else {
            $s_insertclause = DB::arrayToInsertClause($a_update_vars);
            mlavu_query("INSERT INTO `poslavu_MAIN_db`.`tracking` {$s_insertclause}", $a_update_vars);
        }
    }
}

?>