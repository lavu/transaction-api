<?php

/**
 * FUNCTIONS
 * I did't wrote most of this, so if you have real questions about this
 * ask Corey or you're shit outta luck
 */

/**
 * Stupid function to split a name in to first and last and
 * get rid of extra crap
 * Ex: split_name("Dr Testie J McTurdies Jr")
 *      \-> ["Testie", "McTurdies"]
 * @param $name
 * @return array
 */
function split_name ( $name ) {
    $pieces = preg_split( "/[\s,]+/", $name );
    $pieces_copy = $pieces;
    if ( !function_exists("sort_by_length") ) {
        function sort_by_length ( $a, $b ) {
            return strlen ( $b ) - strlen( $a );
        }
    }
    usort( $pieces, "sort_by_length");
    if ( count( $pieces ) > 1 ){
        $og_index1 = array_search( $pieces[0], $pieces_copy );
        $og_index2 = array_search( $pieces[1], $pieces_copy );
        if ( $og_index1 > $og_index2 ) {
            return array( $pieces[1], $pieces[0] );
        } else {
            return array( $pieces[0], $pieces[1] );
        }
    } else {
        return array( trim( $name ), "" );
    }
}

/**
 * get the number of recent credit card authorizations from this ip address
 * @param  integer $i_days_past     How many days ago to start looking
 * @param  integer $i_days_from_now How many days from now to stop looking
 * @return integer                  Integer of number of recent signups upon success, FALSE if the query fails
 */
function getRecentCCs($i_days_past, $i_days_from_now) {

    // create the query
    $min_date_create = date("Y-m-d", strtotime("{$i_days_past} days"))." 00:00:00";
    $max_date_create = date("Y-m-d", strtotime("{$i_days_from_now} days"))." 00:00:00";
    $s_query_string = "SELECT SUM(`fail_count`) as `count` from `poslavu_MAIN_db`.`tracking` where `ipaddress`='[ip]' AND `date`>='[start_date]' AND `date`<='[end_date]'";
    $a_query_vars = array("ip"=>$_SERVER['REMOTE_ADDR'],"start_date"=>$min_date_create,"end_date"=>$max_date_create);
    $tracking_query = mlavu_query($s_query_string,$a_query_vars);

    // check that the query succeeded
    if ($tracking_query === FALSE) {
        foreach($a_query_vars as $k=>$v)
            $s_query_string = str_replace("[$k]", $v, $s_query_string);
        error_log("bad query: $s_query_string              ".__FILE__." ".__LINE__);
    }

    // get the created count from between yesterday and tomorrow for the remote ip address
    if($tracking_query !== FALSE && mysqli_num_rows($tracking_query))
    {
        $tracking_read = mysqli_fetch_assoc($tracking_query);
        $fail_count = $tracking_read['count'];
        return $fail_count;
    }
    return FALSE;
}

// creates a row in a table with matching array indices and table columns
// it does an insert/update with the new data, depending upon the values in $a_if_matching_update
// updates the global $i_reseller_leads_id
// returns true upon an insert or false for an update
function create_or_update_lead_ben_bean_style($a_lead_data, $a_if_matching_update) {
    global $maindb;
    global $i_reseller_leads_id;

    // find any matches that exist
    $a_matches = DB::findMatch(false,'poslavu_MAIN_db', 'reseller_leads', array(
        'rules'=>array(
            array('colname'=>'session_id', 'match_value'=>$a_lead_data['session_id']),
            array('colname'=>'email', 'match_value'=>$a_lead_data['email'], 'match_degree'=>90),
            array('colname'=>'company_name', 'match_value'=>$a_lead_data['company_name'], 'match_degree'=>85),
        ),
    ));
    $i_match_id = -1;
    foreach($a_matches as $a_match) {
        $i_match_id = $a_match['id'];
        break;
    }

    // find the columns to add to the db
    $a_table_columns = DB::getTableColumns(true,'poslavu_MAIN_db', 'reseller_leads');
    $a_lead_columns = array();
    foreach($a_lead_data as $k=>$v)
        $a_lead_columns[] = $k;
    $a_similar_columns_index_by_ints = array_intersect($a_lead_columns, $a_table_columns);
    $a_similar_columns = array();
    foreach($a_similar_columns_index_by_ints as $s_colname)
        $a_similar_columns[$s_colname] = $s_colname;

    // get the update/insert clause
    $a_query_vars = $a_lead_data;
    $b_do_insert = FALSE;
    if ($i_match_id > -1) {
        $s_update_clause = DB::arrayToUpdateClause($a_similar_columns);
        $a_wheres = array('id'=>$i_match_id);
        $s_where_clause = DB::arrayToWhereClause($a_wheres);
        $a_query_vars = array_merge($a_wheres, $a_query_vars);
        $s_query_string = "UPDATE `[maindb]`.`reseller_leads` $s_update_clause $s_where_clause";
    } else {
        $s_insert_clause = DB::arrayToInsertClause($a_similar_columns);
        $s_query_string = "INSERT INTO `[maindb]`.`reseller_leads` $s_insert_clause";
        $b_do_insert = TRUE;
    }
    $a_query_vars = array_merge(array('maindb'=>$maindb), $a_query_vars);

    // submit the query
    mlavu_query($s_query_string, $a_query_vars);
    if ($b_do_insert)
        $i_reseller_leads_id = (int)mlavu_insert_id();
    else
        $i_reseller_leads_id = $i_match_id;

    // send an email
    if ($b_do_insert) {}

    // return the id of the row inserted/updated
    return $b_do_insert;
}

/**
 * Dunno what this thing does, don't ask
 */
function draw_query_string($query_string, $a_input) {
    foreach($a_input as $k=>$v)
        $query_string = str_replace("[$k]", $v, $query_string);
    return $query_string;
}

/**
 * get the number of recent signups from this ip address
 * @param  integer $i_days_past     How many days ago to start looking
 * @param  integer $i_days_from_now How many days from now to stop looking
 * @return integer                  Integer of number of recent signups upon success, FALSE if the query fails
 */
function getRecentSignups($i_days_past, $i_days_from_now) {

    // create the query
    $min_date_create = date("Y-m-d", strtotime("{$i_days_past} days"))." 00:00:00";
    $max_date_create = date("Y-m-d", strtotime("{$i_days_from_now} days"))." 00:00:00";
    $s_query_string = "SELECT count(*) as `count` from `poslavu_MAIN_db`.`signups` left join `poslavu_MAIN_db`.`restaurants` on `restaurants`.`data_name`=`signups`.`dataname` where `signups`.`ipaddress`='[ip]' and `signups`.`dataname`!='' and `restaurants`.`created`>='[start_date]' and `restaurants`.`created`<='[end_date]'";
    $a_query_vars = array("ip"=>$_SERVER['REMOTE_ADDR'],"start_date"=>$min_date_create,"end_date"=>$max_date_create);
    $signups_created_query = mlavu_query($s_query_string,$a_query_vars);

    // check that the query succeeded
    if ($signups_created_query === FALSE) {
        foreach($a_query_vars as $k=>$v)
            $s_query_string = str_replace("[$k]", $v, $s_query_string);
        error_log("bad query: $s_query_string              ".__FILE__." ".__LINE__);
    }

    // get the created count from between yesterday and tomorrow for the remote ip address
    if($signups_created_query !== FALSE && mysqli_num_rows($signups_created_query))
    {
        $signups_created_read = mysqli_fetch_assoc($signups_created_query);
        $signup_count = $signups_created_read['count'];
        return $signup_count;
    }
    return FALSE;
}

function checkForRecentSignups() {
    $signup_count = getRecentSignups(-1, +1);
    if ($signup_count === FALSE) {
        $signup_count = 10; // If can't determine count then don't let continue
    }

    // failed, too many accounts created
    if($signup_count > 1 && (!isset($_GET["escape_ip_check"]) || $_GET["escape_ip_check"] != "1") && empty( $_SERVER['DEV'])) {
        return false;
    }
    return true;
}

function write( $str ) {
    $r =  $str . "<br>";
    return;
}

// Dumb little function to see an error
function set_rollback_error( $h2="", $p="") {
    if(session_id() == '' || !isset($_SESSION)) {
        // session isn't started
        session_start();
    }
    // This is gonna delete an error that might have been set before
    // But that is the intended behavior 
    $_SESSION['error'] = array();
    $_SESSION['error']['h2'] = $h2;
    $_SESSION['error']['p'] = $p;
}

// creates a row in a table with matching array indices and table columns
// it does an insert/update with the new data, depending upon the values in $a_if_matching_update
// updates the global $i_reseller_leads_id
// returns true upon an insert or false for an update
function create_or_update_reseller_lead($a_lead_data, $a_if_matching_update) {
    global $maindb;

    // find any matches that exist
/*    $a_matches = DB::findMatch(true,'poslavu_MAIN_db', 'reseller_leads', array(
        'rules'=>array(
            array('colname'=>'session_id', 'match_value'=>$a_lead_data['session_id']),
            array('colname'=>'email', 'match_value'=>$a_lead_data['email'], 'match_degree'=>90),
            array('colname'=>'company_name', 'match_value'=>$a_lead_data['company_name'], 'match_degree'=>85),
        ),
    ));
    $i_match_id = -1;
    foreach($a_matches as $a_match) {
        $i_match_id = $a_match['id'];
        break;
    }
*/
    $i_match_id = -1;

    // find the columns to add to the db
    $DB = ConnectionHub::getConn('poslavu')->getDBAPI();
    $a_table_columns = $DB->getTableColumns(true,'reseller_leads','poslavu_MAIN_db');
    $a_lead_columns = array();
    foreach($a_lead_data as $k=>$v)
        $a_lead_columns[] = $k;
    $a_similar_columns_index_by_ints = array_intersect($a_lead_columns, $a_table_columns);
    $a_similar_columns = array();
    foreach($a_similar_columns_index_by_ints as $s_colname)
        $a_similar_columns[$s_colname] = $s_colname;

    // get the update/insert clause
    $a_query_vars = $a_lead_data;
    $b_do_insert = FALSE;
    if ($i_match_id > -1) {
        $s_update_clause = $DB->arrayToUpdateClause($a_similar_columns);
        $a_wheres = array('id'=>$i_match_id);
        $s_where_clause = $DB->arrayToWhereClause($a_wheres);
        $a_query_vars = array_merge($a_wheres, $a_query_vars);
        $s_query_string = "UPDATE `[maindb]`.`reseller_leads` $s_update_clause $s_where_clause";
    } else {
        $s_insert_clause = $DB->arrayToInsertClause($a_similar_columns);
        $s_query_string = "INSERT INTO `[maindb]`.`reseller_leads` $s_insert_clause";
        $b_do_insert = TRUE;
    }
    $a_query_vars = array_merge(array('maindb'=>$maindb), $a_query_vars);

    // submit the query
    mlavu_query($s_query_string, $a_query_vars);
    if ($b_do_insert)
        $i_reseller_leads_id = (int)mlavu_insert_id();
    else
        $i_reseller_leads_id = $i_match_id;

    // send an email
    if ($b_do_insert) {}

    // return the id of the row inserted/updated
    return $i_reseller_leads_id;
}

/**
 * Function made by Corey to handle resellers leads, I rewrote a little here to make
 * more MVC like.
 * @param $a_lead_data : {array} a copy of $_POST/$_REQUEST with all the data
 *                       from the previous page
 * @param $a_if_matching_update : {boolean} flag to indicate if the lead should be
 *                                updated if a match is found.
 * 
 */

// creates a row in a table with matching array indices and table columns
// it does an insert/update with the new data, depending upon the values in $a_if_matching_update
// updates the global $i_reseller_leads_id
// returns true upon an insert or false for an update

function create_or_update_lead($a_lead_data, $a_if_matching_update) {
    global $maindb;
    global $i_reseller_leads_id;
    if(isset($a_lead_data['company_name']) && $a_lead_data['company_name']!="") {
        $set_first_name = null;
        $set_last_name = null;
        if ( !empty( $a_lead_data['full_name'] ) ) {
            $fullname = explode(" ",$a_lead_data['full_name'], 2 );
            $set_first_name = trim($fullname[0]);
            $set_last_name = empty($fullname[1] ) ? "" : trim($fullname[1]);
        }

        if(!empty($a_lead_data['firstname'])) $set_first_name = $a_lead_data['firstname'];
        if(!empty($a_lead_data['lastname'])) $set_last_name = $a_lead_data['lastname'];
        $poll_data = array();
        if ( !empty( $a_lead_data['new_pos'] ) ) $poll_data['new_pos'] = $a_lead_data['new_pos'];
        if ( !empty( $a_lead_data['interests'] ) ) $poll_data['interests'] = $a_lead_data['interests'];
        if ( !empty( $poll_data ) ) $a_lead_data['signups_json'] = json_encode( $poll_data );
        $lead_cols = array();
        $lead_cols['lead_source'] = $a_lead_data['lead_source'];
        $lead_cols['created'] = empty( $a_lead_data['created'] ) ?
            ( empty( $a_lead_data['made_demo_account'] ) ?
                date('Y-m-d H:i:s') :
                $a_lead_data['made_demo_account'] ) :
            $a_lead_data['created'] ;
        $lead_cols['company_name'] = $a_lead_data['company_name'];
        if ( !empty( $a_lead_data['salesforce_sent'] ) ) {
            $lead_cols['salesforce_sent'] = $a_lead_data['salesforce_sent'];
        }
        $lead_cols['email'] = $a_lead_data['email'];
        $lead_cols['firstname'] = $set_first_name;
        $lead_cols['lastname'] = $set_last_name;
        $lead_cols['phone'] = $a_lead_data['phone'];
        $lead_cols['session_id'] = empty( $a_lead_data['session_id'] )? session_id() : $a_lead_data['session_id'];
        $lead_cols['lead_strength'] = 1;

        $lead_cols['country'] = empty( $a_lead_data['country'] ) ? get_country() : $a_lead_data['country'];
        $optionals = array('signups_json', 'demo_type', 'data_name', 'made_demo_account', 'address', 'city', 'state', 'zip', 'notes', '_canceled');
        foreach( $optionals as $opt ) {
            if ( !empty( $a_lead_data[$opt] ) ) {
                $lead_cols[$opt] = $a_lead_data[$opt];
            }
        }

        $update_code = "";
        $insert_cols = "";
        $insert_vals = "";
        foreach($lead_cols as $lkey => $lval) {
            if($update_code!="") $update_code .= ",";
            if($insert_cols!="") $insert_cols .= ",";
            if($insert_vals!="") $insert_vals .= ",";
            $escaped_lkey = mysqli_real_escape_string($lkey);

            $update_code .= "`".$escaped_lkey."`='[".$escaped_lkey."]'";
            $insert_cols .= "`".$escaped_lkey."`";
            $insert_vals .= "'[".$escaped_lkey."]'";
        }

        $match_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_leads` where `session_id`='[session_id]' and `company_name` LIKE '[company_name]' and `email` LIKE '[email]' and `distro_code`='' order by id desc limit 1",$lead_cols);
        if( mysqli_num_rows( $match_query ) ) {
            $match_read = mysqli_fetch_assoc($match_query);
            $lead_cols['id'] = $match_read['id'];
            error_log('STEP2 #197 lead update');
            mlavu_query("update `poslavu_MAIN_db`.`reseller_leads` set $update_code where `id`='[id]'",$lead_cols);
            return $match_read['id'];
        } else {
            error_log('STEP2 #204 lead update:: '.$update_code);
            mlavu_query("insert into `poslavu_MAIN_db`.`reseller_leads` ($insert_cols) values ($insert_vals)",$lead_cols);
            return mlavu_insert_id();
            //echo "No Match Found<br>";
        }
    } else {
        return null;
    }
}

/**
 * Supplementary json_encode in case php version is < 5.2 (taken from http://gr.php.net/json_encode)
 */
if (!function_exists('json_encode'))
{
    function json_encode($a=false)
    {
        if (is_null($a)) return 'null';
        if ($a === false) return 'false';
        if ($a === true) return 'true';
        if (is_scalar($a))
        {
            if (is_float($a))
            {
                // Always use "." for floats.
                return floatval(str_replace(",", ".", strval($a)));
            }

            if (is_string($a))
            {
                static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
                return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
            }
            else
            return $a;
        }
        $isList = true;
        for ($i = 0, reset($a); $i < count($a); $i++, next($a))
        {
            if (key($a) !== $i)
            {
                $isList = false;
                break;
            }
        }
        $result = array();
        if ($isList)
        {
            foreach ($a as $v) $result[] = json_encode($v);
            return '[' . join(',', $result) . ']';
        }
        else
        {
            foreach ($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v);
            return '{' . join(',', $result) . '}';
        }
    }
}


/**
 * Validates the supplied promo code.
 * Note: this was taken from ~/public_html/register/step2.php
 * @param  string  $promo_code            Promo code supplied by the user
 * @param  string  $license_name          Package level name of license to be discounted
 * @param  string  $license_discount      Discount amount saved in variable that is passed by reference
 * @param  boolean $unclaimed_codes_only  Whether promo codes should be checked against
 *
 * @return bool                    returns boolean indicating whether promo code is valid and will be accepted in process_promo_code()
 */
function check_promo_code($promo_code, $license_name, &$license_discount, $unclaimed_codes_only = true)
{
    global $o_package_container;
    $code_accepted = false;
    $license_discount = 0;
    $license_display_name = strtolower($o_package_container->get_printed_name_by_attribute('name', $license_name));

    // NRA 2014 mailer promo
    if ( preg_match('/nra\s*14/i', $promo_code) || preg_match('/14\s*nra/i', $promo_code) )  // alexis said she's like to accept a few alternate forms of this promo code.
    {
        $code_accepted = true;
        switch ($license_display_name) {
            case 'silver':
                $license_discount = 100;
                break;
            case 'gold':
                $license_discount = 200;
                break;
            case 'pro':
                $license_discount = 400;
                break;
            default:
                break;
        }
    }
    // Mercury 25% off license promo
    else if ( preg_match('/^.[mM]..[25]..[pPrR0mM0]/', $promo_code) )
    {
        $sql = '';
        if ($unclaimed_codes_only) {
            // When someone's about to claim a promo code, we only want to validate promo codes against unclaimed ones (signified by a "blank" claimed_date).
            $sql = "SELECT * FROM `poslavu_MAIN_db`.`promo_codes` WHERE `promo_code` = '[promo_code]' and `claimed_date` = '0000-00-00'";
        } else {
            // If we're just validating promo codes and getting the discount amount (like when invoicing for Billing), then we don't care about the claimed date.
            $sql = "SELECT * FROM `poslavu_MAIN_db`.`promo_codes` WHERE `promo_code` = '[promo_code]'";
        }

        $results = mlavu_query( $sql, array('promo_code' => $promo_code) );
        $num_rows = ($results === FALSE) ? 0 : mysqli_num_rows($results);

        if (DEV) error_log( "DEBUG: sql={$sql} promo_code={$promo_code} num_rows={$num_rows} mlavu_dberror=". mlavu_dberror());  //debug

        if ( $num_rows >= 1 ) {
            $code_accepted = true;
            $license_discount = .25;
        }
    }
    // BlueStar Activation Codes
    else if ( preg_match('/^..[bBlLUueE].[1238][uUcC]./', $promo_code) )
    {
        $sql = '';
        if ($unclaimed_codes_only) {
            // When someone's about to claim a promo code, we only want to validate promo codes against unclaimed ones (signified by a "blank" claimed_date).
            $sql = "SELECT * FROM `poslavu_MAIN_db`.`promo_codes` WHERE `promo_code` = '[promo_code]' and `promo_name` like 'bluestar%' and `claimed_date` = '0000-00-00'";
        } else {
            // If we're just validating promo codes and getting the discount amount (like when invoicing for Billing), then we don't care about the claimed date.
            $sql = "SELECT * FROM `poslavu_MAIN_db`.`promo_codes` WHERE `promo_code` = '[promo_code]' and `promo_name` like 'bluestar%'";
        }

        $results = mlavu_query( $sql, array('promo_code' => $promo_code) );
        $num_rows = ($results === FALSE) ? 0 : mysqli_num_rows($results);

        if (DEV) error_log( "DEBUG: sql={$sql} promo_code={$promo_code} num_rows={$num_rows} mlavu_dberror=". mlavu_dberror());  //debug

        if ( $num_rows >= 1 ) {
            $code_accepted = true;
            $license_discount = 1.0;  // 100%
        }
    }

    return $code_accepted;
}

/**
 * gateway function to get the country. If a country code is given
 * this functions just checks the MySQL tables to see if the country
 * code is correct. If not, it calls get_country_by_ip to infer the
 * country based on the ip address
 *
 * @param str|null $request_country
 * @return mixed
 */
function get_country( $request_country=null ){
    // If a country code is already given, check if it a valid country code
    if ( !empty( $request_country ) ) {
        $sql = <<<SQL
SELECT `code`, `name` FROM `countries` where `code` like '[country_code]';
SQL;
        $query_args = array( "country_code" => strtoupper( $request_country ) );
        $result = mlavu_query( $sql, $query_args );
        if ( mysqli_num_rows( $result ) ) return $query_args['country_code'];
    }
    // else get the country by ip
    $country = get_country_by_ip( $_SERVER["REMOTE_ADDR"] );
    return $country;
}

/**
 * Transform and IPv4 string into a integer. Returns null if the
 * argument is not a string
 * @param $ip_str
 * @return int|null
 */
function ip_strtoint( $ip_str ){
    if ( !is_string( $ip_str ) ) {
        return null;
    }
    $pieces = explode('.', $ip_str );
    $ip_int = 0;
    for ( $i = count( $pieces ) -1, $offset = 1; $i > -1; $i--, $offset *= 256 ) {
        $ip_int += $pieces[$i] * $offset;
    }
    return $ip_int;
}

/**
 *
 * @param $ip
 */
function get_country_by_ip( $ip=null ) {
    if ( is_null( $ip ) ) $ip = $_SERVER['REMOTE_ADDR'];
    $ip_int = ip_strtoint( $ip );
    $sql = <<<SQL
SELECT  `begin_ip`, `end_ip`, `country_code`, `country_name` FROM `ap_blocks`
WHERE `begin_ip_num` >= [ip_int] AND `end_ip_num` <= [ip_int] LIMIT 1
SQL;
    $query_args = array( "ip_int" => $ip_int );
    $result = mlavu_query( $sql, $query_args );
    if ( mysqli_num_rows( $result ) ) {
        $row = mysqli_fetch_assoc( $result );
        return $row['country_code'];
    } else {
        // now here shit gets real, now we have to query ipinfo
        // and save it in a session and make sure that we don't
        // request again if the session is already set.
        $geodata = load_ipinfo_data( $ip );
        return !empty( $geodata['country'] ) ?$geodata['country']: 'A1' ;
    }
}

/**
 * calls ipinfo.io, loads it in a session and returns a copy of the
 * data stored in the session
 * @param null $ip
 * @return array|mixed
 */
function load_ipinfo_data( $ip=null ){
    if ( empty( $_SESSION['geodata'] ) ) {
        if ( is_null( $ip ) ) $ip = $_SERVER['REMOTE_ADDR'];
        $ch = curl_init( "http://ipinfo.io/$ip/json" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        $json = curl_exec( $ch );
        $errno = curl_errno( $ch );
        if ( !empty( $errno ) ) {
            return null;
        }
        if(session_id() == '' || !isset($_SESSION)) {
            // session isn't started
            session_start();
        }
        $_SESSION['geodata'] = json_decode( $json, true );
    }
    return $_SESSION['geodata'];
}

function sanitize_email( $email ) {
    $email = trim( $email, ". \t\n\r\0\x0B" );
    $email = str_replace('@@', '@', $email);
    $email = strpos($email,'@') === false ? "$email@not.available" : $email;
    if ( function_exists('filter_var') ) {
        if ( !filter_var($email, FILTER_VALIDATE_EMAIL ) ) {
            $email = "not@vail.able";
        }
    } else {
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        if (!preg_match($pattern, $email)) {
            $email = "not@vail.able";
        }
    }
    return $email;
}

function send_to_salesforce( $data_array ) {
    // initialize the arrays with some default values
    $contact_data = array(
        "FirstName" => "N/A",
        "LastName" => "N/A",
    );
    $opportunity_data = array(
        "CloseDate" => date("Y-m-d", time() + 60 * 60 * 24 * 30 ),
        "Name" => "N/A",
        "StageName" => "Prospecting"
    );
    $account_data = array(
        "Name" => "N/A",
        "Type" => "Customer"
    );
    $lead_data = array();
    $lead_id = null;
    $salesforce_id = null;
    // mapping to the license levels
    $license_type_mappings = array(
        "lavu88" => "POS - L88",
        "silver" => "POS - Silver",
        "gold" => "POS - Gold",
        "pro" => "POS - Pro",
        "platinum" => "POS - Pro",
        'fitsilver' => "Fit - Silver",
        'fitgold' => "Fit - Gold",
        'fitpro' => "Fit - Pro",
        "give" => "Give",
        "goldtrial" => "Trial - Gold",
        "protrial" => "Trial - Pro",
        "lavu88trial" => "Trial - L88",
        "mercurygold" => "Free - Gold - Merc",
        "freegoldmerc" => "Free - Gold - Merc",
        "mercurypro" => "Free - Pro - Merc",
        "freepromerc" => "Free - Pro - Merc",
        "mercuryfreelavu88" => "Free - L88 - Merc",
        "freelavu88merc" => "Free - L88 - Merc",
        "evosnapfreelavu88o" => "Free - L88 - Evo",
        "freelavu88evo" => "Free - L88 - Evo",
        "lavu" => "POS - Lavu",
        "lavuenterprise" => "POS - LavuEnterprise",
    );
    $location_type_mapping = array(
        'defaultbar' => 'Bar/Lounge',
        'default_coffee' => 'Coffee Shop',
        'default_pizza_' => 'Pizza Shop',
        'default_restau' => 'Restaurant',
    );

    // all the data comes in the same array, we need to demux it
    // into the three array for each object (account, contact, opportunity)
    // yes, this an fugly piece of code, but c'est la vie
    foreach( $data_array as $key => $v ) {
        $value = trim($v);
        if ( empty( $value ) ) continue;
        switch ( str_replace('_', '', strtolower($key)) ) {
            case "phone" :
                $contact_data["Phone"] = $value;
                $lead_data['Phone'] = $value;
                break;
            case "companyname":
            case "company":
                $account_data["Name"] = $value;
                $opportunity_data["Name"] = $value;
                $lead_data['Company'] = $value;
                break;
            case "zip":
                $account_data["Zip_Postal_Code__c"] = $value;
                break;
            case "phone":
                $contact_data["Phone"] = $value;
                $lead_data['Phone'] = $value;
                break;
            case "email":
                $contact_data["Email"] = sanitize_email($value);
                $opportunity_data["Primary_Email__c"] = $contact_data["Email"];
                $lead_data['Email'] = sanitize_email($value);
                break;
            case 'ipaddress':
                $lead_data['IP_Address__c'] = $value;
                break;
            case "stage":
            case "stagename":
                $opportunity_data["StageName"] = $value;
                break;
            case "lead_source":  // underscores actually get removed
            case "leadsource":
            case "domain":
                $opportunity_data['LeadSource'] = preg_replace('/(Web-SignupForm-){2,}/', '$1', $value);
                $lead_data['LeadSource'] = (strpos($value, 'signup') !== false) ? 'Free Trial' : $value;  // per brad
                break;
            case "level":
            case "license_type":
                 $opportunity_data["License_Type__c"] = $license_type_mappings[strtolower(preg_replace('/[123]$/','', $value ))];
                break;
            case "firstname":
            case "first":
                $contact_data["FirstName"] = $value;
                $lead_data['FirstName'] = $value;
                break;
            case "lastname":
            case "last":
                $contact_data['LastName'] = $value;
                $lead_data['LastName'] = $value;
                break;
            case "city" :
                $account_data['City__c'] = $value;
                $lead_data['City__c'] = $value;
                break;
            case "state" :
                $account_data['State_Provence__c'] = $value;
                $lead_data['State_Provence__c'] = $value;
                break;
            case "country":
                $account_data['Country__c'] = $value;
                $lead_data['Country__c'] = $value;
                break;
            case "address":
                $account_data['Address__c'] = $value;
                $lead_data['Address__c'] = $value;
                break;
            case "restaurantid":
                $account_data['Restaurant_ID__c'] = $value;
                break;
            case "hostingdate":
                $opportunity_data["Hosting_Date__c"] = $value;
                break;
            case "licensingfee":
                $opportunity_data["Licensing_Fee__c"] = $value;
                break;
            case "monthlyfee":
                $opportunity_data["Monthly_Fee__c"] = $value;
                break;
            case "promo":
            case "promocode":
                $opportunity_data["Promo_Code__c"] = $value;
                break;
            case "enteredcc":
                $opportunity_data["Entered_CC__c"] = 1;
                break;
            case "closedate":
                $opportunity_data["CloseDate"] = $value;
                break;
            case "accounttype":
                $account_data['Type']= $value;
                break;
            case "dataname":
            case "data_name":
                $account_data['Data_Name__c'] = $value;
                $lead_data['Data_Name__c'] = $value;
                break;
            case 'default_menu':
                $lead_data['location_type__c'] = isset($location_type_mapping[$value]) ? $location_type_mapping[$value] : '';
                break;
            case "distro":
            case "distrocode":
                $opportunity_data['Distro_Code__c'] = $value;
                break;
            //special case
            case "modules":
                $opportunity_data['Modules__c'] = $value;
                break;
            case "mopointsearned":
                $opportunity_data['Mo_Points_Earned__c'] = $value;
                break;
            case "licensedate":
                $opportunity_data['License_Date__c'] = date('r', strtotime( $value ));
                break;
            case "leadid":
            case "lead_id":
            case "id":
                $lead_id = $value;
                break;
            case "accounttype":
                $account_data["Type"] = $value;
                break;
            case "parentid":
                $account_data['ParentId'] = $value;
                break;
            case "campaignname" :
            case "utmcampaign" :
                $opportunity_data["Campaign_Name__c"] = $value;
                $lead_data['Campaign_Name__c'] = $value;
                break;
            case "campaignsource":
            case "utmsource":
                $opportunity_data["Campaign_Source__c"] = $value;
                $lead_data['Campaign_Source__c'] = $value;
                break;
            case "campaignmedium":
            case "utmmedium":
                $opportunity_data['Campaign_Medium__c'] = $value;
                $lead_data['Campaign_Medium__c'] = $value;
                break;
            case "landingpage":
            case "entryurl":
                $opportunity_data["Landing_Page__c"] = $value;
                break;
            case "licensetype":
                $opportunity_data["License_Type__c"] = $value;
                break;
            case "leadlicensetype":
                $opportunity_data["Lead_License_Type__c"] = isset($license_type_mappings[strtolower(preg_replace('/[123]$/','', $value ))]) ? $license_type_mappings[strtolower(preg_replace('/[123]$/','', $value ))] : $value;
                break;
            case "referringsite":
            case "referer":
                $opportunity_data['Referring_Site__c'] = $value;
                break;
            case "cancelleddate":
                $opportunity_data['Cancelled_Date__c'] = $value;
                break;
            case "numlocations":
                $opportunity_data['Number_Locations__c'] = $value;
                break;
            case "totalterminals":
                $opportunity_data['Total_Terminals__c'] = $value;
                break;
            case "distropoints":
                $opportunity_data['Distro_Points__c'] = $value;
                break;
            // in case the salesforce id is already in the data, then we can use it if there is no lead_id
            case "salesforceid":
                $salesforce_id = $value;
                break;
            case "description" :
                $opportunity_data['Description'] = $value;
                //$account_data['Description'] = $value;
                //$account_data['Description__c'] = $value;
                break;
            // I don't think these Free Lavu fields would come to Salesforce from the reseller_leads stuff (because it gets updated supplementally from the Free Lavu
            // CP pages inserting into the billing_salesforce_events table) but I wanted to include them just so there's record of these Salesforce custom fields.
            case "userupdatedbusinesslocation__c":
            case "user_wants_integrated_payments__c":
            case "user_requested_7_day_trial_extension__c":
            case "userupdatedbusinesslocation__c":
            case "user_wants_integrated_payments__c":
            case "user_requested_7_day_trial_extension__c":
            // Donnie's custom field from the Live Demo signup page.
            case "live_demo_notes__c":
            case "livedemo_notesc":
            // "# of CC Transactions" section in Salesforce
            case 'payment_processor__c': case 'paymentprocessorc':
            case 'first_trans_date__c': case 'firsttransdatec':
            case 'trans_jan__c':   case 'transactions_total_january__c':
            case 'trans_feb__c':   case 'transactions_total_february__c':
            case 'trans_mar__c':   case 'transactions_total_march__c':
            case 'trans_apr__c':   case 'transactions_total_april__c':
            case 'trans_may__c':   case 'transactions_total_may__c':
            case 'trans_june__c':  case 'transactions_total_june__c':
            case 'trans_july__c':  case 'transactions_total_july__c':
            case 'trans_aug__c':   case 'transactions_total_august__c':
            case 'trans_sept__c':  case 'transactions_total_september__c':
            case 'trans_oct__c':   case 'transactions_total_october__c':
            case 'trans_nov__c':   case 'transactions_total_november__c':
            case 'trans_dec__c':   case 'transactions_total_december__c':
            case 'transjanc':   case 'transactionstotaljanuaryc':
            case 'transfebc':   case 'transactionstotalfebruaryc':
            case 'transmarc':   case 'transactionstotalmarchc':
            case 'transaprc':   case 'transactionstotalaprilc':
            case 'transmayc':   case 'transactionstotalmayc':
            case 'transjunec':  case 'transactionstotaljunec':
            case 'transjulyc':  case 'transactionstotaljulyc':
            case 'transaugc':   case 'transactionstotalaugustc':
            case 'transseptc':  case 'transactionstotalseptemberc':
            case 'transoctc':   case 'transactionstotaloctoberc':
            case 'transnovc':   case 'transactionstotalnovemberc':
            case 'transdecc':   case 'transactionstotaldecemberc':
                $opportunity_data[$key] = $value;
                break;
        }
    }

    //error_log( "DEBUG: Passed-in fields for {$data_array['dataname']}\n". print_r($data_array, true) ."\nSalesforce fields for {$data_array['dataname']}\naccount_data=". print_r($account_data, true) ."\nopportunity_data=". print_r($opportunity_data, true) );

    // if the lead_id is not there, abort!
    // Now that everything is in its proper arrays (hopefully)
    // we get the salesforce credentials and initialize the account
    // handler
    $success = true;
    $err_msg = '';
    try {
        // the credentials might have been initialized already and stored in the tmp folder
        // so we don't want to double dip
        $token_file_path = '/tmp/salesforce_token.txt';
        $file_exists = file_exists( $token_file_path );
        $file_not_empty = $file_exists && filesize($token_file_path);
        $is_token_fresh = $file_not_empty && ( time()  < ( filemtime( $token_file_path ) + 60 * 60 * 2 ) );
        $token = $is_token_fresh ? unserialize( file_get_contents($token_file_path ) ) : false;
        require_once 'salesforce/SalesforceLogin.php';
        if ( !empty( $token ) && !empty( $token['access_token'] ) ) {
            $credentials = new SalesforceLogin( $token );
        } else {
            require_once '/home/poslavu/private_html/salesforceCredentials.php';
            $credentials = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
            file_put_contents( $token_file_path, serialize($credentials->get_all() ) );
        }

        // New logic that imports Free Trial signups as Salesforce Leads
        if ( $data_array['create'] == 'lead' )
        {
            require_once 'salesforce/SalesforceLead.php';

            $lead_meta_file = '/home/poslavu/private_html/salesforceLeadMeta.txt';
            $lead_meta = file_exists( $lead_meta_file  ) ? file_get_contents( $lead_meta_file ) : null;
            $lead_meta = @unserialize( $lead_meta );

            $lead_handler = new SalesforceLead( $credentials, $lead_meta );
            $lead_response = $lead_handler->upsert('DBID__c', $lead_id, $lead_data );

            $salesforce_lead_id = $lead_response['id'];
            $success = !empty($salesforce_lead_id);
            $err_msg = (!empty($lead_response[0]['errorCode'])) ? $lead_response[0]['errorCode'] : '';
        }
        // Old logic which imports data as Accounts, Opportunities, and Contacts
        else
        {
            require_once 'salesforce/SalesforceAccount.php';
            require_once 'salesforce/SalesforceContact.php';
            require_once 'salesforce/SalesforceOpportunity.php';

            // if everything went well, initialize the account handler
            $account_meta_file = '/home/poslavu/private_html/salesforceAccountMeta.txt';
            $account_meta = file_exists( $account_meta_file ) ? file_get_contents($account_meta_file ) : null;
            $account_meta = @unserialize( $account_meta );

            $contact_meta_file = '/home/poslavu/private_html/salesforceContactMeta.txt';
            $contact_meta = file_exists( $contact_meta_file  ) ? file_get_contents( $contact_meta_file ) : null;
            $contact_meta = @unserialize( $contact_meta );

            $opportunity_meta_file = '/home/poslavu/private_html/salesforceOpportunityMeta.txt';
            $opportunity_meta = file_exists( $opportunity_meta_file ) ? file_get_contents( $opportunity_meta_file ) : null;
            $opportunity_meta = @unserialize( $opportunity_meta );

            $account_handler = new SalesforceAccount( $credentials, $account_meta );
            $contact_handler = new SalesforceContact( $credentials, $contact_meta );
            $opportunity_handler = new SalesforceOpportunity( $credentials, $opportunity_meta );

            // do the upsert
            if ( !empty( $salesforce_id ) ) {
                $account_response = $account_handler->update_by_id( $salesforce_id, $account_data );
                $account_response  = empty( $account_response ) ? array('updated' => true ) : $account_response;
            } elseif ( empty( $lead_id ) ) {
                $account_response = $account_handler->insert( $account_data );
            } else {
                $account_response  = $account_handler->upsert("Lead_ID__c", $lead_id, $account_data );
            }

            if ( empty( $account_response['updated'] ) ) {
                $account_sforce_id  = $account_response['id'];
                $contact_data["AccountId"] = $account_sforce_id;
                $opportunity_data["AccountId"] = $account_sforce_id;
                $response = $contact_handler->insert( $contact_data );
                $response = $opportunity_handler->insert( $opportunity_data );
                $opportunity_sforce_id = isset($response['id']) ? $response['id'] : '';
            } else {
                if ( empty( $salesforce_id ) ) {
                    $retrieved_account = $account_handler->get_by_external_id( "Lead_ID__c", $lead_id );
                    $account_sforce_id = $retrieved_account['Id'];
                } else {
                    $account_sforce_id = $salesforce_id;
                }

                $contact_data["AccountId"] = $account_sforce_id;
                $opportunity_data["AccountId"] = $account_sforce_id;
                $contact_id = $contact_handler->get_by_field("AccountId", $account_sforce_id );

                // Fix Salesforce duplicate record error caused by the default Salesforce Contact dupe rule sets.  Emualting criteria from the "Contact Email Duplicate Rule"
                // in our soql query but not the "Contact Detailed Duplicate Rule" didn't apply since it uses AccountId and we've already checked for that.
                if ( !isset($contact_id['records']) || (isset($contact_id['records']) && $contact_id['totalSize'] === 0) ) {
                    $contact_id = $contact_handler->query("SELECT Id FROM Contact WHERE Email='". str_replace("'", '', $contact_data['Email']) ."' AND LastName='". str_replace("'", '', $contact_data['LastName']) ."'");
                }
                if ( !empty( $contact_id['records'] ) ){
                    $response = $contact_handler->update_by_id( $contact_id['records'][0]['Id'], $contact_data );
                } else {
                    $response = $contact_handler->insert( $contact_data );
                }
                $opportunity_id = $opportunity_handler->get_by_field("AccountId", $account_sforce_id );
                if ( !empty( $opportunity_id['records'] ) ){
                    $opportunity_sforce_id = $opportunity_id['records'][0]['Id'];
                    $response = $opportunity_handler->update_by_id( $opportunity_sforce_id, $opportunity_data );
                } else {
                    $response = $opportunity_handler->insert( $opportunity_data );
                    $opportunity_sforce_id = isset($response['id']) ? $response['id'] : '';
                }
            }
        }
    } catch ( SalesforceHandlerException $e ) {
        if ( $data_array['create'] == 'lead' && stripos($e->getMessage(), 'duplicate') !== false ) {
             sendDuplicateLeadEmail('Free Trial', $data_array);
        }

        mail("tom@lavu.com", "Salesforce Error", $e->getMessage() ."\n\n". print_r( $data_array, true ));
        return array("success" => false, "err_msg" => "Exception::". $e->getMessage());
    }
    return array(
        "success" => $success,
        "err_msg" => $err_msg,
        "id" => (empty( $account_sforce_id ) ? null : $account_sforce_id),
        "opportunity_id" => (empty( $opportunity_sforce_id ) ? null : $opportunity_sforce_id),
        "salesforce_lead_id" => (empty( $salesforce_lead_id ) ? null : $salesforce_lead_id),
    );
}

function sendDuplicateLeadEmail($site_name, $data) {
    $hostname = php_uname('n');
    $ts = date("Y-m-d H:i:s");

    $html_table_rows = '';
    foreach( $data as $key => $value ) {
        $html_table_rows .= "<tr>{$key}<td></td><td>{$value}</td></tr>";
    }
    $html_email = <<<HTML
<!doctype html>
<html>
    <head><meta charset="UTF-8"></head>
    <body style="font-family:sans-serif;color:#2b2b2b;background-color:#ffffff;padding:0;margin:0;">
        <div style="width:600px;padding:20px;margin:0 auto;">
            <img src="https://www.lavu.com/img/lavu_logo.png" border="0">
            <h2 style="margin-left:5%">{$hostname}</h2>
            <h2 style="margin-left:5%">{$ts}</h2>
            <h2 style="margin-left:5%">Lead #{$data['lead_id']}</h2>
            <h3 style="margin-left:5%">Duplicated Lead Info</h3>
            <table>
            {$html_table_rows}
            </table>
        </div>
    </body>
</html>
HTML;

    require_once('/home/poslavu/public_html/register/mailer.php');
    return send_multipart_email( 'noreply@poslavu.com', "Duplicate Salesforce Lead - {$site_name}", $html_email, '', 'sales@lavu.com' );
}

function authorized_cors_domain( $domain  ){
    $sanitized_domain = preg_replace( '/http(?:s)?:\/\//','',$domain);
    $sanitized_domain = str_replace(  'www.', '', $sanitized_domain );
    $sanitized_domain = str_replace(  '.com', '',  $sanitized_domain );
    return array_key_exists( $sanitized_domain , array(
        "wordpressdev.poslavu" => true,
        "wordpressdev.lavu" => true,
        "admin.poslavu" => true,
        "dev.lavu" => true,
        "dev.poslavu" => true,
        "carlos.poslavu" => true,
        "carlos.lavuweb" => true,
        "carlos.lavuwordpress" => true,
        "baripadpos" => true,
        "barpointofsalesystem" => true,
        "blog.lavu" => true,
        "brewpubpos" => true,
        "dac9ml01200010.lavu" => true,  # tom.lavu
        "dlavu" => true,
        "eventspos" => true,
        "fitnessipadpos" => true,
        "foodtruckipadpos" => true,
        "foodtruckpos" => true,
        "franchiseipadpos" => true,
        "frozenyogurtipadpos" => true,
        "getlavu" => true,
        "gokartpos" => true,
        "gymipadpos" => true,
        "gymipadsystem" => true,
        "gympointofsale" => true,
        "hotelpos" => true,
        "indoorgokartsystem" => true,
        "ipad-pos-app" => true,
        "ipadfitnesspos" => true,
        "ipadgym" => true,
        "ipadgympos" => true,
        "ipadgymsystem" => true,
        "ipadkds" => true,
        "ipadpos-lavu" => true,
        "kartpos" => true,
        "kartsoftware" => true,
        "kitchendisplaysystem" => true,
        "lavapos" => true,
        "lavu" => true,
        "lavuadvance" => true,
        "lavucommunity" => true,
        "lavuconnect" => true,
        "lavufit" => true,
        "lavufitness" => true,
        "lavugive" => true,
        "lavuhardware" => true,
        "lavuhospitality" => true,
        "lavuinc" => true,
        "lavukart" => true,
        "lavulicense" => true,
        "lavulite" => true,
        "lavumed" => true,
        "lavupos" => true,
        "lavustatus" => true,
        "lavusys" => true,
        "lavutithe" => true,
        "lavutogo" => true,
        "medlavu" => true,
        "mylavu" => true,
        "mylavulicense" => true,
        "pizza-pos" => true,
        "pizzaipadpos" => true,
        "poscoffeeshop" => true,
        "posdelivery" => true,
        "posfec" => true,
        "posforbars" => true,
        "posforchurch" => true,
        "posfordeli" => true,
        "posforgym" => true,
        "posforipads" => true,
        "posforpizza" => true,
        "poshotel" => true,
        "poslava" => true,
        "poslavureviews" => true,
        "poswithscale" => true,
        "register.localhost" => true,
        "register.tom.lavu" => true,
        "softwareforgym" => true,
        "tom.lavu" => true,
        "ufcpos" => true,
        "weblavu" => true,
        "welavu" => true,
        "yogurtipadpos" => true
    ) );
}

function send_to_zendesk( $data_array, $action="POST", $url="/tickets.json" ) {
    define("ZDAPIKEY", "iNMnxdnLbCGkTBUGg9buAv6kryhFGiPC4devNOcX");
    define("ZDUSER", "rt@lavu.com");
    define("ZDURL", "https://lavu.zendesk.com/api/v2");
//}

//function curlWrap($url, $json, $action, &$error, &$curl_info, &$decoded, $b_do_ticket_audit_replacements = TRUE, $b_draw_errors = FALSE)
//{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
    curl_setopt($ch, CURLOPT_URL, ZDURL.$url);
    //curl_setopt($ch, CURLOPT_URL, 	"https://lavu.zendesk.com/api/v2".$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, ZDUSER."/token:".ZDAPIKEY);
    $json = json_encode( $data_array );
    //curl_setopt($ch, CURLOPT_USERPWD, "rt@lavu.com"."/token:"."iNMnxdnLbCGkTBUGg9buAv6kryhFGiPC4devNOcX");
    switch($action){
        case "POST":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            break;
        case "GET":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            break;
        case "PUT":
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        default:
            break;
    }
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $output = curl_exec($ch);
    if (trim(curl_error($ch)) != "")
        $error = json_decode(curl_error($ch), true);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $curl_info = curl_getinfo($ch);
    $curl_info['body'] = substr($output, $header_size);
    curl_close($ch);
    $decoded = json_decode($curl_info['body'], true);
    if ((int)$curl_info['http_code'] < 300)
        return TRUE;
    return FALSE;
}
