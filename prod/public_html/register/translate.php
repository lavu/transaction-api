<?php

	// TODO refactor this into a class
		
	function translate($phrase = '', $locale = '') {
	
		// api_key: AIzaSyA2DDg0cWMQOoW9-qLJrzzaylweJO44Kq4
		// spanish: es
		// chinese: zh-TW
		
		$uri = "https://www.googleapis.com/language/translate/v2";
		$api_key = "AIzaSyA2DDg0cWMQOoW9-qLJrzzaylweJO44Kq4";
		$source = "en";
		$localized_content = $phrase; // default content to en source
		
			
		switch ($locale) {
		    case 'es':
		        
		        $target = "es";
		        break;
		    case 'cn':
		        
		        $target = "zh-TW";
		        break;

		    default:
		       // default to english
		       $target = "en";
		       
		}
		
		
		if (!empty($phrase) && $target != 'en') {
		
			// all data to be included with the request
            $values = array(
                'key'    => $api_key,
                'target' => $target,
                'format' => 'html',
                'q'      => $phrase
            );
            
            // turn the data array into raw format so it can be used with cURL
            $tran_data = http_build_query($values);
		
		 	// create a connection to the API endpoint
            $ch = curl_init($uri);
 
            // tell cURL to return the response rather than outputting it
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
            // write the data to the request in the post body
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tran_data);
 
            // include the header to make Google treat this post request as a get request
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET'));
 
            // execute the HTTP request
            $localized_content = curl_exec($ch);
            curl_close($ch);
		
			include_once('../admin/lib/JSON.php'); //include custom json methods
						
			$localized_content = json_decode($localized_content); // decode json response
			
			//var_dump($localized_content);
			$localized_content = $localized_content->data->translations[0]->translatedText;
			
		}
		
		return $localized_content;
				
	}
	
	function translate_test($phrase = '', $locale = '') {
	
		// api_key: AIzaSyA2DDg0cWMQOoW9-qLJrzzaylweJO44Kq4
		// spanish: es
		// chinese: zh-TW
		
		$uri = "https://www.googleapis.com/language/translate/v2";
		$api_key = "AIzaSyA2DDg0cWMQOoW9-qLJrzzaylweJO44Kq4";
		$source = "en";
		$localized_content = $phrase; // default content to en source
		
	
				
			
		switch ($locale) {
		    case 'es':
		        
		        $target = "es";
		        break;
		    case 'cn':
		        
		        $target = "zh-TW";
		        break;

		    default:
		       // default to english
		       $target = "en";
		       
		}
		
		
		if (!empty($phrase) && $target != 'en') {
		
			// all data to be included with the request
            $values = array(
                'key'    => $api_key,
                'target' => $target,
                'format' => 'html',
                'q'      => $phrase
            );
            
            // turn the data array into raw format so it can be used with cURL
            $tran_data = http_build_query($values);
		
		 	// create a connection to the API endpoint
            $ch = curl_init($uri);
 
            // tell cURL to return the response rather than outputting it
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
            // write the data to the request in the post body
            curl_setopt($ch, CURLOPT_POSTFIELDS, $tran_data);
 
            // include the header to make Google treat this post request as a get request
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-HTTP-Method-Override: GET'));
 
            // execute the HTTP request
            $localized_content = curl_exec($ch);
            curl_close($ch);
            
			include_once('../admin/lib/JSON.php'); //include custom json methods
						
			$localized_content = json_decode($localized_content); // decode json response
			
			$localized_content = $localized_content->data->translations[0]->translatedText;
			
		}
		
		return $localized_content;
				
	}
	
?>