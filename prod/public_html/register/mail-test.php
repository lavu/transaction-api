<?php
header( "Content-type: text/plain;" );
require_once( "/home/poslavu/public_html/admin/cp/resources/lavuquery.php" );
$curr_time = time();
$unix_day = 24 * 3600;
$f = "Y-m-d";
$today = date( $f, $curr_time );
$_7_days_ago =  date( $f, $curr_time - 7 * $unix_day );
$_11_days_ago = date( $f, $curr_time - 11 * $unix_day );
$periods = array( $_7_days_ago, $_11_days_ago );
$query_frmt = <<<SQL
select email, signup_timestamp, date, dataname from signups where signup_timestamp like '[period]%'
SQL;
$activity_query = <<<SQL
SELECT last_modified_date FROM menu_items ORDER BY last_modified_date DESC LIMIT 1
SQL;
$distro_query = <<<SQL
SELECT distro_code FROM restaurants WHERE data_name LIKE '[dataname]'
SQL;
$distro_email_query = <<<SQL
SELECT email FROM resellers WHERE username LIKE '[distro_code]'
SQL;
$query_args = array();
foreach( $periods as $period ) {
	echo <<<TXT


Registrations Received on $period
------------------------------------


TXT;
	$query_args["period"] = $period;
	$result = mlavu_query( $query_frmt, $query_args );
	$all_rows = array();
	while ( $row = mysqli_fetch_assoc( $result ) ) {
		$all_rows[] = $row;
	}
	mysqli_free_result( $result );
	if ( !empty( $all_rows ) ) {
		foreach( $all_rows as $row ) {
			// check for activity
	//		print_r( $row );
	//		$GLOBALS['data_name'] = $row['dataname'];
			$db_name = "poslavu_{$row['dataname']}_db";
			echo "\$db_name='$db_name'\n";
			lavu_connect_shard_db( $row['dataname'], $db_name );
			$result = lavu_query( $activity_query );
			$tmp = mysqli_fetch_assoc( $result );
			mysqli_free_result( $result );
			if ( $tmp === false ) echo "error getting activity from {$row['dataname']} : $db_name\n";
			else {
				$no_activity = strtotime( $tmp['last_modified_date'] ) <= strtotime( $row["signup_timestamp"] );
				if ( $no_activity ) {
					// get distro name with $dataname
					$result =  mlavu_query( $distro_query, array( 'dataname' => $row['dataname'] ) );
					extract( mysqli_fetch_assoc( $result ) );
					// now $distro_code is up
					mysqli_free_result( $result );
					// get reseller email
					echo "\$distro_code='$distro_code'\n";
					if ( empty( $distro_code ) ){
						echo "No distro found\n";
					} else {
						$result =  mlavu_query( $distro_email_query , array( 'distro_code' => $distro_code ) );
						$tmp = mysqli_fetch_assoc( $result );
						echo $tmp !== false ? 
							"Sending email to reseller at {$tmp['email']}\n" : 
						    "Distro Code $distro_code is incorrect, no email found\n" ;
						echo "Send reminder to {$row['email']}\n";
					}
				} else{
					echo "{$row['email']} has done stuff, nothing to send\n\n";
				}
			}
		}
	} else {
		"Nobody to send emails ... -_- ";
	}
	
}
//get test user
//$result =  mlavu_query( "select email, signup_timestamp, dataname from signups where signup_timestamp like '[period]%'", array( 'period' => $period ) );
//$result =  mlavu_query( "select email, signup_timestamp, dataname from signups where dataname like '[dataname]'", array( 'dataname' => 'test_inc2' ) );
//$result =  mlavu_query( "select email, signup_timestamp, dataname from signups where email in ( 'electpaisa@yahoo.com.mx', 'carlosa@poslavu.com', 'test@poslavu.com', 'electpaisa@gmail.com' )" );
?>