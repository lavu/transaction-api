<?php
require_once dirname(__FILE__) . "/check_ban_ip.php";

if (getenv('DEV') == '1' && !defined('DEV')) {
    define('DEV', 1);
    ini_set('display_errors', 1);
    error_reporting(error_reporting() ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
} else if (!defined('DEV')) {
    define('DEV', 0);
}

$doc_root = $_SERVER['DOCUMENT_ROOT'];
define('DOC_ROOT', $doc_root);
$hthost = $_SERVER['HTTP_HOST'];
define('HTHOST', $hthost);
$HAProxy_IPS = array(
    "10.130.216.73",
    "192.168.100.42",
    "10.130.216.74",
    "192.168.100.43"
);

//if (HTHOST === 'register.poslavu.com' && $_SERVER['SERVER_PORT'] != 443 && !DEV && ! in_array ($_SERVER['REMOTE_ADDR'], $HAProxy_IPS)) {
//    header("Location: https://register.poslavu.com{$_SERVER["REQUEST_URI"]}");
//    exit ;
//}

$admin_dir = DEV ? 'LavuBackend' : "admin";
$admin_path = DOC_ROOT . "/../$admin_dir";
define("ADMIN_PATH", $admin_path);
$lavuweb = DEV ? 'http://'. str_replace('register.', '', $_SERVER['SERVER_NAME']). ':' .$_SERVER['SERVER_PORT'] : "https://www.lavu.com";
define("LAVUWEB", $lavuweb );
$lavu_admin = DEV ? 'http://'. str_replace('register.', '', $_SERVER['SERVER_NAME']). ':' .$_SERVER['SERVER_PORT'] : "https://admin.poslavu.com";
define("LAVUADMIN", $lavu_admin );
require "$doc_root/lib/limonade.php";

function configure() {
    option('base_uri', option('base_path'));
    option('debug', DEV === 1  );
    require_once("/home/poslavu/public_html/admin/cp/objects/dbFunctions/DB.php");
    require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
    require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");
    require_once option("dir_root") . "signup_shared.php";
    $captcha_public_key = DEV ?
        "6LewUvoSAAAAAKrQXSHa92-SPH3BfMVggcNE7G1O":
        "6LfrtOsSAAAAAAIvqmehbwGW6vBo8GI1qGL_ngVR";
    define( "CAPTCHA_PUBLIC", $captcha_public_key );
    error_layout('layouts/error.php');
    global $o_signup;
    $o_signup = new RegisterSignupShared();
    // some globals
    global $o_package_container;
    if ( !isset($o_package_container) ){
        $o_package_container = new package_container();
    }
    global $maindb;
    $maindb = "poslavu_MAIN_db";
    global $b_restaurant_created_in_auth_account;
    $b_restaurant_created_in_auth_account = FALSE;
    global $allow_4111_testing;
    $allow_4111_testing = true;
    mlavu_select_db($maindb);
}

dispatch("/lavufit-email", function () {
    return html("layouts/lavufit-postregister-email.html.php", null);
});

dispatch("/lavufit-email-txt", function () {
    return txt("layouts/lavufit-postregister-email.txt.php", null);
});

dispatch_post( '/submit', "submit_signup");
dispatch_post( '/staples-submit', "submit_signup");
dispatch_post( '/newegg-submit', "submit_signup");

dispatch_post('/lead-submit', "real_lead_submit");
dispatch('/lead-submit', "real_lead_submit");

dispatch_post("/update-password", "approved_change_password");
dispatch_post( "/resend-email", "approved_resend_email");

function real_lead_submit() {
    $lead_cols = array();
    $lead_cols['lead_source'] = $_REQUEST['lead_source'];
    $lead_cols['created'] = $_REQUEST['created'];
    $lead_cols['company_name'] = $_REQUEST['company'];
    $lead_cols['email'] = $_REQUEST['email'];
    $lead_cols['firstname'] = $_REQUEST['first_name'];
    $lead_cols['lastname'] = $_REQUEST['last_name'];
    $lead_cols['phone'] = $_REQUEST['phone'];
    $lead_cols['session_id'] = empty( $_REQUEST['session_id'] )? session_id() : $_REQUEST['session_id'];
    $lead_cols['lead_strength'] = 1;
    $lead_cols['notes'] = $_REQUEST['message'];
    $dbid = create_or_update_lead( $lead_cols, true );
    return json( array( "success" => $dbid?true:false, "dbid" => $dbid ) );
}

dispatch_post( "/submit-contact-form", "submit_contact_form" );
dispatch(      "/submit-resource-form", "submit_resource_form" );
dispatch_post( "/submit-resource-form", "submit_resource_form" );
dispatch_post( "/submit-pro-lead", "pro_lead_submit_gateway" );
dispatch(      "/submit-pro-lead", "pro_lead_submit_gateway" );
dispatch(      "/live-demo-submit", "live_demo_submit_gateway");
dispatch_post( "/live-demo-submit", "live_demo_submit_gateway");
dispatch(      "/submit-live-demo", "submit_live_demo");
dispatch_post( "/submit-live-demo", "submit_live_demo");
dispatch(      "/submit-contact", "submit_contact");
dispatch_post( "/submit-contact", "submit_contact");

dispatch_post('/lead_submit.php', "faux_lead_submit");
dispatch('/lead_submit.php', "faux_lead_submit");

function faux_lead_submit() {
    send_header('Content-Type: application/json; charset='.strtolower(option('encoding')));
    return "{success:true}";
}

dispatch("^/submission.([abcdef1234567890]*).js", "deferred_submit");
dispatch("^/check-submission-status/([abcdef1234567890]*)", "check_submission_status");

dispatch( '/bluestar/check-code/:promocode', "bluestar_check_promo" );
function bluestar_check_promo(){
    global $o_package_container;
    global $o_signup;
    $promocode = params('promocode');
    if (! preg_match("/\w{2}[BLUE]\w([1238])(?:[UC])\w/i", $promocode, $m ) ) {
        // invalidly formatted promocode, in theory we shouldn't get here
        $json = "{valid:false,err_msg:\"wrong format\"}";
        return js("{$_REQUEST['callback']}($json)");
    }
    //require_once ADMIN_PATH . "/distro/beta/promo_codes.php";
    $lvl_trans = array('8' => "Lavu88", '1' => "Silver", '2' => "Gold", '3' => "Platinum");
    $pkg_lvl = $lvl_trans[strtolower($m[1])];
    $dcnt_amnt = null;
    if ( !check_promo_code( $promocode, $pkg_lvl, $dcnt_amnt ) ) {
        // bad promocode, same as above but for different reasons
        if ( $_SERVER['REQUEST_METHOD'] == "POST" && !empty( $_REQUEST['callback'] ) ) {
            // means this is 
        }
        $json = "{valid:false,err_msg:\"invalid\"}";
        return js("{$_REQUEST['callback']}($json)");
    }
    $ssid = session_id();
    $json = "{valid:true,bluestar_session:\"$ssid\"}";
    return js("{$_REQUEST['callback']}($json)");
}

// No CC Signup (used by Free Lavu)
dispatch_post( '/activation-signup', 'activation_signup' );
dispatch( '/activation-signup', 'activation_signup' );
dispatch( '/activate-signup/:shifted_signups_id/:shifted_reseller_leads_id/:verification_token', 'activate_signup_loading_screen' );
dispatch( '/activate-signup-exec/:shifted_signups_id/:shifted_reseller_leads_id/:verification_token', 'activate_signup' );

// JSON No CC Signup
dispatch_post( '/signup', 'signup' );
dispatch( '/signup', 'signup' );

dispatch( '/approved/:id', "approved" );
dispatch( '/approved.php', "approved" );

dispatch_post('/bluestar', "step2");
dispatch_post('/:locale/:level/step2', "step2");

dispatch_post('/check_recaptcha', "check_recaptcha");
dispatch_post('/check_recaptcha.php', "check_recaptcha");


function check_recaptcha() {
    require_once option('root_dir') . "/recaptcha/recaptchalib.php";
    $privatekey = file_get_contents(option('root_dir') . "/recaptcha/privatekey.txt");
    $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
    echo json($resp -> is_valid ? true : false );
}

dispatch('phpinfo', 'php_info');

function php_info() {
    return phpinfo();
}

dispatch('/step1.php', "old_form");
dispatch('^/(staples|newegg|other)-3-month-promo', '_3_month_promo');
dispatch('/lavu88', 'lavu88');

dispatch('/', "step1_dispatcher");
dispatch('/:locale', 'step1_dispatcher');
dispatch('/:locale/step1.php', 'step1_dispatcher');
dispatch('/:locale/:level', 'step1_dispatcher');
dispatch('/:locale/:level/step1', 'step1_dispatcher');
dispatch('/:locale/:level/step1.php', 'step1_dispatcher');
dispatch("/:locale/:level/:product/", 'step1_dispatcher');
dispatch("/:locale/:level/:product/step1", 'step1_dispatcher');
dispatch("/:locale/:level/:product/step1.php", 'step1_dispatcher');



// dispatch( '/', 'home_page' );
// dispatch( '/:level/', 'step1' );
// dispatch_post( '/:level/step2', 'step2' );
// dispatch_post( '/:level/submit', 'submit_signup' );
// dispatch_post( '/check_recaptcha', "check_recaptcha" );

run();
?>
