Thank you for signing up for POSLavu!
Your username: <?php echo $username; ?>
Your password: <?php echo $password; ?>

To setup your menu and make other changes:
   http://admin.poslavu.com/

Your trial period will last for 14 days.  At the end of your 14-Day Free Trial, the Lavu License fee will be automatically charged in full.

If you are not yet ready to purchase and move forward with Lavu, please cancel and disable your account before your trial period has expired:

    1. Login the to the administrative control panel.

        admin.poslavu.com
    2. See the My Accounts area (top left).

    3. Scroll to the bottom of this section and click the Cancel All Billing & Disable Account button.

    4. Indicate a reason for cancelation, then click the Continue Cancelation and Disable your Account button.

** Failure to cancel the 14-Day Free Trial before the trial expiration will result in a $50.00 USD service charge if you require a refund. Unfortunately, a service fee for refunds is charged by our billing processor and is required to process any refunds.

Please call us at TLK-POS-LAVU (855-767-5288) or for International customers 480-788-LAVU for further information or questions.

Thank you,
The POSLavu Team