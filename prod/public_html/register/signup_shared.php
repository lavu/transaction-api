<?php
	if (getenv('DEV') == '1') {
		@define('DEV', 1);
		ini_set("display_errors","1");
		error_reporting(error_reporting() ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
	} else {
		@define('DEV', 0);
	}

	class RegisterSignupShared {

		// get an object of all niche products available
		// each nich product has the following values:
		//     @posts: an array of values to add to the $_POST vars
		//     @display_name: the printed name of the niche product
		//     @title: the description of the niche product
		//     @image: the image to use for this niche product
		public function getNicheProducts() {
			$cachevars = $this->getCacheVars();
			if (isset($cachevars->nicheProducts))
				return $cachevars->nicheProducts;

			$cachevars->nicheProducts = (object)array(
				'foodtruck'=>array(
                    'posts'=>array(
                        'mname'=>'default_coffee'
                    ), 
                    'display_name'=>'Food Truck', 
                    'display_namecn'=>'餐车', 
                    'display_namees'=>'Camión de Comida', 
                    'title'=>'This Food Truck starter menu is best for quick serve environments generally using single transactions.', 
                    'titlecn'=>'这种食物卡车起动菜单是最好的快速服务环境通常使用单交易。', 
                    'titlees'=>'Este menú de arranque Camión de Comida es mejor para entornos de servicio rápido por lo general el uso de transacciones individuales.', 
                    'image'=>'/images/type_1.png'),
				'pizza'=>array(
                    'posts'=>array(
                        'mname'=>'default_pizza_'
                    ), 
                    'display_name'=>'Pizza Shop', 
                    'display_namecn'=>'餐车', 
                    'display_namees'=>'Pizzeria', 
                    'title'=>'This Pizza Shop starter menu uses a table layout and provides some special functionality.', 
                    'titlecn'=>'这比萨饼店起动菜单使用表布局，并提供了一些特殊的功能。', 
                    'titlees'=>'Este menú de arranque Pizzeria utiliza un diseño de tabla y proporciona alguna funcionalidad especial.', 
                    'image'=>'/images/type_4.png'),
                'bar'=>array(
                    'posts'=>array(
                        "mname"=>"defaultbar"
                    ),
                    "display_name"=>"Bar/Lounge",
                    'display_namecn'=>'酒吧或休息室',
                    'display_namees'=>'Bar/Salón',
                    'title'=>'This Bar/Lounge starter menu uses a tab layout. Tabs are not tethered to a table.',
                    'titlecn'=>"这家酒吧/酒廊起动菜单使用tab布局。制表符不被拴到表中。",
                    'titlees'=>"Este menú de arranque Bar/Salón utiliza un diseño de las tabulación. Tabulaciónes no están atados a una mesa.",
                    "image"=>"/images/type_2.png"),
                'restaurant'=>array(
                    'posts'=>array(
                        "mname"=>"default_restau"
                    ),
                    "display_name"=>"Restaurant",
                    'display_namecn'=>'餐厅',
                    'display_namees'=>'Restaurante',
                    'title'=>'This Restaurant starter menu uses a table layout',
                    'titlecn'=>"此餐厅起动菜单使用表格布局。",
                    'titlees'=>"Este menú de arranque restaurante utiliza un diseño de tabla.",
                    "image"=>"/images/type_3.png"),
                "fitness" => array(
                    "posts"=>array(
                        "mname"=>"lavu_fit_demo"
                    ),
                    "display_name" => "Gym",
                    "display_namecn" => "健身房",
                    "display_namees" => "Gimnasio",
                    "title" => "A specialised starter menu for Gyms and Fitness Centers",
                    "titlecn" => "在健身房和健身中心的一个专门启动菜单",
                    "titlees" => "Menú de arranque especializado para gimnasios",
                    "image" => "/images/fit_icon_green_125px.png"
                )
			);

			return $cachevars->nicheProducts;
		}

		// get an array of valid menu names
		// @return: an array of valid menu names
		public function getValidMenuNames() {
			$cachevars = $this->getCacheVars();
			if (isset($cachevars->validMenuNames))
				return $cachevars->validMenuNames;

			$a_retval = array('default_restau', 'default_pizza_', 'defaultbar', 'default_coffee', 'lavu_fit_demo');
			$o_niche_products = $this->getNicheProducts();
			foreach($o_niche_products as $k=>$a_product) {
				$s_mname = !empty( $a_product['mname'] ) ? $a_product['mname'] : "";
				if (!in_array($s_mname, $a_retval))
					$a_retval[] = $s_mname;
			}
			$cachevars->validMenuNames = $a_retval;
			return $a_retval;
		}

		private function getCacheVars() {
			if (!isset($this->cachevars))
				$this->cachevars = new stdClass();
			return $this->cachevars;
		}
	}

	$o_signup = new RegisterSignupShared();

?>
