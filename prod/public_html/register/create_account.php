<?php
/**
 * Self-Reminder:
 * Don't use limonade functions without checking if they are available!!
 */


$hthost = $_SERVER['HTTP_HOST'];
$doc_root = $_SERVER['DOCUMENT_ROOT'];
$admin_dir_name = ($_SERVER['HTTP_HOST'] == "lavuregister") ? "LavuBackend" : "admin";
$admin_dir = "{$doc_root}/../{$admin_dir_name}";

if ( !defined('HTHOST') ) define( 'HTHOST', $hthost );
if ( !defined('DOC_ROOT') ) define('DOC_ROOT', $doc_root );
if ( !defined('ADMIN_DIR') ) define( "ADMIN_DIR", $admin_dir_name );
if ( !defined('ADMIN_PATH') ) define( "ADMIN_PATH", $admin_dir );

//error_log("DEBUG: env variables  HTHOST=". HTHOST ." DOC_ROOT=". DOC_ROOT ." ADMIN_DIR=". ADMIN_DIR ." ADMIN_PATH=". ADMIN_PATH);  //debug

require_once("/home/poslavu/public_html/admin/sa_cp/billing/send_email.php");

	function get_dataname_for_company($set_co)
	{
		$company_name_lower = trim(strtolower(str_replace(" ", "_", $set_co)));
		$dataname = "";

		for ($i = 0; $i < strlen($company_name_lower); $i++) {

			if ($company_name_lower[$i] == "-") {

				$dataname = $dataname."_";

			} else {

				if (preg_match("/^[ _a-z0-9]/i",$company_name_lower[$i])) {

					$dataname = $dataname.$company_name_lower[$i];
				}
			}
		}

		if (strlen($dataname) > 14) {
			$dataname = substr($dataname, 0, 14);
		}
		$check_data_names = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'",$dataname);
		if (mysqli_num_rows($check_data_names) > 0) {
			$n = 1;
			$new_dataname = $dataname.$n;
			$dataname_established = FALSE;
			while ($dataname_established == FALSE) {
				$found = FALSE;
				$check_data_names = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'",$new_dataname);
				if (mysqli_num_rows($check_data_names) > 0) {
					$n++;
					$new_dataname = $dataname.$n;
				} else {
					$dataname_established = TRUE;
				}
			}
		} else {
			$new_dataname = $dataname;
		}

		return $new_dataname;
	}

	function get_username_for_company($dataname)
	{
		$n = 0;
		$new_username = str_replace("_", ".", $dataname);
		$username_established = FALSE;
		while ($username_established == FALSE) {
			$verify_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$new_username);
			if(mysqli_num_rows($verify_query))
			{
				$n++;
				$new_username = str_replace("_", ".", $dataname).$n;
			}
			else
				$username_established = TRUE;
		}
		return $new_username;
	}

	function create_new_password()
	{
		$word_array = array("test", "eagle", "lavu", "mount", "fuzzy", "ipad", "bodacious", "quiz", "nice", "river", "sky", "cloud", "comp", "key", "scissor", "hello", "dog", "cat", "lion", "wind", "fire", "water", "guitar", "piano", "fish", "wing", "bird", "kind", "love", "car", "demo", "show", "laugh", "happy", "grand", "sunshine");

		$new_password = $word_array[rand(0, (count($word_array) - 1))].rand(1, 99);

		$new_password = create_new_db_password(rand(5,10));

		return $new_password;
	}

	function create_new_db_password($length=12)
	{
		$str = "";
		for($i=0; $i<$length; $i++)
		{
			$n = rand(0,61);
			if($n < 10) // 0-9
			{
				$setn = 48 + $n;
			}
			else if($n<=35) // 10-35
			{
				$setn = 55 + $n;
			}
			else if($n<=61) // 36-61
			{
				$setn = 61 + $n;
			}
			$str .= chr($setn);
		}
		return $str;
	}

	/**
	 * Updated: December 17th 2013 by Alvie (if you don't who Alvie is... go fuck yourself)
	 * This function has to be changed to properly handle multipart emails
     */
	function send_new_account_email($email_to,$username,$password,$lang,$mainvars=null) {
		$s_subject = "Your new Lavu POS Account";
		$html_message = "";
		$message = "";

		if($lang=='es') {

			$message = "
Gracias por inscribirse en POSLavu!
Su nombre de usuario: $username
Su contraseña: $password

Para configurar el menú y hacer otros cambios:
  http://admin.poslavu.com/

Su período de prueba tendrá una duración de 14 días. Al final de su 14-días de prueba gratis, la cuota de licencia Lavu se cargará automáticamente en su totalidad.

Si no está todavía listo para comprar y seguir adelante con Lavu, por favor, cancelar y desactivar su cuenta antes de que su período de prueba ha caducado:

     1. Ingresa la que el panel de control administrativo.

         admin.poslavu.com
     2. Ver el área Mis Cuentas (arriba a la izquierda).

     3. Desplácese hasta la parte inferior de esta sección y haga clic en el botón Cancelar todo Facturación y botón Desactivar cuenta.

     4. Indique una razón para la cancelación, a continuación, haga clic en Continuar la cancelación y deshabilitar el botón Cuenta.

El incumplimiento ** para cancelar la prueba gratuita de 14 días antes de la expiración trial dará lugar a un cargo de servicio de $ 50.00 USD si necesita un reembolso. Por desgracia, una cuota de servicio para las restituciones se pagan directamente en nuestro procesador de facturación y se utiliza para procesar los reembolsos.

Por favor llámenos al TLK-POS-LAVU (855-767-5288) o para clientes internacionales 480-788-LAVU para obtener más información o preguntas.

Gracias,
El equipo de POSLavu";


		} elseif($lang=='cn') {


			$message = "
感謝您為 POSLavu簽署！
您的用戶名: $username
您的密碼: $password

設置您的菜單和進行其他更改：
  http://admin.poslavu.com/

您的試用期將持續14天。在最後的14天免費試用，Lavu許可費將被自動收取全額費用。

如果您還不是準備購買和推進Lavu，請在您的試用期已經過期之前，取消和禁用您的帳戶：

    1。登入管理控制面板。

        admin.poslavu.com
    2。 “我的帳戶”區域（左上圖）。

    3。滾動到本節的底部，然後單擊“取消所有帳單和禁用帳戶”按鈕。

    4。說明取消的原因之一，然後單擊“繼續”對消“和”禁用“您的帳號”按鈕。

*如果取消14天免費試用前的trial的到期將導致在$ 50.00美元的服務費，如果您需要退款。不幸的是，所收取的服務費退款是我們的帳單處理，需要處理任何退款。

你可以在這裡取消免費試用： https://admin.poslavu.com/cp/index.php?mode=billing。

請致電我們 TLK-POS-LAVU（855-767-5288）或為國際客戶提供進一步的信息或問題 480-788-LAVU。

謝謝
POSLavu隊";

		} else {
			if ( !empty($mainvars['demo']) ) {
				$s_subject = "Your Lavu DEMO Account";
				$s_lavu_subject = "Demo signup - POSLavu";
				$html_message = "
				Hello!<br />
				<br />
				Your Lavu DEMO account has been created. Please use the following link to log in.<br />
				<br />
				<br />
				<blockquote style='margin:0px 0px 0px 40px; border:none; padding:0px'>
					<a href='http://admin.poslavu.com/'>http://admin.poslavu.com/</a><br />
					<br />
					<span style='font-weight:bold;'>Your username: {$username}</span><br />
					<span style='font-weight:bold;'>Your password: {$password}</span><br />
				</blockquote>
				<br />
				<br />
				<span style='text-decoration:underline;'>To change your password or add new users</span>:<br />
				<br />
				<br />
				<blockquote style='margin:0px 0px 0px 40px; border:none; padding:0px'>
					1. See <span style='font-weight:bold;'>Workforce </span>(from the left hand nav bar) <span style='font-weight:bold;'> > Manage Users</span> <br />
					<br />
					2. Select a user from the list and click on the <span style='font-weight:bold;'>“Pin & Password”</span> section.<br />
					<br />
					3. Enter your new password, confirm it by re-entering. Click <span style='font-weight:bold;'>Update </span>(green button at the bottom right).<br />
					<br />
				</blockquote>
				<br />
				<br />
				<span style='text-decoration:underline;'>To add new users</span>:<br />
				<br />
				<br />
				<blockquote style='margin:0px 0px 0px 40px; border:none; padding:0px'>
					1. See <span style='font-weight:bold;'>Workforce </span>(from the left hand nav bar) <span style='font-weight:bold;'> > Manage Users</span> <br />
					<br />
					2. Click on <span style='font-weight:bold;'>“Add New User”</span> on the top right corner, fill out all the details and click <span style='font-weight:bold;'>Save</span>.<br />
					<br />
					<blockquote style='margin:0px 0px 0px 40px; border:none; padding:0px'>
						* Note - Access Level 3 and above users can log into the control panel
					</blockquote>
				</blockquote>
				<br />
				<br />
				Thanks!<br />
				Lavu Development";
			} else {
			$s_lavu_subject = "Trial signup - Lavu POS";
			$s_subject = "Your new Lavu POS Account";
			ob_start();
			include 'views/emails/welcome-email.html.php';
			$html_message = ob_get_clean();
			}

		}

		if ($message == "" && $html_message != "") {
			// If we are here, then this is a multipart e-mail
			include_once 'mailer.php';
			$sender = "welcome@lavuinc.com";
			ob_start();
			include 'views/emails/welcome-email.txt.php';
			$message = ob_get_clean();

            if ( !empty( $mainvars['is_fitness'] ) ) {
                ob_start();
                include "/home/poslavu/public_html/register/views/layouts/lavufit-postregister-email.html.php";
                $html_message = ob_get_clean();
                ob_start();
                include "/home/poslavu/public_html/register/views/layouts/lavufit-postregister-email.txt.php";
                $message = ob_get_clean();
            }
            else if ( $mainvars['is_LavuSE'] ) {
                $sender = array('newaccount@lavu.com' => 'Lavu Inc.');
                $company_name = $mainvars['company_name'];

                ob_start();
                include 'views/emails/welcome-email-se.html.php';
                $html_message = ob_get_clean();
                ob_start();
                include 'views/emails/welcome-email-se.txt.php';
                $message = ob_get_clean();
            }
            else if ( !empty( $mainvars['activation_welcome'] ) ) {
                $sender = array('newaccount@lavu.com' => 'Lavu Inc.');
                $company_name = $mainvars['company_name'];

                ob_start();
                include 'views/emails/welcome-email.html.php';
                $html_message = ob_get_clean();
                ob_start();
                include 'views/emails/welcome-email.txt.php';
                $message = ob_get_clean();
            }

			send_multipart_email( $sender, $s_subject, $html_message, $message, $email_to );
			//send_email_with_images($email_to,$s_subject,$html_message,"welcome@poslavu.com");
            $pimps_n_gangsters = array( "corey@poslavu.com" );
            $pimps_n_gangsters[] = "andy@poslavu.com";
            $pimps_n_gangsters[] = "richard@poslavu.com";
            // $pimps_n_gangsters[] = "ben@poslavu.com";
            // $pimps_n_gangsters[] = "travis@poslavu.com";
            if (!DEV) send_multipart_email ( $sender, $s_subject, $html_message, $message, $pimps_n_gangsters );
            //send_email_with_images( implode( ',', $pimps_n_gangsters),$s_lavu_subject,$html_message,"welcome@poslavu.com");

		} else {
			mail($email_to,"Your new Lavu POS Account",$message,"From: support@poslavu.com");
			mail("sales@lavu.com","Trial signup - Lavu POS",$message,"From: support@poslavu.com");
		}
	}
	/**
	 * Description:- create db connection for new dataname while creating new account
	 * Params:- $db
	 * Return:- db connectiomn
	 */
	function connectNewDatanameDB($db)
	{
		$creds = array( "db_host" => my_poslavu_host(FALSE), "db_pass" => my_poslavu_password(), "db_user" => my_poslavu_username() );
		$conn = mysqli_connect($creds['db_host'], $creds['db_user'], $creds['db_pass'], $db);
		if (mysqli_connect_errno()){
			return NULL;
		}
		return $conn;
	}
	/**
	 * Description:- close db connection of new dataname while creating new account
	 * Params:- $conn
	 */
	function closeNewDatanameDB($conn)
	{
		$conn -> close();
	}

	function create_new_poslavu_account($company_name,$email="",$phone="",$firstname="",$lastname="",$address="",$city="",$state="",$zip="",$use_default_db="",$lang="",$account_type="Client",$set_com_product="", $package_info=array(),$countryCode="")
	{
		
		global $main_recordid;  // needed by /home/poslavu/public_html/admin/distro/beta/exec/new_quote.php
		$dataname = get_dataname_for_company($company_name);
		$username = get_username_for_company($dataname);
		$password = create_new_password();
		$dbpassword = create_new_db_password();
		$dbname = "poslavu_".$dataname."_db";
		$jkey = create_new_db_password(20);
		if($use_default_db=="")
		{
			$dbsource = "poslavu_DEFAULT_db";//"poslavu_DEMO_DEFAULT_db";
			$sourcedir = "DEFAULT";//"DEMO_DEFAULT";
		}
		else
		{
			$dbsource = "poslavu_" . $use_default_db . "_db";
			$sourcedir = $use_default_db;
		}

		$isLavuSE = (isset($package_info['package']) && $package_info['package'] == 'LavuSE');

		$check_for_db = mysqli_select_db($dbname);
		if (!$check_for_db)
		{
		    $doc_root = DOC_ROOT;
		    $admin_dir = ADMIN_DIR;
			require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
			require_once("/home/poslavu/public_html/admin/sa_cp/tools/es2_functions.php");

			$mainvars = array();
			$mainvars['company_name'] = $company_name;
			$mainvars['company_code'] = $dataname;
			$mainvars['data_name'] = $dataname;
			$mainvars['logo_img'] = "poslavu.png";
			$mainvars['licensed_for_locations'] = 1;
			$mainvars['dev'] = ($account_type=="Dev")?"2":"0";
			$mainvars['demo'] = ($account_type=="Demo")?"1":"0";
			$mainvars['test'] = ($account_type=="Test")?"1":"0";
			$mainvars['reseller'] = ($account_type=="Reseller")?"1":"0";
			$mainvars['email'] = $email;
			$mainvars['phone'] = $phone;
			$mainvars['dbpassword'] = $dbpassword;
			$mainvars['jkey'] = $jkey;
			$mainvars['integration_key2'] = integration_keystr2();
			$mainvars['modules'] = ($isLavuSE) ? 'extensions.ordering.+posse' : '';
			$mainvars['app_name'] = $package_info['package'];
		
			if(isset($countryCode) && $countryCode == "US"){
				$mainvars['is_new_control_panel_active'] = 1;
				$mainvars['modules'] .= ',extensions.+limited_cp_beta';
			}else{
				$mainvars['is_new_control_panel_active'] = 0;
			}
			

			$create_main_record = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`restaurants` (`company_name`, `company_code`, `data_name`, `logo_img`, `licensed_for_locations`, `dev`, `demo`, `test`, `reseller`, `created`, `last_activity`, `email`, `phone`, `data_access`,`jkey`,`modules`,`app_name`,`is_new_control_panel_active`) VALUES ('[company_name]', '[company_code]', '[data_name]', '[logo_img]', '[licensed_for_locations]', '[dev]', '[demo]', '[test]', '[reseller]', now(), now(), '[email]', '[phone]', AES_ENCRYPT('[dbpassword]','lavupass'),AES_ENCRYPT('[jkey]','[integration_key2]'), '[modules]','[app_name]','[is_new_control_panel_active]')",$mainvars);
			$main_recordid = mlavu_insert_id();
			$action_billing_log = 'Signed up for '.$package_info['package'];
			$a_insert_vars = array('datetime'=>date('Y-m-d H:i:s'), 'dataname'=>$dataname, 'restaurantid'=>$main_recordid, 'username'=>admin_info("username"), 'fullname'=>admin_info("fullname"), 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'action'=>$action_billing_log);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` (`datetime`, `dataname`, `restaurantid`, `username`, `fullname`, `ipaddress`, `action`) values ('[datetime]', '[dataname]','[restaurantid]','[username]','[fullname]','[ipaddress]','[action]')",$a_insert_vars);

			// LP-725 -- Make all Client account types have 14 day trials
			$current_ts = time();
			$current_dt = date('Y-m-d H:i:s', $current_ts);
			$trial_days = 14;
			$trial_end_ts = strtotime("{$current_dt} +{$trial_days} days");
			$trial_end = date('Y-m-d', $trial_end_ts);
			$trial_end_parts = explode('-', $trial_end);
			if (count($trial_end_parts) > 2) $trial_end_ts = mktime(0, 0, 0, $trial_end_parts[1], $trial_end_parts[2], $trial_end_parts[0]);
			$system_settings = array(
			    'dataname'     => $dataname,
			    'restaurantid' => $main_recordid,
				'current_dt'   => date('Y-m-d H:i:s', $current_ts),                 // value5
				'current_ts'   => $current_ts,                                      // value4
				'trial_status' => ($account_type == 'Client') ? '1' : '0',          // value3
				'trial_end'    => ($account_type == 'Client') ? $trial_end : '',    // value2
				'trial_end_ts' => ($account_type == 'Client') ? $trial_end_ts : '', // value, value6
				'package'      => ((isset($package_info['package']) && !empty($package_info['package'])) ? $package_info['package'] : 'Lavu'),
				'ipads'        => ((isset($package_info['ipads'])   && !empty($package_info['ipads']))   ? $package_info['ipads']   : '1'),
				'ipods'        => ((isset($package_info['ipods'])   && !empty($package_info['ipods']))   ? $package_info['ipods']   : '5'),
			);
			// CP3-244 - Prevent SE accounts from being able to log into the POS
			if ($isLavuSE) {
				$system_settings['ipads'] = '0';
				$system_settings['ipods'] = '0';
			}
			$create_payment_status = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`payment_status` (`restaurantid`, `dataname`, `trial_end`) VALUES ('[restaurantid]', '[dataname]', '[trial_end]')", $system_settings);
			$payment_status_id = mlavu_insert_id();

			$custvars = array();
			$custvars['username'] = $username;
			$custvars['dataname'] = $dataname;
			$custvars['restaurant'] = mlavu_insert_id();
			$custvars['email'] = $email;
			$create_customer_record = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`customer_accounts` (`username`, `dataname`, `restaurant`, `email`) VALUES ('[username]', '[dataname]', '[restaurant]', '[email]')",$custvars);

			$create_database = mlavu_query("CREATE DATABASE $dbname");

			// LP-5935 - Get list of db tables to create from the default db model
			$table_directives = array(
				'combo_items'             => 'fill',
				'config'                  => 'fill|`type` IN("location_config_setting" ,"inventory_setting")',
				'custom_field_types'      => 'fill',
				'custom_fields'           => 'fill',
				'custom_fields_metadata'  => 'fill',
				'discount_types'          => 'fill',
				'forced_modifiers'        => 'fill',
				'forced_modifier_groups'  => 'fill',
				'forced_modifier_lists'   => 'fill',
				'ingredients'             => 'fill',
				'inventory_categories'    => 'fill',
				'inventory_settings'      => 'fill',
				'inventory_unit'          => 'fill',
				'inventory_waste_reasons' => 'fill',
				'ingredient_categories'   => 'fill',
				'locations'               => 'fill',
				'menus'                   => 'fill',
				'menu_categories'         => 'fill',
				'menu_items'              => 'fill',
				'menu_groups'             => 'fill',
				'modifiers'               => 'fill',
				'modifier_categories'     => 'fill',
				'order_tags'              => 'fill',
				'payment_types'           => 'fill',
				'states'                  => 'fill',
				'standard_units_measure'  => 'fill',
				'storage_locations'       => 'fill',
				'tables'                  => 'fill',
				'unit_category'           => 'fill',
				'vendors'                 => 'fill',
				'phinxlog'                => 'fill',
			);
			$dbsource_tables = mlavu_query("SHOW TABLES FROM [1]", $dbsource);
		
			$newDB = connectNewDatanameDB($dbname); #db connection for new dataname
			$newDB->query("SET FOREIGN_KEY_CHECKS=0");
			while ($dbsource_table = mysqli_fetch_array($dbsource_tables))
			{
				$tablename = $dbsource_table[0];
				$directive = isset($table_directives[$tablename]) ? $table_directives[$tablename] : '';

				if ($directive == 'skip') continue;

				$createTableObj = mlavu_query("SHOW CREATE TABLE `$dbsource`.`$tablename`");	#get create table structure of source table
				$createResult = mysqli_fetch_assoc($createTableObj);	#get table create query 
				if($newDB != NULL){
					$tableStatus = 0;
					if(isset($createResult['Create Table'])){
						$tableStatus = $newDB->query($createResult['Create Table']);
					}elseif(isset($createResult['Create View'])){
						$dbuser = my_poslavu_username(); 
						$createViewSql = str_replace($dbsource,$dbname,$createResult['Create View']); #replace source dataname with new dataname
						$createViewSql = str_replace("DEFINER=`$dbuser`@`%`","",$createViewSql); #remove difiner to stop access denie 
						$tableStatus = $newDB->query($createViewSql);
					}
					if($tableStatus){
						#Insert data in new created table of new dataname
						if ($directive == "copy" || substr($directive, 0, 4) == "fill") {
							$source_filter = "";
							if (strpos($directive, "|")) {
								$directive_parts = explode("|", $directive);
								$source_filter = " WHERE ". $directive_parts[1];
							}
							$copy_result = $newDB->query("INSERT INTO `$dbname`.`$tablename` SELECT * FROM `$dbsource`.`$tablename`".$source_filter);
						}
					}
				}
			}
			$newDB->query("SET FOREIGN_KEY_CHECKS=1");

			#close db connection on new dataname
			if($newDB != NULL){
				closeNewDatanameDB($newDB);
			}

			if ($isLavuSE) {
				// TO DO : update config row with modules
				$config_rows = mlavu_query("SELECT `id`, `setting`, `value` FROM `{$dbname}`.`menu_categories` WHERE `type`='location_config_setting' AND `setting`='modules' AND `_deleted`=0");
				while ($config_row = mysqli_fetch_assoc($config_rows))
				{
					$config_row['value'] = empty($config_row['value']) ? $mainvars['modules'] : $config_row['value'] .',' .$mainvars['modules'];
					mlavu_query("UPDATE `$dbname`.`config WHERE `id`");
				}
				
				$selectModuleInfo = mlavu_query("SELECT value FROM `$dbname`.`config` WHERE `setting` = 'modules' AND `type`='location_config_setting' AND '_deleted'=0");
				$setModule = '';
				if(mysqli_num_rows($selectModuleInfo)>0) {
					$moduleInfo = mysqli_fetch_assoc($selectModuleInfo);
					if(!empty($moduleInfo['value']) && $moduleInfo['value']!='') {
						$setModule = $moduleInfo['value'].',extensions.ordering.+posse';
					}
					else {
						$setModule = 'extensions.ordering.+posse';
					}
					mlavu_query("UPDATE `$dbname`.`config` SET `location`=1, `value` = '$setModule' WHERE `setting` = 'modules' AND `type`='location_config_setting' AND '_deleted'=0");
				}
				else {
					$setModule = 'extensions.ordering.+posse,extensions.+limited_cp_beta';
					$querySet = "INSERT INTO `$dbname`.`config` set ";
					$querySet .= " `location`=1, `type`= 'location_config_setting', `setting`='modules', `value`= '$setModule'";
					mlavu_query($querySet);
				}
			}

			//----- reset printer settings
			$reset_category_printers = mlavu_query("UPDATE `$dbname`.`menu_categories` SET `printer` = ''");
			$reset_item_printers = mlavu_query("UPDATE `$dbname`.`menu_items` SET `printer` = '0'");

			//----------------- create db user for company
			require_once( "/home/poslavu/public_html/inc/usermgr.php" );
			$set_db_username = "usr_pl_" . $main_recordid;
			$user_db = "poslavu_" . trim($dataname) . "_db";
			if(db_user_exists($set_db_username))
			{
                $gsuccess = grant_db_user($set_db_username,$user_db);
			}
			else
			{
				$success = create_db_user($set_db_username,$dbpassword);
				if(db_user_exists($set_db_username)) {
					$gsuccess = grant_db_user($set_db_username,$user_db);
				}
			}

			mlavu_select_db($dbname);
			$uservars = array();
			$uservars['company_code'] = $dataname;
			$uservars['username'] = $username;
			$uservars['password'] = $password;
			$uservars['email'] = $email;
			$uservars['firstname'] = $firstname;
			$uservars['lastname'] = $lastname;
			$uservars['access_level'] = 3;
			$uservars['PIN'] = "1234";
			$uservars['quick_serve'] = "0";
			$create_user_account = mlavu_query("INSERT INTO `users` (`company_code`,`username`,`f_name`,`l_name`,`password`,`email`,`access_level`,`PIN`,`quick_serve`,`active`) VALUES ('[company_code]', '[username]', '[firstname]', '[lastname]', PASSWORD('[password]'), '[email]', '[access_level]', '[PIN]', '[quick_serve]','1')",$uservars);

			$locvars = array();
			$locvars['title'] = $company_name;
			$locvars['address'] = $address;
			$locvars['city'] = $city;
			$locvars['state'] = $state;
			$locvars['zip'] = $zip;
			$locvars['manager'] = trim($firstname . " " . $lastname);
			$locvars['phone'] = $phone;
			$locvars['website'] = "";

			switch($lang){ //default backend language

				case 'cn':
					$locvars['use_language_pack'] = 3;
					break;
				case 'es':
					$locvars['use_language_pack'] = 2;
					break;
				default:
					$locvars['use_language_pack'] = 1;
			}
			//generate api-key and api-token for new account
			$create_token = e2_api_create_new_password(20);
			$create_key = e2_api_create_new_password(20);
			
			mlavu_query("update `$dbname`.`locations` set `api_key`=AES_ENCRYPT('$create_key','lavuapikey'), `api_token`=AES_ENCRYPT('$create_token','lavutokenapikey'), `title`='[title]', `address`='[address]', `city`='[city]', `state`='[state]', `zip`='[zip]', `manager`='[manager]', `phone`='[phone]', `website`='[website]', `use_language_pack`='[use_language_pack]'",$locvars);

			// Insert package level, terminal limit, and lockout timer rows into POS db config table
			$inserted_lockout_timer = mlavu_query("INSERT INTO `$dbname`.`config` (`location`, `type`, `setting`, `value`, `value2`, `value3`, `value4`, `value5`, `value6`) VALUES ('1', 'system_setting', 'payment_status', '[trial_end_ts]', '[trial_end]', '[trial_status]', '[current_ts]', '[current_dt]', '[trial_end_ts]')", $system_settings);
			$inserted_package_info = mlavu_query("INSERT INTO `$dbname`.`config` (`location`, `type`, `setting`, `value`, `value2`, `value3`) VALUES ('1', 'system_setting', 'package_info', '[package]', '[ipods]', '[ipads]')", $system_settings);

			// send email notification new account
			$mainvars['is_fitness'] = $use_default_db=="lavu_fit_demo";
			$mainvars['activation_welcome'] = (isset($_POST['vf_signup']) && $_POST['vf_signup'] == '2');
			$mainvars['company_name'] = $company_name;
			$mainvars['is_LavuSE'] = $isLavuSE;

			send_new_account_email($email,$username,$password,$lang,$mainvars);

			if ($account_type=="Client") {
				// notify sales team of new account creation
				$account_info = "";
				$account_info .= "Account Info:\n";
				$account_info .= " Full Name: $firstname $lastname\n";
				$account_info .= " Phone: $phone\n";
				$account_info .= " Email: $email\n";
				$account_info .= " Data Name: $dataname\n\n";
				$account_info .= "Please follow up with this new customer within the next 14 days!";

				mail("dev@lavu.com, billing@lavu.com","New POSLavu Account Created",$account_info,"From: support@lavu.com");
			}
			$admin_path = ADMIN_PATH;
			$new_path = "{$admin_path}/images/".$dataname;
			$source_path = "{$admin_path}/images/".$sourcedir;

			function copy_files($from,$to)
			{
				$fp = opendir($from);
				while($fread = readdir($fp))
				{
					if($fread!="."&&$fread!="..")
					{
						if(is_dir($from . "/" . $fread))
						{
						}
						else
						{
							copy($from . "/" . $fread,$to . "/" . $fread);
						}
					}
				}
				closedir($fp);
			}

			mkdir($new_path);
			chmod($new_path, 0775);
			mkdir($new_path."/categories");
			chmod($new_path."/categories", 0775);
			copy_files($source_path."/categories",$new_path."/categories");
			mkdir($new_path."/categories/full");
			chmod($new_path."/categories/full", 0775);
			copy_files($source_path."/categories/full",$new_path."/categories/full");
			mkdir($new_path."/categories/main");
			chmod($new_path."/categories/main", 0775);
			copy_files($source_path."/categories/main",$new_path."/categories/main");

			mkdir($new_path."/items");
			chmod($new_path."/items", 0775);
			copy_files($source_path."/items",$new_path."/items");
			mkdir($new_path."/items/full");
			chmod($new_path."/items/full", 0775);
			copy_files($source_path."/items/full",$new_path."/items/full");
			mkdir($new_path."/items/main");
			chmod($new_path."/items/main", 0775);
			copy_files($source_path."/items/main",$new_path."/items/main");

			if (!is_dir("/home/poslavu/logs/company/".$dataname)) {
				mkdir("/home/poslavu/logs/company/".$dataname,0755);
			}

			copy("$source_path/poslavu.png", "$new_path/poslavu.png");
			copy("$source_path/poslavu_logo_bw.png", "$new_path/poslavu_logo_bw.png");
			curlToSyncDB($dataname);

			if($use_default_db=="lavu_fit_demo")
			{
				$set_com_product = "fitness";
			}
			else if($use_default_db=="ersdemo")
			{
				$set_com_product = "lasertag";
			}
			else if($use_default_db=="fecdemo")
			{
				$set_com_product = "fec";
			}

			//$com_product = "";
			//if ($_SERVER['REMOTE_ADDR'] == '74.95.18.90') {
			//	switch ($use_default_db) {
			//		case "default_pizza_" : $com_product = "pizza"; break;
			//		//case "ersdemo" : $com_product = "lasertag"; break;
			//		//case "p2r_dallas" : $com_product = "lavukart"; break;
			//		//default : $com_product = "customer management"; break;
			//	}

				//error_log("use_default_db: ".$use_default_db." - com_product: ".$com_product);
			if( !empty( $set_com_product ) )
			{
				$com_product = $set_com_product;
				if (!empty($com_product)) {
					ob_start();
					require_once("/home/poslavu/public_html/admin/sa_cp/tools/convert_product.php");
					$rest_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname);
					if ($rest_query !== FALSE && mysqli_num_rows($rest_query)) {
						$rest_read = mysqli_fetch_assoc($rest_query);
						//error_log("update_account_product: ".$rest_read['id']." , ".$com_product);
						update_account_product($rest_read['id'], $com_product);
						error_log("update_account_product: ".$main_recordid." , ".$com_product);
						//update_account_product($main_recordid, $com_product);
					}

					$echoed = ob_get_contents();
					error_log($echoed);

					ob_end_clean();
				}
			}

			return array(true, $username, $password, $dataname, $main_recordid, $trial_end);
		}
		else return array(false);
	}

	// returns the response if there is one, or FALSE on failure (eg dataname isn't recognized)
	function curlToSyncDB($s_dataname) {

		$restaurant_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[dataname]'",
			array('dataname'=>$s_dataname));
		if ($restaurant_query === FALSE or mysqli_num_rows($restaurant_query) == 0)
			return FALSE;
		$restaurant_read = mysqli_fetch_assoc($restaurant_query);
		$rowid = $restaurant_read['id'];

		$vars = 'rowid='.$rowid;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, LAVUADMIN . "/manage/misc/MainFunctions/syncDB.php");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close ($ch);

		return $response;
	}
?>
