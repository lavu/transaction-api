<?php
global $is_lavu88_3mo8;
$username = empty( $username )? "username" : $username;
$password = empty( $password )? "password" : $password;
$color = DEV?  "blue" :"#AECD37";
if ( !defined('LAVU_TRIAL_DAYS') ) {
    define('LAVU_TRIAL_DAYS', 14 );
}
?>
<!doctype html>
<html>

<head></head>
<!-- from : welcome@lavuinc.com -->

<body style="background-color:#f2f2f2;margin:0;padding:0;font-family:Verdana, Geneva, Arial, Helvetica, sans-serif;font-weight:400;padding-bottom:1em;">
    <p style="width:640px;margin:6px auto;text-align:right;color:#ACACAC;font-size:0.75em;">
        855-767-5288 &nbsp;&nbsp;| &nbsp;&nbsp;
        <a href="mailto:sales@lavuinc.com" style="color:#ACACAC;text-decoration:none;">sales
            <img src="http://www.lavu.com/assets/images/i_email_sm_grey.png" alt="" style="margin:0 3px;">
        </a>&nbsp;
        <a href="mailto:pos.support@lavuinc.com" style="color:#ACACAC;text-decoration:none;">support
            <img src="http://www.lavu.com/assets/images/i_email_sm_grey.png" alt="" style="margin:0 3px;">
        </a>&nbsp;
    </p>
    <div id="wrapper" style="max-width:640px;margin:0 auto;background-color:white;text-align:center;border:1px ridge #ccc;overflow:auto;padding:0;">
        <img src="http://www.lavu.com/assets/images/email_header.jpg" width="640" alt="iPad point of sale for restaurants | Lavu">
        <img class="divider" src="http://www.lavu.com/assets/images/divline_fadedown.png" width="480" alt="" style="margin:0 auto 10px auto;">
        <div style="padding:0 60px 1em 60px;">
            <h1 style="font-weight:normal;font-size:3em;margin-top:0;margin-bottom:0.5em;">Welcome to Lavu.</h1>
            <p style="font-size:0.85em;margin-bottom:3px;">Your new account is ready</p>
            <hr style="width:275px;margin:0 auto;border:none;border-bottom:1px solid #d6d6d6;">
            <h2 style="font-weight:100;margin:5px;">
                <span style="text-transform:uppercase;color:#bdbdbd;">User:</span>
                <span style="color:<?php echo $color; ?>;">
                    <?= $username ?>
                </span>
            </h2>
            <h2 style="font-weight:100;margin:5px;">
                <span style="text-transform:uppercase;color:#bdbdbd;">Pass:</span>
                <span style="color:<?php echo $color; ?>;">
                    <?= $password ?>
                </span>
            </h2>
            <hr style="width:300px;margin:0 auto 4em auto;border:none;border-bottom:1px solid #d6d6d6;">
            <img src="http://www.lavu.com/assets/images/setup_ipad_pin.jpg" alt="Lavu Default PIN:1234. Use this pin until you have setup your custom PIN">
            <h2 style="font-weight:normal;font-size:2em;margin:5px 0 0 0;">Try Lavu on your iPad.</h2>
            <p style="margin:5px;font-weight:normal;">
                You can now access your Lavu Front of House
            </p>
            <p style="margin:5px;font-size:0.85em;">
                Use Lavu Front of House where you interact with your customers. Enter your pin and you will see the Lavu FoH menu and ordering screens, as well as Table layout, Tab layout and more!
            </p>
            <!--  #back-office-cp -->
            <img class="divider" src="http://www.lavu.com/assets/images/divline_fadedown.png" width="480" alt="" style="margin:40px auto;">
            <img src="http://www.lavu.com/assets/images/laptop_graph_color.png" alt="Lavu Back Office CP">
            <h2 style="font-weight:normal;font-size:2em;margin:10px 0 0 0;">Setup your business now.</h2>
            <p style="margin:5px;font-weight:normal;">You can try out the Lavu back office CP</p>
            <p style="margin:5px;font-size:0.85em;">The Lavu CP is your Control Panel. This is where you login to setup your menu, pricing and account settings. After you start using Lavu for your business, the CP is where you will go to view your reports.</p>
            <p style="margin:3px auto 20px auto;font-weight:normal;">
                <a href="http://admin.poslavu.com/cp/?mode=assistance_guide_start" style="color:<?php echo $color; ?>;text-decoration:none;">&#9654;
                    <span style="text-decoration:underline">Login to your New Lavu Account here</span>
                </a>
            </p>
            <!-- #support  -->

            <img src="http://www.lavu.com/assets/images/divline_fadedown.png" width="480" alt="" style="margin:40px auto;">
            <img src="http://www.lavu.com/assets/images/i_logo_lavu_specialist.png" alt="Certified Lavu Distributor">
            <h2 style="font-weight:normal;font-size:2em;margin:5px 0 0 0;">How can we assist you?</h2>
            <p style="margin:5px;font-size:0.85em;">
                You will soon be contacted by a Lavu Specialist from your area.
                <br>Lavu Specialists are trained to assist with onsite installations, menu programming, training and support
            </p>
            <p style="font-size:0.85em;">If you would like to get more information about how Lavu iPad POS works, here are a few resources to help you get set up:</p>

            <div style="text-align:left;margin-top:3em;">
                <h3 style="text-transform:uppercase;margin-bottom:0;font-weight:normal;color:#ccc;">Lavu online help center</h3>
                <p style="margin-top:0;font-size:0.85em">
                    You can access The Lavu Help Center at:
                    <br>
                    <a href="http://help.lavu.com" style="color:<?php echo $color; ?>;text-decoration:none;">help.lavu.com</a>
                </p>

                <h3 style="text-transform:uppercase;margin-bottom:0;font-weight:normal;color:#ccc;">Lavu user forums</h3>
                <p style="margin-top:0;font-size:0.85em">
                    Our online forums have been very helpful to many of our customers.
                    <br>
                    <a href="http://www.lavucommunity.com" style="color:<?php echo $color; ?>;text-decoration:none;">www.lavucommunity.com</a>
                </p>

                <h3 style="text-transform:uppercase;margin-bottom:0;font-weight:normal;color:#ccc;">Speak with us now</h3>
                <p style="margin-top:0;font-size:0.85em">
                    Would you like to speak with someone right now?
                    <br>
                    <span style="color:<?php echo $color; ?>;text-decoration:none;font-size:1.5em;">855-767-5288</span>
                </p>
            </div>
            <!-- #free-trial -->
            <?php if ( empty( $_REQUEST['bluestar'] ) && !$is_lavu88_3mo8 ) : ?>
            <img class="divider" src="http://www.lavu.com/assets/images/divline_fadedown.png" width="480" alt="" style="margin:40px auto;">
            <img src="http://www.lavu.com/assets/images/i_tap_ipad.png" alt="Lavu iPad POS Free Trial">
            <h2 style="font-weight:normal;font-size:2em;margin:10px 0 0 0;">
                About the Free Trial
            </h2>
			    <p style="font-size:0.75em;margin:0 0 2em 0;color:red;">Accounts are automatically charged after the <?= LAVU_TRIAL_DAYS ?> day trial period. <br>
If you are not yet ready to purchase and move forward with Lavu, please cancel and disable your account <span style="text-decoration:underline;">before</span> your trial period has expired.</p>
<div style="text-align:justify">

<p style="font-size:0.75em;margin:0 0 2em 0;"><strong>If you want to cancel your account, here is how:</strong></p>
</div>	
        

	<ol style="font-size:0.75em;text-align:justify;">
		<li>
            Login the to the administrative control panel. <br />
            <a href="//admin.poslavu.com" style="text-decoration:none;color:<?php echo $color; ?>;">admin.poslavu.com</a>
        </li>
		<li>
            Click the &ldquo;My Accounts&rdquo; button on the top right of
            your screen.
        </li>
		<li>
            Scroll to the bottom of the &ldquo;My Account&rdquo; page and
            click the &ldquo;Cancel All Billing & Disable Account&rdquo;
            button.
        </li>
		<li>
            Please provide a reason for your cancellation. This is not
            required but we do appreciate your feedback. When complete
            click the &ldquo;Continue Cancellation and Disable your
            Account&rdquo; button.
        </li>
		<li>
            Your Account is now disabled. If you wish to reactivate your
            accounts, please contact Support or your Lavu Specialist.
        </li>
	</ol>				

	<div style="text-align:justify">
        <p style="font-size:0.75em;margin:0 0 2em 0;color:red;text-decoration:underline;">
            Failure to cancel your Free Trial before the <?= LAVU_TRIAL_DAYS ?> day
            expiration results in any applicable License or Monthly Fees being
            charged.<br />
            Once charged, License and Monthly Fees are non-refundable.
        </p>

        <p style="font-size:0.75em;margin:0 0 2em 0;">
            If you are not yet ready to purchase and move forward with Lavu,
            please cancel and disable your account before your trial period
            has expired:
        </p>
    </div>
		<?php endif; ?>
	<p style="font-size:0.75em;margin:0 0 2em 0;">
        Please call us at TLK-POS-LAVU (855-767-5288) or for International
        customers 480-788-LAVU for further information or questions.
    </p>
</div>
</body>

</html>