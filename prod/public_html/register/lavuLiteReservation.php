<?php
	
	//ini_set("display_errors",1);
	
	include_once("../admin/cp/resources/core_functions.php");
	
	$title= 'Reserve your Lavu Lite account today!';
	$numRes= mysqli_num_rows(mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reservations`"));
	$numReservations=500+$numRes;
	
		
?>

<html>
	
	<head>	
		<title><?php echo $title;?></title>
		    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
		    <script src="vf/scripts/jquery.tools.min.js" type="text/javascript" charset="utf-8"></script>
		    <script src="vf/scripts/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
		    <script src="vf/scripts/reservation.js" type="text/javascript" charset="utf-8"></script>
		    <link type="text/css" rel="stylesheet" href="./styles/reservationStyles/styles.css">
	</head>
	
	<body>
		
		<div id='header'>
			<div id='headerSuper'>
				<div id='headerText'>
					<b>Lavu Lite signups are temporarily paused while we improve our service. </b><br>
				</div>
				<div id='btnQuestionMark'>
					<img src='/vf/images/btn_questionmark.png' onclick='openNotice()'>
				</div>
			</div>
		</div>
		
		<div id='notice'>
			<div id='noticeBox'>
				<div id='closeBtn' onclick='closeNotice()'>X</div>
				
				Our decision to temporarily restrict Lavu Lite and offer an updated solution is based on a few factors:<br>
				<ol>
					<li>
						As you may have heard, our initial processing solution for Lavu Lite has discontinued their service.  
						We have already put in place a new, preferred processing solution with an equally low and more universal 
						flat rate to offer even more savings to our customers.
					</li>
					<li>
						We experienced a consistently large volume of signups throughout the initial launch.  
						We are re-structuring our model and operation to best handle the significant interest in Lavu Lite.
					</li>
					<li>
						We are improving functionality across the board to make the signup, setup, and operation of Lavu Lite even more seamless.  
					</li>
					<li>
						We appreciate your patience.  Please do fill out the form to make a reservation, 
						and you will be among the first to receive the new Lavu Lite - complete with a very low processing rate and single, low monthly fee.
					</li>
				</ol>
			</div>
		</div>
		<div id='bodyContent'>

			<div id='liteLogo'>
				<img src='/vf/images/logo_lavulite_app.jpg'/>
			</div>
			<div id='signupCounter'>
				<?php echo $numReservations; ?>
			</div>
			<div id='signupCounterText'>
			 	LavuLite Reservations!
			</div>
			
			
			<div style="position:relative;width:300px;margin:0 auto;top:200px;color:gray;text-align:center;font-size:large;line-height:120%;"><b>We have reached our limit for reservations.  Stay tuned for updates on Lavu Lite.  Please visit our current product page at </b><a href="http://www.poslavu.com" style="color:#aecd37;">www.poslavu.com</a></div>
			
		</div>
		
	</body>
	

</html>