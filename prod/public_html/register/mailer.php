<?php 

include_once "swift-mail/swift_required.php";
function send_multipart_email ( $sender, $subject, $html, $txt, $recipients ) {
    if ( !is_array( $recipients ) ) {
        $recipients = array( $recipients );
    }
	try {
	    $message = Swift_Message::newInstance()
	        ->setSubject( $subject )
	        ->setFrom( $sender )
	        ->setBody( $txt )
	        ->addPart( $html, 'text/html' );
        $transport = DEV?
            Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs') :
            Swift_MailTransport::newInstance();
	    $mailer = Swift_Mailer::newInstance($transport);
        $num_sent = 0 ;
        foreach ( $recipients as $address => $name ) {
            if ( strpos( $name, ',' ) ) {
                $message->setTo( explode(',', $name ) );
            } elseif ( is_int( $address ) ) {
                $message->setTo( $name );
            } else {
                $message->setTo( array( $address => $name ) );
            }
            $num_sent += $mailer->send( $message, $failed_ones );
        }
        return array( "sent" => $num_sent, "failed" => $failed_ones );
	} catch (Exception $e) {
        if (DEV) echo 'Caught exception: ',  $e->getMessage(), "\n";
        if ( is_array( $recipients ) ) {
            send_email_with_images( implode(',', $recipients ), $subject, $html, $sender );
        } else {
            send_email_with_images( $recipients, $subject, $html, $sender );
        }
    }   
}