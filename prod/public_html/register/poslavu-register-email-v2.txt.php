<?php
if ( empty( $username) && empty( $password ) ){
    header('Content-Type: application/json; charset=utf8' );
}
$username = empty( $username )? "username" : $username;
$password = empty( $password )? "password" : $password;
?>

Welcome to Lavu.

Your new account is ready
USER: <?= $username ?> 

PASS: <?= $password ?> 


Lavu Default PIN:1234.
Use this pin until you have setup your custom PIN

** Try Lavu on your iPad **
You can now access your Lavu Front of House
Use Lavu Front of House where you interact with your customers. Enter your pin and you will see the Lavu FoH menu and ordering screens, as well as Table layout, Tab layout and more!

** Lavu Back Office CP **
Setup your business now.
You can try out the Lavu back office CP
The Lavu CP is your Control Panel. This is where you login to setup your menu, pricing and account settings. After you start using Lavu for your business, the CP is where you will go to view your reports.

Go to:
http://admin.poslavu.com/cp/
And Login to your New Lavu Account.

** Certified Lavu Distributor ** 
How can we assist you?
You will soon be contacted by a Lavu Specialist from your area. 
Lavu Specialists are trained to assist with onsite installations, menu programming, training and support
If you would like to get more information about how Lavu iPad POS works, here are a few resources to help you get set up:

LAVU ONLINE HELP CENTER
You can access The Lavu Help Center at: 
help.lavu.com

LAVU USER FORUMS
Our online forums have been very helpful to many of our customers. 
www.lavucommunity.com

SPEAK WITH US NOW
Would you like to speak with someone right now? 
855-767-5288

** Lavu iPad POS Free Trial **
About the Free Trial:
    Accounts are automatically charged after the 14 day trial period. 
    If you are not yet ready to purchase and move forward with Lavu, please
    cancel and disable your account before your trial period has expired.

If you want to cancel your account, here is how:

1. Login the to the administrative control panel. (http://admin.poslavu.com)
2. Click the “My Accounts” button on the top right of your screen.
3. Scroll to the bottom of the “My Account” page and click the “Cancel All
   Billing & Disable Account” button.
4. Please provide a reason for your cancellation. This is not required but
   we do appreciate your feedback. When complete click the “Continue
   Cancellation and Disable your Account” button.
5. Your Account is now disabled. If you wish to reactivate your accounts,
   please contact Support or your Lavu Specialist.

If you like Lavu you can continue using the product uninterrupted and your
account will be billed as per the License Agreement.

If you choose not to continue with Lavu, you may cancel before the end of
your 14 day Free Trial and your account will not be billed.

Lavu © POSLAVU 2010-2014 | ALL RIGHTS RESERVED | IPAD™ IS A TRADEMARK OF APPLE INC.