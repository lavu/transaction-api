<?php
if (getenv('DEV') == '1') {
	@define('DEV', 1);
	ini_set("display_errors","1");
	error_reporting(error_reporting() ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
} else {
	@define('DEV', 0);
}

if (getenv('DEV') == '1') {
	@define('DEV', 1);
	ini_set("display_errors","1");
	error_reporting(error_reporting() ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
} else {
	@define('DEV', 0);
}

//Commenting out the DEV variable if statement as part of a fix to credit-card-batch processing issue with gateway_settings.php. 6/1/16
//if ( DEV ) {
//	require_once(dirname(__FILE__)."/../LavuBackend/sa_cp/billing/authnet_functions.php");  // Don't check this reference to the LavuBackend into git; this is just for local devservers!
//}
//else {
	require_once(dirname(__FILE__)."/../admin/sa_cp/billing/authnet_functions.php");
//}
