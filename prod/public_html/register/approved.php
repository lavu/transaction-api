<?php
if (getenv('DEV') == '1') {
       @define('DEV', 1);
       ini_set("display_errors","1");
       error_reporting(error_reporting() & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
} else {
       @define('DEV', 0);
}

session_start();
$hthost = $_SERVER['HTTP_HOST'];
define( 'HTHOST', $hthost );
$doc_root = $_SERVER['DOCUMENT_ROOT'];
define('DOC_ROOT', $doc_root );
$admin_dir = $_SERVER['HTTP_HOST'] == "lavuregister"? "LavuBackend" : "admin";
define( "ADMIN_DIR", $admin_dir );
$admin_path = "{$doc_root}/../{$admin_dir}";
define( "ADMIN_PATH", $admin_path );
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
$maindb = "poslavu_MAIN_db";

	$rowid = ($_REQUEST['id'] - 12500);
	
	if(isset($_POST['mode']) && $_POST['mode']=='update_password'){
		if(isset($_POST['p_word']) && !empty($_POST['p_word']) && isset($_POST['username']) && !empty($_POST['username'])){
			require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
			$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'", $_POST['username']);
			if( mysqli_num_rows($query)){
				$res = mysqli_fetch_assoc($query);
				$update_query= mlavu_query("UPDATE `poslavu_[1]_db`.`users` SET `password`=PASSWORD('[2]') WHERE `username`='[3]' LIMIT 1 ", $res['dataname'], $_POST['p_word'], $_POST['username']);
				if(mlavu_dberror()){
					echo '{"status":"error", "message":"'.mlavu_dberror().'"}';
					return;
				}else{
					mlavu_query("INSERT INTO `poslavu_[1]_db`.`config` (`setting`,`value`) VALUES ('main_account_password_updated','1')", $res['dataname']);
					echo '{"status":"success", "message":"password_updated"}';
					return;
				}
			}else{
				echo '{"status":"error", "message":"User Does Not exist"}';
				return;
			}
		}else{
			echo '{"status":"error", "message":"username or password not sent"}';
			return;
		}
		return;
	}

	
	
	$s_enforce_security = "and (`sessionid`='[sessionid]' or `ipaddress`='[ipaddress]')";
	$cred_query = mlavu_query("select `username`,`dataname`,`package`,AES_DECRYPT(`password`,'password') as `password`,`email`,`promo` from `poslavu_MAIN_db`.`signups` where `id`='[rowid]' {$s_enforce_security}",
		array('rowid'=>$rowid, 'sessionid'=>session_id(), 'ipaddress'=>$_SERVER['REMOTE_ADDR']));
	if(mysqli_num_rows($cred_query))
	{
		$cred_read = mysqli_fetch_assoc($cred_query);

		// send internal email for sign-ups that happen with a promo code
		if (!empty($cred_read['promo']) && !DEV) {
			mail('ahassley@poslavu.com,ag@lavu.com,tom@lavu.com', "Sign-up with promo code {$cred_read['promo']}", $cred_read['dataname'], 'From:promo@poslavu.com');
		}

		// submit where they heard about us from
		if (isset($_POST['action']) && $_POST['action'] == 'submit_source' && isset($_POST['source']) && $_POST['source'] !== "") {
			mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `source`='[source]' WHERE `id`='[id]'", array('source'=>$_POST['source'], 'id'=>$rowid));
			echo "success";
			return;
		}
		if (isset($_POST['action']) && $_POST['action'] == 'resend_email') {
			require_once(dirname(__FILE__).'/create_account.php');
			require_once(dirname(__FILE__).'/../admin/cp/resources/json.php');

			// don't send the email if the email is blank
			if (trim($cred_read['email']) == '') {
				echo LavuJson::json_encode(array(
					array(
						'action'=>'alert',
						'error'=>'true',
						'message'=>"The email address registered with this account is blank."
					)
				));
				return;
			}

			// send the email
			$lang = (isset($_REQUEST['loc']) ? $_REQUEST['loc'] : (isset($_REQUEST['locale']) ? $_REQUEST['locale'] : 'en'));
			send_new_account_email($cred_read['email'],$cred_read['username'],$cred_read['password'],$lang);
			echo LavuJson::json_encode(array(
				array(
					'action'=>'alert',
					'error'=>'false',
					'message'=>"Success! An email has been sent to your inbox. If the email isn't there yet please wait a few minutes and check again or check your spam folder."
				)
			));
			return;
		}
	} else {
		echo "<style>body { background-color:#eee; }</style>
			<div style='width: 500px; background-color: white; border: 2px solid #666; border-radius: 5px; margin: 100px auto; text-align: center; padding: 15px;'>
				<span style='font-weight:bold;'>Error: Invalid credentials<br /></span>
				<br />
				<span style='color:#666'>Did you get this message in error? Call support at 855-767-5288</span><br />
			</div>";
		return;
	}

	//include_once('translate.php');
	$steps_width = 970;
	$body_width = 990;
	$page_bg = "F4F4F3";
	$body_bg = "F4F4F3";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>POSLavu iPad Point of Sale  | Approved Signup </title>
	<link rel="stylesheet" href="styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<script type="text/javascript" src="/scripts/jquery-1.9.0.js"></script>

	<style type="text/css">
		html {
			background-color: #<?= $page_bg ?>;
		}

		.reveal {
			position: absolute;
			top: 0;
			left: 0;
		}
		.reveal .content {
			width: 100%;
			position: relative;
			background: white;
			border: 2px solid #91918d;
			display: none;
			padding: 15px;
			border-radius: 5px;
		}
		.reveal_arrow {
			position: absolute;
			right: 16px;
			bottom: -20px;
		}

		body {
			background-color: #<?= $page_bg ?>;
			line-height: normal;
			min-width: <?= $body_width ?>px;
			overflow-x: hidden;
		}

		#app_content_wrapper {
			width: <?= $body_width ?>px;
			display: table;
			margin: 0 auto;
			height: 100%;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			position: relative;
			top: -1px;
		}
		#app_content_top {
			width: <?= $body_width ?>px;
			height: 290px;
			margin: 0 auto;
			background-color: transparent;
			position: relative;
		}
		#app_content_bottom {
			width: auto;
			margin: 0 auto;
			background-color: #<?= $body_bg ?>;
			position: relative;
		}

		#source select {
			height: 28px;
		}

		.check_thin_green {
			width: 34px;
			height: 24px;
			background: url('/images/check_thin_green.png');
			background-repeat: no-repeat;
			display: inline-block;
		}
		.check_thin_gray {
			width: 34px;
			height: 24px;
			background: url('/images/check_thin_gray.png');
			background-repeat: no-repeat;
			margin: 0 auto;
		}

		.banner {
			font-size: 21px;
			font-family: Verdana, sans-serif;
			background-color: rgba(74,74,77,0.8);
			text-align: center;
			padding: 22px;
			position: relative;
		}
		.banner .green {
			color: #aecd37;
			display: inline-table;
			margin-top: -3px;
		}
		.banner .white {
			color: white;
			display: inline-table;
			margin-top: -3px;
		}

		.banner_background {
			width: 1px;
			height: 1px;
			margin: 0 auto;
		}
		.banner_background .image {
			position: relative;
			width: 1820px;
			height: 290px;
			left: -910px;
			background: url('/images/bg_signup_banner.jpg');
			background-repeat: no-repeat;
		}

		.banner_dropshadow {
			position: relative;
			float: left;
			width: 0;
			height: 0;
			top: 0;
			left: 0;
		}
		.banner_dropshadow .image {
			position: absolute;
			width: 990px;
			height: 24px;
			background: url('/images/fadeline_down_990x24_dark.png');
			background-repeat: no-repeat;
		}

		.lavuaccountinfo {
			float: left;
			background: url('/images/window_1.png');
			background-repeat: no-repeat;
			position: relative;
			top: 23px;
			width: 648px;
			height: 168px;
		}
		.lavuaccountinfo .spacer {
			display: inline-block;
			width: 200px;
		}
		.lavuaccountinfo .info {
			display: inline-block;
			position: relative;
			top: 41px;
			left: 24px;
		}
		.lavuaccountinfo .info .username_password {
			font-size: 30px;
			font-family: Verdana, sans-serif;
			color: #484848;
		}
		.lavuaccountinfo .info .username_password span {
			font-size: 20px;
			color: white;
		}

		.lavutomeetyou {
			float: right;
			position: relative;
			background-color: rgba(74,74,77,0.8);
			border: 2px solid #2e2e2e;
			width: 240px;
			height: 168px;
			text-align: center;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
			position: relative;
			top: 23px;
			right: 0px;
			border-radius: 5px;
		}
		#source {
			text-align: right;
			width: 150px;
			margin: 0 auto;
		}
		.lavutomeetyou .greeting {
			font-family: Verdana, sans-serif;
			font-size: 16px;
			color: #a3a3a8;
			text-align: right;
			margin: 8px 0 0 0;
		}
		.lavutomeetyou .input-wrapper {
			display: block;
			padding: 0;
			margin-top: 5px;
			text-align: inherit;
		}
		.lavutomeetyou .input-wrapper .input {
			margin: 0;
			width: 150px;
			box-sizing: border-box;
			-moz-box-sizing: border-box;
		}
		.lavutomeetyou .input-wrapper .input.submit {
			margin: 0;
			width: 50px;
			text-align: center;
			background-color: #aecd37;
			border: none;
			border-radius: 5px;
			color: white;
		}
		.lavutomeetyou .greeting_question {
			display: table;
			width: 100%;
			font-family: Verdana, sans-serif;
			font-size: 13px;
			color: #aecd37;
			text-align: center;
			position: absolute;
			bottom: 8px;
		}

		.login_dropshadow {
			position: relative;
			width: 1px;
			height: 1px;
			margin: 0 auto;
		}
		.login_dropshadow .image {
			position: relative;
			width: 990px;
			height: 24px;
			top: -1px;
			left: -495px;
			background: url('/images/fadeline_down_990x24.png');
			background-repeat: no-repeat;
		}

		.login_form_container {
			width: <?= $steps_width ?>px;
			margin: 51px auto;
		}
		.login_form_container td {
			width: <?= ($steps_width/3-5) ?>px;
			height: 224px;
			display: inline-table;
			margin: 0;
			text-align: center;
			padding-top: 30px;
			position: relative;
		}
		.login_form_container td .background {
			width: 250px;
			height: 250px;
			position: absolute;
			left: <?= (($steps_width/3-5) - 250)/2 ?>px;
			top: <?= (224 - (250+4))/2 ?>px;
			background-color: white;
			border: none;
			border-radius: 10000px;
		}
		.login_form_container td p {
			display:table;
			margin:0 auto;
			text-align:center;
			position: relative;
			font-family: Verdana, sans-serif;
			font-size: 13px;
			color: #484848;
		}
		.login_form_container td p.header {
			margin: 0 auto 16px auto;
			font-size: 18px;
		}
		.login_form_container a {
			position: relative;
		}
		.login_form_container a:hover {
			text-decoration: none;
		}
		.login_form_container a.dark_link {
			color: #484848;
			text-decoration: underline;
			cursor: pointer;
		}
		.login_form_container a.dark_link:hover {
			color: #686868;
		}
		.login_form_container .check_thin_grey {
			position: relative;
			width: 34px;
			height: 24px;
			background: url('/images/check_thin_grey.png');
			background-repeat: no-repeat;
			margin: 2px auto;
		}
		.login_form_container a.green_link {
			color: #aecd37;
			text-decoration: underline;
			cursor: pointer;
		}
		.login_form_container a.green_link:hover {
			color: #bed057;
		}
		.login_form_container .check_thin_grey.transparent {
			opacity: 0.5;
		}

		.login_form_container .icon {
			width: 130px;
			height: 100px;
			margin: 0 auto;
			position: relative;
		}
		.login_form_container .icon .email_icon {
			background: url('/images/i_start_email.png');
			background-repeat: no-repeat;
			width: 112px;
			height: 88px;
			margin: 0 auto;
		}
		.login_form_container .icon .app_icon {
			background: url('/images/i_start_appicon.png');
			background-repeat: no-repeat;
			width: 112px;
			height: 88px;
			margin: 0 auto;
		}
		.login_form_container .icon .login_icon {
			background: url('/images/i_start_laptop_graph.png');
			background-repeat: no-repeat;
			width: 112px;
			height: 88px;
			margin: 0 auto;
		}

		.login_link a:hover {
			text-decoration: none;
		}
		.login_link .app-submit-button.new-colors {
			position: relative;
			width: 208px;
			height: 40px;
			margin: 12px auto 0 auto;
			background-color: #aecd37;
			border: 1px solid #aaa;
			border-radius: 5px;
			font-weight: normal;
			font-size: 18px;
			text-shadow: none;
			font-family: Verdana, sans-serif;
			color: white;
			padding: 4px 0 0 20px;
		}
		.app-submit-button.new-colors .check_thin_green {
			display: inline-block;
			position: relative;
			top: 5px;
			width: 34px;
			height: 24px;
			background: url('/images/check_thin_green_light.png');
			background-repeat: no-repeat;
		}

		.next_step {
		}
		.next_step .container {
			cursor: pointer;
			display: table;
			margin: 0 auto;
			padding: 10px 0;
			border: 2px solid white;
			border-left: none;
			border-right: none;
			position: relative;
		}
		.next_step .text {
			font-size: 20px;
			font-family: verdana, sans-serif;
			text-align: center;
			color: #91918d;
			display: inline-block;
			position: relative;
			top: -8px;
			padding: 0 4px;
		}
		.next_step .container:hover .text {
		}
		.next_step .text .bold {
			font-weight: bold;
		}
		.next_step .arrow_icon {
			display: inline-block;
			position: relative;
			width: 32px;
			height: 32px;
			background: url('/images/arrow_next_sm.png');
			background-repeat: no-repeat;
		}
		.next_step .container:hover .arrow_icon {
		}
		.next_step .question_icon {
			display: inline-block;
			position: relative;
			width: 30px;
			height: 30px;
			background: url('/images/i_question.png');
			background-repeat: no-repeat;
		}
		.next_step .container:hover .question_icon {
			background: url('/images/i_question_dark.png');
		}

		.page-bottom-text {
			font-size: 12px;
			font-family: verdana, sans-serif;
			background-color: #<?= $body_bg ?>;
			text-align: center;
			padding: 40px 0 4px 0;
			color: #91918d;
		}
		.page-bottom-text .text-container {
			display: table;
			margin: 0 auto;
		}
		.page-bottom-text .text-container span {
			margin-left: 5px;
		}
		.page-bottom-text .light {
		}
		.page-bottom-text .dark {
		}
		.update_pass_btn{
			background-color:#aecd37;
			
		}
		.update_password_box_cont{
			width:100%;
			height:100%;
			position:fixed;
			top:0px;
			left:0px;
			background-color:rgba(0,0,0,.6);
			z-index:100;
			display:none;	
		}.update_password_box{
			position:relative;
			width:400px;
			height:150px;
			margin:0 auto;
			border:1px solid white;
			border-radius:4px;
			top:200px;
			padding:10px;
			background-color:white;
		}
		
	</style>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		var fb_param = {};
		fb_param.pixel_id = '6009594698894';
		fb_param.value = '0.00';
		fb_param.currency = 'USD';
		(function(){
		  var fpw = document.createElement('script');
		  fpw.async = true;
		  fpw.src = '//connect.facebook.net/en_US/fp.js';
		  var ref = document.getElementsByTagName('script')[0];
		  ref.parentNode.insertBefore(fpw, ref);
		})();

		(function() {
			var time = 200;
			var submitted = false;

			window.source_modified = function(select) {
				var jselect = $(select);
				var jsource_other = jselect.parent().parent().parent().find('input[name=source_other]');
				var jsubmit = jselect.parent().parent().parent().find('input[type=submit]');
				var value = jselect.val();
				if (value == 'other') {
					jsource_other.stop(true,true);
					jsource_other.show(time);
				} else {
					jsource_other.stop(true,true);
					jsource_other.hide(time);
				}
				if (value == "") {
					jsubmit.stop(true,true);
					jsubmit.hide(time);
				} else {
					jsubmit.stop(true,true);
					jsubmit.show(time);
				}
			};

			window.submit_source = function(submit) {
				if (submitted)
					return;
				submitted = true;

				var jsubmit = $(submit);
				var jsource = $("#source");
				var jselect = jsource.parent().parent().find('select[name=source]');
				var jsource_other = jsource.parent().parent().find('input[name=source_other]');
				var source = "";
				if (jselect.val() == "other") {
					source = jsource_other.val();
				} else {
					source = jselect.val();
				}
				if (source == "")
					return;
				$.ajax({
					url: "./approved<?= (strpos(__FILE__, '_dev') !== FALSE ? '_dev' : '') ?>.php",
					async: true,
					cache: false,
					type: "POST",
					data: { 'action': 'submit_source', 'source': source, 'id': <?= $_REQUEST['id'] ?> }
				});
				jsource.append("<p class='thanks' style='display:none; margin-left:28px; color:#a3a3a8'>Thank you for your feedback!</p>");

				$.each(jsource.children(), function(k,v){
					if ($(v).hasClass('thanks')) {
						$(v).show(time);
					} else {
						$(v).stop(true,true);
						$(v).hide(time);
					}
				});
			};

			window.submit_source_key = function(submit, event) {
				if (event.which == 13)
					submit_source(submit);
			};

			window.resend_email = function() {
				$.ajax({
					url: "./approved<?= (strpos(__FILE__, '_dev') !== FALSE ? '_dev' : '') ?>.php",
					async: false,
					cache: false,
					type: "POST",
					data: { action: 'resend_email', id: <?= $_REQUEST['id'] ?> },
					success: function(m) {
						var messages = JSON.parse(m);
						console.log(messages);
						$.each(messages, function(k,message){
							if (message.action == 'alert') {
								var color = (message.error == 'true') ? 'red' : 'black';
								while($("#popup").length > 0)
									$("#popup").remove();
								var jtd = $(".app-submit-button.new-colors.gray").parent().parent().parent();
								jtd.append("<div id='popup' style='position:fixed; top:"+(jtd.offset().top-120)+"px; left:"+(jtd.offset().left-65)+"px; background-color:white; border:1px solid black; color:"+color+"; width:400px; min-height:50px; text-align:center; padding:9px;'>"+message.message+"<div style='margin:10px auto 0 auto; border:1px solid gray; padding:5px; width:50px; text-align:center; cursor:default;' onclick='$(this).parent().remove();' onmouseover='$(this).css({\"background-color\":\"lightgray\"});' onmouseout='$(this).css({\"background-color\":\"white\"});'>Close</div></div>");
							}
						});
					}
				});
			};

			window.show_reveal_arrow = function() {
				var show_reveal_arrow_for_element = function(k, element) {
					var jelement = $(element);
					if (jelement.css("display") == "none") {
						return;
					}
					var jarrow = jelement.children(".reveal_arrow");
					if (jarrow.length > 0) {
						return;
					}
					jarrow = $('<svg height="20" width="25" class="reveal_arrow"><polygon points="1,-1 24,-1 24,19" style="fill:white;stroke:#91918d;stroke-width:2" /></svg>');
					jelement.append(jarrow);
				};
				var jelements = $('.reveal').children(".content");
				$.each(jelements, show_reveal_arrow_for_element);
			};

			window.reveal_content = function(element) {
				var jelement = $(element);
				var jcontent = jelement.children(".reveal").children(".content");

				jcontent.stop(true,true);

				if (jcontent.css("display") !== "none") {
					var jarrow = jcontent.children("reveal_arrow");
					if (jarrow.length > 0)
						jarrow.remove();
					jcontent.hide();
					return;
				}

				jcontent.show();
				jcontent.css({ display:"table" });
				var height = parseInt(jcontent.height());
				var actual_height = height;
				height += parseInt(jcontent.css("padding-top")) + parseInt(jcontent.css("padding-bottom"));
				height += parseInt(jcontent.css("margin-top")) + parseInt(jcontent.css("margin-bottom"));
				var width = parseInt(jelement.width());
				width -= parseInt(jcontent.css("padding-left")) + parseInt(jcontent.css("padding-right"));
				jcontent.hide();

				jcontent.css({
					top: "0px",
					bottom: "0px",
					left: (width + "px"),
					right: "",
					width: "0px",
					height: "0px"
				});
				jcontent.show();
				jcontent.animate({
					width: (width + "px"),
					height: (actual_height + "px"),
					top: (-(height+30) + "px"),
					left: "0px"
				}, 200, show_reveal_arrow);

				jcontent.click(function() {
					if (event.stopPropagation)
						event.stopPropagation();
					else
						event.cancelBubble = true;
					return false;
				});
			};
		})();
		function open_password_editor(){
			var cont = document.getElementsByClassName("update_password_box_cont")[0];
			if($(cont).css("display")=='none'){
				$(cont).css("display","block");	
			}
			else{
				$(cont).css("display","none");	
			}
		}
		function update_pass(evt){
			var inps = evt.target.parentNode.getElementsByTagName("input"); 
			var p1 = inps[0].value;
			var p2 = inps[1].value;
			if( p1 !='' && p2!='' && p1 == p2){
				if(document.getElementsByClassName("error_div").length){
					var node =document.getElementsByClassName("error_div")[0];
					node.parentNode.removeChild(node);
				}
				var res = perform_ajax("/approved.php","mode=update_password&p_word="+p1+"&username=<?php echo $cred_read['username'];?>", false, "POST");
				if( res.status = 'success'){
					document.getElementsByClassName("signup_pass_hidden")[0].value = p1;
					document.getElementsByClassName("login_link")[0].click();
					//alert(res.message);
				}else{
					alert(res.message);
				}
				
			}else{
				if(!document.getElementsByClassName("error_div").length){
					var error_div = document.createElement("div");
						error_div.className ='error_div';
						error_div.style.color='red';
						error_div.innerHTML = 'Passwords do not match.';
					evt.target.parentNode.appendChild(error_div);
				}
			}
		}
		function perform_ajax(URL, urlString,asyncSetting,type ){
			var returnVal='';
			$.ajax({
				url: URL,
				type: type,
				data:urlString,
				async:asyncSetting,
				success: function (string){
					try{
						returnVal = JSON.parse(string);
					}catch(err){
						console.log(err);
						returnVal = string;
					}
				}
			});
			return returnVal;
		}

		function add_evt_listener(){
			var btn = document.getElementsByClassName("update_pass_btn")[0];
				btn.addEventListener("click",update_pass);
		}
		
		window.onload= add_evt_listener;
	</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-30186945-2', 'auto',{'allowLinker':true});
			ga('create', 'UA-30186945-1', 'auto', {'name': 'originalPOSlavu'});
			ga('originalPOSlavu.send', 'pageview');
			ga('send', 'pageview');
	</script>

</head>

<body>

	<div class="banner_background">
		<div class="image"></div>
	</div>

	<div id="app_content_wrapper">

		<div id="app_content_top">

			<div class="banner">
				<div class="check_thin_green"></div>
				<span class="green">Welcome to Lavu!</span>
				<span class="white">Your 14 Day Free Trial has begun!</span>
			</div>

			<div class="banner_dropshadow">
				<div class="image"></div>
			</div>

			<div class="lavuaccountinfo">
				<div class="spacer"></div>
				<div class="info">
					<div class="username_password"><span>USER NAME:</span> <?= $cred_read['username'] ?></div>
					<div class="username_password"><span>PASSWORD:</span><span> <input style='font-size:13pt;cursor:pointer' type='button' value ='Create Your Password' name ='update_pass_btn' onclick ='open_password_editor()' /></span></div>
					
				</div>
			</div>
			<div class ='update_password_box_cont'>
				<div class='update_password_box'>
				
					<div style='color:black;font-family:sans-serif; text-align:center; font-size:15pt; color:gray'>Update Your Password</div><br><br>
					<!--<span style='color:black; padding:5px; float:right; border:1px solid black; cursor:pointer; border-radius:7px; ' onclick = 'open_password_editor()'>X</span><br>-->
					<div class='co' style='margin:0 auto; width:200px'>
						<input type='password' class='password1' placeholder="Password" style='width:100%'/><br>
						<input type='password' class ='password2' placeholder='Re-enter Password' style='width:100%'/><br>
						<input type='button' class ='update_pass_btn' value='Update and Login' style='width:214px'/>
					</div>
				</div>
			</div>
			<div class="lavutomeetyou">
				<div id="source">
					<div class="greeting">Nice to meet you.</div>
					<div class="input-wrapper"><select id="referral" name="source" onchange="source_modified(this);" class="input">
						<option value=""></option>
						<option value="referred">Another Lavu Customer</option>
						<option value="kitchen_nightmares">Kitchen Nightmares</option>
						<option value="distro">Certified Distributor</option>
						<option value="google">Google</option>
						<option value="facebook">Facebook</option>
						<option value="expo">An Expo</option>
                        <option value="staples">Staples</option>
						<option value="other">Other</option>
					</select></div>
					<div class="input-wrapper"><input type="text" name="source_other" style="display:none;" class="input" /></div>
					<div class="input-wrapper"><input type="submit" value="Send" style="display:none;" class="input submit" onclick="submit_source(this);" onkeydown="submit_source_key(this, event);" /></div>
				</div>
				<div class="greeting_question">Where did you hear about Lavu?</div>
			</div>

		</div>
		<div id="app_content_bottom">

			<div class="login_dropshadow">
				<div class="image"></div>
			</div>

			<table class="login_form_container" style="font-size:14px;"><tr>
				<td>
					<div class="background"></div>
					<div class="icon">
						<div class="email_icon"></div>
					</div>
					<p class="header">Save Your Login Info</p>
					<p>An email has been sent to you</p>
					<a class="dark_link" href="#" onclick="resend_email();">Resend Login Info Email</a>
					<div class="check_thin_grey"></div>
				</td>
				<td>
					<div class="background"></div>
					<div class="icon">
						<div class="app_icon"></div>
					</div>
					<p class="header">Download Lavu App</p>
					<p>Try the Front of House on iPad</p>
					<a class="green_link" href="http://itunes.apple.com/us/app/poslavu-client/id391800399?mt=8" target="_blank">Go to the Apple App Store</a>
					<div class="check_thin_grey transparent"></div>
				</td>
				<td>
					<div class="background"></div>
					<div class="icon">
						<div class="login_icon"></div>
					</div>
					<p class="header">User Your Account</p>
					<p>Login for the Quick Start Guide</p>
					<form action = "http://admin.poslavu.com/cp/?mode=guide_start" method ='post' name = 'login_form'  > <!--target="_blank" -->
						<input type='hidden' value ='<?= $cred_read['username'] ?>' name ='new_signup_username' />  
						<input type='hidden' value ='<?= $cred_read['password'] ?>' class= 'signup_pass_hidden' name ='new_signup_password' />  
						
						<a class="login_link" onclick = 'document.querySelector("[name=login_form]").submit();'>
							<div class="app-submit-button new-colors" style='cursor:pointer'>
								<div class="check_thin_green"></div>
									LOGIN TO LAVU
							</div>
						</a>
					</form>
					<!--<a class="login_link" href="http://admin.poslavu.com/cp/?mode=guide_start" target="_blank">
						<div class="app-submit-button new-colors"><div class="check_thin_green"></div>LOGIN TO LAVU</div>
					</a>-->
				</td>

			</tr></table>

			<div class="next_step">
				<div class="container" onclick="reveal_content(this);">
					<div class="arrow_icon"></div>
					<div class="text">
						Now you have a lavu account. <span class="bold">What should you do next?</span>
					</div>
					<div class="question_icon"></div>
					<div class="reveal">
						<div class="content">
							Would you like to speak with a Lavu Specialist now? Please call us at 855-767-5288.
						</div>
					</div>
				</div>
			</div>

		</div>

		<div style="height:60px; background-color:#<?= $body_bg ?>;">&nbsp;</div>

		<div class="page-bottom-text">
			<div class="text-container">
				<span class="light">You can bookmark this page for future reference.</span>
				<span class="dark">Questions about your Lavu account? 855-767-5288</span>
			</div>
		</div>
	</div>
</body>
</html>