/**
 * Created by carlos on 8/29/14.
 */
var fb_param = {};
fb_param.pixel_id = '6009594698894';
fb_param.value = '0.00';
fb_param.currency = 'USD';
(function(){
    var fpw = document.createElement('script');
    fpw.async = true;
    fpw.src = '//connect.facebook.net/en_US/fp.js';
    var ref = document.getElementsByTagName('script')[0];
    ref.parentNode.insertBefore(fpw, ref);
})();


var time = 200;
var submitted = false;

function resend_email () {
    $.ajax({
        url: "/resend-email",
        cache: false,
        type: "POST",
        dataType : "json",
        data: {
            action: 'resend_email',
            rowid: document.getElementById("approved__login-form").getAttribute("data-rowid")
        },
        success: function(messages) {
            console.log(messages);
            $.each(messages, function(k,message){
                if (message.action == 'alert') {
                    var color = (message.error == 'true') ? 'red' : 'black';
                    while($("#popup").length > 0)
                    $("#popup").remove();
                    var jtd = $(".app-submit-button.new-colors.gray").parent().parent().parent();
                    jtd.append("<div id='popup' style='position:fixed; top:"+(jtd.offset().top-120)+"px; left:"+(jtd.offset().left-65)+"px; background-color:white; border:1px solid black; color:"+color+"; width:400px; min-height:50px; text-align:center; padding:9px;'>"+message.message+"<div style='margin:10px auto 0 auto; border:1px solid gray; padding:5px; width:50px; text-align:center; cursor:default;' onclick='$(this).parent().remove();' onmouseover='$(this).css({\"background-color\":\"lightgray\"});' onmouseout='$(this).css({\"background-color\":\"white\"});'>Close</div></div>");
                }
            });
        }
    });
}

function show_reveal_arrow() {
    var show_reveal_arrow_for_element = function(k, element) {
        var jelement = $(element);
        if (jelement.css("display") == "none") {
            return;
        }
        var jarrow = jelement.children(".reveal_arrow");
        if (jarrow.length > 0) {
            return;
        }
        jarrow = $('<svg height="20" width="25" class="reveal_arrow"><polygon points="1,-1 24,-1 24,19" style="fill:white;stroke:#91918d;stroke-width:2" /></svg>');
        jelement.append(jarrow);
    };
    var jelements = $('.reveal').children(".content");
    $.each(jelements, show_reveal_arrow_for_element);
}

function reveal_content(element) {
    var jelement = $(element);
    var jcontent = jelement.children(".reveal").children(".content");

    jcontent.stop(true,true);

    if (jcontent.css("display") !== "none") {
        var jarrow = jcontent.children("reveal_arrow");
        if (jarrow.length > 0)
            jarrow.remove();
        jcontent.hide();
        return;
    }

    jcontent.show();
    jcontent.css({ display:"table" });
    var height = parseInt(jcontent.height());
    var actual_height = height;
    height += parseInt(jcontent.css("padding-top")) + parseInt(jcontent.css("padding-bottom"));
    height += parseInt(jcontent.css("margin-top")) + parseInt(jcontent.css("margin-bottom"));
    var width = parseInt(jelement.width());
    width -= parseInt(jcontent.css("padding-left")) + parseInt(jcontent.css("padding-right"));
    jcontent.hide();

    jcontent.css({
        top: "0px",
        bottom: "0px",
        left: (width + "px"),
        right: "",
        width: "0px",
        height: "0px"
    });
    jcontent.show();
    jcontent.animate({
        width: (width + "px"),
        height: (actual_height + "px"),
        top: (-(height+30) + "px"),
        left: "0px"
    }, 200, show_reveal_arrow);

    jcontent.click(function() {
        if (event.stopPropagation)
            event.stopPropagation();
        else
            event.cancelBubble = true;
        return false;
    });
}


function update_pass(evt){
    evt.preventDefault();
    var p1 = $(".password1").val();
    var p2 = $(".password2").val();
    if( p1 !='' && p2!='' && p1 == p2){
        $(".error_div").remove();
        $.ajax({
            url : "/update-password",
            type : "post",
            data : {
                p_word : p1,
                username : window.lavuCredentials.username,
                rowid: document.getElementById("approved__login-form").getAttribute("data-rowid")
            }
        }).done( function ( res ) {
            if( res.status = 'success'){
                window.lavuCredentials.password = p1;
                $(".signup_pass_hidden").val( p1 );
                document.forms['approved__login-form'].submit();
                //alert(res.message);
            } else {
                alert(res.message);
            }
        } );
    } else {
        if(!$(".error_div").length){
            $("#approved__login-form").append(
                "<div class=\"error_div\">Passwords do not match</div>"
            );
        }
    }
    return false;
}

function perform_ajax(URL, urlString,asyncSetting,type ){
    var returnVal=null;
    $.ajax({
        url: URL,
        type: type,
        data:urlString,
        async:asyncSetting,
        success: function (string){
            try{
                returnVal = JSON.parse(string);
            }catch(err){
                console.log(err);
                returnVal = string;
            }
        }
    });
    return returnVal;
}

window.jQuery( function ( $ ) {
    if ( typeof ga == "function" ) ga('send', 'event', 'complete', "registration - complete");
    var password_box_controls = ["#approved__show-password-box"];
    password_box_controls.push( "#approved__close-password-box" );
    var open_password_editor = function (){
        $(".update_password_box_cont").toggle();
    };
    $(".approved__login-button,.login_link").click( function (e) {
        e.preventDefault();
        var p1 = $(".password1").val();
        var p2 = $(".password2").val();
        if( p1 ==='' && p2 ==='' ){
            document.forms['approved__login-form'].submit();
        } else {
            return update_pass( e );
        }
        return false;
    } );
} );
