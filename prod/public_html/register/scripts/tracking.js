  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30186945-2', 'auto',{'allowLinker':true});
  ga('create', 'UA-30186945-1', 'auto', {'name': 'originalPOSlavu', 'allowLinker':true});
  ga('originalPOSlavu.send', 'pageview');
  ga('send', 'pageview');

window.send_lead_on_exit = true;
window.no_prompt = false;

function update_data() {
    var dat = window.incoming_data || {};
    var frm = document.forms[0];
    var arr = $( 'form' ).serializeArray();
    for( var i = 0; i < arr.length; i++ ) {
        dat[arr[i].name] = arr[i].value;
    }
    if ( !dat.email ) dat.email = frm.email.value;
    if ( !dat.phone ) dat.phone = frm.phone.value;
    if ( !dat.full_name ) dat.full_name = frm.full_name.value;
    dat.level = frm.level? frm.level.value : location.pathname.match("lavu88")? "lavu88" : "silver";
    dat.Entered_CC__c = dat.card_number ? 1 : 0;
    dat.Zip_Postal_Code__c = frm.zip.value;
    dat.company = frm.company_name.value;
    console.log(document.getElementsByName('referrer').value);
    dat.form = "lead_submit";
    var lavu88 = location.pathname.match('lavu88') ? '88' : '';
    var ref = document.getElementsByName('referrer');
    var social_tracker = ref.length ? document.getElementsByName('referrer')[0].value : "";
    dat.lead_source = "signups" + lavu88 + "_"+ref +"__"+social_tracker;
    dat.demo_type = "signups" + lavu88;
    // let's not pass sensitive stuff
    delete dat.card_code;
    delete dat.card_expiration_mo;
    delete dat.card_expiration_yr;
    delete dat.card_number;
    return dat;
}

function push_to_storage( data ){
    if ( window.sessionStorage ) {
        window.sessionStorage.setItem( 'lavu-data', JSON.stringify( data ) );
    }
}