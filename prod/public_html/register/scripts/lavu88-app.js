window.jQuery( function ($) {
    // get country for step1
    var $country_field = $("#user-country");
    if ($country_field.val() === "A1") {
        $.get("http://ipinfo.io", function (response) {
            window.geodata = response;
            $country_field.val(response.country);
        }, "jsonp");
    }

    var main_validator = $('form').validate({
        onkeyup: false,
        onfocusout: false,
        invalidHandler: function (event, validator) {
            $("#recaptcha_reload").click();
        },
        submitHandler: function (form) {
            $("input[type='submit']")[0].disabled=true;
            $('#modal').addClass('show');
            form.submit();
        }
    });
    $("#country-dropdown").change(function () {
        var $us_states_dropdown = $("#us-states-dropdown");
        if ($us_states_dropdown.length) {
            if (this.value !== "US") {
                $("#us-states-dropdown").replaceWith(
                    "<input name=\"state\" class=\"inputsmall required\" type=\"text\" id=\"state-no-us\">"
                );
            }
        } else {
            if (this.value === "US") {
                $("#state-no-us").replaceWith($("#states-dropdown-template").html());
            }
        }
    });
    if ( $("#approved__login-form").length ) { //if in is approved page
        (function () {
            var fpw = document.createElement('script');
            fpw.async = true;
            fpw.src = '/scripts/approved.js';
            var ref = document.getElementsByTagName('script')[0];
            ref.parentNode.insertBefore(fpw, ref);
        })();
    }
});