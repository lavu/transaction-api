function select_function (column) {
	$('table.start_menu').parent().closest("td").removeClass("highlight"); //forget the last highlighted column
	$('table.start_menu').parent().closest("td."+column).addClass("highlight"); //highlight the selected column
	$('table.start_menu').parent().closest("td."+column).find(":radio").attr("checked","checked");
	return false;
}

$(document).ready(function() {
    // Fix for the tooltip thing in Safari
	// Check for Safari
	var isSafari = /constructor/i.test(window.HTMLElement);
	if ( isSafari ) {
		$( ".tooltip" ).hide();
		$( ".tooltip" ).mouseover( function () { $( this ).hide(); } );
		$( ".starter_menu_image" ).mouseover( function () {
			$( this ).children( ".tooltip" ).show();
		} ).mouseout(  function () {
			$( this ).children( '.tooltip' ).hide();
		} );
	}
	//$(".select_link").click(select_function);
	var jmenus = $("a.menu");
	var menu_click = function(k, menu_element) {
		var jmenu_element = $(menu_element);
		var title = jmenu_element.attr("title");
		var jtable_parent = jmenu_element.parent().parent().parent().parent();
		jtable_parent.css({cursor: 'pointer'});
		jtable_parent.click(function(){
			select_function(title);
		});
	};
	$.each(jmenus, menu_click);

	$.validator.addMethod("englishChars", function(value, element) {
		// support not requiring this method for validation
		if (this.optional(element))
			return true;

		var matchVal = value.match(/^[a-zA-Z0-9 '\@-]*$/);
		if (!$.isArray(matchVal) || matchVal[0] != value)
			return false;
		return true;
	});

	// validation
	// validate signup form on keyup and submit
	$("#new_customer").validate({
		rules: {
			mname: "required",
			company_name:  {
				required: true,
				englishChars: true
			},
			phone:  {
				required: true,
				englishChars: true
			},
			email: {
				required: true,
				email: true
			},
			firstname:  {
				required: true,
				englishChars: true
			},
			lastname:  {
				required: true,
				englishChars: true
			},
			city:  {
				required: true,
				englishChars: true
			},
			state:  {
				required: true,
				englishChars: true
			},
			address:  {
				required: true,
				englishChars: true
			},
			zip:  {
				required: true,
				englishChars: true
			},
			promo:  {
				required: false,
				englishChars: true
			},
		},
		messages: {
			company_name: {
				required: "Company name is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			phone: {
				required: "Phone number is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			email: {
				required: "Please enter a valid email address",
				email: "Must be valid email address",
			},
			lastname: {
				required: "Phone is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			firstname: {
				required: "First name is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			city: {
				required: "City is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			state: {
				required: "State is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			address: {
				required: "Address is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			zip: {
				required: "Zip code is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			promo: {
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			},
			full_name: {
				required: "Your full name is required",
				englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
			}
		},
		submitHandler : function ( form  ) {
			window.send_lead_on_exit = false;
			window.no_prompt = true;
			form.submit();
		}
	});

	// highlight the menu name that has been chosen
	var jmname = $("[name=mname]:checked");
	if (jmname.length > 0) {

		// find the parent of an element based on an .is() selector
		// @return: the jparent, or $('div') if the parent can't be found
		function get_parent(jelement, parent_selector) {
			var jparent = jelement.parent();
			if (jparent.is(parent_selector))
				return jparent;
			if (jparent.is('document'))
				return jparent;
			return get_parent(jparent, parent_selector);
		}

		// get the td of the mname and give it the highlight class
		var jtd = get_parent(get_parent(jmname, 'table'), 'td');
		if (jtd.is('td')) {
			if (!jtd.hasClass('highlight')) {
				jtd.addClass('highlight');
			}
		}
	}

});
