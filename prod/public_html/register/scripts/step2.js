$(document).ready(function() {
	$( "#fake_agree_tos" ).click( function ( event ) {
		$( "#agree_tos" ).click();
	} );
	var select_function = function() { //check for the second selection
		var column = $(this).attr('title'); // assign the ID of the column
		if (!column || column == '') {
			column = $(this).children('a').attr('title');
		}
		$('table.start_menu').parent().closest("td").removeClass("highlight"); //forget the last highlighted column
		$('table.start_menu').parent().closest("td."+column).addClass("highlight"); //highlight the selected column
		$('table.start_menu').parent().closest("td."+column).find(":radio").attr("checked","checked");
		return false;
	};

	$(".select_link").click(select_function);
	$("a.menu").click(select_function);
	$(".select_link").css({cursor:'pointer'});

	// highlight radio buttons
//	$('table.start_menu').children().find('img').tooltip();

	// validation
	// validate signup form on keyup and submit
	$("#new_customer").validate( {
		rules: {
			card_number: "required",
			card_expiration_mo: "required",
			card_expiration_yr: "required",
			card_code: "required",
			agree_tos: "required",
			firstname: "required",
			lastname: "required",
			city: "required",
			address: "required",
			zip: "required"
		},
		messages: {
			card_number: "Please enter a valid credit card number",
			card_expiration_mo: "Please enter a valid credit card expiration month",
			card_expiration_yr: "Please enter a valid credit card expiration year",
			card_code: "Please enter a valid credit card code",
			agree_tos: "Please agree to our Terms of Service",
			firstname: "Please enter your first name",
			lastname: "Please enter your last name",
			city: "Please enter a valid city",
			address: "Please enter a valid address",
			zip: "Please enter a valid zip code"
		},
	    ignore: ":hidden",
		submitHandler : function ( form  ) {
			window.no_prompt = true;
			form.submit();
		}
	} );
} );