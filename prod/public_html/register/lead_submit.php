<?php
function sort_by_length ( $a, $b ) {
	return strlen( $b ) - strlen( $a );
}
if ( !$_REQUEST ) exit;

$e = trim( $_REQUEST['email'] );
$p = trim( $_REQUEST['phone'] );
$c = trim( $_REQUEST['company'] );
$n = trim( $_REQUEST['full_name'] );
if ( empty( $e ) || empty( $p ) || ( empty( $c ) && empty( $n ) ) )  {
	exit;
}

$data = array();
$curl_opts = array();
$data['oid'] = empty( $_REQUEST['oid'] )? "00DF000000076Td" : $_REQUEST['oid'];
$data['00NF000000Aao6L'] = empty( $_REQUEST['00NF000000Aao6L'] )? $_REQUEST['level'] : $_REQUEST['00NF000000Aao6L'];
$data['email'] = $e;
$data['phone'] = $p;
$data['company'] = $c;
$data['Zip_Postal_Code__c'] = $_REQUEST['Zip_Postal_Code__c'];
$data['Entered_CC__c'] = $_REQUEST['Entered_CC__c'];
$name_pieces = preg_split( "/[\s,]+/", $n );
$name_pieces_copy = preg_split( "/[\s,]+/", $n );
usort( $name_pieces, "sort_by_length" );
$name = array();
if ( array_search( $name_pieces[0], $name_pieces_copy ) > array_search( $name_pieces[1], $name_pieces_copy ) ) {
	$data['first_name'] = $name_pieces[1];
	$data['last_name'] = $name_pieces[0];
} else {
	$data['first_name'] = $name_pieces[0];
	$data['last_name'] = $name_pieces[1];
}
// $data["debug"] = 1;
// $data["debugEmail"] = "electpaisa@hotmail.com";
$c = curl_init('https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8&' . http_build_query( $data ) );
curl_setopt( $c, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt( $c, CURLOPT_HTTPHEADER, array( "Content-Type" => "application/x-www-form-urlencoded" ) );
curl_setopt( $c, CURLOPT_FOLLOWLOCATION, 1 );
curl_setopt( $c, CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $c, CURLOPT_SSL_VERIFYHOST, false );
curl_setopt( $c, CURLOPT_HEADER, false );
curl_setopt( $c, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)" );
$resp = curl_exec( $c );
$err     = curl_errno( $c ); 
$errmsg  = curl_error( $c ) ; 
$header  = curl_getinfo( $c );
curl_close( $c );
var_dump( $header );
var_dump( $err );
var_dump( $errmsg );
var_dump( $resp );
// $s = empty( $resp )? "false" : "true";
// 	echo "{success:$s}";
if ( !empty( $_REQUEST['origin'] ) && strpos( $_REQUEST['origin'], "_dev" ) !== false ) {
    require_once "mailer.php";
	send_multipart_email(
	    'noreply@poslavu.com',
		'Salesforce lead feedback',
		"<div style='white-space:pre;'>" . htmlentities( $resp ) . "</div>",
		strip_tags( $resp ),
		"electpaisa@hotmail.com"
	);
}

?>