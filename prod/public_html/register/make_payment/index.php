<?php
	session_start();
	ini_set("display_errors","1");
	require_once(dirname(__FILE__) . "/../../admin/cp/resources/core_functions.php");
	require_once(dirname(__FILE__) . "/../../admin/sa_cp/billing/package_levels_object.php");
	require_once(dirname(__FILE__) . "/../../admin/sa_cp/billing/payment_profile_functions.php");
	require_once(dirname(__FILE__) . "/../../admin/sa_cp/billing/procareas/SalesforceIntegration.php");
	require_once(dirname(__FILE__) . "/../../admin/distro/beta/exec/distro_user_functions.php");
	require_once(dirname(__FILE__) . "/../../admin/manage/globals/email_addresses.php");
	require_once(dirname(__FILE__) . '/../../inc/billing/zuora/ZuoraIntegration.php');

	global $o_package_container;
	if (!isset($o_package_container))
		$o_package_container = new package_container();

	$maindb = "poslavu_MAIN_db";

	$zInt = new ZuoraIntegration();

	$key = urlvar("key");
	$refid = $_REQUEST["refId"];  // mind the capital "i"
	$sessionid = urlvar("sessionid");
?>
<html>
<head>
<title>Make a Payment</title>
<style>
body {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
table {
	color: #414141;
	font-family: Arial, Helvetica, sans-serif;
	font-size:12px;
}
#zuora_payment {
	width: 440px;
}
 </style>
</head>
<body>
<div style="margin:12px; margin-bottom:0px; font-weight:500; line-height:1.1;">
	<img src="../images/lavu-logo.png">
	<h2 style="font-family:Arial; color:#999999; margin-bottom:0px">Customer Payments</h2>
</div>
<?php
	// LP-349 -- Added criteria to exclude deleted Payment Links
	$req_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_request` where `key`='[1]' and `_deleted`=''",$key);
	if (mysqli_num_rows($req_query))
	{
		// Load payment_request database row
		$req_read = mysqli_fetch_assoc($req_query);
		$hardware_amount = $req_read['hardware_amount'];
		$license_amount = $req_read['license_amount'];
		$hosting_amount = $req_read['hosting_amount'];
		$points_amount = $req_read['points_amount'];
		$hardware_paid = $req_read['hardware_paid'];
		$license_paid = $req_read['license_paid'];
		$hosting_paid = $req_read['hosting_paid'];
		$points_paid = $req_read['points_paid'];
		$action = $req_read['action'];
		$notes = $req_read['notes'];
		$dataname = $req_read['dataname'];
		$distro_code = $req_read['distro_code'];
		$restaurantid = $req_read['restaurantid'];
		$signupid = $req_read['signupid'];
		$createuser = $req_read['createuser'];
		$shipping = $req_read['shipping'];
		$notify_email = $req_read['notify_email'];
		$hostingcc = $_REQUEST['hostingcc'];
		$hardware_payment_id = "HW" . $req_read['id'];
		$license_payment_id = "L" . $req_read['id'];
		$hosting_payment_id = "H" . $req_read['id'];
		$start_date_ts = mktime(0, 0, 0, date("m"), date("d") + 30, date("Y"));
		$start_date = date("m/d/Y", $start_date_ts);

		// Get signup row ID
		if($req_read['signupid']!="")   $rowid = $req_read['signupid'];
		else if($signup_read['id']!="") $rowid = $signup_read['id'];
		else $rowid = false;

		// Get refid from the payment_request but don't override the one from the http request
		if (empty($refid)) $refid = $req_read['refid'];

		// See if just ran the payment through
		$just_applied_payment = (isset($_REQUEST['refId']) && empty($req_read['refid']));

		// Load signups database row
		$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where id='[1]'",$rowid);
		if (mysqli_num_rows($signup_query))
		{
			$signup_read = mysqli_fetch_assoc($signup_query);
			$first_name = $signup_read['firstname'];
			$last_name = $signup_read['lastname'];
			$address = $signup_read['address'];
			$city = $signup_read['city'];
			$state = $signup_read['state'];
			$country = $signup_read['country'];
			$zip = $signup_read['zip'];
			$email = $signup_read['email'];
			$phone = $signup_read['phone'];
			$company_name = $signup_read['company'];
			$reseller_quote_id = $req_read['reseller_quote_id'];
			$salesforce_quote_id = $req_read['salesforce_quote_id'];
			$quote_pdf_filename =$req_read['quote_pdf_filename'];

			// Older payment_requests don't have datanames so get it from the signup row
			if (empty($dataname)) $dataname = $signup_read['dataname'];
		}

		// Special logic for Quote payments
		if ($action == 'Quote Payment') {
			$q = mlavu_query("SELECT r.`id` AS `restaurantid`, r.`created`, r.`distro_code`, s.`source` AS `signup_source`, s.* FROM `poslavu_MAIN_db`.`signups` s LEFT JOIN `poslavu_MAIN_db`.`restaurants` r ON (s.`dataname` = r.`data_name`) WHERE s.`id`='[signupid]", $req_read);
			if ($q !== false) $signup_read = mysqli_fetch_assoc($q);
			$start_date_ts = billing_get_next_bill_date($signup_read['dataname'], $signup['package'], $signup['created'], $signup['restaurantid'], $signup_read, $billing_profiles);  // from payment_profile_functions.php for the hosting date

			$q = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_quotes` WHERE `id`='[reseller_quote_id]'", $req_read);
			if ($q === false) die(__FILE__ .": ". mlavu_dberror());
			$reseller_quote = mysqli_fetch_assoc($q);
			$distro_code = $reseller_quote['distro_code'];
			$resellerid = $reseller_quote['resellerid'];
			$start_date = date("m/d/Y", $start_date_ts);
		}

		// Display payment_request info
		echo "<table cellspacing=0 cellpadding=12><td>";

		// Compute and display total_amount
		$total_amount = 0.00;
		if($hardware_amount!="" && $hardware_amount > 0 && $hardware_paid!="paid")  $total_amount += $hardware_amount;
		if($license_amount!=""  && $license_amount > 0  && $license_paid!="paid")   $total_amount += $license_amount;
		if($hosting_amount!=""  && $hosting_amount > 0  && $hosting_amount!="paid") $total_amount += $hosting_amount;
		if($total_amount!="" && $total_amount > 0 && $hardware_amount!="paid" && $license_paid!="paid") echo "<br>Amount to charge now: <strong style='font-size:1.3em'>$" . number_format($total_amount,2,".",",") . "</strong>";
		$total_amount_formatted = number_format($total_amount,2,".",",");
		$paid_previously = (empty($req_read['license_amount']) || !empty($req_read['license_paid'])) && (empty($req_read['hosting_amount']) || !empty($req_read['hosting_paid'])) && (empty($req_read['points_amount']) || !empty($req_read['points_paid']));
		$show_form = ($total_amount!="" && $total_amount > 0);

		// Display payment_request note
		if($notes!="") echo "<br><br><strong style='font-size:1.2em'>Notes:</strong> " . htmlspecialchars($notes);

		echo "<br>";
		if($rowid)
		{
			if ( $just_applied_payment || $paid_previously )
			{
				// LP-349 -- Replaced "paid" verbiage per Josh Edwards
				echo "<br>You have authorized Lavu to charge your card in the amount of $". $total_amount_formatted ." (Ref #". htmlspecialchars($req_read['id']) .")<br><br>";
				$show_form = false;
			}

			if ( $just_applied_payment )
			{
				if ($hardware_amount!="" && $hardware_amount > 0 && $hardware_paid!="paid") mlavu_query("update `poslavu_MAIN_db`.`payment_request` set `hardware_paid`='paid' where `key`='[1]'", $key);
				if ($license_amount!="" && $license_amount > 0 && $license_paid!="paid") mlavu_query("update `poslavu_MAIN_db`.`payment_request` set `license_paid`='paid' where `key`='[1]'", $key);
				if ($hosting_amount!="" && $hosting_amount > 0 && $hosting_paid!="paid") mlavu_query("update `poslavu_MAIN_db`.`payment_request` set `hosting_paid`='paid', `hostingcc`='[2]' where `key`='[1]'", $key, $hostingcc);
				if ($req_read['refid']=="") mlavu_query("update `poslavu_MAIN_db`.`payment_request` set `refid`='[2]' where `key`='[1]' and `refid`=''", $key, $refid);

				$rest_update = "";
				if($action=="Grant Silver License")
					$rest_update = "`package_status`='Silver', `license_status`='Paid'";
				else if($action=="Grant Gold License")
					$rest_update = "`package_status`='Gold', `license_status`='Paid'";
				else if($action=="Grant Platinum License")
					$rest_update = "`package_status`='Platinum', `license_status`='Paid'";
				else if($action=="Quote Payment") {
					SalesforceIntegration::createSalesforceEvent($dataname, 'update_quote', json_encode( array( 'QuoteId' => $salesforce_quote_id, 'Status' => 'Paid', 'PaidQuotePDF__c' => $quote_pdf_filename, 'Paid_Date__c' => date('Y-m-d') ) ));
					$sent_billing_email = $o_emailAddressesMap->sendEmailToAddress('payment_notifications', 'Successful Quote Payment', "{$dataname} successfully created a PaymentMethod for Salesforce Quote ID {$salesforce_quote_id} ({$quote_pdf_filename}) ");
				}

				// Attach the newly created PaymentMethod to the Zuora account for the dataname, creating it if it doesn't exist

				$created_account = false;
				$params = array(
					'dataname'   => $dataname,
					// LP-354 -- Fixed PaymentMethod not associating to existing Zuora accounts becauase the zAccountId never got set
					'zAccountId' => $signup_read['zAccountId'],
				);

				// Get the existing Zuora account, if they have one
				$zAccount = $zInt->getAccount($params);

				// No Zuora account found so create one
				if ( $zAccount === null )
				{
					// If we're creating a new account, set the invoice day of the month to be today, since the subscription should start today
					$params['BillCycleDay'] = date('j');

					// Associate with the Salesforce AccountId if we have one on file
					if ( !empty($signup_read['salesforce_id']) )
					{
						$params['CrmId'] = $signup_read['salesforce_id'];
					}

					// Create the Zuora account
					$zAccount = $zInt->createAccount($params);
					$created_account = ($zAccount !== null);

					// Make sure the acount was created ok
					if ( $created_account )
					{
						// We can activate account when associate the PaymentMethod since we now have contacts
						$params['Status'] = 'Active';

						// Josh Edwards wants this to be the default of new accounts
						$params['AutoPay'] = true;

						// Save our Zuora Account ID (in $params for the update, $signup_read for the email)
						$params['zAccountId'] = $zAccount->Id;
						$signup_read['zAccountId'] = $zAccount->Id;
						mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `zAccountId`='[zAccountId]' WHERE `dataname`='[dataname]' AND `zAccountId`=''", $params);
					}
					// Zuora account creation failed (notice we don't exit - but we check for zAccount !== null in next logic block)
					else
					{
						$error_msg = "{$dataname} failed to create a Zuora account";
						error_log(__FILE__ ." got error: ". $error_msg);
						$sent_billing_email = $o_emailAddressesMap->sendEmailToAddress('billing_errors', 'Payment Link Zuora Account Creation Failed', $error_msg);
					}
				}
				// Existing Zuora account found
				else
				{
					// If the Account already has a default PaymentMethod, save it so we can restore it later
					if ( !empty($zAccount->DefaultPaymentMethodId) )
					{
						$originalDefaultPaymentMethodId = $zAccount->DefaultPaymentMethodId;
					}
				}

				// Zuora account creation/retrieval has to have succeeded to proceed with associating the new PaymentMethod to it
				if ( $zAccount !== null )
				{
					// Account needs Bill To and Sold To contact to be activated so we'll just make one (if needed) and use it for both
					// The fields come from the credit card address fields they just entered
					$ccaddress = array();
					$ccaddress['firstname'] = $signup_read['firstname'];
					$ccaddress['lastname']  = $signup_read['lastname'];
					$ccaddress['address']   = $_REQUEST['creditCardAddress1'];
					$ccaddress['address2']  = $_REQUEST['creditCardAddress2'];
					$ccaddress['city']      = $_REQUEST['creditCardCity'];
					$ccaddress['state']     = $_REQUEST['creditCardState'];
					$ccaddress['zip']       = $_REQUEST['creditCardPostalCode'];
					$ccaddress['country']   = $_REQUEST['creditCardCountry'];
					$ccaddress['phone']     = $_REQUEST['phone'];
					$ccaddress['email']     = $_REQUEST['email'];
					$zBillToContact = $zInt->zDataExtractor->getBillToContact($ccaddress);
					$zBillToContact->AccountId = $signup_read['zAccountId'];

					$zContactsToCreate = array();
					if ( $created_account || empty($zAccount->BillToContactId) || empty($zAccount->SoldToContactId) )
					{
						$zContactsToCreate[] = $zBillToContact;
					}

					if ( count($zContactsToCreate) > 0 )
					{
						$response = $zInt->createZuoraObject('zContact', 'create', $zContactsToCreate);

						if ( $response->result->Success && empty($zAccount->BillToContactId) )
						{
							$zAccount->BillToContactId = $response->result->Id;
							$zBillToContact->Id = $response->result->Id;
							$params['BillToId'] = $response->result->Id;
						}
						if ( $response->result->Success && empty($zAccount->SoldToContactId) )
						{
							$zAccount->SoldToContactId = $response->result->Id;
							$params['SoldToId'] = $response->result->Id;
						}
					}

					// Make the newly create PaymentMethod the default on the Zuora account
					$params['DefaultPaymentMethodId'] = $refid;
					$update_ok = $zInt->updateAccount($params);

					// Make sure the update worked
					if ( $update_ok )
					{
						// If the existing account already had a default PaymentMethod, the only way we can associate the newly created PaymentMethod
						// with the Account is by making it the default.  But we can then restore the original PaymentMethod as the default.
						if ( !empty($originalDefaultPaymentMethodId) )
						{
							$params['DefaultPaymentMethodId'] = $originalDefaultPaymentMethodId;
							$update_ok2 = $zInt->updateAccount($params);

							if ( !$update_ok2 )
							{
								$error_msg = "{$dataname} could not set Payment Method ID {$originalDefaultPaymentMethodId} back to the default on Zuora Account ID {$params['zAccountId']}";
								error_log(__FILE__ ." got error: {$dataname} could not set Payment Method ID {$originalDefaultPaymentMethodId} back to the default on Zuora Account ID {$params['zAccountId']}");
								$sent_billing_email = $o_emailAddressesMap->sendEmailToAddress('billing_errors', 'Payment Link Zuora Account Update Failed (Restoring DefaultPaymentMethodId)', $error_msg);
							}
						}
					}
					// Send an error email if it didn't work
					else
					{
						$error_msg = "{$dataname} could not associate Payment Method ID {$refid} with Zuora Account ID {$params['zAccountId']}";
						error_log(__FILE__ ." got error: {$dataname} could not associate Payment Method ID {$refid} with Zuora Account ID {$params['zAccountId']}");
						$sent_billing_email = $o_emailAddressesMap->sendEmailToAddress('billing_errors', 'Payment Link Zuora Account Update Failed', $error_msg);
					}
				}


				// Send notify email
				if($notify_email!="")
				{
					$msg = "";
					$msg .= "A credit card has been entered by the customer via Payment Link";
					if($shipping=="1")
					{
						$msg .= "\nName: " . $_REQUEST['ship_name'];
						$msg .= "\nAddress: " . $_REQUEST['ship_address'];
						$msg .= "\nCity: " . $_REQUEST['ship_city'];
						$msg .= "\nState: " . $_REQUEST['ship_state'];
						$msg .= "\nZip: " . $_REQUEST['ship_zip'];
						if(isset($notes)) $msg .= "\n\nNotes: " . $notes;
					}
					else
					{
						$msg .= "\nPayment Link Type: " . $action;
						$msg .= "\nDataname: " . $dataname . " (Rest ID $restaurantid)";
						$msg .= "\nCompany Name: " . $company_name;
						$msg .= "\nFull Name: " . trim($first_name . " " . $last_name);
						$msg .= "\n";
						if ($hardware_amount!="" && $hardware_amount > 0) $msg .= "\nHardware Amount: " . number_format($hardware_amount,2,".",",");
						if ($license_amount!="" && $license_amount > 0)   $msg .= "\nAmount: "  . number_format($license_amount,2,".",",");
						if ($hosting_amount!="" && $hosting_amount > 0)   $msg .= "\nHosting Amount: "  . number_format($hosting_amount,2,".",",");
						if(isset($createuser)) $msg .= "\n\nCreate User: " . $createuser;
						if(isset($distro_code)) $msg .=  "\nDistro Code: " . $distro_code;
						if(isset($notes)) $msg .= "\n\nNotes: " . $notes;
					}

					$msg .= "\n\nLavu Sales -- Please update quote with this info before pressing Send to Zuora:\n\n";
					if ($signup_read['zAccountId'])  $msg .= "Zuora Account ID ". $signup_read['zAccountId'] ."\n";
					//if ($refid)  $msg .= "Zuora PaymentMethod ID ". $refid ."\n";

					// LP-3534 - Removed 4th argument that forces notify emails to come from info@poslavu.com because it can cause problems with mail getting relayed
					mail($notify_email,"Lavu Payment Link Notification",$msg);
					mail('s.brown@lavu.com',"Lavu Payment Link Notification",$msg);
					$o_emailAddressesMap->sendEmailToAddress('payment_notifications', 'Lavu Payment Link Notification', $msg);

				}
			}
		}
		else
		{
			echo "Error: cannot find signup information";
		}

		echo "</td></table>";
	}
	else echo "Key not found";

// Display Zuora Hosted Payment Method page
if ( $show_form )
{
	$zInt->echoHpmHtmlDiv();
	$zInt->echoHpmJsInc();
	echo '<script>
			function goToThxPage(obj) {
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				for (var key in obj) {
					if (obj.hasOwnProperty(key)) {
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", key);
						hiddenField.setAttribute("value", obj[key]);
						form.appendChild(hiddenField);
					}
				}
				document.body.appendChild(form);
				form.submit();
			}

			function callback(response) {
				if(response.success) {
					//var redirectUrl = window.location.href+"&refid="+response.refId;
					//window.location.replace(redirectUrl);
					goToThxPage(response);
				} else {
					alert("errorcode="+response.errorCode + ", errorMessage="+response.errorMessage);
				}
			}
	';
	$signup_read['page_name'] = 'payment_link';
	$signup_read['auto_show_form'] = true;
	$signup_read['style']  = 'inline';
	$signup_read['amount'] = $total_amount;
	$signup_read['http_host'] = $_SERVER['HTTP_HOST'];
	$zInt->echoHpmJs($signup_read);
	echo '</script>';
}
?>

</body>
</html>
