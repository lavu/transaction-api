<?php

// some globals

if (!isset($o_package_container))
    $o_package_container = new package_container();
$maindb = "poslavu_MAIN_db";
$b_restaurant_created_in_auth_account = FALSE;

$allow_4111_testing = true;


/**
 * Create the billing profiles and payment_hostlink for an account
 * @param  integer $p_signupid          The id of the restaurant's signup row
 * @param  string  $hostlink_type       One of 'hosting', 'license', 'Hosting', or 'License'
 * @param  integer $subscriptionid      The subscription id returned by Authorize.net
 * @param  string  $subscription_start  A datetime string of when the Authorize.net subscription starts
 * @param  double  $subscription_amount The amount that is charged to the customer every time the subscription gets triggered (once a month for hosting, once for license)
 * @param  string  $first_name          The name of the user that entered the credit card information
 * @param  string  $last_name           The name of the user that entered the credit card information
 * @param  string  $card_number         The digits in the credit card number
 * @param  string  $package             The package level of the restuarant
 * @return none                         N/A
 */
function insert_signup_hostlink($p_signupid,$hostlink_type,$subscriptionid,$subscription_start,$subscription_amount,$first_name="",$last_name="",$card_number="",$package="")
{
    $hlink_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_hostlink` where `profileid`='[1]'",$subscriptionid);
    if(mysqli_num_rows($hlink_query))
    {
    }
    else
    {
        mlavu_query("insert into `poslavu_MAIN_db`.`payment_hostlink` (`signupid`,`profileid`,`profiletype`,`profilestart`,`profileamount`) values ('[signupid]','[profileid]','[profiletype]','[profilestart]','[profileamount]')", array('signupid'=>$p_signupid, 'profileid'=>$subscriptionid, 'profiletype'=>$hostlink_type, 'profilestart'=>$subscription_start, 'profileamount'=>$subscription_amount));
    }

    $bprof_query = mlavu_query("select * from `poslavu_MAIN_db`.`billing_profiles` where `subscriptionid`='[1]'",$subscriptionid);
    if(mysqli_num_rows($bprof_query))
    {
    }
    else
    {
        $last_four = substr($card_number,strlen($card_number) - 4);

        $vars = array();
        $vars['subscriptionid'] = $subscriptionid;
        $vars['type'] = $hostlink_type;
        $vars['start_date'] = $subscription_start;
        $vars['last_name'] = $last_name;
        $vars['first_name'] = $first_name;
        $vars['last_four'] = $last_four;
        $vars['amount'] = $subscription_amount;
        $vars['package'] = $package;
        $vars['restaurantid'] = "";
        $vars['dataname'] = "";
        $vars['signupid'] = $p_signupid;
        $vars['last_pay_date'] = "";
        $vars['active'] = "1";
        mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_profiles` (`subscriptionid`, `type`, `start_date`, `last_name`, `first_name`, `last_four`, `amount`, `package`, `restaurantid`, `dataname`, `signupid`, `last_pay_date`, `active`) VALUES ('[subscriptionid]', '[type]', '[start_date]', '[last_name]', '[first_name]', '[last_four]', '[amount]', '[package]', '[restaurantid]', '[dataname]', '[signupid]', '[last_pay_date]', '[active]')",$vars);
    }
}

/**
 * Attach the given information to a billing profile
 */
function connect_billing_profile($subscriptionid,$dataname,$restaurantid)
{
    mlavu_query("UPDATE `poslavu_MAIN_db`.`billing_profiles` set `dataname`='[1]', `restaurantid`='[2]' where `subscriptionid`='[3]'",$dataname,$restaurantid,$subscriptionid);
}

/**
 * Confirms that the restaurant shard in poslavu_MAINREST_db exists.
 * @param  string $data_name The restaurant's dataname
 * @return string            One of:
 *     $firstLetterOfDataname.": ".$data_name." exists"
 *     $firstLetterOfDataname.": ".$data_name." inserting <font color='green'>success</font>"
 *     $firstLetterOfDataname.": ".$data_name." inserting <font color='red'>failed</font>"
 */
function confirm_restaurant_shard($data_name)
{
    $str = "";
    $letter = substr($data_name,0,1);
    if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
        $letter = $letter;
    else
        $letter = "OTHER";
    $tablename = "rest_" . $letter;

    $mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
    if(mysqli_num_rows($mainrest_query))
    {
        $str .= $letter . ": " . $data_name . " exists";
    }
    else
    {
        $str .= $letter . ": " . $data_name . " inserting";

        $rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
        if(mysqli_num_rows($rest_query))
        {
            $rest_read = mysqli_fetch_assoc($rest_query);

            $success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
            if($success) $str .= " <font color='green'>success</font>";
            else $str .= " <font color='red'>failed</font>";
        }
    }
    return $str;
}

/**
 * Creates a poslavu account.
 * Should only be called after the signups row has been created.
 * Redirects to the approved.php page and the equivelant translated pages.
 * @param  integer $rowid          The id of the restaurant's row from the signups table
 * @param  string  $company        The company name of the restaurant, used to derive the dataname, not the dataname
 * @param  string  $email          The email address used during account creation
 * @param  string  $phone          The phone number used during account creation
 * @param  string  $firstname      The first name of the primary user used during account creation
 * @param  string  $lastname       The last name of the primary user used during account creation
 * @param  string  $address        The mailing address used during account creation
 * @param  string  $city           The city used during account creation
 * @param  string  $state          The state used during account creation
 * @param  string  $zip            The zip code used during account creation
 * @param  string  $use_default_db THIS SHOULD ALWAYS BE "MAIN"
 * @return array                   returns an array as one of the following
 *     array('success'=>FALSE)
 *     array('success'=>TRUE, 'dataname'=>dataname)
 */
function execute_poslavu_creation($rowid,$company,$email,$phone,$firstname,$lastname,$address="",$city="",$state="",$zip="",$use_default_db="")
{
    global $maindb;
    global $doc_root;
    $admin_dir;
    echo "we are here " . basename(__FILE__) . ':L' . basename(__LINE__) . DOC_ROOT ."/dev/create_account_dev.php\n<br>" ;
    require_once "create_account_dev.php" ;
    write("we are not here " );
    $result = create_new_poslavu_account($company,$email,$phone,$firstname,$lastname,$address,$city,$state,$zip,$use_default_db,$_POST['locale']);
    $success = $result[0];
    var_dump($result);
    if($success)
    {
        $username = $result[1];
        $password = $result[2];
        $dataname = $result[3];

        $credvars = array();
        $credvars['username'] = $username;
        $credvars['password'] = $password;
        $credvars['dataname'] = $dataname;
        $credvars['rowid'] = $rowid;
        $credvars['reseller_leads_rowid'] = (isset($_POST['reseller_leads_rowid']))?$_POST['reseller_leads_rowid']:'';
        mlavu_query("update `poslavu_MAIN_db`.`signups` set `username`='[username]', `dataname`='[dataname]', `password`=AES_ENCRYPT('[password]','password') where `id`='[rowid]'",$credvars);
        confirm_restaurant_shard($dataname);

        $signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `id`='[1]'",$rowid);
        if(mysqli_num_rows($signup_query))
        {
            $signup_read = mysqli_fetch_assoc($signup_query);
            $rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
            if(mysqli_num_rows($rest_query))
            {
                $rest_read = mysqli_fetch_assoc($rest_query);

                if ($credvars['reseller_leads_rowid'] != '') {
                    mlavu_query("UPDATE `$maindb`.`reseller_leads` SET `data_name`='[dataname]' WHERE `id`='[reseller_leads_rowid]'", $credvars);
                    if (ConnectionHub::getConn('poslavu')->affectedRows()) {
                        mail("sales@poslavu.com", "New Lead Dataname", "A new lead with company name (".$company.") and data name (".$credvars['dataname'].") was created. Please assign them to a distributor at https://admin.poslavu.com/distro/beta/index.php#leads_tab.", "from:noreply@poslavu.com");
                        mail("distributors@poslavu.com", "New Lead Dataname", "A new lead with company name (".$company.") and data name (".$credvars['dataname'].") was created. Please assign them to a distributor at https://admin.poslavu.com/distro/beta/index.php#leads_tab.", "from:noreply@poslavu.com");
                    }
                }

                if(!isset($_POST['vf_signup']))
                {
                    connect_billing_profile($signup_read['arb_license_id'],$dataname,$rest_read['id']);
                    connect_billing_profile($signup_read['arb_hosting_id'],$dataname,$rest_read['id']);
                }
                else if(!isset($_POST['vf_skip']))
                {
                    $toks = Array( 'key' => 'integration_keystr()',
                                    'dname' => $dataname,
                                    'iname' => 'integration',
                                    'int1' => $_POST['vf_username'], //vf_api_secret
                                    'int2' => $_POST['vf_password'], //vf_password
                                    'int3' => $_POST['vf_client_id'], //vf_api_token
                                    'int4' => $_POST['vf_api_token'], //vf_client_id
                                    'int5' => $_POST['vf_api_secret']); //vf_username

                    $qstr = "update `poslavu_[dname]_db`.`locations` set `[iname]1`= AES_ENCRYPT('[int1]','[key]'), `[iname]2`= AES_ENCRYPT('[int2]','[key]'), `[iname]3`= AES_ENCRYPT('[int3]','[key]'), `[iname]4`= AES_ENCRYPT('[int4]','[key]'), `[iname]5`= AES_ENCRYPT('[int5]','[key]'),  `integrateCC`='1', `gateway`='SailPay'";
                    mlavu_query($qstr, $toks);
                }
            }
        }

        // redirect to translated approved pages
        global $s_dev;
        if (!isset($s_dev)) {
            $s_dev = "";
        }
        switch ($_POST['locale']) {
            case 'es':
                $redirect_to = "/es/approved{$s_dev}.php?id=".($rowid + 12500);
                break;

            case 'cn':
                $redirect_to = "/cn/approved{$s_dev}.php?id=".($rowid + 12500);
                break;

            default:
                $redirect_to = "/approved{$s_dev}.php?id=".($rowid + 12500);

        }

        echo "<script language='javascript'>";
        echo "window.location.replace('".$redirect_to."'); ";

        echo "</script>";

        return array(
            'success'=>TRUE,
            'dataname'=>$dataname,
        );
    }
    else
    {
        echo "Failed";

        return array(
            'success'=>FALSE,
        );
    }
}

class AuthAccountExtractedFunctions {

    /**
     * This is the old code to get the information necessary to create an account.
     * @param  array   $a_immutables An array of variables passed in that aren't changed within this function, including
     *     $packages, $package, $mname
     * @param  array   $package_info Set to $packages[$package]
     * @param  array   $fields       An array of arrays that are fields the user is meant to fill in
     * @param  boolean $show_form    Always set to FALSE
     * @param  string  $content      Used to append the $formcode to the output
     * @param  array   $validate     Javascript code to validate the form return values
     * @param  string  $formcode     The html used to create the form
     * @param  string  $insert_cols  Used for an sql insert statement
     * @param  string  $insert_vals  Used for an sql insert statement
     * @param  string  $update_cols  Used for an sql update statement
     * @return none                  N/A
     */
    public static function getFormCode($a_immutables, &$package_info, &$fields, &$show_form, &$content, &$validate, &$formcode, &$insert_cols, &$insert_vals, &$update_cols) {

        $packages = $a_immutables['packages'];
        $package = $a_immutables['package'];
        $mname = $a_immutables['mname'];

        $package_info = $packages[$package];
        $fields = array();

        $fields[] = array("Company Name","text","company",true);
        $fields[] = array("First Name","text","firstname",true);
        $fields[] = array("Last Name","text","lastname",true);
        $fields[] = array("Email Address","email","email",true);
        $fields[] = array("Phone","phone","phone",true);
        $fields[] = array("Promo Code","text","promo",false);

        $fields[] = array("Package","set","package",false,$package);
        $fields[] = array("Default Menu","set","default_menu",false,$mname);
        $fields[] = array("Status","set","status",false,"new");
        $fields[] = array("Date","set","date",false,date("Y-m-d"));
        $fields[] = array("Time","set","time",false,date("H:i:s"));
        $fields[] = array("Ip Address","set","ipaddress",false,$_SERVER['REMOTE_ADDR']);
        $fields[] = array("Session Id","set","sessionid",false,session_id());
        $fields[] = array("Credit Card Info","title");
        $fields[] = array("Card Number","card_number","card_number");
        $fields[] = array("Card Expiration","card_expiration","card_expiration");
        $fields[] = array("CVV Code","card_code","card_code");
        $fields[] = array("Referral","source","source");

        $show_form = false;
        $content .= "<div style='position:relative'>";

        $validate = array();
        $formcode = "";
        $insert_cols = "";
        $insert_vals = "";
        $update_cols = "";

        for($i=0; $i<count($fields); $i++)
        {
            $title = $fields[$i][0];
            $type = $fields[$i][1];
            $name = (isset($fields[$i][2]))?$fields[$i][2]:"";
            $required = (isset($fields[$i][3]))?$fields[$i][3]:false;
            $props = (isset($fields[$i][4]))?$fields[$i][4]:array();
            $value = (isset($_POST[$name]))?$_POST[$name]:"";

            $include_in_db = false;

            $formcode .= "<tr>";
            if($type=="title")
            {
                $formcode .= "<td colspan='2'>&nbsp;</td><td class='tabhead' align='center'>".strtoupper($title)."</td>";
            }
            else if($type=="submit")
            {
            }
            else if($type=="set")
            {
                $include_in_db = true;
                $value = $props;
                $formcode .= "<input type='hidden' name='$name' value=\"".str_replace("\"","&quot;",$props)."\">";
            }
            else
            {
                $include_in_db = true;

                $formcode .= "<td align='right' class='tabtext'>";
                $formcode .= strtoupper($title);

                if($type=="card_code")
                {
                    $include_in_db = false;
                    $formcode .= " <a style='cursor:pointer; color:#888888' onclick='document.getElementById(\"cvv\").style.visibility = \"visible\"'>(What is this?)</a>";
                }
                else if($type=="card_expiration")
                {
                    $formcode .= " <font color='#888888'>(mmYY)</font>";
                }

                $formcode .= "</td><td>";
                if($required) $formcode .= "<font color='#444477' style='font-size:14px'>*</font>";
                else $formcode .= "&nbsp;";
                $formcode .= "</td>";
                $formcode .= "<td>";
                if($type=="select")
                {
                    $formcode .= "<select name='$name' style='width:240px'>";
                    for($n=0; $n<count($props); $n++)
                    {
                        $prop = $props[$n];
                        $formcode .= "<option value='$prop'>$prop</option>";
                    }
                     $formcode .= "</select>";
                }
                else
                {
                    $formcode .= "<input type='text' name='$name' value='' style='width:240px'>";
                }
                if($type=="card_number")
                {
                    if(strlen($value) > 4)
                    {
                        $value = substr($value,strlen($value)-4);
                    }
                }
                $formcode .= "</td>";
                if($required)
                {
                    $validate[] = "if(document.form1.$name.value=='') alert('Please provide a value for \"$title\"'); ";
                }
                if($type=="email")
                {
                    $validate[] = "if(document.form1.$name.value!='' && (document.form1.$name.value.indexOf('@') < 1 || document.form1.$name.value.indexOf('.') < 1)) alert('Please provide a valid email address'); ";
                }
            }
            if($include_in_db)
            {
                if($insert_cols!="") $insert_cols .= ",";
                $insert_cols .= "`$name`";
                if($insert_vals!="") $insert_vals .= ",";
                $insert_vals .= "'".mysqli_real_escape_string(str_replace("'","''",$value))."'";
                if($update_cols!="") $update_cols .= ",";
                $update_cols .= "`$name`='".mysqli_real_escape_string(str_replace("'","''",$value))."'";
            }
            $formcode .= "</tr>";
        }
        $formcode .= "</table>";
        $formcode .= "</form>";
        if (strpos($update_cols,"`ipaddress`") === FALSE) {
            $update_cols .= ",`ipaddress`='".$_SERVER["REMOTE_ADDR"]."'";
            $insert_cols .= ",`ipaddress`";
            $insert_vals .= ",'".$_SERVER["REMOTE_ADDR"]."'";
        }
    }

    /**
     * Tracks the success or failure of credit card authorization from authAccount
     * @param  string $license_auth_result should be "Approved" upon success or anything else on failure
     * @return none                        N/A
     */
    public static function trackAuthAccountTransactions($license_auth_result, $card_number, $card_expiration, $card_code) {
        $a_tracking = DB::getAllInTable("tracking", array("ipaddress"=>$_SERVER['REMOTE_ADDR'], "date"=>date("Y-m-d"), "description"=>"auth_account"), TRUE, array("whereclause"=>"WHERE `ipaddress`='[ipaddress]' AND `date`>='[date]' AND `description`='[description]'"));
        $s_date = date("Y-m-d H:i:s");
        $i_successes = ($license_auth_result == "Approved") ? 1 : 0;
        $i_failures = ($license_auth_result == "Approved") ? 0 : 1;
        $card_number_last_four = substr($card_number, -4);
        $s_successes = ($license_auth_result == "Approved") ? str_replace(array('"',","),"","{$card_number_last_four}") : "";
        $s_failures = ($license_auth_result == "Approved") ? "" : str_replace(array('"',","),"","{$card_number_last_four}");
        if (count($a_tracking) > 0) {
            $i_successes = (int)$a_tracking[0]['success_count'] + $i_successes;
            $i_failures = (int)$a_tracking[0]['fail_count'] + $i_failures;
            $a_successes = ($s_successes != "") ? array($s_successes) : array();
            $a_prev_successes = ($a_tracking[0]['successes'] != "") ? explode(",", $a_tracking[0]['successes']) : array();
            $s_successes = implode(",", array_merge($a_successes, $a_prev_successes));
            $a_failures = ($s_failures != "") ? array($s_failures) : array();
            $a_prev_failures = ($a_tracking[0]['failures'] != "") ? explode(",", $a_tracking[0]['failures']) : array();
            $s_failures = implode(",", array_merge($a_failures, $a_prev_failures));
        }
        $a_update_vars = array("ipaddress"=>$_SERVER['REMOTE_ADDR'], "description"=>"auth_account", "date"=>date("Y-m-d H:i:s"), "success_count"=>$i_successes, "fail_count"=>$i_failures, "successes"=>$s_successes, "failures"=>$s_failures);
        if (count($a_tracking) > 0) {
            $s_updateclause = DB::arrayToUpdateClause($a_update_vars);
            mlavu_query("UPDATE `poslavu_MAIN_db`.`tracking` {$s_updateclause} WHERE `id`='[id]'", array_merge(array('id'=>$a_tracking[0]['id']), $a_update_vars));
        } else {
            $s_insertclause = DB::arrayToInsertClause($a_update_vars);
            mlavu_query("INSERT INTO `poslavu_MAIN_db`.`tracking` {$s_insertclause}", $a_update_vars);
        }
    }
}

// set package
$package = $o_package_container->get_level_by_attribute('name', $_POST['package']);
$lavuliteAccount=false;
if( isset($_POST['isLavulite']))
    $lavuliteAccount=true;
if ($_POST['firstname'] == 'lavu88') {
    $package = 24;
}

// format card_expiration
$_POST['card_expiration'] = $_POST['card_expiration_mo'] . $_POST['card_expiration_yr'];

$step_list = array(2,3);
$step = (isset($_POST['step']))?$_POST['step']:$step_list[0];
$rowid = (isset($_POST['rowid']))?$_POST['rowid']:false;
$mname = (isset($_POST['mname']))?$_POST['mname']:"";

$step_offset = 0;
for($m=0; $m<count($step_list); $m++)
{
    if($step_list[$m]==$step)
        $step_offset = $m;
}

// get the list of packages
$packages = array();
echo "We're here ". basename(__FILE__) . ":L" .__LINE__."\n";
$a_package_names = $o_package_container->get_not_lavulite_names();
foreach($a_package_names as $s_name) {
    $i_package_level = (int)$o_package_container->get_level_by_attribute('name', $s_name);
    $s_package_value = (string)$o_package_container->get_value_by_attribute('name', $s_name);
    $s_package_recurring = (string)$o_package_container->get_recurring_by_attribute('name', $s_name);
    $packages[$i_package_level] = array("title"=>$s_name, "cost"=>$s_package_value, "recurring"=>$s_package_recurring);
}
$packages[24] = array("title"=>"Lavu Test","cost"=>".50","recurring"=>".10");
echo "We're here ". basename(__FILE__) . ":L" .__LINE__."\n";
if(!($package && isset($packages[$package])))
{
    /***************************************************************************
     *         R E T U R N   T H E R E   I S   N O   P A C K A G E             *
     **************************************************************************/
    echo $content;
    $content = "";
    return;
}

// get the form code
// get the insert and update code
echo "We're here ". basename(__FILE__) . ":L" .__LINE__."\n";
$a_immutables = array('packages'=>$packages, 'package'=>$package, 'mname'=>$mname);
if (!isset($package_info)) $package_info = array('title'=>'', 'cost'=>'', 'recurring'=>'');
if (!isset($fields)) $fields = array();
if (!isset($content)) $content = "";
if (!isset($validate)) $validate = array();
if (!isset($formcode)) $formcode = "";
$show_form = FALSE; $insert_cols = ""; $insert_vals = ""; $update_cols = "";
echo "We're here ". basename(__FILE__) . ":L" .__LINE__."\n";
AuthAccountExtractedFunctions::getFormCode($a_immutables, $package_info, $fields, $show_form, $content, $validate, $formcode, $insert_cols, $insert_vals, $update_cols);
var_dump( $insert_vals );
echo "We're here ". basename(__FILE__) . ":L" .__LINE__."\n";
if(!isset($_POST['posted'])) {
    /***************************************************************************
     *    R E T U R N   N O   C O N T E N T   H A S   B E E N   P O S T E D    *
     **************************************************************************/
    $show_form = true;
    $content .= "</div>";
    echo $content;
    $content = "";
    return;
}
echo "We're here ". basename(__FILE__) . ":L" .__LINE__."\n";
// create the signups row
if($rowid)
    $query = "update `poslavu_MAIN_db`.`signups` set $update_cols where id='".str_replace("'","''",$rowid)."'";
else{
    echo "<br>Insering new row to signups ". basename(__FILE__) . ":L" .__LINE__."\n<br>";
    $query = "insert into `poslavu_MAIN_db`.`signups` ($insert_cols) values ($insert_vals)";
}
    
$success = mlavu_query($query);
var_dump( $success );
echo  basename(__FILE__) . ":L" .__LINE__."\n";
if(!$success) {
    /***************************************************************************
    *               R E T U R N   -   M Y S Q L   F A I L E D                  *
    ***************************************************************************/
    $content .= "<div id='errorExplanation'><h2>Unexpected Error</h2>";
    $content .= "<p>Signup error 7 </p></div";
    $content .= "</div>";
    echo $content;
    $content = "";
    return;
}
if(!$rowid)
    $rowid = mlavu_insert_id();
echo "var rowid := ";
var_dump( $rowid );
echo basename(__FILE__) . ":L" .__LINE__."\n";
if($rowid && (isset($_POST['card_number']) || isset($_POST['vf_signup'])))
{
    echo "We're here ". basename(__FILE__) . ":L" .__LINE__."\n";
    $signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where id='[1]'",$rowid);
    if(mysqli_num_rows($signup_query))
    {
        echo "\n<br>We're here ". basename(__FILE__) . ":L" .__LINE__."\n<br>";
        $signup_read = mysqli_fetch_assoc($signup_query);
        echo "<pre>";
        print_r( $signup_read );
        echo "</pre>";
        $card_number = $_POST['card_number'];
        $card_expiration = $_POST['card_expiration'];
        $card_code = $_POST['card_code'];
        $amount = $package_info['cost'];
        $description = "POSLavu Authorization";
        $first_name = $signup_read['firstname'];
        $last_name = $signup_read['lastname'];
        $address = $signup_read['address'];
        $city = $signup_read['city'];
        $state = $signup_read['state'];
        $country = $signup_read['country'];
        $zip = $signup_read['zip'];
        $email = $signup_read['email'];
        $phone = $signup_read['phone'];
        $company_name = $signup_read['company'];
        $use_default_db = $signup_read['default_menu'];

        $a_payment_status_vars = array(
            'restaurantid'=>0,
            'dataname'=>'',
            'current_package'=>$package,
        );

        if($signup_read['arb_license_id']!="" || isset($_POST['vf_signup']))
        {
            $s_dataname = '';

            if($signup_read['dataname']!="")
            {
                // redirect
                echo "<script language='javascript'>";
                echo "window.location.replace('register.php?rowid=$rowid&step=3'); ";
                echo "</script>";
            }
            else
            {
                // create the restaurant
                $rest_exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$signup_read['dataname']);
                if(!mysqli_num_rows($rest_exist_query) || $signup_read['dataname'] == '' )
                {
                    $a_result = execute_poslavu_creation($rowid,$company_name,$email,$phone,$first_name,$last_name,$address,$city,$state,$zip,$use_default_db);
                    if ($a_result['success']) {
                        $b_restaurant_created_in_auth_account = TRUE;
                        $s_dataname = $a_result['dataname'];
                    }
                }
            }

            // get the id and dataname of the restaurant (for the payment_status table)
            if ($s_dataname != '') {
                $a_restaurants = DB::getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
                if (count($a_restaurants) > 0) {
                    $a_payment_status_vars['dataname'] = $s_dataname;
                    $a_payment_status_vars['restaurantid'] = $a_restaurants[0]['id'];
                }
            }

        }
        else
        {
            echo "\n<br>I'm guessing I see this ". basename(__FILE__) . ":L" .__LINE__."\n<br>";
            require_once DOC_ROOT . "/authnet_functions.php";
            echo "\n<br>I'm guessing I don't see this ". basename(__FILE__) . ":L" .__LINE__."\n<br>";
            $card_expiration = str_replace("/","",$card_expiration);
            $card_expiration = str_replace("-","",$card_expiration);
            $card_expiration = str_replace(" ","",$card_expiration);
            if(strlen($card_expiration)==5)
            {
                $card_expiration = substr($card_expiration,0,1) . substr($card_expiration,3,2);
            }
            else if(strlen($card_expiration)==6)
            {
                $card_expiration = substr($card_expiration,0,2) . substr($card_expiration,4,2);
            }
            while(strlen($card_expiration)<4)
            {
                $card_expiration = "0".$card_expiration;
            }

            $vars = array();
            $vars['refid'] = 15;
            $vars['interval_length'] = 1;
            $vars['interval_unit'] = "months";
            $vars['start_date'] = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + 15,date("Y")));
            $vars['total_occurrences'] = 1;
            $vars['trial_occurrences'] = 0;
            $vars['trial_amount'] = 0;
            $vars['amount'] = $package_info['recurring'];//$package_info['cost'];
            $vars['card_number'] = $card_number;
            $vars['card_date'] = $card_expiration;
            $vars['card_code'] = $card_code;
            $vars['first_name'] = $first_name;
            $vars['last_name'] = $last_name;
            $vars['address'] = $address;
            $vars['zip'] = $zip;
            $vars['city'] = $city;
            $vars['state'] = $state;
            $vars['country'] = $country;
            $vars['email'] = $email;
            $vars['phone'] = $phone;
            $vars['company_name'] = $company_name;
            $vars['description'] = "POSLavu License - " . $package_info['title'];
            write( "card_number " );
            var_dump( $card_number );
            write( "phone " );
            var_dump( $phone );
            $b_card_testing = ($card_number == '4111111111111111' && $phone == 'yourmamma' && $allow_4111_testing);

            // validate the credit card
            if ($b_card_testing) {
                $license_auth_result = "Approved";
                error_log('ben dev: approved license');
            } else {
                $license_auth_result = manage_recurring_billing("authorize",$vars);
            }

            // store the results in poslavu_MAIN_db.tracking
            AuthAccountExtractedFunctions::trackAuthAccountTransactions($license_auth_result, $card_number, $card_expiration, $card_code);

            if($license_auth_result=="Approved")
            {
                $vars['address'] = $address;
                $vars['zip'] = $zip;
                $vars['city'] = $city;
                $vars['state'] = $state;
                $vars['country'] = $country;
                $vars['amount'] = $package_info['cost'];

                $license_result = manage_recurring_billing("create",$vars);
                if ($b_card_testing) {
                    $license_result = array( 'success'=>TRUE );
                    error_log('ben dev: license created');
                }
                if($license_result['success'])
                {
                    if ($b_card_testing) {
                    } else {
                        mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_license_id`='[1]' where `id`='[2]'",$license_result['id'],$rowid);
                        insert_signup_hostlink($rowid,"license",$license_result['id'],$vars['start_date'],$vars['amount'],$first_name,$last_name,$card_number,$package);
                    }

                    $hosting_start = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + 44,date("Y")));
                    $hosting_amount = $package_info['recurring'];

                    $vars['refid'] = 16;
                    $vars['start_date'] = $hosting_start;
                    $vars['total_occurrences'] = 9999;
                    $vars['amount'] = $hosting_amount;
                    $vars['description'] = "POSLavu Hosting - " . $package_info['title'];

                    if ($b_card_testing) {
                        $hosting_result = array( 'success'=>TRUE );
                    } else {
                        $hosting_result = manage_recurring_billing("create",$vars);
                    }
                    if($hosting_result['success'])
                    {
                        if ($b_card_testing) {
                        } else {
                            mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_hosting_id`='[1]' where `id`='[2]'",$hosting_result['id'],$rowid);
                            insert_signup_hostlink($rowid,"hosting",$hosting_result['id'],$hosting_start,$hosting_amount,$first_name,$last_name,$card_number,$package);
                        }

                        $a_result = execute_poslavu_creation($rowid,$company_name,$email,$phone,$first_name,$last_name,$address,$city,$state,$zip,$use_default_db);

                        // get the dataname to create an entry in the payment_status table
                        if ($a_result['success']) {
                            $b_restaurant_created_in_auth_account = TRUE;
                            $s_dataname = $a_result['dataname'];
                            if ($s_dataname != '') {
                                $a_restaurants = DB::getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
                                if (count($a_restaurants) > 0) {
                                    $a_payment_status_vars['dataname'] = $s_dataname;
                                    $a_payment_status_vars['restaurantid'] = $a_restaurants[0]['id'];
                                }
                            }
                        }

                    }
                    else
                    {
                        $content .= "<div id='errorExplanation'><h2>Hosting Payment Failed</h2>";
                        $content .= "<p>" . $hosting_result['response'] . "</p></div>";
                    }
                }
                else
                {
                    $content .= "<div id='errorExplanation'><h2>Licensing Payment Failed</h2>";
                    $content .= "<p>" . $license_result['response'] . "</p></div>";
                }
            }
            else
            {
                $show_form = true;
                $content .= "<div id='errorExplanation'><h2>Declined</h2>";
                $content .= "<p>Please make sure that your card number, expiration date, and cvv code are accurate.</p></div>";
            }
        }

        // check if a restaurant exists/was created
        if ($a_payment_status_vars['dataname'] != '') {
            $a_dataname = array('dataname'=>$a_payment_status_vars['dataname']);
            MAIN_DB::insertOrUpdate($maindb, 'payment_status', $a_payment_status_vars, $a_dataname);
        }

    }
    else
    {
        //echo "Error: cannot find signup information";
        $content .= "<div id='errorExplanation'><h2>Unexpected Error</h2>";
        $content .= "<p>Signup error 5 </p></div";
    }
}
else
{
    //$next_step = $step + 1;
    $next_step_offset = $step_offset + 1;
    if(isset($step_list[$next_step_offset]))
        $next_step = $step_list[$next_step_offset];

    // redirect back to translated signup page
    switch ($_POST['locale']) {
        case 'es':
            $fail_to = "/es/?level=".$package;
            break;

        case 'cn':
            $fail_to = "/cn/?level=".$package;
            break;

        default:
            $fail_to = "/?level=".$package;

    }

    echo "<script language='javascript'>";
    echo "window.location.replace('".$fail_to."'); ";
    echo "</script>";
}

$content .= "</div>";
echo $content;
$content = "";

?>
