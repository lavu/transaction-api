<?php

if ( session_id() == '' ) $session_started = session_start();

$doc_root = $_SERVER['DOCUMENT_ROOT'];
$admin_dir = "{$doc_root}/../admin";
define( "DOC_ROOT", $doc_root );
define( "ADMIN_DIR", $admin_dir );

?>
<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
      <title>Lavu 88 - Please wait</title>
    </head>
    <body id="submit_signup" onload="">
      <p style="text-align:center">
          <img src="/images/_anim_spincircs_12_32_007sec.gif" 
          width="98%" alt="" id="wait-animation" />
      </p>
<?php
$action = "/lavu88/step2.php?failed=1";
if ( !empty( $_REQUEST['escape_ip_check'] ) ) {
    $action .= "&escape_ip_check=1";
}
?>
<form action="<?php echo $action; ?>" method="post" id="fallback">
<?php
foreach ($_REQUEST as $key => $value) {
echo "<input type=\"hidden\" name=\"$key\" value=\"$value\" id=\"$key\"/>\n";   
}
?>
</form>
<?php

/**
 * FUNCTIONS
 * I did't wrote most of this, so if you have real questions about this
 * ask Corey or you're shit outta luck
 */


/**
 * get the number of recent credit card authorizations from this ip address
 * @param  integer $i_days_past     How many days ago to start looking
 * @param  integer $i_days_from_now How many days from now to stop looking
 * @return integer                  Integer of number of recent signups upon success, FALSE if the query fails
 */
function getRecentCCs($i_days_past, $i_days_from_now) {

    // create the query
    $min_date_create = date("Y-m-d", strtotime("{$i_days_past} days"))." 00:00:00";
    $max_date_create = date("Y-m-d", strtotime("{$i_days_from_now} days"))." 00:00:00";
    $s_query_string = "SELECT SUM(`fail_count`) as `count` from `poslavu_MAIN_db`.`tracking` where `ipaddress`='[ip]' AND `date`>='[start_date]' AND `date`<='[end_date]'";
    $a_query_vars = array("ip"=>$_SERVER['REMOTE_ADDR'],"start_date"=>$min_date_create,"end_date"=>$max_date_create);
    $tracking_query = mlavu_query($s_query_string,$a_query_vars);

    // check that the query succeeded
    if ($tracking_query === FALSE) {
        foreach($a_query_vars as $k=>$v)
            $s_query_string = str_replace("[$k]", $v, $s_query_string);
        error_log("bad query: $s_query_string              ".__FILE__." ".__LINE__);
    }

    // get the created count from between yesterday and tomorrow for the remote ip address
    if($tracking_query !== FALSE && mysqli_num_rows($tracking_query))
    {
        $tracking_read = mysqli_fetch_assoc($tracking_query);
        $fail_count = $tracking_read['count'];
        return $fail_count;
    }
    return FALSE;
}

// creates a row in a table with matching array indices and table columns
// it does an insert/update with the new data, depending upon the values in $a_if_matching_update
// updates the global $i_reseller_leads_id
// returns true upon an insert or false for an update
function create_or_update_lead($a_lead_data, $a_if_matching_update) {
    global $maindb;

    // find any matches that exist
/*    $a_matches = MAIN_DB::findMatch('poslavu_MAIN_db', 'reseller_leads', array(
        'rules'=>array(
            array('colname'=>'session_id', 'match_value'=>$a_lead_data['session_id']),
            array('colname'=>'email', 'match_value'=>$a_lead_data['email'], 'match_degree'=>90),
            array('colname'=>'company_name', 'match_value'=>$a_lead_data['company_name'], 'match_degree'=>85),
        ),
    ));
    $i_match_id = -1;
    foreach($a_matches as $a_match) {
        $i_match_id = $a_match['id'];
        break;
    }
*/
    $i_match_id = -1;

    // find the columns to add to the db
    $a_table_columns = MAIN_DB::getTableColumns('poslavu_MAIN_db', 'reseller_leads');
    $a_lead_columns = array();
    foreach($a_lead_data as $k=>$v)
        $a_lead_columns[] = $k;
    $a_similar_columns_index_by_ints = array_intersect($a_lead_columns, $a_table_columns);
    $a_similar_columns = array();
    foreach($a_similar_columns_index_by_ints as $s_colname)
        $a_similar_columns[$s_colname] = $s_colname;

    // get the update/insert clause
    $a_query_vars = $a_lead_data;
    $b_do_insert = FALSE;
    if ($i_match_id > -1) {
        $s_update_clause = DB::arrayToUpdateClause($a_similar_columns);
        $a_wheres = array('id'=>$i_match_id);
        $s_where_clause = DB::arrayToWhereClause($a_wheres);
        $a_query_vars = array_merge($a_wheres, $a_query_vars);
        $s_query_string = "UPDATE `[maindb]`.`reseller_leads` $s_update_clause $s_where_clause";
    } else {
        $s_insert_clause = DB::arrayToInsertClause($a_similar_columns);
        $s_query_string = "INSERT INTO `[maindb]`.`reseller_leads` $s_insert_clause";
        $b_do_insert = TRUE;
    }
    $a_query_vars = array_merge(array('maindb'=>$maindb), $a_query_vars);

    // submit the query
    mlavu_query($s_query_string, $a_query_vars);
    if ($b_do_insert)
        $i_reseller_leads_id = (int)mlavu_insert_id();
    else
        $i_reseller_leads_id = $i_match_id;

    // send an email
    if ($b_do_insert) {}

    // return the id of the row inserted/updated
    return $i_reseller_leads_id;
}

/**
 * Dunno what this thing does, don't ask
 */
function draw_query_string($query_string, $a_input) {
    foreach($a_input as $k=>$v)
        $query_string = str_replace("[$k]", $v, $query_string);
    return $query_string;
}

/**
 * get the number of recent signups from this ip address
 * @param  integer $i_days_past     How many days ago to start looking
 * @param  integer $i_days_from_now How many days from now to stop looking
 * @return integer                  Integer of number of recent signups upon success, FALSE if the query fails
 */
function getRecentSignups($i_days_past, $i_days_from_now) {

    // create the query
    $min_date_create = date("Y-m-d", strtotime("{$i_days_past} days"))." 00:00:00";
    $max_date_create = date("Y-m-d", strtotime("{$i_days_from_now} days"))." 00:00:00";
    $s_query_string = "SELECT count(*) as `count` from `poslavu_MAIN_db`.`signups` left join `poslavu_MAIN_db`.`restaurants` on `restaurants`.`data_name`=`signups`.`dataname` where `signups`.`ipaddress`='[ip]' and `signups`.`dataname`!='' and `restaurants`.`created`>='[start_date]' and `restaurants`.`created`<='[end_date]'";
    $a_query_vars = array("ip"=>$_SERVER['REMOTE_ADDR'],"start_date"=>$min_date_create,"end_date"=>$max_date_create);
    $signups_created_query = mlavu_query($s_query_string,$a_query_vars);

    // check that the query succeeded
    if ($signups_created_query === FALSE) {
        foreach($a_query_vars as $k=>$v)
            $s_query_string = str_replace("[$k]", $v, $s_query_string);
        error_log("bad query: $s_query_string              ".__FILE__." ".__LINE__);
    }

    // get the created count from between yesterday and tomorrow for the remote ip address
    if($signups_created_query !== FALSE && mysqli_num_rows($signups_created_query))
    {
        $signups_created_read = mysqli_fetch_assoc($signups_created_query);
        $signup_count = $signups_created_read['count'];
        return $signup_count;
    }
    return FALSE;
}

function checkForRecentSignups() {
    $signup_count = getRecentSignups(-1, +1);
    if ($signup_count === FALSE) {
        $signup_count = 11; // If can't determine count then don't let continue
    }

    // failed, too many accounts created
    if($signup_count > 10 && (!isset($_GET["escape_ip_check"]) || $_GET["escape_ip_check"] != "1") && empty( $_SERVER['DEV']))
    {
        echo <<<HTML
<style type="text/css">
#ip-notice{
     max-width:520px;
     margin:20px auto;
     border:3px solid #CCC;
     -webkit-border-radius:20px;
     -moz-border-radius:20px;
     border-radius:20px;
     padding:5px 20px 20px 20px;
     font-family:Arial, sans-serif;
}
.center {
    text-align:center;
}

#ip-notice>p:first-child{
    margin-bottom:2.5em;
}

#wait-animation{
    display:none;
}
</style>
   <div id="ip-notice">
   <p class="center"><img src="/images/lavu-logo.png" /></p>
   <p>Sorry, we are unable to create more accounts for you at this time. Please contact us (855-767-5288) if you would like additional accounts.</p>
HTML;
        $a_postvars = array();
        foreach($_REQUEST as $k=>$v) {
            $a_postvars[] = "<input type='hidden' name='{$k}' value='{$v}'></input>";
        }
        $a_getvars = array("escape_ip_check=1");
        foreach($_REQUEST as $k=>$v) {
            $a_getvars[] = "{$k}={$v}";
        }
        $s_getvars = (count($a_getvars) > 0) ? "?".implode("&",$a_getvars) : "";
        //echo "<form method='POST' action='step2{$GLOBALS['s_dev']}.php{$s_getvars}'>".implode("", $a_postvars)."<p class='center'><input type='submit' value='Escape IP Check'></input></p></form>";
        echo "</div>";
        exit();
    }
}

function sendActivationEmail($email_to, $shifted_signups_id, $shifted_reseller_leads_id, $verification_token)
{
    global $activation_link_host;
    $activation_link_host = DEV ? $_SERVER['HTTP_HOST'] : 'register.poslavu.com';

    if (DEV) error_log( "DEBUG: http://{$activation_link_host}/activate-signup/{$shifted_signups_id}/{$shifted_reseller_leads_id}/{$verification_token}" );

    $sender = 'newaccount@lavu.com';
    $sender_name = 'Lavu Inc.';
    $s_subject = 'Activate Your Lavu POS Account';
    $html_message = '';
    $message = '';

    ob_start();
    include 'views/emails/activation-signup-email.html.php';
    $html_message = ob_get_clean();

    include_once 'mailer.php';

    ob_start();
    include 'views/emails/activation-signup-email.txt.php';
    $message = ob_get_clean();

    return send_multipart_email( array($sender => $sender_name), $s_subject, $html_message, $message, $email_to );
}

function write( $str ) {
    $r =  $str . "<br>";
    return;
}


/**
 * "MAIN" CODE
 * most of it is recycled/repurposed/clean up code from the register page
 */

require_once "{$admin_dir}/cp/resources/core_functions.php";
require_once "{$admin_dir}/cp/resources/lavuquery.php";
require_once "{$admin_dir}/cp/objects/dbFunctions/DB.php";
require_once "{$admin_dir}/manage/globals/email_addresses.php";
require_once "{$admin_dir}/sa_cp/billing/package_levels_object.php";
require_once "{$admin_dir}/sa_cp/billing/procareas/SalesforceIntegration.php";
require_once "{$doc_root}/signup_shared.php";
require_once "{$doc_root}/lib/ReCaptcha/ReCaptcha.php";
require_once "{$doc_root}/lib/ReCaptcha/RequestMethod.php";
require_once "{$doc_root}/lib/ReCaptcha/RequestParameters.php";
require_once "{$doc_root}/lib/ReCaptcha/Response.php";
require_once "{$doc_root}/lib/ReCaptcha/RequestMethod/Post.php";
require_once "{$doc_root}/lib/ReCaptcha/RequestMethod/Socket.php";
require_once "{$doc_root}/lib/ReCaptcha/RequestMethod/SocketPost.php";

global $o_emailAddressesMap;

$maindb = !isset($maindb)? "poslavu_MAIN_db": $maindb;

// load package container, this is a PHP object that contains all
// the information about the packages and their respective pricing schemes
// this one is used to make sure that all the pricing info is consistent
$o_package_container = ( !isset( $o_package_container ) ) ? 
    new package_container() : $o_package_container;

// self-explanatory
checkForRecentSignups();
$b_weekly_signups = (getRecentSignups(-7, +1) > 0);
$b_failed_ccs = (getRecentCCs(-7, +1) > 0);

$_POST['package'] = $_POST['level'];
$_POST['posted'] = 1;
$_POST['company'] = empty( $_POST['company'] )? $_POST['company_name']: $_POST['company'];  // there are some inconsistencies between using company or company_name so we set both just be safe
$_POST['referring_site'] = $_REQUEST['referring_site'];  // TO DO : wrap with isset()'s
$_POST['utm_campaign'] = $_REQUEST['utm_campaign'];
$_POST['utm_source'] = $_REQUEST['utm_source'];
$_POST['utm_medium'] = $_REQUEST['utm_medium'];
$_POST['lead_source'] = isset($_POST['lead_source']) ? $_POST['lead_source'] . ' nocc signup' : 'lavu.com nocc signup';  // lead_source is set dynamically by lavu.com's global.js file
$a_lead_data = $_POST;

// This piece of code below is rather verbose, but all that it does is to make
// sure the starter menu is set and valid
$a_lead_data['session_id'] = session_id();
if ( isset($a_lead_data['mname'] ) ) {
    $a_valid_mnames = $o_signup->getValidMenuNames();
    if (!in_array($a_lead_data['mname'], $a_valid_mnames)) {
        $o_emailAddressesMap->sendEmailToAddress('billing_errors@poslavu.com', 'bad menu name (mname) for lead', 'A lead tried to access '.__FILE__.' with the mname '.$a_lead_data['mname'].'.'."\n\nData:\n".print_r($_REQUEST,TRUE));
        $a_lead_data['mname'] = 'default_restau';
        $_POST['mname'] = 'default_restau';
        $_POST['default_menu'] = 'default_restau';
    }
}

$b_finished_signup = false;
$recaptcha = new \ReCaptcha\ReCaptcha('6LexqAMTAAAAANOOo1ZCYJY5pIEhEr-XyRMCJy3u');
$recaptcha_response = $recaptcha->verify( $_REQUEST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR'] );

if ( $recaptcha_response->isSuccess() )
{
    // call auth_account, if all goes well it will print the javascript redirect to 
    // go to the approved page
    ob_start();
    require "$doc_root/auth_account.php";
    $error_content = ob_get_contents();
    ob_end_clean();
    echo $error_content;
    $b_finished_signup = true;

    // create the lead
    $i_reseller_leads_id = 0;
    $s_lead_source = $_POST['lead_source'];
    if (isset($_COOKIE['TryMercury'])) $s_lead_source = 'mercury';
    $a_lead_data = array_merge(array('lead_source'=>$s_lead_source, 'created'=>date('Y-m-d H:i:s'), 'demo_type'=>'signups'), $a_lead_data);
    $a_lead_data['signupid'] = $rowid;
    if (!isset($_REQUEST['no_lead']) || $_REQUEST['no_lead'] != '1') {
        $i_reseller_leads_id = create_or_update_lead( $a_lead_data, array(
            array('column'=>'session_id'),
            array('column'=>'email', 'match_percent'=>90),
            array('column'=>'company_name', 'match_percent'=>85))
        );
    }

    // send the activation email
    $shifted_signups_id = ($rowid + 12500);
    $shifted_reseller_leads_id = ($i_reseller_leads_id + 12500);
    $verification_token = substr(session_id(), 0, 8);
    sendActivationEmail($_REQUEST['email'], $shifted_signups_id, $shifted_reseller_leads_id, $verification_token, $i_reseller_leads_id);

    $redirect_to = "activation-signup/?id={$shifted_signups_id}";
}
else
{
    echo "Failed";
    $error_msg = __FILE__ ." recaptcha response failed for IP address ". $_SERVER['REMOTE_ADDR'] .": ". print_r($recaptcha_response->getErrorCodes(), true);
    error_log($error_msg);
    $redirect_to = $_SERVER['HTTP_REFERER'];
}

echo <<<JS
<script language="javascript">window.location.replace("{$redirect_to}");
</script>
JS;

?>
</body></html>