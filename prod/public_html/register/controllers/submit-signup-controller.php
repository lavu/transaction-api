<?php 

function submit_signup() {
    $html_buffer = "";
    $fingerprint = md5( "lentil". time() );
    set( "fingerprint",  $fingerprint );
    $_SESSION[$fingerprint] = array();
    foreach( $_REQUEST as $k=>$v ){
        $_SESSION[$fingerprint][$k] = $v;
        $html_buffer .= <<<HTML
\n<input type="hidden" name="$k" value="$v"/>
HTML;
    }
    if ( !empty( $_SESSION['salesforce_properties'] ) ){
        $_SESSION[$fingerprint]['salesforce_properties'] = $_SESSION['salesforce_properties'];
    }
    if( !empty( $_REQUEST['rollback_url'] ) ) {
        set( "action", $_REQUEST['rollback_url']);
    } else {
        set("action", $_SERVER['HTTP_REFERER']);
    }
    $_SESSION[$fingerprint]['status'] = "inprogress";
    return html( $html_buffer, "layouts/submit-signup.html.php" );
}

function check_submission_status () {
    if ( !empty( $_SESSION[params(0)] ) ) {
        $status = !empty($_SESSION[params(0)]['status']) ? $_SESSION[params(0)]['status'] : "failed";
        $response = array( "status" => $status );
        if ( ( $status === "success" || $status === "failed") &&  !empty( $_SESSION[params(0)]['rowid'] ) ) {
            $response['rowid'] = $_SESSION[params(0)]['rowid'];
        }
        return json( $response );
    }
}

// Most of the initial $8 hosting payments for 3mo8 people get posted back to our servers from Authorize.net before
// we've created a dataname for the account so the payments get associated with an account with a blank dataname (rest
// ID 4495, formerly Tipsy Coffee House) so we have to look for the one with our signup ID in the description and re-run
// the presponse_functions.php function that assigns the payment to its correct account.
function connect3mo8Payment($signup_id) {
    $result = mlavu_query("SELECT `id` FROM `payment_responses` WHERE `match_restaurantid` = '4495' AND `description` = 'PosLavu Auth S[signup_id]'", array('signup_id' => $signup_id));
    if ($result === false) return false;
    $payment_response = mysqli_fetch_assoc($result);
    require_once('/home/poslavu/public_html/admin/sa_cp/presponse_functions.php');
    return connect_response_to_restaurant($payment_response['id']);
}

function deferred_submit() {
    ignore_user_abort(true);
    set_time_limit(0);
    // global created in the 'before' function
    global $o_emailAddressesMap;
    global $o_package_container;
    global $maindb;
    global $o_signup;
    global $allow_4111_testing;
    global $data;
    global $is_lavu88_3mo8;
    require option('lib_dir') . '/submit/auth-account-functions.php';

    checkForRecentSignups();
    $b_weekly_signups = (getRecentSignups(-7, +1) > 0);
    $b_failed_ccs = (getRecentCCs(-7, +1) > 0);
    //copy data from session...
    $data = $_SESSION[params(0)];
    // ...and destroy array
    unset($_SESSION[params(0)]);
    // reallocate the array to contain status updates

    $_SESSION[params(0)] =  array( "status" => "inprogress" );
    session_write_close();
    if ( empty( $data['firstname'] ) || empty($data['lastname']) ) {
        if ( !empty( $data['full_name']) ) {
            $names = split_name( $data['full_name'] );
        } elseif( !empty( $data['name']) ) {
            $names = split_name( $data['name'] );
        }
        if ( !empty( $names ) ) {
            $data['firstname'] = $names[0];
            $data['lastname'] = $names[1];
        }
    }
    // This piece of code below is rather verbose, but all that it does is to make
// sure the starter menu is set and valid
    $valid_sources = array( "staples_lavu88", "newegg_lavu88", "other_lavu88");
    $is_cardless_signup = !empty( $data['source'] ) && in_array( $data['source'], $valid_sources );
    $is_lavu88_3mo8 = !empty( $data['source'] ) && strpos($data['source'], '_3mo8') !== false;
    if ( !$is_cardless_signup) {
        require_once option('root_dir') . "/recaptcha/recaptchalib.php";
        $privatekey = DEV ? "/recaptcha/privatekey_dev.txt": "/recaptcha/privatekey.txt";
        $privatekey = file_get_contents(option('root_dir')  . $privatekey );
        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $data["recaptcha_challenge_field"], $data["recaptcha_response_field"]);
        if ( !$resp->is_valid ) {
            session_start();
            set_rollback_error( "Wrong Recaptcha", "Try again" );
            $content = "sections/js-rollback.html.php";
            $_SESSION[params(0)]['status'] = "failed";
            return js( $content, null );
        }
    }


    if ( empty( $data['company'] ) ) {
        $data['company'] = $data['company_name'];
    }
    $data['session_id'] = session_id();
    if ( isset($data['mname'] ) ) {
        $a_valid_mnames = $o_signup->getValidMenuNames();
        if (!in_array($data['mname'], $a_valid_mnames) && !DEV) {
            $o_emailAddressesMap->sendEmailToAddress('billing_errors@poslavu.com', 'bad menu name (mname) for lead', 'A lead tried to access '.__FILE__.' with the mname '.$data['mname'].'.'."\n\nData:\n".print_r($data,TRUE));
            $data['mname'] = 'default_restau';
        }
    }
    // set package
    $package = null;
    if ( !empty( $data['package'] ) ){
        $package = $o_package_container->get_level_by_attribute('name', $data['package']);
    }
    $lavuliteAccount = false;
    if (isset($data['isLavulite']))
        $lavuliteAccount = true;

    // format card_expiration
    if ( !empty( $data['card_expiration_mo'] ) && !empty( $data['card_expiration_yr'] ) ) {
        $data['card_expiration'] = $data['card_expiration_mo'] . $data['card_expiration_yr'];
    }

    $step_list = array(2, 3);
    $step = (isset($data['step'])) ? $data['step'] : $step_list[0];
    $rowid = (isset($data['rowid'])) ?  $data['rowid']  : false;
    $mname = (isset($data['mname'])) ? $data['mname'] : "";

    $step_offset = 0;
    for ($m = 0; $m < count($step_list); $m++) {
        if ($step_list[$m] == $step)
            $step_offset = $m;
    }
    // get the list of packages
    $a_highest_package_levels = $o_package_container->get_highest_base_package_names();
    $level = ucfirst( $data['level'] );
    if ( $level === "Pro" ) $level = "Platinum";
    $package_levels = array('Silver','Gold','Platinum');
    if ( $mname  === "lavu_fit_demo" ){
        $package_levels = array('FitSilver','FitGold','FitPro');
    }
    if (in_array($level, $package_levels )) {
        $new_level = $level .'2';
        $a_highest_package_levels = $o_package_container->get_highest_base_package_names();
        $new_level = "";
        foreach($a_highest_package_levels as $s_package_level) {
            if (strpos($s_package_level, $level) === 0) {
                $new_level = $s_package_level;
                break;
            }
        }
        $data['level'] = $new_level;
    } else if (!in_array($level, $a_highest_package_levels)) {
        error_log($data['level']);
    }
    $packages = array();
    $a_package_names = $o_package_container->get_not_lavulite_names();

    foreach ($a_package_names as $s_name) {
        $i_package_level = (int)$o_package_container -> get_level_by_attribute('name', $s_name);
        $s_package_value = (string)$o_package_container -> get_value_by_attribute('name', $s_name);
        $s_package_recurring = (string)$o_package_container -> get_recurring_by_attribute('name', $s_name);
        $packages[$i_package_level] = array("title" => $s_name, "cost" => $s_package_value, "recurring" => $s_package_recurring);
    }
    $package = $o_package_container->get_level_by_attribute('name', ucfirst($data['level']) );
    if (!($package && isset($packages[$package]))) {
        /***************************************************************************
         *         R E T U R N   T H E R E   I S   N O   P A C K A G E             *
         **************************************************************************/
        session_start();
        set_rollback_error( "Unexpected Error", "Starter package not valid" );
        $content = "sections/js-rollback.html.php";
        $_SESSION[params(0)]['status'] = "failed";
        return js( $content, null );
    }

    // get the form code
    // get the insert and update code
    $a_immutables = array('packages' => $packages, 'package' => $package, 'mname' => $mname);
    if (!isset($package_info))
        $package_info = array('title' => '', 'cost' => '', 'recurring' => '');
    if (!isset($fields))
        $fields = array();
    if (!isset($content))
        $content = "";
    if (!isset($validate))
        $validate = array();
    if (!isset($formcode))
        $formcode = "";
    $show_form = FALSE;
    $insert_cols = "";
    $insert_vals = "";
    $update_cols = "";
    AuthAccountExtractedFunctions::getFormCode($a_immutables, $package_info, $fields, $show_form, $content, $validate, $formcode, $insert_cols, $insert_vals, $update_cols);
    if ( !empty( $data['salesforce_id'] ) ) {
        $insert_cols .= ",`salesforce_id`";
        $insert_vals .= ",'{$data['salesforce_id']}'";
        $update_cols .= ", `salesforce_id`='{$data['salesforce_id']}'";
    }
    // create the signups row
    if ($rowid) {
        $query = "update `poslavu_MAIN_db`.`signups` set $update_cols where id='" . str_replace("'", "''", $rowid) . "'";
    } else {
        $query = "insert into `poslavu_MAIN_db`.`signups` ($insert_cols) values ($insert_vals)";
    }
    set("rowid", $rowid);
    $success = mlavu_query($query);
    if (!$success) {
        /***************************************************************************
         *               R E T U R N   -   M Y S Q L   F A I L E D                  *
         ***************************************************************************/
        session_start();
        set_rollback_error("Unexpected Error", "Signup error 7");
        $content = "sections/js-rollback.html.php";
        $_SESSION[params(0)]['status'] = "failed";
        $_SESSION[params(0)]['rowid'] = $rowid;
        return js( $content, null );
    }
    if (!$rowid) {
        $rowid = mlavu_insert_id();
        set("rowid", $rowid);
    }
    if ($rowid && (
            $is_cardless_signup ||
            !empty( $data['card_number']) ||
            !empty( $data['vf_signup'] )
        ) ) {
        $signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where id='[1]'", $rowid);
        if (mysqli_num_rows($signup_query)) {
            $signup_read = mysqli_fetch_assoc($signup_query);
            $card_number = "0000000000000000";
            $card_expiration = "00";
            $card_code = "0000";
            if ( !$is_cardless_signup ) {
                $card_number = $data['card_number'];
                $card_expiration = $data['card_expiration'];
                $card_code = $data['card_code'];
            }

            $amount = $package_info['cost'];
            $description = "POSLavu Authorization";
            $first_name = $signup_read['firstname'];
            $last_name = $signup_read['lastname'];
            $address = $signup_read['address'];
            $city = $signup_read['city'];
            $state = $signup_read['state'];
            $country = $signup_read['country'];
            $zip = $signup_read['zip'];
            $email = $signup_read['email'];
            $phone = $signup_read['phone'];
            $company_name = $signup_read['company'];
            $use_default_db = $signup_read['default_menu'];

            $a_payment_status_vars = array('restaurantid' => 0, 'dataname' => '', 'current_package' => $package );

            if ($signup_read['arb_license_id'] != "" || !empty($data['vf_signup'])) {
                $s_dataname = '';

                if ($signup_read['dataname'] != "") {
                    // This script doesn't exist, so I guess I can ust throw an
                    // error of some sort
                    // redirect
                    // echo "<script language='javascript'>";
                    // echo "window.location.replace('register.php?rowid=$rowid&step=3'); ";
                    // echo "</script>";
                    set( "error", "no-dataname");
                } else {
                    // create the restaurant
                    $rest_exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'", $signup_read['dataname']);
                    if (!mysqli_num_rows($rest_exist_query) || $signup_read['dataname'] == '') {
                        $a_result = execute_poslavu_creation($rowid, $company_name, $email, $phone, $first_name, $last_name, $address, $city, $state, $zip, $use_default_db);
                        if ($a_result['success']) {
                            $b_restaurant_created_in_auth_account = TRUE;
                            $s_dataname = $a_result['dataname'];
                            $rowid_offset = $rowid + 12500;
                            $content = "location.href=\"/approved.php?id=$rowid_offset\";";
                        }
                    }
                }

                // get the id and dataname of the restaurant (for the payment_status table)
                if ($s_dataname != '') {
                    $a_restaurants = DB::getAllInTable('restaurants', array('data_name' => $s_dataname), TRUE);
                    if (count($a_restaurants) > 0) {
                        $a_payment_status_vars['dataname'] = $s_dataname;
                        $a_payment_status_vars['restaurantid'] = $a_restaurants[0]['id'];
                    }
                }

            } else {
                require_once( "/home/poslavu/public_html/admin/sa_cp/billing/authnet_functions.php");  // Don't check this reference to the LavuBackend into git; this is just for local devservers!
                $vars = array();
                if (!$is_cardless_signup){
                    $card_expiration = str_replace("/", "", $card_expiration);
                    $card_expiration = str_replace("-", "", $card_expiration);
                    $card_expiration = str_replace(" ", "", $card_expiration);
                    if (strlen($card_expiration) == 5) {
                        $card_expiration = substr($card_expiration, 0, 1) . substr($card_expiration, 3, 2);
                    } else if (strlen($card_expiration) == 6) {
                        $card_expiration = substr($card_expiration, 0, 2) . substr($card_expiration, 4, 2);
                    }
                    while (strlen($card_expiration) < 4) {
                        $card_expiration = "0" . $card_expiration;
                    }
                    $vars['card_number'] = $card_number;
                    $vars['card_date'] = $card_expiration;
                    $vars['card_code'] = $card_code;
                }


                $vars['refid'] = 15;
                $vars['interval_length'] = 1;
                $vars['interval_unit'] = "months";
                $vars['start_date'] = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + 15, date("Y")));
                $vars['total_occurrences'] = 1;
                $vars['trial_occurrences'] = 0;
                $vars['trial_amount'] = 0;
                $vars['amount'] = $package_info['recurring'];
                //$package_info['cost'];

                $vars['first_name'] = $first_name;
                $vars['last_name'] = $last_name;
                $vars['address'] = $address;
                $vars['zip'] = $zip;
                $vars['city'] = $city;
                $vars['state'] = $state;
                $vars['country'] = $country;
                $vars['email'] = $email;
                $vars['phone'] = $phone;
                if ( !empty($data['source']) ){
                    $vars['source'] = $data['source'];
                }
                $vars['company_name'] = $company_name;
                $vars['description'] = "POSLavu License - " . $package_info['title'];
                //$b_card_testing = ( $is_lavu88_3mo8 || ($card_number == '4111111111111111' && $phone == 'yourmamma' && $allow_4111_testing ));
                $b_card_testing = ($card_number == '4111111111111111' && $phone == 'yourmamma' && $allow_4111_testing );
                // validate the credit card
                if ($b_card_testing  || $is_cardless_signup  ) {
                    $license_auth_result = "Approved";
                    error_log( $is_cardless_signup?
                        "DEV::Cardless signup" :
                        'dev: approved license');
                } elseif ( $is_lavu88_3mo8 ) {
                    $vars['amount'] = "8.00";
                    $vars['description'] = "POSLavu Auth S" . $rowid;
                    $license_auth_result = manage_recurring_billing("charge", $vars);
                } else {
                    $license_auth_result = manage_recurring_billing("authorize", $vars);
                }

                // store the results in poslavu_MAIN_db.tracking
                AuthAccountExtractedFunctions::trackAuthAccountTransactions($license_auth_result, $card_number, $card_expiration, $card_code);
                //print_r($license_auth_result);
                if ($license_auth_result == "Approved") {
                    $vars['amount'] = $package_info['cost'];
                    // Check promo code and adjust license cost, if applicable.
                    $used_promo_code = false;
                    $bluestar_activation = false;
                    if ( !empty($data['promo']) ) {
                        if ( !empty( $s_dataname ) ) $s_dataname = "";
                        $promo_code_results = process_promo_code($data['promo'], $vars, $package, $rowid, $s_dataname);  // $s_dataname usually isn't populated here for new signups
                        $used_promo_code = $promo_code_results['code_accepted'];
                        $bluestar_activation = (strstr($promo_code_results['promo_name'], 'bluestar') !== false);
                    }

                    /**
                     * THIS IS FOR THE LICENSE ARB
                     */
                    // Avoid submitting an authorization for $0 for licenses (since that will always result in a failed cc txn) but allow flow to continue.
                    if ($vars['amount'] * 1 == 0)
                    {
                        //$vars['amount'] = 0.01;
                        $hosting_start_offset = 14;
                        if ( $is_cardless_signup || $is_lavu88_3mo8 ) {
                            $hosting_start_offset = 30;
                        }elseif ($used_promo_code && $bluestar_activation) {
                            $hosting_start_offset = 90;
                        }
                        $license_result = array( 'success'=>TRUE );
                        if ( !defined('LAVU_TRIAL_DAYS') ) {
                            define('LAVU_TRIAL_DAYS', $hosting_start_offset );
                        }
                    } else {
                        $hosting_start_offset = 44;
                        if ($b_card_testing || $is_cardless_signup) {
                            $license_result = array( 'success'=> TRUE );
                            error_log('carlos dev: license created');
                        } else {
                            $license_result = manage_recurring_billing("create",$vars);
                        }
                    }
                    if ($license_result['success']) {
                        if ($b_card_testing || $is_cardless_signup ) {
                        } elseif( !empty( $license_result['id'] ) ) {
                            mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_license_id`='[1]' where `id`='[2]'", $license_result['id'], $rowid);
                            if ( DEV ) {
                                $dev_msg = set("dev");
                                if ( empty( $dev_msg ) ) {
                                    $dev_msg = array("license_result" => $license_result);
                                } elseif( is_array( $dev_msg ) ){
                                    $dev_msg["license_result" ] = $license_result;
                                } elseif( is_string( $dev_msg )){
                                    $dev_msg .= "license_result::" . print_r($license_result,true);
                                }
                                set( "dev", $dev_msg );
                            }
                            insert_signup_hostlink($rowid, "license", $license_result['id'], $vars['start_date'], $vars['amount'], $first_name, $last_name, $card_number, $package);
                        }

                        /**
                         * THIS IS FOR THE HOSTING ARB
                         */



                        $hosting_start = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $hosting_start_offset, date("Y")));
                        $hosting_amount = $package_info['recurring'];

                        $vars['refid'] = 16;
                        $vars['start_date'] = $hosting_start;
                        $vars['total_occurrences'] = 9999;
                        $vars['amount'] = $hosting_amount;

                        $vars['description'] = "POS Lavu Hosting - " . $package_info['title'];
                        if ( $is_lavu88_3mo8 ) {
                            $vars['trial_amount'] = 8;
                            $vars['trial_occurrences'] = 2;
                        }
                        if ($b_card_testing || $is_cardless_signup) {
                            $hosting_result = array('success' => TRUE);
                        } else {
                            $hosting_result = manage_recurring_billing("create", $vars);
                        }
                        if ($hosting_result['success']) {
                            if ( $b_card_testing || $is_cardless_signup ) {
                            } else {
                                mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_hosting_id`='[1]' where `id`='[2]'", $hosting_result['id'], $rowid);
                                insert_signup_hostlink($rowid, "hosting", $hosting_result['id'], $hosting_start, $hosting_amount, $first_name, $last_name, $card_number, $package);
                            }

                            $a_result = execute_poslavu_creation($rowid, $company_name, $email, $phone, $first_name, $last_name, $address, $city, $state, $zip, $use_default_db );

                            // get the dataname to create an entry in the payment_status table
                            if ($a_result['success']) {
                                $b_restaurant_created_in_auth_account = TRUE;
                                $s_dataname = $a_result['dataname'];
                                if ($s_dataname != '') {
                                    $a_restaurants = DB::getAllInTable('restaurants', array('data_name' => $s_dataname), TRUE);
                                    if (count($a_restaurants) > 0) {
                                        $a_payment_status_vars['dataname'] = $s_dataname;
                                        $a_payment_status_vars['restaurantid'] = $a_restaurants[0]['id'];
                                    }
                                }
				// Associate initial 3mo8 payment with restaurant, now that we have a dataname.
				if ( $is_lavu88_3mo8 ) {
					connect3mo8Payment($rowid);
				}
                                // Handle incoming lead
                                $a_lead_data = $data;
                                if ( $is_lavu88_3mo8 && preg_match("/(.+?)_lavu88/",$data['source'], $matches ) && empty($data['lead_source'])) {
                                    $lead_source = "Get-Lavu-".ucfirst( $matches[1] );
                                    connect3mo8Payment($rowid);
                                } else {
                                    $lead_source = $data['lead_source'];
                                }
                                $a_lead_data['lead_source'] = $lead_source;
                                $a_lead_data['data_name'] = $s_dataname;
                                $a_lead_data['made_demo_account'] = date('Y-m-d H:i:s');
                                $a_lead_data['salesforce_sent'] = $a_lead_data['made_demo_account'];
                                $b_new_lead = create_or_update_lead( $a_lead_data, array(
                                        array('column'=>'session_id'),
                                        array('column'=>'email', 'match_percent'=>90),
                                        array('column'=>'company_name', 'match_percent'=>85))
                                );
                                if ( !empty( $b_new_lead ) ) {
                                    $file_dir = dirname(__FILE__);
                                    require_once "$file_dir/../../admin/sa_cp/billing/procareas/SalesforceIntegration.php";
                                    $account_info = SalesforceIntegration::getAccountInfo( $s_dataname );
                                    $names = split_name( $data['full_name'] );
                                    $salesforce_data =  array_merge( $account_info, array(
                                        "company_name" => $data['company_name'],
                                        "zip" => $data['zip'],
                                        "address" => $data['address'],
                                        "country" => $data['country'],
                                        "state" => $data['state'],
                                        "city" => $data["city"],
                                        "Type" => "Customer",
                                        "Restaurant_ID" => $a_restaurants[0]['id'],
                                        "Stage" => $is_cardless_signup ? "Trial - No CC" :"Trial - CC",
                                        "LeadSource" => $a_lead_data['lead_source'],
                                        "utm_campaign" => empty( $data['utm_campaign'] ) ? "" : $data['utm_campaign'],
                                        "utm_source" => empty( $data['utm_source'] ) ? "" : $data['utm_source'],
                                        "utm_medium" => empty( $data['utm_source'] ) ? "" : $data['utm_medium'],
                                        "referer" => empty( $data['referer'] ) ? "" : $data['referer'],
                                        "entry_url" => empty( $data['entry_url'] ) ? "" : $data['entry_url'],
                                        "FirstName" => $data['firstname'],
                                        "LastName" => $data['lastname'],
                                        "Email" => $data['email'],
                                        "Phone" => $data['phone'],
                                        "lead_id" => $b_new_lead,
                                        "data_name" => $s_dataname
                                    ) );
                                    if ( !empty( $data['promo'] ) ) {
                                        $salesforce_data["Promo_Code"] = $data['promo'];
                                    }
                                    $salesforce_lead_status =  send_to_salesforce( $salesforce_data );
                                    if( !empty( $salesforce_lead_status['id'] ) ) {
                                        mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `salesforce_id`='[1]' WHERE `id`=[2]",
                                            $salesforce_lead_status['id'], $b_new_lead );
                                        mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `salesforce_id`='[1]' WHERE `id`=[2]",
                                            $salesforce_lead_status['id'], $rowid );
                                    }
                                }
                                // render the successful redirect
                                session_start();
                                set('success_params', $a_result );
                                $rowid_offset = $rowid + 12500;
                                $content = "location.href=\"/approved.php?id=$rowid_offset\";";
                                $_SESSION[params(0)]["status"] = "success";
                                $_SESSION[params(0)]["rowid"] = $rowid_offset;
                            }
                        } else {
                            session_start();
                            set_rollback_error( "Hosting Payment Failed", $hosting_result['response'] );
                            $content = "sections/js-rollback.html.php";
                            $_SESSION[params(0)]["status"] = "failed";
                            $_SESSION[params(0)]['rowid'] = $rowid;
                        }
                    } else {
                        session_start();
                        set_rollback_error( "Licensing Payment Failed", $license_result['response'] );
                        $content = "sections/js-rollback.html.php";
                        $_SESSION[params(0)]["status"] = "failed";
                        $_SESSION[params(0)]['rowid'] = $rowid;
                    }
                } else {
                    //$show_form = true;
                    session_start();
                    set_rollback_error( "Declined", "Please make sure that your card number, expiration date, and cvv code are accurate." );
                    $content = 'sections/js-rollback.html.php';
                    $_SESSION[params(0)]["status"] = "failed";
                    $_SESSION[params(0)]['rowid'] = $rowid;
                    //$content .= "<div id='errorExplanation'><h2>Declined</h2>";
                    //$content .= "<p>Please make sure that your card number, expiration date, and cvv code are accurate.</p></div>";
                }
            }

            // check if a restaurant exists/was created
            if ($a_payment_status_vars['dataname'] != '') {
                $a_dataname = array( array('dataname' => $a_payment_status_vars['dataname']) );
                MAIN_DB::insertOrUpdate($maindb, 'payment_status', $a_payment_status_vars, $a_dataname);
            }

        } else {
            //echo "Error: cannot find signup information";
            session_start();
            set_rollback_error( "Unexpected Error", "Signup Error 5" );
            $content = 'sections/js-rollback.html.php';
            $_SESSION[params(0)]["status"] = "failed";
            $_SESSION[params(0)]['rowid'] = $rowid;
        }
    } else {
        // Something went wrong, set a flag to go back when the content loads
        session_start();
        set_rollback_error( "Unexpected Error", "Signup Error 18" );
        $content = 'sections/js-rollback.html.php';
        $_SESSION[params(0)]["status"] = "failed";
        $_SESSION[params(0)]['rowid'] = $rowid;
    }
    $args = empty( $rowid ) ? null : array("rowid" => $rowid);
    return js( $content, null, $args );
}
