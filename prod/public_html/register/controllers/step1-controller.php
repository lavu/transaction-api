<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 8/25/14
 * Time: 3:51 PM
 */

function old_form() {
    $loc = empty( $_GET['location'] ) ? null : $_GET['location'];
    $loc = empty( $_GET['loc'] ) ? $loc : $_GET['loc'];
    $loc = empty( $_GET['locale'] ) ? $loc : $_GET['locale'];
    $product = empty( $_GET['product'] ) ? null : $_GET['product'];
    $level = empty( $_GET['level'] ) ? null : ucfirst( $_GET['level'] );
    params("level", $level);
    params("locale", $loc);
    params("product", $product);
    return step1_dispatcher();
}

function _3_month_promo(){
    // Redirecting https://register.poslavu.com/staples-3-month-promo, per Donnie
    header('Location: http://www.lavu.com/free-ipad-pos/?utm_source=LavuLandingPage&utm_medium=8Bucks&utm_campaign=FreeLavu');
    exit;
    //params('level', 'Lavu88');
    //params('product', 'all');
    //params('locale', 'en');
    //set("source", params(0). "_lavu88_3mo8");
    //return step1_dispatcher();
}

function step1_dispatcher() {
    // Redirecting https://register.poslavu.com/staples-3-month-promo, per Mattlocke
    header('Location: https://www.lavu.com/free-trial');
    exit;
    $country = empty( $_REQUEST['country'] ) ? null : $_REQUEST['country'];
    $country = get_country( $country );
    $locale = params('locale');
    $supported_languages = array("en", "cn", "es");
    if (  !empty($locale)  && !in_array(strtolower($locale), $supported_languages )) {
        halt(NOT_FOUND, "Sorry. You got the wrong URL");
    }
    set_or_default("level", ucfirst( params('level') ), "Silver");
    set_or_default("locale", params('locale'), "en");
    set_or_default("product", params('product'), "all");
    // set the lead source
    $tracking = array();
    foreach(
        array( 'domain', 'referer', 'entry_url',
            'utm_campaign', 'utm_source', 'utm_medium' ) as $tracking_param ) {
        if ( !empty( $_GET[$tracking_param]) ) {
            $tracking[$tracking_param] = $_GET[$tracking_param];
        }
    }
    set_or_default( "tracking", $tracking, array() );
    require_once ADMIN_PATH . "/sa_cp/billing/package_levels_object.php";
    require_once option('root_dir') . '/signup_shared.php';
    $is_3_month_promo = set("source");
    $is_3_month_promo = !empty( $is_3_month_promo );
    switch( set( "locale" ) ) {
        case "es" :
            $content = "sections/es/free-trial.html.php";
            break;
        default :
            $content="sections/free-trial.html.php";
            if ( strpos(set("source"),"_lavu88_3mo8") ) {
                $content="sections/three-month-promo.html.php";
            }
            break;
    }
    return html( $content, "layouts/step1.php" );
}

function lavu88 () {
    params( 'level', 'Lavu88');
    params( 'product', 'all' );
    params( 'locale', 'en' );
    return step1_dispatcher();
}

?>
