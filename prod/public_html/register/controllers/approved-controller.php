<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 8/29/14
 * Time: 5:27 PM
 */

function approved() {
    $received_id = params('id');

    $received_id = empty( $received_id ) ? $_REQUEST['id'] : $received_id;
    $rowid = ( $received_id - 12500);
    try {
    $credentials = get_user_credentials( $rowid );
    } catch( Exception $e ) {
        syslog(LOG_WARNING, "DB error" );
        $content = "An error happened retrieving your credentials";
        if ( DEV ){
            set('dev' , $e->getMessage() );
        }
        return html($content, "layouts/approved-error.html.php");
    }
    set("source", $credentials['source']);
    if ( $credentials === false ) {
        $content = <<<HTML
<span style='font-weight:bold;'>
    Error: Invalid credentials<br />
</span><br />
<span style='color:#666'>
    Did you get this message in error? Call support at 855-767-5288
</span><br />
HTML;
        return html($content, "layouts/approved-error.html.php");
    }
    // send internal email for sign-ups that happen with a promo code
    if (!empty($credentials['promo']) && !ctype_space($credentials['promo']) && !DEV) {
        mail('ahassley@poslavu.com,ag@lavu.com,tom@lavu.com', "Sign-up with promo code {$credentials['promo']}", $credentials['dataname'], 'From:promo@poslavu.com');
    }
    set("username", $credentials['username']);
    set("password", $credentials['password']);
    set("rowid", $received_id);
    return html("layouts/approved.html.php", null);
}

function get_user_credentials( $rowid ) {
    $query = <<<SQL
SELECT `username`, `dataname`, `package`,
AES_DECRYPT(`password`,'password') AS `password`,
`email`, `promo`, `source`, `package`, `company`
FROM `poslavu_MAIN_db`.`signups` WHERE `id`='[rowid]'
AND (`sessionid`='[sessionid]' OR `ipaddress`='[ipaddress]')
ORDER BY `signup_timestamp` DESC LIMIT 1
SQL;

    $cred_query = mlavu_query( $query, array(
            'rowid'=>$rowid,
            'sessionid'=>session_id(),
            'ipaddress'=>$_SERVER['REMOTE_ADDR']
        )
    );
    if ( !$cred_query ) {
        throw new Exception("DB Error:: " . mlavu_dberror() );
    } elseif ( !mysqli_num_rows( $cred_query ) ) {
        return false;
    } else return mysqli_fetch_assoc($cred_query);
}

function approved_change_password() {
    $to_return = array( "status" => "error");
    foreach( array("username", "p_word", "rowid") as $required ) {
        if ( empty( $_POST[$required] ) ) {
            $to_return["message"] = "missing arguments";
            return json( $to_return, null );
        }
    }
    $rowid = intval($_POST['rowid'] ) - 12500;
    $uname = $_POST['username'];
    $pwd = $_POST['p_word'];
    $result = change_password( $uname, $pwd, $rowid, $error );
    if ( is_null( $result ) ) {
        $to_return['message'] = "User Does Not exist";
    } elseif( $result ) {
        $to_return['status'] = "success";
        $to_return['message'] = "password_updated";
    } else {
        syslog(LOG_WARNING, $error );
        $to_return['message'] = "Internal Error";
    }

    return json( $to_return, null );
}

function approved_resend_email() {
    $row_id = empty( $_REQUEST['rowid'] ) ? params('rowid') : $_REQUEST['rowid'];
    return json( resend_email( $row_id ) );
}

function resend_email( $rowid ) {
    $to_return = array( "status" => "error" );
    $cred_read = get_user_credentials( intval( $rowid ) - 12500 );
    if ( !empty( $cred_read['username'] ) && !empty( $cred_read['password'] ) ) {
        require_once( option('root_dir') .'/create_account.php');
        // require_once('/home/poslavu/public_html/admin/cp/resources/json.php');
        // don't send the email if the email is blank
        if (trim($cred_read['email']) == '') {
            return array(
                    'action'=>'alert',
                    'error'=>'true',
                    'message'=>"The email address registered with this account is blank."
            );
        }
        // send the email
        $lang = "en";
        $lang = empty($_REQUEST['loc']) ? $lang : $_REQUEST['loc'];
        $lang = empty($_REQUEST['locale']) ? $lang : $_REQUEST['locale'];
        $mainvars['activation_welcome'] = ($cred_read['package'] == '14' || $cred_read['package'] == '15' || $cred_read['package'] == '16');
        $mainvars['company_name'] = $cred_read['company'];
        send_new_account_email($cred_read['email'],$cred_read['username'],$cred_read['password'],$lang,$mainvars);
        return array(
                'action'=>'alert',
                'error'=>'false',
                'message'=>"Success! An email has been sent to your inbox. If the email isn't there yet please wait a few minutes and check again or check your spam folder."
        );
    }
}

/**
 * Changes the password coming from the approved page
 * @param $uname
 * @param $pwd
 * @param null $err
 * @return bool|null null if user doesn't exist, false for error, true if it's
 * all good
 *
 */
function change_password( $uname, $pwd, $rowid, &$err=null) {
    $query = <<<SQL
SELECT * FROM `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'
SQL;
    $req = mlavu_query( $query, $uname );
    $err= mlavu_dberror();
    if ( !empty( $err ) )  {
        return false;
    } elseif( !mysqli_num_rows( $req ) ){
        $err = "User Does Not Exist";
        return null;
    }else {
        $curr_row = mysqli_fetch_assoc( $req );
        if ( empty( $curr_row['dataname'] ) ) {
            $err = __FUNCTION__ . ":: Row has no dataname";
            return false;
        }
        $dataname = $curr_row['dataname'];
        $update_query = <<<SQL
UPDATE `poslavu_[1]_db`.`users` SET `password`=PASSWORD('[2]') WHERE `username`='[3]' LIMIT 1
SQL;
        mlavu_query( $update_query, $dataname, $pwd, $uname );
        $update_query_signups = <<<SQL
update `poslavu_MAIN_db`.`signups` set `password`=AES_ENCRYPT('[1]','password') where `id`='[2]'
SQL;
        mlavu_query( $update_query_signups, $pwd, $rowid);
        $err = mlavu_dberror();
        if ( empty( $err ) ) {
            $insert_query = <<<SQL
INSERT INTO `poslavu_[1]_db`.`config` (`setting`,`value`) VALUES ('main_account_password_updated','1')
SQL;
            mlavu_query( $insert_query, $dataname );
            $err = mlavu_dberror();
            if ( !empty( $err ) ) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}