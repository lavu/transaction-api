<?php

function signup() {
	$response = array('success' => false, 'reason' => '');

	// POST'd JSON
	$posted_json = file_get_contents('php://input');
	$data = json_decode($posted_json, true);

	// Check required fields
	$has_req_fields = !(empty($data['default_menu']) || empty($data['package']) || empty($data['company']) || empty($data['email']));

	// Create account
	if ( $has_req_fields )
	{
		$maindb = isset($maindb) ? $maindb : 'poslavu_MAIN_db';
		mlavu_select_db($maindb);

		// Convert the package name into the package level (ex. Silver3 => 21)
		$o_package_container = ( !isset( $o_package_container ) ) ? new package_container() : $o_package_container;
		$package_level = $o_package_container->get_level_by_attribute('name', $data['package']);

		$account = array(
			'package' => $package_level,
			'signup_package' => $package_level,
			'company' => (isset($data['company']) ? $data['company'] : ''),
			'email' => (isset($data['email']) ? $data['email'] : ''),
			'phone' => (isset($data['phone']) ? $data['phone'] : ''),
			'firstname' => (isset($data['firstname']) ? $data['firstname'] : ''),
			'lastname' => (isset($data['lastname']) ? $data['lastname'] : ''),
			'address' => (isset($data['address']) ? $data['address'] : ''),
			'city' => (isset($data['city']) ? $data['city'] : ''),
			'state' => (isset($data['state']) ? $data['state'] : ''),
			'zip' => (isset($data['zip']) ? $data['zip'] : ''),
			'country' => (isset($data['country']) ? $data['country'] : ''),
			'default_menu' => (isset($data['default_menu']) ? $data['default_menu'] : ''),
			'status' => 'new',
			'date' => date('Y-m-d'),
			'time' => date('H:i:s'),
			'ipaddress' => (empty($data['ipaddress']) ? $_SERVER['REMOTE_ADDR'] : $data['ipaddress']),
			'sessionid' => session_id(),
			'source' => (isset($data['source']) ? $data['source'] : ''),
			'locale' => (isset($data['locale']) ? $data['locale'] : ''),
		);

		// Create signup record + set $rowid
		$insert_cols = '';
		$insert_vals = '';
		foreach ($account as $key => $val) {
			if ($key == 'locale') continue;
			$key = mysqli_real_escape_string(ConnectionHub::getConn('poslavu')->getLink('read'),$key);
			$val = mysqli_real_escape_string(ConnectionHub::getConn('poslavu')->getLink('read'),$val);
			$insert_cols .= empty($insert_cols) ? "`{$key}`"   : ", `{$key}`";
			$insert_vals .= empty($insert_vals) ? "'[{$key}]'" : ", '[{$key}]'";
		}
		$insert = mlavu_query("INSERT INTO `{$maindb}`.`signups` ({$insert_cols}) VALUES ({$insert_vals})", $account);
		if ($insert === false) return json_encode(array('successs' => false, 'reason' => mlavu_dberror()));
		$rowid = mlavu_insert_id();

		// Create reseller lead using account data
		$account['posted'] = 1;
		$account['created'] = date('Y-m-d H:i:s');
		$account['signupid'] = $rowid;
		$account['demo_type'] = 'signups';
		$account['session_id'] = $account['sessionid'];
		$account['signups_json'] = $posted_json;
		$account['company_name'] = $account['company'];
		$account['package'] = (isset($data['package']) ? $data['package'] : '');
		$account['entry_url'] = (isset($data['entry_url']) ? $data['entry_url'] : '');
		$account['referring_site'] = (isset($data['referrer']) ? $data['referrer'] : '');
		$account['utm_campaign'] = (isset($data['utm_campaign']) ? $data['utm_campaign'] : '');
		$account['utm_source'] = (isset($data['utm_source']) ? $data['utm_source'] : '');
		$account['utm_medium'] = (isset($data['utm_medium']) ? $data['utm_medium'] : '');
		$account['lead_source'] = (isset($data['lead_source']) ? $data['lead_source'] : '');

		$reseller_lead_id = create_or_update_reseller_lead( $account, array(
			array('column'=>'session_id'),
			array('column'=>'email', 'match_percent'=>90),
			array('column'=>'company_name', 'match_percent'=>85))
		);

		$package_info = array(
			'package' => $data['package'],  // package name not package level
			'ipods' => $o_package_container->get_ipod_limt_by_attribute('level', $package_level),
			'ipads' => $o_package_container->get_ipad_limt_by_attribute('level', $package_level),
		);

		// Populate variables required to create an account in create_account.php.
	    global $error_content;
		$_POST['vf_signup'] = '1';
	    @require_once( $_SERVER['DOCUMENT_ROOT'] .'/create_account.php' );

	    // Create the POS account
		$result = create_new_poslavu_account( $account['company'], $account['email'], $account['phone'], $account['firstname'], $account['lastname'], $account['address'], $account['city'], $account['state'], $account['zip'], $account['default_menu'], $account['locale'], 'Client', '', $package_info );

		// Prepare the response
		$response = array(
			'success'      => (isset($result[0]) ? $result[0] : ''),
			'username'     => (isset($result[1]) ? $result[1] : ''),
			'password'     => (isset($result[2]) ? $result[2] : ''),
			'dataname'     => (isset($result[3]) ? $result[3] : ''),
			'restaurantid' => (isset($result[4]) ? $result[4] : ''),
			'signupid'     => $rowid,
			'trial_end'    => (isset($result[5]) ? $result[5] : ''),
		);

		if ($response['success']) {

			mlavu_query("update `poslavu_MAIN_db`.`signups` set `username`='[username]', `dataname`='[dataname]', `restaurantid`='[restaurantid]', `password`=AES_ENCRYPT('[password]','password') where `id`='[signupid]'", $response);
			mlavu_query("update `poslavu_MAIN_db`.`reseller_leads` set `data_name`='[dataname]' where `id` != '' and `id`='[reseller_lead_id]'", array('dataname' => $response['dataname'], 'reseller_lead_id' => $reseller_lead_id));
			confirm_restaurant_shard_new($response['dataname']);
		}
	}
	else
	{
		$response['reason'] = 'Missing required fields';
	}

	return json_encode($response);
}

// Had to copy function from auth_account.php using slightly different name (appended "_new") because file coudln't be included since it had executable code and not just function declarations.
function confirm_restaurant_shard_new($dataname)
{
	$str = "";
	$letter = substr($dataname,0,1);
	if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
		$letter = $letter;
	else
		$letter = "OTHER";
	$tablename = "rest_" . $letter;

	$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$dataname);
	if(mysqli_num_rows($mainrest_query))
	{
		$str .= $letter . ": " . $dataname . " exists";
	}
	else
	{
		$str .= $letter . ": " . $dataname . " inserting";

		$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);

			$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
			if($success) $str .= " <font color='green'>success</font>";
			else $str .= " <font color='red'>failed</font>";
		}
	}
	return $str;
}

function activation_signup() {
	$maindb = !isset($maindb) ? 'poslavu_MAIN_db' : $maindb;

	$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
	$rowid = ( $id - 12500);


	unset($result);
	$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `id` = '[id]'", array('id' => $rowid));
	if ($result === false) die(mlavu_dberror());
	$signup_rows_found = mysqli_num_rows($result);
	$signup = mysqli_fetch_assoc($result);

    $email = isset($signup['email']) ? $signup['email'] : '';

    set('email', $email);
    set('rowid', $rowid);
    set('username', '');
    set('password', '');

    return html("layouts/activation_signup.php", null);
}

function activate_signup_loading_screen()
{
	$verification_token = params('verification_token');
	$shifted_signups_id = params('shifted_signups_id');
	$shifted_reseller_leads_id = params('shifted_reseller_leads_id');

	return showLoadingScreen($shifted_signups_id, $shifted_reseller_leads_id, $verification_token);
}

function activate_signup()
{
	global $o_emailAddressesMap;

	$maindb = !isset($maindb) ? 'poslavu_MAIN_db' : $maindb;
	$o_package_container = ( !isset( $o_package_container ) ) ? new package_container() : $o_package_container;

	mlavu_select_db($maindb);

	///checkForRecentSignups();  // TO DO : check this code
	///$b_weekly_signups = (getRecentSignups(-7, +1) > 0);  // TO DO : check this code
	$b_finished_signup = false;

	$verification_token = params('verification_token');
	$shifted_signups_id = params('shifted_signups_id');
	$shifted_reseller_leads_id = params('shifted_reseller_leads_id');

	// Scrutinize params here and make sure the the verification_token (aka session ID) is alphanumeric and that the IDs are only numeric.
	if ( preg_match('/[^a-zA-Z0-9]/', $verification_token) || preg_match('/\D/', $shifted_signups_id) || preg_match('/\D/', $shifted_reseller_leads_id) ) {
		return html("Account not found.  Please contact Lavu Support to continue.", "layouts/activation_error.php");	
	}

	$rowid = (intval($shifted_signups_id) - 12500);
	$reseller_leads_id = (intval($shifted_reseller_leads_id) - 12500);

	// Query for the signup row using the passed-in parameters.
	unset($result);
	$result = mlavu_query("SELECT * FROM `signups` WHERE `id` = '[id]'", array('id' => $rowid));
	if ($result === false) die(mlavu_dberror());
	$signup_rows_found = mysqli_num_rows($result);
	$signup = mysqli_fetch_assoc($result);

	// See if the account has been activated already.  If not, create a POS account for it.
	$found_signup = ($signup_rows_found && substr($signup['verification_token'], 0, strlen($verification_token)) == $verification_token);
	$has_account = ($found_signup && !empty($signup['dataname']));

	if ( !$found_signup )
	{
		return html("Account not found.  Please contact Lavu Support to continue.", "layouts/activation_error.php");
	}
	else if ( $found_signup && !$has_account )
	{
		$doc_root = DOC_ROOT;

		// Populate variables required to create an account in auth_account.php.
		$_POST['locale'] = '';
		$_POST['package'] = $o_package_container->get_plain_name_by_level($signup['package']);
		$_POST['posted'] = '1';
		$_POST['vf_skip'] = '1';
		$_POST['vf_signup'] = '2';
		$_POST['rowid'] = $rowid;
		$_POST['mname'] = $signup['default_menu'];
		$_POST = $_POST + $signup;  // merges the arrays (but favors the left-hand array in case of collision)
		$s_dataname = '';
		$content = '';
		$locale = '';
		$b_restaurant_created_in_auth_account = false;

	    // Create the POS account.
	    ///ob_start();
	    global $error_content;
	    @require_once("{$doc_root}/auth_account.php");
	    ///$error_content = ob_get_contents();
	    ///ob_end_clean();
	    ///echo $error_content;

	    if ( $b_restaurant_created_in_auth_account )
	    {
			$a_lead_data = array(
				'maindb' => $reseller_leads_id,
				'id' => $reseller_leads_id,
	        	'data_name' => $s_dataname,  // for `reseller_leads`.`data_name`
				'session_id' => $verification_token,
	        	'made_demo_account' => date("Y-m-d h:m:i"),
	        );
	        mlavu_query( "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `data_name` = '[data_name]', `made_demo_account` = '[made_demo_account]'  WHERE `id` = '[id]' AND `session_id` LIKE '[session_id]%'", $a_lead_data );
	        if ( ConnectionHub::getConn('poslavu')->affectedRows() < 0 ) error_log( __FILE__ ." failed to update reseller_leads row with ". print_r($a_lead_data, true) );

	        // Create a Salesforce Stage-change event to make sure that Salesforce is updated with the dataname in case the account's lead gets sent before it has a dataname.
	        require_once('/home/poslavu/public_html/admin/sa_cp/billing/procareas/SalesforceIntegration.php');
	        SalesforceIntegration::createSalesforceEvent($s_dataname, 'stage_change', 'Trial - No CC');

			// Redirect to standard Register approved page.  (id in URL allows record look-up)
			$redirect_url = "/approved.php?id={$shifted_signups_id}";
			echo "<script language='javascript'>window.location.replace('{$redirect_url}');</script>";
	    }
	    else
	    {
			return html("Account could not be activated.  Please contact Lavu Support to continue.", "layouts/activation_error.php");
	    }
	}
	else if ( $found_signup && $has_account )
	{
		echo "Failed";
		$error_msg = __FILE__ .' '. __METHOD__ ."() got activation attempt from an already-activiated account (rowid={$rowid} signup_rows_found={$signup_rows_found} dataname={$signup['dataname']} ". $_SERVER['REMOTE_ADDR'] .')';
		error_log($error_msg);
		$redirect_url = "/approved.php?id={$shifted_signups_id}";
		echo "<script language='javascript'>window.location.replace('{$redirect_url}');</script>";
	}
}

function showLoadingScreen($shifted_signups_id, $shifted_reseller_leads_id, $verification_token)
{
	echo <<<HTML

<!DOCTYPE html>
<html>
    <head>
      <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
      <title>Lavu - Please wait while we activate your account</title>
    </head>
    <body id="submit_signup" onload="">
      <p style="text-align:center">
          <img src="/images/_anim_spincircs_12_32_007sec.gif" 
          width="98%" alt="" id="wait-animation" />
      </p>
      <script language='javascript'>window.location.replace('/activate-signup-exec/{$shifted_signups_id}/{$shifted_reseller_leads_id}/{$verification_token}');</script>

HTML;
}
