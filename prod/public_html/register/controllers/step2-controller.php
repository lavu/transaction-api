<?php

/**
 * Umbrella function for step2 page
 * @return string
 */
function step2(){
    // Add to log (just for shits and giggles)
    $logstr = "";
    $logstr .= "------------------------\n";
    $logstr .= date("Y-m-d H:i:s") . " - " . $_SERVER['REMOTE_ADDR'] . "\n";
    foreach ($_POST as $postkey => $postval) {
        if ( !is_array( $postval ) ) { $logstr .= $postkey . "=" . $postval . "\n"; }
        else { $logstr .= $postkey . "=" . serialize( $postval ) . "\n"; }
    }
    $fname = "/home/poslavu/private_html/88step2log.txt";
    if (!empty($_SERVER["PRIVATE_DIR"])) {
        $fname = "{$_SERVER["PRIVATE_DIR"]}/88step2log.txt";
    }
    if (file_exists($fname)) {
        $fp = fopen($fname, "a");
        fwrite($fp, $logstr);
        fclose($fp);
    }
    // Make sure that we have a package level, depending on the origin
    // of the incoming data, we might have to infer the package level through
    // different means
    global $o_package_container;
    global $o_signup;
    $content = null;
    $incoming_data = array();
    set("bluestar", false);
    if ( !empty( $_REQUEST['promo'] ) && preg_match("/^..[bBlLUueE].([1238])([uUcC])./", $_REQUEST['promo'], $m ) ){
        $lvl_trans = array('8' => "Lavu88", '1' => "Silver", '2' => "Gold", '3' => "Platinum");
        $level = $lvl_trans[strtolower($m[1])];
        $discount_amount = null;
        if ( !check_promo_code( $_REQUEST['promo'], $level, $discount_amount ) ) {
            // bad promocode, same as above but for different reasons
            halt(HTTP_BAD_REQUEST, "Invalid Promo Code : {$_REQUEST['promo']}");
        }
        $_REQUEST['country'] = $m[2] == "C"? "Canada" : "United States";
        set( 'level', $level );
        set( 'locale', 'en' );
        $incoming_data['bluestar'] = 1;
        set("bluestar", true );
        $content = 'sections/bluestar-content.html.php';
    } else {
        $levels = array( "Lavu88", "Silver", "Gold", "Platinum" );
        $level_is_set = !empty( $_REQUEST['level'] ) ? 1 : 0;
        $num_terminals_is_set = !empty( $_REQUEST['num_terminals'] ) ? 1 : 0;
        $level = $level_is_set ? $_REQUEST['level'] :
            ($num_terminals_is_set ? $levels[$_REQUEST['num_terminals']] : null);
        if ( !empty( $_REQUEST['lavu88'] ) && $_REQUEST['num_terminals'] == 1) {
            $level = $levels[0];
        }
        global $a_highest_package_levels;
        if ( isset( $level ) ) {
            $a_highest_package_levels = $o_package_container->get_highest_base_package_names();
            if (in_array( $level, array("Lavu88", 'Silver','Gold','Platinum'))) {
                $new_level = $level.'2';
                $a_highest_package_levels = $o_package_container->get_highest_base_package_names();
                $new_level = "";
                foreach($a_highest_package_levels as $s_package_level) {
                    if (strpos( $s_package_level, $level ) === 0) {
                        $new_level = $s_package_level;
                        break;
                    }
                }
                $level = $new_level;
            }
        } else if ( !in_array( $level, $a_highest_package_levels ) ) {
            halt("Package level not Found");
        } else {
            halt("Package level not Found");
        }
        set_or_default("level", $level, "Silver" );
        set("locale", params('locale') );
        $content = "sections/verify-card-info.html.php";
    }
    // Handle incoming lead
    $a_lead_data = $_REQUEST;
    $a_lead_data['session_id'] = session_id();
    $a_lead_data['created'] = date('Y-m-d H:i:s');
    $a_lead_data['salesforce_sent'] = $a_lead_data['created'];
    $a_lead_data['lead_source'] = !empty( $_REQUEST['lead_source'] ) ?
        $_REQUEST['lead_source']:
        ( empty($_REQUEST['domain'] ) ?
            'web' :
            $_REQUEST['domain'] );
    set( "lead_source", $a_lead_data['lead_source'] );
    $poll_info = array();
    if ( !empty( $_REQUEST['new_pos'] ) ) {
        $poll_info['new_pos'] = $_REQUEST['new_pos'];
    }
    if ( !empty( $_REQUEST['interests'] ) ) {
        $poll_info['interests'] = $_REQUEST['interests'];
    }
    if ( !empty( $poll_info ) ) {
        $json_poll = json_encode( $poll_info );
        $a_lead_data['signups_json'] = $json_poll;
    }
    $signup_count = getRecentSignups(-1, +1);
    $inherited_from_step1 = array(
        "salesforce_lead_id", "escape_ip_check", "company_name", "phone", "email",
        "zip", "full_name", "mname", "level", "country", "rowid",
        "source", "lead_source", "utm_campaign", "utm_source", "utm_medium", "domain", "referer", "entry_url");
    foreach( $inherited_from_step1 as $inherited ){
        if ( !empty( $_REQUEST[$inherited] ) ) {
            $incoming_data[$inherited] = $_REQUEST[$inherited];
        }
    }
    if ( empty( $incoming_data['lead_source'] ) ) {
        $incoming_data['lead_source'] = $a_lead_data['lead_source'];
    }
    if ( $signup_count && empty( $_REQUEST["escape_ip_check"] ) ) return html("layouts/too-many.html.php", null);
    // if the 'no_lead' variable is set, we don't create a lead
    if ( empty( $_REQUEST['no_lead'] ) ) {
        $b_new_lead = create_or_update_lead( $a_lead_data, array(
                array('column'=>'session_id'),
                array('column'=>'email', 'match_percent'=>90),
                array('column'=>'company_name', 'match_percent'=>85))
        );
        if ( !empty( $b_new_lead ) ) {
            $package = $o_package_container->get_package_by_attribute('name', ucfirst($_REQUEST['level']) );
            $name_split = split_name( $_REQUEST['full_name'] );
            $salesforce_lead_status = send_to_salesforce(array(
                "zip" =>$_REQUEST['zip'],
                "company_name" => $_REQUEST['company_name'],
                "phone" => $_REQUEST['phone'],
                "FirstName" => $name_split[0] ? $name_split[0] : "N/A",
                "LastName" => $name_split[1] ? $name_split[1] : "N/A",
                "level" => $_REQUEST['level'],
                "license_type"=> $_REQUEST['level'],
                "lead_source" => empty( $_REQUEST['domain'] )? "web" : $_REQUEST['domain'],
                "utm_campaign" => empty( $_REQUEST['utm_campaign'] ) ? "" : $_REQUEST['utm_campaign'],
                "utm_source" => empty( $_REQUEST['utm_source'] ) ? "" : $_REQUEST['utm_source'],
                "utm_medium" => empty( $_REQUEST['utm_source'] ) ? "" : $_REQUEST['utm_medium'],
                "referer" => empty( $_REQUEST['referer'] ) ? "" : $_REQUEST['referer'],
                "entry_url" => empty( $_REQUEST['entry_url'] ) ? "" : $_REQUEST['entry_url'],
                "lead_id" => $b_new_lead,
                "stage" => "Prospecting",
                "email" => $_REQUEST['email'],
                "licensing_fee" => $package->get_value(),
                "monthly_fee" => $package->get_recurring()
            ));
            if( !empty( $salesforce_lead_status['id'] ) ) {
                mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `salesforce_id`='[1]' WHERE `id`=[2]",
                    $salesforce_lead_status['id'], $b_new_lead );
                $incoming_data['salesforce_id'] = $salesforce_lead_status['id'];
            }
        }
    }
    set("incoming_data", $incoming_data);
    // serve
    return html( $content, 'layouts/step2.php');
}
