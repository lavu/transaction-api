<?php
/**
 * Wrapper for submit_contact_form() that adapts the existing function with the new lavu.com's convention of posting only JSON.
 */
function submit_contact() {
    $response = array('success' => false, 'reason' => '');

    $posted_json = file_get_contents('php://input');
    $data = json_decode($posted_json, true);

    $reqfields = array('json', 'lead_source', 'company_name', 'email', 'name', 'notes', 'country', 'state', 'zip', 'group');
    foreach ($reqfields as $reqfield) {
        if (isset($_REQUEST[$reqfield]) && !empty($_REQUEST[$reqfield]))
            continue;

        if ( $reqfield != 'json' && (!isset($data[$reqfield]) || (isset($data[$reqfield]) && empty($data[$reqfield])) ) ) {
            $response['reason'] = "Missing Arguments: {$reqfield}";
            return json($response);
        }

        $_REQUEST[$reqfield] = ($reqfield == 'json') ? $posted_json : $data[$reqfield];
    }

    if (empty($_SERVER['HTTP_ORIGIN']))
        $_SERVER['HTTP_ORIGIN'] = 'lavu.com';

    $result_json = submit_contact_form();
    $result = json_decode($result_json, true);

    $response['success'] = $result['success'];
    $response['reason']  = isset($result['msg']) ? $result['msg'] : '';

    //error_log( __FILE__ .' DEBUG: result='. print_r($result, true) .' response='. print_r($response, true));  //debug

    return json($response);
}

/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 12/23/14
 * Time: 10:46 AM
 */

function submit_contact_form () {
    $origin_domain = empty( $_SERVER['HTTP_REFERER'] ) ?
        $_SERVER['HTTP_ORIGIN'] :
        parse_url( $_SERVER['HTTP_ORIGIN'],  PHP_URL_HOST );
    $is_json = !empty( $_REQUEST['json'] );
    /*if ( !$is_json ) {
        if ( empty( $_REQUEST['success_return_url'] ) && empty( $_REQUEST['failed_return_url'] ) ) {
            send_header("Location: " . LAVUWEB. "/contact-form-error" );
            return;
        }
        $fail_domain = parse_url( $_REQUEST['failed_return_url'], PHP_URL_HOST );
        $success_domain = parse_url( $_REQUEST['success_return_url'], PHP_URL_HOST );
        if ( !authorized_cors_domain( $fail_domain ) || !authorized_cors_domain( $success_domain ) ) {
            send_header("Location: " . LAVUWEB. "/contact-form-error" );
            return;
        }
    } else {
        if ( !authorized_cors_domain( $origin_domain ) ) {
            // not that this response matters. they won't see it since its blocked
            return json( array( 'success' => false, "msg" => "CORS failed" ) );
        }
        send_header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    }*/
    $requirements = array("lead_source", "company_name", "email", "name", "notes", "country", "state", "zip", "group");
    $lead_cols = array();
    foreach( $requirements as $required){
        if ( empty( $_REQUEST[$required] ) ){
            if ( $is_json ) {
                return json(array("success" => false, "msg" => "Missing Arguments : '{$required}'"));
            } else {
                send_header("Location: {$_REQUEST['failed_return_url']}" );
                return;
            }
        } else {
            $lead_cols[$required] = mysqli_real_escape_string( ConnectionHub::getConn('poslavu')->getLink('read'), strip_tags( htmlspecialchars ( trim( $_REQUEST[$required] ) ) ) );
        }
    }
    $subject = "$origin_domain Contact Request to '{$lead_cols['group']}";
    $message = str_replace( '\n', "  ", $lead_cols['notes']  );
    $message = str_replace( '\r', "  ", $message );
    $datetime_obj = new DateTime("now", new DateTimeZone('America/Chicago'));
    $timestamp = $datetime_obj->format("Y-m-d H:i:s");
    $message .= " *** Geo Data: Country::{$lead_cols['country']}\nState::{$lead_cols['state']}\nZip::{$lead_cols['zip']} ***";
    $message = "$timestamp:*:{$subject}:*:{$message}*|*";
    $phone = empty($_REQUEST['phone']) ? '' : mysqli_real_escape_string( ConnectionHub::getConn('poslavu')->getLink('read'), strip_tags( htmlspecialchars ( trim( $_REQUEST['phone'] ) ) ) );
    $lead_cols['phone'] = $phone;
    $zendesk_success = prepare_data_for_zendesk( array(
        "message" => $message,
        "name" => $lead_cols['name'],
        "email" => $lead_cols['email'],
        "subject" => $subject,
        "location" => $lead_cols['company_name'],
        "group" => $lead_cols['group'],
        "phone" => $phone
    ) );
    list( $firstname, $lastname ) = split_name( $lead_cols['name'] );
    unset( $lead_cols['name'] );
    $lead_cols['created'] = date('Y-m-d H:i:s');
    $lead_cols['firstname'] = $firstname;
    $lead_cols['lastname'] = $lastname;
    $lead_cols['session_id'] = empty( $_REQUEST['session_id'] )? session_id() : $_REQUEST['session_id'];
    $lead_cols['lead_strength'] = 1;
    $lead_cols['salesforce_sent'] = $lead_cols['created'];
    $lead_cols['notes'] = $message;
    $lead_cols['_canceled'] = (isset($lead_cols['group']) && $lead_cols['group'] != 'sales') ? '1' : '0';  // kalynn asked us to kill non-sales leads in the distro portal but the rest of this code relies on the reseller_leads db id
    $dbid = create_or_update_lead( $lead_cols, true );
    try {
        require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceLead.php";
        require_once "/home/poslavu/private_html/salesforceCredentials.php";
        $credentials = new SalesforceLogin( SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET );
        $lead_handler = new SalesforceLead( $credentials );
        if ( empty( $lead_handler ) ){
            $lead_handler = new SalesforceLead( $credentials );
        }
        $response = $lead_handler->upsert('DBID__c', $dbid, array(
            "FirstName" => empty( $lead_cols['firstname'] ) ? "N/A": $lead_cols['firstname'],
            "LastName" => empty( $lead_cols['lastname'] ) ? "N/A" : $lead_cols['lastname'],
            "Phone" => empty( $lead_cols['phone'] ) ? "N/A" : $lead_cols['phone'],
            "Country__c" => $lead_cols['country'],
            "Company" => empty( $lead_cols['company_name'] ) ? "N/A" : $lead_cols['company_name'],
            "Email" => empty( $lead_cols['email'] ) ? "N/A" : $lead_cols['email'],
            "Description__c" => $lead_cols['notes'],
            "Zip_Postal_Code__c" => $lead_cols['zip'],
            "State_Provence__c" => $lead_cols['state']
        ));
    } catch( SalesforceHandlerException $e ) {
        mail("tom@lavu.com", "Salesforce Error", $e->getMessage() ."\n\n". print_r( $_REQUEST, true ));
        $response['success'] = false;
    }
    if ( $is_json ) {
        return json( array( "success" => !empty( $dbid ) ,  "msg" => "Your inquiry has been submitted. Check your email for a response" ) );
    } else {
        send_header("Location: " . ($dbid ? $_REQUEST['success_return_url'] : $_REQUEST['failed_return_url'] ) );
        return;
    }
}

/**
 * Copy of submit_contact_form() for lavu.com/beginners-guide-to-restaurant-pos
 * but with no required location fields and no zendesk ticket and JSON POST data.
 */
function submit_resource_form() {
    $response = array('success' => false, 'reason' => '');

    // Make sure they domain they're coming from is one registered for CORS
    $origin_domain = empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_ORIGIN'] : parse_url($_SERVER['HTTP_ORIGIN'],  PHP_URL_HOST);
    /*if ( !authorized_cors_domain( $origin_domain ) ) {
        // not that this response matters. they won't see it since its blocked
        $response['reason'] = 'CORS failed';
        return json($response);
    }*/

    // Transmit CORS header
    send_header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");

    // Get POST'd JSON
    $posted_json = file_get_contents('php://input');
    $data = json_decode($posted_json, true);

    // Check required fields
    $lead_cols = array();
    $requirements = array('lead_source', 'company_name', 'email', 'firstname', 'lastname', 'notes', 'group');
    foreach ( $requirements as $required ) {
        if ( empty($data[$required]) ){
            $response['reason'] = "Missing Arguments : '{$required}'";
            return json($response);
        } else {
            $lead_cols[$required] = mysqli_real_escape_string( ConnectionHub::getConn('poslavu')->getLink('read'), strip_tags( htmlspecialchars( trim( $data[$required] ) ) ) );
        }
    }

    // Prep data for `reseller_leads` and Salesforce
    $subject = "{$origin_domain} Resource Request to '{$lead_cols['group']}'";
    $message = str_replace( '\n', "  ", $lead_cols['notes']  );
    $message = str_replace( '\r', "  ", $message );
    $datetime_obj = new DateTime('now', new DateTimeZone('America/Chicago'));
    $timestamp = $datetime_obj->format("Y-m-d H:i:s");
    $message = "{$timestamp}:*:{$subject}:*:{$message}*|*";
    $phone = empty($data['phone']) ? '' : mysqli_real_escape_string( ConnectionHub::getConn('poslavu')->getLink('read'), strip_tags( htmlspecialchars ( trim( $data['phone'] ) ) ) );
    $lead_cols['phone'] = $phone;
    $lead_cols['created'] = date('Y-m-d H:i:s');
    $lead_cols['session_id'] = empty( $data['session_id'] )? session_id() : $data['session_id'];
    $lead_cols['lead_strength'] = 1;
    $lead_cols['salesforce_sent'] = $lead_cols['created'];
    $lead_cols['notes'] = $message;
    $lead_cols['_canceled'] = (isset($lead_cols['group']) && $lead_cols['group'] != 'sales') ? '1' : '0';  // kalynn asked us to kill non-sales leads in the distro portal but the rest of this code relies on the reseller_leads db id
    $dbid = create_or_update_lead( $lead_cols, true );
    try {
        require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceLead.php";
        require_once "/home/poslavu/private_html/salesforceCredentials.php";
        $credentials  = new SalesforceLogin( SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET );
        $lead_handler = new SalesforceLead( $credentials );
        if ( empty( $lead_handler ) ){
            $lead_handler = new SalesforceLead( $credentials );
        }
        $response = $lead_handler->upsert('DBID__c', $dbid, array(
            'FirstName' => empty( $lead_cols['firstname'] ) ? "N/A": $lead_cols['firstname'],
            'LastName' => empty( $lead_cols['lastname'] ) ? "N/A" : $lead_cols['lastname'],
            'Phone' => empty( $lead_cols['phone'] ) ? "N/A" : $lead_cols['phone'],
            'Company' => empty( $lead_cols['company_name'] ) ? "N/A" : $lead_cols['company_name'],
            'Email' => empty( $lead_cols['email'] ) ? "N/A" : $lead_cols['email'],
            'LeadSource' => empty( $lead_cols['lead_source'] ) ? "" : $lead_cols['lead_source'],
        ) );
    } catch( SalesforceHandlerException $e ) {
        mail('tom@lavu.com', 'Salesforce Error', $e->getMessage() ."\n\n". print_r( $data, true ));
        $response['success'] = false;
    }

    $response['success'] = !empty($dbid);
    $response['reason']  = 'Your inquiry has been submitted. Check your email for a response';
    return json($response);
}

function get_customer_field_to_submit($s_name, $s_value) {
    switch($s_name) {
        case 'phone':
            return array(
                'id' => '22570291',
                'name' => 'Phone Number',
                'value' => $s_value,
            );
        case 'name':
            return array(
                'id' => '22564852',
                'name' => 'Customer Name',
                'value' => $s_value,
            );
        case 'location':
            return array(
                'id' => '22567537',
                'name' => 'Location Name',
                'value' => $s_value,
            );
        case 'data_name':
            return array(
                'id' => '22575391',
                'name' => 'Data Name',
                'value' => $s_value,
            );
        default:
            return false;
    }
}

function prepare_data_for_zendesk( $data_array ){
    $group_translations =  array(
        "specialist" => array(
            "id" => 21130553,
            "email" => "pos.support@lavuinc.com"
        ),
        "sales" => array(
            "id" => 20755877,
            "email" => "sales@lavuinc.com"
        ),
        "support" => array(
            "id" => 20739892,
            "email" => "pos.support@lavuinc.com"
        ),
        "billing" => array(
            "id" => 20753913,
            "email" =>"billing@lavuinc.com"
        )
    );
    $custom = array(
        get_customer_field_to_submit('phone', preg_replace('/[^\d]*/i', '', $data_array['phone'].(empty($data_array['phone_ext'])?"":$data_array['phone_ext']))),
        get_customer_field_to_submit('name', $data_array['name']),
        get_customer_field_to_submit('location', $data_array['location'])
    );
    $arr = array(
        "z_subject"=>empty( $data_array['subject'] ) ? "Contact Form Request":$data_array['subject'],
        "z_description"=>trim( preg_replace('/\s+/', ' ', $data_array['message'] ) ),
        "z_recipient"=>$group_translations[$data_array['group']]['email'],
        "z_name"=>$data_array['name'],
        "z_requester"=>$data_array['email']
    );
    $create_arr = array(
        'ticket' => array(
            'subject' => $arr['z_subject'],
            'comment' => array(
                'body' => $arr['z_description'],
            ),
            'requester' => array(
                'name' => $arr['z_name'],
                'email' => $arr['z_requester']
            ),
            'status' => 'new',
            'custom_fields' => $custom,
            "group_id" => $group_translations[$data_array['group']]['id']
        ),
    );
    return send_to_zendesk( $create_arr );
}


/** three channels of submission :
 *  - jsonp through _GET
 *  - ajax through _POST
 *  - form submission
 */
function pro_lead_submit_gateway(){
    $is_jsonp = !empty($_GET) && !empty( $_GET['jsonp'] ) && !empty( $_GET['callback'] );
    $is_ajax = !empty( $_POST ) && !empty( $_REQUEST['ajax'] );
    $is_form = !$is_jsonp && !$is_ajax && !empty( $_POST );
    if ( !$is_jsonp && !$is_ajax && !$is_form ) {
        halt( 400, "Bad Request" );
    }
    if ( $is_ajax ) {
        $origin_domain = empty( $_SERVER['HTTP_REFERER'] ) ?
            $_SERVER['HTTP_ORIGIN'] :
            parse_url( $_SERVER['HTTP_ORIGIN'],  PHP_URL_HOST );
        if ( !authorized_cors_domain( $origin_domain ) ) {
            // not that this response matters. they won't see it since its blocked
            return json( array( 'success' => false, "msg" => "CORS failed" ) );
        }
        send_header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    }
    $result = pro_lead_submit();
    if ( $is_ajax ) {
        return json( $result );
    }
    elseif ( $is_jsonp ) {
        return js("{$_REQUEST['callback']}(" .  json_encode( $result ) . ")");
    } else {
        return html( "<h1>Thank you for your interest in Lavu iPad POS</h1>
        <p>One of our representatives will contact with you shortly</p>", "layouts/error.php" );
    }
}


/**
 * This is the function that actually does that actual work, the function above is
 * just a gateway for the request
 */
function pro_lead_submit(&$req=null){
    if ( is_null( $req ) ) {
        $req = &$_REQUEST;
    }
    $requirements = array("name", "company", "phone", "email", );
    $fields = array();
    $email_pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
    foreach( $requirements as $required ){
        if ( empty( $req[$required]) ){
            return array("success" => false, "err_msg" => "Missing Argument: {$required}" );
        }
        $val = mysqli_real_escape_string( ConnectionHub::getConn('poslavu')->getLink('read'), strip_tags( htmlspecialchars ( trim( $req[$required] ) ) ) );
        $fields[$required] = $val;
    }
    // in theory, the form should chekc that email is valid on its own, but just for good measure...
    if ( !preg_match($email_pattern,$fields["email"]) ) {
        return array("success" => false, "err_msg" => "Invalid Email");
    }
    $optionals = array("state", "zip", "city");
    foreach( $optionals as $opt ) {
        if( !empty( $req[$opt] ) ) {
            $val = mysqli_real_escape_string( ConnectionHub::getConn('poslavu')->getLink('read'), strip_tags( htmlspecialchars ( trim( $req[$opt] ) ) ) );
            $fields[$opt] = $val;
        }
    }
    $fields['country'] = empty( $req['country'] ) ? get_country() :$req['country'];
    // I guess now we send the email
    $fields['company_name'] = $fields['company'];
    list( $fields['firstname'], $fields['lastname'] ) = split_name( $fields['name'] );
    $sanitized_domain = preg_replace( '/http(?:s)?:\/\//','', $_SERVER['HTTP_ORIGIN']);
    $sanitized_domain = str_replace(  'www.', '', $sanitized_domain );
    $sanitized_domain = str_replace(  '.com', '',  $sanitized_domain );
    $fields['lead_source'] = $sanitized_domain;
    $fields['level'] = "pro";
    $lead_id = create_or_update_lead( $fields, array('email' => 100 ) );
    if ( !empty( $lead_id ) ) {
        $fields['lead_id'] = $lead_id;
        $response = send_to_salesforce( $fields );
        if ( !empty( $response['id'] ) ) {
            mlavu_query( "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `salesforce_id`='[1]', `salesforce_sent`=NOW() WHERE `id`=[2]", $response['id'], $lead_id );
            $fields['salesforce_id'] = $response['id'];
        }
    }
    $recipients = array("bryan@lavu.com","kalynn@poslavu.com", "tom@lavu.com");
    if ( DEV ) {
        $recipients =  "tom@lavu.com" ;
    }
    $html = partial("emails/pro-pricing-signup.html.php", $fields );
    $txt =  partial("emails/pro-pricing-signup.txt.php", $fields );
    require_once option('root_dir') . "/mailer.php";
    $result = send_multipart_email( "noreply@poslavu.com", "Pro Customer Contact", $html, $txt , $recipients );
    $result['success'] = empty(  $result['failed'] );
    return $result;
}


function live_demo_submit_gateway() {
    $is_jsonp = !empty($_GET) && !empty( $_GET['jsonp'] ) && !empty( $_GET['callback'] );
    $is_ajax = !empty( $_POST ) && !empty( $_REQUEST['ajax'] );
    $is_form = !$is_jsonp && !$is_ajax && !empty( $_POST );
    if ( !$is_jsonp && !$is_ajax && !$is_form ) {
        halt( 400, "Bad Request" );
    }
    if ( $is_ajax ) {
        $origin_domain = empty( $_SERVER['HTTP_REFERER'] ) ?
            $_SERVER['HTTP_ORIGIN'] :
            parse_url( $_SERVER['HTTP_ORIGIN'],  PHP_URL_HOST );
        if ( !authorized_cors_domain( $origin_domain ) ) {
            // not that this response matters. they won't see it since its blocked
            return json( array( 'success' => false, "msg" => "CORS failed" ) );
        }
        send_header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    }
    $result = live_demo_submit();
    if ( $is_ajax ) {
        return json( $result );
    }
    elseif ( $is_jsonp ) {
        return js("{$_REQUEST['callback']}(" .  json_encode( $result ) . ")");
    } else {
        return html( "<h1>Thank you for your interest in Lavu iPad POS</h1>
        <p>One of our representatives will contact with you shortly</p>", "layouts/error.php" );
    }
}

function submit_live_demo() {
    $response = array('success' => false, 'reason' => '');

    // Make sure they domain they're coming from is one registered for CORS
    $http_origin   = isset($_SERVER['HTTP_ORIGIN'])  ? $_SERVER['HTTP_ORIGIN']: '';
    $origin_domain = empty($_SERVER['HTTP_REFERER']) ? $http_origin : parse_url($http_origin,  PHP_URL_HOST);
    /*if ( !authorized_cors_domain( $origin_domain ) ) {
        // not that this response matters. they won't see it since its blocked
        $response['reason'] = 'CORS failed';
        return json($response);
    }*/

    // Transmit CORS header
    send_header("Access-Control-Allow-Origin: {$http_origin}");

    // Get POST'd JSON
    $posted_json = file_get_contents('php://input');
    $data = json_decode($posted_json, true);

    $result = live_demo_submit($data);

    $response['success'] = $result['success'];
    $response['reason'] = isset($result['err_msg']) ? $result['err_msg'] : '';

    return json($response);
}

function live_demo_submit( &$req=null ) {
    $http_origin   = isset($_SERVER['HTTP_ORIGIN'])  ? $_SERVER['HTTP_ORIGIN']: '';
    $origin_domain = empty($_SERVER['HTTP_REFERER']) ? $http_origin : parse_url( $http_origin,  PHP_URL_HOST );
    if ( is_null( $req )){
        $req = &$_REQUEST;
    }
    $requirements = array("name", "company_name", "country", "phone", "email", "state", "zip", "city", "demo_time");
    $optionals = array( "utm_campaign", "lead_source", "utm_source", "utm_medium", "entry_url", "domain", "demo_notes" );
    $fields = array();
    $opts = array();
    $email_pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
    foreach( $requirements as $required ){
        if ( empty( $req[$required] ) ){
            return array( "success" => false, "err_msg" => "Missing Argument: {$required}" );
        }
        $val = mysqli_real_escape_string( ConnectionHub::getConn('poslavu')->getLink('read'), strip_tags( htmlspecialchars ( trim( $req[$required] ) ) ) );
        $fields[$required] = $val;
    }
    foreach( $optionals as $optional ){
        if ( !empty( $req[$optional] ) ) {
            $val = mysqli_real_escape_string( ConnectionHub::getConn('poslavu')->getLink('read'), strip_tags( htmlspecialchars ( trim( $req[$optional] ) ) ) );
            $opts[$optional] = $val;
        }
    }
    if ( empty( $opts['domain'] ) ) {
        $opts['domain'] = $origin_domain;
    }
    $fields['lead_source'] = $origin_domain;
    date_default_timezone_set('America/Chicago');
    $subject = "Live Demo Request ({$origin_domain})";
    $message = " *** Geo Data: Country::{$fields['country']}\nState::{$fields['state']}\nZip::{$fields['zip']} ***";
    $message = date("Y-m-d H:i:s").":*:{$subject}:*:{$message}*|*";
    $fields["notes"] = $message;
    if ( !preg_match($email_pattern,$fields["email"]) ) {
        return array("success" => false, "err_msg" => "Invalid Email");
    }
    if ( !empty($opts['demo_notes']) ) {
        $opts['demo_notes'] = str_replace( array("\\r\\n", "\\n", "\\r"), ' ', $opts['demo_notes'] );
        if (strlen($opts['demo_notes']) > 255 ) $opts['demo_notes'] = substr($opts['demo_notes'], 0, 255);
    }
    //submit the lead to the database
    //$dbid = create_or_update_lead( $fields, true);
    //if ( $dbid ) {
    list( $firstname, $lastname ) = split_name( $fields['name'] );
    try {
        require_once "/home/poslavu/public_html/register/lib/salesforce/SalesforceLead.php";
        require_once "/home/poslavu/private_html/salesforceCredentials.php";
        $credentials = new SalesforceLogin( SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET );
        $lead_handler = new SalesforceLead( $credentials );
        if ( empty( $lead_handler ) ){
            $lead_handler = new SalesforceLead( $credentials );
        }
        $response = $lead_handler->insert( array(
            "FirstName" => empty( $firstname ) ? "N/A": $firstname,
            "LastName" => empty( $lastname ) ? "N/A" : $lastname,
            "Phone" => $fields['phone'],
            "Country__c" => $fields['country'],
            "Company" => $fields['company_name'],
            "Email" => $fields['email'],
            "Description__c" => $fields['notes'],
            "Zip_Postal_Code__c" => $fields['zip'],
            "State_Provence__c" => $fields['state'],
            "Campaign_Medium__c" => empty( $opts['utm_campaign'] ) ? "" : $opts['utm_campaign'],
            "Campaign_Name__c" => empty($opts['utm_name'])? "" : $opts['utm_name'],
            "Campaign_Source__c" => empty($opts['source'])? "" : $opts['source'],
            "Demo_Time__c" => $fields['demo_time'],
            "Landing_Page__c" => empty( $opts['entry_url'] ) ? "" : $opts['entry_url'],
            "Live_Demo_Notes__c" => empty( $opts['demo_notes'] ) ? "" : $opts['demo_notes'],
            "LeadSource" => $opts['domain']
        ));
    } catch( SalesforceHandlerException $e ) {
        $send_email = sendDuplicateLeadEmail('Live Demo', $fields);
        mail("tom@lavu.com", "Salesforce Error", $e->getMessage() ."\n\n". print_r( $_REQUEST, true ) ."\n\n". print_r($fields, true) ."\n\nsend_email={$send_email}");
        $response['success'] = false;
    }
    //}
    // I guess now we send the email
    $recipients = array( "sales@poslavu.com", "tom@lavu.com", "josh@lavu.com" );
    if ( DEV ) {
        $recipients =  "tom@lavu.com" ;
    }
    $fields['demo_notes'] = isset($opts['demo_notes']) ? $opts['demo_notes'] : '';
    $fields['salesforce_lead_id'] = isset($response['id']) ? $response['id'] : '';
    $html = partial("emails/live-demo-request.html.php", $fields );
    $txt =  partial("emails/live-demo-request.txt.php", $fields );
    require_once option('root_dir') . "/mailer.php";
    $result = send_multipart_email( "noreply@poslavu.com", $subject, $html, $txt , $recipients );
    $result['success'] = empty(  $result['failed'] );
    return $result;
}
