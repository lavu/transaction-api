<?php
    session_start();
    
    include_once("../admin/cp/resources/core_functions.php");

    if ($_SERVER['SERVER_PORT']!=443) // quit if not ssl
    {
        exit();
    }
    if (strpos($_SERVER['HTTP_HOST'],"register.poslavu.com")===false) // quit if not on register.poslavu.com
    {
        exit();
    }

    function generateRandomToken($size)
    {
    	$result = "";
    	for($i = 0; $i < $size; $i++)
    	{
    	
    		//26 + 26 + 10 = 62
	    	$rand = rand(0,61); // 0-9 a-z A-Z
	    	
	    	if($rand < 10)
	    	{ //Digit 0 - 9
		    	$result .= chr(48 + $rand);
	    	}
	    	else if($rand < 36)
	    	{ // a-z 10 - 35
		    	$result .= chr(97 + $rand - 10);
	    	}
	    	else
	    	{ // A-Z 36 - 61
		    	$result .= chr(65 + $rand - 36);
	    	}
	    	
	    }
	    
	   return $result;
	    
    }
  
	if(isset($_REQUEST['signup']))
	{
		$signup_token = $_REQUEST['signup'];
		$destory_token_query= "update `poslavu_MAIN_db`.`signups` set `verification_token`= '[2]' where `verification_token`='[1]' limit 1";
		$signup_query = "SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `verification_token` like '[1]%' and `verification_token` != '[2]'  ORDER BY `id` DESC LIMIT 1";
		
		$signup_result = mlavu_query($signup_query, $signup_token, $signup_token.'12355');
		
		if(mysqli_num_rows($signup_result))
		{
			
			$almostDoneQuery= mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `verification_token` = '[1]' ORDER BY `id` DESC LIMIT 1",$signup_token) ;		
			
			if(mysqli_num_rows($almostDoneQuery)){
				
				date_default_timezone_set('UTC');
				mlavu_query($destory_token_query,$signup_token, $signup_token.date("Y-m-d H:i:s") );
			
				echo '<!DOCTYPE HTML>
						<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
						<head>
						    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						
						    <title>POS Lavu iPad Point of Sale | New Signup</title>
						    <link rel="stylesheet" href="vf/styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <link rel="stylesheet" href="vf/styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <link rel="stylesheet" href="vf/styles/register.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript">
						</script>
						    <script src="vf/scripts/jquery.tools.min.js" type="text/javascript" charset="utf-8"></script>
						    <script src="vf/scripts/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
						       <script src="vf/scripts/register.js" type="text/javascript" charset="utf-8"></script>
						</head>
						
						<body>
						    <div class="content ccentered">
						        <!-- Information Section -->
						        <div class="ccentered container" id="container">
						        <div class="title_header">
									<div class="title_header_small"><div class="shade_left"></div></div>
									<div class="title_header_middle">
										<div class="logo_lavu_lite"></div>
									</div>
									<div class="title_header_small"><div class="shade_right"></div></div>
								</div>

					';
			
				$res = mysqli_fetch_assoc($almostDoneQuery);
				$_POST['firstname'] = $res['firstname'];
				$_POST['lastname'] = $res['lastname'];
				$_POST['company'] = $res['company'];
				$_POST['email'] = $res['email'];
				$_POST['rowid'] = $res['id'];
				$_POST['default_menu'] = $res['default_menu'];
				$_POST['package'] = '4';
				$_POST['posted'] = 1;
				$_POST['vf_signup'] = 1;
				$_POST['vf_skip'] = 1;
				$_POST['ip']=urlencode($_SERVER['REMOTE_ADDR']);

				$_POST['verification_token']= $signup_token;
				
				mysqli_free_result($signup_result);
				
				if(isset($_GET['fromAPP']))
					$_POST['fromAPP']=1;
				
				require_once('create_lavulite_account.php');
				echo '</div>';
			}
			else{
				
				//This means that the user almost finished but didnt finish. 
				//so all they have left to do is to enter their password
				header("location: https://admin.lavulite.com/completeRegistration.php?token=".$signup_token."&fromAPP=".$_POST['fromAPP']."&sailPay=".$_POST['vf_skip']);
				
			}
			
		}
		else
		{echo '<!DOCTYPE HTML>
						<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
						<head>
						    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						
						    <title>POS Lavu iPad Point of Sale | New Signup</title>
						    <link rel="stylesheet" href="vf/styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <link rel="stylesheet" href="vf/styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <link rel="stylesheet" href="vf/styles/register.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript">
						</script>
						    <script src="vf/scripts/jquery.tools.min.js" type="text/javascript" charset="utf-8"></script>
						    <script src="vf/scripts/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
						       <script src="vf/scripts/register.js" type="text/javascript" charset="utf-8"></script>
						</head>
						
						<body>
						    <div class="content ccentered">
						        <!-- Information Section -->
						        <div class="ccentered container" id="container">
						        <div class="title_header">
									<div class="title_header_small"><div class="shade_left"></div></div>
									<div class="title_header_middle">
										<div class="logo_lavu_lite"></div>
									</div>
									<div class="title_header_small"><div class="shade_right"></div></div>
								</div>

					';

			echo '
				<div class="banner">
				    <br />
				    <br />
				    <h2>Unable to Activate!</h2>
				    <h3>This Activation Link Was Already Used, or it is No Longer Available.</h3>
				</div>
				<br />
				<div class="ccentered form_area">
				 </div></body></html>';					
		}
	
	}
	else
	{echo '<!DOCTYPE HTML>
						<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
						<head>
						    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						
						    <title>POS Lavu iPad Point of Sale | New Signup</title>
						    <link rel="stylesheet" href="vf/styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <link rel="stylesheet" href="vf/styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <link rel="stylesheet" href="vf/styles/register.css" type="text/css" media="screen" title="no title" charset="utf-8" />
						    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript">
						</script>
						    <script src="vf/scripts/jquery.tools.min.js" type="text/javascript" charset="utf-8"></script>
						    <script src="vf/scripts/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
						       <script src="vf/scripts/register.js" type="text/javascript" charset="utf-8"></script>
						</head>
						
						<body>
						    <div class="content ccentered">
						        <!-- Information Section -->
						        <div class="ccentered container" id="container">
						        <div class="title_header">
									<div class="title_header_small"><div class="shade_left"></div></div>
									<div class="title_header_middle">
										<div class="logo_lavu_lite"></div>
									</div>
									<div class="title_header_small"><div class="shade_right"></div></div>
								</div>

					';

		echo '
			<div class="banner">
			    <br />
			    <br />
			    <h2>No Application Reference!</h2>
			    <h3>This Page Was Reached With No Activation Credentials.  Probably accessed in Error.</h3>
			</div>
			<br />
			<div class="ccentered form_area">
			</div> </div></body></html>';
	}
	
	
	function doPasswordUpdate($token){
		
	}
