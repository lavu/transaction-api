<?php 
if ( php_sapi_name() == 'cli' || PHP_SAPI == 'cli' ) {
	define ( "WEB", false );
} else  define( "WEB", true );
if ( WEB ){
	exit;
	?>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>email-scheduler</title>
<style type="text/css" media="screen">

table,td,th,tr{
	border:1px solid black;
}

table{
	border-collapse:collapse;
	margin:20px auto;
}
</style>
</head>	<a href="mail-test.php" id="" title="mail-test">mail-test</a>
<body id="email-scheduler" onload="">

	<?php
}
// Context:
// This script will be run from a crontab
// It would have to be hosted at register.poslavu.com

require_once( "/home/poslavu/public_html/admin/cp/resources/lavuquery.php" );
// 1. get current (unix?) timestamp

function print_row_cell( $item, $key ) {
	if ( WEB ) echo "<td>$item</td>";
	else echo str_pad( $item, 30 ) . ' |';
}

function print_table_header () {
	if ( !WEB )
		echo <<<TXT

-------------------------------------------------------------------------
| EMAIL         | SIGNUP TIMESTAMP |   DATE                             |
-------------------------------------------------------------------------

TXT;
    else echo
		"<table><tr><th>Email</th><th>Signup Timestamp</th><th>Date</th></tr>";
	
}

$curr_time = time();
// 2. get a list of all the pertinent days to send emails to ( day after, 5 days after, 5 days before, day before, etc)
// number of seconds in a day
$unix_day = 24 * 3600;
// date format 
$f = "Y-m-d";
// get today in sql format
$today = date( $f, $curr_time );
// get yesterday in sql format
$yesterday = date( $f, $curr_time - $unix_day );
// get 5 days ago in sql format
$_5_days_ago = date( $f, $curr_time - 5 * $unix_day );
// get 10 days ago in sql format
$_10_days_ago = date( $f, $curr_time - 10 * $unix_day );
//get 13 days ago
$_13_days_ago = date( $f, $curr_time - 13 * $unix_day );
// 3. for each period get all the `signups` entries that are inside the pertinent period go in to loop:
$periods = array( $today, $yesterday, $_5_days_ago, $_10_days_ago, $_13_days_ago );
foreach( $periods as $period ) {
//	 3.1 load html and txt content for the e-mail
    $result =  mlavu_query( "select email, signup_timestamp, date from signups where signup_timestamp like '[period]%'", array( 'period' => $period ) );
	 //   3.2 for each entry go into loop:
    print_table_header();
	while ( $row =  mysqli_fetch_assoc( $result ) ) {
		echo WEB? "<tr>" : '| ';
//     3.2.1 get email address
        array_walk(  $row, 'print_row_cell' );
//     3.2.2 initialize swift-mailer
//     3.2.3 send e-mail
//   3.2-- end of loop
        echo WEB? "</tr>" : "\n";
    }
	echo WEB?"</table>" : str_repeat( '-', 77 );
// 3-- end of loop 
}
if ( WEB ) {
	echo <<<HTML
</body>
</html>
HTML;
}
?>