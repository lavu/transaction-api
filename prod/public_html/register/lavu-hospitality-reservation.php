<?php
// TODO November 1st 2013 : This is the page to make the reservations for
// lavuhospitality.com
// There are many ways we could make this work, here are the most viable
// options:
//     1. Pass the parameters from the microsite to this one in a form and
//        and use this as end message.
//     2. Pass the parameters with cross-domain AJAX (JSONP) and return the
//        message as the callback HTML.
// We'll work on this on Monday
/*
+--------------+------------------+------+-----+---------+----------------+
| Field        | Type             | Null | Key | Default | Extra          |
+--------------+------------------+------+-----+---------+----------------+
| id           | int(10) unsigned | NO   | PRI | NULL    | auto_increment | 
| email        | varchar(256)     | NO   |     | NULL    |                | 
| name         | text             | NO   |     | NULL    |                | 
| phone_number | varchar(256)     | YES  |     |         |                | 
| referrer     | varchar(256)     | NO   |     | NULL    |                | 
| type         | text             | YES  |     | NULL    |                | 
+--------------+------------------+------+-----+---------+----------------+
6 rows in set (0.00 sec)
*/
ini_set("display_errors",1);
$libs_folder = "/home/poslavu/public_html/admin/cp/resources";
include_once("$libs_folder/core_functions.php");
include_once("$libs_folder/lavuquery.php");

function insert_entry_hospitality_waiting_list() {
    $fields = array();
    $fields["db"] = '`poslavu_MAIN_db`.`reservations`';
    $fields["name"] = $_REQUEST["name"];
    $fields["phone_number"] = $_REQUEST["phone"];
    $fields["email"] = $_REQUEST["email"];
    $fields["referrer"] = $_REQUEST["referrer"];
    $fields["type"] = "hospitality";
    $query = <<<QRY
INSERT INTO [db]
    (`name`, `email`, `phone_number`, `referrer`, `type`)
VALUES ('[name]', '[email]', '[phone_number]', '[referrer]','[type]')
QRY;
    return mlavu_query( $query, $fields );
}

function check_request_variables(){
  $to_check = array( "name", "email", "phone", "referrer",  "callback");
  foreach ( $to_check as $var_to_check ) {
    if ( empty( $_REQUEST[$var_to_check] ) ) {
      return false;
    }
  }
  return true;
}

function json_response() {
  header('Content-type: text/javascript');
  return "{$_REQUEST["callback"]}({success:true})";
}

if ( isset( $_REQUEST ) ) {
  //check that all variables are properly set
  if ( check_request_variables() === true ) {
    $success = insert_entry_hospitality_waiting_list();
    if ( $success ) {
      echo json_response();
      require_once 'swift-mail/swift_required.php';
      $message = Swift_Message::newInstance()
        ->setSubject('Thank you for registering to the Lavu Hospitality Waiting List')
        ->SetFrom( array( 'info@poslavu.com' => 'Lavu Inc' ) )
        ->setTo( array( $_REQUEST['email'] ) )
        ->setBody( file_get_contents("hospitality-confirmation-email.txt")  )
        ->addPart( file_get_contents("hospitality-confirmation-email.html"), 'text/html'  );
      $transport = Swift_MailTransport::newInstance();
      $mailer = Swift_Mailer::newInstance($transport);
      $mailer->send( $message );
      // Send e-mail to sales@poslavu.com
      $sales_message = Swift_Message::newInstance()
          ->setSubject('Hospitality Waiting List')
          ->setFrom( array( 'info@poslavu.com' => 'Lavu Inc') )
	  ->setTo( 'reservations@poslavu.com' )
	//	  ->setBcc( 'carlosa@poslavu.com' )
	  ->setBody( "Please contact:
    - {$_REQUEST['name']}
    - {$_REQUEST['email']}
    - {$_REQUEST['phone']}
Note: if the email is carlosa@poslavu.com, then this a test and it should be ignored");
      $mailer->send( $sales_message );
      exit;
    }
  } else {
    echo '<h2 style="font-family:sans-serif;color:#2B2B2B;text-align:center;padding-top:100px;">Not enough parameters</h2>';
  }
 } 
  echo '<h2 style="font-family:sans-serif;color:#2B2B2B;text-align:center;padding-top:100px;">Badly formed  request</h2>';
?>