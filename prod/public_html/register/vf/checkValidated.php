<?php
	/*
		The purpose of this file is to check and see if the user has verified and validated their account. if they have then what
		we do is take their initial verification token and modify it to add a time stamp to it. 
		so if the token is found then they have not yet validated. 
		but if we can match a 'like' search to it then they have verified their account. 
	*/
	if(!isset($_POST['token']) || $_POST['token']==''){
		echo 'noToken';
		return;	
	}
	require_once("../../admin/cp/resources/core_functions.php");
	
	if(mysqli_num_rows(mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `verification_token` ='[1]'", $_POST['token']))){
		echo 'failure';
		return;
	
	}
	else if(mysqli_num_rows(mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `verification_token` like '[1]%' ", $_POST['token']))){
	
		$query=mlavu_query("select `username`, AES_DECRYPT(`password`,'lavupass') as `password` from `poslavu_MAIN_db`.`signups` where `verification_token` like '[1]%' ", $_POST['token']);
		$result=mysqli_fetch_assoc($query);
		
		if($result['username']!='' && $result['password']!=''){
		
			echo 'un='.$result['username'].'&pw='.$result['password'];
			return;
		
		}else{
			echo 'noUser';
			return;
		}
	}else{
		echo 'error';
		return;
	}

?>