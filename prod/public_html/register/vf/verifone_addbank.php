<?php
	ini_set('display_errors',1);
	session_start();
	$errorsFound = false;
	
	include_once("../../admin/cp/resources/core_functions.php");
	include_once("queries/verifone_info.php");
	
	$params = Array( 'app_id' => 'app_id',
					 'api_token' => 'api_token',
					 'api_secret' => 'api_secret',
					 'account_holder' => 'account_holder',
					 'routing_num' => 'routing_num',
					 'nickname' => 'nickname',
					 'account_num' => 'account_num',
					 'account_type' => 'account_type'
					  );
					  
	$translation = Array( 'app_id' => 'Application ID',
					 'api_token' => 'API Token',
					 'api_secret' => 'API Secret',
					 'account_holder' => 'Account Holder',
					 'routing_num' => 'routing Number',
					 'nickname' => 'Account Nickname',
					 'account_num' => 'Account Number',
					 'account_type' => 'Account Type'
					  ); 
					  
	$display = Array(   'api_token' => 'api_token',
						'api_secret' => 'api_secret',
						'username' => 'username',
						'password' => 'password',
						'client_id' => 'client_id',
						'email' => 'email'
						);
					 
	$missingParams = Array();
	
	function getParams()
	{
		global $params;
		global $missingParams;
		
		if(validate($params))
		{
			$pars = "";
			
			foreach($params as $key => $value)
			{
				if($pars != "")
					$pars .= "&";
				$pars .= "$key=$value";
			}
			//echo $pars . "<br />";
			handleBankAdd(post_to_verifone($pars, "banks/add"));
			//handleActivate(post_to_verifone($pars, "activate"));
		}
		else
		{
			echo "<div class='errors ccentered w80p' style='display: block;' >";
			echo "<b>The Following Parameters Were Missing:</b><br /><br />";
			foreach($missingParams as $param)
			{
				echo "<li>$param</li>";
			}
			echo "</div>";
		}
	}
	
	function handleBankAdd($response)
	{
		global $params;
		global $display;
		global $translation;
		//echo $response;
		$res = json_decode($response);
		if($res[0]['status'] == "Success")
		{
			validate($display);
			//Bank has been successfully added, display credentials.
			foreach($display as $key => $value)
			{
				if($key == 'email')
					continue;
				echo "<label class='full'>Verifone " . str_replace("_", " ", $key)	 . "</label>";
				echo "<div class='full'>$value</div><br />";
			}
			
			//Send an email with the Verifone Credentials to the Supplied Email Address
			$email_subject = "Your Verifone SailPay Credentials";
			
			$table =  "<table style=\"border-collapse:collapse; border: 0px;\">
						<tbody>";

			foreach($display as $key => $value)
			{
				$table .= "
							<tr>
								<td style='text-align: right;'><b>Verifone " . str_replace("_", " ", $key) . ":</b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td style='text-align: left;'>" . $value . "</td>
							</tr>";
			}	
									
			$table .= "
						</tbody>
					</table>";
									
			 $email_to  = $display['email'];
 
  // subject 
 $email_subject = "Your Verifone SailPay Credentials"; 
 
 date_default_timezone_set("America/New_York");
 // Create a boundary string.  It needs to be unique 
 $sep = sha1(date('r', time()));
 
 $boundary = "lavulite-related-boundary";

 $email_headers = "From: noreply@lavulite.com\r\n"; 
 // Add in our content boundary, and mime type specification:
 $email_headers .= "Content-Type: multipart/related;\r\n boundary=\"$boundary\"\r\n";
 
 $image1 = chunk_split(base64_encode(file_get_contents('images/lavu_email_logo.png')));
 $image2 = chunk_split(base64_encode(file_get_contents('images/sailpay_email_logo.png')));
 
 // Your message here:
$email_body =<<<EOBODY
--$boundary
Content-Type: text/html; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<style type="text/css">
			p{
				text-align: justify;
			}
		</style>
		<div style="max-width: 722px; min-width: 500px;">
			<div style="width: 100%; position: relative; height: 144px;">
				<img src="cid:sailpay_logo" style="position: absolute; top: 20px; left: 20px; width: 97px; height: 105px;"></img>
				<img src="cid:lavulite_logo" style="position: absolute; top: 0px; right: 20px; width: 384px; height: 144px;"></img>
			</div>
			<p>You have successfully signed up for SAIL by Verifone® via the Lavu Lite sign-up process.  The following are the SAIL Credentials that were generated via the sign-up process:</p>
				$table
			<br />
			<p>You can log into your SAIL account at  <a href="https://www.sailpay.com">sailpay.com</a> once you have validated your email using the link in the email from SAILPay.  You can view amounts to be deposited and check on your current transactions through this SAIL portal at anytime.</p>
			<p><i><u>You must also enter the micro-deposit amount SAIL has made into your bank account.  You will not be able to receive deposits until you do this.   You will receive an email from SAIL notifying you when this deposit process has started.</u></i></p>
			<p>If you have any issues using your processing or setting up your SAIL account, please contact the SAIL support department at:</p>
			<br />
				<div style="width: 300px; margin: 0 auto; text-align: center;">
				<b>855-SAIL-PAY (855-7245-729)</b><br /><br />
				or<br /><br />
				<b><a href="mailto:support@sailpay.com">support@sailpay.com</a></b><br /><br />
				</div>
			<br />
			<p>SAIL is a separate service, and is not required to use Lavu Lite.  Phone support for SAIL is limited to that service.  Any questions or issues with Lavu Lite should be resolved by visiting the Lavu Lite section in Help Center at help.lavu.com or submitting a ticket through your Lavu Lite Control Panel.</p>
		</div>
	</body>
</html>

--$boundary
Content-Type: image/png
	 name="lavu_email_logo.png"
Content-Transfer-Encoding: base64
Content-ID: <lavulite_logo>
Content-Disposition: inline;
	 filename="lavu_email_logo.png"
	
$image1
--$boundary
Content-Type: image/png
	 name="sailpay_email_logo.png"
Content-Transfer-Encoding: base64
Content-ID: <sailpay_logo>
Content-Disposition: inline;
	 filename="sailpay_email_logo.png"
	
$image2
--$boundary--
EOBODY;
 mail($email_to, $email_subject, $email_body, $email_headers);
			
			//mail($display['email'],$email_subject,$email_body, $email_headers);
			
			echo '<script type="text/javascript">verifone_added_bank_account=true;</script>';
			
		}
		else
		{
			global $errorsFound;
			$errorsFound = true;
			echo "<div class='errors ccentered w80p' style='display: block;' >";
			echo "<b>The Following Errors Were Encountered While Attempting to Add The Bank Account:</b><br /><br />";
			//echo $response . "<br />";
			$errors = $res[0]['errors'];
			foreach($errors as $k => $e)
			{
				$reason = $e['reason'];
				foreach($translation as $o => $t)
				{
					$reason = str_ireplace($o, $t, $reason);
				}
				if($error['code'] == "error.customer_service" )
				{
					echo "<li>An Error Occurred while processing the request, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";
				}
				else if($error['code'] == "internal.customer_service" )
				{
					echo "<li>An Internal Data Error Occurred, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";			
				}
				else
					echo "<li>" . $reason . "</li>";
			}
			echo "</div>";
		}
	}
	
?>
<div class="title_header">
	<div class="title_header_small"><div class="shade_left"></div></div>
	<div class="title_header_middle">
		<div class="logo_lavu_lite_sail"></div>
	</div>
	<div class="title_header_small"><div class="shade_right"></div></div>
</div>
<div class="banner">
    <br />
    <br />
    <h2>Add Verifone's SAILpay, get a free swipe.</h2>
    <h3>No Per-Transaction Fee, No Monthly Fees.</h3>
</div>
<br />
<div class="form_area ccentered">
	<br />
	<br />
	<br />
	<div class="step_info" style="text-align: left">
    	<div class="stepnum step7"></div><span>Setup your SAILpay credit card processing account.</span>
    </div>
    <br />
    <br />
    <div id="vf_bank_result">
<?php
	echo getParams();
?>
    </div>
</div>

<div style="height: 50px;"></div>

<div class="divider"></div>

<div class="footer">
	<button class="back" type="button" id="step6back" onclick="backStep()" >Back</button>
    <?php
    	//$(\'#new_customer\').submit();
    	if(!$errorsFound)
    	{
    		echo '<button class="next" type="button" id="step6complete" onclick="$(\'#new_customer\').submit();" >Finish</button>';
    	}
    ?>
</div>
	
