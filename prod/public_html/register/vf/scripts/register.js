function Set(){
	var result = Array();
	result.addUnique = function(str) {
		var add = true;
		for (var i = 0; i < this.length; i++) {
			if (this[i] === str) {
				add = false;
				break;
			}
		}
		if (add) this.push(str);
	};
	result.asOptions = function() {
		var result = "";
		for (var i = 0; i < this.length; i++) {
			result += "<option>" + this[i] + "</option>";
		}
		return result;
	};
	return result;
}

function Dict(){
	var result = {};
	result.keys = Array();
	result.values = Array();
	
	var add = function(key, value)
	{
		this.keys.push(key);
		this.values.push(value);
	};
	
	var asOptions = function()
	{
		var result = "";
		for(var i = 0; i < this.keys.length; i++)
		{
			result += "<option value='" + this.values[i] + "'>" + this.keys[i] + "</option>";
		}
		return result;
	};
	
	result.add = add;
	result.asOptions = asOptions;
	
	return result;
}

function isSSNValid(ssn)
{
	if(ssn === "")
		return false;
	var numbers = numbersFromStr(ssn);
	if(numbers.length !== 4)
		return false;
	return true;
}

function verifyPassword(pwd)
{
	var result = {};
	if(pwd === "")
	{
		result.valid = false;
		result.reason = "No Password Entered.";
		return result;
	}
	
	if(pwd.length < 8)
	{
		result.valid = false;
		result.reason = "Password Must be at Least 6 Characters Long.";
		return result;
	}
	
	var nums = numbersFromStr(pwd);
	if(nums.length < 1)
	{
		result.valid = false;
		result.reason = "Password Must Contain at Least 1 Number.";
		return result;
	}
	
	if(pwd.length - nums.length < 1)
	{
		result.valid = false;
		result.reason = "Password Must Contain at Least 1 Character.";
		return result;
	}
	
	for(var i = 0; i < pwd.length; i++)
	{
		if(pwd.charAt(i) === ' ')
		{
			result.valid = false;
			result.reason = "Password Must Not Contain Spaces.";
			return result;
		}
	}
	
	
	result.valid = true;
	result.reason = "Password is A OK.";
	return result;
}

function listYears()
{
	var result = Set();
	var d = new Date();
	var year = d.getFullYear();
	result.addUnique("");
	for(var i = 100; i > 0; i--)
	{
		result.addUnique(year--);
	}
	
	return result.asOptions();
}

function listYears10()
{
	var result = Set();
	var d = new Date();
	var year = d.getFullYear();
	for(var i = year; i < year + 10; i++)
	{
		result.addUnique(i);
	}
	return result.asOptions();
}

function listMonths()
{
	var result = Set();
	result.addUnique("");
	for(var i = 1; i < 13; i++)
	{
		result.addUnique(i);
	}
	
	return result.asOptions();
}

function listDays()
{
	var result = Set();
	result.addUnique("");
	for(var i = 1; i < 32; i++)
	{
		result.addUnique(i);
	}
	
	return result.asOptions();
}


var previousValue = Array();
function handleZip(textBox, id1) {
	var f = function(html1) {
			$('#' + id1).html(html1);
			$('#' + id1).attr("style", "display: '';");
			$('#' + id1).change();
		};
	if (textBox.value !== ""  && textBox.value.length === 5)
	{
		if(previousValue[textBox.id])
		{
			if(previousValue[textBox.id] !== textBox.value)
			{
				previousValue[textBox.id] = textBox.value;
				checkZip(textBox.value, f);
			}
		}
		else
		{
			previousValue[textBox.id] = textBox.value;
			checkZip(textBox.value, f);
		}
		
	}
}

function checkZip(zip, f) {
	var url = "/vf/queries/zip_code.php";
	$.ajax({
		type: "GET",
		url: url,
		data: {
			"zip_code": zip
		},
		dataType: "json",
		cache: false,
		success: function(json_return) {
			if (json_return && (json_return.status === "Success")) {
				// $.each
				var options = Dict();
				//options.add("", "");
				$.each(json_return.cities, function(i, row) {
					//row.city
					//row.statej
					var json = '{"city" : "' +  row.city + '", "state" : "' + row.state + '"}';
					options.add(row.city + ", " + row.state, json);
				});
				f(options.asOptions());
			} else {
				//json error
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			
		}
	});
}

function updateZip(ele, id1, id2)
{
	var json = ele.value;
	var obj = JSON && JSON.parse(json) ||$.parseJSON(json);
	
	if(obj && obj.city && obj.state){
		$('#' + id1).val(obj.city);
		$('#' + id2).val(obj.state);
	}
}

var lastemail = "";
var lastemailstatus = false;

function isEmailValid(str) {
	var atfound = 0;
	var dotfound = 0;
	var result = false;
	for (var i = 0; i < str.length; i++) {
		if (str.charAt(i) == '@') atfound = i;
		if (str.charAt(i) == '.') dotfound = i;
	}
	result = (atfound < dotfound) && (atfound > 0) && (dotfound < str.length - 1) && (Math.abs(atfound - dotfound) > 1);

	if(!result)
	{
		result =  false;
	}
	else{	
	//CHECK TO SEE IF EMAIL IS ALREADY IN USE.
		if(lastemail === str)
		{
			return lastemailstatus;
		}
		
		$.ajax({
			type: "POST",
			url : "/vf/queries/email_check.php",
			data : { "email" : str},
			dataType: "json",
			cache: false,
			async: false,
			success: function(json_return){
				if(!json_return){
					result = false;
				}
				else if(json_return.status !== "available"){
					result = false;
					if($('#emailerror'))
					{
						$('#emailerror').remove();
					}
					$('#email').after('<div class="errors w80p ccentered" id="emailerror" style="display: block;">This Email is Already Taken.</div>');
					$('#container').animate({height: $('#step' + (section + 1)).height(), duration: 1000, queue: false});
				}
				else
				{
					result = true;
					if($('#emailerror'))
					{
						$('#emailerror').remove();
						$('#container').animate({height: $('#step' + (section + 1)).height(), duration: 1000, queue: false});
					}
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				
			}
		});
		
	}
	lastemail = str;
	lastemailstatus = result;
	
	return result;
}

function isPhoneValid(str) {
	if (str === '') return false;
	var numbers = numbersFromStr(str);
	if (numbers.length != 10) return false;
	return true;
}

function numbersFromStr(str)
{
	var patt = new RegExp('[0-9]', 'g');
	var nums = str.match(patt);
	var numbers = '';
	if(nums)
		for (var i = 0; i < nums.length; i++) {
			numbers = numbers + nums[i];
		}
	return numbers;
}
var step1errorcheck = false;
var step2errorcheck = false;
var step3errorcheck = false;
var step4errorcheck = false;
var step5errorcheck = false;
var focuscheck = false;
var section = 0;

function lostFocus() {
	focuscheck = true;
	if (step1errorcheck && section === 0) step1check();
	else if (step2errorcheck && section === 1) step2check();
	else if (step3errorcheck && section === 2) step3check();
	focuscheck = false;
	
	//$('#scrollarea').animate({left: -1 * section * $('.step').width() + "px", duration: 1000, queue: false});
	//$("#scrollarea").css("left", (-$('.step').width() * section) + "px");
}

function nextStep(ele) {

	var func;
	switch (section) {
	case 0:
		func = step1check;
		break;
	case 1:
		func = step2check;
		break;
	case 2:
		func = step3check;
		break;
	case 3:

		func = step4check;
		break;
	case 4:
		func = verifone_validate;
		break;
	case 5:
		func = verifone_bank_validate;
		break;
	default:
		break;
	}
	
	if(func && func(ele))
	{
		//$('#scrollarea').css('left',-1 * (section - 1) * $('.step').width() + "px");
		animating = true;
		$('body').animate({scrollTop: 0, duration: 1000, queue: false});
		$('#scrollarea').animate({left: -1 * section * $('.step').width() + "px", duration: 1000, queue: false});
		$('#container').animate({height: $('#step' + (section + 1)).height(), duration: 1000, queue: false});
		//$('#container').height($('#step' + (section + 1)).height());
		
		if(ele!='lavu_lite' && ele)
			setTimeout(function(){ele.focus();},1000);
		setTimeout(function(){animating = false;},1000);
	}
			
	//$("#scrollarea").css("left", (-$('.step').width() * section) + "px");
	

}

function skipVerifone(ele)
{
	section = 3;
	$('#vf_skip').val(1);
	if(ele)
	{
		setTimeout(function(){ele.focus();}, 1000);
	}
	//$("#scrollarea").css("left", (-$('.step').width() * section) + "px");
	animating = true;
	$('body').animate({scrollTop: 0, duration: 1000, queue: false});
	$('#scrollarea').animate({left: -1 * section * $('.step').width() + "px", duration: 1000, queue: false});
	$('#container').animate({height: $('#step' + (section + 1)).height(), duration: 1000, queue: false});
	//$('#container').height($('#step' + (section + 1)).height());
	setTimeout(function(){animating = false;}, 1000);
}

function skipAndSubmit()
{
	$('#vf_skip').val(1);
	skipped();
}

var animating = false;
function keyDown(prevEle, nextEle)
{
	var e = window.event ? event : e;
			
	if(animating)
		$(this).focus();
	else if(e.keyCode == 9){ // TAB
		if(e.shiftKey) // Shift tab
		{
			if(prevEle)
			{
				backStep(prevEle);
				//setTimeout(function(){prevEle.focus(); window.scroll(0,0); animating = false;}, 1000);
			}
			
		}
		else //Shift Forward
		{
			if(nextEle)
			{
				nextStep(nextEle);
				//setTimeout(function(){nextEle.focus(); window.scroll(0,0); animating = false;}, 1000);
			}
		}
	}
	else if(e.keyCode == 13) // Enter
	{
		if(nextEle)
		{
			nextStep(nextEle);
			//setTimeout(function(){nextEle.focus(); animating = false;}, 1000);
		}
	}
}

var previousSelection;
function select(ele)
{
	if(previousSelection)
		previousSelection.className = "selection";
		
	ele.className = "selection selected";
	previousSelection = ele;
	$('#template').attr("value", ele.id);
		
}

function backStep(ele) {
	if(section == 3 && $('#vf_skip').val() == 1)
	{
		section = 1;
		ele = $('#password');
	}
	else
		section = Math.max(0, section - 1);
	//$("#scrollarea").css("left", -1 * section * $('.step').width() + "px");
	animating = true;
	
	$('body').animate({scrollTop: 0, duration: 1000, queue: false});
	$('#scrollarea').animate({left: -1 * section * $('.step').width() + "px", duration: 1000, queue: false});
	$('#container').animate({height: $('#step' + (section + 1)).height(), duration: 1000, queue: false});//
	//$('#container').height($('#step' + (section + 1)).height());

	if(ele)
	{
		setTimeout(function(){ele.focus();},1000);
	}
	
	setTimeout(function(){animating = false;}, 1000);
}

function addErrorClass(ele)
{
	if(ele.attr('class').indexOf("error") != -1)
	{
		
	}
	else
	{
		ele.attr('class', ele.attr('class') + " " + "error");
	}
}

function removeErrorClass(ele)
{
	ele.attr('class', ele.attr('class').replace('error', ''));
}

function step1check() {
	var noerrors = true;
	var errorlist = Array();
	var ele;
	step1errorcheck = true; /** Ensure the Company Name is Not Empty */
	if ($('#biz_legal_name').val() === '') {
		noerrors = false;
		addErrorClass($('#biz_legal_name'));
		errorlist.push("Need to Enter a Company Name.");
		
		if(!ele)
			ele = $('#biz_legal_name');
	} else {
		removeErrorClass($('#biz_legal_name'));
	} /** EMAIL VALIDATION */
	var email = $('#email').val();
	if (email === '') {
		addErrorClass($('#email'));
		errorlist.push("Email Field Needs an Email Entered.");
		noerrors = false;
		
		if(!ele)
			ele = $('#email');
	} else {
		if (!isEmailValid(email)) {
			addErrorClass($('#email'));
			errorlist.push("Email Field needs a Valid Email Address.");
			noerrors = false;
			
			if(!ele)
				ele = $('#email');
		} else removeErrorClass($('#email'));
	} /** Phone Number Validation */
	
	/*var phone = $('#phone').val();
	if (isPhoneValid(phone)) {
		removeErrorClass($('#phone'));
		var nums = numbersFromStr(phone);
		
		var result = "(" + nums.substr(0,3) +") " + nums.substr(3,3) + " - " + nums.substr(6,4);
		$('#phone').val(result);
	} else {
		noerrors = false;
		addErrorClass($('#phone'));
		errorlist.push("Phome Number Field needs a Valid Phone Number.");
	}*/
	 /* Ensure That First Name is Entered */
	if ($('#first_name').val() === '') {
		noerrors = false;
		addErrorClass($('#first_name'));
		errorlist.push("Need to Enter a First Name.");
		if(!ele)
			ele = $('#first_name');
	} else {
		removeErrorClass($('#first_name'));
	} /** Ensure That First Name is Entered */
	/** Ensure That First Name is Entered */
	if ($('#last_name').val() === '') {
		noerrors = false;
		addErrorClass($('#last_name'));
		errorlist.push("Need to Enter a Last Name.");
		if(!ele)
			ele = $('#last_name');
	} else {
		removeErrorClass($('#last_name'));
	} /** Setup Error Display */
	if (noerrors) {
		if($('#step1errors'))
			$('#step1errors').css("display", "none");
		if (!focuscheck) {
			//$("#step2").attr("style", "display: inline-block");
			section++;
			
			//$("#scrollarea").css("left", (-$('.step').width() * section) + "px");
			//setTimeout(function() {
				//$("#step1").attr("style", "display: none;");
				//$("#step1holder").attr("style", "display: inline-block;");
			//}, 1000);
		}
	} else {
		if($('#step1errors')){
			$('#step1errors').css("display", "block");
			var str = "<p><b>Oops! There were some Errors in the Submission:</b></p>";
			for (var i = 0; i < errorlist.length; i++) {
				str = str + "<li>" + errorlist[i] + "</li>";
			}
			$('#step1errors').html(str);
		}
		if(ele && !focuscheck)
			ele.focus();
	}
	return noerrors;
}

function step2check()
{
	var noerrors = true;
	var errorlist = Array();
	var ele;
	
	$('#vf_skip').val(0);
	
	step2errorcheck = true;
	
	var password = $('#password').val();
	var passVer = verifyPassword(password);
	if(!passVer.valid)
	{
		noerrors = false;
		errorlist.push(passVer.reason);
		addErrorClass($('#password'));
		if(!ele)
			ele = $('#password');
	}
	else
	{
		removeErrorClass($('#password'));
	}
	
	var passwordv = $('#passwordv').val();
	if(passwordv !== password)
	{
		noerrors = false;
		errorlist.push("Password Verification Does Not Match Original Password!");
		addErrorClass($('#passwordv'));
		if(!ele)
			ele = $('#passwordv');
	}
	else
	{
		removeErrorClass($('#passwordv'));
	}
	
	var ssn = $('#ssn_last_4').val();
	if(!isSSNValid(ssn))
	{
		noerrors = false;
		errorlist.push("Please Enter 4 numbers for the Last 4 of the SSN.");
		addErrorClass($('#ssn_last_4'));
		if(!ele)
			ele = $('#ssn_last_4');
	}
	else
	{
		removeErrorClass($('#ssn_last_4'));
	}
	
	var address = $('#address_1').val();
	if(address === "")
	{
		noerrors = false;
		errorlist.push("Please Enter a Valid Address.");
		addErrorClass($('#address_1'));
		if(!ele)
			ele = $('#address_1');
	}
	else
	{
		removeErrorClass($('#address_1'));
	}
	
	var zip = $('#zip_code').val();
	var state = $('#state').val();
	var city = $('#city').val();
	if( zip === "" || zip.length !== 5)
	{
		noerrors = false;
		if(zip === "")
		{
			errorlist.push("Please Enter a Zip Code for your address.");
			if(!ele)
				ele = $('#zip_code');
		}
		else
			errorlist.push("Please Enter a Valid Zip Code for your address.");
		addErrorClass($('#zip_code'));
	}
	else
	{
		removeErrorClass($('#zip_code'));
	}
	
	if(state === "" || city === "")
	{
		noerrors = false;
		errorlist.push("No City/State has Been Specified for Your Address.  Please Double Check the Zip Code and City/State Selection.");
		addErrorClass($('#citystate'));
		if(!ele)
			ele = $('#citystate');
	}
	else
	{
		removeErrorClass($('#citystate'));
	}
	
	var dob_month = $('#dob_month');
	var dob_day = $('#dob_day');
	var dob_year = $('#dob_year');
	if(dob_month.val() === "")
	{
		noerrors = false;
		errorlist.push("Please Specify a Birth Month.");
		addErrorClass(dob_month);
		if(!ele)
			ele = dob_month;
	}
	else
	{
		removeErrorClass(dob_month);
	}
	
	if(dob_day.val() === "")
	{
		noerrors = false;
		errorlist.push("Please Specify a Birth Day.");
		addErrorClass(dob_day);
		if(!ele)
			ele = dob_day;
		
	}
	else
	{
		removeErrorClass(dob_day);
	}
	
	if(dob_year.val() === "")
	{
		noerrors = false;
		errorlist.push("Please Specify a Birth Year");
		addErrorClass(dob_year);
		if(!ele)
			ele = dob_year;
	}
	else
	{
		removeErrorClass(dob_year);
	}
	
	if (noerrors) {
		if($('#step2errors'))
			$('#step2errors').css("display", "none");
		if (!focuscheck) {
			//$("#step3").attr("style", "display: inline-block");
			section++;
			
			//$("#scrollarea").css("left", (-$('.step').width() * section) + "px");
			//setTimeout(function() {
			//	$("#step2").attr("style", "display: none;");
			//	$("#step2holder").attr("style", "display: inline-block;");
			//}, 1000);
		}
	} 
	else {
		if($('#step2errors'))
		{
			$('#step2errors').css("display", "block");
			var str = "<p><b>Oops! There were some Errors in the Submission:</b></p>";
			for (var i = 0; i < errorlist.length; i++) {
				str = str + "<li>" + errorlist[i] + "</li>";
			}
			$('#step2errors').html(str);
		}
		if(ele && !focuscheck)
			ele.focus();
	}
	return noerrors;
}
function step3check()
{
	var noerrors = true;
	var errorlist = Array();
	var ele;
	
	step2errorcheck = true;
	
	var biz_ownership_type = $('#biz_ownership_type');
	if(biz_ownership_type.val() === "")
	{
		noerrors = false;
		errorlist.push("Please Specify Your Ownership Type.");
		addErrorClass(biz_ownership_type);
		
		if(!ele)
			ele = biz_ownership_type;
	}
	else
	{
		removeErrorClass(biz_ownership_type);
	}
	
	var biz_industry_type = $('#biz_industry_type');
	if(biz_industry_type.val() === "")
	{
		noerrors = false;
		errorlist.push("Please Specify Your Industry Type.");
		addErrorClass(biz_industry_type);
		
		if(!ele)
			ele = biz_industry_type;
	}
	else
	{
		removeErrorClass(biz_industry_type);
	}
	
	var biz_address_1 = $('#biz_address_1').val();
	if( biz_address_1 === "" )
	{
		noerrors = false;
		errorlist.push("Please Specifiy an Address for Your Business.");
		addErrorClass($('#biz_address_1'));
		
		if(!ele)
			ele=$('#biz_address_1');
	}
	else
	{
		removeErrorClass($('#biz_address_1'));
	}
	
	var bizzip = $('#biz_zip_code').val();
	var bizcity = $('#biz_city').val();
	var bizstate = $('#biz_state').val();
	if( bizzip === "" || bizzip.length !== 5)
	{
		noerrors = false;
		errorlist.push("Please Specify a Zip Code for Your Business Address.");
		addErrorClass($('#biz_zip_code'));
		
		if(!ele)
			ele=$('#biz_zip_code');
	}
	else
	{
		removeErrorClass($('#biz_zip_code'));
	}
	
	if( bizcity === "" || bizstate === "" )
	{
		noerrors = false;
		errorlist.push("No City/State has been Specified for Your Business Address. Please Double Check the Zip Code and City/State Selections.");
		addErrorClass($('#biz_citystate'));
		
		if(!ele)
			ele=$('#biz_citystate');
	}
	else
	{
		removeErrorClass($('#biz_citystate'));
	}
	
	var bizphone = $('#biz_phone').val();
	if(bizphone === "")
	{
		noerrors = false;
		errorlist.push("Please Enter a Phone Number for your Business.");
		addErrorClass($('#biz_phone'));
		
		if(!ele)
			ele=$('#biz_phone');
	}
	else if(!isPhoneValid(bizphone))
	{
		noerrors = false;
		errorlist.push("Pleas Enter a Valid Phone Number for your Business.");
		addErrorClass($('#biz_phone'));
		
		if(!ele)
			ele=$('#biz_phone');
	}
	else
	{
		//$('#biz_phone').attr("class","");
		removeErrorClass($('#biz_phone'));
		
		var nums = numbersFromStr(bizphone);
		
		var result = "(" + nums.substr(0,3) +") " + nums.substr(3,3) + " - " + nums.substr(6,4);
		$('#biz_phone').val(result);
	}
	
	var biz_ein = $('#biz_ein');
	if(biz_ein.val() === "")
	{
		noerrors = false;
		errorlist.push("Please Specify Your Employer Identification Number.");
		addErrorClass(biz_ein);
		
		if(!ele)
			ele = biz_ein;
	}
	else
	{
		removeErrorClass(biz_ein);
	}
	
	var device_ship_to = $('#device_ship_to');
	if(device_ship_to.val() === "")
	{
		noerrors = false;
		errorlist.push("Please Specify Where you Would Like a Device Shipped.");
		addErrorClass(device_ship_to);
		
		if(!ele)
			ele = device_ship_to;
	}
	else
	{
		removeErrorClass(device_ship_to);
	}
	
		if (noerrors) {
			if($('#step3errors')){
				$('#step3errors').css("display", "none");
			}
			if (!focuscheck) {
			//$("#step4").attr("style", "display: inline-block");
			section++;
			
			//$("#scrollarea").css("left", (-$('.step').width() * section) + "px")
			
			//setTimeout(function() {
			//	$("#step2").attr("style", "display: none;");
			//	$("#step2holder").attr("style", "display: inline-block;");
			//}, 1000);
		}
	} 
	else {
		if($('#step3errors')){
			$('#step3errors').css("display", "block");
			var str = "<p><b>Oops! There were some Errors in the Submission:</b></p>";
			for (var i = 0; i < errorlist.length; i++) {
				str = str + "<li>" + errorlist[i] + "</li>";
			}
			$('#step3errors').html(str);
		}
		
		if(ele && !focuscheck)
			ele.focus();
	}
	return noerrors;

}

function step4check(lite)
{
	var noerrors = true;
	var errorList = Array();
	step4errorcheck = true;
	var ele;
	
	var template = $('#template').val();
	
	
	if(template === "" )
	{
		noerrors = false;
		errorList.push("Please Select a Template Style.");
		addErrorClass($('#selectionsection').children('ul').children('li'));
	}
	else
	{
		removeErrorClass($('#selectionsection').children('ul').children('li'));
	}
	
	var tos = $("#agree_tos").attr('checked');
	if(!tos)
	{
		noerrors = false;
		errorList.push("Please Agree to the TOS.");
		addErrorClass($('#tosagree'));
		ele = $('#agree_tos');
	}
	else
	{
		removeErrorClass($('#tosagree'));
	}
	
	if(noerrors)
	{
		if($('#step4errors')){
			$('#step4errors').css("display", "none");
		}
		
		if(!focuscheck)
		{
			//SUBMIT INFORMATION
			//section++;
			if($('#vf_skip').val()*1)
				return skipped(lite);		
			else
				return verifone_enroll();
			//document.getElementById("new_customer").submit();
		}
	}
	else
	{
		if($('#step4errors')){
			$('#step4errors').css("display", "block");
			var str = "<p><b>Oops! There were some Errors in the Submission:</b></p>";
			for(var i = 0; i < errorList.length; i++)
			{
				str = str + "<li>" + errorList[i] + "</li>";
			}
			$('#step4errors').html(str);
		}
		
		if(ele && !focuscheck)
			ele.focus();
	}
	return noerrors;
}

function step5check()
{
	var noerrors = true;
	var errorList = Array();
	step4errorcheck = true;
	var ele;
	
	var cc_num = $('#card_number');
	cc_num.val(cc_num.val().replace(/\D/g, ""));
	if(cc_num.val().length !== 12)
	{
		addErrorClass(cc_num);
		noerrors = false;
		errorList.push("Please Enter a Valid Credit Card Number");
		
		if(!ele)
			ele = cc_num;
	}
	else
	{
		removeErrorClass(cc_num);		
	}
	
	var card_expire_mo = $('#card_expiration_mo');
	var card_expire_yr = $('#card_expiration_yr');
	
	if(card_expire_mo.val() === "")
	{
		noerrors = false;
		addErrorClass(card_expire_mo);
		errorList.push("Please Select an Expiration Month for your Credit Card.");
		
		if(!ele)
			ele = card_expire_mo;
	}
	
	if(card_expire_yr.val() === "")
	{
		noerrors = false;
		addErrorClass(card_expire_yr);
		errorList.push("Please Select an Expiration Year for your Credit Card.");
		
		if(!ele)
			ele = card_expire_yr;
	}
	
	var d = new Date();
	if((card_expire_mo.val()*1) < d.getMonth() && (card_expire_yr.val()*1) == d.getFullYear())
	{
		noerrors = false;
		addErrorClass(card_expire_mo);
		addErrorClass(card_expire_yr);
		errorList.push("The Expiration Date Has Already Passed.");
		
		if(!ele)
			ele = card_expire_mo;
	}
	else
	{
		removeErrorClass(card_expire_mo);
		removeErrorClass(card_expire_yr);
	}
	
	var ccv = $('#card_code');
	if(ccv.val() === "")
	{
		noerrors = false;
		addErrorClass(ccv);
		errorList.push("Please Specify the Security Code for the Credit Card.");
		
		if(!ele)
			ele = ccv;
	}
	else
	{
		removeErrorClass(ccv);
	}
	
	
	
	if(noerrors)
	{
		if($('#step5errors')){
				$('#step5errors').css("display", "none");
		}
		
		if(!focuscheck){
			$('#new_customer').submit();
			}
	}
	else
	{
		if($('#step5errors')){
			$('#step5errors').css("display", "block");
			var str = "<p><b>Oops! There were some Errors in the Submission:</b></p>";
			for(var i = 0; i < errorList.length; i++)
			{
				str = str + "<li>" + errorList[i] + "</li>";
			}
			$('#step5errors').html(str);
		}
		
		if(ele && !focuscheck)
			ele.focus();
	}
}

var enroll_succeeded = false;
function verifone_enroll()
{
	if(enroll_succeeded)
	{
		section++;
		return true;
	}
		
	var fields = Array(
						'app_id',
						'email',
						'password',
						'plan_id',
						'first_name',
						'last_name',
						'address_1',
						'address_2',
						'city',
						'state',
						'zip_code',
						'phone',
						'ssn_last_4',
						'dob_month',
						'dob_day',
						'dob_year',
						'biz_legal_name',
						'biz_address_1',
						'biz_address_2',
						'biz_city',
						'biz_state',
						'biz_zip_code',
						'biz_phone',
						'biz_ownership_type',
						'biz_industry_type',
						'biz_ein',
						'device_ship_to',
						'template');
						
	var data = "";
	for(var i = 0; i < fields.length; i++)
	{
		if(data !== "")
			data = data + "&";
		data = data + fields[i] + "=" + $('#'+fields[i]).val();
	}
	
	var result = false;
	
	$.ajax({
		url: "/vf/verifone_enroll.php",
		type: "POST",
		data: data,
		success: function(data, textState, jqXHR){
			$('#step5').html(data);
			section++;
			result = true;
		},
		async: false
	});
	
	return result;
}

function load_step5()
{
	var result = false;
	
	$.ajax({
		url: "/vf/steps/step5.php",
		type: "POST",
		success: function(data, textState, jqXHR)
		{
			$('#step5').html(data);
			section++;
			result = true;
		},
		async: false
	});
	
	return result;
}

var verifone_validated = false;
function verifone_validate()
{
	if(verifone_validated)
	{
		section++;
		return true;
	}
		
	getAnswers();
	
	var fields = Array(
						'app_id',
						'enrollment_id',
						'validate_answers'
					  );
						
	var data = "";
	for(var i = 0; i < fields.length; i++)
	{
		if(data !== "")
			data = data + "&";
		data = data + fields[i] + "=" + $('#'+fields[i]).val();
	}
	
	$.ajax({
		url: "/vf/verifone_validate.php",
		type: "POST",
		data: data,
		success: function(data, textState, jqXHR){
			$('#step6').html(data);
			section++;
		},
		async: false
	});
	
	return true;
}

var verifone_added_bank_account = false;
function verifone_AddBankAccount()
{
	if(verifone_added_bank_account)
		return true;
	
	var fields = Array(
						'app_id',
						'api_token',
						'api_secret',
						'account_holder',
						'routing_num',
						'nickname',
						'account_num',
						'account_type',
						'password',  // VERIFONE SPECIFIC INFORMATION
						'username',
						'client_id',
						'email'
						);
						
	var data = "";
	for(var i = 0; i < fields.length; i++)
	{
		if(data !== "")
			data += "&";
		data = data + fields[i] + "=";
		if($('#vf_'+fields[i]).length)
		{
			data += $('#vf_'+fields[i]).val();
		}
		else
		{
			data += $('#'+fields[i]).val();
		}
	}
	
	$.ajax({
		url: "/vf/verifone_addbank.php",
		type: "POST",
		data: data,
		success: function(data, textState, jqXHR){
			
			$('#step7').html(data);
			section++;
		},
		async: false
	});
	
	return true;
}

function skipped(lite)
{
	if(lite=='lavu_lite')
		$('#new_customer').attr('action', '/vf/email_verify.php?fromAPP=1');
	else
		$('#new_customer').attr('action', '/vf/email_verify.php');
	$('#new_customer').submit();
}

$(document).ready(function(){
	$('#container').height($('#step' + (section + 1)).height());
});

function verifyCompletedSignupProcess(token){
	
	var data='token='+token;
	$("#errorVerification").css("display","none");
	var page= window.location.protocol+"//"+window.location.host+window.location.pathname;
	if(page.indexOf("&") != -1){
		page= page.substring(0,page.indexOf('&'));
	}
	//alert(page);	
	var rand=Math.floor((Math.random()*10000)+1);
	//alert(rand);
	$.ajax({
	
		url: "/vf/checkValidated.php?r="+rand ,
		type: "POST",
		data: data,
		success: function(string){
			
			if(string=='error'|| string=='noUser'|| string=='noToken' || string=='failure'){
							
				window.location=page+'?error='+string+'&token='+token;		
			}else{
				$("#errorVerification").css("display","none");
				window.location = "_DO:cmd=auto_login&"+string;
			}
			
		}
	});
}

function verifone_bank_validate()
{
	var noerrors = true;
	var ele = null;

	var account_holder = $('#account_holder');
	if(account_holder.val() === "")
	{
		noerrors = false;
		if(!ele)
			ele = account_holder;
		addErrorClass(account_holder);
	}
	else
	{
		removeErrorClass(account_holder);
	}
	
	var routing_num = $('#routing_num');
	if(routing_num.val() === "")
	{
		noerrors = false;
		if(!ele)
			ele = routing_num;
		addErrorClass(routing_num);
	}
	else
	{
		removeErrorClass(routing_num);
	}

	var nickname = $('#nickname');
	if(nickname.val() === "")
	{
		noerrors = false;
		if(!ele)
			ele = nickname;
		addErrorClass(nickname);
	}
	else
	{
		removeErrorClass(nickname);
	}
	
	
	var account_num = $('#account_num');
	if(account_num.val() === "")
	{
		noerrors = false;
		if(!ele)
			ele = account_num;
		addErrorClass(account_num);
	}
	else
	{
		removeErrorClass(account_num);	
	}
	
	if(noerrors)
	{
		//section++;
		return verifone_AddBankAccount();
	}
	else
	{
		ele.focus();
	}
	
	return noerrors;
}
