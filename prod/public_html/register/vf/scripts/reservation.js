
$(document).ready(function(){
	var email=null;
	var phone=null;
	$("#submitter").click(function(){
		if(!validatePhone($("#phoneNum").val())){
			$("#currentError").html("invalid Phone number");
		}
		else if(!validateEmail($("#email").val()))
			  $("#currentError").html("Not a valid e-mail address");
		else if($("#referrer").val()=='null')	
			$("#currentError").html("Please select a referrer");
		else
			reserve($("#name").val(), $("#phoneNum").val(), $("#email").val(),$("#referrer").val());	
	});
	
	$("#email").focusout(function(){
	
		if( email!= $("#email").val()){
			email= $("#email").val();
			validateEmail($("#email").val());
		}
	
	});
	
	$("#phoneNum").focus(function(){
		$("#phoneNum").attr('placeholder', "(XXX)-XXX-XXXX");
	});
	
	$("#phoneNum").keydown(function(e){
		
		if( e.keyCode!=8 && !(e.keyCode >= 65 && e.keyCode <= 90) ){
			$("#phoneNum").val($("#phoneNum").val().replace(/[^0-9()-]/g,''));
			if($("#phoneNum").val().length==1 && $("#phoneNum").val().indexOf("(")==-1){
					$("#phoneNum").val("("+ $("#phoneNum").val());
			}
			else if($("#phoneNum").val().length==4 && $("#phoneNum").val().indexOf(")")==-1){
					$("#phoneNum").val($("#phoneNum").val()+")");
			}
			else if($("#phoneNum").val().length==8 ){
					$("#phoneNum").val($("#phoneNum").val()+"-");
			}
			else if($("#phoneNum").val().length>13)
				 $("#phoneNum").val(phone);
				
			phone= $("#phoneNum").val();	
		}
		else{
			phone='';
			$("#phoneNum").val($("#phoneNum").val().replace(/[(]{0,1}\d{3}[)]{0,1} {0,1}\d{3}-{0,1}\d{4}?/,''));
		}
	});
	
});


function reserve(name, phone, email, referrer){
	
	var urlString='name='+name+"&phone="+phone+"&email="+email+"&referrer="+referrer;
	
	$.ajax({
		
		url: "/vf/saveReservation.php",
		type: "POST",
		data:urlString,
		success: function (string){
			if( string=='exists')
				$("#currentError").html("Email already registered. We will contact you when Lavu Lite is back up!");
			else if(string)
				showSuccess();
			else
				$("#currentError").html("Email already registered. We will contact you when Lavu Lite is back up!"+string);
		}
	});	
	
}
function openNotice(){
	$("#notice").fadeIn('slow',function(){
		
	});
}
function closeNotice(){
	$("#notice").fadeOut('slow',function(){
		
	});
}

function validatePhone(phone){
	n=phone.replace(/[^0-9]/g,'');
	if( n.length==10)
		return true;
	else 
		return false;
}
function validateEmail(email){

	var atpos=email.indexOf("@");
	var dotpos=email.lastIndexOf(".");
	
	if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length){

	  return false;
	}
	else{
		$("#currentError").html("");
		return true;
	}
}

function showSuccess(){
	$("#signupCounter").html($("#signupCounter").html()*1 +1);
	
	$("#reserveForm").fadeOut('slow',function(){
		$("#reserveForm").html("Reserved!<br>Thank you for your interest in Lavu Lite");
		$("#reserveForm").css("width", "350px");
		$("#reserveForm").fadeIn('slow',function(){});
	});
}