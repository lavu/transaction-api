<?php
	session_start();
	require_once("./verifone_info.php");
	if( isset($_REQUEST['zip_code'])){
		$zip = $_REQUEST['zip_code'];
	
		//$url = $api_url . "check_zip" . "?app_id=" . $app_id . "&zip_code=" . $zip;
		$params = "&zip_code=" . $zip;
		$option = "check_zip";
		echo get_post($option, $params);
		//echo file_get_contents($url);
		
		exit;
	}

	function get_post($option, $params)
    {
	    global $app_id;
	    global $api_url;
	    
	    //echo $api_url . $option . "?app_id=" . $app_id . $params;
	    
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $api_url . $option . "?app_id=" . $app_id . $params);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    
	    return $response;
    }
?>