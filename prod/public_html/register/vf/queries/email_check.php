<?php
	/**
	 * Email Check Simply takes an argument and looks through the customer_accounts table to check to see if the
	 * email provided has already been used or not.  It prints a json result with the status of the provided email
	 * whether it is available for use or not.
	 * 
	 * RECEIVES
	 * email: the email address to use for searching
	 *
	 * RETURNS
	 * a JSON response with the status of the email, IE Availble or Not.  Could also return error, if this happens
	 * it will return a message for debugging purposes.
	 * 
	 */
	 
	 define("EMAIL_AVAILABLE", "available");
	 define("EMAIL_IN_USE","unavailable");
	 define("ERROR","error");
	 define("EMAIL_INVALID","invalid");
	 
	 /************************************************
	  * FUNCTIONS
	  ***********************************************/
	 
	 function quit($status, $msg="")
	 {
		 echo '{ "status" : "' . $status . '"';
		 if($msg != "")
			 echo ', "message" : "' .$msg . '"';
		 echo '}';
		 exit();
	 }
	 
	 /************************************************
	  * START
	  ***********************************************/
	  
	  {
		  if(!isset($_REQUEST['email']))
			  quit(EMAIL_INVALID, "No Email Was Provided");
		  
		  $email = $_REQUEST['email'];
		  
		  //NEED LAVU QUERY
		  require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
		  
		  $email_query = "SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `email`='[1]'";
		  $email_result = mlavu_query($email_query, $email);
		  
		  if(mysqli_num_rows($email_result) >= 1 && $email!='niallsc@poslavu.com' && $email!='developer@poslavu.com')
		  
		  	quit(EMAIL_IN_USE,"The Specified Email is Already In Use.");
		  	
		  quit(EMAIL_AVAILABLE);
	  }
?>