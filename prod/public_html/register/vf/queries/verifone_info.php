<?php
$live = 1;
$app_id;
$api_url;
if($live)
{
	//Live Credentials
	$app_id = "207ecae7-a4ad-4bd9-b8fc-3414bf05d81b";
	$api_url = "https://api.sailpay.com/api/";
}
else
{
	//Test Credentials
	$app_id = "0bf71549-fd4f-4d75-a562-cd4d0536310c";
	$api_url = "https://sandbox-api.sailpay.com/api/";
}

$reseller_id = '61';

	function validate(&$params)
	{
		global $missingParams;
		$found = true;
		foreach($params as $key => $value)
		{
			$found &= isset($_REQUEST[$key]);
			if(isset($_REQUEST[$key]))
				$params[$key] = $_REQUEST[$key];
			else
				array_push($missingParams, $key);
		}
		
		return $found;
	}
	
	function post_to_verifone($postvars, $extn)
	{
		global $api_url;
		$url = $api_url . $extn;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
	
	function get_to_verifone($postvars, $extn)
	{
		global $api_url;
		$url = $api_url . $extn;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url . "?" . $postvars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
	
?>