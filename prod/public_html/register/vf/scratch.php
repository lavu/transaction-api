<?php
 // Setting a timezone, mail() uses this.
 date_default_timezone_set('America/New_York');
  // recipients
 $to  = "theo@poslavu.com" ; // note the comma 
 
  // subject 
 $subject = "Test for Embedded Image & Attachement"; 
 
 // Create a boundary string.  It needs to be unique 
 $sep = sha1(date('r', time()));
 
 $boundary = "lavulite-related-boundary";

 $headers = "From: noreply@lavulite.com\r\n"; 
 // Add in our content boundary, and mime type specification:
 $headers .= "Content-Type: multipart/related;\r\n boundary=\"$boundary\"\r\n";
 
 $image1 = chunk_split(base64_encode(file_get_contents('images/lavu_email_logo.png')));
 $image2 = chunk_split(base64_encode(file_get_contents('images/sailpay_email_logo.png')));
 
 // Your message here:
$body =<<<EOBODY
--$boundary
Content-Type: text/html; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

<!DOCTYPE html>
<html>
	<head>
		<style type="text/css">
			p{
				text-align: justify;
			}
		</style>
	</head>
	<body>
		<div style="max-width: 722px; min-width: 500px;">
			<div style="width: 100%; position: relative; height: 144px;">
				<img src="cid:sailpay_logo" style="position: absolute; top: 20px; left: 20px; width: 97px; height: 105px;"></img>
				<img src="cid:lavulite_logo" style="position: absolute; top: 0px; right: 20px; width: 384px; height: 144px;"></img>
			</div>
			<p>You have successfully signed up for SAIL by Verifone® via the Lavu Lite sign-up process.  The following are the SAIL Credentials that were generated via the sign-up process:</p>
			<br />
			
			<br />
			<p>If you have any issues using your processing or setting up your SAIL account, please contact the SAIL support department at:</p>
			<br />
				<div style="width: 300px; margin: 0 auto; text-align: center;">
				<b>855-SAIL-PAY (855-7245-729)</b><br /><br />
				or<br /><br />
				<b><a href="mailto:support@sailpay.com">support@sailpay.com</a></b><br /><br />
				</div>
			<br />
			<p>SAIL is a separate service, and is not required to use Lavu Lite.  Phone support for SAIL is limited to that service.  Any questions or issues with Lavu Lite should be resolved by visiting the Lavu Lite section in Help Center at help.lavu.com or submitting a ticket through your Lavu Lite Control Panel.</p>
		</div>
	</body>
</html>

--$boundary
Content-Type: image/png
	 name="lavu_email_logo.png"
Content-Transfer-Encoding: base64
Content-ID: <lavulite_logo>
Content-Disposition: inline;
	 filename="lavu_email_logo.png"
	
$image1
--$boundary
Content-Type: image/png
	 name="sailpay_email_logo.png"
Content-Transfer-Encoding: base64
Content-ID: <sailpay_logo>
Content-Disposition: inline;
	 filename="sailpay_email_logo.png"
	
$image2
--$boundary--
EOBODY;
 mail($to, $subject, $body, $headers);

?>