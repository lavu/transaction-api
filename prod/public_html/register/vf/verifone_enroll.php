<?php
	ini_set('display_errors',1);
	session_start();
	
	$enrollment_id;
	include_once("../../admin/cp/resources/core_functions.php");
	include_once("queries/verifone_info.php");
	$errorsFound = false;
	
	$key_list = array( 	'app_id' => 'app_id',
						'email' => 'email',
						'password' => 'password',
						'plan_id' => 'plan_id',
						'first_name' => 'first_name',
						'last_name' => 'last_name',
						'address_1' => 'address_1',
						'address_2' => 'address_2',
						'city' => 'city',
						'state' => 'state',
						'zip_code' => 'zip_code',
						'phone' => 'phone',
						'ssn_last_4' => 'ssn_last_4',
						'dob_month' => 'dob_month',
						'dob_day' => 'dob_day',
						'dob_year' => 'dob_year', 
						'biz_legal_name' => 'biz_legal_name',
						'biz_address_1' => 'biz_address_1',
						'biz_address_2' => 'bit_address_2',
						'biz_city' => 'biz_city',
						'biz_state' => 'biz_state',
						'biz_zip_code' => 'biz_zip_code',
						'biz_phone' => 'biz_phone',
						'biz_ownership_type' => 'biz_ownership_tpe',
						'biz_industry_type' => 'biz_industry_type',
						'biz_ein' => 'biz_ein',
						'device_ship_to' => 'device_ship_to'
						//'template' => 'template',
						//'agree_tos' => 'agree_tos'
						);
						
	$translation =array('app_id' => 'Application ID',
						'email' => 'Email',
						'password' => 'Password',
						'plan_id' => 'Plan ID',
						'first_name' => 'First Name',
						'last_name' => 'Last Name',
						'address_1' => 'Residence Address (Line 1)',
						'address_2' => 'Residence Address (Line 2)',
						'city' => 'Residence City',
						'state' => 'Residence State',
						'zip_code' => 'Residence Zip Code',
						'phone' => 'Phone Number',
						'ssn_last_4' => 'Last Four of SSN',
						'dob_month' => 'Date of Birth (Month)',
						'dob_day' => 'Date of Birth (Day)',
						'dob_year' => 'Date of Birth (Year)', 
						'biz_legal_name' => 'Business Legal Name',
						'biz_address_1' => 'Business Address (Line 1)',
						'biz_address_2' => 'Business Address (Line 2)',
						'biz_city' => 'Business City',
						'biz_state' => 'Business State',
						'biz_zip_code' => 'Business Zip Code',
						'biz_phone' => 'Business Phone Number',
						'biz_ownership_type' => 'Business Ownership Type',
						'biz_industry_type' => 'Buisness Industry Type',
						'biz_ein' => 'Business EIN',
						'device_ship_to' => 'Where to Ship Device'
						);
						
	$lavu_params = Array( 'first_name' => 'firstname', //firstname
					  'last_name' => 'lastname', //lastname
					  'phone' => 'phone', //phone
					  'email' => 'email', //email
					  'package' => 'package', //package
					  'template' => 'default_menu', //default_menu
					  'biz_legal_name' => 'company' //company
					  );
						
	$missing_keys = Array();

function handleEnrollResponse($response)
{
	global $enrollment_id;
	global $key_list;
	global $api_url;
	global $translation;
	
	$res = json_decode($response);
	
	if($res[0]['status'] == "Success")
	{
		//echo $response . "<br />";
		$enrollment_id = $res[0]['enrollment_id'];
		$postvars = "app_id=" . $key_list['app_id'] . "&enrollment_id=" . $enrollment_id;
		
		$url = $api_url . "questions?" . $postvars;
		//echo $url . "<br />";
		
		handleQuestionsResponse(get_to_verifone($postvars, "questions"));
	}
	else
	{
		global $errorsFound;
		$errorsFound = true;
		$errors = $res[0]['errors'];
		echo "<div class='errors w80p ccentered' style='display: block;'>";
		echo "<b>An Unexpected Error Has Occured<b><br /><br /><b>Enrollment Issue Has Occurred:</b><br />";

		foreach($errors as $k => $e)
		{
			$reason = $e['reason'];
			foreach($translation as $o => $t)
			{
				$reason = str_ireplace($o, $t, $reason);
			}
				if($error['code'] == "error.customer_service" )
				{
					echo "<li>An Error Occurred while processing the request, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";
				}
				else if($error['code'] == "internal.customer_service" )
				{
					echo "<li>An Internal Data Error Occurred, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";			
				}
			else
				echo "<li>" . $reason . "</li>";
		}
		echo "<li><a onclick='skipAndSubmit();'>Click Here</a> to Skip SailPay for Now. You Will Need to Contact SailPay in Order to Complete Your Signup Process.</li>";
		echo "</div>";
	}
}

$questionCount;

function handleQuestionsResponse($response)
{
	global $enrollment_id;
	global $questionCount;
	global $translation;
	$res = json_decode($response);
	
	if($res[0]['status'] == "Success")
	{
	echo '
		  <br />
		  <br />
		  <div class="step_info" style="text-align: left">
		  	<div class="stepnum step5"></div><span>Please Verify Who you Are by Answering the Following Questions.</span>
		  </div>
		  <br />
		  <br />';
		//echo "<h3>Please Verify Who you Are by Answering the Following Questions: </h3><br />";
		$questions = $res[0]['activate_questions'];
		echo "<input type='hidden' id='enrollment_id' name='enrollment_id' value='$enrollment_id' />";
		echo "</form>";
		$qcount = 0;
		foreach($questions as $k => $q)
		{
			echo "<label for='" . $q['id'] . "'>" . $q['text'] . "</label>\n";
			echo "<select class='half' id='q" . $qcount++ ."' name='" . $q['id'] ."'>\n";
			foreach($q['choices'] as $choice)
			{
				echo "<option>" . $choice . "</option>\n";
			}
			echo "</select><br /><br /><br />\n";
		}
		//echo "<input type='button' class='next' value='Answer' onclick='postit();' />";
		$questionCount = $qcount;
		
		echo '<script type="text/javascript">enroll_succeeded=true;</script>';
	}
	else
	{
		echo "<div class='errors ccentered w80p' style='display: block;' >";
		echo "<b>The Following Errors Were Encountered While Trying to Retrieve The Question List:</b><br /><br />";
		global $errorsFound;
		$errorsFound = true;

		$errors = $res[0]['errors'];
		foreach($errors as $k => $error)
		{
			$reason = $error['reason'];
			foreach($translation as $o => $t)
			{
				$reason = str_ireplace($o, $t, $reason);
			}
				if($error['code'] == "error.customer_service" )
				{
					echo "<li>An Error Occurred while processing the request, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";
				}
				else if($error['code'] == "internal.customer_service" )
				{
					echo "<li>An Internal Data Error Occurred, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";			
				}
			else
				echo "<li>" . $reason . "</li>";
		}
		echo "<li>Ensure your personal information is accurate.</li>";
		echo "<li><a onclick='skipAndSubmit();'>Click Here</a> to Skip SailPay for Now. You Will Need to Contact SailPay in Order to Complete Your Signup Process.</li>";
		echo "</div>";
	}
}
?>

<div class="title_header">
	<div class="title_header_small"><div class="shade_left"></div></div>
	<div class="title_header_middle">
		<div class="logo_lavu_lite_sail"></div>
	</div>
	<div class="title_header_small"><div class="shade_right"></div></div>
</div>
<div class="banner">
    <br />
    <br />
    <h2>Good Choice - SAILpay rate - 1.95%</h2>
    <h3>No Contracts. Cancel at Any Time.</h3>
</div>
<br />
<div class="form_area ccentered">
	<input type="hidden" name="validate_answers" id="validate_answers" />
<?php            
if( validate($key_list) )
	{
		global $app_id;
		global $enrollment_id;
		//echo "<input type='hidden' name='app_id' value='$app_id' />";
		$postvars = "";
		foreach($key_list as $k => $v)
		{
			//echo "<li>" . $k . " = " . $v . "</li>";
			if( $v === '' )
			{
				
			}
			else
			{
				if($postvars != "")
					$postvars .= "&";
				if($k == 'biz_phone' || $k == 'phone')
				{
					$str = str_ireplace("(", "", $v);
					$str = str_ireplace(")", "", $str);
					$str = str_ireplace("-", "", $str);
					$str = str_ireplace(" ", "", $str);
					$postvars .= $k . "=" . $str;
				}
				else if(($k == 'dob_day' and strlen($v) == 1) or (($k == 'dob_month') and strlen($v) == 1))
				{
					$postvars .= $k . "=0".$v;	
				}
				else
				{
					$postvars .= $k . "=" . $v;
				}
			}
		}
		global $live;
		if($live){
			global $reseller_id;
			$postvars .= "&reseller_id=" . $reseller_id;
		}
		/**
		 * This sets up the additional paramters that will eventually be needed by auth_account
		 */
		foreach($lavu_params as $key => $val)
		{
			if(in_array($key, $key_list))
				echo "<input type=\"hidden\" name=\"$val\" value=\"" . $key_list[$key] . "\" />";
			else
			{
				echo "<input type=\"hidden\" name=\"$val\" value=\"" . $_POST[$key] . "\" />";
			}
		}
		
		//echo "Enroll<br />" . $postvars . "<br />";
		handleEnrollResponse( post_to_verifone($postvars, "enroll") );
		
	}
	else
	{
		echo "<div class='errors w80p ccentered' style='display: block;'>";
		echo "<b>An Unexpected Error Has Occured<b><br /><br /><b>Some Keys Were Missing:</b><br />";
		foreach($missing_keys as $key => $v)
		{
			echo "<li>$v</li>";
		}
		echo "</div>";
		
	}
?>
</div>
<script type="text/javascript">
	function getAnswers()
	{
		var json = "[ ";
		var i = 0;
		while(true)
		{
			if($('#q' + i).length)
			{
			
				if(i > 0)
					json += ",";
				json += "{ ";
				
				json += '"id":"' + $('#q' + i).attr('name') + '", "answer":"' + $('#q'+i).val() + '"';
				
				json += " }";
			}
			else
				break;
			
			i++;
		}
		
		json += " ]";
		
		$('#validate_answers').val(json);
		//document.getElementById('question').submit();
	}
</script>

<div class="divider"></div>

<div class="footer">
	<button class="back" type="button" id="step5back" onclick="backStep($('#tos_agree'))" >Back</button>
	<?php
	if (!$errorsFound)
	    echo '<button class="next" type="button" id="step5complete" onclick="nextStep()" >Next</button>';
    ?>
</div>
