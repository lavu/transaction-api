<?php
	ini_set('display_errors',1);
	session_start();
	$errorsFound = false;
	
	include_once("../../admin/cp/resources/core_functions.php");
	include_once("queries/verifone_info.php");
	
	$params = Array( 'app_id' => 'app_id',
					 'enrollment_id' => 'enrollment_id',
					 'validate_answers' => 'validate_answers'
					  );
					  
	$translation = Array( 'app_id' => 'Application ID',
					 'enrollment_id' => 'Enrollment ID',
					 'validate_answers' => 'Answer Validation'
					  );
					  
	$lavu_params = Array( 'firstname' => 'firstname', //firstname
						  'lastname' => 'lastname', //lastname
						  'phone' => 'phone', //phone
						  'email' => 'email', //email
						  'package' => 'package', //package
						  'default_menu' => 'default_menu', //default_menu
						  'company' => 'company' //company
						  );
					 
	$missingParams = Array();
	
	function getParams()
	{
		global $params;
		global $missingParams;
		global $lavu_params;
		
		if(validate($params))
		{
		
			validate($lavu_params);
			$pars = "";
			
			foreach($params as $key => $value)
			{
				if($pars != "")
					$pars .= "&";
				$pars .= "$key=$value";
			}
			//echo $pars . "<br />";
			handleValidate(post_to_verifone($pars, "questions"));
			//handleActivate(post_to_verifone($pars, "activate"));
		}
		else
		{
			echo "<div class='errors ccentered w80p' style='display: block;' >";
			echo "<b>The Following Parameters Were Missing:</b><br /><br />";
			foreach($missingParams as $param)
			{
				echo "<li>$param</li>";
			}
			echo "</div>";
		}
	}
	
	function handleValidate($response)
	{
		global $params;
		global $translation;
		//echo $response;
		$res = json_decode($response);
		
		if($res[0]['status'] == "Success")
		{
			//Activate Time
			$pars = "app_id=" . $params['app_id'] . "&enrollment_id=" . $params['enrollment_id'];
			handleActivate(post_to_verifone($pars, "activate"));
		}
		else
		{
			global $errorsFound;
			$errorsFound = true;
			echo "<div class='errors ccentered w80p' style='display: block;' >";
			echo "<b>The Following Errors Were Encountered While Attempting to Validate Answers:</b><br /><br />";
			//echo $response . "<br />";
			$errors = $res[0]['errors'];
			foreach($errors as $k => $e)
			{
				$reason = $e['reason'];
				foreach($translation as $o => $t)
				{
					$reason = str_ireplace($o, $t, $reason);
				}
				if($error['code'] == "error.customer_service" )
				{
					echo "<li>An Error Occurred while processing the request, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";
				}
				else if($error['code'] == "internal.customer_service" )
				{
					echo "<li>An Internal Data Error Occurred, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";			
				}
				else
					echo "<li>" . $reason . "</li>";
			}
			echo "<li>Please Ensure That Your Answers are Correct.</li>";
			echo "<li><a onclick='skipAndSubmit();'>Click Here</a> to Skip SailPay for Now. You Will Need to Contact SailPay in Order to Complete Your Signup Process.</li>";
			echo "</div>";
		}
	}
	
	function handleActivate($response)
	{
		$res = json_decode($response);
		
		if($res[0]['status'] == "Success")
		{
			//echo "Congratulations on Registering!<br />";
			$_POST['posted'] = '1';
			$_POST['vf_signup'] = '1';
			
			echo "<input type='hidden' name='posted' value='1' />";
			
			//echo "<h3>Verifone Credentials:</h3>";
			//echo "<p>Be sure to write these down, just in case you need them:</p>";
			
			foreach($res[0] as $key => $value)
			{
				//echo "<label class='full'>$key</label>";
				//echo "<div class='full'>$value</div><br />";
				echo "<input type='hidden' name='vf_$key' id='vf_$key' value='$value' />";
				//echo "<input type='hidden' name='$key' value='$value' />";
				$_POST['vf_' . $key] = $value;
			}
			
			echo "
			<div style='margin: 0 auto;'>
				<p>You must also enter the micro-deposit amount SAIL has made into your bank account.  You will not be able to receive deposits until you do this.   You will receive an email from SAIL notifying you when this deposit process has started.</p>
				<table>
					<tbody>
						<tr>
							<td align='right'>Account Nickname:&nbsp;&nbsp;</td>
							<td align='left'><input type='text' name='nickname' id='nickname' class='full' /></td>
						</tr>
						<tr>
							<td align='right'>Account Holder:&nbsp;&nbsp;</td>
							<td align='left'><input type='text' name='account_holder' id='account_holder' class='full' /></td>
						</tr>
						<tr>
							<td align='right'>Account Type:&nbsp;&nbsp;</td>
							<td align='left'>
								<select name='account_type' id='account_type' class='full'>
									<option value='2'>Checking</option>
									<option value='3'>Savings</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align='right'>Routing Number:&nbsp;&nbsp;</td>
							<td align='left'><input type='text' name='routing_num' id='routing_num' class='full' /></td>
						</tr>
						<tr>
							<td align='right'>Account Number:&nbsp;&nbsp;</td>
							<td align='left'><input type='text' name='account_num' id='account_num' class='full' /></td>
						</tr>
					</tbody>
				</table>
			</div>
			";
			
			echo '<script type="text/javascript">verifone_validated=true;</script>';
			
			//echo "<hr /><h3>Account Creation</h3>";
			
		}
		else
		{
			global $errorsFound;
			$errorsFound = true;
			echo "<div class='errors ccentered w80p' style='display: block;' >";
			echo "<b>The Following Errors Were Encountered While Attempting to Activate:</b><br /><br />";
			$errors = $res[0]['errors'];
			foreach($errors as $k => $e)
			{
				$reason = $e['reason'];
				foreach($translation as $o => $t)
				{
					$reason = str_ireplace($o, $t, $reason);
				}
				if($error['code'] == "error.customer_service" )
				{
					echo "<li>An Error Occurred while processing the request, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";
				}
				else if($error['code'] == "internal.customer_service" )
				{
					echo "<li>An Internal Data Error Occurred, please contact Sailpay Customer Service at 855-SAIL-PAY (855-7245-729) or support@sailpay.com</li>";			
				}
				else
					echo "<li>" . $reason . "</li>";
			}
			echo "<li><a onclick='skipAndSubmit();'>Click Here</a> to Skip SailPay for Now. You Will Need to Contact SailPay in Order to Complete Your Signup Process.</li>";
			echo "</div>";
		}
	}
	
?>
<div class="title_header">
	<div class="title_header_small"><div class="shade_left"></div></div>
	<div class="title_header_middle">
		<div class="logo_lavu_lite_sail"></div>
	</div>
	<div class="title_header_small"><div class="shade_right"></div></div>
</div>
<div class="banner">
    <br />
    <br />
    <h2>Add Verifone's SAILpay, get a free swipe.</h2>
    <h3>No Per-Transaction Fee, No Monthly Fees.</h3>
</div>
<br />
<div class="form_area ccentered">
	<br />
	<br />
	<br />
	<div class="step_info" style="text-align: left">
    	<div class="stepnum step6"></div><span>Setup your SAILpay credit card processing account.</span>
    </div>
    <br />
    <br />

<?php
	echo getParams();
?>
</div>

<div style="height: 50px;"></div>

<div class="divider"></div>

<div class="footer">
	<button class="back" type="button" id="step6back" onclick="backStep()" >Back</button>
    <?php
    	//$(\'#new_customer\').submit();
    	if(!$errorsFound)
    		echo '<button class="next" type="button" id="step6complete" onclick="nextStep()" >Next</button>';
    ?>
</div>
	
