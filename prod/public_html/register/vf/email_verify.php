<?php

	//LIMITS
	$LIMIT_IP_ADDRESS_SUBMISSIONS = 5;
	$LIMIT_EMAIL_ADDRESSES = 1;
	//

    session_start();
    
    include_once("../../admin/cp/resources/core_functions.php");

    if ($_SERVER['SERVER_PORT']!=443) // quit if not ssl
    {
        exit();
    }
    if (strpos($_SERVER['HTTP_HOST'],"register.poslavu.com")===false) // quit if not on register.poslavu.com
    {
        exit();
    }

    function generateRandomToken($size)
    {
    	$result = "";
    	for($i = 0; $i < $size; $i++)
    	{
    	
    		//26 + 26 + 10 = 62
	    	$rand = rand(0,61); // 0-9 a-z A-Z
	    	
	    	if($rand < 10)
	    	{ //Digit 0 - 9
		    	$result .= chr(48 + $rand);
	    	}
	    	else if($rand < 36)
	    	{ // a-z 10 - 35
		    	$result .= chr(97 + $rand - 10);
	    	}
	    	else
	    	{ // A-Z 36 - 61
		    	$result .= chr(65 + $rand - 36);
	    	}
	    	
	    }
	    
	   return $result;
	    
    }
 
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>POS Lavu iPad Point of Sale | New Signup</title>
    <link rel="stylesheet" href="styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <link rel="stylesheet" href="styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <link rel="stylesheet" href="styles/register.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript">
</script>
    <script src="scripts/jquery.tools.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="scripts/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
      <script src="scripts/register.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
    <div class="content ccentered" style='height:100%'>
        <!-- Information Section -->
        <div class="ccentered container" id="container" >
        <?php
        	if( !(isset($_GET['fromAPP']) && $_GET['fromAPP']==1 && stristr($_SERVER['HTTP_USER_AGENT'], 'iPad')) ){
	        	echo '
        <div class="title_header">
			<div class="title_header_small"><div class="shade_left"></div></div>
			<div class="title_header_middle">
				<div class="logo_lavu_lite"></div>
			</div>
			<div class="title_header_small"><div class="shade_right"></div></div>
		</div>
	        	';
	        }
        ?>
		<div class="banner">
		    <br />
		    <br />
		    <br />
		    <h2>Signup Complete.</h2>
		   
		</div>
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<div class="ccentered form_area" style='height:600px' >
		
			<div style='margin: 0 ' id='mailImage'>
				<img style='padding-left:40%' src='images/mail_icon_large.png'>
			</div>
		<br />
		<br /><br />
		<br />

<?php 
	if(isset($_GET['error'])){

		mt_srand(make_seed());
		$randval = mt_rand();
		echo "				
			<script type='text/javascript'>
				$('#mailImage').css('display', 'none');
			</script>
		<div id='errorVerification' style='color:red; text-align:center'>";
				
				if( $_GET['error']=='failure'){
					echo "<br>You have not yet Verified your account. Please verify your account before continuing<br>";
				
				}else if( $_GET['error']=='error'){
					echo "An error has occurred. Please try again later. ";
				
				}else if( $_GET['error']=='noUser'){
					echo "no username and password present. You have not yet completed your verification process. ";	
				
				}else if( $_GET['error']=='noToken'){
					echo "no token passed.";				
				}
			  echo "</div></center>
			<div style='padding-top:20px;'> 
				<center>
					<input type='button' style='background: url(images/btn_big_cont.png) no-repeat; cursor:pointer;border: none; width:300px; height:200px;' onclick='verifyCompletedSignupProcess(\"".$_GET['token']."\",\"".$randval."\")'>
				</center>
			</div>
			<center>";
			
	}
	else {
		sendEmail();
	}

	function make_seed(){
		list($usec, $sec) = explode(' ', microtime());
		return (float) $sec + ((float) $usec * 100000);
	}

	
	function sendEmail(){
		
		if(isset($_REQUEST['email']) && isset($_REQUEST['first_name']) && isset($_REQUEST['last_name']) && isset($_REQUEST['biz_legal_name'])){
			
			$email = $_REQUEST['email'];
			$first_name = $_REQUEST['first_name'];
			$last_name = $_REQUEST['last_name'];
			$company = $_REQUEST['biz_legal_name'];
			$ipaddress = $_SERVER['REMOTE_ADDR'];
			$default_menu = $_REQUEST['template'];
			
			
			$emailquery = "SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `email`='[1]'";
			$ipaddressquery = "SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `ipaddress`='[1]'";
			
			$ipresult = mlavu_query($ipaddressquery,$ipaddress);
			$continue = true;
			$reason = "";
			
			if(mysqli_num_rows($ipresult) && $LIMIT_IP_ADDRESS_SUBMISSIONS != 0 && mysqli_num_rows($ipresult) > $LIMIT_IP_ADDRESS_SUBMISSIONS)
			{
				$continue = false;
				
			$reason = "<br> <br><div style='color:red; font-size:10pt'> Too many Applications have been submitted from this location: " .  mysqli_num_rows($ipresult) . " Submissions submitted from this Location.<br /></div>";
			}
			
			$emailresult = mlavu_query($emailquery,$email,date("y-m-d"));
			if(mysqli_num_rows($emailresult) && $LIMIT_EMAIL_ADDRESSES != 0 && mysqli_num_rows($emailresult) > $LIMIT_EMAIL_ADDRESSES && strstr($emailresult, 'poslavu'))
			{
				$continue = false;
				$reason = "<br> <br><div style='color:red; font-size:10pt'> Too many Applications have been Submitted From this Email: " . mysqli_num_rows($emailresult) . " Submission From Email Address " . $email . ".<br /></div>";
			}
			
			//print_r($_REQUEST);
			if($continue)
			{
				//Generate unqiue 50 character token for unique url
				$token = generateRandomToken(50);
				
				//Make the entry into the signup section.
				$params = Array (
								'default_menu' => $default_menu,
								'firstname' => $first_name,
								'lastname' => $last_name,
								'ipaddress' => $ipaddress,
								'company' => $company,
								'token' => $token,
								'email' => $email,
								'package' => '4'
								);
				
				$signup_query = "INSERT INTO `poslavu_MAIN_db`.`signups` (`default_menu`,`package`,`firstname`,`lastname`,`company`,`email`,`verification_token`,`ipaddress`) 
				VALUES ('[default_menu]','[package]','[firstname]','[lastname]','[company]','[email]','[token]','[ipaddress]')";
				
				$result = mlavu_query($signup_query, $params);
				if(!$result)
				{
					echo "Was Unable to Create Signup Record.";
					exit(); 
				}
				
				$uri="";
				if( isset($_GET['fromAPP']) && $_GET['fromAPP']==1 && stristr($_SERVER['HTTP_USER_AGENT'], 'iPad') )
					$uri =  'https://' . $_SERVER['SERVER_NAME'] . '/email_activate.php?signup=' . $token."&fromAPP=1";
				else
					$uri =  'https://' . $_SERVER['SERVER_NAME'] . '/email_activate.php?signup=' . $token;
				
				//Send an email with Generated Token based url
				
				//display success message
				$headers = 'From: noreply@lavulite.com' . "\r\n" .
				"Reply-To: noreply@poslavu.com" . "\r\n" .
				"MIME-Version: 1.0\r\n".
				"Content-Type: text/html; charset=ISO-8859-1\r\n";
				//"X-Mailer: PHP/" . phpversion();
				
				$first_name = ucwords($first_name);
				
				$email_body =  "<html>
									<head></head>
									<body>
										<p style='text-align: left;'>Greetings $first_name,</p><br /><br />
										<p style='text-align: left;'>Thank you for signing up with Lavu Lite</p><br>
										<p style='text-align: left;'>Next you will login to your Lavu Lite Control Panel </p><br>
										<p> YOU MUST VERIFY YOUR EMAIL ADDRESS BY CLICKING THE FOLLOWING LINK:<br></p>
										<p style= 'margin: 0 auto'><a href=\"$uri\">ACTIVATE LAVULITE ACCOUNT</a></p>
										<p><br /> Thank you<br>The Lavu Team<br/> </p>
									</body>
								</html>";
				
				mail($email,"Verify Your LavuLite Account",$email_body, $headers);
				echo "
					<div style='font-family:helvetica; font-size:20pt; color:#94b9b9'>
					Check your email and confirm your Lavu Account 
				</div>";

				if( isset($_GET['fromAPP']) && $_GET['fromAPP']==1 && stristr($_SERVER['HTTP_USER_AGENT'], 'iPad') ){
				echo "				
					<div style='padding-top:30px;'> 
						<center>click the link in your confirmation email, then return here. </center>
					</div>
					<div style='padding-top:80px;'> 
						<center><input type='button' style='background: url(images/btn_big_cont.png) no-repeat; cursor:pointer;border: none; width:300px; height:200px;' onclick='verifyCompletedSignupProcess(\"".$token."\")'/> </center>
					</div>";	
				}
			}
			else
			{
				//display failure message
				echo $reason;
			}
		
		}
	}
?>

        </div>
    </div>
</body>
</html>
