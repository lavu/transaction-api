<?php
  	  header( 'Location: https://register.poslavu.com/lavuLiteReservation.php' ) ;
    ini_set('display_errors',1);
    session_start();
    
    include_once("../../../admin/cp/resources/core_functions.php");

    if ($_SERVER['SERVER_PORT']!=443) // quit if not ssl
    {
    	header('Location: https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
        exit();
    }
    if ($_SERVER['HTTP_HOST'] != "register.poslavu.com") // quit if not on register.poslavu.com
    {
    	header('Location: https://'."register.poslavu.com".$_SERVER['REQUEST_URI']);
        exit();
    }
    //if ($_REQUEST['level']!="Silver" && $_REQUEST['level']!="Gold" && $_REQUEST['level']!="Platinum") // only allow valid levels
    //{
        //header('Location: http://www.poslavu.com');
        //exit();
    //}
    
    //if ($_GET['loc']=="es" || $_GET['loc']=="cn") // redirect to translated signup pages
    //{
    //    header('Location: https://register.poslavu.com/'.$_GET['loc'].'/?level='.$_REQUEST['level']);
    //    
    //}

    //if (isset($_POST['posted'])) { require('auth_account.php'); }

    function create_option_list($option_name, $options)
    {
        $str = "";
        foreach($options as $key => $val)
        {
            $selected = "";
            if(isset($_POST[$option_name]) && ($_POST[$option_name]==$key || (is_numeric($_POST[$option_name]) && $_POST[$option_name] * 1 == $key * 1))) 
                $selected = " selected";
            $str .= "<option value=\"$key\"".$selected.">$val</option>";
        }
        return $str;
    }
    
	include_once("../queries/verifone_info.php");
	global $app_id;
	global $api_url;
    
    
    function get_post($option, $params)
    {
	    global $app_id;
	    global $api_url;
	    
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $api_url . $option . "?app_id=" . $app_id . $params);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    
	    return $response;
    }

    function createPlansList()
    {
        global $api_url;
        global $app_id;
        global $live;
        $result = "";
        if($live)
        	$result = "2961";
        else
       		$result = "1";
	    $result = "<input type='hidden' id='plan_id' name='plan_id' value='" . $result . "' />";
        
        return $result;
    }
    
    function createIndustryList()
    {
        global $api_url;
        global $app_id;
        $option = "list_industry_types";
        $str = get_post($option, "");//file_get_contents($api_url . $option . "?app_id=" . $app_id);
        $data = lavu_json_decode(trim($str));
        $result = "";
        
        $filterBy = Array('Restaurant');        
        if($data[0]['status'] == "Success")
        {
            foreach($data[0]['types'] as $k => $v)
            {
            	for($i = 0; $i < count($filterBy); $i++)
            	{
	            	if(strstr($v['label'], $filterBy[$i]) !== FALSE && $v['label'] != 'Restaurants-Fast Food')
	            	{
		          		$result = $result . "<option value='" . $v['id'] . "'>" . $v['label'] . "</option>";  	
	            	}	
            	}
            }
        }
        
        return $result;
    }
    //include_once('translate.php');    
?>
<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>POS Lavu iPad Point of Sale | New Signup</title>
    <link rel="stylesheet" href="../styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <link rel="stylesheet" href="../styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <link rel="stylesheet" href="../styles/register.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <link rel="stylesheet" href="../styles/register_ipad.css?temp=<?php echo rand(0, 1000);?>" type="text/css" media="screen" title="no title" charset="utf-8" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript">
</script>
    <script src="../scripts/jquery.tools.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="../scripts/jquery.validate.js" type="text/javascript" charset="utf-8"></script>
    <script src="../scripts/register.js?temp=<?php echo rand(0, 1000);?>" type="text/javascript" charset="utf-8"></script>
       
</head>

<body>
	<div style="text-align:center;padding:100px 100px;width:990px;margin:50px auto;background:white;box-shadow:5px 5px 10px #b0b0b0;">
		<p style="color:#999;font:24px verdana;">We are temporarily not accepting Lavu Lite Sign ups. Please try back soon!</p>
	</div>
    <div class="content ccentered" style="position: relative; display: none;">
        <!-- Information Section -->
        <form class="frm customer" id="new_customer" action="/create_lavulite_account.php" method="post" >
        	<input type="hidden" name="package" value="Silver" />
            <div class="ccentered container" id="container">
                <div class="scrollarea" id="scrollarea">
                    <!-- <ul> -->
                    	<!-- STEP 1 -->
                        <span class="step" id="step1" style="">
                        	<?php  include_once("../steps/step1.php");?>	
                        </span>
                        
                        <!-- STEP 2 -->
                        
                        <span class="step" id="step2" style="">
                        	<?php include_once("../steps/step2.php");?>	
                        </span>
                        
                        <!-- STEP 3 -->
                        
                        <span class="step" id="step3" style="" >
                        	<?php include_once("../steps/step3.php");?>	
                        </span>
                        
                         <!-- STEP 4 -->
                        
                        <span class="step" id="step4" style="" >
                        	<?php include_once("../steps/step4.php");?>	
                        </span>
                        
                         <!-- STEP 5 -->
                        
                        <span class="step" id="step5" style="" >
                        </span>
                        
                         <!-- STEP 6 -->
                        
                        <span class="step" id="step6" style="" >
                        </span>
                                                
                        <!-- STEP 7 -->
                        
                        <span class="step" id="step7" style="" >
                        </span>
                    <!-- </ul> -->
                    <input type="hidden" name="package" id="package" value="Silver" />
                </div>
            </div>
        </form>
        
        <div id="iframe_scroller1" style="position: absolute; width: 100%; height: 100%; opacity:0.4; background: black; filter:alpha(opacity-40); display: none; top: 0px" onclick="$(this).css('display','none'); $('#iframe_scroller2').css('display','none'); ">
		</div>
		<div id="iframe_scroller2" style="width:679px; height:405px; overflow:hidden; margin:0 auto; padding:0px 0px 0px 0px; background: white; filter:alpha(opacity-100); position: absolute; height: 90%; top: 5%; left: 125px; display: none;">
			<div style="overflow-y: scroll; width: 96%; height: 100%; padding: 10px;">
				<div onclick="$('#iframe_scroller1').css('display','none'); $('#iframe_scroller2').css('display','none'); ">x (close)</div><br /><br />
				<?php 
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, "http://www.poslavu.com/views/tos.html.php");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 0);
		
					$response = curl_exec($ch);
					curl_close($ch);
					
					$start = strpos($response, "<style");
					$end = strpos($response, "</style>");
					
					$first_half = substr($response, 0, $start);
					$second_half = substr($response, $end + 8);
					echo $first_half . $second_half
				?>
			</div>
		</div>
		
    </div>
        <script type="text/javascript">
	        //<![CDATA[
	        $("#dob_year").html(listYears());
	        $("#dob_month").html(listMonths());
	        $("#dob_day").html(listDays());
        	//]]>
        </script>
</body>
</html>
