<?php
	if(!isset($_REQUEST['lavu_lite']))
echo '
<div class="title_header">
	<div class="title_header_small"><div class="shade_left"></div></div>
	<div class="title_header_middle">
		<div class="logo_lavu_lite_sail"></div>
	</div>
	<div class="title_header_small"><div class="shade_right"></div></div>
</div>
';
?>
<div class="banner">
    <br />
    <br />
    <h2>Add Verifone's SAILpay, get a free swipe.<sup>*</sup></h2>
    <h3>No Per-Transaction Fee, No Monthly Fees.</h3>
</div>
<br />
<div class="form_area ccentered">
	<br />
    <div class="step_note" style="width: 500px;">
		<div class="us_flag"></div>
		<span><sup>*</sup>For Credit Card Processing in the USA only.  <u onclick="skipVerifone($('#agree_tos'));" style="color: #aecd37;"><?php echo (isset($_REQUEST['lavu_lite']) && $_REQUEST['lavu_lite'] == '1') ? "Touch" : "Click" ?> Here to Skip!</u></span>
	</div>
	<br />
	<div class="step_info" style="text-align: left">
    	<div class="stepnum step2"></div><span>Setup your SAILpay credit card processing account.</span>
    </div>
    <br />
    <br />
    <h2>1.95% Processing Rate with No Transaction Fees</h2>

    <!--<div class="errors w80p ccentered" id="step2errors" name="step2errors" style="display: none;"></div><br />-->
    <input type="hidden" id="app_id" name="app_id" value="<?php echo $app_id; ?>" />

        <?php echo createPlansList(); ?>
        
        <div class="sidebyside third"><label for="password" >Create Verifone Password</label> <input class="" type="password" id="password" name="password" placeholder="Verifone Password" onblur="lostFocus();" onkeydown="keyDown($('#biz_legal_name'), null);" /></div>
        <div class="sidebyside third"><label for="passwordv" >Verify Verifone Password</label> <input class="" type="password" id="passwordv" name="passwordv" placeholder="Verify Password" onblur="lostFocus();" /></div>
        <div class="sidebyside third"><label for="ssn_last_4" >Last 4</label> <input class="" type="password" id="ssn_last_4" name="ssn_last_4" placeholder="SSN Last 4: 0000" onblur="lostFocus();" /></div>
        <div class="half" style="left: 35px; position: relative; width: 350px; text-align: center"><label for="password" >(At least 7 characters, and must contain a number)</label></div>
        <br />
        <br />

    <div class="full" style="text-align: center; width: 100%;" >
        <label class="full" for="address_1" style="width: 510px; margin: 0 auto;" >Your Address</label>
        <input class="full" type="text" id="address_1" name="address_1" placeholder="Address Line 1" onblur="lostFocus();" style="width: 510px;" /><br />
        <input class="full" type="text" id="address_2" name="address_2" placeholder="Address Line 2" style="width: 510px;" /><br />
        <input type="hidden" name="vf_skip" id="vf_skip" value="0" />
    </div>
    <div class="full" style="width: 510px; margin: 0 auto;">
    	<input type="hidden" id="city" name="city" onblur="lostFocus();"/>
	    <input type="hidden" id="state" name="state" />
	    <br />
	    <br />
	    <label class="full" for="citystate" style="">City/State and Zip</label>
	    <select class="half" id="citystate" name="citystate" style="display: none;" onchange=" updateZip(this,'city','state');" >
        </select>
        <input class="fourth" type="text" id="zip_code" name="zip_code" placeholder="Zip: 00000" onblur="handleZip(this, 'citystate');" oninput="handleZip(this, 'citystate');" /><br />
    </div>

    <br />
    <br />
    <div class="curvebox">
	    <br />
	    <ul style="text-align: center;">
	        <li style="list-style: none; text-align: center;" ><label style="text-align: center;" >Date of Birth</label></li>
	
	        <li class="sidebyside"><label for="dob_month" style="text-align: center;">Month</label> <select class="" id="dob_month" name="dob_month">
	            </select></li>
	
	        <li class="sidebyside"><label for="dob_day" style="text-align: center;">Day</label> <select class="" id="dob_day" name="dob_day">
	            </select></li>
	
	        <li class="sidebyside"><label for="dob_year" style="text-align: center;">Year</label> <select class="" id="dob_year" name="dob_year" onkeydown="keyDown(null,$('#biz_ownership_type'));" >
	            </select></li>
	    </ul>
    </div><br />
</div>
<br />
<div class="divider"></div>
<input type="text" class="hidefield" /><input type="text" class="hidefield" /><input type="text" class="hidefield" /><input type="text" class="hidefield" />

<div class="footer">
	<button class="back" type="button" id="step2back" onclick="backStep($('#biz_legal_name'))" >Back</button>
	<button class="skip" type="button" id="skip_vf" name="skip_vf" onclick="skipVerifone($('#agree_tos')); ">Skip SAILpay Signup</button>
    <button class="next" type="button" id="step2complete" onclick="nextStep($('#biz_ownership_type'))" onkeydown="keyDown(null,$('#biz_ownership_type'));" >Next</button>
</div>
