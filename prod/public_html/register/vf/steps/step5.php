<?php
if(!isset($_REQUEST['lavu_lite'])){
	echo '
<div class="title_header">
	<div class="title_header_small"><div class="shade_left"></div></div>
	<div class="title_header_middle">
		<div class="logo_lavu_lite"></div>
	</div>
	<div class="title_header_small"><div class="shade_right"></div></div>
</div>';
}
?>
<div class="banner">
    <br />
    <br />
    <h2>14-Day Free Trial</h2>
    <h3>Please Fill Out the Necessary Information to Begin.</h3>
</div>
<br />
<!--<div class="errors w80p ccentered" id="step5errors" name="step5errors" style="display: none;"></div><br />-->
<div class="form_area ccentered">
	   <br />
	<br />
	<br />
	<div class="step_info" style="text-align: left">
    	<div class="stepnum step5"></div><span>Please Verify your Age by Specifying your Credit Card Information.</span>
    </div>
    <br />
    <br />
    <div class="errors w80p ccentered" id="step5errors" name="step5errors" style="display: none;"></div><br />
    <div style="width: 550px; margin: 0 auto;">
    	<div class="half sidebyside">
		    <label class="full" for="card_number">Credit Card Number</label>
		    <input id="card_number" name="card_number" placeholder="0000000000000000" type="text" class="full" /><br />
    	</div>	    
	    <div class="sidebyside half">
	    	<label class="full">Expires on</label>
	    	<select class="half" id="card_expiration_mo" name="card_expiration_mo">
	    		<option></option>
	    		<option value="01">1 - January</option>
	    		<option value="02">2 - February</option>
	    		<option value="03">3 - March</option>
	    		<option value="04">4 - April</option>
	    		<option value="05">5 - May</option>
	    		<option value="06">6 - June</option>
	    		<option value="07">7 - July</option>
	    		<option value="08">8 - August</option>
	    		<option value="09">9 - September</option>
	    		<option value="10">10 - October</option>
	    		<option value="11">11 - November</option>
	    		<option value="12">12 - December</option>
	    	</select>
	    	<select class="half" id="card_expiration_yr" name="card_expiration_yr">
	    	</select>
	    </div><br /><br />
	    <div class="sidebyside half">
	    	<label for="card_code">Security Code (CCV)</label>
	    	<input class="full" id="card_code" name="card_code" type="text" />
	    </div><br /><br />
	    
	    <div class="half sidebyside">
		    <label class="full" for="promo_code">Promo</label>
		    <input class="full" id="promo_code" name="promo_code" type="text" /><br />
	    </div>
	    <div class="half sidebyside">
		    <label class="full" for="referral">Where Did you Hear About Us?</label>
		    <select class="full" id="referral" name="referral">
		    	<option></option>
		    	<option value="sysco_icare">Sysco iCare</option>
		    	<option value="distro">Certified Distributor</option>
		    	<option value="google">Google</option>
		    	<option value="facebook">Facebook</option>
		    	<option value="expo">An Expo</option>
		    	<option value="other">Other</option>
		    </select>
	    </div>
    </div>
    
</div>
<div style="height: 150px;"></div>

<div class="divider"></div>
<input type="text" class="hidefield" /><input type="text" class="hidefield" /><input type="text" class="hidefield" /><input type="text" class="hidefield" />
<div class="footer">

	<?php
		if( isset($_REQUEST['lavu_lite']) )
			echo '<button class="back" type="button" id="step4back" onclick="backStep($(\'#tos_agree\'))" >Back</button>';
		else
			echo '<button class="next" type="button" id="step4complete" onclick="nextStep()" >Submit</button>';
	?>
</div>

<script type="text/javascript">$('#card_expiration_yr').html("<option></option>" + listYears10());</script>