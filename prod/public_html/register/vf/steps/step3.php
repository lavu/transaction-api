<?php
if(!isset($_REQUEST['lavu_lite'])){
	echo '
<div class="title_header">
	<div class="title_header_small"><div class="shade_left"></div></div>
	<div class="title_header_middle">
		<div class="logo_lavu_lite"></div>
	</div>
	<div class="title_header_small"><div class="shade_right"></div></div>
</div>';
}
?>
<div class="banner">
    <br />
    <br />
    <h2>Good Choice - SAILpay rate - 1.95%</h2>
    <h3>No Contracts. Cancel at Any Time.</h3>
</div>
<br />
<div class="form_area ccentered">
    <br />
	<br />
	<br />
	<div class="step_info" style="text-align: left">
    	<div class="stepnum step3"></div><span>Tell us about your business. you can edit later in the Control Panel (CP).</span>
    </div>
    <br />
    <br />
    <!--<div class="errors w80p ccentered" id="step3errors" name="step3errors" style="display: none;"></div><br />-->
    <p></p>
    <div class="sidebyside fourth" style="padding: 5px;">
	    <label class="full" for="biz_ownership_type">Ownership Type</label>
	    <select class="full" id="biz_ownership_type" name="biz_ownership_type" onkeydown="keyDown($('#dob_year'),null);" onblur="lostFocus();">
	    	<option></option>
	        <option value="1">
	            Corporation
	        </option>
	        <option value="2">
	            LLC
	        </option>
	        <option value="3">
	            Sole Proprietorship
	        </option>
	        <option value="4">
	            Partnership
	        </option>
	    </select>
    </div>
    <div class="sidebyside half" style="padding: 5px">
    	<label class="full" for="biz_ownership_type">Industry Type</label>
	    <select class="full" id="biz_industry_type" name="biz_industry_type" onblur="lostFocus();">
	    	<option></option>
	        <?php echo createIndustryList(); ?>
	    </select>
    </div>
    <br />
    <br />
    <br />

    <div style="text-align: center;">
        <label for="biz_address_1" style="width: 510px; margin: 0 auto;">Business Address</label>
        <input class="" type="text" id="biz_address_1" name="biz_address_1" placeholder="Business Address Line 1" onblur="lostFocus();" style="width: 510px;" /><br />
        <input class="" type="text" id="biz_address_2" name="biz_address_2" placeholder="Business Address Line 2" style="width: 510px;" />
    </div>
    <br />
    <br />
    <div class="full" style="width: 425px; margin: 0 auto;" />
	    <input type="hidden" id="biz_city" name="biz_city" onblur="lostFocus();" /> 
	    <input type="hidden" id="biz_state" name="biz_state" />
	    <br />
	    <br />
	    <label class="full" for="biz_citystate" style="">Business City/State and Zip</label>
	    <select class="half" id="biz_citystate" name="biz_citystate" style="display: none;" onchange="updateZip(this,'biz_city','biz_state');" >
	    </select>
	    <input class="fourth" type="text" id="biz_zip_code" name="biz_zip_code" placeholder="00000" onblur="handleZip(this, 'biz_citystate');" oninput="handleZip(this, 'biz_citystate');" /><br />
    </div>
    <br />
    <div class="sidebyside fourth" style="padding: 5px;">
    	<label class="full" for="biz_phone">Business Phone</label>
	    <input class="full" type="text" id="biz_phone" name="biz_phone" placeholder="(000) 000 - 0000" onchange="$('#phone').val($(this).val());" onblur="lostFocus();" />
    </div>
    <div class="sidebyside half" style="padding: 5px;">
    	<label class="full" for="biz_ein">Employer Identification Number</label>
	    <input class="full" type="text" id="biz_ein" name="biz_ein" placeholder="Employer Identification Number" /><br />
    </div>
    <br />
    <br />
    <div class="half" style="margin: 0 auto;" >
	    <label class="full" for="device_ship_to">Select Where to Ship the Swiper</label>
	    <select class="full" id="device_ship_to" name="device_ship_to" onkeydown="keyDown(null,$('#agree_tos'));" >
	    	<option></option>
	        <option value="owner">Personal Address</option>
	        <option value="business">Business Address</option>
	        <option value="none">I Don't Need/Want a Swiper</option>
	    </select>
    </div>
</div>

<br />
<div class="divider"></div>
<input type="text" class="hidefield" /><input type="text" class="hidefield" /><input type="text" class="hidefield" /><input type="text" class="hidefield" />
<div class="footer">
	<button class="back" type="button" id="step3back" onclick="backStep($('#password'))" >Back</button>
    <button class="next" type="button" id="step3complete" onclick="nextStep($('#agree_tos'))" onkeydown="keyDown(null,$('#agree_tos'));" >Next</button>
</div>
