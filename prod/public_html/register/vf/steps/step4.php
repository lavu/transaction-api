<?php
if(!isset($_REQUEST['lavu_lite'])){
	echo '
<div class="title_header">
	<div class="title_header_small"><div class="shade_left"></div></div>
	<div class="title_header_middle">
		<div class="logo_lavu_lite"></div>
	</div>
	<div class="title_header_small"><div class="shade_right"></div></div>
</div>';
}
?>
<div class="banner">
    <br />
    <br />
    <h2>What Type of Business are You?</h2>
    <h3>Choose the closest example.</h3>
</div>
<br />
<!--<div class="errors w80p ccentered" id="step4errors" name="step4errors" style="display: none;"></div><br />-->
<div class="form_area ccentered" style="width: 700px">
	   <br />
	<br />
	<br />
	<div class="step_info" style="text-align: left">
    	<div class="stepnum step4"></div><span>Your account will be preconfigured with a sample menu and settings.</span>
    </div>
    <br />
    <br />
	<div class="" id="selectionsection" style="text-align: center;" >
		<ul>
    		<li class="sidebyside">
    			<div class="selection" id="default_restau" onclick="select(this); lostFocus();" style="margin: 2px; box-shadow: 0px 0px 10px;">
            		<img src="<?php echo isset($_REQUEST['lavu_lite']) ? "../../" : ""; ?>images/type_3.png" width="100" height="60" title="This Restaurant starter menu uses a table layout." />
            		<div class="boxtitle">Restaurant</div>
            		<br />
            		<div class="select_link ccentered" >Select</div>
    			</div>
    		</li>
    		<li class="sidebyside">
    			<div class="selection" id="default_pizza_" onclick="select(this); lostFocus(); " style="margin: 2px; box-shadow: 0px 0px 10px;" >
        			<img src="<?php echo isset($_REQUEST['lavu_lite']) ? "../../" : ""; ?>images/type_4.png" width="100" height="60" title="This Restaurant starter menu uses a table layout." />
        			<div class="boxtitle">Pizza Shop</div>
        			<br />
        			<div class="select_link ccentered" >Select</div>
    			</div>
    		</li>
    		<li class="sidebyside">
    			<div class="selection" id="defaultbar" onclick="select(this); lostFocus();" style="margin: 2px; box-shadow: 0px 0px 10px;" >
            		<img src="<?php echo isset($_REQUEST['lavu_lite']) ?  "../../" : ""; ?>images/type_2.png" width="100" height="60" title="This Bar/Lounge start menu uses a tab layout.  Tabs are not tethered to a table." />
            		<div class="boxtitle">Bar/Lounge</div>
            		<br />
            		<div class="select_link ccentered" >Select</div>
    			</div>
    		</li>
    		<li class="sidebyside">
    			<div class="selection" id="default_coffee" onclick="select(this); lostFocus();" style="margin: 2px; box-shadow: 0px 0px 10px;">
        			<img src="<?php echo isset($_REQUEST['lavu_lite']) ? "../../" : ""; ?>images/type_1.png" width="100" height="60" title="This Coffee Shop start menu is best for quick serve environments generally using single transactions." />
        			<div class="boxtitle">Coffee Shop</div>
        			<br />
        			<div class="select_link ccentered" >Select</div>
    			</div>
    		</li>
		</ul>
	</div>
	<br />
	<div class="ccentered" style="width: 300px; text-align: center;">
		<input id="template" name="template" type="hidden" />
		<p id="tosagree" class="agree"><input class="" type="checkbox" name="agree_tos" id="agree_tos" onchange=" lostFocus();" onkeydown="keyDown($('#device_ship_to'), null);" />By checking here you agree to our <a onclick="$('#iframe_scroller2').css('display', 'block'); $('#iframe_scroller1').css('display', 'block');">Terms of Service</a>.</p>
	</div>
	<br />
</div>
<div style="height: 50px;"></div>

<div class="divider"></div>
<input type="text" class="hidefield" /><input type="text" class="hidefield" /><input type="text" class="hidefield" /><input type="text" class="hidefield" />
<div class="footer">
	<button class="back" type="button" id="step4back" onclick="backStep($('#biz_ownership_type'))" >Back</button>
	
	<?php
		if( isset($_REQUEST['lavu_lite']) )
    		echo '<button class="next" type="button" id="step4complete" onclick="nextStep(\'lavu_lite\')" onkeydown="keyDown($(\'#device_ship_to\'), null);">Submit</button>';
	    else
			echo '<button class="next" type="button" id="step4complete" onclick="nextStep()" onkeydown="keyDown($(\'#device_ship_to\'), null);">Submit</button>';
	?>
</div>