<?php
if (getenv('DEV') == '1') {
	@define('DEV', 1);
	ini_set("display_errors","1");
	error_reporting(error_reporting() ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
} else {
	@define('DEV', 0);
}

global $error_content;

function postval( $val ) {
    echo !empty( $_POST[$val] )? $_POST[$val]: "";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>POS Lavu iPad Point of Sale  | New Signup</title>
	<link rel="stylesheet" href="/styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="/styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="/styles/register_new.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="/styles/step2.css" type="text/css" media="screen" title="no title" charset="utf-8">
</head>

<body>
	<div class="header-wrapper">
		<header>
			<p id="header-logo" class="clearfix">
				<a class="lavu-logo" href="http://www.poslavu.com">
					<img src="/images/lavu-logo.png" alt="Lavu | Point of Sale" />
			    </a>
	            <sub class="little-text">RESTAURANT POS FOR iPAD</sub>
	        </p>
			<span id="header-phone">1-855-767-5288</span>
	    </header>
	</div>
	<div id="app_content_wrapper">
		<div id="app_content" class="clearfix">
			<div class="left leftcol">
				<p class="step-counter">Step 2 of 2 |
					<span class="capitals"><?= $o_package_container->get_printed_name_by_attribute('name', $_REQUEST['level']); ?> account</span>
				</p>
<!-- 				<h2 class="signup_tagline">14 Day Free Trial</h2> -->
				<h2 class="signup_tagline" style="font-size:32px">Why do we verify credit card info?</h2>
<!-- 				<h3 class="signup_tagline_2 capitals"><b>It's easy</b>. Try Lavu Today</h3> -->
				<h3 class="signup_tagline_2 capitals" style="font-size:16px;color:#2b2b2b;">You take your business seriously and we do too. </h3>
				<p>
							Verifying billing information reduces the amount
							of casual accounts, which allows us more time to devote to you. Plus, should you choose to continue
							to use Lavu after trial, there will be no interruption in service and your settings and menu will
							remain.
						</p>
				<div>
					<div>
						<!-- <p>
							Would you like to continue right now and get your Free Trial started? It is 
							very easy to do yourself and we are always available to help. You can cancel
							anytime without our assistance - no obligations.
						</p> -->
						<div class="first" style="margin:12em 0 0 10px !important;">
						<!-- <h3 >Authorization</h3>  (arial 30 #494949) -->
						<p>
							<em>To verify a valid credit card, your bank will temporarily show an authorization for validation
							purposes only. <b>This amount will not be posted / charged to your account during the Trial Period</b></em>
						</p>
					</div>
					<div class="second">  
						<h3 style="margin-top:3em;font:normal 30px Arial, sans-serif;color:#2b2b2b">Worry Free!</h3> <!-- (arial 30 #494949) -->
						<p style="font:18px Arial,sans-serif;color:#494949;)">You can cancel at any time, with no penalties.</p> 

						<p style="font:bold 13px Arial, sans-serif;color:#494949">You will not be billed unless you continue to use Lavu 
						and keep your account open past the 14 day Free Trial.</p>
						
						<p style="font:italic 13px Arial,sans-serif;color:#494949;">Failure to cancel your Free Trial before the 14 day expiration results in a $50 USD cancelation / refund fee. Refunds result in an administration fee by merchant service providers and this is why we must enforce this policy.</p>  
						<h3 class="capitals lavugreen" style="font-size:18px;">Risk Free. Give Lavu a try!</h3>
					</div>
					</div>
				</div>
			</div>
			<!-- <div class="tooltips"></div>  -->
				<?= $error_content ?>
				<?php
					$a_getvars = array();
					foreach($_GET as $k=>$v)
						$a_getvars[] = "$k=$v";
					if (isset($_GET['no_lead']) && $_GET['no_lead']) {
						?>
						<div id="no_lead" style="position:fixed; top:5px; left:5px; background-color:rgba(0,0,0,0.7); border:1px solid white; border-radius:5px; color:white; padding:5px;">
							No lead information will be generated for this signup.
						</div>
						<?php
					}
				?>

			<form action="<?= $_SERVER['PHP_SELF'].'?'.implode('&',$a_getvars); ?>" class="frm customer step2" id="new_customer" method="POST">
				<?php
				if (isset($rowid)) // from auth_account.php
					$_POST['rowid'] = $rowid;
				if (!isset($_POST['reseller_leads_rowid']))
					$_POST['reseller_leads_rowid'] = $i_reseller_leads_id;
				if (!isset($_POST['posted']))
					$_POST['posted'] = 1;
				if (!isset($_POST['company']) && isset($_POST['company_name']))
					$_POST['company'] = $_POST['company_name'];
				if (!isset($_POST['full_name']))
					$_POST['full_name'] = '';
				$a_names = explode(' ', $_POST['full_name'], 2);
				if (!isset($_POST['firstname']) && isset($a_names[0]))
				    $_POST['firstname'] = $a_names[0];
				if (!isset($_POST['lastname']) && isset($a_names[1]))
					$_POST['lastname'] = $a_names[1];
				foreach($_POST as $k=>$v) {
					echo '<input type="hidden" name="'.$k.'" value="'.$v.'" />
				';
				}
				?>


				<div class="right" id="signupccinfo">

					<div class="form_container">
						<h3>Credit Card Information</h3>
						<ul>
							<li>
								<label for="card_number">Credit card number*</label>
								<input id="card_number" name="card_number" size="30" type="text" />
							</li>
							<li class="fieldsmall left expdate">
								<label for="card_expiration">Expires on*</label>
								<select id="card_expiration_mo" name="card_expiration_mo">
									<option value=""></option>
									<?= create_option_list("card_expiration_mo",array(
										"01"=>"1 - Jan",
										"02"=>"2 - Feb",
										"03"=>"3 - Mar",
										"04"=>"4 - Apr",
										"05"=>"5 - May",
										"06"=>"6 - Jun",
										"07"=>"7 - Jul",
										"08"=>"8 - Aug",
										"09"=>"9 - Sep",
										"10"=>"10 - Oct",
										"11"=>"11 - Nov",
										"12"=>"12 - Dec")
									); ?>
								</select>

								<select id="card_expiration_yr" name="card_expiration_yr">
									<option value=""></option>
						            <?php
						            	$a_years = array();
						            	for ($i = 0; $i < 11; $i++) {
											$year = date('Y', strtotime('+'.$i.' years'));
											$a_years[$year] = $year;
										}
						            	echo create_option_list("card_expiration_yr", $a_years); ?>
								</select>

							</li>
							<li class="fieldsmall right ccvinput">
								<label for="card_code">Security Code (CVV)*</label>
								<input id="card_code" class="inputsmall" name="card_code" size="30" type="text" value="<?php postval('card_code') ?>" />
							</li>
						</ul>
					</div>

					<div class="form_container">
						<h3>Billing Information</h3>
						<ul>

							<li class="fieldsmall fieldleft">
								<label for="firstname">First Name *</label>
								<input id="firstname" class="inputsmall" name="firstname" size="30" type="text" value="<?php postval('firstname') ?>" />
							</li>
							<li class="fieldsmall right">
								<label for="lastname">Last Name *</label>
								<input id="lastname" class="inputsmall" name="lastname" size="30" type="text" value="<?php postval('lastname') ?>" />
							</li>

							<li class="fieldsmall fieldleft">
								<label for="city">City *</label>
								<input id="city" class="inputsmall" name="city" size="30" type="text" value="<?php postval('city') ?>" />
							</li>
							<li class="fieldsmall right">
								<label for="state">State</label>
								<input id="state" class="inputsmall" name="state" size="30" type="text" value="<?php postval('state') ?>" />
							</li>

							<li>
								<label for="address">Address*</label>
								<input id="address" name="address" size="30" type="text" value="<?php postval('address') ?>" />
							</li>

							<li class="fieldsmall fieldleft">
								<label for="zip">Postal Code *</label>
								<input id="zip" class="inputsmall" name="zip" size="30" type="text" value="<?= $_POST['zip'] ?>" />

							</li>
							<li class="fieldsmall right">
								<label for="promo">Promo Code</label>
								<input id="promo" class="inputsmall" name="promo" size="30" type="text" value="<?php postval('promo') ?>" />
							</li>

						</ul>
						<?php
							if (!$_SERVER['DEV'] && ($b_weekly_signups || $b_failed_ccs)) {
								$public_key = "6LfrtOsSAAAAAAIvqmehbwGW6vBo8GI1qGL_ngVR";
								echo str_replace("http", "https", recaptcha_get_html($public_key));
							}
						?>
					</div>
					<div class="right" id="agreement">

					<p class="agree">
						By checking here you agree to our <a href="http://www.poslavu.com/en/tos" target="_blank">Terms of Service</a>.
					</p>
					<div id="submit_checkbox">
							<input type="checkbox" name="agree_tos" value="1" id="agree_tos" />
						    <!-- <img src="/images/trans.png" width="54" height="54" alt="" id="fake_agree_tos" /> -->
					</div>
					
				<input class="app-submit-button new-colors" id="subscription_submit" name="finish_signup" type="submit" value="Create my account &rsaquo;" />
						</div>
				</div>

			</form>
		</div>
	</div>

<!--[if lt IE 10]><script src="/scripts/json2.min.js" type="text/javascript"></script><![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
<script src="/scripts/jquery.validate.js" type="text/javascript"></script>
<script src="/scripts/step2<?php echo $GLOBALS['s_dev']?>.js" type="text/javascript"></script>
<script src="/scripts/tracking.js" type="text/javascript"></script>
</body>
</html>
