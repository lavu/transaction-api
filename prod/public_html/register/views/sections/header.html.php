<!DOCTYPE html>
<!--[if lt IE 7]><html class="ie ie6 oldie" lang="en"><![endif]-->
<!--[if IE 7]><html class="ie ie7 oldie" lang="en"><![endif]-->
<!--[if IE 8]><html class="ie ie8 oldie" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie ie9 modern" lang="en"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-ie modern" lang="en"><!--<![endif]-->
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="initial-scale=1">
    <link rel="stylesheet" href="/styles/lavu88.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/styles/lavu88_fullstyle.css" type="text/css"
          media="screen and (min-width:640px)" />
    <link rel="stylesheet" href="/styles/lavu88_bigscreen.css" type="text/css"
          media="screen and (min-width:990px)" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="/styles/lavu88_fullstyle.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/styles/lavu88_bigscreen.css" type="text/css" media="screen" />
    <![endif]-->
    <?php if( !empty( $extra_css ) ) echo $extra_css; ?>
    <title><?= !empty( $title ) ? $title : "Lavu iPad POS - Sign In Now!" ?></title>
</head>
<body<?php echo !empty( $level) && strtolower( $level ) === "lavu88" ? " class=\"lavu88\"" : "";?>>
<div id="modal"> </div>
<header>
    <div class="wrapper clearfix">
        <p id="header-logo" class="clearfix">
            <a class="lavu-logo" href="http://www.poslavu.com">
                <img src="/images/lavu-logo.png" alt="Lavu | Point of Sale" />
            </a>
            <sub class="little-text">RESTAURANT POS FOR iPAD</sub>
            <span id="header-phone" class="float-right">1-855-767-5288</span>
        </p>
    </div>
</header>