<div class="form_container" id="starter-menu">
<div style="margin:0 auto;width:90%;" id="boxlinks">
<?php
$h = 130;
$o_signup = new RegisterSignupShared();
$o_niche_products = $o_signup->getNicheProducts();
$starter_menu = empty( $o_niche_products->$product ) ? "all" : $o_niche_products->$product;
if ( $starter_menu === "all" ): ?>
<h3><?php
    $choose = $locale === "cn" ? "选择入门菜单" :
        ( $locale === "es" ? "Elija Menú Starter" : "Choose Starter Menu");
    echo !empty($GLOBALS['b_is_niche']) ? 'Your Starter Menu' : $choose;
    ?></h3>


<?php
/***********************************************************************
 *                    D R A W   T H E   M E N U
 **********************************************************************/
$select = $locale === "cn" ? "选择" : ( $locale === "es"? "Selecciona" : "Select");
ob_start();
?>
    <input class="radioMenu required" name="mname"
           id="starter-menu-1" value="default_restau" type="radio" />
    <div class="starter-menu-box box-1">
        <div class="starter-menu">
            <img src="/images/restaurant_icon_100x50px.png" class="starter_menu_image" />


                <div class="tooltip"><?php
                    echo $locale === "cn" ? "此餐厅起动菜单使用表格布局。":
                        ( $locale === "es" ? "Este menú de arranque restaurante utiliza un diseño de tabla." :
                            "This Restaurant starter menu uses a table layout.");
                    ?>
                </div>
        </div>

                            <span class="boxtitle"><?php
                                echo $locale === "cn" ? "餐厅" :
                                    ( $locale === "es"? "Restaurante": "Restaurant"); ?></span>

                    <div class="select_link">
                        <label for="starter-menu-1" class="signup menu" title="1"><?php echo $select ?></label>
                    </div>
        </div>

    <input class="radioMenu" name="mname"
           id="starter-menu-2" value="default_pizza_" type="radio" />
    <div class="starter-menu-box box-2">
        <div class="starter-menu">
            <img src="/images/pizza_icon_100x50px.png" class="starter_menu_image" />

            <div class="tooltip"><?php
                echo $locale === "cn" ? "这比萨饼店起动菜单使用表布局，并提供了一些特殊的功能":
                    ( $locale === "es"? "Este menú de arranque Pizzeria utiliza un diseño de tabla y proporciona alguna funcionalidad especial.":
                        "This Pizza Shop starter menu uses a table layout and provides some special functionality.");
                ?>
            </div>
        </div>

                            <span class="boxtitle"><?php
                                echo $locale === "cn" ? "比萨饼店" :
                                    ($locale === "es" ? "Pizzeria" : "Pizza Shop");
                                ?></span>

                    <div class="select_link">

                        <label for="starter-menu-2" class="signup menu" title="2"><?php echo $select; ?></label>
                    </div>
                </div>

    <input class="radioMenu" name="mname"
           id="starter-menu-3" value="defaultbar" type="radio" />
    <div class="starter-menu-box box-3">

        <div class="starter-menu">
            <img src="/images/bar_icon_100x50px.png" class="starter_menu_image" alt="Bar/Lounge" />

                <div class="tooltip"><?php
                    echo $locale === "cn" ? "这家酒吧/酒廊起动菜单使用tab布局。制表符不被拴到表中。" :
                        ( $locale === "es" ? "Este menú de arranque Bar/Salón utiliza un diseño de las tabulación. Tabulaciónes no están atados a una mesa." :
                            "This Bar/Lounge starter menu uses a tab layout. Tabs are not tethered to a table." );
                    ?>
                </div>
        </div>

                            <span class="boxtitle"><?php
                                echo $locale === "cn" ? "酒吧或休息室":
                                    ( $locale === "es" ? "Bar/Salón" :
                                        "Bar/Lounge"); ?></span>

                    <div class="select_link">
                        <label for="starter-menu-3" class="signup menu" title="3"><?php echo $select; ?></label>
                    </div>
     </div>
    <input class="radioMenu" name="mname" type="radio"
           id="starter-menu-4" value="default_coffee" />
    <div class="starter-menu-box box-4">

        <div class="starter-menu">
            <img src="/images/coffee_icon_100x50px.png" alt="Coffee Shop"  class="starter_menu_image"/>

                <div class="tooltip"><?php
                    echo $locale === "cn"? "此咖啡厅起动菜单是最好的快速服务环境通常使用单交易。" :
                        ( $locale === "es" ? "Este menú de arranque Café es el mejor para entornos servicio rápid generalmente utilizando transacciones individuales." :
                            "This Coffee Shop starter menu is best for quick serve environments generally using single transactions.")
                    ?>
                </div>
        </div>

                            <span class="boxtitle"><?php
                                echo $locale === "cn"? "咖啡馆":
                                    ( $locale === "es"  ? "Café" :"Coffee Shop")?></span>
                    <div class="select_link">
                        <label for="starter-menu-4" class="signup menu" title="4"><?php echo $select; ?></label>
                    </div>
        </div>
<?php else:
$l = $locale;
$l = $l === "en" ? "" : $l;
$s_niche_text = $starter_menu["display_name$l"];
$s_niche_title = $starter_menu["title$l"];
$s_niche_image = $starter_menu['image'];
$mname = $starter_menu['posts']['mname'];
$b_is_niche = true;
$h = 150;
?>
    <div class="center">
    <input class="radioMenu" id="starter-menu-single" name="mname" value="<?= $mname ?>" type="radio"<?= $b_is_niche ? ' CHECKED' : '' ?> />

                <div class="starter-menu-box">

                        <div class="starter_menu_image">
                            <img src="<?= $s_niche_image ?>" width="100" height="60" />
                            <div class="tooltip"><?= $s_niche_title ?></div>
                        </div>
                        <div class="select_link">
                            <label for="starter-menu-single" class="signup menu" title="4"><?=  $s_niche_text ?></label>
                        </div>

                </div>
    </div>
        <?php endif; ?>


        <label for="mname" class="error" style="font-size:18px;">Please choose a starter menu.</label>
            <span class="maintextlight">
                <img src="/images/i_help_sm.png" alt="Notice"><?php
echo $locale === "cn" ? "入门菜单只能作为指导，不影响任何未来的菜单设置决定。您将能够完成这一初始设置后插入自己的菜单。" :
($locale  === "es" ? "Un menú de arranque sólo se utiliza como una guía y no afecta a ningún futuras decisiones del menú de configuración. Usted será capaz de insertar su propio menú después de la configuración inicial.":
"A Starter Menu is only used as a guide and does not affect any future menu setup decisions. You will be able to insert your own menu after this initial setup.")
            ?></span>
  </div>