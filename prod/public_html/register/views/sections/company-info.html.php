<?php
$company = empty( $REQUEST['company_name'] ) ? '' : $_REQUEST['company_name'];
$phone = empty( $_REQUEST['phone'] ) ? '' : $_REQUEST['phone'];
$email = empty( $_REQUEST['email'] ) ? '' : $_REQUEST['email'];
$zip = empty( $_REQUEST['zip'] ) ? '' : $_REQUEST['zip']; 
$name = empty( $_REQUEST['name'] ) ? '' : $_REQUEST['name'];
?>
<div class="form_container" id="company-info">
    <p class="center company-info__emblem">
        <img src="/images/circle_arrow_2b_88.png" alt="" />
    </p>
    <h3><?php echo $locale === "es" ? "Informaci&oacute;n de su Empresa" :
                   ($locale === "cn" ? "公司信息":
                   "Company Information"); ?></h3>
    <ul>
        <li>
            <label for="company_name"><?php
            echo $locale === "es" ? "Nombre de la compañía *":
                 ($locale === "cn" ? "公司名稱*":
                 "Company Name *");
            ?></label>
            <input id="company_name" class="required" name="company_name" type="text"
            value="<?php echo $company; ?>" placeholder=" COMPANY NAME *" />
        </li>

        <li class="fieldsmall fieldleft">
            <label for="phone"><?php
            echo $locale === "es" ? "Teléfono *" :
                 ($locale === "cn" ? "電話*":
                 "Phone *");
                ?></label>
            <input class="inputsmall required" id="phone" name="phone" type="tel"
            value="<?php echo $phone; ?>" placeholder=" PHONE *"/>
        </li>
        <li class="fieldsmall fieldright">
            <label for="email"><?php
            echo $locale === "es" ? "Email *":
                 ($locale === "cn" ? "電子郵件*":
                 "Email *"); 
                ?></label>
            <input class="inputsmall required email" id="email" name="email" type="email"
            value="<?php echo $email; ?>" placeholder=" EMAIL *"/>
        </li>

        <li class="fieldsmall fieldleft">
            <label for="zip"><?php
            echo $locale === "es" ? "Código Postal *":
                 ($locale === "cn" ? "邮政编码*":
                 "Postal Code *");?></label>
            <input class="inputsmall required" id="zip" name="zip" type="text"
            value="<?php echo $zip; ?>" placeholder=" ZIP CODE *" />
        </li>
        <li class="fieldsmall fieldright">
            <label for="full_name"><?php
            echo $locale === "es" ? "Nombre y Apellidos":
                 ($locale === "cn" ? "全名":
                 "Full Name");
                ?></label>
            <input class="inputsmall" id="full_name" name="full_name"
            type="text" value="<?php echo $name; ?>" placeholder=" FULL NAME" />
        </li>
    </ul>
</div>