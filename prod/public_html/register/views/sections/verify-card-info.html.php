<?php
$level_translations = array( 
    "lavu88" => "Lavu 88",
    "silver" => "Silver",
    "silver2" => "Silver",
    "silver3" => "Silver",
    "gold" => "Gold",
    "gold2" => "Gold",
    "gold3" => "Gold",
    "platinum" => "Pro",
    "platinum2" => "Pro",
    "platinum3" => "Pro",
    "pro" => "Pro",
    "fitsilver" => "Fit Silver",
    "fitgold" => "Fit Gold",
    "fitpro" => "Fit Pro"
);
if ( empty($_REQUEST['source'] ) ):?>
<div class="left leftcol">
    
    <p class="step-counter">Step 2 of 2 |
        <span class="upper"><?= $level_translations[strtolower($level)] ?> account</span>
    </p>
   <!-- <h2 class="signup_tagline">14 Day Free Trial</h2> -->
    <h2 class="signup_tagline lavugreen">Why do we verify credit card info?</h2>
   <!-- <h3 class="signup_tagline_2 capitals"><b>It's easy</b>. Try Lavu Today</h3> -->
   
    <div id="verify-top">
    <h3 class="signup_tagline_2 upper">You take your business seriously and we
    do too. </h3>
    <p>
        Verifying billing information reduces the amount
        of casual accounts, which allows us more time to devote to you. Plus,
        should you choose to continue to use Lavu after trial, there will be no
        interruption in service and your settings and menu will remain.
    </p></div>
    
    <div id="verify-bottom">
        <!-- <p>
            Would you like to continue right now and get your Free Trial started? It is 
            very easy to do yourself and we are always available to help. You can cancel
            anytime without our assistance - no obligations.
        </p> -->
        <div class="first">

            <p>
                <em>To verify a valid credit card, your bank will temporarily
                show an authorization for validation purposes only. <b>This
                amount will not be posted / charged to your account during the
                Trial Period</b></em>
            </p>
        </div>
        <div class="second">  
            <h2>Worry Free!</h2> <!-- (arial 30 #494949) -->
            <h3>You can cancel at any time, with no penalties.</h3> 
        
            <p><strong>You will not be billed unless you continue to use Lavu 
            and keep your account open past the 14 day Free Trial.</strong></p>
            
            <p><em>Failure to cancel your Free Trial before the 14 day expiration
            results in a $50 USD cancelation / refund fee. Refunds result in an
            administration fee by merchant service providers and this is why we
            must enforce this policy.</em></p>  
            <h4 class="upper lavugreen">Risk Free. Give Lavu a try!</h4>
        </div>
    </div>
    
</div>
<?php else: ?>
    <div class="left leftcol">

        <p class="step-counter">Step 2 of 2 |
            <span class="upper"><?= $level_translations[strtolower($level)] ?> account</span>
        </p>

        <h2 class="signup_tagline lavugreen">You're almost there!</h2>

        <div id="verify-top">
            <h3 class="signup_tagline_2 upper">
                Complete this form and start using Lavu today.
            </h3>
            <p>
                You can setup your menu and inventory, customize your business
                settings and get your business running.
            </p>
        </div>

        <div id="verify-bottom">
            <div class="second">
                <h2>Worry Free!</h2> <!-- (arial 30 #494949) -->
                <h3>You can cancel at any time, with no penalties.</h3>
                <h4 class="upper lavugreen">Risk Free. Give Lavu a try!</h4>
            </div>
        </div>

    </div>
<?php endif;?>