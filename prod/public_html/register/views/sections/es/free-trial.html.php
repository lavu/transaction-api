<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 9/12/14
 * Time: 11:56 AM
 *
 * Version of the free trial content in spanish
 *
 */ ?><div id="free-trial"c lass="left leftcol">

    <h2 class="signup_tagline">¡14 Dias de Prueba Gratis!</h2>

    <h3 class="signup_tagline_2 lavugreen">
        Registra tu cuenta de Lavu iPad TPV
    </h3>
    <p>
        Tan fácil que lo puedes hacer tu mismo y siempre estamos disponibles para
        ayudarte. Puedes cancelar tu cuenta con o sin asistencia - Y sin obligaciones.
    </p>
    <p>
        <strong>Sin obligaciones, sin presión, sin preocupaciones.</strong><br>
        Aprovecha nuestro periodo de prueba y dante cuenta que Lavu es la mejor opción para tu negocio.
        Nuestros especialistas eston listos para atender cualquier pregunta.
    </p>

</div>