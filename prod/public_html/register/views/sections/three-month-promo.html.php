<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 11/3/14
 * Time: 1:46 PM
 */ ?><div id="free-trial" class="left leftcol 3mo8">
    <style>
        .3mo8 signup_tagline{
            margin-bottom:0;
        }

        .3mo8 signup_tagline_2{
              margin-top:0;
          }
    </style>

    <h2 class="signup_tagline" style="font-size:44px;">First 3 Months for $8/mo!</h2>
    <h3 class="signup_tagline_2 lavugreen">
        Full featured point-of-sale, no upfront fees
    </h3>
    <p>
        Pay an introductory price of only $8 per month for Lavu hosting, then enjoy
    the benefits of Lavu for the affordable rate of only $88 a month, which includes
        our renowned 24/7 software support, free updates and no obligation or contracts.
    </p>
    <p>
        Cancel anytime without penalty and upgrade your package with ease.
    </p>
    <p>
        Sign up and get started today!
    </p>
</div>