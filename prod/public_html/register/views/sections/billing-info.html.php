<?php
/**
 * Shortcut function for conditional priting since we don't have shorthand priting
 * in the admin server
 */
function oldval( $varkey ) {
    echo ( !empty( $_REQUEST[$varkey] ) ) ? $_REQUEST[$varkey] : "";
}


$fullname = !empty( $_REQUEST['full_name'] )? $_REQUEST['full_name'] : "";
$first = !empty( $_REQUEST['firstname'] )? $_REQUEST['firstname'] : "";
//$first = $_REQUEST['firstname'];
$last = !empty( $_REQUEST['lastname'] )? $_REQUEST['lastname'] : "";
//$last = $_REQUEST['last'];
if ( !empty( $fullname ) ) {
    list($f, $l) = split_name( $fullname );
    $first = !empty( $first )? $first : $f;
    $last = !empty( $last )? $last : $l;
}
$states_dropdown_arguments = array();

$states_dropdown_arguments['attributes'] = array(
    "name" => "state",
    "id" => "us-states-dropdown",
    "class" => "required"
);
$us_states = !empty( $_REQUEST['country'] );
$us_states = $us_states && $_REQUEST['country'] === "US";
if (  $us_states && !empty( $_REQUEST['state'] ) ) {
    $states_dropdown_arguments['default'] = $_REQUEST['state'];
}
$states_dropdown = partial("sections/us-states-dropdown.html.php", $states_dropdown_arguments);
?>
<script type="text/template" id="states-dropdown-template">
<?= $states_dropdown ?>
</script>
<div class="form_container" id="billing-info">
    <h3>Billing Information</h3>
    <ul>

        <li class="fieldsmall fieldleft">
            <label for="firstname">First Name *</label>
            <input id="firstname" class="inputsmall required" name="firstname"
            size="30" type="text" placeholder=" FIRST NAME *"
            value="<?php echo $first; ?>"/>
        </li>
        <li class="fieldsmall fieldright">
            <label for="lastname">Last Name *</label>
            <input id="lastname" class="inputsmall required" name="lastname"
            size="30" type="text" placeholder=" LAST NAME *"
            value="<?php echo $last; ?>" />
        </li>
        <li class="fieldsmall fieldleft">
            <label for="country">Country *</label>
            <?= partial('sections/countries-dropdown.html.php', array(
                "default" => empty($_REQUEST['country'])? "US" : $_REQUEST['country'],
                "attributes" => array(
                    "name" => "country",
                    "id" => "country-dropdown",
                    "class" => "required"
                )
            )) ?>
        </li>
        <li class="fieldsmall fieldright">
            <label for="state">State *</label>
            <?php if ( empty($_REQUEST['country']) || $us_states ) : echo $states_dropdown; else : ?>
        <input name="state" class="inputsmall required" type="text" id="state-no-us"
               value="<?= !empty( $_REQUEST['state'] ) ? $_REQUEST['state'] : "" ?>">
    <?php endif; ?>
        </li>
        <li class="fieldsmall fieldleft">
            <label for="city">City *</label>
            <input id="city" class="inputsmall required" name="city"
            size="30" type="text" placeholder=" CITY *"
            value="<?php oldval( 'city' ); ?>" />
        </li>

        <li class="fieldsmall fieldright">
            <label for="zip">Postal Code *</label>
            <input id="zip" class="inputsmall required" name="zip"
                   size="30" type="text" placeholder=" ZIP CODE *"
                   value="<?php if (!$bluestar) oldval("zip"); ?>" />
        </li>

        <li>
            <label for="address">Address*</label>
            <input id="address"  class="required" name="address"
            size="30" type="text" placeholder=" ADDRESS *"
            value="<?php oldval( "address" ); ?>" />
        </li>


        <li class="fieldsmall fieldleft">
            <label for="promo">Promo Code</label>
            <input id="promo" class="inputsmall" name="promo"
            size="30" type="text" placeholder=" PROMO CODE"
            <?= $bluestar ? "readonly" : "" ?>
            value="<?php oldval( "promo" ); ?>" />
        </li>

    </ul>
</div>

<?php
require_once "{$_SERVER['DOCUMENT_ROOT']}/check_ban_ip.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/recaptcha/recaptchalib.php";
$public_key = CAPTCHA_PUBLIC;
echo str_replace("http", "https", recaptcha_get_html($public_key));

