<?php
$level_translations = array( 
    "Lavu88" => "Lavu 88",
    "Silver" => "Silver",
    "Silver2" => "Silver",
    "Silver3" => "Silver",
    "Gold" => "Gold",
    "Gold2" => "Gold",
    "Gold3" => "Gold",
    "Platinum" => "Pro",
    "Platinum2" => "Pro",
    "Platinum3" => "Pro",
);
?>
<div class="left leftcol" id="bluestar-content">
    <p class="step-counter">Step 2 of 2 |
        <span class="upper"><?= $level_translations[$level] ?> account</span>
    </p>
    <h2 class="signup_tagline lavugreen">Set Up Your Lavu Subscription</h2>
    <div id="verify-top">
        <h3 class="signup_tagline_2">Lavu is billed monthly. Cancel anytime.</h3>
        <p>
            Please enter yout billing information now to ensure no interuption
            in service pnce your billing period begins.
        </p>
    </div>
    
    <div id="verify-bottom">
        <!-- <p>
            Would you like to continue right now and get your Free Trial started? It is 
            very easy to do yourself and we are always available to help. You can cancel
            anytime without our assistance - no obligations.
        </p> -->
        <div class="first">
        <!-- <h3 >Authorization</h3>  (arial 30 #494949) -->
            <p>
                <em>To verify a valid credit card, your bank will temporarily
                show an authorization for validation purposes only. </em>
            </p>
        </div>
        <div class="second">  
            <h2 id="bluestar-content__license-level">Your Lavu License Level</h2>
            <h3>Your License Level includes support for the following:</h3> 
<?php switch( $level ) {
    case "Lavu88":
    ?>
            <div class="pricing__column" id="pricing-88">
                <p class="center">
                    <img src="/images/88_icon.png" alt="Lavu 88">
                </p>
                <h4>$88 Monthly</h4>
                <div class="center"><ul>
                    <li>1 iPad Terminal</li>
                    <li>1 iPod/iPhone Device</li>
                    <li>2 Printers</li>
                    <li>1 Cash Drawer</li>
                </ul></div>
            </div>
       <?php
        break;
    case "Silver":
    case "Silver2":
    case "Silver3":
    ?>
            <div class="pricing__column" id="pricing-silver">
                <p class="center">
                    <img src="/images/silver_icon.png" alt="">
                </p>
                <h4>$39 Monthly</h4>
                <div class="center"><ul>
                    <li>1 iPad Terminal</li>
                    <li>1 iPod/iPhone Device</li>
                    <li>2 Printers</li>
                    <li>1 Cash Drawer</li>
                </ul></div>
            </div>
    <?php
        break;
    case "Gold":
    case "Gold2":
    case "Gold3":
    ?>
            <div class="pricing__column" id="pricing-gold">
                <p class="center">
                    <img src="/images/gold_icon.png" alt="">
                </p>
                <h4>$79 Monthly</h4>
                <div class="center"><ul>
                    <li>2 iPad Terminals</li>
                    <li>10 iPods/iPhones Devices</li>
                    <li>5 Printers</li>
                    <li>2 Cash Drawers</li>
                </ul></div>
            </div>
       <?php
        break;
    case "Platinum":
    case "Platinum2":
    case "Platinum3":
    ?>
            <div class="pricing__column" id="pricing-pro">
                <p class="center">
                    <img src="/images/pro_icon.png" alt="">
                </p>
                <h4>$149 Monthly</h4>
                <div class="center"><ul>
                    <li>3 iPad Terminals**</li>
                    <li>15 iPods/iPhones</li>
                    <li>Unlimited Printers</li>
                    <li>Unlimited Cash Drawers</li>
                    <li>
                        **Up to 20 additional terminals
                        <br><strong>$20 / Month each</strong>
                    </li>
                </ul></div>
            </div>
       <?php
        break;
    default:
?>
            <p><strong>You will not be billed unless you continue to use Lavu 
            and keep your account open past the 14 day Free Trial.</strong></p>
            
            <p><em>Failure to cancel your Free Trial before the 14 day expiration
            results in a $50 USD cancelation / refund fee. Refunds result in an
            administration fee by merchant service providers and this is why we
            must enforce this policy.</em></p>  
            <h4 class="upper lavugreen">Risk Free. Give Lavu a try!</h4>

<?php } ?>
        </div>
    </div>
</div>