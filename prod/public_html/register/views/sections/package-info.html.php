<?php
if ( strtolower( $level )  === "lavu88" ) {?>
<div class="form_container" id="myplan">
    <p class="upper"><small>Step 1 of 2</small></p>
    <!-- <h3>Your Lavu Plan: <span class="highlighted">Lavu Pro</span></h3> -->
     <h3><?php
     echo $locale === "es"? "Su Plan" : ( $locale === "cn" ? "你的计划" :"Your Lavu Plan"); 
     ?>: <img src="/images/lavu88-logo-wide.png" style="vertical-align:-15px"
     alt="LAVU 88" width="200" /></h3>
    <input type="hidden" name="level" value="lavu88" />
</div>
<?php } else { ?>
<div class="form_container" id="myplan">
        <p class="capitals">Step 1 of 2</p>
        <!-- <h3>Your Lavu Plan: <span class="highlighted">Lavu <?php // echo $o_package_container->get_printed_name_by_attribute('name', $_REQUEST['level']); ?></span></h3> -->
         <h3><?php
echo $locale === "es"? "Su Plan" :
( $locale === "cn" ? "你的计划" : "Your Lavu Plan");
foreach ( array( "Silver", "Gold", "Pro" )  as $l ) {
    $classes = "level caps";
    echo "<!-- \$level=$level -->";
    if ( strpos( $level, $l ) !== false || ( $l == "Pro" &&  strpos( params( 'level' ), "Platinum" ) !== false ) ) {
        $classes .= " selected";
    }
    echo "<span class='$classes'> <span class='dot'> </span> <br /> $l</span>";
}
?>
             <!-- <span class="level capitals"> <span class="dot"> </span></br> Silver</span>
             <span class="level capitals"> <span class="dot"> </span> </br> Gold</span>
             <span class="level capitals selected"> <span class="dot"> </span> </br> Pro</span></h3> -->
        <!--<a href="http://poslavu.com/signup">Change Plan</a> -->
</h3></div>
<?php } ?>