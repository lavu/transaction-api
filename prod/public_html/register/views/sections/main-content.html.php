<div class="right-column clearfix">
        <h1 class="center" id="sticker">
            <img src="/images/lavu_88_circle.png" alt="LAVU 88" />
        </h1>
        <p id="trial" class="center lavugreen upper big">14 Day free trial</p>
        <p class="center" id="no-license">
            No license fees. No contracts. Cancel any time.
        </p>
    </div>
<div class="left-column">
    <p id="ipad"><img src="/images/lavu_ipad_88.png" alt="" /></p>
    <h2 id="title" class="big lightgray">
            What is <img src="/images/lavu_88_logo.png" alt="LAVU 88" />?
        </h2>
        <p>
            Now more affordable than ever, Lavu 88 is the new standard
            for single terminal iPad POS. With no upfront license fees,
            Lavu 88 serves up the finest in robust feature sets for hospitality
            businesses at a fraction of the cost of legacy systems.
        </p>
        <h3 id="simple" class="center">
            <span>It's simple: <strong class="lavugreen">$88 per month</strong></span>
        </h3>
  <div id="supported" class="wrapper">
    <p class="lavugreen">
        <strong>What is supported</strong>
    </p>
    <ul class="lightgray">
        <li>1 Terminal (iPad)</li>
        <li>1 iPod/iPhone</li>
        <li>2 Printers</li>
        <li>1 Cash Drawer</li>
        <li>Unlimited Users</li>
    </ul>
    <p>* Hardware not included</p>
</div>
<p class="lightgray wrapper">
    <img src="/images/lavu_88_logo.png" alt="LAVU 88" />
    <br /><strong>Feature rich yet simple to use.</strong>
    <br />Lavu POS is designed specifically for hospitality businesses.
    We are in constant development, adding new features to simplify your
    business and ensure success.
</p>
<p class="upper wrapper center" id="learn-features">
    Learn more about our features
    <img src="/images/tour_grey_uparrow.png" alt="" />
</p>

<table id="feature_table" class="wrapper">
 <tbody><tr class="lgprow1">
        <td class="col1knone"></td>
        <td class="col2k"></td>
        <td class="col03k0"></td>
    </tr><!--end row 1-->

    <tr class="notwhite">
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Switch between formats depending on your service types </span> </a></td>
        <td class="col2k">Table, Tab, Quick Serve layouts</td>
        <td class="chart_check"></td>
    </tr><!--end row 2-->

    <tr class="notwhite">
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Assign a printer from the app for guest receipts and/or order tickets </span> </a></td>
        <td class="col2k">Printers</td>
        <td class="chart_check"></td>
    </tr><!--end row 3-->

    <tr class="notwhite">
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Easily track employees time card punches </span> </a></td>
        <td class="col2k">Time Cards</td>
        <td class="chart_check"></td>
    </tr><!--end row 4-->

    <tr class="notwhite">
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Track money in/out of the register for non-sale transactions. Ex: Petty Cash, Pay Out the band, etc. </span> </a></td>
        <td class="col2k">Paid in / Paid out</td>
        <td class="chart_check"></td>
    </tr><!--end row 5-->

    <tr class="notwhite">
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Allow employees to Clock In/Out on the iPad </span> </a></td>
        <td class="col2k">Clock in / Clock out</td>
        <td class="chart_check"></td>
    </tr><!--end row 6-->

    <tr class="notwhite">
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Create custom discounts and apply to items or checks </span> </a></td>
        <td class="col2k">Custom discounts</td>
        <td class="chart_check"></td>
    </tr><!--end row 7-->

    <tr class="notwhite">
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Add notes to an order </span> </a></td>
        <td class="col2k">Order Notes</td>
        <td class="chart_check"></td>
    </tr><!--end row 8-->

    <tr class="notwhite">
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Customer is forced to choose a pre-defined menu item modification or selection. Ex: Soup or Salad </span> </a></td>
        <td class="col2k">Forced Modifiers</td>
        <td class="chart_check"></td>
    </tr><!--end row 9-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Customer may optionally modify items. Ex: Extra Mustard </span> </a></td>
        <td class="col2k">Optional Modifiers</td>
        <td class="chart_check"></td>
    </tr><!--end row 10-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Connect multiple choice type Forced Modifiers when one option is always followed by another. Ex. Salad &gt; Salad Dressing </span> </a></td>
        <td class="col2k">Detours</td>
        <td class="chart_check"></td>
    </tr><!--end row 11-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Associate ingredients with items. Those ingredients are automatically deducted from your inventory </span> </a></td>
        <td class="col2k">Ingredients</td>
        <td class="chart_check"></td>        
    </tr><!--end row 12-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Track inventory stock and costs </span> </a></td>
        <td class="col2k">Inventory</td>
        <td class="chart_check"></td>
    </tr><!--end row 13-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Enter tip amounts before settling the credit card batch </span> </a></td>
        <td class="col2k">Tip Adjustments</td>
        <td class="chart_check"></td>
    </tr><!--end row 14-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Split an order into multiple checks for divided payment. </span> </a></td>
        <td class="col2k">Split Checks</td>
        <td class="chart_check"></td>
    </tr><!--end row 15-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Easily access groups of Menu Categories </span> </a></td>
        <td class="col2k">Menu Groups</td>
        <td class="chart_check"></td>
    </tr><!--end row 16-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Makes it a cinch to select seat number and course number at the table </span> </a></td>
        <td class="col2k">Seat / Course</td>
        <td class="chart_check"></td>
    </tr><!--end row 17-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Track total Guest Count for selected time periods </span> </a></td>
        <td class="col2k">Guest Count</td>
        <td class="chart_check"></td>
    </tr><!--end row 18-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"><img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Create custom payment types and apply to order payments </span> </a></td>
        <td class="col2k">Custom Payment Types</td>
        <td class="chart_check"></td>
    </tr><!--end row 20-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"><img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Multiple orders combine while maintaining separate checks </span> </a></td>
        <td class="col2k">Merge Orders</td>
        <td class="chart_check"></td>
    </tr><!--end row 21-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"><img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Live support and and ticket system 24-7 </span></a></td>
        <td class="col2k">Phone Support for Software</td>
        <td class="chart_check"></td>
    </tr><!--end row 21-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Happy Hour </span> </a></td>
        <td class="col2k">Happy Hour</td>
        <td class="chart_check"></td>
    </tr><!--end row 21-->

    <tr>
        <td class="col1k"><a class="tooltip" href="#"> <img src="http://try.poslavu.com/images/icirc2.png" width="16" height="16" alt=""> <span class="classic border_shadow" style="border-top-left-radius: 10px; border-top-right-radius: 10px; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;"> Cash Reconciliation </span> </a></td>
        <td class="col2k">Cash Reconciliation</td>
        <td class="chart_check"></td>
    </tr><!--end row 21-->
</tbody></table>
</div>