var rollback_form = document.forms[0];
<?php if ( !empty( $rowid ) ) : ?>
var row_id_input = document.createElement('input');
row_id_input.type="hidden";
row_id_input.name="rowid";
row_id_input.value=<?= $rowid ?>;
rollback_form.appendChild(row_id_input);
<?php endif; ?>
<?php if ( !empty($_SESSION['error']) ) :?>
var row_id_input = document.createElement('input');
row_id_input.type="hidden";
row_id_input.name="rollback_error";
row_id_input.value="<?= $_SESSION['error']['h2'] . '-' . $_SESSION['error']['p'] ?>";
rollback_form.appendChild(row_id_input);
<?php endif; ?>
document.forms[0].submit();