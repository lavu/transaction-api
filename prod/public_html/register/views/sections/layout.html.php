<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="initial-scale=1">
<link rel="stylesheet" href="/styles/lavu88.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/styles/lavu88_fullstyle.css" type="text/css"
media="screen and (min-width:768px)" />
<link rel="stylesheet" href="/styles/lavu88_bigscreen.css" type="text/css"
media="screen and (min-width:990px)" />
<!--[if lt IE 9]>
<link rel="stylesheet" href="/styles/lavu88_fullstyle.css" type="text/css"
media="screen" />
<link rel="stylesheet" href="/styles/lavu88_bigscreen.css" type="text/css"
media="screen" />
<![endif]-->
<title>Lavu 88 -Sign In Now!</title>
</head>
<body>
<?php 
$q = empty( $_GET['escape_ip_check'] ) ? "": '?' .http_build_query( $_GET );
?>
<form action="submit_signup.php<?php echo $q?>" method="post">
<input type="hidden" name="referer" id="referer"
value="<?php echo $_SERVER['REQUEST_URI']; ?>" />
<?php include DOC_ROOT ."/views/lavu88/header.html.php"; ?>
<div class="wrapper">
<?php include DOC_ROOT ."/views/lavu88/main-content.html.php"; ?>
<div class="right-column">
<?php include DOC_ROOT ."/views/lavu88/company-info.html.php"; ?>
<?php include DOC_ROOT ."/views/lavu88/starter-menu.html.php"; ?>
<?php include DOC_ROOT ."/views/lavu88/verify-short.html.php"; ?>
<?php include DOC_ROOT ."/views/lavu88/credit-card-info.html.php"; ?>
<?php include DOC_ROOT ."/views/lavu88/billing-info.html.php"; ?>
<?php include DOC_ROOT ."/views/lavu88/submit.html.php"; ?>
</div>
</div>
</form>
<?php if ( !DEV ) { ?>
<script type="text/javascript"
src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<?php } ?>
<script type="text/javascript">window.jQuery || document.write('<sc' +
'ript src="/scripts/jquery-1.9.0.js">\x3C/script>')
</script>
<?php if ( !DEV ) { ?>
<script type="text/javascript"
src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<?php } ?>
<script type="text/javascript">jQuery.validate || document.write('<scr' +
'ipt src="/scripts/jquery.validate.js">\x3C/script>')</script>
<script type="text/javascript" src="/scripts/lavu88-app.js"></script>
</body>
</html>