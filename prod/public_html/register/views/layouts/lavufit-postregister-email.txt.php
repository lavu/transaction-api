<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 9/8/14
 * Time: 11:02 AM
 */
$username = empty( $username )? "username" : $username;
$password = empty( $password )? "password" : $password;
$color = DEV?  "#faa" :"#AECD37";
?>
Contact Us: 855-767-5288 | sales@lavuinc.com | pos.support@lavuinc.com

Lavu Fit | iPad Point of Sale for Gyms

Welcome to Lavu.
Your new account is ready

User: <?= $username . "\n" ?>
Pass: <?= $password . "\n" ?>

Lavu Default PIN:1234.
Use this pin until you have setup your custom PIN

Try Lavu on your iPad
You can now access your Lavu Front of House

Use the Lavu Front of House where you interact with your members.
Enter your PIN and you will see the Lavu FoH member management, as
well as other gym specific features.

* Lavu Back Office CP *
Setup your business now.

Use your Lavu Fit Back of House CP.
The Lavu CP is your Control Panel. This is where you will adjust account
settings, customize the member signup process, and view your sales reports.

To login to your New Lavu Account : https://admin.poslavu.com

* Certified Lavu Distributor *
How can we assist you?

You will soon be contacted by a Lavu Specialist from your area.
Lavu Specialists are trained to assist with onsite installations,
account programming, training and support

Want to learn more about Lavu Fit? Here are some resources to
help you get setup:

- Lavu online help center
  You can access The Lavu Help Center at:
  http://help.lavu.com
- Lavu user forums
  Our online forums have been very helpful to many of our customers.
  http://www.lavucommunity.com
- Speak with us now
  Would you like to speak with someone right now?
  855-767-5288

* Lavu iPad POS Free Trial *
About the Free Trial

Accounts are automatically charged after the 14 day trial period.
If you are not yet ready to purchase and move forward with Lavu, please
cancel and disable your account BEFORE
your trial period has expired.

** If you want to cancel your account, here is how:

1. Login the to the administrative control panel.
    https://admin.poslavu.com
2. Click the "My Accounts"; button on the top right of your screen.
3. Scroll to the bottom of the "My Account" page and click the
   "Cancel All Billing & Disable Account" button.
4. Please provide a reason for your cancellation. This is not required but we
   do appreciate your feedback. When complete click the "Continue Cancellation
   and Disable your Account" button.
5. Your Account is now disabled. If you wish to reactivate your accounts,
   please contact Support or your Lavu Specialist.

****************************************************************************
** Failure to cancel your Free Trial before the 14 day expiration results **
** in any applicable License or Monthly Fees being charged. Once charged, **
** License and Monthly Fees are non-refundable.                           **
****************************************************************************

If you are not yet ready to purchase and move forward with Lavu, please cancel
and disable your account before your trial period has expired:

Please call us at TLK-POS-LAVU (855-767-5288) or for International customers
480-788-LAVU for further information or questions.