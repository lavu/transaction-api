<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 8/29/14
 * Time: 5:24 PM
 */
//include_once('translate.php');
echo partial("sections/approved-header.html.php");
?>
<!--[if lte IE 8]><![endif]-->
    <div class="banner_background">
        <div class="image"></div>
    </div>

    <div id="app_content_wrapper">
        <div id="app_content_top">

            <div class="center">
                <h1>
                    <b>Welcome</b> to <b>Lavu</b>
                </h1>

                <?php if ( empty($source) ) : ?>
                <h2>
                    Your <b>FREE TRIAL</b> has begun!
                </h2>
                <?php else: ?>
                    <h2> &nbsp; </h2>
                 <?php endif; ?>
            </div>

            <div class="lavuaccountinfo clearfix">
                <div class="clearfix lavuaccountinfo__username">
                    <div class="spacer">
                        <img src="/images/lavu-logo.png" alt="LAVU"/>
                    </div>
                    <div class="info">
                        <div class="username_password"><span>USER NAME:</span> <?= $username ?></div>
                    </div>
                </div>
                <div class="approved__new-password">

                    <div class='update_password_box'>
                        <div class="caps">
                            <b>Create a new Password</b>
                        </div>
                        <div class="update_password_box__input-placeholder">
                            <label class="caps center">
                                <small>New Password</small>
                            </label>
                            <div class="approved__input-container">
                                <input type='password' name='password1' class='password1'/>
                            </div>
                        </div>
                        <div class="update_password_box__input-placeholder">
                            <label class="caps center">
                                <small>Confirm Password</small>
                            </label>
                            <div class="approved__input-container">
                                <input type='password' name='password2' class='password2'/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="<?= LAVUADMIN ?>/cp/?mode=assistance_guide_start" method='post' class="center" id="approved__login-form" data-rowid="<?= $rowid ?>">
            <input type='hidden' value='<?= $username ?>' class='new_signup_username' name="username"/>
            <input type='hidden' value='<?= $password ?>' class='signup_pass_hidden' name='password'/>
            <button class="approved__login-button">
                LOG IN <img src="/images/continue_arrow.png" alt=""/>
            </button>
        </form>
    </div>
    <div id="app_content_bottom">

        <div class="wrapper">
            <div class="clearfix">
                    <div class="app_content_bottom__column">

                        <p class="center">
                            <img src="/images/sign_up_mail_icon.png" width="30" alt=""/>
                        </p>

                        <p class="header">Save Your Login Info</p>

                        <p>An email has been sent to you</p>
                        <a class="dark_link" href="#" onclick="resend_email();">Resend Login Info Email</a>

                        <div class="check_thin_grey"></div>
                    </div>
                    <div class="app_content_bottom__column">

                        <p class="center">
                            <img src="/images/lavu_app_icon.png" alt="Lavu App"/>
                        </p>

                        <p class="header">Download Lavu App</p>

                        <p>Try the Front of House on iPad</p>
                        <a class="green_link" href="http://itunes.apple.com/us/app/poslavu-client/id391800399?mt=8"
                           target="_blank">Go to the Apple App Store</a>

                        <div class="check_thin_grey transparent"></div>
                    </div>
                    <div class="app_content_bottom__column">
                        <p class="center">
                            <img src="/images/log_in_icon.png" width="30" alt=""/>
                        </p>

                        <p class="header">Use Your Account</p>

                        <p>Login to access the Quick Start Guide</p>
                        <a class="login_link" href="<?= LAVUADMIN ?>/cp/?mode=assistance_guide_start" target="_blank">
                            LOG IN
                        </a>

                        <div> <!--target="_blank" -->

                        </div>
                    </div>
                </div>

                <div class="page-bottom-text">
                    <div class="text-container">
                        <span class="light">You can bookmark this page for future reference.</span>
                        <span class="dark">Questions about your Lavu account? 855-767-5288</span>
                    </div>
                </div>
        </div>
    </div>
    <script>
        var lavuCredentials = {
            username: "<?= $username ?>",
            password: "<?= $password ?>"
        }
    </script>
<?= partial("sections/footer.html.php") ?>