<?php
$params = array(
    "locale" => $locale,
    "level" => $level
);
echo partial("sections/header.html.php", $params);
?>
<form class="step2" action="<?= url_for("submit")?>" method="post">
<input type="hidden" name="rollback_url" value="<?=  $_SERVER['REQUEST_URI'] ?>" />
<?php if ( empty( $incoming_data['locale'] ) ) : ?>
<input type="hidden" name="locale" value="<?php echo $locale; ?>" >
<?php endif;
if ( empty( $incoming_data['level'] ) ) : ?>
<input type="hidden" name="level" value="<?= $level ?>" id="post_level">
<?php endif;
foreach ($incoming_data as $k=>$v) : ?>
<input type="hidden" name="<?= $k ?>" value="<?= $v ?>" id="<?= $k ?>" />
<?php endforeach; ?>
<div class="wrapper">
<div class="left-column">
<?= $content ?>
</div>
<div class="right-column">
<?= partial( "sections/errors.html.php", $params ) ?>
<?= partial( "sections/credit-card-info.html.php", $params ) ?>
<?= partial( "sections/billing-info.html.php", $params ) ?>
<?= partial( "sections/submit.html.php", $params ) ?>
</div>
</div>
</form>
<?= partial("sections/footer.html.php") ?>