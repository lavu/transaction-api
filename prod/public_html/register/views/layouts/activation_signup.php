<?php
echo partial("sections/approved-header.html.php");
?>

<!--[if lte IE 8]><![endif]-->
    <div class="banner_background">
        <div class="image"></div>
    </div>

    <div id="app_content_wrapper">
        <div id="app_content_top">

            <div class="center">
                <h1>
                    <b>Welcome</b> to <b>Lavu</b>
                </h1>

                <?php if ( empty($source) ) : ?>
                <h2>
                    Activate your <strong style="color:#9ac42d">FREE</strong> trial account now
                </h2>
                <?php else: ?>
                    <h2> &nbsp; </h2>
                 <?php endif; ?>
            </div>

            <div class="lavuaccountinfo clearfix">
                <div class="clearfix lavuaccountinfo__username">
                    <div class="spacer">
                        <img src="/images/lavu-logo.png" alt="LAVU"/>
                    </div>
                </div>
                <div class="approved__new-password">

                    <div class='update_password_box'>
                        <div style="font-size:16pt; letter-spacing:2px; line-height:40px">
                            Click the activation link in the email sent to <p><?= $email ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form action="<?= LAVUADMIN ?>/cp/?mode=assistance_guide_start" method='post' class="center" id="approved__login-form" data-rowid="<?= $rowid ?>">
            <input type='hidden' value='<?= $username ?>' class='new_signup_username' name="username"/>
            <input type='hidden' value='<?= $password ?>' class='signup_pass_hidden' name='password'/>
        </form>
    </div>
    <div id="app_content_bottom">

        <div class="wrapper">
            <div class="clearfix" style="text-align:center">
                    <div class="app_content_bottom__column">

                        <p class="center">
                            <a class="green_link" href="http://itunes.apple.com/us/app/poslavu-client/id391800399?mt=8"
                           target="_blank"><img src="/images/lavu_app_icon2.png" alt="Lavu App"/></a>
                        </p>

                        <p class="header">Download Lavu POS App</p>

                        <p>Try the Front of House on iPad</p>
                        <a class="green_link" href="http://itunes.apple.com/us/app/poslavu-client/id391800399?mt=8"
                           target="_blank">Go to the Apple App Store</a>

                        <div class="check_thin_grey transparent"></div>
                    </div>
                </div>

                <div class="page-bottom-text">
                    <div class="text-container">
                        <span class="light"></span>
                        <span class="dark">Questions about your Lavu account? 855-767-5288</span>
                    </div>
                </div>
        </div>
    </div>
    <script>
        var lavuCredentials = {
            username: "<?= $username ?>",
            password: "<?= $password ?>"
        }
    </script>

    <!-- Google Code for Free Lavu PPC Conversion Page -->
    <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 972516368;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "kgkRCMuAzFwQkNjdzwM";
    var google_remarketing_only = false;
    /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <!-- Yieldmo Conversion Script "lead for Lavu" -->
    <script>
      (function(){
        var __yms, __p;
           __p = document.body || document.head;
           __yms = document.createElement('script');
           __yms.setAttribute('type','text/javascript');
           __yms.setAttribute('async', '');
           __yms.setAttribute('src', '//static.yieldmo.com/ym.adv.min.js');
           __yms.setAttribute('class', 'ym');
           __yms.setAttribute('data-ymadvid', '1175170810422606724');
           __yms.setAttribute('data-conversion-type', 'lead');
           if(__p) __p.appendChild(__yms);
         })();
    </script>
    <noscript>
    <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/972516368/?label=kgkRCMuAzFwQkNjdzwM&amp;guid=ON&amp;script=0"/>
    </div>
    </noscript>

<?= partial("sections/footer.html.php") ?>