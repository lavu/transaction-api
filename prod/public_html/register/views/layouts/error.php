<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="initial-scale=1">
    <link rel="stylesheet" href="/styles/lavu88.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="/styles/lavu88_fullstyle.css" type="text/css"
          media="screen and (min-width:768px)" />
    <link rel="stylesheet" href="/styles/lavu88_bigscreen.css" type="text/css"
          media="screen and (min-width:990px)" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="/styles/lavu88_fullstyle.css" type="text/css"
          media="screen" />
    <link rel="stylesheet" href="/styles/lavu88_bigscreen.css" type="text/css"
          media="screen" />
    <![endif]-->
    <title>Lavu - Error</title>
</head>
<body class="error">
    <?php echo partial( "sections/header.html.php" ); ?>
    <div class="wrapper">
        <?= $content ?>
    </div>
</body>
</html>
