<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 10/7/14
 * Time: 4:51 PM
 */ ?><!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Lavu iPad POS - Invalid Credentials</title>
</head>
<body>
<style>body{ background-color:#eee; } div{max-width: 500px; background-color: white; border: 2px solid #666; border-radius: 5px; margin: 100px auto; text-align: center; padding: 15px;}</style>
<div>
    <?= $content ?>
</div>
<?php if ( DEV && !empty( $dev ) ) : ?>
<script type="text/javascript">
if ( typeof console !== "undefined" ) {
    console.log("<?= $dev ?>")
}
</script>
<?php endif; ?>
</body>
</html>