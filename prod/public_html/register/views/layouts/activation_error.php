<?php
echo partial("sections/approved-header.html.php");
?>

<!--[if lte IE 8]><![endif]-->
    <div class="banner_background">
        <div class="image"></div>
    </div>

    <div id="app_content_wrapper">
        <div id="app_content_top">

            <div class="center">
                <h1>
                    <b>Welcome</b> to <b>Lavu</b>
                </h1>

                <?php if ( empty($source) ) : ?>
                <h2>
                    <strong style="color:#9ac42d">ERROR</strong>
                </h2>
                <?php else: ?>
                    <h2> &nbsp; </h2>
                 <?php endif; ?>
            </div>

            <div class="lavuaccountinfo clearfix">
                <div class="clearfix lavuaccountinfo__username">
                    <div class="spacer">
                        <img src="/images/lavu-logo.png" alt="LAVU"/>
                    </div>
                </div>
                <div class="approved__new-password">

                    <div class='update_password_box'>
                        <div style="font-size:16pt; letter-spacing:2px; line-height:40px">
                            <?= $content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="app_content_bottom">

        <div class="wrapper">
            <div class="clearfix" style="text-align:center">
                <div class="page-bottom-text">
                    <div class="text-container">
                        <span class="light"></span>
                        <span class="dark">Questions about your Lavu account? 855-767-5288</span>
                    </div>
                </div>
        </div>
    </div>
<?= partial("sections/footer.html.php") ?>