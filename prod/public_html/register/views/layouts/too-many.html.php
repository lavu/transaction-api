<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 8/26/14
 * Time: 11:38 AM
 */ ?><!doctype html>
<html>
<head>
    <title>Lavu iPad POS - Sorry!</title>
    <style type="text/css">
        body{
            font-family:Arial, sans-serif;
        }

        body>div{
            max-width:520px;
            margin:20px auto;
            border:3px solid #CCC;
            -webkit-border-radius:20px;
            -moz-border-radius:20px;
            border-radius:20px;
            padding:5px 20px 20px 20px;
        }
        .center {
            text-align:center;
        }

        body>div>p:first-child{
            margin-bottom:2.5em;
        }
    </style>
</head>
<body>
<div>
    <p class="center"><img src="/images/lavu-logo.png" /></p>
    <p>Sorry, we are unable to create more accounts for you at this time. Please contact us (855-767-5288) if you would like additional accounts.</p>
    <?php if ( DEV  || strpos( $_SERVER['REMOTE_ADDR'], "216.243.114" ) !== false  || strpos( $_SERVER['REMOTE_ADDR'], "192.168" ) === 0 ) : ?>
        <form method="post" action='<?= url_for( $locale, $level, "step2" )?>'>
            <input type='hidden' name="escape_ip_check" value='1' />
            <?php foreach($_REQUEST as $k=>$v) :?>
                <?php if ( is_array($v) ) :
                    foreach ( $v as $vk => $vv ) : ?>
                        <input type="hidden" name="<?= $k ?>[]" value="<?= $vv ?>"/>
                        <?php endforeach;
                else :?>
                    <input type='hidden' name='<?= $k ?>' value='<?= $v ?>' />
                <?php endif; ?>
            <?php endforeach; ?>
            <p class='center'>
                <input type='submit' value='Escape IP Check' />
            </p>
        </form>
    <?php endif; ?>
</div>
</body>
</html>