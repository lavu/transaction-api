<?php
$get = $_GET;
unset( $get['uri'] );
unset( $get['level'] );
unset( $get['product'] );
unset( $get['locale'] );
unset( $get['location'] );
$q = empty( $get['escape_ip_check'] ) ? "": '?' .http_build_query( $get );
$params = array(
    "locale" => $locale,
    "level" => $level,
    "product" => $product
);
echo partial("sections/header.html.php", $params);
?>
<form class="step1" action="<?php echo url_for( $locale, $level, "step2" ) . "/$q";?>" method="post">
<input type="hidden" name="session_id" value="<?= session_id() ?>" id="session_id"/>
<input type="hidden" name="level" value="<?= $level ?>" />
<?php if ( !empty( $source ) ) :?>
    <input type="hidden" name="source" value="<?= $source ?>"/>
<?php endif;
if ( !empty($tracking) ):
    foreach( $tracking as $k=>$v ) : ?>
<input type="hidden" name="<?= $k ?>" value="<?= $v ?>"/>
<?php endforeach;
endif; ?>
<div class="wrapper">
<div class="left-column">

<?php
if ( !empty( $content ) ) { echo $content; }
echo partial( "sections/company-info.html.php", $params );
?>
</div>
<div class="right-column">
<?php echo partial( "sections/package-info.html.php", $params ); ?>
<?php echo partial( "sections/starter-menu.html.php", $params );
$continue = $locale === "cn" ? "继续 ›" : ( $locale === "es" ? "Continuar ›" : "Continue ›")
?>
</div>
</div>
<div style="text-align: right;" class="wrapper">
    <input class="app-submit-button new-colors upper float-right" id="subscription_submit"
           name="finish_signup" type="submit" value="<?php echo $continue;?>"
           <?php if ( !DEV ) : ?>onclick="ga('send', 'event', 'Lead Progress', 'lavu88 Continue to Step 2');ga('originalPOSlavu.send', 'event', 'Lead Progress', 'lavu88 Continue to Step 2');"<?php endif; ?> />
</div>
</form>
<?= partial("sections/footer.html.php") ?>