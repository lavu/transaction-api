<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php
if (getenv('DEV') == '1') {
	@define('DEV', 1);
	ini_set("display_errors","1");
	error_reporting(error_reporting() ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
} else {
	@define('DEV', 0);
}

$b = ( $_SERVER['HTTP_HOST'] == 'register.poslavu.com' )? 'admin' : 'LavuBackend';
if ( HTHOST === "register.poslavu.com" ) {
    require_once "../$b/v2/objects/json_encode/jsonwrapper.php";
}
function reqval( $val ) {
    echo !empty( $_REQUEST[$val] )? $_REQUEST[$val]: "";
}
?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

  <title>POS Lavu iPad Point of Sale  | New Signup</title>
  <link rel="stylesheet" href="/styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="/styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="/styles/register_new.css" type="text/css" media="screen" title="no title" charset="utf-8">
  <!-- <script type='text/javascript'>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-30186945-1']);
			_gaq.push(['_setDomainName', 'poslavu.com']);
			_gaq.push(['_trackPageview']);
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

</script> -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30186945-2', 'auto',{'allowLinker':true,'alwaysSendReferrer' : true});
  ga('create', 'UA-30186945-1', 'auto', {'name': 'originalPOSlavu'});
  ga('require', 'displayfeatures');
  ga('originalPOSlavu.require', 'displayfeatures');
  ga('send', 'pageview');
  ga('originalPOSlavu.send', 'pageview');

   window.incoming_data = <?php echo json_encode( array_merge( $_GET, empty( $_POST )? array() : $_POST ) ); ?>;
   window.send_lead_on_exit = <?php echo empty( $GLOBALS['s_dev'] )? 'true': 'false'; ?>;
</script>
</head>

<body class="step1">
	<div class="header-wrapper">
		<header>
			<p id="header-logo" class="clearfix">
				<a class="lavu-logo" href="http://www.poslavu.com">
					<img src="/images/lavu-logo.png" alt="Lavu | Point of Sale" />
			    </a>
	            <sub class="little-text">RESTAURANT POS FOR iPAD</sub>
	        </p>
			<span id="header-phone">1-855-767-5288</span>
	    </header>
	</div>
  <div id="app_content_wrapper">
    <div id="app_content" class="clearfix">
		<div class="left leftcol">
		<!-- <p class="step-counter">Step 1 of 2 |
			<span class="capitals"><?= $o_package_container->get_printed_name_by_attribute('name', $_REQUEST['level']); ?> account</span>
		</p> -->
		
		<h2 class="signup_tagline">14 Day Free Trial</h2>
		<h3 class="signup_tagline_2">Signup now for your Lavu POS account</h3>
		<p>
			It is very easy to do yourself and we are always available to help.
			You can cancel anytime with or without our assistance - no obligations.
		</p>
		<p>
			<strong>No obligations, no pressure, no worries.</strong><br>
			Use our Free Trial to make sure Lavu is the best choice for your business. 
			Our Specialists are available to answer questions.
		</p>
		
		<form action="<?php echo str_replace("step1{$s_dev}", "step2{$s_dev}", $_SERVER['PHP_SELF']).'?'. http_build_query( $_GET ); ?>" class="frm customer step1" id="new_customer" method="POST">
		<div class="form_container">
			<h3>Company Information</h3>
			<ul>
				<li>
					<label for="company_name">Company Name *</label>
					<input id="company_name" name="company_name" size="30" type="text" value="<?php reqval('company_name'); ?>" />
				</li>

				<li class="fieldsmall fieldleft">
					<label for="phone">Phone *</label>
					<input class="inputsmall" id="phone" name="phone" size="30" type="text" value="<?php reqval('phone'); ?>" />
				</li>
				<li class="fieldsmall right">
					<label for="email">Email *</label>
					<input class="inputsmall" id="email" name="email" size="30" type="text" value="<?php reqval('email'); ?>" />
				</li>

				<li class="fieldsmall fieldleft">
					<label for="zip">Postal Code *</label>
					<input class="inputsmall" id="zip" name="zip" size="30" type="text" value="<?php reqval('zip'); ?>" />
				</li>
				<li class="fieldsmall right">
					<label for="full_name">Full Name</label>
					<input class="inputsmall" id="full_name" name="full_name" size="30" type="text" value="<?php reqval('full_name'); ?>" />
				</li>
			</ul>
		</div>

<!-- <div class="tooltips"></div>  -->

 	<?php
 		$a_getvars = array();
 		foreach($_GET as $k=>$v)
 			$a_getvars[] = "$k=$v";

 		if (isset($_GET['no_lead']) && $_GET['no_lead']) {
 			?>
 			<div id="no_lead" style="position:fixed; top:5px; left:5px; background-color:rgba(0,0,0,0.7); border:1px solid white; border-radius:5px; color:white; padding:5px;">
 				No lead information will be generated for this signup.
 			</div>
 			<?php
 		}
 	?>

	
	</div>

	<input type="hidden" name="package" value="<?= $_REQUEST['level'] ?>" />
	<input type="hidden" name="level" value="<?= $_REQUEST['level'] ?>" />
	<input type="hidden" name="rowid" value="<?= isset($rowid)?$rowid:0 ?>" />
	<input type="hidden" name="locale" value="<?= !empty($_GET['location']) ?$_GET['location'] : $_GET['loc'] ?>" />
<?php
if ( !empty( $_REQUEST['r'] ) ) {
    echo <<<HTML
    <input type="hidden" name="referrer" value="{$_REQUEST['r']}" />
HTML;

}
?>
	<input type="hidden" name="no_lead" value="<?= (isset($_GET['no_lead']) ? $_GET['no_lead'] : '0') ?>" />
	<input type="hidden" name="promo" value="<?= (isset($_REQUEST['promo']) ? $_REQUEST['promo'] : '') ?>" />

<!-- <table><tr><td> -->


<!-- </td><td> -->

<div class="right" id="signupccinfo">
	<div class="form_container" id="myplan">
		<p class="capitals">Step 1 of 2</p>
		<!-- <h3>Your Lavu Plan: <span class="highlighted">Lavu <?= $o_package_container->get_printed_name_by_attribute('name', $_REQUEST['level']); ?></span></h3> -->
		 <h3>
			 Your Lavu Plan:
<?php
foreach ( array( "Silver", "Gold", "Pro" )  as $l ) {
	$classes = "level capitals";
	if ( strpos( $_REQUEST['level'], $l ) !== false || ( $l == "Pro" &&  strpos( $_GET['level'], "Platinum" ) !== false ) ) {
		$classes .= " selected";
	}
	echo "<span class='$classes'> <span class='dot'> </span> <br /> $l</span>";
}
?>
			 <!-- <span class="level capitals"> <span class="dot"> </span></br> Silver</span>
			 <span class="level capitals"> <span class="dot"> </span> </br> Gold</span>
			 <span class="level capitals selected"> <span class="dot"> </span> </br> Pro</span></h3> -->
		<!--<a href="http://poslavu.com/signup">Change Plan</a> -->
	</div>

  <!-- starter menu selections -->
  <div class="form_container">
    <h3><?= $b_is_niche ? 'Your Starter Menu' : 'Choose Starter Menu' ?></h3>
    <table width="360" border="0" cellspacing="0" cellpadding="10" id="boxlinks">

		<?php
		/***********************************************************************
		 *                    D R A W   T H E   M E N U
		 **********************************************************************/
		ob_start();
		?>
		<tr>
			<td width="180" height="140" align="center" valign="top" 
			class="1 <?php echo ( ( !empty( $_POST['mname'] ) && $_POST['mname'] =='default_restau' ) || $_GET['product'] === 'all')  ? 'highlight':'' ?>">
				<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
					<tr>
						<td width="120" align="center" valign="middle" >
							<div class="starter_menu_image">
								<img src="/images/type_3.png" width="100" height="60" />
								<div class="tooltip">This Restaurant starter menu uses a table layout.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" >
							<span class="boxtitle">Restaurant</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" class="examtext">
							<div class="select_link">
								<input class="radioMenu" name="mname" value="default_restau" type="radio" <?php echo ( ( !empty( $_POST['mname'] ) && $_POST['mname'] =='default_restau' ) || $_GET['product'] === 'all')  ? 'checked':''; ?> />
								<a href="#" class="signup menu" title="1">SELECT</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="180" height="140" align="center" valign="top" class="2 <?php echo (!empty($_POST['mname']) && $_POST['mname']=='default_pizza_')?'highlight':'' ?>">
				<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
					<tr>
						<td width="120" align="center" valign="middle">
							<div class="starter_menu_image">
								<img src="/images/type_4.png" width="100" height="60" />
								<div class="tooltip">This Pizza Shop starter menu uses a table layout and provides some special functionality.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" >
							<span class="boxtitle">Pizza Shop</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" class="examtext" >
							<div class="select_link">
								<input class="radioMenu" name="mname" value="default_pizza_" type="radio" <?php echo (!empty($_POST['mname']) && $_POST['mname']=='default_pizza_')?'checked':'' ?> />
								<a href="#" class="signup menu" title="2">SELECT</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="180" height="140" align="center" valign="top" class="3 <?php echo (!empty($_POST['mname']) && $_POST['mname']=='defaultbar')?'highlight':'' ?>">
				<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
					<tr>
						<td width="120" align="center" valign="middle">
							<div class="starter_menu_image">
								<img src="/images/type_2.png" width="100" height="60" />
								<div class="tooltip">This Bar/Lounge starter menu uses a tab layout. Tabs are not tethered to a table.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" >
							<span class="boxtitle">Bar/Lounge</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" class="examtext">
							<div class="select_link">
								<input class="radioMenu" name="mname" value="defaultbar" type="radio" <?php echo (!empty($_POST['mname']) && $_POST['mname']=='defaultbar')?'checked':'' ?> />
								<a href="#" class="signup menu" title="3">SELECT</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="180" height="140" align="center" valign="top" class="4 <?php echo (!empty($_POST['mname']) && $_POST['mname']=='default_coffee')?'highlight':'' ?>">
				<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
					<tr>
						<td width="120" align="center" valign="middle">
							<div class="starter_menu_image">
								<img src="/images/type_1.png" width="100" height="60" />
								<div class="tooltip">This Coffee Shop starter menu is best for quick serve environments generally using single transactions.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" >
							<span class="boxtitle">Coffee Shop</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" class="examtext">
							<div class="select_link">
								<input class="radioMenu" name="mname" value="default_coffee" type="radio" <?php echo (!empty($_POST['mname']) && $_POST['mname']=='default_coffee')? 'checked' : '' ?> />
								<a href="#" class="signup menu" title="4">SELECT</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php
			$s_non_niche = ob_get_contents();
			ob_end_clean();
			ob_start();
		?>
		<tr>
			<td width="180" height="140" align="center" valign="top" class="4 highlight" style="background-color:white;">
				<div style="background-color:#ddd; margin-left:-10px; margin-top:-10px; width:180px; height:140px; border:1px solid #ddd; border-radius:4px">
					<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu" style="margin: 0 auto;">
						<tr>
							<td width="120" align="center" valign="middle">
								<div class="starter_menu_image">
									<img src="<?= $s_niche_image ?>" width="100" height="60" />
									<div class="tooltip"><?= $s_niche_title ?></div>
								</div>
							</td>
						</tr>
						<tr>
							<td align="center" valign="middle" >
								<span class="boxtitle">&nbsp;</span>
							</td>
						</tr>
						<tr>
							<td align="center" valign="top" class="examtext">
								<div class="select_link">
									<input class="radioMenu" name="mname" value="<?= $_POST['mname'] ?>" type="radio"<?= $b_is_niche ? ' CHECKED' : '' ?> />
									<a href="#" class="signup menu" title="4"><?=  $s_niche_text ?></a>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<?php
			$s_with_niche = ob_get_contents();
			ob_end_clean();
			if ($b_is_niche)
			echo $s_with_niche;
			else
			echo $s_non_niche;
		?>

	  <tr>
	  	<td colspan="2" align="center"><label for="mname" class="error" style="font-size:18px;">Please choose a starter menu.</label>
		</td>
	  </tr>
	  <tr>
	    <td colspan="2">
	        <span class="maintextlight">
				<img src="images/i_help_sm.png" alt="Notice">
			    A Starter Menu is only used as a guide and does not affect any future menu setup decisions.
				You will be able to insert your own menu after this initial setup.
		    </span>
		</td>
	  </tr>
	</table>
  </div>
  <!-- end starter menu table -->
</div>

<!--</td></tr></table>-->

<input class="app-submit-button new-colors" id="subscription_continue1"
name="continue1" type="submit" value="Continue &rsaquo;"
onclick="ga('send', 'event', 'Lead Progress', 'Continue to Step2', 'Banner Form');ga('originalPOSlavu.send', 'event', 'Lead Progress', 'Continue to Step2', 'Banner Form');"/>

</form>

    </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javascript"></script>
<script src="/scripts/jquery.validate.js" type="text/javascript"></script>
<script src="/scripts/step1<?php echo $GLOBALS['s_dev']; ?>.js" type="text/javascript"></script>
<script src="/scripts/tracking.js" type="text/javascript"></script>
</body>
</html>
