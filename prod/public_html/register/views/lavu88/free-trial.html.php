<div id="free-trial"c lass="left leftcol">
        <!-- <p class="step-counter">Step 1 of 2 |
            <span class="capitals">Pro account</span>
        </p> -->
        
        <h2 class="signup_tagline">14 Day Free Trial</h2>
        <h3 class="signup_tagline_2 lavugreen">Signup now for your Lavu POS account</h3>
        <p>
            It is very easy to do yourself and we are always available to help.
            You can cancel anytime with or without our assistance - no obligations.
        </p>
        <p>
            <strong>No obligations, no pressure, no worries.</strong><br>
            Use our Free Trial to make sure Lavu is the best choice for your business. 
            Our Specialists are available to answer questions.
        </p>

</div>