<?php
/**
 * Shortcut function for conditional priting since we don't have shorthand priting
 * in the admin server
 */
function oldval( $varkey ) {
    echo ( !empty( $_REQUEST[$varkey] ) ) ? $_REQUEST[$varkey] : "";
}
?>
<div class="form_container" id="billing-info">
    <h3>Billing Information</h3>
    <ul>

        <li class="fieldsmall fieldleft">
            <label for="firstname">First Name *</label>
            <input id="firstname" class="inputsmall required" name="firstname"
            size="30" type="text" placeholder=" FIRST NAME *"
            value="<?php oldval( 'firstname' ); ?>"/>
        </li>
        <li class="fieldsmall fieldright">
            <label for="lastname">Last Name *</label>
            <input id="lastname" class="inputsmall required" name="lastname"
            size="30" type="text" placeholder=" LAST NAME *"
            value="<?php oldval( 'lastname' ); ?>" />
        </li>

        <li class="fieldsmall fieldleft">
            <label for="city">City *</label>
            <input id="city" class="inputsmall required" name="city"
            size="30" type="text" placeholder=" CITY *"
            value="<?php oldval( 'city' ); ?>" />
        </li>
        <li class="fieldsmall fieldright">
            <label for="state">State</label>
            <input id="state" class="inputsmall required" name="state"
            size="30" type="text" placeholder=" STATE"
            value="<?php oldval( 'state' ) ?>" />
        </li>

        <li>
            <label for="address">Address*</label>
            <input id="address"  class="required" name="address"
            size="30" type="text" placeholder=" ADDRESS *"
            value="<?php oldval( "address" ); ?>" />
        </li>

        <li class="fieldsmall fieldleft">
            <label for="zip">Postal Code *</label>
            <input id="zip" class="inputsmall required" name="zip"
            size="30" type="text" placeholder=" ZIP CODE *"
            value="<?php oldval("zip"); ?>" />
        </li>
        <li class="fieldsmall fieldright">
            <label for="promo">Promo Code</label>
            <input id="promo" class="inputsmall" name="promo"
            size="30" type="text" placeholder=" PROMO CODE"
            value="<?php oldval( "promo" ); ?>" />
        </li>

    </ul>
</div>

<?php
require_once "{$_SERVER['DOCUMENT_ROOT']}/check_ban_ip.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/recaptcha/recaptchalib.php";
$public_key = "6LfrtOsSAAAAAAIvqmehbwGW6vBo8GI1qGL_ngVR";
echo str_replace("http", "https", recaptcha_get_html($public_key));
?>