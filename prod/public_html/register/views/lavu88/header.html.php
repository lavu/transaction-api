<header>
    <div class="wrapper clearfix">
        <p id="header-logo" class="clearfix">
            <a class="lavu-logo" href="http://www.poslavu.com">
                <img src="/images/lavu-logo.png" alt="Lavu | Point of Sale" />
            </a>
            <sub class="little-text">RESTAURANT POS FOR iPAD</sub>
            <span id="header-phone" class="float-right">1-855-767-5288</span>
        </p>
        
    </div>
</header>