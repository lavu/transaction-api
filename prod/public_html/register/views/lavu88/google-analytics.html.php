<?php
if ( file_exists(dirname(__FILE__) . '/../../../admin/v2/objects/json_encode/jsonwrapper.php') ) require_once( dirname(__FILE__) . '/../../../admin/v2/objects/json_encode/jsonwrapper.php');  // Production
else if ( file_exists(dirname(__FILE__) . '/../../../admin/cp/objects/json_encode/jsonwrapper.php') ) require_once( dirname(__FILE__) . '/../../../admin/cp/objects/json_encode/jsonwrapper.php');  // Localdevserver
if(session_id() == '') {
    session_start();
}
$_GET['session_id'] = session_id();
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-30186945-2', 'auto',{'allowLinker':true,'alwaysSendReferrer' : true});
  ga('create', 'UA-30186945-1', 'auto', {'name': 'originalPOSlavu'});
  ga('send', 'pageview');
  ga('originalPOSlavu.send', 'pageview');

window.incoming_data = <?php echo json_encode( array_merge( $_GET, empty( $_POST )? array() : $_POST ) ); ?>;
window.send_lead_on_exit = <?php
//echo DEV || $_SERVER['REMOTE_ADDR'] !== "127.0.0.1" || $_SERVER['REMOTE_ADDR'] !== "::1" ? 'false': 'true';
echo "true"; ?>;
</script>
