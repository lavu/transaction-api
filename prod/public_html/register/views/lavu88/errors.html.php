<?php
if(session_id() == '') {
    session_start();
}
if ( isset( $_SESSION['error'] ) ) {
?>
<div id='errorExplanation'>
    <h2><?php echo $_SESSION['error']['h2']; ?></h2>
    <p><?php  echo $_SESSION['error']['p']; ?></p>
</div>
<?php 
    unset( $_SESSION['error'] );
}
?>