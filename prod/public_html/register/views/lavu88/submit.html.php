<div class="wrapper right" id="agreement">

    <p class="agree">
        By checking here you agree to our
        <a href="http://www.poslavu.com/en/tos" target="_blank">
            Terms of Service
        </a>.
    </p>

    <div id="submit_checkbox" class="center">
        <input type="checkbox" class="required" name="agree_tos" value="1" id="agree_tos" />
        <!-- <img src="/images/trans.png" width="54" height="54" alt="" id="fake_agree_tos" /> -->
    </div>

    <input class="app-submit-button new-colors upper" id="subscription_submit"
    name="finish_signup" type="submit" value="Create my account &rsaquo;" />
</div>