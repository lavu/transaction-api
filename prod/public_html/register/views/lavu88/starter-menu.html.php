<?php
$admin_dir = $_SERVER['HTTP_HOST'] === "lavuregister" ? 'LavuBackend' : "admin";
require_once $_SERVER['DOCUMENT_ROOT'] . "/../$admin_dir/sa_cp/billing/package_levels_object.php";
require_once "{$_SERVER['DOCUMENT_ROOT']}/signup_shared.php";
function process_incoming_niche_products() {
    global $b_is_niche;
    global $s_niche_image;
    global $s_niche_text;
    global $s_niche_title;
    global $o_signup;

    // build the list of allowed niche products
    $o_niche_products = $o_signup->getNicheProducts();
    $a_niche_names = array();
    foreach($o_niche_products as $k=>$v)
        $a_niche_names[] = $k;

    // find the niche product
    $s_niche = NULL;
    if (isset($_GET['product'])) {
        $s_niche = (trim($_GET['product']) != '') ? trim($_GET['product']) : NULL;
        if ($s_niche !== NULL && ($s_niche == 'all' || in_array($s_niche, $a_niche_names))) {
            $_SESSION['signup_product_name'] = $s_niche;
        } else {
            $s_niche = NULL;
        }
    }
    if (isset($_SESSION['signup_product_name'])) $s_niche = $_SESSION['signup_product_name'];
    if ($s_niche === NULL)
        return;
    $b_is_niche = FALSE;
    if ($s_niche !== NULL && in_array($s_niche, $a_niche_names))
        $b_is_niche = TRUE;

    // apply the niche product to the page
    if ($b_is_niche) {
        $a_niche_product = $o_niche_products->$s_niche;
        foreach($a_niche_product['posts'] as $post_key=>$post_val)
            $_POST[$post_key] = $post_val;
        $s_niche_text = $a_niche_product['display_name'];
        $s_niche_title = $a_niche_product['title'];
        $s_niche_image = $a_niche_product['image'];
    }
}

process_incoming_niche_products();

?>
<div class="form_container" id="starter-menu">
    <h3><?= $b_is_niche ? 'Your Starter Menu' : 'Choose Starter Menu' ?></h3>
    <table border="0" cellspacing="0" cellpadding="10" id="boxlinks">

        <?php
        /***********************************************************************
         *                    D R A W   T H E   M E N U
         **********************************************************************/
        ob_start();
        ?>
        <tr>
            <td width="50%" height="140" align="center" valign="top" class="1">
                <table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
                    <tr>
                        <td width="120" align="center" valign="middle" >
                            <div class="starter_menu_image">
                                <img src="/images/restaurant_icon_100x50px.png" />
                                <div class="tooltip">
                                    This Restaurant starter menu uses a table layout.
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" >
                            <span class="boxtitle">Restaurant</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" class="examtext">
                            <div class="select_link">
                                <input class="radioMenu required" name="mname"
                                value="default_restau" type="radio" />
                                <a href="#" class="signup menu" title="1">SELECT</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%" height="140" align="center" valign="top" class="2">
                <table width="120" border="0" cellspacing="0" cellpadding="0"
                class="start_menu">
                    <tr>
                        <td width="120" align="center" valign="middle">
                            <div class="starter_menu_image">
                                <img src="/images/pizza_icon_100x50px.png" />
                                <div class="tooltip">
                                    This Pizza Shop starter menu uses a table
                                    layout and provides some special functionality.
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" >
                            <span class="boxtitle">Pizza Shop</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" class="examtext" >
                            <div class="select_link">
                                <input class="radioMenu" name="mname" 
                                value="default_pizza_" type="radio" />
                                <a href="#" class="signup menu" title="2">SELECT</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="50%" height="140" align="center" valign="top" class="3">
                <table width="120" border="0" cellspacing="0" cellpadding="0"
                class="start_menu">
                    <tr>
                        <td width="120" align="center" valign="middle">
                            <div class="starter_menu_image">
                                <img src="/images/bar_icon_100x50px.png"
                                class="required" alt="Bar/Lounge" />
                                <div class="tooltip">
                                    This Bar/Lounge starter menu uses a tab layout.
                                    Tabs are not tethered to a table.
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" >
                            <span class="boxtitle">Bar/Lounge</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" class="examtext">
                            <div class="select_link">
                                <input class="radioMenu" name="mname"
                                value="defaultbar" type="radio" />
                                <a href="#" class="signup menu" title="3">SELECT</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td width="50%" height="140" align="center" valign="top" class="4">
                <table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
                    <tr>
                        <td width="120" align="center" valign="middle">
                            <div class="starter_menu_image">
                                <img src="/images/coffee_icon_100x50px.png" alt="Coffee Shop" />
                                <div class="tooltip">This Coffee Shop starter menu is best for quick serve environments generally using single transactions.</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle" >
                            <span class="boxtitle">Coffee Shop</span>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" class="examtext">
                            <div class="select_link">
                                <input class="radioMenu" name="mname" type="radio"
                                value="default_coffee" />
                                <a href="#" class="signup menu" title="4">SELECT</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php
            $s_non_niche = ob_get_contents();
            ob_end_clean();
            ob_start();
        ?>
        <tr>
            <td width="50%" height="140" align="center" valign="top" class="4 highlight" style="background-color:white;">
                <div style="background-color:#ddd; margin-left:-10px; margin-top:-10px; width:180px; height:140px; border:1px solid #ddd; border-radius:4px">
                    <table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu" style="margin: 0 auto;">
                        <tr>
                            <td width="120" align="center" valign="middle">
                                <div class="starter_menu_image">
                                    <img src="<?= $s_niche_image ?>" width="100" height="60" />
                                    <div class="tooltip"><?= $s_niche_title ?></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" >
                                <span class="boxtitle">&nbsp;</span>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top" class="examtext">
                                <div class="select_link">
                                    <input class="radioMenu" name="mname" value="<?= $_POST['mname'] ?>" type="radio"<?= $b_is_niche ? ' CHECKED' : '' ?> />
                                    <a href="#" class="signup menu" title="4"><?=  $s_niche_text ?></a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <?php
            $s_with_niche = ob_get_contents();
            ob_end_clean();
            if ($b_is_niche)
            echo $s_with_niche;
            else
            echo $s_non_niche;
        ?>

      <tr>
        <td colspan="2" align="center"><label for="mname" class="error" style="font-size:18px;">Please choose a starter menu.</label>
        </td>
      </tr>
      <tr>
        <td colspan="2">
            <span class="maintextlight">
                <img src="/images/i_help_sm.png" alt="Notice">
                A Starter Menu is only used as a guide and does not affect any future menu setup decisions.
                You will be able to insert your own menu after this initial setup.
            </span>
        </td>
      </tr>
    </table>
  </div>