<?php
function create_option_list($option_name, $options)
{
    $str = "";
    foreach($options as $key => $val)
    {
        $selected = "";
        if(isset($_POST[$option_name]) && ($_POST[$option_name]==$key || (is_numeric($_POST[$option_name]) && $_POST[$option_name] * 1 == $key * 1)))
            $selected = " selected";
        $str .= "<option value=\"$key\"".$selected.">$val</option>";
    }
    return $str;
}
?>
<div class="form_container" id="cc-info">
    <h3>Credit Card Information</h3>
    <ul>
        <li>
            <label for="card_number">Credit card number*</label>
            <input id="card_number" name="card_number" size="30" type="text"
            placeholder=" CREDIT CARD NUMBER *" class="required" />
        </li>
        <li class="fieldsmall left expdate">
            <label for="card_expiration" class="show">Expires on*</label>
            <select id="card_expiration_mo" name="card_expiration_mo">
                <option value=""></option>
                <?= create_option_list("card_expiration_mo",array(
                    "01"=>"1 - Jan",
                    "02"=>"2 - Feb",
                    "03"=>"3 - Mar",
                    "04"=>"4 - Apr",
                    "05"=>"5 - May",
                    "06"=>"6 - Jun",
                    "07"=>"7 - Jul",
                    "08"=>"8 - Aug",
                    "09"=>"9 - Sep",
                    "10"=>"10 - Oct",
                    "11"=>"11 - Nov",
                    "12"=>"12 - Dec")
                ); ?>
            </select>

            <select id="card_expiration_yr" name="card_expiration_yr">
                <option value=""></option>
                <?php
                    $a_years = array();
                    for ($i = 0; $i < 11; $i++) {
                        $year = date('Y', strtotime('+'.$i.' years'));
                        $a_years[$year] = $year;
                    }
                    echo create_option_list("card_expiration_yr", $a_years); ?>
            </select>

        </li>
        <li class="fieldsmall right ccvinput">
            <label for="card_code">Security Code (CVV)*</label>
            <input id="card_code" class="inputsmall required" name="card_code"
            size="30" type="text" placeholder=" SECURITY CODE (CVV) *"  />
        </li>
    </ul>
</div>