<?php
$company = empty( $REQUEST['company_name'] ) ? '' : $_REQUEST['company_name'];
$phone = empty( $_REQUEST['phone'] ) ? '' : $_REQUEST['phone'];
$email = empty( $_REQUEST['email'] ) ? '' : $_REQUEST['email'];
$zip = empty( $_REQUEST['zip'] ) ? '' : $_REQUEST['zip']; 
$name = empty( $_REQUEST['name'] ) ? '' : $_REQUEST['name'];
?>
<div class="form_container" id="company-info">
    <p class="center">
        <img src="/images/circle_arrow_2b_88.png" alt="" />
    </p>
    <h3>Company Information</h3>
    <ul>
        <li>
            <label for="company_name">Company Name *</label>
            <input id="company_name" class="required" name="company_name" type="text"
            value="<?php echo $company; ?>" placeholder=" COMPANY NAME *" />
        </li>

        <li class="fieldsmall fieldleft">
            <label for="phone">Phone *</label>
            <input class="inputsmall required" id="phone" name="phone" type="text"
            value="<?php echo $phone; ?>" placeholder=" PHONE *"/>
        </li>
        <li class="fieldsmall fieldright">
            <label for="email">Email *</label>
            <input class="inputsmall required" id="email" name="email" type="text"
            value="<?php echo $email; ?>" placeholder=" EMAIL *"/>
        </li>

        <li class="fieldsmall fieldleft">
            <label for="zip">Postal Code *</label>
            <input class="inputsmall required" id="zip" name="zip" type="text"
            value="<?php echo $zip; ?>" placeholder=" ZIP CODE *" />
        </li>
        <li class="fieldsmall fieldright">
            <label for="full_name">Full Name</label>
            <input class="inputsmall" id="full_name" name="full_name"
            type="text" value="<?php echo $name; ?>" placeholder=" FULL NAME" />
        </li>
    </ul>
</div>