<?php

global $activation_link_host;
global $shifted_signups_id;
global $shifted_reseller_leads_id;
global $verification_token;

$color = "#aecd37";
if ( !defined('LAVU_TRIAL_DAYS') ) define('LAVU_TRIAL_DAYS', 14 );

?>
<!DOCTYPE html>
<html>
<head>
  <title>Activate Your Lavu Account</title>
  <style>
@font-face{
  font-family: 'din';
  src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot);
  src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot?#iefix) format('embedded-opentype'),
  url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.woff) format('woff'),
  url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.ttf) format('truetype'),
  url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.svg#dinregular) format('svg');
  font-weight: normal;
  font-style: normal;
}
  @font-face {
    font-family: 'din';
    src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot);
    src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot?#iefix) format('embedded-opentype'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.woff) format('woff'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.ttf) format('truetype'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.svg#dinbold) format('svg');
    font-weight: bold;
    font-style: normal;
  }
  @font-face {
    font-family: 'din';
    src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot);
    src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot?#iefix) format('embedded-opentype'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.woff) format('woff'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.ttf) format('truetype'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.svg#din_lightregular) format('svg');
    font-weight: lighter;
    font-style: normal;
  </style>
</head>
<body style="margin:0;padding:0;background-color:#f4f4f4;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:16px;color:#2b2b2b;min-height:100%;@font-face{font-family: 'din';src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot);src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot?#iefix) format('embedded-opentype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.woff) format('woff'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.ttf) format('truetype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.svg#dinregular) format('svg');font-weight: normal;font-style: normal;}@font-face {font-family: 'din';src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot);src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot?#iefix) format('embedded-opentype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.woff) format('woff'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.ttf) format('truetype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.svg#dinbold) format('svg');font-weight: bold;font-style: normal;}@font-face {font-family: 'din';src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot);src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot?#iefix) format('embedded-opentype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.woff) format('woff'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.ttf) format('truetype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.svg#din_lightregular) format('svg');font-weight: lighter;font-style: normal;">
  <div style="widht:100%;height:100%;">
  <div style="width:100%;height:90px;background-color:#2b2b2b;display:block;text-align:center;"><a href="https://www.lavu.com"><img src="https://www.lavu.com/assets/images/Lavu_iPad_POS_Logo.png" width="150" height="23" style="margin:33px auto 0px auto;"></a></div>
<div style="width:100%;height:3px;background-color:#bdbdbd;margin:0;padding:0;">
    <div style="background-color:#aecd37;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#0eae95;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#3382a9;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#5053a2;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#f68e20;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#ebcf4a;height:100%;width:20px;float:right;"></div>
</div>
<div style="width:640px;height:100%;background-color:#ffffff;margin:0 auto 0 auto;">
  <h1 style="padding:40px 18% 20px 18%;margin:0;text-align:center;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:24px;color:#8b8b8b;font-weight:lighter;">Activate Your Lavu Account</h1>
  <p style="padding:20px 18% 20px 18%;margin:0;text-align:center;font-weight:normal;"><a href="http://<?= $activation_link_host ?>/activate-signup/<?= $shifted_signups_id ?>/<?= $shifted_reseller_leads_id ?>/<?= $verification_token ?>" style="color:#fff;padding:12px 18px 12px 18px;border:2px solid #aecd37;background-color:#aecd37;border-radius:5px;text-decoration:none;">Click Here to Activate</a></p>
  <p style="padding:20px 18% 20px 18%;margin:0;text-align:justify;font-weight:normal;"><img src="https://www.lavu.com/assets/images/lavu_products.png" width="410" height="189" style="display:block;margin-left:auto;margin-right:auto;"></p>
  <hr width="430px" style="width:70%;display: block;margin-top:40px;margin-bottom:0px;margin-left:auto;margin-right:auto;border0;border-top:1px solid #dddddd;">
  <h1 style="padding:40px 18% 20px 18%;margin:0;text-align:center;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:24px;color:#8b8b8b;font-weight:lighter;">NEED HELP?</h1>
  <table style="min-height:170px;width:480px;list-style:none;margin:0 auto 40px auto;border:none;padding:0;"><tr>
    <td style="margin-right:10px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="tel:+18557675288"><img src="https://www.lavu.com/assets/images/support-icon.png" width="97" height="84"></a><p style="padding:10px 0 0 0;text-align:center;">Call Support<br/><a href="tel:+18557675288" style="color:#aecd37;">855.767.5288</a></p></td>
    <td style="margin-right:10px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="https://support.lavu.com"><img src="https://www.lavu.com/assets/images/help-icon.png" width="97" height="84"></a><p style="padding:10px 0 0 0;text-align:center;">Help Center<br/><a href="https://support.lavu.com" style="color:#aecd37;">support.lavu.com</a></p></td>
    <td style="margin-right:20px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="http://lavucommunity.com"><img src="https://www.lavu.com/assets/images/forum-icon.png" width="97" height="84"></a><p style="padding:10px 0 0 0;text-align:center;">User Forums<br/><a href="https://talk.lavu.com" style="color:#aecd37;">talk.lavu.com</a></p></td>
    <td style="margin-right:0px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="http://youtube.com/user/poslavu"><img src="https://www.lavu.com/assets/images/youtube-icon.png" width="97" height="84"></a><p style="padding:10px 0 0 0;text-align:center;">Video Tutorials<br/><a href="http://youtube.com/user/poslavu" style="color:#aecd37;">youtube.com/user/poslavu</a></p></td>
  </tr></table>
</div>
  <div style="width:100%;height:150px;background-color:#dddddd;display:block;text-align:center;">
  <div style="width:500px;height:50px;background-color:transparent;display:block;padding-top:60px;margin:0 auto 0 auto;">
  <h1  style="width:120px;margin:0;text-align:center;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:18px;color:#8b8b8b;font-weight:lighter;float:left;padding:5px 0 0 0;display:inline;vertical-align:center;"><a href="tel:+18557675288" style="color:#8b8b8b;text-decoration:none;">855.767.5288</a></h1>
  <div style="height:30px;width:80px;position:relative;margin:50px auto 0px auto;display:inline;">
  <a href="http://twitter.com/lavuinc"><img src="https://www.lavu.com/assets/images/twitter.png" width="37" height="30" style="vertical-align:center;"></a>
  <a href="http://facebook.com/lavuinc"><img src="https://www.lavu.com/assets/images/facebook.png" width="37" height="30" style="vertical-align:center;"></a>
  </div>
  <h1  style="width:120px;margin:0;text-align:center;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:18px;color:#8b8b8b;font-weight:lighter;float:right;padding:5px 0 0 0;display:inline;vertical-align:center;"><a href="https://www.lavu.com" style="color:#8b8b8b;text-decoration:none;">www.lavu.com</a></h1>
  </div>
  </div>
  </div>
</body>
</html>
