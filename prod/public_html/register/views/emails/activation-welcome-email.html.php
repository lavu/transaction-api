<?php

if ( empty($username) ) $username = "username";
if ( empty($password) ) $password = "password";
if ( empty($company_name) ) $company_name = "";
if ( !defined('LAVU_TRIAL_DAYS') ) {
	define('LAVU_TRIAL_DAYS', 14 );
}

?>
<!DOCTYPE html>
<html>
<head>
  <title>Welcome to Lavu - Getting Started</title>
  <style>
@font-face{
  font-family: 'din';
  src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot);
  src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot?#iefix) format('embedded-opentype'),
  url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.woff) format('woff'),
  url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.ttf) format('truetype'),
  url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.svg#dinregular) format('svg');
  font-weight: normal;
  font-style: normal;
}
  @font-face {
    font-family: 'din';
    src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot);
    src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot?#iefix) format('embedded-opentype'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.woff) format('woff'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.ttf) format('truetype'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.svg#dinbold) format('svg');
    font-weight: bold;
    font-style: normal;
  }
  @font-face {
    font-family: 'din';
    src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot);
    src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot?#iefix) format('embedded-opentype'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.woff) format('woff'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.ttf) format('truetype'),
    url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.svg#din_lightregular) format('svg');
    font-weight: lighter;
    font-style: normal;
}
  </style>
</head>
<body style="margin:0;padding:0;background-color:#f4f4f4;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:16px;color:#8b8b8b;min-height:100%;@font-face{font-family: 'din';src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot);src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.eot?#iefix) format('embedded-opentype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.woff) format('woff'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.ttf) format('truetype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din-webfont.svg#dinregular) format('svg');font-weight: normal;font-style: normal;}@font-face {font-family: 'din';src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot);src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.eot?#iefix) format('embedded-opentype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.woff) format('woff'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.ttf) format('truetype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_medium/din_medium-webfont.svg#dinbold) format('svg');font-weight: bold;font-style: normal;}@font-face {font-family: 'din';src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot);src: url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.eot?#iefix) format('embedded-opentype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.woff) format('woff'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.ttf) format('truetype'),url(https://www.lavu.com/wp-content/themes/lavu.com/fonts/din_light-webfont.svg#din_lightregular) format('svg');font-weight: lighter;font-style: normal;}">
  <div style="widht:100%;height:100%;">
  <div style="width:100%;height:90px;background-color:#2b2b2b;display:block;text-align:center;"><a href="https://www.lavu.com"><img src="https://www.lavu.com/assets/images/Lavu_iPad_POS_Logo.png" width="150" height="23" style="margin:33px auto 0px auto;"></a></div>
<div style="width:100%;height:3px;background-color:#bdbdbd;margin:0;padding:0;">
    <div style="background-color:#aecd37;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#0eae95;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#3382a9;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#5053a2;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#f68e20;height:100%;width:20px;float:right;"></div>
    <div style="background-color:#ebcf4a;height:100%;width:20px;float:right;"></div>
</div>
<div style="width:640px;height:100%;background-color:#ffffff;margin:0 auto 0 auto;">
  <table border="0" style="margin-left:18%; width:100%;padding-top:30px;"><tr>
  <td style="margin-left:10px;width:66px; margin-right:10px;"><img src="http://i.imgur.com/7KSDr66.png" width="66" height="66" style="display:block;margin:0 auto 0px auto;"></td>
  <td style="margin-right:10px;"><span style="padding:0px 10px;margin:0;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:24px;color:#8b8b8b;font-weight:lighter;line-height:34px;">Hello <strong><?= $company_name ?></strong></span></td>
  </tr></table><br />
  <h2 style="text-align:center;font-size:24px;color:#8b8b8b;font-weight:lighter;">Let's get you started, it's super easy.</h2>
  <table style="min-height:170px;width:480px;list-style:none;margin:0 auto 0px auto;border:none;padding:0;"><tr>
    <td style="margin-right:0px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="https://itunes.apple.com/us/app/pos-lavu-client-point-of-sale/id391800399?mt=8"><img src="https://www.lavu.com/assets/images/getstarted-icon.png"></a><p style="font-size:10pt;padding:10px 0 0 0;text-align:center;">1. Get the App<br/><a href="https://itunes.apple.com/us/app/pos-lavu-client-point-of-sale/id391800399?mt=8" style="color:#aecd37;">Lavu POS in the App Store</a></p></td>
    <td style="margin-right:10px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="https://admin.poslavu.com"><img src="https://www.lavu.com/assets/images/nextstep-icon.png"></a><p style="font-size:10pt;padding:10px 0 0 0;text-align:center;">2. Login to Control Panel<br/><a href="https://admin.poslavu.com" style="color:#aecd37;">admin.poslavu.com</a><br></p></td>
  </tr></table>

  <table style="list-style:none;margin:0 auto 0px auto;border:none;padding:0;"><tr>
    <tr><td align="right"><strong style="font-size:16pt">Username:</strong></td><td><span style="font-size:15pt;padding-left:15px;color:#8b8b8b;"><?= $username ?></span></td></tr>
    <tr><td align="right"><strong style="font-size:16pt">Password:</strong></td><td><span style="font-size:15pt;padding-left:15px;color:#8b8b8b;"><?= $password ?></span></td></tr>
    <tr><td colspan="2"><p style="text-align:center;color:#8b8b8b;font-size:9pt;">(You may have changed your password if you've already logged in)</p></td>
  </tr></table>
    
    <p style="text-align:center;"><img src="https://www.lavu.com/assets/images/setup_ipad_pin.jpg" width="410"></p>

      <hr width="430px" style="width:70%;display: block;margin-top:20px;margin-bottom:40px;margin-left:auto;margin-right:auto;border0;border-top:1px solid #dddddd;">

      <h2 style="text-align:center;font-size:24px;color:#8b8b8b;font-weight:lighter;">Free Lavu Promotion</h2>

    <p style="font-size:10pt;padding:20px 18% 20px 18%;margin:0;text-align:justify;font-weight:normal;line-height:20px;color:#8b8b8b;"><strong>Continue using Lavu for FREE after your 14 day trial!</strong><br />Simply sign up with one of our featured merchant service providers and receive up to 3 years of waived hosting fees for our single-terminal system. </p>
    <p style="font-size:10pt;padding:20px 18% 20px 18%;margin:0;text-align:justify;font-weight:normal;line-height:20px;color:#8b8b8b;">(Promotion only valid for customers in US/Canada.)</p>

  <hr width="430px" style="width:70%;display: block;margin-top:20px;margin-bottom:20px;margin-left:auto;margin-right:auto;border0;border-top:1px solid #dddddd;">
  <p style="text-align:center"><a href="https://www.lavu.com/wp-content/uploads/2015/03/lavu-start-guide.pdf" target="_blank"><img src="https://www.lavu.com/wp-content/uploads/2015/05/LAVU_GSG.jpg"></a></p>
  <p style="text-align:center;font-size:10pt">Check out our <a href="https://www.lavu.com/wp-content/uploads/2015/03/lavu-start-guide.pdf" target="_blank" style="color:#aecd37;">Easy Setup Guide</a></p>
  <h1 style="padding:40px 18% 5px 18%;margin:0;text-align:center;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:24px;color:#8b8b8b;font-weight:lighter;">Need Help?</h1>
  <table style="min-height:170px;width:480px;list-style:none;margin:0 auto 40px auto;border:none;padding:0;"><tr>
    <td style="margin-right:10px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="tel:+18557675288"><img src="https://www.lavu.com/assets/images/support-icon.png" width="97" height="84"></a><p style="padding:10px 0 0 0;text-align:center;">Call Support<br/><a href="tel:+18557675288" style="color:#aecd37;">855.767.5288</a></p></td>
    <td style="margin-right:10px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="https://support.lavu.com"><img src="https://www.lavu.com/assets/images/help-icon.png" width="97" height="84"></a><p style="padding:10px 0 0 0;text-align:center;">Help Center<br/><a href="https://support.lavu.com" style="color:#aecd37;">support.lavu.com</a></p></td>
    <td style="margin-right:20px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="http://lavucommunity.com"><img src="https://www.lavu.com/assets/images/forum-icon.png" width="97" height="84"></a><p style="padding:10px 0 0 0;text-align:center;">User Forums<br/><a href="https://talk.lavu.com" style="color:#aecd37;">talk.lavu.com</a></p></td>
    <td style="margin-right:0px;width:110px;height:170px;font-size:10px;text-align:center;"><a href="http://youtube.com/user/poslavu"><img src="https://www.lavu.com/assets/images/youtube-icon.png" width="97" height="84"></a><p style="padding:10px 0 0 0;text-align:center;">Video Tutorials<br/><a href="http://youtube.com/user/poslavu" style="color:#aecd37;">youtube.com/user/poslavu</a></p></td>
  </tr></table>
</div>
  <div style="width:100%;height:150px;background-color:#dddddd;display:block;text-align:center;">
  <div style="width:500px;height:50px;background-color:transparent;display:block;padding-top:60px;margin:0 auto 0 auto;">
  <h1  style="width:120px;margin:0;text-align:center;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:18px;color:#8b8b8b;font-weight:lighter;float:left;padding:5px 0 0 0;display:inline;vertical-align:center;"><a href="tel:+18557675288" style="color:#8b8b8b;text-decoration:none;">855.767.5288</a></h1>
  <div style="height:30px;width:80px;position:relative;margin:50px auto 0px auto;display:inline;">
  <a href="http://twitter.com/lavuinc"><img src="https://www.lavu.com/assets/images/twitter.png" width="37" height="30" style="vertical-align:center;"></a>
  <a href="http://facebook.com/lavuinc"><img src="https://www.lavu.com/assets/images/facebook.png" width="37" height="30" style="vertical-align:center;"></a>
  </div>
  <h1  style="width:120px;margin:0;text-align:center;font-family : 'din', Verdana, Helvetica, sans-serif;font-size:18px;color:#8b8b8b;font-weight:lighter;float:right;padding:5px 0 0 0;display:inline;vertical-align:center;"><a href="https://www.lavu.com" style="color:#8b8b8b;text-decoration:none;">lavu.com</a></h1>
  </div>

  </div>
  </div>
</body>
</html>
