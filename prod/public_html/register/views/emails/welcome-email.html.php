<?php
$username = empty($username) ? '' : $username;
$password = empty($password) ? '' : $password;
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome.</title>
</head>
<body style="width:100%;margin:auto;font-family:Sans-Serif;background-color:#5a5a5d;font-weight:normal;">
	<div style="width:600px;margin:auto;background-color:#fff;">
    	<img src="https://www.lavu.com/perch/emails/Day1_Header.png">
        <h1 style="text-align:center;font-weight:normal;">Welcome to Lavu.</h1>
        <table width="480" cellspacing="0" cellpadding="5" border="0" style="margin:auto;">
        <tr>
        	<td width="40" valign="top">
            	<img src="https://www.lavu.com/perch/emails/numbers/1.png">
            </td>
            <td width="420">
            	<p style="font-size:16px;font-weight:normal;margin:5px 0;padding:0;">Download the Lavu POS 4.0 app</p>
               <p style="font-size:13px;color:#5b5b5b;margin:0 0 20px 0;padding:0;">Visit the Apple App Store to download the Lavu POS 4.0 app on your iPad.</p>
            </td>
        </tr>
        <tr>
        	<td width="40" valign="top">
            	<img src="https://www.lavu.com/perch/emails/numbers/2.png">
            </td>
            <td width="420">
            	<p style="font-size:16px;font-weight:normal;margin:5px 0;padding:0;">Get started</p>
                <p style="font-size:13px;color:#5b5b5b;margin:0 0 20px 0;padding:0;">To begin your free trial, visit  <a href="https://admin.lavu.com" style="color:#000000;">admin.lavu.com</a>, or copy and paste the link into your browser. Your temporary username and password are provided below, and the default PIN for your Lavu POS App is 1234.</p>
            </td>
        </tr>
        <tr>
        	<td colspan="2">
            	<div style="display:block;position:relative;width:200px;padding:7px 18px 5px 18px;margin:0 auto 10px auto;text-align:center;background-color:#aecd37;border-radius:4px;color:#000;text-decoration:none;">Username: <?= $username ?><br>Password: <?= $password ?></div>
            </td>
        </tr>
        <tr>
        	<td width="40" valign="top">
            	<img src="https://www.lavu.com/perch/emails/numbers/3.png">
            </td>
            <td width="420">
            	<p style="font-size:16px;font-weight:normal;margin:5px 0;padding:0;">Change your PIN number and password</p>
                <p style="font-size:13px;color:#5b5b5b;margin:0 0 20px 0;padding:0;">You are now in the Lavu Control Panel. To change your PIN number and password, click <a href="https://admin.lavu.com/cp/index.php?mode=settings_edit_users&rowid=1" style="color:#000000;">here</a>. Type your new password into both password boxes and enter your new PIN number over the old one. When finished, click the submit button on the bottom of the page. </p>
            </td>
        </tr>
        <tr>
        	<td width="40" valign="top">
            	<img src="https://www.lavu.com/perch/emails/numbers/4.png">
            </td>
            <td width="420">
            	<p style="font-size:16px;font-weight:normal;margin:5px 0;padding:0;">Explore your Control Panel</p>
                <p style="font-size:13px;color:#5b5b5b;margin:0 0 20px 0;padding:0;">In the Control Panel, click on the “Assistance” tab on the top of your screen. Here you’ll find tutorials on the layouts and features of Lavu. The “Guide” tab on the “Assistance” screen will assist you in setting up your menu, inventory, and business information such as tax rate and monetary symbol.</p>
            </td>
        </tr>
        </table>
        <div style="width:470px;margin:0 auto 40px auto;">
        	<p style="font-size:13px;color:#5b5b5b;">Over the next few days you’ll receive follow-up emails and a phone call from one of our Support Agents to ensure that your Free Trial is going well. Questions? Call our 24/7 Support Line at <a href="tel:+18555288457" style="color:#000000;">855-528-8457</a> anytime. </p>
        	<p style="font-size:13px;color:#5b5b5b;">Thank you and enjoy your free trial</p>	
        </div>
        <a href="https://admin.lavu.com" style="display:block;position:relative;width:150px;padding:7px 18px 5px 18px;margin:0 auto 40px auto;text-align:center;background-color:#aecd37;border-radius:4px;color:#000;text-decoration:none;">Start Exploring Now</a>
        <div style="background-color:#f1f1ef;width:100%;padding:20px 0;">
        	<div style="width:350px;margin:auto;text-align:center;">
            	<p style="font-size:13px;color:#5b5b5b;margin-top:20px;">Need further assistance? Contact us at <a href="tel:+18555288457" style="color:#000000;">855-528-8457</a>, email <a href="mailto:support@lavu.com" style="color:#000000;">support@lavu.com</a>, or visit our help center <a href="http://support.lavu.com" style="color:#000000;">support.lavu.com</a>.</p>
            	<h3 style="font-size:15px;color:#5b5b5b;">Support from Lavu — always free and 24/7</h3>
        	</div>
        </div>
        <div style="background-color:#2b2b2b;width:100%;padding:20px 0;">
        <table width="200" cellspacing="20" cellpadding="0" border="0" style="margin:auto;">
        <tr>
        	<td valign="middle">
        		<a href="https://instagram.com/lavuinc"><img src="https://www.lavu.com/perch/emails/footer/Insta.png" width="20" height="20"></a>
            </td>
            <td valign="middle">
        		<a href="https://www.facebook.com/lavuinc"><img src="https://www.lavu.com/perch/emails/footer/Facebook.png" width="20" height="20"></a>
            </td>
            <td valign="middle">
        		<a href="https://twitter.com/lavuinc"><img src="https://www.lavu.com/perch/emails/footer/Twits.png" width="20" height="20"></a>
            </td>
            <td valign="middle">
        		<a href="https://www.linkedin.com/company/lavu-inc."><img src="https://www.lavu.com/perch/emails/footer/LinkedIn.png" width="20" height="20"></a>
            </td>
            <td valign="middle">
        		<a href="mailto:support@lavu.com"><img src="https://www.lavu.com/perch/emails/footer/EmailButton.png" width="25" height="25"></a>
            </td>
        </tr>
        </table>
        </div>
    </div>
</body>
</html>
