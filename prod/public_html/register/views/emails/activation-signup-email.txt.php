<?php

global $activation_link_host;
global $shifted_signups_id;
global $shifted_reseller_leads_id;
global $verification_token;

?>

Welcome to Lavu

Your Lavu POS Account is Ready

Click here to activate your account
http://<?= $activation_link_host ?>/activate-signup/<?= $shifted_signups_id ?>/<?= $shifted_reseller_leads_id ?>/<?= $verification_token ?>

(You may need to copy and paste the link above into a web browser.)

NEED HELP?

Call Support
855.767.5288

Help Center
support.lavu.com

User Forums
lavucommunity.com

Video Tutorials
youtube.com/user/poslavu

855.767.5288
lavu.com