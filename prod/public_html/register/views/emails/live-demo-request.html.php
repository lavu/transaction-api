<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 12/15/14
 * Time: 5:20 PM
 */ ?><html>
<body style="background-color:#ffffbb;font-family:sans-serif;">
<div style="width:640px;margin:0 auto;background-color:#ffffd0;padding-bottom:30px;">
    <table border="0">
        <tr>
            <td width="40%" style="padding:1em;">
                <img src="https://www.lavu.com/assets/images/lavu-logo.png" alt="" />
            </td>
            <td>
                <h1 style="text-align:center;">Live Demo Request</h1>
            </td>
        </tr>
    </table>
    <p style="margin:1em;"><b>Salesforce Lead ID:</b> <?= $salesforce_lead_id ?></p>
    <p style="margin:1em;"><b>Time:</b> <?= $demo_time ?></p>
    <p style="margin:1em;"><b>Name:</b> <?= $name ?></p>
    <p style="margin:1em;"><b>Phone:</b> <?= $phone ?></p>
    <p style="margin:1em;"><b>Email:</b> <?= $email ?></p>
    <p style="margin:1em;"><b>Company:</b> <?= $company_name ?></p>
    <p style="margin:1em;"><b>Notes:</b> <?= $demo_notes ?></p>
</div>
</body>
</html>
