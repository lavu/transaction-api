<?php
if ( empty( $username) && empty( $password ) ) header('Content-Type: application/json; charset=utf8' );
if ( empty( $username ) ) $username = "username";
if ( empty( $password ) ) $password = "password";
if ( empty( $company_name ) ) $company_name = "";
if ( !defined('LAVU_TRIAL_DAYS') ) define('LAVU_TRIAL_DAYS', 14 );
$yyyy = date('Y');
?>

** Welcome to Lavu **

Hello <?= $company_name ?>

Let's get you started, it's super easy.

1. Get the App -- https://itunes.apple.com/us/app/pos-lavu-client-point-of-sale/id391800399?mt=8

2. Login to Control Panel -- http://admin.lavu.com/

Username: <?= $username ?> 
Password: <?= $password ?> 

(You may have changed your password if you've already logged in)

Lavu Default PIN: 1234.

(Use this pin until you have setup your custom PIN)


** Free Lavu Promotion ** 

Continue using Lavu for FREE after your 14 day trial!

Simply sign up with one of our featured merchant service providers and receive up to 3 years of waived hosting fees for our single-terminal system.

(Promotion only valid for customers in US/Canada.)


** Need Help? **

Check out our easy Setup Guide -- http://www.lavu.com/wp-content/uploads/2015/03/lavu-start-guide.pdf

Call Support -- 855.767.5288

Help Center -- support.lavu.com

User Forums -- talk.lavu.com

Video Tutorials -- youtube.com/user/poslavu

--

Lavu(c) POSLAVU <?= $yyyy ?> | ALL RIGHTS RESERVED