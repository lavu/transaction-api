<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 10/2/14
 * Time: 6:20 PM
 */ ?><html>
<head></head>
<body style="background-color:#f0f0f0;font-family:sans-serif;color:#333333;">
<div style="width:560px;padding:40px;background-color:white;margin:auto;">
    <h1 style="text-align: center;text-transform: uppercase">
        A Potential PRO License Customer requires attention
    </h1>
    <p>Here's the contact data</p>
    <ul>
        <li>
            Name : <?=  empty( $name  ) ? "ERROR: None captured. Contact carlosa@poslavu.com": $name  ?>
        </li>
        <li>
            Company : <?=  empty( $company  ) ? "ERROR: None captured. Contact carlosa@poslavu.com": $company  ?>
        </li>
        <li>
            Email : <?=  empty( $email  ) ? "ERROR: None captured. Contact carlosa@poslavu.com": $email  ?>
        </li>
        <li>
            Phone :<?=  empty( $phone  ) ? "ERROR: None captured. Contact carlosa@poslavu.com": $phone  ?>
        </li>
        <li>
            Country : <?=  empty( $country  ) ? "ERROR: None captured. Contact carlosa@poslavu.com": $country  ?>
        </li>
        <li>
            ZIP/Postal: <?=  empty( $zip  ) ? "ERROR: None captured. Contact carlosa@poslavu.com": $zip  ?>
        </li>
        <li>
            City : <?=  empty( $city  ) ? "ERROR: None captured. Contact carlosa@poslavu.com": $city  ?>
        </li>
        <li>
            Received : <?= date('D F jS, Y @ h:m A') ?>
        </li>
        <li>
            Lead Source : <?php echo $lead_source; ?>
        </li>
        <li>
            Salesforce Account ID : <?php echo $salesforce_id; ?>
        </li>
    </ul>
</div>
</body>
 </html>