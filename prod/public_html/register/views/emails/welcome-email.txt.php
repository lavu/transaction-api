<?php
$username = empty($username) ? '' : $username;
$password = empty($password) ? '' : $password;
?>

Welcome.
	

1 - Download the Lavu app

Visit the Apple App Store to download the Lavu POS app on your iPad.


2 - Get started

To begin your free trial, visit admin.lavu.com, or copy and paste the link into your browser. Your temporary username and password are provided below, and the default PIN for your Lavu POS App is 1234.

	Username: <?= $username ?>
	Password: <?= $password ?>

	
3 - Change your PIN number and password

You are now in the Lavu Control Panel. To change your PIN number and password, click here.

	https://admin.lavu.com/cp/index.php?mode=settings_edit_users&rowid=1

Type your new password into both password boxes and enter your new PIN number over the old one. When finished, click the submit button on the bottom of the page.
	

4 - Explore your Control Panel

In the Control Panel, click on the “Assistance” tab on the top of your screen. Here you’ll find tutorials on the layouts and features of Lavu. The “Guide” tab on the “Assistance” screen will assist you in setting up your menu, inventory, and business information such as tax rate and monetary symbol.

	https://admin.lavu.com


Over the next few days you’ll receive follow-up emails and a phone call from one of our Support Agents to ensure that your Free Trial is going well. Questions? Call our 24/7 Support Line at 855-528-8457 anytime.

Thank you and enjoy your free trial

Start Exploring Now


--

** Support from Lavu — always free and 24/7 **

Need further assistance? Contact us at 855-528-8457, email support@lavu.com, or visit our help center support.lavu.com.
