<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

	<title>POS Lavu iPad Point of Sale  | New Signup</title>
	<link rel="stylesheet" href="/styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="/styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" href="/styles/register.css" type="text/css" media="screen" title="no title" charset="utf-8">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
	<script src="/scripts/jquery.tools.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="/scripts/jquery.validate.js" type="text/javascript" charset="utf-8"></script>


	<style type="text/css" media="screen">

	/* STARTER MENU ICONS --------------------------- */
	.boxlinks {
		color: #fff;
		font-size:14px;
		font-weight:600;
	}
	.boxlinks a:link{
		color: #fff;
		text-decoration: none;
		font-size:14px;
		font-weight:600;
	}
	.boxlinks a:hover {
		color: #505050;
		text-decoration: none;
		font-size:14px;
		font-weight:600;
	}

	.examtext {
		color: #919191;
		font-size:10px;
		padding:5;
	}

	.maintextlight {
		color: #919191;
		font-size:12px;


	}
	.boxtitle {
		color: #acacac;
		font-size:14px;
		font-weight:100;
		line-height:20px;
	}

	/* LINKS FOR STARTER MENU BUTTONS --------------------------- */
	.select_link {
		background-image:url(/images/btn_100x35.png);
		width:100px;
		height:26px;
		text-align:center;
		vertical-align:middle;
		padding-top:10px;
		color: #fff;
	}

	.select_link a{
		color: #fff;
		font-size:12px;
		padding:5;
	}

	.select_link a:hover {
		font-weight:700;
		text-decoration: none;
	}

	.radioMenu {

		display: none;
	}

	.highlight {
		background-color: #dddddd;
		-webkit-border-top-left-radius: 4px;
		-webkit-border-top-right-radius: 4px;
		-webkit-border-top-bottom-radius: 4px;
		-webkit-border-top-bottom-radius: 4px;
		-moz-border-radius-topleft: 4px;
		-moz-border-radius-topright: 4px;
		border-radius: 4px;
  	}

	.highlight span.boxtitle {

		color: #ffffff;
	}

	.tooltip {
		display:none;
		background:transparent url(/images/black_arrow.png);
		font-size:12px;
		height:70px;
		width:160px;
		padding:25px;
		color:#fff;
		border: none;
	}

	label.error {
		color: #CD2626;
		font-size: 11px;
		display: none;
	}
	input.error, input.error:focus { border-color: #CD2626; }

	div.server_error { color: #CD2626; background: #fff; height  }
	div.server_error h3{ color: #CD2626; font-size: 18px;  }

	.app-submit-button.new-colors {
		background-color: #a8c635;
		background: -moz-linear-gradient(100% 100% 90deg, #809628, #a8c635);
		background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#A8C635), to(#809628));
	}

	html {
		background-color: #f2f2f2;
	}
	body {
		background-color: #f2f2f2;
	}

	body #app_content div.form_container {
		border-color:#ddd;
	}

	</style>

	<script>

		$(document).ready(function() {
			var select_function = function() { //check for the second selection

				var column = $(this).attr('title'); // assign the ID of the column
				if (!column || column == '') {
					column = $(this).children('a').attr('title');
				}

				//console.log();

				$('table.start_menu').parent().closest("td").removeClass("highlight"); //forget the last highlighted column
				$('table.start_menu').parent().closest("td."+column).addClass("highlight"); //highlight the selected column
				$('table.start_menu').parent().closest("td."+column).find(":radio").attr("checked","checked");
				return false;
			}

			$(".select_link").click(select_function);
			$("a.menu").click(select_function);
			$(".select_link").css({cursor:'pointer'});

			// highlight radio buttons
			$('table.start_menu').children().find('img').tooltip();

			// validation
			// validate signup form on keyup and submit
			$("#new_customer").validate({
				rules: {
					card_number: "required",
					card_expiration_mo: "required",
					card_expiration_yr: "required",
					card_code: "required",
					agree_tos: "required",
					firstname: "required",
					lastname: "required",
					city: "required",
					address: "required",
					zip: "required"

				},
				messages: {
					card_number: "Please enter a valid credit card number",
					card_expiration_mo: "Please enter a valid credit card expiration month",
					card_expiration_yr: "Please enter a valid credit card expiration year",
					card_code: "Please enter a valid credit card code",
					agree_tos: "Please agree to our Terms of Service",
					firstname: "Please enter your first name",
					lastname: "Please enter your last name",
					city: "Please enter a valid city",
					address: "Please enter a valid address",
					zip: "Please enter a valid zip code"
				},
				ignore: ":hidden"
			});

		});

	</script>
<script type='text/javascript'>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-30186945-1']);
			_gaq.push(['_setDomainName', 'poslavu.com']);
			_gaq.push(['_trackPageview']);
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

</script>
	</head>

<body style="padding-top:20px;">
	<div id="app_content_wrapper">
		<div id="app_content" class="clearfix">

			<!-- <div class="tooltips"></div>  -->
				<?= $error_content ?>
				<?php
					$a_getvars = array();
					foreach($_GET as $k=>$v)
						$a_getvars[] = "$k=$v";
					if (isset($_GET['no_lead']) && $_GET['no_lead']) {
						?>
						<div id="no_lead" style="position:fixed; top:5px; left:5px; background-color:rgba(0,0,0,0.7); border:1px solid white; border-radius:5px; color:white; padding:5px;">
							No lead information will be generated for this signup.
						</div>
						<?php
					}
				?>

			<form action="<?= $_SERVER['PHP_SELF'].'?'.implode('&',$a_getvars); ?>" class="frm customer" id="new_customer" method="POST">
				<?php
				if (isset($rowid)) // from auth_account.php
					$_POST['rowid'] = $rowid;
				if (!isset($_POST['reseller_leads_rowid']))
					$_POST['reseller_leads_rowid'] = $i_reseller_leads_id;
				if (!isset($_POST['posted']))
					$_POST['posted'] = 1;
				if (!isset($_POST['company']))
					$_POST['company'] = $_POST['company_name'];
				if (!isset($_POST['full_name']))
					$_POST['full_name'] = '';
				$a_names = explode(' ', $_POST['full_name'], 2);
				if (!isset($_POST['firstname']))
				    $_POST['firstname'] = $a_names[0];
				if (!isset($_POST['lastname']))
					$_POST['lastname'] = $a_names[1];
				foreach($_POST as $k=>$v) {
					echo '<input type="hidden" name="'.$k.'" value="'.$v.'" />
				';
				}
				?>

				<div class="left leftcol">

					<table style="border:0px solid none; display:block;">
						<tr style="display:inline-table;">
							<td style="text-align:left;" id="app_header">
								<h1 id="logo" style="width:130px;">POSLavu</h1>
							</td>
							<td style="padding:10px; border-right:1px solid #fff;"></td>
							<td style="padding:10px;"></td>
							<td style="text-align:left">
								<div style="color:#919191; font-size:24px;"><?= $o_package_container->get_printed_name_by_attribute('name',$_REQUEST['package']) ?> Account</div>
								<br />
								<h2 style="color:#bbb;text-align:center;">Step 2 of 2...</h2>
							</td>
						</tr>
					</table>

					<div style="margin-top:100px;">
						<div class="billing_info">
							<img src="/images/header-14dayFree.png" alt="14-day Free Trial" />
						</div>
						<p style="font-family:Arial, Helvetica, sans-serif; color:#999; font-size:11px">&nbsp;</p>

						<div style="font-family:Arial, Helvetica, sans-serif; color:#999; font-size:11px">
							<p><span style="font-family:Arial, Helvetica, sans-serif; color:#747474; font-size:16px">YOU CAN CANCEL AT ANY TIME</span><br />
							If you do not wish to continue using our services, you can login to your backend poslavu to cancel it yourself at anytime.</p>
							<p>If you wish to continue using our services, your account will automatically be charged for the license amount and the hosting fee will be charged on a monthly basis.</p>

							<p><span style="font-family:Arial, Helvetica, sans-serif; color:#747474; font-size:16px">YOU WILL SEE A VERIFICATION/AUTHENTICATION</span><br />
							We need your billing information to reduce fraud and verify you have a valid credit card should you keep your account open. This prevents any interruption in service.</p>
							<p>To verify a valid credit card, your bank will temporarily show an authorization for validation purposes only. This amount will NOT be posted/charged to your account during the Trial Period.</p>
						</div>
					</div>
				</div>

				<div class="right" id="signupccinfo">

					<div class="form_container">
						<h3>Credit Card Information</h3>
						<ul>
							<li>
								<label for="card_number">Credit card number*</label>
								<input id="card_number" name="card_number" size="30" type="text" />
							</li>
							<li class="fieldsmall left expdate">
								<label for="card_expiration">Expires on*</label>
								<select id="card_expiration_mo" name="card_expiration_mo">
									<option value=""></option>
									<?= create_option_list("card_expiration_mo",array(
										"01"=>"1 - Jan",
										"02"=>"2 - Feb",
										"03"=>"3 - Mar",
										"04"=>"4 - Apr",
										"05"=>"5 - May",
										"06"=>"6 - Jun",
										"07"=>"7 - Jul",
										"08"=>"8 - Aug",
										"09"=>"9 - Sep",
										"10"=>"10 - Oct",
										"11"=>"11 - Nov",
										"12"=>"12 - Dec")
									); ?>
								</select>

								<select id="card_expiration_yr" name="card_expiration_yr">
									<option value=""></option>
						            <?php
						            	$a_years = array();
						            	for ($i = 0; $i < 11; $i++) {
											$year = date('Y', strtotime('+'.$i.' years'));
											$a_years[$year] = $year;
										}
						            	echo create_option_list("card_expiration_yr", $a_years); ?>
								</select>

							</li>
							<li class="fieldsmall right ccvinput">
								<label for="card_code">Security Code (CVV)*</label>
								<input id="card_code" class="inputsmall" name="card_code" size="30" type="text" value="<?= $_POST['card_code']?>" />
							</li>
						</ul>
					</div>

					<div class="form_container">
						<h3>Billing Information</h3>
						<ul>

							<li class="fieldsmall fieldleft">
								<label for="firstname">First Name *</label>
								<input id="firstname" class="inputsmall" name="firstname" size="30" type="text" value="<?= $_POST['firstname'] ?>" />
							</li>
							<li class="fieldsmall right">
								<label for="lastname">Last Name *</label>
								<input id="lastname" class="inputsmall" name="lastname" size="30" type="text" value="<?= $_POST['lastname'] ?>" />
							</li>

							<li class="fieldsmall fieldleft">
								<label for="city">City *</label>
								<input id="city" class="inputsmall" name="city" size="30" type="text" value="<?= $_POST['city'] ?>" />
							</li>
							<li class="fieldsmall right">
								<label for="state">State</label>
								<input id="state" class="inputsmall" name="state" size="30" type="text" value="<?= $_POST['state'] ?>" />
							</li>

							<li>
								<label for="address">Address*</label>
								<input id="address" name="address" size="30" type="text" value="<?= $_POST['address'] ?>" />
							</li>

							<li class="fieldsmall fieldleft">
								<label for="zip">Zip Code *</label>
								<input id="zip" class="inputsmall" name="zip" size="30" type="text" value="<?= $_POST['zip'] ?>" />
							</li>
							<li class="fieldsmall right">
								<label for="promo">Promo Code</label>
								<input id="promo" class="inputsmall" name="promo" size="30" type="text" value="<?= $_POST['promo'] ?>" />
							</li>

						</ul>
					</div>

				</div>

				<p class="agree right"><input type="checkbox" name="agree_tos" value="1" id="agree_tos" />
					By checking here you agree to our <a href="http://www.poslavu.com/en/tos" target="_blank">Terms of Service</a>.
				</p>

				<?php
				if ($b_weekly_signups || $b_failed_ccs) {
					$public_key = "6LfrtOsSAAAAAAIvqmehbwGW6vBo8GI1qGL_ngVR";
					echo str_replace("http", "https", recaptcha_get_html($public_key));
				}
				?>

				<input class="app-submit-button new-colors" id="subscription_submit" name="finish_signup" type="submit" value="Create my account &rsaquo;" onclick="_gaq.push(['_trackEvent', 'Button complete', 'clicked', 'Complete signup <?= $_REQUEST['level'] ?>']);"/>
			</form>
		</div>
	</div>
</body>
</html>