<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

  <title>POS Lavu iPad Point of Sale  | New Signup</title>
  <link rel="stylesheet" href="/styles/reset.css" type="text/css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="/styles/global.css" type="text/css" media="screen" title="no title" charset="utf-8">
  <link rel="stylesheet" href="/styles/register.css" type="text/css" media="screen" title="no title" charset="utf-8">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
  <script src="/scripts/jquery.tools.min.js" type="text/javascript" charset="utf-8"></script>
  <script src="/scripts/jquery.validate.js" type="text/javascript" charset="utf-8"></script>


  <style type="text/css" media="screen">

	/* STARTER MENU ICONS --------------------------- */
	.boxlinks {
		color: #fff;
		font-size:14px;
		font-weight:600;
	}
	.boxlinks a:link{
		color: #fff;
		text-decoration: none;
		font-size:14px;
		font-weight:600;
	}
	.boxlinks a:hover {
		color: #505050;
		text-decoration: none;
		font-size:14px;
		font-weight:600;
	}

	.examtext {
		color: #919191;
		font-size:10px;
		padding:5;
	}

	.maintextlight {
		color: #919191;
		font-size:12px;


	}
	.boxtitle {
		color: #acacac;
		font-size:14px;
		font-weight:100;
		line-height:20px;
	}

	/* LINKS FOR STARTER MENU BUTTONS --------------------------- */
	.select_link {
		background-image:url(/images/btn_100x35.png);
		width:100px;
		height:26px;
		text-align:center;
		vertical-align:middle;
		padding-top:10px;
		color: #fff;
	}

	.select_link a{
		color: #fff;
		font-size:12px;
		padding:5;
	}

	.select_link a:hover {
		font-weight:700;
		text-decoration: none;
	}

	.radioMenu {

		display: none;
	}

  .highlight {
    	background-color: #dddddd;
	    -webkit-border-top-left-radius: 4px;
	    -webkit-border-top-right-radius: 4px;
	    -webkit-border-top-bottom-radius: 4px;
	    -webkit-border-top-bottom-radius: 4px;
	    -moz-border-radius-topleft: 4px;
	    -moz-border-radius-topright: 4px;
	    border-radius: 4px;
  	}

	.highlight span.boxtitle {

		color: #ffffff;
	}

	.starter_menu_image {
		position: relative;
		display: inline-block;
	}
	.starter_menu_image:hover .tooltip {
		display: block;
	}
	.starter_menu_image:hover .tooltip:hover {
		display: none;
	}
	.tooltip {
		position: absolute;
		z-index: 1;
		top: -120px;
		left: -56px;
		display:none;
		background:transparent url(/images/black_arrow.png);
		font-size:12px;
		height:70px;
		width:160px;
		padding:25px;
		color:#fff;
		border: none;
	}

	label.error {
		color: #CD2626;
		font-size: 11px;
		display: none;
	}
    input.error, input.error:focus { border-color: #CD2626; }

	div.server_error { color: #CD2626; background: #fff; height  }
	div.server_error h3{ color: #CD2626; font-size: 18px;  }

	.app-submit-button.new-colors {
		background-color: #a8c635;
		background: -moz-linear-gradient(100% 100% 90deg, #809628, #a8c635);
		background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#A8C635), to(#809628));
	}

	html {
		background-color: #f2f2f2;
	}
	body {
		background-color: #f2f2f2;
	}

	body #app_content div.form_container {
		border-color:#ddd;
	}

  </style>

  <script>

	var select_function = function(column) {

		$('table.start_menu').parent().closest("td").removeClass("highlight"); //forget the last highlighted column
		$('table.start_menu').parent().closest("td."+column).addClass("highlight"); //highlight the selected column
		$('table.start_menu').parent().closest("td."+column).find(":radio").attr("checked","checked");
		return false;
	}

	$(document).ready(function() {

		//$(".select_link").click(select_function);
		var jmenus = $("a.menu");
		var menu_click = function(k, menu_element) {
			var jmenu_element = $(menu_element);
			var title = jmenu_element.attr("title");
			var jtable_parent = jmenu_element.parent().parent().parent().parent();
			jtable_parent.css({cursor: 'pointer'});
			jtable_parent.click(function(){
				select_function(title);
			});
		};
		$.each(jmenus, menu_click);

		$.validator.addMethod("englishChars", function(value, element) {
			// support not requiring this method for validation
			if (this.optional(element))
				return true;

			var matchVal = value.match(/^[a-zA-Z0-9 '\@-]*$/);
			if (!$.isArray(matchVal) || matchVal[0] != value)
				return false;
			return true;
		});

		// validation
		// validate signup form on keyup and submit
		$("#new_customer").validate({
			rules: {
				mname: "required",
				company_name:  {
					required: true,
					englishChars: true
				},
				phone:  {
					required: true,
					englishChars: true
				},
				email: {
					required: true,
					email: true
				},
				firstname:  {
					required: true,
					englishChars: true
				},
				lastname:  {
					required: true,
					englishChars: true
				},
				city:  {
					required: true,
					englishChars: true
				},
				state:  {
					required: true,
					englishChars: true
				},
				address:  {
					required: true,
					englishChars: true
				},
				zip:  {
					required: true,
					englishChars: true
				},
				promo:  {
					required: false,
					englishChars: true
				},
			},
			messages: {
				company_name: {
					required: "Company name is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				phone: {
					required: "Phone number is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				email: {
					required: "Please enter a valid email address",
					email: "Must be valid email address",
				},
				lastname: {
					required: "Phone is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				firstname: {
					required: "First name is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				city: {
					required: "City is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				state: {
					required: "State is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				address: {
					required: "Address is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				zip: {
					required: "Zip code is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				promo: {
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				},
				full_name: {
					required: "Your full name is required",
					englishChars: "Can only include letters, numbers, spaces, and the symbols ('-@).",
				}
			}
		});

		// highlight the menu name that has been chosen
		var jmname = $("[name=mname]:checked");
		if (jmname.length > 0) {

			// find the parent of an element based on an .is() selector
			// @return: the jparent, or $('div') if the parent can't be found
			function get_parent(jelement, parent_selector) {
				var jparent = jelement.parent();
				if (jparent.is(parent_selector))
					return jparent;
				if (jparent.is('document'))
					return jparent;
				return get_parent(jparent, parent_selector);
			}

			// get the td of the mname and give it the highlight class
			var jtd = get_parent(get_parent(jmname, 'table'), 'td');
			if (jtd.is('td')) {
				if (!jtd.hasClass('highlight')) {
					jtd.addClass('highlight');
				}
			}
		}

	});

	</script>
<script type='text/javascript'>
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-30186945-1']);
			_gaq.push(['_setDomainName', 'poslavu.com']);
			_gaq.push(['_trackPageview']);
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

</script>
</head>

<body>
  <div id="app_content_wrapper">
    <div id="app_header">
      <h1 id="logo">POSLavu</h1>
    </div>
    <div id="app_content" class="clearfix">



<h2 class="signup_tagline">Signup instantly for a POS Lavu account.</h2>
<h3 class="signup_tagline_2">No Contracts. Cancel at Any Time.</h3>
<h2 style="color:#bbb;text-align:center;">Step 1 of 2...</h2>

<!-- <div class="tooltips"></div>  -->
 	<?= $content ?>
 	<?php
 		$a_getvars = array();
 		foreach($_GET as $k=>$v)
 			$a_getvars[] = "$k=$v";

 		if (isset($_GET['no_lead']) && $_GET['no_lead']) {
 			?>
 			<div id="no_lead" style="position:fixed; top:5px; left:5px; background-color:rgba(0,0,0,0.7); border:1px solid white; border-radius:5px; color:white; padding:5px;">
 				No lead information will be generated for this signup.
 			</div>
 			<?php
 		}
 	?>

<form action="<?php echo str_replace("step1{$s_dev}", "step2{$s_dev}", $_SERVER['PHP_SELF']).'?'.implode('&',$a_getvars); ?>" class="frm customer" id="new_customer" method="POST">
	<input type="hidden" name="package" value="<?= $_REQUEST['level'] ?>" />
	<input type="hidden" name="level" value="<?= $_REQUEST['level'] ?>" />
	<input type="hidden" name="rowid" value="<?= isset($rowid)?$rowid:0 ?>" />
	<input type="hidden" name="locale" value="<?= $_GET['loc'] ?>" />
	<input type="hidden" name="no_lead" value="<?= (isset($_GET['no_lead']) ? $_GET['no_lead'] : '0') ?>" />
	<input type="hidden" name="promo" value="<?= (isset($_REQUEST['promo']) ? $_REQUEST['promo'] : '') ?>" />

<!-- <table><tr><td> -->
<div class="left leftcol">
	<div class="form_container">
		<h3>Company Information</h3>
		<ul>
			<li>
				<label for="company_name">Company Name *</label>
				<input id="company_name" name="company_name" size="30" type="text" value="<?= $_REQUEST['company_name'] ?>" />
			</li>

			<li class="fieldsmall fieldleft">
				<label for="phone">Phone *</label>
				<input class="inputsmall" id="phone" name="phone" size="30" type="text" value="<?= $_REQUEST['phone'] ?>" />
			</li>
			<li class="fieldsmall right">
				<label for="email">Email *</label>
				<input class="inputsmall" id="email" name="email" size="30" type="text" value="<?= $_REQUEST['email'] ?>" />
			</li>

			<li class="fieldsmall fieldleft">
				<label for="zip">Zip Code *</label>
				<input class="inputsmall" id="zip" name="zip" size="30" type="text" value="<?= $_REQUEST['zip'] ?>" />
			</li>
			<li class="fieldsmall right">
				<label for="full_name">Full Name</label>
				<input class="inputsmall" id="full_name" name="full_name" size="30" type="text" value="<?= $_REQUEST['full_name'] ?>" />
			</li>
		</ul>
	</div>
</div>

<!-- </td><td> -->

<div class="right" id="signupccinfo">
	<div class="form_container" id="myplan">
		<h3>Your Plan: <span class="highlighted">Lavu <?= $o_package_container->get_printed_name_by_attribute('name', $_REQUEST['level']); ?></span></h3>
		<!--<a href="http://poslavu.com/signup">Change Plan</a> -->
	</div>

  <!-- starter menu selections -->
  <div class="form_container">
    <h3><?= $b_is_niche ? 'Your Starter Menu' : 'Choose Starter Menu' ?></h3>
    <table width="360" border="0" cellspacing="0" cellpadding="10" id="boxlinks">

		<?php
		/***********************************************************************
		 *                    D R A W   T H E   M E N U
		 **********************************************************************/
		ob_start();
		?>
		<tr>
			<td width="180" height="140" align="center" valign="top" class="1 <?= $_POST['mname']=='default_restau'?'highlight':'' ?>">
				<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
					<tr>
						<td width="120" align="center" valign="middle" >
							<div class="starter_menu_image">
								<img src="/images/type_3.png" width="100" height="60" />
								<div class="tooltip">This Restaurant starter menu uses a table layout.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" >
							<span class="boxtitle">Restaurant</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" class="examtext">
							<div class="select_link">
								<input class="radioMenu" name="mname" value="default_restau" type="radio" <?= $_POST['mname']=='default_restau'?'checked':'' ?> />
								<a href="#" class="signup menu" title="1">SELECT</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="180" height="140" align="center" valign="top" class="2 <?= $_POST['mname']=='default_pizza_'?'highlight':'' ?>">
				<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
					<tr>
						<td width="120" align="center" valign="middle">
							<div class="starter_menu_image">
								<img src="/images/type_4.png" width="100" height="60" />
								<div class="tooltip">This Pizza Shop starter menu uses a table layout and provides some special functionality.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" >
							<span class="boxtitle">Pizza Shop</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" class="examtext" >
							<div class="select_link">
								<input class="radioMenu" name="mname" value="default_pizza_" type="radio" <?= $_POST['mname']=='default_pizza_'?'checked':'' ?> />
								<a href="#" class="signup menu" title="2">SELECT</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="180" height="140" align="center" valign="top" class="3 <?= $_POST['mname']=='defaultbar'?'highlight':'' ?>">
				<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
					<tr>
						<td width="120" align="center" valign="middle">
							<div class="starter_menu_image">
								<img src="/images/type_2.png" width="100" height="60" />
								<div class="tooltip">This Bar/Lounge starter menu uses a tab layout. Tabs are not tethered to a table.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" >
							<span class="boxtitle">Bar/Lounge</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" class="examtext">
							<div class="select_link">
								<input class="radioMenu" name="mname" value="defaultbar" type="radio" <?= $_POST['mname']=='defaultbar'?'checked':'' ?> />
								<a href="#" class="signup menu" title="3">SELECT</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="180" height="140" align="center" valign="top" class="4 <?= $_POST['mname']=='default_coffee'?'highlight':'' ?>">
				<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu">
					<tr>
						<td width="120" align="center" valign="middle">
							<div class="starter_menu_image">
								<img src="/images/type_1.png" width="100" height="60" />
								<div class="tooltip">This Coffee Shop starter menu is best for quick serve environments generally using single transactions.</div>
							</div>
						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" >
							<span class="boxtitle">Coffee Shop</span>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top" class="examtext">
							<div class="select_link">
								<input class="radioMenu" name="mname" value="default_coffee" type="radio" <?= $_POST['mname']=='default_coffee' ? 'checked' : '' ?> />
								<a href="#" class="signup menu" title="4">SELECT</a>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<?php
			$s_non_niche = ob_get_contents();
			ob_end_clean();
			ob_start();
		?>
		<tr>
			<td width="180" height="140" align="center" valign="top" class="4 highlight" style="background-color:white;">
				<div style="background-color:#ddd; margin-left:-10px; margin-top:-10px; width:180px; height:140px; border:1px solid #ddd; border-radius:4px">
					<table width="120" border="0" cellspacing="0" cellpadding="0" class="start_menu" style="margin: 0 auto;">
						<tr>
							<td width="120" align="center" valign="middle">
								<div class="starter_menu_image">
									<img src="<?= $s_niche_image ?>" width="100" height="60" />
									<div class="tooltip"><?= $s_niche_title ?></div>
								</div>
							</td>
						</tr>
						<tr>
							<td align="center" valign="middle" >
								<span class="boxtitle">&nbsp;</span>
							</td>
						</tr>
						<tr>
							<td align="center" valign="top" class="examtext">
								<div class="select_link">
									<input class="radioMenu" name="mname" value="<?= $_POST['mname'] ?>" type="radio"<?= $b_is_niche ? ' CHECKED' : '' ?> />
									<a href="#" class="signup menu" title="4"><?=  $s_niche_text ?></a>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<?php
			$s_with_niche = ob_get_contents();
			ob_end_clean();
			if ($b_is_niche)
			echo $s_with_niche;
			else
			echo $s_non_niche;
		?>

	  <tr>
	  	<td colspan="2" align="center"><label for="mname" class="error" style="font-size:18px;">Please choose a starter menu.</label>
		</td>
	  </tr>
	  <tr>
	    <td colspan="2" align="center">
	    <span class="maintextlight">A Starter Menu is only used as a guide and does not affect any future menu setup decisions. You will be able to insert your own menu after this initial setup.</span></td>
	  </tr>
	</table>
  </div>
  <!-- end starter menu table -->
</div>

<!--</td></tr></table>-->

  <input class="app-submit-button new-colors" id="subscription_continue1" name="continue1" type="submit" value="Continue &rsaquo;" onclick="_gaq.push(['_trackEvent', 'Button continue', 'clicked', 'continue signup <?= $_REQUEST['level'] ?>']);"/>

</form>

    </div>
  </div>
</body>
</html>