<?php
header("Access-Control-Allow-Origin: *");
require_once(__DIR__.'/BaseTest.php');
require_once('/home/poslavu/public_html/admin/api_private/api.php');
require_once('/home/poslavu/public_html/admin/api_private/api_includes.php');


class location extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

        $this->connectToDatabase();

        //$this->refreshTableSchema('menu_items');

    }

	protected function runTests(){

        $action = $_REQUEST['action'];
        if($action == 'getDataName') {
           $lavutogoname = $_REQUEST['lavutogoname'];
           $ltgName = array("lavu_togo_name" => $lavutogoname);
           $apiRequest = array();
           $apiRequest['Auth']['dataname'] = "MAIN";
           $apiRequest['Auth']['user_id'] = 1;
           $apiRequest['Action'] = $action;
           $apiRequest['Params'] = array();
           $apiRequest['Params']['lavu_togo_name'] = $ltgName;
           $apiRequest['Component'] = 'Location';
           $apiRequestJSON = json_encode($apiRequest);
           Internal_API::apiQuery($apiRequestJSON);
        } else if($action == 'getLocationDetail') {
            $lavutogoname = $_REQUEST['lavutogoname'];
            $ltgName = array("lavu_togo_name" => $lavutogoname);
            $apiRequest = array();
            $apiRequest['Auth']['dataname'] = "MAIN";
            $apiRequest['Auth']['user_id'] = 1;
            $apiRequest['Action'] = $action;
            $apiRequest['Params'] = array();
            $apiRequest['Params']['streetAddress'] = $ltgName;
            $apiRequest['Component'] = 'Location';
            $apiRequestJSON = json_encode($apiRequest);
            Internal_API::apiQuery($apiRequestJSON);
        } else if($action == "getLocationConfig") {
    	 	$apiRequest = array();
    		$apiRequest['Auth']['dataname'] = "demo_training_1";
    		$apiRequest['Auth']['user_id'] = 1;
    		$apiRequest['Action'] = 'getLocationConfig';
    		$apiRequest['Params'] = array();
    		$apiRequest['Component'] = 'Location';
    		$apiRequestJSON = json_encode($apiRequest);
    		Internal_API::apiQuery($apiRequestJSON);
        } else if($action == "getLocationAddress") {
            echo $_COOKIE['lavutogoname'];
    	    $lavu_togo_name = array("lavu_togo_name" => "leornadospizza");
            $lavu_togo_name = array("lavu_togo_name" => $_COOKIE['lavutogoname']);
            $searchStr = array("searchStr" => "");
            $apiRequest = array();
            $apiRequest['Auth']['dataname'] = "MAIN";
            $apiRequest['Auth']['user_id'] = 1;
            $apiRequest['Action'] = $action;
            $apiRequest['Params'] = array();
            $apiRequest['Params']['lavu_togo_name'] = $lavu_togo_name;
            $apiRequest['Component'] = 'Location';
            $apiRequestJSON = json_encode($apiRequest);
            Internal_API::apiQuery($apiRequestJSON);

        } else {
	        $streetAddress = $_REQUEST['streetadd'];
            $lavutogoname = $_REQUEST['lavutogoname'];
            $streetAddressArr = array("lavu_togo_name" => $lavutogoname, "streetAddress" => $streetAddress, "maxDist" => "8", "maxTime" => "650", "DeliveryFees" => "$50");
            $apiRequest = array();
            $apiRequest['Auth']['dataname'] = "MAIN";
            $apiRequest['Auth']['user_id'] = 1;
            $apiRequest['Action'] = $action;
            $apiRequest['Params'] = array();
            $apiRequest['Params']['streetAddress'] = $streetAddressArr;
            $apiRequest['Component'] = 'Location';
            $apiRequestJSON = json_encode($apiRequest);
            Internal_API::apiQuery($apiRequestJSON);

        }

	}

	protected function tearDown(){

	}

}

$obj = new location();
