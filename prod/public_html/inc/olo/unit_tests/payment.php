<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1); 
// error_reporting(E_ALL ^ E_NOTICE);
header("Access-Control-Allow-Origin: *");
require_once(__DIR__.'/BaseTest.php');
require_once('/home/poslavu/public_html/admin/api_private/api.php');
require_once('/home/poslavu/public_html/admin/api_private/api_includes.php');

class payment extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

		$this->connectToDatabase();

		// $this->refreshTableSchema('orders_api');
		$this->refreshTableSchema('menu_items');
		$this->refreshTableSchema('orders');

	}

	protected function run(){
        $action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
        // $action='getTokenForPayment';
		$dataname = "demo_training_1";
        $apiRequest = array();
        if($action == "getTokenForPayment") {
			$dataname = "lavutogo";
            $apiRequest['Auth']['dataname'] = $dataname;
            // $apiRequest['Auth']['user_id'] = 1;
            $apiRequest['Action'] = $action;
            $apiRequest['Component'] = 'Payment';
            $apiRequestJSON = json_encode($apiRequest);
            Internal_API::apiQuery($apiRequestJSON);			

        }
        if($action == "initiateWalletPayment") {
			$dataname = "demo_training_1";
            $apiRequest['Auth']['dataname'] = $dataname;
			$apiRequest['Action'] = $action;
            $apiRequest['Component'] = 'Payment';
            $apiRequestJSON = json_encode($apiRequest);
            Internal_API::apiQuery($apiRequestJSON);
			$apiRequest['Params']['paymentDetails'] = $apiRequest;
        }
        
        if($action == "initiateCardPayment") {
			$dataname = "demo_training_1";
		    $apiRequest['Auth']['dataname'] = $dataname;
            $apiRequest['Action'] = $action;
            $apiRequest['Component'] = 'Payment';
            $apiRequestJSON = json_encode($apiRequest);
            // print_r($data);
           
            
            Internal_API::apiQuery($apiRequestJSON);
			$apiRequest['Params']['paymentDetails'] = $apiRequest;
        }
        
        if($action == "initiateCOD") {
			$dataname = "demo_training_1";
		    $apiRequest['Auth']['dataname'] = $dataname;
            $apiRequest['Action'] = $action;
            $apiRequest['Component'] = 'Payment';
			// $apiRequest['Params']['paymentDetails'] = $_POST;
            $apiRequestJSON = json_encode($apiRequest);
            // print_r($apiRequest);
            Internal_API::apiQuery($apiRequestJSON);
        }

    }
   
	protected function tearDown(){

	}

}

$obj = new payment();
