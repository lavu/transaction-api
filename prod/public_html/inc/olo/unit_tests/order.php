<?php
/*error_reporting(E_ALL);
ini_set("display_errors", 1); */
header("Access-Control-Allow-Origin: *");
require_once(__DIR__.'/BaseTest.php');
require_once('/home/poslavu/public_html/admin/api_private/api.php');
require_once('/home/poslavu/public_html/admin/api_private/api_includes.php');

class order extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

		$this->connectToDatabase();

		// $this->refreshTableSchema('orders_api');
		$this->refreshTableSchema('menu_items');
		$this->refreshTableSchema('orders');

	}

	protected function run(){
		
		$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
		$dataname = "demo_training_1";
		$apiRequest = array();

		if($action == "getOrderHistory") {
			$dataname = "lavutogo";
			$userid = $_REQUEST['user_id'];

			$userDetails = array('user_id' => $userid, 'loc_id' => 1);
			//$action = 'myOrderHistory';
			$orderDetails = array();
			$ItemsDetails = array();

			$apiRequest['Params']['userDetails'] = $userDetails;

		}
		else if($action == "reOrder")
		{
			$orderid = $_REQUEST['order_id'];
			$userid = $_REQUEST['user_id'];
			$dineintype = $_REQUEST['type'];
				
			$orderDetails = array('user_id' => $userid,'order_id' => $orderid, 'dinetype'=>$dineintype,'loc_id' => 1);
				
			$apiRequest['Params']['orderDetails'] = $orderDetails;
		}
		else if($action == "getItemTaxDetails")
		{
			// api url: http://olo.poslavu.com/api/order.php?action=getItemTaxDetails&cart_contents=[{"itemid":773,"qty":1,"fmods_checked":[],"omods_checked":[]},{"itemid":971,"qty": 1,"fmods_checked":[390],"omods_checked":[]}]
					
			$orderitems = json_decode($_REQUEST['cart_contents']);
			//$dataname = $_REQUEST['ltg_name'];
			$orderDetails = array('loc_id' => 1);                                                               // here form order's id as order_id
			$ItemsDetails = array();
			$omods = array();
			
			foreach($orderitems as $orderitem){
			
				$ItemsDetails[] = array('itemid'=> $orderitem->itemid, 'qty' => $orderitem->qty, 'fmods_checked' => $orderitem->fmods_checked
						, 'omods_checked' => $orderitem->omods_checked );
			}
			
			$apiRequest['Params']['orderDetails'] = $orderDetails;
			$apiRequest['Params']['orderItemsDetails'] = $ItemsDetails;
		}
		else{
				
			// api url: http://olo.poslavu.com/api/order.php?order_id&cart_contents=[{"itemid": 773,"qty": 1,"fmods_checked": [],"omods_checked": [],"fmods":[], "omods":[], "special":""},{"itemid": 971,"qty": 1,"fmods_checked": [390],"omods_checked": [],"fmods":[390,391,392,393,394,395], "omods":[], "special":""}]
				
			$orderitems = json_decode($_REQUEST['cart_contents']);
			//$orderitems = json_decode($_REQUEST['cart_contents']);
			$orderid = $_REQUEST['order_id'];
			//$dataname = $_REQUEST['ltg_name'];
			$orderDetails = array('order_id' => $orderid, 'loc_id' => 1);                                                               // here form order's id as order_id
			$ItemsDetails = array();
			$omods = array();

			foreach($orderitems as $orderitem){

				$ItemsDetails[] = array('itemid'=> $orderitem->itemid, 'qty' => $orderitem->qty, 'fmods' => $orderitem->fmods , 'fmods_checked' => $orderitem->fmods_checked
						, 'omods' => $orderitem->omods , 'omods_checked' => $orderitem->omods_checked , 'special' => $orderitem->special );
			}

			$action = 'myOrderitems';
			/*if(isset($_REQUEST['loyaltyDetails']) && !empty($_REQUEST['loyaltyDetails']))
				$apiRequest['Params']['loyaltyDetails'] = array("cardnumber"=> "143143","discount_id"=>10,"discount_value"=>4,"discount_points"=> 20,"awardpoints"=>"","loyalty_type"=>"d"); // hardcoded for loyalty points
			*/
			
			$apiRequest['Params']['dataname'] = $dataname;
			$apiRequest['Params']['orderDetails'] = $orderDetails;
			$apiRequest['Params']['orderItemsDetails'] = $ItemsDetails;
		}

		 
		$apiRequest['Auth']['dataname'] = $dataname;
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = $action;
		$apiRequest['Component'] = 'Orders';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);

	}
	protected function tearDown(){

	}

}

$obj = new order();