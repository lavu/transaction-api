 <?php
/*  error_reporting(E_ALL);
 ini_set('display_errors', 1); */
header("Access-Control-Allow-Origin: *");
require_once(__DIR__.'/BaseTest.php');
require_once('/home/poslavu/public_html/admin/api_private/api.php');
require_once('/home/poslavu/public_html/admin/api_private/api_includes.php');

class customers extends BaseTest{

	public function __construct(){
		parent::__construct();
	}
	
	protected function buildUp(){
		$this->connectToDatabase();
		$this->refreshTableSchema('users');
	}
	
	protected function run(){
	    $call_function =  $_REQUEST['action'];
	    $dataName = array("dataname" => "MAIN");
	    $dtime = date("Y-m-d H:i:s");
		if ($call_function == "userSignin"){
			$signInDetails =array("email"=>$_REQUEST['email'],"password" =>$_REQUEST['password']);
			$apiRequest = array();
			$apiRequest['Auth']['dataname'] ="demo_training_1";
			$apiRequest['Auth']['user_id'] = 1;
			$apiRequest['Action'] = 'userSignin';
			$apiRequest['Params'] = array();
			$apiRequest['Params']['dataname'] =$dataName;
			$apiRequest['Params']['signInDetails'] =$signInDetails;
			$apiRequest['Component'] = 'Customers';
			$apiRequestJSON = json_encode($apiRequest);
			$result=Internal_API::apiQuery($apiRequestJSON);

		} else if ($call_function == "userRegistration"){
			$regDetails= array(
					"email"=>$_REQUEST['email'],
					"password" =>md5($_REQUEST['password']),
					"fname"=>$_REQUEST['fname'],
					"lname"=>$_REQUEST['lname'],
			        "mobile"=>$_REQUEST['mobile'],
					"login_attempt"=> 0,
					"login_status"=> 0,
					"created_date"=> $dtime,
					"updated_date"=> $dtime,
					"_deleted" => 0
				    );
			$cpwd=array("cpwd"=>$_REQUEST['cpwd']);
			$apiRequest = array();
			$apiRequest['Auth']['dataname'] ="demo_training_1";
			$apiRequest['Auth']['user_id'] = 1;
			$apiRequest['Action'] = 'userRegistration';
			$apiRequest['Params'] = array();
			$apiRequest['Params']['dataname'] =$dataName;
			$apiRequest['Params']['regDetails'] = $regDetails;
			$apiRequest['Params']['cpwd'] = $cpwd;
			$apiRequest['Component'] = 'Customers';
			$apiRequestJSON = json_encode($apiRequest);
			$response=Internal_API::apiQuery($apiRequestJSON);
		} 
		else if ($call_function == "myAccount"){
			$dataName = array("dataname" => "MAIN");
			$user_info=array( "user_id" =>$_REQUEST['userid'],
					"first_name"=>$_REQUEST['fname'],
					"last_name"=>$_REQUEST['lname'],
					"email"=>$_REQUEST['email'],
					"address"=>$_REQUEST['address'],
					"city"=>$_REQUEST['city'],
					"state"=>$_REQUEST['state'],
					"zipcode"=>$_REQUEST['zipcode'],
					"phone"=>$_REQUEST['phone'],
					"mobile"=>$_REQUEST['mobile'],
					"preferred_language"=>$_REQUEST['preferred_language'],
					"curpwd"=>$_REQUEST['curpwd'],
					"newpwd"=>$_REQUEST['newpwd'],
					"conpwd"=>$_REQUEST['conpwd']
			);
			$apiRequest = array();
			$apiRequest['Auth']['dataname'] ="demo_training_1";
			$apiRequest['Auth']['user_id'] = 1;
			$apiRequest['Action'] = 'myAccount';
			$apiRequest['Params'] = array();
			$apiRequest['Params']['dataname'] =$dataName;
			$apiRequest['Params']['users_info'] = $user_info;
			$apiRequest['Component'] = 'Customers';
			$apiRequestJSON = json_encode($apiRequest);
			Internal_API::apiQuery($apiRequestJSON);
		
		}
     
	}
	
	protected function tearDown(){
	
	}

	

}

$obj = new customers();
