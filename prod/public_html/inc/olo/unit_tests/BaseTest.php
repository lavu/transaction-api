<?php
require_once('/home/poslavu/public_html/admin/api_private/api.php');
require_once('/home/poslavu/public_html/admin/api_private/api_includes.php');

abstract class BaseTest{

	protected function __construct(){
		//$this->buildUp();
		$this->run();
		$this->tearDown();
	}



	protected function connectToDatabase(){
		DBConnection::startAPITestingConnection();
		$clientConnection = DBConnection::getConnectionByType('client');
	}


	
	protected function runAPIRequest($request){

	}

	protected function assertStringEquals($testMessage, $expectedValue, $actualValue){
		$status = false;
		$status = $expectedValue == $actualValue;
		$statusMsg = $status ? "Passed" : "Failed";
		echo "------------------------------------\n";
		echo "Test: assertStringEquals\n";
		echo $testMessage.' -- '.$statusMsg."\n";
		echo "expected:".$expectedValue."\n";
		echo "actual:".$actualValue."\n";
		echo "------------------------------------\n";
	}

	protected function assertNotEmpty($testMessage, $value){
		$status = "Passed";
		if(empty($value)){
			$status = "Failed";
		}
		echo "------------------------------------\n";
		echo "Test: assertNotEmpty\n";
		echo $testMessage.' -- '.$status."\n";
		echo "------------------------------------\n";
	}

	protected function fail($message){
		echo "------------------------------------\n";
		echo "Fail\n";
		echo $message.' -- '."FAILED"."\n";
		echo "------------------------------------\n";
	}

	protected function refreshTableSchema($table){
		DBConnection::clientQuery("DROP TABLE `[1]`", $table);
		$showTableResult = DBConnection::adminQuery("SHOW CREATE TABLE `poslavu_INVENTORY_db`.`[1]`", $table);  // TO DO : change this to lavutogo-related db
		$showTableArr = mysqli_fetch_assoc($showTableResult);
		DBConnection::clientQuery($showTableArr['Create Table']);
	}

	protected abstract function buildUp();
	protected abstract function run();
	protected abstract function tearDown();

}
