<?php
header("Access-Control-Allow-Origin: *");
require_once(__DIR__.'/BaseTest.php');
require_once('/home/poslavu/public_html/admin/api_private/api.php');
require_once('/home/poslavu/public_html/admin/api_private/api_includes.php');

class menu extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

        $this->connectToDatabase();

        $this->refreshTableSchema('menu_groups');
        $this->refreshTableSchema('menu_categories');
        $this->refreshTableSchema('menu_items');

        }

	protected function run(){
        $dataname = $_REQUEST['dataname'];
        $apiRequest = array();
        $apiRequest['Auth']['dataname'] = $dataname;
        $apiRequest['Auth']['user_id'] = 1;
        $apiRequest['Action'] = 'getMenus';
        $apiRequest['Params'] = array();
        $apiRequest['Component'] = 'Menu';
        $apiRequestJSON = json_encode($apiRequest);
        Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}

}

$obj = new menu();
