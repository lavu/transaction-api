<?php
//header("Access-Control-Allow-Origin: *");
error_reporting(E_ALL);
ini_set("display_errors",1);
require_once(__DIR__.'/BaseTest.php');
require_once('/home/poslavu/public_html/admin/api_private/api.php');
require_once('/home/poslavu/public_html/admin/api_private/api_includes.php');


class lavugiftcard extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

        $this->connectToDatabase();
        //$this->refreshTableSchema('menu_items');
    }

	protected function run(){
		
		// sample url: http://olo.localhost/api/lavugiftcard.php?action=getLoyaltyPointDetails&lavutogoname=demo_training_1&lavuCard=143143&orderAmt=20

        $action = $_REQUEST['action'];
        $lavu_card_no = $_REQUEST['lavuCard'];
        $dataname = $_REQUEST['lavutogoname'];
        $orderAmt = $_REQUEST['orderAmt'];        
        $orderitems = json_decode($_REQUEST['items']);
        $ItemsDetails = array();
        
        foreach($orderitems as $orderitem){        		
        	$ItemsDetails[$orderitem->itemid] = $orderitem->qty;
        }
        
        $apiRequest = array();
        $apiRequest['Auth']['dataname'] = $dataname;
        $apiRequest['Auth']['user_id'] = 1;
        $apiRequest['Component'] = 'LavuGiftCard';
        
        if($action == 'getLavuGiftCardDetails') {
          
           $apiRequest['Action'] = $action;
           $apiRequest['Params'] = array("lavu_card" => $lavu_card_no, "dataname" => $dataname, "orderAmt" => $orderAmt);
                     
        }
        elseif ($action == 'getLoyaltyPointDetails'){
        	
        	$apiRequest['Action'] = $action;
        	$apiRequest['Params'] = array("lavu_card" => $lavu_card_no, "dataname" => $dataname, "orderAmt" => $orderAmt, "items" => $ItemsDetails);
        }

        $apiRequestJSON = json_encode($apiRequest);
        Internal_API::apiQuery($apiRequestJSON);
        
	}

	protected function tearDown(){

	}

}

$obj = new lavugiftcard();
