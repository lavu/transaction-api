<?php
/*error_reporting(E_ALL);
ini_set("display_errors", 1);*/

header("Access-Control-Allow-Origin: *");
require_once(__DIR__.'/BaseTest.php');
require_once('/home/poslavu/public_html/admin/api_private/api.php');
require_once('/home/poslavu/public_html/admin/api_private/api_includes.php');

class items extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

        $this->connectToDatabase();

        $this->refreshTableSchema('menu_items');

    }

	protected function run(){

        $action = $_REQUEST['action']; //function name
        $id = $_REQUEST['id'];
        $current_page=$_REQUEST['current_page'];
        $next_page=$_REQUEST['next_page'];
        $per_page=$_REQUEST['per_page'];
        if($action == "getItemList"){
         $id = array("category_id" => $id , "_deleted" => 0, "dining_option" => "like:delivery");
         $pagination = array("current_page"=>1, "next_page"=>2,"previous_page"=>1,"per_page"=>4);
        } else {
         $id = array("id" => $id);
         $pagination = "";
        }
        $dataName = array("dataname" => "demo_training_1");
        $apiRequest = array();
        $apiRequest['Auth']['dataname'] = 'demo_training_1' ;
        $apiRequest['Auth']['user_id'] = 1;
        $apiRequest['Action'] = $action ;
        $apiRequest['Params'] = array();
        $apiRequest['Params']['categoryID'] = $id;
        $apiRequest['Params']['pagination'] = $pagination;
        $apiRequest['Params']['dataname'] = $dataName;
        $apiRequest['Component'] = 'Items';
        $apiRequestJSON = json_encode($apiRequest);
        Internal_API::apiQuery($apiRequestJSON);

	}

	protected function tearDown(){

	}

}

$obj = new items();
