<?php

/**
*This is a wrapper class for PHP-JWT lib. Use it to generate new jwt token and also decodes jwt token
Author: OM
*/

require 'vendor/autoload.php';
use \Firebase\JWT\JWT;


class jwtOlo{

	private $secretKey="d5eab76c23b1923ad3f0999c3a88eaac20d3d42c472155d1360341b3451f5d52";
	private $alg="HS256"; //encryption algorithm
	private $expire=1800; //jwt expiration time in sec
	public  $tokenId;
	public  $jwt; //json token
	public 	$payload; //jwt payload data 
	public 	$exception; //err message
	public 	$response; // response data
	private $status; //success/fail
	private $issuedAt;

	public function __construct($tokenId=''){
		if($tokenId!=''){
			$this->tokenId=$tokenId;
		}else{
			$this->buildTokenId();
		}
	}

	/**
	*This will generate final jwt data
	*param array $data //payload data
	*return json response
	*/
	public function getJwt($args){
                if(!array_key_exists('user_id',$args)){
                  $args['user_id'] = 0;
                }
                $this->payload = $args;
		try{
		if(!is_array($this->payload)) throw new Exception('Invalid data format');
		$jwtData=array(
				"tid"=>$this->tokenId,
				"iat"=>$this->setIssuedAt(),
				"exp"=>$this->issuedAt + $this->expire,
				"data"=>$this->payload
				);
		$this->jwt=JWT::encode($jwtData,$this->secretKey,$this->alg);
		$this->status='success';
		}catch(Exception $err){
			$this->status='fail';
			$this->exception=$err->getMessage();
		}
		$this->buildResponse();
		return $this->response;
	}

	/**
	*Build response for client request
	*
	*/
	private function buildResponse(){
		$response=array(
							"status"=>$this->status,
							"jwt"=>$this->jwt,
							"tokenId"=>$this->tokenId,
							"data"=>$this->payload,
							"exception"=>$this->exception
						);
		$this->response=json_encode($response);
	}

	/**
	*To build tokenId
	*/
	private function buildTokenId(){
                $len=32;
                //$phrase = "OLO";
                //$this->tokenId=base64_encode(openssl_random_pseudo_bytes($len));
                $tokenId = strip_tags(base64_encode(openssl_random_pseudo_bytes($len)));
                $this->tokenId = preg_replace('/[^A-Za-z0-9 !@#$%^&*().]/u','', $tokenId);
		//$this->tokenId=base64_encode(mcrypt_create_iv(32));
                //$this->tokenId = hash_hmac('sha256', $phrase, $this->secretKey);
	}

	/**
	*To capture time when jwt was created
	*/
	private function setIssuedAt(){
		$this->issuedAt=time();
	}
	
	/**
	*To decrypt jwt token and return response with all required details
	*param String $jwt //valid jwt token
	*return String $response //json format string
	*/
	public function getData($args){
                $this->jwt = str_replace("Bearer: ","",$args['jwt']);
                $this->tokenId = $args['tokenId'];
		try{
		//$this->jwt=$jwt;
		$data=JWT::decode($this->jwt, $this->secretKey, array($this->alg));
		$data=(array) $data;
                //echo $this->tokenId."-".$data['tid']."<br>";
		If ($this->tokenId!=$data['tid']){
			throw new Exception("Invalid Token");
		}
		$this->payload=(array) $data['data'];
		$this->status='success';
		}catch(Exception $e){
			$this->status='fail';
			$this->exception=$e->getMessage();
		}

		$this->buildResponse();
		return $this->response;
	}

}

/*Sample script to access above class*/
/*
//===================
$o=new lavu_jwt();

$o->getJwt(array("name"=>"lavu","prod"=>"olo"));
echo "<hr>";
echo $o->response;

//hardcoded jwt token for test purpose
$jwt='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0aWQiOiI3aFduZEVTS1c0OFdpRzhReGE3QzMybmlBRGpLOEFSVWhlUStNY2VYZlZnPSIsImlhdCI6bnVsbCwiZXhwIjoxNDk5Nzc5MDQ1LCJkYXRhIjp7Im5hbWUiOiJzaW1yYW4iLCJjb3Vyc2UiOiJDUyJ9fQ.zhm35VDkHNIx9lAgtg-fF77vlRjSGVz6QoMKVSgdC90';
$o->tokenId='7hWndESKW48WiG8Qxa7C32niADjK8ARUheQ+MceXfVg=';

$o->getData($jwt);
echo "<hr>";
echo $o->response;
*/

?>
