<?php
	/*
	* --------------------------------------------------------------------
	* LavuToGo braintree connect for PayPal card payments 
	* --------------------------------------------------------------------
	* Allows ltg to connect to braintree and generate payments tokens,
	* using cp configured client `public key`, `private key`, `merchant id`
	* --------------------------------------------------------------------
	* @usage
	* Example: validate and get client token
	* $braintreeConnect = new BraintreeConnect([
	*	"integration1" => '<merchant-id>',
	*	"integration2" => '<public-key>',
	*	"integration3" => '<private-key>'
	* ]);
	* $braintreeConnect->getClientToken();
	* --------------------------------------------------------------------
	* @usage
	* Example: do a sale
	* $braintreeConnect = new BraintreeConnect([
	*	"integration1" => '<merchant-id>',
	*	"integration2" => '<public-key>',
	*	"integration3" => '<private-key>'
	* ]);
	* $braintreeConnect->doSale([<sales-info-array>]);
	*/

	require_once( __DIR__.'/../../../admin/vendor/braintree/braintree_php/lib/autoload.php');
	require_once( __DIR__.'/config.php');
	
	class BraintreeConnect {
		private $clientToken;
		private $sandBoxTestAccounts = BRAINTREE_TEST_ACCOUNTS;
		public $hasError = false;
		public $errorMessage = '';
		private $merchantid = '';
		private $publicKey = '';
		private $privatekey = '';
		private $environment = BRAINTREE_PROD;
		private $gateway;

		public function __construct($attributes = array()) {
			$this->merchantid = isset($attributes['integration1']) ? trim($attributes['integration1']) : false;
			$this->publickey  = isset($attributes['integration2']) ? trim($attributes['integration2']) : false;
			$this->privatekey = isset($attributes['integration3']) ? trim($attributes['integration3']) : false;
			$this->sandBoxTestAccounts = explode('|', $this->sandBoxTestAccounts);
			$this->environment = in_array($this->merchantid, $this->sandBoxTestAccounts) ? BRAINTREE_SANDBOX : BRAINTREE_PROD;
			$this->testConfiguration();
			if (!$this->hasError) {
				$this->braintreeConnect();
			}
		}

		private function braintreeConnect() {
			try {
				$this->clearErrors();
				$this->gateway = new Braintree_Gateway([
					'environment' => $this->environment,
					'merchantId' => $this->merchantid,
					'publicKey' => $this->publickey,
					'privateKey' => $this->privatekey
				]);
				$this->clientToken = $this->gateway->clientToken()->generate();
			} catch (Exception $exc) {
				$this->setError(0000);
			}
		}

		public function getClientToken() {
			return $this->clientToken;
		}

		private function testConfiguration() {
			if (!$this->merchantid) {
				$this->setError(1000);
			} else if (!$this->publickey){
				$this->setError(1010);
			} else if (!$this->privatekey){
				$this->setError(1020);
			}
		}

		private function setError($errorCode) {
			$this->hasError = true;
			$this->errorMessage = self::getError($errorCode);
		}

		private function clearErrors() {
			$this->hasError = false;
			$this->errorMessage = '';
		}

		public static function getError($errorCode) {
			switch ($errorCode) {
				case 1000:
					return "Merchant Id is required!";
				case 1010:
					return "Public key is required!";
				case 1020:
					return "Private key is required!";
				case 0000:
				default:
					return "There was an error with the information you entered, please try again.";
			}
		}

		/* 
		New Braintree Response for developer reference
            Success :- {
                    "success": true,
                    "transaction": {
                        "id": "37jkxe3a",
                        "status": "submitted_for_settlement",
                        "type": "sale",
                        "currencyIsoCode": "USD",
                        "amount": "1.05",
                        "merchantAccountId": "lavuinc_instant",
                        "subMerchantAccountId": null,
                        "masterMerchantAccountId": null,
                        "orderId": "2-14042",
                        "createdAt": {
                            "date": "2020-02-06 20:42:42",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        },
                        "updatedAt": {
                            "date": "2020-02-06 20:42:42",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        },
                        "customer": {
                            "id": null,
                            "firstName": null,
                            "lastName": null,
                            "company": null,
                            "email": null,
                            "website": null,
                            "phone": null,
                            "fax": null
                        },
                        "billing": {
                            "id": null,
                            "firstName": null,
                            "lastName": null,
                            "company": null,
                            "streetAddress": null,
                            "extendedAddress": null,
                            "locality": null,
                            "region": null,
                            "postalCode": null,
                            "countryName": null,
                            "countryCodeAlpha2": null,
                            "countryCodeAlpha3": null,
                            "countryCodeNumeric": null
                        },
                        "refundId": null,
                        "refundIds": [],
                        "refundedTransactionId": null,
                        "partialSettlementTransactionIds": [],
                        "authorizedTransactionId": null,
                        "settlementBatchId": null,
                        "shipping": {
                            "id": null,
                            "firstName": null,
                            "lastName": null,
                            "company": null,
                            "streetAddress": null,
                            "extendedAddress": null,
                            "locality": null,
                            "region": null,
                            "postalCode": null,
                            "countryName": null,
                            "countryCodeAlpha2": null,
                            "countryCodeAlpha3": null,
                            "countryCodeNumeric": null
                        },
                        "customFields": null,
                        "avsErrorResponseCode": null,
                        "avsPostalCodeResponseCode": "I",
                        "avsStreetAddressResponseCode": "I",
                        "cvvResponseCode": "M",
                        "gatewayRejectionReason": null,
                        "processorAuthorizationCode": "006691",
                        "processorResponseCode": "1000",
                        "processorResponseText": "Approved",
                        "additionalProcessorResponse": null,
                        "voiceReferralNumber": null,
                        "purchaseOrderNumber": null,
                        "taxAmount": null,
                        "taxExempt": false,
                        "creditCard": {
                            "token": null,
                            "bin": "556337",
                            "last4": "3222",
                            "cardType": "MasterCard",
                            "expirationMonth": "08",
                            "expirationYear": "2022",
                            "customerLocation": "US",
                            "cardholderName": "Test Test",
                            "imageUrl": "https:\\/\\/assets.braintreegateway.com\\/payment_method_logo\\/mastercard.png?environment=production",
                            "prepaid": "No",
                            "healthcare": "No",
                            "debit": "No",
                            "durbinRegulated": "No",
                            "commercial": "No",
                            "payroll": "No",
                            "issuingBank": "SILICON VALLEY BANK",
                            "countryOfIssuance": "USA",
                            "productId": "MCF",
                            "globalId": null,
                            "accountType": null,
                            "uniqueNumberIdentifier": null,
                            "venmoSdk": false
                        },
                        "statusHistory": [
                            [],
                            []
                        ],
                        "planId": null,
                        "subscriptionId": null,
                        "subscription": {
                            "billingPeriodEndDate": null,
                            "billingPeriodStartDate": null
                        },
                        "addOns": [],
                        "discounts": [],
                        "descriptor": [],
                        "recurring": false,
                        "channel": "Lavu_SP",
                        "serviceFeeAmount": null,
                        "escrowStatus": null,
                        "disbursementDetails": [],
                        "disputes": [],
                        "authorizationAdjustments": [],
                        "paymentInstrumentType": "credit_card",
                        "processorSettlementResponseCode": null,
                        "processorSettlementResponseText": null,
                        "networkResponseCode": "00",
                        "networkResponseText": "Approved or completed successfully",
                        "threeDSecureInfo": null,
                        "shipsFromPostalCode": null,
                        "shippingAmount": null,
                        "discountAmount": null,
                        "networkTransactionId": null,
                        "processorResponseType": "approved",
                        "authorizationExpiresAt": {
                            "date": "2020-02-13 20:42:42",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        },
                        "refundGlobalIds": [],
                        "partialSettlementTransactionGlobalIds": [],
                        "refundedTransactionGlobalId": null,
                        "authorizedTransactionGlobalId": null,
                        "globalId": "dHJhbnNhY3Rpb25fMzdqa3hlM2E",
                        "retryIds": [],
                        "retriedTransactionId": null,
                        "retrievalReferenceNumber": null,
                        "creditCardDetails": [],
                        "customerDetails": [],
                        "billingDetails": [],
                        "shippingDetails": [],
                        "subscriptionDetails": []
                    }
                }
		Fail :- {
                "errors": [],
                "params": {
                    "transaction": {
                        "type": "sale",
                        "amount": "2",
                        "paymentMethodNonce": "tokencc_bf_68g798_sxg4wt_5prfcr_6qjh3k_4w7",
                        "orderId": "2-14043",
                        "options": {
                            "paypal": null,
                            "submitForSettlement": "true"
                        },
                        "channel": "Lavu_SP"
                    }
                },
                "message": "Processor Declined",
                "transaction": {
                    "id": "nvvqvqpt",
                    "status": "processor_declined",
                    "type": "sale",
                    "currencyIsoCode": "USD",
                    "amount": "2.00",
                    "merchantAccountId": "lavuinc_instant",
                    "subMerchantAccountId": null,
                    "masterMerchantAccountId": null,
                    "orderId": "2-14043",
                    "createdAt": {
                        "date": "2020-02-07 07:08:06",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    },
                    "updatedAt": {
                        "date": "2020-02-07 07:08:06",
                        "timezone_type": 3,
                        "timezone": "UTC"
                    },
                    "customer": {
                        "id": null,
                        "firstName": null,
                        "lastName": null,
                        "company": null,
                        "email": null,
                        "website": null,
                        "phone": null,
                        "fax": null
                    },
                    "billing": {
                        "id": null,
                        "firstName": null,
                        "lastName": null,
                        "company": null,
                        "streetAddress": null,
                        "extendedAddress": null,
                        "locality": null,
                        "region": null,
                        "postalCode": null,
                        "countryName": null,
                        "countryCodeAlpha2": null,
                        "countryCodeAlpha3": null,
                        "countryCodeNumeric": null
                    },
                    "refundId": null,
                    "refundIds": [],
                    "refundedTransactionId": null,
                    "partialSettlementTransactionIds": [],
                    "authorizedTransactionId": null,
                    "settlementBatchId": null,
                    "shipping": {
                        "id": null,
                        "firstName": null,
                        "lastName": null,
                        "company": null,
                        "streetAddress": null,
                        "extendedAddress": null,
                        "locality": null,
                        "region": null,
                        "postalCode": null,
                        "countryName": null,
                        "countryCodeAlpha2": null,
                        "countryCodeAlpha3": null,
                        "countryCodeNumeric": null
                    },
                    "customFields": null,
                    "avsErrorResponseCode": null,
                    "avsPostalCodeResponseCode": "I",
                    "avsStreetAddressResponseCode": "I",
                    "cvvResponseCode": "U",
                    "gatewayRejectionReason": null,
                    "processorAuthorizationCode": null,
                    "processorResponseCode": "2038",
                    "processorResponseText": "Processor Declined",
                    "additionalProcessorResponse": "63 : SERV NOT ALLOWED",
                    "voiceReferralNumber": null,
                    "purchaseOrderNumber": null,
                    "taxAmount": null,
                    "taxExempt": false,
                    "creditCard": {
                        "token": null,
                        "bin": "411111",
                        "last4": "1111",
                        "cardType": "Visa",
                        "expirationMonth": "12",
                        "expirationYear": "2021",
                        "customerLocation": "US",
                        "cardholderName": "Hayat",
                        "imageUrl": "https:\\/\\/assets.braintreegateway.com\\/payment_method_logo\\/visa.png?environment=production",
                        "prepaid": "Unknown",
                        "healthcare": "Unknown",
                        "debit": "Unknown",
                        "durbinRegulated": "Unknown",
                        "commercial": "Unknown",
                        "payroll": "Unknown",
                        "issuingBank": "Unknown",
                        "countryOfIssuance": "Unknown",
                        "productId": "Unknown",
                        "globalId": null,
                        "accountType": null,
                        "uniqueNumberIdentifier": null,
                        "venmoSdk": false
                    },
                    "statusHistory": [
                        []
                    ],
                    "planId": null,
                    "subscriptionId": null,
                    "subscription": {
                        "billingPeriodEndDate": null,
                        "billingPeriodStartDate": null
                    },
                    "addOns": [],
                    "discounts": [],
                    "descriptor": [],
                    "recurring": false,
                    "channel": "Lavu_SP",
                    "serviceFeeAmount": null,
                    "escrowStatus": null,
                    "disbursementDetails": [],
                    "disputes": [],
                    "authorizationAdjustments": [],
                    "paymentInstrumentType": "credit_card",
                    "processorSettlementResponseCode": null,
                    "processorSettlementResponseText": null,
                    "networkResponseCode": "  ",
                    "networkResponseText": null,
                    "threeDSecureInfo": null,
                    "shipsFromPostalCode": null,
                    "shippingAmount": null,
                    "discountAmount": null,
                    "networkTransactionId": "               ",
                    "processorResponseType": "soft_declined",
                    "authorizationExpiresAt": null,
                    "refundGlobalIds": [],
                    "partialSettlementTransactionGlobalIds": [],
                    "refundedTransactionGlobalId": null,
                    "authorizedTransactionGlobalId": null,
                    "globalId": "dHJhbnNhY3Rpb25fbnZ2cXZxcHQ",
                    "retryIds": [],
                    "retriedTransactionId": null,
                    "retrievalReferenceNumber": null,
                    "creditCardDetails": [],
                    "customerDetails": [],
                    "billingDetails": [],
                    "shippingDetails": [],
                    "subscriptionDetails": []
                },
                "creditCardVerification": null,
                "subscription": null,
                "merchantAccount": null,
                "verification": null
            }
		*/
		public function doSale($data = array()) {
			return $this->gateway->transaction()->sale($data);
		}
	}

?>