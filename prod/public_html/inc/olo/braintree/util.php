 
<?php 
ini_set('display_errors', 1);

function GetOrderInfo($dataname, $order_id, $location_id=NULL)
{
	//ConnectionHub::getConn('rest')->selectDN($dataname);
	//$con=mysqli_connect("localhost","root","password");
       //$con=mysqli_connect("127.0.0.1","root","tnk4zig");
	//mysqli_select_db($con,"poslavu_demo_training_1_db");
	$get_order_info =mysqli_query($GLOBALS['con'],"SELECT `id`, `order_id`, `subtotal`, `tax`, `total`, `card_paid`, `void`, `location_id`, `email`, `togo_time`, `togo_name`, `ioid`, 
			`active_device`, `location_id` FROM `orders` WHERE `order_id`= '$order_id' AND `location_id` = '$location_id' 
			ORDER BY `opened` DESC LIMIT 1");
	if (!$get_order_info)
	{
		error_log("util ".__FUNCTION__." failed: ".mysqli_error($con));
		return array();
	}

	if (mysqli_num_rows($get_order_info) > 0)
	{
		return mysqli_fetch_assoc($get_order_info);
	}
	else
	{
		trigger_error("util ".__FUNCTION__." failed to find order ".$order_id);
		die("Order not found. Please try again momentarily.");
	}

	return false;
}

function UnvoidOrder($dataname, $card_paid, $ioid)
{
	//ConnectionHub::getConn('rest')->selectDN($dataname);
	//$con=mysqli_connect("localhost","root","password");
       //$con=mysqli_connect("127.0.0.1","root","tnk4zig");
	//mysqli_select_db($con,"poslavu_demo_training_1_db");

	//$unvoid_order = lavu_query("UPDATE `orders` SET `void` = '0', `card_paid` = '[1]' WHERE `ioid` = '[2]' AND `void` = '3' AND `togo_status` = '1'", $card_paid, $ioid);
	//echo "UPDATE `orders` SET `void` = '0', `card_paid` = '$card_paid' WHERE `ioid` = '$ioid' AND `void` = '3' AND `togo_status` = '1'";
	$unvoid_order =mysqli_query($GLOBALS['con'],"UPDATE `orders` SET `void` = '0', `card_paid` = '$card_paid' WHERE `ioid` = '$ioid' AND `void` = '3' AND `togo_status` = '1'");
	if (!$unvoid_order)
	{
		ltgDebug(__FILE__, "UnvoidOrder failed: ".mysqli_error());
	}
}
function buildInsertFieldsAndValues($vars, &$fields, &$values)
{
	$fields = "";
	$values = "";

	$keys = array_keys($vars);
	foreach ($keys as $key)
	{
		if ($fields != "")
		{
			$fields .= ", ";
			$values .= ", ";
		}
		$fields .= "`$key`";
		$values .= "'[$key]'";
	}
}

function saveTransaction(&$transaction, $dataname, $ioid, $location_id, $order_id)
	{
                //echo  $dataname;
                //print_r($transaction);
		//ConnectionHub::getConn('rest')->selectDN($dataname);
                 //$con=mysqli_connect("localhost","root","password");
                  //$con=mysqli_connect("127.0.0.1","root","tnk4zig");
                // mysqli_select_db($con,"poslavu_demo_training_1_db");
		$tz_save = date_default_timezone_get();
		//$account_tz = GetTimezone($dataname, $location_id);
                $account_tz ="";
		//date_default_timezone_set($account_tz);
		$local_datetime = date("Y-m-d H:i:s");
		date_default_timezone_set($tz_save);

		if (empty($transaction['internal_id']))
		{
			$transaction['internal_id'] ="";
		}

		$default_tx = array(
			'action'   => "Sale",
			'ag_amount'	=> "",
			'amount'       => "0",
			'auth'				=> "0",
			'auth_code'			=> "",
			'batch_no'			=> "",
			'card_desc'			=> "",
			'card_type'			=> "",
			 'change'				=> "0",
			'check'				=> "1",
			'currency_code'		=> "",
			'customer_id'		=> "0",
			'datetime'			=> $local_datetime,
			'device_udid'		=> "",
			'exp'				=> "",
			'first_four'			=> "",
			'for_deposit'		=> "0",
			'gateway'			=> "",
			'got_response'		=> "1",
			'info'				=> "",
			'info_label'			=> "",
			'ioid'				=> $ioid,
			'is_deposit'			=> "0",
			'last_mod_ts'		=> time(),
			'loc_id'				=> $location_id,
			'more_info'			=> "",
			'mpshc_pid'			=> "",
			'order_id'			=> $order_id,
			'pay_type'			=> "Card",
			'pay_type_id'		=> "2",
			'pin_used_id'		=> "0",
			'preauth_id'			=> "",
			'processed'			=> "0",
			'process_data'		=> "",
			'record_no'			=> "",
			'reader'				=> "",
			'refunded'			=> "0",
			'refunded_by'		=> "0",
			'refund_notes'		=> "",
			'refund_pnref'		=> "",
			'ref_data'			=> "",
			'register'			=> "",
			'register_name'		=> "",
			'rf_id'				=> "",
			'server_id'			=> "0",
			'server_name'		=> "",
			'server_time'		=> "",
			'signature'			=> "0",
			'split_tender_id'	=> "0",
			'swipe_grade'		=> "",
			'temp_data'			=> "",
			'tip_amount'			=> "",
			'tip_for_id'			=> "1",
			'total_collected'	=> "0",
			'transaction_id'	=> "",
			'transtype'			=> "Sale",
			'voice_auth'			=> "0",
			'voided'				=> "0",
			'voided_by'			=> "0",
			'void_notes'			=> "",
			'void_pnref'			=> "",
                        'extra_info'=>'1'
		);

		foreach ($transaction as $key => $value)
		{
			$default_tx[$key] = $value;
		}

		$fields = "";
		$values = "";
  
	     $fields="`" . implode ( "`, `", array_keys($default_tx) ) . "`";
	     $values="'" . implode ( "', '", array_values($default_tx) ) . "'";
		$sql="INSERT INTO `cc_transactions` (".$fields.") VALUES (".$values.")"; //exit;	
		$save_transaction = mysqli_query($GLOBALS['con'],$sql);  
		if (!$save_transaction)
		{
			error_log("util ".__FUNCTION__." failed: ".mysqli_error($GLOBALS['con']));
			return false;
		}
		return true;
	}
