<?php
ini_set('display_errors', 1);
die("sasa");
/**
 * Braintree - Payment Gateway integration (Hosted Fields) with custom stylesheet and validation
 * ==============================================================================
 * 
 * @version v1.0: braintree_hosted_fields_with_style_demo.php 2016/05/13
 * @copyright Copyright (c) 2016, http://www.ilovephp.net
 * @author Sagar Deshmukh <sagarsdeshmukh91@gmail.com>
 * You are free to use, distribute, and modify this software
 * ==============================================================================
 *
 */

// Braintree library
rrequire_once( __DIR__.'/../../../admin/vendor/braintree/braintree_php/lib/autoload.php');
require_once("util.php");
$params = array(
	"testmode"   => "on",
	"merchantid" => "dmd2vqyv8n75q2nn",
	"publickey"  => "rs5x7q5nd7v9dck5",
	"privatekey" => "e9f55a2f436fca5888a082c69b13ea77",
);

if ($params['testmode'] == "on")
{
	Braintree_Configuration::environment('sandbox');
}
else
{
	Braintree_Configuration::environment('production');
}

Braintree_Configuration::merchantId($params["merchantid"]);
Braintree_Configuration::publicKey($params["publickey"]);
Braintree_Configuration::privateKey($params["privatekey"]);

if (isset($_POST['action']) && $_POST['action'] == 'generateclienttoken')
{
	// Generate the nonce
	$clientToken = Braintree_ClientToken::generate();
	echo $clientToken; exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Braintree</title>
<link href="style.css" type="text/css" rel="stylesheet" />
<link href="hosted_field_style.css" type="text/css" rel="stylesheet" />
</head>

<body>
<h1 class="bt_title"></h1>
<div style="float:left;widht:450px">
<form id="bt_custom_form" name="bt_custom_form" method="post" action="">
  <div class="loader_container">
    <div class="loader_img loader"></div>
  </div>
  <div class="bt_form_wrap hide invisible">
    <fieldset>
      <label for="bt_card_number">Card Number</label>
      <div id="bt_card_number" class="inputFields"></div>
      <span class="payment-method-icon"></span>
    </fieldset>
    <div style="height: 84px;">
      <fieldset>
        <div style="float:left; width:49%">
          <label for="bt_exp_date">Expiration (MM/YY)</label>
          <div id="bt_exp_date" class="inputFields"></div>
        </div>
        <div style="float:right; width:49%">
          <label for="bt_cvv">CVV Number</label>
          <div id="bt_cvv" class="inputFields"></div>
          <span id="cvv-icon" class="payment-method-icon"></span> </div>
      </fieldset>
    </div>
    <div>
     <!-- <fieldset>
        <div style="float:left; width:49%; margin-top:9px">
          <input type="checkbox" value="1" name="braintree_subscribe" style="visibility:visible; position:relative;">
          <label for="braintree_subscribe">Set as default payment method</label>
        </div>-->
        <div style="float:right; width:49%" class="btn_container">
          <center><input type="submit" name="" class="button-me green-btn button-small pay-btn" value="PAY NOW" style="width:100%; margin-top:0px;" /></center>
          <input type="hidden" name="amount" value="10"  />
          <span class="loader_img"></span>
        </div>
      </fieldset> 
    </div>
    
  </div>
  <br />
  <br />
  <center>
    <div id="bt_paypal_container" style="display:none"></div>
  </center>
</form>
 </div>
<script type="text/javascript" src="https://js.braintreegateway.com/js/beta/braintree-hosted-fields-beta.18.min.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script src="hosted_field_style.js"></script>
</body>
</html>

