<?php
define("LTG_DISABLE", "0");
define("LTG_MONTHLY", "1");
define("LTG_ANNUAL", "2");
define("PROMO", "3");
define("DEMO", "4");
define("PACK_MONTHLY", "75");
define("PACK_MONTHLY_TEXT"," $75 Monthly");
define("PACK_ANNUAL", "799");
define("PACK_ANNUAL_TEXT", "$799 /Annual($66.58 /month)");
define("LAVUTOGO2", "Lavu To Go 2.0");
define("ZUORA_LTG_MONTHLY", 'Monthly');
define("ZUORA_LTG_ANNUAL", 'Annual');
define("ZUORA_BILLING_EMAIL", 'billing@lavu.com');
$ltgBillingPackages = array(
	LTG_DISABLE => 'Disabled',
	LTG_MONTHLY => 'Monthly Billing',
	LTG_ANNUAL => 'Annual Billing',
	PROMO => 'Promo',
	DEMO => 'Demo'
);
