<?php

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

/**
 *
 * Pulls Lavu data out of our database and puts it into Pepper objects
 *
 */
class PepperDataExtractor
{
	/**
	 * Get Pepper location config settings from Lavu config table
	 *
	 * @param $args -- Array of input args with required key(s): dataname
	 *
	 * @return Array containing location config settings where config.setting (without "pepper_") is the key and config.value is the value
	 */
	public function getLocationConfigSettings($args)
	{
		$settings = array();

		if ( empty($args['dataname']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $settings;
		}

		if ( empty($args['loc_id']) )
		{
			$args['loc_id'] = '1';
		}

		$location_config_settings = $this->queryDatabase('location_config_settings', $args);

		// Put separate config rows into a single associative array where the key is the "setting" column (minus "pepper_") and the value column is the value
		while ( $location_config_setting = mysqli_fetch_assoc($location_config_settings) )
		{
			$key = str_replace('pepper_', '', $location_config_setting['setting']);
			$val = $location_config_setting['value'];

			// Key renames
			switch ($key)
			{
				case 'locationid':
					$key = 'locationId';
					break;
				case 'tenantid':
					$key = 'tenantId';
					break;
			}

			$settings[$key] = $val;
		}

		return $settings;
	}

	/**
	 * Get Customer Mangement field mapping from Lavu config table
	 *
	 * @param $args -- Array of input args with required key(s): loc_id
	 *
	 * @return Array containing Customer Management field labels as keys and the field names (aka database column names) as the values
	 */
	public function getCustomerMangementFieldMapping($args)
	{
		$field_mapping = array();

		if ( empty($args['loc_id']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $field_mapping;
		}

		// Query for Customer Management field mapping and store in associative array
		$customer_mgmt_field_mappings = $this->queryDatabase('config_customer_management_fields', $args);
		while ($customer_mgmt_field_mapping = mysqli_fetch_assoc($customer_mgmt_field_mappings))
		{
			$field_label   = $customer_mgmt_field_mapping['value'];
			$db_field_name = $customer_mgmt_field_mapping['value2'];
			$field_mapping[$field_label] = $db_field_name;
		}

		return $field_mapping;
	}

	/**
	 * Create Location object for Pepper
	 *
	 * @param $args -- Array of input args with required key(s): dataname
	 *
	 * @return Array containing object fields that can be JSON encoded
	 */
	public function getLocation($args)
	{
		$obj = array();

		if ( empty($args['dataname']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$restaurant = $this->queryDatabase('restaurant', $args);
		$signup = $this->queryDatabase('signup', $args);
		///$location = empty($args['loc_id']) ? $this->queryDatabase('location_highest_nondeleted', $args) : $this->queryDatabase('location', $args)

		///$obj['id'] = $location['id'];

		$obj['title'] = $restaurant['company_name'];

		$obj['address'] = array();
		$obj['address']['address']     = $signup['address'];
		$obj['address']['address2']    = '';
		$obj['address']['town']        = $signup['city'];
		$obj['address']['postcode']    = $signup['zip'];
		$obj['address']['country']     = empty($signup['country']) ? 'USA' : $signup['country'];
		$obj['address']['summary']     = $signup['address'] .', '. $signup['city'] .', '. $signup['state'];
		$obj['address']['fullAddress']  = $signup['address'] .', '. $signup['city'] .', '. $signup['state'] .', '. $signup['country'];
		$obj['address']['googleMapsShortUrl'] = '';   // TO DO

		//$obj['geo'] = array(35.084037, -106.648858);  // TO DO : add Wilco's IP address to JSON'd geo loc stuff here
		$obj['geo'] = array(-0.100000, 51.50000);  // TO DO : add Wilco's IP address to JSON'd geo loc stuff here

		$obj['services'] = array();
		$obj['services']['pre_order'] = true;

		$obj['description'] = '';        // TO DO
		$obj['VATnumber'] = 'GB 1111111111';          // TO DO
		$obj['beacons'] = array();       // TO DO
		$obj['beacons']['minor'] = '11111';
		$obj['beacons']['major'] = '11111';
		$obj['beacons']['uuid'] = 'B9407F30-F5F8-466E-AFF9-25556B57FE6D';
		$obj['beacons']['enabled'] = true;

		$obj['links'] = array();         // TO DO
		$obj['links']['facebook'] = array('parameters' => '123456', 'enabled' => true);         // TO DO
		$obj['links']['twitter'] = array('parameters' => '123456', 'enabled' => true);         // TO DO
		$obj['links']['foursquare'] = array('parameters' => '', 'enabled' => false);         // TO DO
		$obj['links']['instagram'] = array('parameters' => '', 'enabled' => false);         // TO DO
		$obj['links']['uber'] = array('enabled' => true);         // TO DO
		$obj['links']['hailo'] = array('enabled' => true);         // TO DO
		$obj['links']['citymapper'] = array('enabled' => true);         // TO DO

		$obj['openingHours'] = array();  // TO DO
		$obj['openingHours']['sunday'] = 'Closed';  // TO DO
		$obj['openingHours']['saturday'] = 'Closed';  // TO DO
		$obj['openingHours']['friday'] = '7:30am to 5:30pm';  // TO DO
		$obj['openingHours']['thursday'] = '7:30am to 5:30pm';  // TO DO
		$obj['openingHours']['wednesday'] = '7:30am to 5:30pm';  // TO DO
		$obj['openingHours']['tuesday'] = '7:30am to 5:30pm';  // TO DO
		$obj['openingHours']['monday'] = '7:30am to 5:30pm';  // TO DO

		return $obj;
	}

	/**
	 * Create Menu for Pepper
	 *
	 * @param $args -- Array of input args with required key(s): dataname
	 *
	 * @return Array containing object fields that can be JSON encoded
	 */
	public function getMenu($args)
	{
		$obj = array();

		if ( empty($args['dataname']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$location = empty($args['loc_id']) ? $this->queryDatabase('location_highest_nondeleted', $args) : $this->queryDatabase('location', $args);
		$restaurant = $this->queryDatabase('restaurant', $args);
		$signup = $this->queryDatabase('signup', $args);

		if (empty($args['loc_id'])) $args['loc_id'] = $location['id'];
		if (empty($args['menu_id'])) $args['menu_id'] = $location['menu_id'];
		$args['location_tax_inclusion'] = $location['tax_inclusion'];

		list($categories, $options) = $this->getMenuCategoriesAndOptions($args);

		$obj['title'] = trim($location['title']) .' Menu';  // required field
		$obj['description'] = "To Go Menu from ". $args['dataname'];
		$obj['schema']  = '3.45.0';     // required - but value does not matter
		$obj['version'] = (int) time(); // required - but value does not matter
		$obj['updated'] = date('c');    // required - but may not actually be
		$obj['bundles'] = array();      // required - but not actually supported yet so should be empty array

		// These values mess up the JSON menu upload so only output them if the API flag is set
		if ( !empty($args['api-extract']) )
		{
			// Adam didn't think we needed any of these fields
			//$obj['timeToLive'] = 1.0;
			//$obj['default'] = false;
			//$obj['active'] = true;
		}

		$obj['categories'] = $categories;
		$obj['options'] = $options;

		return $obj;
	}

	/**
	 * Create Menu for Pepper
	 *
	 * @param $args -- Array of input args with required key(s): dataname
	 *
	 * @return Array containing object fields that can be JSON encoded
	 */
	public function getMenuCategoriesAndOptions($args)
	{
		$options = array();
		$categories = array();
		$allModifiersUsed = array();

		// Make taxProfiles array like tax_profiles except key is the tax_profile ID (and value is still the corresponding tax_profile array)
		// but we need to create a virtual tax profile for "No tax profile" (tax_profile_id = 0) - which is different from a blank value (category pass-thru)
		$tax_profiles = $this->queryDatabase('tax_profiles', $args);
		$tax_profiles[] = array('id' => '0', 'loc_id' => $args['loc_id'], 'title' => 'No Tax', '_deleted' => 0);
		foreach ($tax_profiles as $tax_profile)
		{
			$tax_profile_id = $tax_profile['id'];
			$taxProfiles[$tax_profile_id] = $tax_profile;
		}

		// Get all active, non-deleted menu categories from non-deleted menu groups (since that's what's shown in CP on the menu page)
		// that are flagged for ToGo menu display and organize them in Pepper format
		$menu_categories = $this->queryDatabase('menu_categories_from_nondeleted_menu_groups', $args);
		foreach ($menu_categories as $menu_category)
		{
			$args['category_id'] = $menu_category['id'];
			$args['category_loyalty_display'] = $menu_category['loyalty_display'];
			$args['category_modifier_list_id'] = $menu_category['modifier_list_id'];
			$args['category_forced_modifier_group_id'] = $menu_category['forced_modifier_group_id'];
			$args['category_tax_profile_id'] = $menu_category['tax_profile_id'];
			$args['category_tax_inclusion'] = $menu_category['tax_inclusion'];
			$args['location_tax_inclusion'] = $args['location_tax_inclusion'];
			list($products, $modifiers, $inclusiveTaxProfiles) = $this->getMenuItemsAndModifiersForCategory($args, $allModifiersUsed);


			$category = array(
				'id' => $menu_category['id'],
				'title' => substr(trim($menu_category['name']), 0, 40),
				'shortDescription' => substr(trim($menu_category['description']), 0, 255),
				'sort' => max(0, (int) $menu_category['_order']),
				'products'  => $products,
				'modifiers' => $modifiers,
				'collapsed' => true,
			);

			// Only add taxScheme if a tax profile is used -- TO DO : do this for tax rates that aren't part of a tax profile
			if ( !empty($menu_category['tax_profile_id']) && isset($menu_category['tax_profile_id']) && isset($taxProfiles[$menu_category['tax_profile_id']]) )
			{
				$category['taxScheme'] = $menu_category['tax_profile_id'];
			}

			if ( !empty($menu_category['image']) )
			{
				$category['imageUrl'] = 'https://admin.poslavu.com/images/'. $args['dataname'] .'/categories/main/' . str_replace(' ', '%20', $menu_category['image']);
			}

			// We have to remove this field when empty because they were breaking the entire menu import
			if ( empty($category['shortDescription']) )
			{
				unset($category['shortDescription']);
			}

			$categories[] = $category;
		}

		$taxSchemes = array();
		foreach ($tax_profiles as $tax_profile)
		{
			$args['profile_id'] = $tax_profile['id'];
			$tax_rates = ($tax_profile['id'] === '0') ? array(array('calc' => '0')) : $this->queryDatabase('tax_rates_by_profile_id', $args);

			// TO DO SOMEDAY : add support for stacked tax_rates (by merging tax_rates, maybe?), inline tax rates (used by 2011 legacy accounts)

			$rates = array();
			foreach ($tax_rates as $tax_rate)
			{
				$rate = array(
					'name' => 'TAKE_AWAY',  // Required by Pepper app; menu JSON upload was breaking in the Pepper Console without this!
					'value' => (double) $tax_rate['calc'],
				);
				$rates[] = $rate;
			}

			$taxScheme = array(
				'id' => $tax_profile['id'],
				'title' => substr(trim($tax_profile['title']), 0, 40),
				'inclusive' => in_array($tax_profile['id'], $inclusiveTaxProfiles),
				'rates' => $rates,
			);

			$taxSchemes[] = $taxScheme;
		}

		$options = array(
			'tags' => array(),
			'taxSchemes' => $taxSchemes,
		);

		return array($categories, $options);
	}


	/**
	 * Pull Lavu Menu Items and Modifiers for Category in Pepper structure (for JSON conversion)
	 *
	 * @param $args -- Array of input args with required key(s): dataname, loc_id, menu_category_id
	 *
	 * @return Array containing object fields that can be JSON encoded
	 */
	public function getMenuItemsAndModifiersForCategory($args, &$allModifiersUsed)
	{
		global $allModifiersUsed;

		$products = array();
		$modifiers = array();
		$inclusiveTaxProfiles = array();
		$modifiersUsedForCategory = array();
		$optionalModifiersUsed = array();
		$forcedModifiersUsed = array();
		$taxProfiles = array();

		// Make taxProfiles array like tax_profiles except key is the tax_profile ID (and value is still the corresponding tax_profile array)
		// but start it out with [0] being an automatically created No Tax virtual tax profile that will get used if a tax_profile_id = 0
		// (which explicitly means "No tax profile" in the CP item/category details pop-up, as opposed to blank which means defer to parent)
		$taxProfiles[0] = array('id' => 0, 'loc_id' => $args['loc_id'], 'title' => '', '_deleted' => 0);
		$tax_profiles = $this->queryDatabase('tax_profiles', $args);
		foreach ($tax_profiles as $tax_profile)
		{
			$tax_profile_id = $tax_profile['id'];
			$taxProfiles[$tax_profile_id] = $tax_profile;
		}

		$menu_items = $this->queryDatabase('menu_items_by_category_id', $args);
		foreach ($menu_items as $menu_item)
		{
			$modifiersUsed = array();

			// Forced Modifiers

			// Use Forced Modifier on Menu Category level if one exists and there isn't one on the Menu Item-level
			if (empty($menu_item['forced_modifier_group_id']) && !empty($args['category_forced_modifier_group_id']))
			{
				$menu_item['forced_modifier_group_id'] = $args['category_forced_modifier_group_id'];
			}

			// Forced Modifier Group
			if (!empty($menu_item['forced_modifier_group_id']))
			{
				$first_char = substr($menu_item['forced_modifier_group_id'], 0, 1);
				if ($first_char == 'C')
				{
					// Get forced_modifiers using Category's forced_modifier_group_id
					$args['id'] = $menu_item['category_id'];
					$menu_category = $this->queryDatabase('menu_categories_by_id', $args);

					$forced_modifier_group = array();

					// Forced Modifier List
					if (substr($menu_category['forced_modifier_group_id'], 0, 1) == 'f')
					{
						// When a Forced Modifier List is deteceted, create a virtual force_modifier_group comprised of only one forced_modifer_list_id
						$forced_modifier_group['include_lists'] = str_replace('f', '', $menu_category['forced_modifier_group_id']);
					}
					// Force Modifier Group
					else
					{
						// Get forced modifiers using forced_modifier_groups
						$args['id'] = $menu_category['forced_modifier_group_id'];
						$forced_modifier_group = $this->queryDatabase('forced_modifier_groups_by_id', $args);
					}

					if (!empty($forced_modifier_group['include_lists']))
					{
						// Lavu forced modifiers get mapped to Pepper modifier options
						// But we don't actually map Lavu forced modifiers lists to anything
						$forced_modifier_list_ids = explode('|', $forced_modifier_group['include_lists']);
						foreach ($forced_modifier_list_ids as $forced_modifier_list_id)
						{
							$args['id'] = $forced_modifier_list_id;
							$forced_modifier_list = $this->queryDatabase('forced_modifier_lists_by_id', $args);

							// We map the Lavu forced modifier list to the Pepper modifier whenever a list is used.
							// But we have to prepend the "id" with the appropriate "namespace" to prevent ID collisions.
							$modifier = array();
							$modifier['id'] = 'forced_modifier_list:'. $forced_modifier_list['id'];
							$modifier['title'] = substr(trim($forced_modifier_list['title']), 0, 40);
							$modifier['minSelect'] = 1;  // characteristic of forced modifiers
							$modifier['maxSelect'] = 1;

							$args['list_id'] = $forced_modifier_list['id'];
							$modifier['options'] = $this->getForcedModifierOptions($args);

							$forcedModifiersUsed[] = $modifier['id'];
							$modifiersUsed[] = $modifier['id'];

							if (!isset($modifiersUsedForCategory[$modifier['id']]))
							{
								$modifiers[] = $modifier;
								$modifiersUsedForCategory[$modifier['id']] = 1;
							}

							if (!isset($allModifiersUsed[$modifier['id']]))
							{
								$allModifiersUsed[$modifier['id']] = 1;
							}

							/*
							$forced_modifiers = $this->queryDatabase('forced_modifiers_by_list_id', $args);
							foreach ($forced_modifiers as $forced_modifier)
							{
								$args['id'] = $forced_modifier['id'];
								$modifier['options'] = $this->getForcedModifierOptions($args);

								// Pepper doesn't distinguish between forced and optional modifiers, so we have to prepend the id with the modifier "namespace" to prevent collisions
								$mod_id = 'forced_modifier:'. $forced_modifier['id'];
								$forcedModifiersUsed[] = $mod_id;
								$modifiersUsed[] = $mod_id;
								if (!isset($allModifiersUsed[$modifier['id']]))
								{
									$modifiers[] = $modifier;
									$allModifiersUsed[$modifier['id']] = $modifier;
								}
							}
							*/
						}
					}
				}
				else if ($first_char == 'f')
				{
					$args['id'] = str_replace('f', '', $menu_item['forced_modifier_group_id']);
					$forced_modifier_list = $this->queryDatabase('forced_modifier_lists_by_id', $args);

					// We map the Lavu optional modifier category (and forced modifier group) to the Pepper modifier
					// but we have to prepend the "id" with the appropriate "namespace" to prevent ID collisions.
					$modifier = array();
					$modifier['id'] = 'forced_modifier_list:'. $forced_modifier_list['id'];
					$modifier['title'] = substr(trim($forced_modifier_list['title']), 0, 40);
					$modifier['minSelect'] = 1;  // characteristic of forced modifiers
					$modifier['maxSelect'] = 1;

					// Get forced_modifiers using forced_modifier_lists
					$args['list_id'] = $forced_modifier_list['id'];
					$modifier['options'] = $this->getForcedModifierOptions($args);

					$forcedModifiersUsed[] = $modifier['id'];
					$modifiersUsed[] = $modifier['id'];

					if (!isset($modifiersUsedForCategory[$modifier['id']]))
					{
						$modifiers[] = $modifier;
						$modifiersUsedForCategory[$modifier['id']] = 1;
					}

					$allModifiersUsed[$modifier['id']] = $modifier;

					/*
					$forced_modifiers = $this->queryDatabase('forced_modifiers_by_list_id', $args);
					foreach ($forced_modifiers as $forced_modifier)
					{
						$args['id'] = $forced_modifier['id'];
						$modifier['options'] = $this->getForcedModifierOptions($args);

						// Pepper doesn't distinguish between forced and optional modifiers, so we have to prepend the id with the modifier "namespace" to prevent collisions
						$mod_id = 'forced_modifier:'. $forced_modifier['id'];
						$forcedModifiersUsed[] = $mod_id;
						$modifiersUsed[] = $mod_id;
						if (!isset($allModifiersUsed[$modifier['id']]))
						{
							$modifiers[] = $modifier;
							$allModifiersUsed[$modifier['id']] = 1;
						}
					}
					*/
				}
				else
				{
					// Get forced modifiers using forced_modifier_groups
					$args['id'] = $menu_item['forced_modifier_group_id'];
					$forced_modifier_group = $this->queryDatabase('forced_modifier_groups_by_id', $args);

					$forced_modifier_list_ids = explode('|', $forced_modifier_group['include_lists']);
					foreach ($forced_modifier_list_ids as $forced_modifier_list_id)
					{
						$args['id'] = $forced_modifier_list_id;
						$forced_modifier_list = $this->queryDatabase('forced_modifier_lists_by_id', $args);

						// We map the Lavu optional modifier category (and forced modifier group) to the Pepper modifier
						// but we have to prepend the "id" with the appropriate "namespace" to prevent ID collisions.
						$modifier = array();
						$modifier['id'] = 'forced_modifier_list:'. $forced_modifier_list['id'];
						$modifier['title'] = substr(trim($forced_modifier_list['title']), 0, 40);
						$modifier['minSelect'] = 1;  // characteristic of forced modifiers
						$modifier['maxSelect'] = 1;

						$args['list_id'] = $forced_modifier_list_id;
						$modifier['options'] = $this->getForcedModifierOptions($args);

						$forcedModifiersUsed[] = $modifier['id'];
						$modifiersUsed[] = $modifier['id'];

						if (!isset($modifiersUsedForCategory[$modifier['id']]))
						{
							$modifiers[] = $modifier;
							$modifiersUsedForCategory[$modifier['id']] = 1;
						}

						$allModifiersUsed[$modifier['id']] = 1;

						/*
						$forced_modifiers = $this->queryDatabase('forced_modifiers_by_list_id', $args);
						foreach ($forced_modifiers as $forced_modifier)
						{
							$args['id'] = $forced_modifier['id'];
							$modifier['options'] = $this->getForcedModifierOptions($args);

							// Pepper doesn't distinguish between forced and optional modifiers, so we have to prepend the id with the modifier "namespace" to prevent collisions
							$mod_id = 'forced_modifier:'. $forced_modifier['id'];
							$forcedModifiersUsed[] = $mod_id;
							$modifiersUsed[] = $mod_id;

							if (!isset($allModifiersUsed[$modifier['id']]))
							{
								$modifiers[] = $modifier;
								$allModifiersUsed[$modifier['id']] = 1;
							}
						}
						*/
					}
				}
			}

			// Optional Modifiers

			// Use Optional Modifier on Menu Category level if one exists and there isn't one on the Menu Item-level
			if (empty($menu_item['modifier_list_id']) && !empty($args['category_modifier_list_id']))
			{
				$menu_item['modifier_list_id'] = $args['category_modifier_list_id'];
			}

			if (!empty($menu_item['modifier_list_id']))
			{
				$args['id'] = $menu_item['modifier_list_id'];
				$modifier_category = $this->queryDatabase('modifier_categories_by_id', $args);

				// We map the Lavu optional modifier category (and forced modifier group) to the Pepper modifier
				// but we have to prepend the "id" with the appropriate "namespace" to prevent ID collisions.
				$modifier = array();
				$modifier['id'] = 'optional_modifier_category:'. $modifier_category['id'];
				$modifier['title'] = substr(trim($modifier_category['title']), 0, 40);

				$args['category_id'] = $menu_item['modifier_list_id'];
				$modifier['options'] = $this->getOptionalModifierOptions($args);

				$modifier['minSelect'] = 0;  // characteristic of optional modifiers
				$modifier['maxSelect'] = count($modifier['options']);  // TO DO : restore this and test

				$optionalModifiersUsed[] = $modifier['id'];
				$modifiersUsed[] = $modifier['id'];

				if (!isset($modifiersUsedForCategory[$modifier['id']]))
				{
					$modifiers[] = $modifier;
					$modifiersUsedForCategory[$modifier['id']] = 1;
				}

				$allModifiersUsed[$modifier['id']] = 1;

				/*
				$optional_modifiers = $this->queryDatabase('modifiers_by_category_id', $args);
				foreach ($optional_modifiers as $optional_modifier)
				{
					$args['id'] = $optional_modifier['id'];
					$modifier['options'] = $this->getOptionalModifierOptions($args);

					// Pepper doesn't distinguish between forced and optional modifiers, so we have to prepend the id with the modifier "namespace" to prevent collisions
					$mod_id = 'optional_modifier:'. $optional_modifier['id'];
					$optionalModifiersUsed[] = $mod_id;
					$modifiersUsed[] = $mod_id;
				}
				*/
			}

			$product = array(
				'id' => $menu_item['id'],
				'price' => (double) $menu_item['price'],
				'title' => substr(trim($menu_item['name']), 0, 40),
				'shortDescription' => substr($menu_item['description'], 0, 255),
				'sort' => max(0, (int) $menu_item['_order']),  // -1 prevents menu from loading in Pepper Console
				'tags' => array(),
				'modifiers' => $modifiersUsed,
			);

			if ( !empty($menu_item['image']) )
			{
				$product['imageUrl'] = 'https://admin.poslavu.com/images/'. $args['dataname'] .'/items/main/' . str_replace(' ', '%20', $menu_item['image']);
			}

			// Check for tax_profile_id pass-thru logic to the Category (tax_profile_id = blank) and override the menu_item-level tax_profile_id to make the inclusiveTaxProfile insert simple
			if ($menu_item['tax_profile_id'] == '' && $args['category_tax_profile_id'] != '')
			{
				$menu_item['tax_profile_id'] = $args['category_tax_profile_id'];
			}

			// Tax Profile (Note: tax_profile_id = 0 means "No tax" so it needs to enter this block so it can be assigned to our virtual No Tax tax profile aka taxProfiles[0])
			if ( $menu_item['tax_profile_id'] != '')
			{
				$product['taxScheme'] = $menu_item['tax_profile_id'];

				// Check for tax_inclusion pass-thru logic to the Category or Location tax_inclusion flag and override the menu_item-level tax_inclusion flag to make the $inclusiveTaxProfiles insert simple
				if ($menu_item['tax_inclusion'] == 'Default' && $args['category_tax_inclusion'] == '1')
				{
					$menu_item['tax_inclusion'] = $args['category_tax_inclusion'];
				}
				else if ($menu_item['tax_inclusion'] == 'Default' && $args['category_tax_inclusion'] == 'Default' && $args['location_tax_inclusion'] == '1')
				{
					$menu_item['tax_inclusion'] = $args['location_tax_inclusion'];
				}

				// Remember which menu_items use included tax so we can flag those taxSchemeas as inclusive
				if ( $menu_item['tax_inclusion'] == '1' )
				{
					$inclusiveTaxProfiles[] = $menu_item['tax_profile_id'];
				}
			}

			// We have to remove this field when empty because they were breaking the entire menu import
			if (empty($product['shortDescription'])) unset($product['shortDescription']);

			$products[] = $product;
		}

		return array($products, $modifiers, $inclusiveTaxProfiles);
	}


	/**
	 * Put Lavu Optional Modifier into array with Pepper structure (for JSON conversion)
	 *
	 * @param $args -- Array of input args with required key(s): dataname, id
	 *
	 * @return Array containing object fields that can be JSON encoded
	 */
	public function getOptionalModifierOptions($args)
	{
		$options = array();

		if ( empty($args['dataname']) || empty($args['category_id']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$i = 0;
		$made_first_element_selected = false;
		$optional_modifiers = $this->queryDatabase('modifiers_by_category_id', $args);
		foreach ($optional_modifiers as $optional_modifier)
		{
			if (empty($optional_modifier['_order']))
			{
				$optional_modifier['_order'] = $i;
			}
			else
			{
				$i = (int) $optional_modifier['_order'];
			}

			$option = array(
				'id' => 'optional_modifier:'. $optional_modifier['id'],
				'title' => substr(trim($optional_modifier['title']), 0, 40),
				'price' => (double) $optional_modifier['cost'],
				'sort'  => max(0, (int) $optional_modifier['_order']),
			);


			// One of the modifiers needs to have the selected attribute or else none of them will display in the Pepper app
			if ( !$made_first_element_selected )
			{
				///$option['selected'] = true;
				$made_first_element_selected = true;
			}

			$options[] = $option;

			$i++;
		}

		return $options;
	}

	/**
	 * Put Lavu Forced Modifier into array with Pepper structure (for JSON conversion)
	 *
	 * @param $args -- Array of input args with required key(s): dataname, id
	 *
	 * @return Array containing object fields that can be JSON encoded
	 */
	public function getForcedModifierOptions($args)
	{
		$options = array();

		if ( empty($args['dataname']) || empty($args['list_id']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$i = 0;
		$made_first_element_selected = false;
		$forced_modifiers = $this->queryDatabase('forced_modifiers_by_list_id', $args);
		foreach ($forced_modifiers as $forced_modifier)
		{
			if (empty($forced_modifier['_order']))
			{
				$forced_modifier['_order'] = $i;
			}
			else
			{
				$i = (int) $forced_modifier['_order'];
			}

			$option = array(
				'id' => 'forced_modifier:'. $forced_modifier['id'],
				'title' => substr(trim($forced_modifier['title']), 0, 40),
				'price' => (double) $forced_modifier['cost'],
				'sort' => max(0, (int) $forced_modifier['_order']),
			);

			// One of the modifiers needs to have the selected attribute or else none of them will display in the Pepper app
			if ( !$made_first_element_selected )
			{
				$option['selected'] = true;
				$made_first_element_selected = true;
			}

			$options[] = $option;

			$i++;
		}

		return $options;
	}

	/**
	 * Create Register object for Pepper
	 *
	 * @param $args -- Array of input args with required key(s): title
	 *
	 * @return Array containing object fields that can be JSON encoded
	 */
	public function getRegister($args)
	{
		$obj = array();

		if ( empty($args['title']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$obj['title'] = substr(trim($args['title']), 0, 40);

		return $obj;
	}

	/**
	 * Create array of Register objects for Pepper
	 *
	 * @param $args -- Array of input args with required key(s): dataname
	 *
	 * @return Array of object arrays
	 */
	public function getRegisters($args)
	{
		$obj = array();

		if ( empty($args['dataname']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$registers = $this->queryDatabase('printers', $args);

		return $registers;
	}

	/**
	 * Gets Pepper User object filled with Lavu data
	 *
	 * @param $args -- Array of input args with required key(s): dataname
	 *
	 * @return Array of object arrays
	 */
	public function getUser($args)
	{
		$obj = array();

		if ($args['f_name']) $obj['firstName'] = $args['f_name'];
		if ($args['f_name']) $obj['lastName']  = $args['l_name'];

		return $obj;
	}

	/**
	 * Gets array of Pepper User objects filled with Lavu data
	 *
	 * @param $args -- Array of input args with required key(s): dataname
	 *
	 * @return Array of object arrays
	 */
	public function getUsers($args)
	{
		$obj = array();

		if ( empty($args['dataname']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$users = $this->queryDatabase('users', $args);
		$id_map_row = $this->queryDatabase('config_id_map', $args);
		$id_map = json_encode($id_map_row, true);

		$pepperUsers = array();

		while ( $user = mysqli_fetch_assoc($users) )
		{
			$pepperUsers[] = $this->getUser($args + $user);
		}

		return $pepperUsers;
	}

	/**
	 * Get Order object for Pepper
	 * Pepper's Orders data is equivalent to Lavu's order data
	 *
	 * @param $args -- Array of input args with required key(s): dataname, order_id
	 *
	 * @return Array containing object fields that can be JSON encoded
	 */
	public function getOrder($args)
	{
		$obj = array();

		if ( empty($args['dataname']) || empty($args['order_id']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$order = $this->queryDatabase('order', $args);  // TO DO : see if this should pull split_check_details instead for just the user's check

		$obj['locationId'] = $args['locationId'];  // TO DO : see if this should come from config table, etc.
		$obj['scenario'] = empty($args['scenario']) ? 'PAYATPOS' : $args['scenario'];
		$obj['shortCode'] = $args['order_id'];
		$obj['state'] = empty($args['state']) ? 'OPEN' : $args['state'];
		$obj['totalPrice'] = (double) $order['total'];  //$scd['check_total'];
		$obj['tipAmount'] = (double) $order['cash_tip'] + (double) $order['card_gratuity'] + (double) $order['other_tip'];  // TO DO : change this to function that sums cc_txn.tip_amount for order_id and check
		$obj['totalPrice'] = $obj['totalPrice'] + $obj['tipAmount']; //LP-8836 : Total + Tip
		$obj['tax'] = (double) $order['tax'] + (double) $order['itax'];
		$obj['value'] = (double) $order['total'];

		// TO DO : see if these fields are set from specifying a userId in the API
		//$obj['type'] = '';  // UNUSED
		//$obj['userId'] = '';
		//$obj['userFullName'] = '';
		//$obj['userFirstName'] = '';
		//$obj['userLastName'] = '';

		///$obj['basket'] = $this->getProducts($args);
		$basket = array();
		$products = $this->getProducts($args);
		foreach ($products as $product)
		{
			// Accumulate any points earned onto the Order and remove them from the Product
			if (!empty($product['points']))
			{
				$obj['pointsEarned'] += (int) $product['points'];
				unset($product['points']);
			}

			$basket[] = $product;
		}
		$obj['basket'] = $basket;

		$obj['redemptions'] = $this->getRedemptions($args);

		return $obj;
	}

	/**
	 * Get Products array used in the Order object (which is basically order_contents with modifiers)
	 * Pepper's Orders > Products data is equivalent to Lavu's order_contents data
	 * (although the id field is menu_item.id, not order_contents.id)
	 *
	 * @param $args -- Array of input args with required key(s): dataname, order_id
	 *
	 * @return Array containing array of object fields that can be JSON encoded
	 */

	public function getProducts($args)
	{
		$obj = array();

		if ( empty($args['dataname']) || empty($args['order_id']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		$order_contents = $this->queryDatabase('order_contents', $args);

		foreach ( $order_contents as $order_content )
		{
			// It seems like the id field should've been order_contents.id but it is menu_item.id instead.
			// Seems a little weird (since order_contents contains menu_id), but Pepper doesn't have a quantity field
			// so we, instead, are supposed to repeat Product items quantity number of times.  So it seems weirder to
			// have repeated order_contents.id's than menu_item.id's.
			$item = array();
			$item['id'] = $order_content['item_id'];
			$item['category'] = $order_content['category_id'];
			$item['title'] = substr(trim($order_content['item']), 0, 40);
			$item['modifiers'] = $this->getModifiers($args);

			// LP-7552 - Prevent fully discounted Pepper Items from earning loyalty points/stamps
			if ( ($order_content['idiscount_type'] == 'p' && (1.0 === (double) $order_content['idiscount_value'])) ) {
				$item['useLoyalty'] = true;
			}

			// Get loyalty points for menu item and save it on the Product (even though it's not a real field) so it can be aggregated onto Product
			$args['menu_item_id'] = $order_content['item_id'];
			$menu_item = $this->queryDatabase('menu_item', $args);
			if (!empty($menu_item['loyalty_points'])) $item['points'] = $menu_item['loyalty_points'];

			// Pepper doesn't have a quantity field, they just want us to add the same Product "quantity" number of times
			$quantity = (int) $order_content['quantity'];
			if (!is_numeric($quantity) || $quantity < 0) $quantity = 1;

			// Guard against infinite loops and incorrectly large quantities (capping it at 100)
			for ($i = 0; $i < $quantity && $i < 100; $i++)
			{
				$obj[] = $item;
			}
		}

		return $obj;
	}

	/**
	 * Get Modifiers array used in the Order object
	 * Pepper's Orders > Products > Modifiers data is equivalent to Lavu's modifiers_used data
	 *
	 * @param $args -- Array of input args with required key(s): dataname, order_id, type, list_id, title
	 *
	 * @return Array containing array of object fields that can be JSON encoded
	 */

	public function getModifiers($args)
	{
		$obj = array();

		$modifiers_used = $this->queryDatabase('modifiers_used', $args);

		foreach ( $modifiers_used as $modifier_used )
		{
			// For call to getOptions() method in which we pass $modifier_used instead of $args
			$modifier_used['dataname'] = $args['dataname'];

			$modifier = array();
			///$modifier['id'] = $modifier_used['type'] .':'. $modifier_used['list_id'];  // (optional|forced):(modifier_categories|forced_modifier_list).id
			$modifier['id'] = $modifier_used['list_id'];
			$modifier['title'] = substr(trim($modifier_used['title']), 0, 40);  // (modifier_categories|forced_modifier_list).title
			$modifier['options'] = $this->getOptions($modifier_used);  // modifier_used has order_id, mod_id, and type

			$obj[] = $modifier;
		}

		return $obj;
	}

	/**
	 * Get Options array used in the Order object's Modifiers array.
	 * Pepper's Orders > Products > Modifiers > Options data is equivalent to Lavu's modifiers|forced_modifiers data
	 *
	 * @param $args -- Array of modifiers_used row with required key(s): dataname, order_id, type, mod_id
	 *
	 * @return Array containing array of object fields that can be JSON encoded
	 */

	public function getOptions($args)
	{
		$obj = array();

		$query_name = ($args['type'] == 'forced') ? 'forced_modifiers-mod_id' : 'modifiers_by_mod_id';

		$modifiers = $this->queryDatabase($query_name, $args);

		foreach ( $modifiers as $modifier )
		{
			$option = array();
			///$option['id'] = $args['type'] .':'. $modifier['id'];  // (optional|forced):(modifiers|forced_modifiers).id
			$option['id'] = $modifier['id'];
			$option['title'] = substr(trim($modifier['title']), 0, 40);  // (modifiers|forced_modifier_list).title
			$option['price'] = $modifier['cost'];   // (modifier_categories|forced_modifier_list).cost

			$obj[] = $option;
		}

		return $obj;
	}

	/**
	 * Get Redemptions array used in the Order object
	 *
	 * @param $args -- Array of input args with required key(s): dataname, order_id, check
	 *
	 * @return Array containing array of object fields that can be JSON encoded
	 */

	public function getRedemptions($args)
	{
		$obj = array();

		if ( empty($args['dataname']) || empty($args['order_id']) || empty($args['check']) )
		{
			error_log(__METHOD__ .'() missing required elements from passed-in args: '. json_encode($args));
			return $obj;
		}

		// Allow redemptions array to be passed-in to supplement db values
		if (!empty($args['redemptions']) && is_array($args['redemptions']))
		{
			$obj = $args['redemptions'];
		}
		else
		{
			// split_check_details.discount_info|order_contents.idiscount_info = pepper|(check|item)|(value)|(quantity)|(token)|(awardId)
			$redemption = array();
			$split_check_details = $this->queryDatabase('split_check_details', $args);
			if (!empty($split_check_details['discount_info']))
			{
				// Check-level discount
				$discount_info = explode('|', $split_check_details['discount_info']);
				if (!empty($discount_info))
				{
					// For token
					$discount_type = $this->queryDatabase('discount_type', array('dataname' => $args['dataname'], 'id' => $split_check_details['discount_id']));

					// For quantity
					$matches = array();
					preg_match('/^(\d+) /', $split_check_details['discount_sh'], $matches);

					// split_check_details.discount_info = more info|check|<awardId>
					$redemption['awardId']  = isset($discount_info[2]) ? $discount_info[2] : '';
					$redemption['token']    = $discount_type['token'];
					$redemption['quantity'] = $matches[1];
					$redemption['value']    = $split_check_details['discount'];

					$obj[] = $redemption;
				}
			}
			else
			{
				// Check for item-level discount (idiscount)
				$order_content = $this->queryDatabase('order_contents_idiscount', $args);
				if (!empty($order_content))
				{
					$idiscount_info = explode('|', $order_content['idiscount_info']);

					if (!empty($idiscount_info))
					{
						// For discount_type.token
						$discount_type = $this->queryDatabase('discount_type', array('dataname' => $args['dataname'], 'id' => $order_content['idiscount_id']));

						// For quantity
						$matches = array();
						preg_match('/^(\d+) /', $order_content['idiscount_sh'], $matches);

						// order_contents.idiscount_info = more info|item|<awardId>
						$redemption['awardId']  = isset($idiscount_info[2]) ? $idiscount_info[2] : '';
						$redemption['token']    = $discount_type['token'];
						$redemption['quantity'] = $matches[1];
						$redemption['value']    = $order_content['idiscount_amount'];

						$obj[] = $redemption;
					}
				}
			}
		}

		return $obj;
	}


	/**
	 * Get single Transaction using passed-in vars - used to update the Order object after the Order has been created.
	 * Pepper's Orders > Transactions data is equivalent to Lavu's cc_transactions data
	 *
	 * @param $args -- Array of args
	 *
	 * @return Array containing object which can be JSON encoded
	 */

	public function getTransaction($args)
	{
		$obj = array();

		$split_check_details = $this->queryDatabase('split_check_details', $args);
		$cc_transactions = $this->queryDatabase('cc_transactions', $args);

		$obj['locationId']  = $args['locationId'];
		$obj['registerId']  = $args['registerId'];
		$obj['orderId']     = $args['orderId'];
		$obj['shortCode']   = $args['order_id'];
		$obj['paymentType'] = empty($args['paymentType']) ? 'POS' : strtoupper($args['paymentType']);  // CASH|CARD|POS|APP (Careful: APP charges card on file!  And this field being blank in payload will cause weird error)

		$obj['subTotal']    = (double) $split_check_details['subtotal'];
		$obj['totalPrice']  = (double) $split_check_details['check_total'];
		$obj['totalTax']    = (double) $split_check_details['tax'] + (double) $split_check_details['itax'];

		$obj['totalPayment'] = 0.00;
		$obj['totalTip']     = 0.00;
		foreach ( $cc_transactions as $cc_transaction )
		{
			$obj['totalPayment'] += (double) $cc_transaction['amount'];
			$obj['totalTip']     += (double) $cc_transaction['tip_amount'];

			$obj['paymentType'] = strtoupper($args['pay_type']);  // CASH|CARD|APP (Careful: APP charges card on file in the Pepper app!)
		}

		return $obj;
	}


	/**
	 * Get array of Transactions containing all Lavu payments - used to update the Order object after the Order has been created.
	 * Pepper's Orders > Transactions data is equivalent to Lavu's cc_transactions data
	 *
	 * @param $args -- Array of args
	 *
	 * @return Array containing object which can be JSON encoded
	 */

	public function getTransactions($args)  // TO DO : finish this
	{
		$objs = array();

		$cc_transactions = $this->queryDatabase('cc_transactions', $args);
		$split_check_details = $this->queryDatabase('split_check_details', $args);

		foreach ( $cc_transactions as $cc_transaction )
		{
			// Skip cc_transactions that already have Pepper Transactions created for them
			if (!empty($cc_transaction['transaction_id'])) continue;

			$obj = array();
			$obj['locationId']   = $args['locationId'];
			$obj['registerId']   = $args['registerId'];
			$obj['orderId']      = $args['orderId'];
			$obj['shortCode']    = $args['order_id'];

			$obj['paymentType']  = strtoupper($cc_transaction['paymentType']);  // CASH|CARD|APP (Careful: APP charges card on file in the Pepper app!)
			$obj['subTotal']     = (double) $split_check_details['subtotal'];
			$obj['totalPrice']   = (double) $split_check_details['check_total'] - (double) $split_check_details['alt_paid'];  // Per Adam @ Pepper, the totalPrice should be after any points payment
			$obj['totalTax']     = (double) $split_check_details['tax'] + (double) $split_check_details['itax'];  // TO DO : see if this needs to be adjusted by the portion of the split_check_details[total_check] this payment covers so we don't send the same tax amount on every order
			$obj['totalPayment'] = (double) $cc_transaction['amount'];
			$obj['totalTip']     = (double) $cc_transaction['tip_amount'];

			$objs[] = $obj;
		}

		return $objs;
	}


	/**
	 * Get array of Transactions for Order that already exist in Lavu cc_tranactions.
	 * Pepper's Orders > Transactions data is equivalent to Lavu's cc_transactions data
	 *
	 * @param $args -- Array of modifiers_used row with required key(s): locationId, orderId, registerId, dataname, order_id, check
	 *
	 * @return Array containing array of object fields that can be JSON encoded
	 */

	public function refundTransactions($args)
	{
		$obj = array();

		$cc_transactions = $this->queryDatabase('cc_transactions', $args);

		foreach ( $cc_transactions as $cc_transaction )
		{
			if (!empty($cc_transaction['voided'])) continue;
			$obj['id'] = $cc_transaction['transaction_id'];
			$obj['refund'] = true;
			$obj['refundAmount'] = (double) $cc_transaction['amount'];
		}

		return $obj;
	}


	/**
	 * Handles all database queries and connections
	 *
	 * @param  $query_name String identifying query to run
	 * @param  $args Array of input arguments, if needed for query
	 *
	 * @return mixed Returns mysql resource when $return_single_row is false, array when $return_single_row is true, bool for inserts, updates, failures
	 */
	private function queryDatabase($query_name, $args = array())
	{
		$return_single_row = true;
		$return_all_rows_array = false;
		$die_on_zero_rows  = true;
		$die_on_db_errors  = true;
		$update_or_insert  = false;

		switch ( $query_name )
		{
			case 'cc_transactions':
				$sql = "SELECT `ioid` AS `payment_id`, `total_collected` AS `amount`, `voided` AS `void`, `pay_type` AS `type`, `datetime` AS `paymenttime`, `cc_transactions`.* FROM `poslavu_[dataname]_db`.`cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `action` = 'Sale'";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'config_id_map':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `id` = '[loc_id]'";
				$return_single_row = true;
				break;

			case 'config_customer_management_fields':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `setting` = 'GetCustomerInfo' AND `location` = [loc_id] AND `_deleted` = 0";
				$return_single_row = true;
				break;

			case 'discount_type':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`discount_types` WHERE `id` = '[id]'";
				$die_on_zero_rows = false;
				$return_single_row = true;
				$return_all_rows_array = false;
				break;

			case 'forced_modifiers-mod_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`forced_modifiers` WHERE `id` = '[mod_id]' ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'forced_modifier_groups_by_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`forced_modifier_groups` WHERE `id` = '[id]'";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'forced_modifiers_by_list_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`forced_modifiers` WHERE `list_id` = '[list_id]' AND `_deleted` = 0 ORDER BY `_order`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'forced_modifier_lists_by_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`forced_modifier_lists` WHERE `id` = '[id]'";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'location':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`locations` WHERE `id` = '[loc_id]'";
				$return_single_row = true;
				break;

			case 'location_highest_nondeleted':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`locations` WHERE `_disabled` = 0 ORDER BY `id` DESC";
				$return_single_row = true;
				$die_on_zero_rows  = false;
				break;

			case 'location_config_settings':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `location` = '[loc_id]' AND `type` = 'location_config_setting' AND `setting` LIKE 'pepper_%' AND `_deleted`=0";
				$return_single_row = false;
				$die_on_zero_rows  = false;
				break;

			case 'menu_item':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`menu_items` WHERE `id` = '[menu_item_id]'";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'menu_items_by_category_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`menu_items` WHERE `category_id` = [category_id] AND (`loyalty_display` = 1 AND '1' = '[category_loyalty_display]') AND `_deleted` = 0 ";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'menu_categories':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`menu_categories` WHERE `loyalty_display` = 1 AND `_deleted` = 0 AND `active` = 1 ORDER BY `_order`, `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'menu_categories_from_nondeleted_menu_groups':
				$sql = "SELECT `mc`.* FROM `poslavu_[dataname]_db`.`menu_groups` `mg`, `poslavu_[dataname]_db`.`menu_categories` `mc` WHERE `mg`.id = `mc`.`group_id` AND `mg`.`menu_id` = [menu_id] AND `mc`.`loyalty_display` = 1 AND `mg`.`_deleted` = 0 AND `mc`.`_deleted` = 0 ORDER BY `mc`.`_order`, `mc`.`id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'menu_categories_by_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`menu_categories` WHERE `id` = '[id]' ORDER BY `_order`, `id`";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'modifiers_by_category_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`modifiers` WHERE `category` = '[category_id]' AND `_deleted` = 0 ORDER BY `_order`, `title`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'modifiers_by_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`modifiers` WHERE `id` = '[id]' ORDER BY `id`";  // No _deleted because we may have to pull historical data (instead of just active data)
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'modifiers_by_mod_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`modifiers` WHERE `id` = '[mod_id]' ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'modifier_categories_by_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`modifier_categories` WHERE `id` = '[id]'";
				$die_on_zero_rows  = false;
				$return_single_row = true;
				break;

			case 'modifiers_used':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`modifiers_used` WHERE `order_id` = '[order_id]' ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'restaurant':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[dataname]' ORDER BY `id`";
				break;

			case 'signup':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname` = '[dataname]' ORDER BY `id`";
				break;

			case 'printers':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `location` ='[loc_id]' AND `type` = 'printer' AND `_deleted` != '1'";  // From CP query in admin/cp/areas/printers_kds.php
				$die_on_zero_rows = false;
				$return_single_row = false;
				$return_all_rows_array = true;
				break;

			case 'order':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`orders` WHERE `order_id` = '[order_id]'";
				$die_on_zero_rows  = false;
				break;

			case 'order_contents':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`order_contents` WHERE `order_id` = '[order_id]' ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'order_contents_idiscount':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`order_contents` WHERE `order_id` = '[order_id]' AND `check` = '[check]' AND `idiscount_info` != ''  ORDER BY `id`";
				$die_on_zero_rows = false;
				$return_single_row = true;
				$return_all_rows_array = false;
				break;

			case 'split_check_details':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`split_check_details` WHERE `order_id` = '[order_id]' AND `check` = '[check]'";
				$die_on_zero_rows = false;
				$return_single_row = true;
				$return_all_rows_array = false;
				break;

			case 'tax_profiles':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`tax_profiles` WHERE `_deleted` = 0 ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'tax_rates_by_profile_id':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`tax_rates` WHERE `profile_id` = [profile_id] AND `_deleted` = 0 ORDER BY `id`";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			default:
				die(__METHOD__ ." got unrecognized query {$query_name} for dataname {$this->dataname}\n");
				break;
		}

		$result = mlavu_query($sql, $args);

		if ( $result === false && $die_on_db_errors )
		{
			throw new Exception("Database error - {$query_name} query got: ". mysqli_error() ."\n");
		}
		else if ( $result !== false && $return_all_rows_array )
		{
			$all_rows = array();
			while ( $row = mysqli_fetch_assoc($result) )
			{
				$all_rows[] = $row;
			}
			return $all_rows;
		}
		else if ( $update_or_insert )
		{
			return $result;
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			error_log("No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
			throw new Exception("Database error - No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}

}