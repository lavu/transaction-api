<?php

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperConfig.php');
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperDataExtractor.php');

class PepperApi
{
	public $session_id;
	public $application_id;
	public $endpoint_url_base;
	public $locationId;
	public $pepperLocationConfigSettings;

	//LP-10786
	public $referer;

	public function __construct($args)
	{

		//LP-10786
		if (isset($args['referer'])) {
			$this->referer = $args['referer'];
		}

		// If a dataname is passed in, get the API credentials from the POS database
		if ( !empty($args['dataname']) )
		{
			$dataExtractor = new PepperDataExtractor();
			$this->pepperLocationConfigSettings = $dataExtractor->getLocationConfigSettings($args);

			// Make sure we have the input arguments we need to make the Pepper API call.  Otherwise, get them or die trying.
			if ( (empty($this->pepperLocationConfigSettings['apitoken']) && empty($args['apitoken'])) || (empty($this->pepperLocationConfigSettings['applicationid']) && empty($args['applicationid'])) )
			{
				throw new Exception("Could not get Pepper API credentials: {$method_name_full}");
			}

			// This will keep any array elements from $args but add any location config settings that don't conflict
			$args = array_merge($args, $this->pepperLocationConfigSettings);

			$this->session_id = $args['apitoken'];
			$this->application_id = $args['applicationid'];
			$this->locationId = $args['locationId'];
			$this->endpoint_url_base = (getenv('ENVIRONMENT') != 'production') ? PepperConfig::API_ENDPOINT_BASE_BETA : PepperConfig::API_ENDPOINT_BASE;
		}
	}

	/**
	 * All API calls are routed through this PHP magic method instead of real functions existing for each one
	 *
	 * @param  $method_name String containing method_name used when invoking method (standard 1st arg of PHP magic method)
	 * @param  $method_args String|Array containing any arguments used when invoking method (standard 2nd arg of PHP magic method)
	 *
	 * @return Array containing API response
	 */
	public function __call($method_name, $method_args)
	{
		$response = array();
		$pattern_matches = array();

		$method_name_full = __CLASS__ ."->{$method_name}()";
		//echo "DEBUG: method_name_full={$method_name_full}\n";  //debug
		//echo "DEBUG: method_args=". print_r($method_args, true);  //debug

		// The __call magic method passes n input arguments as an array, so we will use
		// the convention of assuming the first array element is our $arg array, if passed
		$args = isset($method_args[0]) ? $method_args[0] : array();

		// Auto-inject the pepper_* config rows we gathered from the POS db upon instantiation (but do not override passed-in vars)
		$args = array_merge($args, $this->pepperLocationConfigSettings);

		// Use regex to extract API action and object from name of called method (ex. createLocation)
		if ( preg_match('/^(create|delete|get|update|activate|grant|login|logout|refund|reset)([A-Z].*)$/', $method_name, $pattern_matches) )
		{
			$args['action']   = $pattern_matches[1];
			$args['obj_name'] = $pattern_matches[2];

			// Validate method names are correct - throw Exception to guard against typos
			if ( empty($args['action']) || empty($args['obj_name']) )
			{
				throw new Exception("Method called without valid action or obj: {$method_name_full}");
			}

			// Action-specific logic
			switch ( $args['action'] )
			{
				case 'create':
				case 'update':
					// API users can specify their own JSON payload in request_json, pass "extract-data" set to a non-empty value,
					// or default to having the args array turned into their payload (minus "action" and "obj_name")
					if ( !empty($args['extract-data']) )
					{
						// Use data extractor class to create Pepper object filled with Lavu data, if possible
						$dataExtractor = new PepperDataExtractor();
						$extractor_method_name = 'get'. $args['obj_name'];
						if ( method_exists($dataExtractor, $extractor_method_name) )
						{
							$obj = $dataExtractor->$extractor_method_name($args);
							//echo "DEBUG: extracted obj=". print_r($obj, true);  //debug

							$args['request_json'] = json_encode($obj);
						}
					}
					else if ( empty($args['request_json']) )
					{
						$data = $args;

						// Remove our internal meta data that was getting passed to Pepper and causing problems
						unset($data['action']);
						unset($data['obj_name']);

						$args['request_json'] = json_encode($data);
					}
					break;
			}

			// Send request to Pepper REST API
			$response_json = $this->sendApiRequest($args);
			$response = json_decode($response_json, true);
		}
		else
		{
			//echo "DEBUG: {$method_name_full} is an unknown method type\n";  //debug
			throw new Exception('Unknown '. __CLASS__ .' method called '. $method_name_full);
		}

		return $response;
	}

	/**
	 * Get URL path for API endpoint
	 *
	 * @param  $args Array of input args with required key(s): action, obj_name, and others that are specific to action + obj_name combo
	 *
	 * @return String of API endpoint URL path
	 */
	private function getApiEndpointUrlPath($args)
	{
		$api_endpoint_url_path = '';

		switch ( $args['action'] . $args['obj_name'] )
		{
			// Location
			case 'createLocation':
				$api_endpoint_url_path = "/locations";
				$has_required_fields = true;
				break;
			case 'getLocations':
				$api_endpoint_url_path = "/locations?summary=true";
				$has_required_fields = true;
				break;
			case 'getLocation':
			case 'updateLocation':
			case 'deleteLocation':
				if (empty($args['locationId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/locations/{$args['locationId']}";
				break;

			// Register
			case 'createRegister':
			case 'getRegisters':
				$api_endpoint_url_path = "/locations/{$args['locationId']}/registers";
				break;
			case 'getRegister':
			case 'updateRegister':
			case 'deleteRegister':
				if (empty($args['locationId']) || empty($args['registerId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/locations/{$args['locationId']}/registers/{$args['registerId']}";
				break;

			// Menu
			case 'createMenu':
			case 'getMenus':
				$api_endpoint_url_path = "/menus";
				break;
			case 'getMenuForLocation':
				if (empty($args['locationId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/locations/{$args['locationId']}/menu";
				break;
			case 'getMenu':
			case 'updateMenu':
			case 'deleteMenu':
				if (empty($args['menuId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}";
				break;

			// Schedule
			case 'createSchedule':
			case 'getSchedules':
				if (empty($args['menuId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/schedules";
				break;
			case 'getSchedule':
			case 'updateSchedule':
			case 'deleteSchedule':
				if (empty($args['menuId']) || empty($args['scheduleId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/schedules/{$args['scheduleId']}";
				break;

			// Channel
			case 'createChannel':
			case 'getChannels':
				if (empty($args['menuId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/channels";
				break;
			case 'getChannel':
			case 'updateChannel':
			case 'deleteChannel':
				if (empty($args['menuId']) || empty($args['scheduleId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/channels/{$args['scheduleId']}";
				break;

			// TaxScheme
			case 'createTaxScheme':
			case 'getTaxSchemes':
				if (empty($args['menuId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/taxschemes";
				break;
			case 'getTaxScheme':
			case 'updateTaxScheme':
			case 'deleteTaxScheme':
				if (empty($args['menuId']) || empty($args['taxschemeId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/taxschemes/{$args['taxschemeId']}";
				break;

			// Category
			case 'createCategory':
			case 'getCategories':
				if (empty($args['menuId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/categories";
				break;
			case 'getCategory':
			case 'updateCategory':
			case 'deleteCategory':
				if (empty($args['menuId']) || empty($args['categoryId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/categories/{$args['categoryId']}";
				break;

			// Product
			case 'createProduct':
			case 'getProducts':
				if (empty($args['menuId']) || empty($args['categoryId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/categories/{$args['categoryId']}/products";
				break;
			case 'getProduct':
			case 'updateProduct':
			case 'deleteProduct':
				if (empty($args['menuId']) || empty($args['categoryId']) || empty($args['productId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/categories/{$args['categoryId']}/products/{$args['productId']}";
				break;

			// Modifier
			case 'createModifier':
			case 'getModifiers':
				if (empty($args['menuId']) || empty($args['categoryId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/categories/{$args['categoryId']}/modifiers";
				break;
			case 'getModifier':
			case 'updateModifier':
			case 'deleteModifier':
				if (empty($args['menuId']) || empty($args['categoryId']) || empty($args['modifierId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/menus/{$args['menuId']}/categories/{$args['categoryId']}/modifiers/{$args['modifierId']}";
				break;

			// Order
			case 'createOrder':
			case 'getOrders':
				if ( !empty($args['userId']) )
				{
					$api_endpoint_url_path = "/users/{$args['userId']}/orders";
				}
				else if ( !empty($args['locationId']) )
				{
					$api_endpoint_url_path = "/locations/{$args['locationId']}/orders";  // TO DO : see if this is really actions
				}
				else
				{
					throw new Exception("Missing required field in args:". json_encode($args));
				}
				break;
			case 'getOrder':
			case 'updateOrder':
			case 'deleteOrder':
				if (empty($args['orderId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/orders/{$args['orderId']}";
				break;

			// Card
			case 'createCard':
				$api_endpoint_url_path = "/cards";
				break;
			case 'getCards':
				if ( !empty($args['userId']) )
				{
					$api_endpoint_url_path = "/users/{$args['userId']}/cards";
				}
				else
				{
					$api_endpoint_url_path = "/cards";
				}
				break;
			case 'getCard':
			case 'updateCard':
			case 'deleteCard':
				if (empty($args['CardId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/cards/{$args['cardId']}";
				break;

			// Perk
			case 'createPerk':
			case 'getPerks':
				$api_endpoint_url_path = "/perks";
				break;
			case 'getPerk':
			case 'updatePerk':
			case 'deletePerk':
				if (empty($args['perkId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/perks/{$args['perkId']}";
				break;
			case 'grantPerk':
				if (empty($args['perkId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/perks/{$args['perkId']}/awards";
				break;

			// Award
			case 'getAwards':
				if (empty($args['userId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/users/{$args['userId']}/pointawards";
				break;
			case 'updateAwards':  // TO DO : see if this is really used (I think it was abandoned)
				if (empty($args['awardId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/awards/{$args['awardId']}";
				break;

			// Credits
			case 'createCredits':
			case 'getCredits':
				$api_endpoint_url_path = "/users/{$args['userId']}/credits";
				break;

			// Transaction
			case 'createTransaction':
				if ( !empty($args['userId']) )
				{
					$api_endpoint_url_path = "/users/{$args['userId']}/transactions";
				}
				else
				{
					$api_endpoint_url_path = "/transactions";
				}
				break;
			case 'getTransactions':
				if ( !empty($args['userId']) )
				{
					$api_endpoint_url_path = "/users/{$args['userId']}/transactions";
				}
				else if ( !empty($args['locationId']) )
				{
					$api_endpoint_url_path = "/locations/{$args['locationId']}/transactions";
				}
				else
				{
					throw new Exception("Missing required field in args:". json_encode($args));
				}
				break;
			case 'getTransaction':
			case 'updateTransaction':
			case 'deleteTransaction':
			case 'refundTransaction':
				if (empty($args['transactionId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/transactions/{$args['transactionId']}?refund=true";
				break;

			// User
			case 'createUser':
			case 'getUsers':
				$api_endpoint_url_path = "/users";
				if (!empty($args['name'])) $api_endpoint_url_path = "/users?name=". urlencode($args['name']);
				break;
			case 'getUser':
			case 'updateUser':
			case 'deleteUser':
				if (empty($args['userId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/users/{$args['userId']}";
				break;
			case 'activateUser':
				$api_endpoint_url_path = "/users/activate";
				break;
			case 'loginUser':
				if (empty($args['userId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/users/{$args['userId']}/login";
				break;
			case 'logoutUser':
				if (empty($args['userId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/users/{$args['userId']}/logout";
				break;
			case 'resetUser':
				$api_endpoint_url_path = "/users/reset";
				break;

			// UserCredentials
			case 'getUserCredentials':
				if (empty($args['userId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/users/{$args['userId']}/credentials";
				break;

			// Checkins
			case 'createCheckin':
			case 'getCheckins':
				$api_endpoint_url_path = "/checkins";
				break;
			case 'createCheckinForUser':
			case 'getCheckinsForUser':
				if (empty($args['userId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/users/{$args['userId']}/checkins";
				break;
			case 'getCheckinsForLocation':
				if (empty($args['locationId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/locations/{$args['locationId']}/checkins";
				break;
			case 'updateCheckin':
				if (empty($args['checkinId'])) throw new Exception("Missing required field in args:". json_encode($args));
				$api_endpoint_url_path = "/checkins/{$args['checkinId']}";
				break;



			// TO DO -- rest of objs (Messaging, Referrals, Segments, Surveys, Topups)

			default:
				throw new Exception("Unknown api_action or obj_name in args:". json_encode($args));
				break;

		}

		return $api_endpoint_url_path;
	}

	/**
	 * Send Pepper API request via cURL
	 *
	 * @param  $args Array of args with required key(s): api_action, endpoint_url_path, request_json
	 *
	 * @return JSON response string
	 */
	private function sendApiRequest($args)
	{
		$curl_request = curl_init();

		//echo "DEBUG: action=". $args['action'] ."\n";  //debug

		switch ( $args['action'] )
		{
			case 'create':
			case 'grant':     // Perk
			case 'activate':  // User
			case 'reset':     // User
				curl_setopt($curl_request, CURLOPT_POST, true);
				curl_setopt($curl_request, CURLOPT_POSTFIELDS, $args['request_json']);
				break;

			case 'update':
			case 'refund':  // Transaction
				curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($curl_request, CURLOPT_POSTFIELDS, $args['request_json']);
				break;

			case 'delete':
				curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, 'DELETE');
				curl_setopt($curl_request, CURLOPT_POSTFIELDS, $args['request_json']);
				break;

			case 'read':
			case 'login':
			case 'logout':
			default:
				curl_setopt($curl_request, CURLOPT_POST, false);
				break;
		}

		$endpoint_url = $this->endpoint_url_base . $this->getApiEndpointUrlPath($args);

		//error_log("DEBUG: API endpoint_url={$endpoint_url} action={$args['action']} request_json={$args['request_json']}");  //debug

		curl_setopt($curl_request, CURLOPT_URL, $endpoint_url);
		curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl_request, CURLOPT_HTTPHEADER, array(
			'Accept: application/json',
			'Content-Type: application/json',
			'Authorization: Token '. $this->session_id,
			'X-Application-Id: '. $this->application_id,
			'X-Client-Platform: '. PepperConfig::API_CLIENT_PLATFORM,  // Per Danny @ Pepper 2018-02-15
			'X-Client-Version: '. PepperConfig::API_CLIENT_VERSION,    // Per Danny @ Pepper 2018-02-15
		));

		// For Danny's request to get full header info for debugging 502 error in sandbox web console
		//curl_setopt($curl_request, CURLINFO_HEADER_OUT, true);  //debug

		// For my own attempts to retrieve response header *and* body
		//curl_setopt($curl_request, CURLOPT_VERBOSE, true);  //debug
		curl_setopt($curl_request, CURLOPT_HEADER, true);

		//error_log(__FILE__ ." DEBUG: request_json=". $args['request_json']);  //debug

		$response = curl_exec($curl_request);

		//error_log(curl_getinfo($curl_request, CURLINFO_HEADER_OUT));  //debug

		//error_log("DEBUG: API response={$response}");  //debug

		// For my own attempts to retrieve response header *and* body
		$header_size = curl_getinfo($curl_request, CURLINFO_HEADER_SIZE);
		$response_header = substr($response, 0, $header_size);
		$response_json   = substr($response, $header_size);
		$response_json_array = json_decode($response_json, true);
		if ( isset($response_json_array['code']) && $args['action'] != 'refund') {
			//LP-10786
			if ($this->referer == 'webview') {
				errorView();
			} else {
				error_log("Pepper: API response_json for LDSSyncer (File: PepperApi.php)=". $response_json);
			}
		}
		//error_log("DEBUG: API response_json=". $response_json);  //debug

		curl_close($curl_request);

		return $response_json;
	}

}
