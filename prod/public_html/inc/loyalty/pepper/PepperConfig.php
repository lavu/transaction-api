<?php

class PepperConfig
{
	const APP = 'pepper';
	const MODULE = 'extensions.loyalty.pepper';
	const COMPONENT_PACKAGE_ID = 62;
	const COMPONENT_LAYOUT_ID = 112;
	const PAYMENT_EXTENSION_ID_APP = 113;
	const PAYMENT_EXTENSION_ID_POINTS = 113;
	const API_ENDPOINT_BASE = 'https://api.pepperhq.com';
	const API_ENDPOINT_BASE_BETA = 'https://beta-api.pepperhq.com';
	const API_CLIENT_PLATFORM = 'IOS';
	const API_CLIENT_VERSION = '3.35.0.buildnumber';
}
