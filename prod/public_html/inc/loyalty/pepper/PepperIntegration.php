<?php

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperDataExtractor.php');

class PepperIntegration
{
	/**
	 * Check if dataname has Pepper enabled
	 *
	 * @param $args Associative array containing input arguments
	 *
	 * @return Boolean - whether Pepper is enabled
	 */
	public static function isEnabled($args)
	{
		$pepper_enabled = false;

		// TO DO : check module
		// TO DO : check for config row

		return $pepper_enabled;
	}

	/**
	 * Onboard dataname with Pepper
	 *
	 * @param $args Associative array containing input arguments
	 *
	 * @return Boolean - whether Pepper onboarding succeeded
	 */
	public static function enable($args)
	{
		// TO DO : checkreate config row (check for _deleted row)
		// TO DO : add module

		// TO DO : checkreate Location
		// TO DO : checkreate Register
	}

	/**
	 * Disable Pepper onboarding
	 *
	 * @param $args Associative array containing input arguments
	 *
	 * @return Boolean - whether Pepper onboarding succeeded
	 */
	public static function disable($args)
	{
		// TO DO : _delete config row
		// TO DO : remove module
	}

	/**
	 * Deduct Pepper points for user
	 *
	 * @param $args Associative array containing input arguments
	 *
	 * @return Boolean - whether Pepper onboarding succeeded
	 */
	public static function redeemPoints($args)
	{
		$results = array(
			// TO DO : see what endpoint returns and put placeholders for those reponse vars here (?)
			'success'            => false,
			'error'              => '',
		);

		if (empty($args['awardId']) || empty($args['valueToRedeem']) || empty($args['dataname']))
		{
			$results['error'] = __METHOD__ .'() missing required field';
			error_log($results['error'] ." with args ". json_encode($args));
			return $results;
		}

		$pepperApi = new PepperApi($args);
		$response = $pepperApi->updateAwards($args);

		if (empty($response['message']))
		{
			$response['success'] = true;
			$response['message'] = 'Points pay successful';
		}
		else
		{
			$results['error'] = $response['message'];
		}

		return $results;
	}

	public static function sendMenu($args)
	{
		$results = array(
			'menuId'             => '',
			'error'              => '',
			'success'            => false,
			'extractedMenu'      => false,
			'updatedMenu'        => false,
			'createdMenu'        => false,
			'createdCategories'  => false,
			'createdProducts'    => false,
			'createdModifiers'   => false,
			'createdTaxSchemes'  => false,
		);

		$pepperApi = new PepperApi($args);
		$pepperDataExtractor = new PepperDataExtractor($args);

		// Extract Lavu menu data into Pepper format
		$menu = $pepperDataExtractor->getMenu($args);

		// TO DO : make sure $menu contains valid data necessary to proceed with creating Pepper Menu
		if (count($menu))
		{
			$results['extractedMenu'] = true;
		}
		else
		{
			$results['error'] = 'Could not extract data for menu';
			return $results;
		}

		// Use a temp variable to construct the JSON payload for creating the menu (since
		// $menu has too many extra keys, which seems to prevent it from working)
		$menuPayload = array();
		$menuPayload['title'] = $menu['title'];
		//if (!empty($menu['description'])) $menuPayload['description'] = $menu['description'];

		// Create empty Pepper Menu
		$args['request_json'] = json_encode($menuPayload);
		$pepperMenu = $pepperApi->createMenu($args);

		if ( empty($pepperMenu['id']) )  // TO DO : should this be _id (?)
		{
			$results['error'] = $pepperMenu['message'];
			error_log("Could not create Pepper Menu with args ". json_encode($args) .": ". $results['error']);
			return $results;
		}
		else
		{
			$args['menuId']    = $pepperMenu['id'];  // TO DO : should this be _id (?)
			$results['menuId'] = $pepperMenu['id'];  // TO DO : should this be _id (?)
			$results['createdMenu'] = true;
		}

		// Create Categories (and their Products, Modifiers) for Pepper Menu
		foreach ( $menu['categories'] as $category )
		{
			$category['menuId'] = $pepperMenu['menuId'];
			$pepperCategory = $pepperApi->createCategory($category);

			if (empty($pepperCategory['id']))
			{
				$results['error'] = $pepperCategory['message'];
				$results['createdCategories'] = false;
				error_log("Could not create Pepper Category ". json_encode($category) .": ". $results['error']);
				break;
			}
			else
			{
				$results['createdCategories'] = true;
				$results['categoryIds'][] = $pepperCategory['id'];

				// Create Products
				foreach ( $category['products'] as $product )
				{
					// Create Products
					$product['categoryId'] = $pepperCategory['id'];
					$pepperProduct = $pepperApi->createProduct($category);

					if (empty($pepperProduct['id']))
					{
						$results['error'] = $pepperProduct['message'];
						$results['createdProducts'] = false;
						error_log("Could not create Pepper Product ". json_encode($product) .": ". $results['error']);
						break 2;
					}
					else
					{
						$results['createdProducts'] = true;
					}
				}

				// Create Modifiers
				foreach ( $category['modifiers'] as $modifier )
				{
					// Create Modifier
					$modifier['menuId'] = $pepperMenu['menuId'];
					$modifier['categoryId'] = $pepperCategory['id'];
					$pepperModifier = $pepperApi->createModifier($modifier);

					if (empty($pepperModifier['id']))
					{
						$results['error'] = $pepperModifier['message'];
						$results['createdModifiers'] = false;
						error_log("Could not create Pepper Modifier ". json_encode($modifier) .": ". $results['error']);
						break 2;
					}
					else
					{
						$results['createdModifiers'] = true;
					}
				}
			}
		}

		// Create Tax Schemes
		foreach ( $pepperMenu['options']['taxSchemes'] as $taxScheme )
		{
			$taxScheme['menuId'] = $pepperMenu['menuId'];
			$pepperTaxScheme = $pepperApi->createTaxScheme($taxScheme);

			if (empty($pepperTaxScheme['id']))
			{
				$results['error'] = $pepperTaxScheme['message'];
				$results['createdTaxSchemes'] = false;
				error_log("Could not create Pepper Modifier ". json_encode($taxScheme) .": ". $results['error']);
				break;
			}
			else
			{
				$results['createdTaxSchemes'] = true;
			}
		}

		$results['success'] = ($results['createdMenu'] && $results['extractedMenu'] && $results['createdCategories'] && $results['createdProducts'] && $results['createdModifiers'] && $results['createdTaxSchemes']);

		return $results;
	}

	public static function syncUsers($args)
	{
		$pepperApi = new PepperApi($args);
		$pepperDataExtractor = new PepperDataExtractor($args);

		foreach ( $pepperDataExtractor->getUsers() as $pepperUser )
		{
			// Checkreate User
			if ( empty($pepperUser['_id']) )
			{
				$pepperApi->createUser($pepperUser);
			}

			// Activate/deactive User
			if ( empty($pepperUser['tokens']) && empty($pepperUser['tokens']['activation']) )
			{
				$pepperApi->activateUser($pepperUser);
			}
		}

		return $results;
	}

	/**
	 * Function called upon closing of order to send Lavu Order to Pepper
	 *
	 * @param $args Array containing input arguments with required keys: locationId, registerId, userId, dataname, order_id, check
	 *
	 * @return Array - whether Pepper Order was successfully created
	 */
	public static function sendOrder($args)
	{
		$results = array(
			'orderId'            => '',
			'transactionIds'     => '',
			'transactions'       => '',
			'error'              => '',
			'success'            => false,
			'createdOrder'       => false,
			'updatedOrder'       => false,
			'createdTransaction' => false,
		);

		if (empty($args['locationId']) || empty($args['registerId']) || empty($args['userId']) || empty($args['dataname']) || empty($args['order_id']) || empty($args['check']))
		{
			$results['error'] = __METHOD__ .'() missing required field';
			error_log($results['error'] ." with args ". json_encode($args));
			return $results;
		}

		$pepperApi = new PepperApi($args);
		$dataExtractor = new PepperDataExtractor();

		$order = array();

		// Retrieve order if orderId exists, otherwise create new order
		$updateOrder = false; // update order flag make default as false
		if (empty($args['orderId']))
		{
			// Data extract order here so we can use it to include the order total when we update the Pepper Order to finalize it
			$orderData = $dataExtractor->getOrder($args);
			$args['request_json'] = json_encode($orderData);
			if ($args['extract-data']) $args['extract-data'] = false;

			// Create Pepper Order
			$order = $pepperApi->createOrder($args);

			// If order created as OPEN state then we make update order flag true
			if (isset($order['orderId']) && $order['orderId'] != '') {
				$updateOrder = true;
			}

			// Reset payload JSON, extract-data flag
			unset($args['request_json']);
			$args['extract-data'] = true;  // TO DO : see if we should save original flag value instead of setting to be true for all subsequent calls
		}
		else
		{
			$order = $pepperApi->getOrder($args);
			if (empty($order['orderId']) && !empty($order['_id'])) {
				$order['orderId'] = ($order['_id']);
			}
			// if in any case state is not Completed then make update order flag true
			if (isset($order['state']) && $order['state'] != 'COMPLETED') {
				$updateOrder = true;
			}
		}

		// If the Order exists or was successfully created, proceed with creating the Transaction(s)

		if (empty($order['orderId']))
		{
			$results['error'] = $order['message'];
			error_log("Could not create Pepper Order with args ". json_encode($args) .": ". $results['error']);
		}
		else
		{
			$args['orderId']    = $order['orderId'];
			$results['orderId'] = $order['orderId'];
			$results['createdOrder'] = true;

			$txnResults = PepperIntegration::createTransactions($args);

			// If the Transaction was created successfully, proceed with updating the Order to finalize it

			if (empty($txnResults['success']))
			{
				$results['error'] = $txnResults['error'];
				error_log("Could not create Pepper Transaction with args ". json_encode($args) .": ". $results['error']);
			}
			else
			{
				$results['createdTransaction'] = true;
				$results['transactions']   = $txnResults['transactions'];
				$results['transactionIds'] = $txnResults['transactionIds'];

				if (!empty($args['update-order']))
				{
					$totalPrice = $order['totalPrice'];
					if ($totalPrice == null || $totalPrice == '') {
						if ($orderData['totalPrice'] == null || $orderData['totalPrice'] == '') {
							$orderData = $dataExtractor->getOrder($args);
						}
						$totalPrice = $orderData['totalPrice'];
					}
					// Complete Pepper Order
					$updateArgs = array('locationId' => $args['locationId'], 'orderId' => $args['orderId'], 'state' => 'COMPLETED', 'totalPrice' => $totalPrice);  // TO DO : see if we need to remove locationId
					$args['request_json'] = json_encode($updateArgs);
					$args['extract-data'] = false;
					// If update order flag is true then we call updateOrder pepper api
					if ($updateOrder) {
						$order = $pepperApi->updateOrder($args);
					}

					if (empty($order['orderId']))
					{
						$results['error'] = $order['message'];
						error_log("Could not Create or update Pepper Order with args ". json_encode($args) .": ". $results['error']);
					}
					else
					{
						$results['updatedOrder'] = true;
					}
				}
			}
		}

		$results['success'] = ($results['createdOrder'] && ($results['updatedOrder'] || $args['update-order'] === false));  // removed  && $results['createdTransaction']

		return $results;
	}


	/**
	 * Create Pepper Transactions for Lavu payments
	 *
	 * @param $args Associative array containing input arguments
	 *
	 * @return Boolean - whether creation of payment(s) succeeded
	 */
	public static function createTransactions($args)
	{
		$results = array(
			'success'            => true,
			'error'              => '',
			'transactionIds'     => '',
			'transactions'       => array(),
		);

		if (empty($args['locationId']) || empty($args['userId']) || empty($args['orderId']) || empty($args['dataname']) || empty($args['order_id']) || empty($args['check']))
		{
			$results['success'] = false;
			$results['error'] = __METHOD__ .'() missing required field';
			error_log($results['error'] ." with args ". json_encode($args));
			return $results;
		}

		$pepperApi = new PepperApi($args);
		$dataExtractor = new PepperDataExtractor();

		// Allow transactions array to be passed-in to bypass DataExtractor so the order_addon webview can pass in the data it needs created so we can skip querying the db
		$transactionsToCreate = !empty($args['transactions']) ? $args['transactions'] : $dataExtractor->getTransactions($args);

		foreach ($transactionsToCreate as $transactionToCreate)
		{
			// If the transactions array has been passed in, the transactions probably won't have the orderId set (since it wasn't created at the time)
			if (empty($transactionToCreate['orderId']))
			{
				$transactionToCreate['orderId'] = $args['orderId'];
			}

			if (!empty($args['userId']))
			{
				$transactionToCreate['userId'] = $args['userId'];
			}

			// Create Pepper Transaction
			$args['extract-data'] = false;  // since we already did this above
			$args['request_json'] = json_encode($transactionToCreate);
			$transaction = $pepperApi->createTransaction($args);

			// API returned an error
			if (!empty($transaction['message']))
			{
				$results['success'] = false;
				$results['error'] = $transaction['message'];
			}
			else
			{
				$results['success'] = ($results['success'] === false) ? false : true;
				$results['transactionIds'] = empty($results['transactionIds']) ? $transaction['_id'] : ','. $transaction['_id'];
				$results['transactions'][] = $transaction;
			}
		}

		// Keep it from looking like an error occurred if we didn't have any transactions to create so that order can still be finalized
		if (count($transactionsToCreate) === 0)
		{
			$results['success'] = true;
		}

		return $results;
	}


	/**
	 * Refund Pepper Transactions for Lavu refunds/voids
	 *
	 * @param $args Associative array containing input arguments
	 *
	 * @return Boolean - whether refund(s) occurred
	 */
	public static function refundTransactions($args)
	{
		$results = array();

		if (empty($args['locationId']) || empty($args['userId']) || empty($args['orderId']) || empty($args['dataname']) || empty($args['order_id']) || empty($args['check']))
		{
			$results['error'] = __METHOD__ .'() missing required field';
			error_log($results['error'] ." with args ". json_encode($args));
			return $results;
		}

		$success = true;
		$pepperApi = new PepperApi($args);
		$dataExtractor = new PepperDataExtractor();
		$transactions = $dataExtractor->refundTransactions($pepper_args);
		foreach ($transactions as $transaction)
		{
			$result = array(
				'orderId'            => '',
				'transactionId'      => '',
				'transaction'        => '',
				'error'              => '',
				'success'            => false,
			);

			// Refund Pepper Transaction
			$transaction = $pepperApi->refundTransaction($transaction);

			// Refund had error
			if (empty($transaction['message']))
			{
				$result['success'] = false;
				$result['error'] = $transaction['message'];
			}
			else
			{
				$result['success'] = true;
				$result['transactionId'] = $transaction['_id'];
				$result['transaction'] = $transaction;
			}

			$results[] = $result;
		}

		return $results;
	}
}