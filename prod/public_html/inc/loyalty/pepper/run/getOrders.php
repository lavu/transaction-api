<?php

// Usage: php getOrders.php '{"locationId": "58bdd1052aebd96de8b3b32e", "order_id": "1-123"}'

// Confirm required arg (containing JSON args) has been passed-in on command-line
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <args JSON>\n";
	exit;
}

// Slurp in JSON args array passed-in via command-line
$args = json_decode($argv[1], true);
echo "DEBUG: CLI argv[1]=". print_r($argv, true);  //debug
echo "DEBUG: API args=". print_r($args, true);  //debug

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');
$pepper = new PepperApi();
$response = $pepper->getOrders($args);

echo "DEBUG: response=". print_r($response, true);  //debug
exit;
