<?php

// Usage: php createOrder.php '{"locationId": "58bdd1052aebd96de8b3b32e", "userId": "58bdd1052aebd96de8b3b32e", "dataname": "hooli", "order_id": "1-123", "check":"1", "extract-data":1}'

// Confirm required args have been passed-in on command-line
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <args JSON>\n";
	exit;
}

echo "DEBUG: argv=". print_r($argv, true);  //debug

// Slurp in JSON args array passed-in via command-line
$args = json_decode($argv[1], true);
echo "DEBUG: args=". json_encode($args) ."\n";  //debug


require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');
$pepper = new PepperApi();
$response = $pepper->createOrder($args);
echo "DEBUG: response=". print_r($response, true) ."\n";  //debug

exit;
