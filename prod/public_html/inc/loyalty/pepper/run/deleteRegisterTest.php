<?php

// Confirm required args have been passed-in on command-line
if ( empty($argv[1]) || empty($argv[2]) )
{
	echo "Usage: {$argv[0]} <locationId> <registerId>\n";
	exit;
}

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');

$args = array(
	'locationId' => $argv[1],
	'registerId' => $argv[2],
	'title'      => $argv[3],
);

$pepper = new PepperApi();
$response = $pepper->deleteRegister($args);
echo "response=". print_r($response, true);

exit;
