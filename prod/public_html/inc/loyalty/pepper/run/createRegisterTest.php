<?php

// Confirm required args have been passed-in on command-line
if ( empty($argv[1]) || empty($argv[2]) )
{
	echo "Usage: {$argv[0]} <locationId> <title>\n";
	exit;
}

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');

$args = array(
	'locationId' => $argv[1],
	'title'      => $argv[2],
);

$pepper = new PepperApi();
$response = $pepper->createRegister($args);
echo "response=". print_r($response, true);

exit;
