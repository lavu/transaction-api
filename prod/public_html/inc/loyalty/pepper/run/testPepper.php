<?php

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');

$pepper = new PepperApi();

$args = array('locationId' => '58bdd1052aebd96de8b3b32e');

// Test Get Location
$location = $pepper->getLocation($args);
echo "locations=". print_r($locations, true);  //debug

$args['locationId'] = $location['_id'];

// Test Get Registers
$registers = $pepper->getRegisters($args);
echo "BEFORE: locationId {$args['locationId']} registers=". print_r($registers, true);  //debug

// Test Create Register
$args['title'] = 'Newer Register';
$register = $pepper->createRegister($args);

// Test Update Register
$args['registerId'] = $register['_id'];
$args['title'] = 'Marked For Death';
$registers = $pepper->updateRegister($args);

$registers = $pepper->getRegisters($args);
echo "AFTER: locationId {$args['locationId']} registers=". print_r($registers, true);  //debug

// Test Delete Register
$register = $pepper->deleteRegister($args);

$registers = $pepper->getRegisters($args);
echo "AFTER: locationId {$args['locationId']} registers=". print_r($registers, true);  //debug

