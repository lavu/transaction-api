<?php

// Usage: php createTransaction.php '{"locationId": "58bdd1052aebd96de8b3b32e", "registerId": "58c078d8375a56645ace1194", "userId": "59778648b4b9cd32cd6017e6", "orderId": "598c9567ae22ae784db0182f", "dataname": "hooli", "order_id": "1003-15", "check":"1", "extract-data":1}'

// Confirm required args have been passed-in on command-line
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <args JSON>\n";
	exit;
}

echo "DEBUG: argv=". print_r($argv, true);  //debug

// Slurp in JSON args array passed-in via command-line
$args = json_decode($argv[1], true);
echo "DEBUG: args=". json_encode($args) ."\n";  //debug


require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');
$pepper = new PepperApi();
$response = $pepper->createTransaction($args);
echo "DEBUG: response=". print_r($response, true) ."\n";  //debug

exit;
