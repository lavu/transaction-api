<?php

// Usage: php testDataExtractor_GetTransaction.php '{"locationId": "58bdd1052aebd96de8b3b32e", "registerId": "58c078d8375a56645ace1194", "userId": "59778648b4b9cd32cd6017e6", "orderId": "598c9567ae22ae784db0182f", "dataname": "hooli", "order_id": "1003-15", "check":"1"}'

// Confirm required arg (containing JSON args) has been passed-in on command-line
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <args JSON>\n";
	exit;
}

// Slurp in JSON args array passed-in via command-line
$args = json_decode($argv[1], true);
echo "DEBUG: CLI argv[1]=". print_r($argv, true);  //debug
echo "DEBUG: PepperDataExtractor args=". json_encode($args) ."\n";  //debug

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperDataExtractor.php');
$dataExtractor = new PepperDataExtractor();
$data = $dataExtractor->getTransaction($args);

echo "DEBUG: order=". json_encode($data, JSON_PRETTY_PRINT) ."\n";  //debug
exit;
