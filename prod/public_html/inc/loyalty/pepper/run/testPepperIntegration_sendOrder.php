<?php

// Usage: php testPepperIntegration_sendOrder.php '{"locationId": "58bdd1052aebd96de8b3b32e", "registerId": "58c078d8375a56645ace1194", "userId": "59778648b4b9cd32cd6017e6", "orderId": "598c9567ae22ae784db0182f", "dataname": "hooli", "order_id": "1003-15", "check":"1", "data-extract":"1"}'

// Confirm required arg (containing JSON args) has been passed-in on command-line
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <args JSON>\n";
	exit;
}

echo "DEBUG: CLI argv=". print_r($argv, true);  //debug

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperIntegration.php');

// Slurp in JSON args array passed-in via command-line
$args = json_decode($argv[1], true);
echo "DEBUG: PepperIntegration::sendOrder() args=". json_encode($args) ."\n";  //debug

$result = PepperIntegration::sendOrder($args);
echo "DEBUG: result=". json_encode($result, JSON_PRETTY_PRINT) ."\n";  //debug

exit;
