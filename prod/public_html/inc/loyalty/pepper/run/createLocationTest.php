<?php

echo "DEBUG: in ". __FILE__ ."\n";  //debug

if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname>\n";
	exit;
}

echo "DEBUG: before require_once()\n";  //debug
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');
echo "DEBUG: after require_once()\n";  //debug

$pepper = new PepperApi();
echo "DEBUG: after obj instantiated\n";  //debug

$args = array(
	'dataname' => $argv[1],
);
echo "DEBUG: args=". json_encode($args) ."\n";  //debug

$response = $pepper->createLocation($args);
echo "DEBUG: response=". print_r($response, true);  //debug

exit;
