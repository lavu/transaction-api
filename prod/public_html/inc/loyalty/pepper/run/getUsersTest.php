<?php

// Confirm required args have been passed-in on command-line
$args = array();
if ( !empty($argv[1]) )
{
	$args = json_decode($argv[1], true);
}

// Slurp in JSON args array passed-in via command-line
echo "DEBUG: test script args=". print_r($args, true);  //debug

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');
$pepper = new PepperApi($args);
$response = $pepper->getUsers($args);

echo "DEBUG: response=". print_r($response, true);  //debug
exit;
