<?php

// Confirm required args have been passed-in on command-line
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <locationId>\n";
	exit;
}

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');

$debug = true;
$args = array('locationId' => $argv[1]);

$pepper = new PepperApi();
$response = $pepper->getRegisters($args);

if ($debug) echo "response=". print_r($response, true);

exit;
