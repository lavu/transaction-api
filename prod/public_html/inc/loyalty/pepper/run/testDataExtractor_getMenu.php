<?php

// Usage: php testPepperDataExtractor_getMenu.php hooli 1

// Confirm required arg have been passed-in on command-line
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname> <outputFormattedJson>\n";
	exit;
}

$outputFormattedJson = false;
if ( !empty($argv[2]) )
{
	$outputFormattedJson = $argv[2];
}

echo "DEBUG: CLI argv=". print_r($argv, true);  //debug

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperDataExtractor.php');

// Setup method args using command-line args
$dataname = $argv[1];
$args = array('dataname' => $dataname, 'loc_id' => '1');
echo "DEBUG: PepperDataExtractor->getMenu() args=". json_encode($args) ."\n";  //debug

$dataExtractor = new PepperDataExtractor();
$result = $dataExtractor->getMenu($args);
$json = ($outputFormattedJson) ? json_encode($result, JSON_PRETTY_PRINT) : json_encode($result);
echo "DEBUG: json=". $json  ."\n";  //debug

exit;
