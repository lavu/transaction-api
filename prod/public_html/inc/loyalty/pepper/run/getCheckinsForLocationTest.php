<?php

// Confirm required args have been passed-in on command-line
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <args JSON>\n";
	exit;
}

// Slurp in JSON args array passed-in via command-line
$args = json_decode($argv[1], true);
echo "DEBUG: test script args=". print_r($args, true);  //debug

require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');
$pepper = new PepperApi();
$response = $pepper->getCheckinsForLocation($args);

echo "DEBUG: response=". print_r($response, true);  //debug
exit;
