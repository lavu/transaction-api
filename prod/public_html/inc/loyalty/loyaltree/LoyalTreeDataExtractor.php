<?php

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/LoyalTreeConfig.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/bluelabel/LoyalTreeBlueLabel.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/Category.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/Item.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/Payment.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/Redeem.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/Reinstate.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/SalesItem.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/SendTransactionWithCode.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/Staff.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/Store.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/TransactionDetail.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/UpdateStores.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/whitelabel/UUID.php');

class LoyalTreeDataExtractor
{
	private $dataname;
	private $restaurant_id;
	private $loc_id;
	private $order_id;
	private $menu_id;
	private $check;

	private $restaurant;
	private $location;

	private $app;

	// White Label members

	private $order;
	private $order_contents;
	private $cc_transactions;

	private $location_config_settings;

	// Blue Label members

	private $data;
	private $locations;
	private $categories;
	private $items;
	private $payments;

	private $order_info;
	private $check_info;

	// Init methods

	private function init($args)
	{
		// Bail out if we've already initialized the Data Extractor for this dataname, order_id, check combo
		if ( $this->dataname === $args['dataname'] && $this->order_id === $args['order_id'] && $this->args['check'] === $args['check'] )
		{
			return false;
		}
		// Check required args
		else if ( empty($args['dataname']) || empty($args['loc_id']) )
		{
			throw new Exception("Missing required args: dataname|loc_id");
		}

		// Collect data for the account

		$this->dataname = $args['dataname'];
		$this->loc_id = $args['loc_id'];
		$this->order_id = $args['order_id'];
		$this->check = $args['check'];

		$this->restaurant = $this->queryDatabase('restaurant', $args);
		$this->restaurant_id = $this->restaurant['id'];
		$this->location = $this->queryDatabase('location', $args);
		$this->location_config_settings = $this->getLocationConfigSettings($args);
		$this->menu_id = $this->location['menu_id'];

		if ( (isset($args['app']) && $args['app'] === LoyalTreeConfig::WHITELABEL_APP) || (strstr($this->restaurant['modules'], LoyalTreeConfig::WHITELABEL_MODULE) !== false) )
		{
			$this->app = LoyalTreeConfig::WHITELABEL_APP;
		}
		else
		{
			$this->app = LoyalTreeConfig::BLUELABEL_APP;
		}

		return true;
	}

	private function initPurchase($args)
	{
		$this->init($args);

		// Bail out if we've already initialized the Data Extractor for this purchase
		if ( !empty($this->order) && !empty($this->order_contents) && !empty($this->cc_transactions) && !empty($this->salesitems) && $this->order_id === $args['order_id'] && $this->check === $args['check'] )
		{
			return false;
		}

		// Blank member values

		$this->order = '';
		$this->order_contents = array();
		$this->cc_transactions = array();
		$this->salesitems = array();

		// Collect data for account

		$this->order = $this->queryDatabase('order', $args);
		$this->order_contents = $this->queryDatabase('order_contents', $args);
		$this->cc_transactions = $this->queryDatabase('cc_transactions', $args);
		$this->salesitems = $this->queryDatabase('salesitems', $args);

		return true;
	}

	private function initBlueLabel($args)
	{
		$this->init($args);

		// Blank member values

		$this->data = array();
		$this->locations = array();
		$this->categories = array();
		$this->items = array();
		$this->payments = array();

		// Collect data for account

		$this->data = $this->getBlueLabelData($args);

		if ( $args['action'] == 'purchase' )
		{
			$this->items = $this->getBlueLabelItems($args);
			$this->payments = $this->getBlueLabelPayments($args);
		}
		else if ( $args['action'] == 'signup' || $args['action'] == 'update' )
		{
			$this->locations  = LoyalTreeBlueLabel::getLocations($this->restaurant_id);
			$this->categories = LoyalTreeBlueLabel::getCats($this->menu_id);
			$this->items      = LoyalTreeBlueLabel::getItems($this->menu_id);
		}


		return true;
	}

	// General-purpose Methods

	/**
	 * Method called by API to get Purchase payload
	 *
	 * @param  $args Array of args
	 *
	 * @return string of payload
	 */
	public function getPurchase($args)
	{
		if ( !$this->init($args) )
		{
			throw new Exception('Init failed for '. __METHOD__ .' with args='. json_encode($args));
		}

		$retval = array('payload' => '', 'content-type' => '', 'endpoint' => '', 'error' => '', );

		// White label payload
		if ( $this->app == LoyalTreeConfig::WHITELABEL_APP )
		{
			$sendTxnWithCode = $this->getWhiteLabelPurchase($args);
			$retval['payload'] = json_encode($sendTxnWithCode, JSON_PRETTY_PRINT);
			$retval['endpoint'] = LoyalTreeConfig::WHITELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::WHITELABEL_API_ENDPOINT_PATH_PURCHASE;
			$retval['content-type'] = 'application/json';
			$retval['app'] = $this->app;
		}
		// Blue label payload
		else
		{
			$this->initBlueLabel($args);
			$payload = LoyalTreeBlueLabel::purchase($this->data, $this->items, $this->payments);
			$retval['payload'] = $this->formatXml($payload);  //debug
			$retval['endpoint'] = LoyalTreeConfig::BLUELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::BLUELABEL_API_ENDPOINT_PATH_PURCHASE;
			$retval['app'] = $this->app;
		}

		return $retval;
	}

	/**
	 * Method called by API to get Signup payload
	 *
	 * @param  $args Array of args
	 *
	 * @return string of payload
	 */
	public function getSignup($args)
	{
		if ( !$this->init($args) )
		{
			throw new Exception('Init failed for '. __METHOD__ .' with args='. json_encode($args));
		}

		$retval = array('payload' => '', 'content-type' => '', 'endpoint' => '', 'error' => '', );

		// White label payload
		if ( $this->app == LoyalTreeConfig::WHITELABEL_APP )
		{
			$updateStores = $this->getWhiteLabelSignup($args);
			$retval['payload'] = json_encode($updateStores, JSON_PRETTY_PRINT);
			$retval['endpoint'] = LoyalTreeConfig::WHITELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::WHITELABEL_API_ENDPOINT_PATH_SIGNUP;
			$retval['content-type'] = 'application/json';
			$retval['app'] = $this->app;
		}
		// Blue label payload
		else
		{
			$this->initBlueLabel($args);
			$payload = LoyalTreeBlueLabel::signup($this->data['businessid'], $this->data['chainid'], $this->restaurant['company_name'], $this->locations, $this->categories, $this->items);
			$retval['payload'] = $this->formatXml($payload);  //debug
			$retval['endpoint'] = LoyalTreeConfig::BLUELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::BLUELABEL_API_ENDPOINT_PATH_SIGNUP;
			$retval['app'] = $this->app;
		}

		return $retval;
	}

	/**
	 * Method called by API to get Redeem payload
	 *
	 * @param  $args Array of action args
	 *
	 * @return string of payload
	 */
	public function getRedeem($args)
	{
		if ( !$this->init($args) )
		{
			throw new Exception('Init failed for '. __METHOD__ .' with args='. json_encode($args));
		}

		$retval = array('payload' => '', 'content-type' => '', 'endpoint' => '', 'error' => '', );

		// White label payload
		if ( $this->app == LoyalTreeConfig::WHITELABEL_APP )
		{
			$redeem = $this->getWhiteLabelRedeem($args);
			$retval['payload'] = json_encode($redeem, JSON_PRETTY_PRINT);
			$retval['endpoint'] = LoyalTreeConfig::WHITELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::WHITELABEL_API_ENDPOINT_PATH_REDEEM;
			$retval['content-type'] = 'application/json';
			$retval['app'] = $this->app;
		}
		// Blue label payload
		else
		{
			$this->initBlueLabel($args);
			$payload = LoyalTreeBlueLabel::redeem($this->data);
			$retval['payload'] = $this->formatXml($payload);  //debug
			$retval['endpoint'] = LoyalTreeConfig::BLUELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::BLUELABEL_API_ENDPOINT_PATH_REDEEM;
			$retval['app'] = $this->app;
		}

		return $retval;
	}

	/**
	 * Method called by API to get Reinstate payload
	 *
	 * @param  $args Array of action args
	 *
	 * @return string of payload
	 */
	public function getReinstate($args)
	{
		if ( !$this->init($args) )
		{
			throw new Exception('Init failed for '. __METHOD__ .' with args='. json_encode($args));
		}

		$retval = array('payload' => '', 'content-type' => '', 'endpoint' => '', 'error' => '', );

		// White label payload
		if ( $this->app == LoyalTreeConfig::WHITELABEL_APP )
		{
			$reinstate = $this->getWhiteLabelReinstate($args);
			$retval['payload'] = json_encode($reinstate, JSON_PRETTY_PRINT);
			$retval['endpoint'] = LoyalTreeConfig::WHITELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::WHITELABEL_API_ENDPOINT_PATH_REINSTATE;
			$retval['content-type'] = 'application/json';
			$retval['app'] = $this->app;
		}
		// Blue label payload
		else
		{
			$this->initBlueLabel($args);
			$payload = LoyalTreeBlueLabel::reinstate($this->data);
			$retval['payload'] = $this->formatXml($payload);  //debug
			$retval['endpoint'] = LoyalTreeConfig::BLUELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::BLUELABEL_API_ENDPOINT_PATH_REINSTATE;
			$retval['app'] = $this->app;
		}

		return $retval;
	}

	/**
	 * Method called by API to update menu
	 *
	 * @param  $args Array of args
	 *
	 * @return string of payload
	 */
	public function getUpdate($args)
	{
		if ( !$this->init($args) )
		{
			throw new Exception('Init failed for '. __METHOD__ .' with args='. json_encode($args));
		}

		$retval = array('payload' => '', 'content-type' => '', 'endpoint' => '', 'error' => '', );

		// White label payload
		if ( $this->app == LoyalTreeConfig::WHITELABEL_APP )
		{
			$updateStores = $this->getWhiteLabelSignup($args);
			$retval['payload'] = json_encode($updateStores, JSON_PRETTY_PRINT);
			$retval['endpoint'] = LoyalTreeConfig::WHITELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::WHITELABEL_API_ENDPOINT_PATH_SIGNUP;
			$retval['content-type'] = 'application/json';
			$retval['app'] = $this->app;
		}
		// Blue label payload
		else
		{
			$this->initBlueLabel($args);
			$payload = LoyalTreeBlueLabel::preload($this->data, $this->categories, $this->items);
			$retval['payload'] = $this->formatXml($payload);  //debug
			$retval['endpoint'] = LoyalTreeConfig::BLUELABEL_API_ENDPOINT_BASE . LoyalTreeConfig::BLUELABEL_API_ENDPOINT_PATH_SIGNUP;
			$retval['app'] = $this->app;
		}

		return $retval;
	}

	// White Label Methods

	/**
	 * Gets instance of white label purchase object
	 *
	 * @param  $args Array of action args
	 *
	 * @return SendTransactionWithCode object
	 */
	public function getWhiteLabelPurchase($args)
	{
		$this->initPurchase($args);

		// Get order immediately so we can use its loc_id for getting the LocationConfigSettings
		$obj = new SendTransactionWithCode();
		$obj->APIKey = LoyalTreeConfig::WHITELABEL_API_KEY;
		$obj->Username = $this->location_config_settings['loyaltree_username'];
		$obj->Password = $this->location_config_settings['loyaltree_password'];
		$obj->StoreID  = $this->location_config_settings['loyaltree_id'];
		$obj->ServerID = $this->order['server_id'];
		if (!empty($this->order['tablename'])) $obj->TableID = $this->order['tablename'];
		if (!empty($this->order['guests'])) $obj->GuestCount = (int) $this->order['guests'];
		$obj->TransactionID = $this->order['order_id'] .'-'. $args['check'];  // TransactionID = order_id-check
		unset($obj->ParentTransactionID);  // TO DO : see if relevant ("The check identifier that this check was split from.")
		//unset($obj->{$ParentTransactionID});  // php 7.0
		$obj->TotalAmount = number_format($this->order['total'], 2);
		$obj->CheckDiscount = number_format($this->order['discount'], 2);  // TO DO : see if relevant ("The check level discount applied to the transaction.")
		$obj->CheckOpenTime = date('c', strtotime($this->order['opened']));
		$obj->TransactionTime = date('c', strtotime($args['createdtime']));  // This should be the event time
		$obj->GuestReceiptCode = null;  //was $this->getUUID() but we're just going to send Payment.PaymentReceiptCode  //UUID::v5($this->location_config_settings['loyaltree_username'], $obj->TransactionID);
		$obj->Items = $this->getSalesItems($args);
		$obj->Payments = $this->getPayments($args);

		return $obj;
	}

	/**
	 * Gets instance of white label signup object
	 *
	 * @param  $args Array of action args
	 *
	 * @return UpdateStores object
	 */
	public function getWhiteLabelSignup($args)
	{
		$this->init($args);

		// Get order immediately so we can use its loc_id for getting the LocationConfigSettings
		$obj = new UpdateStores();
		$obj->APIKey = LoyalTreeConfig::WHITELABEL_API_KEY;
		$obj->Username = $this->location_config_settings['loyaltree_username'];
		$obj->Password = $this->location_config_settings['loyaltree_password'];
		$obj->Stores = $this->getStores($args);

		return $obj;
	}

	/**
	 * Gets instance of white label redeem object
	 *
	 * @param  $args Array of action args
	 *
	 * @return Redeem object
	 */
	public function getWhiteLabelRedeem($args)
	{
		$this->init($args);

		$this->order = $this->queryDatabase('order', $args);

		// Get order immediately so we can use its loc_id for getting the LocationConfigSettings
		$obj = new Redeem();
		$obj->APIKey = LoyalTreeConfig::WHITELABEL_API_KEY;
		$obj->Username = $this->location_config_settings['loyaltree_username'];
		$obj->Password = $this->location_config_settings['loyaltree_password'];
		$obj->StoreID  = $this->location_config_settings['loyaltree_id'];
		$obj->ServerID = $this->order['server_id'];
		$obj->RewardCode = $args['code'];
		$obj->TransactionDetails = $this->getTransactionDetails($args);

		return $obj;
	}

	/**
	 * Gets instance of white label purchase object
	 *
	 * @param  $args Array of action args
	 *
	 * @return Reinstate object
	 */
	public function getWhiteLabelReinstate($args)
	{
		$this->init($args);

		$this->order = $this->queryDatabase('order', $args);

		// From JSON stored in split_check_details.discount_info
		//$discount_info = $args['discount_info'];

		// Get order immediately so we can use its loc_id for getting the LocationConfigSettings
		$obj = new Reinstate();
		$obj->APIKey = LoyalTreeConfig::WHITELABEL_API_KEY;
		$obj->Username = $this->location_config_settings['loyaltree_username'];
		$obj->Password = $this->location_config_settings['loyaltree_password'];
		$obj->StoreID  = $this->location_config_settings['loyaltree_id'];
		$obj->RedemptionID = $args['rid'];  // $discount_info['RedemptionID'];
		$obj->IsDeal = $args['isdeal'];     // $discount_info['IsDeal'];
		$obj->TransactionID = $this->order['order_id'] .'-'. $args['check'];
		$obj->Reason = $args['reason'];

		return $obj;
	}

	/**
	 * Gets array of white label SalesItems
	 *
	 * @param  $args Array of action args
	 *
	 * @return SalesItems array
	 */
	public function getSalesItems($args)
	{
		$this->initPurchase($args);

		$salesItems = array();

		foreach ( $this->salesitems as $salesitem )
		{
			$obj = new SalesItem();
			$obj->ItemID = $salesitem['item_id'];
			if (!empty($salesitem['category_id'])) $obj->CategoryID = $salesitem['category_id'];
			$obj->Quantity = number_format($salesitem['quantity'], 1);
			$obj->LineTotal = number_format($salesitem['price'], 2);
			$obj->TaxIncludedInLineTotal = empty($salesitem['itax']) ? number_format(0.0, 2) : number_format($salesitem['itax'], 2);
			$obj->ModifierAmount = number_format($salesitem['modifiers'], 2);
			$obj->DiscountAmount = number_format($salesitem['discount'], 2);
			$obj->OrderTime = date('c', strtotime($salesitem['time']));

			$salesItems[] = $obj;
		}

		return $salesItems;
	}

	/**
	 * Gets array of white label SalesItems
	 *
	 * @param  $args Array of action args
	 *
	 * @return SalesItems array
	 */
	public function getPayments($args)
	{
		$this->initPurchase($args);

		$payments = array();

		foreach ( $this->cc_transactions as $cc_transaction )
		{
			$obj = new Payment();
			$obj->PaymentID = $cc_transaction['payment_id'];
			$obj->Amount = number_format($cc_transaction['amount'], 2);
			$obj->VoidPayment = (bool) $cc_transaction['void'];
			$obj->Type = (int) $this->remapPaymentType($cc_transaction['pay_type']);
			$obj->PaymentTime = date('c', strtotime($cc_transaction['paymenttime']));

			// Only keep PaymentReceiptCode member for SendTransactionWithReceiptCode
			if ( $args['action'] == 'purchase' )
			{
				// Get already-created PaymentReceiptCode from split_check_details stored in the JSON in loyaltree_info
				$split_check_details = $this->queryDatabase('split_check_details', $args);
				$loyaltree_info = json_decode($split_check_details['loyaltree_info'], true);
				$obj->PaymentReceiptCode = $loyaltree_info['qrcode'];
			}
			else
			{
				unset($obj->PaymentReceiptCode);
				//unset($obj->{$PaymentReceiptCode}); // php 7.0
			}

			if ( $args['action'] == 'purchase' || $args['action'] == 'redeem' )
			{
				$obj->IsCurrent = ($cc_transaction['check'] == $this->check);
			}
			else
			{
				unset($obj->IsCurrent);
				//unset($obj->{$IsCurrent}); // php 7.0
			}

			// Only keep UserID, UserIDType if email provided
			if ( !empty($args['email']) )
			{
				$obj->UserID = $args['email'];  // TO DO : add support for other User ID types
				$obj->UserIDType = 1;
			}
			else
			{
				unset($obj->UserID);
				unset($obj->UserIDType);
				//unset($obj->{$UserID});     // php 7.0
				//unset($obj->{$UserIDType}); // php 7.0
			}

			$payments[] = $obj;
		}

		return $payments;
	}

	/**
	 * Gets array of white label Stores
	 *
	 * @param  $args Array of args for action
	 *
	 * @return Stores array
	 */
	public function getStores($args)
	{
		$this->init($args);  // needs $this->restaurant, $this->dataname, and $this->menu_id

		// If the restaurant is part of a chain, get all of the other restaurants in the chain
		$stores_in_chain = array();
		if ( !empty($this->restaurant['chain_id']) )
		{
			$args['chain_id'] = $this->restaurant['chain_id'];
			$args['menu_id'] = $this->menu_id;  // so each $store row can be pased to getCategories() with menu_id

			// Get the store_id aka loyaltree_id of each chained sister restaurant
			$chained_restaurants = $this->queryDatabase('restaurants_by_chain_id', $args);
			while ( $chained_rest = mysqli_fetch_assoc($chained_restaurants) )
			{
				// Get the chained sister location so we can get its loc_id
				$chained_loc = $this->queryDatabase('location_highest_nondeleted', $chained_rest);
				$chained_rest['loc_id'] = $chained_loc['id'];

				// Now that we have the dataname and the loc_id in chained_rest, we can get the chained sister location's config settings to get its loyaltree_id
				$chained_loc_config_settings = $this->getLocationConfigSettings($chained_rest);
				$chained_rest['store_id'] = (isset($chained_loc_config_settings['loyaltree_id']) && !empty($chained_loc_config_settings['loyaltree_id'])) ? $chained_loc_config_settings['loyaltree_id'] : '';

				$stores_in_chain[] = $chained_rest;
			}
		}

		$stores = array();

		foreach ( $stores_in_chain as $store )
		{
			$obj = new Store();
			$obj->StoreID = $store['store_id'];
			if ( !empty($store['pos_store_id']) )
			{
				$obj->POSStoreID = $store['pos_store_id'];  // TO DO : I think this should be some kind of LavuToGo ID ("The StoreID assigned by the point of sale system that cooresponds to online ordering.")
			}
			else
			{
				unset($obj->POSStoreID);
				//unset($obj->{$POSStoreID});  // php 7.0
			}
			$obj->Staff = $this->getStaff($store);  // $store passed in instead of $args - since we really only need the dataname and store_id
			$obj->Catalog = $this->getCategories($store);  // $store passed in instead of $args - since we only need the dataname and menu_id

			$stores[] = $obj;
		}

		return $stores;
	}

	/**
	 * Gets array of white label Staff
	 *
	 * @param  $args Array of args for action
	 *
	 * @return Staff array
	 */
	public function getStaff($args)
	{
		$staff = array();

		$users = $this->queryDatabase('users', $args);

		foreach ( $users as $user )
		{
			$obj = new Staff();
			$obj->StaffID = $user['id'];
			$obj->FirstName = $user['f_name'];
			$obj->LastName = $user['l_name'];

			$staff[] = $obj;
		}

		return $staff;
	}

	/**
	 * Gets array of white label Categories; used when building UpdateStores object
	 *
	 * @param $args Array of input arguments with required keys: dataname, menu_id
	 *
	 * @return Category array
	 */
	public function getCategories($args)
	{
		$catalog = array();

		// The query places dataname in each row so we only need to pass menu_category into getItems()
		$menu_categories = $this->queryDatabase('menu_categories', $args);

		while ( $menu_category = mysqli_fetch_assoc($menu_categories) )
		{
			$obj = new Category();
			$obj->CategoryID = $menu_category['id'];
			$obj->Name = $menu_category['name'];
			$obj->Items = $this->getItems($menu_category);

			if ( empty($menu_category['description']) )
			{
				unset($obj->Description);
				//unset($obj->{$Description});  // php 7.0
			}
			else
			{
				$obj->Description = $menu_category['description'];
			}

			$catalog[] = $obj;
		}

		return $catalog;
	}

	/**
	 * Gets white label menu items; used when building UpdateStores object
	 *
	 * @param $args Array of input arguments with required keys: dataname, menu_id, category_id
	 *
	 * @return Array of white label Category objects
	 */
	public function getItems($args)
	{
		$items = array();

		$menu_items = $this->queryDatabase('menu_items', $args);

		while ( $menu_item = mysqli_fetch_assoc($menu_items) )
		{
			$obj = new Item();
			$obj->ItemID = $menu_item['item_id'];
			$obj->Name = $menu_item['name'];
			if ( empty($menu_item['description']) )
			{
				unset($obj->Description);
				//unset($obj->Description);  // php 7.0
			}
			else
			{
				$obj->Description = $menu_item['description'];
			}

			if ( empty($menu_item['UPC']) )
			{
				unset($obj->SKU);
				//unset($obj->SKU);  // php 7.0
			}
			else
			{
				$obj->SKU = $menu_item['UPC'];
			}

			$items[] = $obj;
		}

		return $items;
	}

	/**
	 * Gets single-element array containing white label TransactionDetail array used when building Redeem object
	 *
	 * @param $args Array of args for action
	 *
	 * @return TransactionDetails single-element array
	 */
	public function getTransactionDetails($args)
	{
		$this->initPurchase($args);

		$obj = new TransactionDetail();
		$obj->TransactionID = $this->order['order_id'] .'-'. $args['check'];
		if (!empty($this->order['tablename'])) $obj->TableID = $this->order['tablename'];
		if (!empty($this->order['guests'])) $obj->GuestCount = (int) $this->order['guests'];
		$obj->TotalAmount = number_format($this->order['total'], 2);
		$obj->Items = $this->getSalesItems($args);
		$obj->Payments = $this->getPayments($args);

		return array($obj);
	}

	// Blue Label Methods

	public function getBlueLabelData($args)
	{
		$data = array(
			'businessid'    => (LoyalTreeBlueLabel::$multiplier * $this->restaurant_id),
			'chainid'       => (LoyalTreeBlueLabel::$multiplier * $this->restaurant['chain_id']),
			'storeid'       => $this->restaurant_id . $this->loc_id,
			'company_name'  => $this->restaurant['company_name'],
			'time_zone'     => $this->location['timezone'],
		);

		if ( $args['action'] != 'signup' && $args['action'] != 'update' )
		{
			$this->order_info = $this->queryDatabase('bluelabel_order_info', $args);

			// Use the fields from the order_summary request var if all of the needed fields were passed-in
			if ( empty($this->order_info) && !empty($args['order_id']) && !empty($args['serverid']) && !empty($args['table']) && !empty($args['guest']) && !empty($args['totalamount']) )
			{
				$this->order_info['order_id'] = $args['order_id'];
				$this->order_info['cashier_id'] = $args['serverid'];
				$this->order_info['tablename'] = $args['table'];
				$this->order_info['check_total'] = $args['totalamount'];

				// These fields are wrong; we're sending the guest number in place of the total number of guests and the total number of checks - but we don't have access to that data
				$this->order_info['guests'] = $args['guest'];
				$this->order_info['num_checks'] = $args['guest'];
			}

			$data['transactionid'] = $this->order_id .'-'. $this->check;
			$data['serverid']      = !empty($args['auth_by_id']) ? $args['auth_by_id'] : $this->order_info['cashier_id'];  // TO DO : required arg 'auth_by_id'
			$data['table']         = $this->order_info['tablename'];
			$data['guest']         = $this->order_info['guests'];
			$data['totalamount']   = $this->order_info['check_total'];
		}

		if ( $args['action'] == 'redeem' )
		{
			$data['redeemcode'] = empty($args['code'])     ? '0' : $args['code'];
			$data['serverid']   = empty($args['serverid']) ? '0' : $args['serverid'];
		}
		else if ( $args['action'] == 'reinstate' )
		{
			$data['reason']  = empty($args['reason']) ? 'Order voided' : $args['reason'];  // TO DO : required arg 'reason'
			$data['no_echo'] = '1';

			$get_check_info = $this->queryDatabase('bluelabel_check_info', $args);
			while ( $check_info = mysqli_fetch_assoc($get_check_info) )
			{
				if ( substr($check_info['discount_info'], 0, 15) == "loyaltree|check" )
				{
					$loyaltree_info = explode('|', $check_info['discount_info']);
					$data['redemptionid'] = $loyaltree_info[2];
					$data['isdeal']       = $loyaltree_info[3];
				}
			}

			$get_item_info = $this->queryDatabase('bluelabel_item_info', $args);
			while ( $item_info = mysqli_fetch_assoc($get_item_info) )
			{
				if ( substr($item_info['idiscount_info'], 0, 17) == 'loyaltree|lt_item' )
				{
					$loyaltree_info        = explode('|', $item_info['idiscount_info']);
					$data['redemptionid']  = $loyaltree_info[2];
					$data['isdeal']        = $loyaltree_info[3];
				}
			}
		}

		return $data;
	}

	public function getBlueLabelPayments($args)
	{
		$this->init($args);

		// Code taken from /lib/jcvs/jc_functions.php
		$get_payments = $this->queryDatabase('bluelabel_payments', $args);

		$payments = array();
		while ( $payments_info = mysqli_fetch_assoc($get_payments) )
		{
			$payments_info['current'] = '1';
			$payments[] = $payments_info;
		}

		return $payments;
	}

	public function getBlueLabelItems($args)
	{
		$this->init($args);

		if ( $this->order_info['order_id'] !== $args['order_id'] )
		{
			$this->order_info = $this->queryDatabase('bluelabel_order_info', $args);
		}

		// Code taken from /lib/jcvs/jc_functions.php + /lib/jcvs/jc_inc7.php

		$decimal_places = empty($this->location['disable_decimal']) ? 2 : $this->location['disable_decimal'];
		$smallest_money = (1 / pow(10, $decimal_places));

		$items = array();
		$item_total = 0;
		$get_contents = $this->queryDatabase('bluelabel_get_contents', $args);

		while ( $contents_info = mysqli_fetch_assoc($get_contents) )
		{
			$chk = $contents_info['item_check'];
			$split_factor = 1;

			if ( $chk == 0 )
			{
				$split_factor = $this->order_info['num_checks'];
			}
			else if ( strstr($chk, "|") )
			{
				$chks = explode("||", trim($chk, "|"));
				$split_factor = count($chks);
			}

			if ( $split_factor > 1 )
			{
				$contents_info['quantity']  /= $split_factor;
				$contents_info['price']     /= $split_factor;
				$contents_info['modifiers'] /= $split_factor;
				$contents_info['discount']  /= $split_factor;
				$contents_info['itax']      /= $split_factor;
				$contents_info['tax']       /= $split_factor;
			}

			$items[] = $contents_info;
			$item_total += ($contents_info['price'] + $contents_info['modifiers'] - $contents_info['discount'] + $contents_info['tax']);
		}

		if (abs($this->order_info['check_total'] - $item_total) > ($smallest_money * count($items) * 2)) {
			$this->order_info['check_total'] = number_format($item_total, $decimal_places, ".", "");
		}

		return $items;
	}

	// Utility Methods

	/**
	 * Utility method -- Remap yyyy-mm-dd hh:mm:ss date time stamp to JSON date time format (ISO-8601: yyyy-MM-ddTHH:mm:ss.fffZ)
	 */
	private function remapDatetime($datetime)
	{
		return date('c', strtotime($datetime));
	}

	/**
	 * Utility method -- Remap Lavu pay type label to LoyalTree pay type ID
	 * Legal Values: 1 - Cash, 2 - Credit Card, 3 - Gift Card, 4 - Bank Note, 5 - Gift Certificate, 6 - Other
	 */
	private function remapPaymentType($pay_type)
	{
		$mapped_id = '';

		switch ( strtolower($pay_type) )
		{
			// Cash
			case 'cash':
				$mapped_id = 1;
				break;

			// Credit Card
			case 'card':
			case 'credit card':
			case 'creditcard':
				$mapped_id = 2;
				break;

			// Gift Card
			case 'gift card':
			case 'lavu gift':
				$mapped_id = 3;
				break;

			// Bank Note
			case 'bank note':
			case 'check':
			case 'cheque':
				$mapped_id = 4;
				break;

			// Gift Certificate
			case 'gift certificate':
			case 'gift cert':
			case 'gift cert.':
			case 'gift':
				$mapped_id = 5;
				break;

			// Other
			default:
				$mapped_id = 6;
				break;
		}

		return $mapped_id;
	}

	/**
	 * Utility method -- Creates UUID v4 used for GuestReceiptCode
	 */
	public function getUUID()
	{
		// Taken from php.net comments on com_create_guid()
		if (function_exists('com_create_guid') === true)
		{
			return trim(com_create_guid(), '{}');
		}

		// Generate GUID v4 since com_create_guid() isn't available
		$data = openssl_random_pseudo_bytes(16);
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}

	/**
	 * Utility method -- Gets array of white label location_config_settings from POS config table
	 */
	public function getLocationConfigSettings($args)
	{
		$location_config_settings = array();

		$lcsr = $this->queryDatabase('location_config_settings', $args);

		if (mysqli_num_rows($lcsr) === 0) throw new Exception('No LoyalTree location_config_settings found');

		while ( $lcs = mysqli_fetch_assoc($lcsr) )
		{
			$setting = $lcs['setting'];
			$value = $lcs['value'];

			switch ( $setting )
			{
				case 'loyaltree_password':
					// TO DO : decrypt password
					break;
			}

			$location_config_settings[$setting] = $value;
		}

		return $location_config_settings;
	}

	public function formatXml($xml)
	{
		$dom = new DOMDocument;
		$dom->preserveWhiteSpace = false;
		$dom->loadXML($xml);
		$dom->formatOutput = true;

		return $dom->saveXml();
	}

	/**
	 * Handles all database queries and connections
	 *
	 * @param  $query_name String identifying query to run
	 * @param  $args Array of input arguments, if needed for query
	 *
	 * @return mixed Returns mysql resource when $return_single_row is false, array when $return_single_row is true, bool for inserts, updates, failures
	 */
	private function queryDatabase($query_name, $args = array())
	{
		$return_single_row = true;
		$return_all_rows_array = false;
		$die_on_zero_rows  = true;
		$die_on_db_errors  = true;
		$update_or_insert  = false;

		switch ( $query_name )
		{
			case 'location':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`locations` WHERE `id` = '[loc_id]'";
				break;

			case 'location_highest_nondeleted':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`locations` WHERE `_disabled` = 0 ORDER BY `id` DESC";
				$return_single_row = true;
				$die_on_zero_rows  = false;
				break;

			case 'location_config_settings':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `location` = '[loc_id]' AND `type` = 'location_config_setting' AND `setting` LIKE '%loyaltree%' AND `_deleted`=0";
				$return_single_row = false;
				$die_on_zero_rows  = false;
				break;

			case 'location_config_settings_loyaltree_id_only':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `location` = '[loc_id]' AND `type` = 'location_config_setting' AND `setting` = 'loyaltree_id' AND `_deleted`=0";
				$return_single_row = true;
				$die_on_zero_rows  = false;
				break;

			case 'bluelabel_location_config_settings':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`config` WHERE `location` = '[loc_id]' AND `type` = 'location_config_setting' AND `setting` LIKE '%loyaltree%' AND `_deleted`=0";
				$return_single_row = false;
				break;

			case 'restaurant':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[dataname]' ORDER BY `id`";
				$return_single_row = true;
				break;

			case 'restaurants_by_chain_id':
				$sql = "SELECT `id` AS `store_id`, `data_name` AS `dataname`, '[menu_id]' AS `menu_id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `chain_id` = '[chain_id]' AND `disabled` = 0 ORDER BY `id`";
				$return_single_row = false;
				$die_on_zero_rows  = false;
				break;

			case 'order':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`orders` WHERE `order_id` = '[order_id]'";
				$die_on_zero_rows  = false;
				break;

			case 'order_contents':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`order_contents` WHERE `order_id` = '[order_id]'";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'cc_transactions':
				$sql = "SELECT `id`, `ioid`, `amount`, `check`, `voided`, `pay_type`, `datetime`, `first_four`, `card_desc`, `info`, `server_id`, `pay_type_id`, `ioid` AS `payment_id`, `total_collected` AS `amount`, `voided` AS `void`, `pay_type` AS `type`, `datetime` AS `paymenttime`, `first_four`, `card_desc`, `info`, `server_id`, `pay_type_id` FROM `poslavu_[dataname]_db`.`cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `action` = 'Sale'";
				$die_on_zero_rows  = false;
				$return_all_rows_array = true;
				break;

			case 'salesitems':
				$sql = "SELECT `order_contents`.`item_id` AS `item_id`, `menu_items`.`category_id` AS `category_id`,`order_contents`.`check` AS `item_check`, `order_contents`.`quantity` AS `quantity`, `order_contents`.`subtotal` AS `price`, ((`order_contents`.`forced_modifiers_price` + `order_contents`.`modify_price`) * `order_contents`.`quantity`) AS `modifiers`, (`order_contents`.`discount_amount` + `order_contents`.`idiscount_amount`) AS `discount`, `order_contents`.`itax` AS `itax`, `order_contents`.`tax_amount` AS `tax`, `order_contents`.`device_time` AS `time` FROM `poslavu_[dataname]_db`.`order_contents` LEFT JOIN `poslavu_[dataname]_db`.`menu_items` ON `menu_items`.`id` = `order_contents`.`item_id` WHERE `order_contents`.`item` != 'SENDPOINT' AND `order_contents`.`quantity` > 0 AND `order_contents`.`loc_id` = '[loc_id]' AND `order_contents`.`order_id` = '[order_id]' AND (`order_contents`.`check` = '[check]' OR `order_contents`.`check` LIKE '%|[check]|%' OR `order_contents`.`check` = '0')";
				$return_all_rows_array = true;
				$die_on_zero_rows  = false;
				break;

			case 'users':
				$sql = "SELECT `id`, `f_name`, `l_name` FROM `poslavu_[dataname]_db`.`users`";
				$return_all_rows_array = true;
				$return_single_row = false;
				$die_on_zero_rows  = false;
				break;

			case 'menu_categories':
				$sql = "SELECT `id`, `name`, `menu_id`, `id` AS `category_id`, '[dataname]' AS `dataname`  FROM `poslavu_[dataname]_db`.`menu_categories` WHERE `menu_id` = '[menu_id]' AND `_deleted` = 0 ORDER BY `id`, `_order` DESC";
				$die_on_zero_rows  = false;
				$return_single_row = false;
				break;

			case 'menu_items':
				$sql = "SELECT `id`, `name`, `upc`, `description`, `id` AS `item_id` FROM `poslavu_[dataname]_db`.`menu_items` WHERE `category_id` = '[category_id]' AND `menu_id` = '[menu_id]' AND `_deleted` = 0";
				$die_on_zero_rows  = false;
				$return_single_row = false;
				break;

			case 'split_check_details':
			case 'split_check_info':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'";
				break;

			case 'bluelabel_check_info':
				$sql = "SELECT `discount_info`, `order_id`, `check` FROM `poslavu_[dataname]_db`.`split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]'";
				$return_single_row = false;
				break;

			case 'bluelabel_item_info':
				$sql = "SELECT `idiscount_info`, `check` FROM `poslavu_[dataname]_db`.`order_contents` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]'";
				$return_single_row = false;
				break;

			case 'bluelabel_order_info':
				$sql = "SELECT `orders`.`no_of_checks` AS `num_checks`, `orders`.`cashier_id` AS `cashier_id`, `orders`.`tablename` AS `tablename`, `orders`.`guests` AS `guests`, `split_check_details`.`check_total` AS `check_total`, `split_check_details`.`loyaltree_info` AS `loyaltree_info`, `split_check_details`.`id` AS `scd_id`,`orders`.`total` as `loyaltree_total` FROM `poslavu_[dataname]_db`.`orders` LEFT JOIN `poslavu_[dataname]_db`.`split_check_details` ON (`split_check_details`.`loc_id` = `orders`.`location_id` AND `split_check_details`.`order_id` = `orders`.`order_id` AND `split_check_details`.`check` = '[check]') WHERE `orders`.`location_id` = '[loc_id]' AND `orders`.`order_id` = '[order_id]'";
				$return_single_row = true;
				$die_on_zero_rows  = false;  // To fix real-world cases where the order sync hadn't occurred yet
				break;

			case 'bluelabel_get_contents':
				$sql = "SELECT `order_contents`.`item_id` AS `id`, `menu_items`.`category_id` AS `category_id`,`order_contents`.`check` AS `item_check`, `order_contents`.`quantity` AS `quantity`, `order_contents`.`subtotal` AS `price`, ((`order_contents`.`forced_modifiers_price` + `order_contents`.`modify_price`) * `order_contents`.`quantity`) AS `modifiers`, (`order_contents`.`discount_amount` + `order_contents`.`idiscount_amount`) AS `discount`, `order_contents`.`itax` AS `itax`, `order_contents`.`tax_amount` AS `tax`, `order_contents`.`device_time` AS `time` FROM `poslavu_[dataname]_db`.`order_contents` LEFT JOIN `poslavu_[dataname]_db`.`menu_items` ON `menu_items`.`id` = `order_contents`.`item_id` WHERE `order_contents`.`item` != 'SENDPOINT' AND `order_contents`.`quantity` > 0 AND `order_contents`.`loc_id` = '[loc_id]' AND `order_contents`.`order_id` = '[order_id]' AND (`order_contents`.`check` = '[check]' OR `order_contents`.`check` LIKE '%|[check]|%' OR `order_contents`.`check` = '0')";
				$return_single_row = false;
				$die_on_zero_rows  = false;
				break;

			case 'bluelabel_payments':
				$sql = "SELECT `ioid` AS `paymentid`, `total_collected` AS `amount`, `voided` AS `void`, `pay_type` AS `type`, `datetime` AS `paymenttime`,`first_four`,`card_desc`,`info` FROM `poslavu_[dataname]_db`.`cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `action` = 'Sale'";
				$return_single_row = false;
				$die_on_zero_rows  = false;
				break;

			default:
				die(__METHOD__ ." got unrecognized query {$query_name} for dataname {$this->dataname}\n");
				break;
		}

		$result = mlavu_query($sql, $args);

		if ( $result === false && $die_on_db_errors )
		{
			throw new Exception("Database error - {$query_name} query got: ". mysqli_error() ."\n");
		}
		else if ( $result !== false && $return_all_rows_array )
		{
			$all_rows = array();
			while ( $row = mysqli_fetch_assoc($result) )
			{
				$all_rows[] = $row;
			}
			return $all_rows;
		}
		else if ( $update_or_insert )
		{
			return $result;
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			error_log("No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
			throw new Exception("Database error - No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}
}