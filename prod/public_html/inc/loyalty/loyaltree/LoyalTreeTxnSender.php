<?php

require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/LoyalTreeIntegration.php');

class LoyalTreeTxnSender
{
	/**
	 * Get all LoyalTree white label datanames to send any unsent txns for each one
	 *
	 * @param  $args Array of input arguments, not really used in this method, so just passed through
	 *
	 * @return Three dimensional array of results (dataname -> txn_id -> response JSON decoded into an array)
	 */
	public function sendAllTxns($args)
	{
		$retval = array();

		$restaurants = $this->queryDatabase('whitelabel_restaurants');
		while ( $restaurant = mysqli_fetch_assoc($restaurants) )
		{
			$dataname = $restaurant['data_name'];
			$args['dataname'] = $dataname;

			$results[$dataname] = $this->sendTxnsForDataname($args);
		}

		return $retval;
	}

	/**
	 * Send all unsent LoyalTree white label txns for a particular dataname
	 *
	 * @param  $args Array of input arguments with required keys: dataname
	 *
	 * @return Two dimensional array of results (txn_id -> response JSON decoded into an array)
	 */
	public function sendTxnsForDataname($args)
	{
		$results = array('success' => false);

		// Check for required args
		if ( empty($args['dataname']) )
		{
			return $results;
		}

		$split_check_details = $this->queryDatabase('unsent_loyaltree_info', $args);
		while ( $split_check_detail = mysqli_fetch_assoc($split_check_details) )
		{
			$txn_id = $split_check_detail['loc_id'] .'_'. $split_check_detail['order_id'] .'_'. $split_check_detail['check'];

			$result = $this->sendTxn($split_check_detail + $args);

			// Update split_check_details (unless flag-no-scd-update is)
			if ( !isset($args['flag-no-scd-update']) || empty($args['flag-no-scd-update']) )
			{
				$split_check_detail['dataname'] = $args['dataname'];
				$result['update_ok'] = $this->updateLoyalTreeInfo($split_check_detail, $result);
			}

			$results[$txn_id] = $result;
		}

		return $results;
	}

	/**
	 * Send LoyalTree white label txn
	 *
	 * @param  $args Array of input arguments with required keys: dataname, order_id, loc_id, check
	 *
	 * @return Array decoded from JSON response from LoyalTree endpoint
	 */
	public function sendTxn($args)
	{
		// Check for required args
		if ( empty($args['dataname']) || empty($args['order_id']) || empty($args['loc_id']) || empty($args['check']) )
		{
			return false;
		}

		// Manual pre-send modifications to args array
		$args['action'] = 'purchase';

		// Send txn
		$dataExtractor = new LoyalTreeDataExtractor($args);
		$data = $dataExtractor->getPurchase($args);

		$result_json = LoyalTreeIntegration::sendData($args, $data);
		$result = json_decode($result_json, true);

		// Manual post-send modifications to result array
		$result['sent'] = date('Y-m-d H:i:s');

		return $result;
	}

	/**
	 * Update split_check_details table in POS db with update loyaltree_info JSON
	 *
	 * @param $split_check_detail Array of split_check_details row to update with required keys: id, dataname, order_id, loc_id, check, loyaltree_info
	 *
	 * @return Boolean indicating whether table update was successful
	 */
	public function updateLoyalTreeInfo($split_check_detail, $result)
	{
		// Check for required args
		if ( empty($split_check_detail['id']) || empty($split_check_detail['loyaltree_info']) || empty($split_check_detail['order_id']) || empty($split_check_detail['loc_id']) || empty($split_check_detail['check']) || empty($result['sent']) )
		{
			return false;
		}

		// Convert loyaltree_info JSON into an array to work with it
		$loyaltree_info = json_decode($split_check_detail['loyaltree_info'], true);
		$loyaltree_info['json_status'] = (isset($result['Success']) && $result['Success']) ? 'success' : 'error';
		$loyaltree_info['sent'] = $result['sent'];
		if ($result['Error']) $loyaltree_info['sent'] = $result['Error'];
		if ($result['Message']) $loyaltree_info['message'] = $result['Message'];

		// Put the loyaltree_info back into JSON and update split_check_details with it
		$split_check_detail['loyaltree_info'] = json_encode($loyaltree_info, JSON_UNESCAPED_SLASHES);
		$update_ok = $this->queryDatabase('update_loyaltree_info', $split_check_detail);

		return $update_ok;
	}

	/**
	 * Handles all database queries and connections
	 *
	 * @param  $query_name String identifying query to run
	 * @param  $args Array of input arguments, if needed for query
	 *
	 * @return mixed Returns mysql resource when $return_single_row is false, array when $return_single_row is true, bool for inserts, updates, failures
	 */
	private function queryDatabase($query_name, $args = array())
	{
		$return_single_row = true;
		$return_all_rows_array = false;
		$die_on_zero_rows  = true;
		$die_on_db_errors  = true;
		$update_or_insert  = false;

		switch ( $query_name )
		{
			case 'whitelabel_restaurants':
				$sql = "SELECT `id` AS `restaurantid`, `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `disabled` = 0 AND `modules` LIKE '%extensions.loyalty.+loyaltree_whitelabel%'";
				$return_single_row = false;
				$die_on_zero_rows  = false;
				break;

			case 'unsent_loyaltree_info':
				$sql = "SELECT `id`, `order_id`, `loc_id`, `check`, `loyaltree_info` FROM `poslavu_[dataname]_db`.`split_check_details` WHERE `loyaltree_info` LIKE '%\"sent\":\"\"%'";
				$return_single_row = false;
				$die_on_zero_rows  = false;
				break;

			case 'update_loyaltree_info':
				$sql = "UPDATE `poslavu_[dataname]_db`.`split_check_details` SET `loyaltree_info` = '[loyaltree_info]' WHERE `id` = '[id]'  AND `order_id` = '[order_id]' AND `loc_id` = '[loc_id]' AND `check` = '[check]' AND `loyaltree_info` LIKE '%\"sent\":\"\"%' LIMIT 2";
				$update_or_insert  = true;
				break;

			default:
				die(__METHOD__ ." got recognized query {$query_name} for dataname {$this->dataname}\n");
				break;
		}

		$result = mlavu_query($sql, $args);

		if ( $result === false && $die_on_db_errors )
		{
			throw new Exception("Database error - {$query_name} query got: ". mysqli_error() ."\n");
		}
		else if ( $result !== false && $return_all_rows_array )
		{
			$all_rows = array();
			while ( $row = mysqli_fetch_assoc($result) )
			{
				$all_rows[] = $row;
			}
			return $all_rows;
		}
		else if ( $update_or_insert )
		{
			return $result;
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			error_log("No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
			throw new Exception("Database error - No row for '{$query_name}' query for {$this->dataname}: sql={$sql} args=". json_encode($args));
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}}
