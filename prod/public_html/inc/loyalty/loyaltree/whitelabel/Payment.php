<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/SendTransactionOfflineSupported#payment
 */

class Payment
{
	public $PaymentID;
	public $Amount;
	public $VoidPayment;
	public $Type;
	public $PaymentTime;
	public $IsCurrent;  // only for Redeem
	public $UserID;
	public $UserIDType;
	public $PaymentReceiptCode;  // only for SendTransactionWithReceiptCode
}