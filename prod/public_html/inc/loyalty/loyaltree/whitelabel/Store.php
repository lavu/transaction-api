<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/RetailerSignup#store
 */

class Store
{
	public $StoreID;
	public $POSStoreID;
	public $Staff;
	public $Catalog;
}