<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/Redeem#transactionDetail
 */

class TransactionDetail
{
	public $TransactionID;
	public $TableID;
	public $GuestCount;
	public $TotalAmount;
	public $Items;
	public $Payments;
}