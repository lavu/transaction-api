<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/Redeem
 */

class Redeem
{
	public $APIKey;
	public $Username;
	public $Password;
	public $StoreID;
	public $ServerID;
	public $RewardCode;
	public $TransactionDetails;
}