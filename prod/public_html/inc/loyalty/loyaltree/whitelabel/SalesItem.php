<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/SendTransactionOfflineSupported#salesitem
 */

class SalesItem
{
	public $ItemID;
	public $CategoryID;
	public $Quantity;
	public $LineTotal;
	public $TaxIncludedInLineTotal;
	public $ModifierAmount;
	public $DiscountAmount;
	public $OrderTime;
}