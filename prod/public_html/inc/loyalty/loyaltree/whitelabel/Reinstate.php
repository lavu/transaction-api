<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/ReinstateRedemption
 */

class Reinstate
{
	public $APIKey;
	public $Username;
	public $Password;
	public $StoreID;
	public $RedemptionID;
	public $IsDeal;
	public $TransactionID;
	public $Reason;

	public function __construct($args)
	{
		$this->APIKey = $args['APIKey'];
		$this->Username = $args['Username'];
		$this->Password = $args['Password'];
		$this->StoreID = $args['StoreID'];
		$this->RedemptionID = $args['RedemptionID'];
		$this->IsDeal = (bool) $args['IsDeal'];
		$this->TransactionID = $args['TransactionID'];
		$this->Reason = $args['Reason'];
	}

}