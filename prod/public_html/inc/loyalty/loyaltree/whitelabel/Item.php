<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/RetailerSignup#item
 */

class Item
{
	public $ItemID;
	public $SKU;
	public $Name;
	public $Description;
}