<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/RetailerSignup#server
 */

class Staff
{
	public $StaffID;
	public $FirstName;
	public $LastName;
}