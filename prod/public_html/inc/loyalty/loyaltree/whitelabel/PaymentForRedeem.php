<?php

/**
 * UNUSED - just kept in case we ended up needing to use it
 * From API documentation: http://integration.loyaltree.com/API/Redeem#payment
 * Note: this class was supposed to be called Payment but it differed slightly from the
 * Payment class used by SendTransactionWithReceiptCode so I renamed it PaymentForRedeem
 * (differences: has IsCurrent but is missing PaymentReceiptCode)
 */

class PaymentForRedeem
{
	public $PaymentID;
	public $Amount;
	public $VoidPayment;
	public $Type;
	public $PaymentTime;
	public $IsCurrent;
	public $UserID;
	public $UserIDType;
}