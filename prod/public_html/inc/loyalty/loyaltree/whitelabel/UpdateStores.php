<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/RetailerSignup
 */

class UpdateStores
{
	public $APIKey;
	public $Username;
	public $Password;
	public $Stores;
}