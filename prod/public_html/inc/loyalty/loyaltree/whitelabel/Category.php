<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/RetailerSignup#category
 */

class Category
{
	public $CategoryID;
	public $Name;
	public $Description;
	public $Items;
}