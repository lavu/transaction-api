<?php

/**
 * From API documentation: http://integration.loyaltree.com/API/SendTransactionOfflineSupported
 */

class SendTransactionWithCode
{
	public $APIKey;
	public $Username;
	public $Password;
	public $StoreID;
	public $ServerID;
	public $TableID;
	public $GuestCount;
	public $TransactionID;
	public $ParentTransactionID;
	public $TotalAmount;
	public $CheckDiscount;
	public $CheckOpenTime;
	public $TransactionTime;
	public $GuestReceiptCode;
	public $Items;
	public $Payments;
}