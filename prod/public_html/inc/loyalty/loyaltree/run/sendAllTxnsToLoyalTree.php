<?php

// For LoyalTreeIntegration::loyalTreeDebug()
global $loyaltree_debug;
global $data_name;
$loyaltree_debug = true;
$data_name = $args['dataname'];

require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/LoyalTreeTxnSender.php');
$txnSender = new LoyalTreeTxnSender();

$args = array();
//echo "Sending all LoyalTree txns with args=". json_encode($args) ."\n";

$results = $txnSender->sendAllTxns($args);
//echo "Got results=". print_r($results, true);

exit;