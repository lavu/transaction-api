<?php

if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname>\n";
	exit;
}

// For LoyalTreeIntegration::loyalTreeDebug()
global $loyaltree_debug;
global $data_name;
$loyaltree_debug = true;
$data_name = $args['dataname'];

require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/LoyalTreeTxnSender.php');
$txnSender = new LoyalTreeTxnSender();

$args = array(
	'dataname' => $argv[1],
);
//echo "Sending all LoyalTree txns for dataname with args=". json_encode($args) ."\n";

$results = $txnSender->sendTxnsForDataname($args);
//echo "Got results=". print_r($results, true);

exit;