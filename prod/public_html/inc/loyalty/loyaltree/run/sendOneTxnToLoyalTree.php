<?php

if ( empty($argv[1]) || empty($argv[2]) || empty($argv[3]) || empty($argv[4]) )
{
	echo "Usage: {$argv[0]} <dataname> <loc_id> <ordr_id> <check>\n";
	exit;
}

// For LoyalTreeIntegration::loyalTreeDebug()
global $loyaltree_debug;
global $data_name;
$loyaltree_debug = true;
$data_name = $args['dataname'];

require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/LoyalTreeTxnSender.php');
$txnSender = new LoyalTreeTxnSender();

$args = array(
	'dataname' => $argv[1],
	'loc_id'   => $argv[2],
	'order_id' => $argv[3],
	'check'    => $argv[4],
);
//echo "Sending LoyalTree txn with args=". json_encode($args) ."\n";

$result = $txnSender->sendTxn($args);
//echo "Got result=". print_r($result, true);

exit;
