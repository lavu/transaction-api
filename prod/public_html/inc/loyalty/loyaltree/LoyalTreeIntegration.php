<?php

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/LoyalTreeConfig.php');
require_once('/home/poslavu/public_html/inc/loyalty/loyaltree/LoyalTreeDataExtractor.php');

/**
 * Unified LoyalTree API that supports both White Label and Blue Label API calls
 *
 */
class LoyalTreeIntegration
{
	/**
	 * Unified LoyalTree API call for White Label SendTransactionWithCode or Blue Label purchase
	 *
	 * @param $args Associative array of arguments that get passed through to LoyalDataExtractor and other LoyalTreeIntegration methods
	 *
	 * @return JSON result string
	 */
	public static function purchase($args)
	{
		if ( empty($args['dataname']) )
		{
			error_log(__CLASS__ ."->". __METHOD__ ."() is missing required arg: ". json_encode($args));
			return false;
		}

		$result = null;

		// White Label doesn't send payload to LoyalTree immediately.  For now, we just return a success, a blank sent value (that gets saved in split_check_details
		// and acts as a flag for the separate, cron'd process that transmits txns to LoyalTree), and a newly generated UUID-ish value that the app encodes in a QR code.
		if ( isset($args['app']) && $args['app'] == LoyalTreeConfig::WHITELABEL_APP )
		{
			// Generate the UUID to encoded in our QR code
			$dataExtractor = new LoyalTreeDataExtractor();
			$args['qrcode'] = $args['restaurantid'] .'_'. $args['loc_id'] .'_'. $args['order_id'] .'_'. $args['check'] .'_'. $dataExtractor->getUUID();
			$result = json_encode(array('Success' => true, 'qrcode'  => $args['qrcode']), true);

			$result = self::processPurchaseResultWhiteLabel($args, $result);
		}
		// Blue Label sends its purchase info synchronously
		else
		{
			try
			{
				$dataExtractor = new LoyalTreeDataExtractor($args);
				$data = $dataExtractor->getPurchase($args);
				$result = self::sendData($args, $data);
			}
			catch ( Exception $e )
			{
				throw $e;
			}

			$result = self::processPurchaseResultBlueLabel($result);
		}


		return $result;
	}

	/**
	 * Unified LoyalTree API call for White Label Redeem or Blue Label redeem
	 *
	 * @param $args Associative array of arguments that get passed through to LoyalDataExtractor and other LoyalTreeIntegration methods
	 *
	 * @return JSON results string
	 */
	public static function redeem($args)
	{
		if ( empty($args['dataname']) )
		{
			error_log(__CLASS__ ."->". __METHOD__ ."() is missing required arg: ". json_encode($args));
			return false;
		}

		$result = null;

		try
		{
			$dataExtractor = new LoyalTreeDataExtractor($args);
			$data = $dataExtractor->getRedeem($args);
			$result = self::sendData($args, $data);
		}
		catch ( Exception $e )
		{
			throw $e;
		}

		if ( isset($args['app']) && $args['app'] == LoyalTreeConfig::WHITELABEL_APP )
		{
			$result = self::processRedeemResultWhiteLabel($result, $data['payload']);
		}
		else
		{
			$result = self::processRedeemResultBlueLabel($result, $data['payload']);
		}

		return $result;
	}

	/**
	 * Unified LoyalTree API call for White Label Reinstate or Blue Label reinstate
	 *
	 * @param $args Associative array of arguments that get passed through to LoyalDataExtractor and other LoyalTreeIntegration methods
	 *
	 * @return JSON results string
	 */
	public static function reinstate($args)
	{
		if ( empty($args['dataname']) )
		{
			error_log(__CLASS__ ."->". __METHOD__ ."() is missing required arg: ". json_encode($args));
			return false;
		}

		$result = null;

		try
		{
			$dataExtractor = new LoyalTreeDataExtractor($args);
			$data = $dataExtractor->getReinstate($args);
			$result = self::sendData($args, $data);
		}
		catch ( Exception $e )
		{
			throw $e;
		}

		if ( isset($args['app']) && $args['app'] == LoyalTreeConfig::WHITELABEL_APP )
		{
			$result = self::processReinstateResultWhiteLabel($result);
		}
		else
		{
			$result = self::processReinstateResultBlueLabel($result);
		}


		return $result;
	}

	/**
	 * Unified LoyalTree API call for White Label UpdateStores or Blue Label signup
	 *
	 * @param $args Associative array of arguments that get passed through to LoyalDataExtractor and other LoyalTreeIntegration methods
	 *
	 * @return JSON results string
	 */
	public static function signup($args)
	{
		if ( empty($args['dataname']) )
		{
			error_log(__CLASS__ ."->". __METHOD__ ."() is missing required arg: ". json_encode($args));
			return false;
		}

		$result = null;

		try
		{
			$dataExtractor = new LoyalTreeDataExtractor($args);
			$data = $dataExtractor->getSignup($args);
			$result = self::sendData($args, $data);
		}
		catch ( Exception $e )
		{
			throw $e;
		}

		return $result;
	}

	/**
	 * Unified LoyalTree API call for White Label UpdateStores or Blue Label preload
	 *
	 * @param $args Associative array of arguments that get passed through to LoyalDataExtractor and other LoyalTreeIntegration methods
	 *
	 * @return JSON results string
	 */
	public static function update($args)
	{
		if ( empty($args['dataname']) )
		{
			error_log(__CLASS__ ."->". __METHOD__ ."() is missing required arg: ". json_encode($args));
			return false;
		}

		$result = null;

		try
		{
			$dataExtractor = new LoyalTreeDataExtractor($args);
			$data = $dataExtractor->getUpdate($args);
			$result = self::sendData($args, $data);
		}
		catch ( Exception $e )
		{
			throw $e;
		}

		return $result;
	}

	/**
	 * Debug logging via error log function kept for backwards compatibility with original LoyalTree class
	 *
	 * @param $msg String for debug message
	 *
	 * @return void
	 */
	public static function loyalTreeDebug($msg)
	{
		global $loyaltree_debug;
		global $data_name;

		if ($loyaltree_debug) error_log("LoyalTree - ".$data_name." - req_id: ".$_REQUEST['YIG']." - ".$msg);
	}

	/**
	 * Method to send White Label JSON / Blue Label XML payload to appropriate LoyalTree API endpoint
	 *
	 * @param $data_extraction Associative array with required elements (payload, endpoint) and optional elements (NO_CURL, content-type)
	 *
	 * @return JSON response string
	 */
	public static function sendData($args, $data_extraction)
	{
		if ( empty($data_extraction['payload']) || !empty($args['flag-no-curl']) )
		{
			return null;
		}

		self::loyalTreeDebug("In ". __METHOD__ ."() - endpoint=". $data_extraction['endpoint']);
		isset($data_extraction['content-type']) AND self::loyalTreeDebug("content-type=". $data_extraction['content-type']);
		self::loyalTreeDebug("start request - ".$data_extraction['payload']);

		$start_time = microtime(true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $data_extraction['endpoint']);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_extraction['payload']);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		if (!empty($data_extraction['content-type'])) curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: {$data_extraction['content-type']}"));

		$response = curl_exec($ch);
		curl_close($ch);

		$end_time = microtime(true);
		$process_time = round((($end_time - $start_time) * 1000), 2) . "ms";

		self::loyalTreeDebug("end request - duration: ".$process_time);
		self::loyalTreeDebug("response - ". $response);

		return $response;
	}

	/**
	 * Process response from LoyalTree Blue Label Purchase API call
	 *
	 * @param $result_xml - String containing XML returned from LoyalTree Blue Label Purchase API call
	 *
	 * @return JSON of processed value of response that LoyalTreeIntegration::purchase() should return
	 */
	public static function processPurchaseResultBlueLabel($result_xml)
	{
		if ( $result_xml === null )
		{
			return $result_json;
		}

		$result = simplexml_load_string($result_xml);

		$response = array();

		// Empty error
		if ( empty($result_xml) )
		{
			$response['json_status'] = 'error';
			$response['error'] = 'empty response';

			// Populate values that the app will use to display the error
			$response['title']   = 'Loyalty Error';  // App currently ignores this with hard-coded value ("LoyalTree Error"), but leaving it in in case we fix that someday
			$response['message'] = $response['error'];

		}
		// Error
		else if ( $result->error )
		{
			$response['json_status'] = 'error';
			$response['error'] = (string) $result->error;

			// Populate values that the app will use to display the error
			$response['title']   = 'Loyalty Error';  // App currently ignores this with hard-coded value ("LoyalTree Error"), but leaving it in in case we fix that someday
			$response['message'] = $response['error'];
		}
		// Success
		else
		{
			$response['json_status'] = 'success';
			$response['version']     = (string) $result->version;
			$response['text']        = (string) $result->text;
			$response['qrstring']    = (string) $result->qrstring;
			$response['app']         = 'bluelabel';
		}

		$response_json = json_encode($response, JSON_UNESCAPED_SLASHES);

		return $response_json;
	}

	/**
	 * Process response from LoyalTree White Label Purchase API call
	 *
	 * @param $result - Array containing simulated response values from LoyalTree White Label Purchase API call
	 *
	 * @return JSON of processed value of response that LoyalTreeIntegration::purchase() should return
	 */
	public static function processPurchaseResultWhiteLabel($args, $result_json)
	{
		if ( $result_json === null )
		{
			return $result_json;
		}

		$result = json_decode($result_json, true);

		$response = array();

		// Empty error
		if ( empty($result_json) )
		{
			$response['json_status'] = 'error';
			$response['error'] = 'empty response';

			// Populate values that the app will use to display the error
			$response['title']   = 'Loyalty Error';  // App currently ignores this with hard-coded value ("LoyalTree Error"), but leaving it in in case we fix that someday
			$response['message'] = $response['error'];

		}
		// Error
		else if ( isset($result['Error']) && !empty($result['Error']) )
		{
			$response['json_status'] = 'error';
			$response['error'] = $result['Error'];

			// Populate values that the app will use to display the error
			$response['title']   = 'Loyalty Error';  // App currently ignores this with hard-coded value ("LoyalTree Error"), but leaving it in in case we fix that someday
			$response['message'] = $response['error'];
		}
		// Success
		else
		{
			// Generate the response that will be saved in split_check_details table as JSON
			$response['json_status'] = 'success';  // was 'unsent' but that didn't wasn't getting displayed in app
			$response['sent']        = '';
			$response['text']        = (isset($args['text']) && !empty($args['text'])) ? $args['text'] : "Scan this code with your Rewards app within EIGHT HOURS to collect reward points for today's purchase.";
			$response['qrstring']    = LoyalTreeConfig::QRCODE_ENCODED_URL_BASE .'/'. $result['qrcode'];
			$response['qrcode']      = $result['qrcode'];
			$response['app']         = 'whitelabel';
		}

		$response_json = json_encode($response, JSON_UNESCAPED_SLASHES);

		return $response_json;
	}

	/**
	 * Process response from LoyalTree Blue Label Redeem API call
	 *
	 * @param $result_xml - String containing XML returned from LoyalTree Blue Label Redeem API call
	 * @param $redeem_xml - String containing XML of Blue Label Redeem obj sent to LoyalTree
	 *
	 * @return JSON of processed value of response that LoyalTreeIntegration::redeem() should return
	 */
	public static function processRedeemResultBlueLabel($result_xml, $redeem_xml)
	{
		if ( $result_xml === null )
		{
			return $result_xml;
		}

		$result = simplexml_load_string($result_xml);

		if ( isset($result->error) && !empty($result->error)  )
		{
			$response['json_status'] = 'error';
			$response['error'] = (string) $result->error;

			// Populate values that the app will use to display the error
			$response['title']   = 'Loyalty Error';  // App currently ignores this with hard-coded value ("LoyalTree Error"), but leaving it in in case we fix that someday
			$response['message'] = $response['error'];
		}
		else
		{
			$response['json_status']  = 'success';
			$response['id']           = $result->id;
			$response['percent']      = $result->percent;
			$response['isdeal']       = $result->isdeal;
			$response['amount']       = $result->amount;
			$response['highestValue'] = $result->highestValue;
			$response['items']        = array();

			// Make sure all values are strings because the app (v3.3.0) will blow up if it gets a number for amount
			$response['id']           = (string) $response['id'];
			$response['percent']      = (string) $response['percent'];
			$response['isdeal']       = (string) $response['isdeal'];
			$response['amount']       = (string) $response['amount'];
			$response['highestValue'] = (string) $response['highestValue'];

			foreach ( $result->items as $item )
			{
				foreach ( $item as $i )
				{
					$response['items'][] = array('catid' => (string) $i->catid, 'itemid' => (string) $i->itemid);
				}
			}
		}

		$response_json = json_encode($response);

		return $response_json;
	}

	/**
	 * Process response from LoyalTree White Label Redeem API call
	 *
	 * @param $result_json - String containing JSON returned from LoyalTree White Label Redeem API call
	 * @param $redeem_json - String containing JSON of White Label Redeem obj sent to LoyalTree in payload
	 *
	 * @return JSON of processed value of response that LoyalTreeIntegration::redeem() should return
	 */
	public static function processRedeemResultWhiteLabel($result_json, $redeem_json)
	{
		if ( $result_json === null )
		{
			return $result_json;
		}

		$result = json_decode($result_json, true);

		// Error
		if ( isset($result['Error']) && !empty($result['Error']) )
		{
			$response['json_status'] = 'error';
			$response['error'] = $result['Error'];

			// Populate values that the app will use to display the error
			$response['title']   = 'Loyalty Error';  // App currently ignores this with hard-coded value ("LoyalTree Error"), but leaving it in in case we fix that someday
			$response['message'] = $response['error'];
		}
		// Success
		else
		{
			$response['json_status']  = 'success';
			$response['id']           = $result['RedemptionID'];
			$response['percent']      = isset($result['Percent']) ? $result['Percent'] : '';
			$response['isdeal']       = $result['IsDeal'];
			$response['amount']       = isset($result['FlatDiscountAmount']) ? $result['FlatDiscountAmount'] : '';
			$response['items']        = array();

			// Make sure all values are strings because the app (v3.3.0) will blow up if it gets a number for amount
			$response['id']           = (string) $response['id'];
			$response['percent']      = (string) $response['percent'];
			$response['isdeal']       = (string) $response['isdeal'];
			$response['amount']       = (string) $response['amount'];

			$redeem = json_decode($redeem_json);

			foreach ( $redeem->TransactionDetails as $transactionDetail )
			{
				foreach ( $transactionDetail->Items as $item )
				{
					$response['items'][]= array('catid' => (string) $item->CategoryID, 'itemid' => (string) $item->ItemID);
				}
			}
		}

		$response_json = json_encode($response);

		return $response_json;
	}

	/**
	 * Process response from LoyalTree Blue Label Reinstate API call
	 *
	 * @param $result_xml - String containing XML returned from LoyalTree Blue Label Redeem API call
	 *
	 * @return JSON of processed value of response that LoyalTreeIntegration::reinstate() should return
	 */
	public static function processReinstateResultBlueLabel($result_xml)
	{
		if ( $result_xml === null )
		{
			return $result_xml;
		}

		$result = simplexml_load_string($result_xml);

		// Error
		if ( isset($result->error) && !empty($result->error)  )
		{
			$response['json_status'] = 'error';
			$response['error'] = (string) $result->error;

			// Populate values that the app will use to display the error
			$response['title']   = 'Loyalty Error';  // App currently ignores this with hard-coded value ("LoyalTree Error"), but leaving it in in case we fix that someday
			$response['message'] = (string) $result->error;
		}
		// Success
		else
		{
			$response['json_status']  = 'success';
		}

		$response_json = json_encode($response);

		return $response_json;
	}

	/**
	 * Process response from LoyalTree Reinstate API call
	 *
	 * @param $result_json - String containing JSON returned from LoyalTree Redeem API call
	 *
	 * @return JSON of processed value of response that LoyalTreeIntegration::reinstate() should return
	 */
	public static function processReinstateResultWhiteLabel($result_json)
	{
		if ( $result_json === null )
		{
			return $result_json;
		}

		$result = json_decode($result_json, true);

		// Error
		if ( isset($result['Error']) && !empty($result['Error']) )
		{
			$response['json_status'] = 'error';
			$response['error'] = $result['Error'];

			// Populate values that the app will use to display the error
			$response['title']   = 'Loyalty Error';  // App currently ignores this with hard-coded value ("LoyalTree Error"), but leaving it in in case we fix that someday
			$response['message'] = $response['error'];
		}
		// Success
		else
		{
			$response['json_status']  = 'success';
		}

		$response_json = json_encode($response);

		return $response_json;
	}

}