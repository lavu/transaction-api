<?php

class LoyalTreeConfig
{
	const ENV = 'qa';

	const BLUELABEL_APP  = 'bluelabel';
	const WHITELABEL_APP = 'whitelabel';

	const BLUELABEL_MODULE  = 'extensions.loyalty.+loyaltree';
	const WHITELABEL_MODULE = 'extensions.loyalty.+loyaltree_whitelabel';

	const QRCODE_DIR_BASE = '/home/poslavu/public_html/admin';
	const QRCODE_DISPLAY_URL_BASE = 'http://admin.poslavu.com';
	const QRCODE_ENCODED_URL_BASE = 'http://qa.qr.loyaltr.ee';  //qa (prod = http://qr.loyaltr.ee)
	const QRCODE_ERROR_CORRECTION_LEVEL = 'L';
	const QRCODE_MATRIX_POINT_SIZE      = 4;

	const WHITELABEL_API_KEY = '73CsSrv4qEafyghxNZvPTqmy7Fps2ymQ';             //qa
	const WHITELABEL_API_ENDPOINT_BASE = 'http://qa.cloudpos.svc.loyaltr.ee';  //qa
	const WHITELABEL_API_ENDPOINT_PATH_REDEEM    = '/api/1.0/Redeem';
	const WHITELABEL_API_ENDPOINT_PATH_REINSTATE = '/api/1.0/Reinstate';
	const WHITELABEL_API_ENDPOINT_PATH_PURCHASE  = '/api/1.0/SendTransactionWithCode';
	const WHITELABEL_API_ENDPOINT_PATH_SIGNUP    = '/api/1.0/UpdateStores';

	const BLUELABEL_API_KEY = '5ubvjs85js7v7vusarfd';
	const BLUELABEL_API_ENDPOINT_BASE = 'https://loyaltree.com';
	const BLUELABEL_API_ENDPOINT_PATH_REDEEM    = '/pos/poslavu/redeem.php';
	const BLUELABEL_API_ENDPOINT_PATH_REINSTATE = '/pos/poslavu/redeem_reinstate.php';
	const BLUELABEL_API_ENDPOINT_PATH_PRELOAD   = '/pos/poslavu/preload.php';
	const BLUELABEL_API_ENDPOINT_PATH_PURCHASE  = '/pos/poslavu/qrcode.php';
	const BLUELABEL_API_ENDPOINT_PATH_SIGNUP    = '/pos/poslavu/signup.php';

// This will be stored in the config table but all accounts are to use these same values during testing so I wanted to document them here, for now
//	const WHITELABEL_API_STOREID       = '94E8108E-1856-4235-B640-0728D4C9E5E8';
//	const WHITELABEL_API_USERNAME      = '8D9FB993-40C4-4400-8572-21BC5A3530F2';
//	const WHITELABEL_API_PASSWORD      = 'O7kCgI8M5UYOMUBeTrFyPV1rJ';

}
