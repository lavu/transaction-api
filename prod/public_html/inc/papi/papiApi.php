<?php
/**
 * Created by Code.
 * User: Hayat
 * Date: 30/01/19
 * Time: 12:37 PM
 */

require_once(__DIR__."/jwtPapi.php");

Class papiApi {
    private $tokenId;
    private $encryptionKey;
    private $papiApi;
    public function __construct($args){
        $jwtPapiObj = new jwtPapi($args);
        $this->tokenId = $jwtPapiObj->generatePapiAuthToken();
        $this->encryptionKey = $jwtPapiObj->genEncryptedDataname();
        $this->papiApi = getenv('PAPI_API');
    }

    public function papiTransaction($transactionInfo, $papiRequest) {
        $transtype = $papiRequest['transtype'];
        switch ($transtype) {
			case "Adjustment" :
			case "PreAuthCapture" :
                $captureID = empty ( $papiRequest['authcode'] ) ? (empty ( $papiRequest['txnid'] ) ? "" : $papiRequest['txnid']) : $papiRequest['authcode'];
				if (empty ( $captureID )) {
					return "0|Unable to retrieve transaction identifier";
				}
				$base_amount = $transactionInfo ['amount'];
				$tipAmount = $papiRequest['tipAmount'];
				$card_amount = str_replace ( ",", "", number_format ( $base_amount * 1 + $tipAmount * 1, 2 ) ) * 100;
				$fields = '{"amount": ' . $card_amount . '}';
				$papiApi = '/api/payments/' . $captureID . '/capture';
				break;
			default :
				return "0|Unsupported Transaction Type";
        }
        $response = $this->makePapiPayment ( $fields, $papiApi );
        return $response;
    }

    private function makePapiPayment($fields, $papiApi){
        $papiURL = $this->papiApi.$papiApi;
        $pch = curl_init();
		if ($papiURL == "") {
            return "";
        }

        curl_setopt($pch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($pch, CURLOPT_URL, $papiURL);
		if(lavuDeveloperStatus()){
			curl_setopt($pch, CURLOPT_SSL_VERIFYPEER, false);
        }

        if ($fields) {
            curl_setopt($pch, CURLOPT_POSTFIELDS, $fields);
        }
        curl_setopt($pch, CURLOPT_ENCODING, "");
        curl_setopt($pch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($pch, CURLOPT_FOLLOWLOCATION, FALSE);
        curl_setopt($pch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept: application/json', 'Accept-Language: en_US', 'Authorization: '.$this->tokenId));
        $response = curl_exec($pch);
        $http_status = curl_getinfo($pch, CURLINFO_HTTP_CODE);
        curl_close($pch);
        if ($response == '' && $http_status == 500) {
            $response = 'The transaction id is not found';
        }
        return $response;
    }
}