<?php
/**
 * Created by Code.
 * User: Hayat
 * Date: 25/01/19
 * Time: 12:37 PM
 */

require_once(__DIR__.'/../../3rd_party/jwt/vendor/autoload.php');
require_once(__DIR__.'/../../admin/cp/resources/core_functions.php');

use \Firebase\JWT\JWT;

Class jwtPapi {
    private $secretKey;
    private $alg = 'HS256'; //encryption algorithm
    private $secret;
    public $tokenId;
    private $encryptionKey;
    private $encryptionKeyAlg = 'aes-256-cbc';
    private $dataName;
    private $locationId;
    
    public function __construct($args){
        $this->secretKey = getenv('PAPI_SECRET');
        $this->dataName = $args['dataname'];
        $this->locationId = $args['locationid'];
        $this->tokenId = $this->dataName.'_'.$this->locationId;
        $this->encryptionKey = getenv('ENCRYPTION_KEY');
    }

    public function generatePapiAuthToken(){
        $jwtData = array("userId" => $this->tokenId);
        $authToken = JWT::encode($jwtData, $this->secretKey, $this->alg);
        return $authToken;
    }

    private function encrypt($message) {
        $ivlen = openssl_cipher_iv_length($this->encryptionKeyAlg);
        $iv = substr(bin2hex(openssl_random_pseudo_bytes($ivlen)), 0, $ivlen);    //use this in production
        $encrypted = base64_encode($iv) . openssl_encrypt($message, $this->encryptionKeyAlg, $this->encryptionKey, 0, $iv);
        return urlencode($encrypted);
    }

    public function genEncryptedDataname() {
        return $this->encrypt($this->dataName);
    }

    private function decrypt ($encrypted) {
        $iv = base64_decode(urldecode(substr($encrypted, 0, 24)));
        return openssl_decrypt(substr($encrypted, 24), $this->encryptionKeyAlg, $this->encryptionKey, 0, $iv);
    }

    public function genDecryptDataname($ciphertext) {
        return $this->decrypt($ciphertext);
    }
}