<?php
class Zuora_AmendRequest extends Zuora_Object
{
    ///const TYPE_NAMESPACE = 'http://api.zuora.com/';

    protected $zType = 'AmendRequest';
    
    /**
     * @var Zuora_AmendRequest
     */
    
    public function getSoapVar()
    {
        $data = array();
        if (isset($this->Amendments)) $data['Amendments'] = $this->Amendments->getSoapVar();
        if (isset($this->AmendOptions)) $data['AmendOptions'] = $this->AmendOptions->getSoapVar();
        if (isset($this->PreviewOptions)) $data['PreviewOptions'] = $this->PreviewOptions->getSoapVar();

        return new SoapVar(
            (array) $data,
            SOAP_ENC_OBJECT,
            $this->zType,
            self::TYPE_NAMESPACE
        );
    }
}
