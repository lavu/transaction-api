<?php
class Zuora_Amendment extends Zuora_Object
{
    protected $zType = 'Amendment';

    public function getSoapVar()
    {
        $data = $this->_data;
        if (isset($this->RatePlanData)) $data['RatePlanData'] = $this->RatePlanData->getSoapVar();

        return new SoapVar(
            $data,
            SOAP_ENC_OBJECT,
            $this->zType,
            self::TYPE_NAMESPACE
        );
    }
}
