<?php
class Zuora_Amendments extends Zuora_Object
{
    protected $zType = 'Amendments';

    const TYPE_NAMESPACE = 'http://api.zuora.com/';
    const ZOBJ_NAMESPACE = 'http://object.api.zuora.com/';

    public function getSoapVar()
    {
        foreach ($this->_data as $key => $val) {
            if ($key == 'RatePlanData') {
                $data[] = $this->RatePlanData->getSoapVar();
            }
            else {
                $data[] = new SoapVar(
                    $val,
                    XSD_STRING,
                    null,
                    null,
                    $key,
                    self::ZOBJ_NAMESPACE
                );
            }
        }

        return new SoapVar(
            $data,
            SOAP_ENC_OBJECT,
            'Amendment',
            self::TYPE_NAMESPACE,
            'Amendments',
            self::TYPE_NAMESPACE
        );
    }
}
