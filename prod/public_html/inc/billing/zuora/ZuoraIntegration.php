<?php

require_once(dirname(__FILE__). '/../../../../private_html/ZuoraConfig.php');
require_once(dirname(__FILE__).'/api/API.php');
require_once(dirname(__FILE__).'/ZuoraDataExtractor.php');

class ZuoraIntegration
{
	public $zApi;
	public $zDataExtractor;

	public function __construct()
	{
		$this->zApi = $this->initApi();
		$this->zDataExtractor = new ZuoraDataExtractor();
	}

	public function activateSubscription($vars)
	{
		//$this->logDebug(__METHOD__ ." called argument(s): vars=". json_encode($vars));

		$activated = false;

		if ( !empty($vars['zSubscriptionId']) )
		{
			$zSubscription = new Zuora_Subscription();
			$zSubscription->Id = $vars['zSubscriptionId'];
			$zSubscription->Status = 'Active';

			// Copy every single key value pair from $vars into the Zuora Account object for updating
			unset($vars['zSubscriptionId']);
			foreach ( $vars as $field => $value )
			{
				// Strip the timestamp portion of any Date fields we may have gotten from fetching a Zuora Subscription object
				if ( strstr($field, 'Date') !== false )
				{
					$zSubscription->{$field} = explode('T', $value)[0];
				}
				else
				{
					$zSubscription->{$field} = $value;
				}
			}

			//$this->logDebug("zSubscription=". print_r($zSubscription, true));

			$response = $this->createZuoraObject('zSubscription', 'update', array($zSubscription));

			//$this->logDebug("...zSubscription update response=". print_r($response, true));
			$activated = true;  // TO DO :
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $activated;
	}

	public function createAccount($vars)
	{
		// Check required args
		if ( empty($vars['dataname']) )
		{
			$this->logError(__METHOD__ ."() missing required arg(s): ". json_encode($vars));
			return '';
		}

		// Zuora Data Extractor
		$this->zDataExtractor->init($vars['dataname']);

		// Account
		$zAccount = $this->zDataExtractor->getAccount($vars);

		$response = $this->createZuoraObject('zAccount', 'create', array($zAccount));

		if ( $response->result->Success )
		{
			$zAccount->Id = $response->result->Id;
		}
		else
		{
			$zAccount = null;
		}

		return $zAccount;
	}

	public function createAccountWithContacts($vars, $contact_info = null)
	{
		$zAccountId = '';
		$zBillToContactId = '';
		$zSoldToContactId = '';

		$zAccount = $this->createAccount($vars);

		if ( $zAccount !== null )
		{
			$zAccountId = $zAccount->Id;
			$account_update_vars = array('zAccountId' => $zAccountId, 'Status' => 'Active');

			// Create BillTo contact
			$zBillToContact = $this->zDataExtractor->getBillToContact($contact_info);
			$zBillToContact->AccountId = $zAccountId;

			$response = $this->createZuoraObject('zBillToContact', 'create', array($zBillToContact));

			if ( $response->result->Success )
			{
				$zBillToContactId = $response->result->Id;
				$zBillToContact->Id = $zBillToContactId;
				$account_update_vars['BillToId'] = $zBillToContactId;
			}

			// Create SoldTo Contact
			$zSoldToContact = $this->zDataExtractor->getSoldToContact();
			$zSoldToContact->AccountId = $zAccountId;

			// If the vital data fields for the SoldTo contact are the same as the BillTo contact, just use the BillTo as the SoldTo
			if ( $zBillToContact !== null && ($zBillToContact->FirstName == $zSoldToContact->FirstName) && ($zBillToContact->LastName == $zSoldToContact->LastName) && ($zBillToContact->Address1 == $zSoldToContact->Address1)  && ($zBillToContact->WorkEmail == $zSoldToContact->WorkEmail) )
			{
				$zSoldToContactId = $zBillToContactId;
				$account_update_vars['SoldToId'] = $zBillToContactId;
			}
			// Otherwise create a new contact for the SoldTo contact
			else
			{
				$response = $this->createZuoraObject('zSoldToContact', 'create', array($zSoldToContact));

				if ( $response->result->Success )
				{
					$zSoldToContactId   = $response->result->Id;
					$zSoldToContact->Id = $zSoldToContactId;
					$account_update_vars['SoldToId'] = $zSoldToContactId;
				}
			}

			// Activate account
			$account_activated = $this->updateAccount($account_update_vars);

			if ( !$account_activated )
			{
				$this->logError("Could not activate account (dataname={$vars['dataname']} zAccountId={$zAccountId})");
			}
		}

		return array($zAccountId, $zBillToContactId, $zSoldToContactId);
	}

	public function createHardwareSubscriptionWithPaymentMethodId($args, $contact_info = null)
	{
		// Check required args
		if ( empty($args['dataname']) || empty($args['event_date']) || empty($args['amount']) || empty($args['zPaymentMethodId']) )
		{
			$this->logError(__METHOD__ ."() missing required arg(s): ". json_encode($args));
			return '';
		}

		$this->initApi();

		// Use input args array to store
		if ( empty($args['product_name']) ) $args['product_name'] = 'Lavu POS Hardware';
		if ( empty($args['billing_product_type']) ) $args['billing_product_type'] = 'Hardware';

		// Zuora Data Extractor
		$this->zDataExtractor->init($args['dataname']);

		// Account
		$zAccount = $this->zDataExtractor->getAccount();
		$zAccount->DefaultPaymentMethodId = $args['zPaymentMethodId'];

		// SubscriptionData
		$zSubscriptionData = $this->zDataExtractor->getHardwareSubscriptionData($args);

		// SubscriptionOptions
		$zSubscribeOptions = new Zuora_SubscribeOptions(true, true);  // GenerateInvoice, ProcessPayments

		// PaymentMethod - uses the newly created PaymentMethod ID from the HPM page
		$zPaymentMethod = new Zuora_PaymentMethod();
		$zPaymentMethod->Id = $args['zPaymentMethodId'];

		if ( isset($args['zAccountId']) )
		{
			// We don't need to create the contacts for pre-existing accounts
			$zBillToContact = null;
			$zSoldToContact = null;

			// Set AccountId on all objects, since we have a pre-existing account
			$zAccount->Id = $args['zAccountId'];
			$zSubscriptionData->AccountId = $zAccount->Id;
			$zSubscriptionData->InvoiceOwnerId = $zAccount->Id;
		}
		else
		{
			// Need to actually create contacts for new Accounts
			$zBillToContact = $this->zDataExtractor->getBillToContact($contact_info);
			$zSoldToContact = $this->zDataExtractor->getSoldToContact();

			// Remove AccountId-related member completely instead of just blanking it out to prevent errors on subscribe()
			$zAccount->unsetVar('Id');
			$zSubscriptionData->zSubscription->unsetVar('AccountId');
			$zSubscriptionData->zSubscription->unsetVar('InvoiceOwnerId');
			$zBillToContact->unsetVar('AccountId');
			$zSoldToContact->unsetVar('AccountId');

		}

		//$this->logDebug("...zAccount=". print_r($zAccount, true));
		//$this->logDebug("...zSubscriptionData=". print_r($zSubscriptionData, true));
		//$this->logDebug("...zBillToContact=". print_r($zBillToContact, true));
		//$this->logDebug("...zSoldToContact=". print_r($zSoldToContact, true));
		//$this->logDebug("...zPaymentMethod=". print_r($zPaymentMethod, true));

		try
		{
			$response = $this->zApi->subscribe($zAccount, $zSubscriptionData, $zBillToContact, $zPaymentMethod, $zSubscribeOptions, $zSoldToContact);
		}
		catch ( Exception $e )
		{
			$this->logError($e->getMessage());
		}

		//$this->logDebug("...response=". print_r($response, true));

		// Extract resultant Subscription ID
		if ( isset($response->result) )  $result = $response->result;
		if ( isset($response->results) ) $result = $response->results;
		if ( isset($result->SubscriptionId) ) $zSubscriptionId = $result->SubscriptionId;

		return $zSubscriptionId;
	}

	public function createHostingSubscription($args, $contact_info = null, $import_existing_account = false)
	{
		// Check required args
		if ( empty($args['dataname']) )
		{
			$this->logError(__METHOD__ ."() missing required arg(s): ". json_encode($args));
			return '';
		}

		$this->initApi();

		// Use input args array to store
		if ( empty($args['product_name']) ) $args['product_name'] = 'Lavu POS Hardware';
		if ( empty($args['billing_product_type']) ) $args['billing_product_type'] = 'Hosting';

		// Zuora Data Extractor
		$this->zDataExtractor->init($args['dataname']);

		if ( !empty($args['start_date']) )
		{
			$this->zDataExtractor->invoice_day_of_month = date('j', strtotime($args['start_date']));
		}

		// Account
		$zAccount = $this->zDataExtractor->getAccount();
		$zAccount->DefaultPaymentMethodId = $args['zPaymentMethodId'];

		// SubscriptionData for existing Lavu account being imported
		if ( $import_existing_account )
		{
			$zSubscriptionData = $this->zDataExtractor->getActiveSubscription($args);
		}
		// SubscriptionData for new account
		else
		{
			$zSubscriptionData = $this->zDataExtractor->getHostingSubscriptionData($args);
		}
		$zSubscriptionData->Subscription->Status = 'Active';  // TO DO : does this actually work?

		// SubscriptionOptions
		$zSubscribeOptions = new Zuora_SubscribeOptions(true, false);  // GenerateInvoice, ProcessPayments

		$zPaymentMethod = null;
		if ( !empty($args['zPaymentMethodId']) )
		{
			// PaymentMethod - uses the newly created PaymentMethod ID from the HPM page
			$zPaymentMethod = new Zuora_PaymentMethod();
			$zPaymentMethod->Id = $args['zPaymentMethodId'];

			// Since we have a PaymentMethod, we can process payments
			$zSubscribeOptions = new Zuora_SubscribeOptions(true, true);  // GenerateInvoice, ProcessPayments
		}

		if ( isset($args['zAccountId']) )
		{
			// We don't need to create the contacts for pre-existing accounts
			$zBillToContact = null;
			$zSoldToContact = null;

			// Set AccountId on all objects, since we have a pre-existing account
			$zAccount->Id = $args['zAccountId'];
			$zSubscriptionData->AccountId = $zAccount->Id;
			$zSubscriptionData->InvoiceOwnerId = $zAccount->Id;
		}
		else
		{
			// Need to actually create contacts for new Accounts
			$zBillToContact = $this->zDataExtractor->getBillToContact($contact_info);
			$zSoldToContact = $this->zDataExtractor->getSoldToContact();

			// Remove AccountId-related member completely instead of just blanking it out to prevent errors on subscribe()
			$zAccount->unsetVar('Id');
			$zSubscriptionData->zSubscription->unsetVar('AccountId');
			$zSubscriptionData->zSubscription->unsetVar('InvoiceOwnerId');
			$zBillToContact->unsetVar('AccountId');
			$zSoldToContact->unsetVar('AccountId');

		}

		//$this->logDebug("...zAccount=". print_r($zAccount, true));
		//$this->logDebug("...zSubscriptionData=". print_r($zSubscriptionData, true));
		//$this->logDebug("...zBillToContact=". print_r($zBillToContact, true));
		//$this->logDebug("...zSoldToContact=". print_r($zSoldToContact, true));
		//$this->logDebug("...zPaymentMethod=". print_r($zPaymentMethod, true));

		try
		{
			$response = $this->zApi->subscribe($zAccount, $zSubscriptionData, $zBillToContact, $zPaymentMethod, $zSubscribeOptions, $zSoldToContact);
		}
		catch ( Exception $e )
		{
			$this->logError($e->getMessage());
		}

		//$this->logDebug("...response=". print_r($response, true));

		// Extract resultant Subscription ID
		if ( isset($response->result) )  $result = $response->result;
		if ( isset($response->results) ) $result = $response->results;
		if ( isset($result->SubscriptionId) ) $zSubscriptionId = $result->SubscriptionId;

		return $response;
	}

	/*
	* ------------------------------------------------------------------------------------------
	* Subscribe to Zuora Lavu ToGo packages
	* ------------------------------------------------------------------------------------------
	* @author Bhushan Daddikar (bhushan.daddikar.in@lavu.com)
	* @param array $subscriptionOptions make sure the array consist of dataname and package type
	* @since Method is made available subscribing to Lavutogo premium packages
	* @jira-ticket	LTG-39
	* @return object with subscription details
	* ------------------------------------------------------------------------------------------
	* @usage : Example-01
	* ------------------------------------------------------------------------------------------
	* $zuoraConnect = new ZuoraIntegration();
	* $zuoraConnect->subscribeToLavuToGo([
	* 	'dataname' 		=> '<location-dataname>',
	*	'package' 		=> '<package-name>', 			//possible values Annual, Monthly
	*	'effectiveDate'	=> '<date-in-Y-m-d>', 	//if not given will by default todays date
	*	'status' 		=> '<subscription-status>', 	Default is Active 
	* ]);
	* ------------------------------------------------------------------------------------------
	* @note : Please configure the required package details in depending on environment
	* ------------------------------------------------------------------------------------------
	* Hosting Configuration Files, follow the steps in the comment to configure
	* - inc/billing/ZuoraHostingProductRatePlanObjects.php
	* - inc/billing/ZuoraHostingProductRatePlanObjects.prod.php
	* - inc/billing/ZuoraHostingProductRatePlanObjects.sandbox.php
	* ------------------------------------------------------------------------------------------
	* @todo : 
	* ------------------------------------------------------------------------------------------
	* 1) Need to confirm for automatic payment if Payment Id is given. Zuora 
	*    gives an error something related to electronic funds configuration.
	* 2) Need to confirm if the invocie date separate from the lavu packages
	* 3) Need to check if the customer has account in zuora, if not need to gather the customer 
	*	 information and create a account accordingly.(has Nick Fuqua say its a least scenario customer miss Zuora account can be handled later)
	*/
	public function subscribeToLavuToGo($subscriptionOptions = []) {
		if(empty($subscriptionOptions['dataname']) || empty($subscriptionOptions['package'])) {
			$this->logError(__METHOD__ ."() missing required arg(s): ". json_encode($subscriptionOptions));
			return 'Missing required parameter';
		} else {
			$dataname = $subscriptionOptions['dataname'];
			$status = !empty($subscriptionOptions['status']) ? $subscriptionOptions['status'] : 'Active';
			$package = $subscriptionOptions['package'];
			$productType = 'LavuToGo';
			
			$lookingForProduct = $this->zDataExtractor->getPackageDetails($productType, $package);

			if (empty($lookingForProduct->ProductRatePlanId) || empty($lookingForProduct->ProductRatePlanChargeId)) {
				$error = "Product type $productType with package $package is not defined";
				$this->logError(__METHOD__ ."() $error");
				return $error;
			}

			$effectiveDate = !empty($subscriptionOptions['effectiveDate']) ? $subscriptionOptions['effectiveDate'] :  date('Y-m-d');
			$this->zDataExtractor->init($dataname);
			
			// read stored customer zuora account details from Lavu
			$zAccount = $this->zDataExtractor->getAccount();

			// read customer account details from zuora
			$zuoraAccount = $this->getAccount(['dataname' => $dataname]);

			// check if package is already active 
			$activeSubscriptions = $this->getLavuToGoActiveSubscription($dataname, $zuoraAccount);
			if(isset($activeSubscriptions['zSubscriptions']) && !empty($activeSubscriptions['zSubscriptions'])) {
				foreach ($activeSubscriptions['zSubscriptions'] as $activeSubscription) {
					if($activeSubscription->ProductRatePlanId === $lookingForProduct->ProductRatePlanId) {
						return $activeSubscription;
					}
				}
			}

			// cancel previous subscriptions if any
			$this->cancelLavuToGoSubscriptions([
				'dataname' => $dataname, 
				'cancel_reason' => 'Change of plan'
			]);

			// prepare data for subscription making sure zuora account id is add to the object
			$zAccount->Id = $zuoraAccount->Id;
			$zAccount->AccountNumber = $zuoraAccount->AccountNumber;
			$zAccount->DefaultPaymentMethodId = $zuoraAccount->DefaultPaymentMethodId;
			$zPaymentMethod = null;
			$zSubscribeOptions = new Zuora_SubscribeOptions(true, false);

			// if default payment is set process payments
			if (!empty($zAccount->DefaultPaymentMethodId)) {
				$zPaymentMethod = new Zuora_PaymentMethod();
				$zPaymentMethod->Id = $zAccount->DefaultPaymentMethodId;
				$zSubscribeOptions->ProcessPayments = true;
			}
			
			$zBillToContact = $this->zDataExtractor->getBillToContact(isset($subscriptionOptions['contact_info']) ? $subscriptionOptions['contact_info'] : []);
			$zSoldToContact = $this->zDataExtractor->getSoldToContact();

			$zSubscriptionData = $this->zDataExtractor->getLavuToGoSubscriptionData([
				'zAccountId' => $zAccount->Id,
				'event_date' => $effectiveDate,
				'billing_product_type' => $productType,
				'product_name' => $package,
				'status' => $status,
			]);

			return $this->zApi->subscribe($zAccount, $zSubscriptionData, $zBillToContact, $zPaymentMethod, $zSubscribeOptions, $zSoldToContact);
		}
		
	}

	/*
	* ------------------------------------------------------------------------------------------
	* Get active lavu ToGo subscription
	* ------------------------------------------------------------------------------------------
	* @author Bhushan Daddikar (bhushan.daddikar.in@lavu.com)
	* @param string $datanme
	* @since Method is made available fetching active lavutogo subscriptions
	* @jira-ticket	LTG-39
	* @return object with subscription details
	*/
	public function getLavuToGoActiveSubscription($datname, $zAccount = false) {
		$zuoraAccount = $zAccount !== false ? $zAccount : $this->getAccount(['dataname' => $datname]);
		if(empty($zuoraAccount->Id)) {
			$this->logError(__METHOD__ ."() => Account not found");
			return false;
		}

		$activateSubscription = $this->getActiveSubscriptions(['zAccountId' => $zuoraAccount->Id]);
		$whereClause = '';
		$LavuToGoSubscriptions = [];
		$allSubscriptions = [];

		if ($activateSubscription) {
			foreach ($activateSubscription as $key => $subscription) {
				$whereClause .= "SubscriptionId = '".$subscription->Id."'";
				$allSubscriptions[$subscription->Id] = $subscription;
				if(isset($activateSubscription[$key + 1])) {
					$whereClause .= ' OR ';
				}
			}
			if (!empty($whereClause)) {
				$productRatePlans = $this->getZuoraObjectViaApiQuery('RatePlan', [
					'zObjectName' => 'RatePlan',
					'select_fields' => 'ProductRatePlanId, SubscriptionId',
					'where_clause' => $whereClause,
					'requireArray' => true,
				]);

				if (!empty($productRatePlans)) {
					foreach ($productRatePlans as $productRatePlan) {
						if ($this->zDataExtractor->isLavuToGoPackage($productRatePlan->ProductRatePlanId)) {
								$allSubscriptions[$productRatePlan->SubscriptionId]->ProductRatePlanId = $productRatePlan->ProductRatePlanId;
								$allSubscriptions[$productRatePlan->SubscriptionId]->SubscriptionId = $productRatePlan->SubscriptionId;
								$LavuToGoSubscriptions['zSubscriptions'][] = $allSubscriptions[$productRatePlan->SubscriptionId];
						}
					}
				}
			}
		}

		return $LavuToGoSubscriptions;
	}

	/*
	* ------------------------------------------------------------------------------------------
	* Cancel active lavu ToGo subscription
	* ------------------------------------------------------------------------------------------
	* @author Bhushan Daddikar (bhushan.daddikar.in@lavu.com)
	* @param array $datanme the array must consist of dataname and cancellation reason
	* @since Method is made available for cancelling Lavu ToGo subscriptions
	* @jira-ticket	LTG-39
	* @return boolean
	*/
	public function cancelLavuToGoSubscriptions($params = []) {
		if (empty($params['dataname']) || empty($params['cancel_reason'])) {
			$this->logError(__METHOD__ ."() missing required param: ". $params['dataname']);
			return 'Missing dataname field';
		} else {
			$LavuToGoSubscriptions = $this->getLavuToGoActiveSubscription($params['dataname']);
			if ($LavuToGoSubscriptions === false) {
				return false;
			} else if (!empty($LavuToGoSubscriptions)) {
				$LavuToGoSubscriptions['cancel_reason'] = $params['cancel_reason'];
				$LavuToGoSubscriptions['cancel_date'] = isset($params['cancel_date']) ? $params['cancel_date'] : date('Y-m-d');
				$this->cancelSubscriptions($LavuToGoSubscriptions);
			}
		}
		return true;
	}

	/**
	 * Just keeping this method around for all of the existing code that uses it
	 */
	public function createHostingSubscriptionWithPaymentMethodId($args, $contact_info = null)
	{
		// Check required args
		if ( empty($args['dataname']) || empty($args['start_date']) || empty($args['hosting_amount']) || empty($args['zPaymentMethodId']) )
		{
			$this->logError(__METHOD__ ."() missing required arg(s): ". json_encode($args));
			return '';
		}

		return $this->createHostingSubscription($args, $contact_info);
	}

	public function createInvoicePayment($vars)
	{
		$zInvoicePayment = null;

		// Check required args
		if ( empty($vars['dataname']) || empty($vars['zInvoiceId']) || empty($vars['zPaymentId']) || empty($vars['amount']) )
		{
			$this->logError(__METHOD__ ."() missing required arg(s): ". json_encode($vars));
			return $zPayment;
		}

		// Zuora Data Extractor
		$this->zDataExtractor->init($vars['dataname']);

		// Payment
		$zInvoicePayment = $this->zDataExtractor->getInvoicePayment($vars);

		$response = $this->createZuoraObject('zInvoicePayment', 'create', array($zAccount));

		if ( $response->result->Success )
		{
			$zInvoicePayment->Id = $response->result->Id;
		}
		else
		{
			$zInvoicePayment = null;
		}

		return $zInvoicePayment;
	}

	public function createPayment($vars)
	{
		$zPayment = null;

		// Check required args
		if ( empty($vars['dataname']) || empty($vars['zAccountId']) || empty($vars['zPaymentMethodId']) || empty($vars['amount']) )
		{
			$this->logError(__METHOD__ ."() missing required arg(s): ". json_encode($vars));
			return $zPayment;
		}

		// Zuora Data Extractor
		$this->zDataExtractor->init($vars['dataname']);

		// Payment
		$zPayment = $this->zDataExtractor->getPayment($vars);

		$response = $this->createZuoraObject('zPayment', 'create', array($zPayment));

		if ( $response->result->Success )
		{
			$zPayment->Id = $response->result->Id;
		}
		else
		{
			$zPayment = null;
		}

		return $zPayment;
	}

	public function subscribe($zAccount, $zSubscribeData, $zBillToContact, $zPaymentMethod, $zSubscribeOptions, $zSoldToContact)
	{
		$this->initApi();

		try
		{
			$response = $this->zApi->subscribe($zAccount, $zSubscribeData, $zBillToContact, $zPaymentMethod, $zSubscribeOptions, $zSoldToContact);
		}
		catch ( Exception $e )
		{
			$this->logError($e->getMessage());
		}

		return $response;
	}

	public function cancelAccount($vars)
	{
		$cancelled = false;

		if ( !empty($vars['zAccountId']) )
		{
			// Cancel active Subscriptions
			$subscriptions_cancelled  = $this->cancelActiveSubscriptions($vars);
			//$this->logDebug("subscriptions_cancelled={$subscriptions_cancelled}");  //debug

			// Delete draft Subscriptions
			$drafts_deleted  = $this->deleteDraftSubscriptions($vars);
			//$this->logDebug("drafts_deleted={$drafts_deleted}");  //debug

			// Cancel active PaymentMethods
			$paymentmethods_cancelled = $this->cancelActivePaymentMethods($vars);
			//$this->logDebug("paymentmethods_cancelled={$paymentmethods_cancelled}");  //debug

			// Only cancel Account if the others are cancelled because otherwise you have to re-activate to cancel
			if ( $subscriptions_cancelled && $drafts_deleted && $paymentmethods_cancelled )
			{
				// Cancel Account, which is done simply by setting Account->Status = Canceled once everything else has been cancelled.
				$account_cancelled = $this->updateAccount(array('zAccountId' => $vars['zAccountId'], 'Status' => 'Canceled'));

				$cancelled = ($account_cancelled && $subscriptions_cancelled && $paymentmethods_cancelled);
				//$this->logDebug("cancelled={$cancelled} account_cancelled={$account_cancelled} subscriptions_cancelled={$subscriptions_cancelled} paymentmethods_cancelled={$paymentmethods_cancelled}");  //debug
			}
		}
		else
		{
			$this->logError(__METHOD__ ."() called without required argument(s): vars=". json_encode($vars));
		}

		return $cancelled;
	}

	public function cancelActiveSubscriptions($vars)
	{
		$success = true;

		$zSubscriptions = $this->getActiveSubscriptions($vars);

		if ( $zSubscriptions !== null )
		{
			$vars['zSubscriptions'] = $zSubscriptions;
			$success = $this->cancelSubscriptions($vars);
		}

		return $success;
	}

	public function cancelAllSubscriptions($vars)
	{
		$success = true;

		$zSubscriptions = $this->getAllSubscriptions($vars);

		if ( $zSubscriptions !== null )
		{
			$vars['zSubscriptions'] = $zSubscriptions;
			$success = $this->cancelSubscriptions($vars);
		}

		return $success;
	}

	public function cancelActivePaymentMethods($vars)
	{
		$success = true;

		$zPaymentMethods = $this->getActivePaymentMethods($vars);
		if ( $zPaymentMethods !== null )
		{
			$vars['zPaymentMethods'] = $zPaymentMethods;
			$success = $this->cancelPaymentMethods($vars);
		}

		return $success;
	}

	public function deleteDraftSubscriptions($vars)
	{
		$success = true;

		$zSubscriptions = $this->getDraftSubscriptions($vars);

		if ( $zSubscriptions !== null )
		{
			$vars['zSubscriptions'] = $zSubscriptions;
			$success = $this->deleteSubscriptions($vars);
		}

		return $success;
	}

	public function getAccount($vars)
	{
		$zAccount = null;

		// Prioritized fetching account with zAccountId over dataname, since multiple datanames is possible
		if ( !empty($vars['zAccountId']) )
		{
			$vars['where_clause'] = $this->getWhereClauseFromArray( array('Id' => $vars['zAccountId']) );
			$zAccount = $this->getZuoraObjectViaApiQuery('zAccount', $vars);
		}
		else if ( !empty($vars['dataname']) )
		{
			$zAccount = $this->getZuoraObjectViaApiQuery('zAccount', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zAccount;
	}

	public function getSoldToContact($vars)
	{
		$zSoldToContact = null;

		if ( !empty($vars['zAccountId']) && !empty($vars['Id']) )
		{
			$vars['where_clause'] = $this->getWhereClauseFromArray( array('AccountId' => $vars['zAccountId'], 'Id' => $vars['Id']));
			$zSoldToContact = $this->getZuoraObjectViaApiQuery('zSoldToContact', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zSoldToContact;
	}

	public function getInvoice($vars)
	{
		$zInvoice = null;

		if ( !empty($vars['zAccountId']) && !empty($vars['Id']) )
		{
			$zInvoice = $this->getZuoraObjectViaApiQuery('zInvoice', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .' called without required argument(s): vars='. json_encode($vars));
		}

		return $zInvoice;
	}

	public function getInvoices($vars)
	{
		$zInvoices = array();

		if ( !empty($vars['zAccountId']) )
		{
			$zInvoices = $this->getZuoraObjectViaApiQuery('zInvoices', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zInvoices;
	}

	public function getInvoicesNotFlaggedHidden($vars)
	{
		$zInvoices = array();

		if ( !empty($vars['zAccountId']) )
		{
			$zInvoices = $this->getZuoraObjectViaApiQuery('zInvoicesNotFlaggedHidden', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zInvoices;
	}

	public function getUnpaidInvoices($vars)
	{
		$zInvoices = array();

		if ( !empty($vars['zAccountId']) )
		{
			$zInvoices = $this->getZuoraObjectViaApiQuery('zInvoicesUnpaid', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zInvoices;
	}

	/**
	* Same as getInvoice() except uses different obj_type which results in retrieving the zInvoice.Body value, which contains the Base64 encoded PDF contents
	*/
	public function getInvoicePdf($vars)
	{
		if ( !empty($vars['zAccountId']) && !empty($vars['zInvoiceId']) )
		{
			$zInvoice = $this->getZuoraObjectViaApiQuery('zInvoicePdf', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		// TO DO : save PDF contents in $zInvoice->Body to file system

		return $zInvoice->Body;
	}

	public function getInvoicePaymentsForInvoices($vars)
	{
		$zInvoices = array();

		// Make custom where_clause comprised of OR'ing the InvoiceIds together
		$where_clause = '';
		foreach ( $vars['zInvoices'] as $zInvoice )
		{
			$where_clause .= empty($where_clause) ? "InvoiceId = '". $this->escapeZoql($zInvoice->Id) ."'" : " OR InvoiceId = '". $this->escapeZoql($zInvoice->Id) ."'";
		}
		$vars['where_clause'] = $where_clause;

		if ( isset($vars['zAccountId']) )
		{
			$zInvoices = $this->getZuoraObjectViaApiQuery('zInvoicePaymentsForInvoiceIds', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zInvoices;
	}

	public function getInvoicePaymentsForInvoiceIds($vars)
	{
		$zInvoices = array();

		// Make custom where_clause comprised of OR'ing the InvoiceIds together
		$where_clause = '';
		foreach ( $vars['zInvoiceIds'] as $zInvoiceId )
		{
			$where_clause .= empty($where_clause) ? "{$zInvoiceId} = '". $this->escapeZoql($zInvoiceId) ."'" : " OR {$zInvoiceId} = '". $this->escapeZoql($zInvoiceId) ."'";
		}
		$vars['where_clause'] = "({$where_clause})";

		if ( isset($vars['zAccountId']) )
		{
			$zInvoices = $this->getZuoraObjectViaApiQuery('zInvoicePaymentsForInvoiceIds', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zInvoices;
	}

	public function getPayments($vars)
	{
		$zPayments = array();

		if ( isset($vars['zAccountId']) )
		{
			$zPayments = $this->getZuoraObjectViaApiQuery('zPayments', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zPayments;
	}

	public function getPaymentsWithInvoicePayments($vars)
	{
		$zPaymentsWithInvoicePayments = array();

		if ( isset($vars['zAccountId']) )
		{
			$zPayments = $this->getZuoraObjectViaApiQuery('zPayments', $vars);
			foreach ( $zPayments as $zPayment )
			{
				$vars['zPaymentId'] = $zPayment->Id;
				$zInvoicePayments = $this->getZuoraObjectViaApiQuery('zInvoicePaymentsForPaymentId', $vars);
				$zPayment->InvoicePaymentData = $zInvoicePayments;
				$zPaymentsWithInvoicePayments[] = $zPayment;
			}
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zPaymentsWithInvoicePayments;
	}

	public function getPaymentsSinceDate($vars)
	{
		$zPayments = array();

		if ( !empty($vars['zAccountId']) && !empty($vars['EffectiveDate']) )
		{
			$zPayments = $this->getZuoraObjectViaApiQuery('zPaymentsSinceDate', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zPayments;
	}

	public function getPaymentMethodById($vars)
	{
		$zPaymentMethod = null;

		if  ( !empty($vars['zPaymentMethodId']) )
		{
			$zPaymentMethod = $this->getZuoraObjectViaApiQuery('zPaymentMethodById', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zPaymentMethod;
	}

	public function getActivePaymentMethods($vars)
	{
		$zPaymentMethods = array();

		if ( !empty($vars['zAccountId']) )
		{
			$vars['PaymentMethodStatus'] = 'Active';
			$zPaymentMethods = $this->getZuoraObjectViaApiQuery('zPaymentMethodByPaymentMethodStatus', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zPaymentMethods;
	}

	public function getRatePlan($vars)
	{
		$zRatePlan = null;

		if ( !empty($vars['zSubscriptionId']) )
		{
			$zRatePlan = $this->getZuoraObjectViaApiQuery('zRatePlan', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zRatePlan;
	}

	/**
	 * Similar to getRatePlan() but always returns an array
	 */
	public function getRatePlans($vars)
	{
		$zRatePlans = null;

		if ( !empty($vars['zSubscriptionId']) )
		{
			$zRatePlans = $this->getZuoraObjectViaApiQuery('zRatePlans', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zRatePlans;
	}

	public function getRatePlanCharge($vars)
	{
		$zRatePlanCharge = null;

		if ( !empty($vars['zRatePlanId']) && !empty($vars['Name']) )
		{
			$zRatePlanCharge = $this->getZuoraObjectViaApiQuery('zRatePlanCharge', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zRatePlanCharge;
	}

	public function getRatePlanChargeBySubscriptionId($vars)
	{
		$zRatePlanCharge = null;

		if ( !empty($vars['zSubscriptionId']) )
		{
			$zRatePlanCharge = $this->getZuoraObjectViaApiQuery('zRatePlanChargeBySubscriptionId', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zRatePlanCharge;
	}

	public function getRatePlanChargeById($vars)
	{
		$zRatePlanCharge = null;

		if ( !empty($vars['zRatePlanChargeId']) )
		{
			$zRatePlanCharge = $this->getZuoraObjectViaApiQuery('zRatePlanChargeById', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zRatePlanCharge;
	}

	/**
	 * Similar to getRatePlanChargeById() but takes an array of zRatePlanChargeIds and returns an array of zRatePlanCharges
	 */
	public function getRatePlanChargesByIds($vars)
	{
		$zRatePlanCharges = null;

		if ( !empty($vars['zRatePlanChargeIds']) && !empty($vars['where_clause']) )
		{
			$zRatePlanCharges = $this->getZuoraObjectViaApiQuery('zRatePlanChargesByIds', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zRatePlanCharges;
	}

	/**
	 * LP-284 -- Created version of getRatePlanChargeById() with Price in the SELECT clause that should only be used
	 * on non-tiered RatePlanCharges, since it the Zoql query won't return anything otherwise.
	 */
	public function getRatePlanChargeByIdWithPrice($vars)
	{
		$zRatePlanCharge = null;

		if ( !empty($vars['zRatePlanChargeId']) )
		{
			$zRatePlanCharge = $this->getZuoraObjectViaApiQuery('zRatePlanChargeByIdWithPrice', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zRatePlanCharge;
	}

	/**
	 * LP-284 -- Created method for getting RatePlanChargeTiers for tiered RatePlans, since these are the entities that have the Price
	 */
	public function getRatePlanChargeTiers($vars)
	{
		$zRatePlanChargeTiers = null;

		if ( !empty($vars['zRatePlanChargeId']) )
		{
			$zRatePlanChargeTiers = $this->getZuoraObjectViaApiQuery('zRatePlanChargeTier', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zRatePlanChargeTiers;
	}

	public function getSubscription($vars)
	{
		$zSubscription = null;

		if ( isset($vars['zAccountId']) && isset($vars['Id']) )
		{
			$zSubscription = $this->getZuoraObjectViaApiQuery('zSubscription', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zSubscription;
	}

	public function getSubscriptionByAccountId($vars)
	{
		$zSubscription = null;

		if ( isset($vars['zAccountId']) )
		{
			$zSubscription = $this->getZuoraObjectViaApiQuery('zSubscriptionByAccountId', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zSubscription;
	}

	public function getSubscriptions($vars)
	{
		$zSubscriptions = array();

		if ( isset($vars['zAccountId']) )
		{
			$zSubscriptions = $this->getZuoraObjectViaApiQuery('zSubscriptions', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zSubscriptions;
	}

	public function getActiveSubscriptions($vars)
	{
		$zSubscriptions = array();

		if ( !empty($vars['zAccountId']) )
		{
			$vars['Status'] = 'Active';
			$zSubscriptions = $this->getZuoraObjectViaApiQuery('zSubscriptionsByStatus', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		//$this->logDebug("active zSubscriptions=". print_r($zSubscriptions, true));  //debug

		return $zSubscriptions;
	}

	public function getDraftSubscriptions($vars)
	{
		$zSubscriptions = array();

		if ( !empty($vars['zAccountId']) )
		{
			$vars['Status'] = 'Active';
			$zSubscriptions = $this->getZuoraObjectViaApiQuery('zSubscriptionsDrafts', $vars);
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		//$this->logDebug("active zSubscriptions=". print_r($zSubscriptions, true));  //debug

		return $zSubscriptions;
	}

	public function cancelSubscriptions($vars)
	{
		$cancelled = true;
		if ( !empty($vars['zSubscriptions']) && !empty($vars['cancel_date']) && !empty($vars['cancel_reason']) )
		{
			$zAmendRequests = array();
			foreach ( $vars['zSubscriptions'] as $zSubscription )
			{
				$zAmendments = new Zuora_Amendments();
				$zAmendments->SubscriptionId = $zSubscription->Id;
				$zAmendments->Name = substr($vars['cancel_reason'], 0, 100);
				$zAmendments->Type = 'Cancellation';
				$zAmendments->Status = 'Cancelled';
				$zAmendments->EffectiveDate = $vars['cancel_date'];
				$zAmendments->ContractEffectiveDate = $vars['cancel_date'];
				$zAmendments->ServiceActivationDate = $vars['cancel_date'];
				$zAmendments->CustomerAcceptanceDate = $vars['cancel_date'];

				$zAmendOptions = new Zuora_AmendOptions();
				$zAmendOptions->GenerateInvoice = false;
				$zAmendOptions->ProcessPayments = false;

				$zPreviewOptions = new Zuora_PreviewOptions();
				$zPreviewOptions->EnablePreviewMode = false;

				$zAmendRequest = new Zuora_AmendRequest();
				$zAmendRequest->Amendments = $zAmendments;
				$zAmendRequest->AmendOptions = $zAmendOptions;
				$zAmendRequest->PreviewOptions = $zPreviewOptions;

				try
				{
					$response = $this->zApi->amend(array($zAmendRequest));  // returns an AmendResult object
				}
				catch ( Exception $e )
				{
					error_log(__FILE__ .' '. $e->getMessage());
					return false;
				}

				//$this->logDebug("response=". print_r($response, true));

				if ( isset($response->results->Success) && $response->results->Success )  // NOTE: Notice the plural s on results!
				{
					$cancelled = true AND $cancelled;
					$zAmendResult = $response;  // TO DO : extract Amend obj out of AmendResult
				}
				else
				{
					$cancelled = false;
					$this->logError($response->results->Errors);
				}

			}
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $cancelled;
	}

	public function deleteSubscriptions($vars)
	{
		$cancelled = false;

		if ( !empty($vars['zSubscriptions']) )
		{
			$zPaymentMethods = array();
			foreach ( $vars['zSubscriptions'] as $zSubscription )
			{
				$zSubscriptionIds[] = $zSubscription->Id;
			}

			try
			{
				$response = $this->zApi->delete('Subscription', $zSubscriptionIds);
			}
			catch ( Exception $e )
			{
				$this->logError($e->getMessage());
				return false;
			}

			// Process results

			//$this->logDebug("...response=". print_r($response, true));

			if ( isset($response->result) && isset($response->result->Success) )
			{
				$cancelled = true;
			}
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $cancelled;
	}


	public function cancelPaymentMethods($vars)
	{
		$cancelled = false;

		if ( !empty($vars['zPaymentMethods']) )
		{
			$zPaymentMethods = array();
			foreach ( $vars['zPaymentMethods'] as $zPaymentMethod )
			{
				$zuPaymentMethod = new Zuora_PaymentMethod();
				$zuPaymentMethod->Id = $zPaymentMethod->Id;
				$zuPaymentMethod->PaymentMethodStatus = 'Closed';
				$zuPaymentMethods[] = $zuPaymentMethod;
			}

			$response = $this->createZuoraObject('zPaymentMethod', 'update', $zuPaymentMethods);
			$cancelled = true;  // TO DO : loop throough responses and AND results of each one together
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $cancelled;
	}

	public function cancelPaymentMethod($vars)
	{
		$cancelled = false;

		if ( !empty($vars['zPaymentMethodId']) )
		{
			$zPaymentMethod = new Zuora_PaymentMethod();
			$zPaymentMethod->Id = $vars['zPaymentMethodId'];
			if ($vars['zAccountId']) $zPaymentMethod->AccountId = $vars['zAccountId'];
			$zPaymentMethod->PaymentMethodStatus = 'Closed';

			$zPaymentMethods = array();
			$zPaymentMethods[] = $zPaymentMethod;

			$response = $this->createZuoraObject('zPaymentMethod', 'update', $zPaymentMethods);
			$cancelled = $response->result->Success;
		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $cancelled;
	}

	public function importAccount($vars)
	{
		if ( empty($vars['dataname']) )
		{
			error_log(__METHOD__ ."() missing required vars: ". json_encode($vars));
			return false;
		}

		// Zuora Data Extractor
		$this->zDataExtractor->init($vars['dataname']);

		$already_existing_account = false;

		// This will look account up by dataname
		$zAccount = $this->getAccount($vars);

			// No Account exists in Zuora, so create one
		if ( $zAccount === null )
		{
			// Create Account with hosting Subscription in Zuora
			$response = $this->createHostingSubscription($vars, $this->zDataExtractor->signup, true);
			//$this->logDebug("createHostingSubscription() response=". print_r($response, true));  //debug

			if ( isset($response->result) && isset($response->result->AccountId) )
			{
				$vars['zAccountId'] = $response->result->AccountId;
				$vars['zSubscriptionId'] = $response->result->SubscriptionId;

				// Since we just created a Subscription, we'll need to extract the RatePlanChargeId for the Hosting product later in the code so we populate zSubscriptions to prep for that
				$zSubscription = new Zuora_Subscription();
				$zSubscription->Id = $vars['zSubscriptionId'];
				$zSubscriptions[] = $zSubscription;

				// Enable AutoPay, but only if a PaymentMethod has been specified
				if ( !empty($vars['zPaymentMethodId']) )
				{
					$update_ok = $this->updateAccount(array('zAccountId' => $vars['zAccountId'], 'AutoPay' => true));
				}

			}
			else
			{
				// Display error message
				$this->logError("response=". print_r($response, true));
			}
		}
		else
		{
			$already_existing_account = true;
			$vars['zAccountId'] = $zAccount->Id;

			// If we have a Zuora Account but don't have the zRatePlanId saved in the signups row, then we'll need to populate zSubscriptions to prep for that
			if ( empty($this->zDataExtractor->signup['zRatePlanChargeId']) )
			{
				$zSubscriptions = $this->getActiveSubscriptions($vars);
			}
			// We already have a zRatePlanChargeId recorded in the signups row, so save it for displaying at the end
			else
			{
				$vars['zRatePlanChargeId'] = $this->zDataExtractor->signup['zRatePlanChargeId'];
			}
		}

		// If we have elements in zSubscriptions, then we need to check those Subscription IDs until we find the Hosting RatePlanCharge so we can report its ID
		foreach ( $zSubscriptions as $zSubscription )
		{
			$zRatePlan = $this->getRatePlan(array('zSubscriptionId' => $zSubscription->Id));
			if ( $zRatePlan !== null )
			{
				$zRatePlanCharge = $this->getRatePlanCharge(array('zRatePlanId' => $zRatePlan->Id, 'Name' => 'Hosting'));

				// LP-2559 -- Added RatePlanCharge names for new pricing plans
				if ( $zRatePlanCharge === null )
				{
					$zRatePlanCharge = $this->getRatePlanCharge(array('zRatePlanId' => $zRatePlan->Id, 'Name' => 'Lavu - Monthly Hosting'));
					//$this->logDebug("...had to try 'Lavu - Monthly Hosting' zRatePlanCharge=". print_r($zRatePlanCharge, true));  //debug
				}
				if ( $zRatePlanCharge === null )
				{
					$zRatePlanCharge = $this->getRatePlanCharge(array('zRatePlanId' => $zRatePlan->Id, 'Name' => 'Lavu - Annual Hosting'));
					//$this->logDebug("...had to try 'Lavu - Annual Hosting' zRatePlanCharge=". print_r($zRatePlanCharge, true));
				}
				if ( $zRatePlanCharge === null )
				{
					$zRatePlanCharge = $this->getRatePlanCharge(array('zRatePlanId' => $zRatePlan->Id, 'Name' => 'Monthly Recurring - POS Hosting - Per Terminal'));
					//$this->logDebug("...had to try 'Monthly - POS Hosting' zRatePlanCharge=". print_r($zRatePlanCharge, true));  //debug
				}
				if ( $zRatePlanCharge === null )
				{
					$zRatePlanCharge = $this->getRatePlanCharge(array('zRatePlanId' => $zRatePlan->Id, 'Name' => 'Yearly Recurring - POS Hosting - Per Terminal'));
					//$this->logDebug("...had to try 'Yearly Recurring - POS Hosting - Per TerminalEdit' zRatePlanCharge=". print_r($zRatePlanCharge, true));
				}
				if ( $zRatePlanCharge !== null )
				{
					$vars['zRatePlanChargeId'] = $zRatePlanCharge->Id;
					$vars['zSubscriptionId'] = $zSubscription->Id;
					break;
				}
			}
		}

		return array($vars['zAccountId'], $vars['zRatePlanChargeId'], $already_existing_account);
	}

	/**
	 * Equivalent of performing a Payment Run for that account; initially created for LP-722
	 *
	 * @param $vars Array with required keys (dataname, zAccountId) and optional keys (zPaymentMethodId)
	 *
	 */
	public function payUnpaidInvoices($vars)
	{
		$zPayments = array();

		if ( empty($vars['dataname']) && empty($vars['zAccountId']) )
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
			return $zPayments;
		}

		$zAccount = $this->getAccount($vars);

		if ( $zAccount !== null && !empty($zAccount->Balance) )
		{
			// Allow passed-in zPaymentMethodId to override default PaymentMethod
			if ( empty($vars['zPaymentMethodId']) )
			{
				$vars['zPaymentMethodId'] = $zAccount->DefaultPaymentMethodId;
			}

			// We currently have to create one Payment per Invoice since InvoicePayments didn't work - which is non-ideal
			$zUnpaidInvoices = $this->getUnpaidInvoices($vars);
			foreach ( $zUnpaidInvoices as $zUnpaidInvoice )
			{
				// Create Payment
				$vars['zInvoiceId'] = $zUnpaidInvoice->Id;
				$vars['amount'] = $zUnpaidInvoice->Balance;
				$zPayments[] = $this->createPayment($vars);
			}
		}

		return $zPayments;
	}

	/**
	 * LP-382 -- Created method to be called by sync button on Lavu admin billing page
	 */
	public function syncAccount($vars)
	{
		$synced = false;

		if ( !empty($vars['dataname']) )
		{
			$zAccount = $this->getAccount($vars);

			if ( $zAccount !== null )
			{
				// Extract the Account data as if we're going to import it for the first time
				$this->zDataExtractor->init($vars['dataname']);
				$zAccountExtracted = $this->zDataExtractor->getAccount($vars);

				// Put the extracted data into $vars without overriding any pre-existing array elements
				$vars += $zAccountExtracted->getData();

				// This will override any passed-in zAccountId values in vars, but that seems better since $zAccount->Id came from querying Zuora
				$vars['zAccountId'] = $zAccount->Id;

				// Don't override Zuora-managed fields that could've been modified in the Zuora UI since the account was initially imported
				unset($vars['AccountNumber']);
				unset($vars['AllowInvoiceEdit']);
				unset($vars['AutoPay']);
				unset($vars['Batch']);
				unset($vars['BillCycleDay']);
				unset($vars['CrmId']);
				unset($vars['Currency']);
				unset($vars['Name']);
				unset($vars['PaymentTerm']);
				unset($vars['Status']);

				error_log("DEBUG: vars=". print_r($vars, true));  //debug

				$synced = $this->updateAccount($vars);

				// TO DO : update due_info using zAccount->Balance
			}
		}

		return $synced;
	}

	public function updateAccount($vars)
	{
		$updated = false;

		if ( !empty($vars['zAccountId']) )
		{
			$zAccount = new Zuora_Account();
			$zAccount->Id = $vars['zAccountId'];

			// Copy every single key value pair from $vars into the Zuora Account object for updating
			unset($vars['zAccountId']);
			foreach ( $vars as $field => $value )
			{
				$zAccount->{$field} = $value;
			}

			//$this->logDebug("zAccount=". print_r($zAccount, true));

			$response = $this->createZuoraObject('zAccount', 'update', array($zAccount));
			//$this->logDebug(__METHOD__ ."() API response=". print_r($response, true));

			$updated = $response->result->Success;

			if ( !$updated )
			{
				$this->logError(__METHOD__ .'() got error: '. print_r($response->result->Errors, true));
			}

		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $updated;
	}

	public function updateCreditCard()
	{
	}

	public function updatePayment($vars)
	{
		$updated = false;

		if ( !empty($vars['zPaymentId']) )
		{
			$zPayment = new Zuora_Payment();
			$zPayment->Id = $vars['zPaymentId'];

			// Copy every single key value pair from $vars into the Zuora Account object for updating
			unset($vars['zPaymentId']);
			foreach ( $vars as $field => $value )
			{
				$zPayment->{$field} = $value;
			}

			//$this->logDebug("zPayment=". print_r($zPayment, true));

			$response = $this->createZuoraObject('zPayment', 'update', array($zPayment));
			//$this->logDebug(__METHOD__ ."() API response=". print_r($response, true));

			$updated = $response->result->Success;

			if ( !$updated )
			{
				$this->logError(__METHOD__ .'() got error: '. print_r($response->result->Errors, true));
			}

		}
		else
		{
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $updated;
	}

	public function getZuoraObjectViaApiQuery($obj_type, $vars = array())
	{
		//$this->logDebug("In ". __METHOD__ ."() for obj_type={$obj_type} vars=". json_encode($vars) ." where_clause_args=". json_encode($where_clause_args));

		$zObj = null;

		$return_single_row  = false;
		$return_array       = isset($vars['requireArray']) ? $vars['requireArray']: false;
		$die_on_zero_rows   = false;
		$die_on_api_errors  = false;

		switch ( $obj_type )
		{
			case 'zAccount':
				$zObjectName = 'Account';
				$select_fields = 'Id, Name, AccountNumber, AllowInvoiceEdit, AutoPay, Balance, BillCycleDay, BillToId, CrmId, Currency, Dataname__c, DefaultPaymentMethodId, PaymentTerm, SoldToId, Status';
				$where_clause_args = array('Dataname__c' => $vars['dataname']);
				break;

			case 'zBillToContact':
				$zObjectName = 'Contact';
				$select_fields = 'Id, FirstName, LastName, Address1, Address2, City, State, PostalCode, Country, WorkPhone, WorkEmail, CreatedDate, AccountId';
				$where_clause_args = array('FirstName' => $vars['firstname'], 'LastName' => $vars['LastName'], 'AccountId' => $vars['zAccountId']);
				break;

			case 'zSoldToContact':
				$zObjectName = 'Contact';
				$select_fields = 'Id, FirstName, LastName, Address1, Address2, City, State, PostalCode, Country, WorkPhone, WorkEmail, CreatedDate, AccountId';
				$where_clause_args = null;
				break;

			case 'zLicenseSubscription':
			case 'zUpgradeLicenseSubscription':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'TermStartDate' => $vars['date'], 'TermType' => 'TERMED');
				break;

			case 'zHostingSubscription':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'TermStartDate' => $vars['date'], 'TermType' => 'EVERGREEN');
				break;

			case 'zInvoice':
				$zObjectName = 'Invoice';
				$select_fields = 'Id, AccountId, InvoiceDate, TargetDate, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'Id' => $vars['Id']);
				break;

			case 'zInvoices':
				$zObjectName = 'Invoice';
				$select_fields = 'Id, AccountId, Amount, Balance, DueDate, InvoiceDate, TargetDate, InvoiceNumber, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'Status' => 'Posted');
				$return_single_row = false;
				$return_array = true;
				break;

			// LP-345 -- Added support for hiding Reseller invoices from CP billing page
			case 'zInvoicesNotFlaggedHidden':
				$zObjectName = 'Invoice';
				$select_fields = 'Id, AccountId, Amount, Balance, DueDate, InvoiceDate, TargetDate, InvoiceNumber, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c, HideInLavuBilling__c';
				// Zuora doesn't support parenthesis in Zoql so we have to rely on AND binding tighter than OR, per the documentation
				$where_clause = "AccountId = '{$vars['zAccountId']}' AND Status = 'Posted' AND HideInLavuBilling__c = null OR AccountId = '{$vars['zAccountId']}' AND Status = 'Posted' AND HideInLavuBilling__c = ''";
				$return_single_row = false;
				$return_array = true;
				break;

			// LP-328 -- Added support for button that calculates due_info on demand
			case 'zInvoicesUnpaid':
				$zObjectName = 'Invoice';
				$select_fields = 'Id, AccountId, Amount, Balance, DueDate, InvoiceDate, TargetDate, InvoiceNumber, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c';

				// Zuora doesn't support parenthesis in Zoql so we have to rely on AND binding tighter than OR, per the documentation
				$where_clause = "AccountId = '{$vars['zAccountId']}' AND Status = 'Posted' AND Balance > 0 AND HideInLavuBilling__c = null OR AccountId = '{$vars['zAccountId']}' AND Status = 'Posted' AND Balance > 0 AND HideInLavuBilling__c = ''";
				$return_single_row = false;
				$return_array = true;
				break;

			case 'zInvoicePdf':
				$zObjectName = 'Invoice';
				$select_fields = 'Id, AccountId, InvoiceDate, TargetDate, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c, Body';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'Id' => $vars['zInvoiceId']);
				break;

			case 'zInvoiceUpdate':
				$zObjectName = 'Invoice';
				$select_fields = 'Id, AccountId, InvoiceDate, TargetDate, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'LegacyID__c' => $vars['invoiceid'], 'Status' => 'Posted');
				break;

			case 'zInvoiceItem':
				$zObjectName = 'InvoiceItem';
				$select_fields = 'Id, ChargeAmount, ChargeType, InvoiceId';
				// Needs custom where_clause because it uses invoice-specific params (ChargeType, LegacyInvoiceID__c)
				break;

			case 'zInvoiceItemAdjustment':
				$zObjectName = 'InvoiceItemAdjustment';
				$select_fields = 'Id, AccountId, AdjustmentDate, Amount, Comment, InvoiceId, InvoiceItemName, InvoiceNumber, LegacyInvoiceID__c, SourceId, SourceType, Type';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'InvoiceId' => $vars['zInvoiceId']);  // hopefully it's not bad I removed LegacyInvoiceID__c from where clause
				break;

			case 'zInvoicePayments':
				$zObjectName = 'InvoicePayment';
				$select_fields = 'Id, Amount, CreatedDate, InvoiceId, PaymentId, RefundAmount';
				$where_clause_args = array('PaymentId' => $vars['zPaymentId']);
				$return_single_row = false;
				$return_array = true;
				break;

			case 'zInvoicePaymentsForInvoiceIds':
				$zObjectName = 'InvoicePayment';
				$select_fields = 'Id, Amount, CreatedDate, InvoiceId, PaymentId, RefundAmount';
				$where_clause_args = null;  // Requires custom where_clause for the OR's
				$return_single_row = false;
				$return_array = true;
				break;

			case 'zInvoicePaymentsForPaymentId':
				$zObjectName = 'InvoicePayment';
				$select_fields = 'Id, Amount, CreatedDate, InvoiceId, PaymentId, RefundAmount';
				$where_clause_args = array('PaymentId' => $vars['zPaymentId']);
				$return_single_row = false;
				$return_array = true;
				break;

			case 'zPayment':
				$zObjectName = 'Payment';
				$select_fields = 'Id, AccountId, Amount';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'LegacyID__c' => $vars['paymentid']);
				break;

			case 'zPayments':
				$zObjectName = 'Payment';
				$select_fields = 'Id, AccountId, Amount, CreatedDate, EffectiveDate, PaymentNumber, ReferenceId';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'Status' => 'Processed');
				$return_single_row = false;
				$return_array = true;
				break;

			case 'zPaymentsSinceDate':
				$zObjectName = 'Payment';
				$select_fields = 'Id, AccountId, Amount, CreatedDate, EffectiveDate, PaymentNumber, ReferenceId';
				$where_clause = "AccountId = '{$vars['zAccountId']}' AND Status = 'Processed' AND EffectiveDate > '{$vars['EffectiveDate']}'";
				$return_single_row = false;
				$return_array = true;
				break;

			case 'zPaymentMethod':
				$zObjectName = 'PaymentMethod';
				$select_fields = 'Id, AccountId, Type, UseDefaultRetryRule';
				$where_clause_args = array('AccountId' => $vars['zAccountId']);
				break;

			case 'zPaymentMethodById':
				$zObjectName = 'PaymentMethod';
				$select_fields = 'Id, AccountId, Type, CreatedDate, LastTransactionDateTime, UseDefaultRetryRule, CreditCardHolderName, CreditCardMaskNumber, CreditCardType, CreditCardExpirationMonth, CreditCardExpirationYear, CreditCardAddress1, CreditCardAddress2, CreditCardCity, CreditCardState, CreditCardPostalCode, CreditCardCountry, PaymentMethodStatus';
				$where_clause_args = array('Id' => $vars['zPaymentMethodId']);
				break;

			case 'zPaymentMethodByPaymentMethodStatus':
				$zObjectName = 'PaymentMethod';
				$select_fields = 'Id, AccountId, Type, CreatedDate, LastTransactionDateTime, UseDefaultRetryRule, CreditCardHolderName, CreditCardMaskNumber, CreditCardType, CreditCardExpirationMonth, CreditCardExpirationYear, CreditCardAddress1, CreditCardAddress2, CreditCardCity, CreditCardState, CreditCardPostalCode, CreditCardCountry, PaymentMethodStatus';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'PaymentMethodStatus' => $vars['PaymentMethodStatus']);
				$return_array = true;
				break;

			case 'zRatePlan':
				$zObjectName = 'RatePlan';
				$select_fields = 'Id, CreatedDate, Name, ProductRatePlanId, SubscriptionId';
				$where_clause_args = array('SubscriptionId' => $vars['zSubscriptionId']);
				break;

			case 'zRatePlans':
				$zObjectName = 'RatePlan';
				$select_fields = 'Id, CreatedDate, Name, ProductRatePlanId, SubscriptionId';
				$where_clause_args = array('SubscriptionId' => $vars['zSubscriptionId']);
				$return_array = true;
				break;

			case 'zRatePlanCharge':
				$zObjectName = 'RatePlanCharge';
				$select_fields = 'Id, CreatedDate, EffectiveStartDate, Name, Quantity';  // LP-284 -- Added Quantity, since it's needed for knowing whichRatePlanChargeTiers object to use when extracting Price
				$where_clause_args = array('RatePlanId' => $vars['zRatePlanId'], 'Name' => $vars['Name']);
				break;

			case 'zRatePlanChargeById':
				$zObjectName = 'RatePlanCharge';
				$select_fields = 'Id, CreatedDate, EffectiveStartDate, Name, Quantity, MRR, EffectiveStartDate, EffectiveEndDate';
				$where_clause_args = array('Id' => $vars['zRatePlanChargeId']);
				break;

			case 'zRatePlanChargesByIds':
				$zObjectName = 'RatePlanCharge';
				$select_fields = 'Id, CreatedDate, EffectiveStartDate, Name, Quantity, MRR, EffectiveStartDate, EffectiveEndDate';
				$where_clause_args = array('Id' => $vars['zRatePlanChargeIds']);  // requires custom where_clause
				$return_array = true;
				break;

			case 'zRatePlanChargeByIdWithPrice':
				$zObjectName = 'RatePlanCharge';
				$select_fields = 'Id, CreatedDate, EffectiveStartDate, Name, Quantity, Price';
				$where_clause_args = array('Id' => $vars['zRatePlanChargeId']);
				break;

			case 'zRatePlanChargeTier':
				$zObjectName = 'RatePlanChargeTier';
				$select_fields = 'Id, RatePlanChargeId, CreatedDate, UpdatedDate, Tier, StartingUnit, EndingUnit, Price, IsOveragePrice';
				$where_clause_args = array('RatePlanChargeId' => $vars['zRatePlanChargeId']);
				break;

			case 'zLicenseAmendRequest':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'TermStartDate' => $vars['date'], 'TermType' => 'TERMED', 'Status' => 'Cancelled');  // Could be Status = Expired, too, but not sure if that's correct (TO DO : add support for ORs)
				break;

			case 'zHostingAmendRequest':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'TermStartDate' => $vars['date'], 'TermType' => 'EVERGREEN', 'Status' => 'Cancelled');  // Could be Status = Expired, too, but not sure if that's correct (TO DO : add support for ORs)
				break;

			case 'zSubscription':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, Status, SubscriptionStartDate, TermEndDate, TermStartDate, TermType, Version';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'Id' => $vars['Id']);
				break;

			case 'zSubscriptionByAccountId':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, Status, SubscriptionStartDate, TermEndDate, TermStartDate, TermType, Version';
				$where_clause_args = array('AccountId' => $vars['zAccountId']);
				break;

			case 'zSubscriptions':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, Status, SubscriptionStartDate, TermEndDate, TermStartDate, TermType, Version';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'Id' => $vars['Id']);
				break;

			case 'zSubscriptionsByStatus':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, Status, SubscriptionStartDate, TermEndDate, TermStartDate, TermType, Version';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'Status' => $vars['Status']);
				$return_array = true;
				$return_single_row = false;
				break;

			case 'zSubscriptionsDrafts':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, Status, SubscriptionStartDate, TermEndDate, TermStartDate, TermType, Version';
				$where_clause_args = array('AccountId' => $vars['zAccountId'], 'Status' => 'Draft');
				$return_array = true;
				break;

			case 'zPaymentBetweenDates':
				$zObjectName = 'Payment';
				$select_fields = 'Amount';
				$where_clause = "AccountId = '{$vars['zAccountId']}' AND Status = 'Processed' AND EffectiveDate >= '{$vars['fromDate']}' AND EffectiveDate <= '{$vars['toDate']}'";
				$return_single_row = false;
				$return_array = true;
				break;

			default:
				//$this->logDebug("...got unmapped obj_type={$obj_type}");
				break;
		}

		// Allow override of select fields
		if ( !empty($vars['select_fields']) )
		{
			$select_fields = $vars['select_fields'];
		}

		// Allow override of where clause
		if ( !empty($vars['where_clause']) )
		{
			$where_clause = $vars['where_clause'];
		}

		if(!empty($vars['zObjectName'])) {
			$zObjectName = $vars['zObjectName'];
		}
		// Empty check allows custom where clauses to be specified in the obj_type switch statement
		if ( empty($where_clause) )
		{
			if ( !empty($where_clause_args) )
			{
				$where_clause = $this->getWhereClauseFromArray($where_clause_args);
			}
			else
			{
				$this->logWarning("...skipping API query for obj_type={$obj_type}");
				return null;
			}
		}

		// Empty check allows custom zoql queries to be specified in the obj_type switch statement
		if ( empty($zoql) )
		{
			$zoql = "SELECT {$select_fields} FROM {$zObjectName} WHERE {$where_clause}";
		}

		//$this->logDebug("...zoql={$zoql}");  //debug

		try
		{
			$this->initApi();
			$response = $this->zApi->query($zoql);
		}
		catch ( Exception $e )
		{
			$this->logErrorAndDie($e->getMessage() ." zoql={$zoql}\n");
		}

		$exists = (isset($response->result) && isset($response->result->size) && isset($response->result->done) && $response->result->size > 0 && $response->result->done == '1');
		//$this->logDebug("...exists={$exists} size={$response->result->size} for {$obj_type}");

		// Tie into the Zuora API's SoapClient object's debug settings
		if ( $this->zApi->_client->printRequest )
		{
			//$this->logDebug("API QUERY RESPONSE XML ----------------------------------------------------------". print_r($response, true));
		}

		if ( $exists )
		{
			if ( $response->result->size == 1 )
			{
				$zObj = $response->result->records;

				if ( $return_array )
				{
					$zObj = array($zObj);
				}
			}
			// TO DO : implement $die_on_zero_rows block
			else
			{
				if ( $return_single_row )
				{
					$zObj = $response->result->records[0];
					$this->logWarning("Multiple {$zObjectName}s returned for dataname={$dataname}; using first one ({$zObj->Id})");
				}
				else
				{
					$zObj = $response->result->records;
				}
			}
		}
		// TO DO: implement $die_on_api_errors block

		//$this->logDebug("...zObj=". print_r($zObj, true));

		return $zObj;
	}

	public function createZuoraObject($obj_type, $api_action, $zObj)
	{
		if ( empty($obj_type) || empty($api_action) || empty($zObj) )
		{
			die(__METHOD__ . " missing required arguments for obj_type={$obj_type} api_action={$api_action}");
		}

		$this->initApi();

		try
		{
			$response = $this->zApi->$api_action( $zObj );
			//$this->logDebug(__METHOD__ ."() obj_type={$obj_type} api_action={$api_action} zObj=". print_r($zObj, true) ." response=". print_r($response, true));
		}
		catch ( Exception $e )
		{
			// TO DO : update int table with $e->getMessage()
			$this->logError($e->getMessage());
		}

		return $response;
	}

	public function createZuoraObjects($obj_type, $api_action, $zObjArray)
	{
		if ( empty($obj_type) || empty($api_action) || empty($zObjArray) )
		{
			die(__METHOD__ . " missing required arguments");
		}

		$this->initApi();

		// API limits batching is limiting to 50 zObjs
		$recs_per_batch = 50;
		$total_recs = count($zObjArray);
		$total_batches = ceil($total_recs / $recs_per_batch);
		//$this->logDebug("...recs_per_batch={$recs_per_batch} total_batches={$total_batches}");

		for ( $batch_num = 0; $batch_num < $total_batches; $batch_num++ )
		{
			$batch_start_time = microtime(true);
			$starting_rec_num = $batch_num * $recs_per_batch;
			$zObjArrayBatch = array_slice($zObjArray, $starting_rec_num, $recs_per_batch);  // the last arg is key since it keeps the integer indexes the same as the the full source array

			//$this->logDebug("...batch_num={$batch_num} starting_rec_num={$starting_rec_num} ending_rec_num=". ($starting_rec_num + count($zObjArrayBatch)) ." batch_recs_count=". count($zObjArrayBatch));
			//$this->logDebug("...batch {$batch_num} zObjArrayBatch=". print_r($zObjArrayBatch, true));

			try
			{
				$response = $this->zApi->$api_action( $zObjArrayBatch );
				//$this->logDebug(__METHOD__ ."() obj_type={$obj_type} api_action={$api_action} response=". print_r($response, true));
			}
			catch ( Exception $e )
			{
				// TO DO : update int table with $e->getMessage()
				$this->logError($e->getMessage());
			}

			$responses[] = $response;
			$this->logMsg("...dataname={$this->dataname} batch {$batch_num} took ". (microtime(true) - $batch_start_time) ." seconds");
		}

		return $responses;
	}

	public function zoqlQuery($zoql, $using_query_locator = false)
	{
		if ( empty($zoql) )
		{
			return null;
		}

		$this->initApi();

		$zoql = trim($zoql);

		try
		{
			if ( $using_query_locator )
			{
				$response = $this->zApi->queryMore($zoql);
			}
			else
			{
				$response = $this->zApi->query($zoql);
			}
		}
		catch ( Exception $e )
		{
			return $e->getMessage();
		}

		return $response;
	}


	function getZuoraDigitalSignature($hpm_pageid)
	{
		//$this->logDebug("In ". __METHOD__ ."() for hpm_pageid={$hpm_pageid}");

		$ztoken     = false;
		$zsignature = false;
		$zkey       = false;

		$json_payload = json_encode( array(
			'uri'    => ZuoraConfig::HPM_PAGE_URL,
			'method' => 'POST',
			'pageId' => $hpm_pageid,
		));

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, ZuoraConfig::HPM_DIGITALSIG_ENDPOINT); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_payload);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("apiAccessKeyId:". ZuoraConfig::API_USERNAME, "apiSecretAccessKey:". ZuoraConfig::API_PASSWORD, 'Accept:application/json', 'Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

		$json_response = curl_exec($ch);
		curl_close($ch); 

		$response = json_decode($json_response, true);
		//$this->logDebug("...response=". print_r($response, true));

		if ( isset($response['success']) && $response['success'] )
		{
			$ztoken     = $response['token'];
			$zsignature = $response['signature'];
			$zkey       = $response['key'];
		}

		return array($ztoken, $zsignature, $zkey);
	}

	public function echoHpmForm($vars)
	{
		$this->echoHpmHtmlDiv();
		echo "<script>\n";
		$this->echoHpmJsCallback();
		$this->echoHpmJs($vars);
		echo "</script>\n";
	}


	public function echoHpmHtmlDiv($vars)
	{
		echo <<<HTML
			<div id="zuora_payment"></div>
HTML;
	}

	/**
	 * Echos javascript needed to display HPM
	 *
	 * @param array $vars required keys: page_name, amount, http_host
	 *
	 * LP-282 -- Added support for loading HPMs from other subdomain.domain combos
	 */
	public function echoHpmJs($vars)
	{
		//$this->logDebug("In ". __METHOD__ ."() with vars=". json_encode($vars));

		if ( !isset($vars['page_name']) || !isset($vars['amount']) || !isset($vars['http_host']) )
		{
			$this->logError(__METHOD__ ." had missing required arg(s): vars=". json_encode($vars));
			return;
		}

		// Develop a string to access the appropriate ZuoraConfig const var
		$hpm_pageid_const_name = 'HPM_PAGEID_'. strtoupper($vars['page_name']) .'__'. str_replace(array('.', '-'), '_', $vars['http_host']);
		$hpm_pageid = constant('ZuoraConfig::'. $hpm_pageid_const_name);

		// Request Zuora digital signature needed to display Hosted Payment Method page
		list($ztoken, $zsignature, $zkey) = $this->getZuoraDigitalSignature($hpm_pageid);
		if ( $ztoken === null || $zsignature === null || $zkey === null )
		{
			$this->logError(__METHOD__ ." got null Zuora Digital Signature vars");
			return;
		}

		// Escape double quotes in vars fields before using them to output html
		// This also sets missing fields to blank so we don't throw PHP warnings accessing non-existent keys
		foreach ( array('firstname', 'lastname', 'country', 'address', 'city', 'state', 'zip', 'email', 'amount', 'sessionid') as $key )
		{
			$val = isset($vars[$key]) ? $vars[$key] : '';
			$vars[$key] = str_replace('"', '\\"', $val);
		}

		if ( empty($vars['style']) )
		{
			$vars['style'] = 'inline';
 		}

		if ( !empty($vars['auto_show_form']) )
		{
			$vars['auto_show_form'] = 'loadHostedPage();';
		}

		if ( !empty($vars['zAccountId']) )
		{
			$field_accountId = "field_accountId:\"{$vars['zAccountId']}\",";
		}
		else
		{
			$field_accountId = '';
		}

		// Can't use class const vars in a heredoc so we have to make vars for them
		$hpm_page_url = ZuoraConfig::HPM_PAGE_URL;
		$hpm_tenantid = ZuoraConfig::HPM_TENANTID;
		$hpm_paymentgateway_name = ZuoraConfig::HPM_PAYMENTGATEWAY_NAME;

		echo <<<JAVASCRIPT
			// optional params in case prepopulation of certain fields is desired
			var prepopulateFields = {
				creditCardHolderName:"{$vars['firstname']} {$vars['lastname']}",
				creditCardCountry:"{$vars['country']}",
				creditCardAddress1:"{$vars['address']}",
				creditCardCity:"{$vars['city']}",
				creditCardState:"{$vars['state']}",
				creditCardPostalCode:"{$vars['zip']}",
				phone:"{$vars['phone']}",
				email:"{$vars['email']}"
			};

			var params = {
				tenantId:{$hpm_tenantid},
				url:"{$hpm_page_url}",
				id:"{$hpm_pageid}",
				token:"{$ztoken}",
				signature:"{$zsignature}",
				key:"{$zkey}",
				paymentGateway:"{$hpm_paymentgateway_name}",
				param_supportedTypes:"AmericanExpress,JCB,Visa,MasterCard,Discover",
				authorizationAmount:"1.00",
				field_deviceSessionId:"{$vars['sessionid']}",
				submitEnabled:"true",
				style:"{$vars['style']}",
				{$field_accountId}
				locale:"en"
			};

			function loadHostedPage() {
				Z.render(
					params,
					prepopulateFields,
					callback
				);
			}
			{$vars['auto_show_form']}
JAVASCRIPT;
	}

	public function echoHpmJsCallback()
	{
		echo <<<JAVASCRIPT
			function callback(response) {
				alert(JSON.stringify(response, null, 4));  //debug - REMOVE do not merge
				if(response.success) {
					var redirectUrl = window.location.href+"&refid="+response.refId;
					window.location.replace(redirectUrl);
				} else {
					alert("errorcode="+response.errorCode + ", errorMessage="+response.errorMessage);
				}
			}
JAVASCRIPT;
	}

	public function echoHpmJsInc()
	{
		// Can't use class const vars in a heredoc so we have to make vars for them
		$hpm_js_inc = ZuoraConfig::HPM_JS_INC;

		echo <<<HTML
			<script type="text/javascript" src="{$hpm_js_inc}"></script>
HTML;
	}

	public function logMsg($msg)
	{
		//echo $msg ."\n";
		error_log($msg);
	}

	public function logWarning($msg)
	{
		$this->logMsg('WARNING: '. $msg);
	}

	public function logDebug($msg)
	{
		if (ZuoraConfig::SANDBOX) $this->logMsg('DEBUG: '. $msg);
	}

	public function logError($msg)
	{
		error_log(__FILE__ .' got error: '. $msg);
		$this->logMsg('ERROR: '. $msg);
	}

	public function logErrorAndDie($msg)
	{
		error_log(__FILE__ .' got error in '. __METHOD__ .'(): '. $msg);
		$this->logMsg('ERROR: '. $msg);
		die('Error');
	}

	/**
	 * Private Methods
	 */

	private function escapeZoql($zoql_fragment)
	{
		return str_replace("'", "\\'", $zoql_fragment);
	}

	private function getWhereClauseFromArray($where_clause_args)
	{
		//$this->logDebug("...where_clause_args=". json_encode($where_clause_args));

		$where_clause = '';
		foreach ( $where_clause_args as $key => $val )
		{
			$where_clause .= empty($where_clause) ? "{$key} = '". $this->escapeZoql($val) ."'" : " AND {$key} = '". $this->escapeZoql($val) ."'";
		}

		//$this->logDebug("...where_clause={$where_clause}");

		return $where_clause;
	}

	private function initApi()
	{
		if ( $this->zApi === null )
		{
			// Init Zuora API
			$config = new stdClass();
			$config->wsdl = ZuoraConfig::API_WSDL;
			$this->zApi = Zuora_API::getInstance($config);
			$this->zApi->setLocation(ZuoraConfig::API_ENDPOINT);
			$this->zApi->login(ZuoraConfig::API_USERNAME, ZuoraConfig::API_PASSWORD);
		}
	}

	/**
	* This function is used to get Zuora payment details between two dates.
	*
	* @param array $vars it consists fromDate and toDate
	*
	* @return array $zPayments.
	*/
	public function getPaymentsBetweenDates($vars) {
		$zPayments = array();
		if ( !empty($vars['zAccountId']) && !empty($vars['toDate']) ) {
			$zPayments = $this->getZuoraObjectViaApiQuery('zPaymentBetweenDates', $vars);
		}
		else {
			$this->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
		}

		return $zPayments;
	}

}