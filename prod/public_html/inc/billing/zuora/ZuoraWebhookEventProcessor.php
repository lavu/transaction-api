<?php

require_once(dirname(__FILE__).'/../../../../private_html/ZuoraConfig.php');
require_once(dirname(__FILE__).'/../../../admin/cp/resources/core_functions.php');
require_once(dirname(__FILE__).'/../../../admin/sa_cp/billing/package_levels_object.php');
require_once(dirname(__FILE__).'/ZuoraIntegration.php');
require_once(dirname(__FILE__).'/ZuoraUtils.php');
require_once(dirname(__FILE__).'/../LavuAccountUtils.php');


class ZuoraWebhookEventProcessor
{
	private $event;
	private $vars;

	public $zInt;
	public $zUtils;
	public $accountUtils;

	public function __construct()
	{
		date_default_timezone_set('America/Denver');

		$this->zInt = new ZuoraIntegration();
		$this->zUtils = new ZuoraUtils($this->zInt);
		$this->accountUtils = new LavuAccountUtils();
	}

	private function init($vars)
	{
		$this->vars = $vars;

		if ( isset($vars['action']) )
		{
			$this->action = $vars['action'];
		}
	}

	public function run($vars)
	{
		$this->init($vars);

		switch ( $this->action )
		{
			case 'create':
				// Email is required for Lavu account creation but SoldTo->WorkEmail isn't a required field, so default
				// to using the Billing dept's email so they'll get the Welcome email and be signalled to the issue
				if ( empty($vars['email']) )
				{
					$vars['email'] = ZuoraConfig::SANDBOX ? ZuoraConfig::DEVELOPER_EMAIL : 'billing@lavuinc.com';
				}

				// TO DO : check SubscriptionId to see if it contained a Lavu POS Hosting product

				if ( empty($vars['dataname']) )
				{
					$response = $this->processCreate($vars);
				}
				else if ( !empty($vars['dataname']) && !empty($vars['distro_code']) )
				{
					$response = $this->processCreateWithExistingAccount($vars);
				}

				////if ($response['success']) SalesforceIntegration::createSalesforceEvent($response['dataname'], 'stage_change', 'Closed Won');
				break;

			case 'cancel':
				$response = $this->processCancel($vars);
				////if ($response['success']) SalesforceIntegration::createSalesforceEvent($vars['dataname'], 'stage_change', 'Cancelled');
				break;

			case 'delinquent':
			case 'payment':
				$response = $this->processDelinquent($vars);
				break;

			case 'upgrade':
				$response = $this->processUpgrade($vars);
				break;

			default:
				error_log(__FILE__ ." got unknown webhook event={$this->event} with vars=". json_encode($vars));
				break;
		}

		return $response;
	}


	public function processCreate($vars)
	{
		// Check required vars
		if ( empty($vars['company']) || empty($vars['email']) )
		{
			error_log(__METHOD__ ."() missing required arg(s): ". json_encode($vars));
			return false;
		}

		// Append SoldToContact->Address2 field to Address1, if present
		if ( !empty($vars['address2']) )
		{
			$vars['address'] .= ', '. $vars['address2'];
		}

		// This isn't sent in the webhook but I wanted to have support for specifying different package levels
		if ( empty($vars['package_name']) )
		{
			$vars['package_name']  = 'Lavu';
			$vars['package_level'] = '25';
		}

		error_log("...creating account with vars=". print_r($vars, true));  //debug

		// Create account
		$response = $this->accountUtils->createAccount($vars);
		error_log("...response=". print_r($response, true));  //debug

		if ( isset($response['success']) && $response['success'] )
		{
			// Update Zuora account with newly created Lavu account info
			$params = array(
				'zAccountId'      => $vars['zAccountId'],
				'dataname'        => $response['dataname'],
				'RestaurantID__c' => $response['restaurantid'],
				'SignupID__c'     => $response['signupid'],
				'PackageName__c'  => $vars['package_name'],
				'PackageLevel__c' => $vars['package_level'],
				'LastActivity__c' => date('Y-m-d'),
			);

			$updated = $this->zInt->updateAccount($params);

			if ( !$updated )
			{
				error_log(__FILE__ ." failed to update account got un webhook event={$this->event} with vars=". json_encode($vars));
			}
		}

		return $response['success'];
	}

	public function processCreateWithExistingAccount($vars)
	{
		$success = false;

		$signup = $this->accountUtils->getDatabaseRow('signups', $vars['dataname']);

		if ( $signup['package'] == 'none' && $signup['status'] == 'manual' )
		{
			$payment_request = $this->accountUtils->getDatabaseRow('payment_request-by_refid', $vars['dataname'], $vars);
			$vars = $vars + $payment_request;

			// The webhook action in vars would override the payment_request action
			$vars['payment_request_action'] = $payment_request['action'];

			$reseller_quote = $this->accountUtils->getDatabaseRow('reseller_quote-resellerid+quote_pdf_filename', $vars['dataname'], $vars);
			$vars = $vars + $reseller_quote;

			$success = $this->accountUtils->activateResellerAccountFromDistroPortalQuote($vars);
		}

		return array('success' => $success);
	}


	public function processCancel($vars)
	{
		return $this->accountUtils->cancelAccount($vars);
	}


	public function processDelinquent($vars)
	{
		$details = json_decode($vars['details'], true);

		$due_info = $this->zUtils->getDueInfoArray($vars);

		return $this->accountUtils->setDueInfo($vars['dataname'], $due_info);
	}


	public function processUpgrade($vars)
	{
		// TO DO
	}

}