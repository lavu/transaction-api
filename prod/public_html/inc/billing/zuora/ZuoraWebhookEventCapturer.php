<?php

/**
 * Zuora webhook endpoint processing script
 * intentionally kept lightweight for minimal overhead
 * where we just insert data into row for cron'd script
 *
 */

class ZuoraWebhookEventCapturer
{
	private $action;
	private $zObjType;
	private $details;

	public function __construct($action, $vars)
	{
		$this->action = $action;
		$vars['action'] = $action;

		switch ( $this->action )
		{
			case 'create':
				$vars['zObjType'] = 'zSubscription';
				break;

			case 'cancel':
			case 'upgrade':
				$vars['zObjType'] = 'zAmendment';
				break;

			case 'delinquent':
				$vars['zObjType'] = 'zInvoice';
				break;

			case 'payment':
				$vars['zObjType'] = 'zPayment';
				break;

			default:
				error_log(__FILE__ ." got unmapped action {$this->action}");
				break;
		}

		$this->capture($vars);
	}

	public function capture($vars)
	{
		$processed = false;

		if ( empty($vars['zAccountId']) || empty($vars['zObjId']) )
		{
			error_log("Could not process '{$this->action}' webhook for ". json_encode($vars));  //debug
			return $processed;
		}

		date_default_timezone_set('America/Denver');

		require_once(dirname(__FILE__).'/../../../admin/cp/resources/core_functions.php');
		mlavu_select_db('poslavu_MAIN_db');

		$vars['data'] = json_encode($vars);
		$vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];

		// Insert row for event into the webhooks table
		$result = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_zuora_webhooks` (`createdtime`, `updatedtime`, `ipaddress`, `status`, `action`, `dataname`, `zAccountId`, `zObjId`, `zObjType`, `data`) VALUES (now(), now(), '[ipaddress]', 'new', '[action]', '[dataname]', '[zAccountId]', '[zObjId]', '[zObjType]', '[data]')", $vars);
		$row_id = mlavu_insert_id();

		if ( !empty($row_id) )
		{
			$processed = true;
			//error_log("Inserted row ID {$row_id} for Zuora {$this->action} webhook event for dataname={$vars['dataname']} zAccountId={$vars['zAccountId']}");  //debug

			// Signal to Zuora that the webhook was successful
			http_response_code(200);
		}

		return $processed;
	}
}
