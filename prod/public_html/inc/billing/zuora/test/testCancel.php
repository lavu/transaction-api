<?php

require_once(dirname(__FILE__).'/../ZuoraIntegration.php');

$zAccountId = '2c92c0f856f9d5590157009916506ae6';  // visitation_sta
$reason_with_notes = "Connectivity, Network or Internet Issues (COULDN'T GET THE THING TO WORK SO IT DIDN'T)";
$cvars['cancel_date'] = '2016-09-06';

$zInt = new ZuoraIntegration();
$response = $zInt->cancelAccount(array('zAccountId' => $zAccountId, 'cancel_reason' => $reason_with_notes, 'cancel_date' => $cvars['cancel_date']));
