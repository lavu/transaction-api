<?php

// Untested

require_once(dirname(__FILE__).'/../ZuoraIntegration.php');

// Command-line params
if ( empty($argv[1]) || empty($argv[2]) )
{
	echo "Usage: {$argv[0]} <Lookup key> <Lookup val>\n";
	exit;
}

$arg_key = $argv[1];
$arg_val = $argv[2];
$args = array($arg_key => $arg_val);

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$zInt = new ZuoraIntegration();

$zAccount = $zInt->getAccount($args);
echo "Fetching Zuora account from {$env} Zuora with args=". json_encode($args) ." zAccount=". print_r($zAccount, true) ."\n";

exit;