<?php

require_once('/home/poslavu/public_html/inc/billing/LavuAccountUtils.php');

// Get the file containing overpayment datanames (from due_info_calculator2.php)
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <file with datanames>\n";
	exit;
}

$accountUtils = new LavuAccountUtils();

$lines = file($argv[1]);
$num_rows = count($lines);
$i = 0;

foreach ( $lines as $line )
{
	$i++;
	$dataname = rtrim($line);  // Trim EOL whitespace

	echo $i .'/'. $num_rows ."\t". $dataname ."\t";

	$package_info = $accountUtils->setPosPackageInfo($dataname);

	echo "\t". json_encode($package_info);
	echo "\n";
}

exit;