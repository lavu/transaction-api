<?php

require_once(dirname(__FILE__).'/../ZuoraUtils.php');
require_once(dirname(__FILE__).'/../../LavuAccountUtils.php');

// Command-line params
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname> [<dataname2> ...]\n";
	exit;
}

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$datanames = array_slice($argv, 1);

$zUtils = new ZuoraUtils();
$acctUtils = new LavuAccountUtils();

foreach ( $datanames as $dataname )
{
	$args = array('dataname' => $dataname);
	$due_info_components = $zUtils->getDueInfoArray($args);
	$due_info = $acctUtils->getDueInfo($due_info_components);
	$due_info_updated = $zUtils->setDueInfo($dataname, $due_info_components);
	echo "Zuora {$env} -- dataname={$dataname} due_info={$due_info} due_info_updated={$due_info_updated} due_info_components=". print_r($due_info_components, true);
}

exit;