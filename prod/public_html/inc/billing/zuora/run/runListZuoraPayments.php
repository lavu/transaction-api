<?php

require_once('/home/poslavu/public_html/inc/billing/zuora/ZuoraIntegration.php');

$zInt = new ZuoraIntegration();

// Build Zuora AccountId => Lavu dataname mapping

$zAccountDataname = array();
$zAccountRestaurantId = array();
$zAccountIdsWithNoDataname = array();
$zAccountIdsWithNoRestaurantId = array();

$zoql = "SELECT Id, Dataname__c, RestaurantID__c FROM Account";
$using_query_locator = false;
$i = 1;

while ( $response = $zInt->zoqlQuery($zoql, $using_query_locator) )
{

	//echo "response=". print_r($response, true);  //debug

	// Prep the records returned from the query for processing
	$records = array();
	if ( isset($response) && isset($response->result) )
	{
		if ( $response->result->size === 1 )
		{
			$records[] = $response->result->records;
		}
		else if ( $response->result->size > 1 )
		{
			$records = $response->result->records;
		}
	}

	// Process the records
	foreach ( $records as $zAccount )
	{
		// Build our list of datanames and collect a list of Zuora accounts without datanames
		if ( empty($zAccount->Dataname__c) )
		{
			$zAccountIdsWithNoDataname[] = $zAccount->Id;
		}
		else
		{
			$zAccountDataname[$zAccount->Id] = $zAccount->Dataname__c;
		}

		// Build our list of restaurant IDs and collect a list of Zuora accounts without restaurant IDs
		if ( empty($zAccount->RestaurantID__c) )
		{
			$zAccountIdsWithNoRestaurantId[] = $zAccount->Id;
		}
		else
		{
			$zAccountRestaurantId[$zAccount->Id] = $zAccount->RestaurantID__c;
		}
	}

	// If we received a zQueryLocationId, then there are more results to get (max per batch is 2000) so we need to iterate again
	if ( isset($response->result->queryLocator) )
	{
		$zoql = $response->result->queryLocator;
		$using_query_locator = true;
		$i++;
	}
	// No zQueryLocationId means we've pulled all remaining records so exit the loop
	else
	{
		break;
	}
}

echo "zAccountIdsWithNoDataname=". print_r($zAccountIdsWithNoDataname, true) ." ". count($zAccountIdsWithNoDataname) ."\n";  //debug
echo "zAccountIdsWithNoRestaurantId=". print_r($zAccountIdsWithNoRestaurantId, true) ." ". count($zAccountIdsWithNoRestaurantId) ."\n";  //debug
//echo "zAccountDataname=". print_r($zAccountDataname, true) ." ". count($zAccountDataname) ."\n";  //debug


// Build Payment to AccountId mapping

$plist = array();
$zPaymentsWithoutDataname = array();
$zPaymentsWithoutRestaurantId = array();

$zoql = "SELECT AccountId, Amount, CreatedDate, EffectiveDate, Status FROM Payment WHERE Status = 'Processed' AND EffectiveDate >= '2016-09-01' and EffectiveDate < '2016-10-01'";
$using_query_locator = false;
$i = 1;

while ( $response = $zInt->zoqlQuery($zoql, $using_query_locator) )
{
	//echo "response=". print_r($response, true);  //debug

	// Prep the records returned from the query for processing
	$records = array();
	if ( isset($response) && isset($response->result) )
	{
		if ( $response->result->size === 1 )
		{
			$records[] = $response->result->records;
		}
		else if ( $response->result->size > 1 )
		{
			$records = $response->result->records;
		}
	}

	// Process the records
	foreach ( $records as $zPayment )
	{
		$zAccountId = $zPayment->AccountId;
		$zAccountHasDataname = isset($zAccountDataname[$zAccountId]);
		$zAccountHasRestaurantId = isset($zAccountRestaurantId[$zAccountId]);

		$dataname = $zAccountHasDataname ? $zAccountDataname[$zAccountId] : '';
		$restaurantid = $zAccountHasRestaurantId ? $zAccountRestaurantId[$zAccountId] : '';

		// Build our list of datanames and collect a list of Zuora accounts without datanames
		if ( $zAccountHasDataname )
		{
			// Process per dataname payment count
			if ( isset($plist[$dataname]) )
			{
				$plist[$dataname] += 1;
			}
			else
			{
				$plist[$dataname] = 1;
			}

			// Save the last payment per dataname
			if ( isset($last_pay[$dataname]) )
			{
				// Use the current zPayment as the last payment if it's newer than the one we had saved
				if ( strtotime($zPayment->EffectiveDate) > strtotime($last_pay[$dataname]['date']) )
				{
					$last_pay[$dataname] = array('date' => $zPayment->EffectiveDate, 'amount' => $zPayment->Amount);
				}
			}
			else
			{
				$last_pay[$dataname] = array('date' => $zPayment->EffectiveDate, 'amount' => $zPayment->Amount);
			}
		}
		else
		{
			$zPaymentsWithoutDataname[] = $zPayment->Id;
		}

		// Build our list of restaurant IDs and collect a list of Zuora accounts without restaurant IDs
		if ( $zAccountHasRestaurantId )
		{
			$yrmo = substr($zPayment->EffectiveDate, 0, 7);

			// Process per restaurant ID pay_reads
			if ( isset($pay_reads[$restaurantid]) && isset($pay_reads[$restaurantid]['month']) && $pay_reads[$restaurantid]['month'] == $yrmo )
			{
				$pay_reads[$restaurantid]['count'] += 1;
				$pay_reads[$restaurantid]['total'] += (float) $zPayment->Amount;
			}
			else
			{
				$pay_reads[$restaurantid] = array(
					'count' => 1,
					'total' => (float) $zPayment->Amount,
					'match_restaurantid' => $restaurantid,
					'month' => $yrmo,
				);
			}
		}
		else
		{
			$zPaymentsWithoutRestaurantId[] = $zPayment->Id;
		}
	}

	// If we received a zQueryLocationId, then there are more results to get (max per batch is 2000) so we need to iterate again
	if ( isset($response->result->queryLocator) )
	{
		$zoql = $response->result->queryLocator;
		$using_query_locator = true;
		$i++;
	}
	// No zQueryLocationId means we've pulled all remaining records so exit the loop
	else
	{
		break;
	}
}

//echo "zPaymentsWithoutDataname=". print_r($zPaymentsWithoutDataname, true) ." ". count($zPaymentsWithoutDataname) ."\n";  //debug
//echo "zPaymentsWithoutRestaurantId=". print_r($zPaymentsWithoutRestaurantId, true) ." ". count($zPaymentsWithoutRestaurantId) ."\n";  //debug
//echo "plist=". print_r($plist, true) ." ". count($plist) ."\n";  //debug
