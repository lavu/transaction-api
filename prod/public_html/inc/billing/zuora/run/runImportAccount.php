<?php

require_once(dirname(__FILE__).'/../ZuoraIntegration.php');

// Command-line params
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname> [<zPaymentMethodId>]\n";
	exit;
}

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$dataname = $argv[1];
$zPaymentMethodId = isset($argv[2]) ? $argv[2] : null;

$zInt = new ZuoraIntegration();
list($zAccountId, $zRatePlanChargeId) = $zInt->importAccount( array('dataname' => $dataname, 'zPaymentMethodId' => $zPaymentMethodId) );

echo "Zuora {$env} -- imported dataname={$dataname} zAccountId={$zAccountId} zRatePlanChargeId={$zRatePlanChargeId}\n";

exit;