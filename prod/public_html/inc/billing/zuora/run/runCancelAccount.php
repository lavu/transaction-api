<?php

// Untested

require_once(dirname(__FILE__).'/../ZuoraIntegration.php');
require_once(dirname(__FILE__).'/../../../../admin/cp/resources/core_functions.php');

// Command-line params
if ( empty($argv[1]) || empty($argv[2]) || empty($argv[3]) )
{
	echo "Usage: {$argv[0]} <zAccountId> <cancel_date> <cancel_reason - notes>\n";
	exit;
}

$zAccountId        = $argv[1];
$cancel_date       = $argv[2];
$reason_with_notes = $argv[3];  // reason OR reason - notes

$args = array('zAccountId' => $zAccountId, 'cancel_date' => $cancel_date, 'cancel_reason' => $reason_with_notes);

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$zInt = new ZuoraIntegration();

$response = $zInt->cancelAccount($args);
echo "Cancelling in {$env} Zuora: response=". print_r($response, true) ."\n";

exit;