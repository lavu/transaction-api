<?php

require_once('/home/poslavu/public_html/inc/billing/zuora/ZuoraIntegration.php');

if ( empty($argv[1]) || empty($argv[2]) )
{
	echo "Usage: {$argv[0]} <zAccountId> <zPaymentMethodId>\n";
	exit;
}

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$zAccountId = $argv[1];
$zPaymentMethodId = $argv[2];

$zInt = new ZuoraIntegration();

$params = array(
	'zAccountId' => $zAccountId,
	'DefaultPaymentMethodId' => $zPaymentMethodId,
	'AutoPay' => true,
);

$update_ok = $zInt->updateAccount($params);

echo "Zuora {$env} -- update_ok={$update_ok}\n";
exit;
