<?php

require_once(dirname(__FILE__).'/../ZuoraUtils.php');

// Command-line params
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname> [<dataname2> ...]\n";
	exit;
}

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$datanames = array_slice($argv, 1);

$zUtils = new ZuoraUtils();

foreach ( $datanames as $dataname )
{
	$args = array('dataname' => $dataname);

	$zAccount = $zUtils->zInt->getAccount($args);

	if ( $zAccount !== null )
	{
		$args['zAccountId'] = $zAccount->Id;
		list($recurring_amount, $recurring_start_date) = $zUtils->getMonthlyRecurringInfo($args);
		echo "Zuora {$env} -- dataname={$dataname} zAccountId={$zAccount->Id} recurring_amount={$recurring_amount} recurring_start_date={$recurring_start_date}\n";
	}
	else
	{
		echo "Zuora {$env} -- dataname={$dataname} did not have Zuora Account\n";
	}
}

exit;