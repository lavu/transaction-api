<?php

require_once(dirname(__FILE__).'/../ZuoraIntegration.php');

// Command-line params
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname> [<dataname2> ...]\n";
	exit;
}

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$datanames = array_slice($argv, 1);

$zInt = new ZuoraIntegration();

foreach ( $datanames as $dataname )
{
	$args = array('dataname' => $dataname);
	list($zAccountId, $zBillToContactId, $zSoldToContactId) = $zInt->createAccountWithContacts($args);
	echo "Zuora {$env} -- imported dataname={$dataname} zAccountId={$zAccountId} zBillToContactId={$zBillToContactId} zSoldToContactId={$zSoldToContactId}\n";
}

exit;