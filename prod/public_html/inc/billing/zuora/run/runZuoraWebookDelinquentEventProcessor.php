<?php

require_once(dirname(__FILE__).'/../ZuoraWebhookEventProcessor.php');

date_default_timezone_set('America/Denver');

mlavu_select_db('poslavu_MAIN_db');

// General unique ID for this process
$uid = uniqid('', true);
$dbargs = array('uid' => $uid);

// Update unprocessed recs to claim them for this process (it's possible a previous process is still working on its events)
$update_result = mlavu_query("UPDATE `poslavu_MAIN_db`.`billing_zuora_webhooks` SET `status` = 'in_progress_by_[uid]', `updatedtime` = NOW() WHERE `status` = 'new' AND `action` = 'delinquent'", $dbargs);
$affected_rows = $update_result->affected_rows;

// Check for errors
if ( $affected_rows === -1 )
{
	die('Got db error: '. mlavu_dberror() ."\n");
}
// Check for no event recs
else if ( $update_result === false || $affected_rows === 0 )
{
	echo "...no new events found; exiting\n";
	exit;
}

echo "...uid={$uid} claimed {$affected_rows} event(s)\n";

// Query for claimed event recs
$events = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_zuora_webhooks` WHERE `status` = 'in_progress_by_[uid]'", $dbargs);
if ($events === false) die('Got db error:'. mlavu_dberror() ."\n");

$num_events = mysqli_num_rows($events);
echo "...uid={$uid} fetched {$num_events} event(s)\n";

$processor = new ZuoraWebhookEventProcessor();

// Process event
while ( $event = mysqli_fetch_assoc($events) )
{
	echo "...processing event ID {$event['id']} action={$event['action']}\n";
	$data = json_decode($event['data'], true);

	try
	{
		$result = $processor->run($data);
	}
	catch ( Exception $e )
	{
		error_log("Zuora webhook processor uid={$uid} threw exception for {$event['action']} event ID {$event['id']}: ". $e->getMessage());
		$result['success'] = false;
	}

	// Mark event row as processed|errored
	$event['uid'] = $uid;
	$event['status'] = ($result['success']) ? 'complete' : 'error';
	$update_result = mlavu_query("UPDATE `poslavu_MAIN_db`.`billing_zuora_webhooks` SET `status` = '[status]', `updatedtime` = NOW() WHERE `id` = '[id]' AND `status` = 'in_progress_by_[uid]'", $event);
	if ( !$update_result )
	{
		mail(ZuoraConfig::DEVELOPER_EMAIL, 'Zuora Webhook Error', "Could not flag webhook event ID {$event['id']} as {$event['status']}");
	}
	if ( !$event['status'] )
	{
		mail(ZuoraConfig::DEVELOPER_EMAIL, 'Zuora Webhook Error', "Zuora webhook processor uid={$uid} got error while processing webhook event ID {$event['id']}");
	}
}
