<?php

require_once(dirname(__FILE__).'/../ZuoraIntegration.php');

// Command-line params
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname> [<dataname1> <dataname2> ...]\n";
	exit;
}

echo 'SANDBOX='. ZuoraConfig::SANDBOX ."\n";

// Use the list of white-space-delimited command-line args as our list of datanames to import
$datanames  = array_slice($argv, 1);

// Separate arrays for categorizing the results for easy displaying at the end
$successes  = array();
$failures   = array();
$unknowns   = array();

// Develop a list of update statements to display for easy copy and pasting into Sequel Pro
$update_sql = '';

foreach ( $datanames as $dataname )
{
	$zInt = new ZuoraIntegration();
	try
	{
		list($zAccountId, $zRatePlanChargeId) = $zInt->importAccount( array('dataname' => $dataname) );
	}
	catch ( Exception $e )
	{
			$failures[$dataname] = $e->getMessage();
	}

	if ( !empty($zAccountId) )
	{
		// Concat the values together with a weird separator character that will allow easy double-click copying as well as provide some visual separation of the two values
		$successes[$dataname] = $zAccountId .'^'. $zRatePlanChargeId;

		// The where clause will prevent us from updating datanames that already had their zAccountId set in the signups table, just so we have some indication of the new vs. the pre-existing zAccountIds we have
		$update_sql .= "UPDATE `poslavu_MAIN_db`.`signups` SET `zAccountId` = '{$zAccountId}', `zRatePlanChargeId` = '{$zRatePlanChargeId}' WHERE `dataname` = '{$dataname}' AND (`zAccountId` = '' OR `zRatePlanChargeId` = '') LIMIT 2;\n";
	}
	else
	{
		$unknowns[] = $dataname;
	}
}

// Display results
echo 'errors='.     print_r($errors, true)    ."\n";
echo 'failures='.   print_r($failures, true)  ."\n";
echo 'successes='.  print_r($successes, true) ."\n";
echo "UPDATE_SQL TO RUN\n". $update_sql       ."\n";

exit;