<?php

require_once(dirname(__FILE__).'/../ZuoraIntegration.php');
require_once(dirname(__FILE__).'/../../../../admin/cp/resources/core_functions.php');

// Command-line params
if ( empty($argv[1]) )
{
	echo "Usage: {$argv[0]} <dataname> ...\n";
	exit;
}

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$zInt = new ZuoraIntegration();

// Get all space-delimited datanames they've entered via the command-line for processing
$datanames = array_slice($argv, 1);

$succeeded = array();
$failed    = array();

foreach ( $datanames as $dataname )
{
	$synced = $zInt->syncAccount(array('dataname' => $dataname));
	if ( $synced )
	{
		$succeeded[] = $dataname;
	}
	else
	{
		$failed[] = $dataname;
	}
}

echo "failed=". print_r($failed, true) ."\n";
echo "succeeded=". print_r($succeeded, true) ."\n";
exit;