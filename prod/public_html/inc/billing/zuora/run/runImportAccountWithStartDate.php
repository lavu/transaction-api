<?php

require_once(dirname(__FILE__).'/../ZuoraIntegration.php');

// Command-line params
if ( empty($argv[1]) || empty($argv[2]) )
{
	echo "Usage: {$argv[0]} <dataname> <YYYY-mm-dd>\n";
	exit;
}

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

$args = array();

$dataname = $argv[1];
$args['dataname'] = $dataname;

// Start Date is actually optional but it's treated as a required param in the param checking section
if ( !empty($argv[2]) )
{
	$start_date = $argv[2];
	$args['start_date'] = $start_date;
}

$zInt = new ZuoraIntegration();
list($zAccountId, $zRatePlanChargeId) = $zInt->importAccount($args);

echo "Zuora {$env} -- imported dataname={$dataname} zAccountId={$zAccountId} zRatePlanChargeId={$zRatePlanChargeId} start_date={$start_date}\n";

exit;