<?php

/*

First get the ProductId for the Hosting product:

	SELECT Name, Id FROM Product WHERE Name = 'Lavu POS Hosting'

Then use ProductId to get the list of Package Name to ProductRatePlanId mappings:

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92c0f956bc8fcb0156ebeff1fb2bbb'

You will need reformat the results using a regex search and replace: /^(.*)\t(.*)\t(.*)$/$this->mappingPackgeNameToProductRatePlanId['Hosting']['\2'] = '\3';/

And then you need to replace the value at ['Hosting']['Monthly Lavu Package'] - given by this query:

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92a0fd56c0191d0156c9f68b141fbc' AND Name = 'Monthly Lavu Package'

with this value

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92a0fd56c0191d0156c9f68b141fbc' AND Name = 'Monthly - POS Hosting'

*/

$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Gold Package'] = '2c92c0f856bc84c90156ebeff77b1919';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Silver2 Package'] = '2c92c0f856bc84c90156ebf024941c73';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly MercuryFreeLavu88 Package'] = '2c92c0f856bc84c90156ebf03aa71d68';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly MercuryGold Package'] = '2c92c0f856bc84c90156ebf04b8d1d6a';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly MercuryPro Package'] = '2c92c0f856bc84c90156ebf04f951df3';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly FitPro Package'] = '2c92c0f856bc84ca0156ebf0151b60f2';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Platinum2 Package'] = '2c92c0f856bc84ca0156ebf02bbe6243';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly EvoSnapFreeLavu88 Package'] = '2c92c0f856bc84ca0156ebf0533c63e2';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Gold3 Package'] = '2c92c0f856bc84ca0156ebf060a06511';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Platinum3 Package'] = '2c92c0f856bc84ca0156ebf064586553';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly FitSilver Package'] = '2c92c0f856bc85290156ebf003e11196';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Silver3 Package'] = '2c92c0f856bc85290156ebf05cf2168c';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly LavuEnterprise Package'] = '2c92c0f856bc85290156ebf06ba81745';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Silver Package'] = '2c92c0f956bc8faa0156ebeff3b75f0d';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Platinum Package'] = '2c92c0f956bc8faa0156ebeffbd35fda';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly LavuGive Package'] = '2c92c0f956bc8faa0156ebf01d326236';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly GoldTrial Package'] = '2c92c0f956bc8faa0156ebf02f796400';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly ProTrial Package'] = '2c92c0f956bc8faa0156ebf0335764ef';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Lite Package'] = '2c92c0f956bc8fb40156ebefffea0bfe';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Hospitality Package'] = '2c92c0f956bc8fb40156ebf020d90d93';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly FitGold Package'] = '2c92c0f956bc8fcb0156ebf011122cb3';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Lavu88 Package'] = '2c92c0f956bc8fcb0156ebf0191d2d35';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Gold2 Package'] = '2c92c0f956bc8fcb0156ebf028272e59';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Lavu88Trial Package'] = '2c92c0f956bc8fcb0156ebf036f62ec9';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Lavu Package'] = '2c92c0f85721ff7c015742ee92452dd2';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly - POS Hosting'] = '2c92c0f85721ff7c015742ee92452dd2';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Yearly Recurring - POS Hosting - Per Terminal'] = '2c92c0f957220b5d015742f083e43e4b';
$this->mappingPackgeNameToProductRatePlanId['LavuToGo']['Lavu Online Ordering - Annual'] = '2c92a0ff5def2a4b015df29d655d2551';
$this->mappingPackgeNameToProductRatePlanId['LavuToGo']['Lavu Online Ordering - Monthly'] = '2c92a0ff5def2a4b015df29b50d81b92';
/*

Then get all ProductRatePlanChargeIds for Hosting:

	SELECT ProductRatePlanId, Id FROM ProductRatePlanCharge WHERE Name = 'Hosting' OR Name = 'Monthly Recurring - POS Hosting - Per Terminal' OR Name = 'Yearly Recurring - POS Hosting - Per Terminal'

You will need reformat the results using a regex search and replace: /^(.*)\t(.*)\t(.*)$/$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['\2'] = '\3';/

And then replace the value at this key

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92c0f956bc8fcb0156ebeff1fb2bbb' AND Name = 'Monthly Lavu Package'

with this value at this key

	SELECT Name, Id FROM ProductRatePlanCharge WHERE ProductId = '2c92c0f956bc8fcb0156ebeff1fb2bbb' AND Name = 'Monthly - POS Hosting'

*/

$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f8567ded630156906e2d1d3f17'] = '2c92c0f8567ded6a01569071787120e5';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f9567df78a0156906264ae466b'] = '2c92c0f8567dedcb0156906dcd9b4430';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8faa0156ebeff3b75f0d'] = '2c92c0f856bc84c90156ebeff5931901';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8faa0156ebf01d326236'] = '2c92c0f856bc84c90156ebf01f0a1bde';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8faa0156ebf0335764ef'] = '2c92c0f856bc84c90156ebf0352b1d24';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84c90156ebf04b8d1d6a'] = '2c92c0f856bc84c90156ebf04d9f1dce';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84ca0156ebf068076567'] = '2c92c0f85721ff7c015742f633603679';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc85290156ebf06ba81745'] = '2c92c0f856bc84c90156ebf06d7e1eab';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8fb40156ebefffea0bfe'] = '2c92c0f856bc84ca0156ebf001c35dd1';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8fcb0156ebf011122cb3'] = '2c92c0f856bc84ca0156ebf012f76081';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84ca0156ebf0151b60f2'] = '2c92c0f856bc84ca0156ebf016fb613d';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8faa0156ebf02f796400'] = '2c92c0f856bc84ca0156ebf0314f6296';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84c90156ebf04f951df3'] = '2c92c0f856bc84ca0156ebf0517263ad';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc85290156ebf003e11196'] = '2c92c0f856bc85290156ebf0092f1219';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8fcb0156ebf0191d2d35'] = '2c92c0f856bc85290156ebf01afe1346';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84c90156ebf024941c73'] = '2c92c0f856bc85290156ebf0265f13e7';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8fcb0156ebf028272e59'] = '2c92c0f856bc85290156ebf029fa1421';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f957220b5d015742f083e43e4b'] = '2c92c0f85721ff7c015742f3687133a2';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f85721ff7c015742ee92452dd2'] = '2c92c0f85721ff7c015742f633603679';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8fb40156ebf020d90d93'] = '2c92c0f956bc8faa0156ebf022c362b4';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8faa0156ebeffbd35fda'] = '2c92c0f956bc8fb40156ebeffdaa0bde';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f956bc8fcb0156ebf036f62ec9'] = '2c92c0f956bc8fb40156ebf038cb0e8b';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84c90156ebf03aa71d68'] = '2c92c0f956bc8fb40156ebf044280ec6';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84ca0156ebf0533c63e2'] = '2c92c0f956bc8fb40156ebf05b030eee';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc85290156ebf05cf2168c'] = '2c92c0f956bc8fb40156ebf05eca1004';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84ca0156ebf060a06511'] = '2c92c0f956bc8fb40156ebf0627010a9';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84ca0156ebf064586553'] = '2c92c0f956bc8fb40156ebf06638112e';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84c90156ebeff77b1919'] = '2c92c0f956bc8fcb0156ebeff9512c0f';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92c0f856bc84ca0156ebf02bbe6243'] = '2c92c0f956bc8fcb0156ebf02d8d2e62';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['LavuToGo']['2c92a0ff5def2a4b015df29d655d2551'] = '2c92a0ff5def2a4b015df29d65692553';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['LavuToGo']['2c92a0ff5def2a4b015df29b50d81b92'] = '2c92a0ff5def2a4b015df29b50f21b94';