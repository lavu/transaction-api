<?php

require_once(dirname(__FILE__). '/../../../../../private_html/ZuoraConfig.php');
require_once(dirname(__FILE__).'/api/API.php');

class ZuoraDataUploader
{
	private $dataname;
	private $zApi;
	private $introw;

	private $zAccount;
	private $zBillToContact;
	private $zSoldToContact;
	private $zPaymentMethod;

	private $zAmendOptions;
	private $zPreviewOptions;
	private $zSubscribeOptions;

	private $zObjBatch;
	private $zSubscriptions;
	private $zInvoiceEvents;
	private $zInvoices;
	private $zInvoiceUpdates;
	private $zInvoiceItemAdjs;
	private $zPayments;
	private $zCancellations;

	private $zAccountId;
	private $zBillToContactId;
	private $zSoldToContactId;
	private $zPaymentMethodId;
	private $zSubscriptionId;
	private $zLicenseSubscriptionId;
	private $zHostingSubscriptionId;
	private $zUpgradeLicenseSubscriptionId;
	private $zInvoiceId;
	private $zInvoiceItemId;
	private $zInvoiceItemChargeAmount;

	private $invoiceid2zuoraid;

	public function __construct($dataname = null)
	{
		$this->initApi();

		// All billing_events AmendRequests have the same sets of AmendOptions, PreviewOptions, SubscribeOptions
		$this->zAmendOptions = new Zuora_AmendOptions();
		$this->zAmendOptions->GenerateInvoice = false;
		$this->zAmendOptions->ProcessPayments = false;
		$this->zPreviewOptions = new Zuora_PreviewOptions();
		$this->zPreviewOptions->EnablePreviewMode = false;
		$this->zSubscribeOptions = new Zuora_SubscribeOptions(false, false);

		if ( !empty($dataname) )
		{
			$this->init($dataname);
		}
	}

	public function init($dataname)
	{
		$this->dataname = $dataname;

		$this->zAccount = null;
		$this->zBillToContact = null;
		$this->zSoldToContact = null;
		$this->zPaymentMethod = null;

		$this->zAccountId = null;
		$this->zBillToContactId = null;
		$this->zSoldToContactId = null;
		$this->zPaymentMethodId = null;
		$this->zSubscriptionId = null;
		$this->zLicenseSubscriptionId = null;
		$this->zHostingSubscriptionId = null;
		$this->zUpgradeLicenseSubscriptionId = null;
		$this->zInvoiceId = null;
		$this->zInvoiceItemId = null;
		$this->zInvoiceItemChargeAmount = null;

		$this->zObjBatch = array();
		$this->invoiceid2zuoraid = array();

		// Setup the row in the integration table, if one doesn't already exist.
		/*$this->introw = $this->getDatabaseRow('billing_zuora_integration');
		if ( empty($this->introw) )
		{
			$this->getDatabaseRow('billing_zuora_integration_insert');
			$this->introw = $this->getDatabaseRow('billing_zuora_integration');
		}*/
	}

	private function initApi()
	{
		if ( $this->zApi === null )
		{
			// Init Zuora API
			$config = new stdClass();
			$config->wsdl = ZuoraConfig::API_WSDL;
			$this->zApi = Zuora_API::getInstance($config);
			$this->zApi->setLocation(ZuoraConfig::API_ENDPOINT);
			$this->zApi->login(ZuoraConfig::API_USERNAME, ZuoraConfig::API_PASSWORD);
		}
	}

	public function getDatabaseRow($query_name, $dbargs = null)
	{
		$dbConn = ConnectionHub::getConn('poslavu');

		$return_single_row = true;
		$return_updated    = false;
		$die_on_zero_rows  = true;
		$die_on_db_errors  = true;

		if ( $dbargs === null )
		{
			$dbargs = array('dbtable' => $query_name, 'dataname' => $this->dataname);
		}

		switch ($query_name)
		{
			case 'billing_zuora_integration':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`billing_zuora_integration` WHERE `dataname` = '[dataname]'";
				break;

			case 'billing_zuora_integration_insert':
				$die_on_zero_rows  = false;
				$sql = "INSERT `poslavu_MAIN_db`.`billing_zuora_integration` (`dataname`, `status`) VALUES ('[dataname]', 'NEW')";
				break;

			case 'billing_zuora_integration_update':
				$return_single_row = false;
				$return_updated    = true;
				$die_on_zero_rows  = false;
				$sql = "UPDATE `poslavu_MAIN_db`.`billing_zuora_integration` SET `[field]` = '[value]' WHERE `dataname` = '[dataname]' LIMIT 1";
				break;

			default:
				die(__METHOD__ ." got unmapped table {$query_name} for dataname {$this->dataname}\n");
				break;
		}

		// Perform lavuquery-esque field substitutions (only supports array-based [fieldname] not [1] subs yet)
		foreach ( $dbargs as $key => $val )
		{
			$sql = str_replace('['.$key.']', $dbConn->escapeString($val), $sql);
		}

		// Query the database
		$result = $dbConn->query($sql);

		if ( $result === false && $die_on_db_errors )
		{
			die("Error with '{$query_name}' query: ". mlavu_dberror() ."\n");
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			die("No {$query_name} row for {$this->dataname}: sql={$sql}\n");
		}
		
		if ( $return_updated )
		{
			return $dbConn->affectedRows();
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}

	private function applyPostUploadLogic($obj_type, $zObj)
	{
		// Post-upload/fetch logic by obj_types - mostly for saving IDs to members
		switch ( $obj_type )
		{
			case 'zAccount':
				$this->zAccount = $zObj;
				$this->zAccountId = $zObj->Id;
				break;

			case 'zBillToContact':
				$this->zBillToContact = $zObj;
				$this->zBillToContactId = $zObj->Id;
				break;

			case 'zSoldToContact':
				$this->zSoldToContact = $zObj;
				$this->zSoldToContactId = $zObj->Id;
				break;

			case 'zPaymentMethod':
				$this->zPaymentMethod = $zObj;
				$this->zPaymentMethodId = $zObj->Id;
				break;

			case 'zAccountUpdate':
				$this->zAccount->Batch = $zObj->Batch;
				$this->zAccount->BillToId = $zObj->BillToId;
				$this->zAccount->DefaultPaymentMethodId = $zObj->PaymentMethodId;
				$this->zAccount->SoldToId = $zObj->SoldToId;
				$this->zAccount->Status = $zObj->Status;
				break;

			default:
				break;

		}

	}

	public function uploadZuoraObjectWithQuery($obj_type, $args = null, $where_clause_args = null)
	{
		$this->logDebug('In '. __METHOD__ ."() with dataname={$this->dataname} obj_type={$obj_type} args=". json_encode($args) ." where_clause_args=". json_encode($where_clause_args));

		// Unserialize Zuora object in integration table and then upload to Zuora
		$zObj = $this->getUnserializedZuoraObject($obj_type);

		// Send serialized arrays to a separate method for processing
		if ( is_array($zObj) )
		{
			return $this->uploadZuoraDataArray($obj_type, $zObj);
		}

		// Before we upload what's in the integration table, see if the object already exists in Zuora via an API query (so we don't create duplicates)
		$zObjFromApi = $this->getZuoraObjectViaApiQuery($obj_type, $where_clause_args, $zObj);

		if ( $zObjFromApi !== null )
		{
			$this->logMsg("...using API version of {$obj_type}");
			$zObj = $zObjFromApi;

			// TO DO : compare $zObj and $zObjFromApi to check for differences
			// TO DO : then perform update (?)
			// TO DO : or delete object in Zuora (?)
		}
		else if ( $zObjFromApi === null )
		{
			$this->logDebug("...using db version of {$obj_type}");

			// Upload to Zuora
			$response = $this->uploadZuoraObject($obj_type, $zObj);

			if ( isset($response->result) && isset($response->result->Success) && $response->result->Success )
			{
				$zObj->Id = $response->result->Id;
			}
			else
			{
				die("...exiting due to upload errors\n");
			}
		}

		// Post-upload/fetch logic by obj_types - mostly for saving IDs to members
		$zObj = $this->applyPostUploadLogic($obj_type, $zObj);

		return $zObj;
	}

	/**
	 * Subscribes batches of psuedo-SubscribeRequest objects
	 */
	public function uploadSubscribeRequests($subscribeRequests)
	{
		try
		{
			$response = $this->zApi->subscribeBatch($subscribeRequests);
		}
		catch ( Exception $e )
		{
			$this->logError($e->getMessage());
		}

		return $response;
	}

	public function uploadZuoraDataArray($obj_array_name, $zObjArray)
	{
		$this->logDebug('In '. __METHOD__ ."() with dataname={$this->dataname} obj_array_name={$obj_array_name} args=". json_encode($args) ." where_clause_args=". json_encode($where_clause_args));
		$this->logDebug('...num of data array elements: '. count($zObjArray));

		// Pre-upload logic by obj_array_name

		switch ( $obj_array_name )
		{
			case 'zSubscriptions':
				// Get any pre-existing Account objects before processing any Subscriptions, since zAccountId is used in querying for pre-existing Subscriptions
				$this->initExistingAccountObjects();
				break;

			case 'zInvoices':
			case 'zInvoiceUpdates':
			case 'zInvoiceItemAdjs':
				$this->getLegacyIdToInvoiceIds();
				return $this->uploadZuoraDataArrayBatch($obj_array_name, $zObjArray);
				break;

			case 'zPayments':
				return $this->uploadZuoraDataArrayBatch($obj_array_name, $zObjArray);
				break;
		}

		// For returning collection of IDs processed
		$zObjIds = array();
		$last_obj_type = '';

		foreach ( $zObjArray as $insert_index => $zObjData )
		{
			$index    = $zObjData['index'];
			$obj_type = $zObjData['obj_type'];
			$zObj     = $zObjData['zObj'];
			$legacyid = isset($zObjData['LegacyID']) ? $zObjData['LegacyID'] : '';

			// Unbloat zObjData so we can pass it and the zObj to uploadZuoraObject() separately
			unset($zObjData['zObj']);

			$this->logDebug("...index={$index} obj_type={$obj_type} zObjData=". json_encode($zObjData));

			switch ( $obj_type )
			{
				case 'zInvoiceItemAdjustment':
					// Need to populate this here because it's used in the API query check and when prepping the zObj for upload
					$this->zInvoiceId = $this->legacyInvoiceIdToZuoraInvoiceId[$legacyid];
					break;
			}

			// Check for existence of Zuora object via API query
			$zObjFromApi = $this->getZuoraObjectViaApiQuery($obj_type, null, $zObj);
			$using_api_obj = false;

			if ( $zObjFromApi !== null )
			{
				$zObj = $zObjFromApi;
				$using_api_obj = true;
				$this->logMsg("...using API {$obj_type}");
			}
			else if ( $zObjFromApi === null )
			{
				$this->logDebug("...uploading integration table {$obj_type}");

				// Upload to Zuora
				$response = $this->uploadZuoraObject($obj_type, $zObj, $zObjData);

				if ( isset($response->result) )  $result = $response->result;
				if ( isset($response->results) ) $result = $response->results;

				if ( isset($result->Success) && $result->Success )
				{
					// Post-upload (no fetch) logic by (array-contained) obj_types - mostly for saving IDs into our
					// object or the zObj for when the zObj gets used below in the Post-upload/fetch logic section

					switch ( $obj_type )
					{
						case 'zLicenseSubscription':
						case 'zHostingSubscription':
							if ( empty($this->zAccountId) )
							{
								$this->zAccountId = $result->AccountId;
								$this->zAccount->Id = $this->zAccountId;
							}
							$zObj->AccountId = $this->zAccountId;

							// We set stuff in zObj (a SubscribeData obj) instead of this->zSubscription since all of that is set in the Post-upload/fetch section below
							$zObj->zSubscription->Id = $result->SubscriptionId;

							// The PaymentMethod created by the subscribe() call isn't mentioned in the Zuora_SubscribeResult object so we have to do an API query to get it.  
							if ( $this->zPaymentMethodId === null )
							{
								$this->zPaymentMethod = $this->getZuoraObjectViaApiQuery('zPaymentMethod', null, $this->zPaymentMethod);
								$this->zPaymentMethodId = $this->zPaymentMethod->Id;
							}
							break;

						case 'zLicenseAmendRequest':
						case 'zHostingAmendRequest':
							$zObj->Id = $result->AmendmentIds;
							break;

						default:
							$zObj->Id = $response->result->Id;
							break;
					}
				}
				else
				{
					die("...exiting due to upload errors\n");
				}
			}

			$return_array = true;

			// Post-upload/fetch logic by (array-contained) obj_types - mostly for saving IDs

			switch ( $obj_type )
			{
				case 'zInvoice':
					$this->zInvoiceId = $zObj->Id;
					$this->legacyInvoiceIdToZuoraInvoiceId[$legacyid] = $zObj->Id;
					break;

				case 'zInvoiceItemCharge':
					$this->zInvoiceItemChargeAmount = $zObj->Amount;
					break;

				case 'zInvoicePayment':
					$this->zInvoicePayments[] = $zObj;
					break;

				case 'zLicenseSubscription':
				case 'zHostingSubscription':
				case 'zHardwareSubscription':  // unsed obj_type
					$zSubscription = ($using_api_obj) ? $zObj : $zObj->zSubscription;
					$this->zSubscription   = $zSubscription;
					$this->zSubscriptionId = $zSubscription->Id;
					$this->logDebug("...this->zSubscriptionId={$this->zSubscriptionId} this->zSubscription=". print_r($this->zSubscription, true));

					// If this is our first Subscrption, we create the main Account objects in the subscribe() call 
					// but their IDs are not returned to us so we have to query for them.  We already have the objects
					// but we overwrite what we have and then set the IDs.
					if ( $index === 0 && $this->zBillToContact === null )
					{
						$this->zBillToContact = $this->getZuoraObjectViaApiQuery('zBillToContact', null, $this->zBillToContact);
						$this->zBillToContactId = $this->zBillToContact->Id;
					}

					if ( $index === 0 && $this->zSoldToContact === null )
					{
						$this->zSoldToContact = $this->getZuoraObjectViaApiQuery('zSoldToContact', null, $this->zSoldToContact);
						$this->zSoldToContactId = $this->zSoldToContact->Id;
					}

					if ( $index === 0 && $this->zPaymentMethod === null )
					{
						$this->zPaymentMethod = $this->getZuoraObjectViaApiQuery('zPaymentMethod', null, $this->zPaymentMethod);
						$this->zPaymentMethodId = $this->zPaymentMethod->Id;
					}
					break;

				case 'zLicenseAmendRequest':
				case 'UpgradeLicenseAmendRequest':  // unsed obj_type
					break;

				default:
					$this->logDebug("...got unmapped obj_type={$obj_type}");
					break;
			}

			$zObjIds[] = $zObj->Id;
		}

		return $zObjIds;
	}

	public function uploadZuoraDataArrayBatch($obj_array_name, $zObjDataArray)
	{
		$this->logDebug('In '. __METHOD__ ."() with dataname={$this->dataname}");

		if ( count($zObjDataArray) === 0 )
		{
			$this->logDebug("...exiting; got empty zObjDataArray");
			return;
		}

		$zObjArray = array();

		foreach ( $zObjDataArray as $zObjData )
		{
			$zObj = $zObjData['zObj'];

			// Upload pre-processing by type
			switch ( $obj_array_name )
			{
				case 'zInvoices':
					$api_action = 'generate';
					$zObj->AccountId = $this->zAccountId;

					// Skip any already-created invoices
					if ( isset($this->getLegacyIdToInvoiceId[$zObj->LegacyID__c]) ) continue;
					break;

				case 'zInvoiceUpdates':
					$api_action = 'update';
					$zObj->AccountId = $this->zAccountId;
					break;

				case 'zInvoiceItemAdjs':
					$api_action = 'create';

					$where_clause = "InvoiceId = '". $this->escapeZoql($this->zInvoiceId) ."' AND ServiceStartDate = '". $this->escapeZoql($zObj->AdjustmentDate) ."' AND ChargeAmount >= ". $this->escapeZoql($zObj->Amount);
					$zInvoiceItem = $this->getZuoraObjectViaApiQuery('zInvoiceItem', $where_clause);
					if ( $zInvoiceItem === null )
					{
						$this->logErrorAndDie("Couldn't find zInvoiceItem with where_clause={$where_clause}");
					}

					$zObj->AdjustmentDate = date('Y-m-d');
					$zObj->Amount = $zInvoiceItem->ChargeAmount;
					$zObj->InvoiceId = $this->zInvoiceId;
					$zObj->SourceId = $zInvoiceItem->Id;
					$zObj->unsetVar('LegacyInvoiceID__c');
					break;

				case 'zPayments':
					$api_action = 'create';
					$zObj->AccountId = $this->zAccountId;
					$zObj->PaymentMethodId = $this->zPaymentMethodId;  // TO DO : make this Other PaymentMethod?
					foreach ( $zObj->InvoicePaymentData as $zInvoicePaymentData )
					{
						$zInvoicePaymentData->InvoiceId = $this->zInvoiceId;
					}
					break;

				default:
					$api_action = 'create';
					break;
			}

			$this->logDebug("...api_action={$api_action} zObj=". print_r($zObj, true));

			$zObjArray[] = $zObj;
		}

		$this->logMsg("...dataname={$this->dataname} {$api_action}'ing an array of ". count($zObjArray) ." {$obj_array_name}");

		// API limits batching is limiting to 50 zObjs
		$recs_per_batch = 50;
		$total_recs = count($zObjArray);
		$total_batches = ceil($total_recs / $recs_per_batch);
		$this->logDebug("...recs_per_batch={$recs_per_batch} total_batches={$total_batches}");

		for ( $batch_num = 0; $batch_num < $total_batches; $batch_num++ )
		{
			$batch_start_time = microtime(true);
			$starting_rec_num = $batch_num * $recs_per_batch;
			$zObjArrayBatch = array_slice($zObjArray, $starting_rec_num, $recs_per_batch);  // the last arg is key since it keeps the integer indexes the same as the the full source array

			$this->logDebug("...batch_num={$batch_num} starting_rec_num={$starting_rec_num} ending_rec_num=". ($starting_rec_num + count($zObjArrayBatch)) ." batch_recs_count=". count($zObjArrayBatch));
			//$this->logDebug("...batch {$batch_num} zObjArrayBatch=". print_r($zObjArrayBatch, true));

			try
			{
				$response = $this->zApi->$api_action( $zObjArrayBatch );
				$this->logDebug("...response=". print_r($response, true));
			}
			catch ( Exception $e )
			{
				// TO DO : update int table with $e->getMessage()
				$this->logError($e->getMessage());
			}

			$this->logMsg("...dataname={$this->dataname} batch {$batch_num} took ". (microtime(true) - $batch_start_time) ." seconds");
		}

		// Upload post-processing by type (all batches)
		switch ( $obj_array_name )
		{
			case 'zInvoices':
				$this->getLegacyIdToInvoiceIds();
				$this->logDebug("...zInvoiceId={$this->zInvoiceId} legacyIdToInvoiceId=". print_r($this->legacyIdToInvoiceId, true));
				$this->postInvoice($this->zInvoiceId);
				break;

			default:
				break;
		}

	}

	public function postInvoice($zInvoiceId)
	{
		$zInvoice = new Zuora_Invoice();
		$zInvoice->Id = $zInvoiceId;
		$zInvoice->Status = 'Posted';

		$this->uploadZuoraObject('zInvoiceUpdate', $zInvoice);
	}

	public function getUnserializedZuoraObject($obj_type, $args = null)
	{
		$this->logDebug('In '. __METHOD__ ."() for dataname={$this->dataname} obj_type={$obj_type}...");

		if ( !isset($this->introw[$obj_type]) )
		{
			$this->logError("...ERROR -- no value in integration table for {$obj_type}: introw=". print_r($this->introw));
			return null;
		}

		$serializedObj = $this->introw[$obj_type];

		/*// Field substitutions (default is true)
		$do_field_subs = isset($args['do_field_subs']) ? $args['do_field_subs'] : true;
		if ( $do_field_subs )
		{
			$this->doAllFieldSubstitutions($serializedObj);
		}*/

		$zObj = unserialize($serializedObj);

		if ( !is_array($zObj) )
		{
			//$this->logDebug('...unserialized zObj='. print_r($zObj, true));
		}

		return $zObj;
	}

	public function getZuoraObjectViaApiQuery($obj_type, $where_clause = null, $zObj = null)
	{
		$this->logDebug("In ". __METHOD__ ."() for dataname={$this->dataname} obj_type={$obj_type} where_clause_args=". json_encode($where_clause_args));

		$return_single_row  = true;
		$die_on_zero_rows   = false;
		$die_on_api_errors  = false;

		switch ( $obj_type )
		{
			case 'zAccount':
				$zObjectName = 'Account';
				$select_fields = 'Id, Name, AllowInvoiceEdit, AutoPay, BillCycleDay, BillToId, CrmId, Currency, DefaultPaymentMethodId, PaymentTerm, SoldToId, Status';
				$where_clause_args = array('Dataname__c' => $this->dataname);
				break;

			case 'zBillToContact':
				$zObjectName = 'Contact';
				$select_fields = 'Id, FirstName, LastName, Address1, Address2, City, State, PostalCode, Country, WorkPhone, WorkEmail, CreatedDate, AccountId';
				$where_clause_args = array('FirstName' => $zObj->FirstName, 'LastName' => $zObj->LastName, 'AccountId' => $this->zAccountId);
				break;

			case 'zSoldToContact':
				$zObjectName = 'Contact';
				$select_fields = 'Id, FirstName, LastName, Address1, Address2, City, State, PostalCode, Country, WorkPhone, WorkEmail, CreatedDate, AccountId';
				$where_clause_args = null;
				break;

			case 'zLicenseSubscription':
			case 'zUpgradeLicenseSubscription':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType';
				$where_clause_args = array('AccountId' => $this->zAccountId, 'TermStartDate' => $zObj->zSubscription->TermStartDate, 'TermType' => 'TERMED');
				break;

			case 'zHostingSubscription':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType';
				$where_clause_args = array('AccountId' => $this->zAccountId, 'TermStartDate' => $zObj->zSubscription->TermStartDate, 'TermType' => 'EVERGREEN');
				break;

			case 'zInvoice':
				$zObjectName = 'Invoice';
				$select_fields = 'Id, AccountId, InvoiceDate, TargetDate, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c';
				$where_clause_args = array('AccountId' => $this->zAccountId, 'LegacyID__c' => $zObj->LegacyID__c);
				break;

			case 'zInvoiceUpdate':
				$zObjectName = 'Invoice';
				$select_fields = 'Id, AccountId, InvoiceDate, TargetDate, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c';
				$where_clause_args = array('AccountId' => $this->zAccountId, 'LegacyID__c' => $zObj->LegacyID__c, 'Status' => 'Posted');
				break;

			case 'zInvoiceItem':
				$zObjectName = 'InvoiceItem';
				$select_fields = 'Id, ChargeAmount, ChargeType, InvoiceId';
				// Needs custom where_clause because it uses invoice-specific params (ChargeType, LegacyInvoiceID__c)
				break;

			case 'zInvoiceItemAdjustment':
				$zObjectName = 'InvoiceItemAdjustment';
				$select_fields = 'Id, AccountId, AdjustmentDate, Amount, Comment, InvoiceId, InvoiceItemName, InvoiceNumber, LegacyInvoiceID__c, SourceId, SourceType, Type';
				$where_clause_args = array('AccountId' => $this->zAccountId, 'InvoiceId' => $this->zInvoiceId);  // hopefully it's not bad I removed LegacyInvoiceID__c from where clause
				break;

			case 'zPayment':
				$zObjectName = 'Payment';
				$select_fields = 'Id, AccountId, Amount';
				$where_clause_args = array('AccountId' => $this->zAccountId, 'LegacyID__c' => $zObj->LegacyID__c);
				break;

			case 'zPaymentMethod':
				$zObjectName = 'PaymentMethod';
				$select_fields = 'Id, AccountId, Type, UseDefaultRetryRule';
				$where_clause_args = array('AccountId' => $this->zAccountId);
				break;

			case 'zLicenseAmendRequest':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType';
				$where_clause_args = array('AccountId' => $this->zAccountId, 'TermStartDate' => $this->zSubscription->TermStartDate, 'TermType' => 'TERMED', 'Status' => 'Cancelled');  // Could be Status = Expired, too, but not sure if that's correct (TO DO : add support for ORs)
				break;

			case 'zHostingAmendRequest':
				$zObjectName = 'Subscription';
				$select_fields = 'Id, Name, AccountId, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType';
				$where_clause_args = array('AccountId' => $this->zAccountId, 'TermStartDate' => $this->zSubscription->TermStartDate, 'TermType' => 'EVERGREEN', 'Status' => 'Cancelled');  // Could be Status = Expired, too, but not sure if that's correct (TO DO : add support for ORs)
				break;

			default:
				$this->logDebug("...got unmapped obj_type={$obj_type}");
				break;
		}

		// Empty check allows custom where clauses to be specified in the obj_type switch statement
		if ( empty($where_clause) )
		{
			if ( !empty($where_clause_args) )
			{
				$where_clause = $this->getWhereClauseFromArray($where_clause_args);
			}
			else
			{
				$this->logWarning("...skipping API query for obj_type={$obj_type}");
				return null;
			}
		}

		// Empty check allows custom zoql queries to be specified in the obj_type switch statement
		if ( empty($zoql) )
		{
			$zoql = "SELECT {$select_fields} FROM {$zObjectName} WHERE {$where_clause}";
		}

		try
		{
			$response = $this->zApi->query($zoql);
		}
		catch ( Exception $e )
		{
			die($e->getMessage() ."\n");
		}

		$exists = (isset($response->result) && isset($response->result->size) && isset($response->result->done) && $response->result->size > 0 && $response->result->done == '1');
		$this->logDebug("...exists={$exists}");
		$this->logDebug("...size={$response->result->size}");

		// Tie into the Zuora API's SoapClient object's debug settings
		if ( $zApi->_client->printRequest )
		{
			$this->logDebug("API QUERY RESPONSE XML ----------------------------------------------------------". print_r($response, true));
		}

		if ( $exists )
		{
			if ( $response->result->size == 1 )
			{
				$zObjApi = $response->result->records;
			}
			// TO DO : implement $die_on_zero_rows block
			else
			{
				if ( $return_single_row )
				{
					$zObjApi = $response->result->records[0];
					$this->logWarning("Multiple {$zObjectName}s returned for dataname={$this->dataname}; using first one ({$zObjApi->Id})");
				}
				else
				{
					$zObjApi = $response->result->records;
				}
			}
		}
		// TO DO: implement $die_on_api_errors block

		//$this->logDebug("...zObjApi=". print_r($zObjApi, true));

		return $zObjApi;
	}

	public function getLegacyIdToInvoiceIds()
	{
		$select_fields = 'Id, InvoiceNumber, LegacyID__c';
		$zObjectName = 'Invoice';
		$where_clause_args = array('AccountId' => $this->zAccountId);
		$where_clause = $this->getWhereClauseFromArray($where_clause_args);
		$zoql = "SELECT {$select_fields} FROM {$zObjectName} WHERE {$where_clause}";

		try
		{
			$response = $this->zApi->query($zoql);
		}
		catch ( Exception $e )
		{
			die($e->getMessage() ."\n");
		}

		$exists = (isset($response->result) && isset($response->result->size) && isset($response->result->done) && $response->result->size > 0 && $response->result->done == '1');
		$this->logDebug("...exists={$exists}");
		$this->logDebug("...size={$response->result->size}");

		// Tie into the Zuora API's SoapClient object's debug settings
		if ( $zApi->_client->printRequest )
		{
			$this->logDebug("API QUERY RESPONSE XML ----------------------------------------------------------". print_r($response, true));
		}

		if ( $exists )
		{
			if ( $response->result->size == 1 )
			{
				$records = array($response->result->records);
			}
			else
			{
				$records = $response->result-records;
			}

			$this->logDebug("...processing results");

			foreach ( $records as $zObj )
			{
				$this->logDebug("...zObj=". print_r($zObj, true));
				$this->getLegacyIdToInvoiceId[$zObj->LegacyID__c] = $zObj->Id;
				$this->zInvoiceId = $zObj->Id;
				$this->zInvoiceNumber = $zObj->InvoiceNumber;
			}
		}
	}

	public function uploadZuoraObject($obj_type, $zObj, $zObjData = null)
	{
		$this->logDebug('In '. __METHOD__ ."() for dataname={$this->dataname} obj_type={$obj_type}");

		// Pre-upload data tweaks for each obj_type

		switch ( $obj_type )
		{			
			case 'zAccountUpdate':
				$api_action = 'update';
				$zObj->Id = $this->zAccountId;
				$zObj->BillToId = $this->zBillToContactId;
				$zObj->DefaultPaymentMethodId = $this->zPaymentMethodId;
				$zObj->SoldToId = $this->zSoldToContactId;
				$unset_ok = $zObj->unsetVar('AccountNumber');
				break;

			case 'zBillToContact':
			case 'zSoldToContact':
				$api_action = 'create';
				//$zObj->AccountId = $this->zAccountId;
				break;

			case 'zPaymentMethod':
				$api_action = 'create';
				$zObj->AccountId = $this->zAccountId;
				break;

			case 'zPaymentMethodUpdate':
				$api_action = 'update';
				$zObj->Id = $this->zPaymentMethodId;
				break;

			case 'zInvoice':
				$api_action = 'create';
				$zObj->AccountId = $this->zAccountId;
				break;

			case 'zInvoiceUpdate':
				$api_action = 'update';
				$zObj->Id = $this->zInvoiceId;
				break;

			case 'zInvoiceItemAdjustment':
				$api_action = 'create';
				$zInvoiceItem = $this->getZuoraObjectViaApiQuery('zInvoiceItem', $this->getWhereClauseFromArray( array('InvoiceId' => $this->zInvoiceId, 'ChargeType' => $zObjData['ChargeType'], 'ServiceStartDate' => $zObjData['ServiceStartDate']) ));
				$zObj->SourceId = $zInvoiceItem->Id;
				$zObj->Amount = $zInvoiceItem->ChargeAmount;
				$zObj->InvoiceId = $this->zInvoiceId;
				break;

			case 'zLicenseSubscription':
			case 'zHostingSubscription':
			case 'zHardwareSubscription':
				$api_action = 'subscribe';
				$this->zAccount->Batch = 'Batch1';
				$zObj->zSubscription->AccountId = $this->zAccountId;
				$zObj->zSubscription->InvoiceOwnerId = $this->zAccountId;
				// Need to unserialize the main Account objects if we're doing the first Subscription for the account.
				if ($this->zBillToContact === null)
				{
					$this->zBillToContact = $this->getUnserializedZuoraObject('zBillToContact');
					$this->zBillToContact->unsetVar('AccountId');
				}
				if ($this->zSoldToContact === null)
				{
					$this->zSoldToContact = $this->getUnserializedZuoraObject('zSoldToContact');
					$this->zSoldToContact->unsetVar('AccountId');
				}
				if ($this->zPaymentMethod === null)
				{
					$this->zPaymentMethod = $this->getUnserializedZuoraObject('zPaymentMethod');
					$this->zPaymentMethod->unsetVar('AccountId');
				}
				break;

			case 'zLicenseAmendRequest':
			case 'zHostingAmendRequest':
			case 'zHardwareAmendRequest':
				$api_action = 'amend';
				$zObj->Amendments->SubscriptionId = $this->zSubscriptionId;
				break;

			case 'zPayment':
				$api_action = 'create';
				$zObj->AccountId = $this->zAccountId;
				$zObj->PaymentMethodId = $this->zPaymentMethodId;
				foreach ( $zObj->InvoicePaymentData as $zInvoicePayment )
				{
					list($field_sub_name, $invoiceid) = explode(':', $zInvoicePayment->InvoiceId);
					$invoiceid = substr($invoiceid, 0, strlen($invoiceid) - 2);  // strip ]=
					$zInvoicePayment->InvoiceId = $this->legacyInvoiceIdToZuoraInvoiceId[$invoiceid];
					$this->logDebug("...field_sub_name={$field_sub_name} invoiceid={$invoiceid} legacyInvoiceIdToZuoraInvoiceId=". $this->legacyInvoiceIdToZuoraInvoiceId[$invoiceid]);  //debug
				}
				break;

			// TO : find and unset all unwriteable columns for each obj_type (use Zoql select clauses)

			default:
				$api_action = 'create';
				break;
		}

		//$this->logDebug("...api_action={$api_action} zObj=". print_r($zObj, true));

		if ( $api_action == 'subscribe' )
		{
			$this->logDebug("...zAccount=". print_r($this->zAccount, true));
			$this->logDebug("...zBillToContact=". print_r($this->zBillToContact, true));
			$this->logDebug("...zSoldToContact=". print_r($this->zSoldToContact, true));
			$this->logDebug("...zPaymentMethod=". print_r($this->zPaymentMethod, true));
			$this->logDebug("...zSubscribeData=". print_r($zObj, true));

			try
			{
				$response = $this->zApi->subscribe($this->zAccount, $zObj, $this->zBillToContact, $this->zPaymentMethod, $this->zSubscribeOptions, $this->zSoldToContact);
			}
			catch ( Exception $e )
			{
				$this->logError($e->getMessage());  // TO DO : update int table with $e->getMessage()
			}
		}
		else
		{
			try
			{
				$response = $this->zApi->$api_action( array($zObj) );
			}
			catch ( Exception $e )
			{
				$this->logError($e->getMessage());  // TO DO : update int table with $e->getMessage()
			}
		}

		$this->logDebug("...response=". print_r($response, true));

		return $response;
	}

	public function doAllFieldSubstitutions($str)
	{
		if ( preg_match_all('/=\[(.+?)\]=/', $str_before_subs, $regex_matches) )
		{
			$fieldsub_tokens = $regex_matches[1];
			$max_i = count($matchedfields);  // outside of loop condition so it isn't calculated every time
			$num_fieldsubs = $max_i - 1;
			$this->logDebug("...found {$num_fieldsubs} field substitution(s)");

			for ( $i = 0; $i < $max_i; $i++ )  // starts on 1 because index 0 contains the entire field sub sequence
			{
				$str = $this->doFieldSubstitution($fieldsub_tokens[$i], $str);
			}
		}

		return $str;
	}

	public function doFieldSubstitution($field, $str_before_sub)
	{
		$this->logDebug('In '. __METHOD__ .'() with field='. $field);

		// Some field names contained colon delimited event offsets
		// (ex. zInvoice:4, where 4 is the invoicing event offset)
		$offset = '';
		if ( strstr($field, ':') !== false )
		{
			// Decided not to use offset but kept code to capture it since it removes it from $field
			list($field, $offset) = explode(':', $field);
		}

		$fieldsub = '=['. $field . $offset .']=';
		$fieldval = $this->$field;

		$this->logDebug("...{$field}={$fieldval}");

		$fieldsub_strlen = strlen($fieldsub);
		$fieldval_strlen = strlen($fieldval);

		$serialized_fieldsub = 's:'. $fieldsub_strlen .':"'. $fieldsub .'"';
		$serialized_fieldval = 's:'. $fieldval_strlen .':"'. $fieldval .'"';

		$str_after_sub = str_replace($serialized_fieldsub, $serialized_fieldval, $str_before_sub);

		return $str_after_sub;
	}



	/**
	 * Private Methods
	 */

	private function logMsg($msg)
	{
		echo $msg ."\n";
	}

	private function logWarning($msg)
	{
		$this->logMsg('WARNING: '. $msg);
	}

	private function logDebug($msg)
	{
		//if (SANDBOX) $this->logMsg('DEBUG: '. $msg);
		$this->logMsg('DEBUG: '. $msg);
	}

	private function logError($msg)
	{
		error_log(__FILE__ .' got error: '. $msg);
		$this->logMsg('ERROR: '. $msg);
	}

	private function logErrorAndDie($msg)
	{
		error_log(__FILE__ .' got error in '. __METHOD__ .'(): '. $msg);
		$this->logMsg('ERROR: '. $msg);
		die($msg ."\n");
	}

	private function escapeZoql($zoql_fragment)
	{
		return str_replace("'", "\\'", $zoql_fragment);
	}

	private function getWhereClauseFromArray($where_clause_args)
	{
		$this->logDebug('In '. __METHOD__ .'()');

		$where_clause = '';
		foreach ( $where_clause_args as $key => $val )
		{
			$where_clause .= empty($where_clause) ? "{$key} = '". $this->escapeZoql($val) ."'" : " AND {$key} = '". $this->escapeZoql($val) ."'";
		}

		$this->logDebug("...where_clause={$where_clause}");

		return $where_clause;
	}

}
