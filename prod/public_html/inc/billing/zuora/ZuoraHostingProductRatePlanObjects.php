<?php

/*

First get the ProductId for the Hosting product:

	SELECT Name, Id FROM Product WHERE Name = 'Lavu POS Hosting'

Then use ProductId to get the list of Package Name to ProductRatePlanId mappings:

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92a0fd56c0191d0156c9f68b141fbc'

You will need reformat the results using a regex search and replace: /^(.*)\t(.*)\t(.*)$/$this->mappingPackgeNameToProductRatePlanId['Hosting']['\2'] = '\3';/

And then you need to replace the value at ['Hosting']['Monthly Lavu Package'] - given by this query:

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92a0fd56c0191d0156c9f68b141fbc' AND Name = 'Monthly Lavu Package'

with this value

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92a0fd56c0191d0156c9f68b141fbc' AND Name = 'Monthly - POS Hosting'

*/

$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Silver Package'] = '2c92a0fc56db4ddf0156ecd738ff5f5c';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Platinum Package'] = '2c92a0fc56db4ddf0156ecf0679056dd';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly MercuryGold Package'] = '2c92a0fc56db4ddf0156ecf0a81c5828';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Gold Package'] = '2c92a0fd56c018c30156ecf0657b5b6a';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Lavu88 Package'] = '2c92a0fd56c018c30156ecf076625bfc';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly LavuGive Package'] = '2c92a0fd56c018c30156ecf077e05bfe';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Platinum2 Package'] = '2c92a0fd56c018c30156ecf098495ca1';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly MercuryFreeLavu88 Package'] = '2c92a0fd56c018c30156ecf0a6a65ca4';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Silver3 Package'] = '2c92a0fd56c018c30156ecf0b0405dae';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly GoldTrial Package'] = '2c92a0fd56c018c50156ecf099d53e90';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly MercuryPro Package'] = '2c92a0fd56c018c50156ecf0ad7b3f85';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly FitGold Package'] = '2c92a0fd56c0191d0156ecf072cb1469';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly EvoSnapFreeLavu88 Package'] = '2c92a0fd56c0191d0156ecf0aedb161e';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Lite Package'] = '2c92a0fe56c027860156ecf0698813d7';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly FitPro Package'] = '2c92a0fe56c027860156ecf07416145d';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Platinum3 Package'] = '2c92a0fe56c027860156ecf0b2e5178b';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly FitSilver Package'] = '2c92a0ff56c027830156ecf0710c0462';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Silver2 Package'] = '2c92a0ff56c027830156ecf084680485';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Gold2 Package'] = '2c92a0ff56c027830156ecf09340048f';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly ProTrial Package'] = '2c92a0ff56c027830156ecf09b2004c7';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Lavu88Trial Package'] = '2c92a0ff56c027830156ecf09c6104c8';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly LavuEnterprise Package'] = '2c92a0ff56c027830156ecf0b57605b5';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Hospitality Package'] = '2c92a0ff56ec60c70156ecf079644599';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Gold3 Package'] = '2c92a0ff56ec60c70156ecf0b19546ff';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly Lavu Package'] = '2c92a0fe5c3e2c7f015c45f23129424c';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Monthly - POS Hosting'] = '2c92a0fd56c0191d0156c9f6ec0e265c';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Yearly Recurring - POS Hosting - Per Terminal'] = '2c92a0fc56c018c10156c9f9d0e551bd';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Lavu - Monthly Hosting'] = '2c92a0fe5c3e2c7f015c45f23129424c';
$this->mappingPackgeNameToProductRatePlanId['Hosting']['Lavu - Annual Hosting'] = '2c92a0fe5c3e2c7f015c45f7242a7205';
$this->mappingPackgeNameToProductRatePlanId['LavuToGo']['Lavu Online Ordering - Annual'] = '2c92a0ff5def2a4b015df29d655d2551';
$this->mappingPackgeNameToProductRatePlanId['LavuToGo']['Lavu Online Ordering - Monthly'] = '2c92a0ff5def2a4b015df29b50d81b92';

/*

Then get all ProductRatePlanChargeIds for Licenses:

	SELECT ProductRatePlanId, Id FROM ProductRatePlanCharge WHERE Name = 'Hosting' OR Name = 'Monthly Recurring - POS Hosting - Per Terminal' OR Name = 'Yearly Recurring - POS Hosting - Per Terminal'

You will need reformat the results using a regex search and replace: /^(.*)\t(.*)\t(.*)$/$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['\2'] = '\3';/

And then replace the value at this key

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92a0fd56c0191d0156c9f68b141fbc' AND Name = 'Monthly Lavu Package'

with this value at this key

	SELECT Name, Id FROM ProductRatePlan WHERE ProductId = '2c92a0fd56c0191d0156c9f68b141fbc' AND Name = 'Monthly - POS Hosting'

*/

$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c0191d0156c9f6ec0e265c'] = '2c92a0fc56c018c10156c9f956fa4d3a';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c018c30156ecf076625bfc'] = '2c92a0fc56db4ddf0156ecf07718576e';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c0191d0156ecf072cb1469'] = '2c92a0fd56c018c30156ecf0736f5bf9';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56c027830156ecf084680485'] = '2c92a0fd56c018c30156ecf088745c2b';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c018c50156ecf0ad7b3f85'] = '2c92a0fd56c018c30156ecf0ae2f5d24';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56ec60c70156ecf079644599'] = '2c92a0fd56c018c50156ecf083c93dbd';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c018c30156ecf098495ca1'] = '2c92a0fd56c018c50156ecf098ef3e7f';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56c027830156ecf09b2004c7'] = '2c92a0fd56c018c50156ecf09bc43ec5';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fe56c027860156ecf0698813d7'] = '2c92a0fd56c0191d0156ecf06d51144a';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56c027830156ecf0710c0462'] = '2c92a0fd56c0191d0156ecf0722a1466';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c018c30156ecf077e05bfe'] = '2c92a0fd56c0191d0156ecf078c014a6';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c018c50156ecf099d53e90'] = '2c92a0fd56c0191d0156ecf09a7f15d3';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56ec60c70156ecf0b4354708'] = '2c92a0fc56c018c10156c9f956fa4d3a';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fe56c027860156ecf07416145d'] = '2c92a0fe56c027860156ecf074bf1462';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56c027830156ecf09c6104c8'] = '2c92a0fe56c027860156ecf0a5eb16a6';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c0191d0156ecf0aedb161e'] = '2c92a0fe56c027860156ecf0af891787';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56c027830156ecf0b57605b5'] = '2c92a0fe56c027860156ecf0b627178d';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fc56c018c10156c9f9d0e551bd'] = '2c92a0ff56c027640156c9fbd5b64e5c';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c018c30156ecf0b0405dae'] = '2c92a0ff56c0277e0156ecf0b0df09f8';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fc56db4ddf0156ecf0a81c5828'] = '2c92a0ff56c027830156ecf0a8ee058e';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fc56db4ddf0156ecd738ff5f5c'] = '2c92a0ff56ec60c70156ecf062ec4565';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c018c30156ecf0657b5b6a'] = '2c92a0ff56ec60c70156ecf066a4458b';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fc56db4ddf0156ecf0679056dd'] = '2c92a0ff56ec60c70156ecf068a2458f';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56c027830156ecf09340048f'] = '2c92a0ff56ec60c70156ecf097a045de';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fd56c018c30156ecf0a6a65ca4'] = '2c92a0ff56ec60c70156ecf0a75746e0';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0ff56ec60c70156ecf0b19546ff'] = '2c92a0ff56ec60c70156ecf0b23f4701';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fe56c027860156ecf0b2e5178b'] = '2c92a0ff56ec60c70156ecf0b3934705';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fe5c3e2c7f015c45f23129424c'] = '2c92a0ff5c3e3f1f015c45f65f600de6';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['Hosting']['2c92a0fe5c3e2c7f015c45f7242a7205'] = '2c92a0ff5c3e3f25015c45fc8cf279c9';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['LavuToGo']['2c92a0ff5def2a4b015df29d655d2551'] = '2c92a0ff5def2a4b015df29d65692553';
$this->mappingProductRatePlanIdToProductRatePlanChargeId['LavuToGo']['2c92a0ff5def2a4b015df29b50d81b92'] = '2c92a0ff5def2a4b015df29b50f21b94';

