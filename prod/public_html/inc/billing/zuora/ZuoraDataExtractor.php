<?php

require_once(dirname(__FILE__).'/../../../admin/cp/resources/core_functions.php');
require_once(dirname(__FILE__).'/../../../admin/sa_cp/billing/package_levels_object.php');
require_once(dirname(__FILE__).'/../../../admin/sa_cp/billing/procareas/procpay_functions.php');
require_once(dirname(__FILE__).'/../../../../private_html/ZuoraConfig.php');
require_once(dirname(__FILE__).'/api/API.php');
require_once(dirname(__FILE__).'/../BillingChangeDetector.php');

date_default_timezone_set('America/Denver');

class ZuoraDataExtractor
{
	public $dataname;

	public $restaurant;
	public $signup;
	public $payment_status;
	public $location;

	public $o_package_container;
	public $package_level;
	public $package_name;

	public $setup_props;
	public $invoice_day_of_month;
	public $last_hosting_invoice;
	public $last_payment;
	public $pmtinfo;

	private $zBillToContact;
	private $zAmendOptions;
	private $zPreviewOptions;

	private $zSubscriptions;
	private $zInvoicesEvents;
	private $zInvoices;
	private $zInvoiceUpdates;
	private $zInvoiceItemAdjs;
	private $zPayments;
	private $zCancellations;

	private $legacy2zuora_invoiceids;
	private $mappingPackgeNameToProductRatePlanId;
	private $mappingProductRatePlanIdToProductRatePlanChargeId;

	/**
	 * Public Methods
	 */

	public function __construct($dataname = null)
	{
		// All billing_events AmendRequests have the same sets of AmendOptions
		$this->zAmendOptions = new Zuora_AmendOptions();
		$this->zAmendOptions->GenerateInvoice = false;
		$this->zAmendOptions->ProcessPayments = false;

		// All billing_events AmendRequests have the same sets of PreviewOptions
		$this->zPreviewOptions = new Zuora_PreviewOptions();
		$this->zPreviewOptions->EnablePreviewMode = false;

		$this->mappingPackgeNameToProductRatePlanId = array();
		$this->mappingProductRatePlanIdToProductRatePlanChargeId = array();
		$filenameZuoraHostingProductRatePlanObjects = ZuoraConfig::SANDBOX ? 'ZuoraHostingProductRatePlanObjects.sandbox.php' : 'ZuoraHostingProductRatePlanObjects.prod.php';
		include dirname(__FILE__) .'/'. $filenameZuoraHostingProductRatePlanObjects;

		if ( $dataname !== null )
		{
			$this->init($dataname);
		}
	}

	public function init($dataname, $restaurant = null, $signup = null, $payment_status = null, $location = null)
	{
		global $dbConn;

		$this->dataname = $dataname;
		$this->restaurant     = $this->getDatabaseRow('restaurants');
		$this->signup         = $this->getDatabaseRow('signups');
		$this->payment_status = $this->getDatabaseRow('payment_status');
		$this->location       = $this->getDatabaseRow('locations');

		$this->o_package_container = new package_container();

		$this->restaurantid  = $this->restaurant['id'];
		$this->package_level = (int) $this->signup['package'];
		$this->package_name  = $this->o_package_container->get_plain_name_by_level($this->package_level);

		// Populating this key used by get_setup_props() to avoid PHP warnings and so we can use $restaurant as the 1st arg
		$this->restaurant['signup_source'] = '';

		$this->setup_props = get_setup_props($this->restaurant, $this->package_level, $this->signup['status']);
		$this->invoice_day_of_month = null;
		$this->last_hosting_invoice = null;

		$last_payment  = $this->getDatabaseRow('last_payment', array('dataname' => $this->dataname, 'restaurantid' => $this->restaurantid));
		$this->pmtinfo = $this->getPaymentInfoFromPaymentResponse($last_payment);

		$this->zBillToContact  = null;

		$this->zSubscriptions     = array();
		$this->zInvoiceEvents     = array();
		$this->zInvoices          = array();
		$this->zInvoiceUpdates    = array();
		$this->zInvoiceItemAdjs   = array();
		$this->zPayments          = array();
		$this->zCancellations     = array();
		$this->legacy2zuora_invoiceids = array();
	}

	public function getAccount($args = null)
	{
		$zObj = new Zuora_Account();
		$zObj->Name = (empty($this->restaurant['company_name']) || strlen($this->restaurant['company_name']) > 40 || stristr($this->restaurant['company_name'], 'Tax ID') !== false) ? $this->signup['company'] : $this->restaurant['company_name'];
		$zObj->AccountCreated__c = strstr($this->restaurant['created'], ' ', true);;
		///$zObj->AccountNumber = $this->restaurantid;
		$zObj->AccountSource__c = $this->getAccountSource();
		$zObj->AllowInvoiceEdit = 1;
		$zObj->AutoPay = 0;
		$zObj->Batch = 'Batch1';
		$zObj->BillCycleDay = $this->getInvoiceDayOfMonth();
		$zObj->CrmId = $this->signup['salesforce_id'];
		$zObj->Currency = 'USD';
		$zObj->CustomerAgreementSigned__c = empty($this->payment_status['annual_agreement']) ? 'No' : 'Yes';
		$zObj->Dataname__c = $this->dataname;
		$zObj->DemoAccount__c = $this->restaurant['demo'];
		$zObj->DevAccount__c = $this->restaurant['dev'];
		$zObj->DisabledAccount__c = $this->restaurant['disabled'];
		$zObj->DistroCode__c = $this->restaurant['distro_code'];
		$zObj->EffectiveStartDate = $this->restaurant['created'];
		$zObj->EffectiveEndDate = '';
		$zObj->Gateway__c = $this->location['gateway'];
		$zObj->iPodLimit__c = !empty($this->payment_status['custom_max_ipods']) ? $this->payment_status['custom_max_ipods'] : $this->o_package_container->get_ipod_limt_by_attribute('level', $this->package_level);
		$zObj->iPadLimit__c = !empty($this->payment_status['custom_max_ipads']) ? $this->payment_status['custom_max_ipads'] : $this->o_package_container->get_ipad_limt_by_attribute('level', $this->package_level);
		$zObj->LastActivity__c = strstr($this->restaurant['last_activity'], ' ', true);
		$zObj->LLS__c = ( ($this->location['use_net_path'] == '1' || $this->location['use_net_path'] == '2') && strpos($this->location['net_path'], 'poslavu.com') === false ) ? 'Yes' : 'No';
		$zObj->PackageName__c = $this->package_name;
		$zObj->PackageLevel__c = $this->package_level;
		$zObj->PaymentTerm = 'Due Upon Receipt';
		$zObj->POSData__c = (strstr($this->restaurant['notes'], 'AUTO-DELETED') !== false) ? 'Deleted' : 'Yes';  // TO DO  // Yes|No|Archived|Deleted
		$zObj->RestaurantID__c = $this->restaurantid;
		$zObj->SalesforceAccountID__c = $this->signup['salesforce_id'];
		$zObj->SalesforceOpportunityID__c = $this->signup['salesforce_opportunity_id'];
		$zObj->SignupID__c = $this->signup['id'];
		$zObj->Status = isset($args['Status']) ? $args['Status'] : 'Draft';

		return $zObj;
	}

	public function getAccountUpdate($args = null)
	{
		$zObj = new Zuora_Account();
		$zObj->Id = '=[zAccountId]=';
		//$zObj->AccountNumber = $this->restaurantid;  // AccountNumber kept getting put in the object so we have to unset it before upload
		$zObj->Batch = 'Batch1';
		$zObj->BillToId = '=[zBillToContactId]=';
		$zObj->DefaultPaymentMethodId = '=[zPaymentMethodId]=';
		$zObj->SoldToId = '=[zSoldToContactId]=';
		$zObj->Status = $args['event_status'];

		return $zObj;
	}

	public function getBillToContact($args = null)
	{
		// Fall back to using info from last payment
		if ( $args === null )
		{
			if (!empty($this->pmtinfo['x_first_name'])) $args['firstname'] = $this->pmtinfo['x_first_name'];
			if (!empty($this->pmtinfo['x_last_name'])) $args['lastname'] = $this->pmtinfo['x_last_name'];
			if (!empty($this->pmtinfo['x_address'])) $args['address'] = $this->pmtinfo['x_address'];
			if (!empty($this->pmtinfo['x_city'])) $args['city'] = $this->pmtinfo['x_city'];
			if (!empty($this->pmtinfo['x_state'])) $args['state'] = $this->pmtinfo['x_state'];
			if (!empty($this->pmtinfo['x_zip'])) $args['zip'] = $this->pmtinfo['x_zip'];
			if (!empty($this->pmtinfo['x_country'])) $args['country'] = $this->pmtinfo['x_country'];
			if (!empty($this->pmtinfo['x_phone'])) $args['phone'] = $this->pmtinfo['x_phone'];
			if (!empty($this->pmtinfo['x_email'])) $args['email'] = $this->pmtinfo['x_email'];
		}

		$zObj = new Zuora_Contact();
		///$zObj->AccountId = '=[zAccountId]=';
		if (!empty($args['firstname'])) $zObj->FirstName = mb_substr($args['firstname'], 0, 100, 'utf8');
		if (!empty($args['lastname'])) $zObj->LastName = mb_substr($args['lastname'], 0, 100, 'utf8');
		if (!empty($args['address'])) $zObj->Address1 = mb_substr($args['address'], 0, 255, 'utf8');
		if (!empty($args['address2'])) $zObj->Address2 = mb_substr($args['address2'], 0, 255, 'utf8');
		if (!empty($args['city'])) $zObj->City = mb_substr($args['city'], 0, 30, 'utf8');
		if (!empty($args['state'])) $zObj->State = mb_substr($args['state'], 0, 40, 'utf8');
		if (!empty($args['zip'])) $zObj->PostalCode = mb_substr($args['zip'], 0, 20, 'utf8');
		if (!empty($args['country'])) $zObj->Country = mb_substr($args['country'], 0, 32, 'utf8');
		if (!empty($args['phone'])) $zObj->WorkPhone = mb_substr($args['phone'], 0, 40, 'utf8');
		if (!empty($args['email'])) $zObj->WorkEmail = ZuoraConfig::SANDBOX ? ZuoraConfig::DEVELOPER_EMAIL : mb_substr($this->pmtinfo['x_email'], 0, 80, 'utf8');
		if (empty($zObj->FirstName)) $zObj->FirstName = mb_substr($this->signup['firstname'], 0, 100, 'utf8');
		if (empty($zObj->LastName)) $zObj->LastName = mb_substr($this->signup['lastname'], 0, 100, 'utf8');
		if (empty($zObj->WorkEmail)) $zObj->WorkEmail = ZuoraConfig::SANDBOX ? ZuoraConfig::DEVELOPER_EMAIL : mb_substr($this->signup['email'], 0, 80, 'utf8');
		if (empty($zObj->Address1)) $zObj->Address1 = mb_substr($this->signup['address'], 0, 255, 'utf8');

		// Save Zuora object data for later use
		$this->zBillToContact = $zObj;

		return $zObj;
	}

	public function getSoldToContact($args = null)
	{
		$zObj = new Zuora_Contact();
		///$zObj->AccountId = '=[zAccountId]=';
		$zObj->FirstName = mb_substr($this->signup['firstname'], 0, 100, 'utf8');
		$zObj->LastName  = mb_substr($this->signup['lastname'], 0, 100, 'utf8');
		$zObj->Address1  = mb_substr($this->location['address'], 0, 255, 'utf8');
		if (empty($zObj->FirstName)) mb_substr($zObj->FirstName = $this->zBillToContact->FirstName, 0, 100, 'utf8');
		if (empty($zObj->LastName)) mb_substr($zObj->LastName = $this->zBillToContact->LastName, 0, 100, 'utf8');
		if (empty($zObj->Address1)) $zObj->Address1 = mb_substr($this->signup['address'], 0, 255, 'utf8');
		if (empty($zObj->WorkEmail)) $zObj->WorkEmail = ZuoraConfig::SANDBOX ? ZuoraConfig::DEVELOPER_EMAIL : mb_substr($this->signup['email'], 0, 80, 'utf8');
		if ($this->location['city']) $zObj->City = mb_substr($this->location['city'], 0, 40, 'utf8');
		if ($this->location['state']) $zObj->State = mb_substr($this->location['state'], 0, 40, 'utf8');
		if ($this->location['zip']) $zObj->PostalCode = mb_substr($this->location['zip'], 0, 20, 'utf8');
		//if ($this->location['country']) $zObj->Country = $this->location['country'];  // Too many errors from seemingly valid Country names
		if ($this->location['phone']) $zObj->WorkPhone = mb_substr($this->location['phone'], 0, 40, 'utf8');
		if ($this->location['email']) $zObj->WorkEmail = ZuoraConfig::SANDBOX ? ZuoraConfig::DEVELOPER_EMAIL : mb_substr($this->location['email'], 0, 80, 'utf8');

		return $zObj;
	}

	public function getInvoicePayment($args = null)
	{
		if ( $args === null || empty($args['zInvoiceId']) || empty($args['zPaymentId']) || empty($args['amount']) )
		{
			return null;
		}

		$zObj = new Zuora_InvoicePayment();
		$zObj->InvoiceId = $args['zInvoiceId'];
		$zObj->PaymentId = $args['zPaymentId'];
		$zObj->Amount = $args['amount'];

		return $zObj;
	}

	public function getPayment($args = null)
	{
		if ( $args === null || empty($args['zAccountId']) || empty($args['zPaymentMethodId']) || empty($args['amount']) )
		{
			return null;
		}

		$zObj = new Zuora_Payment();
		$zObj->AccountId = $args['zAccountId'];
		$zObj->Amount = (double) $args['amount'] + (double) $args['credit'];
		$zObj->AppliedCreditBalanceAmount = empty($args['credit']) ? 0 : $args['credit'];
		$zObj->AppliedInvoiceAmount = $args['amount'];
		$zObj->EffectiveDate = empty($args['event_date']) ? date('Y-m-d') : $args['event_date'];
		$zObj->PaymentMethodId = $args['zPaymentMethodId'];
		$zObj->Status = 'Processed';
		$zObj->Type = 'Electronic';
		if ($args['zInvoiceId']) $zObj->InvoiceId = $args['zInvoiceId'];
		if ($args['paymentid']) $zObj->LegacyID__c = $args['paymentid'];
		if ($args['zInvoicePayments']) $zObj->InvoicePaymentData = $args['zInvoicePayments'];

		return $zObj;
	}

	public function getPaymentMethod($args = null)
	{
		$payment_method_type = $this->getPaymentMethodType();
		if ( $payment_method_type == 'CreditCard' )
		{
			// Default to test Credit Card Number but replace last four digits on test card with real last four digits, if available
			$cc_num = '4111111111111111';
			if ( !empty($pmtinfo['x_account_number']) )
			{
				$cc_num = substr($cc_num, 0, 12) . str_replace('XXXX', '', $this->pmtinfo['x_account_number']);
			}

			// Default to Visa test card but replace card type with real card type, if available
			$cc_type = 'Visa';
			if ( !empty($this->pmtinfo['x_card_type']) )
			{
				$cc_type = str_replace(' ', '', $this->pmtinfo['x_card_type']);
			}

			$zObj = new Zuora_PaymentMethod();
			$zObj->CreditCardAddress1 = $this->zBillToContact->Address1;
			$zObj->CreditCardAddress2 = $this->zBillToContact->Address2;
			$zObj->CreditCardExpirationMonth = '01';
			$zObj->CreditCardExpirationYear = '2030';
			$zObj->CreditCardHolderName = $this->zBillToContact->FirstName .' '. $this->zBillToContact->LastName;
			$zObj->CreditCardNumber = $cc_num;
			$zObj->CreditCardPostalCode = $this->zBillToContact->PostalCode;
			$zObj->CreditCardState = $this->zBillToContact->State;
			$zObj->CreditCardType = $cc_type;
			$zObj->Type = $payment_method_type;
			$zObj->UseDefaultRetryRule = true;
		}
		else
		{
			$zObj = $this->zCheckPaymentMethod;
		}
		///$zObj->AccountId = '=[zAccountId]=';

		return $zObj;
	}

	public function getLicenseSubscriptionData($args = null)
	{
		list($zProductRatePlanId, $zProductRatePlanChargeId) = $this->getProductRatePlanObjects($args['billing_product_type'], $args['package_name']);
		$zSubscription = new Zuora_Subscription();
		$zSubscription->AccountId = '=[zAccountId]=';
		$zSubscription->AutoRenew = false;
		$zSubscription->ContractAcceptanceDate = $args['event_date'];
		$zSubscription->ContractEffectiveDate = $args['event_date'];
		$zSubscription->InvoiceOwnerId = '=[zAccountId]=';
		$zSubscription->IsInvoiceSeparate = true;
		$zSubscription->ServiceActivationDate = $args['event_date'];
		$zSubscription->SubscriptionStartDate = null;
		$zSubscription->TermEndDate = null;
		$zSubscription->TermStartDate = $args['event_date'];
		$zSubscription->TermType = 'TERMED';

		$zRatePlan = new Zuora_RatePlan();
		$zRatePlan->ProductRatePlanId = $zProductRatePlanId;

		$zRatePlanCharge = new Zuora_RatePlanCharge();
		$zRatePlanCharge->ProductRatePlanChargeId = $zProductRatePlanChargeId;
		$zRatePlanCharge->Price = $args['amount'];

		$zRatePlanChargeData = new Zuora_RatePlanChargeData($zRatePlanCharge);

		$zRatePlanData = new Zuora_RatePlanData($zRatePlan);
		$zRatePlanData->addRatePlanChargeData($zRatePlanChargeData);

		$zObj = new Zuora_SubscriptionData($zSubscription);
		$zObj->addRatePlanData($zRatePlanData);

		return $zObj;
	}

	public function getHardwareSubscriptionData($args = null)
	{
		list($zProductRatePlanId, $zProductRatePlanChargeId) = $this->getProductRatePlanObjects($args['billing_product_type'], $args['product_name']);
		$zSubscription = new Zuora_Subscription();
		$zSubscription->AccountId = '=[zAccountId]=';
		$zSubscription->AutoRenew = false;
		$zSubscription->ContractAcceptanceDate = $args['event_date'];
		$zSubscription->ContractEffectiveDate = $args['event_date'];
		$zSubscription->InvoiceOwnerId = '=[zAccountId]=';
		$zSubscription->IsInvoiceSeparate = true;
		$zSubscription->ServiceActivationDate = $args['event_date'];
		$zSubscription->SubscriptionStartDate = null;
		$zSubscription->TermEndDate = null;
		$zSubscription->TermStartDate = $args['event_date'];
		$zSubscription->TermType = 'TERMED';

		$zRatePlan = new Zuora_RatePlan();
		$zRatePlan->ProductRatePlanId = $zProductRatePlanId;

		$zRatePlanCharge = new Zuora_RatePlanCharge();
		$zRatePlanCharge->ProductRatePlanChargeId = $zProductRatePlanChargeId;
		$zRatePlanCharge->Price = $args['amount'];  // TO DO : investigate whether Price really needs to be set (even though it seems like $zProductRatePlanChargePrice was never actually got set in the working version)

		$zRatePlanChargeData = new Zuora_RatePlanChargeData($zRatePlanCharge);

		$zRatePlanData = new Zuora_RatePlanData($zRatePlan);
		$zRatePlanData->addRatePlanChargeData($zRatePlanChargeData);

		$zObj = new Zuora_SubscriptionData($zSubscription);
		$zObj->addRatePlanData($zRatePlanData);

		return $zObj;
	}

	public function getHostingSubscriptionData($args = null)
	{
		$hosting_amount = (float) $args['hosting_amount'];
		$package_level = (int) $args['package_level'];
		$terminals = (int) $args['terminals'];

		$default_hosting_amount = (float) $this->o_package_container->get_recurring_by_attribute('level', $package_level);
		$default_terminal_limit = (int)   $this->o_package_container->get_ipad_limt_by_attribute('level', $package_level);

		list($zProductRatePlanId, $zProductRatePlanChargeId) = $this->getProductRatePlanObjects($args['billing_product_type'], $args['package_name']);
		//$this->logDebug("zProductRatePlanId={$zProductRatePlanId} zProductRatePlanChargeId={$zProductRatePlanChargeId}");  //debug

		$zSubscription = new Zuora_Subscription();
		///$zSubscription->AccountId = '=[zAccountId]=';
		$zSubscription->ContractAcceptanceDate = empty($args['first_hosting']) ? $this->setup_props['first_hosting'] : $args['first_hosting'];
		$zSubscription->ContractEffectiveDate = $args['start_date'];
		///$zSubscription->InvoiceOwnerId = '=[zAccountId]=';
		$zSubscription->ServiceActivationDate = $args['start_date'];
		$zSubscription->SubscriptionStartDate = null;
		$zSubscription->TermEndDate = null;  // Evergreen
		$zSubscription->TermStartDate = $args['start_date'];
		$zSubscription->TermType = 'EVERGREEN';

		$zRatePlan = new Zuora_RatePlan();
		$zRatePlan->ProductRatePlanId = $zProductRatePlanId;

		$zRatePlanCharge = new Zuora_RatePlanCharge();
		$zRatePlanCharge->ProductRatePlanChargeId = $zProductRatePlanChargeId;
		$zRatePlanCharge->Quantity = '1';
		$zRatePlanCharge->Price = $hosting_amount;

		// The Lavu package level uses tiered pricing so we need to find and set the per unit price
		if ( $package_level === 25 )
		{
			if ( empty($hosting_amount) )
			{
				$hosting_amount = $default_hosting_amount;
			}

			if ( $terminals > $default_terminal_limit )
			{
				$zRatePlanCharge->Quantity = $terminals;
			}

			$per_unit_amount = $hosting_amount / $terminals;
			$zRatePlanCharge->Price = $per_unit_amount;
			//$this->logWarning("using tiered pricing: \${$per_unit_amount} per unit x {$terminals} = \${$hosting_amount}");
		}

		$zRatePlanChargeData = new Zuora_RatePlanChargeData($zRatePlanCharge);

		$zRatePlanData = new Zuora_RatePlanData($zRatePlan);
		$zRatePlanData->addRatePlanChargeData($zRatePlanChargeData);

		$zObj = new Zuora_SubscriptionData($zSubscription);
		$zObj->addRatePlanData($zRatePlanData);

		return $zObj;
	}

	public function getLavuToGoSubscriptionData($args = null) {
		$productType = isset($args['billing_product_type']) ? $args['billing_product_type'] : 'LavuToGo';
		list($zProductRatePlanId, $zProductRatePlanChargeId) = $this->getProductRatePlanObjects($productType, $args['product_name']);
		$zuroAccountId = !empty($args['zAccountId']) ? $args['zAccountId'] : '=[zAccountId]=';

		$zSubscription = new Zuora_Subscription();
		$zSubscription->AccountId = $zuroAccountId;
		$zSubscription->AutoRenew = false;
		$zSubscription->ContractAcceptanceDate = $args['event_date'];
		$zSubscription->ContractEffectiveDate = $args['event_date'];
		$zSubscription->InvoiceOwnerId = $zuroAccountId;
		$zSubscription->IsInvoiceSeparate = true;
		$zSubscription->ServiceActivationDate = $args['event_date'];
		$zSubscription->SubscriptionStartDate = null;
		$zSubscription->TermEndDate = null;
		$zSubscription->TermStartDate = $args['event_date'];
		$zSubscription->TermType = 'EVERGREEN';
		$zSubscription->Status = $args['status'];

		// create Zuora Rate Plan Object for Subscription
		$zRatePlan = new Zuora_RatePlan();
		$zRatePlan->ProductRatePlanId = $zProductRatePlanId;

		// create Zuora Rate Plan Charge object for Subscription
		$zRatePlanCharge = new Zuora_RatePlanCharge();
		$zRatePlanCharge->ProductRatePlanChargeId = $zProductRatePlanChargeId;
		$zRatePlanChargeData = new Zuora_RatePlanChargeData($zRatePlanCharge);
		$zRatePlanData = new Zuora_RatePlanData($zRatePlan);
		$zRatePlanData->addRatePlanChargeData($zRatePlanChargeData);
		$zObj = new Zuora_SubscriptionData($zSubscription);
		$zObj->addRatePlanData($zRatePlanData);

		return $zObj;
	}

	public function getAmendRequest($args = null)
	{
		$zAmendments = new Zuora_Amendments();
		$zAmendments->SubscriptionId = $args['zSubscriptionId'];
		$zAmendments->Name = $args['event_name'];
		$zAmendments->Type = $args['event_type'];
		$zAmendments->Status = $args['event_status'];
		$zAmendments->EffectiveDate = $args['event_date'];
		$zAmendments->ContractEffectiveDate  = $args['event_date'];
		$zAmendments->ServiceActivationDate  = $args['event_date'];
		$zAmendments->CustomerAcceptanceDate = $args['event_date'];

		$zObj = new Zuora_AmendRequest();
		$zObj->Amendments     = $zAmendments;
		$zObj->AmendOptions   = $this->zAmendOptions;
		$zObj->PreviewOptions = $this->zPreviewOptions;

		return $zObj;
	}

	public function getInvoice($args = null)
	{
		$zObj = new Zuora_Invoice();
		$zObj->AccountId = '=[zAccountId]=';
		$zObj->InvoiceDate = $args['event_date'];
		$zObj->TargetDate = $this->getZuoraInvoiceTargetDate($args['event_date']);
		$zObj->IncludesOneTime   = ($args['type'] != 'Hosting');
		$zObj->IncludesRecurring = ($args['type'] == 'Hosting');  // TO DO : we should check the invoice items for Hosting line items for Hardware invoices
		$zObj->IncludesUsage = false;
		$zObj->LegacyID__c = $args['invoiceid'];

		return $zObj;
	}

	public function getInvoiceUpdate($args = null)
	{
		$zObj = new Zuora_Invoice();
		$zObj->Id = '=[zInvoiceId]=';
		$zObj->LegacyID__c = $args['invoiceid'];
		$zObj->Status = 'Posted';

		return $zObj;
	}

	public function getInvoiceItemAdjustment($args = null)
	{
		$zObj = new Zuora_InvoiceItemAdjustment();
		$zObj->AdjustmentDate = $args['event_date'];
		$zObj->Amount = $args['amount'];
		$zObj->InvoiceId = '=[zInvoiceId]=';
		$zObj->LegacyInvoiceID__c = $args['invoiceid'];
		$zObj->SourceId = '=[zInvoiceItemId]=';
		$zObj->SourceType = 'InvoiceDetail';
		///$zObj->Status = 'Processed';
		$zObj->Type = 'Credit';

		return $zObj;
	}

	public function getPaymentMethodUpdate($args = null)
	{
		$zObj = new Zuora_PaymentMethod();
		$zObj->Id = '=[zPaymentMethodId]=';
		$zObj->PaymentMethodStatus = $args['status'];

		return $zObj;
	}

	public function getActiveSubscription($args = null)
	{
		$license_billed = empty($this->setup_props['license_billed']) ? $this->setup_props['first_hosting'] : $this->setup_props['license_billed'];
		$first_hosting  = $this->setup_props['first_hosting'];

		$package_level = $this->signup['package'];

		// Default to Lavu package level for blank or 'none' (for Distro demo accounts)
		if ( empty($package_level) || $package_level == 'none' || $package_level == '[4]' )
		{
			$package_level = '25';
		}

		$package_name = $this->o_package_container->get_plain_name_by_attribute('level', $package_level);
		$printed_name = $this->o_package_container->get_printed_name_by_attribute('level', $package_level);
		$terminals = !empty($this->payment_status['custom_max_ipads']) ? $this->payment_status['custom_max_ipads'] : $this->o_package_container->get_ipad_limt_by_attribute('level', (int) $package_level);

		// Calculate next billing date
		if ( !empty($args['start_date']) )
		{
			$next_billing_date = $args['start_date'];
		}
		else
		{
			$next_billing_date = sprintf('%d-%02d-%02d', date('Y'), date('m'), $this->invoice_day_of_month);
			$today = date('Y-m-d');
			if ( strtotime($today) > strtotime($next_billing_date) )
			{
				$next_billing_date = date('Y-m-d', strtotime("{$next_billing_date} +1 month"));
			}
		}

		$hosting_amount = isset($args['hosting_amount']) ? $args['hosting_amount'] : $this->getTotalMonthlyCost($package_level, $next_billing_date);
		//$this->logDebug("...hosting_amount={$hosting_amount} args[hosting_amount]={$args['hosting_amount']}");  //debug

		$vars = array(
			'package_level'  => $package_level,
			'package_name'   => $package_name,
			'printed_name'   => $printed_name,
			'start_date'     => $next_billing_date,
			'end_date'       => '',
			'hosting_amount' => $hosting_amount,
			'terminals'      => $terminals,
			'billing_product_type' => 'Hosting',
		);

		//$this->logMsg("...dataname={$this->dataname} hosting subscription vars=". json_encode($vars));  //debug

		$zObj = $this->getHostingSubscriptionData($vars);

		return $zObj;
	}

	public function getSubscriptionEvents()
	{
		$license_billed = empty($this->setup_props['license_billed']) ? $this->setup_props['first_hosting'] : $this->setup_props['license_billed'];
		$first_hosting  = $this->setup_props['first_hosting'];

		$billing_changes = BillingChangeDetector::run($this->dataname, 1, 1);
		$this->logDebug("billing_changes=". print_r($billing_changes, true));

		$num_billing_changes = count($billing_changes);
		$max_i = $num_billing_changes - 1;
		$j = 0;

		for ( $i = $max_i; $i >= 0; $i-- )
		{
			$billing_change = $billing_changes[$i];
			$this->logDebug("i={$i} max_i={$max_i} num_billing_changes={$num_billing_changes} billing_change". print_r($billing_change, true));

			// Create subscription for the License invoice first, for better chronological ordering
			if ( $i === $max_i )
			{
				// Query for the first License invoice that falls on the license_billed date and extract its package_name using getPackageLevelFromLicenseInvoice
				$first_license_invoice = $this->getDatabaseRow('first_license_invoice', array('dataname' => $this->dataname, 'license_billed' => $license_billed));

				if ( $first_license_invoice !== false )
				{
					$first_license_package_level = $this->getPackageLevelFromLicenseInvoice($first_license_invoice, $billing_changes[0]['package_level']);
					$first_license_package_name = $this->o_package_container->get_plain_name_by_level((int) $first_license_package_level);

					$this->logDebug("...first_license_package_level={$first_license_package_level} first_license_package_name={$first_license_package_name}");

					// Create Subscription for the current package level's license and the Amendment for closing it
					$this->zSubscriptions[] = array('index' => $j++, 'obj_type' => 'zLicenseSubscription', 'zObj' => $this->getLicenseSubscriptionData(array('package_name' => $first_license_package_name, 'event_date' => $license_billed, 'billing_product_type' => 'License', 'amount' => $invoice['amount'])));
					$this->zSubscriptions[] = array('index' => $j++, 'obj_type' => 'zLicenseAmendRequest', 'zObj' => $this->getAmendRequest(array('event_name' => 'Close License subscription', 'event_type' => 'Cancellation', 'event_status' => 'Completed', 'event_date' => date('Y-m-d', strtotime("{$first_hosting} -1 day")), 'zSubscriptionId' => '=[zSubscriptionId]=')));
				}
			}

			// Create subscription for the current Hosting level
			$billing_change['billing_product_type'] = 'Hosting';
			$this->zSubscriptions[] = array('index' => $j++, 'obj_type' => 'zHostingSubscription', 'zObj' => $this->getHostingSubscriptionData($billing_change));
			if ( !empty($billing_change['end_date']) )
			{
				// Amendment for cancelling subscription
				$this->zSubscriptions[] = array('index' => $j++, 'obj_type' => 'zHostingAmendRequest', 'zObj' => $this->getAmendRequest(array('event_name' => 'Amendment to cancel subscription due to upgrade', 'event_type' => 'Cancellation', 'event_status' => 'Completed', 'event_date' => $billing_change['end_date'], 'zSubscriptionId' => '=[zSubscriptionId]=')));
			}

			$dbargs = array('dataname' => $this->dataname, 'license_billed' => $license_billed, 'start_date' => $billing_change['start_date'], 'end_date' => (empty($billing_change['end_date']) ? date('Y-m-d') : $billing_change['end_date']));
			$dbargs_date_range = $dbargs['start_date'] .' to '. $dbargs['end_date'];

			// Create License subscription (and the Amendment for closing it) for any upgrade invoices for the current Hosting level
			$upgrade_invoices = $this->getDatabaseRow('upgrade-invoices', $dbargs);
			$this->logDebug("...got ". (int) mysqli_num_rows($upgrade_invoices) ." upgrade License invoices with date_range ". $dbargs_date_range);
			while ( $invoice = mysqli_fetch_assoc($upgrade_invoices) )
			{
				$upgrade_package_name = $billing_change['package_name'] .' to '. $billing_change['next_package_name'];
				$this->zSubscriptions[] = array('index' => $j++, 'obj_type' => 'zLicenseSubscription', 'zObj' => $this->getLicenseSubscriptionData(array('package_name' => $upgrade_package_name, 'event_date' => $invoice['due_date'], 'billing_product_type' => 'License', 'amount' => $invoice['amount'])));
				$this->zSubscriptions[] = array('index' => $j++, 'obj_type' => 'zLicenseAmendRequest', 'zObj' => $this->getAmendRequest(array('event_name' => 'Close Upgrade License subscription', 'event_type' => 'Cancellation', 'event_status' => 'Completed', 'event_date' => date('Y-m-d', strtotime("{$invoice['due_date']} +1 day")), 'zSubscriptionId' => '=[zSubscriptionId]=')));
			}

			// Create subscription for any Hardware invoices that happened in this time range using the same dbargs as the 
			$hardware_invoices = $this->getDatabaseRow('hardware-invoices', $dbargs);
			$this->logDebug("...got ". (int) mysqli_num_rows($hardware_invoices) ." Hardware invoices with date range ". $dbargs_date_range);
			while ( $invoice = mysqli_fetch_assoc($hardware_invoices) )
			{
				$this->zSubscriptions[] = array('index' => $j++, 'obj_type' => 'zHardwareSubscription', 'zObj' => $this->getHardwareSubscriptionData(array('product_name' => 'Lavu POS Hardware', 'event_date' => $invoice['due_date'], 'billing_product_type' => 'Hardware', 'amount' => $invoice['amount'])));
				$this->zSubscriptions[] = array('index' => $j++, 'obj_type' => 'zHardwareAmendRequest', 'zObj' => $this->getAmendRequest(array('event_name' => 'Close Hardware License subscription', 'event_type' => 'Cancellation', 'event_status' => 'Completed', 'event_date' => date('Y-m-d', strtotime("{$invoice['due_date']} +1 day")), 'zSubscriptionId' => '=[zSubscriptionId]=')));
			}
		}

		return $this->zSubscriptions;
	}

	public function getInvoiceEvents()
	{
		$this->logDebug("In ". __METHOD__ ." for dataname={$this->dataname}");

		$i = 0;
		$has_license_invoice = false;
		$last_license_invoice_date = '';
		$invoices = $this->getDatabaseRow('invoices');
		$this->logDebug("...got ". mysqli_num_rows($invoices) ." invoice(s)");

		// TO DO : tally up License invoice waive budget, based on values of reseller_licenses
		$reseller_license_waive_budget = 0.00;
		$reseller_licenses_dataname = $this->getDatabaseRow('reseller_licenses_dataname', array('dataname' => $this->dataname, 'applied' => $invoice['due_date']));
		while ( $reseller_license = mysqli_fetch_assoc($reseller_licenses_dataname) )
		{
			$reseller_license_waive_budget += (float) $reseller_license['value'];
		}
		$this->logDebug("...reseller_license_waive_budget={$reseller_license_waive_budget}");

		while ( $invoice = mysqli_fetch_assoc($invoices) )
		{
			$payments_for_invoice = $this->getDatabaseRow('payment_responses_invoiceid', array('dataname' => $this->dataname, 'dbtable' => 'payment_responses', 'restaurantid' => $this->restaurantid, 'invoiceid' => $invoice['id']));
			$num_payments_for_invoice = mysqli_num_rows($payments_for_invoice);

			$this->logDebug("...isHardwareInvoice=". ($invoice['type'] == 'Hardware'));

			// Avoid creating Zuora invoices that do not have payments for cancelled accounts
			if ( $this->payment_status['canceled'] == '1' && strtotime($this->payment_status['cancel_date']) < strtotime($invoice['due_date']) && $num_payments_for_invoice === 0 )
			{
				$this->logDebug("...skipping invoice with no payments since account was cancelled on: {$invoice['due_date']}");
				continue;
			}
			// Avoid creating Zuora invoices that do not have payments for disabled accounts
			else if ( $this->restaurant['disabled'] !== '0' && mysqli_num_rows($payments) === 0 )
			{
				$this->logDebug("...skipping invoice with no payments since account is disabled");
				continue;
			}
			// Avoid creating Zuora invoices for the second license invoice in one month (since they get ignored in the Lavu billing system)
			else if ( $has_license_invoice && $invoice['type'] == 'License' )
			{
				$invoice_date_ts = strtotime($invoice['due_date']);
				$last_license_invoice_date_ts = strtotime($last_license_invoice_date);
				$next_invoice_date_ts = strtotime("{$last_license_invoice_date} +1 month");
				$next_invoice_date = date("Y-m-d", $next_invoice_date_ts);

				if ( !empty($last_license_invoice_date) && ($invoice_date_ts >= $last_license_invoice_date_ts) && ($invoice_date_ts < $next_invoice_date_ts) )
				{
					$this->logDebug("...skipping 2nd License invoice in one month for invoice ID {$invoice['id']}: last_license_invoice_date={$last_license_invoice_date} invoice_date={$invoice['due_date']} next_invoice_date={$next_invoice_date}");
					$last_license_invoice_date = $invoice['due_date'];
					continue;
				}
			}
			// Hardware invoices get their own, separate subscriptions so we just skip them here
			else if ( $invoice['type'] == 'Hardware' )
			{
				$this->logDebug("...skipping Hardware invoice ID {$invoice['id']}");
				continue;
			}


			$invoice_i = $i;

			// Invoice
			$zInvoice = $this->getInvoice(array('event_date' => $invoice['due_date'], 'type' => $invoice['type'], 'invoiceid' => $invoice['id']));
			$zInvoiceUpdate = $this->getInvoiceUpdate(array('invoiceid' => $invoice['id'], 'zInvoiceId' => "=[zInvoiceId:{$invoice_i}]="));
			$this->zInvoiceEvents[]  = array('index' => $i, 'LegacyID' => $invoice['id'], 'obj_type' => 'zInvoice',       'zObj' => $zInvoice);
			$this->zInvoices[]       = array('index' => $i, 'LegacyID' => $invoice['id'], 'obj_type' => 'zInvoice',       'zObj' => $zInvoice);
			$i++;
			$this->zInvoiceEvents[]  = array('index' => $i, 'LegacyID' => $invoice['id'], 'obj_type' => 'zInvoiceUpdate', 'zObj' => $zInvoiceUpdate);
			$this->zInvoiceUpdates[] = array('index' => $i, 'LegacyID' => $invoice['id'], 'obj_type' => 'zInvoiceUpdate', 'zObj' => $zInvoiceUpdate);
			$i++;

			// Special processing for License invoices
			$waive_reseller_license_invoice = false;
			if ( $invoice['type'] == 'License' )
			{
				// Adjust Subscription terms after License invoice
				$has_license_invoice = true;

				// Record date of every License invoice for the code that checks for two License invoices in one month
				$last_license_invoice_date = $invoice['due_date'];

				// Waive License invoices if there if we had enough reseller licenses to cover it and they didn't pay for this invoice on their own
				if ( (double) $invoice['amount'] > 0.00 && $num_payments_for_invoice === 0 && $reseller_license_waive_budget > 0.00 )
				{
					$waive_reseller_license_invoice = true;
					$reseller_license_waive_budget = $reseller_license_waive_budget - (float) $invoice['amount'];
				}
			}

			$this->logDebug("...invoiceid={$invoice['id']} waived={$invoice['waived']} waive_reseller_license_invoice={$waive_reseller_license_invoice}");

			// Waive invoices by way of InvoiceItemAdjustments
			if ( $waive_reseller_license_invoice || $invoice['waived'] == '1' )
			{
				$zInvoiceItemAdjustment = $this->getInvoiceItemAdjustment( array('event_date' => $invoice['due_date'], 'amount' => $invoice['amount'], 'zInvoiceId' => "=[zInvoiceId:{$invoice_i}]=", 'invoiceid' => $invoice['id'], 'zInvoiceItemId' => "=[zInvoiceItemId:{$invoice_i}]=") );
				$this->logDebug("...waive_reseller_license_invoice={$waive_reseller_license_invoice} invoice[waived]={$invoice['waied']} zInvoiceItemAdjustment=". print_r($zInvoiceItemAdjustment, true));
				$this->zInvoiceEvents[]   = array('index' => $i, 'obj_type' => 'zInvoiceItemAdjustment', 'LegacyID' => $invoice['id'], 'zObj' => $zInvoiceItemAdjustment, 'ChargeType' => (($invoice['type'] == 'License') ? 'OneTime' : 'Recurring'), 'ServiceStartDate' => $invoice['due_date']);
				$this->zInvoiceItemAdjs[] = array('index' => $i, 'obj_type' => 'zInvoiceItemAdjustment', 'LegacyID' => $invoice['id'], 'zObj' => $zInvoiceItemAdjustment, 'ChargeType' => (($invoice['type'] == 'License') ? 'OneTime' : 'Recurring'), 'ServiceStartDate' => $invoice['due_date']);
				$i++;
				$this->logWarning("...waiving invoiceid={$invoice['id']} waived={$invoice['waived']} waive_reseller_license_invoice={$waive_reseller_license_invoice}");
			}

			$this->legacy2zuora_invoiceids[ $invoice['id'] ] = "=[zInvoiceId:{$invoice_i}]=";
		}

		$this->logDebug("...zInvoiceEvents". count($this->zInvoiceEvents));

		return array($this->zInvoiceEvents, $this->zInvoices, $this->zInvoiceUpdates, $this->zInvoiceItemAdjs);
	}

	public function getPaymentEvents()
	{
		//$this->logDebug("In ". __METHOD__ ." for dataname={$this->dataname} package_name={$this->package_name} license_billed={$this->setup_props['license_billed']}");

		$i = 0;
		$total_paid = 0.00;
		$total_unpaid = 0.00;
		$legacy_invoices_paid = array();

		// Import payments from Lavu Billing System

		$payments = $this->getDatabaseRow('payment_responses', array('dataname' => $this->dataname, 'dbtable' => 'payment_responses', 'restaurantid' => $this->restaurant['id']));
		//$this->logDebug("...got ". mysqli_num_rows($payments) ." payment row(s)");

		// Payment
		while ( $payment = mysqli_fetch_assoc($payments) )
		{
			$paymentid = $payment['id'];
			//$this->logDebug("...dataname={$this->dataname} x_amount={$payment['x_amount']} paymentid={$paymentid} invoices_applied_to={$payment['invoices_applied_to']}");

			// Do not load payments that only covered undrafted payments.  But if invoices_applied_to is set, there was enough of the payment left over to cover another invoice item so we'll still load that much of it.
			if ( strstr($payment['batch_action'], 'used to cover') !== false && empty($payment['batch_status']) && empty($payment['invoices_applied_to']) )
			{
				$this->logWarning("...skipping paymentid={$paymentid} because it covered an undrafted payment");
				continue;
			}

			// Figure out which legacy invoices have been paid off by the LBS payment

			$paid_amount = 0.00;
			$used_amount = 0.00;
			$payment_mapping = explode('|', $payment['invoices_applied_to']);
			$num_payment_mappings = count($payment_mapping);
			$zInvoicePayments = array();
			$j = 0;

			foreach ( $payment_mapping as $invoice_applied_to )
			{
				list($paid_amount, $invoiceid) = explode(' to ', $invoice_applied_to);
				//$this->logDebug("...paid_amount={$paid_amount} invoiceid={$invoiceid} invoice[id]={$invoice['id']}");

				if ( empty($paid_amount) )
				{
					//$paid_amount = $payment['x_amount'];
					//$this->logWarning("...got blank paid_amount from invoices_applied_to ({$invoice['invoices_applied_to']}), falling back to x_amount ({$payment['x_amount']}");
				}

				// Init each legacy inoice in the array to a float val so we can += the paid_amount
				if ( !isset( $legacy_invoices_paid[$invoiceid] ) )
				{
					$legacy_invoices_paid[$invoiceid] = 0.00;
				}

				// += in case it's possible the same invoice is in the "invoice_applied_to" value multiple times
				$legacy_invoices_paid[$invoiceid] += (float) $paid_amount;

				// Keep a running total of all of the invoices paid so we can make sure it matches the total for the payment, just in case.
				$used_amount += (float) $paid_amount;

				// Moved this to outside of the loop to only have one InvoicePayment per Payment, since we're going with just one legacy Invoice
				///$zInvoicePayments[] = $this->getInvoicePayment(array('amount' => $paid_amount, 'zInvoiceId' => "=[zInvoiceId:{$invoiceid}]="));
			}

			$total_paid +=  (float) $used_amount;
			$total_unpaid += ((float) $payment['x_amount'] - $used_amount);

			$zInvoicePayments[] = $this->getInvoicePayment(array('amount' => $total_paid, 'zInvoiceId' => "=[zInvoiceId:{$invoiceid}]="));

			$this->zPayments[] = array('index' => $j++, 'obj_type' => 'zPayment', 'LegacyID' => $payment['id'], 'zObj' => $this->getPayment(array('amount' => $payment['x_amount'], 'event_date' => strstr($payment['datetime'], ' ', true), 'zInvoicePayments' => $zInvoicePayments, 'paymentid' => $payment['id'])));
		}

		$this->logDebug("...got ". count($this->zPayments) ." payment event(s)");

		return array('zPayments' => $this->zPayments, 'total_paid' => $total_paid, 'total_unpaid' => $total_unpaid, 'legacy_invoices_paid' => $legacy_invoices_paid);
	}

	public function getCancellationEvents()
	{
		$this->logDebug('In '. __METHOD__);

		$is_currently_cancelled = ($this->restaurant['disabled'] !== '0' || $this->payment_status['canceled'] === '1');
		$this->logDebug("...is_currently_cancelled={$is_currently_cancelled}");

		$i = 0;

		if ( $is_currently_cancelled )
		{
			$cancel_date = empty($this->payment_status['cancel_date']) ? date('Y-m-d') : date('Y-m-d', strtotime($this->payment_status['cancel_date']));

			// Cancel active PaymentMethods
			$this->zCancellations[] = array('index' => $i++, 'obj_type' => 'zPaymentMethodUpdate', 'zObj' => $this->getPaymentMethodUpdate(array('status' => 'Closed')));

			// Cancel active Subscriptions
			$this->zCancellations[] = array('index' => $i++, 'obj_type' => 'zHostingAmendRequest', 'zObj' => $this->getAmendRequest(array('event_name' => 'Close Hosting subscription', 'event_type' => 'Cancellation', 'event_status' => 'Cancelled', 'event_date' => $cancel_date, 'zSubscriptionId' => '=[zSubscriptionId]=')));

			// Cancel Account
			$this->zCancellations[] = array('index' => $i++, 'obj_type' => 'zAccountUpdate', 'zObj' => $this->getAccountUpdate(array('event_status' => 'Canceled')));
		}

		return $this->zCancellations;
	}

	/**
	 * Private Methods
	 */

	private function logMsg($msg)
	{
		error_log($msg);
	}

	private function logWarning($msg)
	{
		$this->logMsg('WARNING: '. $msg);
	}

	private function logDebug($msg)
	{
		if (ZuoraConfig::SANDBOX) $this->logMsg('DEBUG: '. $msg);
	}

	private function logError($msg)
	{
		error_log(__FILE__ .' got error in '. __METHOD__ .'(): '. $msg);
		$this->logMsg('ERROR: '. $msg);
	}

	private function logErrorAndDie($msg)
	{
		error_log(__FILE__ .' got error in '. __METHOD__ .'(): '. $msg);
		$this->logMsg('ERROR: '. $msg);
		die($msg ."\n");
	}

	/**
	 * Utility method for inserting variable values into ZOQL strings
	 */
	private function getAccountSource()
	{
		$account_source = '';

		if ( $this->signup['status'] == 'new' )
		{
			$account_source = 'Web Signup';
		}
		else if ( $this->signup['status'] == 'manual' && !empty($this->payment_status['trial_end']) )
		{
			$account_source = 'Distro Account';
		}
		else if ( $this->signup['status'] == 'manual' && empty($this->payment_status['trial_end']) )
		{
			$account_source = 'Lavu Internal';

			if (!empty($this->restaurant['reseller'])) $account_source .= ' Reseller';
			if (!empty($this->restaurant['dev'])) $account_source .= ' Dev';
			if (!empty($this->restaurant['test'])) $account_source .= ' Test';
			if (!empty($this->restaurant['demo'])) $account_source .= ' Demo';

			$account_source .= ' Account';
		}

		return $account_source;
	}

	public function getDatabaseRow($query_name, $dbargs = null)
	{
		$return_single_row = true;
		$die_on_zero_rows  = true;
		$die_on_db_errors  = true;

		if ( $dbargs === null )
		{
			$dbargs = array('dbtable' => $query_name, 'dataname' => $this->dataname);
		}

		switch ($query_name)
		{
			case 'restaurants':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `data_name` = '[dataname]' ORDER BY `id`";
				$return_single_row = true;
				break;

			case 'locations':
				$die_on_zero_rows  = false;
				$die_on_db_errors  = false;
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`[dbtable]` WHERE `_disabled` IN('', '0') ORDER BY `id` DESC";
				break;

			case 'restaurant_locations':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`restaurant_locations` WHERE `dataname` = '[dataname]' ORDER BY `id` DESC";
				break;

			case 'signups':
			case 'payment_status':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' ORDER BY `id`";
				break;

			case 'payment_responses':
				$die_on_zero_rows  = false;
				$return_single_row = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `match_restaurantid` = '[restaurantid]' AND `x_response_code` = '1' AND `x_type` IN('auth_capture', 'Check', 'Misc') AND !(x_account_number = 'XXXX6781' AND x_description LIKE '%POSLavu -%') AND !(`batch_status` = 'N' AND `datetime` >= '2014-02-01' AND `datetime` < '2014-08-15' AND `batch_action` = '') ORDER BY `datetime`";
				break;

			case 'payment_responses_invoiceid':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `match_restaurantid` = '[restaurantid]' AND `x_response_code` = '1' AND `x_type` IN('auth_capture', 'Check', 'Misc') AND !(x_account_number = 'XXXX6781' AND x_description LIKE '%POSLavu -%') AND !(`batch_status` = 'N' AND `datetime` >= '2014-02-01' AND `datetime` < '2014-08-15' AND `batch_action` = '') AND (`invoices_applied_to` LIKE '%[invoiceid]' OR `invoices_applied_to` LIKE '%[invoiceid]|%') ORDER BY `datetime`";
				break;

			case 'last_payment':
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`payment_responses` WHERE `match_restaurantid` = '[restaurantid]' AND `x_response_code` = '1' AND `x_type` IN('auth_capture', 'Check', 'Misc') AND !(x_account_number = 'XXXX6781' AND x_description LIKE '%POSLavu -%') AND !(`batch_status` = 'N' AND `datetime` >= '2014-02-01' AND `datetime` < '2014-08-15' AND `batch_action` = '') ORDER BY `datetime` DESC LIMIT 1";
				break;

			case 'custom_payments':
				$return_single_row = false;
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`payment_responses` WHERE `match_restaurantid` = '[restaurantid]' AND `x_response_code` = '1' AND `x_type` IN('Check', 'Misc') AND `datetime` >= '2016-01-01' ORDER BY `date`, `time`";
				break;

			case 'invoices':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' ORDER BY `due_date`";
				break;

			case 'upgrade-invoices':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$dbargs['end_date'] = empty($dbargs['end_date']) ? date('Y-m-d') : $dbargs['end_date'];
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'License' AND `due_date` > '[license_billed]' AND `due_date` >= '[start_date]' AND `due_date` < '[end_date]' ORDER BY `due_date`";
				//$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' AND `type` = 'License' ORDER BY `due_date`";
				break;

			case 'hardware-invoices':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$dbargs['end_date'] = empty($dbargs['end_date']) ? date('Y-m-d') : $dbargs['end_date'];
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'Hardware' AND `due_date` >= '[start_date]' AND `due_date` < '[end_date]' ORDER BY `due_date`";
				break;

			case 'last_license_invoice':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'License' AND `due_date` < '[before_date]' ORDER BY `due_date` DESC LIMIT 1";
				break;

			case 'last_hosting_invoice':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'Hosting' ORDER BY `due_date` DESC LIMIT 1";
				break;

			case 'monthly_parts':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$die_on_db_errors  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`monthly_parts` WHERE `dataname`='[dataname]' AND ((`mp_start_day` <= '[end_date]' OR `mp_start_day` = '') AND (`mp_end_day` >= '[start_date]' OR `mp_end_day` = ''))";
				break;

			case 'first_license_invoice':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'License' AND `due_date` = '[license_billed]' ORDER BY `due_date` LIMIT 1";
				break;

			case 'reseller_licenses_dataname':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON (`poslavu_MAIN_db`.`restaurants`.`id` = `poslavu_MAIN_db`.`reseller_licenses`.`restaurantid`) WHERE `data_name` = '[dataname]' ORDER BY `reseller_licenses`.`id`";
				break;

			case 'reseller_license_by_dataname':
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON (`poslavu_MAIN_db`.`restaurants`.`id` = `poslavu_MAIN_db`.`reseller_licenses`.`restaurantid`) WHERE `data_name` = '[dataname]' ORDER BY `reseller_licenses`.`id` LIMIT 1";
				break;

			case 'reseller_license_on_date':
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON (`poslavu_MAIN_db`.`restaurants`.`id` = `poslavu_MAIN_db`.`reseller_licenses`.`restaurantid`) WHERE `data_name` = '[dataname]' AND DATE(`applied`) >= '[applied]' ORDER BY `reseller_licenses`.`id` LIMIT 1";
				break;

			case 'billing_1pmt':
				$return_single_row = false;
				$where_clause = isset($dbargs['dataname']) ? " r.`data_name` = '[dataname]'" : " r.`data_name` != '' AND `datetime` >= '2015-01-01'";
				$limit_clause = isset($dbargs['limit_clause']) ? ' LIMIT [limit_clause]' : '';
				$sql = "SELECT r.`data_name` as dataname, 1pmt.* FROM `billing_1pmt` 1pmt LEFT JOIN `restaurants` r ON (1pmt.`restaurant_id` = r.id) WHERE {$where_clause} ORDER BY 1pmt.`datetime` {$limit_clause}";
				break;

			case 'active_authnet_arbs':
				$return_single_row = false;
				$limit_clause = isset($dbargs['limit_clause']) ? ' LIMIT [limit_clause]' : '';
				$sql = "SELECT * FROM `authnet_899274_1378029` WHERE `Status` = 'Active' ORDER BY `Start Date` {$limit_clause}";
				break;

			default:
				die(__METHOD__ ." got unmapped table {$query_name} for dataname {$this->dataname}\n");
				break;
		}

		$result = mlavu_query($sql, $dbargs);

		if ( $result === false && $die_on_db_errors )
		{
			die("Error with '{$dbtable}' query: ". mlavu_dberror() ."\n");
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			$this->logError("No row for '{$query_name}' query for {$this->dataname}: sql={$sql} dbargs=". json_encode($dbargs));
			die("Database error");
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}

	public function getInvoiceDayOfMonth()
	{
		if ( $this->invoice_day_of_month === null )
		{
			$this->invoice_day_of_month = '';

			$this->last_hosting_invoice = $this->getDatabaseRow('last_hosting_invoice');

			if ( !empty($this->last_hosting_invoice) )
			{
				$this->invoice_day_of_month = date( 'j', strtotime($this->last_hosting_invoice['due_date']) );
			}
			else if ( !empty($this->setup_props['first_hosting']) )
			{
				$this->invoice_day_of_month = date( 'j', strtotime($this->setup_props['first_hosting']) );
			}
			else if ( !empty($this->setup_props['license_billed']) )
			{
				$this->invoice_day_of_month = date( 'j', strtotime($this->setup_props['license_billed']) );
			}
			else
			{
				$this->invoice_day_of_month = '1';
			}

			/*  // Push accounts whose invoice day of month fall on days not found in all months to the 1st
			if ( in_array($this->invoice_day_of_month, array('29', '30', '31')) )
			{
				$this->invoice_day_of_month = '1';
			}*/
		}

		return $this->invoice_day_of_month;
	}

	private function getPaymentInfoFromPaymentResponse($payment_response)
	{
		$pmtinfo = array();

		foreach ( explode("\n", $payment_response['response']) as $keyval )
		{
			list($key, $val) = explode(' = ', $keyval, 2);
			$pmtinfo[$key] = utf8_encode($val);
		}

		//$this->logDebug("pmtinfo=". print_r($pmtinfo, true));

		return $pmtinfo;
	}

	private function getPackageLevelFromLicenseInvoice($invoice, $prev_package_level)
	{
		global $o_package_container;

		$invoice_amount = (float) $invoice['amount'];
		$invoice_date   = $invoice['due_date'];

		// Check for reseller license on that corresponds to invoice date
		if ( $this->payment_status['license_applied'] )
		{
			$reseller_licenses = $this->getDatabaseRow('reseller_licenses_dataname');
			while ( $reseller_license = mysqli_fetch_assoc($reseller_licenses) )
			{
				// Use package level from oldest reseller_license that was applied before our invoice
				if ( strtotime($reseller_license['applied']) <= strtotime($invoice_date) )
				{
					$package_name_pieces = explode(' to ', $reseller_license['type']);
					$package_name = array_pop($package_name_pieces);  // Get the right-most array element

					// Map Silver Promo => Silver
					$package_name = str_replace(' Promo', '', $package_name);

					$package_level = $this->o_package_container->get_level_by_attribute('name', $package_name);
					return $package_level;
				}
			}
		}

		// Use dates to tell 2nd gen vs. 3rd gen core package levels apart from each other
		if ( $invoice_amount === 895.00 && $invoice_date < '2013-03-13' )
		{
			$package_level = 1;
		}
		else if ( $invoice_amount === 895.00 && $invoice_date < '2013-11-20' )
		{
			$package_level = 11;
		}
		else if ( $invoice_amount === 895.00 )
		{
			$package_level = 21;
		}
		else if ( $invoice_amount === 1495.00 && $invoice_date < '2013-03-13' )
		{
			$package_level = 2;
		}
		else if ( $invoice_amount === 1495.00 && $invoice_date < '2013-11-20' )
		{
			$package_level = 12;
		}
		else if ( $invoice_amount === 1495.00 )
		{
			$package_level = 22;
		}
		else if ( $invoice_amount === 3495.00 )
		{
			$package_level = 3;
		}
		else if ( $invoice_amount === 2495.00 && $invoice_date < '2013-11-20' )
		{
			$package_level = 13;
		}
		else if ( $invoice_amount === 2495.00 )
		{
			$package_level = 23;
		}
		// Silver Promo (mapped to Silver)
		else if ( $invoice_amount === 625.00 )
		{
			$package_level = 1;
		}
		else
		{
			$this->logError("***ERROR: Could not determine package level for dataname={$this->dataname} invoice_amount={$invoice_amount} at date=". $invoice['due_date']);
			$package_level = $prev_package_level;  // like Gold D

			// TO DO : prev_package_level doesn't exist
		}

		return $package_level;
	}

	private function getTotalMonthlyCost($package_level, $bill_date)
	{
		// First try to get it from last invoice
		if ( $this->last_hosting_invoice !== null )
		{
			$total_monthly_cost = $this->last_hosting_invoice['amount'];
		}
		else
		{
			$monthly_hosting_cost = !empty($this->payment_status['custom_monthly_hosting']) ? (float) $this->payment_status['custom_monthly_hosting'] : $this->o_package_container->get_recurring_by_attribute('level', $package_level);

			// TO DO : check for payment_status['annual_agreement'] which drops Lavu/LavuEnterprise to 59.00 per month instead of 69.00
			// TO DO : check for annual pre-pay

			// Total monthly parts that apply for bill_date

			$extra_terminals_monthly_cost = 0.00;
			$routing_monthly_cost         = 0.00;
			$other_monthly_cost           = 0.00;

			$monthly_parts = $this->getDatabaseRow('monthly_parts', array('dataname' => $this->dataname, 'start_date' => $bill_date, 'end_date' => date('Y-m-d', strtotime("{$bill_date} +1 day"))) );
			while ( $monthly_part = mysqli_fetch_assoc($monthly_parts) )
			{
				switch ( strtolower($monthly_part['description']) )
				{
					case 'customer ipad limit':
						$extra_terminals_monthly_cost += (float) $monthly_part['amount'];
						break;

					case 'routing':
						$routing_monthly_cost += (float) $monthly_part['amount'];
						break;

					case 'other':
					default:
						$other_monthly_cost += (float) $monthly_part['amount'];
						break;
				}
			}

			$total_monthly_cost = $monthly_hosting_cost + $extra_terminals_monthly_cost + $routing_monthly_cost + $other_monthly_cost;
		}

		return $total_monthly_cost;
	}

	private function getPaymentMethodType()
	{
		$this->logDebug("In ". __METHOD__ ."()");

		$payment_method_type = 'CreditCard';

		// Josh Edwards asked me to change this check to just look at this last payment
		if ( $this->last_payment['type'] == 'Check' || $this->last_payment['type'] == 'Misc' )
		{
			$payment_method_type = 'Check';
		}

		// TO DO : see about overriding payment method or, at the very least, throwing a warning here if they have an active ARB

		return $payment_method_type;
	}

	private function getProductRatePlanObjects($billing_product_type, $package_name)
	{
		//$this->logDebug('In '. __METHOD__ .'()');

		if ( $billing_product_type == 'Hosting' )
		{
			$zProductName = "Monthly {$package_name} Package";
		}
		else if ( $billing_product_type == 'Hardware' )
		{
			$zProductName = $package_name;
		}
		else if ( strstr($package_name, ' to ') !== false )
		{
			$zProductName = "{$package_name} Package Upgrade";
		} else if($billing_product_type == 'LavuToGo') {
			$zProductName = "Lavu Online Ordering - {$package_name}";
		}
		else
		{
			$zProductName = "{$package_name} Package";
		}

		if ( !isset($this->mappingPackgeNameToProductRatePlanId[$billing_product_type]) || !isset($this->mappingPackgeNameToProductRatePlanId[$billing_product_type][$zProductName]) )
		{
			$this->logDebug("...this->mappingPackgeNameToProductRatePlanId[{$billing_product_type}]=". isset($this->mappingPackgeNameToProductRatePlanId[$billing_product_type]));
			$this->logDebug("...this->mappingPackgeNameToProductRatePlanId[{$billing_product_type}][{$zProductName}]=". isset($this->mappingPackgeNameToProductRatePlanId[$billing_product_type][$zProductName]));
			die(__METHOD__ ." could not find zProductRatePlanId mapping for billing_product_type={$billing_product_type} zProductName={$zProductName}\n");
		}

		$zProductRatePlanId = $this->mappingPackgeNameToProductRatePlanId[$billing_product_type][$zProductName];


		if ( !isset($this->mappingProductRatePlanIdToProductRatePlanChargeId[$billing_product_type]) || !isset($this->mappingProductRatePlanIdToProductRatePlanChargeId[$billing_product_type][$zProductRatePlanId]) )
		{
			$this->logDebug("...this->mappingPackgeNameToProductRatePlanId[{$billing_product_type}]=". isset($this->mappingProductRatePlanIdToProductRatePlanChargeId[$billing_product_type]));
			$this->logDebug("...this->mappingPackgeNameToProductRatePlanId[{$billing_product_type}][{$zProductName}]=". isset($this->mappingProductRatePlanIdToProductRatePlanChargeId[$billing_product_type][$zProductRatePlanId]));
			die(__METHOD__ ." could not find zProductRatePlanChargeId mapping for billing_product_type={$billing_product_type} zProductRatePlanId={$zProductRatePlanId}\n");
		}

		$zProductRatePlanChargeId = $this->mappingProductRatePlanIdToProductRatePlanChargeId[$billing_product_type][$zProductRatePlanId];
		///$this->logDebug("...zProductRatePlanId={$zProductRatePlanId} zProductRatePlanChargeId={$zProductRatePlanChargeId}");

		return array($zProductRatePlanId, $zProductRatePlanChargeId);
	}

	private function getZuoraInvoiceTargetDate($due_date)
	{
		$target_date = null;

		$due_date_plus_month_ts = strtotime("{$due_date} +1 month");
		$due_date_plus_month = date('Y-m-d', $due_date_plus_month_ts);
		$target_date_ts = strtotime("{$due_date_plus_month} -1 day");
		$target_date = date('Y-m-d', $target_date_ts);

		//echo "DEBUG: due_date={$due_date} target_date={$target_date}\n";  //debug

		return $target_date;
	}

	public function isLavuToGoPackage($ratePlanId) {
		return isset($this->mappingProductRatePlanIdToProductRatePlanChargeId['LavuToGo'][$ratePlanId]);
	}

	public function getPackageDetails($productType, $package) {
		list($zProductRatePlanId, $zProductRatePlanChargeId) = $this->getProductRatePlanObjects($productType, $package);
		return (object) ['ProductRatePlanId' => $zProductRatePlanId, 'ProductRatePlanChargeId' => $zProductRatePlanChargeId];
	}
}
