<?php

require_once(dirname(__FILE__).'/ZuoraIntegration.php');

class ZuoraUtils
{
	public $zInt;

	public function __construct($zInt = null)
	{
		if ( $zInt !== null )
		{
			$this->zInt = $zInt;
		}
		else
		{
			$this->zInt = new ZuoraIntegration();
		}
	}


	// SOMEDAY : add stuff along top of billing.php and billing_info.php


	/**
	 * Builds and returns an array containing an array containing the component elements of the due_info string
	 *
	 * @param array $vars Required keys: dataname
	 *
	 * @return array Component elements needed to construct payment_status.due_info string - which could be passed to LavuAccountUtils->setDueInfo()
	 */
	public function getDueInfoArray($vars)
	{
		if ( empty($vars['dataname']) )
		{
			$this->zInt->logError(__METHOD__ .'() called without required argument(s): vars='. json_encode($vars));
			return;
		}

		// Get zAccount info
		$zAccount = $this->zInt->getAccount($vars);

		$vars['zAccountId']    = $zAccount->Id;
		$vars['EffectiveDate'] = $zAccount->CreatedDate;

		// Get payment-related due_info array elements from helper function
		$due_info_bills = $this->getDueInfoBillsArray($vars);

		// Get payment-related due_info array elements from helper function
		$due_info_payments = $this->getDueInfoPaymentsArray($vars);

		// Use the union of the two help method arrays as our due_info array
		$due_info = $due_info_bills + $due_info_payments;

		return $due_info;
	}


	/**
	 * Helper method for getDueInfoArray() that retrieves the payment-related elements of the due_info component array
	 *
	 * @param array $vars Required keys: zAccountId, EffectiveDate
	 *
	 * @return array containing due_info array elements specific to payments
	 */
	public function getDueInfoPaymentsArray($vars)
	{
		if ( empty($vars['zAccountId']) )
		{
			throw new Exception(__METHOD__ ."() called without required args zAccountId|EffectiveDate: vars=". json_encode($vars));
			exit;
		}

		$due_info = array(
			'paid_last_four_weeks' => 0.00,     // not used - only paid_last_two_weeks gets used per /sa_cp/billing/billing.php
			'paid_last_two_weeks'  => 0.00,     // loop below
			'last_payment_made'    =>  '',      // loop below
			'other_payments_total' => 0.00,     // not used anymore - Zuora doesn't let accounts have unapplied payments
		);

		// Get payments in last 15 days from Zuora and find newest one as well as the total of them all
		$two_weeks_ago_ts  = strtotime('-15 days');
		$four_weeks_ago_ts = strtotime('-31 days');
		$zPayments = $this->zInt->getPayments($vars);

		foreach ( $zPayments as $zPayment )
		{
			if ( strtotime($zPayment->EffectiveDate) > $two_weeks_ago_ts )
			{
				$due_info['paid_last_two_weeks'] += (float) $zPayment->Amount;
			}

			if ( strtotime($zPayment->EffectiveDate) > $four_weeks_ago_ts )
			{
				$due_info['paid_last_four_weeks'] += (float) $zPayment->Amount;
			}

			if ( empty($due_info['last_payment_made']) || (strtotime($zPayment->EffectiveDate) > strtotime($due_info['last_payment_made']) ) )
			{
				$due_info['last_payment_made'] = $zPayment->EffectiveDate;
			}
		}

		return $due_info;
	}


	/**
	 * Helper method for getDueInfoArray() that retrieves the bill-related elements of the due_info component array
	 *
	 * @param array $vars $dataname Required keys: dataname, zAccountId
	 *
	 * @return array containing due_info array elements specific to bills
	 */
	public function getDueInfoBillsArray($vars)
	{
		if ( empty($vars['dataname']) && empty($vars['zAccountId']) )
		{
			throw new Exception(__METHOD__ ."() called without required args dataname|zAccountId: ". json_encode($vars));
			exit;
		}

		$due_info = array(
			'all_bills_due'     => 0.00,
			'bill_types_due'    => '',  // should be an array with Hosting|License|Hardware as the keys (but only add keys that have values)
			'earliest_past_due' => 0.00,
		);

		// Get Lavu payments for the account
		$this->zInt->zDataExtractor->init($vars['dataname']);
		$payment_events = $this->zInt->zDataExtractor->getPaymentEvents();
		$legacy_invoices_paid = $payment_events['legacy_invoices_paid'];

		// LP-2559 Getting Due Info outstanding balance directly from Zuora, per Zach - without honoring Lavu billing overpayments (since they should all be accounted for in Zuora by now)
		$zAccount = $this->zInt->getAccount($vars);
		$due_info['all_bills_due'] = $zAccount->Balance;

		// Tally total due amount from all unpaid Zuora invoices
		$zInvoices = $this->zInt->getUnpaidInvoices($vars);
		foreach ( $zInvoices as $zInvoice )
		{
			$unpaid_amount = (float) $zInvoice->Balance;

			// Use the oldest due date for the oldest past due zInvoice as the earliest_past_due date
			if ( empty($due_info['earliest_past_due']) || (strtotime($zInvoice->DueDate) < strtotime($due_info['earliest_past_due']) ) )
			{
				$due_info['earliest_past_due'] = $zInvoice->DueDate;
			}

			if ( $zInvoice->IncludesRecurring != '1' )
			{
				$due_info['bill_types_due']['Hardware'] += $unpaid_amount;
			}
			else
			{
				$due_info['bill_types_due']['Hosting'] += $unpaid_amount;
			}
		}

		return $due_info;
	}


	/**
	 * Helper method for getDueInfoArray() that retrieves the bill-related elements of the due_info component array
	 *
	 * @param array $vars $dataname Required keys: zAccountId
	 *
	 * @return array containing due_info array elements specific to bills
	 */
	public function getMonthlyRecurringAmount($vars)
	{
		if ( empty($vars['zAccountId']) )
		{
			throw new Exception(__METHOD__ ."() called without required args dataname|zAccountId: ". json_encode($vars));
			exit;
		}

		// Get Subscriptions
		$zSubscriptions = $this->zInt->getActiveSubscriptions($vars);

		// Get RatePlans
		$vars['where_clause'] = '';
		foreach ( $zSubscriptions as $zSubscription )
		{
			// We need a custom zoql where_clause to string multiple SubscriptionId's together with OR's
			$zoql = "SubscriptionId = '". $zSubscription->Id ."'";
			$vars['where_clause'] .= empty($vars['where_clause']) ? $zoql : ' OR '. $zoql;

			// We need a value in zSubscriptionId so getRatePlan() will pass the required arg check
			$vars['zSubscriptionId'] .= empty($vars['zSubscriptionId']) ? $zSubscription->Id : '|'. $zSubscription->Id;
			$vars['Name'] = $vars['zSubscriptionId'];
		}

		$zRatePlans = $this->zInt->getRatePlans($vars);

		// Get RatePlanCharges
		$vars['where_clause'] = '';
		foreach ( $zRatePlans as $zRatePlan )
		{

			// We need a custom zoql where_clause to string multiple RatePlan's together with OR's
			$zoql = "RatePlanId = '". $zRatePlan->Id ."'";
			$vars['where_clause'] .= empty($vars['where_clause']) ? $zoql : ' OR '. $zoql;

			// We need a value in zRatePlanChargeId so getRatePlanCharge() will pass the required arg check
			$vars['zRatePlanChargeIds'] .= empty($vars['zRatePlanChargeIds']) ? $zRatePlan->Id : ','. $zRatePlan->Id;
		}

		$zRatePlanCharges = $this->zInt->getRatePlanChargesByIds($vars);
		if (!is_array($zRatePlanCharges)) $zRatePlanCharges = array($zRatePlanCharges);

		// Process RatePlanCharges to sum RatePlanCharge.MRR for RatePlanCharges that are currently effective
		$recurring_amount = 0.00;
		$current_date_ts = strtotime(date('Y-m-d'));
		foreach ( $zRatePlanCharges as $zRatePlanCharge )
		{
			// Sum Monthly Recurring Revenue (MRR)
			if ( ($current_date_ts >= strtotime($zRatePlanCharge->EffectiveStartDate)) && (empty($zRatePlanCharge->EffectiveEndDate) || $current_date_ts < strtotime($zRatePlanCharge->EffectiveEndDate)) )
			{
				$recurring_amount += (float) $zRatePlanCharge->MRR;
			}
		}

		return $recurring_amount;
	}


	/**
	 * Retrieves the start date of the recurring Hosting Subscription
	 *
	 * @param array $vars $dataname Required keys: zAccountId
	 *
	 * @return array containing due_info array elements specific to bills
	 */
	public function getRecurringEffectiveDate($vars)
	{
		if ( empty($vars['zAccountId']) )
		{
			throw new Exception(__METHOD__ ."() called without required args dataname|zAccountId: ". json_encode($vars));
			exit;
		}

		// Get Subscriptions
		$zSubscriptions = $this->zInt->getActiveSubscriptions($vars);

		// Get RatePlans
		$vars['where_clause'] = '';
		foreach ( $zSubscriptions as $zSubscription )
		{
			// We need a custom zoql where_clause to string multiple SubscriptionId's together with OR's
			$zoql = "SubscriptionId = '". $zSubscription->Id ."'";
			$vars['where_clause'] .= empty($vars['where_clause']) ? $zoql : ' OR '. $zoql;

			// We need a value in zSubscriptionId so getRatePlan() will pass the required arg check
			$vars['zSubscriptionId'] .= empty($vars['zSubscriptionId']) ? $zSubscription->Id : '|'. $zSubscription->Id;
			$vars['Name'] = $vars['zSubscriptionId'];
		}

		$zRatePlans = $this->zInt->getRatePlans($vars);

		// Get RatePlanCharges
		$vars['where_clause'] = '';
		foreach ( $zRatePlans as $zRatePlan )
		{

			// We need a custom zoql where_clause to string multiple RatePlan's together with OR's
			$zoql = "RatePlanId = '". $zRatePlan->Id ."'";
			$vars['where_clause'] .= empty($vars['where_clause']) ? $zoql : ' OR '. $zoql;

			// We need a value in zRatePlanChargeId so getRatePlanCharge() will pass the required arg check
			$vars['zRatePlanChargeIds'] .= empty($vars['zRatePlanChargeIds']) ? $zRatePlan->Id : ','. $zRatePlan->Id;
		}

		$zRatePlanCharges = $this->zInt->getRatePlanChargesByIds($vars);
		if (!is_array($zRatePlanCharges)) $zRatePlanCharges = array($zRatePlanCharges);

		// Process RatePlanCharges to find the earliest RatePlanCharge.EffectiveStatrDate that contributes RatePlanCharge.MRR (which should mean it's for hosting)
		$earliest_start_date = '';
		foreach ( $zRatePlanCharges as $zRatePlanCharge )
		{
			// Get the (earliest) start date for any rateplan charges that contribute MRR
			if ( !empty($zRatePlanCharge->MRR) && (empty($earliest_start_date) || strtotime($zRatePlanCharge->EffectiveStartDate) < strtotime($earliest_start_date)) )
			{
				$earliest_start_date = $zRatePlanCharge->EffectiveStartDate;
			}
		}

		return $earliest_start_date;
	}


	/**
	 * Helper method for getDueInfoArray() that retrieves the bill-related elements of the due_info component array
	 *
	 * @param array $vars $dataname Required keys: zAccountId
	 *
	 * @return array containing due_info array elements specific to bills
	 */
	public function getMonthlyRecurringInfo($vars)
	{
		if ( empty($vars['zAccountId']) )
		{
			throw new Exception(__METHOD__ ."() called without required arg zAccountId: ". json_encode($vars));
			exit;
		}

		$recurring_amount = 0.00;
		$earliest_start_date = '';

		// Get Subscriptions
		$zSubscriptions = $this->zInt->getActiveSubscriptions($vars);

		$vars['where_clause'] = '';
		foreach ( $zSubscriptions as $zSubscription )
		{
			// We need a custom zoql where_clause to string multiple SubscriptionId's together with OR's
			$zoql = "SubscriptionId = '". $zSubscription->Id ."'";
			$vars['where_clause'] .= empty($vars['where_clause']) ? $zoql : ' OR '. $zoql;

			// We need a value in zSubscriptionId so getRatePlan() will pass the required arg check
			$vars['zSubscriptionId'] .= empty($vars['zSubscriptionId']) ? $zSubscription->Id : '|'. $zSubscription->Id;
			$vars['Name'] = $vars['zSubscriptionId'];
		}

		// Error checking for zSubscriptions
		if ( $zSubscriptions === null || count($zSubscriptions) === 0 )
		{
			error_log(__METHOD__ ."() found no zSubscriptions for vars=". json_encode($vars));
			return array($recurring_amount, $earliest_start_date);
		}

		// Get RatePlans
		$zRatePlans = $this->zInt->getRatePlans($vars);

		// Error checking for zRatePlans
		if ( $zRatePlans === null || count($zRatePlans) === 0 )
		{
			error_log(__METHOD__ ."() found no zSubscriptions for vars=". json_encode($vars));
			return array($recurring_amount, $earliest_start_date);
		}

		// Get RatePlanCharges
		$vars['where_clause'] = '';
		foreach ( $zRatePlans as $zRatePlan )
		{

			// We need a custom zoql where_clause to string multiple RatePlan's together with OR's
			$zoql = "RatePlanId = '". $zRatePlan->Id ."'";
			$vars['where_clause'] .= empty($vars['where_clause']) ? $zoql : ' OR '. $zoql;

			// We need a value in zRatePlanChargeId so getRatePlanCharge() will pass the required arg check
			$vars['zRatePlanChargeIds'] .= empty($vars['zRatePlanChargeIds']) ? $zRatePlan->Id : ','. $zRatePlan->Id;
		}

		$zRatePlanCharges = $this->zInt->getRatePlanChargesByIds($vars);
		if (!is_array($zRatePlanCharges)) $zRatePlanCharges = array($zRatePlanCharges);

		// Error checking for zRatePlanCharges
		if ( $zRatePlanCharges === null || count($zRatePlanCharges) === 0 )
		{
			error_log(__METHOD__ ."() found no zRatePlanCharges for vars=". json_encode($vars));
			return array($recurring_amount, $earliest_start_date);
		}

		// Process RatePlanCharges to sum RatePlanCharge.MRR for RatePlanCharges that are currently effective
		$current_date_ts = strtotime(date('Y-m-d'));
		foreach ( $zRatePlanCharges as $zRatePlanCharge )
		{
			// Sum Monthly Recurring Revenue (MRR)
			if ( ($current_date_ts >= strtotime($zRatePlanCharge->EffectiveStartDate)) && (empty($zRatePlanCharge->EffectiveEndDate) || $current_date_ts < strtotime($zRatePlanCharge->EffectiveEndDate)) )
			{
				$recurring_amount += (float) $zRatePlanCharge->MRR;
			}

			// Get the (earliest) start date for any rateplan charges that contribute MRR
			if ( !empty($zRatePlanCharge->MRR) && (empty($earliest_start_date) || strtotime($zRatePlanCharge->EffectiveStartDate) < strtotime($earliest_start_date)) )
			{
				$earliest_start_date = $zRatePlanCharge->EffectiveStartDate;
			}
		}

		return array($recurring_amount, $earliest_start_date);
	}


}

