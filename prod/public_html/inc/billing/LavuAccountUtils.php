<?php

require_once(dirname(__FILE__).'/../../admin/cp/resources/lavuquery.php');
require_once(dirname(__FILE__).'/../../admin/manage/globals/email_addresses.php');
require_once(dirname(__FILE__).'/../../admin/sa_cp/billing/package_levels_object.php');

class LavuAccountUtils
{
	private $mConn;
	private $o_package_container;
	private $o_emailAddressesMap;

	public function __construct()
	{
		// Set timezone
		date_default_timezone_set('America/Denver');

		// Connect to database
		$this->initDb();

		// Setup package levels object object
		global $o_package_container;
		if ( !isset($o_package_container) )
		{
			$o_package_container = new package_container();
		}
		$this->o_package_container = $o_package_container;

		global $o_emailAddressesMap;
		$this->o_emailAddressesMap = $o_emailAddressesMap;
	}

	private function initDb()
	{
		$this->mConn = ConnectionHub::getConn('poslavu');
	}

	/**
	 * Create and apply reseller license to existing distro-created Lavu account
	 *
	 * Required:
	 * dataname
	 * distro_code
	 * restaurantid
	 * resellerid
	 * package_level
	 * key (payment_request.key)
	 * license_amount
	 * points_amount
	 *
	 * Optional:
	 * license_date
	 * license_value
	 * firstname
	 * lastname
	 * address
	 * zip
	 * city
	 * state
	 * country
	 * reseller_lead_id
	 * salesforce_quote_id
	 * quote_pdf_filename
	 *
	 */
	public function activateResellerAccountFromDistroPortalQuote($vars)
	{
		$vars['package_name'] = $this->o_package_container->get_plain_name_by_attribute('level', $vars['package_level']);
		$vars['promo_name'] = '';
		$vars['promo_expiration'] = '';

		if ( empty($vars['license_value']) )
		{
			$vars['license_value'] = $this->o_package_container->get_value_by_attribute('level', $vars['package_level']);
		}

		if ( empty($vars['license_date']) )
		{
			$vars['license_date'] = date('Y-m-d H:i:s');
		}

		if ( empty($vars['points_amount']) )
		{
			$purchased_with_points = false;
			$collect_amount = $vars['license_amount'];
			$vars['notes'] = '';
		}
		else
		{
			$purchased_with_points = true;
			$collect_amount = '0';

			// Get the distro commission
			$reseller = $this->getDatabaseRow('reseller', $vars['dataname'], $vars);
			$vars['notes'] = "PURCHASED WITH POINTS, distro commission {$reseller['distro_license_com']}";
		}

		$this->getDatabaseRow('reseller_license-insert', $vars['dataname'], $vars);
		$vars['reseller_license_id'] = mlavu_insert_id();

		require_once(dirname(__FILE__) . "/../../../admin/distro/beta/exec/distro_user_functions.php");  // For add_to_distro_history()
		$wrote_reseller_history_event = add_to_distro_history("bought license", "type: {$vars['package_name']}\ncollected: {$collect_amount}\nvalue: {$vars['license_value']}", $vars['resellerid'], $vars['distro_code']);

		if ( $purchased_with_points )
		{
			$this->getDatabaseRow('payment_request-update_points_paid', $vars['dataname'], $vars);
		}

		// Change billing package level
		require_once(dirname(__FILE__) . "/../../admin/sa_cp/billing/payment_profile_functions.php");  // For change_package()
		if ( !change_package($vars['dataname'], $vars['dataname'], $vars['package_level'], $vars['restaurantid']) )
		{
			error_log(__FILE__ . " failed to update package level for dataname={$vars['dataname']} to package_level={$req_read['package_level']}");
			return false;
		}

		$sent_billing_email = $this->o_emailAddressesMap->sendEmailToAddress('billing_notifications', 'Changed Package Level', "{$vars['dataname']} successfully paid for Salesforce Quote ID {$vars['salesforce_quote_id']} ({$vars['quote_pdf_filename']}) so package level was changed to {$vars['package_name']} when Reseller License {$vars['reseller_license_id']} was applied.");

		// Create billing_log event
		$vars['billing_log_action'] = "Package was Changed to {$vars['package_name']}";
		$vars['billing_log_details'] = "changed package to {$vars['package_name']} by Billing System when customer paid Salesforce Quote ID {$vars['salesforce_quote_id']}";
		$this->getDatabaseRow('billing_log-insert', $vars['dataname'], $vars);

		// Update `payment_status` since process for applying license in distro portal does
		if ( $this->getDatabaseRow('payment_status-update_license_applied', $vars['dataname'], $vars) < 1 )
		{
			error_log(__FILE__ . " failed to update payment_status for dataname={$vars['dataname']} with license_applied={$vars['reseller_license_id']} license_type={$vars['package_name']} license_resellerid={$vars['resellerid']}");
			return false;
		}

		// Complete reseller_lead (based on process for applying license in distro portal)
		if ( !empty($vars['reseller_lead_id']) ) {
			if ( $this->getDatabaseRow('reseller_leads-update_license_applied', $vars['dataname'], $vars) < 1 )
			{
				error_log(__FILE__ . " failed to update reseller_leads for reseller_lead_id={$vars['reseller_lead_id']} with license_applied={$vars['license_date']} _finished=1 _canceled=0");
				return false;
			}
		}

		// Update reseller_quote status
		if ( $this->getDatabaseRow('reseller_quotes-update_status', $vars['dataname'], $vars) < 1 )
		{
			error_log(__FILE__ . " failed to update reseller_quotes status for reseller_quote_id={$req_read['reseller_quote_id']} to status=paid");
			return false;
		}

		if ( !$this->o_emailAddressesMap->sendEmailToAddress('sales_quote_notifications', 'Successful Quote Payment', "{$vars['dataname']} successfully paid for {$vars['payment_request_action']}") )
		{
			error_log(__FILE__ . " failed to send 'sales_quote_notifications' email for dataname={$vars['dataname']} {$vars['payment_request_action']}");
			return false;
		}

		return true;
	}

	public function createAccount($vars)
	{
		$response = array('success' => false, 'reason' => '');
		$valid_default_menus = array('default_restau', 'default_pizza_', 'defaultbar', 'default_coffee');

		// Default default_menu to default_restau
		if ( empty($vars['default_menu']) )
		{
			$vars['default_menu'] = 'default_restau';
		}

		// Check for required vars
		if ( empty($vars['default_menu']) || empty($vars['package_name']) || empty($vars['company']) || empty($vars['email']) )
		{
			$response['success'] = false;
			$response['reason'] = 'Missing required fields';
		}
		// Confirm valid value for default_menu
		else if ( !in_array($vars['default_menu'], $valid_default_menus) )
		{
			$response['success'] = false;
			$response['reason'] = 'Bad default menu: '. $vars['default_menu'];
		}
		// Create new Lavu account
		else
		{
			// This code was taken from /home/poslavu/public_html/register/controllers/activation-controller.php but had the reseller_leads bits removed

			// Convert the package name into the package level (ex. Silver3 => 21)
			$package_level = $this->o_package_container->get_level_by_attribute('name', $vars['package_name']);

			// Put revelevant fields into an array and they use the keys as column names for developing the insert sql for the signups table
			$account = array(
				'status'         => 'new',
				'date'           => date('Y-m-d'),
				'time'           => date('H:i:s'),
				'package'        => $package_level,
				'signup_package' => $package_level,
				'company'        => (isset($vars['company']) ? $vars['company'] : ''),
				'email'          => (isset($vars['email']) ? $vars['email'] : ''),
				'phone'          => (isset($vars['phone']) ? $vars['phone'] : ''),
				'firstname'      => (isset($vars['firstname']) ? $vars['firstname'] : ''),
				'lastname'       => (isset($vars['lastname']) ? $vars['lastname'] : ''),
				'address'        => (isset($vars['address']) ? $vars['address'] : ''),
				'city'           => (isset($vars['city']) ? $vars['city'] : ''),
				'state'          => (isset($vars['state']) ? $vars['state'] : ''),
				'zip'            => (isset($vars['zip']) ? $vars['zip'] : ''),
				'country'        => (isset($vars['country']) ? $vars['country'] : ''),
				'default_menu'   => (isset($vars['default_menu']) ? $vars['default_menu'] : ''),
				'ipaddress'      => (isset($vars['ip']) ? $vars['ip'] : $_SERVER['REMOTE_ADDR']),
				'sessionid'      => session_id(),
				'source'         => (isset($vars['source']) ? $vars['source'] : ''),
				'locale'         => (isset($vars['locale']) ? $vars['locale'] : ''),
				'salesforce_id'  => (isset($vars['salesforce_id']) ? $vars['salesforce_id'] : ''),
			);

			// Create signup record + set $rowid
			$insert_cols = '';
			$insert_vals = '';
			foreach ($account as $key => $val) {
				if ($key == 'locale') continue;
				$key = mysqli_real_escape_string($key);
				$val = mysqli_real_escape_string($val);
				$insert_cols .= empty($insert_cols) ? "`{$key}`"   : ", `{$key}`";
				$insert_vals .= empty($insert_vals) ? "'[{$key}]'" : ", '[{$key}]'";
			}
			$insert = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`signups` ({$insert_cols}) VALUES ({$insert_vals})", $account);
			if ($insert === false) return json_encode(array('successs' => false, 'reason' => mlavu_dberror()));
			$rowid = mlavu_insert_id();

			// Create the POS account
		    global $error_content;
			require_once(dirname(__FILE__).'/../../../register/create_account.php');
			$_POST['vf_signup'] = '1';
			$package_info = array(
				'package' => $vars['package_name'],  // package name not package level
				'ipods' => $this->o_package_container->get_ipod_limt_by_attribute('level', $package_level),
				'ipads' => $this->o_package_container->get_ipad_limt_by_attribute('level', $package_level),
			);
			$result = create_new_poslavu_account( $account['company'], $account['email'], $account['phone'], $account['firstname'], $account['lastname'], $account['address'], $account['city'], $account['state'], $account['zip'], $account['default_menu'], $account['locale'], 'Client', '', $package_info );

			// Prepare the response
			$response['success']      = isset($result[0]) ? $result[0] : '';
			$response['username']     = isset($result[1]) ? $result[1] : '';
			$response['password']     = isset($result[2]) ? $result[2] : '';
			$response['dataname']     = isset($result[3]) ? $result[3] : '';
			$response['restaurantid'] = isset($result[4]) ? $result[4] : '';
			$response['signupid']     = $rowid;
			if ( $response['success'] )
			{
				// Update signup row with dataname, password
				mlavu_query("update `poslavu_MAIN_db`.`signups` set `username`='[username]', `dataname`='[dataname]', `password`=AES_ENCRYPT('[password]','password') where `id`='[signupid]'", $response);

				// Ensure the required row in poslavu_MAINREST_db.rest_X exists
				$this->confirm_restaurant_shard_new($response['dataname']);
			}
		}

		return $response;
	}

	/**
	 * Code taken from /sa_cp/billing/billing.php
	 */
	public function cancelAccount($vars)
	{
		// Get restaurant row
		$restaurant = $this->getDatabaseRow('restaurants', $vars['dataname']);
		$vars['restaurantid'] = $restaurant['id'];

		// Get signup row
		$signup = $this->getDatabaseRow('signups', $vars['dataname']);
		if (count($signup) == 0)
		{
			$signup = array(array('success'=>'false'));
		}

		// Send cancel email
		$a_cancel_info = array(
			'dataname' => $vars['dataname'],
			'restaurantid' => $vars['restaurantid'],
			'canceled subscriptions' => '',
			'other subscriptions' => '',
			'signup' => $signup,
		);
		$this->o_emailAddressesMap->sendEmailToAddress('account_cancelations', 'account canceled', "information about the canceled account:\n\n".print_r($a_cancel_info, TRUE));

		// Get payment_status row
		$payment_status = $this->getDatabaseRow('payment_status', $vars['dataname']);
		if(!$payment_status || $payment_status['canceled']!="1")
		{
			$cvars = array();
			$cvars['canceled'] = 1;
			$cvars['canceled_arbs'] = '';
			$cvars['cancel_date'] = date("Y-m-d H:i:s");
			$cvars['cancel_info'] = 'Zuora webhook' . "|" . $_SERVER['REMOTE_ADDR'];
			$cvars['cancel_reason'] = 'Cancelled in Zuora';
			$cvars['cancel_notes'] = '';
			$cvars['restaurantid'] = $vars['restaurantid'];
			$cvars['history'] = "cancel_date: " . $cvars['cancel_date'] . "\ncancel_info: " . $cvars['cancel_info'] . "\ncancel_reason: " . $cvars['cancel_reason'] . "\ncancel_notes: " . $cvars['cancel_notes'] . "\n\n";
			$update_ps = mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `canceled`='[canceled]', `canceled_arbs`='[canceled_arbs]', `cancel_date`='[cancel_date]', `cancel_info`='[cancel_info]', `cancel_reason`='[cancel_reason]', `cancel_notes`='[cancel_notes]' where `restaurantid`='[restaurantid]'", $cvars);
		}
		$update_r = mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `disabled`='2' where `id`='[1]'",$vars['restaurantid']);

		// create a record in the billing log
		$a_insert_vars = array('datetime'=>date('Y-m-d H:i:s'), 'dataname'=>$vars['dataname'], 'restaurantid'=>$vars['restaurantid'], 'username'=>'zuora', 'fullname'=>'Zuora webhook', 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'action'=>'cancel account', 'details'=>$reason.' ('.$notes.')');
		$insert_bl = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` (`datetime`, `dataname`, `restaurantid`, `username`, `fullname`, `ipaddress`, `action`, `details`) VALUES ('[datetime]', '[dataname]', '[restaurantid]', '[username]', '[fullname]', '[ipaddress]', '[action]', '[details]')", $a_insert_vars); // cancel account
		if ( !$insert_bl) error_log(__FILE__ ." couldn't insert billing_log row for ". json_encode($a_insert_vars, true));

		/*// Salesforce integration - adapted the $show_trial_alert logic here to determine if this is a distro-created trial account being cancelled or just a vanilla cancellation.
		$salesforce_event_name = empty($payment_status['license_applied']) && !empty($payment_status['trial_start']) ? 'Cancelled - Trial' : 'Cancelled';
		SalesforceIntegration::logSalesforceEvent($vars['dataname'], $salesforce_event_name);*/

		return array('success' => ($update_ps && $update_r && $insert_bl));
	}

	/**
	 * Build due_info string using due_info_components array generated by ZuoraUtils->getDueInfoArray()
	 */
	public function getDueInfo($due_info_components)
	{
		// Prep passed-in args

		$all_bills_due        = empty($due_info_components['all_bills_due'])        ? 0.00 : $due_info_components['all_bills_due'];
		$bill_types_due       = empty($due_info_components['bill_types_due'])       ? 0.00 : $due_info_components['bill_types_due'];
		$earliest_past_due    = empty($due_info_components['earliest_past_due'])    ? 0.00 : $due_info_components['earliest_past_due'];
		$paid_last_two_weeks  = empty($due_info_components['paid_last_two_weeks'])  ? 0.00 : $due_info_components['paid_last_two_weeks'];
		$paid_last_four_weeks = empty($due_info_components['paid_last_four_weeks']) ? 0.00 : $due_info_components['paid_last_four_weeks'];
		$last_payment_made    = empty($due_info_components['last_payment_made'])    ? ''   : $due_info_components['last_payment_made'];
		$other_payments_total = empty($due_info_components['other_payments_total']) ? 0.00 : $due_info_components['other_payments_total'];

		// This code taken from Lavu billing

		$due_info = "";

		foreach($bill_types_due as $key => $val)
		{
			$due_info .= $key . ": $" . number_format($val,2) . "|";
		}
		$due_info .= "Total: $" . number_format($all_bills_due, 2);

		if($earliest_past_due!="")
			$due_info .= "|Due since " . $earliest_past_due;

		$due_info .= "|Paid last 15: $" . number_format($paid_last_two_weeks,2);
		$due_info .= "|Paid last 31: $" . number_format($paid_last_four_weeks,2);
		$due_info .= "|Last Payment Made: " . $last_payment_made;

		if(isset($other_payments_total))
			$due_info .= "|Unapplied Payments: " . $other_payments_total;

		return $due_info;
	}

	public function setDueInfo($dataname, $due_info_components)
	{
		if ( empty($dataname) )
		{
			$failed_message = __METHOD__ ."() called with missing dataname";
			error_log($failed_message);
			throw new Exception($failed_message);
		}

		// Build due_info string
		$due_info = $this->getDueInfo($due_info_components);

		// Get restaurant row
		$restaurant = $this->getDatabaseRow('restaurants', $dataname);
		$restaurantid = $restaurant['id'];

		// Check for existing payment_status row
		$dbargs = array('dataname' => $dataname, 'restaurantid' => $restaurantid, 'due_info' => $due_info);
		$payment_status = $this->getDatabaseRow('payment_status-nodie', $dataname);

		if ( $payment_status )
		{
			$update = $this->getDatabaseRow('payment_status-updatedueinfo', $dataname, $dbargs);
			if ( $update < 0 )
			{
				$this->o_emailAddressesMap->sendEmailToAddress('billing_errors', 'Failed payment_status Update', print_r($dbargs, true));
			}
		}
		else {
			$success = $this->getDatabaseRow('payment_status-insert', $dataname, $dbargs);
			if ( !$success )
			{
				$failed_message = "<br>Failed to create new payment_status for $restaurantid - $dataname<br>$due_info";
				error_log($failed_message);
				$this->o_emailAddressesMap->sendEmailToAddress('billing_errors', 'Failed payment_status Insert', print_r($dbargs, true));
				exit();
			}
		}

		//error_log("...dataname={$dataname} due_info={$due_info}");  //debug

		$this->recordDueInfoAsSystemSetting($restaurantid, $dataname, $due_info);
		$this->setPosPackageInfo($dataname);

		return array('success' => true, 'due_info' => $due_info);
	}

	/**
	 * Sets package_info config setting row in POS db
	 */
	public function setPosPackageInfo($dataname)
	{
		// Get signup row
		$signup = $this->getDatabaseRow('signups', $dataname);
		$payment_status = $this->getDatabaseRow('payment_status-nodie', $dataname);

		// Update what the app uses for lock out stuff, taken from /sa_cp/billing/procareas/process_payment_status.php
		$package_level = (empty($signup['package']) || $signup['package'] == 'none') ? '25' : $signup['package'];
		$package_display_name = $this->o_package_container->get_printed_name_by_attribute('level', $package_level);
		$package_name = $this->o_package_container->get_plain_name_by_attribute('level', $package_level);
		$ipad_limit = max($payment_status['custom_max_ipads'], $this->o_package_container->get_ipad_limt_by_attribute('level', $package_level));
		$ipod_limit = max($payment_status['custom_max_ipods'], $this->o_package_container->get_ipod_limt_by_attribute('level', $package_level));
		$tablesideIpadLimit = max($payment_status['tableside_max_ipads'], $this->o_package_container->getTablesideIpadLimtByAttribute('level', $package_level));

		// give lee-way for trial accounts
		if ($ipad_limit < 1) $ipad_limit = 20;
		if ($ipod_limit < 1) $ipod_limit = 20;
		if ($tablesideIpadLimit < 1){
			$tablesideIpadLimit = 20;
		}

		// LP-2559 -- Allow custom limits of 0, which will indicate app access is blocked (for reasons like reports access only accounts)
		if ($payment_status['custom_max_ipads'] === '0') $ipad_limit = '0';
		if ($payment_status['custom_max_ipods'] === '0') $ipod_limit = '0';
		if ($payment_status['tableside_max_ipads'] === '0'){
			$tablesideIpadLimit = '0';
		}

		$this->recordPackageInfoAsSystemSetting($dataname, $package_display_name, $ipod_limit, $ipad_limit,$tablesideIpadLimit);

		return array('package_level' => $package_level, 'package_name' => $package_name, 'package_display_name' => $package_display_name, 'ipod_limit' => $ipod_limit, 'ipad_limit' => $ipad_limit, 'tableside_limit' => $tablesideIpadLimit);
	}

	/**
	 * Confirms dataname has the row it needs in poslavu_MAINREST_db.rest_X otherwise inserts it
	 * (Taken from /home/poslavu/public_html/register/controllers/activation-controller.php - which was originally taken from auth_account.php using slightly different name (appended "_new") because file coudln't be included since it had executable code and not just function declarations.
	 */
	private function confirm_restaurant_shard_new($dataname)
	{
		$str = "";
		$letter = substr($dataname,0,1);
		if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
			$letter = $letter;
		else
			$letter = "OTHER";
		$tablename = "rest_" . $letter;

		$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$dataname);
		if(mysqli_num_rows($mainrest_query))
		{
			$str .= $letter . ": " . $dataname . " exists";
		}
		else
		{
			$str .= $letter . ": " . $dataname . " inserting";

			$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
			if(mysqli_num_rows($rest_query))
			{
				$rest_read = mysqli_fetch_assoc($rest_query);

				$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
				if($success) $str .= " success";
				else $str .= " failed";
			}
		}
		return $str;
	}

	/**
	 * From /sa_cp/billing/procareas/process_payment_status.php
	 */
	private function recordDueInfoAsSystemSetting($rest_id, $dataname, $due_info) {

		require_once(dirname(__FILE__)."/../../admin/sa_cp/billing/payment_profile_functions.php");

		// trial_status: 0 = not in trial, 1 = in trial, 2 = trial expired
		$sysset = array("due_ts"=>"", "due_date"=>"", "trial_status"=>"0");

		$get_status_info = mlavu_query("select `trial_end`, `license_applied`, `min_due_date`, `collection_mode`, `uncaptured_amount` from `poslavu_MAIN_db`.`payment_status` where `restaurantid` = '[1]'", $rest_id);
		if (mysqli_num_rows($get_status_info)) {
			$status_info = mysqli_fetch_assoc($get_status_info);
		}

		$current_ts = time();
		$due_ts = 0;

		if (($status_info['uncaptured_amount'] * 1) == 0) {

			$augment_extension = 0;
			switch (date("N")) {
				case 5: $augment_extension = 3; break;
				case 6: $augment_extension = 2; break;
				case 7: $augment_extension = 1; break;
				default: break;
			}

			if (isset($status_info['trial_end'])) {

				// Find out if their license has been waived
				$waived_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `type`='License' and `waived`='1' order by id desc", $dataname);
				$license_waived = (mysqli_num_rows($waived_query));
				if ($status_info['trial_end']!="" && $status_info['license_applied']=="" && !$license_waived) {

					$trial_due_parts = explode(" ", $status_info['trial_end']);
					$trial_due_date = $trial_due_parts[0];

					$due_parts = explode("-", $trial_due_date);
					if (count($due_parts) > 2) {

						$due_ts = mktime(0, 0, 0, $due_parts[1], $due_parts[2], $due_parts[0]);

						$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
						if ($diff_days <= -4) {
							$sysset['trial_status'] = ($diff_days >= 0)?"1":"2";
							$sysset['due_ts'] = ($current_ts + 1209600 + $augment_extension); // display alert for 14 days, starting at 4 days past trial expiration
							$sysset['due_date'] = date("Y-m-d", $sysset['due_ts']);
						}
					}
				}

				if ($sysset['trial_status'] == "0") {

					$min_due_date = $status_info['min_due_date'];

					$rest_created = "";
					$rest_query = mlavu_query("select `created` from `poslavu_MAIN_db`.`restaurants` where `data_name` = '[1]'", $dataname);
					if (mysqli_num_rows($rest_query)) {
						$rest_read = mysqli_fetch_assoc($rest_query);
						$rest_created_parts = explode(" ",$rest_read['created']);
						$rest_created = $rest_created_parts[0];
						if ($rest_created < "2012-01-01") $min_due_date = "2014-09-16";
					}

					$collection_mode = $status_info['collection_mode'];

					if (($rest_created > "2009-01-01" || $collection_mode=="on") && $collection_mode!="off") {

						$due_total = get_due_prop("total", $due_info);
						$due_license = get_due_prop("license", $due_info);
						$due_hosting = get_due_prop("hosting", $due_info);
						$due_since = get_due_prop("due", $due_info);
						$paid_last_15 = get_due_prop("paid last 15", $due_info);
						$paid_last_31 = get_due_prop("paid last 31", $due_info);
						$last_payment_made = get_due_prop("last payment made", $due_info);

						if ($due_total!="" && ($due_total * 1)>0) {

							$use_paid_last = 15;
							if ($use_paid_last == 31) {
								$paid_last_str = $paid_last_31;
								$paid_last_num = 31;
							} else {
								$paid_last_str = $paid_last_15;
								$paid_last_num = 15;
							}

							if ($due_total>0 && ($paid_last_str=="" || ($paid_last_str * 1)==0 || $min_due_date!="")) {

								$collection_due_date = ($due_since < "2014-03-20")?"2014-03-20":$due_since;

								if ($last_payment_made!="" && $min_due_date=="") {
									$lpm_parts = explode("-", $last_payment_made);
									$lpm_due_ts = mktime(0, 0, 0, $lpm_parts[1], ($lpm_parts[2] + $paid_last_num), $lpm_parts[0]);
									$lpm_due_date = date("Y-m-d", $lpm_due_ts);
									if ($collection_due_date < $lpm_due_date) $collection_due_date = $lpm_due_date;
								}

								$due_parts = explode("-", $collection_due_date);
								if (count($due_parts) > 2) {

									$due_ts = mktime(0, 0, 0, $due_parts[1], ($due_parts[2] + 5), $due_parts[0]);

									if (trim($min_due_date) != "") {
										$mdue = explode("-", $min_due_date);
										if (count($mdue) > 2) {
											$min_due_ts = mktime(0, 0, 0, $mdue[1], $mdue[2], $mdue[0]);
											if ($min_due_ts > $due_ts) $due_ts = $min_due_ts;
										}
									}

									$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
									if ($diff_days <= -1) {
										$sysset['due_ts'] = ($current_ts + 2592000 + $augment_extension); // display alert for 30 days starting at 1 day past actual due_ts
										$sysset['due_date'] = date("Y-m-d", $sysset['due_ts']);
									}
								}
							}
						}
					}
				}
			}
		}

		//$sysset = array("due_ts"=>"", "due_date"=>"", "trial_status"=>"0"); // Overriding because clients are getting locked out that should not -CF

		$rdb = "poslavu_".$dataname."_db";
		$get_locations = mlavu_query("SELECT `id`, `use_net_path`, `net_path` FROM `".$rdb."`.`locations`");
		if ($get_locations) {
			if (mysqli_num_rows($get_locations) > 0) {
				while ($loc_info = mysqli_fetch_assoc($get_locations)) {
					$check_config = mlavu_query("SELECT `id`, `value6` FROM `".$rdb."`.`config` WHERE `location` = '[1]' AND `type` = 'system_setting' AND `setting` = 'payment_status'", $loc_info['id']);
					if (mysqli_num_rows($check_config) > 0) {
						$c_info = mysqli_fetch_assoc($check_config);
						$u_vars = array();
						if ($sysset['due_ts'] == "") {
							$u_vars['id'] = $c_info['id'];
							$u_vars['value'] = "";
							$u_vars['value2'] = "";
							$u_vars['value3'] = "0";
							$u_vars['value4'] = "";
							$u_vars['value5'] = "";
							$u_vars['value6'] = "0";
						} else if ($c_info['value6'] != $due_ts) {
							$u_vars['id'] = $c_info['id'];
							$u_vars['value'] = $sysset['due_ts'];
							$u_vars['value2'] = $sysset['due_date'];
							$u_vars['value3'] = $sysset['trial_status'];
							$u_vars['value4'] = $current_ts;
							$u_vars['value5'] = date("Y-m-d H:i:s", $current_ts);
							$u_vars['value6'] = $due_ts;
						}
						if (count($u_vars) > 0) $update = mlavu_query("UPDATE `".$rdb."`.`config` SET `value` = '[value]', `value2` = '[value2]', `value3` = '[value3]', `value4` = '[value4]', `value5` = '[value5]', `value6` = '[value6]' WHERE `id` = '[id]'", $u_vars);					
					} else {
						$i_vars = array();
						$i_vars['location'] = $loc_info['id'];
						$i_vars['value'] = $sysset['due_ts'];
						$i_vars['value2'] = $sysset['due_date'];
						$i_vars['value3'] = $sysset['trial_status'];
						$i_vars['value4'] = $current_ts;
						$i_vars['value5'] = date("Y-m-d H:i:s", $current_ts);
						$i_vars['value6'] = $due_ts;
						$insert = mlavu_query("INSERT INTO `".$rdb."`.`config` (`location`, `type`, `setting`, `value`, `value2`, `value3`, `value4`, `value5`, `value6`) VALUES ('[location]', 'system_setting', 'payment_status', '[value]', '[value2]', '[value3]', '[value4]', '[value5]', '[value6]')", $i_vars);
					}

					if ((int)$loc_info['use_net_path']>0 && !strstr($loc_info['net_path'], "poslavu.com")) { // if lls, schedule config table sync
						$exist_query = mlavu_query("SELECT `id` FROM `".$rdb."`.`config` WHERE `setting` = 'table updated' AND `location` = '[1]' AND `value` = 'config'", $loc_info['id']);
						if (mysqli_num_rows($exist_query) == 0) mlavu_query("INSERT INTO `".$rdb."`.`config` (`setting`, `location`, `value`, `type`) VALUES ('table updated', '[1]', 'config', 'log')", $loc_info['id']);
					}
				}
			}
		} else error_log("billing show_billing_status_alert get_locations: ".mlavu_dberror());
	}

	/**
	 * From /sa_cp/billing/procareas/process_payment_status.php
	 */
	private function recordPackageInfoAsSystemSetting($dataname, $package_name, $ipod_limit, $ipad_limit,$tablesideIpadLimit=0) {
		$rdb = "poslavu_".$dataname."_db";
		$get_locations = mlavu_query("SELECT `id`, `use_net_path`, `net_path` FROM `".$rdb."`.`locations`");
		if ($get_locations) {
			if (mysqli_num_rows($get_locations) > 0) {
				while ($loc_info = mysqli_fetch_assoc($get_locations)) {
					$check_config = mlavu_query("SELECT `id` FROM `".$rdb."`.`config` WHERE `location` = '[1]' AND `type` = 'system_setting' AND `setting` = 'package_info'", $loc_info['id']);
					if (mysqli_num_rows($check_config) > 0) {
						$c_info = mysqli_fetch_assoc($check_config);
						$update = mlavu_query("UPDATE `".$rdb."`.`config` SET `value` = '[1]', `value2` = '[2]', `value3` = '[3]', `value4` = '[4]'  WHERE `id` = '[5]'", $package_name, $ipod_limit, $ipad_limit, $tablesideIpadLimit, $c_info['id']);
					} else{
						$insert = mlavu_query("INSERT INTO `".$rdb."`.`config` (`location`, `type`, `setting`, `value`, `value2`, `value3`, `value4`) VALUES ('[1]', 'system_setting', 'package_info', '[2]', '[3]', '[4]', '[5]')", $loc_info['id'], $sysset_package_name, $sysset_ipod_limit, $sysset_ipad_limit, $syssetTablesideIpadLimit);
					} 
					if ((int)$loc_info['use_net_path']>0 && !strstr($loc_info['net_path'], "poslavu.com")) { // if lls, schedule config table sync
						$exist_query = mlavu_query("SELECT `id` FROM `".$rdb."`.`config` WHERE `setting` = 'table updated' AND `location` = '[1]' AND `value` = 'config'", $loc_info['id']);
						if (mysqli_num_rows($exist_query) == 0) lavu_query("INSERT INTO `".$rdb."`.`config` (`setting`, `location`, `value`, `type`) VALUES ('table updated', '[1]', 'config', 'log')", $loc_info['id']);
					}
				}
			}
		} else error_log("process_payments setup_vars get_locations: ".mlavu_dberror());
	}


	public function getDatabaseRow($query_name, $dataname, $dbargs = null)
	{
		$return_single_row = true;
		$die_on_zero_rows  = true;
		$die_on_db_errors  = true;
		$update_or_insert  = false;

		if ( $dbargs === null )
		{
			$dbargs = array('dbtable' => $query_name, 'dataname' => $dataname);
		}

		switch ($query_name)
		{
			case 'restaurants':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `data_name` = '[dataname]' ORDER BY `id`";
				$return_single_row = true;
				break;

			case 'locations':
				$die_on_zero_rows  = false;
				$die_on_db_errors  = false;
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`[dbtable]` WHERE `_disabled` IN('', '0') ORDER BY `id` DESC";
				break;

			case 'restaurant_locations':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`restaurant_locations` WHERE `dataname` = '[dataname]' ORDER BY `id` DESC";
				break;

			case 'signups':
			case 'payment_status':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' ORDER BY `id`";
				break;

			case 'payment_status-nodie':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`payment_status` WHERE `dataname` = '[dataname]' ORDER BY `id`";
				$die_on_zero_rows  = false;
				break;

			case 'payment_status-updatedueinfo':
				$sql = "UPDATE `poslavu_MAIN_db`.`payment_status` SET `due_info`='[due_info]' WHERE `dataname`='[dataname]' AND `restaurantid`='[restaurantid]' LIMIT 1";
				$update_or_insert = true;
				break;

			case 'payment_status-update_license_applied':
				$sql = "UPDATE `poslavu_MAIN_db`.`payment_status` SET `license_applied`='[reseller_license_id]', `license_type`='[package_name]', `license_resellerid`='[resellerid]' WHERE `dataname`='[dataname]'";
				$update_or_insert = true;
				$die_on_db_errors = false;
				break;

			case 'payment_status-insert':
				$sql = "INSERT INTO `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`,`due_info`) values ('[restaurantid]','[dataname]','[due_info]')";
				$update_or_insert = true;
				break;

			case 'payment_responses':
				$return_single_row = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `match_restaurantid` = '[restaurantid]' AND `x_response_code` = '1' AND x_type = 'auth_capture' AND !(x_account_number = 'XXXX6781' AND x_description LIKE '%POSLavu -%') AND !(`batch_status` = 'N' AND `datetime` >= '2014-02-01' AND `datetime` < '2014-08-15' AND `batch_action` = '') ORDER BY `datetime`";
				break;

			case 'payment_responses_invoiceid':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `match_restaurantid` = '[restaurantid]' AND `x_response_code` = '1' AND x_type = 'auth_capture' AND !(x_account_number = 'XXXX6781' AND x_description LIKE '%POSLavu -%') AND !(`batch_status` = 'N' AND `datetime` >= '2014-02-01' AND `datetime` < '2014-08-15' AND `batch_action` = '') AND (`invoices_applied_to` LIKE '%[invoiceid]' OR `invoices_applied_to` LIKE '%[invoiceid]|%') ORDER BY `datetime`";
				break;

			case 'last_payment':
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`payment_responses` WHERE `match_restaurantid` = '[restaurantid]' AND `x_response_code` = '1' AND `x_type` IN('auth_capture', 'Check', 'Misc') AND !(x_account_number = 'XXXX6781' AND x_description LIKE '%POSLavu -%') AND !(`batch_status` = 'N' AND `datetime` >= '2014-02-01' AND `datetime` < '2014-08-15' AND `batch_action` = '') ORDER BY `datetime` DESC LIMIT 1";
				break;

			case 'custom_payments':
				$return_single_row = false;
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`payment_responses` WHERE `match_restaurantid` = '[restaurantid]' AND `x_response_code` = '1' AND `x_type` IN('Check', 'Misc') AND `datetime` >= '2016-01-01' ORDER BY `date`, `time`";
				break;

			case 'invoices':
				$return_single_row = false;
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' ORDER BY `due_date`";
				break;

			case 'upgrade-invoices':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$dbargs['end_date'] = empty($dbargs['end_date']) ? date('Y-m-d') : $dbargs['end_date'];
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'License' AND `due_date` > '[license_billed]' AND `due_date` >= '[start_date]' AND `due_date` < '[end_date]' ORDER BY `due_date`";
				//$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' AND `type` = 'License' ORDER BY `due_date`";
				break;

			case 'hardware-invoices':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$dbargs['end_date'] = empty($dbargs['end_date']) ? date('Y-m-d') : $dbargs['end_date'];
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'Hardware' AND `due_date` >= '[start_date]' AND `due_date` < '[end_date]' ORDER BY `due_date`";
				break;

			case 'last_license_invoice':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'License' AND `due_date` < '[before_date]' ORDER BY `due_date` DESC LIMIT 1";
				break;

			case 'last_hosting_invoice':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'Hosting' ORDER BY `due_date` DESC LIMIT 1";
				break;

			case 'monthly_parts':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$die_on_db_errors  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`monthly_parts` WHERE `dataname`='[dataname]' AND ((`mp_start_day` <= '[end_date]' OR `mp_start_day` = '') AND (`mp_end_day` >= '[start_date]' OR `mp_end_day` = ''))";
				break;

			case 'first_license_invoice':
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname` = '[dataname]' AND `type` = 'License' AND `due_date` = '[license_billed]' ORDER BY `due_date` LIMIT 1";
				break;

			case 'reseller':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`resellers` WHERE `username`='[distro_code]'";
				$return_single_row = false;
				break;

			case 'reseller_leads-update_license_applied':
				$sql = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `license_applied`='[license_date]',`_finished`='1',`_canceled`='0' WHERE `id`='[reseller_lead_id]'";
				$update_or_insert = true;
				break;

			case 'reseller_licenses_dataname':
				$return_single_row = false;
				$die_on_zero_rows  = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON (`poslavu_MAIN_db`.`restaurants`.`id` = `poslavu_MAIN_db`.`reseller_licenses`.`restaurantid`) WHERE `data_name` = '[dataname]' ORDER BY `reseller_licenses`.`id`";
				break;

			case 'reseller_license_by_dataname':
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON (`poslavu_MAIN_db`.`restaurants`.`id` = `poslavu_MAIN_db`.`reseller_licenses`.`restaurantid`) WHERE `data_name` = '[dataname]' ORDER BY `reseller_licenses`.`id` LIMIT 1";
				break;

			case 'reseller_license_on_date':
				$die_on_zero_rows = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` LEFT JOIN `poslavu_MAIN_db`.`restaurants` ON (`poslavu_MAIN_db`.`restaurants`.`id` = `poslavu_MAIN_db`.`reseller_licenses`.`restaurantid`) WHERE `data_name` = '[dataname]' AND DATE(`applied`) >= '[applied]' ORDER BY `reseller_licenses`.`id` LIMIT 1";
				break;

			case 'reseller_license-insert':
				$sql = "INSERT INTO `poslavu_MAIN_db`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`restaurantid`,`cost`,`value`,`purchased`,`applied`,`notes`,`promo_name`,`promo_expiration`) VALUES ('[package_name]','[resellerid]','[distro_code]','[restaurantid]','[license_amount]','[license_value]','[license_date]','[license_date]','[notes]','[promo_name]','[promo_expiration]')";
				$update_or_insert = true;
				break;

			case 'reseller_quote-resellerid+quote_pdf_filename':
				$sql = "SELECT resellerid, pdf_file AS quote_pdf_filename FROM `poslavu_MAIN_db`.`reseller_quotes` WHERE `distro_code`='[distro_code]' AND `dataname`='[dataname]' AND `status`='approved' AND `payment_link` LIKE '%?key=[key]%'";
				break;

			case 'reseller_quotes-update_status':
				$sql = "UPDATE `poslavu_MAIN_db`.`reseller_quotes` SET `status`='paid' WHERE `id`='[reseller_quote_id]' AND `status`='approved' LIMIT 2";
				$update_or_insert = true;
				break;

			case 'payment_request-by_refid':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`payment_request` WHERE `dataname`='[dataname]' AND `refid`='[zPaymentMethodId]'";
				break;

			case 'payment_request-update_points_paid':
				$sql = "UPDATE `poslavu_MAIN_db`.`payment_request` SET `points_paid`='paid' WHERE `key`='[key]'";
				$update_or_insert = true;
				break;

			case 'billing_1pmt':
				$return_single_row = false;
				$where_clause = isset($dbargs['dataname']) ? " r.`data_name` = '[dataname]'" : " r.`data_name` != '' AND `datetime` >= '2015-01-01'";
				$limit_clause = isset($dbargs['limit_clause']) ? ' LIMIT [limit_clause]' : '';
				$sql = "SELECT r.`data_name` as dataname, 1pmt.* FROM `billing_1pmt` 1pmt LEFT JOIN `restaurants` r ON (1pmt.`restaurant_id` = r.id) WHERE {$where_clause} ORDER BY 1pmt.`datetime` {$limit_clause}";
				break;

			case 'active_authnet_arbs':
				$return_single_row = false;
				$limit_clause = isset($dbargs['limit_clause']) ? ' LIMIT [limit_clause]' : '';
				$sql = "SELECT * FROM `authnet_899274_1378029` WHERE `Status` = 'Active' ORDER BY `Start Date` {$limit_clause}";
				break;

			case 'billing_log-insert':
				$sql = "INSERT INTO `poslavu_MAIN_db`.`billing_log` (`datetime`, `dataname`, `restaurantid`, `username`, `fullname`, `ipaddress`, `action`, `details`, `billid`) VALUES (now(), '[dataname]', '[restaurantid]', '', '[firstname] [lastname]', '', '[billing_log_action]', '[billing_log_details]', '')";
				$update_or_insert = true;
				break;

			default:
				die(__METHOD__ ." got unmapped table {$query_name} for dataname {$dataname}\n");
				break;
		}

		// Perform lavuquery-esque field substitutions (only supports array-based [fieldname] not [1] subs yet)
		foreach ( $dbargs as $key => $val )
		{
			$sql = str_replace('['.$key.']', $this->mConn->escapeString($val), $sql);
		}

		$result = $this->mConn->legacyQuery($sql);

		if ( $result === false && $die_on_db_errors )
		{
			die("Error with '{$query_name}' query: ". mlavu_dberror() ."\n");
		}
		else if ( $update_or_insert )
		{
			return ($this->mConn->affectedRows());
		}
		else if ( mysqli_num_rows($result) == 0 && $die_on_zero_rows )
		{
			die("No row for '{$query_name}' query for {$this->dataname}: sql={$sql} dbargs=". json_encode($dbargs) ."\n");
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}

}
