<?php

///require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
require_once('/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php');

if (!isset($o_package_container)) $o_package_container = new package_container();

class BillingChangeDetector
{
	public static function run($dataname, $count, $total_count)
	{
		global $o_package_container;

		echo "--- {$count}/{$total_count} {$dataname} ----------------------------------------\n";

		$runs = array();

		$signup = self::getDatabaseRow('signups', $dataname);
		$payment_status = self::getDatabaseRow('payment_status', $dataname);
		//$invoices = self::getDatabaseRow('hosting-invoices-desc-no-waived', $dataname);
		$invoices = self::getDatabaseRow('hosting-invoices-desc', $dataname);

		// Records default to OG Gold if the package level is empty so emulate that
		$current_package_level = empty($signup['package']) ? '2' : $signup['package'];

		// Fix for grassburger_2 (unique error)
		if ($current_package_level == '[4]') $current_package_level = '25';

		$package_level = $current_package_level;
		$package_name = $o_package_container->get_plain_name_by_attribute('level', $package_level);
		$printed_name = $o_package_container->get_printed_name_by_attribute('level', $package_level);
		$terminals = !empty($payment_status['custom_max_ipads']) ? $payment_status['custom_max_ipads'] : $o_package_container->get_ipad_limt_by_attribute('level', (int) $current_package_level);

		$prev_package_level = null;
		$prev_hosting_amount = null;
		$prev_invoice_date = null;
		$prev_terminals = null;

		$run_start_date = null;
		$run_end_date = null;

		//$terminals_at_date = self::getTerminalAtDateMap($dataname, $current_package_level);

		$first_run = true;
		$num_invoices = mysqli_num_rows($invoices);
		$i = 0;

		error_log("DEBUG: ...dataname={$dataname} num_invoices={$num_invoices}");  //debug

		// Iterate through the invoices backwards to look for changes in the Hosting amount or the number of terminals

		while ( $invoice = mysqli_fetch_assoc($invoices) )
		{
			$i++;

			$hosting_amount = (float) $invoice['amount'];
			$invoice_date = $invoice['due_date'];

			// demo_dna_consu had bad Hosting invoices for $500
			if ($hosting_amount === 500.00) $hosting_amount = $prev_hosting_amount;

			// For first loop iteration, just set prev vars to current amount to ensure no change is detected
			if (!isset($prev_package_level)) $prev_package_level = $current_package_level;
			if (!isset($prev_hosting_amount)) $prev_hosting_amount = $hosting_amount;
			if (!isset($prev_invoice_date)) $prev_invoice_date = $invoice_date;
			if (!isset($run_end_date)) $run_end_date = '';
			if (!isset($prev_terminals)) $prev_terminals = $terminals;

			//echo "DEBUG: i={$i} num_invoices={$num_invoices} invoice_date={$invoice_date} package_level={$package_level} terminals={$terminals} hosting_amount={$hosting_amount} change_detected=". ( $hosting_amount !== $prev_hosting_amount ) ."\n";  //debug

			// Cap off a run when we detect a change in the Hosting amount
			if ( $hosting_amount !== $prev_hosting_amount )
			{
				$prev_invoice_date = $prev_invoice_date;
				$prev_package_name = $o_package_container->get_plain_name_by_attribute('level', $prev_package_level);
				$prev_printed_name = $o_package_container->get_printed_name_by_attribute('level', $prev_package_level);
				$prev_terminals = $terminals;
				$change_type = self::getBillingChangeReason($package_level, $prev_package_level, $terminals, $prev_terminals);

				array_push($runs, array(
					'change_type' => $change_type,
					'package_level' => $prev_package_level,
					'package_name' => $prev_package_name,
					'printed_name' => $prev_printed_name,
					'start_date' => $prev_invoice_date,
					'end_date' => $run_end_date,
					'hosting_amount' => $prev_hosting_amount,
					'terminals' => $prev_terminals,
					'next_package_level' => $o_package_container->get_level_by_attribute('name', $package_name),
					'next_package_name' => $package_name,
				) );

				// Reset vars for next run
				$run_end_date = $prev_invoice_date;
				$package_level = self::findPackageLevelAtInvoiceDate($dataname, $prev_package_level, $invoice, $payment_status, $signup);
				$terminals = self::getTerminalsPriorToDate($dataname, $invoice_date, $package_level);
			}

			// First invoice
			if ( $i === $num_invoices )
			{
				$package_name = $o_package_container->get_plain_name_by_attribute('level', $package_level);
				$printed_name = $o_package_container->get_printed_name_by_attribute('level', $package_level);
				$change_type = '';
				array_push($runs, array(
					'change_type' => $change_type,
					'package_level' => $package_level,
					'package_name' => $package_name,
					'printed_name' => $printed_name,
					'start_date' => $invoice_date,
					'end_date' => $run_end_date,
					'hosting_amount' => $hosting_amount,
					'terminals' => $terminals,
					'next_package_level' => $o_package_container->get_level_by_attribute('name', $prev_package_name),
					'next_package_name' => $prev_package_name,
				) );
			}

			$prev_package_level = $package_level;
			$prev_hosting_amount = $hosting_amount;
			$prev_invoice_date = $invoice_date;
		}

		return $runs;
	}

	private static function getBillingChangeReason($package_level, $prev_package_level, $terminals, $prev_terminals)
	{
		$change_type = 'Hosting Change';

		if ( ($package_level != $prev_package_level) && ($terminals != $prev_terminals) )
		{
			$change_type = 'Package and Terminal Upgrade';
		}
		else if ( $package_level != $prev_package_level )
		{
			$change_type = 'Package Upgrade';
		}
		else if ( $terminals != $prev_terminals )
		{
			$change_type = 'Terminal Upgrade';
		}

		//echo "DEBUG: In ". __METHOD__ ."() with package_level={$package_level} prev_package_level={$prev_package_level} terminals={$terminals} prev_terminals={$prev_terminals} change_type={$change_type}\n";  //debug

		return $change_type;
	}

	private function findPackageLevelAtInvoiceDate($dataname, $prev_package_level, $invoice, $payment_status, $signup)
	{
		//echo "DEBUG: In ". __METHOD__ ."() with dataname={$dataname} prev_package_level={$prev_package_level} due_date={$invoice['due_date']}\n";  //debug

		global $o_package_container;

		$package_level = 0;
		$invoice_amount = (float) $invoice['amount'];

		// demo_dna_consu had bad Hosting invoices for $500
		if ($invoice_amount == '500') {
			return (int) $prev_package_level;
		}

		if ( !empty($invoice['package']) )
		{
			$package_level = $invoice['package'];
		}
		// Check for reseller license
		else if ( $payment_status['license_applied'] )
		{
			$reseller_licenses = self::getDatabaseRow('reseller_licenses_dataname', $dataname);
			while ( $reseller_license = mysqli_fetch_assoc($reseller_licenses) )
			{
				// Use package level from oldest reseller_license that was applied before our invoice
				if ( strtotime($reseller_license['applied']) <= strtotime($invoice['due_date']) )
				{
					$package_name_pieces = explode(' to ', $reseller_license['type']);
					$package_name = array_pop($package_name_pieces);  // Get the right-most array element

					// Map Silver Promo => Silver
					$package_name = str_replace(' Promo', '', $package_name);

					$package_level = $o_package_container->get_level_by_attribute('name', $package_name);
					echo "DEBUG: ... got package_name={$package_name} package_level={$package_level}\n";  //debug
					return $package_level;
				}
			}
		}

		if ( $invoice_amount === 29.95 )
		{
			$package_level = 1;
		}
		else if ( $invoice_amount === 49.95 )
		{
			$package_level = 2;
		}
		else if ( $invoice_amount === 99.95 )
		{
			$package_level = 3;
		}
		else if ( $invoice_amount === 88.00 || $invoice['amount'] === 8.00 )
		{
			$package_level = 8;
		}
		// Use dates to tell 2nd gen vs. 3rd gen core package levels apart from each other
		else if ( $invoice_amount === 39.00 && $signup['signup_timestamp'] < '2013-03-13' )
		{
			$package_level = 1;
		}
		else if ( $invoice_amount === 39.00 && $signup['signup_timestamp'] < '2013-11-20' )
		{
			$package_level = 11;
		}
		else if ( $invoice_amount === 39.00 )
		{
			$package_level = 21;
		}
		else if ( $invoice_amount === 79.00 && $signup['signup_timestamp'] < '2013-03-13' )
		{
			$package_level = 2;
		}
		else if ( $invoice_amount === 79.00 && $signup['signup_timestamp'] < '2013-11-20' )
		{
			$package_level = 12;
		}
		else if ( $invoice_amount === 79.00 )
		{
			$package_level = 22;
		}
		else if ( $invoice_amount === 149.00 && $signup['signup_timestamp'] < '2013-03-13' )
		{
			$package_level = 3;
		}
		else if ( $invoice_amount === 149.00 && $signup['signup_timestamp'] < '2013-11-20' )
		{
			$package_level = 13;
		}
		else if ( $invoice_amount === 149.00 )
		{
			$package_level = 23;
		}
		else if ( $invoice_amount === 0.00 )
		{
			$package_level = $prev_package_level;
		}
		// TO DO : Lavu + LavuEnterprise?
		else if ( $invoice_amount === 500.00 )
		{
			$package_level = 0;
		}
		else
		{
			echo "***ERROR: Could not determine package level for dataname={$dataname} invoice_amount={$invoice_amount} at date=". $invoice['due_date'] ."\n";
			$package_level = $prev_package_level;  // like Gold D
		}

		return $package_level;
	}

	public static function checkForTerminalChangeAtDate($dataname, $end_date)
	{
		$start_date = self::getMonthBefore($end_date);

		$billing_logs = self::getDatabaseRow('billing_log_terminals', $dataname, array('dataname' => $dataname, 'start_date' => $start_date, 'end_date' => $end_date));

		while ( $billing_log = mysqli_fetch_assoc($billing_logs) )
		{
			$has_terminal_change = true;

			// Parse billing_log action for $new_terminal_limit
			$new_terminal_limit = str_replace('Set Custom iPad Limit to ', '', $billing_log['action']);
			if ($new_terminal_limit == 'none') $new_terminal_limit = '0';
		}

		return array($has_terminal_change, $new_terminal_limit);
	}

	public static function getTerminalsPriorToDate($dataname, $end_date, $package_level)
	{
		global $o_package_container;

		$start_date = '1969-12-31';  // Epoch to have a date that will never preclude invoices

		$billing_logs = self::getDatabaseRow('billing_log_terminals', $dataname, array('dataname' => $dataname, 'start_date' => $start_date, 'end_date' => $end_date));

		//echo "DEBUG: In ". __METHOD__ ."() with end_date={$end_date} billing_logs=". mysqli_num_rows($billing_logs) ."\n";  //debug

		while ( $billing_log = mysqli_fetch_assoc($billing_logs) )
		{
			// Parse billing_log action for $new_terminal_limit
			$terminals = str_replace('Set Custom iPad Limit to ', '', $billing_log['action']);
			if ( $terminals == 'none' )
			{
				$new_terminal_limit = 0;
			}
		}

		if ( !isset($terminals) )
		{
			$terminals = $o_package_container->get_ipad_limt_by_attribute('level', (int) $package_level);
		}

		//echo "DEBUG: ...!isset(terminals)=". !isset($terminals) ." package_level={$package_level} terminals={$terminals}\n";  //debug

		return (int) $terminals;
	}

	public static function getMonthBefore($end_date)
	{
		return date( 'Y-m-d', strtotime("{$end_date} -1 month") );
	}

	public static function getDatabaseRow($query_name, $dataname, $dbargs = null)
	{
		$dbConn = ConnectionHub::getConn('poslavu');

		$return_single_row = true;
		$die_on_zero_rows = true;

		if ( !isset($dbargs) )
		{
			$dbargs = array('dbtable' => $query_name, 'dataname' => $dataname);
		}

		switch ($query_name)
		{
			case 'restaurants':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `data_name` = '[dataname]' ORDER BY `id`";
				break;

			case 'locations':
				$sql = "SELECT * FROM `poslavu_[dataname]_db`.`[dbtable]` WHERE `_disabled` IN('', '0') ORDER BY `id` DESC";
				break;

			case 'restaurant_locations':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`restaurant_locations` WHERE `dataname` = '[dataname]' AND `disabled` IN('', '0') ORDER BY `id` DESC";
				break;

			case 'signups':
			case 'payment_status':
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' ORDER BY `id`";
				break;

			case 'invoices':
				$return_single_row = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' ORDER BY `due_date`";
				break;

			case 'hosting-invoices-desc':
				$dbargs['dbtable'] = 'invoices';
				$return_single_row = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' AND `type` = 'Hosting' ORDER BY `due_date` DESC";
				break;

			case 'hosting-invoices-desc-no-waived':
				$dbargs['dbtable'] = 'invoices';
				$return_single_row = false;
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `dataname` = '[dataname]' AND `type` = 'Hosting' and `waived` IN('0', '') ORDER BY `due_date` DESC";
				break;

			case 'payment_responses':
				$return_single_row = false;
				global $restaurant;
				$dbargs['restaurantid'] = $restaurant['id'];
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`[dbtable]` WHERE `match_restaurantid` = '[restaurantid]' ORDER BY `datetime`";
				break;

			case 'reseller_licenses_dataname':
				$return_single_row = false;
				$sql = "SELECT `reseller_licenses`.* FROM `reseller_licenses` LEFT JOIN `restaurants` ON (`restaurants`.`id` = `reseller_licenses`.`restaurantid`) WHERE `data_name` = '[dataname]' ORDER BY `applied` DESC";
				break;

			case 'billing_1pmt':
				$return_single_row = false;
				$sql = "SELECT r.data_name as dataname, 1pmt.* FROM billing_1pmt 1pmt LEFT JOIN restaurants r ON (1pmt.restaurant_id = r.id) WHERE datetime >= '2015-01-01' ORDER BY 1pmt.datetime LIMIT 10";
				break;

			case 'billing_log_terminals':
				$return_single_row = false;
				$sql = "SELECT * FROM `billing_log` WHERE `action` LIKE 'Set Custom iPad Limit%' AND `dataname` = '[dataname]' AND `datetime` >= '[start_date]' AND `datetime` < '[end_date]' ORDER BY `id`";
				break;

			default:
				die(__METHOD__ ." got unmapped table {$dbtable} for dataname {$dataname}\n");
				break;
		}

		// Perform lavuquery-esque field substitutions (only supports array-based [fieldname] not [1] subs yet)
		foreach ( $dbargs as $key => $val )
		{
			$sql = str_replace('['.$key.']', $dbConn->escapeString($val), $sql);
		}

		$result = $dbConn->query($sql);

		if ( $result === false && $dbtable != 'locations' )
		{
			die($dbConn->last_error ."\n");
		}
		else if ( mysqli_num_rows($result) == 0 && $dbtable != 'locations' && !$die_on_zero_rows )
		{
			die("No {$dbtable} row for {$dataname}\n");
		}

		return ($return_single_row) ? mysqli_fetch_assoc($result) : $result;
	}

}
