<?php

require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");

function create_db_user($user,$pwd,$db=false)
{
	$userConn = ConnectionHub::getConn('poslavu');
	$success = $userConn->query('write', "CREATE USER '".$userConn->escapeString($user)."'@'%' IDENTIFIED BY '".$userConn->escapeString($pwd)."'", true);
	if($db)
	{
		grant_db_user($user,$db);
	}
	return $success;
}

function grant_db_user($user,$db)
{
	$userConn = ConnectionHub::getConn('poslavu');
	$query = "GRANT ALL PRIVILEGES ON `".str_replace("_","\\_",$userConn->escapeString($db))."`.* TO '".$userConn->escapeString($user)."'@'%'";
	$success = $userConn->query('write', $query, true);
	return $success;
}

function db_user_exists($username)
{
	$userConn = ConnectionHub::getConn('poslavu');
	$query = "select `user` from `mysql`.`user` where `user`='$username'";
	$user_query = $userConn->query('write', $query, true);
	return mysqli_num_rows($user_query);
}
