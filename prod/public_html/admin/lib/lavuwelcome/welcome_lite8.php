<?php session_start(); 
	require_once('../info_inc.php');
	require_once('lavu-lite/scripts/customlang.php');
	$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
?>

<!doctype HTML>
<html>
<head>
    <title>POSLavu welcome 8</title>
    <link rel="stylesheet" href="lavu-lite/styles/welcome_lite.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="lavu-lite/scripts/helpful_functions.js"></script>
    <script type="text/javascript">
    	
    	function process()
    	{
		    window.location = "_DO:cmd=close_overlay&action=reload_settings";
    	}
    	
    	preload_images(
			"lavu-lite/images/wizard_window.png",
			"lavu-lite/images/progbar_bg.png",
			"lavu-lite/images/progbar_fill.png",
			"lavu-lite/images/btn_next_off.png",
			"lavu-lite/images/btn_next_on.png",
			"lavu-lite/images/lavulite_logo.png",
			"lavu-lite/images/btn_x.png"
		);
    </script>
</head>

<body>
<div style='position: absolute; width: 100%; height: 100%;'>
	<div class="lavu_lite_logo"></div>
	<div class="btn_x" onclick='window.location = "_DO:cmd=close_overlay&action=reload_settings";'></div>
	
	<div class="progress_bar_bg"></div>
	<?php include_once("ll_pbar.php"); echo pbar(8);?>
	<div class="window">
		<div style="width: 100%; height: 100px;">
			<div class="text_field_label" style="color: grey;">
				<br />
				<br />
				For more assistance, please refer to your Office CP
			</div>
		</div>
		<br />
		<div style="width: 99.8%; height: 70px; border: 1px solid #cccccc; background-color: white;">
			<div style="margin: 0 auto; text-align: left; width: 300px;" onclick='window.location = "_DO:cmd=go_to_browser&url=http://help.lavu.com/products/5-lavu-lite";'>
				<br />
				<div class="arrow_green"></div>
				<div class="text_field_label" style="font-size: x-large;">ADVANCED GUIDE</div>
			</div>
		</div>
		<div style="width: 99.8%; height: 70px; border: 1px solid #cccccc; background-color: white">
			<div style="margin: 0 auto; text-align: left; width: 300px;" onclick='window.location = "_DO:cmd=go_to_browser&url=http://www.poslavu.com/en/ipad-pos-videos";'>
				<br />
				<div class="arrow_green"></div>
				<div class="text_field_label" style="font-size: x-large;">TUTORIALS</div>
			</div>
		</div>
	</div>
	<div class="page_title"><div class="check_mark_mdm check_on_mdm" style="position: absolute; left: 280px; top: 0px;"></div><div style="display: inline-block;"><?php echo strtoupper(trans("MISSION COMPLETE")); ?></div></div>
	<div class="btn_done" id="next" onclick='if(!this.disabled) process();' style="text-align: center;" ><div class="text_field_label" style="margin-top: 25px; font-size: xx-large;"><?php echo trans('DONE'); ?></div></div>
</div>
<script type="text/javascript">
	//document.getElementById('next').disabled = true;
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
</body>
</html>