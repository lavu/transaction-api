<?php session_start(); 
	require_once('../info_inc.php');
	require_once('lavu-lite/scripts/customlang.php');
	$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
?>

<!doctype HTML>
<html>
<head>
    <title>POSLavu welcome 2</title>
    <link rel="stylesheet" href="lavu-lite/styles/welcome_lite.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="lavu-lite/scripts/helpful_functions.js"></script>
    <script type="text/javascript">
    	
    	function process()
    	{
    		var pack_id = document.getElementById('lang').value;
    		var params = "pack_id=" + pack_id + "<?php echo "&" . $fwd_vars; ?>";
    		//alert(params);
    		
    		
    		$.ajax({
	    		url: 'lavu-lite/scripts/setlanguage.php',
	    		data: params,
	    		complete: function(jqXHR, textStatus){
		    		//alert('finished: ' + textStatus);
	    		},
	    		fail: function(jqXHR, textStatus){
		    		//alert('failed: ' + textStatus);
	    		},
	    		done: function(msg){
		    		//alert(msg);
	    		},
	    		async: false
    		});
	    	
	    	window.location = "_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite2.php&slide=left";
    	}
    		
    	preload_images(
    		"lavu-lite/images/language_bg.png",
    		"lavu-lite/images/text_field.png",
			"lavu-lite/images/wizard_window.png",
			"lavu-lite/images/progbar_bg.png",
			"lavu-lite/images/progbar_fill.png",
			"lavu-lite/images/btn_next_off.png",
			"lavu-lite/images/btn_next_on.png",
			"lavu-lite/images/lavulite_logo.png",
			"lavu-lite/images/btn_x.png"
		);
			
    </script>
</head>

<body>
<div style='position:absolute; width: 100%; height: 100%;'>
	<div class="lavu_lite_logo"></div>
	<div class="btn_x" onclick='window.location = "_DO:cmd=close_overlay&action=reload_settings";'></div>
	
	<div class="progress_bar_bg"></div>
	<?php include_once("ll_pbar.php"); echo pbar(1);?>
	<div class="window">
		<div class="lang">
			<select id="lang" class="select_box" style="margin-top: 50px; margin-left: 50px;">
				<?php
					$langs = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_packs`");
					if(mysqli_num_rows($langs))
					{
						while($row = mysqli_fetch_assoc($langs))
						{
							echo "<option value=\"" . $row['id'] . "\">" . $row['language'] . "</option>";
						}
					}
					//use_language_pack
				?>
			</select>
		</div>
	</div>
	
	<div class="page_title"><?php echo strtoupper(trans("LANGUAGE SELECT")); ?></div>
	<div class="btn_next next_on" id="next" onclick='if(!this.disabled) process();' ></div>
</div>
<script type="text/javascript">
	//document.getElementById('next').disabled = true;
	document.getElementById('lang').value = <?php 
		/*$loc_id = $_REQUEST['loc_id'];
		$param = "use_language_pack";
		$lang_query = "SELECT `$param` FROM `locations` WHERE `id`='$loc_id'";
		$lang_res = mlavu_query($lang_query);
		if(mysqli_num_rows($lang_res))
		{
			$lang_row = mysqli_fetch_assoc($lang_res);
			if(isset($lang_row[$param]))
				printf( $lang_row[$param]);
		}*/
		echo $pack_id;
	?>;
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
</body>
</html>