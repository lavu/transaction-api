<?php 
session_start(); 
require_once('../info_inc.php');
require_once('lavu-lite/scripts/customlang.php');
$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
	
?>

<!doctype HTML>
<html>
<head>
    <title>POSLavu welcome 5</title>
    <link rel="stylesheet" href="lavu-lite/styles/welcome_lite.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="lavu-lite/scripts/helpful_functions.js"></script>
    <script type="text/javascript">

	    function checkKey()
	    {
		    if(event.keyCode == 13)
		    {
			    var addmore = document.getElementById('add');
			    verifyItems(addmore);
		    }
	    }
    	function verifyItems(ele)
    	{
	    	
	    	if(verifyImage() && verifyPrice() && verifyTitle())
	    	{
	    		var title = document.getElementById('title').value;
	    		var price = document.getElementById('price').value;
	    		var img_name = document.getElementById('img_name').value;
	    		
	    		//$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
	    		
	    		var params = "title=" + title + "&price=" + price + "&img_name=" + img_name + "<?php echo "&" . $fwd_vars; ?>";
	    		//alert(params);
	    		
	    		
	    		$.ajax({
		    		url: 'lavu-lite/scripts/additem.php',
		    		data: params,
		    		complete: function(jqXHR, textStatus){
			    		//alert('finished: ' + textStatus);
		    		},
		    		fail: function(jqXHR, textStatus){
			    		//alert('failed: ' + textStatus);
		    		},
		    		done: function(msg){
			    		//alert(msg);
		    		},
		    		async: false
	    		});
	    	
	    	
	    		var add = document.getElementById('add');
	    		
		    	if(ele == add)
		    		window.location = "_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite6.php&slide=left"; //Will return to itself
	    	}
	    	var next = document.getElementById('next');
	    	if(ele == next)
		    		window.location = "_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite7.php&slide=left";	/**/
    	}
    	
    	function verifyImage()
    	{
	    	var img_name = document.getElementById('img_name');
	    	return img_name !== '';
    	}
    	
    	function verifyTitle()
    	{
	    	var title = document.getElementById('title');
	    	var titleCheck = document.getElementById('titlecheck');
	    	
	    	if(title.value !== '')
	    	{
		    	titleCheck.className = titleCheck.className.replace("off", "on");
	    	}
	    	else
	    	{
		    	titleCheck.className = titleCheck.className.replace("on", "off");
	    	}
	    	
	    	return title.value !== '';
    	}
    	
    	function verifyPrice()
    	{
	    	var price = document.getElementById('price');
	    	var priceCheck = document.getElementById('pricecheck');
	    	
	    	if(price.value !== '')
	    	{
		    	priceCheck.className = priceCheck.className.replace("off", "on");
	    	}
	    	else
	    	{
		    	priceCheck.className = priceCheck.className.replace("on", "off");
	    	}
	    	
	    	return price.value !== '';
    	}
    	
    	function upload()
    	{
	    	document.getElementById('upload').submit();
    	}
    	
    	preload_images(
			"lavu-lite/images/photo_placeholder.png",
			"lavu-lite/images/btn_takepic.png",
			"lavu-lite/images/title_win.png",
			"lavu-lite/images/check_off.png",
			"lavu-lite/images/check_on.png",
			"lavu-lite/images/btn_add_more.png",
			"lavu-lite/images/wizard_window.png",
			"lavu-lite/images/progbar_bg.png",
			"lavu-lite/images/progbar_fill.png",
			"lavu-lite/images/btn_next_off.png",
			"lavu-lite/images/btn_next_on.png",
			"lavu-lite/images/lavulite_logo.png",
			"lavu-lite/images/btn_x.png"
		);
    	
    </script>
</head>

<body>
<form id="upload" target="upload_result" enctype='multipart/form-data' method='post' action="lavu-lite/scripts/upload.php">
	<input type="hidden" id="dn" name="dn" value="<?php echo $_REQUEST['dn'];?>" />
	<input type="hidden" id="cc" name="cc" value="<?php echo $_REQUEST['cc'];?>" />
	<input type="hidden" id="loc_id" name="loc_id" value="<?php echo $_REQUEST['loc_id'] ?>" />
	<input type="hidden" id="server_id" name="server_id" value="<?php echo $_REQUEST['server_id'] ?>" />
	<input type="hidden" id="server_name" name="server_name" value="<?php echo $_REQUEST['server_name'] ?>" />
	
	<input type="hidden" name="return_id" value="photo" />
	<input type="file" id="upload_button" style="display: none;" name="image" onchange="if(this.value !== '') upload();" />
</form>

<div style='position: absolute; width: 100%; height: 100%;'>
	<div class="lavu_lite_logo"></div>
	<div class="btn_x" onclick='window.location = "_DO:cmd=close_overlay&action=reload_settings";'></div>
	
	<div class="progress_bar_bg"></div>
	<?php include_once("ll_pbar.php"); echo pbar(6);?>
	<div class="window">
		<div class="column" style="padding 25px;">&nbsp;
			<div style="top: 0px; position: relative;">
				<div class="item_display" id="photo" style="background: url('lavu-lite/images/photo_placeholder.png') no-repeat;"></div>
				<p></p>
				<iframe id="upload_result" name="upload_result" style="display: none;"></iframe>
				<input type="hidden" id="img_name" name="img_name" value="" />
				<div class="btn_takepic" onclick="document.getElementById('upload_button').click()" ><div style="bottom: 20px; left: 50px; position: absolute; font-size: small; text-align: center; width: 125px;"><?php echo strtoupper(trans("TAKE PICTURE")); ?></div></div>
			</div>
		</div>
		<div class="column" style="padding 25px; text-align: center;">
			<div style="top: 275px; position: relative;">
				<div class="text_field_label"><?php echo strtoupper(trans("ADD TITLE")); ?></div><br />
				<div class="check_mark check_off" id="titlecheck" style="display: inline-block;"></div>
				<div style="display: inline-block;"><input type="text" class="text_field2" id="title" name="title" oninput="verifyTitle();" /></div>
			</div>
		</div>
		<div class="column" style="padding 25px;">
			<div class="btn_add_more" id="add" onclick='verifyItems(this);'><div style="bottom: 30px; left: 50px; position: absolute; font-size: small; text-align: center; width: 125px;"><?php echo strtoupper(trans("ADD MORE")); ?></div></div>
			<div style="top: 275px; position: relative;">
				<div class="text_field_label"><?php echo strtoupper(trans("ADD PRICE")); ?></div><br />
				<div class="check_mark check_off" size="20" id="pricecheck" style="display: inline-block;" ></div>
				<div style="display: inline-block;"><input type="number" class="text_field2" id="price" name="price" pattern="/[0-9]\.?/gi" oninput="verifyPrice();" /></div>
			</div>
		</div>
		<div class="text_field_label" style="position: absolute; top: 25px; width: 100%; left: 0px; font-size: xx-large;"><?php echo strtoupper(trans("YOUR ITEMS")); ?></div>
	</div>
	<div class="page_title"><?php echo strtoupper(trans("ADD A MENU ITEM")); ?></div>
	<div class="btn_next next_on" id="next" onclick='verifyItems(this);' ></div>
</div>
<script type="text/javascript">
	//document.getElementById('next').disabled = true;
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
</body>
</html>