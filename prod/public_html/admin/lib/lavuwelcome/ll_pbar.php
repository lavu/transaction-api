<?php

function pbar($i)
{
	$result = "";
	$result .= "<div class=\"progress_bar_fill\" style=\"";
	$fcount = 0;
	if($handle = opendir('./')){
		while($entry = readdir($handle))
		{
			if(strstr($entry, "lite"))
				$fcount++;
		}
	}
	
	$result .= "width: " . (($i / ($fcount - 2))*100) . "%;";
	$result .= "\"></div>";
	
	return $result;
}
?>