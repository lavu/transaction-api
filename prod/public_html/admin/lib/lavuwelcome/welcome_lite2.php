<?php session_start(); 
	require_once('../info_inc.php');
	require_once('lavu-lite/scripts/customlang.php');
	require_once('lavu-lite/scripts/pullfield.php');

	$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
?>

<!doctype HTML>
<html>
<head>
    <title>POSLavu welcome 2</title>
    <link rel="stylesheet" href="lavu-lite/styles/welcome_lite.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="lavu-lite/scripts/helpful_functions.js"></script>
    <script type="text/javascript">
    
    	function checkKey()
    	{
	    	if(event.keyCode == 13)
	    	{
		    	process();
	    	}
    	}
    	
    	function checkTax()
    	{
	    	var taxRate = document.getElementById('tax-rate').value;
	    	document.getElementById('tax-rate').value = taxRate;
	    	var next = document.getElementById('next');
	    	var check = document.getElementById('tax-rate-check');
	    	if(taxRate !== "")
	    	{
	    		next.disabled = false;
		    	next.className = next.className.replace("off", "on");
		    	check.className = check.className.replace("off","on");
	    	}
	    	else
	    	{
		    	next.disabled = true;
		    	next.className = next.className.replace("on", "off");
		    	check.className = check.className.replace("on","off");
	    	}
    	}
    	
    	function isallowed(c)
    	{
	    	if((c >= '0' && c <= '9') || c === '.')
	    		return true;
	    	return false;
    	}
    	
    	function formatPercent()
    	{
	  
    	}
    	
    	function process()
    	{
    		var tax_rate =  $('#tax-rate').val();
    		if(tax_rate === '')
    			return;
	    	var params = "tax_rate=" + tax_rate + "<?php echo "&" . $fwd_vars; ?>";
	    		//alert(params);
	    		
	    		
	    		$.ajax({
		    		url: 'lavu-lite/scripts/settaxrate.php',
		    		data: params,
		    		complete: function(jqXHR, textStatus){
			    		//alert('finished: ' + textStatus);
		    		},
		    		fail: function(jqXHR, textStatus){
			    		//alert('failed: ' + textStatus);
		    		},
		    		done: function(msg){
			    		//alert(msg);
		    		},
		    		async: false
	    		});
	    		
	    		window.location = "_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite3.php&slide=left";
    	}
    	
    	preload_images(
    		"lavu-lite/images/text_field.png",
    		"lavu-lite/images/check_medium_off.png",
    		"lavu-lite/images/check_medium_on.png",
			"lavu-lite/images/wizard_window.png",
			"lavu-lite/images/progbar_bg.png",
			"lavu-lite/images/progbar_fill.png",
			"lavu-lite/images/btn_next_off.png",
			"lavu-lite/images/btn_next_on.png",
			"lavu-lite/images/lavulite_logo.png",
			"lavu-lite/images/btn_x.png"
		);
    </script>
</head>

<body>
<div style='position: absolute; width: 100%; height: 100%;'>
	<div class="lavu_lite_logo"></div>
	<div class="btn_x" onclick='window.location = "_DO:cmd=close_overlay&action=reload_settings";'></div>
	
	<div class="progress_bar_bg"></div>
	<?php include_once("ll_pbar.php"); echo pbar(2);?>
	<div class="window">
		<div style="margin-top: 10%;">
			<div class="text_field_label"><?php echo strtoupper(trans("TAX RATE")); ?></div>
			<div style="position: relative; display: inline-block;">
				<input type="number" id="tax-rate" class="text_field" size="10" style="text-align: center; display: inline-block;" placeholder="_ _ _ _ _" oninput="checkTax();" onblur="checkTax();" onkeyup="checkKey();" /><br />
				<div class="check_mark_mdm check_off_mdm" id="tax-rate-check" style="position: absolute; top: 32px; left: 40px;"></div>
				<div class="text_field_label" style="position: absolute; top:40px; left: 400px;">%</div>
				<div style="position: absolute; top: 16px; left: 90px; height: 68px; width: 1px; background-color: #cccccc;"></div>
			</div><br />
			<div class="text_field_label" style="font-size: small;">ex: 5.125</div>
		</div>
	</div>
	
	<div class="page_title"><?php echo strtoupper(trans("ENTER TAX RATE")); ?></div>
	<div class="btn_next next_on" id="next" onclick='if(!this.disabled) process();' ></div>
</div>
<script type="text/javascript">
	//document.getElementById('next').disabled = true;
	document.getElementById('tax-rate').value = <?php
		{ 
			$loc_id = $_REQUEST['loc_id'];
			$dn = $_REQUEST['dn'];
			$field = "taxrate";
			echo getField($field, $dn, $loc_id, "0") . " * 100";
		}
		
	?>;
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
</body>
</html>