<?php

	$type = (isset($_REQUEST['type']))?$_REQUEST['type']:"lavu_welcome";
	$route_to = "welcome.php";
	
	switch ($type) {
		case "lavu_help" : $route_to = "welcome.php"; break; // alternate paths to be determined
		case "lavu_new_features" : $route_to = "welcome.php"; break;
		default : break;
	}
	
	if ($route_to=="welcome.php" && isset($_REQUEST['lavu_lite']) && $_REQUEST['lavu_lite']=="1") $route_to = "welcome_lite.php";
		
	//$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
	
	//echo "$route_to?".$fwd_vars;
	
	require_once(dirname(__FILE__)."/".$route_to);

?>