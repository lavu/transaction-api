<?php session_start(); 
	require_once('../info_inc.php');
	require_once('lavu-lite/scripts/customlang.php');
	require_once('lavu-lite/scripts/pullfield.php');
	$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
?>

<!doctype HTML>
<html>
<head>
    <title>POSLavu welcome 3</title>
    <link rel="stylesheet" href="lavu-lite/styles/welcome_lite.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="lavu-lite/scripts/helpful_functions.js"></script>
    <script type="text/javascript">
    	function verifyTimeZone(){
	    	var tz = document.getElementById('Timezone').value;
	    	var next = document.getElementById('next');
	    	if(tz !== '')
	    	{
		    	next.disabled = false;
		    	next.className = next.className.replace("off", "on");
	    	}
	    	else
	    	{
		    	next.disabled = true;
		    	next.className = next.className.replace("on", "off");
	    	}
    	}
    	
    	function process()
    	{
    		encodeURI()
	    	var time_zone =  document.getElementById('Timezone').value;
	  
	    	time_zone = encodeURIComponent(time_zone);
	    	var params = "time_zone=" + time_zone + "<?php echo "&" . $fwd_vars; ?>";
	    		//alert(params);
	    	
	    		
    		$.ajax({
	    		url: 'lavu-lite/scripts/settimezone.php',
	    		data: params,
	    		complete: function(jqXHR, textStatus){
		    		//alert('finished: ' + textStatus);
	    		},
	    		fail: function(jqXHR, textStatus){
		    		//alert('failed: ' + textStatus);s
	    		},
	    		done: function(msg){
		    		//alert(msg);
	    		},
	    		async: false,
	    		type: "POST"
    		});
	    	
	    	window.location = "_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite4.php&slide=left";
    	}
    	
    	preload_images(
    		"lavu-lite/images/map_g.png",
			"lavu-lite/images/wizard_window.png",
			"lavu-lite/images/progbar_bg.png",
			"lavu-lite/images/progbar_fill.png",
			"lavu-lite/images/btn_next_off.png",
			"lavu-lite/images/btn_next_on.png",
			"lavu-lite/images/lavulite_logo.png",
			"lavu-lite/images/btn_x.png"
		);
    </script>
</head>

<body>
<div style='position:absolute; width: 100%; height: 100%;'>
	<div class="lavu_lite_logo"></div>
	<div class="btn_x" onclick='window.location = "_DO:cmd=close_overlay&action=reload_settings";'></div>
	
	<div class="progress_bar_bg"></div>
	<?php include_once("ll_pbar.php"); echo pbar(3);?>
	<div class="window">
		<div class="map">
			<div style="position: absolute; left: 100px; top: 100px;">
				<div class="text_field_label"><?php echo strtoupper(trans("SELECT")); ?></div>
				<select class="select_box" id='Timezone' name="Timezone" style="display: inline-block; text-align: text-top;" onchange="verifyTimeZone();">
				<?php
					$tzs = array();
				$tzs[""] = "";
				$tzs["Pacific/Midway"] = "(GMT-11:00) Midway Island, Samoa";
				$tzs["America/Adak"] = "(GMT-10:00) Hawaii-Aleutian";
				$tzs["Etc/GMT+10"] = "(GMT-10:00) Hawaii";
				$tzs["Pacific/Marquesas"] = "(GMT-09:30) Marquesas Islands";
				$tzs["Pacific/Gambier"] = "(GMT-09:00) Gambier Islands";
				$tzs["America/Anchorage"] = "(GMT-09:00) Alaska";
				$tzs["America/Ensenada"] = "(GMT-08:00) Tijuana, Baja California";
				$tzs["Etc/GMT+8"] = "(GMT-08:00) Pitcairn Islands";
				$tzs["America/Los_Angeles"] = "(GMT-08:00) Pacific Time (US & Canada)";
				$tzs["America/Denver"] = "(GMT-07:00) Mountain Time (US & Canada)";
				$tzs["America/Chihuahua"] = "(GMT-07:00) Chihuahua, La Paz, Mazatlan";
				$tzs["America/Dawson_Creek"] = "(GMT-07:00) Arizona";
				$tzs["America/Belize"] = "(GMT-06:00) Saskatchewan, Central America";
				$tzs["America/Cancun"] = "(GMT-06:00) Guadalajara, Mexico City, Monterrey";
				$tzs["Chile/EasterIsland"] = "(GMT-06:00) Easter Island";
				$tzs["America/Chicago"] = "(GMT-06:00) Central Time (US & Canada)";
				$tzs["America/New_York"] = "(GMT-05:00) Eastern Time (US & Canada)";
				$tzs["America/Havana"] = "(GMT-05:00) Cuba";
				$tzs["America/Bogota"] = "(GMT-05:00) Bogota, Lima, Quito, Rio Branco";
				$tzs["America/Caracas"] = "(GMT-04:30) Caracas";
				$tzs["America/Santiago"] = "(GMT-04:00) Santiago";
				$tzs["America/La_Paz"] = "(GMT-04:00) La Paz";
				$tzs["Atlantic/Stanley"] = "(GMT-04:00) Faukland Islands";
				$tzs["America/Campo_Grande"] = "(GMT-04:00) Brazil";
				$tzs["America/Goose_Bay"] = "(GMT-04:00) Atlantic Time (Goose Bay)";
				$tzs["America/Glace_Bay"] = "(GMT-04:00) Atlantic Time (Canada)";
				$tzs["America/St_Johns"] = "(GMT-03:30) Newfoundland";
				$tzs["America/Araguaina"] = "(GMT-03:00) UTC-3";
				$tzs["America/Montevideo"] = "(GMT-03:00) Montevideo";
				$tzs["America/Miquelon"] = "(GMT-03:00) Miquelon, St. Pierre";
				$tzs["America/Godthab"] = "(GMT-03:00) Greenland";
				$tzs["America/Argentina/Buenos_Aires"] = "(GMT-03:00) Buenos Aires";
				$tzs["America/Sao_Paulo"] = "(GMT-03:00) Brasilia";
				$tzs["America/Noronha"] = "(GMT-02:00) Mid-Atlantic";
				$tzs["Atlantic/Cape_Verde"] = "(GMT-01:00) Cape Verde Is.";
				$tzs["Atlantic/Azores"] = "(GMT-01:00) Azores";
				$tzs["Europe/Belfast"] = "(GMT) Greenwich Mean Time : Belfast";
				$tzs["Europe/Dublin"] = "(GMT) Greenwich Mean Time : Dublin";
				$tzs["Europe/Lisbon"] = "(GMT) Greenwich Mean Time : Lisbon";
				$tzs["Europe/London"] = "(GMT) Greenwich Mean Time : London";
				$tzs["Africa/Abidjan"] = "(GMT) Monrovia, Reykjavik";
				$tzs["Europe/Amsterdam"] = "(GMT+01:00) Amsterdam, Berlin, Bern";
				$tzs["Europe/Amsterdam"] = "(GMT+01:00) Rome, Stockholm, Vienna";
				$tzs["Europe/Belgrade"] = "(GMT+01:00) Belgrade, Bratislava";
				$tzs["Europe/Belgrade"] = "(GMT+01:00) Budapest, Ljubljana, Prague";
				$tzs["Europe/Brussels"] = "(GMT+01:00) Brussels, Copenhagen";
				$tzs["Europe/Brussels"] = "(GMT+01:00) Madrid, Paris";
				$tzs["Africa/Algiers"] = "(GMT+01:00) West Central Africa";
				$tzs["Africa/Windhoek"] = "(GMT+01:00) Windhoek";
				$tzs["Asia/Beirut"] = "(GMT+02:00) Beirut";
				$tzs["Africa/Cairo"] = "(GMT+02:00) Cairo";
				$tzs["Asia/Gaza"] = "(GMT+02:00) Gaza";
				$tzs["Africa/Blantyre"] = "(GMT+02:00) Harare, Pretoria";
				$tzs["Asia/Jerusalem"] = "(GMT+02:00) Jerusalem";
				$tzs["Europe/Minsk"] = "(GMT+02:00) Minsk";
				$tzs["Asia/Damascus"] = "(GMT+02:00) Syria";
				$tzs["Europe/Moscow"] = "(GMT+03:00) Moscow, St. Petersburg, Volgograd";
				$tzs["Africa/Addis_Ababa"] = "(GMT+03:00) Nairobi";
				$tzs["Asia/Tehran"] = "(GMT+03:30) Tehran";
				$tzs["Asia/Dubai"] = "(GMT+04:00) Abu Dhabi, Muscat";
				$tzs["Asia/Yerevan"] = "(GMT+04:00) Yerevan";
				$tzs["Asia/Kabul"] = "(GMT+04:30) Kabul";
				$tzs["Asia/Yekaterinburg"] = "(GMT+05:00) Ekaterinburg";
				$tzs["Asia/Tashkent"] = "(GMT+05:00) Tashkent";
				$tzs["Asia/Kolkata"] = "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi";
				$tzs["Asia/Katmandu"] = "(GMT+05:45) Kathmandu";
				$tzs["Asia/Dhaka"] = "(GMT+06:00) Astana, Dhaka";
				$tzs["Asia/Novosibirsk"] = "(GMT+06:00) Novosibirsk";
				$tzs["Asia/Rangoon"] = "(GMT+06:30) Yangon (Rangoon)";
				$tzs["Asia/Bangkok"] = "(GMT+07:00) Bangkok, Hanoi, Jakarta";
				$tzs["Asia/Krasnoyarsk"] = "(GMT+07:00) Krasnoyarsk";
				$tzs["Asia/Hong_Kong"] = "(GMT+08:00) Beijing, Chongqing";
				$tzs["Asia/Hong_Kong"] = "(GMT+08:00) Hong Kong, Urumqi";
				$tzs["Asia/Irkutsk"] = "(GMT+08:00) Irkutsk, Ulaan Bataar";
				$tzs["Australia/Perth"] = "(GMT+08:00) Perth";
				$tzs["Australia/Eucla"] = "(GMT+08:45) Eucla";
				$tzs["Asia/Tokyo"] = "(GMT+09:00) Osaka, Sapporo, Tokyo";
				$tzs["Asia/Seoul"] = "(GMT+09:00) Seoul";
				$tzs["Asia/Yakutsk"] = "(GMT+09:00) Yakutsk";
				$tzs["Australia/Adelaide"] = "(GMT+09:30) Adelaide";
				$tzs["Australia/Darwin"] = "(GMT+09:30) Darwin";
				$tzs["Australia/Brisbane"] = "(GMT+10:00) Brisbane";
				$tzs["Australia/Hobart"] = "(GMT+10:00) Hobart";
				$tzs["Asia/Vladivostok"] = "(GMT+10:00) Vladivostok";
				$tzs["Australia/Lord_Howe"] = "(GMT+10:30) Lord Howe Island";
				$tzs["Etc/GMT-11"] = "(GMT+11:00) Solomon Is., New Caledonia";
				$tzs["Asia/Magadan"] = "(GMT+11:00) Magadan";
				$tzs["Pacific/Norfolk"] = "(GMT+11:30) Norfolk Island";
				$tzs["Asia/Anadyr"] = "(GMT+12:00) Anadyr, Kamchatka";
				$tzs["Pacific/Auckland"] = "(GMT+12:00) Auckland, Wellington";
				$tzs["Etc/GMT-12"] = "(GMT+12:00) Fiji, Kamchatka, Marshall Is.";
				$tzs["Pacific/Chatham"] = "(GMT+12:45) Chatham Islands";
				$tzs["Pacific/Tongatapu"] = "(GMT+13:00) Nuku'alofa";
				$tzs["Pacific/Kiritimati"] = "(GMT+14:00) Kiritimati";
				
				foreach($tzs as $place => $code){
					echo "<option value=\"$place\">" . strtoupper(trans($code)) . "</option>";
				}
				?>
				</select>
			</div>
		</div>
	</div>
	<div class="page_title"><?php echo strtoupper(trans("TIMEZONE")); ?></div>
	<div class="btn_next next_on" id="next" onclick='if(!this.disabled) process();' ></div>
</div>
	<script type="text/javascript">
		//document.getElementById('next').disabled = true;
		document.getElementById('Timezone').value = <?php
			$field = "timezone";
			$dn = $_REQUEST['dn'];
			$loc_id = $_REQUEST['loc_id'];
			echo getField($field, $dn, $loc_id);
		?>;
	</script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
</body>
</html>