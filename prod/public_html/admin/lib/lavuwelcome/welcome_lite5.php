<?php session_start(); 
	require_once('../info_inc.php');
	require_once('lavu-lite/scripts/customlang.php');
	require_once('lavu-lite/scripts/pullfield.php');
	$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
?>

<!doctype HTML>
<html>
<head>
    <title>POSLavu welcome 4</title>
    <link rel="stylesheet" href="lavu-lite/styles/welcome_lite.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="lavu-lite/scripts/helpful_functions.js"></script>
    <script type="text/javascript">
    
    	var previousEle;
    	function swapSelected(ele)
    	{
	    	if(previousEle)
	    	{
		    	previousEle.selected = false;
		    	previousEle.className = previousEle.className.replace("on", "off");
	    	}
	    	
	    	previousEle = ele;
	    	ele.selected = true;
	    	ele.className = ele.className.replace("off", "on");
	    	checkType();
	    	
    	}
    
    	function checkType()
    	{
	    	var next = document.getElementById('next');
	    	var table = document.getElementById('table');
	    	var quick = document.getElementById('quick');
	    	var tab = document.getElementById('tab');
	    	
	    	if(table.selected || quick.selected || tab.selected)
	    	{
	    		next.disabled = false;
		    	next.className = next.className.replace("off", "on");
	    	}
	    	else
	    	{
		    	next.disabled = true;
		    	next.className = next.className.replace("on", "off");
	    	}
    	}
    	
    	function process()
    	{
	    	
	    	var options = Array('tab','quick','table');
	    	
	    	if(previousEle)
	    	{
	    		var option = previousEle.id;
	    		
	    		for(var i = 0; i < 3; i++)
	    		{
		    		if(option === options[i])
		    		{
			    		break;
		    		}
	    		}
	    		
	    		option = i+1;
	    		
	    		var params = "tab_view=" + option + "<?php echo "&" . $fwd_vars; ?>";
	    		//alert(params);
	    	
	    		
    		$.ajax({
	    		url: 'lavu-lite/scripts/setview.php',
	    		data: params,
	    		complete: function(jqXHR, textStatus){
		    		//alert('finished: ' + textStatus);
	    		},
	    		fail: function(jqXHR, textStatus){
		    		//alert('failed: ' + textStatus);s
	    		},
	    		done: function(msg){
		    		//alert(msg);
	    		},
	    		async: false,
	    		type: "POST"
    		});
    		
		    	window.location = "_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite6.php&slide=left";	
	    	}
    	}
    	
    	preload_images(
			"lavu-lite/images/layout_table_off.png",
			"lavu-lite/images/layout_table_on.png",
			"lavu-lite/images/layout_tab_off.png",
			"lavu-lite/images/layout_tab_on.png",
			"lavu-lite/images/layout_quick_off.png",
			"lavu-lite/images/layout_quick_on.png",
			"lavu-lite/images/wizard_window.png",
			"lavu-lite/images/progbar_bg.png",
			"lavu-lite/images/progbar_fill.png",
			"lavu-lite/images/btn_next_off.png",
			"lavu-lite/images/btn_next_on.png",
			"lavu-lite/images/lavulite_logo.png",
			"lavu-lite/images/btn_x.png"
		);
    </script>
</head>

<body>
<div style='position: absolute; width: 100%; height: 100%;'>
	<div class="lavu_lite_logo"></div>
	<div class="btn_x" onclick='window.location = "_DO:cmd=close_overlay&action=reload_settings";'></div>
	
	<div class="progress_bar_bg"></div>
	<?php include_once("ll_pbar.php"); echo pbar(5);?>
	<div class="window">
		<div style="margin-top: 10%;">
			<div class="layout table_off" id="table" onclick="swapSelected(this);"><div class="layout_label"><?php echo strtoupper(trans("TABLE")); ?></div></div>
			<div class="layout quick_off" id="quick" onclick="swapSelected(this);"><div class="layout_label"><?php echo strtoupper(trans("QUICKSERVE")); ?></div></div>
			<div class="layout tab_off" id="tab" onclick="swapSelected(this);"><div class="layout_label"><?php echo strtoupper(trans("TAB")); ?></div></div>
		</div>
	</div>
	<div class="page_title"><?php echo strtoupper(trans("SELECT YOUR BUSINESS TYPE")); ?></div>
	<div class="btn_next next_off" id="next" onclick='if(!this.disabled) process();' ></div>
</div>
<script type="text/javascript">
	document.getElementById('next').disabled = true;
	document.getElementById('table').selected = false;
	document.getElementById('quick').selected = false;
	document.getElementById('tab').selected = false;
	
	swapSelected(document.getElementById(<?php
		{	
			$field = tab_view;
			$dn = $_REQUEST['dn'];
			$loc_id = $_REQUEST['loc_id'];
			
			$val = getField($field, $dn, $loc_id,"1");
			$val = intval(str_replace('"', "", $val));
			switch($val)
			{
				case 1:
					echo '"tab"';	
					break;
				case 2:
					echo '"quick"';
					break;
				
				default:
					echo '"table"';
					break;
			}
		}
	?>));
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
</body>
</html>