<?php session_start(); 
	require_once('../info_inc.php');
	require_once('lavu-lite/scripts/customlang.php');
	$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
?>

<!doctype HTML>
<html>
<head>
    <title>POSLavu welcome 4</title>
    <link rel="stylesheet" href="lavu-lite/styles/welcome_lite.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="lavu-lite/scripts/helpful_functions.js"></script>
    <script type="text/javascript">
    	
    	function printerSetup()
    	{
	    	window.location = "_DO:cmd=printer_setup";
    	}
    	
    	function process()
    	{
		    window.location = "_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite8.php&slide=left";	
    	}
    	
    	preload_images(
			"lavu-lite/images/wizard_window.png",
			"lavu-lite/images/progbar_bg.png",
			"lavu-lite/images/progbar_fill.png",
			"lavu-lite/images/btn_next_off.png",
			"lavu-lite/images/btn_next_on.png",
			"lavu-lite/images/lavulite_logo.png",
			"lavu-lite/images/btn_x.png"
		);
    </script>
</head>

<body>
<div style='position: absolute; width: 100%; height: 100%;'>
	<div class="lavu_lite_logo"></div>
	<div class="btn_x" onclick='window.location = "_DO:cmd=close_overlay&action=reload_settings";'></div>
	
	<div class="progress_bar_bg"></div>
	<?php include_once("ll_pbar.php"); echo pbar(7);?>
	<div class="window">
		<div style="margin-top: 10%;">
			<div class="text_field" style="position: absolute; top: 150px; left: 275px;" onclick="printerSetup();"><div style="position: relative; top: 45px; width: 100%;"><?php echo strtoupper(trans("SETUP PRINTER")); ?></div></div>
		</div>
	</div>
	<div class="page_title"><?php echo strtoupper(trans("PRINTER SETUP")); ?></div>
	<div class="btn_next next_on" id="next" onclick='if(!this.disabled) process();' ></div>
</div>
<script type="text/javascript">
	//document.getElementById('next').disabled = true;
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
</body>
</html>