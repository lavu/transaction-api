<?php session_start(); 
	require_once('../info_inc.php');
	require_once('lavu-lite/scripts/customlang.php');
	require_once('lavu-lite/scripts/pullfield.php');
	$fwd_vars = "cc=".$_REQUEST['cc']."&dn=".$_REQUEST['dn']."&loc_id=".$_REQUEST['loc_id']."&server_id=".$_REQUEST['server_id']."&server_name=".$_REQUEST['server_name']."&type=".$_REQUEST['type']."&lavu_lite=".$_REQUEST['lavu_lite'];
?>

<!doctype HTML>
<html>
<head>
    <title>POSLavu welcome 3</title>
    <link rel="stylesheet" href="lavu-lite/styles/welcome_lite.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="lavu-lite/scripts/helpful_functions.js"></script>
    <script type="text/javascript">
    	function verifyTime(){
	    	var time = document.getElementById('time').value;
	    	var next = document.getElementById('next');
	    	if(time !== '')
	    	{
		    	next.disabled = false;
		    	next.className = next.className.replace("off", "on");
	    	}
	    	else
	    	{
		    	next.disabled = true;
		    	next.className = next.className.replace("on", "off");
	    	}
    	}
    	
    	function process()
    	{
	    	var time =  document.getElementById('time').value;
	    	time = time.replace(':', '');
	    	time = time.replace(" ", "");
	    	if(time.substring(time.length-2, time.length).toUpperCase() == "PM")
	    	{
		    	time = time.replace("PM", "");
		    	time = (time*1) + 1200;
	    	}
	    	else
	    	{
	    		time = time.replace("AM", "");
	    	}
	    	
	    	//time = encodeURIComponent(time);
	    	var params = "time=" + time + "<?php echo "&" . $fwd_vars; ?>";
	    	//alert(time);
	    		//alert(params);
	    	
	    		
    		$.ajax({
	    		url: 'lavu-lite/scripts/settime.php',
	    		data: params,
	    		complete: function(jqXHR, textStatus){
		    		//alert('finished: ' + textStatus);
	    		},
	    		fail: function(jqXHR, textStatus){
		    		//alert('failed: ' + textStatus);s
	    		},
	    		done: function(msg){
		    		//alert(msg);
	    		},
	    		async: false,
	    		type: "POST"
    		});
	    	
	    	window.location = "_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite5.php&slide=left";
    	}
    	
    	preload_images(
    		"lavu-lite/images/map_g.png",
			"lavu-lite/images/wizard_window.png",
			"lavu-lite/images/progbar_bg.png",
			"lavu-lite/images/progbar_fill.png",
			"lavu-lite/images/btn_next_off.png",
			"lavu-lite/images/btn_next_on.png",
			"lavu-lite/images/lavulite_logo.png",
			"lavu-lite/images/btn_x.png"
		);
    </script>
</head>

<body>
<div style='position:absolute; width: 100%; height: 100%;'>
	<div class="lavu_lite_logo"></div>
	<div class="btn_x" onclick='window.location = "_DO:cmd=close_overlay&action=reload_settings";'></div>
	
	<div class="progress_bar_bg"></div>
	<?php include_once("ll_pbar.php"); echo pbar(4);?>
	<div class="window">
		<div>
			<div style="position: absolute; left: 100px; top: 100px;">
				<div class="clock_bg"></div>
				<input class="select_box" type="time" id="time" name="time" style="position: absolute; top: 75px; left: 150px;" oninput="verifyTime();" />
			</div>
		</div>
	</div>
	<div class="page_title"><?php echo strtoupper(trans("LATEST STORE CLOSING HOUR")); ?></div>
	<div class="btn_next next_on" id="next" onclick='if(!this.disabled) process();' ></div>
</div>
	<script type="text/javascript">
		//document.getElementById('next').disabled = true;
		document.getElementById('time').value = <?php
			$field = "day_start_end_time";
			$dn = $_REQUEST['dn'];
			$loc_id = $_REQUEST['loc_id'];
			$time = getField($field, $dn, $loc_id, "1200");
			//echo "alert(" . $time . ");";
			$time = str_replace('"', '', $time);
			$ampm = "AM";
			if(intval($time) >= 1200)
			{
				$ampm = "PM";
				$time = (intval($time) - 1200)."";
				
			}
			
			$time = '"' . substr($time, 0, 2) . ":" . substr($time, 2, 2) . " " . $ampm . '"';
			echo $time;
			
		?>;
	</script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.14/jquery-ui.min.js"></script>
</body>
</html>