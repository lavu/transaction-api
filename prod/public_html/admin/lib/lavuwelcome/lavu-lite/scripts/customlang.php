<?php
$pack_id = 0;
	$active_language_pack;
	{
		global $pack_id;
		global $active_language_pack;
		global $active_language_pack_id;
		$loc_id = $_REQUEST['loc_id'];
		$dn = $_REQUEST['dn'];
		$lan_query = "SELECT `use_language_pack` FROM `poslavu_" . $dn . "_db`.`locations`";
		$lan_res = mlavu_query($lan_query);
		if(mysqli_num_rows($lan_res))
		{
			$lan_row = mysqli_fetch_assoc($lan_res);
			$pack_id = $lan_row['use_language_pack'];
		}
		$active_language_pack_id = $pack_id;
		$active_language_pack = getLanguageDictionary($loc_id);
		if (empty($active_language_pack)) {
			$active_language_pack = load_language_pack($pack_id, $loc_id);
			setLanguageDictionary($active_language_pack, $loc_id);
		}
	}
	
	function trans($str)
	{
		global $pack_id;
		
		$temp = speak($str);
		if($temp != $str)
			return $temp;
			
		if($pack_id)
		{
			$trans_query = "SELECT `translation` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `word_or_phrase`='" . $str ."' AND `pack_id`='" . $pack_id ."'";
			$trans_res = mlavu_query($trans_query);
			if(mysqli_num_rows($trans_res))
			{
				$trans_row = mysqli_fetch_assoc($trans_res);
				if($trans_row['translation'] != '')
					return $trans_row['translation'];
				return $str;
			}
			else
				return $str;
		}
		else
			return $str;
		
	}
?>