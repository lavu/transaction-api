<?php
return;
	session_start();
	require_once("/home/poslavu/private_html/rs_upload.php");
	$rootdir =  $_SERVER['DOCUMENT_ROOT'];
	print_r($_FILES);
	if(!(isset($_REQUEST['dn']) && isset($_FILES['image'])))
		quit();
	
	$dn = $_REQUEST['dn'];
	
	$dir = "/images/" . $dn . "/items/";
	
	$main_dir = "main/";
	$full_dir = "full/";
	$thumb_dir = "";
	
	$main_dir = $dir . $main_dir;
	$full_dir = $dir . $full_dir;
	$thumb_dir = $dir . $thumb_dir;
	
	//Specifies the Image Dimensions of the resized Images.
	$im_main_size = array(480,320);
	$im_thumb_size = array(120,80);
	
	//The uploaded imag'es original name. Used only for the exension of the original file.
	$image_name = basename($_FILES['image']['name']);
	
	$extn = explode(".", $image_name);
	$extn = strtolower($extn[count($extn) - 1]);
	
	$allowed_extn = Array("gif","jpg","jpe","jpeg","png");
	
	$is_allowed = in_array($extn, $allowed_extn);
	
	
	$basename = strtolower(date('YmdHis') . '.' . $extn); //This is where the Image's name is created
	
	if($is_allowed)
	{
		$uploadfile = $rootdir . $full_dir . $basename;
		echo $uploadfile . "\n";
		if(rs_move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile))
		{
			chmod($uploadfile,0775);
			
			$mainfile = $rootdir . $main_dir . $basename;
			$convertcmd = "convert '$uploadfile' -thumbnail '".$im_main_size[0]."x".$im_main_size[1].">' '$mainfile'";
			exec($convertcmd);
			chmod($mainfile,0775);
			
			$thumbfile = $rootdir . $thumb_dir . $basename;
			$convertcmd = "convert '$uploadfile' -thumbnail '".$im_thumb_size[0]."x".$im_thumb_size[1].">' '$thumbfile'";
			exec($convertcmd);
			chmod($thumbfile,0775);
		}
		
		$update_picture = $basename;
		
		$display =  $main_dir . $basename;
		$style = "background: url('" . $display ."') no-repeat; background-size: 100% 100%;";
		echo "<script type='text/javascript' >
				parent.document.getElementById('" . $_REQUEST['return_id'] . "').setAttribute(\"style\",\"" . $style .  "\");
				parent.document.getElementById('" . 'img_name' . "').setAttribute(\"value\",\"" . $basename ."\");
			  </script>";
	}
	else
	{
		echo "Not Allowed File Extension Type\n";
	}
?>
