<head>
    <title>POSLavu welcome 1</title>
    <link rel="stylesheet" href="welcome.css" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body style='-webkit-tap-highlight-color:rgba(0,0,0,0);'>
<table width="960" height="700" border="0" cellspacing="0" cellpadding="0" background="images/bg_alpha.png">
  <tr>
    <td height="480" align="center" valign="middle"><img src="images/w_3.jpg" width="698" height="480" alt="w1"></td>
  </tr>
  <tr>
    <td height="170"><table width="960" border="0" cellspacing="0" cellpadding="20" class="extext">
      <tr>
        <td colspan="3" align="center" valign="middle" class="bigtitle" >POSLavu Control Button</td>
      </tr>
      <tr>
        <td width="320" ><span class="bigletters">A: </span>Explore the Control Functions available from selecting the <strong>POSLavu Control Button</strong></td>
        <td width="320"><span class="bigletters">B: </span>The <strong>Reload Settings</strong> button will be used after you make changes to your Backend</td>
        <td width="320"><span class="bigletters">C: </span><strong>Control Functions</strong> differ from screen to screen. Explore, test and learn</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="50" bgcolor="#FFFFFF"><table width="120" border="0" align="right" cellpadding="0" cellspacing="0">
      <tr>
        <td align="right" class="next_button">NEXT</td>
        <td width="50" align="center"><a href="welcome4.php"><img src="images/arrownext.png" alt="next" width="40" height="40" border="0"></a></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
