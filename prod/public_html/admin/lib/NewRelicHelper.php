<?php

require_once(__DIR__."/../cp/resources/core_functions.php");

class NewRelicHelper
{
	private $request_start_ts;

	public function track($key, $val)
	{
		if (!extension_loaded("newrelic"))
		{
			return;
		}
		
		if (empty($key))
		{
			return;
		}
		
		newrelic_add_custom_parameter($key, $val);
	}
	
	public function trackRequestStart()
	{
		$this->request_start_ts = microtime(true);
		
		$this->track("app_name", reqvar("app_name", ""));
		$this->track("app_version", reqvar("app_version", ""));
		$this->track("app_build", reqvar("app_build", ""));
		$this->track("device_id", reqvar("UUID", ""));
		$this->track("yig_req_id", reqvar("YIG", ""));
		$this->track("xig_req_id", reqvar("XIG", ""));
		$this->track("req_start_ts", $this->request_start_ts);
	}
	
	public function trackRequestEnd()
	{
		$end_ts = microtime(true);
		$end_duration = round((($end_ts - $this->request_start_ts) * 1000), 2)."ms";

		$this->track("req_end_ts", $end_ts);
		$this->track("req_duration", $end_duration);		
	}
}

?>