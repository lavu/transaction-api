<?php
    //This file is also responsible for its own ajax requests.
    if(!empty($_GET['ajax'])){
        handleAjax();
        return;
    }

    //ini_set('display_errors', 1);
    require_once("/home/poslavu/public_html/admin/cp/resources/json.php");

    //error_log("dataname: " . $data_name);

    //We put everything we need across functions into a singleton.
    class Deliveries {
        public  $localizedDateTime;
        public  $localizedDateOnly;
        public  $oneWeekAgo;
        public  $dataname;
        private function __construct(){
            global $location_info, $data_name;
            $localTimeZone = $location_info['timezone'];
            $this->localizedDateTime = localize_datetime(date('Y-m-d H:i:s'), $localTimeZone);
            $this->oneWeekAgo = date('Y-m-d H:i:s', strtotime("- 8 day", strtotime($this->localizedDateTime)));//Bakers dozen, 8-day week.
            $this->localizedDateOnly = date('Y-m-d', strtotime($this->localizedDateTime));
            $this->dataname = $data_name;
        }
        public static function deliveries(){
            static $obj = null;
            if(!$obj){ $obj = new Deliveries(); }
            return $obj;
        }
    }

    //First, we pull all significant rows.
    $deliveryRowsFromOrdersTable = getOrderRowsWithDeliveriesToDisplay();
    //Second, we pull the JSON information from each `order`.`original_id` and change it into organized array.
    $deliveriesOrganizedIntoArray = parseDeliveryInformationFromOrderRows($deliveryRowsFromOrdersTable, $sortByDeliveryTime = true);
    //$display goes into the management area div.
    $display = buildMainPage($deliveriesOrganizedIntoArray);
    return;

    //Build the view (HTML)
    function buildMainPage($deliveriesArr){

        //MAIN GUI HTML
		$borderColor = "solid 2px #888888";
        $returnString  = '';
		$returnString .= "<style>";
		$returnString .= "	.dtitle { font-weight:bold; font-family:Verdana,Arial; color:#555555; text-align:center; vertical-align:bottom; border-bottom:$borderColor; } ";
		$returnString .= "	.dvalue {font-family:Verdana,Arial; border-bottom:solid 1px #aaaaaa; } ";
		$returnString .= "</style>";
        $returnString .= "<table cellspacing=0 cellpadding=4>";
        $returnString .= "<tr><td class='dtitle'>".speak("Delivery Time")."</td><td class='dtitle'>First Name</td><td class='dtitle'>Last Name</td><td class='dtitle'>Phone</td><td class='dtitle'>".speak("Street Addr").".</td><td class='dtitle'>".speak("City")."</td><td class='dtitle'>".speak("State")."</td><td class='dtitle'>".speak("Zip")."</td><td class='dtitle'>".speak("Delivered")."</td></tr>";
        foreach($deliveriesArr as $curr){
            //Used to create distinct id's
            $order_row_id = $curr['order_row_id'];
            $deliveryTimeBackColor = ( $curr['delivery_time'] < Deliveries::deliveries()->localizedDateTime ) ? "rgba(255,0,0,0.1)" : "rgba(0,255,0,0.1)";
            $returnString .= "<tr id='tr_$order_row_id' style='background-color:$deliveryTimeBackColor' lc_backgroundColor='$deliveryTimeBackColor' >";
            $deliveryTimeFormatted = date('m/d/Y h:i a', strtotime($curr['delivery_time']));
            $returnString .= "<td style='border-left:$borderColor' class='dvalue'>".$deliveryTimeFormatted."</td>";
            $returnString .= "<td class='dvalue'>".$curr['First_Name']."</td>";
            $returnString .= "<td class='dvalue'>".$curr['Last_Name']."</td>";
            $returnString .= "<td class='dvalue'>".$curr['Phone_Number']."</td>";
            $returnString .= "<td class='dvalue'>".$curr['Street_Address']."</td>";
            $returnString .= "<td class='dvalue'>".$curr['City']."</td>";
            $returnString .= "<td class='dvalue'>".$curr['State']."</td>";
            $returnString .= "<td class='dvalue'>".$curr['Zip_Code']."</td>";
            //The checkbox
            $deliveredAlreadyHTML = $curr['has_been_delivered'] === 'true' ? 'checked' : '';
            //error_log("order_row_id: " . $order_row_id);
			$returnString .= "<td style='border-right:$borderColor' class='dvalue' align='center'>".
                                "<input type='checkbox' style='width:24px; height:24px;' id='checkbox_$order_row_id' onchange='updateOrderOfDeliveryAsBeenSent($order_row_id);' $deliveredAlreadyHTML/>".
                            "</td>";
            $returnString .= "</tr>";
        }
		$returnString .= "<tr><td style='border-top:$borderColor' colspan='20'>&nbsp;</td></tr>";
        $returnString .= "</table>";

        //JAVASCRIPT
        $thisURL =  $_SERVER['SERVER_NAME'] . str_replace($_SERVER['DOCUMENT_ROOT'], '', __FILE__);
        $dataname = Deliveries::deliveries()->dataname;
        $sec_key = sha1($dataname . "pinto");
        $ajaxFunction  = <<<AJAXHEREDOC

            function updateOrderOfDeliveryAsBeenSent(orderID){
                var myTableRow = document.getElementById('tr_' + orderID);
                myTableRow.style.backgroundColor = '#ccc';
                var myCheckbox = document.getElementById('checkbox_' + orderID);
                var is_checked_str = myCheckbox.checked ? "true" : "false";
                var xmlhttp;
                if (window.XMLHttpRequest){
                    xmlhttp = new XMLHttpRequest();
                }else{
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange=function(){
                    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                        //alert('Response Text:' + xmlhttp.responseText);
                        if(xmlhttp.responseText.indexOf("success") > -1);
                            myTableRow.style.backgroundColor = myTableRow.getAttribute('lc_backgroundColor');
                    }
                };

                //var url = 'https://{$thisURL}?ajax=yeah';
                var url = 'management/deliveries.php?ajax=yeah';
                xmlhttp.open('POST', url, true);
                xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                var postArgs = 'data_name={$dataname}&order_row_id='+orderID+'&ajax=yeah&sec_key=$sec_key';
                postArgs += '&is_delivered_set_val=' + is_checked_str;
                xmlhttp.send(postArgs);
            }
AJAXHEREDOC;

        //error_log($ajaxFunction);
        $returnString .= "<script type='text/javascript'>";
        $returnString .= $ajaxFunction;
        $returnString .= "</script>";
        return $returnString;
    }

    //Pull from database
    function getOrderRowsWithDeliveriesToDisplay(){
        //Note `original_id` hold the actual delivery data.
        //$result = lavu_query("SELECT * FROM `orders` WHERE ( (`opened` <> '' AND `closed` < '01') OR (`reopen_datetime` <> '' AND `reclosed_datetime` < '01') ) AND `original_id` <> ''");
        $result = lavu_query("SELECT * FROM `orders` WHERE (`opened` > '[1]' OR `closed` >'[1]') AND `original_id`<>''", Deliveries::deliveries()->oneWeekAgo);
        if(!$result){
            error_log("Error in deliveries.php trying to get delivery orders");
        }
        $resultRows = array();
        while($currRow = mysqli_fetch_assoc($result)){
            $resultRows[] = $currRow;
        }
        return $resultRows;
    }

    //Organize/Process SQL results.
    function parseDeliveryInformationFromOrderRows($orderRows, $sortByDeliveryTime = true){
        $deliveriesByTime = array();
        foreach($orderRows as $currOrderRow){
            $customer_data_field = $currOrderRow['original_id'];
            $customer_data_field_Arr = explode('|o|', $customer_data_field);
            $fullJsonInfoStr = $customer_data_field_Arr[4];
            $hasBeenDelivered = $customer_data_field_Arr[5];
            $fullJsonInfoArr = LavuJson::json_decode($fullJsonInfoStr, 1);
            $deliveryArr = $fullJsonInfoArr['deliveryInfo'];
            $printInfo = $fullJsonInfoArr['print_info'];
            foreach($printInfo as $currCustField){
                $fields_db_name = $currCustField['fields_db_name'];
                $deliveryArr[$fields_db_name] = empty($currCustField['info']) ? '' : $currCustField['info'];
            }
            $deliveryArr['order_row_id'] = $currOrderRow['id'];
            $deliveryArr['has_been_delivered'] = $hasBeenDelivered;
            $i = 0;
            if( $deliveryArr['is_delivering'] ){
                $i++;
                $deliveriesByTime[$deliveryArr['delivery_time'] . $i] = $deliveryArr;
            }
        }
        ksort($deliveriesByTime);

        //New array excluding deliveries before today
        $pertinantDeliveries = array();
        foreach($deliveriesByTime as $currDelivery){
            if($currDelivery['delivery_time'] >= Deliveries::deliveries()->localizedDateOnly){
                $pertinantDeliveries[] = $currDelivery;
            }
        }

        return $pertinantDeliveries;
    }

    //Ajax handling functions below.
    function handleAjax(){
        if($_POST['sec_key'] != sha1($_POST['data_name'] . 'pinto')){
            echo 'sec_k_f';
            exit;
        }
        if( empty($_POST['data_name']) ){echo "missing_dataname"; exit;}
        if( empty($_POST['order_row_id']) ){echo "missing_order_id_row"; exit;}
        if( empty($_POST['is_delivered_set_val'])){echo "missing_is_delivered_set_val"; exit;}
        echo "Received-- dataname: " . $_POST['data_name'] . "   order_row_id: " . $_POST['order_row_id'] . "     is_delivered_set_val:" . $_POST['is_delivered_set_val'] . "    sec_key:".$_POST['sec_key'];
        setHasBeenDeliveredValueForOrder();
        return;
    }

    function setHasBeenDeliveredValueForOrder(){
        //Set up environment to allow for lavu_query().
        $_REQUEST['cc'] = $_POST['cc'] = $_GET['cc'] = $_POST['data_name'];
        require_once(dirname(__FILE__) . '/../info_inc.php');

        //Get the data
        $result = lavu_query("SELECT * FROM `orders` WHERE `id`='[1]'", $_POST['order_row_id']);
        if(!$result || mysqli_num_rows($result) == 0){
            error_log("Error trying to retrieve origial order row for updating delivery in: ".__FILE__."  mysql error: " . lavu_dberror());
            echo "db_select_error"; exit;
        }
        $order_row = mysqli_fetch_assoc($result);
        $dataStr = $order_row['original_id'];
        $dataArr = explode('|o|', $dataStr);
        $dataArr[5] = $_POST['is_delivered_set_val'];
        $dataUpdateStr = implode('|o|', $dataArr);

        //Update the data
        $result = lavu_query("UPDATE `orders` set `original_id`='[1]' WHERE `id` = '[2]'", $dataUpdateStr, $_POST['order_row_id']);
        if(!$result){
            error_log("Error trying to update original order row for updating deliver in: ".__FILE__."  mysql error: " . lavu_dberror());
            echo "db_update_error"; exit;
        }
        echo 'success'; exit;
    }
?>
