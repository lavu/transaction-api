<?php
$timezone = locationSetting("timezone");
if (!empty($timezone))
{
	$datetime = DateTimeForTimeZone($timezone);
}
else
{
	$datetime = date("Y-m-d H:i:s");
}

$dayStartEnd = $location_info['day_start_end_time'];
$dayStartEndLen = strlen($dayStartEnd);
if($dayStartEndLen == 2){
	$dayStartEnd = str_pad($dayStartEnd, 4, '0', STR_PAD_LEFT);
}
$start_time = str_split($dayStartEnd, 2);
$start_hours = $start_time[0];
$start_minutes = $start_time[1];
$currentdate=date('Y-m-d');
$day_start = date('Y-m-d H:i:s', strtotime('+'.$start_hours.' hour +'.$start_minutes.' minutes', strtotime($currentdate)));
$day_end =  date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 seconds', strtotime($day_start)));

$todaysDepositsQuery = lavu_query("SELECT SUM(amount) AS deposit_amount FROM bank_deposit WHERE created_date between '".$day_start."' and '".$day_end."'");
$todaysDeposits = mysqli_fetch_assoc($todaysDepositsQuery);

$lavuCalculatedDepositAmount = 0;
$availableDepositAmount = 0;
/* $totalSalesQuery = lavu_query("SELECT if(count(*) > 0, sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*
	order_contents.quantity - if(order_contents.quantity > '0', if(order_contents.discount_amount < (order_contents.price + order_contents.modify_price +
	order_contents.forced_modifiers_price), order_contents.discount_amount,order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price) +
	order_contents.idiscount_amount + order_contents.itax+`order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` +
	`order_contents`.`tax5`, 0)), '0.00') AS total FROM `orders` LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id`
	WHERE date(orders.closed) >= '".$day_end."' AND orders.void = '0' AND order_contents.item != 'SENDPOINT'"); */

$totalSalesQuery = lavu_query("SELECT sum(orders.total) AS total, sum(orders.refunded) AS refund FROM `orders` LEFT JOIN `cc_transactions` as cc ON `orders`.`order_id` = cc.`order_id` and cc.pay_type='Cash' and cc.action='Sale' where `orders`.closed between '".$day_start."' and '".$day_end."' and `orders`.void = '0'");
$totalSalesInfo = mysqli_fetch_assoc($totalSalesQuery);

$totalSalesInfo['total']=($totalSalesInfo['total'])?$totalSalesInfo['total']:'0.00';
$totalSalesInfo['refund']=($totalSalesInfo['refund'])?$totalSalesInfo['refund']:'0.00';

$totalSales['total'] = $totalSalesInfo['total'] - $totalSalesInfo['refund'];

$totalPaidInsQuery = lavu_query("SELECT order_id,SUM(total_collected) as amount  FROM cc_transactions WHERE order_id IN ('Paid In','Paid Out') AND datetime between '".$day_start."' and '".$day_end."' group by order_id");
if(mysqli_num_rows($totalPaidInsQuery)>0){
	while($row = mysqli_fetch_assoc($totalPaidInsQuery)){
		if($row['order_id'] == 'Paid In'){
			$totalPaidIns = $row['amount'];
		}
		else{
			$totalPaidOuts = $row['amount'];
		}
	}
}
if($totalPaidIns == ""){
	$totalPaidIns = 0;
}

if($totalPaidOuts == ""){
	$totalPaidOuts = 0;
}
$lavuCalculatedDepositAmount = $totalSales['total'] + ($totalPaidIns - $totalPaidOuts);
(float)$availableDepositAmount = $lavuCalculatedDepositAmount - $todaysDeposits['deposit_amount'];

if(isset($_POST['deposit_amount']) && $_POST['deposit_amount']!='')
{
	$get_names = lavu_query("SELECT id FROM `users` WHERE `username` = '[1]'", $_REQUEST['username']);
	if (mysqli_num_rows($get_names) > 0)
	{
		$info = mysqli_fetch_assoc($get_names);
		$user_id = $info['id']; 
	}
	 
	$_POST['deposit_amount'] = trim($_POST['deposit_amount']);

	$depositMessage = "<div style='color:#aecd37'>" . speak('Total Sale Amount');
	if((float)$_POST['deposit_amount'] > (float)$availableDepositAmount){
		$depositMessage .= " " . speak('Over');
	}else if((float)$_POST['deposit_amount'] < (float)$availableDepositAmount){
		$depositMessage .= " " . speak('Under');
	}else{
		$depositMessage .= " " . speak('Equal to');
	}
	$depositMessage .= " " . speak('Deposit') . "</div>";
	
	(float)$availableDepositAmount = (float)$availableDepositAmount - (float)$_POST['deposit_amount'];

	$data = array('amount' => $_POST['deposit_amount'], 'user_id'=>$user_id,'created_date' => $datetime, 'required_deposit' => $lavuCalculatedDepositAmount);
	$insertBd = lavu_query("INSERT INTO `bank_deposit` (`amount`, `user_id`,  `created_date`, `required_deposit`) 
	values('[amount]',  '[user_id]', '[created_date]', '[required_deposit]')", $data);
}

$display = "";

$decimal_char = $location_info['decimal_char'];
$thousands_char = ($decimal_char == ".")?",":".";
if ($location_info['left_or_right'] == "right"){
	$availableDepositAmount = number_format($availableDepositAmount, $location_info['disable_decimal'], $decimal_char, $thousands_char).$location_info['monitary_symbol'];
}else{
	$availableDepositAmount = $location_info['monitary_symbol'].number_format($availableDepositAmount, $location_info['disable_decimal'], $decimal_char, $thousands_char);
}

global $imagePath;
$display .= '<style>
body {
	font-family: arial, sans-serif;
}
/**********************Text input and text area styles********************/
		/*All text inputs*/
		input[type="text"], input[type="tel"] {
			border-radius: 2px;
			-webkit-border-radius: 2px;
			border: 1px solid #bbb;

			font-family: sans-serif, verdana;
			color:#333;
			margin: 0 2px;
			padding-left: 15px;

			cursor: pointer;
			
			background-color: #fafafa;
		}
		/*Hover states for above*/
		input[type="text"]:hover, input[type="tel"]:hover {
			 box-shadow: 0px 1px 3px #999;
		}
		/*Down states for above*/
		input[type="text"]:active, input[type="tel"]:active {
			-moz-box-shadow: inset 0 0 10px #999;
			-webkit-box-shadow: inset 0 0 10px #999;
			box-shadow: inset 0 0 10px #999;
		}
.btn_light_super_long {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:200px;
					height:37px;
					background:url(<?php echo "'.$imagePath.'""/btn_wow_250x37.png" ?>);
					border:hidden;
					padding-top:2px;
					padding-left:8px;
					text-align: center;
				}
		
		
		/*****************Large & medium action button styles**************/
		/*All Save buttons*/
        .btn_inactive {
            			border-radius: 2px;
            			-webkit-border-radius: 2px;
            			border:0 none !important;
            
            			width:10em;
            			height:3em;
            
            			font-family: sans-serif, verdana;
            			font-weight: 600;
            			font-size: 1em;
            			color: #fff;
            			text-transform: uppercase;
            			text-shadow: 0px 1px 3px #999;
            
            			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+51,99bc08+54,aecd37+100 */
            			background: #aecd37; /* Old browsers */
            			background: -moz-linear-gradient(top,  #9fa09a 0%, #9fa09a 51%, #9fa09a 54%, #9fa09a 100%); /* FF3.6-15 */
            			background: -webkit-linear-gradient(top,  #9fa09a 0%,#9fa09a 51%,#9fa09a 54%,#9fa09a 100%); /* Chrome10-25,Safari5.1-6 */
            			background: linear-gradient(to bottom,  #9fa09a 0%,#9fa09a 51%,#9fa09a 54%,#9fa09a 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#aecd37", endColorstr="#aecd37",GradientType=0 ); /* IE6-9 */
                        transition:all 0.3s;
            		}
		input[type="submit"], button[type="submit"] {
			border-radius: 2px;
			-webkit-border-radius: 2px;
			border:0 none !important;

			width:10em;
			height:3em;

			font-family: sans-serif, verdana;
			font-weight: 600;
			font-size: 1em;
			color: #fff;
			text-transform: uppercase;
			text-shadow: 0px 1px 3px #999;

			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+51,99bc08+54,aecd37+100 */
			background: #aecd37; /* Old browsers */
			background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 51%, #99bc08 54%, #aecd37 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#aecd37", endColorstr="#aecd37",GradientType=0 ); /* IE6-9 */

			transition:all 0.3s;
			
			cursor: pointer;
		}
		/*Hover states for all large & medium buttons*/
		input[type="submit"]:hover, button[type="submit"]:hover  {
			cursor: pointer;
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+92,99bc08+94,aecd37+100 */
			background: #aecd37; /* Old browsers */
			background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 92%, #99bc08 94%, #aecd37 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 92%,#99bc08 94%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 92%,#99bc08 94%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#aecd37", endColorstr="#aecd37",GradientType=0 ); /* IE6-9 */

			box-shadow: 0px 1px 3px #999;

			text-shadow: 0px 1px 3px #666;
		}
		/*Down states for above*/
		input[type="submit"]:active, button[type="submit"]:active  {
			-moz-box-shadow: inset 0 0 10px #999;
			-webkit-box-shadow: inset 0 0 10px #999;
			box-shadow: inset 0 0 10px #999;
		}
		 
</style>
<script>
function validateDepositBank(status)
{

	if(document.getElementById("deposit_amount_input").value == ""  || document.getElementById("deposit_amount_input").value<=0)
	{
		document.getElementById("message_depobank").innerHTML = "Please enter valid amount";
		return false;
	}

    if(status == 1){
        var msg = encodeURI("WARNING - You can only submit one deposit per day, are you sure you would like to proceed"); 
        window.location = "_DO:cmd=confirm&title=&js_cmd=bankdeposit_action()&message="+msg+"%3F";
        return false;
     }
}

function bankdeposit_action(){
    document.getElementById("form").submit();
}
</script>';
$restrict_deposit = 0;
if(isset($location_info['restrict_bank_deposit']) && $location_info['restrict_bank_deposit'] != 0){
    $restrict_deposit = $location_info['restrict_bank_deposit'];
}
$display .= "<div >
		<form id='form' method='post' action=''>
					<div id='background_div'>
						<div>
							<table cellpadding='8' style='text-align:center; border-collapse:collapse;'>";
                            if(isset($location_info['availability_bank_deposit']) && ($location_info['availability_bank_deposit'] == 0)){
                             $display .= "<tr>
                							<td style='border:1px solid #777777'>
                								<p>
                								Available Amount to be deposited
                								</p>
                								<span id='available_amount'>".$availableDepositAmount."</span>
                								<br>
                							</td>
			                           </tr> ";
                                 }
                   $display .= "<tr>
									<td>
										<p>
										Please enter amount for deposit
										</p> 
										<input class='btn_light_super_long' type='tel'".use_number_pad_if_available('deposit_amount_input', 'Deposit Amount', 'money')."  name='deposit_amount' id='deposit_amount_input' placeholder='" . speak('Amount') . "'/> 
										<br>
									</td>
								</tr>
								<tr>
									<td><div id='message_depobank'>".$depositMessage."</div></td>
								</tr>
								<tr>
									<td align='center' style='padding:6px 0px 0px 0px;'>";
                                    $currentDayDepositsQuery = lavu_query("SELECT count(*) as count FROM bank_deposit WHERE created_date between '".$day_start."' and '".$day_end."'");
                                    $currentDayDeposits =  mysqli_fetch_assoc($currentDayDepositsQuery);
                                    
                                    if($restrict_deposit == 1 && $currentDayDeposits['count'] != 0){
                                       $display .= "<button class='btn_inactive' type='button'><b>" . speak('Submit') . "</b></button>";
                                    }
                                    else{
                                       $display .= "<button class='btn_light' type='submit' onclick='return validateDepositBank(\"".$restrict_deposit."\")'><b>" . speak('Submit') . "</b></button>";
                                    }
						              $display .= "</td>
								</tr>
							</table> 
						</div>
					</div>
				</form>";
 
$display .= '</div>';
?>
