<?php
	$devBackupStr = strpos(__FILE__, "/dev/") ? "../" : "";
	require_once(dirname(__FILE__)."/../../".$devBackupStr."cp/resources/json.php");
	require_once(dirname(__FILE__)."/../../components/payment_extensions/extfunctions.php");
	require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');

	$imagePath = strpos(__FILE__, "/dev/")  ? "../../../dev/components/payment_extensions/lavugift/images" : "../../../components/payment_extensions/lavugift/images";
	$money_symbol = "'$'";
	// $server_id = save_reqvar("server_id");
	// $server_name = save_reqvar("server_name");


	if($on_ipad){
		echo "<script> setTimeout( function(){ window.location = '_DO:cmd=set_scrollable&set_to=0'; }, 400 ); </script>";
	}

	$chain_id = "";
	
	$data_mode = "";
	$account_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $data_name);
	if(mysqli_num_rows($account_query) > 0)
	{
		$account_read = mysqli_fetch_assoc($account_query);
		$chain_id = $account_read['chain_id'];
	}

	error_log($_POST['card_id']);
	if(!isset($_POST['card_id']) || empty($_POST['card_id'])) {
		$data = getCardList();
		$data_mode = "CardList";
	}
	else {
		$card_id = scrubInput($_POST['card_id']);
		// error_log("Card Number: " . $card_id);
		
		$data = getCardHistory();
		$data_mode = "CardHistory";
	}

	$display = getHistoryPage();

	function getCardList() {
		global $chain_id;
		global $data_name;
		$data = Array();
		
		if(strlen($chain_id) > 1) {
			#load all dataname existing in chain
			$chainedDataname = array();
			$chainedRestaurants =get_restaurants_by_chain($chain_id );	#get all chained restaurants
			foreach($chainedRestaurants as $restaurants ) {
				$chainedDataname[] = $restaurants['data_name'];
			}
			$select_query = mlavu_query('SELECT `chainid`, `name`, `balance`,`id`, `created_datetime`, `points`, `expires`, `history`, `dataname`  FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE (`dataname` IN("'.implode('","', $chainedDataname).'") OR `chainid`="[1]") AND `deleted` = 0 AND `inactive` = 0 ORDER BY id DESC LIMIT 0,100', $chain_id);
		}
		else {
			$select_query = mlavu_query('SELECT `chainid`, `name`, `balance`,`id`, `created_datetime`, `points`, `expires`, `history`, `dataname`  FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `dataname`="[1]" AND deleted = 0 AND `inactive` = 0 ORDER BY id DESC LIMIT 0,100', $data_name);
		}
		
		if(mysqli_num_rows($select_query) > 0)
		{
			while($card_read = mysqli_fetch_assoc($select_query)) {
				$data[] = $card_read;
			}
		}
		return $data;

	}

	function getCardHistory() {
		global $card_id;
		$data = Array();
		$select_query = mlavu_query('SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_history` WHERE `card_id`="[1]"', $card_id);	
		if(mysqli_num_rows($select_query) > 0)
		{
			while($card_read = mysqli_fetch_assoc($select_query)) {
				$data[] = $card_read;
			}
		}
		return $data;
	}

	function scrubInput($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	function getStyleSheet() {
		global $imagePath;
		ob_start();
		?>
			<style type='text/css'>

				#background_div {
					width:800px;
					height:600px;
					background-color:#eeeeee;
					border-radius: 10px;
				}
				#input_text_id {
					width:700px;
					height:10%;
					font-size:16px;
				}
				.btn_light {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:80px;
					height:36px;
					background:url(<?php echo '"'.$imagePath.'/btn_wow_80x37.png"' ?>);
					border:hidden;
					padding-top:3px;
				}

				.btn_light_short {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:37px;
					height:36px;
					background:url(<?php echo '"'.$imagePath.'/btn_wow_37x37.png"' ?>);
					border:hidden;
					padding-top:1px;
				}	

				.btn_light_long {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:167px;
					height:37px;
					background:url(<?php echo '"'.$imagePath.'/btn_wow_167x37.png"' ?>);
					border:hidden;
					padding-top:2px;
				}

				.btn_light_super_long {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:250px;
					height:37px;
					background:url(<?php echo '"'.$imagePath.'/btn_wow_250x37.png"' ?>);
					border:hidden;
					padding-top:2px;
					padding-left:8px;
					text-align: center;
				}

				#data_div {
					/*background-color:#ddaaaa;*/
					height:560px;
					overflow:scroll;
					margin:5px;
				}

				#table {
					margin:5px;
				}

				.header_td {
					cursor:pointer;
					background-color:#cacaca;
				}
				.header_td:hover {
					background-color:#adadad;
				}

				.data_tr {
					cursor: pointer;
					background-color:#efefef;
				}
				.active_data:hover {
					background-color:#dadada;
				}

				.data_td {
					border:1px solid black;
					text-align: left;
				}
			</style>
		<?php
		return ob_get_clean();
	}

	function getHistoryPage() {
		global $data;
		global $data_mode;
		global $money_symbol;
		global $data_mode;

		$json_data = json_encode($data);

		ob_start();
			?>
			<html>
				<?php echo getStyleSheet(); ?>

				<script type='text/javascript'>
					<?php
						echo "var money_symbol = ".$money_symbol.";\n";
						echo "var data = ".$json_data.";\n";
						echo "var data_mode = '".$data_mode."';\n";
						if($data_mode == 'CardList') {
							echo "var col_data = {";
							echo "'name':{'display':true, 'name':'Card #', 'width':'250px', 'numeric':true, 'display_type':'text'},";
							echo "'balance':{'display':true, 'name':'Gift Balance', 'width':'125px', 'numeric':true, 'display_type':'money'},";
							echo "'points':{'display':true, 'name':'Loyalty Points', 'width':'125px', 'numeric':true, 'display_type':'text'},";
							echo "'created_datetime':{'display':true, 'name':'Creation Date', 'width':'250px', 'numeric':false, 'display_type':'text'},";
							echo "'expires':{'display':false, 'name':'', 'width':'', 'numeric':true, 'display_type':'text'},";
							echo "'history':{'display':false, 'name':'', width:'', 'numeric':false, 'display_type':'text'},";
							echo "'id':{'display':false, 'name':'', 'width':'', 'numeric':true, 'display_type':'text'},";
							echo "'dataname':{'display':false, 'name':'', 'width':'', 'numeric':true, 'display_type':'text'},";
							echo "'chainid':{'display':false, 'name':'', 'width':'', 'numeric':true, 'display_type':'text'}";
							echo "};\n";
						}
						else if($data_mode == 'CardHistory') {
							echo "var col_data = {";
							echo "'datetime':{'display':true, 'name':'Timestamp', 'width':'200px', 'numeric':true, 'display_type':'text'},";
							echo "'action':{'display':true, 'name':'Action', 'width':'200px', 'numeric':true, 'display_type':'text'},";
							echo "'previous_points':{'display':true, 'name':'Previous Points', 'width':'50px', 'numeric':true, 'display_type':'text'},";
							echo "'previous_balance':{'display':true, 'name':'Previous Balance', 'width':'50px', 'numeric':true, 'display_type':'text'},";
							echo "'point_amount':{'display':true, 'name':'Point Change', 'width':'50px', 'numeric':true, 'display_type':'text'},";
							echo "'balance_amount':{'display':true, 'name':'Balance Change', 'width':'50px', 'numeric':true, 'display_type':'text'},";
							echo "'new_points':{'display':true, 'name':'Updated Points', 'width':'50px', 'numeric':true, 'display_type':'text'},";
							echo "'new_balance':{'display':true, 'name':'Updated Balance', 'width':'50px', 'numeric':true, 'display_type':'text'},";
							echo "'server_id':{'display':true, 'name':'Server ID', 'width':'50px', 'numeric':true, 'display_type':'text'},";
							echo "'server_name':{'display':true, 'name':'Server Name', 'width':'200px', 'numeric':true, 'display_type':'text'},";
							echo "'item_id_list':{'display':true, 'name':'Item ID(s)', 'width':'100px', 'numeric':true, 'display_type':'text'},";
							echo "'order_id':{'display':true, 'name':'Order ID', 'width':'50px', 'numeric':true, 'display_type':'text'},\n";
							echo "'chainid':{'display':false, 'name':'foo', 'width':'100px', 'numeric':true, 'display_type':'text'},";
							echo "'dataname':{'display':false, 'name':'foo', 'width':'100px', 'numeric':true, 'display_type':'text'},";
							echo "'name':{'display':false, 'name':'Card #', 'width':'250px', 'numeric':true, 'display_type':'text'},";
							echo "'card_id':{'display':false, 'name':'foo', 'width':'100px', 'numeric':true, 'display_type':'text'},";
							echo "'id':{'display':false, 'name':'foo', 'width':'100px', 'numeric':true, 'display_type':'text'}";
							echo "};\n";

						}
					?>

					var sortCol = 'created_datetime'; //the default sort order;
					var sortDir = false;

					var cols = [];
					var fullwidth = 0;
					for(var d in col_data) {
						cols.push(d);
						if(col_data[d].display){
							fullwidth += parseInt(col_data[d].width.split("px")[0]);
							
						}
					}

					function setCardNum(num) {
						var elem = document.getElementById('card_id');
						var form = document.getElementById('tabform');
						elem.value = num;
						form.submit();
					}
					
					function sortData(col) {
						console.log("Sorting by " + col);
						if(col == sortCol) sortDir = !sortDir; //reverse direction of sort
						sortCol = col;
						if(col_data[col]['numeric']){
							console.log('numeric sort');
							data = data.sort(sortNumeric);
						}
						else {
							console.log('alphabetical sort');
							data = data.sort(sortAlphabetical);
						}
						if(sortDir)
							data = data.reverse();
						drawData();
					}

					function sortAlphabetical(a, b) {
						var aa = a[sortCol];
						var bb = b[sortCol];
						if(aa < bb) return -1;
						if(aa > bb) return 1;
						return 0;
					}

					function sortNumeric(a, b) {
						var aa = a[sortCol];
						var bb = b[sortCol];
						if(isNaN(aa)) aa = 0;
						if(isNaN(bb)) bb = 0;
						return aa - bb;
					}

					function drawData() {
						var table = document.getElementById('table');
						table.style.width = fullwidth + "px";
						table.innerHTML = "";
						//Draw header
						var headerRow = document.createElement('tr');
						for(var c in cols) {
							if(col_data[cols[c]]['display']) {
								var headerCol = document.createElement('td');
								headerCol.className = 'header_td';
								var col = cols[c];
								headerCol.onclick = function(c) { return function(){
										sortData(c);		
									}
								}(col);
								headerCol.innerHTML = col_data[cols[c]]['name'];
								headerCol.setAttribute('width', col_data[cols[c]]['width']);
								headerRow.appendChild(headerCol);
							}
						}
						table.appendChild(headerRow);

						//Draw content
						for(var d in data) {
							var contentRow = document.createElement('tr');
							contentRow.className = 'data_tr';
							for(var c in col_data) {
								if(col_data[c]['display']) {
									var contentCol = document.createElement('td');
									contentCol.className = 'data_td';
									if(col_data[c]['display_type'] == 'money') {
										contentCol.innerHTML = parseFloat(data[d][c]).toFixed(2);
										contentCol.style['text-align']='right';
									}
									else if (col_data[c]['display_type'] == 'text') {
										contentCol.innerHTML = data[d][c];
									}
									contentRow.appendChild(contentCol);
								}
							}
							if(data_mode == 'CardList') {
								contentRow.className += " active_data";
								contentRow.setAttribute('id', data[d]['id']);
								contentRow.onclick = function(n) { return function(){
									setCardNum(n);
								}}(data[d]['id']);
							}
							table.appendChild(contentRow);					
						}
					}
					window.onload = function(){
						drawData();
					}
				</script>
				<body>
					<form method='post' id='form' action=''>
						<div id='background_div'>
							
							<div style='text-align:center'>
								<h2><a onclick='testPrint()'>Adjust Gift/Loyalty Card</a></h2>
							</div>
							<div id='data_div'>
								<table id='table' cellpadding='8' style='text-align:center; border-collapse:collapse;'>
									
								</table>
								<div id="ajax_load" style="display:none">
									<p><img src="../../loader.gif"></p>
								</div>
							</div>
						</div>
					</form>
				</body>
			</html>

			<?php
		return ob_get_clean();
	}
?>
