<?php	
	function output_payments()
	{
		$query = "select IF(`voided`='1',0,IF(`action`='Refund', format(0 - sum(`cc_transactions`.`amount`),2), concat('$',format(sum(`cc_transactions`.`total_collected`),2)))) AS `field0`,`cc_transactions`.`server_name` AS `field1`,`cc_transactions`.`datetime` AS `field2`,`cc_transactions`.`loc_id` AS `field3`,`cc_transactions`.`pay_type` AS `field4`,`cc_transactions`.`card_type` AS `field5`,`cc_transactions`.`action` AS `field6`,`orders`.`void` AS `field7`,`cc_transactions`.`register` AS `field8`,`cc_transactions`.`amount` AS `field9`,IF(`voided`!='0',concat('$',format(sum(`total_collected`),2)),0) AS `field10` from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where `cc_transactions`.`datetime` >= '2011-06-24 00:01:00' and `cc_transactions`.`datetime` <= '2011-06-30 00:01:00' and `cc_transactions`.`loc_id` = '8' group by concat(`cc_transactions`.`pay_type`,' ',`card_type`,`action`,`voided`) order by `field4` asc";	
		return $query;
	}
	$eod_ts = '';
	$eod_filter = "opened";

	function get_eod_total($locationid, $field, $def="&nbsp;",$filter="")
	{
		global $eod_ts;
		global $eod_filter;
		if($filter=="") $filter = $eod_filter;
		
		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));

		$total_query = lavu_query("SELECT SUM(`$field`) AS `sum_total` FROM `orders` WHERE `order_id` NOT LIKE 'Paid%' AND `$filter` >= '$startd' AND `$filter` <= '$endd' AND `void` = '0' AND `location_id` = '$locationid'");
		if(! $total_query ){
			error_log("Unable to Pull EOD Total in " . __FILE__ . ": " . lavu_dberror());
			return $def;
		}
		if(mysqli_num_rows($total_query))
		{
			$total_read = mysqli_fetch_assoc($total_query);
			return $total_read['sum_total'];
		}
		else return $def;
	}

	function get_alt_eod_total($locationid, $type_id, $def="&nbsp;",$filter="")
	{
		global $eod_ts;
		global $eod_filter;
		if($filter=="") $filter = $eod_filter;
		
		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$total_query = lavu_query("SELECT SUM(`alternate_payment_totals`.`amount`) AS `sum_total` FROM `alternate_payment_totals` LEFT JOIN `orders` ON `orders`.`location_id` = `alternate_payment_totals`.`loc_id` AND `orders`.`order_id` = `alternate_payment_totals`.`order_id` WHERE `orders`.`$filter` >= '$startd' AND `orders`.`$filter` <= '$endd' AND `orders`.`void` = '0' AND `alternate_payment_totals`.`loc_id` = '$locationid' AND `alternate_payment_totals`.`pay_type_id` = '$type_id'");
		if(! $total_query ){
			error_log("Unable to pull alt EOD total in " . __FILE__ . ": " . lavu_dberror());
			return $def;
		}
		if(mysqli_num_rows($total_query))
		{
			$total_read = mysqli_fetch_assoc($total_query);
			return $total_read['sum_total'];
		}
		else return $def;
	}

	function get_order_count($locationid, $def="&nbsp;", $filter="")
	{
		global $eod_ts;
		global $eod_filter;
		if($filter=="") $filter = $eod_filter;
		
		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$thingy = "SELECT COUNT(`id`) FROM `orders` WHERE `order_id` NOT LIKE 'Paid%' AND `$filter` >= '$startd' AND `$filter` < '$endd' AND `void` = '0' AND `location_id` = '$locationid'";
		//echo "thingy: $thingy<br>";
		$get_count = lavu_query($thingy);
		$count = 0;
		if($get_count){
			$count = (int)mysqli_result($get_count, 0);
		}
		return $count;
	}

	function get_paidin_paidout($locationid)
	{
		global $eod_ts;
		global $eod_filter;
		if($filter=="") $filter = $eod_filter;
		
		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$paidin = 0;
		$paidout = 0;
		
		$trans_query = lavu_query("select sum(`amount`) as `total` from cc_transactions where loc_id='[1]' and `datetime`>='[2]' and `datetime`<='[3]' and `order_id` LIKE 'Paid In'",$locationid,$startd,$endd);
		if($trans_query && mysqli_num_rows($trans_query))
		{
			$trans_read = mysqli_fetch_assoc($trans_query);
			$paidin = $trans_read['total'];
		}
		$trans_query = lavu_query("select sum(`amount`) as `total` from cc_transactions where loc_id='[1]' and `datetime`>='[2]' and `datetime`<='[3]' and `order_id` LIKE 'Paid Out'",$locationid,$startd,$endd);
		if($trans_query && mysqli_num_rows($trans_query))
		{
			$trans_read = mysqli_fetch_assoc($trans_query);
			$paidout = $trans_read['total'];
		}
		
		return ($paidin - $paidout);
	}

	function get_daily_cash_tip_total($loc_id, $server_id, $use_date) {
		$cash_tips = 0;
		$check_server_id = "";
		if (is_numeric($server_id)) $check_server_id = " AND `server_id` = '$server_id'";
		$check_todays_cash_tips = lavu_query("SELECT SUM(`value`) AS `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]'$check_server_id AND `date` = '[2]'", $loc_id, $use_date);
		if ($check_todays_cash_tips && mysqli_num_rows($check_todays_cash_tips)) {
			$info = mysqli_fetch_assoc($check_todays_cash_tips);
			$cash_tips = $info['value'];
		}
		
		return $cash_tips;
	}

	function get_ag_totals($locationid, $filter="")
	{
		global $eod_ts;
		global $eod_filter;
		if($filter=="") $filter = $eod_filter;
		
		$t_details = array();
		$t_details['cash'] = 0;
		$t_details['card'] = 0;
		
		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		$order_query = lavu_query("SELECT * FROM `orders` WHERE `order_id` NOT LIKE 'Paid%' AND `$filter` >= '$startd' AND `$filter` < '$endd' AND `void` = '0' AND `location_id` = '$locationid' AND `gratuity`!='' AND `gratuity`>0");
		if(! $order_query ){
			return $t_details;
		}

		while($order_read = mysqli_fetch_assoc($order_query))
		{
			$use_ag = max(0,$order_read['gratuity']*1);
			$use_cash_paid = max(0,$order_read['cash_applied']*1);
			$use_card_paid = max(0,$order_read['card_paid']*1);
			$ag_cash = 0;
			$ag_card = 0;
			if($use_cash_paid >= $use_ag)
			{
				$ag_cash = $use_ag;
			}
			else
			{
				$ag_cash = $use_cash_paid;
				$use_ag -= $ag_cash * 1;
				if($use_card_paid >= $use_ag)
				{
					$ag_card = $use_ag;
				}
				else
				{
					$ag_card = $use_card_paid;
					$use_ag -= $ag_card * 1;
				}
			}
			
			$t_details['cash'] += ($ag_cash * 1);
			$t_details['card'] += ($ag_card * 1);
		}
		return $t_details;
	}


	function output_sales_totals($location,$filter)
	{		
		global $location_info;
		global $eod_ts;
		global $eod_filter;
		$smallest_money = (1 / pow(10, $decimal_places));
		$eod_filter = $filter;
		$mid = "";
		$locationid = $location_info['id'];
		$decimal_places = $location_info['disable_decimal'];

		$title = "";			
		
		$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
		$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);
		
		
		$use_date = (isset($_REQUEST['device_time']))?$_REQUEST['device_time']:$tz_date;

		$tz_date = $use_date;/*localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);*/
		$tz_hrmin = substr($tz_date,11,2).substr($tz_date,14,2);

		$udate = explode("-", substr($use_date,0,10));
		$uyear = $udate[0];
		$umonth = $udate[1];
		$uday = $udate[2];
		$use_ts = mktime($se_hour, $se_min, 0, $udate[1], $udate[2], $udate[0]);

		if ($location_info['day_start_end_time'] > $tz_hrmin){
			$use_ts -= 86400;
		}
		
		$start_datetime = date("Y-m-d H:i:s", $use_ts);
		$udate_display = date("n/j/Y", $use_ts);

		$use_date2 = /*(isset($_REQUEST['date2']))?$_REQUEST['date2']:$use_date*/ $start_datetime;
		$udate2 = explode("-", $use_date2);
		$uyear2 = $udate2[0];
		$umonth2 = $udate2[1];
		$uday2 = $udate2[2];
		$use_ts2 = mktime($se_hour, $se_min, 0, $udate2[1], $udate2[2], $udate2[0]);
		$end_datetime = date("Y-m-d H:i:s", ($use_ts2 + 86399));
		$udate_display = date("n/j/Y", $use_ts2);
	
		//$title .= "<br>";
		$title .= "<br><span style='font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px; color:#333333;'>($start_datetime to $end_datetime)</span><b>";
		
		$eod_ts = $use_ts;
		
		$mid .= "<table>";
		$alt_payment_types = array();
		$alt_paid_grand_totals = array();
		$alt_column = speak("Gift");
		$alt_m_title = speak("Gift Total");
		$get_alt_payment_types = lavu_query("SELECT * FROM `payment_types` WHERE `loc_id` = '[1]' AND `id` > '3' ORDER BY `_order` ASC, `type` ASC", $location_info['id']);
		if (@mysqli_num_rows($get_alt_payment_types) > 0) {
			$alt_payment_types[] = array(3,speak("Gift Certificate"));
			$alt_paid_grand_totals[] = 0;
			while ($alt_payment_info = mysqli_fetch_assoc($get_alt_payment_types)) {
				$alt_payment_types[] = array($alt_payment_info['id'], $alt_payment_info['type'], $alt_payment_info['special']);
				$alt_paid_grand_totals[] = 0;
			}
			$alt_column = speak("Other");
			$alt_m_title = speak("Other Total");
		}

		$ecount++;
		$t_total = get_eod_total($locationid,'total');
		$t_auto_gratuity = get_eod_total($locationid,'gratuity');
		
		$t_order_count = get_order_count($locationid);
		$t_guest_count = get_eod_total($locationid,'guests');
		
		$t_disc = get_eod_total($locationid,'discount');
		$t_itax = get_eod_total($locationid,'itax');
		$t_subt = (get_eod_total($locationid,'subtotal') - $t_itax);
		$show_subtotal = display_price($t_subt, $decimal_places);
		$show_discount = display_price($t_disc, $decimal_places);
		$show_subtotal_after_discount = display_price((($t_subt * 1) - ($t_disc * 1)), $decimal_places);
		
		$show_tax = display_price((get_eod_total($locationid,'tax') + $t_itax), $decimal_places);
		$show_auto_gratuity = display_price($t_auto_gratuity, $decimal_places);
		$show_total = display_price(($t_total - $t_auto_gratuity), $decimal_places);
		$cash_paid = get_eod_total($locationid,'cash_paid');
		$show_cash = display_price($cash_paid, $decimal_places);

		$paidin_paidout = get_paidin_paidout($locationid);
		$show_paidin_paidout = display_price($paidin_paidout,$decimal_places);

		$t_card_subtotal = get_eod_total($locationid,'card_paid');
		$t_card_tips = get_eod_total($locationid,'card_gratuity');
		$t_card_total = $t_card_subtotal + $t_card_tips;
		
		$cash_gratuity = get_eod_total($locationid,'cash_tip') + get_daily_cash_tip_total($locationid, "all", $use_date);
		$show_cash_gratuity = display_price($cash_gratuity, $decimal_places);
		
		//----Inserted by Brian D. 
		$numerical_cash_total = ($cash_paid + $paidin_paidout) * 1;
		$show_cash_total = display_price($numerical_cash_total, $decimal_places);			
		//
		$show_cash_total_w_tips = display_price(($numerical_cash_total + $cash_gratuity), $decimal_places);
		
		$t_deposits_used = get_eod_total($locationid,'deposit_amount');
		$t_deposits_collected = get_eod_total($locationid, 'deposit_amount', "&nbsp;","opened");
		
		$show_card_subtotal = display_price($t_card_subtotal, $decimal_places);
		$show_card_gratuity = display_price($t_card_tips, $decimal_places);
		$show_card_total = display_price($t_card_total, $decimal_places);
		
		$gc_total = get_eod_total($locationid,'gift_certificate');
		$alt_total = 0;
		$alt_rev_total = 0;
		$show_alt_totals = array();
		$alt_paid_grand_totals[0] += $gc_total;
		
		//Show alt totals is built here alternate_payment_types
		//Alternate 
								
		for ($ap = 1; $ap < count($alt_payment_types); $ap++) {
		if(isset($_GET['dbg'])) echo "alt payment: " . $ptype[0] . " - " . $ptype[1] . " - " . $ptype[2] . " - " . $this_total . "<br>";
			$ptype = $alt_payment_types[$ap];
			$this_total = get_alt_eod_total($locationid,$ptype[0]);
			if ((float)$this_total != 0) {
			
				$alt_total += $this_total;
				$alt_rev_total += ($ptype[2] == 'gift_card' || $ptype[2] == 'loyalty_card') ? $this_total :0.0;//Inserted by Brian D.
				$show_alt_totals[] = array($ptype[1], display_price($this_total, $decimal_places));
				$alt_paid_grand_totals[$ap] += $this_total;
			}
		}
		
		$show_gc_total = display_price($gc_total, $decimal_places);
		$show_alt_total = display_price(($gc_total + $alt_total), $decimal_places);
						
		$amount_due = $t_total - $t_card_subtotal - $cash_paid - $gc_total - $alt_total;
		if(($amount_due > (0 - ($smallest_money / 2))) && ($amount_due < ($smallest_money / 2))) $amount_due = 0;
		$show_amount_due = display_price($amount_due, $decimal_places);
		
		$show_deposits_used = display_price($t_deposits_used, $decimal_places);
		$show_deposits_collected = display_price($t_deposits_collected, $decimal_places);
		
		$show_amount_in = display_price($t_total - $t_deposits_used, $decimal_places);
		$show_actual_sales = display_price($t_total - $t_deposits_collected + $t_deposits_used, $decimal_places);

		$ag_totals = get_ag_totals($locationid);
		$show_ag_card = display_price($ag_totals['card'], $decimal_places);
		$show_ag_cash = display_price($ag_totals['cash'], $decimal_places);
		
		$total_plus_tips = $t_total + $t_card_tips;			
		
		// gift/loyalty card sales totals
		
		$startd = date("Y-m-d H:i:s", $eod_ts);
		$endd = date("Y-m-d H:i:s", ($eod_ts + 86399));
		
		$gc_orderred_total = 0;
		$lc_orderred_total = 0;
		$gce_orderred_total = 0;
		$alternate_orderred_total = 0;
		{
			$gc_orderred_query = lavu_query("SELECT `id` FROM `forced_modifier_lists` WHERE `title`='*Gift Card'"); // consider basing search on hidden1 field (i.e., gift_card_keyed_in)
			if(mysqli_num_rows($gc_orderred_query))
			{
				$gc_orderred_query = lavu_query("SELECT SUM(`order_contents`.`subtotal`) as `subtotal` FROM `order_contents` LEFT JOIN `orders` ON `order_contents`.`loc_id` = `orders`.`location_id` AND `order_contents`.`order_id` = `orders`.`order_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` WHERE `orders`.`$eod_filter` >= '$startd' AND `orders`.`$eod_filter` < '$endd' AND `orders`.`void` = '0' AND `order_contents`.`quantity` > '0' AND (`menu_items`.`forced_modifier_group_id` IN (SELECT CONCAT('f',`id`) as `id` FROM `forced_modifier_lists` WHERE `title`='*Gift Card') OR  (`menu_categories`.`forced_modifier_group_id` IN (SELECT CONCAT('f',`id`) as `id` FROM `forced_modifier_lists` WHERE `title`='*Gift Card') AND 	`menu_items`.`forced_modifier_group_id` = 'C'))");  //Gift Cards
				
				
				if(mysqli_num_rows($gc_orderred_query))
				{
					$temp = mysqli_fetch_assoc($gc_orderred_query);
					//error_log("temp: ".print_r($temp, true));
					$gc_orderred_total = $temp['subtotal'];
				}
			}
			
			$lc_orderred_query = lavu_query("SELECT `id` FROM `forced_modifier_lists` WHERE `title`='*Loyalty Card'");
			if(mysqli_num_rows($lc_orderred_query))
			{	
				$lc_orderred_query = lavu_query("SELECT SUM(`order_contents`.`subtotal`) as `subtotal` FROM `order_contents` LEFT JOIN `orders` ON `order_contents`.`loc_id` = `orders`.`location_id` AND `order_contents`.`order_id` = `orders`.`order_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` WHERE `orders`.`$eod_filter` >= '$startd' AND `orders`.`$eod_filter` < '$endd' AND `orders`.`void` = '0' AND `order_contents`.`quantity` > '0' AND (`menu_items`.`forced_modifier_group_id` IN (SELECT CONCAT('f',`id`) as `id` FROM `forced_modifier_lists` WHERE `title`='*Loyalty Card') OR (`menu_categories`.`forced_modifier_group_id` IN (SELECT CONCAT('f',`id`) as `id` FROM `forced_modifier_lists` WHERE `title`='*Loyalty Card') AND `menu_items`.`forced_modifier_group_id` = 'C'))");  //Loyalty Cards

				if(mysqli_num_rows($lc_orderred_query))
				{
					$temp = mysqli_fetch_assoc($lc_orderred_query);
					//error_log("temp: ".print_r($temp, true));
					$lc_orderred_total = $temp['subtotal'];
				}
			}
			
			$gce_orderred_query = lavu_query("SELECT SUM(`order_contents`.`subtotal`) as `subtotal` FROM `order_contents` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `orders` ON `order_contents`.`loc_id` = `orders`.`location_id` AND `order_contents`.`order_id` = `orders`.`order_id` WHERE `menu_items`.`hidden_value2`='Gift Certificate' AND `orders`.`$eod_filter` >= '$startd' AND `orders`.`$eod_filter` < '$endd' AND `orders`.`void` = '0' AND `order_contents`.`quantity` > '0'");  // Gift Certificate

			$temp = mysqli_fetch_assoc($gce_orderred_query);
			//error_log("temp: ".print_r($temp, true));
			$gce_orderred_total = $temp['subtotal'];			
		}
		
		$not_zero = 0;
		if ($gc_orderred_total != 0) $not_zero++;
		if ($gce_orderred_total != 0) $not_zero++;
		if ($lc_orderred_total != 0) $not_zero++;
		
		$alternate_orderred_total = ($gc_orderred_total + $gce_orderred_total + $lc_orderred_total);
		
		$show_gc_orderred_total = display_price($gc_orderred_total, $decimal_places);
		$show_gce_orderred_total = display_price($gce_orderred_total, $decimal_places);
		$show_lc_orderred_total = display_price($lc_orderred_total, $decimal_places);
		$show_alternate_orderred_total = display_price($alternate_orderred_total, $decimal_places);

		//Inserted by Brian D. ---- New field to go into index.php?mode=reports_end_of_day
		//Total paid = cashTotal + card total + foreach[1] <- these are alternate payment types. + gift certificates
		$show_total_paid = display_price($numerical_cash_total + $t_card_total + $alt_total + $gc_total, $decimal_places);
		$show_total_plus_tips = display_price($total_plus_tips, $decimal_places);
		$show_alt_rev_total = display_price($alt_total + $gc_total, $decimal_places);
		$show_total_revenue = "";
		if($location_info['gift_revenue_tracked'] == 'when_redeemed')
			$show_total_revenue = display_price($numerical_cash_total + $t_card_total + $alt_total + $gc_total - $alternate_orderred_total, $decimal_places);
		else
			$show_total_revenue = display_price($numerical_cash_total + $t_card_total + $alt_total + $gc_total - ($alt_rev_total + $gc_total), $decimal_places);
		//--------------------------
										
		$eod_parts = explode("-", date("Y-n-j", $eod_ts));

		$mid .= "<tr><td align='right'>".speak("Subtotal").":</td><td>" . $show_subtotal . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Discount").":</td><td>" . $show_discount . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Subtotal after Discount").":</td><td>" . $show_subtotal_after_discount . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Tax").":</td><td>" . $show_tax . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Auto Gratuity").":</td><td>" . $show_auto_gratuity . "</td></tr>";
		$mid .= "<tr><td align='right' style='border-top:solid 1px black'>".speak("Total").":</td><td style='border-top:solid 1px black'>" . $show_total . "</td></tr>";
		$mid .= "<tr><td>&nbsp;</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Cash").":</td><td>" . $show_cash . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Cash Tips").":</td><td>" . $show_cash_gratuity . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Cash Total").":</td><td>" . $show_cash_total . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("AG Cash").":</td><td>" . $show_ag_cash . "</td></tr>";
		$mid .= "<tr><td>&nbsp;</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Card").":</td><td>" . $show_card_subtotal . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Card Tips").":</td><td>" . $show_card_gratuity . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Card Total").":</td><td>" . $show_card_total . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("AG Card").":</td><td>" . $show_ag_card . "</td></tr>";
		$mid .= "<tr><td>&nbsp;</td></tr>";
		
		if ($gce_orderred_total>0 || $gc_orderred_total>0 || $lc_orderred_total>0) $mid .= "<tr><td>&nbsp;</td></tr>";
		if ($gce_orderred_total > 0)
			$mid .= "<tr><tr><td align='right'>" . speak("Gift Certificates Ordered") . ":</td><td align='left'>" . $show_gce_orderred_total . "</td></tr>";
		if ($gc_orderred_total > 0)
			$mid .= "<tr><td align='right'>" . speak("Gift Cards Ordered") . ":</td><td align='left'>" . $show_gc_orderred_total . "</td></tr>";
		if ($lc_orderred_total > 0)
			$mid .= "<tr><td align='right'>" . speak("Loyalty Cards Ordered") . ":</td><td align='left'>" . $show_lc_orderred_total . "</td></tr>";
		if ($alternate_orderred_total > 0 && $not_zero > 0)
			$mid .= "<tr><td align='right' style='border-top: 1px solid black;' >" . speak("Alternate Revenue Ordered") . ":</td><td align='left' style='border-top: 1px solid black;' >" . $show_alternate_orderred_total . "</td></tr>";
		$mid .= "<tr><td>&nbsp;</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Gift Certificates Redeemed").":</td><td>" . $show_gc_total . "</td></tr>";
		foreach ($show_alt_totals as $this_alt_total) {
			$mid .= "<tr><td align='right'>".$this_alt_total[0]." ".speak("Redeemed").":</td><td>".$this_alt_total[1]."</td></tr>";
		}
		$mid .= "<tr><td align='right' style='border-top: 1px solid black;'>".speak('Alternate Revenue Redeemed').":</td><td style='border-top: 1px solid black;' >" . $show_alt_rev_total . "</td></tr>";
		$mid .= "<tr><td>&nbsp;</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Unpaid").":</td><td>" . $show_amount_due . "</td></tr>";
		$mid .= "<tr><td>&nbsp;</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Deposits Collected").":</td><td>" . $show_deposits_collected . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Deposits Used").":</td><td>" . $show_deposits_used . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Actual Sales")." <span style='font-size:10; color:#444444'>(".speak("total")." - ".speak("dep collected")." + ".speak("dep used").")</span>:</td><td>" . $show_actual_sales . "</td></tr>";
		$mid .= "<tr><td align='right'>".speak("Amount To Take In")." <span style='font-size:10; color:#444444'>(".speak("total")." - ".speak("dep used").")</span>:</td><td>" . $show_amount_in . "</td></tr>";
		//$mid .= "</table></td></tr>";
		// Added by Brian D.------ step three $alt_rev_total
		//--------------------------					
		// Added by Brian D.------
		//total paid minus gift certificates. Total paid =  cashTotal + card total + foreach[1] <- these are alternate payment types. + gift certificates
		//Total paid goes here step 1.
		//$mid .= "<tr><td align='right'>".speak("Total Paid").":</td><td>" . $show_total_paid . "</td></tr>";
		//step 2 is total revenue
		//$mid .= "<tr><td align='right'>".speak("Total Revenue").":</td><td>" . $show_total_revenue . "</td></tr>";
		//--------------------------
		
		//$mid .= "<tr><td align='right'>".speak("Total + Tips (Manual)").":</td><td>" . $show_total_plus_tips . "</td></tr>";
		//$mid .= "<tr><td>&nbsp;</td></tr>";
		
		$mid .= "<tr><td>&nbsp;</td></tr>";
		$mid .= "</table>";
						
		return $title . "<br><br><table width='100%'><tr><td width='100%' align='center'>" . $mid . "</td></tr></table>";
	}
?>
