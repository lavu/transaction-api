<?php
/*****************************************************************************
 * The user can till in when the register opens, and till out when the register closes.
 * This store the data in the cash_data table, and gives a report upon tilling out.
 * The format in the cash_data table is:
 *     setting = "till_in_out_" . "DateOfTillIn_" . "register:register_name"
 * OR  setting = "till_in_out_" . "DateOfTillIn_" . "server:server_name"
 *     value = "totalin:totalout|NULL
 *     value2 = "TimeOfTillIn" as hh:mm:ss
 *     value3 = "TimeOfTillOut" as hh:mm:ss
 *     value4 = largestbillin/secondlargestbillin/...:largestbillout/secondlargestbillout/...|NULL
 *     value5 = largestcoinin/secondlargestcoinin/...:largestcoinout/secondlargestcoinout/...|NULL
 *     value6 = largestbilltype/secondlargestbilltype/...:largestcointype/secondlargestcointype/...
 *     type = "location_config_setting"
 *****************************************************************************
 * Notes to push live:
 *     add the line $extra_reports[] = array("Cash Reconciliation","reports_cash_reconciliation"); to index.php
 *     copy this file and "transaction_summary.php" from /dev/lib to /lib
 *     add the following lines to "advanced_location_settings.php":
 *           $fields[] = array(speak("Monetary denominations").":", "", "redirect_button", array("prefix"=>"", "value"=>speak("View/Edit"), "link"=>"index.php?mode=edit_monetary_denominations&edit=1"));
 *           $fields[] = array(speak("Cash reconciliation options").":", "", "redirect_button", array("prefix"=>"", "value"=>speak("View/Edit"), "link"=>"index.php?mode=edit_transaction_summary_items&edit=1"));
 *     copy the file "cash_reconciliation.php" from v2/areas/reports to cp/areas/reports
 *     copy the files "edit_transaction_summaries" and "edit_monetary_denominations.php" from v2/areas to cp/areas/
 *****************************************************************************/
global $dev;
if ($dev) {
	ini_set("display_errors", "1");
}
$extra_display_string = "";
$dontprint = "DONTPRINTME";
//error_log(__FILE__ . " " . __LINE__ .  ": Globals: " . print_r($GLOBALS,1));
require_once(__DIR__."/../jcvs/ServerCashDrawerApi.php");
require_once(dirname(__FILE__)."/../transaction_summary.php");
require_once(dirname(__FILE__)."/till_in_out.php");
require_once(dirname(__FILE__)."/../info_inc.php");
require_once(dirname(__FILE__) . "/../../cp/areas/edit_monetary_denominations.php");

adjust_day();
copy_from_old_tables_to_new_tables();

global $report;
global $savein;
global $saveout;
global $tillinout;
global $mode;
global $fwd_vars;
global $real_name;

$isRegisterTill = FALSE;
$report = FALSE; //Used to display a till summary
$savein = FALSE; //Used to save the till
$saveout = FALSE; //Used to save the till
$tillinout = FALSE;
$isPreviousDay = '0'; //0 = false, 1 = true;
$big_title = "";
$db_denominations = NULL;
$left_or_right = NULL;
$decimal_char = NULL;
$monetary_symbol = NULL;
$location_timezone = NULL;
$day_start_end_time = NULL;
$today = NULL;
$today_datetime = NULL;
$i_adjust_day = NULL;
$is_tilled_in = NULL;
$till_in = NULL;
$shift_time = NULL;
$cashDrawerId = '';
$cashDrawerAssignedServerId = '';
$errorMsg = '';

setInitializeTillInOutButtonClasses($tillInButtonClass, $tillOutButtonClass, $_POST['tillInButtonClass'],$_POST['tillOutButtonClass']);
setTitleAndIsRegisterTill($big_title, $isRegisterTill, $mode, $real_name);

/*Set state according to the posted values, as well as time in and time out.*/
if (isset($_POST['report'])){            $report         = TRUE;                        }
if (isset($_POST['savein'])){            $savein         = $_POST['savein'];            }
if (isset($_POST['saveout'])){           $saveout        = $_POST['saveout'];           }
if (isset($_POST['tillinout'])){         $tillinout      = TRUE;                        }
if (isset($_POST['timein'])){            $post_timein    = $_POST['timein'];            }
if (isset($_POST['timeout'])){           $post_timeout   = $_POST['timeout'];           }
if (isset($_POST['isPreviousDay'])){     $isPreviousDay  = $_POST['isPreviousDay'];     }
if (isset($_REQUEST['register_name'])){  $register_name  = $_REQUEST['register_name'];  }
if (isset($_POST['cashDrawerId'])){     $cashDrawerId  = $_POST['cashDrawerId'];      }
if (isset($_POST['cashDrawerAssignedServerId'])){     $cashDrawerAssignedServerId  = $_POST['cashDrawerAssignedServerId'];      }

setUpLocationVarsByReference($decimal_char,$monetary_symbol,$location_timezone,$day_start_end_time,$today_datetime, $today, $db_denominations, $left_or_right, $decimal_char, $loc_id);

$dualDrawerEnabled = sessvar('location_info')['enable_dual_cash_register'];

if ($dualDrawerEnabled) {
	$active_register = $_GET['register'];
	$register = $active_register;
	$drawerQuery = lavu_query("SELECT `value17` as drawer FROM `config` WHERE `setting` = '".$active_register."' AND `type` = 'printer' AND _deleted='0'");
	$drawerResult = mysqli_fetch_assoc($drawerQuery);
	$dualDrawerPrinterEnabled = $drawerResult['drawer'];
	$dualDrawerEnabled = ($dualDrawerPrinterEnabled && $dualDrawerEnabled) ? 1 : 0;
	if ($dualDrawerEnabled) {
		$isRegisterTill = false;
		$users = getAllActiveUser();
		foreach ($users as $user) {
			$usersInfo[$user['id']]['full_name'] = $user['fullname'];
			$usersInfo[$user['id']]['access_level'] = $user['access_level'];
		}
		$drawer = (isset($_GET['drawer']) && $_GET['drawer'] != '') ? $_GET['drawer'] : 1;
		$active_server_id = (isset($_GET['server_id'])) ? (int)$_GET['server_id'] : 0;
		$active_server_name = $usersInfo[$active_server_id]['full_name'];
		$active_access_level = $usersInfo[$active_server_id]['access_level'];
		if ($errorMsg == '' && !$report) {
			if ($saveout == 'true') {
				$unAssign = true;
				$data['printer'] = $active_register;
				$data['drawer'] = $cashDrawerId;
				$data['serverId'] = $cashDrawerAssignedServerId;
				$data['action'] = 'getcashdrawer';
				$apiObj = new ServerCashDrawerApi($data_name, $data);
				$drawerResponse = json_decode($apiObj->doProcess(), true);
				if ($drawerResponse['status']) {
					if ($active_access_level < 3) {
						if ($drawerResponse['data'][0]['server_id'] == 0) {
							$errorMsg = translator('ERR_DCD_1') . ' ' . $cashDrawerId;
							$unAssign = false;
						} else if ($active_server_id != $drawerResponse['data'][0]['server_id']) {
							$errorMsg = translator('ERR_DCD_2', array('{1}'), array('[--CR--]'));
							$unAssign = false;
						}
					}
				}

				if ($unAssign) {
					$data['serverId'] = $drawerResponse['data'][0]['server_id'];
					$data['updatedBy'] = (int)$_GET['server_id'];
					$data['action'] = 'unassignserver';
					$_REQUEST['server_id'] = $drawerResponse['data'][0]['server_id'];
					$apiObj = new ServerCashDrawerApi($data_name, $data);
					$drawerResponse = json_decode($apiObj->doProcess(), true);
					$real_name = str_replace('<', '&lt;', $usersInfo[$cashDrawerAssignedServerId]['full_name']);
					$isRegisterTill = false;
				}
			} else if ($savein) {
				if ($cashDrawerAssignedServerId == 0 || $cashDrawerAssignedServerId == '') {
					$errorMsg = translator('ERR_DCD_3');
				}
				if ($cashDrawerId == 0 || $cashDrawerId == '') {
					$errorMsg = translator('ERR_DCD_4');
				}
				if ($cashDrawerAssignedServerId > 0) {
					$data['printer'] = $active_register;
					$data['updatedDate'] = $today;
					$data['action'] = 'getcashdrawer';
					$apiObj = new ServerCashDrawerApi($data_name, $data);
					$drawerResponse = json_decode($apiObj->doProcess(), true);
					if ($drawerResponse['status']) {
						foreach ($drawerResponse['data'] as $details) {
							$serverAssignedPrinter[] = $details['server_id'];
							$serverDrawer[$details['server_id']] = $details['drawer'];
							$assignedDrawerToServer[$details['drawer']] = $details['server_id'];
						}

						if ( ($active_access_level < 3) && in_array($cashDrawerAssignedServerId, $serverAssignedPrinter) ) {
							//Assign a different user <br> Hayat Shahi is assigned to cash drawer 2
							$errorMsg = translator('ERR_DCD_5', array('{1}', '{2}', '{3}'), array('[--CR--]', $usersInfo[$cashDrawerAssignedServerId]['full_name'], $serverDrawer[$cashDrawerAssignedServerId]));
						}
/*
						if ( ($active_access_level < 3) && isset($assignedDrawerToServer[$cashDrawerId]) && $assignedDrawerToServer[$cashDrawerId] > 0) {
							//Assign a different drawer <br> drawer 1 already assigned to Hayat Shahi
							$errorMsg = translator('ERR_DCD_6', array('{1}', '{2}', '{3}'), array('[--CR--]', $cashDrawerId, $usersInfo[$assignedDrawerToServer[$cashDrawerId]]['full_name']));
						}
*/
					}
				}
				if ($errorMsg == '') {
					$data['printer'] = $active_register;
					$data['serverId'] = $cashDrawerAssignedServerId;
					$_REQUEST['server_id'] = $cashDrawerAssignedServerId;
					$data['assignedBy'] = (int)$_GET['server_id'];
					$data['drawer'] = $cashDrawerId;
					$data['createdBy'] = (int)$_GET['server_id'];
					$data['action'] = 'assignserver';
					$apiObj = new ServerCashDrawerApi($data_name, $data);
					$drawerResponse = json_decode($apiObj->doProcess(), true);
					$real_name = $usersInfo[$cashDrawerAssignedServerId]['full_name'];
					$isRegisterTill = false;
				}
			}
		}
	}
}

setIsPreviousDay_Today_AndIAdjustDay($i_adjust_day, $today, $isPreviousDay, $_POST['adjust_day'], $today_datetime);
$s_location = http_build_query($_REQUEST, '', '&'); //Used for day adjustments.
load_location_info($loc_id);
$shift_time = show_shift_time($today_datetime);
($isRegisterTill ? $useName = $_REQUEST['register_name'] : $useName = $real_name );
$useName = str_replace('<', '&lt;', $useName);
$latest_till = get_last_till_record_today($register, $useName, $today, $monetary_symbol, $loc_id, $isRegisterTill, $drawer); //Literally grabs the latest till-record from the current date.
($latest_till['timein'] !== "NULL" ? $timein = $latest_till['timein'] : $timein = "NULL"); //$timein is set to the latest till's time-in.

/*Write till information to the database, this should be done before checking if tilled in, so that the state of the app is up to date.*/
if (($savein || $saveout) && $errorMsg == '') {
	save_till($savein, $saveout, $loc_id, $decimal_char, $monetary_symbol, $today, $register, $useName, $latest_till, $real_name, $db_denominations, $drawer);
	$latest_till = get_last_till_record_today($register, $useName, $today, $monetary_symbol, $loc_id, $isRegisterTill, $drawer); //Since we just updated the database.
}

setIsTilledIn_TillIn_ShiftTime($is_tilled_in, $till_in, $shift_time, $latest_till, $i_adjust_day, $useName, $today);
/** DISPLAY */
$till_display = "";
$till_display .= generateTillView($register, $useName, $today, $monetary_symbol, $loc_id, $timein, $timeout, $fwd_vars, $shift_time, $isRegisterTill, $big_title, $db_denominations, $monetary_symbol, $left_or_right);
if ($report) { //Report is to be set in the post variables.
	if (count(get_tills_between_times($register, $useName, $today, $monetary_symbol, $loc_id, null, null, $isRegisterTill, '', $drawer)) == 0) {
		error_log("Error: Could not find till for cc $cc, register $register ($register_name)");
	}
	$till_display .= draw_report($register, $useName, $today, $monetary_symbol, $loc_id, $post_timein, $post_timeout, false, $isRegisterTill, false, false, $drawer);
}
setDisplayForLocationIssues($till_display_price_location, $till_display, $extra_display_string);
$display .= $till_display;
/** END DISPLAY */

function setIsTilledIn_TillIn_ShiftTime(&$is_tilled_in,&$till_in,&$shift_time, $latest_till, $i_adjust_day, $useName, $today){
	$is_tilled_in = FALSE;
	$till_in = "true";
	if ($latest_till !== NULL && $latest_till['timeout'] === "NULL" && ($latest_till['register_name'] == $useName)) { //If we have a till-in $is_tilled_in is set to true.
		$is_tilled_in = TRUE;
		$till_in = "false";
	}
	if ($i_adjust_day !== 0 && $i_adjust_day != '*') {
		$till_in = '1';
	}

	// based on date format setting
	$date_setting = "date_format";
	$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
	if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
		$date_format = array("value" => 2);
	}else
		$date_format = mysqli_fetch_assoc( $location_date_format_query );
	$change_format = explode("-", $today);
	switch ($date_format['value']){
		case 1:$today = $change_format[2]."-".$change_format[1]."-".$change_format[0];
		break;
		case 2:$today = $change_format[1]."-".$change_format[2]."-".$change_format[0];
		break;
		case 3:$today = $change_format[0]."-".$change_format[1]."-".$change_format[2];
		break;
	}

	($i_adjust_day != 0 ? $datePiece = 'Date' : $datePiece = 'Today');
	$shift_time = speak($datePiece).": " . $today;
}

function setIsPreviousDay_Today_AndIAdjustDay(&$i_adjust_day, &$today, &$isPreviousDay, $postedAdjustDayValue, $today_datetime){
	$i_adjust_day = '*';
	if (isset($postedAdjustDayValue) && $postedAdjustDayValue != '') {
		$i_adjust_day = intval($postedAdjustDayValue);
		$today = date('Y-m-d', strtotime($today_datetime . " " . $i_adjust_day . " day")); //e.g. date('Y-m-d H:i:s') . ' +1 day'
	} else if (!isset($postedAdjustDayValue)) {
		$i_adjust_day = 0;
	}
	if($i_adjust_day == 0){
		$isPreviousDay = '0';
	}
}

function setTitleAndIsRegisterTill(&$big_title, &$isRegisterTill, $mode, $real_name){
	if($mode == 'cash_reconciliation'){
		$big_title = speak("register till")." - ".$_REQUEST['register_name'];
		$isRegisterTill = true;
	}else if($mode == 'server_reconciliation'){
		$real_name = str_replace('<', '&lt;', $real_name);
		$big_title = speak("server till")." - " . $real_name;
		$isRegisterTill = false;
	}else if(isset($_POST['isRegisterTill'])){
		$isRegisterTill = $_POST['isRegisterTill'];
	}else if(isset($_POST['big_title'])){
		$big_title = $_POST['big_title'];
	}
}

function setUpLocationVarsByReference(&$decimal_char, &$monetary_symbol, &$location_timezone, &$day_start_end_time, &$today_datetime, &$today, &$db_denominations, &$left_or_right, &$decimal_char, $loc_id){
	$decimal_char = get_decimal_char($loc_id);
	$monetary_symbol = get_monetary_symbol($loc_id);
	$location_timezone = get_location_timezone($loc_id);
	$day_start_end_time = get_day_start_end_time($loc_id);
	$today_datetime = date("Y-m-d H:i:s", mystrtotime(localize_datetime(date("Y-m-d H:i:s"), $location_timezone)));
	$today = date('Y-m-d', strtotime($today_datetime));
	$db_denominations = get_db_denominations();
	$left_or_right = get_left_or_right($loc_id);
	$decimal_char = get_decimal_char($loc_id);
}

function setDisplayForLocationIssues($till_display_price_location, &$till_display, $extra_display_string){
	if (strlen($till_display_price_location['timezone']) === 0) {
		$till_display = speak('Unable to load timezone. Please set your timezone to access this feature.');
	} else if (sessvar("isLLS") && FALSE) {
		$till_display = speak('This feature is currently unavailable for local server locations. It will be available with the next local server update.');
	} else {
		$till_display .= $extra_display_string;
	}
}
function setInitializeTillInOutButtonClasses(&$tillInButtonClass, &$tillOutButtonClass, $postTillInButtonClass, $postTillOutButtonClass){
	if(isset($postTillInButtonClass)){
		$tillInButtonClass = $postTillInButtonClass;
	}else{
		$tillInButtonClass = 'tillBtnSelectable';
	}
	if(isset($postTillOutButtonClass)){
		$tillOutButtonClass = $postTillOutButtonClass;
	}else{
		$tillOutButtonClass = 'tillBtnNotSelectable';
	}
}
function adjust_day() {
	if (isset($_GET['adjust_day'])) {
		// adjust the day and set it in a sessvar
		$i_day_adjust = (int)$_SESSION['till_summary_adjust_day'];
		$i_day_adjust += (int)$_GET['adjust_day'];
		$_SESSION['till_summary_adjust_day'] = $i_day_adjust;

		// redirect to not include the getvar
		$a_location_vars = $_GET;
		unset($a_location_vars['adjust_day']);
		$s_location = http_build_query($a_location_vars, '', '&');
		$s_current_location = $_SERVER['REQUEST_URI'];
		header("Location: ?$s_location");
	}
}

// draws the day adjust buttons
// sets the value for $i_day_adjust
// @$i_minimum_day_adjust: the minimum value that $i_day_adjust can have (-$i_minimum_day_adjust days previous)
// @$i_day_adjust: the current day adjustment (range -$i_minimum_day_adjust to 0), set upon calling this function
// @return: text to add to the header
function draw_adjust_day_buttons($i_minimum_day_adjust, &$i_day_adjust) {
	$s_retval = '';

	// get the day adjustment and the redirect location
	if (isset($_SESSION['till_summary_adjust_day'])) {
		$i_day_adjust = (int)$_SESSION['till_summary_adjust_day'];
	}
	$s_location = http_build_query($_GET, '', '&');

	// future days
	if ($i_day_adjust > $i_minimum_day_adjust) {
		$s_retval .= '<input type="button" value="<< ' . speak('Previous Day') . '" style="color:#555555" onclick="window.location=\'?'.$s_location.'&adjust_day=-1\'" /> ';
	}

	// previous days
	if ($i_day_adjust < 0 && $i_day_adjust != -1) {
		$s_retval .= ' <font style="color:#555555">'.abs($i_day_adjust).' days ago</font>';
		$s_retval .= '<input type="button" value="' . speak('Next Day') . ' >>" style="color:#555555" onclick="window.location=\'?'.$s_location.'&adjust_day=+1\'" /> ';
	}
	if ($i_day_adjust == -1) {
		$s_retval .= ' <font style="color:#555555">1 day ago</font> ';
		$s_retval .= '<input type="button" value="' . speak('Current Day') . ' >>" style="color:#555555" onclick="window.location=\'?'.$s_location.'&adjust_day=+1\'" /> ';
	}

	//$s_retval .= '<br /><br />';
	return $s_retval;
}

// draws a select box that includes the name of every server and their id as the value
function draw_server_select($date, $monetary_symbol, $loc_id) {
	global $drawer;

	// get the users
	$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('users', array('active'=>'1', '_deleted'=>'0'), FALSE, array('selectclause'=>'`id`,`access_level`,`username`,CONCAT(`f_name`,\' \',`l_name`) AS `fullname`', 'orderbyclause'=>'ORDER BY `fullname`'));

	// get the default variable values
	$i_current_server = get_register_server_till();
	$b_active_server_is_admin = active_server_is_admin($a_users);
	$a_select_code = array('<select id="server_id_select" name="server_id_select" onchange="document.getElementById(\'server_id_submit\').click();" '.(!$b_active_server_is_admin ? 'style="display:none;"' : '').'>', '</select>');
	$i_default_server_id = (isset($_GET['server_id']) ? (int)$_GET['server_id'] : 1);

	// check that there are any
	if (count($a_users) == 0) {
		return $a_select_code[0].'<option value="'.$i_default_server_id.'">' . speak('No Users Found') . '</option>'.$a_select_code[1];
	}

	// create an option for each
	$a_options = array();
	foreach($a_users as $a_user) {
		$s_selected = ($i_current_server == (int)$a_user['id']) ? 'SELECTED' : '';
		if ($s_selected == '' && !$b_active_server_is_admin)
			continue;
		$a_latest_till = get_last_till_record_today('server', $a_user['username'], $date, $monetary_symbol, $loc_id, $drawer);
		if ($a_latest_till === NULL) {
			$till_status = '';
		} else {
			$till_status = ((string)$a_latest_till['timeout'] == 'NULL') ? ' - tilled in' : ' - tilled out';
		}
		$a_options[] = '<option value="'.$a_user['id'].'" '.$s_selected.'>'.$a_user['fullname'].$till_status.'</option>';
	}

	//$a_select_code[1] .= '<div style="position:absolute; top:0; left:0; width:1000px; height:600px; background-color:white; color:black; text-align:left; overflow-y:auto;"><pre>'.print_r($_GET,TRUE).'</pre></div>';

	return $a_select_code[0].implode('', $a_options).$a_select_code[1];
}

// returns the form inputs as a string
function draw_till_form_elements($bill_denominations, $coin_denominations, $bill_counts, $coin_counts, $monetary_symbol) {
	global $testing;
	$value = 1;
	global $is_tilled_in;
	if(isset($is_tilled_in)) {
		$is_tilled_in = true;
	}else{
		$is_tilled_in = false;
	}

	$retval = "<table style='border 2px gray; font-size:14px; vertical-align:top;'><tr><td>".speak("Bills")."</td><td>".speak("Coins")."</td></tr>";
		$retval .= "<tr>";

		$retval .= "<td style='vertical-align:top;'><table>";
		$input_counter = 0;
		foreach ($bill_denominations as $key=>$bill) {
			if ($testing)
				$displayval = $value;
			else
				$displayval = '';//$bill_counts[$key];

			$retval .= "<tr><td style='text-align:right'>".$bill." x </td><td><input type='number' id='input_".$input_counter."' size='5' name='bill_".preg_replace('/[\.,]/','a',$bill)."' value='".$displayval."'";
			$retval .= use_number_pad_if_available("input_".$input_counter, $monetary_symbol.$bill." ".speak("Bill"), "multi_digit");
			$retval .= "></td></tr>";
			$value++;
			$input_counter++;


		}
		$retval .= "</table>";
		$retval .= "<td style='vertical-align:top;'><table>";
		foreach ($coin_denominations as $key=>$coin) {
			if ($testing)
				$displayval = $value;
			else
				$displayval = '';//$coin_counts[$key];


			$retval .= "<tr><td style='text-align:right'>".$coin." x </td><td><input type='number' id='input_".$input_counter."' size='5' name='coin_".preg_replace('/[\.,]/','a',$coin)."' value='".$displayval."'";
			$retval .= use_number_pad_if_available("input_".$input_counter, $monetary_symbol.$coin." ".speak("Coin"), "multi_digit");
			$retval .= "></td></tr>";
			$value++;
			$input_counter++;
		}
		$retval .= "</table>";

		$retval .= "</tr><tr>";
		$retval .= "<td colspan='2' style='text-align:center;'><input type='submit' id='submit_button'></td>";
		$retval .= "</tr>";

	$retval .= "</table>";



	return $retval;
}

// returns the latest till record from the given date
// if a till record is still open for that date, it will return that till record, instead
// @return_if_full returns a till record even if both the till_in_time and till_out_time are filled
function get_last_till_record_today($register_id, $register_name, $date, $monetary_symbol, $loc_id, $isRegisterTill, $drawer) {
	//Create an associative array corresponding to all tills for the current date.
	$tills = get_tills_between_times($register_id, $register_name, $date, $monetary_symbol, $loc_id, NULL, NULL, $isRegisterTill, FALSE, $drawer);

	//Grab the same report query variables used in the get_tills_between_times function.
	$report_values = get_report_query_variables($register_id, $register_name, $date, $monetary_symbol, $loc_id, NULL, NULL, $isRegisterTill, $drawer);

	$retval = NULL;
	if (count($tills) > 0) {
		$retval = $tills[0]; //set the returned value to the first till row returned from the database.
		foreach ($tills as $till) {
			$tilltimein = strtotime($till['timein']); //timein corresponds with 'value2' from the cash_data table in the database.
			if (strtotime("2013-01-01 ".date("H:i:s", $tilltimein)) < strtotime("2013-01-01 ".date("H:i:s", $report_values['day_start_end_time']))) { //If the day's start time is greater than the till-in time (meaning till time in was yesterday and time out was today?)
				$tilltimein += 60 * 60 * 24; //increase the till-in time by one day.
			}
			//if the till time-in for this row is greater than the first row's till time set the returned row to the current row (so we are going to return the till-in with the latest value.)
			if ($tilltimein > strtotime($retval['timein']) && $till['register_name'] == $register_name) {
				$retval = $till;
			}
		}
	}
	return $retval;
}

// draws the form for tilling in/out
function draw_form($latest_till, $is_tilled_in, $loc_id) {
	$retval = "";

	if ($is_tilled_in) {
		// denominations
		$denominations = $latest_till['denominations'];
		$bill_denominations = explode(":", $denominations);
		$bill_denominations = $bill_denominations[0];
		if (strlen($bill_denominations) > 0)
			$bill_denominations = explode("/", $bill_denominations);
		else
			$bill_denominations = array();
		$coin_denominations = explode(":", $denominations);
		$coin_denominations = $coin_denominations[1];
		if (strlen($coin_denominations) > 0)
			$coin_denominations = explode("/", $coin_denominations);
		else
			$coin_denominations = array();
		// counts
		$bill_counts = explode(":", $latest_till['bills']);
		$bill_counts = $bill_counts[0];
		if (strlen($bill_counts) > 0)
			$bill_counts = explode("/", $bill_counts);
		else
			$bill_counts = array();
		$coin_counts = explode(":", $latest_till['coins']);
		$coin_counts = $coin_counts[0];
		if (strlen($coin_counts) > 0)
			$coin_counts = explode("/", $coin_counts);
		else
			$coin_counts = array();
		$saveinout = "saveout";
	} else {
		$dev = "cp"; if (count(explode("/dev/",$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'])) > 1) $dev = "v2";
		$backup_dir = (strpos(dirname(__FILE__), "/dev/") !== FALSE) ? "../" : "";
		require_once(dirname(__FILE__)."/../../".$backup_dir.$dev."/areas/edit_monetary_denominations.php");
		$denominations = get_db_denominations();
		$bill_denominations = $denominations['bills'];
		$coin_denominations = $denominations['coins'];
		$bill_counts = array();
		foreach($bill_denominations as $denomination)
			$bill_counts[] = "";
		$coin_counts = array();
		foreach($coin_denominations as $denomination)
			$coin_counts[] = "";
		$saveinout = "savein";
	}

	$retval .= "<form method='post'>";
	$retval .= draw_till_form_elements($bill_denominations, $coin_denominations, $bill_counts, $coin_counts, get_monetary_symbol($loc_id));
	$retval .= "<input type='hidden' name='".$saveinout."' value='1'>";
	$retval .= "</form>";

	return $retval;
}

// @savein TRUE or FALSE, saving the in till (should be true if saveout is true)
// @saveout TRUE or FALSE, saving the out till
// @decimal_char the decimal character that this location uses (, or .)
// @today today's data as y-m-d
// @register_id the register id
// @register_name the register name
// @latest_till the till to save
function save_till($savein, $saveout, $loc_id, $decimal_char, $monetary_symbol, $today, $register_id, $register_name, $latest_till, $real_name, $dbDenominations, $drawer) {
	global $isRegisterTill;
	global $till_table; //for cash reconciliation, this is cash_data.
	if($savein == 'true'){
		$savein = true;
	}else{
		$savein = false;
	}

	$report_options = get_report_query_variables($register_id, $register_name, $today, $monetary_symbol, $loc_id, NULL, NULL, $isRegisterTill, $drawer);
	$today = date("y-m-d", $report_options['location_time']);

	// get the denominations and their count
	$bills = array(); // form array(array(denomination1,count1), array(denomination2,count2))
	$coins = array(); // form array(array(denomination1,count1), array(denomination2,count2))
	$coin_denoms = $dbDenominations['coins'];
	$bill_denoms = $dbDenominations['bills'];

	foreach($bill_denoms as $value){ //keys will be integers, it should be a normal array.
		$bills[] = array($value,"");
	}
	foreach($coin_denoms as $value){
		$coins[] = array($value,"");
	}
	foreach($bills as $key=>$value){
		$billValue = trim($_POST["bills" . ($key)]); //e.g. bills0, bills1, ..., etc.
		if(isset($billValue) && $billValue != "") {
			$bills[$key][1] = $billValue;
		}else{
			$bills[$key][1] = 0;
		}
	}
	foreach($coins as $key=>$value){
		$coinValue = trim($_POST["coins" . ($key)]); //e.g. coins0, coins1, ..., etc.
		if(isset($coinValue) && $coinValue != ""){
			$coins[$key][1] = $coinValue;
		}else{
			$coins[$key][1] = 0;
		}
	}

	// get the denominations
	$denominations = array();
	if (!$savein) {
		$denominations = get_denominations_from_till($latest_till);
		// compare the denominations against those saved
		$newbills = array();
		$bill_denominations = $denominations['bills'];
		foreach($bills as $bill) {
			foreach($bill_denominations as $olddenomination) {
				if ($bill[0] == $olddenomination) {
					$newbills[] = $bill;
					break;
				}
			}
		}
		$bills = $newbills;
		$newcoins = array();
		$coin_denominations = $denominations['coins'];
		foreach($coins as $coin) {
			foreach($coin_denominations as $olddenomination) {
				if ($coin[0] == $olddenomination) {
					$newcoins[] = $coin;
					break;
				}
			}
		}
		$coins = $newcoins;
	} else {
		$bill_denominations = array();
		foreach($bills as $bill) {
			$bill_denominations[] = $bill[0];
		}
		$denominations['bills'] = $bill_denominations;
		$coin_denominations = array();
		foreach($coins as $coin) {
			$coin_denominations[] = $coin[0];
		}
		$denominations['coins'] = $coin_denominations;
	}
	// get the totals
	$count = 0;
	foreach($bills as $bill) {
		$count += ($bill[0]*1)*($bill[1]*1);
	}
	foreach($coins as $coin) {
		$count += ($coin[0]*1)*($coin[1]*1);
	}
	if (!$savein) {
		$total_parts = explode(":", $latest_till['totals']);
		$total_in = $total_parts[0];
		$total_out = $count;
	} else {
		$total_out = "NULL";
		$total_in = $count;
	}
	// get the time
	if (!$savein) {
		$time_in = $latest_till['timein'];
		$time_out = date("H:i:s", $report_options['location_time']);
	} else {
		$time_out = "NULL";
		$time_in = date("H:i:s", $report_options['location_time']);
	}
	// get the bills/coins in/out
	if (!$savein) {
		$billsin_parts = explode(":", $latest_till['bills']);
		$billsin = $billsin_parts[0];
		$billsout = array();
		foreach($bills as $bill) {
			$billsout[] = $bill[1];
		}
		$billsout = implode("/", $billsout);
		$billsinout = $billsin . ":" . $billsout;
		$coinsin_parts = explode(":", $latest_till['coins']);
		$coinsin = $coinsin_parts[0];
		$coinsout = array();
		foreach($coins as $coin) {
			$coinsout[] = $coin[1];
		}
		$coinsout = implode("/", $coinsout);
		$coinsinout = $coinsin . ":" . $coinsout;
	} else {
		$billsout = "NULL";
		$billsin = array();
		foreach($bills as $bill) {
			$billsin[] = $bill[1];
		}
		$billsin = implode("/", $billsin);
		$billsinout = $billsin . ":" . $billsout;
		$coinsout = "NULL";
		$coinsin = array();
		foreach($coins as $coin) {
			$coinsin[] = $coin[1];
		}
		$coinsin = implode("/", $coinsin);
		$coinsinout = $coinsin . ":" . $coinsout;
	}
	// save it
	$savevals = array();
	foreach ($report_options as $key=>$value)
		if (gettype($value) !== gettype(array()))
			$savevals[$key] = $value;

	if($isRegisterTill){
		$report_options['setting1'] = str_replace('[register_id]',$latest_till['server_name'],$report_options['setting1']);
	}
	$savevals['setting'] = $report_options['setting1'];
	foreach ($report_options as $key=>$value)
		if (count(explode("[$key]",$savevals['setting'])) > 1)
			$savevals['setting'] = str_replace("[$key]", $value, $savevals['setting']);
	$savevals['value'] = $total_in.":".$total_out;
	$savevals['value2'] = $time_in;
	$savevals['value3'] = $time_out;
	$savevals['value4'] = $billsinout;
	$savevals['value5'] = $coinsinout;
	$savevals['value6'] = implode("/", $denominations['bills']) . ":" . implode("/", $denominations['coins']);
	$savevals['type'] = "location_config_setting";

	//Added by Brian D.
	$savevals['date'] = constructDateFromCashDataSettingsField($savevals['setting']);
	$savevals['server_id'] = !empty($_REQUEST['server_id']) ? $_REQUEST['server_id'] : '';
	$savevals['server_time'] = date('Y-m-d H:i:s');
	$savevals['drawer'] = $drawer;
	//end addition.


	if ($time_out === "NULL") {
		$value_names = array("type", "setting", "value",  "value2",  "value3",  "value4",  "value5",  "value6", "date", "server_id", "server_time", 'drawer');
		lavu_query("INSERT INTO `".$till_table."` (`".implode("`,`",$value_names)."`) VALUES ('[".implode("]','[",$value_names)."]')", $savevals);
	} else {
		$whereclause = "`type` = '[type]' AND `value3` = 'NULL' AND `setting`='[setting]' AND `drawer` = '[drawer]'";
		$setclause = "`value`='[value]',`value3`='[value3]',`value4`='[value4]',`value5`='[value5]',`value6`='[value6]'," .
		                 "`date`='[date]',`server_id`='[server_id]',`server_time`='[server_time]'";
		$querystring = "UPDATE `".$till_table."` SET ".$setclause." WHERE ".$whereclause;
		lavu_query($querystring, $savevals);
	}
}

function constructDateFromCashDataSettingsField($tillSetting){
    $year_2_digits  = substr($tillSetting, 12, 2);
    $month_2_digits = substr($tillSetting, 15, 2);
    $day_2_digits   = substr($tillSetting, 18, 2);
    $fullYear       = substr(date('Y'), 0, 2) . $year_2_digits;
    $fullDate       = $fullYear . '-' . $month_2_digits . '-' . $day_2_digits;
    return $fullDate;
}

$draw_till_line_index = 1;
function draw_till_line($timein, $timeout, $register, $register_name, $today, $monetary_symbol, $loc_id, $server_name, $server_id, $drawer) {
	global $tillname;
	global $draw_till_line_index;
	global $real_name;
	global $usersInfo;
	global $data_name;
	global $dualDrawerEnabled;
	global $active_access_level;

	if (gettype($timein) == gettype("string"))
		$timein = strtotime($timein);
	if (gettype($timeout) == gettype("string") && $timeout !== "NULL" && $timeout !== "-" && $timeout !== "")
		$timeout = strtotime($timeout);
	$draw_till_line_index++;

	if ($dualDrawerEnabled && $active_access_level >= 3) {
		$data['drawer'] = $drawer;
		$data['serverId'] = $server_id;
		$data['action'] = 'getdrawerprinter';
		$apiObj = new ServerCashDrawerApi($data_name, $data);
		$drawerResponse = json_decode($apiObj->doProcess(), true);
		if ($drawerResponse['status']) {
			$register = ($drawerResponse['data'][0]['printer']) ? $drawerResponse['data'][0]['printer'] : $register;

		}
	}

	$tillLineNames = ($drawer > 0) ? $register .' - ' . speak('Assigned to') . ' <b>' . str_replace('<', '&lt;', $usersInfo[$server_id]['full_name']) . '</b>' : ucfirst($server_name) . ' - ' . $register_name;
	$str = "<div class='tillLineContainer'>";
	$str .=     "<div class='tillLine'>
					<div class='tillLineNames'>" . $tillLineNames . "</div>
					<div class='tillLineTimes'>".draw_till_time($timein) . " - " . draw_till_time($timeout)."</div> 
				</div>";
	$str .=     "<div class='tillLineButtons'>
					<div class='tillLineViewSummary'>".draw_till_button($timein, $timeout, "report", "view", $server_id, $register_name) ."</div>";
	$str .=         "<div class='tillLinePrintSummary'>".draw_print_till_button($register, $register_name, $today, $monetary_symbol, $loc_id, $timein, $timeout, "print", FALSE, $real_name, TRUE, $server_id)."</div>
				</div>
				<div class='spaceBelowTill'></div>
			</div>";
	return $str;
}

function draw_till_time($ts) {
	if ($ts === "NULL" || $ts === "-" || $ts === "") {
		$otherstring = "__";
		$hour = "$otherstring";
		$min = "$otherstring";
		$sec = "$otherstring";
		$ampm = "$otherstring";
	} else {
		$hour = date("h",$ts);
		$min = date("i",$ts);
		$sec = date("s",$ts);
		$ampm = date(" a",$ts);
	}
	return "<font color='#008'>" . $hour.":".$min . "</font><font color='#999'>:" . $sec . "</font> <font color='#008'>" . $ampm . "</font>";
}

function draw_print_till_button($register, $register_name, $today, $monetary_symbol, $loc_id, $timein, $timeout, $drawname="print", $isDailyTill=FALSE, $printSingleTill=FALSE) {
	global $tillsummary; // Already translated. Do not feed to speak().
	global $cc;
	global $access_level;
	global $real_name;
	global $isRegisterTill;
	global $drawer;
	if ($timeout === "NULL" || $timeout === "-" || $timeout === "")
		$datetimeout = "NULL";
	else
		$datetimeout = date("H:i:s",$timeout);
	if ($timein === "NULL" || $timein === "-" || $timein === "")
		$datetimein = "NULL";
	else
		$datetimein = date("H:i:s",$timein);
	$register = urlencode($register);
	$register_name = urlencode($register_name);
	$today = urlencode($today);
	$monetary_symbol = urlencode(urlencode($monetary_symbol));
	$loc_id = urlencode($loc_id);
	$datetimein = urlencode($datetimein);
	$datetimeout = urlencode($datetimeout);
	$drawname = explode(" ", $drawname);
	$i_need_a_mode_in_order_for_it_to_print = 1;
	foreach ($drawname as $k=>$v)
		$drawname[$k] = ucfirst($v);
	$drawname = speak(implode(" ", $drawname));

	$a_shift_info = array(
		"today" => $today,
		"reg" => $register,
		"rn" => $register_name,
		"ms" => $monetary_symbol,
		"timein" => $datetimein,
		"timeout" => $datetimeout,
	);

	$a_shift_info_components = array();
	foreach( $a_shift_info as $key => $val ) {
		$a_shift_info_components[] = ($key) . '(eq)' . ($val);
	}

	$s_shift_info = implode('(and)', $a_shift_info_components);

	$a_my_vars = array(
		"from_mgmt_area" => 1,
		"ts_access_level" => $access_level,
		"shift_info" => $s_shift_info,
		"isDailyTill" => $isDailyTill,
		"printSingleTill" => $printSingleTill,
		"real_name" => $real_name,
		"isRegisterTill" => $isRegisterTill,
		"drawer" => $drawer
	);

	$a_query = array(
		'cmd' => 'print',
		'filename' => 'transaction_summary.php',
		'm' => $i_need_a_mode_in_order_for_it_to_print,
		'dn' => $cc,
		'loc_id' => $loc_id,
		'register' => $register,
		'vars' => http_build_query( $a_my_vars )
	);
	return "<div onclick='window.location = \"_DO:".http_build_query($a_query)."\"'>".$drawname." ".$tillsummary."</div>";
}

function draw_till_button($timein, $timeout, $postname, $drawname, $server_id = 0, $register_name = '') {
	global $tillsummary; // Already translated. Do not feed to speak().
	global $i_adjust_day;
	global $isRegisterTill;
	global $drawer;
	if ($timeout === "NULL" || $timeout === "-" || $timeout === "")
		$datetimeout = "NULL";
	else
		$datetimeout = date("H:i:s",$timeout);
	if ($timein === "NULL" || $timein === "-" || $timein === "")
		$datetimein = "NULL";
	else
		$datetimein = date("H:i:s",$timein);
	$drawname = explode(" ", $drawname);
	$register_name = str_replace('<', '&lt;', $register_name);
	foreach ($drawname as $k=>$v)
		$drawname[$k] = ucfirst($v);
	$drawname = speak(implode(" ", $drawname));
	$formname = "tillform" . $timein . $timeout;
	$s_retval = "<form method='post' name='$formname' id='$formname' action=''>
		<input type='hidden' name='timein' value='".$datetimein."'>
		<input type='hidden' name='timeout' value='".$datetimeout."'>
		<input type='hidden' name='".$postname."' value='1'>
		<input type='hidden' name='isPreviousDay' value='1'>
		<input type='hidden' name='adjust_day' value='$i_adjust_day'>
		<input type='hidden' name='isRegisterTill' value='$isRegisterTill'>
		<input type='hidden' name='cashDrawerId' value='$drawer'>
		<input type='hidden' name='real_name' value='".urlencode($register_name)."'>
		<input type='hidden' name='cashDrawerAssignedServerId'value='". $server_id."'>
		<div onclick='document.getElementById(\"$formname\").submit()'>".$drawname." ".$tillsummary."</div>
	</form>";
	return $s_retval;
}

function draw_daily_buttons($register, $register_name, $today, $monetary_symbol, $loc_id) {
	$retval = "<table><tr>";
	$retval .= "<td>".draw_till_button("NULL", "NULL", "report", "view daily")."</td><td width='12'>&nbsp;</td>";
	$retval .= "<td>".draw_print_till_button($register, $register_name, $today, $monetary_symbol, $loc_id, "NULL", "NULL", "print daily", FALSE)."</td>";
	$retval .= "</tr></table>";
	return $retval;
}

function draw_tills_table($register_id, $register_name, $date, $monetary_symbol, $loc_id, $starttime=NULL, $endtime=NULL, $isRegisterTill, $drawer) {
	global $tillname;

	$tills = get_tills_between_times($register_id, $register_name, $date, $monetary_symbol, $loc_id, $starttime, $endtime, $isRegisterTill, false, $drawer);
	$retval = "";

	$numTills = count($tills);
	if ($numTills > 0){
		$i = 0;
		foreach ($tills as $till) {
			$retval .= draw_till_line($till['timein'], $till['timeout'], $register_id, $till['register_name'], $date, $monetary_symbol, $loc_id, $till['server_name'], $till['server_id'], $till['drawer']);
			if($i < $numTills - 1) {
				$retval .= "<hr>";
			}
			$i++;
		}
	}else {
		$register_name = str_replace('<', '&lt;', $register_name);
		$retval .= speak("No tills to report for") . " " . $register_name;
	}
	return $retval;
}

function show_shift_time($dt)
{
	$dt = explode(" ",$dt);
	if(count($dt) > 1)
	{
		$dt_date = explode("-",$dt[0]);
		$dt_time = explode(":",$dt[1]);

		if(count($dt_date) > 2 && count($dt_time) > 2)
		{
			$dt_year = $dt_date[0];
			$dt_month = $dt_date[1];
			$dt_day = $dt_date[2];
			$dt_hour = $dt_time[0];
			$dt_mins = $dt_time[1];
			$dt_secs = $dt_time[2];
			$ts = mktime($dt_hour,$dt_mins,$dt_secs,$dt_month,$dt_day,$dt_year);


			return date("h:i",$ts) . ":" . date("s",$ts) . date(" a",$ts);
		}
	}
}

//TODO: Convert generateBillDenominationInputs and generateCoinDenominationInputs into a single function.
function generateBillDenominationInputs($db_denominations, $monetary_symbol){
	$denomination_bills = $db_denominations['bills'];
	$numDenoms = count($denomination_bills);
	$denominationsOutputHTML = "";
	for($i = 0; $i < $numDenoms; $i++) {
		$inputID = "billsInput".$i;
		$outputID = "billsTotal".$i;
		$spanID = "bills".$i;
		$inputClass = "billInput";
		$inputName = "bills".$i;
		$padDescription = $monetary_symbol." ".$denomination_bills[$i]." ".speak('BILLS');
		$denominationsOutputHTML .= "<li class='tillListItem'>
										<span id=\"".$spanID."\">".$denomination_bills[$i]."</span>
										&nbsp;x
										<input class='$inputClass' name='$inputName' placeholder='0' type=\"number\" id=\"$inputID\" ".till_number_pad_if_available($inputID, $padDescription, 'multi_digit', $denomination_bills[$i], 'bill', $i)."'/>
										".speak('Total').":
										<output class='tillSummable bill' id=\"".$outputID."\">
										".str_replace(' ','',till_display_price('0.00'))."
										</output>
							        </li>";
	}
	return $denominationsOutputHTML;
}

function generateCoinDenominationInputs($db_Denominations, $monetary_symbol){
	$denomination_coins = $db_Denominations['coins'];
	$numDenoms = count($denomination_coins);
	$denominationsOutputHTML = "";
	for($i = 0; $i < $numDenoms; $i++) {
		$inputID = "coinsInput".$i;
		$outputID = "coinsTotal".$i;
		$spanID = "coins".$i;
		$inputClass = "coinInput";
		$inputName = "coins".$i;
		$padDescription = $monetary_symbol." ".$denomination_coins[$i]." ".speak('COINS');
		$denominationsOutputHTML .= "<li class='tillListItem'>
										<span id=\"".$spanID."\">".$denomination_coins[$i]."</span>
										&nbsp;x
										<input class='$inputClass' name='$inputName' placeholder='0' type=\"number\" id=\"$inputID\" ".till_number_pad_if_available($inputID, $padDescription, 'multi_digit', $denomination_coins[$i], 'coin', $i)."/>
										".speak('Total').":
										<output class='tillSummable coin' id=\"$outputID\">
										".str_replace(' ','',till_display_price('0.00'))."
										</output>
							        </li>";
	}
	return $denominationsOutputHTML;
}

function till_number_pad_if_available($field_id, $title, $type, $fieldVal, $denomType, $index) {
	global $available_DOcmds;
	$str = "";
	if (in_array("number_pad", $available_DOcmds)) {
		$str = " READONLY onclick='window.location = \"_DO:cmd=number_pad&type=" . $type . "&max_digits=15&title=" . rawurlencode($title) . "&js=performTillInputUpdate(%27".$field_id."%27,%27".$fieldVal."%27,%27".$denomType."%27,%27".$index."%27,%27ENTERED_NUMBER%27);\"'";
	}
	return $str;
}
function generateTillView($register, $register_name, $today, $monetary_symbol, $loc_id, $timein, $timeout ,$fwd_vars, $shift_time, $isRegisterTill, $big_title, $db_denominations){

//Include the post variables.
	global $real_name;
	global $till_in;
	global $i_adjust_day;
	global $isPreviousDay;
	global $tillInButtonClass;
	global $tillOutButtonClass;
	global $monetary_symbol;
	global $left_or_right;
	global $decimal_char;
	global $data_name;
	global $dualDrawerEnabled;
	global $usersInfo;
	global $active_register;
	global $active_server_id;
	global $active_server_name;
	global $active_access_level;
	global $drawer;
	global $errorMsg;

	$dualDrawerManagerEnabled = false;
	if ($dualDrawerEnabled) {
		$dualDrawerManagerEnabled = ($active_access_level >= 3) ? true : false;
		// if dualcash drawer enabled and server is manager then enabling both till in and till out button
		$till_in = ($active_access_level >= 3) ? true : $till_in;
		$data['printer'] = $active_register;
		$data['drawer'] = $drawer;
		$data['updatedDate'] = $today;
		$data['action'] = 'getcashdrawer';
		$apiObj = new ServerCashDrawerApi($data_name,$data);
		$drawerResponse = json_decode($apiObj->doProcess(), true);
		if ($drawerResponse['status']) {
			foreach ($drawerResponse['data'] as $details) {
				$serversDrawer[$details['printer']][$details['drawer']][] = $details['server_id'];
				$printerServer[$details['printer']][$details['server_id']] = $details['drawer'];
				$assignedServersDrawer[$details['printer']][$details['drawer']][$details['server_id']] = $usersInfo[$details['server_id']]['full_name'];

			}

			$cashDrawerAssignedServerId = $active_server_id;//$serversDrawer[$active_register][$drawer];
		}

	}

	$view = "<script>";
	$view .=   "var postVars = ".json_encode($_POST).";
				var numberOfDays = '".$i_adjust_day."';
				var isPreviousDay = ".$isPreviousDay.";
				var till_in = ".$till_in.";
				var error_msg = '".$errorMsg."';
				var dualDrawerManagerEnabled = '".$dualDrawerManagerEnabled."';
				";
	$view .= "</script>";

	$view .= "<!-- Start top bars & title -->
		    <div class=\"container\" id=\"topBar\">
			      <div class=\"btn\" id=\"backBtn\" onclick='window.location = \"inapp_management.php?mode=menu&".$fwd_vars."\"'>
			        ".speak('Back')."
			      </div>
			      <div id=\"pageTitle\">$big_title</div>
			</div>
		<!-- End top bar & title -->";

	if ($dualDrawerEnabled) {
		$drawerOneClass = ($drawer == 1 ) ? 'class = "drawerBtnSelected"' : '';
		$drawerTwoClass = ($drawer == 2 ) ? 'class = "drawerBtnSelected"' : '';

		$view .= "<!-- Start Dual Cash Drawer button -->
			    <div class=\"container\" id=\"cashDrawer\">
				      <button id=\"cashDrawerOneBtn\" " . $drawerOneClass . " onclick='selectCashDrawer(\"1\");'>
							".speak('Cash Drawer 1')."
						</button>
						<button id=\"cashDrawerTwoBtn\" " . $drawerTwoClass . " onclick='selectCashDrawer(\"2\");'>
							".speak('Cash Drawer 2')."
						</button>
			    </div>
	            <!-- Dual Cash Drawer button -->";
	}

	$view .= "<!-- Start view/print bar-->
			<div class='container' id='tillOptionsContainer'>
				<div class='viewDailyTillBtn'>
					". draw_till_button("NULL", "NULL", "report", "view daily") ."
				</div>
				<div class='printDailyTillBtn'>
					".draw_print_till_button($register, $register_name, $today, $monetary_symbol, $loc_id, $timein, $timeout, $drawname="print daily", TRUE)."
				</div>
			</div>
			<!-- End view/print bar-->
			";

	$view .= "<!-- Start navigation & date -->
		    <div class=\"container\" id=\"navigation\" method='post'>
		    
				<div class=\"$tillInButtonClass\" id=\"tillInButton\" onclick='selectButton(\"till_in\")'>
					".speak('Till In')."
				</div>";
				if ($dualDrawerManagerEnabled) {
					$view .= "
					<div class=\"$tillOutButtonClass\" id=\"tillOutButton\" onclick='getTilledInServerList(\"" . $drawer . "\"); selectButton(\"till_out\")'>
						".speak('Till Out')."
					</div>";
				} else {
					$view .= "
					<div class=\"$tillOutButtonClass\" id=\"tillOutButton\" onclick='selectButton(\"till_out\")'>
						".speak('Till Out')."
					</div>";
				}
	$view .= "
				<div id=\"dateContainer\">
					<button class=\"btn\" id=\"previousBtn\" onclick='changeDate(\"-1\", numberOfDays);'>
						".speak('Previous Day')."
					</button>
					<div class=\"current\"> 
						<span id=\"currentDate\"><strong>$shift_time</strong></span>
					</div>
					<button class=\"btn\" id=\"nextBtn\" style='visibility:hidden' onclick='changeDate(\"+1\", numberOfDays);'>
						".speak('Next')."
					</button>
				</div>
		    </div>
		    <!-- End navigation & date -->
		    ";

 	$view .= "<!-- Start quantity -->
			<form action='' id='tillEntryForm' method='post'>
				<div class=\"container\" id=\"quantityContainer\" style='visibility: hidden; display:none'>
					<!-- Start Bills -->
					<div class=\"col\" id=\"bills\">
						<div class=\"colHeader\">
							".speak('bills')."
						</div>
						<ul>
							".generateBillDenominationInputs($db_denominations, $monetary_symbol)."
						</ul>
					</div>
					
					<!-- End Bills -->
					<!-- Start Coins -->
					<div class=\"col\" id=\"coins\">
						<div class=\"colHeader\">
							".speak('coins')."
						</div>
						<ul>
							".generateCoinDenominationInputs($db_denominations, $monetary_symbol)."
						</ul>
					</div>
					<!-- End Coins -->
					<div id='totalsContainer' style='height:100%'>
						<div class='denominationTotal'>".speak('Bill Total').": <output id=\"billTotal\" class='tillTotal'>".till_display_price('0.00')."</output></div>
						<div class='denominationTotal'>".speak('Coin Total').": <output id=\"coinTotal\" class='tillTotal'>".till_display_price('0.00')."</output></div>
								
						<div id='totalTillIn'>
							".speak('TILL TOTAL').": <output id='totalTillInValue' class='tillTotal'>".till_display_price('0.00')."</output>
					</div>";
	if ($dualDrawerEnabled) {
		$assignedServersToDrawer = '';
		$view .= "<div class=\"container\" id='assignDrawer'>";
		if ($active_access_level >= 3) {
			$brk = '';
			foreach ($serversDrawer[$active_register][$drawer] as $assignedServer) {
				if ($assignedServer > 0 ) {
					$assignedServersToDrawer .= $brk . "<div class='pull-left' id='cashDrawerAssignedServerName'>" . str_replace('<', '&lt;', $assignedServersDrawer[$active_register][$drawer][$assignedServer]) . "</div>";
					$brk = '<br/>';
				}
			}
			$view .= "<div class='pull-left padding-top-20' id='cashDrawerAssigned'><div class='pull-left cashDrawerinfo'>" . speak('Cash drawer assigned to') . ' ' . "</div><div class='pull-left' id='cashDrawerAssignedServerName'>" .  $assignedServersToDrawer . "</div></div>";
			$view .= "<div class='pull-right padding-right-10' id='assign'><button class='btn' id='reAssignSubmission'
                                onclick='getActiveServerList()'><span id='managerAssign'>" . speak('Assign') . "</span><span id='managerReassign' style='visibility: hidden; display:none'>". speak('Reassign') . "</span></button></div> ";
			if ($assignedServersToDrawer) {
				$view .= "<div class='pull-left padding-top-20' id='serverAssigned'><div class='pull-left cashDrawerinfo'>" . speak('Assigned Server : ') . ' ' . "</div><div class='pull-left' id='cashDrawerAssignedServerName'>" . $assignedServersToDrawer . "</div></div>";
			}
		} else {
			$active_server_name = str_replace('<', '&lt;', $active_server_name);
			$active_server_name = str_replace("'", '&#039;', $active_server_name);
			$view .= "<div class='pull-left padding-top-20' id='cashDrawerAssigned''><div class='pull-left cashDrawerinfo'>".speak('Cash drawer assigned to'). ' ' . "</div><div class='pull-left' id='cashDrawerAssignedServerName'>" . str_replace('<', '&lt;', $assignedServersDrawer[$active_register][$drawer][$active_server_id]) . "</div></div>";
		    $view .= "<div class='pull-right padding-right-10' id='assign'><button class='btn' id='reAssignSubmission' style='visibility: hidden; display:none'
				onclick='assignedDrawerToServer(\"$active_server_id\",\"" . addslashes($active_server_name) . "\");'>" . speak('Assign') . "</button></div>";
		}
		$view .= "</div>";
 	}
 	$view .= "<button class='btn' id='tillSubmission' style='visibility:hidden;display:none' onclick='submitTill()'>".speak('Submit')."</button>
		            </div>
				</div>
				<input id='savein' type='hidden' name='savein' style='display:none'/> <!-- value set on form submission -->
				<input id='saveout' type='hidden' name='saveout'style='display:none'/> <!-- value set on form submission -->
				<input id='isTillIn' type='hidden' name='isTillIn'style='display:none'/> <!-- value set on form submission -->
				<input id='adjust_day' type='hidden' name='adjust_day'style='display:none'/> <!-- value set on form submission -->
				<input id='numberOfDays' type='hidden' name='numberOfDays'style='display:none'/>
				<input id='isPreviousDay' type='hidden' name='isPreviousDay'style='display:none'/>
				<input id='big_title' type='hidden' name='big_title' value='$big_title'style='display:none'/>
				<input id='tillInButtonClass' type='hidden' name='tillInButtonClass'style='display:none'/>
				<input id='tillOutButtonClass' type='hidden' name='tillOutButtonClass'style='display:none'/> 
				<input id='tillOutButtonClass' type='hidden' name='tillOutButtonClass'style='display:none'/>
				<input id='cashDrawerId' type='hidden' name='cashDrawerId' value='".$drawer."' style='display:none'/>
				<input id='cashDrawerAssignedServerId' type='hidden' name='cashDrawerAssignedServerId' value='".$cashDrawerAssignedServerId."' style='display:none'/>
			</form>
	        <!-- End Quantity -->
	        ";
	/*View or print till display*/
	$view .= "<div class='container' id='tillListContainer'>
				<div class='tillListContent'>".draw_tills_table($register, $register_name, $today, $monetary_symbol, $loc_id, null, null, $isRegisterTill, $drawer)."</div>
			</div>";

	if(isset($decimal_char)) { //Calculate the number of places after the decimal point.
		$sample = till_display_price("0.00");
		$sample = (str_replace($monetary_symbol, '', $sample));
		$splitSample = explode($decimal_char, $sample);
		$decimals = preg_replace("/[^0]/", "", $splitSample[1]); //replace all non-zero characters with nothing, so we just extract the 0's to determine length after decimal place.
		$decimal_places = strlen($decimals);
	}else{
		$decimal_places = 0;
	}

	$view .= "<script type='text/javascript'>

				function setIsTillIn(){
					if(till_in === '1'){
						isTillIn = '';
					}else if(till_in == 'true' || till_in === true){
						isTillIn = true;
					}else if(till_in == 'false' || till_in === false){
						isTillIn = false;
					}
				}
				function setTillInOutButtonClasses(isTillIn){
					if(isPreviousDay == '1'){
						document.getElementById('tillInButton').className = 'tillBtnNotSelectable';
						document.getElementById('tillOutButton').className = 'tillBtnNotSelectable';
						document.getElementById('nextBtn').style.visibility = 'visible';
					}else if(isTillIn == true || isTillIn == 'true'){
						document.getElementById('tillInButton').className = 'tillBtnSelectable';
						document.getElementById('tillOutButton').className = 'tillBtnNotSelectable';
						document.getElementById('nextBtn').style.visibility = 'hidden';
					}else if(till_in == 'false' || till_in == false){
						document.getElementById('tillInButton').className = 'tillBtnNotSelectable';
						document.getElementById('tillOutButton').className = 'tillBtnSelectable';
						document.getElementById('nextBtn').style.visibility = 'hidden';
					}
					if (dualDrawerManagerEnabled) {
						document.getElementById('tillOutButton').className = 'tillBtnSelectable';
					}
				}

				window.onload = function() {
					validateDrawer();
					setIsTillIn();
					setTillInOutButtonClasses(isTillIn);
			    }

				var isTillIn;
				var till_in = $till_in;
				initView();

				function initView(){
					isTillIn = till_in;
					if(isTillIn == undefined || isTillIn == 'true'){
						isTillIn = true;
					}else if(isTillIn == 'false' || isTillIn == false){
						isTillIn = false;
					}
				}
				<!-- Saving savein/saveout variable. -->
				function setSaveInOut(){
					if(document.getElementById('tillOutButton').className == 'tillBtnSelected'){
						document.getElementById('savein').value='false';
						document.getElementById('saveout').value='true';
					}else{
						document.getElementById('savein').value='true';
						document.getElementById('saveout').value='false';
					}
				}

				function submitTill(){
					setSaveInOut();
					switchSelectableTillMode();
					scrollToTop();
					hideTillScreen();
					formatEmptyInputsForSubmission();
					setTillButtonClassesForSubmissionReload()
					document.getElementById('tillEntryForm').submit();
				}
	
				function setTillButtonClassesForSubmissionReload(){
						var tillInButtonClass = document.getElementById('tillInButtonClass');
						var tillOutButtonClass = document.getElementById('tillOutButtonClass');	
					if(isTillIn){
						tillInButtonClass.value = 'tillBtnNotSelectable';
						tillOutButtonClass.value = 'tillBtnSelectable';
					}else{
						tillInButtonClass.value = 'tillBtnSelectable';
						tillOutButtonClass.value = 'tillBtnNotSelectable';
					}
				}

				function selectButton(till_btn){
					initView();
					if (till_btn == 'till_in' && (isTillIn == true) && (dualDrawerManagerEnabled == false) ) {
						var tibElement = document.getElementById('tillInButton');
						var className = tibElement.className;
						if (className == 'tillBtnSelectable') {
							tibElement.className = 'tillBtnSelected';
							var cashDrawerAssignedServerName = document.getElementById('cashDrawerAssignedServerName');
							if (cashDrawerAssignedServerName != null) {
								cashDrawerAssignedServerName.innerHTML = '';
							}
							showTillScreen();
						} else if (className == 'tillBtnSelected') {
							tibElement.className = 'tillBtnSelectable';
							hideTillScreen();
						}
					} else if (till_btn == 'till_in' && (isTillIn == true) && (dualDrawerManagerEnabled == true) ) {
						var tibElement = document.getElementById('tillInButton');
						var className = tibElement.className;
						if (className == 'tillBtnSelectable') {
							var tobElement = document.getElementById('tillOutButton');
							tobElement.className = 'tillBtnSelectable';
							tibElement.className = 'tillBtnSelected';
							var cashDrawerAssignedServerName = document.getElementById('cashDrawerAssignedServerName');
							if (cashDrawerAssignedServerName != null) {
								cashDrawerAssignedServerName.innerHTML = '';
							}
							var assign = document.getElementById('assign');
						   if (assign != null) {
								assign.style.display = 'block';
								assign.style.visibility = 'visible';
							}
							var serverAssigned = document.getElementById('serverAssigned');
							if (serverAssigned) {
								document.getElementById('serverAssigned').style.display = 'block';
								document.getElementById('serverAssigned').style.visibility = 'visible';
							}
							showTillScreen();
						} else if(className == 'tillBtnSelected') {
								tibElement.className = 'tillBtnSelectable';
								hideTillScreen();
						}
					} else if(till_btn == 'till_out' && isTillIn == false && (dualDrawerManagerEnabled == false) ) {
						var tobElement = document.getElementById('tillOutButton');
						var className = tobElement.className;
						if (className == 'tillBtnSelectable') {
							tobElement.className = 'tillBtnSelected';
							var assign = document.getElementById('assign');
							if (assign != null) {
								assign.style.display = 'none';
								assign.style.visibility = 'hidden';
							}
							var cashDrawerAssignedServerName = document.getElementById('cashDrawerAssignedServerName');
							if (cashDrawerAssignedServerName != null) {
								cashDrawerAssignedServerName.style.display = 'block';
							}
							var serverAssigned = document.getElementById('serverAssigned');
							if (serverAssigned) {
								document.getElementById('serverAssigned').style.display = 'none';
								document.getElementById('serverAssigned').style.visibility = 'hidden';
							}
							showTillScreen();
						} else if (className == 'tillBtnSelected') {
							tobElement.className = 'tillBtnSelectable';
							hideTillScreen();
						}
					} else if (till_btn == 'till_out' && isTillIn == true && (dualDrawerManagerEnabled == true)) {
						var tobElement = document.getElementById('tillOutButton');
						var className = tobElement.className;
						if (className == 'tillBtnSelectable') {
							tobElement.className = 'tillBtnSelected';
							var tibElement = document.getElementById('tillInButton');
							tibElement.className = 'tillBtnSelectable';
							var assign = document.getElementById('assign');
							if (assign != null) {
								assign.style.display = 'none';
								assign.style.visibility = 'hidden';
							}
							var cashDrawerAssignedServerName = document.getElementById('cashDrawerAssignedServerName');
							if (cashDrawerAssignedServerName != null) {
								cashDrawerAssignedServerName.style.display = 'block';
							}
							var serverAssigned = document.getElementById('serverAssigned');
							if (serverAssigned) {
								document.getElementById('serverAssigned').style.display = 'none';
								document.getElementById('serverAssigned').style.visibility = 'hidden';
							}
							showTillScreen();
						} else if (className == 'tillBtnSelected') {
							tobElement.className = 'tillBtnSelectable';
							hideTillScreen();
						}
					}
				}
				
				function switchSelectableTillMode(){
						var tillInButton = document.getElementById('tillInButton');
						var tillOutButton = document.getElementById('tillOutButton');
						if(!isTillIn){
							tillInButton.className = 'tillBtnNotSelectable';
							tillOutButton.className = 'tillBtnSelectable';
						}else{
							tillInButton.className = 'tillBtnSelectable';
							tillOutButton.className = 'tillBtnNotSelectable';
						}
				}

				
				<!-- Till summation -->
				function sumBills(){
					var billElements = document.getElementsByClassName('tillSummable bill');
					var sum = 0;
					var numeric = '';
					for(var i = 0; i < billElements.length; i++){
						numeric = (billElements[i].value).replace(/\s/g,'');
						numeric = (numeric).replace(/^\D+/g,'');
						numeric = (numeric).replace(/\D/g ,'.');
						if(!isNaN(parseFloat(numeric))){
							sum += parseFloat(numeric);
						}
					}
					var billTotal = displayMoneyWithSymbol(sum);

					document.getElementById(\"billTotal\").value = billTotal;
					return sum;
				}
				
				function sumCoins(){
					var coinElements = document.getElementsByClassName('tillSummable coin');
					var sum = 0;
					var numeric = '';
					for(var i = 0; i < coinElements.length; i++){
						numeric = (coinElements[i].value).replace(/\s/g,'');
						numeric = (numeric).replace(/^\D+/g,'');
						numeric = (numeric).replace(/\D/g,'.');
						if(!isNaN(parseFloat(numeric))){
							sum += parseFloat(numeric);
						}
					}
					document.getElementById(\"coinTotal\").value = displayMoneyWithSymbol(sum);
					return sum;
				}
				
				function sumAll(billSum, coinSum){
					var sum = billSum + coinSum;
					document.getElementById(\"totalTillInValue\").value = displayMoneyWithSymbol(sum);
				}
	
				function updateDenominationValue(inputElementID, multiplier, billOrCoin, itemNumber){
					var inputElement = document.getElementById(inputElementID);
					var inputElementValue = inputElement.value;
					var product = inputElementValue * multiplier;
					var prepend = '';
					if(billOrCoin == 'bill'){
						prepend = 'bills';
					}else{
						prepend = 'coins';
					}
					var itemTotalID = prepend + \"Total\" + itemNumber;
					document.getElementById(itemTotalID).value = displayMoneyWithSymbol(product);
					var billSum = sumBills();
					var coinSum = sumCoins();
					sumAll(billSum, coinSum);
				}
				
				function updateDenomInputValuesForSubmission(type,elements){
					for(var i = 0; i < elements.length; i++){
						var outputVal = document.getElementById(type+'Total'+i).value;
						document.getElementById(elements[i].id).value = outputVal;
						
					}
				}
				<!-- Show/Hide functionality -->
				function showTillScreen(){
					document.getElementById(\"quantityContainer\").style.display = 'inline-block';
					document.getElementById(\"quantityContainer\").style.visibility = 'visible';
					document.getElementById(\"tillSubmission\").style.display = 'inline-block';
					document.getElementById(\"tillSubmission\").style.visibility = 'visible';
					document.getElementById(\"reAssignSubmission\").style.display = 'block';
					document.getElementById(\"reAssignSubmission\").style.visibility = 'visible';
					hideTillList();
				}
				
				function hideTillScreen(){
					document.getElementById(\"quantityContainer\").style.display = 'none';
					document.getElementById(\"quantityContainer\").style.visibility = 'hidden';
					document.getElementById(\"tillSubmission\").style.display = 'none';
					document.getElementById(\"tillSubmission\").style.visibility = 'hidden';
					document.getElementById(\"reAssignSubmission\").style.display = 'none';
					document.getElementById(\"reAssignSubmission\").style.visibility = 'hidden';
					showTillList();
				}
				
				function hideTillList(){
					document.getElementById('tillListContainer').style.display = 'none';
					document.getElementById('tillListContainer').style.display = 'hidden';
				}
				
				function showTillList(){
					document.getElementById('tillListContainer').style.display = 'inline-block';
					document.getElementById('tillListContainer').style.display = 'visible';
					populateAllInputsWithPostedValues();
				}
				function populateAllInputsWithPostedValues(){
					var bills = document.getElementsByClassName('billInput');
					var coins = document.getElementsByClassName('coinInput');
					var associativeArgs = getPostArgumentsAssociativeArray();
					if(associativeArgs){
						for(var i = 0; i < bills.length; i++){
							var inputElement = 'bills' + i;
							var outputElement = 'billsTotal' + i;
							var postBillValue = associativeArgs[inputElement];
							if(postBillValue != undefined && postBillValue != 'undefined'){
								document.getElementById(inputElement).value = postBillValue;
								document.getElementById(outputElement).value = postBillValue;
							}
						}
						for(var i = 0; i < coins.length; i++){
							var inputElement = 'coins' + i;
							var outputElement = 'coinsTotal' + i;
							var postCoinValue = associativeArgs[inputElement];
							if(postCoinValue != undefined && postCoinValue != 'undefined'){
								document.getElementById(inputElement).value = postCoinValue;
								document.getElementById(outputElement).value = postCoinValue;
							}
						}
					}
				}
				function getPostArgumentsAssociativeArray(){
					var URL = window.location.search.substring(1);
					var args = URL.split('&')
					var associativeArgs = {};
					for(var i = 0; i < args.length; i++){
						var keyValPair = args[i].split('=')
						associativeArgs[keyValPair[0]] = keyValPair[1];
					}
					return associativeArgs;
				}
								
				function updateFormInputDenomsForSubmission(){
					var bills = document.getElementsByClassName('billInput');
					var coins = document.getElementsByClassName('coinInput');
					updateDenomInputValuesForSubmission('bills',bills);
					updateDenomInputValuesForSubmission('coins',coins);
				}
			
				function formatEmptyInputsForSubmission(){
					var billInputs = document.getElementsByClassName('billInput');
					var coinInputs = document.getElementsByClassName('coinInput');
					
					for(var i = 0; i < coinInputs.length; i++){
					
						if(coinInputs[i].value == ''){
							coinInputs[i].value = '0';
						}
					}
					for(var i = 0; i < billInputs.length; i++){
						if(billInputs[i].value == ''){
							billInputs[i].value = '0';
						}
					}
				}
				
				function changeDate(adjustBy, numDays){
						adjustBy = parseInt(adjustBy,10) + parseInt(numDays,10);
						if(adjustBy == 0){
							document.getElementById('isPreviousDay').value = '0';
						}else{
							document.getElementById('isPreviousDay').value = '1';					
						}
						document.getElementById('numberOfDays').value = numDays;
					    document.getElementById('adjust_day').value = adjustBy;
						document.getElementById('tillEntryForm').submit();
				}
				
				function performTillInputUpdate(field_id,fieldVal,denomType,index, enteredNumber){
					setTillInputFieldValue(field_id,enteredNumber);
					updateDenominationValue(field_id, fieldVal, denomType,index);
				}
				function setTillInputFieldValue(field_id, value) {
					document.getElementById(field_id).value = value;
				}
				
				function scrollToTop(){
					window.scrollTo(0,0);
				}
				
				
				function displayMoney(amt, decimal_char) {
					var decimal_places = parseInt(".$decimal_places.",10);
					var val = parseFloat(amt).toFixed(decimal_places);
					return val.replace('.',decimal_char);
					
                    }

			    function displayMoneyWithSymbol(amt) {
					var monetary_symbol = '".$monetary_symbol."';
					var left_or_right = '".$left_or_right."';
					var decimal_char = '".$decimal_char."';
					if(monetary_symbol == null){
						monetary_symbol = '';
					}

			        if (left_or_right == ''){
			        	left_or_right = 'left';
			        }
			        var rtn_str = displayMoney(amt, decimal_char) + '';
			        if (left_or_right == 'left'){
			            rtn_str = monetary_symbol + '' + rtn_str;
			        }
			        
			        if (left_or_right == 'right'){
			            rtn_str += '' + monetary_symbol;
			        }
			        
			        return rtn_str;
			    }

				function selectCashDrawer(drawer) {
					document.getElementById('cashDrawerId').value = drawer;
					if (drawer == 1) {
						document.getElementById('cashDrawerOneBtn').className = 'drawerBtnSelected';
						document.getElementById('cashDrawerTwoBtn').className = '';
					} else {
						document.getElementById('cashDrawerOneBtn').className = '';
						document.getElementById('cashDrawerTwoBtn').className = 'drawerBtnSelected';
					}
					var url = window.location.toString();
					if (url.indexOf('drawer') > -1) {
						url = url.substring(0, url.lastIndexOf(\"&\"));
					}
					window.location.href = url + '&drawer='+drawer;
				}

				function assignedDrawerToServer(server_id, server_name) {
					var server_name = unescape(server_name);
					server_name = server_name.replace('<', '&lt;');
					document.getElementById('cashDrawerAssignedServerName').innerHTML = server_name;
					document.getElementById('cashDrawerAssignedServerId').value = server_id;
					var managerReassign = document.getElementById('managerReassign');
					if(managerReassign != undefined) {
						document.getElementById('managerReassign').style.display = 'block';
						document.getElementById('managerReassign').style.visibility = 'visible';
						document.getElementById('managerAssign').style.display = 'none';
					}
					if (event != undefined) {
						event.preventDefault();
					}
				}

				var errorMsg;
				var error_msg = '$errorMsg';

				function validateDrawer() {
					errorMsg = error_msg
					if (errorMsg != '') {
						isTillIn = (isTillIn) ? true : false;
						window.location = '_DO:cmd=alert&title=ERROR&message='+errorMsg;
						switchSelectableTillMode();
						scrollToTop();
						hideTillScreen();
						formatEmptyInputsForSubmission();
						setTillButtonClassesForSubmissionReload();
						setIsTillIn();
						setTillInOutButtonClasses(isTillIn);
						document.getElementById('cashDrawerAssignedServerName').innerHTML = '';
						if (event != undefined) {
							event.preventDefault();
						}
						exit;
					}
				}

				function getActiveServerList() {
					window.location = '_DO:cmd=get_active_server_list&js=assignedDrawerToServer(\'SERVER_ID\', \'SERVER_NAME\');';
					if (event != undefined) {
						event.preventDefault();
					}
				}

				function getTilledInServerList(drawer) {
					var tobElement = document.getElementById('tillOutButton');
					if (tobElement.className != 'tillBtnSelected') {
						window.location = '_DO:cmd=get_active_server_list&drawer=' + drawer + '&js=assignedDrawerToServer(\'SERVER_ID\', \'SERVER_NAME\');';
						if (event != undefined) {
							event.preventDefault();
						}
					}
				}

			</script>";

	return $view;
}
?>
