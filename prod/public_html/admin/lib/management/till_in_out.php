<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 10/21/16
 * Time: 3:16 PM
 */

	function getTillCSS(){
?>
		<style type='text/css'>
			/*****************Universal Element Styles***************/
			body {
				background-color: #666675 !important;

				font-family: helvetica, sans-serif;
				font-size: 17px;
			}

			div {
				display: inline-block;
			}

			ul {
				list-style: none;
				margin: 0;
				padding: 0;
			}

			li {
				width: 80%;
				margin: 42px auto;

				word-spacing: 10px;
				text-align: right;
			}

			li div {
				display: inline-block;
				width: 200px;
				height: 25px;

				text-align: center;
				color: #333;

				border-bottom: 1px solid #ccc;
			}

			input[type='text']{
				width: 50px;
				height: 25px;
				background-color: #ddd;
				border-width: 0;
				outline: none;
				border-radius: 2px;
			}
			input[type='number']{
				width: 50px;
				height: 25px;
				background-color: #ddd;
				border-width: 0;
				outline: none;
				border-radius: 2px;
			}

			/*****************Summables*****************************/
			.tillSummable{}
			.tillSummable .bill{}
			.tillSummable .coin{}

			.tillListItem{
				text-align:left;
				margin-left: 20px;
			}

			span[id^="bills"], span[id^="coins"]{
				display: inline-block;
				width:50px;
				text-align: right;
			}

			.tillTotal{
				text-decoration: underline;
			}

			/*****************Container Block Styles****************/
			.container {
				position: relative;
			}

			/*top bar block*/
			#topbar {
				width: 90%;

				margin: 10px 5%;
			}

			/*navigation block*/
			#navigation {
				width: 80%;
				height: 100px;
				margin: 10px 10%;

				border-width: 0;
				outline: none;
				border-radius: 2px;
				box-shadow: 0 1px 4px rgba(0, 0, 0, .4);

				position: relative;

				background-color: #fafafa;
				color: #666675;
			}

			#tillListContainer{
				width: 71%;
				min-height: 46px;
				margin: 15px 15% 0px 15%;
				padding-bottom: 10px;
				display:inline-block;

				border-width: 0;
				outline: none;
				border-radius: 2px;
				box-shadow: 0 1px 4px rgba(0, 0, 0, .4);

				position: relative;

				background-color: #fafafa;
				color: #666675;

				text-align: center;
			}

			#tillOptionsContainer{
				width: 80%;
				min-height: 46px;
				margin: 15px 10%;

				border-width: 0;
				outline: none;
				border-radius: 2px;
				box-shadow: 0 1px 4px rgba(0, 0, 0, .4);

				position: relative;

				background-color: #fafafa;
				color: #666675;
			}
			/*date sub block*/
			#dateContainer {
				background-color: #aaa;
				width: 96%;
				height: 53px;

				display: inline-block;
				/*position: absolute;*/

				padding: 2px 2% 0 2%;
			}

			#totalsContainer{
				height:165px;
				width:100%;
				background-color: #fafafa;
			}
			/*quanity main block*/
			#quantityContainer {
				width: 100%;
				margin: 0px auto;

				border-width: 0;
				outline: none;
				border-radius: 2px;
				box-shadow: 0 1px 4px rgba(0, 0, 0, .4);

				display:inline-block;
				background-color: #fafafa;
				color: #666675;
			}
			/*bills & coins divider cols*/
			.col {
				width: 50%;
				display-inline-block;
			}
			/*bills modifier for above*/
			#bills {
				/*border-right: 1px solid #ddd;*/
				display:inline-block;
			}
			/*coins modifier for above*/
			#coins {
				float:right;
				/*border-left: .5px solid #ddd;*/

				display:inline-block;
			}

			/*total block*/
			#totalTillIn{
				width: 100%;
				height: 30px;

				padding: 10px 0;
				vertical-align: bottom;
				border-top: 1px solid #ddd;
				background-color: #eee;

				text-transform: uppercase;
				color: #333;
				font-weight: 100;
				text-align: center;
			}

			.denominationTotal:first-of-type + .denominationTotal{
				float:right;

			}

			.denominationTotal{
				width: 49.5%;
				height: 30px;
				padding: 10px 0;
				margin-bottom:10px;
				align:left;

				background-color: #eee;

				border-color: #ddd;
				border-style: solid;
				border-width: 1px 0 1px 0;

				text-transform: uppercase;
				color: #333;
				font-weight: 100;
				text-align: center;
			}

			/*total text sub block*/
			#totalContainer span {
				display: inline-block;
				width: 200px;
				height: 25px;

				text-align: center;
				color: #333;

				border-bottom: 1px solid #ccc;
			}


			/****************Button Styles***************/
			.btn {
				border-width: 0;
				outline: none;
				border-radius: 2px;
				box-shadow: 0 1px 4px rgba(0, 0, 0, .4);

				background-color: #fafafa;
				color: #666675;
			}
			#tillSubmission{
				width: 110px;
				height: 30px;

				display: inline-block;

				margin-bottom: 10px;
				margin-top: 10px;
				font-size: 15px;
				line-height: 27px;
				text-align: center;
				vertical-align:bottom;
				/*top:-40px;*/
				right:0%;
			}

			/*back button*/
			#backBtn {
				width: 110px;
				height: 40px;

				position: absolute;
				left:10px;

				text-align: center;
				line-height: 40px;

				cursor: pointer;
			}

			/*till in / out buttons*/
			.tillBtn {
				width: 49.5%;
				height: 45px;

				text-align: center;
				font-weight: bold;
				line-height: 47px;
			}

			#tillInButton{
				border-right: solid;
				border-right-width: 1px;
				border-right-color: #DDD;
			}

			/**Three states of till button activity: not selectable, selectable, and selected.*/
			.tillBtnSelectable {
				width: 49.5%;
				height: 45px;

				text-align: center;
				font-weight: bold;
				line-height: 47px;
				color: #777;
				font-weight: bold;
			}

			.tillBtnNotSelectable{
				width: 49.5%;
				height: 45px;

				text-align: center;
				font-weight: bold;
				line-height: 47px;
				color: #DDD;
				font-weight: bold;
			}
			.tillBtnSelected{
				width: 49.5%;
				height: 45px;

				text-align: center;
				font-weight: bold;
				line-height: 47px;
				color: #090;
				font-weight: bold;
			}

			/*previous day button*/
			#previousBtn {
				width: 110px;
				height: 30px;

				display: inline-block;
				position: absolute;

				left: 10px;
				top: 58px;
				font-size: 15px;
				line-height: 27px;
				text-align: center;
			}

			/*Next day button*/
			#nextBtn {
				width: 110px;
				height: 30px;

				display: inline-block;
				position: absolute;

				right: 10px;
				top: 58px;
				font-size: 15px;
				line-height: 27px;
				text-align: center;
			}

			/*bottom buttons*/
			#cancelBtn, #submitBtn {
				width: 110px;
				height: 40px;

				margin-top: 20px;

				text-align: center;
				line-height: 40px;
			}
			/*submit button modifier*/
			#submitBtn {
				float: right;
			}

			/*Button click styles*/
			#backBtn:active {
				background-color: #fcfcfc;
			}

			#tillBtnInactive {
				color: #ddd;
				font-weight: bold;
			}

			#tillBtnActive{
				color: #090;
				font-weight: bold;
			}

			.viewDailyTillBtn{
				width: 49.5%;
				height: 45px;

				text-align: center;
				font-weight: bold;
				line-height: 47px;

				border-right: solid;
				border-right-width: 1px;
				border-right-color: #DDD;
			}
			.printDailyTillBtn{
				width: 49.5%;
				height: 45px;

				text-align: center;
				font-weight: bold;
				line-height: 47px;
			}

			/*******************Text styles**************/
			#currentDate{
				color: #555;
			}
			#pageTitle {
				text-transform: uppercase;
				font-size: 20px;
				/*color: #fff;*/
				color: #000;
				font-weight: 100;
				text-align: center;
				line-height: 35px;

				width: 100%;
			}

			/*current date style*/
			.current {
				width: 100%%;
				line-height: 50px;
				text-align: center;
				font-size: 13px;
				font-weight: 100;
				color: #333;
			}

			/*denomination style*/
			.colHeader {
				text-transform: uppercase;
				color: #333;
				font-weight: 100;
				text-align: center;
				line-height: 38px;

				width: 100%;

				background-color: #aaa;
			}
			/*Till Line stuff*/
			.tillLineTimes{
				margin-top: 8px;
				margin-bottom: 15px;
				display:inline-block;
				text-align:right;
				margin-right: 5px;
				width:40%;

			}
			.tillLineNames{
				width:55%;
				margin-top: 8px;
				margin-bottom: 15px;
				display:inline-block;
				margin-left: 5px;

				text-align:left;
			}
			/*Till records line*/
			.tillLine{

				display:inline-block;
				position:relative;
				width:100%;
				text-align:center;
			}

			.tillLineButtons{
				display: inline-block;
			}

			.tillLineContainer{

				display:inline-block;
				position: relative;
				width:100%;
			}
			.tillListContent{
				margin-top: 10px;
				margin-bottom: 15px;
				width: 100%;
			}


			.tillLineViewSummary{
				display:inline-block;
				border-width: 0;
				outline: none;
				border-radius: 2px;
				box-shadow: 0 1px 4px rgba(0, 0, 0, .4);
				margin-left: 20px;
				margin-right: 10px;
				padding-left: 10px;
				padding-right: 10px;
				background-color: #fafafa;
				color: #666675;
				height: 24px;
			}

			.spaceBelowTill{
				height:10px;
				width: 100%;
				display:inline-block;
			}

			.tillLinePrintSummary{
				display:inline-block;
				border-width: 0;
				outline: none;
				border-radius: 2px;
				box-shadow: 0 1px 4px rgba(0, 0, 0, .4);
				margin-left: 10px;
				margin-right: 20px;
				padding-left: 10px;
				padding-right: 10px;
				background-color: #fafafa;
				color: #666675;
				height: 24px;
			}

            /* dual cash drawer */
            #assignDrawer{
                width: 100%;
                vertical-align: bottom;
                border-top: 1px solid #ddd;
                background-color: #eee;
                margin-top: 4px;
                padding-bottom: 10px;
                color: #333;
                font-weight: 100;
                text-align: center;
            }
            #reAssignSubmission{
                width: 110px;
                height: 30px;

                display: inline-block;

                margin-bottom: 10px;
                margin-top: 10px;
                font-size: 15px;
                line-height: 27px;
                text-align: center;
                vertical-align:bottom;
                /*top:-40px;*/
                right:0%;
            }
            .drawerBtnNotSelectable{
                width: 49.5%;
                height: 45px;

                text-align: center;
                font-weight: bold;
                line-height: 47px;
                color: #DDD;
                font-weight: bold;
            }
            .drawerBtnSelected{
                width: 49.5%;
                height: 45px;

                text-align: center;
                font-weight: bold;
                line-height: 47px;
                color: #090;
                font-weight: bold;
            }

            /*Drawer one button*/
            #cashDrawerOneBtn {
                width: 145px;
                height: 40px;

                display: inline-block;

                right: 10px;
                top: 58px;
                font-size: 15px;
                line-height: 27px;
                text-align: center;
                margin-left: -2%;
                font-weight: bold;
            }

            /*Drawer two button*/
            #cashDrawerTwoBtn {
                width: 145px;
                height: 40px;

                display: inline-block;

                right: 10px;
                top: 58px;
                font-size: 15px;
                line-height: 27px;
                text-align: center;
                margin-left: -2%;
                font-weight: bold;
            }

            /*navigation block*/
            #cashDrawerContainer {
                width: 300px;
                height: 50px;
                top: 20px;
                display: inline-block;
                /*position: absolute;*/

                padding: 2px 2% 0 2%;
            }
            .pull-right{ float : right;}
            .pull-left{ float : left; }
            .padding-20-40{ padding : 20px 40px;}
            .padding-top-20{ padding-top : 20px;}
            .padding-right-10{ padding-right : 20px;}

            .drawerBtnNotSelectable{
                width: 49.5%;
                height: 45px;

                text-align: center;
                font-weight: bold;
                line-height: 47px;
                color: #DDD;
                font-weight: bold;
            }
            .drawerBtnSelected{
                width: 49.5%;
                height: 45px;

                text-align: center;
                font-weight: bold;
                line-height: 47px;
                color: #090;
                font-weight: bold;
            }
            #cashDrawerAssigned {
                width: 80%
            }

            #serverAssigned {
                width: 60%
            }

            .cashDrawerinfo{
                width:30%;
                position:relative;
            }
		</style>
<?php
	}
	getTillCSS();
?>

