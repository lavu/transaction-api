<?php
	/*****************************************************************************
	 * A duplicate of the payments in vs labor report.
	 * Does a call to the payments_in_vs_labor report.
	 *****************************************************************************
	 * Notes to push live:
	 *     add the line $extra_reports[] = array("Cash Reconciliation","reports_cash_reconciliation"); to index.php
	 *     copy this file and "transaction_summary.php" from /dev/lib to /lib
	 *****************************************************************************/
	
	$s_dataname = $_GET['cc'];
	
	// <>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// index.php
	// <>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	ob_start();
	@require_once($_SERVER['DOCUMENT_ROOT'].'/cp/index.php');
	$s_trash = ob_get_contents();
	ob_end_clean();
	// <>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	// end of: index.php
	// <>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>~<>
	
	global $s_cant_display_error;
	$s_cant_display_error = '';
	$non_time_cards_display = $display;
	
	ob_start();
	require_once($_SERVER['DOCUMENT_ROOT'].'/cp/areas/reports/payments_in_vs_labor.php');
	$s_contents = ob_get_contents();
	ob_end_clean();
	
	echo admin_info('dataname');
	
	if ($s_cant_display_error == '') {
		$non_time_cards_display .= $s_contents;
	} else {
		$non_time_cards_display .= $s_cant_display_error;
	}
	$display = $non_time_cards_display;
