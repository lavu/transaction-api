<?php
function callSquareAPI($request)
{
	$protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
	$http_host = $_SERVER['HTTP_HOST'];
	$transParam = array();
	$squareAPI = $protocol.$http_host.'/components/payment_extensions/square/SquareAccess.php?';
	//$transParam['PHPSESSID'] = $request['PHPSESSID'];
	$transParam['dataname'] = $request['data_name'];
	//$transParam['device_time'] = $request['device_time'];
	//$transParam['app_name'] = $request['app_name'];
	//$transParam['loc_id'] = $request['loc_id'];
	//$transParam['app_version'] = $request['app_version'];
	$transParam['userid'] = $request['userid'];
	//$transParam['usejcinc'] = $request['usejcinc'];
	$transParam['transactionid'] = $request['transaction_id'];
	$transParam['mode'] = 'getDepositTransactionDetails';
	//$transParam['UUID'] = $request['UUID'];
	$transParam['cc'] = $request['cc'];
	//$transParam['UUID'] = $request['UUID'];
	$transParam['locationid'] = $request['locationid'];
	//$transParam['app_build'] = $request['app_build'];
	$transactionURL.=http_build_query($transParam);

	$endURL = $squareAPI.$transactionURL;
	$curlCall = curl_init();
	curl_setopt($curlCall, CURLOPT_URL,            $endURL);
	curl_setopt($curlCall, CURLOPT_CONNECTTIMEOUT, 0);
	curl_setopt($curlCall, CURLOPT_TIMEOUT,        200);
	curl_setopt($curlCall, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curlCall, CURLOPT_RETURNTRANSFER, true );
	session_write_close();
	$result = curl_exec($curlCall);
	if ($result === FALSE) {
		$error = array( "cURLError" => curl_error( $curlCall ) );
		$result = json_encode($error);
	}
	return $result;
}

function getTransactionDetails($request)
{
	for ($i = 0; $i < 3; $i++) {
		$transactionResponse = callSquareAPI($request);
		$transactionResponse = json_decode($transactionResponse, true);
		if ((isset($transactionResponse['error']) || empty($transactionResponse)) && $i < 3) {
			continue;
		}
		break;
	}

	$rf_id = $request['locationid'];
	$userid = $request['userid'];
	$txnid = $request['transaction_id'];
	$datetime = date('Y-m-d H:i:s');
	$return_val = 'success';
	$server_query = lavu_query("select CONCAT_WS(' ',f_name,l_name) as server_name from users where id='".$request['server_id']."'");
	$server_data = mysqli_fetch_assoc($server_query);
	$server_name = $server_data['server_name'];

	if(!isset($transactionResponse['error'])) {
		$orderID = '';
		if (isset($transactionResponse['transaction']['itemizations'][0]['notes'])) {
			$notes = explode(",", $transactionResponse['transaction']['itemizations'][0]['notes']);
			$orderArr = explode('#', $notes[0]);
			$orderID = trim($orderArr[1]);
		}
		$tenders = (isset($transactionResponse['transaction']['tender'])) ? $transactionResponse['transaction']['tender'] : $transactionResponse['transaction']['tenders'];
		$transactionCount = count($tenders);

		$recordID = $transactionResponse['transaction']['id'];
		$separator = '';
		$net_sales_money = (isset($transactionResponse['transaction']['net_sales_money'])) ? $transactionResponse['transaction']['net_sales_money']['amount'] / 100 : 0;
		$transResponse = '';
		foreach($tenders as $tender) {
			$card_brand = '';
			$last_4 = '';
			if (strpos($tender['type'], "CARD") || $tender['type'] == "CARD") {
				$card_brand = (isset($tender['card_brand'])) ? $tender['card_brand'] : $tender['card_details']['card']['card_brand'];
				$last_4 = (isset($tender['pan_suffix'])) ? $tender['pan_suffix'] : $tender['card_details']['card']['last_4'];
			}
			$totalMoney = (isset($tender['total_money']['amount'])) ? $tender['total_money']['amount'] : $tender['amount_money']['amount'];
			$split_tender_id = $tender['id'];
			$process_data = '';
			$tipAmount = 0;
			if(isset($tender['tip_money']['amount']) && $tender['tip_money']['amount']>0)
			{
				$totalMoney = $totalMoney - $tender['tip_money']['amount'];
				$tipAmount = $tender['tip_money']['amount']/100;
			}
			$amount = ($transactionCount == 1 && $net_sales_money > 0 ) ? $net_sales_money : $totalMoney / 100;
				
				
			$cc_data = array('order_id' => $request['order_id'], 'amount'=> $amount, 'refunded' =>0, 'loc_id'=>$request['loc_id'], 'datetime' => $datetime, 'pay_type' => $request['mode'],'total_collected'=>$amount,'transtype'=>$request['type'],'action'=>$request['type'],'server_time'=>$datetime,'transaction_id'=>$txnid,'auth_code'=>$txnid,'card_type'=>$card_brand,'card_desc'=>$last_4,'gateway'=>'square','register'=>$request['register'],'got_response'=>'1','ref_data'=>$rf_id,'register_name'=>$request['register_name'],'pay_type_id'=>'2','server_name'=>$server_name,'process_data'=>$process_data,'preauth_id'=>$txnid,'reader'=>'square','record_no'=>$recordID,'check'=>'1','split_tender_id'=>$split_tender_id);
				
			$insertcc = lavu_query("INSERT INTO `cc_transactions` (`order_id`,`amount`,`refunded`,`loc_id`,`datetime`,`pay_type`,`total_collected`,`transtype`,`action`,`server_time`,`transaction_id`,`auth_code`,`card_type`,`card_desc`,`gateway`,`register`,`got_response`,`ref_data`,`process_data`,`register_name`,`pay_type_id`,`server_name`,`preauth_id`,`reader`,`record_no`,`check`,`split_tender_id`) values('[order_id]','[amount]','[refunded]','[loc_id]','[datetime]','[pay_type]','[total_collected]','[transtype]','[action]','[server_time]','[transaction_id]','[auth_code]','[card_type]','[card_desc]','[gateway]','[register]','[got_response]','[ref_data]','[process_data]','[register_name]','[pay_type_id]','[server_name]','[preauth_id]','[reader]','[record_no]','[check]','[split_tender_id]')",$cc_data);
				
			lavu_query("update deposit_information set pay_status=1 where transaction_id='".$request['order_id']."'");
				
		}
	} else {
		$cc_data = array('order_id' => $request['order_id'], 'refunded' =>0, 'loc_id'=>$request['loc_id'], 'datetime' => $datetime, 'pay_type' => $request['mode'],'transtype'=>$request['type'],'action'=>$request['type'],'server_time'=>$datetime,'transaction_id'=>$request['transaction_id'],'gateway'=>'square','register'=>$request['register'],'got_response'=>'1','register_name'=>$request['register_name'],'pay_type_id'=>'2','server_name'=>$server_name,'reader'=>'square','check'=>'1');
			
		$insertcc = lavu_query("INSERT INTO `cc_transactions` (`order_id`,`refunded`,`loc_id`,`datetime`,`pay_type`,`transtype`,`action`,`server_time`,`transaction_id`,`gateway`,`register`,`got_response`,`register_name`,`server_name`,`reader`,`check`) values('[order_id]','[refunded]','[loc_id]','[datetime]','[pay_type]','[transtype]','[action]','[server_time]','[transaction_id]','[gateway]','[register]','[got_response]','[register_name]','[server_name]','[reader]','[check]')",$cc_data);
		if ($txnid != '') {
			$reqinfo = array('data_name'=>$request['data_name'], 'gateway'=>'square', 'transaction_id'=>$txnid, 'location_id'=>$rf_id, 'user_id'=>$userid, 'status'=>'pending', 'created_date'=>$datetime, 'type' => 'deposit');
				
			if ($reqinfo['transaction_id'] != '' && $reqinfo['location_id'] != '' && $reqinfo['data_name'] != '') {
				mlavu_query("INSERT INTO `gateway_sync_request` (`data_name`,`gateway`,`transaction_id`,`location_id`,`user_id`,`status`,`created_date`,`transaction_type`) VALUES ('[data_name]','[gateway]','[transaction_id]','[location_id]','[user_id]','[status]','[created_date]')",$reqinfo);
					
			}
				
		} else {
			$return_val = 'failure';
		}
	}
	return $return_val;
}