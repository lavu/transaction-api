<?php

	ini_set("max_execution_time",600);
	ini_set("ignore_user_abort",1);
	
	if (isset($_POST['submitted']) && !empty($_POST['t_ids'])) {
		
		require_once(dirname(__FILE__)."/../gateway_functions.php");
		require_once(dirname(__FILE__)."/../mpshc_functions.php");
	
		$integration_info = get_integration_from_location($_REQUEST['loc_id'], "poslavu_".$data_name."_db");

		if (file_exists(dirname(__FILE__)."/../gateway_lib/".strtolower($integration_info['gateway'])."_func.php")) require_once(dirname(__FILE__)."/../gateway_lib/".strtolower($integration_info['gateway'])."_func.php");						
	
		$server_name = "";
		$get_server_name = lavu_query("SELECT `f_name`, `l_name` FROM `users` WHERE `id` = '[1]'", $server_id);
		if (mysqli_num_rows($get_server_name) > 0) {
			$server_info = mysqli_fetch_assoc($get_server_name);
			$server_name = $server_info['f_name'];
			if (!empty($server_name)) $server_name .= " ";
			$server_name .= $server_info['l_name'];
		}
	
		$process_info = array();
		$process_info['mag_data'] = "";
		$process_info['reader'] = "";
		$process_info['card_number'] = "";
		$process_info['card_cvn'] = "";
		$process_info['name_on_card'] = "";
		$process_info['exp_month'] = "";
		$process_info['exp_year'] = "";
		$process_info['card_amount'] = "";
		$process_info['company_id'] = $company_info['id'];
		$process_info['loc_id'] = $loc_id;
		$process_info['ext_data'] = "";
		$process_info['data_name'] = $data_name;
		$process_info['register'] = $register;
		$process_info['register_name'] = $register_name;
		$process_info['device_udid'] = $device_MAC;
		$process_info['server_id'] = $server_id;
		$process_info['server_name'] = $server_name;
		$process_info['device_time'] = $device_time;
		$process_info['set_pay_type'] = "Card";
		$process_info['set_pay_type_id'] = "2";
		$process_info['for_deposit'] = "";
		$process_info['is_deposit'] = "";
		$process_info['username'] = $integration_info['integration1'];
		$process_info['password'] = $integration_info['integration2'];
		$process_info['integration3'] = $integration_info['integration3'];
		$process_info['integration4'] = $integration_info['integration4'];
		$process_info['integration5'] = $integration_info['integration5'];

		$resp = "";
		$error_encountered = false;

		$get_transaction_info = lavu_query("SELECT * FROM `cc_transactions` WHERE `id` IN (".$_POST['t_ids'].") ORDER BY `datetime` ASC");
		error_log("cc_auths: ".mysqli_num_rows($get_transaction_info)." transactions");
		while ($info = mysqli_fetch_assoc($get_transaction_info)) {
			
			$error_code = 0;
			$amt = process_capture_or_tip($location_info, $integration_info, $process_info, $info, number_format(0, $location_info['disable_decimal'], ".", ","), "", false);
			
			if ($error_code == 1) {
				$resp .= "<tr><td align='center'>" . speak('Error') . ': '. speak('Failed to load order ID') . " $error_info...</td></tr>";
				$error_encountered = true;
			} else if ($error_code == 2) {
				$resp .= "<tr><td align='center'>" . speak('Error') . ': '. speak('Failed to load transactions for order ID') . " $error_info...</td></tr>";
				$error_encountered = true;
			} else if ($error_code == 3) {
				$resp .= "<tr><td align='center'>" . speak('Error') . ': '. speak('Unrecognized payment gateway') . "...</td></tr>";
				$error_encountered = true;
			} else if ($error_code == 4) {
				$resp .= "<tr><td align='center'>" . speak('Gateway error') . ": $error_info...</td></tr>";
				$error_encountered = true;
			} else {
				$touch_order = lavu_query("UPDATE `orders` SET `lastmod` = now() WHERE `order_id` = '[1]'", $info['order_id']);
			}
		}
		
		$display .= "<table>".$resp."</table>";
		$resp = "";
	}
	
	$past_seconds = 2592000;
	//if ($data_name == "south__vine_pu") $past_seconds = 5184000;

	$check_for_open_auths = lavu_query("SELECT `cc_transactions`.`auth` AS `auth`, `cc_transactions`.`id` AS `id`, `cc_transactions`.`order_id` AS `order_id`, `cc_transactions`.`check` AS `check`, `cc_transactions`.`server_name` AS `server_name`, `cc_transactions`.`amount` AS `amount`, `cc_transactions`.`card_type` AS `card_type`, `cc_transactions`.`card_desc` AS `card_desc`, `cc_transactions`.`auth_code` AS `auth_code`, `cc_transactions`.`datetime` AS `datetime` FROM `cc_transactions` LEFT JOIN `orders` ON (`orders`.`order_id` = `cc_transactions`.`order_id` AND `orders`.`location_id` = `cc_transactions`.`loc_id`) WHERE `cc_transactions`.`loc_id` = '[1]' AND `cc_transactions`.`pay_type_id` = '2' AND `cc_transactions`.`auth` != '0' AND `cc_transactions`.`got_response` = '1' AND `cc_transactions`.`voided` = '0' AND `cc_transactions`.`datetime` > '[2]' AND `orders`.`void` = '0' ORDER BY `cc_transactions`.`datetime` ASC", $loc_id, date("Y-m-d H:i:s", (time() - $past_seconds)));
	if (mysqli_num_rows($check_for_open_auths) > 0) {
		$t_ids = array();
		$top_row_code = " align='center' style='font-size:14px; color:#444444; border-bottom:1px solid #000066'";
		$display .= "<table cellspacing='0' cellpadding='4'>";
		$display .= "<tr><td colspan='9' align='center'><b><span style='font-size:18px'>" . speak('Uncaptured Transactions') . "</span></b><br><br></td></tr>";
		$display .= "<tr><td colspan='9' align='center' style='padding:3px 3px 6px 3px'>";
		$display .= "<table width='550px'><tr><td align='left'>" . speak('Please review this list of transactions and be sure that none of these transactions have been previously captured from the backend of the remote server.') . "</td></tr></table>";
		$display .= "</td></tr>";
		$display .= "<tr>";
		$display .= "<td".$top_row_code."></td>";
		//$display .= "<td".$top_row_code."><b>ID</b></td>";
		$display .= "<td".$top_row_code."><b>" . speak('Order ID') . "</b></td>";
		$display .= "<td".$top_row_code."><b>" . speak('Check') . "</b></td>";
		$display .= "<td".$top_row_code."><b>" . speak('Server') . "</b></td>";
		$display .= "<td".$top_row_code."><b>" . speak('Amount') . "</b></td>";
		$display .= "<td".$top_row_code."><b>" . speak('Card Type') . "</b></td>";
		$display .= "<td".$top_row_code."><b>" . speak('Last Four') . "</b></td>";
		$display .= "<td".$top_row_code."><b>" . speak('Auth Code') . "</b></td>";
		//$display .= "<td".$top_row_code."><b>" . speak('Auth Type') . "</b></td>";
		$display .= "<td".$top_row_code."><b>" . speak('Date/Time') . "</b></td>";
		$display .= "</tr>";
		$auth_for_tab_message = "";
		while ($info = mysqli_fetch_assoc($check_for_open_auths)) {
		
			//if ($data_name == "minibar") echo print_r($info, true)."<br>";
		
			$auth_for_tab = "";
			if ($info['auth'] == "2") {
				$auth_for_tab_message = "[--CR--]" . speak('Please note that AuthForTab transactions must be captured from the Check Out screen.');
				$auth_for_tab = "<font size='-1' color='#FF3300'>" . speak('AuthForTab') . "</font>";
			} else $t_ids[] = $info['id'];
			
			$display .= "<tr>";
			$display .= "<td>".$auth_for_tab."</td>";
			//$display .= "<td align='center' style='font-size:16px;'>".$info['id']."</td>";
			$display .= "<td align='center' style='font-size:16px;'>".$info['order_id']."</td>";
			$display .= "<td align='center' style='font-size:16px;'>".$info['check']."</td>";
			$display .= "<td align='right' style='font-size:16px;'>".$info['server_name']."</td>";
			$display .= "<td align='right' style='font-size:16px;'>".$info['amount']."</td>";
			$display .= "<td align='center' style='font-size:16px;'>".$info['card_type']."</td>";
			$display .= "<td align='center' style='font-size:16px;'>".$info['card_desc']."</td>";
			$display .= "<td align='center' style='font-size:16px;'>".$info['auth_code']."</td>";
			//$display .= "<td align='center' style='font-size:16px;'>".(($info['auth']=="2")?"Tab PreAuth":"Standard")."</td>";
			$display .= "<td align='center' style='font-size:16px;'>".$info['datetime']."</td>";
			$display .= "</tr>";
		}
		$display .= "<tr><td colspan='9' style='border-top:1px solid #000066'>&nbsp;</td></tr>";
		$display .= "<tr><td colspan='9' align='center'>";
		$display .= "<script language='javascript'>function submit_auths_form() { document.getElementById(\"auths_form\").submit(); }</script>";
		$display .= "<form id='auths_form' action='?mode=cc_auths&".$fwd_vars."' method='POST'>";
		$display .= "<input type='hidden' name='t_ids' value='".implode(",", $t_ids)."'>";
		$display .= "<input type='hidden' name='submitted' value='1'>";
		if (count($t_ids) > 0) $display .= "<input type='button' style='font-size:18px' value='Capture All' onclick='".use_available_confirm("submit_auths_form()", "Capture Authorizations", "Please be sure that no tip adjustments need to be made for any of these transactions.".$auth_for_tab_message."[--CR--]Continue with capture?")."'>";
		else $display .= speak('AuthForTab transactions must be captured from the Check Out screen.');
		$display .= "</form>";
		$display .= "</td></tr>";
		$display .= "</table>";
	
	} else {
	
		$display .= "<table width='800px'>";
		$display .= "<tr><td align='center'><br><b>" . speak('No Uncaptured Transactions') . "</b><br><br></td></tr>";
		$display .= "<tr><td align='center'>" . speak("All credit card authorizations on record are currently marked as captured.") . "</td></tr>";
		$display .= "</table>";
	}
?>