<?php
require_once (__DIR__ . "/../deposit/Receipt.php");
require_once (__DIR__ . "/../deposit/ReceiptTemplate.php");
require_once (__DIR__ . "/../deposit/PaymentGateway.php");
require_once (__DIR__ . "/../deposit/PaymentGatewayModel.php");
$timezone = locationSetting("timezone");
$datetime = date("Y-m-d H:i:s");
if (! empty($timezone)) {
    $datetime = DateTimeForTimeZone($timezone);
}
if (isset($_REQUEST['payment_status']) && $_REQUEST['payment_status'] == '1') {
    if ( isset($_REQUEST['gateway']) && strtolower($_REQUEST['gateway']) == "square") {
        require_once ("deposit_square_func.php");
        $get_details = getTransactionDetails($_REQUEST);
    }
    else if ($_REQUEST['process_data'] == 'native_extension:paypal') {
    	require_once ('/home/poslavu/public_html/admin/lib/jcvs/jc_func8.php');
    	$request = $_REQUEST;
    	updateCcTransactionPaypalEmv($request, 'deposit');
    	lavu_query("update deposit_information set pay_status=1 where transaction_id='" . $_REQUEST['order_id'] . "'");
    	$receipt = getDepositReceipt($_REQUEST['order_id']);
    	echo $receipt;
    	exit;
    }
    else {
        lavu_query("update deposit_information set pay_status=1 where transaction_id='" . $_REQUEST['order_id'] . "'");
        lavu_query("update cc_transactions set transtype='deposit',action='deposit',pay_type='$mode'  where order_id='" . $_REQUEST['order_id'] . "'");
    }
}
if ( isset($_REQUEST['refund_status']) && $_REQUEST['refund_status'] == "1") {
	if (strtolower($_REQUEST['gateway'])=='square') {
		$responseStr=$_REQUEST['responseStr'];
		$responseArr=explode("|",$responseStr);
		$cc_data = array (
            'transaction_id' => $responseArr['1'],
            'card_desc' => $responseArr['2'],
            'auth_code' => $responseArr['4'],
            'card_type' => $responseArr['5'],
            'record_no' => $responseArr['6'],
            'amount' => $responseArr['7'],
		    'total_collected' => $responseArr['7'],
            'order_id' => $responseArr['8'],
		    'ref_data' => $responseArr['9'],
		    'process_data' => $responseArr['10'],
		    'first_four' => $responseArr['13'],
		    'last_mod_ts' => $responseArr['14'],
		    'split_tender_id' => $responseArr['16'],
		    'rf_id' => $responseArr['17'],
		    'loc_id' => $responseArr['17'],
            'refunded' => 1,
            'datetime' => $datetime,
            'pay_type' => $mode,
            'transtype' => 'deposit_refund',
            'action' => 'deposit_refund',
            'server_time' => $datetime,
            'server_name' => $server_name
        );
		$insertcc = lavu_query("INSERT INTO `cc_transactions` (`order_id`,`amount`,`refunded`,`loc_id`,`datetime`,`pay_type`,`total_collected`,`transtype`,`action`,`server_time`,`server_name`,`transaction_id`,`card_desc`,`auth_code`,`card_type`,`record_no`,`ref_data`,`process_data`,`first_four`,`last_mod_ts`,`split_tender_id`,`rf_id`) values('[order_id]','[amount]','[refunded]','[loc_id]','[datetime]','[pay_type]','[total_collected]','[transtype]','[action]','[server_time]','[server_name]','[transaction_id]','[card_desc]','[auth_code]','[card_type]','[record_no]','[ref_data]','[process_data]','[first_four]','[last_mod_ts]','[split_tender_id]','[rf_id]')",$cc_data);
		lavu_query("update deposit_information set pay_status=1,transaction_status=1,`pay_type`='refund' where transaction_id='".$_REQUEST['order_id']."'");
	}
	else if ($_REQUEST['process_data'] == 'native_extension:paypal') {
		require_once ('/home/poslavu/public_html/admin/lib/jcvs/jc_func8.php');
		$request = $_REQUEST;
		updateCcTransactionPaypalEmv($request, 'deposit_refund');
		lavu_query("update deposit_information set transaction_status=1,`refund_date`='$datetime',`pay_type`='refund' where transaction_id='".$_REQUEST['order_id']."'");
		$receipt = getDepositReceipt($_REQUEST['order_id']);
		echo $receipt;
		exit;
	}
	else {
		if($_REQUEST['transaction_id']!='') {
            lavu_query("update deposit_information set transaction_status=1,`refund_date`='$datetime',`pay_type`='refund' where transaction_id='".$_REQUEST['order_id']."'");
            lavu_query("update cc_transactions set transtype='deposit_refund',action='deposit_refund',pay_type='$mode'  where order_id='".$_REQUEST['order_id']."' and transaction_id='".$_REQUEST['transaction_id']."'");
		}
	}
}
if (isset($_REQUEST['refund_status']) && $_REQUEST['refund_status'] == '0') {
    lavu_query("update deposit_information set pay_type='deposit', refund_amount='NULL' where transaction_id='" . $_REQUEST['order_id'] . "'");
}

$display .= '<style>
body {
	font-family: arial, sans-serif;
}
/**********************Text input and text area styles********************/
/*All text inputs*/
input[type="text"], input[type="tel"] {
	border-radius: 2px;
	-webkit-border-radius: 2px;
	border: 1px solid #bbb;

	font-family: sans-serif, verdana;
	color:#333;
	margin: 0 2px;
	padding-left: 15px;

	cursor: pointer;

	background-color: #fafafa;
}
/*Hover states for above*/
input[type="text"]:hover, input[type="tel"]:hover {
	 box-shadow: 0px 1px 3px #999;
}
/*Down states for above*/
input[type="text"]:active, input[type="tel"]:active {
	-moz-box-shadow: inset 0 0 10px #999;
	-webkit-box-shadow: inset 0 0 10px #999;
	box-shadow: inset 0 0 10px #999;
}
.btn_light_super_long {
	color:#7D7D7D;
	font-family:Arial, Helvetica, sans-serif;
	font-size:13px;
	width:200px;
	height:25px;
	background:url(<?php echo "' . $imagePath . '""/btn_wow_250x37.png" ?>);
	border:hidden;
	padding-top:2px;
	padding-left:8px;
}
/*****************Large & medium action button styles**************/
/*All Save buttons*/
input[type="submit"], button[type="submit"] {
	border-radius: 2px;
	-webkit-border-radius: 2px;
	border:0 none !important;
    width:10em;
	height:3em;
    font-family: sans-serif, verdana;
	font-weight: 600;
	font-size: 1em;
	color: #fff;
	text-transform: uppercase;
	text-shadow: 0px 1px 3px #999;
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+51,99bc08+54,aecd37+100 */
	background: #aecd37; /* Old browsers */
	background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 51%, #99bc08 54%, #aecd37 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#aecd37", endColorstr="#aecd37",GradientType=0 ); /* IE6-9 */
    transition:all 0.3s;
    cursor: pointer;
}
/*Hover states for all large & medium buttons*/
input[type="submit"]:hover, button[type="submit"]:hover  {
	cursor: pointer;
	/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+92,99bc08+94,aecd37+100 */
	background: #aecd37; /* Old browsers */
	background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 92%, #99bc08 94%, #aecd37 100%); /* FF3.6-15 */
	background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 92%,#99bc08 94%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
	background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 92%,#99bc08 94%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#aecd37", endColorstr="#aecd37",GradientType=0 ); /* IE6-9 */
	box-shadow: 0px 1px 3px #999;
	text-shadow: 0px 1px 3px #666;
}
/*Down states for above*/
input[type="submit"]:active, button[type="submit"]:active  {
	-moz-box-shadow: inset 0 0 10px #999;
	-webkit-box-shadow: inset 0 0 10px #999;
	box-shadow: inset 0 0 10px #999;
}
.btn_deposit{
	-webkit-border-radius: 2px;
	border: 0 none !important;
	width: 10em;
	height: 3em;
	font-family: sans-serif, verdana;
	font-weight: 600;
	font-size: 1em;
	color: #fff;
	text-transform: uppercase;
	text-shadow: 0px 1px 3px #999;
	background: #aecd37;
	background: -moz-linear-gradient(top, #aecd37 0%, #99bc08 51%, #99bc08 54%, #aecd37 100%);
	background: -webkit-linear-gradient(top, #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%);
	background: linear-gradient(to bottom, #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#aecd37", endColorstr="#aecd37",GradientType=0 );
	transition: all 0.3s;
	cursor: pointer;
}
.required{
	color:#ff0000;
}
.checkouttd{
    text-align: left;
	width: 230px;
}
#country-list{float:left;list-style:none;margin-top:-3px;padding:0;width:190px;position: absolute;}
#country-list li{padding: 10px; background: #f0f0f0; border-bottom: #bbb9b9 1px solid; width:96%;}
#country-list li:hover{background:#ece3d2;cursor: pointer;}
#search-box{padding: 10px;border: #a8d4b1 1px solid;border-radius:4px;}
.req{color: red;font-weight: bold;}
</style>
<script>
	function validateSearch()
	{
        if (document.getElementById("search").value == "")
    	{
    		document.getElementById("message_search").innerHTML = "Please enter a search term";
    		return false;
    	}
    	else
    	{
    		document.getElementById("search_form").submit();
    	}
    }
    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    function validateAddNew(type) {
		
    	if (document.getElementById("user_fname").value == "") {
    		document.getElementById("message_changebank").innerHTML = "Please enter first name";
    		return false;
    	}
    	else if (document.getElementById("user_lname").value == "") {
    		document.getElementById("message_changebank").innerHTML = "Please enter last name";
    		return false;
    	}
    	else if (document.getElementById("user_email").value == "" || !validateEmail(document.getElementById("user_email").value)) {
    		document.getElementById("message_changebank").innerHTML = "Please enter a valid Email";
    		return false;
    	}else {
			checkemailExist(document.getElementById("user_email").value, function(status){
				if ( status.trim() == \'failure\' && type == \'add\') {
		    		document.getElementById("message_changebank").innerHTML = "This email id is already registerd";
		    		return false;
		    	}	
		    	else if (document.getElementById("deposit_amount_input").value == ""  || document.getElementById("deposit_amount_input").value<=0) {
		    		document.getElementById("message_changebank").innerHTML = "Please enter valid amount";
		    		return false;
		    	}
		    	else {
		    		document.getElementById("customerDepositForm").submit();
		    	}
			});
		}
    }
			
    function validateRefund() {
        if (parseFloat(document.getElementById("refund_amount").value) > parseFloat(document.getElementById("old_amount").value)) {
    		document.getElementById("message_refund").innerHTML = "Refund amount should not be greater<br> than the amount initially deposited";
    		return false;
    	}
        else {
    		document.getElementById("refundform").submit();
    	}
    }
</script>';
$mode = strtolower($_REQUEST['mode']);
$a_fwd_vars = array(
    "mode" => $mode,
    "cc" => $_REQUEST['cc'],
    "loc_id" => $_REQUEST['loc_id'],
    "server_id" => $_REQUEST['server_id'],
    "username" => $_REQUEST['username'],
    "register" => $_REQUEST['register'],
    "device_time" => $_REQUEST['device_time'],
    "app_version" => $_REQUEST['app_version'],
    "lavu_lite" => $_REQUEST['lavu_lite'],
    "native_bg" => $_REQUEST['native_bg'],
    "type" => $_REQUEST['type'],
    "data_name" => $_REQUEST['data_name']
);
$fwd_vars = "";
$display_view = '1';
$searchstring == '';
foreach ($a_fwd_vars as $key => $val) {
    if (! empty($fwd_vars)) {
        $fwd_vars .= "&";
    }
    $fwd_vars .= $key . "=" . urlencode($val);
}
foreach ($a_fwd_vars as $key => $val) {
    $hidden_field .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />';
}
$server_query = lavu_query("select CONCAT_WS(' ',f_name,l_name) as server_name from users where id='" . $_REQUEST['server_id'] . "'");
$server_data = mysqli_fetch_assoc($server_query);
$server_name = $server_data['server_name'];
if (isset($_POST['search']) && $_POST['search'] != '') {
    $type = $_POST['type'];
    if (isset($_REQUEST['hidsearch']) && $_REQUEST['hidsearch'] != '') {
        if ($type == 'deposit') {
            $search_query = lavu_query("SELECT id as customer_id,first_name,last_name,email  FROM deposit_customer_info where  id = '" . $_REQUEST['hidsearch'] . "'");
        } else {
            $search_query = lavu_query("SELECT dci.id as customer_id,dci.first_name,dci.last_name,di.id,di.customer_id,di.transaction_id,di.amount,di.created_date as date,di.reason as reason  FROM deposit_information as di left join `deposit_customer_info` as dci  on dci.id=di.customer_id where `pay_type` = 'deposit' and  `deposit_type`='$mode' and  `transaction_status` = 0 and `pay_status`=1 and dci.id = '" . $_REQUEST['hidsearch'] . "'  ORDER BY di.id DESC");
        }
    } else {
        $searchstring = trim($_POST['search']);
        $searchstring = explode(' ', $searchstring);
        $like_query = '';
        $counterStr = count($searchstring);
        if ($counterStr > 0) {
            for ($i = 0; $i < $counterStr; $i ++) {
                $keyword = trim($searchstring[$i]);
                if ($keyword == "") {
                    continue;
                }
                if ($i > 0) {
                    $like_query .= ' OR ';
                }
                $like_query .= " first_name like '$searchstring[$i]%' or last_name like '$searchstring[$i]%' ";
            }
        }
        if ($type == 'deposit') {
            $search_query = lavu_query("SELECT id as customer_id,first_name,last_name,email  FROM deposit_customer_info where  $like_query");
        } else {
            $search_query = lavu_query("SELECT dci.id as customer_id,dci.first_name,dci.last_name,di.id,di.customer_id,di.transaction_id,di.amount,di.created_date as date,di.reason as reason  FROM deposit_information as di left join `deposit_customer_info` as dci  on dci.id=di.customer_id where ( " . $like_query . " ) and `pay_type` = 'deposit' and  `deposit_type`='$mode' and  `transaction_status` = 0 and `pay_status`=1  ORDER BY di.id DESC");
        }
    }
}
if (isset($_REQUEST['cust_id']) && $_REQUEST['cust_id'] != '' && $_REQUEST['submode'] == '' && $_POST['search'] == '') {
    $searchstring_id = $_REQUEST['cust_id'];
    $type = $_REQUEST['type'];
    $cond = '';
    if ($type == 'refund') {
        $cond = "and  `deposit_type`='$mode' ";
    }
    $search_query = lavu_query("SELECT dci.id as customer_id,dci.first_name,dci.last_name,di.id,di.customer_id,di.transaction_id,di.amount,di.created_date as date,di.reason as reason FROM deposit_information as di left join `deposit_customer_info` as dci  on dci.id=di.customer_id where  dci.id='$searchstring_id' and `pay_type` = 'deposit' $cond and `transaction_status` = 0 and `pay_status`=1 ORDER BY di.id DESC");
}
if (isset($_POST['deposit_amount_input']) && $_POST['deposit_amount_input'] != '') {
    $email = strtolower($_POST['user_email']);
    $pay_type = $_REQUEST['type'];
    $check_user = lavu_query("select `id` from deposit_customer_info where email = '$email'");
    if (mysqli_num_rows($check_user) == 0) {
        $status = ($mode === 'cash') ? 1 : 0;

        $data = array(
            'first_name' => trim($_POST['user_fname']),
            'last_name' => trim($_POST['user_lname']),
            'email' => $email,
            'phone' => $_POST['phone'],
            'created_date' => $datetime,
            'status' => $status
        );
        $insertDiposit = lavu_query("INSERT INTO `deposit_customer_info` (`first_name`,`last_name`,`email`,`phone`,`created_date`,`status`) values('[first_name]','[last_name]','[email]','[phone]','[created_date]','[status]')", $data);

        $rdata = lavu_insert_id();
    } else {
        $user_info = mysqli_fetch_assoc($check_user);
        $rdata = $user_info['id'];
    }
    $query = lavu_query("SELECT MAX(CONVERT(SUBSTRING_INDEX(transaction_id, '5000-', -1),UNSIGNED INTEGER)) AS `transaction_id` FROM `deposit_information` where `transaction_id` like '5000-%' ");
    if (mysqli_num_rows($query) > 0) {
        $info = mysqli_fetch_assoc($query);
        $transaction_id = $info['transaction_id'] + 1;
        $transaction_id = "5000-" . $transaction_id;
    } else {
        $transaction_id = "5000-1";
    }
    if ($mode === 'cash') {
        $amount_data = array(
            'customer_id' => $rdata,
            'transaction_id' => $transaction_id,
            'reason' => $_POST['deposit_reason'],
            'amount' => unFormatCurrencyForInsertData($_POST['deposit_amount_input']),
            'created_date' => $datetime,
            'deposit_type' => $mode,
            'pay_type' => $pay_type,
            'pay_status' => 1
        );
        $insertamount = lavu_query("INSERT INTO `deposit_information` (`customer_id`,`transaction_id`,`reason`,`amount`,`created_date`,`deposit_type`,`pay_type`,`pay_status`) values('[customer_id]','[transaction_id]','[reason]','[amount]','[created_date]','[deposit_type]','[pay_type]','[pay_status]')", $amount_data);
        $cc_data = array(
            'order_id' => $transaction_id,
            'amount' => unFormatCurrencyForInsertData($_POST['deposit_amount_input']),
            'refunded' => 0,
            'loc_id' => $_REQUEST['loc_id'],
            'datetime' => $datetime,
            'pay_type' => $mode,
            'total_collected' => unFormatCurrencyForInsertData($_POST['deposit_amount_input']),
            'transtype' => 'deposit',
            'action' => 'deposit',
            'server_time' => $datetime,
            'server_name' => $server_name,
            'server_id'       => $_REQUEST['server_id'],
            'register'        => $_REQUEST['register'],
            'check' => '1'
        );
        $cc_data['register_name'] = "";
        if (isset($_REQUEST['register_name']) && trim($_REQUEST['register_name'])!="") {
            $cc_data['register_name'] = trim($_REQUEST['register_name']);
            $cc_data['register_name'] = addslashes($cc_data['register_name']);
        }
        else{
            $resultData = getRegisterName( $_REQUEST['register'], $_REQUEST['loc_id'] );
            if (isset($resultData['register_name'])) {
                $cc_data['register_name'] = $resultData['register_name'];
            }
        }

        $insertcc = lavu_query("INSERT INTO `cc_transactions` (`order_id`,`amount`,`refunded`,`loc_id`,`datetime`,`pay_type`,`total_collected`,`transtype`,`action`,`server_time`,`server_name`, `server_id`, `register`, `register_name`, `check`) values('[order_id]','[amount]','[refunded]','[loc_id]','[datetime]','[pay_type]','[total_collected]','[transtype]','[action]','[server_time]','[server_name]','[server_id]','[register]','[register_name]','[check]')", $cc_data);
        $depositReceiptJson = depositReceipt($transaction_id);
        $parameterArr['url'] = "deposit_management.php";
        $parameterArr['fwdVars'] = $fwd_vars;
        $parameterArr['commandCall'] = "alert&title=Success&message=Deposit Successful&jsonResponse=" . $depositReceiptJson;
        $parameterArr['custId'] = $rdata;
        echo doiOSReferenceCalls($parameterArr);
    } else {
        $display_view = 0;
        $amount_data = array(
            'customer_id' => $rdata,
            'transaction_id' => $transaction_id,
            'reason' => $_POST['deposit_reason'],
            'amount' => unFormatCurrencyForInsertData($_POST['deposit_amount_input']),
            'created_date' => $datetime,
            'deposit_type' => $mode,
            'pay_type' => $pay_type,
            'pay_status' => 0
        );
        $insertamount = lavu_query("INSERT INTO `deposit_information` (`customer_id`,`transaction_id`,`reason`,`amount`,`created_date`,`deposit_type`,`pay_type`,`pay_status`) values('[customer_id]','[transaction_id]','[reason]','[amount]','[created_date]','[deposit_type]','[pay_type]','[pay_status]')", $amount_data);
        $parameterArr['url'] = "deposit_management.php";
        $parameterArr['fwdVars'] = "{$fwd_vars}&order_id=$transaction_id&checkout=card&amount=" . unFormatCurrencyForInsertData($_POST['deposit_amount_input']);
        echo doiOSReferenceCalls($parameterArr);
    }
}

if (isset($_POST['refund_amount']) && $_POST['refund_amount'] != '') {
    $pay_type = $_REQUEST['type'];
    $transaction_id = $_REQUEST['trans_id'];
    $rdata = $_REQUEST['cust_id'];
    $refund_amount = unFormatCurrencyForInsertData($_POST['refund_amount']);
    if ($mode === 'cash') {
        lavu_query("update `deposit_information` set  `transaction_status`=1,`pay_type` = 'refund',`refund_date`='$datetime',`refund_amount`='$refund_amount', `refund_reason`='" . $_POST['refund_reason'] . "' where transaction_id ='$transaction_id'");
        $cc_data = array (
            'order_id'        => $transaction_id,
            'amount'          => $refund_amount,
            'refunded'        => 1,
            'loc_id'          => $_REQUEST['loc_id'],
            'datetime'        => $datetime,
            'pay_type'        => $mode,
            'total_collected' => $_POST['refund_amount'],
            'transtype'       => 'deposit_refund',
            'action'          => 'deposit_refund',
            'server_time'     => $datetime,
            'server_name'     => $server_name,
            'server_id'       => $_REQUEST['server_id'],
            'register'        => $_REQUEST['register'],
            'check'           => '1'
        );
        $cc_data['register_name'] = "";
        if (isset($_REQUEST['register_name']) && trim($_REQUEST['register_name'])!="") {
            $cc_data['register_name'] = trim($_REQUEST['register_name']);
            $cc_data['register_name'] = addslashes($cc_data['register_name']);
        }
        else {
            $resultData = getRegisterName( $_REQUEST['register'], $_REQUEST['loc_id'] );
            if (isset($resultData['register_name'])) {
                $cc_data['register_name'] = $resultData['register_name'];
            }
        }
        $insertcc = lavu_query("INSERT INTO `cc_transactions` (`order_id`,`amount`,`refunded`,`loc_id`,`datetime`,`pay_type`,`total_collected`,`transtype`,`action`,`server_time`,`server_name`, `server_id`, `register`, `register_name`,`check`) values('[order_id]','[amount]','[refunded]','[loc_id]','[datetime]','[pay_type]','[total_collected]','[transtype]','[action]', '[server_time]', '[server_name]', '[server_id]','[register]', '[register_name]', '[check]')",$cc_data);
        $depositReceiptJson = depositReceipt($transaction_id);
        $parameterArr['url'] = "deposit_management.php";
        $parameterArr['fwdVars'] = $fwd_vars;
        $parameterArr['commandCall'] = "alert&title=Success&message=Deposit Refunded&jsonResponse=" . $depositReceiptJson;
        $parameterArr['custId'] = $rdata;
        echo doiOSReferenceCalls($parameterArr);
    }
    else {
        $cc_query = lavu_query("select * from `cc_transactions` where  `order_id` = '$transaction_id' ");
        $cc_result = mysqli_fetch_assoc($cc_query);
        $cc_trans_id = $cc_result['transaction_id'];
        lavu_query("update deposit_information set refund_reason='" . $_POST['refund_reason'] . "',refund_amount='$refund_amount',`pay_type` = 'refund' where transaction_id='$transaction_id'");
        $parameterArr['url'] = "deposit_management.php";
        $parameterArr['fwdVars'] = $fwd_vars;
        $parameterArr['custId'] = $rdata;
        $register="";
        $registerName="";
        if (isset($_REQUEST['register']) && trim($_REQUEST['register'])!="") {
            $register = $_REQUEST['register'];
            $resultData = getRegisterName( $register, $_REQUEST['loc_id'] );
            if (isset($resultData['register_name'])) {
                $registerName = $resultData['register_name'];
            }
        }
        
        if ($cc_result['gateway'] == 'PayPal' && $cc_result['process_data'] != 'native_extension:paypal') {
            $state = paypalRefund($data_name, $cc_result['preauth_id'], $refund_amount, $_REQUEST['loc_id'], $datetime, $mode, $transaction_id, $server_name);
            if ($state == 'success') {
                $depositReceiptJson = depositReceipt($transaction_id);
                $parameterArr['commandCall'] = "alert&title=Success&message=Deposit Refunded&jsonResponse=" . $depositReceiptJson;
                echo doiOSReferenceCalls($parameterArr);
            } else {
                $message = $state;
            }
        } else if ($cc_result['gateway'] == 'eConduit') {
            $parameterArr['commandCall'] = "card_input_popup&gateway=eConduit&transactionCommand=refund";
            $parameterArr['custId'] = "$rdata&order_id=$transaction_id&transaction_id=$cc_trans_id&amount=$refund_amount";
            echo doiOSReferenceCalls($parameterArr);
        }
        else if ($cc_result['gateway'] == 'Mercury' || $cc_result['gateway'] == 'Heartland') {
            $parameterArr['commandCall'] = "card_input_popup&gateway=" . $cc_result['gateway'] . "&transactionCommand=refund";
            $parameters = "$rdata&order_id=$transaction_id&transaction_id=$cc_trans_id&amount=$refund_amount";
            $parameters .= "&auth_code=" . $cc_result['auth_code'];
            $parameters .= "&orig_last_four=" . $cc_result['card_desc'];
            $parameters .= "&record_no=" . $cc_result['record_no'];
            $parameterArr['custId'] = $parameters;
            echo doiOSReferenceCalls($parameterArr);
        }
        else if (strtolower($cc_result['gateway'])=='square') {
			$parameterArr['commandCall'] = "card_input_popup&gateway=square&transactionCommand=refund";
			$parameterArr['custId'] = "$rdata&order_id=$transaction_id&transaction_id=$cc_trans_id&amount=$refund_amount&split_tender_id=$split_tender_id&authcode=$transaction_id";
			echo doiOSReferenceCalls($parameterArr);
		}
		else if ($cc_result['process_data'] == 'native_extension:paypal') {
			$inutParams = '';
			foreach ($cc_result as $key=>$val) {
				if ($key == 'action' || $key == 'transtype') {
					$val = 'Sale';
				}
				if ($key == 'amount') {
					$val = $refund_amount;
				}

				$inutParams .= '&'.$key.'='.$val;
			}
			$parameterArr['commandCall'] = "card_input_popup";
			$parameterArr['custId'] = "$rdata$inutParams";
			echo doiOSReferenceCalls($parameterArr);
		}
        else {
            $parameterArr['commandCall'] = "card_input_popup";
            $parameterArr['custId'] = "$rdata&order_id=$transaction_id&gateway=" . $cc_result['gateway'] . "&transaction_id=$cc_trans_id&amount=" . $refund_amount;
            echo doiOSReferenceCalls($parameterArr);
        }
    }
}

function paypalRefund($dataName,$refundID,$returnAmount,$locId,$dateTime,$mode,$transaction_id,$server_name, $register, $registerName) {
	require_once ("/home/poslavu/public_html/admin/lib/gateway_lib/paypal_func.php");
	$core = new PayPalCore ();
	$PayPalPaymentHandler = new PayPalPaymentHandler ( $dataName );
	$curlURL = "v1/payments/sale/$refundID/refund";
	$fields = '{"amount": {'.
			'"total": "'. floatval($returnAmount).'",'.
			'"currency": "USD"},'.
			'"is_non_platform_transaction": "yes"}';
	$response = $PayPalPaymentHandler->makePayment ( $fields, $core->PayPalEndpointURL . $curlURL );
	$rspvars = json_decode ( $response, true );
	$txn = (isset ( $rspvars ['transactionNumber'] )) ? $rspvars ['transactionNumber'] : "";
	$message = (isset ( $rspvars ['message'] )) ? $rspvars ['message'] : "No Message";
	$state = (isset ( $rspvars ['state'] )) ? $rspvars ['state'] : "";
	$ReturnID = empty ( $rspvars ['id'] ) ? "" : $rspvars ['id'];
	if ($state === "completed")
	{
		$refundedAmount = floatval($returnAmount);
		$cc_data = array( 'order_id' => $transaction_id,
		                  'amount'=>$refundedAmount,
		                  'refunded' =>1,
		                  'loc_id'=>$locId,
		                  'datetime' => $dateTime,
		                  'pay_type' => $mode,
		                  'total_collected'=>$returnAmount,
		                  'transtype'=>'deposit_refund',
		                  'action'=>'deposit_refund',
		                  'server_time'=>$dateTime,
		                  'transaction_id' => $ReturnID,
		                  'auth_code' => $txn,
		                  'server_name'=>$server_name,
		                  'check'=>'1',
		                  'register'=>$register,
		                  'register_name'=>$registerName
		);

		lavu_query("INSERT INTO `cc_transactions` (`order_id`,`amount`,`refunded`,`loc_id`,`datetime`,`pay_type`,`total_collected`,`transtype`,`action`,`server_time`,`transaction_id`,`auth_code`,`server_name`,`check`,`register`,`register_name`) values('[order_id]','[amount]','[refunded]','[loc_id]','[datetime]','[pay_type]','[total_collected]','[transtype]','[action]','[server_time]','[transaction_id]','[auth_code]','[server_name]','[check]','[register]','[register_name]')",$cc_data);

		lavu_query("update deposit_information set `transaction_status`=1,`refund_date`='$dateTime',`pay_type`='refund' where transaction_id='$transaction_id'");
		return 'success';
	}
	else
	{
		return $message;
	}

}
require_once(dirname(__FILE__)."/../webview_special_functions.php");

if (isset($_REQUEST['type']) && $_REQUEST['trans_id'] == '' && $_REQUEST['submode'] != 'addnew' && $_REQUEST['checkout'] != 'card') {
    $display .= "<div >
		<form id='search_form' method='post' action=''>
            <div id='background_div'>
                <div>
                    <table cellpadding='8' style='text-align:center; border-collapse:collapse;'>
                        <tr>
                            <td><h2>" . ucfirst($_REQUEST['mode']) . " " . ucfirst($_REQUEST['type']) . "</h2></td>
                        </tr>
                        <tr>
                            <td>
                                <input type='hidden' name='hidsearch' id='hidsearch'/>
                            	<input class='btn_light_super_long' type='text' autocomplete='off'  name='search' id='search' placeholder='Search'/>
                            	<div id='suggesstion-box'></div>
                            <td>
                        </tr>
                        <tr>
                            <td colspan='2'><div class='required' id='message_search'>" . $message . "</div></td>
						</tr>
                        <tr>
                            <td align='center' style='padding:6px 0px 0px 0px;'>
                                <button class='btn_light btn_deposit' type='button' onclick='return validateSearch()'><b>Submit</b></button><br>";
    if ($_REQUEST['type'] != 'refund') {
        $display .= "<br>OR<br><br> <button class='btn_light btn_deposit' type='button' onclick='window.location = \"deposit_management.php?{$fwd_vars}&submode=addnew\"'><b>Add New</b></button>";
    }
    $display .= "</td>
								</tr>
							</table>";
    if (mysqli_num_rows($search_query) > 0) {

        $display .= "<table cellpadding='8' style='text-align:center; border-collapse:collapse;'>

									<tr>
										<td>Name</td>";
        if ($type == 'deposit' && ($searchstring != '' || $_REQUEST['hidsearch'] > 0)) {
            $display .= "  <td>Email</td>";
        } else {
            $display .= "  <td>Deposit Amount</td>";
            $display .= "  <td>Deposit Date </td>";
            $display .= "  <td>Reason</td>";
        }
        $display .= "<td>&nbsp;</td>
									</tr>";
        if (mysqli_num_rows($search_query) > 0) {
            while ($search_result = mysqli_fetch_assoc($search_query)) {
                $user_name = $search_result['first_name'] . ' ' . $search_result['last_name'];
                $display .= "<tr>";
                $display .= '<td>' . $user_name . '</td>';
                if ($type == 'deposit' && ($searchstring != '' || $_REQUEST['hidsearch'] > 0)) {
                    $display .= '<td>' . $search_result['email'] . '</td>';
                    $display .= "<td><div onclick='window.location = \"deposit_management.php?{$fwd_vars}&submode=addnew&cust_id=$search_result[customer_id]\"'><img src='images/next_arrow.png'/></div></td>";
                } else {
                    $display .= '<td>' . showAmount($search_result['amount']) . '</td>';
                    $display .= '<td>' . substr($search_result['date'], 0, 10) . '</td>';
                    $display .= '<td>' . $search_result['reason'] . '</td>';
                    if ($type == 'deposit') {
                        $display .= "<td><div onclick='window.location = \"deposit_management.php?{$fwd_vars}&submode=addnew&cust_id=$search_result[customer_id]\"'><img src='images/next_arrow.png'/></div></td>";
                    } else {
                        $display .= "<td><div onclick='window.location = \"deposit_management.php?{$fwd_vars}&type=refund&trans_id=$search_result[transaction_id]&cust_id=$search_result[customer_id]\"'><img src='images/next_arrow.png'/></div></td>";
                    }
                }
                $display .= "</tr>";
            }
        } else {
            $display .= "<tr>
										<td colspan='2'>
											No record found
										</td>
									</tr>";
        }
        $display .= "</table>";
    }
    $display .= "</div>
					</div>";
    $display .= $hidden_field;
    $display .= "</form>";

    $display .= '</div>';
} elseif (isset($_REQUEST['type']) && $_REQUEST['type'] === 'refund' && $_REQUEST['trans_id'] != '') {

    $customer_query = lavu_query("SELECT dci.id as customer_id,dci.first_name,dci.last_name,dci.email,dci.phone,di.id,di.customer_id,di.transaction_id,di.amount FROM deposit_information as di left join `deposit_customer_info` as dci  on dci.id=di.customer_id where di.transaction_id='" . $_REQUEST['trans_id'] . "' and pay_type='deposit'");
    $customer_result = mysqli_fetch_assoc($customer_query);

    $display .= "<div>
    <form id='refundform' method='post' action=''>
        <div id='background_div'>
        	<div>
        		<table cellpadding='8' style='text-align:right; border-collapse:collapse;'>
	                <tbody style='border: #ccc 1px solid;'>
        			<tr style='background-color: #ccc'>
        				<td colspan='2' style='text-align:center;'>
                        <div onclick='window.location = \"deposit_management.php?{$fwd_vars}\"'><img src='images/green_back.png' style='float:left;margin-top: 9px;'> </div>
                        <h2>" . ucfirst($mode) . " " . ucfirst($_REQUEST['type']) . "<h2></td>
        			</tr>
        			<tr>
        				<td colspan='2'><div id='message_refund' class='required' >" . $message . "</div></td>
	        				</tr>
	        				<tr>
	        				<td class='label label-info'>First Name: </td>
	        				<td>
	        				<input class='btn_light_super_long' type='text' readonly  name='user_fname' id='user_fname' placeholder='First Name' value='$customer_result[first_name]'/>
	        				<br>
	        				</td>
	        				</tr>
	        				<tr>
	        				<td class='label label-info'>Last Name: </td>
	        				<td>
	        				<input class='btn_light_super_long' type='text' readonly  name='user_lname' id='user_lname' placeholder='Last Name' value='$customer_result[last_name]' />
	        				<br>
	        				</td>
	        				</tr>
	        				<tr>
	        				<td class='label label-info'>Email: </td>
	        				<td>
	        				<input class='btn_light_super_long' type='text' readonly  name='user_email' id='user_email' placeholder='Email' value='$customer_result[email]'/>
	        				<br>
	        				</td>
	        				</tr>
	        				<tr>
	        				<td class='label label-info'>Phone: </td>
	        				<td>
	        				<input class='btn_light_super_long' type='tel' readonly  name='phone' id='phone' placeholder='Phone' value='$customer_result[phone]'/>
	        				<br>
	        				</td>
	        				</tr>
	        				<tr>
	        				<td class='label label-info'>Refund Amount: <label class='req'>*</label></td>
	        				<td>

	        				<input class='btn_light_super_long' type='tel' READONLY onclick='window.location = \"_DO:cmd=number_pad&type=money&max_digits=15&title=Refund%20Amount&js=setFieldValue(this, %27refund_amount%27,%27ENTERED_NUMBER%27)\"'  name='refund_amount' id='refund_amount' placeholder='Amount' value='" . showAmount($customer_result[amount]) . "'/>

                            <input type='hidden' name='old_amount' id='old_amount' value='$customer_result[amount]'/>
	        				<br>
	        				</td>
	        				</tr>
	        				<tr>
	        				<td class='label label-info'>Reason: </td>
	        				<td>
	        				<input class='btn_light_super_long' type='text'  name='refund_reason' id='refund_reason' placeholder='Reason'/>
	        				<br>
	        				</td>
	        				</tr>
	        				<tr>
	        				<td class='label label-info'>&nbsp; </td>
	        				<td  align='right'><button class='btn_light btn_deposit' type='button' onclick='validateRefund()' ><b>Submit</b></button></td>
	        				</tr>
                            </tbody>
	        				</table>
	        				</div>
	        				</div>";
    $display .= $hidden_field;
    $display .= "</form>";
    $display .= '</div>';
}
else if (isset($_REQUEST['checkout']) && $_REQUEST['checkout'] === 'card') {

    $order_query = lavu_query("SELECT dci.id as customer_id,dci.first_name,dci.last_name,dci.email,dci.phone,di.id,di.transaction_id,di.amount FROM deposit_information as di left join `deposit_customer_info` as dci  on dci.id=di.customer_id where di.transaction_id='" . $_REQUEST['order_id'] . "'");
    $order_result = mysqli_fetch_assoc($order_query);
    $pgObj = new Deposit\PaymentGateway($_REQUEST['loc_id']);
    $pgInfoArray = $pgObj->getCurrentPGInfo( 'payment_extension:82' );
    if ( count($pgInfoArray) > 0 ) {
        if ( isset($pgInfoArray[0]) && $pgInfoArray[0]->paymentGatewayName == "eConduit" ) {
            $firstGatewayName = $pgInfoArray[0]->paymentGatewayName;
        }

        if ( isset($pgInfoArray[0]) && $pgInfoArray[0]->paymentGatewayName != "eConduit" ) {
            $secondGatewayName = $pgInfoArray[0]->paymentGatewayName;
        }

        if ( isset($pgInfoArray[1]) && $pgInfoArray[1]->paymentGatewayName != "eConduit" ) {
            $secondGatewayName = $pgInfoArray[1]->paymentGatewayName;
        }
    }

    $display .= "<div>
    <form id='refundform' method='post' action=''>
        <div id='background_div'>
        	<div>
        		<table cellpadding='8' style='text-align:right; border-collapse:collapse;'>
	                <tbody style='border: #ccc 1px solid;'>
            			<tr style='background-color: #ccc'>
            				<td colspan='2' style='text-align:center;'>
                            <div onclick='window.location = \"deposit_management.php?{$fwd_vars}\"'><img src='images/green_back.png' style='float:left;margin-top: 9px;'> </div>
                            <h2>Checkout<h2></td>
            			</tr>
        				<tr>
            				<td class='label label-info'> Name: </td>
            				<td class='checkouttd'>
                               $order_result[first_name]&nbsp $order_result[last_name]
                            </td>
            			</tr>
                        <tr>
            				<td class='label label-info'>Email: </td>
            				<td class='checkouttd'> $order_result[email]  </td>
            			</tr>
                        <tr>
            				<td class='label label-info'>Phone : </td>
            				<td class='checkouttd'>$order_result[phone] </td>
            			</tr>
                        <tr>
            				<td class='label label-info'>Amount: </td>
            				<td class='checkouttd'>" . showAmount($order_result['amount']) . "</td>
        				</tr>
                        <tr>";
                        if ( isset($firstGatewayName) && $firstGatewayName != '' ) {
                            $display .= " <td>
                                            <input type='button' name='card' class='btn_light btn_deposit'  value='LIP'  onclick='window.location.href =\"deposit_management.php?{$fwd_vars}&_DO:cmd=card_input_popup&gateway={$firstGatewayName}&transactionCommand=sale&cust_id=$order_result[customer_id]&order_id=$order_result[transaction_id]&amount={$order_result['amount']}\"'/>
                            			</td>
                	     			<td>";
                        }else{
                        	$display .= "<td colspan='2' style='text-align:center;'>";
                        }
                        if (isset($secondGatewayName) && $secondGatewayName != '') {
                            $display .= "<input type='button' name='card' class='btn_light btn_deposit'  value='Card'  onclick='window.location.href =\"deposit_management.php?{$fwd_vars}&_DO:cmd=card_input_popup&gateway={$secondGatewayName}&cust_id=$order_result[customer_id]&order_id=$order_result[transaction_id]&amount={$order_result['amount']}\"'/>";
                        }

               			if (isset($secondGatewayName) && $secondGatewayName != '' && $secondGatewayName == 'PayPal') {
               				$display .= "&nbsp;&nbsp;&nbsp;<input type='button' name='card' class='btn_light btn_deposit'  value='Paypal Emv'  onclick='window.location.href =\"deposit_management.php?{$fwd_vars}&_DO:cmd=card_input_popup&gateway=paypal_emv&cust_id=$order_result[customer_id]&order_id=$order_result[transaction_id]&amount={$order_result['amount']}\"'/>";
               			}
               $display .= "</td>";
               $display .= "</tr>
                   </tbody>
	            </table>
            </div>
        </div>";

    $display .= $hidden_field;
    $display .= "</form>";
    $display .= '</div>';
}
else {
    if ($display_view == '1') {
        $customer_result = '';
        $validationRule = 'add';
        if ($_REQUEST['cust_id'] != '') {
            $customer_id = $_REQUEST['cust_id'];
            $customer_query = lavu_query("SELECT *  FROM  deposit_customer_info where id=$customer_id");
            $customer_result = mysqli_fetch_assoc($customer_query);
            $validationRule = 'edit';
        }
        $display .= "<div >
    <form id='customerDepositForm' method='post' action=''>
        <div id='background_div'>
        	<div>
        		<table cellpadding='8' style='text-align:right; border-collapse:collapse;'>
                    <tbody style='border: #ccc 1px solid;'>
        			<tr style='background-color: #ccc'>
        				<td colspan='2' style='text-align:center;'>

                          <div onclick='window.location = \"deposit_management.php?{$fwd_vars}\"'><img src='images/green_back.png' style='float:left;margin-top: 9px;'> </div>
                            <h2>" . ucfirst($mode) . " " . ucfirst($_REQUEST['type']) . "<h2>
                        </td>
        			</tr>
        			<tr>
                        <td class='label label-info'>&nbsp; </td>
        				<td><div id='message_changebank' class='required' >" . $message . "</div></td>
	        		</tr>
        			<tr>
        				<td class='label label-info'>First Name: <label class='req'>*</label></td>
        				<td>
        					<input class='btn_light_super_long' type='text'  name='user_fname' id='user_fname' placeholder='First Name' value='$customer_result[first_name]'/>
        					<br>
        				</td>
        			</tr>
        			<tr>
        				<td class='label label-info'>Last Name: <label class='req'>*</label></td>
        				<td>
        					<input class='btn_light_super_long' type='text'  name='user_lname' id='user_lname' placeholder='Last Name' value='$customer_result[last_name]' />
        					<br>
        				</td>
        			</tr>
        			<tr>
        				<td class='label label-info'>Email: <label class='req'>*</label></td>
        				<td>
        					<input class='btn_light_super_long' type='text'  name='user_email' id='user_email' placeholder='Email' value='$customer_result[email]'/>
        					<br>
        				</td>
        			</tr>
        			<tr>
        				<td class='label label-info'>Phone: </td>
        				<td>
        					<input class='btn_light_super_long' type='tel'  name='phone' id='phone' placeholder='Phone' value='$customer_result[phone]'/>
        					<br>
        				</td>
        			</tr>
        			<tr>
        				<td class='label label-info'>Deposit Amount: <label class='req'>*</label></td>
        				<td>
        					<input class='btn_light_super_long' type='tel' READONLY onclick='window.location = \"_DO:cmd=number_pad&type=money&max_digits=15&title=Deposit%20Amount&js=setFieldValue(this, %27deposit_amount_input%27,%27ENTERED_NUMBER%27)\"' name='deposit_amount_input' id='deposit_amount_input' placeholder='Amount'/>
        					<br>
        				</td>
        			</tr>
        			<tr>
        				<td class='label label-info'>Reason: </td>
        				<td>
        					<input class='btn_light_super_long' type='text'  name='deposit_reason' id='deposit_reason' placeholder='Reason'/>
        					<br>
        				</td>
        			</tr>
        			<tr>
                        <td class='label label-info'>&nbsp; </td>
                        <td  align='right'><button class='btn_light btn_deposit' type='button' onclick='validateAddNew(\"$validationRule\")'><b>Submit</b></button></td>
        			</tr>
                    </tbody>
        		</table>
        	</div>
        </div>";
        $display .= $hidden_field;
        $display .= "</form>";
        $display .= '</div>';
    }
}

function showAmount($amount) {
    global $location_info;
    $precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
    if ($location_info['decimal_char']!="") {
        $decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
        $thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char'] == ".") ? "," : '.';
    }
    $thousands_sep = (!empty($thousands_char)) ? $thousands_char : ",";
    $monitary_symbol = isset($location_info['monitary_symbol']) ? htmlspecialchars($location_info['monitary_symbol']) : '';
    $formattedAmount = number_format($amount, $precision, $decimal_char, $thousands_sep);
    if (isset($location_info['left_or_right']) && $location_info['left_or_right'] !="") {
        if ( strtolower($location_info['left_or_right']) == "right" ) {
            $formattedAmount = $formattedAmount ." ". $monitary_symbol;
        }
        else {
            $formattedAmount = $monitary_symbol ." ". $formattedAmount;
        }
    }
    return $formattedAmount;
}

function depositReceipt($depositId)
{
    $receiptJson = json_encode(array());
    if (is_null($depositId) == false) {
        // Deposit Receipt Print
        $stdObj = new stdClass();
        $stdObj->depositId = $depositId;
        $depositObj = new Deposit\Receipt($stdObj);
        $deposit = $depositObj->getDepositInfo();
        if (is_null($depositId) == false) {
            $receiptObj = new Deposit\ReceiptTemplate($deposit);
            // Pass deposit information in JSON form to iOS App.
            $receiptJson = $receiptObj->printReceipt();
        }
    }
    return $receiptJson;
}

function doiOSReferenceCalls($parameterArr)
{
    $returnStr = "<script language='javascript' type='text/javascript'>window.location.href='";
    if (isset($parameterArr['url'])) {
        $returnStr .= $parameterArr['url'] . "?";
    }

    if (isset($parameterArr['fwdVars'])) {
        $returnStr .= $parameterArr['fwdVars'];
    }

    if (isset($parameterArr['commandCall'])) {
        $returnStr .= "&_DO:cmd=" . $parameterArr['commandCall'];
    }

    if (isset($parameterArr['custId'])) {
        $returnStr .= "&cust_id=" . $parameterArr['custId'];
    }
    $returnStr .= "'</script>";
    return $returnStr;
}

function unFormatCurrencyForInsertData($monetary_str, $monitary_symbol = false)
{
    global $location_info;

    if ($monitary_symbol === false) {
        $monitary_symbol = isset($location_info['monitary_symbol']) ? trim($location_info['monitary_symbol']) : '';
    }
    $decimal_char = isset($location_info['decimal_char']) ? trim($location_info['decimal_char']) : '.';
    $monetary_str = str_replace($monitary_symbol, '', $monetary_str);
    $explodeResultArray = explode($decimal_char, $monetary_str);
    $lastPart = "";
    $final_monetary_str = "";
    if (count($explodeResultArray) > 1) {
        $lastPart = array_pop($explodeResultArray);
    }
    $firstPart = implode($explodeResultArray, '');
    if (! empty($firstPart) && empty($lastPart)) {
        $final_monetary_str .= $firstPart;
    }
    if (! empty($firstPart) && ! empty($lastPart)) {
        $final_monetary_str .= str_replace(array(
            '.',
            ','
        ), '', $firstPart);
    }
    if (!empty($lastPart)) {
        $final_monetary_str .= "." . $lastPart;
    }
    return floatval($final_monetary_str);
}

function updateCcTransactionPaypalEmv($request, $type)
{
	$cc_data = array (
			'order_id' => $request['order_id'],
			'check' => '1',
			'amount' => $request['amount'],
			'card_desc' => $request['card_desc'],
			'transaction_id' => $request['transaction_id'],
			'auth' => $request['auth'],
			'loc_id' => $request['loc_id'],
			'processed' => $request['processed'],
			'auth_code' => $request['auth_code'],
			'card_type' => $request['card_type'],
			'datetime' => $request['datetime'],
			'pay_type' => $request['pay_type'],
			'register' => $request['register'],
			'got_response' => $request['got_response'],
			'transtype' => $type,
			'total_collected' => $request['total_collected'],
			'server_name' => $request['server_name'],
			'action' => $type,
			'ref_data' => $request['ref_data'],
			'process_data' => $request['process_data'],
			'server_id' => $request['server_id'],
			'preauth_id' => $request['preauth_id'],
			'register_name' => $request['register_name'],
			'pay_type_id' => $request['pay_type_id'],
			'first_four' => $request['first_four'],
			'mpshc_pid' => $request['mpshc_pid'],
			'server_time' => $request['server_time'],
			'rf_id' => $request['rf_id'],
			'internal_id' => $request['internal_id'],
			'gateway' => $request['gateway'],
			'extra_info' => $request['extra_info'],
			'record_no' => $request['record_no']
	);

	lavu_query("INSERT INTO `cc_transactions` (`order_id`,`check`,`amount`,`card_desc`,`transaction_id`,`auth`,`loc_id`,`processed`,`auth_code`,`card_type`,`datetime`,`pay_type`,`register`,`got_response`,`transtype`,`total_collected`,`server_name`,`action`,`ref_data`,`process_data`,`server_id`,`preauth_id`,`register_name`,`pay_type_id`,`first_four`,`mpshc_pid`,`server_time`,`rf_id`,`internal_id`,`gateway`,`extra_info`,`record_no`) values('[order_id]','[check]','[amount]','[card_desc]','[transaction_id]','[auth]','[loc_id]','[processed]','[auth_code]','[card_type]','[datetime]','[pay_type]','[register]','[got_response]','[transtype]','[total_collected]','[server_name]','[action]','[ref_data]','[process_data]','[server_id]','[preauth_id]','[register_name]','[pay_type_id]','[first_four]','[mpshc_pid]','[server_time]','[rf_id]','[internal_id]','[gateway]','[extra_info]','[record_no]')", $cc_data);
}

$display .= '<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>';
$display .= '<script>
	$("#search").keyup(function() {
		if ($(this).val().length >= 3)
		{
		$.ajax({
		type: "POST",
		url: "depositUserList.php",
		data:\'keyword=\'+$(this).val()+\'&cc=' . $_REQUEST["data_name"] . '\'+\'&type=' . $_REQUEST["type"] . '\'+\'&mode=' . $_REQUEST["mode"] . '\',
		beforeSend: function() {
			//$("#search").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data) {
			$("#suggesstion-box").show();
			$("#suggesstion-box").html(data);
			$("#search").css("background","#FFF");
		}
		});
		}
		else
		{
			$("#suggesstion-box").hide();
		}
	});
    function selectUser(cust_id,cust_name) {
        $("#search").val(cust_name);
        $("#hidsearch").val(cust_id);
        $("#suggesstion-box").hide();
        document.getElementById("search_form").submit();
    }
	function checkemailExist(email, callback)
	{
		$.ajax({
		type: "POST",
		url: "depositCheckemailExist.php",
		data:\'email=\'+email+\'&cc=' . $_REQUEST["data_name"] . '\'+\'&type=' . $_REQUEST["type"] . '\'+\'&mode=' . $_REQUEST["mode"] . '\',
		success: callback
		});
	}
</script>';
function getRegisterName( $register, $locationId ) {
    $result = array();
    if(trim($register)!=""&&trim($locationId)!=""){
        $query = lavu_query("select value2 as register_name FROM config WHERE type='printer' AND _deleted=0 AND setting='".$register."' AND location=".$locationId);
        if(mysqli_num_rows($query)>0) {
            $result = mysqli_fetch_assoc($query);
        }
    }
    return $result;
}
?>
