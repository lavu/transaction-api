<?php
		/*$display .= "Shifts Screen";
		$display .= "<br><br>Register: " . $register;
		$display .= "<br><br>Server Id: " . $server_id;
		$display .= "<br><br>Device Time: " . $device_time;
		$display .= "<br><br>Location Id: " . $loc_id;*/
		
		
		echo "<script language='javascript'>function start_new_shift(type) { window.location = '?mode=shifts&new_shift=' + type + '&$fwd_vars' }</script>";
		
		function show_shift_time($dt)
		{
			$dt = explode(" ",$dt);
			if(count($dt) > 1)
			{
				$dt_date = explode("-",$dt[0]);
				$dt_time = explode(":",$dt[1]);
				
				if(count($dt_date) > 2 && count($dt_time) > 2)
				{
					$dt_year = $dt_date[0];
					$dt_month = $dt_date[1];
					$dt_day = $dt_date[2];
					$dt_hour = $dt_time[0];
					$dt_mins = $dt_time[1];
					$dt_secs = $dt_time[2];
					$ts = mktime($dt_hour,$dt_mins,$dt_secs,$dt_month,$dt_day,$dt_year);
					
					return "<font color='#000088'>" . date("h:i",$ts) . "</font><font color='#999999'>:" . date("s",$ts) . "</font> <font color='#000088'>" . date(" a",$ts) . "</font>";
				}
			}
		}
		
		$display .= "<style>";
		$display .= " table { ";
		$display .= "   font-size:16px; ";
		$display .= " } ";
		$display .= "</style>";
		$display .= "<font style='font-size:24px'><b>***".speak("Shifts")."***</b></font><br>";
		$display .= "<br>".speak("Current Time").": " . show_shift_time($device_time) . "<br><br>";
		
		if($loc_id)
		{
			$location_query = lavu_query("select * from `locations` where `id`='[1]'",$loc_id);
			if(mysqli_num_rows($location_query))
			{
				$location_read = mysqli_fetch_assoc($location_query);
				$stime = $location_read['day_start_end_time'];
				if(is_numeric($stime))
				{
					$stime_mins = $stime % 100;
					$stime_hours = ($stime - $stime_mins) / 100;
					if($stime_hours < 10) $stime_hours = "0".$stime_hours;
					if($stime_mins < 10) $stime_mins = "0".$stime_mins;
					$apply_stime = $stime_hours . ":" . $stime_mins . ":00";
				}
				else
				{
					$apply_stime = "00:00:00";
				}
			}
			
			$day_date_parts = explode(" ",$device_time);
			$day_date = $day_date_parts[0];
			if(count($day_date_parts) > 1)
			{
				$day_time = $day_date_parts[1];
				if($day_time < $apply_stime)
				{
					$day_parts = explode("-",$day_date);
					$day_year = $day_parts[0];
					$day_month = $day_parts[1];			
					$day_day = $day_parts[2];
					$day_date = date("Y-m-d",mktime(0,0,0,$day_month,$day_day -1,$day_year));
				}
			}
			
			//$day_date = substr($device_time,0,10);

			$day_parts = explode("-",$day_date);
			if(count($day_parts) > 2)
			{
				$day_year = $day_parts[0];
				$day_month = $day_parts[1];
				
				$day_day = $day_parts[2];
				$day_nextdate = date("Y-m-d",mktime(0,0,0,$day_month,$day_day + 1,$day_year));
				
				$day_stime = $day_date . " " . $apply_stime;
				$day_etime = $day_nextdate . " " . $apply_stime;
				
				if(isset($_GET['new_shift']))
				{
					$type = "register";
					if ($_GET['new_shift'] == "2") $type = "house";
					lavu_query("insert into `shifts` (`loc_id`,`register`,`datetime`,`type`) values ('[1]','[2]','[3]','[4]')",$loc_id,$register,$device_time,$type);
				}
				
				$show_rshifts = true;
				$show_hshifts = false;
				if ($location_info['track_shifts_by'] == "house") {
					$show_rshifts = false;
					$show_hshifts = true;
				} else if ($location_info['track_shifts_by'] == "both") $show_hshifts = true;
				
				$shift_query = lavu_query("select * from `shifts` where `loc_id`='[1]' and `register`='[2]' and `datetime`>'[3]' and `datetime`<'[4]'",$loc_id,$register,$day_stime,$day_etime);
				$rshift_count = 1;
				$hshift_count = 1;
				$next_rshift_start = $day_stime;
				$next_hshift_start = $day_stime;
				$hshift_code = "";
				if ($show_rshifts) {
					$display .= "<table cellpadding=2><tr><td style='font-size:14px; color:#666666;'><b>".$register_name." (".$register.")".speak("Shifts")."</b></td></tr></table>";
					$display .= "<table cellpadding=6 cellspacing=0 style='border:solid 2px #777799' bgcolor='#f8f8f8'>";
				}
				while($shift_read = mysqli_fetch_assoc($shift_query))
				{
					$temp_code = "";
					$shift_count = $rshift_count;
					$next_shift_start = $next_rshift_start;
					$report_title = "Register Summary";
					if ($shift_read['type'] == "house") {
						$shift_count = $hshift_count;
						$next_shift_start = $next_hshift_start;
						$report_title = "X Report";
					}
					$temp_code .= "<tr>";
					$temp_code .= "<td align='right'> ". speak('Shift'). ' ' . $shift_count.":</td>";
					$temp_code .= "<td>".show_shift_time($next_shift_start)."</td>";
					$temp_code .= "<td>-</td>";
					$temp_code .= "<td>" . show_shift_time($shift_read['datetime']) . "</td>";
					$temp_code .= "<td><input type='button' value='". speak('View') . " " . speak($report_title) . "' onclick='window.location = \"?mode=shifts&view_shift=1&type=".$shift_read['type']."&shift_number=".$shift_count."&shift_start=".$next_shift_start."&shift_end=".$shift_read['datetime']."&$fwd_vars\"' /></td>";
					$temp_code .= "<td><input type='button' value='". speak('Print') . " " . speak($report_title) . "' onclick='window.location = \"_DO:cmd=print&filename=till_report.php&m=".($shift_read['type']=="house"?"x_report":"register")."&dn=$cc&loc_id=$loc_id&register=$register&vars=shift_info=type(eq)".$shift_read['type']."(and)shift_number(eq)".$shift_count."(and)shift_start(eq)".urlencode($next_shift_start)."(and)shift_end(eq)".urlencode($shift_read['datetime'])."\"' /></td>";
					$temp_code .= "</tr>";
					$next_shift_start = $shift_read['datetime'];
					$shift_count++;
					if ($shift_read['type'] == "house") {
						$hshift_code .= $temp_code;
						$hshift_count = $shift_count;
						$next_hshift_start = $next_shift_start;
					} else if ($show_rshifts) {
						$display .= $temp_code;
						$rshift_count = $shift_count;
						$next_rshift_start = $next_shift_start;
					}
				}
				
				if ($show_rshifts) {
					$current_shift = $rshift_count;//mysqli_num_rows($shift_query) + 1;
					$display .= "<tr bgcolor='#d2d2d2'><td align='right'>Shift $current_shift:</td><td>".show_shift_time($next_rshift_start)."</td><td colspan=4>&nbsp;</td></tr>";
					$display .= "</table>";
					//$display .= "<br><input type='button' value='Start New Register Shift' style='font-size:18px' onclick='if(confirm(\"Start a new shift for this register?\")) window.location = \"?mode=shifts&new_shift=1&$fwd_vars\"'><br><br><br><br>";
					//window.location = \"_DO:cmd=confirm&title=Start a new shift for this register%3F&message=&js_cmd=start_new_shift(1);\";
					$display .= "<br><input type='button' value='".speak("Start New Register Shift")."' style='font-size:18px' onclick='".use_available_confirm("start_new_shift(1)", "", speak("Start a new shift for this register")."?")."'><br><br><br><br>";
				}
				if ($show_hshifts) {
					$display .= "<table cellpadding=2><tr><td style='font-size:14px; color:#666666;'><b>".speak("House Shifts")."</b></td></tr></table>";
					$display .= "<table cellpadding=6 cellspacing=0 style='border:solid 2px #777799' bgcolor='#f8f8f8'>";
					$display .= $hshift_code;
					$current_shift = $hshift_count;
					$display .= "<tr bgcolor='#d2d2d2'><td align='right'>Shift $current_shift:</td><td>".show_shift_time($next_hshift_start)."</td><td colspan=4>&nbsp;</td></tr>";
					$display .= "</table>";
					//$display .= "<br><input type='button' value='Start New House Shift' style='font-size:18px' onclick='if(confirm(\"Start a new shift for the house?\")) window.location = \"?mode=shifts&new_shift=2&$fwd_vars\"'>";
					//window.location = \"_DO:cmd=confirm&title=Start a new shift for the house%3F&message=&js_cmd=start_new_shift(2);\"
					$display .= "<br><input type='button' value='".speak("Start New House Shift")."' style='font-size:18px' onclick='".use_available_confirm("start_new_shift(2)", "", speak("Start a new shift for the house")."?")."'>";
				}
			}
		}
		
		if(isset($_GET['view_shift']))
		{
			$shift_number = $_GET['shift_number'];
			$shift_start = $_GET['shift_start'];
			$shift_end = $_GET['shift_end'];
			//$display .= "shift_start: " . $shift_start . "<br>shift_end: " . $shift_end . "<br>";
			
			$current_request = explode("inapp_management.php",$_SERVER['REQUEST_URI']);
			if(count($current_request) > 1)
			{
				$host = $_SERVER['HTTP_HOST'];
				if ($host != "admin.poslavu.com") $host = "localhost";
			
				$rpt_md = "register";
				if ($_GET['type'] == "house") $rpt_md = "x_report";
			
				$url_file = "http://" . $host . $current_request[0] . "till_report.php";
				$display .= "<br><br><table width='90%'><tr><td><hr></td></tr></table><br><b>Shift " . $shift_number . ":";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url_file . "?m=".$rpt_md."&dn=$cc&loc_id=$loc_id&register=$register&shift_info=shift_number(eq)".$shift_number."(and)shift_start(eq)".urlencode($shift_start)."(and)shift_end(eq)".urlencode($shift_end));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$ch_response = curl_exec($ch);
	
				//$display .= "<br>$url_file";
				//$display .= "<br>error number: " . curl_errno($ch);
				//$display .= "<br>error message: " . curl_error($ch);			
				$ch_response = str_replace(":","<br>",$ch_response);
				$ch_response = str_replace("[c",":",$ch_response);
				
				if((trim(substr($ch_response,0,2))=="1|") || (trim(substr($ch_response,0,6))=="ALERT|"))
				{
					$ch_response = substr($ch_response,2);
					if (trim(substr($ch_response,0,6))=="ALERT|") {
						$ch_response = "<br>".substr($ch_response,6);
						$ch_response = str_replace("for this day", "for this shift", $ch_response);
					}
					$ch_response = str_replace(array("receipt#" . $register, $register . "#" . $register, $register . "#receipt"), array("", "", ""), $ch_response);
					$display .= "<br>" . $ch_response;
				}
			}
		}
		//$display .= "<br><br><a href='_DO:cmd=open_drawer&details=Opened from Shift Screen'>Open Drawer</a>";
		//$display .= "<br><br><a href='_DO:cmd=print&filename=till_report.php&mode=register&vars=abc123'>Print Register Report</a>";
		
?>
