<?php
		$locationid = $loc_id;
		$display .= "<u>".speak("Order Management").": ".speak("Transfers")."</u>";

		if(isset($_GET['tableid'])) $tableid = $_GET['tableid'];
		else if(isset($_POST['tableid'])) $tableid = $_POST['tableid'];
		else
		{
			$tbl_query = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '$locationid' AND `_deleted` != '1'");
			$tbl_read = mysqli_fetch_assoc($tbl_query);
			$tableid = $tbl_read['id'];
		}
		$display .= "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>";
		$display .= "<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>";
		$display .= "<link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' />";
		$display .= "<style>";
		$display .= ".select2-container, .select2-dropdown, .select2-results { font-family: Arial, Verdana; font-size: 12px !important; }";
		$display .= "</style>";
		$display .= "<script type='text/javascript'>";
		$display .= "$(document).ready(function() {";
		$display .= "$('.select-room').select2({ minimumResultsForSearch: Infinity, theme: 'classic' });";
		$display .= "$('.select-tab').select2({ minimumResultsForSearch: Infinity, theme: 'classic' });";
		$display .= "});";
		$display .= "</script>";
		$display .= "<link href='/cp/css/select2-4.0.3.min.css' rel='stylesheet' />";
		$display .= "<script src='/cp/scripts/select2-4.0.3.min.js'></script>";
		$display .= "<script type='text/javascript'>";
		$display .= "$(document).ready(function() {";
		$display .= "$('.select-room').select2({ minimumResultsForSearch: Infinity, theme: 'classic' });";
		$display .= "$('.select-tab').select2({ minimumResultsForSearch: Infinity, theme: 'classic' });";
		$display .= "});";
		$display .= "</script>";

		$display .= "<br><br>".speak("Select Room").": &nbsp;";
		$display .= "<select class='select-room' name='select_room_name' style='width:200px' onchange='window.location = \"inapp_management.php?mode=view_tables&tableid=\" + this.value + \"&".$fwd_vars."\"'>";
		$tcount = 0;
		$tlist_query = lavu_query("select * from tables where `loc_id`='[1]' AND `_deleted` != '1' order by id asc",$locationid);
		while($tlist_read = mysqli_fetch_assoc($tlist_query))
		{
			if((string)$tableid=="0")
			{
				$tableid = $tlist_read['id'];
				$set_title = $tlist_read['title'];
			}
			$display .= "<option value='".$tlist_read['id']."'";
			if($tlist_read['id']==$tableid)
			{
				$display .= " selected";
				$tableid = $tlist_read['id'];
				$set_title = $tlist_read['title'];
			}
			$display .= ">";
			$tcount++;
			$display .= "Room " . $tcount;
			if(trim($tlist_read['title'])!="")
				$display .= " - ";
			$display .= $tlist_read['title']."</option>";
		}
		$display .= "</select>";

		$get_orders = lavu_query("SELECT `id`, `tablename`, `order_id` FROM `orders` WHERE `location_id` = '[1]' AND ((`closed` = '0000-00-00 00:00:00' AND (`tab` = '1' OR `tablename` = 'Quick Serve')) OR (`reopened_datetime` != '' AND `reclosed_datetime` = '')) AND `opened` != '0000-00-00 00:00:00' AND `void` = '0' ORDER BY SUBSTRING(`tablename`, 1, 1) ASC, LENGTH(`tablename`) ASC, `tablename` ASC", $locationid);
		if (mysqli_num_rows($get_orders) > 0) {
			$display .= " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; For Tab/Quick Serve: ";
			$display .= "<select class='select-tab' name='select_tab' onchange='table_clicked(\"Order ID: \" + this.value);'>";
			$display .= "<option value=''></option>";
			while ($order_info = mysqli_fetch_assoc($get_orders)) {
				$display .= "<option value='".$order_info['order_id']."'>".$order_info['tablename']." (".$order_info['order_id'].")</option>";
			}
			$display .= "</select>";
		}

		$display .= "<br><br>";

		//------------------------------ view tables -------------------------------------------//
		$json_cc = req_cc($cc,$cc_companyid);//lsecurity_name($cc,$cc_companyid);

		$host = 'http://127.0.0.1';
		$json_url = $host . '/lib/json_connect.php?cc='.$json_cc.'&usejcinc=7&m=9&loc_id='.$locationid;

		error_log("json url: ".print_r($json_url,1));

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $json_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$contents = curl_exec ($ch);
		$info = curl_getinfo($ch);
		if ($info['http_code'] !== 200) {
			$display .= speak("json_connect HTTP error") . ": {$info['http_code']} ";
		}
		curl_close ($ch);
		//$display .= $json_url;
		//$display.= "The contents are: ". $contents;
		//$display.= "the json_url is:  ". $json_url;
		//mail("niallsc@poslavu.com","grassy stuff", $json_url);
		$tlist = "";
		$cparts = explode("\"open_orders\":\"",$contents);
		if(count($cparts) > 1)
		{
			$cparts = explode("\"",$cparts[1]);
			$tparts = explode("||",$cparts[0]);
			for($i=0; $i<count($tparts); $i++)
			{
				//$tpart = trim($tparts[$i]); // no trim because for some reason some people place spaces in front of the table name
				$tpart = $tparts[$i];
				if($tpart!="")
				{
					if($tlist!="")
						$tlist .= ",";
					$tlist .= "'".str_replace(array("'",'"'), array("&#039;","&quot;"), $tpart)."'";
				}
			}
		}

		//$display.="hello";
		$tcode = "";
		$tcode .= "<div id='root'>";
		$tcode .= "<div id='table_setup_div' style='position:relative; left:0px; top:0px;'>";
		$tcode .= "<table><tr><td id='room_floor' style='width:400px; height:400px;'><table width=\"100%\" height=\"100%\"><td>&nbsp;</td></table></td></tr></table>";
		$tcode .= "<div style=\"position:absolute; left:0px; top:0px; z-index:8080; visibility:hidden\" id=\"info_table_display\"><table><td width='120' height='80' bgcolor=\"#eeeeff\" style='border:solid 1px black;filter:alpha(opacity=80);-moz-opacity:.80;opacity:.80' id=\"info_table_display_content\">Order Info</td></table></div>";
		$tcode .= "</div>";
		$tcode .= "</div>";

		$tcode_js = "";
		$tcode_js .= "<script language='javascript'>";
		$tcode_js .= "table_mode = 'view'; ";
		$tcode_js .= "y_correct = 0; ";
		$tcode_js .= "t_shrink = 2; ";
		$tcode_js .= "t_winwidth = 480; ";
		$tcode_js .= "t_winheight = 480; ";
		$tcode_js .= "t_open_tables = new Array($tlist); ";
		$tcode_js .= "function table_clicked(tablename) { ";
		$tcode_js .= " tablename=encodeURIComponent(tablename); ";
		$tcode_js .= " document.getElementById('t_info').innerHTML = '<iframe frameborder=\"0\" width=\"300\" height=\"400\" src=\"inapp_management.php?simapp=1&cc=$cc&loc_id=$loc_id&mode=table_details&tablename=' + tablename + '\"></iframe>'; ";
		$tcode_js .= "} ";
		$tcode_js .= "</script>";

		//The line below was having issues for many local servers where the file was not actually being imported
		//event though the path was correct, the url did not resolve correctly.
		ob_start();
		  require_once(dirname(__FILE__) . '/resources/table_setup.php');
		$tcode_js .= ob_get_clean();


		$tcode_js .= "<script language='javascript'>";
		//$tcode_js .= "image_path = '".$res_img_path."'; ";
		$tcode_js .= "table_mouseover = 'manage'; ";
		$tbl_query = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '$locationid' and id='$tableid'");
		if(mysqli_num_rows($tbl_query))
		{
			$extract = mysqli_fetch_assoc($tbl_query);
			$set_coord_x = $extract['coord_x'];
			$set_coord_y = $extract['coord_y'];
			$set_shapes = $extract['shapes'];
			$set_widths = $extract['widths'];
			$set_heights = $extract['heights'];
			$set_names = str_replace("'", "&#039;", $extract['names']);

			$tcode_js .= "loadTables('$set_coord_x', '$set_coord_y', '$set_shapes', '$set_widths', '$set_heights', '".str_replace("'", "\'", $set_names)."',t_open_tables); ";
		}
		$tcode_js .= "</script>";

		$display .= "<table><td width='500'>";
		$display .= $tcode;
		$display .= "<td><td width='300' valign='top'>";
		$display .= "<div id='t_info'>&nbsp;</div>";
		$display .= "</td></table>";
		$display .= $tcode_js;
		//$display .= "<br><br>";
		//$display .= "<input type='button' value='Refresh' onclick='window.location.reload(); ' />";
?>
