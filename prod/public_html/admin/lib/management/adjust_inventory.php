<?php
    
    //Needed environment vars:
    //$loc_id
    //$data_name
    $cc = "";
    if( isset($_REQUEST['cc']) ) { 
    	$cc = $_REQUEST['cc']; 
    }
    require_once(dirname(__FILE__).'/../../lib/info_inc.php');
    $isLocalServer = $_SERVER['DOCUMENT_ROOT'] == '/poslavu-local';
    
    //Global Vars
    $web_tree_path;
    $thisPageURL;
    
    if($isLocalServer){
        $web_tree_path = substr( __FILE__, strlen( $_SERVER[ 'DOCUMENT_ROOT' ] ) );
        	$encoding = "";
        if($_SERVER['SERVER_PORT'] == 80 ){
			$encoding = "http";
		} else if ($_SERVER['SERVER_PORT'] == 443 ){
			$encoding = "https";
		}
		$thisPageURL = $encoding . "://" . $_SERVER['SERVER_NAME'] . $web_tree_path;
	}
	else {
		$web_tree_path = substr( __FILE__, strlen( $_SERVER[ 'DOCUMENT_ROOT' ] ) );
		$thisPageURL = "https://admin.poslavu.com$web_tree_path";
	}

	

	
	
    
    $hash_salt = "MR_BEAN";
    $security_hash = sha1(sha1($data_name . $hash_salt . $data_name) . $hash_salt);
    //$suppress_white_bg = true;

    
    /// These will only be set if we are displaying the page within the app.  
    $timeZone;
    $localizedTime;
    ////////////////
    
    
    
    
    //Heap Usage Vars
    $pickedRowsUnit = '';
    
    //  MULTIPLEX  THE  USE  OF  THIS  PAGE.
    if($_POST && isset($_POST['is_ajax_request']) && $_POST['is_ajax_request'] == '1'){
        ENTRY_handle_ajax_request();
    }
    else{
        ENTRY_display_page();
    }
    
    
    
    
    
    //-------------------------ONLY FUNCTIONS BELOW.
    
    
    
    //There are 2 reasons this page would be called.-------------------------------------------
    //First is to display the page, and the second is to
    //handle the ajax requests of the page displayed.
    //  TOP  LEVEL  FUNCTIONS
    function ENTRY_display_page(){
    
        global $_POST, $_SERVER, $display, $timeZone, $localizedTime, $location_info, $thisPageURL;
        
        $timeZone = $location_info['timezone'];
        $localizedTime = localize_datetime(date("Y-m-d H:i:s"),$timez);
        
        //$llsFact = 'Is Local Server: ' . $isLocalServer . ' URL: ' . $thisPageURL;
        
        $display = '';
        //We are displaying the page
        //$display  = '<script type="text/javascript"> alert("'.$hostName.'"); </script>';
        //$display .= "<script type='text/javascript'> alert( 'time zone: $timez    localized date time: $set_time_out '); </script>";
        //$display = '<script type=\'text/javascript\'> alert(\''.$llsFact.'\'); </script>';
        $display .= get_ajax_code_to_insert_for_inventory_amount_change();
        //$display .= get_javascript_debug_div_str_insert();
        //$display .= "<pre style=\"text-align: left; background: white;\">" . print_r($_REQUEST, true)  . "</pre>"; 
        $display .= get_html_js_set_field_handling();
        
        $allIngredientCategories = getAllIngredientCategories();
                
        $display .= getTopHTMLOfAdjustTable();
        foreach($allIngredientCategories as $key => $arrVal){
            if(!isset($arrVal['id']) || $arrVal['id'] =='')
                continue;
            $display .= addIngredientCategoryToInventoryAdjustTable($arrVal);
        }
        $display .= getBottomHTMLOfAdjustTable();
    }
    
    function ENTRY_handle_ajax_request(){
	     global $_POST, $hash_salt;

        //We need lavu_query
        //require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
        //require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
        
        //require_once('./../../cp/resources/core_functions.php');
        
        //We need to setup lavu_query
        
        $posted_coded_data  = $_POST['coded_data'];//Is coded to include all needed info.
        $posted_data_name   = $_POST['data_name'];
        $posted_secur_hash  = $_POST['security_hash'];
        $posted_time_zone   = $_POST['time_zone'];//timeZone
        $posted_location_id = $_POST['location_id'];
        $determined_localized_time = localize_datetime(date("Y-m-d H:i:s"), $posted_time_zone);

        $codedDataArr = explode('_', $posted_coded_data);
        
        $add_or_sub = $codedDataArr[0];
        $ingredient_id = $codedDataArr[1];
        $amount = $codedDataArr[2];
        
        $proper_security_hash = sha1(sha1($posted_data_name . $hash_salt . $posted_data_name) . $hash_salt);

        
        if ($posted_secur_hash == $proper_security_hash && ($add_or_sub == 'add' || $add_or_sub == 'sub')) {
            //We'll do the update next
            if ( $amount!="" ) {
                $incr_decr_query="";
                if ($add_or_sub == 'add') {
                    $incr_decr_query = "UPDATE `[1]`.`ingredients` SET `qty`=`qty` + [2] WHERE `id`='[3]'";
                }
                else if ($add_or_sub == 'sub') {
                    $incr_decr_query = "UPDATE `[1]`.`ingredients` SET `qty`=`qty` - [2] WHERE `id`='[3]'";
                }
                else {
                    echo 'error';
                    exit();
                }
                lavu_query($incr_decr_query, 'poslavu_'.$posted_data_name.'_db', $amount, $ingredient_id);
            } 
            else {
                error_log('MISSING INGREDIENT COUNT in adjust_inventor.php');
                exit();
            }
            
            //We return the amount we found using select.
            $selQuery = "SELECT * FROM `[1]`.`ingredients` WHERE `id`='[2]'";
            $selResult = lavu_query($selQuery, 'poslavu_' . $posted_data_name . '_db', $ingredient_id);
            if(!$selResult){
                error_log('Error in adjust_inventory failed query while handling ajax request:');
                exit();
            }
            $rowArr = mysqli_fetch_assoc($selResult);
            
            //We need to insert into the ingredient_usage table.
            $pos_neg_dir  = ($add_or_sub == 'add') ? -1 : 1;
            $ingrUInsertQ  = "INSERT INTO `[db]`.`ingredient_usage` ";
            $ingrUInsertQ .= "(`ts`,`date`,`orderid`,`itemid`,`ingredientid`,`qty`,`loc_id`,`server_time`,`cost`,`content_id`) ";
            $ingrUInsertQ .= "VALUES ('[ts]','[date]','[orderid]','[itemid]','[ingredientid]','[qty]','[loc_id]','[server_time]','[cost]','[content_id]')";
            $ingrUInsertArr = array();
            $ingrUInsertArr['db'] = 'poslavu_'.$posted_data_name.'_db';
            $ingrUInsertArr['ts'] = time();
            $ingrUInsertArr['date'] = $determined_localized_time;
            $ingrUInsertArr['orderid'] = '';
            $ingrUInsertArr['itemid'] = 0;
            $ingrUInsertArr['ingredientid'] = $ingredient_id;
            $ingrUInsertArr['qty'] = $pos_neg_dir * $amount;
            $ingrUInsertArr['loc_id'] = $posted_location_id;
            $ingrUInsertArr['server_time'] = date('Y-m-d H:i:s');
            $ingrUInsertArr['cost'] = '';
            $ingrUInsertArr['content_id'] = '0';

            $ingrUInsertResult = lavu_query($ingrUInsertQ, $ingrUInsertArr);
            if(!$ingrUInsertResult){
                error_log('MYSQL ERROR in adjust_inventor.php: ' . mlavu_dberror());
                exit();
            }
            
            $returnString  = $rowArr['id'] . '|*|'. $rowArr['qty'] . '|*|';
            $returnString .= $add_or_sub . '|*|' . $determined_localized_time . '|*|' . $posted_location_id . '|*|';
            echo $returnString;
            exit();
        }
    }
    //End of Top-Level (i.e. main) functions----------------------------------------------------
    
    
    
    
    //Secondary functions...
    /*
    function get_javascript_debug_div_str_insert(){
        $str  = "<div id='debug_container'></div>";
        $str .= "<script type='text/javascript'>
                    function AIsetFieldValue(fieldid,fieldval) { 
                        document.getElementById('debug_container').innerHTML = 
                            'field ' + fieldid + ' set to ' + fieldval; 
                        return true;
                    } 
                 </script>";
        return $str;
    }
    */
    function get_html_js_set_field_handling(){
        //ingredient_row_change_quantity( fieldid + '_' + fieldval );
        $str  = '';
        //$str .= "<div id='debug_container'></div>";
        //$str .= "<input type='text' id='user_data_container' READONLY>";
        $str .= "<script type='text/javascript'>
                    
                    function AIsetFieldValue(fieldid,fieldval) {
                        
                        var db_id = fieldid.split('_');
                        db_id = db_id[1];
                        //alert('Field ID: ' + db_id);
                        //<img src=\"management/images/animated-progress.gif\" />
                        
                            //Now we update the qty <td>
                            t_row_id = db_id;
                            t_row_color = '#fff';
                            var dt_leftB = document.getElementById('dt_lbuf_'  + t_row_id);
                            var dt_title = document.getElementById('dt_title_' + t_row_id);
                            var dt_x     = document.getElementById('dt_x_'     + t_row_id);
                            var dt_qty   = document.getElementById('dt_qty_'   + t_row_id);
                            var dt_unit  = document.getElementById('dt_unit_'  + t_row_id);
                            var dt_add   = document.getElementById('dt_add_'   + t_row_id);
                            var dt_sub   = document.getElementById('dt_sub_'   + t_row_id);
                            dt_leftB.style.backgroundColor = t_row_color;//'green';
                            dt_title.style.backgroundColor = t_row_color;//'green';
                            dt_x.style.backgroundColor     = t_row_color;//'green';
                            dt_qty.style.backgroundColor   = t_row_color;//'green';
                            dt_unit.style.backgroundColor  = t_row_color;//'green';
                            dt_add.style.backgroundColor   = t_row_color;//'green';
                            dt_sub.style.backgroundColor   = t_row_color;//'green';
                        
                        
                        document.getElementById('dt_qty_' + db_id).innerHTML = '<img src=\"management/images/animated-progress.gif\" width=\"24\"/>';
                        //document.getElementById('user_data_container').value = fieldid + '_' + fieldval;
                        window.setTimeout( 
                            function() { 
                                ingredient_row_change_quantity( fieldid + '_' + fieldval ); 
                            }, 100);
                        return true;
                        
                    }
                 </script>";
        return $str;
    }
    
    
    function ai_use_number_pad_if_available($field_id, $title, $type) {
		global $available_DOcmds, $pickedRowsUnit;
		
		//mail("BDennedy@gmail.com", 'available dos', print_r($available_DOcmds, 1));
		
		$str = "";
		if (in_array("number_pad", $available_DOcmds)) {
		      $str = " READONLY onclick='window.location = \"_DO:cmd=number_pad&type=" .     
		          $type ."&max_digits=15&title=".rawurlencode($title) .
		          "&js=AIsetFieldValue(%27".$field_id."%27,%27ENTERED_NUMBER%27)\"'";
        }
		return $str;
	}
    
    
    // VIEW....   CREATING THE TABLE WITH 3 STEPS.
    // 1.) THE TOP OF THE TABLE
    function getTopHTMLOfAdjustTable(){
        global $fwd_vars;
        /*
        $_HTML_STRING = "
            <table border=\"1\"> 
                <tbody> ";
        */
        $_HTML_STRING = "
            <style>
                table { font-size:18px; }
            </style>
           <table cellpadding=10>
                <tr>
                    <td width='10%'>&nbsp;</td><td align='center'>
                    <!--<input type='button' value='<<" . speak("Management Menu") . "' onclick='window.location = \"inapp_management.php?mode=menu&".$fwd_vars."\"'>
                    <br><br><br>-->
            <table cellspacing=0 cellpadding=6> 
                <tbody> ";
        return $_HTML_STRING;
    }
    // 2.)  ON EACH INGREDIENT_CATEGORY ITERATION
    function addIngredientCategoryToInventoryAdjustTable($ingredient_category_row){
        $title = $ingredient_category_row['title'];
        $id = $ingredient_category_row['id'];
        $loc_id = $ingredient_category_row['loc_id'];
        
        $ingredientsForThisCategory = getAllIngredientsBelongingToIngredientCategory($id);
        $_HTML_STRING = '';
        
        //We add a title row
        $_HTML_STRING .= "<tr><td colspan='80' bgcolor='#000' align='center' style='font-weight:bold'> <font color='#FFF'>$title</font></td></tr>";
        
        //Programmatically adding <tr>'s//
        foreach($ingredientsForThisCategory as $key => $arrVal){
            if(!isset($arrVal['title']) || $arrVal['title'] == '')
                continue;
            $_HTML_STRING .= ingredientRow2HTMLTableRow($arrVal);             
        }
        $_HTML_STRING .= "<tr><td colspan='80' style='border-top:solid 1px #AAA'>&nbsp;</td></tr>";          

        return $_HTML_STRING;
    }
    // 3.)  ENDING THE TABLE
    function getBottomHTMLOfAdjustTable(){
        $_HTML_STRING = "
                </tbody>        
            </table>
            </td><td width='10%'>&nbsp;</td></tr></table>
        ";
        return $_HTML_STRING;
    }

    $bd_odd_even_flip = true;
    function ingredientRow2HTMLTableRow($ingredientRow){
        global $debug_count, $bd_odd_even_flip;
        $id    = $ingredientRow['id'];
        $title = $ingredientRow['title'];
        $qty   = $ingredientRow['qty'];
        $unit  = $ingredientRow['unit'];
        
        $add_onclick = ai_use_number_pad_if_available("add_$id", "Add: $title ($unit)", "decimal");
        $sub_onclick = ai_use_number_pad_if_available("sub_$id", "Subtract: $title ($unit)", "decimal");
        //id='".$id."_plus"   . "' 
        //id='".$id."_minus"  . "'
        
        $bgColorStr = $bd_odd_even_flip ? "bgcolor='#EEE'" : "bgcolor='#DDD'";
        $bd_odd_even_flip = !$bd_odd_even_flip;
        
        $returnStr = 
            "<tr $bgColorStr> 
                <td $bgColorStr id='".'dt_lbuf_' .$id."' style='border-left:solid 1px #AAA'>&nbsp;</td>
                <td $bgColorStr id='".'dt_title_'.$id."' align='right' style='color:#333'>$title</td>
                <td $bgColorStr id='".'dt_x_'    .$id."' style='color:#006600' align='right' cellpadding='0'>x</td>
                <td $bgColorStr id='".'dt_qty_'  .$id."' style='color:#006600'> $qty</td>
                <td $bgColorStr id='".'dt_unit_' .$id."' style='color:#000066'>$unit</td>
                <td $bgColorStr id='".'dt_sub_'  .$id."' style='border-left:solid 1px #AAA; border-top:solid 1px #AAA'>
                        <input type='button' style='font-size:24px; font-weight:bold; color:#444' value='-' $sub_onclick> </td>
                <td $bgColorStr id='".'dt_add_'  .$id."' style='border-left:solid 1px #AAA; border-top:solid 1px #AAA; border-right:solid 1px #AAA'>
                        <input type='button' style='font-size:24px; font-weight:bold; color:#444' value='+' $add_onclick> </td>            
            </tr>";
        return $returnStr;    
    }

    function getAllIngredientCategories(){
        global $loc_id;
        $queryStr  = "SELECT * FROM `ingredient_categories` WHERE ";
        $queryStr .= "(`_deleted`='0' OR `_deleted`='') AND `loc_id`='[1]' ORDER BY `title` ASC";
        $result = lavu_query($queryStr, $loc_id);
        if(!$result){
            error_log("Malformed Query in ..management/adjust_inventory.php");
            return null;    
        }
        $allIngredientCategories = array();
        while($currIngredientCategory = mysqli_fetch_array($result)){
            $allIngredientCategories[$currIngredientCategory['id']] = $currIngredientCategory;
        }
        return $allIngredientCategories;
    }
    
    //Returns a 2D array where the outer array: ( ingredient_id => array(ingredient_row) ).    
    function getAllIngredientsBelongingToIngredientCategory($ingredientCategoryID){
        $queryStr  = "SELECT * FROM `ingredients` WHERE `category`='[1]' ";
        $queryStr .= "AND (`_deleted`='0' OR `_deleted`='') ORDER BY `title` ASC";
        $result = lavu_query($queryStr, $ingredientCategoryID);
        if(!$result){
            //Handle sqlErrorHere
            error_log("Malformed Query in ..management/adjust_inventory.php");
            return null;
        }
        $allIngredientsFromCategoryArr = array();
        while($currRow = mysqli_fetch_array($result)){
            $allIngredientsFromCategoryArr[$currRow['id']] = $currRow;
        }
        return $allIngredientsFromCategoryArr;
    }
    
    function get_ajax_code_to_insert_for_inventory_amount_change(){
        
        global $thisPageURL, $data_name, $security_hash, $timeZone, $loc_id, $cc;
        $urlEncodedTimeZone = urlencode($timeZone);
        /*
            $timeZone = $location_info['timezone'];
            $localizedTime = localize_datetime(date("Y-m-d H:i:s"),$timez);
        */
        $ajax_code = "
            <script type='text/javascript'>
                
                var ajax_date_append = new Date();
                ajax_date_append = ajax_date_append.getTime();
                //alert('ajax_date_append: ' + ajax_date_append);
                
                function ingredient_row_change_quantity(inventory_adjust_data){
                    var xmlhttp;
                    if (window.XMLHttpRequest){
                        xmlhttp=new XMLHttpRequest();
                    }else{
                        xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");
                    }
                    
                    xmlhttp.onreadystatechange=function(){
                        //alert('ready state:' + xmlhttp.readyState + '  status:' + xmlhttp.status);
                        
                        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                            //alert('got ajax response:' + xmlhttp.responseText);
                   
                            //id = qty_(id)
                            //alert('Ajax Call Response Text: ' + xmlhttp.responseText);
                            var returnedVal = xmlhttp.responseText;
                            //alert('Response: ' + returnedVal);
                            returnedArr = returnedVal.split('|*|');
                            var t_row_id  = returnedArr[0];
                            var t_row_qty = returnedArr[1];
                            var t_add_sub = returnedArr[2];
                            var t_row_color = (t_add_sub == 'sub') ? '#FFC' : '#BFB';
                            //Now we update the qty <td>
                            var dt_leftB = document.getElementById('dt_lbuf_'  + t_row_id);
                            var dt_title = document.getElementById('dt_title_' + t_row_id);
                            var dt_x     = document.getElementById('dt_x_'     + t_row_id);
                            var dt_qty   = document.getElementById('dt_qty_'   + t_row_id);
                            var dt_unit  = document.getElementById('dt_unit_'  + t_row_id);
                            var dt_add   = document.getElementById('dt_add_'   + t_row_id);
                            var dt_sub   = document.getElementById('dt_sub_'   + t_row_id);
                            dt_qty.innerHTML = t_row_qty;
                            dt_leftB.style.backgroundColor = t_row_color;//'green';
                            dt_title.style.backgroundColor = t_row_color;//'green';
                            dt_x.style.backgroundColor     = t_row_color;//'green';
                            dt_qty.style.backgroundColor   = t_row_color;//'green';
                            dt_unit.style.backgroundColor  = t_row_color;//'green';
                            dt_add.style.backgroundColor   = t_row_color;//'green';
                            dt_sub.style.backgroundColor   = t_row_color;//'green';
                        }
                    }
                    //alert('Going to:$thisPageURL');
                    var bd_ajax_url = '$thisPageURL' + '?klax_wave=' + ajax_date_append;
                    //alert(bd_ajax_url);
                    xmlhttp.open('POST', bd_ajax_url, true);
                    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                    //alert('Inventory data being sent: ' + inventory_adjust_data);
                    

                    var post_params  = '';
                    post_params += 'is_ajax_request=1&coded_data=' + inventory_adjust_data;
                    post_params += '&data_name=$data_name&security_hash=$security_hash';
                    post_params += '&time_zone=$urlEncodedTimeZone&location_id=$loc_id&cc={$cc}';
                    xmlhttp.send(post_params);
                }
            </script>
        ";
        //$ajax_code = "<script type='text/javascript'> alert('marker'); </script>";
        return $ajax_code;
    }
    
?>
