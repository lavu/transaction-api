<?php
function lbr_print_display_money($v)
{
	$leading_char = "$";
	$dec_places = 2;
	$v = str_replace(",","",str_replace("", $leading_char, $v)) * 1;
	$str = $leading_char . number_format($v, $dec_places);
	return $str;
}
function lbr_print_showval($v, $mode="display")
{
	if(substr($v,0,7)=="[money]")
	{
		if($mode=="actual")
		{
			return str_replace(",","",substr($v,7));
		}
		$v = lbr_print_display_money(substr($v,7));
	}
	if(substr($v,0,5)=="time(")
	{
		$v_parts = explode(")",substr($v,5),2);
		if(count($v_parts)==2)
		{
			$tdisplay = trim($v_parts[0]);
			if($mode=="actual") return trim($v_parts[1]);
			$v = date($tdisplay,strtotime(trim($v_parts[1])));
		}
	}
	return $v;
}
function lbr_print_val($v)
{
	return lbr_print_showval($v,"actual");
}
function sales_pulling_data($dbname, $vars){
	$query = mlavu_query("select
		 concat('time(h:ia)',date_format(order_contents.device_time, '%Y-%m-%d %H:00')) as item_added_hour,
		 sum(order_contents.quantity) as quantity,
		 if(count(distinct orders.ioid )>count( distinct orders.order_id ),count( distinct orders.ioid ),count( distinct orders.order_id )) as order_count,
		 if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) ),2)), '[money]0.00') as gross,
		 if(count(*) > 0, concat('[money]',format(sum(if(order_contents.quantity > 0, order_contents.discount_amount + order_contents.idiscount_amount, 0)),2)), '[money]0.00') as discount,
		 if(count(*) > 0, concat('[money]',format(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',order_contents.discount_amount + order_contents.idiscount_amount + order_contents.itax, 0)),2)), '[money]0.00') as net,
		 if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5` - `order_contents`.`itax`, 0 ) ),2)), '[money]0.00') as order_tax,
		 if(count(*) > 0, concat('[money]',format(sum( if( order_contents.quantity != 0 AND order_contents.tax_exempt != '1', `order_contents`.`itax`, 0) ),2)), '[money]0.00') as included_tax,
		 concat('time(h:ia)',date_format(order_contents.device_time, '%Y-%m-%d %H:00')) as `group_col_date_formatted`
		 from `$dbname`.`orders` LEFT JOIN `$dbname`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id`
		 where orders.opened >= '[min_datetime]'
		 and orders.opened < '[max_datetime]'
		 and orders.void = '0' and orders.location_id='[loc_id]'
		 and order_contents.item != 'SENDPOINT'
		 group by concat('time(h:ia)',
		 date_format(order_contents.device_time, '%Y-%m-%d %H:00'))", $vars);
	return $query;
}
function clock_punches_data($dbname, $vars){
	$query = mlavu_query("select * from `$dbname`.`clock_punches`
			where `time`>='[min_datetime_check]' and
			`server_time`!='' and `_deleted`!='1' and
			(`time_out`='' or `time_out`>'[min_datetime]') and
			`time`<'[max_datetime]' and `location_id`='[loc_id]'
			and `server`!='' and `punch_type`='Shift' order by `server_time` asc", $vars);
	return $query;
}
?>
