<?php
		//$display .= "<u>Order Management: Transfers</u>";

		$tablename = getvar("tablename");
		echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>";
		echo "<script src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>";
		echo "<link href='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' />";
		echo "<style>";
		echo "  bodu, table { font-family: Arial, Verdana; font-size: 12px; } ";
		echo "  .select2-container, .select2-dropdown, .select2-results { font-family: Arial, Verdana; font-size: 12px !important; } ";
		echo "</style>";
		echo "<body>";

		if($tablename)
		{

			$transfer_to = getvar("transferto");
			if($transfer_to)
			{
				require_once(dirname(__FILE__)."/../jcvs/jc_functions.php");
				if (!isset($location_info)) $location_info = array();
				$device_time = determineDeviceTime($location_info, $_REQUEST);

				$orderid = getvar("orderid");
				$transfer_id = getvar("transferid");
				$utc_time = gmdate("U");

				lavu_query("update `orders` set `server_id`='[1]', `server`='[2]', `last_modified`='[3]', `last_mod_device`='system' ,`last_mod_ts`='$utc_time', `pushed_ts`='$utc_time' where `id`='[4]'",$transfer_id,$transfer_to,$device_time,$orderid);
			}

			$locationid = $loc_id;
			$json_cc = req_cc($cc, $cc_companyid);//$cc;//lsecurity_name($cc,$cc_companyid);
			$host = 'http://127.0.0.1';
			$json_url = $host . '/lib/json_connect.php';
			$tablevar = '&table='.urlencode($tablename);
			if (strstr($tablename, "Order ID: ")) $tablevar = '&order_id='.str_replace("Order ID: ", "", $tablename);
			$json_vars = 'cc='.$json_cc.'&usejcinc=7&m=4&loc_id='.$locationid.$tablevar;
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $json_url . '?' . $json_vars);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$contents = curl_exec ($ch);
			curl_close ($ch);

			$tinfo = lavu_json_decode($contents);
			$tinfo = $tinfo[0];
			//echo "<br><br>" . str_replace("<","&lt;",$contents);

			if($tinfo['json_status']=="success")
			{
				$order_info = $tinfo['order_info'][0];

				echo "<table cellpadding=16><td style='border:solid 3px #aaaabb' bgcolor='#e7e7ee'>";
				echo "<table><td width='140' height='60' align='center' valign='top' style='cursor:default'>";
				echo "<b>";
				echo $tablename . "<br>";
				echo $order_info['guests'] . " " . speak('Guests');
				echo "<br>".speak("Server").": " . $order_info['server'];
				echo "<br>".speak("Total").": " . $order_info['total'];
				echo "</b>";
				echo "<br><br>";
				$orderid = $order_info['id'];
				
				echo "<script type='text/javascript'>";
				echo "$(document).ready(function() {";
				echo "$('.select-transfer-table').select2({ minimumResultsForSearch: Infinity, theme: 'classic' });";
				echo "});";
				echo "</script>";
				$tablename=rawurlencode($tablename);
				echo "<select class='select-transfer-table' onchange='window.location = \"inapp_management.php?simapp=1&cc=$cc&loc_id=$loc_id&mode=table_details&tablename=$tablename&orderid=$orderid&\" + this.value'>";
				echo "<option value=''>Transfer Table To</option>";

				$server_query = lavu_query("select * from `users` WHERE (`loc_id` = '[1]' OR `loc_id` = '0') AND `_deleted` != '1' AND `active`<>'0' order by `username` asc", $locationid);
				while($server_read = mysqli_fetch_assoc($server_query))
				{
					echo "<option value='transferid=".$server_read['id']."&transferto=".trim($server_read['f_name'])."'>".trim($server_read['f_name']." ".$server_read['l_name'])."</option>";
				}

				echo "</select>";

				echo "</td></table>";
				echo "</td></table>";
			}
		}

		echo "</body>";
		exit();
?>
