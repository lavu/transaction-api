<?php


	$devBackupStr = strpos(__FILE__, "/dev/") ? "../" : "";
	require_once(dirname(__FILE__)."/../../".$devBackupStr."cp/resources/json.php");

	#require_once(dirname(__FILE__)."/../../".$devBackupStr."v2/resources/core_functions.php");
	/*
		USING `cash_data` TO HOLD DAILY LOGS.
		FIELDS:
			--date:        Fiscal date the log belongs to.
			--server_id:   Employee id who first started the log
			--value_med:  The data structure, AKA the log object:
			[
				{ 'f_name':'...', 'l_name':'...', 'datetime':'...', 'message':'' },
				{ 'f_name':'...', 'l_name':'...', 'datetime':'...', 'message':'' }
				...
			]
	 */

	$userRow = pullUserRow();
	createNewLogRowIfDoesNotExist();
	$logRowFromCashData = pullFiscalTodayLogRow();
	handleUpdatePostBack($logRowFromCashData);
	$logRowFromCashData = pullFiscalTodayLogRow();
	$logObjectStr = $logRowFromCashData['value_med'];
	$logObject = array();
	if(!empty($logObjectStr)){
		$logObject = LavuJson::json_decode($logObjectStr, 1);
	}else{
		$logObject = array();
	}

	$logDisplayString = stringifyLogObjectForDisplay($logRowFromCashData, $logObject);
	$display = getDisplayPage($logDisplayString);
	echo "<script> setTimeout( function(){ window.location = '_DO:cmd=set_scrollable&set_to=0'; }, 400 ); </script>";

	function handleUpdatePostBack(&$logRow){
		$loc_row = getLocationRow();
		$user_row = pullUserRow();
		if( empty($_REQUEST['is_posting']) ){ return; }
		$logMessageToAdd = $_REQUEST['log_text_input'];
		$logMessagesOnRowJSONStr = $logRow['value_med'];
		$serversLogMessagesOfToday = !empty($logMessagesOnRowJSONStr) ? LavuJson::json_decode($logMessagesOnRowJSONStr, 1) : array();
		$newMessageEntryArr = array();
		$newMessageEntryArr["localized_date_time"] = localize_datetime(date('Y-m-d H:i:s'), $loc_row['timezone']);
		$newMessageEntryArr["message"] = $logMessageToAdd;
		$newMessageEntryArr["f_name"] = $user_row['f_name'];
		$newMessageEntryArr["l_name"] = $user_row['l_name'];
		$serversLogMessagesOfToday[] = $newMessageEntryArr;
		$newJSONStr = LavuJson::json_encode($serversLogMessagesOfToday);
		$result = lavu_query("UPDATE `cash_data` SET `value_med`='[1]' WHERE `id`='[2]'", $newJSONStr, $logRow['id']);
	}

	function pullUserRow(){
		static $user_row = null;
		if(!empty($user_row))
			return $user_row;
		$result = lavu_query("SELECT * FROM `users` WHERE `id`='[1]'", $_REQUEST['server_id']);
		$user_row = mysqli_fetch_assoc($result);
		return $user_row;
	}

	function createNewLogRowIfDoesNotExist(){
		$todaysLogRowForUser = pullFiscalTodayLogRow();
		if(!empty($todaysLogRowForUser)){ return; }
		$todaysFiscalDate = getTodaysFiscalDate();
		$usersRow = pullUserRow();
		$insertArr = array();
		$insertArr['date'] = $todaysFiscalDate;
		$insertArr['server_id'] = $_REQUEST['server_id'];
		$insertArr['device_time'] = $_REQUEST['device_time'];
		$insertArr['type'] = "log";
		$insertArr['server_time'] = date('Y-m-d H:i:s');
		$insertArr['setting']='employee_log';
		$insertArr['loc_id']=$_REQUEST['loc_id'];
		$allKeys = array_keys($insertArr);
		$insertKeys = "(`"  . implode("`,`",$allKeys)   . "`)";
		$insertVals = "('[" . implode("]','[",$allKeys) . "]')";
		$result = lavu_query("INSERT INTO `cash_data` $insertKeys VALUES $insertVals", $insertArr);
		if(!$result) {error_log("Error in ".__FILE__." -vw41x34- error:".lavu_dberror());}
	}

	function pullFiscalTodayLogRow(){
		$todaysFiscalDate = getTodaysFiscalDate();
		$result = lavu_query("SELECT * FROM `cash_data` WHERE `setting`='employee_log' AND `date`='[1]'", $todaysFiscalDate);
		$logRow = false;
		if(mysqli_num_rows($result)){
			$logRow = mysqli_fetch_assoc($result);
		}
		return $logRow;
	}

	function getTodaysFiscalDate(){
		static $fiscalDate = null;
		if(!empty($fiscalDate))
			return $fiscalDate;
		$result = lavu_query("SELECT * FROM `locations`");
		$loc_row = mysqli_fetch_assoc($result);
		$deviceTimeWithoutDate = substr($_REQUEST['device_time'], 11);
		$deviceDateWithoutTime = substr($_REQUEST['device_time'], 0, 10);
		$fiscalDate = "";
		$dayStartEndTime = substr($loc_row['day_start_end_time'], 0, 2).':'.substr($loc_row['day_start_end_time'],3,2);
		if( $deviceTimeWithoutDate < $dayStartEndTime){
			$fiscalDate = date('Y-m-d', strtotime("- 1 day", strtotime($deviceDateWithoutTime)));
		}else{
			$fiscalDate = $deviceDateWithoutTime;
		}
		return $fiscalDate;
	}


	function stringifyLogObjectForDisplay(&$logRow, &$logObjectArr){
		$htmlStrBuilder = "";
		//$htmlStrBuilder .= "<span style='font-size:20px;font-style:italics;'>" . date('D n/j/Y') . "</span> <br>";

		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );
		$change_format = explode("-", $today);
		switch ($date_format['value']){
			case 1:$htmlStrBuilder .= "<span style='font-size:20px;font-style:italics;'>" . date('D j/n/Y') . "</span> <br>";
			break;
			case 2:$htmlStrBuilder .= "<span style='font-size:20px;font-style:italics;'>" . date('D n/j/Y') . "</span> <br>";
			break;
			case 3:$htmlStrBuilder .= "<span style='font-size:20px;font-style:italics;'>" . date('D Y/n/j') . "</span> <br>";
			break;
		}

		$reverseOrderLogObjectArr = array_reverse( $logObjectArr );

		$htmlStrBuilder .= "<table cellpadding=4 cellspacing=0>";
		foreach($reverseOrderLogObjectArr as $currEntry){
			$formattedDate = date("g:ia", strtotime($currEntry['localized_date_time']) );
			//$htmlStrBuilder .= $formattedDate . ": " . "<i>" . $currEntry['message'] . "</i><br>";
			//$htmlStrBuilder .= "<tr><td>".$formattedDate."</td><td>".$currEntry['message']."</td></tr>";
			$fullName = $currEntry['f_name'] . " " . $currEntry['l_name'];
			$htmlStrBuilder .= "<tr><td style='text-align:left;'><span style='font-size:15px; text-align:right;'>".$formattedDate."</span></td><td><span style='font-weight:bold'>".$fullName.":</span> ".
			                   "<span style='font-style:italic'>".$currEntry['message']."</span></td></tr>";
		}
		$htmlStrBuilder .= "</table>";
		return $htmlStrBuilder;
	}

	function getLocalizedDatetime(){
		$locationRow = getLocationRow();
		return localize_datetime(date('Y-m-d H:i:s'), $locationRow['timezone']);
	}

	function getLocationRow(){
		static $locationRow = null;
		if(!empty($locationRow)){ return $locationRow; }
		$result = lavu_query("SELECT * FROM `locations`");
		if($result)
			$locationRow = mysqli_fetch_assoc($result);
		return $locationRow;
	}

	function getDisplayPage($displayString){
			ob_start();
			?>
			<html>
				<style type='text/css'>
					body {
						background-color:transparent;
					}
					#background_div {
						width:800px;
						height:400px;
						/*background-color:gray;*/
					}
					#input_text_id {
						width:700px;
						height:10%;
						font-size:16px;
					}
					#display_logs_div_id {
						background-color:#EEEEEE;
						width:700px;
						height:90%;
						padding:20px;
						border:solid 1px #aaaaaa;
						text-align:left;
						overflow-y: scroll;
					}
				</style>
				<body>
					<form method='post' action=''>
						<div id='background_div'>
							<input type='hidden' name='is_posting' value='1' />
							<input type='hidden' name='time' value='' />
							<input type='text' name='log_text_input' id='input_text_id' placeholder='<?php speak("Type New Message Here"); ?>....'>
							</input>
                            <br><br>
							<div id='display_logs_div_id'>
								<?php echo $displayString; ?>
							</div>
						</div>
					</form>
				</body>
			</html>
			<?php
		return ob_get_clean();
	}
?>
