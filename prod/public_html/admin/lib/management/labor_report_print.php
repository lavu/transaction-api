<?php
	require(dirname(__FILE__) . "/../../cp/resources/core_functions.php");
	require(dirname(__FILE__) . "/../../cp/reports_v2/report_functions.php");
	require(dirname(__FILE__) . "/../../cp/reports_v2/area_reports.php");
	require(dirname(__FILE__) . "/labor_print_functions.php");
	$loc_id				= reqvar("loc_id", "");
	$mode				= reqvar("m", "");
	$register			= reqvar("register", "receipt");
	$vars_str = (isset($_REQUEST['report_type']))?$_REQUEST['report_type']:"";
	if ($vars_str != "") {
		$var_parts = explode("(eq)", $vars_str);
	}
	$datetime			= reqvar("datetime", "");
	$datetime = strtotime($datetime);
	//$date_format = date("d/m/Y H:i:s", $datetime);
	$date_value = $_GET['dn'];
	//echo $date_value; exit;

	 switch ($date_value){
	 case 1:
	 $date_format = date("d/m/Y H:i:s", $datetime);
	 break;
	 case 2:
	 $date_format = date("m/d/Y H:i:s", $datetime);
	 break;
	 case 3:
	 $date_format = date("Y/m/d H:i:s", $datetime);
	 break;
	 }

	if($var_parts[1] == 'by_the_day'){
		$arep_vars = array("base_url"=>"/lib/management/labor_report_print.php");
		$arep = new area_reports($arep_vars);
		$arep->report_id = "autodirect:labor_report_print";
		if(strpos($_SERVER['REQUEST_URI'],"widget")===false) $_GET['mode'] = "day";
		$arep->report_selection = "Preset Date Ranges";
		$arep_result = $arep->datetime_selector();
		$day_start_datetime = $arep_result['vars']['start_datetime'];
		$day_end_datetime = $arep_result['vars']['end_datetime'];
	
		if(strtotime($day_end_datetime) - strtotime($day_start_datetime) > 60 * 60 * 24 * 45)
		{
			$special_message = "Sorry, cannot run this report for more than a 45 day span";
			$day_start_datetime = date("Y-m-d") . " 00:00:00";
			$day_end_datetime = date("Y-m-d") . " 00:00:01";
		}
	
		$day_start_ts = strtotime($day_start_datetime);
		$day_end_ts = strtotime($day_end_datetime);
	
		$day_start_datetime_check = date("Y-m-d H:i:s",strtotime($day_start_datetime) - (60 * 60 * 24));
	
		$vars = array();
		$vars['loc_id'] = $loc_id;
		$vars['min_datetime'] = $day_start_datetime;
		$vars['min_datetime_check'] = $day_start_datetime_check;
		$vars['max_datetime'] = $day_end_datetime;
		$dinfo = array();
		$dbname = "poslavu_".$dataname."_db";
		$sales_query = sales_pulling_data($dbname, $vars);
		$dinfo['sales_total'] = '$0.00';
		while($sales_read = mysqli_fetch_assoc($sales_query))
		{
			$h_ts = strtotime(lbr_print_val($sales_read['item_added_hour']));
			$d_index = date("Y-m-d", $h_ts);
	
			$dinfo['sales_total'] += lbr_print_val($sales_read['net']);
		}
		$min_week_ago = date("Y-m-d",strtotime($vars['min_datetime']) - (60 * 60 * 24 * 7)) . " 00:00:00";
		$vars['min_week_ago'] = $min_week_ago;
		$dinfo['labor_hours'] = '0';
		$dinfo['labor_cost'] = '$0.00';
		$clock_hours = array();
		$clock_query = clock_punches_data($dbname, $vars);
		while($clock_read = mysqli_fetch_assoc($clock_query))
		{
			$punch_start_ts = strtotime($clock_read['time']);
			if($clock_read['time_out']=="") $punch_end_ts = time();
			else $punch_end_ts = strtotime($clock_read['time_out']);
	
			if($punch_end_ts > $punch_start_ts && $punch_end_ts < $punch_start_ts + (60 * 60 * 24 * 7))
			{
				for($t=$punch_start_ts; $t<=$punch_end_ts+(60*60); $t+=(60*60))
				{
					$hour_start_datetime = date("Y-m-d H",$t) . ":00:00";
					$hour_end_datetime = date("Y-m-d H",$t) . ":59:59";
					$hour_start_ts = strtotime($hour_start_datetime);
					$hour_end_ts = strtotime($hour_end_datetime);
	
					if($hour_start_ts > $punch_end_ts)
					{
						$hour_add = 0;
					}
					else if($hour_end_ts < $punch_start_ts)
					{
						$hour_add = 0;
					}
					else if($punch_start_ts <= $hour_start_ts && $punch_end_ts >= $hour_end_ts)
					{
						$hour_add = 1;
					}
					else if($punch_start_ts <= $hour_start_ts && $punch_end_ts < $hour_end_ts)
					{
						$hour_add = number_format(($punch_end_ts - $hour_start_ts) / 60 / 60,2,".","");
						if($hour_add * 1 < 0)
						{
							echo date("Y-m-d H:i:s",$hour_start_ts) . " - " . date("Y-m-d H:i:s",$punch_end_ts) . "<br>";
						}
					}
					else if($punch_start_ts > $hour_start_ts && $punch_end_ts >= $hour_end_ts)
					{
						$hour_add = number_format(($hour_end_ts - $punch_start_ts) / 60 / 60,2,".","");
					}
					else if($punch_start_ts > $hour_start_ts && $punch_end_ts < $hour_end_ts)
					{
						$hour_add = number_format(($punch_end_ts - $punch_start_ts) / 60 / 60,2,".","");
					}
					else
					{
						$hour_add = 0;
					}
					$cost_add = $clock_read['payrate'] * $hour_add;
	
					$dindex = date("Y-m-d",$t);
					
					$server_id = $clock_read['server_id'];
	
					$dinfo['labor_hours'] += $hour_add * 1;
					$dinfo['labor_cost'] += $cost_add * 1;
				}
			}
		}
		if($dinfo['sales_total'] * 1 > 0 && $dinfo['labor_cost'] * 1 > 0)
		{
			$labor_calc = number_format(($dinfo['labor_cost'] * 1 / $dinfo['sales_total'] * 1) * 100,2) . "%";
		}
		else if($dinfo['sales_total'] * 1 > 0)
		{
			$labor_calc = "0%";
		}
		else if($dinfo['labor_cost'] * 1 > 0)
		{
			$labor_calc = "---";
		}
		else
		{
			$labor_calc = "---";
		}

		$date_time = str_replace(":","[c",$date_format);
		$output_string = $register;
		$output_string .= ":---------------------------";
		$output_string .= "As of[c ".$date_time;
		$output_string .= ":".speak("Sales");
		$output_string .= ":".lbr_print_display_money($dinfo['sales_total']).":";
		$output_string .= ":".speak("Labor Hours");
		$output_string .= ":".$dinfo['labor_hours'].":";
		$output_string .= ":".speak("Labor Cost")." %";
		$output_string .= ":".$labor_calc;
		echo "1|".$output_string;
		exit();
	}
?>