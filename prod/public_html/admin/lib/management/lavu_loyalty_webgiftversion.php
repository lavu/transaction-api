<?php
	$devBackupStr = strpos(__FILE__, "/dev/") ? "../" : "";
	require_once(dirname(__FILE__)."/../../".$devBackupStr."cp/resources/json.php");
	require_once(dirname(__FILE__)."/../../components/payment_extensions/extfunctions.php");
	require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
	$imagePath = strpos(__FILE__, "/dev/")  ? "../../../dev/components/payment_extensions/lavugift/images" : "../../../components/payment_extensions/lavugift/images";
	$point_adjustment = postvar('point_adjustment', 0);
	$balance_adjustment = postvar('balance_adjustment', 0);
	$new_exp_date = postvar('new_exp_date', -1);
	$reset = postvar('reset', 0);
	$register_card = postvar('register_card', 0);
	$server_id = save_reqvar("server_id");
	$server_name = save_reqvar("server_name");
	$management_session = $_SESSION['management_session'];
	if($point_adjustment == '') $point_adjustment = 0;
	if($balance_adjustment == '') $balance_adjustment = 0;

	$on_ipad = false;
	if(isset($_SESSION['available_DOcmds']))
	{
		$on_ipad = true;
		$available_DOcmds = $_SESSION['available_DOcmds'];
	}

	if(!isset($_POST['card_num']) || $reset) {
		$display = getCardEnterPage();
		$card_num = "";
	}

	else {
		$card_num = scrubInput($_POST['card_num']);
		$display = getCardModifyPage();
	}
	if($on_ipad){
		echo "<script> setTimeout( function(){ window.location = '_DO:cmd=set_scrollable&set_to=0'; }, 400 ); </script>";
	}

	function scrubInput($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	function getStyleSheet() {
	global $imagePath;
		ob_start();
		if(canModify()) { 
					$bgdiv_width = "800px"; 
					$input_text_width = "700px";
					$bgdiv_height = "450px";
					$paddingTop=" padding-top:20px;";
					$bgDivColor=" #eeeeee";
				}
				else { 
					$bgdiv_width = "100%"; 
					$bgdiv_height = "100%";
					$input_text_width = "200px";
					$paddingTop="";
					$bgDivColor=" #FFFFFF";
				}
		?>
			<style type='text/css'>
				#background_div {
					width:<?php echo $bgdiv_width?>;
					height:<?=$bgdiv_height;?>;
					/*background-color:<?=$bgDivColor;?>;*/
					border-radius: 10px;
					<?=$paddingTop?>
				}
				#input_text_id {
					width:<?php echo $input_text_width?>;
					height:10%;
					font-size:16px;
				}
				.btn_light {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:80px;
					height:36px;
					background:url(<?php echo '"'.$imagePath.'/btn_wow_80x37.png"' ?>);
					border:hidden;
					padding-top:3px;
				}

				.btn_light_short {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:37px;
					height:36px;
					background:url(<?php echo '"'.$imagePath.'/btn_wow_37x37.png"' ?>);
					border:hidden;
					padding-top:1px;
				}

				.btn_light_long {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:167px;
					height:37px;
					background:url(<?php echo '"'.$imagePath.'/btn_wow_167x37.png"' ?>);
					border:hidden;
					padding-top:2px;
				}

				.btn_light_super_long {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:200px;
					height:37px;
					background:url(<?php echo '"'.$imagePath.'/btn_wow_250x37.png"' ?>);
					border:hidden;
					padding-top:2px;
					padding-left:8px;
					text-align: center;
				}

				.btn_number_pad {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:26px;
					font-weight:bold;
					text-align:center;
					width:87px;
					height:52px;
					background-image:url(<?php echo '"'.$imagePath.'/btn_wow_87x52.png"' ?>);
					background-repeat:no-repeat;
					background-position:center;
					border:hidden;
				}

				.btn_number_pad_wide {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:26px;
					font-weight:bold;
					text-align:center;
					width:132px;
					height:52px;
					background-image:url(<?php echo '"'.$imagePath.'/btn_wow_132x52.png"' ?>);
					background-repeat:no-repeat;
					background-position:center;
					border:hidden;
				}
				
				/**********************Universal styles***********************************/
				/*Removes outline on click of all elements*/
				*:focus{
						outline:0;
					}

				
				/**********************Text input and text area styles********************/
				/*All text inputs*/
				input[type="text"], input[type="tel"] {
					border-radius: 2px;
					-webkit-border-radius: 2px;
					border: 1px solid #bbb;

					font-family: sans-serif, verdana;
					color:#333;
					margin: 0 2px;
					padding-left: 15px;

					cursor: pointer;
					
					background-color: #fafafa;
				}
				/*Hover states for above*/
				input[type="text"]:hover, input[type="tel"]:hover {
					 box-shadow: 0px 1px 3px #999;
				}
				/*Down states for above*/
				input[type="text"]:active, input[type="tel"]:active {
					-moz-box-shadow: inset 0 0 10px #999;
					-webkit-box-shadow: inset 0 0 10px #999;
					box-shadow: inset 0 0 10px #999;
				}

				
				
				/*****************Large & medium action button styles**************/
				/*All Save buttons*/
				input[type="submit"], button[type="submit"] {
					border-radius: 2px;
					-webkit-border-radius: 2px;
					border:0 none !important;

					width:10em;
					height:3em;

					font-family: sans-serif, verdana;
					font-weight: 600;
					font-size: 1em;
					color: #fff;
					text-transform: uppercase;
					text-shadow: 0px 1px 3px #999;

					/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+51,99bc08+54,aecd37+100 */
					background: #aecd37; /* Old browsers */
					background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 51%, #99bc08 54%, #aecd37 100%); /* FF3.6-15 */
					background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
					background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
					filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#aecd37', endColorstr='#aecd37',GradientType=0 ); /* IE6-9 */

					transition:all 0.3s;
					
					cursor: pointer;
				}
				/*Hover states for all large & medium buttons*/
				input[type="submit"]:hover, button[type="submit"]:hover  {
					cursor: pointer;
						/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+92,99bc08+94,aecd37+100 */
					background: #aecd37; /* Old browsers */
					background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 92%, #99bc08 94%, #aecd37 100%); /* FF3.6-15 */
					background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 92%,#99bc08 94%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
					background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 92%,#99bc08 94%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
					filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#aecd37', endColorstr='#aecd37',GradientType=0 ); /* IE6-9 */

					box-shadow: 0px 1px 3px #999;

					text-shadow: 0px 1px 3px #666;
				}
				/*Down states for above*/
				input[type="submit"]:active, button[type="submit"]:active  {
					-moz-box-shadow: inset 0 0 10px #999;
					-webkit-box-shadow: inset 0 0 10px #999;
					box-shadow: inset 0 0 10px #999;
				}
				
			</style>
		<?php
		return ob_get_clean();
	}

	function getCardEnterPage() {
		global $on_ipad;
		global $available_DOcmds;
		$ipad = $on_ipad ? 'true':'false';
		ob_start();
		?>
		<html>
			<?php echo getStyleSheet(); ?>
			<script type='text/javascript'>
				<?php
					echo "on_ipad = $ipad;";
					echo "available_DOcmds = '$available_DOcmds';\n";
				?>
				// function validate(){
				// 	document.getElementById('card_num_input').value = document.getElementById('card_num_input').value.replace(/\D/g,'');
				// }

				function log(str){document.getElementById('err').innerHTML += str + "<br/>"};

				function startListening() {
					if(!on_ipad) return;
					if(available_DOcmds.indexOf('startListeningForSwipe') != -1) {
						window.location = '_DO:cmd=startListeningForSwipe';
					}
				}

				function handleCardSwipe(swipeString, hexString, reader, encrypted) {
					var cardNumber = "";
					var cardRead = false;
					// alert(available_DOcmds);
					// componentAlert("Card swiped: swipeString=" + swipeString + ", hexString=" + hexString + ", reader=" + reader + ", encrypted=" + encrypted);
					// alert(swipeString);
					// componentAlert("reader= " + reader);
					if (reader == "llsswiper") {
						if (hexString.length > 0) {
							var actual_swipe = fromHex(hexString);
							var tracks = actual_swipe.split("?");
							var track_parts = tracks[0].split(";");
							cardNumber = track_parts[1];
							cardRead = (cardNumber.length > 0);
						}
						if (!cardRead && swipeString.length>0) {
							var tracks = swipeString.split("?");
							for (var i = 0; i < tracks.length; i++) {
								var trk = tracks[i].replace(/[^a-z;=\d]+/ig,'');
								if (trk.substring(0, 1) == ";") {
									var track2 = trk.split("=");
									cardNumber = track2[0].trim("; ");
									if (cardNumber.length > 0) break;
								}
							}
						}
					}

					else if (reader == "idynamo") {
						swipeString = decodeURIComponent(swipeString);
						var tracks = swipeString.split("?");
						for (var i = 0; i < tracks.length; i++) {
							if(tracks[i].substring(0, 1) == ";") {
								cardNumber = tracks[i].split(';')[1].split('=')[0];
							}

							// var trk = tracks[i].split('|'); //.replace(/[^a-z;=\d]+/ig,'');
						}
					}
					if(isNumber(cardNumber)) {
						window.setTimeout(function(){
							var input_field = document.getElementById("card_num_input");
							input_field.value = cardNumber;
							validate();
							document.getElementById("form").submit();
						}, 50);
					}

					return 1;
				}
				function isNumber(n) {
					return !isNaN(parseFloat(n)) && isFinite(n);
				}

				function numPadPressed(num) {
					var input_field = document.getElementById("card_num_input");
					if(num == 'back') {
						input_field.value = input_field.value.substring(0, input_field.value.length-1);
						return;
					}
					input_field.value = input_field.value + num;
				}

			</script>
			<body onload='startListening();'>
				<form id='form' method='post' action=''>
					<div id='background_div'>
						<input type='hidden' name='is_posting' value='1' />
						<input type='hidden' name='time' value='' />
						<br/>
						
						<br/>
						<!-- <input class='btn_light_super_long' type='number' name='card_num' id='card_num_input' placeholder='Enter Card #' onkeyup='validate()' /> -->
						<input class='btn_light_super_long' type='text' name='card_num' id='card_num_input' placeholder='<?php echo speak("Enter Card");?>#' onkeyup='validate()' />
						<table height="100%" width="100%" cellspacing='2' cellpadding='0'>
								<!-- <tr>
									<td width='1px'></td>
									<td class='btn_number_pad' onclick='numPadPressed("7"); return false;'>7</td>
									<td class='btn_number_pad' onclick='numPadPressed("8"); return false;'>8</td>
									<td class='btn_number_pad' onclick='numPadPressed("9"); return false;'>9</td>
								</tr>
								<tr>
									<td width='1px'></td>
									<td class='btn_number_pad' onclick='numPadPressed("4"); return false;'>4</td>
									<td class='btn_number_pad' onclick='numPadPressed("5"); return false;'>5</td>
									<td class='btn_number_pad' onclick='numPadPressed("6"); return false;'>6</td>
								</tr>
								<tr>
									<td width='1px'></td>
									<td class='btn_number_pad' onclick='numPadPressed("1"); return false;'>1</td>
									<td class='btn_number_pad' onclick='numPadPressed("2"); return false;'>2</td>
									<td class='btn_number_pad' onclick='numPadPressed("3"); return false;'>3</td>
								</tr>
								<tr> 
									<td colspan='4'>
										<table cellspacing='0' cellpadding='0'>
											<tr>
												<td width='1px'></td>
												<td class='btn_number_pad_wide' onclick='numPadPressed("back"); return false;' style='padding-right: 3px;'><img style='padding:3px 0px 0px 0px;' src='../../../components/payment_extensions/lavugift/images/btn_cc_back.png'></td>
												<td class='btn_number_pad_wide' onclick='numPadPressed("0"); return false;'>0</td>
											</tr>
										</table>
									</td>
								</tr> -->
								<tr>
									<td colspan='4' align='center' style='padding:6px 0px 0px 0px;'>
										<button class='btn_light' type='submit'><b><?php echo speak("Submit"); ?></b></button>
									</td>
								</tr>
							</table>
						<!-- <p style='color:blue' id="err"><br/></p> -->
					</div>
				</form>
			</body>
		</html>

		<?php
		return ob_get_clean();
	}

	function getErrorPage(){
		global $card_num;
		ob_start();
			?>
			<html>
				<?php echo getStyleSheet(); ?>
				<body>
					<form method='post' action=''>
						<div id='background_div'>
							<br/>
							<h2>Card Not Found:</h2>
							<br/>
							<?php
								echo "<input type='hidden' name='card_num' value='".$card_num."'/><p style='font-size:18'> ";
								echo $card_num;
								echo "</p><br/>";
								echo "<br/>";
								echo "<input class='btn_light' type='submit' name='reset' value='Cancel'/>";
								if(canModify())
								{
									// if(!empty($card_num) && intval($card_num)) {
									if(!empty($card_num)) {
										echo "&nbsp&nbsp&nbsp<input class='btn_light' type='submit' name='register_card' value='Register'/>";
									}
								}
							?>

						</div>
					</form>
				</body>
			</html>

			<?php
		return ob_get_clean();
	}

	function canModify() {
		global $ll_mode;
		if(isset($ll_mode) && $ll_mode=="admin") return true;
		else return false;
	}

	function getCardModifyPage() {
		global $on_ipad;
		global $data_name;
		global $card_num;
		global $point_adjustment;
		global $balance_adjustment;
		global $server_name;
		global $server_id;
		global $register_card;
		global $management_session;
		global $new_exp_date;

		$chain_id = "";
		$account_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $data_name);
		if(mysqli_num_rows($account_query) > 0)
		{
			$account_read = mysqli_fetch_assoc($account_query);
			$chain_id = $account_read['chain_id'];

			#load all dataname existing in chain
			if (strlen($chain_id) > 1 ) {
				$chainedDataname = array();
				$chainedRestaurants =get_restaurants_by_chain($chain_id );	#get all chained restaurants
				foreach($chainedRestaurants as $restaurants ) {
					$chainedDataname[] = $restaurants['data_name'];
				}
			}
		}
		if($register_card) {
			// error_log("Registering Card: $card_num");

			$vars = array();
			$vars['chainid'] = $chain_id;
			$vars['dataname'] = $data_name;
			$vars['name'] = $card_num;
			$vars['points'] = 0;
			$vars['expires'] = "";
			$vars['created_datetime'] = date("Y-m-d H:i:s");
			$vars['balance'] = '0';
			$insert_query = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_cards` (`chainid`,`dataname`,`name`,`points`,`expires`,`created_datetime`,`balance`) VALUES ('[chainid]','[dataname]','[name]','[points]','[expires]','[created_datetime]','[balance]')",$vars);
			if($insert_query){
				$hist_vars = array();
				$hist_vars['chainid'] = $chain_id;
				$hist_vars['dataname'] = $data_name;
				$hist_vars['name'] = $card_num;
				$hist_vars['server_id'] = $server_id;
				$hist_vars['server_name'] = $server_name;
				$hist_vars['datetime'] = date("Y-m-d H:i:s");
				$hist_vars['card_id'] = mlavu_insert_id();
				$hist_vars['action'] = "Created";
				$hist_vars['previous_points'] = 0;
				$hist_vars['previous_balance'] = 0;
				$hist_vars['point_amount'] = 0;
				$hist_vars['balance_amount'] = 0;
				$hist_vars['new_points'] = 0;
				$hist_vars['new_balance'] = 0;
				$hist_vars['item_id_list'] = "";
				$hist_vars['order_id'] = NULL;
				$hist_success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`chainid`, `dataname`, `name`, `card_id`, `action`, `server_id`, `server_name`, `previous_points`, `previous_balance`, `point_amount`, `balance_amount`, `new_points`, `new_balance`, `item_id_list`, `datetime`) VALUES ('[chainid]', '[dataname]', '[name]', '[card_id]', '[action]', '[server_id]', '[server_name]', '[previous_points]', '[previous_balance]', '[point_amount]', '[balance_amount]', '[new_points]', '[new_balance]', '[item_id_list]', '[datetime]')", $hist_vars);
			}
			else {
				//TODO error handling
			}
		}
		if(strlen($chain_id) > 1) {
			$select_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted`=0 AND `inactive` = 0 AND `name`='[1]' AND (`dataname` IN('".implode("','", $chainedDataname)."') OR `chainid`='[2]')", $card_num, $chain_id);
		}
		else {
			$select_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted` = 0 AND `inactive` = 0 AND `name`='[1]' AND `dataname`='[2]'", $card_num, $data_name);
		}
		if(mysqli_num_rows($select_query) > 0)
		{
			$card_read = mysqli_fetch_assoc($select_query);
			$current_amount = floatval($card_read['balance']);
			$current_points = floatval($card_read['points']);
			if(empty($current_amount) || is_nan($current_amount)) $current_amount = 0;
			if(empty($current_points) || is_nan($current_points)) $current_points = 0;
			$card_dbid = $card_read['id'];
			$created = $card_read['created_datetime'];
			$expires = $card_read['expires'];
			if($new_exp_date == -1) $new_exp_date = $expires;
			$new_balance = max($current_amount + $balance_adjustment, 0);
			$new_points = max($current_points + $point_adjustment, 0);


			if($new_balance != $current_amount || $new_points != $current_points || $new_exp_date != $expires) {

				if(strlen($chain_id) > 1) {
					$success = mlavu_query('UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET `points`="[1]",`balance`="[2]",`expires`="[3]" WHERE `deleted` = 0 AND `name`="[4]" AND (`dataname` IN("'.implode('","', $chainedDataname).'") OR `chainid`="[5]")', $new_points, $new_balance, $new_exp_date, $card_num, $chain_id);
				}
				else {
					$success = mlavu_query('UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET `points`="[1]",`balance`="[2]",`expires`="[3]" WHERE deleted = 0 AND `name`="[4]" AND `dataname`="[5]"', $new_points, $new_balance, $new_exp_date, $card_num, $data_name);
				}
				if($success) {
					$hist_vars = array();
					$hist_vars['chainid'] = $chain_id;
					$hist_vars['dataname'] = $data_name;
					$hist_vars['name'] = $card_num;
					$hist_vars['server_id'] = $server_id;
					$hist_vars['server_name'] = $server_name;
					$hist_vars['datetime'] = date("Y-m-d H:i:s");
					$hist_vars['card_id'] = $card_dbid;
					$hist_vars['action'] = "Edited";
					$hist_vars['previous_points'] = $current_points;
					$hist_vars['previous_balance'] = $current_amount;
					$hist_vars['point_amount'] = $point_adjustment;
					$hist_vars['balance_amount'] = $balance_adjustment;
					$hist_vars['new_points'] = $new_points;
					$hist_vars['new_balance'] = $new_balance;
					$hist_vars['item_id_list'] = "";
					$hist_vars['order_id'] = "";
					$hist_success = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`chainid`, `dataname`, `name`, `card_id`, `action`, `server_id`, `server_name`, `previous_points`, `previous_balance`, `point_amount`, `balance_amount`, `new_points`, `new_balance`, `item_id_list`, `datetime`) VALUES ('[chainid]', '[dataname]', '[name]', '[card_id]', '[action]', '[server_id]', '[server_name]', '[previous_points]', '[previous_balance]', '[point_amount]', '[balance_amount]', '[new_points]', '[new_balance]', '[item_id_list]', '[datetime]')", $hist_vars);
				}
				else {
					//TODO error handling for failure to update card
				}
			}
			$current_amount = $new_balance;
			$show_current_amount = ext_display_money_with_symbol($current_amount);
			$current_points = $new_points;
			$expires = $new_exp_date;
		}

		else return getErrorPage();

		ob_start();
			?>
			<html>
				<?php echo getStyleSheet(); ?>
				<script type='text/javascript'>
					function validate(pointOffset, balanceOffset){
						pointOffset = pointOffset || 0;
						balanceOffset = balanceOffset || 0;
						var tmp1 = parseInt(document.getElementById('adjust_points_input').value, 10);
						if(isNaN(tmp1)) tmp1 = 0;
						document.getElementById('adjust_points_input').value = tmp1 + pointOffset;

						var tmp2 = parseFloat(document.getElementById('adjust_balance_input').value);
						if(isNaN(tmp2)) tmp2 = 0;
						document.getElementById('adjust_balance_input').value = tmp2 + balanceOffset;
					}

					function reset(){
						document.getElementById("reset").value = "1";
						document.getElementById("form").submit();
					}

				</script>
				<body>
					<form method='post' id='form' action=''>
						<div id='background_div'>
							<input type='hidden' name='card_num' value='<?php echo "$card_num"; ?>' />
							<input type='hidden' name='card_id' value='<?php echo "$card_dbid;"; ?>' />
							<input type='hidden' id='reset' name='reset' value='0' />
							<div style='text-align:center'>
								<h2>
                                	<?php
                                    	if(canModify()) echo "<a onclick='testPrint()'>Adjust Gift/Loyalty Card</a>";
										else echo "Gift/Loyalty Balance";
									?>
                                    </h2>
								<input class='btn_light_super_long' type='button' value="<?php echo "$card_num"; ?>" onclick='document.getElementById("reset").value = "1"; document.getElementById("form").submit();' />
								<br/><br/>
							</div>
							<div>
								<table cellpadding='8' style='text-align:center; border-collapse:collapse;'>
									<tr>
										<td style='border-right:1px solid #777777'>
											<b>Gift Card Balance:</b>&nbsp;&nbsp;
											<p>
												<?php echo "$show_current_amount" ?>
											</p>
										</td>
										<td>
											&nbsp;&nbsp;<b>Loyalty Card Points:</b>
											<p>
												<?php echo "$current_points" ?>
											</p>
										</td>
									</tr>
                                    <?php if(canModify()) { ?>
									<tr>
										<td style='border-right:1px solid'>
											<p>
											Adjust Amount:
											</p>
											<!-- <input class='btn_light_short' type='button' value='-5' onclick='validate(0, -5)'> -->
											<input class='btn_light' type='tel' name="balance_adjustment" id="adjust_balance_input" placeholder='0' onblur='validate()' />
											<!-- <input class='btn_light_short' type='button' value='+5' onclick='validate(0, 5)'> -->
											<br>
										</td>
										<td>
											<p>
											Adjust Amount:
											</p>
											<!-- <input class='btn_light_short' type='button' value='-' onclick='validate(-1, 0)'> -->
											<input class='btn_light' type='tel' name="point_adjustment" id="adjust_points_input" placeholder='0' onblur='validate()' />
											<!-- <input class='btn_light_short' type='button' value='+' onclick='validate(1, 0)'> -->
											<br>
										</td>
									</tr>
                                    <?php } ?>
								</table><br/>
                                <?php
									if(canModify()){
										echo "Expires: <input class='btn_light_long' name='new_exp_date' type='date' value='$expires' />";
									}
									else{
										if(trim($expires)!="")
										{
											echo "Expires: $expires";
										}
									}
								?>
							</div>
							<div style='text-align:center'>
								<br/><br/>

								<?php
									$showOk=true;
									if(canModify()){
										$showOk=false;
										echo "<input class='btn_light_long' type='submit' />";
									}

									if($management_session && canModify()){
										$showOk=false;
										echo "<input class='btn_light_long' type='submit' name='switch_to_history' value='View Card History' style='width:200px; margin-left:20px;'/>";
									}
									if($showOk){
								?>
									<script>
									function reDirect(){
										window.location.href="<?=$_SERVER['REQUEST_URI'];?>"
									} 
									</script>
									<input type="button" value="OK" onClick="javascript:reDirect();">
								<?php
									}
								?>
							</div>
						</div>
					</form>
					<script>
					if(typeof(parent) == 'undefined') {
						console.log('Not Iframe');
					}
					else {
   						console.log('iframe');
					}
					</script>
				</body>
			</html>

			<?php
		return ob_get_clean();
	}
?>