<?php
$cbaresult = lavu_query("SELECT id,value FROM `config` WHERE `setting`='required_change_bank_amount'");
if(!$cbaresult){
	error_log("mysql error in " . __FILE__ . "  mysql error:" . lavu_dberror());
	return false;
}
$req_amount = '';
if(mysqli_num_rows($cbaresult)>0){ 
	$settingRow = mysqli_fetch_assoc($cbaresult);
	$req_amount = $settingRow['value'];
}
$timezone = locationSetting("timezone");
if (!empty($timezone))
{
	$datetime = DateTimeForTimeZone($timezone);
}
else
{
	$datetime = date("Y-m-d H:i:s");
}

$message = ""; 
if(isset($_POST['change_amount']) && $_POST['change_amount']!='')
{
	$get_names = lavu_query("SELECT id FROM `users` WHERE `username` = '[1]'", $_REQUEST['username']);
	if (mysqli_num_rows($get_names) > 0)
	{
		$info = mysqli_fetch_assoc($get_names);
		$userid = $info['id'];
		$lname = $info['l_name'];
	}

	//THIS WAS ADDED BY SURESH.
	// LP-5922 Altering change_bank field to drop unique key index for user_id column
	/*
	$check_count = lavu_query("SELECT `id` FROM `change_bank`");
	if(mysqli_num_rows($check_count) == 1)
	{
		lavu_query("ALTER TABLE `change_bank` DROP FOREIGN KEY `change_bank_ibfk_1`");
		lavu_query("ALTER TABLE `change_bank` DROP INDEX `user_id`");
		lavu_query("CREATE INDEX `idx_user_id` ON change_bank(user_id)");
	}*/

	$data = array('change_amount' => $_POST['change_amount'], 'user_id' => $userid, 'req_amount'=>$req_amount,'created_date' => $datetime);
	$insertCbd = lavu_query("INSERT INTO `change_bank` (`amount`,`user_id`,`required_amount`,`created_date`) values('[change_amount]','[user_id]','[req_amount]','[created_date]')",$data);
	$message = "<div style='color:#aecd37'>" . speak('Amount submitted successfully') . "</div>";
}
ob_start();
$display = "";

$decimal_char = $location_info['decimal_char'];
$thousands_char = ($decimal_char == ".")?",":".";
if ($location_info['left_or_right'] == "right"){
	$req_amount = number_format($req_amount, $location_info['disable_decimal'], $decimal_char, $thousands_char).$location_info['monitary_symbol'];
}else{
	$req_amount = $location_info['monitary_symbol'].number_format($req_amount, $location_info['disable_decimal'], $decimal_char, $thousands_char);
}

global $imagePath;
$display .= '<style>
body {
	font-family: arial, sans-serif;
}
/**********************Text input and text area styles********************/
		/*All text inputs*/
		input[type="text"], input[type="tel"] {
			border-radius: 2px;
			-webkit-border-radius: 2px;
			border: 1px solid #bbb;

			font-family: sans-serif, verdana;
			color:#333;
			margin: 0 2px;
			padding-left: 15px;

			cursor: pointer;
			
			background-color: #fafafa;
		}
		/*Hover states for above*/
		input[type="text"]:hover, input[type="tel"]:hover {
			 box-shadow: 0px 1px 3px #999;
		}
		/*Down states for above*/
		input[type="text"]:active, input[type="tel"]:active {
			-moz-box-shadow: inset 0 0 10px #999;
			-webkit-box-shadow: inset 0 0 10px #999;
			box-shadow: inset 0 0 10px #999;
		}
		.btn_light_super_long {
					color:#7D7D7D;
					font-family:Arial, Helvetica, sans-serif;
					font-size:13px;
					width:200px;
					height:37px;
					background:url(<?php echo "'.$imagePath.'""/btn_wow_250x37.png" ?>);
					border:hidden;
					padding-top:2px;
					padding-left:8px;
					text-align: center;
				}
		
		
		/*****************Large & medium action button styles**************/
		/*All Save buttons*/
		input[type="submit"], button[type="submit"] {
			border-radius: 2px;
			-webkit-border-radius: 2px;
			border:0 none !important;

			width:10em;
			height:3em;

			font-family: sans-serif, verdana;
			font-weight: 600;
			font-size: 1em;
			color: #fff;
			text-transform: uppercase;
			text-shadow: 0px 1px 3px #999;

			/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+51,99bc08+54,aecd37+100 */
			background: #aecd37; /* Old browsers */
			background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 51%, #99bc08 54%, #aecd37 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#aecd37", endColorstr="#aecd37",GradientType=0 ); /* IE6-9 */

			transition:all 0.3s;
			
			cursor: pointer;
		}
		/*Hover states for all large & medium buttons*/
		input[type="submit"]:hover, button[type="submit"]:hover  {
			cursor: pointer;
				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+92,99bc08+94,aecd37+100 */
			background: #aecd37; /* Old browsers */
			background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 92%, #99bc08 94%, #aecd37 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 92%,#99bc08 94%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 92%,#99bc08 94%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#aecd37", endColorstr="#aecd37",GradientType=0 ); /* IE6-9 */

			box-shadow: 0px 1px 3px #999;

			text-shadow: 0px 1px 3px #666;
		}
		/*Down states for above*/
		input[type="submit"]:active, button[type="submit"]:active  {
			-moz-box-shadow: inset 0 0 10px #999;
			-webkit-box-shadow: inset 0 0 10px #999;
			box-shadow: inset 0 0 10px #999;
		}
		 
</style>
<script>
function validateChangeBank()
{
	if(document.getElementById("change_amount_input").value == "" || document.getElementById("change_amount_input").value<=0)
	{
		document.getElementById("message_changebank").innerHTML = "' . speak('Please enter valid amount') . '";
		return false;
	}
}
</script>';
$display .= "<div >
		<form id='form' method='post' action=''>
					<div id='background_div'>
						
						<div>
							<table cellpadding='8' style='text-align:center; border-collapse:collapse;'>
								<tr>
									<td style='border:1px solid #777777'>
										<p>
										" . speak('Required Change Bank Amount') . "
										</p>
										".$req_amount."	
										<br>
									</td>
								</tr>
								<tr>
									<td>
										<p>
										Current Change Bank
										</p> 
										<input class='btn_light_super_long' type='tel'".use_number_pad_if_available('change_amount_input', 'Change Amount', 'money')."  name='change_amount' id='change_amount_input' placeholder='Amount' /> 
										<br>
									</td>
								</tr>
								<tr>
									<td><div id='message_changebank'>".$message."</div></td>
								</tr>
								<tr>
									<td align='center' style='padding:6px 0px 0px 0px;'>
										<button class='btn_light' type='submit' onclick='return validateChangeBank()'><b>" . speak('Submit') . "</b></button>
									</td>
								</tr>
							</table> 
							
						</div>
					</div>
				</form>";
 
$display .= '</div>';



?>
