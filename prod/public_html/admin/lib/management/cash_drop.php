<?php

/*

	$_REQUEST KEYS: mode, cc, loc_id, server_id, username, register, register_name, device_time, poslavu_version,PHPSESSID
			Advanced Location Setting-"Register Deposits" 'enable/disable'

			-ag [10:32 AM] 
			-Date / Time
			-User (account id, f_name, l_name and id)
			-Amount (Use number pad)
			-Type (Cash Drop)
			-Register (register name)
			-Notes (Area for insert notes)
			Enter/Cancel buttons

			Management - In App

			Per register, per shift, respect day end/start time.

			Backend - Regular report format (date range, sort column headers, included in 'Summary Report' V1->Summary at to end (only appears if using feature))

	Field Map:
	id
	loc_id
	type = 'cash_drop'
	date = 'server date time'
	device_time = 'From the device'
	server_time = 'Rackspace time'
	server_id = 'Server id of who's signed in
	value = The cash drop amount without the monitary symbol
	value2 = The Register
	value3 = localized date time
	setting = cash_drop
	value4
	value5
	value6
	_deleted
	value_med = The notes from the server

*/
	require_once(dirname(__FILE__)."/../info_inc.php");
	
	
	
	lavu_connect_dn($_REQUEST['cc'],'poslavu_'.$_REQUEST['cc'].'_db');

	$locationRow = getLocationRow();


	$registerName = getRegisterTitleFromName();

	$header_title = "Cash Drop - ".$registerName;
	$calculated_security_hash = sha1($_REQUEST['cc'].'_an_unlikely_password');
	$serverDatetime = date('Y-m-d H:i:s');
	$localizedDatetime =  localize_datetime($serverDatetime);//localize_datetime($timestamp, $to_tz)
	$localizedDatetimeThreshold = date('Y-m-d H:i:s', strtotime('-24 hour', strtotime($localizedDatetime)));
	
	
	//If Entry Posted
	if($_POST['entry_posted']){
		if(empty($_POST['sec_hash']) || $_POST['sec_hash'] != $calculated_security_hash){
			echo "Missing hash"; return;
		}
		$insertArray = array();
		$insertArray['loc_id'] = $locationRow['id'];
		$insertArray['type'] = 'cash_drop';
		$insertArray['date'] = date('Y-m-d');
		$insertArray['device_time'] = $_REQUEST['device_time'];
		$insertArray['server_time'] = date('Y-m-d H:i:s');
		$insertArray['server_id'] = $_REQUEST['server_id'];
		$insertArray['value'] = $_REQUEST['drop_amount'];
		$insertArray['value2'] = $_REQUEST['register'];
		$insertArray['value3'] = $localizedDatetime;
		$insertArray['value4'] = $registerName;
		$insertArray['setting'] = 'cash_drop';
		$insertArray['value_med'] = $_REQUEST['user_notes'];

		$queryStr  = "INSERT INTO `cash_data` ";
		$queryStr .= "( `loc_id`,   `type`,  `date`, `device_time`,  `server_time`,   `server_id`, `value`,  `value2`,  `value3`,   `value4`, `setting`,  `value_med` ) VALUES ";
		$queryStr .= "( '[loc_id]','[type]','[date]','[device_time]','[server_time]','[server_id]','[value]','[value2]','[value3]','[value4]','[setting]','[value_med]' )";
		$insertResult = lavu_query($queryStr, $insertArray);
		if(!$insertResult){
			error_log('Mysql error in '.__FILE__.' -s8f7- '.lavu_dberror());
		}
	}

	
	//Pull Cash Drop Data For Display
	//$display .= "Date:".date('Y-m-d h:i:s')." Localized datetime:".$localizedDatetime." localizedDatetimeThreshold:".$localizedDatetimeThreshold;
	$recentCashDropsAndUserQuery = <<<CASH_DATA_JOIN_USERS_QUERY
		SELECT    `cash_data`.`loc_id`, `cash_data`.`date`, `cash_data`.`device_time`,
		          `cash_data`.`server_time`, `cash_data`.`server_id`, `cash_data`.`value` as drop_amount,
		          `cash_data`.`value2` as register, `cash_data`.`value3` as loc_datetime, 
		          `cash_data`.`value_med` as user_notes,
		          `users`.`f_name`,`users`.`l_name`,
		          `cash_data`.`value4` as register_title
		FROM `cash_data`
		JOIN `users` ON `cash_data`.`server_id`=`users`.`id`
		WHERE `cash_data`.`value3` > '[1]' AND `cash_data`.`type`='cash_drop'
		      AND `cash_data`.`value4`='[3]'
		ORDER BY `cash_data`.`value3` DESC LIMIT 20
CASH_DATA_JOIN_USERS_QUERY;

	$cashDropsResult = lavu_query($recentCashDropsAndUserQuery, $localizedDatetimeThreshold, $_REQUEST['register'],$registerName);	
		
	if(!$cashDropsResult){
		error_log("Mysql error in ".__FILE__." mysql error:".lavu_dberror());
	}
	$cashDropRows = array();
	while($currCashDropRow = mysqli_fetch_assoc($cashDropsResult)){
		$cashDropRows[] = $currCashDropRow;
	}

	$decimal_places = $locationRow['disable_decimal'];
	//TABLE FOR DISPLAYING LAST CASH DROPS
	$display .= "<style>";
	$display .= "	.rtitle { border-bottom:solid 1px black; } ";
	$display .= "	.rcol { color:#555555; vertical-align:top; border-right:solid 1px #cccccc; border-bottom:solid 1px #cccccc; } ";
	$display .= "	.rcol_left { vertical-align:top; border-left:solid 1px #cccccc; } ";
	$display .= "	.rcol_notes { color:#555555; } ";
	$display .= "	.rcol_money { color:#007700; } ";
	$display .= "	.rcol_time { color:#000077; } ";
	$display .= "</style>";
	$display .= "<div style='overflow-y:scroll;max-height:260px'>";
	$display .= "<table cellspacing=0 cellpadding=4>";
	//TitleRow
	$display .= "<tr><td class='rtitle'>" . speak('Drop Time') . "</td><td class='rtitle'>" . speak('Register') . "</td><td class='rtitle'>" . speak('Name') . "</td><td class='rtitle'>" . speak('Amount') . "</td><td class='rtitle'>" . speak('Notes') . "</td></tr>";
	foreach($cashDropRows as $currRow){
		$display .= "<tr style='height:20px'>".
						"<td class='rcol rcol_left rcol_time'>".date('n/j/Y g:ia', strtotime($currRow['loc_datetime']))."</td>".
						"<td class='rcol'>".$currRow['register_title']."</td>".
						"<td class='rcol'>".$currRow['f_name'].' '.$currRow['l_name']."</td>".
						"<td style='text-align:right' class='rcol rcol_money'>".display_price($currRow['drop_amount'], $decimal_places)."</td>".
						"<td class='rcol rcol_notes'><div style='max-width:400px;max-height:50px;overflow:scroll'>".$currRow['user_notes']."</div></td>".
				    "</tr>";
	}
	$display .= "</table>";
	$display .= "</div>";
	$display .= "<br><br>";
	//TABLE FOR INSERT
	/*
		      $str = " READONLY onclick='window.location = \"_DO:cmd=number_pad&type=" .     
		          $type ."&max_digits=15&title=".rawurlencode($title) .
		          "&js=AIsetFieldValue(%27".$field_id."%27,%27ENTERED_NUMBER%27)\"'";

	*/
    $maxLength = '100';
	$display .= "".
		"<script>".
		"function submitDropForm(){".
			"var submitForm = document.getElementById('post_cash_drop_form');".
			"var dropAmountInput = document.getElementById('drop_amount');".
			"var amountString = document.getElementById('drop_amount').value;".
			"amountString = amountString.replace(/[^0-9,.]/, '');".
			"if(amountString == ''){ window.location='_DO:cmd=alert&title=Attention&message=Please enter valid Cash Drop amount.'; return;}".
			"document.getElementById('drop_amount').value = amountString;".
			"submitForm.appendChild(document.getElementById('drop_amount'));".
			"submitForm.appendChild(document.getElementById('user_notes'));".
			"submitForm.submit();".
		"}".
		"function loadNumberPad(){".
		    "window.location='_DO:cmd=number_pad&type=money&max_digits=15&title=Drop%20Amount&js=AIsetFieldValue(%27ENTERED_NUMBER%27)';".
		"}".
		"function AIsetFieldValue(fieldVal){".
			"document.getElementById('drop_amount').value=fieldVal;".
		"}".
		"function adjustTotalCharactersLeftCount(){".
			"setTimeout(function(){".
				"var characterCount = document.getElementById('user_notes').value.length;".
				"document.getElementById('chars_left_div').innerHTML = characterCount+'/$maxLength';".
			"}, 50);".
		"}".
		"</script>".
	    "<form method='POST' id='post_cash_drop_form' style='display:none'>".
			"<input type='hidden' name='entry_posted' value='1'/>".
			"<input type='hidden' name='sec_hash' value='$calculated_security_hash'/>".
	    "</form>".
    	"<table style='width:400px' id='cash_drop_form_table'>".
    		"<tr><td colspan='2'><input placeholder='".speak("Cash Drop Amount")."' style='font-size:18px; border:solid 1px #555555; border-radius:8px' type='text' name='drop_amount' id='drop_amount' onclick='loadNumberPad()' readonly/></td></tr>".
    		
    		"<tr><td colspan='2'><textarea placeholder='".speak("Notes")."' maxlength='$maxLength' rows='3' style='width:100%; font-size:18px'; name='user_notes' id='user_notes'  '></textarea></td></tr>".
			"<tr><td style='text-align:left'><input type='submit' style='width:120px' onclick=\"submitDropForm();\" value='". speak('Submit') . "'/></td><td style='text-align:right;'><div id='chars_left_div' style='color:#8888aa'>0/$maxLength</div></td></tr>".
    	"</table>".
    	"<script>".
    		"document.getElementById('user_notes').addEventListener('keydown', adjustTotalCharactersLeftCount);".
    	"</script>";

	$display .= "<script> window.location='_DO:cmd=set_scrollable&set_to=false'; </script>";
	function getLocationRow(){
		$result = lavu_query("SELECT * FROM `locations` ORDER BY `id` ASC LIMIT 1");
		$row = mysqli_fetch_assoc($result);
		error_log('number of rows:'.mysqli_num_rows($result));
		return $row;
	}


	function getRegisterTitleFromName(){
		$registerName = $_REQUEST['register'];
		$registerResult = lavu_query("SELECT `value2` FROM `config` WHERE `_deleted`='0' AND `setting`='[1]' AND `type`='printer'", $_REQUEST['register']);
		if(!$registerResult){
			error_log('Mysql error in '.__FILE__.' -ki- mysql error:'.lavu_dberror());
		}
		if(!mysqli_num_rows($registerResult)){
			//Could not find register...
			return $_REQUEST['register'];
		}else{
			$row = mysqli_fetch_assoc($registerResult);
			return $row['value2'];
		}
	}

?>




