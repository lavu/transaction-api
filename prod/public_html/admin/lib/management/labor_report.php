<?php
	ini_set("display_errors",1);
	
	//echo "Labor Report:<br><br>";
	ob_start();
	// Load Labor Report Library
	$_GET['output_mode'] = "data";
	$_GET['run_report_start_datetime'] = "2000-00-00 00:00:00";
	$_GET['run_report_end_datetime'] = "2000-00-00 00:00:00";
	require(dirname(__FILE__) . "/../../cp/areas/labor_report.php");
	$labor_output = ob_get_contents();
	ob_end_clean();
	
	ob_start();
	$lbr_date = lbr_date_start_datetime();
	$lbr_end_date = lbr_date_add($lbr_date,1);
	
	// Load Labor Report For Day
	$_GET['output_mode'] = "run_report";
	$_GET['run_report_start_datetime'] = $lbr_date;
	$_GET['run_report_end_datetime'] = $lbr_end_date;
	require(dirname(__FILE__) . "/../../cp/areas/labor_report.php");
	//$day_data = $data;
	$labor_output = ob_get_contents();
	ob_end_clean();
	
	// For some reaon the report is populating twice in the labor output variable.  Below is a simple workaround to make sure only the second one is displayed - CF
	$findstr = "</div>";
	$labor_parts = explode($findstr,$labor_output);
	if(count($labor_parts)==3)
	{
		$labor_output = $labor_parts[1] . $findstr . $labor_parts[2];
	}
	$display .= $labor_output;
?>