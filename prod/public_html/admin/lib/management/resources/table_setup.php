<script type='text/javascript'>

placing_table = "none";
tables_placed = 1;

coord_x = new Array();
coord_y = new Array();
table_shapes = new Array();
table_widths = new Array();
table_heights = new Array();
table_names = new Array();
image_path = document.location.protocol + "//" + document.location.hostname + "/lib/management/";
table_mouseover = "showTableDetails";

var table_number, drag_table, resize_table, resize_img, resize_cell, offsetX, offsetY, setX, setY, table_startX, table_startY, is_dragging = false, is_dragging2 = false, is_resizingW = false, is_resizingH = false; is_resizingWH = false;
document.onmousemove = moveTable;
document.onmouseup = stopDrag;
document.onkeydown = keyCmd;
//alert("loading");
//var y_correct = 33;
//var table_mode = 'edit';

window.onload = init_tables;

function init_tables()
{
	if(table_mode=="edit")
	{
		var drag_square = document.getElementById("drag_square");
		var drag_circle = document.getElementById("drag_circle");
		var drag_diamond = document.getElementById("drag_diamond");
		var drag_slant_right = document.getElementById("drag_slant_right");
		var drag_slant_left = document.getElementById("drag_slant_left");
	
		drag_square.ondrag = function() { return false; };
		drag_square.ondragstart = function() { return false; };
		drag_square.onselectstart = function() { return false; };
		drag_circle.ondrag = function() { return false; };
		drag_circle.ondragstart = function() { return false; };
		drag_circle.onselectstart = function() { return false; };
		drag_diamond.ondrag = function() { return false; };
		drag_diamond.ondragstart = function() { return false; };
		drag_diamond.onselectstart = function() { return false; };
		drag_slant_right.ondrag = function() { return false; };
		drag_slant_right.ondragstart = function() { return false; };
		drag_slant_right.onselectstart = function() { return false; };
		drag_slant_left.ondrag = function() { return false; };
		drag_slant_left.ondragstart = function() { return false; };
		drag_slant_left.onselectstart = function() { return false; };
	}
	preload_img1 = new Image(); preload_img1.src = image_path + "images/table_square.png";
	preload_img2 = new Image(); preload_img2.src = image_path + "images/table_circle.png";
	preload_img3 = new Image(); preload_img3.src = image_path + "images/table_diamond.png";
	preload_img4 = new Image(); preload_img4.src = image_path + "images/table_slant_right.png";
	preload_img5 = new Image(); preload_img5.src = image_path + "images/table_slant_left.png";
	
	/*preload_img6 = new Image(); preload_img6.src = image_path + "images/settings_over.png";
	preload_img7 = new Image(); preload_img7.src = image_path + "images/help_over.png";
	preload_img8 = new Image(); preload_img8.src = image_path + "images/sign_out_over.png";
	preload_img9 = new Image(); preload_img9.src = image_path + "images/save_over.png";
	preload_img10 = new Image(); preload_img10.src = image_path + "images/exit_over.png";*/
}

function rzCC(s) {
	for (var exp=/-([a-z])/; exp.test(s); s=s.replace(exp,RegExp.$1.toUpperCase()));
		return s;
}

function _setStyle(element, declaration) {  // special function to allow style set in IE
	if (declaration.charAt(declaration.length-1)==';')
		declaration = declaration.slice(0, -1);
	var k, v;
	var splitted = declaration.split(';');
	for (var i=0, len=splitted.length; i<len; i++) {
		k = rzCC(splitted[i].split(':')[0]);
		v = splitted[i].split(':')[1];
		eval("element.style."+k+"='"+v+"'");
	}
}

function findPosX(obj) {
	var curleft = 0;
	if (obj.offsetParent) {
		while (1) {
			curleft += obj.offsetLeft;
			if (!obj.offsetParent) {
				break;
			}
			obj = obj.offsetParent;
		}
	} else if (obj.x) {
		curleft += obj.x;
	}
	return curleft;
}

function findPosY(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		while (1) {
			curtop += obj.offsetTop;
			if (!obj.offsetParent) {
				break;
			}
			obj = obj.offsetParent;
		}
	} else if (obj.y) {
		curtop += obj.y;
	}
	return curtop;
}

function loadTables(Xs, Ys, Ss, Ws, Hs, Ns, Op) {
	coord_x = Xs.split("|");
	coord_y = Ys.split("|");
	table_shapes = Ss.split("|");
	table_widths = Ws.split("|");
	table_heights = Hs.split("|");
	table_names = Ns.split("|");
	table_active = new Array();
	table_info = new Array();
	
	tables_placed = coord_x.length + 1;
	
	var debug_info = "";
	if(table_mode=="view")
	{
		show_debug_message = false;
		debug = "";
		
		min_coord_x = 1000;
		min_coord_y = 1000;
		max_coord_x = 0;
		max_coord_y = 0;
		for(i = 0; i < coord_x.length; i++) {
			debug += coord_x[i] + "/" + coord_y[i] + "\n";
			if(coord_x[i] * 1 < min_coord_x * 1) min_coord_x = coord_x[i];
			if(coord_y[i] * 1 < min_coord_y * 1) min_coord_y = coord_y[i];
			//if((coord_x[i] * 1) > max_coord_x) max_coord_x = (coord_x[i] * 1 + table_widths[i] * 1);
			//if((coord_y[i] * 1) > max_coord_y) max_coord_y = (coord_y[i] * 1 + table_heights[i] * 1);
			if((coord_x[i] * 1 + table_widths[i] * 1) > max_coord_x) max_coord_x = (coord_x[i] * 1 + table_widths[i] * 1);
			if((coord_y[i] * 1 + table_heights[i] * 1) > max_coord_y) max_coord_y = (coord_y[i] * 1 + table_heights[i] * 1);
		}
		for(i = 0; i < coord_x.length; i++) {
			coord_x[i] -= min_coord_x;
			coord_y[i] -= min_coord_y;
		}
		max_coord_x -= min_coord_x;
		max_coord_y -= min_coord_y;
		
		t_shrink_w = 1;
		t_shrink_h = 1;
		if(t_winwidth < max_coord_x)
		{
			t_shrink_w = t_winwidth / max_coord_x;
		}
		if(t_winheight < max_coord_y)
		{
			t_shrink_h = t_winheight / max_coord_y;
		}
		if(t_shrink_w < t_shrink_h) t_shrink = t_shrink_w; else t_shrink = t_shrink_h;
		
		for(i = 0; i < coord_x.length; i++) {
			coord_x[i] = coord_x[i] * t_shrink;
			coord_y[i] = coord_y[i] * t_shrink;
			table_widths[i] = table_widths[i] * t_shrink;
			table_heights[i] = table_heights[i] * t_shrink;
			
			table_active[i] = false;
			for(n = 0; n < Op.length; n++) {
				if(Op[n]==table_names[i])
				{
					table_shapes[i] = table_shapes[i] + "_active";
					table_active[i] = true;
				}
			}
		}
	}
	
	for (i = 0; i < coord_x.length; i++) {
		//create container div
		new_table_div = document.createElement('div');
		if(table_mode!="edit")
		{
			table_is_active = table_active[i];
			if(table_is_active)
			{
				new_table_div.onmouseover = showTableDetails;
				new_table_div.onclick = clickTable;
			}
		}
		new_table_div.id = "table_" + (i + 1);
		table_info[new_table_div.id] = table_names[i];
		_setStyle(new_table_div, "position:absolute; left:" + coord_x[i] + "px; top:" + (parseInt(coord_y[i]) + y_correct) + "px; z-index:" + (100 + ((i + 1) * 2)) + ";");
		//debug += coord_x[i] + " / " + (parseInt(coord_y[i]) + y_correct) + "\n";
		document.getElementById('table_setup_div').appendChild(new_table_div);
		//create table for structure and functional regions
		new_table_table = document.createElement('table');
		new_table_table.setAttribute("cellspacing", "0");
		new_table_table.setAttribute("cellpadding", "0");
		if(table_mode=="edit")
		{
			new_table_table.onmouseover = HL;
			new_table_table.onmouseout = unHL;
		}
		new_table_div.appendChild(new_table_table);
		// create tbody
		new_table_tbody = document.createElement('tbody');
		new_table_table.appendChild(new_table_tbody);		
		// create rows
		new_table_row_1 = document.createElement('tr');
		new_table_tbody.appendChild(new_table_row_1);
		new_table_row_2 = document.createElement('tr');
		new_table_tbody.appendChild(new_table_row_2);
		// create cells
		new_table_cell_1 = document.createElement('td');
		if(table_mode=="edit")
		{
			_setStyle(new_table_cell_1, "border-left:1px dotted #BBBBBB; border-top:1px dotted #BBBBBB;");
		}
		new_table_row_1.appendChild(new_table_cell_1);
		new_table_cell_2 = document.createElement('td');
		if(table_mode=="edit")
		{
			_setStyle(new_table_cell_2, "width:2px; cursor:e-resize; border-top:1px dotted #BBBBBB; border-right:1px dotted #BBBBBB;");
			new_table_cell_2.onmousedown = resizeW;
		}
		new_table_row_1.appendChild(new_table_cell_2);
		new_table_cell_3 = document.createElement('td');
		if(table_mode=="edit")
		{
			_setStyle(new_table_cell_3, "height:2px; cursor:s-resize; border-left:1px dotted #BBBBBB; border-bottom:1px dotted #BBBBBB;");
			new_table_cell_3.onmousedown = resizeH;
		}
		new_table_row_2.appendChild(new_table_cell_3);
		new_table_cell_4 = document.createElement('td');
		if(table_mode=="edit")
		{
			_setStyle(new_table_cell_4, "width:2px; height:2px; cursor:se-resize; border-right:1px dotted #BBBBBB; border-bottom:1px dotted #BBBBBB;");
			new_table_cell_4.onmousedown = resizeWH;
		}
		new_table_row_2.appendChild(new_table_cell_4);
		//create table img
		new_table_img = document.createElement('img');
		new_table_img.src = image_path + "images/table_" + table_shapes[i] + ".png";
		debug_info += image_path + "images/table_" + table_shapes[i] + ".png\n";
		_setStyle(new_table_img, "width:" + table_widths[i] + "px; height:" + table_heights[i] + "px;");
		new_table_img.id = "table_" + (i + 1) + "_img";
		new_table_cell_1.appendChild(new_table_img);
		//create continer div for table name
		new_table_name_div = document.createElement('div');
		_setStyle(new_table_name_div, "position:absolute; left:0px; top:0px; width:" + table_widths[i] + "px; height:" + table_heights[i] + "px; z-index:" + (101 + ((i + 1) * 2)) + "; text-align:center; vertical-align:middle;");
		new_table_cell_1.appendChild(new_table_name_div);
		//create table for structure and functional regions
		new_table_name_table = document.createElement('table');
		new_table_name_table.setAttribute("cellspacing", "0");
		new_table_name_table.setAttribute("cellpadding", "0");
		_setStyle(new_table_name_table, "width:" + table_widths[i] + "px;");
		new_table_name_div.appendChild(new_table_name_table);
		// create tbody
		new_table_name_tbody = document.createElement('tbody');
		new_table_name_table.appendChild(new_table_name_tbody);
		// create row
		new_table_name_row = document.createElement('tr');
		new_table_name_tbody.appendChild(new_table_name_row);
		// create cell
		new_table_name_cell = document.createElement('td');
		new_table_name_cell.setAttribute("align", "center");
		new_table_name_cell.setAttribute("valign", "middle");
		if(table_mode=="edit")
		{
			_setStyle(new_table_name_cell, "height:" + table_heights[i] + "px; cursor:move;");
		}
		else
		{
			_setStyle(new_table_name_cell, "height:" + table_heights[i] + "px;");
		}
		new_table_name_cell.setAttribute("class", "table_name_cell_bg");
		new_table_name_cell.id = "table_" + (i + 1) + "_name_cell";
		if(table_mode=="edit")
		{
			new_table_name_cell.onmousedown = startDrag2;
		}
		new_table_name_row.appendChild(new_table_name_cell);
		if(table_mode=="edit")
		{
			//create name input
			new_table_name = document.createElement('input');
			new_table_name.setAttribute("type", "text");
			new_table_name.setAttribute("size", "6");
			new_table_name.setAttribute("class", "table_label");
			new_table_name.setAttribute("value", table_names[i]);
			new_table_name.onclick = selectThis;
			new_table_name.onkeyup = transferNameValue;
			new_table_name.name = "table_" + (i + 1) + "_name";
			new_table_name_cell.appendChild(new_table_name);
		}
		else if(table_mode=="view")
		{
			//create name input
			new_table_name = document.createElement('span');
			new_table_name.innerHTML = table_names[i];
			if(table_is_active)
				new_table_name.setAttribute("style", "font-size:10px; color:#ffffff");
			else
				new_table_name.setAttribute("style", "font-size:10px; color:#777777");
			new_table_name_cell.appendChild(new_table_name);
		}
		//create table number hidden value
		new_table_num = document.createElement('input');
		new_table_num.setAttribute("type", "hidden");
		new_table_num.setAttribute("value", (i + 1));
		new_table_div.appendChild(new_table_num);
	}
	if(show_debug_message)
		alert(debug);
}


function placeTable(e) {
	var posx = 0;
	var posy = 0;
	var debug_info = "";
	if (!e) var e = window.event;
	if (e.pageX || e.pageY) 	{
		posx = e.pageX;
		posy = e.pageY;
	}
	else if (e.clientX || e.clientY) 	{
		posx = e.clientX + document.body.scrollLeft	+ document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	
	var setup_div_x = findPosX(document.getElementById('table_setup_div'));
	var setup_div_y = findPosY(document.getElementById('table_setup_div'));

	if ((placing_table == "square") || (placing_table == "circle") || (placing_table == "diamond") || (placing_table == "slant_right") || (placing_table == "slant_left")) {
		document.getElementById('drag_' + placing_table).visibility = 'hidden';

		coord_x[tables_placed - 1] = (posx - setup_div_x);
		coord_y[tables_placed - 1] = (posy - setup_div_y - y_correct);
		table_shapes[tables_placed - 1] = placing_table;
		table_widths[tables_placed - 1] = 70;
		table_heights[tables_placed - 1] = 70;
		table_names[tables_placed - 1] = "Table " + tables_placed;
		
		setInput("coord_x", coord_x.join("|"));
		setInput("coord_y", coord_y.join("|"));
		setInput("shapes", table_shapes.join("|"));
		setInput("widths", table_widths.join("|"));
		setInput("heights", table_heights.join("|"));
		setInput("names", table_names.join("|"));

		//create container div
		new_table_div = document.createElement('div');
		new_table_div.id = "table_" + tables_placed;
		_setStyle(new_table_div, "position:absolute; left:" + (posx - setup_div_x) + "px; top:" + (posy - setup_div_y) + "px; z-index:" + (100 + (tables_placed * 2)) + ";");
		document.getElementById('table_setup_div').appendChild(new_table_div);
				
		//create table for structure and functional regions
		new_table_table = document.createElement('table');
		new_table_table.setAttribute("cellspacing", "0");
		new_table_table.setAttribute("cellpadding", "0");
		new_table_table.onmouseover = HL;
		new_table_table.onmouseout = unHL;
		new_table_div.appendChild(new_table_table);
		// create tbody
		new_table_tbody = document.createElement('tbody');
		new_table_table.appendChild(new_table_tbody);
		// create rows
		new_table_row_1 = document.createElement('tr');
		new_table_tbody.appendChild(new_table_row_1);
		new_table_row_2 = document.createElement('tr');
		new_table_tbody.appendChild(new_table_row_2);
		// create cells
		new_table_cell_1 = document.createElement('td');
		_setStyle(new_table_cell_1, "border-left:1px dotted #BBBBBB; border-top:1px dotted #BBBBBB;");
		new_table_row_1.appendChild(new_table_cell_1);
		new_table_cell_2 = document.createElement('td');
		_setStyle(new_table_cell_2, "width:2px; cursor:e-resize; border-top:1px dotted #BBBBBB; border-right:1px dotted #BBBBBB;");
		new_table_cell_2.onmousedown = resizeW;
		new_table_row_1.appendChild(new_table_cell_2);
		new_table_cell_3 = document.createElement('td');
		_setStyle(new_table_cell_3, "height:2px; cursor:s-resize; border-left:1px dotted #BBBBBB; border-bottom:1px dotted #BBBBBB;");
		new_table_cell_3.onmousedown = resizeH;
		new_table_row_2.appendChild(new_table_cell_3);
		new_table_cell_4 = document.createElement('td');
		_setStyle(new_table_cell_4, "width:2px; height:2px; cursor:se-resize; border-right:1px dotted #BBBBBB; border-bottom:1px dotted #BBBBBB;");
		new_table_cell_4.onmousedown = resizeWH;
		new_table_row_2.appendChild(new_table_cell_4);
		//create table img
		new_table_img = document.createElement('img');
		new_table_img.src = image_path + "images/table_" + placing_table + ".png";
		debug_info += image_path + "images/table_" + placing_table + ".png\n";
		_setStyle(new_table_img, "width:70px; height:70px;");
		new_table_img.id = "table_" + tables_placed + "_img";
		new_table_cell_1.appendChild(new_table_img);
		//create continer div for table name
		new_table_name_div = document.createElement('div');
		_setStyle(new_table_name_div, "position:absolute; left:0px; top:0px; width:70px; height:70px; z-index:" + (101 + (tables_placed * 2)) + "; text-align:center; vertical-align:middle;");
		new_table_cell_1.appendChild(new_table_name_div);
		//create table for structure and functional regions
		new_table_name_table = document.createElement('table');
		new_table_name_table.setAttribute("cellspacing", "0");
		new_table_name_table.setAttribute("cellpadding", "0");
		_setStyle(new_table_name_table, "width:70px;");
		new_table_name_div.appendChild(new_table_name_table);
		// create tbody
		new_table_name_tbody = document.createElement('tbody');
		new_table_name_table.appendChild(new_table_name_tbody);
		// create row
		new_table_name_row = document.createElement('tr');
		new_table_name_tbody.appendChild(new_table_name_row);
		// create cell
		new_table_name_cell = document.createElement('td');
		new_table_name_cell.setAttribute("align", "center");
		new_table_name_cell.setAttribute("valign", "middle");
		_setStyle(new_table_name_cell, "height:70px; cursor:move;");
		new_table_name_cell.setAttribute("class", "table_name_cell_bg");
		new_table_name_cell.id = "table_" + tables_placed + "_name_cell";
		new_table_name_cell.onmousedown = startDrag2;
		new_table_name_row.appendChild(new_table_name_cell);
		//create name input
		new_table_name = document.createElement('input');
		new_table_name.setAttribute("type", "text");
		new_table_name.setAttribute("size", "6");
		new_table_name.setAttribute("class", "table_label");
		new_table_name.setAttribute("value", "Table " + tables_placed);
		new_table_name.onclick = selectThis;
		new_table_name.onkeyup = transferNameValue;
		new_table_name.name = "table_" + tables_placed + "_name";
		new_table_name_cell.appendChild(new_table_name);
		//create table number hidden value
		new_table_num = document.createElement('input');
		new_table_num.setAttribute("type", "hidden");
		new_table_num.setAttribute("value", tables_placed);
		new_table_div.appendChild(new_table_num);
		
		tables_placed++;
		placing_table = "none";

	} else {
		coord_x[table_number - 1] = setX;
		coord_y[table_number - 1] = (setY - y_correct);

		setInput("coord_x", coord_x.join("|"));
		setInput("coord_y", coord_y.join("|"));
	}
}

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent) {
	do {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		} while (obj = obj.offsetParent);
	}
	return [curleft,curtop];
}

function startDrag(id, e) {
	if (!e) var e = window.event;
	
	var ScrollTop = document.body.scrollTop;
	var offset_root = findPos(document.getElementById('root'));
	var offset_left = offset_root[0];
	var offset_top = offset_root[1];
	
	if (ScrollTop == 0) {
		if (window.pageYOffset) {
			ScrollTop = window.pageYOffset;
		} else {
			ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
		}
	}

	var mouseX = e.clientX;
	var mouseY = e.clientY;

	is_dragging = true;

	drag_table = document.getElementById('drag_' + id);
	drag_table.style.left = (mouseX - offset_left) + "px";
	drag_table.style.top = ((mouseY - offset_top) + drag_table.offsetHeight + ScrollTop) + "px";
	drag_table.style.visibility = 'visible';
	placing_table = id;

	var dragX = parseInt(drag_table.style.left);
  	var dragY = parseInt(drag_table.style.top);

	offsetX = mouseX - dragX;
	offsetY = mouseY - dragY;

	return false;
}

function startDrag2(e) {
	if (window.event) e = window.event;

	var mouseX = e.clientX;
	var mouseY = e.clientY;
	
	is_dragging2 = true;
	drag_table = this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
	placing_table = drag_table.id;
	table_number = drag_table.childNodes[1].value;

	table_startX = parseInt(drag_table.style.left);
	table_startY = parseInt(drag_table.style.top);
	var dragX = parseInt(drag_table.style.left);
  	var dragY = parseInt(drag_table.style.top);

	offsetX = mouseX - dragX;
	offsetY = mouseY - dragY;

	setX = table_startX;
	setY = table_startY;

	return false;
}

function moveTable(e) {
	if (window.event) e = window.event;

	var ScrollTop = document.body.scrollTop;

	if (ScrollTop == 0) {
		if (window.pageYOffset) {
			ScrollTop = window.pageYOffset;
		} else {
			ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
		}
	}

	if ((is_dragging == true) || (is_dragging2 == true)) {
		var newX = e.clientX - offsetX;
		var newY = e.clientY - offsetY;	

		drag_table.style.left = newX + "px";
		drag_table.style.top = newY + "px";
		
		setX = newX;
		setY = newY;

	} else if ((is_resizingW == true) || (is_resizingH == true) || (is_resizingWH == true)) {
		var newX = e.clientX;
		var newY = e.clientY;
		
		var table_pos_x = findPosX(resize_img);
		var table_pos_y = findPosY(resize_img);
		var floor_border_r = findPosX(document.getElementById('room_floor')) + document.getElementById('room_floor').offsetWidth - 3;
		var floor_border_b = findPosY(document.getElementById('room_floor')) + document.getElementById('room_floor').offsetHeight - 3;
	}
	
	if (is_resizingW == true) {
		if (((startW + (newX - start_resizeX)) > 50) && ((startW + (newX - start_resizeX)) < 350) && ((startW + (newX - start_resizeX)) < (floor_border_r - table_pos_x))) {
			resize_img.style.width = (startW + (newX - start_resizeX)) + "px";
			resize_table.style.width = (startW + (newX - start_resizeX)) + "px";
			table_widths[table_number - 1] = (startW + (newX - start_resizeX));
			setInput("widths", table_widths.join("|"));
		}
	}

	if (is_resizingH == true) {
		if (((startH + (newY - start_resizeY)) > 50) && ((startH + (newY - start_resizeY)) < 350) && ((startH + (newY - start_resizeY)) < (floor_border_b - table_pos_y))) {
			resize_img.style.height = (startH + (newY - start_resizeY)) + "px";
			resize_cell.style.height = (startH + (newY - start_resizeY)) + "px";
			table_heights[table_number - 1] = (startH + (newY - start_resizeY));
			setInput("heights", table_heights.join("|"));
		}
	}

	if (is_resizingWH == true) {
		if (((startW + (newX - start_resizeX)) > 50) && ((startW + (newX - start_resizeX)) < 350) && ((startW + (newX - start_resizeX)) < (floor_border_r - table_pos_x))) {
			resize_img.style.width = (startW + (newX - start_resizeX)) + "px";
			resize_table.style.width = (startW + (newX - start_resizeX)) + "px";
			table_widths[table_number - 1] = (startW + (newX - start_resizeX));
			setInput("widths", table_widths.join("|"));
		}
		if (((startH + (newY - start_resizeY)) > 50) && ((startH + (newY - start_resizeY)) < 350) && ((startH + (newY - start_resizeY)) < (floor_border_b - table_pos_y))) {
			resize_img.style.height = (startH + (newY - start_resizeY)) + "px";
			resize_cell.style.height = (startH + (newY - start_resizeY)) + "px";
			table_heights[table_number - 1] = (startH + (newY - start_resizeY));
			setInput("heights", table_heights.join("|"));
		}
	}

	return false;
}

function stopDrag(e) {
	if (window.event) e = window.event;
	
	if ((is_dragging == true) || (is_dragging2 == true)) {
		var mouseX = e.clientX;
		var mouseY = e.clientY;
		var floorX = findPosX(document.getElementById('room_floor'));
		var floorY = findPosY(document.getElementById('room_floor'));
		var floorW = document.getElementById('room_floor').offsetWidth;
		var floorH = document.getElementById('room_floor').offsetHeight;
		
		if ((placing_table == "square") || (placing_table == "circle") || (placing_table == "diamond") || (placing_table == "slant_right") || (placing_table == "slant_left")) {
			document.getElementById('drag_' + placing_table).style.visibility = 'hidden';
			var tableX = findPosX(document.getElementById('drag_' + placing_table));
			var tableY = findPosY(document.getElementById('drag_' + placing_table));
			var tableW = document.getElementById('drag_' + placing_table).offsetWidth;
			var tableH = document.getElementById('drag_' + placing_table).offsetHeight;
		} else {
			var tableX = findPosX(document.getElementById(placing_table));
			var tableY = findPosY(document.getElementById(placing_table));
			var tableW = document.getElementById(placing_table).offsetWidth;
			var tableH = document.getElementById(placing_table).offsetHeight;
		}
	
		if (((tableX > floorX) && (tableX < (floorX + floorW - tableW))) && ((tableY > floorY) && (tableY < (floorY + floorH - tableH)))) {
			placeTable(e);
		} else {
			if ((placing_table != "square") && (placing_table != "circle") && (placing_table != "diamond") && (placing_table != "slant_right") && (placing_table != "slant_left")) {
				document.getElementById(placing_table).style.left = table_startX;
				document.getElementById(placing_table).style.top = table_startY;
			}
		}
	
		is_dragging = false;
		is_dragging2 = false;

	} else if (is_resizingW == true) {
		
		is_resizingW = false;

	} else if (is_resizingH == true) {
		
		is_resizingH = false;

	} else if (is_resizingWH == true) {
		
		is_resizingWH = false;
	}
	return false;
}

function resizeW(e) {
	if (window.event) e = window.event;
	
	resize_img = this.previousSibling.childNodes[0];
	resize_table = this.previousSibling.childNodes[1].childNodes[0];
	table_number = this.parentNode.parentNode.parentNode.nextSibling.value;
	
	start_resizeX = e.clientX;
	startW = parseInt(resize_img.offsetWidth);
	
	is_resizingW = true;


	return false;
}

function resizeH(e) {
	if (window.event) e = window.event;
	
	resize_img = this.parentNode.previousSibling.childNodes[0].childNodes[0];
	resize_cell = this.parentNode.previousSibling.childNodes[0].childNodes[1].childNodes[0].childNodes[0].childNodes[0].childNodes[0];
	table_number = this.parentNode.parentNode.parentNode.nextSibling.value;
	
	start_resizeY = e.clientY;
	startH =  parseInt(resize_img.offsetHeight);
	
	is_resizingH = true;
	
	return false;
}

function resizeWH(e) {
	if (window.event) e = window.event;	

	resize_img = this.parentNode.previousSibling.childNodes[0].childNodes[0];
	resize_table = this.parentNode.previousSibling.childNodes[0].childNodes[1].childNodes[0];
	resize_cell = this.parentNode.previousSibling.childNodes[0].childNodes[1].childNodes[0].childNodes[0].childNodes[0].childNodes[0];
	table_number = this.parentNode.parentNode.parentNode.nextSibling.value;

	start_resizeX = e.clientX;
	start_resizeY = e.clientY;
	startW = resize_img.offsetWidth;
	startH = resize_img.offsetHeight;
	
	is_resizingWH = true;

	return false;
}

function HL() {
	this.childNodes[0].childNodes[0].childNodes[0].style.borderLeft = "1px dotted #000000";
	this.childNodes[0].childNodes[0].childNodes[0].style.borderTop = "1px dotted #000000";
	this.childNodes[0].childNodes[0].childNodes[1].style.borderTop = "1px dotted #000000";
	this.childNodes[0].childNodes[0].childNodes[1].style.borderRight = "1px dotted #000000";
	this.childNodes[0].childNodes[1].childNodes[0].style.borderLeft = "1px dotted #000000";
	this.childNodes[0].childNodes[1].childNodes[0].style.borderBottom = "1px dotted #000000";
	this.childNodes[0].childNodes[1].childNodes[1].style.borderBottom = "1px dotted #000000";
	this.childNodes[0].childNodes[1].childNodes[1].style.borderRight = "1px dotted #000000";
}

function unHL() {
	this.childNodes[0].childNodes[0].childNodes[0].style.borderLeft = "1px dotted #BBBBBB";
	this.childNodes[0].childNodes[0].childNodes[0].style.borderTop = "1px dotted #BBBBBB";
	this.childNodes[0].childNodes[0].childNodes[1].style.borderTop = "1px dotted #BBBBBB";
	this.childNodes[0].childNodes[0].childNodes[1].style.borderRight = "1px dotted #BBBBBB";
	this.childNodes[0].childNodes[1].childNodes[0].style.borderLeft = "1px dotted #BBBBBB";
	this.childNodes[0].childNodes[1].childNodes[0].style.borderBottom = "1px dotted #BBBBBB";
	this.childNodes[0].childNodes[1].childNodes[1].style.borderBottom = "1px dotted #BBBBBB";
	this.childNodes[0].childNodes[1].childNodes[1].style.borderRight = "1px dotted #BBBBBB";
}

function keyCmd(e) {
	if (window.event) e = window.event;	

	if ((e.keyCode == 68) && (is_dragging2 == true)) { // delete table
		document.getElementById('table_setup_div').removeChild(document.getElementById('table_' + table_number));
		coord_x[table_number - 1] = "";
		coord_y[table_number - 1] = "";
		table_shapes[table_number - 1] = "";
		table_widths[table_number - 1] = "";
		table_heights[table_number - 1] = "";
		table_names[table_number - 1] = "";

		setInput("coord_x", coord_x.join("|"));
		setInput("coord_y", coord_y.join("|"));
		setInput("shapes", table_shapes.join("|"));
		setInput("widths", table_widths.join("|"));
		setInput("heights", table_heights.join("|"));
		setInput("names", table_names.join("|"));

		is_dragging2 = false;
	} else if ((e.keyCode == 37) && (is_dragging2 == true)) {
		if (e.preventDefault) { e.preventDefault();
		} else { e.returnValue = false;	}
		currentX = parseInt(document.getElementById('table_' + table_number).style.left);
		if (currentX > 2) {
			document.getElementById('table_' + table_number).style.left = (currentX - 1) + "px";
			setX = (currentX - 1);
		}
	} else if ((e.keyCode == 38) && (is_dragging2 == true)) {
		if (e.preventDefault) { e.preventDefault();
		} else { e.returnValue = false;	}
		currentY = parseInt(document.getElementById('table_' + table_number).style.top);
		if (currentY > 82) {
			document.getElementById('table_' + table_number).style.top = (currentY - 1) + "px";
			setY = (currentY - 1);
		}
	} else if ((e.keyCode == 39) && (is_dragging2 == true)) {
		if (e.preventDefault) { e.preventDefault();
		} else { e.returnValue = false;	}
		currentX = parseInt(document.getElementById('table_' + table_number).style.left);
		currentW = parseInt(document.getElementById('table_' + table_number).offsetWidth);
		if (currentX < (894 - currentW)) {
			document.getElementById('table_' + table_number).style.left = (currentX + 1) + "px";
			setX = (currentX + 1);
		}
	} else if ((e.keyCode == 40) && (is_dragging2 == true)) {
		if (e.preventDefault) { e.preventDefault();
		} else { e.returnValue = false;	}
		currentY = parseInt(document.getElementById('table_' + table_number).style.top);
		currentH = parseInt(document.getElementById('table_' + table_number).offsetHeight);
		if (currentY < (894 - currentH)) {
			document.getElementById('table_' + table_number).style.top = (currentY + 1) + "px";
			setY = (currentY + 1);
		}
	}
}

function setInput(id, value) {
	document.getElementById(id).value = value;
}

function transferNameValue() {
	
	if (this.value == undefined) {
		this_table_name = " ";
	} else if (this.value == "") {
		this_table_name = " ";
	} else {
		this_table_name = this.value;
	}
	
	table_number = this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.nextSibling.value;
	table_names[table_number - 1] = this_table_name;

	setInput("names", table_names.join("|"));
}

function selectThis() {
	this.select();
}

function showTableDetails(e) {
	if (window.event) e = window.event;
	if(table_mouseover=="showTableDetails")
	{
		var itd = document.getElementById("info_table_display");
		var itdc = document.getElementById("info_table_display_content");
		itd.style.visibility = "visible";
		itd.style.left = this.offsetLeft - 60;
		itd.style.top = this.offsetTop - 40;
		set_tablename = table_info[this.id];
		itd.onmouseout = hideTableDetails;
		//alert("click");
		itdc.innerHTML = "<iframe frameborder='0' allowtransparency='yes' scrolling='no' width='160' height='100' src='resources/general_dialog.php?area=table_view&mode=table_info&tablename=" + set_tablename + "'></iframe>";
	}
}

function clickTable(e) {
	if(window.event) e = window.event;
	if(table_mouseover=="manage")
	{
		set_tablename = table_info[this.id];
		table_clicked(set_tablename);
		//alert("clicked " + set_tablename);
	}
}

function hideTableDetails() {
	document.getElementById("info_table_display").style.visibility = "hidden";
	document.getElementById("info_table_display_content").innerHTML = "&nbsp;";
}

</script>
