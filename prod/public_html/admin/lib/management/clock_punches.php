<?php
	$display .= speak("Currently Clocked In").":<br><br>";
	
	function show_clock_time($t,$type="all")
	{
		$ts = strtotime($t);
		$date_str = "";
		$time_str = "";

		$date_setting = "date_format";
		$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_option = array("value" => 2);
		}else
			$date_option = mysqli_fetch_assoc( $location_date_format_query );

		if(date("Y-m-d",$ts)==date("Y-m-d"))
		{
			$date_str = "Today";
			$time_str = date("h:i a",$ts);
		}
		else
		{
			switch ($date_option['value']){
				case 1:
					$date_str = date("d/m/Y",$ts);
				 break;
				case 2:
					$date_str = date("m/d/Y",$ts);
				 break;
				case 3:
					$date_str = date("Y/m/d",$ts);
				 break;
			}
			//$date_str = date("m/d/Y",$ts);
			$time_str = date("h:i a",$ts);
		}
		
		if($type=="date")
			return $date_str;
		else if($type=="time")
			return $time_str;
		else
			return $date_str . " - " . $time_str;
	}
	function lzero($n)
	{
		$n = $n * 1;
		if($n < 10) return "0" . $n;
		else return $n;
	}
	function get_hours_difference($t1, $t2)
	{
		//echo $t1 . "<br>" . $t2 . "<br>------------";
		$clockInArray = explode(" ", $t1);
		$clockInDate = explode("-", $clockInArray[0]);
		$clockInTime = explode(":", $clockInArray[1]);
		$inStamp = mktime($clockInTime[0], $clockInTime[1], 0, $clockInDate[1], $clockInDate[2], $clockInDate[0]);

		$clockOutArray = explode(" ", $t2);
		$clockOutDate = explode("-", $clockOutArray[0]);
		$clockOutTime = explode(":", $clockOutArray[1]);
		$outStamp = mktime($clockOutTime[0], $clockOutTime[1], 0, $clockOutDate[1], $clockOutDate[2], $clockOutDate[0]);

		$hours = (($outStamp - $inStamp) / 3600);
		return $hours;
	}
	
	/*
	 * function to determine the device time
	 */
	function determineDeviceTime($location_info, $post_vars) {
	
		$return_time = date("Y-m-d H:i:s");
		$tz_convert = true;
	
		if (isset($post_vars['device_time'])) {
			$datetime_parts = explode(" ", $post_vars['device_time']);
			$date_parts = explode("-", $datetime_parts[0]);
			$time_parts = explode(":", $datetime_parts[1]);
			$device_ts = mktime((int)$time_parts[0], (int)$time_parts[1], (int)$time_parts[2], (int)$date_parts[1], (int)$date_parts[2], (int)$date_parts[0]);
			/*if (($date_parts[0] - date("Y")) > 500) {
				$tz_convert = false;
				$return_time = $post_vars['device_time'];
				} else*/ if (abs(time() - $device_ts) < 2678400) {
				$tz_convert = false;
				$return_time = date("Y-m-d H:i:s", $device_ts);
			}
		}
	
		$timezone = (isset($location_info['timezone']))?$location_info['timezone']:"";
		if ($tz_convert && $timezone!="") {
			$return_time = localize_datetime($return_time, $location_info['timezone']);
		}
	
		return $return_time;
	}

	$timez = $location_info['timezone'];
	
	$burl = "inapp_management.php?mode=clock_punches&".$fwd_vars;
	
	$cmd = getvar("cmd","");
	$punch_id = getvar("punch_id","");
	$new_time = getvar("new_time","");
	$should_check_punch_status = false;
	global $location_info;
	$device_time = determineDeviceTime($location_info, $_REQUEST);
	if($cmd=="delete_punch" && $punch_id!="")
	{
		$punch_query = lavu_query("select `time`, `server`, `server_id`, `location_id` ,`time` ,`time_out` from `clock_punches` where `id`='[1]'",$punch_id);
		if(mysqli_num_rows($punch_query))
		{
			$punch_read = mysqli_fetch_assoc($punch_query);
			
			$set_time_in = $punch_read['time'];
			//$set_time_out = date("Y-m-d H:i:s");
			$server_name = $punch_read['server'];
			$should_check_punch_status = $punch_read['server_id'];
			//$set_hours = number_format(get_hours_difference($set_time_in, $set_time_out),2,".","");
			lavu_query("update `clock_punches` set `_deleted`='1' where `id`='[1]'",$punch_id);
			
			$display .= speak('Deleted punch for') . " <b>$server_name</b> " . speak('at') . ' ' . $set_time_in . "<br><br>";

			//update action log table for showing in reports
			$device_id = determineDeviceIdentifier($_REQUEST);
			date_default_timezone_set($location_info['timezone']);
			$log_vars = array(
					"action"		=> "Clock Punch Deleted",
					"loc_id"		=> $punch_read['location_id'],
					"order_id"		=> "",
					"time"			=> $device_time,
					"user"			=> $server_name,
					"user_id"		=> $should_check_punch_status,
					"device_udid"	=> $device_id,
					"details"		=> speak('User') . " : ".$server_name."\n\n". speak('Clock In Time') . ": ".$punch_read['time']."\n\n". speak('Clock Out Time') . ": ".$punch_read['time_out']." \n\n". speak('Time Of Delete') . ":  " . date("Y-m-d H:i:s")."\n\n ". speak('Deleted By') . ": ".$server_name,
					"details_short"	=> "",
					"ialid"			=> newInternalID()
			);

			$keys = array_keys($log_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);
			//end
		}
	}
	else if($cmd=="clockout" && $punch_id!="")
	{
		$punch_query = lavu_query("select `time`, `time_out`, `server`, `server_id`, `location_id` from `clock_punches` where `id`='[1]'",$punch_id);
		if(mysqli_num_rows($punch_query))
		{
			$punch_read = mysqli_fetch_assoc($punch_query);
			
			$set_time_in = $punch_read['time'];
			$set_time_out = localize_datetime(date("Y-m-d H:i:s"),$timez);
			$server_name = $punch_read['server'];
			$should_check_punch_status = $punch_read['server_id'];
			$set_hours = number_format(get_hours_difference($set_time_in, $set_time_out),2,".","");
			lavu_query("update `clock_punches` set `time_out`='[1]', `punched_out`='1', `hours`='[2]' where `id`='[3]' and `time_out`=''",$set_time_out,$set_hours,$punch_id);
			
			$display .= speak('Punched Out') . " <b>$server_name</b> ". speak('at') . $set_time_out . "<b>($set_hours ". speak('Hours') . ")</b><br><br>";
		}
	}
	else if($cmd=="change_clockin" && $punch_id!="" && $new_time!="")
	{
		$punch_query = lavu_query("select `time`, `server`, `server_id`, `location_id` from `clock_punches` where `id`='[1]'",$punch_id);
		if(mysqli_num_rows($punch_query))
		{
			$punch_read = mysqli_fetch_assoc($punch_query);
			
			$set_time_in_parts = explode(" ",$punch_read['time']);
			$set_time_in = $set_time_in_parts[0] . " " . $new_time;
			
			$server_name = $punch_read['server'];
			$should_check_punch_status = $punch_read['server_id'];
			if(localize_datetime(date("Y-m-d H:i:s"),$timez) < $set_time_in)
			{
				$display .= speak('Cannot Punch-In in the future') . "<br><br>";
			}
			else
			{
				//$set_time_out = date("Y-m-d H:i:s");
				//$set_hours = number_format(get_hours_difference($set_time_in, $set_time_out),2,".","");
				lavu_query("update `clock_punches` set `time`='[1]' where `id`='[2]'",$set_time_in,$punch_id);
				
				$display .= speak('Changed Punch In for') . " <b>$server_name</b> ". speak('to') . " <b>" . show_clock_time($set_time_in) . "</b><br><br>";

				//update action log table for showing in reports
				$device_id = determineDeviceIdentifier($_REQUEST);
				$userClockInfo = speak('User') . ': '. $punch_read['server'];
				$originalTime =  speak('Original Clock In Time') . ': ' . $punch_read['time'];
				$newTime = speak('New Clock In Time') . ': ' . $set_time_in;
				$timeOfChange = speak('Time Of Change') . ': ' . date("Y-m-d H:i:s");
				$changedByUser = speak('Changed By') . ': ' . $server_name;

				$details = array($userClockInfo, $originalTime, $newTime, $timeOfChange, $changedByUser);
				$details = implode(' \n\n ', $details);
				date_default_timezone_set($location_info['timezone']);
				$log_vars = array(
						"action"		=> "Clock In Time Edited",
						"loc_id"		=> $punch_read['location_id'],
						"order_id"		=> "",
						"time"			=> $device_time,
						"user"			=> $server_name,
						"user_id"		=> $should_check_punch_status,
						"device_udid"	=> $device_id,
						"details"		=> $details,
						"details_short"	=> "",
						"ialid"			=> newInternalID()
				);

				$keys = array_keys($log_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") {
						$q_fields .= ", ";
						$q_values .= ", ";
					}
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);
				//end
			}
		}
	}

	if ($should_check_punch_status) checkPunchStatusAndSetFlag($should_check_punch_status, $location_info);
		
	$display .= "<script language='javascript'>";
	$display .= "function clockout_server(punch_id) { ";
	$display .= "   window.location = \"$burl&cmd=clockout&punch_id=\" + punch_id; ";
	$display .= "} ";
	$display .= "function delete_punch(punch_id) { ";
	$display .= "   window.location = \"$burl&cmd=delete_punch&punch_id=\" + punch_id; ";
	$display .= "} ";
	$display .= "function change_clockin_time(punch_id,new_time) { ";
	$display .= "   window.location = \"$burl&cmd=change_clockin&punch_id=\" + punch_id + \"&new_time=\" + new_time; ";
	$display .= "} ";
	$display .= "function clock_display_click(punch_id) { ";
	$display .= "   document.getElementById('clock_display_' + punch_id).style.display = 'none'; ";
	$display .= "   document.getElementById('clock_edit_' + punch_id).style.display = 'block'; ";
	$display .= "} ";
	$display .= "</script>";
	$display .= "<table style='border:solid 1px #888888' cellpadding=4>";
	$punch_query = lavu_query("select * from `clock_punches` where `location_id`='[1]' and `time_out`='' and _deleted!='1' order by `time` asc", $loc_id);
	while($punch_read = mysqli_fetch_assoc($punch_query))
	{
		//foreach($punch_read as $key => $val) $display .= $key . " = " . $val . "<br>";
		$time_in = $punch_read['time'];
		$server_name = $punch_read['server'];
		$server_id = $punch_read['server_id'];
		$punch_id = $punch_read['id'];
		
		$co_title = rawurlencode("Clock Out");
		$co_message = rawurlencode("Are you sure you want to clock out $server_name?");
		$co_cmd = rawurlencode("clockout_server($punch_id)");

		$dco_title = rawurlencode("Delete Punch");
		$dco_message = rawurlencode("Are you sure you want to delete the punch for $server_name?");
		$dco_cmd = rawurlencode("delete_punch($punch_id)");
		
		$display .= "<tr>";
		$display .= "<td>$server_name</td>";
		$display .= "<td>";
		
		$display .= "<div style='display:block; cursor:pointer' id='clock_display_$punch_id' onclick='clock_display_click($punch_id)'>";
		$display .= "In: " . show_clock_time($time_in);
		$display .= "</div>";
		$display .= "<div style='display:none' id='clock_edit_$punch_id'>";
		
		$display .= "<table cellspacing=0 cellpadding=0><tr>";
		$display .= "<td>In: " . show_clock_time($time_in,"date") . "&nbsp;</td>";
		
		$display .= "<td>";
		$display .= "<select onchange='change_clockin_time(\"$punch_id\",this.value)'>";
		$current_t_display = show_clock_time($time_in,"time");
		for($i=0; $i<2400; $i++)
		{
			$mins = $i % 100;
			if($mins < 60)
			{
				$hours = ($i - $mins) / 100;
				
				$t = lzero($hours) . ":" . lzero($mins) . ":00";
				$t_display = date("h:i a",mktime($hours,$mins,0,date("m"),date("d"),date("Y")));
				
				$display .= "<option value='$t'";
				if($current_t_display==$t_display) $display .= " selected";
				$display .= ">" . $t_display . "</option>";
			}
		}
		$display .= "</select>";
		$display .= "</td>";
		
		/*$display .= "<td>";
		$display .= "<select>";
		for($i=1; $i<12; $i++)
		{
			$istr = lzero($i);
			$display .= "<option value='$istr'";
			$display .= ">$istr</option>";
		}
		$display .= "</select>";
		$display .= "</td><td>";
		$display .= "<select>";
		for($i=0; $i<60; $i++)
		{
			$istr = lzero($i);
			$display .= "<option value='$istr'>$istr</option>";
		}
		$display .= "</select>";
		$display .= "</td>";
		$display .= "<td>";
		$display .= "<select>";
		$display .= "<option value='am'>am</option>";
		$display .= "<option value='pm'>pm</option>";
		$display .= "</select>";
		$display .= "</td>";*/
		$display .= "</tr></table>";
		
		$display .= "</div>";
		
		$display .= "</td>";
		$display .= "<td><input type='button' value='".speak("Clock Out")."' onclick='window.location = \"_DO:cmd=confirm&title=$co_title&message=$co_message&js_cmd=$co_cmd\"'></td>";
		$display .= "<td><input type='button' value='".speak("Delete")."' onclick='window.location = \"_DO:cmd=confirm&title=$dco_title&message=$dco_message&js_cmd=$dco_cmd\"'></td>";
		$display .= "</tr>";
	}
	$display .= "</table>";
?>
