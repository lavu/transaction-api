<?php

	$step = (isset($_REQUEST['step']))?$_REQUEST['step']:"";

	if ($step == "1") {

		$display .= "<table width='600px'>";
 		$display .= "<tr><td align='center'><b>".speak("Settle Credit Card Batch")."</b><br><br></td></tr>";
		$display .= "<tr><td align='left'>".speak("Proceed with credit card transaction batch settlement only if all tip amounts have been entered and applied. If you are in the process of adjusting tips for more than one day, you must send all tip adjustments for orders created since the last batch settlement before settling the current credit card batch").".</td></tr>";
		$display .= "<tr><td align='center'><br><br><input type='button' style='font-size:16px;' onclick='window.location = \"inapp_management.php?mode=cc_settle&step=2&".$fwd_vars."\"' value='".speak("Proceed with Batch Settlement")."'></td></tr>";
		$display .= "</table>";
	
	} else if ($step == "2") {
	
		require_once(dirname(__FILE__)."/../gateway_functions.php");
		require_once(dirname(__FILE__)."/../mpshc_functions.php");

		$process_info = array();
		$process_info['mag_data'] = "";
		$process_info['reader'] = "";
		$process_info['card_number'] = "";
		$process_info['card_cvn'] = "";
		$process_info['name_on_card'] = "";
		$process_info['exp_month'] = "";
		$process_info['exp_year'] = "";
		$process_info['card_amount'] = "";
		$process_info['company_id'] = $company_info['id'];
		$process_info['loc_id'] = $location_info['id'];
		$process_info['ext_data'] = "";
		$process_info['data_name'] = $data_name;
		$process_info['register'] = $register;
		$process_info['register_name'] = $register_name;
		$process_info['device_udid'] = $UDID;
		$process_info['server_id'] = $server_id;
		$process_info['server_name'] = $server_name;
		$process_info['device_time'] = $device_time;
		$process_info['set_pay_type'] = "Card";
		$process_info['set_pay_type_id'] = "2";
		$process_info['for_deposit'] = "";
	
		$dataname = $data_name;
		$companyid = $company_info['id'];
		$locationid = $location_info['id'];
		$serverid = $server_id;
	
		require_once(resource_path()."/../areas/reports/settle_batch.php");
		
		$display .= "<table>".$mid."</table>";
	}
?>