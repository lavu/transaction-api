<?php

    //ini_set('display_errors', 1);
    
/*
1|receipt::---------------------------:Server End of Day::---------------------------::SALES[c::Cash Sales[c 0.00::Credit Card Sales[c 0.00::Total Sales[c [0.00]::---------------------------::AMOUNT DUE[c::Restaurant To Pay Out Server[c [0.00]::---------------------------::TILL[c::Till-In[c 0.00::Till-Out[c 0.00::---------------------------::TIPS[c::Credit Card Tips[c 0.00::Cash Tips[c 0.00::Total Tips[c [0.00]::---------------------------::Tipout Rules[c::::---------------------------::DISCOUNTS[c::---------------------------::SUPERGROUPS[c:::::::
*/

    //This file is used to print out the receipt data for a server_tips submission.
    //This data can be found in a cash data row, or by actually just posting the submission arr.
    //The former makes more sense as we print a submitted report.
    //If the following request var is non-empty then that row will be used from cash data,
    //else it is assumed the array is being sent in:   $_REQUEST['server_submission_post_arr']
    $postData_ = $_REQUEST['post_data'];
    $cash_data_server_tips_row_id = explode("(eq)", $postData_);
    $cash_data_server_tips_row_id = $cash_data_server_tips_row_id[1];


    $scriptArgs = array();
    $argPairs = explode('(and)', $postData_);
    foreach($argPairs as $currPair){
        $currArgPair = explode('(eq)', $currPair);
        $scriptArgs[$currArgPair[0]] = $currArgPair[1];
    }

    $data_name = $scriptArgs['data_name'];
    $secHash = $scriptArgs['sec_hash'];

    $_POST['cc'] = $_REQUEST['cc'] = $_GET['cc'] = $data_name;

    require_once(dirname(__FILE__) . '/../../info_inc.php');

    $isLLS = strpos(__FILE__, 'poslavu-local') !== false;


    $nonSpecial = empty($_GET['test_query']) && empty($_GET['get_saved_post']);

    if($nonSpecial){
        $receiptDocument = generateFullReceiptString();
        echo $receiptDocument;
    }
    else if(!empty($_GET['get_saved_post'])){
        $originalPost = getOriginalServerTipsSubmissionPost($cash_data_server_tips_row_id);
        echo "Original post: <pre> ".print_r($originalPost,1)."</pre>";
    }
    else if(!empty($_GET['test_query'])){

        $paymentReportReport = getPaymentsReportAsPertainsServer(1, $data_name, '2013-09-03 04:30:00', '2013-09-04 04:30:00');
        $superGroupReport    = getSuperGroupsReport(1, $data_name, '2013-09-03 04:30:00', '2013-09-04 04:30:00');
        $checkDiscountReport = getCheckDiscountsReport(1, $data_name, '2013-09-03 04:30:00', '2013-09-04 04:30:00');
        $itemDiscountReport  = getItemDiscountsReport(1, $data_name, '2013-09-03 04:30:00', '2013-09-04 04:30:00');

        echo 'paymentReportReport:<br>' . nl2br(print_r($paymentReportReport, 1));
        echo '<br>-------------------------------------------------------------------------------------------------<br><br><br>';
        echo 'superGroupReport:<br>'    . nl2br(print_r($superGroupReport, 1));
        echo '<br>-------------------------------------------------------------------------------------------------<br><br><br>';
        echo 'checkDiscountReport:<br>' . nl2br(print_r($checkDiscountReport, 1));
        echo '<br>-------------------------------------------------------------------------------------------------<br><br><br>';
        echo 'itemDiscountReport:<br>'  . nl2br(print_r($itemDiscountReport, 1));
        echo '<br>-------------------------------------------------------------------------------------------------<br><br><br>';
    }


    function generateFullReceiptString(){
        global $data_name;
        global $cash_data_server_tips_row_id;
        global $decimalPlaces, $decimalChar, $thousandsChar;
        /*
        //Original Post Parameters
        is_tilling_out
        total_tips
        cash_tips
        cc_tips
        over_short
        till_out_total
        till_out_bills
        till_out_coins
        server_id
        location_id
        tipout_mappings
        number_of_decimal_places
        receipt_data
        fiscal_day_start
        */
        $originalPost = getOriginalServerTipsSubmissionPost($cash_data_server_tips_row_id);

        $fiscalDayStart = $originalPost['fiscal_day_start'];
        $fiscalDayEnd   = date('Y-m-d H:i:s', strtotime('+ 1 day', strtotime($fiscalDayStart)));

        //Number formatters
        //We can use the following below for:    number_format  if we choose to use it
        $locationID  = $originalPost['location_id'];
        $locationRow = getLocationRow($locationID);

        //echo "<pre>" . print_r($locationRow,1) . "</pre>";

        $decimalPlaces = $locationRow['disable_decimal'];
        $decimalChar   = $locationRow['decimal_char'];
        $thousandsChar = $locationRow['thousands_char'];

        //We need to ensure correct values from the above number formatting options.
        if(empty($decimalChar)){
            $decimalChar = '.';
            $thousandsChar = ',';
        }
        else if ($decimalChar == '.'){
            $thousandsChar = empty($thousandsChar) ? ',' : $thousandsChar;
        }
        else if ($decimalChar == ','){
            $thousandsChar = empty($thousandsChar) ? '.' : $thousandsChar;
        }
        $decimalPlaces = empty($decimalPlaces) ? 2 : $decimalPlaces;


        $lines = generateRepeatedCharacter('-',27);

        if(0){
            echo nl2br(print_r($originalPost,1));
            return;
        }
        //Building the report
        ////$print_string  = 'receipt::';

        $p_register = !empty($_REQUEST['register']) ? $_REQUEST['register'] : 'receipt';
        $print_string = $p_register . '::';
        $print_string .= $lines . ":";
        $print_string .= "Server End of Day::";

        //Server Recon, over/short amount.
        $totalSales      = $originalPost['total_sales'];
        $totalCashSales  = $originalPost['total_cash_sales'];
        $creditCardSales = (is_numeric($totalSales) && is_numeric($totalCashSales)) ? $totalSales - $totalCashSales : "Err";


        //Creating the receipt string

        //Sales
        $print_string .= $lines . "::";
        $print_string .= "SALES[c::";
        $print_string .= "Cash Sales[c ".
                            number_format($originalPost['total_cash_sales'],$decimalPlaces,$decimalChar,$thousandsChar)."::";
        $print_string .= "Credit Card Sales[c ".
                            number_format($creditCardSales,$decimalPlaces,$decimalChar,$thousandsChar)."::";
        $print_string .= "Total Sales[c ".'['.
                            number_format($totalSales,$decimalPlaces,$decimalChar,$thousandsChar).']::';


        //Amount Due
        $amountDue     = abs($originalPost['calculated_value_server_turn_in']);
        $print_string .= $lines . "::";
        $print_string .= "AMOUNT DUE[c::";
        $print_string .= $originalPost['server_deposite_take_title'] . "[c [".
                            number_format($amountDue,$decimalPlaces,$decimalChar,$thousandsChar).']::';


        //Till
        $print_string .= $lines . "::";
        $print_string .= "TILL[c::";
        $print_string .= "Till-In[c " .
                            number_format($originalPost['till_in_total'],$decimalPlaces,$decimalChar,$thousandsChar).'::';
        $print_string .= "Till-Out[c ".
                            number_format($originalPost['till_out_total'],$decimalPlaces,$decimalChar,$thousandsChar).'::';
        /*
        $print_string .= "OVER/SHORT[c [".
                            number_format($originalPost['over_short'],$decimalPlaces,$decimalChar,$thousandsChar)."]::";
        */

        //General Tips Info
        $print_string .= $lines . "::";
        $print_string .= "TIPS[c::";
        $print_string .= "Credit Card Tips[c ".
                            number_format($originalPost['cc_tips'],$decimalPlaces,$decimalChar,$thousandsChar)."::";
        $print_string .= "Cash Tips[c ".
                            number_format($originalPost['cash_tips'],$decimalPlaces,$decimalChar,$thousandsChar)."::";
        $print_string .= "Total Tips[c [".
                            number_format($originalPost['total_tips'],$decimalPlaces,$decimalChar,$thousandsChar)."]::";

        //Tipout
        $print_string .= $lines . "::";
        $print_string .= "Tipout Rules[c::" . generateTipoutString($originalPost) . "::";


        //check calc.
        //echo 'fiscal day start: '.$fiscalDayStart.'<br>';
        //echo 'fiscal day end: '  .$fiscalDayEnd.'<br>';

        //Discount Report String
        $print_string .= $lines . "::";
        $print_string .= generateDiscountReceiptString(1,    $data_name, $fiscalDayStart, $fiscalDayEnd);
        $print_string .= $lines . "::";
        $print_string .= generateSupergroupsReceiptString(1, $data_name, $fiscalDayStart, $fiscalDayEnd);

        //End Of Receipt
        $print_string .= ":::::";

        //Switches to webview of receipt.
        if(0){
            $print_string = str_replace(':', "<br>",   $print_string);
            $print_string = str_replace(' ', "&nbsp;", $print_string);
            $print_string = str_replace('[c', ':',     $print_string);
            $print_string = "<br><br><span style='font-family:Courier'> " . $print_string . '</span>';
        }
        else{
            $print_string = str_replace('  ', '. ', $print_string);
        }
        return '1|' . $print_string;
    }


















    //------------------------
    //                        \ DATA BELOW
    function getOriginalServerTipsSubmissionPost($cash_data_row_id){
        global $isLLS;
        if(!empty($cash_data_row_id)){
            if(!empty($_REQUEST['server_submission_post_arr'])){
                return $_REQUEST['server_submission_post_arr'];
            }
        }
        $result = lavu_query("SELECT `value_med` FROM `cash_data` WHERE `id`='[1]'", $cash_data_row_id);
        if(!$result){
            //error_log("MYSQL error in 'tips_wizard_report.php' -asfehsse- error: " . lavu_dberror());
            echo "MYSQL ERROR:" . lavu_dberror();
            return false;
        }
        else if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            $originalPostStr = $row['value_med'];

            $origPostArr;
            if($isLLS){
                $origPostArr = json_decode($originalPostStr, 1);
            }else{
                $origPostArr = json_decode($originalPostStr);
                $origPostArr = $origPostArr[0];
            }

            return $origPostArr;
        }
        else{
            return false;
        }
    }

    function getServersUserRow($server_id){
        $result = lavu_query("SELECT * FROM `users` WHERE `id`='[1]'", $server_id);
        if(!$result){
            error_log("SQL ERROR IN tips_wizard_report.php -4k4fiou3- error:" . lavu_dberror());
            return false;
        }
        else if (mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            return $row;
        }
        else{
            return false;
        }
    }

    function generateTipoutString($originalPost){
        global $decimalPlaces,$decimalChar,$thousandsChar;

        //Output should look like:
        //
        //  1/4  3% to Maitre D & Busser              $10.00
        //       Maitre D: Pretned Betterthannow       $5.00
        //         Busser: Fatty McGee                 $5.00

        //Top level variables
        $serversTotalTips = $originalPost['total_tips'];

        //The dimensions of the ascii monospace table
        $column1Width = 20;
        $column2Width = 20;
        $column3Width = 20;

        //The output string.
        $tipoutDisplayString = '';

        //Tipout Mappings
        $tipoutMappings = $originalPost['tipout_mappings'];
        $tipoutMappings = str_replace('null', '""', $tipoutMappings);
        $tipoutMappings = json_decode($tipoutMappings);
        $tipoutCount = count($tipoutMappings);
        //For each tipout rule
        for($i=0;$i<$tipoutCount;$i++){

            //Layout all needed variables
            $currTipout = $tipoutMappings[$i];
            $ruleTipoutPercentage = $currTipout['percentage'];
            $primary_employee_class_title = $currTipout['primary_emp_class_title'];
            $secondary_employee_class_title = $currTipout['secondary_emp_class_title'];
            $primary_employee_class_bin = $currTipout['primary_emp_class_colleague_bin'];
            $secondary_employee_class_bin = $currTipout['secondary_emp_class_colleague_bin'];
            $primary_employee_class_bin   = is_array($primary_employee_class_bin)   ? $primary_employee_class_bin   : array();
            $secondary_employee_class_bin = is_array($secondary_employee_class_bin) ? $secondary_employee_class_bin : array();
            $totalCardinality = count($primary_employee_class_bin) + count($secondary_employee_class_bin);
            $amountForThisTipoutRule = $serversTotalTips * ($ruleTipoutPercentage / 100);

            $amountForEachTipoutUnderThisRule;
            if($totalCardinality != 0)
                $amountForEachTipoutUnderThisRule = $amountForThisTipoutRule / $totalCardinality;
            else
                $amountForEachTipoutUnderThisRule = 0;

            $amountForThisTipoutRule = $amountForThisTipoutRule;
            $amountForEachTipoutUnderThisRule = $amountForEachTipoutUnderThisRule;
            //<>~-  THE FULL TITLE ROW IN 3 PARTS  -~<>
            //The 1/8 etc. e.g. For tipout rule 1 of 8 total.
            $tipoutDisplayString .= ":" . ($i + 1) . ' of ' . $tipoutCount . "::";

            $tipout_rule_title = '';
            if( !empty($secondary_employee_class_title) ){
                $tipout_rule_title .= $primary_employee_class_title . ' & ' . $secondary_employee_class_title;
            }
            else{
                $tipout_rule_title .= $primary_employee_class_title;
            }
            $tipoutDisplayString .= '-' . $currTipout['percentage'] . '% to ' . $tipout_rule_title . '[c ';
            //The last part, just the total tip amount.
            $tipoutDisplayString .= '['.number_format($amountForThisTipoutRule,$decimalPlaces,$decimalChar,$thousandsChar).']'."::";

            $combinedBins = array_merge($primary_employee_class_bin, $secondary_employee_class_bin);
            foreach($combinedBins as $currTipoutEmployee){
                //Each employee is as follows:
                //      Colleage Object
                //`server_id`,
                //`employee_class`, Strangely is just empty, use title instead.
                //`f_name`,`l_name`,
                //`emp_classes`.
                //`title` as `title`,
                //`emp_classes`.`id` as `emp_class_id`

                //1.) We begin with the employee class, which is the 'title'.
                $empClassTitle = $currTipoutEmployee['title'];
                //2.) First and Last Name
                $fullName = $currTipoutEmployee['f_name'] . ' ' . $currTipoutEmployee['l_name'];
                //3.) The amount this person is getting
                $tipout_amount = number_format($amountForEachTipoutUnderThisRule,$decimalPlaces,$decimalChar,$thousandsChar);
                //Finally, we build the full string row.
                /* If building the monspace table
                $rowStr = alignTextAtSpecifiedWidth($empClassTitle . '[c', $column1Width, true).
                            alignTextAtSpecifiedWidth($fullName, $column2Width, false).
                            $tipout_amount;
                $rowStr .= '::';
                */
                $tipoutDisplayString .= '--' . $fullName . '[c ' . $tipout_amount;

                $tipoutDisplayString .= $rowStr . '::';
            }
        }
        return $tipoutDisplayString;
    }

    function generateDiscountReceiptString($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive){
        global $decimalPlaces, $decimalChar, $thousandsChar;

        $checkLevelDiscounts = getCheckDiscountsReport($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive);
        $checkLevelDiscounts = is_array($checkLevelDiscounts) ? $checkLevelDiscounts : array();
        $itemLevelDiscounts  = getItemDiscountsReport($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive);
        $itemLevelDiscounts  = is_array($itemLevelDiscounts) ? $itemLevelDiscounts : array();

        /*
        echo 'Check Level Discounts:' . nl2br(print_r($checkLevelDiscounts,1));
        echo 'Item Level Discounts:'  . nl2br(print_r($itemLevelDiscounts ,1));
        */

        //Check Level Discounts
        //$discountStr .= createLeftRightSidedTitle("Check Discounts") . "::";
        $itemLevelDiscountArr  = array();
        $checkLevelDiscountArr = array();
        foreach ($checkLevelDiscounts as $currCheckDiscount){
            $discountTypeTitle     = $currCheckDiscount['field4'];
            $discountTypeQuantity  = $currCheckDiscount['field5'];
            $discountTypeSummation = $currCheckDiscount['field6'];
            /* Now combining check-level and item-level per each title.
            $discountStr .= $discountTypeTitle . '[c::';
            $discountStr .= '-Quantity[c' . $discountTypeQuantity . '::';
            $discountStr .= '-Total Amount[c' . $discountTypeSummation . '::';
            $discountStr .= '::';
            */
            $itemLevelDiscountArr[$discountTypeTitle] = array();
            $itemLevelDiscountArr[$discountTypeTitle]['title']     = $discountTypeTitle;
            $itemLevelDiscountArr[$discountTypeTitle]['quantity']  = $discountTypeQuantity;
            $itemLevelDiscountArr[$discountTypeTitle]['summation'] = $discountTypeSummation;
        }

        //Item Level Discounts
        //$discountStr .= createLeftRightSidedTitle("Item Discounts") . "::";
        foreach ($itemLevelDiscounts as $currItemDiscount){
            $discountTypeTitle     = $currItemDiscount['field7'];
            $discountTypeQuantity  = $currItemDiscount['field8'];
            $discountTypeSummation = $currItemDiscount['field9'];
            /* Now combining check-level and item-level per each title.
            $discountStr .= $discountTypeTitle . '[c::';
            $discountStr .= '-Quantity[c' . $discountTypeQuantity . '::';
            $discountStr .= '-Total Amount[c' . $discountTypeSummation . '::';
            $discountStr .= '::';
            */
            $checkLevelDiscountArr[$discountTypeTitle] = array();
            $checkLevelDiscountArr[$discountTypeTitle]['title']     = $discountTypeTitle;
            $checkLevelDiscountArr[$discountTypeTitle]['quantity']  = $discountTypeQuantity;
            $checkLevelDiscountArr[$discountTypeTitle]['summation'] = $discountTypeSummation;
        }

        //Combine arrays
        $combinedDiscountArr = $itemLevelDiscountArr;
        foreach($checkLevelDiscountArr as $key => $val){
            if(is_array($combinedDiscountArr[$key])){
                $combinedDiscountArr[$key]['quantity']  += $checkLevelDiscountArr[$key]['quantity'];
                $combinedDiscountArr[$key]['summation'] += $checkLevelDiscountArr[$key]['summation'];
            }
            else{
                $combinedDiscountArr[$key] = $checkLevelDiscountArr[$key];
            }
        }


        $discountStr = 'DISCOUNTS[c::';
        //Build the string from the combined arrays
        foreach($combinedDiscountArr as $key => $val){
            $discountTypeTitle     = $val['title'];
            $discountTypeQuantity  = $val['quantity'];
            $discountTypeSummation = $val['summation'];
            $discountStr .= $discountTypeTitle . '[c::';
            $discountStr .= '-Quantity[c '      . $discountTypeQuantity  . '::';
            $discountStr .= '-Total Amount[c '  . number_format($discountTypeSummation,$decimalPlaces,$decimalChar,$thousandsChar) . '::';
            $discountStr .= '::';
            $discountStr .= "";
        }


        return $discountStr;
    }

    function generateSupergroupsReceiptString($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive){

        global $decimalPlaces, $decimalChar, $thousandsChar;

        $superGroupReport = getSuperGroupsReport($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive);
        $superGroupReport = is_array($superGroupReport) ? $superGroupReport : array();
        $supergroupReceiptStr = "SUPERGROUPS[c::";
        $count = count($superGroupReport);

        foreach($superGroupReport as $currSupergroupArr){

            $Group         = $currSupergroupArr['field17'];
            $ItemTotal     = $currSupergroupArr['field18'];
            $Modifier;//Don't know what field this is.
            $ItemDiscount  = $currSupergroupArr['field20'];
            $Subtotal      = $currSupergroupArr['field21'];
            $CheckDiscount = $currSupergroupArr['field22'];
            $AfterDiscount = $currSupergroupArr['field23'];
            $Tax           = $currSupergroupArr['field24'];
            $Total         = $currSupergroupArr['field25'];

            //If not assigned
            $Group = empty($Group) ? "Not Assigned" : $Group;

            //Build the receipt string
            $supergroupReceiptStr .= createLeftRightSidedTitle($Group) . "::";
            $supergroupReceiptStr .= "Item Total[c "  . number_format($ItemTotal,$decimalPlaces, $decimalChar, $thousandsChar) . "::";
            $supergroupReceiptStr .= "Item Disc.[c "  . number_format($ItemDiscount, $decimalPlaces, $decimalChar, $thousandsChar) . "::";
            $supergroupReceiptStr .= "Subtotal[c "    . number_format($Subtotal, $decimalPlaces, $decimalChar, $thousandsChar) . "::";
            $supergroupReceiptStr .= "Check Disc.[c " . number_format($CheckDiscount, $decimalPlaces, $decimalChar, $thousandsChar) . "::";
            $supergroupReceiptStr .= "After Disc.[c " . number_format($AfterDiscount, $decimalPlaces, $decimalChar, $thousandsChar) . "::";
            $supergroupReceiptStr .= "Tax[c "    . number_format($Tax, $decimalPlaces, $decimalChar, $thousandsChar)   . "::";
            $supergroupReceiptStr .= "Total[c [" . number_format($Total, $decimalPlaces, $decimalChar, $thousandsChar) ."]::";
            $supergroupReceiptStr .= "::";
        }

        return $supergroupReceiptStr;
    }







    //<>~-    V I E W    H E L P I N G    F U N C T I O N S    -~<>
    function alignTextAtSpecifiedWidth($text, $width, $spacingChar, $rightAlign = false){
        if(strlen($text) > $width){
            return substr($text,0,$width-3) . '...';
        }
        $spaces_count = $width - strlen($text);
        $whitespace = generateRepeatedCharacter($spacingChar, $spaces_count);
        return $rightAlign ? $whitespace . $text : $text . $whitespace;
    }
    function generateRepeatedCharacter($char, $times){
        $str='';for($i=0;$i<$times;$i++){$str.=$char;}return $str;
    }
    function createLeftRightSidedTitle($str){
        $fullSize = 27;
        $numberOfSpaces = $fullSize - strlen($str);
        $leftCount  = floor($numberOfSpaces/2);
        $leftCount  = max(0, $leftCount);
        $rightCount = ceil($numberOfSpaces/2);
        $rightCount = max(0, $rightCount);
        $leftSide  = generateRepeatedCharacter('-', $leftCount);
        $rightSide = generateRepeatedCharacter('-', $rightCount);
        return $leftSide . $str . $rightSide;
    }












    //<>~-~<>~-        R E P O R T S       -~<>~-~<>

    //  I M P O R T A N T    N O T E  - - -  A L L    Q U E R I E S    O B T A I N E D    O R I G I N A L L Y    B Y   C P - R E P O R T S    A N D    U S I N G    G E T    V A R: test44=1

    function getPaymentsReportAsPertainsServer($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive){

       $query = <<<REPORT_ID_24
       select SQL_NO_CACHE IF(`cc_transactions`.`order_id` LIKE "Paid %",0,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`total_collected`)), sum(`cc_transactions`.`total_collected`)))) AS `Sales Collected`,`cc_transactions`.`datetime` AS `datetime`,`cc_transactions`.`loc_id` AS `loc_id`,`orders`.`void` AS `void`,IF(`cc_transactions`.`for_deposit`>0,'deposit','') AS `Deposit`,`orders`.`location_id` AS `location_id`,`orders`.`ag_cash` AS `ag_cash`,`orders`.`ag_card` AS `ag_card`,`orders`.`gratuity` AS `gratuity`,`cc_transactions`.`transtype` AS `transtype`,`cc_transactions`.`order_id` AS `order_id`,`orders`.`order_id` AS `order_id`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`total_collected`)), sum(`cc_transactions`.`total_collected`))) AS `Collected`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`tip_amount`)), sum(`cc_transactions`.`tip_amount`))) AS `Card Tips`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`total_collected` + `cc_transactions`.`tip_amount`)), sum(`cc_transactions`.`total_collected` + `cc_transactions`.`tip_amount`))) AS `Total`,IF(`cc_transactions`.`pay_type`='Card',IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund',(0 - sum(`cc_transactions`.`amount` + `cc_transactions`.`tip_amount`)), sum(`cc_transactions`.`amount` + `cc_transactions`.`tip_amount`))),0) AS `Total Card`,`cc_transactions`.`pay_type` AS `Payment Type`,`cc_transactions`.`card_type` AS `Card Type`,IF(`cc_transactions`.`order_id`='Paid Out','Paid Out',IF(`cc_transactions`.`order_id`='Paid In','Paid In',`cc_transactions`.`action`)) AS `Action`,IF(`cc_transactions`.`voided`='1',sum(`cc_transactions`.`total_collected`),0) AS `Voided` from `[1]`.`cc_transactions` LEFT JOIN `[1]`.`orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`

           where `cc_transactions`.`datetime` >= '[2]' and `cc_transactions`.`datetime` <= '[3]' and `orders`.`void` = 0 and (`orders`.`location_id` = `cc_transactions`.`loc_id` OR `cc_transactions`.`order_id` = "Paid In" OR `cc_transactions`.`order_id` = "Paid Out") and `cc_transactions`.`transtype` != "AddTip" and (`cc_transactions`.`action` = "Sale" OR `cc_transactions`.`action` = "Refund") and `cc_transactions`.`voided` != 1

           AND `cc_transactions`.`server_id`='[4]'

           group by concat(`cc_transactions`.`pay_type`,' ',`cc_transactions`.`card_type`,`cc_transactions`.`action`,`cc_transactions`.`voided`,IF(`cc_transactions`.`order_id` LIKE 'Paid%','PaidInOut',''))
REPORT_ID_24;


        $result = lavu_query($query, 'poslavu_'.$dataName.'_db', $left_boundary_time_inclusive, $right_boundary_time_inclusive, $serverID);
        $resultTable = array();
        if(!$result){
            error_log("mysql error in tips_wizard_report.php -ssgs- error:" . lavu_dberror());
            return false;
        }
        while($currRow = mysqli_fetch_assoc($result)){
            $resultTable[] = $currRow;
        }
        return $resultTable;
    }

    function getSuperGroupsReport($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive){
        $query = <<<REPORT_ID_25
        select SQL_NO_CACHE `orders`.`opened` AS `field0`,`order_contents`.`quantity` AS `field1`,`menu_items`.`super_group_id` AS `field2`,`menu_categories`.`super_group_id` AS `field3`,`orders`.`location_id` AS `field4`,`orders`.`void` AS `field5`,`order_contents`.`loc_id` AS `field6`,`order_contents`.`forced_modifiers_price` AS `field7`,`orders`.`server` AS `field8`,`orders`.`cashier` AS `field9`,`order_contents`.`item_id` AS `field10`,`order_contents`.`void` AS `field11`,`order_contents`.`itax` AS `field12`,`orders`.`server_id` AS `field13`,`orders`.`cashier_id` AS `field14`,`order_contents`.`item` AS `field15`,`menu_categories`.`name` AS `field16`,`super_groups`.`title` AS `field17`,IF (sum(`order_contents`.`price` * `order_contents`.`quantity`) > sum(`order_contents`.`itax`), sum((`order_contents`.`price` * `order_contents`.`quantity`) - `order_contents`.`itax`), sum(`order_contents`.`price` * `order_contents`.`quantity`)) AS `field18`,IF (sum(`order_contents`.`price` * `order_contents`.`quantity`) > sum(`order_contents`.`itax`), sum((`order_contents`.`modify_price` + `order_contents`.`forced_modifiers_price`) * `order_contents`.`quantity`), sum(((`order_contents`.`modify_price` + `order_contents`.`forced_modifiers_price`) * `order_contents`.`quantity`) - `order_contents`.`itax`)) AS `field19`,sum(`order_contents`.`idiscount_amount`) AS `field20`,sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`idiscount_amount` - `order_contents`.`itax`) AS `field21`,sum(`order_contents`.`discount_amount`) AS `field22`,sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` - `order_contents`.`idiscount_amount` - `order_contents`.`itax`) AS `field23`,sum(`order_contents`.`tax_amount` + `order_contents`.`itax`) AS `field24`,sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` + `order_contents`.`tax_amount` - `order_contents`.`idiscount_amount`) AS `field25` from `[1]`.`orders` LEFT JOIN `[1]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` LEFT JOIN `[1]`.`menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `[1]`.`menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `[1]`.`super_groups` ON IF (`menu_items`.`super_group_id` = '', `menu_categories`.`super_group_id`, `menu_items`.`super_group_id`) = `super_groups`.`id`

            where `orders`.`closed` >= '[2]' and `orders`.`closed` <= '[3]' and (`order_contents`.`quantity` * 1) > 0 and
            #`orders`.`location_id` = '1' and
            `orders`.`void` = "0" and `order_contents`.`loc_id` = `orders`.`location_id` and `order_contents`.`item_id` > 0

            AND `orders`.`server_id`='[4]'

            group by `field17`
REPORT_ID_25;

        $resultTable = array();
        $result = lavu_query($query, 'poslavu_' . $dataName . '_db', $left_boundary_time_inclusive, $right_boundary_time_inclusive, $serverID);
        if(!$result){
            error_log("mysql error in tips_wizard_report.php -ahga- error:".lavu_dberror());
            return false;
        }
        while($currRow = mysqli_fetch_assoc($result)){
            $resultTable[] = $currRow;
        }
        return $resultTable;
    }

    function getCheckDiscountsReport($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive){

        /*  Original query done by going to cp-reports and using the test44=1 get variable.
            select SQL_NO_CACHE `orders`.`location_id` AS `field0`,`orders`.`opened` AS `field1`,`orders`.`void` AS `field2`,`split_check_details`.`discount_id` AS `field3`,`discount_types`.`title` AS `field4`,count(`discount_types`.`calc`) AS `field5`,sum(`split_check_details`.`discount`) AS `field6` from `poslavu_demo_brian_d_db`.`orders` LEFT JOIN `poslavu_demo_brian_d_db`.`split_check_details` ON `orders`.`order_id` = `split_check_details`.`order_id` LEFT JOIN `poslavu_demo_brian_d_db`.`discount_types` ON `split_check_details`.`discount_id` = `discount_types`.`id`where `orders`.`location_id` = '1' and `orders`.`closed` >= '2013-09-03 04:30:00' and `orders`.`closed` <= '2013-09-04 04:30:00' and `orders`.`void` != 1 and (`split_check_details`.`discount` + 1 - 1) != 0 group by `field3`
         */
        $query = <<<REPORT_ID_68_CHECK_LEVEL
        select SQL_NO_CACHE `orders`.`location_id` AS `field0`,`orders`.`opened` AS `field1`,`orders`.`void` AS `field2`,`split_check_details`.`discount_id` AS `field3`,`discount_types`.`title` AS `field4`,count(`discount_types`.`calc`) AS `field5`,sum(`split_check_details`.`discount`) AS `field6` from `[1]`.`orders` LEFT JOIN `[1]`.`split_check_details` ON `orders`.`order_id` = `split_check_details`.`order_id` LEFT JOIN `[1]`.`discount_types` ON `split_check_details`.`discount_id` = `discount_types`.`id`
            where `orders`.`server_id`='[2]' AND `orders`.`closed` >= '[3]' and `orders`.`closed` <= '[4]' and `orders`.`void` != 1 and (`split_check_details`.`discount` + 1 - 1) != 0 group by `field3`
REPORT_ID_68_CHECK_LEVEL;
        //Took out: ( `orders`.`location_id`='1' and ) from the WHERE condition.  We filter by server id instead.

        $resultTable = array();
        $result = lavu_query($query, 'poslavu_'.$dataName.'_db', $serverID, $left_boundary_time_inclusive, $right_boundary_time_inclusive);
        if(!$result){
            error_log("mysql error in tips_wizard_report.php -nd87g3- error:".lavu_dberror());
            return false;
        }
        while($currRow = mysqli_fetch_assoc($result)){
            $resultTable[] = $currRow;
        }
        return $resultTable;
    }

    function getItemDiscountsReport($serverID, $dataName, $left_boundary_time_inclusive, $right_boundary_time_inclusive){
        /*
            select SQL_NO_CACHE `orders`.`location_id` AS `field0`,`orders`.`opened` AS `field1`,`orders`.`void` AS `field2`,`order_contents`.`void` AS `field3`,`order_contents`.`quantity` AS `field4`,`order_contents`.`loc_id` AS `field5`,`order_contents`.`idiscount_id` AS `field6`,`discount_types`.`title` AS `field7`,count(`discount_types`.`calc`) AS `field8`,sum(`order_contents`.`idiscount_amount`) AS `field9` from `poslavu_demo_brian_d_db`.`orders` LEFT JOIN `poslavu_demo_brian_d_db`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` LEFT JOIN `poslavu_demo_brian_d_db`.`discount_types` ON `order_contents`.`idiscount_id` = `discount_types`.`id`where `orders`.`location_id` = '1' and `orders`.`closed` >= '2013-09-03 04:30:00' and `orders`.`closed` <= '2013-09-04 04:30:00' and `orders`.`void` != 1 and (`order_contents`.`quantity` + 1 - 1) > 0 and `order_contents`.`loc_id` = `orders`.`location_id` and (`order_contents`.`idiscount_amount` + 1 - 1) != 0 group by `field6`
         */
        $query = <<<REPORT_ID_68_ITEM_LEVEL
            select SQL_NO_CACHE `orders`.`location_id` AS `field0`,`orders`.`opened` AS `field1`,`orders`.`void` AS `field2`,`order_contents`.`void` AS `field3`,`order_contents`.`quantity` AS `field4`,`order_contents`.`loc_id` AS `field5`,`order_contents`.`idiscount_id` AS `field6`,`discount_types`.`title` AS `field7`,count(`discount_types`.`calc`) AS `field8`,sum(`order_contents`.`idiscount_amount`) AS `field9` from `[1]`.`orders` LEFT JOIN `[1]`.`order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` LEFT JOIN `[1]`.`discount_types` ON `order_contents`.`idiscount_id` = `discount_types`.`id`

                where `orders`.`closed` >= '[2]' and `orders`.`closed` <= '[3]' and `orders`.`void` != 1 and (`order_contents`.`quantity` + 1 - 1) > 0 and `order_contents`.`loc_id` = `orders`.`location_id` and (`order_contents`.`idiscount_amount` + 1 - 1) != 0 group by `field6`
REPORT_ID_68_ITEM_LEVEL;
        //Removed:(`orders`.`location_id` = '1' and ) and instead went by the server_id.
        $resultTable = array();
        $result = lavu_query($query, 'poslavu_' . $dataName . '_db', $left_boundary_time_inclusive, $right_boundary_time_inclusive);
        if(!$result){
            error_log("mysql error in tips_wizard_report.php -dfgohafe- error:".lavu_dberror());
            return false;
        }
        while($currRow = mysqli_fetch_assoc($result)){
            $resultTable[] = $currRow;
        }
        return $resultTable;
    }

    function getLocationRow($location_id){
        $result = lavu_query("SELECT * FROM `locations` WHERE `id`='[1]'", $location_id);
        if(!$result){
            return false;
        }
        else{
            return mysqli_fetch_assoc($result);
        }
    }
?>