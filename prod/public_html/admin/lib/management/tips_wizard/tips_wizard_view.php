<?php

    
    //For opening/closing the html.
    function getOpenFullBackgroundDiv($id){
        ob_start();
        ?>
            <style type='text/css'>
                div{
                  -webkit-touch-callout: none;
                  -webkit-user-select: none;
                  -khtml-user-select: none;
                  -moz-user-select: none;
                  -ms-user-select: none;
                  user-select: none;
                }
                body{
                  -webkit-touch-callout: none;
                  -webkit-user-select: none;
                  -khtml-user-select: none;
                  -moz-user-select: none;
                  -ms-user-select: none;
                  user-select: none; 
                }
            </style>
        <?php
        return ob_get_clean();//"<div id='$id' style='width:100%;height:100%;background-color:rgba(255,0,0,0.1);display:relative;'>";
    }
    function getCloseFullBackgroundDiv(){
        return '';//'</div>';
    }
    //---------------------------------------------

    
    //This is the furthest back (z-buffer) div that contains all other divs.
    function getContainingDiv($divs_id){
        ob_start();
        ?>
            <style type='text/css'>
            .mainBackground{
                /*
                width:750px;
                height:400px;
                
                border-radius:10px;
                */
                /*background-color:#CCC;*/
                display:inline-block;
                 /*
                float:left;
                */
            }
            div{
              -webkit-touch-callout: none;
              -webkit-user-select: none;
              -khtml-user-select: none;
              -moz-user-select: none;
              -ms-user-select: none;
              user-select: none;
            }
            </style>
            <div class='mainBackground' id='<?php echo $divs_id;?>'>
            </div>
        <?php
        $outputHTML = ob_get_clean();

        return $outputHTML;
    }
    // End containing div.
    //-------------------------------------------------------
    
    
    function addPagingComponentToContainerDiv($containingDivs_ID, $receiptData, 
                    $decimalChar, $monitary_symbol, $decimal_place_count, $tillOutRow,
                    $js_denominations, $totalSales, $totalCashSales, $totalTillIn, 
                    $server_clock_punch_id, $tillID, $tipoutMappingsJSON, 
                    $askForTotalOrJustCashTips,$totalTipouts, $serverID, $locationID,
                    $datetime_now, $serverDateTime_now, $fiscalDayStart,  
                    $data_name, $sec_hash_for_printing){
        ob_start();
        if($tillOutRow === false){$tillOutRow = 'false';}
        if($tillOutRow === true){$tillOutRow  = 'true';}
        ?>
        
    
          <style type='text/css'>
            .mainPagingContainer{
                position:relative;
                border:1px solid #666;
                top:20px;
                /*width:380px;*/
                width:723px;
                height:391px;
                background-color:#FFF;
                display:inline-block;
                vertical-align:top;
                border-radius:15px;
            }



            /*  T I P    L A B E L    &    I N P U T  */
            .tipInputLabel{
                position:absolute;
                left:25px;
                /*width:120px;*/
                height:30px;
                top:230px;
                font-size:16px;
                font-family:verdana;
                margin-top:4px;
                /*background-color:red;*/
                text-align:right;
                color:#606a8a;
            }
            .tipInputDivStyle{
                position:absolute;
                display:block;
                left:188px;
                top:210px;
                width:184px;
                text-align:left;
                height:65px;
                font-family:verdana;
                font-size:50px;
                color:#5f698a;
                /*background-color:#aaa;*/
                overflow-x:scroll;
                overflow-y:hidden;
            }
            


          /*   N A V I G A T I O N    B U T T O N S   */
          .goBackBtn{
              position:absolute;
              border:1px solid #666;
              border-radius:5px;
              transition: background-color ease-out 0.25s;
              -webkit-transition: background-color ease-out 0.25s;
              -webkit-tap-highlight-color:rgba(0,0,0,0.0);
              background-color:#BBBBBB;
              width:140px;
              height:32px;
              left:25px;
              top:310px;
              color:#FFF;
              text-align:left;
              font-weight:bold;
              font-size:22px;
           }
          .goBackBtnPressed{
              position:absolute;
              border:1px solid #666;
              border-radius:5px;
              -webkit-tap-highlight-color:rgba(0,0,0,0.0);
              background-color:rgba(50,50,50,1.0);
              width:140px;
              height:32px;
              left:25px;
              top:310px;
              color:#777;
              text-align:left;
              font-weight:bold;
              font-size:22px;
          }
          .goForwardBtn{
              position:absolute;
              border:1px solid #666;
              border-radius:5px;
              transition: background-color ease-out 0.25s;
              -webkit-transition: background-color ease-out 0.25s;
              -webkit-tap-highlight-color:rgba(0,0,0,0.0);
              background-color:#AECD37;
              width:140px;
              height:32px;
              left:232px;
              top:310px;
              color:#FFF;
              text-align:right;
              font-weight:bold;
              font-size:22px;
          }
          .goForwardBtnPressed{
              position:absolute;
              border:1px solid #666;
              border-radius:5px;
              background-color:rgba(50,50,50,1.0);
              -webkit-tap-highlight-color:rgba(0,0,0,0.0);
              width:140px;
              height:32px;
              left:232px;
              top:310px;
              color:#777;
              text-align:right;
              font-weight:bold;
              font-size:22px;
          }
          .inactiveButtonStyle{
              position:absolute;
              border:1px solid #666;
              border-radius:5px;
              transition: background-color ease-out 0.25s;
              -webkit-transition: background-color ease-out 0.25s;
              -webkit-tap-highlight-color:rgba(0,0,0,0.0);
              background-color:rgba(80,80,80,0.15);
              
              width:140px;
              height:32px;
              left:232px;
              top:310px;
              color:#FFF;
              text-align:right;
              font-weight:bold;
              font-size:22px;
          }
          
          
          
          /* S C R O L L     U P / D O W N    B U T T O N S */
          .scrollButton{
            position:absolute;
            border:1px solid #aaa;
            border-radius:0px;
            transition: background-color ease-out 0.25s;
            -webkit-transition: background-color ease-out 0.25s;
            -webkit-tap-highlight-color:rgba(0,0,0,0.0);
            background-color:rgb(223,239,223);
            /*color:rgb(170,175,170);*/
            
            color:rgb(113,117,113);
            /*background-color:#ebedf1;*/
            /*color:#b3b8c8;*/
          }
          .scrollButtonPressed{
            position:absolute;
            border:1px solid #aaa;
            border-radius:0px;
            -webkit-tap-highlight-color:rgba(0,0,0,0.0);
            background-color:rgb(147,160,147);
            color:rgb(113,117,113);
          }
          
          
          /* S C R O L L I N G     B E H A V I O U R   */
          ::-webkit-scrollbar {
              -webkit-appearance: none;
              width: 5px;
              height: 5px;
          }
          ::-webkit-scrollbar-thumb {
              border-radius: 4px;
              background-color: rgba(174,206,43,1.0);
              -webkit-box-shadow: 0 0 1px rgba(0,0,0,0.5);
          }
          ::-webkit-scrollbar-track-piece{
              /*background-color: #aaa;*/
          }
          
          
          
          /*    O T H E R    C R A P    B E L O W   */
          /* The components for the summary pages */
          /* Totals Report */
          .totalCashSalesLabelDiv{
              position:absolute;
              left:60px;
              width:120px;
              height:30px;
              top:50px;
              font-size:16px;
              margin-top:4px;
              background-color:red;
              text-align:right;
          }
          .yourCCTipsLabelDiv{
              position:absolute;
              left:60px;
              width:120px;
              height:30px;
              top:100px;
              font-size:16px;
              margin-top:4px;
              background-color:green;
              text-align:right;
          }
          
          /* Cash Reconciliation */
          .cashReconInputsContainerDiv{
              position:absolute;
              left:25px;
              top:25px;
              overflow-y:scroll;
              overflow-x:visible;
          }

          /* Totals Page */
          .totalsPageDiv{
              position:absolute;
              left:0px;
              top:0px;
          }
          .tipOutDiv{
              position:absolute;
              left:25px;
              top:25px;
          }
          /* Finished Page */
          .finishedPageDiv{
              position:absolute;
              left:25px;
              top:25px;
          }
          
          .receiptInfoLabel{
              color:#b7bac4;
              font-family:verdana;
              font-weight: bold;
              font-size:14px;
          }
          .receiptInfoValue{
              color:#5c6173;
              font-family:verdana;
              font-weight: bold;
              padding-left: 5px;
              font-size:14px;
          }
        </style>
            
        <script type='text/javascript'>
            
        var currPage = 0;
        var receiptData;
        var serverID = <?php echo $serverID; ?>;
        var decimalPlaceCount = <?php echo $decimal_place_count;?>;
        var tillOutRow = <?php echo $tillOutRow;?>;
        var __js_denominations = <?php echo $js_denominations;?>;
        var totalSales = <?php echo $totalSales;?>;
        var totalCashSales = <?php echo $totalCashSales;?>;
        var totalTillIn = <?php echo $totalTillIn;?>;
        var serverClockPunchID = <?php echo $server_clock_punch_id;?>;
        var tillInID = <?php echo $tillID;?>;
        var tipoutMappings = <?php echo $tipoutMappingsJSON;?>;
        var totalTipouts = <?php echo $totalTipouts;?>;
        var monitarySymbol = '<?php echo $monitary_symbol;?>';
        var askForTotalOrJustCashTips = '<?php echo $askForTotalOrJustCashTips;?>';
        var locationID = <?php echo $locationID;?>;
        var fiscalDayStart = '<?php echo $fiscalDayStart;?>';
        var decimalChar = '<?php echo $decimalChar;?>';
        function initPagingComponentWithReceiptData(){
            receiptData = <?php echo $receiptData;?>;
            var pagingComponent = document.createElement("div");
            pagingComponent.id = 'paging_component_div_id';
            pagingComponent.className = 'mainPagingContainer';
            pagingComponent.innerHTML = "&nbsp;";
            var parentContainer = document.getElementById('<?php echo $containingDivs_ID;?>');
            pagingComponent.style.marginTop = '-25px';
            parentContainer.appendChild(pagingComponent);
        }
        
        function setupInnerComponents(monitary_symbol){
            var pagingComponent = document.getElementById('paging_component_div_id');
            
            if(!monitary_symbol){
                monitary_symbol = '$';
            }
            
            var receiptDataTable = document.createElement('table');
            receiptDataTable.id = 'receipt_data_table_id';
            receiptDataTable.cellSpacing = '2';
            receiptDataTable.border = '0';
            receiptDataTable.style.position = 'absolute';
            receiptDataTable.style.left = '30px';
            receiptDataTable.style.top = '25px';
            
            //Receipt x_of_total
            var x_of_total_row = document.createElement('tr');
            var x_of_total_label = document.createElement('td');
            x_of_total_label.style.textAlign = 'right';
            x_of_total_label.id = 'x_of_total_label_td_id';
            x_of_total_label.className = 'receiptInfoLabel';
            var x_of_total_value = document.createElement('td');
            x_of_total_value.id = 'x_of_total_value_td_id';
            x_of_total_value.className = 'receiptInfoValue';
            x_of_total_row.appendChild(x_of_total_label);
            x_of_total_row.appendChild(x_of_total_value);
            receiptDataTable.appendChild(x_of_total_row);
            
            //Table Name
            var table_name_row = document.createElement('tr');
            var table_name_label = document.createElement('td');
            table_name_label.style.textAlign = 'right';
            table_name_label.className = 'receiptInfoLabel';
            table_name_label.innerHTML = "Table Name:";
            var table_name_value = document.createElement('td');
            table_name_value.className = 'receiptInfoValue';
            table_name_value.id = 'table_name_value_td_id';
            table_name_row.appendChild(table_name_label);
            table_name_row.appendChild(table_name_value);
            receiptDataTable.appendChild(table_name_row);
            
            //Order ID.
            var order_id_row = document.createElement('tr');
            var order_id_label = document.createElement('td');
            order_id_label.style.textAlign = 'right';
            order_id_label.id = 'order_id_label_td_id';
            order_id_label.className = 'receiptInfoLabel';
            var order_id_value = document.createElement('td');
            order_id_value.id = 'order_id_value_td_id';
            order_id_value.className = 'receiptInfoValue';
            order_id_row.appendChild(order_id_label);
            order_id_row.appendChild(order_id_value);
            receiptDataTable.appendChild(order_id_row);
            
            //Card Info.
            var card_info_row = document.createElement('tr');
            var card_info_label = document.createElement('td');
            card_info_label.style.textAlign = 'right';
            card_info_label.id = 'card_info_label_td_id';
            card_info_label.className = 'receiptInfoLabel';
            var card_info_value = document.createElement('td');
            card_info_value.id = 'card_info_value_td_id';
            card_info_value.className = 'receiptInfoValue';
            card_info_row.appendChild(card_info_label);
            card_info_row.appendChild(card_info_value);
            receiptDataTable.appendChild(card_info_row);
            
            //Transaction Id Row.
            var transaction_id_row = document.createElement('tr');
            var transaction_id_label = document.createElement('td');
            transaction_id_label.style.textAlign = 'right';
            transaction_id_label.id = 'transaction_id_label_td_id';
            transaction_id_label.className = 'receiptInfoLabel';
            var transaction_id_value = document.createElement('td');
            transaction_id_value.id = 'transaction_id_value_td_id';
            transaction_id_value.className = 'receiptInfoValue';
            transaction_id_row.appendChild(transaction_id_label);
            transaction_id_row.appendChild(transaction_id_value);
            receiptDataTable.appendChild(transaction_id_row);
            
            //Date/Time Row.
            var date_time_row = document.createElement('tr');
            var date_time_label = document.createElement('td');
            date_time_label.style.textAlign = 'right';
            date_time_label.id = 'date_time_label_td_id';
            date_time_label.className = 'receiptInfoLabel'
            var date_time_value = document.createElement('td');
            date_time_value.id = 'date_time_value_td_id';
            date_time_value.className = 'receiptInfoValue';
            date_time_row.appendChild(date_time_label);
            date_time_row.appendChild(date_time_value);
            receiptDataTable.appendChild(date_time_row);
            
            //Total Row
            var total_row = document.createElement('tr');
            var total_row_label = document.createElement('td');
            total_row_label.style.textAlign = 'right';
            total_row_label.id = 'total_row_label_td_id';
            total_row_label.className = 'receiptInfoLabel';
            var total_row_value = document.createElement('td');
            total_row_value.id = 'total_row_value_td_id';
            total_row_value.className = 'receiptInfoValue';
            total_row.appendChild(total_row_label);
            total_row.appendChild(total_row_value);
            receiptDataTable.appendChild(total_row);

            pagingComponent.appendChild(receiptDataTable);
            //                //Receipt Info Display Table
            //               /
            //---------------
            //               \
            //                \\Other Main Nav. Components.
            //Tip Input Label
            var tip_input_label = document.createElement('div');
            tip_input_label.className = 'tipInputLabel';
            tip_input_label.innerHTML = "TIP AMOUNT: ";
            tip_input_label.id = 'tip_input_label_id';
            pagingComponent.appendChild(tip_input_label);
            //Tip Input Monitary Symbol Div
            var monitarySymbolDiv = document.createElement('div');
            monitarySymbolDiv.id = 'monitary_symbol_div_id';
            monitarySymbolDiv.style.position = 'absolute';
            monitarySymbolDiv.style.top = '208px';
            monitarySymbolDiv.style.left = '152px';
            monitarySymbolDiv.style.width = '20px';
            monitarySymbolDiv.style.height = '100%';
            monitarySymbolDiv.style.fontFamily = 'verdana';
            monitarySymbolDiv.style.fontSize = '50px';
            monitarySymbolDiv.style.color = '#5f698a';
            monitarySymbolDiv.innerHTML = monitarySymbol;
            pagingComponent.appendChild(monitarySymbolDiv);
            //Tip Input top button accent box
            var accentBoxDiv = document.createElement('div');
            accentBoxDiv.id = 'accent_box_div_id';
            accentBoxDiv.style.position = 'absolute';
            accentBoxDiv.style.left   = '25px';
            accentBoxDiv.style.top    = '202px';
            accentBoxDiv.style.height = '80px';
            accentBoxDiv.style.width  = '347px';
            accentBoxDiv.style.borderTop = '1px solid black';
            accentBoxDiv.style.borderBottom = '1px solid black';
            pagingComponent.appendChild(accentBoxDiv);
            //Tip Input Div
            var tip_input_div = document.createElement('div');
            tip_input_div.id = 'tip_input_div_id';
            tip_input_div.type = 'text';
            tip_input_div.className = 'tipInputDivStyle';
            tip_input_div.readOnly = true;
            pagingComponent.appendChild(tip_input_div);


            //--------------------
            //                    \
            //                 Buttons
            //
            //Go Left Button
            var goLeftBtnPushed = false;
            var goLeftBtn = document.createElement('div');
            goLeftBtn.id = 'go_left_div_id';
            goLeftBtn.className = 'goBackBtn';
            goLeftBtn.innerHTML = "&nbsp;<";
            goLeftBtn.addEventListener('touchstart', function() {
                if(goLeftBtnPushed){return;}
                if(this.isDisabled){
                    // .isDisabled is set to true in the tips_wizard_finished_page.php when the submit
                    //    process returns 'success' from the server.
                    return;
                }
                goLeftBtnPushed = true;
                goLeftBtn.className = 'goBackBtnPressed';
                goToPreviousReceiptPressed(true);
            });
            goLeftBtn.addEventListener('touchend', function() {
                goLeftBtn.className = 'goBackBtn';
                goLeftBtnPushed = false;
            });
            pagingComponent.appendChild(goLeftBtn);
            //
            //Go Right Button
            var goRightBtnPushed = false;
            var goRightBtn = document.createElement('div');
            goRightBtn.id = 'go_right_div_id';
            goRightBtn.className = 'goForwardBtn';
            goRightBtn.innerHTML = '>&nbsp;';
            goRightBtn.addEventListener('touchstart', function() {
                if(goRightBtnPushed){return;}
                goRightBtnPushed = true;
                if(goRightBtn.className == 'goForwardBtn')
                    goRightBtn.className = 'goForwardBtnPressed';
                goToNextReceiptPressed(true);
            });
            goRightBtn.addEventListener('touchend', function() {
                if(goRightBtn.className == 'goForwardBtnPressed')
                    goRightBtn.className = 'goForwardBtn';
                goRightBtnPushed = false;
            });
            pagingComponent.appendChild(goRightBtn);
        try{
            setupInnerSummaryComponents();
        }catch(e){alert(e.message)};

        }
        function setupInnerSummaryComponents(){
            var pagingComponent = document.getElementById('paging_component_div_id');
            var total_sales_label_div = document.createElement('div');
            total_sales_label_div.id = 'total_sales_label_div_id';
            total_sales_label_div.className = 'totalCashSalesLabelDiv';
            total_sales_label_div.style.display = 'none';
            pagingComponent.appendChild(total_sales_label_div);
            var your_cc_tips_label_div = document.createElement('div');
            your_cc_tips_label_div.id = 'your_cc_tips_label_div_id';
            your_cc_tips_label_div.className = 'yourCCTipsLabelDiv';
            your_cc_tips_label_div.style.display = 'none';
            pagingComponent.appendChild(your_cc_tips_label_div);
            setupCashReconciliationPage(pagingComponent);
            setupTotalsPage(pagingComponent);
            setupTipOutPage(pagingComponent);
            setupFinishedPage(pagingComponent);
        }
        function setupCashReconciliationPage(pagingDiv){

            if(tillOutRow === false){
                return; //If tillOutRow is false, then we do not do the cash recon page, and we don't
                        //have any till in data which would break the following code.
            }
      //alert('till out row:' + JSON.stringify(tillOutRow));
            var denominations = tillOutRow.value6;
      //alert('denominations: ' + denominations);
            var parts = denominations.split(':');
            var mainUnitDenominations  = (parts[0]).split('/');//e.g. dollar types (5 or 10 bills)
            var subUnitDenominations   = (parts[1]).split('/');//e.g. cents (penny, quarter, etc.)
            var cash_recon_component_div = document.createElement('div');
            cash_recon_component_div.id = 'cash_recon_component_div_id';
            cash_recon_component_div.className = 'cashReconInputsContainerDiv';
            cash_recon_component_div.style.display = 'none';
            //Now build the cash recon inner components
            var totalRowsForDenominations = Math.max(mainUnitDenominations.length, subUnitDenominations.length);
            var serverReconBuilder = {};
            serverReconBuilder.width = 350;
            serverReconBuilder.height = 250;
            serverReconBuilder.main_denominations = __js_denominations['bills'];
            serverReconBuilder.sub_denominations  = __js_denominations['coins'];

            serverReconBuilder.row_height = 20;
            serverReconBuilder.desired_row_space = 10;
            serverReconBuilder.min_row_space = 5;
            serverReconBuilder.label_font_size = 16;
            serverReconBuilder.returnOnLastInputFunction = goToNextReceiptPressed;
            generateServerReconDisplayComponent(cash_recon_component_div, serverReconBuilder)
            pagingDiv.appendChild(cash_recon_component_div);
        }
        
        
        function setupTotalsPage(pagingDiv){
            
            var totalsPageDiv = document.createElement('div');
            totalsPageDiv.style.display='none';
            totalsPageDiv.className = 'totalsPageDiv';
            totalsPageDiv.id = 'totals_page_div_id';
            
            var totalsPageBuilder = {};
            totalsPageBuilder.width = 400;
            totalsPageBuilder.height = 300;
            totalsPageBuilder.receiptData = receiptData;
            totalsPageBuilder.totalSales  = totalSales;
            totalsPageBuilder.totalCashSales = totalCashSales;
            totalsPageBuilder.monitarySymbol = monitarySymbol;
            totalsPageBuilder.decimalCount   = decimalPlaceCount;
            totalsPageBuilder.askForTotalOrJustCashTips = askForTotalOrJustCashTips;
            totalsPageBuilder.isUsingServerCashRecon = isUsingServerCashRecon;
            totalsPageBuilder.totalTillIn = totalTillIn;
            generateTotalsPage(totalsPageDiv, totalsPageBuilder);
            pagingDiv.appendChild(totalsPageDiv);
        }
        
        function setupTipOutPage(pagingDiv){
        
            var tipOutPageDiv = document.createElement('div');
            tipOutPageDiv.style.display='none';
            tipOutPageDiv.className = 'tipOutDiv';
            tipOutPageDiv.id = 'tip_out_page_div_id';
            var builder={};
            builder.width = 350;
            builder.height = 250;
            builder.tipoutMappings = tipoutMappings;
            builder.decimalPlaceCount = decimalPlaceCount;
            generateTipOutDisplayComponent(tipOutPageDiv, builder);
            pagingDiv.appendChild(tipOutPageDiv);
        }
        
        function setupFinishedPage(pagingDiv){
            var finishedPageDiv = document.createElement('div');
            finishedPageDiv.style.display='none';
            finishedPageDiv.className = 'finishedPageDiv';
            finishedPageDiv.id = 'finished_page_div_id';
            
            var builder={};
            builder.width = 350;
            builder.height = 250;
            builder.serverClockPunchID = serverClockPunchID;
            builder.tillInID = tillInID;
            builder.serverID = serverID;
            builder.locationID = locationID;
            builder.totalTillIn = totalTillIn;
            builder.fiscalDayStart = fiscalDayStart;
            builder.decimalChar = decimalChar;
            builder.dataName = '<?php echo $data_name;?>';
            builder.secHash  = '<?php echo $sec_hash_for_printing;?>';
            builder.serversID = serverID;
            generateFinishedPage(finishedPageDiv, builder);
            pagingDiv.appendChild(finishedPageDiv);
        }



//             / - Setup functions
//           /\
//------------------------------------------------------------------------------------------------
//           |/
//            \  - Summary area navigation
        
        
        
        
        var isInSummary = false;
        function toggleToOrFromSummaryArea(toSummary){
            if(toSummary && isInSummary){ return; }
            if(!toSummary && !isInSummary){ return; }
            isInSummary = toSummary;
            var displayType = toSummary ? 'none' : 'block';
            var receiptInfoTable = document.getElementById('receipt_data_table_id');
            receiptInfoTable.style.display = displayType;
        
            //Tip Input
            var tip_input_label = document.getElementById('tip_input_label_id');
            tip_input_label.style.display = displayType;
            var tip_input_div = document.getElementById('tip_input_div_id');
            if(!toSummary){ tip_input_div.className = 'tipInputDivStyle';}
            
            //Monitary Symbol.
            var monitarySymbolDiv = document.getElementById('monitary_symbol_div_id');
            monitarySymbolDiv.style.display = displayType;
            var accentDiv = document.getElementById('accent_box_div_id');
            accentDiv.style.display = displayType;
        }

        
        function getTipAmountEntry(){
            var tip_input_div = document.getElementById('tip_input_div_id');
            returnVal = tip_input_div.innerHTML;
            returnVal = returnVal.replace(/&nbsp;/g, '');
            returnVal = returnVal.replace( monitarySymbol, '');
            return returnVal;
        }
        function setTipAmountEntry(val){
            var tip_input_div = document.getElementById('tip_input_div_id');
            tip_input_div.innerHTML = val;
            tip_input_div.scrollLeft = 100000000;
        }
        
        
        //SUMMARY PAGE FUNCTIONS, SERVER RECONCILIATION, AND TOTAL.
        var summaryPages = [];
        var hasTipouts = !(tipoutMappings.length == 0 || totalTipouts == 0);
        if(tillOutRow){ summaryPages.push(loadOrLeaveCashReconPage); }
        summaryPages.push(loadOrLeaveTotalSalesAndTipsPage);
        if(hasTipouts){ summaryPages.push(loadOrLeaveTipOutPage); }
        summaryPages.push(loadOrLeaveFinishedPage);
        

        function load_summary_page(summaryPageIndex){
            currPage = summaryPageIndex + receiptData.length;
            toggleToOrFromSummaryArea(true);
            summaryPages[summaryPageIndex](true);
        }
        
        function leave_summary_page(summaryPageIndexToLeave){
            //We detect if the scroll up or scroll down was used and remove the buttons if so.
            var scrollUpButton = document.getElementById('scroll_up_button_div_id');
            var scrollDownButton = document.getElementById('scroll_down_button_div_id');
            if(scrollUpButton){
                scrollUpButton.parentNode.removeChild(scrollUpButton);
            }
            if(scrollDownButton){
                scrollDownButton.parentNode.removeChild(scrollDownButton);
            }
            summaryPages[summaryPageIndexToLeave](false);
        }
        
        
        
        //Server Reconciliation
        var isCashReconPage = false;
        var isUsingServerCashRecon = tillOutRow ? true : false;
        var lastCashReconData;//Set whenever leaving the server_recon page.
        function loadOrLeaveCashReconPage(isLoading){
            isCashReconPage = isLoading ? true : false;
            var displayType = isLoading ? 'block' : 'none';
            var cash_recon_component_div = document.getElementById('cash_recon_component_div_id');
            cash_recon_component_div.style.display = displayType;
            if(!isLoading){
                lastCashReconData = cash_recon_component_div.getReconciliationArraysAndTotal();
            } 
        }

        //Totals Page
        isTotalsPage = false;
        lastEnteredTipsTotalCashAndCC = 0;
        lastEnteredTipsCashAlone = 0;
        lastEnteredTipsCCsOnly = 0;
        lastOverShortAmount = 0;
        function loadOrLeaveTotalSalesAndTipsPage(isLoading){

            isTotalsPage = isLoading ? true : false;
            var displayVal = isLoading ? 'block' : 'none';
            
            var tip_input_div = document.getElementById('tip_input_div_id');
            tip_input_div.style.display = isLoading ? 'none' : 'block';

            var totals_page_component = document.getElementById('totals_page_div_id');
            totals_page_component.style.display = displayVal;
            
            //tillInTotalOrNull
            if(isLoading){
                //Entering page.
                var cashReconArg = false;
                if(lastCashReconData && isUsingServerCashRecon){
                    cashReconArg = lastCashReconData;
                }
                totals_page_component.cash_recon_arg = cashReconArg;
                totals_page_component.totalTillIn = totalTillIn < 0 ? 0 : totalTillIn;
                totals_page_component.decimalPlaceCount = decimalPlaceCount;
                totals_page_component.display(cashReconArg, totalTillIn, decimalPlaceCount);
            }
            else{
                //Leaving page.
                totals_page_component.finalSanitizeInputAmount();
                lastEnteredTipsTotalCashAndCC = totals_page_component.getTotalTipsCashAndCC();
                lastEnteredTipsCCsOnly   = totals_page_component.getTotalCCTipAmount();
                lastEnteredTipsCashAlone = totals_page_component.getCashTips();
                lastOverShortAmount = parseFloat(totals_page_component.overShortAmount).toFixed(decimalPlaceCount);
                lastOverShortAmount = lastOverShortAmount == "NaN" ? 0 : lastOverShortAmount;
                
                //We do the toFixed to keep the input clean
                var inputValue = totals_page_component.getTipsInputValue();
                inputValue = inputValue.replace(/&nbsp;/g, '');
                
                inputValue = parseFloat(inputValue);
                if(inputValue == NaN){ inputValue = 0; }
                inputValue = inputValue.toFixed(decimalPlaceCount);
                totals_page_component.setTipsInputValue(inputValue);
            }
        }
        
        function loadOrLeaveTipOutPage(isLoading){
            var tip_out_page_component = document.getElementById('tip_out_page_div_id');
            tip_out_page_component.style.display = isLoading ? 'block' : 'none';
           
            var tip_input_div = document.getElementById('tip_input_div_id');
            tip_input_div.style.display = isLoading ? 'none' : 'block';
           
            if(isLoading){
                tip_out_page_component.display(lastEnteredTipsTotalCashAndCC);
            }
        }
        
        
        function loadOrLeaveFinishedPage(isLoading){
            var msg = "Finished Page: ";
            msg += isLoading ? "loading" : "leaving"
            
            displayVal = isLoading ? 'block' : 'none';
            
            var tip_input_div = document.getElementById('tip_input_div_id');
            tip_input_div.style.display = isLoading ? 'none' : 'block';
            
            var finished_page_component = document.getElementById('finished_page_div_id');
            finished_page_component.style.display = displayVal;
            
            var forwardNavigationButton = document.getElementById('go_right_div_id');
            if(isLoading){
                var cashReconArg = false;
                if(lastCashReconData && isUsingServerCashRecon){
                    cashReconArg = lastCashReconData;
                }
                //forwardNavigationButton.style.backgroundColor='#BBBBBB';
                forwardNavigationButton.className = 'inactiveButtonStyle';
                finished_page_component.tipoutMappings = tipoutMappings;
                finished_page_component.lastEnteredTipsTotalCashAndCC = lastEnteredTipsTotalCashAndCC;
                finished_page_component.lastEnteredTipsCCsOnly = lastEnteredTipsCCsOnly;
                finished_page_component.lastEnteredTipsCashAlone = lastEnteredTipsCashAlone;
                finished_page_component.lastOverShortAmount = lastOverShortAmount;
                finished_page_component.totalCashSales = totalCashSales;
                finished_page_component.totalSales = totalSales;
                finished_page_component.dateTimeNow = '<?php echo $datetime_now;?>';
                finished_page_component.serverDateTimeNow = '<?php echo $serverDateTime_now;?>';
                finished_page_component.display(cashReconArg, decimalPlaceCount, receiptData);
            }else{
                //forwardNavigationButton.style.backgroundColor='#AECD37';
                forwardNavigationButton.className = 'goForwardBtn';
            }
            
        }

//        - Summary Area 
//      /
//-----------------------------------------------------------------------
//      \
//       - General Navigation
        
        function load_page(page_index){
            
            if(page_index < 0){
                return;
            }
            if(page_index == receiptData.length + summaryPages.length){
                return;
            }
            if(currPage >= receiptData.length && currPage != page_index){
                leave_summary_page(currPage - receiptData.length);
            }
            if(page_index >= receiptData.length){
                load_summary_page(page_index - receiptData.length);
                return;
            }
            
            toggleToOrFromSummaryArea(false);
            currPage = page_index;
            

            var currReciept = receiptData[page_index];
            var tableNameValue = document.getElementById('table_name_value_td_id');
            var xOfTotalLabel = document.getElementById('x_of_total_label_td_id');
            var xOfTotalValue = document.getElementById('x_of_total_value_td_id');
            var orderIDLabel  = document.getElementById('order_id_label_td_id');
            var orderIDValue  = document.getElementById('order_id_value_td_id');
            var cardInfoLabel = document.getElementById('card_info_label_td_id');
            var cardInfoValue = document.getElementById('card_info_value_td_id');
            var transactionIDLabel = document.getElementById('transaction_id_label_td_id');
            var transactionIDValue = document.getElementById('transaction_id_value_td_id');
            var dateTimeLabel = document.getElementById('date_time_label_td_id');
            var dateTimeValue = document.getElementById('date_time_value_td_id');
            var totalRowLabel = document.getElementById('total_row_label_td_id');
            var totalRowValue = document.getElementById('total_row_value_td_id');
            
            xOfTotalLabel.innerHTML = "Receipt:";
            xOfTotalValue.innerHTML = (currPage + 1) + " of " + receiptData.length;
            tableNameValue.innerHTML = currReciept['table_name'];
            orderIDLabel.innerHTML  = "Order ID:";
            orderIDValue.innerHTML  = currReciept['order_id'];
            cardInfoLabel.innerHTML = "Card Info:";
            cardInfoValue.innerHTML = currReciept['card_type'] + ' ' + currReciept['card_desc'];
            transactionIDLabel.innerHTML = "Trans:";
            transactionIDValue.innerHTML = currReciept['ref_number'];
            dateTimeLabel.innerHTML = "Date Time:";
            dateTimeValue.innerHTML = currReciept['date_time'];
            totalRowLabel.innerHTML = "Total:";
            totalRowValue.innerHTML = monitarySymbol + currReciept['amount'];

            var tipAmountInput = document.getElementById('tip_input_div_id');
            tipAmountInput.innerHTML = currReciept['tip_amount'];
            
        }
        
        function sendPostToUpdateCurrentReceiptAndSaveInLocalReceiptData(){
            finalSanitizeInput();
            currReceiptInfoArr = receiptData[currPage];
            tipAmount = getTipAmountEntry();
            tipAmount = tipAmount.replace(/,/g, '.');
            //We actually are not updating the tips on forward push now.
            //---
            ///postTipAmount(currReceiptInfoArr.transaction_id, currReceiptInfoArr.server_id, tipAmount, currPage, handleReturnedAjaxResponse);
            //---
            receiptData[currPage].tip_amount = tipAmount;
            
        }
        
        function handleReturnedAjaxResponse(serverResponse){
            serverResponse = JSON.parse(serverResponse);
            if(serverResponse.status && serverResponse.status == 'success'){
                updatedTipAmount = serverResponse.tip_amount;
                updatedIndex = serverResponse.updated_index;
                receiptData[updatedIndex].tip_amount = updatedTipAmount;
            }
        }
        
        function finalSanitizeInput(){
            var tipInput = document.getElementById('tip_input_div_id');
            tipInputValue = getTipAmountEntry();
            if( tipInputValue == '' ){
                var val_ = 0 + '<?php echo $decimalChar;?>' + '00';
                setTipAmountEntry(val_);
                return;
            }
            else if( tipInputValue.match(/^[0-9]*$/) ){
                var val_ = tipInputValue + '<?php echo $decimalChar;?>' + '00';
                setTipAmountEntry(val_);
                return;
            }
            else if( tipInputValue.match(/^[0-9]*\.[0-9]$/) || tipInputValue.match(/^[0-9]*\,[0-9]$/)){
                var val_ = tipInputValue + '0';
                setTipAmountEntry(val_);
                return;
            }
            else if( tipInputValue.match(/^[0-9]*\.$/) || tipInputValue.match(/^[0-9]*\,$/) ){
                var val_ = tipInputValue + '00';
                setTipAmountEntry(val_);
                return;
            }
            return;
        }
        
        function goToNextReceiptPressed(postUpdate){
            if(currPage == receiptData.length + summaryPages.length - 1){ return; }
            if(postUpdate && currPage < receiptData.length){
                sendPostToUpdateCurrentReceiptAndSaveInLocalReceiptData();
            }
            load_page(currPage + 1);
        }
        
        function goToPreviousReceiptPressed(postUpdate){
            if(currPage == 0){ return; }
            if(postUpdate && currPage < receiptData.length){
                sendPostToUpdateCurrentReceiptAndSaveInLocalReceiptData();
            }
            try{
            load_page(currPage - 1);
            }
            catch(error){alert(error.message + "\n\n" + error.stack);}
        }
        
        initPagingComponentWithReceiptData();
        setupInnerComponents('<?php echo $monitary_symbol;?>');
        load_page(0);

        </script>
        <?php
        $outputHTML = ob_get_clean();
        return $outputHTML;
    }


//     - General Navigation
//   /
// -------------------------------------------------------------------------------
//   \
//     - Numpad/numpad handling.


    function addNumpad($decimalChar){
        //echo "---------CONTAINING DIV: " . $containingDivID;
        ob_start();
        ?>
        <script type='text/javascript'>
        var tipInput = document.getElementById('tip_input_div_id');
        var decimalChar = '<?php echo $decimalChar;?>';
        var escapedDecimalChar = decimalChar == '.' ? '\\.' : decimalChar;
        function numKeyPressed(key_pressed){
            
            if(isCashReconPage){
                handleNumKeyForRecon(key_pressed);
                return;
            }
            if(isTotalsPage){
                handleNumKeyForTotalTips(key_pressed);
                return;
            }
            if(key_pressed == 'delete'){
                var _val = getTipAmountEntry();
                
                var setTo = _val.substr(0, _val.length - 1);
                setTipAmountEntry(setTo);
            }
            else if(key_pressed == 'enter'){
                goToNextReceiptPressed(true);
            }
            else if( key_pressed == decimalChar || (key_pressed * 1) >= 0 && (key_pressed * 1) <= 9){
                var _val = getTipAmountEntry();
                if( key_pressed == decimalChar && (_val.indexOf(decimalChar) > -1) ){
                    return;
                }
                else if(key_pressed == decimalChar && _val == ''){
                    _val = '0' + decimalChar;
                    setTipAmountEntry(_val);
                }
                else if(_val.match( 
                        new RegExp('^.*'+escapedDecimalChar+'[0-9]{'+decimalPlaceCount+'}$'))){
                    return;                    
                }
                else if(_val == '0' && (key_pressed+'').match(/^[0-9]$/)){
                    setTipAmountEntry("" + key_pressed);
                }
                else{
                    setTipAmountEntry(_val + key_pressed);
                }
            }
        }
        function handleNumKeyForRecon(key_pressed){
          
        
            var reconComponent = document.getElementById('cash_recon_component_div_id');
            var currVal = reconComponent.getSelectedElemValue();
            if(key_pressed == 'delete'){
                var inputVal = (currVal == '') ? currVal : currVal.substr(0, currVal.length - 1);
                reconComponent.setSelectedElemValue(inputVal);
                return;
            }
            else if(key_pressed == 'enter'){
                reconComponent.moveFocusToNextInput();
                return;
            }
            else if( (currVal+'').replace(/&nbsp;/g, '').length == 7 ){
                return;
            }
            else if(key_pressed == '.'){
                return;
            }
            else if((key_pressed + '').match(/^[0-9]$/) && currVal.replace(/&nbsp;/g, '') == '0'){
                //reconComponent.setSelectedElementValue(""+key_pressed);
                var inputVal = '&nbsp;' + key_pressed;
                reconComponent.setSelectedElemValue(inputVal);
                return;
            }
            else if((key_pressed + '').match(/^[0-9]$/)){
                var inputVal = currVal + '' + key_pressed;
                reconComponent.setSelectedElemValue(inputVal);
                return;
            }
        }
        
        function handleNumKeyForTotalTips(key_pressed){
            var totalsComponent = document.getElementById('totals_page_div_id');
            var currVal = totalsComponent.getTipsInputValue();
            var newVal = '';  
            if(key_pressed == 'delete'){
                newVal = currVal.substr(0, currVal.length - 1);
            }
            else if(key_pressed == 'enter'){
                goToNextReceiptPressed(true);
                return;
            }
            else if( key_pressed == decimalChar || (key_pressed * 1) >= 0 && (key_pressed * 1) <= 9){
                if( key_pressed == decimalChar && (currVal.indexOf(decimalChar) > -1) ){
                    return;
                }
                else if(key_pressed == decimalChar && currVal == ''){
                    newVal = '0' + decimalChar;
                }
                else if(currVal.match( 
                        new RegExp('^.*'+escapedDecimalChar+'[0-9]{'+decimalPlaceCount+'}$'))){
                    return;                    
                }
                else if(currVal.replace(/&nbsp;/g, '') == '0' && (key_pressed+'').match(/^[0-9]$/)){
                    newVal = "" + key_pressed;
                }
                else{
                    newVal = currVal + key_pressed;
                }
            }
            totalsComponent.setTipsInputValue(newVal);
        }

        
        var numpad = initializeNumpad(400, 0, 300, 348, decimalChar, numKeyPressed);
        numpad.style.display = 'inline-block';
        numpad.style.top = '20px';
        
        document.getElementById('paging_component_div_id').appendChild(numpad);
        </script>
        <?php
        return ob_get_clean();
    }
?>