<script type='text/javascript'>

    var _totals_page_component_ref;


    function generateTotalsPage(componentRef, builderObject){

        _totals_page_component_ref = componentRef;
        for(var attributeName in builderObject){
            componentRef[attributeName] = builderObject[attributeName];
        }

        componentRef.style.width = componentRef.width;
        componentRef.style.height = componentRef.height;
        //componentRef.style.border = "1px solid #eee";
        componentRef.display = _display;
        componentRef.getTipsInputValue = _getTipsInputValue;
        componentRef.setTipsInputValue = _setTipsInputValue;
        componentRef.getComplementaryOutputValue = _getComplementaryOutputValue;
        componentRef.finalSanitizeInputAmount = _finalSanitizeInputAmount;
        //Below already loaded, possible values: 'cash_only' or 'total_including_cc'
        //This affects the title of the total tips label.
        if(componentRef.askForTotalOrJustCashTips == 'total_including_cc'){
            componentRef.tipsLabelTitle = 'TOTAL TIPS:';
            componentRef.compLabelTitle = 'Calculated Cash Tips';
        }else{
            componentRef.tipsLabelTitle = 'CASH TIPS:';
            componentRef.compLabelTitle = 'Total Tips';
        }

        //getTipsInputValue and setTipsInputValue is for the input div, what this value
        // means if different based on .askForTotalOrJustCashTips.  Thus we retrieve
        // the full total, cash + cc, from a function that accounts for this.
        componentRef.getTotalTipsCashAndCC = _getTotalTipsIncludingCashAndCCs;
        componentRef.getTotalCCTipAmount = ___getTotalCCTipAmount;
        componentRef.getCashTips = _getCashTips;
        //addSubcomponents(componentRef);
        addInnerComponents(componentRef);
    }

    function addInnerComponents(componentRef){


        //<>~--------------------------------------------------
        //                                                      \
        //                                                      THE TABLE

        //We can use the classes from the receipt's page.

        /*
        The two css classes from the receipt page.
        .receiptInfoLabel
        .receiptInfoValue
        */
        componentRef.textAlign = 'left';
        var displayTable = document.createElement('table');
        displayTable.style.left = '5px';
        displayTable.style.top = '25px';
        displayTable.cellSpacing;
        displayTable.border = '0';
        //displayTable.style.width = '350px';
        displayTable.style.position = 'absolute';
        displayTable.style.display = 'inline-block';
        //displayTable.style.border = '1px solid #aaa';
        generalFontSize = '12px';
        //The Compliment Row, if input is total_tips, then this is cash tips,
        //   if input is 'cash tips' then this is total tips
        //Complement Row
        var complementRow = document.createElement('tr');
        var complementRowLabel = document.createElement('td');
        complementRowLabel.className = 'receiptInfoLabel';
        complementRowLabel.style.textAlign = 'right';
        complementRowLabel.innerHTML = componentRef.compLabelTitle + ':';
        //complementRowLabel.style.fontSize = generalFontSize;
        complementRowLabel.id = 'complement_row_label_td_id';
        var complementRowValue = document.createElement('td');
        complementRowValue.className = 'receiptInfoValue';
        complementRowValue.id = 'complement_row_value_td_id';
        //complementRowValue.style.fontSize = generalFontSize;
        complementRowValue.innerHTML = '&nbsp;';//Overwritten in 'display()'
        complementRow.appendChild(complementRowLabel);
        complementRow.appendChild(complementRowValue);
        displayTable.appendChild(complementRow);

        //Total Sales
        var totalSalesRow = document.createElement('tr');
        var totalSalesLabelTD = document.createElement('td');
        totalSalesLabelTD.className = 'receiptInfoLabel';
        totalSalesLabelTD.style.textAlign = 'right';
        //totalSalesLabelTD.style.fontSize = generalFontSize;
        totalSalesLabelTD.innerHTML = "Total Sales:";
        var totalSalesValueTD = document.createElement('td');
        totalSalesValueTD.className = 'receiptInfoValue';
        //totalSalesValueTD.style.fontSize = generalFontSize;
        var tcsddHTML = componentRef.monitarySymbol + componentRef.totalSales.toFixed(componentRef.decimalCount);
        totalSalesValueTD.innerHTML = tcsddHTML;
        totalSalesRow.appendChild(totalSalesLabelTD);
        totalSalesRow.appendChild(totalSalesValueTD);
        displayTable.appendChild(totalSalesRow);

        //Total Cash Sales
        var totalCashSalesRow = document.createElement('tr');
        var totalCashSalesLabelTD = document.createElement('td');
        totalCashSalesLabelTD.className = 'receiptInfoLabel';
        totalCashSalesLabelTD.style.textAlign = 'right';
        //totalCashSalesLabelTD.style.fontSize = generalFontSize;
        totalCashSalesLabelTD.innerHTML = "Total Cash Sales:";
        var totalCashSalesValueTD = document.createElement('td');
        totalCashSalesValueTD.className = 'receiptInfoValue';
        //totalCashSalesValueTD.style.fontSize = generalFontSize;
        totalCashSalesValueTD.innerHTML = componentRef.monitarySymbol + componentRef.totalCashSales.toFixed(componentRef.decimalCount);
        totalCashSalesRow.appendChild(totalCashSalesLabelTD);
        totalCashSalesRow.appendChild(totalCashSalesValueTD);
        displayTable.appendChild(totalCashSalesRow);

        //Total CC_Tips
        var totalCCTipsRow = document.createElement('tr');
        var totalCCTipsLabel = document.createElement('td');
        totalCCTipsLabel.className = 'receiptInfoLabel';
        totalCCTipsLabel.style.textAlign = 'right';
        //totalCCTipsLabel.style.fontSize = generalFontSize;
        totalCCTipsLabel.innerHTML = 'Total CC Tips:';
        var totalCCTipsValue = document.createElement('td');
        totalCCTipsValue.className = 'receiptInfoValue';
        //totalCCTipsValue.style.fontSize = generalFontSize;
        totalCCTipsValue.innerHTML = 'TOTAL CC TIPS VALUE'; //Overwritten in 'display()'.
        totalCCTipsValue.id = 'total_cc_tips_display_td_id';
        totalCCTipsRow.appendChild(totalCCTipsLabel);
        totalCCTipsRow.appendChild(totalCCTipsValue);
        displayTable.appendChild(totalCCTipsRow);

        //Cash Reconcilliation Info if tilled out.
        if(componentRef.isUsingServerCashRecon){
            //Till in amount row.
            var tillInAmountRow = document.createElement('tr');
            var tillInAmountLabel = document.createElement('td');
            tillInAmountLabel.className = 'receiptInfoLabel';
            //tillInAmountLabel.style.fontSize = generalFontSize;
            tillInAmountLabel.style.textAlign = 'right';
            tillInAmountLabel.innerHTML = 'Till-In Total Cash:';
            var tillInAmountValue = document.createElement('td');
            tillInAmountValue.className = 'receiptInfoValue';
            //tillInAmountValue.style.fontSize = generalFontSize;
            tillInAmountValue.innerHTML = componentRef.monitarySymbol + componentRef.totalTillIn.toFixed(componentRef.decimalCount);
            tillInAmountRow.appendChild(tillInAmountLabel);
            tillInAmountRow.appendChild(tillInAmountValue);
            displayTable.appendChild(tillInAmountRow);

            //Till out amount row.
            var tillOutAmountRow = document.createElement('tr');
            var tillOutAmountLabel = document.createElement('td');
            tillOutAmountLabel.className = 'receiptInfoLabel';
            tillOutAmountLabel.style.textAlign = 'right';
            //tillOutAmountLabel.style.fontSize = generalFontSize;
            tillOutAmountLabel.innerHTML = 'Till-Out Total Cash:';
            var tillOutAmountValue = document.createElement('td');
            tillOutAmountValue.className = 'receiptInfoValue';
            tillOutAmountValue.id = 'till_out_amount_value_td_id';
            //tillOutAmountValue.style.fontSize = generalFontSize;
            tillOutAmountValue.innerHTML = '';//Overwritten in 'display()';
            tillOutAmountRow.appendChild(tillOutAmountLabel);
            tillOutAmountRow.appendChild(tillOutAmountValue);
            displayTable.appendChild(tillOutAmountRow);
        }

        //Till over-under row.
        var tillOutOverUnderRow = document.createElement('tr');
        var tillOutOverUnderLabel = document.createElement('td');
        tillOutOverUnderLabel.className = 'receiptInfoLabel';
        tillOutOverUnderLabel.style.textAlign = 'right';
        //tillOutOverUnderLabel.style.fontSize = generalFontSize;
        tillOutOverUnderLabel.innerHTML = 'Over / Short:';
        tillOutOverUnderLabel.id = 'till_out_over_under_label_td_id';
        var tillOutOverUnderValue = document.createElement('td');
        tillOutOverUnderValue.className = 'receiptInfoValue';
        tillOutOverUnderValue.id = 'till_out_over_under_value_td_id';
        //tillOutOverUnderValue.style.fontSize = generalFontSize;
        tillOutOverUnderValue.innerHTML = '';//Overwritten in 'display()'
        tillOutOverUnderRow.appendChild(tillOutOverUnderLabel);
        tillOutOverUnderRow.appendChild(tillOutOverUnderValue);
        displayTable.appendChild(tillOutOverUnderRow);
        

        //Finally, add the table.
        componentRef.appendChild(displayTable);


        //--------------------------------------------
        //                                            \
        //                                    The Tips Input Area

        // Accent Div
        var accentDiv = document.createElement('div');
        accentDiv.style.position = 'absolute';
        accentDiv.style.left = '25px';
        accentDiv.style.top  = '202px';
        accentDiv.style.height = '80px';
        accentDiv.style.width = '347px';
        accentDiv.style.borderTop = '1px solid black';
        accentDiv.style.borderBottom = '1px solid black';
        componentRef.appendChild(accentDiv);

        // Monitary Symbol
        var monitarySymbolDiv = document.createElement('div');
        monitarySymbolDiv.style.position = 'absolute';
        monitarySymbolDiv.style.top = '208px';
        monitarySymbolDiv.style.left = '135px';
        monitarySymbolDiv.style.width = '20px';
        monitarySymbolDiv.style.height = '100%';
        monitarySymbolDiv.style.fontFamily = 'verdana';
        monitarySymbolDiv.style.fontSize = '50px';
        monitarySymbolDiv.style.color = '#5f698a';
        monitarySymbolDiv.innerHTML = componentRef.monitarySymbol;
        componentRef.appendChild(monitarySymbolDiv);

        // Tips Input
        var tipsLabelDiv = document.createElement('div');
        tipsLabelDiv.style.position = 'absolute';
        tipsLabelDiv.className = 'tipInputLabel';
        tipsLabelDiv.innerHTML = componentRef.tipsLabelTitle;
        componentRef.appendChild(tipsLabelDiv);
        var tipsValueInput = document.createElement('div');
        tipsValueInput.id = 'tips_value_input_div_id';
        tipsValueInput.className = 'tipInputDivStyle';
        tipsValueInput.style.left = '171px';
        tipsValueInput.style.width = '198px';
        //tipsValueInput.style.textDecoration = 'underline';
        //tipsValueInput.style.minWidth = '200px';
        //tipsValueInput.style.border = '1px solid #9b9';
        //tipsValueInput.style.backgroundColor = '#eafAf4';
        //tipsValueInput.style.fontSize = '36px';
        //tipsValueInput.style.color = '#777';
        tipsValueInput.innerHTML = '&nbsp;';
        componentRef.appendChild(tipsValueInput);



    }



    function _getTotalTipsIncludingCashAndCCs(){
        if(this.askForTotalOrJustCashTips == 'total_including_cc'){
            //Then we just return from the input
            var tipsInputValue = this.getTipsInputValue();
            if(tipsInputValue == ''){tipsInputValue = 0.0;}
            var returnVal = parseFloat(tipsInputValue);
            return isNaN(returnVal) ? 'ERR' : returnVal.toFixed(this.decimalCount);
        }else{
            var tipsInputValue = this.getTipsInputValue();
            if(tipsInputValue == ''){tipsInputValue = 0.0;}
            var returnVal = parseFloat(tipsInputValue) + parseFloat(_getTotalCCTipAmount(this));
            return isNaN(returnVal) ? 'ERR2' : returnVal.toFixed(this.decimalCount);
        }
    }

    function _getComplementaryOutputValue(){

        var tipsInputValue = this.getTipsInputValue();
        if(tipsInputValue == ''){ tipsInputValue = '0'; }
        if(this.askForTotalOrJustCashTips == 'total_including_cc'){
            //Then we just return from the input
            var returnVal = parseFloat( tipsInputValue ) - parseFloat( _getTotalCCTipAmount(this) );
            return isNaN(returnVal) ? '' : returnVal.toFixed(this.decimalCount);
        }else{
            var returnVal = parseFloat( tipsInputValue ) + parseFloat( _getTotalCCTipAmount(this) );
            return isNaN(returnVal) ? '' : returnVal.toFixed(this.decimalCount);
        }
    }

    //This is just for the input div!!! Could mean total cash tips or cash+cc based on context.
    function _getTipsInputValue(){
        var totalTipsDivInput = document.getElementById('tips_value_input_div_id');
        var totalTipsValue = totalTipsDivInput.innerHTML;
        totalTipsValue = totalTipsValue.replace(/&nbsp;/g, '');
        return totalTipsValue;
    }
    //This is just for the input div!!! Could mean total cash tips or cash+cc based on context.
    function _setTipsInputValue(value){
        if(value == ''){value='&nbsp;';}
        var totalTipsDivInput = document.getElementById('tips_value_input_div_id');
        totalTipsDivInput.innerHTML = value;
        var complementTipNumberDisplay = document.getElementById('complement_row_value_td_id');
        complementTipNumberDisplay.innerHTML = this.monitarySymbol + this.getComplementaryOutputValue();
/*
        totals_page_component.cash_recon_arg = cashReconArg;
        totals_page_component.totalTillIn = totalTillIn < 0 ? 0 : totalTillIn;
        totals_page_component.decimalPlaceCount = decimalPlaceCount;
*/
        this.display(this.cash_recon_arg, this.totalTillIn, this.decimalPlaceCount);
    }

    function _getTotalCCTipAmount(componentRef){
        //We calculate the total tips from the receipts
        var ccTipsAmountTotals = 0;
        for(i = 0; i < this.receiptData.length; i++){
            ccTipsAmountTotals += parseFloat( this.receiptData[i]['tip_amount'] );
        }
        return ccTipsAmountTotals;
    }
    function ___getTotalCCTipAmount(){
        return _getTotalCCTipAmount(this).toFixed(this.decimalCount);
    }
    function _getCashTips(){
        var totalTips = this.getTotalTipsCashAndCC();
        var totaCCTips = this.getTotalCCTipAmount();

        return parseFloat(totalTips - totaCCTips).toFixed(this.decimalCount);
    }

    //THESE WILL BE PULLED INTO THE SUBMISSION DATA, SO WE KEEP THEM GLOBAL
    //var server_deposite_to_restaurant_amount = 0;//Was already done.
    var server_deposite_take_title = '';
    function _display(cashReconData, totalTillIn, decimalCount){

        var ccTipsAmountTotals = _getTotalCCTipAmount(this);

        var ccTipTotalDisplayTD = document.getElementById('total_cc_tips_display_td_id');
        ccTipTotalDisplayTD.innerHTML = this.monitarySymbol + ccTipsAmountTotals.toFixed(decimalCount);

        var calculatedTips;
        //HERE WE GATHER ALL THE NEEDED VARS TO CALL 
        //calculateServerDepositePayoutAmount(cash_sales, cc_sales, cash_tips, cc_tips, till_in, till_out)
        var cashSales = this.totalCashSales;
        var ccSales = this.totalSales - totalCashSales;
        var cashTips = this.getCashTips();
        var ccTips = ccTipsAmountTotals;
        var tillIn  = cashReconData ? totalTillIn : 0;
        var tillOut = cashReconData ? cashReconData.totalCash : 0;
        var deposite_payout_val = calculateServerDepositePayoutAmount(cashSales, ccSales, cashTips, ccTips, tillIn, tillOut);
        deposite_payout_val = deposite_payout_val.toFixed(decimalCount);
        var overShortAmount = deposite_payout_val;//We're now changing the meaning of this var.
        server_deposite_to_restaurant_amount = deposite_payout_val;
        if(cashReconData){
            var tillOutAmountTD = document.getElementById('till_out_amount_value_td_id');
            tillOutAmountTD.innerHTML = this.monitarySymbol + parseFloat(cashReconData.totalCash).toFixed(decimalCount);
            //var overShortAmount = (cashReconData.totalCash - totalTillIn) - this.totalCashSales;
            this.overShortAmount = overShortAmount;
            var tillOverUnderTD_label = document.getElementById('till_out_over_under_label_td_id');
            //Over / Short
            if(overShortAmount > 0){
                //tillOverUnderTD_label.innerHTML = "<span style='text-decoration:underline;'>Over</span> / Short";
                tillOverUnderTD_label.innerHTML = "Restaurant To Pay Out Server:";
                server_deposite_take_title = "Restaurant To Pay Out Server";
            }
            else if(overShortAmount < 0){
                //tillOverUnderTD_label.innerHTML = "Over / <span style='text-decoration:underline;'>Short</span>";
                tillOverUnderTD_label.innerHTML = "Server Deposite To Restaurant:";
                server_deposite_take_title = "Server Deposite To Restaurant";
            }
            else{
                //tillOverUnderTD_Label.innerHTML = "Over / Short";
                tillOverUnderTD_label.innerHTML = "Restaurant To Pay Out Server:";
                server_deposite_take_title = "Restaurant To Pay Out Server";
            }
            ///var tillOverUnderTD = document.getElementById('till_out_over_under_value_td_id');

            //calculateServerDepositePayoutAmount(cash_sales, cc_sales, cash_tips, cc_tips, till_in, till_out)
            ///tillOverUnderTD.innerHTML = this.monitarySymbol + Math.abs(parseFloat(overShortAmount)).toFixed(decimalCount);
        }
        else{
            var tillOverUnderTD_label = document.getElementById('till_out_over_under_label_td_id');
            if(overShortAmount > 0){
                tillOverUnderTD_label.innerHTML = "Server Deposite To Restaurant:";
                server_deposite_take_title = "Server Deposite To Restaurant";
            }else{
                tillOverUnderTD_label.innerHTML = "Restaurant To Pay Out Server:";
                server_deposite_take_title = "Restaurant To Pay Out Server";
            }

            ///var tillOverUnderTD = document.getElementById('till_out_over_under_value_td_id');
            ///tillOverUnderTD.innerHTML = this.monitarySymbol + Math.abs(this.totalCashSales);
        }
        var tillOverUnderTD = document.getElementById('till_out_over_under_value_td_id');
        tillOverUnderTD.innerHTML = this.monitarySymbol + Math.abs( overShortAmount ).toFixed(decimalCount);

        var complementTipNumberDisplay = document.getElementById('complement_row_value_td_id');
        complementTipNumberDisplay.innerHTML = this.monitarySymbol + Math.max(0,this.getComplementaryOutputValue()).toFixed(decimalCount);

    }

    function _finalSanitizeInputAmount(){
         var totalTipsDivInput = document.getElementById('tips_value_input_div_id');
         var currTipsInputValue = this.getTipsInputValue();
         if( !currTipsInputValue || currTipsInputValue == ''){
             currTipsInputValue = '0.00';
         }
         else if( currTipsInputValue.match(/^.*\.$/) || currTipsInputValue.match(/^.*,$/) ){
             currTipsInputValue = currTipsInputValue + '00';
         }
         this.setTipsInputValue(currTipsInputValue);
    }
</script>