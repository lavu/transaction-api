<?php

    //$lines = generateRepeatedCharacter('-',27);
    
    
    $dataStr = $_REQUEST['script_args'];
    $dataParts = explode('(and)', $dataStr);
    
    $getArgs = array();
    foreach($dataParts as $currEntry){
        $entryParts = explode('(eq)', $currEntry);
        $getArgs[$entryParts[0]] = $entryParts[1];
    }
    
    $servers_id = $getArgs['servers_id'];
    $data_name = $getArgs['data_name'];
    $sec_hash = $getArgs['sec_hash'];
    $sec_hash_calc = 'hashbeaneggs' . $data_name;
    $sec_hash_calc = sha1($sec_hash_calc);
    $loc_id = $getArgs['location_id'];
    $checkedInTime  = $getArgs['checked_in_time'];
    $checkedOutTime = $getArgs['checked_out_time'];
    $hours = $getArgs['hours'];
    $lines = genRepeatedChar('-', 27);
    
    
    
    if($sec_hash != $sec_hash_calc){
        return "1|receipt::Err. Could not verify location.";
    }
    
    $_POST['cc'] = $_REQUEST['cc'] = $_GET['cc'] = $data_name;
    require_once(dirname(__FILE__) . '/../../info_inc.php');
    
    $location_row = getLocationRow($loc_id);
    $title = $location_row['title'];

    $servers_user_row = getServersUserRow($servers_id);
    $serversFullName = $servers_user_row['f_name'] . ' ' . $servers_user_row['l_name'];
    
    
    $checkedInDateTime  = str_replace(":","[c", $checkedInTime);
    $checkedOutDateTime = str_replace(":","[c", $checkedOutTime);
    
    $checkedInTime  = explode(' ', $checkedInDateTime);
    $checkedInTime  = $checkedInTime[1];
    $checkedOutTime = explode(' ', $checkedOutDateTime);
    $checkedOutTime = $checkedOutTime[1];
    
    // MAKE THE RECEIPT STRING
    $print_string  = "1|receipt::";
    $print_string .= $lines . ":";
    $print_string .= "Clocked Out".":";
    $print_string .= $title.":";
    $print_string .= $checkedOutTime.":";
    $print_string .= $lines . ":";
    $print_string .= "::";
    $print_string .= $serversFullName.":";
    $print_string .= "In at ".$checkedInTime.":";
    $print_string .= "Out at ".$checkedOutTime.":";
    $print_string .= "::";
    $print_string .= "Hours[c ".$hours.":";
    $print_string .= ":::::::::";
    echo $print_string;
    
    function getLocationRow($loc_id){
        $result = lavu_query("SELECT * FROM `locations` WHERE `id`='[1]'", $loc_id);
        if(!$result){
            error_log('mysql error in tips_wizard_clockout_print.php -87af5- error:' . lavu_dberror());
            return false;
        }
        else if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        else{
            return array();
        }
    }
    function getServersUserRow($server_id){
        $result = lavu_query("SELECT * FROM `users` WHERE `id`='[1]'", $server_id);
        if(!$result){
            error_log('mysql error tips_wizard_clockout_print.php -gsrj4- error:' . lavu_dberror());
            return false;
        }
        else if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        else{
            return array();
        }
    }
    function genRepeatedChar($char, $length){
        $strBuilder = '';
        for( $i=0;$i<$length;$i++){ $strBuilder .= $char; }
        return $strBuilder;
    }
?>