<?php
    //
    $isLocalServer = strpos(__FILE__, 'poslavu-local') !== false;

    $isDev = strpos(__FILE__, '/dev/') !== false;
    $backOutOfDevStr = $isDev ? "../" : "";


    $pathToEditMonetary = dirname(__FILE__) . '/../../../' . $backOutOfDevStr . 'cp/areas/edit_monetary_denominations.php';

    //We now have the function: [    get_db_denominations();    ]
    require_once($pathToEditMonetary);

    $data_name;//Defined in info_inc.php
    $sec_hash_for_printing = 'hashbeaneggs' . $data_name;
    $sec_hash_for_printing = sha1($sec_hash_for_printing);

    //$got_loc_id is valid on admin, but empty on lls, on lls it's:
    //  $server_remote_locationid as defined in local_settings.php.
    $got_loc_id = empty($got_loc_id) ? $server_remote_locationid : $got_loc_id;
    $location_row = getLocationRowAsArray($got_loc_id);

    $REQUEST_register = str_replace("'","\\'",$_REQUEST['register']);
    $REQUEST_register_name = str_replace("'","\\'",$_REQUEST['register_name']);
    $SESSION_UUID = $_SESSION['UUID'];

    //So we don't have to do the same query over and over again.
    $savedTransactions = false;
    /*
    global $All_SERVER_CC_TRANSACTIONS;
    $All_SERVER_CC_TRANSACTIONS = false;
    */

// Date handling functions
    function getDayStartEndTime(){
        global $got_loc_id, $location_row;
        // MADE GLOBAL $location_row = getLocationRowAsArray($got_loc_id);

        //  Start/End Time Shenanagans.
        $dayStartEndTime = $location_row['day_start_end_time'];
        $dayStartEndHour = floor($dayStartEndTime / 100);
        $dayStartEndHour = strlen($dayStartEndHour) == 1 ? '0' . $dayStartEndHour : $dayStartEndHour;
        $dayStartEndMins = $dayStartEndTime % 100;
        $dayStartEndMins = strlen($dayStartEndMins) == 1 ? '0' . $dayStartEndMins : $dayStartEndMins;
        $dayStartEndTime_His = $dayStartEndHour . ":" . $dayStartEndMins . ":" . "00";
        return $dayStartEndTime_His;
    }

    //Appends the day-start time to the given date.
    function getFiscalDayStartTimeGivenDate($date){
        global $dayStartEndTime_His;
        $dayStartEndTime_His = getDayStartEndTime();
        $returnDateTime = date('Y-m-d', strtotime($date)) . ' ' . $dayStartEndTime_His;
        return $returnDateTime;
    }

    //For Server Deposit to Restaurant/Restaurant to Pay Out Server expression
    function getServerReconCalcSettingsJavascriptFunc(){
        global $savedTransactions;
        $gcPiPoTotals = getTotalGiftCertificateTotalPaidInTotalPaidOut();
        $srcs = getServerReconCalcSettings();
        $s_r = $srcs['summary_rules'];
        $t_r = $srcs['tip_rules'];
        $gc = $gcPiPoTotals['gift_card_total'];
        $pi = $gcPiPoTotals['paid_in_total'];
        $po = $gcPiPoTotals['paid_out_total'];


        // TRICKY !!!    WE ARE ASSUMING SOME JS VARIABLES TO EXIST FOR THIS TO WORK.
        // We are going to create 10 expressions based on settings.  When these expressions
        // are put in a string sequence we will get a valid JS expression for the final calculation.
        $paid_in_summary_expression  = $s_r['server_till_customization_settings_paid_in'];
        $paid_in_summary_expression  = $paid_in_summary_expression == 'ignore'  ? '' : $paid_in_summary_expression  . ' ' . $pi . ' ';//Will be constant in JS-Land
        $paid_out_summary_expression = $s_r['server_till_customization_settings_paid_out'];
        $paid_out_summary_expression = $paid_out_summary_expression == 'ignore' ? '' : $paid_out_summary_expression . ' ' . $po . ' ';//Will be constant in JS-Land
        $total_cash_sales_summary_expression  = $s_r['server_till_customization_settings_cash'];
        $total_cash_sales_summary_expression  = $total_cash_sales_summary_expression == 'ignore'  ? '' : $total_cash_sales_summary_expression . ' ' . 'cash_sales' . ' ';
        $gift_certificate_summary_expresssion = $s_r['server_till_customization_settings_gift_certificate'];
        $gift_certificate_summary_expresssion = $gift_certificate_summary_expresssion == 'ignore' ? '' : $gift_certificate_summary_expresssion . ' ' . 'gc' . ' ';
        $credit_card_summary_expression = $s_r['server_till_customization_settings_credit_card'];
        $credit_card_summary_expression = $credit_card_summary_expression == 'ignore' ? '' : $credit_card_summary_expression . ' ' . 'cc_sales' . ' ';

        $paid_in_tips_expression  = $t_r['server_till_customization_settings_paid_in'];
        $paid_in_tips_expression  = $paid_in_tips_expression == 'ignore'  ? '' : $paid_in_tips_expression  . ' ' . $pi . ' ';//Will be constant in JS-Land
        $paid_out_tips_expression = $t_r['server_till_customization_settings_paid_out'];
        $paid_out_tips_expression = $paid_out_tips_expression == 'ignore' ? '' : $paid_out_tips_expression . ' ' . $po . ' ';//Will be constant in JS-Land
        $total_cash_sales_tips_expression  = $t_r['server_till_customization_settings_cash'];
        $total_cash_sales_tips_expression  = $total_cash_sales_tips_expression == 'ignore'  ? '' : $total_cash_sales_tips_expression . ' ' . 'cash_tips' . ' ';
        $gift_certificate_tips_expresssion = $t_r['server_till_customization_settings_gift_certificate'];
        $gift_certificate_tips_expresssion = $gift_certificate_tips_expresssion == 'ignore' ? '' : $gift_certificate_tips_expresssion . ' ' . 'gc' . ' ';
        $credit_card_tips_expression = $t_r['server_till_customization_settings_credit_card'];
        $credit_card_tips_expression = $credit_card_tips_expression == 'ignore' ? '' : $credit_card_tips_expression . ' ' . 'cc_tips' . ' ';

        $js_calculation_expression = '0 ' .
            $paid_in_summary_expression .
            $paid_out_summary_expression .
            $total_cash_sales_summary_expression .
            $gift_certificate_summary_expresssion .
            $credit_card_summary_expression .
            $paid_in_tips_expression .
            $paid_out_tips_expression .
            $total_cash_sales_tips_expression .
            $gift_certificate_tips_expresssion .
            $credit_card_tips_expression;

        $expression_debug = "ExpressionParts|||paid_in_summary_expression[$paid_in_summary_expression]|".
                                              "paid_out_summary_expression[$paid_out_summary_expression]|".
                                              "total_cash_sales_summary_expression[$total_cash_sales_summary_expression]|".
                                              "gift_certificate_summary_expresssion[$gift_certificate_summary_expresssion]|".
                                              "credit_card_summary_expression[$credit_card_summary_expression]|".
                                              "paid_in_tips_expression[$paid_in_tips_expression]|".
                                              "paid_out_tips_expression[$paid_out_tips_expression]|".
                                              "total_cash_sales_tips_expression[$total_cash_sales_tips_expression]|".
                                              "gift_certificate_tips_expresssion[$gift_certificate_tips_expresssion]|".
                                              "credit_card_tips_expression[$credit_card_tips_expression]";
        //For later debugging:
        //error_log("Expression: " . $js_calculation_expression);

        $expressionParametersDebug = '';

        //"alert('cash sales:' + cash_sales + ' cc_sales:' + cc_sales + ' cash_tips:' + cash_tips + ' cc_tips:' + cc_tips + ' till_in:' + till_in + ' till_out:' + till_out);".
        $strB  = "<script type='text/javascript'>".
                     "var calculated_expression_used_server_turn_in='';".
                     "var calculated_value_server_turn_in='';".
                     "var calculated_expression_all_parts='$expression_debug';".
                     "var calculated_value_param_args='';".
                     "function calculateServerDepositePayoutAmount(cash_sales, cc_sales, cash_tips, cc_tips, till_in, till_out){".
                          "var gc=$gc;".//Gift Certificates total
                          "var pi=$pi;".//Paid In total
                          "var po=$po;".//Paid Out total
                          "calculated_expression_used_server_turn_in='$js_calculation_expression';".
                          "calculated_value_server_turn_in=$js_calculation_expression;".
                          "calculated_value_param_args='cash_sales['+cash_sales+']|cc_sales['+cc_sales+']|cash_tips['+cash_tips+']|cc_tips['+cc_tips+']|till_in['+till_in+']|till_out['+till_out+']';".
                          "return $js_calculation_expression;".
                     "}".
                 "</script>";
        return $strB;
    }

    function getTotalGiftCertificateTotalPaidInTotalPaidOut(){
        //global $ALL_SERVER_CC_TRANSACTIONS;
        global $savedTransactions;

        $giftCardTotal = 0.0;
        $paidInTotal   = 0.0;
        $paidOutTotal  = 0.0;
        if(!empty($savedTransactions)){
            //We first calculate the total gift cards
            foreach($savedTransactions as $currRow){
                if($currRow['pay_type'] == 'Gift Certificate'){
                    $giftCardTotal += ($currRow['amount'] * 1);
                }
                else if($currRow['order_id'] == 'Paid In'){
                    $paidInTotal += ($currRow['amount'] * 1);
                }
                else if($currRow['order_id'] == 'Paid Out'){
                    $paidOutTotal += ($currRow['amount'] * 1);
                }
            }
        }
        return array("gift_card_total" => $giftCardTotal, "paid_in_total" => $paidInTotal, "paid_out_total" => $paidOutTotal);
    }

    function getServerReconCalcSettings(){
        //We default the settings arrays.
        $summaryRules = array('server_till_customization_settings_paid_in'  => '+',
                              'server_till_customization_settings_paid_out' => '-',
                              'server_till_customization_settings_cash'     => '+',
                              'server_till_customization_settings_gift_certificate' => 'ignore',
                              'server_till_customization_settings_credit_card' => 'ignore' );

        $tipRules     = array('server_till_customization_settings_paid_in'  => 'ignore',
                              'server_till_customization_settings_paid_out' => 'ignore',
                              'server_till_customization_settings_cash'     => 'ignore',
                              'server_till_customization_settings_gift_certificate' => 'ignore',
                              'server_till_customization_settings_credit_card' => '-' );

        $result = lavu_query("SELECT * FROM `cash_data` WHERE `setting`='server_till_customization_settings_paid_in' OR ".
                                                              "`setting`='server_till_customization_settings_paid_out' OR ".
                                                              "`setting`='server_till_customization_settings_cash' OR ".
                                                              "`setting`='server_till_customization_settings_gift_certificate' OR ".
                                                              "`setting`='server_till_customization_settings_credit_card'");
        if(!$result){error_log("bad query in tips_wizard_data.php error:" . lavu_dberror());}
        //We override the rules if they are set in the database.
        $resultArr = array();
        if($result && mysqli_num_rows($result)){
            while($curr = mysqli_fetch_assoc($result)){
                $resultArr[ $curr['setting'] ] = $curr;
            }
        }

        foreach($resultArr as $key => $val){
            $summaryRules[$key] = $val['value'];
            $tipRules[$key] = $val['value2'];
        }
        return array('summary_rules' => $summaryRules, 'tip_rules' => $tipRules);
    }

    //For instance if it were 1:30 am on saturday morning and we are closing with a start/end time of 4am
    //  then passing:  (Saturday's date) 01:30:00    will return the date time at which the work day started,
    //  which would be:  (Friday's date) 04:00:00.
    //
    //  This is useful in finding all transactions for a given 'fiscal' day.
    function getFiscalDayStartGivenDatetime($dateTime){
        //Compare the seconds at the time to see if the 'fiscal' date is previous, same, or next.
        $dateTimeParts = explode(' ', $dateTime);
        $timeParts = explode(':', $dateTimeParts[1]);
        $timeHrs = $timeParts[0];
        $timeMin = $timeParts[1];
        $timeSec = $timeParts[2];
        $totalOfDay_secs = $timeHrs * 3600 + $timeMin * 60 + $timeSec;

        //The fiscal start in secs.
        $startEndTime = getDayStartEndTime();
        $startEndTimeParts = explode(':', $startEndTime);
        $startEndTimeHrs = $startEndTimeParts[0];
        $startEndTimeMins = $startEndTimeParts[1];
        $startEndTime_secs = $startEndTimeHrs * 3600 + $startEndTimeMins * 60;

        //Debug
        //return "Total day seconds: " . $totalOfDay_secs . "<br>Start End Time seconds: " . $startEndTime_secs . "<br>";

        //If the seconds of the '$dateTime' time is less that startEndTime in secs, then yet's fiscally one day previous.
        $fiscalDate;
        if( $totalOfDay_secs < $startEndTime_secs){
            $fiscalDate = date('Y-m-d', strtotime( '-1 day', strtotime($dateTimeParts[0])));
        }
        else{
            $fiscalDate = date('Y-m-d', strtotime($dateTimeParts[0]));
        }
        return $fiscalDate . ' ' . $startEndTime;
    }
// End of date handling functions





// Query functions

    //SELECTS etc., INITIAL GET DATA QUERIES...
    function getCompanyID($dataName){
        global $isLocalServer;
        static $returnValCompID = false;
        if($returnValCompID){
            return $returnValCompID;
        }
        if($isLocalServer){
            $settingsArr = getSettingsFileAsArray();
            $returnValCompID = empty($settingsArr['company_id']) ? false : $settingsArr['company_id'];
            return $returnValCompID;
        }
        else{
            $rowFromMainRestaraunts = getCompanyRestaurantTableRowFromMain($data_name);
            $returnValCompID = $rowFromMainRestaraunts['id'];
            return $returnValCompID;
        }
    }
    function getSettingsFileAsArray(){
        $fileLocation = "/poslavu-local/local/settings.txt";
        $fp = fopen($fileLocation, 'r');
        $contents = fread($fp, filesize($fileLocation));
        fclose($fp);
        $settingsFileLines = explode("\n", $contents);
        $settingsArr = array();
        foreach($settingsFileLines as $currLine){
            $lineParts = explode('=', $currLine);
            $lineParts[0] = trim($lineParts[0]);
            $lineParts[1] = trim($lineParts[1]);
            $settingsArr[$lineParts[0]] = $lineParts[1];
        }
        return $settingsArr;
    }
    function getServerID(){
        if(!empty($_REQUEST['server_id'])){
            return $_REQUEST['server_id'];
        }
        return admin_info('loggedin');
    }
    function getLocationRowAsArray($possibleLocID){
        global $got_loc_id;
        static $locRow = false;
        if($locRow)
            return $locRow;
        $locRowResult;
        if($got_loc_id){ $locRowResult = lavu_query("SELECT * FROM `locations` WHERE `id`='[1]'", $possibleLocID); }
        else{            $locRowResult = lavu_query("SELECT * FROM `locations` ORDER BY `id` ASC LIMIT 1"); }
        if(!$locRowResult){ error_log("MYSQL error -skefe4- in /lib/tips_wizard/tips_wizard_data.php, error: " . lavu_dberror()); }
        else if(mysqli_num_rows($locRowResult)){ return $locRow = mysqli_fetch_assoc($locRowResult); }
        else{ error_log("Error trying to find location row inside of /lib/tips_wizard/tips_wizard_data.php"); return false; }
    }

    function getCompanyRestaurantTableRowFromMain($data_name){
        global $data_name;
        $result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $data_name);
        if(!$result){
            error_log("mysql error in tips_wizard_data.php -sg83hdh9a- mysql error:" . mlavu_dberror());
            return false;
        }
        else if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        else{
            return array();
        }
    }
    //Also includes the tablename
    function getServerCreditCardTransactionsAndTableForFiscalDate($serverID, $fiscalDate, $onlyWhereNotEntered = true){
        global $got_loc_id;
        $fiscalDateDayStart = getFiscalDayStartTimeGivenDate($fiscalDate);
        $fiscalDayLater = date('Y-m-d H:i:s', strtotime('+ 1 day - 1 second', strtotime($fiscalDateDayStart)));


        //return "FiscalDateStart: $fiscalDateDayStart<br>FiscalDateEnd: $fiscalDayLater";

        $andOnlyClause = $onlyWhereNotEntered ? " AND `tip_amount`='' " : "";
        /* Original doesn't include tablename
        $getCCsQuery = "SELECT * FROM `cc_transactions` ".
                       "WHERE `datetime` >= '[1]' AND `datetime` < '[2]' ".
                              "AND `pay_type`='Card' AND `server_id`='[3]' ".
                              "AND `processed`='0' $andOnlyClause".
                       "ORDER BY `id` ASC";
        */
        $getCCsQuery = "SELECT `cc_transactions`.*, `orders`.`tablename` as `table_name` ".
                       "FROM `cc_transactions` ".
                       "LEFT JOIN `orders` ".
                       "ON `orders`.`order_id`=`cc_transactions`.`order_id` ".
                       "WHERE `cc_transactions`.`datetime` >= '[1]' AND `cc_transactions`.`datetime` < '[2]' ".
                              "AND `cc_transactions`.`pay_type`='Card' AND `cc_transactions`.`server_id`='[3]' ".
                              "AND `cc_transactions`.`processed`='0' AND `orders`.`location_id`='[4]' ".
                              "AND `cc_transactions`.`loc_id`='[4]' $andOnlyClause".
                       "ORDER BY `id` ASC";

        /* LEFT JOIN EXAMPLE
        "SELECT DISTINCT(`server_id`) as `server_id`,`employee_class`,`f_name`,`l_name` ".
                          "FROM `clock_punches` ".
                          "LEFT JOIN `users` ".
                          "ON `clock_punches`.`server_id`=`users`.`id` ".
                          "WHERE `time` < '[1]' AND (`time_out` > '[2]' || `time_out`='') AND `location_id`='[3]' ".
                          "AND `server_id`<>'[4]' AND `clock_punches`.`_deleted`='0' ".
                          "ORDER BY `f_name` ASC, `l_name` ASC";
        */

        $ccResults = lavu_query($getCCsQuery, $fiscalDateDayStart, $fiscalDayLater, $serverID, $got_loc_id);
        if(!$ccResults){
            error_log("MYSQL Error in tips_wizard_data, -342x- error: " . lavu_dberror());
            return lavu_dberror();
            return false;
        }
        else if(mysqli_num_rows($ccResults)){

            $rows = array();
            while($currRow = mysqli_fetch_assoc($ccResults)){
                $rows[] = $currRow;
            }
            return $rows;
        }
        else{
            return array();
        }
    }

    function getLastTillInWithoutTillOutTime_falseIfEmpty(){
        $aWeekAgo = date('Y-m-d', strtotime('- 3 day', time()));
        $selectQuery = "SELECT * FROM `cash_data` ".
                       "WHERE `date` > '[1]' AND (`value3`='NULL' OR `value3`='') AND ".
                            "`server_id`='[2]' AND `setting` > 'till_in_out' AND `setting` < 'till_in_ouz'".//Other data is tacked on so we have to do it this way.
                       "ORDER BY `id` DESC LIMIT 1";
        $tillResult = lavu_query($selectQuery, $aWeekAgo, $_REQUEST['server_id']);
        if(empty($tillResult) || mysqli_num_rows($tillResult) == 0){
            return false;
        }
        return mysqli_fetch_assoc($tillResult);
    }

    function getTotalAmountFromTillInRow($tillInRow){

        //Bills are stored in value4 and coins are stored in value5. Definitions are in value 6;
        $tillInRow['value4'];
        $tillInRow['value5'];
        $tillInRow['value6'];

        //The following are 'how many bills and coins' were reported at till in.
        $billsAmountArray = $tillInRow['value4'];//From cash data
        $billsAmountArray = explode(':', $billsAmountArray);
        $billsAmountArray = $billsAmountArray[0];//Defined by left side of ...:...
        $billsAmountArray = explode('/', $billsAmountArray);

        $coinsAmountArray = $tillInRow['value5'];
        $coinsAmountArray = explode(':', $coinsAmountArray);
        $coinsAmountArray = $coinsAmountArray[0];
        $coinsAmountArray = explode('/', $coinsAmountArray);

        //The following is the definition of bill and coin denominations
        //How much each are worth.
        $denoms = $tillInRow['value6'];
        $denoms = explode(':', $denoms);

        $billDenoms = $denoms[0];
        $billDenoms = explode('/', $billDenoms);
        $coinDenoms = $denoms[1];
        $coinDenoms = explode('/', $coinDenoms);

        //Perform sanity check
        if( count($billDenoms) != count($billsAmountArray) ||
            count($coinDenoms) != count($coinsAmountArray) ){
            return false;
        }
        //Total bills:
        $billsCardinality = count($billDenoms);
        $totalAmountInBills = 0.0;
        for($i = 0; $i < $billsCardinality; $i++){
            $totalAmountInBills += $billDenoms[$i] * $billsAmountArray[$i];
        }

        $coinsCardinality = count($coinDenoms);
        $totalAmountInCoins = 0.0;
        for($i = 0; $i < $coinsCardinality; $i++){
            $totalAmountInCoins += $coinDenoms[$i] * $coinsAmountArray[$i];
        }

        return $totalAmountInBills + $totalAmountInCoins;
    }






    //--------------------------------
    //                                \
    //                          Getting clock_punch for server.
    function getClockPunchRowForCurrentServerNotClockedOutYet($serverID, $locationID){
        $selectQuery = "SELECT * FROM `clock_punches` ".
                       "WHERE `location_id`='[1]' AND `server_id`='[2]' ".
                       "AND `punched_out`='0' AND `_deleted`='0' ORDER BY `server_time` ".
                       "LIMIT 1";
        $result = lavu_query($selectQuery, $locationID, $serverID);
        if(!$result){
            return false;
        }
        else if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }else{
            return false;
        }
    }
    function getClockPunchRowForCurrentServerLastAlreadyClockedOut($serverID, $locationID, $leftLimit){
        $selectQuery = "SELECT * FROM `clock_punches` ".
                       "WHERE `location_id`='[1]' AND `server_id`='[2]' AND `time` >= '[3]' ".
                       "AND `punched_out`='1' AND `_deleted`='0' ORDER BY `time_out` DESC ".
                       "LIMIT 1";
        $result = lavu_query($selectQuery, $locationID, $serverID, $leftLimit);
        if(!$result){
            return false;
        }
        else if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
error_log("Row punch id: " . $row['id']);
            return $row;
        }else{
error_log("Returning false");
            return false;
        }
    }
    function getClockPunchRowSignedInOrLast($serverID, $locationID, $leftLimit){
        $signedInRow = getClockPunchRowForCurrentServerNotClockedOutYet($serverID, $locationID);
        if($signedInRow){
            return $signedInRow;
        }
        $lastSignedOutRow = getClockPunchRowForCurrentServerLastAlreadyClockedOut($serverID, $locationID, $leftLimit);
        return $lastSignedOutRow;
    }
    function getDateTimeFromDateTime($date, $timeDiff){
        $datesTime = strtotime($date);
        $newTime = strtotime($timeDiff,$datesTime);
        return date('Y-m-d H:i:s', $newTime);
    }
    //-----------------------------------------------------------------------------------------------------





    function getAllServerTransactionsForFiscalDate($serverID, $fiscalDate){
    	global $got_loc_id, $savedTransactions;
        //global $All_SERVER_CC_TRANSACTIONS;
        $fiscalDateDayStart = getFiscalDayStartTimeGivenDate($fiscalDate);
        $fiscalDayLater = date('Y-m-d H:i:s', strtotime('+ 1 day - 1 second', strtotime($fiscalDateDayStart)));

        $getCCsQuery = "SELECT * FROM `cc_transactions` ".
                       "WHERE `datetime` >= '[1]' AND `datetime` < '[2]' ".
                              "AND `server_id`='[3]' ".
                              "AND `processed`='0' ".
							  "AND `loc_id`='[4]' ".
                       "ORDER BY `id` ASC";
        $ccResults = lavu_query($getCCsQuery, $fiscalDateDayStart, $fiscalDayLater, $serverID, $got_loc_id);
        if(!$ccResults){
            error_log("MYSQL Error in tips_wizard_data, -3adwx- error: " . lavu_dberror());
            return false;
        }
        else if(mysqli_num_rows($ccResults)){
            $rows = array();
            while($currRow = mysqli_fetch_assoc($ccResults)){
                $rows[] = $currRow;
            }
            $savedTransactions = $rows;
            return $rows;
        }
        else{
            return array();
        }
    }


    function getTotalSalesForFiscalDate($serverID, $fiscalDate, $payTypeFilter = false){
        $allTransactions = getAllServerTransactionsForFiscalDate($serverID, $fiscalDate);
        $total_calc = 0.0;

        foreach($allTransactions as $currTransaction){
            if($currTransaction['order_id'] == 'Paid In' || $currTransaction['order_id'] == 'Paid Out'){
                continue;//Make sure we have a valid order_id.
            }
            if(!empty($payTypeFilter)){
                if($currTransaction['pay_type'] == $payTypeFilter){
                    $total_calc += $currTransaction['amount'] * 1;
                }
            }else{
                $total_calc += $currTransaction['amount'] * 1;
            }
        }
        return $total_calc;
    }


    function getCashDataRowById($id){
        $result = lavu_query("SELECT * FROM `cash_data` WHERE `id`='[1]'", $id);
        if($result && mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        return false;
    }




    function getOtherConcurrentWorkersForTipout($serverID, $serverClockIn, $timeClockedOutOrNow, $timeOverlapRequirementInMinutes, $locationID){
        $left_boundary  = date('Y-m-d H:i:s', strtotime("+ $timeOverlapRequirementInMinutes minutes", strtotime($serverClockIn)));
        $right_boundary = date('Y-m-d H:i:s', strtotime("- $timeOverlapRequirementInMinutes minutes", strtotime($timeClockedOutOrNow)));

        echo "Left Boundary: " . $left_boundary . "<br>";
        echo "Right Boundary: ". $right_boundary. "<br>";
        /*
        $getOthersQuery = "SELECT DISTINCT(`server_id`) as `server_id`,`employee_class`,`f_name`,`l_name` ".
                          "FROM `clock_punches` ".
                          "LEFT JOIN `users` ".
                          "ON `clock_punches`.`server_id`=`users`.`id` ".
                          "WHERE `time` < '[1]' AND (`time_out` > '[2]' || `time_out`='') AND `location_id`='[3]' ".
                          "AND `server_id`<>'[4]' AND `clock_punches`.`_deleted`='0' ".
                          "ORDER BY `f_name` ASC, `l_name` ASC";
        */
        $getOthersQuery = "SELECT DISTINCT(`server_id`) as `server_id`,`employee_class`,`f_name`,`l_name`, ".
                                    "`emp_classes`.`title` as `title`, `emp_classes`.`id` as `emp_class_id` ".
                          "FROM `clock_punches` ".
                          "LEFT JOIN `users` ".
                          "ON `clock_punches`.`server_id`=`users`.`id` ".
                          "LEFT JOIN `emp_classes` ".
                          "ON `users`.`role_id`=`emp_classes`.`id` ".
                          "WHERE `time` < '[1]' AND (`time_out` > '[2]' || `time_out`='') AND `location_id`='[3]' ".
                          "AND `server_id`<>'[4]' AND `clock_punches`.`_deleted`='0' ".
                          "ORDER BY `f_name` ASC, `l_name` ASC";
        $othersResult = lavu_query($getOthersQuery, $right_boundary, $left_boundary, $locationID, $serverID);
        $returnArray = array();
        if(!$othersResult){
            error_log('failed query in tips_wizard_data, -eef325- error: ' . lavu_dberror());
            return lavu_dberror();
        }
        else if(mysqli_num_rows($othersResult)){
            while($currRow = mysqli_fetch_assoc($othersResult)){
                $returnArray[] = $currRow;
            }
        }
        return $returnArray;
    }
    function getServerTipoutRules($serverID){
        $query = "SELECT `tipout_rules` FROM `users` ".
                 "LEFT JOIN `emp_classes` ".
                 "ON `users`.`role_id`=`emp_classes`.`id` ".
                 "WHERE `users`.`id`='[1]'";


        $result = lavu_query($query, $serverID);
        if(!$result){
            error_log("mysql error in tips_wizard_data.php -as8hf- mysql error: " . lavu_dberror());
            return "mysql error: " . lavu_dberror();
        }
        else if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            return $row['tipout_rules'];
        }
        else{
            return false;
        }
    }
    function decodeServerTipoutRulesArray($tipoutRulesStr){
        $rulesEncodedArr = explode(',', $tipoutRulesStr);
        $rulesDecodedArr = array();
        foreach($rulesEncodedArr as $currRuleEncoded){
            $currRuleNumeric = explode('|', $currRuleEncoded);
            $decodedArray = array();
            $decodedArray['percentage'] = $currRuleNumeric[1];
            $decodedArray['primary'] = $currRuleNumeric[2];
            $decodedArray['secondary'] = $currRuleNumeric[0];
            $rulesDecodedArr[] = $decodedArray;
        }
        return $rulesDecodedArr;
    }

    function getEmployeeClassIDToTitleMap(){
        $result = lavu_query("SELECT * FROM `emp_classes` where `_deleted`='0' OR `_deleted`=''");
        if(!$result){
            error_log("Mysql error in tips_wizard_data.php -ujf0hs- mysql error: " . lavu_dberror());
            return false;
        }
        $mapArr = array();
        while($currRow = mysqli_fetch_assoc($result)){
            $mapArr[$currRow['id']] = $currRow['title'];
        }
        return $mapArr;
    }

    function getNowAsLocalizedDateTime(){
        global $location_row;
        $timezone = $locationRow['timezone'];
        $localizedDateTimeNow = localize_datetime(date('Y-m-d H:i:s'), $timezone);
        return $localizedDateTimeNow;
    }
    function getTipoutDataToOthersMappingForServer($serverID, $serverClockPunchRow, $locationRow){
        
        if(!$serverClockPunchRow){
            return array();
        }
        /*
        //   `Other Worker`:
                `server_id`,`employee_class`,`f_name`,`l_name`,`emp_classes`.`title` as `title`, `emp_classes`.`id` as `emp_class_id`

        //  `Tipout Properties`:
                `percentage`, `primary`(id of the emp_class), `secondary`(id of secondary emp_class)
        */

        $workersOverlapInMinutes = getServerTipsTimeIntervalIntegerMinsForTipout();

        //Get other workers who overlap by specified time.
        //
        $timezone = $locationRow['timezone'];
        $localizedDateTimeNow = localize_datetime(date('Y-m-d H:i:s'), $timezone);
        $timeClockedOutOrNow = !empty($serverClockPunchRow['punched_out']) ? $serverClockPunchRow['time_out'] : $localizedDateTimeNow;
        //function getOtherConcurrentWorkersForTipout($serverID, $serverClockIn, $timeClockedOutOrNow, $timeOverlapRequirementInMinutes, $locationID)
        $otherWorkersWhoOverlapped = getOtherConcurrentWorkersForTipout($serverID, $serverClockPunchRow['time'], $timeClockedOutOrNow, 30, $locationRow['id']);
        //<>~------------------------------------------------------------------------------------------


        //Get the server tip out rules and decode them.
        //
        $serverTipoutRules = getServerTipoutRules($serverID);
        $serverTipoutRules = decodeServerTipoutRulesArray($serverTipoutRules);


        //Each 'OtherWorker' has `server_id`,`employee_class`,`f_name`,`l_name`,`emp_classes`.`title` as `title`, `emp_classes`.`id` as `emp_class_id`
        //<>~------------------------------------------------------------------------------------------

        //Given the id of an emp_class, maps to its title.
        $ruleID2TitleMap = getEmployeeClassIDToTitleMap();

        //Now for each 'tipout_rule' we construct an object that contains all the tip-out information:
        //  tipout_rule_enaction:
        //          1.) percentage
        //          2.) primary_emp_class_id
        //          3.) primary_emp_class_title
        //          4.) primary_emp_class_colleague_bin
        //          5.) secondary_emp_class_id
        //          6.) secondary_emp_class_title
        //          7.) secondary_emp_class_colleague_bin
        $tipoutRuleEnactions = array();
        foreach($serverTipoutRules as $currRule){
            $curr_tipout_rule_enaction = array();
            $curr_tipout_rule_enaction['primary_emp_class_id']   = $currRule['primary'];
            $curr_tipout_rule_enaction['secondary_emp_class_id'] = $currRule['secondary'];
            $curr_tipout_rule_enaction['primary_emp_class_title']   = $ruleID2TitleMap[$currRule['primary']];
            $curr_tipout_rule_enaction['secondary_emp_class_title'] = $ruleID2TitleMap[$currRule['secondary']];
            $curr_tipout_rule_enaction['percentage'] = $currRule['percentage'];
            //Now we add all 'other workers' to primary or secondary bins if they belong.
            $primaryEmpClassBin = array();
            $secondaryEmpClassBin = array();
            foreach($otherWorkersWhoOverlapped as $currOverlappedWorker){
                if($currRule['primary'] != '' && $currOverlappedWorker['emp_class_id'] == $currRule['primary']){
                    $primaryEmpClassBin[] = $currOverlappedWorker;
                    $curr_tipout_rule_enaction['primary_emp_class_colleague_bin'] = $primaryEmpClassBin;
                }
                else if($currRule['secondary'] != '' && $currOverlappedWorker['emp_class_id'] == $currRule['secondary']){
                    $secondaryEmpClassBin[] = $currOverlappedWorker;
                    $curr_tipout_rule_enaction['secondary_emp_class_colleague_bin'] = $secondaryEmpClassBin;
                }
            }
            $tipoutRuleEnactions[] = $curr_tipout_rule_enaction;
        }

        return $tipoutRuleEnactions;
    }

    function getNumberOfCoWorkersInTipoutBins($tipoutData){
        $totalCount = 0;
        foreach($tipoutData as $tipoutRuleEnaction){
            $primaryBin = $tipoutRuleEnaction['primary_emp_class_colleague_bin'];
            $secondaryBin = $tipoutRuleEnaction['secondary_emp_class_colleague_bin'];
            $totalCount += count($primaryBin) + count($secondaryBin);
        }
        return $totalCount;
    }

    function getServerTipsTimeIntervalIntegerMinsForTipout(){
        $result = lavu_query("SELECT `value` FROM `config` WHERE `setting`='server_tips_time_interval_for_tipout'");
        if(!$result){
            error_log("mysql error in tips_wizard_data.php mysql error:" . lavu_dberror());
            return false;
        }
        else if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            $timeIntervalValue = trim($row['value']);
            $timeIntervalValue = intval($timeIntervalValue);
            if( !($timeIntervalValue > 0) ){
                error_log('Function getServerTipsTimeIntervalForTipout in tips_wizard_data.php is pulling server_tips_time_interval_for_tipout from config where its NOT an integer');
                return 30;
            }
            return $timeIntervalValue;
        }
        else{
            //Not set in advanced location settings, return default: 30min.
            return 30;
        }
    }


    //Config queries
    //Total defaults to 'cash_only' other option is 'total_including_cc'
    function getWhetherToDisplayInputForTotalCCAndCashOrJustCashTips(){
        $result = lavu_query("SELECT * FROM `config` WHERE `setting`='management_server_tips_total_or_cash_tips'");
        if(!$result){
            error_log("mysql error in 'tips_wizard_data.php' -au4fhkcf- lavu_dberror: " . lavu_dberror());
            return 'cash_only';
        }
        else if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            return $row['value'];
        }
        else{
            return 'cash_only';
        }
    }
















    //Updates
    function updateCCTransactionTipAmountForCC_TransIDWithServer($transaction_id, $server_id,$tip_amount){
        $updateQuery = "UPDATE `cc_transactions` SET `tip_amount`='[1]' " .
                       "WHERE `id`='[2]' AND `server_id`='[3]'";
        $result = lavu_query($updateQuery, $tip_amount, $transaction_id, $server_id);
        if(!$result){
            return false;
        }
        return ConnectionHub::getConn('rest')->affectedRows();
    }

    function getDenominations_JSArrays(){
        $denoms = get_db_denominations();
        $bills = $denoms['bills'];
        $coins = $denoms['coins'];
        $bills;
        $coins;
        foreach($bills as $key => $val){
            $bills[$key] = str_replace("\"", "\\\"", $val);
        }
        foreach($coins as $key => $val){
            $coins[$key] = str_replace("\"", "\\\"", $val);
        }
        return array('bills' => '["'.implode('","',$bills).'"]', 'coins' => '["'.implode('","',$coins).'"]');
    }

    function submitServerCashReconciliation($tillOutTotal, $tillOutBills, $tillOutCoins, $cashDataID){
        global $location_row;
        //Parts for till out:
        //    Value 4 contains bill reconciliations the '/' delimited to the left of ':'
        //        are the amounts given to the server, the right are tilled out by server.
        //    Value 5 are the coins given to the server : coins given back by server.
        //    Value 3 is the till out time(without date apparently).
        //    Value is of the format: amountGivenToServer:amountInDrawer
        // cash_data fields to update, by indices.
        // 'posted'      till_out_total. Contains the till out total with the correct decimal length.
        //                    ->goes to 2nd half of `value`.
        // 'dynamic'     localized check_out_time in form: 'H:i:s'
        // 'posted'      till_out_bills.  '/' delimited list of how many bills are checking in.
        //                    ->goes to 2nd half of `value4`.
        // 'posted'      till_out_coins.  '/' delimited list of how many coins are checking in.
        // 'posted'      cashDataID.  The id of the cash_data row that we will update.
        // 'posted'      number_of_decimal_places
        //We begin by calculating the check_out_time.
        $timezone = $location_row['timezone'];
        //localize_datetime($timestamp, $to_tz);
        $nowDateTime = date('Y-m-d H:i:s');
        $localizedDateTime = localize_datetime($nowDateTime, $timezone);
        $localizedTime = substr($localizedDateTime, -8);

        //echo "Cash Data ID: " . $cashDataID;
        $cashDataRow = getCashDataRowById($cashDataID);

        //Now we update the values inside of variables that will be updated
        // value
        $value = $cashDataRow['value'];
        $valueParts = explode(':', $value);
        $valueParts[1] = $tillOutTotal;
        $value = implode(':', $valueParts);

        //value3
        $value3 = $localizedTime;

        //value4 (recon: billsReceived:billsPutBack)
        $value4 = $cashDataRow['value4'];
        $value4Parts = explode(':', $value4);
        $value4Parts[1] = str_replace(',', '/', $tillOutBills);
        $value4 = implode(':', $value4Parts);

        //value5 (recon: coinsReceived:coinsPutBack)
        $value5 = $cashDataRow['value5'];
        $value5Parts = explode(':', $value5);
        $value5Parts[1] = str_replace(',', '/', $tillOutCoins);
        $value5 = implode(':', $value5Parts);

        /*TODO whether or not to check for error, parts changed should be 'NULL' */


        //Finally, we just need to do the update.
        $updateStr = "UPDATE `cash_data` ".
                     "SET `value`='[1]',`value3`='[2]',`value4`='[3]',`value5`='[4]' ".
                     "WHERE `id`='[5]'";
        $result = lavu_query($updateStr, $value, $value3, $value4, $value5, $cashDataID);

        $updateStatus = ConnectionHub::getConn('rest')->affectedRows() ? "success" : "not_inserted";

        if(!$result){
            error_log("MYSQL error in tips_wizard_data - firtwu09ah - mysql error: " . lavu_dberror());
            return "failed";
        }
        return "success";
    }

    function insertServerTipsCashData($serverTipsCashData, $timezone, $ajaxPostArr){
        $insertArray = array();

        $currDateTime = date('Y-m-d H:i:s');
        $insertArray['setting'] = 'server_tips_submission';
        $insertArray['server_time'] = $currDateTime;
        $insertArray['device_time'] = localize_datetime($currDateTime, $timezone);
        $insertArray['server_id'] = $serverTipsCashData['server_id'];
        $insertArray['loc_id'] = $serverTipsCashData['location_id'];
        $insertArray['value'] = $serverTipsCashData['cash_tips'];
        $insertArray['value2'] = $serverTipsCashData['total_tips'];
        $insertArray['value3'] = $serverTipsCashData['over_short'];
        $insertArray['value4'] = $serverTipsCashData['cc_transaction_ids_comma_delim_str'] .
                ':' . $serverTipsCashData['till_out_id_lt1_4_not'];
        $insertArray['value_med'] = $serverTipsCashData['full_post'];
        //We leave value6 in case we forgot something later.

        //INSERTED by Brian D.
        $insertArray['type'] = 'server_cash_tips';
        $insertArray['date'] = getFiscalDayStartGivenDatetime($insertArray['device_time']);
        $insertArray['date'] = substr($insertArray['date'], 0, 10);
        $insertColumnsArr = array_keys($insertArray);

        $insertColumnsStr = "`"  . implode("`,`", $insertColumnsArr)   . '`';
        $insertValuesStr  = "'[" . implode("]','[", $insertColumnsArr) . "]'";


        $query = "INSERT INTO `cash_data` (" . $insertColumnsStr . ") VALUES (" . $insertValuesStr . ")";
        $result = lavu_query($query, $insertArray);
        if(!$result){
            error_log("Error in ajax submit call from 'server tips', in tips_wizard_totals_page.php -342sjgrd4- MYSQL error: " . lavu_dberror());
            return "failed";
        }
        else{
            $insertID = ConnectionHub::getConn('rest')->insertID();
            createOrdersAndCCTransactionsRowForCashToShowOnReports($ajaxPostArr);

            return $insertID;
        }
    }
    function organizeAndInsertServerTipsCashData($ajaxPostArr){

        global $location_row;
        $timezone = $location_row['timezone'];
        $receipt_data = json_decode($ajaxPostArr['receipt_data']);

        if(0){
            print_r($ajaxPostArr);
            exit;
        }
        if(0){
            foreach($receipt_data as $currReceipt){
                echo "---------------------<br>\n";
                print_r($currReceipt);
            }
            exit;
        }

        $cc_ids_card_tip_entry = array();
        foreach($receipt_data as $currReceipt){
            $cc_ids_4_card_tip_entry[] = $currReceipt['transaction_id'];
        }

        $serverTipsCashData = array();
        $serverTipsCashData['server_date_time_received'] = date('Y-m-d H:i:s');
        $serverTipsCashDate['server_date_time_tips_wizard_started'] = $ajaxPostArr['server_date_time_tips_wizard_started'];
        $serverTipsCashData['localized_date_time_tips_wizard_started'] = $ajaxPostArr['localized_date_time_tips_wizard_started'];
        $serverTipsCashData['server_id']   = $ajaxPostArr['server_id'];
        $serverTipsCashData['location_id'] = $ajaxPostArr['location_id'];
        $serverTipsCashData['cash_tips']   = $ajaxPostArr['cash_tips'];
        $serverTipsCashData['total_tips']  = $ajaxPostArr['total_tips'];
        $serverTipsCashData['over_short']  = $ajaxPostArr['over_short'];
        $serverTipsCashData['cc_transaction_ids_comma_delim_str'] = count($cc_ids_4_card_tip_entry) ? implode(',', $cc_ids_4_card_tip_entry) : '';
        $serverTipsCashData['till_out_id_lt1_4_not'] = $ajaxPostArr['cash_data_id'];
        $serverTipsCashData['calculated_expression_used_server_turn_in'] = $ajaxPostArr['calculated_expression_used_server_turn_in'];
        $serverTipsCashData['calculated_value_server_turn_in'] = $ajaxPostArr['calculated_value_server_turn_in'];
        $serverTipsCashData['calculated_expression_all_parts'] = $ajaxPostArr['calculated_expression_all_parts'];
        //$serverTipsCashData['server_deposite_to_restaurant_amount'] = $ajaxPostArr['server_deposite_to_restaurant_amount'];//Redundant
        $serverTipsCashData['server_deposite_take_title'] = $ajaxPostArr['server_deposite_take_title'];
        $serverTipsCashData['calculated_value_param_args'] = $ajaxPostArr['calculated_value_param_args'];
        $serverTipsCashData['full_post'] = json_encode($ajaxPostArr);
        return insertServerTipsCashData($serverTipsCashData, $timezone, $ajaxPostArr);
    }
// End Query Functions



// Misc. Processing Functions
    function transformQueryResultsForJSONDelivery($rows){
        $ticketArrays = array();
        foreach ($rows as $row){
            $currJSONTicket = array();
            $currJSONTicket['transaction_id'] = $row['id'];
            $currJSONTicket['card_type']      = $row['card_type'];
            $currJSONTicket['card_desc']      = $row['card_desc'];
            $currJSONTicket['ref_number']     = $row['transaction_id'];
            $currJSONTicket['auth_code']      = $row['auth_code'];
            $currJSONTicket['date_time']      = $row['datetime'];
            $currJSONTicket['server_name']    = $row['server_name'];
            $currJSONTicket['server_id']      = $row['server_id'];
            $currJSONTicket['amount']         = $row['amount'];
            $currJSONTicket['order_id']       = $row['order_id'];
            $currJSONTicket['tip_amount']     = $row['tip_amount'];
            $currJSONTicket['table_name']     = $row['table_name'];
            $ticketArrays[] = $currJSONTicket;
        }
        $cc_transactions_JSONEncoded = json_encode($ticketArrays);
        return $cc_transactions_JSONEncoded;
    }



































    //INSERTED BY Brian D.
    //---------------------------------------------------   EVERYTHING BELOW THIS POINT IS FOR INSERTING CC_TRANS AND ORDERS ROW SO TIPS ARRIVE IN REPORTS.
    //
    //CC_TRANSACTIONS:
    //
    //populate the following fields:
    //tip_amount
    //voided = 5
    //order_id
    //check = 1
    //ioid
    //internal_id
    //datetime
    //server_time
    //pay_type = Cash
    //pay_type_id = 1
    //server_id
    //server_name
    //more_info = 'Lump Sum Cash Tip Collection'
    //loc_id
    //
    //ORDERS:
    //
    //location
    //all neccessary fields
    //void = 4
    //opened = closed = now()
    //server = cashier
    //checked_out = 1
    function createOrdersAndCCTransactionsRowForCashToShowOnReports($serverTipsSubmissionArr){
        ////error_log('function start');
        //assignNextWithPrefix($field, $tablename, $where_field, $where_value, $prefix)
        $cashSubmitOrderPrefix = getPrependForOrderID(); //TipsCashSubmission
        $nextCashTipsOrderID = assignNextWithPrefix('order_id', 'orders', 'location_id', $serverTipsSubmissionArr['location_id'], $cashSubmitOrderPrefix);
        $fiscalDayNext = date('Y-m-d H:i:s', strtotime('+1 day', strtotime($serverTipsSubmissionArr['fiscal_day_start'])));



        //TODO IF NEXT ORDER_ID ISN"T SET, THEN RETURN.

        $location_row = getLocationRowForID($serverTipsSubmissionArr['location_id']);

        // First we see if it already exists for this fiscal day.  If it does then we have the order_id,
        // else we need to pull the next order_id from core_functions':
        //        getNextWithPrefix($field=order_id,$tablename='Orders',$wherefield='??',$whereValue='??','tcs')
        ////error_log('marker0');
        $orderRowCashSubmissionExists4TodayQuery = "SELECT `id`,`order_id` FROM `orders` ".
                                                   "WHERE `order_id` >= '[1]' AND `order_id` <= '[2]' ".
                                                   "AND `opened`='[3]' ".
                                                   "AND `server_id`='[4]'";

        $result = lavu_query($orderRowCashSubmissionExists4TodayQuery,
                                $cashSubmitOrderPrefix, $cashSubmitOrderPrefix . 'z',
                                $serverTipsSubmissionArr['fiscal_day_start'],
                                $serverTipsSubmissionArr['server_id']);

        ////error_log('marker1: result:' . $result);
        if(!$result){error_log("Query error in ".__FILE__." -asefa4s- error: " . lavu_dberror());}
        $currOrderRow = false;
        $currCCTransactionRow = false;
        $orderRowCount = mysqli_num_rows($result);
        if($orderRowCount){
            $currOrderRow = mysqli_fetch_assoc($result);
            $nextCashTipsOrderID = $currOrderRow['order_id'];
    error_log("\n\n\ncurrOrderRow: ".print_r($currOrderRow,1));
            $currCCTransRow = getAccompanyingCCTransactionRowToOrderRow($currOrderRow['order_id']);
        }
        $serversRow = getServersUserRowForID($serverTipsSubmissionArr['server_id']);


        $ordersTableValues = _fillUpdateOrInsertValuesForOrdersArr($serverTipsSubmissionArr, $serversRow, $nextCashTipsOrderID, $location_row['title']);
        $ccTransValues = _fillUpdateOrInsertValuesForCC_TransArr($serverTipsSubmissionArr, $serversRow, $nextCashTipsOrderID);
        if($orderRowCount){
            error_log('UPDATING orders table... m1');
            $result = updateColumnsOfTable($currOrderRow['id'], $ordersTableValues, 'orders');
            if($currCCTransRow){
                error_log('updating cc_transactions... m2');
                $result = updateColumnsOfTable($currCCTransRow['id'], $ccTransValues, 'cc_transactions');
            }
            else{
                error_log('inserting into cc_transactions... m3');
                $result = insertColumnsIntoTable($ccTransValues, 'cc_transactions', 'sql error in '.__FILE__.' -aefaje43- error:' . lavu_dberror() );
            }
        }else{
            error_log('Inserting into orders table... m4');
            $result = insertColumnsIntoTable($ordersTableValues, 'orders',  'sql error in '.__FILE__.' -aefaje43- error:' . lavu_dberror() );
            if(!$result){
                error_log("mysql error -r77tsdrfa- in ".__FILE__." error:".lavu_dberror());
            }
            error_log('INSERTING cc_transactions... m5');
            $result = insertColumnsIntoTable($ccTransValues, 'cc_transactions',  'sql error in '.__FILE__.' -aefaje43- error:' . lavu_dberror() );
            if(!$result){
                error_log("mysql error -aslkfaose- in ".__FILE__." error:".lavu_dberror());
            }
        }
    }

    function getAccompanyingCCTransactionRowToOrderRow($order_id){
        $result = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `order_id`='[1]'",$order_id);
        if(!$result){
            error_log("mysql in ".__FILE__." -54uyg- error: ".lavu_dberror());
            return false;
        }
        if(mysqli_num_rows($result) == 0){
            return 0;
        }
        return mysqli_fetch_assoc($result);
    }

    function updateColumnsOfTable($rowsID, $columnsArr, $table){
        $keys = array_keys($columnsArr);
        $queryBuilder = '';
        foreach($columnsArr as $key => $val){
            $queryBuilder .= "`$key`='[$key]',";
        }
        $queryBuilder = substr($queryBuilder, 0, -1);
        $columnsArr['id'] = $rowsID;
        $table = ConnectionHub::getConn('rest')->escapeString($table);
        $fullQuery = "UPDATE `$table` SET $queryBuilder WHERE `id`='[id]'";
        $result = lavu_query("UPDATE `$table` SET $queryBuilder WHERE `id`='[id]'", $columnsArr);
        if(!$result){
            error_log("mysql error in ".__FILE__." error:".lavu_dberror());
            return false;
        }
        return ConnectionHub::getConn('rest')->insertID();
    }
    function insertColumnsIntoTable($columnsArr, $table, $errorStr){
        $keys = array_keys($columnsArr);
        $columnsPart =    "(`".   implode("`,`",$keys).     "`)";
        $valuesPart  =    "('[".  implode("]','[",$keys).   "]')";
        $table = ConnectionHub::getConn('rest')->escapeString($table);
        $queryStr = "INSERT INTO `$table` $columnsPart VALUES $valuesPart";
        $result = lavu_query($queryStr, $columnsArr);
        if(!$result){
            error_log($errorStr . "\n\nQuery:".$queryStr."\n\n");
        }
        return ConnectionHub::getConn('rest')->insertID();
    }


    function _fillUpdateOrInsertValuesForOrdersArr($serverTipsSubmissionArr, $serversRow, $orderID, $locRowTitle){
        $columnVals = array('location' => $locRowTitle,
                            'order_id' => $orderID,
                            'location_id' => $serverTipsSubmissionArr['location_id'],
                            'void' => '4',
                            'opened' => $serverTipsSubmissionArr['fiscal_day_start'],
                            'closed' => $serverTipsSubmissionArr['fiscal_day_start'],
                            'server' => $serversRow['f_name'],
                            'server_id' => $serversRow['id'],
                            'checked_out' => '1',
                            'cash_tip' => $serverTipsSubmissionArr['cash_tips']
                            );
        return $columnVals;
    }
    function _fillUpdateOrInsertValuesForCC_TransArr($serverTipsSubmissionArr, $serversRow, $orderID){
        global $cc_companyid, $data_name;
        
        //error_log("Got company id from info.inc: $cc_companyid");
        //error_log("Got company id for dataname:[$data_name] from func: " . getCompanyID($data_name));
        $cashPaymentTypeID = getPaymentTypeIDForCash();
        $columnVals = array('tip_amount' => $serverTipsSubmissionArr['cash_tips'],
                            'voided' => 0,
                            'order_id'=> $orderID,
                            'check' => 1,
                            //'ioid' =>          //May need this later
                            //'internal_id' =>   //May need this later
                            'datetime' => getNowAsLocalizedDateTime(),
                            'server_time' => date('Y-m-d H:i:s'),
                            'pay_type' => 'Cash',
                            'pay_type_id' => $cashPaymentTypeID,
                            'server_id' => $serverTipsSubmissionArr['server_id'],
                            'server_name' => $serversRow['f_name'],
                            'more_info' => 'Submitted via tips wizard',
                            'loc_id' => $serverTipsSubmissionArr['location_id']
                            );
        return $columnVals;
    }

    function getLocationRowForID($locID){
        $result = lavu_query("SELECT * FROM `locations` WHERE `id`='[1]'", $locID);
        if(!$result){ error_log("mysql error in ".__FILE__." -dd6fj- error: " . lavu_dberror()); }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }else{
            return false;
        }
    }


    function getServersUserRowForID($server_id){
        $result = lavu_query("SELECT * FROM `users` WHERE `id`='[1]'", $server_id);
        if(!$result){ error_log("mysql error in ".__FILE__." error: " . lavu_dberror()); }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }else{
            return false;
        }
    }

    function getPaymentTypeIDForCash(){
        $result = lavu_query("SELECT `id` FROM `payment_types` WHERE `type`='Cash'");
        if(!$result){
            error_log("Error in ".__FILE__." -aslk375s- error:" . lavu_dberror());
        }
        if(!mysqli_num_rows($result)){
            error_log("Error finding 'Cash' type in payment types in file: ".__FILE__);
        }
        $row = mysqli_fetch_assoc($result);
        return $row['id'];
    }

    function getPrependForOrderID(){
        $isLLS = strpos(__FILE__, '/poslavu-local/') !== false;
        if($isLLS){
            $installCount = getInstallCountIfLLS();
            if(!$installCount) $installCount = 0;
            $prependStr = 'cts8'.translateInstallCountIntoSingleChar($installCount).'-';
        }else{
            $prependStr = 'cts1-';
        }
        return $prependStr;
    }

    function getInstallCountIfLLS(){
        if(!file_exists('/poslavu-local/local/settings.txt')){
            return false;
        }
        $fp = fopen('/poslavu-local/local/settings.txt', 'r');
        $content = fread($fp, filesize('/poslavu-local/local/settings.txt'));
        $contentLines = explode("\n", $content);
        foreach($contentLines as $currLine){
            $lineParts = explode('=', $currLine);
            $key = trim($lineParts[0]);
            $val = trim($lineParts[1]);
            if($key == 'install_count'){
                $val = intval(trim($val));
                if(is_numeric($val)){
                    return $val;
                }else{
                    return false;
                }
            }
        }
        return false;
    }
    function translateInstallCountIntoSingleChar($installCount){
        $base = intval($installCount) % 64;
        if($base < 10)  return chr( ord('0')+($base) );
        if($base < 36)  return chr( ord('a')+($base-10) );
        if($base < 62)  return chr( ord('A')+($base-36) );
        if($base == 62) return "^";
        if($base == 63) return '~';
    }

// End of Misc. processing functions



    //  PRINTING REPORTS
    /*
        Original line to look at from shifts line 133.
        $temp_code .= "<td><input type='button' value='Print ".$report_title."' onclick='window.location = \"_DO:cmd=print&filename=till_report.php&m=".($shift_read['type']=="house"?"x_report":"register")."&dn=$cc&loc_id=$loc_id&register=$register&vars=shift_info=type(eq)".$shift_read['type']."(and)shift_number(eq)".$shift_count."(and)shift_start(eq)".urlencode($next_shift_start)."(and)shift_end(eq)".urlencode($shift_read['datetime'])."\"' /></td>";

        onclick='window.location =
        \"_DO:cmd=print
            &filename=till_report.php
            &m=".($shift_read['type']=="house"?"x_report":"register")."
            &dn=$cc&loc_id=$loc_id
            &register=$register
            &vars=shift_info=type(eq)".$shift_read['type']."(and)shift_number(eq)"$shift_count."(and)shift_start(eq)".urlencode($next_shift_start)."(and)shift_end(eq)".urlencode($shift_read['datetime'])."\"'
    */







?>
