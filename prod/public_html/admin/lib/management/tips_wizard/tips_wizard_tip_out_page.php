<script>

/*
        Tip out object
//          1.) percentage
//          2.) primary_emp_class_id
//          3.) primary_emp_class_title
//          4.) primary_emp_class_colleague_bin
//          5.) secondary_emp_class_id
//          6.) secondary_emp_class_title
//          7.) secondary_emp_class_colleague_bin

        Colleage Object
            `server_id`,
            `employee_class`, Strangely is just empty, use title instead.
            `f_name`,`l_name`,
            `emp_classes`.
            `title` as `title`, 
            `emp_classes`.`id` as `emp_class_id`
*/



    function generateTipOutDisplayComponent(componentRef, builderObj){
        for(var key in builderObj){
            componentRef[key] = builderObj[key];
        }
        
        componentRef.style.width = componentRef.width;
        componentRef.style.maxHeight = componentRef.height;
        //componentRef.style.border = "1px solid #eee";
        componentRef.style.borderTop = '1px solid #ccc';
        componentRef.style.borderBottom = '1px solid #ccc';
        componentRef.style.overflowY = 'scroll';
        componentRef.style.overflowX = 'hidden';
        componentRef.display = _displayTipOutComponent;
        
        
        componentRef.textAlign = 'left';
    }


    
    var tipoutDisplayTable = false;
    function _displayTipOutComponent(totalTipsAmount){
    
        
    //alert( JSON.stringify(this.tipoutMappings));
        
    
        if(tipoutDisplayTable){
            this.removeChild(tipoutDisplayTable);
        }
        if(totalTipsAmount == ''){
            totalTipsAmount = 0;
        }
        var componentRef = this;
        var childrenNodeArray = this.children;
        var childrenCount = childrenNodeArray.length;
        for(var i = 0; i < childrenCount; i++){
            this.remove(childrenNodeArray[0]);
        }
        
        var tableFullWidth = '350px';//Will auto adjust for scroll buttons
        var col_1_width = 50;
        var col_2_width = 25;
        var col_3_width = 25;
        
        tipoutDisplayTable = document.createElement('table');
        tipoutDisplayTable.id = 'tipout_display_table_id';
        tipoutDisplayTable.style.position = 'relative';
        tipoutDisplayTable.align = 'left';
        tipoutDisplayTable.style.left = 0;
        tipoutDisplayTable.style.width = tableFullWidth + 'px';;
        tipoutDisplayTable.style.maxWidth = tableFullWidth;
        tipoutDisplayTable.border = 0;
        
        var offsetTopArray = [];
        var tipoutMappingCount = componentRef.tipoutMappings.length;
        var currTipoutNumber = 0;
        for(var i = 0; i < componentRef.tipoutMappings.length; i++){
            
            currTipoutNumber = parseInt(currTipoutNumber) + 1;
            
            //Glb. Vars..
            var currPercentage = componentRef.tipoutMappings[i]['percentage'];
            var primaryEmpClass = componentRef.tipoutMappings[i]['primary_emp_class_title'];
            var secondaryEmpClass = componentRef.tipoutMappings[i]['secondary_emp_class_title'];
            var primaryEmpClass_colleages_bin = componentRef.tipoutMappings[i]['primary_emp_class_colleague_bin'];
            var secondaryEmpClass_colleages_bin = componentRef.tipoutMappings[i]['secondary_emp_class_colleague_bin'];
            
            
            
            //Create and append TIPOUT TITLE ROW.
            var tipoutTitleRow = document.createElement('tr');
            var currTipoutTitleRowTD = document.createElement('td');
            //currTipoutTitleRowTD.style.textDecoration = 'underline';
            tipoutTitleRow.appendChild(currTipoutTitleRowTD);
            //currTipoutTitleRowTD.colSpan = 3;
            currTipoutTitleRowTD.colSpan = 2;
            tipoutTitleRow.isTitleRow = true;
            var tipoutRuleTitleEnd = primaryEmpClass;
            if(secondaryEmpClass && secondaryEmpClass != ''){
                tipoutRuleTitleEnd += ' & ' + secondaryEmpClass;
            }
            //tipoutRuleTitle = "Tipout " + currTipoutNumber + " of " + tipoutMappingCount;
            tipoutRuleTitle =   currTipoutNumber + "/" + tipoutMappingCount  + ":";
            tipoutRuleTitle += '&nbsp;&nbsp;&nbsp;' + currPercentage + '%' + ' to ' + tipoutRuleTitleEnd;
            //currTipoutTitleRowTD.style.maxWidth = '10px';
            currTipoutTitleRowTD.innerHTML = "<div style='text-decoration:underline;white-space:nowrap;max-width:265px;overflow:hidden;text-overflow:ellipsis;'>" + tipoutRuleTitle + "</div";
            
            
            var subtitleAmount = ((currPercentage/100) * totalTipsAmount).toFixed(this.decimalPlaceCount);
            
            var totalRuleAmountTD = document.createElement('td');
            totalRuleAmountTD.innerHTML = '$' + subtitleAmount;
            totalRuleAmountTD.style.textAlign = 'right';
            totalRuleAmountTD.style.paddingRight = '10px';
            totalRuleAmountTD.style.minWidth = '40px';
            tipoutTitleRow.appendChild(totalRuleAmountTD);
            tipoutDisplayTable.appendChild(tipoutTitleRow);
    
    
            var colleagesInPrimaryBinCount = primaryEmpClass_colleages_bin.length;
            var colleagesInSecondaryBinCount = secondaryEmpClass_colleages_bin ? secondaryEmpClass_colleages_bin.length : 0;
            var totalColleagesForTipouts = colleagesInPrimaryBinCount + colleagesInSecondaryBinCount;
            
            var toEachColleageAmount = subtitleAmount/totalColleagesForTipouts;
            toEachColleageAmount = "$" + toEachColleageAmount.toFixed(this.decimalPlaceCount);

            //Create a row for every colleage who belongs in primary bin.
            for(var k = 0; k < colleagesInPrimaryBinCount; k++){
                var currTipOutColleage = primaryEmpClass_colleages_bin[k];
                var colleageRow = document.createElement('tr');
                var colleageTD_1 = document.createElement('td');
                colleageTD_1.className = 'receiptInfoLabel';
                colleageTD_1.style.fontWeight = 'normal';
                colleageTD_1.style.width = col_2_width + '%';
                colleageTD_1.style.overflowX = 'scroll';
                colleageTD_1.innerHTML = primaryEmpClass + ':';
                colleageTD_1.style.fontSize = '12px';
                colleageTD_1.style.textAlign = 'right';
                colleageRow.appendChild(colleageTD_1);
                var colleageTD_2 = document.createElement('td');
                colleageTD_2.className = 'receiptInfoValue';
                colleageTD_2.style.fontWeight = 'normal';
                colleageTD_2.style.width = col_1_width + '%';////
                colleageTD_2.style.overflowX = 'scroll';
                colleageTD_2.innerHTML = currTipOutColleage['f_name'] + ' ' + currTipOutColleage['l_name'];
                colleageTD_2.style.fontSize = '12px';
                colleageTD_2.style.textAlign = 'left';
                colleageRow.appendChild(colleageTD_2);
                var colleageTD_3 = document.createElement('td');
                colleageTD_3.style.paddingRight = '10px';
                colleageTD_3.style.width = col_3_width + '%';
                colleageTD_3.style.textAlign = 'right';
                colleageTD_3.style.overflowX = 'scroll';
                colleageTD_3.innerHTML = toEachColleageAmount;
                colleageTD_3.style.fontSize = '12px';
                colleageRow.appendChild(colleageTD_3);
                tipoutDisplayTable.appendChild(colleageRow);  
            }
            //Create a row for every colleage who belongs in secondary bin.
            for(var k = 0; k < colleagesInSecondaryBinCount; k++){
                var currTipOutColleage = secondaryEmpClass_colleages_bin[k];
                var colleageRow = document.createElement('tr');
                var colleageTD_1 = document.createElement('td');
                colleageTD_1.className = 'receiptInfoLabel';
                colleageTD_1.style.fontWeight = 'normal';
                colleageTD_1.style.width = col_2_width + '%';
                colleageTD_1.style.overflowX = 'scroll';
                colleageTD_1.innerHTML = secondaryEmpClass + ':';
                colleageTD_1.style.fontSize = '12px';
                colleageTD_1.style.textAlign = 'right';
                colleageRow.appendChild(colleageTD_1);
                var colleageTD_2 = document.createElement('td');
                colleageTD_2.className = 'receiptInfoValue';
                colleageTD_2.style.fontWeight = 'normal';
                colleageTD_2.style.width = col_1_width + '%';
                colleageTD_2.style.overflowX = 'scroll';
                colleageTD_2.innerHTML = currTipOutColleage['f_name'] + ' ' + currTipOutColleage['l_name'];
                colleageTD_2.style.fontSize = '12px';
                colleageTD_2.style.textAlign = 'left';
                colleageRow.appendChild(colleageTD_2);
                var colleageTD_3 = document.createElement('td');
                colleageTD_3.style.paddingRight = '10px';
                colleageTD_3.style.width = col_3_width + '%';
                colleageTD_3.style.textAlign = 'right';
                colleageTD_3.style.overflowX = 'scroll';
                colleageTD_3.innerHTML = toEachColleageAmount;
                colleageTD_3.style.fontSize = '12px';
                colleageRow.appendChild(colleageTD_3);
                tipoutDisplayTable.appendChild(colleageRow);  
            }

        }

        componentRef.appendChild(tipoutDisplayTable);
                    //addUpDownScrollButtonsToDiv(div, upCallback, downCallback, classNameUnPresssed, classNamePressed, width)
        
        //Now we make an array of all the title rows.
        var titleRowArray = [];
        for(var i = 0; i < tipoutDisplayTable.rows.length; i++){
            var currRow = tipoutDisplayTable.rows[i];
            if(currRow.isTitleRow && currRow.offsetTop + componentRef.offsetHeight < tipoutDisplayTable.offsetHeight){
                titleRowArray.push(currRow.offsetTop);
            }
        }
        
        var currTitleIndex = 0;
        var upPressedFunction = function(){
            currTitleIndex = Math.max(0, currTitleIndex - 1);
            componentRef.scrollTop = Math.max(0, titleRowArray[currTitleIndex]);
        };
        var downPressedFunction = function(){
            if(currTitleIndex == titleRowArray.length - 1){
                componentRef.scrollTop = tipoutDisplayTable.offsetHeight - componentRef.offsetHeight;
            }else{
                currTitleIndex = Math.min(titleRowArray.length - 1, currTitleIndex + 1);
                componentRef.scrollTop = Math.min( tipoutDisplayTable.offsetHeight - componentRef.offsetHeight, titleRowArray[currTitleIndex]);
            }
        };
    
        //Now we add the scrolling buttons if the table is longer than the container.
        if(tipoutDisplayTable.offsetHeight > componentRef.height){
            var buttonsWidth = 25;
            tipoutDisplayTable.style.width = (parseInt(tableFullWidth) - parseInt(buttonsWidth)) + 'px';
            addUpDownScrollButtonsToDiv(componentRef, upPressedFunction, downPressedFunction, 'scrollButton', 'scrollButtonPressed', buttonsWidth);
        }
            
    }
</script>