<?php
    
?>

<style>
    .touchButtonStyle{
        position:absolute;
        border:1px solid #222;
        border-radius:5px;
        transition: background-color ease-out 0.25s;
        -webkit-transition: background-color ease-out 0.25s;
        -webkit-tap-highlight-color:rgba(0,0,0,0.0);
        background-color:#AECD37
    }
    .touchButtonStyleDisabled{
        position:absolute;
        border:1px solid #888;
        border-radius:5px;
        transition: background-color ease-out 0.25s;
        -webkit-transition: background-color ease-out 0.25s;
        -webkit-tap-highlight-color:rgba(0,0,0,0.0);
        background-color:rgba(88,88,88,0.15);
        color:#bbb;
    }
    .touchButtonPressedStyle{
        position:absolute;
        border:1px solid #444;
        border-radius:5px;
        background-color:rgba(150,150,150,1.0);
        -webkit-tap-highlight-color:rgba(0,0,0,0.0);
    }
</style>


<script>
    
    function generateFinishedPage(componentRef, builderObject){
        for(var attributeName in builderObject){
            componentRef[attributeName] = builderObject[attributeName];
        }
        componentRef.style.width = componentRef.width;
        componentRef.style.height = componentRef.height;
        //componentRef.style.backgroundColor = '#ABC';
        componentRef.display = _display_finished_page;
        componentRef.submitOnlyPressed = submitOnlyPressed;
        componentRef.clockOutPressed = clockOutPressed;
        componentRef.printReport = _printReport;
        _loadFinishedPageComponents(componentRef);
    }
    
    function _loadFinishedPageComponents(componentRef){

        var __width = 200;
        var _width  = __width + 'px';
        var __left  = componentRef.width/2 - __width/2;
        var _left   = __left + 'px';
        var topOffset = 20;
        var y_multiplier = 80;
        var submitWithoutClockingOutBtnDiv = document.createElement('div');
        
        //Submit server tips and info.
        var selectedColor  = '#AECD37';
        var nonSelectColor = 'rgba(88,88,88,0.15)';
        var pressedColor   = 'rgba(50,50,50,1.0)';
        submitWithoutClockingOutBtnDiv.isUserEnabled = true;
        submitWithoutClockingOutBtnDiv.id = 'submit_button_id';
        submitWithoutClockingOutBtnDiv.className = 'touchButtonStyle';
        submitWithoutClockingOutBtnDiv.style.left = _left;
        submitWithoutClockingOutBtnDiv.style.top  = topOffset+"px";
        submitWithoutClockingOutBtnDiv.style.width = _width;
        submitWithoutClockingOutBtnDiv.style.height = "32px";
        submitWithoutClockingOutBtnDiv.style.textAlign = "center";
        var vAlignLeft_1_Div = document.createElement('div');
        vAlignLeft_1_Div.style.height = '100%';
        vAlignLeft_1_Div.style.width = '0px';
        vAlignLeft_1_Div.style.verticalAlign = 'middle';
        vAlignLeft_1_Div.style.display = 'inline-block';
        //vAlignLeft_1_Div.style.backgroundColor='#FAA';
        var text_1_div = document.createElement('div');
        text_1_div.style.display = 'inline-block';
        text_1_div.style.position = 'inline';
        text_1_div.innerHTML = 'Submit';
        //text_1_div.style.backgroundColor='#AFA';
        submitWithoutClockingOutBtnDiv.appendChild(vAlignLeft_1_Div);
        submitWithoutClockingOutBtnDiv.appendChild(text_1_div);
        submitWithoutClockingOutBtnDiv.addEventListener('touchstart', function(){ _buttonPressed(this); });
        submitWithoutClockingOutBtnDiv.addEventListener('touchend', function(){ _buttonDepressed(this); });
        
        componentRef.appendChild(submitWithoutClockingOutBtnDiv);
        
        //Reprint button
        var printReportBtnDiv = document.createElement('div');
        printReportBtnDiv.id = 'print_report_btn_div_id';
        printReportBtnDiv.isUserEnabled = false;
        printReportBtnDiv.className = 'touchButtonStyleDisabled';
        printReportBtnDiv.style.left =  _left;
        printReportBtnDiv.style.top  =  topOffset + y_multiplier + "px";
        printReportBtnDiv.style.width = _width;
        printReportBtnDiv.style.height = "32px";
        var vAlignLeft_2_Div = document.createElement('div');
        vAlignLeft_2_Div.style.height = '100%';
        vAlignLeft_2_Div.style.width = '0px';
        vAlignLeft_2_Div.style.verticalAlign = 'middle';
        vAlignLeft_2_Div.style.display = 'inline-block';
        var text_2_div = document.createElement('div');
        text_2_div.style.display = 'inline-block';
        text_2_div.style.position = 'inline';
        text_2_div.innerHTML = "Re-Print Summary";
        printReportBtnDiv.appendChild(vAlignLeft_2_Div);
        printReportBtnDiv.appendChild(text_2_div);
        printReportBtnDiv.addEventListener('touchstart', function(){ _buttonPressed(this);});
        printReportBtnDiv.addEventListener('touchend',   function(){ _buttonDepressed(this);});
        
        componentRef.appendChild(printReportBtnDiv);
        
        //clockout
        var clockOutButton = document.createElement('div');
        clockOutButton.id = 'clock_out_button_div_id';
        clockOutButton.isUserEnabled = false;
        clockOutButton.className = 'touchButtonStyleDisabled';
        clockOutButton.style.left = _left;
        clockOutButton.style.top  = topOffset + 2*y_multiplier + "px";
        clockOutButton.style.width = _width;
        clockOutButton.style.height = "32px";
        var vAlignLeft_3_Div = document.createElement('div');
        vAlignLeft_3_Div.style.height = '100%';
        vAlignLeft_3_Div.style.width = '0px';
        vAlignLeft_3_Div.style.verticalAlign = 'middle';
        vAlignLeft_3_Div.style.display = 'inline-block';
        var text_3_div = document.createElement('div');
        text_3_div.style.display = 'inline-block';
        text_3_div.style.position = 'inline';
        text_3_div.innerHTML = 'Clockout';
        clockOutButton.appendChild(vAlignLeft_3_Div);
        clockOutButton.appendChild(text_3_div);
        clockOutButton.addEventListener('touchstart', function(){_buttonPressed(this);});
        clockOutButton.addEventListener('touchend',function(){_buttonDepressed(this);});
        
        componentRef.appendChild(clockOutButton);
    }
    
    function _printReport(){
        var cash_data_submission_row_id = this.submission_response['cash_data_submission_row_id'];
        var printFilePath = '_DO:cmd=print&filename=management/tips_wizard/tips_wizard_report.php&';
        var printFileArgs = 'vars=post_data=cash_data_submission_row_id(eq)'+cash_data_submission_row_id+'(and)data_name(eq)'+this.dataName+'(and)sec_hash(eq)'+this.secHash;
        window.location   = printFilePath + printFileArgs;
    }
        
    function _display_finished_page(reconData, decimalPlaceCount, receiptData){
        this.reconData = reconData;//False if not tilling out.
        this.decimalPlaceCount = decimalPlaceCount;
        this.receiptDataJSON = JSON.stringify(receiptData);
    }
    
    function submitOnlyPressed(){
        _submitFinalDataToServer(this);
    }
    
    function clockOutPressed(){
        
        var data_name = this.dataName;
        var sec_hash = this.secHash;
        var loc_id = this.locationID
        var servers_id = this.serversID;
        var serverClockPunchID = this.serverClockPunchID;
        var callbackPunchOutHandler = function (returnedDataFromServer){ 
        
            var clockoutBtn = document.getElementById('clock_out_button_div_id');
            clockoutBtn.waitingOnAjaxCall = false;
            
            //alert('Clockout response from server:' + returnedDataFromServer);
            deserializedResponse = JSON.parse(returnedDataFromServer);
            if(deserializedResponse.status == 'failed' && deserializedResponse.reason == 'already_clocked_out'){
                alert('Already clocked out');
                //printClockedOutReceipt(data_name, loc_id, sec_hash, servers_id, 'xxxx-xx-xx xx:xx:xx', 'xxxx-xx-xx xx:xx:xx', '0.02');
            }
            else{
                var clockOutButton = document.getElementById('clock_out_button_div_id');
                setTimeout(function(){ clockOutButton.isUserEnabled = false; setButtonClassNameStylesBasedOnWhetherEnabled(); }, 200);
                var punchRowStr = deserializedResponse['punch_row'];
                var punchRowObj = JSON.parse(punchRowStr);
                printClockedOutReceipt(data_name, loc_id, sec_hash, servers_id, punchRowObj['time'], punchRowObj['time_out'], punchRowObj['hours']);
            }
        };
        var currDeviceTime = getDeviceTime();
        postClockOut(this.serverClockPunchID, callbackPunchOutHandler, currDeviceTime);
        
    }
    function printClockedOutReceipt(dataName, loc_id, secHash, servers_id, checkedInTime, checkedOutTime, hours){
        /*
            builder.dataName = '<?php echo $data_name;?>';
            builder.secHash  = '<?php echo $sec_hash_for_printing;?>';
        */  
        var checked_in_time  = encodeURIComponent(checkedInTime);
        var checked_out_time = encodeURIComponent(checkedOutTime);
        var locationPartStr  = "_DO:cmd=print&filename=management/tips_wizard/tips_wizard_clockout_print.php&vars=";
        var argumentsPartStr = 'script_args=checked_in_time(eq)'+checked_in_time+'(and)checked_out_time(eq)'+checked_out_time;
        argumentsPartStr += "(and)data_name(eq)"+dataName+'(and)sec_hash(eq)'+secHash;
        argumentsPartStr += "(and)location_id(eq)"+loc_id+"(and)servers_id(eq)"+servers_id;
        argumentsPartStr += "(and)hours(eq)"+hours;

        window.location = locationPartStr + argumentsPartStr;
    }
    function _submitFinalDataToServer(componentRef){
        
        //Parts for till out:
        //    Value 4 contains bill reconciliations the '/' delimited to the left of ':'
        //        are the amounts given to the server, the right are tilled out by server.
        //    Value 5 are the coins given to the server : coins given back by server.
        //    Value 3 is the till out time(without date apparently).
        //    Value is of the format: amountGivenToServer:amountInDrawer
        // cash_data fields to update, by indices.   
        // 'posted'      till_out_total. Contains the till out total with the correct decimal length.
        //                    ->goes to 2nd half of `value`.
        // 'dynamic'     check_out_time in form: 'H:i:s'
        // 'posted'      till_out_bills.  '/' delimited list of how many bills are checking in.
        //                    ->goes to 2nd half of `value4`.
        // 'posted'      till_out_coins.  '/' delimited list of how many coins are checking in.
        // 'posted'      cash_data_id.  The id of the cash_data row that we will update.
        // 'posted'      number_of_decimal_places.
        submitObject = {};
        submitObject.total_tips = componentRef.lastEnteredTipsTotalCashAndCC;
        submitObject.cash_tips  = componentRef.lastEnteredTipsCashAlone;
        submitObject.cc_tips    = componentRef.lastEnteredTipsCCsOnly;
        //submitObject.over_short = componentRef.lastOverShortAmount;//No longer using this... this value is now incorrect anyways.
        submitObject.server_id = componentRef.serverID;
        submitObject.location_id = componentRef.locationID;
        submitObject.tipout_mappings = encodeURIComponent( JSON.stringify(componentRef.tipoutMappings) );
        submitObject.number_of_decimal_places = componentRef.decimalPlaceCount;
        submitObject.receipt_data = componentRef.receiptDataJSON;
        submitObject.total_cash_sales = componentRef.totalCashSales;
        submitObject.total_sales = componentRef.totalSales;
        submitObject.localized_date_time_tips_wizard_started = componentRef.dateTimeNow;
        submitObject.decimal_char = componentRef.decimalChar;
        submitObject.server_date_time_tips_wizard_started = componentRef.serverDateTimeNow;
        submitObject.fiscal_day_start = componentRef.fiscalDayStart;
        submitObject.device_time = getDeviceTime();
    
        submitObject.register = '<?php echo $REQUEST_register;?>';
        submitObject.register_name = '<?php echo $REQUEST_register_name;?>';
        submitObject.UUID = '<?php echo $SESSION_UUID;?>';
    
        if( typeof calculated_expression_used_server_turn_in != 'undefined'){
            submitObject.calculated_expression_used_server_turn_in = calculated_expression_used_server_turn_in;
        }
        if( typeof calculated_value_server_turn_in != 'undefined'){
            submitObject.calculated_value_server_turn_in = calculated_value_server_turn_in;
        }
        if( typeof calculated_expression_all_parts != 'undefined'){
            submitObject.calculated_expression_all_parts = calculated_expression_all_parts;
        }
        if( typeof calculated_value_param_args != 'undefined'){
            submitObject.calculated_value_param_args = calculated_value_param_args;
        }
        /*
        var server_deposite_to_restaurant_amount = 0;
        var server_deposite_take_title = '';
         */
        /*
        if( typeof server_deposite_to_restaurant_amount != 'undefined'){
            submitObject.server_deposite_to_restaurant_amount;
        }
        */
        if( typeof server_deposite_take_title != 'undefined'){
            submitObject.server_deposite_take_title = server_deposite_take_title;
        }

        if(componentRef.reconData){
            submitObject.is_tilling_out = "1";
            submitObject.till_out_bill_denominations = componentRef.reconData.billDenominations;
            submitObject.till_out_coin_denominations = componentRef.reconData.coinDenominations;
            submitObject.till_cash_data_id = componentRef.tillInID;
            submitObject.till_in_total  = componentRef.totalTillIn;
            submitObject.till_out_total = componentRef.reconData.totalCash;
            submitObject.till_out_bills = componentRef.reconData.billsReturned;
            submitObject.till_out_coins = componentRef.reconData.coinsReturned;
        }
        else{
            submitObject.is_tilling_out = "0";
        }
        if(componentRef.submission_response){
            //alert('submission already occured, response object: ' + componentRef.submission_response);
            return;
        }

        submitFinalDataToServer(submitObject, callbackFromSubmissionAjax);
    }
    
    function getDeviceTime(){

        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth()+1;
        month = month < 10 ? "0"+month : ""+month;
        var date = d.getDate();
        date = date < 10 ? "0"+date : ""+date;
        var hour = d.getHours();
        hour = hour < 10 ? "0"+hour : ""+hour;
        var mins = d.getMinutes();
        mins = mins < 10 ? "0"+mins : ""+mins;
        var secs = d.getSeconds();
        secs = secs < 10 ? "0"+secs : ""+secs;
        var fullDateTime = year+'-'+month+'-'+date+' '+hour+':'+mins+':'+secs;
        return fullDateTime;

    }
    
    
    function callbackFromSubmissionAjax(responseText){
        
        //We now re-enable the ability to press the button.  On success it will
        //disable anyways.
        var submitButton = document.getElementById('submit_button_id');
        submitButton.waitingOnAjaxCall = false;
        
        
        var componentRefByID = document.getElementById('finished_page_div_id');
        //Here we can just go back to the management page.
        componentRefByID.submission_response = JSON.parse(responseText);
        var responseObj = componentRefByID.submission_response;
        //alert("response: " + responseText);
        
        if(!responseObj || responseObj.status != "success"){
            //alert("Connection error, please check network settings.");
            return;
        }
        if(responseObj.status == "success"){
            setTimeout(function(){submitHasReturnedUpdateState(responseObj);}, 200);
        }
        else{
            alert("There was an unknown failure while attempting to upload data");
            return;
        }
    }
    function submitHasReturnedUpdateState(responseObj){
    
        var errors_arr = responseObj['gateway_errors'];
        if(errors_arr.length == 0){
            alert("Submission Successful.");
        }else{
            var alertStr = '';
            alertStr += "The credit card gateway has reported issues while updating tip amounts for the following transactions.  ";
            alertStr += "Please contact support for more information.\n\n";
            for(var i = 0; i < errors_arr.length; i++){
                //"cc_transaction_row" => $cc_trans_row, "order_id" => $orderID, "tip_amount" => $tip_amount
                alertStr += "Order ID: " + errors_arr[i]['order_id'] + "\n";
                alertStr += "Transaction ID: " + errors_arr[i]['cc_transaction_row']['transaction_id'] + "\n";
                alertStr += "Gateway Response: " + errors_arr[i]['error_response'] + "\n\n";
            }
            alert(alertStr);
        }
        
    
        var submitButtonDiv = document.getElementById('submit_button_id');
        submitButtonDiv.isUserEnabled = false;
        var printReportButtonDiv = document.getElementById('print_report_btn_div_id');
        printReportButtonDiv.isUserEnabled = true;
        var clockoutButtonDiv = document.getElementById('clock_out_button_div_id');
        clockoutButtonDiv.isUserEnabled = true;
        setButtonClassNameStylesBasedOnWhetherEnabled();
        
        //We have submitted the server data, so we don't want them to go back.
        // id of that button:go_left_div_id, property: .isDisabled
        var goLeftNavButton = document.getElementById('go_left_div_id');
        goLeftNavButton.isDisabled = true;
        goLeftNavButton.style.backgroundColor = 'rgba(88,88,88,0.15)';
        //goLeftNavButton.className = 'touchButtonStyleDisabled';
        
        //Now we print out the regular report
        var componentRefByID = document.getElementById('finished_page_div_id');
        componentRefByID.printReport();
    }




    function setButtonClassNameStylesBasedOnWhetherEnabled(){
        //    .touchButtonStyle{      .touchButtonStyleDisabled{     .touchButtonPressedStyle{
        var submitButton = document.getElementById('submit_button_id');
        submitButton.className = submitButton.isUserEnabled ? 'touchButtonStyle' : 'touchButtonStyleDisabled';
        
        var printButton    = document.getElementById('print_report_btn_div_id');
        printButton.className = printButton.isUserEnabled ? 'touchButtonStyle' : 'touchButtonStyleDisabled';
              
        var clockOutButton = document.getElementById('clock_out_button_div_id');
        clockOutButton.className = clockOutButton.isUserEnabled ? 'touchButtonStyle' : 'touchButtonStyleDisabled';
    }
    function _buttonPressed(elem){
        if(elem.pressedDown){
            return;
        }
        if(!elem.isUserEnabled){
            return;
        }
        if(elem.waitingOnAjaxCall){
            return;
        }
        elem.waitingOnAjaxCall = true;
        
        
        var localComponentRef = document.getElementById('finished_page_div_id');
        //Handling the button pressed events.
        if(elem.id=='submit_button_id'){
            elem.className = 'touchButtonPressedStyle';
            localComponentRef.submitOnlyPressed();
        }
        if(elem.id=='print_report_btn_div_id'){
            elem.className = 'touchButtonPressedStyle';
            localComponentRef.printReport();
        }
        if(elem.id=='clock_out_button_div_id'){
            elem.className = 'touchButtonPressedStyle';
            localComponentRef.clockOutPressed();
        }
        elem.pressedDown = true;
    }
    
    function _buttonDepressed(elem){
        elem.pressedDown = false;
        if(elem.className == 'touchButtonPressedStyle' && elem.id != 'submit_button_id'){
            elem.className = 'touchButtonStyle';
            setTimeout(function(){elem.className='touchButtonStyle';}, 100);
        }
    }

</script>