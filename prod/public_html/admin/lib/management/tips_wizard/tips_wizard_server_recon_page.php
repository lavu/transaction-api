<script>

    //The builderObject is an object that contains all the attributes for the component.
    //Should be empty except for needed properties:
    //  width, height, 
    //  main_denominations (array of 1, 2, 5, 10, 20, etc.), 
    //  sub_denominations (array of 1.00, .50, .25, .10, .5, .1, etc.)
    //  row_height (actual height of the entry label/inputs)
    //  desired_row_space  (space between entry rows)
    //  min_row_space
    //  label_font_size
    //  input_font_size
    //  input_selected_event_handler(col, row)
    //
    //  This component has a reference to all the inputs:
    //  getInputAtColumnRow(col, row), returns the actual html-input reference.
    function generateServerReconDisplayComponent(componentRef, builderObject){
        //componentRef = document.createElement('div');
        for(var attributeName in builderObject){
            componentRef[attributeName] = builderObject[attributeName];
        }
        componentRef.style.width = componentRef.width + 5;
        componentRef.style.height = componentRef.height;
        componentRef.style.backgroundColor = '#FAFAFA';
        //componentRef.style.border = "1px solid #aaa";
        //componentRef.style.overflow = 'scroll';
        //Total Columns and Rows.
        componentRef.totalRows = Math.max(componentRef.main_denominations.length, componentRef.sub_denominations.length);
        componentRef.leftInputDivs = [];
        componentRef.rightInputDivs = [];
        //_sr_calculateDimensions(componentRef);
        if(!componentRef.main_denominations_title){componentRef.main_denominations_title = 'Bills';}
        if(!componentRef.sub_denominations_title){ componentRef.sub_denominations_title = 'Coins';}
        _sr_buildInnerTableWithComponents(componentRef);
        componentRef.getSelectedElemValue = function(){ return componentRef.selectedInput.innerHTML; };
        componentRef.setSelectedElemValue = function(value){ componentRef.selectedInput.innerHTML = value; };
        componentRef.moveFocusToNextInput = function(){
        
            var _selected_row = componentRef.selectedInput.rowInTable;
            var _selected_col = componentRef.selectedInput.columnInTable;      
            //3 conditions, bottom left, bottom right, or anything else.
            if(_selected_col == 0 && _selected_row == componentRef.leftInputDivs.length - 1){
                if(componentRef.rightInputDivs.length > 0){
                    newSelectedElem = componentRef.rightInputDivs[0];
                    _setSelectedInput(componentRef, newSelectedElem);
                }
            }
            else if(_selected_col == 1 && _selected_row == componentRef.rightInputDivs.length - 1){
                componentRef.returnOnLastInputFunction(true);
            }
            else{
                var newSelectedElem;
                if( _selected_col == 0 ){
                    newSelectedElem = componentRef.leftInputDivs[_selected_row + 1];
                }
                else{
                    newSelectedElem = componentRef.rightInputDivs[_selected_row + 1];
                }
                _setSelectedInput(componentRef, newSelectedElem);
            }

            /*
            componentRef.selectedInput
            leftValueDiv.rowInTable = i;
            leftValueDiv.columnInTable = 0;
            */
            
        };
        //componentRef.getTotalCashReported = getTotalCashReported;
        componentRef.getReconciliationArraysAndTotal = getReconciliationArraysAndTotal;
    }
    function _sr_calculateDimensions(componentRef){
        //We first see if we can fit the desired dimensions by calculating ration
        var totalInnerComponentsHeight = componentRef.totalRows * componentRef.row_height;
            totalInnerComponentsHeight += componentRef.totalRows * componentRef.desired_row_width;
        
        //If naturally fits
        if(totalInnerComponentsHeight <= componentRef.height){
            componentRef.actual_row_space  = componentRef.desired_row_space;    
            componentRef.contentHeight = componentRef.height;
            return;
        }
    
        //If we only need to reduce the size of the spaces.
        totalInnerHeightWithMinSpaces  = componentRef.totalRows * componentRef.min_row_space;
        totalInnerHeightWithMinSpaces += componentRef.totalRows * componentRef.row_height;
        if(totalInnerHeightWithMinSpaces < componentRef.height){
            //We calculate the amount of space then that's needed
            var totalSpaceHeight = componentRef.height - (componentRef.totalRows * row_height);
            componentRef.actual_row_space = totalSpaceHeight/componentRef.totalRows;
            componentRef.contentHeight = componentRef.height;
            return;
        }
        
        //We make the space equal to the minumum allowed space
        componentRef.actual_row_space = componentRef.min_row_space;
        componentRef.contentHeight  = componentRef.totalRows * componentRef.actual_row_space;
        componentRef.contentHeight += componentRef.totalRows * componentRef.row_height;
        return;
    }




    function _sr_buildInnerTableWithComponents(componentRef){
        var innerTable = document.createElement('table');
        var tableBody  = document.createElement('tbody');
        var borderWidth = 4;
        var totalBorder = 8 * borderWidth;
        var amortizedWidth = componentRef.width - totalBorder;
        innerTable.style.height = componentRef.conentHeight;
        innerTable.style.width  = componentRef.width;
        innerTable.border = '0';
        //innerTable.style.border = '1px solid #777';
        innerTable.cellSpacing = '0';
        var a_1r = Math.floor(235 * 0.92);
        var a_1g = Math.floor(241 * 0.95);
        var a_1b = Math.floor(237 * 0.92);
        var rowShadeOne = 'rgb(' + a_1r + ',' + a_1g + ',' + a_1b + ')';//'#ebf1ed';
        var rowshadeTwo = 'rgb(235,241,237);'//'#FAFAFA';
        var inputShade  = '#FFF';
        
        //We create the title row.
        var titleRow = document.createElement('tr');
        //titleRow.style.backgroundColor = '#EEE';
        var titleRowDivLeft = document.createElement('div');
        titleRowDivLeft.style.width = '100%';
        titleRowDivLeft.style.height = 25;
        titleRowDivLeft.innerHTML = componentRef.main_denominations_title;
        titleRowDivLeft.style.fontSize = '18px';
        titleRowDivLeft.style.textAlign = 'center';
        var leftTitleTD = document.createElement('td');
        leftTitleTD.colSpan = 1;
        leftTitleTD.appendChild(titleRowDivLeft);
        titleRow.appendChild(document.createElement('td'));
        titleRow.appendChild(leftTitleTD);
        
        var titleRowDivRight = document.createElement('div');
        titleRowDivRight.style.width = '100%';
        titleRowDivRight.style.height = 25;
        titleRowDivRight.innerHTML = componentRef.sub_denominations_title;
        titleRowDivRight.style.fontSize = '18px';
        titleRowDivRight.style.textAlign = 'center';
        var rightTitleTD = document.createElement('td');
        rightTitleTD.colSpan = 1;
        rightTitleTD.appendChild(titleRowDivRight);
        titleRow.appendChild(document.createElement('td'));
        titleRow.appendChild(rightTitleTD);
        tableBody.appendChild(titleRow);
        
        for(i = 0; i < componentRef.totalRows; i++){    
            var currRow = document.createElement('tr');
            
            //Left Label
            //currRow.style.backgroundColor = i%2==0 ? rowShadeOne : rowshadeTwo;
            var leftLabelTD   = document.createElement('td');
            var leftLabelDiv  = document.createElement('div');
            leftLabelDiv.innerHTML = "&nbsp;"
            leftLabelDiv.style.textAlign = 'right';
            //leftLabelDiv.style.backgroundColor = i%2==0 ? rowShadeOne : rowshadeTwo;
            leftLabelDiv.style.width = amortizedWidth*3/16;
            leftLabelDiv.style.height = componentRef.row_height;
            
            //Left Value
            var leftValueTD   = document.createElement('td');
            leftValueTD.style.maxHeight = componentRef.row_height;
            var leftValueDiv  = document.createElement('div');
            leftValueDiv.style.overflowX = 'hidden';
            leftValueDiv.style.overflowY = 'hidden';
            leftValueDiv.innerHTML = "&nbsp;";
            leftValueDiv.style.textAlign = 'center';
            //leftValueDiv.style.backgroundColor = inputShade;
            leftValueDiv.style.width = amortizedWidth*5/16;
            leftLabelDiv.style.height = componentRef.row_height;

 
            //Right Label
            var rightLabelTD  = document.createElement('td');
            var rightLabelDiv = document.createElement('div');
            rightLabelDiv.innerHTML = "&nbsp;";
            rightLabelDiv.style.textAlign = 'right';
            //rightLabelDiv.style.backgroundColor = i%2==0 ? rowShadeOne : rowshadeTwo;
            rightLabelDiv.style.width = amortizedWidth*3/16;
            rightLabelDiv.style.height = componentRef.row_height;
            
            //Right Value
            var rightValueTD  = document.createElement('td');
            var rightValueDiv = document.createElement('div');
            rightValueDiv.innerHTML = "&nbsp;";
            rightValueDiv.style.textAlign = 'center';
            //rightValueDiv.style.backgroundColor = inputShade;
            rightValueDiv.style.width = amortizedWidth*5/16;
            leftLabelDiv.style.height = componentRef.row_height;
            
            
            //componentRef.main_denominations.length, componentRef.sub_denominations.length
            if(i < componentRef.main_denominations.length){
                leftLabelTD.appendChild(leftLabelDiv); 
                leftValueTD.appendChild(leftValueDiv);
            }
            
            if(i < componentRef.sub_denominations.length){
                rightLabelTD.appendChild(rightLabelDiv);
                rightValueTD.appendChild(rightValueDiv);
            }
            currRow.appendChild(leftLabelTD);
            currRow.appendChild(leftValueTD);
            currRow.appendChild(rightLabelTD);
            currRow.appendChild(rightValueTD);
            //build the row
            
            
            //3 conditions, 1.) only_bill, 2.) only_coin, 3.) both
            var rowType;
            var hasBill = i < componentRef.main_denominations.length;
            var hasCoin = i < componentRef.sub_denominations.length;
            var _tchd = false;//Mutex on touch events.
            //Here we create the inner html of the divs.
            
            if(hasBill){
                leftLabelDiv.innerHTML = componentRef.main_denominations[i] + ' x ';
                leftValueDiv.rowInTable = i;
                leftValueDiv.columnInTable = 0;
                leftValueDiv.onclick = function(){_setSelectedInput(componentRef, this); };
                componentRef.leftInputDivs[i] = leftValueDiv;
                leftValueDiv.addEventListener('touchstart', function(){ if(_tchd) return; _tchd=true;_setSelectedInput(componentRef,this);});
                leftValueDiv.addEventListener('touchend', function(){ _tchd=false;});

            }
            if(hasCoin){
                rightLabelDiv.innerHTML = componentRef.sub_denominations[i] + ' x ';
                rightValueDiv.rowInTable = i;
                rightValueDiv.columnInTable = 1;
                rightValueDiv.onclick = function(){_setSelectedInput(componentRef, this); };
                componentRef.rightInputDivs[i] = rightValueDiv;
                rightValueDiv.addEventListener('touchstart', function(){ if(_tchd) return; _tchd=true;_setSelectedInput(componentRef,this);});
                rightValueDiv.addEventListener('touchend', function(){ _tchd=false;});
                
            }
            
            tableBody.appendChild(currRow);
        }
        innerTable.appendChild(tableBody);
        componentRef.appendChild(innerTable);
        _setSelectedInput( componentRef, componentRef.leftInputDivs[0] );
        
    }
   


    /*
    function _sr_buildInnerTableWithComponents(componentRef){
        var innerTable = document.createElement('table');
        var tableBody  = document.createElement('tbody');
        var borderWidth = 4;
        var totalBorder = 8 * borderWidth;
        var amortizedWidth = componentRef.width - totalBorder;
        innerTable.style.height = componentRef.conentHeight;
        innerTable.style.width  = componentRef.width;
        innerTable.border = '0';
        innerTable.style.border = '1px solid #777';
        innerTable.cellSpacing = '0';
        var a_1r = Math.floor(235 * 0.92);
        var a_1g = Math.floor(241 * 0.95);
        var a_1b = Math.floor(237 * 0.92);
        var rowShadeOne = 'rgb(' + a_1r + ',' + a_1g + ',' + a_1b + ')';//'#ebf1ed';
        var rowshadeTwo = 'rgb(235,241,237);'//'#FAFAFA';
        var inputShade  = '#FFF';
        
        //We create the title row.
        var titleRow = document.createElement('tr');
        titleRow.style.backgroundColor = '#EEE';
        var titleRowDivLeft = document.createElement('div');
        titleRowDivLeft.style.width = '100%';
        titleRowDivLeft.style.height = 25;
        titleRowDivLeft.innerHTML = componentRef.main_denominations_title;
        titleRowDivLeft.style.fontSize = '18px';
        titleRowDivLeft.style.textAlign = 'center';
        var leftTitleTD = document.createElement('td');
        leftTitleTD.colSpan = 1;
        leftTitleTD.appendChild(titleRowDivLeft);
        
        var titleRowDivRight = document.createElement('div');
        titleRowDivRight.style.width = '100%';
        titleRowDivRight.style.height = 25;
        titleRowDivRight.innerHTML = componentRef.sub_denominations_title;
        titleRowDivRight.style.fontSize = '18px';
        titleRowDivRight.style.textAlign = 'center';
        var rightTitleTD = document.createElement('td');
        rightTitleTD.colSpan = 1;
        rightTitleTD.appendChild(titleRowDivRight);
        
        titleRow.appendChild(leftTitleTD);
        titleRow.appendChild(document.createElement('td'));
        titleRow.appendChild(rightTitleTD);
        titleRow.appendChild(document.createElement('td'));
        tableBody.appendChild(titleRow);
        
        for(i = 0; i < componentRef.totalRows; i++){    
            var currRow = document.createElement('tr');
            
            //Left Label
            currRow.style.backgroundColor = i%2==0 ? rowShadeOne : rowshadeTwo;
            var leftLabelTD   = document.createElement('td');
            var leftLabelDiv  = document.createElement('div');
            leftLabelDiv.innerHTML = "&nbsp;"
            leftLabelDiv.style.textAlign = 'right';
            leftLabelDiv.style.backgroundColor = i%2==0 ? rowShadeOne : rowshadeTwo;
            leftLabelDiv.style.width = amortizedWidth*3/16;
            leftLabelDiv.style.height = componentRef.row_height;
            
            //Left Value
            var leftValueTD   = document.createElement('td');
            leftValueTD.style.maxHeight = componentRef.row_height;
            var leftValueDiv  = document.createElement('div');
            leftValueDiv.style.overflowX = 'hidden';
            leftValueDiv.style.overflowY = 'hidden';
            leftValueDiv.innerHTML = "&nbsp;";
            leftValueDiv.style.textAlign = 'center';
            leftValueDiv.style.backgroundColor = inputShade;
            leftValueDiv.style.width = amortizedWidth*5/16;
            leftLabelDiv.style.height = componentRef.row_height;

 
            //Right Label
            var rightLabelTD  = document.createElement('td');
            var rightLabelDiv = document.createElement('div');
            rightLabelDiv.innerHTML = "&nbsp;";
            rightLabelDiv.style.textAlign = 'right';
            rightLabelDiv.style.backgroundColor = i%2==0 ? rowShadeOne : rowshadeTwo;
            rightLabelDiv.style.width = amortizedWidth*3/16;
            rightLabelDiv.style.height = componentRef.row_height;
            
            //Right Value
            var rightValueTD  = document.createElement('td');
            var rightValueDiv = document.createElement('div');
            rightValueDiv.innerHTML = "&nbsp;";
            rightValueDiv.style.textAlign = 'center';
            rightValueDiv.style.backgroundColor = inputShade;
            rightValueDiv.style.width = amortizedWidth*5/16;
            leftLabelDiv.style.height = componentRef.row_height;
            
            
            //componentRef.main_denominations.length, componentRef.sub_denominations.length
            if(i < componentRef.main_denominations.length){
                leftLabelTD.appendChild(leftLabelDiv); 
                leftValueTD.appendChild(leftValueDiv);
            }
            
            if(i < componentRef.sub_denominations.length){
                rightLabelTD.appendChild(rightLabelDiv);
                rightValueTD.appendChild(rightValueDiv);
            }
            currRow.appendChild(leftLabelTD);
            currRow.appendChild(leftValueTD);
            currRow.appendChild(rightLabelTD);
            currRow.appendChild(rightValueTD);
            //build the row
            
            
            //3 conditions, 1.) only_bill, 2.) only_coin, 3.) both
            var rowType;
            var hasBill = i < componentRef.main_denominations.length;
            var hasCoin = i < componentRef.sub_denominations.length;
            var _tchd = false;//Mutex on touch events.
            //Here we create the inner html of the divs.
            
            if(hasBill){
                leftLabelDiv.innerHTML = componentRef.main_denominations[i] + ' x ';
                leftValueDiv.rowInTable = i;
                leftValueDiv.columnInTable = 0;
                leftValueDiv.onclick = function(){_setSelectedInput(componentRef, this); };
                componentRef.leftInputDivs[i] = leftValueDiv;
                leftValueDiv.addEventListener('touchstart', function(){ if(_tchd) return; _tchd=true;_setSelectedInput(componentRef,this);});
                leftValueDiv.addEventListener('touchend', function(){ _tchd=false;});

            }
            if(hasCoin){
                rightLabelDiv.innerHTML = componentRef.sub_denominations[i] + ' x ';
                rightValueDiv.rowInTable = i;
                rightValueDiv.columnInTable = 1;
                rightValueDiv.onclick = function(){_setSelectedInput(componentRef, this); };
                componentRef.rightInputDivs[i] = rightValueDiv;
                rightValueDiv.addEventListener('touchstart', function(){ if(_tchd) return; _tchd=true;_setSelectedInput(componentRef,this);});
                rightValueDiv.addEventListener('touchend', function(){ _tchd=false;});
                
            }
            
            tableBody.appendChild(currRow);
        }
        innerTable.appendChild(tableBody);
        componentRef.appendChild(innerTable);
        _setSelectedInput( componentRef, componentRef.leftInputDivs[0] );
        
    }
    */
    
    function _setSelectedInput(componentRef, elem){
        for(i=0;i<componentRef.leftInputDivs.length;i++){
            componentRef.leftInputDivs[i].style.border = '2px solid #aaa';
            componentRef.leftInputDivs[i].style.borderRadius = 8;
        }
        for(i=0;i<componentRef.rightInputDivs.length;i++){
            componentRef.rightInputDivs[i].style.border = '2px solid #aaa';
            componentRef.rightInputDivs[i].style.borderRadius = 8;
        }
        componentRef.selectedInput = elem;
        elem.style.border = '2px solid #665';
        elem.style.borderRadius = 5;
    }
    function getReconciliationArraysAndTotal(){
        var serverReconValues = {};
        serverReconValues.billsReturned = [];
        serverReconValues.coinsReturned = [];
        //serverReconTotal = this.getTotalCashReported();
        var totalValueInBills = 0.0;
        for(var i = 0; i < this.main_denominations.length; i++){
            var currBillDenom = parseFloat(this.main_denominations[i]);
            var numberOfBills = this.leftInputDivs[i].innerHTML;
            numberOfBills = numberOfBills.replace(/&nbsp;/g, "");
            numberOfBills = numberOfBills == '' ? 0 : parseInt(numberOfBills);
            serverReconValues.billsReturned[i] = numberOfBills;
            totalValueInBills += serverReconValues.billsReturned[i] * currBillDenom;
        }
        var totalValueInCoins = 0.0;
        for(var i = 0; i < this.sub_denominations.length; i++){
            var currCoinDenom = parseFloat(this.sub_denominations[i]);
            var numberOfCoins = this.rightInputDivs[i].innerHTML;
            numberOfCoins = numberOfCoins.replace(/&nbsp;/g, "");
            numberOfCoins = numberOfCoins == '' ? 0 : parseInt(numberOfCoins);
            serverReconValues.coinsReturned[i] = numberOfCoins;
            totalValueInCoins += serverReconValues.coinsReturned[i] * currCoinDenom;
        }
        serverReconValues.totalCash = totalValueInBills + totalValueInCoins;
        serverReconValues.billDenominations = this.main_denominations;
        serverReconValues.coinDenominations = this.sub_denominations;
        return serverReconValues;
    }

</script>