<?php

    require_once(dirname(__FILE__) . '/../../gateway_functions.php');
    
    
    function send_all_cc_transactions_to_gateway($data_name, $location_row, $receipt_info_arr){

        //debugLog("-------------------------START--------------------------");
        $cc_transaction_ids = array();
        foreach($receipt_info_arr as $currReceipt){
            //Here we pick up all the transaction ids.
            $cc_transaction_ids[] = $currReceipt['transaction_id'];
        }
        
        //All the cc_transaction rows mentioned in $receipt_info_arr, with row's id as key.
        $ccTransactionRows = getCCTransactionRowsByIds($cc_transaction_ids);
        
        //Now we have the ccTransactionRows in an array where their id is the key.
        //We now attach the ccTransaction row into the corresponding receipt.
        $fullReceiptAndCCRowArrs = array();
        foreach($receipt_info_arr as $currReceipt){
            $currReceipt_cc_row = $ccTransactionRows[$currReceipt['transaction_id']];
            $fullReceiptAndCCRowArrs[] = array('cc_transaction_row' => $currReceipt_cc_row, 'receipt_arr' => $currReceipt);
        }
        
        $failedSubmissionArr = array();
        foreach($fullReceiptAndCCRowArrs as $currRow){
            $cc_trans_row = $currRow['cc_transaction_row'];
            $receipt_row  = $currRow['receipt_arr'];
            $tip_amount = $receipt_row['tip_amount'];
            $orderID = $receipt_row['order_id'];
            $response = send_cc_trans_to_gateway($data_name, $location_row, $cc_trans_row, $tip_amount, $orderID);
            if($response!==true){
                $failedSubmissionArr[] = array("cc_transaction_row" => $cc_trans_row, 
                                                "order_id" => $orderID, "tip_amount" => $tip_amount, "error_response" => $response);
            }
        }
        return $failedSubmissionArr;
        //debugLog("------------------------- END --------------------------\n\n\n\n\n");
    }
    
    function send_cc_trans_to_gateway($data_name, $location_row, $transaction_row, $tip_amount, $orderID){
        
        /*
        error_log("TRANSACTION ROW: " . $transaction_row['id']);
        error_log("TIP AMOUNT: " . $tip_amount);
        error_log("ORDER-ID: " . $orderID);
        */
        //The entire purpose of this file is for each cc_transaction 
        //                 with a card tip to call the gateway function:
        //   ---  process_capture_or_tips()
        //   ---  and passing the 8 args:
        //   ---    location_info, integration_info, process_info, transaction_info,
        //   ---    apply_tip, cardtip_orderid, over_paid_CCs, and true. (not sure about that last one yet ;)
        
        // 1.)  $location_row
        $location_info = $location_row;//Defined in tips_wizard_data.php.
        
        // 2.)  $integration_info, set as how is set in manage_tips.php
        $integration_info = get_integration_from_location($location_info['id'], 'poslavu_'.$data_name.'_db');
        
        // 3.)  $process_info  (Found how this is built inside of 'manage_tips.php').

        /* ALL occurances inside of manage_tips.php
        
        //L. 224--
        $process_info = array();
    	$process_info['mag_data'] = "";
    	$process_info['reader'] = "";
    	$process_info['card_number'] = "";
    	$process_info['card_cvn'] = "";
    	$process_info['name_on_card'] = "";
    	$process_info['exp_month'] = "";
    	$process_info['exp_year'] = "";
    	$process_info['card_amount'] = "";
    	$process_info['company_id'] = $company_info['id'];
    	$process_info['loc_id'] = $loc_id;
    	$process_info['ext_data'] = "";
    	$process_info['data_name'] = $company_info['data_name'];
    	$process_info['register'] = $register;
    	$process_info['register_name'] = $register_name;
    	$process_info['device_udid'] = $device_id; 
    	$process_info['server_id'] = $server_id;
    	$process_info['server_name'] = "";
    	$process_info['device_time'] = $device_time;
    	$process_info['set_pay_type'] = "";
    	$process_info['set_pay_type_id'] = "";
    	$process_info['for_deposit'] = "";
    	$process_info['is_deposit'] = "";
    	
    	//L.259--
    	$process_info['username'] = $integration_info['integration1'];
        $process_info['password'] = $integration_info['integration2'];
        $process_info['integration3'] = $integration_info['integration3'];
        $process_info['integration4'] = $integration_info['integration4'];
        $process_info['integration5'] = $integration_info['integration5'];
        
        //L.291--
        $process_info['username'] = $integration_info['integration1'];
        $process_info['password'] = $integration_info['integration2'];
        $process_info['order_id'] = $transaction_info['order_id'];
        $process_info['check'] = $transaction_info['check'];
        $process_info['pnref'] = $transaction_info['transaction_id'];
        */
        
        $process_info = array();
        $process_info['mag_data'] = "";
    	$process_info['reader'] = "";
    	$process_info['card_number'] = "";
    	$process_info['card_cvn'] = "";
    	$process_info['name_on_card'] = "";
    	$process_info['exp_month'] = "";
    	$process_info['exp_year'] = "";
    	$process_info['card_amount'] = "";
    	$process_info['company_id'] = getCompanyID($data_name);//defined in tips_wizard_data.php
    	$process_info['loc_id'] = $location_info['id'];
    	$process_info['ext_data'] = "";
    	$process_info['data_name'] = $data_name;
    	$process_info['register'] = $_REQUEST['register'];//Posted by javascript-tips_wizard_finished_page.php
    	$process_info['register_name'] = $_REQUEST['register_name'];//Posted by javascript-tips_wizard_finished_page.php
    	$process_info['device_udid'] = $_REQUEST['UUID'];//Posted by javascript-tips_wizard_finished_page.php
    	$process_info['server_id'] = getServerID();
    	$process_info['server_name'] = "";
    	$process_info['device_time'] = $_REQUEST['device_time'];//Posted by javascript-tips_wizard_finished_page.php
    	$process_info['set_pay_type'] = "";
    	$process_info['set_pay_type_id'] = "";
    	$process_info['for_deposite'] = "";
    	$process_info['is_deposit'] = "";
    	//--
    	$process_info['username'] = $integration_info['integration1'];
    	$process_info['password'] = $integration_info['integration2'];
    	$process_info['integration3'] = $integration_info['integration3'];
        $process_info['integration4'] = $integration_info['integration4'];
        $process_info['integration5'] = $integration_info['integration5'];
        //--
        $process_info['order_id'] = $transaction_info['order_id'];
        $process_info['check'] = $transaction_info['check'];
        $process_info['pnref'] = $transaction_info['transaction_id'];
        
        // 4.) $transaction_info  CHECK!
        $transaction_row;
        
        // 5.) $apply_tip (the tip amount entered by the server). CHECK!
        $tip_amount;
        
        // 6.) $cardtip_orderid (the order id that is associated with the cc_transaction, which is one if its fields anyways) CHECK!
        $transaction_row['order_id'];
        
        // 7.) $over_paid_CCs (MAY IMPLEMENT LATER, NOW TOLD TO LEFT BLANK)
        
        // 8.) true.
        
        //Now we may call the actual function for processing, it should return the tip amount
        ////-> as defined: process_capture_or_tip($location_info, $integration_info, $process_info, $transaction_info, $apply_tip, $overpaid_CCs, true);
        
        
        /*
        debugLog("Current Transaction Gateway Pass: ");
        debugLog("Checking input prior to calling gateway: ");
        debugLog("Location Info: title->" . $location_info['title'] . "  array size: " . count($location_info));
        debugLog("Integrations_info: 1:" . $integration_info['integration1'] . "   2:" .$integration_info['integration2'] . '  etc.');
        debugLog("Process_Info size: " . count($process_info));
        debugLog("Transaction Row:  id->" . $transaction_row['id']);
        debugLog("Tip amount: " . $tip_amount);
        debugLog("Transactions order-id: " . $transaction_row['order_id']);
        */
        
        global $resp;
		global $error_encountered;
		global $error_code;
        
        $resp = "---GATEWAY RESPONSE: ";
        $returnValue = process_capture_or_tip($location_info, $integration_info, $process_info, $transaction_row, $tip_amount, array(), true);
        $resp = str_replace("<tr>", "", $resp);
        $resp = str_replace("<td align='center'>", "", $resp);
        $resp = str_replace("</td>", "", $resp);
        $resp = str_replace("</tr>", "", $resp);
        
        
        //error_log($resp);
        
        //Error
        if($returnValue === '' || is_null($returnValue) || !isset($returnValue)){
            return $resp; 
        }else{
            return true;
        }
        
        
        
        
        /*
        debugLog("Returned value from process_capture_or_tip: " . $returnValue);
        debugLog($resp);
        debugLog("error_encountered: " . $error_encountered);
        debugLog("error_code: " . $error_code);
        debugLog("-----------------------");
        */
    }
    
    /*
    function debugLog($string){
    
        if($isLocalServer){
            if(!file_exists("/poslavu-local/local/debugLogs")){
                mkdir("/poslavu-local/local/debugLogs");
            }
            $fp = fopen("/poslavu-local/local/debugLogs/debug_log", "a");
            fwrite($fp, date('Y-m-d H:i:s') . " debug log:" . $string . "\n");
            fclose($fp);
        }
        else{
            if(!file_exists("/home/poslavu/private_html/debugLogs/brian_d_debug_log")){
                return false;
            }
            $fp = fopen("/home/poslavu/private_html/debugLogs/brian_d_debug_log", "a");
            fwrite($fp, date('Y-m-d H:i:s') . " debug log:" . $string . "\n");
            fclose($fp);
        }

    }
    */
    
	function getCCTransactionRowsByIds($cc_transaction_ids){

    	$transactionIDs = array();
    	foreach($cc_transaction_ids as $currID){
        	$transactionIDs[] = ConnectionHub::getConn('rest')->escapeString( $currID );
    	}
        //Now we get all the cc_transaction rows.
        $queryIDs = "('" . implode("','", $transactionIDs) . "')";

        $transactionRows = array();
        $result = lavu_query("SELECT * FROM `cc_transactions` WHERE `id` IN $queryIDs");
        if(!$result){
            error_log("MYSQL Error -f3t4s4s- error: " . lavu_dberror());
            return false;
        }
        else{
            while($currRow = mysqli_fetch_assoc($result)){
                $transactionRows[$currRow['id']] = $currRow; 
            }
        }
        return $transactionRows;
	}
?>
