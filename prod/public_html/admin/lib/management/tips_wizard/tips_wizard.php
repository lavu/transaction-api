<?php 

    //ini_set('display_errors', 1);
    
    require_once(dirname(__FILE__) . '/../../info_inc.php');
    require_once(dirname(__FILE__) . '/tips_wizard_data.php');
    require_once(dirname(__FILE__) . '/tips_wizard_view.php');
    require_once(dirname(__FILE__) . '/tips_wizard_network_and_ajax.php');
    require_once(dirname(__FILE__) . '/js_user_components.php');
    require_once(dirname(__FILE__) . '/tips_wizard_totals_page.php');
    require_once(dirname(__FILE__) . '/tips_wizard_server_recon_page.php');
    require_once(dirname(__FILE__) . '/tips_wizard_finished_page.php');
    require_once(dirname(__FILE__) . '/tips_wizard_tip_out_page.php');

    global $All_SERVER_CC_TRANSACTIONS;
    // DATA
    $location_row;//Is retrieved in tips_wizard_data. = getLocationRowAsArray($got_loc_id);//$got_loc_id is `loc_id` defined in info_inc.php
    $timezone = $location_row['timezone'];//w
    $numberOfDecimalPlaces = $location_row['disable_decimal'];
    $datetime_now = localize_datetime(date('Y-m-d H:i:s'), $timezone);
    $fiscalDayStart = getFiscalDayStartGivenDatetime($datetime_now);
    $yesterdayFiscalDayStart = getDateTimeFromDateTime($fiscalDayStart, '-1 day');
    $tillInWithoutTillOutRow = getLastTillInWithoutTillOutTime_falseIfEmpty();
    $tillRowToTillOutFrom = $tillInWithoutTillOutRow ? json_encode($tillInWithoutTillOutRow) : false; //last cash_data row where till in exists but till out doesnt. 
    $totalTillInAmount = $tillInWithoutTillOutRow ? getTotalAmountFromTillInRow($tillInWithoutTillOutRow) : -1;
    $tillID = $tillInWithoutTillOutRow ? $tillInWithoutTillOutRow['id'] : "false";//Goes to JS, need "'s to be interpreted.
    $denomination_js_data = getDenominations_JSArrays();
    $js_denominations = '{"bills":' . $denomination_js_data['bills'] . ',"coins":' . $denomination_js_data['coins'] . '}';
    $serverID = getServerID();
    $cc_transactionsWithCreditCardForToday = 
            getServerCreditCardTransactionsAndTableForFiscalDate($serverID, $fiscalDayStart, $onlyWhereNotEntered = false);
    $jsonCC_Transaction_Data = transformQueryResultsForJSONDelivery($cc_transactionsWithCreditCardForToday);
    $totalSales = getTotalSalesForFiscalDate($serverID, $fiscalDayStart);
    $totalCashSales = getTotalSalesForFiscalDate($serverID, $fiscalDayStart, "Cash");

    //HERE WE NEED TO GIVE A LEFT LIMIT FOR SHIFT END OR FOR SHIFT START OR BOTH.
    $clockPunchRow = getClockPunchRowSignedInOrLast(getServerID(), $location_row['id'], $yesterdayFiscalDayStart);
    $server_clock_punch_id = $clockPunchRow['id'] ? $clockPunchRow['id'] : 0;


    $tipoutMappings = getTipoutDataToOthersMappingForServer(getServerID(), $clockPunchRow, $location_row);
    $totalTipouts = getNumberOfCoWorkersInTipoutBins($tipoutMappings);
    $tipoutMappingsJSON = json_encode($tipoutMappings);
    
    $askForTotalOrJustCashTips = getWhetherToDisplayInputForTotalCCAndCashOrJustCashTips();
    $serverDateTime_now = date('Y-m-d H:i:s');

    //Put these javascript functions into the DOM.
    //They are defined in tips_wizard_data.php.  Also they use state variables so should be placed after all other 'DATA' but before 'VEIW'
    echo getAjaxPostFunctions();
    echo getServerReconCalcSettingsJavascriptFunc();//function calculateServerDepositePayoutAmount(cash_sales, cc_sales, cash_tips, cc_tips, till_in, till_out){...}

    // VIEW
    $display  = '</center>';
    $display .= getOpenFullBackgroundDiv('full_background_div_id');
    $display .= getContainingDiv("containing_div_id");
    $display .= addPagingComponentToContainerDiv("containing_div_id", $jsonCC_Transaction_Data, 
                    $location_row['decimal_char'], $location_row['monitary_symbol'], 
                    $numberOfDecimalPlaces, $tillRowToTillOutFrom, $js_denominations, $totalSales,
                    $totalCashSales, $totalTillInAmount, $server_clock_punch_id, $tillID, 
                    $tipoutMappingsJSON, $askForTotalOrJustCashTips, 
                    $totalTipouts,$serverID,$location_row['id'], $datetime_now, 
                    $serverDateTime_now, $fiscalDayStart, $data_name, $sec_hash_for_printing);
    $display .= addNumpad($location_row['decimal_char']);
    $display .= getCloseFullBackgroundDiv();
    
    //Freeze the UIWebView from scrolling if possible.
    $isBlockScrollingEnabled = is_array($available_DOcmds) && in_array('set_scrollable', $available_DOcmds);
    if($isBlockScrollingEnabled){
        echo "<script> setTimeout( function(){ window.location = '_DO:cmd=set_scrollable&set_to=0'; }, 400 ); </script>";
    }
    
?>