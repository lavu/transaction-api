<?php


    
    
    echo getNumericPadJS();

    function getNumericPadJS(){
    ?>
      <style type='text/css'>
        .numericPadButtonStyle{
            position:absolute;
            border:2px solid black;
            border-radius:0px;
            transition: background-color ease-out 0.25s;
            -webkit-transition: background-color ease-out 0.25s;
            -webkit-tap-highlight-color:rgba(0,0,0,0.0);
            background-color:#ebedf1;
            color:#b3b8c8;
        }
        .numericPadButtonPressedStyle{
            position:absolute;
            border:2px solid black;
            border-radius:0px;
            background-color:#757678;
            color:#888;
            -webkit-tap-highlight-color:rgba(0,0,0,0.0);
        }
        .numericPadButtonStyleRight{
            position:absolute;
            border:2px solid black;
            border-radius:0px;
            transition: background-color ease-out 0.25s;
            -webkit-transition: background-color ease-out 0.25s;
            -webkit-tap-highlight-color:rgba(0,0,0,0.0);
            background-color:#b7bac4;
            color:#f8f8f8;
        }
        .numericPadButtonStyleRightPressed{
            position:absolute;
            border:2px solid black;
            border-radius:0px;
            background-color:#5B5D62;
            color:#888;
            -webkit-tap-highlight-color:rgba(0,0,0,0.0);
        }
        .backBoardDivStyle{
            background-color: black;
        }
      </style>
        
      <script type='text/javascript'>
        var key_pressed_callback_external_function
        var key_being_pressed = false;
        function initializeNumpad(left, top, width, height, decimal_char, key_pressed_callback){
        
            key_pressed_callback_external_function = key_pressed_callback;
            var columnWidth  = width/4;
            var rowHeight = height/4;

            //Set up the background div.
            var backboardDiv = document.createElement('div');
            backboardDiv.className = 'backBoardDivStyle';
            backboardDiv.style.width  = width  + 'px';
            backboardDiv.style.height = height + 'px';
            backboardDiv.style.left   = left + 'px';
            backboardDiv.style.top    = top + 'px';
            backboardDiv.style.position = 'absolute';
            //We create the key/buttons.
            var keys = []; //Note keys[10] is '.', 11 => backspace, 12 => enter.
            for(var i = 0; i < 13; i++){
                keys[i] = document.createElement('div');
                if(i == 11 || i == 12){
                    keys[i].className = 'numericPadButtonStyleRight';
                }
                else{
                    keys[i].className = 'numericPadButtonStyle';
                }
                
                keys[i].index = i;
                keys[i].addEventListener('touchstart', function(){
                    if(key_being_pressed){
                        return;
                    }
                    key_being_pressed = true;
                    numpad_key_pressed(this.index);
                    if(this.index == 11 || this.index == 12){
                        this.className = 'numericPadButtonStyleRightPressed'; 
                    }
                    else{
                        this.className = 'numericPadButtonPressedStyle'; 
                    }
                    
                });
                keys[i].addEventListener('touchend', function() { 
                    key_being_pressed = false;
                    if(this.index == 11 || this.index == 12){
                        this.className = 'numericPadButtonStyleRight';
                    }
                    else{
                        this.className = 'numericPadButtonStyle';
                    }
                     
                });
                
                //keys[i].onmousedown = function(){  numpad_key_pressed(this.index); };
            }
            
            position_keys(keys, columnWidth, rowHeight);
            add_text_to_keys(keys, 36, decimal_char);
            
            for(i = 0; i < 13; i++){
                backboardDiv.appendChild(keys[i]);
            }
            
            return backboardDiv;
        }
        
        function position_keys(keys, columnWidth, rowHeight){
            columnWidth = columnWidth;
            rowHeight = rowHeight;
            //Position the keys
            for(var i = 1; i < 10; i++){
                col = (i-1) % 3;
                row = 2 - Math.floor( ((i-1) / 3) );
                keys[i].style.width = columnWidth;
                keys[i].style.height = rowHeight;
                keys[i].style.left = col * columnWidth;
                keys[i].style.top =  row * rowHeight;
            }
            keys[0].style.left = 0;
            keys[0].style.top = 3 * rowHeight;
            keys[0].style.width = columnWidth * 2;
            keys[0].style.height = rowHeight;
            keys[10].style.left = 2 * columnWidth;
            keys[10].style.top = 3 * rowHeight + 2;
            keys[10].style.width = columnWidth;
            keys[10].style.height = rowHeight - 2;
            keys[11].style.left = 3 * columnWidth;
            keys[11].style.top = 0;
            keys[11].style.width = columnWidth;
            keys[11].style.height = 2*rowHeight;
            keys[12].style.left = 3 * columnWidth;
            keys[12].style.top = 2*rowHeight + 2;
            keys[12].style.width = columnWidth;
            keys[12].style.height = 2*rowHeight - 2;
        }
        
        function add_text_to_keys(keys, fontSizePX, decimal_char){
        
            if(!fontSizePX){
                fontSizePX = '36';
            }
            backspaceFontSize = 0.75 * fontSizePX;
            returnKeyFontSize = 1.15 * fontSizePX;
            
            fontSizePX;
            backspaceFontSize;
            returnKeyFontSize;
            
            for(i = 0; i < 13; i++){
                var currTextDiv = document.createElement('div');
                
                if( i < 10){
                    currTextDiv.innerHTML = i;
                    keys[i].style.fontSize = fontSizePX + 'px';
                }
                else if( i == 10 ){
                    currTextDiv.innerHTML = decimal_char;
                    keys[i].style.fontSize = fontSizePX + 'px';
                }
                else if( i == 11 ){
                    currTextDiv.innerHTML = '&#9003;';
                    keys[i].style.fontSize = backspaceFontSize + 'px';
                }
                else if( i == 12 ){
                    currTextDiv.innerHTML = '&#8629;';
                    keys[i].style.fontSize = returnKeyFontSize + 'px';
                }

                currTextDiv.style.position = 'inline';

                var btn_w = keys[i].style.width;
                var btn_h = keys[i].style.height;

                keys[i].style.textAlign = 'center';
                currTextDiv.style.display = 'inline-block';

                var emptyVAlignDiv = document.createElement('div');
                emptyVAlignDiv.style.display = 'inline-block';
                emptyVAlignDiv.style.height = '100%';
                emptyVAlignDiv.style.width = '0';
                emptyVAlignDiv.style.verticalAlign = 'middle';
                
                keys[i].appendChild(emptyVAlignDiv);
                keys[i].appendChild(currTextDiv);
            }
        }
        
        function numpad_key_pressed(keyIndex){
            //alert('pressed' + keyIndex);
                 if(keyIndex <  10){ key_pressed_callback_external_function(keyIndex); }
            else if(keyIndex == 10){ key_pressed_callback_external_function('.'); }
            else if(keyIndex == 11){ key_pressed_callback_external_function('delete'); }
            else if(keyIndex == 12){ key_pressed_callback_external_function('enter'); }
        }
        



        function addUpDownScrollButtonsToDiv(div, upCallback, downCallback, classNameUnPressed, classNamePressed, width){
            
            //The up button
            var scrollUpButton = document.createElement('div');
            scrollUpButton.id = 'scroll_up_button_div_id';
            scrollUpButton.className = classNameUnPressed;
            //scrollUpButton.style.border = '1px solid black';
            scrollUpButton.style.height = Math.floor(div.offsetHeight/2);
            scrollUpButton.style.top = div.offsetTop;
            scrollUpButton.style.position = 'absolute';
            scrollUpButton.style.display = 'block';
            scrollUpButton.style.left = (div.offsetLeft + div.offsetWidth - width) + 'px';
            scrollUpButton.style.width = width + 'px';
            scrollUpButton.upPressed = function(){
                if(this.isPressed){ return; }
                this.isPressed = true;
                this.className = classNamePressed;
                upCallback();
            };
            scrollUpButton.upReleased = function(){
                this.isPressed = false;
                this.className = classNameUnPressed;
            };
            scrollUpButton.addEventListener('touchstart', scrollUpButton.upPressed);
            scrollUpButton.addEventListener('touchend', scrollUpButton.upReleased);
            
            var scrollUpButtonInnerImage = document.createElement('div');
            scrollUpButtonInnerImage.style.position = 'relative';
            scrollUpButtonInnerImage.style.top = '-6px';
            scrollUpButtonInnerImage.style.left = '0px';
            scrollUpButtonInnerImage.style.fontSize = '32px';
            scrollUpButtonInnerImage.innerHTML = '&#8593;';
            scrollUpButton.appendChild(scrollUpButtonInnerImage);
             
            
            
            //The down button
            var scrollDownButton = document.createElement('div');
            scrollDownButton.id = 'scroll_down_button_div_id';
            scrollDownButton.className = classNameUnPressed;
            //scrollDownButton.style.border = '1px solid black';
            scrollDownButton.style.height = Math.floor(div.offsetHeight/2) - 2;
            scrollDownButton.style.top = div.offsetTop + div.offsetHeight/2;
            scrollDownButton.style.position = 'absolute';
            scrollDownButton.style.display = 'block';
            scrollDownButton.style.left = (div.offsetLeft + div.offsetWidth - width) + 'px';
            scrollDownButton.style.width = width + 'px';
            scrollDownButton.downPressed = function(){
                if(this.isPressed){ return; }
                this.isPressed = true;
                this.className = classNamePressed;
                downCallback();                                               
            };
            scrollDownButton.downReleased = function(){
                this.isPressed = false;
                this.className = classNameUnPressed;
            };
            scrollDownButton.addEventListener('touchstart', scrollDownButton.downPressed);
            scrollDownButton.addEventListener('touchend', scrollDownButton.downReleased);
            
            var scrollDownButtonInnerImage = document.createElement('div');
            scrollDownButtonInnerImage.style.left = '0px';
            scrollDownButtonInnerImage.style.position = 'relative';
            scrollDownButtonInnerImage.style.fontSize = '32px';
            scrollDownButtonInnerImage.innerHTML = '&#8595';
            scrollDownButtonInnerImage.style.top = '76px';
            scrollDownButton.appendChild(scrollDownButtonInnerImage);
            
            div.parentNode.appendChild(scrollUpButton);
            div.parentNode.appendChild(scrollDownButton);
        }
      </script>
    
    <?php
    }
?>