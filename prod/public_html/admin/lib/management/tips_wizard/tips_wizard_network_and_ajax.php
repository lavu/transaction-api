<?php

    require_once(dirname(__FILE__) . '/../../info_inc.php');
    require_once(dirname(__FILE__) . '/tips_wizard_data.php');
    
    //Local Server vs. Admin stuff...
    $isLocalServer = $_SERVER['DOCUMENT_ROOT'] == '/poslavu-local';
    
    //Global Vars
    $web_tree_path;
    $thisPageURL;
    
    if($isLocalServer){
        $web_tree_path = substr( __FILE__, strlen( $_SERVER[ 'DOCUMENT_ROOT' ] ) );
        
        $encoding = "";
        if($_SERVER['SERVER_PORT'] == 80 ){
			$encoding = "http";
		} else if ($_SERVER['SERVER_PORT'] == 443 ){
			$encoding = "https";
		}
		$thisPageURL = $encoding . "://" . $_SERVER['SERVER_NAME'] . $web_tree_path;
	}
	else {
		$web_tree_path = substr( __FILE__, strlen( $_SERVER[ 'DOCUMENT_ROOT' ] ) );
		$thisPageURL = "https://admin.poslavu.com$web_tree_path";
	}
	
	
	// Ajax Server Responders
	
	//<>~- - - -  Now tips should not be updated by the server_tips, rather only when loading to the gateway.
	/*
    if(!empty($_POST['handle_ajax_tip_update'])){
        if( empty($_POST['transaction_id']) ||
            empty($_POST['server_id'])      ||
            !isset($_POST['tip_amount'])    ||
            !isset($_POST['updated_index']) || //The index of the receipt from the original query, used to know which to update on async callback.
            empty($_POST['cc'])){
            echo '{"status":"failed","reason":"missing_post_info"}';
            exit;
        }
        if( sha1( $_POST['cc'].'B3AN3S' ) != $_POST['sec_pass'] ){
            echo '{"status":"failed","reason":"bad_sec_key"}';
            exit;
        }
        
        $rowsAffected = updateCCTransactionTipAmountForCC_TransIDWithServer(
            $_POST['transaction_id'], 
            $_POST['server_id'],
            $_POST['tip_amount']);      
        $tip_amount = $_POST['tip_amount'];
        $updatedIndex = $_POST['updated_index'];
        $echoStr = '';
        if($rowsAffected == 1 || ($rowsAffected == 0 && lavu_dberror() == '')){
            $echoStr = "{\"status\":\"success\",\"tip_amount\":\"$tip_amount\",\"updated_index\":\"$updatedIndex\"}";
        }
        else{
            $echoStr = "{\"status\":\"failed\",\"reason\":\"no_rows_affected\"}";
        }
        echo $echoStr;
        exit;
    }
    */
    
    if(!empty($_POST['handle_ajax_clock_out'])){

        //$punch_id = getvar("punch_id","");
        if(!isset($_POST['punch_id'])){
            echo "{\"status\":\"failed\", \"reason\":\"missing_punch_id\"}";
            exit;
        }
        if(empty($_POST['cc'])){
            echo "{\"status\":\"failed\", \"reason\":\"no_dataname\"}";
            exit;
        }
        if( sha1( $_POST['cc'].'B3AN3S' ) != $_POST['sec_pass']){
            echo "{\"status\":\"failed\", \"reason\":\"bad_sec_pass\"}";
            exit;
        }
        
        $punch_query = lavu_query("select * from `clock_punches` where `id`='[1]'",$_POST['punch_id']);
		if(mysqli_num_rows($punch_query))
		{
			$punch_read = mysqli_fetch_assoc($punch_query);
			$set_time_in = $punch_read['time'];
			
			//Now we are using the device time
			//$set_time_out = localize_datetime(date("Y-m-d H:i:s"), $location_row['timezone']);
			$localizedDateTime = localize_datetime(date("Y-m-d H:i:s"), $location_row['timezone']);
			$set_time_out = !empty($_POST['device_time']) ? $_POST['device_time'] : $localizedDateTime;
			
			
			$server_name = $punch_read['server'];
			$set_hours = number_format(get_hours_difference($set_time_in, $set_time_out),2,".","");
			$result = lavu_query("update `clock_punches` set `time_out`='[1]',`hours`='[2]',`punched_out`='1' where `id`='[3]' and `time_out`=''",$set_time_out,$set_hours,$_POST['punch_id']);
			if(!$result){
    			echo "{\"status\":\"failed\", \"reason\":\"query_failed\"}";
    			exit;
			}
			else if(!ConnectionHub::getConn('rest')->affectedRows()){
    			echo "{\"status\":\"failed\",\"reason\":\"already_clocked_out\"}";
    			exit;
			}
			else{
			    //We get the content of the updated row.
			    $selectResult = lavu_query("select * from `clock_punches` where `id`='[1]'",$_POST['punch_id']);
			    $thePunchRowArr;
			    if(!$selectResult){ error_log("MYSQL ERROR -asdjfha44- in tips_wizard_network... error:" . lavu_dberror()); }
			    else if(mysqli_num_rows($selectResult)){
    			    $thePunchRowArr = mysqli_fetch_assoc($selectResult);
			    }
			    else{
    			    $thePunchRowArr = array();
			    }
			    $thePunchRowStr = json_encode($thePunchRowArr);
			    $thePunchRowStr = str_replace('"', '\"', $thePunchRowStr);
    			echo "{\"status\":\"success\",\"punch_row\":\"".$thePunchRowStr."\"}";
    			exit;
			}
		}
		else{
    		echo "{\"status\":\"failed\",\"reason\":\"already_clocked_out\"}";
    		exit;
		}
    }
    if(!empty($_POST['handle_final_server_submission'])){

        if( sha1($_POST['cc'] . 'B3AN3S') != $_POST['sec_pass']){
            echo "{\"status\":\"failed\",\"reason\":\"bad_sec_key\"}";
            exit;
        }

        //Parts for till out:
        //    Value 4 contains bill reconciliations the '/' delimited to the left of ':'
        //        are the amounts given to the server, the right are tilled out by server.
        //    Value 5 are the coins given to the server : coins given back by server.
        //    Value 3 is the till out time(without date apparently).
        //    Value is of the format: amountGivenToServer:amountInDrawer
        // cash_data fields to update, by indices.   
        // 'posted'      till_out_total. Contains the till out total with the correct decimal length.
        //                    ->goes to 2nd half of `value`.
        // 'dynamic'     check_out_time in form: 'H:i:s'
        // 'posted'      till_out_bills.  ',' delimited list of how many bills are checking in.
        //                    ->goes to 2nd half of `value4`.
        // 'posted'      till_out_coins.  ',' delimited list of how many coins are checking in.
        // 'posted'      cash_data_id.  The id of the cash_data row that we will update.
        // 'posted'      number_of_decimal_places
        if( empty($_POST['cc']) ){
            echo "{\"status\":\"failed\",\"reason\":\"missing_post_cc\"}";       
            exit;
        }    
        
        $returnArr = array();
        if( !empty($_POST['is_tilling_out']) ){
            if( !isset($_POST['till_in_total'])  ||
                !isset($_POST['till_out_total']) ||
                !isset($_POST['till_out_bills']) ||
                !isset($_POST['till_out_coins']) ||
                !isset($_POST['till_cash_data_id'])   ||
                !isset($_POST['till_out_bill_denominations']) ||
                !isset($_POST['till_out_coin_denominations']) ||
                !isset($_POST['number_of_decimal_places'] ) ){
                    echo "{\"status\":\"failed\",\"reason\":\"missing_till_argument\"}";       
                    exit;
            }
            //submitServerCashReconciliation($tillOutTotal, $tillOutBills, $tillOutCoins, $cashDataID)
            // 'success' or 'failed' is returned.
    //$tillOutResponse = submitServerCashReconciliation($_POST['till_out_total'], $_POST['till_out_bills'], $_POST['till_out_coins'], $_POST['till_cash_data_id']);
            $returnArr['till_out_affected_rows'] . $tillOutResponse;
        }
        foreach($_POST as $key=>$val){
            $_POST[$key] = rawurldecode($val);
        }
        $insertID = organizeAndInsertServerTipsCashData($_POST);

        $returnArr['cash_data_submission_row_id'] = $insertID;
        $returnArr['status'] = 'success';
        
        
        /***  TODO   HERE   WE   PASS   THE   TIP   DATA   INTO   THE   GATEWAY  ***/
        require_once(dirname(__FILE__).'/tips_wizard_gateway.php');
        $receiptData = json_decode($_POST['receipt_data'],$isLocalServer);
        
        $error_arr = send_all_cc_transactions_to_gateway($data_name, $location_row, $receiptData);
        
        $returnArr['gateway_errors'] = $error_arr;
        $returnStr = json_encode($returnArr);
        echo $returnStr;
        exit;
    }

    
    function getAjaxPostFunctions(){
        global $thisPageURL;
        $dataname = $_REQUEST['cc'];
        $sec_pass = sha1($_REQUEST['cc'] . 'B3AN3S');
        
        ob_start();
    ?>
        <script type='text/javascript'>

            function submitFinalDataToServer(submitObject, response_callback_function){
                var xmlhttp;
                if(window.XMLHttpRequest){
                    xmlhttp = new XMLHttpRequest();
                }else{
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function(){
                    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                        response_callback_function(xmlhttp.responseText);
                        //alert('callback: ' + xmlhttp.responseText);
                    }
                }
                var d = new Date();
                var ese_url = '<?php echo $thisPageURL; ?>' + '?ca=' + d.getTime();
                xmlhttp.open('POST', ese_url, true);
                xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                var post_params = 'handle_final_server_submission=1';
                post_params += '&cc=<?php echo $dataname; ?>';
                post_params += '&sec_pass=<?php echo $sec_pass; ?>';
                for (var key in submitObject){
                    post_params += '&' + key + '=' + encodeURIComponent(submitObject[key]);
                }
                
                //alert("Hijacking: " + JSON.stringify(post_params));
                xmlhttp.send(post_params);
            }
            
            var tipPostedWithServerResponse = [];
            function postTipAmount(transaction_id, server_id, tip_amount, updated_index, response_callback_function){
    
                tipPostedWithServerResponse[updated_index] = 'sent';
                var xmlhttp;
                if (window.XMLHttpRequest){
                    xmlhttp = new XMLHttpRequest();
                }else{
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function(){
                    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                        tipPostedWithServerResponse[updated_index] = 'received';
                        response_callback_function(xmlhttp.responseText);
                    }
                }
                var d = new Date();
                var post_tip_amount_url = '<? echo $thisPageURL; ?>' + '?ca=' + d.getTime();
       
                xmlhttp.open('POST', post_tip_amount_url, true);
                xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                
                var post_params  = '';
                post_params += 'handle_ajax_tip_update=1';
                post_params += '&transaction_id=' + transaction_id;
                post_params += '&server_id='     + server_id;
                post_params += '&tip_amount='    + tip_amount;
                post_params += '&updated_index=' + updated_index;
                post_params += '&sec_pass='+'<?php echo $sec_pass; ?>';
                post_params += '&cc=<?php echo $dataname; ?>';
                
               
                xmlhttp.send(post_params);
            }
            
            
            function postClockOut(punch_id, response_callback_function, deviceTime){
                var xmlhttp;
                if(window.XMLHttpRequest){
                    xmlhttp = new XMLHttpRequest();
                }else{
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function(){
                    if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
                        response_callback_function(xmlhttp.responseText);
                    }
                }
                var d = new Date();
                var urls = '<?php echo $thisPageURL;?>' + '?ca=' + d.getTime();
                xmlhttp.open('POST', urls, true);
                xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                var post_params = '';
                post_params += 'handle_ajax_clock_out=1';
                post_params += '&cc=<?php echo $dataname;?>';
                post_params += '&punch_id='  + punch_id;
                post_params += '&sec_pass='  + '<?php echo $sec_pass;?>';
                post_params += '&random=' + d.getTime();
                post_params += '&device_time=' + encodeURIComponent(deviceTime);
                
                xmlhttp.send(post_params);
            }
            
            
        </script>
    <?php
        return ob_get_clean();
    }

    
    function get_hours_difference($t1, $t2){
		//echo $t1 . "<br>" . $t2 . "<br>------------";
		$clockInArray = explode(" ", $t1);
		$clockInDate = explode("-", $clockInArray[0]);
		$clockInTime = explode(":", $clockInArray[1]);
		$inStamp = mktime($clockInTime[0], $clockInTime[1], 0, $clockInDate[1], $clockInDate[2], $clockInDate[0]);

		$clockOutArray = explode(" ", $t2);
		$clockOutDate = explode("-", $clockOutArray[0]);
		$clockOutTime = explode(":", $clockOutArray[1]);
		$outStamp = mktime($clockOutTime[0], $clockOutTime[1], 0, $clockOutDate[1], $clockOutDate[2], $clockOutDate[0]);

		$hours = (($outStamp - $inStamp) / 3600);
		return $hours;
	}
?>
