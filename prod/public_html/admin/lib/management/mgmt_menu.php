<?php

	$display = "";
	$areas = array();

	if ($is_mgmt_user) {

		$areas[] = array(
			speak("Shifts"),
			"inapp_management.php?mode=shifts&".$fwd_vars,
			'shifts',
			'images/shifts-icon.svg'
		);
		$areas[] = array(
			speak("Clock Punches"),
			"inapp_management.php?mode=clock_punches&".$fwd_vars,
			'clock_punches',
			'images/clock_punch-icon.svg'
		);
		$areas[] = array(
			speak("Transfer Orders"),
			"inapp_management.php?mode=view_tables&".$fwd_vars,
			'transfer_orders',
			'images/transfer_orders-icon.svg'
		);
		$areas[] = array(
			speak("Adjust Inventory"),
			"inapp_management.php?mode=adjust_inventory&".$fwd_vars,
			'adjust_inventory',
			'images/adjust_inventory-icon.svg'
		);
		$areas[] = array(
			speak("Cash Reconciliation"),
			"inapp_management.php?mode=cash_reconciliation&".$fwd_vars,
			'cash_reconciliation',
			'images/cash_reconciliation-icon.svg'
		);
		$areas[] = array(
			speak("Adjust Lavu Card"),
			"inapp_management.php?mode=lavu_loyalty&".$fwd_vars,
			'lavu_loyalty',
			'images/lavu_loyalty-icon.svg'
		);
}
	if(count($areas) == 0 && $is_access_transfer_table) {
		$areas[] = array(
				speak("Transfer Orders"),
				"inapp_management.php?mode=view_tables&".$fwd_vars,
				'transfer_orders',
				'images/transfer_orders-icon.svg'
		);
	}

	$areas[] = array(
		speak("Server Reconciliation"),
		"inapp_management.php?mode=server_reconciliation&".$fwd_vars,
		'server_reconciliation',
		'images/server_reconciliation-icon.svg'
	);
	
	//Enabling only for my test account for now...
	$enable_for = array(
		"demo_brian_d",
		"demo_lavu_ls_l",
		"demo_lavu_lann",
		"pomme_bistro",
		"DEV",
		"test_pizza_sho",
		"super_mario_pi",
		"cafe_nate",
		"hardware__hard",
		"the_rackhouse_1",
		"little_spoon_e",
		"main_street_bi",
		"da_conch_shack",
		"anna_schinske_",
		"demo_pos_revol",
		"joe_jac_inc",
		"punk_pizzeria"
	);
	if (in_array($data_name, $enable_for)) {

		$areas[] = array(
			speak("End of Shift Wizard"),
			"inapp_management.php?mode=server_tips&".$fwd_vars,
			'end_of_shift_wizard',
			'images/end_of_shift_wizard-icon.svg'
		);
	}
	
	// Removed by EV 9/16 per LP-65
	// Removes conditional to enable report for accounts in $enable_for array above
	// if(in_array($data_name, $enable_for)) {
	// 	$areas[] = array(
	// 		"Labor Report",
	// 		"inapp_management.php?mode=labor_report&".$fwd_vars,
	// 		"sales_totals", // use the same class as sales totals
	// 		'images/cash_reconciliation-icon.svg' // use the same icon as cash recon
	// 	);
	// }
	
	
	//Moved by EV 10/31 per LP-65 
	// Adds Labor report to manager accounts only
	if ($is_mgmt_user) {
		
		$areas[] = array(
			speak("Labor Report"),
			"inapp_management.php?mode=labor_report&".$fwd_vars,
			"sales_totals", // use the same class as sales totals
			'images/cash_reconciliation-icon.svg' // use the same icon as cash recon
		);
	}
	
	if ($is_mgmt_user) {

		$areas[] = array(
			speak("Sales Totals"),
			"inapp_management.php?mode=sales_totals&".$fwd_vars,
			'sales_totals',
			'images/sales_totals-icon.svg'
		);
	}
	
	if ($is_mgmt_user) {

		$areas[] = array(
			speak("Manager Log"),
			"inapp_management.php?mode=manager_log&".$fwd_vars,
			'manager_log',
			'images/manager_log-icon.svg'
		);
	}
	
	if ($is_mgmt_user) {
	
		$areas[] = array(
				speak("Vault Management"),
				"inapp_management.php?mode=vault_management&".$fwd_vars,
				'vault_management',
				'images/vault_management-icon.svg'
		);
	}
	
	if ( getWhetherShouldHaveDeliveryInManagementArea() ){

		$areas[] = array(
			speak("Deliveries"),
			"inapp_management.php?mode=deliveries&".$fwd_vars,
			'deliveries',
			'images/deliveries-icon.svg'
		);
	}
	
	if ($lls && $location_info['lls_queue_print_jobs']=="1") {

		$areas[] = array(
			speak("LLS Print Queue"),
			"inapp_management.php?mode=print_queue&".$fwd_vars,
			'lls_print_queue',
			'images/lls_print_queue-icon.svg'
		);
	}

	$gateway = $location_info['gateway'];
	$showing_cca = false;

	if ($is_mgmt_user && $lls && $location_info['integrateCC']=="1" && $location_info['cc_transtype']=="Auth" && !in_array($gateway, array("Authorize.net", "BluePay", "CyberSource", "Elavon", "Magensa"))) {

		$showing_cca = true;
		$areas[] = array(
			speak("Credit Card Authorizations"),
			"inapp_management.php?mode=cc_auths&".$fwd_vars,
			'credit_card_authorizations',
			'images/credit_card_authorizations-icon.svg'
		);
	}
	
	if ($is_mgmt_user && $location_info['allow_front_end_cc_batch']=="1" && !in_array($gateway, array("SailPay", "USAePay", "WorldPay"))) {

		$areas[] = array(
			speak("Settle Credit Card Batch"),
			"inapp_management.php?mode=cc_settle&step=1&".$fwd_vars,
			'settle_credit_card_batch',
			'images/settle_credit_card_batch-icon.svg'
		);
	}
	/*
	$areas[] = array(
		"Tips Search",
		"inapp_management.php?mode=tip_entry&".$fwd_vars,
		"tip_entry",
		'images/end_of_shift_wizard-icon.svg'
	);
	*/
	/*
	$areas[] = array(
		"eConduit",
		"inapp_management.php?mode=econduit&".$fwd_vars,
		"econduit",
		'images/end_of_shift_wizard-icon.svg'
	);
	*/
	
	if($data_name == 'chained_1'    ||
	   $data_name == 'chained_2'    ||
	   $data_name == 'chained_3'    ||
	   $modules->hasModule('management.cash_drop') ){
		if ($is_mgmt_user){
			$areas[] = array(
				speak("Cash Drop"),
				"inapp_management.php?mode=cash_drop&".$fwd_vars,
				"cash_drop",
				'images/cash_drop_icon.svg'
			);
		}
	}
	
	/*
	if($data_name == 'chained_1'    ||
	   $data_name == 'chained_2'    ||
	   $data_name == 'chained_3' ){
		if ($is_mgmt_user){
			$areas[] = array(
				"Cash Drop",
				"inapp_management.php?mode=cash_drop&".$fwd_vars,
				"cash_drop",
				'images/cash_drop_icon.svg'
			);
		}
	}
	*/

	echo <<<HTML
<script type='application/javascript'>
function refreshPage() { 
	window.location = '?mode=".$mode."&".$fwd_vars."'; 
}
</script>
<style><!-- // 240 x 140 (for 2 cols) -->
/*.cell {
	font-family:Arial, sans-serif;
	font-size:12px;
	width:160px;
	height:125px;
	text-align:center;
	font-size:24px;
	font-weight:bold;
	color:#555555;
}*/

body {
	font-family: arial, sans-serif;
}

a {
	display: inline-block;
	text-decoration: inherit;
	color: inherit;
}

.container {
	position: absolute;
	margin: 3em 4em;
}

/** TIME  */
.shifts, .clock_punches {
	/*color: #3382A9;*/
	fill: #3382A9;
}

/** MONEY */
.cash_reconciliation, .server_reconciliation, .end_of_shift_wizard, .lavu_loyalty {
	/*color: #AECD37;*/
	fill: #AECD37;
}

/** CREDIT CARD */
.credit_card_authorizations, .settle_credit_card_batch {
	/*color: #5053A2;*/
	fill: #5053A2;
}


/** ADMINISTRATIVE */
.transfer_orders, .deliveries, .adjust_inventory {
	/*color: #0EAE95;*/
	fill: #0EAE95;
}

/** MANAGER */
.sales_totals, .lls_print_queue, .manager_log, .vault_management {
	/*color: #F68E20;*/
	fill: #F68E20;
}

.shifts,
.clock_punches,
.transfer_orders,
.sales_totals,
.cash_reconciliation,
.lavu_loyalty,
.server_reconciliation,
.adjust_inventory,
.end_of_shift_wizard,
.manager_log,
.vault_management,
.deliveries,
.lls_print_queue,
.credit_card_authorizations,
.settle_credit_card_batch,
.cash_drop {
	width: 160px;
	height: 170px;
	margin: 1em 0.4em;
	border-radius: 1em;
	/*background: rgba( 255, 255, 255, 0.9 );*/
	overflow: hidden;
	color: #8b8b8b;
}

.shifts .title,
.clock_punches .title,
.transfer_orders .title,
.sales_totals .title,
.cash_reconciliation .title,
.lavu_loyalty .title,
.server_reconciliation .title,
.adjust_inventory .title,
.end_of_shift_wizard .title,
.manager_log .title,
.vault_management .title,
.deliveries .title,
.lls_print_queue .title,
.credit_card_authorizations .title,
.settle_credit_card_batch .title, 
.cash_drop .title{
	font-weight: bold;
	text-align: center;
	color: inherit;
}

.shifts .icon,
.clock_punches .icon,
.transfer_orders .icon,
.sales_totals .icon,
.cash_reconciliation .icon,
.lavu_loyalty .icon,
.server_reconciliation .icon,
.adjust_inventory .icon,
.end_of_shift_wizard .icon,
.manager_log .icon,
.vault_management .icon,
.deliveries .icon,
.lls_print_queue .icon,
.credit_card_authorizations .icon,
.settle_credit_card_batch .icon,
.cash_drop .icon {
	width: 150px;
	height: 150px;
	margin: 0 auto;
	/*display: table;*/
}

svg {
	width: inherit;
	height: inherit;
}
/*
.shifts .icon{
	background: url(images/shifts-icon.svg) no-repeat;
	background-size: cover;
}
.clock_punches .icon{
	background: url(images/clock_punch-icon.svg) no-repeat;
	background-size: cover;
}
.transfer_orders .icon{
	background: url(images/transfer_orders-icon.svg) no-repeat;
	background-size: cover;
}
.sales_totals .icon{
	background: url(images/sales_totals-icon.svg) no-repeat;
	background-size: cover;
}
.cash_reconciliation .icon{
	background: url(images/cash_reconciliation-icon.svg) no-repeat;
	background-size: cover;
}
.server_reconciliation .icon{
	background: url(images/server_reconciliation-icon.svg) no-repeat;
	background-size: cover;
}
.adjust_inventory .icon{
	background: url(images/adjust_inventory-icon.svg) no-repeat;
	background-size: cover;
}
.end_of_shift_wizard .icon{
	background: url(images/end_of_shift_wizard-icon.svg) no-repeat;
	background-size: cover;
}
.manager_log .icon{
	background: url(images/manager_log-icon.svg) no-repeat;
	background-size: cover;
}
.vault_management .icon{
	background: url(images/vault_management-icon.svg) no-repeat;
	background-size: cover;
}
.deliveries .icon{
	background: url(images/deliveries.svg) no-repeat;
	background-size: cover;
}
.lls_print_queue .icon{
	background: url(images/lls_print_queue-icon.svg) no-repeat;
	background-size: cover;
}
.credit_card_authorizations .icon{
	background: url(images/credit_card_authorizations-icon.svg) no-repeat;
	background-size: cover;
}
.settle_credit_card_batch .icon {
	background: url(images/settle_credit_card_batch-icon.svg) no-repeat;
	background-size: cover;
}
*/
</style>
HTML;

// $bgcolors = array("#ccffcc","#eeffee","#aaffbb","#ddffcc","#aaddbb");

// $row = 1;
// $col = 1;
// $color_index = 0;
// $maxcols = 3;//2;
// $maxi = 15;//8;
// if(count($areas) > $maxi) $maxi = count($areas);
// if($maxi % $maxcols != 0) {
// 	$maxi += $maxcols - ($maxi % $maxcols);
// }

echo '<div class="container">';
for($i=0; $i<count($areas); $i++) {

	$area_title = '';
	$img_src = '';
	$classname = '';
	// $area_click = '';
	if(isset($areas[$i])) {
		$area_title = speak($areas[$i][0]);
		$area_link = $areas[$i][1];
		$classname = $areas[$i][2];
		$img_src = $areas[$i][3];
		// $area_click = " onclick='window.location = \"$area_link\"'";
	} else {
		$area_title = "&nbsp;";
		// $area_click = "";
	}

	// echo "<td class='cell' bgcolor='$current_bgcolor'".$area_click.">$area_title</td>";

	$svg_path = dirname(dirname(__FILE__)).'/'.$img_src;
	$svg = file_get_contents($svg_path);

	echo <<<HTML
	<a href="{$area_link}">
		<div class="{$classname}">
			<div class="icon" style="max-width:149px; max-height:149px; overflow:auto">
				{$svg}
			</div>
			<div class="title">{$area_title}</div>
		</div>
	</a>
HTML;
	// $col++;
	// if($col > $maxcols) { $row++; $col = 1; echo "</tr><tr>"; }

	// $color_index++;
	// if($color_index >= count($bgcolors))
	// 	$color_index = 0;
}
echo '</div>';
echo '<script> setTimeout( function(){ window.location = "_DO:cmd=set_scrollable&set_to=1"; }, 400 ); </script>';

function getWhetherShouldHaveDeliveryInManagementArea(){
	global $location_info;
	$compPackage = $location_info['component_package_code'];
	if($compPackage != 'customer' && $compPackage != 'pizza'){ return false; }
	$result = lavu_query("SELECT * FROM `config` WHERE `setting`='delivery_options'");
	if(!$result){
		error_log("mysql error in " . __FILE__ . "  mysql error:" . lavu_dberror());
		return false;
	}
	if(mysqli_num_rows($result) == 0){ return false; }
	$settingRow = mysqli_fetch_assoc($result);
	$value = $settingRow['value'];
	$parts = explode('|o|', $value);
	return $parts[4] == 'true' ? true : false;
}
