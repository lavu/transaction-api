<?php
$display = "";
$vaultAreas = array();
/*$get_names = lavu_query("SELECT f_name,l_name FROM `users` WHERE `username` = '[1]'", $_REQUEST['username']);
if (mysqli_num_rows($get_names) > 0)
{
	$info = mysqli_fetch_assoc($get_names);
	$fname = $info['f_name'];
	$lname = $info['l_name'];
	$fwd_vars .= "&fname=".$fname."&lname=".$lname;
}*/

if ($is_mgmt_user) {
	
	$vaultAreas[] = array(
			speak("Change Bank"),
			"inapp_management.php?mode=change_bank&".$fwd_vars,
			'change_bank',
			'images/cash_reconciliation-icon.svg'
	);	
	$vaultAreas[] = array(
			speak("Bank Deposit"),
			"inapp_management.php?mode=bank_deposit&".$fwd_vars,
			'bank_deposit',
			'images/cash_reconciliation-icon.svg'
	);
}

$display .= "<style>
body {
	font-family: arial, sans-serif;
}

a {
	display: inline-block;
	text-decoration: inherit;
	color: inherit;
}
.change_bank, .bank_deposit{
	fill: #AECD37;
	width: 160px;
	height: 170px;
	margin: 1em 0.4em;
	border-radius: 1em; 
	overflow: hidden;
	color: #8b8b8b;
}
.change_bank .title, .bank_deposit .title{
	font-weight: bold;
	text-align: center;
	color: inherit;
}		
.change_bank .icon, .bank_deposit .icon {
	width: 150px;
	height: 150px;
	margin: 0 auto; 
	background: url(images/cash_reconciliation-icon.svg) no-repeat;
	background-size: cover;
}
svg {
	width: inherit;
	height: inherit;
}

</style>";
$display .= '<div >';
for($i=0; $i<count($vaultAreas); $i++) {

	$vaultarea_title = '';
	$img_src = '';
	$classname = '';
	// $area_click = '';
	if(isset($vaultAreas[$i])) {
		$vaultarea_title = $vaultAreas[$i][0];
		$vaultarea_link = $vaultAreas[$i][1];
		$classname = $vaultAreas[$i][2];
		$img_src = $vaultAreas[$i][3];
	} else {
		$area_title = "&nbsp;";
		
	}
	$svg_path = dirname(dirname(__FILE__)).'/'.$img_src;
	$svg = file_get_contents($svg_path);

$display .= <<<HTML
	<a href="{$vaultarea_link}">
		<div class="{$classname}">
			<div class="icon" style="max-width:149px; max-height:149px; overflow:auto">
				{$svg}
			</div>
			<div class="title">{$vaultarea_title}</div>
		</div>
	</a>
HTML;
				
}
$display .= '</div>';



?>
