<?php
	require_once("gateway_settings.php");

	function prepareExp($exp) {
		if (!strpos($exp, "/")) {
			$len = strlen($exp);
			$m = substr($exp, 0, ($len - 2));
			$y = substr($exp, -2);
			$exp = $m."/".$y;
		}
		return $exp;
	}

	function storeSessionData($payment_id, $session_data) {
		$dir = "/home/poslavu/logs/company/_MPSHC_";
		if (!is_dir($dir)) {
			if (!mkdir($dir, 0777, true)) {
				return false;
			}
		}

		$success = false;
		$fp = fopen($dir."/".$payment_id, "w");
		if ($fp) {
			$bytes_written = fwrite($fp, json_encode($session_data));
			$success = ($bytes_written > 0);
		}
		fclose($fp);
		return $success;
	}

	function retrieveAndClearSessionData($payment_id) {
		$dir = "/home/poslavu/logs/company/_MPSHC_";
		$fp = fopen($dir."/".$payment_id, "r");
		$session_json = fread($fp, 2048);
		fclose($fp);
		unlink($dir."/".$payment_id);
		if (empty($session_json)) {
			return false;
		}
		return json_decode($session_json, true);
	}

	function post_to_mercury_hc($postvars, $data_name, $loc_id, $transtype, $method, $password="") {
		global $mpshc_server_url;
		global $location_info;
		$poststr = "";
		for ($i = 0; $i < count($postvars); $i++) {
			$field_title = $postvars[$i][0];
			$field_value = $postvars[$i][1];
			$poststr .= "<mer:".$field_title.">".$field_value."</mer:".$field_title.">\n";
		}
		$use_url =  "https://".$mpshc_server_url."/hcws/hcservice.asmx";
		$service = "/hcws/hcservice.asmx?WDSL";
		$soap_action_method = $method;
		$password_xml = "";
		if (strstr($method, "Token")) {
			$use_url = "https://".$mpshc_server_url."/tws/transactionservice.asmx";
			$service = "/tws/transactionservice.asmx";
			$soap_action_method = $method;
			$password_xml = "<mer:Password>".$password."</mer:Password>";
		}
		$start_data = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:mer=\"http://www.mercurypay.com/\">"
			."<soapenv:Header/>\n<soapenv:Body>\n<mer:".$method.">\n<mer:request>";
		$end_data = "</mer:request>".$password_xml."\n</mer:".$method.">\n</soapenv:Body>\n</soapenv:Envelope>";
		$post_data = $start_data.$poststr.$end_data;

		$header = array("POST ".$service." HTTP/1.1", "Host: ".$mpshc_server_url, "Content-Type: text/xml; charset=utf-8", "SOAPAction: \"http://www.mercurypay.com/".$soap_action_method."\""); 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $use_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 45);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		$response = curl_exec($ch);
		curl_close($ch);
		$start_tag = "<".$method."Result>";
		$end_tag = "</".$method."Result>";
		$start_pos = (strpos($response, $start_tag) + strlen($start_tag));
		$end_pos = strpos($response, $end_tag);
		$curl_result = htmlspecialchars_decode(substr($response, $start_pos, ((strlen($response) - $start_pos)) - (strlen($response) - $end_pos)));
		if ($location_info['gateway_debug'] == "1"){
			gateway_debug($data_name, $loc_id, "MPSHC", $transtype."-".$method, "XML", "URL: ".$use_url."\n\nPOST_DATA: ".htmlspecialchars_decode($post_data)."\n\nRESPONSE: ".htmlspecialchars_decode($response)."\n\n\nRESULT: ".$curl_result);
		}
		return $curl_result;
	}

	function post_to_mercury_tws($postvars, $data_name, $loc_id, $transtype, $method, $password) {
		global $mpshc_server_url;
		global $location_info;

		$poststr = "";
		for ($i = 0; $i < count($postvars); $i++) {
			$field_title = $postvars[$i][0];
			$field_value = $postvars[$i][1];
			$poststr .= "<".$field_title.">".$field_value."</".$field_title.">\n";
		}
		$service = "/tws/transactionservice.asmx";
		$start_data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
			." xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soap:Body><"
			.$method." xmlns=\"http://www.mercurypay.com/\">\n<request>";
		$end_data = "</request>\n<password>".$password."</password>\n</".$method.">\n</soap:Body>\n</soap:Envelope>";
		$post_data = $start_data.$poststr.$end_data;
		$debug .= "\n\n".htmlspecialchars_decode($post_data);
		$header = array("POST ".$service." HTTP/1.1", "Host: ".$mpshc_server_url, "Content-Type: text/xml; charset=utf-8", "Content-Length: ".(int)strlen($post_data), "SOAPAction: \"http://www.mercurypay.com/".$method."\"");
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://".$mpshc_server_url."/tws/transactionservice.asmx");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 45);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		$response = curl_exec($ch);
		curl_close($ch);
		$start_tag = "<".$method."Result>";
		$end_tag = "</".$method."Result>";
		if (strstr($response, "<h1>Service Unavailable</h1>")){
			$response = $start_tag."<Status>Service Currently Unavailable (Hosted Checkout ".$method.")</Status><Message>Service Currently Unavailable (Hosted Checkout ".$method.")</Message>".$start_tag;
		}
		$start_pos = (strpos($response, $start_tag) + strlen($start_tag));
		$end_pos = strpos($response, $end_tag);
		$curl_result = htmlspecialchars_decode(substr($response, $start_pos, ((strlen($response) - $start_pos)) - (strlen($response) - $end_pos)));
		if ($location_info['gateway_debug'] == "1"){
			gateway_debug($data_name, $loc_id, "MPSTS", $transtype."-".$method, "XML", "POST_DATA: ".htmlspecialchars_decode($post_data)."\n\nRESPONSE: ".htmlspecialchars_decode($response)."\n\n\nRESULT: ".$curl_result);
		}
		return $curl_result;
	}

	function mpshc_add_tip($transtype, $merchant_id, $password, $data_name, $loc_id, $order_id, $card_amount, $tip_amount, $ref_no, $authcode, $mtoken, $poslavu_version, $register, $server_id, $acq_ref_data, $card_type, $card_desc) {
		global $decimal_places;
		global $location_info;
		if ($transtype == "Adjustment"){
			$method = "CreditAdjustToken";
		}else if ($transtype == "PreAuthCapture"){
			$method = "CreditPreAuthCaptureToken";
		}
		$postvars     = array();
		$postvars[]   = array("MerchantID", $merchant_id); // 494691720
		$postvars[]   = array("PurchaseAmount", $card_amount);
		$postvars[]   = array("RefNo", $ref_no);
		$postvars[]   = array("Invoice", str_replace("-", "", $loc_id.$order_id));
		$postvars[]   = array("Memo", "POSLavu v".$poslavu_version);
		$postvars[]   = array("AuthCode", $authcode);
		$postvars[]   = array("Token", $mtoken);
		$postvars[]   = array("Frequency", "OneTime");
		$postvars[]   = array("TerminalName", $register);
		$postvars[]   = array("OperatorID", $server_id);
		//$postvars[] = array("CardHolderName", "");
		$postvars[]   = array("GratuityAmount", $tip_amount);
		if ($transtype == "PreAuthCapture") {
			$postvars[]      = array("AuthorizeAmount", $card_amount);
			$postvars[]      = array("TaxAmount", "0.00");
			//$postvars[]    = array("CustomerCode", "");
			$postvars[]      = array("AcqRefData", $acq_ref_data);
		}
		$debug            = print_r($postvars, true);
		$curl_result      = post_to_mercury_tws($postvars, $data_name, $loc_id, $transtype, $method, $password);
		$debug           .= "\n\n".$curl_result;
		//$xml            = simplexml_load_string($curl_result);
		$status           = extract_value($curl_result, "Status");
		$message          = extract_value($curl_result, "Message");
		$new_pnref        = extract_value($curl_result, "RefNo");
		$new_authcode     = extract_value($curl_result, "AuthCode");
		$new_ref_data     = extract_value($curl_result, "AcqRefData");
		$new_process_data = extract_value($curl_result, "ProcessData");
		$new_token        = extract_value($curl_result, "Token");

		$log_string       = "MERCURY HOSTED CHECKOUT[--CR--]";
		$log_string      .= "LOCATION: $loc_id - ";
		$log_string      .= "REGISTER: $register - ";
		$log_string      .= "SERVER ID: $server_id - ";
		$log_string      .= "ORDER ID: $order_id - ";
		$log_string      .= "CHECK : $check - ";
		$log_string      .= "AMOUNT: $card_amount - ";
		$log_string      .= "TIP AMOUNT: $tip_amount - ";
		$log_string      .= "TRANSTYPE: $transtype - ";
		$log_string      .= "PNREF: $new_pnref - ";
		$log_string      .= "AUTHCODE: $new_authcode - ";
		$log_string      .= "CARD TYPE: $card_type - ";
		$log_string      .= "LAST FOUR: $card_desc - ";
		$log_string      .= "SWIPED: 0[--CR--]";
		$log_string      .= "STATUS: $status [--CR--]";
		$log_string      .= "MESSAGE: $message [--CR--]";
		write_log_entry("gateway", $data_name, $log_string."\n");
		$approved = "0";
		if ($status == "Approved"){$approved = "1";}
		if ($location_info['gateway_debug'] == "1"){
			gateway_debug($data_name, $loc_id, "MPSHC", $transtype, $approved, $debug);
		}
		if ($approved == "1"){
			$return_string = "1|$new_pnref||$status|$new_authcode||$new_token|||$new_ref_data|".rawurlencode($new_process_data);
		}else{
			$return_string = "0|".$status;
		}
		return $return_string;
	}

	function close_hosted_checkout_batch($company_id, $loc_id, $merchant_id, $password, $data_name, $poslavu_version, $server_id, $register) {
		global $decimal_places;
		global $device_time;
		global $server_name;
		global $location_info;
		$postvars                 = array();
		$postvars[]               = array("MerchantID", $merchant_id); // 494691720
		$postvars[]               = array("Memo", "POSLavu v".$poslavu_version);
		$postvars[]               = array("OperatorID", $server_id);
		$postvars[]               = array("TerminalName", $register);
		$debug                    = print_r($postvars, true);
		$curl_result              = post_to_mercury_tws($postvars, $data_name, $loc_id, "BatchSummary", "BatchSummary", $password);
		$debug                   .= "\n\n".$curl_result;
		//$xml                    = simplexml_load_string($curl_result);
		$status                   = extract_value($curl_result, "Status");
		$message                  = extract_value($curl_result, "Message");
		$batch_no                 = extract_value($curl_result, "BatchNo");
		$batch_item_count         = extract_value($curl_result, "BatchItemCount");
		$net_batch_total          = extract_value($curl_result, "NetBatchTotal");
		$credit_purchase_count    = extract_value($curl_result, "CreditPurchaseCount");
		$credit_purchase_amount   = extract_value($curl_result, "CreditPurchaseAmount");
		$credit_return_count      = extract_value($curl_result, "CreditReturnCount");
		$credit_return_amount     = extract_value($curl_result, "CreditReturnAmount");
		$debit_purchase_count     = extract_value($curl_result, "DebitPurchaseCount");
		$debit_purchase_amount    = extract_value($curl_result, "DebitPurchaseAmount");
		$debit_return_count       = extract_value($curl_result, "DebitReturnCount");
		$debit_return_amount      = extract_value($curl_result, "DebitReturnAmount");

		$log_string               = "MERCURY HOSTED CHECKOUT\nLOCATION: ".$loc_id." - REGISTER: ".$register." - SERVER ID: ".$server_id." - TRANSTYPE: BatchSummary - BATCH NUMBER: ".$batch_no." - STATUS: ".$status."\nMESSAGE: ".$message."\n\n";
		write_log_entry("gateway", $data_name, $log_string);
		$approved = "0";
		if (($status == "Success") || ($status == "Approved")){$approved = "1";}
		if ($location_info['gateway_debug'] == "1"){
			gateway_debug($data_name, $loc_id, "MPSHC", "BatchSummary", $approved, $debug);
		}
		if ($batch_item_count == "") { $batch_item_count = "0"; }
		$batchvars                            = array();
		$batchvars['loc_id']                  = $loc_id;
		$batchvars['hc']                      = "1";
		$batchvars['datetime']                = $device_time;
		$batchvars['gateway']                 = "Mercury";
		$batchvars['user_id']                 = $server_id;
		$batchvars['user_name']               = $server_name;
		$batchvars['batch_no']                = $batch_no;
		$batchvars['batch_item_count']        = $batch_item_count;
		$batchvars['net_batch_total']         = $net_batch_total;
		$batchvars['credit_purchase_count']   = $credit_purchase_count;
		$batchvars['credit_purchase_amount']  = $credit_purchase_amount;
		$batchvars['credit_return_count']     = $credit_return_count;
		$batchvars['credit_return_amount']    = $credit_return_amount;
		$batchvars['debit_purchase_count']    = $debit_purchase_count;
		$batchvars['debit_purchase_amount']   = $debit_purchase_amount;
		$batchvars['debit_return_count']      = $debit_return_count;
		$batchvars['debit_return_amount']     = $debit_return_amount;
		record_batch($data_name, $batchvars);
		if ($approved == "1") {
			$postvars    = array();
			$postvars[]  = array("MerchantID", $merchant_id); // 494691720
			$postvars[]  = array("Memo", "POSLavu v".$poslavu_version);
			$postvars[]  = array("OperatorID", $server_id);
			$postvars[]  = array("TerminalName", $register);
			$postvars[]  = array("BatchNo", $batch_no);
			$postvars[]  = array("BatchItemCount", $batch_item_count);
			$postvars[]  = array("NetBatchTotal", $net_batch_total);
			$postvars[]  = array("CreditPurchaseCount", $credit_purchase_count);
			$postvars[]  = array("CreditPurchaseAmount", $credit_purchase_amount);
			$postvars[]  = array("CreditReturnCount", $credit_return_count);
			$postvars[]  = array("CreditReturnAmount", $credit_return_amount);
			$postvars[]  = array("DebitPurchaseCount", $debit_purchase_count);
			$postvars[]  = array("DebitPurchaseAmount", $debit_purchase_amount);
			$postvars[]  = array("DebitReturnCount", $debit_return_count);
			$postvars[]  = array("DebitReturnAmount", $debit_return_amount);
			$debug       = print_r($postvars, true);
			$curl_result = post_to_mercury_tws($postvars, $data_name, $loc_id, "BatchClose", "BatchClose", $password);
			$debug      .= "\n\n".$curl_result;
			$status      = extract_value($curl_result, "Status");
			$message     = extract_value($curl_result, "Message");

			$log_string  = "MERCURY HOSTED CHECKOUT\nLOCATION: ".$loc_id." - REGISTER: ".$register." - SERVER ID: ".$server_id." - TRANSTYPE: BatchClose - BATCH NUMBER: ".$batch_no." - STATUS: ".$status."\nMESSAGE: ".$message."\n\n";
			write_log_entry("gateway", $data_name, $log_string);
			$approved = "0";
			if (($status == "Success") || ($status == "Approved")){$approved = "1";}
			if ($location_info['gateway_debug'] == "1"){
				gateway_debug($data_name, $loc_id, "MPSHC", "BatchClose", $approved, $debug);
			}
			if ($approved == "1") {
				$batch_total    = number_format((float)$net_batch_total, $decimal_places, ".", "");
				$bda            = array();
				$bda[]          = $batch_item_count;
				$bda[]          = $batch_total;
				$bda[]          = $credit_purchase_count;
				$bda[]          = number_format((float)$credit_purchase_amount, $decimal_places, ".", "");
				$bda[]          = $credit_return_count;
				$bda[]          = number_format((float)$credit_return_amount, $decimal_places, ".", "");
				$bda[]          = $debit_purchase_count;
				$bda[]          = number_format((float)$debit_purchase_amount, $decimal_places, ".", "");
				$bda[]          = $debit_return_count;
				$bda[]          = number_format((float)$debit_return_amount, $decimal_places, ".", "");
				$batch_breakdown = "<table cellspacing='0' cellpadding='3'>
					<tr><td align='center' colspan='2'>HOSTED CHECKOUT BATCH CLOSED</td></tr>
					<tr><td align='right'>Batch Number:</td><td align='left'>$batch_no</td></tr>
					<tr><td align='right'>Batch Item Count:</td><td align='left'>".$bda[0]."</td></tr>
					<tr><td align='right'>Net Batch Total:</td><td align='left'>$ ".$bda[1]."</td></tr>
					<tr><td align='right'>Credit Purchase Count:</td><td align='left'>".$bda[2]."</td></tr>
					<tr><td align='right'>Credit Purchase Amount:</td><td align='left'>$ ".$bda[3]."</td></tr>
					<tr><td align='right'>Credit Return Count:</td><td align='left'>".$bda[4]."</td></tr>
					<tr><td align='right'>Credit Return Amount:</td><td align='left'>$ ".$bda[5]."</td></tr>
					<tr><td align='right'>Debit Purchase Count:</td><td align='left'>".$bda[6]."</td></tr>
					<tr><td align='right'>Debit Purchase Amount:</td><td align='left'>$ ".$bda[7]."</td></tr>
					<tr><td align='right'>Debit Return Count:</td><td align='left'>".$bda[8]."</td></tr>
					<tr><td align='right'>Debit Return Amount:</td><td align='left'>$ ".$bda[9]."</td></tr>
				</table>";
				return "1|Hosted Checkout Batch - ".$status." - ".$message."|".number_format($net_batch_total, $decimal_places, ".", "")."|".$batch_breakdown."|".$batch_no."|".join(":.:", $bda);
			} else {
				return "0|Hosted Checkout Batch - ".$status." - ".$message;
			}
		} else {
			return "0|Hosted Checkout Batch - ".$status." - ".$message;
		}
	}
	
	function isDuplicatePayment($request_data) {
		$result = false;
        $order_id = $request_data['order_id'];
        $check = $request_data['check'];
        $amount = $request_data['card_amount'];
        $loc_id = $request_data['loc_id'];
        $check_transactions = lavu_query("SELECT `order_id`, `total_collected`, `auth`, `transtype`, `temp_data`, `preauth_id`, `auth_code`, `card_type`, `first_four`, `last_mod_ts`, `internal_id`, `action` FROM `cc_transactions` WHERE `order_id` = '[1]' AND `check` = '[2]' AND `voided` = '0' AND `loc_id`= '[3]' And `action` != 'Void' AND  pay_type in ('Cash', 'Card')", $order_id, $check, $loc_id);
        $cc_sum = 0;
        if (mysqli_num_rows($check_transactions) > 0) {
            while ($cc_data = mysqli_fetch_assoc($check_transactions)) {
                if ($cc_data['action'] == 'Sale') {
                    $cc_sum += $cc_data['total_collected'];
                } elseif ($cc_data['action'] == 'Refund') {
                    $cc_sum -= $cc_data['total_collected'];
                }
            }
        }
        $split_query = lavu_query("select (check_total-discount_value) as split_total_amount from `split_check_details` where `order_id`='[1]' AND `check` = '[2]' AND `loc_id`= '[3]' ", $order_id, $check, $loc_id);
        $split_data    = mysqli_fetch_assoc($split_query);
        $total_amount =$cc_sum+$amount;
        if ($total_amount > $split_data['split_total_amount']) {
              $result = true;
         }
         return  $result;
    }

	function HTMLResponse($message) {
	
		return "<table width='100%' height='150px'><tr><td align='center' style='padding:20px 20px 20px 20px; font-family:sans-serif; color:#222222;'>".$message."</td></tr></table>";
	}

	function createJSTags($js){
		return <<<JAVASCURPTS
		<script type="text/javascript">
			{$js}
		</script>
JAVASCURPTS;
	}
?>