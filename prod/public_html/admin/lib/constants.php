<?php
define("DELIVERY",          "delivery");
define("DELIVERY_LABEL",    "Delivery");
define("DINE_IN",           "dinein");
define("DINE_IN_LABEL",     "Dine In");
define("PICKUP",            "pickup");
define("PICKUP_LABEL",      "Pick Up");
define("LavuToGo",			"Lavu ToGo");