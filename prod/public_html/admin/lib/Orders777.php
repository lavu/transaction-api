<?php
require_once(dirname(__FILE__) . "/../cp/resources/core_functions.php");

class Orders777{

	private $loc_id, $server_id, $use_date, $device_time, $location_info, $specify_total, $cashier_id;
	private $todays_cash_tips, $order_id, $server_date_time, $localized_date_time;
	private $data_name, $register, $register_name;
	private $insert_items;
	private $cash_tips_mode = ""; //0='per_order', 1='daily_total', 2='none'
	private $user_row;
	private $numOrders;

	private $error;

	public function __construct($cashTipMode, $dataName, $register, $registerName, $serverID, $useDate, $locationInfo, $specifyTotal = false){
		$this->error = false;
		$this->specify_total = $specifyTotal;
		$this->setCashTipsMode($cashTipMode);
		$this->location_info = $locationInfo;
		$this->loc_id = $locationInfo['id'];
		$this->server_id = $serverID;
		$this->cashier_id = $serverID; //TODO: For now reports should be fine with cashier_id = server_id, but it is recommended that cashier_id be pulled into the 777 orders.
		$this->device_time = $this->currentLocalTime();
		$this->register = $register;
		$this->register_name = $registerName;
		$this->data_name = $dataName;
		if($this->specify_total == false){
			$this->todays_cash_tips = false;
		}else{
			$this->todays_cash_tips = $specifyTotal;
		}
		$this->use_date = $useDate;
		$this->order_id = '777'.$this->server_id.'-'.str_replace('-', '', $this->use_date);
		$this->server_date_time = date('Y-m-d H:i:s', time());
		$this->localized_date_time = localize_datetime($this->serverDatetime, $this->location_info['timezone']);
		$this->user_row = $this->getUserRow();
	}

	private function setCashTipsMode($modeInt){
		switch($modeInt){
			case 0:
				$this->cash_tips_mode = 'per_order';
				break;
			case 1:
				$this->cash_tips_mode = 'daily_total';
				break;
			case 2:
				$this->cash_tips_mode = 'none';
				break;
			default:
				$this->cash_tips_mode = 'none';
				$this->error = true;
				$this->debug777(__LINE__, "ERROR: Cash Tips mode was not set to a known integer: ".$modeInt);
		}
	}
	private function getUserRow(){
		$userRowResult = lavu_query("SELECT * FROM `users` WHERE `id`='[1]'", $this->server_id);
		if(!$userRowResult){
			$this->error = true;
			$this->debug777(__LINE__, "ERROR: ".lavu_dberror());
		}
		if(mysqli_num_rows($userRowResult) == 0){
			$this->error = true;
			$this->debug777(__LINE__, "ERROR: user could not be found.");
		}
		$userRow = mysqli_fetch_assoc($userRowResult);
		return $userRow;
	}

	private function buildInsertItems(){
		$ii = array();
		$ii['tip_amount']   = $this->todays_cash_tips;
		$ii['loc_id']       = $this->loc_id;
		$ii['datetime']     = $this->localized_date_time;
		$ii['server_id']    = $this->server_id;
		$ii['server_time']  = date('Y-m-d H:i:s');
		$ii['order_id']     = $this->order_id;
		$ii['location']     = $this->location_info['title'];
		$ii['opened']       = $this->device_time;
		$ii['closed']       = $this->device_time;
		$ii['datetime'] 	= $this->device_time;;
		$ii['tablename']    = "";
		if($this->cash_tips_mode == 'per_order'){
			$ii['tablename'] = "tips_submission_per_order";
		}else if($this->cash_tips_mode == 'daily_total'){
			$ii['tablename'] = "tips_submission_daily_total";
		}
		$ii['register']         = $this->register;
		$ii['register_name']    = $this->register_name;
		$ii['server_name']      = $this->user_row['f_name'];
		$ii['server']           = $this->user_row['f_name'];
		$ii['last_mod_ts']      = time();
		$ii['current_time']     = $this->device_time;
		$ii['cashier_id']       = $this->cashier_id;
		return $ii;
	}

	public function ensure777CashTipsOrderInsertion(){
		$this->insert_items = $this->buildInsertItems();
		$this->ensure777CCTransactionExists();
		$this->ensure777OrderExists();
		return $this->error;
	}

	private function ensure777CCTransactionExists(){
		$ccTransExistsResult = lavu_query("SELECT * FROM `cc_transactions` WHERE `order_id`='[1]'",$this->order_id);
		if(!$ccTransExistsResult){
			$this->error = true;
			$this->debug777(__LINE__, "ERROR: while trying to find cc_transaction. -lkasnbdfh- mysql: ".lavu_dberror());
		}
		if(mysqli_num_rows($ccTransExistsResult)){
			$updateCCTransAmountQuery = "UPDATE `cc_transactions` SET `tip_amount`='[tip_amount]', `last_mod_ts`='[last_mod_ts]' WHERE `order_id`='[order_id]'";
			$updateCCTransUpdate = lavu_query($updateCCTransAmountQuery,$this->insert_items);
			if(!$updateCCTransUpdate){
				$this->error = true;
				$this->debug777(__LINE__, "ERROR: dataname:".$this->data_name." -nhw4- mysql error: ".lavu_dberror());
			}
		}
		else{
			$insertCCTransQuery = "INSERT INTO `cc_transactions` ".
				"(`order_id`,`tip_amount`,`loc_id`,`datetime`,`server_id`,`server_time`,`pay_type`,`pay_type_id`,`server_name`,`action`,`last_mod_ts`,`register`, `register_name`) VALUES ".
				"('[order_id]','[tip_amount]','[loc_id]','[datetime]','[server_id]','[server_time]','Cash','1','[server_name]','CashTips','[last_mod_ts]','[register]', '[register_name]')";
			$insertCashTipCCTransRowResult = lavu_query($insertCCTransQuery,$this->insert_items);
			if(!$insertCashTipCCTransRowResult){
				$this->error = true;
				$this->debug777(__LINE__, "ERROR: dataname:".$this->data_name." -sfg67- mysql error: ".lavu_dberror());
			}
		}
	}

	private function ensure777OrderExists()
	{
		$this->debug777(__LINE__, "Cashier ID: ".$this->cashier_id);

		// Orders, Update or insert
		$ordersExistsResult = lavu_query("SELECT `id` FROM `orders` WHERE `order_id`='[1]'",$this->order_id);
		if(mysqli_num_rows($ordersExistsResult)){
			$updateOrdersAmountQuery = "UPDATE `orders` SET `cash_tip`='[tip_amount]', `last_modified`='[current_time]', `last_mod_device`='MANAGE TIPS', `last_mod_ts`='[last_mod_ts]', `pushed_ts`='[last_mod_ts]' WHERE `order_id`='[order_id]'";
			$updateOrdersResult = lavu_query($updateOrdersAmountQuery, $this->insert_items);
			if(!$updateOrdersResult){
				$this->error = true;
				$this->debug777(__LINE__, "ERROR: dataname:".$this->data_name." -sisr- mysql error: ".lavu_dberror());
			}

		}else{
			$insertOrderIDQuery = "INSERT INTO `orders` ".
				"(`location`,`order_id`,`location_id`,`server_id`,`cash_tip`,`opened`,`closed`,`server`,`last_mod_device`,`last_mod_ts`,`pushed_ts`,`tablename`,`register`, `register_name`, `cashier_id`) VALUES ".
				"('[location]','[order_id]','[loc_id]','[server_id]','[tip_amount]','[opened]','[closed]','[server]','MANAGE TIPS','[last_mod_ts]','[last_mod_ts]','[tablename]','[register]', '[register_name]', '[cashier_id]')";
			$insertCashTipOrderResult = lavu_query($insertOrderIDQuery,$this->insert_items);
			if(!$insertCashTipOrderResult){
				$this->error = true;
				$this->debug777(__LINE__, "ERROR: dataname:".$this->data_name." -4svtd- mysql error: ".lavu_dberror());
			}
		}
	}

	private function currentLocalTime() {
		$current_time = date("Y-m-d H:i:s", time());
		$tz = $this->location_info['timezone'];
		if (!empty($tz)) $current_time = localize_datetime(date("Y-m-d H:i:s"), $tz);
		return $current_time;
	}

	private function debug777($line, $message)
	{
		// ** DEBUG LOGGING ** //
		// keeping this commented for now until a debug logging mechanism is in place - RJG 8/29/2016
		//error_log(filePathToName(__FILE__)." - line ".$line." - ".$message);
	}
}
