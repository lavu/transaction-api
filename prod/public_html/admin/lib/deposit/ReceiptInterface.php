<?php
namespace Deposit;
interface ReceiptInterface {
    public function getDepositInfo();
}