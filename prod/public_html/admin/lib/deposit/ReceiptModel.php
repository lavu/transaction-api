<?php
namespace Deposit;

class receiptModel {
    public $depositId;
    public $dataName;
    public $tableName;
    public function __construct(Receipt $depositObj) {
        $this->depositId = $depositObj->depositId;
        $this->tableName = "`deposit_information`";
    }
    /**
     * This function will return information for deposit by id.
     */
    public function getDepositById() {
        $query = "SELECT `di`.`transaction_id` as 'depositId',
                    `cc`.`card_desc` as 'cardLast4',
                    `cc`.`server_name` as 'server',
                    CONCAT(`dci`.`first_name`, ' ', `dci`.`last_name`) AS customerName,
                    if(count(*) > 0, concat('', format(sum( IF(cc.transtype='deposit_refund', -1*cc.amount, cc.amount)),2)), '0.00') as 'ccAmount',
                    `di`.`deposit_type` as 'transactionType',
                    max(`cc`.`datetime`) as 'ccDate',
                    `di`.`amount` as 'depositedAmount',
                    `di`.`reason` as 'depositReason',
                    `di`.`created_date` as 'depositedDate',
                    `di`.`refund_amount` as 'refundedAmount',
                    `di`.`refund_date` as 'refundedDate',
                    `di`.`refund_reason` as 'refundedReason',
                    `di`.`pay_type` as 'currentStatus'
                FROM `deposit_customer_info` as dci
                JOIN ".$this->tableName." as di on (dci.id=di.customer_id)
                JOIN `cc_transactions` as cc on (cc.order_id=di.transaction_id )
                WHERE `di`.`transaction_id`='[1]' group by `cc`.`order_id` order by `cc`.`datetime` desc";
        $queryResult = \lavu_query( $query, $this->depositId );
        $resultObj = \mysqli_fetch_object( $queryResult );
        return $resultObj;
    }
}