<?php
namespace Deposit;

class ReceiptTemplate {
    public $depositId;
    public $cardLast4;
    public $server;
    public $customerName;
    public $ccAmount;
    public $transactionType;
    public $ccDate;
    public $depositedAmount;
    public $depositReason;
    public $depositedDate;
    public $refundedAmount;
    public $refundedDate;
    public $refundedReason;
    public $currentStatus;

    public function __construct(\stdClass $depositObj) {
        $this->setDepositInfo( $depositObj );
    }

    public function setDepositInfo(\stdClass $depositObj) {
        $this->depositId = $depositObj->depositId;
        $this->cardLast4 = $depositObj->cardLast4;
        $this->server = $depositObj->server;
        $this->customerName = $depositObj->customerName;
        $this->ccAmount = $depositObj->ccAmount;
        $this->transactionType = $depositObj->transactionType;
        $this->ccDate = $depositObj->ccDate;
        $this->depositedAmount = $depositObj->depositedAmount;
        $this->depositReason = $depositObj->depositReason;
        $this->depositedDate = $depositObj->depositedDate;
        $this->refundedAmount = $depositObj->refundedAmount;
        $this->refundedDate = $depositObj->refundedDate;
        $this->refundedReason = $depositObj->refundedReason;
        $this->currentStatus = $depositObj->currentStatus;
    }

    public function printReceipt() {
        $response = ( !is_null( $this->depositId ) ) ? $this  : array( );
        return json_encode( $response );
    }
}