<?php
namespace Deposit;

class PaymentGateway {
    public $locId;
    public function __construct( $locId ) {
        $this->locId = $locId;
    }
    
    public function getCurrentPGInfo( $extensionId ) {
        $paymentGatewayObj = new PaymentGatewayModel( $this->locId );
        return $paymentGatewayObj->getPaymentGatewayInfo( $extensionId );
    }
}