<?php
namespace Deposit;
require_once("ReceiptModel.php");
require_once("ReceiptInterface.php");

class Receipt implements ReceiptInterface {
    public $depositId;
    public $dataName;
    public function __construct(\stdClass $depositInfo) {
        if (isset($depositInfo->depositId) && !is_null($depositInfo->depositId)) {
            $this->depositId = $depositInfo->depositId;
        }
        if (isset($depositInfo->dataName) && !is_null($depositInfo->dataName)) {
            $this->dataName = $depositInfo->dataName;
        }
    }

    public function getDepositInfo() {
        if (isset($this->depositId) && !is_null($this->depositId)) {
            $receiptDataObj = new ReceiptModel($this);
            return $receiptDataObj->getDepositById();
        }
    }
}