<?php
namespace Deposit;

class PaymentGatewayModel {
    public $id;
    public $locId;
    public $type;
    public $deleted;
    public $order;
    public $getInfo;
    public $special;
    public $paymentGatewayName;
    public $paymentGatewayArr;
    
    
    public function __construct($locId){
        $this->locId = $locId;
    }
    
    public function getPaymentGatewayInfo( $extensionId = "") {
        if ( $extensionId !="" ) {
            $queryForEconduit = "SELECT `id`, `loc_id` as locId, `type`, `_deleted` as deleted, `_order` as 'order', `special` from `payment_types` where _deleted='0' AND loc_id=".$this->locId." AND `special`='".$extensionId."'";
            $queryResult = \lavu_query( $queryForEconduit );
            if ( mysqli_num_rows($queryResult) > 0 ) {
                $resultObj = \mysqli_fetch_object( $queryResult );
                $this->id = $resultObj->id;
                $this->locId = $resultObj->locId;
                $this->type = $resultObj->type;
                $this->deleted = $resultObj->deleted;
                $this->order = $resultObj->order;
                $this->getInfo = $resultObj->getInfo;
                $this->special = $resultObj->special;
                if( $this->special == "payment_extension:82" ) {
                    $this->paymentGatewayName = "eConduit";
                }
            }
            $this->paymentGatewayArr[] = $this;
        }
        $queryForPaymentGateway = "SELECT `id` as locId , `gateway` as paymentGatewayName, `_disabled` as deleted from `locations` where `_disabled`='0' AND `id`=".$this->locId;
        $queryResult = \lavu_query( $queryForPaymentGateway );
        if ( mysqli_num_rows($queryResult) > 0 ) {
            $resultObj = \mysqli_fetch_object( $queryResult );
            $this->paymentGatewayArr[] = $resultObj;
        }
        return $this->paymentGatewayArr;
    }
}