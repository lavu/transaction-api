<?php

if (!class_exists("LavuMonitor")) {

	class LavuMonitor {
		
		protected $monitor_level;

		private static $sync_monitor = NULL;
		private static $gateway_monitor = NULL;
		
		protected $instance_id;
		protected $start_datetime;
		protected $start_ts;
		protected $remote_ip;
		protected $host;
	
		protected $process_name;
		protected $dataname;
		protected $device_id;
		
		protected $app_name;
		protected $app_version;
		protected $app_build;
	
		protected $register;
		protected $server_id;
		
		public $ioid;
		public $order_id;
		public $check;
		
		private $mysql_errors;
		private $general_errors;
	
		public $suppress_reporting;
	
		protected function __construct() {
	
			require_once(dirname(__FILE__)."/../../cp/resources/core_functions.php");
			
			$this->generateInstanceID();
			$this->startTiming();
	
			$this->remote_ip			= reliableRemoteIP();
			$this->host					= $_SERVER['SERVER_NAME'];
			
			$this->process_name			= "";
			$this->dataname				= "";
			$this->device_id			= "";
			
			$this->app_name				= "";
			$this->app_version			= "";
			$this->app_build			= "";
	
			$this->register				= "";
			$this->server_id			= "";
	
			$this->ioid					= "";
			$this->order_id				= "";
			$this->check				= "";
	
			$this->mysql_errors			= array();
			$this->general_errors		= array();
			
			$this->suppress_reporting	= false;
		}
		
		public function __destruct() {
		
			//error_log(__CLASS__."::".__FUNCTION__);
		}
		
		private function generateInstanceID() {
		
			$prefix = "";
			while (strlen($prefix) < 4) {
				$pnum = rand(65, 90);
				$prefix .= (string)chr($pnum);
			}
			
			$ts_parts = explode(".", (string)microtime(true));
		
			$suffix = str_pad($ts_parts[1], 4, "0", STR_PAD_RIGHT);
		
			$this->instance_id = $prefix."-".$ts_parts[0]."-".$suffix;
		}
		
		private function startTiming() {

			$this->start_datetime	= $this->DenverDateTime();
			$this->start_ts			= microtime(true);
		}
		
		public function reportAndReset($resetTX=true) {
		
			$this->reportData();
			$this->startTiming();
			
			$this->mysql_errors		= array();
			$this->general_errors	= array();

			$this->resetInfo($resetTX);
		}
		
		public function setProcessName($process_name) {
			
			$this->process_name	= $process_name;
		}
		
		public function sessionDetails($process_name, $dataname, $device_id, $app_name, $app_version, $app_build, $register="", $server_id="") {
	
			$this->process_name	= $process_name;
			$this->dataname		= $dataname;
			$this->device_id	= $device_id;
			$this->app_name		= $app_name;
			$this->app_version	= $app_version;
			$this->app_build	= $app_build;
			$this->register		= $register;
			$this->server_id	= $server_id;
		}
		
		public function orderDetails($ioid, $order_id, $check) {
		
			$this->ioid		= $ioid;
			$this->order_id	= $order_id;	
			$this->check	= $check;
		}
	
		public function reportMysqlError($query, $error) {
			
			$error_info = array(
				"query" => $query,
				"error" => $error
			);
			
			error_log(__CLASS__."::".__FUNCTION__." - ".$query." - ".$error);
			
			$this->mysql_errors[] = $error_info;
		}
		
		protected function getMysqlErrors() {
	
			if (count($this->mysql_errors) == 0) {
				return "";	
			}
	
			return json_encode($this->mysql_errors)	;
		}
	
		public function reportGeneralError($error) {
			
			$this->general_errors[] = $error;

			if (is_array($error) || is_object($error)) {
				$error = json_encode($error);	
			}
			
			error_log(__CLASS__."::".__FUNCTION__." - ".$this->dataname." - ".$error);
		}
		
		protected function getGeneralErrors() {
	
			if (count($this->general_errors) == 0) {
				return "";	
			}
	
			return json_encode($this->general_errors);
		}
		
		protected function checkConnectionToMonitorDB() {
			// using poslavu_MAIN_db until another solution is devised
			mlavu_select_db("poslavu_MAIN_db");
		}
		
		protected function DenverDateTime($timezone_label=false) {
		
			$denver_tz = new DateTimeZone('America/Denver');
			$datetime = new DateTime(NULL, $denver_tz);
			
			$format_str = "Y-m-d H:i:s";
			if ($timezone_label) {
				$format_str .= " T";
			}
	
			return $datetime->format($format_str);	
		}
	}
}
?>
