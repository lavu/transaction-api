<?php
	ini_set('display_errors',1);
	
	$RESET_CONFIRM_URI = "https://" . $_SERVER['SERVER_NAME'] . "/lib/pwfunc.php";
	/**
	 *  PASSWORD RESET (ENGAGE, ENFORCE, ENHANCE?)
	 *  This file recieves via POST, the cue to reset a password via an Email request.  Using the information provided,
	 *  This file will attempt to initiate a password reset, and return either a success, or error message status.  
	 *
	 *  RECEIVES
	 *  email:          Either email or username specified.  Please note that either would have to have access to the backend in
	 *                  order for this function to find them, and then reset the user's password.
	 *
	 *  device_time:    The time specified by the requesting device/page.  This field may be used for logging purposes.
	 *
	 *  UUID:           The device ID of the requesting device.  This would only apply to Apple iOS device.  This field may be
	 *                  used for logging purposes.
	 *
	 *  MAC:            The MAC Address of the requesting device.  This field may be used for logging purposes.
	 *
	 *
	 *
	 *  RETURNS
	 *  A json response signifying the outcome.  The json reponse is guaranteed to have a 'json_status' field.
	 *  json_status:    Possible values:
	 *					success: Everything was processed correctly.
	 *					usernameNotFound: The Specified Entry has been deemed a user name, and it has not been found.
	 *					EmailNotFound:  The specified Entry has been deemed an email address, and it has not been found.
	 *					NoEmail:  No entry has been provided
	 *  
	 *  alert_title:    Reponse Box Title, Can contain any text, this will overwrite any default values specified.  OPTIONAL.
	 *
	 *  alert_message:  Response Message Text.  The idea is that it will appear in an alert box signifying further instructions.
	 *                  This will overwrite the default response.  OPTIONAL.
	 *  
	 **/
	header("Content-type: application/json");
	
	define("NO_EMAIL", "NoEmail");
	define("USERNAME_NOT_FOUND", "usernameNotFound");
	define("EMAIL_NOT_FOUND", "EmailNotFound");
	define("SUCCESS", "success");
	define("FAILED", "failed");
	
	/**
	 * Used for lavu_query.php
	 */
	require_once("../cp/resources/core_functions.php");
	
	/******************************************************************************
	 * FUNCTIONS
	 ******************************************************************************/
	function Quit_Status($json_status, $alert_title="", $alert_message="")
	{
		$return_string = '{';
		$return_string .= '"json_status" : "' . $json_status . '"';
		
		if($alert_title != "")
		{
			$return_string .= ', "alert_title" : "' . $alert_title . '"';
		}
		
		if($alert_message != "")
		{
			$return_string .= ', "alert_message" : "' . $alert_message . '"';
		}
		
		$return_string .= '}';
		
		echo $return_string;
		ConnectionHub::closeAll();
		exit();
		
	}
	
	/******************************************************************************
	 * START
	 ******************************************************************************/
	{
		/**
		 *  Ensures that we have been given the minimum of what we need
		 */
		if(!isset($_REQUEST['email']))
			Quit_Status(NO_EMAIL);
		
		$email = $_REQUEST['email'];
		
		/**
		 *  Determining whether the the email field is an email, via the @ indicator required in all email addresses.
		 */ 
		$is_email = strstr($email, "@") != FALSE;
		
		$dataname = "";
		$username = "";
		$id = "";
		
		{ //Ensures that we Are actually a legitmate user, without polluting our namespace
			$users_query = "SELECT * FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `[1]`='[2]'";
			$users_result = mlavu_query($users_query, $is_email ? 'email' : 'username', $email);

			if($users_result->num_rows == 0)
				Quit_Status($is_email ? EMAIL_NOT_FOUND : USERNAME_NOT_FOUND);
				
			if(mysqli_num_rows($users_result) > 1)
			{
				$result = mysqli_fetch_assoc($users_result);
				$dataname = $result['dataname'];
				while($result = mysqli_fetch_assoc($users_result))
				{
					if($result['dataname'] != $dataname)
						Quit_Status(EMAIL_NOT_FOUND, "Email is not unique", "The email specified in used for more than one users / accounts. Please use Username instead");
				}	
			}
			else
			{
				$result = mysqli_fetch_assoc($users_result);
				$dataname = $result['dataname'];
				$username = $result['username'];
				$email    = $result['email'];
				$id 	  = $result['id'];
			}
			
			mysqli_free_result($users_result);
		}
		
		/**
		 * Connect to the Local Database of the User's Account
		 */ 
		lavu_connect_dn($dataname);
		
		{ // Ensures that the User Isn't Deleted, and Does Still Exist
			$users_result;
			if($username != "")
			{
				$users_query = "";
				if($email == "")
					$users_query = "SELECT * FROM `poslavu_" . $dataname . "_db`.`users` WHERE `username`='[2]' AND `_deleted`='0'";
				else
					$users_query = "SELECT * FROM `poslavu_" . $dataname . "_db`.`users` WHERE `email`='[1]' AND `username`='[2]' AND `_deleted`='0'";
						
				$users_result = lavu_query($users_query, $email, $username);
			}
			else
			{
				$users_query = "SELECT * FROM `poslavu_" . $dataname_db . "`.`users` WHERE `[1]`='[2]' AND `_deleted`='0'";
				$users_result = lavu_query($users_query, $is_email ? 'email' : 'username', $email);
			}
			
			if(!mysqli_num_rows($users_result))
				Quit_Status($is_email ? EMAIL_NOT_FOUND : USERNAME_NOT_FOUND, "User not found", "The Email/Username specified was not found. Possibly a deleted users");
				
			if(mysqli_num_rows($users_result) > 1)
				Quit_Status(EMAIL_NOT_FOUND, "Email is not unique", "The email specified in used for more than one users / accounts. Please use Username instead");
			$result = mysqli_fetch_assoc($users_result);
			$username = $result['username'];
			$email = $result['email'];
			
			mysqli_free_result($users_result);
		} //Now Guaranteed to Be Only Referencing One User, the username/email are both populated, and the user is active.
		
		/**
		 * DO EMAIL STUFFS
		 */
		 {
			 if($email == "")
			 	Quit_Status(EMAIL_NOT_FOUND, "No email on file", "Unable to reset password, as there is no email on file");
			 
			 $token = generateToken(50);
		 
			 {// PUT THE TOKEN IN THE CUSTOMER ACCOUNTS TABLE
				 $token_query = "UPDATE `poslavu_MAIN_db`.`customer_accounts` SET `password_token`='[1]' WHERE `id`='[2]'";
				 $token_result = mlavu_query($token_query,$token,$id);
				 
				 if(ConnectionHub::getConn('poslavu')->affectedRows() == 0)
				 	Quit_Status(FAILED, "Request failed", "Unable to process request, Password reset failed token save");
				 	
				 //mysqli_free_result($token_result);
			 }
			 
			 {// GENERATE AND SEND EMAIL WITH GENERATED TOKEN
				$headers = 'From: noreply@poslavu.com' . "\r\n" .
							"Reply-To: noreply@poslavu.com" . "\r\n" .
							"MIME-Version: 1.0\r\n".
							"Content-Type: text/html; charset=ISO-8859-1\r\n";
							
				$subject = "Lavu Password Reset Request";
				
				$uri = $RESET_CONFIRM_URI . "?action=reset&token=$token";
				
				$email_body =  "<html>
	<head></head>
	<body>
		Greetings,<br /><br />
		We have received a request to reset the password for the Lavu account associated with this email. To continue with the reset, please follow the link below:<br /><br />
		<a href='$uri'>$uri</a><br /><br />
		If you have received this email in error, or you do not wish to change your password, please disregard this email.<br /><br />
		Best Wishes,<br />
		Lavu
	</body>
</html>";

				mail($email,$subject,$email_body, $headers);
			 }
		 }
		 
		 Quit_Status(SUCCESS);
	}
		
?>
