<?php
session_start();
require_once("info_inc.php");
function req_cc($cc_name, $cc_companyid) 
{

	return lsecurity_name($cc_name,$cc_companyid);
}

function getvar($str, $def=false) {
	return (isset($_REQUEST[$str]))?$_REQUEST[$str]:$def;
}

function display_price($price, $display_price) {

	global $location_info;

	$decimal_char = ".";
	if ($location_info['decimal_char'] != "") $decimal_char = $location_info['decimal_char'];

	if ($location_info['left_or_right'] == "right") return number_format($price, $display_price, $decimal_char, ",")." ".$location_info['monitary_symbol'];
	else return $location_info['monitary_symbol']." ".number_format($price, $display_price, $decimal_char, ",");
}

$display = "";
$resp = "";

$access_level = 0;
$location_query = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $loc_id);
if(mysqli_num_rows($location_query))
{
	$location_read = mysqli_fetch_assoc($location_query);
	$server_query = lavu_query("SELECT * FROM `users` WHERE `id`='[1]'", $server_id);
	if(mysqli_num_rows($server_query))
	{
		$server_read = mysqli_fetch_assoc($server_query);
		$access_level = $server_read['access_level'];
		//echo "Logged in: " . $server_read['username'] . " <a onclick='location.reload(true)'>(refresh)</a>";
	}
}
$mode = getvar("mode","menu");

if($mode == 'cash' || $mode == 'card'){
	require_once(dirname(__FILE__) . "/management/customer_deposit.php");
}

if($mode=="cash" || $mode=="card")
{
	$display = $hdr . $display;
}

$pack_id = reqvar("pack_id", "");
$active_language_pack = getWebViewLanguagepack($pack_id, $loc_id);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Management</title>
		<style>
			body { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; }
			.cct_labels { color:#999999; font-size:11px; }
			.cct_info { color:#666666; font-size:11px; }
			.error_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#CC0000; }
			.info_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; }
<?php
	if ($native_bg == "0") {
		echo ".mt_panel_top { background-image:url(\"management/images/top_management.png\"); background-repeat:no-repeat; width:980px; height:50px; }";
		echo ".mt_panel_mid { background-image:url(\"management/images/middle_management.png\"); background-repeat:repeat-y; width:980px; height:564px; }";
		echo ".mt_panel_bottom { background-image:url(\"management/images/bottom_management.png\"); background-repeat:no-repeat; width:980px; height:30px; }";
	} else {
		echo ".mt_panel_top { width:986px; height:30px; }";
		echo ".mt_panel_mid { width:986px; height:564px; }";
		echo ".mt_panel_bottom { width:986px; height:30px; }";
	}
?>
			.small_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; }
			.tbot { border-top:solid 2px #777777; text-align:right; font-weight:bold; }
			.title_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:18px; color:#000000; }
			.ttop { border-bottom:solid 2px #777777; text-align:center; font-weight:bold; }
			.mtpanel_top2 { }
			.mtpanel_mid2 { }
			.mtpanel_bottom2 { }
		</style>
		<script language='javascript'>
			
				function refreshPage() {
					window.location = "?<?php echo "mode=".$mode."&".$fwd_vars; ?>";
				}
		</script>
		
		<?php echo $webspefun; ?>
	</head>

	<body> <!-- background='management/images/iambg2.png' -->
		<center>
        	<?php
				if(isset($suppress_white_bg) && $suppress_white_bg)
				{
					echo $display;
				}
				else
				{
					?>
					<table cellspacing='0' cellpadding='0' width='<?php echo ($native_bg == "0")?"980":"986"; ?>px'>
						<!--<tr><td background='management/images/iambg3.png'>&nbsp;</td></tr>-->
						<tr><td class='mt_panel_top'>&nbsp;</td></tr> <!-- iambg3 -->
						<tr>
							<td class='mt_panel_mid' align='center' valign='top'>
								<table cellspacing='0' cellpadding='3' width='<?php echo ($native_bg == "0")?"960":"966"; ?>px'>
									<tr><td align='center'><?php echo $display; ?></td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td><?php echo $resp; ?></td></tr>
									<tr><td>&nbsp;</td></tr>
								</table>
							</td>
						</tr>
						<tr><td class='mt_panel_bottom' align='center'>
                        	<table width='848' background=''><tr><td style='height:42px'>&nbsp;</td></tr></table> <!-- iambg4 -->
                        </td></tr>
					</table>
                    <?php
				}
			?>
		</center>
	</body>
</html>
