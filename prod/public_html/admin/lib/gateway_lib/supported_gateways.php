<?php
    /*
    * supported_gateways, returns a list of *payment gateways*
    * that can be used with Lavu. There are two optional
    * arguments:
    *   - currentGateway: Payment gateway _key_ currently selected
    *                     or used by someone if any. This will return
    *                     the gateway regardless of restriction.
    *   - showRestrictedGateways: Override to return payment gateways
    *                     that should only be enable by admins.
    */
    function supported_gateways($currentGateway="", $showRestrictedGateways=false)
    {
        global $modules;
        $isLavuPayEnabled = $modules->hasModule("extensions.payment.lavupay");

        $gateways = array(
            "Authorize.net" => array("name" => "Authorize.net", "restricted" => true),
            "BluePay"       => array("name" => "BluePay", "restricted" => true),
            "TGate"         => array("name" => "BridgePay (TGate)", "restricted" => false),
            "EVOSnap"       => array("name" => "EVO Snap", "restricted" => false),
            "Heartland"     => array("name" => "Heartland", "restricted" => false),
            "Magensa"       => array("name" => "Magensa", "restricted" => true),
            "MerchantWARE"  => array("name" => "MerchantWARE", "restricted" => true),
            "Mercury"       => array("name" => "Mercury", "restricted" => false),
            "MIT"           => array("name" => "MIT", "restricted" => false),
            "Moneris"       => array("name" => "Moneris", "restricted" => false),
            "eConduit"      => array("name" => "Lavu Integrated Payments", "restricted" => false),
            "Square"        => array("name" => "Square", "restricted" => false),
            "NETS"          => array("name" => "NETS", "restricted" => false),
            "PayPal"        => array("name" => "PayPal", "restricted" => true),
            "RedFin"        => array("name" => "RedFin", "restricted" => true),
            "USAePay"       => array("name" => "USAePay", "restricted" => true),
            "LavuPay"       => array("name" => "Lavu Pay", "restricted" => !$isLavuPayEnabled)
        );

        $output = array();
        foreach ($gateways as $key => $value) {
            if (!$value["restricted"] || ($showRestrictedGateways || $key == $currentGateway)) {
                $output[$key] = $value["name"];
            }
        }

        return $output;
    }
?>
