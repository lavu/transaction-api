<?php
	
	function process_usa_epay2($process_info, $location_info, $transaction_vars) {
	
		require_once(dirname(__FILE__)."/usaepay.php");
		
		global $usaepay_sandbox;
		global $server_order_prefix;
		global $usaepay_test_mode;
		
		$debug = print_r($process_info, true)."\n\n";

		if ($process_info['set_pay_type'] != "") {
			$pay_type = $process_info['set_pay_type'];
			$pay_type_id = $process_info['set_pay_type_id'];
		} else {
			$pay_type = "Card";
			$pay_type_id = "2";
		}
		
		$transtype = $process_info['transtype'];
		$ext_data = $process_info['ext_data'];
		$send_amount = $process_info['card_amount'];
		$cn = $process_info['card_number']; // keep for response string
		$first_four = substr($cn, 0, 4);
		$card_type = getCardTypeFromNumber($cn);
		$log_tip_amount = "";
		
		$swiped = "0";
		$swipe_grade = "H";
		$send_mag_data = "";
		if ($process_info['mag_data']) {
			$swiped = "1";		
			if ($process_info['reader']=="idynamo" || $process_info['reader']=="udynamo") {
			
				$swipe_grade = "A";
				$send_mag_data = "enc://".base64_encode($process_info['mag_data']);
			
			} else if ($process_info['reader'] == "paysaber") {
				
				if($process_info['encrypted'] == '1') // Should Always be the Case
				{
					$mag_data = $process_info['mag_data'];
		
					$pos = 0;
					$startpos = 0;
					
					if($mag_data[2] == chr(2))
					{
						$pos = 2;
						$startpos = 2;
					}
					else if($mag_data[3] == chr(2))
					{
						$pos = 3;
						$startpos = 3;
					}
					
					$debug .= "Start: $startpos\n";
					
					if($mag_data[$pos] == chr(2))
					{ // ENCRYPTED MAG DATA
						$pos += 3;
						$encoding = $mag_data[$pos++];
						$decodingStatus = $mag_data[$pos++];
						$track1len = $mag_data[$pos++];
						$track2len = $mag_data[$pos++];
						$track3len = $mag_data[$pos++];
						
						if($mag_data[$pos] == chr(3))
							$pos += 2;
							
						if(strlen($mag_data) <= $pos + $track1len + $track2len + $track3len)
						{
							$debug .= "Sent Full Track.\n";
							$send_mag_data = $mag_data;
						}
						else
						{
							$encryptedLen = $track1len + $track2len + $track3len;
							$maskedTrack = substr($mag_data, $pos, $encryptedLen);
							$pos += $encryptedLen;
							
							if ($encryptedLen % 8)
								$encryptedLen += (8 - ($encryptedLen%8));
							
							if(strlen($mag_data) <= $pos + $encryptedLen)
							{
								$debug .= "Sent Masked Track.\n";
								$send_mag_data = $maskedTrack;
							}
							else
							{
								
								if($startpos)
								{
									$debug .= "Sent Base64 Encoded Sub Track, Starting at $startpos.\n";
									$sub_track = substr($mag_data, $startpos);
									$hex='';
									for ($i=0; $i < strlen($mag_data); $i++)
								    {
								        $hex .= dechex(ord($mag_data[$i]));
								    }
									$debug .= $hex . "\n";
									
									$hex='';
									for ($i=0; $i < strlen($sub_track); $i++)
								    {
								        $hex .= dechex(ord($sub_track[$i]));
								    }
									$debug .= $hex . "\n";
									
									$send_mag_data = "enc://" . base64_encode($sub_track);
								}
								else
								{
									$debug .= "Sent Base64 Encoded Full Track.\n";
									$send_mag_data = base64_encode($mag_data);
								}
							}
							
						}
					}
					else if($mag_data[1] == chr(0))
					{
						$tempstr = substr($mag_data, 2);
						$components = explode($tempstr,"\r");
						if(count($components) >= 2)
						{
							$tempstr = $components[1] . $components[0];
							$send_mag_data = $tempstr;
							$debug .= "Reversed Across Character Return, and sent.\n";
						}
					}
					else
					{
						$debug .= "Sending the Original Mag Data.\n";
						$send_mag_data = $mag_data;
						
					}
					
					$swipe_grade = "A";
				}
				
			} else if ($process_info['mag_data'] && $process_info['encrypted']=="1") {
		
				$e2e = true;
				$encrypted_info = idtech_encrypted($process_info['mag_data'], $process_info, $location_info);			
				$send_mag_data = "enc://".base64_encode($encrypted_info['sub_mag_data']);
									
			} else {
				
				$swipe_grade = "G";
				$tracks = explode("?", $process_info['mag_data']);
				$track2_index = 1;
				if (strstr($tracks[0], "=")) $track2_index = 0;
				if (($track2_index == 1) && (count($tracks) >= 1)) {
					$t1 = $tracks[0];
					if (($t1 != "") && ($t1 != "ERROR")) {
						$send_mag_data = $t1;
						$swipe_grade = "F";
					}
				}
				if (count($tracks) > $track2_index) {
					$t2 = $tracks[$track2_index];
					if (($t2 != "") && ($t2 != "ERROR")) {
						if ($swipe_grade == "F") {
							$swipe_grade = "D";
							$send_mag_data .= "?".$t2;
						} else {
							$swipe_grade = "E";
							$send_mag_data = $t2;
						}
					}
				}
			}
		}
		
		$card_exp = $process_info['exp_month'].$process_info['exp_year'];
			
		$uep_cmd = "";
		$send_account_info = false;
		$send_ref_num = false;
		if ($transtype == "") $transtype = "Sale";
		
		switch ($transtype) {
			case "Sale" :
				$uep_cmd = "sale";
				$send_account_info = true;
				break;
			case ($transtype=="Auth" || $transtype=="AuthForTab") :
				$uep_cmd = "preauth";
				$send_account_info = true;
				if ($process_info['order_id'] == "0") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
				break;
			case ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") :
				$uep_cmd = "capture";
				$send_ref_num = true;
				$get_transaction_info = lavu_query("SELECT `auth`, `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				if (!empty($process_info['card_amount'])) $send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				else $send_amount = number_format(($transaction_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				if ($cn == "") $cn = $transaction_info['card_desc'];
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				break;
			case "Adjustment" :
				$uep_cmd = "adjust";
				$send_ref_num = true;
				$get_transaction_info = lavu_query("SELECT `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$send_amount = ($transaction_info['amount'] + $process_info['tip_amount']);
				if ($cn == "") $cn = $transaction_info['card_desc'];
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				break;
			case ($transtype=="Void" || $transtype=="VoidPreAuthCapture") :
				$uep_cmd = "void";
				$send_ref_num = true;
				break;
			case "Return" :
				$uep_cmd = "refund";
				$send_ref_num = true;
				$ext_data .= "<Force>T</Force>";
				$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $process_info['loc_id'], $process_info['order_id'], $process_info['check'], substr($cn, (strlen($cn) - 4), 4));
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$process_info['pnref'] = $transaction_info['transaction_id'];
				$send_account_info = true;
				$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
				break;
			default :
				return "0|Unable to determine transaction type.|||".$process_info['order_id'];
				break;
		}

		$transaction_vars['transtype'] = $transtype;
		$tran['swipe_grade'] = $swipe_grade;
		
		$q_fields = "";
		$q_values = "";
		$keys = array_keys($transaction_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		
		$system_id = $transaction_vars['system_id'];
		
		$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";
		
		$debug .= "\n\n".print_r($process_info, true)."\n\n";

		$tran = new umTransaction;

		$tran->key = $process_info['username']; // _jtRJJBnpvBQ2pNLH8MUGiy0jwKm0o6c // EvOsao2kn3n6oCzxl4zW2O8kDJi7n7Vi
		if ($process_info['password'] != "") $tran->pin = $process_info['password'];
		$tran->ip = $_SERVER['REMOTE_ADDR'];
 
 		$tran->usesandbox = $usaepay_sandbox;
		$tran->testmode = $usaepay_test_mode;
		
		if ($uep_cmd != "") $tran->command = $uep_cmd;
		$tran->amount = $send_amount;
		if (($uep_cmd == "capture") || ($uep_cmd == "adjust")) $tran->tip_amount = $process_info['tip_amount'];

		$tran->invoice = substr(str_replace("-", "", $process_info['loc_id'].$process_info['order_id']), -10);
		$tran->orderid = str_replace("-", "", $process_info['loc_id'].$process_info['order_id']);
		$tran->description = $location_info['title'];	// description of charge
		
		if ($send_account_info) {
			if ($swiped == "1") {
				$tran->magstripe = $send_mag_data;  	// mag stripe data.  can be either Track 1, Track2  or  Both  (Required if card,exp,cardholder,street and zip aren't filled in)
				$tran->cardpresent = true;
				$tran->termtype = "POS";
				$tran->magsupport = "yes";
				$tran->contactless = "no";
			} else {
				$tran->card = $process_info['card_number'];
				$tran->exp = $card_exp;
				$tran->cardholder = $process_info['name_on_card'];
				$tran->street = "";
				$tran->zip = "";
				$tran->cvv2 = $process_info['card_cvn'];
			}
		}
		
		if ($send_ref_num) $tran->refnum = $process_info['pnref'];
		
		$debug .= "\n\nUSAEPAY\n\nBEFORE PROCESS(): ".print_r($tran, true);
			 
 		$processed = $tran->Process();
		
		$result = $tran->result;
		$new_pnref = $tran->refnum;
		$authcode = $tran->authcode;
		$respmsg = $tran->error;
		
		if ($respmsg == "") $respmsg = $result;

		$debug .= "\n\nAFTER PROCESS: ".print_r($tran, true)."\n\nRESULT: $result\nRESPONSE MESSAGE: $respmsg\n$authcode\n$new_pnref\n$card_type\n\n";

		$approved = "0";
		if ($processed) $approved = "1";
		if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "USAEPAY", $transtype, $approved, $debug);

		$log_string = "USAEPAY[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - CHECK : ".$process_info['check']." - AMOUNT: $send_amount".$log_tip_amount." - TRANSTYPE: $transtype - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $authcode - CARD TYPE: $card_type - FIRST FOUR: $first_four - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4)." - SWIPED: ".$swiped."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
		write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

		$rtn = array();

		if ($processed) {

			$process_info['last_mod_ts'] = time();

			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");
	
			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['order_id'];
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['last_mod_ts'];

		} else {

			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
	
			$rtn[] = "0"; // 0
			$rtn[] = $respmsg;
			$rtn[] = $new_pnref;
			$rtn[] = $$process_info['order_id'];
		}
		
		return implode("|", $rtn);
	}

function process_usa_epay($process_info, $location_info) {

	require_once(dirname(__FILE__)."/usaepay.php");

	global $usaepay_sandbox;
	global $server_order_prefix;
	global $usaepay_test_mode;

	$debug = print_r($process_info, true)."\n\n";

	if ($process_info['set_pay_type'] != "") {
		$pay_type = $process_info['set_pay_type'];
		$pay_type_id = $process_info['set_pay_type_id'];
	} else {
		$pay_type = "Card";
		$pay_type_id = "2";
	}

	$transtype = $process_info['transtype'];
	$ext_data = $process_info['ext_data'];
	$send_amount = $process_info['card_amount'];
	$cn = $process_info['card_number']; // keep for response string
	$first_four = substr($cn, 0, 4);
	$card_type = getCardTypeFromNumber($cn);
	$log_tip_amount = "";

	$swiped = "0";
	$swipe_grade = "H";
	$send_mag_data = "";
	if ($process_info['mag_data']) {
		$swiped = "1";
		if ($process_info['reader']=="idynamo" || $process_info['reader']=="udynamo") {

			$swipe_grade = "A";
			$send_mag_data = "enc://".base64_encode($process_info['mag_data']);

		} else if ($process_info['reader'] == "paysaber") {

			if($process_info['encrypted'] == '1') // Should Always be the Case
			{
				$mag_data = $process_info['mag_data'];

				$pos = 0;
				$startpos = 0;

				if($mag_data[2] == chr(2))
				{
					$pos = 2;
					$startpos = 2;
				}
				else if($mag_data[3] == chr(2))
				{
					$pos = 3;
					$startpos = 3;
				}

				$debug .= "Start: $startpos\n";

				if($mag_data[$pos] == chr(2))
				{ // ENCRYPTED MAG DATA
					$pos += 3;
					$encoding = $mag_data[$pos++];
					$decodingStatus = $mag_data[$pos++];
					$track1len = $mag_data[$pos++];
					$track2len = $mag_data[$pos++];
					$track3len = $mag_data[$pos++];

					if($mag_data[$pos] == chr(3))
						$pos += 2;

					if(strlen($mag_data) <= $pos + $track1len + $track2len + $track3len)
					{
						$debug .= "Sent Full Track.\n";
						$send_mag_data = $mag_data;
					}
					else
					{
						$encryptedLen = $track1len + $track2len + $track3len;
						$maskedTrack = substr($mag_data, $pos, $encryptedLen);
						$pos += $encryptedLen;

						if ($encryptedLen % 8)
							$encryptedLen += (8 - ($encryptedLen%8));

						if(strlen($mag_data) <= $pos + $encryptedLen)
						{
							$debug .= "Sent Masked Track.\n";
							$send_mag_data = $maskedTrack;
						}
						else
						{

							if($startpos)
							{
								$debug .= "Sent Base64 Encoded Sub Track, Starting at $startpos.\n";
								$sub_track = substr($mag_data, $startpos);
								$hex='';
								for ($i=0; $i < strlen($mag_data); $i++)
								{
									$hex .= dechex(ord($mag_data[$i]));
								}
								$debug .= $hex . "\n";

								$hex='';
								for ($i=0; $i < strlen($sub_track); $i++)
								{
									$hex .= dechex(ord($sub_track[$i]));
								}
								$debug .= $hex . "\n";

								$send_mag_data = "enc://" . base64_encode($sub_track);
							}
							else
							{
								$debug .= "Sent Base64 Encoded Full Track.\n";
								$send_mag_data = base64_encode($mag_data);
							}
						}

					}
				}
				else if($mag_data[1] == chr(0))
				{
					$tempstr = substr($mag_data, 2);
					$components = explode($tempstr,"\r");
					if(count($components) >= 2)
					{
						$tempstr = $components[1] . $components[0];
						$send_mag_data = $tempstr;
						$debug .= "Reversed Across Character Return, and sent.\n";
					}
				}
				else
				{
					$debug .= "Sending the Original Mag Data.\n";
					$send_mag_data = $mag_data;

				}

				$swipe_grade = "A";
			}

		} else if ($process_info['mag_data'] && $process_info['encrypted']=="1") {

			$e2e = true;
			$encrypted_info = idtech_encrypted($process_info['mag_data'], $process_info, $location_info);
			$send_mag_data = "enc://".base64_encode($encrypted_info['sub_mag_data']);

		} else {

			$swipe_grade = "G";
			$tracks = explode("?", $process_info['mag_data']);
			$track2_index = 1;
			if (strstr($tracks[0], "=")) $track2_index = 0;
			if (($track2_index == 1) && (count($tracks) >= 1)) {
				$t1 = $tracks[0];
				if (($t1 != "") && ($t1 != "ERROR")) {
					$send_mag_data = $t1;
					$swipe_grade = "F";
				}
			}
			if (count($tracks) > $track2_index) {
				$t2 = $tracks[$track2_index];
				if (($t2 != "") && ($t2 != "ERROR")) {
					if ($swipe_grade == "F") {
						$swipe_grade = "D";
						$send_mag_data .= "?".$t2;
					} else {
						$swipe_grade = "E";
						$send_mag_data = $t2;
					}
				}
			}
		}
	}

	$card_exp = $process_info['exp_month'].$process_info['exp_year'];

	$uep_cmd = "";
	$check_for_duplicate = false;
	$create_transaction_record = false;
	$send_account_info = false;
	$send_ref_num = false;
	if ($transtype == "") $transtype = "Sale";

	switch ($transtype) {
		case "Sale" :
			$uep_cmd = "sale";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$send_account_info = true;
			break;
		case ($transtype=="Auth" || $transtype=="AuthForTab") :
			$uep_cmd = "preauth";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$send_account_info = true;
			if ($process_info['order_id'] == "0") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
			break;
		case ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") :
			$uep_cmd = "capture";
			$send_ref_num = true;
			$get_transaction_info = lavu_query("SELECT `auth`, `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if (!empty($process_info['card_amount'])) $send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
			else $send_amount = number_format(($transaction_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
			if ($cn == "") $cn = $transaction_info['card_desc'];
			$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			break;
		case "Adjustment" :
			$uep_cmd = "adjust";
			$send_ref_num = true;
			$get_transaction_info = lavu_query("SELECT `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$send_amount = ($transaction_info['amount'] + $process_info['tip_amount']);
			if ($cn == "") $cn = $transaction_info['card_desc'];
			$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			break;
		case ($transtype=="Void" || $transtype=="VoidPreAuthCapture") :
			$uep_cmd = "void";
			$send_ref_num = true;
			$create_transaction_record = true;
			break;
		case "Return" :
			$uep_cmd = "refund";
			$send_ref_num = true;
			$ext_data .= "<Force>T</Force>";
			$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $process_info['loc_id'], $process_info['order_id'], $process_info['check'], substr($cn, (strlen($cn) - 4), 4));
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$process_info['pnref'] = $transaction_info['transaction_id'];
			$create_transaction_record = true;
			$send_account_info = true;
			$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
			break;
		default :
			return "0|Unable to determine transaction type.|||".$process_info['order_id'];
			break;
	}

	$transaction_vars = generate_transaction_vars($process_info, $location_info, $cn, $card_type, $transtype, $swipe_grade, $pay_type, $pay_type_id, $process_info['card_amount']);


	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	$system_id = 0;
	if ($create_transaction_record) {
		$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
		if (!$preliminary_record_transaction) return "0|Failed to create transaction record.||".$process_info['order_id'];
		$system_id = lavu_insert_id();
	}

	$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";

	$debug .= "\n\n".print_r($process_info, true)."\n\n";

	$tran = new umTransaction;

	$tran->key = $process_info['username']; // _jtRJJBnpvBQ2pNLH8MUGiy0jwKm0o6c // EvOsao2kn3n6oCzxl4zW2O8kDJi7n7Vi
	if ($process_info['password'] != "") $tran->pin = $process_info['password'];
	$tran->ip = $_SERVER['REMOTE_ADDR'];

	$tran->usesandbox = $usaepay_sandbox;
	$tran->testmode = $usaepay_test_mode;

	if ($uep_cmd != "") $tran->command = $uep_cmd;
	$tran->amount = $send_amount;
	if (($uep_cmd == "capture") || ($uep_cmd == "adjust")) $tran->tip_amount = $process_info['tip_amount'];

	$tran->invoice = substr(str_replace("-", "", $process_info['loc_id'].$process_info['order_id']), -10);
	$tran->orderid = str_replace("-", "", $process_info['loc_id'].$process_info['order_id']);
	$tran->description = $location_info['title'];	// description of charge

	if ($send_account_info) {
		if ($swiped == "1") {
			$tran->magstripe = $send_mag_data;  	// mag stripe data.  can be either Track 1, Track2  or  Both  (Required if card,exp,cardholder,street and zip aren't filled in)
			$tran->cardpresent = true;
			$tran->termtype = "POS";
			$tran->magsupport = "yes";
			$tran->contactless = "no";
		} else {
			$tran->card = $process_info['card_number'];
			$tran->exp = $card_exp;
			$tran->cardholder = $process_info['name_on_card'];
			$tran->street = "";
			$tran->zip = "";
			$tran->cvv2 = $process_info['card_cvn'];
		}
	}

	if ($send_ref_num) $tran->refnum = $process_info['pnref'];

	$debug .= "\n\nUSAEPAY\n\nBEFORE PROCESS(): ".print_r($tran, true);

	$processed = $tran->Process();

	$result = $tran->result;
	$new_pnref = $tran->refnum;
	$authcode = $tran->authcode;
	$respmsg = $tran->error;

	if ($respmsg == "") $respmsg = $result;

	$debug .= "\n\nAFTER PROCESS: ".print_r($tran, true)."\n\nRESULT: $result\nRESPONSE MESSAGE: $respmsg\n$authcode\n$new_pnref\n$card_type\n\n";

	$approved = "0";
	if ($processed) $approved = "1";
	if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "USAEPAY", $transtype, $approved, $debug);

	$log_string = "USAEPAY[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - CHECK : ".$process_info['check']." - AMOUNT: $send_amount".$log_tip_amount." - TRANSTYPE: $transtype - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $authcode - CARD TYPE: $card_type - FIRST FOUR: $first_four - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4)." - SWIPED: ".$swiped."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
	write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

	$rtn = array();

	if ($processed) {

		$process_info['last_mod_ts'] = time();

		update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

		$rtn[] = "1"; // 0
		$rtn[] = $new_pnref;
		$rtn[] = substr($cn, -4);
		$rtn[] = $respmsg;
		$rtn[] = $authcode;
		$rtn[] = $card_type;
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = $process_info['order_id'];
		$rtn[] = "";
		$rtn[] = ""; // 10
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = $process_info['last_mod_ts'];

	} else {

		$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

		$rtn[] = "0"; // 0
		$rtn[] = $respmsg;
		$rtn[] = $new_pnref;
		$rtn[] = $$process_info['order_id'];
	}

	return implode("|", $rtn);
}
?>