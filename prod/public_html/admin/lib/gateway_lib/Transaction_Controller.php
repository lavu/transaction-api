<?php

/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 5/12/16
 * Time: 3:14 PM
 * This class will host everything necessary to check transactions.
 * Acts as a man in the middle between the various <gateway>_func.php files and the things that invoke them.
 * Checks for duplicate transactions.
 * Creates records.
 * Creates transaction variables, some of which get updated after the fact by the <gateway>_func.php scripts for
 * consistency.
 */

require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery_safe.php");
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once(dirname(__FILE__)."/ValidateOrderAndCCTransactionsTables.php"); //Added by Leif Guillermo
require_once(dirname(__FILE__) . "/cardconnect/payment.php");

putenv("TZ=America/Chicago");

class Transaction_Controller
{
    /** @var string|null Payment Gateway name */
    private $gateway;

    /** @var string Transaction attempt response */
    private $response = "0|No response";

    /** @var mixed|null Request data */
    private $process_info;

    /** @var mixed|null Location's information */
    private $location_info;

    /** @var mixed|null Credit Card transaction variables */
    private $transaction_vars;

    public function __construct($gateway = "", $process_info = null)
    {
        $this->gateway = $gateway;
        if (isset($process_info)) {
            $this->process_info = $process_info;
        }
    }

    /**
     * Method for non-standard gateway interactions to check
     * the status of a transaction.
     *
     * @param mixed $process_info Request data
     * @param mixed $location_info Location's information
     *
     * @return string[]
     */
    public function checkTransaction($process_info, $location_info)
    {
        $this->process_info = $process_info;
        $this->location_info = $location_info;
        return $this->getTransaction($location_info);
    }

    //******************************Transaction Methods****************************
    /**
     * Gets the status of the current transaction based off of
     * the _process info_ associative array.
     *
     * @param mixed[] $location_info associative array
     *
     * @return string[] array with status and description
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    private function getTransaction($location_info)
    {
        $this->generate_transaction_vars();

	    //Added by Leif G.
	    if(!(strpos($this->transaction_vars['order_id'], '1-') == 0)){ //If the order ID starts with 1-, we are in retro, and the auth code for some reason doesn't make it here, so we can't verify as the code currently is.
		    if($this->transaction_vars['transtype'] == "Return"){
		        $authCode = $this->process_info['authcode'];
		        $validateTablesObject = new ValidateOrderAndCCTransactionsTables($this->transaction_vars['amount'], $this->transaction_vars['order_id'], $authCode, $location_info['id']);
			    $returnTransactionStatus = $validateTablesObject->validateReturnTransaction();
			    if($returnTransactionStatus !== true){
				    $rtn[] = "0";
				    $rtn[] = $returnTransactionStatus;
				    return $rtn;
			    }
		    }
	    }
	    //End addition by Leif G.

        $rtn = array();
        $check_transactions = lavu_query("SELECT `auth`, `transtype`, `temp_data`, `preauth_id`, `auth_code`, `card_type`, `first_four`,
                                        `last_mod_ts`, `internal_id` FROM `cc_transactions` WHERE `order_id` = '[order_id]' AND
                                        `check` = '[check]' AND `amount` = '[amount]' AND `card_desc` = '[card_desc]' AND `refunded` = '0'
                                        AND `processed` = '0' AND `loc_id` = '[loc_id]' AND `pay_type_id` = '[pay_type_id]'
                                        AND `voided` = '0' AND `transtype` = '[transtype]'", $this->transaction_vars);
        if (mysqli_num_rows($check_transactions) > 0) {
            $tx_info = mysqli_fetch_assoc($check_transactions);
            if ($this->transaction_vars['transtype'] == "PreAuthCapture" && $tx_info['auth'] == 0) {
                $rtn[]= '0';
                $rtn[]= 'Duplicate PreAuthCapture Detected';
                return $rtn;
            }

            if ($tx_info['auth_code'] !== '' && $tx_info['transtype'] === $this->transaction_vars['transtype']) {
                if ($this->gateway === 'LavuPay') {
                    $rtn = PaymentGateways\CardConnect\Payment::toMessageArray(
                            $tx_info,
                            $this->transaction_vars,
                            "APPROVED (internal retry)"
                        );
                } else {
                    $rtn[] = "0";
                    $rtn[] = "Pending Transaction has completed, please check for transactions before continuing.";
                }
                return $rtn;
            }

            if ($tx_info['temp_data'] === 'pending') {
                $rtn[] = "0";
                $rtn[] = "Pending Transaction";
                return $rtn;
            } else {
                if ($this->gateway === 'LavuPay') {
                    $rtn = PaymentGateways\CardConnect\Payment::toMessageArray(
                        $tx_info,
                        $this->transaction_vars,
                        "APPROVED (internal retry)"
                    );
                } else {
                    $rtn[] = "0";
                    $rtn[] = "Duplicate Transaction";
                }
                return $rtn;
            }
        } else {
            $rtn[] = "2";
        }
        return $rtn;
    }

    //*******************************Core Functionality
    /**
     * Returns the result of the payment gateway
     * transaction attempt.
     *
     * @param boolean $debug
     * @param boolean $kiosk
     *
     * @return string
     */
    public function getResponse($debug=false, $kiosk=false)
    {
		if ($kiosk) {
			$response = $this->response;
			$response = explode('|', $response);
			$response = array_pad($response, 19, '');
			$response[18] = $this->gateway;
			$this->response = implode('|', $response);
		}

		if ($debug) error_log("Gateway response: ". $this->response);

        return $this->response;
    }

    /**
     * Handle the transaction workflow.
     *
     * @param mixed $process_info  Request data
     * @param mixed $location_info Location's information
     *
     * @see Transaction_Controller::getResponse() To get transaction response.
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     */
    public function process_transaction($process_info, $location_info)
    {
        //assign properties
        $this->process_info = $process_info;
        $this->location_info = $location_info;

        //include relevant gateway file:
        $gatewayExists = $this->includeGatewayMethods();
        if (!$gatewayExists) {
            return;
        }

        $transactionPending = $this->getTransaction($location_info);
        if ($transactionPending[0] !== "2") {
            $this->response = implode("|", $transactionPending);
            return;
        }

        //submits the payment to the gateway
        $this->process_gateway();
    }
    //*****************************Gateway Functions***************************

    /**
     * Returns `true` if _payment gateway_ specified for the Location
     * has been implemented. It does so by checking the current directory
     * for files that follow the convention `<gateway name>_func.php`.
     *
     * @return boolean
     */
    private function includeGatewayMethods()
    {
        $func_path = dirname(__FILE__)."/".strtolower(str_replace(".", "_", $this->gateway))."_func.php";
        if (file_exists($func_path)) {
            require_once($func_path);
            return true;
        }
        else {
            $this->response = "0 | Gateway functions not found";
            return false;
        }
    }

    /**
     * Invokes process method for the Location's current
     * payment gateway
     */
    private function process_gateway()
	{
		$gatewayCheck = false;
		/*
		 * Code to track Audit Log
		 * this will track the process hit how many times while checkout
		 */
        $transtype = $this->process_info['transtype'];
       // if ($this->gateway == 'TGate') {
       //     if ($transtype == "Sale") {
       //         $check_details  = $this->toVerifyDuplicateGatewayRecords($this->process_info) ;
       //         if ($check_details == 'duplicate') {
       //              $this->response  = "0|-- Duplicate Payment Transaction --" . $this->gateway;
       //              return true;
       //         }
       //         if ($check_details == 'overpaid') {
       //              $this->response  = "0|-- Overpaid Payment Transaction --" . $this->gateway;
       //              return true;
       //         }
       //     }
       // }
		if ($this->gateway == 'TGate' || $this->gateway == 'Heartland' || $this->gateway == 'Mercury') {
			require_once(dirname(__FILE__)."/../AuditLog.php");

			$backtrace = debug_backtrace();
			$auditObj = new AuditLog();
			$gatewayCheck = true;
			$auditObj->gatewayAuditLog($backtrace, 'Request', $this->process_info, $this->gateway, $this->process_info['order_id'], $this->process_info['server_id']);
		}
		
        switch ($this->gateway)
		{
            case "Authorize.net" :
            case "BluePay" :
                $this->response = process_authorize_net($this->process_info, $this->location_info);
                //$this->response = process_authorize_net($this->process_info, $this->location_info, $this->transaction_vars);
                break;

			case "eConduit" : // LP-3759

				$eConduitGateway = new eConduitGateway();
				$this->response = $eConduitGateway->processTransaction($this->process_info, $this->location_info);
				break;

            case "EVOSnap" :
                $this->response = process_evo_snap($this->process_info, $this->location_info);
                //$this->response  = process_evo_snap($this->process_info, $this->location_info, $this->transaction_vars);
                break;

            case "Heartland" :

                $this->response = process_heartland($this->process_info, $this->location_info);
                //$this->response  = process_heartland($this->process_info, $this->location_info, $this->transaction_vars);
                break;

            case "Magensa" :

                $this->response = process_magensa($this->process_info, $this->location_info);
                //$this->response  = process_magensa($this->process_info, $this->location_info, $this->transaction_vars);
                break;

            case "MerchantWARE" :

                $this->response = process_merchantware($this->process_info, $this->location_info);
                //$this->response  = process_merchantware($this->process_info, $this->location_info, $this->transaction_vars);
                break;

            case "Mercury" :

                $this->response = process_mercury($this->process_info, $this->location_info);
                //$this->response  = process_mercury($this->process_info, $this->location_info, $this->transaction_vars);
                break;

            case "RedFin" :

                $this->response = process_redfin($this->process_info, $this->location_info);
                //$this->response  = process_redfin($this->process_info, $this->location_info, $this->transaction_vars);
                break;

            case "TGate" :
            case "PivotalPayments" :
                $this->response = process_tgate($this->process_info, $this->location_info);
                break;

            case "USAePay" :
                $this->response = process_usa_epay($this->process_info, $this->location_info);
                //$this->response  = process_usa_epay($this->process_info, $this->location_info, $this->transaction_vars);
                break;

			case "PayPal" :
                $this->response = process_paypal($this->process_info, $this->location_info);
                //$this->response  = process_usa_epay($this->process_info, $this->location_info, $this->transaction_vars);
                break;

            case "Square" :
                $this->response = process_square($this->process_info, $this->location_info);
                break;

            case "LavuPay": //In this context, LavuPay means PAPI
                try {
                    $this->response = process_papi($this->process_info, $this->location_info);
                }
                catch (Exception $e) {
                    error_log($e->getMessage());
                    $this->response = "0| Error processing Payment API request. Gateway: " . $this->gateway;
                }
                break;

            default:
                $this->response  = "0|--Unknown Gateway--" . $this->gateway;
                break;
        }
       if ($gatewayCheck) {
          // As Per LP-5183
          // if ($this->gateway == 'TGate') {
          //      $this->updatePaymentGatewayLogs($this->process_info, $this->response, '4', '') ;
          //  }
        $auditObj->gatewayAuditLog($backtrace, 'Response', $this->response, $this->gateway, $this->process_info['order_id'], $this->process_info['server_id']);
       }
    }

    //********************************Supporting Methods*******************************

    /**
     * Generate transaction variables that will eventually be stored
     * in the cc_transactions table. Uses this->process_info and
     * this->location_info Sets this->transaction_vars
     *
     * @return void
     */
    private function generate_transaction_vars()
    {
        $p_info = $this->process_info;
        $l_info = $this->location_info;
        $card_number = $p_info['card_number'];
        $card_type = $this->getCardTypeFromNumber($card_number);
        $transtype = $p_info['transtype'];
        $amount = $p_info['card_amount'];
        if ($p_info['set_pay_type'] != "") {
            $pay_type = $p_info['set_pay_type'];
            $pay_type_id = $p_info['set_pay_type_id'];
        } else {
            $pay_type = "Card";
            $pay_type_id = "2";
        }


        $vars = array(
            "loc_id" => $p_info['loc_id'],
            "device_udid" => $p_info['device_udid'],
            "ioid" => $p_info['ioid'],
            "order_id" => $p_info['order_id'],
            "check" => $p_info['check'],
            "internal_id" => $p_info['itid'],
            "transtype" => $transtype,
            "amount" => number_format((float)$amount, $l_info['disable_decimal'], ".", ""),
            "total_collected" => number_format((float)$amount, $l_info['disable_decimal'], ".", ""),
            "change" => number_format(0, $l_info['disable_decimal'], ".", ""),
            "card_type" => $card_type,
            "first_four" => substr($card_number, 0, 4),
            "card_desc" => substr($card_number, -4),
            "exp" => $p_info['exp_month'] . "/" . $p_info['exp_year'],
            "info" => $p_info['name_on_card'],
            "swipe_grade" => '',
            "auth_code" => $p_info['authcode'],
            "register" => $p_info['register'],
            "register_name" => $p_info['register_name'],
            "server_name" => $p_info['server_name'],
            "server_id" => $p_info['server_id'],
            "pay_type" => $pay_type,
            "pay_type_id" => $pay_type_id,
            "for_deposit" => $p_info['for_deposit'],
            "is_deposit" => $p_info['is_deposit'],
            "datetime" => $p_info['device_time'],
            "server_time" => UTCDateTime(true),
            "gateway" => $p_info['gateway'],
            "reader" => $p_info['reader'],
            "temp_data" => "pending",
        );

        if ($transtype == "Return") {
            $vars['tip_amount'] = $p_info['refund_tip_amount'];
        } else if (!empty($p_info['tip_amount'])) {
            $vars['tip_amount'] = $p_info['tip_amount'];
        }

        $process_info['exp'] = $vars['exp'];

        $this->transaction_vars = $vars;
    }

    /**
     * Returns card issuer based on card number pattern. A work around
     * has been introduced for card numbers that come from the
     * [CardPointe API](https://developer.cardconnect.com/cardsecure-api#understanding-tokens)
     * this needs to be addressed a different to avoid issues.
     *
     * @param string $cardNumber Credit Card number
     *
     * @return string credit card brand name
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function getCardTypeFromNumber($cardNumber)
    {

        $disc_array = array();
        $disc_array[] = 6011;
        for ($n = 622126; $n <= 622925; $n++) {
            $disc_array[] = $n;
        }
        for ($n = 644; $n <= 649; $n++) {
            $disc_array[] = $n;
        }
        $disc_array[] = 65;

        $jcb_array = array();
        for ($n = 3528; $n <= 3589; $n++) {
            $jcb_array[] = $n;
        }

        if ($cardNumber != "") {
            if ($this->numberBeginsWith($cardNumber, array(34,37)) && (strlen($cardNumber) >= 15)) {
                return 'AMEX'; // AMEX begins with 34 or 37, and length is 15. (allow for longer numbers for incorrect iDynamo/uDynamo mask length)
            } else if ($this->numberBeginsWith($cardNumber, array(51,52,53,54,55)) && strlen($cardNumber) == 16) {
                return 'MasterCard'; // MasterCard beigins with 51-55, and length is 16.
            } else if ($this->numberBeginsWith($cardNumber, array(4)) && ((strlen($cardNumber) == 13) || (strlen($cardNumber) == 16))) {
                return 'Visa'; // VISA begins with 4, and length is 13 or 16.
            } else if ($this->numberBeginsWith($cardNumber, array(300,301,302,303,304,305,36,38)) && (strlen($cardNumber) == 14)) {
                return 'DinersClub'; // Diners Club begins with 300-305 or 36 or 38, and length is 14.
            } else if ($this->numberBeginsWith($cardNumber, array(2014,2149)) && (strlen($cardNumber) == 15)) {
                return 'enRoute'; // enRoute begins with 2014 or 2149, and length is 15.
            } else if (($this->numberBeginsWith($cardNumber, [6]) || $this->numberBeginsWith($cardNumber, $disc_array)) && (strlen($cardNumber) == 16)) {
                return 'Discover'; //Discover begins with 6011, 622126 to 622925, 644 to 649, or 65, and length is 16.
            } else if ($this->numberBeginsWith($cardNumber, $jcb_array) && (strlen($cardNumber) == 16)) {
                return 'JCB';  // JCB begins with 3528 to 3589, and length is 16.
            } else if ($this->numberBeginsWith($cardNumber, array(2131,1800)) && (strlen($cardNumber) == 15)) {
                return 'JCB';  // JCB begins with 2131 or 1800, and length is 15.
            } else if ($this->numberBeginsWith($cardNumber, array(50)) && (strlen($cardNumber) == 19)) {
                return 'GiftCard';  // Gift Card begins with 50, and length is 19.
            } else if ($this->numberBeginsWith($cardNumber, array(9)) && strlen($cardNumber) === 16) {
                return $this->getCardTypeFromNumber(substr($cardNumber, 1) . "0");
            }	else {
                return 'UNKNWN';
            }
        } else {
            return "NA";
        }
    }

    /**
     * Returns `true` if string begins with any of the
     * patterns provided.
     *
     * @param string    $cardNumber number as a string
     * @param int[]     $look_for list of patterns to compare against
     *
     * @return boolean
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function numberBeginsWith($cardNumber, $look_for)
    {

        $found_it = false;

        foreach ($look_for as $lf) {
            if (substr($cardNumber, 0, strlen($lf)) == $lf) {
                $found_it = true;
                break;
            }
        }

        return $found_it;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function toVerifyDuplicateGatewayRecords($request_data)
    {
        $order_id = $request_data['order_id'];
        $check = $request_data['check'];
        $amount = $request_data['card_amount'];
        $pay_type = $request_data['transtype'];
        $loc_id = $request_data['loc_id'];
        $request_data = json_encode($request_data);
        $insert_data =array ('order_id' => $order_id, 'check' => $check, 'amount' => $amount, 'request_data' =>$request_data, 'pay_type' => $pay_type, 'payment_status' => '0' , 'process_status' =>'2', 'loc_id' => $loc_id);
        $check_transactions = lavu_query("SELECT `order_id`, `total_collected`, `auth`, `transtype`, `temp_data`, `preauth_id`, `auth_code`, `card_type`, `first_four`, `last_mod_ts`, `internal_id`, `action` FROM `cc_transactions` WHERE `order_id` = '[1]' AND `check` = '[2]' AND `voided` = '0' AND `auth` = 0 AND `loc_id`= '[3]' And `action` != 'Void' AND  pay_type in ('Cash', 'Card')", $order_id, $check, $loc_id);
        $cc_sum = 0;
        if (mysqli_num_rows($check_transactions) > 0) {
            while ($cc_data = mysqli_fetch_assoc($check_transactions)) {
                if ($cc_data['action'] == 'Sale') {
                    $cc_sum += $cc_data['total_collected'];
                } elseif ($cc_data['action'] == 'Refund') {
                    $cc_sum -= $cc_data['total_collected'];
                }
            }
        }
        $split_query = lavu_query("select (check_total-discount_value) as split_total_amount from `split_check_details` where `order_id`='[1]' AND `check` = '[2]' AND `loc_id`= '[3]' ", $order_id, $check, $loc_id);
        $split_data    = mysqli_fetch_assoc($split_query);
        $pament_log_query = lavu_query("select amount, pay_type from `payment_gateway_logs` where `order_id`='[1]' AND `check` = '[2]' AND payment_status = '0' AND `loc_id`= '[3]'", $order_id, $check, $loc_id);
        $payment_log_sum = 0;
        while ($pament_log_query_data = mysqli_fetch_assoc($pament_log_query)) {
            if ($pament_log_query_data['pay_type']== 'Refund') {
                $payment_log_sum -= $pament_log_query_data['amount'];
            } else {
                $payment_log_sum += $pament_log_query_data['amount'];
            }
        }
        $total_amount =$cc_sum+$payment_log_sum+$amount;
        if ( $split_data['split_total_amount'] == '0' || $pay_type == 'PreAuthCaptureForTab' || $split_data['split_total_amount'] == '') {
                $this-> insertPaymentGatewayLogs ($insert_data);
        } else if ($total_amount > $split_data['split_total_amount']) {
             $check_pament_log_query = lavu_query("select amount, pay_type from `payment_gateway_logs` where `order_id`='[1]' AND `check` = '[2]' AND payment_status in ('0', '1') AND `loc_id`= '[3]' AND `amount` = '[4]'", $order_id, $check, $loc_id, $amount);
             if (mysqli_num_rows($check_pament_log_query) > 0) {
                return  "duplicate";
             } else {
                return  "overpaid";
             }
        } else {
            $this-> insertPaymentGatewayLogs ($insert_data);
        }
    }

    public function insertPaymentGatewayLogs($transaction_vars)
    {
        $q_fields = "";
        $q_values = "";
        $transaction_vars['request_timestamp'] = date("Y-m-d H:i:s");
        $keys = array_keys($transaction_vars);
        foreach ($keys as $key) {
            if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
            $q_fields .= "`$key`";
            $q_values .= "'[$key]'";
        }
        $preliminary_record_transaction = lavu_query("INSERT INTO `payment_gateway_logs` ($q_fields) VALUES ($q_values)", $transaction_vars);
        return $preliminary_record_transaction;
    }

    public function updatePaymentGatewayLogs($request_data, $response_data, $status, $updated_id)
    {
        if ($updated_id == '') {
            $order_id = $request_data['order_id'];
            $check = $request_data['check'];
            $amount = $request_data['card_amount'];
            $loc_id = $request_data['loc_id'];
            $response_timestamp = date("Y-m-d H:i:s");
            $response_type = explode("|", $response_data);
            if ($response_type[0] == '1') {
                $payment_status = 1;
            } else {
                $payment_status = 2;
            }
            $gateway_log_query = lavu_query("select id from payment_gateway_logs where `order_id` = '[1]' and `check` = '[2]' and `amount` = '[3]' and `loc_id`= '[4]' and `payment_status` = 0 order by id ASC limit 0,1", $order_id, $check, $amount, $loc_id);
            $gateway_log_data  = mysqli_fetch_assoc($gateway_log_query);
            $payment_gateway_log_id = $gateway_log_data['id'];
            if ($payment_gateway_log_id != '') {
                lavu_query("update `payment_gateway_logs` set `response_data` = '$response_data', `response_timestamp` = '$response_timestamp', `payment_status` = '$payment_status' , `process_status` = '$status' where  id = $payment_gateway_log_id" );
            }
        } else {
            $payment_gateway_log_id = lavu_query("update `payment_gateway_logs` set  `process_status` = '$status' where  id = $updated_id" );
        }
        return  $payment_gateway_log_id;
    }
}
