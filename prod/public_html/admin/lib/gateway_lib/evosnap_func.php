<?php
require_once(dirname(__FILE__)."/evosnap.php");

function process_evo_snap2($process_info, $location_info, $transaction_vars) {
	$evo_payment = new EvoSnap(
		$process_info['username'],
		$process_info['password'],
		$process_info['integration3'],
		$process_info['integration4'],
		$process_info['integration5']
	);

	$cn = false;
	$ioid = false;
	$itid = false;
	$is_deposit = false;

	$send_amount = $process_info['card_amount'];
	$transtype = $process_info['transtype'];
	if ($transtype == ""){
		$transtype = 'Sale';
	}

	$needSwipe = true;
	if(!empty($process_info['pnref'])){
		$needSwipe = false;
	}
	if(in_array($transtype, array('Void','VoidPreAuthCapture','VoidLoyaltyCardIssueOrReload','VoidGiftCardIssueOrReload','LoyaltyCardBalance','GiftCardBalance','CaptureAll','Adjustment'))){
		$needSwipe = false;
	}
	if($needSwipe == true){
		$cn = $process_info['card_number'];
		$card_type = getCardTypeFromNumber($cn);
		if(empty($cn)){
			return "0|Unable to determine card number";
		}

		if(strlen($process_info['mag_data']) == 0){
			return "0|No card swipe data. Unable to process.";
		}

		if ($process_info['encrypted'] != "1"){
			return "0|Unable to process unencrypted swipes.";
		}

		if($process_info["reader"] != "idynamo"){
			return "0|Reader not iDynamo.";
		}

		if(false == $evo_payment->set_iDyanamo($process_info['mag_data'])){
			return "0|Card read failed. Try Again.\n";
		}
		$evo_payment->CardType = $card_type;
		$swiped = 'y';
		$swipe_grade = 'A';
	}

	switch ($transtype) {
		case "Sale" :
		case "IssueGiftCard" :
		case "GiftCardSale" :
		case "ReloadGiftCard" :
		case "IssueLoyaltyCard" :
		case "LoyaltyCardSale" :
		case "LoyaltyCardReward" :
		case "LoyaltyCardAddValue" :
			$evo_payment->setTotal($send_amount);
			$evo_payment->OrderID = $process_info['order_id'];
			break;
		case "Auth":
		case "AuthForTab":
			if ($process_info['order_id'] == "0"){
				$process_info['order_id'] = create_new_order_record($process_info, $location_info);
			}
			if ($transtype=="AuthForTab" && (float)$send_amount==0) {
				$process_info['card_amount'] = $location_info['default_preauth_amount'];
				$send_amount = $process_info['card_amount'];
			}
			$evo_payment->setTotal($send_amount);
			break;
		case "PreAuthCapture":
		case "PreAuthCaptureForTab":
			$AuthQuery =
				"SELECT `auth`, `card_desc`, `amount`, `transaction_id`".
				"FROM `cc_transactions` ".
				"WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND".
				"`transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'";

			$get_transaction_info = lavu_query($AuthQuery, $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if (!empty($process_info['card_amount'])){
				$send_amount = number_format(
					($process_info['card_amount'] + $process_info['tip_amount']),
					$location_info['disable_decimal'], ".", ""
					);
			}else{
				$send_amount = number_format(
					($transaction_info['amount']  + $process_info['tip_amount']),
					$location_info['disable_decimal'], ".", ""
					);
			}
			if ($cn == ""){
				$cn = $transaction_info['card_desc'];
			}
			$tempTipAmt = number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$log_tip_amount = " - TIP AMOUNT: ".$tempTipAmt;
			$evo_payment->setTip($tempTipAmt);
			$evo_payment->setTotal($send_amount);
			$evo_payment->setRefTxnId($transaction_info['transaction_id']);
			break;
		case "Adjustment" :
			$adjQuery = "SELECT `amount`, `transaction_id`, `card_desc` ".
				"FROM `cc_transactions` ".
				"WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' ".
				"AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'";

			$get_transaction_info = lavu_query($adjQuery, $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$send_amount = number_format(
					($transaction_info['amount'] + $process_info['tip_amount']),
					$location_info['disable_decimal'],
					".",
					""
				 );

			if ($cn == ""){
				$cn = $transaction_info['card_desc'];
			}

			$tempTipAmt = number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$log_tip_amount = " - TIP AMOUNT: ".$tempTipAmt;

			$evo_payment->setTotal($send_amount);
			$evo_payment->setTip($tempTipAmt);
			$evo_payment->setRefTxnId($transaction_info['transaction_id']);
			break;
		case "Void":
		case "VoidPreAuthCapture":
			$voidQuery = "SELECT * FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' ".
				"AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' ".
				"AND (`action` = 'Sale' OR `action` = 'Refund')";

			$get_transaction_info = lavu_query($voidQuery , $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			$cn = $transaction_info['card_desc'];
			$evo_payment->setTotal($send_amount);
			$evo_payment->setRefTxnId($transaction_info['transaction_id']);
			if(!empty($transaction_info['ref_data'])){
				$evo_payment->setHostedVoid($transaction_info['ref_data']);
			}
			break;
		case "VoidLoyaltyCardIssueOrReload" :
		case "VoidGiftCardIssueOrReload" :
			$voidQuery = "SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`,".
				" `first_four`,`card_desc`, `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' ".
				"AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' ".
				"AND `action` = 'IssueGiftCard'";
			$get_transaction_info = lavu_query($voidQuery , $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			$cn = $transaction_info['card_desc'];
			$evo_payment->setTotal($send_amount);
			if(empty($transaction_info['transaction_id'])){
				$evo_payment->setRefTxnId($process_info['pnref']);
			}else{
				$evo_payment->setRefTxnId($transaction_info['transaction_id']);
			}
			break;

		case "Return" :
			$retQuery = "SELECT `card_desc`, `card_type` FROM `cc_transactions` WHERE `order_id` = '[1]' AND `transaction_id` = '[2]'";
			$get_card_info = lavu_query($retQuery, $process_info['order_id'], $process_info['pnref']);
			if (mysqli_num_rows($get_card_info) > 0) {
				$info = mysqli_fetch_assoc($get_card_info);
				$cn = $info['card_desc'];
				$card_type = $info['card_type'];
			}
			$returnQuery = "SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' ".
				"AND `order_id` = '[2]' AND `check` = '[3]' AND `transaction_id` = '[4]' ".
				"AND `voided` != '1' AND `action` = 'Sale'";

			$get_transaction_info = lavu_query(
					$returnQuery,
					$process_info['loc_id'],
					$process_info['order_id'],
					$process_info['check'],
					$process_info['pnref']
				);

			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if ($transaction_info['transtype'] == "Auth"){
				$process_info['pnref'] = $transaction_info['preauth_id'];
			}
			$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
			$evo_payment->setTotal($send_amount);
			$evo_payment->setRefTxnId($process_info['pnref']);
			if(!empty($transaction_info['ref_data'])){
				$evo_payment->setHostedVoid($transaction_info['ref_data']);
			}
			break;
		case "LoyaltyCardBalance" :
		case "GiftCardBalance" :
		case "CaptureAll" :
			break;
		default :
			error_log(__FUNCTION__." 31101 Unknown Transtype : $transtype");
			return "0|Unable to determine transaction type|||".$process_info['order_id'];
			break;
	}

	$transaction_vars['swipe_grade'] = 'A';
	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != ""){
			$q_fields .= ", ";
			$q_values .= ", ";
		}
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	if(false == $evo_payment->performTransaction($transtype)){
		if(!empty($evo_payment->GatewayErrorResponse)){
			if(!empty($evo_payment->GatewayErrorResponse['Messages'][0])){
				return "0|{$evo_payment->GatewayErrorResponse['Messages'][0]}";
			}
			if(!empty($evo_payment->GatewayErrorResponse['message'])){
				return "0|{$evo_payment->GatewayErrorResponse['message']}";
			}
			trigger_error(print_r($evo_payment->GatewayErrorResponse,1));
			trigger_error(print_r($evo_payment->GatewayResponse,1));
			return "0|Perform Transaction Failed. ".__LINE__;
		}else{
			trigger_error(print_r($evo_payment->GatewayErrorResponse,1));
			trigger_error(print_r($evo_payment->GatewayResponse,1));
			return "0|Perform Transaction Failed. ".__LINE__;
		}
	}

	if(empty($evo_payment->GatewayResponse)){
		return '0|Gateway Response Empty';
	}

	$status  = $evo_payment->GatewayResponse['Status'];
	$statusMessage  = $evo_payment->GatewayResponse['StatusMessage'];
	$approved = false;
	if(0 == strcasecmp ( $status , 'Successful' ) ){
		$approved = true;
	}
	if(0 == strcasecmp ( $status , 'Approved' ) ){
		$approved = true;
	}
	if(0 == strcasecmp ( $status , 'Failure' ) ){
		trigger_error('Failure!!!!!'.print_r($evo_payment->GatewayResponse,1));
		$retStr = "0| {$evo_payment->GatewayResponse['Status']} \n {$evo_payment->GatewayResponse['StatusMessage']}\n ";
		error_log(__FILE__.": {$retStr}");
		return $retStr;
	}

	if ($approved==false){
		trigger_error(print_r($evo_payment->GatewayResponse,1));
		$retStr = "0| {$evo_payment->GatewayResponse['Status']} \n {$evo_payment->GatewayResponse['StatusMessage']}\n ";
		error_log(__FILE__.": {$retStr}");
		return $retStr;
	}

	$system_id = $transaction_vars['system_id'];

	$result       = $evo_payment->GatewayResponse['StatusCode'];
	$respmsg      = $evo_payment->GatewayResponse['Status'];
	$authcode     = $evo_payment->GatewayResponse['ApprovalCode'];
	$new_pnref    = $evo_payment->GatewayResponse['TransactionId'];
	$message      = $evo_payment->GatewayResponse['StatusMessage'];
	$gateway_code = $evo_payment->GatewayResponse['StatusCode'];
	
	$process_info['last_mod_ts'] = time();
	
	update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");
	
	error_log(__FILE__."{$evo_payment->GatewayResponse['Status']} \n {$evo_payment->GatewayResponse['StatusMessage']}");
	
	$rtn = array();
	
	$rtn[] = "1"; // 0
	$rtn[] = $new_pnref;
	$rtn[] = substr($cn, -4);
	$rtn[] = $respmsg;
	$rtn[] = $authcode;
	$rtn[] = $card_type;
	$rtn[] = "";
	$rtn[] = $process_info['card_amount'];
	$rtn[] = $process_info['order_id'];
	$rtn[] = "";
	$rtn[] = ""; // 10
	$rtn[] = "";
	$rtn[] = "";
	$rtn[] = "";
	$rtn[] = $process_info['last_mod_ts'];
	
	return implode("|", $rtn);
}


function process_evo_snap($process_info, $location_info) {
	$evo_payment = new EvoSnap(
		$process_info['username'],
		$process_info['password'],
		$process_info['integration3'],
		$process_info['integration4'],
		$process_info['integration5']
	);

	$cn = false;
	$check_for_duplicate = false;
	$create_transaction_record = false;
	$ioid = false;
	$itid = false;
	$is_deposit = false;
	$card_type = "";

	$send_amount = $process_info['card_amount'];
	$transtype = $process_info['transtype'];
	if ($transtype == ""){
		$transtype = 'Sale';
	}

	$needSwipe = true;
	if(!empty($process_info['pnref'])){
		$needSwipe = false;
	}
	if(in_array($transtype, array('Void','VoidPreAuthCapture','VoidLoyaltyCardIssueOrReload','VoidGiftCardIssueOrReload','LoyaltyCardBalance','GiftCardBalance','CaptureAll','Adjustment'))){
		$needSwipe = false;
	}
	if($needSwipe == true){
		$cn = $process_info['card_number'];
		$card_type = getCardTypeFromNumber($cn);
		if(empty($cn)){
			return "0|Unable to determine card number";
		}

		if(strlen($process_info['mag_data']) == 0){
			return "0|No card swipe data. Unable to process.";
		}

		if ($process_info['encrypted'] != "1"){
			return "0|Unable to process unencrypted swipes.";
		}

		if($process_info["reader"] != "idynamo"){
			return "0|Reader not iDynamo.";
		}

		if(false == $evo_payment->set_iDyanamo($process_info['mag_data'])){
			return "0|Card read failed. Try Again.\n";
		}
		$evo_payment->CardType = $card_type;
		$swiped = 'y';
		$swipe_grade = 'A';
	}

	if ($process_info['set_pay_type'] != "") {
		$pay_type = $process_info['set_pay_type'];
		$pay_type_id = $process_info['set_pay_type_id'];
	} else {
		$pay_type = "Card";
		$pay_type_id = "2";
	}

	$checkCapture = false;

	switch ($transtype) {
		case "Sale" :
		case "IssueGiftCard" :
		case "GiftCardSale" :
		case "ReloadGiftCard" :
		case "IssueLoyaltyCard" :
		case "LoyaltyCardSale" :
		case "LoyaltyCardReward" :
		case "LoyaltyCardAddValue" :
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$evo_payment->setTotal($send_amount);
			$evo_payment->OrderID = $process_info['order_id'];
			break;
		case "Auth":
		case "AuthForTab":
			$check_for_duplicate = true;
			$create_transaction_record = true;
			if ($process_info['order_id'] == "0"){
				$process_info['order_id'] = create_new_order_record($process_info, $location_info);
			}
			if ($transtype=="AuthForTab" && (float)$send_amount==0) {
				$process_info['card_amount'] = $location_info['default_preauth_amount'];
				$send_amount = $process_info['card_amount'];
			}
			$evo_payment->setTotal($send_amount);
			break;
		case "PreAuthCapture":
		case "PreAuthCaptureForTab":
		    //Need to make sure we can adjust the transaction
            $checkCapture = true;
			$AuthQuery =
				"SELECT `auth`, `card_desc`, `amount`, `transaction_id`, `server_time`".
				"FROM `cc_transactions` ".
				"WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND".
				"`transaction_id` = '[pnref]' AND `voided` != '1'";

			$get_transaction_info = lavu_query($AuthQuery, $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if (!empty($process_info['card_amount'])){
				$send_amount = number_format(
					($process_info['card_amount'] + $process_info['tip_amount']),
					$location_info['disable_decimal'], ".", ""
				);
			}else{
				$send_amount = number_format(
					($transaction_info['amount']  + $process_info['tip_amount']),
					$location_info['disable_decimal'], ".", ""
				);
			}
			if ($cn == ""){
				$cn = $transaction_info['card_desc'];
			}
			$tempTipAmt = number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$log_tip_amount = " - TIP AMOUNT: ".$tempTipAmt;
			$evo_payment->setTip($tempTipAmt);
			$evo_payment->setTotal($send_amount);
			$evo_payment->setRefTxnId($transaction_info['transaction_id']);
			break;
		case "Adjustment" :
		    //Make sure we can adjust transaction
            $checkCapture = true;
			$adjQuery = "SELECT `amount`, `transaction_id`, `card_desc`, `server_time` ".
				"FROM `cc_transactions` ".
				"WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' ".
				"AND `transaction_id` = '[pnref]' AND `voided` != '1'";

			$get_transaction_info = lavu_query($adjQuery, $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$send_amount = number_format(
				($transaction_info['amount'] + $process_info['tip_amount']),
				$location_info['disable_decimal'],
				".",
				""
			);

			if ($cn == ""){
				$cn = $transaction_info['card_desc'];
			}

			$tempTipAmt = number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$log_tip_amount = " - TIP AMOUNT: ".$tempTipAmt;

			$evo_payment->setTotal($send_amount);
			$evo_payment->setTip($tempTipAmt);
			$evo_payment->setRefTxnId($transaction_info['transaction_id']);
			break;
		case "Void":
		case "VoidPreAuthCapture":
		    //make sure we can void the transaction
            $checkCapture = true;
			$voidQuery = "SELECT * FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' ".
				"AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' ".
				"AND (`action` = 'Sale' OR `action` = 'Refund')";

			$get_transaction_info = lavu_query($voidQuery , $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			$cn = $transaction_info['card_desc'];
			$create_transaction_record = true;
			$evo_payment->setTotal($send_amount);
			$evo_payment->setRefTxnId($transaction_info['transaction_id']);
			if(!empty($transaction_info['ref_data'])){
				$evo_payment->setHostedVoid($transaction_info['ref_data']);
			}
			break;
		case "VoidLoyaltyCardIssueOrReload" :
		case "VoidGiftCardIssueOrReload" :
			$voidQuery = "SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`,".
				" `first_four`,`card_desc`, `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' ".
				"AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' ".
				"AND `action` = 'IssueGiftCard'";
			$get_transaction_info = lavu_query($voidQuery , $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			$cn = $transaction_info['card_desc'];
			$create_transaction_record = true;
			$evo_payment->setTotal($send_amount);
			if(empty($transaction_info['transaction_id'])){
				$evo_payment->setRefTxnId($process_info['pnref']);
			}else{
				$evo_payment->setRefTxnId($transaction_info['transaction_id']);
			}
			break;

		case "Return" :
			$retQuery = "SELECT `card_desc`, `card_type` FROM `cc_transactions` WHERE `order_id` = '[1]' AND `transaction_id` = '[2]'";
			$get_card_info = lavu_query($retQuery, $process_info['order_id'], $process_info['pnref']);
			if (mysqli_num_rows($get_card_info) > 0) {
				$info = mysqli_fetch_assoc($get_card_info);
				$cn = $info['card_desc'];
				$card_type = $info['card_type'];
			}
			$returnQuery = "SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' ".
				"AND `order_id` = '[2]' AND `check` = '[3]' AND `transaction_id` = '[4]' ".
				"AND `voided` != '1' AND `action` = 'Sale'";

			$get_transaction_info = lavu_query(
				$returnQuery,
				$process_info['loc_id'],
				$process_info['order_id'],
				$process_info['check'],
				$process_info['pnref']
			);

			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if ($transaction_info['transtype'] == "Auth"){
				$process_info['pnref'] = $transaction_info['preauth_id'];
			}
			$create_transaction_record = true;
			$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
			$evo_payment->setTotal($send_amount);
			$evo_payment->setRefTxnId($process_info['pnref']);
			if(!empty($transaction_info['ref_data'])){
				$evo_payment->setHostedVoid($transaction_info['ref_data']);
			}
			break;
		case "LoyaltyCardBalance" :
		case "GiftCardBalance" :
		case "CaptureAll" :
			break;
		default :
			error_log(__FUNCTION__." 31101 Unknown Transtype : $transtype");
			return "0|Unable to determine transaction type|||".$process_info['order_id'];
			break;
	}

	$transaction_vars = generate_transaction_vars(
		$process_info,
		$location_info,
		$cn,
		$card_type,
		$transtype,
		'A',
		$pay_type,
		$pay_type_id,
		$process_info['card_amount']
	);


	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != ""){
			$q_fields .= ", ";
			$q_values .= ", ";
		}
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	if($checkCapture){
	    //if we cannot capture the transaction, flag it as such.
	    if(false == $evo_payment->canCaptureTransaction($transaction_info)){
            $AuthQuery =
                "UPDATE `cc_transactions` ".
                "SET `auth` = 0 ".
                "WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND ".
                "`transaction_id` = '[pnref]' AND `voided` != '1'";

            lavu_query($AuthQuery, $process_info);
	        return "0|Transaction included in previously closed batch";
        }
    }

	if(false == $evo_payment->performTransaction($transtype)){
		if(!empty($evo_payment->GatewayErrorResponse)){
			if(!empty($evo_payment->GatewayErrorResponse['Messages'][0])){
				return "0|{$evo_payment->GatewayErrorResponse['Messages'][0]}";
			}
			if(!empty($evo_payment->GatewayErrorResponse['message'])){
				return "0|{$evo_payment->GatewayErrorResponse['message']}";
			}
			trigger_error(print_r($evo_payment->GatewayErrorResponse,1));
			trigger_error(print_r($evo_payment->GatewayResponse,1));
			return "0|Perform Transaction Failed. ".__LINE__;
		}else{
			trigger_error(print_r($evo_payment->GatewayErrorResponse,1));
			trigger_error(print_r($evo_payment->GatewayResponse,1));
			return "0|Perform Transaction Failed. ".__LINE__;
		}
	}

	if(empty($evo_payment->GatewayResponse)){
		return '0|Gateway Response Empty';
	}

	$status  = $evo_payment->GatewayResponse['Status'];
	$statusMessage  = $evo_payment->GatewayResponse['StatusMessage'];
	$approved = false;
	if(0 == strcasecmp ( $status , 'Successful' ) ){
		$approved = true;
	}
	if(0 == strcasecmp ( $status , 'Approved' ) ){
		$approved = true;
	}
	if(0 == strcasecmp ( $status , 'Failure' ) ){
		trigger_error('Failure!!!!!'.print_r($evo_payment->GatewayResponse,1));
		$retStr = "0| {$evo_payment->GatewayResponse['Status']} \n {$evo_payment->GatewayResponse['StatusMessage']}\n ";
		error_log(__FILE__.": {$retStr}");
		return $retStr;
	}

	if ($approved==false){
		trigger_error(print_r($evo_payment->GatewayResponse,1));
		$retStr = "0| {$evo_payment->GatewayResponse['Status']} \n {$evo_payment->GatewayResponse['StatusMessage']}\n ";
		error_log(__FILE__.": {$retStr}");
		return $retStr;
	}

	$system_id = 0;
	if (in_array($transtype, array("Sale", "Auth", "AuthForTab", "Void", "Return","IssueGiftCard","IssueLoyaltyCard","GiftCardSale"))) {
		$ccQuery = "INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)";
		$preliminary_record_transaction = lavu_query($ccQuery, $transaction_vars);
		if (!$preliminary_record_transaction){
			return "0|Failed to create transaction record. {$process_info['order_id']}||{$process_info['order_id']}";
		}
		$system_id = lavu_insert_id();
	}

	$result       = $evo_payment->GatewayResponse['StatusCode'];
	$respmsg      = $evo_payment->GatewayResponse['Status'];
	$authcode     = $evo_payment->GatewayResponse['ApprovalCode'];
	$new_pnref    = $evo_payment->GatewayResponse['TransactionId'];
	$message      = $evo_payment->GatewayResponse['StatusMessage'];
	$gateway_code = $evo_payment->GatewayResponse['StatusCode'];

	$process_info['last_mod_ts'] = time();

	update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

	error_log(__FILE__."{$evo_payment->GatewayResponse['Status']} \n {$evo_payment->GatewayResponse['StatusMessage']}");

	$rtn = array();

	$rtn[] = "1"; // 0
	$rtn[] = $new_pnref;
	$rtn[] = substr($cn, -4);
	$rtn[] = $respmsg;
	$rtn[] = $authcode;
	$rtn[] = $card_type;
	$rtn[] = "";
	$rtn[] = $process_info['card_amount'];
	$rtn[] = $process_info['order_id'];
	$rtn[] = "";
	$rtn[] = ""; // 10
	$rtn[] = "";
	$rtn[] = "";
	$rtn[] = "";
	$rtn[] = $process_info['last_mod_ts'];

	return implode("|", $rtn);
}
?>
