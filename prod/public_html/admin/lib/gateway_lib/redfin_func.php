<?php

	function process_redfin2($process_info, $location_info, $transaction_vars) {

		global $server_order_prefix;

		$transtype = $process_info['transtype'];
		$ext_data = $process_info['ext_data'];

		$debug = print_r($process_info, true)."\n\n";

		$url = "https://secure.redfinnet.com/smartpayments/transact3.asmx/ProcessCreditCard";
		if ($process_info['set_pay_type'] == "Gift Card") $url = "https://secure.redfinnet.com/smartpayments/transact3.asmx/ProcessGiftCard";
		else if ($process_info['set_pay_type'] == "Loyalty Card") $url = "https://secure.redfinnet.com/smartpayments/transact3.asmx/ProcessLoyaltCard";
		//$username = "test3955"; // test01poslavu
		//$password = "3955Test"; // poslavu01Test
		//$username = "pole7370";
		//$password = "OKCcc2010";
		
		$pay_type = $process_info['set_pay_type'];
		$pay_type_id = $process_info['set_pay_type_id'];

		$send_amount = $process_info['card_amount'];

		$send_card_number = $process_info['card_number'];
		$send_exp_month = $process_info['exp_month'];
		$send_exp_year = $process_info['exp_year'];
		
		$cn = $send_card_number;
		$first_four = substr($cn, 0, 4);
		$card_type = $transaction_vars['card_type'];
		$force_track1 = false;

		$swiped = "0";
		$swipe_grade = "H";		
		$send_mag_data = "";
		if ($process_info['mag_data'] && ($process_info['reader']=="idynamo" || $process_info['reader']=="udynamo")) {
			$swiped = "1";
			$track1good = false;
			$track2good = false;
			$split_mag_data = explode("|", $process_info['mag_data']);
			$encrypted_track1 = $split_mag_data[2];
			$encrypted_track2 = $split_mag_data[3];
			$dukpt_ksn = $split_mag_data[9];
			$ext_data .= "<SecureFormat>MagneSafeV1</SecureFormat>";
			if (!strstr($encrypted_track1, "E?") && ($encrypted_track1 != "")) {
				$ext_data .= "<Track1>".$encrypted_track1."</Track1>";
				$swipe_grade = "C";
				$track1good = true;
			} else if ($force_track1) {
				$ext_data .= "<Track1></Track1>";
			}
			if (!strstr($encrypted_track2, "E?") && ($encrypted_track2 != "")) {
				$ext_data .= "<Track2>".$encrypted_track2."</Track2>";
				if ($swipe_grade == "C") $swipe_grade = "A";
				else $swipe_grade = "B";
				$track2good = true;
			}
			$ext_data .= "<SecurityInfo>".$dukpt_ksn."</SecurityInfo>";
			$send_card_number = "";
			$send_exp_month = "";
			$send_exp_year = "";
			
			if (!$track1good && !$track2good) {
				
				if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "REDFIN", $transtype, "BR", $debug);

				$log_string = "REDFIN[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: ".$process_info['card_amount']." - TRANSTYPE: $transtype - CARD TYPE: ??? - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4)." - SWIPED: ".$swiped."[--CR--]EXT_DATA: ".$process_info['ext_data']."[--CR--]MAG DATA: ".$process_info['mag_data']."[--CR--]";
				write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

				return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."||Error|||";
			}
			$send_mag_data = "";
		
		} else if ($process_info['mag_data']) {
		
			$swiped = "1";
			$swipe_grade = "G";
			$tracks = explode("?", $process_info['mag_data']);
			$track2_index = 1;
			if (strstr($tracks[0], "=")) $track2_index = 0;
			if (count($tracks) > $track2_index) {
				$track2 = $tracks[$track2_index];
				if (($track2 != "") && ($track2 != "ERROR")) {
					$send_mag_data = str_replace(";", "", $track2);
					$swipe_grade = "E";
				}
			}
		}
		
		$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";

		$street = "";
		$zip = "";
		$city = "";
		$state = "";
		$country = "";

		$result = "";
		$log_tip_amount = "";
		$temp_data = "";
		
		$send_inv_num = $process_info['loc_id'].str_replace("-", "", $process_info['order_id']);
		
		if ($transtype == "") $transtype = "Sale";

		switch ($transtype) {
			case ($transtype=="Sale" || $transtype=="") :
				$rftranstype = "Sale";
				$ext_data .= "<Force>T</Force>";
				break;
			case ($transtype=="Auth" || $transtype=="AuthForTab") :
				$rftranstype = "Auth";
				$ext_data .= "<Force>T</Force>";
				if ($process_info['order_id'] == "0") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
				if ($transtype=="AuthForTab" && (float)$send_amount==0) {
					$process_info['card_amount'] = $location_info['default_preauth_amount'];
					$send_amount = $process_info['card_amount'];
				}
				break;
			case ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") :
				$rftranstype = "Force";
				$get_transaction_info = lavu_query("SELECT `auth`, `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				if (!empty($process_info['card_amount'])) $send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				else $send_amount = number_format(($transaction_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				if ($cn == "") $cn = $transaction_info['card_desc'];
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				$include_inv_num = true;
				break;
			case "Adjustment" :
				$rftranstype = "Adjustment";
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				$send_inv_num = "";
				break;
			case ($transtype=="Void" || $transtype=="VoidPreAuthCapture") :
				$rftranstype = "Void";
				$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				if ($transaction_info['action'] == "Sale") $void_action = "Payment Voided";
				else if ($transaction_info['action'] == "Refund") $void_action = "Refund Voided";
				$card_type = $transaction_info['card_type'];
				$first_four = $transaction_info['first_four'];
				$process_info['card_number'] = $transaction_info['card_desc'];
				$process_info['last_mod_ts'] = time();
				if (($transaction_info['transtype']=="Auth" || $transaction_info['transtype']=="AuthForTab") && ($transaction_info['auth']=="1" || $transaction_info['auth']=="2")) {
					$mark_as_voided = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = 'VOIDED', `last_mod_ts` = '[3]' WHERE `id` = '[4]'", $process_info['more_info'], $process_info['server_id'], time(), $transaction_info['id']);
					$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - ".$transaction_info['amount']."), `card_desc` = '', `transaction_id` = '', `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
					$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - ".$transaction_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $process_info['loc_id'], $process_info['order_id'], $transaction_info['check']);

					// ??? should auth_by and auth_by_id replace server_name and server_id ???
					actionLogItGW($process_info, $void_action, "Check ".$process_info['check']." - ".$pay_type." - ".priceFormatGW($transaction_info['amount']));

					return "1|VOIDED|".substr($cn, (strlen($cn) - 4), 4)."|APPROVED|VOIDED|".$transaction_info['card_type']."||";
				}
				break;
			case "Return" :
				$rftranstype = "Return";
				$ext_data .= "<Force>T</Force>";
				if (empty($process_info['pnref'])) {
					$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $process_info['loc_id'], $process_info['order_id'], $process_info['check'], substr($process_info['card_number'], (strlen($process_info['card_number']) - 4), 4));
					$transaction_info = mysqli_fetch_assoc($get_transaction_info);
					$process_info['pnref'] = $transaction_info['transaction_id'];
				}
				$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
				if ((float)$process_info['refund_tip_amount'] != 0) $ext_data .= "<TipAmt>".$process_info['refund_tip_amount']."</TipAmt>";
				break;
			case "CaptureAll" :
				$rftranstype = "CaptureAll";
				break;
			case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") :
				$rftranstype = "Activate";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = true;
				if (!empty($send_mag_data)) $temp_data = "S".disinterpret($send_mag_data);
				else $temp_data = "K".disinterpret($send_card_number);
				$send_exp_month = "";
				$send_exp_year = "";
				$dupe_info = checkForPreviousIssueOrReload($process_info);
				if (!empty($dupe_info)) return $dupe_info;
				break;
			case ($transtype=="ReloadGiftCard" || $transtype=="ReloadLoyaltyCard") :
				$rftranstype = "Reload";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = true;
				if (!empty($send_mag_data)) $temp_data = "S".disinterpret($send_mag_data);
				else $temp_data = "K".disinterpret($send_card_number);
				$send_exp_month = "";
				$send_exp_year = "";
				$dupe_info = checkForPreviousIssueOrReload($process_info);
				if (!empty($dupe_info)) return $dupe_info;
				break;
			case ($transtype=="VoidGiftCardIssueOrReload" || $transtype=="VoidLoyaltyCardIssueOrReload") :
				//$rftranstype = "Void";
				$rftranstype = "Redeem";
				$get_transaction_info = lavu_query("SELECT `total_collected`, `temp_data` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `transaction_id` = '[3]' AND `pay_type_id` = '[4]' AND `voided` = '0'", $process_info['loc_id'], $process_info['order_id'], $process_info['pnref'], $pay_type_id);
				if (mysqli_num_rows($get_transaction_info) > 0) {
					$transaction_info = mysqli_fetch_assoc($get_transaction_info);
					$send_amount = $transaction_info['total_collected'];
					if (substr($transaction_info['temp_data'], 0, 1)=="K") $send_card_number = interpret(substr($transaction_info['temp_data'], 1));
					else {
						$send_mag_data = interpret(substr($transaction_info['temp_data'], 1));
						$card_info = extract_card_info($send_mag_data);
						$send_card_number = $card_info['number'];
					}
				}
				$ext_data .= "<Force>T</Force>";
				break;
			case ($transtype=="DeactivateGiftCard" || $transtype=="DeactivateLoyaltyCard") :
				$rftranstype = "Deactivate";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = false;
				$send_exp_month = "";
				$send_exp_year = "";
				break;
			case (($transtype == "GiftCardBalance") || ($transtype == "LoyaltyCardBalance")) :
				$rftranstype = "Inquire";
				$ext_data .= "<Force>T</Force>";
				$send_amount = "";
				$include_inv_num = true;
				$send_exp_month = "";
				$send_exp_year = "";
				break;
			case ($transtype=="GiftCardSale" || $transtype=="LoyaltyCardSale") :
				$rftranstype = "Redeem";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = true;
				if ($transtype == "LoyaltyCardSale") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
				if (!empty($send_mag_data)) $temp_data = "S".disinterpret($send_mag_data);
				else $temp_data = "K".disinterpret($send_card_number);
				$send_exp_month = "";
				$send_exp_year = "";
				break;
			case ($transtype=="GiftCardVoid" || $transtype=="LoyaltyCardVoid") :
				//$rftranstype = "Void";
				$get_transaction_info = lavu_query("SELECT `action`, `temp_data` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `transaction_id` = '[3]' AND `pay_type_id` = '[4]' AND `voided` = '0'", $process_info['loc_id'], $process_info['order_id'], $process_info['pnref'], $pay_type_id);
				if (mysqli_num_rows($get_transaction_info) > 0) {
					$transaction_info = mysqli_fetch_assoc($get_transaction_info);
					if ($transaction_info['action'] == "Refund") $rftranstype = "Redeem";
					else $rftranstype = "Reload";
					if (substr($transaction_info['temp_data'], 0, 1) == "K") $send_card_number = interpret(substr($transaction_info['temp_data'], 1));
					else {
						$send_mag_data = interpret(substr($transaction_info['temp_data'], 1));
						$card_info = extract_card_info($send_mag_data);
						$send_card_number = $card_info['number'];
					}
				}
				if ($transtype == "LoyaltyCardVoid") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
				break;
			case ($transtype=="GiftCardReturn" || $transtype=="LoyaltyCardReturn") :
				$rftranstype = "Refund";
				$ext_data .= "<Force>T</Force>";
				if (empty($process_info['pnref'])) {
					$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $process_info['loc_id'], $process_info['order_id'], $process_info['check'], substr($cn, (strlen($cn) - 4), 4));
					$transaction_info = mysqli_fetch_assoc($get_transaction_info);
					$process_info['pnref'] = $transaction_info['transaction_id'];
				}
				$include_inv_num = true;
				if ($transtype == "LoyaltyCardReturn") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
				if (!empty($send_mag_data)) $temp_data = "S".disinterpret($send_mag_data);
				else $temp_data = "K".disinterpret($send_card_number);
				$send_exp_month = "";
				$send_exp_year = "";
				break;
			default: break;
		}

		$ext_data .= "<RegisterNum>1</RegisterNum>";
		
		$postvars = array();
		$postvars['UserName'] = $process_info['username'];
		$postvars['Password'] = $process_info['password'];
		$postvars['TransType'] = $rftranstype;
		$postvars['Amount'] = $send_amount;
		$postvars['CardNum'] = $send_card_number; // required even when mag data is present
		$postvars['ExpDate'] = $send_exp_month.$send_exp_year;
		$postvars['CVNum'] = $process_info['card_cvn'];
		$postvars['MagData'] = $send_mag_data;
		$postvars['NameOnCard'] = strtoupper($process_info['name_on_card']);
		$postvars['PNRef'] = $process_info['pnref'];
		$postvars['Zip'] = $zip;
		$postvars['Street'] = $street;
		$postvars['InvNum'] = $send_inv_num;
		$postvars['ExtData'] = $ext_data;
		
		$debug .= "\n\n".$url."\n\n".print_r($postvars, true)."\n\n";

		$post_data = "";
		foreach ($postvars as $key => $val) {
			if ($post_data != "") $post_data .= "&";
			$post_data .= $key."=".rawurlencode($val);
		}		

		$transaction_vars['swipe_grade'] = $swipe_grade;
		$transaction_vars['transtype'] = $transtype;

		$q_fields = "";
		$q_values = "";
		$keys = array_keys($transaction_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}

		$system_id = $transaction_vars['system_id'];

		ConnectionHub::closeAll();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1) ;
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		
		$xml = simplexml_load_string($response);
	
		$debug .= "\n\n".print_r($xml, true);

		$result = $xml->Result;
		$respmsg = $xml->RespMSG;
		$authcode = $xml->AuthCode;
		$new_pnref = $xml->PNRef;
		$message = $xml->Message;
		$r_ext_data = explode(",", $xml->ExtData);

		$a_ext_data = array();
		foreach ($r_ext_data as $ext) {
			$ext_parts = explode("=", $ext);
			if (count($ext_parts) >= 2) {
				$ext_parts1_parts = explode("<", $ext_parts[1]);
				$a_ext_data[$ext_parts[0]] = $ext_parts1_parts[0];
			}
		}
		if (isset($a_ext_data['CardType'])) $card_type = $a_ext_data['CardType'];

		//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "RedFin", $process_info['reader'], $transtype, $result, $respmsg, $card_type, $swiped, $pay_type);
		
		$log_string = "REDFIN[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: $send_amount".$log_tip_amount." - TRANSTYPE: $transtype - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $authcode - CARD TYPE: $card_type - LAST FOUR: ".substr($cn, -4)." - SWIPED: ".$swiped."[--CR--]EXT_DATA: ".$ext_data."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
		$log_string .= "MESSAGE: ".$message."[--CR--]";
		$log_string .= "RECEIVED EXT_DATA: ".join(" - ", $r_ext_data)."[--CR--]";
		write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

		if (($respmsg == "") && ($message != "")) $respmsg = $message;

		$approved = "0";
		if (strtoupper($respmsg)=="APPROVED" || ($result=="0" && (strstr($message, "Balance:") || strstr($message, "BalanceAmount:") || strstr($message, "GiftCardBalanceAmount:") || strstr($message, "LoyaltyCardBalanceAmount:") || ($respmsg=="" && $message=="")))) $approved = "1";	
		if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "REDFIN", $transtype, $approved, $debug);
											
		if ($approved == "1") {

			$new_balance = "";
			if (in_array($transtype, array("IssueGiftCard", "IssueLoyaltyCard", "ReloadGiftCard", "ReloadLoyaltyCard", "GiftCardSale", "LoyaltyCardSale", "GiftCardVoid", "LoyaltyCardVoid"))) {
				if (strstr(strtolower($respmsg), "balance")) {
					$msg_parts = explode(":", $respmsg);
					if (count($msg_parts) >= 2) {
						$bal_parts = explode("-", $msg_parts[1]);
						$new_balance = trim($bal_parts[0]);
					}
					if (strstr($transtype, "Void")) $process_info['info'] = $new_balance;
					else $process_info['more_info'] = $new_balance;
				}
			}

			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", $temp_data, "", "", "");
		
			if ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") {
				$split_message = explode(":", $message);
				$authcode = trim($split_message[1]);
			}
		
			return "1|$new_pnref|".substr($cn, -4)."|$respmsg|$authcode|$card_type|||".$process_info['order_id']."|||$new_balance";

		} else {

			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

			if (($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") && (strstr($respmsg, "ALREADY ACTIVE") || strstr($message, "CARD ACTIVE")))  {
				if ($transtype == "IssueGiftCard") $process_info['transtype'] = "ReloadGiftCard";
				else if ($transtype == "IssueLoyaltyCard") $process_info['transtype'] = "ReloadLoyaltyCard";
				return process_redfin($process_info, $location_info);
			}

			if (strpos($respmsg, "exception") || strlen($respmsg)>125) $respmsg = "There is a problem with the gateway.";
			else if (empty($respmsg)) $respmsg = "No response received from gateway.";

			$resptitle = "Declined";
			if ($result != "12") {
				$resptitle = "Error";
				$respmsg .= " Please call RedFin support at 866-834-9576 for more information.";
			}
		
			return "0|$respmsg|$new_pnref|".$process_info['order_id']."||".$resptitle;
		}
	}

function process_redfin($process_info, $location_info) {

	global $server_order_prefix;

	$transtype = $process_info['transtype'];
	$ext_data = $process_info['ext_data'];

	$debug = print_r($process_info, true)."\n\n";

	$url = "https://secure.redfinnet.com/smartpayments/transact3.asmx/ProcessCreditCard";
	if ($process_info['set_pay_type'] == "Gift Card") $url = "https://secure.redfinnet.com/smartpayments/transact3.asmx/ProcessGiftCard";
	else if ($process_info['set_pay_type'] == "Loyalty Card") $url = "https://secure.redfinnet.com/smartpayments/transact3.asmx/ProcessLoyaltCard";
	//$username = "test3955"; // test01poslavu
	//$password = "3955Test"; // poslavu01Test
	//$username = "pole7370";
	//$password = "OKCcc2010";

	$pay_type = $process_info['set_pay_type'];
	$pay_type_id = $process_info['set_pay_type_id'];

	$send_amount = $process_info['card_amount'];

	$send_card_number = $process_info['card_number'];
	$send_exp_month = $process_info['exp_month'];
	$send_exp_year = $process_info['exp_year'];

	$cn = $send_card_number;
	$first_four = substr($cn, 0, 4);
	$card_type = getCardTypeFromNumber($cn);

	$swiped = "0";
	$swipe_grade = "H";
	$send_mag_data = "";
	if ($process_info['mag_data'] && ($process_info['reader']=="idynamo" || $process_info['reader']=="udynamo")) {
		$swiped = "1";
		$track1good = false;
		$track2good = false;
		$split_mag_data = explode("|", $process_info['mag_data']);
		$encrypted_track1 = $split_mag_data[2];
		$encrypted_track2 = $split_mag_data[3];
		$dukpt_ksn = $split_mag_data[9];
		$ext_data .= "<SecureFormat>MagneSafeV1</SecureFormat>";
		if (!strstr($encrypted_track1, "E?") && ($encrypted_track1 != "")) {
			$ext_data .= "<Track1>".$encrypted_track1."</Track1>";
			$swipe_grade = "C";
			$track1good = true;
		} else if ($force_track1) {
			$ext_data .= "<Track1></Track1>";
		}
		if (!strstr($encrypted_track2, "E?") && ($encrypted_track2 != "")) {
			$ext_data .= "<Track2>".$encrypted_track2."</Track2>";
			if ($swipe_grade == "C") $swipe_grade = "A";
			else $swipe_grade = "B";
			$track2good = true;
		}
		$ext_data .= "<SecurityInfo>".$dukpt_ksn."</SecurityInfo>";
		$send_card_number = "";
		$send_exp_month = "";
		$send_exp_year = "";

		if (!$track1good && !$track2good) {

			if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "REDFIN", $transtype, "BR", $debug);

			$log_string = "REDFIN[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: ".$process_info['card_amount']." - TRANSTYPE: $transtype - CARD TYPE: ??? - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4)." - SWIPED: ".$swiped."[--CR--]EXT_DATA: ".$process_info['ext_data']."[--CR--]MAG DATA: ".$process_info['mag_data']."[--CR--]";
			write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

			return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."||Error|||";
		}
		$send_mag_data = "";

	} else if ($process_info['mag_data']) {

		$swiped = "1";
		$swipe_grade = "G";
		$tracks = explode("?", $process_info['mag_data']);
		$track2_index = 1;
		if (strstr($tracks[0], "=")) $track2_index = 0;
		if (count($tracks) > $track2_index) {
			$track2 = $tracks[$track2_index];
			if (($track2 != "") && ($track2 != "ERROR")) {
				$send_mag_data = str_replace(";", "", $track2);
				$swipe_grade = "E";
			}
		}
	}

	$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";

	$street = "";
	$zip = "";
	$city = "";
	$state = "";
	$country = "";

	$result = "";
	$log_tip_amount = "";
	$temp_data = "";

	$send_inv_num = $process_info['loc_id'].str_replace("-", "", $process_info['order_id']);

	$check_for_duplicate = false;
	$create_transaction_record = false;
	if ($transtype == "") $transtype = "Sale";

	switch ($transtype) {
		case ($transtype=="Sale" || $transtype=="") :
			$rftranstype = "Sale";
			$ext_data .= "<Force>T</Force>";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			break;
		case ($transtype=="Auth" || $transtype=="AuthForTab") :
			$rftranstype = "Auth";
			$ext_data .= "<Force>T</Force>";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			if ($process_info['order_id'] == "0") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
			if ($transtype=="AuthForTab" && (float)$send_amount==0) {
				$process_info['card_amount'] = $location_info['default_preauth_amount'];
				$send_amount = $process_info['card_amount'];
			}
			break;
		case ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") :
			$rftranstype = "Force";
			$get_transaction_info = lavu_query("SELECT `auth`, `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if (!empty($process_info['card_amount'])) $send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
			else $send_amount = number_format(($transaction_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
			if ($cn == "") $cn = $transaction_info['card_desc'];
			$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$include_inv_num = true;
			break;
		case "Adjustment" :
			$rftranstype = "Adjustment";
			$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$send_inv_num = "";
			break;
		case ($transtype=="Void" || $transtype=="VoidPreAuthCapture") :
			$rftranstype = "Void";
			$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if ($transaction_info['action'] == "Sale") $void_action = "Payment Voided";
			else if ($transaction_info['action'] == "Refund") $void_action = "Refund Voided";
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			$process_info['card_number'] = $transaction_info['card_desc'];
			$process_info['last_mod_ts'] = time();
			if (($transaction_info['transtype']=="Auth" || $transaction_info['transtype']=="AuthForTab") && ($transaction_info['auth']=="1" || $transaction_info['auth']=="2")) {
				$mark_as_voided = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = 'VOIDED', `last_mod_ts` = '[3]' WHERE `id` = '[4]'", $process_info['more_info'], $process_info['server_id'], time(), $transaction_info['id']);
				$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - ".$transaction_info['amount']."), `card_desc` = '', `transaction_id` = '', `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
				$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - ".$transaction_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $process_info['loc_id'], $process_info['order_id'], $transaction_info['check']);

				// ??? should auth_by and auth_by_id replace server_name and server_id ???
				actionLogItGW($process_info, $void_action, "Check ".$process_info['check']." - ".$pay_type." - ".priceFormatGW($transaction_info['amount']));

				return "1|VOIDED|".substr($cn, (strlen($cn) - 4), 4)."|APPROVED|VOIDED|".$transaction_info['card_type']."||";
			}
			$create_transaction_record = true;
			break;
		case "Return" :
			$rftranstype = "Return";
			$ext_data .= "<Force>T</Force>";
			if (empty($process_info['pnref'])) {
				$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $process_info['loc_id'], $process_info['order_id'], $process_info['check'], substr($process_info['card_number'], (strlen($process_info['card_number']) - 4), 4));
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$process_info['pnref'] = $transaction_info['transaction_id'];
			}
			$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
			if ((float)$process_info['refund_tip_amount'] != 0) $ext_data .= "<TipAmt>".$process_info['refund_tip_amount']."</TipAmt>";
			$create_transaction_record = true;
			break;
		case "CaptureAll" :
			$rftranstype = "CaptureAll";
			break;
		case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") :
			$rftranstype = "Activate";
			$ext_data .= "<Force>T</Force>";
			$create_transaction_record = true;
			$include_inv_num = true;
			if (!empty($send_mag_data)) $temp_data = "S".disinterpret($send_mag_data);
			else $temp_data = "K".disinterpret($send_card_number);
			$send_exp_month = "";
			$send_exp_year = "";
			$dupe_info = checkForPreviousIssueOrReload($process_info);
			if (!empty($dupe_info)) return $dupe_info;
			break;
		case ($transtype=="ReloadGiftCard" || $transtype=="ReloadLoyaltyCard") :
			$rftranstype = "Reload";
			$ext_data .= "<Force>T</Force>";
			$create_transaction_record = true;
			$include_inv_num = true;
			if (!empty($send_mag_data)) $temp_data = "S".disinterpret($send_mag_data);
			else $temp_data = "K".disinterpret($send_card_number);
			$send_exp_month = "";
			$send_exp_year = "";
			$dupe_info = checkForPreviousIssueOrReload($process_info);
			if (!empty($dupe_info)) return $dupe_info;
			break;
		case ($transtype=="VoidGiftCardIssueOrReload" || $transtype=="VoidLoyaltyCardIssueOrReload") :
			//$rftranstype = "Void";
			$rftranstype = "Redeem";
			$get_transaction_info = lavu_query("SELECT `total_collected`, `temp_data` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `transaction_id` = '[3]' AND `pay_type_id` = '[4]' AND `voided` = '0'", $process_info['loc_id'], $process_info['order_id'], $process_info['pnref'], $pay_type_id);
			if (mysqli_num_rows($get_transaction_info) > 0) {
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$send_amount = $transaction_info['total_collected'];
				if (substr($transaction_info['temp_data'], 0, 1)=="K") $send_card_number = interpret(substr($transaction_info['temp_data'], 1));
				else {
					$send_mag_data = interpret(substr($transaction_info['temp_data'], 1));
					$card_info = extract_card_info($send_mag_data);
					$send_card_number = $card_info['number'];
				}
			}
			$ext_data .= "<Force>T</Force>";
			$create_transaction_record = true;
			break;
		case ($transtype=="DeactivateGiftCard" || $transtype=="DeactivateLoyaltyCard") :
			$rftranstype = "Deactivate";
			$ext_data .= "<Force>T</Force>";
			$create_transaction_record = true;
			$include_inv_num = false;
			$send_exp_month = "";
			$send_exp_year = "";
			break;
		case (($transtype == "GiftCardBalance") || ($transtype == "LoyaltyCardBalance")) :
			$rftranstype = "Inquire";
			$ext_data .= "<Force>T</Force>";
			$send_amount = "";
			$include_inv_num = true;
			$send_exp_month = "";
			$send_exp_year = "";
			break;
		case ($transtype=="GiftCardSale" || $transtype=="LoyaltyCardSale") :
			$rftranstype = "Redeem";
			$ext_data .= "<Force>T</Force>";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$include_inv_num = true;
			if ($transtype == "LoyaltyCardSale") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
			if (!empty($send_mag_data)) $temp_data = "S".disinterpret($send_mag_data);
			else $temp_data = "K".disinterpret($send_card_number);
			$send_exp_month = "";
			$send_exp_year = "";
			break;
		case ($transtype=="GiftCardVoid" || $transtype=="LoyaltyCardVoid") :
			//$rftranstype = "Void";
			$get_transaction_info = lavu_query("SELECT `action`, `temp_data` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `transaction_id` = '[3]' AND `pay_type_id` = '[4]' AND `voided` = '0'", $process_info['loc_id'], $process_info['order_id'], $process_info['pnref'], $pay_type_id);
			if (mysqli_num_rows($get_transaction_info) > 0) {
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				if ($transaction_info['action'] == "Refund") $rftranstype = "Redeem";
				else $rftranstype = "Reload";
				if (substr($transaction_info['temp_data'], 0, 1) == "K") $send_card_number = interpret(substr($transaction_info['temp_data'], 1));
				else {
					$send_mag_data = interpret(substr($transaction_info['temp_data'], 1));
					$card_info = extract_card_info($send_mag_data);
					$send_card_number = $card_info['number'];
				}
			}
			$create_transaction_record = true;
			if ($transtype == "LoyaltyCardVoid") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
			break;
		case ($transtype=="GiftCardReturn" || $transtype=="LoyaltyCardReturn") :
			$rftranstype = "Refund";
			$ext_data .= "<Force>T</Force>";
			if (empty($process_info['pnref'])) {
				$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $process_info['loc_id'], $process_info['order_id'], $process_info['check'], substr($cn, (strlen($cn) - 4), 4));
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$process_info['pnref'] = $transaction_info['transaction_id'];
			}
			$create_transaction_record = true;
			$include_inv_num = true;
			if ($transtype == "LoyaltyCardReturn") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
			if (!empty($send_mag_data)) $temp_data = "S".disinterpret($send_mag_data);
			else $temp_data = "K".disinterpret($send_card_number);
			$send_exp_month = "";
			$send_exp_year = "";
			break;
		default: break;
	}

	$ext_data .= "<RegisterNum>1</RegisterNum>";

	$postvars = array();
	$postvars['UserName'] = $process_info['username'];
	$postvars['Password'] = $process_info['password'];
	$postvars['TransType'] = $rftranstype;
	$postvars['Amount'] = $send_amount;
	$postvars['CardNum'] = $send_card_number; // required even when mag data is present
	$postvars['ExpDate'] = $send_exp_month.$send_exp_year;
	$postvars['CVNum'] = $process_info['card_cvn'];
	$postvars['MagData'] = $send_mag_data;
	$postvars['NameOnCard'] = strtoupper($process_info['name_on_card']);
	$postvars['PNRef'] = $process_info['pnref'];
	$postvars['Zip'] = $zip;
	$postvars['Street'] = $street;
	$postvars['InvNum'] = $send_inv_num;
	$postvars['ExtData'] = $ext_data;

	$debug .= "\n\n".$url."\n\n".print_r($postvars, true)."\n\n";

	$post_data = "";
	foreach ($postvars as $key => $val) {
		if ($post_data != "") $post_data .= "&";
		$post_data .= $key."=".rawurlencode($val);
	}

	$transaction_vars = generate_transaction_vars($process_info, $location_info, $process_info['card_number'], $card_type, $transtype, $swipe_grade, $pay_type, $pay_type_id, $send_amount);


	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	$system_id = 0;
	if ($create_transaction_record) {
		$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
		if (!$preliminary_record_transaction) return "0|Failed to create transaction record.||".$process_info['order_id'];
		$system_id = lavu_insert_id();
	}

	ConnectionHub::closeAll();

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1) ;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	$response = curl_exec($ch);
	curl_close($ch);

	$xml = simplexml_load_string($response);

	$debug .= "\n\n".print_r($xml, true);

	$result = $xml->Result;
	$respmsg = $xml->RespMSG;
	$authcode = $xml->AuthCode;
	$new_pnref = $xml->PNRef;
	$message = $xml->Message;
	$r_ext_data = explode(",", $xml->ExtData);

	$a_ext_data = array();
	foreach ($r_ext_data as $ext) {
		$ext_parts = explode("=", $ext);
		if (count($ext_parts) >= 2) {
			$ext_parts1_parts = explode("<", $ext_parts[1]);
			$a_ext_data[$ext_parts[0]] = $ext_parts1_parts[0];
		}
	}
	if (isset($a_ext_data['CardType'])) $card_type = $a_ext_data['CardType'];

	//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "RedFin", $process_info['reader'], $transtype, $result, $respmsg, $card_type, $swiped, $pay_type);

	$log_string = "REDFIN[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: $send_amount".$log_tip_amount." - TRANSTYPE: $transtype - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $authcode - CARD TYPE: $card_type - LAST FOUR: ".substr($cn, -4)." - SWIPED: ".$swiped."[--CR--]EXT_DATA: ".$ext_data."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
	$log_string .= "MESSAGE: ".$message."[--CR--]";
	$log_string .= "RECEIVED EXT_DATA: ".join(" - ", $r_ext_data)."[--CR--]";
	write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

	if (($respmsg == "") && ($message != "")) $respmsg = $message;

	$approved = "0";
	if (strtoupper($respmsg)=="APPROVED" || ($result=="0" && (strstr($message, "Balance:") || strstr($message, "BalanceAmount:") || strstr($message, "GiftCardBalanceAmount:") || strstr($message, "LoyaltyCardBalanceAmount:") || ($respmsg=="" && $message=="")))) $approved = "1";
	if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "REDFIN", $transtype, $approved, $debug);

	if ($approved == "1") {

		$new_balance = "";
		if (in_array($transtype, array("IssueGiftCard", "IssueLoyaltyCard", "ReloadGiftCard", "ReloadLoyaltyCard", "GiftCardSale", "LoyaltyCardSale", "GiftCardVoid", "LoyaltyCardVoid"))) {
			if (strstr(strtolower($respmsg), "balance")) {
				$msg_parts = explode(":", $respmsg);
				if (count($msg_parts) >= 2) {
					$bal_parts = explode("-", $msg_parts[1]);
					$new_balance = trim($bal_parts[0]);
				}
				if (strstr($transtype, "Void")) $process_info['info'] = $new_balance;
				else $process_info['more_info'] = $new_balance;
			}
		}

		update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", $temp_data, "", "", "");

		if ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") {
			$split_message = explode(":", $message);
			$authcode = trim($split_message[1]);
		}

		return "1|$new_pnref|".substr($cn, -4)."|$respmsg|$authcode|$card_type|||".$process_info['order_id']."|||$new_balance";

	} else {

		$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

		if (($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") && (strstr($respmsg, "ALREADY ACTIVE") || strstr($message, "CARD ACTIVE")))  {
			if ($transtype == "IssueGiftCard") $process_info['transtype'] = "ReloadGiftCard";
			else if ($transtype == "IssueLoyaltyCard") $process_info['transtype'] = "ReloadLoyaltyCard";
			return process_redfin($process_info, $location_info);
		}

		if (strpos($respmsg, "exception") || strlen($respmsg)>125) $respmsg = "There is a problem with the gateway.";
		else if (empty($respmsg)) $respmsg = "No response received from gateway.";

		$resptitle = "Declined";
		if ($result != "12") {
			$resptitle = "Error";
			$respmsg .= " Please call RedFin support at 866-834-9576 for more information.";
		}

		return "0|$respmsg|$new_pnref|".$process_info['order_id']."||".$resptitle;
	}
}
?>