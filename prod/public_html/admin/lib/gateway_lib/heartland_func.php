<?php
	require_once(dirname(__FILE__)."/heartland.php");
	require_once(dirname(__FILE__)."/../gateway_settings.php");

	function process_heartland2($process_info, $location_info, $transaction_vars) {
		global $heartland_server_url;
		global $heartland_stateful_url;
		if('cc_swiper_test' == $process_info['data_name']){
			$heartland_server_url =  $heartland_stateful_url;
		}

		if ($process_info['encrypted'] == "2"){
			$myParsedCryptoCard = new HeartlandMSR;
			$myParsedCryptoCard->SetMSR($process_info['mag_data']);
			$process_info['card_number'] = $myParsedCryptoCard->GetCardNumber();
		}

		$payment = new HeartlandPayments(
				$process_info['username'],
				$process_info['password'],
				$process_info['integration3'],
				$process_info['integration4'],
				$process_info['integration5'],
				$heartland_server_url
			);

		$transtype = $process_info['transtype'];
		$debug  = print_r($process_info, true)."\n\n";
		$debug .= $heartland_server_url."\n\n";
		$force_track1 = false;

		$send_amount = $process_info['card_amount'];
		$cn = $process_info['card_number']; // keep for response string
		$first_four = substr($cn, 0, 4);
		$card_type = getCardTypeFromNumber($cn);
		$swiped = "0";
		$swipe_grade = "H";
		$entry_mode = "MANUAL";
		$track2 = "";
		$log_tip_amount = "";
		$reader_present = ($process_info['reader'] == "")?"N":"Y";

		if ($process_info['mag_data']) {
			$swiped = "1";
			$swipe_grade = "G";
			$track1good = false;
			$track2good = false;
			if ($process_info['encrypted'] == "1") {
				$track1good = true;
				$track2good = true;
				$encrypted_info = idtech_encrypted($process_info['mag_data'], $process_info, $location_info, 16);
				$enc_track = $encrypted_info['track'];
				$enc_data = $encrypted_info['data'];
				if ($process_info['reader'] == "linea"){
					$enc_track = empty($encrypted_info['track2'])?1:2;
					$enc_data = $encrypted_info['track1'].$encrypted_info['track2'];
				}
				$payment->setEncryptedSwipe(
						$enc_track,
						base64_encode($enc_data),
						base64_encode($encrypted_info['ksn']),
						"03"
					);

				if ($encrypted_info['track'] == "2"){
					$swipe_grade = "B";
				}else{
					$swipe_grade = "C";
				}

			} else if ($process_info['encrypted'] == "2") {
				$track1good = true;
				$track2good = true;

				if(NULL ==$myParsedCryptoCard->bestTrack)
				{
					$debug .= "No Best Track\n\n";
					return "0|Card read failed.\nPlease try again.";
				}

				$encrypted_info['track'] = $myParsedCryptoCard->bestTrack;
				if($myParsedCryptoCard->unencrypted == false){
					$payment->setEncryptedSwipe(
						$encrypted_info['track'],
						$myParsedCryptoCard->GetCardTrack($myParsedCryptoCard->bestTrack),
						$myParsedCryptoCard->ktb,
						"02"
						);
				}else{

					if($_REQUEST['PingBack'] == 'PingMe'){error_log('Pingback');exit('PingBack'.$myParsedCryptoCard->GetCardTrack(1).$myParsedCryptoCard->GetCardTrack(2));}
					$payment->setSwiped($myParsedCryptoCard->GetCardTrack(1),str_replace(';','',$myParsedCryptoCard->GetCardTrack(2 ) ) );
				}

				if ($encrypted_info['track'] == "2"){
					$swipe_grade = "B";
				}else{
					$swipe_grade = "C";
				}
				
			} else if ($process_info['encrypted'] == "3") {
			
				$enc_parts = explode("|", hexToString($process_info['mag_data']));
				$enc_data = "";
				$enc_track = 1;
				foreach ($enc_parts as $ep) {
					if (substr($ep, 0, 1)=="%" || (strstr($ep, "^") && substr($ep, -1)=="?")) {
						$track1good = true;
						$enc_data = trim($ep, "%?");
					} else if (substr($ep, 0, 1)==";" || (strstr($ep, "=") && substr($ep, -1)=="?")) {
						$track2good = true;
						$enc_track = 2;
						$enc_data = trim($ep, ";?");
					}
				}
				
				$payment->setEncryptedSwipe($enc_track, $enc_data, trim($enc_parts[(count($enc_parts) - 1)], "~"), "02");
					
				if ($track1good && $track2good) $swipe_grade = "A";
				else if ($track2good) $swipe_grade = "B";
				else if ($track3good) $swipe_grade = "C";
				
			} else {
				$split_mag_data = explode(";", $process_info['mag_data']);
				$track1 = $split_mag_data[0];
				$track2 = $split_mag_data[1];
				$dukpt_ksn = $split_mag_data[9];
				if (!strstr($track1, "E?") && ($track1 != "")) {
					$swipe_grade = "C";
					$track1good = true;
				}
				if (!strstr($track2, "E?") && ($track2 != "")) {
					if ($swipe_grade == "C"){
						$swipe_grade = "A";
					}else{
						$swipe_grade = "B";
					}
					$track2good = true;
				}
				$payment->setSwiped($track1,$track2);
			}

			if (!$track1good && !$track2good) {
				if ($location_info['gateway_debug'] == "1"){
					gateway_debug(
							$process_info['data_name'],
							$process_info['loc_id'],
							"HEARTLAND",
							$transtype,
							"BR",
							$debug
						);
				}
				$log_string = "HEARTLAND[--CR--]LOCATION: ".$process_info['loc_id'].
					" - REGISTER: ".$process_info['register'].
					" - SERVER ID: ".$process_info['server_id'].
					" - ORDER ID: ".$process_info['order_id'].
					" - AMOUNT: ".$process_info['card_amount'].
					" - TRANSTYPE: $transtype - CARD TYPE: ??? - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4).
					" - SWIPED: ".$swiped.
					"[--CR--]MAG DATA: ".$process_info['mag_data']."[--CR--]";

				write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

				return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."||Error|||";
			}

		} else{
			$payment->setKeyedIn(
					$process_info['card_number'],
					$process_info['exp_month'],
					$process_info['exp_year'],
					$process_info['card_cvn'],
					$reader_present,
					$process_info['card_present']
				);
		}

		if ($transtype == "Return") {
			$retQuery = "SELECT `card_desc`, `card_type` FROM `cc_transactions`".
				"WHERE `order_id` = '[1]' AND `transaction_id` = '[2]'";

			$get_card_info = lavu_query($retQuery, $process_info['order_id'], $process_info['pnref']);
			if (mysqli_num_rows($get_card_info) > 0) {
				$info = mysqli_fetch_assoc($get_card_info);
				$cn = $info['card_desc'];
				$card_type = $info['card_type'];
			}
		}
		if ($transtype == ""){
			$transtype = "Sale";
		}

		switch ($transtype) {
			case "Sale" :
			case "IssueGiftCard" :
			case "GiftCardSale" :
			case "ReloadGiftCard" :
			case "IssueLoyaltyCard" :
			case "LoyaltyCardSale" :
			case "LoyaltyCardReward" :
			case "LoyaltyCardAddValue" :
				$payment->setTotal($send_amount);
				break;
			case "Auth":
			case "AuthForTab":
				if ($process_info['order_id'] == "0"){
					$process_info['order_id'] = create_new_order_record($process_info, $location_info);
				}
				if ($transtype=="AuthForTab" && (float)$send_amount==0) {
					$process_info['card_amount'] = $location_info['default_preauth_amount'];
					$send_amount = $process_info['card_amount'];
				}
				$payment->setTotal($send_amount);
				break;
			case "PreAuthCapture":
			case "PreAuthCaptureForTab":
				$AuthQuery =
					"SELECT `auth`, `card_desc`, `amount`, `transaction_id`".
					"FROM `cc_transactions` ".
					"WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND".
					"`transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'";

				$get_transaction_info = lavu_query($AuthQuery, $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				if (!empty($process_info['card_amount'])){
					$send_amount = number_format(
						($process_info['card_amount'] + $process_info['tip_amount']),
						$location_info['disable_decimal'], ".", ""
						);
				}else{
					$send_amount = number_format(
						($transaction_info['amount']  + $process_info['tip_amount']),
						$location_info['disable_decimal'], ".", ""
						);
				}
				if ($cn == ""){
					$cn = $transaction_info['card_desc'];
				}
				$tempTipAmt = number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				$log_tip_amount = " - TIP AMOUNT: ".$tempTipAmt;
				$payment->setTotal($send_amount);
				$payment->setRefTxnId($transaction_info['transaction_id']);
				break;
			case "Adjustment" :
				$adjQuery = "SELECT `amount`, `transaction_id`, `card_desc` ".
					"FROM `cc_transactions` ".
					"WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' ".
					"AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'";

				$get_transaction_info = lavu_query($adjQuery, $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$send_amount = number_format(
						($transaction_info['amount'] + $process_info['tip_amount']),
						$location_info['disable_decimal'],
						".",
						""
					 );

				if ($cn == ""){
					$cn = $transaction_info['card_desc'];
				}

				$tempTipAmt = number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				$log_tip_amount = " - TIP AMOUNT: ".$tempTipAmt;

				$payment->setTotal($send_amount);
				$payment->setTip($tempTipAmt);
				$payment->setRefTxnId($transaction_info['transaction_id']);
				break;
			case "Void":
			case "GiftCardVoid":
			case "VoidPreAuthCapture":
				$voidQuery = "SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`,".
					" `first_four`,`card_desc`, `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' ".
					"AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' ".
					"AND (`action` = 'Sale' OR `action` = 'Refund')";

				$get_transaction_info = lavu_query($voidQuery , $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$card_type = $transaction_info['card_type'];
				$first_four = $transaction_info['first_four'];
				$cn = $transaction_info['card_desc'];
				$payment->setTotal($send_amount);
				$payment->setRefTxnId($transaction_info['transaction_id']);
				break;
			case "VoidLoyaltyCardIssueOrReload" :
			case "VoidGiftCardIssueOrReload" :
				$voidQuery = "SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`,".
					" `first_four`,`card_desc`, `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' ".
					"AND `transaction_id` = '[pnref]' AND `voided` != '1' ".
					"AND (`action` = 'IssueGiftCard' OR `action` = 'Reload')";
				$get_transaction_info = lavu_query($voidQuery , $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$card_type = $transaction_info['card_type'];
				$first_four = $transaction_info['first_four'];
				$cn = $transaction_info['card_desc'];
				$payment->setTotal($send_amount);
				if(empty($transaction_info['transaction_id'])){
					$payment->setRefTxnId($process_info['pnref']);	
				}else{
					$payment->setRefTxnId($transaction_info['transaction_id']);	
				}
				
				break;

			case "Return" :
				$returnQuery = "SELECT `preauth_id`, `transtype` FROM `cc_transactions` WHERE `loc_id` = '[1]' ".
					"AND `order_id` = '[2]' AND `check` = '[3]' AND `transaction_id` = '[4]' ".
					"AND `voided` != '1' AND `action` = 'Sale'";

				$get_transaction_info = lavu_query(
						$returnQuery,
						$process_info['loc_id'],
						$process_info['order_id'],
						$process_info['check'],
						$process_info['pnref']
					);

				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				if ($transaction_info['transtype'] == "Auth"){
					$process_info['pnref'] = $transaction_info['preauth_id'];
				}
				$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
				$payment->setTotal($send_amount);
				$payment->setRefTxnId($process_info['pnref']);
				break;
			case "LoyaltyCardBalance" :
			case "GiftCardBalance" :
			case "CaptureAll" :
				break;
			default :
				error_log(__FUNCTION__." 31101 Unknown Transtype : $transtype");
				return "0|Unable to determine transaction type|||".$process_info['order_id'];
				break;
		}

		$transaction_vars['transtype'] = $transtype;
		$transaction_vars['swipe_grade'] =$swipe_grade;


		$q_fields = "";
		$q_values = "";
		$keys = array_keys($transaction_vars);
		foreach ($keys as $key) {
			if ($q_fields != ""){
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}

		$system_id = $transaction_vars['system_id'];

		$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";
		$debug .= "\n\nURL: ".$heartland_server_url."\n\n";

		$payment->performTransaction($transtype);
		$result = $payment->getCode();
		$gateway_code = $payment->getResponseCode();
		$respmsg = $payment->getResponse();
		$authcode = $payment->getAuthCode();
		$new_pnref = $payment->getReference();
		$message = $payment->getMessage();

		if (empty($respmsg) && !empty($message)){
			$respmsg = $message;
		}

		$approved = "0";
		if ($gateway_code=="0" && (empty($result) || $result=="00" || $result=="10" || $result=="13")){
			$approved = "1";
		}

		$debug .= "\n\nHEARTLAND\n\n".$payment->sxe->asXML();
		$debug .= "\n\nRESULT: $result\n";
		$debug .= "RESPONSE MESSAGE: $respmsg\n$authcode\n$new_pnref\n$card_type\n\n".$payment->raw_return;

		if ($location_info['gateway_debug'] == "1"){
			gateway_debug($process_info['data_name'], $process_info['loc_id'], "HEARTLAND", $transtype, $approved, $debug);
		}

		$log_string = "HEARTLAND[--CR--]LOCATION: ".$process_info['loc_id'].
			" - REGISTER: ".$process_info['register'].
			" - SERVER ID: ".$process_info['server_id'].
			" - ORDER ID: ".$process_info['order_id'].
			" - CHECK : ".$process_info['check'].
			" - AMOUNT: $send_amount".$log_tip_amount.
			" - TRANSTYPE: $transtype".
			" - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $authcode - CARD TYPE: $card_type".
			" - RESP: $respmsg - FIRST FOUR: $first_four ".
			"- CARD DESC: ".substr($cn, (strlen($cn) - 4), 4).
			" - SWIPED: ".$swiped."[--CR--]";

		$log_string .= "RESPONSE MESSAGE:".$respmsg."[--CR--]";

		if (($message != "") && (strtolower(trim($message)) != strtolower(trim($respmsg)))){
			$log_string .= "MESSAGE: ".$message."[--CR--]";
		}

		write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

		$process_info['last_mod_ts'] = time();
		$rtn = array();

		if ($approved=="1" && $result!="10" && $result!="13") {
			
			if(!empty($payment->return_data->SplitTenderCardAmt) && ($payment->return_data->SplitTenderCardAmt != 0)){
				$process_info['card_amount'] = $payment->return_data->SplitTenderCardAmt;
			}
			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");
			if ($transtype == "CaptureAll") {
				$batch_info = $payment->getBatchInfo();
				$new_pnref = $batch_info->BatchSeqNbr;

				$process_info['card_amount'] = number_format(
						(float)$batch_info->TotalAmt,
						$location_info['disable_decimal'],
						".",
						","
					);

				$authcode = "<table cellspacing='0' cellpadding='3'>".
						"<tr><td align='center' colspan='2'>BATCH CLOSED</td></tr>".
						"<tr>".
							"<td align='right'>Batch Number:</td>".
							"<td align='left'>".$batch_info->BatchSeqNbr."</td>".
						"</tr>".
						"<tr>".
							"<td align='right'>Batch Item Count:</td>".
							"<td align='left'>".$batch_info->TxnCnt."</td>".
						"</tr>".
						"<tr>".
							"<td align='right'>Net Batch Total:</td>".
							"<td align='left'>$ ".$process_info['card_amount']."</td>".
						"</tr>".
					"</table>";
			}
			
			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = $process_info['card_amount'];
			$rtn[] = $process_info['order_id'];
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['last_mod_ts'];
			
		} else if ($approved=="1" && $result=="10") {

			if(!empty($payment->return_data->SplitTenderCardAmt) && ($payment->return_data->SplitTenderCardAmt != 0)){
				$process_info['card_amount'] = $payment->return_data->SplitTenderCardAmt;
			}else{
				$process_info['card_amount'] = $payment->return_data->AuthAmt;
			}
			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = $process_info['card_amount'];
			$rtn[] = $process_info['order_id'];
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['last_mod_ts'];

		} else if ($approved=="1" && $result=="13") {

			$process_info['card_amount'] = $payment->return_data->SplitTenderCardAmt;
			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = $process_info['card_amount'];
			$rtn[] = $process_info['order_id'];
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['last_mod_ts'];

		} else {

			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
			if ($message != ""){
				if(strtolower(trim($message)) != strtolower(trim($respmsg)) ){
					$respmsg .= " - ".$message;
				}
			}

			$resptitle = "Declined";
			if (strpos($respmsg, "InvalidSecureTrack")){
				$respmsg = "Card read failed.\nPlease swipe card again.";
			}else if (strpos($payment->raw_return, "Server was unable to read request")){
				$respmsg = "Server was unable to read request.";
				if (empty($process_info['username'])){
					$respmsg .= " Integration Data 1 is empty.";
				}else if (empty($process_info['password'])){
					$respmsg .= " Integration Data 2 is empty.";
				}else if (empty($process_info['integration3'])){
					$respmsg .= " Integration Data 3 is empty.";
				}else if (empty($process_info['integration4'])){
					$respmsg .= " Integration Data 4 is empty.";
				}else if (empty($process_info['integration5'])){
					$respmsg .= " Integration Data 5 is empty.";
				}
			} else if (empty($respmsg)) {
				$resptitle = "Error";
				$respmsg = "No response received from gateway. Please call Heartland support at 888-963-3600.";
			}
			
			$rtn[] = "0";
			$rtn[] = $respmsg;
			$rtn[] = $new_pnref;
			$rtn[] = $process_info['order_id'];
			$rtn[] = $respmsg;
			$rtn[] = $resptitle;
		}
		
		return implode("|", $rtn);
	}

function process_heartland($process_info, $location_info) {
	global $heartland_server_url;
	global $heartland_stateful_url;
	if('cc_swiper_test' == $process_info['data_name']){
		$heartland_server_url =  $heartland_stateful_url;
	}

	if ($process_info['encrypted'] == "2"){
		$myParsedCryptoCard = new HeartlandMSR;
		$myParsedCryptoCard->SetMSR($process_info['mag_data']);
		$process_info['card_number'] = $myParsedCryptoCard->GetCardNumber();
	}

	$payment = new HeartlandPayments(
		$process_info['username'],
		$process_info['password'],
		$process_info['integration3'],
		$process_info['integration4'],
		$process_info['integration5'],
		$heartland_server_url
	);

	$transtype = $process_info['transtype'];
	$debug  = print_r($process_info, true)."\n\n";
	$debug .= $heartland_server_url."\n\n";
	$force_track1 = false;

	if ($process_info['set_pay_type'] != "") {
		$pay_type = $process_info['set_pay_type'];
		$pay_type_id = $process_info['set_pay_type_id'];
	} else {
		$pay_type = "Card";
		$pay_type_id = "2";
	}

	$send_amount = $process_info['card_amount'];
	$cn = $process_info['card_number']; // keep for response string
	$first_four = substr($cn, 0, 4);
	$card_type = getCardTypeFromNumber($cn);
	$swiped = "0";
	$swipe_grade = "H";
	$entry_mode = "MANUAL";
	$track2 = "";
	$log_tip_amount = "";
	$reader_present = ($process_info['reader'] == "")?"N":"Y";

	if ($process_info['mag_data']) {
		$swiped = "1";
		$swipe_grade = "G";
		$track1good = false;
		$track2good = false;
		if ($process_info['encrypted'] == "1") {
			$track1good = true;
			$track2good = true;
			$encrypted_info = idtech_encrypted($process_info['mag_data'], $process_info, $location_info, 16);
			$enc_track = $encrypted_info['track'];
			$enc_data = $encrypted_info['data'];
			if ($process_info['reader'] == "linea"){
				$enc_track = empty($encrypted_info['track2'])?1:2;
				$enc_data = $encrypted_info['track1'].$encrypted_info['track2'];
			}
			$payment->setEncryptedSwipe(
				$enc_track,
				base64_encode($enc_data),
				base64_encode($encrypted_info['ksn']),
				"03"
			);

			if ($encrypted_info['track'] == "2"){
				$swipe_grade = "B";
			}else{
				$swipe_grade = "C";
			}

		} else if ($process_info['encrypted'] == "2") {
			$track1good = true;
			$track2good = true;

			if(NULL ==$myParsedCryptoCard->bestTrack)
			{
				$debug .= "No Best Track\n\n";
				return "0|Card read failed.\nPlease try again.";
			}

			$encrypted_info['track'] = $myParsedCryptoCard->bestTrack;
			if($myParsedCryptoCard->unencrypted == false){
				$payment->setEncryptedSwipe(
					$encrypted_info['track'],
					$myParsedCryptoCard->GetCardTrack($myParsedCryptoCard->bestTrack),
					$myParsedCryptoCard->ktb,
					"02"
				);
			}else{

				if($_REQUEST['PingBack'] == 'PingMe'){error_log('Pingback');exit('PingBack'.$myParsedCryptoCard->GetCardTrack(1).$myParsedCryptoCard->GetCardTrack(2));}
				$payment->setSwiped($myParsedCryptoCard->GetCardTrack(1),str_replace(';','',$myParsedCryptoCard->GetCardTrack(2 ) ) );
			}

			if ($encrypted_info['track'] == "2"){
				$swipe_grade = "B";
			}else{
				$swipe_grade = "C";
			}

		} else if ($process_info['encrypted'] == "3") {

			$enc_parts = explode("|", hexToString($process_info['mag_data']));
			$enc_data = "";
			$enc_track = 1;
			foreach ($enc_parts as $ep) {
				if (substr($ep, 0, 1)=="%" || (strstr($ep, "^") && substr($ep, -1)=="?")) {
					$track1good = true;
					$enc_data = trim($ep, "%?");
				} else if (substr($ep, 0, 1)==";" || (strstr($ep, "=") && substr($ep, -1)=="?")) {
					$track2good = true;
					$enc_track = 2;
					$enc_data = trim($ep, ";?");
				}
			}

			$payment->setEncryptedSwipe($enc_track, $enc_data, trim($enc_parts[(count($enc_parts) - 1)], "~"), "02");

			if ($track1good && $track2good) $swipe_grade = "A";
			else if ($track2good) $swipe_grade = "B";
			else if ($track3good) $swipe_grade = "C";

		} else {
			$split_mag_data = explode(";", $process_info['mag_data']);
			$track1 = $split_mag_data[0];
			$track2 = $split_mag_data[1];
			$dukpt_ksn = $split_mag_data[9];
			if (!strstr($track1, "E?") && ($track1 != "")) {
				$swipe_grade = "C";
				$track1good = true;
			}
			if (!strstr($track2, "E?") && ($track2 != "")) {
				if ($swipe_grade == "C"){
					$swipe_grade = "A";
				}else{
					$swipe_grade = "B";
				}
				$track2good = true;
			}
			$payment->setSwiped($track1,$track2);
		}

		if (!$track1good && !$track2good) {
			if ($location_info['gateway_debug'] == "1"){
				gateway_debug(
					$process_info['data_name'],
					$process_info['loc_id'],
					"HEARTLAND",
					$transtype,
					"BR",
					$debug
				);
			}
			$log_string = "HEARTLAND[--CR--]LOCATION: ".$process_info['loc_id'].
				" - REGISTER: ".$process_info['register'].
				" - SERVER ID: ".$process_info['server_id'].
				" - ORDER ID: ".$process_info['order_id'].
				" - AMOUNT: ".$process_info['card_amount'].
				" - TRANSTYPE: $transtype - CARD TYPE: ??? - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4).
				" - SWIPED: ".$swiped.
				"[--CR--]MAG DATA: ".$process_info['mag_data']."[--CR--]";

			write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

			return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."||Error|||";
		}

	} else{
		$payment->setKeyedIn(
			$process_info['card_number'],
			$process_info['exp_month'],
			$process_info['exp_year'],
			$process_info['card_cvn'],
			$reader_present,
			$process_info['card_present']
		);
	}

	if ($transtype == "Return") {
		$retQuery = "SELECT `card_desc`, `card_type` FROM `cc_transactions`".
			"WHERE `order_id` = '[1]' AND `transaction_id` = '[2]'";

		$get_card_info = lavu_query($retQuery, $process_info['order_id'], $process_info['pnref']);
		if (mysqli_num_rows($get_card_info) > 0) {
			$info = mysqli_fetch_assoc($get_card_info);
			$cn = $info['card_desc'];
			$card_type = $info['card_type'];
		}
	}

	$check_for_duplicate = false;
	$create_transaction_record = false;
	if ($transtype == ""){
		$transtype = "Sale";
	}

	switch ($transtype) {
		case "Sale" :
		case "IssueGiftCard" :
		case "GiftCardSale" :
		case "ReloadGiftCard" :
		case "IssueLoyaltyCard" :
		case "LoyaltyCardSale" :
		case "LoyaltyCardReward" :
		case "LoyaltyCardAddValue" :
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$payment->setTotal($send_amount);
			break;
		case "Auth":
		case "AuthForTab":
			$check_for_duplicate = true;
			$create_transaction_record = true;
			if ($process_info['order_id'] == "0"){
				$process_info['order_id'] = create_new_order_record($process_info, $location_info);
			}
			if ($transtype=="AuthForTab" && (float)$send_amount==0) {
				$process_info['card_amount'] = $location_info['default_preauth_amount'];
				$send_amount = $process_info['card_amount'];
			}
			$payment->setTotal($send_amount);
			break;
		case "PreAuthCapture":
		case "PreAuthCaptureForTab":
			$AuthQuery =
				"SELECT `auth`, `card_desc`, `amount`, `transaction_id`".
				"FROM `cc_transactions` ".
				"WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND".
				"`transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'";

			$get_transaction_info = lavu_query($AuthQuery, $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if (!empty($process_info['card_amount'])){
				$send_amount = number_format(
					($process_info['card_amount'] + $process_info['tip_amount']),
					$location_info['disable_decimal'], ".", ""
				);
			}else{
				$send_amount = number_format(
					($transaction_info['amount']  + $process_info['tip_amount']),
					$location_info['disable_decimal'], ".", ""
				);
			}
			if ($cn == ""){
				$cn = $transaction_info['card_desc'];
			}
			$tempTipAmt = number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$log_tip_amount = " - TIP AMOUNT: ".$tempTipAmt;
			$payment->setTotal($send_amount);
			$payment->setRefTxnId($transaction_info['transaction_id']);
			break;
		case "Adjustment" :
			$adjQuery = "SELECT `amount`, `transaction_id`, `card_desc` ".
				"FROM `cc_transactions` ".
				"WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' ".
				"AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'";

			$get_transaction_info = lavu_query($adjQuery, $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$send_amount = number_format(
				($transaction_info['amount'] + $process_info['tip_amount']),
				$location_info['disable_decimal'],
				".",
				""
			);

			if ($cn == ""){
				$cn = $transaction_info['card_desc'];
			}

			$tempTipAmt = number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$log_tip_amount = " - TIP AMOUNT: ".$tempTipAmt;

			$payment->setTotal($send_amount);
			$payment->setTip($tempTipAmt);
			$payment->setRefTxnId($transaction_info['transaction_id']);
			break;
		case "Void":
		case "GiftCardVoid":
		case "VoidPreAuthCapture":
			$voidQuery = "SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`,".
				" `first_four`,`card_desc`, `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' ".
				"AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' ".
				"AND (`action` = 'Sale' OR `action` = 'Refund')";

			$get_transaction_info = lavu_query($voidQuery , $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			$cn = $transaction_info['card_desc'];
			$create_transaction_record = true;
			$payment->setTotal($send_amount);
			$payment->setRefTxnId($transaction_info['transaction_id']);
			break;
		case "VoidLoyaltyCardIssueOrReload" :
		case "VoidGiftCardIssueOrReload" :
			$voidQuery = "SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`,".
				" `first_four`,`card_desc`, `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' ".
				"AND `transaction_id` = '[pnref]' AND `voided` != '1' ".
				"AND (`action` = 'IssueGiftCard' OR `action` = 'Reload')";
			$get_transaction_info = lavu_query($voidQuery , $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			$cn = $transaction_info['card_desc'];
			$create_transaction_record = true;
			$payment->setTotal($send_amount);
			if(empty($transaction_info['transaction_id'])){
				$payment->setRefTxnId($process_info['pnref']);
			}else{
				$payment->setRefTxnId($transaction_info['transaction_id']);
			}

			break;

		case "Return" :
			$returnQuery = "SELECT `preauth_id`, `transtype` FROM `cc_transactions` WHERE `loc_id` = '[1]' ".
				"AND `order_id` = '[2]' AND `check` = '[3]' AND `transaction_id` = '[4]' ".
				"AND `voided` != '1' AND `action` = 'Sale'";

			$get_transaction_info = lavu_query(
				$returnQuery,
				$process_info['loc_id'],
				$process_info['order_id'],
				$process_info['check'],
				$process_info['pnref']
			);

			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			if ($transaction_info['transtype'] == "Auth"){
				$process_info['pnref'] = $transaction_info['preauth_id'];
			}
			$create_transaction_record = true;
			$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
			$payment->setTotal($send_amount);
			$payment->setRefTxnId($process_info['pnref']);
			break;
		case "LoyaltyCardBalance" :
		case "GiftCardBalance" :
		case "CaptureAll" :
			break;
		default :
			error_log(__FUNCTION__." 31101 Unknown Transtype : $transtype");
			return "0|Unable to determine transaction type|||".$process_info['order_id'];
			break;
	}

	$transaction_vars = generate_transaction_vars(
		$process_info,
		$location_info,
		$cn,
		$card_type,
		$transtype,
		$swipe_grade,
		$pay_type,
		$pay_type_id,
		$process_info['card_amount']
	);


	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != ""){
			$q_fields .= ", ";
			$q_values .= ", ";
		}
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	$system_id = 0;
	if (in_array($transtype, array("", "Sale", "Auth", "AuthForTab", "Void", "Return","IssueGiftCard","IssueLoyaltyCard","GiftCardSale"))) {
		$ccQuery = "INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)";
		$preliminary_record_transaction = lavu_query($ccQuery, $transaction_vars);
		if (!$preliminary_record_transaction){
			return "0|Failed to create transaction record.||".$process_info['order_id'];
		}
		$system_id = lavu_insert_id();
	}

	$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";
	$debug .= "\n\nURL: ".$heartland_server_url."\n\n";

	$payment->performTransaction($transtype);
	$result = $payment->getCode();
	$gateway_code = $payment->getResponseCode();
	$respmsg = $payment->getResponse();
	$authcode = $payment->getAuthCode();
	$new_pnref = $payment->getReference();
	$message = $payment->getMessage();

	if (empty($respmsg) && !empty($message)){
		$respmsg = $message;
	}

	$approved = "0";
	if ($gateway_code=="0" && (empty($result) || $result=="00" || $result=="10" || $result=="13")){
		$approved = "1";
	}

	$debug .= "\n\nHEARTLAND\n\n".$payment->sxe->asXML();
	$debug .= "\n\nRESULT: $result\n";
	$debug .= "RESPONSE MESSAGE: $respmsg\n$authcode\n$new_pnref\n$card_type\n\n".$payment->raw_return;

	if ($location_info['gateway_debug'] == "1"){
		gateway_debug($process_info['data_name'], $process_info['loc_id'], "HEARTLAND", $transtype, $approved, $debug);
	}

	$log_string = "HEARTLAND[--CR--]LOCATION: ".$process_info['loc_id'].
		" - REGISTER: ".$process_info['register'].
		" - SERVER ID: ".$process_info['server_id'].
		" - ORDER ID: ".$process_info['order_id'].
		" - CHECK : ".$process_info['check'].
		" - AMOUNT: $send_amount".$log_tip_amount.
		" - TRANSTYPE: $transtype".
		" - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $authcode - CARD TYPE: $card_type".
		" - RESP: $respmsg - FIRST FOUR: $first_four ".
		"- CARD DESC: ".substr($cn, (strlen($cn) - 4), 4).
		" - SWIPED: ".$swiped."[--CR--]";

	$log_string .= "RESPONSE MESSAGE:".$respmsg."[--CR--]";

	if (($message != "") && (strtolower(trim($message)) != strtolower(trim($respmsg)))){
		$log_string .= "MESSAGE: ".$message."[--CR--]";
	}

	write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

	$process_info['last_mod_ts'] = time();
	$rtn = array();

	if ($approved=="1" && $result!="10" && $result!="13") {

		if(!empty($payment->return_data->SplitTenderCardAmt) && ($payment->return_data->SplitTenderCardAmt != 0)){
			$process_info['card_amount'] = $payment->return_data->SplitTenderCardAmt;
		}
		update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");
		if ($transtype == "CaptureAll") {
			$batch_info = $payment->getBatchInfo();
			$new_pnref = $batch_info->BatchSeqNbr;

			$process_info['card_amount'] = number_format(
				(float)$batch_info->TotalAmt,
				$location_info['disable_decimal'],
				".",
				","
			);

			$authcode = "<table cellspacing='0' cellpadding='3'>".
				"<tr><td align='center' colspan='2'>BATCH CLOSED</td></tr>".
				"<tr>".
				"<td align='right'>Batch Number:</td>".
				"<td align='left'>".$batch_info->BatchSeqNbr."</td>".
				"</tr>".
				"<tr>".
				"<td align='right'>Batch Item Count:</td>".
				"<td align='left'>".$batch_info->TxnCnt."</td>".
				"</tr>".
				"<tr>".
				"<td align='right'>Net Batch Total:</td>".
				"<td align='left'>$ ".$process_info['card_amount']."</td>".
				"</tr>".
				"</table>";
		}

		$rtn[] = "1"; // 0
		$rtn[] = $new_pnref;
		$rtn[] = substr($cn, -4);
		$rtn[] = $respmsg;
		$rtn[] = $authcode;
		$rtn[] = $card_type;
		$rtn[] = "";
		$rtn[] = $process_info['card_amount'];
		$rtn[] = $process_info['order_id'];
		$rtn[] = "";
		$rtn[] = ""; // 10
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = $process_info['last_mod_ts'];

	} else if ($approved=="1" && $result=="10") {

		if(!empty($payment->return_data->SplitTenderCardAmt) && ($payment->return_data->SplitTenderCardAmt != 0)){
			$process_info['card_amount'] = $payment->return_data->SplitTenderCardAmt;
		}else{
			$process_info['card_amount'] = $payment->return_data->AuthAmt;
		}
		update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

		$rtn[] = "1"; // 0
		$rtn[] = $new_pnref;
		$rtn[] = substr($cn, -4);
		$rtn[] = $respmsg;
		$rtn[] = $authcode;
		$rtn[] = $card_type;
		$rtn[] = "";
		$rtn[] = $process_info['card_amount'];
		$rtn[] = $process_info['order_id'];
		$rtn[] = "";
		$rtn[] = ""; // 10
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = $process_info['last_mod_ts'];

	} else if ($approved=="1" && $result=="13") {

		$process_info['card_amount'] = $payment->return_data->SplitTenderCardAmt;
		update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

		$rtn[] = "1"; // 0
		$rtn[] = $new_pnref;
		$rtn[] = substr($cn, -4);
		$rtn[] = $respmsg;
		$rtn[] = $authcode;
		$rtn[] = $card_type;
		$rtn[] = "";
		$rtn[] = $process_info['card_amount'];
		$rtn[] = $process_info['order_id'];
		$rtn[] = "";
		$rtn[] = ""; // 10
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = $process_info['last_mod_ts'];

	} else {

		$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
		if ($message != ""){
			if(strtolower(trim($message)) != strtolower(trim($respmsg)) ){
				$respmsg .= " - ".$message;
			}
		}

		$resptitle = "Declined";
		if (strpos($respmsg, "InvalidSecureTrack")){
			$respmsg = "Card read failed.\nPlease swipe card again.";
		}else if (strpos($payment->raw_return, "Server was unable to read request")){
			$respmsg = "Server was unable to read request.";
			if (empty($process_info['username'])){
				$respmsg .= " Integration Data 1 is empty.";
			}else if (empty($process_info['password'])){
				$respmsg .= " Integration Data 2 is empty.";
			}else if (empty($process_info['integration3'])){
				$respmsg .= " Integration Data 3 is empty.";
			}else if (empty($process_info['integration4'])){
				$respmsg .= " Integration Data 4 is empty.";
			}else if (empty($process_info['integration5'])){
				$respmsg .= " Integration Data 5 is empty.";
			}
		} else if (empty($respmsg)) {
			$resptitle = "Error";
			$respmsg = "No response received from gateway. Please call Heartland support at 888-963-3600.";
		}

		$rtn[] = "0";
		$rtn[] = $respmsg;
		$rtn[] = $new_pnref;
		$rtn[] = $process_info['order_id'];
		$rtn[] = $respmsg;
		$rtn[] = $resptitle;
	}

	return implode("|", $rtn);
}
?>