<?php
require_once(dirname(__FILE__)."/../gateway_settings.php");

if ( !function_exists( 'hex2bin' ) ) {
	function hex2bin( $str ) {
		$sbin = "";
		$len = strlen( $str );
		for ( $i = 0; $i < $len; $i += 2 ) {
			$sbin .= pack( "H*", substr( $str, $i, 2 ) );
		}
		return $sbin;
	}
}

class HeartlandMSR{
	private $MSR;
	private $CardInfo;
	public $bestTrack = false;
	public $ktb;
	public $unencrypted = false;
	public $MaskedTrack1 = false;
	public $MaskedTrack2 = false;

	public function SetMSR($msr){
		$this->MSR = $msr;
		$this->CardInfo = $this->Parse($msr);
		if(!empty($this->CardInfo['Track 1 Indicator']) ){
			$this->bestTrack = 1;
		}
		if(!empty($this->CardInfo['Track 2 Indicator']) ){
			$this->bestTrack = 2;
		}
		if(!empty($this->CardInfo['Key Block'])){
			$this->ktb = $this->CardInfo['Key Block'];
		}
	}

	public function GetCardTrack($myTrackNumber){
		if($myTrackNumber == 1){
			if(!empty($this->CardInfo['Track 1 Indicator'])){
				if('1' == $this->CardInfo['Track 1 Indicator']){
					return $this->CardInfo['Encrypted Track 1'];
				}else{
					return $this->CardInfo['Masked Track 1'];
				}
			}
		}

		if($myTrackNumber == 2){
			if(!empty($this->CardInfo['Track 2 Indicator'])){
				if('1' == $this->CardInfo['Track 2 Indicator']){
					return $this->CardInfo['Encrypted Track 2'];
				}else{
					return $this->CardInfo['Masked Track 2'];
				}
			}
		}
	}

	public function GetCardNumber(){
		if(!empty($this->CardInfo['Card Number'])){
			return $this->CardInfo['Card Number'];
		}
		return NULL;
	}

	private function Parse($magHex){
		$msr = hex2bin($magHex);
		$parsed = array();
		$parsed['Track 1 Indicator'] = hexdec(bin2hex(substr($msr,0,1)));
		$parsed['Track 2 Indicator'] = hexdec(bin2hex(substr($msr,1,1)));
		$parsed['Track 3 Indicator'] = hexdec(bin2hex(substr($msr,2,1)));
		$parsed['Track 1 Encrypted Length'] = hexdec(bin2hex(substr($msr,3,1)));
		$parsed['Track 2 Encrypted Length'] = hexdec(bin2hex(substr($msr,4,1)));
		$parsed['Track 3 Encrypted Length'] = hexdec(bin2hex(substr($msr,5,1)));
		$parsed['Card Encode Type'] = hexdec(bin2hex(substr($msr,6,1)));
		$parsed['Track 1 Luhn Flag'] = hexdec(bin2hex(substr($msr,7,1)));
		$parsed['Track 2 Luhn Flag'] = hexdec(bin2hex(substr($msr,8,1)));
		$parsed['Track 3 Luhn Flag'] = hexdec(bin2hex(substr($msr,9,1)));
		$parsed['Layout Indicator'] = substr($msr,10,2);
		if(0 != $parsed['Track 1 Encrypted Length']){
			$parsed['Encrypted Track 1'] = substr($msr,12,$parsed['Track 1 Encrypted Length']);
		}
		if(0 != $parsed['Track 2 Encrypted Length']){
			$parsed['Encrypted Track 2'] = substr($msr,172,$parsed['Track 2 Encrypted Length']);
		}
		if(0 != $parsed['Track 3 Encrypted Length']){
			$parsed['Encrypted Track 3'] = substr($msr,332,$parsed['Track 3 Encrypted Length']);
		}
		$parsed['Masked Track 1 Length'] = hexdec(bin2hex(substr($msr,492,1)));
		$parsed['Masked Track 2 Length'] = hexdec(bin2hex(substr($msr,493,1)));
		$parsed['Masked Track 3 Length'] = hexdec(bin2hex(substr($msr,494,1)));
		if(0 != $parsed['Masked Track 1 Length']){
			$parsed['Masked Track 1'] = substr($msr,495,$parsed['Masked Track 1 Length']);
			preg_match('/^%([A-Z])([0-9]{1,21})\^([^\^]{2,26})\^([0-9]{4}|\^)([0-9]{3}|\^)([^\?]+)\?$/',$parsed['Masked Track 1'],$matches);
			if (!empty($matches[2]) ) {
				$parsed['Card Number'] = $matches[2];
			}

		}
		if(0 != $parsed['Masked Track 2 Length']){
			$parsed['Masked Track 2'] = substr($msr,655,$parsed['Masked Track 2 Length']);
			preg_match('/;([0-9\*]{1,21})=([\d\*]{4}|=)([\d\*]{3}|=)([\d\*]*)?\?/',$parsed['Masked Track 2'],$matches);
			if (!empty($matches[1]) ) {
				$parsed['Card Number'] = $matches[1];
			}
			else
            {
                preg_match('/;([0-9\*]{1,21})\?/',$parsed['Masked Track 2'],$matches);
                if (!empty($matches[1]))
                {
                    $parsed['Card Number'] = $matches[1];
                }
            }
		}
		if(0 != $parsed['Masked Track 3 Length']){
			$parsed['Masked Track 3'] = substr($msr,815,$parsed['Masked Track 3 Length']);
		}
		$parsed['Key Block Length'] = hexdec(bin2hex(substr($msr,975,2)));
		if(0 != $parsed['Key Block Length'] ){
			$parsed['Key Block'] = substr($msr,977,$parsed['Key Block Length']);
		}
		$parsed['Track 1 PAN Length'] = hexdec(bin2hex(substr($msr,1327,1)));
		$parsed['Track 2 PAN Length'] = hexdec(bin2hex(substr($msr,1328,1)));
		$parsed['Track 3 PAN Length'] = hexdec(bin2hex(substr($msr,1329,1)));
		if(0 != $parsed['Track 1 PAN Length']){
			$parsed['Track 1 PAN'] = substr($msr,1330,$parsed['Track 1 PAN Length']);
		}
		if(0 != $parsed['Track 2 PAN Length']){
			$parsed['Track 2 PAN'] = substr($msr,1458,$parsed['Track 2 PAN Length']);
		}
		if(0 != $parsed['Track 3 PAN Length']){
			$parsed['Track 3 PAN'] = substr($msr,1586,$parsed['Track 3 PAN Length']);
		}
		if('2' == $parsed['Track 1 Indicator']){$parsed['Unencrypted'] = 1;$this->unencrypted = 1;}
		if('2' == $parsed['Track 2 Indicator']){$parsed['Unencrypted'] = 1;$this->unencrypted = 1;}
		if('2' == $parsed['Track 3 Indicator']){$parsed['Unencrypted'] = 1;$this->unencrypted = 1;}
		if( ('0' == $parsed['Track 1 Indicator']) && ($parsed['Track 1 Encrypted Length'] > 0) ){
			$parsed['Unencrypted'] = 1;
			$this->unencrypted = 1;
			$parsed['Track 1 Indicator'] = 2;
		}
		if( ('0' == $parsed['Track 2 Indicator']) && ($parsed['Track 2 Encrypted Length'] > 0) ){
			$parsed['Unencrypted'] = 1;
			$this->unencrypted = 1;
			$parsed['Track 2 Indicator'] = 2;
		}
		if( ('0' == $parsed['Track 3 Indicator']) && ($parsed['Track 3 Encrypted Length'] > 0) ){
			$parsed['Unencrypted'] = 1;
			$this->unencrypted = 1;
			$parsed['Track 3 Indicator'] = 2;
		}

		return $parsed;
	}
}

class HeartlandPayments {
	public $send_data = '';
	public $return_data;
	public $raw_return = '';
	public $sxe;
	public $CurlInfo;
	private $api_url = "";
	private $trans_type;
	private $swiped = null;
	private $cc_t1;
	private $cc_t2;
	private $cc_cvn;
	private $keyed = null;
	private $reader_present;
	private $cc_num;
	private $cc_year;
	private $cc_mon;
	private $RefTxnId = null;
	private $ccPresent;
	private $encrypted_swipe = null;
	private $enc_type;
	private $enc_track;
	private $enc_data;
	private $enc_ksn;
	private $TxnId = null;
	private $GatewayRsp;
	private $orderTotal = false;
	private $Tip;
	private $debug = false;
	private $userName;
	private $pword;
	private $licenseID;
	private $siteID;
	private $deviceID;
	private $certificationMode = false;
	private $loyalty = false;
	private $lodging_mode = false;
	private $alias = false;
	private $reciept_notes = false;
	private $request_token = false;
	private $token = false;
	private $corporate_card_info = false;
	private $allow_duplicate_transaction = false;
	private $auth_amt = false;
	private $DebugTransType;

	//
	//Lodging Data
	//
	private $lodging_data = false;
	//Array()
		//PrestigiousPropertyLimit : NOT_PARTICIPATING, LIMIT_500, LIMIT_1000, LIMIT_1500
		//NoShow : Y/N
		//AdvancedDepositType : ASSURED_RESERVATION, CARD_DEPOSIT, PURCHASE, OTHER
		//PreferredCustomer : Y/N
	private $lodging_data_edit = false;
	//Array()
		//FolioNumber : INT
		//Duration : INT 
		//CheckInDate : DateTime
		//CheckOutDate : DateTime
		//Rate : Decimal
		//ExtraChargeAmtInfo : Price of extras
	private $lodging_extra_charges = false;
	//Array()
		//Restaurant : Y/N
		//GiftShop : Y/N
		//MiniBar : Y/N
		//Telephone : Y/N
		//Other : Y/N
		//Laundry : Y/N

	//
	// Constructor
	//

	public function __construct($un, $pw, $lID, $sID, $dID, $hsURL){
		$this->userName =$un;
		$this->pword = $pw;
		$this->licenseID = $lID;
		$this->siteID = $sID;
		$this->deviceID = $dID;
		$this->api_url = $hsURL;
	}

	//
	// Get-ers
	//
	public function allowDuplicate($input){
		$this->allow_duplicate_transaction = $input;
	}

	public function getAuthCode(){
		if('GiftCardBalance' == $this->trans_type){
			if(false !== $this->loyalty){
				return $this->return_data->PointsBalanceAmt;
			}else{
				return $this->return_data->BalanceAmt;
			}
		}
		return $this->return_data->AuthCode;
	}

	public function getBatchInfo(){
		return $this->return_data;
	}

	public function getCode(){
		return $this->return_data->RspCode;
	}

	public function getMessage(){
		return $this->GatewayRsp;
	}

	public function getRecieptNotes(){
		return $this->reciept_notes;
	}

	public function getReference(){
		return $this->TxnId;
	}

	public function getResponse(){
		return $this->return_data->RspText;
	}

	public function getResponseCode(){
		return $this->GatewayRspCode;
	}

	public function getToken(){
		return $this->token;
	}

	public function returnRawReturn(){
		return $this->raw_return;
	}

	public function returnRawSend(){
		return $this->send_data;
	}

	//
	// Set-ers
	//
	public function setCertificationMode(){
		$this->certificationMode = true;
	}

	public function setAlias($myAlias){
		$this->alias = $myAlias;
	}

	public function setExtraCharges($input){
		$this->lodging_extra_charges = $input;
	}

	public function setLodgingData($input){
		$this->lodging_data = $input;
	}

	public function setLodgingDataEdit($input){
		$this->lodging_data_edit = $input;
	}

	public function setLodgingMode($input){
		$this->lodging_mode = $input;
	}

	public function setTotal($total){
		$this->orderTotal = $total;
	}

	public function setAuthAmt($total){
		$this->auth_amt = $total;
	}

	public function setTip($tip){
		$this->Tip = $tip;
	}

	public function setCPC($input){
		$this->corporate_card_info = $input;
	}

	public function setRefTxnId($id){
		$this->RefTxnId = $id;
	}

	public function setFolioNumber($input){
		$this->folio_number = $input;
	}

	public function setCheckInDate($input){
		$this->check_in_date = $input;
	}

	public function setCheckOutDate($input){
		$this->check_out_date = $input;
	}

	public function setDuration($input){
		$this->duration = $input;
	}

	public function setRate($input){
		$this->rate = $input;
	}

	public function setEncryptedSwipe($enc1,$enc2,$enc3,$cryptoType){
		$this->encrypted_swipe = true;
		$this->enc_track = $enc1;
		$this->enc_data = $enc2;
		$this->enc_ksn = $enc3;
		if (isset($cryptoType)){
			$this->enc_type = $cryptoType;
		}else{
			$this->enc_type = "03";
		}
	}

	public function setSwiped($t1,$t2){
		$this->swiped = true;
		$this->cc_t1 = $t1;
		$this->cc_t2 = $t2;
	}

	public function setKeyedIn($num,$mon,$year,$cvn=false,$reader_present, $CardPresent="N"){
		$this->keyed = true;
		$this->ccPresent = $CardPresent;
		$this->reader_present = $reader_present;
		$this->cc_num = $num;
		$this->cc_mon = $mon;
		$this->cc_year = "20".$year;
		$this->cc_cvn = $cvn;
	}

	//
	// Public
	// 
	public function performTransaction($transtype){
		$this->DebugTransType = $transtype;
		switch ($transtype) {
			//CreditTxnEdit
			case "Adjustment":
				$this->prepOrder("CreditTxnEdit",false,false);
			break;
			//CreditAuth
			case "Auth":
			case "AuthForTab":
				$this->prepOrder("CreditAuth");
			break;
			case "CreditIncrementalAuth":
				$this->prepOrder("CreditIncrementalAuth");
			break;
			//BatchClose
			case "CaptureAll":
				$this->prepOrderEmpty("BatchClose");
			break;
			//CreditAddToBatch
			case "PreAuthCapture":
			case "PreAuthCaptureForTab":
				$this->prepOrder("CreditAddToBatch",false,false);
			break;
			//CreditReturn
			case "Return":
				$this->prepOrder("CreditReturn");
			break;
			//CreditSale
			case "Sale":
				$this->prepOrder("CreditSale");
			break;
			//CreditReversal
			case "Void":
			case "VoidPreAuthCapture":
			if(null !== $this->RefTxnId){
				$this->prepOrderVoid("CreditVoid",false,false);
			}else{
				$this->prepOrder("CreditReversal",false,false);
			}
			break;
			case "Reverse":
				$this->prepOrder("CreditReversal",false,true);
			break;
			//
			// Gift Cards
			//
			case 'IssueGiftCard':
				$this->prepOrder("GiftCardActivate");
			break;
			case 'ReloadGiftCard':
				$this->prepOrder("GiftCardAddValue");
			break;
			case 'DeactivateGiftCard':
				$this->prepOrder("GiftCardDeactivate");
			break;
			case 'GiftCardBalance':
				$this->prepOrder("GiftCardBalance");
			break;
			case 'GiftCardSale':
				$this->prepOrder("GiftCardSale");
			break;
			case 'GiftCardVoid':
				$this->prepOrderVoid("GiftCardVoid",false,true);
			break;
			case 'VoidGiftCardIssueOrReload':
				$this->prepOrderVoid("GiftCardVoid",false,true);
			break;
			case 'GiftCardReturn':
				$this->prepOrder("GiftCardReversal");
			break;
			case 'GiftCardReplace':
				$this->prepOrder("GiftCardReplace");
			break;

			//
			// Loyalty
			//
			case 'IssueLoyaltyCard':
				$this->prepOrder("GiftCardActivate",true);
			break;
			case 'ReloadLoyaltyCard':
				$this->prepOrder("GiftCardAddValue",true);
			break;
			case 'RewardLoyaltyCard':
				// Send amount from order to auto-reward customer.
				$this->prepOrder("GiftCardReward");
			break;
			case 'DeactivateLoyaltyCard':
				$this->prepOrder("GiftCardDeactivate");
			break;
			case 'LoyaltyCardBalance':
				$this->loyalty = true;
				$this->prepOrder("GiftCardBalance");
			break;
			case 'LoyaltyCardSale':
				$this->prepOrder("GiftCardSale",true);
			break;
			case 'LoyaltyCardVoid':
				$this->prepOrderVoid("GiftCardVoid",false,true);
			break;
			case 'VoidLoyaltyCardIssueOrReload':
				$this->prepOrderVoid("GiftCardVoid",false,true);
			break;
			case 'LoyaltyCardReturn':
				$this->prepOrder("GiftCardReversal",true);
			break;

			//
			//  GiftCardAlias
			//
			case 'GiftCardAliasAdd':
				$this->prepAlias('ADD');
			break;
			case 'GiftCardAliasDelete':
				$this->prepAlias('DELETE');
			break;
			case 'GiftCardAliasCreate':
				$this->prepAlias('CREATE');
			break;

			//
			// Lodging
			//
			case 'CreditAccountVerify':
				$this->request_token = true;
				$this->prepOrder("CreditAccountVerify");
			break;
			//
			// Corporate Cards
			//
			case 'CreditCPCEdit':
				$this->prepCPCEdit();
			break;

			default:
				error_log(__FILE__." ".__FUNCTION__."31101 Unknown Trans Type: $transtype");
			break;
		}
	}

	//
	// Private
	//

	private function prepOrder($type,$points = false,$block1 = true){
		$this->trans_type = $type;
		$this->createHeader($type);
		if(!empty($this->RefTxnId)){
			$Transaction = $this->handleByTxnID($type,$points,$block1);
		}elseif(false != $this->token){
			$Transaction = $this->handleToken($type,$points,$block1);
		}elseif(false != $this->encrypted_swipe){
			$Transaction = $this->handleEncrypted($type,$points,$block1);
		}elseif(false != $this->swiped){
			$Transaction = $this->handleSwiped($type,$points,$block1);
		}elseif(false != $this->keyed){
			$Transaction = $this->handleKeyed($type,$points,$block1);
		}
		if ($type == "CreditSale" or $type == "CreditAuth") {
			if(!empty($this->corporate_card_info)){
				$Transaction->CPCReq = 'Y';
			}
			$Transaction->AllowPartialAuth = 'Y';
			if ($this->allow_duplicate_transaction) {
				$Transaction->AllowDup = 'Y';
			}
			if ($this->Tip) {
				$Transaction->GratuityAmtInfo = $this->Tip;
			}
		}
		if($this->request_token){
			$Transaction->CardData->TokenRequest = 'Y';
		}
		if($this->lodging_mode){
			$this->lodgingFunctions($type,$points,$block1);
		}
		//echo "\n".$this->sxe->asXML()."\n";
		//return;
		$this->curlGateway();
	}

	//
	// For Batching
	//
	private function prepOrderEmpty($type){
		$this->trans_type = $type;
		$this->createHeader($type);
		$this->curlGateway();
	}

	private function prepOrderVoid($type,$points = false,$block1 = false){
		$this->trans_type = $type;
		$this->createHeader($type);
		if($block1){
			$this->sxe->Transaction->$type->Block1->GatewayTxnId = $this->RefTxnId;
		}else{
			$this->sxe->Transaction->$type->GatewayTxnId = $this->RefTxnId;
		}

		$this->curlGateway();
	}

	private function prepAlias($aliasType){
		$type = 'GiftCardAlias';
		$this->trans_type = 'GiftCardAlias';
		$this->createHeader('GiftCardAlias');
		if ($this->encrypted_swipe) {
			$Transaction = $this->handleEncrypted($type,false,true);
		} else if ($this->swiped) {
			$Transaction = $this->handleSwiped($type,false,true);
		} else if($this->keyed) {
			$Transaction = $this->handleKeyed($type,false,true);
		}
		$this->sxe->Transaction->GiftCardAlias->Block1->Action = $aliasType;
		if(!$this->alias){
			if($aliasType != 'DELETE'){
				return false;
			}
		}else{
			$this->sxe->Transaction->GiftCardAlias->Block1->Alias = $this->alias;
		}

		return $this->curlGateway();
	}

	private function prepCPCEdit(){
		$this->trans_type = 'CreditCPCEdit';
		$this->createHeader('CreditCPCEdit');
		$this->sxe->Transaction->CreditCPCEdit->GatewayTxnId = $this->RefTxnId;
		if(!empty($this->corporate_card_info)){
			foreach ($this->corporate_card_info as $key => $value) {
				$this->sxe->Transaction->CreditCPCEdit->CPCData->$key = $value;
			}
		}
		return $this->curlGateway();
	}

	private function createHeader($type){
		$this->sxe = new SimpleXMLElement('<Ver1.0/>');
		$Header = $this->sxe->addChild("Header");
		$Header->SiteId = $this->siteID; // integration data 4 - 20655
		$Header->DeviceId = $this->deviceID; // integration data 5 - 1413539
		$Header->LicenseId = $this->licenseID; // integration data 3 - 20658
		$Header->UserName = $this->userName; // integration data 1 - 777700000892
		$Header->Password = $this->pword; // integration data 2 - $Test1234
		$Header->DeveloperID = '002814';
		$Header->VersionNbr = '1148';
		$this->sxe->addChild("Transaction")->addChild($type);
		return;
	}
	private function handleByTxnID($type,$points = false,$block1 = true){
		if($block1){
			$Transaction = $this->sxe->Transaction->$type->addChild('Block1');
		}else{
			$Transaction = $this->sxe->Transaction->$type;
		}
		$Transaction->GatewayTxnId = $this->RefTxnId;
		if($this->orderTotal){
			$Transaction->Amt = $this->orderTotal;
		}
		if($this->auth_amt !== false){
			$Transaction->AuthAmt = $this->auth_amt;
		}
		if ($this->Tip) {
			$Transaction->GratuityAmtInfo = $this->Tip;
		}
	}

	private function handleToken($type,$points = false,$block1 = true){
		//To Be Implimented
	}

	private function handleKeyed($type,$points = false,$block1 = true){
		if($block1){
			$Transaction = $this->sxe->Transaction->$type->addChild('Block1');
		}else{
			$Transaction = $this->sxe->Transaction->$type;
		}
		if(strstr($type, 'Gift') || strstr($type, 'Loyal')){
			$Transaction->CardData->CardNbr = $this->cc_num;
		}else{
			$Transaction->CardData->ManualEntry->ReaderPresent = $this->reader_present;
			$Transaction->CardData->ManualEntry->CardPresent = $this->ccPresent;
			$Transaction->CardData->ManualEntry->CardNbr = $this->cc_num;
			$Transaction->CardData->ManualEntry->ExpMonth = $this->cc_mon;
			$Transaction->CardData->ManualEntry->ExpYear = $this->cc_year;
            if ($this->cc_cvn){
                $Transaction->CardData->ManualEntry->CVV2 = $this->cc_cvn;
            }
		}

		if($this->orderTotal){
			$Transaction->Amt = $this->orderTotal;
		}
		if($this->auth_amt !== false){
			$Transaction->AuthAmt = $this->auth_amt;
		}
		if($points){
			$Transaction->Currency = 'POINTS';
		}
		return $Transaction;
	}

	private function handleSwiped($type,$points = false,$block1 = true){
		if($block1){
			$Transaction = $this->sxe->Transaction->$type->addChild('Block1');
		}else{
			$Transaction = $this->sxe->Transaction->$type;
		}

		$Transaction->CardData->TrackData = $this->cc_t1.";".$this->cc_t2;
		if($this->orderTotal){
			$Transaction->Amt = $this->orderTotal;
		}
		if($this->auth_amt !== false){
			$Transaction->AuthAmt = $this->auth_amt;
		}
		if($points){
			$Transaction->Currency = 'POINTS';
		}
		return $Transaction;
	}

	private function handleEncrypted($type,$points = false,$block1 = true){
		if($block1){
			$Transaction = $this->sxe->Transaction->$type->addChild('Block1');
		}else{
			$Transaction = $this->sxe->Transaction->$type;
		}

		$Transaction->CardData->TrackData = $this->enc_data;
		$Transaction->CardData->EncryptionData->Version = $this->enc_type;
		if ($this->enc_type == "02"){
			$Transaction->CardData->EncryptionData->KTB = $this->enc_ksn;
		}else if (!empty($this->enc_ksn)) {
			$Transaction->CardData->EncryptionData->KSN = $this->enc_ksn;
		}
		if (!empty($this->enc_track)) $Transaction->CardData->EncryptionData->EncryptedTrackNumber = $this->enc_track;
		if($this->orderTotal){
			$Transaction->Amt = $this->orderTotal;
		}
		if($this->auth_amt !== false){
			$Transaction->AuthAmt = $this->auth_amt;
		}
		if($points){
			$Transaction->Currency = 'POINTS';
		}
		return $Transaction;
	}

	private function lodgingFunctions($type,$points = false,$lodging = false){
		$stuff = $this->sxe->Transaction->$type;
		if(!isset($stuff)){
			error_log(__FILE__." ".__FUNCTION__.' $this->sxe->Transaction->$type Empty?');
		}
		if(!isset($stuff->Block1)){
			$xml_lodging_data_edit = $stuff->addChild('LodgingDataEdit');
		}else{
			$xml_lodging_data = $stuff->Block1->addChild('LodgingData');
			$xml_lodging_data_edit = $xml_lodging_data->addChild('LodgingDataEdit');
		}
		if(!empty($this->lodging_data)){
			foreach ($this->lodging_data as $key => $value) {
				$xml_lodging_data->$key = $value;
			}
		}
		if(!empty($this->lodging_data_edit)){
			foreach ($this->lodging_data_edit as $key => $value) {
				$xml_lodging_data_edit->$key = $value;
			}
		}
		if(!empty($this->lodging_extra_charges)){
			foreach ($this->lodging_extra_charges as $key => $value) {
				$xml_lodging_data_edit->ExtraCharges->$key = $value;
			}
		}
	}

	private function curlGateway(){
		if($this->debug){
			echo $this->sxe->asXML();
			echo "<p></p>";
		}

		$this->send_data =
			"<?xml version='1.0' encoding='utf-8'?>".
			"<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>".
				"<soap:Body>".
					"<PosRequest clientType=\"PosGatewayClient\" clientVer=\"1.0.0.0\" xmlns=\"http://Hps.Exchange.PosGateway\">".
						str_replace('<?xml version="1.0"?>','',$this->sxe->asXML()).
					"</PosRequest>".
				"</soap:Body>".
			"</soap:Envelope>";
		if(!$this->certificationMode){
			ConnectionHub::closeAll();
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->api_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->send_data);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: text/xml; charset=utf-8'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);

		$data = curl_exec($ch);
		$this->CurlInfo = curl_getinfo($ch);
		curl_close($ch);
		$this->raw_return = $data;
		$start = strpos($data,'<Ver1.0>');
		$end = strpos($data,'</Ver1.0>') + 9;
		if(!$this->certificationMode && !empty($process_info) && !empty($process_info['data_name'])){
			write_log_entry("gateway", $process_info['data_name'], "\n\n".$data."\n\n");
		}
		$data2 = substr($data,$start,$end-$start);
		try{
			$xml = @new SimpleXMLElement($data2);
		}catch(Exception $e){
			error_log($this->send_data);
			$this->GatewayRspCode = "-1";
			$this->GatewayRsp = "Data Parse Error";
			return;
		}
		$type = $this->trans_type;
		$this->return_data = $xml->Transaction->$type;
		if(!empty($this->return_data) && !empty($this->return_data->Notes)){
			$this->reciept_notes = $this->return_data->Notes;
		}
		if(!empty($xml->Header->TokenData) && ('Success' == $xml->Header->TokenData->TokenRspMsg) && !empty($xml->Header->TokenData->TokenValue)){
			$this->token = $xml->Header->TokenData->TokenValue;
		}

		$this->TxnId = $xml->Header->GatewayTxnId;
		$this->GatewayRsp = $xml->Header->GatewayRspMsg;
		$this->GatewayRspCode = $xml->Header->GatewayRspCode;
	}

}

?>