<?php

	function process_authorize_net2($process_info, $location_info, $transaction_vars) {
		
		global $company_info;
		
		$debug = print_r($process_info, true)."\n\n";

		$card_number = $process_info['card_number'];
		$mag_data = $process_info['mag_data'];


		$swiped = "0";
		$swipe_grade = "H";
		$track1 = "";
		$track2 = "";
		if ($mag_data) {
			$swiped = "1";
			$swipe_grade = "G";
			$tracks = explode("?", $mag_data);
			$track2_index = 1;
			if (strstr($tracks[0], "=")) $track2_index = 0;
			if (($track2_index == 1) && (count($tracks) >= 1)) {
				$t1 = $tracks[0];
				if (strlen($t1) > 10) {
					$track1 = $t1;
					$swipe_grade = "F";
				}
			}
			if (count($tracks) > $track2_index) {
				$t2 = $tracks[$track2_index];
				if (strlen($t2) > 10) {
					$track2 = $t2;
					if ($swipe_grade == "F") $swipe_grade = "D";
					else $swipe_grade = "E";
				}
			}
		}

		$transtype = $process_info['transtype'];
		if ($transtype == "") $transtype = "Sale";

		switch ($transtype) {
			case "Sale" : $use_transtype = "AUTH_CAPTURE";  break;
			case "Auth" : $use_transtype = "AUTH_ONLY";  break;
			case "PreAuthCapture" : $use_transtype = "PRIOR_AUTH_CAPTURE"; break;
			case "Return" :
				$use_transtype = "CREDIT";
				//$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `card_desc` = '[last_four]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				//$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				//$process_info['pnref'] = $pnref;
				break;
			case "Void" : 
				$use_transtype = "VOID";
				$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$transaction_vars['card_type'] = $transaction_info['card_type'];
				$transaction_vars['first_fours'] = $transaction_info['first_four'];
				break;
			case "VoidAuth" : $use_transtype = "VOID"; break;
			case "AddTip" : $use_transtype = "AUTH_CAPTURE"; break;
		}



		$q_fields = "";
		$q_values = "";
		$keys = array_keys($transaction_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}

		$this_gateway = "Authorize.net";
		$tx_desc = "POS Lavu - ".$company_info['company_name'];
		if (in_array($transtype, array("Void", "GiftCardVoid", "LoyaltyCardVoid", "VoidGiftCardIssueOrReload", "VoidLoyaltyCardIssueOrReload"))) $tx_desc = "VOID - ".$tx_desc;
		if (in_array($transtype, array("PreAuthCapture", "PreAuthCaptureForTab"))) $tx_desc = "Capture - ".$tx_desc;
		if ($transtype == "AddTip") $tx_desc = "Added Tip - ".$tx_desc; 

		if (in_array($location_info['market_type'], array("", "Web", "Moto", "Mobile")) || in_array($location_info['gateway'], array("BluePay", "Elavon"))) {
		
			$post_url = "https://secure.authorize.net/gateway/transact.dll";
			if ($location_info['gateway'] == "BluePay") {
				$this_gateway = "BluePay";
				$post_url = "https://secure.bluepay.com/interfaces/a.net";
			}
			if ($location_info['gateway'] == "Elavon") {
				$this_gateway = "Elavon";
				$post_url = "https://secure.internetsecure.com/process.cgi";
			}

			$rcode_index = 0;
			$transaction_id_index = 6;
			$card_type_index = 51;
			$split_tender_id_index = 52;
		
			$post_values = array(
				"x_login"			=> $process_info['username'], // 8wvM4Z5T // 40001
				"x_tran_key"		=> $process_info['password'], // 8x4cPL589rhWuW5X // 11111111115
			
				"x_version"			=> "3.1",
				"x_delim_data"		=> "TRUE",
				"x_delim_char"		=> "|",
				"x_relay_response"	=> "FALSE",
			
				"x_type"			=> $use_transtype,
				"x_method"			=> "CC",
				"x_card_num"		=> $process_info['card_number'], //"4111111111111111",
				"x_card_code"		=> $process_info['card_cvn'],
				"x_exp_date"		=> $process_info['exp_month'].$process_info['exp_year'], //"0115",
			
				"x_amount"			=> $process_info['card_amount'], //"19.99",
				"x_description"		=> $tx_desc, //"Sample Transaction",
				"x_invoice_num" => $process_info['loc_id'].$process_info['order_id'],
				"x_trans_id" => $process_info['pnref'],
			
				"x_first_name"		=> "", //"John",
				"x_last_name"		=> "", //"Doe",
				"x_address"			=> "", //"1234 Street",
				"x_city"			=> "",
				"x_state"			=> "", //"WA",
				"x_zip"				=> "", //"98004"
				
			);
			
			if (in_array($transtype, array("Void", "VoidAuth", "PreAuthCapture", "Return", "Settle", "Adjust&Settle"))) {
				$more_values = array("x_trans_id" => $process_info['pnref']);
				$post_values = array_merge($post_values, $more_values);
			}
							
		} else if ($location_info['market_type'] == "Retail") {

			$post_url = "https://cardpresent.authorize.net/gateway/transact.dll";

			$market_type_code = 2;
			$device_type = 7;
			
			$rcode_index = 1;
			$transaction_id_index = 7;
			$card_type_index = 21;
			$split_tender_id_index = 22;		
		
			$post_values = array(
			
				"x_login"			=> $process_info['username'], // 8wvM4Z5T
				"x_tran_key"		=> $process_info['password'], // 8x4cPL589rhWuW5X
			
				"x_market_type" => $market_type_code,
				"x_device_type" => $device_type,
			
				"x_cpversion"			=> "1.0",
				//"x_test_request"  => "TRUE",
			
				"x_response_format" => "1",
				"x_delim_char"      => "|",
	
				"x_invoice_num" => $process_info['loc_id'].$process_info['order_id'],
				"x_description"		=> $tx_desc, //"Sample Transaction",
			
				"x_amount"			=> $process_info['card_amount'], //"19.99",
				"x_method"			=> "CC",
				"x_type"			=> $use_transtype,

				"x_card_num"		=> $process_info['card_number'], //"4111111111111111",
				"x_exp_date"		=> $process_info['exp_month'].$process_info['exp_year'], //"0115",
				"x_card_code"		=> $process_info['card_cvn'],

				"x_track1" => $track1,
				"x_track2" => $track2
			);

			if (in_array($transtype, array("Void", "VoidAuth", "PreAuthCapture", "Return", "Settle", "Adjust&Settle"))) {
				$more_values = array("x_ref_trans_id" => $process_info['pnref']);
				$post_values = array_merge($post_values, $more_values);
			}
		}

		$debug .= "\n\n".$post_url."\n\n";
					
		$post_string = "";
		foreach( $post_values as $key => $value ) {
			$post_string .= "$key=" . urlencode( $value ) . "&";
		}
		$post_string = rtrim( $post_string, "& " );
		
		ConnectionHub::closeAll();
		
		$request = curl_init($post_url); // initiate curl object
		curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
		$post_response = curl_exec($request); // execute curl post and store results in $post_response
		curl_close($request); // close curl object
		
		$response_array = explode($post_values["x_delim_char"], $post_response);
		$response_code = (string)$response_array[$rcode_index];
		$response_reason = $response_array[2];
		$response_description = $response_array[3];
		$authcode = $response_array[4];
		$new_pnref = $response_array[$transaction_id_index];
		$card_type = $response_array[$card_type_index];
		$split_tender_id = $response_array[$split_tender_id_index];
		
		if ($mag_data && empty($track1) && empty($track2) && $response_code!="1") $response_description = "Card read failed. Please try again.||".$process_info['order_id']."||Error|||";

		$debug .= strtoupper($this_gateway)."\n\n".$post_string."\n\nRESULT: ".$response_code."\nRESPONSE DESCRIPTION: ".$response_description."\nRESPONSE REASON: ".$response_reason."\n".$authcode."\n".$new_pnref."\n".$split_tender_id."\n".$card_type."\n\n".$post_response;			

		$approved = "0";
		if ($response_code=="1" && !strstr($response_code, ".")) $approved = "1";
		if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], $this_gateway, $transtype, $approved, $debug);
		
		$log_string = strtoupper($this_gateway)."[--CR--]";
		$log_string .= "LOCATION: ".$process_info['loc_id']." - ";
		$log_string .= "REGISTER: ".$process_info['register']." - ";
		$log_string .= "SERVER ID: ".$process_info['server_id']." - ";
		$log_string .= "ORDER ID: ".$process_info['order_id']." - ";
		$log_string .= "AMOUNT: ".$process_info['card_amount']." - ";
		$log_string .= "TRANSTYPE: ".$transtype." - ";
		$log_string .= "RESULT: ".$response_code." - ";
		$log_string .= "TRANSACTION ID: ".$new_pnref." - ";
		$log_string .= "SPLIT TENDER ID: ".$split_tender_id." - ";
		$log_string .= "AUTHCODE: ".$authcode." - ";
		$log_string .= "CARD TYPE: ".$card_type." - ";
		$log_string .= "CARD DESC: ".substr($card_number, -4)." - ";
		$log_string .= "SWIPED: ".$swiped."[--CR--]";
		$log_string .= "EXT_DATA: ".$ext_data."[--CR--]";
		$log_string .= "RESPONSE DESCRIPTION: ".$response_description."[--CR--]";
		$log_string .= "RESPONSE REASON: ".$response_reason."[--CR--]";
		write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

		$system_id = $transaction_vars['system_id'];

		if ($approved == "1") {
			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, $split_tender_id, disinterpret($card_number."|".$process_info['exp_month'].$process_info['exp_year']."|".$process_info['card_cvn']), "", "", "");

			return "1|".$new_pnref."|".substr($card_number, -4)."|Approved|".$authcode."|".$card_type."|||".$process_info['order_id'];
		
		} else {
		
			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

			$resptitle = "Error";
			$msg = (empty($response_description))?"Unknown Response":$response_description;
			switch ($response_code) {
				case 2 : $resptitle = "Declined"; break;
				case 4 : $msg = "Held for Review"; break;
			}
			
			return "0|".$msg."|".$new_pnref."|".$process_info['order_id']."||".$resptitle;
		}
	}

	function process_authorize_net($process_info, $location_info) {

	global $company_info;

	$debug = print_r($process_info, true)."\n\n";

	$card_number = $process_info['card_number'];
	$mag_data = $process_info['mag_data'];

	$first_four = substr($card_number, 0, 4);
	$process_info['last_four'] = substr($card_number, -4);
	$card_type = getCardTypeFromNumber($card_number);

	$swiped = "0";
	$swipe_grade = "H";
	$track1 = "";
	$track2 = "";
	if ($mag_data) {
		$swiped = "1";
		$swipe_grade = "G";
		$tracks = explode("?", $mag_data);
		$track2_index = 1;
		if (strstr($tracks[0], "=")) $track2_index = 0;
		if (($track2_index == 1) && (count($tracks) >= 1)) {
			$t1 = $tracks[0];
			if (strlen($t1) > 10) {
				$track1 = $t1;
				$swipe_grade = "F";
			}
		}
		if (count($tracks) > $track2_index) {
			$t2 = $tracks[$track2_index];
			if (strlen($t2) > 10) {
				$track2 = $t2;
				if ($swipe_grade == "F") $swipe_grade = "D";
				else $swipe_grade = "E";
			}
		}
	}

	if ($process_info['set_pay_type'] != "") {
		$pay_type = $process_info['set_pay_type'];
		$pay_type_id = $process_info['set_pay_type_id'];
	} else {
		$pay_type = "Card";
		$pay_type_id = "2";
	}

	$transtype = $process_info['transtype'];
	if ($transtype == "") $transtype = "Sale";

	$check_for_duplicate = false;
	switch ($transtype) {
		case "Sale" : $use_transtype = "AUTH_CAPTURE"; $check_for_duplicate = true; break;
		case "Auth" : $use_transtype = "AUTH_ONLY"; $check_for_duplicate = true; break;
		case "PreAuthCapture" : $use_transtype = "PRIOR_AUTH_CAPTURE"; break;
		case "Return" :
			$use_transtype = "CREDIT";
			//$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `card_desc` = '[last_four]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
			//$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			//$process_info['pnref'] = $pnref;
			break;
		case "Void" :
			$use_transtype = "VOID";
			$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			break;
		case "VoidAuth" : $use_transtype = "VOID"; break;
		case "AddTip" : $use_transtype = "AUTH_CAPTURE"; break;
	}

	$transaction_vars = generate_transaction_vars($process_info, $location_info, $card_number, $card_type, $transtype, $swipe_grade, $pay_type, $pay_type_id, $process_info['card_amount']);

	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	$system_id = 0;
	if ($transtype!="VoidAuth" && $transtype!="PreAuthCapture") {
		$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
		if (!$preliminary_record_transaction) return "0|Failed to create transaction record.||".$process_info['order_id'];
		$system_id = lavu_insert_id();
	}

	$this_gateway = "Authorize.net";
	$tx_desc = "POS Lavu - ".$company_info['company_name'];
	if (in_array($transtype, array("Void", "GiftCardVoid", "LoyaltyCardVoid", "VoidGiftCardIssueOrReload", "VoidLoyaltyCardIssueOrReload"))) $tx_desc = "VOID - ".$tx_desc;
	if (in_array($transtype, array("PreAuthCapture", "PreAuthCaptureForTab"))) $tx_desc = "Capture - ".$tx_desc;
	if ($transtype == "AddTip") $tx_desc = "Added Tip - ".$tx_desc;

	if (in_array($location_info['market_type'], array("", "Web", "Moto", "Mobile")) || in_array($location_info['gateway'], array("BluePay", "Elavon"))) {

		$post_url = "https://secure.authorize.net/gateway/transact.dll";
		if ($location_info['gateway'] == "BluePay") {
			$this_gateway = "BluePay";
			$post_url = "https://secure.bluepay.com/interfaces/a.net";
		}
		if ($location_info['gateway'] == "Elavon") {
			$this_gateway = "Elavon";
			$post_url = "https://secure.internetsecure.com/process.cgi";
		}

		$rcode_index = 0;
		$transaction_id_index = 6;
		$card_type_index = 51;
		$split_tender_id_index = 52;

		$post_values = array(
			"x_login"			=> $process_info['username'], // 8wvM4Z5T // 40001
			"x_tran_key"		=> $process_info['password'], // 8x4cPL589rhWuW5X // 11111111115

			"x_version"			=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "FALSE",

			"x_type"			=> $use_transtype,
			"x_method"			=> "CC",
			"x_card_num"		=> $process_info['card_number'], //"4111111111111111",
			"x_card_code"		=> $process_info['card_cvn'],
			"x_exp_date"		=> $process_info['exp_month'].$process_info['exp_year'], //"0115",

			"x_amount"			=> $process_info['card_amount'], //"19.99",
			"x_description"		=> $tx_desc, //"Sample Transaction",
			"x_invoice_num" => $process_info['loc_id'].$process_info['order_id'],
			"x_trans_id" => $process_info['pnref'],

			"x_first_name"		=> "", //"John",
			"x_last_name"		=> "", //"Doe",
			"x_address"			=> "", //"1234 Street",
			"x_city"			=> "",
			"x_state"			=> "", //"WA",
			"x_zip"				=> "", //"98004"

		);

		if (in_array($transtype, array("Void", "VoidAuth", "PreAuthCapture", "Return", "Settle", "Adjust&Settle"))) {
			$more_values = array("x_trans_id" => $process_info['pnref']);
			$post_values = array_merge($post_values, $more_values);
		}

	} else if ($location_info['market_type'] == "Retail") {

		$post_url = "https://cardpresent.authorize.net/gateway/transact.dll";

		$market_type_code = 2;
		$device_type = 7;

		$rcode_index = 1;
		$transaction_id_index = 7;
		$card_type_index = 21;
		$split_tender_id_index = 22;

		$post_values = array(

			"x_login"			=> $process_info['username'], // 8wvM4Z5T
			"x_tran_key"		=> $process_info['password'], // 8x4cPL589rhWuW5X

			"x_market_type" => $market_type_code,
			"x_device_type" => $device_type,

			"x_cpversion"			=> "1.0",
			//"x_test_request"  => "TRUE",

			"x_response_format" => "1",
			"x_delim_char"      => "|",

			"x_invoice_num" => $process_info['loc_id'].$process_info['order_id'],
			"x_description"		=> $tx_desc, //"Sample Transaction",

			"x_amount"			=> $process_info['card_amount'], //"19.99",
			"x_method"			=> "CC",
			"x_type"			=> $use_transtype,

			"x_card_num"		=> $process_info['card_number'], //"4111111111111111",
			"x_exp_date"		=> $process_info['exp_month'].$process_info['exp_year'], //"0115",
			"x_card_code"		=> $process_info['card_cvn'],

			"x_track1" => $track1,
			"x_track2" => $track2
		);

		if (in_array($transtype, array("Void", "VoidAuth", "PreAuthCapture", "Return", "Settle", "Adjust&Settle"))) {
			$more_values = array("x_ref_trans_id" => $process_info['pnref']);
			$post_values = array_merge($post_values, $more_values);
		}
	}

	$debug .= "\n\n".$post_url."\n\n";

	$post_string = "";
	foreach( $post_values as $key => $value ) {
		$post_string .= "$key=" . urlencode( $value ) . "&";
	}
	$post_string = rtrim( $post_string, "& " );

	ConnectionHub::closeAll();

	$request = curl_init($post_url); // initiate curl object
	curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
	curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
	curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
	curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
	$post_response = curl_exec($request); // execute curl post and store results in $post_response
	curl_close($request); // close curl object

	$response_array = explode($post_values["x_delim_char"], $post_response);
	$response_code = (string)$response_array[$rcode_index];
	$response_reason = $response_array[2];
	$response_description = $response_array[3];
	$authcode = $response_array[4];
	$new_pnref = $response_array[$transaction_id_index];
	$card_type = $response_array[$card_type_index];
	$split_tender_id = $response_array[$split_tender_id_index];

	if ($mag_data && empty($track1) && empty($track2) && $response_code!="1") $response_description = "Card read failed. Please try again.||".$process_info['order_id']."||Error|||";

	$debug .= strtoupper($this_gateway)."\n\n".$post_string."\n\nRESULT: ".$response_code."\nRESPONSE DESCRIPTION: ".$response_description."\nRESPONSE REASON: ".$response_reason."\n".$authcode."\n".$new_pnref."\n".$split_tender_id."\n".$card_type."\n\n".$post_response;

	$approved = "0";
	if ($response_code=="1" && !strstr($response_code, ".")) $approved = "1";
	if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], $this_gateway, $transtype, $approved, $debug);

	$log_string = strtoupper($this_gateway)."[--CR--]";
	$log_string .= "LOCATION: ".$process_info['loc_id']." - ";
	$log_string .= "REGISTER: ".$process_info['register']." - ";
	$log_string .= "SERVER ID: ".$process_info['server_id']." - ";
	$log_string .= "ORDER ID: ".$process_info['order_id']." - ";
	$log_string .= "AMOUNT: ".$process_info['card_amount']." - ";
	$log_string .= "TRANSTYPE: ".$transtype." - ";
	$log_string .= "RESULT: ".$response_code." - ";
	$log_string .= "TRANSACTION ID: ".$new_pnref." - ";
	$log_string .= "SPLIT TENDER ID: ".$split_tender_id." - ";
	$log_string .= "AUTHCODE: ".$authcode." - ";
	$log_string .= "CARD TYPE: ".$card_type." - ";
	$log_string .= "CARD DESC: ".substr($card_number, -4)." - ";
	$log_string .= "SWIPED: ".$swiped."[--CR--]";
	$log_string .= "EXT_DATA: ".$ext_data."[--CR--]";
	$log_string .= "RESPONSE DESCRIPTION: ".$response_description."[--CR--]";
	$log_string .= "RESPONSE REASON: ".$response_reason."[--CR--]";
	write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

	if ($approved == "1") {

		update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, $split_tender_id, disinterpret($card_number."|".$process_info['exp_month'].$process_info['exp_year']."|".$process_info['card_cvn']), "", "", "");

		return "1|".$new_pnref."|".substr($card_number, -4)."|Approved|".$authcode."|".$card_type."|||".$process_info['order_id'];

	} else {

		$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

		$resptitle = "Error";
		$msg = (empty($response_description))?"Unknown Response":$response_description;
		switch ($response_code) {
			case 2 : $resptitle = "Declined"; break;
			case 4 : $msg = "Held for Review"; break;
		}

		return "0|".$msg."|".$new_pnref."|".$process_info['order_id']."||".$resptitle;
	}
}
?>