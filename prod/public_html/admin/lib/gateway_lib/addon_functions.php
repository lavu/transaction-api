<?php

	function get_gateway_addons($transtype) {

		$addon_list = array();

		$path = dirname(__FILE__)."/../jcvs/jc_addons/gw/".strtolower($transtype);
		if (is_dir($path)) {
			if ($dir = opendir($path)) {
				while (false !== ($file = readdir($dir))) {
					if (substr($file, -4) == ".php") $addon_list[] = str_replace(".php", "", $file);
				}
				closedir($dir);
			} else error_log("Unable to open addon path: ".$path);
		}

		return $addon_list;
	}

	function process_gateway_addons($gateway_addons, $active_modules, $transtype, $boa, $response="") { // boa = "before or after"

		global $company_info;
		global $location_info;
		global $device_time;

		//error_log("process_gateway_addons: ".$transtype." ".$boa);
		//error_log("gateway_addons: ".print_r($gateway_addons, true));

		foreach ($gateway_addons as $addon) {

			//error_log("addon: ".$addon);

			$check_module = str_replace("_", ".", $addon);

			if ($active_modules->hasModule($check_module)) {

				//error_log("has module: ".$check_module);

				$addon_path = dirname(__FILE__)."/../jcvs/jc_addons/gw/".strtolower($transtype)."/".$addon.".php";
				
				//error_log("addon_path: ".$addon_path);
					
				if (file_exists($addon_path)) {
					//error_log("including ".$addon_path);
					require_once($addon_path);
				}

				if (function_exists($boa."_".$addon)) {
					//error_log("should call ".$boa."_".$addon);
					$response = call_user_func($boa."_".$addon, $response);
				}

				//error_log("response: ".$response);

			} //else error_log("doesn't have module: ".$check_module);
		}

		return $response;
	}

?>