<?php

require_once(dirname(__FILE__) . "/../../components/payment_extensions/PayPal/PayPalPaymentHandler.php");
require_once(dirname(__FILE__) . "/../gateway_functions.php");

function format_paypal_gateway_response($rvars, $process_info)
{
    $lastModTS = (empty($process_info['last_mod_ts'])) ? "" : $process_info['last_mod_ts'];
	$rtn = array();
	if(isset($rvars['approved']) && $rvars['approved'] === true)
	{
		$rtn[] = "1"; // 0
		$rtn[] = $rvars['txnid']; //transaction ID
		$rtn[] = $rvars['last_four'];
		$rtn[] = $rvars['message'];;
		$rtn[] = $rvars['txnid'];
		$rtn[] = getCardTypeFromNumber($process_info['card_number']);
		$rtn[] = "";
		$rtn[] = $process_info['card_amount'];
		$rtn[] = $process_info['order_id'];
		$rtn[] = "";
		$rtn[] = ""; // 10
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = $lastModTS;
	}
	else
	{
		$rtn[] = "0";
		$rtn[] = $rvars['message'];
		$rtn[] = $rvars['txnid'];
		$rtn[] = $rvars['order_id'];
		$rtn[] = "";
		$rtn[] = (isset($rvars['title']))?$rvars['title']:"Unable to Process";
	}

	return implode("|",$rtn);
}

function pp_postvar($k,$d="")
{
	if(isset($_POST[$k])) return $_POST[$k];
	else if(isset($_GET[$k])) return $_GET[$k];
	else return $d;
}

function paypal_format_decline($message,$process_info)
{
	$rvars = array();
	$rvars['approved'] = false;
	$rvars['message'] = $message;
	$rvars['order_id'] = pp_postvar("order_id");
	$rvars['txnid'] = "";
	$rvars['last_four'] = substr($process_info['card_number'],-4);

	return format_paypal_gateway_response($rvars, $process_info);
}

function paypal_write_logfile($wstr)
{
	error_log(__FILE__ . " RESPONSE FROM PAYPAL API CALL:  ".$wstr);

}

function setTransactionFields($paypal_email, $process_info, $location_info){

    $PaymentAction = ($process_info['transtype'] === 'Sale') ? "sale" : "authorization";
	$card_amount = (isset($process_info['card_amount']))?$process_info['card_amount']:pp_postvar("card_amount");
    $tip_amount = (empty($process_info['tip_amount']))?"0":$process_info['tip_amount'];
    $set_number = (isset($process_info['card_number']))?$process_info['card_number']:"";
	$set_exp_month = $process_info['exp_month'];
	$set_exp_year = $process_info['exp_year'];
	$set_cvv = (isset($process_info['card_cvn']))?$process_info['card_cvn']:"";
	$card_fields = '"card": {'.
						 '"inputType": "keyIn",'.
						 '"accountNumber": "'.$set_number.'",'.
						 '"expirationMonth": "'.$set_exp_month.'",'.
						 '"expirationYear": "'.$set_exp_year.'",'.
						 '"cvv": "'.$set_cvv.'",'.
						 '"postalCode": "87120"},';

	if(!empty($process_info['mag_data'])){
		$mag_info = explode("|",$process_info['mag_data']);
		$set_track1 = $mag_info[2];
		$set_track2 = $mag_info[3];
		$set_reader_serial = $mag_info[7];
		$set_key_serial = $mag_info[9];

		$card_fields = '"card":{'.
					  '"track1":"'.$set_track1.'",'.
					  '"inputType":"swipe",'.
					  '"track2":"'.$set_track2.'",'.
					  '"reader":{'.
						'"readerSerialNumber":"'.$set_reader_serial.'",'.
						 '"vendor":"MAGTEK",'.
						 '"keySerialNumber":"'.$set_key_serial.'"},'.
					  '"signatureRequired":false},';
	}

	$fields = '{'.
				   '"paymentType":"card",'.
				   '"paymentAction":"'.$PaymentAction.'",' //authorization
				   .$card_fields.
				   '"invoice":{'.
					  '"cashierId":"YourCashierIDHere",'.
					  '"merchantEmail":"'.$paypal_email.'",'.
					  '"merchantInfo":{'.
						' "businessName":"Business Name",'.
						 '"address":{'.
							'"line1":"530 Brannan Street",'.
							'"line2":"Suite 311",'.
							'"city":"San Francisco",'.
							'"state":"CA",'.
							'"postalCode":"94107",'.
							'"country":"US"'.
						 '},'.
						 '"phoneNumber":"408-542-9673"'.
					  '},'.
					  '"items":[{'.
							'"name":"Payment",'.
							'"quantity":1,'.
							'"unitPrice":'.$card_amount.','.
							'"taxRate":0,'.
							'"taxName":"null"}],'.
					  '"currencyCode":"USD",'.
					  '"partnerReferrerCode":"Lavu_POS_PPHSDK",'.
					  '"paymentTerms":"Paid in full.",'.
					  '"receiptDetails":{'.
						'"merchant":{'.
							'"terminalId":"YourTerminalId",'.
							'"storeId":"StoreID123"}},'.
					  '"gratuityAmount":'.$tip_amount.','.
					  '"discountAmount":0,'.
					  '"taxCalculatedAfterDiscount":true}}';
	return $fields;
}


function process_paypal($process_info,$location_info)
{
	global $data_name;
	$order_id = $process_info["order_id"];
	$transtype = isset($process_info['transtype'])?$process_info['transtype']:"";

	$transaction_vars = generate_transaction_vars($process_info, $location_info, $process_info['card_number'],
													getCardTypeFromNumber($process_info['card_number']), $process_info['transtype'],
													"", $process_info['set_pay_type'], $process_info['set_pay_type_id'], $process_info['card_amount']);

	if(!isset($data_name)) {
		return "0|Merchant Information Unavailable, please try processing via the PayPal button.";
	}

	//error_log("PAYPAL FUNC REQUEST".print_r($_REQUEST, 1));
	$core = new PayPalCore();
	$_REQUEST['dataname'] = $data_name;
	//$paypal_email = get_paypal_account_email();//Paypal account email is already available in $process_info.
	if (!$process_info['kiosk']) {
		$paypal_email = $process_info['username'];
		if(trim($paypal_email)=="" || strpos($paypal_email,"@")===false || strpos($paypal_email,".")===false)
		{
			return paypal_format_decline("Incomplete Setup: Please enter your paypal email in your control panel under Settings > CC Integration > Integration 1", $process_info);
		}
	}

	//Block to setup the transaction

	//set Transaction ID
	$cleanupRequired = false;
    $txnid = isset($process_info['authcode'])?$process_info['authcode']:isset($process_info['pnref'])?$process_info['pnref']:"";
    $record_no = "";
    $authcode = "";
    $pnref = $process_info['pnref'];

    //getting the transaction row if it exists, generating the row if it does not exist and we need to create it
    $transactionRow = lavu_query("select * from `cc_transactions` where `order_id`='[1]' and (`preauth_id`='[2]' OR `transaction_id`='[2]' OR `auth_code`='[2]') and `check`='[3]' order by id desc limit 1",$order_id,$txnid, $process_info['check']);
    if(mysqli_num_rows($transactionRow) < 1){
        if($transtype === "Sale" || $transtype === "Auth") {
            $txn_system_id = paypal_create_initial_transaction_record($location_info, $transaction_vars);
            $cleanupRequired = true;
        }
    }
    else{
        $transactionArray = mysqli_fetch_assoc($transactionRow);
        $txn_system_id = isset($transactionArray['id'])?$transactionArray['id']:"";
        $preauthID = isset($transactionArray['preauth_id'])?$transactionArray['preauth_id']:"";
        $authcode = isset($transactionArray['auth_code'])?$transactionArray['auth_code']:"";
        $txnid = isset($transactionArray['transaction_id'])?$transactionArray['transaction_id']:$txnid;

    }
    
    $create_transaction_record = false;
    
	switch($transtype){
		case "Void":
		    $voidID = empty($preauthID) ? $authcode : $preauthID;
			$fields = '';
			$curlURL = "v1/payments/authorization/$voidID/void";
			break;
		case "Return":
		    $RefundID = empty($preauthID)? (empty($authcode)? $pnref : $authcode) : $preauthID;
		    $returnCard = $process_info['card_amount'];
            $returnTip = $process_info['refund_tip_amount'];
            $return_amount = str_replace(",","",number_format($returnCard * 1 + $returnTip * 1,2));
			$create_transaction_record = true;
			if ($process_info['reskin'] && $process_info['kiosk']) {
				$papiEndpoint = "/api/paypal/payments/$RefundID/refund";
				require_once(__DIR__ .'/../../../inc/papi/jwtPapi.php');
				$locationId = paypal_current_location_id();
				$args['dataname'] = $data_name;
				$args['locationid'] = $locationId;
				$jwtPapiObj = new jwtPapi($args);
				$encDataname = $jwtPapiObj->genEncryptedDataname();
				$return_amount = $return_amount * 100;
				$fields = '{'.
					'"amount": '. $return_amount.', '.
					'"idempotentId": "'. uniqid().'", '.
					'"gateway": {"dataname": "' . $encDataname . '", "refreshUrl": "' . getenv("PAYPAL_REFRESH_TOKEN_URL") . '"}'.
					'}';
			} else {
				if($transactionArray['transtype'] === 'Auth'){
					if( $transactionArray['temp_data']=="" && $transactionArray['auth']==0 ){
						$RefundID = $txnid;
					}
					$curlURL = "v1/payments/capture/$RefundID/refund";
				}
				else{
					$ReturnID = empty($authcode)? empty($txnid) ? "" : $txnid : $authcode;
					$curlURL = "v1/payments/sale/$RefundID/refund";
				}
				$fields = '{"amount": {'.
					'"total": '. $return_amount.','.
					'"currency": "USD"},'.
					'"is_non_platform_transaction": "yes"}';
				}

			break;
		case "Sale":
		case "Auth":
			$fields = setTransactionFields($paypal_email, $process_info, $location_info);
			$curlURL = "retail/merchant/v1/pay";
			break;
		case "Adjustment":
		case "PreAuthCapture":
			$tip_amount = $process_info['tip_amount'];
            $captureID = empty($authcode)? (empty($txnid)? "" : $txnid) : $authcode;
            if(empty($captureID)){
                return "0|Unable to retrieve transaction identifier";
            }
            $base_amount = $transactionArray['amount'];
            $card_amount = str_replace(",","",number_format($base_amount * 1 + $tip_amount * 1,2));

			$fields = '{"amount":{'.
				'"currency":"USD",'.
				'"total":[amount]},'.
			  '"is_final_capture":"false"}';

			$fields = str_replace("[amount]",$card_amount,$fields);

			$curlURL = "v1/payments/authorization/$captureID/capture";
			break;
		default:
			return "0|Unsupported Transaction Type";
	}

	$q_fields = "";
	$q_values = "";

	if ($create_transaction_record) {
		buildInsertFieldsAndValues($transaction_vars, $q_fields, $q_values);
		$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
		if (!$preliminary_record_transaction) return "0|Failed to create transaction record.||".$process_info['order_id'];
		$txn_system_id = lavu_insert_id();
		$cleanupRequired = true;
	}

	//Perform the transaction

	if ($process_info['reskin'] && $process_info['kiosk']) {
		require_once(dirname(__FILE__) . "/../../components/payment_extensions/PayPal/PapiPaymentHandler.php");
		$location_id = paypal_current_location_id();
		$PapiPaymentHandler = new PapiPaymentHandler($data_name, $location_id);
		$response = $PapiPaymentHandler->makePapiPayment($fields, $papiEndpoint);
	} else {
		$PayPalPaymentHandler = new PayPalPaymentHandler($data_name);
		$response = $PayPalPaymentHandler->makePayment($fields, $core->PayPalEndpointURL.$curlURL);
	}

	$rspvars = json_decode($response,true);

	if(lavuDeveloperStatus()){
		paypal_write_logfile($response);
	}

	$txn = (isset($rspvars['transactionNumber']))?$rspvars['transactionNumber']:"";
	$rcode = (isset($rspvars['responseCode']))?$rspvars['responseCode']:"";
	$message = (isset($rspvars['message']))?$rspvars['message']:"";
	$state = (isset($rspvars['state']))?$rspvars['state']:"";
    $ReturnID = empty($rspvars['id'])?"":$rspvars['id'];
    $errorType = empty($rspvars['errorType'])? "" : $rspvars['errorType'];

    $rspstr = "No Recognized Response";

    $approved = true;
    $rspstr = "Approved";
    /*
     * transactionNumber = auth_code
     * id = refund_pnref or void_pnref
     * When Adjusting or capturing, the id will populate the preauth_id field
     */
	if($rcode==="0000") {
		paypal_update_transaction_record($process_info,$location_info,$txn,$txn,$txn_system_id, "");
	}
	else if($state==="completed" && ($transtype === "Adjustment" || $transtype === "PreAuthCapture")){
        paypal_update_transaction_record($process_info,$location_info,$ReturnID,$txnid,$txn_system_id, "");
        lavu_query("update `cc_transactions` set `preauth_id`='[1]' where `id` = '[2]'",$ReturnID,$txn_system_id);
    }
    else if($state === "completed" && $transtype === "Return"){
        paypal_update_transaction_record($process_info,$location_info,$txn,$txn,$txn_system_id, "");
        lavu_query("update `cc_transactions` set `refunded`='1', `refund_pnref`='[1]' where `id`='[2]'",$ReturnID,$txn_system_id);
    }
	else if($state === "voided"){
        paypal_update_transaction_record($process_info,$location_info,$txn,"",$txn_system_id, "");
        lavu_query("update `cc_transactions` set `voided`='1', `void_pnref`='[1]' where `id`='[2]'",$ReturnID,$txn_system_id);

	}
	else {
		$approved = false;
        switch($errorType){
            case "invalid_cvv":
                $message = "Invalid CVV, double check manual entry.";
                break;
            case "service_error":
                $message = "PayPal Server Error.  If this problem persists, please contact PayPal Support";
                break;
            case "pay/bad_card":
                $message = "Invalid Card Data.  Please retry.";
                break;
            default:
                break;

        }
		$rspstr = "Declined\n$message";
		if($cleanupRequired && $transtype === "Sale" || $transtype === "Auth" || $transtype === "Return") {
			lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $txn_system_id);
		}
	}

    $card_amount = (isset($process_info['card_amount']))?$process_info['card_amount']:pp_postvar("card_amount");
    $tip_amount = (empty($process_info['tip_amount']))?"0":$process_info['tip_amount'];
    $lg_str = "PayPal REST Payment [--CR--]";
    $lg_str .= "LOCATION: ".			$process_info['loc_id']						." - ";
    $lg_str .= "REGISTER: ".			$process_info['register']	." - ";
    $lg_str .= "SERVER ID: ".		$process_info['server_id']	." - ";
    $lg_str .= "ORDER ID: ".			$order_id					." - ";
    $lg_str .= "CHECK : ".			$process_info['check']		." - ";
    $lg_str .= "AMOUNT: ".			$card_amount. " TIP AMOUNT: ".$tip_amount." - ";
    $lg_str .= "TRANSTYPE: ".		$transtype					." - ";
    $lg_str .= "RESULT: ".			$approved						." - ";
    $lg_str .= "PNREF: ".			empty($txn) ? empty($ReturnID)?"" : $ReturnID : $txn					." - ";
    $lg_str .= "AUTHCODE: ".			$authcode					." - ";
    $lg_str .= "CARD TYPE: ".		$transaction_vars['card_type']					." - ";
    $lg_str .= "FIRST FOUR: ".		$transaction_vars['first_four']					." - ";

    $set_number = (isset($process_info['card_number']))?$process_info['card_number']:"";
    $lg_str .= "LAST FOUR: ".		substr($set_number, -4)				." - ";
    $swiped = 0;
    if (!empty($process_info['mag_data'])){

        $mag_info = explode("|", $process_info['mag_data']);
        $readerstring = "MAGTEK SWIPER SERIAL NO. " . $mag_info[7];
        $lg_str.= "READER: ".	$readerstring	." - ";
        $swiped = 1;
    }
    $lg_str .= "SWIPED: ".			$swiped						."[--CR--]";
    $lg_str .= "SENT EXT_DATA: ".	$process_info['ext_data']					."[--CR--]";
    $lg_str .= "RESPONSE MESSAGE: ".$rspstr						."[--CR--]";
    write_log_entry("gateway", $data_name, $lg_str."\n");

	$rvars = array();
	$rvars['approved'] = $approved;
	$rvars['message'] = $rspstr;
	$rvars['order_id'] = $order_id;
	$rvars['txnid'] = empty($txn) ? (empty($ReturnID)?"":$ReturnID):$txn;
	$rvars['last_four'] = substr($process_info['card_number'],-4);


	return format_paypal_gateway_response($rvars, $process_info);

}

function first_string_element_only($str)
{
	$str_parts = explode(",",$str);
	$str_parts = explode(";",$str_parts[0]);
	return trim($str_parts[0]);
}

function urldecode_string($str)
{
	$vars = array();
	$str_parts = explode("&",$str);
	for($i=0; $i<count($str_parts); $i++)
	{
		$inner_parts = explode("=",$str_parts[$i]);
		if(count($inner_parts) > 1)
		{
			$k = trim($inner_parts[0]);
			$v = trim($inner_parts[1]);
			$vars[$k] = $v;
		}
	}
	return $vars;
}

function paypal_current_location_id()
{
	global $location_info;
	global $location_id;
	global $locationid;
	global $loc_id;
	if(isset($loc_id)) return $loc_id;
	else if(isset($locationid)) return $locationid;
	else if(isset($location_id)) return $location_id;
	else if(isset($location_info) && is_array($location_info) && isset($location_info['id'])) return $location_info['id'];
	else return "1";
}

function paypal_create_initial_transaction_record($location_info, $transaction_vars)
{
	global $location_info;
	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	$create_transaction_record = true;
	$system_id = 0;
	if ($create_transaction_record) {
		$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
		if (!$preliminary_record_transaction) return 0;//"0|Failed to create transaction record.||".$order_id;
		$system_id = lavu_insert_id();
	}
	return $system_id;
}

function paypal_update_transaction_record($process_info,$location_info,$transaction_id,$authcode,$system_id, $record_no)
{
	update_tables($process_info, $location_info, getCardTypeFromNumber($process_info['card_number']), $transaction_id, $authcode, $system_id, "0", "", "", "", $record_no);
}
/*
 * getPaypalCaptureDetails -LP-5756
 */
function getPaypalCaptureDetails($order_id, $transaction_id, $check, $device_time, $loc_id, $server_id, $server_name, $company_id, $device_udid){
    global $data_name;
    global $location_info;
    
    $check_got_response = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";
    $transactionRow = lavu_query("select * from `cc_transactions` where `order_id`='[1]' and `transaction_id`='[2]' and `check`='[3]' and `auth`='1' AND `pay_type_id` = '2' AND `action` = 'Sale' AND `voided` != '1' AND `order_id` NOT LIKE '777%' ".$check_got_response." order by `check`, `id` desc limit 1", $order_id, $transaction_id, $check); 
    $tipTransactionData=array();
    if(mysqli_num_rows($transactionRow) > 0){
        $transaction_info = mysqli_fetch_assoc($transactionRow);
        $cc_orderId = $transaction_info['order_id'];
        $cc_transactionId = $transaction_info['transaction_id'];
        
        $get_main_tips = mlavu_query("SELECT `order_id`, `transaction_id` FROM `cc_tip_transactions` WHERE `dataname` = '[1]' AND `order_id` = '[2]' AND transaction_id='[3]' ", $data_name, $cc_orderId, $cc_transactionId);
       
        if(mysqli_num_rows($get_main_tips)< 1){
            require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
            $integration_info = get_integration_from_location($loc_id, "poslavu_".$data_name."_db");
            
            $process_info = array(
                'card_amount'		=> "",
                'card_cvn'			=> "",
                'card_number'		=> "",
                'company_id'		=> $company_id,
                'data_name'			=> $data_name,
                'device_time'		=> $device_time,
                'device_udid'		=> $device_udid,
                'exp_month'			=> "",
                'exp_year'			=> "",
                'ext_data'			=> "",
                'for_deposit'		=> "",
                'is_deposit'		=> "",
                'loc_id'			=> $loc_id,
                'mag_data'			=> "",
                'name_on_card'		=> "",
                'reader'			=> "",
                'register'			=> $transaction_info['register_name'],
                'register_name'		=> $transaction_info['register_name'],
                'server_id'			=> $server_id,
                'server_name'		=> $server_name,
                'set_pay_type'		=> "Card",
                'set_pay_type_id'	=> "2",
                'for_deposite'      => $transaction_info['for_deposite'],
                'order_id'          => $cc_orderId,
                'authcode'          => $transaction_info['auth_code']
            );
           
            $tipTransactionData['dataname'] = $data_name;
            $tipTransactionData['order_id'] = $cc_orderId;
            $tipTransactionData['transaction_id'] = $cc_transactionId;
            $idatetime = date("Y-m-d H:i:s");
            $transaction_info_array = array("location_info" => $location_info, "integration_info" => $integration_info, "process_info" => $process_info, "transaction_info" => $transaction_info, "tip_amount"=>$transaction_info['tip_amount']);
            $tipTransactionData['transaction_info'] = json_encode($transaction_info_array);
            $tipTransactionData['status'] = 3; // default in pending -> 3 status
            $tipTransactionData['created_date'] = $idatetime;
            $tipTransactionData['updated_date'] = $idatetime;
            
            mlavu_query("INSERT INTO `cc_tip_transactions` (`dataname`, `order_id`,`transaction_id`, `transaction_info`, `status`, `created_date`, `updated_date`) VALUES ('[dataname]', '[order_id]', '[transaction_id]','[transaction_info]', '[status]', '[created_date]', '[updated_date]')", $tipTransactionData);
           
             if(mlavu_insert_id()){
                $reponse_arr['order_id'] = $cc_orderId;
                $reponse_arr['transaction_id'] = $cc_transactionId;
                $reponse_arr['success_message'] = 'Tip details has been inserted.';
                $reponse_arr['status']= 'success';
            }
        }
        else{
            $reponse_arr['order_id'] = $cc_orderId;
            $reponse_arr['transaction_id'] = $cc_transactionId;
            $reponse_arr['error_message'] = 'Tip details already exists';
            $reponse_arr['status']= 'failure';
        }
    }
    else{
        $reponse_arr['order_id'] = $order_id;
        $reponse_arr['transaction_id'] = $transaction_id;
        $reponse_arr['error_message'] = 'Transaction details are not available.';
        $reponse_arr['status']= 'failure';
    }
     return $reponse_arr;
}//end of getPaypalCaptureDetails