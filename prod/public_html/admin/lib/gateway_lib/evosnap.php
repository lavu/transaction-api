<?php
#EvoSnap
error_reporting(E_ALL);

require_once(__DIR__.'/../../cp/resources/core_functions.php');

class EvoSnap
{
	private $LavuToken;
	private $SessionToken;

	private $ApplicationProfileId = '73141';
	private $MerchantProfileId    = '';
	private $ServiceID            = '';
	private $WorkflowID           = 'A121700001';
	private $Integration3;
	private $Integration4;
	private $Integration5;
	private $tokenid = 52215;

	private $BaseURL = 'https://api.cipcert.goevo.com/REST/2.0.22/';
	private $TXNURL = 'https://cert-hp.evosnap.com/srv/txn.php';

	#Card Info
	private $Encrypted;
	private $Keyed;
	public $CardType;
	private $PAN; #Keyed
	private $Expiration; #Keyed
	private $IdentificationInformation;
	private $EncryptionKeyId;
	private $SecurePaymentAccountData;
	private $SwipeStatus;

	#Transaction Info
	public $Amount;
	public $CurrencyCode    = 'USD';
	public $EmployeeID      = '0000';
	public $OrderID         = '5-123';
	public $ReferenceID;
	public $TipAmount       = '0.00';
	public $TransactionType = '';
	private $HostedTransaction = false;

	#DEBUG VARS
	public $CurlSent;
	public $CurlInfo;
	public $CurlHeaders;
	public $CurlRawResponse;
	public $GatewayErrorResponse;
	public $GatewayResponse;

	public function __construct($integration1 = '',$integration2 = '',$integration3 = '',$integration4 = '',$integration5 = '')
	{
		$this->MerchantProfileId = $integration1;
		$this->ServiceID = $integration2;
		$this->Integration3 = $integration3;
		$this->Integration4 = $integration4;
		$this->Integration5 = $integration5;

		$this->LavuToken = file_get_contents('/home/poslavu/private_html/evosnap.token.txt');
		if(!lavuDeveloperStatus()){
			$this->LavuToken = file_get_contents('/home/poslavu/private_html/evosnap.token.production.txt');
			$this->BaseURL = 'https://api.cip.goevo.com/REST/2.0.22/';
			$this->TXNURL = 'https://hp.evosnap.com/srv/txn.php';
			$this->ApplicationProfileId = '179430';
			$this->WorkflowID = 'A01391300C';
			$this->tokenid = 52216;
		}
		else{
            $this->LavuToken = file_get_contents('/home/poslavu/private_html/evosnap.token.txt');
            $this->BaseURL = 'https://api.cipcert.goevo.com/REST/2.0.22/';
            $this->TXNURL = 'https://cert-hp.evosnap.com/srv/txn.php';
            $this->ApplicationProfileId = '73141';
            $this->WorkflowID = 'A121700001';
            $this->tokenid = 52216;
        }
		//try and get session token from db
		$this->SessionToken = $this->getTokenFromDB();
		if(empty($this->SessionToken)){
			$this->startSession();
		}

	}

	private function getTokenFromDB()
	{
		$result =  mlavu_query('SELECT UNIX_TIMESTAMP(`ts`) AS u_time,`token_contents` FROM `poslavu_MAIN_db`.`tokens` WHERE `token_type` = '.$this->tokenid);
		if(empty($result)){
			return false;
		}
		$token_info = mysqli_fetch_assoc($result );
		if(empty($token_info['token_contents'])){
			error_log(__FILE__.' '.__LINE__." Empty Token");
			return false;
		}
		$curr_time = time();
		if($token_info['u_time'] > $curr_time){
			error_log(__FILE__.' '.__LINE__." Invalid Time");
			return false;
		}
		if( ($curr_time - $token_info['u_time'] ) > 1200){
			error_log(__FILE__.' '.__LINE__." Token Expired");
			return false;
		}
		return $token_info['token_contents'];
	}

	private function setTokenToDB($token)
	{
		if ( base64_encode(base64_decode($token, true)) === $token){
			mlavu_query('UPDATE `poslavu_MAIN_db`.`tokens` SET `token_contents` = "'.$token.'" WHERE `token_type` = '.$this->tokenid);
			return true;
		}else{
			error_log(__FILE__.' '.__LINE__." Token Not Base64 ??! WTF");
			return false;
		}
	}

	public function set_iDyanamo($iDynamoString)
	{
		$splitData = explode("|", $iDynamoString);
		$idynamoData = array();
		$idynamoData['Reader']            = 'idynamo';
		$idynamoData['MaskedData']        = $splitData[0];
		$idynamoData['EncryptionStatus']  = $splitData[1];
		$idynamoData['TrackOneEncrypt']   = $splitData[2];
		$idynamoData['TrackTwoEncrypt']   = $splitData[3];
		$idynamoData['TrackThreeEncrypt'] = $splitData[4];
		$idynamoData['MagnePrintStatus']  = $splitData[5];
		$idynamoData['EncryptionMPData']  = $splitData[6];
		$idynamoData['DeviceSerial']      = $splitData[7];
		$idynamoData['SessionID']         = $splitData[8];
		$idynamoData['DUKPTSerial']       = $splitData[9];
		$t2Location = strpos($idynamoData['MaskedData'],';');
		if($idynamoData['TrackTwoEncrypt'] == ';E?'){
			error_log(__FILE__." Unable to parse Track2 Data");
			return false;
		}
		if($t2Location === false){
			error_log(__FILE__." Unable to parse Track2 MaskedData");
			return false;
		}
		$splitData = explode("=", substr($idynamoData['MaskedData'], $t2Location + 1));
		$idynamoData['PAN'] = str_replace('*','X',$splitData[0]);
		if(empty($idynamoData['PAN'])){
			error_log(__FILE__." Unable to parse Track2 MaskedData 2");
			return false;
		}
		return $this->setEncrypted($idynamoData);
	}

	public function startSession()
	{
		$this->SessionToken = $this->LavuToken;
		$tokenRawResponse = $this->post('SvcInfo/token');
		$tokenResponse = json_decode($tokenRawResponse,true);
		if( empty($tokenResponse) || (is_array($tokenResponse) && isset($tokenResponse['ErrorId']) ) ){
			error_log(__FILE__." ".__LINE__." Start Session Failure\n");
			$this->GatewayErrorResponse = $tokenResponse;
			return false;
		}else{
			$this->setTokenToDB($tokenResponse);
			$this->SessionToken = $tokenResponse;
			return true;
		}
	}

	public function setKeyed($PAN,$Expiration,$CardType)
	{
		$this->reset();
		$this->Keyed      = true;
		#$this->WorkflowID = '39C6700001';
		$this->PAN        = $PAN;
		$this->Expiration = $Expiration;
		$this->CardType   = $CardType;
		return true;
	}

	public function setEncrypted($EncryptionVars)
	{
		$this->reset();
		$this->Encrypted                 = true;
		#$this->WorkflowID                = 'A121700001';
		$this->PAN                       = $EncryptionVars['PAN'];
		$this->IdentificationInformation = $EncryptionVars['EncryptionMPData'];
		$this->EncryptionKeyId           = $EncryptionVars['DUKPTSerial'];
		$this->SecurePaymentAccountData  = $EncryptionVars['TrackTwoEncrypt'];
		$this->SwipeStatus               = $EncryptionVars['MagnePrintStatus'];
		$this->DeviceSerialNumber        = $EncryptionVars['DeviceSerial'];
		return true;
	}

	private function reset()
	{
		$this->Encrypted                 = false;
		$this->Keyed                     = false;
		$this->CardType                  = null;
		$this->PAN                       = null;
		$this->Expiration                = null;
		$this->IdentificationInformation = null;
		$this->EncryptionKeyId           = null;
		$this->SecurePaymentAccountData  = null;
		$this->SwipeStatus               = null;
		$this->Amount                    = null;
		$this->CurrencyCode              = 'USD';
		$this->EmployeeID                = '0000';
		$this->OrderID                   = '5-123';
		$this->ReferenceID               = null;
		$this->TipAmount                 = '0.00';
		$this->TransactionType           = '';
		$this->CurlSent                  = null;
		$this->CurlInfo                  = null;
		$this->CurlRawResponse           = null;
		$this->GatewayErrorResponse      = null;
		$this->GatewayResponse           = null;
		return true;
	}

	public function setTotal($newTotal)
	{
		$this->Amount = $newTotal;
		return true;
	}

	public function setTip($newTip)
	{
		$this->TipAmount = $newTip;
		return true;
	}

	public function setRefTxnId($newRef)
	{
		$this->ReferenceID = $newRef;
		return true;
	}

	public function performTransaction($transtype)
	{
		$this->TransactionType = $transtype;
		switch ($transtype) {
			case "Sale" :
				return $this->genericTrans("AuthorizeAndCaptureTransaction,http://schemas.evosnap.com/CWS/v2.0/Transactions/Rest");
			case "Auth":
			case "AuthForTab":
				return $this->genericTrans("AuthorizeTransaction,http://schemas.evosnap.com/CWS/v2.0/Transactions/Rest");
			case "PreAuthCapture":
			case "PreAuthCaptureForTab":
				return $this->captureTrans();
			case "Adjustment" :
				error_log(__FILE__.' This gateway does not support Adjustments');
				return false;
			case "Void":
			case "VoidPreAuthCapture":
				return $this->voidTrans();
			case "Return" :
				return $this->returnTrans();
			case "CaptureAll" :
				return $this->captureAllTrans();
				break;
			case "IssueGiftCard" :
			case "GiftCardSale" :
			case "ReloadGiftCard" :
			case "IssueLoyaltyCard" :
			case "LoyaltyCardSale" :
			case "LoyaltyCardReward" :
			case "LoyaltyCardAddValue" :
			case "VoidLoyaltyCardIssueOrReload" :
			case "VoidGiftCardIssueOrReload" :
			case "LoyaltyCardBalance" :
			case "GiftCardBalance" :
				error_log(__FILE__.' Unimplimented Trans Type '.$transtype);
				return false;
				break;
			default:
				error_log(__FILE__.' Unknown Trans Type :'.$transtype);
				return false;
				break;

		}
		return false;
	}

	private function genericTrans($type)
	{
		$toSend = array();
		$toSend['$type'] = $type;
		$toSend['ApplicationProfileId'] = $this->ApplicationProfileId;
		$toSend['MerchantProfileId'] = $this->MerchantProfileId;
		$toSend['Transaction'] = $this->getTransaction();

		if(defined ( 'JSON_PRETTY_PRINT' )){
			$toSendJSON = json_encode($toSend,JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);
		}else{
			$toSendJSON = json_encode($toSend);
			$toSendJSON = str_replace('\\/', '/', $toSendJSON);
		}

		$aResponse = json_decode($this->post('Txn/'.$this->WorkflowID,$toSendJSON),true);
		if(empty($aResponse) || isset($aResponse['ErrorId']) ){
			if($aResponse['ErrorId'] == '5005' || $aResponse['ErrorId'] == '5000'){
				$this->startSession();
				return $this->genericTrans($type);
			}
			error_log(__FILE__." ".__LINE__.' Generic Trans Error '.print_r($this->CurlInfo,1));
			error_log(__FILE__." ".__LINE__.' Generic Trans Error '.print_r($this->CurlRawResponse,1));
			error_log(__FILE__." ".__LINE__.' Generic Trans Error '.print_r($toSend,1));
			$this->GatewayErrorResponse = $aResponse;
			return false;
		}else{
			$this->GatewayResponse = $aResponse;
			return true;
		}
	}

	private function captureTrans()
	{
		$toSend['$type'] = "Capture,http://schemas.evosnap.com/CWS/v2.0/Transactions/Rest";
		$toSend['ApplicationProfileId'] = $this->ApplicationProfileId;
		$diffData = array();
		$diffData['$type'] = "BankcardCapture,http://schemas.evosnap.com/CWS/v2.0/Transactions/Bankcard";
		$diffData['TransactionId'] = $this->ReferenceID;
		$diffData['Amount'] = $this->Amount;
		$diffData['TipAmount'] = $this->TipAmount;
		$diffData['Addendum'] = null;
		$toSend['DifferenceData'] = $diffData;

		if(defined ( 'JSON_PRETTY_PRINT' )){
			$toSendJSON = json_encode($toSend,JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);
		}else{
			$toSendJSON = json_encode($toSend);
			$toSendJSON = str_replace('\\/', '/', $toSendJSON);
		}
		$aResponse = json_decode($this->put('Txn/'.$this->WorkflowID.'/'.$this->ReferenceID,$toSendJSON),true);
		if(empty($aResponse) || isset($aResponse['ErrorId']) ){
			if($aResponse['ErrorId'] == '5005' || $aResponse['ErrorId'] == '5000'){
				$this->startSession();
				return $this->captureTrans();
			}
			error_log(__FILE__." ".__LINE__.' Capture Error');
			if(isset($aResponse['ErrorId'])){
				error_log(__FILE__." ".__LINE__.' '.print_r($aResponse,1));
			}
			error_log(__FILE__." ".__LINE__." {$toSendJSON}");
			$this->GatewayErrorResponse = $aResponse;
			return false;
		}else{
			$this->GatewayResponse = $aResponse;
			return true;
		}
	}

	private function captureAllTrans()
	{
		$toSend['$type'] = "CaptureAll,http://schemas.evosnap.com/CWS/v2.0/Transactions/Rest";
		$toSend['ApplicationProfileId'] = $this->ApplicationProfileId;
		$toSend['MerchantProfileId'] = $this->MerchantProfileId;
		$diffData = array();
		$diffData['$type'] = "BankcardCapture,http://schemas.evosnap.com/CWS/v2.0/Transactions/Bankcard";
		$toSend['DifferenceData'] = $diffData;
		if(defined ( 'JSON_PRETTY_PRINT' )){
			$toSendJSON = json_encode($toSend,JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);
		}else{
			$toSendJSON = json_encode($toSend);
			$toSendJSON = str_replace('\\/', '/', $toSendJSON);
		}
		$aResponse = json_decode($this->post('Txn/'.$this->WorkflowID,$toSendJSON),true);
		if(empty($aResponse) || isset($aResponse['ErrorId']) ){
			if($aResponse['ErrorId'] == '5005' || $aResponse['ErrorId'] == '5000'){
				$this->startSession();
				return $this->captureAllTrans();
			}
			error_log(__FILE__." ".__LINE__.' Auth Error');
			$this->GatewayErrorResponse = $aResponse;
			return false;
		}else{
			$this->GatewayResponse = $aResponse;
			return true;
		}
	}

	private function voidTrans()
	{
		if(false != $this->HostedTransaction){
			return $this->evoHostedVoid();
		}
		$toSend['$type'] = "Undo,http://schemas.evosnap.com/CWS/v2.0/Transactions/Rest";
		$toSend['ApplicationProfileId'] = $this->ApplicationProfileId;
		//$toSend['MerchantProfileId'] = $this->MerchantProfileId;
		$diffData = array();
		$diffData['$type'] = "BankcardUndo,http://schemas.evosnap.com/CWS/v2.0/Transactions/Bankcard";
		$diffData['TransactionId'] = $this->ReferenceID;
		$toSend['DifferenceData'] = $diffData;

		if(defined ( 'JSON_PRETTY_PRINT' )){
			$toSendJSON = json_encode($toSend,JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);
		}else{
			$toSendJSON = json_encode($toSend);
			$toSendJSON = str_replace('\\/', '/', $toSendJSON);
		}

		$aResponse = json_decode($this->put('Txn/'.$this->WorkflowID.'/'.$this->ReferenceID,$toSendJSON),true);
		if(empty($aResponse) || isset($aResponse['ErrorId']) ){
			if($aResponse['ErrorId'] == '5005' || $aResponse['ErrorId'] == '5000'){
				$this->startSession();
				return $this->voidTrans();
			}
			error_log(__FILE__." ".__LINE__."\n".' Void Error');
			error_log(__FILE__." ".__LINE__.' Void Error '.print_r($this->CurlInfo,1));
			error_log(__FILE__." ".__LINE__.' Void Error '.print_r($this->CurlRawResponse,1));
			error_log(__FILE__." ".__LINE__.' Void Error '.print_r($toSend,1));
			$this->GatewayErrorResponse = $aResponse;
			return false;
		}else{
			$this->GatewayResponse = $aResponse;
			return true;
		}
	}

	private function returnTrans()
	{
		if(false != $this->HostedTransaction){
			return $this->evoHostedVoid();
		}
		$toSend['$type'] = "ReturnById,http://schemas.evosnap.com/CWS/v2.0/Transactions/Rest";
		$toSend['ApplicationProfileId'] = $this->ApplicationProfileId;
		$toSend['MerchantProfileId'] = $this->MerchantProfileId;
		$diffData = array();
		$diffData['$type'] = "BankcardReturn,http://schemas.evosnap.com/CWS/v2.0/Transactions/Bankcard";
		$diffData['TransactionId'] = $this->ReferenceID;
		$diffData['Amount'] = $this->Amount;
		$diffData['Addendum'] = null;
		$toSend['DifferenceData'] = $diffData;

		if(defined ( 'JSON_PRETTY_PRINT' )){
			$toSendJSON = json_encode($toSend,JSON_PRETTY_PRINT + JSON_UNESCAPED_SLASHES);
		}else{
			$toSendJSON = json_encode($toSend);
			$toSendJSON = str_replace('\\/', '/', $toSendJSON);
		}

		$aResponse = json_decode($this->post('Txn/'.$this->WorkflowID,$toSendJSON),true);
		if(empty($aResponse) || isset($aResponse['ErrorId']) ){
			if($aResponse['ErrorId'] == '5005' || $aResponse['ErrorId'] == '5000'){
				$this->startSession();
				return $this->returnTrans();
			}
			error_log(__FILE__.__LINE__.' Return Error');
			$this->GatewayErrorResponse = $aResponse;
			return false;
		}else{
			$this->GatewayResponse = $aResponse;
			return true;
		}
	}

	private function getTransaction()
	{
		$Transaction = array();
		$Transaction['$type']           = "BankcardTransaction,http://schemas.evosnap.com/CWS/v2.0/Transactions/Bankcard";
		$Transaction['TenderData']      = $this->getTenderData();
		$Transaction['TransactionData'] = $this->getTransactionData();
		return $Transaction;
	}

	private function getTenderData()
	{
		$TenderData = array();
		if($this->Keyed){
			$TenderData['CardData'] = array();
			$TenderData['CardData']['PAN']      = $this->PAN;
			$TenderData['CardData']['Expire']   = $this->Expiration;
			$TenderData['CardData']['CardType'] = $this->evosnapCardnameConversion($this->CardType);
		}
		if($this->Encrypted){
			$TenderData['EncryptionKeyId']            = $this->EncryptionKeyId;
			$TenderData['SecurePaymentAccountData']   = $this->SecurePaymentAccountData;
			$TenderData['SwipeStatus']                = $this->SwipeStatus;
			$TenderData['DeviceSerialNumber']         = $this->DeviceSerialNumber;
			$TenderData['CardData']                   = array();
			$TenderData['CardData']['PAN']            = $this->PAN;
			$TenderData['CardData']['CardType']       = $this->evosnapCardnameConversion($this->CardType);
			$TenderData['CardData']['Track2Data']     = null;
			$CardSecData                              = array();
			$CardSecData['CVData']                    = null;
			$CardSecData['CVDataProvided']            = 'NotSet';
			$CardSecData['IdentificationInformation'] = $this->IdentificationInformation;
			$TenderData['CardSecurityData']           = $CardSecData;
		}
		return $TenderData;
	}

	private function getTransactionData()
	{
		$TransactionData = array();
		$TransactionData['Amount']               = $this->Amount;
		$TransactionData['CurrencyCode']         = $this->CurrencyCode;
		$TransactionData['EmployeeId']           = $this->EmployeeID;
		$TransactionData['TransactionDateTime']  = date(DATE_ATOM);
		$TransactionData['GoodsType']            = 'PhysicalGoods';
		$TransactionData['IndustryType']         = 'Restaurant';
		$TransactionData['OrderId']              = $this->OrderID;
		$TransactionData['TipAmount']            = $this->TipAmount;
		$TransactionData['Reference']            = substr(md5(date(DATE_ATOM)),0,15);
		if($this->Keyed){
			$TransactionData['EntryMode']        = 'Keyed';
		}
		if($this->Encrypted){
			$TransactionData['EntryMode']        = 'Track2DataFromMSR';
		}
		return $TransactionData;
	}

	private function post($url,$toSend = '')
	{
		$fullURL = $this->BaseURL.$url;
		$ch = curl_init();
		if(!empty($toSend)){
			curl_setopt($ch, CURLOPT_POST      , true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $toSend);
			$this->CurlSent = $toSend;
		}
		curl_setopt($ch, CURLOPT_HEADER, false);
		$curlHeaders = array();
		$curlHeaders[] = 'Content-Type: application/json';
		$curlHeaders[] = 'Accept: ';
		$curlHeaders[] = 'Expect: 100-continue';
		$curlHeaders[] = 'Authorization: Basic '. base64_encode($this->SessionToken.":");
		//$curlHeaders[] = 'Host: '.$host;
		curl_setopt($ch, CURLOPT_HTTPHEADER    ,$curlHeaders);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if(lavuDeveloperStatus()) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }#Need to disable verifypeer for test transactions
		curl_setopt($ch, CURLOPT_URL           , $fullURL);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT       , 20);
		curl_setopt($ch, CURLOPT_SSLVERSION    , 6);
		$this->CurlRawResponse = curl_exec($ch); // Run the request
		$this->CurlInfo        = curl_getinfo($ch);
		$this->CurlHeaders     = $curlHeaders;
		curl_close($ch);
		return $this->CurlRawResponse;
	}

	private function put($url,$toSend = '')
	{
		$fullURL = $this->BaseURL.$url;
		$ch = curl_init();
		if(!empty($toSend)){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $toSend);
			$this->CurlSent = $toSend;
		}
		curl_setopt($ch, CURLOPT_HEADER, false);
		$curlHeaders = array();
		$curlHeaders[] = 'Content-Type: application/json';
		$curlHeaders[] = 'Accept: ';
		$curlHeaders[] = 'Expect: 100-continue';
		$curlHeaders[] = 'Authorization: Basic '. base64_encode($this->SessionToken.":");
		//$curlHeaders[] = 'Host: '.$host;
		curl_setopt($ch, CURLOPT_HTTPHEADER    ,$curlHeaders);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if(lavuDeveloperStatus()) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }#Need to disable verifypeer for test transactions
		curl_setopt($ch, CURLOPT_URL           , $fullURL);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
		curl_setopt($ch, CURLOPT_TIMEOUT       , 20);
		curl_setopt($ch, CURLOPT_SSLVERSION    , 6);
		$this->CurlRawResponse = curl_exec($ch); // Run the request
		$this->CurlInfo        = curl_getinfo($ch);
		$this->CurlHeaders     = $curlHeaders;
		curl_close($ch);
		return $this->CurlRawResponse;
	}

	function prettyPrint( $json )
	{
		$result = '';
		$level = 0;
		$in_quotes = false;
		$in_escape = false;
		$ends_line_level = NULL;
		$json_length = strlen( $json );

		for( $i = 0; $i < $json_length; $i++ ) {
			$char = $json[$i];
			$new_line_level = NULL;
			$post = "";
			if( $ends_line_level !== NULL ) {
				$new_line_level = $ends_line_level;
				$ends_line_level = NULL;
			}
			if ( $in_escape ) {
				$in_escape = false;
			} else if( $char === '"' ) {
				$in_quotes = !$in_quotes;
			} else if( ! $in_quotes ) {
				switch( $char ) {
					case '}': case ']':
						$level--;
						$ends_line_level = NULL;
						$new_line_level = $level;
						break;
					case '{': case '[':
						$level++;
					case ',':
						$ends_line_level = $level;
						break;
					case ':':
						$post = " ";
						break;
					case " ": case "\t": case "\n": case "\r":
						$char = "";
						$ends_line_level = $new_line_level;
						$new_line_level = NULL;
						break;
				}
			} else if ( $char === '\\' ) {
				$in_escape = true;
			}
			if( $new_line_level !== NULL ) {
				$result .= "\n".str_repeat( "\t", $new_line_level );
			}
			$result .= $char.$post;
		}
		return $result;
	}

	function evosnapCardnameConversion($cardType){
		switch ($cardType) {
			case 'AmericanExpress':
			case 'AMEX':
				return 'AmericanExpress';
				break;
			case 'Visa':
			case 'MasterCard':
			case 'Discover':
			case 'DinersClub':
			case 'JCB':
				return $cardType;
				break;
			default:
				return 'NotSet';
				break;
		}
	}

	#Additional Stuff to handle voids of Hosted Transactions

	function setHostedVoid($evoOrderID = false){
		$this->HostedTransaction = $evoOrderID ;
	}

	function evoHostedVoid(){
		$code = $this->Integration4;
		$authkey =  $this->Integration5;
		$toSend = array();
		$toSend['action'] = 'credit';
		$toSend['code'] = $code;
		$toSend['merchant_order_id'] = $this->HostedTransaction;
		$toSend['txn_id'] = $this->ReferenceID;
		$toSend['mac'] = $this->evoHostedCreateMAC($toSend,$authkey);
		$toSend['return'] = 'json';
		$rCurl = curl_init();
		curl_setopt($rCurl, CURLOPT_URL,$this->TXNURL);
		if(false === stripos(__DIR__,'/dev/')){
			curl_setopt($rCurl, CURLOPT_SSL_VERIFYPEER, false);
		}else{
			curl_setopt($rCurl, CURLOPT_SSL_VERIFYPEER, true);
		}
		curl_setopt($rCurl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($rCurl, CURLOPT_POST,true);
		curl_setopt($rCurl, CURLOPT_SSLVERSION    , 6);
		curl_setopt($rCurl, CURLOPT_POSTFIELDS,$this->evoHostedEncodeRequest($toSend) );
		$aResponse = curl_exec($rCurl);
		$aReturn = (array) json_decode($aResponse);
		curl_close($rCurl);
		if(empty($aResponse) ){
			error_log(__FILE__.__LINE__.' Hosted Void Error : No Response');
			$this->GatewayErrorResponse = '';
			return false;
		}
		if(empty($aReturn['success'])){
			error_log(__FILE__.__LINE__.' Hosted Void Error : Failure Response');
			$this->GatewayErrorResponse = $aReturn;
			return false;
		}
		$this->GatewayResponse = $aReturn;
		$this->GatewayResponse['StatusCode'] = $aReturn['success'];
		$this->GatewayResponse['StatusMessage'] = $aReturn['status'];
		$this->GatewayResponse['Status'] = ($aReturn['success'] == 1)?'Successful':'Failure';
		$this->GatewayResponse['ApprovalCode'] = $aReturn['success'];
		$this->GatewayResponse['TransactionId'] = $aReturn['txn_id'];
		return true;
		
	}

	function evoHostedCreateMAC($aParams, $sPrivateKey){
		// Order Mac Generation 'code + customer[email] + order[merchant_order_id] + order[total] + order[total_subtotal] + authkey';
		$sUnencoded = '';
		if (!empty($aParams)){
			$aTemp = array();
			foreach ($aParams as $key => $val){
				$aTemp[$key] = $val;
			}
			ksort($aTemp);
			$sUnencoded.= implode('', $aTemp);
		}
		if (!empty($sPrivateKey)){
			$sUnencoded.= $sPrivateKey;
		}
		return md5($sUnencoded);
	}

	function evoHostedEncodeRequest($aRequest){
		$aFields = array();
		if(is_array($aRequest) && !empty($aRequest)){
			foreach ($aRequest as $key => $val){
				if (is_array($val) || is_object($val))
					$aFields[] = $key.'='.urlencode(json_encode($val));
				else
					$aFields[] = $key.'='.urlencode($val);
			}
		}
		return implode('&', $aFields);
	}


    /*
     * Returns true if a transaction is still adjustable.  Returns false otherwise.
     * For a transaction to be adjustable capture_state
     * must either be ReadyForCapture, or it must be Captured AND the time of the
     * new adjustment must be before the batch closes, 3am Eastern.
     */
    public function canCaptureTransaction($fields){

        date_default_timezone_set('America/New_York');
        $batchDeadline = strtotime('today 3 a.m.');
        $txTime = $fields['server_time'];

        $txTime = rtrim($txTime, 'Z');

        $order_time = strtotime($txTime);
        $canCapture = true;

        if(($order_time < $batchDeadline) && $batchDeadline < time()) {
            $canCapture = false;
        }

        return $canCapture;
    }
}
