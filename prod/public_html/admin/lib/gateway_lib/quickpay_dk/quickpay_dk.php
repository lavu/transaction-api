<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

class QuickpayDKPayments {
	private $merchant;
	private $apikey;
	private $secret;
	private $certificationMode = false;
	private $protocol = '7';
	private $msgtype = 'authorize';
	private $ordernumber = null;
	private $language = 'en';
	private $amount  = null;
	private $currency = 'USD';
	private $continueurl = 'http://admin.poslavu.com/success.php';
	private $cancelurl = 'http://admin.poslavu.com/fail.php';
	private $callbackurl = 'http://admin.poslavu.com/callback.php';
	private $dataname;
	private $postarray;
	private $location_id;

	// Constructor
	public function __construct($merch ,$sec , $apikey) {
		$this->merchant = $merch;
		$this->secret = $sec;
		$this->apikey = $apikey;
	}
	// Setters
	public function setCertificationMode(){
		$this->certificationMode = true;
		$this->testmode = '1';
	}

	public function setContinueURL($input){
		$this->continueurl = $input;
	}
	public function setCancelURL($input){
		$this->cancelurl = $input;
	}
	public function setCallbackURL($input){
		$this->callbackurl = $input;
	}
	public function setLocationID($input){
		$this->location_id = $input;
	}

	public function setOrderNumber($input){
		if( 1 == preg_match('/^[a-zA-Z0-9]{4,20}$/',$input) ){
			$this->ordernumber = $input;
			return true;
		}else{
			error_log(__FILE__.' '.__FUNCTION__." Invalid Order Number");
			return false;
		}
	}
	public function setLanguage($input){
		if( 1 == preg_match('/^[a-z]{2}$/',$input) ){
			$this->language = $input;
			return true;
		}else{
			error_log(__FILE__.' '.__FUNCTION__." Invalid Language");
			return false;
		}
	}
	public function setAmount($input){
		if( 1 == preg_match('/^[0-9]{1,9}$/',$input) ){
			$this->amount = $input;
			return true;
		}else{
			error_log(__FILE__.' '.__FUNCTION__." Invalid Amount");
			return false;
		}
	}
	public function setCurrency($input){
		if( 1 == preg_match('/^[A-Z]{3}$/',$input) ){
			$this->currency = $input;
			return true;
		}else{
			error_log(__FILE__.' '.__FUNCTION__." Invalid Currency");
			return false;
		}
	}

	public function setCardTypeLock($input){
		if( 1 == preg_match('/^[a-zA-Z,]{0,128}$/',$input) ){
			$this->cardtypelock = $input;
			return true;
		}else{
			error_log(__FILE__.' '.__FUNCTION__." Invalid Card-type Lock");
			return false;
		}
	}
	public function setDataName($input){
		$this->dataname = $input;
		return true;
	}
	// Create Payment Button
	public function createForm($type,$submitButton = true){
		if($type == 'auth'){ $this->prepAuth(); }
		if($type == 'sale'){ $this->prepSale(); }
		if($type == 'subscription'){ $this->prepSubscribe(); }
		$ouput = '<form id="quickpayForm" action="https://secure.quickpay.dk/form/" method="post">'."\n";
		foreach ($this->postarray as $key => $value) {
			$ouput .= '<input type="hidden" name="'.$key.'" value="'.$value.'" />'."\n";
		}
		if($submitButton === true){
			$ouput .= '<input type="submit" value="Open Quickpay payment window" />'."\n";
		}
		return $ouput;
	}
	private function prepAuth(){
		$this->autocapture = 0;
		$this->msgtype = 'authorize';
		$this->prepGeneric();
	}
	private function prepSale(){
		$this->autocapture = 1;
		$this->msgtype = 'authorize';
		$this->prepGeneric();
	}
	private function prepSubscribe(){
		$this->autocapture = 1;
		$this->msgtype = 'subscribe';
		if(!isset( $this->description) || (null === $this->description) ){
			error_log(__FILE__.' '.__FUNCTION__." Error : Description Is No Optional For Subscriptions");
			echo "Error : Description Is No Optional For Subscriptions";
			return;
		}
		return $this->prepGeneric();
	}
	private function prepGeneric(){
		$output = array();
		$fields = array(
				'protocol',
				'msgtype',
				'merchant',
				'language',
				'ordernumber',
				'amount',
				'currency',
				'continueurl',
				'cancelurl',
				'callbackurl',
				'autocapture',
				'autofee',
				'cardtypelock',
				'description',
				'group',
				'testmode',
				'splitpayment',
				'forcemobile',
				'deadline',
				'cardhash'
			);
		foreach ($fields as $value) {
			if(isset( $this->$value) && (null !== $this->$value) ){
				$output["$value"] = $this->$value;
			}
		}
		$output['md5check'] = $this->generateMessageMD5($output,$this->secret);
		$output['CUSTOM_dataname'] = $this->dataname;
		$output['CUSTOM_ordernumber'] = $this->ordernumber;
		$output['CUSTOM_locid'] = $this->location_id;
		$this->postarray = $output;
		return $output;
	}
	// Check Order Status
	public function performStatusByOrder($ordernumber,$dataname = null){
		if($dataname !== null){
			$yoDawg = QuickpayDKPayments::QuickpayObjectFromDataname($dataname);
			return $yoDawg->performStatusByOrder($ordernumber);
		}
		$post['protocol'] = '7';
		$post['msgtype'] = 'status';
		$post['merchant'] = $this->merchant;
		$post['ordernumber'] = "$ordernumber";
		$post['apikey'] = $this->apikey;
		$temp = '7status'.$this->merchant.$ordernumber.$this->apikey.$this->secret;
		$post['md5check'] = hash('md5',$temp);
		return 1;
		return $this->curlRequest($post);
	}
	// Static Util Functions
	public static function curlRequest($postarray){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://secure.quickpay.dk/api');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postarray);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
	public static function generateMessageMD5($input,$secret){
		$toHash = '';
		foreach ($input as $value) {
			$toHash .= "$value";
		}
		$toHash .= $secret;
		return hash('md5',$toHash);
	}
	public static function parseQuickPayStatus($input){
		$myXML_object = @simplexml_load_string($input);
		if(empty($myXML_object) ){
			error_log(__FILE__.' '.__FUNCTION__.' Parse Failed');
			return false;
		}
		if(isset($myXML_object->state) ){
			return $myXML_object->state;
		}
		error_log(__FILE__.' '.__FUNCTION__.' No State');
		return false;
	}
}

class QuickpayCallbackHandler {
	private $request;
	private $secret;

	public function __construct($input,$secret) {
		$this->request = $input;
		$this->secret = $secret;
	}

	public function Validate(){
		if(empty($this->request)){return;}
		if(empty($this->request['CUSTOM_dataname'] )){return;}
		if(empty($this->request['md5check'] )){return;}
		return $this->md5CheckRequest();
	}

	private function md5CheckRequest(){
		$toHash = '';
		$fields = array(
				'msgtype','ordernumber','amount','currency','time','state','qpstat',
				'qpstatmsg','chstat','chstatmsg','merchant','merchantemail','transaction',
				'cardtype','cardnumber','cardhash','cardexpire','acquirer','splitpayment',
				'fraudprobability','fraudremarks','fraudreport','fee'
			);
		foreach ($fields as $value) {
			if(isset($this->request["$value"]) && (null !== $this->request["$value"]) ){
				$toHash .= $this->request["$value"];
			}
		}
		$toHash .= $this->getSecret();
		$test = hash('md5',$toHash);
		if($test == $this->request['md5check']){
			return true;
		}
		return false;
	}

	public function Process(){
		error_log(__FILE__.' '.__FUNCTION__);
		return true;
	}

	private static function translateStatusCode($input){
		$messages[] = array();
		$messages['000'] = "Approved.";
		$messages['001'] = "Rejected by acquirer. See field 'chstat' and 'chstatmsg' for further explanation.";
		$messages['002'] = "Communication error.";
		$messages['003'] = "Card expired.";
		$messages['004'] = "Transition is not allowed for transaction current state.";
		$messages['005'] = "Authorization is expired.";
		$messages['006'] = "Error reported by acquirer.";
		$messages['007'] = "Error reported by QuickPay.";
		$messages['008'] = "Error in request data.";
		$messages['009'] = "Payment aborted by shopper.";
		if(array_key_exists("$input",$messages) ) {
			return $messages["$input"];
		}
		return "Unknown Status Code $input";
	}

	public static function translateTransactionState($code){
		$states[] = array();
		$states['0'] = 'Initial'; // Transaction started, but not completed. Eg. the customer may have left the payment window without completing the payment.
		$states['1'] = 'Authorized'; // Transaction authorized by acquirer and can be captured. The customer has completed the payment in the payment window.
		$states['3'] = 'Captured'; // You have captured the money. You have asked the acquirer to transfer the money from the customers bank account to yours.
		$states['5'] = 'Cancelled'; // Transaction cancelled. You have cancelled the transaction, eg. if you or the customer does not want to complete the order.
		$states['7'] = 'Refunded'; // Transaction refunded. You have returned money to the customers bank account, eg. you or the customer has cancelled a part of or the entire order.
		$states['9'] = 'Subscribed'; // Transaction is a subscription. You can make several recurring payments from the same customer, eg. a subscription for a magazine or music download.
		if(array_key_exists("$code",$states) ) {
			return $states["$code"];
		}
		return "Unknown Status Code $code";
	}

	private function getSecret(){
		return $this->secret;
	}
}

function quickpayPayment($dataname,$order_id, $payment, $location_id,$order_info,$integration_data){
	
	$payment = new QuickpayDKPayments(
			$integration_data['integration1'],
			$integration_data['integration2'],
			$integration_data['integration3']
		);
	$callbackURL = LavuHostName().'/lib/gateway_lib/hosted_checkout/quickpay_callback.php';
	if(lavuDeveloperStatus()){
		$payment->setCertificationMode();
	}
	$payment->setOrderNumber( str_replace('-','z',$order_id) );
	$payment->setAmount( SmallestUnitCurrency($dataname,$order_info['total'],$location_id) );
	$payment->setDataName( $dataname );
	$goodURL = 'https://www.lavutogo.com'.$_POST['return_url'].'?return=success';
	$badURL = 'https://www.lavutogo.com'.$_POST['return_url'].'?return=failure';
	$payment->setContinueURL($goodURL);
	$payment->setCancelURL($badURL);
	$payment->setCallbackURL($callbackURL);
	$payment->setLocationID($location_id);
	echo $payment->createForm('sale',false)."\n";
	echo "<script>document.getElementById('quickpayForm').submit();</script>";
	return;
}
