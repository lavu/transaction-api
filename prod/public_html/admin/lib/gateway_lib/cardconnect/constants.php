<?php
namespace PaymentGateways\CardConnect;

define(__NAMESPACE__ . "\UNKNOWN_ACTION_ERROR", "Unknown Action");
define(__NAMESPACE__ . "\UNKNOWN_ERROR", "Unknown Error");
define(__NAMESPACE__ . "\UNABLE_TO_PROCESS_TRANSACTION_ERROR", "Unable to process transaction. Unknown Error");
define(__NAMESPACE__ . "\NOTHING_TO_BATCH_ERROR", "Found no transactions to batch");
define(__NAMESPACE__ . "\TXN_DECLINED_ERROR", "(PAPI:LP-100) Transaction Declined.");
define(__NAMESPACE__ . "\CARD_READ_FAILED_ERROR", "(LAVU:LP-120) Card read failed.\nPlease try again.");
define(__NAMESPACE__ . "\CARDCONNECT_GATEWAY_NAME", "cardconnect");
define(__NAMESPACE__ . "\FREEDOMPAY_GATEWAY_NAME", "freedompay");
define(__NAMESPACE__ . "\LAVUPAY_GATEWAY_NAME", "lavupay");
define(__NAMESPACE__ . "\REQUEST_TIME_OUT", "Request timed out");
define(__NAMESPACE__ . "\CONNECTION_TIME_OUT", "Connection timed out");
define(__NAMESPACE__ . "\REQUEST_TIMEOUT_ERRORCODE", 408);
define(__NAMESPACE__ . "\CONNECTION_TIMEOUT_ERRORCODE", 522);
