<?php
namespace PaymentGateways\CardConnect;

use PaymentGateways\CardConnect\CardInput\NetworkReader;

require_once(dirname(__FILE__) . "/payment.php");

// http://dev-papi.devposlavu.com/develop/#api-Payments-PostPaymentWithDevice
class NetworkDevicePayment extends Payment
{
    /**
     * @param array $request
     */
    public function __construct(NetworkReader $input)
    {
        parent::__construct($input);

        // need to determine where does the device id come from
        $this->resource = "payments/devices/:id";
    }
}