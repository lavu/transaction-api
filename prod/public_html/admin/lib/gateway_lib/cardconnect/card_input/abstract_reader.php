<?php
namespace PaymentGateways\CardConnect\CardInput;

use getCardTypeFromNumber;

require_once(dirname(__FILE__) . "/card_input_interface.php");
require_once(dirname(__FILE__) . "/helpers.php");

require_once(dirname(__FILE__) . "/../../../gateway_functions.php");

// http://dev-papi.devposlavu.com/develop/#api-Payments-PostPayment
abstract class AbstractReader
{
    public function cardNumber($display_full_number=false)
    {
        $number = $this->getRawRequest()["card_number"];
        return $display_full_number ? $number : substr($number, -4);
    }

    public function cardBrand()
    {
        return getCardTypeFromNumber($this->cardNumber(true));
    }

    public function cardExpirationDate()
    {
        $month = $this->cardExpirationMonth();
        $year  = $this->cardExpirationYear();
        return "$month/$year";
    }

    public function cardExpirationMonth()
    {
        $request = $this->getRawRequest();
        return isset($request["exp_month"]) ? $request['exp_month'] : null;
    }

    public function cardExpirationYear()
    {
        return $this->getRawRequest()["exp_year"];
    }

    public function dataname()
    {
        return $this->getRawRequest()["data_name"];
    }

    public function getAdditionalTransactionInformation()
    {
        return $this->getRawRequest()["more_info"];
    }

    // Payment Gateway transaction ID
    public function getPNREF()
    {
        return $this->getRawRequest()["pnref"];
    }

    public function getRefundAmount($asPennies = false)
    {
        return $this->getTotalAmount($asPennies);
    }

    /**
     * Return intended amount to be voided
     * @param bool $asPennies
     * @return float|int
     */
    public function getVoidAmount($asPennies = false)
    {
        $request = $this->getRawRequest();
        $amountToVoid = $request['card_amount'];
        return $asPennies ? amountToPennies($amountToVoid) : $amountToVoid;
    }

    public function getServer()
    {
        return array(
            "id" => $this->getRawRequest()["server_id"],
            "name" => $this->getRawRequest()["server_name"],
            "fullName" => $this->getRawRequest()["server_full_name"]
        );
    }
    /**
     * Return the tip amount of the payment.
     * @param bool $asPennies
     * @return float|int
     */
    public function getTipAmount($asPennies=false)
    {
        $amount = $this->getRawRequest()["tip_amount"];
        return $asPennies ? amountToPennies($amount) : $amount;
    }

    /**
     * Return the total amount of the payment, base + tip.
     * @param bool $asPennies
     * @return float|int
     */
    public function getTotalAmount($asPennies=false)
    {
        $request = $this->getRawRequest();
        $amount  = empty($request["card_amount"]) ? $request["base_amount"] : $request["card_amount"];
        if (empty($amount)) {
            $amount = $request["amount"];
        }
        $tip    = $this->getTipAmount();
        $total  = $amount + $tip;
        return $asPennies ? amountToPennies($total) : $total;
    }

    public function isValid()
    {
        return $this->isCardReadSuccessful();
    }

    public function locationId()
    {
        return $this->getRawRequest()["loc_id"];
    }

    public function orderId()
    {
        return $this->getRawRequest()["order_id"];
    }

    public function transactionType()
    {
        if (isset($this->getRawRequest()["transtype"])) {
            return strtoupper($this->getRawRequest()["transtype"]);
        }

        return "";
    }
}