<?php
namespace PaymentGateways\CardConnect\CardInput;

interface ICardInput
{
    public function authorizationFields();
    public function captureFields();
    public function isValid();
    public function getRawLocationInfo();
    public function getRawRequest();
    public function gatewayFields();
}