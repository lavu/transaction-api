<?php
namespace PaymentGateways\CardConnect\CardInput;

require_once(dirname(__FILE__) . "/card_input_interface.php");
require_once(dirname(__FILE__) . "/abstract_reader.php");
require_once(dirname(__FILE__) . "/helpers.php");

// http://dev-papi.devposlavu.com/develop/#api-Payments-PostPayment
class NetworkReader extends AbstractReader implements ICardInput
{
    private $location_info;
    private $request;

    /**
     * @param array $request
     */
    public function __construct(array $request, array $location_info)
    {
        $this->location_info= $location_info;
        $this->request  = $request;
    }

    public function authorizationFields($amount)
    {
        return array(
            "amount"        => $this->getTotalAmount(true),
            "capture"       => false
        );
    }

    public function captureFields($amount)
    {
        return array(
            "amount"        => $this->getTotalAmount(true),
            "capture"       => true
        );
    }

    public function getRawLocationInfo()
    {
        return $this->location_info;
    }

    public function getRawRequest()
    {
        return $this->request;
    }

    public function isValid()
    {
        return true;
    }
}