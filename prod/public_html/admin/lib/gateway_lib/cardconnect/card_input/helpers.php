<?php

namespace PaymentGateways\CardConnect\CardInput;

function amountToPennies($amount)
{
    return $amount * 100;
}