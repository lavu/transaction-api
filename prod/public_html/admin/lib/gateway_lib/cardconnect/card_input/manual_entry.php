<?php
namespace PaymentGateways\CardConnect\CardInput;

require_once(dirname(__FILE__) . "/card_input_interface.php");
require_once(dirname(__FILE__) . "/abstract_reader.php");
require_once(dirname(__FILE__) . "/helpers.php");

// http://dev-papi.devposlavu.com/develop/#api-Payments-PostPayment
class ManualEntry extends AbstractReader implements ICardInput
{
    private $location_info;
    private $request;
    private $gateway;

    /**
     * @param array $request
     */
    public function __construct(array $request, array $location_info, array $gateway)
    {
        $this->location_info= $location_info;
        $this->request      = $request;
        $this->gateway      = $gateway;
    }

    /**
     * This will suppress UnusedLocalVariable
     * warnings in this method
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function authorizationFields()
    {
        return array(
            "idempotentId"  => $this->request["idempotent_id"],
            "amount"        => $this->getTotalAmount(true),
            "currency"      => "USD",
            "gateway"       => $this->gatewayFields(),
            "card"          => $this->cardInformation(),
            "capture"       => false
        );
    }

    /**
     * This will suppress UnusedLocalVariable
     * warnings in this method
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function captureFields()
    {
        return array(
            "idempotentId"  => $this->request["idempotent_id"],
            "amount"        => $this->getTotalAmount(true),
            "currency"      => "USD",
            "gateway"       => $this->gatewayFields(),
            "card"          => $this->cardInformation(),
            "capture"       => true
        );
    }

    public function deviceInformation()
    {
        return array(
            "id"            => "",
            "manufacturer"  => "",
            "model"         => "device model",
            "version"       => "device firmware version"
        );
    }

    /**
     * Return associative array of current card information with
     * - number
     * - expMonth
     * - expYear
     * Only returns keys which are present
     * @return array
     */
    public function cardInformation()
    {
        $cardInformation = array();
        $cardNumber      = $this->cardNumber(true);
        if (!empty($cardNumber)) {
            $cardInformation["number"] = $cardNumber;
        }
        if (isset($this->request['payment_key']) && !empty($this->request['payment_key'])) {
            $cardInformation['number'] = $this->request['payment_key'];
        }
        $cardExpirationMonth = $this->cardExpirationMonth();
        if (!empty($cardExpirationMonth)) {
            $cardInformation["expMonth"] = $cardExpirationMonth;
        }
        $cardExpirationYear = $this->cardExpirationYear();
        if (!empty($cardExpirationYear)) {
            $cardInformation["expYear"] = $cardExpirationYear;
        }

        return $cardInformation;
    }

    public function getRawLocationInfo()
    {
        return $this->location_info;
    }

    public function getRawRequest()
    {
        return $this->request;
    }

    public function isValid()
    {
        return true;
    }

    public function gatewayFields()
    {
        return $this->gateway;
    }
}