<?php
namespace PaymentGateways\CardConnect\CardInput;

require_once(dirname(__FILE__) . "/card_input_interface.php");
require_once(dirname(__FILE__) . "/abstract_reader.php");
require_once(dirname(__FILE__) . "/helpers.php");

// http://dev-papi.devposlavu.com/develop/#api-Payments-PostPayment
class IDynamoReader extends AbstractReader  implements ICardInput
{
    private $location_info;
    private $request;
    private $gateway;

    /**
     * @param array $request
     */
    public function __construct(array $request, array $location_info, array $gateway)
    {
        $this->location_info= $location_info;
        $this->request      = $request;
        $this->gateway      = $gateway;
    }

    public function authorizationFields()
    {
        return array(
            "idempotentId"  => $this->request["idempotent_id"],
            "amount"        => $this->getTotalAmount(true),
            "currency"      => "USD",
            "gateway"       => $this->gatewayFields(),
            "capture"       => false
        );
    }

    public function captureFields()
    {
        return array(
            "idempotentId"  => $this->request["idempotent_id"],
            "amount"        => $this->getTotalAmount(true),
            "currency"      => "USD",
            "gateway"       => $this->gatewayFields(),
            "capture"       => true
        );
    }

    public function getRawLocationInfo()
    {
        return $this->location_info;
    }

    public function getRawRequest()
    {
        return $this->request;
    }

    public function isValid()
    {
        return $this->transactionType() == "PREAUTHCAPTUREFORTAB" || $this->isCardReadSuccessful();
    }

    public function gatewayFields()
    {
        $gatewayDataToMerge = array(
            "data" => array(
                "track" => $this->request["mag_data"]
            )
        );
        return array_merge_recursive($this->gateway, $gatewayDataToMerge);
    }

    // algorithm copied from prod/public_html/admin/lib/gateway_lib/tgate_func.php
    private function isCardReadSuccessful()
    {
        $mag_data    = $this->request["mag_data"];
        $pay_type_id = $this->request["set_pay_type_id"];
        $is_mag_data_valid = (substr($mag_data, 0, 2) == "%B" ||
                                ((substr($mag_data, 0, 1) == ";" || substr($mag_data, 0, 3) == "%E?" ) && $pay_type_id!="2"));

        $split_mag_data = explode("|", $mag_data);
        $track1good = !empty($split_mag_data[2]) && !strstr($split_mag_data[2], "E?");
        $track2good = !empty($split_mag_data[3]) && !strstr($split_mag_data[3], "E?");

        return $is_mag_data_valid || ($track1good && $track2good);
    }
}