<?php
namespace PaymentGateways\CardConnect;
use Lavu\Location\Config;

use Exception;
require_once(dirname(__FILE__) . "/api_client.php");
require_once(dirname(__FILE__) . "/gateway.php");
require_once(dirname(__FILE__) . "/../../Lavu/Locations/Config.php");


class TenantAccount extends APIClient
{
    private $papiId;
    public $name;
    public $createdAt;
    public $updatedAt;
    public $gateways;
    public $resources = "tenant-accounts/";

    /**
     * Suppress StaticAccess warnings in
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function __construct($dataname, $locationId)
    {
        parent::__construct($dataname, $locationId);

        $this->papiId = TenantAccount::getPapiId();
        $this->resource = $this->resources . $this->papiId;
        $account = $this->fetchTenantAccount();
        if ($account) {
            $this->name = $account->tenantName;
            $this->createdAt = $account->createdAt;
            $this->updatedAt = $account->createdAt;
            $this->gateways = array_map(function (& $gateway) {
                return new Gateway($gateway);
            }, $this->arrayWrap($account->locationPaymentGateways));
        }
    }

    private function fetchTenantAccount()
    {
        $response = $this->execute();
        return $response->getData();
    }

    public function defaultGateway()
    {
        if ($this->gateways) {
            $gateways = $this->arrayWrap($this->gateways);
            $gatewayCount = sizeof($gateways);
            for ($i = 0; $i < $gatewayCount; $i++) {
                if ($gateways[$i]->isDefault) {
                    return $gateways[$i];
                }
            }
        }
        return null;
    }

    public function cardConnectGateway()
    {
        if ($this->gateways) {
            $gateways = $this->arrayWrap($this->gateways);
            $gatewayCount = sizeof($gateways);
            for ($i = 0; $i < $gatewayCount; $i++) {
                if ($gateways[$i]->gatewayName == CARDCONNECT_GATEWAY_NAME) {
                    return $gateways[$i];
                }
            }
        }
        return null;
    }

    public function arrayWrap($arr)
    {
        if (!is_array($arr)) {
           $arr = array($arr);
        }
        return $arr;
    }

    public static function getPapiId()
    {
        $papiId = null;
        $tenant = Config::query()
            ->where('setting', '=', 'papi_account_uuid')
            ->where('type', '=', 'location_config_setting')
            ->first();
        if (isset($tenant)) {
            $papiId = $tenant->value;
        }
        return $papiId;
    }

}