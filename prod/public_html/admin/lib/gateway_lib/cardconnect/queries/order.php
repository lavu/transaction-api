<?php
namespace PaymentGateways\CardConnect\Queries;

class Order
{
    public static function updateVoidedOrder($locationId, $orderId, $amount)
    {
        if (empty($orderId)) {
            throw new Exception("Unable to void; order ID missing.");
        }

        $query = "UPDATE `orders` SET `card_paid` = (`card_paid` - " . $amount .
                 "), `card_desc` = '', `transaction_id` = '' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]';";
        $args = array(
            "loc_id" => $locationId,
            "order_id" => $orderId
        );
        return lavu_query($query, $args);
    }
}