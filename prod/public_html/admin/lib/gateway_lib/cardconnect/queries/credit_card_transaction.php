<?php
namespace PaymentGateways\CardConnect\Queries;

class CreditCardTransaction
{

const GET_TXN_STATEMENT = <<<'EOD'
    SELECT `id`, `transaction_id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`, `tip_amount`,`first_four`, `card_desc`
    FROM `cc_transactions`
    WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]'
            AND `transaction_id` = '[pnref]' AND `voided` != '1'
            AND (`action` = 'Sale' OR `action` = 'Refund');
EOD;

    const GET_TXN_BY_ID_STATEMENT = <<<'EOD'
    SELECT `id`, `transaction_id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`, `tip_amount`,
           `first_four`, `card_desc`, `last_mod_ts`
    FROM `cc_transactions`
    WHERE `id` = '[id]';
EOD;

const VOID_TXN_STATEMENT = <<<'EOD'
    UPDATE `cc_transactions`
    SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = 'VOIDED', `last_mod_ts` = '[3]'
    WHERE `id` = '[4]';
EOD;

    public static function get($locationId, $orderId, $transactionId)
    {
        $args = array(
            "loc_id" => $locationId,
            "order_id" => $orderId,
            "pnref" => $transactionId
        );

        return mysqli_fetch_assoc(lavu_query(self::GET_TXN_STATEMENT, $args));
    }

    /**
     * Return cc_transaction row with given id
     *
     * @param $transactionId
     * @return array|null - db row with transaction info
     */
    public static function getById($transactionId)
    {
        $args = array("id" => $transactionId);

        return mysqli_fetch_assoc(lavu_query(self::GET_TXN_BY_ID_STATEMENT, $args));
    }

    public static function void($transactionId, $voidReason, $voidedById)
    {
        if (empty($transactionId)) {
            throw new Exception("Unable to void; transaction ID missing.");
        }

        return lavu_query(self::VOID_TXN_STATEMENT, $voidReason, $voidedById, time(), $transactionId);
    }
}