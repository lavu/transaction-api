<?php
namespace PaymentGateways\CardConnect\Queries;

class Check
{
    public static function updateVoidedCheck($locationId, $orderId, $check, $amount)
    {
        if (empty($orderId)) {
            throw new Exception("Unable to void; order ID missing.");
        }

        $query = "UPDATE `split_check_details` SET `card` = (`card` - " . $amount .
                 ") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]';";

        return lavu_query($query, $locationId, $orderId, $check);
    }
}