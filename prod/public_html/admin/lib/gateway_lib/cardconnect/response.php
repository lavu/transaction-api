<?php
namespace PaymentGateways\CardConnect;

require_once(dirname(__FILE__) . "/constants.php");

class Response extends APIClient
{
    private $response;

    // Regex for HTTP Status 2xx
    // https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.2
    const HTTP_STATUS_SUCCESSFUL = "/2[02][0-9]/";

    /**
     * @param string $response
     */
    public function __construct($response, $httpstatus)
    {
        $this->response = json_decode($response);
        $this->status = $httpstatus;
    }

    public function isSuccessful()
    {
        return preg_match_all(self::HTTP_STATUS_SUCCESSFUL, $this->status);
    }

    public function getData()
    {
        return $this->response;
    }

    public function getErrorMessage()
    {
        $message = UNKNOWN_ERROR;
        if (\property_exists($this->response, "errorMessage")) {
            $message = $this->response->errorMessage;
        } else if (\property_exists($this->response, "message")) {
            $message = $this->response->message;
        }

        return $message;
    }

    public function pnref()
    {
        return $this->response->id;
    }

    public function statusCode()
    {
        return $this->response->status;
    }

    public function transactionId()
    {
        return $this->response->id;
    }
}