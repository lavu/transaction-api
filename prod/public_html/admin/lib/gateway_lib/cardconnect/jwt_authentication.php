<?php
namespace PaymentGateways\CardConnect;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/../../../cp/resources/core_functions.php");

use \Firebase\JWT\JWT;

class JWTAuthentication {
    public $tokenId;

    private $secretKey;
    private $alg = 'HS256'; //encryption algorithm
    private $encryptionKey;
    private $encryptionKeyAlg = 'aes-256-cbc';
    private $dataName;
    private $locationId;

    public function __construct($args){
        $this->secretKey = getenv('PAPI_SECRET');
        $this->dataName = $args['dataname'];
        $this->locationId = $args['locationid'];
        $this->tokenId = $this->dataName.'_'.$this->locationId;
        $this->encryptionKey = getenv('ENCRYPTION_KEY');
    }

    /**
     * Suppress StaticAccess warnings in
     * this function.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function generatePapiAuthToken(){
        $jwtData = array("userId" => $this->tokenId);
        $authToken = JWT::encode($jwtData, $this->secretKey, $this->alg);
        return $authToken;
    }

    public function genEncryptedDataname() {
        //Return hardcoded encrypted dataname, because what crypt logic written in node.js
        // is not supported bt php5.6. After discussion with Felipe will change it.
        return '445a1636ad49a4bbf64716d1f502bd36:01fa2df5eaa9411e54161eb9e071f9bf';
        /*$ivlen = openssl_cipher_iv_length($this->encryptionKeyAlg);
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($this->dataName, $this->encryptionKeyAlg, $this->encryptionKey, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $this->encryptionKey, $as_binary = true);
        $encryptedDataname = base64_encode( $iv.$hmac.$ciphertext_raw );
        return $encryptedDataname;*/
    }

    /**
     * Suppress some warnings in until we figure
     * out if this method is even necessary.
     *
     * @SuppressWarnings(PHPMD.ShortVariable)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function genDecryptDataname($ciphertext) {
        $cypher = base64_decode($ciphertext);
        $ivlen = openssl_cipher_iv_length($this->encryptionKeyAlg);
        $iv = substr($cypher, 0, $ivlen);
        $hmac = substr($cypher, $ivlen, $sha2len = 32);
        $ciphertext_raw = substr($cypher, $ivlen + $sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $this->encryptionKeyAlg, $this->encryptionKey, $options = OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $this->encryptionKey, $as_binary = true);

        if (hash_equals($hmac, $calcmac)) {
            return $original_plaintext;
        } else {
            return '';
        }
    }
}