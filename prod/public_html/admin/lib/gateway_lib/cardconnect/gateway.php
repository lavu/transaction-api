<?php
namespace PaymentGateways\CardConnect;



class Gateway {

    public function __construct($params) {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Return associative array with the specific fields that the
     * given PAPI-supported gateway requires.
     * @param array $request
     * @return array
     */
    public function getPapiFields(array $request)
    {
        if ($this->gatewayName === CARDCONNECT_GATEWAY_NAME) {
            return array(
                    "mid"  => $this->gatewayAccountId,
                    "auth" => $this->gatewayAuthToken,
                   );
        }
        if ($this->gatewayName === FREEDOMPAY_GATEWAY_NAME) {
            return array(
                    "esKey"      => getenv('FREEDOMPAY_ESKEY'),
                    "sessionKey" => $request['session_key'],
                    "storeId"    => $this->gatewayStoreId,
                    "terminalId" => $this->gatewayTerminalId,
                   );
        }
    }

    /**
     * Casts self as an array and calls Gateway::normalizeKeys on each key
     *
     * @return array
     * Suppress StaticAccess warnings in
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function asNormalizedArray() {
        $thisAsArray     = (array) $this;
        $normalizedArray = array();
        array_walk( $thisAsArray, function ($value, $key) use (&$normalizedArray) {
            $normalizedArray[lcfirst(Gateway::normalizeKey($key))] = $value;
        });
        return $normalizedArray;
    }

    /**
     * Reformats keys by removing the 'gateway' portion of the key name
     *
     * @param string     $key
     *
     * @return string
     */
    public static function normalizeKey($key) {
        $prefix = 'gateway';
        return str_replace($prefix, "", $key);
    }
}
