<?php
namespace PaymentGateways\CardConnect;
use Lavu\CircuitBreaker\CircuitBreaker;
use Exception;

require_once(dirname(__FILE__) . "/constants.php");
require_once(dirname(__FILE__) . "/jwt_authentication.php");
require_once(dirname(__FILE__) . "/response.php");
require_once(dirname(__FILE__) . "/../../Lavu/circuit_breaker/circuit_breaker.php");
class APIClient {
    private $token;
    private $key;
    private $baseURL;
    protected $resource;
    protected $fields;

    public function __construct($dataname, $locationid)
    {
        $this->baseURL = getenv('PAPI_API') . "/api";
        $papi_authentication = new JWTAuthentication(array("dataname" => $dataname, "locationid" => $locationid));
        $this->token = $papi_authentication->generatePapiAuthToken();
        $this->key   = $papi_authentication->genEncryptedDataname();
    }

    /**
     * Executes HTTP request
     *
     * @param string        $method     HTTP method to use, defaults to `GET`
     * @param mixed|null    $payload    the data to be sent as the request body
     * @param string|null   $resource   endpoint that will be used for the request
     * @param callable|null $onError    Closure to call when the HTTP status is not
     *                                  successful
     *
     * @throws Exception if request is not successful (i.e. no 2xx HTTP status)
     *
     * @return PaymentGateways\CardConnect\Response
     */
    protected function execute($method="GET", $payload=null, $resource=null, \Closure $onError=null)
    {
        $circuitBreaker = new CircuitBreaker("LavuPay");

        if (!$resource) {
            $resource = $this->resource;
        }
        $papiURL = $this->baseURL . "/" . $resource;
        $pch = curl_init();
		if ($papiURL == "") {
            return "";
        }

        curl_setopt($pch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($pch, CURLOPT_URL, $papiURL);
		if (lavuDeveloperStatus()) {
			curl_setopt($pch, CURLOPT_SSL_VERIFYPEER, false);
        }

        if ($payload) {
            curl_setopt($pch, CURLOPT_POSTFIELDS, $payload);
        }
        curl_setopt($pch, CURLOPT_ENCODING, "");
        curl_setopt($pch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($pch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($pch, CURLOPT_HTTPHEADER, $this->requestHeaders());
        #check circut is closed or open ?
        if ($circuitBreaker->isCircuitClosed()) {
            $response = new Response(curl_exec($pch), curl_getinfo($pch, CURLINFO_HTTP_CODE));
            curl_close($pch);
            if ($response->isSuccessful()) {
                $circuitBreaker->serviceSuccess();   #close circuit breaker
                return $response;
            } else {
                $httpstatus = curl_getinfo($pch, CURLINFO_HTTP_CODE);
                #check error code for request timeout or connection timeout
                if ($httpstatus == REQUEST_TIMEOUT_ERRORCODE || $httpstatus == CONNECTION_TIMEOUT_ERRORCODE) {
                    $circuitBreaker->serviceFailed(); #open the circuit breaker
                }
                if ($onError instanceof \Closure) {
                    return $onError($response);
                } else {
                    throw new Exception($this->processErrorResponse($response));
                }
            }
        } else {
            throw new Exception(UNABLE_TO_PROCESS_TRANSACTION_ERROR);
        }
    }

    private function requestHeaders()
    {
        return array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Accept-Language: en_US',
            'Authorization: ' . $this->token
        );
    }

    private function processErrorResponse($error)
    {
        $flatten_error = function($value) {
            return $value->param . ": " . $value->msg;
        };

        if (property_exists($error, "errors") &&
            property_exists($error->errors, "_error") &&
            property_exists($error->errors->_error, "nestedErrors")) {
            $error_response = implode("\n", array_map($flatten_error, $error->errors->_error->nestedErrors));
        } else if (method_exists($error, "getErrorMessage")) {
            $error_response = $error->getErrorMessage();
        } else {
            $error_response = UNABLE_TO_PROCESS_TRANSACTION_ERROR;
        }

        return $error_response;
    }
}
