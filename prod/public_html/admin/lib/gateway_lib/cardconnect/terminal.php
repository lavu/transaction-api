<?php
namespace PaymentGateways\CardConnect;

use Exception;
use Illuminate\Support\Collection;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/api_client.php");
require_once(dirname(__FILE__) . "/tenant_account.php");
require_once(dirname(__FILE__) . "/response.php");

class Terminal extends APIClient
{
    private $papiAccountId;
    private $dataname;
    private $locationId;
    public $_tenantAccount;

    /**
     * Suppress StaticAccess warnings in
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function __construct($dataname, $locationId)
    {
        parent::__construct($dataname, $locationId);
        $this->dataname = $dataname;
        $this->locationId = $locationId;
        $this->papiAccountId = TenantAccount::getPapiId();
        $this->tenantAccountResource = "tenant-accounts/" . $this->papiAccountId;
        $this->resource = $this->tenantAccountResource . "/terminals";
    }

    public function tenantAccount() {
        if (!$this->_tenantAccount) {
            $this->_tenantAccount = new TenantAccount($this->dataname, $this->locationId);
        }
        return $this->_tenantAccount;
    }

    /**
     * Cancel Bolt terminal session
     *
     * @param string $terminalId Terminal's unique identifier
     *
     * @return stdClass|null
     */
    public function cancel($terminalId)
    {
        try {
            $this->validatePapiAccountActive();
            $account = $this->tenantAccount();
            $resource = "terminals" . "/" . $terminalId . "/cancelOperations";
            $gatewayName = null;
            $gatewayAccountId = null;
            $cardConnectGateway = $account->cardConnectGateway();
            if ($cardConnectGateway) {
                $gatewayName = $cardConnectGateway->gatewayName;
                $gatewayAccountId = $cardConnectGateway->gatewayAccountId;
            }
            $payload = array(
                "gateway" => array(
                    "type" => $gatewayName,
                    "data" => array(
                        "mid" => $gatewayAccountId,
                        "boltAuth" => getenv("BOLT_API_KEY")
                    )
                )
            );
            return $this->execute("POST", json_encode($payload), $resource);
        } catch (Exception $err) {
            error_log($err);
            throw $err;
        }
    }

    /**
     * Establish a _connection/session_ with a Bolt Terminal
     * 
     * @param string $terminalId Terminal's unique identifier
     * 
     * @return stdClass|null Payment API response
     */
    public function connect($terminalId)
    {
        try {
            $this->validatePapiAccountActive();
            $this->resource = $this->resource . "/" . $terminalId . "/connect";
            $response = $this->execute("POST");
            return $response->getData();
        } catch (Exception $err) {
            error_log($err);
            throw $err;
        }
    }

    /**
     * List Terminals associated with tenant
     * 
     * @param string $terminalId filter by terminalId
     * @param string $deviceId fitler by deviceId
     *
     * @return array List of Terminals
     */
    public function listTerminals($terminalId=null, $deviceId=null)
    {
        // supported filters
        $filters = collect([
            'terminalId' => $terminalId,
            'pairedDevice' => $deviceId
        ]);

        try {
            $this->validatePapiAccountActive();

            // select only filters that have a value
            $filterBy = $filters->filter(function($value) {
                return isset($value);
            });

            if (!empty($filterBy)) {
                // construct query string with selected filters
                $querystring = http_build_query($filterBy->all());
                $response = $this->execute('GET', null, $this->resource . "?$querystring");
            } else {
                $response = $this->execute();
            }
            return $response->getData();
        } catch (Exception $err) {
            error_log($err);
            throw $err;
        }
    }

    /**
     * Pair a device to a terminal
     * 
     * @param string $terminalId Terminal's unique identifier
     * @param string $deviceId Device's unique identifier
     *
     * @throws Exception
     *
     * @return stdClass|null Payment API response
     */
    public function pair($terminalId, $deviceId)
    {
        try {
            $this->validatePapiAccountActive();
            $this->resource = $this->resource . "/" . $terminalId . "/pair";
            $payload = array("deviceId" => $deviceId);

            // use custom handler to handle exception to
            // return specific error object to client.
            $response = $this->execute("POST", json_encode($payload), null, function($response) {
                $data = $response->getData();
                $err = [
                    'code' => $data->errorCode,
                    'error' => $data->errorMessage
                ];

                return new Response(json_encode($err), $response->status);
            });

            return $response->getData();
        } catch (Exception $err) {
            error_log($err);
            throw $err;
        }
    }

    /**
     * Unpair a device from a terminal
     * 
     * @param string $terminalId Terminal's unique identifier
     *
     * @return stdClass|null Payment API response
     */
    public function unpair($terminalId)
    {
        try {
            $this->validatePapiAccountActive();
            $this->resource = $this->resource . "/" . $terminalId . "/unpair";
            $response = $this->execute("POST");
            return $response->getData();
        } catch (Exception $err) {
            error_log($err);
            throw $err;
        }
    }

    public function read($terminalId, $amount)
    {
        try {
            $this->validatePapiAccountActive();
            $account = $this->tenantAccount();
            $resource = "terminals" . "/" . $terminalId . "/card";
            $gatewayName = null;
            $gatewayAccountId = null;
            $cardConnectGateway = $account->cardConnectGateway();
            if ($cardConnectGateway) {
                $gatewayName = $cardConnectGateway->gatewayName;
                $gatewayAccountId = $cardConnectGateway->gatewayAccountId;
            }
            $payload = array(
                "amount"=> $amount,
                "gateway" => array(
                    "type" => $gatewayName,
                    "data" => array(
                        "mid" => $gatewayAccountId,
                        "boltAuth" => getenv("BOLT_API_KEY")
                    )
                )
            );
            $response = $this->execute("POST", json_encode($payload), $resource);
            return $response->getData();
        } catch (Exception $err) {
            error_log($err);
            throw $err;
        }
    }

    private function validatePapiAccountActive()
    {
        if (!isset($this->papiAccountId) || empty($this->papiAccountId)) {
            throw new Exception("No LavuPay Account found for " . $this->dataname . ".");
        }
    }
}