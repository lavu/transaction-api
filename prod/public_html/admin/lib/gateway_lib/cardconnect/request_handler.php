<?php
namespace PaymentGateways\CardConnect;

use PaymentGateways\CardConnect\CardInput\IDynamoReader;
use PaymentGateways\CardConnect\CardInput\ManualEntry;
use papiApi;
use Exception;

require_once(dirname(__FILE__) . "/../../gateway_functions.php");
require_once(dirname(__FILE__) . "/card_input/card_input_interface.php");
require_once(dirname(__FILE__) . "/card_input/idynamo_reader.php");
require_once(dirname(__FILE__) . "/card_input/manual_entry.php");
require_once(dirname(__FILE__) . "/payment.php");
require_once(dirname(__FILE__) . "/tenant_account.php");

/**
 * Class RequestHandler
 * Class meant to handle requests coming specifically for all gateways
 * handled by our internal payment api
 *
 * @package PaymentGateways\CardConnect
 */
class RequestHandler
{
    private $request;
    private $location_info;
    public $tenantAccount;
    private $successful_response_attributes = array(
                                               '1',
                                               'success',
                                              );

    /**
     * @param array $request
     */
    public function __construct(array $request, $location_info)
    {
        $this->request       = $request;
        $this->location_info = $location_info;
    }

    public function tenantAccount()
    {
        if (!$this->tenantAccount) {
            $this->tenantAccount = new TenantAccount($this->request['data_name'], $this->request['loc_id']);
        }
        return $this->tenantAccount;
    }

    public function gateway()
    {
        $account        = $this->tenantAccount();
        $defaultGateway = $account->defaultGateway();
        return array(
                "type" => $defaultGateway->gatewayName,
                "data" => $defaultGateway->getPapiFields($this->request),
               );
    }

    /*
    * - create txn in cc_transactions table
    * - execute _request_
    * - if successful
    *   - return success message
    * - else
    *   - delete from cc_transactions table
    *   - return error message
    */
    public function process()
    {
        $payment = new Payment($this->inputMethodProxy());
        // what happens when this is a batch^
        switch ($payment->transactionType()) {
            case "AUTH":
            case "AUTHFORTAB":
                return $this->successfulTxnMessage($payment->authorize());
            case "CAPTUREALL":
                return $this->successfulTxnMessage($payment->batch());
            case "VOID":
                try {
                    if ($payment->isVoidable()) { // hack for PAY-230: Refund if void is not an eligible action
                        return $payment->void();
                    } else {
                        return $payment->refund();
                    }
                } catch (\Exception $exception) {
                    return $this->failedTxnMessage("Void Error", $exception->getMessage());
                }
                break;
            case "RETURN":
                try { // hack for PAY-230: Void if a refund is not an eligible action
                    if ($payment->isRefundable()) {
                        return $payment->refund();
                    } else {
                        return $this->processRefundAsVoid($payment);
                    }
                } catch (\Exception $exception) {
                    return $this->failedTxnMessage("Refund Error", $exception->getMessage());
                }
                break;
            case "SALE":
                return $payment->capture();
            case "ADJUSTMENT":
            case "PREAUTHCAPTURE":
            case "PREAUTHCAPTUREFORTAB":
                return $payment->adjust();
                break;
            default:
                return "0|Unsupported Transaction Type";
        }
    }

    /**
     * Process a refund as a void/recapture. Refunds can only be performed on transactions that have been
     * settled. However, the POS UI presents 'Refund' as an option even on non-settled transactions.
     * Instead of throwing an error (which would be the correct option) when the user selects 'Refund' on
     * a non-settled transaction, we actually go behind the scenes and void or recapture
     * the transaction at a lesser amount.
     * @param Payment $payment
     * @return string
     * @throws Exception if payment doesn't exist in database
     */
    public function processRefundAsVoid(Payment $payment)
    {
        $creditCardTxn = $payment->getCreditCardTransaction();
        if (empty($creditCardTxn)) {
            throw new Exception("No transaction found for payment");
        }
        $existingTxnAmount    = $creditCardTxn['amount'];
        $existingTxnTipAmount = $creditCardTxn['tip_amount'];
        $existingFullAmount   = $existingTxnAmount + $existingTxnTipAmount;

        $refundAmount    = $this->request['card_amount'];
        $refundTipAmount = $this->request['refund_tip_amount'];

        $newAmount     = $existingTxnAmount - $refundAmount;
        $newTipAmount  = $existingTxnTipAmount - $refundTipAmount;
        $newFullAmount = $existingFullAmount - $refundAmount - $refundTipAmount;
        $txnResult     = null;
        if ($newFullAmount > 0) {
            $clonedRequest                = $this->request; // this is effectively an array copy
            $clonedRequest['tip_amount']  = $newTipAmount;
            $clonedRequest['card_amount'] = $newAmount;
            // have to use Manual here since we don't have the card to swipe again
            $proxy      = new ManualEntry($clonedRequest, $this->location_info, $this->gateway());
            $newPayment = new Payment($proxy);
            $txnResult  = $newPayment->adjust();
        } else {
            $txnResult = $payment->void();
        }
        return $txnResult;
    }

    /*
        Some of these calls return successfully with no body
        this function is intended to mock a successful message in the form `1|success`
        Eventually we would like to only deal with responses of the type Response however,
        at this time it would require to many changes across multiple gateways to remove the
        bar seperated string conventions
    */
    public function successfulTxnMessage($response=null) {
        if (!$response || is_a($response, 'PaymentGateways\CardConnect\Response')) {
            $response = implode('|', $this->successful_response_attributes);
        } else if (is_array($response)) {
            $response = implode('|', $response);
        }
        return $response;
    }

    /**
     * Return a failed transaction response in format expected by POS app
     * @param string $title
     * @param string $message
     * @return string
     */
    public function failedTxnMessage($title="", $message="")
    {
        $response = array(
                     0,
                     $message,
                     "<PNREF>",
                     $this->request['order_id'],
                     "",
                     $title,
                    );
        return implode("|", $response);
    }

    public function inputMethodProxy()
    {
        if ($this->isIDynamoReader()) {
            return new IDynamoReader($this->request, $this->location_info, $this->gateway());
        }

        // attempt to enter payment using _Manual Entry_
        return new ManualEntry($this->request, $this->location_info, $this->gateway());
    }

    public function isFromCardReader()
    {
        return isset($this->request["reader"]);
    }

    public function reader()
    {
        return $this->isFromCardReader() ? strtolower($this->request["reader"]) : null;
    }

    private function isIDynamoReader()
    {
        return in_array($this->reader(), array("idynamo", "udynamo"));
    }
}
