<?php
namespace PaymentGateways\CardConnect;

use Lavu\Payments\PaymentStatus;
use PaymentGateways\CardConnect\CardInput\ICardInput;
use PaymentGateways\CardConnect\Queries;
use Exception;
use generate_transaction_vars;
use update_tables;
use lavu_query;

require_once(dirname(__FILE__) . "/api_client.php");
require_once(dirname(__FILE__) . "/constants.php");
require_once(dirname(__FILE__) . "/card_input/card_input_interface.php");
require_once(dirname(__FILE__) . "/card_input/helpers.php");
require_once(dirname(__FILE__) . "/queries/check.php");
require_once(dirname(__FILE__) . "/queries/credit_card_transaction.php");
require_once(dirname(__FILE__) . "/queries/order.php");

require_once(dirname(__FILE__) . "/../../gateway_functions.php");
require_once(dirname(__FILE__) . "/../../Lavu/Payments/PaymentStatus.php");

// http://dev-papi.devposlavu.com/develop/#api-Payments-PostPayment

/**
 * Suppress ToomanyMethods warnings for
 * this class.
 *
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class Payment extends APIClient
{
    private $input;
    private $db_txn_id;
    private $credit_card_txn;
    private $papi_payment;
    private $is_refundable;
    private $is_voidable;

    /**
     * @param ICardInput $input
     */
    public function __construct(ICardInput $input)
    {
        parent::__construct($input->dataname(), $input->locationId());
        $this->resource = "payments";
        $this->input    = $input;
    }

    public function authorize()
    {
        if ($this->input->isValid()) {
            try {
                $this->insertCreditCardTxnInDB();
                $response = $this->execute("POST", json_encode($this->input->authorizationFields()));
                if ($response->statusCode() == PaymentStatus::DECLINED) {
                    throw new Exception(TXN_DECLINED_ERROR);
                }
                $this->commitCreditCardTxnInDB($response);
            } catch (Exception $err) {
                $this->rollbackCreditCardTxnInDB();
                return $this->failedTxnMessage("Error", $err->getMessage());
            }
        } else {
            return $this->failedTxnMessage("Card read failed", CARD_READ_FAILED_ERROR);
        }

        return $this->successfulTxnMessage("Success!", "Successful Transaction", $response);
    }

    public function capture()
    {
        if ($this->input->isValid()) {
            try {
                $this->insertCreditCardTxnInDB();
                $response = $this->execute("POST", json_encode($this->input->captureFields()));
                if ($response->statusCode() == PaymentStatus::DECLINED) {
                    throw new Exception(TXN_DECLINED_ERROR);
                }
                $this->commitCreditCardTxnInDB($response);
            } catch (Exception $err) {
                $this->rollbackCreditCardTxnInDB();
                return $this->failedTxnMessage("Error", $err->getMessage());
            }
        } else {
            return $this->failedTxnMessage("Card read failed", CARD_READ_FAILED_ERROR);
        }
        return $this->successfulTxnMessage("Success", "Successul Transaction", $response);
    }

    public function adjust()
    {
        if ($this->input->isValid()) {
            try {
                $this->resource = "payments/" . $this->input->getPNREF() . "/capture";
                $payload = array(
                    "amount" => $this->input->getTotalAmount(true),
                );
                $response = $this->execute("POST", json_encode($payload));
                if ($response->statusCode() == PaymentStatus::DECLINED) {
                    throw new Exception(TXN_DECLINED_ERROR);
                }
                $this->commitCreditCardTxnInDB($response);
            } catch (Exception $err) {
                return $this->failedTxnMessage("Error", $err->getMessage());
            }
        } else {
            return $this->failedTxnMessage("Card read failed", CARD_READ_FAILED_ERROR);
        }

        return $this->successfulTxnMessage("Success!", "Successful Transaction", $response);
    }

    public function batch()
    {
        try {
            $this->resource = "payments/batch";
            $payload = array(
                "gateway" => $this->input->gatewayFields()
            );
            return $this->execute("POST", json_encode($payload));
        } catch (Exception $err) {
            return $this->failedTxnMessage("Error", $err->getMessage());
        }
    }

    /**
     * Suppress UnusedLocalVariable, StaticAccess warning
     * for this function. We may need $response at some
     * point later.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function void()
    {
        if ($this->isVoidable()) {
            try {
                $this->resource = "payments/" . $this->input->getPNREF() . "/void";

                $payload = array(
                    "amount" => 0,
                );
                $response       = $this->execute("POST", json_encode($payload));
                $server         = $this->input->getServer();
                $voidReason     = $this->input->getAdditionalTransactionInformation();

                // this should be a SQL transaction
                Queries\CreditCardTransaction::void($this->credit_card_txn['id'], $voidReason, $server["id"]);
                Queries\Order::updateVoidedOrder(
                    $this->input->locationId(),
                    $this->input->orderId(),
                    $this->credit_card_txn['amount']
                );
                Queries\Check::updateVoidedCheck(
                    $this->input->locationId(),
                    $this->input->orderId(),
                    $this->credit_card_txn['check'],
                    $this->credit_card_txn['amount']
                );
            } catch (Exception $err) {
                return $this->failedTxnMessage("Error", $err->getMessage());
            }
        } else {
            return $this->failedTxnMessage("Unable to void transaction", "Unable to void transaction.\n Transaction has already been captured.");
        }

        return $this->successfulTxnMessage("Voided successfully", "Voided successfully");
    }

    /**
     * Suppress UnusedLocalVariable, StaticAccess warning
     * for this function. We may need $response at some
     * point later.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function refund()
    {
        $refundable = false;
        try {
            $refundable = $this->isRefundable();
        } catch (Exception $exception) {
            error_log($exception);
            return $this->failedTxnMessage("Error", $exception->getMessage());
        }
        if ($this->input->isValid() && $refundable) {
            try {
                $this->resource = "payments/" . $this->input->getPNREF() . "/refund";
                $payload = array(
                    "amount" => $this->input->getRefundAmount(true),
                );
                $response = $this->execute("POST", json_encode($payload));
                if ($response->statusCode() == PaymentStatus::DECLINED) {
                    throw new Exception("Transaction Declined.");
                }
                $this->commitCreditCardTxnInDB($response);
            } catch (Exception $err) {
                return $this->failedTxnMessage("Error", $err->getMessage());
            }
        } else {
            return $this->failedTxnMessage("Card read failed", "Card read failed.\nPlease try again.");
        }

        return $this->successfulTxnMessage("Success!", "Successful Transaction", $response);
    }

    // TODO: implement
    public function listAll()
    {
        $response = $this->execute();
        return $response;
    }

    public function transactionId()
    {
        return $this->response->id;
    }

    public function transactionType()
    {
        return $this->input->transactionType();
    }

    /**
     * Suppress StaticAccess warning for this function.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getCreditCardTransaction()
    {
        if (!isset($this->credit_card_txn)) {
            $this->credit_card_txn = Queries\CreditCardTransaction::get(
                $this->input->locationId(),
                $this->input->orderId(),
                $this->input->getPNREF()
            );
        }

        return $this->credit_card_txn;
    }

    /**
     * Return whether the transaction is voidable in its current state
     * @return boolean - true if the transaction is voidable, false otherwise
     * @throws Exception - when payment with transaction id isn't found
     * Suppress StaticAccess warning for this function.
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function isVoidable()
    {
        if (!isset($this->is_voidable)) {
            $transaction = $this->getCreditCardTransaction();
            $transactionId = $transaction['transaction_id'];
            $payment = $this->fetch();
            if ($payment != null) {
                $statusCode = $payment->statusCode();
                $this->is_voidable = ($statusCode === PaymentStatus::AUTHORIZED
                                      || $statusCode === PaymentStatus::CAPTURED);
            } else {
                throw new Exception("Payment with transaction id ${transactionId} not found");
            }
        }
        return $this->is_voidable;
    }

    /**
     * Return whether the payment is refundable in its current state.
     *
     * @return boolean - true if refundable, false otherwise
     * @throws Exception if payment cannot be found
     */
    public function isRefundable()
    {
        if (!isset($this->is_refundable)) {
            $transaction = $this->getCreditCardTransaction();
            $transactionId = $transaction['transaction_id'];
            $payment = $this->fetch();
            if ($payment != null) {
                $this->is_refundable = ($payment->statusCode() === PaymentStatus::ACCEPTED);
            } else {
                throw new Exception("Payment with transaction id ${transactionId} not found");
            }
        }
        return $this->is_refundable;
    }

    /**
     * Returns an array of string values that are
     * utilized to build a string message for the
     * iOS application.
     *
     * @param string $message message to be send to the app
     * @param mixed $txn credit card transaction db record
     * @param mixed $txnVars request params
     *
     * @return string[]
     */
    public static function toMessageArray($txn, $txnVars, $message="")
    {
        $output = array(
            '1',
            $txn['preauth_id'],
            $txnVars['card_desc'],
            $message,
            $txn['auth_code'],
            $txn['card_type'],
            "",
            "",
            $txnVars['order_id'],
            "",
            "",
            "",
            "",
            $txn['first_four'],
            $txn['last_mod_ts']
        );
        return $output;
    }

    /**
     * Fetch payment from Payment API
     * @return object - Payment object as represented by the Payment API if found, null otherwise
     */
    private function fetch()
    {
        if (!isset($this->papi_payment)) {
            $this->papi_payment = null;
            $transaction = $this->getCreditCardTransaction();
            $transactionId = $transaction['transaction_id'];
            if ($transactionId) {
                try {
                    $this->resource = "payments/" . $transactionId;
                    $this->papi_payment = $this->execute("GET");
                } catch (Exception $exception) {
                    error_log($exception->getMessage());
                }
            }
        }
        return $this->papi_payment;
    }

    private function insertCreditCardTxnInDB()
    {
        $transaction_vars = generate_transaction_vars(
            $this->input->getRawRequest(),
            $this->input->getRawLocationInfo(),
            $this->input->cardNumber(true),
            $this->input->cardBrand(),
            $this->input->getRawRequest()["transtype"],
            null,
            $this->input->getRawRequest()["set_pay_type"],
            $this->input->getRawRequest()["set_pay_type_id"],
            $this->input->getRawRequest()["card_amount"]
        );
        // need to verify record actually got persisted
        lavu_query($this->prepareInsertStatement(array_keys($transaction_vars)), $transaction_vars);
        $this->db_txn_id = lavu_insert_id();
    }

    private function commitCreditCardTxnInDB($response)
    {
        $data       = $response->getData();
        $rawRequest = $this->input->getRawRequest();
        $request    = $this->preDBCommit($rawRequest, $data);
        update_tables(
            $request,
            $this->input->getRawLocationInfo(),
            $this->input->cardBrand(),
            $response->transactionId(),
            "",
            $this->db_txn_id,
            "0",
            "",
            "",
            "",
            "");
    }

    /**
     * Enhance request with additional data prior to committing anything to the
     * database.
     * @param $request - initial gateway request with data to be saved to DB
     * @param $papiResponse - response from PAPI
     * @return mixed - data to be saved to DB
     */
    private function preDBCommit($request, $papiResponse) {
        $result = $request;
        if (isset($papiResponse->card->expMonth) && isset($papiResponse->card->expYear)) {
            $result['exp'] = str_pad($papiResponse->card->expMonth, 2, '0', STR_PAD_LEFT) .
                                 '/' . $papiResponse->card->expYear;
        }

        return $result;
    }

    private function rollbackCreditCardTxnInDB()
    {
        lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $this->db_txn_id);
    }

    /*
    * returns string with shape:
    * INSERT INTO `cc_transactions` (`fieldA`,`fieldB`, ...,`fieldN`) VALUES ('[fieldA]','[fieldB]', ...,'[fieldN]')
    */
    private function prepareInsertStatement(array $fields)
    {
        $reducer = function ($carry, $item) {
            $carry["fields"] .= "`$item`,";
            $carry["values"] .= "'[$item]',";
            return $carry;
        };

        $output = array_reduce($fields, $reducer, array("fields" => "", "values" => ""));
        return "INSERT INTO `cc_transactions` (" . trim($output["fields"], ",") . ") VALUES (" . trim($output["values"], ",") . ")";
    }


    //TODO: Think about moving/refactoring this method since the payment model
    //shouldn't need to know how a POS response is structured.
    /**
     * Suppress UnusedFormalParameter warning for
     * this function until we figure out where to
     * use the $title param in the response.
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    private function successfulTxnMessage($title = "", $message = "", $response = null)
    {
        $profile     = $this->input->getRawRequest(); #this function will return profile info
        $transaction = Queries\CreditCardTransaction::getById($this->db_txn_id);
        $output      = array(
                   1,
                   (isset($response) ? $response->pnref() : ""),
                   $this->input->cardNumber(),
                   $message,
                   "", // AUTH CODE
                   $this->input->cardBrand(),
                   "",
                   "",
                   $this->input->orderId(),
                   "",
                   "",
                   "", // NEW BALANCE
                   "", // BONUS MESSAGE
                   "", // FIRST FOUR
                   (isset($transaction['last_mod_ts']) ? $transaction['last_mod_ts'] : ""),
                   "",
                   "",
                   "",
                   "",
                   $profile['name_on_card'],
        );
        return implode("|", $output);
    }

    private function failedTxnMessage($title="", $message="")
    {
        $response = array(
            0,
            $message,
            "<PNREF>",
            $this->input->orderId(),
            "",
            $title
        );

        return implode("|", $response);
    }
}