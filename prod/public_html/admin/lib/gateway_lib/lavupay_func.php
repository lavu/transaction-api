<?php
include(dirname(__FILE__) . "/cardconnect/request_handler.php");

use PaymentGateways\CardConnect\RequestHandler;

function process_papi($request, $location_info)
{
    $location_info['gateway'] = getGatewayFromRequest($request);
    $req = new RequestHandler($request, $location_info);
    return $req->process();
}

/**
 * Return papi gateway from the given request
 *
 * @param $request
 * @return mixed
 * @throws Exception
 */
function getGatewayFromRequest($request)
{
    $papiGateways = array(
                     \PaymentGateways\CardConnect\CARDCONNECT_GATEWAY_NAME,
                     \PaymentGateways\CardConnect\FREEDOMPAY_GATEWAY_NAME,
                     \PaymentGateways\CardConnect\LAVUPAY_GATEWAY_NAME,
                    );
    if (isset($request['request_gateway']) && in_array(strtolower($request['request_gateway']), $papiGateways)) {
        return $request['request_gateway'];
    }
    throw new Exception('Valid gateway not present in request object');
}
