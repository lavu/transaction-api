<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 8/21/17
 * Time: 10:19 PM
 */

require_once(dirname(__FILE__) . "/../../components/payment_extensions/square/SquarePaymentHandler.php");
require_once(dirname(__FILE__) . "/../gateway_functions.php");

global $data_name;

function format_square_gateway_response($rvars, $process_info)
{
    $lastModTS = (empty($process_info['last_mod_ts'])) ? "" : $process_info['last_mod_ts'];
    $rtn = array();
    if(isset($rvars['approved']) && $rvars['approved'] === true)
    {
        //  0 - success flag ("1")
        //  1 - [transaction_id]
        //  2 - [card_desc] (last_four)
        //  3 - response message
        //  4 - [auth_code]
        //  5 - [card_type]
        //  6 - [record_no]
        //  7 - [amount]
        //  8 - [order_id]
        //  9 - [ref_data]
        // 10 - [process_data]
        // 11 - new balance
        // 12 - bonus message
        // 13 - [first_four]
        // 14 - [last_mod_ts]
        // 15 - [tip_amount]
        // 16 - [split_tender_id]
        // 17 - [rf_id] Storing location ID

        $rtn[] = "1"; // 0
        $rtn[] = $rvars['txnid']; //transaction ID
        $rtn[] = $rvars['last_four'];
        $rtn[] = $rvars['message'];;
        $rtn[] = $rvars['txnid'];
        $rtn[] = $rvars['card_type'];
        $rtn[] = $rvars['record_no'];;
        $rtn[] = $process_info['card_amount'];
        $rtn[] = $process_info['order_id'];
        $rtn[] = "";
        $rtn[] = ""; // 10
        $rtn[] = "";
        $rtn[] = "";
        $rtn[] = "";
        $rtn[] = $lastModTS;
        $rtn[] = $rvars['tip_amount'];
        $rtn[] = $rvars['split_tender_id'];
        $rtn[] = $rvars['rf_id'];
    }
    else
    {
        $rtn[] = "0";
        $rtn[] = $rvars['message'];
        $rtn[] = $rvars['txnid'];
        $rtn[] = $rvars['order_id'];
        $rtn[] = "";
        $rtn[] = (isset($rvars['title']))?$rvars['title']:"Unable to Process";
    }

    return implode("|",$rtn);
}

function pp_postvar($k,$d="")
{
    if(isset($_POST[$k])) return $_POST[$k];
    else if(isset($_GET[$k])) return $_GET[$k];
    else return $d;
}

function square_format_decline($message,$process_info)
{
    $rvars = array();
    $rvars['approved'] = false;
    $rvars['message'] = $message;
    $rvars['order_id'] = pp_postvar("order_id");
    $rvars['txnid'] = "";
    $rvars['last_four'] = substr($process_info['card_number'],-4);

    return format_square_gateway_response($rvars, $process_info);
}

function square_write_logfile($wstr)
{
    error_log(__FILE__ . " RESPONSE FROM SQUARE API CALL:  ".$wstr);

}

function process_square($process_info,$location_info)
{
    global $data_name;

    $SquarePaymentHandler = new SquarePaymentHandler($data_name);
    $order_id = $process_info["order_id"];
    $transtype = isset($process_info['transtype']) ? $process_info['transtype'] : "";
    $split_tender_id = isset($process_info['split_tender_id']) ? $process_info['split_tender_id'] : "";

    $transaction_vars = generate_transaction_vars($process_info, $location_info, $process_info['card_number'],
        getCardTypeFromNumber($process_info['card_number']), $process_info['transtype'],
        "", $process_info['set_pay_type'], $process_info['set_pay_type_id'], $process_info['card_amount']);

    if (!isset($data_name)) {
        return "0|Merchant Information Unavailable, please try processing via the Square button.";
    }
    $_REQUEST['dataname'] = $data_name;
    if ($SquarePaymentHandler->getLocationID() == "") {
        return square_format_decline("Incomplete Setup: Please enter your square location id in your control panel under Extensions > Square Payment Method", $process_info);
    }

    //Block to setup the transaction
    // Set Transaction ID
    $cleanupRequired = false;
    $txnid = (isset($process_info['authcode']) && $process_info['authcode'] != "") ? $process_info['authcode'] : "";
   // $record_no = "";
    $authcode = "";
    //getting the transaction row if it exists, generating the row if it does not exist and we need to create it
    $transactionRow = lavu_query("select * from `cc_transactions` where `order_id`='[1]' and (`preauth_id`='[2]' OR `transaction_id`='[2]' OR `auth_code`='[2]') and `check`='[3]' and `transtype` IN ('Sale', 'GiftCardSale') order by id desc limit 1", $order_id, $txnid, $process_info['check']);
    if (mysqli_num_rows($transactionRow) < 1) {
        if ($transtype === "Sale" || $transtype === "Auth") {
            $txn_system_id = square_create_initial_transaction_record($location_info, $transaction_vars);
            $cleanupRequired = true;
        }
    } else {
        $transactionArray = mysqli_fetch_assoc($transactionRow);
        $txn_system_id = isset($transactionArray['id']) ? $transactionArray['id'] : "";
       // $preauthID = isset($transactionArray['preauth_id']) ? $transactionArray['preauth_id'] : "";
        $authcode = isset($transactionArray['auth_code']) ? $transactionArray['auth_code'] : "";
        $txnid = isset($transactionArray['transaction_id']) ? $transactionArray['transaction_id'] : $txnid;
        //$split_tender_id = isset($transactionArray['split_tender_id']) ? $transactionArray['split_tender_id'] : '';

    }

    $fields = '';
    switch ($transtype) {
        case "Void":
            $curlURL = '/transactions/'.$txnid.'/void';
            break;
        case "Return":
        case "GiftCardReturn":
            if($split_tender_id == '') {
                $retrieveTransaction = '/transactions/' . $txnid;
                $response = $SquarePaymentHandler->makeApiCall($fields, $SquarePaymentHandler->getApiEndPointWithLocation() . $retrieveTransaction, 'GET');
                $decodeResponse = json_decode($response, true);
                if(!isset($decodeResponse['errors'])) {
                    $split_tender_id = $decodeResponse["transaction"]["tenders"][0]["id"];
                } else {
                    return "0|".$decodeResponse['errors'][0]['detail'];
                }
            }
            if ($split_tender_id != '') {
                $returnCard = $process_info['card_amount'];
                $returnTip = $process_info['refund_tip_amount'];
                $return_amount = str_replace(",", "", number_format($returnCard * 1 + $returnTip * 1, 2));
                $reason = ($process_info['more_info'] != '') ? $process_info['more_info'] : 'Refund';
                $curlURL = '/transactions/' . $txnid . '/refund';
                $currencyCode = $decodeResponse["transaction"]["tenders"][0]["amount_money"]["currency"];
                $currencyCode = ($currencyCode != '' )? $currencyCode : "USD";
                $conversionDetails = $SquarePaymentHandler->tokenHelper->getSquareConversionRate();
                $conversionRate = (isset($conversionDetails[$currencyCode])) ? $conversionDetails[$currencyCode] : 100;
                $return_amount = $return_amount * $conversionRate;
                $fields = '{' .
                    '"idempotency_key": "' . uniqid() . '",' .
                    '"tender_id": "' . $split_tender_id . '",' .
                    '"reason":  "' . $reason . '",' .
                    '"amount_money": {' .
                    '"amount": ' . $return_amount . ',' .
                    '"currency": "' . $currencyCode . '"' .
                    '}
                }';
            } else {
                return "0|Tender id is not found";
            }
            break;
        case "Sale":
        case "Auth":
        case "GiftCardSale":
            break;
        case "Adjustment":
        case "PreAuthCapture":
            if (empty($txnid)) {
                return "0|Unable to retrieve transaction identifier";
            }
            $curlURL = '/transactions/'.$txnid.'/capture';
            break;
        default:
            return "0|Unsupported Transaction Type";
    }


    //Perform the transaction
    $response = "";
    $state = "";
    $error = true;
    if (!in_array($transtype, array("Sale", "Auth", "GiftCardSale"))) {
        $response = $SquarePaymentHandler->makeApiCall($fields, $SquarePaymentHandler->getApiEndPointWithLocation() . $curlURL, 'POST');
        $rspvars = json_decode($response, true);
        if(!isset($rspvars['errors'])) {
            $error = false;
            $txn = $txnid;
            $state = "completed";
            $ReturnID = $txn;
            $transactionArray['record_no'] = $rspvars['refund']['id'];
        }
        
        	
    } else {
        $txn = $txnid;
        $state = 'completed';
    }

    if(lavuDeveloperStatus()){
        square_write_logfile($response);
    }
    $rspstr = "No Recognized Response";

    $approved = true;
    $rspstr = "Approved";
    /*
     * transactionNumber = auth_code
     * id = refund_pnref or void_pnref
     * When Adjusting or capturing, the id will populate the preauth_id field
     */
    // Commented this block because we are inserting record form lds_connect.php sync(). No need to update from backend.
    /*if($state === "completed" && $transtype === "Sale") {
        square_update_transaction_record($process_info,$location_info,$txn,$txn,$txn_system_id, "");
    }
    else if($state==="completed" && ($transtype === "Adjustment" || $transtype === "PreAuthCapture")){
        square_update_transaction_record($process_info,$location_info,$ReturnID,$txnid,$txn_system_id, "");
        lavu_query("update `cc_transactions` set `preauth_id`='[1]' where `id` = '[2]'",$ReturnID,$txn_system_id);
    }
    else if($state === "completed" && $transtype === "Return"){
        square_update_transaction_record($process_info,$location_info,$txn,$txn,$txn_system_id, "");
        lavu_query("update `cc_transactions` set `refunded`='1', `refund_pnref`='[1]', `refund_notes`='[3]' where `id`='[2]'",$ReturnID,$txn_system_id,$process_info['more_info']);
    }
    else if($state === "completed" && $transtype === "Void"){
        square_update_transaction_record($process_info,$location_info,$txn,"",$txn_system_id, "");
        lavu_query("update `cc_transactions` set `voided`='1', `void_pnref`='[1]', `void_notes`='[3]' where `id`='[2]'",$ReturnID,$txn_system_id,$process_info['more_info']);

    }
    else {*/
    if ($state != "completed") {
        $approved = false;
        $message = "Square Server Error ". $rspvars['errors'][0]['detail'];
        $rspstr = "Declined\n$message";
        if($cleanupRequired && $transtype === "Auth"){
            lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $txn_system_id);
        }
    }

    $card_amount = (isset($process_info['card_amount']))?$process_info['card_amount']:pp_postvar("card_amount");
    $tip_amount = (empty($process_info['tip_amount']))?"0":$process_info['tip_amount'];
    $lg_str = "PayPal REST Payment [--CR--]";
    $lg_str .= "LOCATION: ".			$process_info['loc_id']						." - ";
    $lg_str .= "REGISTER: ".			$process_info['register']	." - ";
    $lg_str .= "SERVER ID: ".		$process_info['server_id']	." - ";
    $lg_str .= "ORDER ID: ".			$order_id					." - ";
    $lg_str .= "CHECK : ".			$process_info['check']		." - ";
    $lg_str .= "AMOUNT: ".			$card_amount. " TIP AMOUNT: ".$tip_amount." - ";
    $lg_str .= "TRANSTYPE: ".		$transtype					." - ";
    $lg_str .= "RESULT: ".			$approved						." - ";
    $lg_str .= "PNREF: ".			empty($txn) ? empty($ReturnID)?"" : $ReturnID : $txn					." - ";
    $lg_str .= "AUTHCODE: ".			$authcode					." - ";
    $lg_str .= "CARD TYPE: ".		$transaction_vars['card_type']					." - ";
    $lg_str .= "FIRST FOUR: ".		$transaction_vars['first_four']					." - ";

    //$set_number = (isset($process_info['card_number']))?$process_info['card_number']:"";
    $lg_str .= "LAST FOUR: ".		$transactionArray['card_desc']				." - ";
    $swiped = 0;
    if (!empty($process_info['mag_data'])){

        $mag_info = explode("|", $process_info['mag_data']);
        $readerstring = "MAGTEK SWIPER SERIAL NO. " . $mag_info[7];
        $lg_str.= "READER: ".	$readerstring	." - ";
        $swiped = 1;
    }
    $lg_str .= "SWIPED: ".			$swiped						."[--CR--]";
    $lg_str .= "SENT EXT_DATA: ".	$process_info['ext_data']					."[--CR--]";
    $lg_str .= "RESPONSE MESSAGE: ".$rspstr						."[--CR--]";
    write_log_entry("gateway", $data_name, $lg_str."\n");

    $rvars = array();
    $rvars['approved'] = $approved;
    $rvars['message'] = $rspstr;
    $rvars['order_id'] = $order_id;
    $rvars['txnid'] = empty($txn) ? (empty($ReturnID)?"":$ReturnID):$txn;
    $rvars['last_four'] = $transactionArray['card_desc'];
    $rvars['card_type'] = $transactionArray['card_type'];
    $rvars['tip_amount'] = $tip_amount;
    $rvars['split_tender_id'] = $split_tender_id;
    $rvars['rf_id'] = $transactionArray['rf_id'];
    $rvars['record_no'] = $transactionArray['record_no'];
	//print_r($rvars);
   /* if(strpos($rvars['message'],'refund amount exceeds')!=false){
    	$strfail = format_square_gateway_response($rvars, $process_info);
    	$transDetails = $SquarePaymentHandler->getTransactionFromV2ApiAndUpdateTransaction($txnid);
    	//      print_r($transDetails);
    	if(count($transDetails)>0){
    		foreach ($transDetails as $values){
    			//print_r($values);
    			$rvars['approved'] = true;
    			$rvars['message'] = 'Approved';
    			$rvars['order_id'] = $order_id;
    			$rvars['txnid']  =$txnid;
    			$rvars['last_four'] = $transactionArray['card_desc'];
    			$rvars['card_type'] = $transactionArray['card_type'];
    			$rvars['tip_amount'] = $tip_amount;
    			$rvars['split_tender_id'] = $values['split_tender_id'];
    			$rvars['rf_id'] = $values['rf_id'];
    			$rvars['record_no'] = $values['record_no'];
    			$process_info['card_amount'] = $values['amount'];
    			$strtransdata[] = format_square_gateway_response($rvars, $process_info);
    		}
    		return $strfail."|reconcile_info"."|".json_encode($strtransdata);
    	}else{
    		return format_square_gateway_response($rvars, $process_info);
    	}
    }
    else{
    	return format_square_gateway_response($rvars, $process_info);
    }*/
    return format_square_gateway_response($rvars, $process_info);
}

function first_string_element_only($str)
{
    $str_parts = explode(",",$str);
    $str_parts = explode(";",$str_parts[0]);
    return trim($str_parts[0]);
}

function urldecode_string($str)
{
    $vars = array();
    $str_parts = explode("&",$str);
    for($i=0; $i<count($str_parts); $i++)
    {
        $inner_parts = explode("=",$str_parts[$i]);
        if(count($inner_parts) > 1)
        {
            $keys = trim($inner_parts[0]);
            $values = trim($inner_parts[1]);
            $vars[$keys] = $values;
        }
    }
    return $vars;
}

function square_create_initial_transaction_record($location_info, $transaction_vars)
{
    global $location_info;
    $q_fields = "";
    $q_values = "";
    $keys = array_keys($transaction_vars);
    foreach ($keys as $key) {
        if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
        $q_fields .= "`$key`";
        $q_values .= "'[$key]'";
    }

    $create_transaction_record = true;
    $system_id = 0;
    if ($create_transaction_record) {
        $preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
        if (!$preliminary_record_transaction) return 0;//"0|Failed to create transaction record.||".$order_id;
        $system_id = lavu_insert_id();
    }
    return $system_id;
}

function square_update_transaction_record($process_info,$location_info,$transaction_id,$authcode,$system_id, $record_no)
{
    update_tables($process_info, $location_info, "", $transaction_id, $authcode, $system_id, "0", "", "", "", $record_no);
}
function getCurrencyCode()
{
	$currencyCode='';
	$query = "SELECT value,value2  FROM `config` where type='location_config_setting' and setting in ('primary_currency_code') ";
	
	$result = lavu_query($query);
	if(!$result){
		error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
	}
	else{
		if(mysqli_num_rows($result)>0){
			$cur_info = mysqli_fetch_assoc($result);
				 
			$currency_id = $cur_info['value2'];
		 
			$query_cur = "select id, name, alphabetic_code from country_region where id=[1]";
			$result_cur = mlavu_query($query_cur,$currency_id);
			if(!$result_cur){
				error_log("mysql error in ".__FILE__." mysql error:" . lavu_dberror());
			}
			else{
				if(mysqli_num_rows($result_cur)>0){
					$region_info = mysqli_fetch_assoc($result_cur);
					$currencyCode = $region_info['alphabetic_code'];
				}
			}
		}
	}
	
	return $currencyCode;
}
