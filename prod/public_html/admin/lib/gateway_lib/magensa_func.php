<?php

	function translate_status_code($status_code) {
	
		$rtn = $status_code;
	
		switch ($status_code) {
			case "111" : $rtn = "Capture error: transaction has already been captured"; break;
			case "C089" : $rtn = "Error Validating (MerchantID and MerchantPW) against assigned DB or Operation."; break;
			case "C099" : $rtn = "Error Validating (HostID and HostPW) against assigned DB or Operation."; break;
			case "H001" : $rtn = "HostID has incorrect length"; break;
			case "H002" : $rtn = "HostID has incorrect format"; break;
			case "H003" : $rtn = "HostPW has incorrect length"; break;
			case "H004" : $rtn = "HostPW has incorrect format"; break;
			case "H005" : $rtn = "MerchantID has incorrect length"; break;
			case "H006" : $rtn = "MerchantID has incorrect format"; break;
			case "H007" : $rtn = "MerchantPW has incorrect length"; break;
			case "H008" : $rtn = "MerchantPW has incorrect format"; break;
			case "H176" : $rtn = "Card read failure. Please try again."; break; // EncryptedTrack1 has incorrect format
			case "H178" : $rtn = "Card read failure. Please try again."; break; // EncryptedTrack2 has incorrect format
			case "H179" : $rtn = "Card read failure. Please try again."; break; // EncryptedTrack2 has incorrect length
			case "H180" : $rtn = "Card read failure. Please try again."; break; // EncryptedTrack3 has incorrect format
			case "H181" : $rtn = "Card read failure. Please try again."; break; // EncryptedTrack3 has incorrect length
			case "H182" : $rtn = "Card read failure. Please try again."; break; // EncryptedMP has incorrect format
			case "H183" : $rtn = "Card read failure. Please try again."; break; // EncryptedMP has incorrect length
			case "H186" : $rtn = "Card read failure. Please try again."; break; // KSN has incorrect format
			case "H187" : $rtn = "Card read failure. Please try again."; break; // KSN has incorrect length
			case "H188" : $rtn = "Card read failure. Please try again."; break; // MPStatus has incorrect format
			case "H189" : $rtn = "MPStatus has incorrect length"; break; // MPStatus has incorrect length
			case "H320" : $rtn = "Invalid Amount"; break;
			case "H321" : $rtn = "Invalid Transaction Type"; break;
			case "H322" : $rtn = "Invalid AuthCode"; break;
			case "H380" : $rtn = "CVV has incorrect length"; break;
			case "H381" : $rtn = "CVV has incorrect format"; break;
			case "H385" : $rtn = "ZIP has incorrect length"; break;
			case "H386" : $rtn = "ZIP has incorrect format"; break;
			case "H400" : $rtn = "Invalid Tax Amount"; break;
			case "H401" : $rtn = "PAN has incorrect length - Input Validation"; break;
			case "H405" : $rtn = "Invalid Cardholder Name"; break;
			case "K089" : $rtn = "Error Validating (MerchantID and MerchantPW) against assigned DB or Operation.";
			case "K099" : $rtn = "Error Validating (HostID and HostPW) against assigned DB or Operation.";
			default: break;
		}
		
		return $rtn;
	}
	
	function translate_transaction_status($result, $transaction_msg) {
	
		global $send_debug_messages;
	
		$return_string = $transaction_msg;
		
		switch ($result) {
			case "0" : $return_string = "Approved"; break;
			case "12" : $return_string = "Transaction Status 12"; break;
			case "111" : $return_string = "Approved"; break;
			default: break;
		}
		
		return $return_string;
	}

	function post_to_magensa($request, $process_type, $data_name, $transtype, $location_info) {

		$server_url = "mppg.magensa.net";
		
		$header = array();
		$header[] = "POST /MPPG/service.asmx HTTP/1.1";
		$header[] = "Host: ".$server_url;
		$header[] = "Content-Length: ".(strlen($request) + 0);
		$header[] = "Content-Type: text/xml; charset=utf-8";
		$header[] = "SOAPAction: \"http://www.magensa.net/".$process_type."\"";

		ConnectionHub::closeAll();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://".$server_url."/MPPG/service.asmx");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		//curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_TIMEOUT, 6);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		$response = curl_exec($ch);
		curl_close($ch);
				
		$start_tag = "<".$process_type."Result>";
		$end_tag = "</".$process_type."Result>";
		
		$start_pos = (strpos($response, $start_tag) + strlen($start_tag));
		$end_pos = strpos($response, $end_tag);
		
		$curl_result = htmlspecialchars_decode(substr($response, $start_pos, ((strlen($response) - $start_pos)) - (strlen($response) - $end_pos)));

		if ($location_info['gateway_debug'] == "1") gateway_debug($data_name, $loaction_info['id'], "MAGENSA", $transtype, "XML", "HEADER INFO: ".print_r($header, true)."\n\nPOST_DATA: ".htmlspecialchars_decode($request)."\n\nRESPONSE: ".htmlspecialchars_decode($response)."\n\n\nRESULT: ".$curl_result);

		return $curl_result;
	}
		
	function process_magensa2($process_info, $location_info, $transaction_vars) {

		$debug = print_r($process_info, true)."\n\n";
	
		$card_number = $process_info['card_number'];
		$mag_data = $process_info['mag_data'];
		$send_amount = $process_info['card_amount'];
		$cn = $card_number; // keep for response string
		$first_four = substr($cn, 0, 4);
		
		$swiped = "0";
		if ($mag_data) $swiped = "1";
				
		$dukpt_ksn = "";
		if ($mag_data && $process_info['reader']=="idynamo") {
			$e2e = true;
			$split_mag_data = explode("|", $mag_data);
			$encrypted_track1 = $split_mag_data[2];
			$encrypted_track2 = $split_mag_data[3];
			$encrypted_track3 = $split_mag_data[4];
			$encrypted_mp = $split_mag_data[6];
			$dukpt_ksn = $split_mag_data[9];
			$mp_status = $split_mag_data[5];
			
			$process_type = "ProcessCS10";
			
		} else {
		
			$e2e = false;
			$process_type = "ProcessKE10";
		}

		$send_card_amount = $process_info['card_amount'];

		$transtype = $process_info['transtype'];
		if ($transtype == "") $transtype = "Sale";
		
		switch ($transtype) {
			case "Sale" :
				$qptrantype = "S";
				break;
			case ($transtype=="Auth" || $transtype=="AuthForTab") :
				$qptrantype = "A";
				if ($process_info['order_id'] == "0") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
				if ($transtype=="AuthForTab" && (float)$send_card_amount==0) {
					$process_info['card_amount'] = $location_info['default_preauth_amount'];
					$send_card_amount = $process_info['card_amount'];
				}
				break;
			case "PreAuthCapture":
			case "PreAuthCaptureForTab":
				$qptrantype = "D";
				$process_type = "ProcessTI10";
				break;
			case "Void" :
				$qptrantype = "V";
				$process_type = "ProcessTI10";
				$send_card_amount = "";
				$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$card_type = $transaction_info['card_type'];
				$first_four = $transaction_info['first_four'];
				$cn = $transaction_info['card_desc'];
				// Magensa allows Auths to be voided
				break;
			case "Return" :
				$qptrantype = "R";
				$send_card_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
				break;
			default: break;
		}
		$transaction_vars['transtype'] = $transtype;
		if(isset($swipe_grade)) $transaction_vars['swipe_grade'] = $swipe_grade;
		else  $transaction_vars['swipe_grade'] = 'A';


		$q_fields = "";
		$q_values = "";
		$keys = array_keys($transaction_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
	
		$system_id = $transaction_vars['system_id'];
		
		$host_id = "MPH013756870";
		$host_pw = "a@K6ZHM82!m7gp";
		
		// MPM345713279
		// m!j5#7YhuQ63fo

		$name_on_card = "None entered";
	
		if (in_array($transtype, array("Void", "PreAuthCapture", "PreAuthCaptureForTab"))) {
			$request = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><".$process_type." xmlns=\"http://www.magensa.net/\"><".$process_type."_Input><TransactionType>$qptrantype</TransactionType><HostID>$host_id</HostID><HostPW>$host_pw</HostPW><MerchantID>".$process_info['username']."</MerchantID><MerchantPW>".$process_info['password']."</MerchantPW><TransactionID>".$process_info['pnref']."</TransactionID><Amt>$send_card_amount</Amt></".$process_type."_Input></".$process_type."></soap:Body></soap:Envelope>";
		} else {
			if ($e2e) $request = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><".$process_type." xmlns=\"http://www.magensa.net/\"><".$process_type."_Input><TransactionType>$qptrantype</TransactionType><HostID>$host_id</HostID><HostPW>$host_pw</HostPW><MerchantID>".$process_info['username']."</MerchantID><MerchantPW>".$process_info['password']."</MerchantPW><EncryptedTrack1>$encrypted_track1</EncryptedTrack1><EncryptedTrack2>$encrypted_track2</EncryptedTrack2><EncryptedTrack3></EncryptedTrack3><EncryptedMP>$encrypted_mp</EncryptedMP><KSN>$dukpt_ksn</KSN><MPStatus>$mp_status</MPStatus><Amt>$send_card_amount</Amt><TaxAmt></TaxAmt><PurchaseOrder></PurchaseOrder><InvoiceNumber>".str_replace("-", "", $location_id.$order_id)."</InvoiceNumber><CVV></CVV><ZIP></ZIP></".$process_type."_Input></".$process_type."></soap:Body></soap:Envelope>";
			else $request = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><".$process_type." xmlns=\"http://www.magensa.net/\"><".$process_type."_Input><TransactionType>$qptrantype</TransactionType><HostID>$host_id</HostID><HostPW>$host_pw</HostPW><MerchantID>".$process_info['username']."</MerchantID><MerchantPW>".$process_info['password']."</MerchantPW><PAN>$card_number</PAN><CHName>$name_on_card</CHName><CardExpDt>".$process_info['exp_month'].$process_info['exp_year']."</CardExpDt><CVV>".$process_info['card_cvn']."</CVV><Amt>$send_card_amount</Amt><TaxAmt></TaxAmt><PurchaseOrder></PurchaseOrder><InvoiceNumber>".str_replace("-", "", $process_info['loc_id'].$process_info['order_id'])."</InvoiceNumber><ZIP></ZIP></".$process_type."_Input></".$process_type."></soap:Body></soap:Envelope>";
		}
		$curl_result = post_to_magensa($request, $process_type, $process_info['data_name'], $transtype, $location_info);

		$status_code = extract_value($curl_result, "StatusCode");
		
		$result = "1";
		$new_pnref = extract_value($curl_result, "TransactionID");

		if ($status_code == "1000") {
	
			$result = extract_value($curl_result, "TransactionStatus");
			$respmsg = translate_transaction_status($result, extract_value($curl_result, "TransactionMsg"));
			$authcode = extract_value($curl_result, "AuthCode");
		
		} else if (strstr($curl_result, "Server was unable to process request")) {
		
			$respmsg = "Server was unable to process request...";
		
		} else {

			$result = "Status Code: ".$status_code;
			$respmsg = translate_status_code($status_code);						
		}

		$card_type = getCardTypeFromNumber($cn);

		//record_stats($company_id, $data_name, $location_id, "Magensa", $reader, $transtype, $result, $respmsg, $card_type, $swiped);

		$debug .= "MAGENSA\n\n".$request."\n\nRESULT: ".$result."\nRESPONSE MESSAGE: ".$respmsg."\n".$authcode."\n".$new_pnref;
		
		$approved = "0";
		if (($status_code == "1000") && (($result == "0") || ($result == "111"))) $approved = "1";
		if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "MAGENSA", $transtype, $approved, $debug);
		
		$log_string = "MAGENSA[--CR--]";
		$log_string .= "LOCATION: ".$process_info['loc_id']." - ";
		$log_string .= "REGISTER: ".$process_info['register']." - ";
		$log_string .= "SERVER ID: ".$process_info['server_id']." - ";
		$log_string .= "ORDER ID: ".$process_info['order_id']." - ";
		$log_string .= "AMOUNT: ".$send_card_amount." - ";
		$log_string .= "TRANSTYPE: ".$transtype." - ";
		$log_string .= "RESULT: ".$result." - ";
		$log_string .= "PNREF: ".$new_pnref." - ";
		$log_string .= "AUTHCODE: ".$authcode." - ";
		$log_string .= "CARD TYPE: ".$card_type." - ";
		$log_string .= "CARD DESC: ".substr($cn, -4)." - ";
		$log_string .= "SWIPED: ".$swiped."[--CR--]";
		$log_string .= "EXT_DATA: ".$ext_data."[--CR--]";
		$log_string .= "RESPONSE MESSAGE: ".$respmsg."[--CR--]";
		write_log_entry("gateway", $process_info['data_name'], $log_string."\n");
		
		$rtn = array();
		
		if ($approved == "1") {

			$process_info['last_mod_ts'] = time();

			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['order_id'];
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['last_mod_ts'];

		} else {

			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

			if ($respmsg == "") $respmsg = "Error - No response received from gateway.";

			$rtn[] = "0";
			$rtn[] = $respmsg;
			$rtn[] = $new_pnref;
			$rtn[] = $process_info['order_id'];
		}
		
		return implode("|", $rtn);
	}

function process_magensa($process_info, $location_info) {

	$debug = print_r($process_info, true)."\n\n";

	$card_number = $process_info['card_number'];
	$mag_data = $process_info['mag_data'];

	$cn = $card_number; // keep for response string
	$first_four = substr($cn, 0, 4);

	$swiped = "0";
	if ($mag_data) $swiped = "1";

	$dukpt_ksn = "";
	if ($mag_data && $process_info['reader']=="idynamo") {
		$e2e = true;
		$split_mag_data = explode("|", $mag_data);
		$encrypted_track1 = $split_mag_data[2];
		$encrypted_track2 = $split_mag_data[3];
		$encrypted_track3 = $split_mag_data[4];
		$encrypted_mp = $split_mag_data[6];
		$dukpt_ksn = $split_mag_data[9];
		$mp_status = $split_mag_data[5];

		$process_type = "ProcessCS10";

	} else {

		$e2e = false;
		$process_type = "ProcessKE10";
	}

	if ($process_info['set_pay_type'] != "") {
		$pay_type = $process_info['set_pay_type'];
		$pay_type_id = $process_info['set_pay_type_id'];
	} else {
		$pay_type = "Card";
		$pay_type_id = "2";
	}
	$send_card_amount = $process_info['card_amount'];

	$transtype = $process_info['transtype'];
	if ($transtype == "") $transtype = "Sale";

	switch ($transtype) {
		case "Sale" :
			$qptrantype = "S";
			$check_for_duplicate = true;
			break;
		case ($transtype=="Auth" || $transtype=="AuthForTab") :
			$qptrantype = "A";
			$check_for_duplicate = true;
			if ($process_info['order_id'] == "0") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
			if ($transtype=="AuthForTab" && (float)$send_card_amount==0) {
				$process_info['card_amount'] = $location_info['default_preauth_amount'];
				$send_card_amount = $process_info['card_amount'];
			}
			break;
		case ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") :
			$qptrantype = "D";
			$process_type = "ProcessTI10";
			break;
		case "Void" :
			$qptrantype = "V";
			$process_type = "ProcessTI10";
			$send_card_amount = "";
			$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$card_type = $transaction_info['card_type'];
			$first_four = $transaction_info['first_four'];
			$cn = $transaction_info['card_desc'];
			// Magensa allows Auths to be voided
			break;
		case "Return" :
			$qptrantype = "R";
			$send_card_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
			break;
		default: break;
	}

	$transaction_vars = generate_transaction_vars($process_info, $location_info, $cn, $card_type, $transtype, $swipe_grade, $pay_type, $pay_type_id, $send_card_amount);


	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	$system_id = 0;
	if ($transtype != "PreAuthCapture") {
		$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
		if (!$preliminary_record_transaction) return "0|Failed to create transaction record.||".$process_info['order_id'];
		$system_id = lavu_insert_id();
	}

	$host_id = "MPH013756870";
	$host_pw = "a@K6ZHM82!m7gp";

	// MPM345713279
	// m!j5#7YhuQ63fo

	$name_on_card = "None entered";

	if (in_array($transtype, array("Void", "PreAuthCapture", "PreAuthCaptureForTab"))) {
		$request = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><".$process_type." xmlns=\"http://www.magensa.net/\"><".$process_type."_Input><TransactionType>$qptrantype</TransactionType><HostID>$host_id</HostID><HostPW>$host_pw</HostPW><MerchantID>".$process_info['username']."</MerchantID><MerchantPW>".$process_info['password']."</MerchantPW><TransactionID>".$process_info['pnref']."</TransactionID><Amt>$send_card_amount</Amt></".$process_type."_Input></".$process_type."></soap:Body></soap:Envelope>";
	} else {
		if ($e2e) $request = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><".$process_type." xmlns=\"http://www.magensa.net/\"><".$process_type."_Input><TransactionType>$qptrantype</TransactionType><HostID>$host_id</HostID><HostPW>$host_pw</HostPW><MerchantID>".$process_info['username']."</MerchantID><MerchantPW>".$process_info['password']."</MerchantPW><EncryptedTrack1>$encrypted_track1</EncryptedTrack1><EncryptedTrack2>$encrypted_track2</EncryptedTrack2><EncryptedTrack3></EncryptedTrack3><EncryptedMP>$encrypted_mp</EncryptedMP><KSN>$dukpt_ksn</KSN><MPStatus>$mp_status</MPStatus><Amt>$send_card_amount</Amt><TaxAmt></TaxAmt><PurchaseOrder></PurchaseOrder><InvoiceNumber>".str_replace("-", "", $location_id.$order_id)."</InvoiceNumber><CVV></CVV><ZIP></ZIP></".$process_type."_Input></".$process_type."></soap:Body></soap:Envelope>";
		else $request = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><".$process_type." xmlns=\"http://www.magensa.net/\"><".$process_type."_Input><TransactionType>$qptrantype</TransactionType><HostID>$host_id</HostID><HostPW>$host_pw</HostPW><MerchantID>".$process_info['username']."</MerchantID><MerchantPW>".$process_info['password']."</MerchantPW><PAN>$card_number</PAN><CHName>$name_on_card</CHName><CardExpDt>".$process_info['exp_month'].$process_info['exp_year']."</CardExpDt><CVV>".$process_info['card_cvn']."</CVV><Amt>$send_card_amount</Amt><TaxAmt></TaxAmt><PurchaseOrder></PurchaseOrder><InvoiceNumber>".str_replace("-", "", $process_info['loc_id'].$process_info['order_id'])."</InvoiceNumber><ZIP></ZIP></".$process_type."_Input></".$process_type."></soap:Body></soap:Envelope>";
	}
	$curl_result = post_to_magensa($request, $process_type, $process_info['data_name'], $transtype, $location_info);

	$status_code = extract_value($curl_result, "StatusCode");

	$result = "1";
	$new_pnref = extract_value($curl_result, "TransactionID");

	if ($status_code == "1000") {

		$result = extract_value($curl_result, "TransactionStatus");
		$respmsg = translate_transaction_status($result, extract_value($curl_result, "TransactionMsg"));
		$authcode = extract_value($curl_result, "AuthCode");

	} else if (strstr($curl_result, "Server was unable to process request")) {

		$respmsg = "Server was unable to process request...";

	} else {

		$result = "Status Code: ".$status_code;
		$respmsg = translate_status_code($status_code);
	}

	$card_type = getCardTypeFromNumber($cn);

	//record_stats($company_id, $data_name, $location_id, "Magensa", $reader, $transtype, $result, $respmsg, $card_type, $swiped);

	$debug .= "MAGENSA\n\n".$request."\n\nRESULT: ".$result."\nRESPONSE MESSAGE: ".$respmsg."\n".$authcode."\n".$new_pnref;

	$approved = "0";
	if (($status_code == "1000") && (($result == "0") || ($result == "111"))) $approved = "1";
	if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "MAGENSA", $transtype, $approved, $debug);

	$log_string = "MAGENSA[--CR--]";
	$log_string .= "LOCATION: ".$process_info['loc_id']." - ";
	$log_string .= "REGISTER: ".$process_info['register']." - ";
	$log_string .= "SERVER ID: ".$process_info['server_id']." - ";
	$log_string .= "ORDER ID: ".$process_info['order_id']." - ";
	$log_string .= "AMOUNT: ".$send_card_amount." - ";
	$log_string .= "TRANSTYPE: ".$transtype." - ";
	$log_string .= "RESULT: ".$result." - ";
	$log_string .= "PNREF: ".$new_pnref." - ";
	$log_string .= "AUTHCODE: ".$authcode." - ";
	$log_string .= "CARD TYPE: ".$card_type." - ";
	$log_string .= "CARD DESC: ".substr($cn, -4)." - ";
	$log_string .= "SWIPED: ".$swiped."[--CR--]";
	$log_string .= "EXT_DATA: ".$ext_data."[--CR--]";
	$log_string .= "RESPONSE MESSAGE: ".$respmsg."[--CR--]";
	write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

	$rtn = array();

	if ($approved == "1") {

		$process_info['last_mod_ts'] = time();

		update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

		$rtn[] = "1"; // 0
		$rtn[] = $new_pnref;
		$rtn[] = substr($cn, -4);
		$rtn[] = $respmsg;
		$rtn[] = $authcode;
		$rtn[] = $card_type;
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = $process_info['order_id'];
		$rtn[] = "";
		$rtn[] = ""; // 10
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = "";
		$rtn[] = $process_info['last_mod_ts'];

	} else {

		$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

		if ($respmsg == "") $respmsg = "Error - No response received from gateway.";

		$rtn[] = "0";
		$rtn[] = $respmsg;
		$rtn[] = $new_pnref;
		$rtn[] = $process_info['order_id'];
	}

	return implode("|", $rtn);
}
?>