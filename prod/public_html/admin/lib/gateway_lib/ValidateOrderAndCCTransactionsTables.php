<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 9/8/16
 * Time: 2:49 PM
 *
 * ValidateOrderAndCCTransactionsTables
 *      This class can be used to make sure the cc_transactions table and the orders table are consistent and valid
 *      before performing a Return transaction.
 */
class ValidateOrderAndCCTransactionsTables
{
	private $returnAmountToBeTransacted;
	private $orderID;
	private $authCode;

	private $ordersRow;
	private $ccTransactionsRows;

	/**
	 * Date: 9/9/2016
	 * Developer: Leif Guillermo
	 *
	 * __construct
	 *      populates the member variable of the CheckOrderTables object. The order ID, Return Amount to be transacted,
	 *      orders table row, and cc_transactions table rows are populated.
	 */
	public function __construct($returnAmountToBeTransacted=null, $orderID=null, $authCode=null, $location_id=null)
	{
		$this->returnAmountToBeTransacted = $returnAmountToBeTransacted;
		$this->orderID = $orderID;
		$this->authCode = $authCode;
		$this->location_id = $location_id;
	}

	/**
	 * Date: 9/9/2016
	 * Developer: Leif Guillermo
	 *
	 * validateReturnTransaction
	 *      Runs all of the methods in the CheckOrderTables class which deal with making sure the return transaction
	 *      is valid.
	 *
	 * Returns:
	 *      true if successful.
	 *      status/error message string if results show that the return should not be performed.
	 */
	public function validateReturnTransaction(){
		//Perform mysql queries to get orders and cc_transasction rows associated with this object's order_id value.
		$queryResult = $this->populateOrdersAndCCTransactionsRows($this->orderID);
		if($queryResult !== true){ return $queryResult; }

		//Verify that the cc_transactions table is consistent regarding the rows associated with this object's order_id value and returns/voids/sales.
		$ccTransactionsConsistencyResult = $this->checkConsistencyOfCCTransactionsTable($this->ccTransactionsRows, $this->authCode);
		if($ccTransactionsConsistencyResult !== true){ return $ccTransactionsConsistencyResult; }

		//Get sum/difference of sale, void and refund transactions from cc_transactions table

		//Changed/Added by Chris Aitken
		//$ccTransactionSum = $this->sumSaleVoidAndRefundTransactionsFromCCTransactionsTable($this->ccTransactionsRows);
		//Verify that the total sums between the orders table and the cc_transactions table are coherent.
        $orderTotal = 0;
        $orderTotal += floatval($this->ordersRow['card_paid'] . "");
        $orderTotal += floatval($this->ordersRow['card_gratuity'] . "");
        $ccTotal =  0;
        $ccTotal += floatval($this->ccTransactionsRows[0]['amount']."");
        $ccTotal += floatVal($this->ccTransactionsRows[0]['tip_amount'] . "");
		//End of Changes/Additions by Chris Aitken.

		//Removing this function because it's very difficult to ensure that the tables will be consistent due to rounding issues etc.
		//Also, this should be done during syncing anyway, not during transactions.
//		$ccTransactionsCompareWithOrdersSumResult = $this->compareCCTransactionSumAgainstOrdersTableTotal($ccTotal, $orderTotal);
//		if($ccTransactionsCompareWithOrdersSumResult !== true){ return $ccTransactionsCompareWithOrdersSumResult; }

		//Changed/Added by Chris Aitken
		$transactionTotal = floatval($this->ordersRow['total'] . "");
		//Verify that the amount requesting to be returned is a valid amount (doesn't exceed amount left on order, nor is it less than or equal to 0).
		$verifyReturnAmountResult = $this->checkReturnAmountAgainstOrdersTable($this->returnAmountToBeTransacted, $transactionTotal);
		//End of Changes/Additions by Chris Aitken.

		if($verifyReturnAmountResult !== true){ return $verifyReturnAmountResult; }

		return true;
	}
 
	/**
	 * Date: 9/9/2016
	 * Developer: Leif Guillermo
	 *
	 * populateOrdersAndCCTransactionsRows
	 *      pulls in data from the orders and cc_transactions tables. Returns true if successful, or an error message if
	 *      unsuccessful.
	 *
	 * Returns:
	 *      true if successful.
	 *      status/error message string if database queries fail or if results show that the return should not be
	 *      performed.
	 */
	private function populateOrdersAndCCTransactionsRows($orderID){
		//perform queries to get cc_transactions and orders rows.
		$orders_row_query_result = lavu_query("SELECT * FROM `orders` WHERE `orders`.`order_id` ='" . $orderID . "' AND `orders`.`location_id` = " . $this->location_id);
		$cc_transactions_rows_query_result = lavu_query("SELECT * FROM `cc_transactions` WHERE `cc_transactions`.`order_id` = '" . $orderID . "' AND `cc_transactions`.`auth_code` = '".$this->authCode."' AND `refunded` = '0' AND `voided` = '0'");

		//Check for any mysql errors and return those errors if they occurred.
		if(lavu_dberror($orders_row_query_result)){
			return __FILE__ . __LINE__ . " MySQL Error: " . lavu_dberror($orders_row_query_result);
		}
		if(lavu_dberror($cc_transactions_rows_query_result)){
			return __FILE__ . __LINE__ . " MySQL Error: " . lavu_dberror($cc_transactions_rows_query_result);
		}

		//Get the number of rows returned for the cc_transactions table and the orders table
		$num_orders_rows = mysqli_num_rows($orders_row_query_result);
		$num_cc_transactions_rows = mysqli_num_rows($cc_transactions_rows_query_result);

		//If the number of rows is somehow not a valid number of rows, return an error message.
		if($num_orders_rows <= 0 || $num_cc_transactions_rows <= 0){
			return __FILE__ . __LINE__ . " The orders Table contains '" . $num_orders_rows . "' rows, and cc_transactions table contains '" . $num_cc_transactions_rows . "' rows associated with the queried order_id: " . $orderID . ", and Auth-Code: " . $this->authCode;
		}
		if($num_orders_rows > 1){
			return __FILE__ . __LINE__ . " There exists more than one row in the orders table associated with the queried order_id: " . $orderID;
		}

		//Added by Chris Aitken: Makes sure that an array of rows is always returned (not an array of a row's items).
		$ccTransArray = array();
        if($num_cc_transactions_rows === 1){
            $ccTransArray[] = mysqli_fetch_assoc($cc_transactions_rows_query_result);
        }
        else $ccTransArray = mysqli_fetch_assoc($cc_transactions_rows_query_result);
		//End of Chris Aitken's addition.

		//Set this object's member variables, pertaining to the rows of the two queried tables.
		$this->ordersRow = mysqli_fetch_assoc($orders_row_query_result);

		$this->ccTransactionsRows = $ccTransArray;

		return true;
	}

	//Chris Aitken removed
//	/**
//	 * Date: 9/8/2016
//	 * Developer: Leif Guillermo
//	 *
//	 * checkPostedAuthCodeForExistenceInCCTransactionsTable
//	 *      Checks to if there is an authcode value in the cc_transactions table that matches the authcode associated
//	 *      with this object's authCode value.
//	 *
//	 * Returns:
//	 *      True if an authcode is matched.
//	 *      Error message string if no match was found.
//	 */
//	private function checkPostedAuthCodeForExistenceInCCTransactionsTable($cc_transactions_rows, $authCode){
//		//loop through the cc_transaction rows. If the authcode is found return true, else return an error.
//		foreach($cc_transactions_rows as $row){
//			if($row['auth_code'] == $authCode){
//				return true;
//			}
//		}
//		return __FILE__ . " " . __LINE__ . " Could not find auth_code in the cc_transactions table associated with the posted authcode.";
//	}
//
//	/**
//	 * Date: 9/9/2016
//	 * Developer: Leif Guillermo
//	 *
//	 * sumSaleVoidAndRefundTransactionsFromCCTransactionsTable
//	 *      Sums/subtracts  the total amount sold/voided/returned from all cc_transaction rows associated with this object's order_id value.
//	 *
//	 * Parameters:
//	 *      $cc_transactions_rows
//	 *          An array of associative arrays representing the rows in the cc_transactions table associated with this object's order_id value.
//	 *
//	 * Returns:
//	 *      Sum of sales, voids, and refunds from the cc_transactions table associated with this object's order_id value.
//	 */
//	private function sumSaleVoidAndRefundTransactionsFromCCTransactionsTable($cc_transactions_rows){
//		$sum = 0;
//		$field_to_compare = 'total_collected';
//		foreach($cc_transactions_rows as $row){ //Loop through the cc_transaction rows
//			if($row['voided'] == '0') { //If the transaction has not been voided add/subtract the row's total value
//				switch ($row['action']) {
//					case 'Sale':
//						$sum += floatval($row[$field_to_compare] . "");
//						break;
//					case 'Void':
//						$sum -= floatval($row[$field_to_compare] . "");
//						break;
//					case 'Refund':
//						$sum -= floatval($row[$field_to_compare] . "");
//						break;
//					default:
//						break;
//				}
//			}
//		}
//		return $sum;
//	}
	//End of Chris Aitken's removal.

	/**
	 * Date: 9/9/2016
	 * Developer: Leif Guillermo
	 *
	 * compareTotalsFromCCTransactionsAndOrdersTable
	 *      Verifies that the cc_transactions table is consistent regarding the rows associated with this object's
	 *      order_id value and returns/voids/sales.
	 *
	 * Returns:
	 *      True if everything matches correctly.
	 *      Error message string if there are any discrepancies.
	 */
	private function checkConsistencyOfCCTransactionsTable($cc_transactions_rows, $authCode)
	{
		//check pn_ref for all voids against original transaction_id;
		$void_pnref_check_result = $this->checkPNREFAgainstTransactionIDs($cc_transactions_rows, 'Void','void_pnref', 'transaction_id');
		if($void_pnref_check_result !== true){
			return $void_pnref_check_result;
		}

		//look for transactions with refund_pnref = original internal_id
		$return_pnref_check_result = $this->checkPNREFAgainstTransactionIDs($cc_transactions_rows, 'Return','refund_pnref', 'internal_id');
		if($return_pnref_check_result !== true){
			return $return_pnref_check_result;
		}

		//Chris Aitken removed the old auth-code check.
		return true;
	}
	/**
	 * Date: 9/9/2016
	 * Developer: Leif Guillermo
	 *
	 * checkVoidPNREFAgainstTransactionIDs
	 *      Compare all voided cc_transactions against cc_transactions with the same order_id to see if the void_pnref
	 *      is associated with a valid transaction_id.
	 *
	 * Parameters:
	 *      $cc_transaction_rows
	 *          An array of associative arrays representing all cc_transaction rows associated with this object's
	 *          order_id value.
	 *      $action
	 *          The action to check pnref and id of. This can be 'Void' or 'Return'
	 *      $pnref_field
	 *          The type of pnref field. This can be void_pnref or return_pnref.
	 *      $id_field
	 *          The type of id field. This can be transaction_id or internal_id.
	 *
	 * Returns:
	 *      True if all voids are associated with a transaction from the same order, or an error message if not.
	 */
	private function checkPNREFAgainstTransactionIDs($cc_transaction_rows, $action, $pnref_field, $id_field){
		foreach($cc_transaction_rows as $row){
			//If there is a void, we need to look and see if there is a matching id for it's pnref.
			if($row['action'] == $action){
				$foundMatch = false;
				//Loop through all of the rows to see if the transaction_id exists.
				foreach($cc_transaction_rows as $row_for_transaction_id_check){
					//If we find a match, make a note of it and break from the inner for-loop
					if($row[$pnref_field] == $row_for_transaction_id_check[$id_field]){
						$foundMatch = true;
						break;
					}
				}
				//If we didn't find a match return an error message with some info.
				if(!$foundMatch) {
					return "Error, no transaction id associated with pnref for ".$action."ed order. pnref: " . $row[$pnref_field] . ", order_id: " . $this->orderID;
				}
			}
		}
		return true;
	}

//	/**
//	 * Date: 9/9/2016
//	 * Developer: Leif Guillermo
//	 *
//	 * compareCCTransactionSumAgainstOrdersTableSum
//	 *      Check to see if the cc_transactions table is coherent with the orders table. If so return true, if not
//	 *      return an error message
//	 *
//	 * Parameters:
//	 *      $cc_transaction_rows
//	 *          An array of associative arrays representing all cc_transaction rows associated with this object's
//	 *          order_id value.
//	 *
//	 * Returns:
//	 *      true if cc_transactions and orders totals are consistent.
//	 *      status/error message string if the cc_transactions table totals and orders table total do not match.
//	 */
//	public function compareCCTransactionSumAgainstOrdersTableTotal($ccTransactionSum, $orders_row_total_collected){
//		if(floatval($orders_row_total_collected."") !== floatval($ccTransactionSum."")){
//			return __FILE__ . " " . __LINE__ . ": cc_transactions and orders tables totals are inconsistent with each other";
//		}
//		return true;
//	}

	/**
	 * Date: 9/9/2016
	 * Developer: Leif Guillermo
	 *
	 * redundancyCheckForReturnAmountCorrectness
	 *      will check an incoming return transaction and ensure that it only
	 *      goes through if the amount to be returned is less than or equal to the original transaction amount, and greater
	 *      than or equal to $0.00.
	 *
	 * Parameters:
	 *      $return_amount_to_be_transacted
	 *          Return amount being requested for the transaction.
	 *      $orders_row_total_collected
	 *          Total amount remaining on the order. This should be retrieved from the orders table.
	 *
	 * Returns:
	 *      true if transaction should be performed.
	 *      status/error message string if transaction amount is above the original amount or less than 0.
	 */
	public function checkReturnAmountAgainstOrdersTable($return_amount_to_be_transacted, $orders_row_total_collected)
	{
		$return_amount_to_be_transacted_float_value = floatval($return_amount_to_be_transacted . "");

		//Pull information from the database's orders table about the original transaction. If the row does not exist, return false.
		$total_collected = floatval($orders_row_total_collected . "");

		//Check to make sure if the return transaction should be successful. If so, return true. If not, the function returns false.
		if (($total_collected >= 0) && ($return_amount_to_be_transacted_float_value <= $total_collected) && ($return_amount_to_be_transacted_float_value >= 0)) {
			return true;
		}

		//If we make it this far, something is wrong, so return an error message.
		$return_value = "Return amount is too much or too little";
		$return_value .= "\n     Return Amount: " . $return_amount_to_be_transacted;
		$return_value .= "\n     Amount available in the orders table: " . $orders_row_total_collected;
		return $return_value;
	}
}
