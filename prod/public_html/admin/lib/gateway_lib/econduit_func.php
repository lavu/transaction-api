<?php

require_once(__DIR__."/../gateway_functions.php");

/*
 *
 * econduit payment gateway
 *
 */
class eConduitGateway
{
	private $order_id;
	private $ref_id;
	private $authcode;
	private $record_no;
	private $ref_data;
	private $process_data;

	private $base_amount;
	private $capture_amount;
	private $tip_amount;

	private $send_vars;

	public function processTransaction($process_info, $location_info)
	{
		error_log("eConduitGateway process_info: ".json_encode($process_info));

		$this->order_id			= $process_info['order_id'];
		$this->ref_id			= $process_info['pnref'];
		$this->authcode			= $process_info['authcode'];
		$this->record_no		= $process_info['record_no'];
		$this->ref_data			= $process_info['ref_data'];
		$this->process_data		= $process_info['process_data'];

		$this->base_amount		= $process_info['base_amount'];
		$this->capture_amount	= $process_info['capture_amount'];
		$this->tip_amount		= $process_info['tip_amount'];

		$this->send_vars = array(
			'key'			=> $process_info['integration9'],
			'password'		=> $process_info['integration10'],
			'refID'			=> $this->ref_id,
			'terminalId'	=> $this->ref_data
		);

		if (!empty($this->record_no))
		{
			$this->send_vars['expDate']			= "";
			$this->send_vars['invoiceNumber']	= $this->order_id;
			$this->send_vars['merchantId']		= "";
			$this->send_vars['token']			= $this->record_no;
		}

		$transtype = $process_info['transtype'];
		switch ($transtype)
		{
			case "Adjustment":

				$this->TipAdjustment();
				break;

			case "ReauthorizePreAuth":

				$this->ReauthorizePreAuth();
				break;

			case "PreAuthCapture":
			case "PreAuthCaptureForTab":

				$this->PreAuthCapture();
				break;

			default:

				return "0|Error: Unsupported transtype (".$transtype.")...";
		}

		return $this->getResponseFromGateway();
	}

	/**
	 * @param string $type
	 * @param array $request_params
	 * @return mixed
	 */
	private function TipAdjustment()
	{
		$this->send_vars['amount']	= number_format(($this->base_amount + $this->tip_amount), 2, ".", "");
		$this->send_vars['command']	= "tipadjust";
	}

	private function ReauthorizePreAuth()
	{

		return "0|ReauthorizePreAuth coming soon!";
	}

	private function PreAuthCapture()
	{
		$this->send_vars['amount']	= number_format(($this->capture_amount + $this->tip_amount), 2, ".", "");
		$this->send_vars['command']	= "capture";
	}

	private function sendVarsString()
	{
		$vars_string = "";
		foreach ($this->send_vars as $key => $value)
		{
			$vars_string .= $key."=".urlencode($value)."&";
		}

		return rtrim($vars_string, "&");
	}

	private function getResponseFromGateway()
	{
		$target_url = "https://econduitapp.com/services/api.asmx/runTransaction?".$this->sendVarsString();
		error_log("target_url: ".$target_url);
		try
		{
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $target_url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlCall, CURLOPT_CONNECTTIMEOUT, 60);
			curl_setopt($curl, CURLOPT_TIMEOUT, 60);
			$result_json = curl_exec($curl);
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close ($curl);
			if ($httpcode != "200")
			{
				return formattedGatewayDecline("Error", "Failed to connect to eConduit.", $this->order_id, "", "HTTP Code ".$httpcode);
			}
		}
		catch (Exception $e)
		{
			return formattedGatewayDecline("Error", "Failed to connect to eConduit.", $this->order_id);
		}
		error_log("result_json: ".$result_json);
		$result = json_decode($result_json);

		$result_code	= $result->ResultCode;
		$message		= $result->Message;
		$card_type		= $result->CardType;

		$response = "";

		switch ($result_code)
		{
			case "Approved":

				$response = formattedGatewayApproval($this->ref_id, $result->Last4, $result_code, $this->authcode, $card_type, $this->record_no, "", $this->order_id, $this->ref_data, $this->process_data);
				break;

			default:

				$response = formattedGatewayDecline("Declined", $message, $this->order_id, $this->ref_id, $result_code);
				break;
		}

error_log("response: ".$response);

		return $response;
	}
}
