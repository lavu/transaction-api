<?php
	function translate_response($respmsg) {

		switch ($respmsg) {
			case "AP" : $return_msg = "Approved"; break;
			case "CALL AE" : $return_msg = "Please call AE for voice authorization code."; break;
			case "CALL DI" : $return_msg = "Please call DI for voice authorization code."; break;
			case "CALL ND" : $return_msg = "Please call ND for voice authorization code."; break;
			case "EDC UNAVAILABLE" : $return_msg = "EDC application down.\nPlease try again later."; break;
			case "Merchant Setting Requires RecordNo and Frequency." : $return_msg = "Gateway requires card to be swiped."; break;
			case "Merchant Setting Requires Encrypted Data." : $return_msg = "Gateway requires card to be swiped."; break;
			case "Decryption Service Unavailable. - 200 Response With Status:Failure Message:Failed to decrypt" : $return_msg = "Decryption failure. Please try again."; break;
			default: $return_msg = $respmsg; break;
		}

		return $return_msg;
	}

	function post_to_mercury($postvars, $password, $TorA, $data_name, $loc_id, $transtype, $pay_type, $special_designator, $gateway_debug, $reader="", $failover=false)
	{
		global $mercury_server_url;
		global $mercury_secondary_url;
		global $mercury_gift_card_server_url;
		
		if ($failover) error_log("using Mercury's secondary server: ".$data_name);

		$http = "https";
		$webservice = "/ws/ws.asmx";
		if ($pay_type=="Gift Card" || $special_designator=="gift_card")
		{
			$server_url = $mercury_gift_card_server_url;
			$method = "GiftTransaction";
			$port = "";
		}
		else
		{
			if ($failover)
			{
				$server_url = $mercury_secondary_url;
			}
			else
			{
				$server_url = $mercury_server_url; //$mercury_server_url;
			}
			$method = "CreditTransaction";
			$port = "";
		}

		$poststr = "";
		for($i=0; $i<count($postvars); $i++) {
			$field_title = $postvars[$i][0];
			$field_value = $postvars[$i][1];
			$poststr .= "&lt;".$field_title."&gt;".$field_value."&lt;/".$field_title."&gt;\n";
		}

		$start_data = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
			."<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
			."<soap:Body>\n"
			."<".$method." xmlns=\"http://www.mercurypay.com\">\n"
			."<tran>\n"
			."&lt;TStream&gt;&lt;".$TorA."&gt;\n";
		$end_data = "&lt;/".$TorA."&gt;\n"
			."&lt;/TStream&gt;</tran>\n"
			."<pw>".$password."</pw>\n"
			."</".$method.">\n"
			."</soap:Body>\n"
			."</soap:Envelope>\n";
		$post_data = $start_data.$poststr.$end_data;

		$header = array("POST ".$webservice."?WSDL HTTP/1.1", "Host: ".$server_url, "Content-Type: text/xml; charset=utf-8", "SOAPAction: \"http://www.mercurypay.com/".$method."\"");

		ConnectionHub::closeAll();

		$curl_start = microtime(true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $http."://".$server_url.$webservice);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		$response = curl_exec($ch);
		//$my_curl_info = curl_getinfo($ch); // for GatewayResponseTimingLogger
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$curl_error = curl_error($ch);
		curl_close($ch);

		/*global $gwtl; ; // for GatewayResponseTimingLogger
		@$gwtl->GatewayName = 'Mercury';
		@$gwtl->ResponseSpeed = $my_curl_info['total_time'];
		@$gwtl->TransType = $transtype;*/
		
		$curl_end = microtime(true);
		$curl_time = round((($curl_end - $curl_start) * 1000), 2) . "ms";

		$start_tag = "<".$method."Result>";
		$end_tag = "</".$method."Result>";

		$start_pos = (strpos($response, $start_tag) + strlen($start_tag));
		$end_pos = strpos($response, $end_tag);

		$curl_result = htmlspecialchars_decode(substr($response, $start_pos, ((strlen($response) - $start_pos)) - (strlen($response) - $end_pos)));

		if ($gateway_debug == "1") {
			gateway_debug($data_name, $loc_id, "MERCURY", $transtype, "XML", "URL: ".$server_url."\n\ncURL time: ".$curl_time."\n\nPOST_DATA: ".htmlspecialchars_decode($post_data)."\n\nRESPONSE: ".htmlspecialchars_decode($response)."\n\n\nRESULT: ".$curl_result);
			error_log("received gateway response: ".$data_name." - ".$_REQUEST['YIG']." - ".$curl_time);
		}
		
		if (!$failover && in_array((int)$http_code, array(0, 404, 408, 500, 503))) $curl_result = post_to_mercury($postvars, $password, $TorA, $data_name, $loc_id, $transtype, $pay_type, $special_designator, $gateway_debug, $reader, true);

		return $curl_result;
	}

function process_mercury($process_info, $location_info) {

	// approval test return "1|1234567890|1234|Yay!|123ABC|TEST|0987654321|".$process_info['card_amount']."|".$process_info['order_id']."|REF_DATA|".rawurlencode("PROCESS_DATA");

	// return '0|Invalid Parameter CALL 800-846-4472|11-1|1-1||Error';

	global $poslavu_version;
	global $poslavu_build;
	global $server_order_prefix;
	global $mercury_server_url;

	$debug = print_r($process_info, true)."\n\nurl: ".$mercury_server_url."\n\n";

	$transtype = $process_info['transtype'];
	$tip_amount = $process_info['tip_amount'];

	$TorA = "Transaction";
	$e2e = false;

	$pay_type = $process_info['set_pay_type'];
	$pay_type_id = $process_info['set_pay_type_id'];
	$special_designator = specialDesignatorForPayTypeID($pay_type_id);

	$send_amount = $process_info['card_amount'];
	$log_tip_amount = "";

	$mtrantype = ($pay_type=="Gift Card" || $special_designator=="gift_card")?"PrePaid":"Credit";

	$cn = $process_info['card_number']; // keep for response string
	$first_four = substr($cn, 0, 4);
	$card_type = getCardTypeFromNumber($cn);

	$swiped = "0";
	$swipe_grade = "H";
	$send_mag_data = "";
	if ($process_info['mag_data']) {
		$swiped = "1";
		$swipe_grade = "G";
		$send_mag_data = $process_info['mag_data'];
	}

	$track2 = "";
	$dukpt_ksn = "";
	if ($process_info['mag_data'] && ($process_info['reader']=="idynamo" || $process_info['reader']=="udynamo")) {
		$chr1 = substr($process_info['mag_data'], 0, 1);
		$valid_mag_data = (substr($process_info['mag_data'], 0, 2)=="%B" || (($chr1=="%" || $chr1==";") && $pay_type_id!="2"));
		$e2e = true;
		$split_mag_data = explode("|", $process_info['mag_data']);
		$encrypted_track1 = $split_mag_data[2];
		$encrypted_track2 = $split_mag_data[3];
		$dukpt_ksn = $split_mag_data[9];

		if (!strstr($encrypted_track1, "E?") && ($encrypted_track1 != "")) $swipe_grade = "C";
		if (!strstr($encrypted_track2, "E?") && ($encrypted_track2 != "")) {
			if ($swipe_grade == "C") $swipe_grade = "A";
			else $swipe_grade = "B";
		}

		if (!$valid_mag_data || strstr($encrypted_track2, "E?") || (strlen($encrypted_track2) < 5) || strstr($dukpt_ksn, "E?") || empty($dukpt_ksn)) {

			if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "MERCURY", $transtype, "BR", $debug);

			//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "Mercury", $process_info['reader'], $transtype, "99999", "Read Error - Missing track 1 and 2", "", $swiped, $pay_type);

			$log_string = "MERCURY[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: ".$process_info['card_amount']." - TRANSTYPE: $transtype - CARD TYPE: ??? - CARD DESC: ".substr($cn, -4)." - SWIPED: ".$swiped."[--CR--]MAG DATA: ".$process_info['mag_data']."[--CR--]";
			write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

			return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."||Error|||";
		}

		$track2 = $encrypted_track2;

	} else if ($process_info['mag_data'] && $process_info['encrypted']=="1") {

		$e2e = true;
		$swipe_grade = "B";
		$encrypted_info = idtech_encrypted($process_info['mag_data'], $process_info, $location_info);
		$track2 = hex_string($encrypted_info['track2']);
		$dukpt_ksn = hex_string($encrypted_info['ksn']);
		if (empty($track2) || empty($dukpt_ksn)) {
			if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $process_info['loc_id'], "MERCURY", $transtype, "BR", $debug);
			$log_string = "MERCURY[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: ".$process_info['card_amount']." - TRANSTYPE: $transtype - CARD TYPE: ??? - CARD DESC: ".substr($cn, -4)." - SWIPED: ".$swiped."[--CR--]MAG DATA: ".$process_info['mag_data']."[--CR--]";
			write_log_entry("gateway", $process_info['data_name'], $log_string."\n");
			return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."|||||";
		}

	} else if ($process_info['mag_data'] && $process_info['encrypted']=="2") {

		require_once(dirname(__FILE__)."/mercury_lls_func.php");
		$e2e = true;
		$swipe_grade = "B";
		$myLLSSwipe = new IDTechLLS();
		$myLLSSwipe->setMagData($process_info['mag_data']);
		$encrypted_info = $myLLSSwipe->getEncryptedInfoBlock();
		if(empty($encrypted_info['track2'])){
			return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."|||||";
		}
		$track2 = $encrypted_info['track2'];
		$dukpt_ksn = $encrypted_info['ksn'];

	} else if ($process_info['mag_data']) {

		$tracks = explode("?", $process_info['mag_data']);
		$track2_index = 1;
		$swipe_grade = "D";
		if (strstr($tracks[0], "=")) {
			$track2_index = 0;
			$swipe_grade = "E";
		}
		if (count($tracks) > $track2_index) {
			$track2 = $tracks[$track2_index];
			if ($track2!="" && $track2!="ERROR") $send_mag_data = str_replace(";", "", $track2);
			else $send_mag_data = "";
		}
	}

	$cancel_logging = false;
	$check_for_duplicate = false;
	$create_transaction_record = false;
	$got_account_info = false;
	$include_tran_info = false;
	if ($transtype == "") $transtype = "Sale";

	switch ($transtype) {
		case "Sale" :
			$trancode = "Sale";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$got_account_info = true;
			$include_tran_info = true;
			break;
		case "Auth" :
		case "AuthForTab" :
			$trancode = "PreAuth";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$got_account_info = true;
			$include_tran_info = true;
			if ($process_info['order_id']=="0" || $process_info['order_id']=="") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
			if ($transtype=="AuthForTab" && (float)$send_amount==0) {
				$process_info['card_amount'] = number_format((float)$location_info['default_preauth_amount'], $location_info['disable_decimal'], ".", "");
				$send_amount = $process_info['card_amount'];
			}
			break;
		case "PreAuthCapture" :
		case "PreAuthCaptureForTab" :
			$trancode = "PreAuthCapture";
			$include_tran_info = true;
			$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			break;
		case "VoiceAuth" :
			$trancode = "VoiceAuth";
			$check_for_duplicate = true;
			if ($process_info['mag_data']) {
				$create_transaction_record = true; // added 3/18/13 - version 2.2 and beyond will prompt user for card swipe
				$got_account_info = true; // added 3/18/13
			}
			$include_tran_info = true;
			break;
		case "Adjustment" :
			$trancode = "Adjust";
			$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			break;
		case "CaptureAll" :
			$trancode = "BatchSummary";
			break;
		case "Void" :
		case "VoidPreAuthCapture" :
			$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc`, `reader`, `voided` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
			if (mysqli_num_rows($get_transaction_info) > 0) {
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$card_type = $transaction_info['card_type'];
				$first_four = $transaction_info['first_four'];
				$cn = $transaction_info['card_desc'];
				if ($transaction_info['action'] == "Sale") {
					$trancode = "VoidSale";
					$void_action = "Payment Voided";
				} else if ($transaction_info['action'] == "Refund") {
					$trancode = "VoidReturn";
					$void_action = "Refund Voided";
				}
				$create_transaction_record = true;
				$include_tran_info = true;
				
			} else return "0|Unable to locate transaction record for void.||".$process_info['order_id']."||Error|||";
			break;
		case "Return" :
			$trancode = "Return";
			$create_transaction_record = true;
			$got_account_info = !empty($process_info['mag_data']);
			if ((float)$process_info['refund_tip_amount'] != 0) {
				$tip_amount = $process_info['refund_tip_amount'];
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$tip_amount, $location_info['disable_decimal'], ".", "");
			}
			break;
		case "IssueGiftCard" :
			$trancode = "Issue";
			$create_transaction_record = true;
			$got_account_info = true;
			$include_tran_info = true;
			$dupe_info = checkForPreviousIssueOrReload($process_info);
			if (!empty($dupe_info)) return $dupe_info;
			break;
		case "ReloadGiftCard" :
			$trancode = "Reload";
			$create_transaction_record = true;
			$got_account_info = true;
			$include_tran_info = true;
			$dupe_info = checkForPreviousIssueOrReload($process_info);
			if (!empty($dupe_info)) return $dupe_info;
			break;
		case "VoidGiftCardIssueOrReload" :
			$get_transaction_info = lavu_query("SELECT `order_id`, `transtype`, `amount`, `auth_code` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' ORDER BY `id` DESC LIMIT 1", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$process_info['order_id'] = $transaction_info['order_id'];
			$send_amount = $transaction_info['amount'];
			$original_authcode = $transaction_info['auth_code'];
			$process_info['authcode'] = $original_authcode;
			if ($transaction_info['transtype'] == "IssueGiftCard") $trancode = "VoidIssue";
			else if ($transaction_info['transtype'] == "ReloadGiftCard") $trancode = "VoidReload";
			$create_transaction_record = true;
			//$include_tran_info = true;
			break;
		case "GiftCardBalance" :
			$trancode = "Balance";
			$got_account_info = true;
			$include_tran_info = true;
			break;
		case "GiftCardSale" :
			$trancode = "NoNSFSale";
			//$check_for_duplicate = true;
			$create_transaction_record = true;
			$got_account_info = true;
			$include_tran_info = true;
			break;
		case "GiftCardVoid" :
			$get_transaction_info = lavu_query("SELECT `action` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1'", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$trancode = ($transaction_info['action'] == "Refund")?"VoidReturn":"VoidSale";
			$create_transaction_record = true;
			break;
		case "GiftCardReturn" :
			$trancode = "Return";
			$create_transaction_record = true;
			break;
		default: break;
	}

	$transaction_vars = generate_transaction_vars($process_info, $location_info, $cn, $card_type, $transtype, $swipe_grade, $pay_type, $pay_type_id, $process_info['card_amount']);

	$q_fields = "";
	$q_values = "";

	buildInsertFieldsAndValues($transaction_vars, $q_fields, $q_values);

	$system_id = 0;
	if ($create_transaction_record) {
		$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
		if (!$preliminary_record_transaction) return "0|Failed to create transaction record.||".$process_info['order_id'];
		$system_id = lavu_insert_id();
	}

	$transaction_vars['transaction_id'] = $process_info['pnref'];
	$transaction_vars['orig_internal_id'] = $process_info['orig_internal_id'];

	$record_number = "RecordNumberRequested";
	$original_authcode = "";
	$original_ref_data = "";
	$original_process_data = "";
	$additional_data = "";

	$pp_create_invoice_id = $process_info['ioid'];
	require_once(resource_path()."/json.php");

	$accountvars = array();
	if ($got_account_info)
	{
		if (!$e2e && !empty($track2)) {
			$accountvars[] = array("Track2", trim($track2, ";"));
			$accountvars[] = array("Name", $process_info['name_on_card']);
		} else if (!$e2e) {
			$accountvars[] = array("AcctNo", $process_info['card_number']);
			$accountvars[] = array("ExpDate", $process_info['exp_month'].$process_info['exp_year']);
		} else {
			$accountvars[] = array("EncryptedFormat", "MagneSafe");
			$accountvars[] = array("AccountSource", "Swiped");
			$accountvars[] = array("EncryptedBlock", $track2);
			$accountvars[] = array("EncryptedKey", $dukpt_ksn);
		}
		if ($trancode == "VoiceAuth") $tran_info_xml = "&lt;AuthCode&gt;".$process_info['authcode']."&lt;/AuthCode&gt;";
	} else {
		$get_transaction_info = lavu_query("SELECT `id`, `auth`, `record_no`, `temp_data`, `amount`, `tip_amount`, `auth_code`, `ref_data`, `process_data`, `reader`, `first_four`, `card_desc`, `info` FROM `cc_transactions` WHERE `pay_type_id` = '[pay_type_id]' AND `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[transaction_id]' AND (`auth_code` = '[auth_code]' OR (`voice_auth` = '1' AND `amount` = '[amount]'))", $transaction_vars);
		$extract = mysqli_fetch_assoc($get_transaction_info);

		if ($system_id == 0) $system_id = $extract['id'];
		$record_number = $extract['record_no'];
		if ($record_number=="" || $record_number=="[2]") {
			$e2e = false;
			$acct_info = explode("|", interpret($extract['temp_data']));
			$accountvars[] = array("AcctNo", $acct_info[0]);
			$accountvars[] = array("ExpDate", $acct_info[1]);
		} else {
			$e2e = true;
			$split_mag_data = explode("|", $extract['temp_data']);
			$accountvars[] = array("EncryptedFormat", "MagneSafe");
			$accountvars[] = array("AccountSource", "Swiped");
			$accountvars[] = array("EncryptedBlock", $split_mag_data[3]);
			$accountvars[] = array("EncryptedKey", $split_mag_data[9]);
		}

		if ($extract['auth']!=2 && $trancode!="Return") $send_amount = $extract['amount'];
		if ($trancode == "VoiceAuth") {
			$original_authcode = $process_info['authcode'];
			$record_number = "RecordNumberRequested";
		} else $original_authcode = $extract['auth_code'];
		$original_ref_data = $extract['ref_data'];
		$original_process_data = $extract['process_data'];
		$tran_info_xml = "&lt;AuthCode&gt;".$original_authcode."&lt;/AuthCode&gt;";
		if ($original_ref_data != "") $tran_info_xml .= "&lt;AcqRefData&gt;".$original_ref_data."&lt;/AcqRefData&gt;";
		if ($original_process_data != "") $tran_info_xml .= "&lt;ProcessData&gt;".$original_process_data."&lt;/ProcessData&gt;";
		if ($trancode == "VoidSale") {
			$tip_amount = $extract['tip_amount'];
			if ($extract['transtype'] == "Return") $trancode = "VoidReturn";
		}
	}

	$account_xml = "";
	for($i = 0; $i < count($accountvars); $i++) {
		$field_title = $accountvars[$i][0];
		$field_value = $accountvars[$i][1];
		$account_xml .= "&lt;".$field_title."&gt;".$field_value."&lt;/".$field_title."&gt;\n";
	}

	$amount_xml = "&lt;Purchase&gt;".number_format((float)$send_amount, $location_info['disable_decimal'], ".", "")."&lt;/Purchase&gt;";
	if ($tip_amount != "") $amount_xml .= "&lt;Gratuity&gt;".number_format((float)$tip_amount, $location_info['disable_decimal'], ".", "")."&lt;/Gratuity&gt;";

	if ($transtype == "PreAuthCaptureForTab") $amount_xml .= "&lt;Authorize&gt;".number_format((float)$process_info['more_info'], $location_info['disable_decimal'], ".", "")."&lt;/Authorize&gt;";
	else if (($trancode == "PreAuth") || ($trancode == "PreAuthCapture")) $amount_xml .= "&lt;Authorize&gt;".number_format((float)$send_amount, $location_info['disable_decimal'], ".", "")."&lt;/Authorize&gt;";

	$retry_amount = $amount_xml;
	//$amount_xml = "&lt;Purchase&gt;24.01&lt;/Purchase&gt;"; // for testing non-support of realtime reversals

	if ($e2e) {
		switch ($trancode) {
			case "Sale" : $trancode = "Sale"; break;
			case "PreAuth" : $trancode = "PreAuth"; break;
			case "PreAuthCapture" : $trancode = "PreAuthCaptureByRecordNo"; break;
			case "VoiceAuth" : $trancode = "VoiceAuth"; break;
			case "Adjust" : $trancode = "AdjustByRecordNo"; break;
			case "BatchSummary" : $trancode = "BatchSummary"; break;
			case "VoidSale" :
				if ($pay_type_id == "2") $trancode = "VoidSaleByRecordNo";
				break;
			case "VoidReturn" :
				if ($pay_type_id == "2") $trancode = "VoidReturnByRecordNo";
				break;
			case "Return" :
				$trancode = "ReturnByRecordNo";
				break;
			default: break;
		}
	} else if (!$e2e && $process_info['username']=="337234005" && $process_info['password']=="xyz") {
		$process_info['username'] = "337234001";
		$process_info['password'] = "xyz";
	}

	$attempting_realtime_reversal = false;

	$server_id = (DEV)?"test":$process_info['server_id'];

	$memo = "POS Lavu v".(empty($poslavu_version)?"1.5.6":$poslavu_version);

	$postvars = array();
	$postvars[] = array("MerchantID", $process_info['username']);
	$postvars[] = array("OperatorID", $server_id);
	$postvars[] = array("TranCode", $trancode);
	$postvars[] = array("Memo", $memo);
	if ($trancode == "BatchSummary") $TorA = "Admin";
	else {
		$postvars[] = array("TranType", $mtrantype);
		if ($mtrantype == "PrePaid") $postvars[] = array("IpPort", "9100");
		$postvars[] = array("InvoiceNo", str_replace("-", "", $process_info['loc_id'].$process_info['order_id']));
		if (!empty($additional_data) && strlen($additional_data)<=5000) $postvars[] = array("AdditionalData", $additional_data);
		if (!strpos($trancode, "ByRecordNo")) $postvars[] = array("Account", $account_xml);
		$card_cvn = $process_info['card_cvn'];
		if ($card_cvn != "") {
			if (($mtrantype == "PrePaid") && (strlen($card_cvn) < 3)) $card_cvn = str_pad($card_cvn, 3, "0", STR_PAD_LEFT);
			$postvars[] = array("CVVData", $card_cvn);
		}
		if ($e2e) {
			$postvars[] = array("RecordNo", $record_number);
			$postvars[] = array("Frequency", "OneTime");
		}

		if ($include_tran_info) {
			$postvars[] = array("TranInfo", $tran_info_xml);
			if (strstr($trancode, "VoidSale") || strstr($trancode, "VoidReturn")) {
				$postvars[] = array("RefNo", "1");
				$attempting_realtime_reversal = true;
			} else $postvars[] = array("RefNo", str_replace("-", "", $process_info['loc_id'].$process_info['order_id']));
		} else {
			if ($process_info['pnref'] != "") $postvars[] = array("RefNo", $process_info['pnref']);
			else if ($transtype == "GiftCardReturn") $postvars[] = array("RefNo", "1");
			if ($original_authcode != "") $postvars[] = array("AuthCode", $original_authcode);
		}
		$postvars[] = array("PartialAuth", "Allow"); // if ($location_info['allow_partial_auth'] == "1")
		if ($send_amount != "") $postvars[] = array("Amount", $amount_xml);
		$postvars[] = array("TerminalName", $process_info['register']);
	}

	$curl_result = post_to_mercury($postvars, $process_info['password'], $TorA, $process_info['data_name'], $process_info['loc_id'], $transtype, $pay_type, $special_designator, $cancel_logging?"0":$location_info['gateway_debug'], $process_info['reader']);

	$xml_start = microtime(true);
	$xml = simplexml_load_string($curl_result);
	$xml_end = microtime(true);

	$debug .= "\ntime taken to process XML: ".round((($xml_end - $xml_start) * 1000), 2) . "ms";

	$results = print_r($xml, true);

	$debug .= "\n\n\nresult: ".$results;

	if ($trancode == "BatchSummary") {

		$result = $xml->CmdResponse->CmdStatus;
		$respmsg = $xml->CmdResponse->TextResponse;
		$respmsg = $xml->CmdResponse->TextResponse;
		$batch_no = $xml->BatchSummary->BatchNo;
		$batch_item_count = $xml->BatchSummary->BatchItemCount;
		$net_batch_total = $xml->BatchSummary->NetBatchTotal;
		$credit_purchase_count = $xml->BatchSummary->CreditPurchaseCount;
		$credit_purchase_amount = $xml->BatchSummary->CreditPurchaseAmount;
		$credit_return_count = $xml->BatchSummary->CreditReturnCount;
		$credit_return_amount = $xml->BatchSummary->CreditReturnAmount;
		$debit_purchase_count = $xml->BatchSummary->DebitPurchaseCount;
		$debit_purchase_amount = $xml->BatchSummary->DebitPurchaseAmount;
		$debit_return_count = $xml->BatchSummary->DebitReturnCount;
		$debit_return_amount = $xml->BatchSummary->DebitReturnAmount;

		//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "Mercury", $process_info['reader'], "BatchSummary", $result, $respmsg, "", "", $pay_type);

		$log_string = "MERCURY[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - TRANCODE: BatchSummary - RESULT: ".$result."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
		write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

		if ($batch_item_count == "") { $batch_item_count = "0"; }

		$batchvars = array();
		$batchvars['loc_id'] = $location_info['id'];
		$batchvars['hc'] = "0";
		$batchvars['datetime'] = $process_info['device_time'];
		$batchvars['gateway'] = "Mercury";
		$batchvars['user_id'] = $process_info['server_id'];
		$batchvars['user_name'] = $process_info['server_name'];
		$batchvars['batch_no'] = $batch_no;
		$batchvars['batch_item_count'] = $batch_item_count;
		$batchvars['net_batch_total'] = $net_batch_total;
		$batchvars['credit_purchase_count'] = $credit_purchase_count;
		$batchvars['credit_purchase_amount'] = $credit_purchase_amount;
		$batchvars['credit_return_count'] = $credit_return_count;
		$batchvars['credit_return_amount'] = $credit_return_amount;
		$batchvars['debit_purchase_count'] = $debit_purchase_count;
		$batchvars['debit_purchase_amount'] = $debit_purchase_amount;
		$batchvars['debit_return_count'] = $debit_return_count;
		$batchvars['debit_return_amount'] = $debit_return_amount;

		if ($result == "Success") {

			record_batch($process_info['data_name'], $batchvars);

			$postvars = array();
			$postvars[] = array("MerchantID", $process_info['username']);
			$postvars[] = array("OperatorID", $process_info['server_id']);
			$postvars[] = array("TranCode", "BatchClose");
			$postvars[] = array("Memo", "POSLavu v".$poslavu_version);
			$postvars[] = array("BatchNo", $batch_no);
			$postvars[] = array("BatchItemCount", $batch_item_count);
			$postvars[] = array("NetBatchTotal", $net_batch_total);
			$postvars[] = array("CreditPurchaseCount", $credit_purchase_count);
			$postvars[] = array("CreditPurchaseAmount", $credit_purchase_amount);
			$postvars[] = array("CreditReturnCount", $credit_return_count);
			$postvars[] = array("CreditReturnAmount", $credit_return_amount);
			$postvars[] = array("DebitPurchaseCount", $debit_purchase_count);
			$postvars[] = array("DebitPurchaseAmount", $debit_purchase_amount);
			$postvars[] = array("DebitReturnCount", $debit_return_count);
			$postvars[] = array("DebitReturnAmount", $debit_return_amount);

			$curl_result = post_to_mercury($postvars, $process_info['password'], $TorA, $process_info['data_name'], $process_info['loc_id'], "BatchClose", $pay_type, $special_designator, $location_info['gateway_debug'], $process_info['reader']);

			$xml = simplexml_load_string($curl_result);

			$results = print_r($xml, true);

			$debug .= "\n\n\nresult: ".$results;

			$result = $xml->CmdResponse->CmdStatus;
			$respmsg = $xml->CmdResponse->TextResponse;

			//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "Mercury", $process_info['reader'], "BatchClose", $result, $respmsg, "", "", $pay_type);

			$log_string = "MERCURY[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - TRANCODE: BatchClose - RESULT: ".$result."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
			write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

			if ($result == "Success") {

				$batch_total = number_format((float)$net_batch_total, $location_info['disable_decimal'], ".", "");
				$bda = array();
				$bda[] = $batch_item_count;
				$bda[] = $batch_total;
				$bda[] = $credit_purchase_count;
				$bda[] = number_format((float)$credit_purchase_amount, $location_info['disable_decimal'], ".", "");
				$bda[] = $credit_return_count;
				$bda[] = number_format((float)$credit_return_amount, $location_info['disable_decimal'], ".", "");
				$bda[] = $debit_purchase_count;
				$bda[] = number_format((float)$debit_purchase_amount, $location_info['disable_decimal'], ".", "");
				$bda[] = $debit_return_count;
				$bda[] = number_format((float)$debit_return_amount, $location_info['disable_decimal'], ".", "");

				$batch_breakdown = "<table cellspacing='0' cellpadding='3'>
						<tr><td align='center' colspan='2'>BATCH CLOSED</td></tr>
						<tr><td align='right'>Batch Number:</td><td align='left'>$batch_no</td></tr>
						<tr><td align='right'>Batch Item Count:</td><td align='left'>".$bda[0]."</td></tr>
						<tr><td align='right'>Net Batch Total:</td><td align='left'>$ ".$bda[1]."</td></tr>
						<tr><td align='right'>Credit Purchase Count:</td><td align='left'>".$bda[2]."</td></tr>
						<tr><td align='right'>Credit Purchase Amount:</td><td align='left'>$ ".$bda[3]."</td></tr>
						<tr><td align='right'>Credit Return Count:</td><td align='left'>".$bda[4]."</td></tr>
						<tr><td align='right'>Credit Return Amount:</td><td align='left'>$ ".$bda[5]."</td></tr>
						<tr><td align='right'>Debit Purchase Count:</td><td align='left'>".$bda[6]."</td></tr>
						<tr><td align='right'>Debit Purchase Amount:</td><td align='left'>$ ".$bda[7]."</td></tr>
						<tr><td align='right'>Debit Return Count:</td><td align='left'>".$bda[8]."</td></tr>
						<tr><td align='right'>Debit Return Amount:</td><td align='left'>$ ".$bda[9]."</td></tr>
					</table>";

				return "1|".$result." - ".translate_response($respmsg)."|".$batch_total."|".$batch_breakdown."|".$batch_no."|".join(":.:", $bda);

			} else {

				return "0|".$result." - ".translate_response($respmsg);
			}

		} else {

			return "0|".$result." - ".translate_response($respmsg);
		}

	} else {

		$result = extract_value($curl_result, "CmdStatus");
		$respmsg = extract_value($curl_result, "TextResponse");
		$acct_no = (string)$xml->TranResponse->AcctNo;
		$exp_date = (string)$xml->TranResponse->ExpDate;
		$card_type = (string)$xml->TranResponse->CardType;
		$new_authcode = (string)$xml->TranResponse->AuthCode;
		$new_pnref = (string)$xml->TranResponse->RefNo;
		if ($new_pnref == "") { $new_pnref = $process_info['loc_id'].$process_info['order_id']; }
		$t_record_number = (string)$xml->TranResponse->RecordNo;
		$ref_data = trim((string)$xml->TranResponse->AcqRefData);
		$process_data = (string)$xml->TranResponse->ProcessData;
		$purchase_amount = (string)$xml->TranResponse->Amount->Purchase;
		$authorize_amount = (string)$xml->TranResponse->Amount->Authorize;

		$debug .= "MERCURY\n\n".print_r($postvars, true)."\n\nRESULT: $result\nRESPONSE MESSAGE: $respmsg\n$new_authcode\n$new_pnref\n".$card_type;

		$approved = "0";
		if ($result=="Approved") $approved = "1";
		if ($location_info['gateway_debug']=="1" && !$cancel_logging) gateway_debug($process_info['data_name'], $process_info['loc_id'], "MERCURY", $trancode, $approved, $debug);

		//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "Mercury", $process_info['reader'], $transtype, $result, $respmsg, $card_type, $swiped, $pay_type)

		if (!$cancel_logging) {
			$log_string = "MERCURY[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - CHECK: ".$process_info['check']." - AMOUNT: ".$purchase_amount.$log_tip_amount." - PAYTYPE: $pay_type - TRANCODE: $trancode - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $new_authcode - CARD TYPE: $card_type - CARD DESC: ".substr($cn, -4)." - SWIPED: ".$swiped." - READER: ".$process_info['reader']."[--CR--]RESPONSE MESSAGE: ".$respmsg.(($respmsg=="PARTIAL AP")?" (".$authorize_amount.")":"")."[--CR--]";
			write_log_entry("gateway", $process_info['data_name'], $log_string."\n");
		}

		$temp_data = "";
		if (!$e2e)
		{
			$temp_data = disinterpret($acct_no."|".$exp_date);
		}
		else if ($pay_type=="Gift Card" || $special_designator=="gift_card")
		{
			$temp_data = disinterpret($xml->TranResponse->AcctNo."|");
		}

		if ($approved == "1") {

			$new_balance = "";
			if ($trancode == "Balance") {
				$new_authcode = $xml->TranResponse->Amount->Balance;
				$new_balance = $new_authcode;
				$t_record_number = $xml->TranResponse->AcctNo;
			} else if (in_array($transtype, array("IssueGiftCard", "ReloadGiftCard", "GiftCardSale", "GiftCardVoid", "GiftCardReturn"))) {
				$new_balance = $xml->TranResponse->Amount->Balance;
				if (in_array($transtype, array())) $process_info['info'] = $new_balance;
				else $process_info['more_info'] = $new_balance;
			}

			$process_info['last_mod_ts'] = time();

			$rtn = array();

			if ($respmsg == "PARTIAL AP") {

				$process_info['card_amount'] = $authorize_amount;

				$rtn[] = "2"; // 0
				$rtn[] = $new_pnref;
				$rtn[] = substr($cn, -4);
				$rtn[] = translate_response($respmsg);
				$rtn[] = $new_authcode;
				$rtn[] = $card_type;
				$rtn[] = $t_record_number;
				$rtn[] = $process_info['card_amount'];
				$rtn[] = $process_info['order_id'];
				$rtn[] = $ref_data;
				$rtn[] = rawurlencode($process_data); // 10
				$rtn[] = $new_balance;
				$rtn[] = "";
				$rtn[] = $first_four;
				$rtn[] = $process_info['last_mod_ts'];
				$rtn[] = "";//15
				$rtn[] = "";//16
				$rtn[] = "";//17
				$rtn[] = "";//18
				$rtn[] = $process_info['name_on_card'];
			} else {

				if ($respmsg == "AP*") {
					$check_transactions = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `pay_type_id` = '[2]' AND `auth_code` = '[3]' AND `server_time` > '[4]' AND `voided` = '0'", $process_info['loc_id'], $pay_type_id, $new_authcode, date("Y-m-d H:i:s", (time() - 86400)));
					if (@mysqli_num_rows($check_transactions) > 0) {
						if ($create_transaction_record) $remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
						return "0|Duplicate Transaction||".$process_info['order_id'];
					}
				}

				$process_info['card_amount'] = $purchase_amount;

				$rtn[] = "1"; // 0
				$rtn[] = $new_pnref;
				$rtn[] = substr($cn, -4);
				$rtn[] = translate_response($respmsg);
				$rtn[] = $new_authcode;
				$rtn[] = $card_type;
				$rtn[] = $t_record_number;
				$rtn[] = $process_info['card_amount'];
				$rtn[] = $process_info['order_id'];
				$rtn[] = $ref_data;
				$rtn[] = rawurlencode($process_data); // 10
				$rtn[] = $new_balance;
				$rtn[] = "";
				$rtn[] = $first_four;
				$rtn[] = $process_info['last_mod_ts'];
				$rtn[] = "";//15
				$rtn[] = "";//16
				$rtn[] = "";//17
				$rtn[] = "";//18
				$rtn[] = $process_info['name_on_card'];
			}

			update_tables($process_info, $location_info, $card_type, $new_pnref, $new_authcode, $system_id, "0", $temp_data, $ref_data, $process_data, $t_record_number);

			return implode("|", $rtn);

		} else if ($attempting_realtime_reversal) {

			$postvars = array();
			$postvars[] = array("MerchantID", $process_info['username']);
			$postvars[] = array("OperatorID", $server_id);
			$postvars[] = array("TranCode", $trancode);
			$postvars[] = array("Memo", "POSLavu v".$poslavu_version);
			$postvars[] = array("TranType", $mtrantype);
			$postvars[] = array("InvoiceNo", $process_info['loc_id'].$process_info['order_id']);
			if (!strpos($trancode, "ByRecordNo")) $postvars[] = array("Account", $account_xml);
			if ($e2e) {
				$postvars[] = array("RecordNo", $record_number);
				$postvars[] = array("Frequency", "OneTime");
			}
			if ($process_info['pnref'] != "") $postvars[] = array("RefNo", $process_info['pnref']);
			if ($original_authcode != "") $postvars[] = array("AuthCode", $original_authcode);
			if ($send_amount != "") $postvars[] = array("Amount", $retry_amount);
			$postvars[] = array("TerminalName", $process_info['register']);

			$curl_result = post_to_mercury($postvars, $process_info['password'], $TorA, $process_info['data_name'], $process_info['loc_id'], $transtype, $pay_type, $special_designator, $location_info['gateway_debug'], $process_info['reader']);

			$xml = simplexml_load_string($curl_result);

			$results = print_r($xml, true);

			$debug .= "\n\n\nresult: ".$results;

			$result = $xml->CmdResponse->CmdStatus;
			$respmsg = $xml->CmdResponse->TextResponse;
			$acct_no = $xml->TranResponse->AcctNo;
			$exp_date = $xml->TranResponse->ExpDate;
			$card_type = $xml->TranResponse->CardType;
			$new_authcode = $xml->TranResponse->AuthCode;
			$new_pnref = $xml->TranResponse->RefNo;
			if ($new_pnref == "") { $new_pnref = $location_id.$order_id; }
			$t_record_number = $xml->TranResponse->RecordNo;
			$ref_data = trim($xml->TranResponse->AcqRefData);
			$process_data = $xml->TranResponse->ProcessData;

			//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "Mercury", $process_info['reader'], $transtype, $result, $respmsg, $card_type, $swiped, $pay_type);

			$log_string = "MERCURY[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: ".$process_info['card_amount'].$log_tip_amount." - TRANCODE: $trancode - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $new_authcode - CARD TYPE: $card_type - CARD DESC: ".substr($cn, -4)." - SWIPED: ".$swiped."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
			write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

			$temp_data = "";
			if (!$e2e) $temp_data = disinterpret($acct_no."|".$exp_date);

			if ($result == "Approved") {

				$process_info['last_mod_ts'] = time();

				update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", $temp_data, $ref_data, $process_data, $t_record_number);

				$rtn = array();

				$rtn[] = "1"; // 0
				$rtn[] = $new_pnref;
				$rtn[] = substr($cn, -4);
				$rtn[] = translate_response($respmsg);
				$rtn[] = $new_authcode;
				$rtn[] = $card_type;
				$rtn[] = $t_record_number;
				$rtn[] = "";
				$rtn[] = $process_info['order_id'];
				$rtn[] = "1";
				$rtn[] = "1"; // 10
				$rtn[] = "1";
				$rtn[] = "1";
				$rtn[] = "1";
				$rtn[] = $process_info['last_mod_ts'];
				$rtn[] = "";//15
				$rtn[] = "";//16
				$rtn[] = "";//17
				$rtn[] = "";//18
				$rtn[] = $process_info['name_on_card'];
				return implode("|", $rtn);
			}

		} else if (($transtype == "IssueGiftCard") && ($respmsg == "Account Already Issued")) {

			$process_info['transtype'] = "ReloadGiftCard";
			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
			return process_mercury($process_info, $location_info);

		} else if (($transtype == "ReloadGiftCard") && ($respmsg == "Account Not Issued")) {

			$process_info['transtype'] = "IssueGiftCard";
			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
			return process_mercury($process_info, $location_info);

		} else {

			if ((substr($respmsg, 0, 4) == "CALL") && ($transtype != "AuthForTab")) {

				if ($process_info['mag_data']!="" && ($process_info['reader']=="idynamo" || $process_info['reader']=="udynamo")) $temp_data = $process_info['mag_data'];
				else $temp_data = disinterpret($acct_no."|".$exp_date);

				$mark_for_voice_auth = lavu_query("UPDATE `cc_transactions` SET `temp_data` = '[1]', `record_no` = '[2]', `got_response` = '1', `voice_auth` = '1', `last_mod_ts` = '[3]' WHERE `id` = '[4]'", $temp_data, $t_record_number, time(), $system_id);

			} else if ($create_transaction_record) $remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
		}

		$rslt = "";
		$resptitle = "Declined";
		if ($result != "Declined") {
			$resptitle = $result;
			if (empty($result)) {
				$result = "Error";
				if (empty($respmsg)) $rslt = "No response received from gateway. Please call Mercury support at 800-846-4472.";
			}
		}

		$rtn = array();

		$rtn[] = "0";
		$rtn[] = $rslt.translate_response($respmsg);
		$rtn[] = $new_pnref;
		$rtn[] = $process_info['order_id'];
		$rtn[] = "";
		$rtn[] = $resptitle;

		return implode("|", $rtn);
	}
}
?>
