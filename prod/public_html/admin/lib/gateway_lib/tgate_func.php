<?php

	function process_tgate2($process_info, $location_info, $transaction_vars) {
	
		global $tgate_server_url;
		global $tgate_gift_card_server_url;
		global $tgate_loyalty_card_server_url;
		global $server_order_prefix;

		$debug = print_r($process_info, true)."\n\n";
		
		$loc_id				= $process_info['loc_id'];
		$order_id			= $process_info['order_id'];
		$data_name			= $process_info['data_name'];
		$transtype			= $process_info['transtype'];
		$ext_data			= $process_info['ext_data'];
		$pay_type			= $process_info['set_pay_type'];
		$pay_type_id		= $process_info['set_pay_type_id'];
		$send_amount		= $process_info['card_amount'];
		$send_card_number	= $process_info['card_number'];
		$send_exp_month		= $process_info['exp_month'];
		$send_exp_year		= $process_info['exp_year'];
		$mag_data 			= $process_info['mag_data'];
		$reader				= $process_info['reader'];
		
		$log_gateway = "TGATE";
		if ($location_info['gateway'] == "PivotalPayments") { // un: hill2680 - pw: Stanmil3579
	
			$log_gateway = "PIVOTAL PAYMENTS";
			$tgate_server_url = "https://secure1.pivotalpayments.com/SmartPayments/transact.asmx/ProcessCreditCard";

		} else if ($data_name=="bfc_demo" || strpos($data_name, "hotel") !== false) {

			$tgate_server_url = "https://gateway.itstgate.com:10221/SmartPayments/transact3.asmx/ProcessCreditCard";
		}

		$special_designator = specialDesignatorForPayTypeID($pay_type_id);

		$force_track1 = false;
		if ($pay_type=="Gift Card" || $special_designator=="gift_card")
		{
			$posturl = $tgate_gift_card_server_url;
			$force_track1 = true;
		}
		else if ($pay_type=="Loyalty Card" || $special_designator=="loyalty_card")
		{
			$posturl = $tgate_loyalty_card_server_url;
			$force_track1 = true;
		}
		else
		{
			$posturl = $tgate_server_url;
		}
		
		$debug .= $posturl."\n\n";

		$swiped = "0";
		$swipe_grade = "H";		
		$entry_mode = "MANUAL";
		$track2 = "";
		$send_mag_data = "";
		if ($mag_data) {
			$swiped = "1";
			$swipe_grade = "G";
			$send_mag_data = $mag_data;
		}
		$log_tip_amount = "";
		
		$cn = $send_card_number; // keep for response string
		$first_four = substr($cn, 0, 4);
		$card_type = getCardTypeFromNumber($cn);

		$processed_mag_data = false;
		if ($mag_data && ($reader=="idynamo" || $reader=="udynamo")) {

			$processed_mag_data = true;
			$valid_mag_data = (substr($mag_data, 0, 2)=="%B" || ((substr($mag_data, 0, 1)==";" || substr($mag_data, 0, 3)=="%E?") && $pay_type_id!="2"));
			$track1good = false;
			$track2good = false;
			$split_mag_data = explode("|", $mag_data);
			$encrypted_track1 = $split_mag_data[2];
			$encrypted_track2 = $split_mag_data[3];
			$dukpt_ksn = $split_mag_data[9];
			$ext_data = "<SecureFormat>MagneSafeV1</SecureFormat>";
			if (!strstr($encrypted_track1, "E?") && ($encrypted_track1 != "")) {
				$ext_data .= "<Track1>".$encrypted_track1."</Track1>";
				$swipe_grade = "C";
				$track1good = true;
			} else if ($force_track1) {
				$ext_data .= "<Track1></Track1>";
			}
			if (!strstr($encrypted_track2, "E?") && ($encrypted_track2 != "")) {
				$ext_data .= "<Track2>".$encrypted_track2."</Track2>";
				if ($swipe_grade == "C") $swipe_grade = "A";
				else $swipe_grade = "B";
				$track2good = true;
			}
			$ext_data .= "<SecurityInfo>".$dukpt_ksn."</SecurityInfo>";
			$send_card_number = "";
			$send_exp_month = "";
			$send_exp_year = "";
			
			if (!$valid_mag_data || (!$track1good && !$track2good)) {
				
				if ($location_info['gateway_debug'] == "1") gateway_debug($data_name, $loc_id, $log_gateway, $transtype, "BR", $debug);
				
				//record_stats($process_info['company_id'], $data_name, $loc_id, "TGate", $reader, $transtype, "99999", "Read Error - Missing track 1 and 2", "", $swiped, $pay_type);

				$lg_str = $log_gateway."[--CR--]";
				$lg_str .= "LOCATION: ".		$loc_id						." - ";
				$lg_str .= "REGISTER: ".		$process_info['register']	." - ";
				$lg_str .= "SERVER ID: ".	$process_info['server_id']	." - ";
				$lg_str .= "ORDER ID: ".		$order_id	." - ";
				$lg_str .= "AMOUNT: ".		$process_info['card_amount']	." - ";
				$lg_str .= "TRANSTYPE: ".	$transtype					." - ";
				$lg_str .= "CARD TYPE: ??? - ";
				$lg_str .= "LAST FOUR: ".	substr($cn, -4)				." - ";
				$lg_str .= "READER: ".		$reader						."[--CR--]";
				$lg_str .= "EXT_DATA: ".		$ext_data					."[--CR--]";
				$lg_str .= "MAG DATA: ".		$mag_data					."[--CR--]";
				write_log_entry("gateway", $data_name, $lg_str."\n");

				$rtn = array();
				$rtn[] = "0";
				$rtn[] = "Card read failed.\nPlease try again.";
				$rtn[] = "";
				$rtn[] = $order_id;
				$rtn[] = "";
				$rtn[] = "Error";
				$rtn[] = "";
				$rtn[] = "";
				$rtn[] = "";
	
				return implode("|", $rtn);
			}
			$send_mag_data = "";
			$entry_mode = "MAGNETICSTRIPE";

		} else if ($mag_data && $process_info['encrypted']=="1") {

			$not_really_encrypted = ($reader=="idtech_unimag" && substr($mag_data, 0, 1)==";");

			if (!$not_really_encrypted) { // check to make sure mag_data is actually encrypted
			
				$processed_mag_data = true;
				$swipe_grade = "B";
				$encrypted_info = idtech_encrypted($mag_data, $process_info, $location_info);
				$encrypted_track1 = hex_string($encrypted_info['track1']);
				$encrypted_track2 = hex_string($encrypted_info['track2']);
				$dukpt_ksn = hex_string($encrypted_info['ksn']);
				if ((empty($encrypted_track1) && empty($encrypted_track2)) || empty($dukpt_ksn)) {

					if ($location_info['gateway_debug'] == "1") gateway_debug($data_name, $loc_id, $log_gateway, $transtype, "BR", $debug);

					$lg_str = $log_gateway."[--CR--]";
					$lg_str .= "LOCATION: ".		$loc_id						." - ";
					$lg_str .= "REGISTER: ".		$process_info['register']	." - ";
					$lg_str .= "SERVER ID: ".	$process_info['server_id']	." - ";
					$lg_str .= "ORDER ID: ".		$order_id	." - ";
					$lg_str .= "AMOUNT: ".		$process_info['card_amount']	." - ";
					$lg_str .= "TRANSTYPE: ".	$transtype					." - ";
					$lg_str .= "CARD TYPE: ??? - ";
					$lg_str .= "LAST FOUR: ".	substr($cn, - 4)				." - ";
					$lg_str .= "READER: ".		$reader						."[--CR--]";
					$lg_str .= "MAG DATA: ".		$mag_data					."[--CR--]";
					write_log_entry("gateway", $data_name, $lg_str."\n");

					$rtn = array();
					$rtn[] = "0";
					$rtn[] = "Card read failed.\nPlease try again.";
					$rtn[] = "";
					$rtn[] = $order_id;
					$rtn[] = "";
					$rtn[] = "Error";
					$rtn[] = "";
					$rtn[] = "";
					$rtn[] = "";
		
					return implode("|", $rtn);
				}
				
				$ext_data = "<SecureFormat>SecureMag</SecureFormat>";
				if (!empty($encrypted_track1)) {
					$ext_data .= "<Track1>".$encrypted_track1."</Track1>";
					$swipe_grade = "C";
				} else if ($force_track1) $ext_data .= "<Track1></Track1>";
				if (!empty($encrypted_track2)) {
					$ext_data .= "<Track2>".$encrypted_track2."</Track2>";
					if ($swipe_grade == "C") $swipe_grade = "A";
					else $swipe_grade = "B";
				}
				$ext_data .= "<SecurityInfo>".$dukpt_ksn."</SecurityInfo>";
				$send_card_number = "";
				$send_exp_month = "";
				$send_exp_year = "";		
				$send_mag_data = "";
				$entry_mode = "MAGNETICSTRIPE";
			}

		} else if ($mag_data && $process_info['encrypted']=="2") {
	
			$processed_mag_data = true;
			$swipe_grade = "B";
		
			require_once(dirname(__FILE__)."/mercury_lls_func.php");
			$myLLSSwipe = new IDTechLLS();
			$myLLSSwipe->setMagData($mag_data);
			$encrypted_info = $myLLSSwipe->getEncryptedInfoBlock();
			
			if(empty($encrypted_info['ksn'])){
				if(empty($encrypted_info['track2'])){error_log(__FILE__ . ' ' . __FUNCTION__.' track2 empty? ');}
				$send_mag_data = str_replace(";", "", hex2bin($encrypted_info['track2']));
				if(empty($encrypted_info['track2'])){error_log(__FILE__ . ' ' . __FUNCTION__.' send_mag_data empty? ');}
				$entry_mode = "MAGNETICSTRIPE";
				$swipe_grade = "E";
			}else{
				$encrypted_track1 = $encrypted_info['track1'];
				$encrypted_track2 = $encrypted_info['track2'];

				$ext_data = "<SecureFormat>SecureMag</SecureFormat>";
				if (!empty($encrypted_track1)) {
					$ext_data .= "<Track1>".$encrypted_track1."</Track1>";
					$swipe_grade = "C";
				} else if ($force_track1) $ext_data .= "<Track1></Track1>";
				if (!empty($encrypted_track2)) {
					$ext_data .= "<Track2>".$encrypted_track2."</Track2>";
					if ($swipe_grade == "C") $swipe_grade = "A";
					else $swipe_grade = "B";
				}

				$ext_data .= "<SecurityInfo>".$encrypted_info['ksn']."</SecurityInfo>";
				$send_card_number = "";			
				$send_exp_month = "";
				$send_exp_year = "";		
				$send_mag_data = "";
				$entry_mode = "MAGNETICSTRIPE";
			}
		}
		
		if ($mag_data!="" && !$processed_mag_data) {

			$tracks = explode("?", $mag_data);
			$track2_index = 1;
			if (strstr($tracks[0], "=")) {
				$track2_index = 0;
			}
			if (count($tracks) > $track2_index) {
				$track2 = $tracks[$track2_index];
				if (($track2 != "") && ($track2 != "ERROR")) {
					$send_mag_data = str_replace(";", "", $track2);
					$entry_mode = "MAGNETICSTRIPE";
					$swipe_grade = "E";
				} else $send_mag_data = "";
			}
		}

		$include_inv_num = false;
		if ($transtype == "") $transtype = "Sale";
			
		switch ($transtype) {

			case "" :
			case "Sale" :

				$tgtranstype = "Sale";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = true;
				break;

			case "Auth" :
			case "AuthForTab" :

				$tgtranstype = "Auth";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = true;
				if ($order_id == "0") {
					$order_id = create_new_order_record($process_info, $location_info);
					$process_info['order_id'] = $order_id;
				}
				if ($transtype=="AuthForTab" && (float)$send_amount==0) {
					$process_info['card_amount'] = $location_info['default_preauth_amount'];
					$send_amount = $process_info['card_amount'];
				}
				break;
				
			case "PreAuthCapture" :
			case "PreAuthCaptureForTab" :

				$tgtranstype = "Force";
				$get_transaction_info = lavu_query("SELECT `auth`, `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				$tx_info = mysqli_fetch_assoc($get_transaction_info);
				if (!empty($process_info['card_amount'])) $send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				else $send_amount = number_format(($tx_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				if ($cn == "") $cn = $tx_info['card_desc'];
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				$include_inv_num = true;
				break;
				
			case "Adjustment" :

				$tgtranstype = "Adjustment";
				if (isset($location_info['tip_adjust_send_total']) && ($location_info['tip_adjust_send_total'] == "1")) {
					$get_transaction_info = lavu_query("SELECT `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
					$tx_info = mysqli_fetch_assoc($get_transaction_info);
					$send_amount = number_format(($tx_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
					if ($cn == "") $cn = $tx_info['card_desc'];
				}
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				break;

			case "Void" :
			case "VoidPreAuthCapture" :

				$tgtranstype = "Void";
				$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
				$tx_info = mysqli_fetch_assoc($get_transaction_info);
				$void_action = ($tx_info['action'] == "Sale")?"Payment Voided":"Refund Voided";
				$card_type = $tx_info['card_type'];
				$first_four = $tx_info['first_four'];
				$cn = $tx_info['card_desc'];

				if (($tx_info['transtype']=="Auth" || $tx_info['transtype']=="AuthForTab") && ($tx_info['auth']=="1" || $tx_info['auth']=="2")) {

					$last_mod_ts = time();

					$mark_as_voided = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = 'VOIDED', `last_mod_ts` = '[3]' WHERE `id` = '[4]'", $process_info['more_info'], $process_info['server_id'], $last_mod_ts, $tx_info['id']);
					$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - ".$tx_info['amount']."), `card_desc` = '', `transaction_id` = '' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
					$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - ".$tx_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $tx_info['check']);

					actionLogItGW($process_info, $void_action, "Check ".$tx_info['check']." - ".$pay_type." - ".priceFormatGW($tx_info['amount']));
		
					$rtn = array();
					$rtn[] = "1"; // 0
					$rtn[] = "VOIDED";
					$rtn[] = substr($cn, -4);
					$rtn[] = "APPROVED";
					$rtn[] = "VOIDED";
					$rtn[] = $card_type;
					$rtn[] = "";
					$rtn[] = "";
					$rtn[] = "";
					$rtn[] = "";
					$rtn[] = ""; // 10
					$rtn[] = "";
					$rtn[] = "";
					$rtn[] = "";
					$rtn[] = $last_mod_ts;
		
					return implode("|", $rtn);
				}

				break;
				
			case "Return" :

				$tgtranstype = "Return";
				$ext_data .= "<Force>T</Force>";
				if (empty($process_info['pnref'])) {
					$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $loc_id, $order_id, $process_info['check'], substr($cn, -4));
					$tx_info = mysqli_fetch_assoc($get_transaction_info);
					$process_info['pnref'] = $tx_info['transaction_id'];
				}
				$include_inv_num = true;
				$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
				if ((float)$process_info['refund_tip_amount'] != 0) $ext_data .= "<TipAmt>".$process_info['refund_tip_amount']."</TipAmt>";
				break;

			case "CaptureAll" :

				$tgtranstype = "CaptureAll";
				break;

			case "IssueGiftCard" :
			case "IssueLoyaltyCard" :

				$tgtranstype = "Activate";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = true;
				$dupe_info = checkForPreviousIssueOrReload($process_info);
				if (!empty($dupe_info)) return $dupe_info;
				break;

			case "ReloadGiftCard" :
			case "ReloadLoyaltyCard" :
	
				$tgtranstype = "Reload";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = true;
				$dupe_info = checkForPreviousIssueOrReload($process_info);
				if (!empty($dupe_info)) return $dupe_info;
				break;
				
			case "VoidGiftCardIssueOrReload" :
			case "VoidLoyaltyCardIssueOrReload" :

				$tgtranstype = "Void";
				$ext_data .= "<Force>T</Force>";
				break;

			case "DeactivateGiftCard" :
			case "DeactivateLoyaltyCard" :

				$tgtranstype = "Deactivate";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = false;
				break;

			case "GiftCardBalance" :
			case "LoyaltyCardBalance" :

				$tgtranstype = "Inquire";
				$ext_data .= "<Force>T</Force>";
				$send_amount = "";
				$include_inv_num = true;
				break;

			case "GiftCardSale" :
			case "LoyaltyCardSale" :

				$tgtranstype = "Redeem";
				$ext_data .= "<Force>T</Force>";
				$include_inv_num = true;
				if ($transtype == "LoyaltyCardSale") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
				break;

			case "GiftCardVoid" :
			case "LoyaltyCardVoid" :

				$tgtranstype = "Void";
				if ($transtype == "LoyaltyCardVoid") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
				break;

			case "GiftCardReturn" :
			case "LoyaltyCardReturn" :

				$tgtranstype = "Refund";
				$ext_data .= "<Force>T</Force>";
				if (empty($process_info['pnref'])) {
					$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $loc_id, $order_id, $process_info['check'], substr($cn, (strlen($cn) - 4), 4));
					$tx_info = mysqli_fetch_assoc($get_transaction_info);
					$process_info['pnref'] = $tx_info['transaction_id'];
				}
				$include_inv_num = true;
				if ($transtype == "LoyaltyCardReturn") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
				break;

			default :

				return "0|Unable to determine transaction type|||".$order_id;
				break;
		}

		if ($process_info['register'] == "Back End") $register_num = 100;
		else if ((strtoupper($process_info['register']) == strtoupper(speak("Not set"))) || ($process_info['register'] == "receipt")) $register_num = 1;
		else $register_num = str_replace("receipt", "", $process_info['register']);

		$ext_data .= "<RegisterNum>$register_num</RegisterNum>";
		if ($transtype != "Adjustment") $ext_data .= "<ServerID>".$process_info['server_id']."</ServerID>";
		
		$transaction_vars['swipe_grade'] = $swipe_grade;
		$transaction_vars['transtype'] = $transtype;
		
		$q_fields = "";
		$q_values = "";
		$keys = array_keys($transaction_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		
		$system_id = $transaction_vars['system_id'];
		
		$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";
	
		$postvars = array();
		$postvars['UserName']			= $process_info['username']; // posl4423 // posl6448 // Gieske // 
		$postvars['Password']			= $process_info['password']; // 8Balilavu // 8Balilavu // Password!01 //
		$postvars['TransType']			= $tgtranstype;
		if ($transtype == "CaptureAll") {
			$postvars['PaymentType']		= "All";
		}
		$postvars['CardNum']				= $send_card_number;
		$postvars['ExpDate']				= $send_exp_month.$send_exp_year;
		$postvars['MagData']				= $send_mag_data;
		if ($pay_type == "Card") {
			$postvars['NameOnCard']		= $process_info['name_on_card'];
			$postvars['Zip']				= "";
			$postvars['Street']			= "";
			$postvars['CVNum']			= $process_info['card_cvn'];
		}
		$postvars['Amount']				= ($transtype == "Adjustment")?"":$send_amount;
		$postvars['PNRef']				= $process_info['pnref'];
		$postvars['InvNum']				= ($include_inv_num)?str_replace("-", "", $loc_id.$order_id):"";
		$postvars['ExtData']				= $ext_data;
		
		$post_data = "";
		foreach ($postvars as $key => $val) {
			if ($post_data != "") $post_data .= "&";
			$post_data .= $key."=".rawurlencode($val);
		}

		ConnectionHub::closeAll();

		$ch = curl_init($posturl);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line for stage server
		$response = curl_exec($ch);
		curl_close($ch);

		$debug .= "\n\nPost Data:".$post_data;
		$debug .= "\n\n".$response;
				
		$xml = simplexml_load_string($response);
		
		$debug .= "\n\n".print_r($xml, true);

		$result = $xml->Result;
		$respmsg = $xml->RespMSG;
		$authcode = $xml->AuthCode;
		$new_pnref = $xml->PNRef;
		$message = $xml->Message;
		$r_ext_data = explode(",", $xml->ExtData);

		$a_ext_data = array();
		foreach ($r_ext_data as $ext) {
			$ext_parts = explode("=", $ext);
			if (count($ext_parts) >= 2) {
				$ext_parts1_parts = explode("<", $ext_parts[1]);
				$a_ext_data[$ext_parts[0]] = $ext_parts1_parts[0];
			}
		}
		if (isset($a_ext_data['CardType'])) $card_type = $a_ext_data['CardType'];

		//record_stats($process_info['company_id'], $data_name, $loc_id, "TGate", $reader, $transtype, $result, $respmsg, $card_type, $swiped, $pay_type);
				
		if (($respmsg == "") && ($message != "")) $respmsg = $message;

		$debug .= "\n\nTGATE\n\n$post_data\n\nRESULT: $result\nRESPONSE MESSAGE: $respmsg\n$authcode\n$new_pnref\n$card_type\n\n";
		if ($transtype == "CaptureAll") {
			//error_log("TGATE - BATCH XML RESPONSE:{$response}");
			$debug .= "BATCH XML RESPONSE:{$response}";
		}
		$approved = "0";
		if (strtoupper($respmsg)=="APPROVED" || ($result=="0" && (strstr($message, "BalanceAmount:") || strstr($message, "GiftCardBalanceAmount:") || strstr($message, "LoyaltyCardBalanceAmount:") || strstr($message, "Balance:") || strstr($message, "USD balance:") || strstr($message, "Points balance:") || ($respmsg=="" && $message=="")))) $approved = "1";	
		if ($location_info['gateway_debug'] == "1") gateway_debug($data_name, $loc_id, $log_gateway, $transtype, $approved, $debug);

		$lg_str = $log_gateway."[--CR--]";
		$lg_str .= "LOCATION: ".			$loc_id						." - ";
		$lg_str .= "REGISTER: ".			$process_info['register']	." - ";
		$lg_str .= "SERVER ID: ".		$process_info['server_id']	." - ";
		$lg_str .= "ORDER ID: ".			$order_id					." - ";
		$lg_str .= "CHECK : ".			$process_info['check']		." - ";
		$lg_str .= "AMOUNT: ".			$send_amount.$log_tip_amount." - ";
		$lg_str .= "TRANSTYPE: ".		$transtype					." - ";
		$lg_str .= "RESULT: ".			$result						." - ";
		$lg_str .= "PNREF: ".			$new_pnref					." - ";
		$lg_str .= "AUTHCODE: ".			$authcode					." - ";
		$lg_str .= "CARD TYPE: ".		$card_type					." - ";
		$lg_str .= "FIRST FOUR: ".		$first_four					." - ";
		$lg_str .= "LAST FOUR: ".		substr($cn, -4)				." - ";
		if ((int)$swiped > 0) {
			$lg_str.= "READER: ".		$reader						." - ";
		}
		$lg_str .= "SWIPED: ".			$swiped						."[--CR--]";
		$lg_str .= "SENT EXT_DATA: ".	$ext_data					."[--CR--]";
		$lg_str .= "RESPONSE MESSAGE: ".$respmsg						."[--CR--]";
		if (($message != "") && (strtolower(trim($message)) != strtolower(trim($respmsg)))) {
			$lg_str .= "MESSAGE: ".		$message					."[--CR--]";
		}
		$lg_str .= "RCVD EXT_DATA: ".	join(" - ", $r_ext_data)		."[--CR--]";

		write_log_entry("gateway", $data_name, $lg_str."\n");
		
		$rtn = array();

		if ($approved == "1") {

			$new_balance = "";
			$bonus_message = "";
			if (in_array($transtype, array("IssueGiftCard", "IssueLoyaltyCard", "ReloadGiftCard", "ReloadLoyaltyCard", "GiftCardSale", "LoyaltyCardSale", "GiftCardVoid", "LoyaltyCardVoid"))) {
				if (strstr(strtolower($respmsg), "balance")) {
					$msg_parts = explode(":", $respmsg);
					if (count($msg_parts) >= 2) $new_balance = trim($msg_parts[1]);
					if (strstr($transtype, "Loyalty") && strstr($new_balance, ".")) {
						$nb_parts = explode(".", $new_balance);
						$new_balance = $nb_parts[0];
					}
					if (strstr($transtype, "Void")) $process_info['info'] = $new_balance;
					else $process_info['more_info'] = $new_balance;
				}
				if (strstr(strtolower($respmsg), "congratulations")) $bonus_message = $respmsg;
			}
			
			$process_info['last_mod_ts'] = time();

			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

			if ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") {
				if (!empty($message)) {
					$split_message = explode(":", $message);
					$authcode = trim($split_message[1]);
					if (empty($authcode)) $authcode = "Unavailable";
				} else {
					$cents = substr($authcode, -2);
					$authcode = substr_replace($authcode, ".".$cents, -2);
				}
			}
						
			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $order_id;
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = $new_balance;
			$rtn[] = $bonus_message;
			$rtn[] = $first_four;
			$rtn[] = $process_info['last_mod_ts'];
			
		} else {
		
			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

			if (($message != "") && (strtolower(trim($message)) != strtolower(trim($respmsg)))) $respmsg .= " - ".$message;

			if (($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") && ($respmsg=="ALREADY ACTIVE" || strstr($message, "CARD ACTIVE")))  {
				
				$process_info['transtype'] = ($transtype == "IssueGiftCard")?"ReloadGiftCard":"ReloadLoyaltyCard";

				return process_tgate($process_info, $location_info);
			}
			
			$resptitle = "Declined";
			if (strpos($respmsg, "InvalidSecureTrack")) $respmsg = "Card read failed.\nPlease swipe card again.";
			else {
				if (strpos($respmsg, "exception") || strlen($respmsg)>125) $respmsg = "There is a problem with the gateway.";
				else if (empty($respmsg)) $respmsg = "No response received from gateway.";

				if ($result != "12") {
					$resptitle = "Error";
					$respmsg = ucfirst(strtolower($respmsg));
					$respmsg .= ". Please call BridgePay support at 866-322-9894.";
				}
			}
			
			$rtn[] = "0";
			$rtn[] = $respmsg;
			$rtn[] = $new_pnref;
			$rtn[] = $order_id;
			$rtn[] = "";
			$rtn[] = $resptitle;
		}

		return implode("|", $rtn);
	}

function process_tgate($process_info, $location_info) {

	global $tgate_server_url;
	global $tgate_gift_card_server_url;
	global $tgate_loyalty_card_server_url;
	global $server_order_prefix;

	$debug = print_r($process_info, true)."\n\n";

	$loc_id				= $process_info['loc_id'];
	$order_id			= $process_info['order_id'];
	$data_name			= $process_info['data_name'];
	$transtype			= $process_info['transtype'];
	$ext_data			= $process_info['ext_data'];
	$pay_type			= $process_info['set_pay_type'];
	$pay_type_id		= $process_info['set_pay_type_id'];
	$send_amount		= $process_info['card_amount'];
	$send_card_number	= $process_info['card_number'];
	$send_exp_month		= $process_info['exp_month'];
	$send_exp_year		= $process_info['exp_year'];
	$mag_data 			= $process_info['mag_data'];
	$reader				= $process_info['reader'];

	$log_gateway = "TGATE";
	if ($location_info['gateway'] == "PivotalPayments") { // un: hill2680 - pw: Stanmil3579

		$log_gateway = "PIVOTAL PAYMENTS";
		$tgate_server_url = "https://secure1.pivotalpayments.com/SmartPayments/transact.asmx/ProcessCreditCard";

	} else if ($data_name=="bfc_demo" || strpos($data_name, "hotel") !== false) {

		$tgate_server_url = "https://gateway.itstgate.com:10221/SmartPayments/transact3.asmx/ProcessCreditCard";
	}

	$special_designator = specialDesignatorForPayTypeID($pay_type_id);

	$force_track1 = false;
	if ($pay_type=="Gift Card" || $special_designator=="gift_card")
	{
		$posturl = $tgate_gift_card_server_url;
		$force_track1 = true;
	}
	else if ($pay_type=="Loyalty Card" || $special_designator=="loyalty_card")
	{
		$posturl = $tgate_loyalty_card_server_url;
		$force_track1 = true;
	}
	else
	{
		$posturl = $tgate_server_url;
	}

	$debug .= $posturl."\n\n";

	$swiped = "0";
	$swipe_grade = "H";
	$entry_mode = "MANUAL";
	$track2 = "";
	$send_mag_data = "";
	if ($mag_data) {
		$swiped = "1";
		$swipe_grade = "G";
		$send_mag_data = $mag_data;
	}
	$log_tip_amount = "";

	$cn = $send_card_number; // keep for response string
	$first_four = substr($cn, 0, 4);
	$card_type = getCardTypeFromNumber($cn);

	$processed_mag_data = false;
	if ($mag_data && ($reader=="idynamo" || $reader=="udynamo")) {

		$processed_mag_data = true;
		$valid_mag_data = (substr($mag_data, 0, 2)=="%B" || ((substr($mag_data, 0, 1)==";" || substr($mag_data, 0, 3)=="%E?") && $pay_type_id!="2"));
		$track1good = false;
		$track2good = false;
		$split_mag_data = explode("|", $mag_data);
		$encrypted_track1 = $split_mag_data[2];
		$encrypted_track2 = $split_mag_data[3];
		$dukpt_ksn = $split_mag_data[9];
		$ext_data = "<SecureFormat>MagneSafeV1</SecureFormat>";
		if (!strstr($encrypted_track1, "E?") && ($encrypted_track1 != "")) {
			$ext_data .= "<Track1>".$encrypted_track1."</Track1>";
			$swipe_grade = "C";
			$track1good = true;
		} else if ($force_track1) {
			$ext_data .= "<Track1></Track1>";
		}
		if (!strstr($encrypted_track2, "E?") && ($encrypted_track2 != "")) {
			$ext_data .= "<Track2>".$encrypted_track2."</Track2>";
			if ($swipe_grade == "C") $swipe_grade = "A";
			else $swipe_grade = "B";
			$track2good = true;
		}
		$ext_data .= "<SecurityInfo>".$dukpt_ksn."</SecurityInfo>";
		$send_card_number = "";
		$send_exp_month = "";
		$send_exp_year = "";

		if (!$valid_mag_data || (!$track1good && !$track2good)) {

			if ($location_info['gateway_debug'] == "1") gateway_debug($data_name, $loc_id, $log_gateway, $transtype, "BR", $debug);

			//record_stats($process_info['company_id'], $data_name, $loc_id, "TGate", $reader, $transtype, "99999", "Read Error - Missing track 1 and 2", "", $swiped, $pay_type);

			$lg_str = $log_gateway."[--CR--]";
			$lg_str .= "LOCATION: ".		$loc_id						." - ";
			$lg_str .= "REGISTER: ".		$process_info['register']	." - ";
			$lg_str .= "SERVER ID: ".	$process_info['server_id']	." - ";
			$lg_str .= "ORDER ID: ".		$order_id	." - ";
			$lg_str .= "AMOUNT: ".		$process_info['card_amount']	." - ";
			$lg_str .= "TRANSTYPE: ".	$transtype					." - ";
			$lg_str .= "CARD TYPE: ??? - ";
			$lg_str .= "LAST FOUR: ".	substr($cn, -4)				." - ";
			$lg_str .= "READER: ".		$reader						."[--CR--]";
			$lg_str .= "EXT_DATA: ".		$ext_data					."[--CR--]";
			$lg_str .= "MAG DATA: ".		$mag_data					."[--CR--]";
			write_log_entry("gateway", $data_name, $lg_str."\n");

			$rtn = array();
			$rtn[] = "0";
			$rtn[] = "Card read failed.\nPlease try again.";
			$rtn[] = "";
			$rtn[] = $order_id;
			$rtn[] = "";
			$rtn[] = "Error";
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = "";

			return implode("|", $rtn);
		}
		$send_mag_data = "";
		$entry_mode = "MAGNETICSTRIPE";

	} else if ($mag_data && $process_info['encrypted']=="1") {

		$not_really_encrypted = ($reader=="idtech_unimag" && substr($mag_data, 0, 1)==";");

		if (!$not_really_encrypted) { // check to make sure mag_data is actually encrypted

			$processed_mag_data = true;
			$swipe_grade = "B";
			$encrypted_info = idtech_encrypted($mag_data, $process_info, $location_info);
			$encrypted_track1 = hex_string($encrypted_info['track1']);
			$encrypted_track2 = hex_string($encrypted_info['track2']);
			$dukpt_ksn = hex_string($encrypted_info['ksn']);
			if ((empty($encrypted_track1) && empty($encrypted_track2)) || empty($dukpt_ksn)) {

				if ($location_info['gateway_debug'] == "1") gateway_debug($data_name, $loc_id, $log_gateway, $transtype, "BR", $debug);

				$lg_str = $log_gateway."[--CR--]";
				$lg_str .= "LOCATION: ".		$loc_id						." - ";
				$lg_str .= "REGISTER: ".		$process_info['register']	." - ";
				$lg_str .= "SERVER ID: ".	$process_info['server_id']	." - ";
				$lg_str .= "ORDER ID: ".		$order_id	." - ";
				$lg_str .= "AMOUNT: ".		$process_info['card_amount']	." - ";
				$lg_str .= "TRANSTYPE: ".	$transtype					." - ";
				$lg_str .= "CARD TYPE: ??? - ";
				$lg_str .= "LAST FOUR: ".	substr($cn, - 4)				." - ";
				$lg_str .= "READER: ".		$reader						."[--CR--]";
				$lg_str .= "MAG DATA: ".		$mag_data					."[--CR--]";
				write_log_entry("gateway", $data_name, $lg_str."\n");

				$rtn = array();
				$rtn[] = "0";
				$rtn[] = "Card read failed.\nPlease try again.";
				$rtn[] = "";
				$rtn[] = $order_id;
				$rtn[] = "";
				$rtn[] = "Error";
				$rtn[] = "";
				$rtn[] = "";
				$rtn[] = "";

				return implode("|", $rtn);
			}

			$ext_data = "<SecureFormat>SecureMag</SecureFormat>";
			if (!empty($encrypted_track1)) {
				$ext_data .= "<Track1>".$encrypted_track1."</Track1>";
				$swipe_grade = "C";
			} else if ($force_track1) $ext_data .= "<Track1></Track1>";
			if (!empty($encrypted_track2)) {
				$ext_data .= "<Track2>".$encrypted_track2."</Track2>";
				if ($swipe_grade == "C") $swipe_grade = "A";
				else $swipe_grade = "B";
			}
			$ext_data .= "<SecurityInfo>".$dukpt_ksn."</SecurityInfo>";
			$send_card_number = "";
			$send_exp_month = "";
			$send_exp_year = "";
			$send_mag_data = "";
			$entry_mode = "MAGNETICSTRIPE";
		}

	} else if ($mag_data && $process_info['encrypted']=="2") {

		$processed_mag_data = true;
		$swipe_grade = "B";

		require_once(dirname(__FILE__)."/mercury_lls_func.php");
		$myLLSSwipe = new IDTechLLS();
		$myLLSSwipe->setMagData($mag_data);
		$encrypted_info = $myLLSSwipe->getEncryptedInfoBlock();

		if(empty($encrypted_info['ksn'])){
			if(empty($encrypted_info['track2'])){error_log(__FILE__ . ' ' . __FUNCTION__.' track2 empty? ');}
			$send_mag_data = str_replace(";", "", hex2bin($encrypted_info['track2']));
			if(empty($encrypted_info['track2'])){error_log(__FILE__ . ' ' . __FUNCTION__.' send_mag_data empty? ');}
			$entry_mode = "MAGNETICSTRIPE";
			$swipe_grade = "E";
		}else{
			$encrypted_track1 = $encrypted_info['track1'];
			$encrypted_track2 = $encrypted_info['track2'];

			$ext_data = "<SecureFormat>SecureMag</SecureFormat>";
			if (!empty($encrypted_track1)) {
				$ext_data .= "<Track1>".$encrypted_track1."</Track1>";
				$swipe_grade = "C";
			} else if ($force_track1) $ext_data .= "<Track1></Track1>";
			if (!empty($encrypted_track2)) {
				$ext_data .= "<Track2>".$encrypted_track2."</Track2>";
				if ($swipe_grade == "C") $swipe_grade = "A";
				else $swipe_grade = "B";
			}

			$ext_data .= "<SecurityInfo>".$encrypted_info['ksn']."</SecurityInfo>";
			$send_card_number = "";
			$send_exp_month = "";
			$send_exp_year = "";
			$send_mag_data = "";
			$entry_mode = "MAGNETICSTRIPE";
		}
	}

	if ($mag_data!="" && !$processed_mag_data) {

		$tracks = explode("?", $mag_data);
		$track2_index = 1;
		if (strstr($tracks[0], "=")) {
			$track2_index = 0;
		}
		if (count($tracks) > $track2_index) {
			$track2 = $tracks[$track2_index];
			if (($track2 != "") && ($track2 != "ERROR")) {
				$send_mag_data = str_replace(";", "", $track2);
				$entry_mode = "MAGNETICSTRIPE";
				$swipe_grade = "E";
			} else $send_mag_data = "";
		}
	}

	$check_for_duplicate = false;
	$create_transaction_record = false;
	$include_inv_num = false;
	if ($transtype == "") $transtype = "Sale";

	switch ($transtype) {

		case "" :
		case "Sale" :

			$tgtranstype = "Sale";
			$ext_data .= "<Force>T</Force>";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$include_inv_num = true;
			break;

		case "Auth" :
		case "AuthForTab" :

			$tgtranstype = "Auth";
			$ext_data .= "<Force>T</Force>";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$include_inv_num = true;
			if ($order_id == "0") {
				$order_id = create_new_order_record($process_info, $location_info);
				$process_info['order_id'] = $order_id;
			}
			if ($transtype=="AuthForTab" && (float)$send_amount==0) {
				$process_info['card_amount'] = $location_info['default_preauth_amount'];
				$send_amount = $process_info['card_amount'];
			}
			break;

		case "PreAuthCapture" :
		case "PreAuthCaptureForTab" :

			$tgtranstype = "Force";
			$check_for_duplicate = true;
			$get_transaction_info = lavu_query("SELECT `auth`, `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
			$tx_info = mysqli_fetch_assoc($get_transaction_info);
			if (!empty($process_info['card_amount'])) $send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
			else $send_amount = number_format(($tx_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
			if ($cn == "") $cn = $tx_info['card_desc'];
			$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			$include_inv_num = true;
			break;

		case "Adjustment" :

			$tgtranstype = "Adjustment";
			if (isset($location_info['tip_adjust_send_total']) && ($location_info['tip_adjust_send_total'] == "1")) {
				$get_transaction_info = lavu_query("SELECT `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				$tx_info = mysqli_fetch_assoc($get_transaction_info);
				$send_amount = number_format(($tx_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				if ($cn == "") $cn = $tx_info['card_desc'];
			}
			$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
			break;

		case "Void" :
		case "VoidPreAuthCapture" :

			$tgtranstype = "Void";
			$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `action`, `auth`, `card_type`, `check`, `amount`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
			$tx_info = mysqli_fetch_assoc($get_transaction_info);
			$void_action = ($tx_info['action'] == "Sale")?"Payment Voided":"Refund Voided";
			$card_type = $tx_info['card_type'];
			$first_four = $tx_info['first_four'];
			$cn = $tx_info['card_desc'];

			if (($tx_info['transtype']=="Auth" || $tx_info['transtype']=="AuthForTab") && ($tx_info['auth']=="1" || $tx_info['auth']=="2")) {

				$last_mod_ts = time();

				$mark_as_voided = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = 'VOIDED', `last_mod_ts` = '[3]' WHERE `id` = '[4]'", $process_info['more_info'], $process_info['server_id'], $last_mod_ts, $tx_info['id']);
				$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - ".$tx_info['amount']."), `card_desc` = '', `transaction_id` = '' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
				$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - ".$tx_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $tx_info['check']);

				actionLogItGW($process_info, $void_action, "Check ".$tx_info['check']." - ".$pay_type." - ".priceFormatGW($tx_info['amount']));

				$rtn = array();
				$rtn[] = "1"; // 0
				$rtn[] = "VOIDED";
				$rtn[] = substr($cn, -4);
				$rtn[] = "APPROVED";
				$rtn[] = "VOIDED";
				$rtn[] = $card_type;
				$rtn[] = "";
				$rtn[] = "";
				$rtn[] = "";
				$rtn[] = "";
				$rtn[] = ""; // 10
				$rtn[] = "";
				$rtn[] = "";
				$rtn[] = "";
				$rtn[] = $last_mod_ts;

				return implode("|", $rtn);
			}
			$create_transaction_record = true;

			break;

		case "Return" :
			error_log(__FILE__ . " " . __LINE__ . " In Return");
			$tgtranstype = "Return";
			$ext_data .= "<Force>T</Force>";
			if (empty($process_info['pnref'])) {
				$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $loc_id, $order_id, $process_info['check'], substr($cn, -4));
				$tx_info = mysqli_fetch_assoc($get_transaction_info);
				$process_info['pnref'] = $tx_info['transaction_id'];
			}
			$create_transaction_record = true;
			$include_inv_num = true;
			$send_amount = number_format(((float)$send_amount + (float)$process_info['refund_tip_amount']), 2);
			if ((float)$process_info['refund_tip_amount'] != 0) $ext_data .= "<TipAmt>".$process_info['refund_tip_amount']."</TipAmt>";
			break;

		case "CaptureAll" :

			$tgtranstype = "CaptureAll";
			break;

		case "IssueGiftCard" :
		case "IssueLoyaltyCard" :

			$tgtranstype = "Activate";
			$ext_data .= "<Force>T</Force>";
			$create_transaction_record = true;
			$include_inv_num = true;
			$dupe_info = checkForPreviousIssueOrReload($process_info);
			if (!empty($dupe_info)) return $dupe_info;
			break;

		case "ReloadGiftCard" :
		case "ReloadLoyaltyCard" :

			$tgtranstype = "Reload";
			$ext_data .= "<Force>T</Force>";
			$create_transaction_record = true;
			$include_inv_num = true;
			$dupe_info = checkForPreviousIssueOrReload($process_info);
			if (!empty($dupe_info)) return $dupe_info;
			break;

		case "VoidGiftCardIssueOrReload" :
		case "VoidLoyaltyCardIssueOrReload" :

			$tgtranstype = "Void";
			$ext_data .= "<Force>T</Force>";
			$create_transaction_record = true;
			break;

		case "DeactivateGiftCard" :
		case "DeactivateLoyaltyCard" :

			$tgtranstype = "Deactivate";
			$ext_data .= "<Force>T</Force>";
			$create_transaction_record = true;
			$include_inv_num = false;
			break;

		case "GiftCardBalance" :
		case "LoyaltyCardBalance" :

			$tgtranstype = "Inquire";
			$ext_data .= "<Force>T</Force>";
			$send_amount = "";
			$include_inv_num = true;
			break;

		case "GiftCardSale" :
		case "LoyaltyCardSale" :

			$tgtranstype = "Redeem";
			$ext_data .= "<Force>T</Force>";
			$check_for_duplicate = true;
			$create_transaction_record = true;
			$include_inv_num = true;
			if ($transtype == "LoyaltyCardSale") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
			break;

		case "GiftCardVoid" :
		case "LoyaltyCardVoid" :

			$tgtranstype = "Void";
			$create_transaction_record = true;
			if ($transtype == "LoyaltyCardVoid") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
			break;

		case "GiftCardReturn" :
		case "LoyaltyCardReturn" :

			$tgtranstype = "Refund";
			$ext_data .= "<Force>T</Force>";
			if (empty($process_info['pnref'])) {
				$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $loc_id, $order_id, $process_info['check'], substr($cn, (strlen($cn) - 4), 4));
				$tx_info = mysqli_fetch_assoc($get_transaction_info);
				$process_info['pnref'] = $tx_info['transaction_id'];
			}
			$create_transaction_record = true;
			$include_inv_num = true;
			if ($transtype == "LoyaltyCardReturn") $send_amount = number_format(($process_info['card_amount'] * 100), 0, "", "");
			break;

		default :

			return "0|Unable to determine transaction type|||".$order_id;
			break;
	}

	if ($process_info['register'] == "Back End") $register_num = 100;
	else if ((strtoupper($process_info['register']) == strtoupper(speak("Not set"))) || ($process_info['register'] == "receipt")) $register_num = 1;
	else $register_num = str_replace("receipt", "", $process_info['register']);

	$ext_data .= "<RegisterNum>$register_num</RegisterNum>";
	if ($transtype != "Adjustment") $ext_data .= "<ServerID>".$process_info['server_id']."</ServerID>";

	$transaction_vars = generate_transaction_vars($process_info, $location_info, $cn, $card_type, $transtype, $swipe_grade, $pay_type, $pay_type_id, $process_info['card_amount']);


	$q_fields = "";
	$q_values = "";
	$keys = array_keys($transaction_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}

	$system_id = 0;
	if ($create_transaction_record) {
		$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
		if (!$preliminary_record_transaction) return "0|Failed to create transaction record.||".$order_id;
		$system_id = lavu_insert_id();
	}

	$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";

	$postvars = array();
	$postvars['UserName']			= $process_info['username']; // posl4423 // posl6448 // Gieske // 
	$postvars['Password']			= $process_info['password']; // 8Balilavu // 8Balilavu // Password!01 //
	$postvars['TransType']			= $tgtranstype;
	if ($transtype == "CaptureAll") {
		$postvars['PaymentType']		= "All";
	}
	$postvars['CardNum']				= $send_card_number;
	$postvars['ExpDate']				= $send_exp_month.$send_exp_year;
	$postvars['MagData']				= $send_mag_data;
	if ($pay_type == "Card") {
		$postvars['NameOnCard']		= $process_info['name_on_card'];
		$postvars['Zip']				= "";
		$postvars['Street']			= "";
		$postvars['CVNum']			= $process_info['card_cvn'];
	}
	$postvars['Amount']				= ($transtype == "Adjustment")?"":$send_amount;
	$postvars['PNRef']				= $process_info['pnref'];
    $postvars['InvNum']				= ($include_inv_num)?str_replace("-", "", substr($order_id, 1)):"";
	$postvars['ExtData']				= $ext_data;

	if($postvars['TransType']=="Redeem" && $pay_type=="Gift Card"){
		$xmlObj = checkGiftCardBalance($posturl, $postvars);
		if(property_exists($xmlObj, "Message")){
			$splitArray = explode(":",$xmlObj->Message);
			if(isset($splitArray[1])){
				$balanceInGiftCard = floatval(trim($splitArray[1]));
			}
		}
		$resultValue = (property_exists($xmlObj, "Result")) ? $xmlObj->Result : "no result";
	
		if($resultValue == 0){
			if((abs($balanceInGiftCard) <= 0)||(abs($balanceInGiftCard) < floatval($postvars['Amount']))){
				lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
				$rtn[] = "0";
				$rtn[] = "Insufficient balance: ".trim($splitArray[1])."!";
				$rtn[] = (property_exists($xmlObj, "PNRef")) ? $xmlObj->PNRef:"";
				$rtn[] = $postvars['InvNum'];
				$rtn[] = "";
				$rtn[] = "Please Check Balance";
				return implode("|", $rtn);
			}
		}else{
			lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
			$rtn[] = "0";
			$rtn[] = "";
			$rtn[] = (property_exists($xmlObj, "PNRef")) ? $xmlObj->PNRef:"";
			$rtn[] = $postvars['InvNum'];
			$rtn[] = "";
			$rtn[] = "Invalid Gift Card!";
			return implode("|", $rtn);
		}
	}
	
	$post_data = "";
	foreach ($postvars as $key => $val) {
		if ($post_data != "") $post_data .= "&";
		$post_data .= $key."=".rawurlencode($val);
	}

	ConnectionHub::closeAll();

	$ch = curl_init($posturl);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line for stage server\

	#Wrap Tgate/BridgePay requests in circuit breaker.
	try{
		$circuitBreaker = new Lavu\CircuitBreaker\CircuitBreaker('BridgePay');
	}catch(Exception $e){
		$rtn[] = "0";
		$rtn[] = $e->getMessage();
		$rtn[] = "";
		$rtn[] = "";	
		$rtn[] = "";
		$rtn[] = "Service is not registered in circuit breaker";
		return implode("|", $rtn);
	}
	#check circut is closed or open ?
	if(!empty($circuitBreaker) && $circuitBreaker->isCircuitClosed()) {
		$response = curl_exec($ch);
		$httpstatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		$debug .= "\n\nPost Data:".$post_data;
		$debug .= "\n\n".$response;

		$xml = simplexml_load_string($response);

		$debug .= "\n\n".print_r($xml, true);

		$result = $xml->Result;
		$respmsg = $xml->RespMSG;
		$authcode = $xml->AuthCode;
		$new_pnref = $xml->PNRef;
		$message = $xml->Message;

		$r_ext_data = explode(",", $xml->ExtData);
		$a_ext_data = array();
		foreach ($r_ext_data as $ext) {
			$ext_parts = explode("=", $ext);
			if (count($ext_parts) >= 2) {
				$ext_parts1_parts = explode("<", $ext_parts[1]);
				$a_ext_data[$ext_parts[0]] = $ext_parts1_parts[0];
			}
		}
		if(!isset($process_info['authcode']) || $process_info['authcode'] == '') {
			$process_info['authcode'] = $authcode;
		}
		if (isset($a_ext_data['CardType'])) $card_type = $a_ext_data['CardType'];

		//record_stats($process_info['company_id'], $data_name, $loc_id, "TGate", $reader, $transtype, $result, $respmsg, $card_type, $swiped, $pay_type);

		if (($respmsg == "") && ($message != "")) $respmsg = $message;

		$debug .= "\n\nTGATE\n\n$post_data\n\nRESULT: $result\nRESPONSE MESSAGE: $respmsg\n$authcode\n$new_pnref\n$card_type\n\n";
		if ($transtype == "CaptureAll") {
			//error_log("TGATE - BATCH XML RESPONSE:{$response}");
			$debug .= "BATCH XML RESPONSE:{$response}";
		}
		$approved = "0";
		if (strtoupper($respmsg)=="APPROVED" || ($result=="0" && (strstr($message, "BalanceAmount:") || strstr($message, "GiftCardBalanceAmount:") || strstr($message, "LoyaltyCardBalanceAmount:") || strstr($message, "Balance:") || strstr($message, "USD balance:") || strstr($message, "Points balance:") || ($respmsg=="" && $message=="")))) $approved = "1";
		if ($location_info['gateway_debug'] == "1") gateway_debug($data_name, $loc_id, $log_gateway, $transtype, $approved, $debug);

		$lg_str = $log_gateway."[--CR--]";
		$lg_str .= "LOCATION: ".			$loc_id						." - ";
		$lg_str .= "REGISTER: ".			$process_info['register']	." - ";
		$lg_str .= "SERVER ID: ".		$process_info['server_id']	." - ";
		$lg_str .= "ORDER ID: ".			$order_id					." - ";
		$lg_str .= "CHECK : ".			$process_info['check']		." - ";
		$lg_str .= "AMOUNT: ".			$send_amount.$log_tip_amount." - ";
		$lg_str .= "TRANSTYPE: ".		$transtype					." - ";
		$lg_str .= "RESULT: ".			$result						." - ";
		$lg_str .= "PNREF: ".			$new_pnref					." - ";
		$lg_str .= "AUTHCODE: ".			$authcode					." - ";
		$lg_str .= "CARD TYPE: ".		$card_type					." - ";
		$lg_str .= "FIRST FOUR: ".		$first_four					." - ";
		$lg_str .= "LAST FOUR: ".		substr($cn, -4)				." - ";
		if ((int)$swiped > 0) {
			$lg_str.= "READER: ".		$reader						." - ";
		}
		$lg_str .= "SWIPED: ".			$swiped						."[--CR--]";
		$lg_str .= "SENT EXT_DATA: ".	$ext_data					."[--CR--]";
		$lg_str .= "RESPONSE MESSAGE: ".$respmsg						."[--CR--]";
		if (($message != "") && (strtolower(trim($message)) != strtolower(trim($respmsg)))) {
			$lg_str .= "MESSAGE: ".		$message					."[--CR--]";
		}
		$lg_str .= "RCVD EXT_DATA: ".	join(" - ", $r_ext_data)		."[--CR--]";

		write_log_entry("gateway", $data_name, $lg_str."\n");

		$rtn = array();

		if ($approved == "1") {

			$new_balance = "";
			$bonus_message = "";
			if (in_array($transtype, array("IssueGiftCard", "IssueLoyaltyCard", "ReloadGiftCard", "ReloadLoyaltyCard", "GiftCardSale", "LoyaltyCardSale", "GiftCardVoid", "LoyaltyCardVoid"))) {
				if (strstr(strtolower($respmsg), "balance")) {
					$msg_parts = explode(":", $respmsg);
					if (count($msg_parts) >= 2) $new_balance = trim($msg_parts[1]);
					if (strstr($transtype, "Loyalty") && strstr($new_balance, ".")) {
						$nb_parts = explode(".", $new_balance);
						$new_balance = $nb_parts[0];
					}
					if (strstr($transtype, "Void")) $process_info['info'] = $new_balance;
					else $process_info['more_info'] = $new_balance;
				}
				if (strstr(strtolower($respmsg), "congratulations")) $bonus_message = $respmsg;
			}

			$process_info['last_mod_ts'] = time();

			update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

			if ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") {
				if (!empty($message)) {
					$split_message = explode(":", $message);
					$authcode = trim($split_message[1]);
					if (empty($authcode)) $authcode = "Unavailable";
				} else {
					$cents = substr($authcode, -2);
					$authcode = substr_replace($authcode, ".".$cents, -2);
				}
				$new_balance=$authcode;
			}
	//	error_log(__FILE__ . " " . __LINE__ . " " . "Authcode, before returning: " . $authcode);
			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $order_id;
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = $new_balance;
			$rtn[] = $bonus_message;
			$rtn[] = $first_four;
			$rtn[] = $process_info['last_mod_ts'];
			$rtn[] = "";//15
			$rtn[] = "";//16
			$rtn[] = "";//17
			$rtn[] = "";//18
			$rtn[] = $process_info['name_on_card'];

			#close circuit 
			$circuitBreaker->serviceSuccess();
		} else {

			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

			if (($message != "") && (strtolower(trim($message)) != strtolower(trim($respmsg)))) $respmsg .= " - ".$message;

			if (($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") && ($respmsg=="ALREADY ACTIVE" || strstr($message, "CARD ACTIVE")))  {

				$process_info['transtype'] = ($transtype == "IssueGiftCard")?"ReloadGiftCard":"ReloadLoyaltyCard";

				return process_tgate($process_info, $location_info);
			}

			$resptitle = "Declined";
			/*check error code for request timeout or connection timeout
			* connection timeout error code 408
			* request timeout error code 522
			*/

			if($httpstatus == 408 || $httpstatus == 522) { 
				$resptitle = "Timeout";
				$respmsg = "Request timed out.\nPlease try again.";
				$circuitBreaker->serviceFailed();
			} elseif (strpos($respmsg, "InvalidSecureTrack")) {
				$respmsg = "Card read failed.\nPlease swipe card again.";
			} else {
				if (strpos($respmsg, "exception") || strlen($respmsg)>125) $respmsg = "There is a problem with the gateway.";
				else if (empty($respmsg)) $respmsg = "No response received from gateway.";

				if ($result != "12") {
					$resptitle = "Error";
					$respmsg = ucfirst(strtolower($respmsg));
					$respmsg .= ". Please call BridgePay support at 866-322-9894.";
				}
			}

			$rtn[] = "0";
			$rtn[] = $respmsg;
			$rtn[] = $new_pnref;
			$rtn[] = $order_id;
			$rtn[] = "";
			$rtn[] = $resptitle;
		}

	} else {
		#if circut is open
		$rtn[] = "0";
		$rtn[] = "Request timed out.\nPlease try again.";
		$rtn[] = "";
		$rtn[] = "";	
		$rtn[] = "";
		$rtn[] = "Timeout";
	}
	return implode("|", $rtn);
}

/**
 * This function checks balance in Gift Card
 * @param string $postUrl
 * @param array $postParams
 * @return float
 */
function checkGiftCardBalance( $postUrl, $postParams ){
	//Unset variables which we don't want to post;
	if(isset($postParams['NameOnCard'])){
		unset($postParams['NameOnCard']);
	}

	if(isset($postParams['Zip'])){
		unset($postParams['Zip']);
	}

	if(isset($postParams['Street'])){
		unset($postParams['Street']);
	}

	if(isset($postParams['CVNum'])){
		unset($postParams['CVNum']);
	}
	$postParams['TransType'] = "Inquire";
	$postParams['Amount'] = "";

	$postVarString = "";
	foreach ($postParams as $key => $val) {
		if ($postVarString != ""){
			$postVarString .= "&";
		}
		$postVarString .= $key."=".rawurlencode($val);
	}

	$xmlResponse = sendPostRequest($postUrl, $postVarString);
	$xmlObj = simplexml_load_string($xmlResponse);
	//trigger_error(var_export($xmlObj,true),E_USER_NOTICE);
	return $xmlObj;
}

/**
 * Curl Call for TGate Gateway
 * @param string $postUrl
 * @param string $postVarString
 * @return XML
 */
function sendPostRequest($postUrl, $postVarString){
	$ch = curl_init($postUrl);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postVarString);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
}
?>