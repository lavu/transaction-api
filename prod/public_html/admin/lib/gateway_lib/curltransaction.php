<?php

/***
 *	Outlines a Basic CURL Transaction Setup and Processing functions
 **/
class CurlTransaction
{
	protected $url;
	protected $isPost;
	protected $headers;
	protected $variables;
	protected $error;
	protected $connection_info;
	protected $response;
	protected $username;
	protected $password;
	
	public function __construct(){
		$this->response           = null;
		$this->error              = null;
		$this->connection_info    = null;
		$this->url                = "";
		$this->isPost             = false;
		$this->headers 			  = array();
		$this->variables          = array();
		$this->variables['GET']   = array();
		$this->variables['POST']  = array();
		
	}
	
	public function addVariable($varName, $varValue, $useGET=true){
		if($useGET){
			$this->variables['GET'][$varName] = $varValue;
		}
		else{
			if($varName){
				$this->variables['POST'][$varName] = $varValue;
			}
			else{
				$this->variables['POST'] = $varValue;
			}
		}
	}
	
	public function getURL(){
		return $this->url;
	}
	
	public function setURL($url){
		$this->url = $url;
	}

	public function addHeader($header){
		$this->headers[] = $header;
	}

	public function setUsername( $username ){
		$this->username = $username;
	}

	public function setPassword( $password ) {
		$this->password = $password;
	}
	
	public function isPost(){
		return $this->isPost;
	}
	
	public function setPost($isPost){
		$this->isPost = $isPost;
	}
	
	public function getVariables(){
		return $this->variables;
	}
	
	public function getGETVariables(){
		return $this->variables['GET'];
	}
	
	public function getPOSTVariables(){
		return $this->variables['POST'];
	}
	
	public function clearVariables(){
		unset($this->variables);
		$this->variables = array();
		$this->variables['GET'] = array();
		$this->variables['POST'] = array();
	}
	
	public function hasError(){
		return $this->error != null;
	}
	
	public function getError(){
		return $this->error;
	}
	
	public function getConnectionInfo(){
		return $this->connection_info;
	}
	
	public function getResponse(){
		return $this->response;
	}
	
	public function validate(){
		return true;
	}
	
	public function process(){
		if(!$this->url || $this->url == ""){
			$this->error = "No URL Specified.";
			return null;
		}
		
		if(!$this->validate()){
			if(!$this->error){
				$this->error = "Request Failed Validation.";
			}
			return null;
		}
	
		$ch = curl_init();
		if(!$ch){
			$this->error = "Unable to Create Connection.";
			return null;
		}
		
		$url = $this->url;
		$curl_vars = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => 0
		);

		{ //HEADERS
			if( count($this->headers) ){
				$curl_vars[CURLOPT_HTTPHEADER] = $this->headers;
			}
		} //HEADERS END

		{// AUHTORIZATION
			if( $this->username && $this->password) {
				$curl_vars[CURLOPT_USERPWD] = "{$this->username}:{$this->password}";
			}
		}// AUHTORIZATION END

		#pragma mark Variables BEGIN
		{// VARIABLES
			if($this->isPost){
				$curl_vars[CURLOPT_POST] = 1;
				if( is_array($this->variables['POST']) ){
					$values = array();
					foreach($this->variables['POST'] as $varName => $varVal){
						$values[] = $varName . "=" . $varVal;
					}
					$values = implode("&", $values);
					$curl_vars[CURLOPT_POSTFIELDS] = $values;
				} else {
					$curl_vars[CURLOPT_POSTFIELDS] = $this->variables['POST'];
				}
			}
			
			$values = array();
			foreach($this->variables['GET'] as $varName => $varVal){
				$values[] = urlencode($varName) . "=" . urlencode($varVal);
			}
			$values = implode("&", $values);
			if($values) {
				$url = $url . "?" . $values;
			}
		}// VARIABLES END
		#pragma mark Variables END
		
		$curl_vars[CURLOPT_URL] = $url;
		
		foreach($curl_vars as $type => $var){
			curl_setopt($ch, $type, $var);
		}
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		$response = curl_exec($ch);
		$this->connection_info = curl_getinfo($ch);
		$this->connection_info['postfields'] = $this->variables['POST'];
		
		if($this->connection_info['http_code'] < 200  || $this->connection_info['http_code'] >= 400){
			//Log the Error
			$this->error = curl_error($ch);
			if(!$this->error){
				$this->error = "Connection Error";
			}
			$this->connection_info['response'] = $response;
			curl_close($ch);
			return null;
		}
		curl_close($ch);
		
		$this->response = $response;
		return $response;		
	}
}