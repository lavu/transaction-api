<?php
	//ini_set('include_path', "/Volumes/Macintosh HD/Users/tschnepp/Documents/Sites/Lavu Backend/dev/lib/gateway_lib/mercury_loyalty/");
	ini_set('display_errors','1');
	require_once("comm/response/tag_decoder.php");
	require_once("comm/setup/mercury_loyalty.php");
	require_once("comm/response/mercury_responsehandler.php");

	$mercuryFactory = new MLoyalty();
	if(isset($_REQUEST['mercury_api_key'])){
		$mercuryFactory->setAPIKey($_REQUEST['mercury_api_key']);
	}
	
	if(isset($_REQUEST['mercury_api_secret'])){
		$mercuryFactory->setAPISecret($_REQUEST['mercury_api_secret']);
	}
	$comm;
	 
	if($_REQUEST['mcmd'] == "getprogramdescription"){
		$comm = $mercuryFactory->createGetProgramDescription();
	}
	//Gets Program Information
	
	else if($_REQUEST['mcmd'] == "getstatus"){
		$comm = $mercuryFactory->createGetStatus();
		$comm->addVariable("customeridentifier","5053630644");
	}

	//Gets the Customer's Status


	else if($_REQUEST['mcmd'] == "addcredits"){
		$comm = $mercuryFactory->createAddCredits();
		$comm->addVariable("customeridentifier","5053630644");
		//$comm->addVariable("TestMode", true); //For Testing (Doesn't Add to Total)
		$comm->addVariable("client", "Test Lavu");
		$comm->addVariable("description", "Test");
		$comm->addVariable("employee_id", "JiveTurkey");
		$comm->addVariable("station_id", "Station 1");
		$comm->addVariable("ticket_id", "1-2");
		$comm->addVariable("revenue", "10.0");
		$comm->addVariable("units", "22.0");
		//Array
		//	(
		//	    [decimal] => 3.00
		//	)
	}

  //Add Credits Information


	else if ($_REQUEST['mcmd'] == "closetransactionwithsku"){
		$comm = $mercuryFactory->createCloseTransactionWithSKU();
		//$comm->addVariable("revenue","10.0");
		//$comm->addVariable("guestcount", "1");
		//$comm->addVariable("employee_id", "Theo");
		//$comm->addVariable("station_id", "Station 1");
		//$comm->addVariable("ticket_id", "1-1");
		//$comm->addVariable("isloyalty", "true");
		//$comm->addVariable("device_id", "Device 1");
		
		//$skulist = new MSKUList();
		//$skuitem = new MSKUItem();
		//$skulist->addMSKUItem($skuitem);
		//$skuitem->__set("Description", "Puddin");
		//$skuitem->__set("Price", "10.0");
		//$skuitem->__set("Qty", "2");
		//$skuitem->__set("SKU", "Puddin");
		//$comm->addVariable(null, $skulist, false);
		
		//$temp = $comm->getVariables();
	}

	//Close Transaction With SKU Information
	

	else if ($_REQUEST['mcmd'] == "redeemcoupon"){
		$comm = $mercuryFactory->createRedeemCoupon();
		$comm->addVariable("customeridentifier","5053630644");
		$comm->addVariable("code","8B70C5");
		//$comm->addVariable("cardnumber","0000");
		$comm->addVariable("deviceId","Device 1");
		$basket = new MSKUBasket();
		$comm->addVariable(null, $basket, false);
		//$item = new MSKUBasketItem();
		//$basket->addItemToBasket($item);
		//$item->__set("Quantity","1");
		//$item->__set("SKU","Makin Bacon");
		//$item->__set("UnitPrice","10.0");
	}
	
	else if($_REQUEST['mcmd'] == "getcoupon"){
		$comm = $mercuryFactory->createGetCoupon();
		$comm->addVariable("code","196B07");
	}
	
	if(isset($_REQUEST['data'])){
		$data = $_REQUEST['data'];
		/*
			{
				"varname" : "value",
				....
				
			}
		*/
	}
	
	
	//Redeems a Coupon
	
	if(!$comm){
		exit();
	}
	$response = $comm->process();
	$responseHandler = new MResponseHandler();
	$result = $responseHandler->parse($comm);
	
	if($result !== null){
		echo  "<pre>" . print_r($result, true) . "</pre>";
		echo "<pre>" . htmlspecialchars(print_r($comm->getConnectionInfo(), true)) . "</pre>";
	}
	else{
		echo htmlspecialchars($responseHandler->getError()) . "<br />";
		echo "<br /><pre>" . htmlspecialchars(print_r($comm->getConnectionInfo(), true)) . "</pre>";
	}
?>