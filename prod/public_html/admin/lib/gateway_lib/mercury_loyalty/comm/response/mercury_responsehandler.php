<?php
	require_once(dirname(__FILE__)."/tag_decoder.php");
	require_once(dirname(__FILE__)."/../communications/mercury_transaction.php");
	require_once(dirname(__FILE__)."/../datastructures/mercury_loyaltyprogram.php");
	require_once(dirname(__FILE__)."/../datastructures/mercury_loyaltyprogramstatus.php");
	require_once(dirname(__FILE__)."/../datastructures/mercury_redeemcouponresponse.php");
	date_default_timezone_set('America/Denver');
	/* <LoyaltyException xmlns="http://api.mercuryloyalty.com/v3_0/Data" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
		<Error>ApiKeyApiSecret Failed</Error>
		<ErrorCode>400</ErrorCode>
		<Reason>Token Not Found: LAVU : 23a6be5028a7450880d4128ce543</Reason>
		<SubErrorCode>1404</SubErrorCode>
	</LoyaltyException> */
	
	//<LoyaltyException xmlns="http://api.mercuryloyalty.com/v3_0/Data" xmlns:i="http://www.w3.org/2001/XMLSchema-instance"><Error>ApiKeyApiSecret Failed</Error><ErrorCode>400</ErrorCode><Reason>Token Not Found: LAVU : 23a6be5028a7450880d4128ce543</Reason><SubErrorCode>1404</SubErrorCode></LoyaltyException>
	global $nodeToClassMap;
	$nodeToClassMap = array(
		"LoyaltyException" => "MLoyaltyException",
		"LoyaltyProgramStatus" => "MLoyaltyProgramStatus",
		"LoyaltyProgram" => "MLoyaltyProgram",
		"RedeemCouponResponse" => "MRedeemCouponResponse",
		"LoyaltyCoupon" => "MLoyaltyCoupon",
		"Global" => "MCustomerInfo",
		"Local" => "MCustomerInfo",
		"Customer" => "MCustomer"
	);
	
	class MLoyaltyException extends MBaseResponseClass
	{
		public 
		 $Error,
		 $ErrorCode,
		 $Reason,
		 $SubErrorCode;
	
		public function __construct(DOMNode $root = null){
			$this->initialize($root);
		}
		
		public function __set($property, $value){
			$this->$property = $value;
		}
		
		private function printTagWithValue($tagName, $tagValue){
			return "<$tagName>$tagValue</$tagName>";
		}
		
		public function __toString(){
			$str = "";
			foreach($this as $property => $value){
				$str .= $this->printTagWithValue($property, $value);
			}
			
			return $this->printTagWithValue("LoyaltyException", $str);
		}
	}
	
	class MResponseHandler
	{
		protected $error;
		protected $result;
		protected $str;
		
		public function __construct($response = null){
			$this->result    = null;
			$this->error     = null;
			$this->str       = $response;
			if($response){
				$this->parse($response);
			}
		}
		
		/*
		[Options] => Array
		    (
		        [xmlns:a] => http://schemas.microsoft.com/2003/10/Serialization/Arrays
		        [children] => Array
		            (
		                [a:KeyValueOfstringstring] => Array
		                    (
		                        [children] => Array
		                            (
		                                [a:Key] => isprogramactive
		                                [a:Value] => 1
		                            )
		
		                    )
		
		            )
		
		    )
		*/
		private function resolveMicrosoftSerialization(&$arr){
			if(is_array($arr) && count($arr) > 0){
				foreach($arr as $key => $child){
					if(stristr($key, "xmlns") !== FALSE){
						$xmlns = explode(":", $key);
						if(count($xmlns) > 1){
							$xmlns = $xmlns[1];
						}
						else{
							$xmlns = "";
						}
						if($xmlns != ""){
							$xmlns .= ":";
						}
						if($child == "http://schemas.microsoft.com/2003/10/Serialization/Arrays"){
							$elements = $arr['children'];
							$newarr = array();  //resulting new array to replace serialization with
							if(is_array($elements)){
								foreach($elements as $entry => $subArr){
									if(stristr($entry, $xmlns . "KeyValue") !== FALSE){
										$newarr[$subArr['children'][$xmlns.'Key']] = $subArr['children'][$xmlns.'Value'];
									}
								}
							}
							$arr = $newarr;
						}
						else if($child == "http://schemas.datacontract.org/2004/07/System"){
							$elements = $arr['children'];
							$newVal = "";
							if(is_array($elements)){
								$newVal = date("Y-m-d h:i:s", strtotime($elements[$xmlns.'DateTime']) + mktime(0, $elements[$xmlns.'OffsetMinutes'], 0, 0, 0, 0) - mktime(0, 0, 0, 0, 0, 0));
								//$newVal = mktime(0, $elements[$xmlns.'OffsetMinutes'], 0, 0, 0, 0);
								//mktim
								//$newVal = mktime (0, $elements[$xmlns.'OffsetMinutes'] ,0, 0, 0, 0,  strtotime($elements[$xmlns.'DateTime']));
									//$newarr[$subArr['children'][$xmlns.'Key']] = $subArr['children'][$xmlns.'Value'];
									
									
							}
							$arr =  $newVal;
							break;
						}
						else if($child == "http://schemas.microsoft.com/2003/10/Serialization/"){
							$elements = $arr['children'];
							$newVal = "";
							$newVal = array_pop($elements);
							$arr = $newVal;
							break;
						}
					}
					else if(is_array($child)){
						$arr[$key] = $this->resolveMicrosoftSerialization($child);
					}
				}
			}
			return $arr;
		}
		
		public function getError(){
			return $this->error;
		}
		
		private function flatten($arr){
			if(is_array($arr)){
				foreach($arr as $key => $child){
					$arr[$key] = $this->flatten($child);
					if(stristr($key, "xmlns") !== FALSE){
						unset($arr[$key]);
					}
				}
				if(count($arr) == 0){
					$arr = null;
				}
				else if(count($arr) == 1){
					if(isset($arr['children'])){
						foreach($arr['children'] as $key => $value){
							$arr[$key] = $value;
						}
						
						unset($arr['children']);
					}
					else if(isset($arr['i:nil']) && $arr['i:nil'] == 'true'){
						$arr = null;
					}
					else if(array_key_exists("0", $arr)){
						$arr = $arr['0'];
					}
				}
			}
			
			return $arr;
		}
		
		public function parse($str = null){
			if(!$str){
				$str = $this->str;
			}
				
			if(!$str){
				$this->error = "Nothing to Parse.";
				return null;
			}
			$this->str = $str;
			
			if($str instanceof MercuryTransaction){
				if($str->hasError()){
					$this->error = $str->getError();
					$str = $str->getConnectionInfo();
					if(isset($str['response'])){
						$str = $str['response'];
					}
					else{
						return null;
					}
				}
				else{
					$str = $str->getResponse();
				}
			}
			
			//original
			$result = decode_tags($str);
			
			if(!$result){
				$this->error = "Unable to Decode Tags, Possibly Malformed Tag Language.";
				return null;
			}
			
			if($result->nodeName== "LoyaltyException"){
				//An Error Occurred With our Request, Oh Noes!
				$result = new MLoyaltyException($result);
				$this->error = $result;
				return null;
			}
			
			//$arr = $result->toArray();
			//$arr = $this->resolveMicrosoftSerialization($arr);
			//$arr = $this->flatten($arr);
			
			global $nodeToClassMap;
			if( isset($nodeToClassMap[$result->nodeName]) && class_exists($nodeToClassMap[$result->nodeName]) ){
				$className = $nodeToClassMap[$result->nodeName];
				return new $className ($result);
			}
			else{
				return $result->nodeValue;
			}
			
			return null;
			
		}
		
		
	}
?>