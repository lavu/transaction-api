<?php
	function speekc(&$str){
		return substr($str, 0, 1);
	}
	
	function sgetc(&$str){
		if(!is_string($str)){
			return null;
		}
		$c = speekc($str);
		$str = substr($str, 1);
		return $c;
	}
	
	function sputc(&$str, $c)
	{
		$str = $c . $str;
	}
	
	function read_until_close(&$stream, $close)
	{
		$result = "";
		while(($c = sgetc($stream)) != $close && $c != ""){
			if($c=="\\")
				$c = sgetc($stream);
			$result .= $c;
			if(substr($result, strlen($result) - strlen($close)) == $close){
				$result = substr($result, 0, strlen($result) - strlen($close));
				break;
			}
		}
		
		return $result;
	}
	
	function ensureArraySet(&$arr, $val, $key=null)
	{
		if(!$arr)
			$arr = array();
		if($key)
		{
			$arr[$key]=$val;
		}
		else
			$arr[] = $val;
	}
	
	class Node
	{
		public $name;
		public $properties;
		public $children;
		
		public function __toString(){
			return $this->toString(0);
		}
		
		public function toString($level = 0){
		
		$str = "";
		$tabs = "";
		
			for($i = 0; $i < $level; $i++)
				$tabs .= "  ";
			$str .= $tabs;
			$str .=  "<" . $this->name;
			if($this->properties || $this->children){
				if($this->properties)
					foreach($this->properties as $key => $val){
						if($val != ""){
							$str .=  " " . $key . "=\"" . $val . "\"";
						}
						else{
							$str .=  " " . $key;
						}
					}
				$str .= ">";
				$addtabs = true;
				if($this->children)
					foreach($this->children as $k => $child){
						if($child instanceof Node){
							$inc = $level;
							//echo ord(substr($this->name, 2, 1)) . "\n";
							if(substr($this->name, 0, 3) != chr(237).chr(189).chr(191) && substr($this->name, 0, 1) != "!")
								$inc += 1;
							$str .= "\n" . $child->toString($inc);
						}
						else{
							$str .= $child;
							$addtabs = false;
						}
					}
				if($addtabs)
					$str .= "\n" . $tabs;
				$str.="</" . $this->name . ">";
			}
			else{
				$str .= "/>";
			}
			
			return $str;
		}
		
		public function toArray(&$parent=array()){
			$result = array();
			if($this->properties){
				foreach($this->properties as $key => $val){
					$result[$key] = $val;
				}
			}
			
			if($this->children){
				$result['children'] = array();
				foreach($this->children as $key => $child){
					if($child instanceof Node){
						$child->toArray($result['children']);
					}
					else{
						$result['children'][] = $child;
					}
				}
			}
			
			if(!$this->properties && $this->children){
				if(count($result['children']) == 1){
					$result = array_pop($result['children']);
				}
			}
			
			if(is_array($result) && count($result) == 0){
				$result = null;
			}
			$cnt = "";
			while(isset($parent[$this->name . $cnt])){
				$cnt++;
			}
			$parent[$this->name . $cnt] = $result;
			
			return $parent;
		}
	}
	
	function decode_tags(&$stream){
	
		$dom = DOMDocument::loadXML($stream);
		$document_root = $dom->childNodes->item(0);
		return $document_root;
	
		if($stream == "")
			return;
		$result = new Node();
		$result->name = "";
		
		$build = "";
		$prop = null;
		$object_declared = false;
		while(($c = sgetc($stream)) != ""){
			if($c == "<"){ //Open Detected
				if(speekc($stream) == "/"){ //Close Detected
					sgetc($stream);
					$name = read_until_close($stream, ">");
					
					assert($name == $result->name);
					if(trim($build) != "")
						ensureArraySet($result->children, $build);
					$build = "";
					//echo $name . "\n\n";
					break; //END OF OBJECT
				}
				//else if(speekc($stream) == "!"){ //Header or Comment, Ignore
				//	$commentorheader = read_until_close($stream, ">");
				//}
				else{ //Open Detected
					if(!$object_declared){
						//Next thing would be the name... but it's not guaranteed to be followed by a character
					}
					else{
						if(trim($build) != "")
							ensureArraySet($result->children, $build);
						$build = "";
						sputc($stream, $c);
						ensureArraySet($result->children, decode_tags($stream));
					}
				}
			}
			else if($c == ">"){ //Closed Tag, but not the end of this object
				if($result->name == ""){
					$result->name = $build;
				}
				$build = "";
				$object_declared = true;
			}
			else if($c == "\""){//Found a Quote, Ignore Parsing Until End Quote Found
				$str = read_until_close($stream, "\"");
				if($result->name == ""){
					$result = $str;
					break;
				}
				if($prop){
					ensureArraySet($result->properties,$str,$prop);
					$prop = null;
				}
				else{
					ensureArraySet($result->properties, null, "\"" . $str . "\"");
				}
			}
			else if(($c == " " || $c == "\n" || $c == "\t") && !$object_declared){//Eat Tabs, Newlines, and spaces (if we haven't found an object)
				if($result->name == ""){
					$result->name = $build;
					$build = "";				
				}
				else if($prop){
					ensureArraySet($result->properties,$build,$prop);
					$prop = null;
				}
				else{
					if($build != "")
						ensureArraySet($result->properties,null,$build);
				}
				$build = "";
				continue; //Skip Spaces, new lines, and tabs
			}
			else if(!$object_declared && $c == "/"){ //Close Detected
				if($result->name == "")
					$result->name = $build;
				
				$build = "";
				$c = sgetc($stream);
				if(!assert($c == ">")){
					echo "Assertion Failed, Expected > Recieved: $c<br />$result->name";
				}
				break;
			}
			else if($c == "="){//Defined a property
				$prop = $build;
				ensureArraySet($result->properties,null,$prop);
				$build = "";
			}
			else{ //Characters
				$build .= $c;  //Have a non-special character, it must be the name of something, add them all together (likely to be in sequence)
			}
		}
		
		return $result;
	}
?>