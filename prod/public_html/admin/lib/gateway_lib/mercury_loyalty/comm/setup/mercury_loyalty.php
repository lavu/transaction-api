<?php 
	require_once(dirname(__FILE__)."/../communications/mercury_GetProgramDescription.php");
	require_once(dirname(__FILE__)."/../communications/mercury_AddCredits.php");
	require_once(dirname(__FILE__)."/../communications/mercury_CloseTransactionWithSKU.php");
	require_once(dirname(__FILE__)."/../communications/mercury_GetStatus.php");
	require_once(dirname(__FILE__)."/../communications/mercury_RedeemCoupon.php");
	require_once(dirname(__FILE__)."/../communications/mercury_GetCoupon.php");
	
	require_once(dirname(__FILE__)."/../setup/mercury_config.php");
	require_once(dirname(__FILE__)."/../datastructures/mercury_skubasket.php");
	require_once(dirname(__FILE__)."/../datastructures/mercury_skulist.php");
	/***
	 * BASE FILE
	 *
	 * This file will contain a class which will contain static factories for each of the mercury
	 * loyalty program function calls.  
	 *
	 *
	 **/
	 class MLoyalty
	 {
		 private $apikey;
		 private $apisecret;
		 
		 public function __construct(){
			 $this->apikey = null;
			 $this->apisecret = null;
			 global $CRED_Mercury_Key;
			 global $CRED_Mercury_Secret;
			 
			 if(isset($CRED_Mercury_Key)){
			 	$this->apikey = $CRED_Mercury_Key;
			 }
			 if(isset($CRED_Mercury_Secret)){
			 	$this->apisecret = $CRED_Mercury_Secret;
			 }
		 }
		 
		 public function setAPIKey($apikey){
			 $this->apikey = $apikey;
		 }
		 
		 public function setAPISecret($apisecret){
			 $this->apisecret = $apisecret;
		 }
		 
		 public function createGetProgramDescription(){
		 	if(!$this->apikey || !$this->apisecret){
			 	return null;
		 	}
			 $communication = new MGetProgramDescription();
			 $communication->addVariable('apikey', $this->apikey);
			 $communication->addVariable('apisecret', $this->apisecret);
			 
			 return $communication;
		 }
		 
		 public function createAddCredits(){
		 	if(!$this->apikey || !$this->apisecret){
			 	return null;
		 	}
			 $communication = new MAddCredits();
			 $communication->addVariable('apikey', $this->apikey);
			 $communication->addVariable('apisecret', $this->apisecret);
			 
			 return $communication;
		 }

		 public function createCloseTransactionWithSKU(){
		 	if(!$this->apikey || !$this->apisecret){
			 	return null;
		 	}
			 $communication = new MCloseTransactionWithSKU();
			 $communication->addVariable('apikey', $this->apikey);
			 $communication->addVariable('apisecret', $this->apisecret);
			 
			 return $communication;
		 }

		 public function createGetStatus(){
		 	if(!$this->apikey || !$this->apisecret){
			 	return null;
		 	}
			 $communication = new MGetStatus();
			 $communication->addVariable('apikey', $this->apikey);
			 $communication->addVariable('apisecret', $this->apisecret);
			 
			 return $communication;
		 }
		 
		 public function createRedeemCoupon(){
			 if(!$this->apikey || !$this->apisecret){
			 	return null;
		 	}
			 $communication = new MRedeemCoupon();
			 $communication->addVariable('apikey', $this->apikey);
			 $communication->addVariable('apisecret', $this->apisecret);
			 
			 return $communication;
		 }
		 
		 public function createGetCoupon(){
			 if(!$this->apikey || !$this->apisecret){
			 	return null;
		 	}
			 $communication = new MGetCoupon();
			 $communication->addVariable('apikey', $this->apikey);
			 $communication->addVariable('apisecret', $this->apisecret);
			 
			 return $communication;
		 }
	 }
?>