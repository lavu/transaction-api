<?php
	$test_environment = 1;
	
	$URL_Mecury_Base;
	
	$CRED_Mercury_Secret;
	$CRED_Mercury_Key;
	
	global $URL_Mercury_getProgramDescription;
	global $URL_Mercury_addCredits;
	global $URL_Mercury_closeTransactionWithSKU;
	global $URL_Mercury_getStatus;
	global $URL_Mercury_RedeemCoupon;
	global $URL_Mercury_GetCoupon;

	$URL_Mercury_getProgramDescription = "GetProgramDescription/";
	$URL_Mercury_addCredits = "AddCredits/";
	$URL_Mercury_closeTransactionWithSKU = "CloseTransactionWithSKU/";
	$URL_Mercury_getStatus = "GetStatus/";
	$URL_Mercury_RedeemCoupon = "RedeemCoupon/";
	$URL_Mercury_GetCoupon = "GetCoupon/";
	
	if($test_environment){
		$URL_Mecury_Base = "https://api.cert.mercuryloyalty.com/v3_0/Loyalty/";
		$CRED_Mercury_Key = "LAVU";
		$CRED_Mercury_Secret = "23a6be5028a7450880d4128ce543c2e6";
	}
	else{
		$URL_Mecury_Base = "";
		$CRED_Mercury_Key = "";
		$CRED_Mercury_Secret = "";
	}
	
	$URL_Mercury_getProgramDescription = $URL_Mecury_Base . $URL_Mercury_getProgramDescription;
	$URL_Mercury_addCredits = $URL_Mecury_Base . $URL_Mercury_addCredits;
	$URL_Mercury_closeTransactionWithSKU = $URL_Mecury_Base . $URL_Mercury_closeTransactionWithSKU;
	$URL_Mercury_getStatus = $URL_Mecury_Base . $URL_Mercury_getStatus;
	$URL_Mercury_RedeemCoupon = $URL_Mecury_Base . $URL_Mercury_RedeemCoupon;
	$URL_Mercury_GetCoupon = $URL_Mecury_Base . $URL_Mercury_GetCoupon;
	
?>