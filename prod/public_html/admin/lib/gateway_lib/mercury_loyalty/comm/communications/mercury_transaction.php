<?php
	require_once(dirname(__FILE__)."/../../../curltransaction.php");

/***
 *	Defines Neccessarily Validation Requirements for all Mercury Based CurlTransactions
 **/
class MercuryTransaction extends CurlTransaction
{	
	public function __construct(){
		parent::__construct();
	}
	
	
	/**
	 * All/Most Mecury Transactions Require These Fields
	 */
	public function validate(){
		if(!isset($this->variables['GET']['apikey'])){
			$this->error = "API Key was Not Specified.";
			return false;
		}
			
		if(!isset($this->variables['GET']['apisecret'])){
			$this->error = "API Secret was Not Specified.";
			return false;
		}
			
		return true;
	}

}
?>