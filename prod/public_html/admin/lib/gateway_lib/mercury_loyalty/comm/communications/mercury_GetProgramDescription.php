<?php
	require_once(dirname(__FILE__)."/../communications/mercury_transaction.php");
	require_once(dirname(__FILE__)."/../setup/mercury_config.php");
	
	class MGetProgramDescription extends MercuryTransaction
	{
		public function __construct(){
			parent::__construct();
			global $URL_Mercury_getProgramDescription;
			$this->setURL($URL_Mercury_getProgramDescription);
		}
		
		/**
		 * Unneccessary, but It Overtly specifies that it doesn't require further validation
		 */
		public function validate(){  
			return parent::validate();
		}
	}
?>