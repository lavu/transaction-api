<?php
	require_once(dirname(__FILE__)."/../communications/mercury_transaction.php");
	require_once(dirname(__FILE__)."/../setup/mercury_config.php");
	
	class MGetStatus extends MercuryTransaction
	{
		public function __construct(){
			parent::__construct();
			global $URL_Mercury_getStatus;
			$this->setURL($URL_Mercury_getStatus);
		}
		
		/**
		 * Unneccessary, but It Overtly specifies that it doesn't require further validation
		 */
		public function validate(){  
			if(!parent::validate()){
				return false;
			}
				
			$get = $this->variables['GET'];
			
			if(!isset($get['customeridentifier']) && !isset($get['cardnumber']) && !isset($get['customerid'])){
				$this->error = "Form of Customer Identification Was Not Supplied.\nSupply one of the following: 'customeridentifier', 'cardnumber', or 'customerid'.";
				return false;
			}
			
			return true;
		}
	}
?>