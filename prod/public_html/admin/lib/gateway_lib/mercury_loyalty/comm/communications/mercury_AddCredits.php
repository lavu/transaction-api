<?php
	require_once(dirname(__FILE__)."/mercury_transaction.php");
	require_once(dirname(__FILE__)."/../setup/mercury_config.php");
	
	class MAddCredits extends MercuryTransaction
	{
		public function __construct(){
			parent::__construct();
			global $URL_Mercury_addCredits;
			$this->setURL($URL_Mercury_addCredits);
		}
		
		/**
		 * Specifies that this function requires the specified variables:
		 *	CardNumber - Optional
		 *	Client - Arbitrary string identifying your integration.  eg. "Vanilla POS 2012"
		 *	Delay - Optional.  Sets the time that the transaction occurred.  Useful if internet 
		 *    outages prevented a transaction from calling AddCredits.  If this field is set, we 
		 *    know that this is a delayed transaction, and will attempt to schedule messaging during
		 *    normal business hours.
		 *	Description - Required.  Describes the transaction.  In-store Visit, In-store Spend.  Arbitrary.
		 *	EmployeeID - Required.  Employee's name or ID.
		 *	Station ID - Required.  Station ID closing the transaction.
		 *	Language - Optional.  Language Preference.  en, es, fr.
		 *	MobileOrEmail - Required unless a valid CardNumber is specified.
		 *	TestMode - for development only.  Allows the same customer to call AddCredits with the same
		 *    parameters multiple times.  Normally, this behavior is trapped as a safety measure to prevent
		 *    duplicate customer messaging.
		 *	TicketID - Required. A unique ID for the ticket.  Recommended format for IDs that recycle:
		 *	YYYY-MM-DD-SID-CHECKID (SID = StationID, CHECKID = POS CHECKID)
		 *	TicketRevenue - Required. The Ticket Total, Post-Tax Recommended.
		 *	Units - Required. The loyalty credits given.  For Spend campaigns (See GetProgramDescription), 
		 *    this is equal to Revenue.  For Visit campaigns, this is generally equal to GuestCount.
		 *	Version - Version of this Integration.
		 */
		public function validate(){  
			if(!parent::validate())
				return false;
				
			$required_list = array(
				"client",
				"description",
				"employee_id",
				"station_id",
				"ticket_id",
				"revenue",
				"units"
			);
			
			foreach($required_list as $key => $val){
				if(!isset($this->variables['GET'][$val])){
					$this->error = "Validation Failed, Missing GET Variable: $val";
					return false;
				}
			}
			
			if(!isset($this->variables['GET']['cardnumber']) && !isset($this->variables['GET']['customeridentifier'])){
				$this->error = "Validation Failed Missing Customer Identification or CardNumber";
				return false;
			}
				
			return true;
		}
	}
?>