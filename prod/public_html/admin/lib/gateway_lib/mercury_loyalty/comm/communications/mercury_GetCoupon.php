<?php
	require_once(dirname(__FILE__)."/../communications/mercury_transaction.php");
	require_once(dirname(__FILE__)."/../setup/mercury_config.php");
	
	class MGetCoupon extends MercuryTransaction
	{
		public function __construct(){
			parent::__construct();
			global $URL_Mercury_GetCoupon;
			$this->setURL($URL_Mercury_GetCoupon);
			//$this->setPost(true);
		}
		
		/**
		 * 
		 */
		public function validate(){  
			if(!parent::validate())
				return false;
				
			$required_list = array(
				"code"
			);
			
			foreach($required_list as $key => $var){
				if(!isset($this->variables['GET'][$var])){
					$this->error = "No information supplied for entry: $var.";
					return false;
				}
			}
			
			return true;
		}
	}
?>