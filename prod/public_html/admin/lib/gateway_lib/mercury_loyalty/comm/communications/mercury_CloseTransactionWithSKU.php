<?php
	require_once(dirname(__FILE__)."/../communications/mercury_transaction.php");
	require_once(dirname(__FILE__)."/../setup/mercury_config.php");
	require_once(dirname(__FILE__)."/../datastructures/mercury_skulist.php");
	
	class MCloseTransactionWithSKU extends MercuryTransaction
	{
		public function __construct(){
			parent::__construct();
			global $URL_Mercury_closeTransactionWithSKU;
			$this->setURL($URL_Mercury_closeTransactionWithSKU);
			$this->setPost(true);
		}
		
		/**
		 * 
		 */
		public function validate(){  
			if(!parent::validate())
				return false;
				
			$required_list = array(
				"revenue",
				"guestcount",
				"employee_id",
				"station_id",
				"ticket_id",
				"isloyalty"
				//"device_id"
			);
			
			foreach($required_list as $key => $var){
				if(!isset($this->variables['GET'][$var])){
					$this->error = "No Information Specified for the Variable: $var.";
					return false;
				}
			}
			
			if(!isset($this->variables['POST']) || !($this->variables['POST'] instanceof MSKUList) || !($this->variables['POST']->validate()) ){
				$this->error = "POST Not Specified or POST isn't an MSKUList or MSKUList isn't Valid.";
				return false;
			}
			
			return true;
		}
	}
?>