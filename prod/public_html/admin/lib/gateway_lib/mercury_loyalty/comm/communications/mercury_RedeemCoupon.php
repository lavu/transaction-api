<?php
	require_once(dirname(__FILE__)."/../communications/mercury_transaction.php");
	require_once(dirname(__FILE__)."/../setup/mercury_config.php");
	require_once(dirname(__FILE__)."/../datastructures/mercury_skubasket.php");
	
	class MRedeemCoupon extends MercuryTransaction
	{
		public function __construct(){
			parent::__construct();
			global $URL_Mercury_RedeemCoupon;
			$this->setURL($URL_Mercury_RedeemCoupon);
			$this->setPost(true);
		}
		
		/**
		 * 
		 */
		public function validate(){  
			if(!parent::validate())
				return false;
				
			$required_list = array(
				"code",
				"customeridentifier",
				//"cardnumber",
				"deviceId"
			);
			
			foreach($required_list as $key => $var){
				if(!isset($this->variables['GET'][$var])){
					$this->error = "No information supplied for entry: $var.";
					return false;
				}
			}
			
			if(!$this->variables['POST'] || !($this->variables['POST'] instanceof MSKUBasket) || !($this->variables['POST']->validate())){
				$this->error = "POST Variables Not Specified, or Is not an MSKUBasket, or the MSKUBasket is Missing Details.";
				return false;
			}
			
			return true;
		}
	}
?>