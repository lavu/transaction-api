<?php
/*
[Customer] => Array
(
    [Address] => <address>
    [Anniversary] => <anniversay date>
    [Birthday] => <birthday>
    [City] => <city>
    [EmailAddress] => <email>
    [FaxNumber] => <fax number>
    [FirstName] => <first name>
    [FullyRegistered] => false
    [ID] => <id>
    [LastName] => <last name>
    [MobileNumber] => <mobile number>
    [Options] => 
    [State] => <state abbreviation>
    [Zipcode] => <zipcode>
)
*/
//require_once("mercury_redeemcouponresponse.php");
require_once(dirname(__FILE__)."/mercury_baseresponseclass.php");

class MCustomer extends MBaseResponseClass
{
	public
	 $Address,
	 $Anniversary,
	 $Birthday,
	 $City,
	 $EmailAddress,
	 $FaxNumber,
	 $FirstName,
	 $FullyRegistered,
	 $ID,
	 $LastName,
	 $MobileNumber,
	 $Options,
	 $State,
	 $Zipcode;
	
	public function __construct(DOMNode $root = null){
		$this->initialize($root);
	}
}
?>