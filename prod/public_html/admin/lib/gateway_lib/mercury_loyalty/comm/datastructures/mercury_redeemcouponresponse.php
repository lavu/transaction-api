<?php
//Array
//(
//    [RedeemCouponResponse] => Array
//        (
//            [CouponType] => EntireTicket //Apply Type
//            [Customer] => MCustomer
//                (
//                )
//            [DiscountAmount] => 0
//            [DiscountSKU] => 
//            [DiscountType] => Unknown
//            [Status] => Invalid
//            [Template] => 
//        )
//)
require_once(dirname(__FILE__)."/mercury_baseresponseclass.php");
require_once(dirname(__FILE__)."/mercury_customer.php");

class MRedeemCouponResponse extends MBaseResponseClass
{
	protected $CouponType;
	protected $Customer;
	protected $DiscountAmount;
	protected $DiscountSKU;
	protected $DiscountType;
	protected $Status;
	protected $Template;
	
	public function __construct(DOMNode $root = null){
		$this->initialize($root);
	}
}


?>