<?php
	class MSKUItem{
		private $Description;
		private $ID;
		private $KeywordID;
		private $Price;
		private $Qty;
		private $SKU;
		private $TransactionID;
		
		public function __construct(){
			$this->description = null;
			$this->id = null;
			$this->keywordID = null;
			$this->price = null;
			$this->qty = null;
			$this->sku = null;
			$this->transactionID = null;
		}
		
		private function writeTagValue($tagName, $value){
			if($value)
				return "<$tagName>$value</$tagName>";
			return "";
		}
		
		/**
		 *	REQUIRED FIELDS
		 **/
		public function validate(){
			if(!$this->Description)
				return false;
			if(!$this->Price)
				return false;
			if(!$this->Qty)
				return false;
			if(!$this->SKU)
				return false;
			return true;
		}
		
		public function __set($varName, $varValue){
			$this->$varName = $varValue;
		}
		
		public function __get($varName){
			if(isset($this->$varName)){
				return $this->$varName;
			}
			return null;
		}
		
		public function __toString(){
			$str = "";
/*
			$str .= $this->writeTagValue("Description", $this->description);
			$str .= $this->writeTagValue("ID", $this->id);
			$str .= $this->writeTagValue("KeywordID", $this->keywordID);
			$str .= $this->writeTagValue("Price", $this->description);
			$str .= $this->writeTagValue("Qty", $this->qty);
			$str .= $this->writeTagValue("SKU", $this->sku);
			$str .= $this->writeTagValue("TransactionID", $this->transactionID);
*/
			
			foreach($this as $property => $value){
				$str .= $this->writeTagValue($property, $value);
			}
			
			return $this->writeTagValue("SKUItem", $str);

		}
		
	}

	/**
	 *	Holds a List of MSKUItems, as well as providing some nice access functions
	 **/
	class MSKUList{
		
		private $skulist;
		
		public function __construct(){
			$this->skulist = array();
		}
		
		public function addMSKUItem(MSKUItem $item){
			if(!$item)
				return false;
			$this->skulist[] = $item;
			return true;
		}
		
		private function writeTagValue($tagName, $value){
			if($value)
				return "<$tagName xmlns=\"http://api.mercuryloyalty.com/v3_0/Data\">$value</$tagName>";
			return "";
		}
		
		public function validate(){
			foreach($this->skulist as $key => $skuitem){
				if(!$skuitem->validate()){
					return false;
				}
			}
			
			return true;
		}

		
		public function __toString(){
			$str = "";
			foreach($this->skulist as $key => $skuitem){
				$str .= $skuitem;
			}
		
			return $this->writeTagValue("ArrayOfSKUItem", $str);
		}
	}
?>