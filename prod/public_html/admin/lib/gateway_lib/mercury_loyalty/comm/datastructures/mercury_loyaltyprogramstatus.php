<?php
/*
[LoyaltyProgramStatus] => Array
    (
        [Customer] => MCustomer
        [Global] => MCustomerInfo
        [Local] => MCustomerInfo
        [OpenCoupons] => MOpenCoupons	
    )
*/
require_once(dirname(__FILE__)."/mercury_customer.php");
require_once(dirname(__FILE__)."/mercury_customerinfo.php");
require_once(dirname(__FILE__)."/mercury_loyaltycoupon.php");
require_once(dirname(__FILE__)."/mercury_baseresponseclass.php");

class MLoyaltyProgramStatus extends MBaseResponseClass
{
	public
	 $Customer,
	 $Global,
	 $Local,
	 $OpenCoupons;
	
	public function __construct(DOMNode $root = null){
		$this->initialize($root);
	}
}

/*
<CardNumber>0000</CardNumber>
<CurrentCredits>2.00</CurrentCredits>
<LastVisit xmlns:a="http://schemas.datacontract.org/2004/07/System">
	<a:DateTime>2013-02-07T16:48:17Z</a:DateTime>
	<a:OffsetMinutes>0</a:OffsetMinutes>
</LastVisit>
<LifetimeCredits>12.00</LifetimeCredits>
<LifetimeRevenue>29.98</LifetimeRevenue>
<Rating>5</Rating>
<RatingName>Platinum</RatingName>
<RewardsEarned>3</RewardsEarned>
*/
?>