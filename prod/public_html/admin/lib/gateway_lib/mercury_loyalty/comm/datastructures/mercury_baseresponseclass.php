<?php
class MBaseResponseClass
{
	public function __construct(){

	}
	
	public function __get($property){
		if(isset($this->$property)){
			return $this->$property;
		}
		return null;
	}

	public function __set($property, $value){
		$this->$property = $value;
	}
	
	public function initialize(DOMNode $root){
		if($root == null)
			return;
		$children = $root->childNodes;
		for($i = 0; $i < $children->length; $i++){
			$child =  $children->item($i);
			$name = $child->nodeName;
			global $nodeToClassMap;
			if(isset($nodeToClassMap[$name]) && class_exists($nodeToClassMap[$name])){
				$className = $nodeToClassMap[$name];
				$this->$name = new $className ($child);
			}
			else if($name == "OpenCoupons"){
				$arr = array();
				$children2 = $child->childNodes;
				for($j = 0; $j < $children2->length; $j++){
					$child2 = $children2->item($j);
					if(isset($nodeToClassMap[$child2->nodeName]) && class_exists($nodeToClassMap[$child2->nodeName])){
						$className = $nodeToClassMap[$child2->nodeName];
						$arr[] = new $className($child2);
					}
				}
				$this->$name = $arr;
			}
			else{
				$this->set($name, $child);
			}
		}
	}
	
	public function set($property, $value){
		$children = $value->childNodes;
		global $nodeToClassMap;
		for($i = 0; $i < $children->length; $i++){
			$child = $children->item($i);
			if($child->nodeName == "#text"){
				$this->$property = $child->nodeValue;
			}
			else if($child->namespaceURI != $child->baseURI){
				if(isset($this->$property) && is_array($this->$property)){
					$temp = MBaseResponseClass::resolveSerialization($child);
					if(is_array($temp)){
						array_merge($this->$property, $temp);
					}
					else{
						array_push($this->$property, $temp);
					}
				}
				else{
					$this->$property = MBaseResponseClass::resolveSerialization($child);
				}
			}
			else if( isset($nodeToClassMap[$child->nodeName]) && class_exists($nodeToClassMap[$child->nodeName])){
				$className = $nodeToClassMap[$child->nodeName];
				$this->$property = new $className($child);
			}
			else{
				error_log("ML: Found Unexpected Result: $child->nodeName");
			}
		}
	}
	
	private static function resolveMicrosoftArraySerialization($value){
		$types = array(
			'boolean',
			'bytes',
			'base64Binary',
			'boolean',
			'byte',
			'dateTime',
			'decimal',
			'double',
			'float',
			'int',
			'long',
			'QName',
			'short',
			'string',
			'unsignedByte',
			'unsignedInt',
			'unsignedLong',
			'unsignedShort',
			'char'
		);
		
		$serializedTypes = array(
			'KeyValue',
			'Array'
		);
		
		$name = $value->localName;
		if(stripos($name, $serializedTypes[0] . 'Of') !== FALSE){
			//Associative Array (Dictionary)
			$name = substr($name, strlen($serializedTypes[0] . 'Of'));
			$key_type = 'string';
			$value_type = 'string';
			foreach($types as $type){
				if(substr($name, 0, strlen($type)) == $type){
					$key_type = $type;
					$name = substr($name, strlen($type));
					break;
				}
			}
			
			foreach($types as $type){
				if(substr($name, 0, strlen($type)) == $type){
					$value_type = $type;
					$name = substr($name, strlen($type));
					break;
				}
			}
			
			$result = array();
			$children = $value->childNodes;
			$key = "";
			$value = "";
			for($i = 0; $i < $children->length; $i++){
				$child = $children->item($i);
				if($child->localName == "Key"){
					$key = $child->nodeValue;
				}
				else if($child->localName == "Value"){
					$value = $child->nodeValue;
					if(isset($key)){
						$result[$key] = $value;
					}
					unset($key);
					unset($value);
				}
			}
			
			return $result;
			
		}
		else if(stripos($name, $serializedTypes[1] . 'Of') !== FALSE){
			//Standard Array
			$name = substr($name, strlen($serializedTypes[1] . 'Of'));
			$value_type = 'string';
			foreach($types as $type){
				if(substr($name, 0, strlen($type)) == $type){
					$value_type = $type;
					$name = substr($name, strlen($type));
					break;
				}
			}
			$result = array();
			$children = $value->childNodes;
			for($i = 0; $i < $children->length; $i++){
				$child = $children->item($i);
				if($child->localName == $value_type){
					$result[] = $child->nodeValue;	
				}
			}
			
			return $result;
		}
		else if(in_array($value->localName, $types)){
			return $value->nodeValue;
		}
		else{
			error_log("ML: Unable to Resolve Microsoft Array Serialization: $value->localName.");
			error_log($value->ownerDocument->saveXML());
			return "";
		}
	}
	
	private static function resolveDataContractSystemSerialization($value){
		//First child is a UTC formatted date.  The Second Child is the Minutes Offset
		$children = $value->childNodes;
		$datetime;
		$offsetminutes;
		for($i = 0; $i < $children->length; $i++){
			$child = $children->item($i);
			if($child->localName == "DateTime"){
				$datetime = $child->nodeValue;
			}
			else if($child->localName == "OffsetMinutes"){
				$offsetminutes = $child->nodeValue;
			}
			else{
				error_log("ML: Unexpected token in DataContract Serialization: $child->localName");
			}
		}
		return $datetime;
	}
	
	public static function resolveSerialization($value){
		$uri = $value->namespaceURI;
		if($uri == "http://schemas.microsoft.com/2003/10/Serialization/Arrays"){
			return MBaseResponseClass::resolveMicrosoftArraySerialization($value);
		}
		else if($uri == "http://schemas.datacontract.org/2004/07/System"){
			return MBaseResponseClass::resolveDataContractSystemSerialization($value->parentNode);
		}
	}
}
?>