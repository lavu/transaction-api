<?php
	
	class MSKUBasketItem
	{
		private $Quantity;
		private $SKU;
		private $UnitPrice;
		
		public function __construct(){
			$this->quantity = null;
			$this->sku = null;
			$this->unitPrice = null;
		}
		
		
		public function validate(){
			if(!$Quantity)
				return false;
			if(!$SKU)
				return false;
			if(!$UnitPrice)
				return false;
			return true;
		}
		
		private function printTagWithValue($tagName, $value){
			if($value)
				return "<$tagName>$value</$tagName>";
			return "";
		}
		
		public function __set($varName, $varValue){
			$this->$varName = $varValue;
		}
		
		public function __get($varName){
			return $this->$varName;
		}
		
		public function __toString(){
			$str = "";
			foreach($this as $property => $value){
				$str .= $this->printTagWithValue($property, $value);
			}
			
			return $this->printTagWithValue("SKUBasketItem", $str);
		}
	}
	
	
	class MSKUBasket
	{
		private $basket;
		
		public function __construct(){
			$this->basket = array();
		}
		
		private function printTagWithValue($tagName, $value){
			if($value)
				return "<$tagName xmlns=\"http://api.mercuryloyalty.com/v3_0/Data\">$value</$tagName>";
			return "";
		}
		
		public function addItemToBasket(MSKUBasketItem $item){
			$basket[] = $item;
		}
		
		public function __set($varName, $varValue){
			$this->$varName = $varValue;
		}
		
		public function __get($varName){
			return $this->$varName;
		}
		
		public function validate(){
			foreach($this->basket as $key => $item){
				if(!$item->validate()){
					return false;
				}
			}
			
			return true;
		}
		
		public function __toString(){
			$str = "";
			foreach($this->basket as $key => $val){
				$str .= $val;
			}
			return $this->printTagWithValue("ArrayOfSKUBasketItem", $str);
		}
	}
?>