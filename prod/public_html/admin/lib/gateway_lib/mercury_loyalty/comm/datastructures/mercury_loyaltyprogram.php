<?php
/*
[LoyaltyProgram] => Array
    (
        [Action] => Earn //Campaign Type
        [BonusForEmail] => 0
        [BonusForReg] => 0
        [BonusForSocialMedia] => 0
        [BonusForSurvey] => 0
        [BusinessEmail] => <email>
        [BusinessName] => <business name>
        [BusinessURL] => <business url>
        [Goal] => 5
        [Incentive] => Free Yogurt
        [KeywordID] => 20
        [KeywordName] => <keyword name>
        [KioskAreaCode] => false
        [LogoURL] => 
        [Options] => Array
            (
                [isprogramactive] => 1
            )

        [RegAreaCode] => <area code>
        [RegImg1024x768] => 
        [RegImg1280x1024] => 
        [RegImg800x600] => 
        [Shortcode] => <short code>
        [Units] => Points //Loyalty Unit Type
    )
*/
require_once(dirname(__FILE__)."/mercury_baseresponseclass.php");

class MLoyaltyProgram extends MBaseResponseClass
{
	public
	 $Action,
	 $BonusForEmail,
	 $BonusForReg,
	 $BonusForSocialMedia,
	 $BonusForSurvey,
	 $BusinessEmail,
	 $BusinessName,
	 $BusinessURL,
	 $Goal,
	 $Incentive,
	 $KeywordID,
	 $KeywordName,
	 $KioskAreaCode,
	 $LogoURL,
	 $Options,
	 $RegAreaCode,
	 $RegImg1024x768,
	 $RegImg1280x1024,
	 $RegImg800x600,
	 $Shortcode,
	 $Units;
	
	public function __construct(DOMNode $root = null){
		$this->initialize($root);
	}
}
?>