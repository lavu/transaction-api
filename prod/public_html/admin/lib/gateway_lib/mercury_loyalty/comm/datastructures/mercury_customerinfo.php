<?php
/*
[Global] => Array
    (
        [CurrentCredits] => 0.00
        [LastVisit] => 2013-02-01 01:28:36
        [LifetimeCredits] => 10.00
        [LifetimeRevenue] => 27.98
        [Rating] => 5
        [RatingName] => Platinum
        [RewardsEarned] => 2
    )
*/
require_once(dirname(__FILE__)."/mercury_baseresponseclass.php");
class MCustomerInfo extends MBaseResponseClass
{
	public
	 $CurrentCredits,
	 $LastVisit,
	 $LifetimeCredits,
	 $LifetimeRevenue,
	 $Rating,
	 $RatingName,
	 $RewardsEarned;
	
	public function __construct(DOMNode $root = null){
		$this->initialize($root);
	}
}
?>