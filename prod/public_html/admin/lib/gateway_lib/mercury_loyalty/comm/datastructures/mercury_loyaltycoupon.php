<?php
/*
[OpenCoupons] => Array
    (
        [Code] => ADE5CE
        [CouponClass] => Individual
        [CouponType] => EntireTicket
        [CreateDate] => 2013-02-01 08:28:37
        [Customer] => MCustomer
            (
            )

        [Description] => Free Yogurt
        [Disclaimer] => 
        [Discount] => 3.00
        [DiscountType] => Currency
        [ExpireDate] => 2013-02-15 04:59:59
        [QuantityDiscounted] => 0
        [RedeemDate] => 0000-12-31 05:00:00
        [SKUBasket] => 
        [SKUDiscounted] => 
        [SKUQuantityRequired] => 
        [Source] => 
        [Title] => Free Stuff
    )
*/
require_once(dirname(__FILE__)."/mercury_customer.php");
require_once(dirname(__FILE__)."/mercury_baseresponseclass.php");
class MLoyaltyCoupon extends MBaseResponseClass
{
	public 
	$Code,
    $CouponClass,
    $CouponType,
    $CreateDate,
    $Customer,
    $Description,
    $Disclaimer,
    $Discount,
    $DiscountType,
    $ExpireDate,
    $QuantityDiscounted,
    $RedeemDate,
    $SKUBasket,
    $SKUDiscounted,
    $SKUQuantityRequired,
    $Source,
    $Title;
    
    public function __construct(DOMNode $root = null){
		$this->initialize($root);
	}
}


?>