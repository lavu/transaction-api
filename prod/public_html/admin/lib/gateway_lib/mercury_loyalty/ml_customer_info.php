<?php
require_once("../../info_inc.php");
$order_id       = $_REQUEST['order_id'];
$location_id    = $_REQUEST['loc_id'];
$dataname       = $_REQUEST['dn'];
$check_number   = $_REQUEST['check'];
$action			= $_REQUEST['action'];
$goal 			= $_REQUEST['goal'];
$incentive		= $_REQUEST['incentive'];
$discount_info 	= $_REQUEST['discount_info'];
$subtype        = $_REQUEST['subtype'];

$selected_coupon_code = "";
if(!empty($discount_info)){
	$discount_parts = explode("|", $discount_info);
	if(isset($discount_parts[2])){
		$selected_coupon_code =  $discount_parts[2];
	}
}

//ini_set('display_errors','1');
require_once("comm/response/tag_decoder.php");
require_once("comm/setup/mercury_loyalty.php");
require_once("comm/response/mercury_responsehandler.php");

$fail = false;
$mercuryFactory = new MLoyalty();
if(isset($location_info['mercury_loyalty_api_key'])){
	$mercuryFactory->setAPIKey($location_info['mercury_loyalty_api_key']);
}

if(isset($location_info['mercury_loyalty_api_secret'])){
	$mercuryFactory->setAPISecret($location_info['mercury_loyalty_api_secret']);
}


$comm = $mercuryFactory->createGetStatus();
$customeridentifier = $_REQUEST['customeridentifier'];
$loyaltynumber = $_REQUEST['loyaltynumber'];
$couponcode = $_REQUEST['couponcode'];
if(!empty($customeridentifier)){
	$comm->addVariable("customeridentifier",$customeridentifier);
}
if(!empty($loyaltynumber)){
	$comm->addVariable("cardnumber",$loyaltynumber);
}

if(!empty($couponcode)){
	$comm = $mercuryFactory->createGetCoupon();
	$comm->addVariable("code", $couponcode);
}

$result;
$error_message = "Unable to Load Mercury Loyalty at this Time.";
if(!$comm){
	$fail = true;
}
else{
	$response = $comm->process();
	$responseHandler = new MResponseHandler();
	$result = $responseHandler->parse($comm);
	
	if($result !== null){
		
	}
	else{
		$fail = true;
		$result = $responseHandler->getError();
		
	}
//echo "<pre>" . print_r($_REQUEST, true) . "</pre>";
}
?>
<!doctype HTML>
<html>
	<head>
		<style type="text/css">
			body, div, span, table, tr, td, th, tbody, thead {
				margin: 0 0 0 0;
				border: 0px solid black;
				padding: 0 0 0 0;
				font-size: medium;
			}
			
			body {
				background-color: white;
				min-width: 600px;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="page/styles/mercury_loyalty.css" />
		<script type="text/javascript">
			var currentSelection = null;
			var toggleSelection = function(event){
				var element = event.currentTarget;
				if(currentSelection){
					currentSelection.removeAttribute('selected');
				}
				
				if(element == currentSelection){
					currentSelection = null;
					return;
				}
				
				element.setAttribute('selected', '');
				currentSelection = element;
			};
			
			var getCouponInfo = function(){
				var element = currentSelection;
				if(!element)
					return "";
				var coupontype = "check";
				if(element.getAttribute('CouponType') == 'EntireTicket'){
					coupontype = "check";
				}
				else{
					
				}
				
				var discounttype = "p";
				if(element.getAttribute('DiscountType') == 'Currency'){
					discounttype = "d";
				}
				else if(element.getAttribute('DiscountType') == 'Percentage'){
					discounttype = "p";
				}
				else{
					
				}
				
				var discountamount = element.getAttribute('Discount');
				var discountdescription = element.getAttribute('Description');
				var discountcode = element.getAttribute('Code');
				
				var result = "&discount=" + coupontype + "|" + discounttype + "|" + discountamount + "|" + discountdescription + "|" +  discountcode;
				return result;
				//&discount=check|p|0.15|MLP 10% Off|1122334455|more_info
			}
		</script>
	</head>
	<body style="background: url(page/images/win_overlay_full.png) no-repeat no-repeat;">
		<div style="width: 95%; margin: 0 auto; position: relative; left: 15px;">
		<?php
			if($result instanceof MLoyaltyCoupon){
				$coupon = $result;
				if(empty($customeridentifier)){
					$customeridentifier = $coupon->Customer->EmailAddress;
				}
				
				if(empty($customeridentifier)){
					$customeridentifier = $coupon->Customer->MobileNumber;
				}
				
				$name_segment = array();
				if($coupon->Customer->FirstName)
					$name_segment[] = $coupon->Customer->FirstName;
				if($coupon->Customer->LastName)
					$name_segment[] = $coupon->Customer->LastName;
					
				$printcustomername = implode(" ", $name_segment);
				if(empty($printcustomername)){
					$printcustomername = $customeridentifier;
				}
				
				$coupon_type = "check";
				if($coupon->CouponType == "EntireTicket"){
					$coupon_type = "check";
				}
				
				$discount_type = "d";
				if($coupon->DiscountType == "Currency"){
					$discount_type = "d";
				}
				else if($coupon->DiscountType == "Percentage"){
					$discount_type = "p";
				}
				
				
				if((strtotime($coupon->RedeemDate) <= time() && strtotime($coupon->RedeemDate) > strtotime('2013-01-01Z00:00:00'))){
					$fail = true;
					$error_message = "This Coupon has Already Been Redeemed.";
				}
				else if(empty($customeridentifier)){
					$fail = true;
					$error_message = "Unable to Find Customer Based on the Provided Coupon.";
				}
				else{
					echo <<<SCRIPT
				<script type="text/javascript">
					window.location = "_DO:cmd=set_mercury_loyalty&customer=mercury_loyalty|$printcustomername|{$coupon->Customer->ID}|$customeridentifier|$action&discount={$coupon_type}|{$discount_type}|{$coupon->Discount}|{$coupon->Description}|{$coupon->Code}_DO:cmd=close_overlay";
				</script>
SCRIPT;
				exit();						
				}
			}
			else if($result instanceof MLoyaltyProgramStatus){
				$customername = array();
				if($result->Customer->FirstName){
					$customername[] = $result->Customer->FirstName;
				}
				if($result->Customer->LastName){
					$customername[] = $result->Customer->LastName;
				}
				
				if(empty($customeridentifier)){
					$customeridentifier = $result->Customer->EmailAddress;
				}
				
				if(empty($customeridentifier)){
					$customeridentifier = $result->Customer->MobileNumber;
				}
				
				$printcustomername = implode(" ", $customername);
				if(empty($printcustomername)){
					$printcustomername = $customeridentifier;
				}
				
				$fields = array(
					'Local' => array(
						'CurrentCredits' => 'Points',
						'LastVisit' => 'Last Visit',
						'LifetimeCredit' => 'Rewards Earned',
						'RatingName' => 'Customer Rank'
					),
					'Customer' => array(
						'Birthday' => 'Birthday',
						'Anniversary' => 'Anniversary'
					)
				);
				$toprint = "";
				
				foreach($fields as $name => $arr){
					foreach($arr as $fieldname => $displayname){
						
						$value = $result->$name->$fieldname;
						
						if( !empty($value) && in_array($fieldname, array( "LastVisit", "Birthday", "Anniversary")) ){
							$value = date("Y-m-d h:i:s", strtotime($value));
						}
						
						if(empty($value)){
							$value = "N/A";
						}
						$toprint .= "<td><div style=\"text-align: center; font-size: small;\">$displayname</div><div style=\"text-align: center; font-size: small;\">$value</div></td>";
					}
				}
				
				$coupons = $result->OpenCoupons;
				$tablecontents = "";
				$coupon_selected = null;
				foreach($coupons as $coupon){
					
					$issue_date = date("Y-m-d h:i:s",strtotime($coupon->CreateDate));
					$expire_date = date("Y-m-d h:i:s",strtotime($coupon->ExpireDate));
					
					$attributes_arr = array();
					foreach($coupon as $name => $val){
						if($name == "Customer"){
							continue;
						}
						if(empty($val))
							$attributes_arr[]  = $name;
						else
							$attributes_arr[] = "$name=\"$val\"";
					}					
					$attributes = implode(" ", $attributes_arr);
					$selected_data = "";
					if($selected_coupon_code == $coupon->Code){
						$selected_data = "selected";
					}
					
					$tablecontents .= <<<COUPON
				<tr $attributes onclick="toggleSelection(event)" $selected_data>
					<td>
						$coupon->Code
					</td>
					<td>
						$coupon->Description
					</td>
					<td>
						$coupon->Discount
					</td>
					<td>
						$issue_date
					</td>
					<td>
						$expire_date
					</td>
				</tr>
COUPON;
				}
				
				
				$content = file_get_contents(dirname(__FILE__) . '/customer_information.htm');
				if($content === FALSE){
					$content = "Unable to Load Customer Information at this Time.";
				}
				else{
					$customerid = $result->Customer->ID;
					$command_base = "window.location = \"_DO:cmd=set_mercury_loyalty&customer=mercury_loyalty|{$printcustomername}|{$customerid}|{$customeridentifier}|{$action}\"";
					
					if($subtype == '2'){
						echo "<script type\"text/javascript\">{$command_base} + \"_DO:cmd=close_overlay\"; </script>";
						exit();
					}
					
					$command_extended = $command_base . " + getCouponInfo() + \"_DO:cmd=close_overlay\";";
					$command_base .= " + \"_DO:cmd=close_overlay\";";
					$content = str_ireplace("<![CDATA [customername]]>", $printcustomername, $content);
					$content = str_ireplace("<![CDATA [customerinformation]]>", $toprint, $content);
					$content = str_ireplace("<![CDATA [availablecoupons]]>", $tablecontents, $content);
					$content = str_ireplace("<![CDATA [acceptdata]]>", "$command_extended", $content);
					$content = str_ireplace("<![CDATA [canceldata]]>", "$command_base", $content);
				}
				echo $content;
			}
			if($result instanceof MLoyaltyException || $fail){
				$content = file_get_contents(dirname(__FILE__) . '/ml_error.htm');
				if($content === FALSE){
					$content = "An Error Occured, and We are Unable to Display it at this Time.";
				}
				else{
					$message = "";
					if($result instanceof MLoyaltyException){
						$message = $result->Reason;
					}
					else{
						$message = $error_message;
					}
					$content = str_ireplace("<![CDATA [backurl]]>", "gateway_lib/mercury_loyalty/loyalty1.php", $content);
					if(!empty($customeridentifier)){
						$printcustomername;
						$accept_action = "use";
						if(empty($printcustomername)){
							$printcustomername = $customeridentifier;
							$accept_action = "create";
						}
						$customer_info = <<<HTML
						<br />
						<div style="text-align: center;">
							Customer's Display Name: {$printcustomername}
							<br />
							<div style="text-align: left; margin: 0 auto; width: 300px;">
								Tap:
								<ul>
									<li>Accept to {$accept_action} this Customer</li>
									<li>Back to try again</li>
									<li>Cancel if you'd like to cancel</li>
								</ul>
							</div>
						</div>
HTML;
						
						
						$content = str_ireplace("<![CDATA [errormessage]]>", $message . $customer_info, $content);
						
						$content = str_ireplace("<![CDATA [acceptbutton]]>", "<button class=\"acceptbutton\" onclick=\"window.location='_DO:cmd=set_mercury_loyalty&customer=mercury_loyalty|{$customeridentifier}||{$customeridentifier}|{$action}_DO:cmd=close_overlay'\"></button><div class=\"inline-block\" style=\"width: 24px;\"></div>", $content);
					}
					else{
						$content = str_ireplace("<![CDATA [errormessage]]>", $message, $content);
					}
				}
				echo $content;
			}
			else{
				//echo "Unexpected Result.";
				//echo "<pre>" . print_r($result, true) . "</pre>";
			}
		?>
		</div>
		<script type="text/javascript">
			{
				var rows = document.getElementsByTagName('tr');
				for(var i = 0; i < rows.length; i++){
					if(rows[i].hasAttribute('selected')){
						currentSelection = rows[i];
						break;
					}
				}
			}
		</script>
	</body>
</html>