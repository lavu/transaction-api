<?php
require_once("../../info_inc.php");
$order_id       = $_REQUEST['order_id'];
$location_id    = $_REQUEST['loc_id'];
$dataname       = $_REQUEST['dn'];
$check_number   = $_REQUEST['check'];
$subtype        = $_REQUEST['subtype'];
$loyalty_info	= $_REQUEST['loyalty_info'];

$customeridentifier = "";
if(!empty($loyalty_info)){
	$loyalty_data = explode("|", $loyalty_info);
	if(isset($loyalty_data[1]))
		$customeridentifier = $loyalty_data[1];
}

//$subtype == 1;  Initial Checkout Part
//$subtype == 2;  Last Checkout Part

ini_set('display_errors','1');
require_once("comm/response/tag_decoder.php");
require_once("comm/setup/mercury_loyalty.php");
require_once("comm/response/mercury_responsehandler.php");

$fail = false;
$mercuryFactory = new MLoyalty();
if(isset($location_info['mercury_loyalty_api_key'])){
	$mercuryFactory->setAPIKey($location_info['mercury_loyalty_api_key']);
}

if(isset($location_info['mercury_loyalty_api_secret'])){
	$mercuryFactory->setAPISecret($location_info['mercury_loyalty_api_secret']);
}
$comm = $mercuryFactory->createGetProgramDescription();

$error_message = "Unable to Load Mercury Loyalty at this Time.";
if(!$comm){
	$fail = true;
	
}
else{
	$response = $comm->process();
	$responseHandler = new MResponseHandler();
	$result = $responseHandler->parse($comm);
	
	if($result !== null){
		//echo  "<pre>" . print_r($result, true) . "</pre>";
		$programactive = $result->Options['isprogramactive'];
		if(!$programactive){
			$fail = true;
			$error_message = "The Specified Loyalty Program is not Currently Active.";
		}
	}
	else{
		$fail = true;
		$error_message = $responseHandler->getError();
		//echo htmlspecialchars($responseHandler->getError()) . "<br />";
		//echo "<br /><pre>" . htmlspecialchars(print_r($comm->getConnectionInfo(), true)) . "</pre>";
	}
}
//echo "<pre>" . print_r($_REQUEST, true) . "</pre>";
?>
<!doctype HTML>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="page/styles/mercury_loyalty.css" />
	</head>
	<body style="background: url(page/images/win_overlay_keysplit.png) no-repeat no-repeat;">
		<div id="content" style="width: 100%; margin: 0 auto; text-align: center;">
			<div class="loyalty_logo"><br /><img src="page/images/logo_mercury_loyalty.png" alt="Mercury Loyalty" /></div>
			<?php
			$content = "";
			 if( empty($location_id) || empty($dataname) || $fail){
				$content = <<<CONTENT
				<div class="inline-block center width30p">$error_message</div>
				<input type='button' onclick='window.location = "_DO:cmd=close_overlay";' value='Close' />
CONTENT;
				//echo "<pre>" . print_r($_REQUEST, true) . "</pre>";
			}
			else{
			$coupon_section ="";
				if($subtype == '1'){
					$coupon_section = <<<CONTENT
			<div class="inline-block center inputarea">
				<label for="couponcode">Coupon Code</label><br />
				<input type="text" id="couponcode" />
			</div>
CONTENT;
				}
			
				$content = <<<CONTENT
			<input type="hidden" id="action" value="{$result->Action}" />
			<input type="hidden" id="goal" value="{$result->Goal}" />
			<input type="hidden" id="incentive" value="{$result->Incentive}" />
			<div class="inline-block center inputarea">
				<label for="customeridentifier">Mobile or Email Address</label><br />
				<input type="email" id="customeridentifier" value="$customeridentifier" />
			</div>
			<div class="inline-block center inputarea">
				<label for="loyaltynumber">Loyalty Card Number</label><br />
				<input type="number" id="loyaltynumber" />
			</div>
			$coupon_section
			<br />
			<div class="inline-block center" style="padding: 5px 5px 5px 5px;">
				<button class="cancelbutton" onclick='window.location = "_DO:cmd=close_overlay";'></button>
				<div class="inline-block" style="width: 48px;"></div>
				<button class="acceptbutton" onclick="submit()"></button>
			</div>
CONTENT;
			
			}
			echo $content;
			?>
		</div>
		
		<script type="text/javascript">
			var elements = new Array(
				'customeridentifier',
				'loyaltynumber',
				'couponcode',
				'action',
				'goal',
				'incentive'
			);
			
			var processing = false;
			var submit = function(){
				if(processing)
					return;
				processing = true;
				var vars = "";
				var inputs = document.getElementsByTagName('input');
				for(var i = 0; i < inputs.length; i++){
					if(inputs[i].value){
						if(vars){
							vars += "&";
						}
						vars += escape(inputs[i].id) + "=" + escape(inputs[i].value);
					}
				}
				
				var eles = new Array();
				for(var i = 0; i < elements.length; i++){
					eles[i] = document.getElementById(elements[i]);
				}
				
				if(vars!="" && eles[0] && eles[1] && (eles[0].value || eles[1].value || (eles[2] && eles[2].value)) ){
					window.location = "_DO:cmd=load_url&filenames=gateway_lib/mercury_loyalty/ml_customer_info.php?" + vars;
					
				}
				
				processing = false;
			}
			
			var keydownhandler = function(event){
				if(event.keyCode == 13){
					submit();
				}
			}
		
			for(var i = 0; i < 3; i++){
				if(document.getElementById(elements[i]))
					document.getElementById(elements[i]).addEventListener('keydown', keydownhandler);
			}
			if(document.getElementById('customeridentifier'))
				document.getElementById('customeridentifier').focus();
		</script>
		<!--
		<table cellpadding=5 bgcolor='#FFFFFF'>
			<tr>
				<td>
					VARS: <?php echo print_r($_REQUEST, true); ?>
					<br><input type='button' onclick='window.location = "_DO:cmd=set_mercury_loyalty&customer=mercury_loyalty|Common Name|LoyaltyAccountNumber|more_info";' value='Test Set Customer' />
					<br><input type='button' onclick='window.location = "_DO:cmd=set_mercury_loyalty&discount=check|d|1.00|MLP $1.00 Off|1234567890|more_info";' value='Test Dollar Amount Discount' />
					<br><input type='button' onclick='window.location = "_DO:cmd=set_mercury_loyalty&discount=check|p|0.1|MLP 10% Off|9876543210|more_info";' value='Test Percentage Discount' />
					<br><input type='button' onclick='window.location = "_DO:cmd=set_mercury_loyalty&customer=mercury_loyalty|John Wayne|LoyaltyAccountNumber|more_info&discount=check|p|0.15|MLP 10% Off|1122334455|more_info";' value='Test Set Customer and Discount' />
					<br><input type='button' onclick='window.location = "_DO:cmd=close_overlay";' value='Close' />
				</td>
			</tr>
		</table>
		-->
	</body>
</html>
<?php

//_DO:cmd=load_url&filenames=lavuwelcome/welcome_lite2.php&slide=left