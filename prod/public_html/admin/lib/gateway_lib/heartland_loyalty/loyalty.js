
//Util Functions
{
	function DoCommandStartListen() {
		window.location = '_DO:cmd=startListeningForSwipe';
		Stuffer("Listening For Swipe");
	}

	function DoCommandUser(myID){
		window.location = "_DO:cmd=set_loyalty&customer=heartland_loyalty|"+myID+"|"+myID;
		Stuffer("Setting User:"+myID);
	}

	function DoCommandClose(){
		window.location = "_DO:cmd=close_overlay";
		Stuffer("Closing");
	}

	function DoManualEntry(){
		window.location = "_DO:cmd=input_number&for_form=manualEntry&for_field=loyaltyNumber&title=Loyalty Number";
		Stuffer('Manual Entry Mode');
	}

	function setFieldValue(formid, fieldname, setto) {
		handleManualEntry(setto);
		Stuffer(setto);
	}

	function Stuffer(input){
		document.getElementById('stuffme').innerHTML = input;
	}
}

// Stage One / Card and User Processing
{
	function StageOne(){
		if(window.postJSON['loyalty_info'].length > 1){
			var resplosion = window.postJSON['loyalty_info'].split('|');
			if(resplosion.length > 1 && resplosion[2].length > 1){
				HitPorticoWithJSON(window.postJSON);
			}
		}
		DoCommandStartListen();
	}

	function handleManualEntry(setto){
			window.postJSON['card_number'] = setto;
			window.postJSON['encrypted'] = 0;
			delete window.postJSON['reader'];
			delete window.postJSON['mag_data'];
			delete window.postJSON['balance'];
			if(window.issueMode == 1){
				window.postJSON['t_type'] = 'Issue';
			}else{
				delete window.postJSON['t_type'];
			}
			setTimeout(function() { HitPorticoWithJSON(window.postJSON); }, 1);
	}

	function handleCardSwipe(swipeString, hexString, reader, encrypted) {
		window.location = "_DO:cmd=stopListeningForSwipe";
		window.postJSON['reader']      = reader;
		window.postJSON['encrypted']   = encrypted;
		delete window.postJSON['card_number'];
		delete window.postJSON['balance'];
		if(window.issueMode == 1){
			window.postJSON['t_type'] = 'Issue';
		}else{
			delete window.postJSON['t_type'];
		}
		var inputString = "handleCardSwipe "+reader+' '+encrypted+"<br>";
		var cardNumber = "";
		if (hexString.length > 1) {
			inputString += "Hex String Length:"+hexString.length+"<br>";
			window.postJSON['mag_data'] = hexString;
		}else if (swipeString.length > 1) {
			inputString += "Plain String Length: "+swipeString.length+"<br>";
			inputString += "Plain String: "+decodeURIComponent(swipeString)+"<br>";
			window.postJSON['mag_data'] = swipeString;
		}
		Stuffer(inputString);
		setTimeout(function() { HitPorticoWithJSON(window.postJSON); }, 1);
		return 1;
	}

	function HitPorticoWithJSON(sendJSON){
		if(window.blocker == 1){alert('blocked');return;}
		window.blocker = 1;
		var myPost = $.post("portico_interface.php",sendJSON,PorticoResponder);
		return 1;
	}

	function PorticoResponder(responseText){
		window.blocker = 0;
		if(responseText.length < 1){
			Stuffer('Empty response from gateway.');
			DoCommandStartListen();
			return;
		}
		if(responseText.indexOf("PingBack") > -1 ){
			HandlePingBack(responseText);
			return;
		}
		var resplosion = responseText.split('|');
		if(resplosion.length < 6){
			Stuffer('Malformed response from gateway.'+responseText);
			//DoCommandStartListen();
			return;
		}
		if(resplosion[0] != 1){
			Stuffer('Gateway returned failure.'+responseText);
			//DoCommandStartListen();
			return;
		}
		if(resplosion[3] != 'Success'){
			Stuffer('Gateway returned failure.: '+resplosion[3]);
			//DoCommandStartListen();
			return;
		}
		if(window.issueMode == 1){
			Stuffer('Card Issued');
			ToggleIssueMode();
			delete window.postJSON['t_type'];
			setTimeout(function() { HitPorticoWithJSON(window.postJSON); }, 10);
			return ;
		}
		resplosion[2]; //LastFour
		resplosion[3]; //Message
		resplosion[4]; //Balance
		Stuffer('Balance on account '+resplosion[2]+' = '+resplosion[4]);
		window.postJSON['balance'] = resplosion[4];
		//window.postJSON['balance'] = 1000000; #DEV
		GetDiscounts(window.postJSON);
		if(window.postJSON['card_number'].length > 1 ){DoCommandUser(window.postJSON['card_number']);}
		return 1;
	}

	function HandlePingBack(pingbackText){
		Stuffer(pingbackText);
		if(pingbackText.indexOf("%B") > -1 ){
			var resplosion = pingbackText.split('%B');
			Stuffer(resplosion[1].substring(0,19));
			setFieldValue('','',resplosion[1].substring(0,19) );
			return;
		}
		if(pingbackText.indexOf(";") > -1 ){
			var resplosion = pingbackText.split(';');
			Stuffer(resplosion[1].substring(0,19));
			setFieldValue('','',resplosion[1].substring(0,19));
			return;
		}
	}
}

// State Two Discounts
{
	function GetDiscounts(sendJSON){
		if(window.blocker == 1){return;}
		window.blocker = 1;
		Stuffer('Getting Discounts');
		var myPost = $.post("get_discounts.php",sendJSON,DiscountDisplay);
		return;
	}

	function DiscountDisplay(discountsText){
		window.blocker = 0;
		if(discountsText.length > 1){
			document.getElementById('discountList').innerHTML = discountsText;
			Stuffer('Current Loyalty Balance:'+window.postJSON['balance']);
		}else{
			document.getElementById('discountList').innerHTML = 'No discounts available. Points Balance:'+window.postJSON['balance'];
		}
		return;
	}

	function SpendPoints(dcCode,dcCost,dcType){
		if(window.blocker == 1){return;}
		window.blocker = 1;
		window.postJSON.dcCode = dcCode;
		window.postJSON.dcCost = dcCost;
		window.postJSON.dcType = dcType;
		Stuffer("Applying Discount");
		var myPost = $.post("use_discounts.php",postJSON,SpendResponder);
		return;
	}

	function SpendResponder(spendText){
		window.blocker = 0;
		var myResponseText = spendText.trim();
		if(('' == myResponseText)||('{}' == myResponseText)){
			Stuffer("Recieved No Response From Server");
			return;
		}
		var retObj = jQuery.parseJSON( myResponseText );
		if( 'Yes' == retObj.Success.trim()){
			Stuffer("Discount Applied");
			document.location = "_DO:cmd=set_loyalty&"+retObj.DiscountURL;
			setTimeout(function() {DoCommandClose();}, 200);
		}else{
			Stuffer("Error: "+retObj.Error);
		}
		return;
	}
}

// Issue Mode
{
	function ToggleIssueMode(){
		if(window.issueMode == 0){
			window.issueMode = 1;
			document.getElementById('issueButton').style.backgroundColor = 'rgba(255,0,0,0.3)';
		}else{
			window.issueMode = 0;
			document.getElementById('issueButton').style.backgroundColor = 'transparent';
		}
		
	}
}

window.blocker = 0;
window.issueMode = 0;
if(window.postJSON.dn.length > 0){
	setTimeout(function() {StageOne();}, 1);
}

