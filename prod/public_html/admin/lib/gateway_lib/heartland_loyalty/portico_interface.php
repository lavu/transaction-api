<?php
error_reporting(E_ALL & ~E_NOTICE );
if(empty($_REQUEST)){return;}
if(empty($_REQUEST['cc'])){return;}
if(empty($_REQUEST['dn'])){return;}
if(empty($_REQUEST['order_id'])){return;}
if(empty($_REQUEST['loc_id'])){return;}

if(!empty($_REQUEST['loyalty_info']) ){
	$temp = explode ('|' , $_REQUEST['loyalty_info']);
	$_REQUEST['card_number'] = $temp[2];
}

if(empty($_REQUEST['t_type']) ){
	$_REQUEST['PingBack'] = 'PingMe';
	$_REQUEST['transtype'] = 'LoyaltyCardBalance';
}else{
	if($_REQUEST['t_type'] == 'Spend'){
		// GiftCardSale
		$_REQUEST['transtype'] = 'LoyaltyCardSale';
	}elseif($_REQUEST['t_type'] == 'Give'){
		// GiftCardAddValue
		$_REQUEST['transtype'] = 'LoyaltyCardAddValue';
	}elseif($_REQUEST['t_type'] == 'Earn'){
		// GiftCardReward
		$_REQUEST['transtype'] = 'LoyaltyCardReward';
	}elseif($_REQUEST['t_type'] == 'Issue'){
		// GiftCardReward
		$_REQUEST['PingBack'] = 'PingMe';
		$_REQUEST['transtype'] = 'IssueLoyaltyCard';
		$_REQUEST['card_amount'] = '100';
	}
}

include(dirname(__FILE__)."/../../gateway.php");
return;