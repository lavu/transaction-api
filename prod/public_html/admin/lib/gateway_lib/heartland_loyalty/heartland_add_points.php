<?php
require_once( dirname(__FILE__)."/../../info_inc.php");
require_once( "/home/poslavu/public_html/admin/cp/resources/json.php" );
require_once(dirname(__FILE__)."/../heartland.php");
require_once(dirname(__FILE__)."/../../gateway_settings.php");
error_reporting(E_ALL);

function heartlandAddPoints($location_info,$loyalty_info,$discount_info,$points)
{
	error_log('gateway heartlandAddPoints '.$points);
	if(empty($loyalty_info[1])){
		return false;
	}
	$integration_data = get_integration_from_location($_REQUEST['loc_id']);
	include(dirname(__FILE__)."/../../gateway_settings.php");
	global $data_name;
	if($data_name == 'cc_swiper_test'){
		$heartland_server_url = $heartland_stateful_url;
		error_log("gateway In Stateful $heartland_server_url");
	}else{
		error_log("gateway $data_name $heartland_server_url");
	}
	$payment = new HeartlandPayments(
				$integration_data['integration1'],
				$integration_data['integration2'],
				$integration_data['integration3'],
				$integration_data['integration4'],
				$integration_data['integration5'],
				$heartland_server_url
			);
	$payment->setKeyedIn($loyalty_info[1],12,25, false, 'Y','Y');
	$payment->setTotal($points * 100); #DEV
	// $payment->setTotal($points); #release
	$payment->performTransaction('ReloadLoyaltyCard');
	$returnCode = $payment->getCode();
	if($returnCode != '0'){
		error_log('gateway |'.$payment->returnRawReturn().'|'.$payment->returnRawSend());
		return false;
	}
	return;
}
?>
