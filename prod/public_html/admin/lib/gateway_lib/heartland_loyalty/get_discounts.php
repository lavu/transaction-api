<?php
if(empty($_POST)){return;}
require_once(dirname(__FILE__)."/../../info_inc.php");

echo makeButtonsForDiscounts($_POST['balance']);
return;

function getAvailableDiscounts($points){
	$availableDiscounts = array();
	$discounts = getLoyaltyDiscounts();
	foreach ($discounts as $oneDiscount) {
		if($points >= $oneDiscount['loyalty_cost'] ) {
			$availableDiscounts[] = $oneDiscount;
		}
	}
	return $availableDiscounts;
}

function getLoyaltyDiscounts(){
	$discounts = array();
	$myQuery = 'SELECT * FROM `discount_types` WHERE `loyalty_cost` != 0';
	$queryReturn = lavu_query($myQuery);
	while ($row = mysqli_fetch_assoc($queryReturn)) {
		$discounts[] = $row;
	}
	return $discounts;
}

function makeButtonsForDiscounts($points)
{
	if(empty($points)){return;}
	$returnString = '';
	$availableDiscounts = getAvailableDiscounts($points);
	foreach ($availableDiscounts as $oneDiscount) {
		$dc = $oneDiscount['id'];
		$cost = $oneDiscount['loyalty_cost'];
		$type = $oneDiscount['loyalty_use_or_unlock'];
		$name = htmlspecialchars($oneDiscount['title'], ENT_QUOTES);
		$returnString .= "<div class='Discount'>";
		$returnString .= 	"<p>$name  &nbsp;&nbsp;&nbsp;";
		$returnString .= 	"<button class=\"rounded\" onTouchDown=\"SpendPoints('$dc','$cost','$type')\" onclick=\"SpendPoints('$dc','$cost','$type')\">APPLY SAVINGS &#9656;</button>";
		$returnString .= "</div>";
	}
	return $returnString;
}
