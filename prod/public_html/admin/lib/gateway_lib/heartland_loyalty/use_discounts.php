<?php
error_reporting(E_ALL & ~E_NOTICE );
if(empty($_POST)){return;}
if(empty($_POST['dcCode'])){return;}
if(empty($_POST['dcCost'])){return;}
if(empty($_POST['dcType'])){return;}
require_once( dirname(__FILE__)."/../../info_inc.php");
require_once(dirname(__FILE__)."/../heartland.php");

if(1 == $_POST['dcType']){
	spendType();
}else{
	unlockType();
}

function spendType()
{
	$discount = getDiscountInfo();
	$spendInfo = spendPoints($discount);
	if(false == $spendInfo['Success']){
		echo '{"Success":"No","Error":"Not Enough Points On Account"}';
		exit;
	}else{
		generateDiscountURL($discount,$spendInfo);
	}
}

function unlockType()
{
	$discount = getDiscountInfo();
	generateFreeDiscountURL($discount);
}

function getDiscountInfo() // Will either return valid info or exit
{
	if(!is_numeric($_POST['dcCode'])){
		echo '{"Success":"No","Error":"No Discount Code"}';
		exit;
	}
	$dcCode = intval($_POST['dcCode']);
	$myQuery = "SELECT * FROM `discount_types` WHERE `id` = $dcCode";
	$queryReturn = lavu_query($myQuery);
	$row = mysqli_fetch_assoc($queryReturn);
	if(empty($row)){
		echo '{"Success":"No","Error":"Discount Not Found"}';
		exit;
	}
	return $row;
}

function generateFreeDiscountURL($discount){
	$urlString = 'discount=check|'.$discount['code'].'|'.$discount['calc'].'|'.$discount['label'].'|0';
	echo '{"Success":"Yes","DiscountURL":"'.$urlString.'"}';
}

function generateDiscountURL($discount,$spendInfo){
	$urlString = 'discount=check|'.$discount['code'].'|'.$discount['calc'].'|'.$discount['label'].'|'.$spendInfo['AuthCode'];
	echo '{"Success":"Yes","DiscountURL":"'.$urlString.'"}';
}

function spendPoints($discount){
	$spendInfo = array();
	$spendInfo['AuthCode'] = time();
	$spendInfo['Success'] = false;
	$spendInfo['Cost'] = $_POST['dcCost'];
	$spendInfo['Success'] = HeartlandSubtractPoints(explode ('|' , $_REQUEST['loyalty_info']),$_POST['dcCost'] );
	return $spendInfo;
}

function HeartlandSubtractPoints($loyalty_info,$points){
	error_log("gateway HeartlandSubtractPoints");
	if(empty($loyalty_info[1])){
		return false;
	}
	error_log("gateway HeartlandSubtractPoints");
	$integration_data = get_integration_from_location($_REQUEST['loc_id']);
	error_log("gateway HeartlandSubtractPoints");
	include(dirname(__FILE__)."/../../gateway_settings.php");
	global $data_name;
	if($data_name == 'cc_swiper_test'){
		$heartland_server_url = $heartland_stateful_url;
		error_log("gateway In Stateful $heartland_server_url");
	}else{
		error_log("gateway $data_name $heartland_server_url");
	}
	$payment = new HeartlandPayments(
				$integration_data['integration1'],
				$integration_data['integration2'],
				$integration_data['integration3'],
				$integration_data['integration4'],
				$integration_data['integration5'],
				$heartland_server_url
			);
	$payment->setKeyedIn($loyalty_info[1],12,25, false, 'Y','Y');
	$payment->setTotal($points * 100);#dev
	//$payment->setTotal($points);#release
	$payment->performTransaction('LoyaltyCardSale');
	$returnCode = $payment->getCode();
	if($returnCode != '0'){
		return false;
	}
	return true;
}

?>
