<?php

	function translate_card_type($card_type) {
		
		$return_string = "Unknown";
		switch ($card_type) {
			case "1": $return_string = "AMEX"; break;
			case "2": $return_string = "DISCOVER"; break;
			case "3": $return_string = "MASTERCARD"; break;
			case "4": $return_string = "VISA"; break;
			case "5": $return_string = "DEBIT"; break;
			case "6": $return_string = "EBT"; break;
			case "7": $return_string = "EGC"; break;
			case "8": $return_string = "WEX"; break;
			case "9": $return_string = "VOYAGER"; break;
			case "10": $return_string = "JCB"; break;
			case "11": $return_string = "CP"; break;
			default: break;
		}
		
		return $return_string;
	}

	function post_to_merchantware($postvars, $mwtranstype, $data_name, $gateway_debug, $gift_card) {

		global $merchantware_subdomain;
		
		$soap_action = "http://merchantwarehouse.com/MerchantWARE/Client3_1/TransactionRetail";
		$post_url = "MerchantWARE/ws/RetailTransaction/TXRetail31.asmx";
		$subdomain = "ps1";
		if ($gift_card) {
			$soap_action = "http://schemas.merchantwarehouse.com/merchantware/40/GiftcardMagensa10";
			$post_url = "Merchantware/ws/ExtensionServices/v4/GiftcardMagensa10.asmx";
			$subdomain = $merchantware_subdomain;
		}

		$poststr = "";
		for($i=0; $i<count($postvars); $i++) {
			$field_title = $postvars[$i][0];
			$field_value = $postvars[$i][1];			
			$poststr .= "<".$field_title.">".$field_value."</".$field_title.">"; 
		}
		
		$start_data = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
<soap:Body>
<".$mwtranstype." xmlns=\"".$soap_action."\">";
		$end_data = "</".$mwtranstype."></soap:Body></soap:Envelope>";
		$post_data = $start_data.$poststr.$end_data;
		
		$header = array();
		$header[] = "POST /".$post_url." HTTP/1.1";
		$header[] = "Host: ".$subdomain.".merchantware.net";
		$header[] = "Content-Length: ".(strlen($post_data) + 0);
		$header[] = "Content-Type: text/xml; charset=utf-8";
		$header[] = "SOAPAction: \"".$soap_action."/".$mwtranstype."\"";

		ConnectionHub::closeAll();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://".$subdomain.".merchantware.net/".$post_url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		//curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
		$response = curl_exec($ch);
		curl_close($ch);
				
		$start_tag = "<".$mwtranstype."Result>";
		$end_tag = "</".$mwtranstype."Result>";
		
		$start_pos = (strpos($response, $start_tag) + strlen($start_tag));
		$end_pos = strpos($response, $end_tag);
		
		$curl_result = htmlspecialchars_decode(substr($response, $start_pos, ((strlen($response) - $start_pos)) - (strlen($response) - $end_pos)));

		if ($gateway_debug == "1") gateway_debug($data_name, $loc_id, "MERCHANTWARE", $mwtranstype, "XML", "HEADER INFO: ".print_r($header, true)."\n\nPOST_DATA: ".htmlspecialchars_decode($post_data)."\n\nRESPONSE: ".htmlspecialchars_decode($response)."\n\n\nRESULT: ".$curl_result);
		
		return $curl_result;
	}
		
	function process_merchantware2($process_info, $location_info, $transaction_vars) {

		global $server_order_prefix;
	
		$transtype = $process_info['transtype'];
			
		$debug = print_r($process_info, true)."\n\n";

		if ($process_info['set_pay_type_id'] == "") {
			$process_info['set_pay_type_id'] = "2";	
			$process_info['set_pay_type'] = "Card";	
		}

		$send_amount = $process_info['card_amount'];
		
		$cn = $process_info['card_number']; // keep for response string
		$first_four = substr($cn, 0, 4);
		$card_type = $transaction_vars['card_type'];
		
		$swiped = "0";
		$swipe_grade = "H";		
		if ($process_info['mag_data']) {
			$swiped = "1";
			$swipe_grade = "G";
		}
				
		$e2e = false;
		$dukpt_ksn = "";
		$send_mag_data = "";
		if ($process_info['mag_data'] && ($process_info['reader']=="idynamo" || $process_info['reader']=="udynamo")) {
			$e2e = true;
			$split_mag_data = explode("|", $process_info['mag_data']);
			$encrypted_track1 = $split_mag_data[2];
			$encrypted_track2 = $split_mag_data[3];
			$encrypted_track3 = $split_mag_data[4];
			$encrypted_mp = $split_mag_data[6];
			$dukpt_ksn = $split_mag_data[9];
			$mp_status = $split_mag_data[5];
			
			$missing_tracks = 0;
			if (strstr($encrypted_track1, "E?")) {
				$missing_tracks++;
				$encrypted_track1 = "";
			} else {
				$swipe_grade = "C";
			}
			if (strstr($encrypted_track2, "E?")) {
				$missing_tracks++;
				$encrypted_track2 = "";
			} else if ($swipe_grade == "C") {
				$swipe_grade = "A";
			} else {
				$swipe_grade = "B";
			}
			if (strstr($encrypted_track3, "E?")) {
				$missing_tracks++;
				$encrypted_track3 = "";
			}		
			
			$read_error = "";
			if ($missing_tracks == 3) $read_error = "Read Error - Missing track 1, 2, and 3";
			else if (($encrypted_mp == "") || ($dukpt_ksn == "") || ($mp_status == "")) $read_error = "Read Error - Missing MP data, MP status, or KSN";
			
			if ($read_error != "") {

				//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "MerchantWare", $process_info['reader'], $transtype, "99999", $read_error, "", $swiped, $pay_type);

				$log_string = "MERCHANTWARE[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: ".$process_info['card_amount']." - TRANSTYPE: $transtype - CARD TYPE: ??? - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4)." - SWIPED: ".$swiped."[--CR--]EXT_DATA: ".$process_info['ext_data']."[--CR--]MAG DATA: ".$process_info['mag_data']."[--CR--]";
				write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

				return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."||Error|||";
			}
			
		} else if ($process_info['mag_data'] != "") {

			$tracks = explode("?", $process_info['mag_data']);
			$track2_index = 1;
			if (strstr($tracks[0], "=")) {
				$track2_index = 0;
			}
			if (count($tracks) > $track2_index) {
				$track2 = $tracks[$track2_index];
				if (($track2 != "") && ($track2 != "ERROR")) {
					$send_mag_data = str_replace(";", "", $track2);
					$swipe_grade = "E";
				}
			}
		}

		$send_card_info = false;
		if ($transtype == "") $transtype = "Sale";
	
		$log_tip_amount = "";
		
		$gift_card = false;
		$nameKey = "strName";
		$siteIdKey = "strSiteId";
		$keyKey = "strKey";
		$orderNumberKey = "strOrderNumber";
		$amountKey = "strAmount";
		$eTrack1Key = "strEncTrack1";
		$eTrack2Key = "strEncTrack2";
		$eTrack3Key = "strEncTrack3";
		$ksnKey = "strKSN";
		$mpdataKey = "strMPData";
		$mpstatusKey = "strMPStatus";
		$cardNumberKey = "strPAN";
		$pnrefKey = "ReferenceID";
		$authcodeKey = "AuthCode";

		if ($e2e) { // iDynamo or uDynamo swipe

			switch ($transtype) {
				case "Sale" : $mwtranstype = "IssueEncryptedSaleMagensa10"; break;
				case ($transtype=="Auth" || $transtype=="AuthForTab") : $mwtranstype = "IssueEncryptedPreAuthMagensa10"; break;
				//case "Return" : $mwtranstype = "IssueEncryptedRefundMagensa10"; break;
				case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") : $mwtranstype = "ActivateCard"; break;
				case "ReloadGiftCard" : $mwtranstype = "AddValue"; break;
				case "ReloadLoyaltyCard" : $mwtranstype = "AddPoints"; break;
				case ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") : $mwtranstype = "BalanceInquiry"; break;
				case "GiftCardSale" : $mwtranstype = "ForceSale"; break;
				case "LoyaltyCardSale" : $mwtranstype = "RedeemPoints"; break;
				default: break;
			}

		} else if ($swiped == "1") { // BlueBamboo or LineaPro swipe

			switch ($transtype) {
				case "Sale" : $mwtranstype = "IssueSwipedSale"; break;
				case ($transtype=="Auth" || $transtype=="AuthForTab") : $mwtranstype = "IssueSwipedPreAuth"; break;
				case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") : $mwtranstype = "ActivateCardKeyed"; break;
				case "ReloadGiftCard" : $mwtranstype = "AddValueKeyed"; break;
				case "ReloadLoyaltyCard" : $mwtranstype = "AddPointsKeyed"; break;
				case ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") : $mwtranstype = "BalanceInquiryKeyed"; break;
				case "GiftCardSale" : $mwtranstype = "ForceSaleKeyed"; break;
				case "LoyaltyCardSale" : $mwtranstype = "RedeemPointsKeyed"; break;
				default: break;
			}

		} else { // keyed in

			switch ($transtype) {
				case "Sale" : $mwtranstype = "IssueKeyedSale"; break;
				case ($transtype=="Auth" || $transtype=="AuthForTab") : $mwtranstype = "IssueKeyedPreAuth"; break;
				case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") : $mwtranstype = "ActivateCardKeyed"; break;
				case "ReloadGiftCard" : $mwtranstype = "AddValueKeyed"; break;
				case "ReloadLoyaltyCard" : $mwtranstype = "AddPointsKeyed"; break;
				case ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") : $mwtranstype = "BalanceInquiryKeyed"; break;
				case "GiftCardSale" : $mwtranstype = "ForceSaleKeyed"; break;
				case "LoyaltyCardSale" : $mwtranstype = "RedeemPointsKeyed"; break;
				default: break;
			}
		}
		
		switch ($transtype) {
			case ($transtype=="Sale" || $transtype=="Auth" || $transtype=="AuthForTab") :
				$send_card_info = true;
				if ($process_info['order_id'] == "0") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
				if ($transtype=="AuthForTab" && (float)$send_amount==0) {
					$process_info['card_amount'] = $location_info['default_preauth_amount'];
					$send_amount = $process_info['card_amount'];
				}
				break;
			case ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") :
				$mwtranstype = "IssuePostAuth";
				$get_transaction_info = lavu_query("SELECT `auth`, `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				//if (($process_info['card_amount'] != "") && ((float)$process_info['card_amount'] < (float)$transaction_info['amount'])) {
					//$send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				//} else if ($transaction_info['auth'] != "2") {
					//$send_amount = number_format(($transaction_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				//}
				if (!empty($process_info['card_amount'])) $send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				else $send_amount = number_format(($transaction_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				if ($cn == "") $cn = $transaction_info['card_desc'];
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				break;
			case "Adjustment" :
				$mwtranstype = "ApplyTip";
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $decimal_places, ".", "");
				break;
			case ($transtype=="Void" || $transtype=="VoidPreAuthCapture") :
				$mwtranstype = "IssueVoid";
				$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `auth`, `card_type`, `check`, `amount`, `action`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				if ($transaction_info['action'] == "Sale") $void_action = "Payment Voided";
				else if ($transaction_info['action'] == "Refund") $void_action = "Refund Voided";
				$card_type = $transaction_info['card_type'];
				$first_four = $transaction_info['first_four'];
				$cn = $transaction_info['card_desc'];
				if (($transaction_info['transtype']=="Auth" || $transaction_info['transtype']=="AuthForTab") && ($transaction_info['auth']=="1" || $transaction_info['auth']=="2")) {
					$mark_as_voided = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = 'VOIDED', `last_mod_ts` = '[3]' WHERE `id` = '[4]'", $process_info['more_info'], $process_info['server_id'], time(), $transaction_info['id']);
					$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - ".$transaction_info['amount']."), `card_desc` = '', `transaction_id` = '' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
					$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - ".$transaction_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $process_info['loc_id'], $process_info['order_id'], $transaction_info['check']);

					$q_fields = "";
					$q_values = "";
					$l_vars = array();
					$l_vars['action'] = $void_action;
					$l_vars['loc_id'] = $process_info['loc_id'];
					$l_vars['order_id'] = $process_info['order_id'];
					$l_vars['check'] = $process_info['check'];
					$l_vars['time'] = $process_info['device_time'];
					$l_vars['user'] = $process_info['server_name'];
					$l_vars['user_id'] = $process_info['server_id'];
					$l_vars['details'] = $transaction_vars['pay_type']." - ".number_format((float)$transaction_info['amount'], $location_info['disable_decimal'], ".", "");
					$l_vars['device_udid'] = $process_info['device_udid'];
					$keys = array_keys($l_vars);
					foreach ($keys as $key) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
					$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

					GWFupdateSignatureForNorwayActionLog();	// LP-1171

					// START LP-1171
					require_once(__DIR__."/../jcvs/jc_func8.php");
					$currentActionLogId = lavu_insert_id();
					updateSignatureForNorwayActionLog($currentActionLogId);
					// END  LP-1171

					return "1|VOIDED|".substr($cn, (strlen($cn) - 4), 4)."|APPROVED|VOIDED|".$transaction_info['card_type']."||";
				}
				break;
			case "Return" :
				$mwtranstype = "IssueRefundByReference";
				$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $process_info['loc_id'], $process_info['order_id'], $process_info['check'], substr($cn, (strlen($cn) - 4), 4));
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$pnref = $transaction_info['transaction_id'];
				$send_amount = number_format(($send_amount + $process_info['refund_tip_amount']), $location_info['disable_decimal'], ".", "");
				break;
			case "CaptureAll" :
				$mwtranstype = "IssueBatch";
				break;
			case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard" || $transtype=="ReloadGiftCard" || $transtype=="ReloadLoyaltyCard") :
				$gift_card = true;
				$send_card_info = true;
				$dupe_info = checkForPreviousIssueOrReload($process_info);
				if (!empty($dupe_info)) return $dupe_info;
				break;
			case ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") :
				$gift_card = true;
				$send_card_info = true;
				break;
			case ($transtype=="GiftCardSale" || $transtype=="LoyaltyCardSale") :
				$gift_card = true;
				$send_card_info = true;
				if ($transtype == "LoyaltyCardSale") $send_amount = number_format(($process_info['card_amount'] * 10), 0, "", "");
				break;
			case ($transtype=="GiftCardVoid" || $transtype=="LoyaltyCardVoid") :
				$mwtranstype = "VoidSale";
				$gift_card = true;
				break;
			default: break;
		}
		
		if ($gift_card) {
			$nameKey = "merchantName";
			$siteIdKey = "merchantSiteId";
			$keyKey = "merchantKey";
			$orderNumberKey = "invoiceNumber";
			$amountKey = "amount";
			$eTrack1Key = "encryptedTrack1";
			$eTrack2Key = "encryptedTrack2";
			$eTrack3Key = "encryptedTrack3";
			$ksnKey = "ksn";
			$mpdataKey = "magnePrintData";
			$mpstatusKey = "magnePrintStatus";
			$cardNumberKey = "cardNumber";
			$pnrefKey = "TransactionID";
			$authcodeKey = "Token";
		}
		
		$transaction_vars['transtype'] = $transtype;
		$transaction_vars['swipe_grade'] = $swipe_grade;
		
		$q_fields = "";
		$q_values = "";
		$keys = array_keys($transaction_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
	
		$system_id = $transaction_vars['system_id'];
		
		$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";
		
		if ($process_info['data_name']=="DEV" && !$gift_card) {
			$process_info['username'] = "EC284TJN";
			$process_info['password'] = "VVDWT-XE8PR-YXD2Z-R9D1J-3X3A2";
		}

		$postvars = array();
		$postvars[] = array($nameKey, $process_info['integration3']); // POSLavu
		$postvars[] = array($siteIdKey, $process_info['username']); // EC284TJN // B4VGPXA7
		$postvars[] = array($keyKey, $process_info['password']); // VVDWT-XE8PR-YXD2Z-R9D1J-3X3A2 // Y7XX4-Y4W7T-1D211-RVY1H-GTGQB
		if ($transtype != "CaptureAll") {
			$postvars[] = array($orderNumberKey, substr(str_replace("-", "", $process_info['loc_id'].$process_info['order_id']), -8));
		}
		if ($send_card_info) {
			$postvars[] = array($amountKey, $send_amount);
			if ($e2e) {
				$postvars[] = array($eTrack1Key, $encrypted_track1);
				$postvars[] = array($eTrack2Key, $encrypted_track2);
				$postvars[] = array($eTrack3Key, $encrypted_track3);
				$postvars[] = array($ksnKey, $dukpt_ksn);
				$postvars[] = array($mpdataKey, $encrypted_mp);
				$postvars[] = array($mpstatusKey, $mp_status);
				if (!$gift_card) $postvars[] = array("strEntryMode", "MAGNETICSTRIPE");
			} else {
				if ($swiped=="1" && $send_mag_data!="" && !$gift_card) {
					$postvars[] = array("strTrackData", ";".$send_mag_data."?");
					$postvars[] = array("strEntryMode", "MAGNETICSTRIPE");
				} else {
					$postvars[] = array($cardNumberKey, $cn);
					if (!$gift_card) {
						$postvars[] = array("strExpDate", $process_info['exp_month'].$process_info['exp_year']);
						$postvars[] = array("strAVSStreetAddress", $location_info['address']);
						$postvars[] = array("strAVSZipCode", $location_info['zip']);
						$postvars[] = array("strCVCode", $process_info['card_cvn']);
					}
				}
			}
			if (!$gift_card) {
				$postvars[] = array("strCardHolder", $process_info['name_on_card']);
				$postvars[] = array("strAllowDuplicates", "TRUE");
			}
		} else if ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") {
			$postvars[] = array("strAmount", $send_amount);
			$postvars[] = array("strOriginalReferenceID", $process_info['pnref']);
			//$postvars[] = array("strOverrideAmount", $total_amount);
			$postvars[] = array("strAllowDuplicates", "TRUE");
		} else if ($transtype == "Adjustment") {
			$postvars[] = array("strOriginalReferenceID", $process_info['pnref']);
			$postvars[] = array("strTipAmount", $process_info['tip_amount']);
		} else if ($transtype=="Void" || $transtype=="VoidPreAuthCapture") {
			$postvars[] = array("strOriginalReferenceID", $process_info['pnref']);
		} else if ($transtype == "Return") {
			$postvars[] = array("strOriginalReferenceID", $process_info['pnref']);
			$postvars[] = array("strOverrideAmount", $send_amount);
		}
		if ($transtype!="Adjustment" && $transtype!="CaptureAll" && !$gift_card) {
			$postvars[] = array("strRegisterNum", $process_info['register']);
			$postvars[] = array("strTransactionId", $process_info['pnref']);
		}
		if ($transtype=="GiftCardVoid" || $transtype=="LoyaltyCardVoid") $postvars[] = array("token", $process_info['authcode']);
		if ($gift_card) $postvars[] = array("extraData", "Server_ID=".$process_info['server_id']);
		
		$curl_result = post_to_merchantware($postvars, $mwtranstype, $process_info['data_name'], $location_info['gateway_debug'], $gift_card);
		
		if ($transtype == "CaptureAll") {
			$status = extract_value($curl_result, "BatchStatus");
			$resp_index = 0;
		} else {
			$status = extract_value($curl_result, "ApprovalStatus");
			$resp_index = 1;
		}
		$new_pnref = extract_value($curl_result, $pnrefKey);
		if ($new_pnref=="" && $gift_card) $new_pnref = extract_value($curl_result, "MerchantTransactionId");
		if ($new_pnref=="" && $gift_card) $new_pnref = extract_value($curl_result, "Token");
		$authcode = extract_value($curl_result, $authcodeKey);
		$card_type = translate_card_type(extract_value($curl_result, "CardType"));
		$error_message = extract_value($curl_result, "ErrorMessage");
	
		$amount_approved = "";
		$force_sale_zero = false;
		if ($gift_card) {
			$got_amount = extract_value($curl_result, "Amount");
			if ($got_amount!="" && (float)$got_amount!=$send_amount) {
				if ((float)$got_amount>0) {
					$process_info['card_amount'] = $got_amount;
					$amount_approved = " ($got_amount)";
				} else $force_sale_zero = true;
			}
		}
		
		if (strtoupper($status)=="APPROVED" && !$force_sale_zero) {
			$result = "0";
			$respmsg = "APPROVED";
		} else if (strtoupper($status)=="SUCCESS" && !$force_sale_zero) {
			$result = "0";
			$respmsg = "SUCCESS";
		} else if (strtoupper($status)=="DECLINED" || $force_sale_zero) {
			$result = "Declined";
			$respmsg = extract_value($curl_result, "ResponseMessage");
			if (strstr($respmsg, "The card balance =")) {
				$resp_parts = explode("=", $respmsg);
				$respmsg = "Current Balance: ".trim($resp_parts[1]);
			} else if ($force_sale_zero) $respmsg = "Current Balance: ".number_format(0, $location_info['disable_decimal'], ".", "");
		} else if (strstr($curl_result, "Server was unable to process request")) {
			$fault_string = extract_value($curl_result, "faultstring");
			if ($fault_string != "") $respmsg = str_replace(" --->", "", $fault_string);
			else $respmsg = "Server was unable to process request...";
		} else if ($error_message != "") {
			$respmsg = $error_message;
		} else {
			$status_parts = explode(";", $status);
			$result = $status_parts[$resp_index];
			$respmsg = ucfirst($status_parts[$resp_index + 1]);
		}
		
		if ($result == "") $result = $error_message;
		if ($respmsg == "") $respmsg = $error_message;
		
		$debug .= "\n\nMERCHANTWARE\n\n".print_r($postvars, true)."\n\nRESULT: $result\nRESPONSE MESSAGE: $respmsg\n$authcode\n$new_pnref\n$card_type\n\n";

		$approved = "0";
		if ((strtoupper($status)=="APPROVED" || strtoupper($status)=="SUCCESS") && !$force_sale_zero) $approved = "1";
		if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $transaction_vars['loc_id'], "MERCHANTWARE", $transtype, $approved, $debug);
		
		//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "MerchantWARE", $process_info['reader'], $transtype, $result, $respmsg, $card_type, $swiped, $pay_type);
		
		$log_string = "MERCHANTWARE[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - CHECK: ".$process_info['check']." - AMOUNT: ".$send_amount.$amount_approved.$log_tip_amount." - TRANSTYPE: $transtype - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $authcode - CARD TYPE: $card_type - FIRST FOUR: $first_four - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4)." - SWIPED: ".$swiped."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
		if ($transtype == "GiftCardBalance") $log_string .= "Card Balance: ".extract_value($curl_result, "CardBalance")."[--CR--]";
		if ($transtype == "LoyaltyCardBalance") $log_string .= "Points Balance: ".extract_value($curl_result, "PointsBalance")."[--CR--]";
		write_log_entry("gateway", $process_info['data_name'], $log_string."\n");

		$rtn = array();

		if ($approved == "1") {
			
			$gorl_bal_key = (strstr($transtype, "Gift"))?"CardBalance":"PointsBalance";
			$new_balance = "";
			if (in_array($transtype, array("IssueGiftCard", "IssueLoyaltyCard", "ReloadGiftCard", "ReloadLoyaltyCard", "GiftCardSale", "LoyaltyCardSale", "GiftCardVoid", "LoyaltyCardVoid"))) {
				$new_balance = extract_value($curl_result, $gorl_bal_key);
				if (in_array($transtype, array("GiftCardVoid", "LoyaltyCardVoid"))) $process_info['info'] = $new_balance;
				else $process_info['more_info'] = $new_balance;
			}

			$process_info['last_mod_ts'] = time();

			update_tables($process_info, $cn, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");

			if (strstr($transtype, "Balance")) $authcode = extract_value($curl_result, $gorl_bal_key);

			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = $process_info['card_amount'];
			$rtn[] = $process_info['order_id'];
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = $new_balance;
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['last_mod_ts'];

		} else {

			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);

			if ($respmsg == "") $respmsg = "Error - No response received from gateway.";

			$rtn[] = "0"; // 0
			$rtn[] = $respmsg;
			$rtn[] = $new_pnref;
			$rtn[] = $process_info['order_id'];
		}
		
		return implode("|", $rtn);
	}

	function process_merchantware($process_info, $location_info) {
	
		global $server_order_prefix;
	
		$transtype = $process_info['transtype'];
	
		$debug = print_r($process_info, true)."\n\n";
	
		if ($process_info['set_pay_type_id'] == "") {
			$process_info['set_pay_type_id'] = "2";
			$process_info['set_pay_type'] = "Card";
		}
		$pay_type = $process_info['set_pay_type'];
		$pay_type_id = $process_info['set_pay_type_id'];
	
		$send_amount = $process_info['card_amount'];
	
		$cn = $process_info['card_number']; // keep for response string
		$first_four = substr($cn, 0, 4);
		$card_type = getCardTypeFromNumber($cn);
	
		$swiped = "0";
		$swipe_grade = "H";
		if ($process_info['mag_data']) {
			$swiped = "1";
			$swipe_grade = "G";
		}
	
		$e2e = false;
		$dukpt_ksn = "";
		$send_mag_data = "";
		if ($process_info['mag_data'] && ($process_info['reader']=="idynamo" || $process_info['reader']=="udynamo")) {
			$e2e = true;
			$split_mag_data = explode("|", $process_info['mag_data']);
			$encrypted_track1 = $split_mag_data[2];
			$encrypted_track2 = $split_mag_data[3];
			$encrypted_track3 = $split_mag_data[4];
			$encrypted_mp = $split_mag_data[6];
			$dukpt_ksn = $split_mag_data[9];
			$mp_status = $split_mag_data[5];
	
			$missing_tracks = 0;
			if (strstr($encrypted_track1, "E?")) {
				$missing_tracks++;
				$encrypted_track1 = "";
			} else {
				$swipe_grade = "C";
			}
			if (strstr($encrypted_track2, "E?")) {
				$missing_tracks++;
				$encrypted_track2 = "";
			} else if ($swipe_grade == "C") {
				$swipe_grade = "A";
			} else {
				$swipe_grade = "B";
			}
			if (strstr($encrypted_track3, "E?")) {
				$missing_tracks++;
				$encrypted_track3 = "";
			}
	
			$read_error = "";
			if ($missing_tracks == 3) $read_error = "Read Error - Missing track 1, 2, and 3";
			else if (($encrypted_mp == "") || ($dukpt_ksn == "") || ($mp_status == "")) $read_error = "Read Error - Missing MP data, MP status, or KSN";
	
			if ($read_error != "") {
	
				//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "MerchantWARE", $process_info['reader'], $transtype, "99999", $read_error, "", $swiped, $pay_type);
	
				$log_string = "MERCHANTWARE[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - AMOUNT: ".$process_info['card_amount']." - TRANSTYPE: $transtype - CARD TYPE: ??? - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4)." - SWIPED: ".$swiped."[--CR--]EXT_DATA: ".$process_info['ext_data']."[--CR--]MAG DATA: ".$process_info['mag_data']."[--CR--]";
				write_log_entry("gateway", $process_info['data_name'], $log_string."\n");
	
				return "0|Card read failed.\nPlease try again.||".$process_info['order_id']."||Error|||";
			}
	
		} else if ($process_info['mag_data'] != "") {
	
			$tracks = explode("?", $process_info['mag_data']);
			$track2_index = 1;
			if (strstr($tracks[0], "=")) {
				$track2_index = 0;
			}
			if (count($tracks) > $track2_index) {
				$track2 = $tracks[$track2_index];
				if (($track2 != "") && ($track2 != "ERROR")) {
					$send_mag_data = str_replace(";", "", $track2);
					$swipe_grade = "E";
				}
			}
		}
	
		$check_for_duplicate = false;
		$create_transaction_record = false;
		$send_card_info = false;
		if ($transtype == "") $transtype = "Sale";
	
		$log_tip_amount = "";
	
		$gift_card = false;
		$nameKey = "strName";
		$siteIdKey = "strSiteId";
		$keyKey = "strKey";
		$orderNumberKey = "strOrderNumber";
		$amountKey = "strAmount";
		$eTrack1Key = "strEncTrack1";
		$eTrack2Key = "strEncTrack2";
		$eTrack3Key = "strEncTrack3";
		$ksnKey = "strKSN";
		$mpdataKey = "strMPData";
		$mpstatusKey = "strMPStatus";
		$cardNumberKey = "strPAN";
		$pnrefKey = "ReferenceID";
		$authcodeKey = "AuthCode";
	
		if ($e2e) { // iDynamo or uDynamo swipe
	
			switch ($transtype) {
				case "Sale" : $mwtranstype = "IssueEncryptedSaleMagensa10"; break;
				case ($transtype=="Auth" || $transtype=="AuthForTab") : $mwtranstype = "IssueEncryptedPreAuthMagensa10"; break;
				//case "Return" : $mwtranstype = "IssueEncryptedRefundMagensa10"; break;
				case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") : $mwtranstype = "ActivateCard"; break;
				case "ReloadGiftCard" : $mwtranstype = "AddValue"; break;
				case "ReloadLoyaltyCard" : $mwtranstype = "AddPoints"; break;
				case ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") : $mwtranstype = "BalanceInquiry"; break;
				case "GiftCardSale" : $mwtranstype = "ForceSale"; break;
				case "LoyaltyCardSale" : $mwtranstype = "RedeemPoints"; break;
				default: break;
			}
	
		} else if ($swiped == "1") { // BlueBamboo or LineaPro swipe
	
			switch ($transtype) {
				case "Sale" : $mwtranstype = "IssueSwipedSale"; break;
				case ($transtype=="Auth" || $transtype=="AuthForTab") : $mwtranstype = "IssueSwipedPreAuth"; break;
				case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") : $mwtranstype = "ActivateCardKeyed"; break;
				case "ReloadGiftCard" : $mwtranstype = "AddValueKeyed"; break;
				case "ReloadLoyaltyCard" : $mwtranstype = "AddPointsKeyed"; break;
				case ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") : $mwtranstype = "BalanceInquiryKeyed"; break;
				case "GiftCardSale" : $mwtranstype = "ForceSaleKeyed"; break;
				case "LoyaltyCardSale" : $mwtranstype = "RedeemPointsKeyed"; break;
				default: break;
			}
	
		} else { // keyed in
	
			switch ($transtype) {
				case "Sale" : $mwtranstype = "IssueKeyedSale"; break;
				case ($transtype=="Auth" || $transtype=="AuthForTab") : $mwtranstype = "IssueKeyedPreAuth"; break;
				case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard") : $mwtranstype = "ActivateCardKeyed"; break;
				case "ReloadGiftCard" : $mwtranstype = "AddValueKeyed"; break;
				case "ReloadLoyaltyCard" : $mwtranstype = "AddPointsKeyed"; break;
				case ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") : $mwtranstype = "BalanceInquiryKeyed"; break;
				case "GiftCardSale" : $mwtranstype = "ForceSaleKeyed"; break;
				case "LoyaltyCardSale" : $mwtranstype = "RedeemPointsKeyed"; break;
				default: break;
			}
		}
	
		switch ($transtype) {
			case ($transtype=="Sale" || $transtype=="Auth" || $transtype=="AuthForTab") :
				$check_for_duplicate = true;
				$create_transaction_record = true;
				$send_card_info = true;
				if ($process_info['order_id'] == "0") $process_info['order_id'] = create_new_order_record($process_info, $location_info);
				if ($transtype=="AuthForTab" && (float)$send_amount==0) {
					$process_info['card_amount'] = $location_info['default_preauth_amount'];
					$send_amount = $process_info['card_amount'];
				}
				break;
			case ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") :
				$mwtranstype = "IssuePostAuth";
				$get_transaction_info = lavu_query("SELECT `auth`, `card_desc`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND `action` = 'Sale'", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				//if (($process_info['card_amount'] != "") && ((float)$process_info['card_amount'] < (float)$transaction_info['amount'])) {
				//$send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				//} else if ($transaction_info['auth'] != "2") {
				//$send_amount = number_format(($transaction_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				//}
				if (!empty($process_info['card_amount'])) $send_amount = number_format(($process_info['card_amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				else $send_amount = number_format(($transaction_info['amount'] + $process_info['tip_amount']), $location_info['disable_decimal'], ".", "");
				if ($cn == "") $cn = $transaction_info['card_desc'];
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $location_info['disable_decimal'], ".", "");
				break;
			case "Adjustment" :
				$mwtranstype = "ApplyTip";
				$log_tip_amount = " - TIP AMOUNT: ".number_format((float)$process_info['tip_amount'], $decimal_places, ".", "");
				break;
			case ($transtype=="Void" || $transtype=="VoidPreAuthCapture") :
				$mwtranstype = "IssueVoid";
				$get_transaction_info = lavu_query("SELECT `id`, `transtype`, `auth`, `card_type`, `check`, `amount`, `action`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $process_info);
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				if ($transaction_info['action'] == "Sale") $void_action = "Payment Voided";
				else if ($transaction_info['action'] == "Refund") $void_action = "Refund Voided";
				$card_type = $transaction_info['card_type'];
				$first_four = $transaction_info['first_four'];
				$cn = $transaction_info['card_desc'];
				if (($transaction_info['transtype']=="Auth" || $transaction_info['transtype']=="AuthForTab") && ($transaction_info['auth']=="1" || $transaction_info['auth']=="2")) {
					$mark_as_voided = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = 'VOIDED', `last_mod_ts` = '[3]' WHERE `id` = '[4]'", $process_info['more_info'], $process_info['server_id'], time(), $transaction_info['id']);
					$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - ".$transaction_info['amount']."), `card_desc` = '', `transaction_id` = '' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
					$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - ".$transaction_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $process_info['loc_id'], $process_info['order_id'], $transaction_info['check']);
	
					$q_fields = "";
					$q_values = "";
					$l_vars = array();
					$l_vars['action'] = $void_action;
					$l_vars['loc_id'] = $process_info['loc_id'];
					$l_vars['order_id'] = $process_info['order_id'];
					$l_vars['check'] = $process_info['check'];
					$l_vars['time'] = $process_info['device_time'];
					$l_vars['user'] = $process_info['server_name'];
					$l_vars['user_id'] = $process_info['server_id'];
					$l_vars['details'] = $pay_type." - ".number_format((float)$transaction_info['amount'], $location_info['disable_decimal'], ".", "");
					$l_vars['device_udid'] = $process_info['device_udid'];
					$keys = array_keys($l_vars);
					foreach ($keys as $key) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
					$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

					GWFupdateSignatureForNorwayActionLog();	// LP-1171
	
					return "1|VOIDED|".substr($cn, (strlen($cn) - 4), 4)."|APPROVED|VOIDED|".$transaction_info['card_type']."||";
				}
				$create_transaction_record = true;
				break;
			case "Return" :
				$mwtranstype = "IssueRefundByReference";
				$get_transaction_info = lavu_query("SELECT `transaction_id` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `card_desc` = '[4]' AND `voided` != '1' AND `action` = 'Sale'", $process_info['loc_id'], $process_info['order_id'], $process_info['check'], substr($cn, (strlen($cn) - 4), 4));
				$transaction_info = mysqli_fetch_assoc($get_transaction_info);
				$pnref = $transaction_info['transaction_id'];
				$create_transaction_record = true;
				$send_amount = number_format(($send_amount + $process_info['refund_tip_amount']), $location_info['disable_decimal'], ".", "");
				break;
			case "CaptureAll" :
				$mwtranstype = "IssueBatch";
				break;
			case ($transtype=="IssueGiftCard" || $transtype=="IssueLoyaltyCard" || $transtype=="ReloadGiftCard" || $transtype=="ReloadLoyaltyCard") :
				$gift_card = true;
				$create_transaction_record = true;
				$send_card_info = true;
				$dupe_info = checkForPreviousIssueOrReload($process_info);
				if (!empty($dupe_info)) return $dupe_info;
				break;
			case ($transtype=="GiftCardBalance" || $transtype=="LoyaltyCardBalance") :
				$gift_card = true;
				$send_card_info = true;
				break;
			case ($transtype=="GiftCardSale" || $transtype=="LoyaltyCardSale") :
				$gift_card = true;
				$check_for_duplicate = true;
				$create_transaction_record = true;
				$send_card_info = true;
				if ($transtype == "LoyaltyCardSale") $send_amount = number_format(($process_info['card_amount'] * 10), 0, "", "");
				break;
			case ($transtype=="GiftCardVoid" || $transtype=="LoyaltyCardVoid") :
				$mwtranstype = "VoidSale";
				$gift_card = true;
				$create_transaction_record = true;
				break;
			default: break;
		}
	
		if ($gift_card) {
			$nameKey = "merchantName";
			$siteIdKey = "merchantSiteId";
			$keyKey = "merchantKey";
			$orderNumberKey = "invoiceNumber";
			$amountKey = "amount";
			$eTrack1Key = "encryptedTrack1";
			$eTrack2Key = "encryptedTrack2";
			$eTrack3Key = "encryptedTrack3";
			$ksnKey = "ksn";
			$mpdataKey = "magnePrintData";
			$mpstatusKey = "magnePrintStatus";
			$cardNumberKey = "cardNumber";
			$pnrefKey = "TransactionID";
			$authcodeKey = "Token";
		}
	
		$transaction_vars = generate_transaction_vars($process_info, $location_info, $cn, $card_type, $transtype, $swipe_grade, $pay_type, $pay_type_id, $process_info['card_amount']);
	
		
	
		$q_fields = "";
		$q_values = "";
		$keys = array_keys($transaction_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
	
		$system_id = 0;
		if ($create_transaction_record) {
			$preliminary_record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields) VALUES ($q_values)", $transaction_vars);
			if (!$preliminary_record_transaction) return "0|Failed to create transaction record.||".$process_info['order_id'];
			$system_id = lavu_insert_id();
		}
	
		$debug .= "\n\nSWIPE GRADE: ".$swipe_grade."\n\n";
	
		if ($process_info['data_name']=="DEV" && !$gift_card) {
			$process_info['username'] = "EC284TJN";
			$process_info['password'] = "VVDWT-XE8PR-YXD2Z-R9D1J-3X3A2";
		}
	
		$postvars = array();
		$postvars[] = array($nameKey, $process_info['integration3']); // POSLavu
		$postvars[] = array($siteIdKey, $process_info['username']); // EC284TJN // B4VGPXA7
		$postvars[] = array($keyKey, $process_info['password']); // VVDWT-XE8PR-YXD2Z-R9D1J-3X3A2 // Y7XX4-Y4W7T-1D211-RVY1H-GTGQB
		if ($transtype != "CaptureAll") {
			$postvars[] = array($orderNumberKey, substr(str_replace("-", "", $process_info['loc_id'].$process_info['order_id']), -8));
		}
		if ($send_card_info) {
			$postvars[] = array($amountKey, $send_amount);
			if ($e2e) {
				$postvars[] = array($eTrack1Key, $encrypted_track1);
				$postvars[] = array($eTrack2Key, $encrypted_track2);
				$postvars[] = array($eTrack3Key, $encrypted_track3);
				$postvars[] = array($ksnKey, $dukpt_ksn);
				$postvars[] = array($mpdataKey, $encrypted_mp);
				$postvars[] = array($mpstatusKey, $mp_status);
				if (!$gift_card) $postvars[] = array("strEntryMode", "MAGNETICSTRIPE");
			} else {
				if ($swiped=="1" && $send_mag_data!="" && !$gift_card) {
					$postvars[] = array("strTrackData", ";".$send_mag_data."?");
					$postvars[] = array("strEntryMode", "MAGNETICSTRIPE");
				} else {
					$postvars[] = array($cardNumberKey, $cn);
					if (!$gift_card) {
						$postvars[] = array("strExpDate", $process_info['exp_month'].$process_info['exp_year']);
						$postvars[] = array("strAVSStreetAddress", $location_info['address']);
						$postvars[] = array("strAVSZipCode", $location_info['zip']);
						$postvars[] = array("strCVCode", $process_info['card_cvn']);
					}
				}
			}
			if (!$gift_card) {
				$postvars[] = array("strCardHolder", $process_info['name_on_card']);
				$postvars[] = array("strAllowDuplicates", "TRUE");
			}
		} else if ($transtype=="PreAuthCapture" || $transtype=="PreAuthCaptureForTab") {
			$postvars[] = array("strAmount", $send_amount);
			$postvars[] = array("strOriginalReferenceID", $process_info['pnref']);
			//$postvars[] = array("strOverrideAmount", $total_amount);
			$postvars[] = array("strAllowDuplicates", "TRUE");
		} else if ($transtype == "Adjustment") {
			$postvars[] = array("strOriginalReferenceID", $process_info['pnref']);
			$postvars[] = array("strTipAmount", $process_info['tip_amount']);
		} else if ($transtype=="Void" || $transtype=="VoidPreAuthCapture") {
			$postvars[] = array("strOriginalReferenceID", $process_info['pnref']);
		} else if ($transtype == "Return") {
			$postvars[] = array("strOriginalReferenceID", $process_info['pnref']);
			$postvars[] = array("strOverrideAmount", $send_amount);
		}
		if ($transtype!="Adjustment" && $transtype!="CaptureAll" && !$gift_card) {
			$postvars[] = array("strRegisterNum", $process_info['register']);
			$postvars[] = array("strTransactionId", $process_info['pnref']);
		}
		if ($transtype=="GiftCardVoid" || $transtype=="LoyaltyCardVoid") $postvars[] = array("token", $process_info['authcode']);
		if ($gift_card) $postvars[] = array("extraData", "Server_ID=".$process_info['server_id']);
	
		$curl_result = post_to_merchantware($postvars, $mwtranstype, $process_info['data_name'], $location_info['gateway_debug'], $gift_card);
	
		if ($transtype == "CaptureAll") {
			$status = extract_value($curl_result, "BatchStatus");
			$resp_index = 0;
		} else {
			$status = extract_value($curl_result, "ApprovalStatus");
			$resp_index = 1;
		}
		$new_pnref = extract_value($curl_result, $pnrefKey);
		if ($new_pnref=="" && $gift_card) $new_pnref = extract_value($curl_result, "MerchantTransactionId");
		if ($new_pnref=="" && $gift_card) $new_pnref = extract_value($curl_result, "Token");
		$authcode = extract_value($curl_result, $authcodeKey);
		$card_type = translate_card_type(extract_value($curl_result, "CardType"));
		$error_message = extract_value($curl_result, "ErrorMessage");
	
		$amount_approved = "";
		$force_sale_zero = false;
		if ($gift_card) {
			$got_amount = extract_value($curl_result, "Amount");
			if ($got_amount!="" && (float)$got_amount!=$send_amount) {
				if ((float)$got_amount>0) {
					$process_info['card_amount'] = $got_amount;
					$amount_approved = " ($got_amount)";
				} else $force_sale_zero = true;
			}
		}
	
		if (strtoupper($status)=="APPROVED" && !$force_sale_zero) {
			$result = "0";
			$respmsg = "APPROVED";
		} else if (strtoupper($status)=="SUCCESS" && !$force_sale_zero) {
			$result = "0";
			$respmsg = "SUCCESS";
		} else if (strtoupper($status)=="DECLINED" || $force_sale_zero) {
			$result = "Declined";
			$respmsg = extract_value($curl_result, "ResponseMessage");
			if (strstr($respmsg, "The card balance =")) {
				$resp_parts = explode("=", $respmsg);
				$respmsg = "Current Balance: ".trim($resp_parts[1]);
			} else if ($force_sale_zero) $respmsg = "Current Balance: ".number_format(0, $location_info['disable_decimal'], ".", "");
		} else if (strstr($curl_result, "Server was unable to process request")) {
			$fault_string = extract_value($curl_result, "faultstring");
			if ($fault_string != "") $respmsg = str_replace(" --->", "", $fault_string);
			else $respmsg = "Server was unable to process request...";
		} else if ($error_message != "") {
			$respmsg = $error_message;
		} else {
			$status_parts = explode(";", $status);
			$result = $status_parts[$resp_index];
			$respmsg = ucfirst($status_parts[$resp_index + 1]);
		}
	
		if ($result == "") $result = $error_message;
		if ($respmsg == "") $respmsg = $error_message;
	
		$debug .= "\n\nMERCHANTWARE\n\n".print_r($postvars, true)."\n\nRESULT: $result\nRESPONSE MESSAGE: $respmsg\n$authcode\n$new_pnref\n$card_type\n\n";
	
		$approved = "0";
		if ((strtoupper($status)=="APPROVED" || strtoupper($status)=="SUCCESS") && !$force_sale_zero) $approved = "1";
		if ($location_info['gateway_debug'] == "1") gateway_debug($process_info['data_name'], $loc_id, "MERCHANTWARE", $transtype, $approved, $debug);
	
		//record_stats($process_info['company_id'], $process_info['data_name'], $process_info['loc_id'], "MerchantWARE", $process_info['reader'], $transtype, $result, $respmsg, $card_type, $swiped, $pay_type);
	
		$log_string = "MERCHANTWARE[--CR--]LOCATION: ".$process_info['loc_id']." - REGISTER: ".$process_info['register']." - SERVER ID: ".$process_info['server_id']." - ORDER ID: ".$process_info['order_id']." - CHECK: ".$process_info['check']." - AMOUNT: ".$send_amount.$amount_approved.$log_tip_amount." - TRANSTYPE: $transtype - RESULT: $result - PNREF: $new_pnref - AUTHCODE: $authcode - CARD TYPE: $card_type - FIRST FOUR: $first_four - CARD DESC: ".substr($cn, (strlen($cn) - 4), 4)." - SWIPED: ".$swiped."[--CR--]RESPONSE MESSAGE: ".$respmsg."[--CR--]";
		if ($transtype == "GiftCardBalance") $log_string .= "Card Balance: ".extract_value($curl_result, "CardBalance")."[--CR--]";
		if ($transtype == "LoyaltyCardBalance") $log_string .= "Points Balance: ".extract_value($curl_result, "PointsBalance")."[--CR--]";
		write_log_entry("gateway", $process_info['data_name'], $log_string."\n");
	
		$rtn = array();
	
		if ($approved == "1") {
	
			$gorl_bal_key = (strstr($transtype, "Gift"))?"CardBalance":"PointsBalance";
			$new_balance = "";
			if (in_array($transtype, array("IssueGiftCard", "IssueLoyaltyCard", "ReloadGiftCard", "ReloadLoyaltyCard", "GiftCardSale", "LoyaltyCardSale", "GiftCardVoid", "LoyaltyCardVoid"))) {
				$new_balance = extract_value($curl_result, $gorl_bal_key);
				if (in_array($transtype, array("GiftCardVoid", "LoyaltyCardVoid"))) $process_info['info'] = $new_balance;
				else $process_info['more_info'] = $new_balance;
			}
	
			$process_info['last_mod_ts'] = time();
	
			update_tables($process_info, $cn, $card_type, $new_pnref, $authcode, $system_id, "0", "", "", "", "");
	
			if (strstr($transtype, "Balance")) $authcode = extract_value($curl_result, $gorl_bal_key);
	
			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = substr($cn, -4);
			$rtn[] = $respmsg;
			$rtn[] = $authcode;
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = $process_info['card_amount'];
			$rtn[] = $process_info['order_id'];
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = $new_balance;
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $process_info['last_mod_ts'];
	
		} else {
	
			$remove_transaction = lavu_query("DELETE FROM `cc_transactions` WHERE `id` = '[1]'", $system_id);
	
			if ($respmsg == "") $respmsg = "Error - No response received from gateway.";
	
			$rtn[] = "0"; // 0
			$rtn[] = $respmsg;
			$rtn[] = $new_pnref;
			$rtn[] = $process_info['order_id'];
		}
	
		return implode("|", $rtn);
	}
?>
