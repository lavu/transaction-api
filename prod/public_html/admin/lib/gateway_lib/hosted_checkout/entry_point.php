<?php

	// Lavu ToGo Entry Point for hosted checkouts.

	require_once(__DIR__."/util.php");

	$ltg_debug = (reqvar("ltg_debug", "0") == "1");
	$data_name = reqvar("data_name", ""); // expected global in ltdDebug()

	ltgDebug(__FILE__, "request in: ".json_encode($_REQUEST));

	if (false === isset($_REQUEST['data_name'], $_REQUEST['order_id'], $_REQUEST['loc_id']))
	{
		return;
	}

	session_start();

	$_SESSION['IsLTG'] = true;

	$dataname    = $_REQUEST['data_name'];
	$rest_info   = ValidateDataname($dataname, true);
	$rest_id     = $rest_info['id'];
	$location_id = ValidateLocID($_REQUEST['loc_id']);
	$order_id    = ValidateOrderID($_REQUEST['order_id']);

	if ((false === $rest_info) || (false === $rest_id ))
	{
		ltgDebug(__FILE__, "dataname failed validation: ".$dataname);
		return;
	}

	if (false === $location_id)
	{
		ltgDebug(__FILE__, "location_id failed validation: ".$location_id);
		return;
	}

	if (false === $order_id)
	{
		ltgDebug(__FILE__, "order_id failed validation: ".$order_id);
		return;
	}

	$return_url = $_REQUEST['return_url'];

	$order_info = GetOrderInfo($dataname, $order_id, $location_id);
	if (empty($order_info))
	{
		ltgDebug(__FILE__, "order not found: ".$order_id);
		return;
	}

	$integration_data = GetGatewayInfo($dataname, $location_id);
	if (empty($integration_data))
	{
		ltgDebug(__FILE__, " - GetGatewayInfo failed");
		return;
	}

	switch ($integration_data['gateway'])
	{
		case "EVOSnap":

			echo "Directing to secure payment via Evosnap";

			require_once(__DIR__.'/evosnap_util.php');

			$location_info = GetLocationInfo($dataname, $location_id);

			evoPayment($dataname, $order_id, $location_id, $order_info, $integration_data, $location_info);

			break;

		case "Mercury":

			echo "Directing to secure payment via Mercury Hosted Checkout";

			require_once(__DIR__."/mercury_util.php");

			mercuryPayment($dataname, $order_id, $location_id, $order_info, $integration_data);

			break;

		case "QuickpayDK":

			echo "Directing to secure payment via Quickpay.dk";

			require_once("../quickpay_dk/quickpay_dk.php");

			quickpayPayment($dataname, $order_id, $payment, $location_id, $order_info, $integration_data);

			break;

		default:

			$_SESSION['dataname']	= $dataname;
			$_SESSION['order_id']	= $order_id;
			$_SESSION['location_id']	= $location_id;
			$_SESSION['return_url']	= $_REQUEST['return_url'];
			$_SESSION['is_dev']		= $_REQUEST['is_dev'];

			LavuHostedCheckoutForm("", $ltg_debug);

			break;
	}
?>