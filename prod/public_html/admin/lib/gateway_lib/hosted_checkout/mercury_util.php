<?php
error_reporting(E_ALL);
if(session_id() == ''){
	session_start();
}

require_once(dirname(__FILE__) . "/../../gateway_settings.php");

global $mercHC_URL;
$mercHC_URL = "https://".$mpshc_server_url."/hcws/HCService.asmx?WSDL";

$hcws = new MercuryHCClient();

class MercuryHCClient{
	private $wsClient;
	function __construct(){
		global $mercHC_URL;
		$wsdlURL = $mercHC_URL;
		$this->wsClient = new SoapClient($wsdlURL);
	}
	public function getTypes(){
		return $this->wsClient->__getTypes();
	}
	public function sendInitializeCardInfo($initializeCardInfo){
		$initCardInfoRequest = array("request" => $initializeCardInfo);
		return $this->wsClient->InitializeCardInfo($initCardInfoRequest);
	}
	public function sendInitializePayment($initializePayment){
		$initPaymentRequest = array("request" => $initializePayment);
		return $this->wsClient->InitializePayment($initPaymentRequest);
	}
	public function sendVerifyCardInfo($verifyCardInfo){
		$verifyCardInfoRequest = array("request" => $verifyCardInfo);
		return $this->wsClient->VerifyCardInfo($verifyCardInfoRequest);
	}
	public function sendVerifyPayment($verifyPayment){
		$verifyPaymentRequest = array("request" => $verifyPayment);
		return $this->wsClient->VerifyPayment($verifyPaymentRequest);
	}
}

function mercuryPayment($dataname,$order_id,$location_id,$order_info,$integration_data){
	global $hcws;
    global $mpshc_server_url;
	if(!lavuDeveloperStatus()){
		$goodURL = 'https://admin.poslavu.com/lib/gateway_lib/hosted_checkout/mercury_callback.php?PHPSESSID='.session_id();
	}else{
		$goodURL = 'http://'.$_SERVER['SERVER_NAME'].'/lib/gateway_lib/hosted_checkout/mercury_callback.php?PHPSESSID='.session_id();
	}
	$checkoutURL = "https://" . $mpshc_server_url . "/Checkout.aspx";
	$badURL = 'https://www.lavutogo.com'.$_POST['return_url'].'?return=failure';
	$initPaymentRequest = array(
			"MerchantID"         => "{$integration_data['integration3']}",
			"Password"           => "{$integration_data['integration4']}",
			"Invoice"            => "{$order_id}",
			"TotalAmount"        => $order_info['total'],
			"TaxAmount"          => $order_info['tax'],
			"TranType"           => "Sale",
			"Frequency"          => "OneTime",
			"Memo"               => "LavuToGo",
			"ProcessCompleteUrl" => "{$goodURL}",
			"ReturnUrl"          => "{$badURL}"
		);
	$initPaymentResponse = $hcws->sendInitializePayment($initPaymentRequest);
	$code = $initPaymentResponse->InitializePaymentResult->ResponseCode;
	$message = $initPaymentResponse->InitializePaymentResult->Message;
	if($initPaymentResponse->InitializePaymentResult->ResponseCode != 0){
		error_log(__FILE__." Response Code: {$code} Message: {$message}");
	}
	$payID = $initPaymentResponse->InitializePaymentResult->PaymentID;
	echo "<form id=mercPayment action='{$checkoutURL}' method='post'>\n";
	echo "<input type='hidden' name='PaymentID' value='{$payID}'/>\n";
	echo "</form>\n";
	echo "<script>document.getElementById('mercPayment').submit();</script>\n";
	$_SESSION['dataname']    =$dataname;
	$_SESSION['order_id']    =$order_id;
	$_SESSION['payment_id']  =$payID;
	$_SESSION['location_id'] =$location_id;
	$_SESSION['return_url']  =$_POST['return_url'];
	return;
}

function mercLogPayment($pmntID,$integration_data,$dataname,$order_id, $location_id){
	global $hcws;
	$verifyPaymentRequest = array(
			"MerchantID" => "{$integration_data['integration3']}",
			"Password"   => "{$integration_data['integration4']}",
			"PaymentID"  => $pmntID
		);
	$verifyPaymentResponse = $hcws->sendVerifyPayment($verifyPaymentRequest);
	$code = $verifyPaymentResponse->VerifyPaymentResult->ResponseCode;
	if($code != 0){
		$Status = $verifyPaymentResponse->VerifyPaymentResult->Status;
		$StatusMessage = $verifyPaymentResponse->VerifyPaymentResult->StatusMessage;
		$DisplayMessage = $verifyPaymentResponse->VerifyPaymentResult->DisplayMessage;
		error_log(__FILE__." {$code} {$Status} {$StatusMessage} {$DisplayMessage}");
		return false;
	}
	//error_log(__FILE__.' '.print_r($verifyPaymentResponse->VerifyPaymentResult,1));
	$maskedNumber = $verifyPaymentResponse->VerifyPaymentResult->MaskedAccount;
	$payment = array();
	$payment['card_type'] = $verifyPaymentResponse->VerifyPaymentResult->CardType;
	$payment['record_no'] = $verifyPaymentResponse->VerifyPaymentResult->Token;
	$payment['ref_data'] = $verifyPaymentResponse->VerifyPaymentResult->AcqRefData;
	//$payment['first_four'] = substr($maskedNumber,0,4); //Optional-preferred
	$payment['card_desc'] = substr($maskedNumber,-4); //Optional-preferred
	$payment['amount'] = $verifyPaymentResponse->VerifyPaymentResult->Amount;
	$payment['auth_code'] = $verifyPaymentResponse->VerifyPaymentResult->AuthCode;
	$payment['transtype'] = $verifyPaymentResponse->VerifyPaymentResult->TranType;
	$payment['exp'] = $verifyPaymentResponse->VerifyPaymentResult->ExpDate;
	$payment['transaction_id'] = $verifyPaymentResponse->VerifyPaymentResult->RefNo;
	$payment['preauth_id'] = $payment['transaction_id'];
	$payment['mpshc_pid'] = $pmntID;
	$payment['process_data'] = $verifyPaymentResponse->VerifyPaymentResult->ProcessData;
	$payment['gateway'] = 'Mercury';
	$payment['reader'] = 'ltg_mpshc';
	$payment['server_name'] = 'Lavu ToGo';
	$payment['register'] = 'Lavu ToGo';
	$payment['register_name'] = 'Lavu ToGo';
	$payment['loc_id'] = $location_id;
	$payment['device_udid'] = '4C415655544F474F';
	$payment['total_collected'] = $payment['amount'];
	$payment['info'] = $verifyPaymentResponse->VerifyPaymentResult->CardholderName;
	$payment['check'] = "1";

	$order_info = GetOrderInfo($dataname, $order_id, $location_id);

	$ioid = $order_info['ioid'];
    $result = false;
	if(saveTransaction($payment, $dataname, $ioid, $location_id, $order_id)){
	    prepareOrderConfirmation($dataname, $order_info);
	    UnvoidOrder($dataname, $payment['amount'], $ioid);
	    $result = true;
	}
	return $result;
}