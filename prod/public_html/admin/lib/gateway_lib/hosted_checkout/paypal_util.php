<?php
global $paypayURL;
if(!lavuDeveloperStatus()){
	$paypayURL ='https://api.paypal.com';
}else{
	$paypayURL ='https://api.sandbox.paypal.com';
}
require_once(__DIR__.'/util.php');

function paypayGetToken($client_id,$secret){
	global $paypayURL;

	$toPost = 'grant_type=client_credentials';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_USERPWD, $client_id . ":" . $secret);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $toPost); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL,$paypayURL.'/v1/oauth2/token');

	$response = curl_exec($ch);
	if (empty($response)) {
		error_log(__FILE__.' '.__LINE__." ".curl_error($ch));
		return '';
	}

	curl_close($ch);

	$jsonResponse = json_decode($response,1);
	if( empty($jsonResponse) || empty($jsonResponse['token_type']) || empty($jsonResponse['access_token']) ){return '';}
	$token = $jsonResponse['token_type'].' '.$jsonResponse['access_token'];
	return $token;
}

function paypalCreatePayment($dataname,$order_id, $location_id,$order_info,$integration_data){
	global $paypayURL;
	$token = paypayGetToken($integration_data['integration1'],$integration_data['integration2']);
	session_start();

	$return_url = (empty($_POST['return_url']))?'':$_POST['return_url'];
	$_SESSION['dataname']    = $dataname;
	$_SESSION['order_id']    = $order_id;
	$_SESSION['payment_id']  = $payID;
	$_SESSION['location_id'] = $location_id;
	$_SESSION['return_url']  = $return_url;
	$_SESSION['token']       = $token;
	$_SESSION['total']       = $order_info['total'];

	$headers = array();
	$headers[] = "Authorization: ".$token;
	$headers[] = 'Accept-type: application/json';
	$headers[] = 'Content-type: application/json';
	
	$toPost = array();
	$toPost["intent"] = "sale";
	$toPost["payer"] = array();
	$toPost["payer"]["payment_method"] = 'paypal';
	$toPost["transactions"] = array();
	$transaction = array();
	$transaction['description'] = 'LavuToGo';
	$transaction['amount'] = array();
	$transaction['amount']['total'] = "{$order_info['total']}";
	$transaction['amount']['currency'] = getCurrencyCode($dataname,$location_id);
	$transaction['amount']['details'] = array();
	$transaction['amount']['details']['subtotal'] = $order_info['subtotal'];
	$transaction['amount']['details']['tax'] = $order_info['tax'];
	$toPost["transactions"][] = $transaction;
	$toPost["redirect_urls"] = array();

	if(!lavuDeveloperStatus()){
		$toPost["redirect_urls"]['return_url'] = LavuHostName().'/lib/gateway_lib/hosted_checkout/paypal_callback.php?PHPSESSID='.session_id();
		$toPost["redirect_urls"]['cancel_url'] = LavuHostName().'/lib/gateway_lib/hosted_checkout/cancel.php';
	}else{
		$toPost["redirect_urls"]['return_url'] = 'https://'.$_SERVER['SERVER_NAME'].'/lib/gateway_lib/hosted_checkout/paypal_callback.php?PHPSESSID='.session_id();
		$toPost["redirect_urls"]['cancel_url'] = 'https://'.$_SERVER['SERVER_NAME'].'/lib/gateway_lib/hosted_checkout/cancel.php';
	}
	if($_SESSION['IsLTG']){
		$toPost["redirect_urls"]['cancel_url'] = 'https://www.lavutogo.com'.$return_url.'?return=failure';
	}

	$jsToPost = json_encode($toPost);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$jsToPost); 
	curl_setopt($ch, CURLOPT_URL,$paypayURL.'/v1/payments/payment');
	//curl_setopt($ch, CURLOPT_USERPWD, $client_id . ":" . $secret);
	$response = curl_exec($ch);
	if (empty($response)) {
	    $error = curl_error($ch);
		error_log(__FILE__." ".__LINE__." ".$error);
		return '';
	}

	curl_close($ch);
	$jsonResponse = json_decode($response,1);
	//error_log(__FILE__.' '.__LINE__." ".print_r($jsonResponse,1) );
	if( empty($jsonResponse) || empty($jsonResponse['links'])){return '';}
	$_SESSION['payment_id'] = $jsonResponse['id'];
	foreach ($jsonResponse['links'] as $retURL) {
		if(!empty($retURL['method']) && ($retURL['method'] == 'REDIRECT') ){
			header("Location: ".$retURL['href']);
			return;
		}
	}
	return '';
}

function paypalExecutePayment($token,$payer_id,$payment_id,$dataname,$order_id, $payment, $location_id,$order_info){
	global $paypayURL;
	session_start();

	$headers = array();
	$headers[] = "Authorization: ".$token;
	$headers[] = 'Accept-type: application/json';
	$headers[] = 'Content-type: application/json';
	
	$toPost = array();
	$toPost["payer_id"] = $payer_id;
	$jsToPost = json_encode($toPost);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS,$jsToPost); 
	curl_setopt($ch, CURLOPT_URL,$paypayURL.'/v1/payments/payment/'.$payment_id.'/execute');
	$response = curl_exec($ch);
	if (empty($response)) {
		error_log(__FILE__." ".__LINE__." ".$error);
		return false;
	}

	curl_close($ch);

	$jsonResponse = json_decode($response,1);

	//error_log(__FILE__.' '.print_r($jsonResponse,1) );

	if( empty($jsonResponse) || empty($jsonResponse['state'])){
		error_log(__FILE__." ".__LINE__." ".$error." | ".$response);
		return false;
	}
	if($jsonResponse['state'] != 'approved'){
		return false;
	}
	

	$paidTotal = $jsonResponse['transactions'][0]['amount']['total'];
	if( ($paidTotal + 0.0001) < $_SESSION['total']){
		error_log(__FILE__." ".__LINE__." ".$error." | ".$response);
		return false;
	}
	$payment = array();
	$payment['transaction_id'] = $jsonResponse['id'];
	$payment['card_type'] = 'Paypal'; //Optional-preferred
	$payment['first_four'] = '0000'; //Optional-preferred
	$payment['card_desc'] = 'Paypal'; //Optional-preferred price_currency
	$payment['gateway'] = 'Paypal';
	$payment['register'] = 'LavuToGo';
	$payment['register_name'] = 'LavuToGo';
	$payment['server_name'] = 'LavuToGo';
	$payment['register_name'] = 'LavuToGo';
	$payment['loc_id'] = $location_id;
	$payment['device_udid'] = '4C415655544F474F';
	$payment['mpshc_pid'] = $payment_id;
	$payment['total_collected'] =  $paidTotal;
	$payment['ioid'] = $order_info['ioid'];
	if($_SESSION['IsLTG']){
		ApplyPayment($dataname,$order_id, $payment, $location_id, "paypal_hosted");
		UnvoidOrder($dataname,$order_id, $location_id);
	}else{
		error_log(__FILE__." ".__LINE__." Finished Paypal");
		$transInfo = paypalAppTransaction($dataname,$order_id, $payment, $location_id);
		printJavascriptRedirect("_DO:cmd=addTransaction&tx_info=".urlencode(json_encode($transInfo) ) );
		session_destroy();
	}
	return true;
}

function paypalAppTransaction($dataname,$order_id, $payment, $location_id){
	$toReturn = array();
	$toReturn["amount"] = $payment['total_collected'];
	$toReturn["tip_amount"] = '0';
	$toReturn["total_collected"] = $payment['total_collected'];
	$toReturn["pay_type"] = $payment['card_type'];
	if(empty($_SESSION['REQUEST']['pay_type_id'])){
		error_log(__FILE__.__LINE__.print_r($_SESSION,1));
		$toReturn["pay_type_id"] = null;
	}else{
		$toReturn["pay_type_id"] = $_SESSION['REQUEST']['pay_type_id'];
	}
	$toReturn["transtype"] = 'Sale';
	$toReturn["action"] = 'Sale';
	$toReturn["change"] = '0';
	$toReturn['mpshc_pid'] = $payment['mpshc_pid'];
	$toReturn["internal_id"] = $payment['mpshc_pid'];
	$toReturn["got_response"] = '1';
	return $toReturn; 
}

?>
