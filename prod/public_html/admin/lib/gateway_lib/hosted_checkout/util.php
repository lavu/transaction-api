<?php

	error_reporting(E_ALL);

	require_once(__DIR__."/../../../cp/resources/core_functions.php");

	function GetGatewayInfo($dataname, $location_id=NULL)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$queryString = "SELECT `gateway`, `cc_transtype`, ";
		$queryString .= "AES_DECRYPT(`integration1`, '".integration_keystr()."') as `integration1`, ";
		$queryString .= "AES_DECRYPT(`integration2`, '".integration_keystr()."') as `integration2`, ";
		$queryString .= "AES_DECRYPT(`integration3`, '".integration_keystr()."') as `integration3`, ";
		$queryString .= "AES_DECRYPT(`integration4`, '".integration_keystr()."') as `integration4`, ";
		$queryString .= "AES_DECRYPT(`integration5`, '".integration_keystr()."') as `integration5`, ";
		$queryString .= "AES_DECRYPT(`integration6`, '".integration_keystr()."') as `integration6`, ";
		$queryString .= "AES_DECRYPT(`integration7`, '".integration_keystr()."') as `integration7` ";
		$queryString .= "FROM `locations`";

		if ($location_id !== NULL)
		{
			$queryString .= " WHERE `id`='".$location_id."'";
		}

		$get_gateway_info = lavu_query($queryString);
		if (!$get_gateway_info)
		{
			error_log("util ".__FUNCTION__." failed: ".lavu_dberror());
			return array();
		}

		$gateway_info = mysqli_fetch_assoc($get_gateway_info);
		if (empty($gateway_info))
		{
			error_log("util ".__FUNCTION__." yielded empty result: ".$queryString);
		}

		return $gateway_info;
	}

	function getAltIntegrationData($type, $location_id)
	{
		$queryString = "SELECT `value`, ";
		$queryString .= "AES_DECRYPT(`value2`, '".integration_keystr()."') as `integration1`, ";
		$queryString .= "AES_DECRYPT(`value3`, '".integration_keystr()."') as `integration2`, ";
		$queryString .= "AES_DECRYPT(`value4`, '".integration_keystr()."') as `integration3`, ";
		$queryString .= "AES_DECRYPT(`value5`, '".integration_keystr()."') as `integration4`, ";
		$queryString .= "AES_DECRYPT(`value6`, '".integration_keystr()."') as `integration5`, ";
		$queryString .= "AES_DECRYPT(`value7`, '".integration_keystr()."') as `integration6` ";
		$queryString .= "FROM `config` ";
		$queryString .= "WHERE `setting` = 'alternate_payment_types' AND `value` = '[1]' AND `location` = '[2]'";

		$get_alt_integration_data = lavu_query($queryString, $type, $location_id);
		if (!$get_alt_integration_data)
		{
			error_log("util ".__FUNCTION__." failed: ".lavu_dberror());
			return array();
		}

		$alt_integration_data = mysqli_fetch_assoc($get_alt_integration_data);
		if (empty($alt_integration_data))
		{
			error_log("util ".__FUNCTION__." yielded empty result: ".$queryString);
		}

		return $alt_integration_data;
	}

	function GetLocationInfo($dataname, $location_id)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$location_info = array();

		$get_location_info = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $location_id);
		if ($get_location_info)
		{
			if (mysqli_num_rows($get_location_info) > 0)
			{
				$location_info = mysqli_fetch_assoc($get_location_info);
			}
		}
		else
		{
			error_log("util ".__FUNCTION__." failed on locations table: ".lavu_dberror());
		}

		$get_location_config = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `type` = 'location_config_setting' AND `location` = '[1]'", $location_id);
		while ($config_info = mysqli_fetch_assoc($get_location_config))
		{
			$location_info[$config_info['setting']] = $config_info['value'];
		}

		return $location_info;
	}

	function GetOrderInfo($dataname, $order_id, $location_id=NULL)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$get_order_info = lavu_query("SELECT `id`, `order_id`, `subtotal`, `tax`, `total`, `card_paid`, `void`, `location_id`, `email`, `togo_time`, `togo_name`, `ioid`, `active_device`, `location_id` FROM `orders` WHERE `order_id` = '[1]' AND `location_id` = '[2]' ORDER BY `opened` DESC LIMIT 1", $order_id, $location_id);
		if (!$get_order_info)
		{
			error_log("util ".__FUNCTION__." failed: ".lavu_dberror());
			return array();
		}

		if (mysqli_num_rows($get_order_info) > 0)
		{
			return mysqli_fetch_assoc($get_order_info);
		}
		else
		{
			trigger_error("util ".__FUNCTION__." failed to find order ".$order_id);
			die("Order not found. Please try again momentarily.");
		}

		return false;
	}

	function LavuHostName()
	{
		$hostname = gethostname();
		if (false === strstr($hostname, ".com"))
		{
			// new rackspace infra - we have SSL cert for .poslavu.com
			return "https://".$hostname.".poslavu.com";
		} else {
			// legacy environment
			return "http://".$hostname;
		}
	}

	function callbackIdentifier($order_id, $dataname)
	{
		return $order_id."|".rand(1000,9999)."|".$dataname;
	}

	function parsedCallbackIdentifier($callback_identifier)
	{
		$ci_parts = explode("|", $callback_identifier);
		if (count($ci_parts) != 3)
		{
			error_log("util ".__FUNCTION__." - callback_identifier is incomplete - ".$callback_identifier);
			return false;
		}

		return array(
			'dataname'	=> $ci_parts[2],
			'order_id'	=> $ci_parts[0]
		);
	}

	function requiredSessionKey($key, $filepath)
	{
		if (empty($_SESSION[$key]))
		{
			ltgDebug($filepath, "empty ".$key." in session - ".json_encode($_SESSION));
			return false;
		}

		return $_SESSION[$key];
	}

	function SaveCallbackSession($dataname, $order_identifier, $session_data, $type)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		if (rand(0, 99) % 5)
		{
			CleanUpOldSessionData($dataname, $type);
		}

		unset($session_data['poslavu_234347273_company_info']);

		$location_info = &$session_data['poslavu_234347273_location_info'];
		unset($location_info['integration1']);
		unset($location_info['integration2']);
		unset($location_info['integration3']);
		unset($location_info['integration4']);
		unset($location_info['integration5']);
		unset($location_info['integration6']);
		unset($location_info['integration7']);
		unset($location_info['api_key']);
		unset($location_info['api_token']);

		$session_data['session_save_ts'] = time();

		$save_session = lavu_query("INSERT INTO `callback_sessions` (`order_identifier`, `session_data`, `date_time`, `type`) VALUES ('[1]','[2]', '[3]', '[4]')", $order_identifier, json_encode($session_data), UTCDateTime(true), $type);
		if (!$save_session)
		{
			error_log("util ".__FUNCTION__." failed: ".lavu_dberror());
		}
	}

	function CleanUpOldSessionData($dataname, $type)
	{
		lavu_connect_byid($dataname, true);

		$min_date_time = UTCDateTime(true, (time() - 900)); // 15 minutes

		$clean_sessions = lavu_query("DELETE FROM `callback_sessions` WHERE `type` = '[1]' AND `date_time` <= '[2]'", $type, $min_date_time);
		if (!$clean_sessions)
		{
			error_log("util ".__FUNCTION__." failed: ".lavu_dberror());
		}
	}

/*
 * Retrieves session information relevant to our hosted checkout functionality.
 * Order_id is any identifier used to reinflate the sessions
 * dataname exclusively used for database connection initiation.
 */
 	function GetSessionInfo($dataname, $order_identifier)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$get_session_info = lavu_query("SELECT * FROM `callback_sessions` WHERE `order_identifier` = '[1]'", $order_identifier);
		if (!$get_session_info)
		{
			error_log("util ".__FUNCTION__." failed: ".lavu_dberror());
			return false;
		}

		if (mysqli_num_rows($get_session_info) > 0)
		{
			return mysqli_fetch_assoc($get_session_info);
		}

		return false;
	}

/*
 * Updates the sessions data in the callback sessions database.
 */
	function UpdateSessionData($dataname, $order_identifier, $session_data)
	{
		lavu_connect_byid($dataname, true);

		$update_session = lavu_query("UPDATE `callback_sessions` SET `session_data` = '[1]' WHERE `order_identifier` = '[2]'", json_encode($session_data), $order_identifier);
		if (!$update_session)
		{
			error_log("util ".__FUNCTION__." failed: ".lavu_dberror());
		}
	}

	function printJavascriptRedirect($url)
	{
		$script = "<script type='text/javascript'>\n";
		$script .= "function DoCommandClose(){ window.location = '".$url."';}\n";
		$script .= "window.onload = DoCommandClose();\n";
		$script .= "</script>";

		echo $script;
	}

	function printIframeRedirect($url, $is_ipad)
	{
		$width = "315";
		$height = "472";
		if (!empty($is_ipad))
		{
			$width = "400";
			$height = "527";
		}

		$script	= file_get_contents(__DIR__."/evosnap_appview.php");

		$script = str_replace(array("[evo_window_width]", "[evo_window_height]"), array($width, $height), $script);

		$script .= "<script type='text/javascript'>\n";
		$script .= "function IFrameRedirect()
		{
			var x = document.getElementById('myIframe');
			x.src = '".$url."';
			x.onload = function()
			{
				document.getElementById('activity_shade').style.display = 'none';
			}
		}\n";
		$script .= "window.onload = IFrameRedirect();\n";
		$script .= "</script>";

		echo $script;
	}

	function shortInternalID($loc_id)
	{
		$prefix = $loc_id."0";
		$randomString = str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);

		return $prefix."-".$randomString."-".date("YmdHis");
	}

	function saveTransaction(&$transaction, $dataname, $ioid, $location_id, $order_id)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$tz_save = date_default_timezone_get();
		$account_tz = GetTimezone($dataname, $location_id);
		date_default_timezone_set($account_tz);
		$local_datetime = date("Y-m-d H:i:s");
		date_default_timezone_set($tz_save);

		if (empty($transaction['internal_id']))
		{
			$transaction['internal_id'] = shortInternalID($location_id);
		}

		$default_tx = array(
			'action'				=> "Sale",
			'ag_amount'			=> "",
			'amount'				=> "0",
			'auth'				=> "0",
			'auth_code'			=> "",
			'batch_no'			=> "",
			'card_desc'			=> "",
			'card_type'			=> "",
			'change'				=> "0",
			'check'				=> "1",
			'currency_code'		=> "",
			'customer_id'		=> "0",
			'datetime'			=> $local_datetime,
			'device_udid'		=> "",
			'exp'				=> "",
			'first_four'			=> "",
			'for_deposit'		=> "0",
			'gateway'			=> "",
			'got_response'		=> "1",
			'info'				=> "",
			'info_label'			=> "",
			'ioid'				=> $ioid,
			'is_deposit'			=> "0",
			'last_mod_ts'		=> time(),
			'loc_id'				=> $location_id,
			'more_info'			=> "",
			'mpshc_pid'			=> "",
			'order_id'			=> $order_id,
			'pay_type'			=> "Card",
			'pay_type_id'		=> "2",
			'pin_used_id'		=> "0",
			'preauth_id'			=> "",
			'processed'			=> "0",
			'process_data'		=> "",
			'record_no'			=> "",
			'reader'				=> "",
			'refunded'			=> "0",
			'refunded_by'		=> "0",
			'refund_notes'		=> "",
			'refund_pnref'		=> "",
			'ref_data'			=> "",
			'register'			=> "",
			'register_name'		=> "",
			'rf_id'				=> "",
			'server_id'			=> "0",
			'server_name'		=> "",
			'server_time'		=> UTCDateTime(true),
			'signature'			=> "0",
			'split_tender_id'	=> "0",
			'swipe_grade'		=> "",
			'temp_data'			=> "",
			'tip_amount'			=> "",
			'tip_for_id'			=> "",
			'total_collected'	=> "0",
			'transaction_id'	=> "",
			'transtype'			=> "Sale",
			'voice_auth'			=> "0",
			'voided'				=> "0",
			'voided_by'			=> "0",
			'void_notes'			=> "",
			'void_pnref'			=> "",
		);

		foreach ($transaction as $key => $value)
		{
			$default_tx[$key] = $value;
		}

		$fields = "";
		$values = "";

		buildInsertFieldsAndValues($default_tx, $fields, $values);

		$save_transaction = lavu_query("INSERT INTO `cc_transactions` (".$fields.") VALUES (".$values.")", $default_tx);
		if (!$save_transaction)
		{
			error_log("util ".__FUNCTION__." failed: ".lavu_dberror());
			return false;
		}

		return true;
	}

	function prepareOrderConfirmation($dataname, $order_info)
	{
		$get_company_info = mlavu_query("SELECT `company_name`, `logo_img` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname);
		$c_info = mysqli_fetch_assoc($get_company_info);

		$mailvars = array(
			'customer_email'	=> $order_info['email'],
			'merchant_image'	=> "http://admin.poslavu.com/images/".$dataname."/".$c_info['logo_img'],
			'merchant_name'		=> $c_info['company_name'],
			'order_id'			=> $order_info['order_id'],
			'togo_name'			=> $order_info['togo_name'],
			'togo_time'			=> $order_info['togo_time']
		);

		send_order_confirmation($mailvars);
	}

	function send_order_confirmation($mailvars)
	{
		$subject = "Your Online Order from ".$mailvars['merchant_name'];

		$emailText = "<html><body><img src='".$mailvars['merchant_image']."' /><br />\n";
		$emailText .= "<h3>Hi ".$mailvars['togo_name'].",</h3>\n";
		$emailText .= "<h4>Order confirmation for your online order ".$mailvars['order_id']." scheduled for ".$mailvars['togo_time'].".</h4>\n";
		$emailText .= "<p>\nThe merchant does not guarantee the order will be ready at the time you requested... \n";
		$emailText .= "	Please be patient if they're busy. \n";
		$emailText .= "	Thank you for your service. \n";
		$emailText .= "	Please order online with us again!\n";
		$emailText .= "</p>\n<br><br>\n";
		$emailText .= "<img src='https://www.lavutogo.com/assets/images/powered_by_lavu.png' /></body></html>\n";

		$headers = "From: LavuToGo@lavuinc.com\r\n";
		$headers .= "Reply-To: noreply@lavuinc.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		mail($mailvars['customer_email'], $subject, $emailText, $headers);
	}

	function UnvoidOrder($dataname, $card_paid, $ioid)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$unvoid_order = lavu_query("UPDATE `orders` SET `void` = '0', `card_paid` = '[1]' WHERE `ioid` = '[2]' AND `void` = '3' AND `togo_status` = '1'", $card_paid, $ioid);
		if (!$unvoid_order)
		{
			ltgDebug(__FILE__, "UnvoidOrder failed: ".lavu_dberror());
		}
	}

	function applyTransactionInApp($transaction)
	{
		printJavascriptRedirect("_DO:cmd=addTransaction&tx_info=".rawurlencode(json_encode($transaction)));

		session_destroy();

		return;
	}

	function LavuHostedCheckoutForm($notes="", $ltg_debug=false)
	{
		$dataname = requiredSessionKey("dataname", __FILE__."::".__FUNCTION__);
		if (!$dataname)
		{
			echo "Missing dataname";
			return;
		}

		$order_id = requiredSessionKey("order_id", __FILE__."::".__FUNCTION__);
		if (!$order_id)
		{
			echo "Missing order_id";
			return;
		}

		$location_id = requiredSessionKey("location_id", __FILE__."::".__FUNCTION__);
		if (!$location_id)
		{
			echo "Missing location_id";
			return;
		}

		if (!empty($notes))
		{
			$notes				= "<p id='Header'>".$notes."</p>";
		}
		$postURL				= LavuHostName()."/lib/gateway_lib/hosted_checkout/lavu_callback.php";
		$callback_identifier	= callbackIdentifier($order_id, $dataname);

		SaveCallbackSession($dataname, $callback_identifier, $_SESSION, "lavu_hosted_checkout");

		$checkoutHTML = file_get_contents(__DIR__."/checkout_template.html");
		$checkoutHTML = str_replace(
			array(
				"[{notes}]",
				"[{postURL}]",
				"[{sessionID}]",
				"[{callback_identifier}]",
				"[{ltg_debug}]"
			),
			array(
				$notes,
				$postURL,
				session_id(),
				$callback_identifier,
				$ltg_debug?"1":"0"
			),
			$checkoutHTML
		);

		echo $checkoutHTML;
	}

	function LavuHostedCheckout($dataname, $location_id, $order_id)
	{
	}


	function GetTimezone($dataname, $location_id=NULL)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$queryString = 'SELECT `timezone` FROM `locations`';
		if ($location_id !== NULL)
		{
			$queryString .= " WHERE `id` = {$location_id}";
		}
		$queryString .= ' GROUP BY `timezone` ORDER BY COUNT(*) DESC';
		$result = lavu_query($queryString);

		if (empty($result))
		{
			return 'America/Chicago';
		}

		$resultRows = array();
		$idCount = 0;
		while ($resultRows[] = mysqli_fetch_assoc($result))
		{
			$idCount++;
		}

		if ($idCount < 1)
		{
			return 'America/Chicago';
		}

		if ($idCount > 1)
		{
			error_log('More than one company_id in devices. Guessing. Next time submit a location value');
		}

		if (empty($resultRows[0]['timezone']))
		{
			return 'America/Chicago';
		}

		return $resultRows[0]['timezone'];
	}

	function ValidateDataname($input, $fullInfo=false)
	{
		if (!is_string($input))
		{
			return false;
		}

		if (strlen($input) < 1)
		{
			return false;
		}

		$test = preg_replace("/[^\da-zA-Z\_]/", "", $input);
		if (0 != strcmp($test, $input))
		{
			return false;
		}

		$result = mlavu_query("SELECT `id`,`data_name`,`dev` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '{$input}'");
		while ($resultRows = mysqli_fetch_assoc($result))
		{
			if ($fullInfo)
			{
				return $resultRows;
			}
			else
			{
				return $resultRows['id'];
			}
		}

		return false;
	}

	function ValidateOrderID($input)
	{
		if (!is_string($input))
		{
			return false;
		}

		if (strlen($input) < 1)
		{
			return false;
		}

		$test = preg_replace("/[^\d\-]/", "", $input);
		if (0 != strcmp($test, $input))
		{
			return false;
		}

		return $input;
	}

	function ValidateLocID($input)
	{
		if (!is_string($input))
		{
			return false;
		}

		if (strlen($input) < 1)
		{
			return false;
		}

		$test = preg_replace("/[^\d]/", "", $input);
		if (0 != strcmp($test, $input))
		{
			return false;
		}

		return $input;
	}

	function ValidateAPIKey($dataname, $api_key, $fullInfo=false)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$test = preg_replace("/[^\da-zA-Z]/", "", $api_key);
		if (0 != strcmp($test,$api_key))
		{
			error_log(__FILE__." Invalid API Key {$api_key} {$test}");
			return false;
		}

		$queryString = "SELECT `id`, `menu_id`, `net_path`, `use_net_path` FROM `locations` WHERE `api_key` = AES_ENCRYPT('".$api_key."', 'lavuapikey')";
		$result = lavu_query($queryString);
		if (empty($result))
		{
			error_log('Query Failed: $queryString');
			return false;
		}

		$returnMe = mysqli_fetch_assoc($result);
		if (empty($returnMe))
		{
			error_log("Empty Result: {$queryString}");
			return false;
		}

		if ($fullInfo)
		{
			return $returnMe;
		}
		else
		{
			return $returnMe['id'];
		}
	}

	// Converts currency from decimilized to normalized form $10.00 -> 1000
	function SmallestUnitCurrency($dataname, $input, $location_id=NULL)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$after_decimal = 0;

		$queryString = 'SELECT `disable_decimal` FROM `locations`';
		if ($location_id !== NULL)
		{
			$queryString .= " WHERE `id` = {$location_id}";
		}

		$resultRows = array();
		$idCount = 0;
		$result = lavu_query($queryString);
		if (empty($result))
		{
			error_log("Query Failed: {$queryString}");
			return $input;
		}

		while($resultRows[] = mysqli_fetch_assoc($result))
		{
			$idCount++;
		}

		if ($idCount > 0)
		{
			$after_decimal = $resultRows[0]['disable_decimal'];
		}

		if ($idCount > 1)
		{
			error_log('More than one location. Guessing. Next time submit a location value');
		}

		for ($i = 0; $i < $after_decimal; $i++)
		{
			$input = $input * 10;
		}

		return floor($input);
	}

	function SmallestUnitCurrencyToStandard($dataname, $input, $location_id=NULL)
	{
		ConnectionHub::getConn('rest')->selectDN($dataname);

		$after_decimal = 0;

		$queryString = 'SELECT `disable_decimal` FROM `locations`';
		if ($location_id !== NULL)
		{
			$queryString .= " WHERE `id` = {$location_id}";
		}

		$resultRows = array();
		$idCount = 0;
		$result = lavu_query($queryString);
		if (empty($result))
		{
			error_log("Query Failed: {$queryString}");
			return $input;
		}

		while($resultRows[] = mysqli_fetch_assoc($result ))
		{
			$idCount++;
		}

		if ($idCount > 0)
		{
			$after_decimal = $resultRows[0]['disable_decimal'];
		}

		if ($idCount > 1)
		{
			error_log('More than one location. Guessing. Next time submit a location value');
		}

		for ($i = 0; $i < $after_decimal; $i++)
		{
			$input = $input / 10;
		}

		return number_format($input, $after_decimal);
	}

	function getCurrencyCode($dataname, $location_id)
	{
		$queryString = "SELECT `value`,`value2` FROM `config` WHERE `setting` = 'primary_currency_code' AND `location` = {$location_id}";
		$result = lavu_query($queryString);
		if (empty($result))
		{
			error_log("Query Failed: {$queryString}");
			return;
		}

		$returnMe = mysqli_fetch_assoc($result);
		if (empty($returnMe))
		{
			error_log("Empty Result: {$queryString}");
		}

		return $returnMe['value2'];
	}

	function Callback_Redirect($url)
	{
		error_log("Redirecting Callback: ".print_r($_REQUEST, 1));

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 45);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($_REQUEST));
		curl_exec($ch);

		if (curl_errno($ch))
		{
			error_log("Error during callback redirect: ".curl_error($ch));
		}

		curl_close($ch);
	}

	function LTGBaseGatewayURL($dataname, $location_id)
	{
		//$default_gateway_url = "https://cg1.lavusys.com";
		// as long as we're sure the gateway code will continue to be hosted on the same server
		$default_gateway_url = "http://localhost";

		$rdb = "poslavu_".$dataname."_db";

		$get_ltg_gateway_url = mlavu_query("SELECT `value` FROM `[1]`.`config` WHERE `location` = '[2]' AND `type` = 'location_config_setting' AND `setting` = 'ltg_use_central_server_url'", $rdb, $location_id);
		if (!$get_ltg_gateway_url)
		{
			ltgDebug(__FILE__, "failed to retrieve value for `ltg_use_central_server_url` - ".mlavu_dberror());

			return $default_gateway_url;
		}

		if (mysqli_num_rows($get_ltg_gateway_url) == 0)
		{
			return $default_gateway_url;
		}

		$info = mysqli_fetch_assoc($get_ltg_gateway_url);

		$lavu_central_url = $info['value'];

		if (substr($lavu_central_url, 0, 4) == "http")
		{
			return $lavu_central_url;
		}

		return $default_gateway_url;
	}
?>
