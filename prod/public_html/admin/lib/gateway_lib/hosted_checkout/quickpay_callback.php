<?php
error_reporting(E_ALL);
if( empty($_REQUEST['CUSTOM_ordernumber']) || empty($_REQUEST['CUSTOM_dataname'])){
	error_log('Missing stuff? '.print_r($_REQUEST,1));
	return;
}
require_once(__DIR__.'/util.php');
require_once(__DIR__.'/../quickpay_dk/quickpay_dk.php');
$dataname    = $_POST['CUSTOM_dataname'];
$rest_id     = ValidateDataname($dataname);
$order_id    = ValidateOrderID(str_replace('z','-',$_POST['CUSTOM_ordernumber']));
$location_id = ValidateLocID($_POST['CUSTOM_locid']);

if(isset($_POST['CUSTOM_sessid'])){
	session_id($_POST['CUSTOM_sessid']);
	session_start();
	if($_SESSION['IsLTG'] == false){
		$_SESSION['QP_RESPONSE'] = $_REQUEST;
		error_log(__FILE__.' Saved Quickpay Response');
		return;
	}
}

if(false === $rest_id ){
	error_log(__FILE__." Dataname: {$_POST['CUSTOM_dataname']} failed validation");
	return;
}
if(false === $order_id ){
	error_log(__FILE__." Order ID: {$_POST['CUSTOM_ordernumber']} failed validation");
	return;
}
if(false === $location_id ){
	error_log(__FILE__." Location ID: {$_POST['CUSTOM_loc_id']} failed validation");
	return;
}

$order_info = GetOrderInfo($dataname,$order_id,$location_id);
if(empty($order_info)){
	error_log(__FILE__." No such order: {$order_id}");
	return;
}

$integration_data = GetGatewayInfo($dataname,$location_id);
if (empty($integration_data)){
	error_log(__FILE__." Grab Integration Data Failed");
	return;
}

$quickpay = new QuickpayDKPayments(
			$integration_data['integration1'],
			$integration_data['integration2'],
			$integration_data['integration3']
		);
if(empty($quickpay) ){
	error_log('new QuickpayDKPayments failed?');
	return;
}
$OrderPaymentState = $_REQUEST['state'];
$callback = new QuickpayCallbackHandler($_REQUEST,$integration_data['integration2']);
$test = $callback->Validate();
if($test !== true){
	error_log(__FILE__.' Callback failed validation');
	return;
}
if($OrderPaymentState == 3 || $OrderPaymentState == 1){
	qpLogPayment($dataname,$order_id, $location_id,$order_info);
	error_log(__FILE__.' Quickpay Callback Applied Payment');
	return;
}else{
	error_log(__FILE__."OrderPaymentState : {$OrderPaymentState}");
	$StateInfo = QuickpayCallbackHandler::translateTransactionState($OrderPaymentState);
	return;
}

function qpLogPayment($dataname,$order_id, $location_id,$order_info){
	$payment = array();
	$payment['transaction_id'] = $_REQUEST['transaction'];
	$payment['auth_code'] = $_REQUEST['qpstat'];
	$payment['card_type'] = $_REQUEST['cardtype']; //Optional-preferred
	$payment['first_four'] = substr($_REQUEST['cardnumber'],4); //Optional-preferred
	$payment['card_desc'] = substr($_REQUEST['cardnumber'],-4); //Optional-preferred
	$payment['gateway'] = 'Quickpay';
	$payment['server_name'] = 'LavuToGo';
	$payment['register'] = 'LavuToGo';
	$payment['register_name'] = 'LavuToGo';
	$payment['loc_id'] = $location_id;
	$payment['device_udid'] = '4C415655544F474F';
	$payment['total_collected'] = SmallestUnitCurrencyToStandard($dataname,$_REQUEST['amount'],$location_id);
	$payment['ioid'] = $order_info['ioid'];
	if($_SESSION['IsLTG']){
		ApplyPayment($dataname,$order_id, $payment, $location_id, "quickpay_hosted");
		UnvoidOrder($dataname,$order_id, $location_id);
	}else{
		$transInfo = quickpayAppTransaction($dataname,$order_id, $payment, $location_id);
		printJavascriptRedirect("_DO:cmd=addTransaction&tx_info=".urlencode(json_encode($transInfo) ));
		session_destroy();
	}
	return;
}




