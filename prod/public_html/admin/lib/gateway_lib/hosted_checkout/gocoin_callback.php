<?php
error_log(__FILE__." ".print_r($_REQUEST,1));

error_log(__FILE__." gocoin callback");
error_reporting(E_ALL);

require_once(__DIR__.'/util.php');
if(!empty($_REQUEST['PHPSESSID']) ){
	session_id($_REQUEST['PHPSESSID']);
}
session_start();
//error_log(__FILE__." ".print_r($_SESSION,1));

if(empty($_SESSION['dataname'])    ){
	error_log(__FILE__.' Dataname in session empty');
	echo '1';
	return;
}
if(empty($_SESSION['order_id'])    ){
	error_log(__FILE__.' Order ID in session empty');
	echo '2';
	return;
}
if(!isset($_SESSION['location_id']) ){
	error_log(__FILE__.' Location ID in session empty');
	echo '3';
	return;
}

$dataname    = $_SESSION['dataname'];
$rest_id     = ValidateDataname($dataname);
$order_id    = ValidateOrderID($_SESSION['order_id']);
$location_id = ValidateLocID($_SESSION['location_id']);
$integration_data =  GetGatewayInfo($dataname,$location_id);
$order_info = GetOrderInfo($dataname,$order_id,$location_id);

if(false === $rest_id ){
	error_log(__FILE__." Dataname: {$_SESSION['dataname']} failed validation");
	return;
}
if(false === $order_id ){
	error_log(__FILE__." Order ID: {$_SESSION['order_id']} failed validation");
	return;
}
if(false === $location_id ){
	error_log(__FILE__." Location ID: {$_SESSION['location_id']} failed validation");
	return;
}

if(empty($integration_data)){
	error_log(__FILE__." Get integration data failed.");
	return;
}

$data = json_decode(file_get_contents('php://input'), true);
//error_log(print_r($data,1));

if($_SESSION['payment_id'] != $data['payload']['id']){
	error_log(__FILE__." Payment IDs don't match :  {$_SESSION['payment_id']} : {$data['payload']['id']}");
	return;
}

if($data['event'] == 'invoice_payment_received'){
	//First Confirmation
	//unvoid order
	if($_SESSION['IsLTG'] == false){
		error_log(__FILE__.' Set GC_RESPONSE');
		error_log(__FILE__.print_r($data,1));
		$_SESSION['GC_RESPONSE'] = $data;
		return;
	}
	UnvoidOrder($dataname,$order_id, $location_id);
}

if($data['event'] == 'invoice_ready_to_ship'){
	if($_SESSION['IsLTG'] == false){
		error_log(__FILE__.' Set GC_RESPONSE2');
		$_SESSION['GC_RESPONSE2'] = $data;
		return;
	}
	//Full Confirmation
	// payment confirmed
	$payment = array();
	$payment['transaction_id'] = $data['payload']['id'];
	$payment['card_type'] = $data['payload']['price_currency']; //Optional-preferred
	$payment['first_four'] = '0000'; //Optional-preferred
	$payment['card_desc'] = $data['payload']['price_currency']; //Optional-preferred price_currency
	$payment['gateway'] = 'goCoin';
	$payment['server_name'] = 'LavuToGo';
	$payment['register_name'] = 'LavuToGo';
	$payment['loc_id'] = $location_id;
	$payment['device_udid'] = '4C415655544F474F';
	$payment['mpshc_pid'] = $data['payload']['id'];
	$payment['total_collected'] =  $data['payload']['base_price'];
	ApplyPayment($dataname,$order_id, $payment, $location_id, "gocoin_hosted");
}