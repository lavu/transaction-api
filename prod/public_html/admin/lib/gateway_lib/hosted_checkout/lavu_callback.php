<?php

	session_start();

	error_reporting(E_ALL);

	require_once(__DIR__."/util.php");

	$callback_identifier	= reqvar("cbid", "");
	$ltg_debug				= (reqvar("ltg_debug", "0") == "1");

	$parsed_callback_identifier = parsedCallbackIdentifier($callback_identifier);
	if (!$callback_identifier)
	{
		ltgDebug(__FILE__, "missing or invalid callback_identifier - request in: ".json_encode($_REQUEST), true);
		exit();
	}

	$dataname = $parsed_callback_identifier['dataname'];
	$order_id = $parsed_callback_identifier['order_id'];

	$data_name = $dataname; // expected global in ltgDebug()

	ltgDebug(__FILE__, "request in: ".json_encode($_REQUEST));

	$session_info = GetSessionInfo($dataname, $callback_identifier);
	$_SESSION = json_decode($session_info['session_data'], true);

	$location_id = requiredSessionKey("location_id", __FILE__);
	if (!$location_id)
	{
		exit();
	}

	$order_info = GetOrderInfo($dataname, $order_id, $location_id);
	if (empty($order_info))
	{
		ltgDebug(__FILE__, "failed to GetOrderInfo - order_id: ".$order_id);
		echo "Failed to retrieve order information for order ".$order_id;
		exit();
	}

	$card_number		= reqvar("card_number", "");
	$card_expiration	= reqvar("card_expiration", "");
	$card_code			= reqvar("card_code", "");

	if (empty($card_number))
	{
		LavuHostedCheckoutForm("No card number.", $ltg_debug);
		return;
	}

	$card_number = preg_replace("/[^\d]/", "", $card_number);
	if (empty($card_number))
	{
		LavuHostedCheckoutForm("Invalid card number. Please try again.", $ltg_debug);
		return;
	}

	if (empty($_REQUEST['card_expiration']))
	{
		LavuHostedCheckoutForm("No card expiration.", $ltg_debug);
		return;
	}

	$card_expiration = preg_replace("/[^\d]/", "", $card_expiration);
	if (empty($card_expiration))
	{
		LavuHostedCheckoutForm("Invalid card expiration. Please try again.", $ltg_debug);
		return;
	}

	if (empty($card_code))
	{
		LavuHostedCheckoutForm("No CVN.", $ltg_debug);
		return;
	}

	$card_code = preg_replace("/[^\d]/", "", $card_code);
	if (empty($card_code))
	{
		LavuHostedCheckoutForm("Invalid CVN. Please try again.",$ltg_debug);
		return;
	}

	$vars = array(
		'cc'					=> $dataname.'_key_'.lsecurity_key_by_dn($dataname),
		'check'				=> "1",
		'data_name'			=> $dataname,
		'card_amount'		=> $order_info['total'],
		'card_cvn'			=> $card_code,
		'card_expiration'	=> $card_expiration, // MMYY
		'card_number'		=> $card_number,
		'internal_id'		=> shortInternalID($location_id),
		'ioid'				=> $order_info['ioid'],
		'is_dev'				=> $_SESSION['is_dev'],
		'loc_id'				=> $location_id,
		'more_info'			=> "",
		'order_id'			=> $order_id,
		'poslavu_version'	=> "Lavu ToGo - BE ".currentLavuBackendVersion(),
		'register'			=> "Lavu ToGo",
		'register_name'		=> "Lavu ToGo",
		'server_id'			=> 0,
		'server_name'		=> "Lavu ToGo",
		'transtype'			=> "Sale",
		'UUID'				=> gethostname()
	);

	$gateway_url = LTGBaseGatewayURL($dataname, $location_id)."/lib/gateway.php";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $gateway_url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($vars));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$curl_start = microtime(true);

	$response = curl_exec($ch);

	$curl_end = microtime(true);
	$curl_duration = round((($curl_end - $curl_start) * 1000), 2);
	$curl_info = curl_getinfo($ch);
	$curl_error = curl_error($ch);

	$info_and_error = (empty($response))?" - info: ".json_encode($curl_info)." - error: ".$curl_error:"";
	ltgDebug(__FILE__, "response: ".$response." - duration: ".$curl_duration." ms".$info_and_error);

	curl_close($ch);

	if (empty($response))
	{
		LavuHostedCheckoutForm("No response from gateway. Please try again.", $ltg_debug);
		return;
	}

	$returnArray = explode("|", $response);
	if (empty($returnArray))
	{
		LavuHostedCheckoutForm("Unrecognized gateway response. Please try again.", $ltg_debug);
		return;
	}

	if ((int)$returnArray[0] != 1)
	{
		$decline_message = (empty($returnArray[1]))?"Unknown Error":$returnArray[1];
		LavuHostedCheckoutForm($decline_message, $ltg_debug);
		return;
	}

	UnvoidOrder($dataname, $order_id, $location_id);

	prepareOrderConfirmation($dataname, $order_info);

	header("Location: https://www.lavutogo.com".$_SESSION['return_url']."?return=success");
?>