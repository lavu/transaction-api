<!DOCTYPE html>
<html>
<head>
	<title>Evo Appview</title>
	<style type='text/css'>
		body
		{
			font-family:sans-serif;
			background-color: transparent;
			border: 0 0 0 0;
			margin: 0 0 0 0;
			padding: 0 0 0 0;
			-webkit-tap-highlight-color:rgba(0, 0, 0, 0);
			-webkit-user-select: none;
		}

		.inner_div1 {
			position: absolute;
			left: 0px;
			top: 0px;
			width: [evo_window_width]px;
			height: [evo_window_height]px;
			display: block;
		}

		.evo_iframe {
			border: none;
			width: [evo_window_width]px;
			height: [evo_window_height]px;
			margin: 0;
			padding: 0;
			opacity: 1;
			display:block;
		}

		.msg_txt1 {
			color:#888888;
			font-family:Arial, Helvetica, sans-serif;
			font-size:17px;
		}

	</style>
</head>
<body>
	<div id='iframe_container' class='inner_div1'>
		<iframe id='myIframe' class='evo_iframe' onload='iframeLoad(this);'></iframe>
	</div>
	<div id='activity_shade' class='inner_div1' style='display:block; background-color:#FFFFFF; opacity:0.90;'>
		<table style='width:[evo_window_width]px; height:[evo_window_height]px;'>
			<tr><td align='center' valign='bottom' style='height:171px; padding: 6px 8px 6px 8px;'><span id='activity_message' class='msg_txt1'>Contacting EVO Snap...</span></td></tr>
			<tr><td align='center' valign='top' style='height:241px; padding-top:6px;'><img src='../../../components/payment_extensions/moneris/images/moneris_activity.gif' width='32px' height='32px'/></td></tr>
		</table>
	</div>
</body>
</html>