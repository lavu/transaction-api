<?php
//echo '<pre>'.print_r($_REQUEST,1).'</pre>';
error_reporting(E_ALL);
require_once(__DIR__.'/paypal_util.php');
require_once(__DIR__.'/util.php');

if(!empty($_REQUEST['PHPSESSID']) ){
	session_id($_REQUEST['PHPSESSID']);
}
session_start();

//echo '<pre>'.print_r($_SESSION,1).'</pre>';

if(empty($_SESSION['dataname'])    ){
	error_log(__FILE__.' Dataname in session empty');
	return;
}
if(empty($_SESSION['order_id'])    ){
	error_log(__FILE__.' Order ID in session empty');
	return;
}
if(!isset($_SESSION['location_id']) ){
	error_log(__FILE__.' Location ID in session empty');
	return;
}

$dataname    = $_SESSION['dataname'];
$rest_id     = ValidateDataname($dataname);
$order_id    = ValidateOrderID($_SESSION['order_id']);
$location_id = ValidateLocID($_SESSION['location_id']);
$integration_data =  GetGatewayInfo($dataname,$location_id);
$order_info = GetOrderInfo($dataname,$order_id,$location_id);

if(false === $rest_id ){
	error_log(__FILE__." Dataname: {$_SESSION['dataname']} failed validation");
	return;
}
if(false === $order_id ){
	error_log(__FILE__." Order ID: {$_SESSION['order_id']} failed validation");
	return;
}
if(false === $location_id ){
	error_log(__FILE__." Location ID: {$_SESSION['location_id']} failed validation");
	return;
}

/*
if($_SESSION['payment_id'] != $_REQUEST['paymentId']){
	error_log(__FILE__." Payment IDs don't match :  {$_SESSION['payment_id']} : {$_REQUEST['paymentId']}");
	return;
}*/

if(empty($integration_data)){
	error_log(__FILE__." Get integration data failed.");
	return;
}
$return_url = $_SESSION['return_url'];

if(empty($_REQUEST['paymentId']) ){
	error_log(__FILE__."Payment Id Empty");
	return;
}
if(empty($_SESSION['token']) ){
	error_log(__FILE__."Session: Token Empty");
	return;
}
if(empty($_REQUEST['PayerID']) ){
	error_log(__FILE__."No PayerID");
	return;
}


if(paypalExecutePayment($_SESSION['token'],$_REQUEST['PayerID'],$_REQUEST['paymentId'],$dataname,$order_id, $payment, $location_id,$order_info)){
	if($_SESSION['IsLTG']){
		header("Location: https://www.lavutogo.com".$return_url.'?return=success');
	}
}else{
	if($_SESSION['IsLTG']){
		header("Location: https://www.lavutogo.com".$return_url.'?return=failure');
	}
}
session_destroy();
return;

?>