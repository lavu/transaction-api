<?php
error_reporting(E_ALL);
require_once(__DIR__.'/util.php');

if(session_id() == ''){
	session_start();
}

if(empty($_SESSION['dataname'])){
	error_log(__FILE__.print_r($_SESSION,1).print_r($_REQUEST,1));
	error_log(__FILE__.' Dataname in session empty');
	return;
}
if(empty($_SESSION['order_id'])    ){
	error_log(__FILE__.' Order ID in session empty');
	return;
}
if(empty($_SESSION['payment_id'])  ){
	error_log(__FILE__.' Payment ID in session empty');
	return;
}
if(!isset($_SESSION['location_id']) ){
	error_log(__FILE__.' Location ID in session empty');
	return;
}

$dataname			= $_SESSION['dataname'];
$rest_info			= ValidateDataname($dataname, true);
$rest_id			= $rest_info['id'];
$_REQUEST['is_dev']	= $rest_info['dev'];
$order_id			= ValidateOrderID($_SESSION['order_id']);
$location_id		= ValidateLocID($_SESSION['location_id']);
$integration_data	= GetGatewayInfo($dataname,$location_id);
$order_info			= GetOrderInfo($dataname,$order_id,$location_id);

require_once(__DIR__.'/mercury_util.php');

if(false === $rest_id ){
	error_log(__FILE__." Dataname: {$_SESSION['dataname']} failed validation");
	return;
}
if(false === $order_id ){
	error_log(__FILE__." Order ID: {$_SESSION['order_id']} failed validation");
	return;
}
if(false === $location_id ){
	error_log(__FILE__." Location ID: {$_SESSION['location_id']} failed validation");
	return;
}

if($_SESSION['payment_id'] != $_POST['PaymentID']){
	error_log(__FILE__." Payment IDs don't match :  {$_SESSION['payment_id']} : {$_POST['PaymentID']}");
	return;
}

if(empty($integration_data)){
	error_log(__FILE__." Get integration data failed.");
	return;
}
$return_url = $_SESSION['return_url'];

if(mercLogPayment($_POST['PaymentID'],$integration_data,$dataname,$order_id, $location_id,$order_info)){
	session_destroy();
	header("Location: https://www.lavutogo.com".$return_url.'?return=success');
}else{
	header("Location: https://www.lavutogo.com".$return_url.'?return=failure');
}
return;

?>