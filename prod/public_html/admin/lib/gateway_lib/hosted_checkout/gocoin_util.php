<?php
error_reporting(E_ALL);
if(!isset($_SESSION['IsLTG'])){
	session_start();
}
require_once(__DIR__.'/util.php');
$gocoinURL = 'https://api.gocoin.com/api/v1';

function goCoinPayment($dataname,$order_id,$location_id,$order_info,$integration_data){
	global $gocoinURL;
	$merchantID = $integration_data['integration1'];
	$token = $integration_data['integration2'];
	$_SESSION['dataname'] = $dataname;
	$_SESSION['order_id'] = $order_id;
	$_SESSION['location_id'] = $location_id;
	$callback = 'https://admin.poslavu.com/dev/lib/gateway_lib/hosted_checkout/gocoin_callback.php?PHPSESSID='.session_id();
	if($_SESSION['IsLTG']){
		$_SESSION['return_url']  =$_REQUEST['return_url'];
		$goodURL = 'https://www.lavutogo.com'.$_POST['return_url'].'?return=success';
	}else{
		$goodURL = LavuHostName().'/dev/components/payment_extensions/lavu_hosted_checkout/gocoin_waiting.php?PHPSESSID='.session_id();
	}
	$toSend = array();
	$toSend["base_price"] = $order_info['total'];
	$toSend["callback_url"] = $callback;
	$toSend["redirect_url"] = $goodURL;
	$toSend["base_price_currency"] = getCurrencyCode($dataname,$location_id);
	if(0 == strcmp($dataname,'matt_lavu') ){
		$toSend["base_price_currency"] = 'XDG';
	}
	$toSend["type"] = "bill";
	$headers = array();
	$headers[] = 'Content-type: application/json';
	$headers[] = "Authorization: Bearer ".$token;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($toSend) );
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_URL,$gocoinURL.'/merchants/'.$merchantID.'/invoices');
	$result=curl_exec($ch);
	curl_close($ch);
	$invoice = json_decode($result,1);
	if(empty($invoice) || empty($invoice['id']) ){
		echo '<pre>'.print_r($toSend,1).'</pre>';
		echo '<pre>'.print_r($invoice,1).'</pre>';
		return;
	}
	$_SESSION['payment_id'] = $invoice['id'];
	if(empty($invoice['gateway_url']) ){
		echo '<pre>'.print_r($toSend,1).'</pre>';
		echo '<pre>'.print_r($invoice,1).'</pre>';
		return;
	}
	header("Location: ".$invoice['gateway_url']);
}
