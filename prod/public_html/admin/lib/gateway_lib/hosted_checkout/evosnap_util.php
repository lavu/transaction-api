<?php

	error_reporting(E_ALL);

	if (session_id() == "")
	{
		session_start();
	}

	require_once(__DIR__."/util.php");

	$debug_evosnap = true;

	function evoHostedURL()
	{
		if (lavuDeveloperStatus())
		{
			return "https://cert-hp.evosnap.com/srv/checkout/";
		}
		else
		{
			return "https://hp.evosnap.com/srv/checkout/";
		}
	}

	function evoPayment($dataname, $order_id, $location_id, $order_info, $integration_data, $location_info)
	{
		global $debug_evosnap;

		$evo_hosted_url = evoHostedURL();

		$authkey = $integration_data['integration5'];
		$code = $integration_data['integration4'];

		$is_lavu_togo	= !empty($_SESSION['IsLTG']);
		$is_ipad		= reqvar("is_ipad", "0");

		session_unset();

		$_SESSION['IsLTG']			= $is_lavu_togo;
		$_SESSION['dataname']		= $dataname;
		$_SESSION['order_id']		= $order_id;
		$_SESSION['location_id']		= $location_id;
		$_SESSION['total']			= $order_info['total'];
		$_SESSION['check']			= reqvar("check", "1");
		$_SESSION['register']		= reqvar("register", "");
		$_SESSION['register_name']	= reqvar("register_name", "");
		$_SESSION['server_id']		= reqvar("server_id", "");
		$_SESSION['server_name']		= reqvar("server_name", "");
		$_SESSION['is_ipad']			= $is_ipad;
		
		$device_udid = reqvar("UUID", "");
		if (empty($device_udid))
		{
			$device_udid = reliableRemoteIP();
		}
		$_SESSION['device_udid'] = $device_udid;

		$baseCallbackURL = "https://admin.poslavu.com/";
		if (lavuDeveloperStatus())
		{
			$config_key = ($is_lavu_togo?"ltg_use_central_server_url":"gateway_path");

			$use_url = locationSetting($config_key);
			if (!empty($use_url) && substr($use_url, 0, 4)=="http")
			{
				$baseCallbackURL = $use_url."/";
			}

			error_log("evosnap_util ".__FUNCTION__." baseCallbackURL: ".$baseCallbackURL);
		}

		$_SESSION['ORIGIN'] = LavuHostName();
		$_SESSION['baseCallbackURL'] = $baseCallbackURL;

		$order_id_for_evo = callbackIdentifier($order_id, $dataname);

		$goodURL = $baseCallbackURL."components/payment_extensions/lavu_hosted_checkout/evosnap_waiting.php?order_identifier=".$order_id_for_evo;
		$problemURL = $baseCallbackURL."components/payment_extensions/lavu_hosted_checkout/problem.php";

		if ($is_lavu_togo)
		{
			$return_url = reqvar("return_url", "");
			$_SESSION['return_url']	= "https://www.lavutogo.com".$return_url."?return=success";

			$cancelURL = "https://www.lavutogo.com".$return_url."?return=failure";
		}
		else
		{
			$cancelURL = $baseCallbackURL."components/payment_extensions/lavu_hosted_checkout/cancel.php";
		}

		$order_info['total']		= number_format($order_info['total'], 2, ".", ""); // thousands separator causes error if included in order totals
		$order_info['tax']		= number_format($order_info['tax'], 2, ".", "");
		$order_info['subtotal']	= number_format(($order_info['total'] - $order_info['tax']), 2, ".", "");

		$customer = array( 'email' => "developer@poslavu.com" );

		$order = array(
			'total'				=> $order_info['total'],
			'total_tax'			=> $order_info['tax'],
			'total_subtotal'	=> $order_info['subtotal'],
			'merchant_order_id'	=> $order_id_for_evo
		);

		$orderitem = array(
			array(
				'sku'	=> "Lavu Order",
				'name'	=> 'Lavu Order',
				'price'	=> $order_info['subtotal'],
				'tax'	=> $order_info['tax'],
				'qty'	=> "1"
			)
		);

		$aMACParams = array(
			'code'					=> $code,
			'email'					=> $customer['email'],
			'order_total_subtotal'	=> number_format($order_info['subtotal'], 2), // thousands separator necessary for mac only
			'order_total'			=> number_format($order_info['total'], 2),
			'merchant_order_id'		=> $order_id_for_evo
		);

		$toSend = array(
			'customer'			=> $customer,
			'order'				=> $order,
			'order_item'			=> $orderitem,
			'merchant_order_id'	=> $order_id_for_evo,
			'checkout_layout'	=> "iframe",
			'return_url'			=> $goodURL,
			'cancel_url'			=> $cancelURL,
			//'currency'		=> getCurrencyCode($dataname,$location_id),
			'action'				=> "order",
			'code'				=> $code,
			'mac'				=> create_mac($aMACParams, $authkey)
		);

		if ($debug_evosnap) error_log("evosnap_util toSend: ".json_encode($toSend));

		$toPost = encodeRequest($toSend);

		$rCurl = curl_init();

		curl_setopt($rCurl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($rCurl, CURLOPT_URL, $evo_hosted_url);
		curl_setopt($rCurl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($rCurl, CURLOPT_SSLVERSION, 6);
		curl_setopt($rCurl, CURLOPT_POST, true);
		curl_setopt($rCurl, CURLOPT_SSLVERSION,6);
		curl_setopt($rCurl, CURLOPT_POSTFIELDS, $toPost);

		$aReturn = (array)json_decode(curl_exec($rCurl));

		curl_close($rCurl);

		if ($debug_evosnap)
		{
			error_log("evosnap_util evo_hosted_url: ".$evo_hosted_url);
			error_log("evosnap_util aReturn: ".json_encode($aReturn));
		}

		if ($aReturn['success'] != false)
		{
			SaveCallbackSession($dataname, $order_id_for_evo, $_SESSION, "evosnap");
			if ($is_lavu_togo)
			{
				header("Location: ".$aReturn['url'], true, 303);
			}
			else
			{
				printIframeRedirect($aReturn['url'], $is_ipad);
			}
		}
		else
		{
			header("Location: ".$problemURL."?message=".rawurlencode(translateReturnMessage($aReturn['message'])), true, 303);
		}
	}

	function create_mac($aParams, $sPrivateKey)
	{
		// Order Mac Generation 'code + customer[email] + order[merchant_order_id] + order[total] + order[total_subtotal] + authkey';

		global $debug_evosnap;

		$sUnencoded = "";
		if (!empty($aParams))
		{
			$aTemp = array();
			foreach ($aParams as $key => $val)
			{
				$aTemp[$key] = $val;
			}
			ksort($aTemp);
			$sUnencoded .= implode("", $aTemp);
		}

		if (!empty($sPrivateKey))
		{
			$sUnencoded .= $sPrivateKey;
		}

		$MAC = md5($sUnencoded);

		if ($debug_evosnap) error_log("evosnap_util ".__FUNCTION__.": ".$MAC);

		return $MAC;
	}

	function encodeRequest($aRequest)
	{
		$aFields = array();

		if (is_array($aRequest) && !empty($aRequest))
		{
			foreach ($aRequest as $key => $val)
			{
				if (is_array($val) || is_object($val))
				{
					$aFields[] = $key."=".urlencode(json_encode($val));
				}
				else
				{
					$aFields[] = $key."=".urlencode($val);
				}
			}
		}

		return implode("&", $aFields);
	}

	function translateReturnMessage($message)
	{
		switch ($message)
		{
			case "ERR_MAC":

				return "There was a problem validating the Merchant Authentication Code. Please contact Lavu Customer Care if this problem persists.";

			default:

				return "An error has occurred: ".$message;
		}
	}

	function getLocationProcessingInfo($merchant_order_id)
	{
		// Return FALSE or an array of useful properties. Used in EVOSnap Hosted Checkout

		if (empty($merchant_order_id))
		{
			error_log("evosnap_util ".__FUNCTION__.": merchant_order_id is empty");
			return false;
		}

		$parsed_merchant_order_id = parsedCallbackIdentifier($merchant_order_id);
		if (!$parsed_merchant_order_id)
		{
			return false;
		}

		$dataname = $parsed_merchant_order_id['dataname'];
		$order_id = $parsed_merchant_order_id['order_id'];

		$session_info = GetSessionInfo($dataname, $merchant_order_id);
		$session_data = json_decode($session_info['session_data'], 1);

		$location_id = $session_data['location_id'];

		$order_info = GetOrderInfo($dataname, $order_id, $location_id);

		if (empty($order_info))
		{
			error_log("evosnap_util ".__FUNCTION__.": GetOrderInfo failed");
			return false;
		}
	
		$session_data['order_info'] = $order_info;
	
		return $session_data;
	}
?>