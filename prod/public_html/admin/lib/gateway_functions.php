<?php

require_once(__DIR__."/gateway_settings.php");
require_once(__DIR__."/gateway_lib/Transaction_Controller.php");
require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperApi.php');

// ***** swipe grades *****
//
//  A - E2E with both track 1 and 2
//  B - E2E with track 2 only
//	C - E2E with track 1 only
//  D - standard with both track 1 and track 2
//	E - standard with track 2 only
//  F - standard with track 1 only
//  G - standard swipe but equivalent to keyed in
//  H - keyed in manually

function formattedGatewayApproval($tx_id, $last4, $msg1, $auth_code, $card_type, $record_no="", $amount="", $order_id="", $ref_data="", $process_data="", $new_balance="", $msg2="", $first4="", $last_mod_ts="", $flag="1")
{
	$rtn = array(
		$flag,
		$tx_id,
		$last4,
		$msg1,
		$auth_code,
		$card_type,
		$record_no,
		$amount,
		$order_id,
		$ref_data,
		$process_data, // 10
		$new_balance,
		$msg2,
		$first4,
		$last_mod_ts
	);

	return implode("|", $rtn);
}

function formattedGatewayDecline($title, $message, $order_id="", $pnref="", $message2="") {

	$rtn = array(
		"0",
		$message,
		$pnref,
		$order_id,
		$message2,
		$title
	);

	return implode("|", $rtn);
}

function position_of_first_number($string, $start=0)
{
	for ($i = $start; $i < strlen($string); $i++)
	{
		if (is_numeric($string[$i]))
		{
			$start = $i;
			break;
		}
	}

	return $start;
}

function priceFormatGW($value)
{
	global $location_info;

	$formatted = number_format((float)str_replace(",", "", $value), $location_info['disable_decimal'], $location_info['decimal_char'], ",");
	$symbol = $location_info['monitary_symbol'];

	if ($location_info['left_or_right'] == "right")
	{
		return trim($formatted." ".$symbol);
	}
	else
	{
		return trim($symbol." ".$formatted);
	}
}

	function extract_card_info($mag_data)
	{
		$card = array(
			'number'	=> "",
			'exp_month'	=> "",
			'exp_year'	=> "",
			'name'		=> ""
		);

		$tracks = explode("?", $mag_data);
		$index_adj = 0;
		if (strlen($tracks[0]) < 6)
		{
			$index_adj = 1;
		}

		$track2_index = (1 + $index_adj);
		if (strstr($tracks[$index_adj], "="))
		{
			$track2_index = $index_adj;
		}

		if ((strlen($tracks[1]) > 10) || ($track2_index == $index_adj))
		{
			$got_good_track_2 = false;
			if (strstr($tracks[$track2_index], "="))
			{
				$track2 = explode("=", $tracks[$track2_index]);
				$cn_start = position_of_first_number($track2[0], 0);
				$card['number'] = str_replace(array(" ", "%*", "%", "*;", ";", "*"), array("", "", "", "", "", "0"), substr($track2[0], $cn_start));
				$card['exp_month'] = substr($track2[1], 2, 2);
				$card['exp_year'] = substr($track2[1], 0, 2);
				if (is_numeric(substr($card['number'], 0, 4)) && is_numeric(substr($card['number'], -4)))
				{
					$got_good_track_2 = true;
				}
			}
		}

		$track1 = explode("^", $tracks[$index_adj]);
		$cn_start = 0;
		if (strstr($track1[0], "%"))
		{
			$cn_start = strpos($track1[0], "%");
		}
		$cn_start = position_of_first_number($track1[0], $cn_start);
		$name_array = explode("/", $track1[1]);
		$card['name'] = trim(trim($name_array[1])." ".trim($name_array[0]));
		if (!$got_good_track_2)
		{
			$card['number'] = str_replace(array( " ", "%*", "%", "*;", ";", "*" ), array( "", "", "", "", "", "0" ), substr($track1[0], $cn_start));
			$card['exp_month'] = substr($track1[2], 2, 2);
			$card['exp_year'] = substr($track1[2], 0, 2);
		}

		if (!is_numeric($card['number']))
		{
			$card['number'] = "00000000";
		}

		if (!is_numeric($card['exp_month']))
		{
			$card['exp_month'] = "";
		}

		if (!is_numeric($card['exp_year']))
		{
			$card['exp_year'] = "";
		}

		return $card;
	}

function extract_value($curl_result, $tag)
{
	$return_string = "";

	if ((!strstr($curl_result, "<".$tag." />")) && (strstr($curl_result, "<".$tag.">")))
	{
		$start_tag = "<".$tag.">";
		$end_tag = "</".$tag.">";

		$start_pos = (strpos($curl_result, $start_tag) + strlen($start_tag));
		$end_pos = strpos($curl_result, $end_tag);

		$return_string = substr($curl_result, $start_pos, ((strlen($curl_result) - $start_pos)) - (strlen($curl_result) - $end_pos));
	}

	return $return_string;
}

function GWFupdateSignatureForNorwayActionLog()
{
	// LP-1171
	require_once(__DIR__."/jcvs/jc_func8.php");
	$currentActionLogId = lavu_insert_id();
	updateSignatureForNorwayActionLog($currentActionLogId);
}

function create_new_order_record($process_info, $location_info)
{
	global $server_order_prefix;

	$o_vars = array();
	$o_vars['ioid'] = $process_info['ioid'];
	$o_vars['opened'] = $process_info['device_time'];
	$o_vars['server'] = $process_info['server_name'];
	$o_vars['server_id'] = $process_info['server_id'];
	$o_vars['tablename'] = $process_info['more_info'];
	$o_vars['send_status'] = "0";
	$o_vars['guests'] = "1";
	$o_vars['no_of_checks'] = "1";
	$o_vars['tab'] = "1";
	$o_vars['register'] = $process_info['register'];
	$o_vars['register_name'] = $process_info['register_name'];
	$o_vars['location'] = $location_info['title'];
	$o_vars['location_id'] = $process_info['loc_id'];
	$o_vars['closed'] = "0000-00-00 00:00:00";
	$o_vars['opening_device'] = $process_info['device_udid'];
	if ($process_info['transtype'] == "AuthForTab")
	{
		$o_vars['tabname'] = $process_info['more_info'];
	}

	$fields = "";
	$values = "";

	buildInsertFieldsAndValues($o_vars, $fields, $values);

	$device_prefix = "";
	if ($location_info['use_device_prefix'] && $location_info['use_device_prefix']=="1")
	{
		$device_prefix = getDevicePrefix();
	}
	$o_vars['order_id'] = assignNextWithPrefix("order_id", "orders", "location_id", $process_info['loc_id'], $server_order_prefix.$device_prefix."-");

	$save_new_order = lavu_query("INSERT INTO `orders` (`order_id`, ".$fields.") VALUES ('[order_id]', ".$values.")", $o_vars);

	$l_vars = array();
	$l_vars['action'] = "Order Opened (Tab PreAuth)";
	$l_vars['loc_id'] = $location_info['id'];
	$l_vars['order_id'] = $o_vars['order_id'];
	$l_vars['server_time'] = UTCDateTime(TRUE);
	$l_vars['time'] = $process_info['device_time'];
	$l_vars['user'] = $process_info['server_name'];
	$l_vars['user_id'] = $process_info['server_id'];
	$l_vars['details'] = $process_info['more_info'];
	$l_vars['device_udid'] = $process_info['device_udid'];

	$fields = "";
	$values = "";

	buildInsertFieldsAndValues($l_vars, $fields, $values);

	$log_this = lavu_query("INSERT INTO `action_log` (".$fields.") VALUES (".$values.")", $l_vars);
	// START LP-1171
	include_once('jcvs/jc_func8.php');
	$currentActionLogId = lavu_insert_id();
	updateSignatureForNorwayActionLog($currentActionLogId);
	// END  LP-1171

	GWFupdateSignatureForNorwayActionLog();	// LP-1171

	return $o_vars['order_id'];
}

function actionLogItGW($process_info, $action, $details)
{
	$app_name		= reqvar("app_name", "");
	$app_version	= reqvar("app_version", "");
	$app_build		= reqvar("app_build", "");
	$host			= $_SERVER['HTTP_HOST'];

	$ioid = reqvar("ioid", "");;
	if (empty($ioid) && isset($_REQUEST['ioid']))
	{
		$ioid = $_REQUEST['ioid'];
	}

	$details_short = array();
	if (!empty($app_name))
	{
		$details_short[] = $app_name;
	}
	if (!empty($app_version))
	{
		$details_short[] = $app_version;
	}
	if (!empty($app_build))
	{
		$details_short[] = "(".$app_build.")";
	}
	if (!empty($host))
	{
		$details_short[] = "via ".$host;
	}

	$l_vars = array(
		'action'			=> $action,
		'check'			=> $process_info['check'],
		'details'		=> $details,
		'details_short'	=> implode(" ", $details_short),
		'device_udid'	=> $process_info['device_udid'],
		'ialid'			=> newInternalID(),
		'ioid'			=> $ioid,
		'loc_id'			=> $process_info['loc_id'],
		'order_id'		=> $process_info['order_id'],
		'server_time'	=> UTCDateTime(TRUE),
		'time'			=> $process_info['device_time'],
		'user'			=> $process_info['server_name'],
		'user_id'		=> $process_info['server_id']
	);

	$fields = "";
	$values = "";

	buildInsertFieldsAndValues($l_vars, $fields, $values);

	$log_this = lavu_query("INSERT INTO `action_log` (".$fields.") VALUES (".$values.")", $l_vars);

	GWFupdateSignatureForNorwayActionLog();	// LP-1171
}

function update_tables($process_info, $location_info, $card_type, $new_pnref, $authcode, $system_id, $split_tender_id, $temp_data, $ref_data, $process_data, $record_no)
{
	$data_name = $process_info['data_name'];
	$transtype = $process_info['transtype'];

	$timezone = locationSetting("timezone");
	if (!empty($timezone))
	{
		$process_info['device_time'] = DateTimeForTimeZone($timezone);
	}

	$transaction_vars = array(
		'auth_code'			=> $authcode,
		'card_type'			=> $card_type,
		'datetime'			=> $process_info['device_time'],
		'process_data'		=> $process_data,
		'record_no'			=> $record_no,
		'ref_data'			=> $ref_data,
		'split_tender_id'	=> $split_tender_id,
		'temp_data'			=> $temp_data,
		'transaction_id'	=> $new_pnref
	);

	if (!empty($process_info['card_amount']))
	{
		$transaction_vars = array_merge( $transaction_vars, array(
			'amount'			=> $process_info['card_amount'],
			'total_collected'	=> $process_info['card_amount']
		));
	}

	if (!empty($process_info['exp'])) {
		$transaction_vars['exp'] = $process_info['exp'];
	}

	$cc_transactions_update = "";

	buildUpdate($transaction_vars, $cc_transactions_update);

	$transaction_vars = array_merge( $transaction_vars, array(
		'info'			=> $process_info['info'],
		'last_mod_ts'	=> time(),
		'location_id'	=> $process_info['loc_id'],
		'more_info'		=> $process_info['more_info'],
		'server_id'		=> $process_info['server_id'],
		'system_id'		=> $system_id,
		'order_id'		=> $process_info['order_id'],
		'pnref'			=> isset($process_info['pnref']) ? $process_info['pnref'] : ''
	));

	$process_info['last_mod_ts'] = $transaction_vars['last_mod_ts'];

	$check_detail_vars = array(
		'card'		=> $process_info['card_amount'],
		'check'		=> $process_info['check'],
		'ioid'		=> $process_info['ioid'],
		'loc_id'	=> $process_info['loc_id'],
		'order_id'	=> $process_info['order_id']
	);

	$ptid = $process_info['set_pay_type_id'];

	$alt_pay_detail_vars = array(
		'amount'		=> $process_info['card_amount'],
		'check'			=> $process_info['check'],
		'ioid'			=> $process_info['ioid'],
		'loc_id'		=> $process_info['loc_id'],
		'order_id'		=> $process_info['order_id'],
		'pay_type_id'	=> $ptid,
		'tip'			=> ($transtype == "Return")?$process_info['refund_tip_amount']:$process_info['tip_amount'],
		'type'			=> $process_info['set_pay_type']
	);

	usleep(10);

	$o_key = "card_paid";
	$or_key = "refunded_cc";
	$c_key = "card";
	$cr_key = "refund_cc";
	if ((int)$ptid != 2)
	{
		$o_key = "alt_paid";
		$or_key = "alt_refunded";
		$c_key = "alt_paid";
		$cr_key = "alt_refunded";
	}

	if (in_array($transtype, array( "Sale", "Auth", "AuthForTab", "VoiceAuth" )))
	{
		$auth = 0;
		$save_preauth_id = ", `preauth_id` = '[transaction_id]'";
		if ($transtype == "Auth")
		{
			$auth = 1;
		}
		if ($transtype == "AuthForTab")
		{
			$auth = 2;
		}

		$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET ".$cc_transactions_update.", `auth` = '$auth', `preauth_id` = '[transaction_id]', `voice_auth` = '0', `action` = 'Sale', `got_response` = '1', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[system_id]'", $transaction_vars);

		$update_order = lavu_query("UPDATE `orders` SET `".$o_key."` = (`".$o_key."` + [card_amount]), `void` = '0', `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
		$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $check_detail_vars);
		if (mysqli_num_rows($check_for_details) > 0)
		{
			$update_details = lavu_query("UPDATE `split_check_details` SET `".$c_key."` = (`".$c_key."` + [card]) WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $check_detail_vars);
		}
		else
		{
			$create_details = lavu_query("INSERT INTO `split_check_details` (`ioid`, `loc_id`, `order_id`, `check`, `".$c_key."`) VALUES ('[ioid]', '[loc_id]', '[order_id]', '[check]', '[card]')", $check_detail_vars);
		}

		if ($ptid != "2")
		{
			$check_for_alt_pay_details = lavu_query("SELECT `id` FROM `alternate_payment_totals` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `pay_type_id` = '[pay_type_id]' LIMIT 1", $alt_pay_detail_vars);
			if (mysqli_num_rows($check_for_alt_pay_details) > 0)
			{
				$alt_pay_info = mysqli_fetch_assoc($check_for_alt_pay_details);
				$alt_pay_detail_vars['row_id'] = $alt_pay_info['id'];
				$update_details = lavu_query("UPDATE `alternate_payment_totals` SET `amount` = (`amount` + [amount]), `tip` = (`tip` + [tip]) WHERE `id` = '[row_id]'", $alt_pay_detail_vars);
			}
			else
			{
				$create_details = lavu_query("INSERT INTO `alternate_payment_totals` (`loc_id`, `order_id`, `check`, `type`, `pay_type_id`, `amount`, `tip`, `ioid`) VALUES ('[loc_id]', '[order_id]', '[check]', '[type]', '[pay_type_id]', '[amount]', '[tip]', '[ioid]')", $alt_pay_detail_vars);
			}
		}

		actionLogItGW($process_info, ($transtype == "AuthForTab")?"Tab PreAuth":"Payment Applied", "Check ".$process_info['check']." - ".$process_info['set_pay_type']." - ".priceFormatGW($process_info['card_amount']));
	}

	if ($transtype == "PreAuthCaptureForTab")
	{
		$get_transaction_info = lavu_query("SELECT `id`, `amount` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `preauth_id` = '[pnref]'", $process_info);
		$transaction_info = mysqli_fetch_assoc($get_transaction_info);

		$diff = ($process_info['card_amount'] - $transaction_info['amount']);
		$transaction_vars['id'] = $transaction_info['id'];
		$transaction_vars['original_auth'] = "Original auth amount: ".$transaction_info['amount'];
		$transaction_vars['tip_amount'] = $process_info['tip_amount'];

		$update_transaction = lavu_query("UPDATE `cc_transactions` SET `amount` = '[amount]', `total_collected` = '[amount]', `tip_amount` = '[tip_amount]', `transaction_id` = '[transaction_id]', `ref_data` = '[ref_data]', `process_data` = '[process_data]', `more_info` = '[original_auth]', `auth` = '0', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[id]'", $transaction_vars);
		$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` + ".($diff * 1)."), `card_gratuity` = (`card_gratuity` + [tip_amount]), `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
		$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $check_detail_vars);
		if (mysqli_num_rows($check_for_details) > 0)
		{
			$check_detail_vars['card'] = $diff;
			$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` + [card]) WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $check_detail_vars);
		}
		else
		{
			$create_details = lavu_query("INSERT INTO `split_check_details` (`ioid`, `loc_id`, `order_id`, `check`, `card`) VALUES ('[ioid]', '[loc_id]', '[order_id]', '[check]', '[card]')", $check_detail_vars);
		}

		actionLogItGW($process_info, "Tab PreAuth Captured", "Check ".$process_info['check']." - ".priceFormatGW($process_info['card_amount'])." (".priceFormatGW($transaction_info['amount'])." Pre-auth)");
	}

	if ($transtype=="GiftCardSale" || $transtype=="LoyaltyCardSale")
	{
		$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET ".$cc_transactions_update.", `voice_auth` = '0', `action` = 'Sale', `got_response` = '1', `more_info` = '[more_info]', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[system_id]'", $transaction_vars);
		$update_order = lavu_query("UPDATE `orders` SET `alt_paid` = (`alt_paid` + [card_amount]), `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
		$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $check_detail_vars);
		if (mysqli_num_rows($check_for_details) > 0)
		{
			$update_details = lavu_query("UPDATE `split_check_details` SET `alt_paid` = (`alt_paid` + [card]) WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $check_detail_vars);
		}
		else
		{
			$create_details = lavu_query("INSERT INTO `split_check_details` (`ioid`, `loc_id`, `order_id`, `check`, `alt_paid`) VALUES ('[ioid]', '[loc_id]', '[order_id]', '[check]', '[card]')", $check_detail_vars);
		}

		$check_for_alt_pay_details = lavu_query("SELECT `id` FROM `alternate_payment_totals` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `pay_type_id` = '[pay_type_id]' LIMIT 1", $alt_pay_detail_vars);
		if (mysqli_num_rows($check_for_alt_pay_details) > 0)
		{
			$alt_pay_info = mysqli_fetch_assoc($check_for_alt_pay_details);
			$alt_pay_detail_vars['row_id'] = $alt_pay_info['id'];
			$update_details = lavu_query("UPDATE `alternate_payment_totals` SET `amount` = (`amount` + [amount]) WHERE `id` = '[row_id]'", $alt_pay_detail_vars);
		}
		else
		{
			$create_details = lavu_query("INSERT INTO `alternate_payment_totals` (`loc_id`, `order_id`, `check`, `type`, `pay_type_id`, `amount`, `ioid`) VALUES ('[loc_id]', '[order_id]', '[check]', '[type]', '[pay_type_id]', '[amount]', '[ioid]')", $alt_pay_detail_vars);
		}

		$log_amount = ($transtype == "GiftCardSale")?priceFormatGW($process_info['card_amount']):$process_info['card_amount'];
		actionLogItGW($process_info, "Payment Applied", "Check ".$process_info['check']." - ".$process_info['set_pay_type']." - ".$log_amount);
	}

	if ($transtype=="Void" || $transtype=="VoidPreAuthCapture")
	{
		$update_void_transaction = lavu_query("UPDATE `cc_transactions` SET `transaction_id` = '[transaction_id]', `ref_data` = '[ref_data]', `process_data` = '[process_data]', `auth_code` = '[auth_code]', `action` = 'Void', `void_notes` = '[more_info]', `got_response` = '1', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[system_id]'", $transaction_vars);
		if ($transtype == "Void")
		{
			$process_info['void_system_id'] = $transaction_vars['system_id'];
			$get_transaction_info = lavu_query("SELECT `id`, `check`, `action`, `amount`, `tip_amount`, `pay_type` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `id` != '[void_system_id]'", $process_info);
			$transaction_info = mysqli_fetch_assoc($get_transaction_info);
			$t_action = $transaction_info['action'];

			$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = '[3]', `last_mod_ts` = '[4]' WHERE `id` = '[5]'", $process_info['more_info'], $process_info['server_id'], $new_pnref, time(), $transaction_info['id']);
			if ($ptid != "2") {
				$check_for_alt_pay_details = lavu_query("SELECT `id` FROM `alternate_payment_totals` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `pay_type_id` = '[pay_type_id]' LIMIT 1", $alt_pay_detail_vars);
				if (@mysqli_num_rows($check_for_alt_pay_details) > 0) {
					$alt_pay_info = mysqli_fetch_assoc($check_for_alt_pay_details);
					$alt_pay_detail_vars['row_id'] = $alt_pay_info['id'];
					$changes = ($t_action == "Sale")?"`amount` = (`amount` - [amount]), `tip` = (`tip` - [tip])":"`amount` = (`amount` + [amount]), `tip` = (`tip` + [tip]), `refund_amount` = (`refund_amount` - [amount])";
					$update_details = lavu_query("UPDATE `alternate_payment_totals` SET ".$changes." WHERE `id` = '[row_id]'", $alt_pay_detail_vars);
				}
			}

			$adjust_card_gratuity = "";
			if ($t_action == "Sale")
			{
				if ($ptid == "2")
				{
					$adjust_card_gratuity = ", `card_gratuity` = (`card_gratuity` - ".($transaction_info['tip_amount'] * 1).")";
				}
				$update_order = lavu_query("UPDATE `orders` SET `".$o_key."` = (`".$o_key."` - ".($transaction_info['amount'] * 1)."), `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]'".$adjust_card_gratuity." WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
				$update_details = lavu_query("UPDATE `split_check_details` SET `".$c_key."` = (`".$c_key."` - ".($transaction_info['amount'] * 1).") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $process_info['loc_id'], $process_info['order_id'], $transaction_info['check']);
			}
			else if ($t_action == "Refund")
			{
				if ($ptid == "2")
				{
					$adjust_card_gratuity = ", `card_gratuity` = (`card_gratuity` + ".($transaction_info['tip_amount'] * 1).")";
				}
				$update_order = lavu_query("UPDATE `orders` SET `".$o_key."` = (`".$o_key."` + ".$transaction_info['amount']."), `".$or_key."` = (`".$or_key."` - ".$transaction_info['amount']."), `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]'".$adjust_card_gratuity." WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
				$update_details = lavu_query("UPDATE `split_check_details` SET `".$c_key."` = (`".$c_key."` + ".$transaction_info['amount']."), `".$cr_key."` = (`".$cr_key."` - ".$transaction_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $process_info['loc_id'], $process_info['order_id'], $transaction_info['check']);
			}

			actionLogItGW($process_info, (($t_action == "Sale")?"Payment":"Refund")." Voided", "Check ".$process_info['check']." - ".$transaction_info['pay_type']." - ".priceFormatGW($transaction_info['amount']));
		}
	}

	if ($transtype=="GiftCardVoid" || $transtype=="LoyaltyCardVoid")
	{
		$update_void_transaction = lavu_query("UPDATE `cc_transactions` SET `transaction_id` = '[transaction_id]', `ref_data` = '[ref_data]', `process_data` = '[process_data]', `auth_code` = '[auth_code]', `action` = 'Void', `void_notes` = '[more_info]', `got_response` = '1', `more_info` = '[info]', `card_type` = '[card_type]', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[system_id]'", $transaction_vars);
		$process_info['void_system_id'] = $transaction_vars['system_id'];
		$get_transaction_info = lavu_query("SELECT `id`, `check`, `action`, `amount`, `pay_type` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]' AND `id` != '[void_system_id]' ORDER BY `id` DESC", $process_info);
		$transaction_info = mysqli_fetch_assoc($get_transaction_info);
		$t_action = $transaction_info['action'];
		$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = '[3]', `last_mod_ts` = '[4]' WHERE `id` = '[5]'", $process_info['more_info'], $process_info['server_id'], $new_pnref, time(), $transaction_info['id']);

		if ($t_action == "Sale")
		{
			$update_order = lavu_query("UPDATE `orders` SET `alt_paid` = (`alt_paid` - ".$transaction_info['amount']."), `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
			$update_details = lavu_query("UPDATE `split_check_details` SET `alt_paid` = (`alt_paid` - ".$transaction_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $process_info['loc_id'], $process_info['order_id'], $transaction_info['check']);
			$alt_pay_field = "amount";
		}
		else if ($t_action == "Refund")
		{
			$update_order = lavu_query("UPDATE `orders` SET `alt_paid` = (`alt_paid` + ".$transaction_info['amount']."), `alt_refunded` = (`alt_refunded` - ".$transaction_info['amount']."), `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
			$update_details = lavu_query("UPDATE `split_check_details` SET `alt_paid` = (`alt_paid` + ".$transaction_info['amount']."), `alt_refunded` = (`alt_refunded` - ".$transaction_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $process_info['loc_id'], $process_info['order_id'], $transaction_info['check']);
			$alt_pay_field = "refund_amount";
		}

		$log_amount = ($transtype == "GiftCardVoid")?priceFormatGW($transaction_info['amount']):$transaction_info['amount'];
		actionLogItGW($process_info, (($t_action == "Sale")?"Payment":"Refund")." Voided", "Check ".$process_info['check']." - ".$transaction_info['pay_type']." - ".$log_amount);

		$check_for_alt_pay_details = lavu_query("SELECT `id` FROM `alternate_payment_totals` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `pay_type_id` = '[pay_type_id]' LIMIT 1", $alt_pay_detail_vars);
		if (mysqli_num_rows($check_for_alt_pay_details) > 0)
		{
			$alt_pay_info = mysqli_fetch_assoc($check_for_alt_pay_details);
			$alt_pay_detail_vars['row_id'] = $alt_pay_info['id'];
			$alt_pay_detail_vars['amount'] = $transaction_info['amount'];
			$update_details = lavu_query("UPDATE `alternate_payment_totals` SET `$alt_pay_field` = (`$alt_pay_field` - [amount]) WHERE `id` = '[row_id]'", $alt_pay_detail_vars);
		}
	}

	if ($transtype=="VoidGiftCardIssueOrReload" || $transtype=="VoidLoyaltyCardIssueOrReload")
	{
        $update_record = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[more_info]', `voided_by` = '[server_id]', `void_pnref` = '[pnref]', `last_mod_ts` = '[last_mod_ts]' WHERE `loc_id` = '[loc_id]' AND `transaction_id` = '[pnref]' AND `pay_type_id` = '[set_pay_type_id]'", $process_info);
		$get_item_record = lavu_query("SELECT `id`, `hidden_data6` FROM `order_contents` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `hidden_data6` LIKE '%::[pnref]'", $process_info);
		if (mysqli_num_rows($get_item_record) > 0)
		{
			$info = mysqli_fetch_assoc($get_item_record);
			$hd6 = explode("::", $info['hidden_data6']);
			$update_record = lavu_query("UPDATE `order_contents` SET `hidden_data6` = '[1]' WHERE `id` = '[2]'", $hd6[0]."::".$hd6[1]."::Voided", $info['id']);
		}
	}

	if ($transtype == "VoidAuth")
	{
		$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = 'Cancelled Auth', `voided_by` = '[server_id]', `void_pnref` = '[transaction_id]', `last_mod_ts` = '[last_mod_ts]' WHERE `loc_id` = '[location_id]' AND `order_id` = '[order_id]' AND `transaction_id` = '[pnref]'", $transaction_vars);
	}

	if ($transtype == "AddTip")
	{
		$get_original_id = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `transaction_id` = '[pnref]'", $process_info);
		$original_info = mysqli_fetch_assoc($get_original_id);
		$transaction_vars['tip_for_id'] = $original_info['id'];
		$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET `action` = 'Sale', `processed` = '1', `tip_for_id` = '[tip_for_id]', `transaction_id` = '[transaction_id]', `auth_code` = '[auth_code]', `card_type` = '[card_type]', `got_response` = '1', `split_tender_id` = '[split_tender_id]', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[system_id]'", $transaction_vars);
	}

	if ($transtype == "Return")
	{
		$update_return_transaction = lavu_query("UPDATE `cc_transactions` SET ".$cc_transactions_update.", `action` = 'Refund', `got_response` = '1', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[system_id]'", $transaction_vars);

		$adjust_card_gratuity = ($ptid == "2")?", `card_gratuity` = (`card_gratuity` - [refund_tip_amount])":"";
		$update_order = lavu_query("UPDATE `orders` SET `".$o_key."` = (`".$o_key."` - [card_amount])".$adjust_card_gratuity.", `".$or_key."` = (`".$or_key."` + [card_amount]), `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
		$update_details = lavu_query("UPDATE `split_check_details` SET `".$c_key."` = (`".$c_key."` - [card]), `".$cr_key."` = (`".$cr_key."` + [card]) WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $check_detail_vars);
		if ($ptid != "2")
		{
			$check_for_alt_pay_details = lavu_query("SELECT `id` FROM `alternate_payment_totals` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `pay_type_id` = '[pay_type_id]' LIMIT 1", $alt_pay_detail_vars);
			if (mysqli_num_rows($check_for_alt_pay_details) > 0)
			{
				$alt_pay_info = mysqli_fetch_assoc($check_for_alt_pay_details);
				$alt_pay_detail_vars['row_id'] = $alt_pay_info['id'];
				$update_details = lavu_query("UPDATE `alternate_payment_totals` SET `amount` = (`amount` - [amount]), `tip` = (`tip` - [tip]), `refund_amount` = (`refund_amount` + [amount]) WHERE `id` = '[row_id]'", $alt_pay_detail_vars);
			}
			else
			{
				$create_details = lavu_query("INSERT INTO `alternate_payment_totals` (`loc_id`, `order_id`, `check`, `type`, `pay_type_id`, `refund_amount`, `ioid`) VALUES ('[loc_id]', '[order_id]', '[check]', '[type]', '[pay_type_id]', '[amount]', '[ioid]')", $alt_pay_detail_vars);
			}
		}

		actionLogItGW($process_info, "Refund Applied", "Check ".$process_info['check']." - ".$process_info['set_pay_type']." - ".priceFormatGW($process_info['card_amount']));
	}

	if ($transtype=="GiftCardReturn" || $transtype=="LoyaltyCardReturn")
	{
		$update_return_transaction = lavu_query("UPDATE `cc_transactions` SET ".$cc_transactions_update.", `action` = 'Refund', `got_response` = '1', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[system_id]'", $transaction_vars);
		$update_order = lavu_query("UPDATE `orders` SET `alt_paid` = (`alt_paid` - [card_amount]), `alt_refunded` = (`alt_refunded` + [card_amount]), `last_modified` = '[device_time]', `last_mod_device` = '[device_udid]', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $process_info);
		$update_details = lavu_query("UPDATE `split_check_details` SET `alt_paid` = (`alt_paid` - [card]), `alt_refunded` = (`alt_refunded` + [card]) WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $check_detail_vars);
		$check_for_alt_pay_details = lavu_query("SELECT `id` FROM `alternate_payment_totals` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `pay_type_id` = '[pay_type_id]' LIMIT 1", $alt_pay_detail_vars);
		if (mysqli_num_rows($check_for_alt_pay_details) > 0)
		{
			$alt_pay_info = mysqli_fetch_assoc($check_for_alt_pay_details);
			$alt_pay_detail_vars['row_id'] = $alt_pay_info['id'];
			$update_details = lavu_query("UPDATE `alternate_payment_totals` SET `amount` = (`amount` - [amount]), `refund_amount` = (`refund_amount` + [amount]) WHERE `id` = '[row_id]'", $alt_pay_detail_vars);
		}
		else
		{
			$create_details = lavu_query("INSERT INTO `alternate_payment_totals` (`loc_id`, `order_id`, `check`, `type`, `pay_type_id`, `refund_amount`, `ioid`) VALUES ('[loc_id]', '[order_id]', '[check]', '[type]', '[pay_type_id]', '[amount]', '[ioid]')", $alt_pay_detail_vars);
		}

		$log_amount = ($transtype == "GiftCardReturn")?priceFormatGW($process_info['card_amount']):$process_info['card_amount'];
		actionLogItGW($process_info, "Refund Applied", "Check ".$process_info['check']." - ".$process_info['set_pay_type']." - ".priceFormatGW($process_info['card_amount']));
	}

	if (in_array($transtype, array( "IssueGiftCard", "ReloadGiftCard", "IssueLoyaltyCard", "ReloadLoyaltyCard" )))
	{
		$transaction_vars['transtype'] = $transtype;
		$transaction_vars['icid'] = (isset($_REQUEST['icid']))?$_REQUEST['icid']:"";
		$update_transaction = lavu_query("UPDATE `cc_transactions` SET ".$cc_transactions_update.", `action` = '[transtype]', `got_response` = '1', `info` = '[icid]', `more_info` = '[more_info]', `last_mod_ts` = '[last_mod_ts]' WHERE `id` = '[system_id]'", $transaction_vars);
	}
}

function checkForPreviousIssueOrReload($process_info)
{
	//error_log("checkForPreviousIssueOrReload");

	$rtn_info = "";

	$icid = (isset($_REQUEST['icid']))?$_REQUEST['icid']:"";
	if (!empty($icid))
	{
		$check_transactions = lavu_query("SELECT `transaction_id`, `card_desc`, `auth_code`, `amount`, `more_info` FROM `cc_transactions` WHERE `got_response` = '1' AND `voided` = '0' AND `order_id` = '[1]' AND (`transtype` LIKE 'Issue%' OR `transtype` LIKE 'Reload%') AND `info` = '[2]' ORDER BY `id` DESC LIMIT 1", $process_info['order_id'], $icid);
		if ($check_transactions)
		{
			if (mysqli_num_rows($check_transactions))
			{
				$info = mysqli_fetch_assoc($check_transactions);
				//error_log("found info: ".print_r($info, true));
				$rtn_info = "1|".$info['transaction_id']."|".$info['card_desc']."|Approved|".$info['auth_code']."|||".$info['amount']."|".$process_info['order_id']."|||".$info['more_info'];
			}
		}
	}

	return $rtn_info;
}

function record_batch($data_name, $batch_vars)
{
	$q_fields = "";
	$q_values = "";
	$keys = array_keys($batch_vars);
	foreach ($keys as $key)
	{
		if ($q_fields != "")
		{
			$q_fields .= " ,";
		}
		$q_fields .= "`$key`";
		if ($q_values != "")
		{
			$q_values .= " ,";
		}
		$q_values .= "'[".$key."]'";
	}

	$save_batch = lavu_query("INSERT INTO `cc_batches` (".$q_fields.") VALUES (".$q_values.")", $batch_vars);
}

function process_capture_or_tip($location_info, $integration_info, $process_info, $transaction_info, $apply_tip, $overpaid_CCs, $allow_reset_base_amount)
{
	global $poslavu_version;
	global $resp;
	global $error_encountered;
	global $error_code;
	global $gatewayMsg;
	global $responseMsg;
	$use_gateway = $transaction_info['gateway']; // THIS IS ADDED TO TAKE THE GATEWAY FROM CCTRANSACTIONS TABLE RATHER THEN LOCATIONS TABLE
	if (empty($use_gateway))
	{
		$use_gateway = $integration_info['gateway']; // Fall back on location setting if `gateway` was not written to the transaction record
	}

	$transactionController = new Transaction_Controller($use_gateway, $process_info);

	$process_info['username']		= $integration_info['integration1'];
	$process_info['password']		= $integration_info['integration2'];
	$process_info['integration3']	= $integration_info['integration3'];
	$process_info['integration4']	= $integration_info['integration4'];
	$process_info['integration5']	= $integration_info['integration5'];
	$process_info['integration6']	= $integration_info['integration6'];
	$process_info['integration7']	= $integration_info['integration7'];
	$process_info['integration8']	= $integration_info['integration8'];
	$process_info['integration9']	= $integration_info['integration9'];
	$process_info['integration10']	= $integration_info['integration10'];
	$process_info['ioid']			= $transaction_info['ioid'];
	$process_info['order_id']		= $transaction_info['order_id'];
	$process_info['check']			= $transaction_info['check'];
	$process_info['pnref']			= $transaction_info['transaction_id'];
	$process_info['record_no']		= $transaction_info['record_no'];
	$process_info['ref_data']		= $transaction_info['ref_data'];
	$process_info['process_data']	= $transaction_info['process_data'];

	$return_tip_amount = 0;

	if (in_array($use_gateway, array("Authorize.net", "BluePay", "CyberSource", "Elavon")) || ($use_gateway=="Magensa" && $transaction_info['transtype']=="Auth"))
	{
		$update_tip_amount = lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '$apply_tip', `last_mod_ts` = '[1]' WHERE `id` = '[2]'", time(), $transaction_info['id']);
		$return_tip_amount = $apply_tip;
	}
	else if ($use_gateway=="Magensa" && $transaction_info['transtype']!="Auth")
	{
		$error_encountered = true;
		$resp .= "<tr><td align='center'>Order #".$transaction_info['order_id']." - ".$transaction_info['card_type']." ...".$transaction_info['card_desc']." - Tip: $apply_tip - Cannot apply tip to Sale transaction (Set default transaction type to Auth)</td></tr>";
		$gatewayMsg = "Cannot apply tip to Sale transaction (Set default transaction type to Auth)";
		$responseMsg = $gatewayMsg;
		$set_cardtip = 0;
	}
	else
	{
		$transtype = "Adjustment";
		if ($transaction_info['auth'] == 1)
		{
			$transtype = "PreAuthCapture";
		}

		$good_to_go = true;
		if ($transaction_info['preauth_id']!="" && $transaction_info['preauth_id']!=$transaction_info['transaction_id'] && !in_array($use_gateway, array("eConduit", "Heartland", "Mercury", "PayPal", "")))
		{
			$void_notes = "Voided PreAuthCapture to readjust tip amount for transaction ".$transaction_info['id']. " (".$transaction_info['preauth_id'].")";

			$process_info['more_info']		= $void_notes;
			$process_info['transtype']		= "VoidPreAuthCapture";
			$process_info['card_amount']	= $transaction_info['amount'];

			$should_break = false;
			$response = "";
			switch ($use_gateway)
			{
				case "EVOSnap" :
					break;
				case "MerchantWARE" :
					break;
				case "RedFin" :
					$process_info['ext_data'] = "";
					break;
				case "TGate" :
					$process_info['ext_data'] = "";
					break;
				case "LavuPay":
					break;
				default :
					$response = "0|Error: Unrecognized Payment Gateway (".$use_gateway.")";
					$should_break = true;
					break;
			}

			if (!$should_break)
			{
				$transactionController->process_transaction($process_info, $location_info);
				$response = $transactionController->getResponse();
			}

			if (substr($response, 0, 2) != "1|")
			{
				$good_to_go = false;
			}
			else
			{
				$update_id = lavu_query("UPDATE `cc_transactions` SET `auth` = '1', `voided` = '0', `void_notes` = '', `transaction_id` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $transaction_info['preauth_id'], time(), $transaction_info['id']);
			}

			$transtype = "PreAuthCapture";
			$transaction_info['transaction_id'] = $transaction_info['preauth_id'];
			$process_info['pnref'] = $transaction_info['preauth_id'];
		}

		if ($good_to_go)
		{
			$set_base_amount = $transaction_info['amount'];
			$set_card_amount = "";
			if ($allow_reset_base_amount && ($transtype == "PreAuthCapture"))
			{
				$set_card_amount = $transaction_info['amount'];
				if (isset($overpaid_CCs[$transaction_info['ioid']]))
				{
					$set_base_amount = number_format(((float)$transaction_info['amount'] - (float)$overpaid_CCs[$transaction_info['ioid']]), $location_info['disable_decimal'], ".", "");
					$set_card_amount = $set_base_amount;
				}
			}

			$process_info['base_amount']	= $set_base_amount;
			$process_info['capture_amount']	= $set_base_amount;
			$process_info['card_amount']	= $set_card_amount;
			$process_info['transtype']		= $transtype;
			$process_info['more_info']		= $apply_tip;
			$process_info['authcode']		= $transaction_info['auth_code'];
			$process_info['tip_amount']		= $apply_tip;

			$response = "";

			switch ($use_gateway)
			{
				case "eConduit" :
					// LP-3759
					break;
				case "EVOSnap" :
					$process_info['transtype'] = "PreAuthCapture";
					break;
				case "Heartland" :
					break;
				case "MerchantWARE" :
					break;
				case "Mercury" :
					if ($transaction_info['mpshc_pid'] != "")
					{
						$response = mpshc_add_tip($transtype, $integration_info['integration3'], $integration_info['integration4'], $process_info['data_name'], $location_info['id'], $transaction_info['order_id'], $transaction_info['amount'], $apply_tip, $transaction_info['transaction_id'], $transaction_info['auth_code'], $transaction_info['record_no'], $poslavu_version, "Back End", $process_info['server_id'], $transaction_info['ref_data'], $transaction_info['card_type'], $transaction_info['card_desc']);
					}
					break;
				case "RedFin" :
					$process_info['ext_data'] = "<TipAmt>".$apply_tip."</TipAmt>";
					break;
				case "TGate" :
					$process_info['ext_data'] = "<TipAmt>".$apply_tip."</TipAmt>";
					break;
				case "USAePay" :
					break;
				case "PayPal" :
					break;
				case "LavuPay":
					break;
				default :
					$error_code = 3;
					$response = "0|Unknown gateway setting (".$use_gateway.")";
					break;
			}

			if ($response == "")
			{
				$transactionController->process_transaction($process_info, $location_info);
				$response = $transactionController->getResponse();
			}
		}

		$response_parts = explode("|", $response);
		if ($response_parts[0] == "1")
		{
			$record_ref_no		= "";
			$update_processed	= "";

			if ($transtype == "PreAuthCapture")
			{
				$record_ref_no = ", `transaction_id` = '".$response_parts[1]."', `record_no` = '".$response_parts[6]."'";
				if (count($response_parts) > 9)
				{
					$record_ref_no .= ", `ref_data` = '".$response_parts[9]."'";
				}

				if (count($response_parts) > 10)
				{
					$record_ref_no .= ", `process_data` = '".rawurldecode($response_parts[10])."'";
				}

				if ($use_gateway == "eConduit")
				{
					$update_processed = "`processed` = '1', "; // LP-3759
				}
			}
			$reset_amount = "";
			if ($set_base_amount != "")
			{
				 $reset_amount = "`amount` = '".$set_base_amount."', `total_collected` = '".$set_base_amount."', ";
			}

			$update_transaction_record = lavu_query("UPDATE `cc_transactions` SET ".$reset_amount.$update_processed."`tip_amount` = '".$apply_tip."', `last_mod_ts` = '[1]', `auth` = '0'".$record_ref_no." WHERE `id` = '[2]'", time(), $transaction_info['id']);
			$return_tip_amount = $apply_tip;
			$respmsg = $response_parts[3];
		}
		else
		{
			$return_tip_amount = $transaction_info['tip_amount'];
			$error_encountered = true;
			$respmsg = $response_parts[1];
		}

		if ($respmsg == "")
		{
			 $respmsg = "Unknown Error";
		}

		$responseMsg = $respmsg;
		$resp .= "<tr><td align='center'>Order #".$transaction_info['order_id']." - ".$transaction_info['card_type']." ...".$transaction_info['card_desc']." - Tip: $apply_tip - $respmsg</td></tr>";
		$gatewayMsg = $respmsg;
	}

	return $return_tip_amount;
}

function useIFrameTipAdjust($gateway)
{
	switch ($gateway)
	{
		case "eConduit":

			return (locationSetting("iFrameTipAdjust") == "1");

		default;

			return FALSE;
	}
}

function disinterpret($data)
{
	$key_access = array("5", "2", "3", "6", "1");
	$text_string = $data;
	$key = array(0, rand(1,8), rand(1,8), rand(1,8), 0, rand(1,8), rand(1,8), rand(1,8));
	$return_number = "";
	for ($c = 0; $c < strlen($data); $c++)
	{
		$code = ord($data[$c]);
		$mult = $key[(int)$key_access[($c % 4)]];
		$return_number .= str_pad(($mult * $code), 4, "0", STR_PAD_LEFT);
	}

	return implode("", $key).$return_number;
}

function interpret($data)
{
	$key_access = (substr($data, 0, 1) == "0")?array("5", "2", "3", "6", "1"):array(0, 1, 3, 6);
	$ecodes = array();
	$dchars = array();
	$number_string = $data;
	$key = substr($number_string, 0, 8);
	$edata = substr($number_string, 8, (strlen($number_string) - 8));
	for ($g = 0; $g < floor(strlen($edata) / 4); $g++)
	{
		$dchars[] = chr(((int)substr($edata, ($g * 4), 4) / $key[(int)$key_access[($g % 4)]]));
	}
	$result = join("", $dchars);

	return $result;
}

function hex_string($string)
{
	$return_string = "";

	for ($c = 0; $c < strlen($string); $c++)
	{
		$return_string .= str_pad(dechex(ord($string[$c])), 2, "0", STR_PAD_LEFT);
	}

	return $return_string;
}

function hexToString($hex)
{
	$string = "";
	for ($i=0; $i < (strlen($hex) - 1); $i+=2)
	{
		$string .= chr(hexdec($hex[$i].$hex[($i + 1)]));
	}

	return $string;
}

function idtech_encrypted($mag_data, $process_info, $location_info, $force_blocklen=false)
{
	$debug = hex_string($mag_data);

	$encrypted_info = array();
	$encrypted_info['track1'] = "";
	$encrypted_info['track2'] = "";
	$encrypted_info['track3'] = "";
	$encrypted_info['hash1'] = "";
	$encrypted_info['hash2'] = "";
	$encrypted_info['hash3'] = "";

	$linea = ($process_info['reader'] == "linea");

	$pos = 0;
	$startpos = 0;
	if ($linea && $process_info['gateway']!="USAePay")
	{
		$pos = 2;
		$startpos = 2;
	}
	else
	{
		for ($c = 0; $c < 6; $c++)
		{
			if ($mag_data[$c] == chr(2))
			{
				$pos = $c;
				$startpos = $c;
				break;
			}
		}
	}

	$debug .= "\n\nStart: $startpos\n\n";

	$encrypted_info['sub_mag_data'] = substr($mag_data, $startpos);

	if (!$linea)
	{
		$pos += 3;
		$encoding = $mag_data[$pos++];
		$decodingStatus = $mag_data[$pos++];
	}
	$track1len = $mag_data[$pos++];
	$track2len = $mag_data[$pos++];
	$track3len = $mag_data[$pos++];

	if (!$linea)
	{
		$debug .= "encoding: ".hex_string($encoding)."\n";
		$debug .= "decodingStatus: ".hex_string($decodingStatus)."\n";
	}
	$debug .= "track1len: ".hex_string($track1len)." ".ord($track1len)."\n";
	$debug .= "track2len: ".hex_string($track2len)." ".ord($track2len)."\n";
	$debug .= "track3len: ".hex_string($track3len)." ".ord($track3len)."\n\n";

	$blocklen = 8;
	if (!$linea)
	{
		$mask_sent_status = decbin(ord($mag_data[$pos]));
		if (substr($mask_sent_status, 2, 2) == "01")
		{
			$blocklen = 16;
		}
	}
	if ($force_blocklen)
	{
		$blocklen = $force_blocklen;
	}
	$encryption_type = ($blocklen == 8)?"TDES":"AES";
	$debug .= "mask_sent_status: ".$mask_sent_status." (".$encryption_type.")\n";
	if (!$linea)
	{
		$hash_sent_status = decbin(ord($mag_data[($pos + 1)]));
		$debug .= "hash_sent_status: ".$hash_sent_status."\n";
		$pos += 2;
	}
	$debug .= "pos: ".$pos."\n\n";

	$track1len = ord($track1len);
	$track2len = ord($track2len);
	$track3len = ord($track3len);

	$maskedLen = ($track1len + $track2len + $track3len);

	if ($track1len % $blocklen)
	{
		$track1len += ($blocklen - ($track1len % $blocklen));
	}
	if ($track2len % $blocklen)
	{
		$track2len += ($blocklen - ($track2len % $blocklen));
	}
	if ($track3len % $blocklen)
	{
		$track3len += ($blocklen - ($track3len % $blocklen));
	}

	$debug .= "track1len (factor of $blocklen): ".$track1len."\n";
	$debug .= "track2len (factor of $blocklen): ".$track2len."\n";
	$debug .= "track3len (factor of $blocklen): ".$track3len."\n\n";

	$encryptedLen = ($track1len + $track2len);
	if (!$linea)
	{
		$encryptedLen += $track3len;
	}

	$debug .= "encryptedLen: ".$encryptedLen."\n\n";

	$maskedTracks = substr($mag_data, $pos, $maskedLen);
	$pos += $maskedLen;

	$debug .= "maskedTracks (hex): (len: ".strlen($maskedTracks).")\n".hex_string($maskedTracks)."\n\n";
	$debug .= "maskedTracks:\n".$maskedTracks."\n\n";

	//if ($encryptedLen % 8) $encryptedLen += (8 - ($encryptedLen % 8));

	$encryptedTracks = substr($mag_data, $pos, $encryptedLen);
	$pos += $encryptedLen;

	$debug .= "encryptedTracks:\n".hex_string($encryptedTracks)."\n\n";

	$encryptedTrack1 = substr($encryptedTracks, 0, $track1len);
	$encryptedTrack2 = substr($encryptedTracks, $track1len, $track2len);
	$encryptedTrack3 = substr($encryptedTracks, ($track1len + $track2len), $track3len);

	/*$t125pos = strpos($encryptedTrack2, "T125");
	if ($t125pos) {
		$debug .= "t123pos: ".$t125pos."\n\n";
		$encryptedTrack2 = substr($encryptedTrack2, 0, $t125pos);
	}*/

	$debug .= "encryptedTrack1:\n".hex_string($encryptedTrack1)."\n\n";
	$debug .= "encryptedTrack2:\n".hex_string($encryptedTrack2)."\n\n";
	$debug .= "encryptedTrack3:\n".hex_string($encryptedTrack3)."\n\n";

	if ($track1len > 0)
	{
		$encrypted_info['track'] = "1";
		$encrypted_info['data'] = $encryptedTrack1;
		$encrypted_info['track1'] = $encryptedTrack1;
		if (($linea || $hash_sent_status[4]=="1") && (strlen($mag_data) > $pos + 20))
		{
			$encrypted_info['hash1'] = substr($mag_data, $pos, 20);
			$pos += 20;
		}
	}

	$debug .= "track1Hash:\n".hex_string($encrypted_info['hash1'])."\n\n";

	if ($track2len > 0)
	{
		$encrypted_info['track'] = "2";
		$encrypted_info['data'] = $encryptedTrack2;
		$encrypted_info['track2'] = $encryptedTrack2;
		if (($linea || $hash_sent_status[3]=="1") && (strlen($mag_data) > $pos + 20))
		{
			$encrypted_info['hash2'] = substr($mag_data, $pos, 20);
			$pos += 20;
		}
	}

	$debug .= "track2Hash:\n".hex_string($encrypted_info['hash2'])."\n\n";

	if ($track3len > 0)
	{
		if (!$linea && $hash_sent_status[2]=="1" && (strlen($mag_data) > $pos + 20))
		{
			$encrypted_info['hash3'] = substr($mag_data, $pos, 20);
			$pos += 20;
		}
	}

	$debug .= "track3Hash:\n".hex_string($encrypted_info['hash3'])."\n\n";

	$ksn = "";
	if ($pos + 10 < strlen($mag_data))
	{
		$ksn = substr($mag_data, $pos, 10);
	}

	$debug .= "ksn:\n".hex_string($ksn)."\n\n";

	if ($linea)
	{
		$ksn = substr($mag_data, -10);
	}
	else
	{
		$ksn = substr($mag_data, -13, -3);
	}

	$debug .= "ksn:\n".hex_string($ksn)."\n\n";

	$encrypted_info['ksn'] = $ksn;

	//$debug .= "encrypted_info: ".print_r($encrypted_info, true)."\n\n";

	$debug .= "BASE 64:\n";
	$debug .= "encryptedTrack1: ".base64_encode($encrypted_info['track1'])."\n";
	$debug .= "encryptedTrack2: ".base64_encode($encrypted_info['track2'])."\n";
	$debug .= "encryptedTrack3: ".base64_encode($encrypted_info['track3'])."\n";
	$debug .= "track1Hash: ".base64_encode($encrypted_info['hash1'])."\n";
	$debug .= "track2Hash: ".base64_encode($encrypted_info['hash2'])."\n";
	$debug .= "track3Hash: ".base64_encode($encrypted_info['hash3'])."\n";
	$debug .= "ksn: ".base64_encode($encrypted_info['ksn'])."\n";

	if ($location_info['gateway_debug'] == "1")
	{
		gateway_debug($process_info['data_name'], $process_info['loc_id'], strtoupper($location_info['gateway']), $process_info['transtype'], "ENC", $debug);
	}

	return $encrypted_info;
}

function getCardTypeFromNumber($cn)
{
	$disc_array = array();
	$disc_array[] = 6011;
	for ($n = 622126; $n <= 622925; $n++)
	{
		$disc_array[] = $n;
	}
	for ($n = 644; $n <= 649; $n++)
	{
		$disc_array[] = $n;
	}
	$disc_array[] = 65;

	$jcb_array = array();
	for ($n = 3528; $n <= 3589; $n++)
	{
		$jcb_array[] = $n;
	}

	if ($cn != "")
	{
		if (numberBeginsWith($cn, array(34, 37)) && (strlen($cn) >= 15))
		{
			return 'AMEX'; // AMEX begins with 34 or 37, and length is 15. (allow for longer numbers for incorrect iDynamo/uDynamo mask length)
		}
		else if (numberBeginsWith($cn, array(51, 52, 53, 54, 55)) && strlen($cn) == 16)
		{
			return 'MasterCard'; // MasterCard beigins with 51-55, and length is 16.
		}
		else if (numberBeginsWith($cn, array(4)) && ((strlen($cn) == 13) || (strlen($cn) == 16)))
		{
			return 'Visa'; // VISA begins with 4, and length is 13 or 16.
		}
		else if (numberBeginsWith($cn, array(300, 301, 302, 303, 304, 305, 36, 38)) && (strlen($cn) == 14))
		{
			return 'DinersClub'; // Diners Club begins with 300-305 or 36 or 38, and length is 14.
		}
		else if (numberBeginsWith($cn, array(2014, 2149)) && (strlen($cn) == 15))
		{
			return 'enRoute'; // enRoute begins with 2014 or 2149, and length is 15.
		}
		else if ((numberBeginsWith($cn, [6]) || numberBeginsWith($cn, $disc_array)) && (strlen($cn) == 16))
		{
			return 'Discover'; //Discover begins with 6011, 622126 to 622925, 644 to 649, or 65, and length is 16.
		}
		else if (numberBeginsWith($cn, $jcb_array) && (strlen($cn) == 16))
		{
			return 'JCB';  // JCB begins with 3528 to 3589, and length is 16.
		}
		else if (numberBeginsWith($cn, array(2131, 1800)) && (strlen($cn) == 15))
		{
			return 'JCB';  // JCB begins with 2131 or 1800, and length is 15.
        } else if (numberBeginsWith($cn, array(50)) && (strlen($cn) == 19)) {
            return 'GiftCard';  // Gift Card begins with 50, and length is 19.
		}
		else if (numberBeginsWith($cn, array(9)) && strlen($cn) === 16) {
			return getCardTypeFromNumber(substr($cn, 1) . "0");
		} else {
			return 'UNKNWN';
		}
	}
	else
	{
		return "NA";
	}
}

function numberBeginsWith($cn, $look_for)
{
	$found_it = false;

	foreach ($look_for as $lf)
	{
		if (substr($cn, 0, strlen($lf)) == $lf)
		{
			$found_it = true;
			break;
		}
	}

	return $found_it;
}


function generate_transaction_vars(&$process_info, $location_info, $card_number, $card_type, $transtype, $swipe_grade, $pay_type, $pay_type_id, $amount)
{
	$p_info = $process_info;
	$l_info = $location_info;

	$vars = array(
		"loc_id" => $p_info['loc_id'],
		"device_udid" => $p_info['device_udid'],
		"ioid" => $p_info['ioid'],
		"order_id" => $p_info['order_id'],
		"check" => $p_info['check'],
		"internal_id" => isset($p_info['itid']) ? $p_info['itid'] : '',
		"transtype" => $transtype,
		"amount" => number_format((float)$amount, $l_info['disable_decimal'], ".", ""),
		"total_collected" => number_format((float)$amount, $l_info['disable_decimal'], ".", ""),
		"change" => number_format(0, $l_info['disable_decimal'], ".", ""),
		"card_type" => $card_type,
		"first_four" => substr($card_number, 0, 4),
		"card_desc" => substr($card_number, -4),
		"exp" => ($p_info['exp_month']) ? $p_info['exp_month'] . "/" . $p_info['exp_year'] : '',
		"info" => $p_info['name_on_card'],
		"swipe_grade" => $swipe_grade,
		"auth_code" => isset($p_info['authcode']) ? $p_info['authcode'] : '',
		"register" => $p_info['register'],
		"register_name" => $p_info['register_name'],
		"server_name" => $p_info['server_name'],
		"server_id" => $p_info['server_id'],
		"pay_type" => $pay_type,
		"pay_type_id" => $pay_type_id,
		"for_deposit" => $p_info['for_deposit'],
		"is_deposit" => $p_info['is_deposit'],
		"datetime" => $p_info['device_time'],
		"server_time" => UTCDateTime(true),
		"gateway" => $l_info['gateway'],
		"reader" => $p_info['reader'],
		"temp_data" => "pending",
		"card_holder_name" => $p_info['name_on_card'],
	);

	if ($transtype == "Return" || $transtype=="GiftCardReturn" || $transtype=="LoyaltyCardReturn")
	{
		$vars['tip_amount'] = $p_info['refund_tip_amount'];
		$vars['refund_pnref'] = $p_info['orig_internal_id'];
	}
	else if (!empty($p_info['tip_amount']))
	{
		$vars['tip_amount'] = $p_info['tip_amount'];
	}

	$process_info['exp'] = $vars['exp'];

	return $vars;
}

function specialDesignatorForPayTypeID($pay_type_id)
{
	if ($pay_type_id <= 3)
	{
		return "";
	}

	$get_pay_type = lavu_query("SELECT `special` FROM `payment_types` WHERE `id` = '[1]'", $pay_type_id);
	if (!$get_pay_type)
	{
		return "";
	}

	if (mysqli_num_rows($get_pay_type) == 0)
	{
		return "";
	}

	$info = mysqli_fetch_assoc($get_pay_type);

	return $info['special'];
}

function parseCardSwipe(&$mag_data,$card_reader,$encrypted)
{
	$parsed_card_info = array(
		'name'		=> "",
		'number'	=> "",
		'exp_month'	=> "",
		'exp_year'	=> ""
	);

	if ($encrypted == "2")
	{
		if(strlen($mag_data) == 3428)
		{
			require_once(__DIR__."/gateway_lib/heartland.php");
			$myParsedCryptoCard = new HeartlandMSR;
			$myParsedCryptoCard->SetMSR($mag_data);
			$parsed_card_info['number'] = $myParsedCryptoCard->GetCardNumber();
		}
		else
		{
			require_once(__DIR__."/gateway_lib/mercury_lls_func.php");
			$idtechLLSSwipe = new IDTechLLS();
			$idtechLLSSwipe->setMagData($mag_data);
			$parsed_card_info['number'] = $idtechLLSSwipe->GetMaskedCardNumber();
			// e2e encrypted swipe for LLS hosted card swipe
		}

	}
	else if ($encrypted == "3")
	{
		$mag_parts = explode("|", hexToString($mag_data));
		$found_track1 = false;
		$found_track2 = false;
		foreach ($mag_parts as $mp)
		{
			if (is_numeric($mp) && !$found_track1 && !$found_track2) $card_number = $mp;
			else if (substr($mp, 0, 1) == "%")
			{
				$found_track1 = true;
				$t1 = explode("^", $mp);
				$parsed_card_info['number'] = trim($t1[0], "%B");
			}
			else if (substr($mp, 0, 1) == ";")
			{
				$found_track2 = true;
				$t2 = explode("=", $mp);
				$parsed_card_info['number'] = trim($t1[0], ";");
			}
			if (strlen($card_number) >= 8) break;
		}

	}
	else if ($card_reader!="" && $mag_data)
	{
		if (in_array($card_reader, array("bluebamboo", "bluebamboo_p25i", "idtech_imag", "idtech_unimag", "linea", "zbar")) && $encrypted=="0")
		{
				$mag_data = interpret($mag_data);
		}
		else if ($card_reader=="idynamo" || $card_reader=="udynamo")
		{
			if (!strstr($mag_data, "|"))
			{
				$mag_data = interpret($mag_data);
			}
			if (strstr($mag_data, "xxxo|o"))
			{
				$mag_data_parts = explode("xxxo|o", $mag_data);
				$mag_data = "%".$mag_data_parts[(count($mag_data_parts) - 1)];
			}
			$mag_data = str_replace("o|o", "%", $mag_data);
		}
		else if ($encrypted == "1")
		{
			$hex = $mag_data;
			$mag_data = "";
			for ($h = 0; $h < strlen($hex)-1; $h += 2)
			{
				$mag_data .= chr(hexdec($hex[$h].$hex[$h+1]));
			}
		}
		if ($card_reader != "zbar")
		{
			$card_info = extract_card_info($mag_data);
			$parsed_card_info['name']		= $card_info['name'];
			$parsed_card_info['number']		= $card_info['number'];
			$parsed_card_info['exp_month']	= $card_info['exp_month'];
			$parsed_card_info['exp_year']	= $card_info['exp_year'];
		}
	}
	return $parsed_card_info;
}

function updateCCRecord($transactionId,$tip){
	lavu_query("UPDATE `cc_transactions` SET `tip_amount`='[1]' WHERE `transaction_id` = '[2]'", $tip,$transactionId);
}
/**
 * To refund amount on order , the order which is placed through PEPPER APP
 * To check if the transaction id is available
 * @param array $request_data
 * @return  Suscess string
 */
function toRefundAmountPepperOrder($request_data) {
	$get_transaction_id = lavu_query("SELECT `id`, `transaction_id`, `pay_type_id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `pay_type` = 'Loyalty App' AND `voided` = '0' AND  `action` = 'Sale' ", $request_data);
	if (mysqli_num_rows($get_transaction_id) > 0) {
		$info = mysqli_fetch_assoc($get_transaction_id);
		if ($info['transaction_id'] != '' ) {
			$transaction_id = $info['transaction_id'];
		} else {
			$request_data['cc_id'] = $info['id'];
			$transaction_id = toGetTransactionIdPepperOrder($request_data);
		}
		if ($transaction_id != "") {
			$refund_amount = $request_data['card_amount'];
			$args['transactionId'] = $transaction_id;
			$args['request_json'] = '{"refundAmount": "'.$refund_amount.'"}';
			$data['dataname'] = $request_data['data_name'];
			$pepper = new PepperApi($data);
			$response = $pepper->refundTransaction($args);
			if (isset($response['totalRefunded'])) {
				$last_mod_ts = time();
				// Successful Transaction Response Indexes

				$res = pepperResponseForPos(array('success_flag'=>1, 'transaction_id'=>$transaction_id, 'amount'=>$request_data['card_amount'], 'order_id'=>$request_data['order_id'],'ref_data'=>$response['shortCode'],'process_data'=>1, 'bonus_msg'=>1, 'last_mod_ts'=>$last_mod_ts, 'pay_type'=>'Loyalty App', 'pay_type_id'=>$info['pay_type_id'], 'transaction_type'=>'Return'));
	  		} else {
	  			$message = isset($response['message']) ? $response['message'] : "Unable to determine transaction type";
	  			$res = "0|".$message."|||".$request_data['order_id'];
	  		}
	  	} else {
	  		$res = "0|Unable to fetch transaction ID|||".$request_data['order_id'];
	  	}
  		return $res;
	}
}

function pepperResponseForPos($data) {
	$keys = array(
    'success_flag' => '',
    'transaction_id' => '',
    'card_last_four' => '',
    'response_msg' => '',
    'auth_code' => '',
    'card_type' => '',
    'record_no' => '',
    'amount' => '',
    'order_id' => '',
    'ref_data' => '',
    'process_data' => '',
    'new_balance' => '',
    'bonus_msg' => '',
    'card_first_four' => '',
    'last_mode_ts' => '',
    'tip_amount' => '',
    'split_tender_id' => '',
    'rf_id' => '',
    'pay_type' => '',
    'pay_type_id' => '',
    'transaction_type' => ''
	);

	foreach ($keys as $idx=>$key) {
		if ($data[$idx]) {
			$keys[$idx] = $data[$idx];
		}
	}

	$result = implode("|", $keys);

	return $result;

}

/**
 * To get transaction id an order , the order which is placed through PEPPER APP
 * To call Peeper api
 * @param array $request_data
 * @return  transaction ID
 */
function toGetTransactionIdPepperOrder($request_data) {
	$get_original_id = lavu_query("SELECT `original_id` FROM `orders` WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]' ", $request_data);
	if (mysqli_num_rows($get_original_id) > 0) {
		$info_order = mysqli_fetch_assoc($get_original_id);
		$original_id = explode('|o|', $info_order['original_id']);
		$pepper_data = isset($original_id[4]) ? json_decode($original_id[4], true) : array();
		$loyaltyInfo = isset($pepper_data['loyaltyInfo']) ? $pepper_data['loyaltyInfo'] : array();
		$data['dataname'] = $request_data['data_name'];
		$pepper = new PepperApi($data);
		$args['orderId'] = (isset($loyaltyInfo['orderId']) ? $loyaltyInfo['orderId'] : '');
		$response = $pepper->getOrder($args);
		lavu_query("UPDATE `cc_transactions` SET `transaction_id` = '". $response['transactions'][0]."' WHERE `id` = '[1]'", $request_data['cc_id']);
		return $response['transactions'][0];
	}

}
?>
