<?php
namespace Lavu\Helpers;
use Symfony\Component\HttpFoundation;

require_once(__DIR__."/../../vendor/autoload.php");

/**
 * Applies _asset version_ to path
 *
 * @var string
 */
function getAssetPath($path) {
    $version = getenv('ASSET_VERSION');

    if (!empty($version)) {
        $newPath = preg_replace("/^(scripts\/|styles\/)/", '${1}' . $version . '/', $path);
        if (file_exists($newPath)) {
            return $newPath;
        }
    }

    return $path;
}

/**
 * Checks whether the request is secure or not.
 *
 * Wrap Symfony's Request object to avoid
 * confusing developers that do not use
 * _Composer_.
 *
 * @return bool
 */
function isRequestSecure()
{
   $request = new HttpFoundation\Request();
   return $request->isSecure();
}

/**
 * Returns URL Protocol as a string
 *
 * Wrap Symfony's Request object to avoid
 * confusing developers that do not use
 * _Composer_.
 *
 * @return string
 */
function getRequestProtocol()
{
    $request = new HttpFoundation\Request();
    return $request->getScheme();
}