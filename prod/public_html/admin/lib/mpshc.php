<?php

	session_start();

	require_once(__DIR__."/mpshc_sessions.php");

	$tkn = urlvar("tkn", "");
	if (!empty($tkn))
	{
		$tkn_parts = explode(";", $tkn);
		if (count($tkn_parts) > 1)
		{
			$data_name = $tkn_parts[0];
			$_REQUEST['m'] = $tkn_parts[1];
		}
	}

	if (!isset($_REQUEST['cc']) && isset($_REQUEST['PaymentID']))
	{
		$payment_id			= $_REQUEST['PaymentID'];
		//$session_data		= $_SESSION['hc_'.$payment_id]; // using database to store session instead
		$session_data		= retrieveCallbackSession($data_name, $payment_id);
		$_REQUEST['cc']		= $session_data['cc'];
		$_REQUEST['loc_id']	= $session_data['loc_id'];
		$_REQUEST['ioid']	= $session_data['ioid'];
		$_REQUEST['is_dev']	= $session_data['dev_flag'];
	}

	$mode = (isset($_REQUEST['m']))?$_REQUEST['m']:"complete";
	$hold_cc = (isset($_REQUEST['cc']))?$_REQUEST['cc']:"";	

	require_once(__DIR__."/info_inc.php");
	require_once(__DIR__."/mpshc_functions.php");
	require_once(__DIR__."/gateway_functions.php");
	require_once(__DIR__."/gateway_lib/hosted_checkout/util.php");

	$gateway				= reqvar("gateway", "");
	$host_mode				= reqvar("host_mode", "");
	$custom_payment_info	= reqvar("custom_payment_info", "");	

	mlavu_close_db();

	log_comm_vars($data_name, $got_loc_id, "mpshc");
	
	$send_debug_messages = $location_info['gateway_debug'];

	$is_dev = lavuDeveloperStatus();

	if ($mode == "init") {
		$data_name				= reqvar("data_name", false);
		$loc_id					= reqvar("loc_id", false);
		$order_id				= reqvar("order_id", false);
		$check					= reqvar("check", "1");
		$card_amount			= reqvar("card_amount", false);
		$reskin					= reqvar("reskin", false);
		if ($reskin) {
			$duplicate_chheck ['order_id'] = $order_id;
			$duplicate_chheck ['check'] = $check;
			$duplicate_chheck ['card_amount'] = $card_amount;
			$duplicate_chheck ['loc_id'] = $loc_id;
			if (isDuplicatePayment($duplicate_chheck)) {
				echo createJSTags("window.location = '_DO:cmd=Mercury_Duplicate&info=Transaction successfully processed&errorType=Mercury_Duplicate_Transaction'");
				exit();
			}
		}
		$transtype				= reqvar("transtype", "Sale");
		$register				= reqvar("register", "");
		$register_name			= reqvar("register_name", "");
		$server_id				= $is_dev?"Test":reqvar("server_id", 0);
		$server_name			= reqvar("server_name", "");
		$device_udid			= determineDeviceIdentifier($_REQUEST);
		$for_deposit			= reqvar("for_deposit", "0");
		$is_deposit				= reqvar("is_deposit", "0");
		$bg_color				= reqvar("bc", "#E7E7E7");
		$font_color				= reqvar("fc", "#000000");
		$internal_order_id		= reqvar("ioid", "");
		$internal_tx_id			= reqvar("internal_id", "");
		$tip_amount				= reqvar("tip_amount", false);

		$app_name				= reqvar("app_name", "POS Lavu");
		$app_version			= reqvar("app_version", "");
		$app_build				= reqvar("app_build", "");
		if (empty($app_version)) {
			$app_version		= reqvar("version", "2.0");
			$app_build			= reqvar("build", "20110826-1");
		}
		
		$device_time			= reqvar("device_time", date("Y-m-d H:i:s", time()));
		$tz						= $location_info['timezone'];	
		if ($tz != "") {
			$device_time		= localize_datetime(date("Y-m-d H:i:s"), $tz);
		}

		$dev_flag = reqvar("is_dev", "0");

		if (isset($_REQUEST['inner_content'])) {
			
			if ($host_mode == "general") {
				$mpshc_referral = true;
				require_once(dirname(__FILE__) . "/hosted_payment.php");
				exit();
			} else if($host_mode == 'tyro'){
				require_once(dirname(__FILE__) . '/tyro.php');
				exit();
			}
		
			$session_data = array();
			$session_data['cc']						= $hold_cc;
			$session_data['data_name']				= $data_name;
			$session_data['dev_flag']				= $dev_flag;
			$session_data['loc_id']					= $loc_id;
			$session_data['order_id']				= $order_id;
			$session_data['check']					= $check;
			$session_data['card_amount']				= $card_amount;
			$session_data['transtype']				= $transtype;
			$session_data['register']				= $register;
			$session_data['register_name']			= $register_name;
			$session_data['server_id']				= $server_id;
			$session_data['server_name']				= $server_name;
			$session_data['device_time']				= $device_time;
			$session_data['device_udid']				= $device_udid;
			$session_data['send_debug_messages']		= $send_debug_messages;
			$session_data['for_deposit']				= $for_deposit;
			$session_data['is_deposit']				= $is_deposit;
			$session_data['app_name']				= $app_name;
			$session_data['app_version']				= $app_version;
			$session_data['app_build']				= $app_build;
			$session_data['for_lavu_togo']			= reqvar("ltg", "0");
			$session_data['ioid']					= $internal_order_id;
			$session_data['itid']					= $internal_tx_id;
			$session_data['tip_amount']				= $tip_amount;

			if ($data_name && $loc_id && is_numeric($loc_id)) {

				$db_name = "poslavu_".$data_name."_db";
				$integration_data = get_integration_from_location($loc_id, $db_name);
				
				$debug = print_r($_REQUEST, true);
				
				switch ($transtype) {
					case "Sale" :
						$trancode = "Sale";
						break;
					case "Auth" :
						$trancode = "PreAuth";
						break;
					default: break;
				}
				
				$frequency = "OneTime";

				$useAVS = "";
				if ($session_data['for_lavu_togo']=="1" || locationSetting("mercury_hosted_checkout_avs_fields")=="1")
				{
					$useAVS = "Zip";
				}

				$callback_domain = getenv('URI_SCHEME')."://".$_SERVER['SERVER_NAME'];
				$callback_url = $callback_domain."/lib/mpshc.php";

				$postvars = array(
					array( "MerchantID",				$integration_data['integration3'] ), // 494691720
					array( "Password",				$integration_data['integration4'] ), // KRD%8rw#+p9C13,T
					array( "TranType",				$trancode ),
					array( "TotalAmount",			$card_amount ),
					array( "Frequency",				$frequency ),
					array( "Invoice",				str_replace("-", "", $loc_id.$order_id) ),
					array( "Memo",					$app_name." v".$app_version ),
					array( "TaxAmount",				"0.00" ),
					array( "CardHolderName",			"Keyed Entry" ),
					array( "DefaultSwipe",			"Manual" ),
					array( "CardEntryMethod",		"Manual" ),
					array( "AVSFields",				$useAVS ),
					array( "CustomerCode",			"" ),
					array( "ProcessCompleteUrl",	$callback_url."?tkn=".$data_name.";complete"),
					array( "ReturnUrl",				$callback_url."?tkn=".$data_name.";cancel" ),
					array( "DisplayStyle",			"Custom" ),
					array( "BackgroundColor",		$bg_color ),
					array( "FontColor",				$font_color ),
					array( "FontFamily",				"Arial" ),
					array( "FontSize",				"Small" ),
					array( "LogoURL",				"" ),
					array( "PageTitle",				"" ),
					array( "SecurityLogo",			"Off" ),
					array( "OrderTotal",				"On" ),
					array( "SubmitButtonText",		"Submit" ),
					array( "CancelButtonText",		"Cancel" ),
                    array("OperatorID",         	$server_id)
				);

				$curl_result = post_to_mercury_hc($postvars, $data_name, $loc_id, $transtype, "InitializePayment");
	
				$debug .= "\n\n".$curl_result;

				$response_code = extract_value($curl_result, "ResponseCode");
				$message = extract_value($curl_result, "Message");
				$payment_id = extract_value($curl_result, "PaymentID");
				
				if ($message == "Authenticate Failed") {
					$response = "Authentication Failed - Please check your Merchant ID (Integration data 3) and Password (Integration data 4)";
					lavu_echo(HTMLResponse($response));
				}
				
				if (empty($payment_id)) {
					$response = "Unable to store session data: invalid payment_id";
					lavu_echo(HTMLResponse($response));
				}
				
				//$_SESSION['hc_'.$payment_id] = $session_data; // using database to store session data

				if (!storeCallbackSession($payment_id, $session_data))
				{
					$response = "Unable to store session data: database query failed";
					lavu_echo(HTMLResponse($response));
				}

				$debug .= "\n\n".$response_code."\n\n".$payment_id."\n\n".$callback_url;

				$approved = "0";
				if (($response_code == "0") && ($response_code != "")) $approved = "1";
				if ($send_debug_messages == "1") gateway_debug($data_name, $loc_id, "MPSHC", "INIT", $approved, $debug);

				if ($approved == "1") {				
				
					header("Location: https://".$mpshc_server_url."/Checkoutiframe.aspx?pid=".$payment_id);
					exit();
				
				} else {
				
					$lg_str = "MERCURY HOSTED CHECKOUT[--CR--]";
					$lg_str .= "LOCATION: ".			$loc_id			." - ";
					$lg_str .= "REGISTER: ".			$register		." - ";
					$lg_str .= "SERVER ID: ".		$server_id		." - ";
					$lg_str .= "ORDER ID: ".			$order_id		." - ";
					$lg_str .= "CHECK: ".			$check			." - ";
					$lg_str .= "AMOUNT: ".			$card_amount	." - ";
					$lg_str .= "TRANSTYPE: ".		$transtype		." - ";
					$lg_str .= "RESPONSE CODE: ".	$response_code	."[--CR--]";
					$lg_str .= "RESPONSE MESSAGE: ".	$message		."[--CR--]";
					$lg_str .= "Failed to initialize hosted checkout payment...[--CR--]";
					write_log_entry("gateway", $data_name, $lg_str."\n");
				
					$send_message = "";
					switch ($response_code) {
						
						case 100:
							
							$send_message = "Invalid Credentials";
							break;
						
						case 101:
						
							break;
						
						default:
						
							$send_message = "ResponseCode ".$response_code." - ".$message;
							break;
					}

					lavu_echo(HTMLResponse($send_message));
				}
			}

		} else {

			echo "<html>";
			echo "<body style='background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; -webkit-tap-highlight-color:rgba(0,0,0,0);'>";
			echo "<center>";
			echo "<iframe name='the_iframe' frameborder='0' width='450px' height='450px' scrolling='no' src=''></iframe>";
			echo "<form name='the_form' method='post' action='mpshc.php' target='the_iframe'>";
			echo "<input type='hidden' name='gateway' value='$gateway'>";
			echo "<input type='hidden' name='host_mode' value='$host_mode'>";
			echo "<input type='hidden' name='custom_payment_info' value=\"".str_replace("\"","&quot;",$custom_payment_info)."\">";
			echo "<input type='hidden' name='inner_content' value='1'>";
			echo "<input type='hidden' name='m' value='init'>";
			echo "<input type='hidden' name='cc' value='$hold_cc'>";
			echo "<input type='hidden' name='data_name' value='$data_name'>";
			echo "<input type='hidden' name='is_dev' value='".$dev_flag."'>";
			echo "<input type='hidden' name='loc_id' value='$loc_id'>";
			echo "<input type='hidden' name='order_id' value='$order_id'>";
			echo "<input type='hidden' name='check' value='$check'>";
			echo "<input type='hidden' name='card_amount' value='$card_amount'>";
			echo "<input type='hidden' name='transtype' value='$transtype'>";
			echo "<input type='hidden' name='register' value='$register'>";
			echo "<input type='hidden' name='register_name' value='".str_replace("'", "\'", $register_name)."'>";
			echo "<input type='hidden' name='server_id' value='$server_id'>";
			echo "<input type='hidden' name='server_name' value='".str_replace("'", "\'", $server_name)."'>";
			echo "<input type='hidden' name='app_name' value='$app_name'>";
			echo "<input type='hidden' name='app_version' value='$app_version'>";
			echo "<input type='hidden' name='app_build' value='$app_build'>";
			echo "<input type='hidden' name='device_time' value='$device_time'>";
			echo "<input type='hidden' name='UDID' value='$device_udid'>";
			echo "<input type='hidden' name='for_deposit' value='$for_deposit'>";
			echo "<input type='hidden' name='is_deposit' value='$is_deposit'>";
			echo "<input type='hidden' name='bc' value='$bg_color'>";
			echo "<input type='hidden' name='fc' value='$font_color'>";
			echo "<input type='hidden' name='ioid' value='$internal_order_id'>";
			echo "<input type='hidden' name='internal_id' value='$internal_tx_id'>";
			echo "<input type='hidden' name='tip_amount' value='$tip_amount'>";
			echo "</form>";
			echo "</center>";
			echo createJSTags("the_form.submit();");
			echo "</body>";
			echo "</html>";	

		}

	} else if ($mode=="complete" || $mode=="cancel") {

		$debug			= print_r($_REQUEST, true);
		$return_code	= $_REQUEST['ReturnCode'];
		$payment_id		= $_REQUEST['PaymentID'];
		//$session_data	= $_SESSION['hc_'.$payment_id];
		if (!$session_data) {
			$error = "Failed to retieve session data...";
			error_log("mpshc error: ".$error);
			lavu_echo(HTMLResponse($error));
		}
		$lavu_code		= lc_encode("LTG:".date("md"));
			
		if ($return_code == "0") {
		
			$data_name			= $session_data['data_name'];
			$db_name			= "poslavu_".$data_name."_db";
			$integration_data	= get_integration_from_location($session_data['loc_id'], $db_name);

			$postvars = array();
			$postvars[] = array("MerchantID", $integration_data['integration3']); // 494691720
			$postvars[] = array("Password", $integration_data['integration4']); // KRD%8rw#+p9C13,T
			$postvars[] = array("PaymentID", $payment_id);
			$poststr = json_encode($postvars);
			$debug .= "\n\n".$poststr;
			$curl_result = post_to_mercury_hc($postvars, $data_name, $session_data['loc_id'],  $session_data['transtype'], "VerifyPayment");

			$debug .= "\n\n".$curl_result;
			
			$response_code		= extract_value($curl_result, "ResponseCode");
			$status_message		= extract_value($curl_result, "StatusMessage");
			
			$masked_card_number	= extract_value($curl_result, "MaskedAccount");
			$first_four			= trim(substr($masked_card_number, 0, 4), " x*");
			$card_desc			= substr($masked_card_number, -4);
	
			$action = ($session_data['transtype'] == "Return")?"Refund":"Sale";
				
			$t_vars = array();
			$t_vars['loc_id']			= $session_data['loc_id']; // 0
			$t_vars['order_id']			= $session_data['order_id'];
			$t_vars['check']				= $session_data['check'];
			$t_vars['amount']			= $session_data['card_amount']; //number_format($session_data['card_amount'], $decimal_places, ".", "");
			$t_vars['datetime']			= $session_data['device_time'];
			$t_vars['register']			= $session_data['register'];
			$t_vars['register_name']		= $session_data['register_name'];
			$t_vars['transtype']			= $session_data['transtype'];
			$t_vars['auth']				= ($session_data['transtype']=="Auth")?"1":"0";
			$t_vars['change']			= "0";
			$t_vars['total_collected']	= $session_data['card_amount']; //number_format($session_data['card_amount'], $decimal_places, ".", ""); // 10
			$t_vars['server_id']			= $session_data['server_id'];
			$t_vars['server_name']		= $session_data['server_name'];
			$t_vars['for_deposit']		= $session_data['for_deposit'];
			$t_vars['is_deposit']		= $session_data['is_deposit'];
			$t_vars['action']			= $action;
			$t_vars['card_type']			= extract_value($curl_result, "CardType");
			$t_vars['card_desc']			= $card_desc;
			$t_vars['transaction_id']	= extract_value($curl_result, "RefNo");
			$t_vars['preauth_id']		= $t_vars['transaction_id'];
			$t_vars['auth_code']			= extract_value($curl_result, "AuthCode"); // 20
			$t_vars['record_no']			= extract_value($curl_result, "Token");
			$t_vars['process_data']		= extract_value($curl_result, "ProcessData");
			$t_vars['ref_data']			= extract_value($curl_result, "AcqRefData");
			$t_vars['pay_type']			= "Card";
			$t_vars['pay_type_id']		= "2";
			$t_vars['mpshc_pid']			= $payment_id;
			$t_vars['swipe_grade']		= "H";
			$t_vars['got_response']		= "1";
			$t_vars['ioid']				= $session_data['ioid'];
			$t_vars['internal_id']		= $session_data['itid']; // 30
			$t_vars['tip_amount']		= "";
			$t_vars['gateway']			= "Mercury";
			$t_vars['reader']			= "mpshc";
			$t_vars['info']				= extract_value($curl_result, "CardholderName");
			$t_vars['exp']				= prepareExp(extract_value($curl_result, "ExpDate"));
			$t_vars['first_four']		= $first_four;
			$t_vars['device_udid']		= $session_data['device_udid'];
			$t_vars['last_mod_ts']		= time();
			$q_fields = "";
			$q_values = "";
			$fwd_string = "";
			$keys = array_keys($t_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
				if ($fwd_string != "") $fwd_string .= ":*:";
				$fwd_string .= $t_vars[$key];
			}
					
			if ($response_code=="0" && $response_code!="") {
			
				$ap_result = post_to_mercury_hc($postvars, $data_name, $session_data['loc_id'], $session_data['transtype'], "AcknowledgePayment");

				switch ($ap_result) {
				
					case "0":
					
						$ap_message = "Approved";
						break;
						
					case "100":
					
						$ap_message = "Acknowledge Authentication failure";
						break;
						
					case "300":
					
						$ap_message = "Acknowledge Validation failure. Check MerchantID Password and Payment ID are valid.";
						break;
						
					default:
					
						$ap_message = "An unknown error occurred while performing payment acknowledgement. Please try processing payment again.";
						break;		
				}

				$log_fwd = "\nForward to App: ".$fwd_string;

				$lg_str = "MERCURY HOSTED CHECKOUT[--CR--]";
				$lg_str .= "LOCATION: ".				$session_data['loc_id']					." - ";
				$lg_str .= "REGISTER: ".				$session_data['register']				." - ";
				$lg_str .= "SERVER ID: ".			$session_data['server_id']				." - ";
				$lg_str .= "ORDER ID: ".				$session_data['order_id']				." - ";
				$lg_str .= "CHECK : ".				$session_data['check']					." - ";
				$lg_str .= "AMOUNT: ".				$session_data['card_amount']				." - ";
				$lg_str .= "TRANSTYPE: ".			$session_data['transtype']				." - ";
				$lg_str .= "RESULT: ".				$ap_result								." - ";
				$lg_str .= "REFERENCE #: ".			extract_value($curl_result, "RefNo")		." - ";
				$lg_str .= "AUTHCODE: ".				extract_value($curl_result, "AuthCode")	." - ";
				$lg_str .= "CARD TYPE: ".			extract_value($curl_result, "CardType")	." - ";
				$lg_str .= "LAST FOUR: ".			$card_desc								."[--CR--]";
				$lg_str .= "RESPONSE MESSAGE: ".		$ap_message								."[--CR--]";
				$lg_str .= $log_fwd."[--CR--]";
				write_log_entry("gateway", $data_name, $lg_str."\n");		

				$approved = ($ap_result=="0" && $ap_result!="")?"1":"0";
				if ($session_data['send_debug_messages'] == "1") gateway_debug($data_name, $session_data['loc_id'], "MPSHC", "ACK", $approved, $debug);
								
				if ($approved == "1")
				{
					$build_int = (int)str_replace(array("-", "AH"), array("", ""), $session_data['app_build']);
					if ($build_int < 201202271)
					{
						$record_transaction = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $t_vars);
						$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` + [amount]), `void` = '0', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]', `last_modified` = '[datetime]', `last_mod_device` = '[device_udid]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $t_vars);
						$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $t_vars);
						if (@mysqli_num_rows($check_for_details) > 0)
						{
							$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` + [amount]) WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $t_vars);
						}
						else
						{
							$create_details = lavu_query("INSERT INTO `split_check_details` (`ioid`, `loc_id`, `order_id`, `check`, `card`) VALUES ('[ioid]', '[loc_id]', '[order_id]', '[check]', '[amount]')", $t_vars);
						}
					
						$process_info = array(
							"loc_id"		=> $t_vars['loc_id'],
							"order_id"		=> $t_vars['order_id'],
							"check"			=> $t_vars['check'],
							"device_time"	=> $session_data['device_time'],
							"server_name"	=> $t_vars['server_name'],
							"server_id"		=> $t_vars['server_id'],
							"device_udid"	=> $session_data['device_udid']
						);
						
						$_REQUEST['app_name']		= $session_data['app_name'];
						$_REQUEST['app_version']		= $session_data['app_version'];
						$_REQUEST['app_build']		= $session_data['app_build'];

						actionLogItGW($process_info, "Payment Applied", "Check ".$t_vars['check']." - Card - ".priceFormatGW($t_vars['amount']));
					}
					
					$info_parts = array(
						$t_vars['transaction_id'],
						$card_desc,
						"Approved",
						$t_vars['auth_code'],
						$t_vars['card_type'],
						$payment_id,
						$t_vars['record_no']
					);
					
					$info = implode(":o:", $info_parts);
					
					if (!empty($session_data['for_lavu_togo']) ) {

						if ($session_data['for_lavu_togo'] == '2') {
							echo createJSTags("window.location = '{$_REQUEST['goodURL']}';");
						} else if ($build_int < 201202271) {
							echo createJSTags("window.location = 'https://www.lavutogo.com/app/libs/fwd_mpshc.php?c=".$lavu_code."&r=1&i=".$info."';");
						} else {
							echo createJSTags("window.location = 'https://www.lavutogo.com/app/libs/fwd_mpshc.php?c=".$lavu_code."&r=1&i=".$fwd_string."';");
						}

					} else {
						echo createJSTags("window.location = '_DO:cmd=Approved&info=".$info.":o:".$fwd_string."'");
					}

					exit();
					
				} else {
				
					if (!empty($session_data['for_lavu_togo']) ) {

						if($session_data['for_lavu_togo'] == '2') {
							echo createJSTags("window.location = '{$_REQUEST['badURL']}';");
						} else {
							echo createJSTags("window.location = 'https://www.lavutogo.com/app/libs/fwd_mpshc.php?c=".$lavu_code."&r=0&i=".rawurlencode($ap_message)."';");
						}

					} else {

						echo createJSTags("window.location = '_DO:cmd=Declined&info=".$ap_message."'");
					}

					exit();
				}
			
			} else {
	
				$lg_str = "MERCURY HOSTED CHECKOUT[--CR--]";
				$lg_str .= "LOCATION: ".			$session_data['loc_id']					." - ";
				$lg_str .= "REGISTER: ".			$session_data['register']				." - ";
				$lg_str .= "SERVER ID: ".		$session_data['server_id']				." - ";
				$lg_str .= "ORDER ID: ".			$session_data['order_id']				." - ";
				$lg_str .= "CHECK : ".			$session_data['check']					." - ";
				$lg_str .= "AMOUNT: ".			$session_data['card_amount']				." - ";
				$lg_str .= "TRANSTYPE: ".		$session_data['transtype']				." - ";
				$lg_str .= "RESPONSE CODE: ".	$response_code							." - ";
				$lg_str .= "REFERENCE #: ".		extract_value($curl_result, "RefNo")		." - ";
				$lg_str .= "AUTHCODE: ".			extract_value($curl_result, "AuthCode")	." - ";
				$lg_str .= "CARD TYPE: ".		extract_value($curl_result, "CardType")	." - ";
				$lg_str .= "LAST FOUR: ".		$card_desc								."[--CR--]";
				$lg_str .= "STATUS MESSAGE: ".	$status_message							."[--CR--]";
				$lg_str .= "Failed to verify hosted checkout payment...[--CR--]";
				write_log_entry("gateway", $session_data['data_name'], $lg_str."\n");
							
				if ($session_data['send_debug_messages'] == "1") gateway_debug($session_data['data_name'], $session_data['loc_id'], "MPSHC", "VERIFY", "0", $debug);

				$send_message = ($response_code != "101")?"ResponseCode ".$response_code." - ".$status_message:"";

				if (!empty($session_data['for_lavu_togo']) ) {

					if($session_data['for_lavu_togo'] == '2') {
						echo createJSTags("window.location = '{$_REQUEST['badURL']}';");
					} else {
						echo createJSTags("window.location = 'https://www.lavutogo.com/app/libs/fwd_mpshc.php?c=".$lavu_code."&r=0&i=".rawurlencode($send_message)."';");
					}
				
				} else {

					echo createJSTags("document.location = '_DO:cmd=Declined&info=".$send_message."'");
				}

				exit();
			}
		
		} else {

			$lg_str = "MERCURY HOSTED CHECKOUT[--CR--]";
			$lg_str .= "LOCATION: ".			$session_data['loc_id']					." - ";
			$lg_str .= "REGISTER: ".			$session_data['register']				." - ";
			$lg_str .= "SERVER ID: ".		$session_data['server_id']				." - ";
			$lg_str .= "ORDER ID: ".			$session_data['order_id']				." - ";
			$lg_str .= "CHECK : ".			$session_data['check']					." - ";
			$lg_str .= "AMOUNT: ".			$session_data['card_amount']				." - ";
			$lg_str .= "TRANSTYPE: ".		$session_data['transtype']				." - ";
			$lg_str .= "RETURN CODE: ".		$return_code							." - ";
			$lg_str .= "nRETURN MESSAGE: ".	$_REQUEST['ReturnMessage']				."[--CR--]";
			$lg_str .= "Failed to process hosted checkout payment...[--CR--]";
			write_log_entry("gateway", $session_data['data_name'], $lg_str."\n");
				
			if ($session_data['send_debug_messages'] == "1") gateway_debug($session_data['data_name'], $session_data['loc_id'], "MPSHC", "COMPLETE", "0", $debug);

			$send_message = ($return_code != "101")?"ReturnCode ".$return_code." - ".$_REQUEST['ReturnMessage']:"";
	
			if (!empty($session_data['for_lavu_togo']) ) {

				if ($session_data['for_lavu_togo'] == '2') {
					echo createJSTags("window.location = '{$_REQUEST['badURL']}';");
				} else {
					echo createJSTags("window.location = 'https://www.lavutogo.com/app/libs/fwd_mpshc.php?c=".$lavu_code."&r=0&i=".rawurlencode($send_message)."';");
				}
				
			} else {
				
				echo createJSTags("document.location = '_DO:cmd=Declined&info=".$send_message."'");
			}

			exit();
		}

	} else if ($mode == "void") {

		$data_name					= reqvar("data_name", false);
		$loc_id						= reqvar("loc_id", false);
		$transtype					= reqvar("transtype", "Void");
		$order_id					= reqvar("order_id", false);
		$pnref						= reqvar("pnref", false);
		$server_id					= reqvar("server_id", 0);
		$more_info					= reqvar("more_info", "");		
		$register					= reqvar("register", "");
		$server_name				= reqvar("server_name", "");
		$authcode					= reqvar("authcode", "");
		$check						= reqvar("check", "1");
		$register_name				= reqvar("register_name", "");
		$pay_type					= reqvar("pay_type", "Card");
		$pay_type_id				= reqvar("pay_type_id", "2");
		$device_udid				= determineDeviceIdentifier($_POST);
		$internal_order_id			= reqvar("ioid", "");
		$internal_tx_id				= reqvar("internal_id", "");
		$card_type					= reqvar("orig_card_type", "");
		$first_four					= reqvar("orig_first_four", "");
		$last_four					= reqvar("orig_last_four", "");
		$orig_action				= reqvar("orig_action", "");
		$card_amount				= reqvar("card_amount", "");
		$tip_amount					= reqvar("tip_amount", "");

		$app_name					= reqvar("app_name", "POS Lavu");
		$app_version				= reqvar("app_version", "");
		$app_build					= reqvar("app_build", "");
		if (empty($app_version)) {
			$app_version			= reqvar("version", "2.0");
			$app_build				= reqvar("build", "20110826-1");
		}

		$mtoken 						= reqvar("record_no", "");
		if (empty($mtoken)) $mtoken	= reqvar("ref_data", "");
		$mtoken = str_replace("[PLUS]", "+", $mtoken);

		$device_time				= reqvar("device_time", date("Y-m-d H:i:s", time()));
		if ($location_info['timezone'] != "") {
			$device_time			= localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
		}

		$debug = print_r($_POST, true);

		$get_transaction_info = lavu_query("SELECT `id`, `action`, `transtype`, `auth`, `pay_type`, `card_type`, `first_four`, `card_desc`, `check`, `amount`, `tip_amount` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `transaction_id` = '[3]' AND `voided` != '1' AND (`action` = 'Sale' OR `action` = 'Refund')", $loc_id, $order_id, $pnref);
		$tx_info = mysqli_fetch_assoc($get_transaction_info);
		
		if (empty($card_type)) $card_type = $tx_info['card_type'];
		if (empty($first_four)) $first_four = $tx_info['first_four'];
		if (empty($last_four)) $last_four = $tx_info['card_desc'];
	
		if ($tx_info['transtype']=="Auth" && $tx_info['auth']=="1") {

			if ($send_debug_messages == "1") gateway_debug($data_name, $loc_id, "MPSHC", "VOID", "1", $debug);
		
			$last_mod_ts = time();
		
			$mark_as_voided = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = 'VOIDED', `last_mod_ts` = '[3]' WHERE `id` = '[4]'", $more_info, $server_id, $last_mod_ts, $tx_info['id']);
			$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - ".$tx_info['amount']."), `card_desc` = '', `transaction_id` = '', `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = '[3]' WHERE `location_id` = '[4]' AND `order_id` = '[5]'", $last_mod_ts, $device_time, $device_udid, $loc_id, $order_id);
			$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - ".$tx_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $tx_info['check']);
			
			$process_info = array(
				"loc_id"		=> $loc_id,
				"order_id"		=> $order_id,
				"check"			=> $check,
				"device_time"	=> $device_time,
				"server_name"	=> $server_name,
				"server_id"		=> $server_id,
				"device_udid"	=> $device_udid
			);
			
			actionLogItGW($process_info, "Payment Voided", "Check ".$check." - ".$tx_info['pay_type']." - ".priceFormatGW($tx_info['amount']));
				
			$rtn = array();
			$rtn[] = "1"; // 0
			$rtn[] = "VOIDED";
			$rtn[] = $last_four;
			$rtn[] = "APPROVED";
			$rtn[] = "VOIDED";
			$rtn[] = $card_type;
			$rtn[] = "";
			$rtn[] = $tx_info['amount'];
			$rtn[] = $order_id;
			$rtn[] = "";
			$rtn[] = ""; // 10
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $first_four;
			$rtn[] = $last_mod_ts;
			
			echo implode("|", $rtn);
			exit();
		
		} else {

			$db_name			= "poslavu_".$data_name."_db";
			$integration_data	= get_integration_from_location($loc_id, $db_name);	
			$frequency			= "OneTime";
			
			if (empty($orig_action)) $orig_action = $tx_info['action'];		
			if (empty($tip_amount)) $tip_amount = $tx_info['tip_amount'];

			$void_method = ($orig_action == "Refund")?"CreditVoidReturnToken":"CreditVoidSaleToken";

			$postvars = array();
			$postvars[] = array("MerchantID", $integration_data['integration3']); // 494691720
			$postvars[] = array("PurchaseAmount", number_format(($card_amount + $tip_amount), $decimal_places, ".", ""));
			$postvars[] = array("Invoice", str_replace("-", "", $loc_id.$order_id));
			$postvars[] = array("RefNo", $pnref);
			$postvars[] = array("TerminalName", $register);
			$postvars[] = array("OperatorID", $server_id);
			$postvars[] = array("Memo", $app_name." v".$app_version);
			$postvars[] = array("Token", $mtoken);
			$postvars[] = array("Frequency", $frequency);	
			$postvars[] = array("AuthCode", $_POST['authcode']);
			
			$debug .= "\n\n".print_r($postvars, true);
						
			$curl_result = post_to_mercury_tws($postvars, $data_name, $loc_id, $transtype, $void_method, $integration_data['integration4']);
		
			$debug .= "\n\n".$curl_result;
			
			$status				= extract_value($curl_result, "Status");
			$message			= extract_value($curl_result, "Message");
			$new_pnref			= extract_value($curl_result, "RefNo");
			$new_authcode		= extract_value($curl_result, "AuthCode");
			$new_ref_data		= extract_value($curl_result, "AcqRefData");
			$new_process_data	= extract_value($curl_result, "ProcessData");
			$new_token			= extract_value($curl_result, "Token");			
		
			$lg_str = "MERCURY HOSTED CHECKOUT[--CR--]";
			$lg_str .= "LOCATION: ".		$loc_id			." - ";
			$lg_str .= "REGISTER: ".		$register		." - ";
			$lg_str .= "SERVER ID: ".	$server_id		." - ";
			$lg_str .= "ORDER ID: ".		$order_id		." - ";
			$lg_str .= "CHECK : ".		$check			." - ";
			$lg_str .= "AMOUNT: ".		$card_amount	." - ";
			$lg_str .= "TRANSTYPE: ".	$transtype		." - ";
			$lg_str .= "REFERENCE #: ".	$new_pnref		." - ";
			$lg_str .= "AUTHCODE: ".		$new_authcode	." - ";
			$lg_str .= "CARD TYPE: ".	$card_type		." - ";
			$lg_str .= "LAST FOUR: ".	$last_four		."[--CR--]";
			$lg_str .= "STATUS: ".		$status			."[--CR--]";
			$lg_str .= "MESSAGE: ".		$message		."[--CR--]";
			write_log_entry("gateway", $data_name, $lg_str."\n");
		
			$approved = ($status == "Approved")?"1":"0";
			if ($send_debug_messages == "1") gateway_debug($data_name, $loc_id, "MPSHC", "VOID", $approved, $debug);

			if ($approved == "1") {
				
				$last_mod_ts = time();
	
				$t_vars = array();
				$t_vars['loc_id']			= $loc_id;
				$t_vars['order_id']			= $order_id;
				$t_vars['check']				= $check;
				$t_vars['datetime']			= $device_time;
				$t_vars['amount']			= number_format($card_amount, $decimal_places, ".", "");
				$t_vars['total_collected']	= number_format($card_amount, $decimal_places, ".", "");
				$t_vars['transaction_id']	= $new_pnref;
				$t_vars['card_type']			= $card_type;
				$t_vars['first_four']		= $first_four;
				$t_vars['card_desc']			= $last_four;
				$t_vars['transaction_id']	= $new_pnref;
				$t_vars['auth_code']			= $new_authcode;
				$t_vars['pay_type']			= $pay_type;
				$t_vars['pay_type_id']		= $pay_type_id;		
				$t_vars['register']			= $register;
				$t_vars['register_name']		= $register_name;
				$t_vars['transtype']			= $void_method;
				$t_vars['action']			= "Void";
				$t_vars['got_response']		= "1";
				$t_vars['server_id']			= $server_id;
				$t_vars['server_name']		= $server_name;
				$t_vars['ref_data']			= $new_ref_data;
				$t_vars['process_data']		= $new_process_data;
				$t_vars['record_no']			= $new_token;
				$t_vars['ioid']				= $internal_order_id;
				$t_vars['gateway']			= "Mercury";
				$t_vars['reader']			= "mpshc";
				$t_vars['internal_id']		= $internal_tx_id;
				$t_vars['device_udid']		= $device_udid;
				$t_vars['last_mod_ts']		= $last_mod_ts;
				$q_fields = "";
				$q_values = "";
				$keys = array_keys($t_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$record_void = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $t_vars);
				
				$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = '[3]', `last_mod_ts` = '[4]' WHERE `id` = '[5]'", $more_info, $server_id, $new_pnref, $last_mod_ts, $tx_info['id']);
		
				$t_action = $orig_action;
				if ($t_action == "Sale") {

					$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - ".$tx_info['amount']."),`void`='0', `card_gratuity` = (`card_gratuity` - ".number_format((float)$tx_info['tip_amount'], $decimal_places, ".", "")."), `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = '[3]' WHERE `location_id` = '[4]' AND `order_id` = '[5]'", $last_mod_ts, $device_time, $device_udid, $loc_id, $order_id);
					$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - ".$tx_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $tx_info['check']);

				} else if ($t_action == "Refund") {

					$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` + ".$tx_info['amount']."), `refunded_cc` = (`refunded_cc` - ".$tx_info['amount']."), `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = '[3]' WHERE `location_id` = '[4]' AND `order_id` = '[5]'", $last_mod_ts, $device_time, $device_udid, $loc_id, $order_id);
					$tx_info['amount'] = str_replace("'", "", $tx_info['amount']);
					$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` + ".$tx_info['amount']."), `refund_cc` = (`refund_cc` - ".$tx_info['amount'].") WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $tx_info['check']);
				}
				
				$process_info = array(
					"loc_id"		=> $loc_id,
					"order_id"		=> $order_id,
					"check"			=> $check,
					"device_time"	=> $device_time,
					"server_name"	=> $server_name,
					"server_id"		=> $server_id,
					"device_udid"	=> $device_udid
				);
				
				actionLogItGW($process_info, (($t_action == "Sale")?"Payment":"Refund")." Voided", "Check ".$check." - ".$tx_info['pay_type']." - ".priceFormatGW($tx_info['amount']));
					
				$rtn = array();
				$rtn[] = "1"; // 0
				$rtn[] = $new_pnref;
				$rtn[] = $last_four;
				$rtn[] = $status;
				$rtn[] = $new_authcode;
				$rtn[] = $card_type;
				$rtn[] = $new_token;
				$rtn[] = $tx_info['amount'];
				$rtn[] = $order_id;
				$rtn[] = $new_ref_data;
				$rtn[] = rawurlencode($new_process_data); // 10
				$rtn[] = "";
				$rtn[] = "";
				$rtn[] = $first_four;
				$rtn[] = $last_mod_ts;
				
				echo implode("|", $rtn);
				exit();
			
			} else {
		
				$rtn = array();
				$rtn[] = "0";
				$rtn[] = $status." - ".$message;
		
				echo implode("|", $rtn);		
				exit();
			}
		}

	} else if ($mode == "return") {

		$data_name							= reqvar("data_name", false);
		$loc_id								= reqvar("loc_id", false);
		$transtype							= reqvar("transtype", "Return");
		$order_id							= reqvar("order_id", false);
		$pnref								= reqvar("pnref", false);
		$server_id							= reqvar("server_id", 0);
		$more_info							= reqvar("more_info", "");		
		$register							= reqvar("register", "");
		$server_name						= reqvar("server_name", "");
		$authcode							= reqvar("authcode", "");
		$check								= reqvar("check", "1");
		$register_name						= reqvar("register_name", "");
		$pay_type							= reqvar("pay_type", "Card");
		$pay_type_id						= reqvar("pay_type_id", "2");
		$device_udid						= determineDeviceIdentifier($_POST);
		$for_deposit						= reqvar("for_deposit", "0");
		$is_deposit							= reqvar("is_deposit", "0");
		$internal_order_id					= reqvar("ioid", "");
		$internal_tx_id						= reqvar("internal_id", "");
		$card_type							= reqvar("orig_card_type", "");
		$first_four							= reqvar("orig_first_four", "");
		$last_four							= reqvar("orig_last_four", "");
	
		$card_amount						= reqvar("card_amount", "");
		$tip_amount							= reqvar("refund_tip_amount", "");
		if (empty($tip_amount)) $tip_amount	= reqvar("tip_amount", "");

		$app_name							= reqvar("app_name", "POS Lavu");
		$app_version						= reqvar("app_version", "");
		$app_build							= reqvar("app_build", "");
		if (empty($app_version)) {
			$app_version					= reqvar("version", "2.0");
			$app_build						= reqvar("build", "20110826-1");
		}

		$mtoken 								= reqvar("record_no", "");
		if (empty($mtoken)) $mtoken			= reqvar("mtoken", "");
		$mtoken = str_replace("[PLUS]", "+", $mtoken);

		$device_time						= reqvar("device_time", date("Y-m-d H:i:s", time()));
		if ($location_info['timezone'] != "") {
			$device_time					= localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
		}
		
		$db_name			= "poslavu_".$data_name."_db";
		$integration_data	= get_integration_from_location($loc_id, $db_name);
		$frequency			= "OneTime";
		$method				= "CreditReturnToken";

		$postvars = array();
		$postvars[] = array("MerchantID", $integration_data['integration3']); // 494691720
		$postvars[] = array("PurchaseAmount", ($card_amount + $tip_amount));
		$postvars[] = array("Invoice", str_replace("-", "", $loc_id.$order_id));
		$postvars[] = array("TerminalName", $register);
		$postvars[] = array("OperatorID", $server_id);
		$postvars[] = array("Memo", $app_name." v".$app_version);
		$postvars[] = array("Token", $mtoken);
		$postvars[] = array("Frequency", $frequency);	
		
		$debug .= "\n\n".print_r($postvars, true);
		
		$get_transaction_info = lavu_query("SELECT `id`, `check`, `action`, `amount`, `tip_amount`, `pay_type`, `card_type`, `first_four`, `card_desc` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `transaction_id` = '[3]'", $loc_id, $order_id, $pnref);
		$tx_info = mysqli_fetch_array($get_transaction_info);
	
		if (empty($card_type)) $card_type = $tx_info['card_type'];
		if (empty($first_four)) $first_four = $tx_info['first_four'];
		if (empty($last_four)) $last_four = $tx_info['card_desc'];
				
		$curl_result = post_to_mercury_tws($postvars, $data_name, $loc_id, $transtype, $method, $integration_data['integration4']);

		$debug = "\n\n".$curl_result;
		
		$status				= extract_value($curl_result, "Status");
		$message			= extract_value($curl_result, "Message");
		$new_pnref			= extract_value($curl_result, "RefNo");
		$new_authcode		= extract_value($curl_result, "AuthCode");
		$new_ref_data		= extract_value($curl_result, "AcqRefData");
		$new_process_data	= extract_value($curl_result, "ProcessData");
		$new_token			= extract_value($curl_result, "Token");

		$lg_str = "MERCURY HOSTED CHECKOUT[--CR--]";
		$lg_str .= "LOCATION: ".		$loc_id			." - ";
		$lg_str .= "REGISTER: ".		$register		." - ";
		$lg_str .= "SERVER ID: ".	$server_id		." - ";
		$lg_str .= "ORDER ID: ".		$order_id		." - ";
		$lg_str .= "CHECK : ".		$check			." - ";
		$lg_str .= "AMOUNT: ".		$card_amount	." - ";
		$lg_str .= "TRANSTYPE: ".	$transtype		." - ";
		$lg_str .= "REFERENCE #: ".	$new_pnref		." - ";
		$lg_str .= "AUTHCODE: ".		$new_authcode	." - ";
		$lg_str .= "CARD TYPE: ".	$card_type		." - ";
		$lg_str .= "LAST FOUR: ".	$last_four		."[--CR--]";
		$lg_str .= "STATUS: ".		$status			."[--CR--]";
		$lg_str .= "MESSAGE: ".		$message		."[--CR--]";
		write_log_entry("gateway", $data_name, $lg_str."\n");
			
		$approved = ($status == "Approved")?"1":"0";
		if ($send_debug_messages == "1") gateway_debug($data_name, $loc_id, "MPSHC", "RETURN", $approved, $debug);
	
		if ($approved == "1") {
		
			$last_mod_ts = time();
		
			$t_vars = array();
			$t_vars['loc_id']			= $loc_id;
			$t_vars['order_id']			= $order_id;
			$t_vars['check']				= $check;
			$t_vars['datetime']			= $device_time;
			$t_vars['amount']			= number_format($card_amount, $decimal_places, ".", "");
			$t_vars['tip_amount']		= number_format($tip_amount, $decimal_places, ".", "");
			$t_vars['total_collected']	= number_format($card_amount, $decimal_places, ".", "");
			$t_vars['card_type']			= $card_type;
			$t_vars['first_four']		= $first_four;			
			$t_vars['card_desc']			= $last_four;
			$t_vars['transaction_id']	= $new_pnref;
			$t_vars['auth_code']			= $new_authcode;
			$t_vars['pay_type']			= $pay_type;
			$t_vars['pay_type_id']		= $pay_type_id;		
			$t_vars['register']			= $register;
			$t_vars['register_name']		= $register_name;
			$t_vars['transtype']			= $method;
			$t_vars['action']			= "Refund";
			$t_vars['got_response']		= "1";
			$t_vars['server_id']			= $server_id;
			$t_vars['server_name']		= $server_name;
			$t_vars['ref_data']			= $new_ref_data;
			$t_vars['process_data']		= $new_process_data;
			$t_vars['record_no']			= $new_token;
			$t_vars['mpshc_pid']			= "HC";
			$t_vars['for_deposit']		= $for_deposit;
			$t_vars['is_deposit']		= $is_deposit;
			$t_vars['ioid']				= $internal_order_id;
			$t_vars['internal_id']		= $internal_tx_id;
			$t_vars['gateway']			= "Mercury";
			$t_vars['reader']			= "mpshc";
			$t_vars['device_udid']		= $device_udid;
			$t_vars['last_mod_ts']		= $last_mod_ts;
			$q_fields = "";
			$q_values = "";
			$keys = array_keys($t_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$record_refund = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $t_vars);
			
			$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` - [1]), `refunded_cc` = (`refunded_cc` + [1]), `void` = '0', `last_mod_ts` = '[2]', `pushed_ts` = '[2]', `last_modified` = '[3]', `last_mod_device` = '[4]' WHERE `location_id` = '[5]' AND `order_id` = '[6]'", $card_amount, $last_mod_ts, $device_time, $device_udid, $loc_id, $order_id);
			$card_amount = str_replace("'", "", $card_amount);
			$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` - [1]), `refund_cc` = (`refund_cc` + [1]) WHERE `loc_id` = '[2]' AND `order_id` = '[3]' AND `check` = '[4]'", $card_amount, $loc_id, $order_id, $check);

			$process_info = array(
				"loc_id"		=> $loc_id,
				"order_id"		=> $order_id,
				"check"			=> $check,
				"device_time"	=> $device_time,
				"server_name"	=> $server_name,
				"server_id"		=> $server_id,
				"device_udid"	=> $device_udid
			);
				
			actionLogItGW($process_info, "Refund Applied", "Check ".$check." - ".$pay_type." - ".priceFormatGW($card_amount));
	
			$rtn = array();
			$rtn[] = "1"; // 0
			$rtn[] = $new_pnref;
			$rtn[] = $last_four;
			$rtn[] = $message;
			$rtn[] = $new_authcode;
			$rtn[] = $card_type;
			$rtn[] = $new_token;
			$rtn[] = $card_amount;
			$rtn[] = $order_id;
			$rtn[] = $new_ref_data;
			$rtn[] = rawurlencode($new_process_data); // 10
			$rtn[] = "";
			$rtn[] = "";
			$rtn[] = $first_four;
			$rtn[] = $last_mod_ts;
			
			echo implode("|", $rtn);
			exit;
		
		} else {

			$rtn = array();
			$rtn[] = "0";
			$rtn[] = $status." - ".$message;
	
			echo implode("|", $rtn);		
			exit();
		}
	}
?>
