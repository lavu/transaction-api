<?php

require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

putenv("TZ=America/Chicago");

if (!sessvar_isset('ENABLED_REDIS')) {
	isCacheEnabled();
}

function log_comm_vars($company_code, $got_loc_id, $caller)
{
	// For details related to Lavu's logging system, refer to http://wiki.lavu/Server_Logs

	if (empty($company_code))
	{
		return;
	}

	$use_date_time	= MountainDateTime();
	$use_dt_parts	= explode(" ", $use_date_time);
	$use_date		= $use_dt_parts[0];
	$req_src		= reliableRemoteIP();

	$str = number_format(microtime(TRUE), 1, ".", "")." - ".$use_date_time." Mountain - Request from ".$req_src." (".$caller.")";

	if ($got_loc_id)
	{
		$str .= " - LOCATION: ".$got_loc_id;
	}

	ksort($_GET);
	ksort($_POST);

	$getvars = "";
	foreach ($_GET as $key => $val)
	{
		if ($getvars != "")
		{
			$getvars .= "&";
		}
		$getvars .= $key."=".$val;
	}

	if (!empty($getvars))
	{
		$str .= "[--CR--][--CR--]Getvars: ".$getvars;
	}

	$postvars = "";
	foreach ($_POST as $key => $val)
	{
		if ($postvars != "")
		{
			$postvars .= "&";
		}

		switch ($key) // Redact sensitive credit card info
		{
			case "mag_data":

				if ($_POST['encrypted']=="0" && !empty($val))
				{
					$val = "[UNENCRYPTED_SWIPE]";
				}
				break;

			case "card_number":

				if (!empty($val))
				{
					$val = "[MANUAL_ENTRY]";
				}
				break;

			case "card_expiration":

				if (!empty($val))
				{
					$val = "[EXP]";
				}
				break;

			case "CVN":

				if (!empty($val)) {
					$val = "[CVN]";
				}
				break;

			default:

				break;
		}

		$postvars .= $key."=".$val;
	}

	if (!empty($postvars))
	{
		$str .= "[--CR--][--CR--]Postvars: " . $postvars;
	}

	if (count($_FILES) > 0)
	{
		$filevars = json_encode($_FILES, TRUE);
		$str .= "[--CR--][--CR--]Files: ".$filevars;
	}

	$str .= "[--CR--][--CR--]\n";

	$logdir		= "/home/poslavu/logs/company/".$company_code."/operational";
	$fileroot	= (in_array($caller, array( "gateway", "mpshc", "save_sig" )))?"gateway_vars":"json_vars";
	$filename	= $fileroot."_".$use_date.".log";
	$full_path	= $logdir."/".$filename;

	if (!is_dir($logdir))
	{
		mkdir($logdir, 0755, TRUE);
	}

	if (is_dir($logdir))
	{
		file_put_contents($full_path, str_replace('"', '&quot;', $str), FILE_APPEND);
		chmod($full_path, 0775);
	}

	return $full_path;
}

function allow_connection($company_id, $data_name, $c_info_from_session)
{
	$allow = TRUE;

	$fn = "connection_control.txt";
	if (file_exists($fn))
	{
		$fp = fopen($fn, "r");
		$concon_info = fgets($fp);
		fclose($fp);

		if (strpos($concon_info, "|"))
		{
			$cci = explode("|", trim($concon_info));
			$type = $cci[0];
			if ($type == "MAX_CID")
			{
				$max_cid = (int)$cci[1];
				$allow = ((int)$company_id <= (int)$max_cid);
			}
			else if ($type == "START_TS")
			{
				$ts = $cci[1];
				$priority_cids = $cci[2];
				if ($ts == "now")
				{
					$ts = time();
					$fp = fopen($fn, "w");
					fputs($fp, "START_TS|".$ts."|".$priority_cids);
					fclose($fp);
				}
				$max_cid = (16 * (time() - $ts + 1));
				$pcids = explode(",", $priority_cids);
				$allow = (($company_id <= $max_cid) || in_array($company_id, $pcids));
			}

			if ($allow && !$c_info_from_session)
			{
				error_log("allowing new app connection: ".$data_name." (".$company_id.")");
			}
		}
	}

	return $allow;
}

$build_number = "";
$company_code = "";
if (isset($_REQUEST['cc']))
{
	$company_code = $_REQUEST['cc'];
}
else if (isset($_SESSION['company_code']))
{
	$company_code = $_SESSION['company_code'];
}

$hold_cc = $_REQUEST['cc'];

mlavu_select_db("poslavu_MAIN_db");

$ccrt				= lsecurity_pass($company_code);
$company_code		= $ccrt['cc'];
$company_code_key	= $ccrt['key'];

if (isset($_POST['cc']))
{
	$ccrt = lsecurity_pass($_POST['cc']);
	$_POST['cc'] = $ccrt['cc'];
}

if (isset($_GET['cc']))
{
	$ccrt = lsecurity_pass($_GET['cc']);
	$_GET['cc'] = $ccrt['cc'];
}

if (isset($_REQUEST['cc']))
{
	$ccrt = lsecurity_pass($_REQUEST['cc']);
	$_REQUEST['cc'] = $ccrt['cc'];
}

$server_order_prefix	= 1;
$got_loc_id				= reqvar("loc_id", FALSE);
$recvd_sess_id			= reqvar("PHPSESSID", "");
$jc_mode				= (string)reqvar("m", "");

$cc_required = TRUE;
if ($jc_mode=="44" && reqvar("pack_id", "")!="0")
{
	$cc_required = FALSE;
}

$cc_companyid = 0;
if ($company_code!="NEEDTOGET" && $company_code!="" && $cc_required)
{
	if (!isset($mode))
	{
		$mode = "";
	}

	$company_info = array();
	$c_info_from_session = FALSE;
	if (sessvar("company_info") && !in_array($mode, array( "2", "load_location_list" )))
	{
		$company_info = sessvar("company_info");
		$c_info_from_session = ($company_info['data_name'] == $company_code);
	}

	if (!$c_info_from_session)
	{
		set_sessvar("location_info", FALSE);

		$data_name = $company_code;
		$get_company_info = mlavu_query("SELECT `poslavu_MAIN_db`.`restaurants`.*, `poslavu_MAIN_db`.`signups`.`package` AS `signup_package`, `poslavu_MAIN_db`.`payment_status`.`current_package` AS `package_status` FROM `poslavu_MAIN_db`.`restaurants` LEFT JOIN `poslavu_MAIN_db`.`payment_status` ON `poslavu_MAIN_db`.`payment_status`.`restaurantid` = CONCAT(`poslavu_MAIN_db`.`restaurants`.`id`,'') LEFT JOIN `poslavu_MAIN_db`.`signups` ON `poslavu_MAIN_db`.`signups`.`dataname` = `poslavu_MAIN_db`.`restaurants`.`data_name` WHERE `poslavu_MAIN_db`.`restaurants`.`company_code` = '[1]'", $company_code, NULL, NULL, NULL, NULL, NULL, FALSE);
		if (@mysqli_num_rows($get_company_info) > 0)
		{
			$company_info = mysqli_fetch_assoc($get_company_info);

			set_sessvar("company_info", $company_info);
		}
	}

	if (!$company_info || (count($company_info) <= 0))
	{
		if ($json_session)
		{
			echo '{"json_status":"NoCC"}';
		}
		else
		{
			// Set header status 500 because The server encountered an unexpected condition which prevented it from fulfilling the request.
			// Ref. LP-11989
			header('HTTP/1.0 500 Internal Server Error');
			echo "Error: No record found for company code: ".$_SESSION['company_code']."<br /><br /><a href='/index.php'>Back to Log In</a>";
		}

		lavu_exit();
	}

	$data_name = $company_info['data_name'];
	$cc_companyid = $company_info['id'];
	if (!allow_connection($cc_companyid, $data_name, $c_info_from_session))
	{
		lavu_exit();
	}

	lavu_connect_dn($company_info['data_name'], "poslavu_".$data_name."_db");

	require_once(__DIR__."/../cp/resources/log_process_info.php");

	lavu_write_process_info(array());

	$loc_id = reqvar("loc_id", FALSE);
	if (!$loc_id && isset($_REQUEST['loc_id']))
	{
		$loc_id = $_REQUEST['loc_id']; // reqvar may not properly pull manually set $_REQUEST keys
	}

	if (!isset($force_load_location))
	{
		$force_load_location = FALSE;
	}

	if ($loc_id)
	{
		$l_info_from_session = FALSE;

		if (!$force_load_location && sessvar("location_info") && !in_array($jc_mode, array( "2", "load_location_list", "3", "load_location", "26", "reload_settings" )))
		{
			$location_info = sessvar("location_info");
			$l_info_from_session = ($location_info['data_name']==$data_name && $location_info['id']==$loc_id);
		}

		if (!$l_info_from_session)
		{
			$get_location_info = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $loc_id, NULL, NULL, NULL, NULL, NULL, FALSE);
			$location_info = mysqli_fetch_assoc($get_location_info);

			$companyInfo = sessvar('company_info');
			$companyId = $companyInfo['id'];
			$get_location_config = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `type` = 'location_config_setting' AND `location` = '[1]' OR `location` = '[2]'", $loc_id, $companyId, NULL, NULL, NULL, NULL, FALSE);
			while ($config_info = mysqli_fetch_assoc($get_location_config))
			{
				$setting	= $config_info['setting'];
				$value		= $config_info['value'];

				$location_info[$setting] = $value;
			}

			$location_info['data_name'] = $data_name;

			set_sessvar("location_info", $location_info);
		}

		$decimal_places = locationSetting("disable_decimal", 2);
	}
}

function getTotalTips($dateInfo, $locationId, $empClassId, $dataName, $userarr = array()) {
	if(empty($userarr)) {
		$cond = "SELECT `users`.`id` FROM $dataName.`users` LEFT JOIN $dataName.`clock_punches` on  `clock_punches`.`server_id` = `users`.`id` WHERE `clock_punches`.`role_id` = '".$empClassId."' AND `clock_punches`.`punched_out` = 0 AND `users`.`_deleted` = 0 group by `users`.`id`";
	} else {
		$implodeUser = implode(',', $userarr);
		$cond = $implodeUser;
	}
	$tips_query = mlavu_query("select SUM(`orders`.`cash_tip`) as 'total_cashtip',SUM(`orders`.`card_gratuity`) as 'total_cardtip' from $dataName.`orders` LEFT JOIN $dataName.`clock_punches` as cp ON `cp`.`server_id`= `orders`.`server_id` where `orders`.`location_id` =".$locationId." AND `orders`.`server_id` in ($cond) AND `orders`.`closed` >= '".$dateInfo[2]."' AND `orders`.`closed` < '".$dateInfo[3]."' AND `orders`.`closed` >= `cp`.`time` AND `orders`.`closed` < if(`cp`.`time_out` <> '', `cp`.`time_out`, '".$dateInfo[3]."') AND `cp`.`time` >= '".$dateInfo[2]."' AND `cp`.`time` < '".$dateInfo[3]."' AND `orders`.`void` = '0' AND `cp`.`role_id`=".$empClassId." AND `cp`.`punched_out` = 0");
	
	$rows = mysqli_fetch_assoc($tips_query);
	$getClockoutUserTip = getNosaleClockoutUserTip($dateInfo, $empClassId, $dataName);
    $totalTip = $rows['total_cashtip'] + $rows['total_cardtip'] - $getClockoutUserTip;
    return $totalTip;
}

function getUserTips($dateInfo, $locationId, $userId, $dataName, $empClassId) {
	$tips_query = mlavu_query("select SUM(`orders`.`cash_tip`) as 'cashtip',SUM(`orders`.`card_gratuity`) as 'cardtip' from $dataName.`orders` LEFT JOIN $dataName.`clock_punches` as cp ON `cp`.`server_id`= `orders`.`server_id` where `orders`.`location_id` =".$locationId." AND `orders`.`server_id` in ($userId) AND `orders`.`closed` >= '".$dateInfo[2]."' AND `orders`.`closed` < '".$dateInfo[3]."' AND `orders`.`closed` >= `cp`.`time` AND `orders`.`closed` < if(`cp`.`time_out` <> '', `cp`.`time_out`, '".$dateInfo[3]."') AND `cp`.`time` >= '".$dateInfo[2]."' AND `cp`.`time` < '".$dateInfo[3]."'AND `orders`.`void` = '0' AND `cp`.`role_id`=".$empClassId." AND `cp`.`punched_out` = 0");

    $rowsTip = mysqli_fetch_assoc($tips_query);
    $card_tip = $rowsTip['cardtip'];
    $cash_tip = $rowsTip['cashtip'];
    $total_tip = $card_tip + $cash_tip;
    $tipvalues = array('cardtip'=>$card_tip,'cashtip'=>$cash_tip,'totalTip'=>$total_tip);
    return $tipvalues;
}

function getTotalTipout($uid, $createdDate, $locationId, $classId, $dataName) {
    $tipout_query = mlavu_query("select  SUM(`value`) as 'cashtipout',SUM(`value2`) as 'cardtipout' from $dataName.`tipouts` where `server_id` ='".$uid."' and `date` = '".$createdDate."' AND `loc_id` = '".$locationId."' AND `emp_class_id` = '".$classId."' ");
    $rowsTipout = mysqli_fetch_assoc($tipout_query);
    $cashTipout = $rowsTipout['cashtipout'];
    $cardTipout = $rowsTipout['cardtipout'];
    $tipOut = $cashTipout+$cardTipout;
    return $tipOut;
    
}

function insertIntoSharingLog($createdDate, $locationId, $uid, $classId,$day, $dataName) {
    $today_day = date('l');
   // if ($day == $today_day) {
        $select_query_weekly = mlavu_query("select `id`,`server_id`,`tip_sharing_rule`,`time_for_tip_sharing`,`hours_worked`, `sales`, `card_tip`, `cash_tip`,`total_tip`, `tip_out`, `tip_in`, `tip_due`, `tip_pool`, `created_date`,`week_end_date`,`server_name`, `emp_class_name` from $dataName.tip_sharing where location_id='".$locationId."' AND server_id='".$uid."' AND `emp_class_id` = '".$classId."' AND `week_end_date` >= '".$createdDate."'   ");
        $pre_row_count = mysqli_num_rows($select_query_weekly);
        if ($pre_row_count > 0) {
            $sale_data_weekly = mysqli_fetch_assoc($select_query_weekly);
            $pre_tip_sharing_rule = $sale_data_weekly['tip_sharing_rule'];
            $pre_time_for_tip_sharing = $sale_data_weekly['time_for_tip_sharing'];
            $pre_sales = $sale_data_weekly['sales'];
            $pre_card_tip = $sale_data_weekly['card_tip'];
            $pre_cash_tip = $sale_data_weekly['cash_tip'];
            $pre_total_tip = $sale_data_weekly['total_tip'];
            $pre_tip_out = $sale_data_weekly['tip_out'];
            $pre_tip_in = $sale_data_weekly['tip_in'];
            $pre_tip_due = $sale_data_weekly['tip_due'];
            $pre_tip_pool = $sale_data_weekly['tip_pool'];
            $pre_created_date = $sale_data_weekly['created_date'];
            $pre_week_end_date = $sale_data_weekly['week_end_date'];
            $server_name_user = $sale_data_weekly['server_name'];
            $class_name = $sale_data_weekly['emp_class_name'];
            mlavu_query("INSERT INTO $dataName.`tip_sharing_log` (`location_id`, `server_id`, `server_name`, `emp_class_id`,`emp_class_name`, `tip_sharing_rule`,`time_for_tip_sharing`, `hours_worked`, `sales`, `card_tip`, `cash_tip`,`total_tip`, `tip_out`, `tip_in`, `tip_due`, `tip_pool`, `created_date`, `week_end_date`) VALUES ('".$locationId."', '".$uid."', '".$server_name_user."', '".$classId."', '".$class_name."', '".$pre_tip_sharing_rule."', '".$pre_time_for_tip_sharing."', '".$hours_worked."', '".$pre_sales."', '".$pre_card_tip."', '".$pre_cash_tip."', '".$pre_total_tip."', '".$pre_tip_out."','".$pre_tip_in."', '".$pre_tip_due."', '".$pre_tip_pool."', '".$pre_created_date."', '".$pre_week_end_date."') ");
        }
   // }
}

function getUserTime($locationId, $uid, $date_hrs, $tom_Date_hrs, $todayDate, $dataName, $empClassId, $decimalPlaces) {
    $hours_query = mlavu_query("select `clock_punches`.`time`,`clock_punches`.`hours`,`clock_punches`.`time_out` from $dataName.`clock_punches` where `location_id` ='".$locationId."' AND `server_id` = '".$uid."' AND `clock_punches`.`time` >= '".$date_hrs."' AND `clock_punches`.`time` < '".$tom_Date_hrs."' AND `clock_punches`.`role_id` ='".$empClassId."' AND `clock_punches`.`punched_out` = 0");
    $hrs_current_user = 0;
    while ($row_time = mysqli_fetch_assoc($hours_query)) {
        if ($row_time['time_out'] != "") {
            $hrs_current_user += round($row_time['hours'], $decimalPlaces);
        } else {
            $inTime = $row_time['time'];
            $inTime= strtotime($inTime);
            $today= strtotime($todayDate);
            $userHours = round(abs($today-$inTime)/60/60, $decimalPlaces);
            $hrs_current_user += $userHours;
        }
    }
    
    return $hrs_current_user;
}

function tipsCashData($dateInfo, $locationId, $empClassId, $dataName, $userarr = array()) {
	if(empty($userarr)) {
		$cond = "SELECT `id` FROM $dataName.`users` WHERE `employee_class` = '".$empClassId."' AND `_deleted` = 0 ";
	} else {
		$implodeUser = implode(',', $userarr);
		$cond = $implodeUser;
	}

	$tips_query = mlavu_query("SELECT  SUM(`value`) as 'total_cashtip' from $dataName.`cash_data` WHERE `date` >= DATE_FORMAT('".$dateInfo[2]."', '%Y-%m-%d') and `date` < DATE_FORMAT('".$dateInfo[3]."', '%Y-%m-%d') AND `type`='server_cash_tips' AND `_deleted` = 0 AND `loc_id` = '".$locationId."' AND `server_id` in ($cond)");
	$rows = mysqli_fetch_assoc($tips_query);
	$totalTip = $rows['total_cashtip'];
	return $totalTip;
}

/**
* This function is used to fix the issue of rounding value of tip sharing amount.
*
* @since 2019-09-23 : LP-10004
*
* @param integer $totalValue  mismatch value.
* @param integer $numberOfUsers number of users.
* @param integer $decimalPlaces number of decimal places to show.
*
* @return array list of distributedValue for mismatch amount.
*/
function getDistributedValue($totalValue, $numberOfUsers, $decimalPlaces=2) {
	$smallestMoney      = 1/(pow(10,$decimalPlaces)); //smalest number of $decimalPlaces
	$splitPrice          = sprintf("%0.".$decimalPlaces."f",($totalValue / $numberOfUsers)); // till $decimalPlaces
	$splitPriceRemainder = sprintf("%0.".$decimalPlaces."f",($totalValue - ($splitPrice * $numberOfUsers))); // till decimalPlaces
	$isReminderInNegative  = false;

	if ( $splitPriceRemainder < 0 ) {
		$isReminderInNegative = true;
		$splitPriceRemainder = -($splitPriceRemainder);
	}

	$distributedDictionary = array();
	for ( $i = 0 ; $i < $numberOfUsers; $i ++ ) {
		if ( $splitPriceRemainder >= (($i+1)*$smallestMoney)) {
			if ( $isReminderInNegative ){
				$distributedDictionary[$i] = ($splitPrice - $smallestMoney);
			} else {
				$distributedDictionary[$i] = ($splitPrice + $smallestMoney);
			}
		} else {
			$distributedDictionary[$i] = $splitPrice;
		}
	}

	return $distributedDictionary;
}

/**
* This function is used to get total tip of clockout user without sale.
*
* @param array $dateInfo
* @param integer $empClassId
* @param string $dataName
*
* @return integer $tipDue.
*/
function getNosaleClockoutUserTip($dateInfo, $empClassId, $dataName) {
	$tipDue = 0;
	$subQry = "SELECT `users`.`id` FROM $dataName.`users` LEFT JOIN $dataName.`clock_punches` on  `clock_punches`.`server_id` = `users`.`id` WHERE `clock_punches`.`role_id` = '".$empClassId."' AND `clock_punches`.`punched_out` != 0 AND `clock_punches`.`time` >= '".$dateInfo[2]."' AND `clock_punches`.`time` < '".$dateInfo[3]."' AND `users`.`_deleted` = 0 group by `users`.`id`";
	$tipDueQuery = mlavu_query("SELECT SUM(`tip_due`) AS 'total_tip_due' FROM $dataName.`tip_sharing` WHERE `ts`.`sales` = 0 AND `server_id` IN ($subQry) AND `created_date` = DATE_FORMAT('".$dateInfo[2]."', '%Y-%m-%d')");
	if (mysqli_num_rows($tipDueQuery)) {
		$tipDueInfo = mysqli_fetch_assoc($tipDueQuery);
		$tipDue = $tipDueInfo['total_tip_due'];
	}
	return $tipDue;
}
