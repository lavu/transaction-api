<?php
	ini_set('display_errors',1);
	
	/**
	 * PASSWORD FUNCTIONS
	 * This file allows you to complete actions using the password_token within the customer_accounts
	 * table of the main database.  Please note that any functional use of the token will destroy it
	 * from the customer_accounts table.  This is a security measure meant to prevent repeated access/
	 * use of the same token multiple times.
	 *
	 * In order for this file to do anything, it needs an action, and a token.  Otherwise the result
	 * should do nothing.
	 *
	 * RECEIVES
	 * 
	 * action:  This is an action that specifies what to do once the token has been authenticated.
	 *
	 * token :  The token used to find the user. 
	 *
	 * 
	 */
	 
	 /**
	 * Used for lavu_query.php
	 */
	 require_once("../cp/resources/core_functions.php");
	 
	 /******************************************************************************
	 * FUNCTIONS
	 ******************************************************************************/
	 
	 function leave($msg)
	 {
		 echo $msg;
		 exit();
	 }
	 
	 function destroyToken()
	 {
	 	global $token;
	 	if($token != "")
	 	{
			$destroy_query = "UPDATE `poslavu_MAIN_db`.`customer_accounts` SET `password_token`=NULL WHERE `password_token`='[1]'";
			mlavu_query($destroy_query, $token);
		}
	 }
	 
	 function resetPassword()
	 {
		 global $token;
		 
		 $username = "";
		 $dataname = "";
		 {
			 $user_query = "SELECT * FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `password_token`='[1]'";
			 $user_result = mlavu_query($user_query, $token);
			 
			 if(!ConnectionHub::getConn('poslavu')->affectedRows())
			 	leave("This request URL no longer Exists.  Perhaps another request was generated after the initial request.");
			 	
			 $user_array = mysqli_fetch_assoc($user_result);
			 
			 $username = $user_array['username'];
			 $dataname = $user_array['dataname'];
			 
			 mysqli_free_result($user_result);
		 }
		 
		 if($dataname == "" || $username == "")
		 	leave("Unable to Proceed, unable to find User Name or Company.");
		 	
			?>
			
			<script type='text/javascript'>
				function handleResetRequest(result) {
					var pwra = document.getElementById("pwresetarea");
					if (result) {
						if (result.status == "success") pwra.innerHTML = "<form action='https://admin.poslavu.com'><input type='submit' value='Click Here To Goto the Backend' /></form>";
						else pwra.innerHTML= "Password Reset Failed: " + result.error;
					} else pwra.innerHTML = "No response... Please try again.";		
				}
			</script>
		 
			<?php
		 
		 session_start();
		 $_SESSION['verified']=1;
		 $reset_script_path = "password/pwreset.php";
		 require_once(dirname(__FILE__)."/password/password_reset_form.php");	 	
		 
		 destroyToken();
		 //leave("Password Succesfully Reset, Please check Your Email for Your New Password.");
	 }
	 
	 /******************************************************************************
	 * START
	 ******************************************************************************/
	 
	 if(!isset($_REQUEST['token']) || !isset($_REQUEST['action']))
	 	leave("Token or Action was Not Supplied.");
	 	
	 $action = $_REQUEST['action'];
	 $token  = $_REQUEST['token'];
	 
	 if($action == 'reset')
	 {
		 resetPassword();
	 }
	 else if ($action == 'test')
	 {
	 	leave('test with great success!.');
	 }
?>
