<?php
  
session_start();

header("Content-type: application/json");

$mode = (isset($_REQUEST['m']))?$_REQUEST['m']:"";
require_once(__DIR__."/NewRelicHelper.php");

$NewRelic = new NewRelicHelper();
$NewRelic->trackRequestStart();
$NewRelic->track("jc_mode", $mode);
$NewRelic->track("client_ip", getClientIPServer());

$high_memory_modes = array(
	2,
	"load_location_list",
	3,
	"load_location",
	4,
	7,
	8,
	26,
	"reload_settings",
	30,
	47,
	"device_check_in"
);

if (in_array($mode, $high_memory_modes))
{
	ini_set("memory_limit", "64M");
}
ini_set("display_errors", 0);
ini_set("log_errors", 1);
$json_session = TRUE;

require_once(__DIR__."/info_inc.php");
require_once(__DIR__."/ml_connect.php");

if (!empty($data_name))
{
	$NewRelic->track("dataname", $data_name);
}

if ($company_code!="NEEDTOGET" && $cc_required)
{
	$company_id		= $company_info['id']; // determined by info_inc.php
	$lsecurity_id	= lsecurity_id($company_code_key, $data_name);
	$lsecurity_id1	= lsecurity_id1($company_code_key);

	if ($company_id!=$lsecurity_id && $company_id!=$lsecurity_id1)
	{
		session_destroy();
		lavu_exit();
	}
}

$jc_inc = "jc_inc.php";

$usejcinc = reqvar("usejcinc", "");
if (!empty($usejcinc))
{
	$jc_inc = "jc_inc".$usejcinc.".php";
}
else
{
	$compare_bn = (int)str_replace("-", "", $build_number);

	if ($compare_bn == 20113211)
	{
		$jc_inc = "jc_inc1.php";
	}
	else if ($compare_bn >= 201108011)
	{
		$jc_inc = "jc_inc6.php";
	}
	else if ($compare_bn >= 201105241)
	{
		$jc_inc = "jc_inc5.php";
	}
	else if ($compare_bn >= 201105061)
	{
		$jc_inc = "jc_inc4.php";
	}
	else if ($compare_bn >= 201104071)
	{
		$jc_inc = "jc_inc3.php";
	}
	else if ($compare_bn >= 201103241)
	{
		$jc_inc = "jc_inc2.php";
	}
	else if ($compare_bn >= 201102221)
	{
		$jc_inc = "jc_inc1.php";
	}
}

if (!isset($server_order_prefix))
{
	$server_order_prefix = 1;
}

if (isset($_REQUEST['use_server_prefix']))
{
	$server_order_prefix = $_REQUEST['use_server_prefix'];
}
require_once(__DIR__."/jcvs/".$jc_inc);

lavu_exit(); // calls NewRelicHelper's trackRequestEnd()