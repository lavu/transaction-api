<?
/**
 * Created by PhpStorm.
 * User: Hayat
 * Date: 04/24/17
 * Time: 3:26 PM
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Batch Settlement</title>
		<link rel="stylesheet" type="text/css" href="/cp/styles/styles.css">
		<style>
			body { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; }
			.cct_labels { color:#999999; font-size:11px; }
			.cct_info { color:#666666; font-size:11px; }
			.error_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#CC0000; }
			.info_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; }
			.paging_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:19px; padding:3px 15px 3px 15px; }
			.small_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; }
			.tbot { border-top:solid 2px #777777; text-align:right; font-weight:bold; }
			.title_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:17px; color:#000000; }
			.ttop { border-bottom:solid 2px #777777; text-align:center; font-weight:bold; }

			.tbl-qa {
				width: 98%;
				margin-top: 20px;
				font-size: 1em;
				border: solid 2px #aecd37;
			}
			.btn_light_long {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
				width:200px;
				height:37px;
				background:url("images/btn_wow_200x37.png");
				border:hidden;
				padding-top:2px;
			}
			.enabledBtn {
				border-radius: 2px;
				-webkit-border-radius: 2px;
				border:0 none !important;

				width:10em;
				height:3em;

				font-family: sans-serif, verdana;
				font-weight: 600;
				font-size: 1em;
				color: #fff;
				text-transform: uppercase;
				text-shadow: 0px 1px 3px #999;

				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#aecd37+0,99bc08+51,99bc08+54,aecd37+100 */
				background: #aecd37; /* Old browsers */
				background: -moz-linear-gradient(top,  #aecd37 0%, #99bc08 51%, #99bc08 54%, #aecd37 100%); /* FF3.6-15 */
				background: -webkit-linear-gradient(top,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom,  #aecd37 0%,#99bc08 51%,#99bc08 54%,#aecd37 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#aecd37', endColorstr='#aecd37',GradientType=0 ); /* IE6-9 */

				transition:all 0.3s;
			}
			.disabledBtn {
				border-radius: 2px;
				-webkit-border-radius: 2px;
				border:0 none !important;

				width:10em;
				height:3em;

				font-family: sans-serif, verdana;
				font-weight: 600;
				font-size: 1em;
				color: #fff;
				text-transform: uppercase;
				text-shadow: 0px 1px 3px #999;

				/* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#c6000d+0,fc0019+51,fc0019+54,c6000d+100 */
				background: #ada7a7; /* Old browsers */
				background: -moz-linear-gradient(top, #ada7a7 0%, #ada7a7 51%, #ada7a7 54%, #ada7a7 100%); /* FF3.6-15 */
				background: -webkit-linear-gradient(top, #ada7a7 0%,#ada7a7 51%,#ada7a7 54%,#ada7a7 100%); /* Chrome10-25,Safari5.1-6 */
				background: linear-gradient(to bottom, #ada7a7 0%,#ada7a7 51%,#ada7a7 54%,#ada7a7 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c6000d', endColorstr='#ada7a7',GradientType=0 ); /* IE6-9 */

				transition:all 0.3s;
			}
			.iFrameOverlay {
				display:none;
				position:fixed;
				top:0;
				left:0;
				width:100%;
				height:100%;
				background-color:#EFEFEF;
				opacity:0.99;
			}
			.ajax-action-button  img{
				color: #09F;
				margin: 10px 0px;
				cursor: pointer;
				display: inline-block;
				padding: 10px 20px;
			}

			.button {
				background: #98B624;
				border-radius: 0px;
				color: white;
			}

		</style>
			<script src="../cp/scripts/ajax_prototype.js"></script>
			<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"> </script>
			<script language="javascript">
				function continueSettlement() {
					document.getElementById("batch_form").submit();
				}
			</script>
	</head>

<body>
<div id='eConduitOverlay' class='iFrameOverlay'>
	<button class='btn_light_long' ontouchstart='hideActivityShade();'>
		<b>Close Overlay</b>
	</button>
</div>
<?php
/**
 *  This file is used to get pre-auth records which is not settle yet.
*/
ini_set("display_errors",0);
session_start();
require_once(__DIR__."/info_inc.php");
require_once(__DIR__."/gateway_functions.php");
require_once(__DIR__."/webview_special_functions.php");
$location_info = !empty($location_info) ? $location_info : sessvar('location_info');
if (empty($location_info)) {
	exit();
}

$display			= "";

$loc_id				= reqvar("loc_id", '');
$register			= reqvar("register", '');
$register_name		= reqvar("register_name", '');
$device_time		= reqvar("device_time", '');
$device_id			= reqvar("UUID", '');
$native_bg			= reqvar("native_bg", "0");
$app_name	 		= reqvar("app_name", "POSLavu Client");
$app_version		= reqvar("app_version", '');
$server_name 		= reqvar("server_name", '');

$loc_id = ($loc_id) ? $loc_id : $location_info['id'];

$local_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
$decimal_places = $location_info['disable_decimal'];

if (empty($device_time)) {
	$device_time = date("Y-m-d H:i:s", time());
}
$tz = $location_info['timezone'];
if (!empty($tz)) {
	$device_time = localize_datetime(date("Y-m-d H:i:s"), $tz);
}

$should_post = true;
if (isset($_REQUEST['do_not_post'])) {
	$should_post = ($_REQUEST['do_not_post']=="1") ? false : true;
}

$data_name = ($data_name) ? $data_name : admin_info('dataname');

$locRow = get_location_row();
$resp = '';

$use_gateway = 'LavuPay';
//$use_gateway = 'TGate'; #assign payment gateway name

$companyId = ($company_info['id']) ? $company_info['id'] : admin_info('companyid');
$serverId = ($server_id) ? $server_id : admin_info('loggedin');
$register_name = ($register_name) ? $register_name : 'Back End';
$process_info = array(
	'card_amount'		=> "",
	'card_cvn'			=> "",
	'card_number'		=> "",
	'company_id'		=> $companyId,
	'data_name'			=> $data_name,
	'device_time'		=> $device_time,
	'device_udid'		=> $device_id,
	'exp_month'			=> "",
	'exp_year'			=> "",
	'ext_data'			=> "",
	'for_deposit'		=> "",
	'loc_id'			=> $loc_id,
	'mag_data'			=> "",
	'name_on_card'		=> "",
	'reader'			=> "",
	'register'			=> "Back End",
	'register_name'		=> $register_name,
	'server_id'			=> $serverId,
	'server_name'		=> $server_name,
	'set_pay_type'		=> "Card",
	'set_pay_type_id'	=> "2"
);

$hidden_fields		= "";
$send_alert = "";

#If uncaptured form get submitted
$cardtip_list = postvar("cardtip_list");
$cctip_list = postvar("cctip_list");
$form_post = postvar("form_post");

if ($form_post) {
	if ($location_info['integrateCC'] == "1") {
		$integration_info = get_integration_from_location($loc_id, "poslavu_".$data_name."_db");
		$func_path = __DIR__."/gateway_lib/".strtolower($use_gateway)."_func.php";
		if (file_exists($func_path)) {
			require_once($func_path);
		}
	}
	if ($cardtip_list) {
		$error_encountered = false;
		$cardtip_list = explode("|", $cardtip_list);
		$used_preauth_ids = array(); // safety net to prevent auths from being captured more than once
		$totalTransaction = count($cardtip_list);
		for ($c = 0; $c < $totalTransaction; $c++) {
			$cardtip_identifier = $cardtip_list[$c];
			$a_tip_has_changed = false;

			$set_cardtip = postvar('set_cardtip_'.$cardtip_identifier, "na");
			if ($set_cardtip != "na") {
				$set_cardtip = str_replace("$", "", $set_cardtip);
				$set_cardtip = str_replace(",", ".", $set_cardtip);
				if (is_numeric($set_cardtip) || ($set_cardtip == "") || ($set_cardtip == "multi")) {

					$check_got_response = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";
					$id_parts = explode("_", $cardtip_identifier);
					$where_clause = " `order_id` = '[1]' AND `loc_id` = '[2]'";
					$where_identifier = $id_parts[0];
					if (!empty($id_parts[1])) {
						$where_clause = " `ioid` = '[1]'";
						$where_identifier = $id_parts[1];
					}

					$get_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE ".$where_clause."$check_got_response AND `pay_type_id` = '2' AND `action` = 'Sale' AND `voided` != '1' ORDER BY `check`, `id` ASC", $where_identifier, $loc_id);
					$transaction_count = mysqli_num_rows($get_transactions);
					if ($transaction_count == 1) {
						$transaction_info = mysqli_fetch_assoc($get_transactions);
						$apply_tip =  number_format((float)$set_cardtip, $decimal_places, ".", "");
						if (((float)$apply_tip != (float)$transaction_info['tip_amount']) || ($transaction_info['auth'] == 1) || isset($overpaid_CCs[$cardtip_identifier])) {
							$a_tip_has_changed = true;
							$set_cardtip = $apply_tip;
							lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $apply_tip, time(), $transaction_info['id']);
						}
					} else if ($transaction_count > 1) {
						$tip_total = 0;
						$cctip_list_array = explode("|", $cctip_list);
						$cctipListCount = count($cctip_list_array);
						while ($transaction_info = mysqli_fetch_assoc($get_transactions)) {

							for ($i = 0; $i < $cctipListCount; $i++) {
								$cct_id = $cctip_list_array[$i];

								if ($transaction_info['id'] == $cct_id) {
									$apply_tip = postvar('set_cctip_'.$cct_id, "na");
									if ($apply_tip != "na") {
										$apply_tip = number_format((float)str_replace("$", "", $apply_tip), $decimal_places, ".", "");

										if (((float)$apply_tip != (float)$transaction_info['tip_amount']) || ($transaction_info['auth'] == 1)) {
											$a_tip_has_changed = true;
											$tip_total += $apply_tip;
											lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $apply_tip, time(), $transaction_info['id']);
										} else {
											$tip_total += $transaction_info['tip_amount'];
											if ($location_info['integrateCC'] == "1") {
												$resp .= "<tr><td align='center'>Order #".$transaction_info['order_id']." - ".$transaction_info['card_type']." ...".$transaction_info['card_desc']." - Tip: $apply_tip - Tip unchanged</td></tr>";
											}
										}
									}
								}
							}
						}

						$set_cardtip = $tip_total;
					}

					if ($a_tip_has_changed) {
						$where_clause = " `order_id` = '[4]' AND `location_id` = '[5]'";
						$where_identifier = $id_parts[0];
						if (!empty($id_parts[1])) {
							$where_clause = " `ioid` = '[4]'";
							$where_identifier = $id_parts[1];
						}

						lavu_query("UPDATE `orders` SET `card_gratuity` = '[1]', `last_modified` = '[2]', `last_mod_ts` = '[3]', `pushed_ts` = '[3]', `last_mod_device` = 'END OF DAY' WHERE " . $where_clause, str_replace("'", "''", number_format($set_cardtip, $decimal_places, ".", "")), $local_time, time(), $where_identifier, $loc_id);
					}
				} 
			} 
		}

		if (!$error_encountered) {
			$resp .= "<tr><td align='center'>All card tips successfully updated with no errors.<script language='javascript'>window.location = '_DO:cmd=continue_overlay'</script></td></tr>";
		} 

		if ($return_json) {
			http_response_code($error_encountered ? 400 : 200);
			echo json_encode([
				'message' => $resp,
			]);

			exit();
		}
	}
	
}

#Create web view form for uncaptured transactions
$usersIdName = array();
$preAuth = 1; #set transactio auth value as 1
$user_query = lavu_query('SELECT * FROM `users` WHERE _deleted = 0');
while ($user_read = mysqli_fetch_assoc($user_query)) {
	$usersIdName[$user_read['id']] = trim($user_read['f_name'] . " " . $user_read['l_name']);
}
$cc_pay_type_ids = array("2");
$default_setdate = date("Y-m-d");

$batch_date = (isset($_REQUEST['date']))?$_REQUEST['date']:$default_setdate;
$start_datetime = date('Y-m-d', strtotime ( '-7 days', strtotime($batch_date)  ) ) . " 00:00:00";
//$start_datetime = "2017-06-01 00:00:00";
$end_datetime = $batch_date. " 23:59:59";

$display .= "<script src='../cp/scripts/ajax_prototype.js'></script>";
$display .= "<script>var isEmbeddedWebView = false; function refreshPage() { window.location.reload(); }</script>";
$header = "<tr>
		<td class='ttop'>".speak("Order ID")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Opened")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Table")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("SubTotal")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Tax")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Total")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Due")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Cash Paid")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Card Paid")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Card Tip")."</td><td class='ttop'>&nbsp;</td>
		<td class='ttop'>".speak("Card Total")."</td><td class='ttop'>&nbsp;</td>
	</tr
	";
$display .= '<table align= "center" cellspacing="0" cellpadding="1" width="98%" class="tbl-qa"><tr><td>&nbsp;</td></tr><tr><td>';
$display .= '<table cellspacing="0" cellpadding="1" width="80%" align="center"><tbody><tr><td align="center"><span class="title_text"><b>'.speak("Uncaptured Transaction Alert").'</b></span></td></tr><tr><td>&nbsp;</td></tr>';
$display .= '<tr><td align="center"><span class="small_text">'.speak("This batch contains one or more authorizations that have not been captured. Please review the transaction list below and add any applicable card tips before continuing with batch settlement. Any transaction with no tip amount entered will be captured and settled for the sale amount only").'.</span></td></tr>';
$display .= '<tr><td style=\'border-bottom:1px solid #000000\'>&nbsp;</td></tr><tr><td>&nbsp;</td></tr></tbody></table>';
$display .= "<br><form id='batch_form' name='batch_form' method='post' action=''>";
$display .= '<table cellspacing="0" cellpadding="2" border=0 width="100%">';

#Find uncaptured/unadjusted transactions
$order_query = lavu_query("SELECT `or`.* FROM `orders` as `or` JOIN `cc_transactions` as `cc` on `or`.`order_id` = `cc`.`order_id` WHERE `or`.`order_id` NOT LIKE 'Paid%' AND `or`.`order_id` NOT LIKE '777%' AND `or`.`closed` >= '$start_datetime' AND `or`.`closed` <= '$end_datetime' AND `cc`.`gateway` = '".$use_gateway."' AND `cc`.pay_type_id = 2 AND `cc`.auth = {$preAuth} AND `or`.`void` = '0'  AND `or`.`location_id` = '".$loc_id."' ORDER BY `or`.`closed` ASC");

#check if there is no uncaptured transaction with auth =1 ordered by server
$pending_capture=0;
$pending_capture = mysqli_num_rows($order_query);

if (!$pending_capture) {
	$display .= "<td colspan='16' align='center'>&nbsp;<br>".speak("No Orders Found")."<br>&nbsp;</td>";
}

#Get all uncaptured transaction with auth =1 ordered by server
$serverOrderDetails = array();
while ($order_read = mysqli_fetch_assoc($order_query)) {
	$serverOrderDetails[$order_read['server_id']][] = $order_read; #server wise transaction
}

$order_read = array();
$serverName = '';
$midjs_list     = "";
$cctjs_list     = "";
foreach ($serverOrderDetails as $userId => $orderDetails) {
	$serverName = $usersIdName[$userId] ? $usersIdName[$userId] : "NA";
	$serverName = 'Server - ' . $serverName;
	$displayServerName = '<tr><td colspan="22" align="center">' . $serverName . '</td></tr><tr><td colspan="22" align="center">&nbsp;</td></tr>';
	$display .= $displayServerName;
	$display .= $header;
	$t_subtotal         = 0;
	$t_discount         = 0;
	$t_gift_certificate = 0;
	$t_auto_grat        = 0;
	$t_tax              = 0;
	$t_itax             = 0;
	$t_total            = 0;
	$t_card             = 0;
	$t_due              = 0;
	$t_card_tip         = 0;
	$t_card_total       = 0;
	
	$hidden_fields  = "";

	$row_count = 0;
	$tempOrderId="";
	foreach ($orderDetails as $order_read) {
		$row_count++;
		if($tempOrderId == $order_read['order_id']){
			continue;
		}else{
			$tempOrderId = $order_read['order_id'];
		}
			
		
		$show_multiple_ccts = false;
		$show_single_cct = false;
		$method_paid = "";
		$order_identifier = $order_read['order_id']."_".$order_read['ioid'];
		if ($order_read['cash_paid'] > 0) {
			$method_paid .= "Cash";
		}

		if ($order_read['card_paid'] > 0) {
			if ($method_paid != "") {
				$method_paid .= ", ";
			}
			$method_paid .= "Card";
		}

		if (isset($order_read['cash_applied']) && (string)$order_read['cash_applied'] != "") {
			$cash_applied = ($order_read['cash_applied'] * 1);
		} else if (isset($order_read['cash_paid'])) {
			$cash_applied = ($order_read['cash_paid'] * 1);
		}

		$alt_paid_total = 0;
		$alt_paid_amounts = array();
		$alt_paid_amounts[] = array(speak("Gift Certificate"), display_price($order_read['gift_certificate'], $decimal_places));
		$alt_paid_grand_totals[0] += $order_read['gift_certificate'];
		for ($ap = 1; $ap < count($alt_payment_types); $ap++) {
			$ptype = $alt_payment_types[$ap];
			$this_total = get_alt_total($loc_id, $order_read['order_id'], $ptype[0]);
			$alt_paid_total += $this_total;
			$alt_paid_amounts[] = array($ptype[1], display_price($this_total, $decimal_places));
			$alt_paid_grand_totals[$ap] += $this_total;
		}

		$amount_due = $order_read['total'] - $order_read['gift_certificate'] - $order_read['card_paid'] - $alt_paid_total;
		if (($amount_due > (0 - ($smallest_money / 2))) && ($amount_due < ($smallest_money / 2))) {
			$amount_due = 0;
		}
		$set_cash_tip = number_format((float)$order_read['cash_tip'], $decimal_places, ".", "");
		if ($set_cash_tip == "") {
			$set_cash_tip = 0;
		}
		$set_card_tip = number_format((float)$order_read['card_gratuity'], $decimal_places, ".", "");

		if ($set_card_tip == "") {
			$set_card_tip = 0;
		}

		$t_subtotal         += $order_read['subtotal'];
		$t_discount         += $order_read['discount'];
		$t_tax              += $order_read['tax'];
		$t_itax             += $order_read['itax'];
		$t_total            += $order_read['total'];
		$t_due              += $amount_due;
		$t_gift_certificate	+= $order_read['gift_certificate'];
		$t_cash             += ($order_read['cash_paid'] - $order_read['change_amount']);
		$t_cash_tip         += $set_cash_tip;
		$t_card             += $order_read['card_paid'];
		$t_card_tip         += $set_card_tip;
		$t_card_total       += $order_read['card_paid'] + $set_card_tip;
		$t_auto_grat        += $order_read['gratuity'];

		$gift_color = ($order_read['gift_certificate'] > 0)?"#007841":"#bbbbbb";
		$cash_color = ($order_read['cash_paid'] > 0)?"#00aa00":"#bbbbbb";
		$card_color = ($order_read['card_paid'] > 0)?"#00aa00":"#bbbbbb";

		$display_gift_certificate	= display_price($order_read['gift_certificate'], $decimal_places);
		$display_cash_paid			= display_price($cash_applied, $decimal_places);
		$display_card_paid			= display_price($order_read['card_paid'], $decimal_places);
		$display_card_total			= display_price(($order_read['card_paid'] + $set_card_tip), $decimal_places);
		$display_gift_certificate	= "<font color='$gift_color'>$display_gift_certificate</font>";
		$display_cash_paid			= "<font color='$cash_color'>$display_cash_paid</font>";
		$display_card_paid			= "<font color='$card_color'>$display_card_paid</font>";
		$display_card_total			= "<font color='$card_color'>$display_card_total</font>";

		$row_color = (($row_count % 2) == 0)?"#EDEDED":"#FFFFFF";
		$display .= "<tr bgcolor='".$row_color."'>
			<td class='info_text' align='center'>" . $order_read['order_id'] . "</td><td>&nbsp;</td>
			<td class='info_text' align='right'>" . display_time_($order_read['opened']) . "</td><td>&nbsp;</td>
			<td class='info_text'>" . $order_read['tablename'] . "</td><td>&nbsp;</td>
			<td class='info_text' align='right'><font color='#000055'>" . display_price($order_read['subtotal'], $decimal_places) . "</font></td><td>&nbsp;</td>
			<td class='info_text' align='right'><font color='#005500'>" . display_price($order_read['tax'], $decimal_places) . "</font></td><td>&nbsp;</td>
			<td class='info_text' align='right'><font color='#0000aa'>" . display_price($order_read['total'], $decimal_places) . "</font></td><td>&nbsp;</td>
			<td class='info_text' align='right'>";

		$font_color = ($amount_due > 0)?"#dd0000":"#aaaaaa";
		$cash_tip_value = "";
		if ((float)$set_cash_tip >= $smallest_money) {
			$cash_tip_value = $set_cash_tip;
		}
		$display .= "<font color='$font_color'>" . display_price($amount_due, $decimal_places) . "</font>
			</td>
			<td>&nbsp;</td>
			<td class='info_text' align='right'>" . $display_cash_paid . "</td><td>&nbsp;</td>";
		$display .= "<td class='info_text' align='right'>" . $display_card_paid . "</td><td>&nbsp;</td>";

		if ($cashtipjs_list != "") {
			$cashtipjs_list .= "|";
		}
		$cashtipjs_list .= "$order_identifier";

		$check_got_response = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";

		$where_clause = " `order_id` = '[1]' AND `loc_id` = '[2]'";
		$where_identifier = $order_read['order_id'];
		if (!empty($order_read['ioid'])) {
			$where_clause = 	" `ioid` = '[1]'";
			$where_identifier = $order_read['ioid'];
		}

		$check_cct = lavu_query("SELECT * FROM `cc_transactions` WHERE ".$where_clause." AND `pay_type_id` IN ('".implode("','", $cc_pay_type_ids)."') AND `action` = 'Sale' AND `voided` != '1'$check_got_response AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $where_identifier, $loc_id);
		// create an array of all records
		while ($check = mysqli_fetch_assoc($check_cct)) {
			$all_check_ccts[] = $check;
		}

		// reset the pointer back so we can still access this
		mysqli_data_seek($check_cct, 0);

		if (@mysqli_num_rows($check_cct) > 1) {
			$show_multiple_ccts = true;
			$display .= "<td class='info_text' align='right'>$".$set_card_tip."</td><td>&nbsp;</td>";
			$hidden_fields .= "<input type='hidden' name='set_cardtip_$order_identifier' value='multi'>";
			if ($midjs_list != "") {
				$midjs_list .= "|";
			}
			$midjs_list .= "$order_identifier";
		} else if ((@mysqli_num_rows($check_cct) == 1) || ($order_read['card_paid'] > 0)) {
			$cct_info	= mysqli_fetch_assoc($check_cct);
			$gateway	= $cct_info['gateway'];
			$auth		= $cct_info['auth'];
			$processed	= $cct_info['processed'];

			$show_single_cct = true;
			$display .= "<td class='info_text' align='right' colspan='2'>";
			if ($processed=="1" || $auth==2)
			{
				$display .= "$".$set_card_tip;
			}
			else
			{
			$card_tip_value = "";
			if ((float)$set_card_tip >= $smallest_money) {
				$card_tip_value = $set_card_tip;
			}

			$tipAdjustCallback	= "setFieldValue";
			$lavupay_command	= ($auth == "0")?"tipadjust":"capture";
			$available_DOcmds=array('number_pad');
			$extraArgs = array(
				$cct_info['ref_data'],
				$cct_info['transaction_id'],
				$cct_info['id'],
				$cct_info['amount'],
				$order_read['ioid'],
				currentLocalTime(),
				$loc_id,
				$data_name,
				$set_card_tip,
				$lavupay_command
			);
			$display .= "<input class='info_text' type='tel' ".use_number_pad_if_available("input_card_tip_".$order_identifier, "Card Tip", "money", $tipAdjustCallback, $extraArgs)." size='6' style='text-align:right; font-size:16px' id='input_card_tip_$order_identifier' name='set_cardtip_$order_identifier' value='$card_tip_value' onfocus='if(this.value==\"$0.00\" || this.value==\"0.00\" || this.value==\"0\") this.value = \"\"; '>";
			
			

			if ($midjs_list!="") {
				$midjs_list .= "|";
			}
			$midjs_list .= "$order_identifier";
		}
			$display .= "</td>";
		} else {
			$display .= "<td class='info_text' align='center'>-</td><td>&nbsp;</td>";
		}

		$display .= "<td class='info_text' id='show_card_total_".$order_identifier."' align='right'>".$display_card_total."</td><td>&nbsp;</td>
		</tr>";

		if (mysqli_num_rows($check_cct)) {
			mysqli_data_seek($check_cct, 0);
		}

		if ($show_multiple_ccts || $show_single_cct) {
			$colspan = 20;
			$display .= "<tr bgcolor='".$row_color."'>
				<td colspan='1'>&nbsp</td>
				<td align='right' colspan='".$colspan."'>
					<table cellspacing='0' width='100%' cellpadding='2'>";

			while ($extract = mysqli_fetch_array($check_cct)) {
				
				$gateway	= $extract['gateway'];
				$auth		= $extract['auth'];
				$action		= $extract['action'];
				$processed	= $extract['processed'];
				$tip_value  = number_format((float)$extract['tip_amount'], $decimal_places, ".", "");

				$display .= "<tr>";
				$first_column_content = false;

				if ($auth == "2") {
					$first_column_content = true;
					$display .= "<td class='cct_labels'><font color='#FF3300'>".speak("AuthForTab")."</font></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
				}

				if (!$first_column_content) {
					$display .= "<td></td><td></td>";
				}

				$display .= "<td class='cct_labels'>".(!empty($extract['card_type'])?$extract['card_type']." ...":speak("Card Transaction"))."<span class='cct_info'>".(!empty($extract['card_desc'])?$extract['card_desc']:"")."</span></td><td width='10'></td>";
				$display .= (!empty($extract['transaction_id'])?"<td class='cct_labels'>".speak("Ref #").": <span class='cct_info'>".$extract['transaction_id']."</span></td><td width='10'></td>":"");
				$display .= (!empty($extract['auth_code'])?"<td class='cct_labels'>".speak("Auth Code").": <span class='cct_info'>".$extract['auth_code']."</span></td><td width='10'></td>":"<td></td><td></td>");
				$display .= "<td class='cct_labels' width='100px'>".speak("Amount").": <span class='cct_info'>$".number_format($extract['amount'], 2)."</span></td><td width='1'></td>";

				if ($show_multiple_ccts && ((($processed=="1" || $auth=="2") && $action=="Sale"))) {
					$display .= "<td class='cct_labels'>".$label.": <span class='cct_info'>$".$tip_value."</span></td>";
				} else if ($show_multiple_ccts && $processed=="0" && $action=="Sale") {
						 
					// show editable textfield
					$tipAdjustCallback	=  "setFieldValue" ;
					$lavupay_command	= ($auth == "0")?"tipadjust":"capture";
					$available_DOcmds=array('number_pad');
					$extraArgs = array(
						$extract['ref_data'],
						$extract['transaction_id'],
						$extract['id'],
						$extract['amount'],
						$order_read['ioid'],
						currentLocalTime(),
						$loc_id,
						$data_name,
						$tip_value,
						$lavupay_command
					);

					$display .= "<td class='cct_labels'>".$label.": <input type='tel'".use_number_pad_if_available("input_cctip_".$extract['id'], "Card Tip", "money", $tipAdjustCallback, $extraArgs)." size='6' style='text-align:right; font-size:16px' id='input_cctip_".$extract['id']."' name='set_cctip_".$extract['id']."' onfocus='if(this.value==\"$0.00\" || this.value==\"0.00\" || this.value==\"0\") this.value = \"\"; ' value='".$tip_value."'></td>";

					if ($cctjs_list != "") {
						$cctjs_list .= "|";
					}
					$cctjs_list .= $extract['id'];
				} else {
					$display .= "<td class='cct_labels'></td>";
				}

				$display .= "<td width='5%'>&nbsp;</td></tr>";
				
			}

			$display .= "</table>
				</td><td colspan='4'>&nbsp;</td>
			</tr>";

			
		}
	}
}

$display .= '</table>';
$display .= $hidden_fields."<input type='hidden' name='form_post' value='1'>
			<input type='hidden' name='cardtip_list' value='$midjs_list'>
			<input type='hidden' name='cctip_list' value='$cctjs_list'>";
$display .= '</form>';
$display .= '<table cellspacing="0" cellpadding="1" width="80%" align="center"><tbody>';
$display .= '<tr><td>&nbsp;</td></tr>';

$display .= ($resp) ? $resp : '';
$display .= '<tr><td>&nbsp;</td></tr>';
$display .= '<tr><td colspan="22" align="center"><button class="disabledBtn" style="cursor: pointer" onClick="window.location = `_DO:cmd=close_overlay`;"><b>'.speak("Cancel").'</b></button>&nbsp;&nbsp;&nbsp;<button class="enabledBtn" style="cursor: pointer" onclick="continueSettlement();">'.speak("Continue").'</button></td></tr><tr><td>&nbsp;</td></tr></tbody></table>';
$display .= '</td></tr></table>';
echo $display;

#Efunction to use their UTF-8 monitary symbol.
function display_price($price, $decimal_places) {
	global $locRow;
	$monitarySymbol = !empty($locRow) && !empty($locRow['monitary_symbol']) ? $locRow['monitary_symbol'] : "$";
	$decimal_char = isset($locRow['decimal_char']) ? $locRow['decimal_char'] : '.';
	$thousands_char	= ($decimal_char == ".")?",":".";
	return $monitarySymbol.number_format((float)$price, $decimal_places, $decimal_char, $thousands_char);
}

#function to get monitary symbol for display.
function get_location_row()
{
	global $loc_id; #Included from info_inc.php
	static $locRow = false;
	if ($locRow) {
		return $locRow;
	}
	$query = "SELECT * FROM `locations` WHERE `id`='[1]'";
	$result = lavu_query($query, $loc_id);
	if (!$result) {
		error_log("mysql error in ".__FILE__." -3i3wr- mysql error:" . lavu_dberror());
	} else {
		$locRow = mysqli_fetch_assoc($result);
	}
	return $locRow;
}

function display_time_($dpt) {
	$dpt = explode(" ", $dpt);
	$date = trim($dpt[0]);
	$time = ($dpt[1]);
	$date = explode("-", $date);
	$time = explode(":", $time);
	$sts = mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]);
	return date("n/j g:i:s a", $sts);
}

function currentLocalTime()
{
	global $location_info;

	$current_time = date("Y-m-d H:i:s", time());
	$tzone = $location_info['timezone'];
	if (!empty($tzone)) {
		$current_time = localize_datetime(date("Y-m-d H:i:s"), $tzone);
	}

	return $current_time;
}

function get_alt_total($loc_id, $order_id, $type_id)
{
	$total_query = lavu_query("SELECT SUM(`amount`) AS `sum_total` FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `pay_type_id` = '[3]'", $loc_id, $order_id, $type_id);
	if (mysqli_num_rows($total_query)) {
		$total_read = mysqli_fetch_assoc($total_query);
		return $total_read['sum_total'];
	}

	return 0;
}

?>
	<script language="javascript">
		function continueSettlement() {
			document.getElementById("batch_form").submit();
		}
	</script>
</body>

</html>
