<?php 
$header = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://servicios/">
   <soapenv:Header/>
   <soapenv:Body>
      <ser:generarFactura>
         <arg0>
            <destino></destino>
            <fechaVencimiento>{{date}}</fechaVencimiento>
            <fechaComprobante>{{date}}</fechaComprobante>
			<formaPago>{{form}}</formaPago>
            <montoBruto>{{tax_included}}</montoBruto>
            <montoNoFacturable>0</montoNoFacturable>
            <nroCompra>{{orderid}}</nroCompra>';

$product_details = '<productos>
               <indicadorFacturacion>{{tax_code}}</indicadorFacturacion>
               <nombre>{{product_name}}</nombre>
               <cantidad>{{qty}}</cantidad>
               <precioUnitario>{{unit_price}}</precioUnitario>
               <unidadDeMedida>UN</unidadDeMedida> 
               <descripcion></descripcion>
               <monto>{{product_total}}</monto>
               <montoTotalAPagar>{{product_subtotal}}</montoTotalAPagar>
               <nroLinea>{{slno}}</nroLinea> 
               <porcentajeDescuento>{{product_dicount}}</porcentajeDescuento> 
               <porcentajeRecargo>0</porcentajeRecargo>
</productos>';

$business_details = '<receptor>
<ciudad>{{city}}</ciudad>
<cp>{{zipcode}}</cp>
<departamento>{{company_name}}</departamento>
<direccion>{{address}}</direccion>
<documento>
<documento>{{company_number}}</documento>
<pais>UY</pais>
<tipo>2</tipo>
</documento>
<nombre>{{company_name}}</nombre>
<pais>UY</pais>
</receptor>';

$order_details = '<tasaBasicaIva>22</tasaBasicaIva>
<tasaMinimaIva>10</tasaMinimaIva>
<tipo>{{invoicetype}}</tipo>
<tipoCambio>1</tipoCambio>
<tipoMoneda>UYU</tipoMoneda>
<totalMontoExportacion>0</totalMontoExportacion>
<totalMontoImpuestoPercibido>0</totalMontoImpuestoPercibido>
<totalMontoIvaEnSuspenso>0</totalMontoIvaEnSuspenso>
<totalMontoIvaOtraTasa>0</totalMontoIvaOtraTasa>
<totalMontoIvaTasaBasica>{{subtotal_3}}</totalMontoIvaTasaBasica>
<totalMontoIvaTasaMinima>{{subtotal_2}}</totalMontoIvaTasaMinima>
<totalMontoNoGravado>{{subtotal_1}}</totalMontoNoGravado>';

$refund_details = '<infoReferencias>
 			<nroCFEReferencia>{{ref_invoice}}</nroCFEReferencia>
 			<nroLinea>1</nroLinea>
 			<serieCFEReferencia>{{ref_serial}}</serieCFEReferencia>
 			<tipoCFEReferencia>{{ref_invoice_type}}</tipoCFEReferencia>
            </infoReferencias>';


$footer = '</arg0>
<arg1></arg1>
<arg2></arg2>
</ser:generarFactura>
</soapenv:Body>
</soapenv:Envelope>';
?>