<?php
//error_reporting(E_ALL);

require_once( __DIR__ . '/../info_inc.php');
require_once( __DIR__ . '/LdsDataValidation.php');
require_once(__DIR__."/../NewRelicHelper.php");
require_once(__DIR__."/../ldp/SyncTagAcknowledgements.php");

$lds_debug = (locationSetting("lds_debug", 0) == "1");

if ($lds_debug) error_log("LDS - ".$data_name." - req_id: ".urlvar("YIG")." - app version: ".reqvar("app_version", "")." - debugging enabled");

$lavu_query_should_log = $lds_debug;

/**
 * LDS (Local Data Store or Lavu Device Store) Data Syncer.
 *
 * Replaces the order data in the database with the POST'd version.
 *
 * Expects the order data to be sent in the following format:
 *
 * array(
 * 	'dataname' => 'example_datana',
 * 	'location_id' => '1-123'
 * 	'order_data' => array(
 * 		array(
 * 			'order' => array(...)
 *			'contents' => array( array(...), array(...) ),
 *			'alternate_tx_totals' => array( array(...), array(...) ),
 *			'check_details' => array( array(...), array(...) ),
 *			'modifiers' => array( array(...), array(...) ),
 *			'transactions' => array( array(...), array(...) ),
 *		),
 *		array(...),
 *		array(...),
 *	)
 * )
 */
class LdsDataSyncer
{
	private $debug;
	private $lds_debug;
	private $req_id;
	private $NewRelic;

	private $dataname;
	private $location_id;
	private $order_id;

	private $tables_to_sync;
	private $order_data;
	private $pending_posts;
	private $action_logs;
	private $send_logs;

	private $synced_ioids;
	private $conflicted_ioids;

	public $device_id;
	public $app_version;
	public $internal_updates;
	public $conflict_fixes;
	public $response_statuses;

	private $columns_to_sync;

	private $syncTagAcknowledgements;

	private $logzioErrorActions;

	public function __construct()
	{
		global $lds_debug;
		global $NewRelic;

		$this->debug		= FALSE;
		$this->lds_debug 	= $lds_debug;
		$this->req_id		= urlvar("YIG");
		$this->NewRelic 	= $NewRelic;

		$this->tables_to_sync = array(
			// json_key           => table_name
			'alternate_tx_totals' => 'alternate_payment_totals',
			'check_details'       => 'split_check_details',
			'contents'            => 'order_contents',
			'modifiers'           => 'modifiers_used',
			'order'               => 'orders',
			'transactions'        => 'cc_transactions',
			'transactionsExtend'  => 'cc_transactions_ext'
		);

		$this->logzioErrorActions = array(
			'Printer Error' => 'Printer Error',
			'Active User' => 'Active User',
			'PayPal Error' => 'PayPal Error',
			'PayPal Error Declined' => 'PayPal Error Declined',
			'Card reader udynamo disconnected' => 'Card reader udynamo disconnected',
			'Card reader idynamo disconnected' => 'Card reader idynamo disconnected',
			'PayPal Declined' => 'PayPal Declined',
			'Settings Reloaded' => 'Settings Reloaded'
		);

		$this->syncTagAcknowledgements = new SyncTackAcknowledgements($_REQUEST);
	}

	public function __destruct()
	{
	}

	/**
	 * Synchronize orders for a dataname and location id, process pending posts, and record action_log and send_log entries.
	 *
	 * @param request Array of HTTP Request vars (probably) containing the order data to sync
	 */
	public function sync($request)
	{
		global $post_vars;
		global $data_name;
		global $company_id;
		global $hold_cc;
		global $location_info;
		global $decimal_places;
		global $smallest_money;
		global $device_time;
		global $data_syncer;

		$this->initialize($request);

		if (!empty($this->dataname)) {
			$this->NewRelic->track("dataname", $this->dataname);
		}

		$this->syncTagAcknowledgements->check($this);

		$this->syncOrders();

		$prepared_globals = FALSE;
		$processed_ppids = array();

		foreach ($this->pending_posts as $pending_post)
		{
			if (!empty($pending_post['id']) && !empty($pending_post['ioid']) && !empty($pending_post['post_string']))
			{
				$success = FALSE;
				$post_id = $pending_post['id'];
				$post_ioid = $pending_post['ioid'];

				if (in_array($post_ioid, $this->conflicted_ioids)) {
					// dequeue pending post for conflicted ioids for versions before 3.1
					$this->setPendingPostSucceeded($post_ioid, $post_id);
					continue;
				}

				$all_pp_start_time = microtime(TRUE);
				$this->NewRelic->track("pending_posts_start_ts", $all_pp_start_time);

				if (!in_array($post_id, $processed_ppids)) {

					$processed_ppids[] = $post_id;

					$device_time = reqvar("device_time", FALSE);

					$post_vars = array();
					$post_parts = explode("&", $pending_post['post_string']);
					foreach ($post_parts as $pstprt)
					{
						$eqpos = strpos($pstprt, "=");
						$key = substr($pstprt, 0, $eqpos);
						$value = rawurldecode(substr($pstprt, ($eqpos + 1)));
						if ($key=="device_time" && $device_time)
						{
							$value = $device_time;
						}
						$post_vars[$key] = $value;
					}

					$dn_changed = $this->useDatanameLocation($post_vars['dataname'], $post_vars['loc_id']);
					if ($dn_changed && $prepared_globals)
					{
						$prepared_globals = FALSE;
					}

					if (!$prepared_globals)
					{
						$data_name = $this->dataname;
						$company_id = $post_vars["company_id"];
						$hold_cc = $post_vars["cc"];
						$location_info = $this->getLocationInfo();
						$decimal_places = $location_info['disable_decimal'];
						$smallest_money = (1 / pow(10, $decimal_places));
						$prepared_globals = TRUE;
					}

					// perhaps consider usejcinc post variable
					require_once(__DIR__."/../jcvs/jc_func8.php");

					$device_time = determineDeviceTime($location_info, $post_vars);
					$data_syncer = $this;

					$pp_mode = $post_vars['m'];
					$pp_start_time = microtime(TRUE);

					$this->syncDebug("pending post start - ".$pp_mode." (".$post_id.") - ioid: ".$post_ioid);

					switch ($pp_mode)
					{
						case "close_order":
							$success = (closeOrder($post_vars) == "1");
							break;
						case "email_receipt":
							require_once(__DIR__."/../email_func.php");
							$success = (substr(sendEmailReceipt($post_vars), 0, 2) == "1|");
							break;
						case "refund_order":
						case "void_order":
							if (!empty($location_info['pepper_enabled']))
							{
								processPepperVoidOrRefund($post_vars);
							}
							break;
						default:
							break;
					}

					$pp_end_time = microtime(TRUE);
					$pp_process_time = round((($pp_end_time - $pp_start_time) * 1000), 2) . "ms";

					$this->syncDebug("pending post end - ".$pp_mode." (".$post_id.") - ioid: ".$post_ioid." - succeeded: ".($success?"yes":"no")." - duration: ".$pp_process_time);

					if ($success) $this->setPendingPostSucceeded($post_ioid, $post_id);
				}

				$all_pp_end_time = microtime(TRUE);
				$all_pp_process_time = round((($all_pp_end_time - $all_pp_start_time) * 1000), 2) . "ms";

				$this->NewRelic->track("pending_posts_end_ts", $all_pp_end_time);
				$this->NewRelic->track("pending_posts_duration", $all_pp_process_time);
			}
		}

		$action_log_count = count($this->action_logs);
		$send_log_count = count($this->send_logs);

		if ($action_log_count)
		{
			$al_start_time = microtime(TRUE);

			$this->NewRelic->track("action_logs_start_ts", $al_start_time);

			$this->syncDebug("action log sync start - ".$action_log_count." ".(($action_log_count > 1)?"entries":"entry"));

			$this->response_statuses['action_log'] = array();
			foreach ($this->action_logs as $action_log)
			{
				$this->useDatanameLocation($action_log['dataname'], $action_log['loc_id']);
				$ialid = $action_log['ialid'];
				// If action is exist in logzioErrorActions array then we are not inserting into action_log table. Ref. LP-10212
				if (in_array($action_log["action"], $this->logzioErrorActions)) {
					$logStr = 'POS ACTION ERROR - action : ' . $action_log["action"] . ' | loc_id : ' . $action_log['loc_id'] . ' | order_id : ' . $action_log['order_id'] . ' | time : ' . $action_log['time'] . ' | user : ' . $action_log['user'] . ' | user_id : ' . $action_log['user_id'] . ' | server_time : ' . $action_log['server_time'] . ' | item_id : ' . (int)$action_log['item_id'] . ' | details : ' . $action_log['details']. ' | check : ' . (int)$action_log['check'] . ' | device_udid : ' . $action_log['device_udid'] . ' | details_short : ' . $action_log['details_short'] . ' | ialid : ' . $action_log['ialid'] . ' | ioid : ' . $action_log['ioid']. ' | signature : ' . $action_log['signature']. ' | key_version : ' . $action_log['key_version'];
					$this->syncDebug($logStr);
				} else {
					$this->insertOrUpdateRow('action_log', $action_log, $ialid, TRUE);

					if (in_array($action_log["action"], array( "Paid In", "Paid Out" )))
					{
						$this->updatePaidInPaidOutPlaceHolder($action_log["action"]);
						$tx = $action_log['tx'];
						$this->insertOrUpdateRow('cc_transactions', $tx, $tx['internal_id'], TRUE);
					}
				}

				$this->setLogStatusIfUnset('action_log', $ialid, 'success');
			}

			$al_end_time = microtime(TRUE);
			$al_process_time = round((($al_end_time - $al_start_time) * 1000), 2) . "ms";

			$this->NewRelic->track("action_logs_end_ts", $al_end_time);
			$this->NewRelic->track("action_logs_duration", $al_process_time);

			$this->syncDebug("action log sync end - duration: ".$al_process_time);
		}

		if ($send_log_count)
		{
			$sl_start_time = microtime(TRUE);

			$this->NewRelic->track("send_logs_start_ts", $sl_start_time);

			$this->syncDebug("send log sync start - ".$send_log_count." ".(($send_log_count > 1)?"entries":"entry"));

			$this->response_statuses['send_log'] = array();
			foreach ($this->send_logs as $send_log)
			{
				$islid = $send_log['islid'];
				$this->insertOrUpdateRow('send_log', $send_log, $islid);
				$this->setLogStatusIfUnset('send_log', $islid, 'success');
			}

			$sl_end_time = microtime(TRUE);
			$sl_process_time = round((($sl_end_time - $sl_start_time) * 1000), 2) . "ms";

			$this->NewRelic->track("send_logs_end_ts", $al_end_time);
			$this->NewRelic->track("send_logs_duration", $al_process_time);

			$this->syncDebug("send log sync end - duration: ".$sl_process_time);
		}
	}

	public function echoResponse()
	{
		$response_statuses_json = $this->generateResponseJson($this->response_statuses);

		$this->syncDebug("response: ".print_r($response_statuses_json, TRUE));

		echo $response_statuses_json;
	}

	private function validateOrder($order)
	{
	    $this->syncDebug(__FUNCTION__);

		$order_id = $order['order']['order_id'];
		$ioid = $order['order']['ioid'];
		foreach($order['contents'] as $k => $v){
			$order['contents'][$k] = LdsValidateData('order_contents', $order_id, $ioid, null, $v);
		}
		$order['order'] = LdsValidateData('orders', $order_id, $ioid, null, $order['order']);
		foreach($order['transactions'] as $k => $v){
			$order['transactions'][$k] = LdsValidateData('cc_transactions', $order_id, $ioid, null, $v);
		}
		return $order;
	}

	/**
	 * Loop through the order_data array and commit order data to the database if no sync conflict is detected.
	 */
	private function syncOrders()
	{
		$all_sync_start_time = microtime(TRUE);

		$this->NewRelic->track("order_syncs_start_ts", $all_sync_start_time);

		$this->synced_ioids = array();
		$this->conflicted_ioids = array();

		// sync_sequence defines the order in which the various tables should be synced. The most important
		// consideration is that the order record should be updated last to help eliminate the possibility,
		// be it ever so minute, that a device looking for updates will determine it needs to pull an order
		// before all portions of the order have been updated.
		$sync_sequence = array(
			"contents",
			"modifiers",
			"transactions",
			"alternate_tx_totals",
			"check_details",
			"order",
			"transactionsExtend"
		);

		// Iterate over each order to sync.
		foreach ($this->order_data as $order_data)
		{
		    //Filtering null values out of order to eliminate junk data coming from parts unknown - Chris 10/6/16
            $order_data['order'] = array_filter($order_data['order'], function($value) { return null !== $value; });
			// Fix potential inconsistencies in data
            //REMOVING ON 10/21 DUE TO PROBLEMS CAUSED BY VALIDATEORDER.
			//$order_data = $this->validateOrder($order_data);
			// Setup vars for hold commonly used values for sightliness.
			$order = $this->fixOrderStatus($order_data['order']);

			$order_id = $order['order_id'];
			$ioid = $order['ioid'];
			$check_count	= (int)$order['no_of_checks'];


			//start new code for LP-3611..
			$customerId=explode("|",$order['original_id'])[6];

			if(isset($customerId)&& $customerId!=""){
			    lavu_query("UPDATE `med_customers` SET `last_activity` = '[1]' WHERE `id` = '[2]'", date("Y-m-d H:i:s"), $customerId);
			}
			//end of new code for LP-3611..

			$this->order_id = $order_id;

			$this->checkCodedLocation($order['location'], $order['location_id']);

			$sync_start_time = microtime(TRUE);

			$this->syncDebug("order sync start - order_id: ".$order_id." - ioid: ".$ioid);

			if ($this->checkForSyncConflict($order_data))
			{
				$this->conflicted_ioids[] = $ioid;
				continue;
			}

			// Go over our set list of tables according to the sync_sequence to sync and check for rows.
			foreach ($sync_sequence as $json_key)
			{
				$table_name = $this->tables_to_sync[$json_key];

				// Only process tables that have rows to sync, though `modifiers_used` needs to be allowed
				// to continue in case all modifiers have been removed from an order.
				if (empty($order_data[$json_key]) && $table_name!="modifiers_used")
				{
					continue;
				}

				// Place the associative array for 'order' inside of an array to make it match how the
				// rest of the tables get stored so we can iterate over it the same way in the next loop.
				if ($json_key == "order")
				{
					$order_data[$json_key] = array($order);
				}

				$should_delete_existing_rows = (!in_array($table_name, array( "orders", "cc_transactions", "order_contents" )));
				// This check makes sure we only run the deletes once.  And if there are no rows
				// to delete then our attempt will just whiff harmlessly in the air - but we still
				// save some time/effort by skipping the check for whether there are pre-existing
				// rows to delete. Lastly, this check was put inside of the inner-most loop so we
				// only do the delete when there are rows to insert.
				if ($should_delete_existing_rows)
				{
					$this->deletePreexistingOrderRows($table_name, $ioid, $check_count);
				}

				$contentSyncKey = 'contents';
				if (isset($order_data[$contentSyncKey]) && $json_key == $contentSyncKey) {
					$this->syncOrderContents($table_name, $order_data[$contentSyncKey], $ioid, $order_id);
					continue;
				}
				foreach ($order_data[$json_key] as $key => $row)
				{
					// Skip inserting offline cc_transaction record
					// For offline transaction POS will send 'offline_transaction' flag in 'info' key Ref:- LP-11195
					if ($json_key == 'transactions' && ($row['info'] == 'offline_transaction')) {
						continue;
					}
					switch ($table_name)
					{
						case "alternate_payment_totals":

							foreach ($row as $ptid => $details)
							{
								$details['loc_id']		= $this->location_id;
								$details['order_id']		= $order_id;
								$details['check']		= $key;
								$details['pay_type_id']		= $ptid;
								$details['ioid']		= $ioid;

								$this->insertOrUpdateRow($table_name, $details, $ioid);
							}
							break;

						case "split_check_details":

							$check_number = (int)$row['check'];
							if ($check_number > $check_count)
							{
								// left over check detail record
								break;
							}

						default:

							$this->insertOrUpdateRow($table_name, $row, $ioid);
							break;
					}
				}
			}

			$this->synced_ioids[] = $ioid;

			// We do the ...IfUnset version of this method to keep from overwriting an error message
			// (set while processing the order data) with a success flag.
			$this->setStatusIfUnset($ioid, "success");

			$sync_end_time = microtime(TRUE);
			$sync_process_time = round((($sync_end_time - $sync_start_time) * 1000), 2) . "ms";

			$this->syncDebug("order sync end - order_id: ".$order_id." - ioid: ".$ioid." - duration: ".$sync_process_time);
		}

		$all_sync_end_time = microtime(TRUE);
		$all_sync_process_time = round((($all_sync_end_time - $all_sync_start_time) * 1000), 2) . "ms";

		$this->NewRelic->track("order_syncs_end_ts", $all_sync_end_time);
		$this->NewRelic->track("order_syncs_duration", $all_sync_process_time);
	}

	/**
	 * The order_status field was introduced and implemented in Lavu POS 3.1.4 and is meant to allow the replacement
	 * of complicated order list queries with simpler ones that won't require UNIONs to take advantage of indexing.
	 * This function is provided to help ensure the order_status field is properly set for pre-3.1.4 versions.
	 *
	 * @param order_stub Associative array containing order data directly relating to a row in the orders table
	 */
	private function fixOrderStatus($order_stub)
	{
		$acvc = appVersionCompareValue($this->app_version);

		$original_order_status = arrayValueForKey($order_stub, "order_status");

		if (($acvc >= 30104) && !empty($original_order_status))
		{ // no need for correction
			return $order_stub;
		}

		$fixed_order_status = determineOrderStatusForOrder($order_stub);

		$order_stub['order_status'] = $fixed_order_status;

		return $order_stub;
	}

	/**
	 * Attempts to determine if there is a potential conflict between the order data trying to be synced
	 * and the data that is already present in the database.
	 *
	 * @param order_data Associative array containing combined order info for a particular order
	 */
	private function checkForSyncConflict($order_data)
	{
		global $location_info;

		$order = $order_data['order'];
		$ioid = $order['ioid'];

		$check_order_stub = lavu_query("SELECT `order_id`, `total`, `sync_tag`, `sync_tag_ack`, `last_modified`, `last_mod_ts`, `last_mod_device`, `closed` FROM `orders` WHERE `ioid` = '[1]' ORDER BY `sync_tag` DESC, `last_mod_ts` DESC, `id` DESC LIMIT 1", $ioid);
		if (mysqli_num_rows($check_order_stub) > 0)
		{
			$db_order = mysqli_fetch_assoc($check_order_stub);

			$db_sync_tag		= (int)$db_order['sync_tag'];
			$db_sync_tag_ack	= $db_order['sync_tag_ack'];
			$db_last_modified	= str_replace("CLAIM-ONLY-", "", $db_order['last_modified']);
			$db_last_mod_ts		= $db_order['last_mod_ts'];
			$db_last_mod_device	= $db_order['last_mod_device'];
			$db_closed			= $db_order['closed'];

			$order_sync_tag			= (int)$order['sync_tag'];
			$order_last_modified	= str_replace("CLAIM-ONLY-", "", $order['last_modified']);
			$order_last_mod_ts		= $order['last_mod_ts'];
			$order_last_mod_device	= $order['last_mod_device'];
			$order_closed			= $order['closed'];

			$db_compare_last_mod = $db_last_modified;
			$order_compare_last_mod = $order_last_modified;

			if (appVersionCompareValue($this->app_version) >= 30200)
			{
				$db_compare_last_mod = $db_last_mod_ts;
				$order_compare_last_mod = $order_last_mod_ts;

				if (locationSetting("lds_conflict_detection_check_same_device") == "1")
				{
					$this->syncDebug("checking device ids for ioid ".$ioid." - current_device: ".$this->device_id." - db_last_mod_device: ".$db_last_mod_device." - order_last_mod_device: ".$order_last_mod_device);

					if (($db_last_mod_device == $this->device_id) && ($order_last_mod_device == $this->device_id))
					{
						// All device ids match so the order coming from the app can be accepted.
						return FALSE;
					}
				}

				if (!empty($db_sync_tag_ack) && $this->syncTagAcknowledgements->enabled())
				{
					$sta_parts = explode(",", $db_sync_tag_ack);
					$sta_ts = $sta_parts[0];
					$sta_uuid = $sta_parts[1];

					// Only consider the sync_tag_ack flag when the acknowledgement was initiated by another device.
					if ($sta_uuid != $this->device_id)
					{
						// Only allow the sync_tag_ack flag to prevent syncing if was created within 5 minutes.
						if ((time() - $sta_ts) < 300)
						{
							$this->setStatusIfUnset($ioid, "fail", "order awaiting sync_tag acknowledgement");
							return TRUE;
						}
					}
				}

				if ($db_sync_tag > $order_sync_tag)
				{
					$this->syncConflictDetected($order_data, $db_order, "sync_tag out of sequence");
					return TRUE;
				}
			}

			if ($db_compare_last_mod > $order_compare_last_mod)
			{
				$this->syncConflictDetected($order_data, $db_order, "modified out of sequence");
				return TRUE;
			}

			if (empty($order_closed) || $order_closed=="0000-00-00 00:00:00")
			{
				if (!empty($db_closed) && $db_closed!="0000-00-00 00:00:00")
				{
					$this->syncConflictDetected($order_data, $db_order, "update would nullify previous close");
					return TRUE;
				}
			}
			else if ($order_closed>$db_closed && !empty($db_closed) && $db_closed!="0000-00-00 00:00:00")
			{
				$this->syncConflictDetected($order_data, $db_order, "update would replace previous close");
				return TRUE;
			}
		}

		return FALSE;
	}

	private function syncConflictDetected($order_data, $db_order, $conflict)
	{
		$order		= $order_data['order'];
		$ioid		= $order['ioid'];
		$order_id	= $order['order_id'];

		$this->syncDebug("sync conflict detected - order_id: ".$order_id ." - ioid: ".$ioid." - conflict: ".$conflict);

		if ($this->lds_debug)
		{
			$this->ksortRecursive($order_data);

			$lg_str = $this->dataname." - ".$this->app_version." ".reqvar("app_build", "")." - ".$conflict."[--CR--]";
			$lg_str .= "LOCATION: ".		$this->location_id	." - ";
			$lg_str .= "DEVICE: ".		$this->device_id	." - ";
			$lg_str .= "REMOTE IP: ".	reliableRemoteIP()	." - ";
			$lg_str .= "IOID: ".			$ioid				."[--CR--]";
			$lg_str .= "DEVICE ORDER: ".	$order['order_id'].", ".$order['total'].", ".$order['sync_tag'].", ".$order['last_modified'].", ".$order['last_mod_device'].", ".$order['closed']."[--CR--]";
			$lg_str .= "DB ORDER: ".		$db_order['order_id'].", ".$db_order['total'].", ".$db_order['sync_tag'].", ".$db_order['last_modified'].", ".$db_order['last_mod_device'].", ".$db_order['closed']."[--CR--]";
			$lg_str .= "DEVICE JSON: ".json_encode($order_data)."[--CR--]";

			require_once(__DIR__."/../jcvs/jc_func8.php");
			$combined_db_order_info = loadOrderByIOID($ioid, "sync_conflict");
			$this->ksortRecursive($combined_db_order_info[$ioid]);
			$lg_str .= "DB JSON: ".json_encode($combined_db_order_info[$ioid]);

			write_log_entry("sync_conflict", $this->dataname, $lg_str."\n");
		}

		// Reset last_modified and last_mod_ts on device record to allow updates from cloud to replace it in pre-3.1.4 versions.
		$iu = &$this->internal_updates;
		$iu[$ioid]['orders'][] = array( 'last_modified' => "1970-01-01 00:00:00" );
		$iu[$ioid]['orders'][] = array( 'last_mod_ts' => "1" );

		// Mark status as success so the app stops attempting to sync an out-of-date record.
		$this->setStatusIfUnset($ioid, "success");
	}

	/**
	 * Recursively sorts an associative array and any contained associative arrays according to their keys.
	 *
	 * @param array Associative array passed by reference
	 * @param sort_flags Designates the sorting algorithm
	 */
	private function ksortRecursive(&$array, $sort_flags = SORT_REGULAR)
	{
		if (!is_array($array))
		{
			return FALSE;
		}

		ksort($array, $sort_flags);

		foreach ($array as &$arr)
		{
			$this->ksortRecursive($arr, $sort_flags);
		}

		return TRUE;
	}

	/**
	 * Sets up private members, connects to database.
	 *
	 * @param params Associative array (probably from the HTTP Request) containing the order data to sync.
	 */
	private function initialize($params)
	{
		if (empty($params['dataname']) && !empty($params['cc']))
		{
			$params['dataname'] = $params['cc']; // cc has already been converted to dataname via info_inc.
		}

		$this->validateData($params);

		$this->dataname			= $params['dataname'];
		$this->location_id		= $params['loc_id'];
		$this->device_id		= $params['UUID'];
		$this->app_version		= $params['app_version'];

		$this->order_data				= array();
		$this->pending_posts			= array();
		$this->action_logs				= array();
		$this->send_logs				= array();
		$this->internal_updates			= array();
		$this->conflict_fixes			= array();

		// Convert the JSON in the request variable 'order_data' to arrays so we can iterate over it.
		$sync_data = json_decode($params['sync_data'], TRUE);
		$this->syncTagAcknowledgements->initialize($sync_data);

		if (isset($sync_data['orders_combined_data_array']))
		{
			$this->order_data = $sync_data['orders_combined_data_array'];
		}

		if (isset($sync_data['pending_posts_array']))
		{
			$this->pending_posts = $sync_data['pending_posts_array'];
		}

		if (isset($sync_data['action_log_array']))
		{
			$this->action_logs = $sync_data['action_log_array'];
		}

		if (isset($sync_data['send_log_array']))
		{
			$this->send_logs = $sync_data['send_log_array'];
		}

		global $data_name; // Globalized for lavu_query logging.
		$data_name = $this->dataname;
		ConnectionHub::getConn('rest')->selectDN($this->dataname);
	}

	/**
	 * Check for the coded location identifier so the dataname and location id properties can be properly
	 * updated and the proper database connection can be established to perform syncing for a particular
	 * record.
	 *
	 * @param coded_location String with the format "{[company_id]:[dataname]:[location_id]}"
	 * @param location_id Integer value to specific the location if not found in coded_location
	 */
	public function checkCodedLocation($coded_location, $location_id)
	{
		if (substr($coded_location, 0, 1) != "{")
		{
			return;
		}

		$cdl = explode(":", trim($coded_location, "{}"));
		if (count($cdl) < 2)
		{
			return;
		}

		$use_dn = $cdl[1];
		$use_lid = (!empty($cdl[2]))?$cdl[2]:$location_id;
		if (empty($use_lid))
		{
			$use_lid = $this->location_id;
		}
		$this->useDatanameLocation($use_dn, $use_lid);
	}

	/**
	 * Specify the dataname and location id associated with the current sync operation by setting the appropriate properties.
	 * Establish a new database connection if the dataname is different from what was previously set.
	 *
	 * @param dataname String specifying the dataname associated with the syncing of a particular record
	 * @param loc_id Integer speicifying the location id associated with the syncing of a particular record
	 */
	private function useDatanameLocation($dataname, $loc_id)
	{
		if (empty($dataname))
		{
			return FALSE;
		}

		if (empty($loc_id))
		{
			return FALSE;
		}

		$changed = FALSE;

		if ($dataname != $this->dataname)
		{
			$changed = TRUE;
			ConnectionHub::getConn('rest')->selectDN($dataname);
		}

		$this->dataname = $dataname;
		$this->location_id = $loc_id;

		global $data_name; // Globalized for lavu_query logging.
		$data_name = $this->dataname;

		return $changed;
	}

	/**
	 * Make sure the required fields are present.
	 *
	 * @param params Associative array (probably from the HTTP Request) containing the order data to sync
	 */
	public function validateData($params)
	{
		if (empty($params['dataname']))
		{
			$this->setStatus($this->ioid, "fail", "Missing dataname");
			//$this->logErrorAndDie( "ERROR - Missing dataname" );
			die("ERROR - Missing dataname\n");
		}
		if (empty($params['loc_id']))
		{
			$this->setStatus($this->ioid, "fail", "Missing location ID");
			//$this->logErrorAndDie( "ERROR - Missing loc_id" );
			die("ERROR - Missing location ID\n");
		}

		return TRUE;
	}

	private function getLocationInfo()
	{
		if (sessvar("location_info"))
		{
			$location_info = sessvar("location_info");
		}
		else
		{
			$loc_id = $this->location_id;

			$get_location_info = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $loc_id, NULL, NULL, NULL, NULL, NULL, FALSE);
			$location_info = mysqli_fetch_assoc($get_location_info);

			$get_location_config = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `type` = 'location_config_setting' AND `location` = '[1]'", $loc_id, NULL, NULL, NULL, NULL, NULL, FALSE);
			while ($config_info = mysqli_fetch_assoc($get_location_config))
			{
				$location_info[$config_info['setting']] = $config_info['value'];
			}

			set_sessvar("location_info", $location_info);
		}

		return $location_info;
	}

	/**
	 * Delete all pre-existing rows for the ioid that we're syncing when appropriate.
	 *
	 * @param table Database table name that we're syncing
	 * @param ioid Unique order instance identifier needed in case of error
	 * @param check_count Integer indicating the number of checks present for a given ioid
	 */
	private function deletePreexistingOrderRows($table, $ioid, $check_count)
	{
		// Checking for blank values for the vars in the where clause should've already
		// happened by now, but I opted for paranoia since we're deleting stuff.
		if (empty($ioid))
		{
			return 0;
		}

		$sql = "";

		switch ($table)
		{
			case "modifiers_used":
			case "alternate_payment_totals":

				$sql = "DELETE FROM `".$table."` WHERE `ioid` = '[ioid]'";
				break;

			case "split_check_details":

				$sql = "DELETE FROM `".$table."` WHERE `ioid` = '[ioid]' AND `check` > '[check_count]'";
				break;

			case "orders":
				// Don't clear out order records so that serial_no and sync_tag incrementation stays sound.
			case "cc_transactions":
				// Don't clear out transaction records so they are guaranteed to persist.
				break;

			case "cc_transactions_ext":
				return 0;

			default:

				$this->logError("ERROR - encountered unrecognized table ".$table);
				$this->setStatus($ioid, "fail", "Sync error from unrecognized table ".$table);
				return 0;
				break;
		}

		$this->logDebug("ioid=".$ioid." sql=".$sql);

		$result = lavu_query($sql, array( 'ioid' => $ioid, 'check_count' => $check_count ));
		if ($result === FALSE)
		{
			$this->logError(__FILE__." on ". __LINE__." got :".lavu_dberror());
			$this->setStatus($ioid, "fail", "Sync error 1");
			return 0;
		}

		$rows_deleted = ConnectionHub::getConn('rest')->affectedRows();
		$this->logDebug("rows_deleted=".$rows_deleted);

		return $rows_deleted;
	}

	/**
	 * Insert the new rows or update existing rows for the record being synced.
	 *
	 * @param table Database table name that we're syncing
	 * @param row Associative array with key => value pairs associated with a row in a table
	 * @param ioid Unique order instance identifier needed in case of error
	 * @param allow_empty_order_ids Boolean designating whether ioid or order_id can be allowed to remain empty
	 */
	private function insertOrUpdateRow($table, $row, $ioid, $allow_empty_order_ids=FALSE)
	{
		$preserve_fields = array(); // When the populated value in the database takes precedence over an empty value in the post.
		switch ($table)
		{
			case "split_check_details":
				$preserve_fields = array( "loyaltree_info" );
				break;
			default:
				break;
		}

		$row_last_modified	= (isset($row['last_modified']))?$row['last_modified']:"";
		$pushed_ts = time();

		$vars = array();

		//LP-8195
		if($table == 'orders'){
			$row = $this->verifyAndSetCustomerIDField($row);
		}

		// Filter out certain key => value pairs according to the table to which the insert/update is destined.
		foreach ($row as $col => $val)
		{
			if ( $col == 'needs_sync' ) continue;
			else if ( $col == 'id' ) continue;
			else if ( $table == 'orders' )
			{
				if ( $col == 'dataname' ) continue;
				else if ( $col == 'key_version' ) continue;
				else if ( $col == 'lastmod' ) continue;
				else if ( $col == 'lastsync' ) continue;
				else if ( $col == 'order_hash' ) continue;
				else if ( $col == 'reopen_datetime' ) continue;
				else if ( $col == 'serial_no' ) continue;
				else if ( $col == 'signature' ) continue;
				else if ( $col == 'kitchen_status' ) continue;  // LP-7502
				else if ( $col == 'merged_conflict' ) continue;  // LP-10359
				else if ( $col == 'sync_tag_ack' )
				{
					// To ensure that any sync_tag_ack value coming from the app is never committed to the database.
					$val = "";
				}
				else if ( ($col == 'last_modified') )
				{
					$val = str_replace("CLAIM-ONLY-", "", $val);
				}
			}
			else if ( $table == 'split_check_details' )
			{
				if ( $col == 'overpaid_amount' ) continue;
				else if ( $col == 'deposit_amount' ) continue;
				else if ( $col == 'deposit_remaining' ) continue;
				else if ( $col == 'iTaxExemptAdjustments' ) continue;
				else if ( $col == 'rawSubtotal' ) continue;
				else if ( $col == 'tax_fraction' ) continue;
				else if ( $col == 'rfid_payment_flag' ) continue;
				else if ( $col == 'displayDiscount' ) continue;
				else if ( $col == 'check_item_count' ) continue;
				else if ( $col == 'checkTaxExemption' ) continue;
				else if ( $col == 'empty_check_flag' ) continue;

				if ( $col == 'print_info' ) {
				    if ( !empty($val) ) {
				        $val1= str_replace("}{","}###{",$val);
                        $fiscalJsons= explode("###", $val1);
                        foreach($fiscalJsons as $fiscalJson) {
                            $val=$fiscalJson;
                        	$get_already_fiscal_info = lavu_query("SELECT id FROM `fiscal_transaction_log` WHERE `transaction_info` LIKE '%".$fiscalJson."%'");
                        	$get_already_fiscal_info= mysqli_num_rows($get_already_fiscal_info);
                        	if ( !$get_already_fiscal_info ) {
                            	$fiscalJson_array = json_decode($fiscalJson,1);
                            	$type=$fiscalJson_array['receipt_type'];
                            	$pv_number=$fiscalJson_array['PV_Number'];
                            	$transaction_id=$fiscalJson_array['transaction_id'];
                            	$subtotal_amount=$fiscalJson_array['SubtotalAmount'];
                            	$send_void='0.00';
                            	$item_void='0.00';
                            	$total_amt_data=lavu_query("SELECT * from `fiscal_transaction_log`  WHERE `order_id`='[1]' order by id limit 1 ", $this->order_id);
                            	$total_amt_data = mysqli_fetch_assoc($total_amt_data);
                            	$total=$total_amt_data['total'];
                            	if ($total!="") {
        	                        	if ($type=="TiqueFacturaA" || $type=="TiqueFacturaB") {
        	                        		$send_total=$subtotal_amount; $send_refund=$total-$subtotal_amount;
        	                        	}
                            		else {
                            		    if ($fiscalJson_array['has_been_voided']=='true') {
                            		        $send_total='0.00';
                            		        $send_refund='0.00';
                            		        $send_void=$total;
                            		    }
                                   		else {
                               		        $act_amt_data=lavu_query("SELECT * from `fiscal_transaction_log`  WHERE `order_id`='[1]' AND `show`='1' ", $this->order_id);
                               		        $act_amt_data = mysqli_fetch_assoc($act_amt_data);
                               		        $act_total=$act_amt_data['total'];
                               		        $send_total=$act_total; $send_refund=$act_total;
                                   		}
                            		}
                            	}
                            	else {
                            	    $send_total=$subtotal_amount; $send_refund="0.0";
                            	}
                            	lavu_query("UPDATE `fiscal_transaction_log` SET `show` = '0' WHERE `order_id`='[1]'", $this->order_id);
                            	if ($send_void == '0.00') {
                            	    $item_void = $fiscalJson_array['item_voided_amount'];
                            	}

                            	$vals = array( $this->order_id,
                            				   $row['check'],
                            		           $fiscalJson,
                            		           $type,
                            		           1,
                            		           date('Y-m-d H:i:s'),
                            		           $send_total,
                            		           $send_refund,
                            		           $send_void,
                            		           $transaction_id,
                            		           $pv_number,
                            		           $item_void
                            		       );
                                $fiscal_transaction_log_sql = "INSERT INTO `fiscal_transaction_log` (`order_id`,`check`,`transaction_info`,`type`,`show`,`created_datetime`,`total`,`refund`,`void_amount`,`transaction_id`,`pv_number`,`item_void_amount`) VALUES ('".implode("','", $vals)."')";
                                $fiscal_transaction_log_result = lavu_query($fiscal_transaction_log_sql);
                        	}
                        }
				    }
				}
		    }
			else if ( $table == 'cc_transactions' )
			{
				if ( $col == 'NEW PAYMENT' ) continue;
				else if ( $col == 'DO_NOT_DRAW_ORDER' ) continue;
				else if ( $col == 'take_tip' ) continue;
				else if ( $col == 'take_signature' ) continue;
				else if ( $col == 'orig_action' ) continue;
				else if ( $col == 'orig_internal_id' ) continue;
				else if ( $col == 'server' ) continue;
			}
			else if ( $table == 'action_log' )
			{
				if ( $col == 'company_id' ) continue;
				else if ( $col == 'dataname' ) continue;
				else if ( $col == 'tx' ) continue;
			}
			else if ( $table == 'send_log' )
			{
				if ( $col == 'company_id' ) continue;
				else if ( $col == 'dataname' ) continue;
				else if ( $col == 'loc_id' ) continue;
			}

			$vars[$col] = $val;
		}

		$app_version_compare_value = appVersionCompareValue($this->app_version);

		if ($table == "orders")
		{
			// sync_tag starts at 1 if unset
			$sync_tag = ((int)$vars['sync_tag'] + 1);

			// sync_tag incrementation will wait for sync_tag acknowledgement as of app version 3.2.4
			if ($this->syncTagAcknowledgements->enabled() && ($app_version_compare_value >= 30204))
			{
				$vars['sync_tag_ack'] = time().",".$this->device_id;
			}
			else
			{
				$vars['sync_tag'] = $sync_tag;
				$vars['sync_tag_ack'] = "";
			}

			$vars['pushed_ts'] = $pushed_ts;
		}

		$loc_id_key = ($table=="orders" || $table=="send_log")?"location_id":"loc_id";
		if (empty($vars[$loc_id_key]) && $table != 'cc_transactions_ext')
		{
			$vars[$loc_id_key] = $this->location_id;
		}

		if (!$allow_empty_order_ids && $table != 'cc_transactions_ext')
		{
			if (empty($vars['ioid']))
			{
				$vars['ioid'] = $ioid;
			}

			if ($table!="action_log" && $table!="send_log")
			{
				$vars['order_id'] = $this->order_id;
			}
		}

		$ioid = $vars['ioid'];

		$update_id = FALSE;
		$order_serial_no = "";
		if ($table=="orders" && !empty($ioid))
		{
			$check_existing = lavu_query("SELECT `id`, `serial_no`, `last_modified`, `active_device` FROM `orders` WHERE `ioid` = '[ioid]' ORDER BY `sync_tag` DESC, `last_mod_ts` DESC, `id` DESC", $vars);
			if ($check_existing !== FALSE)
			{
				$existing_count = mysqli_num_rows($check_existing);
				if ($existing_count > 0)
				{
					$existing_record = mysqli_fetch_assoc($check_existing);
					$update_id = $existing_record['id'];
					$order_serial_no = $existing_record['serial_no'];

					if ($app_version_compare_value >= 30200)
					{
						$db_last_modified = $existing_record['last_modified'];
						if (substr($db_last_modified, 0, 11) == "CLAIM-ONLY-")
						{
							$row_last_modified = str_replace("CLAIM-ONLY-", "", $row_last_modified);
							$db_last_modified = str_replace("CLAIM-ONLY-", "", $db_last_modified);
							$db_active_device = $existing_record['active_device'];

							if (!empty($vars['active_device']) && empty($db_active_device) && ($db_last_modified >= $row_last_modified))
							{
								$vars['active_device'] = "";
							}
						}
					}

					if ($existing_count > 1)
					{
						$delete_extra = lavu_query("DELETE FROM `orders` WHERE `ioid` = '[1]' AND `id` != '[2]'", $ioid, $update_id);
					}
				}
			}
		}
		else if ($table=="cc_transactions" && !empty($vars['internal_id']))
		{
			$check_existing = lavu_query("SELECT `id`, `last_mod_ts` FROM `cc_transactions` WHERE `internal_id` = '[internal_id]'", $vars);
			if ($check_existing!==FALSE && mysqli_num_rows($check_existing))
			{
				$existing_record = mysqli_fetch_assoc($check_existing);

				// Version 3.1 and later can be trusted for cc_transactions last_mod_ts comparisons.
				if (($app_version_compare_value >= 30100) && ((int)$vars['last_mod_ts'] <= (int)$existing_record['last_mod_ts']))
				{
					// Record already up to date.
					return 1;
				}

				$update_id = $existing_record['id'];
			}
		}
		else if ($table == "split_check_details")
		{
			$check_fields = (count($preserve_fields) > 0)?",`".implode("`,`", $preserve_fields)."`":"";
			$check_existing = lavu_query("SELECT `id`".$check_fields." FROM `split_check_details` WHERE `ioid` = '[ioid]' AND `check` = '[check]'", $vars);
			if ($check_existing!==FALSE && mysqli_num_rows($check_existing))
			{
				$existing_record = mysqli_fetch_assoc($check_existing);
				foreach ($preserve_fields as $pf)
				{
					if (empty($vars[$pf]) && !empty($existing_record[$pf]))
					{
						unset($vars[$pf]);
						$this->addInternalUpdate($ioid, "check_details", $pf, $existing_record[$pf], "check", $vars['check']);
					}
				}
				$update_id = $existing_record['id'];
			}
		}
		else if ($table=="action_log" && !empty($vars['ialid']))
		{
			$check_existing = lavu_query("SELECT `id` FROM `action_log` WHERE `ialid` = '[ialid]'", $vars );
			if ($check_existing!==FALSE && mysqli_num_rows($check_existing))
			{
				$existing_record = mysqli_fetch_assoc($check_existing);
				$update_id = $existing_record['id'];
			}
			else if ($vars['action'] == "Order Closed")
			{
				$check_for_previous_close = lavu_query("SELECT `id` FROM `action_log` WHERE `ioid` = '[ioid]' AND `action` = 'Order Closed'", $vars);
				if ($check_for_previous_close!==FALSE && mysqli_num_rows($check_for_previous_close))
				{
					$this->syncDebug("second close detected and rejected - order_id: ".$vars['order_id']." - ioid: ".$ioid);
					return 1;
				}
			}
		}
		else if ($table=="send_log" && !empty($vars['islid']))
		{
			$check_existing = lavu_query("SELECT `id` FROM `send_log` WHERE `islid` = '[islid]'", $vars);
			if ($check_existing!==FALSE && mysqli_num_rows($check_existing))
			{
				$existing_record = mysqli_fetch_assoc($check_existing);
				$update_id = $existing_record['id'];
			}
		}

		if ($table=="modifiers_used") {
			if (isset($vars['ordertag_id']) && (trim($vars['ordertag_id']) == '' || is_null($vars['ordertag_id']))) {
			    unset($vars['ordertag_id']);
			}
		}

		$update	= "";
		$cols	= "";
		$vals	= "";

		if ($update_id)
		{
			buildUpdate($vars, $update);
		}
		else
		{
			buildInsertFieldsAndValues($vars, $cols, $vals);
		}

		if ($table=="orders" && !$update_id)
		{
			$get_last_serial_no = lavu_query("SELECT `serial_no` FROM `orders` ORDER BY `serial_no` DESC LIMIT 1");
			if ($get_last_serial_no!==FALSE && mysqli_num_rows($get_last_serial_no))
			{
				$last_record = mysqli_fetch_assoc($get_last_serial_no);
				$order_serial_no = ($last_record['serial_no'] + 1);
				$vars['serial_no'] = $order_serial_no;
				$cols .= ", `serial_no`";
				$vals .= ", '[serial_no]'";
			}
		}

		//LP-8628
		//This would only execute for order if already exist in orders table
		if ($table == "orders" && $row['gratuity'] > 0 && $update_id) {
			$ag_cash = 0;
			$ag_card = 0;
			$ag_other = 0;
			$gc_paid = $row['gift_certificate'];
			$alt_paid = $row['alt_paid'];
			$use_other_paid = ($gc_paid + $alt_paid);
			$gratuity = $row['gratuity'];
			$cash_applied = $row['cash_applied'];
			$card_paid = $row['card_paid'];
			// Using if condition because gratuity can be paid by cash, card and other payment method.
			if ($cash_applied > 0) {
				if ($cash_applied >= $gratuity) {
					$ag_cash = $gratuity * 1;
				} else {
					$ag_cash = $cash_applied * 1;
				}
				$gratuity = $gratuity - $ag_cash;
			}
			if ($card_paid > 0 && $gratuity > 0) {
				if ($card_paid >= $gratuity) {
					$ag_card = $gratuity * 1;
				} else {
					$ag_card = $card_paid * 1;
				}
				$gratuity = $gratuity - $ag_card;
			}
			if ($use_other_paid > 0 && $gratuity > 0) {
				if ($use_other_paid >= $gratuity) {
					$ag_other = $gratuity * 1;
				} else {
					$ag_other = $use_other_paid * 1;
				}
				$gratuity = $gratuity - $ag_other;
			}
			$vars['ag_card'] = $ag_card;
			$vars['ag_cash'] = $ag_cash;
			$vars['ag_other'] = $ag_other;
			$update .= ", `ag_cash` = '[ag_cash]', `ag_card` = '[ag_card]', `ag_other` = '[ag_other]'";
		}

		$sql = $update_id?"UPDATE `".$table."` SET ".$update." WHERE `id` = '".$update_id."'":"INSERT INTO `".$table."` (".$cols.") VALUES (".$vals.")";

		$this->logDebug("sql=".$sql);

		if ($table == "orders" && ($row['order_status'] == 'closed' || $row['order_status'] == 'reclosed')) {
			shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $row[server_id] $row[location_id] $_REQUEST[dataname] > /dev/null 2>/dev/null &");
		}

		$affected_rows = 0;
		$result = lavu_query( $sql, $vars );

		// START  LP-2494
		if ($table == "action_log")
		{
			require_once(__DIR__."/../jcvs/jc_func8.php");
			$currentActionLogId = ($update_id == "")?lavu_insert_id():$update_id;
			updateSignatureForNorwayActionLog($currentActionLogId);
		}
		// END  LP-2494

		if ($result === false)
		{
			if ($table=="action_log" || $table=="send_log")
			{
				$this->setLogStatus($table, $ioid, "fail", "Sync error 2: ".lavu_dberror()." : ".$sql);
			}
			else
			{
				$this->setStatus($ioid, "fail", "Sync error 2: ".lavu_dberror()." : ".$sql);
			}
		}
		else
		{
			$affected_rows = ConnectionHub::getConn('rest')->affectedRows();
		}

		if ($table=="orders" && ($update_id || $affected_rows>0))
		{ // Send serial_no, sync_tag, and pushed_ts changes back to app to update internal record.

			$iu = &$this->internal_updates;

			$this->addInternalUpdate($ioid, "orders", "serial_no", (string)$order_serial_no);
			$this->addInternalUpdate($ioid, "orders", "sync_tag", (string)$sync_tag);
			$this->addInternalUpdate($ioid, "orders", "pushed_ts", (string)$pushed_ts); // For forward compatibility with DataPlexSyncer
		}

		return $affected_rows;
	}

	/**
	 * Updates the Paid In or Paid Out order record's closed time when a Paid In or Paid Our record is synced.
	 *
	 * @param paid_in_or_out Paid In|Paid Out
	 */
	private function updatePaidInPaidOutPlaceHolder($paid_in_or_out)
	{
		$check_placeholder = lavu_query("SELECT `id` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $this->location_id, $paid_in_or_out);
		if (mysqli_num_rows($check_placeholder) > 0)
		{
			$info = mysqli_fetch_assoc($check_placeholder);
			lavu_query("UPDATE `orders` SET `closed` = '[1]' WHERE `id` = '[2]'", date("Y-m-d H:i:s"), $info['id']);
		}
		else
		{
			$create_placeholder = lavu_query("INSERT INTO `orders` (`location_id`, `order_id`, `opened`, `closed`) VALUES ('[1]', '[2]', '[3]', '[3]')", $this->location_id, $paid_in_or_out, date("Y-m-d H:i:s"));
		}
	}

	/**
	 * Specify an update to be performed on the device's internal record after the completion of the sync request.
	 *
	 * @param ioid internal order id specifying the target order for the update
	 * @param table orders|check_details
	 * @param set_field key specifying the field to update
	 * @param set_value value to which to set the field
	 * @param match_field field specified to filter target record beyond ioid, for check_details only
	 * @param match_value value specified to filter target record beyond ioid, for check_details only
	 */
	public function addInternalUpdate($ioid, $table, $set_field, $set_value, $match_field="", $match_value="")
	{
		$iu = &$this->internal_updates;

		if (!isset($iu[$ioid]))
		{
			$iu[$ioid] = array();
		}

		if (!isset($iu[$ioid][$table]))
		{
			$iu[$ioid][$table] = array();
		}

		switch ($table)
		{
			case "orders":

				$iu[$ioid][$table][] = array( $set_field => $set_value );

				break;

			case "check_details":

				if (!isset($iu[$ioid][$table][$match_field]))
				{
					$iu[$ioid][$table][$match_field] = array();
				}

				if (!isset($iu[$ioid][$table][$match_field][$match_value]))
				{
					$iu[$ioid][$table][$match_field][$match_value] = array();
				}

				$iu[$ioid][$table][$match_field][$match_value][] = array( $set_field => $set_value );

				break;

			default:

				break;
		}
	}

	/**
	 * Generate response message to send to LDS sync queue manager process.
	 *
	 * @param statuses Array with IOID keys and values are status arrays
	 */
	private function generateResponseJson($statuses)
	{
		$response = array();

		$empty_object = new stdClass();

		$response['ioid_server_save_statuses'] = ($statuses != NULL)?$statuses:$empty_object;

		if (count($this->syncTagAcknowledgements->responses) > 0)
		{
			$response['sync_tag_ack_responses'] = $this->syncTagAcknowledgements->responses;
		}

		if (count($this->internal_updates) > 0)
		{
			$response['internal_updates'] = $this->internal_updates;
		}

		if (count($this->conflict_fixes) > 0)
		{
			$response['conflict_fixes'] = $this->conflict_fixes;
		}

		$response_json = json_encode($response);

		$this->logDebug($response_json);

		return $response_json;
	}

	/**
	 * Sets Log Type per-internal id status field.
	 *
	 * @param log_type a string indicating the logtype: action_log|send_log
	 * @param internal_id Unique log type instance identifier needed in case of error
	 * @param status success|fail
	 * @param reason Failure reason, if status = fail
	 */
	private function setLogStatus($log_type, $internal_id, $status, $reason="")
	{
		$this->response_statuses[$log_type][$internal_id] = array( 'status' => $status );
		if ($status == "fail")
		{
			$this->response_statuses[$log_type][$internal_id]['fail_reason'] = $reason;
		}
	}

	/**
	 * Sets Log Type per-interal id status field if it hasn't already been set previously.
	 *
	 * @param log_type a string indicating the log type: action_log|send_log
	 * @param internal_id Unique log type instance identifier needed in case of error
	 * @param status success|fail
	 * @param reason Failure reason, if status = fail
	 */
	private function setLogStatusIfUnset($log_type, $internal_id, $status, $reason="")
	{
		$status_set = FALSE;
		if (empty($this->response_statuses[$log_type][$internal_id]))
		{
			$this->setLogStatus($log_type, $internal_id, $status, $reason);
			$status_set = TRUE;
		}

		return $status_set;
	}

	/**
	 * Sets per-IOID status field.
	 *
	 * @param ioid Unique order instance identifier needed in case of error
	 * @param status success|fail
	 * @param reason Failure reason, if status = fail
	 */
	private function setStatus($ioid, $status, $reason="", $status_type="", $status_msg="")
	{
		$this->response_statuses[$ioid] = array( 'status' => $status );
		if ($status_type != "")
		{
			$this->response_statuses[$ioid]['status_type'] = $status_type;
		}
		if ($status == "fail")
		{
			$this->response_statuses[$ioid]['fail_reason'] = $reason;
		}
		if ($status_msg != "")
		{
			$this->response_statuses[$ioid]['status_msg'] = $status_msg;
		}
	}

	/**
	 * Sets per-IOID status field if it hasn't already been set previously.
	 *
	 * @param ioid Unique order instance identifier needed in case of error
	 * @param status success|fail
	 * @param reason Failure reason, if status = fail
	 */
	private function setStatusIfUnset($ioid, $status, $reason="", $status_type="", $status_msg="")
	{
		$status_set = FALSE;
		if (empty($this->response_statuses[$ioid]))
		{
			$this->setStatus($ioid, $status, $reason, $status_type, $status_msg);
			$status_set = TRUE;
		}

		return $status_set;
	}

	/**
	 * Adds to the list of successfully posted pending post ids for a given ioid.
	 *
	 * @param ioid internal order id
	 * @param post_id pending post id
	 */
	private function setPendingPostSucceeded($ioid, $post_id)
	{
		$resp4ioid = $this->response_statuses[$ioid];
		$posted_posts = (isset($resp4ioid['posted_posts']))?$resp4ioid['posted_posts']:array();
		$posted_posts[] = $post_id;

		$this->response_statuses[$ioid]['posted_posts'] = $posted_posts;
	}

	/**
	 * Print logging message.
	 *
	 * @param msg Message to log
	 */
	private function logMsg($msg)
	{
		error_log($msg);
	}

	/**
	 * Print logging message if debug flag member is set.
	 *
	 * @param msg Message to log
	 */
	private function logDebug($msg)
	{
		if ($this->debug)
		{
			$this->logMsg($msg);
		}
	}

	/**
	 * Print logging message if lds_debug flag member is set.
	 *
	 * @param msg Message to log
	 */
	public function syncDebug($msg)
	{
		if ($this->lds_debug)
		{
			$this->logMsg("LDS - ".$this->dataname." - req_id: ".$this->req_id." - ".$msg);
		}
	}

	/**
	 * Log error message.
	 *
	 * @param msg Message to log
	 */
	private function logError($msg)
	{
		error_log($msg);
	}

	/**
	 * Log error message followed by die().
	 *
	 * @param msg Message to log
	 */
	private function logErrorAndDie($msg)
	{
		$this->logError($msg);
		die($msg."\n");
	}

	//LP-8195
	private function verifyAndSetCustomerIDField($orderRow){
		global $data_name;

		//See if order has a customer attached
		if(empty($orderRow['original_id'])){
			return $orderRow;
		}
		//For when it is changed inside the app.
		if(!empty($orderRow['customer_id'])){
			return $orderRow;
		}
		//See if they do in fact have the customer_id field in the orders table.
		//This is precautionary and can be removed later, not sure if this will be a special
		//field, or for everyone.
		$schemaQuery = "SELECT * FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA` = '[1]' AND TABLE_NAME = 'orders' AND COLUMN_NAME = 'customer_id'";

		$result = mlavu_query($schemaQuery, 'poslavu_'.$data_name.'_db');
		if(!$result || mysqli_num_rows($result) == 0){
			return $orderRow;
		}
		//19|o|Customer|o|Brian Dennedy|o|2810|o|{"print_info":
		$original_id_parts = explode('|o|', $orderRow['original_id']);
		$customerID = intval($original_id_parts[3]);
		if(empty($customerID)){
			return $orderRow;
		}
		$orderRow['customer_id'] = $customerID;
		return $orderRow;
	}

	/**
	 * This function is used to update or insert records into order_contents table.
	 * @param string $table table name
	 * @param array $data order contents records
	 * @param string $ioid internal order id
	 * @param string $order_id id of order
	 * @return true and log message
	 */
	private function syncOrderContents($table, $data, $ioid, $order_id) {
		if (!empty($data)) {
			$existingOrderContents = array();
			// Get existing records from order_contents table for given order id and ioid
			$getOrderContents = lavu_query("Select * FROM `".$table."` WHERE `ioid` = '[ioid]' AND `order_id` = '[order_id]'", array( 'ioid' => $ioid, 'order_id' => $order_id));
			if (mysqli_num_rows($getOrderContents) > 0 ) {
				while ($orderContentsInfo = mysqli_fetch_assoc($getOrderContents))
				{
					$existingOrderContents[$orderContentsInfo['icid']] = $orderContentsInfo;
				}
			}
			$newOrderContents = array();
			// Customize data of order contents come from POS
			foreach ($data as $details) {
				unset($details['needs_sync']);
				if (isset($details['ordertag_id']) && (trim($details['ordertag_id']) == '' || is_null($details['ordertag_id']))) {
					unset($details['ordertag_id']);
				}
				unset($details['send_to_kds']);
				unset($details['id']);
				if ($details['icid'] != '') {
					if ($details['ioid'] == '') {
						$details['ioid'] = $ioid;
					}
					$newOrderContents[$details['icid']] = $details;
				}
			}
			// Get existing and new data
			$updateOrderContents = array();
			$insertOrderContents = array();
			if (!empty($existingOrderContents) && !empty($newOrderContents)) {
				$updateOrderContents = array_intersect_key($newOrderContents, $existingOrderContents);
				$insertOrderContents = array_diff_key($newOrderContents, $existingOrderContents);
			} else if (!empty($newOrderContents)) {
				$insertOrderContents = $newOrderContents;
			}

			// update table with existing data
			if (!empty($updateOrderContents)) {
				foreach ($updateOrderContents as $details) {
					$update = '';
					buildUpdate($details, $update);
					$sql = "UPDATE `".$table."` SET ". $update . " WHERE `order_id` = '" . $order_id . "' AND `ioid` = '" . $ioid ."' AND `icid` = '" . $details['icid'] . "'";
					$updateResult = lavu_query( $sql, $details );
					if (!$updateResult) {
						$updateResult = lavu_query( $sql, $details );
						if (!$updateResult) {
							$this->syncDebug("Update failed sql=".$sql);
						}
					}
				}
			}
			// insert data into table with new data of order
			if (!empty($insertOrderContents)) {
				foreach ($insertOrderContents as $details) {
					$cols = '';
					$vals = '';
					buildInsertFieldsAndValues($details, $cols, $vals);
					$sql = "INSERT INTO `".$table."` (".$cols.") VALUES (". $vals . ")";
					$insertResult = lavu_query( $sql, $details );
					if (!$insertResult) {
						$insertResult = lavu_query( $sql, $details );
						if (!$insertResult) {
							$this->syncDebug("Insert failed sql=".$sql);
						}
					}

				}
			}
		}
	}
}
