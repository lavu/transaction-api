<?php
require_once(dirname(__FILE__)."/../jcvs/jc_func8.php");
/**---------------------------------------------------------------------
*	The features and specification of this file are documented at:
*
*		wiki.lavu/Order_Declobberer
*
*	@author Ezra Stallings
*	@email ezra@lavu.com
**----------------------------------------------------------------------
*/

class LdsDeclobberer
{
	// private $dbSyncTag;
	private $dbOrder;
	private $newHash;
	private $dbHash;
	private $orderHash;
	private $ioid;
	private $orderSyncTag;
	private $dbSyncTag;
	private $bypass = false;

	/**
	*	Build a LdsDeclobberer. The constructor requires:
	*	@param dbSyncTag : the sync tag on the order as stored in the database.
	*		Not guaranteed or required to be a good sync tag or even exist!
	*	@param orderSyncTag : the sync tag on the order as passed in from the iPad.
	*		Also not guaranteed to be good or exist.
	*	@param order_data : an associative array of all the data in the order. Used to generate new hash
	*/
	public function __construct($order_data) {
		$ioid = $order_data['order']['ioid'];
		$ldslobber = loadOrderByIOID($ioid);
		$this->dbOrder = json_decode(json_encode($ldslobber[$ioid]), true);
		$this->orderHash = $order_data['order']['order_hash'];
		if($this->dbOrder['status'] == "NoMatch") {
			error_log("BYPASSING DECLOBBERER");
			$this->newHash = $this->makeOrderHash($order_data);
			$this->orderSyncTag = 0;
			$this->bypass = true;
			return;
		}
		$this->diff($this->cleanOrder($this->overlay($order_data, $this->dbOrder)), $this->cleanOrder($this->dbOrder), "diff");
		$this->orderSyncTag = $order_data['order']['sync_tag'];
		$this->dbSyncTag = $this->dbOrder['order']['sync_tag'];

		$this->newHash = $this->makeOrderHash($this->overlay($order_data, $this->dbOrder));
		$this->dbHash = $this->makeOrderHash($this->dbOrder);
		$this->ioid = $ioid;

		error_log("order hash: " . $this->orderHash . " , newhash: " . $this->newHash . " , dbHash: " . $this->dbHash . " , orderSyncTag: " . $this->orderSyncTag . " , dbSyncTag: " . $this->dbSyncTag);
		error_log("ioid: " . $ioid . ", order_id: " . $order_data['order']['order_id'] . ", subtotal: " . print_r($order_data['check_details'][1]['subtotal'], true));
	}

	/**
	*	Main function called to:
	*		1) detect syncing issues
	*		2) set response fields on the LdsDataSyncer object (if there was an issue)
	*		3) to generate a new sync tag (if there was no issue)
	*	@param ldsDataSyncer : the LdsDataSyncer object that stores and processes responses
	*	@param order_ref : a reference to the order as held by the LdsDataSyncer, so we can update it's sync tag
	*/
	public function process(&$ldsDataSyncer, &$order_ref)
	{
		if(!$this->bypass)
		{

			error_log("Checking for clobber");
			//First, detect problems
			//No-Ops:
			if($this->isNoOp())
			{
				$this->setNoOpResponse($ldsDataSyncer);
				return true;
			}
			//Out-Of-Sequence updates
			if($this->isOOS())
			{
				$this->setOOSResponse($ldsDataSyncer);
				return true;
			}
		}

		//---- SET NEW SYNC TAG ----//
		//increment sequence number now that we know a conflict isn't occurring
		$new_sync_tag = $this->orderSyncTag+1;
		error_log("New Sync Tag: " . $new_sync_tag . " hash: " . $this->newHash);
		$ldsDataSyncer->internal_updates[$this->ioid]['orders'][] = array("sync_tag"=>$new_sync_tag);
		// $ldsDataSyncer->internal_updates[$this->ioid]['orders'][] = array("order_hash"=>$this->newHash);
		$order_ref['sync_tag'] = $new_sync_tag;
		$order_ref['order_hash'] = $this->newHash;
		//---- END SYNC TAG ----//

		return false;
	}

	public function setReturnHash(&$ldsDataSyncer)
	{
		$getOrder = loadOrderByIOID($this->ioid);
		$dbOrder = json_decode(json_encode($getOrder[$this->ioid]), true);
		$dbHash = $this->makeOrderHash($dbOrder);
		$ldsDataSyncer->internal_updates[$this->ioid]['orders'][] = array("order_hash"=>$dbHash);
		error_log("Order Hash: " . $dbHash);
	}

	/**
	*	Sets the response statuses on the LdsDataSyncer object
	*	correctly to handle a No-Op. If the update was also stale,
	*	we also set the stale response statuses.
	*	@param $ldsDataSyncer : the LdsDataSyncer class object responsible for processing the response
	*/
	private function setNoOpResponse(&$ldsDataSyncer)
	{
		error_log("No-Op Detected!");
		if($this->isStale())
		{
			error_log("    Stale!");
			$ldsDataSyncer->response_statuses[$this->ioid] = array('status' => 'success', 'status_type' => 'noop,stale');
			$this->setStaleResponse($ldsDataSyncer, $this->ioid);
		}
		else
		{
			$ldsDataSyncer->response_statuses[$this->ioid] = array('status' => 'success', 'status_type' => 'noop');
		}
	}

	/**
	*	Sets the response statuses on the LdsDataSyncer object
	*	correctly to handle an Out Of Sequence update, including setting the stale response statuses.
	*	@param $ldsDataSyncer : the LdsDataSyncer class object responsible for processing the response
	*/
	private function setOOSResponse(&$ldsDataSyncer)
	{
		error_log("Out-Of-Sequence Detected!");
		$ldsDataSyncer->response_statuses[$this->ioid] = array('status' => 'fail', 'status_type' => 'oos', 'status_msg' => 'Out-Of-Sequence Operation Detected!');
		$this->setStaleResponse($ldsDataSyncer, $this->ioid);
	}

	/**
	*	Sets the response statuses on the LdsDataSyncer object so that the iPad will get a new
	*	version of the stale (out of date with the database) order. Helper function used by
	*	both setOOSResponse and setNoOpResponse.
	*	@param $ldsDataSyncer : the LdsDataSyncer class object responsible for processing the response
	*/
	private function setStaleResponse(&$ldsDataSyncer)
	{
		error_log("Processing Stale!: " . $this->ioid);
		$ldsDataSyncer->conflict_fixes[$this->ioid] = $this->dbOrder;
		$ldsDataSyncer->internal_updates[$this->ioid]['orders']['sync_tag'] = $this->dbSyncTag;
		$ldsDataSyncer->internal_updates[$this->ioid]['orders']['order_hash'] = $this->dbHash;
	}

	/**
	*	Removes a few 'forbidden' fields from the order data to faciliate hashing
	*	Specifically, removes those fields which are pertinent to last-modified dates and devices
	*/
	private function cleanOrder($order) {
		$new_order = array();
		$new_order['check_details'] = $order['check_details'];
		foreach($new_order['check_details'] as &$check){
			unset($check->overpaid_amount);
			unset($check->deposit_amount);
			unset($check->deposit_remaining);
			unset($check->iTaxExemptAdjustments);
			unset($check->rawSubtotal);
			unset($check->tax_fraction);
			unset($check->rfid_payment_flag);
			unset($check->displayDiscount);
			unset($check->check_item_count);
			unset($check->checkTaxExemption);
			unset($check->empty_check_flag);
			unset($check->id);
		}
		$new_order['modifiers'] = $order['modifiers'];
		foreach($new_order['modifiers'] as &$mod){
			unset($mod['id']);
		}
		$new_order['order'] = $order['order'];
		unset($new_order['order']['active_device']);
		unset($new_order['order']['last_mod_device']);
		unset($new_order['order']['last_mod_ts']);
		unset($new_order['order']['last_modified']);
		unset($new_order['order']['needs_sync']);
		unset($new_order['order']['dataname']);
		unset($new_order['order']['serial_no']);
		unset($new_order['order']['lastmod']);
		unset($new_order['order']['lastsync']);
		unset($new_order['order']['id']);
		unset($new_order['order']['reopen_datetime']);
		unset($new_order['order']['auto_gratuity_is_taxed']);
		// unset($new_order['order']['last_mod_register_name']);
		unset($new_order['order']['sync_tag']);
		unset($new_order['order']['pushed_ts']); //Might fix 6962
		$new_order['transactions'] = $order['transactions'];
		foreach($new_order['transactions'] as &$trans){
			unset($trans['last_modified']);
			unset($trans['last_mod_ts']);
			unset($trans['NEW PAYMENT']);
			unset($trans['DO_NOT_DRAW_ORDER']);
			unset($trans['take_tip']);
			unset($trans['take_signature']);
			unset($trans['orig_action']);
			unset($trans['orig_internal_id']);
			unset($trans['server']);
			unset($trans['id']);
		}
		$new_order['alternate_tx_totals'] = $order['alternate_tx_totals'];
		$new_order['contents'] = $order['contents'];
		foreach($new_order['contents'] as &$content){
			unset($content['id']);
			unset($content['override_id']);
		}
		return $new_order;
	}

	/**
	*	Recursively sorts the keys on an array, passed by reference. Used to uniquify an object for hashing.
	*/
	private function ksortRecursive(&$array, $sort_flags = SORT_REGULAR) {
		if (!is_array($array)) return false;
		ksort($array, $sort_flags);
		foreach ($array as &$arr) {
			$this->ksortRecursive($arr, $sort_flags);
		}
		return true;
	}

	private function diff($new, $base, $name)
	{
		if(!is_array($base) && !is_object($base)) {
			if($new == $base) return "";
			error_log($name . ": " . $new . " -> " . $base);
		}
		foreach($base as $k => $v) {
			if(isset($new[$k]))
				$this->diff($new[$k], $v, $name . "-" . $k);
		}
	}

	/**
	* Overlays OVERLAY onto BASE, assuming both have same basic structure
	*/
	private function overlay($overlay, $base)
	{
		if(!is_array($base)) {
			return $overlay;
		}
		$ret = array();
		foreach($base as $k => $v) {
			if(isset($overlay[$k]))
				$ret[$k] = $this->overlay($overlay[$k], $v);
			else
				$ret[$k] = $v;
		}
		return $ret;
	}

	/**
	*	Generates the order hash for an order.
	*/
	private function makeOrderHash($order)
	{
		// $diff = $this->diff($cleaned, $dbOrder, "diff");
		$cleaned = $this->cleanOrder($order);
		$this->ksortRecursive($cleaned);
		$json = json_encode($cleaned);
		$hashed = sha1($json);
		return $hashed;
	}

	private function isNoOp()
	{
		// $order_hash == $old_hash
		error_log("No-op detection: " . $this->orderHash . ' vs ' . $this->newHash);
		return ($this->orderHash == $this->newHash);
	}

	private function isStale()
	{
		error_log("Is Stale: " . $this->orderHash . ' vs ' . $this->dbHash);
		return ($this->orderSyncTag != $this->dbSyncTag);
		// return ($this->orderHash != $this->dbHash);
	}

	private function isOOS()
	{
		error_log("OOS detection: " . $this->orderHash . ' vs ' . $this->dbHash);
		// $seq_num != $db_seq_num
		return ($this->orderSyncTag != $this->dbSyncTag);
		// return ($this->orderHash != $this->dbHash);
	}
}