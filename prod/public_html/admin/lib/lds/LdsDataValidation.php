<?php
	function LdsValidateData($table_name, $order_id, $ioid, $no_of_checks, $row, $obj=false)
	{
		if($table_name=="order_contents")
		{
			// For A Problem in which the "After Discount" amount would be very high.
			// Only Observed in order_contents that are shared between multiple checks
			
			$calc_after_discount = $row['subtotal_with_mods'] * 1 - $row['discount_amount'] * 1;
			$calc_diff = $row['after_discount'] * 1 - $calc_after_discount;
			if($calc_diff > 0.10)
			{
				if($row['tax_rate2']=="" && $row['tax_rate3']=="" && $row['subtotal_with_mods'] * 1 > $row['discount_amount'] * 1)
				{
					LdsSetNumberPrecision($row['subtotal_with_mods']);
					$calc_tax_amount = $calc_after_discount * $row['tax_rate1'];
					
					$row['after_discount'] = LdsPrNumber($calc_after_discount);
					$row['tax_subtotal1'] = LdsPrNumber($calc_after_discount);
					$row['tax_amount'] = LdsPrNumber($calc_tax_amount);
					$row['tax1'] = LdsPrNumber($calc_tax_amount);
					$row['total_with_tax'] = LdsPrNumber($calc_after_discount + $calc_tax_amount);
				}
			}
		}
		else if($table_name=="cc_transactions")
		{
			$check_cct = lavu_query("SELECT * FROM `cc_transactions` WHERE `ioid` = '[1]' AND `transaction_id` = '[2]' AND `check` = '[3]' AND `transtype` = '[4]'", $ioid, $row['transaction_id'], $row['check'], $row['transtype']);
			if(mysqli_num_rows($check_cct)) {
				$content = mysqli_fetch_assoc($check_cct);
				$row['internal_id'] = $content['internal_id'];
			}
		}
		else if($table_name=="orders")
		{
			// Verify that Order Tax is Equal to sum of Order Contents Tax
			
			if($row['multiple_tax_rates']=="0")
			{
				LdsSetNumberPrecision($row['total']);
				
				$order_tax = $row['tax'];
				$total_with_tax = $row['total'];
				$content_query = lavu_query("select sum(`tax_amount`) as `tax_amount`, sum(`subtotal_with_mods`) as `subtotal_before_discount`, sum(`discount_amount`) as `discount_amount`, sum(`idiscount_amount`) as `idiscount_amount` from `order_contents` where `order_id`='[1]' AND `void` = '0'",$row['order_id']);
				if(mysqli_num_rows($content_query))
				{
					$content_read = mysqli_fetch_assoc($content_query);
					$content_tax = $content_read['tax_amount'];
					$content_subtotal = $content_read['subtotal_before_discount'];
					$content_discount = $content_read['discount_amount'];
					$content_idiscount = $content_read['idiscount_amount'];
					//echo "Ot: $order_tax Ct: $content_tax<br>";
					if($row['subtotal'] !== $content_subtotal){
						$row['subtotal'] = $content_subtotal;
					}
					if($row['discount'] !== $content_discount){
						$row['discount'] = $content_discount;
					}
					if($row['idiscount_amount'] != $content_idiscount){
						$row['idiscount_amount'] = $content_idiscount;
					}
					//$calc_diff = abs($order_tax * 1 - $content_tax * 1);
					if($order_tax != $content_tax)
					{
						$row['tax'] = LdsPrNumber($content_tax);
					}
				}
				$discount = $row['discount'] + $row['idiscount_amount'];
				$calc_total_with_tax = LdsPrNumber($row['subtotal'] * 1 - $discount * 1 + $row['tax'] + $row['gratuity'] + $row['rounding_amount']);
				if($total_with_tax != $calc_total_with_tax)
				{
					$row['total'] = $calc_total_with_tax;
				}
			}

			$correct_tip_amount = $row['card_gratuity'];
			$correct_cc_amount = $row['card_paid'];
			$check_cct = lavu_query("SELECT * FROM `cc_transactions` WHERE `ioid` = '[1]' AND `voided` != '1' AND `pay_type_id` = '2' AND (`action` = 'Sale' OR `action` = 'Refund') AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $row['ioid'], $row['location_id']);
			if ($check_cct->num_rows > 1) {
				$show_multiple_ccts = true;
				
				$check_cc_total = lavu_query("SELECT SUM(`cc_transactions`.`amount`) AS `amount`, SUM(`tip_amount`) AS `tip_amount` FROM `cc_transactions` WHERE `ioid` = '[1]' AND `loc_id` = '[2]' AND `voided` != '1' AND `pay_type_id` = '2' AND `action` = 'Sale' AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $row['ioid'], $row['location_id']);
				$cc_total_info = $check_cc_total->fetch_assoc();
				$check_cc_refund = lavu_query("SELECT SUM(`cc_transactions`.`amount`) AS `amount`, SUM(`tip_amount`) AS `tip_amount` FROM `cc_transactions` WHERE `ioid` = '[1]' AND `loc_id` = '[2]' AND `voided` != '1' AND `pay_type_id` = '2' AND `action` = 'Refund' AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $row['ioid'], $row['location_id']);
				$cc_refund_info = $check_cc_refund->fetch_assoc();
				if (((float)$cc_total_info['amount'] > 0) || ((float)$cc_refund_info['amount'] > 0)) {
					$correct_cc_amount = ($cc_total_info['amount'] - $cc_refund_info['amount']);
				}
				if (((float)$cc_total_info['tip_amount'] > 0) || ((float)$cc_refund_info['tip_amount'] > 0)) {
					$correct_tip_amount = ($cc_total_info['tip_amount'] - $cc_refund_info['tip_amount']);
					$set_card_tip = number_format($correct_tip_amount, 2, ".", "");
				}

			} else if (($check_cct->num_rows == 1) || ($row['card_paid'] > 0)) {
				$cct_info = $check_cct->fetch_assoc();
				
				$correct_cc_amount = $cct_info['amount'];
				$correct_tip_amount = $cct_info['tip_amount'];
				if (((float)$row['card_gratuity'] > 0) && ($correct_tip_amount == 0)) {
					$correct_tip_amount = $row['card_gratuity'];
					$update_record = lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $correct_tip_amount, time(), $cct_info['id']);
				}	
				
			}

			if (number_format((float)$correct_cc_amount,2) != number_format((float)$row['card_paid'],2))
				//echo "HERE 4: $correct_cc_amount<br>";
				$row['card_paid'] = number_format((float)$correct_cc_amount, 2, ".", "");

			if ($correct_tip_amount != $row['card_gratuity'])
				$row['card_gratuity'] = number_format((float)$correct_tip_amount, 2, ".", "");
		}
		return $row;
	}
	
	$lds_number_precision = 2;
	function LdsPrNumber($num)
	{
		global $lds_number_precision;
		if(!isset($lds_number_precision)) $lds_number_precision = 2;
		
		return number_format($num,$lds_number_precision,'.','');
	}
	
	function LdsSetNumberPrecision($num)
	{
		global $lds_number_precision;
		if(!isset($lds_number_precision)) $lds_number_precision = 2;
		$num_parts = explode(".",$num);
		if(count($num_parts) > 1)
		{
			$lds_number_precision = strlen(trim($num_parts[1]));
		}
	}

