<?php

session_start();
header("Content-type: application/json");

require_once("info_inc.php");

$loc_id = reqvar("loc_id", "");

$skinpackResult = lavu_query("SELECT `value` FROM `config` WHERE `type` = 'location_config_setting' AND `setting` = 'skinpack' AND `_deleted` = '0' AND `location` = '[1]'", $loc_id);

if (!$skinpackResult || mysqli_num_rows($skinpackResult)==0)
{
	lavu_echo('{"skinpack_info":"No Skinpack"}'); // exits
}

$skinpackConfigRow = mysqli_fetch_assoc($skinpackResult);

$skinpackToUse = $skinpackConfigRow['value'];

$skinpackDirectory = $_SERVER['DOCUMENT_ROOT']."/skinpacks/".$skinpackToUse;
$skinpackInfoFile = $skinpackDirectory."/skinpack_info.json";

$contents = file_get_contents($skinpackInfoFile);

$skinPackSavedInfoArr = json_decode($contents, 1);

if (empty($skinPackSavedInfoArr))
{
	lavu_echo('{"skinpack_info":"No Skinpack","error":"could not parse skinpack info file"}'); // exits
}

if (empty($skinPackSavedInfoArr['settings']))
{
	lavu_echo('{"skinpack_info":"Invalid Skinpack","error":"settings key is missing"}'); // exits
}

$skin_pack_settings = $skinPackSavedInfoArr['settings'];

// Build image file list based on entries in skinpack_info.json, including the last modified timestamp
// for each file. If a file is not found in the skin pack directory then a "NOT FOUND" flag will be
// present instead of a last modified timestamp.

$images_directory = $skinpackDirectory."/images";
$image_file_info = array();

foreach ($skin_pack_settings as $key => $info)
{
	if (isset($info['image_name']))
	{
		$file_name = $info['image_name'];
		if (!isset($image_file_info[$file_name]))
		{
			$last_mod_ts = filemtime($images_directory."/".$file_name);

			$image_file_info[$file_name] = ($last_mod_ts === FALSE)?"NOT FOUND":$last_mod_ts;
		}
	}

	if (isset($info['pressed_image_name']))
	{
		$pressed_file_name = $info['pressed_image_name'];
		if (!isset($image_file_info[$pressed_file_name]))
		{
			$last_mod_ts = filemtime($images_directory."/".$pressed_file_name);

			$image_file_info[$pressed_file_name] = ($last_mod_ts === FALSE)?"NOT FOUND":$last_mod_ts;
		}
	}
}

lavu_echo('{"load_skinpack":"1","skinpack_info":'.json_encode($skinPackSavedInfoArr).',"image_file_info":'.json_encode($image_file_info).'}');