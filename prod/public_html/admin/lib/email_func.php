<?php

	function sendEmail($wt_recipient, $s_subject, $s_body, $s_headers=NULL)
	{
		// check for multiple emails
		if (is_array($wt_recipient))
		{
			foreach ($wt_recipient as $s_recipient)
			{
				mail($s_recipient, $s_subject, $s_body, $s_headers);
			}
		}
		else
		{
			mail($wt_recipient, $s_subject, $s_body, $s_headers);
		}
	}

	function sendEmailReceipt($post_vars)
	{
		mlavu_close_db();
		$email_body = email_receipt($post_vars['loc_id'], $post_vars['ioid'], $post_vars['order_id'], $post_vars['check'], $post_vars['receipt_string'], $post_vars['email'], $post_vars['server_id'], $post_vars['server'], false, $post_vars['copy']);

		return "1|".$email_body;
	}

	function email_receipt($loc_id, $ioid, $order_id, $check, $receipt_string, $email, $server_id, $server, $num_checks=false, $copy=false)
	{
		global $data_name;
		global $company_info;
		global $location_info;
		global $device_time;
		global $smallest_money;
		global $details_short; // array, for action_log entry

		$receipt_parts		= explode(":", $receipt_string);
		$company_name		= $receipt_parts[1];

		$email_addresses	= explode(",", $email);
		$subject_line		= "Receipt from ".$company_name." - Order #".$order_id.", Check #".$check;
		if ($copy == true || $copy == '1' || $copy == 1) {
		    $subject_line = speak("Copy")." - ".$subject_line;
		}
		$email_body			= "";
		$headers			= "";

		$thank_you_line	= speak("Thank you so much for allowing us to serve you today.");
		$message_plain	= $thank_you_line."\n\n";
		$message_html	= $thank_you_line."<br><br>";

		$hagd = speak("Have a great day!");
		$vuas = speak("Visit us again soon!");

		$line_count = 0;

		$replacement_totals = array();

		$validlines=5;
		$totallines=substr_count($receipt_string, '---------------------------');
		$skiplines=$totallines - $validlines;

		for ($l = 2; $l < count($receipt_parts); $l++)
		{
			$this_line = $receipt_parts[$l];
			if ($this_line == "---------------------------")
			{
				if ($skiplines>0) {
					$message_plain	.= "\n------------------------------------------------------------";
					$message_html	.= "<table width='400px'><tr><td colspan='3'><hr color='#EEEEEE'></td></tr></table>";
					$skiplines--;
					continue;
				}

				$line_count++;
				if ($line_count == 1)
				{
					$message_plain	.= "\n\n";
					$message_html	.= "<br><br><table cellspacing='0' cellpadding='2' width='400px'>";
				}
				else if ($line_count == 2)
				{
					$message_plain	.= "\n------------------------------------------------------------";
					$message_html	.= "<tr><td colspan='3'><hr color='#EEEEEE'></td></tr>";
				}
				else if ($line_count == 3)
				{
					$message_plain	.= "\n\n\n".$hagd." ".$vuas."\n\n";
					$message_html	.= "</table><br><br>".$hagd." ".$vuas."<br><br>";
				}
				else if ($line_count == 4)
				{
					break;
				}
			}
			else
			{
				if ($line_count==0 || $line_count==3)
				{
					$this_line		= str_replace("[s", "*", $this_line);
					$message_plain	.= str_replace("[c", ":", $this_line)."\n";
					$message_html	.= str_replace("[c", ":", $this_line)."<br>";
				}
				else if ($line_count == 1)
				{
					$line_parts = explode(" ", trim($this_line));
					$quantity = $line_parts[0];

					if ($quantity=="-" || $quantity=="[s" || $quantity=="[s[s")
					{
						$quantity = "";
					}
					else
					{
						$line_parts[0] = "";
						$this_line = join(" ", $line_parts);
					}

					$price_separated = explode("*", str_replace("[c", ":", $this_line));
					$price_separated = str_replace("[s", "*", $price_separated);

					$message_plain	.= "\n".str_pad($quantity, 5, " ", STR_PAD_LEFT)."  ".str_pad($price_separated[0], 38, " ", STR_PAD_RIGHT)."  ".str_pad($price_separated[1], 10, " ", STR_PAD_LEFT);
					$message_html	.= "<tr><td align='right' style='padding:0px 5px 2px 5px'>".$quantity."</td><td align='left' valign='top' style='padding:0px 5px 2px 5px'>".$price_separated[0]."</td><td align='right' valign='top' style='padding:0px 5px 2px 5px'>".$price_separated[1]."</td></tr>";
				}
				else if ($line_count == 2)
				{
					if (strstr($this_line, "*"))
					{
						$line_parts = explode("*", str_replace("[c", ":", $this_line));
						if ($line_parts[0] == "")
						{
							$line_parts[0] = " ";
						}

						// fix subtotal and tax lines for 2.3.8 re-email
						if ($line_parts[0] == speak("Subtotal").":")
						{
							if ((float)$line_parts[1] == 0)
							{
								if (($check_subtotal_and_tax = lavu_query("SELECT `subtotal`, `tax` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $check)) !== false)
								{
									if (mysqli_num_rows($check_subtotal_and_tax) > 0)
									{
										$check_info = mysqli_fetch_assoc($check_subtotal_and_tax);
										if ((float)$check_info['subtotal'] >= ($smallest_money * 0.75))
										{
											$decimalChar = ($location_info['decimal_char'] =='') ? '.' : $location_info['decimal_char'];
											$decimalSeparator = ($decimalChar == '.') ? ',' : '.';
											$monetarySymbol = $location_info['monitary_symbol'];
											$leftRight = $location_info['left_or_right'];
											if ($leftRight == 'left') {
												$left_side = html_entity_decode($monetarySymbol."&lrm;");
											} else {
												$right_side = html_entity_decode($monetarySymbol."&rlm;");
											}

											$line_parts[1] = $left_side.number_format((float)$check_info['subtotal'], (int)$location_info['disable_decimal'], $decimalChar, $decimalSeparator).$right_side;
											$replacement_totals['tax'] = $left_side.number_format((float)$check_info['tax'], (int)$location_info['disable_decimal'], $decimalChar, $decimalSeparator).$right_side;
										}
									}
									mysqli_free_result($check_subtotal_and_tax);
								}
							}
						}
						else if ($line_parts[0] == speak("No Tax").":")
						{
							if (isset($replacement_totals['tax']))
							{
								$line_parts[0] = speak("Tax").":";
								$line_parts[1] = $replacement_totals['tax'];
							}
						}

						$message_plain	.= "\n       ".str_pad(str_replace("[s", "*", $line_parts[0]), 38, " ", STR_PAD_RIGHT)."  ".str_pad(str_replace("[s", "*", $line_parts[1]), 10, " ", STR_PAD_LEFT);
						$message_html	.= "<tr><td></td><td align='left' valign='top' style='padding:2px 5px 2px 5px'>".str_replace("[s", "*", $line_parts[0])."</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".str_replace("[s", "*", $line_parts[1])."</td></tr>";
					}
					else
					{
						$message_plain	.= "\n       ".str_pad(str_replace(array("[s", "[c"), array("*", ":"), $this_line), 38, " ", STR_PAD_RIGHT);
						$message_html	.= "<tr><td colspan='3' align='left' valign='top' style='padding:1px 5px 1px 5px'>".nl2br(str_replace("[c", ":", $this_line))."</td></tr>";
					}
				}
			}
		}

		if (loyalTreeEnabledForLocation($location_info))
		{
			$sep = sha1(date("r", time()));
			require_once(__DIR__.'/../../inc/loyalty/loyaltree/LoyalTreeIntegration.php');
			require_once(resource_path()."/phpqrcode/qrlib.php");

			if (!$num_checks)
			{
				$num_checks = 0;
				if (($check_check = lavu_query("SELECT * FROM `split_check_details` where `order_id`='[1]'", $order_id)) !== false)
				{
					$num_checks = mysqli_num_rows($check_check);
					mysqli_free_result($check_check);
				}
			}

			$decoded = json_decode(get_loyaltree_purchase_info($loc_id, $check, $order_id, $email));
			if ($decoded->json_status != "error")
			{
				if (isset($decoded->qrstring) && !empty($decoded->qrstring))
				{
					if (!empty($decoded->qrstring))
					{
						$qrcode_path = '/images/'. $data_name .'/qrcodes';
						$qrcode_dir = LoyalTreeConfig::QRCODE_DIR_BASE . $qrcode_path;

						if ( !file_exists($qrcode_dir) )
						{
							$qrcode_dir_created = mkdir($qrcode_dir, 0755, true);

							if ( !$qrcode_dir_created )
							{
								error_log("Could not create {$qrcode_dir} for emailed receipt with LoyalTree; falling back to old dir");
								$qrcode_dir = $_SERVER['DOCUMENT_ROOT'].'/loyaltree_qr_codes/';
							}
						}

						if ( isset($decoded->qrcode) && !empty($decoded->qrcode) )
						{
							$qrcode = $decoded->qrcode;
						}
						else
						{
							// Come up with a White Label-esque naming convention that is unlikely to have collisions
							$qrcode = $data_name .'-'. $loc_id .'-'. $order_id .'-'. $check .'-'. substr($decoded->qrstring, strpos($decoded->qrstring, '='));
						}

						$filename = $qrcode .'.png';

						$filename_full_path = $qrcode_dir .'/'. $filename;

						// Create QR code of encoded qrstring written out to admin/images/<dataname>/qrcodes
						QRcode::png($decoded->qrstring, $filename_full_path, 'L', 4, 2);

						// QR code caption
						$text = $decoded->text;

						// Base64 encoded QR code image
						$inline = chunk_split(base64_encode(file_get_contents($filename_full_path)));

						// Link to Lavu-hosted (previous LoyalTree-hosted) QR code image for text only version of the email
						$qrcode_link = $decoded->qrstring;
						if ( isset($decoded->app) )
						{
							$qrcode_link = LoyalTreeConfig::QRCODE_DISPLAY_URL_BASE . $qrcode_path .'/'. $filename;
						}
					}

					$email_body =<<<EOBODY
--PHP-mixed-{$sep}
Content-Type: multipart/alternative; boundary="PHP-alt-{$sep}"

--PHP-alt-{$sep}
Content-Type: text/plain

$message_plain

--PHP-alt-{$sep}
Content-Type: multipart/related; boundary="PHP-related-{$sep}"

--PHP-related-{$sep}
Content-Type: text/html

$message_html
<img src="cid:PHP-CID-{$sep}" /></p>
<br>
$text

--PHP-related-{$sep}
Content-Type: image/png
Content-Transfer-Encoding: base64
Content-ID: <PHP-CID-{$sep}>

{$inline}
--PHP-related-{$sep}--

--PHP-alt-{$sep}--
EOBODY;

					$message_plain .= "<br><br> To retrieve your LoyalTree Coupon click the following link: <br>".$qrcode_link;
				}
				else
				{
					$text = "Your Loyaltree points have been added to your account successfully. Thanks for using Loyaltree!";
					$message_plain .= $decoded->text;
					$email_body =<<<EOBODY
--PHP-mixed-{$sep}
Content-Type: multipart/alternative; boundary="PHP-alt-{$sep}"

--PHP-alt-{$sep}
Content-Type: text/plain

$message_plain

--PHP-alt-{$sep}
Content-Type: multipart/related; boundary="PHP-related-{$sep}"

--PHP-related-{$sep}
Content-Type: text/html

$message_html

$text
--PHP-related-{$sep}--

--PHP-alt-{$sep}--
EOBODY;

				}
			}
			else
			{
				// ** DEBUG LOGGING ** //
				//error_log("LoyalTree failed for emailed receipt: ".json_encode($decoded));
			}
		}

		if (empty($email_body))
		{
			$sep = sha1(date("r", time()));
			$email_body =<<<EOBODY
--PHP-mixed-{$sep}
Content-Type: multipart/alternative; boundary="PHP-alt-{$sep}"

--PHP-alt-{$sep}
Content-Type: text/plain

$message_plain

--PHP-alt-{$sep}
Content-Type: multipart/related; boundary="PHP-related-{$sep}"

--PHP-related-{$sep}
Content-Type: text/html

$message_html

--PHP-related-{$sep}--

--PHP-alt-{$sep}--
EOBODY;
		}

		if (locationSetting("multipart_emails") == "1")
		{
			$email_body = html_entity_decode($message_html, ENT_QUOTES, "UTF-8");
			$headers .= "Content-type: text/html; charset=iso-8859-1"."\r\n";
			$headers .= "From: ".$company_name." Lavu POS <receipts@lavusys.com>"."\r\n";
		}
		else
		{
			$headers .= "From: receipts@lavusys.com\r\nX-Mailer: Custom PHP Script";
			$headers .="\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-{$sep}\"";
		}

		$email_body =  wordwrap($email_body, 400, "\n");
		sendEmail($email_addresses, $subject_line, $email_body, $headers);

		$l_vars = array(
			'action'			=> "Order Receipt Emailed",
			'check'			=> $check,
			'details'		=> $email,
			'details_short'	=> implode(" ", $details_short),
			'device_udid'	=> determineDeviceIdentifier($_REQUEST),
			'ialid'			=> newInternalID(),
			'ioid'			=> $ioid,
			'loc_id'			=> $loc_id,
			'order_id'		=> $order_id,
			'server_time'	=> UTCDateTime(true),
			'time'			=> $device_time,
			'user'			=> $server,
			'user_id'		=> $server_id
		);

		$fields = "";
		$values = "";

		buildInsertFieldsAndValues($l_vars, $fields, $values);

		$log_this = lavu_query("INSERT INTO `action_log` (".$fields.") VALUES (".$values.")", $l_vars);

		// START LP-1171
		if (function_exists('updateSignatureForNorwayActionLog'))
		{
			// updateSignatureForNorwayActionLog() will not be defined for requests coming from the Lavu Retro app
			$currentActionLogId = lavu_insert_id();
			updateSignatureForNorwayActionLog($currentActionLogId);
		}
		// END  LP-1171

		// START LP-1171
		if (function_exists('updateSignatureForNorwayActionLog'))
		{
			// updateSignatureForNorwayActionLog() will not be defined for requests coming from the Lavu Retro app
			$currentActionLogId = lavu_insert_id();
			updateSignatureForNorwayActionLog($currentActionLogId);
		}
		// END  LP-1171

		return $email_body;
	}
?>
