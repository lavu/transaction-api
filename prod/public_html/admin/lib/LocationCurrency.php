<?php
/**
 * Created by PhpStorm.
 * User: Irshad Ahmad
 * Date: 09/13/2019
 * Time: 16:58 PM
 */

class LocationCurrency {
	public $locId;

	public function __construct($locId){
		$this->locId = $locId;
	}

	/**
	* This function is used to get multiple secondary currency list when multiple secondary currency setting is enabled.
	*
	* @return array list of selected multiple secondary currency list.
	*/
	function getMultipleSecondaryCurrency($timezone) {
	    $result = array();
	    $currencyInfo = array();
	    $pcurrency_query = lavu_query("select value2 from config where  setting='primary_currency_code'");
	    $primaryCurrencyInfo = mysqli_fetch_assoc($pcurrency_query);
	    $primaryCurrencyCode = $primaryCurrencyInfo['value2'];
	    $old_tz = date_default_timezone_get();
	    date_default_timezone_set($timezone);
	    $currentDate = DateTimeForTimeZone($timezone);
	    date_default_timezone_set($old_tz);
	    $companyInfo = sessvar('company_info');
	    $primaryRestaurantID = $companyInfo['id'];

	    $secondaryCurrencyQuery = lavu_query("SELECT `id`, `to_currency_id`, `to_currency_code`, `rate` FROM `secondary_exchange_rates` WHERE (`to_currency_id`, `effective_date`) IN (SELECT `to_currency_id`, max(`effective_date`) FROM `secondary_exchange_rates` WHERE DATE(`effective_date`) <= DATE_FORMAT('".$currentDate."', '%Y-%m-%d') AND `from_currency_id` = '".$primaryCurrencyCode."' AND `primary_restaurantid` = '".$primaryRestaurantID."' AND `_deleted` = '0' AND custom = '0' GROUP BY `to_currency_id`)");

	    $countryIdList = array();
	    $countryId = 0;
	    if ( mysqli_num_rows($secondaryCurrencyQuery) > 0 ) {
	        while ($secondaryCurrencyInfo = mysqli_fetch_assoc($secondaryCurrencyQuery)) {
	            if ($secondaryCurrencyInfo['to_currency_id'] != '' && $secondaryCurrencyInfo['to_currency_id'] != '0') {
	                $currencyId = $secondaryCurrencyInfo['to_currency_id'];
	                $countryIdList[$currencyId] = $currencyId;
	                $result[$currencyId]['exchange_rate_id'] = $secondaryCurrencyInfo['id'];
	                $result[$currencyId]['exchange_rate_value'] = $secondaryCurrencyInfo['rate'];
	            }
	        }

	        if (!empty($countryIdList)) {
		    $countryId = implode("', '", $countryIdList);
		    lavu_query("SET NAMES 'utf8'");
		    $country_query = lavu_query(" SELECT id, alphabetic_code, name, monetary_symbol FROM `poslavu_MAIN_db`.`country_region` WHERE id IN ('".$countryId."') AND active='1'");
			    if ( mysqli_num_rows($country_query) > 0 ) {
			        $count = 0;
			        while ($countryRegionInfo = mysqli_fetch_assoc($country_query)) {
			            $id = $countryRegionInfo['id'];
			            $currencyInfo[$count]['secondary_currency_code_id'] = $countryRegionInfo['id'];
			            $currencyInfo[$count]['exchange_rate_id'] = $result[$id]['exchange_rate_id'];
			            $currencyInfo[$count]['exchange_rate_value'] = $result[$id]['exchange_rate_value'];
			            $currencyInfo[$count]['secondary_currency_code'] = $countryRegionInfo['alphabetic_code'];
			            $currencyInfo[$count]['secondary_currency_name']= $countryRegionInfo['name'];
			            $currencyInfo[$count]['secondary_currency_monetary_symbol'] = trim($countryRegionInfo['monetary_symbol']);
			            $count++;
			        }
			    }
			}
	    }

	    return $currencyInfo;
	}


}