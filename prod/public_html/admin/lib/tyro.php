<?php
	$poslavu_version;
	$fromApp = 'false';

	if( isset( $mode ) ) {
		$fromApp = 'true';
	}
	if(!isset($poslavu_version)){
		$poslavu_version = '2.2.2';
	}

	$functionToCall = 'drawNotYetImplemented()';
	$first_half_submission_str = "";
	$last_half_submission_str = "";
	{
		$t_vars1                         	= array();
		$t_vars1['loc_id']               	= $loc_id;
		$t_vars1['order_id']             	= $order_id;
		$t_vars1['check']                	= $check;
		$t_vars1['amount']               	= $card_amount; //number_format($session_data['card_amount'], $decimal_places, ".", "");
		$t_vars1['datetime']             	= $device_time;
		$t_vars1['register']             	= $register;
		$t_vars1['register_name']        	= $register_name;
		$t_vars1['transtype']            	= $transtype;
		$t_vars1['auth']             		  = "0";
		$t_vars1['change']               	= "0";
		$t_vars1['total_collected']      	= $card_amount; //number_format($session_data['card_amount'], $decimal_places, ".", "");
		$t_vars1['server_id']            	= $server_id;
		$t_vars1['server_name']          	= $server_name;
		$t_vars1['for_deposit']          	= $for_deposit;
		$t_vars1['is_deposit']          	= $is_deposit;
		$t_vars1['action']            		= ($transtype == "Return")?'Refund':'Sale';
		$first_half_submission_str = implode(":*:", $t_vars1);

		/*
		$t_vars['card_type']         		= extract_value($curl_result, "CardType");
		$t_vars['card_desc']         		= $card_desc;
		$t_vars['transaction_id']    		= extract_value($curl_result, "RefNo");
		$t_vars['preauth_id']        		= $t_vars['transaction_id'];
		$t_vars['auth_code']         		= extract_value($curl_result, "AuthCode");
		$t_vars['record_no']         		= extract_value($curl_result, "Token");
		$t_vars['process_data']      		= extract_value($curl_result, "ProcessData");
		$t_vars['ref_data']          		= extract_value($curl_result, "AcqRefData");
		*/

		$t_vars2                         	= array();
		$t_vars2['pay_type']          		= "Card";
		$t_vars2['pay_type_id']       		= "2";
		$t_vars2['mpshc_pid']         		= $payment_id;
		$t_vars2['swipe_grade']       		= "A";
		$t_vars2['got_response']      		= "1";
		$t_vars2['ioid']              		= $internal_order_id;
		$t_vars2['internal_id']       		= $internal_tx_id;
		$last_half_submission_str = implode(":*:", $t_vars2);
	}
	
	switch( $transtype ) {
		case 'Sale' : {
			//$card_amount
			if(empty($card_amount) || $card_amount * 1 <= 0) {
				break;
			}

			if( (strlen($card_amount) - strpos($card_amount, ".")) > 3 ) {
				break;
			}

			$act_amount = str_ireplace(".", "", $card_amount);

			$functionToCall = "terminal.processTransaction('{$act_amount}', false, '0')";
			break;
		}
		case 'Return':
		{
			$amount = $card_amount + $tip_amount;
			error_log("amount: ".$amount);
		if(empty($amount) || $amount * 1 <= 0) {
				break;
			}

			if( (strlen($amount) - strpos($amount, ".")) > 3 ) {
				break;
			}

			$act_amount = str_ireplace(".", "", $amount);

			$functionToCall = "terminal.processRefund('{$act_amount}', false)";
			error_log($functionToCall);
			break;
		} default : { }
	}

?><!doctype html>
<html>
	<head>
		<script src="https://iclient.test.tyro.com/iclient-v1.js" type="text/javascript"></script>
		<script type="text/javascript">

		function LavuTyroHandler( vendor ){
			this._debug_mode = true;
			this._userAgent = navigator.userAgent;
			this._isFromApp = <?php echo $fromApp ?>;
			this._appVersion = '<?php echo $poslavu_version; ?>';
			this._lastQuestionMessage = '';
			this.iclient = new TYRO.IClient("apiKey", {posProductVendor: vendor, posProductName: "POS Lavu", posProductVersion: this._appVersion });

		}
		LavuTyroHandler.prototype = {};
		LavuTyroHandler.prototype.constructor = LavuTyroHandler;

		(function() {

			/**
			 * AREA CREATION FUNCTIONS BEGIN
			 **/

			 function createElement( type ){
			 	if(!type){
			 		return null;
			 	}
			 	var result = document.createElement( type );
			 	result.style.padding = '5px';
			 	result.style.margin = '0px';
			 	return result;
			 }

			function displayConfigButton(){
				var configContentArea = document.getElementById('config_container');

				var config_button = document.createElement('button');
				config_button.innerText = 'Configure Pairing';
				config_button.addEventListener( 'click', displayPairingMenu );
				config_button.style.position = 'absolute';
				config_button.style.left = '0px';
				config_button.style.top = '0px';
				configContentArea.appendChild( config_button );
			}

			function displayMIDandTID( mid, tid ){
				var configContentArea = document.getElementById('config_container');

				var toDelete1 = document.getElementById( 'mid_container' );
				var toDelete2 = document.getElementById( 'tid_container' );
				if(toDelete1){
					toDelete1.parentNode.removeChild(toDelete1);
				}

				if(toDelete2){
					toDelete2.parentNode.removeChild(toDelete2);
				}

				var createDivContainerWithId = function( idStr ){
					var result = createElement('div');
					if(idStr) {
						result.id = idStr;
					}
					result.style.display = 'inline-table';
					result.style.fontSize = '11px';
					return result;
				}

				var mid_div =  createDivContainerWithId('mid_container');
				var tid_div = createDivContainerWithId('tid_container');

				var appendLabelTitleAndValue = function( parentNode, titleText, valueText ){
					if(!parentNode || ! titleText || !valueText ){
						return;
					}

					var title_span = document.createElement('span');
					var value_span = document.createElement('span');
					title_span.innerText = titleText;
					value_span.innerText = valueText;

					title_span.style.color = '#aaaaaa';
					value_span.style.color = '#aece37';

					parentNode.appendChild( title_span );
					parentNode.appendChild( value_span );
				}

				appendLabelTitleAndValue( mid_div, 'mid:', mid );
				appendLabelTitleAndValue( tid_div, 'tid:', tid );

				configContentArea.appendChild( mid_div );
				configContentArea.appendChild( tid_div );
				configContentArea.style.textAlign = 'right';
			}

			function killPopup(){
				var popup_blackout = document.getElementById('popup_blackout');
				if(popup_blackout){
					popup_blackout.parentNode.removeChild(popup_blackout);
				}

				var popup_screen_fill = document.getElementById('popup_screen_fill');
				if( popup_screen_fill ){
					popup_screen_fill.parentNode.removeChild(popup_screen_fill);
				}
			}

			function displayPopup( content ) {
				if( !content ){
					return;
				}

				killPopup();

				var body = document.body;

				var overlay = document.createElement('div');
				overlay.id = 'popup_blackout';
				overlay.style.width = '100%';
				overlay.style.height = '100%';
				overlay.style.backgroundColor = '#000000';
				overlay.style.opacity = 0.5;
				overlay.style.position = 'absolute';
				overlay.style.top = '0px';

				body.appendChild( overlay );

				var container = document.createElement('div');
				container.id = 'popup_screen_fill';
				container.style.width = '100%';
				container.style.height = '100%';
				container.style.position = 'absolute';
				container.style.top = '0px';

				var inner_container = document.createElement('div');
				inner_container.id = 'popup_container';
				inner_container.style.width = '300px';
				inner_container.style.maxHeight = '300px';
				inner_container.style.margin = '0 auto';
				inner_container.style.top = '25px';
				inner_container.style.position = 'relative';
				inner_container.style.backgroundColor = 'white';
				inner_container.style.borderRadius = '5px';
				inner_container.style.boxShadow = '2.5px 2.5px 15px';
				container.appendChild( inner_container );

				inner_container.appendChild( content );

				body.appendChild( container );
			}

			function createButtonWithLabelAndAction( labelText, action ){
				if(! labelText || ! action ){
					return null;
				}
				var button = document.createElement('button');
				button.innerText = labelText;
				button.addEventListener( 'click', action );
				return button;
			}


			function displayPairingMenu(){

				var container = document.createElement('div');

				var createInputWithLabel = function( id, labelText, type ){
					if(!id || !labelText || !type ){
						return;
					}
					var result = createElement('div');
					result.style.textAlign = 'center';
					result.style.width = '100%';
					var label = document.createElement('div');
					label.innerText = labelText;
					label.style.display = 'inline-table';
					label.style.width = '100px';
					var input = createElement('input');
					input.id = id;
					input.type = type;

					result.appendChild( label );
					result.appendChild( input );

					return result;
				}

				var instructions = createElement( 'h4' );

				instructions.innerText = 'Tyro Terminal Pairing Configuration';
				instructions.style.textAlign = 'center';
				var input1 = createInputWithLabel( 'mid_textbox', 'Merchant ID:', 'number' );
				var input2 = createInputWithLabel( 'tid_textbox', 'Terminal ID:', 'number' );

				container.appendChild( instructions );
				container.appendChild( input1 );
				container.appendChild( input2 );
				container.appendChild( document.createElement('br') );

				var cancel_button = createButtonWithLabelAndAction( 'Cancel', killPopup );
				var pair_button = createButtonWithLabelAndAction( 'Pair', function( event ) {
					var mid = document.getElementById('mid_textbox');
					var tid = document.getElementById('tid_textbox');

					if( mid.value <= 0 || tid.value <= 0 ) {
						return;
					}

					//killPopup();
					terminal.processPairTerminal( mid.value, tid.value );
				});

				var button_container = document.createElement('div');
				button_container.style.width = '100%';
				button_container.style.position = 'relative';
				button_container.style.height = '25px';

				cancel_button.style.position = 'absolute';
				cancel_button.style.top = '0px';
				cancel_button.style.left = '5px';

				pair_button.style.position = 'absolute';
				pair_button.style.top = '0px';
				pair_button.style.right = '5px';
				button_container.appendChild( cancel_button );
				button_container.appendChild( pair_button );

				container.appendChild( button_container );

				displayPopup( container );
				//var container = document.createElemnnt('div');
			}

			function drawTerminalInformation( terminalInformation ){
				if( !terminalInformation ){
					return;
				}

				var container = createElement('div');
				container.style.textAlign = 'center';
				var message = createElement('p');

				var second_message = createElement('p');

				var button;
				switch( terminalInformation.status ){
					case 'failure' : {
						message.innerText = 'Unable to Connect to the Terminal, please ensure that the Terminal is on and connected and try again.';
						second_message.innerText = terminalInformation.message;
						var retry_action = function(){
							terminal.getTerminalInformation();
						}
						var cancel_action = function(){
							killPopup();
						}
						button = createElement('div');
						var cancel_button = createButtonWithLabelAndAction( 'Cancel', cancel_action );
						var retry_button = createButtonWithLabelAndAction( 'Retry', retry_action );
						button.appendChild( cancel_button );
						button.appendChild( retry_button );
						break;
					} case 'inProgress' : {
						message.innerText = 'Connection Test in Progress.';
						second_message.innerText = terminalInformation.message;
						var action = function(){
							terminal.cancel();
						}
						button = createButtonWithLabelAndAction('Cancel', action);
						break;
					} case 'success' : {
						button = createElement('div');
						message.innerText = 'The EFTPOS Terminal is not currently available. Will recheck in 30 seconds, or you can trigger it manually.';

						var t = setTimeout( terminal.getTerminalInformation, 30000 );

						var cancel_action = function(){
							clearTimeout( t );
							killPopup();
						}

						var retry_action = function(){
							clearTimeout( t );
							terminal.getTerminalInformation();
						}

						var cancel_button = createButtonWithLabelAndAction('Cancel', cancel_action );
						var retry_button = createButtonWithLabelAndAction('Retry', retry_action );
						button.appendChild( cancel_button );
						button.appendChild( retry_button );
						break;
					} default : {
						button = document.createElement('div');
					}
				}

				container.appendChild( message );
				container.appendChild( second_message );
				container.appendChild( button );

				displayPopup( container );
			}

			function drawProcessTerminalResponse( pairResponse ){
				var container = document.createElement('div');
				container.style.textAlign = 'center';
				var message = createElement('p');

				var second_message = createElement('p');
				second_message.innerText = pairResponse.message;

				var button;

				switch( pairResponse.status ){
					case 'failure' : {
						message.innerText = 'Attempt to pair was unsuccessful.  Please verify the Merhant ID, the Terminal ID, and the EFTPOS System and try again.';

						var second_message = document.createElement('p');
						second_message.style.color = '#ff0000';
						second_message.innerText = 'Error: ' + second_message.innerText;

						var action = function(){
							displayPairingMenu();
						};

						button = createButtonWithLabelAndAction('Try Again', action );

						break;
					} case 'success' : {
						message.innerText = 'Pairing Successful.  Please press continue to continue the transaction.';

						var action = function(){
							killPopup();
							terminal.getConfiguration();
						};

						button = createButtonWithLabelAndAction('Continue', action );
						break;
					} case 'inProgress' : {
						message.innerText = 'Pairing in Process, this may take up to a minute.';

						var action = function(){
							killPopup();
							terminal.iclient.cancelCurrentTransaction();
						};

						button = createButtonWithLabelAndAction('Cancel', action );
						break;
					} default : { }
				}

				container.style.textAlign = 'center';
				container.appendChild( message );
				container.appendChild( document.createElement('br') );
				container.appendChild( second_message );
				container.appendChild( document.createElement('br') );
				container.appendChild( button );

				displayPopup( container );
			}

			function drawQuestionDetails( questionObject, answerCallback ){
				if(!questionObject || !answerCallback ){
					return;
				}
				terminal._lastQuestionMessage = questionObject.text;
				var container = createElement('div');
				container.style.textAlign = 'center';

				var question = document.createElement('p');
				question.innerText = questionObject.text;
				container.appendChild( question );
				var action = function(){
					answerCallback( this.value );
				}

				for( var i = 0; i < questionObject.options.length; i++ ){
					var option = createButtonWithLabelAndAction( questionObject.options[i], action);
					option.value = questionObject.options[i];
					container.appendChild( option );
				}

				displayPopup( container );
			}

			function drawStatusMessage( message ){
				var container;
				{
					var temp = document.getElementById('transaction_status_container');
					if(temp){
						container = temp;
						for(var i = 0; i < container.childNodes.length; i++){
							var ele = container.childNodes[i];
							ele.style.color = '#aece37';
							ele.style.fontSize = '14px';
						}
					} else {
						container = createElement('div');
						container.id = 'transaction_status_container';
						container.style.textAlign = 'left';
					}
				}

				var message_container = createElement('p');
				message_container.style.color = '#000000';
				message_container.style.fontSize = '20px';
				message_container.innerText = message;

				container.appendChild( message_container );

				var outter_container = document.createElement('div');
				var button;

				if(message == 'APPROVED' || message == 'CANCELLED'){
					var action = function(){
						killPopup();
					}

					var cancel_button = createButtonWithLabelAndAction('Continue', action);

				} else {
					var action = function(){
						killPopup();
						terminal.cancel();
					}

					var cancel_button = createButtonWithLabelAndAction('Cancel', action);
				}

				outter_container.style.textAlign = 'center';
				outter_container.appendChild(container);
				outter_container.appendChild(cancel_button);


				displayPopup( outter_container );
			}

			function drawManualSettlementResponse( settlementResponse ) {
				if( !settlementResponse ){
					return;
				}


			}

			function drawNotYetImplemented() {
				var container = createElement('div');
				var message = createElement('p');
				message.innerText = 'This function is not currently supported or Implemented.';

				var action = function(){
					killPopup();
				}

				var button = createButtonWithLabelAndAction( 'Continue', action );
				container.style.textAlign = 'center';
				container.appendChild( message );
				container.appendChild( button );

				displayPopup( container );
			}

			/**
			 * AREA CREATION FUNCTIONS END
			 **/

			/**
			 * LavuTyroHandler Function Implementation BEGIN
			 **/



			function handleConfiguration( configuration ) {
				debugLog( 'handleConfiguration' );
				debugLog( configuration );

				if(!configuration || !configuration.mid || !configuration.tid ){
					debugLog( "Not Yet Paired." );
					displayConfigButton();
					displayPairingMenu();

				} else {
					debugLog( "Currently Paired." );
					displayConfigButton();
					displayMIDandTID( configuration.mid, configuration.tid );
					terminal.getTerminalInformation();
				}
			}

			function getConfiguration() {
				this.iclient.getConfiguration( this.handleConfiguration );
			}

			function handleTerminalInformation( terminalInformation ) {
				//terminalInformation.status = failure | success | inProgress
				// if failure || inProgress, terminalInformation.message
				// else if success, terminalInformation.terminalInfo

				if( !terminalInformation ){
					return;
				}

				if( terminalInformation.status != 'success' || !terminalInformation.terminalInfo.available ){
					drawTerminalInformation( terminalInformation );
				} else {
					//DO STUFF
					<?php echo $functionToCall; ?>;
				}

				debugLog( 'handleTerminalInformation' );
				debugLog( terminalInformation );
			}

			function getTerminalInformation(){
				this.iclient.terminalInfo( this.handleTerminalInformation );
			}

			function cancel() {
				this.iclient.cancelCurrentTransaction();
			}

			function handleprocessPairTerminal( pairResponse ) {
				if(!pairResponse) {
					return;
				}

				drawProcessTerminalResponse( pairResponse );
				debugLog( 'handleprocessPairTerminal' );
				debugLog( pairResponse );
			}

			function processPairTerminal( mid, tid ) {
				this.iclient.pairTerminal( mid, tid, this.handleprocessPairTerminal );
			}

			function handleManualSettlement( settlementResponse ) {
				if( !settlementResponse ){
					return;
				}

				drawManualSettlementResponse( settlementResponse );

				debugLog( 'handleManualSettlement' );
				debugLog( settlementResponse );
			}

			function processManualSettlement() {
				this.iclient.manualSettlement( this.handleManualSettlement );
			}

			function handleQuestionCallback( questionObject, answerCallback ) {
				//question.text = Would you like to diaf?
				//question.options = [0] => 'Yes'. [1] => 'No'
				if( !questionObject || !answerCallback ){
					return;
				}
				drawQuestionDetails( questionObject, answerCallback );
				debugLog( 'handleQuestionCallback' );
				debugLog( questionObject );
				//answerCallback( question[0] );
			}

			//Update the Screen With the Current Status of the Transaction
			function handleStatusMessageCallback( message ) {
				if(terminal._lastQuestionMessage == message || !message ){
					terminal._lastQuestionMessage = '';
					return;
				}
				terminal._lastQuestionMessage = '';

				drawStatusMessage( message );

				debugLog( 'handleStatusMessageCallback' );
				debugLog( message );
			}

			function handleReceiptCallback( receipt ) {
				//receipt.signatureRequired
				//receipt.merchantReceipt

				drawNotYetImplemented();

				debugLog( 'handleReceiptCallback' );
				debugLog( receipt );
			}

			function handleTransactionCompleteCallback( transactionData ) {
				//transactionData.result = APPROVED | CANCELLED | REVERSED | DECLINED | SYSTEM ERROR | NOT STARTED | UNKNOWN
				//transactionData.cardType = Visa, MasterCard, EFTPOS
				//transactionData.transactionReference (STAN) (reference to the transaction for Tyro)
				//transactionData.authorisationCode (reference to the transaction for the card processor)
				//transactionData.issuerActionCode ( raw result code from the card issuer )
				//transactionData.elidedPan (the elided credit card number used for the transaction )
				//transactionData.rrn ( The Retrieval Reference Numberr generated by teh payment service. Guaranteed to be unique for a 7-day period. )
				//transactionData.tipAmount ( if processing a tip completetion this will be the amount in cents );
				//transactionData.tipCompletionReference ( tyro's reference to the tip completion );
				//transactionData.tabCompletionReference ( tyro's reference to a tab completion );
				//transactionData.customerReceipt ( Text representation of the Tyro receipt indtended for the customer.  Only included if integrated receipt printing is enabled.  Formatted in monospaced font );

				if( terminal._isFromApp ) {
					if(transactionData.result == 'APPROVED' || transactionData.result == 'REVERSED'){
						var fwdstr_array = [];
						fwdstr_array.push( "<?php echo $first_half_submission_str; ?>" );

						var card_details = new Array(8);
						var info_array = new Array(7);
						{
							var card_type = 'EFTPOS';
							if( transactionData.cardType ) {
								card_type = transactionData.cardType;
							}
							var card_desc = '';
							if( transactionData.elidedPan ) {
								card_desc = transactionData.elidedPan.substr( -4 );
							}
							var preauth_id = '';
							if( transactionData.transactionReference ) {
								preauth_id = transactionData.transactionReference;
							}
							var payment_id = transaction_id;
							var transaction_id = '';
							if( transactionData.tabCompletionReference ) {
								transaction_id = transactionData.tabCompletionReference;
							} else if( transactionData.tabCompletionReference ){
								transaction_id = tabCompletionReference;
							} else {
								transaction_id = preauth_id;
							}
							var auth_code = '';
							if( transactionData.authorisationCode ) {
								auth_code = transactionData.authorisationCode;
							}
							var record_no = '';
							if( transactionData.rrn ) {
								record_no = transactionData.rrn;
							}
							var process_data = '';
							if( transactionData.issuerActionCode ) {
								process_data = transactionData.issuerActionCode;
							}
							var ref_data = '';
							var tipAmount = '<?php echo $tip_amount; ?>';
							if( transactionData.tipAmount ) {
								tipAmount = (transactionData.tipAmount / 100).toFixed(2);
							}
							card_details = [ card_type, card_desc, transaction_id, preauth_id, auth_code, record_no, process_data, ref_data ];
							fwdstr_array.push( card_details.join( ':*:' ) );
							fwdstr_array.push( "<?php echo $last_half_submission_str; ?>" );
							fwdstr_array.push( tipAmount );
							fwdstr_array.push( 'Tyro' );
							fwdstr_array.push( 'eftpos' );
							info_array = [ transaction_id, card_desc, 'Approved', auth_code, card_type, payment_id, record_no, fwdstr_array.join( ':*:' ), tipAmount ];
						}

						var info = info_array.join( ':o:' );
						//$info = $t_vars['transaction_id'].":o:".$card_desc.":o:Approved:o:".$t_vars['auth_code'].":o:".$t_vars['card_type'].":o:".$payment_id.":o:".$t_vars['record_no']
						window.location = '_DO:cmd=Approved&info=' + info;
						alert( info );
					} else {
						var message = '';
						var status = 'Declined';
						switch( transactionData.result ) {
							case 'CANCELLED' : {
								status = 'Cancelled';
								message = 'The User Cancelled the transaction.';
								break;
							} case 'DECLINED' : {
								message = 'The card was declined.';
								break;
							} case 'SYSTEM ERROR' : {
								message = 'A System Error occurred.';
								break;
							} case 'NOT STARTED' : {
								message = 'The system has not yet been started.';
								break;
							} case 'UNKNOWN' : {

							} default : {
								message = 'An unknown Error occurred, the transaction could not be validated.  Please check the EFTPOS terminal.';
							}
						}
						window.location = '_DO:cmd=' + status + '&info=' + message;
					}
				} else {

				}

				debugLog('handleTransactionCompleteCallback' );
				debugLog( transactionData );
			}

			function processTransaction( chargeAmount, integratedReceipt, cashoutAmount ) {
				var requestParams = {};
				var callbacks = {	'questionCallback' : this.handleQuestionCallback,
									'statusMessageCallback' : this.handleStatusMessageCallback,
									'transactionCompleteCallback' : this.handleTransactionCompleteCallback
								};

				requestParams['integratedReceipt'] = false;
				if( integratedReceipt ) {
					requestParams['integratedReceipt'] = true;
					callbacks['receiptCallback'] = handleReceiptCallback;
				}

				if( cashoutAmount ) {
					requestParams['cashout'] = cashoutAmount;
				}

				requestParams['amount'] = chargeAmount;

				this.iclient.initiatePurchase( requestParams, callbacks);
			}

			function processRefund( chargeAmount, integratedReceipt ) {
				var requestParams = {};
				var callbacks = {	'questionCallback' : this.handleQuestionCallback,
									'statusMessageCallback' : this.handleStatusMessageCallback,
									'transactionCompleteCallback' : this.handleTransactionCompleteCallback
								};

				requestParams['integratedReceipt'] = false;
				if( integratedReceipt ) {
					requestParams['integratedReceipt'] = true;
					callbacks['receiptCallback'] = handleReceiptCallback;
				}
				requestParams['amount'] = chargeAmount;

				this.iclient.initiateRefund( requestParams, callbacks);

			}

			function handleTipResponse( tipResponse ) {
				debugLog( 'handleTipResponse' );
				debugLog( tipResponse );
			}

			function addTipForReference( completionReference, tipAmount ) {
				this.iclient.addTip( completionReference, tipAmount, this.handleTipResponse );
			}

			function processOpenTab( chargeAmount, integratedReceipt ) {
				var requestParams = {};
				var callbacks = {	'questionCallback' : this.handleQuestionCallback,
									'statusMessageCallback' : this.handleStatusMessageCallback,
									'transactionCompleteCallback' : this.handleTransactionCompleteCallback
								};

				requestParams['integratedReceipt'] = false;
				if( integratedReceipt ) {
					requestParams['integratedReceipt'] = true;
					callbacks['receiptCallback'] = handleReceiptCallback;
				}
				requestParams['amount'] = chargeAmount;

				this.iclient.initiateOpenTab( requestParams, callbacks );
			}

			function handleCloseTabCallback( closeTabResult ) {
				//closeTabResult.result = success | failure
				//closeTabResult.message = "";
				debugLog( "handleCloseTabCallback" );
				debugLog( closeTabResult );
			}

			function processCloseTab( completionReference, chargeAmount ) {
				this.iclient.closeTab( completionReference, chargeAmount, this.handleCloseTabCallback );
			}

			function handleVoidTabCallback( voidTabResult ) {
				//voidTabResult.result = success | failure
				//voidTabResult.message = "";
				debugLog( "handleVoidTabCallback" );
				debugLog( voidTabResult );
			}

			function processVoidTab( completionReference ) {
				this.iclient.voidTab( completionReference, this.handleVoidTabCallback );
			}

			LavuTyroHandler.prototype.addTipForReference = addTipForReference;
			LavuTyroHandler.prototype.cancel = cancel;
			LavuTyroHandler.prototype.debugLog = debugLog;
			LavuTyroHandler.prototype.getConfiguration = getConfiguration;
			LavuTyroHandler.prototype.getTerminalInformation = getTerminalInformation;
			LavuTyroHandler.prototype.handleCloseTabCallback = handleCloseTabCallback;
			LavuTyroHandler.prototype.handleConfiguration = handleConfiguration;
			LavuTyroHandler.prototype.handleManualSettlement = handleManualSettlement;
			LavuTyroHandler.prototype.handleprocessPairTerminal = handleprocessPairTerminal;
			LavuTyroHandler.prototype.handleQuestionCallback = handleQuestionCallback;
			LavuTyroHandler.prototype.handleReceiptCallback = handleReceiptCallback;
			LavuTyroHandler.prototype.handleStatusMessageCallback = handleStatusMessageCallback;
			LavuTyroHandler.prototype.handleTerminalInformation = handleTerminalInformation;
			LavuTyroHandler.prototype.handleTipResponse = handleTipResponse;
			LavuTyroHandler.prototype.handleTransactionCompleteCallback = handleTransactionCompleteCallback;
			LavuTyroHandler.prototype.handleVoidTabCallback = handleVoidTabCallback;
			LavuTyroHandler.prototype.processCloseTab = processCloseTab;
			LavuTyroHandler.prototype.processManualSettlement = processManualSettlement;
			LavuTyroHandler.prototype.processOpenTab = processOpenTab;
			LavuTyroHandler.prototype.processPairTerminal = processPairTerminal;
			LavuTyroHandler.prototype.processRefund = processRefund;
			LavuTyroHandler.prototype.processTransaction = processTransaction;
			LavuTyroHandler.prototype.processVoidTab = processVoidTab;

			/**
			 * LavuTyroHandler Function Implementation END
			 **/

		})();

		function initialize(){
			terminal = new LavuTyroHandler( "Test" );
			terminal.getConfiguration();
		}

		function debugLog( item ) {
			if(terminal._debug_mode){
				console.log( item );
			}
		}

		</script>
	</head>
	<body onLoad="initialize()" style="margin: 0px; padding: 0px;">
		<div id="content-container" style="margin: 0 auto; width: 400px; height: 400px; position: relative; border: 1px solid black;" >
			<div id="config_container" style="width: 100%; height: 25px; position:L relative;"></div>
			<div id="main_content" style="width:100%;"></div>
		</div>
	</body>
</html>
<?php exit(); ?>