<?php

	session_start();
	if(!isset($_SESSION['verified']))
		exit();
		
	if($_SESSION['verified'] != 1)
		exit();

?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			
			function blurCheck()
			{
				if(pwResetSubmitAttempt)
				{
					passwordVerifyCheck();
				}
			}
			
			function passwordVerifyCheck()
			{
				var password = $('#password');
				var verify = $('#verify');
				
				if(password.val() === "" )
				{
					password.attr('style', 'border: 1px solid red;');
					return false;
				}
				
				password.attr('style', '');
				
				if(password.val() !== verify.val())
				{
					verify.attr('style', 'border: 1px solid red;');
					return false;
				}
				
				verify.attr('style', '');
								
				return true;
			}
			
			function getpwresetFormElements()
			{
				var result = "";
				$('input').each(function()
				{
					if(result == "");
						result += "&";
					result += $(this).attr('name') + "=" + $(this).val();
				});
				
				return result;
			}
			
			var pwResetSubmitAttempt = false;
			function checkAndSend()
			{
				pwResetSubmitAttempt = true;
				if(passwordVerifyCheck())
				{
				//alert('submitting');
				//document.pwreset.submit();
					//$('#pwreset').submit();
					
					var datastr = getpwresetFormElements();
					
					/**
					 * handleResetRequest DOES NOT EXIST IN THIS FILE.  It relies on the calling page to of defined
					 * the corresponding javascript function in order for it to operate.
					 * 
					 * function handleResetRequest(result)
					 * {
					 * 		if(result){
					 *			if(result.success === "success")
					 *			{
					 *				$('#pwresetarea').html("Password Successfully Reset");
					 *			}
					 *			else
					 *			{
					 *				$('#pwresetarea').html("Password Not Reset: " + result.error);
					 *			}
					 * 		}
					 *		else
					 *		{
					 *			$('#pwresetarea').html("No Result Response, something Went Wrong.");		
					 *		}
					 * }
					 * 
					 */
					$.ajax({
						type: "POST",
						url : "<?php echo $reset_script_path; ?>",
						data : datastr,
						dataType: "json",
						success: handleResetRequest
					});
				}
			}
		</script>
		<div id="pwresetarea" style="font-family:Arial, Helvetica, sans-serif;">
			<h4>Please Enter Your New Password Credentials</h4>
			<form id="pwreset" name="pwreset" action="" method="POST" onSubmit="return false;">
				<input type="hidden" id="dataname" name="dataname" value="<?php echo $dataname ?>" />
				<input type="hidden" id="username" name="username" value="<?php echo $username ?>" />
				<input type="hidden" id="user_id" name="user_id" value="<?php echo isset($user_id)?$user_id:""; ?>" />
				<input type="hidden" id="product_code" name="product_code" value="<?php echo isset($product_code)?$product_code:""; ?>" />
				<input type="hidden" id="reset_type" name="reset_type" value="<?php echo isset($reset_type)?$reset_type:""; ?>" />
				<table style="border-collapse: collapse;">
				<tbody>
					<tr>
						<td align='right's>
							<label>Password: </label>
						</td>
						<td>
							<input type="password" name="password" id="password" value="" placeholder="Password" onblur="blurCheck();" />
						</td>
					</tr>
					<tr>
						<td align='right'>
							<label>Verify: </label>
						</td>
						<td>
							<input type="password" name="passwordVerification" id="verify" value="" placeholder="Verify Password" onblur="blurCheck();" />
						</td>
					</tr>
					<tr>
					<td></td><td>
						</form>
						<button onclick="checkAndSend();">Change Password</button>
					</td>
					</tr>
				
				</tbody>
				</table>
		</div>