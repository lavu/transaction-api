<?php

	/*
	 * This File Changes a user's password to the password specified by the input.
	 *
	 *
	 * RECEIVES:
	 *	dataname: The dataname that this user is associated with.
	 *  username: The username of the user themselves.
	 *  password: The password to set as the new password for the user.
	 * 
	 * RETURNS:
	 *  A JSON object containing the following fields:
	 *  status: either success or failed
	 *  error: Returned when status == failed. It is a message to relay to the user.
	 */
	
	ini_set('display_errors',0);
	require_once("../../cp/resources/core_functions.php");
	
	/******************************************************
	 * FUNCTIONS
	 *****************************************************/
	 
	 define("FAILED","failed");
	 define("SUCCESS","success");
	 
	 function quit($status, $message="") {
			echo '{ "status" : "' . $status . '"';
			if ($message != "") echo ', "error" : "' . $message . '"';
			echo "}";
			
			lavu_exit();
		}
		
	/******************************************************
	 * START
	 ******************************************************/
	 
	session_start();
	if (!isset($_SESSION['verified']))
		quit(FAILED,"Session not verified");
	
	if ($_SESSION['verified'] != 1)
		quit(FAILED,"Session not verified");
	 
	if (!isset($_REQUEST['dataname']) || !isset($_REQUEST['username']) || !isset($_REQUEST['password']))
		quit(FAILED,"Missing required information field.");
		
	$password = $_REQUEST['password'];
	$username = $_REQUEST['username'];
	$dataname = $_REQUEST['dataname'];
	$user_id = $_REQUEST['user_id'];
	$product_code = $_REQUEST['product_code'];
	$reset_type = $_REQUEST['reset_type'];

	mlavu_select_db("poslavu_MAIN_db");
	lavu_connect_dn($dataname, "poslavu_".$dataname."_db");

	$user_table = "users";
	$filter_field = "username";
	$filter_value = $username;
	$password_code = "`password` = PASSWORD('[1]')";
	if ($product_code == "lg") {
		if ($reset_type == "customer") {
			$user_table = "med_customers";
			$filter_field = "id";
			$filter_value = $user_id;
			$password_code = "`user_password_hash` = '[1]'";
			$password = sha1($password.'garbonzo');
		}			
	}
	
	$passwordQuery = "UPDATE `".$user_table."` SET ".$password_code." WHERE `".$filter_field."` = '[2]'";
	$update_password = lavu_query($passwordQuery, $password, $filter_value);
	
	if (!$update_password)
		quit(FAILED, "Unable to update password due to a database error.");

	// ***** should schedule customer account sync for LLS locations		 
			 
	if ($product_code == "lg") {
		
		if ($reset_type == "customer") {
		
			$f_name = ",";
			$email = $username;
			$get_customer_info = lavu_query("SELECT `f_name` FROM `med_customers` WHERE `id` = '[1]'", $user_id);
			if (mysqli_num_rows($get_customer_info) > 0) {
				$customer_info = mysqli_fetch_assoc($get_customer_info);
				$f_name = " ".$customer_info['f_name'].",";
			}
		
			$subject = "Lavu Give Password Reset Complete";
						
			$email_body = "<html><head></head><body>";
			$email_body .= "Hello".$f_name."<br /><br />";
			$email_body .= "This email is to inform you that your Password for the account, ".$username.", was just modified via a Password Reset. If you did not change the password for ".$username.", then please contact support as soon as possible.  Please do not respond to this email, instead contact us at 855-528-8457 (855-LAVU-HLP).<br /><br />";
			$email_body .= "Best Wishes,<br />";
			$email_body .= "Lavu";
			$email_body .= "</body></html>";
		
			$headers = "Content-type: text/html; charset=iso-8859-1"."\r\n";
			$headers .= "From: ".$location_info['title']." Lavu Give <noreply@lavugive.com>"."\r\n";
									
			mail(trim($email), $subject, $email_body, $headers);
			
			$kill_token = lavu_query("DELETE FROM `config` WHERE `type` = 'password_reset_token' AND `setting` = 'customer' AND `value` = '[1]'", $user_id);
		}
		
	} else {

		$firstname = "";
		$email = "";
		
		$userQuery = "SELECT * FROM `users` WHERE `username` = '[1]' AND `_deleted`='0'";
		$userResult = lavu_query($userQuery, $username);
		
		if (mysqli_num_rows($userResult)) {
			
			$userArray = mysqli_fetch_assoc($userResult);
			$firstname = $userArray['f_name'];
			$email = $userArray['email'];
			if ($email != "") {
				$headers = 'From: noreply@poslavu.com' . "\r\n" .
					"Reply-To: noreply@poslavu.com" . "\r\n" .
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/html; charset=ISO-8859-1\r\n";
				
				$subject = "Lavu Password Reset Complete";
				
				$email_body = "<html>
<head></head>
<body>
	Greetings $firstname<br /><br />
	This email is to inform you that your Password for the account, $username, was just modified via a Password Reset. If you did not change the password for $username, then please contact support as soon as possible. Please do not respond to this email, instead contact us at the following number 855-528-8457 (855-LAVU-HLP).<br /><br />
	Best Wishes,<br />
	Lavu
</body>
</html>";

				mail($email,$subject,$email_body, $headers);
			}
		}
	}

	$_SESSION['verified'] = 0;

	quit(SUCCESS);
	
?>