<?php

	function isResellerAdminDevice($device_uuid) {

		$reseller_admin = false;
		
		if ($device_uuid != "") {
		
			$uuid_parts = explode("-", $device_uuid);
			$uuid_min = $uuid_parts[0]."-".$uuid_parts[1];
			$sess_key = "specialist_device_".$uuid_min;
	
			if (isset($_SESSION[$sess_key])) $reseller_admin = ($_SESSION[$sess_key] == 1);
			else {
				$devices_query = mlavu_query("SELECT `UUID_min` FROM `poslavu_MAIN_db`.`reseller_devices` WHERE `UUID_min`='[1]' AND `type`='admin_device'", $uuid_min);
				$reseller_admin = (mysqli_num_rows($devices_query) > 0);
				$_SESSION[$sess_key] = $reseller_admin?1:0;
			}
		}
		
		return $reseller_admin;
	}
	
	function getCurrentPackageInfo() {
	
		global $location_info;
	
		// "Lavu Silver" and what not
		
		$package_info = array("package"=>"Gold D", "ipods"=>"10", "ipads"=>"2");
		//if (sessvar("package_info")) $package_info = sessvar("package_info"); // start checking for session again once Lavu 3.1 / Retro 2.3.17 is widely adopted
		//else {
			$get_package_info = lavu_query("SELECT `value` AS `package`, `value2` AS `ipods`, `value3` AS `ipads`, `value4` AS `tableside_ipads` FROM `config` WHERE `location` = '[1]' AND `type` = 'system_setting' AND `setting` = 'package_info'", $location_info['id']);
			if (mysqli_num_rows($get_package_info) > 0) {
				$p_info = mysqli_fetch_assoc($get_package_info);
				if (!empty($p_info['package'])) $package_info = $p_info;
				set_sessvar("package_info", $package_info);
			}
		//}		
		
		return $package_info;
	}

	function devicesActiveForCurrentBusinessDay() {

		global $location_info;
		global $device_time;

		$date_info = get_use_date_end_date($location_info, $device_time);

		$result = array();

		$query = lavu_query("SELECT * FROM `devices` WHERE `last_checkin` >= '[1]' AND `loc_id` = '[2]'", $date_info[2], $location_info['id']);
		while ($info = mysqli_fetch_assoc($query)) {
			$result[] = $info;
		}

		return $result;
	}

	function isLicenseCompliant($data_name, $device_model, $usage_type) {

		global $app_name;
		global $lfMessage;
		global $lfTitle;
		global $lfKickout;
		
		global $company_info;

		$debug = false;

		$UUID = (isset($_REQUEST['UUID']))?$_REQUEST['UUID']:"";
		$MAC = (isset($_REQUEST['MAC']))?$_REQUEST['MAC']:"";
		$locID = (isset($_REQUEST['loc_id']))?$_REQUEST['loc_id']:"";

		$package_info = getCurrentPackageInfo();
	
		if ($debug) error_log("****** CHECK DEVICE LIMITS ****** ".$data_name." PACKAGE INFO: ".print_r($package_info, true));

		$licenseString = $package_info['package'];
		$licenseiPodMaxCount = ($package_info['ipods'] == "") ? 0 : $package_info['ipods'];
		$licenseiPadMaxCount = ($package_info['ipads'] == "") ? 0 : $package_info['ipads'];
		$licenseTablesideiPadMaxCount = $package_info['tableside_ipads'];

		if ($debug) error_log("licenseiPadMaxCount: ".$licenseiPadMaxCount.", licenseiPodMaxCount: ".$licenseiPodMaxCount.", licenseTablesideiPadMaxCount: ".$licenseTablesideiPadMaxCount);

		$devices = devicesActiveForCurrentBusinessDay();
		
		$claimedIpads = 0;
		$claimedTablesideIpads = 0;
		$claimedIpods = 0;
		foreach ($devices as $device) {
			$model = $device['model'];
			$claimed = $device['slot_claimed'];
			$utype = $device['usage_type'];

			if ($debug) error_log("model: ".$model.", claimed: ".$claimed.", UUID: ".$device['UUID']);

			if ($claimed == date('Y-m-d')) {

				if ($debug) error_log("is claimed");

				if ($device['UUID'] == $UUID) return true; // stop here because this device already has a slot claimed
				if (strstr($model, "iPad") && ($utype=="terminal" || $utype=="")) $claimedIpads++;
				else if (strstr($model, "iPad")) $claimedTablesideIpads++;
				else $claimedIpods++;
			}
		}

		if ($debug) error_log("claimedIpods: ".$claimedIpods.", claimedTablesideIpads: ".$claimedTablesideIpads.", claimedIpads: ".$claimedIpads.", device_model: ".$device_model.", usage_type: ".$usage_type);

		$message1 = "";
		$message2 = "";
		$message3 = "";

		$tableside_iPad = (strstr($device_model, "iPad") && $usage_type=="tableside");

		if (strstr($app_name, "Lavu Lite") && ($claimedIpads+$claimedTablesideIpads+$claimedIpods)>=1) {

			$message1 = speak("You have a Lavu Lite account, which allows you to use one mobile device at a time.");
			$message2 = speak("You recently used another device for this account.");
			$message3 = speak("Please log out of this account on the other device before logging in on this device.");

		} else if (strstr($device_model, "iPad") && !$tableside_iPad && $claimedIpads>=$licenseiPadMaxCount) {

			$dvc = "iPad";
			if ($licenseiPadMaxCount > 1) $dvc .= "s";
			$message1 = speak("You have a $licenseString account, which allows you to use up to $licenseiPadMaxCount $dvc.");
			$dvc = "iPad";
			if ($claimedIpads != 1) $dvc .= "s";
			$message2 = speak("You recently used $claimedIpads other $dvc.");
			$message3 = speak("Please exit this location on the other $dvc or contact support for more info.");

		} else if ((!strstr($device_model, "iPad") && !$tableside_iPad) && $claimedIpods >= $licenseiPodMaxCount) {

			$dvc = "iPhone/iPod";
			if ($licenseiPodMaxCount > 1) $dvc = "iPhones/iPods";
			
			$message1 = speak("You have a $licenseString account, which allows you to use up to $licenseiPodMaxCount $dvc.");
			$dvc = "iPhone/iPod";
			$clmd = $claimedIpods;
			if ($clmd != 1) $dvc = "iPhones/iPods";
			
			$message2 = speak("You recently used $clmd other $dvc.");
			$message3 = speak("Please exit this location on the other $dvc or contact support for more info.");
		} elseif ($tableside_iPad && $claimedTablesideIpads >= $licenseTablesideiPadMaxCount) {

			$dvc = "tableside device";

			if ($licenseTablesideiPadMaxCount > 1){
				$dvc = "tableside devices";
			} 

			$message1 = speak("You have a $licenseString account, which allows you to use up to $licenseTablesideiPadMaxCount $dvc.");

			$message2 = speak("You recently used $claimedTablesideIpads other $dvc.");

			$message3 = speak("Please exit this location on the other $dvc or contact support for more info.");
		}

		if (!empty($message1)) {
			
			error_log("enforcing device limits: ".$data_name." (".$licenseString."/".$licenseiPodMaxCount."/".$licenseiPadMaxCount."/".$licenseTablesideiPadMaxCount."/".$device_model."/".$usage_type.")");
			
			$lfMessage = $message1." [--CR--][--CR--]".$message2." [--CR--][--CR--]".$message3;
			$lfTitle = speak("Number of Allowed Devices Exceeded");
			$lfKickout = 1;

			if ($debug) error_log($lfMessage);
	
			return false;
			
		} else {
		
			$query = lavu_query("UPDATE `devices` SET `slot_claimed` = '".date("Y-m-d")."' WHERE `UUID` = '[1]' AND `last_checkin` LIKE '".date("Y-m-d")."%'", $UUID);
	
			if ($debug) error_log("claiming slot: ".lavu_dberror());
		
			return true;
		}
	}

?>
