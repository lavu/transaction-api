<?php

	session_start();

	require_once(__DIR__."/NewRelicHelper.php");

	$NewRelic = new NewRelicHelper();
	$NewRelic->trackRequestStart();

	$json_session = true;

	require_once(__DIR__."/info_inc.php");
	require_once(__DIR__."/jcvs/jc_func8.php"); // for get_loyaltree_purchase_info()

	$NewRelic->track("client_ip", getClientIPServer());
	if (!empty($data_name))
	{
		$NewRelic->track("dataname", $data_name);
	}

	// expect request variables: m, cc, data_name, version, build, loc_id, order_id, check, device_time
	
	$company_id	= $company_info['id'];
	$timezone	= $location_info['timezone'];

	$mode				= reqvar("m", "");
	$loc_id				= reqvar("loc_id", "");
	$order_id			= reqvar("order_id", "");
	$check				= reqvar("check", "1");
	$device_time		= reqvar("device_time", date("Y-m-d H:i:s"));
	if ($timezone != "")
	{
		$device_time	= localize_datetime(date("Y-m-d H:i:s"), $timezone);
	}

	$NewRelic->track("rar_mode", $mode);

	if (lsecurity_id($company_code_key, $data_name)!=$company_id && lsecurity_id1($company_code_key)!=$company_id)
	{
		session_destroy();
		lavu_exit();
	}

	log_comm_vars($data_name, $loc_id, "RECEIPT APPEND RESULTS");

	$dset = $location_info['day_start_end_time'];
	$se_hour = substr(str_pad($dset, 4, "0", STR_PAD_LEFT), 0, 2);
	$se_min = substr(str_pad($dset, 4, "0", STR_PAD_LEFT), 2, 2);
	$idatetime = explode(" ", $device_time);
	$idate = explode("-", $idatetime[0]);
	$itime = explode(":", $idatetime[1]);
	$yts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] - 1), $idate[0]);
	$nowts = mktime($itime[0], $itime[1], $itime[2], $idate[1], $idate[2], $idate[0]);
	$tts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] + 1), $idate[0]);

	if ($itime[0].$itime[1].$itime[2] >= $se_hour.$se_min."00")
	{
		$use_date = date("Y-m-d", $nowts);
		$end_date = date("Y-m-d", $tts);
		$today_ts = $nowts;
	}
	else
	{
		$use_date = date("Y-m-d", $yts);
		$end_date = date("Y-m-d", $nowts);
		$today_ts = $yts;
	}

	$udate = explode("-", $use_date);
	$uyear = $udate[0];
	$umonth = $udate[1];
	$uday = $udate[2];
	$udate_display = date("n/j/Y", mktime(0, 0, 0, $umonth, $uday, $uyear));

	$end_hour = $se_hour;
	$end_min = str_pad(($se_min - 1), 2, "0", STR_PAD_LEFT);
	if ($end_min < 0) {
		$end_hour = str_pad(($se_hour - 1), 2, "0", STR_PAD_LEFT);
		$end_min = 59;
	}
	if ($end_hour < 0) {
		$end_hour = 23;
		$end_date = date("Y-m-d", $nowts);
	}

	$start_datetime = "$use_date $se_hour:$se_min:00";
	$end_datetime = "$end_date $end_hour:$end_min:59";

	if (!isset($decimal_places))
	{
		$decimal_places = 2;
	}
	$smallest_money = (1 / pow(10, $decimal_places));

	if ($mode == "meal_plan") {

		$get_payment_type_ids = lavu_query("SELECT `id` FROM `payment_types` WHERE `loc_id` = '[1]' AND `special` = 'rf_id'", $loc_id);
		if (@mysqli_num_rows($get_payment_type_ids) > 0) {
			$payment_type_ids = array();
			while ($info = mysqli_fetch_assoc($get_payment_type_ids)) {
				$payment_type_ids[] = $info['id'];
			}
			$ppdatetime = explode(" ", $location_info['apple_pay_periods']);
			$ppdate = explode("-", $ppdatetime[0]);
			$pptime = explode(":", $ppdatetime[1]);
			$ppts = mktime($pptime[0], $pptime[1], $pptime[2], $ppdate[1], $ppdate[2], $ppdate[0]);

			$get_customer_info = lavu_query("SELECT `cc_transactions`.`customer_id` AS `customer_id`, `customer_accounts`.`f_name` AS `f_name`, `customer_accounts`.`l_name` AS `l_name` FROM `cc_transactions` LEFT JOIN `customer_accounts` ON `customer_accounts`.`id` = `cc_transactions`.`customer_id` WHERE `cc_transactions`.`loc_id` = '[1]' AND `cc_transactions`.`order_id` = '[2]' AND `cc_transactions`.`check` = '[3]' AND `cc_transactions`.`pay_type_id` IN (".implode(",", $payment_type_ids).") ", $loc_id, $order_id, $check);
			if (@mysqli_num_rows($get_customer_info) > 0) {
				while ($info = mysqli_fetch_assoc($get_customer_info)) {

					$period_total = 0;
					$day_total = 0;

					$get_period_transactions = lavu_query("SELECT `total_collected`, `datetime` FROM `cc_transactions` WHERE `datetime` >= '[1]' AND `datetime` <= '[2]' AND `customer_id` = '[3]' AND `pay_type_id` IN (".implode(",", $payment_type_ids).")", $location_info['apple_pay_periods'], $end_datetime, $info['customer_id']);
					while ($t_info = mysqli_fetch_assoc($get_period_transactions)) {
						$period_total += $t_info['total_collected'];
						if ($t_info['datetime'] >= $start_datetime) $day_total += $t_info['total_collected'];
					}
					echo "Meal Plan totals for ".$info['f_name']." ".$info['l_name']."|o|[C]|*||*|Today (".date("F j", $today_ts)."):|o|".showAmount($day_total)."|*|Period (Since ".date("M j h:ia", $ppts)."):|o|".showAmount($period_total)."|*|";
				}
			}
		}
		lavu_exit();
	
	} else if ( ($mode=="loyaltree_qr_code" || $mode=="loyaltree_email_points" ) && $_POST['prepare_image']==1 ) {

		$post_vars = $_REQUEST;

		echo get_loyaltree_purchase_info($loc_id, $check, $order_id, false);

		lavu_exit();
	}
	
	lavu_exit(); // calls NewRelicHelper's trackRequestEnd()
	
	function showAmount($amount) {

		global $location_info;

		$amount = str_replace(",", "", $amount);
		$amount = number_format($amount, $location_info['disable_decimal'], $location_info['decimal_char'], ",");
		$symbol = $location_info['monitary_symbol'];

		if ($location_info['left_or_right'] == "right")
			return $amount.$symbol;
		else
			return $symbol.$amount;
	}
?>