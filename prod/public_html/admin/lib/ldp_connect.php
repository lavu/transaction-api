<?php

session_start();

if (!isset($_REQUEST['cc']))
{
	error_log("ldp_connect - missing company code");
	echo '{"error":"missing company code"}';
	lavu_exit();
}

require_once(__DIR__."/NewRelicHelper.php");

$NewRelic = new NewRelicHelper();
$NewRelic->trackRequestStart();
$NewRelic->track("client_ip", getClientIPServer());
require_once(__DIR__."/info_inc.php");
require_once(__DIR__."/jcvs/jc_func8.php");
require_once(__DIR__."/ldp/LavuDataPlexSyncer.php");

if (!isset($location_info))
{
	$location_info = array();
}
$device_time = determineDeviceTime($location_info, $_REQUEST);

$ldp_debug = (locationSetting("lds_debug", 0) == "1");

$short_device_id = shortDeviceUUID(reqvar("UUID", ""));

if ($ldp_debug) error_log("DataPlex - ".$data_name." - req_id: ".urlvar("YIG")." - device_id: ".$short_device_id." - app version: ".reqvar("app_version", "")." - debugging enabled");

$lavu_query_should_log = $ldp_debug;

$dataPlexSyncer = new LavuDataPlexSyncer();
$dataPlexSyncer->process($_REQUEST);

lavu_exit(); // calls NewRelicHelper's trackRequestEnd()