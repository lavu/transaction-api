<?php

// For details related to Lavu's logging system, refer to http://wiki.lavu/Server_Logs

session_start();
require_once("info_inc.php");

if (empty($data_name))
{
	exit();
}

$log_name		= empty($_REQUEST['log_name'])?"console":"console_".$_REQUEST['log_name'];
$from_disk		= reqvar("from_disk", "");
$self_defined	= reqvar("self_define_log_rows", FALSE);
$timezone		= locationSetting("timezone", "America/Denver");
$file_name_date	= DateTimeForTimeZone($timezone);

if (!empty($self_defined))
{
	$log_string = reqvar("log_string", "");
}
else
{
	$datetime		= "";
	$log_string		= processConsoleJSON($datetime, $timezone);
	$file_name_date	= str_replace( array( "-", ":", " " ), array( "", "", "_"), $datetime);
}

if (empty($log_string))
{
	exit();
}

$log_path = "/home/poslavu/logs/company/".$data_name."/debug";
if (!is_dir($log_path))
{
	mkdir($log_path, 0755, TRUE);
}

if (is_dir($log_path))
{
	$file_name = "device_".$log_name."_".$file_name_date.$from_disk.".log";

	$log_file = fopen($log_path."/".$file_name, "a");
	fputs($log_file, $log_string);
	fclose($log_file);
}

function processConsoleJSON(&$datetime, $timezone)
{
	$event			= reqvar("event", "");
	$model			= reqvar("model", "");
	$identifier		= determineDeviceIdentifier($_REQUEST);
	$app			= reqvar("app", "Lavu Client");
	$version		= reqvar("version", "");
	$build			= reqvar("build", "");
	$console_json	= reqvar("consoleJSON", "");

	if (strlen($console_json) == 0)
	{
		return "";
	}

	$log_string = $from_disk."\n";
	$log_string .= $event."\n";
	$log_string .= $model." ".$identifier."\n";
	$log_string .= $app." ".$version." ".$build."\n";
	$log_string .= "Location Timezone: ".$timezone."\n";

	if (appVersionCompareValue($version) >= 30204)
	{
		$log_data = json_decode(rawurldecode($console_json), true);

		if (!isset($log_data['now_year']))
		{
			return "";
		}

		if (!isset($log_data['log_lines']))
		{
			return "";
		}

		$now_year = $log_data['now_year'];
		$log_lines = $log_data['log_lines'];

		foreach ($log_lines as $log_line)
		{
			$this_line = $log_line;
			if (substr($this_line, 0, 4) != $now_year)
			{
				$this_line = $now_year.$log_line;
			}
			$datetime = substr($this_line, 0, 19);
			$log_string .= "\n".$this_line;
		}
	}
	else
	{
		$log_array = lavu_json_decode(str_replace(array( "[AMPERSAND]", chr(92).chr(110) ), array( "&", chr(13) ), $console_json));

		foreach ($log_array as $log_line)
		{
			$timestamp = (int)$log_line['Time'];
			$datetime = DateTimeForTimeZone($timezone, $timestamp);

			$log_string .= "\n".$datetime." - ".$log_line['Sender']." - ".$log_line['Message'];
		}
	}

	return $log_string;
}