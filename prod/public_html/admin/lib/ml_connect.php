<?php

	function update_emailing_list($action, $customer_type, $data_name, $location_info, $customer_id) {

		if ($location_info['ml_type'] == "Critical Impact") {
			if ($data_name == '')
				error_log('location_info["title"]:'.$location_info['title']);
			update_critical_impact_list($action, $customer_type, $data_name, $location_info['ml_un'], $location_info['ml_pw'], $location_info['ml_listid'], $customer_id);
		}
	}

	function update_critical_impact_list($action, $customer_type, $data_name, $username, $password, $list_id, $customer_id) {
	
		$rdb = "poslavu_".$data_name."_db";
		
		if ($customer_type == "racer") {

			$get_customer_info = mlavu_query("SELECT * FROM `[1]`.`lk_customers` WHERE `id` = '[2]' LIMIT 1", $rdb, $customer_id);
			if ($get_customer_info === FALSE) {
				error_log("ben_dev, /lib/ml_connect.php, SELECT * FROM `$rdb`.`lk_customers` WHERE `id`='$customer_id' LIMIT 1");
			}
			if (mysqli_num_rows($get_customer_info) > 0) {
				$customer_info = mysqli_fetch_array($get_customer_info);		
		
				if ($list_id != "") {
				
					$la_parts = explode(" ", $customer_info['last_activity']);
					$lad_parts = explode("-", $la_parts[0]);
					$la_ts = mktime(0, 0, 0, $lad_parts[1], $lad_parts[2], (int)$lad_parts[0]);
					$last_activity = date("F j, Y", $la_ts);
			
					$bd_parts = explode("-", $customer_info['birth_date']);
					$bd_ts = mktime(0, 0, 0, $bd_parts[1], $bd_parts[2], (int)$bd_parts[0]);
					$age = ((date("Y") - $bd_parts[0]) - 1);
					if ((date("m") >= $bd_parts[1]) && (date("d") >= $bd_parts[2])) {
						$age++;
					}
					
					$membership_status = "Member";
					if (($customer_info['membership'] == "") || ($customer_info['membership'] == "Unqualified") || ($customer_info['membership'] == "Racer")) {
						$membership_status = "Non-member";
					}
			
					$url = "http://clients.criticalimpact.com/api/api.cfm?ci=<criticalimpact><login><username>".$username."</username><password>".$password."</password></login>";
					$url .= "<command><action>".$action."</action>";
					$url .= "<listid>".$list_id."</listid>";
					$url .= "<email>".$customer_info['email']."</email>";
					$url .= "<firstname>".$customer_info['f_name']."</firstname>";
					$url .= "<lastname>".$customer_info['l_name']."</lastname>";
					$url .= "<custom1>".$last_activity."</custom1>";
					$url .= "<custom2>".$bd_parts[2]."</custom2>";
					$url .= "<custom3>".date("F", $bd_ts)."</custom3>";
					$url .= "<custom4>".$bd_parts[0]."</custom4>";
					$url .= "<custom5>".$age."</custom5>";
					$url .= "<custom6>".$customer_info['state']."</custom6>";
					$url .= "<custom7>".$customer_info['postal_code']."</custom7>";
					$url .= "<custom8>".$customer_info['country']."</custom8>";
					$url .= "<custom9>".$customer_info['membership_expiration']."</custom9>";
					$url .= "<custom10>".$membership_status."</custom10>";
					$url .= "<custom11>".$customer_id."</custom11>";
					$url .= "<custom12>".$customer_info['racer_name']."</custom12>";
					$url .= "<triggeredid>0</triggeredid>";
					$url .= "<unsubscribe>0</unsubscribe>";
					$url .= "</command></criticalimpact>";
					
					
					
					$crl = curl_init();
					$timeout = 10;
					curl_setopt ($crl, CURLOPT_URL, str_replace(" ", "%20", $url));
					curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
					$ret = curl_exec($crl);
					curl_close($crl);
	
					$xml = simplexml_load_string($ret);
					
					if ($action == "subscriberadd") {
						$email_list_id = $xml->emailid;
						if ($email_list_id == "") { $email_list_id = "Invalid Email Address"; }
						$update_customer_record = mlavu_query("UPDATE `$rdb`.`lk_customers` SET `email_list_id` = '[1]' WHERE `id` = '[2]'", $email_list_id, $customer_id);
					}
	
					//mail("richard@greenkeyconcepts.com","ci sent", str_replace("<", "\n<<", $url));
					//mail("richard@greenkeyconcepts.com","ci response", $ret);
				}
			}
		}
	}
?>