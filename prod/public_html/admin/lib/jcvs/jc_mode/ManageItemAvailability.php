<?php
/**
 * Created by Irshad Ahmad.
 * User: Irshad Ahmad
 * Date: 02/07/19
 * Time: 6:15 PM
 */
require_once(__DIR__.'/../../../api_private/Redis/RedisPubSub.php');

/**
 * This function is used to save and respond related status message.
 *
 * @param array saveData menu item details.
 * @param string deviceId POS deviceId.
 *
 * @return array Message with status of the operation with soldoutTomorrow and soldoutIndefinitely details.
 */
function manageItemAvailability ($saveData, $deviceId) {
	$saveStatus = array('fail' => 'Something went wrong, unable to save Menu Item Availability changes.');
	$saveInfo = 0;
	$resultData = array();
	if (!empty($saveData)) {

		global $location_info;
		$dataName = $location_info['data_name'];
		$timezone = $location_info['timezone'];
		$dayStartEndTime = $location_info['day_start_end_time'];
		$old_tz = date_default_timezone_get();
		date_default_timezone_set($timezone);
		$nextDayStartTime = getNextBusinessDayStartTime($timezone, $dayStartEndTime);
		$unavailableUntil = strtotime("-5 minutes", $nextDayStartTime);
		$maxTimestamp = strtotime('+10 years', $nextDayStartTime);

		$dateTimeInfo = array(
 			'nextDayStartTime'	=> $nextDayStartTime,
 			'unavailableUntil'	=> $unavailableUntil,
 			'maxTimestamp'		=> $maxTimestamp,
 			'dataName'			=> $dataName
		);

		foreach ($saveData as $type => $data) {
			if (!empty($data)) {
				$saveInfo = saveItemAvailabilityInfo($type, $data, $deviceId, $dateTimeInfo);
			}
		}

		if ($saveInfo) {
			$saveStatus = array(
				'success'				=> 'Menu Item Availability changes are saved successfully.',
				'soldoutTomorrow' 		=> $unavailableUntil,
				'soldoutIndefinitely'	=> $maxTimestamp
			);
		}
		date_default_timezone_set($old_tz);
	}
	return $saveStatus;
}

/**
* This function is used to check and save menu item availability information to respective information.
*
* @param string $type  menu item type.
* @param array $data  respective menu item availability details.
* @param string $timezone  selected location timezone.
*
* @return boolean save status 0/1 message.
*/
function saveItemAvailabilityInfo ($type, $data, $deviceId, $dateTimeInfo) {
	$dataName = $dateTimeInfo['dataName'];
	$nextDayStartTime = $dateTimeInfo['nextDayStartTime'];
	$unavailableUntil = $dateTimeInfo['unavailableUntil'];
	$maxTimestamp = $dateTimeInfo['maxTimestamp'];
	$soldoutTomorrow = array();
	$soldoutIndefinitely = array();
	$available = array();
	$publishingData = array();

    $saveInfo = 0;
	$update_soldoutTomorrow = 1;
	$update_soldoutIndefinitely = 1;
	$update_available = 1;
	$insert_item_available = 1;

	 if ($type == 'menu_items' || $type == 'modifiers' || $type == 'forced_modifiers') {
		$table = $type;
	  	$subQuery = ($table == "menu_items") ? "`inv_count` = '0'," : "" ;

		foreach ($data as $id => $resultData) {
			if ($resultData == 'soldoutTomorrow') {
				$soldoutTomorrow[] = $id;
			} else if ($resultData == 'soldoutIndefinitely') {
				$soldoutIndefinitely[] = $id;
			} else if ($resultData == 'available') {
				$available[] = $id;
			}
		}

		if (!empty($soldoutTomorrow)) {
			$soldoutTomorrowIds = implode("','", $soldoutTomorrow);
			$update_soldoutTomorrow = lavu_query("UPDATE `[1]` SET ".$subQuery." `unavailable_until` = '".$nextDayStartTime."' WHERE `id` IN('".$soldoutTomorrowIds."')", $table);
			if ($update_soldoutTomorrow) {
				$publishingData[$table][] = array('id' => $soldoutTomorrow, 'unavailableUntil' => $unavailableUntil, 'deviceId' => $deviceId);
			}
		}

		if (!empty($soldoutIndefinitely)) {
			$soldoutIndefinitelyIds = implode("','", $soldoutIndefinitely);
			$update_soldoutIndefinitely = lavu_query("UPDATE `[1]` SET ".$subQuery." `unavailable_until` = '".$maxTimestamp."'  WHERE `id` IN('".$soldoutIndefinitelyIds."')", $table);
			if ($update_soldoutIndefinitely) {
				$publishingData[$table][] = array('id' => $soldoutIndefinitely, 'unavailableUntil' => $maxTimestamp, 'deviceId' => $deviceId);
			}
		}

		if (!empty($available)) {
			$availableIds = implode("','", $available);
			$subQuery = ($table == "menu_items") ? " `inv_count` = '0', `track_86_count` = '', " : "" ;
			$update_available = lavu_query("UPDATE `[1]` SET ".$subQuery." `unavailable_until` = '0' WHERE `id` IN('".$availableIds."')", $table);
			if ($update_available) {
				$publishingData[$table][] = array('id' => $available, 'unavailableUntil' => 0, 'deviceId' => $deviceId);
			}
		}

	}

	if ($update_soldoutTomorrow && $update_soldoutIndefinitely &&	$update_available && $insert_item_available) {
		$saveInfo = 1;
		$RedisSocketServerStatus = getenv('REDIS_SOCKET_ENABLED');
		if ($RedisSocketServerStatus) {
			publishManageItemAvailability($publishingData, $dataName);
		}
	}

	return $saveInfo;
}

/**
* This function is used to get restarent next Business DayStartTime.
*
* @param string $timezone  selected location timezone.
* @param string $dayStartEndTime  restarent start end time.
*
* @return string next business day start time.
**/
function getNextBusinessDayStartTime($timezone, $dayStartEndTime) {
	$datetime_str = DateTimeForTimeZone($timezone);
	$datetime_arr = explode(" ", $datetime_str);
	$day = $datetime_arr[0];
	$day_start_end_time = $dayStartEndTime * 1;
	$minutes = $day_start_end_time % 100;
	$day_start_end_time -= $minutes;
	$day_start_end_time /= 100;
	$hours = $day_start_end_time % 100;
	$seconds = (($hours * 60) + $minutes)*60;
	$currentTime = $datetime_arr[1];
	$splitTime = explode(":", $currentTime);
	$splitHours = $splitTime[0];
	$splitMinutes = $splitTime[1];
	$presentSec = (($splitHours * 60) + $splitMinutes)*60;
	if ($presentSec > $seconds) {
		$day = date('Y-m-d', strtotime("+1 day", strtotime($day)));
	}
	$nextDayStartTime = strtotime($day." ".$hours.":".$minutes);
	return $nextDayStartTime;
}

/**
* This function is used to check, seleted item list is in cron job or not, if exists in cron its deleted.
*
* @param string $table table name.
* @param string $dataName location database name.
* @param array $itemIdList list of item Ids.
*
* @return boolean deleted status(1/0).
**/
function deleteItemAvailability($table, $dataName, $itemIdList) {
	$itemStatus = 0;
	if (!empty($itemIdList) ) {
		if ($table == "menu_items" || $table == "modifiers" || $table == "forced_modifiers") {
			$mainTable = $table.'_availability';
			$itemIds = implode("','", $itemIdList);
				if (($getRecords = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`$mainTable` WHERE `dataname` = '".$dataName."' AND `item_id` IN ('".$itemIds."')")) !== false ) {
				if (mysqli_num_rows($getRecords) > 0) {
					while ($row = mysqli_fetch_assoc($getRecords)) {
						$itemAvailability[] = $row['id'];
					}
					$availableIds = implode("','", $itemAvailability);
				    $itemStatus = mlavu_query("DELETE FROM `poslavu_MAIN_db`.`$mainTable` WHERE `id` IN('".$availableIds."')");
				}
			}
		}

	}
	return $itemStatus;
}

/**
* It will pass payload for publish to Redis socket communication.
*
* @param array $itemIdList item list
* @param string $dataName location database name.
* @return json
*/
function publishManageItemAvailability($itemIdList, $dataName) {
	$response = array();
	$keyName = "kraken_redis_main_channel";
	if (!empty($itemIdList)) {
		$itemDetails['dataName'] = $dataName;
		foreach ($itemIdList as $itemType => $itemInfo) {
			$itemDetails['eventName'] = str_replace('_', '.', $itemType).".status";
			$itemDetails['eventBody'] = getEventBody($itemInfo);
		}
		$data = json_encode($itemDetails);

		$response = RedisPubSub::publishData($keyName, $data);
	}
	return $response;
}

/**
* It will get item details for Redis payload.
*
* @param array $itemInfo item list.
*
* @return array result data
*/
function getEventBody($itemInfo) {
	$finalData = array();
	if (!empty($itemInfo)) {
		foreach ($itemInfo as $data) {
			$itemBodyInfo = getItemsList($data);
			$finalData = array_merge($finalData, $itemBodyInfo);
		}
	}
	return $finalData;
}

/**
* It will return Redis payload.
*
* @param array $itemInfo item list.
*
* @return array $resultData.
*/
function getItemsList($resultInfo) {
	$resultData = array();
	$unavailableUntil = $resultInfo['unavailableUntil'];

	foreach ($resultInfo['id'] as $itemId) {
		$resultData[$itemId] = array(
			'id' => $itemId,
			'unavailableUntil' => $unavailableUntil,
			'deviceId' => $resultInfo['deviceId']
		);
	}
   	return $resultData;
}

/**
* Initiate from jc_inc8.php
*
* @param string method name to call dynamically
*
* @return array result.
*/
function doProcess($funcName) {
	$saveData = json_decode(reqvar("save_data", ""), true);
	$deviceId = reqvar("UUID", "");
	$result = array('fail' => 'Something went wrong, Please Try again after some time.');
	if (function_exists($funcName)) {
       $result = $funcName($saveData, $deviceId);
	}
	return $result;
}

?>