<?php

/**
 * Author : Dhruvpalsinh Chudasama
 * Date   : 05-09-2019
 * File   : AllDeviceInfo.php
 * Purpose: It will provide device info for last 30 days like appname,appversion,UUID
 */

/**
* Initiate from jc_inc8.php
* @param string $funcname
* @return array result.
*/
function doProcess($funcname) {
	$result = array('status' => 'failure', 'msg' => 'Something went wrong, Please Try again after some time.');
	if (function_exists($funcname)) {
       $result = $funcname();
	}
	return $result;
}

/**
* @purpose Get All Device Info
* @param
* @return array $deviceInfo
*/
function getAllDeviceInfo() {
	global $location_info;
	$deviceInfo = array();
	$deviceInfo['deviceInfo'] = array();
	$poslavu_version = '';
	$device_time = '';
    $device_time = reqvar("device_time");
	//dataname
	$dataName = $location_info['data_name'];
	$dn = 'poslavu_'.$dataName.'_db';
	//query for get all device info
	$deviceQuery = lavu_query("SELECT DISTINCT app_name,poslavu_version,UUID FROM `$dn`.`devices` WHERE device_time between (DATE_ADD('".$device_time."', INTERVAL -30 DAY)) AND '".$device_time."'");
	//Check for Rows available
	if (mysqli_num_rows($deviceQuery) > 0) {
		while ($rowdinfo = mysqli_fetch_assoc($deviceQuery)) {
			$dinfo = array();
			$dinfo['appname'] = preg_replace('/[\x00-\x1F\x7F]/', '', $rowdinfo['app_name']);
			$poslavu_version = explode(" ", $rowdinfo['poslavu_version']);
			$dinfo['appversion'] = $poslavu_version[0];
			$dinfo['uuid'] = $rowdinfo['UUID'];
			array_push($deviceInfo['deviceInfo'], $dinfo);
		}
		$deviceInfo['status'] = 'success';
		$deviceInfo['msg'] = 'Device Info Available';
	} else {
		$deviceInfo['status'] = 'failure';
		$deviceInfo['msg'] = 'Device Info Not Available';
	}
	//return result
	return $deviceInfo;
}
?>