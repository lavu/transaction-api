<?php
/**Created for LP-8640
* This will get SUM of total amount paid removing refunded amount for given orderID and check number
* Author: OM
*/
$gateway = reqvar('gateway', '');
$check = reqvar('check', '');
$orderId = reqvar('order_id', '');
$locId =  reqvar('loc_id', '');
$dataname = sessvar('admin_dataname');
$db = 'poslavu_'.$dataname.'_db';
if ($gateway && $check && $orderId && $locId) {
    //query cc_transactions
    $result = mlavu_query('SELECT action,SUM(amount) as total_amount FROM '.$db.'.cc_transactions WHERE order_id="[1]" AND `check`=[2] AND action IN ("Sale","Refund") AND voided=0 AND loc_id=[3] group by action', $orderId, $check, $locId);
    if (mysqli_num_rows($result)) {
        while ($row = mysqli_fetch_assoc($result)) {
            if ($row['action'] == 'Sale') {
                $response['data'][$orderId][$check] += $row['total_amount'];
            }else if ($row['action'] == 'Refund') {
                $response['data'][$orderId][$check] -= $row['total_amount'];
            }
        }
    }
}
?>