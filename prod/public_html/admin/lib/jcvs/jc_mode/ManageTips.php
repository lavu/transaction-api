<?php

/**
 * Purpose: It will provide API implementation of Display Tips and Adjust Tips for All Payment Gateways
 * Date   : 26-11-2019
 * File   : ManageTips
 */

require_once(__DIR__."/../../gateway_functions.php");
require_once(__DIR__."/../../mpshc_functions.php");

/**
* Initiate from jc_inc8.php
* @param string $funcname
* @return array result.
*/
function doProcess($funcname) {
    $resultTip = array(
                  'status' => 'failure',
                  'msg'    => 'Something went wrong, Please Try again after some time.',
                 );
    if (function_exists($funcname)) {
       $resultTip = $funcname();
    }
    return $resultTip;
}

/**
* @purpose Get Manage Tips Actions & Redirect
* @param
* @return array $responseDisplayTips
*/
function manageTips() {
    //Action Type
    $actionType = reqvar('actionType');
    //Check for actionType
    if ($actionType == 'adjustTips') {
        $responseAdjustTips = adjustTips();
        return $responseAdjustTips;
    } else {
        $responseDisplayTips = displayTips();
        return $responseDisplayTips;
    }
}

/**
* @purpose To Adjust Tips
* @param
* @return array $resultTipAdjust
*/
function adjustTips() {
    //Get Cash Tip Info
    $cashTipInfo = reqvar('cashtip_list');
    //Get Card Tip Info
    $cardTipInfo = reqvar('cardtip_list');
    //Check for Cash Tip Adjustment
    $resultTipAdjustCash = '';
    if ($cashTipInfo != '') {
        $resultTipAdjustCash = adjustCashTip($cashTipInfo);
    }
    //Check for Card Tip
    $resultTipAdjustCard = '';
    if ($cardTipInfo != '') {
        $resultTipAdjustCard = adjustCardTip($cardTipInfo);
    }
    //get latest server tip info
    $getServerTip = getServerTipInfo();
    //Merge Responses for Cash & Card
    if ($resultTipAdjustCash != '' && $resultTipAdjustCard != '') {
        $resultTipAdjust                  = array_merge($resultTipAdjustCash, $resultTipAdjustCard);
        $resultTipAdjust['netSales']      = $getServerTip['netSales'];
        $resultTipAdjust['tipPercentage'] = $getServerTip['tipPercentage'];
        $resultTipAdjust['totalTips']     = $getServerTip['totalTips'];
        return $resultTipAdjust;
    }
    //Check if only cash tip Adjust
    if ($resultTipAdjustCash != '') {
        $resultTipAdjust                  = $resultTipAdjustCash;
        $resultTipAdjust['netSales']      = $getServerTip['netSales'];
        $resultTipAdjust['tipPercentage'] = $getServerTip['tipPercentage'];
        $resultTipAdjust['totalTips']     = $getServerTip['totalTips'];
        return $resultTipAdjust;
    }
    //Check if only card tip Adjust
    if ($resultTipAdjustCard != '') {
        $resultTipAdjust                  = $resultTipAdjustCard;
        $resultTipAdjust['netSales']      = $getServerTip['netSales'];
        $resultTipAdjust['tipPercentage'] = $getServerTip['tipPercentage'];
        $resultTipAdjust['totalTips']     = $getServerTip['totalTips'];
        return $resultTipAdjust;
    }
}

/**
 * @purpose get latest server tips info
 * @param
 * @return array $getTipInfo
*/
function getServerTipInfo() {
    $getTipInfo = array();
    global $location_info;
    //decimal places
    $decimal_places = $location_info['disable_decimal'];
    //locId
    $locId      = reqvar('loc_id');
    //serverId
    $serverId   = reqvar('server_id');
    //deviceTime
    $deviceTime = reqvar('device_time');
    //Hour, Min
    $se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
    $se_min  = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);
    //Explode devicetime
    $idatetime = explode(" ", $deviceTime);
    //Date
    $idate = explode("-", $idatetime[0]);
    //Time
    $itime = explode(":", $idatetime[1]);
    $yts   = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] - 1), $idate[0]);
    $nowts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2]), $idate[0]);
    $tts   = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] + 1), $idate[0]);
    //If devicetime greater than configured start time
    if ($itime[0].$itime[1].$itime[2] >= $se_hour.$se_min."00") {
        $use_date = date("Y-m-d", $nowts);
        $end_date = date("Y-m-d", $tts);
    } else {
        $use_date = date("Y-m-d", $yts);
        $end_date = date("Y-m-d", $nowts);
    }
     //End Hour
     $end_hour = $se_hour;
     //End Min
     $end_min = str_pad(($se_min - 1), 2, "0", STR_PAD_LEFT);
     if ($end_min < 0) {
         $end_hour = str_pad(($se_hour - 1), 2, "0", STR_PAD_LEFT);
         $end_min  = 59;
     }
     if ($end_hour < 0) {
         $end_hour = 23;
         $end_date = date("Y-m-d", $nowts);
     }
     //Start Datetime
     $start_datetime              = "$use_date $se_hour:$se_min:00";
     $getTipInfo['startDateTime'] = $start_datetime;
     //End Datetime
     $end_datetime              = "$end_date $end_hour:$end_min:59";
     $getTipInfo['endDateTime'] = $end_datetime;
     //Server filter
     $server_filter = " AND `or1`.`server_id` = '$serverId'";
     //Where Caluse
     $where_and_order_by =  " WHERE `or1`.`opened` >= '".$start_datetime."' AND `or1`.`opened` <= '".$end_datetime."' AND `or1`.`void` = '0' AND `or1`.`location_id` = '[1]'".$server_filter." AND `or1`.`order_id` NOT LIKE '777%' AND `cc`.`pay_type` IN ('Card', 'Cash', 'PayPal', 'Paypal') AND `cc`.`voided` != '1' AND `cc`.`action` IN ('Sale', 'Refund')";
     //Query server tips
     //$queryServerTip = lavu_query("SELECT sum(`or1`.`cash_tip`) as totalcashtips, sum(`or1`.`card_gratuity`) as totalcardtips, sum(`or1`.`total`) as total, sum(`or1`.`cash_paid` + `or1`.`card_paid` + `or1`.`alt_paid`) as netTotal FROM `orders` as `or1`".$where_and_order_by, $locId);
     $queryServerTip = lavu_query("SELECT `or1`.`cash_tip`, `or1`.`order_id`, `or1`.`cash_paid`, `or1`.`card_paid`, `or1`.`alt_paid`, `cc`.`pay_type`, `cc`.`action`, `cc`.`tip_amount`, `cc`.`amount` FROM `orders` as `or1` LEFT JOIN `cc_transactions` as `cc` ON  `cc`.`order_id` = `or1`.`order_id`".$where_and_order_by, $locId);
     //check for num rows
     $getTipInfo['netSales']      = 0;
     $getTipInfo['tipPercentage'] = 0;
     $getTipInfo['totalTips']     = 0;
     $tipPercent                  = 0;
     $totalTips                   = 0;
     $netSales                    = 0;
     $orderIdAry                  = array();
     if (mysqli_num_rows($queryServerTip) > 0) {
         while ($rowTip = mysqli_fetch_assoc($queryServerTip)) {
            if ($rowTip['action'] == 'Sale') {
                $netSales  += $rowTip['amount'];
                $totalTips += $rowTip['tip_amount'];
            } else if ($rowTip['action'] == 'Refund') {
                $netSales  -= $rowTip['amount'];
                $totalTips -= $rowTip['tip_amount'];
            }
            if (!in_array($rowTip['order_id'], $orderIdAry)) {
                $orderIdAry[] = $rowTip['order_id'];
                $totalTips   += $rowTip['cash_tip'];
            }
         }
         $getTipInfo['netSales'] = $netSales;
         $tipPercent             = (($totalTips) * 100) / ($netSales);
         //percentage
         $getTipInfo['tipPercentage'] = round($tipPercent, $decimal_places);
         //Total tips
         $getTipInfo['totalTips'] = $totalTips;
     }
     //return result
     return $getTipInfo;
}

/**
* @purpose To Adjust Card Tips
* @param string $cardTipInfo - Request parameter card tips
* @return array $cardTipResponse
*/
function adjustCardTip($cardTipInfo) {
    //locationinfo
    global $location_info;
    $resp = "";
    //decimalplaces
    $decimal_places = $location_info['disable_decimal'];
    //devicetime
    $deviceTime = reqvar('device_time');
    if (empty($deviceTime)) {
        $deviceTime = date("Y-m-d H:i:s", time());
    }
    $tz = $location_info['timezone'];
    if (!empty($tz)) {
        $deviceTime = localize_datetime(date("Y-m-d H:i:s"), $tz);
    }
    //dataname
    $dataName = $location_info['data_name'];
    //deviceId
    $deviceId = reqvar('UUID');
    //locId
    $locId = reqvar('loc_id');
    //server_id
    $serverId = reqvar('server_id');
    //regsiter
    $register = reqvar('register');
    //regsiter_name
    $registerName = reqvar('register_name');
    //Process Info
    $processInfo = array(
                    'card_amount'     => "",
                    'card_cvn'        => "",
                    'card_number'     => "",
                    'company_id'      => $company_id,
                    'data_name'       => $dataName,
                    'device_time'     => $deviceTime,
                    'device_udid'     => $deviceId,
                    'exp_month'       => "",
                    'exp_year'        => "",
                    'ext_data'        => "",
                    'for_deposit'     => "",
                    'is_deposit'      => "",
                    'loc_id'          => $locId,
                    'mag_data'        => "",
                    'name_on_card'    => "",
                    'reader'          => "",
                    'register'        => $register,
                    'register_name'   => $registerName,
                    'server_id'       => $serverId,
                    'server_name'     => "",
                    'set_pay_type'    => "Card",
                    'set_pay_type_id' => "2",
                   );
    //Cardtiplist
    $cardTipList = explode("|", $cardTipInfo);
    //Integrationinfo
    if ($location_info['integrateCC'] == "1") {
        $integrationInfo = get_integration_from_location($locId, "poslavu_".$dataName."_db");
        $func_path       = __DIR__."/../../gateway_lib/".strtolower($integrationInfo['gateway'])."_func.php";
        if (file_exists($func_path)) {
                require_once($func_path);
        }
        $processInfo['username']     = $integrationInfo['integration1'];
        $processInfo['password']     = $integrationInfo['integration2'];
        $processInfo['integration3'] = $integrationInfo['integration3'];
        $processInfo['integration4'] = $integrationInfo['integration4'];
        $processInfo['integration5'] = $integrationInfo['integration5'];
    }
    $foundUnprocessedTransactions = false;
    $usedPreauthIds               = array();
    $cardTipResponse              = array();
    $cardTipResponse['cardTip']   = array();
    //count cardtip
    $countCardTip = count($cardTipList);
    //start iteration
    for ($cdtip = 0; $cdtip < $countCardTip; $cdtip++) {
        //tip changed flag
        $tipHasChanged = false;
        $cardRes       = array();
        //cardtip identifier
        $cardTipIdentifier = $cardTipList[$cdtip];
        //cardtip parts
        $cardTipParts = explode("_", $cardTipIdentifier);
        //orderid
        $ordId = $cardTipParts[0];
        //ioid
        $ioId = $cardTipParts[1];
        //amount
        $cardTip = $cardTipParts[2];
        //id
        $tId = $cardTipParts[3];
        //check for card tip validation
        $whereClause     = '';
        $whereIdentifier = '';
        $ccInfo          = array();
        if (is_numeric($cardTip)) {
            $checkResponse    = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";
            $whereClause      = " `order_id` = '[1]' AND `loc_id` = '[2]' AND `ioid` = '[3]' AND `id` = '[4]' ";
            $getTransactions  = lavu_query("SELECT * FROM `cc_transactions` WHERE ".$whereClause."$checkResponse AND `pay_type_id` = '2' AND `action` = 'Sale' AND `voided` != '1' AND `order_id` NOT LIKE '777%' ORDER BY `check`, `id` ASC", $ordId, $locId, $ioId, $tId);
            $transactionCount = mysqli_num_rows($getTransactions);
            //check for transaction count
            if ($transactionCount == 1) {
                $foundUnprocessedTransactions = true;
                $transactionInfo              = mysqli_fetch_assoc($getTransactions);
                $applyTip                     = number_format((float)$cardTip, $decimal_places, ".", "");
                $processInfo['username']      = $integrationInfo['integration1'];
                $processInfo['password']      = $integrationInfo['integration2'];
                $processInfo['ioid']          = $transactionInfo['ioid'];
                $processInfo['order_id']      = $transactionInfo['order_id'];
                $processInfo['check']         = $transactionInfo['check'];
                $processInfo['pnref']         = $transactionInfo['transaction_id'];
                $cardRes['order_id']          = $transactionInfo['order_id'];
                $cardRes['ioid']              = $transactionInfo['ioid'];
                $cardRes['check']             = $transactionInfo['check'];
                $cardRes['id']                = $transactionInfo['id'];
                $cardRes['transaction_id']    = $transactionInfo['transaction_id'];
                //set value in ccinfo
                $ccInfo['processed']   = $transactionInfo['processed'];
                $ccInfo['action']      = $transactionInfo['action'];
                $ccInfo['gateway']     = $transactionInfo['gateway'];
                $ccInfo['auth']        = $transactionInfo['auth'];
                $ccInfo['order_id']    = $transactionInfo['order_id'];
                $ccInfo['internal_id'] = $transactionInfo['internal_id'];
                //gateway
                $useGateway = $transactionInfo['gateway'];
                if (empty($use_gateway)) {
                    $useGateway = $integrationInfo['gateway'];
                }
                //check for same tip
                if (((float)$applyTip != (float)$transactionInfo['tip_amount']) || ($transactionInfo['auth'] == 1)) {
                    $tipHasChanged = true;
                    if ($location_info['integrateCC'] == "1") {
                        //compare record
                        $compareRecord = $transactionInfo['preauth_id'].$transactionInfo['record_no'];
                        //check same occurence
                        if (!in_array($compareRecord, $usedPreauthIds) || $compareRecord=="") {
                            $usedPreauthIds[] = $compareRecord;
                            if (strtolower($useGateway) == GATEWAY_TYPE_PAYPAL && $transactionInfo['transtype'] == TRANSACTION_TYPE_AUTH) {
                                //get rows
                                $getMainTips            = mlavu_query("SELECT `id`, `status` FROM `cc_tip_transactions` WHERE `dataname` = '[1]' AND `order_id` = '[2]' AND transaction_id='[3]' ", $processInfo['data_name'], $processInfo['order_id'], $transactionInfo['transaction_id']);
                                $tipRows                = mysqli_num_rows($getMainTips);
                                $vars                   = array();
                                $vars['dataname']       = $processInfo['data_name'];
                                $vars['order_id']       = $processInfo['order_id'];
                                $vars['transaction_id'] = $transactionInfo['transaction_id'];
                                $idatetime              = date("Y-m-d H:i:s");
                                $locationInfoArray      = $location_info;
                                if (isset($locationInfoArray['ltg_pizza_info']) ) {
                                    unset($locationInfoArray['ltg_pizza_info']);
                                }
                                $transactionInfoArray     = array(
                                                             "location_info"    => $locationInfoArray,
                                                             "integration_info" => $integrationInfo,
                                                             "process_info"     => $processInfo,
                                                             "transaction_info" => $transactionInfo,
                                                             "tip_amount"       => $applyTip,
                                                             "last_tip"         => $transactionInfo['tip_amount'],
                                                            );
                                $vars['transaction_info'] = json_encode($transactionInfoArray);
                                $vars['status']           = 3; // default in pending -> 3 status
                                $vars['created_date']     = $idatetime;
                                $vars['updated_date']     = $idatetime;
                                $inProcessQueue           = false;
                                //check for rows
                                if ($tipRows < 1) {
                                    mlavu_query("INSERT INTO `cc_tip_transactions` (`dataname`, `order_id`,`transaction_id`, `transaction_info`, `status`, `created_date`, `updated_date`) VALUES ('[dataname]', '[order_id]', '[transaction_id]','[transaction_info]', '[status]', '[created_date]', '[updated_date]')", $vars);
                                    $inProcessQueue = true;
                                } else if ($tipRows == 1) {
                                    $mainTipsData = mysqli_fetch_assoc($getMainTips);
                                    if ($mainTipsData['status'] == "pending") {
                                        mlavu_query("UPDATE `cc_tip_transactions` SET `transaction_info` = '[1]', `updated_date` = '[2]' where `transaction_id`='[3]' ", $vars['transaction_info'], $vars['updated_date'], $transactionInfo['transaction_id'] );
                                        $inProcessQueue = true;
                                    }
                                }
                                //process
                                if ($inProcessQueue) {
                                    updateCCRecord($transactionInfo['transaction_id'], $applyTip);
                                    $cardRes['displayType'] = NONEDIT_TYPE;
                                    $cardRes['status']      = 'success';
                                    $cardRes['msg']         = "Order #".$transactionInfo['order_id']." - ".$transactionInfo['card_type']." ...".$transactionInfo['card_desc']." - Tip: $applyTip - Success";
                                }
                            } else {
                                //other
                                $setCardtip = process_capture_or_tip($location_info, $integrationInfo, $processInfo, $transactionInfo, $applyTip, "", true);
                                $sucessAry  = array(
                                               "Success",
                                               "APPROVAL",
                                               "Approved",
                                               "Successful Transaction",
                                              );
                                global $gatewayMsg;
                                global $error_code;
                                global $error_info;
                                global $responseMsg;
                                $respMessage = '';
                                switch ($error_code) {
                                    case 1:
                                    $gatewayMsg .= "Error: Failed to load order ID ".$error_info;
                                    break;
                                    case 2:
                                    $gatewayMsg .= "Error: Failed to load transactions for order ID ".$error_info;
                                    break;
                                    case 3:
                                    $gatewayMsg .= "Error: Unrecognized payment gateway...";
                                    break;
                                    case 4:
                                    $gatewayMsg .= "Gateway error: ".$error_info;
                                    break;
                                    default:
                                    break;
                                }
                                if (in_array($responseMsg, $sucessAry)) {
                                    $respMessage = 'success';
                                } else {
                                    $respMessage = 'failure';
                                }
                                $isEditable             = checkEditable($ccInfo);
                                $cardRes['displayType'] = $isEditable;
                                $cardRes['status']      = $respMessage;
                                $cardRes['msg']         = $gatewayMsg;
                             }
                    } else {
                        $cardRes['status']      = 'failure';
                        $cardRes['displayType'] = EDIT_TYPE;
                        $cardRes['msg']         = 'Failed to Update Tip Amount : '.$applyTip.' For OrderID : '.$ordId;
                    }
                } else {
                    //update in cc_transactions
                    $updateCc = lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $applyTip, time(), $transactionInfo['id']);
                    if ($updateCc) {
                        $cardRes['status'] = 'success';
                        $cardRes['msg']    = "Order #".$transactionInfo['order_id']." - ".$transactionInfo['card_type']." ...".$transactionInfo['card_desc']." - Tip: $applyTip - Success";
                    } else {
                        $cardRes['status'] = 'failure';
                        $cardRes['msg']    = 'Failed to Update Tip Amount : '.$applyTip.' For OrderID : '.$ordId;
                    }
                    $cardRes['displayType'] = EDIT_TYPE;
                 }
             } else {
                $setCardtip = $transactionInfo['tip_amount'];
                if ($location_info['integrateCC'] == "1") {
                    $isEditable             = checkEditable($ccInfo);
                    $cardRes['displayType'] = $isEditable;
                    $cardRes['status']      = 'info';
                    $cardRes['msg']         = "Order #".$transactionInfo['order_id']." - ".$transactionInfo['card_type']." ...".$transactionInfo['card_desc']." - Tip: $applyTip - Tip unchanged";
                }
             }
            } else {
                $cardRes['status']      = 'failure';
                $cardRes['displayType'] = EDIT_TYPE;
                $cardRes['msg']         = 'Invalid request data provided';
            }
            //Tip update
            if ($tipHasChanged) {
                //get card tips
                $cardTip        = cardTipInfo($ordId);
                $tip            = $cardTip['cardTip'];
                $whereClauseOrd = " `order_id` = '[4]' AND `location_id` = '[5]' AND `ioid` = '[6]' ";
                //Update Card Tip in Orders
                lavu_query("UPDATE `orders` SET `card_gratuity` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'MANAGE TIPS', `last_mod_ts` = '[3]', `pushed_ts` = '[3]' WHERE ".$whereClauseOrd, str_replace("'", "''", number_format((float)$tip, $decimal_places, ".", "")), currentLocalTime(), time(), $ordId, $locId, $ioId);
            }
        } else {
            $cardRes['status']      = 'failure';
            $cardRes['displayType'] = EDIT_TYPE;
            $cardRes['msg']         = 'Invalid Cash Tip Amount : '.$cardTip.' For OrderID : '.$ordId;
        }
        array_push($cardTipResponse['cardTip'], $cardRes);
    }
    //Update In Tip Sharing
    updateTipSharing($serverId, $locId, $dataName);
    //return response
    return $cardTipResponse;
}

/**
* @purpose To Adjust Cash Tips
* @param string $cashTipInfo - Request parameter to save cash tips
* @return
*/
function adjustCashTip($cashTipInfo) {
    //Locationinfo
    global $location_info;
    //datanmame
    $dataName = $location_info['data_name'];
    //cash tip mode
    $cashTipMode = (int)$location_info['cash_tips_as_daily_total'];
    //decimalplaces
    $decimal_places = $location_info['disable_decimal'];
    //loc id
    $locId = reqvar('loc_id');
    //devicetime
    $deviceTime = reqvar('device_time');
    //server id
    $server_id = reqvar('server_id');
    if (empty($device_time)) {
        $deviceTime = date("Y-m-d H:i:s", time());
    }
    $tz = $location_info['timezone'];
    if (!empty($tz)) {
        $deviceTime = localize_datetime(date("Y-m-d H:i:s"), $tz);
    }
    //Build Response
    $cashTipResponse            = array();
    $cashTipResponse['cashTip'] = array();
    //Cash Tip List
    $cashTipList = explode("|", $cashTipInfo);
    //count cashTipList
    $countCashTip = count($cashTipList);
    //Start iteration
    for ($ctip = 0; $ctip < $countCashTip; $ctip++) {
        $cInfo = array();
        //Cash Tip Identifier
        $cashTipIdentifier = $cashTipList[$ctip];
        //Explode by _
        $cashTipIdParts = explode("_", $cashTipIdentifier);
        //order id
        $ordId             = $cashTipIdParts[0];
        $cInfo['order_id'] = $ordId;
        //Ioid
        $ioId          = $cashTipIdParts[1];
        $cInfo['ioid'] = $ioId;
        //Amount
        $cashTip = $cashTipIdParts[2];
        //check for tip validation
        if (is_numeric($cashTip)) {
            $whereClause     = " `order_id` = '[4]' AND `location_id` = '[5]'";
            $whereIdentifier = $ordId;
            if (!empty($ioId)) {
                $whereClause     = " `ioid` = '[4]'";
                $whereIdentifier = $ioId;
            }
            $cashTip = number_format($cashTip, $decimal_places, ".", "");
            //Update Query
            $updCashTip = lavu_query("UPDATE `orders` SET `cash_tip` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'MANAGE TIPS', `last_mod_ts` = '[3]', `pushed_ts` = '[3]' WHERE ".$whereClause, $cashTip, $deviceTime, time(), $whereIdentifier, $locId);
            if ($updCashTip) {
                $cInfo['status'] = 'success';
                $cInfo['msg']    = 'Successfully Cash Tip Updated with Amount : '.$cashTip.' For OrderID : '.$ordId;
            } else {
                $cInfo['status'] = 'failure';
                $cInfo['msg']    = 'Failed to Update Cash Tip Amount : '.$cashTip.' For OrderID : '.$ordId;
            }
        } else {
            $cInfo['status'] = 'failure';
            $cInfo['msg']    = 'Invalid Cash Tip Amount : '.$cashTip.' For OrderID : '.$ordId;
        }
        array_push($cashTipResponse['cashTip'], $cInfo);
    }
    //Update In Tip Sharing
    updateTipSharing($server_id, $locId, $dataName);
    //return response
    return $cashTipResponse;
}

/**
* @purpose Update in Tip Sharing
* @param string $serverId, $locId, $dataName
* @return
*/
function updateTipSharing($serverId, $locId, $dataName) {
    shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $serverId $locId $dataName > /dev/null 2>/dev/null &");
}

/**
* @purpose Display Tips Info
* @param
* @return array $loadOrder
*/
function displayTips() {
    //Locationinfo
    global $location_info;
    $locId      = reqvar('loc_id');
    $serverId   = reqvar('server_id');
    $deviceTime = reqvar('device_time');
    //dateinfo
    $dateInfo = getServerTipInfo();
    //Start Datetime
    $start_datetime = $dateInfo['startDateTime'];
    //End Datetime
    $end_datetime = $dateInfo['endDateTime'];
    //Server filter
    $server_filter = " AND `or1`.`server_id` = '$serverId'";
    //Where Caluse
    $where_and_order_by =  " WHERE `or1`.`opened` >= '".$start_datetime."' AND `or1`.`opened` <= '".$end_datetime."' AND `or1`.`void` = '0' AND `or1`.`location_id` = '[1]'".$server_filter." AND `or1`.`order_id` NOT LIKE '777%' GROUP BY `or1`.`order_id` ORDER BY `or1`.`opened` ";
    //Query
    $showTipQuery = lavu_query("SELECT `or1`.`order_id`, `or1`.`cash_tip`, `or1`.`card_gratuity`, `or1`.`cash_tip`, `or1`.`total`, `or1`.`alt_paid`, `or1`.`alt_refunded`, `or1`.`tablename`, `or1`.`closed`, `or1`.`opened`, `or1`.`card_paid`, `or1`.`cash_paid`, `or1`.`refunded_cc` FROM `orders` as `or1` JOIN `cc_transactions` as `cc` on `or1`.`order_id` = `cc`.`order_id`".$where_and_order_by, $locId);
    //Total Net Sale
    $netSale        = 0;
    $totalCashTip   = 0;
    $totalCardTip   = 0;
    $totalTips      = 0;
    $displayCashTip = "";
    $calTip         = 0;
    $orderAry       = array();
    $decimal_places = $location_info['disable_decimal'];
    //Cash tip mode
    $cash_tip_mode = (int)$location_info['cash_tips_as_daily_total'];
    //action ary
    $actionAry = array(
                  'Sale',
                  'Refund',
                 );
    $loadOrder = new stdClass();
    //Check for records
    if (mysqli_num_rows($showTipQuery) > 0) {
        $smallest_money = (1 / pow(10, $decimal_places));
        while ($rowtipinfo = mysqli_fetch_assoc($showTipQuery)) {
            $tipRow = array();
            if (!isset($loadOrder->{$rowtipinfo['order_id']})) {
                $loadOrder->{$rowtipinfo['order_id']} = array();
            }
            //data from cc_transactions
            $altCnt     = 0;
            $cnt        = 0;
            $refund     = 0;
            $void       = 0;
            $queCCInfo  = lavu_query("SELECT * FROM `cc_transactions` WHERE `order_id` = '[1]' AND `voided` = '[2]'", $rowtipinfo['order_id'], $void);
            while ($rowCcInfo = mysqli_fetch_assoc($queCCInfo)) {
                $isCardPush = 0;
                $rowCcInfo['tablename'] = $rowtipinfo['tablename'];
                $rowCcInfo['closed']    = $rowtipinfo['closed'];
                if ($rowCcInfo['pay_type'] == 'Cash') {
                    $cnt++;
                    $rowCcInfo['cash_tip']  = ($rowtipinfo['cash_tip'] != '') ? $rowtipinfo['cash_tip'] : '';
                    $rowCcInfo['card_paid'] = '0.00';
                    if ($cnt == 1) {
                        $cashInfo                 = refundAlternateOrderInfo($rowCcInfo['order_id'], $rowCcInfo['check'], $rowCcInfo['internal_id'], $rowCcInfo['pay_type']);
                        $rowCcInfo['cash_paid']   = (string)$cashInfo['totalAmt'];
                        $rowCcInfo['amount']      = ($rowCcInfo['cash_paid'] != '') ? $rowCcInfo['cash_paid'] : '0.00';
                        $rowCcInfo['displayType'] = EDIT_TYPE;
                        if ($rowCcInfo['cash_paid'] != 0) {
                            if (!in_array($rowCcInfo['order_id'], $orderAry)) {
                                $orderAry[] = $rowCcInfo['order_id'];
                                $calTip    += $rowCcInfo['cash_tip'];
                            }
                            $netSale += $rowCcInfo['amount'];
                            $loadOrder->{$rowtipinfo['order_id']}[] = $rowCcInfo;
                        }
                    }
                } else if ($rowCcInfo['pay_type'] != 'Cash') {
                      //Gateway
                    $gateway = $rowCcInfo['gateway'];
                    //Auth
                    $auth = $rowCcInfo['auth'];
                    //Processed
                    $processed = $rowCcInfo['processed'];
                    //Action
                    $action = $rowCcInfo['action'];
                    if (in_array($rowCcInfo['action'], $actionAry) && $rowCcInfo['tip_for_id'] == '0' && $rowCcInfo['voided'] != '1') {
                    if ($rowCcInfo['pay_type'] == PAYMENT_TYPE_CARD || strtolower($rowCcInfo['pay_type']) == GATEWAY_TYPE_PAYPAL) {
                        $rowCcInfo['cash_tip']   = ($rowtipinfo['cash_tip'] != '') ? $rowtipinfo['cash_tip'] : '';
                        $rowCcInfo['cash_paid']  = '0.00';
                        $refundInfo              = refundOrderInfo($rowCcInfo['order_id'], $rowCcInfo['check'], $rowCcInfo['internal_id'], $rowCcInfo['pay_type']);
                        $rowCcInfo['card_paid']  = (string)$rowCcInfo['amount'];
                        $rowCcInfo['tip_amount'] = ($rowCcInfo['tip_amount'] != '') ? (string)$rowCcInfo['tip_amount'] : '';
                        $rowCcInfo['amount']     = (string)$rowCcInfo['amount'];
                        if (count($refundInfo) > 0) {
                            $rowCcInfo['amount']     = (string)($rowCcInfo['amount'] - $refundInfo['amount']);
                            $rowCcInfo['card_paid']  = (string)$rowCcInfo['amount'];
                            $refundAmount            = $rowCcInfo['tip_amount'] - $refundInfo['tip_amount'];
                            $rowCcInfo['tip_amount'] = ($refundAmount != '') ? (string)$refundAmount : '';
                        }
                    } /*else {
                        $altCnt++;
                        if ($altCnt == 1) {
                        $rowCcInfo['cash_tip']   = ($rowtipinfo['cash_tip'] != '') ? number_format($rowtipinfo['cash_tip'], $decimal_places) : '';
                        $rowCcInfo['cash_paid']  = (string)$rowtipinfo['cash_paid'];
                        $rowCcInfo['card_paid']  = (string)$rowtipinfo['card_paid'];
                        $rowCcInfo['tip_amount'] = ($rowCcInfo['tip_amount'] != '') ? number_format($rowCcInfo['tip_amount'], $decimal_places) : '';
                        $altPaidInfo             = refundAlternateOrderInfo($rowCcInfo['order_id'], $rowCcInfo['check'], $rowCcInfo['internal_id'], $rowCcInfo['pay_type']);
                        if (count($altPaidInfo) > 0) {
                            $rowCcInfo['amount'] = (string)$altPaidInfo['totalAmt'];
                        }
                        }
                    }*/
                    if (((($processed=="1" || $auth=="2" || in_array($gateway, array("iZettle", "MIT", "Moneris"))) && $action=="Sale") || $action=="Refund")) {
                    $rowCcInfo['displayType'] = NONEDIT_TYPE;
                    } else if ($processed=="0" && $action=="Sale") {
                        $isrefund = isRefunded($rowCcInfo['internal_id']);
                        if ($isrefund) {
                             $rowCcInfo['displayType'] = NONEDIT_TYPE;
                        } else {
                            $rowCcInfo['displayType'] = EDIT_TYPE;
                        }
                    } else {
                        $rowCcInfo['displayType'] = NONEDIT_TYPE;
                    }
                    // if ($rowCcInfo['pay_type'] == 'Card' && $rowtipinfo['card_paid'] == 0) {
                    //     unset($loadOrder->{$rowtipinfo['order_id']});
                    // }
                    if ($rowCcInfo['action'] != 'Refund' && isset($loadOrder->{$rowtipinfo['order_id']}) && $rowCcInfo['amount'] != 0) {
                        if ($rowCcInfo['pay_type'] == PAYMENT_TYPE_CARD || strtolower($rowCcInfo['pay_type']) == GATEWAY_TYPE_PAYPAL) {
                            $isCardPush++;
                        } /*else if($altCnt == 1) {
                            $isCardPush++;
                        }*/
                    }
                    //Prepare final array
                    if ($isCardPush == 1) {
                        if (!in_array($rowCcInfo['order_id'], $orderAry)) {
                            $orderAry[] = $rowCcInfo['order_id'];
                           $calTip     += $rowtipinfo['cash_tip'];
                        }
                        $calTip  += $rowCcInfo['tip_amount'];
                        $netSale += $rowCcInfo['amount'];
                        $loadOrder->{$rowtipinfo['order_id']}[] = $rowCcInfo;
                    }
                    }
                }
            }
        }
        //Net Sale
        $loadOrder->{'netSales'} = $netSale;
        //Tip Percentage
        $totalTips                    = (($calTip) * 100) / ($netSale);
        $loadOrder->{'tipPercentage'} = round($totalTips, $decimal_places);
        //Total Tips = Cash Tips + Card Tips
        $loadOrder->{'totalTips'} = $calTip;
        $loadOrder->{'status'}    = 'success';
        $loadOrder->{'msg'}       = 'Tip info available';
    } else {
        $loadOrder->{'status'} = 'failure';
        $loadOrder->{'msg'}    = 'Tips info not available..!!';
    }
    //return result
    return $loadOrder;
}

/**
* @purpose Display Price
* @param string $p - Price, $isSymbolDisable - want to display price
* @return mixed
*/
function displayPrice($p, $isSymbolDisable = false) {
        global $location_info;
        $decimal_places = $location_info['disable_decimal'];
        $monitarySymbol = !empty($location_info['monitary_symbol']) ? $location_info['monitary_symbol'] : "$";
        $decimal_char   = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
        $thousands_char = ($decimal_char == ".")?",":".";
        $left_or_right = isset($location_info['left_or_right']) ? $location_info['left_or_right'] : 'left';
        if ($left_or_right == 'left') {
			$left_side = $monitarySymbol;
		} else {
			$right_side = $monitarySymbol;
		}
        if ($isSymbolDisable) {
            $monitarySymbol = '';
            $left_side = '';
            $right_side = '';
        }
        return $left_side.number_format((float)$p, $decimal_places, $decimal_char, $thousands_char).$right_side;
}

/**
* @purpose Current Local Time
* @param
* @return string $current_time
*/
function currentLocalTime() {
        global $location_info;
        $current_time = date("Y-m-d H:i:s", time());
        $tz           = $location_info['timezone'];
        if (!empty($tz)) {
            $current_time = localize_datetime(date("Y-m-d H:i:s"), $tz);
        }
        return $current_time;
}

/**
 * @purpose Check Refund
 * @param string $ordId, string $checkId, string $internalId
 * @return mixed $refundResult
 */
function refundOrderInfo($ordId, $checkId, $internalId, $payType) {
    //refund data
    $action       = 'Refund';
    $refundResult = array();
    $refundQuery  = lavu_query("SELECT sum(amount) as amount, sum(tip_amount) as tip_amount FROM `cc_transactions` WHERE `order_id` = '[1]' AND `check` = '[2]' AND `refund_pnref` = '[3]' AND `pay_type` = '[4]' AND `action` = '[5]'", $ordId, $checkId, $internalId, $payType, $action);
    //Check for rows
    if (mysqli_num_rows($refundQuery) > 0) {
        $rowRefund    = mysqli_fetch_assoc($refundQuery);
        $refundResult = $rowRefund;
    }
    return $refundResult;
}

/**
 * @purpose Check Alternate Refund
 * @param string $ordId, string $checkId, string $internalId, string $payType
 * @return mixed $refundAltResult
 */
function refundAlternateOrderInfo($ordId, $checkId, $internalId, $payType) {
    $paymentAry = array(
                   'Cash',
                   'Card',
                   'PayPal',
                   'Paypal',
                  );
    $whereStr   = '';
    $paymentStr = '';
    if (!in_array($payType, $paymentAry)) {
        $paymentStr = join("', '", $paymentAry);
        $whereStr = " cc.pay_type NOT IN('$paymentStr')";
    } else {
        $whereStr = " cc.pay_type IN('Cash')";
    }
    $refundAltResult = array();
    $altQuery        = lavu_query("SELECT (SELECT c1.amount FROM `cc_transactions` as c1 WHERE c1.id = cc.id AND c1.action = 'Sale') as alttotalsell ,(SELECT c2.amount FROM `cc_transactions` as c2 WHERE c2.id = cc.id AND c2.action = 'Refund') as alttotalrefunded FROM `cc_transactions` as cc WHERE ".$whereStr." AND cc.order_id = '[1]'", $ordId);
    $totalSale       = '';
    $totalRefund     = '';
    //Check for rows
    if (mysqli_num_rows($altQuery) > 0) {
        while ($rowAlt = mysqli_fetch_assoc($altQuery)) {
            if ($rowAlt['alttotalsell'] != '') {
                $totalSale += $rowAlt['alttotalsell'];
            }
            if ($rowAlt['alttotalrefunded'] != '') {
                $totalRefund += $rowAlt['alttotalrefunded'];
            }
        }
        $refundAltResult['totalAmt'] = $totalSale - $totalRefund;
    }
    return $refundAltResult;
}

/**
 * @purpose Check Tip Amount
 * @param string $ordId
 * @return mixed $cardTipResult
 */
 function cardTipInfo($ordId) {
    $cardTipResult = array();
     //payment typess to be considered
     $payTypeAry = array(
                    'Card',
                    'PayPal',
                    'Paypal',
                   );
      //WhereStr
      $paymentStr = join("', '", $payTypeAry);
      $whereStr   = " cc.pay_type IN('$paymentStr')";
      //query
      $cardTipQuery   = lavu_query("SELECT (SELECT c1.tip_amount FROM `cc_transactions` as c1 WHERE c1.id = cc.id AND c1.action = 'Sale') as totalselltip ,(SELECT c2.tip_amount FROM `cc_transactions` as c2 WHERE c2.id = cc.id AND c2.action = 'Refund') as totalrefundedtip FROM `cc_transactions` as cc WHERE ".$whereStr." AND cc.order_id = '[1]'", $ordId);
      $totalSaleTip   = '';
      $totalRefundTip = '';
      //check for rows
      if (mysqli_num_rows($cardTipQuery) > 0) {
          while ($rowctip = mysqli_fetch_assoc($cardTipQuery)) {
              if ($rowctip['totalselltip'] != '') {
                $totalSaleTip += $rowctip['totalselltip'];
              }
              if ($rowctip['totalrefundedtip'] != '') {
                $totalRefundTip += $rowctip['totalrefundedtip'];
              }
          }
          $cardTipResult['cardTip'] = $totalSaleTip - $totalRefundTip;
      }
      return $cardTipResult;
 }

/**
 * @purpose tip editable fieldtype
 * @param string $ccInfo
 * @return mixed $isEditable
 */
function checkEditable($ccInfo) {
    $isEditable = NONEDIT_TYPE;
    if (((($ccInfo['processed'] == '1' || $ccInfo['auth'] == '2' || in_array($ccInfo['gateway'], array('iZettle', 'MIT', 'Moneris', 'eConduit'))) && $ccInfo['action'] == 'Sale') || $ccInfo['action'] == 'Refund')) {
        $isEditable = NONEDIT_TYPE;
    } else if ($ccInfo['processed'] == '0' && $ccInfo['action'] == 'Sale') {
        $isrefund = isRefunded($ccInfo['internal_id']);
        if ($isrefund) {
            $isEditable = NONEDIT_TYPE;
        } else {
            $isEditable = EDIT_TYPE;
        }
    }
    return $isEditable;
}

/**
 * @purpose Check Is Refunded
 * @param string $internalId unique id of cc_transaction record
 * @return boolean $refundResult true/false
 */
function isRefunded($internalId) {
    //Refund query
    $isRefunded  = false;
    $refundQuery = lavu_query("SELECT order_id FROM `cc_transactions` WHERE action = 'Refund' AND refund_pnref = '[1]'", $internalId);
    if (mysqli_num_rows($refundQuery) > 0) {
        $isRefunded = true;
    }
    return $isRefunded;
}
?>
