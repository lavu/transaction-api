<?php
/**
 * This function is used to get Active menu details. For POS 4.0
 * return array status with all details
 */
function menuItemsDetails() {
	global $data_name;
	global $location_info;
	global $active_modules;
	$unavailableUntilReduceInterval = "`unavailable_until` - IF(`unavailable_until` > 0, 300, 0) AS `unavailable_until`";
	$result = array();
	$menu_groups_array = array();
	$menu_categories_array = array();
	$menu_items_array = array();
	$used_modifier_lists = "";
	$active_field = ($for_lavu_togo == "1")?"ltg_display":"active";
	$limit = "";
	$universal_order = "ASC";
	$lavu_lite = false;
	$menu_id = $location_info['menu_id'];
	global $o_happyHoursGetCategoryOrItem;
	require_once(resource_path()."/../areas/happyhours/php/happyhours_getcategoryoritem.php");
	if ( ($get_menu_groups = lavu_query("SELECT `id`, `group_name`, `orderby` FROM `menu_groups` WHERE `menu_id` = '".$menu_id."' AND `_deleted` != '1' ORDER BY (`group_name` = 'Universal') ".$universal_order.", `_order` ASC".$limit)) !== false ) {
		if (mysqli_num_rows($get_menu_groups) > 0) {
			while ($data_obj_MG = mysqli_fetch_object($get_menu_groups)) {
				$sort_type = $data_obj_MG->orderby;
				if ($data_obj_MG->group_name == "Universal") {
					$menu_groups_array[] = $data_obj_MG;
				}
				$order_by = ($sort_type == "manual")?" ORDER BY `_order` ASC":" ORDER BY `name` ASC";
				if ( ($get_menu_categories = lavu_query("SELECT `id`, `group_id`, `name`, `image`, `storage_key` , `description`, `active`, `_order`, `print`, `last_modified_date`, `printer`, `modifier_list_id`, `apply_taxrate`, `custom_taxrate`, `forced_modifier_group_id`, `print_order`, `super_group_id`, `tax_inclusion`, `happyhour`, `tax_profile_id`, `price_tier_profile_id`, `no_discount`, `enable_reorder`, `allow_ordertype_tax_exempt` FROM `menu_categories` WHERE `menu_id` = '".$menu_id."' AND `group_id` = '".$data_obj_MG->id."' AND `$active_field` = '1' AND `_deleted` != '1'".$order_by)) !== false ) {
					if ( mysqli_num_rows($get_menu_categories) > 0 ) {
						if ($data_obj_MG->group_name != "Universal") {
							$menu_groups_array[] = $data_obj_MG;
						}
						$got_a_category = true;
						while ($data_obj = mysqli_fetch_object($get_menu_categories)) {
							if ($data_obj->printer == "") {
								$data_obj->print = "0";
							}
							$image_path = "/mnt/poslavu-files/images/".$data_name."/categories/".$data_obj->image;
							if (!file_exists($image_path) || is_dir($image_path)) {
								$data_obj->image = "";
							}
							if ((!strstr($used_modifier_lists, "|".$data_obj->modifier_list_id."|")) && ($data_obj->modifier_list_id != 0)) {
								$used_modifier_lists .= "|".$data_obj->modifier_list_id."|";
							}
							// get the menu items and happy hours
							$check_for_combo = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'disable_combo_bulider' AND `location` = '1'");
							$combo_info = mysqli_fetch_assoc($check_for_combo);
							$column = "`id`, `category_id`, `name`, `price`, `description`, `image`,  `storage_key`, `active`, `print`, `quick_item`, `last_modified_date`, `printer`, `apply_taxrate`, `custom_taxrate`, `modifier_list_id`, `forced_modifier_group_id`, `open_item`, `hidden_value`, `hidden_value2`, `allow_deposit`, `UPC`, `hidden_value3`, `inv_count`, `super_group_id`, `tax_inclusion`, `happyhour`, `tax_profile_id`, `track_86_count`, `price_tier_profile_id`, `no_discount`, `product_code`, `tax_code` ,`combo`, `comboitems`, `currency_type`, `allow_ordertype_tax_exempt`, `kitchen_nickname`, ".$unavailableUntilReduceInterval." ";
							if ($combo_info['value']==1) {
							$menu_query = "SELECT". $column." FROM `menu_items` WHERE `menu_id` = '".$menu_id."' AND `category_id` = '".$data_obj->id."' AND `$active_field` = '1' AND `_deleted`!='1'";
							} else {
							$menu_query = "SELECT ".$column." FROM `menu_items` WHERE `menu_id` = '".$menu_id."' AND `category_id` = '".$data_obj->id."' AND `$active_field` = '1' AND `_deleted`!='1' AND `combo`!='1' ";
							}
							$a_menu_items = array();
							if (($get_menu_items = lavu_query($menu_query.$order_by)) !== false ) {
								if ( mysqli_num_rows($get_menu_items) > 0) {
									while ($row = mysqli_fetch_assoc($get_menu_items)) {
										$a_menu_items[] = $row;
									}
								}
								mysqli_free_result( $get_menu_items );
							}

							$item_hhs = $o_happyHoursGetCategoryOrItem->get_all_items_from_category_happyhours($data_obj->id, (array)$data_obj, $a_menu_items, true);
							if (count($a_menu_items) > 0) {
								$menu_categories_array[] = $data_obj;
								for ($i = 0; $i < count($a_menu_items); $i++) {
									$data_obj2 = (object)$a_menu_items[$i];

									if (isset($data_obj2->name)) {
										$data_obj2->name = str_pad($data_obj2->name, 2, " ", STR_PAD_RIGHT);
									}

									if (($data_obj2->printer == "") || ($data_obj->printer == "" && $data_obj2->printer == "0")) {
										$data_obj2->print = "0";
									}
									$image_path = "/mnt/poslavu-files/images/".$data_name."/items/".$data_obj2->image;
									if (!file_exists($image_path) || is_dir($image_path)) {
										$data_obj2->image = "";
									}
									if (isset($item_hhs[$data_obj2->id])) {
										$data_obj2->happyhour = $item_hhs[$data_obj2->id];
									} else {
										$data_obj2->happyhour = "";
									}
									$menu_items_array[] = $data_obj2;
									if ((!strstr($used_modifier_lists, "|".$data_obj2->modifier_list_id."|")) && ($data_obj2->modifier_list_id != 0)) {
										$used_modifier_lists .= "|".$data_obj2->modifier_list_id."|";
									}
								}
							}
						}
						mysqli_free_result( $get_menu_categories );
					}
				}
			}

		} else {
			lavu_echo('{"json_status":"NoGroups"}');
		}
		mysqli_free_result( $get_menu_groups );
	} else {
		lavu_echo('{"json_status":"NoGroups"}');
	}
	if (!$got_a_category) {
		lavu_echo('{"json_status":"NoC"}');
	}
	foreach ($menu_items_array as $menu_combo) {
		if ($menu_combo->combo == '1') {
			$menu_combo->forced_modifier_group_id = '';
			$menu_combo->modifier_list_id = '0';
			$menu_combo->comboitems = getmenuccomboddetails($menu_combo->id); }
	}

	$result['menu_items'] = $menu_items_array;
	$forced_modifier_list_array = array();
	$forced_modifier_list_array[] = array(
		"id"		=> "99999999",
		"title"		=> "place_holder",
		"type"		=> "choice",
		"modifiers"	=>	array(
			array(
				"id"		=> "0",
				"title"		=> "MOD ERROR",
				"cost"		=> "0.00",
				"detour"	=> "0"
			)
		)
	);

	if ( ($get_forced_modifier_lists = lavu_query("SELECT `id`, `title`, `type`, `type_option` FROM `forced_modifier_lists` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id)) !== false ) {
		if (mysqli_num_rows($get_forced_modifier_lists) > 0) {
			$happyHourForcedModifierListsIds = array();
			$f_m_l = lavu_query("SELECT `id`, `happyhours_id` FROM `forced_modifier_lists` WHERE `_deleted` != 1");
			while ($f_m_l_h = mysqli_fetch_assoc($f_m_l)) {
				$happyHourForcedModifierListsIds[$f_m_l_h['id']] = $f_m_l_h['happyhours_id'];
			}
			$lists_array = array();
			$get_forced_modifiers = lavu_query("SELECT `id`, `list_id`, `title`, `cost`, `detour`, `extra`, `extra2`, `extra3`, `extra4`, `extra5`, `product_code`, `tax_code`,`happyhours_id`, `ordertype_id`, ".$unavailableUntilReduceInterval." FROM `forced_modifiers` WHERE `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC");
			while ($extract2 = mysqli_fetch_assoc($get_forced_modifiers)) {
				if (is_null($extract2['ordertype_id'])) {
					$extract2['ordertype_id'] = "";
				}
				$la_key = "l".$extract2['list_id'];
				if (!isset($lists_array[$la_key])) {
					$lists_array[$la_key] = array();
				}
				if ($extract2['happyhours_id'] != 0) {
					$f_happyhours = $o_happyHoursGetCategoryOrItem->get_all_details_id_happyhours($extract2['happyhours_id'], 'forced');
				} else {
					$extract2_list_id = $extract2['list_id'];
					if (isset($happyHourForcedModifierListsIds[$extract2_list_id]) && $happyHourForcedModifierListsIds[$extract2_list_id] != '0') {
					$f_happyhours = $o_happyHoursGetCategoryOrItem->get_all_details_id_happyhours($happyHourForcedModifierListsIds[$extract2_list_id], 'forced');
					} else {
						$f_happyhours = '';
					}
				}

				$extract2['happyhour'] = $f_happyhours;
				$lists_array[$la_key][] = $extract2;
			}
			while ($extract = mysqli_fetch_assoc($get_forced_modifier_lists)) {
				$la_key = "l".$extract['id'];
				if (!isset($lists_array[$la_key])) {
					$lists_array[$la_key] = array();
				}
				$extract['modifiers'] = $lists_array[$la_key];
				$forced_modifier_list_array[] = $extract;
			}
		}
		mysqli_free_result( $get_forced_modifier_lists );
	}
	$result['forced_modifier_lists'] = $forced_modifier_list_array;
	$coded_modifier_list_array = array();
	$happyHourModifierCategories = array();
	if (!$lavu_lite && $used_modifier_lists!="") {
		$used_modifier_lists_array = explode("|", trim($used_modifier_lists, "|"));
		$o_m_c = lavu_query("SELECT `id`, `happyhours_id` FROM `modifier_categories` WHERE `_deleted` != 1") ;
		while ($o_m_l_h = mysqli_fetch_assoc($o_m_c)) {
			$happyHourModifierCategories[$o_m_l_h['id']] = $o_m_l_h['happyhours_id'];
		}
		foreach ($used_modifier_lists_array as $ml) {
			if (($get_modifiers = lavu_query("SELECT `id`, `title`, `cost`, `product_code`, `tax_code`,`happyhours_id`, ".$unavailableUntilReduceInterval." FROM `modifiers` WHERE `category` = '".$ml."' AND `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC")) !== false) {
				if (mysqli_num_rows($get_modifiers) > 0) {
					$modifiers_list = array();
					$modifiers_string = "";
					$count = 0;
					while ($extract = mysqli_fetch_assoc($get_modifiers)) {
						$modifiers_string .= $extract['id']."|o|".$extract['title']."|o|".$extract['cost'];
						if ($extract['happyhours_id'] != 0) {
							$f_happyhours = $o_happyHoursGetCategoryOrItem->get_all_details_id_happyhours($extract['happyhours_id'], 'optional');
						} else {
							if (isset($happyHourModifierCategories[$ml]) && $happyHourModifierCategories[$ml] != '0') {
								$f_happyhours = $o_happyHoursGetCategoryOrItem->get_all_details_id_happyhours($happyHourModifierCategories[$ml], 'optional');
							} else {
								$f_happyhours = '';
							}
						}

						$extract['happyhour'] = $f_happyhours;
						$modifiers_list[] = $extract;
						$count++;
						if ($count < mysqli_num_rows($get_modifiers)) {
							$modifiers_string .= "|*|";
						}
					}

					$coded_modifier_list_array[] = array(
						"id"			=> $ml,
						"modifiers"		=> $modifiers_string,
						"modifiers_o"	=> $modifiers_list
					);
				}
				mysqli_free_result($get_modifiers);
			}
		}
	}

	$result['modifier_lists'] = $coded_modifier_list_array;
	$fmg_array[] = array(
		"id"		=> "99999999",
		"title"		=> "place_holder",
		"include_lists"		=> "",
		"_deleted"		=> ""
	);

	if (($get_forced_modifier_groups = lavu_query("SELECT `id`, `title`, `include_lists` FROM `forced_modifier_groups` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id)) !== false) {
		if (mysqli_num_rows($get_forced_modifier_groups) > 0) {
			while ($data_obj = mysqli_fetch_object($get_forced_modifier_groups)) {
				$fmg_array[] = $data_obj;
			}
		}
		mysqli_free_result( $get_forced_modifier_groups );
	}
	$result['forced_modifier_groups'] = $fmg_array;
	return $result;
}

function doProcess($funcName) {
		$result = array('fail' => 'Something went wrong, Please Try again after some time.');
		if (function_exists($funcName)) {
			$result= $funcName();
		}
		return $result;
}
?>