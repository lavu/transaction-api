<?php
/**
 * This function is used to Undo Loyalty Discount when orders transactions is voided. For POS 4.0
 * @param  request array (order_id,dataname,datetime .. etc)
 * return array status with all details
 */
function undoLoyaltyDiscount($vars) {
	$vars['keyname'] = "remember_reqvar_loyalty_for_order_".$vars['order_id'];
	$vars['status'] = "success";
	$key_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_status` WHERE `keyname` = '[keyname]' AND `dataname` = '[dataname]' AND info !=''", $vars);
	if (mysqli_num_rows($key_query) > 0) {
		$key_read = mysqli_fetch_assoc($key_query);
		$vars['status_id'] = $key_read['id'];
		$str_parts = explode("&", $key_read['info']);
		$count_no = count($str_parts);
		for ($lenth = 0; $lenth < $count_no; $lenth++) {
			$inner_parts = explode("=", $str_parts[$lenth]);
			if (count($inner_parts) == 2) {
				$inner_key = urldecode(trim($inner_parts[0]));
				$inner_val = urldecode(trim($inner_parts[1]));
				$vars[$inner_key] = $inner_val;
			}
		}
		$vars['point_amount'] = "+".$vars['reward_cost'];
		$vars['action'] = "Loyalty";
		$account_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE deleted = 0 AND inactive = 0 AND `id` = '[card_dbid]' AND `dataname` = '[dataname]'", $vars);
		if (mysqli_num_rows($account_query)) {
			$account_read = mysqli_fetch_assoc($account_query);
			$vars['chainid'] = $account_read['chainid'];
			$vars['previous_points'] = $account_read['points'];
			$vars['balance'] = $account_read['balance'];
			$vars['new_points'] = $account_read['points'] + $vars['reward_cost'];
		}
		$success = mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET `points` = '[new_points]' WHERE `id`='[card_dbid]'", $vars);
		if ($success) {
			 mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`chainid`, `dataname`, `name`, `card_id`, `action`, `server_id`, `server_name`, `previous_points`, `previous_balance`, `point_amount`, `balance_amount`, `new_points`, `new_balance`, `item_id_list`, `datetime`) VALUES ('[chainid]', '[dataname]', '[name]', '[card_dbid]', '[action]', '[server_id]', '[server_name]', '[previous_points]', '[balance]', '[point_amount]', '0', '[new_points]', '[balance]', '', '[datetime]')", $vars);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_status` SET `state` = '', `info` = '' WHERE `id` = '[status_id]'", $vars);
		}
	}
	return $vars;
}
function doProcess($funcName) {
		global $data_name;
		$data['server_id'] = reqvar("server_id", "");
		$data['order_id'] = reqvar("order_id", "");
		$data['datetime'] = reqvar("device_time", "");
		$data['dataname']=$data_name;
		return $funcName($data);
}
?>