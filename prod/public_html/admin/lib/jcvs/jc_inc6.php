<?php

	//ini_set('display_errors',1);
	//error_reporting(E_ALL|E_STRICT);
	
	class fmg_placeholder {
	
		var $id;
		var $title;
		var $include_lists;
		var $_deleted;

		function __construct() {
		
			$this->id = "9999999";
			$this->title = "PLACEHOLDER_GROUP_EMPTY";
			$this->include_lists = "";
			$this->_deleted = 0;
		}
	}

	$device_time = (isset($_REQUEST['device_time']))?$_REQUEST['device_time']:date("Y-m-d H:i:s", time());

	function getModifierUnitCost($mod_id, $mod_type) {
	
		$unit_cost = 0;
		if ($mod_type == "forced") {
			$tablename = "forced_modifiers";
		} else {
			$tablename = "modifiers";
		}
		$get_unit_cost = lavu_query("SELECT `cost` FROM `[1]` WHERE `id` = '[2]'", $tablename, $mod_id);
		if (@mysqli_num_rows($get_unit_cost) > 0) {
			$mod_info = mysqli_fetch_assoc($get_unit_cost);
			$unit_cost = $mod_info['cost'];
		}
		
		return $unit_cost;
	}
	
	function setCorrectSum($value, $correction, $smallest_unit) {

			if (($correction >= ($value - $smallest_unit)) && ($correction <= ($save_subtotal + $smallest_unit))) {
				return $correction;
			} else {
				return $value;
			}
	}
	
	function saveAlternatePaymentTotals($loc_id, $order_id, $check, $payment_totals) {
	
		$clear_previous = lavu_query("DELETE FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $check);
	
		$payment_totals_array = explode("|*|", $payment_totals);
		foreach ($payment_totals_array as $total_info) {
			$total_info_array = explode("|o|", $total_info);
			$q_fields = "";
			$q_values = "";
			$p_vars = array();
			$p_vars['loc_id'] = $loc_id;
			$p_vars['order_id'] = $order_id;
			$p_vars['check'] = $check;
			$p_vars['pay_type_id'] = $total_info_array[4];
			$p_vars['type'] = $total_info_array[0];
			$p_vars['amount'] = $total_info_array[1];
			$p_vars['tip'] = $total_info_array[2];
			$p_vars['refund_amount'] = $total_info_array[3];
			$keys = array_keys($p_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$record_it = lavu_query("INSERT INTO `alternate_payment_totals` ($q_fields) VALUES ($q_values)", $p_vars);
		}
	}
	
	function loadMenuData($menu_id, $loc_id) { // load all menu data and more; includes groups, categories, items, forced/optional modifiers, printer groups, quick pay buttons, and tax rates
	
		global $location_info;
	
		$menu_categories_array = array();
		$menu_items_array = array();
		$menu_groups_array = array();
		$menu_items = "";
		
		$printer_groups_return_string = '{"ph1":"ignore",';
		$get_printer_groups = lavu_query("SELECT `id`, `value`, `value2`, `value3`, `value4` FROM `config` WHERE `type` = 'printer_group' AND `location` = '".$loc_id."'");
		if (@mysqli_num_rows($get_printer_groups) > 0) {
			while ($extract = mysqli_fetch_assoc($get_printer_groups)) {
				$printer_groups_return_string .= '"pg_'.$extract['id'].'":"|'.$extract['value'].'|'.$extract['value2'].'|'.$extract['value3'].'|'.$extract['value4'].'|",';
			}
		}
		$printer_groups_return_string .= '"ph2":"ignore"}';

		//$used_forced_modifier_groups = "";
		$used_modifier_lists = "";
		
		$got_a_cateogory = FALSE;
		
		$get_menu_groups = lavu_query("SELECT * FROM `menu_groups` WHERE `menu_id` = '".$menu_id."' ORDER BY (`group_name` = 'Universal') ASC, `_order` ASC");
		if (@mysqli_num_rows($get_menu_groups) > 0) {
			while ($data_obj_MG = mysqli_fetch_object($get_menu_groups)) {
				$menu_groups_array[] = $data_obj_MG;
				$sort_type = $data_obj_MG->orderby;
				if ($sort_type == "manual") {
					$order_by = " ORDER BY `_order` ASC";
				} else {
					$order_by = " ORDER BY `name` ASC";
				}
				$get_menu_categories = lavu_query("SELECT * FROM `menu_categories` WHERE `menu_id` = '".$menu_id."' AND `group_id` = '".$data_obj_MG->id."' AND `active` = '1' AND `_deleted` != '1'".$order_by);
				if (@mysqli_num_rows($get_menu_categories) > 0) {
					$got_a_category = TRUE;
					while ($data_obj = mysqli_fetch_object($get_menu_categories)) {
						$menu_categories_array[] = $data_obj;
						if ((!strstr($used_modifier_lists, "|".$data_obj->modifier_list_id."|")) && ($data_obj->modifier_list_id != 0)) {
							$used_modifier_lists .= "|".$data_obj->modifier_list_id."|";
						}						
						$get_menu_items = lavu_query("SELECT * FROM `menu_items` WHERE `menu_id` = '".$menu_id."' AND `category_id` = '".$data_obj->id."' AND `show_in_app` = '1' AND `active` = '1' AND `_deleted`!='1'".$order_by);
						if (@mysqli_num_rows($get_menu_items) > 0) {
							while ($data_obj2 = mysqli_fetch_object($get_menu_items)) {
								if (isset($data_obj2->name)) {
									$data_obj2->name = str_pad($data_obj2->name, 2, " ", STR_PAD_RIGHT);
								}
								$menu_items_array[] = $data_obj2;
								if ((!strstr($used_modifier_lists, "|".$data_obj2->modifier_list_id."|")) && ($data_obj2->modifier_list_id != 0)) {
									$used_modifier_lists .= "|".$data_obj2->modifier_list_id."|";
								}
							}
						} else {
							echo '{"json_status":"NoItems","for_category":"'.$data_obj->name.'"}';
							exit;
						}
					}
				}
			}
			
		} else {

			echo '{"json_status":"NoGroups"}';
			exit;
		}
		
		if ($got_a_category == FALSE) {
			echo '{"json_status":"NoC"}';
			exit;
		}
		
		$forced_modifier_group_array = array();
		$forced_modifier_group_array[] = new fmg_placeholder();
		$get_forced_modifier_groups = lavu_query("SELECT * FROM `forced_modifier_groups` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id);
		if (@mysqli_num_rows($get_forced_modifier_groups) > 0) {
			while ($data_obj = mysqli_fetch_object($get_forced_modifier_groups)) {
				$forced_modifier_group_array[] = $data_obj;
			}
		}
		
		$forced_modifier_list_array = array();
		$forced_modifier_list_array[] = '{"id":"99999999","title":"place_holder","type":"choice","modifiers":[{"id":"0","title":"MOD ERROR","cost":"0.00","detour":"0"}]}';
		$get_forced_modifier_lists = lavu_query("SELECT `id`, `title`, `type` FROM `forced_modifier_lists` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id);
		if (@mysqli_num_rows($get_forced_modifier_lists) > 0) {
			while ($extract = mysqli_fetch_assoc($get_forced_modifier_lists)) {
				$forced_modifier_array = array();
				$get_forced_modifiers = lavu_query("SELECT `id`, `title`, `cost`, `detour`, `extra`, `extra2` FROM `forced_modifiers` WHERE `list_id` = '".$extract['id']."' AND `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC");
				while ($extract2 = mysqli_fetch_assoc($get_forced_modifiers)) {
					$forced_modifier_array[] = '{"id":"'.$extract2['id'].'","title":"'.str_replace('"', '\"', $extract2['title']).'","cost":"'.$extract2['cost'].'","detour":"'.$extract2['detour'].'","extra":"'.$extract2['extra'].'","extra2":"'.$extract2['extra2'].'"}';
				}
				$forced_modifier_list_array[] = '{"id":"'.$extract['id'].'","title":"'.str_replace('"', '\"', $extract['title']).'","type":"'.$extract['type'].'","modifiers":['.join(",", $forced_modifier_array).']}';
				unset($forced_modifier_array);
			}
		//} else {
			//$forced_modifier_list_array[] = '{"id":"0"}';
		}
		$forced_modifier_list_return_string = '"forced_modifier_lists":['.join(",", $forced_modifier_list_array).']';
		
		$coded_modifier_list_array = array();
		if ($used_modifier_lists != "") {
			$used_modifier_lists_array = explode("|", trim($used_modifier_lists, "|"));
			foreach ($used_modifier_lists_array as $ml) {
				$get_modifiers = lavu_query("SELECT `id`, `title`, `cost` FROM `modifiers` WHERE `category` = '".$ml."' AND `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC");
				if (mysqli_num_rows($get_modifiers) > 0) {
					$modifiers_string = "";
					$count = 0;
					while ($extract = mysqli_fetch_assoc($get_modifiers)) {
						$modifiers_string .= $extract['id']."|o|".$extract['title']."|o|".$extract['cost'];
						$count++;
						if ($count < mysqli_num_rows($get_modifiers)) {
							$modifiers_string .= "|*|";
						}
					}
					$coded_modifier_list_array[] = '{"id":"'.$ml.'","modifiers":"'.str_replace('"', '\"', $modifiers_string).'"}';
				}
			}
		}
		$modifier_list_return_string = '"modifier_lists":['.join($coded_modifier_list_array, ",").']';
		
		$get_quick_pay_buttons = lavu_query("SELECT * FROM `quick_pay_buttons` WHERE `loc_id` = '".$loc_id."' LIMIT 1");
		if (@mysqli_num_rows($get_quick_pay_buttons) > 0) {
			$qp_info = mysqli_fetch_assoc($get_quick_pay_buttons);
			$qp_titles = '["'.$qp_info['title1'].'","'.$qp_info['title2'].'","'.$qp_info['title3'].'","'.$qp_info['title4'].'"]';
			$qp_values = '["'.$qp_info['value1'].'","'.$qp_info['value2'].'","'.$qp_info['value3'].'","'.$qp_info['value4'].'"]';
		} else {
			$qp_titles = '["$1","$5","$10","$20"]';
			$qp_values = '["1","5","10","20"]';
		}
		
		$payment_types_return_string = '{"id":"3","type":"Gift Certificate"}';
		$get_payment_types = lavu_query("SELECT `id`, `type` FROM `payment_types` WHERE `loc_id` = '[1]' AND `_deleted` != '1' ORDER BY `_order` ASC", $loc_id);
		if (@mysqli_num_rows($get_payment_types) > 0) {
			while ($ptype = mysqli_fetch_assoc($get_payment_types)) {
				$payment_types_return_string .= ',{"id":"'.$ptype['id'].'","type":"'.$ptype['type'].'"}';
			}
		}
		
		$tax_rate_array = array();
		$get_tax_rates = lavu_query("SELECT * FROM `tax_rates` WHERE `loc_id` = '".$loc_id."'");
		if (@mysqli_num_rows($get_tax_rates) > 0) {
			while ($data_obj = mysqli_fetch_object($get_tax_rates)) {
				$tax_rate_array[] = $data_obj;
			}
			$tax_rates_return_string = lavu_json_encode($tax_rate_array);
		} else {
			$tax_rates_return_string = '[{"title":"Tax","calc":"'.$location_info['taxrate'].'","type":"p","stack":"y","tier_type1":"","tier_value1":"","force_tier1":"0","calc2":"","force_apply":""}]';
		}
		
		return '"payment_types":['.$payment_types_return_string.'],"tax_rates":'.$tax_rates_return_string.',"qp_titles":'.$qp_titles.',"qp_values":'.$qp_values.',"printer_groups":'.$printer_groups_return_string.',"forced_modifier_groups":'.lavu_json_encode($forced_modifier_group_array).','.$forced_modifier_list_return_string.',"modifications":"'.$modifications.'",'.$modifier_list_return_string.',"menu_categories":'.lavu_json_encode($menu_categories_array).',"menu_items":'.lavu_json_encode($menu_items_array).',"menu_groups":'.lavu_json_encode($menu_groups_array);
	}
	
	function loadComponents($comp_pack) { // load components associated with assigned spin-off package
	
		$layout_ids_string = "";
		$component_layouts_string = "";
		$components_string = "";
					
		$get_layout_ids = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_packages` WHERE `id` = '[1]'", $_REQUEST['comp_pack']);
		if (@mysqli_num_rows($get_layout_ids) > 0) {
			$extract = mysqli_fetch_assoc($get_layout_ids);
			$layout_ids_string = ',"layout_ids":"'.$extract['layout_ids'].'"';
			$layout_ids_array = explode(",", $extract['layout_ids']);
			$component_layouts_array = array();
			$component_ids_array = array();
			$components_array = array();
			foreach ($layout_ids_array as $layout_id) {
				if ($layout_id != "0") {
					$get_layout_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_layouts` WHERE `id` = '[1]'", $layout_id);
					$extract = mysqli_fetch_assoc($get_layout_data);
					$component_layouts_array[] = '{"id":"'.$layout_id.'","title":"'.$extract['title'].'","background":"'.$extract['background'].'","c_ids":"'.$extract['c_ids'].'","c_xs":"'.$extract['c_xs'].'","c_ys":"'.$extract['c_ys'].'","c_ws":"'.$extract['c_ws'].'","c_hs":"'.$extract['c_hs'].'","c_zs":"'.$extract['c_zs'].'","c_types":"'.$extract['c_types'].'"}';
					$these_component_ids = explode(",", $extract['c_ids']);
					foreach ($these_component_ids as $component_id) {
						if (!in_array($component_id, $component_ids_array)) {
							$component_ids_array[] = $component_id;
							$get_component_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`components` WHERE `id` = '[1]'", $component_id);
							$extract2 = mysqli_fetch_assoc($get_component_data);
							$components_array[] = '{"id":"'.$component_id.'","title":"'.$extract2['title'].'","type":"'.$extract2['type'].'","html":"'.$extract2['html'].'","allow_scroll":"'.$extract2['allow_scroll'].'","name":"'.$extract2['name'].'","filename":"'.$extract2['filename'].'"}';
						}
					}
				}
			}
			
			$component_layouts_string = ',"component_layouts":['.join($component_layouts_array, ",").']';
			$components_string = ',"components":['.join($components_array, ",").']';
			
			return $layout_ids_string.$component_layouts_string.$components_string;

		} else {

			echo '{"json_status":"MissingCompPack"}';
			exit;
		}
		
		return "";
	}
		
	function getSupportedPrinters($loc_id) {

		$supported_printers_id_array = array();
		$config_settings_return_string = '{"no_settings":"none"}';
		$get_config_settings = lavu_query("SELECT * FROM `config` WHERE `location` = '".$loc_id."' AND `type` = 'printer'");
		if (@mysqli_num_rows($get_config_settings) > 0) {
			$config_settings_return_string = '{';
			while ($extract_cs = mysqli_fetch_assoc($get_config_settings)) {
				$printer_name = strtolower($extract_cs['setting']);
				$config_settings_return_string .= '"'.$printer_name.'-ip":"'.$extract_cs['value3'].'",';
				$config_settings_return_string .= '"'.$printer_name.'-port":"'.$extract_cs['value5'].'",';
				$config_settings_return_string .= '"'.$printer_name.'_drawer_code":"'.$extract_cs['value4'].'",';
				$config_settings_return_string .= '"'.$printer_name.'_command_set":"'.$extract_cs['value6'].'",';
				$config_settings_return_string .= '"'.$printer_name.'_cpl_standard":"'.$extract_cs['value7'].'",'; // characters per line for standard font
				$config_settings_return_string .= '"'.$printer_name.'_cpl_emphasize":"'.$extract_cs['value8'].'",'; // characters per line for emphasize and double height fonts
				$config_settings_return_string .= '"'.$printer_name.'_lsf":"'.$extract_cs['value9'].'",'; // line spacing factor
				$config_settings_return_string .= '"'.$printer_name.'_raster_capable":"'.$extract_cs['value10'].'",'; // raster capable flag
				if (!in_array($extract_cs['value6'], $supported_printers_id_array)) {
					$supported_printers_id_array[] = $extract_cs['value6'];
				}
			}
			$config_settings_return_string .= '"place_holder":"ignore"}';
		}
	
		$mfilters = array();
		$mcount = 0;
		$spid_filter = "";
		foreach ($supported_printers_id_array as $spid) {
		  $mcount++;
		  $mfilters['p_'.$mcount] = $spid;
			$spid_filter .= " OR `id` = '[p_$mcount]'";
		}
	
		$supported_printers_return_string = '[{"no_printers":"none"}]';
		$printer_commands_array = array();
		$get_supported_printers = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`supported_printers` WHERE `id` = '0'".$spid_filter, $mfilters);
		if (@mysqli_num_rows($get_supported_printers) > 0) {
			while ($extract_sp = mysqli_fetch_object($get_supported_printers)) {
				$printer_commands_array[] = $extract_sp;
			}
			$supported_printers_return_string = lavu_json_encode($printer_commands_array);
		}
		
		return '"supported_printers":'.$supported_printers_return_string.',"config_settings":'.$config_settings_return_string;
	}
	
	function getAvailableLanguagePacks() {
		
		$language_pack_array = array();
		$get_language_packs = mlavu_query("SELECT `id`, `language` FROM `poslavu_MAIN_db`.`language_packs` ORDER BY `language` ASC");
		while ($lang_pack = mysqli_fetch_assoc($get_language_packs)) {
			$language_pack_array[] = '{"id":"'.$lang_pack['id'].'","language":"'.$lang_pack['language'].'"}';
		}
		
		return '['.join($language_pack_array, ",").']';
	}
	
	function generateLanguagePack($loc_id, $pack_id) {

		static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));

		$language_pack_string = '{';
		if ($pack_id == "1") {
			$language_pack_string .= '"STANDARD POSLAVU ENGLISH":"STANDARD POSLAVU ENGLISH"';
		} else if ($pack_id == "0") {
			$language_pack_string .= '"CUSTOM LANGUAGE PACK":"CUSTOM LANGUAGE PACK"';
			$get_custom_pack = lavu_query("SELECT * FROM `custom_dictionary` WHERE `loc_id` = '[1]' AND `translation` != ''", $loc_id);
			while ($extract = mysqli_fetch_assoc($get_custom_pack)) {
				$language_pack_string .= ',"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['word_or_phrase']).'":"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['translation']).'"';
			}
		} else {
			$get_language_name = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` = '[1]'", $pack_id);
			$extract = mysqli_fetch_assoc($get_language_name);
			$language_pack_string .= '"'.strtoupper($extract['language']).' LANGUAGE PACK":"'.strtoupper($extract['language']).' LANGUAGE PACK"';
			$get_language_pack = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '[1]' AND `translation` != ''", $pack_id);
			while ($extract = mysqli_fetch_assoc($get_language_pack)) {
				$language_pack_string .= ',"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['word_or_phrase']).'":"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['translation']).'"';
			}
		}
		$language_pack_string .= '}';
		
		return $language_pack_string;
	}
	
	function get_column_names($table_name) {
 
		$result = lavu_query("SHOW COLUMNS FROM `$table_name`");
		$column_names = array();
		while ($row = mysqli_fetch_assoc($result)) {
			$column_names[] = $row['Field'];
		}
		
		return $column_names;
	}
	
	function create_field_list($table_name, $exclude) {

    $column_names = get_column_names($table_name);
    $field_list = "";
 
		foreach($column_names as $name) {
			if (!in_array($name, $exclude)) {
				if($field_list == "")
					$field_list = "`$name`";
				else
					$field_list .= ", `$name`";
				}
			}
 
    return $field_list;
	}
	
	function update_order_item_details($item_details, $order_id, $loc_id) {

		$item_details_array = explode("|*|", $item_details);
		$get_order_contents = lavu_query("SELECT `id` FROM `order_contents` WHERE `order_id` = '[1]' AND `loc_id` = '[2]' AND `item` != 'SENDPOINT' ORDER BY `id` ASC", $order_id, $loc_id);
		if (@mysqli_num_rows($get_order_contents) > 0) {
			$row = 0;
			while ($order_contents_info = mysqli_fetch_assoc($get_order_contents)) {
				$details = explode(":", $item_details_array[$row]);
				$item_vars = array();
				if (count($details) >= 5) {
					$item_vars['discount_amount'] = $details[0];
					$item_vars['discount_value'] = $details[1];
					$item_vars['discount_type'] = $details[2];
					$item_vars['after_discount'] = $details[3];
					$item_vars['tax_amount'] = $details[4];
				}
				if (count($details) >= 17) {
					$item_vars['total_with_tax'] = $details[5];
					$item_vars['itax_rate'] = $details[6];
					$item_vars['itax'] = $details[7];
					$item_vars['tax_rate1'] = $details[8];
					$item_vars['tax1'] = $details[9];
					$item_vars['tax_rate2'] = $details[10];
					$item_vars['tax2'] = $details[11];
					$item_vars['tax_rate3'] = $details[12];
					$item_vars['tax3'] = $details[13];
					$item_vars['tax_subtotal1'] = $details[14];
					$item_vars['tax_subtotal2'] = $details[15];
					$item_vars['tax_subtotal3'] = $details[16];
				}
				if (count($details) >= 25) {
					$item_vars['void'] = $details[17];
					$item_vars['quantity'] = $details[18];
					$item_vars['discount_id'] = $details[19];
					$item_vars['idiscount_id'] = $details[20];
					$item_vars['idiscount_sh'] = $details[21];
					$item_vars['idiscount_value'] = $details[22];
					$item_vars['idiscount_type'] = $details[23];
					$item_vars['idiscount_amount'] = $details[24];
				}
				if (count($details) >= 29) {
					$item_vars['split_factor'] = $details[25];
					$item_vars['tax_name1'] = $details[26];
					$item_vars['tax_name2'] = $details[27];
					$item_vars['tax_name3'] = $details[28];
				}
				$keys = array_keys($item_vars);
				$q_update = "";
				foreach ($keys as $key) {
					if ($q_update != "") { $q_update .= ", "; }
					$q_update .= "`$key` = '[$key]'";
				}
				$item_vars['id'] = $order_contents_info['id'];
				$update_item = lavu_query("UPDATE `order_contents` SET $q_update WHERE `id` = '[id]'", $item_vars);
				$row++;
			}
		}
	}
	
	$smallest_money = (1 / pow(10, $decimal_places));
	
	if(isset($_POST['m'])) $mode = $_POST['m'];
	else if(isset($_GET['m'])) $mode = $_GET['m'];
	else $mode = "";
	
	if ($mode == "punch_status") {
	
		$punch_query = lavu_query("SELECT `punch_type`, `time`, `time_out`, `id` FROM `clock_punches` WHERE `_deleted` != '1' AND `server_id` = '[1]' ORDER BY `time` DESC LIMIT 1", $_REQUEST['server_id']);
		if (mysqli_num_rows($punch_query)) {
			$punch_read = mysqli_fetch_assoc($punch_query);
			$punch_type = $punch_read['punch_type'];
			$punch_time = $punch_read['time'];
			$punch_time_out = $punch_read['time_out'];
			$punch_id = $punch_read['id'];
		} else {
			$punch_type = "";
			$punch_time = "";
			$punch_time_out = "";
			$punch_id = "";
		}
		if ($punch_type=="Shift" && $punch_time_out=="" && $punch_time!="") $clocked_in = 1; else $clocked_in = 0;
		
		echo '{"json_status":"success","clocked_in":"'.$clocked_in.'","time":"'.$punch_time.'","punchid":"'.$punch_id.'"}';
		exit();
	
	} else if ($mode == "clock_in") {

		$q_fields = "";
		$q_values = "";
		$p_vars = array();
		$p_vars['location'] = $location_info['title'];
		$p_vars['location_id'] = $_REQUEST['loc_id'];
		$p_vars['punch_type'] = "Shift";
		$p_vars['server'] = $_REQUEST['server_name'];
		$p_vars['server_id'] = $_REQUEST['server_id'];
		$p_vars['time'] = $_REQUEST['time'];
		$p_vars['hours'] = "0";
		$p_vars['punched_out'] = "0";
		$keys = array_keys($p_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$insert_punch = lavu_query("INSERT INTO `clock_punches` ($q_fields, `server_time`) VALUES ($q_values, now())", $p_vars);

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = "Server Clocked In";
		$log_vars['loc_id'] = $_REQUEST['loc_id'];
		$log_vars['order_id'] = "0";
		$log_vars['time'] = $_REQUEST['time'];
		$log_vars['user'] = $_REQUEST['server_name'];
		$log_vars['user_id'] = $_REQUEST['server_id'];
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		echo '{"json_status":"success"}';
		exit();
	
	} else if ($mode == "clock_out") {

		$get_punch = lavu_query("SELECT `id`, `time` FROM `clock_punches` WHERE `_deleted` != '1' AND `id` = '[1]'", $_REQUEST['punchid']);
		if (mysqli_num_rows($get_punch)) {
			$extract = mysqli_fetch_assoc($get_punch);
				
			$clockInArray = explode(" ", $extract['time']);
			$clockInDate = explode("-", $clockInArray[0]);
			$clockInTime = explode(":", $clockInArray[1]);
			$inStamp = mktime($clockInTime[0], $clockInTime[1], $clockInTime[2], $clockInDate[1], $clockInDate[2], $clockInDate[0]);
	
			$clockOutArray = explode(" ", $_REQUEST['time']);
			$clockOutDate = explode("-", $clockOutArray[0]);
			$clockOutTime = explode(":", $clockOutArray[1]);
			$outStamp = mktime($clockOutTime[0], $clockOutTime[1], $clockOutTime[2], $clockOutDate[1], $clockOutDate[2], $clockOutDate[0]);
	
			$hours = (($outStamp - $inStamp) / 3600);
		
			$update_punched_out = lavu_query("UPDATE `clock_punches` SET `punched_out` = '1', `time_out` = '[1]', `hours` = '[2]', `server_time_out` = now() WHERE id = '[3]'", $_REQUEST['time'], number_format($hours,3), $extract['id']);
			
			$q_fields = "";
			$q_values = "";
			$log_vars = array();
			$log_vars['action'] = "Server Clocked Out";
			$log_vars['loc_id'] = $_REQUEST['loc_id'];
			$log_vars['order_id'] = "0";
			$log_vars['time'] = $_REQUEST['time'];
			$log_vars['user'] = $_REQUEST['server_name'];
			$log_vars['user_id'] = $_REQUEST['server_id'];
			$keys = array_keys($log_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);
	
			echo '{"json_status":"success","time_in":"'.$clockInArray[1].'","hours":"'.number_format($hours, 3).'"}';
		}
		exit();
	
	} else if ($mode == 1)  { // log in from iPhone, iPod touch, or iPad

		$cust_query = mlavu_query("SELECT `dataname` FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `username` = '[1]'", trim($_REQUEST['un']));
		if(mysqli_num_rows($cust_query) > 0) {
			$cust_read = mysqli_fetch_assoc($cust_query);
			$dataname = $cust_read['dataname'];

			$get_company_info = mlavu_query("SELECT `id`, `data_name`, `company_name`, `company_code`, `logo_img`, `dev`, `demo` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]' and `disabled`!='1'", $dataname);
			if (mysqli_num_rows($get_company_info) > 0) {
				$company_info = mysqli_fetch_assoc($get_company_info);
			} else {
				echo '{"json_status":"NoRestaurants"}';
				exit;
			}

			lavu_connect_byid($company_info['id'], "poslavu_".$dataname."_db");
			
			$user_query = lavu_query("SELECT `id`, `quick_serve`, `service_type`, `f_name`, `l_name`, `access_level`, `PIN` FROM `users` WHERE `username` = '[1]' AND `password` = PASSWORD('[2]')", trim($_REQUEST['un']), trim($_REQUEST['pw']));
			if(mysqli_num_rows($user_query) > 0) {

				$this_user_info = mysqli_fetch_assoc($user_query);
				
				$valid_PINs = "";
				$get_valid_PINs = lavu_query("SELECT `PIN` FROM `users` WHERE `access_level` > '0'");
				while ($extract2 = mysqli_fetch_assoc($get_valid_PINs)) {
					$valid_PINs = $valid_PINs."|".$extract2['PIN']."|";
				}
				
				$valid_admin_PINs = "";
				$get_valid_admin_PINs = lavu_query("SELECT `PIN` FROM `users` WHERE `access_level` > '1'");
				while ($extract2 = mysqli_fetch_assoc($get_valid_admin_PINs)) {
					$valid_admin_PINs = $valid_admin_PINs."|".$extract2['PIN']."|";
				}

				$user_data_array = array();
				$get_all_user_data = lavu_query("SELECT * FROM `users` ORDER BY `l_name`, `f_name` ASC");
				while ($data_obj = mysqli_fetch_object($get_all_user_data)) {
					$user_data_array[] = $data_obj;
				}

				$clocked_in_server_ids = "";
				$get_punches = lavu_query("SELECT `server_id` FROM `clock_punches` WHERE `_deleted` != '1' AND `punch_type` = 'Clocked In' AND `punched_out` = '0'");
				while ($extract2 = mysqli_fetch_assoc($get_punches)) {
					if ($closed_in_server_ids != "" ) {
						$clocked_in_server_ids .= "|";
					}
					$clocked_in_server_ids .= $extract2['server_id'];
				}

				$update_last_activity = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `data_name` = '[1]'", $company_info['data_name']);

				$tack_on_build = ":".$_REQUEST['build'];

				$q_fields = "";
				$q_values = "";
				$log_vars = array();
				$log_vars['action'] = "Server Logged In";
				$log_vars['loc_id'] = "0";
				$log_vars['order_id'] = "0";
				$log_vars['time'] = $device_time;
				$log_vars['user'] = $this_user_info['f_name']." ".$this_user_info['l_name'];
				$log_vars['user_id'] = $this_user_info['id'];
				$keys = array_keys($log_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);
				
				echo '{"json_status":"Valid","server_id":"'.$this_user_info['id'].'","quick_serve":"'.$this_user_info['quick_serve'].'","service_type":"'.$this_user_info['service_type'].'","all_user_data":'.lavu_json_encode($user_data_array).',"clocked_in_server_ids":"'.$clocked_in_server_ids.'","access_level":"'.$this_user_info['access_level'].'","PIN":"'.$this_user_info['PIN'].'","f_name":"'.$this_user_info['f_name'].'","l_name":"'.$this_user_info['l_name'].'","company_id":"'.$company_info['id'].'","company_name":"'.$company_info['company_name'].'","company_code":"'.lsecurity_name($company_info['company_code'], $company_info['id']).$tack_on_build.'","data_name":"'.$company_info['data_name'].'","logo_img":"'.$company_info['logo_img'].'","dev":"'.$company_info['dev'].'","demo":"'.$company_info['demo'].'","valid_PINs":"'.$valid_PINs.'","valid_admin_PINs":"'.$valid_admin_PINs.'"}';
				exit;			

			} else {
			
				echo '{"json_status":"Invalid"}';
				exit;
			}

		} else {
		
			echo '{"json_status":"Invalid"}';
			exit;
		}
				
	} else if ($mode == 2) { // load company locations

		$locations_array = array();
		$exclude = array("integration1", "integration2", "integration3", "integration4", "api_key", "api_token");
 		$get_locations = lavu_query("SELECT ".create_field_list("locations", $exclude)." FROM `locations` ORDER BY `state`, `city`, `title` ASC");
		if (@mysqli_num_rows($get_locations) > 0) {
			while ($data_obj = mysqli_fetch_object($get_locations)) {
				$locations_array[] = $data_obj;
			}
			echo '{"json_status":"success","locations":'.lavu_json_encode($locations_array).'}';
			exit;
		} else {
			echo '{"json_status":"failure"}';
			exit;
		}
					
	} else if ($mode == 3) { // load table setup, printer configuration, menu categories, menu items, menu groups, language pack
	
		$table_setup_array = array();
		//$get_table_setup = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '[1]' ORDER BY `_order`, `title` ASC", $_REQUEST['loc_id']);
		$get_table_setup = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '[1]' ORDER BY `id` ASC", $_REQUEST['loc_id']);
		if (@mysqli_num_rows($get_table_setup) > 0) {
			while ($data_obj = mysqli_fetch_object($get_table_setup)) {
				$table_setup_array[] = $data_obj;
			}
		} else {
			echo '{"json_status":"NoTS"}';
			exit;
		}
		$table_setup_string = lavu_json_encode($table_setup_array);
		
		$discount_types_array = array();
		$get_discount_types = lavu_query("SELECT * FROM `discount_types` WHERE `loc_id` = '[1]' AND `_deleted` != '1' ORDER BY `title` ASC", $_REQUEST['loc_id']);
		while ($data_obj = mysqli_fetch_object($get_discount_types)) {
			$discount_types_array[] = $data_obj;
		}

		$reports_list_return_string = '"reports_list":[{"title":"Register Summary","mode":"register","filename":"till_report.php"},{"title":"Server Summary","mode":"server","filename":"till_report.php"}]';
		$printers_return_string = getSupportedPrinters($_REQUEST['loc_id']);
		$all_menu_data_return_string = loadMenuData($_REQUEST['menu_id'], $_REQUEST['loc_id']);

		$get_server_name = lavu_query("SELECT `f_name`, `l_name` FROM `users` WHERE id = '[1]'", $_REQUEST['server_id']);
		$this_server_info = mysqli_fetch_assoc($get_server_name);
				
		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = "Location Chosen";
		$l_vars['details'] = $location_info['title'];
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = "0";
		$l_vars['check'] = "0";
		$l_vars['time'] = $device_time;
		$l_vars['user'] = $extract['f_name']." ".$extract['l_name'];
		$l_vars['user_id'] = $_REQUEST['server_id'];
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// load components if necessary
		$component_data_return_string = "";
		$comp_pack = (isset($_REQUEST['comp_pack']))?$_REQUEST['comp_pack']:"0";
		if ($comp_pack != "0") {
			$component_data_return_string = loadComponents($comp_pack);
		}

		if (isset($_REQUEST['UDID'])) {
			mlavu_query("UPDATE `poslavu_MAIN_db`.`client_devices` SET `needs_reload` = '0' WHERE `UDID` = '[1]'", $_REQUEST['UDID']);
		}

		echo '{"json_status":"success","table_setup":'.$table_setup_string.',"discount_types":'.lavu_json_encode($discount_types_array).','.$reports_list_return_string.','.$printers_return_string.','.$all_menu_data_return_string.$component_data_return_string.',"available_language_packs":'.getAvailableLanguagePacks().',"language_pack":'.generateLanguagePack($_REQUEST['loc_id'], $location_info['use_language_pack']).'}';
		exit;

	} else if ($mode == 4) { // load open order, returns number of send points
	
		$order_info = array();
		$order_contents = array();
		$order_modifiers = array();
		$send_points = 0;
		
		$vars = array();
		$vars['location_id'] = $_REQUEST['loc_id'];
		$vars['tablename'] =$_REQUEST['table'] ;
		$check_order_id = "";
		if (isset($_REQUEST['order_id'])) {
			$check_order_id = " AND `order_id` = '[order_id]'";
			$vars['order_id'] = $_REQUEST['order_id'];
		}
	
		$get_order = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '[location_id]' AND `closed` = '0000-00-00 00:00:00' AND `tablename` COLLATE latin1_general_cs = '[tablename]'".$check_order_id, $vars);
		if (@mysqli_num_rows($get_order) > 0) {
			$data_obj = mysqli_fetch_object($get_order);
			$order_info[] = $data_obj;
			$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' ORDER BY `id` ASC", $_REQUEST['loc_id']);
			$send_points = 0;
			while ($data_obj2 = mysqli_fetch_object($get_contents)) {
				$order_contents[] = $data_obj2;
				if ($data_obj2->item == "SENDPOINT") {
					$send_points++;
				}
			}
			$get_modifiers = lavu_query("SELECT * FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' AND `qty` != 0 ORDER BY `id` ASC", $_REQUEST['loc_id']);
			while ($data_obj2 = mysqli_fetch_object($get_modifiers)) {
				$order_modifiers[] = $data_obj2;
			}				

		} else {
			echo '{"json_status":"NoMatch"}';
			exit;
		}
		
		echo '{"json_status":"success","order_info":'.lavu_json_encode($order_info).',"order_contents":'.lavu_json_encode($order_contents).',"order_modifiers":'.lavu_json_encode($order_modifiers).',"send_points":"'.$send_points.'"}';
		exit;
	
	} else if ($mode == 5) { // save open order info, returns order id, deprecated and replaced by mode 28

	} else if ($mode == 6) { // save order contents (deprecated in favor of POST method, see mode = 17)

	} else if ($mode == 7) { // load open orders
	
		$lavu_togo = (isset($_REQUEST['lavu_togo']))?$_REQUEST['lavu_togo']:"0";
		
		$order_info = array();
		$order_contents = array();
		$recorded_payments = array();
		
		$limit_string = "";
		if (isset($_REQUEST['page'])) {
			$limit_string = " LIMIT ".(((int)$_REQUEST['page'] - 1) * 20).", 20";
		}

		$get_count = lavu_query("SELECT COUNT(`id`) FROM `orders` WHERE `location_id` ='[1]' AND `closed` = '0000-00-00 00:00:00' AND `opened` != '0000-00-00 00:00:00'", $_REQUEST['loc_id']);
		$total_count = mysqli_result($get_count, 0);
		
		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `location_id` ='[1]' AND `closed` = '0000-00-00 00:00:00' AND `opened` != '0000-00-00 00:00:00' ORDER BY `order_id` ASC".$limit_string, $_REQUEST['loc_id']);
		if (@mysqli_num_rows($get_orders) > 0) {
			while ($data_obj = mysqli_fetch_object($get_orders)) {
				$order_info[] = $data_obj;
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' AND `quantity` > 0 ORDER BY `id` ASC", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}
				$get_payments_and_refunds = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' AND `voided` = '0' AND `tip_for_id` = '0'", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_payments_and_refunds)) {
					$recorded_payments[] = $data_obj2;
				}
			}
		} else {
			echo '{"json_status":"NoMatch"}';
			exit;
		}
		
		echo '{"json_status":"success","order_info":'.lavu_json_encode($order_info).',"order_contents":'.lavu_json_encode($order_contents).',"recorded_payments":'.lavu_json_encode($recorded_payments).',"total_count":"'.$total_count.'"}';
		exit;
	
	} else if ($mode == 8) { // load today's closed orders
	
		if (isset($_REQUEST['check_time'])) {

			$start_datetime = $_REQUEST['check_time'];
		
		} else {

			$se_hour = substr(str_pad($_REQUEST['start_time'], 4, "0", STR_PAD_LEFT), 0, 2);
			$se_min = substr(str_pad($_REQUEST['start_time'], 4, "0", STR_PAD_LEFT), 2, 2);
		
			$now_parts = explode(" ", $_REQUEST['now_time']);
			$now_date = explode("-", $now_parts[0]);
			$now_time = explode(":", $now_parts[1]);

			if (($now_time[0].$now_time[1]) < ($se_hour.$se_min)) {
				$use_ts = mktime($se_hour, $se_min, 0, $now_date[1], ($now_date[2] - 1), $now_date[0]);
				$start_datetime = date("Y-m-d H:i:s", $use_ts);
			} else {
				$start_datetime = $now_parts[0]." $se_hour:$se_min:00";
			}
		}

		$order_info = array();
		$order_contents = array();
		$order_modifiers = array();
		$check_details = array();
		$recorded_payments = array();
		
		$limit_string = "";
		if (isset($_REQUEST['page'])) {
			$limit_string = " LIMIT ".(((int)$_REQUEST['page'] - 1) * 20).", 20";
		}
		
		$filter = " AND `closed` >= '$start_datetime' AND `void` != '1'";
		if (!empty($_REQUEST['filter'])) {
			$filter = str_replace("[server_prefix]", $server_order_prefix, $_REQUEST['filter']);
		}

		$get_count = lavu_query("SELECT COUNT(`id`) FROM `orders` WHERE `location_id` = '[1]'$filter", $_REQUEST['loc_id']);
		$total_count = mysqli_result($get_count, 0);
			
		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '[1]'$filter ORDER BY `closed` DESC".$limit_string, $_REQUEST['loc_id']);
		if (@mysqli_num_rows($get_orders) > 0) {
			while ($data_obj = mysqli_fetch_object($get_orders)) {
				$order_info[] = $data_obj;
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' AND `quantity` > 0 ORDER BY `id` ASC", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}				
				$get_modifiers = lavu_query("SELECT * FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' AND `qty` != 0 ORDER BY `id` ASC", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_modifiers)) {
					$order_modifiers[] = $data_obj2;
				}				
				$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."'", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_details)) {
					$check_details[] = $data_obj2;
				}
				$get_payments_and_refunds = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' AND `voided` = '0' AND `tip_for_id` = '0'", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_payments_and_refunds)) {
					$recorded_payments[] = $data_obj2;
				}
			}

		} else {
			echo '{"json_status":"NoMatch"}';
			exit;
		}
		
		echo '{"json_status":"success","order_info":'.lavu_json_encode($order_info).',"order_contents":'.lavu_json_encode($order_contents).',"order_modifiers":'.lavu_json_encode($order_modifiers).',"check_details":'.lavu_json_encode($check_details).',"recorded_payments":'.lavu_json_encode($recorded_payments).',"total_count":"'.$total_count.'"}';
		exit;
	
	} else if ($mode == 9) { // check open orders

		$open_orders = "";
		$checks_printed = "";
		$table_list = "";
		$id_list = "";
		$printed_list = "";
		
		if (isset($_REQUEST['tab_mode'])) {
			if ($_REQUEST['tab_mode'] == "tabs") {
				$tab_filter = " AND `tab` = '1'";
			} else if ($_REQUEST['tab_mode'] == "tables") {
				$tab_filter = " AND `tab` != '1'";
			} else if ($_REQUEST['tab_mode'] == "both") {
				$tab_filter = "";
			} else {
				$tab_filter = " AND `tab` != '1'";
			}
		} else {
			$tab_filter = " AND `tab` != '1'";
		}
		
		if (isset($_REQUEST['for_id'])) {
			if ($_REQUEST['for_id'] == "0") {
				$server_id_filter = "";
			} else {
				$server_id_filter = " AND `server_id` = '".$_REQUEST['for_id']."'";
			}
		} else {
			$server_id_filter = "";
		}
	
		$get_open_orders = lavu_query("SELECT `tablename`, `order_id`, `check_has_printed` FROM `orders` WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `tablename` != 'Quick Serve' AND `closed` = '0000-00-00 00:00:00'".$tab_filter.$server_id_filter." ORDER BY `tablename` ASC, `opened` DESC");
		if (mysqli_num_rows($get_open_orders) > 0) {
			while ($extract = mysqli_fetch_assoc($get_open_orders)) {
				$table_list .= "|".$extract['tablename']."|";
				$id_list .= "|".$extract['order_id']."|";
				if ($extract['check_has_printed'] == "1") {
					$printed_list .= "|".$extract['tablename']."|";
				}
			}
			$open_orders = '"open_orders":"|'.$table_list.'|"';
			$checks_printed = '"checks_printed":"|'.$printed_list.'|"';
		} else {
			$open_orders = '"open_orders":"none"';
			$checks_printed = '"checks_printed":"none"';
		}
		
		echo '{"json_status":"success",'.$open_orders.','.$checks_printed.',"open_ids":"|'.$id_list.'|"}';
		exit;

	} else if ($mode == 10) { // close order after checkout

		// ***** close_ifo indexes *****
		//
		//  0 - closed
		//  1 - discount
		//  2 - discount label
		//  3 - cash paid
		//  4 - card paid
		//  5 - BLANK
		//  6 - server name
		//  7 - tax amount
		//  8 - total
		//  9 - PIN used
		// 10 - tax rate
		// 11 - send to email
		// 12 - gratuity
		// 13 - cashier ID
		// 14 - paid by gift certificate
		// 15 - gratuity percent
		// 16 - refund amount
		// 17 - refund gift certificate amount
		// 18 - BLANK
		// 19 - subtotal
		// 20 - deposit status
		// 21 - active register
		// 22 - discount value
		// 23 - discount type
		// 24 - refund cc amount
		// 25 - rounded amount
		// 26 - auto gratuity is taxed
		// 27 - active register name

		$close_info = explode("|*|", $_REQUEST['close_info']);

		$auth_by = convertPIN($close_info[9], "name");
		$auth_by_id = convertPIN($close_info[9]);
		
		$closed_action = "Order Closed";
		
		update_order_item_details($_REQUEST['item_details'], $_REQUEST['order_id'], $_REQUEST['loc_id']);
	
		$get_totals_from_contents = lavu_query("SELECT SUM(`subtotal_with_mods`) AS `subtotal`, SUM(`discount_amount`) AS `discount`, SUM(`tax_amount`) AS `tax`, SUM(`total_with_tax`) AS `total` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		$total_data = mysqli_fetch_assoc($get_totals_from_contents);

		$q_update = "";
		$order_vars = array();
		$order_vars['subtotal'] = setCorrectSum($close_info[19], $total_data['subtotal'], $smallest_money);
		$order_vars['tax'] = setCorrectSum($close_info[7], $total_data['tax'], $smallest_money);
		$order_vars['total'] = setCorrectSum($close_info[8], $total_data['total'], $smallest_money);
		$order_vars['closed'] = $close_info[0];
		$order_vars['taxrate'] = $close_info[10];
		$order_vars['discount'] = setCorrectSum($close_info[1], $total_data['discount'], $smallest_money);
		$order_vars['discount_sh'] = $close_info[2];
		$order_vars['discount_value'] = $close_info[22];
		$order_vars['discount_type'] = $close_info[23];
		$order_vars['gratuity'] = $close_info[12];
		$order_vars['gratuity_percent'] = $close_info[15];
		$order_vars['cash_paid'] = $close_info[3];
		$order_vars['cash_applied'] = ($close_info[3] - $close_info[5]);
		if ($location_info['integrateCC'] != "1") {
			$order_vars['card_paid'] = $close_info[4];
		}
		$order_vars['gift_certificate'] = $close_info[14];
		$order_vars['change_amount'] = 0;
		$order_vars['refunded'] = $close_info[16];
		$order_vars['refunded_cc'] = $close_info[24];
		$order_vars['refunded_gc'] = $close_info[17];
		$order_vars['cashier'] = $close_info[6];
		$order_vars['cashier_id'] = $close_info[13];
		$order_vars['auth_by'] = $auth_by;
		$order_vars['auth_by_id'] = $auth_by_id;
		$order_vars['email'] = $close_info[11];
		$order_vars['register'] = $close_info[21];
		$order_vars['register_name'] = $close_info[27];
		$order_vars['auto_gratuity_is_taxed'] = $close_info[26];
		$order_vars['closing_device'] = (isset($_REQUEST['UDID']))?$_REQUEST['UDID']:"";
		$keys = array_keys($order_vars);
		foreach ($keys as $key) {
			if ($q_update != "") { $q_update .= ", "; }
			$q_update .= "`$key` = '[$key]'";
		}
			$order_vars['location_id'] = $_REQUEST['loc_id'];
		$order_vars['order_id'] = $_REQUEST['order_id'];

		$update_closed = lavu_query("UPDATE `orders` SET $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $order_vars);
	
		$deposit_order_ids = array();
		$total_deposit_amount = 0;

		$get_order_contents = lavu_query("SELECT `order_contents`.`item_id` as item_id, `order_contents`.`subtotal_with_mods` as subtotal_with_mods, `order_contents`.`allow_deposit` as allow_deposit, `order_contents`.`deposit_info` as deposit_info, `order_contents`.`item` as item, `order_contents`.`options` as options, `order_contents`.`quantity` as quantity, `menu_items`.`hidden_value` as hidden_value, `menu_items`.`hidden_value2` as hidden_value2 FROM `order_contents` LEFT JOIN `menu_items` ON `menu_items`.`id` = `order_contents`.`item_id` WHERE `order_contents`.`loc_id` = '[1]' AND `order_contents`.`order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (@mysqli_num_rows($get_order_contents) > 0) {
			$should_update_deposit_status = 0;
			while ($extract = mysqli_fetch_assoc($get_order_contents)) {
			
				// deduct inventory and record usage
				$itemid = $extract['item_id'];
				$item_qty = $extract['quantity'];
				$usage_query = lavu_query("select * from `ingredient_usage` where `orderid`='[1]' and `itemid`='[2]' AND `ingredientid` = '[3]' AND `loc_id` = '[4]'", $_REQUEST['order_id'], $itemid, $_REQUEST['loc_id']);
				if(mysqli_num_rows($usage_query))
				{
					$usage_read = mysqli_fetch_assoc($usage_query);
					$usage_qty = $usage_read['qty'];
					$usage_id = $usage_read['id'];
				}
				else 
				{
					$usage_qty = 0;
					$usage_id = false;
				}
				$used_qty = $item_qty - $usage_qty;
				if($used_qty > 0)
				{
					$item_query = lavu_query("select * from `menu_items` where `id`='".$itemid."'");
					if(mysqli_num_rows($item_query))
					{
						$item_read = mysqli_fetch_assoc($item_query);
						$inglist = explode(",",$item_read['ingredients']);
						for($i=0; $i<count($inglist); $i++)
						{
							$inginfo = explode("x",$inglist[$i]);
							$ingid = trim($inginfo[0]);
							if ($ingid > 0) {
								if(count($inginfo) > 1) $ingqty = trim($inginfo[1]);
								else $ingqty = 1;
								
								lavu_query("insert into `ingredient_usage` (`ts`,`date`,`orderid`,`itemid`,`ingredientid`,`qty`,`loc_id`) values ('".time()."','".date("Y-m-d H:i:s")."','".$_REQUEST['order_id']."','".$itemid."','".$ingid."','".($used_qty * $ingqty)."','".$_REQUEST['loc_id']."')");
								usleep(10);
								lavu_query("update `ingredients` set `qty`=(`qty`-".($used_qty * $ingqty).") where `id`='".$ingid."'");
							}
						}
					}
					
					if($usage_id)
						lavu_query("update `ingredient_usage` set `qty`='[1]' where `id`='[2]'", $item_qty, $usage_id);
					else
						lavu_query("insert into `ingredient_usage` (`ts`,`date`,`orderid`,`itemid`,`ingredientid`,`qty`,`loc_id`) values ('".time()."','".date("Y-m-d H:i:s")."','".$_REQUEST['order_id']."','".$itemid."','','".$item_qty."','".$_REQUEST['order_id']."','".$_REQUEST['loc_id']."')");
				}
				
				// check for need to apply LavuKart race credits or membership
			
				if (strstr($extract['options'], "RACER:")) {
					$racer_id = substr($extract['options'], (strpos($extract['options'], "(") + 1), (strpos($extract['options'], ")") - (strpos($extract['options'], "(") + 1)));
					$get_racer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '[1]'", $racer_id);
					if (@mysqli_num_rows($get_racer_info) > 0) {
						$extract_r = mysqli_fetch_assoc($get_racer_info);

						$set_confirmed = "";
						if ($extract_r['confirmed_by_attendant'] == "") {
							$set_confirmed = ", `confirmed_by_attendant` = '".$close_info[13]."'";
							$signature = "Signature";
							if ($extract_r['minor_or_adult_at_signup'] == "minor") {
								$signature = "Signatures";
							}
							$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$signature." Confirmed', '".$racer_id."', '".$_REQUEST['order_id']."', now(), '".$close_info[13]."', '0', '".$_REQUEST['loc_id']."')");
						}
						
						$update_total_visits = "";
						$today = date("Y-m-d");
						$last_activity = explode(" ", $extract_r['last_activity']);
						if ($last_activity != $today) {
							$update_total_visits = ", `total_visits` = `total_visits`+1";
						}

						if ($extract['hidden_value2'] == "membership") {
	
							$add_credits = ($extract['hidden_value'] * $extract['quantity']);
					
							$now = time();
							$update_membership = lavu_query("UPDATE `lk_customers` SET `credits` = `credits`+".$add_credits.$set_confirmed.", `membership` = '".$extract['item']."', `membership_expiration` = '".date("Y-m-d", mktime(date("h", $now), date("i", $now), date("s", $now), date("m", $now), date("d", $now), (date("Y", $now) + 1)))."', `last_activity` = now()".$update_total_visits." WHERE `id` = '".$racer_id."'");
							
							$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$extract['item']."', '".$racer_id."', '".$_REQUEST['order_id']."', now(), '".$close_info[13]."', '".$add_credits."', '".$_REQUEST['loc_id']."')");
						
						} else if ($extract['hidden_value2'] == "race credits") {		

							$add_credits = ($extract['hidden_value'] * $extract['quantity']);
							
							$show_s = "";
							if ($add_credits > 1) {
								$show_s = "s";
							}
						
							$update_credits = lavu_query("UPDATE `lk_customers` SET `credits` = `credits`+".$add_credits.$set_confirmed.", `last_activity` = now()".$update_total_visits." WHERE id = '".$racer_id."'");
	
							$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('Added ".$add_credits." Race Credit".$show_s."', '".$racer_id."', '".$_REQUEST['order_id']."', now(), '".$close_info[13]."', '".$add_credits."', '".$_REQUEST['loc_id']."')");
						}
					}
				}
				
				// check for LavuKart group event info, payments, deposits
				
				if ($extract['hidden_value2'] == "group event") {
					$info_parts = explode(":", $extract['options']);
					$event_id = str_replace("Event #", "", $info_parts[0]);
					$update_event_record = lavu_query("UPDATE `lk_group_events` SET `item_id` = '".$extract['item_id']."', `duration` = '".$extract['hidden_value']."', `order_id` = '".$_REQUEST['order_id']."', `checked_out` = '1' WHERE `id` = '".$event_id."'");
					$update_log_record = lavu_query("UPDATE `lk_action_log` SET `order_id` = '[1]' WHERE `group_event_id` = '[2]'", $_REQUEST['order_id'], $event_id);
				}
				
				if ($extract['allow_deposit'] == "1") {
					$should_update_deposit_status = 3;
				} else if ($extract['allow_deposit'] == "2") {
					$should_update_deposit_status = 2;
					$total_deposit_amount = ($total_deposit_amount + $extract['subtotal_with_mods']);
				}
				
				//if ($close_info[20] == "3") {
				//	$deposit_info = explode("#", $extract['deposit_info']);
				//	if (!in_array($deposit_info[1], $deposit_order_ids)) {
				//		$deposit_order_ids[] = $deposit_info[1];
				//	}
				//}			
			}
		}
		
		// apply appropriate LavuKart special event deposit status
				
		if ($should_update_deposit_status != 0) {
			$set_as_opened = "";
			$record_deposit_amount = "";
			if ($close_info[20] == "3") {
				$log_message = "Remaining payment received for Group Event";
				$should_update_deposit_status = 3;
				//foreach ($deposit_order_ids as $doi) {
					//$update_order = lavu_query("UPDATE `orders` SET `deposit_status` = '3' WHERE `order_id` = '".$doi."' AND `location_id` = '".$_REQUEST['loc_id']."'");
				//}
			} else {
				if ($should_update_deposit_status == 2) {
					$log_message = "Deposit received for Group Event";
					$set_as_opened = ", `closed` = '0000-00-00 00:00:00'";
					$record_deposit_amount = ", `deposit_amount` = '".number_format($total_deposit_amount, $decimal_places, ".", "")."', `subtotal_without_deposit` = '".number_format(($close_info[8] - $total_deposit_amount), $decimal_places, ".", "")."'";
					$closed_action = "Deposit paid";
				} else if ($should_update_deposit_status == 3) {
					$log_message = "Full payment received for Group Event";
				}
			}
			$update_deposit_status = lavu_query("UPDATE `orders` SET `deposit_status` = '".$should_update_deposit_status."'".$set_as_opened.$record_deposit_amount." WHERE `order_id` = '".$_REQUEST['order_id']."' AND location_id = '".$_REQUEST['loc_id']."'");
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$log_message."', '0', '".$_REQUEST['order_id']."', now(), '".$close_info[13]."', '0', '".$_REQUEST['loc_id']."')");
		}
	
		$check_details_array = explode("|*|", $_REQUEST['check_details']);

		$subtotal_array = explode("|o|", $check_details_array[0]);
		$discount_array = explode("|o|", $check_details_array[1]);
		$discount_sh_array = explode("|o|", $check_details_array[2]);
		$tax_array = explode("|o|", $check_details_array[3]);
		$gratuity_array = explode("|o|", $check_details_array[4]);
		$cash_array = explode("|o|", $check_details_array[5]);
		$card_array = explode("|o|", $check_details_array[6]);
		$gift_certificate_array = explode("|o|", $check_details_array[7]);
		$refund_array = explode("|o|", $check_details_array[8]);
		$remaining_array = explode("|o|", $check_details_array[9]);
		$total_array = explode("|o|", $check_details_array[10]);
		$email_array = explode("|o|", $check_details_array[11]);
		$discount_ids_array = explode("|o|", $check_details_array[12]);
		$refund_gc_array = explode("|o|", $check_details_array[13]);
		$discount_values_array = explode("|o|", $check_details_array[14]);
		$discount_types_array = explode("|o|", $check_details_array[16]);
		$refund_cc_array = explode("|o|", $check_details_array[17]);
		$rounding_amounts_array = explode("|o|", $check_details_array[18]);

		for ($i = 0; $i < count($subtotal_array); $i++) {
		
			$get_totals_from_contents = lavu_query("SELECT SUM(`subtotal_with_mods`) AS `subtotal`, SUM(`discount_amount`) AS `discount`, SUM(`tax_amount`) AS `tax`, SUM(`total_with_tax`) AS `total` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $_REQUEST['loc_id'], $_REQUEST['order_id'], ($i + 1));
			$total_data = mysqli_fetch_assoc($get_totals_from_contents);

			$q_update = "";
			$q_fields = "";
			$q_values = "";
			$detail_vars = array();
			$detail_vars['subtotal'] = setCorrectSum($subtotal_array[$i], $total_data['subtotal'], $smallest_money);
			$detail_vars['tax'] = setCorrectSum($tax_array[$i], $total_data['tax'], $smallest_money);
			$detail_vars['check_total'] = setCorrectSum($total_array[$i], $total_data['total'], $smallest_money);
			$detail_vars['discount'] = setCorrectSum($discount_array[$i], $total_data['discount'], $smallest_money);
			$detail_vars['discount_sh'] = $discount_sh_array[$i];
			$detail_vars['discount_value'] = $discount_values_array[$i];
			$detail_vars['discount_type'] = $discount_types_array[$i];
			$detail_vars['discount_id'] = $discount_ids_array[$i];
			$detail_vars['gratuity'] = $gratuity_array[$i];
			$detail_vars['gratuity_percent'] = $check_details_array[15];
			$detail_vars['cash'] = $cash_array[$i];
			if ($location_info['integrateCC'] != "1") {
				$detail_vars['card'] = $card_array[$i];
			}
			$detail_vars['gift_certificate'] = $gift_certificate_array[$i];
			$detail_vars['refund'] = $refund_array[$i];
			$detail_vars['refund_cc'] = $refund_cc_array[$i];
			$detail_vars['refund_gc'] = $refund_gc_array[$i];
			$detail_vars['remaining'] = $remaining_array[$i];
			$detail_vars['email'] = $email_array[$i];
			$detail_vars['change'] = 0;
			$detail_vars['cash_applied'] = $cash_array[$i];
			$detail_vars['rounding_amount'] = $rounding_amounts_array[$i];
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$detail_vars['loc_id'] = $_REQUEST['loc_id'];
			$detail_vars['order_id'] = $_REQUEST['order_id'];
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			
			$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '".($i + 1)."'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
			if (@mysqli_num_rows($check_for_details) > 0) {
				$update_details = lavu_query("UPDATE `split_check_details` SET $q_update WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '".($i + 1)."'", $detail_vars);
			} else {
				$create_details = lavu_query("INSERT INTO `split_check_details` ($q_fields, `check`) VALUES ($q_values, '".($i + 1)."')", $detail_vars);
			}
		}

		$clean_up_extra_details = lavu_query("DELETE FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` > '".count($subtotal_array)."'", $detail_vars);

		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = $closed_action;
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = $_REQUEST['order_id'];
		$l_vars['time'] = $close_info[0];
		$l_vars['user'] = $close_info[6];
		$l_vars['user_id'] = $close_info[13];
		$l_vars['device_udid'] = (isset($_REQUEST['UDID']))?$_REQUEST['UDID']:"";
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo "1";
		exit;

	} else if ($mode == 11) { // update send_status for open order, save send point, log send

		// ***** order_info indexes *****
		//
		//  0 order id
		//  1 table 
		//  2 server who opened order (name)
		//  3 order total
		//  4 time
		//  5 server who sent order (name)
		//  6 server who sent order (id)
		//  7 server who opened order (id)

		$order_info = explode("|*|", $_REQUEST['order_info']);
		
		$get_db_order_info = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $order_info[0]);
		$db_order_info = mysqli_fetch_assoc($get_db_order_info);
		
		$sending_course = (isset($_REQUEST['sending_course']))?$_REQUEST['sending_course']:"0";
		$max_course = (isset($_REQUEST['max_course']))?$_REQUEST['max_course']:"0";

		$q_array = array();
		if (($sending_course == 0) || ($sending_course >= $max_course)) {
			$q_array[] = "`send_status` = '0'";
		}
		$q_array[] = "`last_course_sent` = '[3]'";

		if (count($q_array) > 0) {
			$update_send_status = lavu_query("UPDATE `orders` SET ".join(", ", $q_array)." WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $order_info[0], $sending_course);
		}
		
		$filter_sent = "";
		if ($sending_course != 0) {
			$filter_sent = " AND `course` = '[3]'";
		}
		$update_items_sent = lavu_query("UPDATE `order_contents` SET `sent` = '1' WHERE `loc_id` = '[1]' AND `order_id` = '[2]'$filter_sent", $_REQUEST['loc_id'], $order_info[0], $sending_course);
		
		//deBug("...sending_course: ".$sending_course."\n\nmax_course: ".$max_course."\n\n".print_r($q_array, true));

		$q_fields = "";
		$q_values = "";
		$sp_vars = array();
		$sp_vars['loc_id'] = $_REQUEST['loc_id'];
		$sp_vars['order_id'] = $order_info[0];
		$sp_vars['item'] = "SENDPOINT";
		$sp_vars['price'] = "0";
		$sp_vars['quantity'] = $order_info[6];
		$sp_vars['options'] = $order_info[5];
		$sp_vars['special'] = $order_info[4];
		$sp_vars['modify_price'] = "0";
		$sp_vars['print'] = "0";
		$sp_vars['check'] = "0";
		$sp_vars['seat'] = "0";
		$sp_vars['course'] = $sending_course;
		$sp_vars['item_id'] = "0";
		$sp_vars['printer'] = "0";
		$sp_vars['modifier_list_id'] = "0";
		$sp_vars['device_time'] = $order_info[4];
		$keys = array_keys($sp_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$insert_send_point = lavu_query("INSERT INTO `order_contents` ($q_fields, `server_time`) VALUES ($q_values, now())", $sp_vars);
														
		$q_fields = "";
		$q_values = "";
		$sl_vars = array();
		$sl_vars['order_id'] = $order_info[0];
		$sl_vars['location'] = $location_info['title'];
		$sl_vars['location_id'] = $_REQUEST['loc_id'];
		$sl_vars['tablename'] = $order_info[1];
		$sl_vars['total'] = $order_info[3];
		$sl_vars['server'] = $order_info[2];
		$sl_vars['server_id'] = $order_info[7];
		$sl_vars['send_server'] = $order_info[5];
		$sl_vars['send_server_id'] = $order_info[6];
		$sl_vars['time_sent'] = $order_info[4];
		$sl_vars['device_udid'] = $_REQUEST['UDID'];
		$keys = array_keys($sl_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$update_send_log = lavu_query("INSERT INTO `send_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $sl_vars); 
	
		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		if ($sending_course == 0) {
			$l_vars['action'] = "Order Sent to Kitchen";
		} else {
			$l_vars['action'] = "Course Sent to Kitchen";
			$l_vars['details'] = $sending_course;
		}
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = $order_info[0];
		$l_vars['time'] = $order_info[4];
		$l_vars['user'] = $order_info[5];
		$l_vars['user_id'] = $order_info[6];
		$l_vars['device_udid'] = $_REQUEST['UDID'];
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo "1";
		exit;

	} else if ($mode == 12) { // perform void

		// ****** void_info indexes ******
		//
		//  0 - time
		//  1 - server name
		//  2 - PIN used

		$void_info = explode("|*|", $_REQUEST['void_info']);

		$auth_by = convertPIN($void_info[2], "name");
		$auth_by_id = convertPIN($void_info[2]);
		
		$v_vars = array();
		$v_vars['closed'] = $void_info[0];
		$v_vars['cashier'] = $void_info[1];
		$v_vars['auth_by'] = $auth_by;
		$v_vars['auth_by_id'] = $auth_by_id;
		$v_vars['location_id'] = $_REQUEST['loc_id'];
		$v_vars['order_id'] = $_REQUEST['order_id'];
		$v_vars['closing_device'] = (isset($_REQUEST['UDID']))?$_REQUEST['UDID']:"";
		$record_void = lavu_query("UPDATE `orders` SET `closed` = '[closed]', `void` = '1', `cashier` = '[cashier]', `auth_by` = '[auth_by]', `auth_by_id` = '[auth_by_id]', `closing_device` = '[closing_device]' WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $v_vars);

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = "Order Voided";
		$log_vars['loc_id'] = $_REQUEST['loc_id'];
		$log_vars['order_id'] = $_REQUEST['order_id'];
		$log_vars['time'] = $void_info[0];
		$log_vars['user'] = $auth_by;
		$log_vars['user_id'] = $auth_by_id;
		$log_vars['device_udid'] = (isset($_REQUEST['UDID']))?$_REQUEST['UDID']:"";
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		
		echo '{"json_status":"success"}';
		exit;
		
	} else if ($mode == 13) { // log item edits after send
	
		// ****** item_info indexes ******
		//
		//  0 - note
		//  1 - order id
		//  2 - item name
		//  3 - options
		//  4 - special
		//  5 - quantity
		//  6 - time
		//  7 - server name
		//  8 - server ID
		//  9 - PIN used
		// 10 - modify_price

		$item_info = explode("|*|", $_REQUEST['item_info']);

		$auth_by = convertPIN($item_info[9], "name");

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['note'] = $item_info[0];
		$log_vars['order_id'] = $item_info[1];
		$log_vars['location'] = $location_info['title'];
		$log_vars['location_id'] = $_REQUEST['loc_id'];
		$log_vars['item'] = $item_info[2];
		$log_vars['options'] = $item_info[3];
		$log_vars['special'] = $item_info[4];
		$log_vars['modify_price'] = $item_info[10];
		$log_vars['quantity'] = $item_info[5];
		$log_vars['time'] = $item_info[6];
		$log_vars['server'] = $item_info[7];
		$log_vars['server_id'] = $item_info[8];
		$log_vars['auth_by'] = $auth_by;
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$update_log = lavu_query("INSERT INTO `edit_after_send_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars); 
	
		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = "Item changed after send - ".$item_info[0];
		$log_vars['loc_id'] = $_REQUEST['loc_id'];
		$log_vars['order_id'] = $item_info[1];
		$log_vars['time'] = $item_info[6];
		$log_vars['user'] = $item_info[7];
		$log_vars['user_id'] = $item_info[8];
		$log_vars['details'] = $item_info[5]." ".$item_info[2];
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 14) { // email receipt,  deprecated as of version 1.7, replaced by mode 43

	} else if ($mode == 15) { // save payment and discount details, this mode is considered deprecated after version 1.6.5 build 20110406-1, replaced by mode 40

	} else if ($mode == 16) { // update number of guests for order
		
		$q_update = "";
		$o_vars = array();
		$o_vars['guests'] = $_REQUEST['guests'];
		if ($_REQUEST['gratuity'] != "DONOTUPDATE") {
			$o_vars['gratuity'] = $_REQUEST['gratuity'];
		}
		$keys = array_keys($o_vars);
		foreach ($keys as $key) {
			if ($q_update != "") { $q_update .= ", "; }
			$q_update .= "`$key` = '[$key]'";
		}
		$o_vars['location_id'] = $_REQUEST['loc_id'];
		$o_vars['order_id'] = $_REQUEST['order_id'];
		
		$update_order = lavu_query("UPDATE `orders` SET $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 17) { // save order contents by POST method, may need to keep around for 'adding to tab' functionality

		// ***** order_item indexes *****
		//
		//  0 - item
		//  1 - price
		//  2 - quantity
		//  3 - options
		//  4 - special
		//  5 - print
		//  6 - modify price
		//  7 - check number
		//  8 - seat number
		//  9 - item id
		// 10 - printer
		// 11 - apply taxrate
		// 12 - custom taxrate
		// 13 - modifier list id
		// 14 - forced modifier group id
		// 15 - forced modifiers price
		// 16 - course number
		// 17 - open item
		// 18 - allow deposit
		// 19 - deposit info

		if (!isset($_POST['do_not_clear'])) {
			$clear_current_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_POST['loc_id'], $_POST['order_id']);
		}

		$order_contents = explode("|*|", $_POST['order_contents']);

		foreach ($order_contents as $oc) {
			$order_item = explode("|o|", $oc);
			$q_fields = "";
			$q_values = "";
			$oc_vars = array();
			$oc_vars['loc_id'] = $_POST['loc_id'];
			$oc_vars['order_id'] = $_POST['order_id'];
			$oc_vars['item'] = $order_item[0];
			$oc_vars['price'] = $order_item[1];
			$oc_vars['quantity'] = $order_item[2];
			$oc_vars['options'] = $order_item[3];
			$oc_vars['special'] = $order_item[4];
			$oc_vars['modify_price'] = $order_item[5];
			$oc_vars['print'] = $order_item[6];
			$oc_vars['check'] = $order_item[7];
			$oc_vars['seat'] = $order_item[8];
			$oc_vars['item_id'] = $order_item[9];
			$oc_vars['printer'] = $order_item[10];
			$oc_vars['apply_taxrate'] = $order_item[11];
			$oc_vars['custom_taxrate'] = $order_item[12];
			$oc_vars['modifier_list_id'] = $order_item[13];
			$oc_vars['forced_modifier_group_id'] = $order_item[14];
			$oc_vars['forced_modifiers_price'] = $order_item[15];
			$oc_vars['course'] = $order_item[16];
			$oc_vars['open_item'] = $order_item[17];
			$oc_vars['subtotal'] = ($order_item[1] * $order_item[2]);
			$oc_vars['allow_deposit'] = $order_item[18];
			$oc_vars['deposit_info'] = $order_item[19];
			$keys = array_keys($oc_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}	

			$save_order_item = lavu_query("INSERT INTO `order_contents` ($q_fields) VALUES ($q_values)", $oc_vars);
		}
		
		echo "1";
		exit;

	} else if ($mode == 18) { // clock server in - deprecated - not in use by app - noted 2/22/2011

	} else if ($mode == 19) { // clock server out - deprecated - not in use by app - noted 2/22/2011
	
	} else if ($mode == 20) { // reopen closed order

		$auth_by = convertPIN($_REQUEST['PINused'], "name");
		$auth_by_id = convertPIN($_REQUEST['PINused']);

		$get_original_close_time = lavu_query("SELECT `closed` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		$extract = mysqli_fetch_assoc($get_original_close_time);

		$update_order = lavu_query("UPDATE `orders` SET `closed` = '0000-00-00 00:00:00', `tablename` = '[1]', `reopened_datetime` = '[2]' WHERE `location_id` = '[3]' AND `order_id` = '[4]'", $_REQUEST['set_tablename'], $device_time, $_REQUEST['loc_id'], $_REQUEST['order_id']);
	
		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = "Order Reopened";
		$log_vars['loc_id'] = $_REQUEST['loc_id'];
		$log_vars['order_id'] = $_REQUEST['order_id'];
		$log_vars['time'] = $device_time;
		$log_vars['user'] = $auth_by;
		$log_vars['user_id'] = $auth_by_id;
		$log_vars['details'] = "Original close: ".$extract['closed'];
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}	
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		
		echo '{"json_status":"success"}';
		exit;
	
	} else if ($mode == 21) { // open cash drawer and log it, unless it is called from checkout with no receipt or when the cash button is pressed

		if (!isset($_REQUEST['do_not_log'])) {

			$auth_by = convertPIN($_REQUEST['PINused'], "name");
			$auth_by_id = convertPIN($_REQUEST['PINused']);

			$q_fields = "";
			$q_values = "";
			$log_vars = array();
			$log_vars['action'] = "Drawer Opened";
			$log_vars['loc_id'] = $_REQUEST['loc_id'];
			$log_vars['order_id'] = (isset($_REQUEST['order_id']))?$_REQUEST['order_id']:"0";
			$log_vars['time'] = $device_time;
			$log_vars['user'] = $auth_by;
			$log_vars['user_id'] = $auth_by_id;
			$log_vars['device_udid'] = (isset($_REQUEST['UDID']))?$_REQUEST['UDID']:"";
			if (isset($_REQUEST['at_checkout'])) {
				$log_vars['details'] = "At checkout";
			}
			$keys = array_keys($log_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		}
		
		$use_direct_printing = (isset($_REQUEST['use_direct_printing']))?$_REQUEST['use_direct_printing']:"0";
		
		if ($use_direct_printing != "1") {
			$dd = "";
			if ($_REQUEST['dev_dir'] != "") {
				$dd = "/..";
			}
			$hloc = "..".$dd."/print/index.php?location=".str_replace(" ", "%20", $_REQUEST['location'])."&print=".urlencode("drawer#".$_REQUEST['register'].":open");
			//mail("corey@meyerwind.com","print",$hloc,"From:corey@poslavu.com");
			header("Location: $hloc");
			//header("Location: ..".$dd."/print/index.php?location=".str_replace(" ", "%20", $_REQUEST['location'])."&print=drawer:open");
		}
		exit;

	} else if ($mode == 22) { // log change to order after check has printed
	
		$change_info = explode("|*|", $_REQUEST['change_info']);
				
		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = $change_info[0];
		$log_vars['loc_id'] = $_REQUEST['loc_id'];
		$log_vars['order_id'] = $change_info[1];
		$log_vars['time'] = $change_info[2];
		$log_vars['user'] = $change_info[3];
		$log_vars['user_id'] = $change_info[4];
		$log_vars['details'] = $change_info[5];
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo '"json_status":"success"';
		exit;

	} else if ($mode == 23) { // retrieve multiple check details

		$check_details_array = array();

		$get_check_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' ORDER BY `check` ASC", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (@mysqli_num_rows($get_check_details) > 0) {
			while ($data_obj = mysqli_fetch_object($get_check_details)) {
				$check_details_array[] = $data_obj;
			}

		} else {
			echo '{"json_status":"nr"}';
			exit;
		}
		
		echo '{"json_status":"success","check_details":'.lavu_json_encode($check_details_array).'}';
		exit;

	} else if ($mode == 24) { // save multiple check details during close, deprecated in v1.7.1 and moved to mode 10
		
	} else if ($mode == 25) { // save order in quick serve mode, returns order id (this additional mode may prove unnecessary) ***** is not currently being called by app RJG 11-01-2010 - kept alive for older versions of app ********* 
	
	} else if ($mode == 26) { // reload settings
					
		$get_user_info = lavu_query("SELECT `quick_serve`, `access_level`, `PIN`, `f_name`, `l_name` FROM `users` WHERE `id` = '[1]'", $_REQUEST['server_id']);
		if (@mysqli_num_rows($get_user_info) > 0) {
			$this_user_info = mysqli_fetch_assoc($get_user_info);
		} else {
			echo '{"json_status":"NoU"}';
			exit;
		}

		$valid_PINs = "";
		$get_valid_PINs = lavu_query("SELECT `PIN` FROM `users` WHERE `access_level` > '0'");
		while ($extract2 = mysqli_fetch_assoc($get_valid_PINs)) {
			$valid_PINs = $valid_PINs."|".$extract2['PIN']."|";
		}
		
		$valid_admin_PINs = "";
		$get_valid_admin_PINs = lavu_query("SELECT `PIN` FROM `users` WHERE `access_level` > '1'");
		while ($extract2 = mysqli_fetch_assoc($get_valid_admin_PINs)) {
			$valid_admin_PINs = $valid_admin_PINs."|".$extract2['PIN']."|";
		}

		$user_data_array = array();
		$get_all_user_data = lavu_query("SELECT * FROM `users` ORDER BY `l_name`, `f_name` ASC");
		while ($data_obj = mysqli_fetch_object($get_all_user_data)) {
			$user_data_array[] = $data_obj;
		}

		$locations_array = array();
		$exclude = array("integration1", "integration2", "integration3", "integration4", "api_key", "api_token");
		$get_locations = lavu_query("SELECT ".create_field_list("locations", $exclude)." FROM `locations` WHERE `id` = '".$_REQUEST['loc_id']."'");
		if (@mysqli_num_rows($get_locations) > 0) {
			while ($data_obj = mysqli_fetch_object($get_locations)) {
				$locations_array[] = $data_obj;
			}
			$menu_id = $location_info['menu_id'];
		} else {
			echo '{"json_status":"NoL"}';
			exit;
		}

		$table_setup_array = array();
		$get_table_setup = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '[1]' ORDER BY `_order`, `title` ASC", $_REQUEST['loc_id']);
		if (@mysqli_num_rows($get_table_setup) > 0) {
			while ($data_obj = mysqli_fetch_object($get_table_setup)) {
				$table_setup_array[] = $data_obj;
			}
		} else {
			echo '{"json_status":"NoTS"}';
			exit;
		}
		$table_setup_string = lavu_json_encode($table_setup_array);
		
		$discount_types_array = array();
		$get_discount_types = lavu_query("SELECT * FROM `discount_types` WHERE `loc_id` = '[1]' AND `_deleted` != '1' ORDER BY `title` ASC", $_REQUEST['loc_id']);
		while ($data_obj = mysqli_fetch_object($get_discount_types)) {
			$discount_types_array[] = $data_obj;
		}

		$reports_list_return_string = '"reports_list":[{"title":"Register Summary","mode":"register","filename":"till_report.php"},{"title":"Server Summary","mode":"server","filename":"till_report.php"}]';
		$printers_return_string = getSupportedPrinters($_REQUEST['loc_id']);
		$all_menu_data_return_string = loadMenuData($menu_id, $_REQUEST['loc_id']);

		// load components if necessary
		$component_data_return_string = "";				
		$comp_pack = (isset($_REQUEST['comp_pack']))?$_REQUEST['comp_pack']:"0";
		if ($comp_pack != "0") {
			$component_data_return_string = loadComponents($comp_pack);
		}

		$update_last_activity = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `data_name` = '[1]'", $company_info['data_name']);
		$update_needs_reload = mlavu_query("UPDATE `poslavu_MAIN_db`.`client_devices` SET `needs_reload` = '0' WHERE `UDID` = '[1]'", $_REQUEST['UDID']);
		
		$tack_on_build = ":".$_REQUEST['build'];

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = "Settings reloaded";
		$log_vars['loc_id'] = $_REQUEST['loc_id'];
		$log_vars['order_id'] = "0";
		$log_vars['time'] = $device_time;
		$log_vars['user'] = $this_user_info['f_name']." ".$this_user_info['l_name'];
		$log_vars['user_id'] = $this_user_info['id'];
		$log_vars['device_udid'] = $_REQUEST['UDID'];
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);
	
		echo '{"json_status":"success","company_code":"'.lsecurity_name($company_info['company_code'], $company_info['id']).$tack_on_build.'","logo_img":"'.$company_info['logo_img'].'","receipt_logo_img":"'.$company_info['receipt_logo_img'].'","demo":"'.$company_info['demo'].'","company_id":"'.$company_info['id'].'","company_name":"'.$company_info['company_name'].'","valid_PINs":"'.$valid_PINs.'","valid_admin_PINs":"'.$valid_admin_PINs.'","location":'.lavu_json_encode($locations_array).',"all_user_data":'.lavu_json_encode($user_data_array).',"quick_serve":"'.$this_user_info['quick_serve'].'","access_level":"'.$this_user_info['access_level'].'","PIN":"'.$this_user_info['PIN'].'","f_name":"'.$this_user_info['f_name'].'","l_name":"'.$this_user_info['l_name'].'","table_setup":'.$table_setup_string.',"discount_types":'.lavu_json_encode($discount_types_array).','.$reports_list_return_string.','.$printers_return_string.','.$all_menu_data_return_string.$component_data_return_string.',"available_language_packs":'.getAvailableLanguagePacks().',"language_pack":'.generateLanguagePack($_REQUEST['loc_id'], $location_info['use_language_pack']).'}';
		exit;
	
	} else if ($mode == 27) { // reload location settings (net_path, use_net_path, net_path_print) only
		
		$locations_array = array();
		
		$get_net_paths = lavu_query("SELECT `net_path`, `use_net_path`, `net_path_print` FROM `locations` WHERE `id` = '[1]' LIMIT 1", $_REQUEST['loc_id']);
		if (@mysqli_num_rows($get_net_paths) > 0) {
			$extract = mysqli_fetch_assoc($get_net_paths);
	
			echo '{"json_status":"success","net_path":"'.$extract['net_path'].'","use_net_path":"'.$extract['use_net_path'].'","net_path_print":"'.$extract['net_path_print'].'"}';
			exit;
			
		} else {

			echo '{"json_status":"NoL"}';
			exit;
		}

	} else if ($mode == 28) { // save order (info, contents, and modifier usage), combines modes 5 and 17, should replace them altogether in the future, returns order id
	
		// ***** order_info indexes *****
		//
		//  0 - order id
		//  1 - opened
		//  2 - closed
		//  3 - subtotal
		//  4 - taxrate
		//  5 - tax
		//  6 - total
		//  7 - server
		//  8 - tablename
		//  9 - send_status
		// 10 - guests
		// 11 - server id
		// 12 - number of checks
		// 13 - multiple tax rates
		// 14 - to tab or not to tab
		// 15 - deposit status
		// 16 - register
		// 17 - register name
		
		//ini_set('display_errors',1);
		//error_reporting(E_ALL|E_STRICT);

		$order_info = explode("|*|", $_POST['order_info']);
		$order_id = "0";
		
		$q_fields = "";
		$q_values = "";
		$q_update = "";
		$o_vars = array();
		$o_vars['opened'] = $order_info[1];
		$o_vars['server'] = $order_info[7];
		$o_vars['server_id'] = $order_info[11];
		$o_vars['tablename'] = $order_info[8];
		$o_vars['subtotal'] = $order_info[3];
		$o_vars['tax'] = $order_info[5];
		$o_vars['taxrate'] = $order_info[4];
		$o_vars['total'] = $order_info[6];
		$o_vars['send_status'] = $order_info[9];
		$o_vars['guests'] = $order_info[10];
		$o_vars['no_of_checks'] = $order_info[12];
		$o_vars['multiple_tax_rates'] = $order_info[13];
		$o_vars['tab'] = $order_info[14];
		$o_vars['deposit_status'] = $order_info[15];
		$o_vars['register'] = $order_info[16];
		$o_vars['register_name'] = $order_info[17];
		$keys = array_keys($o_vars);
		$q_update = "";
		foreach ($keys as $key) {
			if ($q_update != "") { $q_update .= ", "; }
			$q_update .= "`$key` = '[$key]'";
		}
		$o_vars['location'] = $location_info['title'];
		$o_vars['location_id'] = $_POST['loc_id'];
		$o_vars['closed'] = $order_info[2];
		$o_vars['closed'] = $order_info[2];
	
		if ($order_info[0] == "0") {

			$check_for_previous_save = lavu_query("SELECT `order_id` FROM `orders` WHERE `location_id` = '[1]' AND `opened` = '[2]' AND `server` = '[3]' AND `tablename` = '[4]' LIMIT 1", $_POST['loc_id'], $order_info[1], $order_info[7], $order_info[8]);
			if (mysqli_num_rows($check_for_previous_save) > 0) {
				$extract = mysqli_fetch_assoc($check_for_previous_save);
				$order_id = $extract['order_id'];
				$update_order = lavu_query("UPDATE `orders` SET `void` = '0', $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '$order_id'", $o_vars);
			} else {
				//$check_for_zeroed_open = lavu_query("SELECT * FROM `orders` WHERE (`opened` = '0000-00-00 00:00:00' OR `opened` = '') AND `location_id` = '[1]' ORDER BY `id` ASC LIMIT 1", $_REQUEST['loc_id']);
				//if (@mysqli_num_rows($check_for_zeroed_open) > 0) {
					//$extract = mysqli_fetch_assoc($check_for_zeroed_open);
					//$order_id = $extract['order_id'];
					//$update_order = lavu_query("UPDATE `orders` SET `void` = '0', $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '$order_id'", $o_vars);
				//} else {
					//$o_vars['order_id'] = $order_id;
					$o_vars['opening_device'] = (isset($_REQUEST['UDID']))?$_REQUEST['UDID']:"";
					$keys = array_keys($o_vars);
					foreach ($keys as $key) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
					$order_id = assignNextWithPrefix("order_id", "orders", "location_id", $_POST['loc_id'], $server_order_prefix."-");
					$save_new_order = lavu_query("INSERT INTO `orders` ($q_fields, `order_id`) VALUES ($q_values, '".$order_id."')", $o_vars);
				//}
			}

		} else {
		
			$o_vars['order_id'] = $order_info[0];
			$update_order = lavu_query("UPDATE `orders` SET `void` = '0', $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);
			$order_id = $order_info[0];
		}

		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = "Order Saved";
		$l_vars['loc_id'] = $_POST['loc_id'];
		$l_vars['order_id'] = $order_id;
		$l_vars['time'] = $order_info[1];
		$l_vars['user'] = $order_info[7];
		$l_vars['user_id'] = $order_info[11];
		$l_vars['device_udid'] = (isset($_REQUEST['UDID']))?$_REQUEST['UDID']:"";
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
				
		$update_last_activity = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `data_name` = '[1]'", $company_info['data_name']);

		// ***** order_item indexes *****
		//
		//  0 - item
		//  1 - price
		//  2 - quantity
		//  3 - options
		//  4 - special
		//  5 - print
		//  6 - modify price
		//  7 - check number
		//  8 - seat number
		//  9 - item id
		// 10 - printer
		// 11 - apply taxrate
		// 12 - custom taxrate
		// 13 - modifier list id
		// 14 - forced modifier group id
		// 15 - forced modifiers price
		// 16 - course number
		// 17 - open item
		// 18 - allow deposit
		// 19 - deposit info
		// 20 - void flag
		// 21 - device time
		// 22 - notes
		// 23 - hidden data 1
		// 24 - hidden data 2
		// 25 - hidden data 3
		// 26 - hidden data 4
		// 27 - tax inclusion
		// 28 - sent

		$clear_current_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_POST['loc_id'], $order_id);

		$order_contents = explode("|*|", $_POST['order_contents']);

		foreach ($order_contents as $oc) {
		
			$order_item = explode("|o|", $oc);
			$q_fields = "";
			$q_values = "";
			$i_vars = array();
			$i_vars['loc_id'] = $_POST['loc_id'];
			$i_vars['order_id'] = $order_id;
			$i_vars['item'] = $order_item[0];
			$i_vars['price'] = $order_item[1];
			$i_vars['quantity'] = $order_item[2];
			$i_vars['options'] = $order_item[3];
			$i_vars['special'] = $order_item[4];
			$i_vars['modify_price'] = $order_item[6];
			$i_vars['print'] = $order_item[5];
			$i_vars['check'] = $order_item[7];
			$i_vars['seat'] = $order_item[8];
			$i_vars['item_id'] = $order_item[9];
			$i_vars['printer'] = $order_item[10];
			$i_vars['apply_taxrate'] = $order_item[11];
			$i_vars['custom_taxrate'] = $order_item[12];
			$i_vars['modifier_list_id'] = $order_item[13];
			$i_vars['forced_modifier_group_id'] = $order_item[14];
			$i_vars['forced_modifiers_price'] = $order_item[15];
			$i_vars['course'] = $order_item[16];
			$i_vars['open_item'] = $order_item[17];
			$i_vars['subtotal'] = ($order_item[1] * $order_item[2]);
			$i_vars['allow_deposit'] = $order_item[18];
			$i_vars['deposit_info'] = $order_item[19];
			$i_vars['subtotal_with_mods'] = ($order_item[2] * ($order_item[1] + $order_item[6] + $order_item[15]));
			$i_vars['void'] = $order_item[20];
			$i_vars['device_time'] = $order_item[21];
			$i_vars['notes'] = $order_item[22];
			$i_vars['hidden_data1'] = $order_item[23];
			$i_vars['hidden_data2'] = $order_item[24];
			$i_vars['hidden_data3'] = $order_item[25];
			$i_vars['hidden_data4'] = $order_item[26];
			$i_vars['tax_inclusion'] = $order_item[27];
			$i_vars['sent'] = $order_item[28];
			$keys = array_keys($i_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$save_order_item = lavu_query("INSERT INTO `order_contents` ($q_fields, `server_time`) VALUES ($q_values, now())", $i_vars);
		}
		
		// ***** modifier indexes *****
		//
		//  0 - mod id
		//  1 - type
		//  2 - content row
		//  3 - quantity
		//  4 - unit cost
		
		$clear_current_contents = lavu_query("DELETE FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_POST['loc_id'], $order_id);

		if ($_POST['modifiers_used'] != "") {

			$order_modifiers = explode("|*|", $_POST['modifiers_used']);
	
			foreach ($order_modifiers as $om) {
			
				$modifier_info = explode("|o|", $om);
				$q_fields = "";
				$q_values = "";
				$m_vars = array();
				$m_vars['loc_id'] = $_POST['loc_id'];
				$m_vars['order_id'] = $order_id;
				$m_vars['mod_id'] = $modifier_info[0];
				$m_vars['type'] = $modifier_info[1];
				$m_vars['row'] = $modifier_info[2];
				$m_vars['qty'] = $modifier_info[3];
	
				if (isset($modifier_info[4])) {
					$unit_cost = $modifier_info[4];
				} else {
					$unit_cost = getModifierUnitCost($modifier_info[0], $modifier_info[1]);
				}
				$m_vars['cost'] = ($unit_cost * $modifier_info[3]);
				$m_vars['unit_cost'] = $unit_cost;
	
				$keys = array_keys($m_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$save_modifier_info = lavu_query("INSERT INTO `modifiers_used` ($q_fields) VALUES ($q_values)", $m_vars);
			}
		}
		
		echo "1:".$order_id;
		exit;

	} else if ($mode == 29) { // quick query to create a blank order and return the order_id
	
		$new_id = assignNextWithPrefix("order_id", "orders", "location_id", $_REQUEST['loc_id'], $server_order_prefix);
		$create_order = lavu_query("INSERT INTO `orders` (`order_id`, `location`, `location_id`, `register`) VALUES ('[1]', '[2]', '[3]', '[4]')", $new_id, $location_info['title'], $_REQUEST['loc_id'], $_REQUEST['register']);		
		echo $new_id;
		exit;

	} else if ($mode == 30) { // load orders with open deposits

		$order_info = array();
		$order_contents = array();
		$check_details = array();

		$limit_string = "";
		if (isset($_REQUEST['page'])) {
			$limit_string = " LIMIT ".(((int)$_REQUEST['page'] - 1) * 20).", 20";
		}

		$get_count = lavu_query("SELECT COUNT(`id`) FROM `orders` WHERE `location_id` = '[1]' AND `deposit_status` = '2' AND `closed` = '0000-00-00 00:00:00' AND `opened` != '0000-00-00 00:00:00' AND `void` != '1'", $_REQUEST['loc_id']);
		$total_count = mysqli_result($get_count, 0);

		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '[1]' AND `deposit_status` = '2' AND `closed` = '0000-00-00 00:00:00' AND `void` != '1' ORDER BY `opened` ASC".$limit_string, $_REQUEST['loc_id']);
		if (@mysqli_num_rows($get_orders) > 0) {
			while ($data_obj = mysqli_fetch_object($get_orders)) {
				$order_info[] = $data_obj;
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' ORDER BY `id` ASC", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}				
				$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."'", $_REQUEST['loc_id']);
				while ($data_obj3 = mysqli_fetch_object($get_details)) {
					$check_details[] = $data_obj3;
				}
			}

		} else {
			echo '{"json_status":"NoMatch"}';
			exit;
		}
		
		echo '{"json_status":"success","order_info":'.lavu_json_encode($order_info).',"order_contents":'.lavu_json_encode($order_contents).',"check_details":'.lavu_json_encode($check_details).',"total_count":"'.$total_count.'"}';
		exit;

	} else if ($mode == 31) { // close open deposit as unpaid

		$get_items = lavu_query("SELECT `id`, `deposit_info`  FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `allow_deposit` != '0'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (@mysqli_num_rows($get_items) > 0) {
			while ($extract = mysqli_fetch_assoc($get_items)) {
				$update_item = lavu_query("UPDATE `order_contents` SET `deposit_info` = '[1]' WHERE `id` = '[2]'", $extract['deposit_info']."\nCLOSED AS UNPAID on ".$_REQUEST['now']." by ".$_REQUEST['server'], $extract['id']);
			}
			$update_deposit_status = lavu_query("UPDATE `orders` SET `deposit_status` = '5' WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		
			echo "1";
			exit;
			
		} else {
		
			echo "0";
			exit;
		}
		
	} else if ($mode == 32) { // load credit card transaction details and recorded payments and refunds

		$all_transactions_array = array();

		$get_cc_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' AND `voided` = '0' AND `action` != 'Void' AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC");
		if (@mysqli_num_rows($get_cc_transactions) > 0) {
			while ($data_obj = mysqli_fetch_object($get_cc_transactions)) {
				$all_transactions_array[] = $data_obj;
			}
		}
	
		echo '{"json_status":"success","cc_transactions":'.lavu_json_encode($all_transactions_array).'}';
		exit;

	} else if ($mode == 33) { // save credit card transaction details - deprecated - this now occurs in the gateway functions script
	
	} else if ($mode == 34) { // record refund, deprecated - refunds recorded as transactions with details included
	
	} else if ($mode == 35) { // retrieve item by UPC, change quantity for qmode = inventoryQuantities
	
		$get_item = lavu_query("SELECT * FROM `menu_items` WHERE `UPC` = '[1]' LIMIT 1", $_REQUEST['UPC']);
		if (@mysqli_num_rows($get_item) > 0) {
		
			if ($_REQUEST['smode'] == "order") {
			
				$data_obj = mysqli_fetch_object($get_item);
				echo '{"json_status":"add_to_order","item_info":'.lavu_json_encode($data_obj).'}';
				
			} else if ($_REQUEST['smode'] == "inventoryItems") {
			
				$extract = mysqli_fetch_assoc($get_item);
				echo '{"json_status":"item_info","item_id":"'.$extract['id'].'","item_name":"'.$extract['name'].'","item_price":"'.$extract['price'].'","description":"'.$extract['description'].'","count":"'.$extract['inv_count'].'"}';
			
			} else if ($_REQUEST['smode'] == "inventoryQuantities") {
			
				$extract = mysqli_fetch_assoc($get_item);
				
				$quantity_adj = (isset($_REQUEST['q_adj']))?$_REQUEST['q_adj']:"1";
				if ($_REQUEST['qmode'] == "minus") {
					$new_quantity = ($extract['inv_count'] - $quantity_adj);
				} else {
					$new_quantity = ($extract['inv_count'] + $quantity_adj);
				}
				
				$update_item = lavu_query("UPDATE `menu_items` SET `inv_count` = '[1]', last_modified_date = now() WHERE `id` = '[2]'", $new_quantity, $extract['id']);
	
				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `time`, `user`, `user_id`, `server_time`, `item_id`) VALUES ('Inventory Adjustment: ".$_REQUEST['qmode']." ".$quantity_adj."', '".$_REQUEST['loc_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now(), '".$extract['id']."')");

				echo '{"json_status":"inventory_change","item_name":"'.$extract['name'].'","old_quantity":"'.$extract['inv_count'].'","new_quantity":"'.$new_quantity.'"}';			
			}
			
		} else {

			if ($_REQUEST['smode'] == "inventoryItems") {
				echo '{"json_status":"item_info","item_id":"0","item_name":"NEW ITEM","item_price":"","description":"","count":"0"}';			
			} else {
				echo '{"json_status":"not_found"}';
			}
		}

		exit();

	} else if ($mode == 36) { // clear order and contents, called after adding contents to existing tab
	
		$clear_order = lavu_query("UPDATE `orders` SET `opened` = '0000-00-00 00:00:00' WHERE `order_id` = '[1]'", $_REQUEST['order_id']);
		$clear_contents = lavu_query("DELETE FROM `order_contents` WHERE `order_id` = '[1]'", $_REQUEST['order_id']);
		echo "1";
		exit();

	} else if ($mode == 37) { // save or create item from barcode scan
	
		if ($_REQUEST['iid'] == "0") {
			deBug("INSERT INTO `menu_items` (`category_id`, `menu_id`, `name`, `price`, `description`, `inv_count`, `UPC`) VALUES ('112', '11', '".str_replace("'", "''", $_REQUEST['iname'])."', '".number_format((float)$_REQUEST['iprice'], $decimal_places, ".", "")."', '".str_replace("'", "''", $_REQUEST['idesc'])."', '".(int)$_REQUEST['icount']."', '".$_REQUEST['UPC']."')");
			$create_item = lavu_query("INSERT INTO `menu_items` (`category_id`, `menu_id`, `name`, `price`, `description`, `inv_count`, `UPC`) VALUES ('112', '11', '".str_replace("'", "''", $_REQUEST['iname'])."', '".number_format((float)$_REQUEST['iprice'], $decimal_places, ".", "")."', '".str_replace("'", "''", $_REQUEST['idesc'])."', '".(int)$_REQUEST['icount']."', '".$_REQUEST['UPC']."')");
			$new_id = ConnectionHub::getConn("rest")->InsertID();
			echo '{"json_status":"item_created","new_id":"'.$new_id.'"}';
		} else {
			deBug("UPDATE `menu_items` SET `name` = '".str_replace("'", "''", $_REQUEST['iname'])."', `price` = '".number_format((float)$_REQUEST['iprice'], $decimal_places, ".", "")."', `description` = '".str_replace("'", "''", $_REQUEST['idesc'])."', `inv_count` = '".(int)$_REQUEST['icount']."' WHERE `id` = '".$_REQUEST['iid']."'");
			$update_item = lavu_query("UPDATE `menu_items` SET `name` = '".str_replace("'", "''", $_REQUEST['iname'])."', `price` = '".number_format((float)$_REQUEST['iprice'], $decimal_places, ".", "")."', `description` = '".str_replace("'", "''", $_REQUEST['idesc'])."', `inv_count` = '".(int)$_REQUEST['icount']."' WHERE `id` = '".$_REQUEST['iid']."'");
			echo '{"json_status":"item_saved"}';
		}
		
		exit();

	} else if ($mode == 38) { // nullify needs_reload field
	
		mlavu_query("UPDATE `poslavu_MAIN_db`.`client_devices` SET `needs_reload` = '0' WHERE `UDID` = '".$_REQUEST['UDID']."'");
		echo "1";
		exit();
		
	} else if ($mode == 39) { // clear payments and refunds

	} else if ($mode == 40) { // a new, better way of recording payments and discounts
	
		// ***** check_info indexes *****
		//
		//  0 - order id
		//  1 - discount amount
		//  2 - discount label
		//  3 - gratuity amount
		//  4 - guests
		//  5 - cash amount
		//  6 - card amount
		//  7 - gift certificate amount
		//  8 - check count
		//  9 - active check
		// 10 - checked out flag 
		// 11 - refund gift certificate amount
		// 12 - subtotal
		// 13 - tax amount
		// 14 - check total
		// 15 - refund amount
		// 16 - remaining amount
		// 17 - discount type id
		// 18 - discount value
		// 19 - gratuity percent
		// 20 - register
		// 21 - discount type
		// 22 - datetime
		// 23 - refund_cc amount
		// 24 - server name
		// 25 - server id
		// 26 - reopened datetime
		// 27 - rounded amount
		// 28 - register name

		$check_info = explode("|*|", $_REQUEST['check_info']);
		
		// ***** payments_and_refunds_array indexes *****
		//
		//  0 - order id
		//  1 - check
		//  2 - amount
		//  3 - total collected
		//  4 - pay type
		//  5 - datetime
		//  6 - action
		//  7 - refund notes
		//  8 - server name
		//  9 - server id
		// 10 - register
		// 11 - change
		// 12 - got response
		// 13 - row id
		// 14 - register name
		// 15 - pay type id
			
		$payments_and_refunds_array = explode("|*|", $_REQUEST['pnr_details']);
		
		$check_has_printed = (isset($_REQUEST['check_has_printed']))?$_REQUEST['check_has_printed']:0;
		
		$row_ids = array();
		$last_payment = false;
		foreach ($payments_and_refunds_array as $payment_or_refund) {
			if ($payment_or_refund != "") {
				$detail_array = explode("|o|", $payment_or_refund);
				$q_fields = "";
				$q_values = "";
				$q_update = "";
				$pnr_vars = array();
				$pnr_vars['check'] = $detail_array[1];
				$pnr_vars['refund_notes'] = $detail_array[7];
				$keys = array_keys($pnr_vars);
				foreach ($keys as $key) {
					if ($q_update != "") { $q_update .= ", "; }
					$q_update .= "`$key` = '[$key]'";
				}
				$pnr_vars['loc_id'] = $_REQUEST['loc_id'];
				$pnr_vars['order_id'] = $detail_array[0];
				$pnr_vars['register'] = $check_info[20];
				$pnr_vars['register_name'] = $check_info[28];
				$pnr_vars['amount'] = $detail_array[2];
				$pnr_vars['total_collected'] = $detail_array[3];
				$pnr_vars['change'] = $detail_array[11];
				$pnr_vars['pay_type'] = $detail_array[4];
				$pnr_vars['pay_type_id'] = $detail_array[15];
				$pnr_vars['datetime'] = $detail_array[5];
				$pnr_vars['action'] = $detail_array[6];
				$pnr_vars['server_name'] = $detail_array[8];
				$pnr_vars['server_id'] = $detail_array[9];
				$keys = array_keys($pnr_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$pnr_vars['id'] = $detail_array[13];
	
				if ($pnr_vars['id'] == 0) {
					$record_detail = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $pnr_vars);
					$row_ids[$pr_index] = lavu_insert_id();
				} else {
					$update_detail = lavu_query("UPDATE `cc_transactions` SET $q_update WHERE `id` = '[id]'", $pnr_vars);
					$row_ids[$pr_index] = $pnr_vars['id'];
				}
				
				if ($detail_array[6] == "Sale") {
					$paid_total += $detail_array[$amount_key];
				} else {
					$paid_total -= $detail_array[$amount_key];
				}
			}
		}
		
		update_order_item_details($_REQUEST['item_details'], $check_info[0], $_REQUEST['loc_id']);
		
		if (isset($_REQUEST['alternate_payments'])) {
			if ($_REQUEST['alternate_payments'] != "") {
				saveAlternatePaymentTotals($_REQUEST['loc_id'], $check_info[0], $check_info[9], $_REQUEST['alternate_payments']);
			}
		}
		
		if ($check_info[8] == 1) {
		
			$get_totals_from_contents = lavu_query("SELECT SUM(`subtotal_with_mods`) AS `subtotal`, SUM(`discount_amount`) AS `discount`, SUM(`tax_amount`) AS `tax`, SUM(`total_with_tax`) AS `total` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $check_info[0]);
			$total_data = mysqli_fetch_assoc($get_totals_from_contents);
			
			$q_update = "";
			$order_vars = array();
			$order_vars['subtotal'] = $total_data['subtotal'];
			$order_vars['tax'] = $total_data['tax'];
			$order_vars['total'] = $total_data['total'];
			$order_vars['refunded'] = $check_info[15];
			$order_vars['refunded_cc'] = $check_info[23];
			$order_vars['refunded_gc'] = $check_info[11];
			$order_vars['discount'] = setCorrectSum($check_info[1], $total_data['discount'], $smallest_money);
			$order_vars['discount_sh'] = $check_info[2];
			$order_vars['discount_value'] = $check_info[18];
			$order_vars['discount_type'] = $check_info[21];
			$order_vars['discount_id'] = $check_info[17];
			$order_vars['gratuity'] = $check_info[3];
			$order_vars['rounding_amount'] = $check_info[27];
			$order_vars['cash_paid'] = ($check_info[5] - $check_info[17]);
			$order_vars['cash_applied'] = ($check_info[5] - $check_info[17]);
			if ($location_info['integrateCC'] != "1") {
				$order_vars['card_paid'] = $check_info[6];
			}
			$order_vars['gift_certificate'] = $check_info[7];
			$order_vars['guests'] = $check_info[4];
			$order_vars['check_has_printed'] = $check_has_printed;
			$keys = array_keys($order_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$order_vars['location_id'] = $_REQUEST['loc_id'];
			$order_vars['order_id'] = $check_info[0];

			$update_order = lavu_query("UPDATE `orders` SET $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $order_vars);

			echo '{"json_status":"success","pnr_row_ids":"'.join("|", $row_ids).'"}';
			exit;

		} else {
			
			$get_totals_from_contents = lavu_query("SELECT SUM(`subtotal_with_mods`) AS `subtotal`, SUM(`discount_amount`) AS `discount`, SUM(`tax_amount`) AS `tax`, SUM(`total_with_tax`) AS `total` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $_REQUEST['loc_id'], $check_info[0], $check_info[9]);
			$total_data = mysqli_fetch_assoc($get_totals_from_contents);

			$q_update = "";
			$q_fields = "";
			$q_values = "";
			$detail_vars = array();
			$detail_vars['subtotal'] = setCorrectSum($check_info[12], $total_data['subtotal'], $smallest_money);
			$detail_vars['tax'] = setCorrectSum($check_info[13], $total_data['tax'], $smallest_money);
			$detail_vars['check_total'] = setCorrectSum($check_info[14], $total_data['total'], $smallest_money);
			$detail_vars['discount'] = setCorrectSum($check_info[1], $total_data['discount'], $smallest_money);
			$detail_vars['discount_sh'] = $check_info[2];
			$detail_vars['discount_value'] = $check_info[18];
			$detail_vars['discount_type'] = $check_info[21];
			$detail_vars['discount_id'] = $check_info[17];
			$detail_vars['gratuity'] = $check_info[3];
			$detail_vars['gratuity_percent'] = $check_info[19];
			$detail_vars['cash'] = ($check_info[5] - $check_info[17]);
			if ($location_info['integrateCC'] != "1") {
				$detail_vars['card'] = $check_info[6];
			}
			$detail_vars['gift_certificate'] = $check_info[7];
			$detail_vars['card_desc'] = $check_info[10];
			$detail_vars['transaction_id'] = $check_info[10];
			$detail_vars['rounding_amount'] = $check_info[27];
			$detail_vars['refund'] = $check_info[15];
			$detail_vars['refund_cc'] = $check_info[23];
			$detail_vars['refund_gc'] = $check_info[11];
			$detail_vars['remaining'] = $check_info[16];
			$detail_vars['change'] = 0;
			$detail_vars['cash_applied'] = ($check_info[5] - $check_info[17]);
			$detail_vars['checked_out'] = $check_info[10];
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$detail_vars['loc_id'] = $_REQUEST['loc_id'];
			$detail_vars['order_id'] = $check_info[0];
			$detail_vars['check'] = $check_info[9];
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
	
			$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' LIMIT 1", $detail_vars);
			if (@mysqli_num_rows($check_for_details) > 0) {
				$update_details = lavu_query("UPDATE `split_check_details` SET $q_update WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $detail_vars);
			} else {
				$create_details = lavu_query("INSERT INTO `split_check_details` ($q_fields) VALUES ($q_values)", $detail_vars);
			}

			$discount = 0;
			$gratuity = 0;
			$cash = 0;
			$card = 0;
			$gift_certificate = 0;
			$refund = 0;
			$refund_cc = 0;
			$refund_gc = 0;
			$change = 0;
			$cash_applied = 0;
			$rounded_amount = 0;

			$get_detail_totals = lavu_query("SELECT `discount`, `gratuity`, `cash`, `card`, `gift_certificate`, `refund`, `refund_cc`, `cash_applied`, `rounding_amount` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` <= '[check]'", $detail_vars);
			if (@mysqli_num_rows($get_detail_totals) > 0) {
				while ($extract = mysqli_fetch_assoc($get_detail_totals)) {
					$discount += $extract['discount'];
					$gratuity += $extract['gratuity'];
					$cash += $extract['cash'];
					$card += $extract['card'];
					$gift_certificate += $extract['gift_certificate'];
					$refund += $extract['refund'];
					$refund_cc += $extract['refund_cc'];
					$refund_gc += $extract['refund_gc'];
					//$change += $extract['change'];
					$cash_applied += $extract['cash_applied'];
					$rounded_amount += $extract['rounding_amount'];
				}
			}

			$q_update = "";
			$order_vars = array();
			$order_vars['discount'] = number_format($discount, $decimal_places, '.', '');
			$order_vars['gratuity'] = number_format($gratuity, $decimal_places, '.', '');
			$order_vars['cash_paid'] = number_format($cash, $decimal_places, '.', '');
			if ($location_info['integrateCC'] != "1") {
				$order_vars['card_paid'] = number_format($card, $decimal_places, '.', '');
			}
			$order_vars['gift_certificate'] = number_format($gift_certficate, $decimal_places, '.', '');
			$order_vars['refunded'] = number_format($refund, $decimal_places, '.', '');
			$order_vars['refunded_cc'] = number_format($refund_cc, $decimal_places, '.', '');
			$order_vars['refunded_gc'] = number_format($refund_gc, $decimal_places, '.', '');
			$order_vars['change_amount'] = number_format($change, $decimal_places, '.', '');
			$order_vars['cash_applied'] = number_format($cash_applied, $decimal_places, '.', '');
			$order_vars['rounding_amount'] = number_format($rounded_amount, $decimal_places, '.', '');
			$order_vars['guests'] = $check_info[4];
			$keys = array_keys($order_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$order_vars['location_id'] = $_REQUEST['loc_id'];
			$order_vars['order_id'] = $check_info[0];
			
			$update_order = lavu_query("UPDATE `orders` SET $q_update, `check_has_printed` = '$check_has_printed' WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $order_vars);

			echo '{"json_status":"success","pnr_row_ids":"'.join("|", $row_ids).'"}';
			exit();
		}
	
	} else if ($mode == 41) { // log payment reassignment after item movement results in check disappearance, also updates check numbers for integrated card transactions
	
		if ($_REQUEST['pmoved'] != "") {
			$pnrs = explode(",", $_REQUEST['pmoved']);
			foreach ($pnrs as $por) {
				$por_parts = explode(":", $por);
				$update_record = lavu_query("UPDATE `cc_transactions` SET `check` = '[1]' WHERE `id` = '[2]'", $por_parts[4], $por_parts[0]);
				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`, `details`) VALUES ('Payments check reassigned', '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now(), '".$por_parts[1]." ".$por_parts[2]." (".$por_parts[3]." to ".$por_parts[4].")')");				
			}

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		}
		
		if ($_REQUEST['cmoved'] != "") {
			$transactions = explode(",", $_REQUEST['cmoved']);
			foreach ($transactions as $t) {
				$t_parts = explode(":", $t);
				$update_transaction = lavu_query("UPDATE `cc_transactions` SET `check` = '[1]' WHERE `transaction_id` = '[2]' AND `auth_code` = '[3]'", $t_parts[2], $t_parts[0], $t_parts[1]);
				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`, `details`) VALUES ('Payments check reassigned', '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now(), '".$por_parts[1]." ".$por_parts[2]." (".$por_parts[3]." to ".$por_parts[4].")')");
			}

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		}

		echo "1";
		exit();
		
	} else if ($mode == 42) { // save single payment or refund and return new row id
	
		// ***** pnr_details indexes *****
		//
		//  0 - order id
		//  1 - check
		//  2 - amount
		//  3 - total collected
		//  4 - pay type
		//  5 - datetime
		//  6 - action
		//  7 - refund notes
		//  8 - server name
		//  9 - server id
		// 10 - register
		// 11 - change
		// 12 - got response
		// 13 - row id
		// 14 - register name
		// 15 - pay type id
			
		$detail_array = explode("|*|", $_REQUEST['pnr_details']);
		
		$q_fields = "";
		$q_values = "";
		$pnr_vars = array();
		$pnr_vars['loc_id'] = $_REQUEST['loc_id'];
		$pnr_vars['order_id'] = $detail_array[0];
		$pnr_vars['check'] = $detail_array[1];
		$pnr_vars['register'] = $detail_array[10];
		$pnr_vars['register_name'] = $detail_array[14];
		$pnr_vars['amount'] = $detail_array[2];
		$pnr_vars['total_collected'] = $detail_array[3];
		$pnr_vars['change'] = $detail_array[11];
		$pnr_vars['pay_type'] = $detail_array[4];
		$pnr_vars['pay_type_id'] = $detail_array[15];
		$pnr_vars['datetime'] = $detail_array[5];
		$pnr_vars['action'] = $detail_array[6];
		$pnr_vars['refund_notes'] = $detail_array[7];
		$pnr_vars['server_name'] = $detail_array[8];
		$pnr_vars['server_id'] = $detail_array[9];
		$keys = array_keys($pnr_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}

		$record_detail = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $pnr_vars);
		$row_id = lavu_insert_id();
		
		if (($pnr_vars['action'] == "Sale") && ($pnr_vars['pay_type'] == "Cash")) {
			$update_order = " `cash_paid` = (`cash_paid` + [total_collected]), `cash_applied` = (`cash_applied` + [total_collected])";
			$update_check = " `cash` = (`cash` + [total_collected]), `cash_applied` = (`cash_applied` + [total_collected])";
			$c_fields = "`cash`, `cash_applied`";
			$c_values = "'[total_collected]', '[total_collected]'";
		} else if (($pnr_vars['action'] == "Refund") && ($pnr_vars['pay_type'] == "Cash")) {
			$update_order = " `cash_paid` = (`cash_paid` - [amount]), `cash_applied` = (`cash_applied` - [amount]), `refunded` = (`refunded` + [amount])";
			$update_check = " `cash` = (`cash` - [amount]), `cash` = (`cash` - [amount]), `refund` = (`refund` + [amount])";
			$c_fields = "`cash`, `cash_applied`, `refunded`";
			$c_values = "'[amount]', '[amount]', '[amount]'";
		} else if (($pnr_vars['action'] == "Sale") && ($pnr_vars['pay_type'] == "Card")) {
			$update_order = " `card_paid` = (`card_paid` + [amount])";
			$update_check = " `card` = (`card` + [amount])";
			$c_fields = "`card`";
			$c_values = "'[amount]'";
		} else if (($pnr_vars['action'] == "Refund") && ($pnr_vars['pay_type'] == "Card")) {
			$update_order = " `card_paid` = (`card_paid` - [amount]), `refunded_cc` = (`refunded_cc` + [amount])";
			$update_check = " `card` = (`card` - [amount]), `refund_cc` = (`refund_cc` + [amount])";
			$c_fields = "`card`, `refund_cc`";
			$c_values = "'[amount]', '[amount]'";
		} else if (($pnr_vars['action'] == "Sale") && ($pnr_vars['pay_type'] == "Gift Certificate")) {
			$update_order = " `gift_certificate` = (`gift_certificate` + [amount])";
			$update_check = " `gift_certificate` = (`gift_certificate` + [amount])";
			$c_fields = "`gift_certificate`";
			$c_values = "'[amount]'";
		} else if (($pnr_vars['action'] == "Refund") && ($pnr_vars['pay_type'] == "Gift Certificate")) {
			$update_order = " `gift_certificate` = (`gift_certificate` - [amount]), `refunded_gc` = (`refunded_gc` + [amount])";
			$update_check = " `gift_certificate` = (`gift_certificate` - [amount]), `refund_gc` = (`refund_gc` + [amount])";
			$c_fields = "`gift_certificate`, `refund_gc`";
			$c_values = "'[amount]', '[amount]'";
		} else if ($pnr_vars['action'] == "Sale") {
			$update_order = " `alt_paid` = (`alt_paid` + [amount])";
			$update_check = " `alt_paid` = (`alt_paid` + [amount])";
			$c_fields = "`alt_paid`";
			$c_values = "'[amount]'";
		} else if ($pnr_vars['action'] == "Refund") {
			$update_order = " `alt_paid` = (`alt_paid` - [amount]), `alt_refunded` = (`alt_refunded` + [amount])";
			$update_check = " `alt_paid` = (`alt_paid` - [amount]), `alt_refunded` = (`alt_refunded` + [amount])";
			$c_fields = "`alt_paid`, `alt_refunded`";
			$c_values = "'[amount]', '[amount]'";
		}

		$update_order = lavu_query("UPDATE `orders` SET $update_order WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $pnr_vars);

		if (isset($_REQUEST['alternate_payments'])) {
			if ($_REQUEST['alternate_payments'] != "") {
				saveAlternatePaymentTotals($_REQUEST['loc_id'], $detail_array[0], $detail_array[1], $_REQUEST['alternate_payments']);
			}
		}

		//if ($_REQUEST['no_of_checks'] > 1) {
		
			// ***** check_details indexes *****
			//
			// 	0 - subtotal
			//  1 - discount amount
			//  2 - discount sh
			//  3 - tax amount
			//  4 - gratuity amount
			//  5 - gratuity percent
			//  6 - remaining amount
			//  7 - check total amount
			//  8 - discount value
			//  9 - discount type
			// 10 - rounded amount
			// 11 - checked out flag
			
			$check_details = explode("|*|", $_REQUEST['check_details']);

			$q_fields = "";
			$q_values = "";
			$c_vars = array();
			$c_vars['loc_id'] = $_REQUEST['loc_id'];
			$c_vars['order_id'] = $detail_array[0];
			$c_vars['check'] = $detail_array[1];
			$c_vars['subtotal'] = $check_details[0];
			$c_vars['discount'] = $check_details[1];
			$c_vars['discount_sh'] = $check_details[2];
			$c_vars['tax'] = $check_details[3];
			$c_vars['gratuity'] = $check_details[4];
			$c_vars['gratuity_percent'] = $check_details[5];
			$c_vars['remaining'] = $check_details[6];
			$c_vars['check_total'] = $check_details[7];
			$c_vars['discount_value'] = $check_details[8];
			$c_vars['discount_type'] = $check_details[9];
			$c_vars['rounding_amount'] = $check_details[10];
			$c_vars['checked_out'] = $check_details[11];
			$keys = array_keys($c_vars); 
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$c_vars['amount'] = $detail_array[2];
			$c_vars['total_collected'] = $detail_array[3];

			$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' LIMIT 1", $pnr_vars);
			if (@mysqli_num_rows($check_for_details) > 0) {
				$update_details = lavu_query("UPDATE `split_check_details` SET $update_check WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $pnr_vars);
			} else {	
				$create_details = lavu_query("INSERT INTO `split_check_details` ($c_fields, $q_fields) VALUES ($c_values, $q_values)", $c_vars);
			}
		//}
	
		echo '{"json_status":"success","pnr_row_id":"'.$row_id.'"}';
		exit();

	} else if ($mode == 43) { // a new better way of emailing receipts - version 1.7 and later
	
		$receipt_parts = explode(":", $_REQUEST['receipt_string']);
		$subject_line = "Receipt from ".$receipt_parts[1]." - Order #".$_REQUEST['order_id'].", Check #".$_REQUEST['check'];

		$messsage_plain = "Thank you so much for allowing us to server you today.\n\n";
		$message_html = "Thank you so much for allowing us to serve you today.<br><br>";
		$line_count = 0;
		for ($l = 2; $l < count($receipt_parts); $l++) {
			$this_line = $receipt_parts[$l];
			if ($this_line == "---------------------------") {
				$line_count++;
				if ($line_count == 1) {
					$message_plain .= "\n\n";
					$message_html .= "<br><br><table cellspacing='0' cellpadding='2' width='400px'>";
				} else if ($line_count == 2) {
					$message_plain .= "\n------------------------------------------------------------";
					$message_html .= "<tr><td colspan='3'><hr color='#EEEEEE'></td></tr>";
				} else if ($line_count == 3) {
					$message_plain .= "\n\n\nHave a great day! Visit us again soon!\n\n";
					$message_html .= "</table><br><br>Have a great day! Visit us again soon!<br><br>";
				} else if ($line_count == 4) {
					break;
				}
			} else {
				if (($line_count == 0) || ($line_count == 3)) {
					$this_line = str_replace("[s", "*", $this_line);
					$message_plain .= str_replace("[c", ":", $this_line)."\n";
					$message_html .= str_replace("[c", ":", $this_line)."<br>";
				} else if ($line_count == 1) {
					$line_parts = explode(" ", trim($this_line));
					$quantity = $line_parts[0];
					if (($quantity == "-") || ($quantity == "[s")) {
						$quantity = "";
					} else {
						$line_parts[0] = "";
						$this_line = join(" ", $line_parts);
					}
					$price_separated = explode("*", $this_line);
					$price_separated = str_replace("[s", "*", $price_separated);
					$message_plain .= "\n".str_pad($quantity, 5, " ", STR_PAD_LEFT)."  ".str_pad($price_separated[0], 38, " ", STR_PAD_RIGHT)."  ".str_pad($price_separated[1], 10, " ", STR_PAD_LEFT);
					$message_html .= "<tr><td align='right' style='padding:0px 5px 2px 5px'>".$quantity."</td><td align='left' valign='top' style='padding:0px 5px 2px 5px'>".$price_separated[0]."</td><td align='right' valign='top' style='padding:0px 5px 2px 5px'>".$price_separated[1]."</td></tr>";
				} else if ($line_count == 2) {
					if (strstr($this_line, "*")) {
						$line_parts = explode("*", str_replace("[c", ":", $this_line));
						if ($line_parts[0] == "") {
							$line_parts[0] = " ";
						}
						$message_plain .= "\n       ".str_pad(str_replace("[s", "*", $line_parts[0]), 38, " ", STR_PAD_RIGHT)."  ".str_pad(str_replace("[s", "*",$line_parts[1]), 10, " ", STR_PAD_LEFT);
						$message_html .= "<tr><td></td><td align='left' valign='top' style='padding:2px 5px 2px 5px'>".str_replace("[s", "*", $line_parts[0])."</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".str_replace("[s", "*",$line_parts[1])."</td></tr>";
					} else {
						$message_plain .= "\n       ".str_pad(str_replace(array("[s", "[c"), array("*", ":"), $this_line), 38, " ", STR_PAD_RIGHT);
						$message_html .= "<tr><td colspan='3' align='left' valign='top' style='padding:1px 5px 1px 5px'>".nl2br(str_replace("[c", ":", $this_line))."</td></tr>";
					}
				}
			}
		}
		
		$mime_boundary = "----=_NextPart_X[".md5(time())."]";

		$headers  = "MIME-Version: 1.0" . "\r\n";
		//$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		//$headers .= "Content-Type: multipart/alternative; boundary=\"".$mime_boundary."\"\r\n";
		$headers .= "From: ".$receipt_parts[1]." POSLavu <receipts@poslavu.com>" . "\r\n";

		/*$email_body = "�".$mime_boundary."\r\n".
"Content-Type: text/plain; charset=\�utf-8\"\r\n".
"Content-Transfer-Encoding: quoted-printable\r\n\r\n".
$message_plain."\r\n\r\n".
"--".$mime_boundary."\r\n".
"Content-Type: text/html; charset=\"utf-8\"\r\n".
"Content-Transfer-Encoding: 8bit\r\n\r\n".
$message_html."\r\n\r\n".
"--".$mime_boundary."--\r\n";*/

		$email_body = $message_plain;

		mail($_REQUEST['email'], $subject_line, $email_body, $headers);
		
		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = "Order Receipt Emailed";
		$l_vars['details'] = $_REQUEST['email'];
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = $_REQUEST['order_id'];
		$l_vars['check'] = $_REQUEST['check'];
		$l_vars['time'] = $device_time;
		$l_vars['user'] = $_REQUEST['server'];
		$l_vars['user_id'] = $_REQUEST['server_id'];
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		
		echo "1|".$email_body;
		exit();
	
	} else if ($mode == 44) { // load language pack and set location language pack id
	
		$pack_id = (isset($_REQUEST['pack_id']))?$_REQUEST['pack_id']:"1";
		
		if ($_REQUEST['loc_id'] != "") {
			$update_location = lavu_query("UPDATE `locations` SET `use_language_pack` = '[1]' WHERE `id` = '[2]'", $pack_id, $_REQUEST['loc_id']);
		}
		
		echo'{"json_status":"success","language_pack":'.generateLanguagePack($_REQUEST['loc_id'], $pack_id).'}';
		exit();
	
	} else if ($mode == 45) { // load alternate payment totals for an order
	
		$jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
	
		$return_string = "";
		$checks_array = array();

		$get_alternate_payment_totals = lavu_query("SELECT * FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' ORDER BY `check` ASC", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (@mysqli_num_rows($get_alternate_payment_totals) > 0) {
			while ($extract = mysqli_fetch_assoc($get_alternate_payment_totals)) {
				if (!array_key_exists($extract['check'], $checks_array)) {
					$checks_array[$extract['check']] = array();	
				}
				$checks_array[$extract['check']][] = '{"pay_type_id":"'.$extract['pay_type_id'].'","type":"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['type']).'","amount":"'.$extract['amount'].'","tip":"'.$extract['tip'].'","refund_amount":"'.$extract['refund_amount'].'"}';	
			}
			
			$keys = array_keys($checks_array);
			foreach ($keys as $check) {
				$return_string .= ',"'.$check.'":['.join(",",$checks_array[$check]).']';
			}
		}
		
		echo '{"json_status":"success"'.$return_string.'}';
		exit;
		
	} else if ($mode == 46) { // void a payment
	
		$auth_by = convertPIN($_REQUEST['PINused'], "name");
		$auth_by_id = convertPIN($_REQUEST['PINused']);
	
		$q_filter = "";
		$u_vars = array();
		$u_vars['loc_id'] = $_REQUEST['loc_id'];
		$u_vars['order_id'] = $_REQUEST['order_id'];
		$u_vars['check'] = $_REQUEST['check'];
		$u_vars['pay_type'] = $_REQUEST['pay_type'];
		$u_vars['action'] = $_REQUEST['action'];
		$keys = array_keys($u_vars);
		foreach ($keys as $key) {
			$q_filter .= " AND `$key` = '[$key]'";
		}
		$u_vars['amount'] = $_REQUEST['amount'];
		$get_payment = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `voided` = '0'$q_filter AND (`amount` * 1) = ('[amount]' * 1) ORDER BY `id` ASC LIMIT 1", $u_vars);
		if (@mysqli_num_rows($get_payment) > 0) {
			$id_info = mysqli_fetch_assoc($get_payment);
			$void_payment = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]' WHERE `id` = '[3]'", $_REQUEST['reason'], $auth_by_id, $id_info['id']);
		}
		
		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		if ($_REQUEST['action'] == "Sale") {
			$l_vars['action'] = "Payment Voided";
		} else {
			$l_vars['action'] = "Refund Voided";
		}
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = $_REQUEST['order_id'];
		$l_vars['check'] = $_REQUEST['check'];
		$l_vars['time'] = $device_time;
		$l_vars['user'] = $auth_by;
		$l_vars['user_id'] = $auth_by_id;
		$l_vars['details'] = $_REQUEST['pay_type']." - ".number_format((float)$_REQUEST['amount'], $decimal_places, ".", "");
		$l_vars['device_udid'] = $_REQUEST['UDID'];
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
	
		echo "1";
		exit();
	}

?>
