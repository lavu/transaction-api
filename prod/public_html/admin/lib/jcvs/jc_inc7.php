<?php

	require_once(__DIR__."/jc_functions.php");

	if (!isset($location_info))
	{
		$location_info = array();
	}
	$device_time = determineDeviceTime($location_info, $_REQUEST);

	if (!isset($decimal_places))
	{
		$decimal_places = locationSetting("disable_decimal", 2);
	}
	$smallest_money = (1 / pow(10, $decimal_places));

	$mode = (isset($_REQUEST['m']))?$_REQUEST['m']:"";
	$client_app = (isset($_REQUEST['app']))?$_REQUEST['app']:"";

	if ($mode==12 || $mode==53 || $mode==58)
	{
		require_once(dirname(__FILE__).'/../../../inc/loyalty/loyaltree/LoyalTreeIntegration.php');  // LoyalTree class
	}

	$lls = ((($location_info['use_net_path'] * 1) > 0) && !strstr($location_info['net_path'], "lavu") && !strstr($location_info['net_path'], "lsvpn"));

	if (!isset($location_info['modules'])) {
		$location_info['modules'] = "";
	}
	if (!isset($company_info['signup_package'])) {
		$company_info['signup_package'] = "2";
	}
	if (!isset($company_info['package_status'])) {
		$company_info['package_status'] = "2";
	}
	
	require_once(dirname(__FILE__)."/../modules.php");
	$active_modules = getActiveModules($location_info['modules'], (!empty($company_info['signup_package'])?$company_info['signup_package']:$company_info['package_status']), $lls);

	$jc_addons = get_addon_list($mode);
	process_jc_addons($jc_addons, $active_modules, $mode, "before");

	if ($mode == "punch_status") {

		mlavu_close_db();

		$send_id_and_name = "";

		if (isset($_REQUEST['rf_id'])) {

			$get_user = lavu_query("SELECT `id`, `f_name`, `l_name` FROM `users` WHERE `rf_id` = '[1]'", $_REQUEST['rf_id']);
			if (mysqli_num_rows($get_user) == 1) {
				$info = mysqli_fetch_assoc($get_user);
				$server_id = $info['id'];
				$server_name = $info['f_name']." ".$info['l_name'];
				$send_id_and_name = ',"server_id":"'.$server_id.'","server_name":"'.$server_name.'"';
			} else if (mysqli_num_rows($get_user) > 1) {
				lavu_echo('{"json_status":"error","error_title":"Status Check Failure","error_message":"More than one user record found with RFID '.$_REQUEST['rf_id'].'."}');
			} else {
				lavu_echo('{"json_status":"error","error_title":"Status Check Failure","error_message":"Unable to match RFID string ('.$_REQUEST['rf_id'].') to user record."}');
			}

		} else {
			
			$server_id = $_REQUEST['server_id'];
		}
		
		$punch_query = lavu_query("SELECT `punch_type`, `time`, `time_out`, `id` FROM `clock_punches` WHERE `punched_out` = '0' AND `_deleted` != '1' AND `server_id` = '[1]' AND `location_id` = '[2]' ORDER BY `time` DESC LIMIT 1", $server_id, $_REQUEST['loc_id']);
		if (mysqli_num_rows($punch_query)) {
			$punch_read = mysqli_fetch_assoc($punch_query);
			$punch_type = $punch_read['punch_type'];
			$punch_time = $punch_read['time'];
			$punch_time_out = $punch_read['time_out'];
			$punch_id = $punch_read['id'];
			$clocked_in = 1;
		} else {
			$punch_type = "";
			$punch_time = "";
			$punch_time_out = "";
			$punch_id = "";
			$clocked_in = 0;
		}
		//if ($punch_type=="Shift" && $punch_time_out=="" && $punch_time!="") $clocked_in = 1; else $clocked_in = 0;

		$open_order_list = '[]';
		if ($clocked_in=="1" && $location_info['open_orders_prevent_clock_out']=="1") {
			$cfoo_base_query = "SELECT `order_id`, `tablename`, `opened` FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[1]' AND `server_id` = '[2]' AND `void` = '0' AND (opened_or_reopened)";
			$cfoo_full_query = str_replace("(opened_or_reopened)", "`opened` != '0000-00-00 00:00:00' AND `opened` != '' AND `closed` = '0000-00-00 00:00:00'", $cfoo_base_query);
			$cfoo_full_query .= " UNION ".str_replace("(opened_or_reopened)", "`reopened_datetime` != '' AND `reclosed_datetime` = ''", $cfoo_base_query);			
			$check_for_open_orders = lavu_query($cfoo_full_query." ORDER BY `opened` DESC", $_REQUEST['loc_id'], $server_id);
			if (mysqli_num_rows($check_for_open_orders) > 0) {
				$order_array = array();
				while ($info = mysqli_fetch_assoc($check_for_open_orders)) {
					$order_array[] = '["'.$info['order_id'].'","'.json_escape($info['tablename']).'"]';
				}
				$open_order_list = '['.implode(",", $order_array).']';
			}
		}

		lavu_echo('{"json_status":"success","PHPSESSID":"'.session_id().'","open_orders":'.$open_order_list.',"clocked_in":"'.$clocked_in.'","time":"'.$punch_time.'","punchid":"'.$punch_id.'"'.$send_id_and_name.'}');

	} else if ($mode == "clock_in") {

		mlavu_close_db();

		$device_id = determineDeviceIdentifier($_REQUEST);

		// Put the payrate in the clock_punches table to support better reporting for multiple roles for one user.
		$role_id = "";
		$payrate = "";
		$get_user_info = lavu_query("SELECT role_id, payrate FROM `users` where `id` = '[1]'", $_REQUEST['server_id']);
		if (mysqli_num_rows($get_user_info) > 0) {
			$user_info = mysqli_fetch_assoc($get_user_info);
			// Start by using the pay rate contained in the payrate column in the users table but allow that to be
			// overridden if they have the pipe-delimited payrate info in the role_id column.
			$payrate = $user_info['payrate'];
			if (isset($_REQUEST['role_id'])) {
				$role_id = $_REQUEST['role_id'];
				if (!empty($role_id)) {
					if (strpos($user_info['role_id'], '|')) {
						$role_payrates = explode(',', $user_info['role_id']);
						foreach ($role_payrates as $rp) {
							$rp_parts = explode('|', $rp);
							$db_role_id = $rp_parts[0];
							$db_payrate = $rp_parts[1];
							if  ($db_role_id == $role_id) {
								$payrate = $db_payrate;
							}
						}
					}
				}
			}
		}

		$q_fields = "";
		$q_values = "";
		$p_vars = array();
		$p_vars['location'] = $location_info['title'];
		$p_vars['location_id'] = $_REQUEST['loc_id'];
		$p_vars['punch_type'] = "Shift";
		$p_vars['server'] = $_REQUEST['server_name'];
		$p_vars['server_id'] = $_REQUEST['server_id'];
		$p_vars['time'] = $device_time;
		$p_vars['hours'] = "0";
		$p_vars['punched_out'] = "0";
		$p_vars['udid_in'] = $device_id;
		$p_vars['ip_in'] = $_SERVER['REMOTE_ADDR'];
		$p_vars['role_id'] = $role_id;
		$p_vars['payrate'] = $payrate;
		$keys = array_keys($p_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$insert_punch = lavu_query("INSERT INTO `clock_punches` ($q_fields, `server_time`) VALUES ($q_values, now())", $p_vars);

		$update_user = lavu_query("UPDATE `users` SET `clocked_in` = '1' WHERE `id` = '[1]'", $_REQUEST['server_id']);

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = "Server Clocked In";
		$log_vars['loc_id'] = $_REQUEST['loc_id'];
		$log_vars['order_id'] = "0";
		$log_vars['time'] = $device_time;
		$log_vars['user'] = $_REQUEST['server_name'];
		$log_vars['user_id'] = $_REQUEST['server_id'];
		$log_vars['device_udid'] = $device_id;
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		$dt_parts = explode(" ", $device_time);

		if ($insert_punch) {
			$success_str = "success";
		} else {
			$success_str = "failed";
		}

		lavu_echo('{"json_status":"'.$success_str.'","PHPSESSID":"'.session_id().'","time_in":"'.$dt_parts[1].'","full_time":"'.$device_time.'"}');

	} else if ($mode == "clock_out") {

		mlavu_close_db();

		$get_punch = lavu_query("SELECT `id`, `time`, `role_id` FROM `clock_punches` WHERE `_deleted` != '1' AND `id` = '[1]'", $_REQUEST['punchid']);
		if (mysqli_num_rows($get_punch)) {

			$extract = mysqli_fetch_assoc($get_punch);

			$clockInArray = explode(" ", $extract['time']);
			$clockInDate = explode("-", $clockInArray[0]);
			$clockInTime = explode(":", $clockInArray[1]);
			$inStamp = mktime($clockInTime[0], $clockInTime[1], $clockInTime[2], $clockInDate[1], $clockInDate[2], $clockInDate[0]);

			$clockOutArray = explode(" ", $device_time);
			$clockOutDate = explode("-", $clockOutArray[0]);
			$clockOutTime = explode(":", $clockOutArray[1]);
			$outStamp = mktime($clockOutTime[0], $clockOutTime[1], $clockOutTime[2], $clockOutDate[1], $clockOutDate[2], $clockOutDate[0]);

			$hours = (($outStamp - $inStamp) / 3600);

			$device_id = determineDeviceIdentifier($_REQUEST);

			$p_vars = array();
			$q_update = "";
			$p_vars['time_out'] = $device_time;
			$p_vars['hours'] = number_format($hours, 3);
			$p_vars['udid_out'] = $device_id;
			$p_vars['ip_out'] = $_SERVER['REMOTE_ADDR'];
			$p_vars['break'] = (isset($_REQUEST['break']))?$_REQUEST['break']:"0";
			$keys = array_keys($p_vars);
			foreach ($keys as $key) {
				if ($q_update != "") {
					$q_update .= ", ";
				}
				$q_update .= "`$key` = '[$key]'";
			}
			$p_vars['id'] = $extract['id'];

			$update_punched_out = lavu_query("UPDATE `clock_punches` SET `punched_out` = '1', $q_update, `server_time_out` = now() WHERE id = '[id]'", $p_vars);
			$update_user = lavu_query("UPDATE `users` SET `clocked_in` = '0' WHERE `id` = '[1]'", $_REQUEST['server_id']);

			$q_fields = "";
			$q_values = "";
			$log_vars = array();
			$log_vars['action'] = "Server Clocked Out";
			$log_vars['loc_id'] = $_REQUEST['loc_id'];
			$log_vars['order_id'] = "0";
			$log_vars['time'] = $device_time;
			$log_vars['user'] = $_REQUEST['server_name'];
			$log_vars['user_id'] = $_REQUEST['server_id'];
			$log_vars['device_udid'] = $device_id;
			$keys = array_keys($log_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

			$dt_parts = explode(" ", $device_time);

			lavu_echo('{"json_status":"success","PHPSESSID":"'.session_id().'","time_in":"'.$clockInArray[1].'","time_out":"'.$dt_parts[1].'","hours":"'.number_format($hours, 3).'","full_time":"'.$device_time.'","role_id":"'.$extract['role_id'].'"}');
		}

		lavu_exit();

	} else if ($mode == 1)  { // log in from iPhone, iPod touch, or iPad
	
		if ($client_app == "Lavu Deliver") {

			performExternalLogin($client_app, "driver", 1);
		
		} else if ($client_app == "Lavu Task") {
			
			performExternalLogin($client_app, "maintenance", 1);
		
		} else {

			$cust_query = mlavu_query("SELECT `dataname` FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `username` = '[1]'", trim($_REQUEST['un']));
			if(mysqli_num_rows($cust_query) > 0) {
				$cust_read = mysqli_fetch_assoc($cust_query);
				$dataname = $cust_read['dataname'];
	
				$get_company_info = mlavu_query("SELECT `id`, `data_name`, `company_name`, `company_code`, `logo_img`, `receipt_logo_img`, `dev`, `demo`, `disabled`, `chain_id`, `lavu_lite_server` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname);
				if (mysqli_num_rows($get_company_info) > 0) {
					$company_info = mysqli_fetch_assoc($get_company_info);
					if ($company_info['disabled']=="1" || $company_info['disabled'] == "2" || $company_info['disabled'] == "3") {
						lavu_echo('{"json_status":"AccountDisabled"}');
					}
				} else {
					lavu_echo('{"json_status":"NoRestaurants"}');
				}
	
				mlavu_close_db();
				lavu_connect_byid($company_info['id'], "poslavu_".$dataname."_db");

				$user_query = lavu_query("SELECT `id`, `quick_serve`, `service_type`, `loc_id`, `f_name`, `l_name`, `access_level`, `PIN`, `lavu_admin`, `role_id`, `_use_tunnel`, `username` FROM `users` WHERE `username` = '[1]' AND `password` = PASSWORD('[2]') AND `_deleted` != '1' AND `active` = '1'", trim($_REQUEST['un']), trim($_REQUEST['pw']));
				if(mysqli_num_rows($user_query) > 0) {
	
					$this_user_info = mysqli_fetch_assoc($user_query);
					set_sessvar('active_user_info', $this_user_info);

					$valid_PINs = "";
					$valid_admin_PINs = "";
					$user_data_array = array();
					$get_all_user_data = lavu_query("SELECT * FROM `users` WHERE `_deleted` != '1' ORDER BY `l_name`, `f_name` ASC");
					while ($data_obj = mysqli_fetch_object($get_all_user_data)) {
						$roles = explode(",", $data_obj->role_id);
						for( $roles_i = 0 ; $roles_i < count($roles); $roles_i++ ){
							$role_parts = explode("|", $roles[$roles_i] );
							$roles[$roles_i] = $role_parts[0];
						}
						$data_obj->role_id = implode(",", $roles);
	
						$user_data_array[] = $data_obj;
						if ($data_obj->active == "1") {
							$valid_PINs .= "|".$data_obj->PIN."|";
							if ($data_obj->access_level > 1) {
								$valid_admin_PINs .= "|".$data_obj->PIN."|";
							}
						}
					}
	
					$tack_on_build = ":".$_REQUEST['build'];
	
					$q_fields = "";
					$q_values = "";
					$log_vars = array();
					$log_vars['action'] = "User Logged In";
					$log_vars['loc_id'] = "0";
					$log_vars['order_id'] = "0";
					$log_vars['time'] = $device_time;
					$log_vars['user'] = $this_user_info['f_name']." ".$this_user_info['l_name'];
					$log_vars['user_id'] = $this_user_info['id'];
					$keys = array_keys($log_vars);
					foreach ($keys as $key) {
						if ($q_fields != "") {
							$q_fields .= ", ";
							$q_values .= ", ";
						}
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
					$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);
	
					lavu_echo(cleanJSON('{"json_status":"Valid","server_id":"'.$this_user_info['id'].'","quick_serve":"'.$this_user_info['quick_serve'].'","service_type":"'.$this_user_info['service_type'].'","lavu_admin":"'.$this_user_info['lavu_admin'].'","all_user_data":'.json_encode($user_data_array).',"access_level":"'.$this_user_info['access_level'].'","PIN":"'.$this_user_info['PIN'].'","f_name":"'.$this_user_info['f_name'].'","l_name":"'.$this_user_info['l_name'].'","company_id":"'.$company_info['id'].'","company_name":"'.$company_info['company_name'].'","company_code":"'.lsecurity_name($company_info['company_code'], $company_info['id']).$tack_on_build.'","data_name":"'.$company_info['data_name'].'","logo_img":"'.$company_info['logo_img'].'","receipt_logo_img":"'.$company_info['receipt_logo_img'].'","dev":"'.$company_info['dev'].'","demo":"'.$company_info['demo'].'","lavu_lite_server":"'.$company_info['lavu_lite_server'].'","valid_PINs":"'.$valid_PINs.'","valid_admin_PINs":"'.$valid_admin_PINs.'"}'));

				} else {
	
					lavu_echo('{"json_status":"Invalid","query":"2"}');
				}
	
			} else {
	
				lavu_echo('{"json_status":"Invalid","query":"1"}');
			}
		}

	} else if ($mode == 2) { // load company locations

		///
		//need to pull all needed session data before it is destroyed.
		$activeUserInfo = sessvar('active_user_info');
		///
		session_destroy();

		/*$chain_restaurants = array();
		$chain_restaurants[] = $company_info;
		if ($company_info['chain_id']!="" && is_numeric($company_info['chain_id'])) {
			$get_chain_restaurants = mlavu_query("SELECT `poslavu_MAIN_db`.`restaurants`.`id` AS `id`, `poslavu_MAIN_db`.`restaurants`.`data_name` AS `data_name`, `poslavu_MAIN_db`.`payment_status`.`force_allow_tabs_n_tables` AS `allow_tnt_mode` FROM `poslavu_MAIN_db`.`restaurants` LEFT JOIN `poslavu_MAIN_db`.`payment_status` ON `poslavu_MAIN_db`.`payment_status`.`restaurantid` = `poslavu_MAIN_db`.`restaurants`.`id` WHERE `chain_id` = '[1]' AND `data_name` != '[2]' AND `disabled` != '1' AND `disabled` != '2' AND `disabled` != '3'", $company_info['chain_id'], $data_name);
			if (mysqli_num_rows($get_chain_restaurants) > 0) {
				while ($chain_restaurant = mysqli_fetch_assoc($get_chain_restaurants)) {
					//$chain_restaurants[] = $chain_restaurant;
				}
			}
		}*/

		//echo "chain_restaurants: ".print_r($chain_restaurants, true);

		mlavu_close_db();
		
		$menu_id = "";
		$locations_array = loadLocations(false, $data_name, $menu_id);
		
		lavu_echo(cleanJSON('{"json_status":"success","PHPSESSID":"","locations":'.json_encode($locations_array).'}'));

	} else if ($mode == 3) { // load table setup, printer configuration, menu categories, menu items, menu groups, language pack

		register_connect("jc_inc", $company_info['id'], $data_name, reqvar("loc_id", ""), reqvar("model", ""), reqvar("UUID", ""));

		$for_lavu_togo = reqvar("ltg", "0");
		$pay_stat = checkPaymentStatus();

		if ($pay_stat['kickout']) {

			$flag_message = array("json_status"=>"LocationFlag", "flag_title"=>$pay_stat['title'], "flag_message"=>$pay_stat['message'], "flag_kickout"=>"1");
	
			lavu_echo(cleanJSON(json_encode($flag_message)));

		} else {

			// LP-726 -- Reset session var that controls whether or not to display the trial days remaining countdown for Free Trials
			set_sessvar('pay_status_warning_ts', '');

			$discount_types_array = array();
			$get_discount_types = lavu_query("SELECT * FROM `discount_types` WHERE `loc_id` = '[1]' AND `_deleted` != '1' ORDER BY `_order`, `title` ASC", $_REQUEST['loc_id']);
			while ($data_obj = mysqli_fetch_object($get_discount_types)) {
				$discount_types_array[] = $data_obj;
			}

			$reports_list_return_string = '"reports_list":[{"title":"Register Summary","mode":"register","filename":"till_report.php"},{"title":"Server Summary","mode":"server","filename":"till_report.php"}]';
			$printers_return_string = getSupportedPrinters($_REQUEST['loc_id']);
			$all_menu_data_return_string = loadMenuData($_REQUEST['menu_id'], $_REQUEST['loc_id'], $for_lavu_togo);

			if ($client_app=="Lavu Deliver" || $client_app=="Lavu Maintenance") {
				$this_server_info = getERSuserInfo($_REQUEST['server_id']);
			} else {
				$get_server_name = lavu_query("SELECT `f_name`, `l_name` FROM `users` WHERE id = '[1]'", $_REQUEST['server_id']);
				$this_server_info = mysqli_fetch_assoc($get_server_name);
			}

			$q_fields = "";
			$q_values = "";
			$l_vars = array();
			$l_vars['action'] = "Location Chosen";
			$l_vars['details'] = $location_info['title'];
			$l_vars['loc_id'] = $_REQUEST['loc_id'];
			$l_vars['order_id'] = "0";
			$l_vars['check'] = "0";
			$l_vars['time'] = $device_time;
			$l_vars['user'] = $this_server_info['f_name']." ".$this_server_info['l_name'];
			$l_vars['user_id'] = $_REQUEST['server_id'];
			$l_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
			$keys = array_keys($l_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

			$check_field = "UDID";
			if (isset($_REQUEST['UUID'])) {
				$check_field = "UUID";
			} else if (isset($_REQUEST['MAC'])) {
				$check_field = "MAC";
			}

			lavu_query("UPDATE `devices` SET `needs_reload` = '0' WHERE `$check_field` = '[1]'", $_REQUEST[$check_field]);

			// load components if necessary
			$component_data_return_string = "";
			$comp_pack = (isset($_REQUEST['comp_pack']))?$_REQUEST['comp_pack']:"0";
			if (isset($_REQUEST['comp_pack_number'])) {
				switch ($_REQUEST['comp_pack_number']) {
					case 1:
						$comp_pack = $location_info['component_package'];
						break;
					case 2:
						$comp_pack = $location_info['component_package2'];
						break;
					case 3:
						$comp_pack = $location_info['component_package3'];
						break;
					default:
						break;
				}
			}
			if ($comp_pack!="0" && $comp_pack!="") {
				$component_data_return_string = loadComponents($comp_pack, $active_modules);
			}

			if (isset($_REQUEST['lavu_lite'])) {
				if (isset($location_info['using_lavu_lite_app'])) {
					$update_setting = lavu_query("UPDATE `config` SET `value` = '[1]' WHERE `type` = 'location_config_setting' AND `setting` = 'using_lavu_lite_app' AND `location` = '[2]'", $_REQUEST['lavu_lite'], $location_info['id']);
				} else {
					$create_setting = lavu_query("INSERT INTO `config` (`type`, `setting`, `location`, `value`) VALUES ('location_config_setting', 'using_lavu_lite_app', '[1]', '[2]')", $location_info['id'], $_REQUEST['lavu_lite']);
				}
			}

			$prefix_and_next_offline_order_id = "";
			if ($_REQUEST['UUID']) {
				$device_prefix = determineDevicePrefix($_REQUEST['loc_id'], $_REQUEST['UUID'], $_REQUEST['app_name'], $_REQUEST['model']);
				$prefix_digits = MIN(MAX((int)reqvar("prefix_digits", "2"), 2), 4);
				$full_prefix = "3".str_pad($device_prefix, $prefix_digits, "0", STR_PAD_LEFT)."-";
				$next_offline_order_id = str_replace($full_prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $_REQUEST['loc_id'], $full_prefix));			
				$prefix_and_next_offline_order_id = ',"device_prefix":"'.$device_prefix.'","next_offline_order_id":"'.$next_offline_order_id.'"';
			}

			$response = '{"json_status":"success","PHPSESSID":"'.session_id().'"'.$prefix_and_next_offline_order_id.',"discount_types":'.json_encode($discount_types_array).','.$reports_list_return_string.','.$printers_return_string.','.$all_menu_data_return_string.$component_data_return_string.',"available_language_packs":'.getAvailableLanguagePacks().',"language_pack":'.generateLanguagePack($_REQUEST['loc_id'], $location_info['use_language_pack']).'}';

			lavu_echo(cleanJSON($response));
		}

	} else if ($mode == 4) { // load open order, returns number of send points

		mlavu_close_db();

		$order_info = array();
		$order_contents = array();
		$order_modifiers = array();
		$send_points = 0;

		$vars = array();
		$vars['location_id'] = $_REQUEST['loc_id'];
		$filter = "";
		if (isset($_REQUEST['table']) && !empty($_REQUEST['table']) && empty($_REQUEST['order_id'])) {
			$filter .= " AND (`tablename` COLLATE latin1_general_cs = '[tablename]' OR `tabname` COLLATE latin1_general_cs = '[tablename]')";
			$vars['tablename'] = str_replace("[AMPERSAND]", "&", $_REQUEST['table']);
		}
		if (isset($_REQUEST['order_id'])) {
			$filter .= " AND `order_id` = '[order_id]'";
			$vars['order_id'] = $_REQUEST['order_id'];
		}
		if (isset($_REQUEST['ioid'])) {
			$filter .= " AND `ioid` = '[ioid]'";
			$vars['ioid'] = $_REQUEST['ioid'];
		}

		$held_by = "";
		$device_id = determineDeviceIdentifier($_REQUEST);

		$get_order = lavu_query("SELECT * FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[location_id]' AND `void` = '0' AND (`closed` = '0000-00-00 00:00:00')".$filter." UNION SELECT * FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[location_id]' AND `void` = '0' AND `reopened_datetime` != '' AND `reclosed_datetime` = ''".$filter, $vars);
		if (mysqli_num_rows($get_order) > 0) {
			$data_obj = mysqli_fetch_object($get_order);
			$order_info[] = $data_obj;
			$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $data_obj->order_id, $data_obj->ioid);
			$send_points = 0;
			while ($data_obj2 = mysqli_fetch_object($get_contents)) {
				$order_contents[] = $data_obj2;
				if ($data_obj2->item == "SENDPOINT") {
					$send_points++;
				}
			}
			$get_modifiers = lavu_query("SELECT * FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `qty` != 0", $_REQUEST['loc_id'], $data_obj->order_id);
			while ($data_obj2 = mysqli_fetch_object($get_modifiers)) {
				$order_modifiers[] = $data_obj2;
			}
			$all_transactions_array = array();
			$alt_totals = array();
			$voided_cct_filter = ((isset($_REQUEST['pull_voided_ccts'])) && $_REQUEST['pull_voided_ccts']=="1")?"":" AND `voided` = '0'";		
			$get_cc_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'".$voided_cct_filter." AND (`action` = 'Sale' OR `action` = 'Refund') AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $_REQUEST['loc_id'], $data_obj->order_id);
			if (mysqli_num_rows($get_cc_transactions) > 0) {
				while ($data_obj2 = mysqli_fetch_object($get_cc_transactions)) {
					$all_transactions_array[] = $data_obj2;
					$action = $data_obj2->action;
					if ((int)$data_obj2->pay_type_id>=4 && ($action=="Sale" || $action=="Refund") && $data_obj2->voided==0) {
						$alt_key = $data_obj2->check."_".$data_obj2->pay_type_id;
						$alt_amt = ($action == "Sale")?$data_obj2->total_collected:0;
						$alt_tip = $data_obj2->tip_amount;
						$alt_rfnd = ($action == "Refund")?$data_obj2->total_collected:0;
						if (isset($alt_totals[$alt_key])) {
							$alt_totals[$alt_key]['amount'] += $alt_amt;
							$alt_totals[$alt_key]['tip'] += $alt_tip;
							$alt_totals[$alt_key]['refund_amount'] += $alt_rfnd;
						} else {
							$alt_totals[$alt_key] = array("amount"=>$alt_amt, "tip"=>$alt_tip, "refund_amount"=>$alt_rfnd, "type"=>$data_obj2->pay_type);
						}
					}
				}
			}
			$check_details_array = array();
			$get_check_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' ORDER BY `check` ASC", $_REQUEST['loc_id'], $data_obj->order_id);
			if (mysqli_num_rows($get_check_details) > 0) {
				while ($data_obj2 = mysqli_fetch_object($get_check_details)) {
					$check_details_array[] = $data_obj2;
				}
			}
			$alternate_payments_array = array();
			$get_alternate_payment_totals = lavu_query("SELECT `ioid`, `check`, `pay_type_id`, `type`, `amount`, `tip`, `refund_amount` FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' ORDER BY `check` ASC", $_REQUEST['loc_id'], $data_obj->order_id);
			if (mysqli_num_rows($get_alternate_payment_totals) > 0) {
				while ($data_obj2 = mysqli_fetch_object($get_alternate_payment_totals)) {
					$alternate_payments_array[] = $data_obj2;
				}
			} else if (count($alt_totals) > 0) {
				foreach ($alt_totals as $key => $val) {
					$key_parts = explode("_", $key);
					$object = new stdClass();
					$object->ioid = $data_obj->ioid;
					$object->check = $key_parts[0];
					$object->pay_type_id = $key_parts[1];
					$object->type = $val['type'];
					$object->amount = $val['amount'];
					$object->tip = $val['tip'];
					$object->refund_amount = $val['refund_amount'];
					$alternate_payments_array[] = $object;
				}
			}

			// *** order holding kept track on order record	***
			//if ($data_obj->active_device=="" || $data_obj->active_device==$device_id) $update_active_device = lavu_query("UPDATE `orders` SET `active_device` = '[1]' WHERE `id` = '[2]'", $device_id, $data_obj->id);
			//else $held_by = ',"held_by":"'.$data_obj->active_device.'"';

			// *** order holding kept track on device record ***
			$holding_device = deviceHoldingThisOrder($_REQUEST['loc_id'], $data_obj->order_id);
			if ($holding_device && $holding_device!=$device_id) {
				$held_by = ',"held_by":"'.$holding_device.'"';
			} else {
				setOrderHeldByDevice($_REQUEST['loc_id'], $data_obj->order_id, $device_id);
			}

		} else {

			lavu_echo('{"json_status":"NoMatch","PHPSESSID":"'.session_id().'"}');
		}

		lavu_echo(cleanJSON('{"json_status":"success","PHPSESSID":"'.session_id().'"'.$held_by.',"order_info":'.json_encode($order_info).',"order_contents":'.json_encode($order_contents).',"order_modifiers":'.json_encode($order_modifiers).',"send_points":"'.$send_points.'","cc_transactions":'.json_encode($all_transactions_array).',"check_details":'.json_encode($check_details_array).',"alternate_payments":'.json_encode($alternate_payments_array).'}'));

	} else if ($mode == 5) { // save open order info, returns order id, replaced by mode 28

	} else if ($mode == 6) { // save order contents, replaced by mode 17)

	} else if ($mode == 7) { // load open orders

		mlavu_close_db();

		$now_parts = explode(" ", $device_time);
		$now_date = explode("-", $now_parts[0]);
		$now_time = explode(":", $now_parts[1]);

		$three_months_ts = mktime($now_time[0], $now_time[1], $now_time[2], ($now_date[1] - 3), $now_date[2], $now_date[0]);
		$three_months_ago = date("Y-m-d H:i:s", $three_months_ts);

		$order_info = array();
		$order_contents = array();
		$check_details = array();
		$recorded_payments = array();
		$upcoming_togo_orders = array();

		$limit_int = 20;
		if (isset($_REQUEST['lavu_togo'])) {
			$limit_int = 10;
		}

		$limit_string = "";
		if (isset($_REQUEST['page'])) {
			$limit_string = " LIMIT ".(((int)$_REQUEST['page'] - 1) * $limit_int).", $limit_int";
		}

		$ms_vars = array();
		$ms_vars['loc_id'] = $_REQUEST['loc_id'];

		$filter = " AND (not_closed_or_reopened) AND `opened` != '0000-00-00 00:00:00' AND `opened` != ''";
		if (!empty($_REQUEST['filter'])) {
			$filter .= str_replace("= '[server_prefix]-", "= '", urldecode($_REQUEST['filter']));
			if (strstr($filter, "[searchOrderContents::")) {
				$position1 = (strpos($filter, "[searchOrderContents::") + 22);
				$position2 = strpos($filter, "]");
				$search_string = substr($filter, $position1, ($position2 - $position1));
				$s_vars = array();
				$s_vars['loc_id'] = $_REQUEST['loc_id'];
				$s_vars['three_months_ago'] = $three_months_ago;
				$s_vars['search_string'] = $search_string;
				$matching_ids = "";
				$perform_search = lavu_query("SELECT `order_contents`.`order_id` FROM `orders` LEFT JOIN `order_contents` ON `order_contents`.`order_id` = `orders`.`order_id` WHERE `orders`.`order_id` NOT LIKE '777%' AND `orders`.`opened` >= '[three_months_ago]' AND `orders`.`location_id` = '[loc_id]' AND `order_contents`.`loc_id` = `orders`.`location_id` AND (`order_contents`.`item` LIKE '%[search_string]%' OR `order_contents`.`deposit_info` LIKE '%[search_string]%' OR `order_contents`.`options` LIKE '%[search_string]%' OR `order_contents`.`special` LIKE '%[search_string]%') GROUP BY `orders`.`order_id`", $s_vars);
				while ($matching_id = mysqli_fetch_assoc($perform_search)) {
					if ($matching_ids != "") {
						$matching_ids .= ",";
					}
					$matching_ids .= "'".$matching_id['order_id']."'";
				}
				if ($matching_ids != "") {
					$matching_ids = "`order_id` IN (".$matching_ids.") OR ";
				}
				$matching_ids .= "SUBSTRING(`opened`, 6, 5) LIKE '%[search_string]%' OR `togo_name` LIKE '%[search_string]%' OR `email` LIKE '%[search_string]%' OR `tablename` LIKE '%[search_string]%' OR `tabname` LIKE '%[search_string]%'";
				$ms_vars['search_string'] = $search_string;
				$filter = str_replace("[searchOrderContents::".$search_string."]", $matching_ids, $filter);
			}
		}

		$for_server_id = (!empty($_REQUEST['for_server_id']))?$_REQUEST['for_server_id']:"all";

		$inapp_ltg_check = isset($_REQUEST['check_upcoming']);

		$order_by = "ORDER BY `opened` DESC";
		if (isset($_REQUEST['lavu_togo'])) {
			$filter .= " AND `togo_status` = '1'";
			$order_by = "ORDER BY `togo_time` DESC";
			$printable = (isset($_REQUEST['printable']))?$_REQUEST['printable']:"0";
			if ($printable=="1" || $inapp_ltg_check) {
				$ltg_minutes = 30;
				if (isset($location_info['auto_print_minutes']) && $location_info['auto_print_minutes']!="") {
					$ltg_minutes = ($location_info['auto_print_minutes'] + 1);
				}
				$ltg_ts = mktime($now_time[0], ($now_time[1] + $ltg_minutes), $now_time[2], $now_date[1], $now_date[2], $now_date[0]);
				$ltg_datetime = date("Y-m-d H:i:s", $ltg_ts);
				$order_by = "ORDER BY `togo_time` ASC";
				if ($printable == "1") {
					$filter .= " AND `send_status` = '1' AND `togo_time` <= '$ltg_datetime'";
					$limit_string = " LIMIT 3";
				}
				if ($inapp_ltg_check) {
					$ten_min_ago = date("Y-m-d H:i:s", mktime($now_time[0], ($now_time[1] - 10), $now_time[2], $now_date[1], $now_date[2], $now_date[0]));
					$twenty_four_hr_from_now = date("Y-m-d H:i:s", mktime(($now_time[0] + 24), $now_time[1], $now_time[2], $now_date[1], $now_date[2], $now_date[0]));
					$filter .= " AND `togo_time` > '".$ten_min_ago."' AND `togo_time` <= '".$twenty_four_hr_from_now."'";
				}
			} else if ($_REQUEST['check_upcoming']) {
				$print_limit = 3;
			}
		} else if ($for_server_id != "all") {
			$filter .= " AND `server_id` = '[server_id]'";
			$ms_vars['server_id'] = $for_server_id;
		}

		if (!$inapp_ltg_check) {
			$get_count = lavu_query("SELECT COUNT(`id`) FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '0'".str_replace("(not_closed_or_reopened)", "`closed` = '0000-00-00 00:00:00'", $filter)." UNION SELECT COUNT(`id`) FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '0'".str_replace("(not_closed_or_reopened)", "`reopened_datetime` != '' AND `reclosed_datetime` = ''", $filter), $ms_vars);
			$total_count = 0;
			for ($r = 0; $r < mysqli_num_rows($get_count); $r++) {
				$total_count += mysqli_result($get_count, $r);
			}
		}

		$ltg_full_info_count = 0;
		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '0'".str_replace("(not_closed_or_reopened)", "`closed` = '0000-00-00 00:00:00'", $filter)." UNION SELECT * FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '0'".str_replace("(not_closed_or_reopened)", "`reopened_datetime` != '' AND `reclosed_datetime` = ''", $filter)." ".$order_by.$limit_string, $ms_vars);
		if (mysqli_num_rows($get_orders) > 0) {

			$order_ids = array();
			while ($data_obj = mysqli_fetch_object($get_orders)) {
				if ($inapp_ltg_check) {
					$upcoming_togo_orders[$data_obj->order_id] = array("order_id"=>$data_obj->order_id, "opened"=>$data_obj->opened, "togo_time"=>$data_obj->togo_time, "total"=>$data_obj->total, "togo_type"=>$data_obj->togo_type);
				}
				if (!$inapp_ltg_check || ($ltg_full_info_count<5 && $data_obj->togo_time<=$ltg_datetime && $data_obj->send_status=="1")) {
					$ltg_full_info_count++;
					$order_info[] = $data_obj;
					$order_ids[] = $data_obj->order_id;

					$no_of_checks = cappedCheckCount($data_obj->no_of_checks);
					
					$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' ORDER BY `check` ASC", $_REQUEST['loc_id']);
					if (mysqli_num_rows($get_details) < $no_of_checks) {
						for ($c = 1; $c <= $no_of_checks; $c++) {
							$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $_REQUEST['loc_id'], $data_obj->order_id, $c);
							if (mysqli_num_rows($check_for_details) == 0) {
								$insert = lavu_query("INSERT INTO `split_check_details` (`loc_id`, `order_id`, `check`) VALUES ('[1]', '[2]', '[3]')", $_REQUEST['loc_id'], $data_obj->order_id, $c);
							}
						}
						$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' ORDER BY `check` ASC", $_REQUEST['loc_id']);
					}
					while ($data_obj2 = mysqli_fetch_object($get_details)) {
						$check_details[] = $data_obj2;
					}
				}
			}
			$order_id_str = "'".implode("','", $order_ids)."'";
			
			if (count($order_ids) > 0) {
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` IN (".$order_id_str.") AND `quantity` > 0", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}
				$voided_cct_filter = ((isset($_REQUEST['pull_voided_ccts'])) && $_REQUEST['pull_voided_ccts']=="1")?"":" AND `voided` = '0'";	
				$get_payments_and_refunds = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` IN (".$order_id_str.")".$voided_cct_filter." AND (`action` = 'Sale' OR `action` = 'Refund') AND `tip_for_id` = '0'", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_payments_and_refunds)) {
					$recorded_payments[] = $data_obj2;
				}
			}
			if ($inapp_ltg_check) {
				$total_count = count($upcoming_togo_orders);
			}
		} else {
			lavu_echo('{"json_status":"NoMatch"'.$more_info.',"PHPSESSID":"'.session_id().'"}');
		}

		lavu_echo(cleanJSON('{"json_status":"success","PHPSESSID":"'.session_id().'","order_info":'.json_encode($order_info).',"order_contents":'.json_encode($order_contents).',"check_details":'.json_encode($check_details).',"recorded_payments":'.json_encode($recorded_payments).',"total_count":"'.$total_count.'","ltg_upcoming":'.json_encode($upcoming_togo_orders).'}'));

	} else if ($mode == 8) { // load today's closed orders (can pull up voided orders as well)

		mlavu_close_db();

		if (isset($_REQUEST['check_time'])) {

			$start_datetime = $_REQUEST['check_time'];

			$now_parts = explode(" ", date("Y-m-d H:i:s"));
			$now_date = explode("-", $now_parts[0]);
			$now_time = explode(":", $now_parts[1]);

		} else {

			$device_time = determineDeviceTime($location_info, array("device_time"=>$_REQUEST['now_time']));

			$se_hour = substr(str_pad($_REQUEST['start_time'], 4, "0", STR_PAD_LEFT), 0, 2);
			$se_min = substr(str_pad($_REQUEST['start_time'], 4, "0", STR_PAD_LEFT), 2, 2);

			$now_parts = explode(" ", $device_time);
			$now_date = explode("-", $now_parts[0]);
			$now_time = explode(":", $now_parts[1]);

			if (($now_time[0].$now_time[1]) < ($se_hour.$se_min)) {
				$use_ts = mktime($se_hour, $se_min, 0, $now_date[1], ($now_date[2] - 1), $now_date[0]);
				$start_datetime = date("Y-m-d H:i:s", $use_ts);
			} else {
				$start_datetime = $now_parts[0]." $se_hour:$se_min:00";
			}
		}

		$three_months_ts = mktime($now_time[0], $now_time[1], $now_time[2], ($now_date[1] - 3), $now_date[2], $now_date[0]);
		$three_months_ago = date("Y-m-d H:i:s", $three_months_ts);

		$order_info = array();
		$order_contents = array();
		$check_details = array();
		$recorded_payments = array();

		$limit_int = 20;
		if (isset($_REQUEST['lavu_togo'])) {
			$limit_int = 10;
		}

		$limit_string = "";
		if (isset($_REQUEST['page'])) {
			$limit_string = " LIMIT ".(((int)$_REQUEST['page'] - 1) * $limit_int).", $limit_int";
		}

		$ms_vars = array();
		$ms_vars['loc_id'] = $_REQUEST['loc_id'];

		$filter = " AND `order_id` NOT LIKE 'Paid%' AND ((`reopened_datetime` = '' AND `reclosed_datetime` = '') OR (`reopened_datetime` != '' AND `reclosed_datetime` != ''))";
		if (!empty($_REQUEST['filter'])) {
			$filter .= str_replace("= '[server_prefix]-", "= '", urldecode($_REQUEST['filter']));
			if (strstr($filter, "[searchOrderContents::")) {
				$position1 = (strpos($filter, "[searchOrderContents::") + 22);
				$position2 = strpos($filter, "]");
				$search_string = substr($filter, $position1, ($position2 - $position1));
				$s_vars = array();
				$s_vars['loc_id'] = $_REQUEST['loc_id'];
				$s_vars['three_months_ago'] = $three_months_ago;
				$s_vars['search_string'] = $search_string;
				$matching_ids = "";
				$perform_search = lavu_query("SELECT `order_contents`.`order_id` FROM `orders` LEFT JOIN `order_contents` ON `order_contents`.`order_id` = `orders`.`order_id` WHERE `orders`.`order_id` NOT LIKE '777%' AND `orders`.`opened` >= '[three_months_ago]' AND `orders`.`location_id` = '[loc_id]' AND `order_contents`.`loc_id` = `orders`.`location_id` AND (`order_contents`.`item` LIKE '%[search_string]%' OR `order_contents`.`deposit_info` LIKE '%[search_string]%' OR `order_contents`.`options` LIKE '%[search_string]%' OR `order_contents`.`special` LIKE '%[search_string]%') GROUP BY `orders`.`order_id`", $s_vars);
				while ($matching_id = mysqli_fetch_assoc($perform_search)) {
					if ($matching_ids != "") {
						$matching_ids .= ",";
					}
					$matching_ids .= "'".$matching_id['order_id']."'";
				}
				if ($matching_ids != "") {
					$matching_ids = "`order_id` IN (".$matching_ids.") OR ";
				}
				$matching_ids .= "SUBSTRING(`opened`, 6, 5) LIKE '%[search_string]%' OR SUBSTRING(`closed`, 6, 5) LIKE '%[search_string]%' OR `togo_name` LIKE '%[search_string]%' OR `email` LIKE '%[search_string]%' OR `tablename` LIKE '%[search_string]%' OR `tabname` LIKE '%[search_string]%'";
				$ms_vars['search_string'] = $search_string;
				$filter = str_replace("[searchOrderContents::".$search_string."]", $matching_ids, $filter);
				$filter .= " AND `closed` >= '$three_months_ago'";
			}
		} else {
			$filter .= " AND `closed` >= '$start_datetime'";
		}

		$for_server_id = (!empty($_REQUEST['for_server_id']))?$_REQUEST['for_server_id']:"all";

		$order_by = "ORDER BY `closed` DESC";
		if (isset($_REQUEST['lavu_togo'])) {
			$filter .= " AND `togo_status` = '1'";
			$order_by = "ORDER BY `togo_time` DESC";
		} else if ($for_server_id != "all") {
			$filter .= " AND `server_id` = '[server_id]'";
			$ms_vars['server_id'] = $for_server_id;
		}

		$list_mode = (isset($_REQUEST['order_list_mode']))?$_REQUEST['order_list_mode']:"closed";
		if ($list_mode == "closed") {
			$void_value = "0";
		} else if ($list_mode == "voided") {
			$void_value = "1";
		} else if ($list_mode == "merged") {
			$void_value = "2";
		}

		$get_count = lavu_query("SELECT COUNT(`id`) FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '".$void_value."'".$filter, $ms_vars);
		$total_count = 0;
		for ($r = 0; $r < mysqli_num_rows($get_count); $r++) {
			$total_count += mysqli_result($get_count, $r);
		}

		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '".$void_value."'".$filter." ".$order_by.$limit_string, $ms_vars);
		if (mysqli_num_rows($get_orders) > 0) {
		
			$order_ids = array();
			while ($data_obj = mysqli_fetch_object($get_orders)) {
				
				$order_info[] = $data_obj;
				$order_ids[] = $data_obj->order_id;
				
				$no_of_checks = cappedCheckCount($data_obj->no_of_checks);
				
				$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' ORDER BY `check` ASC", $_REQUEST['loc_id']);
				if (mysqli_num_rows($get_details) < $no_of_checks) {
					for ($c = 1; $c <= $no_of_checks; $c++) {
						$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $_REQUEST['loc_id'], $data_obj->order_id, $c);
						if (mysqli_num_rows($check_for_details) == 0) {
							$insert = lavu_query("INSERT INTO `split_check_details` (`loc_id`, `order_id`, `check`) VALUES ('[1]', '[2]', '[3]')", $_REQUEST['loc_id'], $data_obj->order_id, $c);
						}
					}
					$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' ORDER BY `check` ASC", $_REQUEST['loc_id']);
				}
				while ($data_obj2 = mysqli_fetch_object($get_details)) {
					$check_details[] = $data_obj2;
				}
			}
			$order_id_str = "'".implode("','", $order_ids)."'";
			
			if (count($order_ids) > 0) {
			
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` IN (".$order_id_str.") AND `quantity` > 0", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}
				$voided_cct_filter = ((isset($_REQUEST['pull_voided_ccts'])) && $_REQUEST['pull_voided_ccts']=="1")?"":" AND `voided` = '0'";
				$get_payments_and_refunds = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` IN (".$order_id_str.")".$voided_cct_filter." AND (`action` = 'Sale' OR `action` = 'Refund') AND `tip_for_id` = '0'", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_payments_and_refunds)) {
					$recorded_payments[] = $data_obj2;
				}
			}

		} else {
			lavu_echo('{"json_status":"NoMatch","PHPSESSID":"'.session_id().'"}');
		}

		lavu_echo(cleanJSON('{"json_status":"success","PHPSESSID":"'.session_id().'","order_info":'.json_encode($order_info).',"order_contents":'.json_encode($order_contents).',"check_details":'.json_encode($check_details).',"recorded_payments":'.json_encode($recorded_payments).',"total_count":"'.$total_count.'"}'));

	} else if ($mode == 9) { // check open orders

		mlavu_close_db();

		$open_orders = "";
		$checks_printed = "";
		$table_list = "";
		$tab_list = "";
		$id_list = "";
		$printed_list = "";

		$server_id_list = "";
		$server_ids = "";

		$goo_vars = array("loc_id"=>$_REQUEST['loc_id']);

		$tab_filter = " AND `tab` != '1'";
		if (isset($_REQUEST['tab_mode'])) {
			if ($_REQUEST['tab_mode'] == "tabs") {
				$tab_filter = " AND (`tab` = '1' OR `tabname` != '')";
			} else if ($_REQUEST['tab_mode'] == "tables") {
				$tab_filter = " AND `tab` != '1'";
			} else if ($_REQUEST['tab_mode'] == "both") {
				$tab_filter = "";
			} else {
				$tab_filter = " AND `tab` != '1'";
			}
		}

		if (isset($_REQUEST['not_order'])) {
			$goo_vars['not_order'] = $_REQUEST['not_order'];
			$tab_filter .= " AND `order_id` != '[not_order]'";
		}

		$server_id_filter = "";
		if (isset($_REQUEST['for_id'])) {
			if ($_REQUEST['for_id'] == "0") {
				$server_id_filter = "";
			} else {
				$goo_vars['server_id'] = $_REQUEST['for_id'];
				$server_id_filter = " AND `server_id` = '[server_id]'";
			}
		}

		$qs_filter = " AND `tablename` != 'Quick Serve'";
		if (isset($_REQUEST['qs'])) {
			$qs_filter = "";
		}

		$order_by = " ORDER BY SUBSTRING(`tablename`, 1, 1) ASC, LENGTH(`tablename`) ASC, `tablename` ASC, `opened` DESC";
		$table_list_key = "tablename";
		if (isset($_REQUEST['for_merge']) && $_REQUEST['for_merge']=="1" && $location_info['merge_order_list_identifier']=="order_id") {
			$order_by = " ORDER BY `order_id` ASC, `opened` DESC";
			$table_list_key = "order_id";
		}

		$goo_query = "SELECT `opened`, `tablename`, `tabname`, `order_id`, `check_has_printed`, `server_id` FROM `orders` WHERE `location_id` = '[loc_id]'$qs_filter AND [not_closed_or_reopened] AND `opened` != '0000-00-00 00:00:00' AND `opened` != '' AND `void` = '0'".$tab_filter.$server_id_filter;

		$get_open_orders = lavu_query(str_replace("[not_closed_or_reopened]", "`closed` = '0000-00-00 00:00:00'", $goo_query)." UNION ".str_replace("[not_closed_or_reopened]", "`reopened_datetime` != '' AND `reclosed_datetime` = ''", $goo_query).$order_by, $goo_vars);
		if (mysqli_num_rows($get_open_orders) > 0) {
			while ($extract = mysqli_fetch_assoc($get_open_orders)) {
				if ($extract['tablename'] == "Quick Serve") {
					$extract['tablename'] = "QS ".$extract['order_id'];
				}
				$table_list .= "|".$extract[$table_list_key]."|";
				$tab_list .= "|".$extract['tabname']."|";
				$id_list .= "|".$extract['order_id']."|";
				if ($extract['check_has_printed'] == "1") {
					$printed_list .= "|".$extract['tablename']."|";
				}
				$server_id_list .= "|".$extract['server_id']."|";
			}
			$open_orders = '"open_orders":"|'.json_escape($table_list).'|"';
			$tab_names = ',"tab_names":"|'.json_escape($tab_list).'|"';
			$checks_printed = '"checks_printed":"|'.json_escape($printed_list).'|"';
			$server_ids = ',"server_ids":"|'.$server_id_list.'|"';
		} else {
			$open_orders = '"open_orders":"none"';
			$tab_names = ',"tab_names":"none"';
			$checks_printed = '"checks_printed":"none"';
		}

		$flag_message = "";
		//$flag_message = ',"flag_message":"You\'ve been caught red handed trying to steal from our company! Please contact your local legal authority for immediate prosecution!","flag_title":"AH HA!!!","flag_kickout":"1"';

		$response = '{"json_status":"success","PHPSESSID":"'.session_id().'"'.$tab_names.$server_ids.','.$open_orders.','.$checks_printed.',"open_ids":"|'.$id_list.'|"'.$flag_message.'}';

		lavu_echo(cleanJSON($response));

	} else if ($mode == 10) { // close order after checkout

		$result = closeOrder($_REQUEST);
		$result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);

		lavu_echo($result);
		
	} else if ($mode == 11) { // update send_status for open order, save send point, log send

		$response = logSend($_REQUEST);

		lavu_echo($response);

	} else if ($mode == 12) { // perform order void

		$result = voidOrder($_REQUEST);
		$result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);

		lavu_echo($result);

	} else if ($mode == 13) { // log item edits after send

		$response = logItemEdits($_REQUEST);

		lavu_echo($response);

	} else if ($mode == 14) { // email receipt, not in use by app as of version 1.7, replaced by mode 43

	} else if ($mode == 15) { // save payment and discount details, not is use by app as of version 1.6.5 build 20110406-1, replaced by mode 40

	} else if ($mode == 16) { // update number of guests for order

		mlavu_close_db();

		$now_ts = time();

		$q_update = "";
		$o_vars = array();
		$o_vars['guests'] = $_REQUEST['guests'];
		if ($_REQUEST['gratuity'] != "DONOTUPDATE") {
			$o_vars['gratuity'] = $_REQUEST['gratuity'];
		}
		$o_vars['last_mod_ts'] = $now_ts;
		$o_vars['pushed_ts'] = $now_ts;
		$o_vars['last_modified'] = $device_time;
		$o_vars['last_mod_device'] = determineDeviceIdentifier($_REQUEST);
		$o_vars['last_mod_register_name'] = (isset($_REQUEST['register_name']))?$_REQUEST['register_name']:"";
		$keys = array_keys($o_vars);
		foreach ($keys as $key) {
			if ($q_update != "") {
				$q_update .= ", ";
			}
			$q_update .= "`$key` = '[$key]'";
		}
		$o_vars['location_id'] = $_REQUEST['loc_id'];
		$o_vars['order_id'] = $_REQUEST['order_id'];

		$log_guest_count_change = false;
		$get_previous_info = lavu_query("SELECT `guests` FROM `orders` WHERE`location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);
		$previous_info = mysqli_fetch_assoc($get_previous_info);
		if ($previous_info['guests'] != $o_vars['guests']) {
			$log_guest_count_change = "Guest count changed from ".$previous_info['guests']." to ".$o_vars['guests'];
		}

		if ($log_guest_count_change) {
			$q_fields = "";
			$q_values = "";
			$l_vars = array();
			$l_vars['action'] = $log_guest_count_change;
			$l_vars['loc_id'] = $_REQUEST['loc_id'];
			$l_vars['order_id'] = $_REQUEST['order_id'];
			$l_vars['time'] = $device_time;
			$l_vars['user'] = $_REQUEST['server'];
			$l_vars['user_id'] = $_REQUEST['server_id'];
			$l_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
			$keys = array_keys($l_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		}

		$update_order = lavu_query("UPDATE `orders` SET $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);

		lavu_echo('{"json_status":"success"}');

	} else if ($mode == 17) { // save order contents only, used to merge orders

		// ***** order_item indexes *****
		//
		//  0 - item
		//  1 - price
		//  2 - quantity
		//  3 - options
		//  4 - special
		//  5 - print
		//  6 - modify price
		//  7 - check number
		//  8 - seat number
		//  9 - item id
		// 10 - printer
		// 11 - apply taxrate
		// 12 - custom taxrate
		// 13 - modifier list id
		// 14 - forced modifier group id
		// 15 - forced modifiers price
		// 16 - course number
		// 17 - open item
		// 18 - allow deposit
		// 19 - deposit info
		// 20 - void flag
		// 21 - device time
		// 22 - notes
		// 23 - hidden data 1
		// 24 - hidden data 2
		// 25 - hidden data 3
		// 26 - hidden data 4
		// 27 - tax inclusion
		// 28 - sent
		// 29 - tax exempt
		// 30 - exemption id
		// 31 - exemption name
		// 32 - checked out
		// 33 - hidden data 5
		// 34 - price override
		// 35 - original price
		// 36 - override id
		// 37 - idiscount id
		// 38 - idiscount sh
		// 39 - idiscount value
		// 40 - idiscount type
		// 41 - idiscount amount
		// 42 - hidden data 6
		// 43 - idiscount info
		// 44 - category id
		// 45 - internal order id (ioid)
		// 46 - internal contents id (icid)

		mlavu_close_db();

		$check_offset = 1;
		$device_id = determineDeviceIdentifier($_REQUEST);
		$force_held_order_save = (isset($_REQUEST['force_held_order_save']))?$_REQUEST['force_held_order_save']:"0";
		$toIOID = "";

		if (!isset($_POST['do_not_clear'])) {
			$clear_current_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_POST['loc_id'], $_POST['order_id']);
		} else if (isset($_POST['subtotal']) && isset($_POST['tax']) && isset($_POST['total'])) {
			$o_vars = array();
			$o_vars['subtotal'] = (float)decimalize($_POST['subtotal']);
			$o_vars['tax'] = (float)decimalize($_POST['tax']);
			$o_vars['total'] = (float)decimalize($_POST['total']);
			$o_vars['location_id'] = $_POST['loc_id'];
			$o_vars['order_id'] = $_POST['order_id'];
			$o_vars['added_checks'] = (isset($_REQUEST['check_count']))?$_REQUEST['check_count']:getCheckCount($_POST['loc_id'], $_POST['from_order_id']);
			$o_vars['last_mod_ts'] = time();
			$o_vars['last_modified'] = $device_time;
			$o_vars['last_mod_device'] = $device_id;
			$o_vars['last_mod_register_name'] = (isset($_REQUEST['register_name']))?$_REQUEST['register_name']:"";
			$add_payments = "";
			if (isset($_POST['cash']) && isset($_POST['card']) && isset($_POST['gift']) && isset($_POST['alt']) && isset($_POST['casha'])) {
				$o_vars['cash_paid'] = (float)decimalize($_POST['cash']);
				$o_vars['card_paid'] = (float)decimalize($_POST['card']);
				$o_vars['gift_certificate'] = (float)decimalize($_POST['gift']);
				$o_vars['alt_paid'] = (float)decimalize($_POST['alt']);
				$o_vars['cash_applied'] = (float)decimalize($_POST['casha']);
				$o_vars['refunded'] = (float)decimalize($_POST['cashr']);
				$o_vars['refunded_cc'] = (float)decimalize($_POST['cardr']);
				$o_vars['refunded_gc'] = (float)decimalize($_POST['giftr']);
				$o_vars['alt_refunded'] = (float)decimalize($_POST['altr']);
				$add_payments = ", `cash_paid` = ((`cash_paid` * 1) + [cash_paid]), `card_paid` = ((`card_paid` * 1) + [card_paid]), `gift_certificate` = ((`gift_certificate` * 1) + [gift_certificate]), `alt_paid` = ((`alt_paid` * 1) + [alt_paid]), `cash_applied` = ((`cash_applied` * 1) + [cash_applied]), `refunded` = ((`refunded` * 1) + [refunded]), `refunded_cc` = ((`refunded_cc` * 1) + [refunded_cc]), `refunded_gc` = ((`refunded_gc` * 1) + [refunded_gc]), `alt_refunded` = ((`alt_refunded` * 1) + [alt_refunded])";
			}
			$add_gratuity = "";
			if (isset($_POST['grat'])) {
				$o_vars['gratuity'] = (float)decimalize($_POST['grat']);
				$add_gratuity = ", `gratuity` = ((`gratuity` * 1) + [gratuity])";
			}
			$get_info = lavu_query("SELECT `no_of_checks`, `merges`, `ioid`, `guests` FROM `orders` WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);
			$info = mysqli_fetch_assoc($get_info);

			$holding_device = deviceHoldingThisOrder($_REQUEST['loc_id'], $_REQUEST['order_id']);

			if ($holding_device && $holding_device!=$device_id && $force_held_order_save!="1") {

				lavu_echo("0:held_by:".str_replace(":", "[c", $holding_device));

			} else {

				$toIOID = $info['ioid'];

				$set_guests = "";
				if (isset($_POST['guests'])) {
					$gst_cnt = $_POST['guests'];
					if (substr($gst_cnt, 0, 8) == "combine_") {
						$gst_cnt = ($info['guests'] + (int)substr($gst_cnt, 8));
					}
					if ($gst_cnt != "target") {
						$o_vars['guests'] = $gst_cnt;
						$set_guests = ", `guests` = '[guests]'";
					}
				}

				if ($info['no_of_checks'] > 1) {
					$check_offset = (int)$info['no_of_checks'];
				}
				$merges = $info['merges'];
				if ($merges != "") {
					$merges .= "|";
				}
				$merges .= $_POST['from_order_id'];
				if (!empty($_POST['merges'])) {
					if ($merges != "") {
						$merges .= "|";
					}
					$merges .= $_POST['merges'];
				}
				$o_vars['merges'] = $merges;
				$update_order = lavu_query("UPDATE `orders` SET `merges` = '[merges]', `subtotal` = ((`subtotal` * 1) + [subtotal]), `tax` = ((`tax` * 1) + [tax]), `total` = ((`total` * 1) + [total])".$set_guests.$add_payments.$add_gratuity.", `no_of_checks` = ((`no_of_checks` * 1) + [added_checks]), `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]', `last_modified` = '[last_modified]', `last_mod_device` = '[last_mod_device]', `last_mod_register_name` = '[last_mod_register_name]' WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);
			}
		}

		if (isset($_POST['from_order_id'])) {

			if (isset($_POST['check_details'])) {

				$check_details_array = explode("|*|", $_POST['check_details']);
				for ($c = 0; $c < cappedCheckCount(count($check_details_array)); $c++) {

					$detail_array = explode("|o|", $check_details_array[$c]);
					
					$q_fields = "";
					$q_values = "";
					$q_update = "";
					$d_vars = array();
					$d_vars['discount'] = $detail_array[1];
					$d_vars['discount_sh'] = $detail_array[2];
					$d_vars['discount_value'] = $detail_array[3];
					$d_vars['discount_type'] = $detail_array[4];
					$d_vars['discount_id'] = $detail_array[5];
					$d_vars['discount_info'] = $detail_array[6];
					$d_vars['subtotal'] = $detail_array[7];
					$d_vars['tax'] = $detail_array[8];
					$d_vars['gratuity'] = $detail_array[9];
					$d_vars['check_total'] = $detail_array[10];
					$keys = array_keys($d_vars);
					foreach ($keys as $key) {
						if ($q_update != "") {
							$q_update .= ", ";
						}
						$q_update = "`$key` = '[$key]'";
					}
					$d_vars['loc_id'] = $_POST['loc_id'];
					$d_vars['order_id'] = $_POST['from_order_id'];
					$d_vars['check'] = $detail_array[0];
					$d_vars['ioid'] = $toIOID;

					$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' LIMIT 1", $_vars);
					if (mysqli_num_rows($check_for_details) > 0) {
						$d_vars['new_order_id'] = $_POST['order_id'];
						$update_details = lavu_query("UPDATE `split_check_details` SET $q_update, `order_id` = '[new_order_id]', `ioid` = '[ioid]', `check` = (`check` + $check_offset) WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $d_vars);

					} else {

						$d_vars['order_id'] = $_POST['order_id'];
						$d_vars['check'] = ((int)$detail_array[0] + $check_offset);
						$keys = array_keys($d_vars);
						foreach ($keys as $key) {
							if ($q_fields != "") {
								$q_fields .= ", ";
								$q_values .= ", ";
							}
							$q_fields .= "`$key`";
							$q_values .= "'[$key]'";
						}
						$create_details = lavu_query("INSERT INTO `split_check_details` ($q_fields) VALUES ($q_values)", $d_vars);
					}
				}
			}

			$move_payments = lavu_query("UPDATE `cc_transactions` SET `ioid` = '[1]', `order_id` = '[2]', `check` = (`check` + $check_offset) WHERE `loc_id` = '[3]' AND `order_id` = '[4]'", $toIOID, $_POST['order_id'], $_POST['loc_id'], $_POST['from_order_id']);
		}

		$order_contents = explode("|*|", $_POST['order_contents']);
		$error_occurred = false;

		$icid_index = array();

		foreach ($order_contents as $oc) {
			$order_item = explode("|o|", $oc);
			$q_fields = "";
			$q_values = "";
			$i_vars = array();
			$i_vars['loc_id'] = $_POST['loc_id'];
			$i_vars['order_id'] = $_POST['order_id'];
			$i_vars['item'] = $order_item[0];
			$i_vars['price'] = decimalize($order_item[1]);
			$i_vars['quantity'] = decimalize($order_item[2]);
			$i_vars['options'] = $order_item[3];
			$i_vars['special'] = $order_item[4];
			$i_vars['print'] = $order_item[5];
			$i_vars['modify_price'] = decimalize($order_item[6]);
			$i_vars['check'] = ($order_item[7] == "0")?"0":($order_item[7] + $check_offset);
			$i_vars['seat'] = $order_item[8];
			$i_vars['item_id'] = $order_item[9];
			$i_vars['printer'] = $order_item[10];
			$i_vars['apply_taxrate'] = $order_item[11];
			$i_vars['custom_taxrate'] = $order_item[12];
			$i_vars['modifier_list_id'] = $order_item[13];
			$i_vars['forced_modifier_group_id'] = $order_item[14];
			$i_vars['forced_modifiers_price'] = decimalize($order_item[15]);
			$i_vars['course'] = $order_item[16];
			$i_vars['open_item'] = $order_item[17];
			$i_vars['subtotal'] = (decimalize($order_item[1]) * decimalize($order_item[2]));
			$i_vars['allow_deposit'] = $order_item[18];
			$i_vars['deposit_info'] = $order_item[19];
			$i_vars['subtotal_with_mods'] = (decimalize($order_item[2]) * (decimalize($order_item[1]) + decimalize($order_item[6]) + decimalize($order_item[15])));
			$i_vars['void'] = $order_item[20];
			$i_vars['device_time'] = $order_item[21];
			$i_vars['notes'] = $order_item[22];
			$i_vars['hidden_data1'] = $order_item[23];
			$i_vars['hidden_data2'] = $order_item[24];
			$i_vars['hidden_data3'] = $order_item[25];
			$i_vars['hidden_data4'] = $order_item[26];
			$i_vars['hidden_data5'] = (isset($order_item[33])?$order_item[33]:"");
			$i_vars['hidden_data6'] = (isset($order_item[42])?$order_item[42]:"");
			$i_vars['tax_inclusion'] = $order_item[27];
			$i_vars['sent'] = $order_item[28];
			$i_vars['tax_exempt'] = $order_item[29];
			$i_vars['exemption_id'] = $order_item[30];
			$i_vars['exemption_name'] = $order_item[31];
			$i_vars['checked_out'] = $order_item[32];
			$i_vars['price_override'] = (isset($order_item[34])?$order_item[34]:"0");
			$i_vars['original_price'] = (isset($order_item[35])?$order_item[35]:"");
			$i_vars['override_id'] = (isset($order_item[36])?$order_item[36]:"0");
			$i_vars['idiscount_id'] = (isset($order_item[37])?$order_item[37]:"");
			$i_vars['idiscount_sh'] = (isset($order_item[38])?$order_item[38]:"");
			$i_vars['idiscount_value'] = (isset($order_item[39])?decimalize($order_item[39]):"");
			$i_vars['idiscount_type'] = (isset($order_item[40])?$order_item[40]:"");
			$i_vars['idiscount_amount'] = (isset($order_item[41])?decimalize($order_item[41]):"");
			$i_vars['idiscount_info'] = (isset($order_item[43])?$order_item[43]:"");
			if (isset($order_item[44])) {
				$i_vars['category_id'] = $order_item[44];
			}
			$i_vars['ioid'] = $toIOID;
			$i_vars['icid'] = (isset($order_item[46])?$order_item[46]:"");
			$keys = array_keys($i_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}

			$save_order_item = lavu_query("INSERT INTO `order_contents` ($q_fields) VALUES ($q_values)", $i_vars);
			if (!$save_order_item) {
				$error_occurred = true;
			}

			$icid_index[] = $i_vars['icid'];
		}

		$modifiers_used = (isset($_POST['modifiers_used']))?$_POST['modifiers_used']:"";
		if ($modifiers_used != "") {

			$order_modifiers = explode("|*|", $_POST['modifiers_used']);

			$get_current_row_count = lavu_query("SELECT COUNT(`id`) FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
			$row_count = mysqli_result($get_current_row_count, 0);

			foreach ($order_modifiers as $om) {

				$modifier_info = explode("|o|", $om);
				$q_fields = "";
				$q_values = "";
				$m_vars = array();
				$m_vars['loc_id'] = $_POST['loc_id'];
				$m_vars['order_id'] = $_POST['order_id'];
				$m_vars['mod_id'] = $modifier_info[0];
				$m_vars['type'] = $modifier_info[1];
				$m_vars['row'] = ($modifier_info[2] + $row_count);
				$m_vars['qty'] = $modifier_info[3];
				if (!empty($modifier_info[5])) {
					$icid = $modifier_info[5];
				} else {
					$icid = $icid_index[((int)$m_vars['row'] - 1)];
				}
				$m_vars['icid'] = $icid;
				if (isset($modifier_info[4])) {
					$unit_cost = $modifier_info[4];
				} else {
					$unit_cost = getModifierUnitCost($modifier_info[0], $modifier_info[1]);
				}
				$m_vars['cost'] = ($unit_cost * $modifier_info[3]);
				$m_vars['unit_cost'] = $unit_cost;
				$m_vars['ioid'] = $toIOID;
				$keys = array_keys($m_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") {
						$q_fields .= ", ";
						$q_values .= ", ";
					}
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$save_modifier_info = lavu_query("INSERT INTO `modifiers_used` ($q_fields) VALUES ($q_values)", $m_vars);
			}
		}
		
		$now_ts = time();

		if (!$error_occurred) {

			$q_update = "";
			$v_vars = array();
			$v_vars['cashier'] = $_REQUEST['server'];
			$v_vars['auth_by'] = $_REQUEST['server'];
			$v_vars['auth_by_id'] = $_REQUEST['server_id'];
			if (($void_info[3] == "") && ($void_info[4] == "")) {
				$v_vars['closed'] = $device_time;
				$v_vars['closing_device'] = $device_id;
			} else if (($void_info[3] != "") && ($void_info[4] == "")) {
				$v_vars['reclosed_datetime'] = $device_time;
				$v_vars['reclosing_device'] = $device_id;
			}
			$v_vars['order_status'] = "merged";
			$v_vars['void'] = "2";
			$v_vars['void_reason'] = "Merged with Order #".$_REQUEST['order_id'];
			$v_vars['last_mod_ts'] = $now_ts;
			$v_vars['pushed_ts'] = $now_ts;
			$v_vars['last_modified'] = $device_time;
			$v_vars['last_mod_device'] = $device_id;
			$v_vars['last_mod_register_name'] = (isset($_REQUEST['register_name']))?$_REQUEST['register_name']:"";
			$v_vars['active_device'] = "";
			$keys = array_keys($v_vars);
			foreach ($keys as $key) {
				if ($q_update != "") {
					$q_update .= ", ";
				}
				$q_update .= "`$key` = '[$key]'";
			}
			$v_vars['location_id'] = $_REQUEST['loc_id'];
			$v_vars['order_id'] = $_REQUEST['from_order_id'];
			$record_void = lavu_query("UPDATE `orders` SET $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $v_vars);

			$q_fields = "";
			$q_values = "";
			$log_vars = array();
			$log_vars['action'] = "Merged with Order #".$_REQUEST['order_id'];
			$log_vars['loc_id'] = $_REQUEST['loc_id'];
			$log_vars['order_id'] = $_REQUEST['from_order_id'];
			$log_vars['time'] = $device_time;
			$log_vars['user'] = $_REQUEST['server'];
			$log_vars['user_id'] = $_REQUEST['server_id'];
			$log_vars['device_udid'] = $device_id;
			$log_vars['details'] = "";
			$keys = array_keys($log_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

			$log_vars['action'] = "Order #".$_REQUEST['from_order_id']." Merged";
			$log_vars['order_id'] = $_REQUEST['order_id'];
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

			lavu_echo("1");

		} else {
			
			lavu_echo("0");
		}

	} else if ($mode == 18) { // clock server in - not in use by app - noted 2/22/2011

	} else if ($mode == 19) { // clock server out - not in use by app - noted 2/22/2011

	} else if ($mode == 20) { // reopen closed order

		mlavu_close_db();

		$auth_by = convertPIN($_REQUEST['PINused'], "name");
		$auth_by_id = convertPIN($_REQUEST['PINused']);

		$get_original_close_time = lavu_query("SELECT `closed` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		$extract = mysqli_fetch_assoc($get_original_close_time);

		$now_ts = time();

		$q_update = "";
		$o_vars = array();
		$o_vars['tablename'] = $_REQUEST['set_tablename']; //utf8_encode($_REQUEST['set_tablename']);
		$o_vars['order_status'] = "reopened";
		$o_vars['reopened_datetime'] = $device_time;
		$o_vars['reclosed_datetime'] = "";
		$o_vars['reopening_device'] = determineDeviceIdentifier($_REQUEST);
		$o_vars['reclosing_device'] = "";
		$o_vars['auth_by'] = $auth_by;
		$o_vars['auth_by_id'] = $auth_by_id;
		$o_vars['last_mod_ts'] = $now_ts;
		$o_vars['pushed_ts'] = $now_ts;
		$o_vars['last_modified'] = $device_time;
		$o_vars['last_mod_device'] = determineDeviceIdentifier($_REQUEST);
		$o_vars['last_mod_register_name'] = (isset($_REQUEST['register_name']))?$_REQUEST['register_name']:"";
		$keys = array_keys($o_vars);
		foreach ($keys as $key) {
			if ($q_update != "") {
				$q_update .= ", ";
			}
			$q_update .= "`$key` = '[$key]'";
		}
		$o_vars['location_id'] = $_REQUEST['loc_id'];
		$o_vars['order_id'] = $_REQUEST['order_id'];

		$update_order = lavu_query("UPDATE `orders` SET $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);

		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = "Order Reopened";
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = $_REQUEST['order_id'];
		$l_vars['time'] = $device_time;
		$l_vars['user'] = $auth_by;
		$l_vars['user_id'] = $auth_by_id;
		$l_vars['details'] = "Original close: ".$extract['closed'];
		$l_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		$result = "1";
		$result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);

		lavu_echo($result);

	} else if ($mode == 21) { // open cash drawer and log it, unless it is called from checkout with no receipt or when the cash button is pressed

		$response = logOpenCashDrawer($_REQUEST);

		lavu_echo($response);

	} else if ($mode == 22) { // log change to order after check has printed

		$response = logOrderChangeAfterCheckPrinted($_REQUEST);

		lavu_echo($response);

	} else if ($mode == 23) { // retrieve multiple check details - added to mode ?? on 4/16/2012, may be unnecessary in versions beyond 2.0.3

		mlavu_close_db();

		$check_details_array = array();

		$get_check_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' ORDER BY `check` ASC", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (mysqli_num_rows($get_check_details) > 0) {
			while ($data_obj = mysqli_fetch_object($get_check_details)) {
				$check_details_array[] = $data_obj;
			}

		} else {
			
			lavu_echo('{"json_status":"nr"}');
		}

		$response = cleanJSON('{"json_status":"success","check_details":'.json_encode($check_details_array).'}');

		lavu_echo($response);

	} else if ($mode == 24) { // save multiple check details during close, not in use as of v1.7.1 and moved to mode 10

	} else if ($mode == 25) { // save order in quick serve mode, not in use in app 11-01-2010

	} else if ($mode == 26) { // reload settings

		$flag_message = array();
	
		if ($company_info['disabled']=="1" || $company_info['disabled']=="2" || $company_info['disabled']=="3") {
		
			$flag_message = array(
				"json_status"	=> "LocationFlag",
				"flag_title"	=> speak("Account Inactive"),
				"flag_message"	=> speak("Please contact support."),
				"flag_kickout"	=> "2"
			);
		
		} else {
			
			$pay_stat = checkPaymentStatus();
			if (!empty($pay_stat['message'])) {
	
				$flag_message = array(
					"json_status"	=> "LocationFlag",
					"flag_title"	=> $pay_stat['title'],
					"flag_message"	=> $pay_stat['message'],
					"flag_kickout"	=> $pay_stat['kickout']?"1":"0"
				);
			}
		}

		if (isset($flag_message['flag_message'])) {

			// flag_kickout = 1 takes user to locations
			// flag_kickout = 2 takes user to welcome screen

			lavu_echo(cleanJSON(json_encode($flag_message)));

		} else {

			$user_data_array = array();
			$valid_PINs = "";
			$valid_admin_PINs = "";

			if ($client_app=="Lavu Deliver" || $client_app=="Lavu Maintenance") {
	
				$this_user_info = getERSuserInfo($_REQUEST['server_id']);
				if (count($this_user_info) > 0) {				
					$user_data_array[] = $this_user_info;
					$valid_PINs = "1234";
					$valid_admin_PINs = "1234";
				} else {
					lavu_echo('{"json_status":"NoU"}');
				}
				
			} else {

				$get_user_info = lavu_query("SELECT `id`, `quick_serve`, `access_level`, `PIN`, `f_name`, `l_name`, `role_id`, `_use_tunnel`, `username` FROM `users` WHERE `id` = '[1]'", $_REQUEST['server_id']);
				if (mysqli_num_rows($get_user_info) > 0) {
					$this_user_info = mysqli_fetch_assoc($get_user_info);
				} else {
					lavu_echo('{"json_status":"NoU"}');
				}
	
				$activeUserInfo = $this_user_info;
				$get_all_user_data = lavu_query("SELECT * FROM `users` WHERE `_deleted` != '1' ORDER BY `l_name`, `f_name` ASC");
				while ($data_obj = mysqli_fetch_object($get_all_user_data)) {
					$roles = explode(",", $data_obj->role_id);
					for ($roles_i = 0 ; $roles_i < count($roles); $roles_i++) {
						$role_parts = explode("|", $roles[$roles_i] );
						$roles[$roles_i] = $role_parts[0];
					}
					$data_obj->role_id = implode(",", $roles);
	
					$user_data_array[] = $data_obj;
					if ($data_obj->active == "1") {
						$valid_PINs .= "|".$data_obj->PIN."|";
						if ($data_obj->access_level > 1) {
							$valid_admin_PINs .= "|".$data_obj->PIN."|";
						}
					}
				}
			}

			$menu_id = "";
			$locations_array = loadLocations($_REQUEST['loc_id'], $data_name, $menu_id);

			$discount_types_array = array();
			$get_discount_types = lavu_query("SELECT * FROM `discount_types` WHERE `loc_id` = '[1]' AND `_deleted` != '1' ORDER BY `_order`, `title` ASC", $_REQUEST['loc_id']);
			while ($data_obj = mysqli_fetch_object($get_discount_types)) {
				$discount_types_array[] = $data_obj;
			}

			$reports_list_return_string = '"reports_list":[{"title":"Register Summary","mode":"register","filename":"till_report.php"},{"title":"Server Summary","mode":"server","filename":"till_report.php"}]';
			$printers_return_string = getSupportedPrinters($_REQUEST['loc_id']);
			$all_menu_data_return_string = loadMenuData($menu_id, $_REQUEST['loc_id'], "0");

			// load components if necessary
			$component_data_return_string = "";
			$comp_pack = (isset($_REQUEST['comp_pack']))?$_REQUEST['comp_pack']:"0";
			if (isset($_REQUEST['comp_pack_number'])) {
				switch ($_REQUEST['comp_pack_number']) {
					case 1:
						$comp_pack = $location_info['component_package'];
						break;
					case 2:
						$comp_pack = $location_info['component_package2'];
						break;
					case 3:
						$comp_pack = $location_info['component_package3'];
						break;
					default:
						break;
				}
			}
			if ($comp_pack!="0" && $comp_pack!="") {
				$component_data_return_string = loadComponents($comp_pack, $active_modules);
			}

			$tack_on_build = ":".$_REQUEST['build'];

			$q_fields = "";
			$q_values = "";
			$log_vars = array();
			$log_vars['action'] = "Settings Reloaded";
			$log_vars['loc_id'] = $_REQUEST['loc_id'];
			$log_vars['order_id'] = "0";
			$log_vars['time'] = $device_time;
			$log_vars['server_time'] = gmdate("Y-m-d H:i:s")."Z";
			$log_vars['user'] = $this_user_info['f_name']." ".$this_user_info['l_name'];
			$log_vars['user_id'] = $this_user_info['id'];
			$log_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
			$log_vars['details'] = $client_app;
			$keys = array_keys($log_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields) VALUES ($q_values)", $log_vars);

			$check_field = "UDID";
			if (isset($_REQUEST['UUID'])) {
				$check_field = "UUID";
			} else if (isset($_REQUEST['MAC'])) {
				$check_field = "MAC";
			}

			$prefix_and_next_offline_order_id = "";
			if ($_REQUEST['UUID']) {
				$device_prefix = determineDevicePrefix($_REQUEST['loc_id'], $_REQUEST['UUID'], $_REQUEST['app_name'], $_REQUEST['model']);
				$prefix_digits = MIN(MAX((int)reqvar("prefix_digits", "2"), 2), 4);
				$full_prefix = "3".str_pad($device_prefix, $prefix_digits, "0", STR_PAD_LEFT)."-";
				$next_offline_order_id = str_replace($full_prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $_REQUEST['loc_id'], $full_prefix));			
				$prefix_and_next_offline_order_id = ',"device_prefix":"'.$device_prefix.'","next_offline_order_id":"'.$next_offline_order_id.'"';
			}

			$json_return = '{"json_status":"success"'.$prefix_and_next_offline_order_id.',"company_code":"'.lsecurity_name($company_info['company_code'], $company_info['id']).$tack_on_build.'","logo_img":"'.$company_info['logo_img'].'","receipt_logo_img":"'.$company_info['receipt_logo_img'].'","dev":"'.$company_info['dev'].'","demo":"'.$company_info['demo'].'","company_id":"'.$company_info['id'].'","company_name":"'.$company_info['company_name'].'","valid_PINs":"'.$valid_PINs.'","valid_admin_PINs":"'.$valid_admin_PINs.'","location":'.json_encode($locations_array).',"all_user_data":'.json_encode($user_data_array).',"quick_serve":"'.$this_user_info['quick_serve'].'","access_level":"'.$this_user_info['access_level'].'","PIN":"'.$this_user_info['PIN'].'","f_name":"'.$this_user_info['f_name'].'","l_name":"'.$this_user_info['l_name'].'","discount_types":'.json_encode($discount_types_array).','.$reports_list_return_string.','.$printers_return_string.','.$all_menu_data_return_string.$component_data_return_string.',"available_language_packs":'.getAvailableLanguagePacks().',"language_pack":'.generateLanguagePack($_REQUEST['loc_id'], $location_info['use_language_pack']).'}';

			session_destroy();

			lavu_echo(cleanJSON($json_return));
		}

	} else if ($mode == 27) { // reload location settings (net_path, use_net_path, net_path_print) only

		session_destroy();

		mlavu_close_db();

		$locations_array = array();

		$get_net_paths = lavu_query("SELECT `net_path`, `use_net_path`, `net_path_print` FROM `locations` WHERE `id` = '[1]' LIMIT 1", $_REQUEST['loc_id']);
		if (mysqli_num_rows($get_net_paths) > 0) {
			$extract = mysqli_fetch_assoc($get_net_paths);

			$set_use_net_path = $extract['use_net_path'];
			if ($set_use_net_path == "2") {
				$set_use_net_path = "1";
			}

			lavu_echo('{"json_status":"success","PHPSESSID":"","net_path":"'.$extract['net_path'].'","use_net_path":"'.$set_use_net_path.'","net_path_print":"'.$extract['net_path_print'].'"}');

		} else {

			lavu_echo('{"json_status":"NoL","PHPSESSID":""}');
		}

	} else if ($mode == 28) { // save order (info, contents, and modifier usage), combines modes 5 and 17, should replace them altogether in the future, returns order id

		$result = saveOrder($_REQUEST);
		$result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);

		lavu_echo($result);

	} else if ($mode == 29) { // quick query to create a blank order and return the order_id

		mlavu_close_db();

		$device_prefix = "";
		if ($location_info['use_device_prefix'] && $location_info['use_device_prefix']=="1") {

			$check_field = "UDID";
			if (isset($_REQUEST['UUID'])) {
				$check_field = "UUID";
			} else if (isset($_REQUEST['MAC'])) {
				$check_field = "MAC";
			}

			$get_device_prefix = lavu_query("SELECT `prefix` FROM `devices` WHERE `UDID` = '$check_field'", $_REQUEST[$check_field]);
			if (mysqli_num_rows($get_device_prefix) > 0) {
				$device_info = mysqli_fetch_assoc($get_device_prefix);
				$device_prefix = str_pad((string)$device_info['prefix'], 2, "0", STR_PAD_LEFT);
			}
		}
		$new_id = assignNextWithPrefix("order_id", "orders", "location_id", $_REQUEST['loc_id'], $server_order_prefix.$device_prefix."-");
		$create_order = lavu_query("INSERT INTO `orders` (`order_id`, `location`, `location_id`, `register`) VALUES ('[1]', '[2]', '[3]', '[4]')", $new_id, $location_info['title'], $_REQUEST['loc_id'], $_REQUEST['register']);
		if (!$create_order) {
			$new_id = "";
		}

		lavu_echo($new_id);

	} else if ($mode == 30) { // load orders with open deposits

		mlavu_close_db();

		$now_parts = explode(" ", $device_time);
		$now_date = explode("-", $now_parts[0]);
		$now_time = explode(":", $now_parts[1]);

		$three_months_ts = mktime($now_time[0], $now_time[1], $now_time[2], ($now_date[1] - 3), $now_date[2], $now_date[0]);
		$three_months_ago = date("Y-m-d H:i:s", $three_months_ts);

		$order_info = array();
		$order_contents = array();
		$check_details = array();

		$limit_string = "";
		if (isset($_REQUEST['page'])) {
			$limit_string = " LIMIT ".(((int)$_REQUEST['page'] - 1) * 20).", 20";
		}

		$ms_vars = array();
		$ms_vars['loc_id'] = $_REQUEST['loc_id'];

		$filter = " AND `deposit_status` != '0' AND (not_closed_or_reopened) AND `opened` != '0000-00-00 00:00:00' AND `opened` != ''";
		if (!empty($_REQUEST['filter'])) {
			$filter .= str_replace("= '[server_prefix]-", "= '", urldecode($_REQUEST['filter']));
			if (strstr($filter, "[searchOrderContents::")) {
				$position1 = (strpos($filter, "[searchOrderContents::") + 22);
				$position2 = strpos($filter, "]");
				$search_string = substr($filter, $position1, ($position2 - $position1));
				$s_vars = array();
				$s_vars['loc_id'] = $_REQUEST['loc_id'];
				$s_vars['three_months_ago'] = $three_months_ago;
				$s_vars['search_string'] = $search_string;
				$matching_ids = "";
				$perform_search = lavu_query("SELECT `order_contents`.`order_id` FROM `orders` LEFT JOIN `order_contents` ON `order_contents`.`order_id` = `orders`.`order_id` WHERE `orders`.`order_id` NOT LIKE '777%' AND `orders`.`opened` >= '[three_months_ago]' AND `orders`.`location_id` = '[loc_id]' AND `order_contents`.`loc_id` = `orders`.`location_id` AND (`order_contents`.`item` LIKE '%[search_string]%' OR `order_contents`.`deposit_info` LIKE '%[search_string]%' OR `order_contents`.`options` LIKE '%[search_string]%' OR `order_contents`.`special` LIKE '%[search_string]%') GROUP BY `orders`.`order_id`", $s_vars);
				while ($matching_id = mysqli_fetch_assoc($perform_search)) {
					if ($matching_ids != "") {
						$matching_ids .= ",";
					}
					$matching_ids .= "'".$matching_id['order_id']."'";
				}
				if ($matching_ids != "") {
					$matching_ids = "`order_id` IN (".$matching_ids.") OR ";
				}
				$matching_ids .= "SUBSTRING(`opened`, 6, 5) LIKE '%[search_string]%' OR `togo_name` LIKE '%[search_string]%' OR `email` LIKE '%[search_string]%' OR `alt_tablename` LIKE '%[search_string]%' OR `tablename` LIKE '%[search_string]%' OR `tabname` LIKE '%[search_string]%'";
				$ms_vars['search_string'] = $search_string;
				$filter = str_replace("[searchOrderContents::".$search_string."]", $matching_ids, $filter);
			}
		}

		$for_server_id = (!empty($_REQUEST['for_server_id']))?$_REQUEST['for_server_id']:"all";

		if ($for_server_id != "all") {
			$filter .= " AND `server_id` = '[server_id]'";
			$ms_vars['server_id'] = $for_server_id;
		}

		$get_count = lavu_query("SELECT COUNT(`id`) FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '0'".str_replace("(not_closed_or_reopened)", "`closed` = '0000-00-00 00:00:00'", $filter)." UNION SELECT COUNT(`id`) FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '0'".str_replace("(not_closed_or_reopened)", "`reopened_datetime` != '' AND `reclosed_datetime` = ''", $filter), $ms_vars);
		$total_count = 0;
		for ($r = 0; $r < mysqli_num_rows($get_count); $r++) {
			$total_count += mysqli_result($get_count, $r);
		}

		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '0'".str_replace("(not_closed_or_reopened)", "`closed` = '0000-00-00 00:00:00'", $filter)." UNION SELECT * FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `void` = '0'".str_replace("(not_closed_or_reopened)", "`reopened_datetime` != '' AND `reclosed_datetime` = ''", $filter)." ORDER BY `opened` DESC".$limit_string, $ms_vars);
		if (mysqli_num_rows($get_orders) > 0) {
		
			$order_ids = array();
			while ($data_obj = mysqli_fetch_object($get_orders)) {
	
				$order_info[] = $data_obj;
				$order_ids[] = $data_obj->order_id;
				
				$no_of_checks = cappedCheckCount($data_obj->no_of_checks);
	
				$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' ORDER BY `check` ASC", $_REQUEST['loc_id']);
				if (mysqli_num_rows($get_details) < $no_of_checks) {
					for ($c = 1; $c <= $no_of_checks; $c++) {
						$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $_REQUEST['loc_id'], $data_obj->order_id, $c);
						if (mysqli_num_rows($check_for_details) == 0) {
							$insert = lavu_query("INSERT INTO `split_check_details` (`loc_id`, `order_id`, `check`) VALUES ('[1]', '[2]', '[3]')", $_REQUEST['loc_id'], $data_obj->order_id, $c);
						}
					}
					$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '".$data_obj->order_id."' ORDER BY `check` ASC", $_REQUEST['loc_id']);
				}
				while ($data_obj2 = mysqli_fetch_object($get_details)) {
					$check_details[] = $data_obj2;
				}
			}
			$order_id_str = "'".implode("','", $order_ids)."'";
			
			if (count($order_ids) > 0) {
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` IN (".$order_id_str.")", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}
				$voided_cct_filter = ((isset($_REQUEST['pull_voided_ccts'])) && $_REQUEST['pull_voided_ccts']=="1")?"":" AND `voided` = '0'";
				$get_payments_and_refunds = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` IN (".$order_id_str.")".$voided_cct_filter." AND (`action` = 'Sale' OR `action` = 'Refund') AND `tip_for_id` = '0'", $_REQUEST['loc_id']);
				while ($data_obj2 = mysqli_fetch_object($get_payments_and_refunds)) {
					$recorded_payments[] = $data_obj2;
				}
			}

		} else {
			
			lavu_echo('{"json_status":"NoMatch","PHPSESSID":"'.session_id().'"}');
		}

		lavu_echo(cleanJSON('{"json_status":"success","PHPSESSID":"'.session_id().'","order_info":'.json_encode($order_info).',"order_contents":'.json_encode($order_contents).',"check_details":'.json_encode($check_details).',"recorded_payments":'.json_encode($recorded_payments).',"total_count":"'.$total_count.'"}'));

	} else if ($mode == 31) { // close open deposit as unpaid

		// candidate for deprecation from version 2.1+

		mlavu_close_db();

		$get_items = lavu_query("SELECT `id`, `deposit_info`  FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `allow_deposit` != '0'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (mysqli_num_rows($get_items) > 0) {
			while ($extract = mysqli_fetch_assoc($get_items)) {
				$update_item = lavu_query("UPDATE `order_contents` SET `deposit_info` = '[1]' WHERE `id` = '[2]'", $extract['deposit_info']."\nCLOSED AS UNPAID on ".$_REQUEST['now']." by ".$_REQUEST['server'], $extract['id']);
			}

			$u_vars = array();
			$u_vars['last_mod_ts'] = time();
			$u_vars['last_modified'] = determineDeviceTime($location_info, $_REQUEST);
			$u_vars['device_id'] = "JC FUNCTIONS";
			$u_vars['loc_id'] = $_REQUEST['loc_id'];
			$u_vars['order_id'] = $_REQUEST['order_id'];
			
			$update_deposit_status = lavu_query("UPDATE `orders` SET `deposit_status` = '5', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]', `last_modified` = '[last_modified]', `last_mod_device` = '[device_id]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $u_vars);

			lavu_echo("1");

		} else {

			lavu_echo("0");
		}

	} else if ($mode == 32) { // load credit card transaction details and recorded payments and refunds

		mlavu_close_db();

		$all_transactions_array = array();

		$voided_cct_filter = ((isset($_REQUEST['pull_voided_ccts'])) && $_REQUEST['pull_voided_ccts']=="1")?"":" AND `voided` = '0'";
		$get_cc_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'".$voided_cct_filter." AND `action` != 'Void' AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (mysqli_num_rows($get_cc_transactions) > 0) {
			while ($data_obj = mysqli_fetch_object($get_cc_transactions)) {
				$all_transactions_array[] = $data_obj;
			}
		}

		lavu_echo(cleanJSON('{"json_status":"success","cc_transactions":'.json_encode($all_transactions_array).'}'));

	} else if ($mode == 33) { // save credit card transaction details, not in use by app, occurs in the gateway functions script

	} else if ($mode == 34) { // record refund, not in use by app, refunds recorded as transactions with details included

	} else if ($mode == 35) { // retrieve item by UPC, change quantity for qmode = inventoryQuantities

		mlavu_close_db();

		$get_item = lavu_query("SELECT * FROM `menu_items` WHERE `UPC` = '[1]' LIMIT 1", $_REQUEST['UPC']);
		if (mysqli_num_rows($get_item) > 0) {

			if ($_REQUEST['smode'] == "order") {

				$data_obj = mysqli_fetch_object($get_item);
				lavu_echo('{"json_status":"add_to_order","item_info":'.json_encode($data_obj).'}');

			} else if ($_REQUEST['smode'] == "inventoryItems") {

				$extract = mysqli_fetch_assoc($get_item);
				lavu_echo(cleanJSON('{"json_status":"item_info","item_id":"'.$extract['id'].'","item_name":"'.$extract['name'].'","item_price":"'.$extract['price'].'","description":"'.$extract['description'].'","count":"'.$extract['inv_count'].'"}'));

			} else if ($_REQUEST['smode'] == "inventoryQuantities") {

				$extract = mysqli_fetch_assoc($get_item);

				$quantity_adj = (isset($_REQUEST['q_adj']))?$_REQUEST['q_adj']:"1";
				if ($_REQUEST['qmode'] == "minus") {
					$new_quantity = ($extract['inv_count'] - $quantity_adj);
				} else {
					$new_quantity = ($extract['inv_count'] + $quantity_adj);
				}

				$update_item = lavu_query("UPDATE `menu_items` SET `inv_count` = '[1]', last_modified_date = now() WHERE `id` = '[2]'", $new_quantity, $extract['id']);

				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `time`, `user`, `user_id`, `server_time`, `item_id`) VALUES ('Inventory Adjustment: ".$_REQUEST['qmode']." ".$quantity_adj."', '".$_REQUEST['loc_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now(), '".$extract['id']."')");

				lavu_echo(cleanJSON('{"json_status":"inventory_change","item_name":"'.$extract['name'].'","old_quantity":"'.$extract['inv_count'].'","new_quantity":"'.$new_quantity.'"}'));
			}

		} else {

			if ($_REQUEST['smode'] == "inventoryItems") {
				lavu_echo('{"json_status":"item_info","item_id":"0","item_name":"NEW ITEM","item_price":"","description":"","count":"0"}');
			} else {
				lavu_echo('{"json_status":"not_found"}');
			}
		}

	} else if ($mode == 36) { // clear order and contents, called after adding contents to existing tab

		// candidate for deprecation from version 2.0.2+

		mlavu_close_db();

		//$clear_order = lavu_query("UPDATE `orders` SET `opened` = '0000-00-00 00:00:00' WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		$clear_order = lavu_query("DELETE FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		$clear_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);

		lavu_echo("1");

	} else if ($mode == 37) { // save or create item from barcode scan (linea pro)

		mlavu_close_db();

		$i_vars = array();
		$i_vars['category_id'] = "112";
		$i_vars['name'] = $_REQUEST['iname'];
		$i_vars['price'] = priceFormat($_REQUEST['iprice']);
		$i_vars['description'] = $_REQUEST['idesc'];
		$i_vars['inv_count'] = $_REQUEST['icount'];
		$q_update = "";
		$keys = array_keys($i_vars);
		foreach ($keys as $key) {
			if ($q_update != "") {
				$q_update .= ", ";
			}
			$q_update .= "`$key` = '[$key]'";
		}
		$i_vars['id'] = $_REQUEST['iid'];

		if ($_REQUEST['iid'] == "0") {

			$i_vars['menu_id'] = $location_info['menu_id'];
			$i_vars['UPC'] = $_REQUEST['UPC'];
			$q_fields = "";
			$q_values = "";
			$keys = array_keys($i_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}

			$create_item = lavu_query("INSERT INTO `menu_items` ($q_fields) VALUES ($q_values)", $i_vars);
			$new_id = lavu_insert_id();

			lavu_echo('{"json_status":"item_created","new_id":"'.$new_id.'"}');

		} else {

			$update_item = lavu_query("UPDATE `menu_items` SET $q_update WHERE `id` = '[id]'", $i_vars);
			
			lavu_echo('{"json_status":"item_saved"}');
		}

	} else if ($mode == 38) { // nullify needs_reload field

		mlavu_close_db();

		$check_field = "UDID";
		if (isset($_REQUEST['UUID'])) {
			$check_field = "UUID";
		} else if (isset($_REQUEST['MAC'])) {
			$check_field = "MAC";
		}

		lavu_query("UPDATE `devices` SET `needs_reload` = '0' WHERE `$check_field` = '[1]'", $_REQUEST[$check_field]);

		lavu_echo("1");

	} else if ($mode == 39) { // clear payments and refunds

	} else if ($mode == 40) { // a new, better way of recording payments and discounts

        $result = savePaymentsAndDiscounts($_REQUEST);
		process_jc_addons($jc_addons, $active_modules, $mode, "after", $_REQUEST);
        
		lavu_echo($result);

	} else if ($mode == 41) { // log payment reassignment after item movement results in check disappearance, also updates check numbers for integrated card transactions

		$response = logPaymentReassignments($_REQUEST);

		lavu_echo($response);

	} else if ($mode == 42) { // save single payment or refund and return new row id

		$result = saveSinglePayment($_REQUEST);
		process_jc_addons($jc_addons, $active_modules, $mode, "after", $_REQUEST);		

		lavu_echo($result);

	} else if ($mode == 43) { // a new better way of emailing receipts - version 1.7 and later

		require_once(__DIR__."/../email_func.php");

		$post_vars = $_REQUEST;

		$response = sendEmailReceipt($post_vars);

		lavu_echo($response);

	} else if ($mode == 44) { // load language pack and set location language pack id

		$pack_id = (isset($_REQUEST['pack_id']))?$_REQUEST['pack_id']:"1";

		/*if ($_REQUEST['loc_id'] != "") {
			$update_location = lavu_query("UPDATE `locations` SET `use_language_pack` = '[1]' WHERE `id` = '[2]'", $pack_id, $_REQUEST['loc_id']);
		}*/

		lavu_echo(cleanJSON('{"json_status":"success","language_pack":'.generateLanguagePack($_REQUEST['loc_id'], $pack_id).'}'));

	} else if ($mode == 45) { // load alternate payment totals for an order - obsolete after verison 2.1 (moved to mode 4)

		mlavu_close_db();

		$jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));

		$return_string = "";
		$checks_array = array();

		$get_alternate_payment_totals = lavu_query("SELECT * FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' ORDER BY `check` ASC", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (mysqli_num_rows($get_alternate_payment_totals) > 0) {
			while ($extract = mysqli_fetch_assoc($get_alternate_payment_totals)) {
				if (!array_key_exists($extract['check'], $checks_array)) {
					$checks_array[$extract['check']] = array();
				}
				$checks_array[$extract['check']][] = '{"pay_type_id":"'.$extract['pay_type_id'].'","type":"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['type']).'","amount":"'.$extract['amount'].'","tip":"'.$extract['tip'].'","refund_amount":"'.$extract['refund_amount'].'","ioid":"'.$extract['ioid'].'","check":"'.$extract['check'].'"}';
			}

			$keys = array_keys($checks_array);
			foreach ($keys as $check) {
				$return_string .= ',"'.$check.'":['.join(",",$checks_array[$check]).']';
			}
		}

		lavu_echo(cleanJSON('{"json_status":"success"'.$return_string.'}'));

	} else if ($mode == 46) { // void a payment or refund

		mlavu_close_db();

		$auth_by = convertPIN($_REQUEST['PINused'], "name");
		$auth_by_id = convertPIN($_REQUEST['PINused']);

		$q_filter = "";
		$u_vars = array();
		$u_vars['loc_id'] = $_REQUEST['loc_id'];
		$u_vars['order_id'] = $_REQUEST['order_id'];
		$u_vars['check'] = $_REQUEST['check'];
		$u_vars['pay_type'] = $_REQUEST['pay_type'];
		if (isset($_REQUEST['pay_type_id'])) {
			$u_vars['pay_type_id'] = $_REQUEST['pay_type_id'];
		}
		$u_vars['action'] = $_REQUEST['action'];
		$keys = array_keys($u_vars);
		foreach ($keys as $key) {
			$q_filter .= " AND `$key` = '[$key]'";
		}
		$u_vars['amount'] = decimalize($_REQUEST['amount']);
		$u_vars['total_collected'] = decimalize($_REQUEST['total_collected']);
		$u_vars['tip_amount'] = decimalize($_REQUEST['tip_amount']);
		if ($u_vars['tip_amount'] == "") {
			$u_vars['tip_amount'] = "0";
		}
		$get_payment = lavu_query("SELECT `id`, `total_collected`, `pay_type_id` FROM `cc_transactions` WHERE `voided` = '0'$q_filter AND (`amount` * 1) = ('[amount]' * 1) LIMIT 1", $u_vars);
		if (mysqli_num_rows($get_payment) > 0) {
			$id_info = mysqli_fetch_assoc($get_payment);
			$void_payment = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]' WHERE `id` = '[3]'", $_REQUEST['reason'], $auth_by_id, $id_info['id']);
			$u_vars['total_collected'] = decimalize($id_info['total_collected']);
			if (!isset($_REQUEST['pay_type_id'])) {
				$u_vars['pay_type_id'] = $id_info['pay_type_id'];
			}
		}
		$u_vars['last_mod_ts'] = time();
		$u_vars['last_modified'] = $device_time;
		$u_vars['last_mod_device'] = determineDeviceIdentifier($_REQUEST);
		$u_vars['last_mod_register_name'] = (isset($_REQUEST['register_name']))?$_REQUEST['register_name']:"";

		$update_alt_pay_total = "";
		if (($u_vars['action'] == "Sale") && ($u_vars['pay_type_id'] == "1")) {
			$update_order = " `cash_paid` = (`cash_paid` - [total_collected]), `cash_applied` = (`cash_applied` - [total_collected])";
			$update_check = " `cash` = (`cash` - [total_collected]), `cash_applied` = (`cash_applied` - [total_collected])";
			$c_fields = "`cash`, `cash_applied`";
			$c_values = "'[total_collected]', '[total_collected]'";
		} else if (($u_vars['action'] == "Refund") && ($u_vars['pay_type_id'] == "1")) {
			$update_order = " `cash_paid` = (`cash_paid` + [amount]), `cash_applied` = (`cash_applied` + [amount]), `refunded` = (`refunded` - [amount])";
			$update_check = " `cash` = (`cash` + [amount]), `cash` = (`cash` + [amount]), `refund` = (`refund` - [amount])";
			$c_fields = "`cash`, `cash_applied`, `refunded`";
			$c_values = "'[amount]', '[amount]', '[amount]'";
		} else if (($u_vars['action'] == "Sale") && ($u_vars['pay_type_id'] == "2")) {
			$update_order = " `card_paid` = (`card_paid` - [amount]), `card_gratuity` = (`card_gratuity` - [tip_amount])";
			$update_check = " `card` = (`card` - [amount])";
			$c_fields = "`card`";
			$c_values = "'[amount]'";
		} else if (($u_vars['action'] == "Refund") && ($u_vars['pay_type_id'] == "2")) {
			$update_order = " `card_paid` = (`card_paid` + [amount]), `refunded_cc` = (`refunded_cc` - [amount]), `card_gratuity` = (`card_gratuity` + [tip_amount])";
			$update_check = " `card` = (`card` + [amount]), `refund_cc` = (`refund_cc` - [amount])";
			$c_fields = "`card`, `refund_cc`";
			$c_values = "'[amount]', '[amount]'";
		} else if (($u_vars['action'] == "Sale") && ($u_vars['pay_type_id'] == "3")) {
			$update_order = " `gift_certificate` = (`gift_certificate` - [amount])";
			$update_check = " `gift_certificate` = (`gift_certificate` - [amount])";
			$c_fields = "`gift_certificate`";
			$c_values = "'[amount]'";
		} else if (($u_vars['action'] == "Refund") && ($u_vars['pay_type_id'] == "3")) {
			$update_order = " `gift_certificate` = (`gift_certificate` + [amount]), `refunded_gc` = (`refunded_gc` - [amount])";
			$update_check = " `gift_certificate` = (`gift_certificate` + [amount]), `refund_gc` = (`refund_gc` - [amount])";
			$c_fields = "`gift_certificate`, `refund_gc`";
			$c_values = "'[amount]', '[amount]'";
		} else if ($u_vars['action'] == "Sale") {
			$update_order = " `alt_paid` = (`alt_paid` - [amount])";
			$update_check = " `alt_paid` = (`alt_paid` - [amount])";
			$c_fields = "`alt_paid`";
			$c_values = "'[amount]'";
			$update_alt_pay_total = "`amount` = (`amount` - [amount])";
		} else if ($u_vars['action'] == "Refund") {
			$update_order = " `alt_paid` = (`alt_paid` + [amount]), `alt_refunded` = (`alt_refunded` - [amount])";
			$update_check = " `alt_paid` = (`alt_paid` + [amount]), `alt_refunded` = (`alt_refunded` - [amount])";
			$c_fields = "`alt_paid`, `alt_refunded`";
			$c_values = "'[amount]', '[amount]'";
			$update_alt_pay_total = "`refund_amount` = (`refund_amount` - [amount])";
		}
		$update_order .= ", `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]', `last_modified` = '[last_modified]', `last_mod_device` = '[last_mod_device]', `last_mod_register_name` = '[last_mod_register_name]'";

		$update_o = lavu_query("UPDATE `orders` SET $update_order WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $u_vars);
		$update_c = lavu_query("UPDATE `split_check_details` SET $update_check WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $u_vars);
		if (($update_alt_pay_total != "") && isset($id_info)) {
			$u_vars['pay_type_id'] = $id_info['pay_type_id'];
			$update_apt = lavu_query("UPDATE `alternate_payment_totals` SET $update_alt_pay_total WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `pay_type_id` = '[pay_type_id]'", $u_vars);
		}

		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = (($_REQUEST['action'] == "Sale")?"Payment":"Refund")." Voided";
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = $_REQUEST['order_id'];
		$l_vars['check'] = $_REQUEST['check'];
		$l_vars['time'] = $device_time;
		$l_vars['user'] = $auth_by;
		$l_vars['user_id'] = $auth_by_id;
		$l_vars['details'] = "Check ".$_REQUEST['check']." - ".$_REQUEST['pay_type']." - ".priceFormat($_REQUEST['amount']);
		$l_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		lavu_echo("1");

	} else if ($mode == 47) { // device check in to replace call to print/terminal_list/index.php

		// device_checkin index
		//
		// 0 - device name
		// 1 - model
		// 2 - system name
		// 3 - system version
		// 4 - UDID // no longer used as of 3/24/12 - instead use MAC or UUID - MAC defunct with iOS 7
		// 5 - poslavu version
		// 6 - poslavu build
		// 7 - company id
		// 8 - app name
		// 9 - device ip address
		// 10 - device time

		require_once(dirname(__FILE__)."/../deviceValidations.php");
		$reseller_admin = isResellerAdminDevice($_REQUEST['UUID']);

		$num = rand(0, 99);

		$allow_ULA = true;
		if (sessvar("LULA") && (time() < ((int)sessvar("LULA") + 3600))) $allow_ULA = false; //  && (isset($recvd_sess_id)?$recvd_sess_id:"")!=""

		$device_prefix = "";
		$needs_reload = false;

		$device_checkin = explode("||", $_POST['device_checkin']);
		$device_model = $device_checkin[1];
		$default_usage_type = (strstr($device_model, "iPad"))?"terminal":"tableside";
		$usage_type = (isset($_REQUEST['usage_type']))?$_REQUEST['usage_type']:$default_usage_type;

		$json_status = "success";
		$locationFlag = false;
		$lfTitle = "";
		$lfMessage = "";
		$lfKickout = 0;

		if ($allow_ULA || $lls) {

			set_sessvar("LULA", time());

			$ipaddress = (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];

			$vars = array( "reseller_admin"=>"0" );
			if (count($device_checkin) >= 8) {
				$app_name = (isset($device_checkin[8]))?$device_checkin[8]:"POS Lavu Client";
				$vars['dn'] = $data_name;
				$vars['loc_id'] = $_REQUEST['loc_id'];
				for ($x = 0; $x <= 11; $x++) {
					if (isset($device_checkin[$x])) {
						$vars['device_checkin_'.$x] = $device_checkin[$x];
					} else {
						$vars['device_checkin_'.$x] = "";
					}
				}
				$vars['app_name'] = $app_name;
				$vars['UUID'] = (isset($_REQUEST['UUID']))?$_REQUEST['UUID']:"";
				$vars['MAC'] = (isset($_REQUEST['MAC']))?$_REQUEST['MAC']:"";
				$vars['ipaddress'] = $ipaddress;
				$vars['lavu_admin'] = (isset($_REQUEST['lavu_admin']))?$_REQUEST['lavu_admin']:"0";
				$vars['usage_type'] = $usage_type;
				$vars['reseller_admin'] = $reseller_admin?1:0;

				$check_filter = "`UDID` = '[device_checkin_4]'";
				$data_field = "[device_checkin_4]";
				$use_udid = true;
				if (isset($_REQUEST['UUID'])) {
					$check_filter = "`UUID` = '[UUID]'";
					$data_field = "[UUID]";
					$use_udid = false;
				} else if (isset($_REQUEST['MAC'])) {
					$check_filter = "`MAC` = '[MAC]'";
					$data_field = "[MAC]";
					$use_udid = false;
				}

				register_connect("jc_inc", $company_info['id'], $data_name, $_REQUEST['loc_id'], $device_checkin[1], $_REQUEST['UUID']);

				$check_record = lavu_query("SELECT `id`, `loc_id`, `prefix`, `system_version`, `poslavu_version`, `app_name`, `usage_type`, `needs_reload`, `inactive`, `first_checkin` FROM `devices` WHERE $check_filter", $vars);
				$matches = mysqli_num_rows($check_record);
				if ($matches > 0) {
					$extract = mysqli_fetch_assoc($check_record);
					$vars['id'] = $extract['id'];
					$vars['prefix'] = $extract['prefix'];

					if ($device_checkin[3] != $extract['system_version']) {
						$vars['old_system_version'] = $extract['system_version'];
						$vars['local_time'] = $device_time;
						$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('iOS version changed', now(), '[local_time]', '0', '[ipaddress]', '$data_field', '[device_checkin_0]', 'from [old_system_version]', 'to [device_checkin_3]', '[loc_id]')", $vars);
					}

					if (($device_checkin[5]." (".$device_checkin[6].")" != $extract['poslavu_version']) && ($app_name == $extract['app_name'])) {
						$vars['old_poslavu_version'] = $extract['poslavu_version'];
						$vars['local_time'] = $device_time;
						$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('[app_name] version changed', now(), '[local_time]', '0', '[ipaddress]', '$data_field', '[device_checkin_0]', 'from [old_poslavu_version]', 'to [device_checkin_5] ([device_checkin_6])', '[loc_id]')", $vars);
					}

					if ($vars['usage_type'] != $extract['usage_type']) {
						$vars['old_usage_type'] = $extract['usage_type'];
						$vars['local_time'] = $device_time;
						$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('Usage type changed', now(), '[local_time]', '0', '[ipaddress]', '$data_field', '[device_checkin_0]', 'from [old_usage_type]', 'to [usage_type]', '[loc_id]')", $vars);
					}

					if ($app_name != $extract['app_name']) {
						$vars['local_time'] = $device_time;
						$vars['from_app'] = $extract['app_name'];
						$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('Switched apps', now(), '[local_time]', '0', '[ipaddress]', '$data_field', '[device_checkin_0]', 'from [from_app]', 'to [app_name]', '[loc_id]')", $vars);
					}

					$clear_holding_order_id = "";
					if ($vars['loc_id'] != $extract['loc_id']) {
						$vars['local_time'] = $device_time;
						$vars['prefix'] = getLowestPrefix($vars['loc_id']);
						$vars['from_location'] = getFieldInfo($extract['loc_id'], "id", "locations", "title");
						$vars['to_location'] = $location_info['title'];
						$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('Switched locations', now(), '[local_time]', '0', '[ipaddress]', '$data_field', '[device_checkin_0]', 'from [from_location]', 'to [to_location]', '[loc_id]')", $vars);
						$clear_holding_order_id = ", `holding_order_id` = ''";
					} else if ($extract['inactive']=="1" && !$use_udid) {
						$vars['prefix'] = getLowestPrefix($vars['loc_id']);
						$clear_holding_order_id = ", `holding_order_id` = ''";
					}

					$fill_in_first_checkin = (empty($extract['first_checkin']) || $extract['first_checkin']=="[device_time]")?", `first_checkin` = '[device_checkin_10]'":"";

					$update_record = lavu_query("UPDATE `devices` SET `company_id` = '[device_checkin_7]', `data_name` = '[dn]', `loc_id` = '[loc_id]', `prefix` = '[prefix]', `name` = '[device_checkin_0]', `model` = '[device_checkin_1]', `specific_model` = '[device_checkin_11]', `system_name` = '[device_checkin_2]', `system_version` = '[device_checkin_3]', `UDID` = '[device_checkin_4]', `poslavu_version` = '[device_checkin_5] ([device_checkin_6])', `last_checkin` = now(), `device_time` = '[device_checkin_10]', `app_name` = '[app_name]', `ip_address` = '[device_checkin_9]', `UUID` = '[UUID]', `MAC` = '[MAC]', `lavu_admin` = '[lavu_admin]', `inactive` = '0', `remote_ip` = '[ipaddress]', `usage_type` = '[usage_type]', `reseller_admin` = '[reseller_admin]'".$clear_holding_order_id.$fill_in_first_checkin." WHERE `id` = '[id]'",$vars);

					if ($matches > 1) {
						$delete_duplicates = lavu_query("DELETE FROM `devices` WHERE `UUID` = '[1]' AND `id` != '[2]'", $_REQUEST['UUID'], $extract['id']);
					}

					if ($extract['needs_reload'] == "1") {
						$needs_reload = true;
					}

				} else {

					$vars['prefix'] = getLowestPrefix($_REQUEST['loc_id']);
					$create_record = lavu_query("INSERT INTO `devices` (`company_id`, `data_name`, `loc_id`, `name`, `model`, `specific_model`, `system_name`, `system_version`, `UDID`, `poslavu_version`, `last_checkin`, `device_time`, `app_name`, `ip_address`, `prefix`, `UUID`, `MAC`, `lavu_admin`, `inactive`, `remote_ip`, `usage_type`, `reseller_admin`, `first_checkin`) VALUES ('[device_checkin_7]', '[dn]', '[loc_id]', '[device_checkin_0]', '[device_checkin_1]', '[device_checkin_11]', '[device_checkin_2]', '[device_checkin_3]', '[device_checkin_4]', '[device_checkin_5] ([device_checkin_6])', now(), '[device_checkin_10]', '[app_name]', '[device_checkin_9]', '[prefix]', '[UUID]', '[MAC]', '[lavu_admin]', '0', '[ipaddress]', '[usage_type]', '[reseller_admin]', '[device_checkin_10]')", $vars);
				}

				$device_prefix = (string)$vars['prefix'];

				$update_version = "";
				if (in_array($app_name, array("Lavu Client", "POSLavu Client", "Lavu Retro")) || strstr($app_name, "Lavu Lite")) {
					$update_version = ", `app_name` = '[device_checkin_8]', `version_number` = '[device_checkin_5]', `build_number` = '[device_checkin_6]'";
				}

				$update_main_record = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now()".$update_version." WHERE `id` = '[device_checkin_7]'",$vars);

				if (strstr($_SERVER['HTTP_HOST'], "admin.lavulite.com")) {
					updateCentralLastActivity($data_name, $app_name, $vars['device_checkin_5'], $vars['device_checkin_6']);
				}

				if (!$lls && !$locationFlag) {
					$check_disabled_flag = mlavu_query("SELECT `disabled` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[device_checkin_7]'", $vars);
					if (mysqli_num_rows($check_disabled_flag) > 0) {
						$info = mysqli_fetch_assoc($check_disabled_flag);
						if ($info['disabled'] != "0") {
							$locationFlag = true;
							$lfTitle = "Account Inactive";
							$lfMessage = "Please contact support.";
							$lfKickout = "2";

							$temp_log = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`temp_info` (`type`, `dataname`, `loc_id`, `datetime`) VALUES ('mode_47_disabled_kickout', '[1]', '[2]', '[3]')", $data_name, $_REQUEST['loc_id'], date("Y-m-d H:i:s"));
						}
					}
				}
			}

			mlavu_close_db();

		} else {

			mlavu_close_db();

			$check_field = "UDID";
			if (isset($_REQUEST['UUID'])) {
				$check_field = "UUID";
			} else if (isset($_REQUEST['MAC'])) {
				$check_field = "MAC";
			}

			$check_record = lavu_query("SELECT `id`, `prefix`, `needs_reload` FROM `devices` WHERE `$check_field` = '[1]'", $_REQUEST[$check_field]);
			if (mysqli_num_rows($check_record) > 0) {
				$info = mysqli_fetch_assoc($check_record);
				$device_prefix = $info['prefix'];
				$needs_reload = ($info['needs_reload'] == "1");

				$one_month_ago = date("Y-m-d H:i:s", (time() - 2419200));
				$check_device_prefix_duplicate = lavu_query("SELECT `id` FROM `devices` WHERE `loc_id` = '[1]' AND `$check_field` != '[2]' AND `prefix` = '[3]' AND `last_checkin` >= '[4]'", $_REQUEST['loc_id'], $_REQUEST[$check_field], $info['prefix'], $one_month_ago);
				if (mysqli_num_rows($check_device_prefix_duplicate) > 0) {
					$new_prefix = getLowestPrefix($_REQUEST['loc_id']);
					if (!empty($new_prefix)) {
						$device_prefix = (string)$new_prefix;
						$update_prefix = lavu_query("UPDATE `devices` SET `prefix` = '[1]' WHERE `id` = '[2]'", $new_prefix, $info['id']);
					}
				}
			}

			$device_checkin = explode("||", $_POST['device_checkin']);
			$app_name = (isset($device_checkin[8]))?$device_checkin[8]:"POS Lavu Client";
		}

		if (($num % 5) == 0) {
			$mark_inactive_devices = lavu_query("UPDATE `devices` SET `inactive` = '1' WHERE `last_checkin` < '[1]'", date("Y-m-d H:i:s", (time() - 604800)));
		}

		if (!isset($_REQUEST['KEEP_ACTIVE_DEVICE'])) {
			$clear_holding_order_id = lavu_query("UPDATE `devices` SET `holding_order_id` = '' WHERE `UUID` = '[1]'", determineDeviceIdentifier($_REQUEST));
		}

		if ($_REQUEST['lavu_admin']!="1" && !$reseller_admin) {
			$locationFlag = !isLicenseCompliant($data_name, $device_model, $usage_type);
		}

		$pay_stat = checkPaymentStatus();
		if (!empty($pay_stat['message'])) {
			$locationFlag = true;
			$lfTitle = $pay_stat['title'];
			$lfMessage = $pay_stat['message'];
			$lfKickout = $pay_stat['kickout']?"1":"0";
		}

		if ($locationFlag) {

			// kickout = 1 takes user to locations
			// kickout = 2 takes user to welcome screen

			$json_status = "LocationFlag";
			if ($lfKickout > 0) {
				lavu_echo('{"json_status":"'.$json_status.'","flag_message":"'.$lfMessage.'","flag_title":"'.$lfTitle.'","flag_kickout":"'.$lfKickout.'","file_dates":{"lls":"0"}}');
			}
		}

		$devices_info = "";
		if (($num % 5) == 0) {
			$device_list_key = (isset($_REQUEST['device_list_key']))?$_REQUEST['device_list_key']:"MAC";
			$get_device_info = lavu_query("SELECT `MAC`, `UUID`, `model`, `name`, `ip_address`, `prefix` FROM `devices` WHERE `loc_id` = '[1]' AND `inactive` = '0'", $_REQUEST['loc_id']);
			if (mysqli_num_rows($get_device_info) > 0) {
				$devices_info = ',"devices_info":{';
				$count = 0;
				while ($info = mysqli_fetch_assoc($get_device_info)) {
					$count++;
					if ($count > 1) {
						$devices_info .= ',';
					}
					$devices_info .= '"'.$info[$device_list_key].'":"'.$info['model'].'|*|'.json_escape($info['name']).'|*|'.$info['ip_address'].'"';
					if ($info[$device_list_key]==$_REQUEST[$device_list_key] && $device_prefix=="") {
						$device_prefix = $info['prefix'];
					}
				}
				$devices_info .= '}';
			}
		}

		$date_info = get_use_date_end_date($location_info, $device_time);
		$todays_cash_tips = "";
		$check_todays_cash_tips = lavu_query("SELECT `id`, `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $_REQUEST['loc_id'], $_REQUEST['server_id'], $date_info[0]);
		if (mysqli_num_rows($check_todays_cash_tips) > 0) {
			$info = mysqli_fetch_assoc($check_todays_cash_tips);
			$todays_cash_tips = $info['value'];
		}

		$offline_order_mode = (isset($location_info['offline_order_mode']))?$location_info['offline_order_mode']:1;

		$order_info = "";
		if ($offline_order_mode != 0) {
		
			$now_parts = explode(" ", $device_time);
			$now_date = explode("-", $now_parts[0]);
			$now_time = explode(":", $now_parts[1]);
			$one_month_ts = mktime($now_time[0], $now_time[1], $now_time[2], ($now_date[1] - 1), $now_date[2], $now_date[0]);
			$one_month_ago = date("Y-m-d H:i:s", $one_month_ts);	

			$info_key = (isset($_REQUEST['info_key']))?$_REQUEST['info_key']:"order_id";			

			// old way
			$include_closed = "";
			if ($info_key=="ioid" && $offline_order_mode==2) {
				$include_closed = " OR (`closed` >= '".$date_info[2]."' AND `closed` < '".$date_info[3]."')";
			}
			$co_full_query = "SELECT `order_id`, `ioid`, `last_modified`, `last_mod_device`, `last_mod_register_name`, `opened`, `closed`, `reopened_datetime`, `reclosed_datetime`, `send_status`, `merges`, `server`, `server_id` FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[1]' AND `void` = '0' AND (`closed` = '0000-00-00 00:00:00' AND `opened` != '0000-00-00 00:00:00' {$include_closed}) UNION SELECT `order_id`, `ioid`, `last_modified`, `last_mod_device`, `last_mod_register_name`, `opened`, `closed`, `reopened_datetime`, `reclosed_datetime`, `send_status`, `merges`, `server`, `server_id` FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[1]' AND `void` = '0' AND (`reopened_datetime` != '' AND `reclosed_datetime` = '')";
			
			// new way? - using additional UNION doesn't seem to speed up query
			//$co_base_query = "SELECT `order_id`, `ioid`, `last_modified`, `last_mod_device`, `last_mod_register_name`, `opened`, `closed`, `reopened_datetime`, `reclosed_datetime`, `send_status`, `merges`, `server`, `server_id` FROM `orders` WHERE `location_id` = '[1]' AND `void` = '0' AND (open_or_reopened_or_closed)";
			//$co_full_query = str_replace("(open_or_reopened_or_closed)", "`opened` != '0000-00-00 00:00:00' AND `opened` != '' AND `opened` >= '".$one_month_ago."' AND `closed` = '0000-00-00 00:00:00'", $co_base_query);
			//$co_full_query .= " UNION ".str_replace("(open_or_reopened_or_closed)", "`reopened_datetime` != '' AND `reclosed_datetime` = ''", $co_base_query);
			//if ($info_key=="ioid" && $offline_order_mode==2) $co_full_query .= " UNION ".str_replace("(open_or_reopened_or_closed)", "`closed` >= '".$date_info[2]."' AND `closed` < '".$date_info[3]."'", $co_base_query);

			$check_orders = lavu_query($co_full_query, $_REQUEST['loc_id']);
			if (mysqli_num_rows($check_orders) > 0) {
				$order_info = ',"order_info":{';
				$count = 0;
				while ($info = mysqli_fetch_assoc($check_orders)) {
					$count++;
					if ($count > 1) {
						$order_info .= ',';
					}
					$order_info .= '"'.$info[$info_key].'":"'.$info['last_modified'].'|*|'.$info['last_mod_device'].'|*|'.json_escape($info['last_mod_register_name']).'|*|'.$info['order_id'].'|*|'.$info['opened'].'|*|'.$info['closed'].'|*|'.$info['reopened_datetime'].'|*|'.$info['reclosed_datetime'].'|*|'.$info['send_status'].'|*|'.$info['merges'].'|*|'.$info['server'].'|*|'.$info['server_id'].'"';
				}
				$order_info .= '}';
			}
		}

		$file_dates = '{"lls":"0"}';
		$lls_main_timestamp = "";
		if ($lls) {
			/*$file_dates = "";
			$fd = array();
			$fd['lls'] = 1;
			$fd['core_functions'] = (file_exists("../cp/resources/core_functions.php"))?date("Ymd", filemtime("../cp/resources/core_functions.php")):"";
			$fd['jc_inc7'] = (file_exists("jcvs/jc_inc7.php"))?date("Ymd", filemtime("jcvs/jc_inc7.php")):"";
			$fd['sync_files'] = (file_exists("../local/sync_exec.php"))?date("Ymd", filemtime("../local/sync_exec.php")):"";
			$fd['print_server'] = (file_exists("../local/print_server.php"))?date("Ymd", filemtime("../local/print_server.php")):"";
			$fd['gateway_functions'] = (file_exists("gateway_functions.php"))?date("Ymd", filemtime("gateway_functions.php")):"";
			$fd['management'] = (file_exists("inapp_management.php"))?date("Ymd", filemtime("inapp_management.php")):"";
			$keys = array_keys($fd);
			foreach ($keys as $key) {
				if ($file_dates != "") $file_dates .= ',';
				$file_dates .= '"'.$key.'":"'.$fd[$key].'"';
			}
			$file_dates = '{'.$file_dates.'}';

			//$file_dates = '{"lls":"1","core_functions":"'.date("Ymd", filemtime("../cp/resources/core_functions.php")).'","jc_inc7":"'.date("Ymd", filemtime("jcvs/jc_inc7.php")).'","gateway_functions":"'.date("Ymd", filemtime("gateway_functions.php")).'"}';
			*/
			if (file_exists("lls-main-timestamp")) {
				$lines = file("lls-main-timestamp");
				$lls_main_timestamp = "LLS ".str_replace(array("-", ":", " "), array("", "", "-"), $lines[0]);
			}
		}

		$send_nr = "";
		if ($needs_reload && !$lls) {
			$send_nr = ',"needs_reload":"1"';
		}

		/*$flag_message = "";
		$check_device_messages = lavu_query("SELECT `id`, `title`, `message` FROM `device_messages` WHERE `udid` = '[1]' AND `udid` != '' AND `read` = ''", determineDeviceIdentifier($_REQUEST));
		$msg_count = mysqli_num_rows($check_device_messages);
		if ($msg_count > 0) {
			$title = "Alert Messages";
			$message = "";
			$ts = time();
			$dt = date("Y-m-d H:i:s", $ts);
			while ($info = mysqli_fetch_assoc($check_device_messages)) {
				if ($message != "") $message .= "\n\n\n";
				if ($msg_count == 1) $title = $info['title'];
				else $message .= strtoupper($info['title'])."\n\n";
				$message .= $info['message'];
				$mark_read = lavu_query("UPDATE `device_messages` SET `read` = '[1]', `read_ts` = '[2]' WHERE `id` = '[3]'", $dt, $ts, $info['id']);
			}
			$json_status = "LocationFlag";
			$flag_message = ',"flag_title":"'.$title.'","flag_message":"'.str_replace("\n", "[--CR--]", $message).'","flag_kickout":"0"';
		}*/

		$flag_message = "";
		$deliver_messages = array();
		$check_device_messages = lavu_query("SELECT `id`, `ts`, `title`, `message`, `udid`, `read_by` FROM `device_messages` WHERE `read` = ''"); // limit by time since creation?
		if (mysqli_num_rows($check_device_messages) > 0) {
			$ts = time();
			$dt = date("Y-m-d H:i:s", $ts);
			$read_by_id = $_REQUEST['server_id'];
			$got_user_count = 0;
			while ($info = mysqli_fetch_assoc($check_device_messages)) {
				$update_it = "";
				if ($info['udid'] == determineDeviceIdentifier($_REQUEST)) {
					$deliver_messages[] = $info;
					$update_it = " `read` = '".$dt."', `read_ts` = '".$ts."', `read_by` = '|".$read_by_id."|'";
				} else if ($info['udid'] == "all_servers") {
					if (!strstr($info['read_by'], "|".$read_by_id."|")) {
						$update_it = " `read_ts` = '".$ts."', `read_by` = '".$info['read_by']."|".$read_by_id."|'";
						$deliver_messages[] = $info;
					}
					if ((time() - $info['ts']) > 345600) {
						if ($update_it != "") {
							$update_it .= ", ";
						}
						$update_it .= " `read` = '".$dt."'";				
					}
				}
				if ($update_it != "") {
					$mark_read = lavu_query("UPDATE `device_messages` SET".$update_it." WHERE `id` = '[1]'", $info['id']);
				}
			}
		}
		$msg_count = count($deliver_messages);
		if ($msg_count > 0) {
			$title = "Alert Messages";
			$message = "";
			foreach ($deliver_messages as $msg) {						
				if ($message != "") {
					$message .= "\n\n\n";
				}
				if ($msg_count == 1) {
					$title = $msg['title'];
				} else {
					$message .= strtoupper($msg['title'])."\n\n";
				}
				$message .= $msg['message'];				
			}
			$json_status = "LocationFlag";
			$flag_message = ',"flag_title":"'.$title.'","flag_message":"'.str_replace(array(chr(13), chr(10), "\n"), array("", "[--CR--]", "[--CR--]"), $message).'","flag_kickout":"0"';
		}

		$temp_str = "";
		$check_users = lavu_query("SELECT `id` FROM `users` WHERE `clocked_in` = '1'");
		while ($info = mysqli_fetch_assoc($check_users)) {
			if ($temp_str != "") {
				$temp_str .= ",";
			}
			$temp_str .= '"'.$info['id'].'"';
		}
		$clocked_in_servers = ',"clocked_in_servers":['.$temp_str.']';

		$current_86_counts = determine86counts($active_modules, $_REQUEST['loc_id']);

		$prefix_digits = MIN(MAX((int)reqvar("prefix_digits", "2"), 2), 4);
		$full_offline_prefix = "3".str_pad($device_prefix, $prefix_digits, "0", STR_PAD_LEFT)."-";
		$next_offline_order_id = str_replace($full_offline_prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $_REQUEST['loc_id'], $full_offline_prefix));			
		$prefix_and_next_offline_order_id = ',"device_prefix":"'.$device_prefix.'","next_offline_order_id":"'.$next_offline_order_id.'"';

		lavu_close_db();

		lavu_echo(cleanJSON('{"json_status":"'.$json_status.'","flag_message":"'.$lfMessage.'","flag_title":"'.$lfTitle.'","flag_kickout":"'.$lfKickout.'"'.$prefix_and_next_offline_order_id.',"file_dates":'.$file_dates.',"lls_main_timestamp":"'.$lls_main_timestamp.'"'.$devices_info.$order_info.',"cash_tips":"'.$todays_cash_tips.'"'.$send_nr.$flag_message.$clocked_in_servers.$current_86_counts.'}'));

	} else if ($mode == 48) { // commit item void

		$get_item_info = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `id` = '[3]' LIMIT 1", $_REQUEST['loc_id'], $_REQUEST['order_id'], $_REQUEST['row_id']);
		if (mysqli_num_rows($get_item_info) > 0) {
			$item_info = mysqli_fetch_assoc($get_item_info);
			$original_void = $item_info['void'];
			$original_quantity = $item_info['quantity'];

			$update_item_discount = "";

			if ($item_info['idiscount_type'] == "p") {
				$new_item_subtotal = (($_REQUEST['q'] * $item_info['price']) + ($_REQUEST['q'] * $item_info['forced_modifiers_price']) + ($_REQUEST['q'] * $item_info['modify_price']));
				$discount_amount = ($new_item_subtotal * $item_info['idiscount_value']);
				if ($discount_amount > $new_item_subtotal) {
					$discount_amount = $new_item_subtotal;
				}
				$update_item_discount = ", `idiscount_amount` = '$discount_amount'";
			}

			$update_item = lavu_query("UPDATE `order_contents` SET `void` = '[1]', `quantity` = '[2]', `subtotal` = ([2] * `price`), `subtotal_with_mods` = ([2] * (`price` + `modify_price` + `forced_modifiers_price`))$update_item_discount WHERE `id` = '[3]'", $_REQUEST['v'], $_REQUEST['q'], $item_info['id']);

			$auth_by = convertPIN($_REQUEST['PINused'], "name");
			$auth_by_id = convertPIN($_REQUEST['PINused']);

			$q_fields = "";
			$q_values = "";
			$l_vars = array();
			if ($_REQUEST['q'] == 0) {
				$l_vars['action'] = "Item Voided";
			} else {
				$l_vars['action'] = "Item Partially Voided";
			}
			$l_vars['loc_id'] = $_REQUEST['loc_id'];
			$l_vars['order_id'] = $_REQUEST['order_id'];
			$l_vars['check'] = $_REQUEST['check'];
			$l_vars['time'] = $device_time;
			$l_vars['user'] = $auth_by;
			$l_vars['user_id'] = $auth_by_id;
			$l_vars['details'] = ($_REQUEST['v'] - $original_void)." ".$item_info['item']." - ".priceFormat(($_REQUEST['v'] - $original_void) * ($item_info['price'] + $item_info['modifiy_price'] + $item_info['forced_modifiers_price']));
			$l_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
			$keys = array_keys($l_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

			if (strstr($item_info['options'], "RACER:")) {
				$racer_id = substr($item_info['options'], (strpos($item_info['options'], "(") + 1), (strpos($item_info['options'], ")") - (strpos($item_info['options'], "(") + 1)));
				$get_racer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '[1]'", $racer_id);
				if (mysqli_num_rows($get_racer_info) > 0) {
					$racer_info = mysqli_fetch_assoc($get_racer_info);

					$update_total_visits = "";
					$today = date("Y-m-d");
					$last_activity = explode(" ", $racer_info['last_activity']);
					if ($last_activity[0] != $today) {
						$update_total_visits = ", `total_visits` = `total_visits`+1";
					}

					if ($item_info['hidden_data2'] == "membership") {

						$subtract_credits = (int)($item_info['hidden_data1'] * ($_REQUEST['v'] - $original_void));

						$now = time();
						$update_membership = lavu_query("UPDATE `lk_customers` SET `credits` = `credits`-".$subtract_credits.", `membership` = 'Unqualified', `membership_expiration` = '', `last_activity` = now()".$update_total_visits." WHERE `id` = '".$racer_id."'");

						$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('Voided ".$item_info['item']."', '".$racer_id."', '".$_REQUEST['order_id']."', now(), '".$auth_by_id."', '-".$subtract_credits."', '".$_REQUEST['loc_id']."')");

					} else if ($item_info['hidden_data2'] == "race credits") {

						$subtract_credits = (int)($item_info['hidden_data1'] * ($_REQUEST['v'] - $original_void));

						$show_s = "";
						if ($subtract_credits > 1) {
							$show_s = "s";
						}

						$update_credits = lavu_query("UPDATE `lk_customers` SET `credits` = `credits`-".$subtract_credits.", `last_activity` = now()".$update_total_visits." WHERE id = '".$racer_id."'");

						$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('Voided ".$subtract_credits." Race Credit".$show_s."', '".$racer_id."', '".$_REQUEST['order_id']."', now(), '".$auth_by_id."', '-".$subtract_credits."', '".$_REQUEST['loc_id']."')");
					}
				}
			}

			if (strstr($item_info['options'], "Event #") && ($_REQUEST['q'] == 0)) {

				$info_parts = explode(":", $item_info['options']);
				$event_id = str_replace("Event #", "", $info_parts[0]);
				$get_event_info = lavu_query("SELECT `title` FROM `lk_group_events` WHERE `id` = '$event_id'");
				$event_info = mysqli_fetch_assoc($get_event_info);
				$update_event_record = lavu_query("UPDATE `lk_group_events` SET `_deleted` = '1' WHERE `id` = '$event_id'");

				$q_fields = "";
				$q_values = "";
				$l_vars = array();
				$l_vars['action'] = "Group Event Deleted: ".$event_info['title'];
				$l_vars['loc_id'] = $_REQUEST['loc_id'];
				$l_vars['order_id'] = $_REQUEST['order_id'];
				$l_vars['time'] = $device_time;
				$l_vars['user_id'] = $auth_by_id;
				$l_vars['group_event_id'] = $event_id;
				$keys = array_keys($l_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") {
						$q_fields .= ", ";
						$q_values .= ", ";
					}
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$lk_log_this = lavu_query("INSERT INTO `lk_action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);
			}
			
			$result = "1";
			$result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);
	
			lavu_echo($result);
		}

	} else if ($mode == 49) { // check RFID scan against customer database

		mlavu_close_db();

		$return_string = '{"json_status":"NotFound"}';
		$check_accounts = lavu_query("SELECT `id`, `f_name`, `l_name`, `email` FROM `customer_accounts` WHERE `rf_id` = '[1]' AND (`loc_id` = '[2]' OR `loc_id` = '0')", $_REQUEST['RFID'], $_REQUEST['loc_id']);
		if (mysqli_num_rows($check_accounts) > 1) {
			$return_string = '{"json_status":"MultipleAccounts"}';
		} else if (mysqli_num_rows($check_accounts) == 1) {
			$account_info = mysqli_fetch_assoc($check_accounts);
			$return_string = '{"json_status":"success","id":"'.$account_info['id'].'","f_name":"'.json_escape($account_info['f_name']).'","l_name":"'.json_escape($account_info['l_name']).'","email":"'.json_escape($account_info['email']).'"}';
		}

		lavu_echo(cleanJSON($return_string));

	} else if ($mode == 50) { // record paid in/out

		mlavu_close_db();

		$auth_by = convertPIN($_REQUEST['PINused'], "name");
		$auth_by_id = convertPIN($_REQUEST['PINused']);

		$return_string = "0";
		$ioid = "";

		$check_for_order_placeholder = lavu_query("SELECT `id`, `ioid` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['action']);
		if (mysqli_num_rows($check_for_order_placeholder) > 0) {

			$info = mysqli_fetch_assoc($check_for_order_placeholder);
	
			$ioid = $info['ioid'];
			$update_ioid = "";
			if (empty($ioid)) {
				$ioid = newInternalID();
				$update_ioid = ", `ioid` = '[3]'";
			}
			
			lavu_query("UPDATE `orders` SET `closed` = '[1]'".$update_ioid." WHERE `id` = '[2]'", date("Y-m-d H:i:s"), $info['id'], $ioid);

		} else {

			$create_placeholder = lavu_query("INSERT INTO `orders` (`location_id`, `order_id`, `opened`, `closed`) VALUES ('[1]', '[2]', '[3]', '[3]')", $_REQUEST['loc_id'], $_REQUEST['action'], date("Y-m-d H:i:s"));
		}

		$q_fields = "";
		$q_values = "";
		$vars = array();
		$vars['loc_id'] = $_REQUEST['loc_id'];
		$vars['order_id'] = $_REQUEST['action'];
		$vars['ioid'] = $ioid;
		$vars['internal_id'] = newInternalID();
		$vars['action'] = (($_REQUEST['action'] == "Paid In")?"Sale":"Refund");
		$vars['register'] = $_REQUEST['register'];
		$vars['register_name'] = $_REQUEST['register_name'];
		$vars['amount'] = priceFormat(decimalize($_REQUEST['amount']));
		$vars['total_collected'] = priceFormat(decimalize($_REQUEST['amount']));
		$vars['info'] = $_REQUEST['notes'];
		$vars['info_label'] = "Notes";
		$vars['datetime'] = $device_time;
		$vars['server_id'] = $auth_by_id;
		$vars['server_name'] = $auth_by;
		$vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
		$vars['pay_type_id'] = "1";
		$vars['pay_type'] = "Cash";
		$keys = array_keys($vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$record_it = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $vars);

		if ($record_it) {
			$row_id = lavu_insert_id();
			$return_string = "1|".$row_id;
		}

		lavu_echo($return_string);

	} else if ($mode == 51) { // record mercury hosted checkout payment (also used for other hosted payment types that are hosted on a 3rd party server)

		mlavu_close_db();

		$fwd_info = explode(":*:", str_replace("[PLUS]", "+", $_REQUEST['forward_info']));

		$success = false;

		$internal_id = reqvar("internal_id", "");
		if (empty($internal_id)) {
			$internal_id = reqvar("itid", "");	
		}

		$t_vars = array();
		$t_vars['loc_id'] = $fwd_info[0];
		$t_vars['order_id'] = $fwd_info[1];
		$t_vars['check'] = $fwd_info[2];
		$t_vars['amount'] = $fwd_info[3];
		$t_vars['datetime'] = $fwd_info[4];
		$t_vars['register'] = $fwd_info[5];
		$t_vars['register_name'] = $fwd_info[6];
		$t_vars['transtype'] = $fwd_info[7];
		$t_vars['auth'] = $fwd_info[8];
		$t_vars['change'] = $fwd_info[9];
		$t_vars['total_collected'] = $fwd_info[10];
		$t_vars['server_id'] = $fwd_info[11];
		$t_vars['server_name'] = $fwd_info[12];
		$t_vars['for_deposit'] = $fwd_info[13];
		$t_vars['is_deposit'] = $fwd_info[14];
		$t_vars['action'] = $fwd_info[15];
		$t_vars['card_type'] = $fwd_info[16];
		$t_vars['card_desc'] = $fwd_info[17];
		$t_vars['transaction_id'] = $fwd_info[18];
		$t_vars['preauth_id'] = $fwd_info[19];
		$t_vars['auth_code'] = $fwd_info[20];
		$t_vars['record_no'] = $fwd_info[21];
		$t_vars['process_data'] = $fwd_info[22];
		$t_vars['ref_data'] = $fwd_info[23];
		$t_vars['pay_type'] = $fwd_info[24];
		$t_vars['pay_type_id'] = $fwd_info[25];
		$t_vars['mpshc_pid'] = $fwd_info[26];
		$t_vars['swipe_grade'] = $fwd_info[27];
		$t_vars['got_response'] = $fwd_info[28];
		$t_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
		$t_vars['ioid'] = (isset($_REQUEST['ioid']))?$_REQUEST['ioid']:"";
		$t_vars['internal_id'] = $internal_id;
		$t_vars['tip_amount'] = (isset($fwd_info[31]))?$fwd_info[31]:"";
		$t_vars['gateway'] = (isset($fwd_info[32]))?$fwd_info[32]:"";
		$t_vars['reader'] = (isset($fwd_info[33]))?$fwd_info[33]:"";
		$t_vars['info'] = (isset($fwd_info[34]))?$fwd_info[34]:"";
		$t_vars['exp'] = (isset($fwd_info[35]))?$fwd_info[35]:"";
		$q_values = "";
		$keys = array_keys($t_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$t_vars['last_mod_ts'] = time();
		$t_vars['last_modified'] = $device_time;

		$check_for_duplicate = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' AND `preauth_id` = '[preauth_id]' AND `auth_code` = '[auth_code]' AND `record_no` = '[record_no]'", $t_vars);
		if (mysqli_num_rows($check_for_duplicate) == 0) {
			$success = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $t_vars);
			if ($success) {
				$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (`card_paid` + [amount]), `void`='0', `last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]', `last_modified` = '[last_modified]', `last_mod_device` = '[device_udid]', `last_mod_register_name` = '[register_name]' WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $t_vars);
				$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $t_vars);
				if (mysqli_num_rows($check_for_details) > 0) {
					$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (`card` + [amount]) WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $t_vars);
				} else {
					$create_details = lavu_query("INSERT INTO `split_check_details` (`loc_id`, `order_id`, `check`, `card`) VALUES ('[loc_id]', '[order_id]', '[check]', '[amount]')", $t_vars);
				}
				
				$q_fields = "";
				$q_values = "";
				$l_vars = array();
				$l_vars['action'] = "Payment Applied";
				$l_vars['loc_id'] = $t_vars['loc_id'];
				$l_vars['order_id'] = $t_vars['order_id'];
				$l_vars['check'] = $t_vars['check'];
				$l_vars['time'] = $t_vars['datetime'];
				$l_vars['user'] = $t_vars['server_name'];
				$l_vars['user_id'] = $t_vars['server_id'];
				$l_vars['details'] =  "Check ".$t_vars['check']." - ".$t_vars['pay_type']." - ".priceFormat($t_vars['amount']);
				$l_vars['device_udid'] = $t_vars['device_udid'];
				$keys = array_keys($l_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") {
						$q_fields .= ", ";
						$q_values .= ", ";
					}
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

				// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
			}

		} else {

			$success = true;
		}

		lavu_echo($success?"1":"0");

	} else if ($mode == 52) { // save in-app printer configuration changes or remove printer setting

		if (isset($_POST['assign'])) {

			$assign_printers = explode("|", $_POST['assign']);

			foreach ($assign_printers as $printer) {

				$vars = array();
				$vars['value2'] = $_POST[$printer."-name"];
				$vars['value3'] = $_POST[$printer."-ip"];
				$vars['value5'] = $_POST[$printer."-port"];
				$vars['value6'] = $_POST[$printer."_command_set"];
				$vars['value7'] = $_POST[$printer."_cpl_standard"];
				$vars['value8'] = $_POST[$printer."_cpl_emphasize"];
				$vars['value9'] = $_POST[$printer."_lsf"];
				$vars['value10'] = $_POST[$printer."_raster_capable"];
				$vars['value11'] = $_POST[$printer."_perform_status_check"];
				$vars['value12'] = $_POST[$printer."_use_etb"];
				$vars['value13'] = $_POST[$printer."_skip_ping"];
				$vars['value14'] = $_POST[$printer."_sleep_seconds"];
				$vars['value_long2'] = $_POST[$printer."_special_info"];
				$special_info = json_decode($vars['value_long2'], true);
				$vars['value4'] = (isset($special_info[0]['brand']))?$special_info[0]['brand']:"";
				$vars['_deleted'] = "0";
				$q_update = "";
				$keys = array_keys($vars);
				foreach ($keys as $key) {
					if ($q_update != "") {
						$q_update .= ", ";
					}
					$q_update .= "`$key` = '[$key]'";
				}
				$vars['location'] = $_POST['loc_id'];
				$vars['type'] = "printer";
				$vars['setting'] = $printer;
				$q_fields = "";
				$q_values = "";
				$keys = array_keys($vars);
				foreach ($keys as $key) {
					if ($q_fields != "") {
						$q_fields .= ", ";
						$q_values .= ", ";
					}
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}

				$check1 = lavu_query("SELECT `id` FROM `config` WHERE `location` = '[location]' AND `type` = 'printer' AND `setting` = '[setting]' AND `_deleted` = '0'", $vars);
				if (mysqli_num_rows($check1) > 0) {
					$info = mysqli_fetch_assoc($check1);
					$vars['id'] = $info['id'];
					lavu_query("UPDATE `config` SET $q_update WHERE `id` = '[id]'", $vars);
				} else {
					lavu_query("INSERT INTO `config` ($q_fields) VALUES ($q_values)", $vars);
				}

				$check2 = lavu_query("SELECT `id` FROM `config` WHERE `location` = '[location]' AND `type` = '' AND `setting` = '[setting]'", $vars);
				if (mysqli_num_rows($check2) > 0) {
					$info = mysqli_fetch_assoc($check2);
					$vars['id'] = $info['id'];
					lavu_query("UPDATE `config` SET `value` = '[value2]' WHERE `id` = '[id]'", $vars);
				} else {
					lavu_query("INSERT INTO `config` (`location`, `setting`, `value`) VALUES ('[location]', '[setting]', '[value2]')", $vars);
				}
			}

			schedule_remote_to_local_sync_if_lls($location_info, $_POST['loc_id'], "table updated", "config");

			$set_direct_printing = lavu_query("UPDATE `locations` SET `use_direct_printing` = '1' WHERE `id` = '[1]'", $_POST['loc_id']);
			schedule_remote_to_local_sync_if_lls($location_info, $_POST['loc_id'], "table updated", "locations");

		} else if (isset($_POST['remove'])) {

			$remove_printers = explode("|", $_POST['remove']);

			foreach ($remove_printers as $printer) {

				lavu_query("DELETE FROM `config` WHERE `location` = '[1]' AND `type` = 'printer' AND `setting` = '[2]'", $_POST['loc_id'], $printer);
				lavu_query("DELETE FROM `config` WHERE `location` = '[1]' AND `type` = '' AND `setting` = '[2]'", $_POST['loc_id'], $printer);
			}
		}

		lavu_echo("1");

	} else if ($mode == 53) { // check LoyalTree promo code for redemption

		$order_id  = reqvar('order_id', '');
		$check     = reqvar('check_id', '1');
		$server_id = reqvar('server_id', '');
		$code      = reqvar('code', '');

		$args = array(
			'action'        => 'redeem',
			'dataname'      => $data_name,
			'order_id'      => $order_id,
			'loc_id'        => $location_info['id'],
			'businessid'    => $location_info['loyaltree_id'],
			'chainid'       => $location_info['loyaltree_chain_id'],
			'storeid'       => $company_info['id'].$location_info['id'],
			'serverid'      => $server_id,
			'code'          => $code,
			'transactionid' => $order_id.'-'.$check,
			'check'         => $check,
			'app'           => (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP),
		);

		$response_json = LoyalTreeIntegration::redeem($args);

		$response = json_decode($response_json, true);

		if ($response['json_status'] == 'success')
		{
			$args['discount_info'] = 'loyaltree|check|'. $response['id'] .'|0|'. time();

			if (!mysqli_num_rows(lavu_query("SELECT * FROM `split_check_details` WHERE `order_id` = '[order_id]' AND `check` = '[check]'", $args))) {
				lavu_query("INSERT INTO `split_check_details` (`loc_id`,`order_id`,`check`,`discount_info`) VALUES ('[loc_id]','[order_id]','[check]','[discount_info]')", $args);
			} else {
				lavu_query("UPDATE `split_check_details` SET `discount_info` = '[discount_info]' WHERE `order_id` = '[order_id]' AND `check` = '[check]'", $args);
			}
		}

		// Echo JSON response for app
		echo $response_json;

		lavu_exit();

	} else if ($mode == 54) { // mark check as printed

		$mark_check_printed = lavu_query("UPDATE `orders` SET `check_has_printed` = '1' WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		lavu_exit();

	} else if ($mode == 55) { // search customer_accounts for RF_ID payment (Apple Meal Plan)

		$vars = array();
		$vars['loc_id'] = $_REQUEST['loc_id'];
		$vars['search'] = $_REQUEST['search'];

		$filter = "`f_name` LIKE '%[search]%' OR `l_name` LIKE '%[search]%' OR `email` LIKE '%[search]%' OR `rf_id` LIKE '%[search]%'";
		if (strstr($_REQUEST['search'], " ")) {
			$search_parts = explode(" ", $_REQUEST['search']);
			$vars['f_name'] = $search_parts[0];
			$vars['l_name'] = $search_parts[1];
			$filter = "`f_name` LIKE '%[f_name]%' OR `l_name` LIKE '[l_name]%'";
		}

		$return_string = '{"json_status":"NotFound"}';
		$check_accounts = lavu_query("SELECT `id`, `f_name`, `l_name`, `email`, `employee_id`, `rf_id` FROM `customer_accounts` WHERE ($filter) AND (`loc_id` = '[loc_id]' OR `loc_id` = '0') ORDER BY `l_name`, `f_name`, `email` ASC LIMIT 100", $vars);
		if (mysqli_num_rows($check_accounts) > 0) {
			$found_records_array = array();
			while ($data_obj = mysqli_fetch_object($check_accounts)) {
				$found_records_array[] = $data_obj;
			}
			$return_string = '{"json_status":"success","PHPSESSID":"'.session_id().'","found_records":'.json_encode($found_records_array).'}';
		}

		lavu_echo(cleanJSON($return_string));

	} else if ($mode == 56) { // clear claimed device slot

		$clear_slot = lavu_query("UPDATE `devices` SET `slot_claimed` = '' WHERE `UUID` = '[1]'", $_REQUEST['UUID']);
		lavu_exit();

	} else if ($mode == 57) { // destroy current php session - needed for local server when client reloads settings

		killLavuSession();
		lavu_exit();

	} else if ($mode == 58) { // cancel LoyalTree redemption, AKA reward reinstate

		$order_id = reqvar('order_id', '');
		$check = reqvar('check', '1');
		$rid = reqvar('rid', '');  // redemptionid
		$isdeal = reqvar('isdeal', '');
		$reason = reqvar('reason', '');
		$server_id = reqvar('server_id', '');

		if (empty($order_id)) {
			$res = mysqli_fetch_assoc(lavu_query("SELECT `order_id` FROM `order_contents` WHERE `idiscount_info` LIKE '%[1]%' ORDER BY `id` DESC LIMIT 1", $rid));
			$order_id = $res['order_id'];
		}

		$args = array(
			'action'        => 'reinstate',
			'dataname'      => $data_name,
			'order_id'      => $order_id,
			'loc_id'        => $location_info['id'],
			'check'         => $check,
			'transactionid' => $order_id.'-'.$check,
			'businessid'	=> $location_info['loyaltree_id'],
			'chainid'		=> $location_info['loyaltree_chain_id'],
			'storeid'		=> $company_info['id'].$location_info['id'],
			'serverid'	    => $server_id,
			'rid'           => $rid,
			'isdeal'		=> $isdeal,
			'reason'		=> $reason,
			'app'           => (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP),
		);

		$response = LoyalTreeIntegration::reinstate($args);

		// Echo JSON response for app
		echo $response;

		lavu_exit();

	} else if ($mode == 59) { // single out multiple quantity item for LoyalTree discount

		$return_status = "failed";
		$return_row_id = "0";

		$get_item = lavu_query("SELECT * FROM `order_contents` WHERE `order_id` = '[1]' AND `id` = '[2]'", $_POST['order_id'], $_POST['row_id']);
		if (mysqli_num_rows($get_item) > 0) {

			$item_info = mysqli_fetch_assoc($get_item);
			$new_quantity = ((float)$item_info['quantity'] - 1);

			$update = lavu_query("UPDATE `order_contents` SET `quantity` = '[1]', `subtotal` = '[2]' WHERE `id` = '[3]'", $new_quantity, ($new_quantity * (float)$item_info['price']), $item_info['id']);
			if ($update) {

				$item_info['quantity'] = "1";
				$item_info['subtotal'] = $item_info['price'];
				$item_info['subtotal_with_mods'] = ((float)$item_info['price'] + (float)$item_info['forced_modifiers_price'] + (float)$item_info['modify_price']);
				$q_fields = "";
				$q_values = "";
				$keys = array_keys($item_info);
				foreach ($keys as $key) {
					if ($key != "id") {
						if ($q_fields != "") {
							$q_fields .= ", ";
							$q_values .= ", ";
						}
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
				}
				$insert = lavu_query("INSERT INTO `order_contents` ($q_fields) VALUES ($q_values)", $item_info);
				if ($insert) {
					$return_status = "success";
					$return_row_id = lavu_insert_id();
				}
			}
		}

		lavu_echo('{"json_status":"'.$return_status.'","PHPSESSID":"'.session_id().'","row_id":"'.$return_row_id.'"}');

	} else if ($mode == 60) { // add print job to local server print queue

		require_once(dirname(__FILE__)."/../../local/print_server.php");

		$ip_addresses = explode("|*|", $_REQUEST['ip']);
		$ports = explode("|*|", $_REQUEST['port']);
		$designators = explode("|*|", $_REQUEST['designator']);
		$printer_names = explode("|*|", $_REQUEST['printer_name']);
		$command_set_ids = explode("|*|", $_REQUEST['command_set']);
		$image_capabilities = explode("|*|", $_REQUEST['image_capability']);
		$print_strings = explode("|*|", $_REQUEST['print_string']);
		$plain_texts = explode("|*|", $_REQUEST['plain_text']);

		$results = array();

		for ($pj = 0; $pj < count($ip_addresses); $pj++) {
		
			$ip = $ip_addresses[$pj];
			$port = $ports[$pj];
			$des = $designators[$pj];
			$pname = $printer_names[$pj];
			$csid = $command_set_ids[$pj];
			$ic = $image_capabilities[$pj];
			$pstr = $print_strings[$pj];
			$ptxt = $plain_texts[$pj];
		
			if ($csid == "19") {
				$ip = $_SERVER['SERVER_ADDR'];
				$port = "5288";
			}

			if (!empty($ip) && (string)$port!="" && !empty($des) && !empty($pname) && !empty($csid) && (string)$ic!="" && !empty($pstr)) {
			
				if (substr($pstr, 0, 10) == "DUPLICATE:") {
					$ps_parts = explode(":", $pstr);
					$di = $ps_parts[1];
					$pstr = $print_strings[$di];
					$ptxt = $plain_texts[$di];
				}
		
				$vars = array(
					"printer_ipaddress"	=> $ip,
					"printer_port"		=> $port,
					"designator"		=> $des,
					"printer_name"		=> $pname,
					"image_capability"	=> $ic,
					"contents"			=> $pstr,
					"plain_text"		=> $ptxt,
					"udid"				=> determineDeviceIdentifier($_REQUEST),
					"source_ipaddress"	=> $_SERVER['REMOTE_ADDR'],
					"order_id"			=> $_REQUEST['order_id'],
					"datetime"			=> date("Y-m-d H:i:s"),
					"ts"				=> time(),
					"status"			=> "new",
					"sent_ts"			=> "",
					"message"			=> "",
					"alert_sent"		=> "",
					"message_history"	=> ""
				);
					
				$result = add_to_queue($vars);
				if (!isset($result[0])) {
					$result[0] = false;
				}
				if (!$result[0]) {
					$result[1] = "db_error";
				}
				if (!isset($result[1])) {
					$result[1] = "";
				}
				if (!isset($result[2])) {
					$result[2] = "";
				}
				$result[3] = $pj;
				$results[] = $result;

			} else if (!empty($ip) || (string)$port!="" || !empty($des) || !empty($pname) || !empty($csid) || (string)$ic!="" || !empty($pstr)) {
				
				$missing = array();
				if (empty($ip)) {
					$missing[] = "IP Address";
				}
				if ((string)$port == "") {
					$missing[] = "Port";
				}
				if (empty($des)) {
					$missing[] = "Designator";
				}
				if (empty($csid)) {
					$missing[] = "Command Set ID";
				}
				if ((string)$ic == "") {
					$missing[] = "Image Capability";
				}
				if (empty($pstr)) {
					$missing[] = "Contents";
				}
				$result = array(false, "incomplete_data", implode(",", $missing), $pj);
				$results[] = $result;
			}
		}
		
		$overall_result = "";
		$did_check_last_print_ts = false;
		foreach ($results as $rslt) {

			if ($rslt[0] && !$did_check_last_print_ts) {
	
				$did_check_last_print_ts = true;
				$ri = $rslt[3];
				$check_ts = lavu_query("SELECT `last_print_ts` FROM `local_printer_status` WHERE (`ipaddress` = '[1]' OR (`ipaddress` = '[2]' AND `port` = '[3]')) ORDER BY `last_print_ts` DESC LIMIT 1", "queue_".$designators[$ri], $ip_addresses[$ri], $ports[$ri]);
				if (mysqli_num_rows($check_ts) > 0) {
	
					$info = mysqli_fetch_assoc($check_ts);
					$seconds_past = (time() - $info['last_print_ts']);
					if ($seconds_past > 60) {
						$overall_result = "queue_not_running|".floor($seconds_past / 60);
						break;
					}
	
				} else {
	
					$overall_result = "queue_not_running|";
					break;
				}
			}
	
			if ($overall_result != "") {
				$overall_result .= "|*|";
			}
			$overall_result .= $rslt[1]."|".$rslt[2]."|".$rslt[3];
		}

		lavu_echo($overall_result);

	} else if ($mode == 61) { // retrieve current cash deposit total for a server/regsiter for the current shift

		$type = "register";
		if ($location_info['track_shifts_by']=="house" || $location_info['track_shifts_by'] == "both") {
			$type = "house";
		}

		$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
		$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

		$idatetime = explode(" ", $device_time);
		$idate = explode("-", $idatetime[0]);
		$itime = explode(":", $idatetime[1]);
		$yts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] - 1), $idate[0]);
		$nowts = mktime($itime[0], $itime[1], $itime[2], $idate[1], $idate[2], $idate[0]);
		$tts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] + 1), $idate[0]);

		if ($itime[0].$itime[1].$itime[2] >= $se_hour.$se_min."00") {
			$use_date = date("Y-m-d", $nowts);
			$end_date = date("Y-m-d", $tts);
		} else {
			$use_date = date("Y-m-d", $yts);
			$end_date = date("Y-m-d", $nowts);
		}

		$udate = explode("-",$use_date);
		$uyear = $udate[0];
		$umonth = $udate[1];
		$uday = $udate[2];
		$udate_display = date("n/j/Y",mktime(0,0,0,$umonth,$uday,$uyear));

		$end_hour = $se_hour;
		$end_min = str_pad(($se_min - 1), 2, "0", STR_PAD_LEFT);
		if ($end_min < 0) {
			$end_hour = str_pad(($se_hour - 1), 2, "0", STR_PAD_LEFT);
			$end_min = 59;
		}
		if ($end_hour < 0) {
			$end_hour = 23;
			$end_date = date("Y-m-d", $nowts);
		}

		$start_datetime = "$use_date $se_hour:$se_min:00";
		$end_datetime = "$end_date $end_hour:$end_min:59";

		$shift_start = $start_datetime;
		$check_for_shifts = lavu_query("SELECT * FROM `shifts` WHERE `loc_id` = '[1]' AND `register` = '[2]' AND `datetime` > '[3]' AND `datetime` < '[4]' AND `type` = '[5]' ORDER BY `id` DESC", $_REQUEST['loc_id'], $_REQUEST['register'], $start_datetime, $end_datetime, $type);
		if (mysqli_num_rows($check_for_shifts) > 0) {
			$info = mysqli_fetch_assoc($check_for_shifts);
			$shift_start = $info['datetime'];
		}

		$vars = array();
		$vars['loc_id'] = $_REQUEST['loc_id'];
		$vars['server_id'] = $_REQUEST['server_id'];
		$vars['register'] = $_REQUEST['register'];
		$vars['date'] = $use_date;
		$vars['start_datetime'] = $shift_start;
		$vars['end_datetime'] = $end_datetime;

		$deposited = 0;
		$get_cash_deposits = lavu_query("SELECT SUM(`value`) AS `value` FROM `cash_data` WHERE `type` = 'server_register_deposit' AND `loc_id` = '[loc_id]' AND `server_id` = '[server_id]' AND `value2` = '[register]' AND `date` = '[date]' AND `device_time` >= '[start_datetime]' AND `device_time` <= '[end_datetime]'", $vars);
		if (mysqli_num_rows($get_cash_deposits) > 0) {
			$info = mysqli_fetch_assoc($get_cash_deposits);
			$deposited = $info['value'];
		}

		$expected = 0;
		$get_expected_cash = lavu_query("SELECT SUM(IF(`cc_transactions`.`action`='Refund', 0 - `cc_transactions`.`total_collected`, `cc_transactions`.`total_collected`)) AS `amount` FROM `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` WHERE `pay_type_id` = '1' AND `cc_transactions`.`datetime` >= '[start_datetime]' AND `cc_transactions`.`datetime` <= '[end_datetime]' AND `cc_transactions`.`loc_id` = '[loc_id]' AND `orders`.`void` = '0' AND (`orders`.`location_id` = `cc_transactions`.`loc_id` OR `cc_transactions`.`order_id` = \"Paid In\" OR `cc_transactions`.`order_id` = \"Paid Out\") AND `cc_transactions`.`action` != \"\" AND `cc_transactions`.`voided` != '1'", $vars);
		if (mysqli_num_rows($get_expected_cash) > 0) {
			$info = mysqli_fetch_assoc($get_expected_cash);
			$expected = $info['amount'];
		}

		lavu_echo("1|".priceFormat($expected - $deposited));

	} else if ($mode == 62) { // offline order info sync

		set_time_limit(0);
		ignore_user_abort(1);

		$posts = explode("[:_:]", $_REQUEST['posts']);
		$pcount = count($posts);

		$device_id = determineDeviceIdentifier($_REQUEST);
		$device_name = $device_id;
		$get_device_name = lavu_query("SELECT `name` FROM `devices` WHERE `UUID` = '[1]' AND `inactive` = '0' ORDER BY `id` DESC LIMIT 1", $device_id);
		if (mysqli_num_rows($get_device_name) > 0) {
			$info = mysqli_fetch_assoc($get_device_name);
			if (!empty($info['name'])) {
				$device_name = $info['name']." (".$device_id.")";
			}
		}

		$scount = 0;
		$syncing_order = "";
		$synced_orders = array();
		$mode_28_failed = false;
		$failed_posts = array();

		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = "Started Offline Order Sync";
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = "";
		$l_vars['check'] = "";
		$l_vars['time'] = determineDeviceTime($location_info, $_REQUEST);
		$l_vars['user'] = $_REQUEST['server_name'];
		$l_vars['user_id'] = $_REQUEST['server_id'];
		$l_vars['details'] = $pcount." post".(($pcount>1)?"s":"");
		$l_vars['device_udid'] = $device_id;
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);
		mlavu_close_db();

		foreach ($posts as $post) {

			$post_vars = array();
			$post_parts = explode("[VARAMP]", $post);
			foreach ($post_parts as $part) {
				$eqpos = strpos($part, "=");
				$post_vars[substr($part, 0, $eqpos)] = substr($part, ($eqpos + 1));
			}

			if ($post_vars['m'] == 10) { // close order

				if (!$mode_28_failed || $record_already_exists) {
					$response = closeOrder($post_vars);
					if ($response == "1") {
						$scount++;
					} else {
						$failed_posts[] = $post;
					}
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 11) { // log send

				$response = logSend($post_vars);
				if ($response == "1") {
					$scount++;
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 12) { // void order

				if (!$mode_28_failed || $record_already_exists) {
					$response = voidOrder($post_vars);
					if ($response == "1") {
						$scount++;
					} else {
						$failed_posts[] = $post;
					}
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 13) { // log item edits

				$response = logItemEdits($post_vars);
				if ($response == "1") {
					$scount++;
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 21) { // log open cash drawer

				$response = logOpenCashDrawer($post_vars);
				if ($response == "1") {
					$scount++;
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 22) { // log order change after check printed

				$response = logOrderChangeAfterCheckPrinted($post_vars);
				if ($response == "1") {
					$scount++;
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 28) { // save order

				$posted_order_info = explode("|*|", $post_vars['order_info']);
				$order_id = $posted_order_info[0];

				if ($syncing_order!="" && $order_id!=$syncing_order && !$mode_28_failed) {
					$synced_orders[] = $syncing_order;
				}
				$syncing_order = $order_id;

				$record_already_exists = false;
				$check_order_info = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $order_id);
				if (mysqli_num_rows($check_order_info) > 0) {
					$record_already_exists = true;
					$saved_order_info = mysqli_fetch_assoc($check_order_info);
					if ($saved_order_info['last_mod_device'] != $device_id) {
						logCurrentOrderState($saved_order_info, $device_id, "Offline Sync");
					}
				}

				$mode_28_failed = false;
				$response = saveOrder($post_vars);
				$response_parts = explode(":", $response);
				if ($response_parts[0] == "1") {
					$scount++;
				} else {
					$mode_28_failed = true;
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 40) { // save payments and discounts

				if (!$mode_28_failed || $record_already_exists) {
					$response = savePaymentsAndDiscounts($post_vars);
					if (strstr($response, '"json_status":"success"')) {
						$scount++;
					} else {
						$failed_posts[] = $post;
					}
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 41) { // log payment reassignments

				if (!$mode_28_failed || $record_already_exists) {
					$response = logPaymentReassignments($post_vars);
					if ($response == "1") {
						$scount++;
					} else {
						$failed_posts[] = $post;
					}
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 42) { // save single payment

				if (!$mode_28_failed || $record_already_exists) {
					$response = saveSinglePayment($post_vars);
					if (strstr($response, '"json_status":"success"')) {
						$scount++;
					} else {
						$failed_posts[] = $post;
					}
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 43) { // send email receipt

				$response = sendEmailReceipt($post_vars);
				if (substr($response, 0, 2) == "1|") {
					$scount++;
				} else {
					$failed_posts[] = $post;
				}

			} else if ($post_vars['m'] == 64) { // write action log entry

				$response = writeActionLogEntry($post_vars);
				if ($response == "1") {
					$scount++;
				} else {
					$failed_posts[] = $post;
				}
			}
		}

		if (!$mode_28_failed) {
			$synced_orders[] = $syncing_order;
		}

		$ocount = count($synced_orders);
		$fcount = count($failed_posts);

		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = "Finished Offline Order Sync";
		$l_vars['loc_id'] = $_REQUEST['loc_id'];
		$l_vars['order_id'] = "";
		$l_vars['check'] = "";
		$l_vars['time'] = determineDeviceTime($location_info, array());
		$l_vars['user'] = $_REQUEST['server_name'];
		$l_vars['user_id'] = $_REQUEST['server_id'];
		$l_vars['details_short'] = $pcount." post".(($pcount>1)?"s":"")." - ".$scount." succeeded (".$ocount." order".(($ocount>1)?"s":"")."), ".$fcount." failed";
		$l_vars['details'] = implode(",", $synced_orders);
		$l_vars['device_udid'] = $device_id;
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") {
				$q_fields .= ", ";
				$q_values .= ", ";
			}
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);		

		if (count($failed_posts) > 0) {
			$path = "/home/poslavu/logs/company/".$data_name."/offline_syncing";
			if (!is_dir($path)) {
				mkdir($path, 0755);
			}
			$fp = fopen($path."/failed_posts_".str_replace(":", "", $device_id)."_".date("YmdHis"), "a");
			fputs($fp, implode("[:_:]", $failed_posts));
			fclose($fp);
		}

		lavu_echo('{"json_status":"success","stats":"'.$pcount.':'.$scount.':'.$ocount.':'.$fcount.'"}');

	} else if ($mode == 63) { // set active device id for an order

		mlavu_close_db();

		$set_to = $_REQUEST['set_active_device'];
		$device_id = determineDeviceIdentifier($_REQUEST);

		// *** order holding kept track on order record	***
		/*$filter = "";
		if ($set_to == "this_device") $set_to = $device_id;
		else if ($set_to == "") $filter = " AND (`active_device` = '' OR `active_device` = '".$device_id."')";

		$update = lavu_query("UPDATE `orders` SET `active_device` = '[1]' WHERE `location_id` = '[2]' AND `order_id` = '[3]'".$filter, $set_to, $_REQUEST['loc_id'], $_REQUEST['order_id']);*/

		// *** order holding kept track on device record	***

		$loc_id = $_REQUEST['loc_id'];
		$order_id = $_REQUEST['order_id'];

		if ($set_to == "this_device") {
			$update = setOrderHeldByDevice($loc_id, $order_id, $device_id);
		} else if ($set_to == "") {
			$update = lavu_query("UPDATE `devices` SET `holding_order_id` = '' WHERE `loc_id` = '[1]' AND (`holding_order_id` = '[2]' OR `UUID` = '[3]')", $loc_id, $order_id, $device_id);
			if ($order_id != "0") {
				$update_order = lavu_query("UPDATE `orders` SET `active_device` = '', `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = '[3]' WHERE `location_id` = '[4]' AND `order_id` = '[5]'", time(), $device_time, $device_id, $loc_id, $order_id);
			}
		}

		lavu_echo($update?"1":"");

	} else if ($mode == 64) { // write action log entry only

		$response = writeActionLogEntry($_REQUEST);

		lavu_echo($response);

	} else if ($mode == 65) { // retrieve device next offline order id

		$initial_digit = reqvar("initial_digit", "3");
		$prefix_digits = MIN(MAX((int)reqvar("prefix_digits", "2"), 2), 4);
		$prefix = $initial_digit.str_pad($_REQUEST['device_prefix'], $prefix_digits, "0", STR_PAD_LEFT)."-";

		$response = "1|".str_replace($prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $_REQUEST['loc_id'], $prefix));
		
		lavu_echo($response);

	} else if ($mode == 66) { // check 86 count for an item (when in-app value equals 5 or less)

		mlavu_close_db();

		$item_id = $_REQUEST['item_id'];
		$config_id = false;
		$saved_count = false;
		$device_portions = array();

		$check_temp_count = lavu_query("SELECT `id`, `value`, `value2`, `value3` FROM `config` WHERE `type` = 'temp_inv_count' AND `setting` = '[1]'", $item_id);
		if (mysqli_num_rows($check_temp_count) > 0) {
			$info = mysqli_fetch_assoc($check_temp_count);
			$config_id = $info['id'];
			if ($info['value2'] > date("Y-m-d H:i:s", (time() - 180))) {
				$adj_saved = 0;
				$saved_device_portions = json_decode($info['value3'], true);
				if (is_array($saved_device_portions)) {
					if (isset($saved_device_portions[0][$_REQUEST['UUID']])) {
						$adj_saved = (int)$saved_device_portions[0][$_REQUEST['UUID']];
						unset($saved_device_portions[0][$_REQUEST['UUID']]);
					}
					$device_portions = $saved_device_portions[0];
				}
				$saved_count = ($info['value'] + $adj_saved);
			}
		}

		if (!$saved_count) {
			$check_inv_count = lavu_query("SELECT `inv_count` FROM `menu_items` WHERE `id` = '[1]'", $item_id);
			if (mysqli_num_rows($check_inv_count) > 0) {
				$info = mysqli_fetch_assoc($check_inv_count);
				$saved_count = (int)$info['inv_count'];
			}
		}

		$device_portions[$_REQUEST['UUID']] = $_REQUEST['added'];

		$vars = array();
		$vars['loc_id'] = $_REQUEST['loc_id'];
		$vars['item_id'] = $item_id;
		$vars['temp_count'] = ($saved_count - (int)$_REQUEST['added']);
		$vars['datetime'] = date("Y-m-d H:i:s");
		$vars['device_portions'] = json_encode($device_portions);
		$vars['config_id'] = $config_id;

		$query = "INSERT INTO `config` (`location`, `type`, `setting`, `value`, `value2`, `value3`) VALUES ('[loc_id]', 'temp_inv_count', '[item_id]', '[temp_count]', '[datetime]', '[device_portions]')";
		if ($config_id) {
			$query = "UPDATE `config` SET `value` = '[temp_count]', `value2` = '[datetime]', `value3` = '[device_portions]' WHERE `id` = '[config_id]'";
		}
		$save_temp_count = lavu_query($query, $vars);

		lavu_echo("1|".$vars['temp_count']);

	} else if ($mode == 67) { // save configuration settings for LavuPiPrinter

		if (isset($_REQUEST['LavuController'])) {

			if ($_REQUEST['LavuController'] == "") {
				$set_lqpj = "0";
			} else {
				$set_lqpj = "1";
				$update = lavu_query("UPDATE `locations` SET `net_path_print` = 'http://[1]' WHERE `id` = '[2]'", $_REQUEST['LavuController'], $_REQUEST['loc_id']);
			}

			$check_config = lavu_query("SELECT `id` FROM `config` WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting` = 'lls_queue_print_jobs'", $_REQUEST['loc_id']);
			if (mysqli_num_rows($check_config) > 0) {
				$info = mysqli_fetch_assoc($check_config);
				$update = lavu_query("UPDATE `config` SET `value` = '[1]' WHERE `id` = '[2]'", $set_lqpj, $info['id']);
			} else {
				$insert = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'lls_queue_print_jobs', '[2]')", $_REQUEST['loc_id'], $set_lqpj);
			}
		}

		lavu_echo("1");

	} else if ($mode == 68) { // pull send log for an order

		mlavu_close_db();

		$response_json = '{"json_status":"db_error"}';
	
		$order_ids = array($_REQUEST['order_id']);
		$check_for_merges = lavu_query("SELECT `merges` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id']);
		if (mysqli_num_rows($check_for_merges) > 0) {
			$info = mysqli_fetch_assoc($check_for_merges);
			if (!empty($info['merges'])) {
				$order_ids = array_merge($order_ids, explode("|", $info['merges']));
			}
		}

		$get_send_log = lavu_query("SELECT `time_sent`, `tablename`, `course`, `total`, `send_server`, `send_server_id`, `device_udid`, `items_sent`, `resend` FROM `send_log` WHERE `location_id` = '[1]' AND `order_id` IN ('".implode("','", $order_ids)."') ORDER BY `time_sent` ASC", $_REQUEST['loc_id']);
		if (mysqli_num_rows($get_send_log) > 0) {
			$send_log_array = array();
			while ($data_obj = mysqli_fetch_object($get_send_log)) {
				$send_log_array[] = $data_obj;
			}
			$response_json = '{"json_status":"success","log":'.json_encode($send_log_array).'}';
		} else {
			$response_json = '{"json_status":"no_sends"}';
		}

		lavu_echo($response_json);

	} else if ($mode == 69) { // periodic (minor) device check-in, initially added to handle grabbing clocked in status of users while in waiting_for_PIN mode
	
		mlavu_close_db();
	
		$lavu_query_should_log = false;
		$json_status = "failed";
	
		$temp_str = "";
		$check_users = lavu_query("SELECT `id` FROM `users` WHERE `clocked_in` = '1'");
		if ($check_users) {
			$json_status = "success";
			while ($info = mysqli_fetch_assoc($check_users)) {
				if ($temp_str != "") {
					$temp_str .= ",";
				}
				$temp_str .= '"'.$info['id'].'"';
			}
		}
		$clocked_in_servers = '"clocked_in_servers":['.$temp_str.']';

		lavu_echo(cleanJSON('{"json_status":"'.$json_status.'",'.$clocked_in_servers.'}'));
	}
?>
