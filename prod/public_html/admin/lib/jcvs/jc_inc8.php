<?php

	require_once(__DIR__."/jc_func8.php");
	require_once(dirname(__FILE__) . "/../../vendor/autoload.php");
	require_once(dirname(__FILE__) . "/../../app/Http/Controllers/LavuPayController.php");
	require_once(__DIR__ .'/../LocationCurrency.php');

	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\HttpFoundation\Response;
	use App\Controller\LavuPayController;

	if (!isset($location_info))
	{
		$location_info = array();
	}

	$device_time = determineDeviceTime($location_info, $_REQUEST);

	if (!isset($decimal_places))
	{
		$decimal_places = (isset($location_info['disable_decimal']))?$location_info['disable_decimal']:2;
	}

	$smallest_money = (1 / pow(10, $decimal_places));

	$mode			= reqvar("m", "");
	$app_name		= reqvar("app_name", "");
	$app_version	= reqvar("app_version", "");
	$app_build		= reqvar("app_build", "");
	$host			= $_SERVER['HTTP_HOST'];
	$break_time  	= 0;
	$details_short = array();
	if (in_array($mode, array( "clock_in", "clock_out", "log_in", "load_location", "reload_settings", "email_receipt","transition_details","xml_uruguay","pickup_seq" )))
	{
		if (!empty($app_name))
		{
			$details_short[] = $app_name;
		}
		if (!empty($app_version))
		{
			$details_short[] = $app_version;
		}
		if (!empty($app_build))
		{
			$details_short[] = "(".$app_build.")";
		}
		if (!empty($host))
		{
			$details_short[] = "via ".$host;
		}
	}

	$modeArr = [
        'undoLoyaltyDiscount' => [
            'file' => 'UndoLoyaltyDiscount',
            'func' => 'undoLoyaltyDiscount'
        ],
        'manage_item_availability' => [
            'file' => 'ManageItemAvailability',
            'func' => 'manageItemAvailability'
        ],
        'menuItemsDetails' => [
            'file' => 'MenuItemsDetails',
            'func' => 'menuItemsDetails'
        ],
        'getAllDeviceInfo' => [
		'file' => 'AllDeviceInfo',
		'func' => 'getAllDeviceInfo'
        ],
        'manageTips' => [
        'file' => 'ManageTips',
        'func' => 'manageTips'
        ]
	];

	if (array_key_exists($mode, $modeArr)) {
		$fileName = $modeArr[$mode]['file'];
		$funcName = $modeArr[$mode]['func'];
		$filePath = __DIR__."/jc_mode/".$fileName.".php";
		if (file_exists($filePath)) {
			require_once($filePath);
			$response = doProcess($funcName);
			lavu_echo(cleanJSON(json_encode($response)));
		}


	} else {
	if ($mode == 'getLatestZReportID')
	{
		$register		= reqvar("register", "");
		$register_name	= reqvar("register_name", "");
		$get_missing	= (reqvar("get_missing", "") == "1");

		if ($get_missing)
		{
			$current_report_id	= reqvar("current_report_id", "");
			$missing_ids		= getMissingZReportIDs($current_report_id, $register, $register_name);
			$response			= array( 'missing_ids' => $missing_ids );

			lavu_echo(json_encode($response)); // exits
		}
		else
		{
			$report_id = getLastZReportID($register, $register_name);
			lavu_echo($report_id); // exits
		}
	}

	if ($mode == 'insertZReport')
	{
		$ZReport = reqvar('ZReport');
		insertNewZReport($ZReport);
	}

	if ($mode == 'missing_fiscal_transactions')
	{
		$orderList = getListOfOrdersWithoutTransactionID();
		lavu_echo(json_encode($orderList));
	}

	if ($mode == 'get_customer_by_id')
	{
    	$customerRowResult = lavu_query("SELECT * FROM `med_customers` WHERE `id`='[1]'", $_POST['cust_id']);
        if (!$customerRowResult || mysqli_num_rows($customerRowResult) == 0)
		{
        	$returnArr = array("status" => "failed", "reason" => "Customer not found");
            return json_encode($returnArr);
        }
		else
		{
        	$customerRow = mysqli_fetch_assoc($customerRowResult);
            $customerJSONString = json_encode($customerRow);
            if (!empty($customerJSONString))
			{
            	echo $customerJSONString;
            }
			else
			{
            	$returnArr = array("status" => "failed", "reason" => "JSON encoding error.");
                return json_encode($returnArr);
            }
        }
    }

	$lls = isLLS($location_info);
	$remote_ip = reliableRemoteIP();

	if (in_array($mode, array( 53, "redeem_loyaltree_code", 58, "cancel_loyaltree_redemption" )))
	{
		require_once(dirname(__FILE__).'/../../../inc/loyalty/loyaltree/LoyalTreeIntegration.php');  // LoyalTree class
	}

	$newInventoryConfigured = false;
	$migrationStatus = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'INVENTORY_MIGRATION_STATUS'");
	$migrationResult = mysqli_fetch_assoc($migrationStatus);
	if ($migrationResult['value'] == "Completed_Ready" || $migrationResult['value'] == "Skipped_Ready")
	{
		$newInventoryConfigured = true;
	}

	if ($newInventoryConfigured && in_array($mode, array('waste_reasons', 'get_waste_reasons', 'get_menuitem_status', 'get_forced_modifier_status',
		'get_modifier_status', 'get_menuitem_86count', 'get_forced_modifier_86count', 'get_modifier_86count',
		'process_order', 'deliver_order', 'get_menu_status','get_menuitem_86count_status', 'get_forced_modifier_86count_status',
        'get_modifier_86count_status', 'get_menu_86count_status', 'get_86count_inventory_status'))) {
        set_sessvar("admin_dataname",$data_name);
        set_sessvar("admin_companyid", $company_id);
        set_sessvar("PHPSESSID",$recvd_sess_id);
        $ioid		 	= reqvar("ioid", "");
        $server_id	 	= reqvar('server_id', '');
        $loc_id		 	= reqvar('loc_id', '');
        $UUID		 	= reqvar('UUID', '');
        $ialid		 	= newInternalID();
        $device_time 	= reqvar('device_time', '');
        $server_time 	= UTCDateTime(TRUE);
        $app_name		= reqvar("app_name", "");
        $app_version	= reqvar("app_version", "");
        $app_build		= reqvar("app_build", "");
        $details_short = $app_name . ' ' . $app_version . ' (' . $app_build . ') ' . 'via '.$host;

        require_once(dirname(__FILE__).'/../../cp/resources/inventory_functions.php');
	}

	if (!isset($location_info['modules']))
	{
		$location_info['modules'] = "";
	}

	if (!isset($company_info['signup_package']))
	{
		$company_info['signup_package'] = "2";
	}

	if (!isset($company_info['package_status']))
	{
		$company_info['package_status'] = "2";
	}

	require_once(dirname(__FILE__)."/../modules.php");
	$active_modules = getActiveModules($location_info['modules'], (!empty($company_info['signup_package'])?$company_info['signup_package']:$company_info['package_status']), $lls);

	$jc_addons = get_addon_list($mode);
	process_jc_addons($jc_addons, $active_modules, $mode, "before");

	//Option to call this will be in the app only if they are using a fiscal printer. Otherwise it would be impossible to call for other clients.
	if ($mode == "get_check_2_order_join_of_checks_without_fiscal_receipt")
	{
		$sendJson = getCheck2OrderJoinOfChecksWithoutFiscalReceipt(reqvar("loc_id",0));
		echo $sendJson;
	}


	if ($mode == "get_all_open_orders_count_per_register"){
	    mlavu_close_db();
	    $register_name = reqvar("register_name", "");

	    $get_open_orders = lavu_query("SELECT COUNT(`order_id`) AS total FROM `orders` WHERE (`closed` = '0000-00-00 00:00:00' OR (`reopened_datetime` != '' AND `reclosed_datetime` = '')) AND `void` = '0' AND `opened` != '0000-00-00 00:00:00' AND last_mod_register_name = '".$register_name."' AND location_id=".reqvar("loc_id",0));

	    if (mysqli_num_rows($get_open_orders) == 1) {
	        $info = mysqli_fetch_assoc($get_open_orders);
	        $response['openOrdersCount'] = $info['total'];
	    }

	    lavu_echo(json_encode($response));
	}

	if ($mode == "get_order_receipt_duplicate_copy_info") {

	   mlavu_close_db();
	   $order_status = reqvar("order_status", "");
	   $get_order_info = lavu_query("SELECT email,check_has_printed FROM `split_check_details` WHERE loc_id=".reqvar("loc_id",0)." AND order_id='".reqvar("order_id",0)."' AND `check`=".reqvar("check",0)."");
	   if (mysqli_num_rows($get_order_info) == 1) {
	       $info = mysqli_fetch_assoc($get_order_info);
	       if($order_status =='reclosed') {
				if ($info['email'] != '' && $info['check_has_printed'] == 1) {
					$response['receiptOptions'] = array('email'=>1, 'print'=>1, 'status'=>'do not print');
				} else if ($info['email'] == '' && $info['check_has_printed'] == 1) {
					$response['receiptOptions'] = array('email'=>0, 'print'=>1, 'status'=>'copy');
				} else if ($info['email'] == '' && $info['check_has_printed'] == 2) {
					$response['receiptOptions'] = array('email'=>0, 'print'=>0, 'status'=>'do not print');
				}
	       } else {
    	       if ($info['email'] != '' && $info['check_has_printed'] == 1) {
				$response['receiptOptions'] = array('email'=>1, 'print'=>1, 'status'=>'do not print');
    	       } else if ($info['email'] == '' && $info['check_has_printed'] == 1) {
				$response['receiptOptions'] = array('email'=>0, 'print'=>1, 'status'=>'copy');
    	       } else if ($info['email'] != '' && $info['check_has_printed'] == 0){
				$response['receiptOptions'] = array('email'=>1, 'print'=>0, 'status'=>'copy');
    	       } else if ($info['email'] == '' && $info['check_has_printed'] == 0) {
				$response['receiptOptions'] = array('email'=>0, 'print'=>0, 'status'=>'original');
    	       }else if ($info['email'] == '' && $info['check_has_printed'] == 2) {
				$response['receiptOptions'] = array('email'=>0, 'print'=>0, 'status'=>'do not print');
    	       }
	       }
       } else {
           $response['receiptOptions'] = '';
       }

       lavu_echo(json_encode($response));
	}

//This was done during LP-8640
function getModeFile($mode) {
if (!$mode) {
				return 0;
}

$modeFiles = array (
				'check_order_payment_status' => 'checkOrderPaymentStatus.php',
);
if ($modeFiles[$mode] != '') {
$modeFile = __DIR__.'/jc_mode/'.$modeFiles[$mode];

if (file_exists($modeFile)) {
				return $modeFile;
} else {
				return 0;
}
} else {
	return 0;
}
}

$modeFile = getModeFile($mode);
if ($modeFile) {
			$response = array(
							'status' => 'fail',
			);
			require_once $modeFile;
			if (isset($response['data'])) {
							$response['status'] = 'success';
			}
			lavu_echo(cleanJSON(json_encode($response)));
} else {

	if ($mode == "punch_status") {

		mlavu_close_db();

		$loc_id = reqvar("loc_id");
		$server_id = reqvar("server_id");
		$rf_id = reqvar("rf_id", false);
		/*$break_time  = 0;
		$last_punch = lavu_query("SELECT `id`, `time`, `time_out`, `role_id`, `break` FROM `clock_punches` WHERE `_deleted` != '1' AND `server_id` = '[1]' AND `time_out` != '' ORDER BY id desc LIMIT 1", $server_id);
		$last_punch_info = mysqli_fetch_assoc($last_punch);
		$last_time_out = $last_punch_info['time_out'];

		if($last_punch_info['break'] == 1){
			$last_time_out = $last_punch_info['time_out'];
			$last_break_time = strtotime($last_time_out);
			$current_time = strtotime($device_time);
			$interval  = abs($current_time - $last_break_time);
			$break_time   = round($interval);
		}else{
			$break_time   = 0;
		}*/

		$response = array();

		if ($rf_id) {

			$get_user = lavu_query("SELECT `id`, `f_name`, `l_name` FROM `users` WHERE `rf_id` = '[1]'", $rf_id);
			if (mysqli_num_rows($get_user) == 1) {

				$info = mysqli_fetch_assoc($get_user);
				$server_id = $info['id'];
				$response['server_id'] = (string)$server_id;
				$response['server_name'] = $info['f_name']." ".$info['l_name'];

			} else if (mysqli_num_rows($get_user) > 1) {

				$response['json_status'] = "error";
				$response['error_title'] = "Status Check Failure";
				$response['error_message'] = "More than one user record found with RFID ".$rfid.".";

				lavu_echo(json_encode($response));

			} else {

				$response['json_status'] = "error";
				$response['error_title'] = "Status Check Failure";
				$response['error_message'] = "Unable to match RFID string (".$rfid.") to user record.";

				lavu_echo(json_encode($response));
			}
		}

		$punch_query = lavu_query("SELECT `punch_type`, `time`, `time_out`, `id` FROM `clock_punches` WHERE `punched_out` = '0' AND `_deleted` != '1' AND `server_id` = '[1]' AND `location_id` = '[2]' ORDER BY `time` DESC LIMIT 1", $server_id, $loc_id);
		if (mysqli_num_rows($punch_query))
		{
			$punch_read = mysqli_fetch_assoc($punch_query);
			$punch_type = $punch_read['punch_type'];
			$punch_time = $punch_read['time'];
			$punch_time_out = $punch_read['time_out'];
			$punch_id = $punch_read['id'];
			$clocked_in = "1";
		}
		else
		{
			$punch_type = "";
			$punch_time = "";
			$punch_time_out = "";
			$punch_id = "";
			$clocked_in = "0";
		}

		if($clocked_in == "0"){
			$last_punch = lavu_query("SELECT `id`, `time`, `time_out`, `role_id`, `break` FROM `clock_punches` WHERE `_deleted` != '1' AND `server_id` = '[1]' AND `time_out` != '' AND `location_id` = '[2]' AND `punched_out` != '0' ORDER BY id desc LIMIT 1", $server_id, $loc_id);
			$last_punch_info = mysqli_fetch_assoc($last_punch);

			if($last_punch_info['break'] == 1){
				$last_time_out = $last_punch_info['time_out'];
				$datetime1 = strtotime($last_time_out);
				$datetime2 = strtotime($device_time);
				$interval  = abs($datetime2 - $datetime1);
				$break_time   = round($interval);
			}else{
				$break_time   = 0;
			}
		}else{
			$break_time   = 0;
		}
		//if ($punch_type=="Shift" && $punch_time_out=="" && $punch_time!="") $clocked_in = 1; else $clocked_in = 0;

		$open_orders = array();
		if ($clocked_in=="1" && $location_info['open_orders_prevent_clock_out']=="1")
		{
			$cfoo_base_query = "SELECT `order_id`, `tablename`, `opened` FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[1]' AND `server_id` = '[2]' AND `void` = '0' AND (opened_or_reopened)";
			$cfoo_full_query = str_replace("(opened_or_reopened)", "`opened` != '0000-00-00 00:00:00' AND `opened` != '' AND `closed` = '0000-00-00 00:00:00'", $cfoo_base_query);
			$cfoo_full_query .= " UNION ".str_replace("(opened_or_reopened)", "`reopened_datetime` != '' AND `reclosed_datetime` = ''", $cfoo_base_query);
			$check_for_open_orders = lavu_query($cfoo_full_query." ORDER BY `opened` DESC", $loc_id, $server_id);
			if (mysqli_num_rows($check_for_open_orders) > 0)
			{
				while ($info = mysqli_fetch_assoc($check_for_open_orders))
				{
					$open_orders[] = array($info['order_id'], $info['tablename']);
				}
			}
		}

		$response['json_status']	= "success";
		$response['PHPSESSID']	= (string)session_id();
		$response['open_orders']	= $open_orders;
		$response['clocked_in']	= (string)$clocked_in;
		$response['time']		= (string)$punch_time;
		$response['punchid']		= (string)$punch_id;
		$response['break_out_time']	= (string)$break_time;

		lavu_echo(cleanJSON(json_encode($response)));
	}
	else if ($mode == "clock_in")
	{
		mlavu_close_db();

		$loc_id = reqvar("loc_id");
		$server_id = reqvar("server_id");
		$server_name = reqvar("server_name");
		$role_id = reqvar("role_id", "");
		$device_id = determineDeviceIdentifier($_REQUEST);

		// Put the payrate in the clock_punches table to support better reporting for multiple roles for one user.
		$payrate = "";
		$get_user_info = lavu_query("SELECT `role_id`, `payrate` FROM `users` where `id` = '[1]'", $server_id);
		if (mysqli_num_rows($get_user_info) > 0)
		{
			$user_info = mysqli_fetch_assoc($get_user_info);
			// Start by using the pay rate contained in the payrate column in the users table but allow that to be
			// overridden if they have the pipe-delimited payrate info in the role_id column.
			$payrate = $user_info['payrate'];
			if (!empty($role_id))
			{
				if (strpos($user_info['role_id'], '|'))
				{
					$role_payrates = explode(',', $user_info['role_id']);
					foreach ($role_payrates as $rp)
					{
						$rp_parts = explode('|', $rp);
						$db_role_id = $rp_parts[0];
						$db_payrate = $rp_parts[1];
						if ($db_role_id == $role_id)
						{
							$payrate = $db_payrate;
						}
					}
				}
			}
			else
			{
				$last_punch = lavu_query("SELECT `id`, `role_id` FROM `clock_punches` WHERE `_deleted` != '1' AND `server_id` = '[1]' AND `time_out` != '' AND `location_id` = '[2]' AND `punched_out` != '0' ORDER BY id desc LIMIT 1", $server_id, $loc_id);
				$last_punch_info = mysqli_fetch_assoc($last_punch);
				$role_id = $last_punch_info['role_id'];
				if (!empty($role_id))
				{
					if (strpos($user_info['role_id'], '|'))
					{
						$role_payrates = explode(',', $user_info['role_id']);
						foreach ($role_payrates as $rp)
						{
							$rp_parts = explode('|', $rp);
							$db_role_id = $rp_parts[0];
							$db_payrate = $rp_parts[1];
							if ($db_role_id == $role_id)
							{
								$payrate = $db_payrate;
							}
						}
					}
				}
			}
		}

		$p_vars = array(
			'hours'				=> "0",
			'ip_in'				=> $remote_ip,
			'location'			=> $location_info['title'],
			'location_id'		=> $loc_id,
			'needs_calculation'	=> "1",
			'payrate'			=> $payrate,
			'punch_type'		=> "Shift",
			'punched_out'		=> "0",
			'role_id'			=> $role_id,
			'server'			=> $server_name,
			'server_id'			=> $server_id,
			'time'				=> $device_time,
			'udid_in'			=> $device_id
		);

		$q_fields = "";
		$q_values = "";

		buildInsertFieldsAndValues($p_vars, $q_fields, $q_values);

		$insert_punch = lavu_query("INSERT INTO `clock_punches` (".$q_fields.", `server_time`) VALUES (".$q_values.", now())", $p_vars);

		$update_user = lavu_query("UPDATE `users` SET `clocked_in` = '1' WHERE `id` = '[1]'", $server_id);

		$log_vars = array(
			'action'		=> "Server Clocked In",
			'details'		=> roleFromRoleID($role_id),
			'details_short'	=> implode(" ", $details_short),
			'device_udid'	=> $device_id,
			'ialid'			=> newInternalID(),
			'loc_id'		=> $loc_id,
			'order_id'		=> "",
			'server_time'	=> UTCDateTime(TRUE),
			'time'			=> $device_time,
			'user'			=> $server_name,
			'user_id'		=> $server_id
		);

		$q_fields = "";
		$q_values = "";

		buildInsertFieldsAndValues($log_vars, $q_fields, $q_values);

		$log_this = lavu_query("INSERT INTO `action_log` (".$q_fields.") VALUES (".$q_values.")", $log_vars);

		$dt_parts = explode(" ", $device_time);

		$response = array(
			'json_status'	=> $insert_punch?"success":"failed",
			'PHPSESSID'		=> (string)session_id(),
			'time_in'		=> (string)$dt_parts[1],
			'full_time'		=> (string)$device_time
		);

		lavu_echo(json_encode($response));
	}
	else if ($mode == "clock_out")
	{
		mlavu_close_db();

		$response = array( "json_status" => "db_error" );

		$get_punch = lavu_query("SELECT `id`, `time`, `role_id` FROM `clock_punches` WHERE `_deleted` != '1' AND `id` = '[1]'", reqvar("punchid", ""));
		if (mysqli_num_rows($get_punch))
		{
			$punch_info = mysqli_fetch_assoc($get_punch);
			$role_id = $punch_info['role_id'];

			$clockInArray = explode(" ", $punch_info['time']);
			$clockInDate = explode("-", $clockInArray[0]);
			$clockInTime = explode(":", $clockInArray[1]);
			$inStamp = mktime($clockInTime[0], $clockInTime[1], $clockInTime[2], $clockInDate[1], $clockInDate[2], $clockInDate[0]);

			$clockOutArray = explode(" ", $device_time);
			$clockOutDate = explode("-", $clockOutArray[0]);
			$clockOutTime = explode(":", $clockOutArray[1]);
			$outStamp = mktime($clockOutTime[0], $clockOutTime[1], $clockOutTime[2], $clockOutDate[1], $clockOutDate[2], $clockOutDate[0]);

			$hours = (($outStamp - $inStamp) / 3600);

			$loc_id = reqvar("loc_id");
			$server_id = reqvar("server_id");
			$server_name = reqvar("server_name");
			$device_id = determineDeviceIdentifier($_REQUEST);
			shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $server_id $loc_id $data_name");

			$p_vars = array(
				'time_out'	=> $device_time,
				'hours'		=> number_format($hours, 3),
				'udid_out'	=> $device_id,
				'ip_out'	=> $remote_ip,
				'break'		=> (reqvar("break"))?reqvar("break"):"0"
			);

			$q_update = "";

			buildUpdate($p_vars, $q_update);

			$p_vars['id'] = $punch_info['id'];

			$update_punched_out = lavu_query("UPDATE `clock_punches` SET `punched_out` = '1', $q_update, `server_time_out` = now() WHERE id = '[id]'", $p_vars);
			$update_user = lavu_query("UPDATE `users` SET `clocked_in` = '0' WHERE `id` = '[1]'", $server_id);

			$log_vars = array(
				'action'		=> "Server Clocked Out",
				'details'		=> roleFromRoleID($role_id),
				'details_short'	=> implode(" ", $details_short),
				'device_udid'	=> $device_id,
				'ialid'			=> newInternalID(),
				'loc_id'		=> $loc_id,
				'order_id'		=> "",
				'server_time'	=> UTCDateTime(TRUE),
				'time'			=> $device_time,
				'user'			=> $server_name,
				'user_id'		=> $server_id
			);

			$q_fields = "";
			$q_values = "";

			buildInsertFieldsAndValues($log_vars, $q_fields, $q_values);

			$log_this = lavu_query("INSERT INTO `action_log` (".$q_fields.") VALUES (".$q_values.")", $log_vars);

			$dt_parts = explode(" ", $device_time);

			$response = array(
				'json_status'	=> "success",
				'PHPSESSID'		=> (string)session_id(),
				'time_in'		=> (string)$clockInArray[1],
				'time_out'		=> (string)$dt_parts[1],
				'hours'			=> (string)number_format($hours, 3),
				'full_time'		=> (string)$device_time,
				'role_id'		=> (string)$role_id
			);
		}

		lavu_echo(json_encode($response));
	}
	else if ($mode == "log_in") // log in from iPhone, iPod touch, or iPad, mode 1 prior to Lavu POS 3
	{
		if ($app_name == "Lavu Deliver")
		{
			performExternalLogin($app_name, "driver", 1);
		}
		else if ($app_name == "Lavu Task")
		{
			performExternalLogin($app_name, "maintenance", 1);
		}
		else
		{
			$cust_query = mlavu_query("SELECT `dataname` FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `username` = '[1]'", trim($_REQUEST['un']));

			if (mysqli_num_rows($cust_query) > 0)
			{
				$cust_read = mysqli_fetch_assoc($cust_query);
				$dataname = $cust_read['dataname'];
				$data_name = $dataname; // globalized for mysql logging

				$get_company_info = mlavu_query("SELECT `id`, `data_name`, `company_name`, `company_code`, `logo_img`, `receipt_logo_img`, `dev`, `demo`, `test`, `disabled`, `chain_id`, `lavu_lite_server`, `email` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname);
				if (mysqli_num_rows($get_company_info) > 0)
				{
					$company_info = mysqli_fetch_assoc($get_company_info);
					if ($company_info['disabled']=="1" || $company_info['disabled'] == "2" || $company_info['disabled'] == "3")
					{
						lavu_echo('{"json_status":"AccountDisabled"}');
					}
				}
				else
				{
					lavu_echo('{"json_status":"NoRestaurants"}');
				}

				lavu_connect_byid($company_info['id'], "poslavu_".$dataname."_db");

				$user_query = lavu_query("SELECT `id`, `quick_serve`, `service_type`, `loc_id`, `f_name`, `l_name`, `access_level`, `PIN`, `lavu_admin`, `role_id`, `_use_tunnel`, `username` FROM `users` WHERE `username` = '[1]' AND `password` = PASSWORD('[2]') AND `_deleted` != '1' AND `active` = '1'", trim($_REQUEST['un']), trim($_REQUEST['pw']));
				if (mysqli_num_rows($user_query) > 0)
				{
					$this_user_info = mysqli_fetch_assoc($user_query);
					set_sessvar('active_user_info', $this_user_info);

					// Added this session parameter because we are checking this session parameter while calling inventory API
                    set_sessvar("admin_dataname", $company_info['data_name']);
                    set_sessvar("admin_companyid",$company_info['id']);

					$valid_PINs = "";
					$valid_admin_PINs = "";
					$user_data_array = array();
					$get_all_user_data = lavu_query("SELECT * FROM `users` WHERE `_deleted` != '1' ORDER BY `l_name`, `f_name` ASC");
					while ($data_obj = mysqli_fetch_object($get_all_user_data))
					{
						if (is_null($data_obj->settings)) {
							$data_obj->settings = "";
						}
						$roles = explode(",", $data_obj->role_id);
						for ($roles_i = 0; $roles_i < count($roles); $roles_i++)
						{
							$role_parts = explode("|", $roles[$roles_i] );
							$roles[$roles_i] = $role_parts[0];
						}
						$data_obj->role_id = implode(",", $roles);

						$user_data_array[] = $data_obj;
						if ($data_obj->active == "1")
						{
							$valid_PINs .= "|".$data_obj->PIN."|";
							if ($data_obj->access_level > 1)
							{
								$valid_admin_PINs .= "|".$data_obj->PIN."|";
							}
						}
					}

					$log_vars = array(
						'action'		=> "User Logged In",
						'details_short'	=> implode(" ", $details_short),
						'device_udid'	=> determineDeviceIdentifier($_REQUEST),
						'ialid'			=> newInternalID(),
						'loc_id'		=> "",
						'order_id'		=> "",
						'server_time'	=> UTCDateTime(TRUE),
						'time'			=> $device_time,
						'user'			=> $this_user_info['f_name']." ".$this_user_info['l_name'],
						'user_id'		=> $this_user_info['id']
					);

					$q_fields = "";
					$q_values = "";

					buildInsertFieldsAndValues($log_vars, $q_fields, $q_values);

					$log_this = lavu_query("INSERT INTO `action_log` (".$q_fields.") VALUES (".$q_values.")", $log_vars);


					$response = array(
						'json_status'		=> "Valid",
						'server_id'			=> (string)$this_user_info['id'],
						'quick_serve'		=> (string)$this_user_info['quick_serve'],
						'service_type'		=> (string)$this_user_info['service_type'],
						'lavu_admin'		=> (string)$this_user_info['lavu_admin'],
						'all_user_data'		=> $user_data_array,
						'access_level'		=> (string)$this_user_info['access_level'],
						'PIN'				=> (string)$this_user_info['PIN'],
						'f_name'			=> (string)$this_user_info['f_name'],
						'l_name'			=> (string)$this_user_info['l_name'],
						'company_id'		=> (string)$company_info['id'],
						'company_name'		=> (string)$company_info['company_name'],
						'company_code'		=> (string)lsecurity_name($company_info['company_code'], $company_info['id']).":".$app_build,
						'data_name'			=> (string)$company_info['data_name'],
						'logo_img'			=> (string)$company_info['logo_img'],
						'receipt_logo_img'	=> (string)$company_info['receipt_logo_img'],
						'dev'				=> (string)$company_info['dev'],
						'demo'				=> (string)$company_info['demo'],
						'test'				=> (string)$company_info['test'],
						'lavu_lite_server'	=> (string)$company_info['lavu_lite_server'],
						'valid_PINs'		=> (string)$valid_PINs,
						'valid_admin_PINs'	=> (string)$valid_admin_PINs,
						'account_email'		=> (string)$company_info['email'],
						'allow_adv_feature' => "false"
					);

					/*
					 *  code to validate whether Lavu KDS device is allowed to login or not
					 */

					if($app_name == "Lavu KDS"){
						$cust_query = mlavu_query("SELECT custom_max_kds
														FROM `poslavu_MAIN_db`.`payment_status`
													WHERE `restaurantid` = '[1]' AND custom_max_kds!='' ",
													$company_info['id']);


						$UUID = reqvar("UUID");
						if(isset($_REQUEST['refresh']) && empty($_REQUEST['refresh'])==false){
							$activeKDS_query = lavu_query("SELECT count(id) as activeKDSCount
															FROM `devices`
														WHERE `app_name` = 'Lavu KDS' AND `slot_claimed` != '' AND `uuid`='[1]' ", $UUID);
							if(mysqli_num_rows($activeKDS_query) > 0) {
								$response['allow_adv_feature']='true';
							}
						}
						else {
							$activeKDS_query = lavu_query("SELECT count(id) as activeKDSCount
															FROM `devices`
														WHERE `app_name` = 'Lavu KDS' AND `slot_claimed` != ''");

							$activeKDS_info = mysqli_fetch_assoc($activeKDS_query);

							$kds_info = mysqli_fetch_assoc($cust_query);

							if( $activeKDS_info['activeKDSCount'] < $kds_info['custom_max_kds']) {
								/*
								 * check whether the UUID is already there for this device,
								 * if it exists then will update else Insert
								 * into device information in the devices database table
								 */
								$activeKDS_query = lavu_query("SELECT id
																FROM `devices`
															WHERE `app_name` = 'Lavu KDS' AND `uuid`='[1]'", $UUID);

								if (mysqli_num_rows($activeKDS_query) > 0) {
									$updateDevice = lavu_query("UPDATE `devices`
												SET `slot_claimed`='".date('Y-m-d')."'
											WHERE `app_name` = 'Lavu KDS' AND `uuid`='[1]'", $UUID);
								}
								else{
									$q_fields = "";
									$q_values = "";
									$data = array(
											"company_id"		=> $company_info['id'],
											"data_name"			=> $company_info['data_name'],
											"loc_id"			=> reqvar("loc_id"),
											"name"				=> reqvar("device_name", "iPad"),
											"model"				=> "iPad",
											"system_name"		=> reqvar("system_name"),
											"system_version"	=> reqvar("system_version"),
											"UDID"				=> "-",
											"poslavu_version" 	=> reqvar("app_version")." (".reqvar("app_build").")",
											"last_checkin"		=> date("Y-m-d H:i:s"),
											"needs_reload"		=> "1",
											"app_name" 			=> "Lavu KDS",
											"ip_address"		=> reqvar("local_ip"),
											"device_time"		=> reqvar("device_time"),
											"prefix"			=> "",
											"UUID"				=> $UUID,
											"MAC"				=> reqvar("MAC"),
											"inactive"			=> "1",
											"remote_ip" 		=> reliableRemoteIP(),
											"slot_claimed" 		=> date("Y-m-d"),
											"lavu_admin" 		=> reqvar("lavu_admin"),
											"holding_order_id" 	=> "",
											"usage_type"		=> "",
											"specific_model" 	=> reqvar("specific_model"),
											"reseller_admin" 	=> "0",
											"first_checkin" 	=> date("Y-m-d H:i:s"),
											"last_checkin_ts" 	=> date("Y-m-d H:i:s")
									);
									$keys = array_keys($data);
									foreach ($keys as $key) {
										if ($q_fields != "") {
											$q_fields .= ", ";
											$q_values .= ", ";
										}
										$q_fields .= "`$key`";
										$q_values .= "'[$key]'";
									}
									$insertDevice = lavu_query("INSERT INTO `devices` ($q_fields) VALUES ($q_values)", $data);
								}
								$response['allow_adv_feature']='true';
							}
						}
					}
					mlavu_close_db();
					lavu_close_db();

					lavu_echo(cleanJSON(json_encode($response)));

				} else {

					lavu_echo('{"json_status":"Invalid","query":"2"}');
				}

			} else {

				lavu_echo('{"json_status":"Invalid","query":"1"}');
			}
		}

		lavu_exit();

	} else if ($mode == "load_location_list") { // load company locations, mode 2 prior to Lavu POS 3

		$activeUserInfo = sessvar('active_user_info');

		killLavuSession();

		/*$chain_restaurants = array();
		$chain_restaurants[] = $company_info;
		if ($company_info['chain_id']!="" && is_numeric($company_info['chain_id'])) {
			$get_chain_restaurants = mlavu_query("SELECT `poslavu_MAIN_db`.`restaurants`.`id` AS `id`, `poslavu_MAIN_db`.`restaurants`.`data_name` AS `data_name`, `poslavu_MAIN_db`.`payment_status`.`force_allow_tabs_n_tables` AS `allow_tnt_mode` FROM `poslavu_MAIN_db`.`restaurants` LEFT JOIN `poslavu_MAIN_db`.`payment_status` ON `poslavu_MAIN_db`.`payment_status`.`restaurantid` = `poslavu_MAIN_db`.`restaurants`.`id` WHERE `chain_id` = '[1]' AND `data_name` != '[2]' AND `disabled` != '1' AND `disabled` != '2' AND `disabled` != '3'", $company_info['chain_id'], $data_name);
			if (mysqli_num_rows($get_chain_restaurants) > 0) {
				while ($chain_restaurant = mysqli_fetch_assoc($get_chain_restaurants)) {
					//$chain_restaurants[] = $chain_restaurant;
				}
			}
		}*/

		//echo "chain_restaurants: ".print_r($chain_restaurants, true);

		//if ($data_name == "harold_and_mau" || $data_name == "DEV") mail("richard@greenkeyconcepts.com","CHAIN RESTAURANTS",print_r($chain_restaurants, true));

		mlavu_close_db();

		$menu_id = "";
		$locations_array = loadLocations(false, $data_name, $menu_id);

		lavu_echo(cleanJSON('{"json_status":"success","PHPSESSID":"","locations":'.json_encode($locations_array).'}'));

	}
	else if ($mode == "load_location") // load table setup, printer configuration, menu categories, menu items, menu groups, language pack, mode 3 prior to Lavu POS 3
	{
		$loc_id			= reqvar("loc_id", "");
		$server_id		= reqvar("server_id", "");
		$UUID			= determineDeviceIdentifier($_REQUEST);
		$model			= reqvar("model", "");
		$for_lavu_togo	= reqvar("ltg", "0");

		register_connect("jc_inc", $company_info['id'], $data_name, $loc_id, $model, $UUID);

		$pay_stat = checkPaymentStatus();

		if ($pay_stat['kickout'])
		{
			$flag_message = array(
				'flag_kickout'	=> "1",
				'flag_message'	=> (string)$pay_stat['message'],
				'flag_title'	=> (string)$pay_stat['title'],
				'json_status'	=> "LocationFlag"
			);

			lavu_echo(cleanJSON(json_encode($flag_message)));
		}
		else
		{
			// LP-726 -- Reset session var that controls whether or not to display the trial days remaining countdown for Free Trials
			set_sessvar('pay_status_warning_ts', '');

			$message = array();
			$message['reports_list'] = array(
				array(
					'filename' 	=> "till_report.php",
					'mode'		=> "register",
					'title'		=> "Register Summary"
				),
				array(
					'filename' 	=> "till_report.php",
					'mode'		=> "server",
					'title'		=> "Server Summary"
				)
			);

			getSupportedPrinters($loc_id, $message);

			loadMenuData(reqvar("menu_id", ""), $loc_id, $for_lavu_togo, $message);

			#Addtional response for POS OLM [LP-9976]
			loadOlmConfigurationData($message);	#load OLM configuration variables

			if ($app_name=="Lavu Deliver" || $app_name=="Lavu Maintenance")
			{
				$this_server_info = getERSuserInfo($server_id);
			}
			else
			{
				$get_server_name = lavu_query("SELECT `f_name`, `l_name` FROM `users` WHERE id = '[1]'", $server_id);
				$this_server_info = mysqli_fetch_assoc($get_server_name);
			}

			$l_vars = array(
				'action'		=> "Location Chosen",
				'check'			=> "",
				'details'		=> $location_info['title'],
				'details_short'	=> implode(" ", $details_short),
				'device_udid'	=> $UUID,
				'ialid'			=> newInternalID(),
				'loc_id'		=> $loc_id,
				'order_id'		=> "",
				'server_time'	=> UTCDateTime(TRUE),
				'time'			=> $device_time,
				'user'			=> $this_server_info['f_name']." ".$this_server_info['l_name'],
				'user_id'		=> $server_id
			);

			$q_fields = "";
			$q_values = "";

			buildInsertFieldsAndValues($l_vars, $q_fields, $q_values);

			$log_this = lavu_query("INSERT INTO `action_log` (".$q_fields.") VALUES (".$q_values.")", $l_vars);

			$check_field = "UDID";
			if (isset($_REQUEST['UUID']))
			{
				$check_field = "UUID";
			}
			else if (isset($_REQUEST['MAC']))
			{
				$check_field = "MAC";
			}

			lavu_query("UPDATE `devices` SET `needs_reload` = '0' WHERE `$check_field` = '[1]'", $_REQUEST[$check_field]);

			// load components if necessary
			$component_data_return_string = "";
			$comp_pack = reqvar("comp_pack", "0");
			$cpn = reqvar("comp_pack_number");
			if ($cpn)
			{
				$key = "component_package".(($cpn > 1)?$cpn:"");
				$comp_pack = $location_info[$key];
				}
			if (!empty($comp_pack))
			{
				loadComponents($comp_pack, $active_modules, $message);
			}

			if (!empty($UUID))
			{
				$server_prefix = $lls?"8":"1";
				$device_prefix = determineDevicePrefix($loc_id, $UUID, $app_name, $model);
				$full_prefix = $server_prefix.str_pad($device_prefix, 3, "0", STR_PAD_LEFT)."-";
				$next_order_id = str_replace($full_prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $loc_id, $full_prefix));
				$message['device_prefix'] = (string)$device_prefix;
				$message['next_order_id'] = (string)$next_order_id;
			}

			$devices_info = getLocationDevices($loc_id);
			if (count($devices_info) > 0)
			{
				$message['devices_info'] = $devices_info;
			}

			$message['json_status']	= "success";
			$message['PHPSESSID']	= (string)session_id();
			$message['debug_flags']	= debugFlagsForDataname($data_name, $loc_id);

			getAvailableLanguagePacks( $message );

			global $active_language_pack_id;
			$use_language_pack = languagePackForUserID($server_id);
			$active_language_pack_id = getActiveLanguagePackId($server_id, $loc_id);
			unset_sessvar("active_language_pack");
			unset_sessvar("active_language_pack_id");
			$active_language_pack_id = $use_language_pack;
			setActiveLanguagePackId($active_language_pack_id, $server_id, $loc_id);

			$active_language_pack = getLanguageDictionary($loc_id, 'app');
			//TODO: We will remove hard coded tag
			$regenerate = false;
			if (!isset($active_language_pack['POS4_UPGRADE_NOTIFICATION'])) {
				$regenerate = true;
			}
			if (empty($active_language_pack) || $regenerate) {
				generateLanguagePack($loc_id, $active_language_pack_id, $message, TRUE, $server_id);
			} else {
				$message['language_pack'] = $active_language_pack;
			}

			$message['track_method_calls'] = "1";
			$message['inventory_configured'] = $newInventoryConfigured;
			getFiscalSetting($message);
			//TO ADD FISCAL RECEIPT TPL FIELDS INFO
			$fiscalReceiptTpl=reqvar('fiscal_receipt_tpl','');
			if($fiscalReceiptTpl && $location_info['enable_fiscal_receipt_templates'] == 1){
				$message['fiscal_receipt_tpl']= getFiscalTpl($data_name);
			}
			$message['secondary_currency']	= getSecondaryCurrency();
			$message['business_country'] = $location_info['business_country'];

			/**
			 * This condition is used to hold multiple secondary currency details.
			 */
			if ($location_info['enable_multiple_secondary_currency'] == 1) {
				$LocationCurrency = new LocationCurrency($location_info['id']);
				$timezone = $location_info['timezone'];
				$message['multiple_secondary_currency']	= $LocationCurrency->getMultipleSecondaryCurrency($timezone);
			}

			//To get Country Region Details
			$message['country_region'] =  getCountryRegionDetails();

			//To get DTT integration settings.
			if ($location_info['dtt_enabled'] == 1) {
				$device_uuid = reqvar("UUID", "");
				$message['dtt_integration'] = getDttIntegration($device_uuid);
			}

			// Environment variables to be used for the iFrame used for card connect manual entry so it is pointed to
			// the correct environment (Ref. LP-10478)
			$message['cardconnect_url'] = getenv('CARDCONNECT_URL');

			// Environment variables to be used for hype lab offline mode (Ref. OLM-262)
			$message['hype_app_identifier'] = getenv('HYPE_APP_IDENTIFIER');
			$message['hype_mode'] = getenv('HYPE_MODE');
			$message['hype_access_token'] = getenv('HYPE_ACCESS_TOKEN');

			/**
			 * This condition is used to hold 'manage_item_availability' and 'access_level_manage_item_availability'.
			 */
			if ($location_info['manage_item_availability'] == 1) {
				$krakenUrl = getenv('KRAKEN_URL');
				$message['manage_item_availability'] = $location_info['manage_item_availability'];
				$message['access_level_manage_item_availability'] = $location_info['access_level_manage_item_availability'];
				$message['kraken_url'] = $krakenUrl;
			}

			// Adding this condition because we need papi encrypted data only for paypal gateway
			// Please refer ticket LP-8840
			// Below block of code being used by POS 4.X, But it will not break POS 3.X, Becuase iOS code
			// for POS 3.X is not using this new variable
			if ((sessvar('location_info')['gateway'] == 'PayPal') || (strpos(sessvar('location_info')['modules'], 'extensions.payment.+paypal') !== false) || $location_info['manage_item_availability']) {
				require_once(__DIR__ .'/../../../inc/papi/jwtPapi.php');
				$args['dataname'] = sessvar('location_info')['data_name'];
				$args['locationid'] = $loc_id;
				$jwtPapiObj = new jwtPapi($args);
				$encryptedDataname = $jwtPapiObj->genEncryptedDataname();
				$message['ENCRYPTED_DATANAME'] = $encryptedDataname;
				if ((sessvar('location_info')['gateway'] == 'PayPal') || (strpos(sessvar('location_info')['modules'], 'extensions.payment.+paypal') !== false)) {
					$papiPaypalAuthToken = $jwtPapiObj->generatePapiAuthToken();
					$message['PAPI_PAYPAL_AUTH_TOKEN'] = $papiPaypalAuthToken;
					$message['PAYPAL_REFRESH_TOKEN_URL'] = getenv('PAYPAL_REFRESH_TOKEN_URL');
					$message['PAPI_API'] = getenv('PAPI_API');
					$message['PAYPAL_ENV'] = getenv('PAYPAL_ENV');
				}
			}

			//Show Alert on POS
			if (!isset($location_info['show_alert_on_pos'])) {
				$message['config_settings']['show_alert_on_pos'] = 0;
			}

			//Tip New Ui
			if (!isset($location_info['enable_new_server_tip_screen'])) {
				$message['config_settings']['enable_new_server_tip_screen'] = 0;
			}

			//Menu Item per row
			if (isset($location_info['menu_items_per_row']) && $location_info['menu_items_per_row'] == 3) {
				$message['config_settings']['menu_items_per_row'] = '4';
			}
			// Check if dual cash drawer PIN prompt setting access lavel value is not set then set 2 as default
			setDefaultDualDrawerAccessLevel($message);
			//Third Party Notification poll time interval 60 seconds
			$message['thirdparty_poll_interval'] = '60';

			lavu_echo(cleanJSON(json_encode($message)));
		}

	} else if ($mode == 4) { // load open order, returns number of send points

		// unused as of Lavu POS 3, replaced by load_order

	} else if ($mode == 5) { // save open order info, returns order id, replaced by mode 28

	} else if ($mode == 6) { // save order contents, replaced by mode 17)

	} else if ($mode == 7) { // load open orders

		// unused as of Lavu POS 3, replaced by load_order_list

	} else if ($mode == 8) { // load today's closed orders (can pull up voided orders as well)

		// unused as of Lavu POS 3, replaced by load_order_list

	} else if ($mode == 9) { // check open orders

		// unused as of Lavu POS 3, replaced by load_order_list

	} else if ($mode == 10) { // close order after checkout

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

		// what about jc_addons?

		//$result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);

	} else if ($mode == 11) { // update send_status for open order, save send point, log send

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 12) { // perform order void

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

		// what about jc_addons?

		//$result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);

		// what about LoyalTreeIntegration::reinstate?

	} else if ($mode == 13) { // log item edits after send

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 14) { // email receipt, not in use by app as of version 1.7, replaced by mode 43

	} else if ($mode == 15) { // save payment and discount details, not is use by app as of version 1.6.5 build 20110406-1, replaced by mode 40

	} else if ($mode == 16) { // update number of guests for order

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 17) { // save order contents only, used to merge orders

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 18) { // clock server in - not in use by app - noted 2/22/2011

	} else if ($mode == 19) { // clock server out - not in use by app - noted 2/22/2011

	} else if ($mode == 20) { // reopen closed order

		// what about jc_addons?

		//$result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);

	} else if ($mode == 21) { // open cash drawer and log it, unless it is called from checkout with no receipt or when the cash button is pressed

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 22) { // log change to order after check has printed

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 23) { // retrieve multiple check details - added to mode ?? on 4/16/2012, may be unnecessary in versions beyond 2.0.3

		// unused by Lavu POS 3

	} else if ($mode == 24) { // save multiple check details during close, not in use as of v1.7.1 and moved to mode 10

	} else if ($mode == 25) { // save order in quick serve mode, not in use in app 11-01-2010

	}
	else if ($mode == "reload_settings") // reload settings, mode 26 prior to Lavu POS 3
	{
		$flag_message = array();

		if (in_array($company_info['disabled'], array("1", "2", "3")))
		{
			$flag_message['flag_kickout']	= "2";
			$flag_message['flag_message']	= speak("Please contact support.");
			$flag_message['flag_title']		= speak("Account Inactive");
			$flag_message['json_status'] = "LocationFlag";
		}
		else
		{
			$pay_stat = checkPaymentStatus();
			if (!empty($pay_stat['message']))
			{
				$flag_message['flag_kickout']	= $pay_stat['kickout']?"1":"0";
				$flag_message['flag_message']	= $pay_stat['message'];
				$flag_message['flag_title']		= $pay_stat['title'];
				$flag_message['json_status'] = "LocationFlag";
			}
		}

		if (isset($flag_message['flag_message']))
		{
			// flag_kickout = 1 takes user to locations
			// flag_kickout = 2 takes user to welcome screen

			lavu_echo(cleanJSON(json_encode($flag_message)));
		}
		else
		{
			$user_data_array = array();
			$valid_PINs = "";
			$valid_admin_PINs = "";

			$loc_id = reqvar("loc_id", "");
			$server_id = reqvar("server_id", "");
			$UUID		= reqvar("UUID", "");

			if ($app_name=="Lavu Deliver" || $app_name=="Lavu Maintenance")
			{
				$this_user_info = getERSuserInfo($server_id);
				if (count($this_user_info) > 0)
				{
					$user_data_array[] = $this_user_info;
					$valid_PINs = "1234";
					$valid_admin_PINs = "1234";
				}
				else
				{
					lavu_echo('{"json_status":"NoU"}');
				}
			}
			else
			{
				$message = array();

				$get_user_info = lavu_query("SELECT `access_level`, `f_name`, `id`, `l_name`, `PIN`, `quick_serve`, `role_id`, `use_language_pack`, `username`, `_use_tunnel` FROM `users` WHERE `id` = '[1]'", $server_id);
				if (mysqli_num_rows($get_user_info) > 0)
				{
					$this_user_info = mysqli_fetch_assoc($get_user_info);
				}
				else
				{
					lavu_echo('{"json_status":"NoU"}');
				}

				$activeUserInfo = $this_user_info;
				$get_all_user_data = lavu_query("SELECT * FROM `users` WHERE `_deleted` != '1' ORDER BY `l_name`, `f_name` ASC");
				while ($data_obj = mysqli_fetch_object($get_all_user_data))
				{
					if (is_null($data_obj->settings)) {
						$data_obj->settings = "";
					}
					$roles = explode(",", $data_obj->role_id);
					for ($roles_i = 0; $roles_i < count($roles); $roles_i++)
					{
						$role_parts = explode("|", $roles[$roles_i] );
						$roles[$roles_i] = $role_parts[0];
					}
					$data_obj->role_id = implode(",", $roles);

					$user_data_array[] = $data_obj;
					if ($data_obj->active == "1")
					{
						$valid_PINs .= "|".$data_obj->PIN."|";
						if ($data_obj->access_level > 1)
						{
							$valid_admin_PINs .= "|".$data_obj->PIN."|";
						}
					}
				}
			}

			$menu_id = "";
			$locations_array = loadLocations($loc_id, $data_name, $menu_id);

			$message['reports_list'] = array(
				array(
					'title'		=> 'Register Summary',
					'mode'		=> 'register',
					'filename' 	=> 'till_report.php'
				),
				array(
					'title'		=> 'Server Summary',
					'mode'		=> 'server',
					'filename' 	=> 'till_report.php'
				)
			);

			getSupportedPrinters($loc_id, $message);
			$message['config_settings']['inventory_configured'] = $newInventoryConfigured;
			// Check if dual cash drawer PIN prompt setting access lavel value is not set then set 2 as default
			setDefaultDualDrawerAccessLevel($message);
			loadMenuData($menu_id, $loc_id, "0", $message);

			// load components if necessary
			$component_data_return_string = "";
			$comp_pack = reqvar("comp_pack", "0");
			$cpn = reqvar("comp_pack_number");
			if ($cpn)
			{
				$key = "component_package".(($cpn > 1)?$cpn:"");
				$comp_pack = $location_info[$key];
			}
			if (!empty($comp_pack))
			{
				loadComponents($comp_pack, $active_modules, $message);
			}

			// Storing Settings Reloaded action in error_log file instead of action_log table. Ref. LP-10212
			$logStr = 'POS ACTION ERROR - action : Settings Reloaded | loc_id : ' . $loc_id . ' | order_id :  | time : ' . $device_time . ' | user : ' . $this_user_info['f_name']." ".$this_user_info['l_name'] . ' | user_id : ' . $server_id . ' | server_time : ' . UTCDateTime(TRUE) . ' | item_id : 0 | details : ' . reqvar("server_name", "") . " (" . reqvar("server_id") . ")" . ' | check : 0 | device_udid : ' . $UUID . ' | details_short : ' . implode(" ", $details_short) . ' | ialid : ' . newInternalID() . ' | ioid :  | signature :  | key_version : ';
			error_log($logStr);
			// Commented below block of code because 'Settings Reloaded' action getting insert into DB due to below code. Ref. LP-10212
			/*
			$log_vars = array(
				'action'		=> "Settings Reloaded",
				'check'			=> "",
				'details'		=> reqvar("server_name", "")." (".reqvar("server_id").")",
				'details_short'	=> implode(" ", $details_short),
				'device_udid'	=> $UUID,
				'ialid'			=> newInternalID(),
				'loc_id'		=> $loc_id,
				'order_id'		=> "",
				'server_time'	=> UTCDateTime(TRUE),
				'time'			=> $device_time,
				'user'			=> $this_user_info['f_name']." ".$this_user_info['l_name'],
				'user_id'		=> $server_id
			);

			$q_fields = "";
			$q_values = "";

			buildInsertFieldsAndValues($log_vars, $q_fields, $q_values);

			$log_this = lavu_query("INSERT INTO `action_log` (".$q_fields.") VALUES (".$q_values.")", $log_vars);
			*/

			if (!empty($UUID))
			{
				$server_prefix = $lls?"8":"1";
				$device_prefix = determineDevicePrefix($loc_id, $UUID, $app_name, reqvar("model", ""));
				$full_prefix = $server_prefix.str_pad($device_prefix, 3, "0", STR_PAD_LEFT)."-";
				$next_order_id = str_replace($full_prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $loc_id, $full_prefix));
				$message['device_prefix'] = (string)$device_prefix;
				$message['next_order_id'] = (string)$next_order_id;
			}

			$message['json_status']		= "success";
			$message['company_code']		= (string)lsecurity_name($company_info['company_code'], $company_info['id']).":".$app_build;
			$message['dev']				= (string)$company_info['dev'];
			$message['demo']				= (string)$company_info['demo'];
			$message['test']				= (string)$company_info['test'];
			$message['company_id']		= (string)$company_info['id'];
			$message['company_name']		= (string)$company_info['company_name'];
			$message['logo_img']			= (string)$company_info['logo_img'];
			$message['receipt_logo_img']	= (string)$company_info['receipt_logo_img'];
			$message['account_email']	= (string)$company_info['email'];
			$message['valid_PINs']		= (string)$valid_PINs;
			$message['valid_admin_PINs']	= (string)$valid_admin_PINs;
			$message['location']			= $locations_array;
			$message['all_user_data']	= $user_data_array;
			$message['quick_serve']		= (string)$this_user_info['quick_serve'];
			$message['access_level']		= (string)$this_user_info['access_level'];
			$message['PIN']				= (string)$this_user_info['PIN'];
			$message['f_name']			= (string)$this_user_info['f_name'];
			$message['l_name']			= (string)$this_user_info['l_name'];
			$message['PHPSESSID']		= (string)session_id();
			$message['debug_flags']		= debugFlagsForDataname($data_name, $loc_id);

			#Addtional response for POS OLM [LP-9976]
			loadOlmConfigurationData($message);	#load OLM configuration variables


			//TO ADD FISCAL RECEIPT TPL FIELDS INFO
			$fiscalReceiptTpl=reqvar('fiscal_receipt_tpl','');
			if($fiscalReceiptTpl && $location_info['enable_fiscal_receipt_templates'] == 1){
				$message['fiscal_receipt_tpl']= getFiscalTpl($data_name);
			}

			/**
			 * This condition is used to hold multiple secondary currency details.
			 */
			if ($location_info['enable_multiple_secondary_currency'] == 1) {
				$LocationCurrency = new LocationCurrency($location_info['id']);
				$timezone = $location_info['timezone'];
				$message['multiple_secondary_currency']	= $LocationCurrency->getMultipleSecondaryCurrency($timezone);
			}

			//To get Country Region Details
			$message['country_region'] =  getCountryRegionDetails();

			//To get DTT integration settings.
			if ($location_info['dtt_enabled'] == 1) {
				$device_uuid = reqvar("UUID", "");
				$message['dtt_integration'] = getDttIntegration($device_uuid);
			}
			$message['business_country'] = $location_info['business_country'];
			/**
			 * This condition is used to hold 'manage_item_availability' and 'access_level_manage_item_availability'.
			 */
			if ($location_info['manage_item_availability'] == 1) {
				$krakenUrl = getenv('KRAKEN_URL');
				$message['manage_item_availability'] = $location_info['manage_item_availability'];
				$message['access_level_manage_item_availability'] = $location_info['access_level_manage_item_availability'];
				$message['kraken_url'] = $krakenUrl;
			}

			// Environment variables to be used for the iFrame used for card connect manual entry so it is pointed to
			// the correct environment (Ref. LP-10478)
			$message['cardconnect_url'] = getenv('CARDCONNECT_URL');

			// Environment variables to be used for hype lab offline mode (Ref. OLM-262)
			$message['hype_app_identifier'] = getenv('HYPE_APP_IDENTIFIER');
			$message['hype_mode'] = getenv('HYPE_MODE');
			$message['hype_access_token'] = getenv('HYPE_ACCESS_TOKEN');


			getAvailableLanguagePacks( $message );

			global $active_language_pack_id;
			$use_language_pack = languagePackForUserID($server_id);
			$active_language_pack_id = getActiveLanguagePackId($server_id, $loc_id);
			unset_sessvar("active_language_pack");
			unset_sessvar("active_language_pack_id");
			$active_language_pack_id = $use_language_pack;
			setActiveLanguagePackId($active_language_pack_id, $server_id, $loc_id);
			$active_language_pack = getLanguageDictionary($loc_id, 'app');
			if (empty($active_language_pack)) {
				generateLanguagePack($loc_id, $active_language_pack_id, $message, TRUE, $server_id);
			} else {
				$message['language_pack'] = $active_language_pack;
			}

			//Show Alert on POS
			if (!isset($location_info['show_alert_on_pos'])) {
				$message['config_settings']['show_alert_on_pos'] = 0;
			}

			//Tip New Ui
			if (!isset($location_info['enable_new_server_tip_screen'])) {
				$message['config_settings']['enable_new_server_tip_screen'] = 0;
			}

			//Menu Item per row
			if (isset($location_info['menu_items_per_row']) && $location_info['menu_items_per_row'] == 3) {
				$message['config_settings']['menu_items_per_row'] = '4';
			}
			//Third Party Notification poll time interval 60 seconds
			$message['thirdparty_poll_interval'] = '60';

			unset_sessvar("company_info");
			unset_sessvar("location_info");

			lavu_echo(cleanJSON(json_encode($message)));
		}

	} else if ($mode == 27) { // reload location settings (net_path, use_net_path, net_path_print) only

		// unused by Lavu POS 3

	} else if ($mode == 28) { // save order (info, contents, and modifier usage), combines modes 5 and 17, should replace them altogether in the future, returns order id

		// unused as of Lavu POS 3, replaced by lds_connect functionality

	} else if ($mode == 29) { // quick query to create a blank order and return the order_id

		// unused by Lavu POS 3

	} else if ($mode == 30) { // load orders with open deposits

		// unused as of Lavu POS 3, replaced by load_order_list

	} else if ($mode == 31) { // close open deposit as unpaid

		// candidate for deprecation from version 2.1+

		// unused by Lavu POS 3

	} else if ($mode == "check_transactions") { // load credit card transaction details and recorded payments and refunds, mode 32 prior to Lavu POS 3

		mlavu_close_db();

		$loc_id = reqvar("loc_id", "");
		$order_id = reqvar("order_id", "");

		$response = array( "json_status"=>"success", "cc_transactions"=>array() );

		$get_cc_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $loc_id, $order_id);
		if (mysqli_num_rows($get_cc_transactions) > 0) {
			while ($data_obj = mysqli_fetch_object($get_cc_transactions)) {
				$response['cc_transactions'][] = $data_obj;
			}
		}

		lavu_echo(cleanJSON(json_encode($response)));

	} else if ($mode == 33) { // save credit card transaction details, not in use by app, occurs in the gateway functions script

	} else if ($mode == 34) { // record refund, not in use by app, refunds recorded as transactions with details included

	} else if ($mode == "retrieve_item_by_upc") { // retrieve item by UPC, change quantity for qmode = inventoryQuantities, mode 35 prior to Lavu POS 3

		mlavu_close_db();

		$get_item = lavu_query("SELECT * FROM `menu_items` WHERE `UPC` = '[1]' LIMIT 1", $_REQUEST['UPC']);
		if (mysqli_num_rows($get_item) > 0) {

			if ($_REQUEST['smode'] == "order") {

				$data_obj = mysqli_fetch_object($get_item);
				lavu_echo('{"json_status":"add_to_order","item_info":'.json_encode($data_obj).'}');

			} else if ($_REQUEST['smode'] == "inventoryItems") {

				$extract = mysqli_fetch_assoc($get_item);
				lavu_echo(cleanJSON('{"json_status":"item_info","item_id":"'.$extract['id'].'","item_name":"'.$extract['name'].'","item_price":"'.$extract['price'].'","description":"'.$extract['description'].'","count":"'.$extract['inv_count'].'"}'));

			} else if ($_REQUEST['smode'] == "inventoryQuantities") {

				$extract = mysqli_fetch_assoc($get_item);

				$quantity_adj = (isset($_REQUEST['q_adj']))?$_REQUEST['q_adj']:"1";
				if ($_REQUEST['qmode'] == "minus") {
					$new_quantity = ($extract['inv_count'] - $quantity_adj);
				} else {
					$new_quantity = ($extract['inv_count'] + $quantity_adj);
				}

				$update_item = lavu_query("UPDATE `menu_items` SET `inv_count` = '[1]', last_modified_date = now() WHERE `id` = '[2]'", $new_quantity, $extract['id']);

				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `time`, `user`, `user_id`, `server_time`, `item_id`) VALUES ('Inventory Adjustment: ".$_REQUEST['qmode']." ".$quantity_adj."', '".$_REQUEST['loc_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now(), '".$extract['id']."')");

				lavu_echo(cleanJSON('{"json_status":"inventory_change","item_name":"'.$extract['name'].'","old_quantity":"'.$extract['inv_count'].'","new_quantity":"'.$new_quantity.'"}'));
			}

		} else {

			if ($_REQUEST['smode'] == "inventoryItems") {
				lavu_echo('{"json_status":"item_info","item_id":"0","item_name":"NEW ITEM","item_price":"","description":"","count":"0"}');
			} else {
				lavu_echo('{"json_status":"not_found"}');
			}
		}

		lavu_exit();

	} else if ($mode == 36) { // clear order and contents, called after adding contents to existing tab

		// candidate for deprecation from version 2.0.2+

		// unused by Lavu POS 3

	} else if ($mode == "create_or_update_item") { // save or create item from barcode scan (linea pro), mode 37 prior to Lavu POS 3

		mlavu_close_db();

		$i_vars = array();
		$i_vars['category_id'] = "112";
		$i_vars['name'] = $_REQUEST['iname'];
		$i_vars['price'] = priceFormat($_REQUEST['iprice']);
		$i_vars['description'] = $_REQUEST['idesc'];
		$i_vars['inv_count'] = $_REQUEST['icount'];
		$q_update = "";
		$keys = array_keys($i_vars);
		foreach ($keys as $key) {
			if ($q_update != "") {
				$q_update .= ", ";
			}
			$q_update .= "`$key` = '[$key]'";
		}
		$i_vars['id'] = $_REQUEST['iid'];

		if ($_REQUEST['iid'] == "0") {

			$i_vars['menu_id'] = $location_info['menu_id'];
			$i_vars['UPC'] = $_REQUEST['UPC'];
			$q_fields = "";
			$q_values = "";
			$keys = array_keys($i_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") {
					$q_fields .= ", ";
					$q_values .= ", ";
				}
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}

			$create_item = lavu_query("INSERT INTO `menu_items` ($q_fields) VALUES ($q_values)", $i_vars);
			$new_id = lavu_insert_id();

			lavu_echo('{"json_status":"item_created","new_id":"'.$new_id.'"}');

		} else {

			$update_item = lavu_query("UPDATE `menu_items` SET $q_update WHERE `id` = '[id]'", $i_vars);

			lavu_echo('{"json_status":"item_saved"}');
		}

		lavu_exit();

	} else if ($mode == "nullify_reload_flag") { // nullify needs_reload field, mode 38 prior to Lavu POS 3

		mlavu_close_db();

		$UUID = reqvar("UUID");
		if ($UUID) {
			lavu_query("UPDATE `devices` SET `needs_reload` = '0' WHERE `UUID` = '[1]'", $UUID);
		}

		lavu_echo("1");

	} else if ($mode == 39) { // clear payments and refunds

	} else if ($mode == 40) { // a new, better way of recording payments and discounts

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 41) { // log payment reassignment after item movement results in check disappearance, also updates check numbers for integrated card transactions

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 42) { // save single payment or refund and return new row id,

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	}
	else if ($mode == "email_receipt") // a new better way of emailing receipts - version 1.7 and later, mode 43 prior to Lavu POS 3
	{
		require_once(__DIR__."/../email_func.php");

		$response = sendEmailReceipt($_REQUEST);

		lavu_echo($response);
	}
	else if ($mode == "load_language_pack") // load language pack and set location language pack id, mode 44 prior to Lavu POS 3
	{
		$loc_id = reqvar("loc_id", "1");
		$pack_id = reqvar("pack_id", "1");

		$response = array( "json_status"=>"success" );

		$active_language_pack_id = $pack_id;
		unset_sessvar("active_language_pack");
		$active_language_pack = getLanguageDictionary($loc_id, 'app');
		if (empty($active_language_pack)) {
			generateLanguagePack($loc_id, $pack_id, $response, TRUE);
		} else {
			$response['language_pack'] = $active_language_pack;
		}
		lavu_echo(cleanJSON(json_encode($response)));
	}
	else if ($mode == 45) // load alternate payment totals for an order - obsolete after verison 2.1 (moved to mode 4)
	{
	// unused by Lavu POS 3
	}
	else if ($mode == 46) // void a payment or refund
	{
		// unused as of Lavu POS 3, replaced by lds_connect.php functionality
	}
	else if ($mode == "device_check_in") // device check in to replace call to print/terminal_list/index.php, mode 47 prior to Lavu POS 3
	{
		// unused if DataPlexSyncer is implemented

		require_once(__DIR__."/../deviceValidations.php");

		$server_id = reqvar("server_id");

		$response = array(
			'json_status'	=> "success",
			'needs_reload'	=> "0",
			'PHPSESSID'		=> (string)session_id()
		);

		$dvc = array(
			'app_name'			=> $app_name,
			'check_in_engine'	=> "json_connect",
			'company_id'		=> $company_info['id'],
			'data_name'			=> $data_name,
			'device_model'		=> reqvar("device_model"),
			'device_name'		=> reqvar("device_name"),
			'device_time'		=> $device_time,
			'lavu_admin'		=> reqvar("lavu_admin"),
			'loc_id'			=> reqvar("loc_id"),
			'local_ip'			=> reqvar("local_ip"),
			'MAC'				=> reqvar("MAC"),
			'poslavu_version'	=> $app_version." (".$app_build.")",
			'remote_ip'			=> $remote_ip,
			'reseller_admin'	=> isResellerAdminDevice($dvc['UUID'])?"1":"0",
			'specific_model'	=> reqvar("specific_model"),
			'SSID'				=> reqvar("SSID", ""),
			'system_name'		=> reqvar("system_name"),
			'system_version'	=> reqvar("system_version"),
			'usage_type'		=> reqvar("usage_type"),
			'UUID'				=> reqvar("UUID")
		);

		$check_device_record = lavu_query("SELECT `id`, `loc_id`, `prefix`, `system_version`, `poslavu_version`, `app_name`, `usage_type`, `needs_reload`, `inactive`, `first_checkin` FROM `devices` WHERE `UUID` = '[1]'", $dvc['UUID']);
		$matches = mysqli_num_rows($check_device_record);
		if ($matches > 0)
		{
			$this_device = mysqli_fetch_assoc($check_device_record);
			$dvc['id']					= $this_device['id'];
			$dvc['prefix']				= $this_device['prefix'];
			$dvc['holding_order_id']	= $this_device['holding_order_id'];

			$admin_action_log_insert = "INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('[aal_action]', now(), '[device_time]', '0', '[remote_ip]', '[UUID]', '[device_name]', '[detail2]', '[detail3]', '[loc_id]')";

			if ($dvc['system_version'] != $this_device['system_version'])
			{
				$dvc['aal_action']	= "iOS version changed";
				$dvc['detail2']		= "from ".$this_device['system_version'];
				$dvc['detail3']		= "to ".$dvc['system_version'];
				lavu_query($admin_action_log_insert, $dvc);
			}

			if ($dvc['app_name'] == $this_device['app_name'])
			{
				if ($dvc['poslavu_version'] != $this_device['poslavu_version'])
				{
					$dvc['aal_action']	= $dvc['app_name']." version changed";
					$dvc['detail2']		= "from ".$this_device['poslavu_version'];
					$dvc['detail3']		= "to ".$dvc['poslavu_version'];
					lavu_query($admin_action_log_insert, $dvc);
				}

				if ($dvc['usage_type'] != $this_device['usage_type'])
				{
					$dvc['aal_action']	= "Usage type changed";
					$dvc['detail2']		= "from ".$this_device['usage_type'];
					$dvc['detail3']		= "to ".$dvc['usage_type'];
					lavu_query($admin_action_log_insert, $dvc);
				}
			}
			else
			{
				$dvc['aal_action']	= "Switched apps";
				$dvc['detail2']		= "from ".$this_device['app_name'];
				$dvc['detail3']		= "to ".$dvc['app_name'];

                lavu_query($admin_action_log_insert, $dvc);
			}

			if ($dvc['loc_id'] != $this_device['loc_id'])
			{
				$dvc['prefix']				= getLowestPrefix($dvc['loc_id']);
				$dvc['holding_order_id']	= "";
				$dvc['aal_action']			= "Switched locations";
				$dvc['detail2']				= "from ".getFieldInfo($this_device['loc_id'], "id", "locations", "title");
				$dvc['detail3']				= "to ".$location_info['title'];

                lavu_query($admin_action_log_insert, $dvc);
			}
			else if ($this_device['inactive'] == "1")
			{
				$dvc['prefix']				= getLowestPrefix($dvc['loc_id']);
				$dvc['holding_order_id']	= "";
			}

			if ($matches > 1)
			{
				lavu_query("DELETE FROM `devices` WHERE `UUID` = '[1]' AND `id` != '[2]'", $dvc['UUID'], $this_device['id']);
			}

			$dvc['one_month_ago'] = date("Y-m-d H:i:s", (time() - 2419200));
			$check_for_prefix_duplicates = lavu_query("SELECT `id` FROM `devices` WHERE `loc_id` = '[loc_id]' AND `id` != '[id]' AND `prefix` = '[prefix]' AND `last_checkin` >= '[one_month_ago]'", $dvc);
			if (mysqli_num_rows($check_for_prefix_duplicates) > 0)
			{
				$new_prefix = getLowestPrefix($dvc['loc_id']);
				if (!empty($new_prefix))
				{
					$dvc['prefix'] = $new_prefix;
				}
			}

			$fill_in_first_checkin = (empty($this_device['first_checkin']))?", `first_checkin` = '[device_time]'":"";

			$update_record = lavu_query("UPDATE `devices` SET `check_in_engine` = '[check_in_engine]', `company_id` = '[company_id]', `data_name` = '[data_name]', `loc_id` = '[loc_id]', `prefix` = '[prefix]', `name` = '[device_name]', `model` = '[device_model]', `specific_model` = '[specific_model]', `system_name` = '[system_name]', `system_version` = '[system_version]', `UDID` = '-', `poslavu_version` = '[poslavu_version]', `last_checkin` = now(), `device_time` = '[device_time]', `app_name` = '[app_name]', `ip_address` = '[local_ip]', `UUID` = '[UUID]', `MAC` = '[MAC]', `lavu_admin` = '[lavu_admin]', `inactive` = '0', `remote_ip` = '[remote_ip]', `SSID` = '[SSID]', `usage_type` = '[usage_type]', `reseller_admin` = '[reseller_admin]'".$fill_in_first_checkin." WHERE `id` = '[id]'", $dvc);

			$response['needs_reload'] = (string)$lls?"0":$this_device['needs_reload'];
		}
		else
		{
			$dvc['prefix'] = getLowestPrefix($dvc['loc_id']);
			lavu_query("INSERT INTO `devices` (`check_in_engine`, `company_id`, `data_name`, `loc_id`, `name`, `model`, `specific_model`, `system_name`, `system_version`, `UDID`, `poslavu_version`, `last_checkin`, `device_time`, `app_name`, `ip_address`, `prefix`, `UUID`, `MAC`, `lavu_admin`, `inactive`, `remote_ip`, `SSID`, `usage_type`, `reseller_admin`, `first_checkin`) VALUES ('[check_in_engine]', '[company_id]', '[data_name]', '[loc_id]', '[device_name]', '[device_model]', '[specific_model]', '[system_name]', '[system_version]', '-', '[poslavu_version]', now(), '[device_time]', '[app_name]', '[local_ip]', '[prefix]', '[UUID]', '[MAC]', '[lavu_admin]', '0', '[remote_ip]', '[SSID]', '[usage_type]', '[reseller_admin]', '[device_time]')", $dvc);
		}

		$response['device_prefix'] = (string)$dvc['prefix'];

		lavu_query("UPDATE `devices` SET `inactive` = '1', `holding_order_id` = '' WHERE `last_checkin` < '[1]'", date("Y-m-d H:i:s", (time() - 604800))); // 7 days

		$devices_info = getLocationDevices($loc_id);
		if (count($devices_info) > 0)
		{
			$response['devices_info'] = $devices_info;
		}

		clearLeftoverOrderClaims($dvc['device_time'], $dvc['UUID'], reqvar("active_ioid", ""));

		// LP-2187 - Ensure Every Device Check-in Verifies The Order ID Sequence
		$server_prefix = $lls?"8":"1";
		$full_prefix = $server_prefix.str_pad($response['device_prefix'], 3, "0", STR_PAD_LEFT)."-";
		$response['next_order_id'] = (string)str_replace($full_prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $loc_id, $full_prefix));

		if ($lls && file_exists("lls-main-timestamp"))
		{
			$lines = file("lls-main-timestamp");
			$response['lls_main_timestamp'] = "LLS ".str_replace(array("-", ":", " "), array("", "", "-"), $lines[0]);
		}

		$response['server_version'] = currentLavuBackendVersion();

		$LCU = sessvar("LaCeUp"); // last central update
		if ((empty($LCU) || (time() > ((int)$LCU + 3600))) && !$lls)
		{
			set_sessvar("LaCeUp", time());
			mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now(), `app_name` = '[1]', `version_number` = '[2]', `build_number` = '[3]' WHERE `id` = '[4]'", $app_name, $app_version, $app_build, $dvc['company_id']);
			register_connect("jc_inc", $dvc['company_id'], $dvc['data_name'], $dvc['loc_id'], $dvc['device_model'], $dvc['UUID']);
		}

		$lfKickout = 0;

		if (!$lls)
		{
			$check_disabled_flag = mlavu_query("SELECT `disabled` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[company_id]' AND `disabled` != '0'", $dvc);
			if (mysqli_num_rows($check_disabled_flag) > 0)
			{
				$response = array();
				$lfKickout = 2;
				$response['json_status'] = "LocationFlag";
				$response['flag_title'] = "Account Inactive";
				$response['flag_message'] = "Please contact support.";
				$response['flag_kickout'] = "2";
			}
		}

		mlavu_close_db();

		if ($response['json_status']=="success" && $dvc['lavu_admin']!="1" && $dvc['reseller_admin']!="1" && (rand(0, 99) % 3)==0)
		{
			$lfMessage = "";
			$lfTitle = "";

			if (!isLicenseCompliant($data_name, $dvc['device_model'], $dvc['usage_type']))
			{
				if ($lfKickout > 0)
				{
					$response = array();
					$response['flag_kickout'] = (string)$lfKickout;
				}
				$response['json_status'] = "LocationFlag";
				$response['flag_message'] = $lfMessage ;
				$response['flag_title'] = $lfTitle;
			}
		}

		if ($response['json_status'] == "success")
		{
			$pay_stat = checkPaymentStatus();
			if (!empty($pay_stat['message']))
			{
				if ($pay_stat['kickout'])
				{
					$response = array();
					$lfKickout = 1;
					$response['flag_kickout'] = "1";
				}
				$response['json_status'] = "LocationFlag";
				$response['flag_message'] = (string)$pay_stat['message'];
				$response['flag_title'] = (string)$pay_stat['title'];
			}
		}

		if ($response['json_status'] == "success")
		{
			$deliver_messages = array();
			$check_device_messages = lavu_query("SELECT `id`, `ts`, `title`, `message`, `udid`, `read_by` FROM `device_messages` WHERE `read` = ''"); // limit by time since creation?
			if (mysqli_num_rows($check_device_messages) > 0)
			{
				$ts = time();
				$dt = date("Y-m-d H:i:s", $ts);
				$read_by_id = $server_id;
				$got_user_count = 0;
				while ($info = mysqli_fetch_assoc($check_device_messages))
				{
					$update_it = "";
					if ($info['udid'] == $dvc['UUID'])
					{
						$deliver_messages[] = $info;
						$update_it = " `read` = '".$dt."', `read_ts` = '".$ts."', `read_by` = '|".$read_by_id."|'";
					}
					else if ($info['udid'] == "all_servers")
					{
						if (!strstr($info['read_by'], "|".$read_by_id."|"))
						{
							$update_it = " `read_ts` = '".$ts."', `read_by` = '".$info['read_by']."|".$read_by_id."|'";
							$deliver_messages[] = $info;
						}
						if ((time() - $info['ts']) > 345600)
						{
							if ($update_it != "")
							{
								$update_it .= ", ";
							}
							$update_it .= " `read` = '".$dt."'";
						}
					}
					if ($update_it != "")
					{
						$mark_read = lavu_query("UPDATE `device_messages` SET".$update_it." WHERE `id` = '[1]'", $info['id']);
					}
				}
			}

			$msg_count = count($deliver_messages);
			if ($msg_count > 0)
			{
				$title = "Alert Messages";
				$message = "";
				foreach ($deliver_messages as $msg)
				{
					if ($message != "")
					{
						$message .= "\n\n\n";
					}
					if ($msg_count == 1)
					{
						$title = $msg['title'];
					}
					else
					{
						$message .= strtoupper($msg['title'])."\n\n";
					}
					$message .= $msg['message'];
				}
				$response['json_status'] = "LocationFlag";
				$response['flag_title'] = (string)$title;
				$response['flag_message'] = (string)str_replace(array(chr(13), chr(10), "\n"), array("", "[--CR--]", "[--CR--]"), $message);
				$response['flag_kickout'] = "0";
			}
		}

		if ($lfKickout == 0)
		{
			$check_clocked_in_users = lavu_query("SELECT `id` FROM `users` WHERE `clocked_in` = '1'");
			if (mysqli_num_rows($check_clocked_in_users) > 0)
			{
				$clocked_in_users = array();
				while ($info = mysqli_fetch_assoc($check_clocked_in_users))
				{
					$clocked_in_users[] = $info['id'];
				}
				$response['clocked_in_servers'] = $clocked_in_users;
			}

			$date_info = get_use_date_end_date($location_info, $device_time);
			$check_todays_cash_tips = lavu_query("SELECT `id`, `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $dvc['loc_id'], $server_id, $date_info[0]);
			if (mysqli_num_rows($check_todays_cash_tips) > 0)
			{
				$info = mysqli_fetch_assoc($check_todays_cash_tips);
				$response['cash_tips'] = (string)$info['value'];
			}

			$skip_order_check = reqvar("skip_order_check", false);
			if (!$skip_order_check)
			{
				$dvc['last_good_check_in'] = reqvar("last_good_check_in");
				$get_recently_changed_orders = lavu_query("SELECT `ioid`, `order_id`, `opened`, `closed`, `reopened_datetime`, `reclosed_datetime`, `send_status`, `merges`, `server`, `server_id`, `cashier`, `cashier_id`, `void`, `active_device`, `last_mod_device`, `last_mod_register_name`, `last_modified` FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[loc_id]' AND `opened` != '0000-00-00 00:00:00' AND `opened` != '' AND `last_modified` > '[last_good_check_in]' AND `last_mod_device` != '[UUID]' ORDER BY `last_modified` DESC LIMIT 250", $dvc);
				if (mysqli_num_rows($get_recently_changed_orders) > 0)
				{
					$order_info = array();
					while ($oi = mysqli_fetch_object($get_recently_changed_orders))
					{
						$order_info[$oi->ioid] = array($oi->order_id, $oi->opened, $oi->closed, $oi->reopened_datetime, $oi->reclosed_datetime, $oi->send_status, $oi->merges, $oi->server, $oi->server_id, $oi->cashier, $oi->cashier_id, $oi->void, $oi->active_device, $oi->last_mod_device, $oi->last_mod_register_name, $oi->last_modified);
					}
					$response['order_info'] = $order_info;
				}
			}
		}

		lavu_echo(cleanJSON(json_encode($response)));
	}
	else if ($mode == 48) // commit item void
	{
		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

		// what about jc_addons?

		// $result = process_jc_addons($jc_addons, $active_modules, $mode, "after", $result);

	} else if ($mode == "check_customer_rfid") { // check RFID scan against customer database, mode 49 prior to Lavu POS 3

		mlavu_close_db();

		$loc_id = reqvar("loc_id", "");
		$rfid = reqvar("RFID", "");

		$response = array( "json_status"=>"NotFound" );

		// error_title and error_message may be returned to provide custom messages to front end user

		$check_accounts = lavu_query("SELECT `id`, `f_name`, `l_name`, `email` FROM `customer_accounts` WHERE (`loc_id` = '[1]' OR `loc_id` = '0') AND `rf_id` = '[2]'", $loc_id, $rfid);
		if (mysqli_num_rows($check_accounts) > 1) {

			$response['json_status'] = "MultipleAccounts";

		} else if (mysqli_num_rows($check_accounts) == 1) {

			$response = mysqli_fetch_assoc($check_accounts);
			$response['json_status'] = "success";
		}

		lavu_echo(cleanJSON(json_encode($response)));

	} else if ($mode == "record_paid_in_paid_out") { // record paid in/out, mode 50 prior to Lavu POS 3

		// unused as of Lavu POS 3 (Build 20150427), replaced by lds_connect.php functionality

	} else if ($mode == 51) { // record mercury hosted checkout payment (also used for other hosted payment types that are hosted on a 3rd party server)

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == "update_printer_configuration") { // save in-app printer configuration changes or remove printer setting, mode 52 prior to Lavu POS 3

		mlavu_close_db();

		$loc_id = reqvar("loc_id", "");
		$assign = reqvar("assign", false);
		$remove = reqvar("remove", false);

		if ($assign) {

			$assign_printers = explode("|", $assign);

			foreach ($assign_printers as $printer) {

				$vars = array();
				$vars['value2']			= $_POST[$printer."-name"];
				$vars['value3']			= $_POST[$printer."-ip"];
				$vars['value5']			= $_POST[$printer."-port"];
				$vars['value6']			= $_POST[$printer."_command_set"];
				$vars['value7']			= $_POST[$printer."_cpl_standard"];
				$vars['value8']			= $_POST[$printer."_cpl_emphasize"];
				$vars['value9']			= $_POST[$printer."_lsf"];
				$vars['value10']			= $_POST[$printer."_raster_capable"];
				$vars['value11']			= $_POST[$printer."_perform_status_check"];
				$vars['value12']			= $_POST[$printer."_use_etb"];
				$vars['value13']			= $_POST[$printer."_skip_ping"];
				$vars['value14']			= $_POST[$printer."_sleep_seconds"];
				$vars['value_long2']		= $_POST[$printer."_special_info"];
				$special_info =			json_decode($vars['value_long2'], true);
				$vars['value4']			= (isset($special_info['brand']))?$special_info['brand']:"";
				$vars['_deleted']		= "0";

				$q_update = "";
				$keys = array_keys($vars);
				foreach ($keys as $key) {
					if ($q_update != "") {
						$q_update .= ", ";
					}
					$q_update .= "`$key` = '[$key]'";
				}

				$vars['location']	= $loc_id;
				$vars['type']		= "printer";
				$vars['setting']		= $printer;

				$q_fields = "";
				$q_values = "";
				$keys = array_keys($vars);
				foreach ($keys as $key) {
					if ($q_fields != "") {
						$q_fields .= ", ";
						$q_values .= ", ";
					}
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}

				$check1 = lavu_query("SELECT `id` FROM `config` WHERE `location` = '[location]' AND `type` = 'printer' AND `setting` = '[setting]' AND `_deleted` = '0'", $vars);
				if (mysqli_num_rows($check1) > 0) {
					$info = mysqli_fetch_assoc($check1);
					$vars['id'] = $info['id'];
					lavu_query("UPDATE `config` SET $q_update WHERE `id` = '[id]'", $vars);
				} else {
					lavu_query("INSERT INTO `config` ($q_fields) VALUES ($q_values)", $vars);
				}

				$check2 = lavu_query("SELECT `id` FROM `config` WHERE `location` = '[location]' AND `type` = '' AND `setting` = '[setting]'", $vars);
				if (mysqli_num_rows($check2) > 0) {
					$info = mysqli_fetch_assoc($check2);
					$vars['id'] = $info['id'];
					lavu_query("UPDATE `config` SET `value` = '[value2]' WHERE `id` = '[id]'", $vars);
				} else {
					lavu_query("INSERT INTO `config` (`location`, `setting`, `value`) VALUES ('[location]', '[setting]', '[value2]')", $vars);
				}

			}

			schedule_remote_to_local_sync_if_lls($location_info, $loc_id, "table updated", "config");

			$set_direct_printing = lavu_query("UPDATE `locations` SET `use_direct_printing` = '1' WHERE `id` = '[1]'", $loc_id);
			schedule_remote_to_local_sync_if_lls($location_info, $loc_id, "table updated", "locations");

		} else if ($remove) {

			$remove_printers = explode("|", $remove);

			foreach ($remove_printers as $printer) {

				lavu_query("DELETE FROM `config` WHERE `location` = '[1]' AND `type` = 'printer' AND `setting` = '[2]'", $loc_id, $printer);
				lavu_query("DELETE FROM `config` WHERE `location` = '[1]' AND `type` = '' AND `setting` = '[2]'", $loc_id, $printer);
			}
		}

		lavu_echo("1");
	}
	else if ($mode == "redeem_loyaltree_code") // check LoyalTree promo code for redemption, mode 53 prior to Lavu POS 3
	{
		$ioid		= reqvar("ioid", "");
		$order_id	= reqvar('order_id', '');
		$check		= reqvar('check_id', '1');
		$server_id	= reqvar('server_id', '');
		$code		= reqvar('code', '');

		$args = array(
			'action'        => 'redeem',
			'dataname'      => $data_name,
			'ioid'			=> $ioid,
			'order_id'      => $order_id,
			'loc_id'        => $location_info['id'],
			'businessid'    => $location_info['loyaltree_id'],
			'chainid'       => $location_info['loyaltree_chain_id'],
			'storeid'       => $company_info['id'].$location_info['id'],
			'serverid'      => $server_id,
			'code'          => $code,
			'transactionid' => $order_id.'-'.$check,
			'check'         => $check,
			'app'           => (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP),
		);

		$response_json = LoyalTreeIntegration::redeem($args);

		$response = json_decode($response_json, true);

		if ($response['json_status'] == 'success')
		{
			$args['discount_info'] = 'loyaltree|check|'. $response['id'] .'|0|'. time();

			if (!mysqli_num_rows(lavu_query("SELECT * FROM `split_check_details` WHERE `order_id` = '[order_id]' AND `check` = '[check]'", $args)))
			{
				lavu_query("INSERT INTO `split_check_details` (`ioid`, `loc_id`, `order_id`, `check`, `discount_info`) VALUES ('[ioid]', '[loc_id]', '[order_id]', '[check]', '[discount_info]')", $args);
			}
			else
			{
				lavu_query("UPDATE `split_check_details` SET `discount_info` = '[discount_info]' WHERE `order_id` = '[order_id]' AND `check` = '[check]'", $args);
			}
		}

		// Echo JSON response for app
		echo $response_json;

		lavu_exit();
	}
	else if ($mode == 54) // mark check as printed
	{
		// unused as of Lavu POS 3, replaced by lds_connect.php functionality
	}
	else if ($mode == "search_customer_accounts") // search customer_accounts for RF_ID payment (Apple Meal Plan), mode 55 prior to Lavu POS 3
	{
		mlavu_close_db();

		$vars = array();
		$vars['loc_id'] = $_REQUEST['loc_id'];
		$vars['search'] = $_REQUEST['search'];

		$filter = "`f_name` LIKE '%[search]%' OR `l_name` LIKE '%[search]%' OR `email` LIKE '%[search]%' OR `rf_id` LIKE '%[search]%'";
		if (strstr($_REQUEST['search'], " ")) {
			$search_parts = explode(" ", $_REQUEST['search']);
			$vars['f_name'] = $search_parts[0];
			$vars['l_name'] = $search_parts[1];
			$filter = "`f_name` LIKE '%[f_name]%' OR `l_name` LIKE '[l_name]%'";
		}

		$response = array( "json_status"=>"NotFound", "PHPSESSID"=>session_id() );

		$check_accounts = lavu_query("SELECT `id`, `f_name`, `l_name`, `email`, `employee_id`, `rf_id` FROM `customer_accounts` WHERE ($filter) AND (`loc_id` = '[loc_id]' OR `loc_id` = '0') ORDER BY `l_name`, `f_name`, `email` ASC LIMIT 100", $vars);
		if (mysqli_num_rows($check_accounts) > 0) {

			$response['json_status'] = "success";
			$response['found_records'] = array();

			while ($data_obj = mysqli_fetch_object($check_accounts)) {
				$response['found_records'][] = $data_obj;
			}
		}

		lavu_echo(cleanJSON(json_encode($response)));
	}
	else if ($mode == "clear_claimed_device_slot") // clear claimed device slot, mode 56 prior to Lavu POS 3
	{
		$UUID = reqvar("UUID", "");
		if (!empty($UUID))
		{
			$clear_slot = lavu_query("UPDATE `devices` SET `slot_claimed` = '' WHERE `UUID` = '[1]'", $UUID);
		}
		lavu_echo(1);
	}
	else if ($mode == "reset_session") // destroy current php session - needed for local server when client reloads settings, mode 57 prior to Lavu POS 3
	{
		killLavuSession();
		lavu_exit();
	}
	else if ($mode == "cancel_loyaltree_redemption") // cancel LoyalTree redemption, AKA reward reinstate, mode 58 prior to Lavu POS 3
	{
		$order_id = reqvar('order_id', '');
		$check = reqvar('check', '1');
		$rid = reqvar('rid', '');  // redemptionid
		$isdeal = reqvar('isdeal', '');
		$reason = reqvar('reason', '');
		$server_id = reqvar('server_id', '');

		if (empty($order_id)) {
			$res = mysqli_fetch_assoc(lavu_query("SELECT `order_id` FROM `order_contents` WHERE `idiscount_info` LIKE '%[1]%' ORDER BY `id` DESC LIMIT 1 ", $rid));
			$order_id = $res['order_id'];
		}

		$args = array(
			'action'        => 'reinstate',
			'dataname'      => $data_name,
			'order_id'      => $order_id,
			'loc_id'        => $location_info['id'],
			'check'         => $check,
			'transactionid' => $order_id.'-'.$check,
			'businessid'	=> $location_info['loyaltree_id'],
			'chainid'		=> $location_info['loyaltree_chain_id'],
			'storeid'		=> $company_info['id'].$location_info['id'],
			'serverid'	    => $server_id,
			'rid'           => $rid,
			'isdeal'		=> $isdeal,
			'reason'		=> $reason,
			'app'           => (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP),
		);

		$response = LoyalTreeIntegration::reinstate($args);

		// Echo JSON response for app
		echo $response;

		lavu_exit();

	} else if ($mode == 59) { // single out multiple quantity item for LoyalTree discount

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == "print_via_lls_queue") { // add print job to local server print queue, mode 60 prior to Lavu POS 3

		mlavu_close_db();

		require_once(dirname(__FILE__)."/../../local/print_server.php");

		$ip_addresses			= explode("|*|", $_POST['ip']);
		$ports					= explode("|*|", $_POST['port']);
		$designators			= explode("|*|", $_POST['designator']);
		$printer_names			= explode("|*|", $_POST['printer_name']);
		$command_set_ids		= explode("|*|", $_POST['command_set']);
		$image_capabilities		= explode("|*|", $_POST['image_capability']);
		$print_strings			= explode("|*|", $_POST['print_string']);
		$plain_texts			= explode("|*|", $_POST['plain_text']);

		$results = array();

		for ($pj = 0; $pj < count($ip_addresses); $pj++) {

			$ip		= $ip_addresses[$pj];
			$port	= $ports[$pj];
			$des	= $designators[$pj];
			$pname	= $printer_names[$pj];
			$csid	= $command_set_ids[$pj];
			$ic		= $image_capabilities[$pj];
			$pstr	= $print_strings[$pj];
			$ptxt	= $plain_texts[$pj];

			if ($csid == "19") {
				$ip = $_SERVER['SERVER_ADDR'];
				$port = "5288";
			}

			if (!empty($ip) && (string)$port!="" && !empty($des) && !empty($pname) && !empty($csid) && (string)$ic!="" && !empty($pstr)) {

				if (substr($pstr, 0, 10) == "DUPLICATE:") {
					$ps_parts = explode(":", $pstr);
					$di = $ps_parts[1];
					$pstr = $print_strings[$di];
					$ptxt = $plain_texts[$di];
				}

				$vars = array(
					"printer_ipaddress"	=> $ip,
					"printer_port"		=> $port,
					"designator"		=> $des,
					"printer_name"		=> $pname,
					"image_capability"	=> $ic,
					"contents"			=> $pstr,
					"plain_text"		=> $ptxt,
					"udid"				=> determineDeviceIdentifier($_POST),
					"source_ipaddress"	=> $_SERVER['REMOTE_ADDR'],
					"order_id"			=> $_POST['order_id'],
					"datetime"			=> date("Y-m-d H:i:s"),
					"ts"				=> time(),
					"status"			=> "new",
					"sent_ts"			=> "",
					"message"			=> "",
					"alert_sent"		=> "",
					"message_history"	=> ""
				);

				$result = add_to_queue($vars);
				if (!isset($result[0])) {
					$result[0] = false;
				}
				if (!$result[0]) {
					$result[1] = "db_error";
				}
				if (!isset($result[1])) {
					$result[1] = "";
				}
				if (!isset($result[2])) {
					$result[2] = "";
				}
				$result[3] = $pj;
				$results[] = $result;

			} else if (!empty($ip) || (string)$port!="" || !empty($des) || !empty($pname) || !empty($csid) || (string)$ic!="" || !empty($pstr)) {

				$missing = array();
				if (empty($ip)) {
					$missing[] = "IP Address";
				}
				if ((string)$port == "") {
					$missing[] = "Port";
				}
				if (empty($des)) {
					$missing[] = "Designator";
				}
				if (empty($csid)) {
					$missing[] = "Command Set ID";
				}
				if ((string)$ic == "") {
					$missing[] = "Image Capability";
				}
				if (empty($pstr)) {
					$missing[] = "Contents";
				}
				$result = array(false, "incomplete_data", implode(",", $missing), $pj);
				$results[] = $result;
			}
		}

		$overall_result = "";
		$did_check_last_print_ts = false;
		foreach ($results as $rslt) {
			if ($rslt[0] && !$did_check_last_print_ts) {
				$did_check_last_print_ts = true;
				$ri = $rslt[3];
				$check_ts = lavu_query("SELECT `last_print_ts` FROM `local_printer_status` WHERE (`ipaddress` = '[1]' OR (`ipaddress` = '[2]' AND `port` = '[3]')) ORDER BY `last_print_ts` DESC LIMIT 1", "queue_".$designators[$ri], $ip_addresses[$ri], $ports[$ri]);
				if (mysqli_num_rows($check_ts) > 0) {
					$info = mysqli_fetch_assoc($check_ts);
					$seconds_past = (time() - $info['last_print_ts']);
					if ($seconds_past > 60) {
						$overall_result = "queue_not_running|".floor($seconds_past / 60);
						break;
					}
				} else {
					$overall_result = "queue_not_running|";
					break;
				}
			}
			if ($overall_result != "") {
				$overall_result .= "|*|";
			}
			$overall_result .= $rslt[1]."|".$rslt[2]."|".$rslt[3];
		}

		lavu_echo($overall_result);

	} else if ($mode=="current_cash_deposit") { // retrieve current cash deposit total for a server/regsiter for the current shift, mode 61 prior to Lavu POS 3

		mlavu_close_db();

		$type = "register";
		if ($location_info['track_shifts_by']=="house" || $location_info['track_shifts_by'] == "both") {
			$type = "house";
		}

		$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
		$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

		$idatetime = explode(" ", $device_time);
		$idate = explode("-", $idatetime[0]);
		$itime = explode(":", $idatetime[1]);
		$yts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] - 1), $idate[0]);
		$nowts = mktime($itime[0], $itime[1], $itime[2], $idate[1], $idate[2], $idate[0]);
		$tts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] + 1), $idate[0]);

		if ($itime[0].$itime[1].$itime[2] >= $se_hour.$se_min."00") {
			$use_date = date("Y-m-d", $nowts);
			$end_date = date("Y-m-d", $tts);
		} else {
			$use_date = date("Y-m-d", $yts);
			$end_date = date("Y-m-d", $nowts);
		}

		$udate = explode("-",$use_date);
		$uyear = $udate[0];
		$umonth = $udate[1];
		$uday = $udate[2];
		$udate_display = date("n/j/Y",mktime(0,0,0,$umonth,$uday,$uyear));

		$end_hour = $se_hour;
		$end_min = str_pad(($se_min - 1), 2, "0", STR_PAD_LEFT);
		if ($end_min < 0) {
			$end_hour = str_pad(($se_hour - 1), 2, "0", STR_PAD_LEFT);
			$end_min = 59;
		}
		if ($end_hour < 0) {
			$end_hour = 23;
			$end_date = date("Y-m-d", $nowts);
		}

		$start_datetime = "$use_date $se_hour:$se_min:00";
		$end_datetime = "$end_date $end_hour:$end_min:59";

		$shift_start = $start_datetime;
		$check_for_shifts = lavu_query("SELECT * FROM `shifts` WHERE `loc_id` = '[1]' AND `register` = '[2]' AND `datetime` > '[3]' AND `datetime` < '[4]' AND `type` = '[5]' ORDER BY `id` DESC", $_REQUEST['loc_id'], $_REQUEST['register'], $start_datetime, $end_datetime, $type);
		if (mysqli_num_rows($check_for_shifts) > 0) {
			$info = mysqli_fetch_assoc($check_for_shifts);
			$shift_start = $info['datetime'];
		}

		$vars = array();
		$vars['loc_id'] = $_REQUEST['loc_id'];
		$vars['server_id'] = $_REQUEST['server_id'];
		$vars['register'] = $_REQUEST['register'];
		$vars['date'] = $use_date;
		$vars['start_datetime'] = $shift_start;
		$vars['end_datetime'] = $end_datetime;

		$deposited = 0;
		$get_cash_deposits = lavu_query("SELECT SUM(`value`) AS `value` FROM `cash_data` WHERE `type` = 'server_register_deposit' AND `loc_id` = '[loc_id]' AND `server_id` = '[server_id]' AND `value2` = '[register]' AND `date` = '[date]' AND `device_time` >= '[start_datetime]' AND `device_time` <= '[end_datetime]'", $vars);
		if (mysqli_num_rows($get_cash_deposits) > 0) {
			$info = mysqli_fetch_assoc($get_cash_deposits);
			$deposited = $info['value'];
		}

		$expected = 0;
		$get_expected_cash = lavu_query("SELECT SUM(IF(`cc_transactions`.`action`='Refund', 0 - `cc_transactions`.`total_collected`, `cc_transactions`.`total_collected`)) AS `amount` FROM `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` WHERE `pay_type_id` = '1' AND `cc_transactions`.`datetime` >= '[start_datetime]' AND `cc_transactions`.`datetime` <= '[end_datetime]' AND `cc_transactions`.`loc_id` = '[loc_id]' AND `orders`.`void` = '0' AND (`orders`.`location_id` = `cc_transactions`.`loc_id` OR `cc_transactions`.`order_id` = \"Paid In\" OR `cc_transactions`.`order_id` = \"Paid Out\") AND `cc_transactions`.`action` != \"\" AND `cc_transactions`.`voided` != '1'", $vars);
		if (mysqli_num_rows($get_expected_cash) > 0) {
			$info = mysqli_fetch_assoc($get_expected_cash);
			$expected = $info['amount'];
		}

		lavu_echo("1|".priceFormat($expected - $deposited));

	} else if ($mode == 62) { // offline order info sync

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == 63) { // set active device id for an order

		// unused as of Lavu POS 3, replaced by OrderManager and json_connect modes claim_order and unclaim_order

	} else if ($mode == 64) { // write action log entry only

		// unused as of Lavu POS 3, replaced by lds_connect.php functionality

	} else if ($mode == "check_offline_order_id") { // retrieve device next offline order id, mode 65 prior to Lavu POS 3

		// unused if DataPlexSyncer is implemented

		mlavu_close_db();

		$loc_id = reqvar("loc_id");
		$device_prefix = reqvar("device_prefix");

		$initial_digit = reqvar("initial_digit")?reqvar("initial_digit"):"3";
		$prefix_digits = reqvar("prefix_digits")?(int)reqvar("prefix_digits"):2;

		$prefix = $initial_digit.str_pad($device_prefix, $prefix_digits, "0", STR_PAD_LEFT)."-";

		$response = "1|".str_replace($prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $loc_id, $prefix));

		lavu_echo($response);

	} else if ($mode == 66) { // check 86 count for an item (when in-app value equals 5 or less)

		// unused as of Lavu POS 3, replaced by check_86_status periodic reconciliation of all 86 tracked items

	} else if ($mode == "reconcile_86_counts") { // unused if DataPlexSyncer is implemented

		mlavu_close_db();

		$a_diffs = array();

		$diffs = reqvar("diffs", false);
		if ($diffs) 	{
			$a_diffs = json_decode($diffs, true);
		}

		$response = array(
			"json_status"	=> "success",
			"counts"		=> array()
		);

		$check_menu_items = lavu_query("SELECT `id`, `track_86_count`, `inv_count` FROM `menu_items` WHERE `track_86_count` != '' AND `_deleted` != '1'");
		while ($info = mysqli_fetch_assoc($check_menu_items)) {
			if ($info['track_86_count'] == "86count") {
				$iid = $info['id'];
				$count = $info['inv_count'];
				if (isset($a_diffs[$iid])) {
					$d = (int)$a_diffs[$iid];
					if ($d != 0) {
						$count -= $d;
						$update_item = lavu_query("UPDATE `menu_items` SET `inv_count` = '[1]' WHERE `id` = '[2]'", $count, $iid);
					}
				}
				$response['counts'][$iid] = $count;
			}
		}

		lavu_echo(json_encode($response));

	} else if ($mode == "configure_lavu_controller") { // save configuration settings for LavuPiPrinter, mode 67 prior to Lavu POS 3

		mlavu_close_db();

		$lavu_controller = reqvar("LavuController");

		if ($lavu_controller) {

			$loc_id = reqvar("loc_id", "");

			if ($lavu_controller == "") {
				$set_lqpj = "0";
			} else {
				$set_lqpj = "1";
				$update = lavu_query("UPDATE `locations` SET `net_path_print` = 'http://[1]' WHERE `id` = '[2]'", $lavu_controller, $loc_id);
			}

			$check_config = lavu_query("SELECT `id` FROM `config` WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting` = 'lls_queue_print_jobs'", $loc_id);
			if (mysqli_num_rows($check_config) > 0) {
				$info = mysqli_fetch_assoc($check_config);
				$update = lavu_query("UPDATE `config` SET `value` = '[1]' WHERE `id` = '[2]'", $set_lqpj, $info['id']);
			} else {
				$insert = lavu_query("INSERT INTO `config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'lls_queue_print_jobs', '[2]')", $loc_id, $set_lqpj);
			}
		}

		lavu_echo("1");

	} else if ($mode == "load_send_log") { // pull send log for an order, mode 68 prior to Lavu POS 3

		mlavu_close_db();

		$loc_id = reqvar("loc_id", "");
		$order_id = reqvar("order_id", "");

		$response = array( "json_status"=>"db_error" );

		$order_ids = array($order_id);
		$check_for_merges = lavu_query("SELECT `merges` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id);
		if (mysqli_num_rows($check_for_merges) > 0) {
			$info = mysqli_fetch_assoc($check_for_merges);
			if (!empty($info['merges'])) {
				$order_ids = array_merge($order_ids, explode("|", $info['merges']));
			}
		}

		$get_send_log = lavu_query("SELECT `time_sent`, `tablename`, `course`, `total`, `send_server`, `send_server_id`, `device_udid`, `items_sent`, `resend` FROM `send_log` WHERE `location_id` = '[1]' AND `order_id` IN ('".implode("','", $order_ids)."') ORDER BY `time_sent` ASC", $loc_id);
		if (mysqli_num_rows($get_send_log) > 0) {
			$response['json_status'] = "success";
			$response['log'] = array();

			while ($data_obj = mysqli_fetch_object($get_send_log)) {
				$response['log'][] = $data_obj;
			}

		} else {

			$response['json_status'] = "no_sends";
		}

		lavu_echo(cleanJSON(json_encode($response)));

	} else if ($mode == "minor_device_check_in") { // periodic (minor) device check-in, initially added to handle grabbing clocked in status of users while in waiting_for_PIN mode, mode 69 prior to Lavu POS 3

		// unused if DataPlexSyncer is implemented

		mlavu_close_db();

		$response = array(
			"json_status"			=> "failed",
			"clocked_in_servers"	=> array()
		);

		$lavu_query_should_log = false;
		$check_users = lavu_query("SELECT `id` FROM `users` WHERE `clocked_in` = '1'");
		if ($check_users) {
			$response['json_status'] = "success";
			while ($info = mysqli_fetch_assoc($check_users)) {
				$response['clocked_in_servers'][] = $info['id'];
			}
		}

		$uuid = reqvar("UUID", "");

		if (!empty($device_time) && !empty($uuid))
		{
			clearLeftoverOrderClaims($device_time, $uuid);
		}

		lavu_echo(cleanJSON(json_encode($response)));

	} else if ($mode == "load_menu_category") {

		$response = cleanJSON(loadMenuCategory($_REQUEST));

		lavu_echo($response);
	}
	else if ($mode == "load_modifiers")
	{
		$response = cleanJSON(loadModifiers($_REQUEST));

		lavu_echo($response);
	}
	else if ($mode == "load_order_list")
	{
		mlavu_close_db();

		$device_time	= determineDeviceTime($location_info, $_REQUEST);
		$uuid			= reqvar("UUID", "");
		$auto_send		= reqvar("auto_send", ""); // LP-3129
		$lavu_admin		= reqvar("lavu_admin", "");  // LP-5956
		$ltg			= reqvar("ltg");
		$upcoming_ltg	= reqvar("upcoming_ltg", false);

		(!$ltg && !$upcoming_ltg) ? clearLeftoverOrderClaims($device_time, $uuid) : ''; //LP-6579

		$criteria = array(
			"auto_send"			=> $auto_send,
			"date_range"		=> reqvar("date_range", ""), // LP-5589, 4.0+, OrderListDateRange
			"datetime_end"		=> reqvar("datetime_end", ""), // LP-5589, 4.0+
			"datetime_start"	=> reqvar("datetime_start", ""), // LP-5589, 4.0+
			"device_time"		=> $device_time,
			"for_server_id"		=> (!empty($_REQUEST['for_server_id']))?$_REQUEST['for_server_id']:"all",
			"loc_id"			=> reqvar("loc_id"),
			"ltg"				=> reqvar("ltg"),
			"order_types"		=> reqvar("order_types", ""), // LP-5589, 4.0+
			"page"				=> reqvar("page"),
			"regular_pull"		=> false,
			"search_string"		=> reqvar("search_string"),
			"search_type"		=> reqvar("search_type"),
			"server_ids"		=> reqvar("server_ids", ""), // LP-5589, 4.0+
			"showing_ioids"		=> reqvar("showing_ioids", ""),
			"sort_mode"			=> reqvar("sort_mode", ""),
			"submode"			=> reqvar("submode"),
			"upcoming_ltg"		=> reqvar("upcoming_ltg", false),
			"UUID"				=> $uuid,
			"show_alert_on_pos" => reqvar("show_alert_on_pos")
 		);

		$response = loadOrderList($criteria);

		// LP-5956 - don't let lavu_admin pick up auto sender orders on their devices - because then those orders may never print!
		// LP-8427 - removed lavu_admin check and allowing all users to auto send orders to kitchen printers.
		// Putting lavu_admin back as suggested by Nick
		if ($auto_send == "1" && $lavu_admin != "1")
		{
			$auto_send_order = pullNextOrderForAutoSend($loc_id, $uuid);
			if (is_array($auto_send_order))
			{
				$response['auto_send_order'] = $auto_send_order;
			}
		}

		lavu_echo(cleanJSON(json_encode($response)));
	}
	else if ($mode == "load_order")
	{
		mlavu_close_db();
		$ioid			= reqvar("ioid", "");
		$order_info = loadOrderByIOID($ioid, "load_order");
		$order_info = cleanJSON(json_encode($order_info[$ioid]));

		lavu_echo($order_info);
	}
	else if ($mode == "claim_order")
	{
		mlavu_close_db();

		$response = array( 'json_status'=>"success" );

		$ioid			= reqvar("ioid", "");
		$order_id		= reqvar("order_id", "");
		$uuid			= reqvar("UUID", "");
		$last_modified	= reqvar("last_modified", "");

		updateActiveDevice(true, $ioid, $order_id, $uuid, $last_modified, $app_version);

		if (!empty($order_id) && !empty($uuid))
		{
			lavu_query("UPDATE `devices` SET `holding_order_id` = '[1]' WHERE `UUID` = '[2]'", $order_id, $uuid);
		}

		lavu_echo(json_encode($response));
	}
	else if ($mode == "unclaim_order")
	{
		mlavu_close_db();

		$response = array( 'json_status'=>"success" );

		$ioid			= reqvar("ioid", "");
		$order_id		= reqvar("order_id", "");
		$uuid			= reqvar("UUID", "");
		$last_modified	= reqvar("last_modified", "");

		if (appVersionCompareValue($app_version) >= 30200)
		{
			updateActiveDevice(false, $ioid, $order_id, $uuid, $last_modified, $app_version);
		}

		if (!empty($order_id) && !empty($uuid))
		{
			lavu_query("UPDATE `devices` SET `holding_order_id` = '' WHERE `holding_order_id` = '[1]' AND `UUID` = '[2]'", $order_id, $uuid);
		}

		/*
		 * updating signature for Norway Restaurant orders
		 *
		 */
		$nor_way = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'norway_mapping'");
		$norway_values = mysqli_fetch_assoc($nor_way);
		$norway_status=$norway_values['value'];

		if($norway_status=='1')
		{
		    updateSignatureForNorwayOrder($order_id);
		}
		/*
		 * end of signature for Norway Restaurants
		 *
		 */

		lavu_echo(json_encode($response));
	}
	else if ($mode == "ownership_request")
	{
		$response = array();
		$error_code = "";
		$error = "";

		$ioid = reqvar("ioid", "");
		if (empty($ioid))
		{
			$error_code	= "1"; // OwnershipRequestErrorMissingTargetIOID
			$error		= "Missing target order identifier";
		}

		if (empty($error_code))
		{
			$target_device_id = reqvar("target_device", "");
			if (empty($target_device_id))
			{
				$error_code	= "2"; // OwnershipRequestErrorMissingTargetUUID
				$error		= "Missing target device identifier";
			}
		}

		if (empty($error_code))
		{
			$last_checkin_ts = 0;
			$check_device_record = lavu_query("SELECT `last_checkin_ts` FROM `devices` WHERE `UUID` = '[1]'", $target_device_id);
			if ($check_device_record!==FALSE && mysqli_num_rows($check_device_record)>0)
			{
				$info = mysqli_fetch_assoc($check_device_record);
				$last_checkin_ts = $info['last_checkin_ts'];
			}

			$diff = (time() - $last_checkin_ts);
			if ($diff > 3600) // 1 hour
			{
				$interval_value = "1";
				$interval_unit = "hour";

				if ($diff > 259200) // 3 days
				{
					$interval_value = "3";
					$interval_unit = "days";
				}
				else if ($diff > 86400) // 1 day
				{
					$interval_value = "1";
					$interval_unit = "day";
				}
				else if ($diff > 28800) // 8 hours
				{
					$interval_value = "8";
					$interval_unit = "hours";
				}
				else if ($diff > 14400) // 4 hours
				{
					$interval_value = "4";
					$interval_unit = "hours";
				}

				$error_code	= "13"; // OwnershipRequestErrorDeviceInactive
				$error		= "Target device is not active";

				$response['interval_unit']	= $interval_unit;
				$response['interval_value']	= $interval_value;
			}
		}

		if (empty($error_code))
		{
			$requestor_device_id = reqvar("UUID", "");
			if (empty($requestor_device_id))
			{
				$error_code	= "3"; // OwnershipRequestErrorMissingRequestorUUID
				$error		= "Missing requestor device identifier";
			}
		}

		// setting = active device ID, target of claim request
		// type = ownership_request
		// value = IOID
		// value2 = asking device ID
		// value3 = status, blank = unfulfilled, otherwise matches OwnershipRequestError enumeration
		// value4 = unix timestamp, used to clear out excess records older than 5 minutes

		$config_id = 0;

		if (empty($error_code))
		{
			$check_existing = lavu_query("SELECT `id` FROM `config` WHERE `type` = 'ownership_request' AND `setting` = '[1]' AND `value` = '[2]' AND `value2` = '[3]'", $target_device_id, $ioid, $requestor_device_id);
			if (!$check_existing)
			{
				$error_code	= "11"; // OwnershipRequestErrorBadResponse
				$error		= "Bad response due to failed database connection";
			}
			else if (mysqli_num_rows($check_existing) > 0)
			{
				$info = mysqli_fetch_assoc($check_existing);
				$config_id = $info['id'];
				$reset_request = lavu_query("UPDATE `config` SET `value3` = '', `value4` = '[1]' WHERE `id` = '[2]'", time(), $config_id);
			}
			else
			{
				$create_request = lavu_query("INSERT INTO `config` (`type`, `setting`, `value`, `value2`, `value3`, `value4`) VALUES ('ownership_request', '[1]', '[2]', '[3]', '', '[4]')", $target_device_id, $ioid, $requestor_device_id, time());
				if ($create_request)
				{
					$config_id = lavu_insert_id();
				}
				else
				{
					$error_code	= "11"; // OwnershipRequestErrorBadResponse
					$error		= "Bad response due to failed database connection";
				}
			}
		}

		if (empty($error_code))
		{
			$start_ts = time();
			$elapsed = 0;
			$interval = (int)reqvar("interval", "10");
			$got_status = false;

			while ($elapsed <= $interval)
			{
				usleep(1000000); // wait 1 second

				$check_status = lavu_query("SELECT `value3` AS `status` FROM `config` WHERE `id` = '[1]'", $config_id);
				if ($check_status)
				{
					$info = mysqli_fetch_assoc($check_status);
					if ($info['status'] != "")
					{
						$got_status = true;
						$error_code = $info['status'];
						break;
					}
				}

				$elapsed = (time() - $start_ts);
				if ($elapsed > 30) // cap time allowed to prevent infinite loop
				{
					break;
				}
			}

			$clear_request = lavu_query("DELETE FROM `config` WHERE `id` = '[1]' OR (`type` = 'ownership_request' AND `value4` < '[2]')", $config_id, (time() - 300));

			if (!$got_status)
			{
				$error_code	= "12"; // OwnershipRequestErrorTimedOut
				$error		= "Target device did not respond in time";
			}
			else
			{
				switch ($error_code)
				{
					case "0":

						$response['ioid']		= $ioid;
						$response['uuid']		= $target_device_id;
						$response['order_data']	= claimAndPullIOID($ioid, $requestor_device_id);
						break;

					case "9": // OwnershipRequestErrorOrderIsBeingEdited

						$error = "Target order is being edited";
						break;

					case "14": // OwnershipRequestErrorWrongDevice

						$error = "Target device no longer holds claim to this order";
						break;

					default:

						$error = "Unknown error";
						break;
				}
			}
		}

		$response['error_code'] = $error_code;
		if (!empty($error))
		{
			$response['error'] = $error;
		}

		lavu_echo(json_encode($response));
	}
	else if ($mode == "pull_orders")
	{
		mlavu_close_db();

		$loc_id				= reqvar("loc_id");
		$max_orders			= reqvar("max_orders", 5);
		$pull_ioids			= (!empty($_REQUEST['pull_ioids']))?explode(",", $_REQUEST['pull_ioids']):array();
		$pull_open_orders	= (int)reqvar("pull_open_orders", "0");
		$last_pull			= reqvar("last_sync", "");
		$uuid				= reqvar("UUID", "");

		$response = pullOrders($loc_id, $max_orders, $pull_ioids, $pull_open_orders, $last_pull, 0, $uuid);

		lavu_echo(cleanJSON(json_encode($response)));
	}
	else if ($mode == "transition_details")
	{
		$uuid=reqvar("UUID", "");
		$device_time= reqvar("device_time", "");
		$server_id=reqvar("server_id", "");
		$register=reqvar("active_register", "");
		$data=getTransitionFullDetails($uuid,$server_id,$device_time,$register);
		lavu_echo(cleanJSON(json_encode($data)));
	}
	else if ($mode == "user_exited_location")
	{
		$UUID = reqvar("UUID", "");
		if (!empty($UUID))
		{
			$deactivate_device = lavu_query("UPDATE `devices` SET `inactive` = '1', `slot_claimed` = '' WHERE `UUID` = '[1]'", $UUID);
		}
		lavu_echo(1);
	} else if ($mode == 'waste_reasons' && $newInventoryConfigured) {
        $itemId				= reqvar('item_id', '');
        $wasteReason		= reqvar('waste_reason', '');
        $userID				= reqvar('server_id', "");
        $wasteQuantity		= reqvar('waste_quantity', '');
        $forcedModifierId   = reqvar('forced_modifier_id', '');
        $modifierId   		= reqvar('optional_modifier_id', '');
        $inventoryWasteAuditRequest[0]['id'] 				= $itemId;
        $inventoryWasteAuditRequest[0]['wasteQuantity'] 	= $wasteQuantity;
        $inventoryWasteAuditRequest[0]['userID'] 			= $userID;
        $inventoryWasteAuditRequest[0]['wasteReason'] 		= $wasteReason;
        $inventoryWasteAuditRequest[0]['forcedModifierID'] 	= json_decode($forcedModifierId);
        $inventoryWasteAuditRequest[0]['modifierID'] 		= json_decode($modifierId);

        $inventoryWasteAuditResponse = insert_waste_inventory_audits(json_encode($inventoryWasteAuditRequest, JSON_NUMERIC_CHECK));
        lavu_echo(cleanJSON(json_encode($inventoryWasteAuditResponse)));
	} else if ($mode == 'get_waste_reasons' && $newInventoryConfigured) {
        $wasteReasonsResponse = get_waste_reasons();
        lavu_echo(cleanJSON(json_encode($wasteReasonsResponse)));
	} else if ($mode == 'get_menuitem_status' && $newInventoryConfigured) {
		$itemId				= reqvar('item_id', '');
        $itemIdRequest		= json_decode($itemId, true);
        $getMenuItemStatusResponse = get_menuitem_status($itemIdRequest);
        lavu_echo(cleanJSON(json_encode($getMenuItemStatusResponse)));
	} else if ($mode == 'get_forced_modifier_status' && $newInventoryConfigured) {
        $forcedModifierID = reqvar('forced_modifier_id', '');
        $forcedModifierIdRequest = json_decode($forcedModifierID, true);
        $forcedModifierStatusResponse = get_forced_modifier_status($forcedModifierIdRequest);
        lavu_echo(cleanJSON(json_encode($forcedModifierStatusResponse)));
    } else if ($mode == 'get_modifier_status' && $newInventoryConfigured) {
        $modifierID			= reqvar('optional_modifier_id', '');
        $modifierIdRequest		= json_decode($modifierID, true);
        $modifierStatusResponse = get_modifier_status($modifierIdRequest);
        lavu_echo(cleanJSON(json_encode($modifierStatusResponse)));
	} else if ($mode == 'get_menuitem_86count' && $newInventoryConfigured) {
        $itemId				= reqvar('item_id', '');
        $itemIdRequest		= json_decode($itemId, true);
        $menuItemCountResponse = get_menuitem_86count($itemIdRequest);
        lavu_echo(cleanJSON(json_encode($menuItemCountResponse)));
    } else if ($mode == 'get_forced_modifier_86count' && $newInventoryConfigured) {
        $forcedModifierID = reqvar('forced_modifier_id', '');
        $forcedModifierRequest = json_decode($forcedModifierID, true);
        $forcedModifierCountResponse = get_forced_modifier_86count($forcedModifierRequest);
        lavu_echo(cleanJSON(json_encode($forcedModifierCountResponse)));
    } else if ($mode == 'get_modifier_86count' && $newInventoryConfigured) {
        $modifierID			= reqvar('modifier_id', '');
        $modifierRequest	= json_decode($modifierID, true);
        $modifierCountResponse = get_modifier_86count($modifierRequest);
        lavu_echo(cleanJSON(json_encode($modifierCountResponse)));
    } else if ( $mode == 'process_order' && $newInventoryConfigured) {
		$menuOrder = reqvar('menu_order', '');
        $menuOrderRequest = json_decode($menuOrder);
        $menuOrderResponse = process_order(json_encode($menuOrderRequest, JSON_NUMERIC_CHECK));
        $log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `time`, `user`, `user_id`, `server_time`, `details`, `device_udid`, `details_short`, `ialid`, `ioid`) VALUES ('Inventory Process Order', '".$loc_id."', '".$device_time."', '', '".$server_id."', '" . $server_time . "', '" . $menuOrder . "', '" . $UUID. "', '" . $details_short . "' , '" . $ialid . "', '" . $ioid . "')");
        lavu_echo(cleanJSON(json_encode($menuOrderResponse)));
	} else if ( $mode == 'deliver_order' && $newInventoryConfigured) {
		$menuOrder = reqvar('menu_order', '');
		$menuOrderRequest = json_decode($menuOrder);
		$menuOrderResponse = deliver_order(json_encode($menuOrderRequest, JSON_NUMERIC_CHECK));
		$menuStatusRequest = json_decode($menuOrder, true);
		$menuRemovedStatusRequest = $menuStatusRequest[0]['removed'];
		$statusFlag = false;
		if (!empty($menuRemovedStatusRequest['item_id'])) {
			foreach ($menuRemovedStatusRequest['item_id'] as $key => $value) {
				$menuRemovedStatusRequest['item_id'][$key]['id'] = $value['id'];
				$menuRemovedStatusRequest['item_id'][$key]['quantity'] = 1;
			}
			$statusFlag = true;
		} else {
			$menuRemovedStatusRequest['item_id'] = array();
		}
		if (!empty($menuRemovedStatusRequest['forced_modifier_id'])) {
			foreach ($menuRemovedStatusRequest['forced_modifier_id'] as $key => $value) {
				$menuRemovedStatusRequest['forced_modifier_id'][$key]['id'] = $value['id'];
				$menuRemovedStatusRequest['forced_modifier_id'][$key]['quantity'] = 1;
			}
			$statusFlag = true;
		} else {
			$menuRemovedStatusRequest['forced_modifier_id'] = array();
		}
		if (!empty($menuRemovedStatusRequest['optional_modifier_id'])) {
			foreach ($menuRemovedStatusRequest['optional_modifier_id'] as $key => $value) {
				$menuRemovedStatusRequest['optional_modifier_id'][$key]['id'] = $value['id'];
				$menuRemovedStatusRequest['optional_modifier_id'][$key]['quantity'] = 1;
			}
			$statusFlag = true;
		} else {
			$menuRemovedStatusRequest['optional_modifier_id'] = array();
		}

		$menuRemovedJsonRequest[0] = $menuRemovedStatusRequest;
		if ($statusFlag) {
			$menuOrderResponse = get_menu_status(json_encode($menuRemovedJsonRequest, JSON_NUMERIC_CHECK));
		}
		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `time`, `user`, `user_id`, `server_time`, `details`, `device_udid`, `details_short`, `ialid`, `ioid`) VALUES ('Inventory Deliver Order', '".$loc_id."', '".$device_time."', '', '".$server_id."', '" . $server_time . "', '" . $menuOrder . "', '" . $UUID. "', '" . $details_short . "' , '" . $ialid . "', '" . $ioid . "')");
		lavu_echo(cleanJSON(json_encode($menuOrderResponse)));
	} else if ( $mode == 'get_menu_status' && $newInventoryConfigured) {
        $menuStatus = reqvar('menu_status', '');
        $menuStatusRequest = json_decode($menuStatus);
        $menuStatusResponse = get_menu_status(json_encode($menuStatusRequest, JSON_NUMERIC_CHECK));
        lavu_echo(cleanJSON(json_encode($menuStatusResponse)));
    } else if ($mode == 'get_menuitem_86count_status' && $newInventoryConfigured) {
		$itemId				= reqvar('item_id', '');
		$itemIdRequest		= json_decode($itemId, true);
		$getMenuItemStatusResponse = get_menuitem_86count_status($itemIdRequest);
		lavu_echo(cleanJSON(json_encode($getMenuItemStatusResponse)));
	} else if ($mode == 'get_forced_modifier_86count_status' && $newInventoryConfigured) {
		$forcedModifierID = reqvar('forced_modifier_id', '');
		$forcedModifierIdRequest = json_decode($forcedModifierID, true);
		$forcedModifierStatusResponse = get_forced_modifier_86count_status($forcedModifierIdRequest);
		lavu_echo(cleanJSON(json_encode($forcedModifierStatusResponse)));
	} else if ($mode == 'get_modifier_86count_status' && $newInventoryConfigured) {
        $modifierID = reqvar('optional_modifier_id', '');
        $modifierIdRequest = json_decode($modifierID, true);
        $modifierStatusResponse = get_modifier_86count_status($modifierIdRequest);
        lavu_echo(cleanJSON(json_encode($modifierStatusResponse)));
    } else if ( $mode == 'get_menu_86count_status' && $newInventoryConfigured) {
        $menuStatus = reqvar('menu_status', '');
        $menuStatusRequest = json_decode($menuStatus);
        $menuStatusResponse = get_menu_86count_status(json_encode($menuStatusRequest, JSON_NUMERIC_CHECK));
        lavu_echo(cleanJSON(json_encode($menuStatusResponse)));
    } else if ( $mode == 'get_86count_inventory_status' && $newInventoryConfigured) {
        $categoryId = reqvar('category_id', '');
        $categoryRequest = json_decode($categoryId, true);
        $inventoryStatusResponse = get_86count_inventory_status($categoryRequest);
        lavu_echo(cleanJSON(json_encode($inventoryStatusResponse)));
    } else if ($mode == "xml_uruguay"){
		$device_time= reqvar("device_time", "");
		$xml_data= reqvar("required_order_details", "");
		$data=toCreateXmlUruguay($device_time,$xml_data);
		lavu_echo(cleanJSON(json_encode($data)));
	//FOR SACOA
    } else if (strtolower($mode) === "sacoa"){
    	global $data_name;
    	if($data_name == ""){
    		$data_name = admin_info('dataname');
    	}

	//get sacoa extension
	$result = mlavu_query("SELECT id from extensions WHERE title='Sacoa Playcard'");
	$rec = mysqli_fetch_assoc($result);
	$extensionId=$rec['id'];

	if($extensionId) {

      		$card_action = reqvar("action", "");
        	require_once(__DIR__."/SacoaApi.php");
        	$apiObj = new SacoaApi();
		$apiObj->dataname=$data_name;
		$apiObj->extensionId=$extensionId;
		$apiObj->init();
        	if($card_action == ""){
                	$card_action = "getApiLogin";
        	}

        	$sacoa_url = $apiObj->apiHost."/login";

		$rtn_response = array();
		$check_token_expiry = mlavu_query("SELECT `token_expiration_date` FROM `gateway_auth_tokens` WHERE _deleted=0 AND `dataname` = '[1]' AND `extension_id` = '[2]' AND token_expiration_date > now() ", $data_name, $extensionId);
    		$num_of_rows = mysqli_num_rows($check_token_expiry);

		//IF TOKEN EXPIRES, REGNERATE NEW TOKEN THROUGH SACOA API
    		if(!$num_of_rows){
	                $result=mlavu_query('SELECT token from auth_tokens WHERE extension_id='.$extensionId.' AND dataname="'.$data_name.'" AND token_type="secret"');
        	        $rec = mysqli_fetch_assoc($result);
                	if($rec['token']){
                        	$data=explode('|',$rec['token']);
                        	$sacoa_username=$data[0];
                        	$sacoa_pwd=base64_decode($data[1]);
                        	$sacoa_req = array("user" => $sacoa_username, "pass" => $sacoa_pwd, "computerId" => "101");
                        	$sacoa_req_string = json_encode($sacoa_req);
                	}
    			mlavu_query("DELETE FROM `poslavu_MAIN_db`.`gateway_auth_tokens` WHERE `extension_id` = '[1]' AND `dataname` = '[2]'", $extensionId, $data_name);
 			$response = $apiObj->sacoaApi($sacoa_req_string, $sacoa_url, 1);
		}
		$customer_id = reqvar("customer_id", "");
	    	$card_number = reqvar("card_number", "");
	    	$amount = reqvar("amount", "");
	    	$server_name = reqvar("server_full_name", "");
	    	$target_card = reqvar("target_card_number", "");
	    	$order_id = reqvar("order_id", "");
	    	$transaction_id = reqvar("transaction_id", "");
	    	switch($card_action){
	    		case 'getApiLogin':
	    			$response = $apiObj->sacoaApi($sacoa_req_string, $sacoa_url, 1);
	    			$res_data = json_decode($response, 1);
	    			lavu_echo(cleanJSON(json_encode($response)));
	    		break;
	    		case 'IssueNewCard':
	    			$response = $apiObj->issueNewCard($customer_id, $card_number, $amount, $server_name);
	    			lavu_echo(format_sacoa_response($response));
	    		break;
	    		case 'ReloadCard':
	    			$response = $apiObj->reloadCard($customer_id, $card_number, $amount, $card_action);
	    			lavu_echo(format_sacoa_response($response));
	    		break;
	    		case 'ViewBalance':
	    			$response = $apiObj->viewBalance($customer_id, $card_number);
	    			lavu_echo(format_sacoa_response($response));
	    		break;
	    		case 'DebitAmount':
	    			$response = $apiObj->debitAmount($customer_id, $card_number, $amount);
	    			lavu_echo(format_sacoa_response($response));
	    		break;
	    		case 'TransferAmount':
	    			$response = $apiObj->transferAmount($card_number, $target_card, $amount, $server_name);
	    			lavu_echo(format_sacoa_response($response));
	    		break;
	    		case 'RefundAmount':
	    			$response = $apiObj->refundAmount($order_id, $transaction_id, $amount, $server_name);
	    			lavu_echo(format_sacoa_response($response));
	    		break;
	    		case 'TicketsRedemption':
	    			$response = $apiObj->ticketsRedemption($customer_id, $card_number, $amount);
	    			lavu_echo(format_sacoa_response($response));
	    		break;
	    	}
    	} else{
    		$apiObj->setErr($rtn_response,'SACOA_ERR_12');
    		$rtn_response['flag'] = "0";
    		$rtn_response['txnid'] = "";
    		$rtn_response['order_id'] = "";
    		lavu_echo(format_sacoa_response($rtn_response));
    	}
    } else if ($mode == "pickup_seq"){
    	$device_time = reqvar("device_time", "");
    	$loc_id = reqvar("loc_id", "");
    	$server_id = reqvar("UUID");
    	$order_id = reqvar("order_id", "");
    	$data=toCreatePickupSequence($device_time,$loc_id,$server_id,$order_id);
    	lavu_echo(cleanJSON(json_encode($data)));
	} else if ($mode == 'cashdrawer') {
		$data['action'] = reqvar("action", "");
		$data['dataname']=$data_name;
		$data['printer']=reqvar("printer","");
		$data['drawer']=reqvar("drawer","");
		$data['serverId']=reqvar("server_id","");
		$data['assignedBy']=reqvar("assigned_by","");
		$data['sourceDevice']=reqvar("srcdevice","");

		require_once(__DIR__."/ServerCashDrawerApi.php");
		$apiObj = new ServerCashDrawerApi($data_name,$data);
		$res=$apiObj->doProcess();
		lavu_echo($res);
     } else if($mode == "paypalCapture"){
        $device_time = reqvar("device_time", "");
        $loc_id = reqvar("loc_id", "");
        $orderDtls = json_decode(reqvar("required_order_details",""),true);
        $order_id = $orderDtls['order_id'];
        $transaction_id = $orderDtls['transaction_id'];
        $check = $orderDtls['check'];
        $device_udid = $orderDtls['device_udid'];
        $company_id = $orderDtls['company_id'];
        $server_id = $orderDtls['server_id'];
        $server_name = $orderDtls['server_name'];
        require_once(__DIR__."/../gateway_lib/paypal_func.php");
        $data = getPaypalCaptureDetails($order_id, $transaction_id, $check, $device_time, $loc_id, $server_id, $server_name, $company_id, $device_udid);
        lavu_echo(cleanJSON(json_encode($data)));
     } else if ($mode == "checkStatusLIP"){
         $loc_id		= reqvar('loc_id', '');
         $ref_id		= reqvar("transaction_id", "");
         $terminal_id  	= reqvar("ref_data", "");
         $order_id		= reqvar("order_id", "");
         $check 		= reqvar("check", "");
         $ioid			= reqvar("ioid", "");
         $reskin		= reqvar("reskin", "");
         $data = getTransactionStatusLip($ref_id, $terminal_id, $loc_id, $order_id, $check, $ioid, $reskin);
         lavu_echo($data);
     } else if ($mode == "getTransactionIdLIP"){
        $order_id				= reqvar("order_id", "");
        $check					= reqvar("check", "");
        $check_total_amount		= reqvar("check_total_amount", 0);
        $transaction_amount		= reqvar("transaction_amount", 0);
        $terminal_id			= reqvar("terminal_id", "");
        $reskin					= reqvar("reskin", "");
        $data = getLipTransactionId($order_id, $check, $check_total_amount, $transaction_amount, $terminal_id, $reskin);
		lavu_echo($data);
	 } else if ($mode == "refundVoidDeleteTransactionLIP") {
		$order_id	= reqvar("order_id", "");
		$check		= reqvar("check", "");
		$amount		= reqvar("amount", "");
		$ref_id		= reqvar("transaction_id", "");
		$delete 	= reqvar("delete", 0);
		$data = refundVoidDeleteTransactionLIP($order_id, $check, $amount, $ref_id, $delete);
		lavu_echo($data);
	} else if ($mode == "pendingTransactionsLIP"){
		$order_id	= reqvar("order_id", "");
		$ioid		= reqvar("ioid", "");
		$data = getPendingEconduitOrders($order_id, $ioid);
		lavu_echo($data);
     } else if (strtolower($mode) === "fiscal_receipt_tpl_pos"){
		$data['action'] = reqvar("action", "");
		$data['dataname']=$data_name;
		$data['mode'] = $mode;
	    require_once(__DIR__."/FiscalReceiptTplApi.php");
	    $apiObj = new FiscalReceiptTplApi($data_name,$data);
	   	$res=$apiObj->doProcess();
	    lavu_echo($res);
     } else if (strtolower($mode) === "fiscal_receipt_invoice_pos"){
		$data['action'] = reqvar("action", "");
		$data['server_id'] = reqvar("server_id", "");
		$data['order_id'] = reqvar("order_id", "");
		$data['dataname']=$data_name;
		$data['mode'] = $mode;
	    require_once(__DIR__."/FiscalReceiptInvoiceApi.php");
	    $apiObj = new FiscalReceiptInvoiceApi($data_name,$data);
	    $res=$apiObj->doProcess();
	    lavu_echo($res);
     } else if ($mode == "insertCustomSecondaryExchangeRate"){
         $exchangeRateData = array();
         $exchangeRateData['primary_currency_code'] = reqvar("primary_currency_code", "");
         $exchangeRateData['secondary_currency_id'] = reqvar("secondary_currency_id", "");
         $exchangeRateData['secondary_currency_code'] = reqvar("secondary_currency_code", "");
         $exchangeRateData['rate'] = reqvar("rate", "");
         $data=insertCustomSecondaryExchangeRate($exchangeRateData);
         lavu_echo(cleanJSON(json_encode($data)));
     } else if ( $mode == "getDepositReceipt" ) {
         $depositId = reqvar( "depositId", "" );
         if ( trim($depositId) !="" ) {
             $data=getDepositReceipt( $depositId );
             lavu_echo( ( $data ) );
         }
     } else if ( $mode == "dttDevicesPairInfo" ) {
     	$deviceUUID = reqvar("UUID", "");
     	$deviceTime = reqvar("device_time", "");
     	$serverId = reqvar("server_id", "");
     	$pair = json_decode(reqvar("paired",""),true);
     	$unpair = json_decode(reqvar("unpaired",""),true);
     	$pairInfo = getDttPairing($deviceUUID, $serverId, $deviceTime, $pair, $unpair);
     	lavu_echo(cleanJSON(json_encode($pairInfo)));
     }

	if ($mode == "lavupay:terminals:list") {
		try {
			require_once(dirname(__FILE__).'/../gateway_lib/cardconnect/terminal.php');
			$dataname = sessvar('location_info')['data_name'];
			$locationid = $loc_id;

			$terminals = new PaymentGateways\CardConnect\Terminal($dataname, $locationid);
			$result = $terminals->listTerminals();

			lavu_echo(json_encode($result));
		} catch (Exception $err) {
			error_log($err);
			lavu_echo(json_encode(array("error" => $err->getMessage())));
		}
	}

	if ($mode === "lavupay:terminals") {
		try {
			$action = reqvar("action", "");
			require_once(dirname(__FILE__).'/../gateway_lib/cardconnect/terminal.php');
			require_once(dirname(__FILE__).'/../gateway_lib/cardconnect/constants.php');
			$dataname = sessvar('location_info')['data_name'];
			$terminalId = reqvar("terminal_id", "");
			$locationid = $loc_id;
			$deviceId = determineDeviceIdentifier($_REQUEST);
			$amount = reqvar("amount", "");

			$terminals = new PaymentGateways\CardConnect\Terminal($dataname, $locationid);

			switch ($action) {
				case "cancel":
					$response = $terminals->cancel($terminalId);
					if ($response && $response->status === Response::HTTP_NO_CONTENT) {
						$result = array("success" => true);
					}
					break;
				case "connect":
					$result = $terminals->connect($terminalId);
					break;
				case "get-paired-terminal":
					$response = $terminals->listTerminals($terminalId, $deviceId);
					if (count($response) > 0) {
						$result = $response[0];
					} else {
						$result = null;
					}
					break;
				case "list":
					$result = $terminals->listTerminals();
					break;
				case "pair":
					$result = $terminals->pair($terminalId, $deviceId);
					break;
				case "unpair":
					$result = $terminals->unpair($terminalId, $deviceId);
					break;
				// WARNING THE READ CARD OPERATION IS A BLOCKING CALL
				case "read";
					$result = $terminals->read($terminalId, $amount);
					break;
				default:
					$result = array("error" => PaymentGateways\CardConnect\UNKNOWN_ACTION_ERROR);
			}

			lavu_echo(json_encode($result));
		} catch (Exception $err) {
			error_log($err);
			lavu_echo(json_encode(array("error" => $err->getMessage())));
		}
	}
	if ($mode === "lavupay:batching") {
		LavuPayController::route(Request::createFromGlobals(), $company_info, $location_info);
	}

	if ($mode === "lavupay:batching:initiate") {

		require_once(dirname(__FILE__) . "/../../lib/Lavu/Queue/Queue.php");
		require_once(dirname(__FILE__) . "/../../app/Jobs/BatchJob.php");
		require_once(dirname(__FILE__) . "/../../lib/Lavu/Support/Recurrence.php");

		$data_name = reqvar("data_name", "");
		$loc_id = reqvar("loc_id", "");
		$rrule = reqvar("rrule", "");
		$timeToRun =  \Lavu\Support\Recurrence::getNextOccurrence($rrule);
		$q = new \Lavu\Queue\Queue();

		$data = new App\Jobs\BatchJob(null, $data_name, $loc_id, $rrule);
		$q->later($timeToRun, 'BatchJob', $data);
	}
}// END if
}
?>
