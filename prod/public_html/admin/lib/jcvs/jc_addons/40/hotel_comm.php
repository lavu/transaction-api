<?php
	require_once(dirname(__FILE__) . "/../jc_addon_functions/hotel_comm_functions.php");
	// Mode 40 - Save payments and discounts (happens after Mode 42 sometimes - its a kind of safety check for payment totals)

	/*function hotel_comm_log($str)
	{
		global $data_name;
		$str = "--------------------------\n" . date("Y-m-d H:i:s") . "\n" . $str . "\n";
		$fp = fopen("/home/poslavu/logs/debug/hotel_comm.txt","a");
		fwrite($fp,$str);
		fclose($fp);
	}*/

	function after_hotel_comm($jc_result)
	{
		//hotel_comm_log("mode 40 running");
		//hotel_comm_log($jc_result);

		$check_info = $_REQUEST['check_info'];
		$check_info_parts = explode("|*|",$check_info);
		if(count($check_info_parts) > 1)
		{
			$order_id = trim($check_info_parts[0]);

			if($order_id!="")
			{
				comm_order_contents($order_id);
			}
		}

		return $jc_result;
	}
?>