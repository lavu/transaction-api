<?php
	// This module is used to block an account from saving (A method for kicking out disabled accounts who do not reload settings)
	
	function before_system_saveblock($vars) {
		exit();
		return false;
	}
	
	function after_system_saveblock($vars) {
		exit();
		return false;
	}
?>
