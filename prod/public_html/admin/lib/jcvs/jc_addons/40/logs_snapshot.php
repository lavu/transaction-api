<?php
    
    function after_logs_snapshot($jc_return_str){
        //error_log("Does it reach");
        //$huh = print_r($jc_result, 1);
        //bd_writeToLog($huh, "logsSnapshot.log");
        //error_log("after_logs_snapshot_  After:  " . print_r($_REQUEST, 1));
        
        //error_log("Request: " . print_r($_REQUEST, 1));
        
        global $location_info;
        
        $location_info['timezone'];
        //checkInfo stuff
        $checkInfo_   = $_REQUEST['check_info'];
        $checkInfoArr = explode('|*|', $checkInfo_);
        
        
        
        //Details save string
        $detailsStr = '';
        foreach($_REQUEST as $key => $val){
            $detailsStr .= $key . "<^-^>" . $val . "[^.^]";
        } 
        $detailsStr = substr($detailsStr, 0, strlen($detailsStr) - 5);//Remove last delimiter.
        
        
        //
        $insertArr = array();
        $insertArr['action'] = 'Order_Snapshot';
        $insertArr['loc_id'] = $_REQUEST['loc_id'];
        $insertArr['order_id'] = $checkInfoArr[0];
        $insertArr['time'] = localize_datetime(date('Y-m-d H:i:s'), $location_info['timezone']);//////////// NEEDS to be localized
        $insertArr['user'] = $checkInfoArr[24];
        $insertArr['user_id'] = $checkInfoArr[25];
        $insertArr['server_time'] = date('Y-m-d H:i:s');
        $insertArr['item_id'] = $itemDetails[44];
        $insertArr['details'] = $detailsStr;
        $insertArr['check'] = '0';
        $insertArr['device_udid'] = $_REQUEST['MAC'];
        $insertArr['details_short'] = "";
        
        //Order_Snapshot insert into the Action Log
        $sqlInsert  = "INSERT INTO `action_log` ";
        $sqlInsert .= "(`action`,`loc_id`,`order_id`,`time`,`user`,`user_id`,`server_time`,`item_id`,`details`,`check`,`device_udid`,`details_short`) VALUES ";
        $sqlInsert .= "('[action]','[loc_id]','[order_id]','[time]','[user]','[user_id]','[server_time]','[item_id]','[details]','[check]','[device_udid]','[details_short]')";
        
        $result = lavu_query($sqlInsert, $insertArr);

		// START LP-1171
		if (function_exists('updateSignatureForNorwayActionLog'))
		{
			// updateSignatureForNorwayActionLog() will not be defined for requests coming from the Lavu Retro app
			$currentActionLogId = lavu_insert_id();
			updateSignatureForNorwayActionLog($currentActionLogId);
		}
        // END  LP-1171

		if (!$result)
		{
			lavu_dberror();
		}

        return $jc_return_str;
    }
    
    
    
    function before_logs_snapshot($jc_result){
        return $jc_result;
    }

?>
