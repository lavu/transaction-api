<?php
	require_once(dirname(__FILE__) . "/../jc_addon_functions/hotel_comm_functions.php");

	// Mode 48
	// Other modes: Mode 42 - save a single payment or refund and return a row id
	//				Mode 12 - void an order
	//				Mode 48 - void an item
	//				Mode 28 - save order contents
	//				Mode gw/void - process a gateway void

	function after_hotel_comm($jc_result)
	{
		hotel_comm_log($jc_result);

		if(isset($_REQUEST['order_id']) && trim($_REQUEST['order_id'])!="")
		{
			comm_order_contents($_REQUEST['order_id']);
		}

		return $jc_result;
	}
?>