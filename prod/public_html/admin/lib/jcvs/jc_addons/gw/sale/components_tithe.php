<?php

// Appended _ON_HOLD to components_tithe functions so they don't get called if/when an account gets assigned the components.* module.
// This prevents untimely loss of order contents after running a card transaction. The code hooks are all still in place in gateway.php
// in case the gateway addon model is ever revived.

	function before_components_tithe_ON_HOLD($response) {
	
		global $company_info;
		global $location_info;
		global $process_info;
		global $server_order_prefix;
		
		$cid = $company_info['id'];
		$loc_id = $location_info['id'];
		$device_prefix = getDevicePrefix();
		$rndm1 = rand(1, 9999);
		$rndm2 = rand(1, 9999);
		
		$old_tz = getenv('TZ');
		$to_tz = $location_info['timezone'];
   	if (!empty($to_tz)) putenv("TZ=$to_tz");
    $ioid = $cid.$loc_id.$device_prefix."-".str_pad((string)$rndm1, 4, "0", STR_PAD_LEFT)."-".date("YmdHis");
		$icid = $cid.$loc_id.$device_prefix."-".str_pad((string)$rndm2, 4, "0", STR_PAD_LEFT)."-".date("YmdHis");
		putenv("TZ=$old_tz");
		
		$amount = $process_info['card_amount'];
	
		if (empty($process_info['order_id'])) {
		
			$member_info = "19|o|Member|o|".$_REQUEST['member_full_name']."|o|".$_REQUEST['member_id'];
		
			$q_fields = "";
			$q_values = "";
			$o_vars = array();
			$o_vars['ioid'] = $ioid;
			$o_vars['opened'] = $process_info['device_time'];
			$o_vars['closed'] = "0000-00-00 00:00:00";
			$o_vars['void'] = "3";
			$o_vars['server'] = $process_info['server_name'];
			$o_vars['server_id'] = $process_info['server_id'];
			$o_vars['cashier'] = $process_info['server_name'];
			$o_vars['cashier_id'] = $process_info['server_id'];
			$o_vars['tablename'] = $_REQUEST['member_full_name'];
			$o_vars['send_status'] = "0";
			$o_vars['guests'] = "1";
			$o_vars['no_of_checks'] = "1";
			$o_vars['register'] = $process_info['register'];
			$o_vars['register_name'] = $process_info['register_name'];
			$o_vars['location'] = $location_info['title'];
			$o_vars['location_id'] = $process_info['loc_id'];
			$o_vars['opening_device'] = $process_info['device_udid'];
			$o_vars['last_mod_device'] = $process_info['device_udid'];
			$o_vars['last_mod_register_name'] = $process_info['register_name'];
			$o_vars['last_modified'] = $process_info['device_time'];
			$o_vars['subtotal'] = $amount;
			$o_vars['total'] = $amount;
			$o_vars['tax_exempt'] = "1";
			$o_vars['original_id'] = $member_info;
			$keys = array_keys($o_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$process_info['order_id'] = assignNextWithPrefix("order_id", "orders", "location_id", $process_info['loc_id'], $server_order_prefix."-");
			$o_vars['order_id'] = $process_info['order_id'];
			$save_new_order = lavu_query("INSERT INTO `orders` (`order_id`, $q_fields) VALUES ('[order_id]', $q_values)", $o_vars);
			if (!$save_new_order) {
				echo "0|Failed to create invoice record||||Error";
				lavu_exit();
			}
		}
		
		$clear_current_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $process_info['order_id']);
			
		$item_id = $_REQUEST['ministry_id'];
		$item_name = $_REQUEST['ministry_name'];
		
		$set_hidden4 = "";
		$set_hidden5 = "";
		switch ($_REQUEST['frequency']) {
			case "Monthly" :
				$set_hidden4 = $amount."|1 year";
				$set_hidden5 = "recurring_item";
				break;
			case "Weekly" :
				$set_hidden4 = $amount."|1 year";
				$set_hidden5 = "recurring_item";
				break;
			default : break;
		}
		
		$options = "MEMBER: ".$_REQUEST['member_full_name'];
		if (!empty($_REQUEST['member_id'])) $options .= " (#".$_REQUEST['member_id'].")";
		
		$q_fields = "";
		$q_values = "";
		$i_vars = array();
		$i_vars['loc_id'] = $loc_id;
		$i_vars['order_id'] = $process_info['order_id'];
		$i_vars['ioid'] = $ioid;
		$i_vars['icid'] = $icid;
		$i_vars['item_id'] = $item_id;
		$i_vars['item'] = $item_name;			
		$i_vars['category_id'] = getFieldInfo($item_id, "id", "menu_items", "category_id");		
		$i_vars['open_item'] = "1";
		$i_vars['price'] = $amount;
		$i_vars['quantity'] = "1";
		$i_vars['subtotal'] = $amount;
		$i_vars['subtotal_with_mods'] = $amount;
		$i_vars['total_with_tax'] = $amount;
		$i_vars['after_discount'] = $amount;
		$i_vars['after_gratuity'] = $amount;
		$i_vars['check'] = "1";
		$i_vars['seat'] = "1";
		$i_vars['course'] = "1";
		$i_vars['device_time'] = $process_info['device_time'];
		$i_vars['tax_exempt'] = "1";
		$i_vars['split_factor'] = "1";
		$i_vars['hidden_data4'] = $set_hidden4;
		$i_vars['hidden_data5'] = $set_hidden5;
		$i_vars['options'] = $options;
		$keys = array_keys($i_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$save_order_item = lavu_query("INSERT INTO `order_contents` ($q_fields, `server_time`) VALUES ($q_values, now())", $i_vars);
		if (!$save_order_item) {
			echo "0|Failed to create invoice contents||".$process_info['order_id']."||Error";
			lavu_exit();
		}
	}

	function after_components_tithe_ON_HOLD( $response ){
		
		global $process_info;
		global $location_info;
		
		if( !$response || empty( $response ) ){
			return $response;
		}

		$response_parts = explode('|', $response );
		if( !count( $response_parts )){
			return $response;
		}

		if( !$response_parts[0] || empty( $response_parts[0] ) ){
			return $response;
		}
	
		// transaction succeeded
	
		$update_order = lavu_query("UPDATE `orders` SET `void` = '0', `closed` = '[1]', `last_modified` = '[1]', `checked_out` = '1' WHERE `location_id` = '[2]' AND `order_id` = '[3]'", localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']), $_REQUEST['loc_id'], $process_info['order_id']);
		$update_contents = lavu_query("UPDATE `order_contents` SET `checked_out` = '1' WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $_REQUEST['loc_id'], $process_info['order_id']);

		$ministry_name = getFieldInfo($_POST['ministry_id'], "id", "menu_items", "name");

		$args = array();
		$args['ministry_id'] = isset($_POST['ministry_id'])?$_POST['ministry_id']:'';
		$args['customerId'] = isset($_POST['member_id'])?$_POST['member_id']:'';
		$args['member_full_name'] = isset($_POST['member_full_name'])?$_POST['member_full_name']:'';
		$args['orderId'] = isset($process_info['order_id'])?$process_info['order_id']:'';
		$args['locationId'] = isset($_POST['loc_id'])?$_POST['loc_id']:'';
		$args['profileName'] = $ministry_name;
		$args['recurringAmount'] = isset($_POST['card_amount'])?$_POST['card_amount']:'';
		$args['cc_num_last4'] = $response_parts[2];
		$args['cc_ref_num'] = $response_parts[1];
		$args['cc_exp_date'] = $process_info['exp_month'].'/'.($process_info['exp_year']%100);
		$args['cc_card_type'] = $response_parts[5];
		$args['frequency'] = isset($_POST['frequency'])?strtolower($_POST['frequency']):'';
		$args['start_date'] = date("Y-m-d");
		$args['end_date'] = date("Y-m-d", strtotime('+ 1 year'));
		
		medActionLogIt("$".number_format((float)$args['recurringAmount'], $location_info['disable_decimal'], ".", "")." offering made to ".$ministry_name, $_REQUEST['member_id'], $process_info['order_id']);
		
		//Potentially No Card Type (may need to parse response) (will need to parse it anyway to check to see if it was successful)

		switch( $args['frequency'] ){
			case 'monthly' :
				$args['offset'] = MIN(date("j")*1, 28);
				$args['first_bill_date'] = date("Y-m-d", strtotime('+ 1 month'));
				break;
			case 'bi-weekly' :
				$args['offset'] = date("w");
				$args['first_bill_date'] = date("Y-m-d", strtotime('+ 2 week'));
				break;
			case 'weekly' :
				$args['offset'] = date("w");
				$args['first_bill_date'] = date("Y-m-d", strtotime('+ 1 week'));
				break;
			case 'daily' :
				$args['offset'] = date("H");
				$args['first_bill_date'] = date("Y-m-d", strtotime('+ 1 day'));
				break;				
			case 'one time' :
			default :
				return $response; //Either no reoccurence, or an unrecognized option
		}
		
		$args['next_bill_date'] = $args['first_bill_date'];
		$args['invoice_template'] = <<<XML
<invoice order_id="4-x-nnn" subtotal="{$args['recurringAmount']}" total="{$args['recurringAmount']}" tax_rate="0.00" tax="0.00">
	<item>
		<item_id>{$args['ministry_id']}</item_id>
		<name>{$ministry_name}</name>
		<amount>{$args['recurringAmount']}</amount>
		<quantity>1</quantity>
		<subtotal>{$args['recurringAmount']}</subtotal>
		<total>{$args['recurringAmount']}</total>
		<tax>0.0</tax>
		<tax_rate>0.00</tax_rate>
		<options>MEMBER: {$args['member_full_name']} (#{$args['customerId']})</options>
	</item>
</invoice>
XML;

		foreach( $args as $k => $v ){
			if( empty( $v ) ) {
				$response .= '|{"addon_info":{"status":"error","message":"Missing required billing parameter ('.$k.')"}}';
				return $response;
			}
		}

		$abpInsertVars = array(
			'customer_id' => $args['customerId'],
			'order_id' => $args['orderId'],
			'location_id' => $args['locationId'],
			'type' => 'billing_profile',
			'name' => $args['profileName'],
			'status' => 'active',
			'recurring_amount' => $args['recurringAmount'],
			'invoice_template' => $args['invoice_template'],
			'start_date' => $args['start_date'],
			'end_date' => $args['end_date'],
			'first_bill_date' => $args['first_bill_date'],
			'next_bill_date' => $args['next_bill_date'],
			'bill_frequency' => $args['frequency'],
			'bill_event_offset' => $args['offset'],
			'cc_ref_num' => $args['cc_ref_num'],
			'cc_card_type' => $args['cc_card_type'],
			'cc_num_last4' => $args['cc_num_last4'],
			'cc_exp_date' => $args['cc_exp_date']
		);
		
		$q_fields = "";
		$q_values = "";
		$keys = array_keys($abpInsertVars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
				
		$abpInsertSql = "INSERT INTO `med_billing` ($q_fields, `createtime`, `updatetime`) VALUES ($q_values, now(), now())";

		if( lavu_query( $abpInsertSql, $abpInsertVars ) === FALSE ){
			$response .= '|{"addon_info":{"status":"error","message":"Failed to create billing record"}}';
			return $response;
		}
		
		medActionLogIt("Started Recurring Offering: $".number_format((float)$args['recurringAmount'], $location_info['disable_decimal'], ".", "")." ".$args['frequency']." to ".$ministry_name, $_REQUEST['member_id'], $process_info['order_id']);
		
		$response .= '|{"addon_info":{"status":"success"}}';
		return $response;
	}
	
	function medActionLogIt($action, $customer_id, $order_id) {
	
		$q_fields = "";
		$q_values = "";
		$vars = array();
		$vars['action'] = $action;
		$vars['loc_id'] = $_REQUEST['loc_id'];
		$vars['order_id'] = $order_id;
		$vars['customer_id'] = $customer_id;
		$vars['restaurant_time'] = $_REQUEST['device_time'];
		$vars['device_time'] = $_REQUEST['device_time'];
		$keys = array_keys($vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `med_action_log` ($q_fields) VALUES ($q_values)", $vars);
	}

?>
