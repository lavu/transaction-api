<?php
	require_once(dirname(__FILE__) . "/../../jc_addon_functions/hotel_comm_functions.php");

	function after_hotel_comm($response) {

		$before_response = $response;

		// failed:  0|(message)|(reference number or empty)|(order-id)
		// success:  1|(reference number)|(last four of card)|(message)|(authcode)|(card type)|(ref 2 or empty)|(ref 3 or empty)|(order-id)
		// partial approval:   ref3 is an amount instead

		$rsp_parts = explode("|",$response);
		if(count($rsp_parts) > 3)
		{
			$rsp_success = trim($rsp_parts[0]);
			if($rsp_success=="1" || $rsp_success=="2")
			{
				$result_order_id = trim($rsp_parts[8]);
				$rsp_names = array("success","reference1","lastfour","message","authcode","cardtype","reference2","reference3","order_id");
				$rsp_details = "";
				$rsp_details .= "action=void";
				for($n=0; $n<count($rsp_parts); $n++)
				{
					if(isset($rsp_parts[$n]) && isset($rsp_names[$n]))
					{
						if($rsp_details!="") $rsp_details .= "<__prop__>";
						$rsp_details .= urlencode($rsp_names[$n]) . "=" . urlencode($rsp_parts[$n]);
					}
				}

				$server_ip = get_ers_netpath("server_ip");
				$post_url = "https://$server_ip/view/update_poslavu_order/";

				global $location_info;
				if (trim($server_ip) != "" && trim($result_order_id)!="") {

					$getvars = "";
					$postvars = "poslavu_order_id=$result_order_id&gateway_payment_contents=" . $rsp_details;

					hotel_comm_log($post_url.$getvars);

					$ch = curl_init();
					//curl_setopt($ch, CURLOPT_URL, "http://hotel.ers/cp/review_events/".$getvars);
					curl_setopt($ch, CURLOPT_URL, $post_url.$getvars);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
					$response = curl_exec($ch);

					$cstr = $response;
					hotel_comm_log($cstr);
				}
			}
			else
			{
				$result_message = trim($rsp_parts[1]);
				$result_order_id = trim($rsp_parts[3]);
			}
		}

		$response = $before_response;
		return $response;
	}
?>