<?php

	require_once( dirname(__FILE__) . "/../../../../components/fitness/RecurringBilling.php" );

	function after_customers_billing( $result )
	{
		ini_set("display_errors","0");

		$dataname = $_REQUEST['cc'];
		$locationId = $_REQUEST['loc_id'];
		$customerId = '';
		$orderId = $_REQUEST['order_id'];
		$itemTriggersRecurringBilling = false;

		$close_info = explode( '|*|', $_REQUEST['close_info'] );
		$itemsOnClosedOrder = explode( '|*|', $_REQUEST['item_details'] );

		foreach ( $itemsOnClosedOrder as $delimitedItem )
		{
			// Parse delimited string that gives you the item purchased.
			$item = explode( '|o|', $delimitedItem );
			$purchasedItem = $item[38];
			$special = $item[64];
			$specialDetail1 = explode( '|', $item[60] );  // "Special Details 1" in CP item details aka hidden_data4 aka index 60 in item_details.
			$recurringAmount = isset($specialDetail1[0]) ? $specialDetail1[0] : '';
			$endDateModifier = isset($specialDetail1[1]) ? $specialDetail1[1] : '';
			$billFrequency = isset($specialDetail1[2]) ? $specialDetail1[2] : '';
			$startDate = isset($specialDetail1[3]) ? $specialDetail1[3] : '';

			// The Special drop-down in the item's Details popup determines whether it activates recurring billing.
			$itemTriggersRecurringBilling = ($special == 'recurring_item') ? true : false;

			// Parse delimited string that gives you Customer ID.
			preg_match( '/^MEMBER: .* [(][#](\d+)[)]$/', $item[34], $matches );
			$customerId = $matches[1];

			// Get server_id from parsed close_info.
			$employeeId = $close_info[13];

			//error_log( "LAVU FIT DEBUG: gymWorkerId={$gymWorkerId} dataname={$dataname} customerId={$customerId} orderId={$orderId} locationId={$locationId} profileName={$purchasedItem} employeeId={$employeeId} billFrequency={$billFrequency} startDate={$startDate}" );  //debug

			// Check for order with item that should start recurring billing.
			if ( $itemTriggersRecurringBilling ) {
				$arbUtil = new RecurringBilling();
				$args = array(
					'dataname' => $dataname,
					'customerId' => $customerId,
					'orderId' => $orderId,
					'locationId' => $locationId,
					'profileName' => $purchasedItem,
					'recurringAmount' => $recurringAmount,
					'startDate' => $startDate,
					'endDateModifier' => $endDateModifier,
					'employeeId' => $employeeId,
					'billFrequency' => $billFrequency
				);
				if ($dataname == 'sityodtong_inc' || $dataname == 'lavu_fitness') {
					$arbId = $arbUtil->createProfileAndWaiver($args);
				} else {
					$arbId = $arbUtil->createProfile($args);
				}
			}
		}

		$bool2str = array( true => 'true', false => 'false' );

		//mail( "tom@lavu.com", "Debug email from ". __FILE__, "Howdy\nitemTriggersRecurringBilling={$bool2str[$itemTriggersRecurringBilling]}\ncloserUser={$closerUser}\ncustomerId={$customerId}\norderId={$orderId}startDate={$startDate}endDateModifier={$endDateModifier}billFrequency={$billFrequency}\n\n_REQUEST=\n". print_r($_REQUEST, true) ."\nitemsOnClosedOrder=\n". print_r($itemsOnClosedOrder, true), "From:jcaddons@poslavu.com" );  //debug

		return $result;
	}
?>
