<?php


	function after_accounting_quickbooks_jcaddonsync($jc_result){
		global $data_name;
		global $location_info;//Strange timezone not present.
		global $device_time;
		//order_id
		//error_log("Order saved for dataname data available: dataname:" . $data_name . "\nREQUEST:" . print_r($_REQUEST,1) . "\n\njs_result: " . print_r($jc_result,1) . "\n\nLocationRow: " . print_r($location_info,1));
		putClosedOrderOnQuickbooksQueueIfNotAlreadyThere($data_name, $_REQUEST['order_id'], $device_time);

		return $jc_result;
	}

	function putClosedOrderOnQuickbooksQueueIfNotAlreadyThere($dataName, $orderID, $device_time){
		$selectResult = mlavu_query("SELECT * FROM `poslavu_LAVU_QUICKBOOKS_db`.`closed_order_queue` WHERE `data_name`='[1]' AND `order_id`='[2]' AND `server_date_time_sent`=''", $dataName, $orderID, $device_time);
		$isDevValue = strpos(__FILE__, "/dev/") ? "1" : "0";
		$qbook_api_version = "1";//For later developement this means which api is called.
		if(!$selectResult) {error_log("MySQL error in ".__FILE__." error:".mlavu_dberror());}
		if(mysqli_num_rows($selectResult)){
			return;
		}
		$queryStr = "INSERT INTO `poslavu_LAVU_QUICKBOOKS_db`.`closed_order_queue` ".
		            "(`data_name`,`order_id`,`server_date_time_created`,`device_time`,`dev`,`api_version`) ".
		            "VALUES ('[1]','[2]','[3]','[4]','[5]','[6]')";
		$result = mlavu_query($queryStr, $dataName, $orderID, date('Y-m-d H:i:s'), $device_time,$isDevValue,$qbook_api_version);
		if(!$result){
			error_log("MySQL error in ".__FILE__." -sihfgiu- error:".mlavu_dberror());
		}
	}

?>
