<?php
	function get_ers_netpath($mode="netpath")
	{
		$config_query = lavu_query("select `value` from `config` where `setting`='ers_netpath' and `type`='location_config_setting'");
		if(mysqli_num_rows($config_query))
		{
			$config_read = mysqli_fetch_assoc($config_query);
			$ers_netpath = $config_read['value'];
		}
		else $ers_netpath = "";

		/*if(trim($ers_netpath)=="")
		{
			echo ".";
			exit();
		}*/
		if($mode=="server_ip")
		{
			$server_ip = str_replace("https://","",str_replace("http://","",$ers_netpath));
			return $server_ip;
		}
		else return $ers_netpath;
	}

	if(!function_exists("urlencode_array"))
	{
		function urlencode_array($arr,$encode_dimensional=false)
		{
			$str = "";
			foreach($arr as $akey => $aval)
			{
				if($str!="") $str .= "&";
				$str .= $akey . "=" . urlencode($aval);
			}
			if($encode_dimensional)
			{
				$str = str_replace("=","(pass_eq)",str_replace("&","(pass_and)",$str));
			}
			return $str;
		}

		function urldecode_string($str,$decode_dimensional=false)
		{
			if($decode_dimensional)
			{
				$str = str_replace("=","(pass_inner_eq)",str_replace("&","(pass_inner_and)",$str));
				$str = str_replace("(pass_eq)","=",str_replace("(pass_and)","&",$str));
			}
			$arr = array();
			$str_parts = explode("&",$str);
			for($i=0; $i<count($str_parts); $i++)
			{
				$inner_parts = explode("=",$str_parts[$i]);
				if(count($inner_parts) > 1)
				{
					$setval = urldecode($inner_parts[1]);
					if($decode_dimensional)
					{
						$setval = str_replace("(pass_inner_eq)","=",str_replace("(pass_inner_and)","&",$setval));
					}
					$arr[$inner_parts[0]] = $setval;
				}
			}
			return $arr;
		}
	}

	function hotel_comm_log($str)
	{
		global $data_name;
		$str = "--------------------------\n" . date("Y-m-d H:i:s") . "\n" . $str . "\n";
		$fp = fopen("/home/poslavu/logs/debug/hotel_comm.txt","a");
		fwrite($fp,$str);
		fclose($fp);
	}

	function comm_order_status($order_id)
	{
		$order_query = lavu_query("select `opened`,`closed`,`reopened_datetime`,`reclosed_datetime`,`void` from `orders` where `order_id`='[1]'",$order_id);
		if(mysqli_num_rows($order_query))
		{
			$order_read = mysqli_fetch_assoc($order_query);
			$set_info = "";
			foreach($order_read as $key => $val)
			{
				$valstr = urlencode($val);
				if($set_info!="") $set_info .= "&";
				$set_info .= $key . "=" . $valstr;
			}
			comm_order_command("close_order",$order_id,$set_info);
		}
	}

	function comm_order_command($cmd,$order_id,$sendstr="")
	{
		$server_ip = get_ers_netpath("server_ip");
		$post_url = "https://$server_ip/view/update_poslavu_order/";

		global $location_info;
		if (trim($server_ip) != "")
		{
			$getvars = "";
			$postvars = "poslavu_order_id=$order_id&poslavu_action=$cmd";
			if($sendstr!="") $postvars .= "&poslavu_info=" . urlencode($sendstr);

			hotel_comm_log($post_url.$getvars . "\npost: " . $postvars);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $post_url.$getvars);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			$response = curl_exec($ch);

			hotel_comm_log($response);
		}
	}

	function comm_order_contents($result_order_id,$result_order_contents=false)
	{
		if(!$result_order_contents)
		{
			$result_order_contents = array();
			$content_id_query = lavu_query("select `id` from `order_contents` where `order_id`='[1]'",$result_order_id);
			while($content_id_read = mysqli_fetch_assoc($content_id_query))
			{
				$result_order_contents[] = $content_id_read['id'];
			}
		}

		$cstr = "";
		for($i=0; $i<count($result_order_contents); $i++)
		{
			$content_id = $result_order_contents[$i];

			if($cstr!="") $cstr .= "<__item__>";
			$content_query = lavu_query("select * from `order_contents` where `id`='[1]'",$content_id);
			while($content_read = mysqli_fetch_assoc($content_query))
			{
				$debug .= "---------------------------------\n";
				$istr = "";
				foreach($content_read as $key => $val) 
				{
					if($istr!="") $istr .= "<__prop__>";
						$istr .= urlencode($key) . "=" . urlencode($val);
				}
				/*$content_item_name = $content_read['item_name'];
				$content_item_unit_cost = $content_read['unit_cost'];
				$content_item_quantity = $content_read['quantity'];
				$content_item_cost = $content_read['cost'];*/

				$cstr .= $istr;
			}
		}
		hotel_comm_log($cstr);

		$server_ip = get_ers_netpath("server_ip");
		$post_url = "https://$server_ip/view/update_poslavu_order/";

		//if($result_order_id=="7-11")
		//{
			global $location_info;
			//$server_ip = str_replace("/poslavu","",str_replace("http://","",str_replace("https://","",$location_info['net_path'])));

			if (trim($server_ip) != "") {

				$getvars = "";
				$postvars = "poslavu_order_id=$result_order_id&contents=" . $cstr;

				hotel_comm_log($post_url.$getvars);

				$ch = curl_init();
				//curl_setopt($ch, CURLOPT_URL, "http://hotel.ers/cp/review_events/".$getvars);
				curl_setopt($ch, CURLOPT_URL, $post_url.$getvars);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
				$response = curl_exec($ch);

				$cstr = $response;
				hotel_comm_log($cstr);

				//error_log($cstr);
			}

			//$debug = "";
			/*if($debug!="")
			{
				$fp = fopen("/poslavu-local/logs/after_hotel_comm.txt","w");
				fwrite($fp,$cstr);
				fclose($fp);
			}*/
		//}
	}
?>
