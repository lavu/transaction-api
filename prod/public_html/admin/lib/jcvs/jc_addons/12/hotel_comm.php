<?php
	require_once(dirname(__FILE__) . "/../jc_addon_functions/hotel_comm_functions.php");
	
	//				Mode 12 - void an order
	// Other modes: Mode 42 - save a single payment or refund and return a row id
	//				Mode 12 - void an order
	//				Mode 48 - void an item
	//				Mode gw/void - process a gateway void
	//				Mode 28 - save order
	// 				Mode 10 - close order
	// 				Mode 20 - Reopen order
	
	function after_hotel_comm($jc_result)
	{
		$debug = "";
		
		$order_id = $_REQUEST['order_id'];
		hotel_comm_log("Voiding Order for ".$order_id.": " . $jc_result);
		//$set_info = "closed=no";
		//$sendstr = "poslavu_order_id=$order_id&poslavu_action=close_order&poslavu_info=" . urlencode($set_info);
		//comm_order_command("close_order",$order_id,$set_info);
		comm_order_status($order_id);
		
		return $jc_result;
	}
?>
