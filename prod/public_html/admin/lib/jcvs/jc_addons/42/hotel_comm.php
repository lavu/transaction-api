<?php
	require_once(dirname(__FILE__) . "/../jc_addon_functions/hotel_comm_functions.php");

	// Mode 42 - // save single payment or refund and return new row id
	// Other modes: Mode 28 - Save Order
	//				Mode gw/void - process a gateway void

	function after_hotel_comm($vars)
	{
		hotel_comm_log("mode 42 running");

		$result_order_id = "";
		if(isset($vars['pnr_details']))
		{
			$pnr_detail_parts = explode("|*|",$vars['pnr_details']);
			if(count($pnr_detail_parts) > 1)
			{
				$result_order_id = $pnr_detail_parts[0];
			}
		}

		$dbgstr = "";
		$istr = "";
		foreach($vars as $key => $val) 
		{
			if($istr!="") $istr .= "<__prop__>";
				$istr .= urlencode($key) . "=" . urlencode($val);
			//$dbgstr .= $key . " = " . $val . "\n";
		}
		$dbgstr .= "Send poslavu-id: " . $result_order_id . "\n";
		hotel_comm_log($dbgstr);

		$server_ip = get_ers_netpath("server_ip");
		$post_url = "https://$server_ip/view/update_poslavu_order/";

		global $location_info;
		if (trim($server_ip) != "" && trim($result_order_id)!="") {

			$getvars = "";
			$postvars = "poslavu_order_id=$result_order_id&payment_contents=" . $istr;

			hotel_comm_log($post_url.$getvars);

			$ch = curl_init();
			//curl_setopt($ch, CURLOPT_URL, "http://hotel.ers/cp/review_events/".$getvars);
			curl_setopt($ch, CURLOPT_URL, $post_url.$getvars);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			$response = curl_exec($ch);

			$cstr = $response;
			hotel_comm_log($cstr);
		}

		return $jc_result;
	}
?>