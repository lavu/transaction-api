<?php
	require_once(dirname(__FILE__) . "/../jc_addon_functions/hotel_comm_functions.php");
	
	function after_hotel_comm($jc_result)
	{
		$debug = "";
		
		hotel_comm_log($jc_result);
		
		$result_parts = explode(":",$jc_result);
		$result_success = (isset($result_parts[0]))?$result_parts[0]:"0";
		$result_order_id = (isset($result_parts[1]))?$result_parts[1]:"";
		$result_order_contents = explode(",",(isset($result_parts[2]))?$result_parts[2]:"");
		
		$cstr = "";
		for($i=0; $i<count($result_order_contents); $i++)
		{
			$content_id = $result_order_contents[$i];
			
			if($cstr!="") $cstr .= "<__item__>";
			$content_query = lavu_query("select * from `order_contents` where `id`='[1]'",$content_id);
			while($content_read = mysqli_fetch_assoc($content_query))
			{
				$debug .= "---------------------------------\n";
				$istr = "";
				foreach($content_read as $key => $val) 
				{
					if($istr!="") $istr .= "<__prop__>";
						$istr .= urlencode($key) . "=" . urlencode($val);
				}
				/*$content_item_name = $content_read['item_name'];
				$content_item_unit_cost = $content_read['unit_cost'];
				$content_item_quantity = $content_read['quantity'];
				$content_item_cost = $content_read['cost'];*/
				
				$cstr .= $istr;
			}
		}
		hotel_comm_log($cstr);
		
		$server_ip = get_ers_netpath("server_ip");
		$post_url = "https://$server_ip/view/update_poslavu_order/";
		
		//if($result_order_id=="7-11")
		//{
			global $location_info;
			//$server_ip = str_replace("/poslavu","",str_replace("http://","",str_replace("https://","",$location_info['net_path'])));
			
			if (trim($server_ip) != "") {
			
				$getvars = "";
				$postvars = "poslavu_order_id=$result_order_id&contents=" . $cstr;

				hotel_comm_log($post_url.$getvars);
				
				$ch = curl_init();
				//curl_setopt($ch, CURLOPT_URL, "http://hotel.ers/cp/review_events/".$getvars);
				curl_setopt($ch, CURLOPT_URL, $post_url.$getvars);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
				$response = curl_exec($ch);
				
				$cstr = $response;
				hotel_comm_log($cstr);
				
				//error_log($cstr);
			}
				
			//$debug = "";
			/*if($debug!="")
			{
				$fp = fopen("/poslavu-local/logs/after_hotel_comm.txt","w");
				fwrite($fp,$cstr);
				fclose($fp);
			}*/
		//}
		
		
		return $jc_result;
	}
?>
