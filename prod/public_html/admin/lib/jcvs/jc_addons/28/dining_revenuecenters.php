<?php
	
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/areas/revenue_center_files/order_get_revenue_center.php");

	// for process to occur prior to standard functionality: function before_dining_mealperiods($mode) { }

	function after_dining_revenuecenters($jc_result) {
		
		global $company_info; // a few global variables inherited from above that may reduce need to pull data from database
		global $location_info;
		global $device_time;
		
		$order_id = explode(":", $jc_result);
		$order_id = $order_id[1];
		$location_id = $location_info["id"];
		
		$rca = 'revenue_center_assigned_'.$location_id."_".$order_id;
		if(!isset($_SESSION[$rca]))
		{
			$defaults = array();
			
			// gets the default values for the revenue centers from the `config` table
		    // returns an array in the form [['name'=namedefault,'id'=iddefault], ['name'=namequickserve,'id'=idquickserve], ['name'=nametabs,'id'=idtabs]
		    {
			    $defaults = array();
		    	$defaults_queries = array();
		    	$defaults_queries[] = lavu_query("SELECT `value` FROM `config` WHERE `location` = '[1]' AND `setting` = '[2]'", $location_id, "revenue_center_default_Default");
		    	$defaults_queries[] = lavu_query("SELECT `value` FROM `config` WHERE `location` = '[1]' AND `setting` = '[2]'", $location_id, "revenue_center_default_QuickServe");
		    	$defaults_queries[] = lavu_query("SELECT `value` FROM `config` WHERE `location` = '[1]' AND `setting` = '[2]'", $location_id, "revenue_center_default_Tabs");
		    	
		    	$badval = array();
		    	$badval[] = '';
		    	$badval[] = '-1';
		    	
		    	foreach ($defaults_queries as $row_query) {
		    		if ($row_query) {
		    			$row = mysqli_fetch_assoc($row_query);
						$rcid = $row['value'];
						if ($rcid === FALSE || strlen($rcid) < 1) { $rcid = NULL; }
						if ($rcid) {
							$default = lavu_query("SELECT `name`,`id` FROM `revenue_centers` WHERE `id` = '[1]'", $rcid);
							if ($default) {
								$row = mysqli_fetch_array($default);
								if ($row[1] == "") $row[1] = "-1";
								$defaults[] = $row;
							} else {
								$defaults[] = $badval;
							}
						} else {
							$defaults[] = $badval;
						}
					} else {
						$defaults[] = $badval;
					}
				}
		    }
		
			$override = FALSE;
			$store_nulls = TRUE;
			$result = order_get_revenue_center($order_id, $location_id, $defaults[0][1], $defaults[1][1], $defaults[2][1], NULL, $override, $store_nulls);
			
			$_SESSION[$rca] = 1;
		}
		return $jc_result; // always return jc_result which is echoed back to the app, whether changes are made or not - result format may vary from mode to mode
		
	}


?>
