<?php
	
	require_once(dirname(__FILE__)."/../../../../cp/areas/meal_periods/order_get_meal_period_type_names.php");

	// for process to occur prior to standard functionality: function before_dining_mealperiods($mode) { }

	function after_dining_mealperiods($jc_result) {
		
		global $company_info; // a few global variables inherited from above that may reduce need to pull data from database
		global $location_info;
		global $device_time;
		global $data_name;
		
		$order_id = explode(":", $jc_result);
		$order_id = $order_id[1];
		$location_id = $location_info["id"];
		
		if ($order_id == '1-5025')
			error_log($data_name);
		if ($order_id == '1-5025')
			error_log(print_r($jc_result));
		
		$mpa = 'meal_period_assigned_'.$location_id."_".$order_id;
		if(!isset($_SESSION[$mpa]))
		{
			$override = FALSE;
			$store_zeroes = TRUE;
			$result = order_get_meal_period_type_names($order_id, $location_id, $override, $store_zeroes);
			
			if ($order_id == '1-5025')
				error_log(print_r($result));
			
			$_SESSION[$mpa] = 1;
		}
		return $jc_result; // always return jc_result which is echoed back to the app, whether changes are made or not - result format may vary from mode to mode
		
	}


?>
