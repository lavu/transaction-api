<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/cp/resources/core_functions.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/lib/jcvs/ApiHelper.php');
ApiHelper::includeOrm('ConfigLocal');
ApiHelper::includeOrm('FiscalReceiptInvoiceLocal');

class FiscalReceiptInvoiceApi extends ApiHelper{
		private $errCode;
		private $res;
		private $req;
		public $ormObj;
		public $resFormat;
		public $action;
		public $region;

		public function __construct($dataname, $reqData = ''){
			$this->dataname=$dataname;
			$this->resFormat='json';
			if($reqData){
				$this->req = $reqData;
				$this->action = $reqData['action'];
			}
		}

		private function allowInvoiceUpdate(){
			$obj=new ConfigLocal($this->dataname);
			$result=$obj->getInfo('value','setting = "invoice_no_series" AND _deleted=0');
			$val=$result[0]['value'];
            if ($val) {
	            $data = json_decode($val, true);
	            $validTill = ($data['validtill']!='')?($this->req['loc_time'] > $data['validtill']):1;
	            $orm = new FiscalReceiptInvoiceLocal($this->dataname);
	            $result=$orm->getInfo('COUNT(1) as found');
	            if ($validTill && $result[0]['found'] > 0) {
					return 0;
				}
			}
			return 1;
		}

		private function getInvoiceNum(){
			
			$obj=new ConfigLocal($this->dataname);
			$result=$obj->getInfo('setting,value','setting IN ("invoice_no_series","enable_fiscal_invoice","enable_fiscal_receipt_templates","fiscal_receipt_templates")');

			foreach ( $result as $val ) {
				$config[$val['setting']] = $val['value'];
			}

			if($config['enable_fiscal_invoice'] && $config['enable_fiscal_receipt_templates']){
				$invoiceSeries=json_decode($config['invoice_no_series'],1);
				$invoiceStart=$invoiceSeries['start'];
				$invoiceEnd=$invoiceSeries['end'];
				$invoicePrefix=$invoiceSeries['prefix'];
				$invoicePostfix=$invoiceSeries['postfix'];
				$invoicePrePostFix = $invoicePrefix.'~'.$invoicePostfix;
			}else{
				return $this->setMsg('ERR_FRI_3',0); // invoice / receipt template not enabled
			}

			//check if invoice number reached end
			$obj=new FiscalReceiptInvoiceLocal($this->dataname);
			$result=$obj->getInfo('SUM(invoice_number) AS invoice_sum','server_id="'.$this->req['server_id'].'" AND invoice_number >= '.$invoiceStart.' AND invoice_number <= '.$invoiceEnd.' AND pre_post_fix = "'.$invoicePrePostFix.'"');
			$actualInvoiceSum=$result[0]['invoice_sum'];
			$expectedInvoiceSum=array_sum(range($invoiceStart,$invoiceEnd));
			if(($actualInvoiceSum == $expectedInvoiceSum) || ($invoiceSeries['validtill'] && (time() > $invoiceSeries['validtill']))) {
				return $this->setMsg('ERR_FRI_4',0); // invoice number finished
			}

			//generate invoice number
			$insert = mlavu_query('INSERT INTO '.$obj->table.'(server_id,order_id,invoice_number,pre_post_fix,created_date) VALUES ("'.$this->req['server_id'].'","'.$this->req['order_id'].'",(if((select p.invoice_number from '.$obj->table.' as p where invoice_number >= '.$invoiceStart.' AND invoice_number <='.$invoiceEnd.' AND pre_post_fix="'.$invoicePrePostFix.'" limit 0,1),(select p.invoice_number from '.$obj->table.' as p where invoice_number >= '.$invoiceStart.' AND invoice_number <= '.$invoiceEnd.' AND pre_post_fix="'.$invoicePrePostFix.'" order by p.fiscal_receipt_invoice_id desc limit 0,1)+1,'.$invoiceStart.')),"'.$invoicePrePostFix.'",NOW())');

			//get next invoice number
			$result=$obj->getInfo('invoice_number','server_id="'.$this->req['server_id'].'" ORDER BY fiscal_receipt_invoice_id DESC limit 0,1');
			$invoiceNumber=$result[0]['invoice_number'];
			$invoiceNumber = $invoicePrefix.$invoiceNumber.$invoicePostfix;
			$arr[]=array('invoice_number'=>$invoiceNumber,'status'=>1);
			return $arr;
		}

		public function doProcess(){
			$functions=array(
				'getinvoicenum'=>'getInvoiceNum',
				'allowinvoiceupdate'=>'allowInvoiceUpdate',
			);

			if($functions[$this->action]){
				if($this->action=='getinvoicenum' && ($this->req['server_id']=='' || $this->req['order_id']==''))
				{
					$this->res=$this->setMsg('ERR_FRI_2',0);//array('body'=>'Parameter missing. Required server_id & order_id','title'=>'Failed'));
				}else{
					$this->res=$this->{$functions[$this->action]}();
				}
			}else{
				$this->res=$this->setMsg('ERR_API_1',0); //Invalid Request
			}

			if($this->resFormat=='json'){
				return json_encode($this->res);
			}

			return $this->res;

		}		
			
}//END CLASS		
				
?>
