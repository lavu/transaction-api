<?php

class Extension{

	public $id;
	public $title;
	public $thumbnail;
	public $description;
	public $module;
	public $groupId;
	public $cost;
	public $costType;
	public $dev;
	public $prerequisites;
	public $enabledDescription;
	public $native;
	public $codeWord;
	public $extraInfo;
	public $hasError=0;
	public $errCode=array();
	public $table='poslavu_MAIN_db.extensions';

	public function __construct($title='', $id=''){
		$this->setError($title);
		$this->title = $title;
		$this->id = $id;

		if($title){
			$this->setInfo($title);			
		}
	}

	public function setError($val1=''){
		$this->errCode = array(
			'1000' => array('title'=>'Failure', 'message'=>"Extension '$val1' not found / not enabled")
		);
	}

	public function setInfo($title){
		$resource = mlavu_query('SELECT * from '.$this->table.' WHERE title="'.$title.'"');
		if(mysqli_num_rows($resource)){
			$rec = mysqli_fetch_assoc($resource);
			$this->id=$rec['id'];
			$this->title=$rec['title'];
			$this->thumbnail=$rec['thumbnail'];
			$this->description=$rec['short_description'];
			$this->module=$rec['module'];
			$this->groupId=$rec['group_ids'];
			$this->cost=$rec['cost'];
			$this->costType=$rec['cost_type'];
			$this->dev=$rec['dev'];
			$this->prerequisites=$rec['prerequisites'];
			$this->enabledDescription=$rec['enabled_description'];
			$this->native=$rec['native'];
			$this->codeWord=$rec['code_word'];
			$this->extraInfo=json_decode($rec['extra_info'],1);
			return 1;
		}else{
			$this->hasError=1000;
			return 0;
		}
	}

}
?>
