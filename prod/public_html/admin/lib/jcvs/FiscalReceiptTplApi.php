<?php
/**
This class contains action objects to manage template fields and templates based on location region
Author: OM
*/

include_once($_SERVER['DOCUMENT_ROOT'].'/cp/resources/core_functions.php');

//Include ORM files
include_once($_SERVER['DOCUMENT_ROOT'].'/lib/jcvs/ApiHelper.php');
ApiHelper::includeOrm('ConfigLocal');
ApiHelper::includeOrm('FiscalReceiptTpl');
ApiHelper::includeOrm('FiscalReceiptSection');
ApiHelper::includeOrm('FiscalReceiptTplSection');
ApiHelper::includeOrm('FiscalReceiptTplFields');
ApiHelper::includeOrm('FiscalReceiptFields');
ApiHelper::includeOrm('FiscalReceiptTplLocal');

class FiscalReceiptTplApi extends ApiHelper{
		private $errCode;
		private $res;
		private $req;
		public $ormObj;
		public $resFormat;
		public $action;
		public $region;
		public $restaurantId;
		public $tplSections;
		public $fiscalReceiptTplObj;
		public $fiscalReceiptTplFieldObj;
		public $fiscalReceiptFieldObj;
		public $fiscalReceiptTplLocalObj;
		public $fieldValueMissing = array();

		public function __construct($dataname, $reqData = ''){
			$this->dataname=$dataname;
			$this->resFormat=$reqData['res_format'];
			if($reqData){
				$this->req = $reqData;
				$this->action = $reqData['action'];
			}
		}

		private function setTplId(){
			$this->tplId=$this->req['tpl_id'];
			if(!$this->tplId){				
				//get config level data
				$obj=new ConfigLocal($this->dataname);
				$result=$obj->getInfo('setting,value','setting IN ("fiscal_receipt_template")');
				$this->tplId=$result[0]['value'];
			}
		}

		private function setRestaurantId(){
			$this->restaurantId = admin_info('companyid');
		}

		private function getTplByRegion(){

	       $obj=new ConfigLocal($this->dataname);
	       $result=$obj->getInfo('setting,value','setting="business_country" AND location='.$this->restaurantId);
	       $country=$result[0]['value'];
	       if ($country) {
				$fiscalQuery = mlavu_query("SELECT `frt`.`fiscal_receipt_tpl_id`, `frt`.`title` FROM `fiscal_receipt_tpl_region` as frtr LEFT JOIN `fiscal_receipt_tpl` as frt ON `frtr`.`fiscal_receipt_tpl_region_id` = `frt`.`fiscal_receipt_tpl_region_id` WHERE `frtr`.`country` IN ('lavu', '[1]') AND `frt`.`_deleted` = '0'", $country);
				while ($fiscalTemplates = mysqli_fetch_assoc($fiscalQuery)) {
					$fiscalTemplateArr[] = $fiscalTemplates;
				}
				$this->res=$fiscalTemplateArr;
	   		} else {
	   			$this->res='';
	   		}
		}

		public function getTplSections(){
			if(!$this->tplId) return 0;
			$obj=new FiscalReceiptTplSection($this->dataname);
			$data=$obj->getInfo('fiscal_receipt_section_id,allow_edit','fiscal_receipt_tpl_id='.$this->tplId,'','seq asc');
			foreach($data as $key=>$rec){
				$this->tplSections[$rec['fiscal_receipt_section_id']]=$data[$key];
			}
		}

		/**
		*method to get template fields along with location custom data
		*@param
		*@return Integer 0 only during exception
		*/
		private function getTplFields(){
			//If tplId not set there is no point to proceed.
			if ( !$this->tplId ) {
					$this->res = '';
					return 0;
			}

			//get location based data for specifi tpl
			$obj = new FiscalReceiptTplLocal($this->dataname);
			$result = $obj->getInfo('fiscal_receipt_tpl_field_id,value, label, display,fiscal_receipt_tpl_id,fiscal_receipt_section_id', 'fiscal_receipt_tpl_id='.$this->tplId);
			$tplFieldsLocal=array();
			foreach($result as $row){
				$tplFieldId = $row['fiscal_receipt_tpl_field_id'];
				$tplFieldsLocal[$tplFieldId]=array(
									'loc_tpl_id'=>$row['fiscal_receipt_tpl_id'],
									'loc_tpl_field_id'=>$row['fiscal_receipt_tpl_field_id'],
									'loc_value'	=> $row['value'],
									'loc_label' => $row['label'],
									'loc_display' => $row['display'],
									'loc_section' => $row['fiscal_receipt_section_id']
									);
			}

			//get tpl fields data
			$result = mlavu_query('SELECT frtf.fiscal_receipt_tpl_field_id, frtf.fiscal_receipt_field_id, frtf.label as tpl_field_label, frf.label AS field_label, frtf.display AS tpl_field_display, frtf.required, frtf.seq, frf.format,frf.type as field_type,frf.title,frtf.format as tpl_field_format, frtf.show_label,frtf.allow_hide,frf.description,frtf.type as tpl_field_type, frtf.fiscal_receipt_section_id as section, frtf.macros, frtf.allow_edit FROM fiscal_receipt_tpl_fields as frtf LEFT JOIN fiscal_receipt_fields as frf ON frtf.fiscal_receipt_field_id=frf.`fiscal_receipt_field_id` WHERE  frtf.fiscal_receipt_tpl_id='.$this->tplId.' AND frf._deleted=\'0\''.(($this->action == 'edittplfields')?' AND frtf.allow_edit=1':'').' AND frtf._deleted=\'0\' AND frtf.fiscal_receipt_tpl_field_id is not null ORDER BY frtf.seq asc');

				$tplFields=array();
				
				while($row = mysqli_fetch_assoc($result)){
					$fieldFormat=json_decode($row[format],1);
					$tplFieldFormat=json_decode($row['tpl_field_format'],1);
					if($tplFieldFormat['type']){
						$finalFormat = $tplFieldFormat['type'];
						$finalSize = $tplFieldFormat['size'];
					}else{
						$finalFormat = $fieldFormat['type'];
						$finalSize = $fieldFormat['size'];						
					}

					$tplFieldId=$row['fiscal_receipt_tpl_field_id'];
					$finalLabel=($row['show_label']=='1')?(($tplFieldsLocal[$tplFieldId]['loc_label'])?$tplFieldsLocal[$tplFieldId]['loc_label']:(($row['tpl_field_label'])?$row['tpl_field_label']:$row['field_label'])):'';
					$finalDisplay=(isset($tplFieldsLocal[$tplFieldId]['loc_display']))?$tplFieldsLocal[$tplFieldId]['loc_display']:$row['tpl_field_display'];
					$finalType=($row['tpl_field_type'])?$row['tpl_field_type']:$row['field_type'];
					$finalMacros=($row['macros'])?$row['macros']:$row['title'];

					//get final section
					$finalSection=($tplFieldsLocal[$tplFieldId]['loc_section'])?$tplFieldsLocal[$tplFieldId]['loc_section']:$row['section'];

					if($this->req['mode'] == 'fiscal_receipt_tpl_pos'){
						if(!$finalDisplay) continue;
						$value=($tplFieldsLocal[$tplFieldId]['loc_value']=='')?'':$tplFieldsLocal[$tplFieldId]['loc_value'];
						$arr=array(
								'title'=>'['.strtoupper($finalMacros).']',
								'final_label'=>$finalLabel,
								'value'=>$value,
								'pos'=>($tplFieldFormat['pos'])?$tplFieldFormat['pos']:''
							);
						$tplFields[]=$arr;
					}else{
						$arr = array (
							'fiscal_receipt_field_id' => $row['fiscal_receipt_field_id'],
							'fiscal_receipt_tpl_field_id' => $row['fiscal_receipt_tpl_field_id'],
							'field_label' => $row['field_label'],
							'tpl_field_label' => $row['tpl_field_label'],
							'title' => $finalMacros,
							'final_label' => $finalLabel,
							'final_display' => $finalDisplay,
							'final_type' => $finalType,
							'final_size' => $finalSize,
							'final_dom' => $finalFormat,
							'final_section' => $finalSection,
							'seq' => $row['seq'],
							'pos' => $tplFieldFormat['pos'],
							'required' => $row['required'],
							'show_label' => $row['show_label'],
							'allow_hide' => $row['allow_hide'],
							'allow_edit' => $row['allow_edit'],
							'description' => $row['description'],
							'loc_tpl_field_id' => $tplFieldsLocal[$tplFieldId]['loc_tpl_field_id'],
						);
						$tplFields[$row['seq']]=(is_array($tplFieldsLocal[$tplFieldId]))?array_merge($arr,$tplFieldsLocal[$tplFieldId]):$arr;
					}//END IF
				}//END WHILE
			$this->res=$tplFields;		
		}

		private function getTplFieldsBySection(){
			$this->getTplSections();
			//get tpl sections
			$obj = new FiscalReceiptSection($this->dataname);
			$sections = $obj->getInfo('fiscal_receipt_section_id,title','','fiscal_receipt_section_id');
			$this->getTplFields();
			$data=array();
			foreach($this->tplSections as $key=>$rec){
				$data[$sections[$key]['title']]['allow_edit']=$rec['allow_edit'];
			}

			foreach($this->res as $row){
				$data[$sections[$row['final_section']]['title']]['fields'][]=$row;
			}

			$this->res=$data;

		}

		private function isRequiredFieldValueMissing() {
			$this->getTplFields();
			foreach ($this->res as $key => $data) {
				if ($data['required'] == 1 && $data['final_type'] == 'field' && ( !isset($data['loc_value']) || $data['loc_value'] == '') ) {
					$this->fieldValueMissing[]=$data['tpl_field_label'];
				}
			}
			$this->res = (count($this->fieldValueMissing)) ? 1 : 0;
		}

		public function doProcess(){
			$functions=array(
				'gettplfields'=>'getTplFields',
				'updatetplfieldslocal'=>'updateTplFieldsLocal',
				'gettplfieldsbysection'=>'getTplFieldsBySection',
				'edittplfields'=>'getTplFieldsBySection',
				'gettplbyregion'=>'getTplByRegion',
				'isrequiredfieldvaluemissing'=>'isRequiredFieldValueMissing'
			);

			if($functions[$this->action]){
				$this->setRestaurantId();
				$this->setTplId();
				$this->{$functions[$this->action]}();
			}else{
				$this->res=$this->setMsg('ERR_FRT_1',0); //Invalid Request
			}

			if($this->resFormat=='json'){
				return json_encode($this->res);
			}

			return $this->res;

		}		
}
?>
