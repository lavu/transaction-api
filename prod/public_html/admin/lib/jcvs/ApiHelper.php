<?php

class ApiHelper{
	
	public static function includeOrm($orm){
		include_once($_SERVER['DOCUMENT_ROOT'].'/orm/'.$orm.'.php');
	}

	public function setMsg($tag,$status,$replace=''){
		$err=getErrMessage($tag,$replace);
		return array('status'=>$status, 'message'=>$err['body'], 'response_title'=>$err['title'], 'code'=>$tag);
	}

}
?>
