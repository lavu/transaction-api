<?php

	class fmg_placeholder {

		var $id;
		var $title;
		var $include_lists;
		var $_deleted;

		function __construct() {

			$this->id = "9999999";
			$this->title = "PLACEHOLDER_GROUP_EMPTY";
			$this->include_lists = "";
			$this->_deleted = 0;
		}
	}

	function loadMenuData($menu_id, $loc_id) { // load all menu data, including groups, categories, items, forced/optional modifiers, and printer groups

		$menu_categories_array = array();
		$menu_items_array = array();
		$menu_groups_array = array();
		$menu_items = "";

		$printer_groups_return_string = '{"ph1":"ignore",';
		$get_printer_groups = lavu_query("SELECT * FROM `config` WHERE `type` = 'printer_group' AND `location` = '".$loc_id."'");
		if (@mysqli_num_rows($get_printer_groups) > 0) {
			while ($extract = mysqli_fetch_assoc($get_printer_groups)) {
				$printer_groups_return_string .= '"pg_'.$extract['id'].'":"|'.$extract['value'].'|'.$extract['value2'].'|'.$extract['value3'].'|'.$extract['value4'].'|",';
			}
		}
		$printer_groups_return_string .= '"ph2":"ignore"}';

		//$used_forced_modifier_groups = "";
		$used_modifier_lists = "";

		$got_a_cateogory = FALSE;

		$get_menu_groups = lavu_query("SELECT * FROM `menu_groups` WHERE `menu_id` = '".$menu_id."' ORDER BY (`group_name` = 'Universal') ASC, `_order` ASC");
		if (@mysqli_num_rows($get_menu_groups) > 0) {
			while ($data_obj_MG = mysqli_fetch_object($get_menu_groups)) {
				$menu_groups_array[] = $data_obj_MG;
				$sort_type = $data_obj_MG->orderby;
				if ($sort_type == "manual") {
					$order_by = " ORDER BY `_order` ASC";
				} else {
					$order_by = " ORDER BY `name` ASC";
				}
				$get_menu_categories = lavu_query("SELECT * FROM `menu_categories` WHERE `menu_id` = '".$menu_id."' AND `group_id` = '".$data_obj_MG->id."' AND `active` = '1' AND `_deleted` != '1'".$order_by);
				if (@mysqli_num_rows($get_menu_categories) > 0) {
					$got_a_category = TRUE;
					while ($data_obj = mysqli_fetch_object($get_menu_categories)) {
						$menu_categories_array[] = $data_obj;
						if ((!strstr($used_modifier_lists, "|".$data_obj->modifier_list_id."|")) && ($data_obj->modifier_list_id != 0)) {
							$used_modifier_lists .= "|".$data_obj->modifier_list_id."|";
						}
						$get_menu_items = lavu_query("SELECT * FROM `menu_items` WHERE `menu_id` = '".$menu_id."' AND `category_id` = '".$data_obj->id."' AND `show_in_app` = '1' AND `active` = '1' AND `_deleted`!='1'".$order_by);
						if (@mysqli_num_rows($get_menu_items) > 0) {
							while ($data_obj2 = mysqli_fetch_object($get_menu_items)) {
								if (isset($data_obj2->name)) {
									$data_obj2->name = str_pad($data_obj2->name, 2, " ", STR_PAD_RIGHT);
								}
								$menu_items_array[] = $data_obj2;
								if ((!strstr($used_modifier_lists, "|".$data_obj2->modifier_list_id."|")) && ($data_obj2->modifier_list_id != 0)) {
									$used_modifier_lists .= "|".$data_obj2->modifier_list_id."|";
								}
							}
						} else {
							echo '{"json_status":"NoItems","for_category":"'.$data_obj->name.'"}';
							exit;
						}
					}
				}
			}

		} else {

			echo '{"json_status":"NoGroups"}';
			exit;
		}

		if ($got_a_category == FALSE) {
			echo '{"json_status":"NoC"}';
			exit;
		}

		$forced_modifier_group_array = array();
		$forced_modifier_group_array[] = new fmg_placeholder();
		$get_forced_modifier_groups = lavu_query("SELECT * FROM `forced_modifier_groups` WHERE `_deleted` != '1'");
		if (@mysqli_num_rows($get_forced_modifier_groups) > 0) {
			while ($data_obj = mysqli_fetch_object($get_forced_modifier_groups)) {
				$forced_modifier_group_array[] = $data_obj;
			}
		}

		$forced_modifier_list_array = array();
		$forced_modifier_list_array[] = '{"id":"9999999","title":"place_holder","type":"choice","modifiers":[{"id":"0","title":"MOD ERROR","cost":"0.00","detour":"0"}]}';
		$get_forced_modifier_lists = lavu_query("SELECT * FROM `forced_modifier_lists` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id);
		if (@mysqli_num_rows($get_forced_modifier_lists) > 0) {
			while ($extract = mysqli_fetch_array($get_forced_modifier_lists)) {
				$forced_modifier_array = array();
				$get_forced_modifiers = lavu_query("SELECT * FROM `forced_modifiers` WHERE `list_id` = '".$extract['id']."' AND `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC");
				while ($extract2 = mysqli_fetch_array($get_forced_modifiers)) {
					$forced_modifier_array[] = '{"title":"'.str_replace('"', '\"', $extract2['title']).'","cost":"'.$extract2['cost'].'","detour":"'.$extract2['detour'].'","extra":"'.$extract2['extra'].'","extra2":"'.$extract2['extra2'].'"}';
				}
				$forced_modifier_list_array[] = '{"id":"'.$extract['id'].'","title":"'.str_replace('"', '\"', $extract['title']).'","type":"'.$extract['type'].'","modifiers":['.join(",", $forced_modifier_array).']}';
				unset($forced_modifier_array);
			}
		//} else {
			//$forced_modifier_list_array[] = '{"id":"0"}';
		}
		$forced_modifier_list_return_string = '"forced_modifier_lists":['.join(",", $forced_modifier_list_array).']';

		$coded_modifier_list_array = array();
		$get_menu_default_modifications = lavu_query("SELECT `modifications` FROM `menus` WHERE `id` = '".$menu_id."'");
		if (@mysqli_num_rows($get_menu_default_modifications) > 0) {
			$extract = mysqli_fetch_array($get_menu_default_modifications);
			$coded_modifier_list_array[] = '{"id":"0","modifiers":"'.$extract['modifications'].'"}';
			$modifications = $extract['modifications']; // modifications variable kept in place for older versions of the app (8/20/2010)
		} else {
			echo '{"json_status":"NoMod"}';
			exit;
		}
		if ($used_modifier_lists != "") {
			$used_modifier_lists_array = explode("|", trim($used_modifier_lists, "|"));
			foreach ($used_modifier_lists_array as $ml) {
				$get_modifiers = lavu_query("SELECT * FROM `modifiers` WHERE `category` = '".$ml."' AND `_deleted` != '1' ORDER BY `title` ASC");
				if (mysqli_num_rows($get_modifiers) > 0) {
					$modifiers_string = "";
					$count = 0;
					while ($extract = mysqli_fetch_array($get_modifiers)) {
						$modifiers_string .= $extract['title']."::".$extract['cost'];
						$count++;
						if ($count < mysqli_num_rows($get_modifiers)) {
							$modifiers_string .= ",";
						}
					}
					$coded_modifier_list_array[] = '{"id":"'.$ml.'","modifiers":"'.str_replace('"', '\"', $modifiers_string).'"}';
				}
			}
		}
		$modifier_list_return_string = '"modifier_lists":['.join($coded_modifier_list_array, ",").']';

		return '"printer_groups":'.$printer_groups_return_string.',"forced_modifier_groups":'.lavu_json_encode($forced_modifier_group_array).','.$forced_modifier_list_return_string.',"modifications":"'.$modifications.'",'.$modifier_list_return_string.',"menu_categories":'.lavu_json_encode($menu_categories_array).',"menu_items":'.lavu_json_encode($menu_items_array).',"menu_groups":'.lavu_json_encode($menu_groups_array);
	}

	function loadComponents($comp_pack) { // load components associated with assigned spin-off package

		$layout_ids_string = "";
		$component_layouts_string = "";
		$components_string = "";

		$get_layout_ids = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_packages` WHERE `id` = '[1]'", $_REQUEST['comp_pack']);
		if (@mysqli_num_rows($get_layout_ids) > 0) {
			$extract = mysqli_fetch_array($get_layout_ids);
			$layout_ids_string = ',"layout_ids":"'.$extract['layout_ids'].'"';
			$layout_ids_array = explode(",", $extract['layout_ids']);
			$component_layouts_array = array();
			$component_ids_array = array();
			$components_array = array();
			foreach ($layout_ids_array as $layout_id) {
				if ($layout_id != "0") {
					$get_layout_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_layouts` WHERE `id` = '[1]'", $layout_id);
					$extract = mysqli_fetch_array($get_layout_data);
					$component_layouts_array[] = '{"id":"'.$layout_id.'","title":"'.$extract['title'].'","background":"'.$extract['background'].'","c_ids":"'.$extract['c_ids'].'","c_xs":"'.$extract['c_xs'].'","c_ys":"'.$extract['c_ys'].'","c_ws":"'.$extract['c_ws'].'","c_hs":"'.$extract['c_hs'].'","c_zs":"'.$extract['c_zs'].'","c_types":"'.$extract['c_types'].'"}';
					$these_component_ids = explode(",", $extract['c_ids']);
					foreach ($these_component_ids as $component_id) {
						if (!in_array($component_id, $component_ids_array)) {
							$component_ids_array[] = $component_id;
							$get_component_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`components` WHERE `id` = '[1]'", $component_id);
							$extract2 = mysqli_fetch_array($get_component_data);
							$components_array[] = '{"id":"'.$component_id.'","title":"'.$extract2['title'].'","type":"'.$extract2['type'].'","html":"'.$extract2['html'].'","allow_scroll":"'.$extract2['allow_scroll'].'","name":"'.$extract2['name'].'","filename":"'.$extract2['filename'].'"}';
						}
					}
				}
			}

			$component_layouts_string = ',"component_layouts":['.join($component_layouts_array, ",").']';
			$components_string = ',"components":['.join($components_array, ",").']';

			return $layout_ids_string.$component_layouts_string.$components_string;

		} else {

			echo '{"json_status":"MissingCompPack"}';
			exit;
		}

		return "";
	}

	function performCreditCardVoid() {

	}

	function getSupportedPrinters($loc_id) {

		$supported_printers_id_array = array();
		$config_settings_return_string = '{"no_settings":"none"}';
		$get_config_settings = lavu_query("SELECT * FROM `config` WHERE `location` = '".$loc_id."' AND `type` = 'printer'");
		if (@mysqli_num_rows($get_config_settings) > 0) {
			$config_settings_return_string = '{';
			while ($extract_cs = mysqli_fetch_array($get_config_settings)) {
				$printer_name = strtolower($extract_cs['setting']);
				$config_settings_return_string .= '"'.$printer_name.'-ip":"'.$extract_cs['value3'].'",';
				$config_settings_return_string .= '"'.$printer_name.'-port":"'.$extract_cs['value5'].'",';
				$config_settings_return_string .= '"'.$printer_name.'_drawer_code":"'.$extract_cs['value4'].'",';
				$config_settings_return_string .= '"'.$printer_name.'_command_set":"'.$extract_cs['value6'].'",';
				$config_settings_return_string .= '"'.$printer_name.'_cpl_standard":"'.$extract_cs['value7'].'",'; // characters per line for standard font
				$config_settings_return_string .= '"'.$printer_name.'_cpl_emphasize":"'.$extract_cs['value8'].'",'; // characters per line for emphasize and double height fonts
				$config_settings_return_string .= '"'.$printer_name.'_lsf":"'.$extract_cs['value9'].'",'; // line spacing factor
				if (!in_array($extract_cs['value6'], $supported_printers_id_array)) {
					$supported_printers_id_array[] = $extract_cs['value6'];
				}
			}
			$config_settings_return_string .= '"place_holder":"ignore"}';
		}

		$mfilters = array();
		$mcount = 0;
		$spid_filter = "";
		foreach ($supported_printers_id_array as $spid) {
		  $mcount++;
		  $mfilters['p_'.$mcount] = $spid;
			$spid_filter .= " OR `id` = '[p_$mcount]'";
		}

		$supported_printers_return_string = '{"no_printers":"none"}';
		$printer_commands_array = array();
		$get_supported_printers = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`supported_printers` WHERE `id` = '0'".$spid_filter, $mfilters);
		if (@mysqli_num_rows($get_supported_printers) > 0) {
			while ($extract_sp = mysqli_fetch_object($get_supported_printers)) {
				$printer_commands_array[] = $extract_sp;
			}
			$supported_printers_return_string = lavu_json_encode($printer_commands_array);
		}

		return '"supported_printers":'.$supported_printers_return_string.',"config_settings":'.$config_settings_return_string;
	}

	function get_column_names($table_name) {
 
		$result = lavu_query("SHOW COLUMNS FROM `$table_name`");
		$column_names = array();
		while ($row = mysqli_fetch_array($result)) {
			$column_names[] = $row['Field'];
		}

		return $column_names;
	}

	function create_field_list($table_name, $exclude) {

    $column_names = get_column_names($table_name);
    $field_list = "";
 
		foreach($column_names as $name) {
			if (!in_array($name, $exclude)) {
				if($field_list == "")
					$field_list = "`$name`";
				else
					$field_list .= ", `$name`";
				}
			}
 
    return $field_list;
	}

	if(isset($_POST['m'])) $mode = $_POST['m'];
	else if(isset($_GET['m'])) $mode = $_GET['m'];
	else $mode = "";

	if($mode == "punch_status") {
		$punch_query = lavu_query("SELECT * FROM `clock_punches` WHERE `_deleted` != '1' AND `server_id` = '".$_REQUEST['server_id']."' ORDER BY `time` DESC LIMIT 1");
		if(mysqli_num_rows($punch_query))
		{
			$punch_read = mysqli_fetch_assoc($punch_query);
			$punch_type = $punch_read['punch_type'];
			$punch_time = $punch_read['time'];
			$punch_time_out = $punch_read['time_out'];
			$punch_id = $punch_read['id'];
		}
		else 
		{
			$punch_type = "";
			$punch_time = "";
			$punch_time_out = "";
			$punch_id = "";
		}
		if($punch_type=="Shift" && $punch_time_out=="" && $punch_time!="") $clocked_in = 1; else $clocked_in = 0;
		echo '{"json_status":"success","clocked_in":"'.$clocked_in.'","time":"'.$punch_time.'","punchid":"'.$punch_id.'"}';
	}
	else if($mode == "clock_in") {
		$insert_punch = lavu_query("INSERT INTO `clock_punches` (`location`, `location_id`, `punch_type`, `server`, `server_id`, `time`, `hours`, `punched_out`) VALUES ('".str_replace("'", "''", getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title"))."', '".$_REQUEST['loc_id']."', 'Shift', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', '".$_REQUEST['time']."', '0', '0')");

		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Server Clocked In', '".$_REQUEST['loc_id']."', '0', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now())");

		echo '{"json_status":"success"}';
		exit;
	}
	else if($mode == "clock_out") {
		$get_punch = lavu_query("SELECT * FROM clock_punches WHERE `_deleted` != '1' AND `id` = '".$_REQUEST['punchid']."'");
		if(mysqli_num_rows($get_punch))
		{
			$extract = mysqli_fetch_assoc($get_punch);

			$clockInArray = explode(" ", $extract['time']);
			$clockInDate = explode("-", $clockInArray[0]);
			$clockInTime = explode(":", $clockInArray[1]);
			$inStamp = mktime($clockInTime[0], $clockInTime[1], $clockInTime[2], $clockInDate[1], $clockInDate[2], $clockInDate[0]);

			$clockOutArray = explode(" ", $_REQUEST['time']);
			$clockOutDate = explode("-", $clockOutArray[0]);
			$clockOutTime = explode(":", $clockOutArray[1]);
			$outStamp = mktime($clockOutTime[0], $clockOutTime[1], $clockOutTime[2], $clockOutDate[1], $clockOutDate[2], $clockOutDate[0]);

			$hours = (($outStamp - $inStamp) / 3600);

			$update_punched_out = lavu_query("UPDATE `clock_punches` SET `punched_out` = '1', `time_out`='".$_REQUEST['time']."', `hours`='".number_format($hours,3)."' WHERE id = '".$extract['id']."'");

			$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Server Clocked Out', '0', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now())");

			echo '{"json_status":"success","time_in":"'.$clockInArray[1].'","hours":"'.number_format($hours, 3).'"}';
		}
		exit;
	}
	else if ($mode == 0) { // test mode

		$data_array = array();

		$get_data = lavu_query("SELECT * FROM users");

		while($data_obj = mysqli_fetch_object($get_data)) {
  			$data_array[] = $data_obj;
		}

		echo '{"data":'.lavu_json_encode($data_array).'}';
		exit;

	} else if ($mode == 1)  { // log in from iPhone, iPod touch, or iPad

		$user_data_array = array();

		//mlavu_select_db("poslavu_MAIN_db");

		$cust_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `username` = '[1]'", trim($_REQUEST['un']));
		if(mysqli_num_rows($cust_query) > 0) {
			$cust_read = mysqli_fetch_assoc($cust_query);
			$dataname = $cust_read['dataname'];

			$get_company_info = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]' and `disabled`!='1'", $dataname);
			if (mysqli_num_rows($get_company_info) > 0) {
				$company_info = mysqli_fetch_array($get_company_info);
			} else {
				echo '{"json_status":"NoRestaurants"}';
				exit;
			}

			lavu_connect_byid($company_info['id'], "poslavu_".$dataname."_db");

			$user_query = lavu_query("select * from `users` where `username`='[1]' and `password`=PASSWORD('[2]')", trim($_REQUEST['un']), trim($_REQUEST['pw']));
			if(mysqli_num_rows($user_query) > 0) {

				$extract = mysqli_fetch_array($user_query);

				$valid_PINs = "";
				$get_valid_PINs = lavu_query("SELECT `PIN` FROM `users` WHERE `access_level` > '0'");
				while ($extract2 = mysqli_fetch_array($get_valid_PINs)) {
					$valid_PINs = $valid_PINs."|".$extract2['PIN']."|";
				}

				$valid_admin_PINs = "";
				$get_valid_admin_PINs = lavu_query("SELECT `PIN` FROM `users` WHERE `access_level` > '1'");
				while ($extract2 = mysqli_fetch_array($get_valid_admin_PINs)) {
					$valid_admin_PINs = $valid_admin_PINs."|".$extract2['PIN']."|";
				}

				$get_all_user_data = lavu_query("SELECT * FROM `users` ORDER BY `l_name`, `f_name` ASC");
				while ($data_obj = mysqli_fetch_object($get_all_user_data)) {
					$user_data_array[] = $data_obj;
				}

				$clocked_in_server_ids = "";
				$i = 0;
				$get_punches = lavu_query("SELECT * FROM `clock_punches` where `_deleted` != '1' AND `punch_type` = 'Clocked In' and `punched_out` = '0'");
				while ($extract2 = mysqli_fetch_array($get_punches)) {
					$i++;
					if ($i < @mysqli_num_rows($get_punches)) {
						$clocked_in_server_ids .= $extract2['server_id']."|";
					} else {
						$clocked_in_server_ids .= $extract2['server_id'];
					}
				}

				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Server Logged In', '0', '0', now(), '[1]', '[2]', now())", $extract['f_name']." ".$extract['l_name'], $extract['id']);

				$update_last_activity = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `data_name` = '[1]'", $company_info['data_name']);

				$tack_on_build = "";
				if (isset($_REQUEST['build'])) {
					$tack_on_build = ":".$_REQUEST['build'];
				}
				//echo '{"json_status":"Valid","server_id":"'.$extract['id'].'","quick_serve":"'.$extract['quick_serve'].'","all_user_data":'.lavu_json_encode($user_data_array).',"clocked_in_server_ids":"'.$clocked_in_server_ids.'","access_level":"'.$extract['access_level'].'","PIN":"'.$extract['PIN'].'","f_name":"'.$extract['f_name'].'","l_name":"'.$extract['l_name'].'","company_name":"'.$company_info['company_name'].'","company_code":"'.$company_info['company_code'].'","data_name":"'.$company_info['data_name'].'","logo_img":"'.$company_info['logo_img'].'","dev":"'.$company_info['dev'].'","demo":"'.$company_info['demo'].'","valid_PINs":"'.$valid_PINs.'","valid_admin_PINs":"'.$valid_admin_PINs.'"}';
				echo '{"json_status":"Valid","server_id":"'.$extract['id'].'","quick_serve":"'.$extract['quick_serve'].'","service_type":"'.$extract['service_type'].'","all_user_data":'.lavu_json_encode($user_data_array).',"clocked_in_server_ids":"'.$clocked_in_server_ids.'","access_level":"'.$extract['access_level'].'","PIN":"'.$extract['PIN'].'","f_name":"'.$extract['f_name'].'","l_name":"'.$extract['l_name'].'","company_id":"'.$company_info['id'].'","company_name":"'.$company_info['company_name'].'","company_code":"'.lsecurity_name($company_info['company_code'],$company_info['id']).$tack_on_build.'","data_name":"'.$company_info['data_name'].'","logo_img":"'.$company_info['logo_img'].'","dev":"'.$company_info['dev'].'","demo":"'.$company_info['demo'].'","valid_PINs":"'.$valid_PINs.'","valid_admin_PINs":"'.$valid_admin_PINs.'"}';
				exit;


			} else {

				echo '{"json_status":"Invalid"}';
				exit;
			}

		} else {

			echo '{"json_status":"Invalid"}';
			exit;
		}

	} else if ($mode == 2) { // load company locations

		$locations_array = array();

		$exclude = array("integration1", "integration2", "integration3", "integration4", "api_key", "api_token");
 		$get_locations = lavu_query("SELECT ".create_field_list("locations", $exclude)." FROM `locations` ORDER BY `state`, `city`, `title` ASC");
		if (@mysqli_num_rows($get_locations) > 0) {
			while ($data_obj = mysqli_fetch_object($get_locations)) {
				$locations_array[] = $data_obj;
			}

			echo '{"json_status":"success","locations":'.lavu_json_encode($locations_array).'}';
			exit;

		} else {

			echo '{"json_status":"failure"}';
			exit;
		}

	} else if ($mode == 3) { // load table setup, printer configuration, menu categories, menu items, menu groups

		$table_setup_array = array();
		$discount_types_array = array();
		$menu_items = "";

		$get_table_setup = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '".$_REQUEST['loc_id']."' LIMIT 1");
		if (@mysqli_num_rows($get_table_setup) > 0) {
			$table_setup_array = mysqli_fetch_object($get_table_setup);
		} else {
			echo '{"json_status":"NoTS"}';
			exit;
		}

		$get_discount_types = lavu_query("SELECT * FROM `discount_types` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `_deleted` != '1' ORDER BY `title` ASC");
		while ($data_obj = mysqli_fetch_object($get_discount_types)) {
			$discount_types_array[] = $data_obj;
		}

		$get_server_name = lavu_query("SELECT * FROM users WHERE id = '".$_REQUEST['server_id']."'");
		$extract = mysqli_fetch_array($get_server_name);

		$reports_list_return_string = '"reports_list":[{"title":"Register Summary","mode":"register","filename":"till_report.php"},{"title":"Server Summary","mode":"server","filename":"till_report.php"}]';

		$printers_return_string = getSupportedPrinters($_REQUEST['loc_id']);

		$all_menu_data_return_string = loadMenuData($_REQUEST['menu_id'], $_REQUEST['loc_id']);

		$log_this = lavu_query("INSERT INTO action_log (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Location Chosen - ".getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title")."', '".$_REQUEST['loc_id']."', '0', now(), '".$extract['f_name']." ".$extract['l_name']."', '".$_REQUEST['server_id']."', now())");

		// load components if necessary

		$component_data_return_string = "";

		$comp_pack = (isset($_REQUEST['comp_pack']))?$_REQUEST['comp_pack']:"0";

		if ($comp_pack != "0") {
			$component_data_return_string = loadComponents($comp_pack);
		}

		if (isset($_REQUEST['UDID'])) {
			mlavu_query("UPDATE `poslavu_MAIN_db`.`client_devices` SET `needs_reload` = '0' WHERE `UDID` = '".$_REQUEST['UDID']."'");
		}

		// modifications field kept in place for older versions of the app (8/20/2010)

		echo '{"json_status":"success","table_setup":'.lavu_json_encode($table_setup_array).',"discount_types":'.lavu_json_encode($discount_types_array).','.$reports_list_return_string.','.$printers_return_string.','.$all_menu_data_return_string.$component_data_return_string.'}';
		exit;

	} else if ($mode == 4) { // load open order, returns number of send points

		$order_info = array();
		$order_contents = array();
		$send_points = 0;

		$check_order_id = "";
		if (isset($_REQUEST['order_id'])) {
			$check_order_id = " AND `order_id` = '".$_REQUEST['order_id']."'";
		}

		$get_order = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `closed` = '0000-00-00 00:00:00' AND `tablename` COLLATE latin1_general_cs = '".$_REQUEST['table']."'".$check_order_id);
		if (@mysqli_num_rows($get_order) > 0) {
			$data_obj = mysqli_fetch_object($get_order);
			$order_info[] = $data_obj;
			$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."' ORDER BY `id` ASC");
			$send_points = 0;
			while ($data_obj2 = mysqli_fetch_object($get_contents)) {
				$order_contents[] = $data_obj2;
				if ($data_obj2->item == "SENDPOINT") {
					$send_points++;
				}
			}

		} else {
			echo '{"json_status":"NoMatch"}';
			exit;
		}

		echo '{"json_status":"success","order_info":'.lavu_json_encode($order_info).',"order_contents":'.lavu_json_encode($order_contents).',"send_points":"'.$send_points.'"}';
		exit;

	} else if ($mode == 5) { // save open order info, returns order id

		// ***** order_ifo indexes *****
		//
		//  0 - order id
		//  1 - opened
		//  2 - closed
		//  3 - subtotal
		//  4 - taxrate
		//  5 - tax
		//  6 - total
		//  7 - server
		//  8 - tablename
		//  9 - send_status
		// 10 - guests
		// 11 - server id
		// 12 - number of checks
		// 13 - multiple tax rates
		// 14 - to tab or not to tab
		// 15 - deposit status

		$order_info = explode("|*|", $_REQUEST['order_info']);
		$order_id = "0";

		if ($order_info[0] == "0") {

			$check_for_previous_save = lavu_query("SELECT order_id FROM orders WHERE location_id = '".$_REQUEST['loc_id']."' AND opened = '".$order_info[1]."' AND total = '".$order_info[6]."' AND server = '".$order_info[7]."' AND tablename = '".$order_info[8]."'");

			if (mysqli_num_rows($check_for_previous_save) < 1) {

				$save_new_order = lavu_query("INSERT INTO orders (`order_id`, `location`, `location_id`, `opened`, `closed`, `subtotal`, `taxrate`, `tax`, `total`, `server`, `server_id`, `tablename`, `send_status`, `discount`, `discount_sh`, `gratuity`, `gratuity_percent`, `card_gratuity`, `cash_paid`, `card_paid`, `gift_certificate`, `change_amount`, `void`, `cashier`, `cashier_id`, `auth_by`, `auth_by_id`, `guests`, `email`, `permission`, `check_has_printed`, `no_of_checks`, `card_desc`, `transaction_id`, `multiple_tax_rates`, `tab`, `deposit_status`) VALUES ('".assignNext("order_id", "orders", "location_id", $_REQUEST['loc_id'])."', '".getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title")."', '".$_REQUEST['loc_id']."', '".$order_info[1]."', '".$order_info[2]."', '".$order_info[3]."', '".$order_info[4]."', '".$order_info[5]."', '".$order_info[6]."', '".$order_info[7]."', '".$order_info[11]."', '".$order_info[8]."', '".$order_info[9]."', '0.00', '', '0.00', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0', '', '0', '', '0', '".$order_info[10]."', '', '0', '0', '".$order_info[12]."', '', '', '".(isset($order_info[13])?$order_info[13]:"0")."', '".(isset($order_info[14])?$order_info[14]:"0")."', '".(isset($order_info[15])?$order_info[15]:"0")."')");

				$previous_save = FALSE;

			} else {

				$previous_save = TRUE;
			}

			$get_id = lavu_query("SELECT `order_id` FROM `orders` WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `opened` = '".$order_info[1]."' AND `total` = '".$order_info[6]."' AND `server` = '".$order_info[7]."' AND `tablename` = '".$order_info[8]."'");
			if (mysqli_num_rows($get_id) > 0) {
				$extract = mysqli_fetch_array($get_id);
				$order_id = $extract['order_id'];

				if ($previous_save == TRUE) {
					$update_order = lavu_query("UPDATE `orders` SET `subtotal = '".$order_info[3]."', `taxrate` = '".$order_info[4]."', `tax` = '".$order_info[5]."', `total` = '".$order_info[6]."', `send_status` = '".$order_info[9]."', `guests` = '".$order_info[10]."', `no_of_checks` = '".$order_info[12]."', `deposit_status` = '".(isset($order_info[15])?$order_info[15]:"0")."' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$order_id."'");
				}

				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Saved', '".$_REQUEST['loc_id']."', '".$order_id."', '".$order_info[1]."', '".$order_info[7]."', '".$order_info[11]."', now())");

				// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

				echo '{"json_status":"success","order_id":"'.$order_id.'"}';
				exit;

			} else {

				echo '{"json_status":"failure"}';
				exit;
			}

		} else {

			$update_order = lavu_query("UPDATE `orders` SET `tablename` = '".$order_info[8]."', `subtotal` = '".$order_info[3]."', `taxrate` = '".$order_info[4]."', `tax` = '".$order_info[5]."', `total` = '".$order_info[6]."', `send_status` = '".$order_info[9]."', `guests` = '".$order_info[10]."', `no_of_checks` = '".$order_info[12]."', `deposit_status` = '".(isset($order_info[15])?$order_info[15]:"0")."' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$order_info[0]."'");

			$check_update = lavu_query("SELECT `order_id` FROM `orders` WHERE location_id = '".$_REQUEST['loc_id']."' AND order_id = '".$order_info[0]."' AND `subtotal` = '".$order_info[3]."' AND `tax` = '".$order_info[5]."' AND `total` = '".$order_info[6]."'");
			if (mysqli_num_rows($check_update) > 0) {
				$extract = mysqli_fetch_array($check_update);
				$order_id = $extract['order_id'];

				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Saved', '".$_REQUEST['loc_id']."', '".$_REQUEST['loc_id']."', '".$order_id."', '".$order_info[1]."', '".$order_info[7]."', '".$order_info[11]."', now())");

				// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

				echo '{"json_status":"success","order_id":"'.$order_id.'"}';
				exit;

			} else {

				echo '{"json_status":"failure"}';
				exit;
			}
		}

	} else if ($mode == 6) { // save order contents (deprecated in favor of POST method, see mode = 17)

		/*$order_item = explode("|*|", $_REQUEST['order_item']);

		$save_order_item = lavu_query("INSERT INTO order_contents VALUES (NULL, '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$order_item[0]."', '".$order_item[1]."', '".$order_item[2]."', '".$order_item[3]."', '".$order_item[4]."', '".$order_item[5]."', '')");

		$check_save = lavu_query("SELECT * FROM order_contents WHERE item = '".$order_item[0]."' AND price = '".$order_item[1]."' AND quantity = '".$order_item[2]."' AND options = '".$order_item[3]."' AND special = '".$order_item[4]."' AND loc_id = '".$_REQUEST['loc_id']."' AND order_id = '".$_REQUEST['order_id']."'");
		if (mysqli_num_rows($check_save) > 0) {

			echo '{"json_status":"success2"}';
			exit;

		} else {

			echo '{"json_status":"failure2"}';
			exit;

		}*/

	} else if ($mode == 7) { // load open orders

		$lavu_togo = (isset($_REQUEST['lavu_togo']))?$_REQUEST['lavu_togo']:"0";

		$order_info = array();
		$order_contents = array();
		$cc_transactions = array();

		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `location_id` ='".$_REQUEST['loc_id']."' AND `closed` = '0000-00-00 00:00:00' AND `opened` != '0000-00-00 00:00:00' ORDER BY `order_id` ASC");
		if (@mysqli_num_rows($get_orders) > 0) {
			while ($data_obj = mysqli_fetch_object($get_orders)) {
				$order_info[] = $data_obj;
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."' ORDER BY `id` ASC");
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}
				$get_cc_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."' AND `voided` = '0' AND `pay_type` = 'Card' AND `action` = 'Sale' AND `got_response` = '1'");
				while ($data_obj4 = mysqli_fetch_object($get_cc_transactions)) {
					$cc_transactions[] = $data_obj4;
				}
			}
		} else {
			echo '{"json_status":"NoMatch"}';
			exit;
		}

		echo '{"json_status":"success","order_info":'.lavu_json_encode($order_info).',"order_contents":'.lavu_json_encode($order_contents).',"cc_transactions":'.lavu_json_encode($cc_transactions).'}';
		exit;

	} else if ($mode == 8) { // load today's closed orders

		if (isset($_REQUEST['check_time'])) {

			$start_datetime = $_REQUEST['check_time'];

		} else {

			$se_hour = substr(str_pad($_REQUEST['start_time'], 4, "0", STR_PAD_LEFT), 0, 2);
			$se_min = substr(str_pad($_REQUEST['start_time'], 4, "0", STR_PAD_LEFT), 2, 2);

			$now_parts = explode(" ", $_REQUEST['now_time']);
			$now_date = explode("-", $now_parts[0]);
			$now_time = explode(":", $now_parts[1]);

			if (($now_time[0].$now_time[1]) < ($se_hour.$se_min)) {
				$use_ts = mktime($se_hour, $se_min, 0, $now_date[1], ($now_date[2] - 1), $now_date[0]);
				$start_datetime = date("Y-m-d H:i:s", $use_ts);
			} else {
				$start_datetime = $now_parts[0]." $se_hour:$se_min:00";
			}
		}

		$order_info = array();
		$order_contents = array();
		$check_details = array();
		$cc_transactions = array();
		$recorded_payments = array();

		$rpfilter = "";
		if ($location_info['integrateCC'] == "1") {
			$rpfilter = " AND (`pay_type` = 'Cash' OR (`pay_type` = 'Card' AND `action` = 'Refund') OR `pay_type` = 'Gift Certificate')";
		}

		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `location_id` ='".$_REQUEST['loc_id']."' AND `closed` >= '".$start_datetime."' AND `void` != '1' ORDER BY `closed` DESC");
		if (@mysqli_num_rows($get_orders) > 0) {
			while ($data_obj = mysqli_fetch_object($get_orders)) {
				$order_info[] = $data_obj;
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."' ORDER BY `id` ASC");
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}
				$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."'");
				while ($data_obj3 = mysqli_fetch_object($get_details)) {
					$check_details[] = $data_obj3;
				}
				$get_cc_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."' AND `pay_type` = 'Card' AND `action` = 'Sale' AND `voided` = '0'");
				while ($data_obj4 = mysqli_fetch_object($get_cc_transactions)) {
					$cc_transactions[] = $data_obj4;
				}
				$get_recorded_payments = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."'".$rpfilter." AND `voided` = '0' ORDER BY `id` ASC");
				while ($data_obj5 = mysqli_fetch_object($get_recorded_payments)) {
					$recorded_payments[] = $data_obj5;
				}
			}

		} else {
			echo '{"json_status":"NoMatch"}';
			exit;
		}

		echo '{"json_status":"success","order_info":'.lavu_json_encode($order_info).',"order_contents":'.lavu_json_encode($order_contents).',"check_details":'.lavu_json_encode($check_details).',"cc_transactions":'.lavu_json_encode($cc_transactions).',"recorded_payments":'.lavu_json_encode($recorded_payments).'}';
		exit;

	} else if ($mode == 9) { // check open orders

		$open_orders = "";
		$checks_printed = "";
		$table_list = "";
		$id_list = "";
		$printed_list = "";

		if (isset($_REQUEST['tab_mode'])) {
			if ($_REQUEST['tab_mode'] == "tabs") {
				$tab_filter = " AND `tab` = '1'";
			} else if ($_REQUEST['tab_mode'] == "tables") {
				$tab_filter = " AND `tab` != '1'";
			} else if ($_REQUEST['tab_mode'] == "both") {
				$tab_filter = "";
			} else {
				$tab_filter = " AND `tab` != '1'";
			}
		} else {
			$tab_filter = " AND `tab` != '1'";
		}

		if (isset($_REQUEST['for_id'])) {
			if ($_REQUEST['for_id'] == "0") {
				$server_id_filter = "";
			} else {
				$server_id_filter = " AND `server_id` = '".$_REQUEST['for_id']."'";
			}
		} else {
			$server_id_filter = "";
		}

		$get_open_orders = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `tablename` != 'Quick Serve' AND `closed` = '0000-00-00 00:00:00'".$tab_filter.$server_id_filter." ORDER BY `tablename` ASC");
		if (mysqli_num_rows($get_open_orders) > 0) {
			while ($extract = mysqli_fetch_array($get_open_orders)) {
				$table_list .= "|".$extract['tablename']."|";
				$id_list .= "|".$extract['order_id']."|";
				if ($extract['check_has_printed'] == "1") {
					$printed_list .= "|".$extract['tablename']."|";
				}
			}
			$open_orders = '"open_orders":"|'.$table_list.'|"';
			$checks_printed = '"checks_printed":"|'.$printed_list.'|"';
		} else {
			$open_orders = '"open_orders":"none"';
			$checks_printed = '"checks_printed":"none"';
		}

		echo '{"json_status":"success",'.$open_orders.','.$checks_printed.',"open_ids":"|'.$id_list.'|"}';
		exit;

	} else if ($mode == 10) { // close order after checkout

		// ***** close_ifo indexes *****
		//
		//  0 - closed
		//  1 - discount
		//  2 - discount label
		//  3 - cash paid
		//  4 - card paid
		//  5 - change
		//  6 - server name
		//  7 - tax amount
		//  8 - total
		//  9 - PIN used
		// 10 - tax rate
		// 11 - send to email
		// 12 - gratuity
		// 13 - cashier ID
		// 14 - paid by gift certificate
		// 15 - gratuity percent
		// 16 - refund amount
		// 17 - card description
		// 18 - transaction id
		// 19 - subtotal
		// 20 - deposit status
		// 21 - active register
		// 22 - discount value
		// 23 - discount type
		// 24 - refund cc amount

		$close_info = explode("|*|", $_REQUEST['close_info']);

		$auth_by = convertPIN($close_info[9], "name");
		$auth_by_id = convertPIN($close_info[9]);

		if (isset($close_info[19])) {
			$update_subtotal = ", `subtotal` = '".$close_info[19]."'";
		} else {
			$update_subtotal = "";
		}

		$log_this_close = true;

		if (isset($_REQUEST['item_details'])) {
			$item_details_array = explode("|*|", $_REQUEST['item_details']);
			$get_order_contents = lavu_query("SELECT * FROM `order_contents` WHERE `order_id` = '".$_REQUEST['order_id']."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `item` != 'SENDPOINT' ORDER BY `id` ASC");
			if (@mysqli_num_rows($get_order_contents) > 0) {
				$row = 0;
				while ($extract = mysqli_fetch_assoc($get_order_contents)) {
					$details = explode(":", $item_details_array[$row]);
					$update_item = lavu_query("UPDATE `order_contents` SET `discount_amount` = '".$details[0]."', `discount_value` = '".$details[1]."', `discount_type` = '".$details[2]."', `after_discount` = '".$details[3]."', `tax_amount` = '".$details[4]."' WHERE `id` = '".$extract['id']."'");
					$row++;
				}
			}
		}

		//$update_closed = lavu_query("UPDATE `orders` SET `closed` = '".$close_info[0]."', `taxrate` = '".$close_info[10]."', `tax` = '".$close_info[7]."', `total` = '".$close_info[8]."', `discount` = '".$close_info[1]."', `discount_sh` = '".$close_info[2]."', `discount_value` = '".(isset($close_info[22])?$close_info[22]:"0")."', `discount_type` = '".(isset($close_info[23])?$close_info[23]:"0")."', `gratuity` = '".$close_info[12]."', `gratuity_percent` = '".$close_info[15]."', `cash_paid` = '".$close_info[3]."', `card_paid` = '".$close_info[4]."', `gift_certificate` = '".$close_info[14]."', `change_amount` = '".$close_info[5]."', `reopen_refund` = '".$close_info[16]."', `cashier` = '".$close_info[6]."', `cashier_id` = '".$close_info[13]."', `auth_by` = '".$auth_by."', `auth_by_id` = '".$auth_by_id."', `email` = '".$close_info[11]."', `card_desc` = '".$close_info[17]."', `transaction_id` = '".$close_info[18]."', `register` = '".(isset($close_info[21])?$close_info[21]:"")."'".$update_subtotal." WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");

		$record_card_paid = "";
		if ($location_info['integrateCC'] != "1") {
			$record_card_paid = ", `card_paid` = '".$close_info[4]."'";
		}

		$update_closed = lavu_query("UPDATE `orders` SET `closed` = '".$close_info[0]."', `taxrate` = '".$close_info[10]."', `tax` = '".$close_info[7]."', `total` = '".$close_info[8]."', `discount` = '".$close_info[1]."', `discount_sh` = '".$close_info[2]."', `discount_value` = '".(isset($close_info[22])?$close_info[22]:"0")."', `discount_type` = '".(isset($close_info[23])?$close_info[23]:"0")."', `gratuity` = '".$close_info[12]."', `gratuity_percent` = '".$close_info[15]."', `cash_paid` = '".$close_info[3]."', `cash_applied` = '".($close_info[3] - $close_info[5])."'".$record_card_paid.", `gift_certificate` = '".$close_info[14]."', `change_amount` = '".$close_info[5]."', `refunded` = '".$close_info[16]."', `refunded_cc` = '".$close_info[24]."', `cashier` = '".$close_info[6]."', `cashier_id` = '".$close_info[13]."', `auth_by` = '".$auth_by."', `auth_by_id` = '".$auth_by_id."', `email` = '".$close_info[11]."', `card_desc` = '".$close_info[17]."', `transaction_id` = '".$close_info[18]."', `register` = '".(isset($close_info[21])?$close_info[21]:"")."'".$update_subtotal." WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");

		$deposit_order_ids = array();
		$total_deposit_amount = 0;

		$get_order_contents = lavu_query("SELECT `order_contents`.`item_id` as item_id, `order_contents`.`subtotal_with_mods` as subtotal_with_mods, `order_contents`.`allow_deposit` as allow_deposit, `order_contents`.`deposit_info` as deposit_info, `order_contents`.`item` as item, `order_contents`.`options` as options, `order_contents`.`quantity` as quantity, `menu_items`.`hidden_value` as hidden_value, `menu_items`.`hidden_value2` as hidden_value2 FROM `order_contents` LEFT JOIN `menu_items` ON `menu_items`.`id` = `order_contents`.`item_id` WHERE `order_contents`.`loc_id` = '".$_REQUEST['loc_id']."' AND `order_contents`.`order_id` = '".$_REQUEST['order_id']."'");
		if (@mysqli_num_rows($get_order_contents) > 0) {
			$should_update_deposit_status = 0;
			while ($extract = mysqli_fetch_array($get_order_contents)) {

				// deduct inventory and record usage
				$itemid = $extract['item_id'];
				$item_qty = $extract['quantity'];
				$usage_query = lavu_query("select * from `ingredient_usage` where `orderid`='".$_REQUEST['order_id']."' and `itemid`='".$itemid."'");
				if(mysqli_num_rows($usage_query))
				{
					$usage_read = mysqli_fetch_assoc($usage_query);
					$usage_qty = $usage_read['qty'];
					$usage_id = $usage_read['id'];
				}
				else 
				{
					$usage_qty = 0;
					$usage_id = false;
				}
				$used_qty = $item_qty - $usage_qty;
				if($used_qty > 0)
				{
					$item_query = lavu_query("select * from `menu_items` where `id`='".$itemid."'");
					if(mysqli_num_rows($item_query))
					{
						$item_read = mysqli_fetch_assoc($item_query);
						$inglist = explode(",",$item_read['ingredients']);
						for($i=0; $i<count($inglist); $i++)
						{
							$inginfo = explode("x",$inglist[$i]);
							$ingid = trim($inginfo[0]);
							if(count($inginfo) > 1) $ingqty = trim($inginfo[1]);
							else $ingqty = 1;

							lavu_query("insert into `ingredient_usage` (`ts`,`date`,`orderid`,`itemid`,`ingredientid`,`qty`) values ('".time()."','".date("Y-m-d H:i:s")."','".$_REQUEST['order_id']."','".$itemid."','".$ingid."','".($used_qty * $ingqty)."')");
							lavu_query("update `ingredients` set `qty`=`qty`-".($used_qty * $ingqty)." where `id`=".$ingid);
						}
					}

					if($usage_id)
						lavu_query("update `ingredient_usage` set `qty`='".$item_qty."' where `id`='".$usage_id."'");
					else
						lavu_query("insert into `ingredient_usage` (`ts`,`date`,`orderid`,`itemid`,`ingredientid`,`qty`) values ('".time()."','".date("Y-m-d H:i:s")."','".$_REQUEST['order_id']."','".$itemid."','','".$item_qty."')");
				}

				// check for need to apply LavuKart race credits or membership

				if (strstr($extract['options'], "RACER:")) {
					$racer_id = substr($extract['options'], (strpos($extract['options'], "(") + 1), (strpos($extract['options'], ")") - (strpos($extract['options'], "(") + 1)));
					$get_racer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '".$racer_id."'");
					if (@mysqli_num_rows($get_racer_info) > 0) {
						$extract_r = mysqli_fetch_array($get_racer_info);

						$set_confirmed = "";
						if ($extract_r['confirmed_by_attendant'] == "") {
							$set_confirmed = ", `confirmed_by_attendant` = '".$close_info[13]."'";
							$signature = "Signature";
							if ($extract_r['minor_or_adult_at_signup'] == "minor") {
								$signature = "Signatures";
							}
							$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$signature." Confirmed', '".$racer_id."', '".$_REQUEST['order_id']."', now(), '".$close_info[13]."', '0', '".$_REQUEST['loc_id']."')");
						}

						$update_total_visits = "";
						$today = date("Y-m-d");
						$last_activity = explode(" ", $extract_r['last_activity']);
						if ($last_activity != $today) {
							$update_total_visits = ", `total_visits` = `total_visits`+1";
						}

						if ($extract['hidden_value2'] == "membership") {

							$add_credits = ($extract['hidden_value'] * $extract['quantity']);

							$now = time();
							$update_membership = lavu_query("UPDATE `lk_customers` SET `credits` = `credits`+".$add_credits.$set_confirmed.", `membership` = '".$extract['item']."', `membership_expiration` = '".date("Y-m-d", mktime(date("h", $now), date("i", $now), date("s", $now), date("m", $now), date("d", $now), (date("Y", $now) + 1)))."', `last_activity` = now()".$update_total_visits." WHERE `id` = '".$racer_id."'");

							$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$extract['item']."', '".$racer_id."', '".$_REQUEST['order_id']."', now(), '".$close_info[13]."', '".$add_credits."', '".$_REQUEST['loc_id']."')");

						} else if ($extract['hidden_value2'] == "race credits") {

							$add_credits = ($extract['hidden_value'] * $extract['quantity']);

							$show_s = "";
							if ($add_credits > 1) {
								$show_s = "s";
							}

							$update_credits = lavu_query("UPDATE `lk_customers` SET `credits` = `credits`+".$add_credits.$set_confirmed.", `last_activity` = now()".$update_total_visits." WHERE id = '".$racer_id."'");

							$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('Added ".$add_credits." Race Credit".$show_s."', '".$racer_id."', '".$_REQUEST['order_id']."', now(), '".$close_info[13]."', '".$add_credits."', '".$_REQUEST['loc_id']."')");
						}
					}
				}

				// check for LavuKart group event info, payments, deposits

				if ($extract['hidden_value2'] == "group event") {
					$info_parts = explode(":", $extract['options']);
					$event_id = str_replace("Event #", "", $info_parts[0]);
					$update_event_record = lavu_query("UPDATE `lk_group_events` SET `item_id` = '".$extract['item_id']."', `duration` = '".$extract['hidden_value']."', `order_id` = '".$_REQUEST['order_id']."', `checked_out` = '1' WHERE `id` = '".$event_id."'");
					$update_log_record = lavu_query("UPDATE `lk_action_log` SET `order_id` = '".$_REQUEST['order_id']."' WHERE `group_event_id` = '".$event_id."'");
				}

				if ($extract['allow_deposit'] == "1") {
					$should_update_deposit_status = 3;
				} else if ($extract['allow_deposit'] == "2") {
					$should_update_deposit_status = 2;
					$total_deposit_amount = ($total_deposit_amount + $extract['subtotal_with_mods']);
				}

				//if ($close_info[20] == "3") {
				//	$deposit_info = explode("#", $extract['deposit_info']);
				//	if (!in_array($deposit_info[1], $deposit_order_ids)) {
				//		$deposit_order_ids[] = $deposit_info[1];
				//	}
				//}
			}
		}

		// apply appropriate LavuKart special event deposit status

		if ($should_update_deposit_status != 0) {
			$set_as_opened = "";
			$record_deposit_amount = "";
			if ($close_info[20] == "3") {
				$log_message = "Remaining payment received for Group Event";
				$should_update_deposit_status = 3;
				//foreach ($deposit_order_ids as $doi) {
					//$update_order = lavu_query("UPDATE `orders` SET `deposit_status` = '3' WHERE `order_id` = '".$doi."' AND `location_id` = '".$_REQUEST['loc_id']."'");
				//}
			} else {
				if ($should_update_deposit_status == 2) {
					$log_message = "Deposit received for Group Event";
					$set_as_opened = ", `closed` = '0000-00-00 00:00:00'";
					$record_deposit_amount = ", `deposit_amount` = '".number_format($total_deposit_amount, $decimal_places, ".", "")."', `subtotal_without_deposit` = '".number_format(($close_info[8] - $total_deposit_amount), $decimal_places, ".", "")."'";
					$log_this_close = false;
				} else if ($should_update_deposit_status == 3) {
					$log_message = "Full payment received for Group Event";
				}
			}
			$update_deposit_status = lavu_query("UPDATE `orders` SET `deposit_status` = '".$should_update_deposit_status."'".$set_as_opened.$record_deposit_amount." WHERE `order_id` = '".$_REQUEST['order_id']."' AND location_id = '".$_REQUEST['loc_id']."'");
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$log_message."', '0', '".$_REQUEST['order_id']."', now(), '".$close_info[13]."', '0', '".$_REQUEST['loc_id']."')");
		}

		if ($log_this_close == true) {
			$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Closed', '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$close_info[0]."', '".$close_info[6]."', '".$close_info[13]."', now())");

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		}

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 11) { // update send_status for open order, save send point, log send

		// ***** order_info indexes *****
		//
		//  0 order id
		//  1 table 
		//  2 server who opened order (name)
		//  3 order total
		//  4 time
		//  5 server who sent order (name)
		//  6 server who sent order (id)
		//  7 server who opened order (id)

		$order_info = explode("|*|", $_REQUEST['order_info']);

		$update_send_status = lavu_query("UPDATE `orders` SET `send_status` = '0' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$order_info[0]."'");
		$insert_send_point = lavu_query("INSERT INTO `order_contents` (`loc_id`, `order_id`, `item`, `price`, `quantity`, `options`, `special`, `modify_price`, `print`, `check`, `seat`, `item_id`, `printer`, `apply_taxrate`, `custom_taxrate`, `modifier_list_id`) VALUES ('".$_REQUEST['loc_id']."', '".$order_info[0]."', 'SENDPOINT', '0', '".$order_info[6]."', '".$order_info[5]."', '".$order_info[4]."', '0', '0', '0', '0', '0', '0', '', '0', '0')");
		$update_send_log = lavu_query("INSERT INTO `send_log` (`order_id`, `location`, `location_id`, `tablename`, `total`, `server`, `server_id`, `send_server`, `send_server_id`, `time_sent`) VALUES ('".$order_info[0]."', '".getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title")."', '".$_REQUEST['loc_id']."', '".$order_info[1]."', '".$order_info[3]."', '".$order_info[2]."', '".$order_info[7]."', '".$order_info[5]."', '".$order_info[6]."', '".$order_info[4]."')");
		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Sent to Kitchen', '".$_REQUEST['loc_id']."', '".$order_info[0]."', '".$order_info[4]."', '".$order_info[5]."', '".$order_info[6]."', now())");

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 12) { // perform void

		// ****** item_info indexes ******
		//
		//  0 - time
		//  1 - server name
		//  2 - PIN used

		$void_info = explode("|*|", $_REQUEST['void_info']);

		$auth_by = convertPIN($void_info[2], "name");
		$auth_by_id = convertPIN($void_info[2]);

		$record_void = lavu_query("UPDATE `orders` SET `closed` = '".$void_info[0]."', `void` = '1', `cashier` = '".$void_info[1]."', `auth_by` = '".$auth_by."', `auth_by_id` = '".$auth_by_id."' WHERE `location` = '".str_replace("'", "''", getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title"))."' AND `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");
		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Voided', '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$void_info[0]."', '".$auth_by."', '".$auth_by_id."', now())");

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 13) { // log item edits after send

		// ****** item_info indexes ******
		//
		//  0 - note
		//  1 - order id
		//  2 - item name
		//  3 - options
		//  4 - special
		//  5 - quantity
		//  6 - time
		//  7 - server name
		//  8 - server ID
		//  9 - PIN used
		// 10 - modify_price

		$item_info = explode("|*|", $_REQUEST['item_info']);

		$auth_by = convertPIN($item_info[9], "name");

		$update_log = lavu_query("INSERT INTO `edit_after_send_log` (`note`, `order_id`, `location`, `location_id`, `item`, `options`, `special`, `modify_price`, `quantity`, `time`, `server`, `server_id`, `auth_by`) VALUES ('".$item_info[0]."', '".$item_info[1]."', '".getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title")."', '".$_REQUEST['loc_id']."', '".$item_info[2]."', '".$item_info[3]."', '".$item_info[4]."', '".$item_info[10]."', '".$item_info[5]."', '".$item_info[6]."', '".$item_info[7]."', '".$item_info[8]."', '".$auth_by."')");

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 14) { // email receipt

		// ****** order_info indexes ******
		//
		//  0 - send to email
		//  1 - company name
		//  2 - location title
		//  3 - location address
		//  4 - location city
		//  5 - location state
		//  6 - location zip
		//  7 - location phone
		//  8 - location website
		//  9 - location manager
		// 10 - order id
		// 11 - tablename
		// 12 - server
		// 13 - cashier
		// 14 - closed time
		// 15 - subtotal
		// 16 - discount label string
		// 17 - discount
		// 18 - subtotal after discount
		// 19 - tax rate
		// 20 - tax amount
		// 21 - gratuity amount
		// 22 - total
		// 23 - cash paid
		// 24 - paid by card
		// 25 - total paid
		// 26 - change amount
		// 27 - cashier id
		// 28 - paid by gift certificate
		// 29 - gratuity percent
		// 30 - refund amount
		// 31 - gratuity label
		// 32 - refund_cc amount

		$order_info = explode("|*|", $_POST['order_info']);
		$quantities = explode("|*|", $_POST['quantities']);
		$items = explode("|*|", $_POST['items']);
		$prices = explode("|*|", $_POST['prices']);
		$options = explode("|*|", $_POST['options']);
		$specials = explode("|*|", $_POST['specials']);
		$modify_prices = explode("|*|", $_POST['modify_prices']);
		$forced_modifiers_prices = explode("|*|", $_POST['forced_modifiers_prices']);
		$cc_transactions = explode("|*|", $_POST['cc_transactions']);

		$tax_rates_and_amounts = array();
		$get_tax_rates_and_amounts = lavu_query("SELECT * FROM `order_contents` WHERE `tax_amount` != '' AND `order_id` = '".$order_info[10]."' AND `loc_id` = '".$_REQUEST['loc_id']."'");
		while ($extract = mysqli_fetch_assoc($get_tax_rates_and_amounts)) {
			$tax_rate = $extract['custom_taxrate'];
			if (array_key_exists($tax_rate, $tax_rates_and_amounts)) {
				$previous_tax = $tax_rates_and_amounts[$tax_rate];
				$tax_rates_and_amounts[$tax_rate] = ($extract['tax_amount'] + $previous_tax);
			} else {
				$tax_rates_and_amounts[$tax_rate] = $extract['tax_amount'];
			}
		}

		$subject_line = "Receipt from ".$order_info[1];

		$tax_label = "Tax (".($order_info[19] * 100)."%):";

		$email_body = "Thank you so much for allowing us to serve you today.<br />
<br />
Order ID: ".$order_info[10]."<br />
Table: ".$order_info[11]."<br />
Server: ".$order_info[12]."<br />
Cashier: ".$order_info[13]."<br />
Date and Time: ".$order_info[14]."<br />
<br />
<br />
<table cellspacing='0' cellpadding='2' width='400px'>";

		for ($i = 0; $i < count($quantities); $i++) {

			$item = $items[$i];
			if (strlen($item > 30)) {
				$item = substr($item, 0, 34);
			}

			$email_body .= "<tr><td align='right' valign='top' style='padding:5px 5px 2px 5px'>".$quantities[$i]."</td><td align='left' valign='top' style='padding:5px 5px 2px 5px'>".$item."</td><td align='right' valign='top' style='padding:5px 5px 2px 5px'>".number_format($prices[$i], $decimal_places, ".", "")."</td></tr>";

			if ($options[$i] != "") {
				$option = $options[$i];
				if (strlen($option > 35)) {
					$option = substr($option, 0, 34);
				}
				if ($forced_modifiers_prices[$i] != 0) {
					$email_body .= "<tr><td></td><td align='left' valign='top' style='padding:0px 5px 2px 5px'> - ".$option."</td><td align='right' valign='top' style='padding:0px 5px 2px 5px'>".number_format(($forced_modifiers_prices[$i] * $quantities[$i]), $decimal_places, ".", "")."</td></tr>";
				} else {
					$email_body .= "<tr><td></td><td align='left' valign='top' style='padding:2px 5px 2px 5px'> - ".$option."</td><td></td></tr>";
				}
			}
			if ($specials[$i] != "") {
				$special = $specials[$i];
				if (strlen($special > 35)) {
					$special = substr($special, 0, 34);
				}
				if ($modify_prices[$i] != 0) {
					$email_body .= "<tr><td></td><td align='left' valign='top' style='padding:0px 5px 2px 5px'> * ".$special."</td><td align='right' valign='top' style='padding:0px 5px 2px 5px'>".number_format(($modify_prices[$i] * $quantities[$i]), $decimal_places, ".", "")."</td></tr>";
				} else {
					$email_body .= "<tr><td></td><td align='left' valign='top' style='padding:2px 5px 2px 5px'> * ".$special."</td><td></td></tr>";
				}
			}
		}
		$email_body .= "<tr><td colspan='3'><hr color='#EEEEEE'></td></tr>";
		$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>Subtotal:</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[15], $decimal_places, ".", "")."</td></tr>";
		if ($order_info[17] != "0.00") {
			$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".$order_info[16]."</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[17], $decimal_places, ".", "")."</td></tr>";
			$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>Subtotal after discount:</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[18], $decimal_places, ".", "")."</td></tr>";
		}
		if (count($tax_rates_and_amounts) > 0) {
			$rates = array_keys($tax_rates_and_amounts);
			foreach ($rates as $rate) {
				$tax_label = "Tax (".($rate * 100)."%):";
				$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".$tax_label."</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($tax_rates_and_amounts[$rate], $decimal_places, ".", "")."</td></tr>";
			}
		} else {
			$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".$tax_label."</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[20], $decimal_places, ".", "")."</td></tr>";
		}
		if ($order_info[21] != "0.00") {
			$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".$order_info[31]." (".($order_info[29] * 100)."%):</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[21], $decimal_places, ".", "")."</td></tr>";
		}
		$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 12px 5px'>Total:</td><td align='right' valign='top' style='padding:2px 5px 12px 5px'>".number_format($order_info[22], $decimal_places, ".", "")."</td></tr>";
		if ($order_info[23] != "0.00") {
			$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>Paid in cash:</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[23], $decimal_places, ".", "")."</td></tr>";
		}
		if ($order_info[24] != "0.00") {
			if ($cc_transactions[0] != "") {
				foreach ($cc_transactions as $cct) {
					$cct_parts = explode(":", $cct);
					$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>Paid with card (".$cct_parts[0]."):</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($cct_parts[1], $decimal_places, ".", "")."</td></tr>";
				}
			} else {
				$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>Paid with card:</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[24], $decimal_places, ".", "")."</td></tr>";
			}
		}
		if ($order_info[28] != "0.00") {
			$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>Paid with gift certificate:</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[28], $decimal_places, ".", "")."</td></tr>";
		}
		if ((($order_info[23] != 0) && ($order_info[24] != 0)) || (($order_info[23] != 0) && ($order_info[28] != 0)) || (($order_info[28] != 0) && ($order_info[24] != 0))) {
			$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>Total paid:</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[25], $decimal_places, ".", "")."</td></tr>";
		}
		$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>Change:</td><td align='right' valign='top' style='padding:2px 5px 2px 5px'>".number_format($order_info[26], $decimal_places, ".", "")."</td></tr>";
		if ($order_info[30] != 0) {
			$email_body .= "<tr><td></td><td align='right' valign='top' style='padding:12px 5px 2px 5px'>Refunded (after reopen):</td><td align='right' valign='top' style='padding:12px 5px 2px 5px'>".number_format($order_info[30], $decimal_places, ".", "")."</td></tr>";
		}

$email_body .= "</table><br />
<br />
Have a great day! Visit us again soon!<br />
<br />
".$order_info[2]."<br />
".$order_info[3]."<br />
".$order_info[4].", ".$order_info[5]." ".$order_info[6]."<br />
".$order_info[7]."<br />
".$order_info[8]."<br />
General Manager: ".$order_info[9]."<br />
";

		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		$headers .= "From: ".$order_info[1]." POSLavu <receipts@poslavu.com>" . "\r\n";

		mail($order_info[0], $subject_line, $email_body, $headers);

		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Receipt Emailed to ".$order_info[0]."', '".$_REQUEST['loc_id']."', '".$order_info[10]."', '".$order_info[14]."', '".$order_info[13]."', '".$order_info[27]."', now())");

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo "1";
		exit;

	} else if ($mode == 15) { // ******************************* save payment and discount details ******************************

		// this mode is considered deprecated after version 1.6.5 build 20110406-1, replaced by mode 40

		// ***** check_info indexes *****
		//
		//  0 - order id
		//  1 - discount amount
		//  2 - discount label
		//  3 - gratuity amount
		//  4 - guests
		//  5 - cash amount
		//  6 - card amount
		//  7 - gift certificate amount
		//  8 - check count
		//  9 - active check
		// 10 - card description (last four)
		// 11 - transaction_id
		// 12 - subtotal
		// 13 - tax amount
		// 14 - check total
		// 15 - refund amount
		// 16 - remaining amount
		// 17 - change amount
		// 18 - discount value
		// 19 - gratuity percent
		// 20 - register
		// 21 - discount type
		// 22 - datetime
		// 23 - refund_cc amount
		// 24 - server name
		// 25 - server id

		$check_info = explode("|*|", $_REQUEST['check_info']);

		$record_datetime = "now()";
		if (isset($check_info[22])) {
			$record_datetime = "'".$check_info[22]."'";
		}

		if ((float)$check_info[5] > 0) { // record cash payment
			$check_previous = lavu_query("SELECT SUM(`amount`) as `cash_sum`, SUM(`total_collected`) as `cash_collected` FROM `cc_transactions` WHERE `order_id` = '".$check_info[0]."' AND `check` = '".$check_info[9]."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Cash' AND `action` = 'Sale'");
			$previous_sum = 0;
			$previous_collected = 0;
			if (@mysqli_num_rows($check_previous) > 0) {
				$extract = mysqli_fetch_assoc($check_previous);
				$previous_sum = $extract['cash_sum'];
				$previous_collected = $extract['cash_collected'];
			}
			if ((float)number_format(($check_info[5] - $previous_collected), $decimal_places, ".", "") > 0) {
				$save_cash_payment = lavu_query("INSERT INTO `cc_transactions` (`order_id`, `check`, `amount`, `loc_id`, `datetime`, `pay_type`, `register`, `change`, `total_collected`, `server_name`, `server_id`, `action`) VALUES ('".$check_info[0]."', '".$check_info[9]."', '".number_format(($check_info[5] - $previous_sum), $decimal_places, ".", "")."', '".$_REQUEST['loc_id']."', $record_datetime, 'Cash', '".(isset($check_info[20])?$check_info[20]:"")."', '".number_format((isset($check_info[17])?$check_info[17]:0), $decimal_places, ".", "")."', '".number_format((($check_info[5] - $previous_sum) - (isset($check_info[17])?$check_info[17]:0)), $decimal_places, ".", "")."', '".(isset($check_info[24])?$check_info[24]:"")."', '".(isset($check_info[25])?$check_info[25]:"")."', 'Sale')");
			} else if ($previous_collected > ($check_info[5] - $check_info[17])) {
				$change_left = $check_info[17];
				$get_previous = lavu_query("SELECT * FROM `cc_transactions` WHERE `order_id` = '".$check_info[0]."' AND `check` = '".$check_info[9]."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Cash' AND `action` = 'Sale' ORDER BY `id` DESC");
				while ($extract2 = mysqli_fetch_assoc($get_previous)) {
					if ($extract2['amount'] < $change_left) {
						$update_payment = lavu_query("UPDATE `cc_transactions` SET `change` = '".number_format($extract2['amount'], $decimal_places, ".", "")."', `total_collected` = '".number_format(0, $decimal_places, ".", "")."' WHERE `id` = '".$extract2['id']."'");
						$change_left = ($change_left - $extract2['amount']);
					} else {
						$update_payment = lavu_query("UPDATE `cc_transactions` SET `change` = '".number_format($check_info[17], $decimal_places, ".", "")."', `total_collected` = '".number_format(($extract2['amount'] - $check_info[17]), $decimal_places, ".", "")."' WHERE `id` = '".$extract2['id']."'");
						break;
					}
				}
			}
		}
		if (((float)$check_info[6] > 0) && ($location_info['integrateCC'] != "1")) { // record card payment
			$check_previous = lavu_query("SELECT SUM(`amount`) as `card_sum` FROM `cc_transactions` WHERE `order_id` = '".$check_info[0]."' AND `check` = '".$check_info[9]."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Card' AND `action` = 'Sale'");
			$previous_sum = 0;
			if (@mysqli_num_rows($check_previous) > 0) {
				$extract = mysqli_fetch_assoc($check_previous);
				$previous_sum = $extract['card_sum'];
			}
			if ((float)number_format(($check_info[6] - $previous_sum), $decimal_places, ".", "") > 0) {
				$save_card_payment = lavu_query("INSERT INTO `cc_transactions` (`order_id`, `check`, `amount`, `loc_id`, `datetime`, `pay_type`, `register`, `change`, `total_collected`, `server_name`, `server_id`, `action`) VALUES ('".$check_info[0]."', '".$check_info[9]."', '".number_format(($check_info[6] - $previous_sum), $decimal_places, ".", "")."', '".$_REQUEST['loc_id']."', $record_datetime, 'Card', '".(isset($check_info[20])?$check_info[20]:"")."', '".number_format(0, $decimal_places, ".", "")."', '".number_format(($check_info[6] - $previous_sum), $decimal_places, ".", "")."', '".(isset($check_info[24])?$check_info[24]:"")."', '".(isset($check_info[25])?$check_info[25]:"")."', 'Sale')");
			}
		}
		if ((float)$check_info[7] > 0) { // record gift certificate payment
			$check_previous = lavu_query("SELECT SUM(`amount`) as `gc_sum` FROM `cc_transactions` WHERE `order_id` = '".$check_info[0]."' AND `check` = '".$check_info[9]."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Gift Certificate' AND `action` = 'Sale'");
			$previous_sum = 0;
			if (@mysqli_num_rows($check_previous) > 0) {
				$extract = mysqli_fetch_assoc($check_previous);
				$previous_sum = $extract['gc_sum'];
			}
			if ((float)number_format(($check_info[7] - $previous_sum), $decimal_places, ".", "") > 0) {
				$save_gift_certificate_payment = lavu_query("INSERT INTO `cc_transactions` (`order_id`, `check`, `amount`, `loc_id`, `datetime`, `pay_type`, `register`, `change`, `total_collected`, `server_name`, `server_id`, `action`) VALUES ('".$check_info[0]."', '".$check_info[9]."', '".number_format(($check_info[7] - $previous_sum), $decimal_places, ".", "")."', '".$_REQUEST['loc_id']."', $record_datetime, 'Gift Certificate', '".(isset($check_info[20])?$check_info[20]:"")."', '".number_format(0, $decimal_places, ".", "")."', '".number_format(($check_info[7] - $previous_sum), $decimal_places, ".", "")."', '".(isset($check_info[24])?$check_info[24]:"")."', '".(isset($check_info[25])?$check_info[25]:"")."', 'Sale')");
			}
		}
		if ((float)$check_info[15] > 0) { // record cash refund
			$check_previous = lavu_query("SELECT SUM(`amount`) as `refund_sum` FROM `cc_transactions` WHERE `order_id` = '".$check_info[0]."' AND `check` = '".$check_info[9]."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Cash' AND `action` = 'Refund'");
			$previous_sum = 0;
			if (@mysqli_num_rows($check_previous) > 0) {
				$extract = mysqli_fetch_assoc($check_previous);
				$previous_sum = $extract['refund_sum'];
			}
			if ((float)number_format(($check_info[15] - $previous_sum), $decimal_places, ".", "") > 0) {
				$save_cash_refund = lavu_query("INSERT INTO `cc_transactions` (`order_id`, `check`, `amount`, `loc_id`, `datetime`, `pay_type`, `register`, `change`, `total_collected`, `server_name`, `server_id`, `action`) VALUES ('".$check_info[0]."', '".$check_info[9]."', '".number_format(($check_info[15] - $previous_sum), $decimal_places, ".", "")."', '".$_REQUEST['loc_id']."', $record_datetime, 'Cash', '".(isset($check_info[20])?$check_info[20]:"")."', '".number_format(0, $decimal_places, ".", "")."', '".number_format(0, $decimal_places, ".", "")."', '".(isset($check_info[24])?$check_info[24]:"")."', '".(isset($check_info[25])?$check_info[25]:"")."', 'Refund')");
			}
		}
		if (((float)$check_info[23] > 0) && ($location_info['integrateCC'] != "1")) { // record card refund
			$check_previous = lavu_query("SELECT SUM(`amount`) as `refund_cc_sum` FROM `cc_transactions` WHERE `order_id` = '".$check_info[0]."' AND `check` = '".$check_info[9]."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Card' AND `action` = 'Refund'");
			$previous_sum = 0;
			if (@mysqli_num_rows($check_previous) > 0) {
				$extract = mysqli_fetch_assoc($check_previous);
				$previous_sum = $extract['refund_cc_sum'];
			}
			if ((float)number_format(($check_info[23] - $previous_sum), $decimal_places, ".", "") > 0) {
				$save_card_refund = lavu_query("INSERT INTO `cc_transactions` (`order_id`, `check`, `amount`, `loc_id`, `datetime`, `pay_type`, `register`, `change`, `total_collected`, `server_name`, `server_id`, `action`) VALUES ('".$check_info[0]."', '".$check_info[9]."', '".number_format(($check_info[23] - $previous_sum), $decimal_places, ".", "")."', '".$_REQUEST['loc_id']."', $record_datetime, 'Gift Certificate', '".(isset($check_info[20])?$check_info[20]:"")."', '".number_format(0, $decimal_places, ".", "")."', '".number_format(0, $decimal_places, ".", "")."', '".(isset($check_info[24])?$check_info[24]:"")."', '".(isset($check_info[25])?$check_info[25]:"")."', 'Refund')");
			}
		}

		if (isset($_REQUEST['item_details'])) {
			$item_details_array = explode("|*|", $_REQUEST['item_details']);
			$get_order_contents = lavu_query("SELECT * FROM `order_contents` WHERE `order_id` = '".$check_info[0]."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `item` != 'SENDPOINT' ORDER BY `id` ASC");
			if (@mysqli_num_rows($get_order_contents) > 0) {
				$row = 0;
				while ($extract = mysqli_fetch_assoc($get_order_contents)) {
					$details = explode(":", $item_details_array[$row]);
					$update_item = lavu_query("UPDATE `order_contents` SET `discount_amount` = '".$details[0]."', `discount_value` = '".$details[1]."', `discount_type` = '".$details[2]."', `after_discount` = '".$details[3]."', `tax_amount` = '".$details[4]."' WHERE `id` = '".$extract['id']."'");
					$row++;
				}
			}
		}

		if ($check_info[8] == 1) {

			$record_card_paid = "";
			if ($location_info['integrateCC'] != "1") {
				$record_card_paid = ", `card_paid` = '".$check_info[6]."'";
			}

			$update_order = lavu_query("UPDATE `orders` SET `refunded` = '".(isset($check_info[15])?$check_info[15]:number_format(0, $decimal_places, ".", ""))."', `refunded_cc` = '".(isset($check_info[23])?$check_info[23]:number_format(0, $decimal_places, ".", ""))."', `discount` = '".$check_info[1]."', `discount_sh` = '".$check_info[2]."', `discount_value` = '".(isset($check_info[18])?$check_info[18]:"0")."', `discount_type` = '".(isset($check_info[21])?$check_info[21]:"0")."', `gratuity` = '".$check_info[3]."', `cash_paid` = '".$check_info[5]."', `cash_applied` = '".($check_info[5] - $check_info[17])."'".$record_card_paid.", `gift_certificate` = '".$check_info[7]."', `guests` = '".$check_info[4]."', `check_has_printed` = '1' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$check_info[0]."'");

			echo '{"json_status":"success"}';
			exit;

		} else {

			$q_update = "";
			$q_fields = "";
			$q_values = "";
			$detail_vars = array();
			$detail_vars['discount'] = $check_info[1];
			$detail_vars['discount_sh'] = $check_info[2];
			$detail_vars['discount_value'] = $check_info[18];
			$detail_vars['discount_type'] = $check_info[21];
			$detail_vars['gratuity'] = $check_info[3];
			$detail_vars['gratuity_percent'] = $check_info[19];
			$detail_vars['cash'] = $check_info[5];
			if ($location_info['integrateCC'] != "1") {
				$detail_vars['card'] = $check_info[6];
			}
			$detail_vars['gift_certificate'] = $check_info[7];
			$detail_vars['card_desc'] = $check_info[10];
			$detail_vars['transaction_id'] = $check_info[10];
			$detail_vars['subtotal'] = $check_info[12];
			$detail_vars['tax'] = $check_info[13];
			$detail_vars['check_total'] = $check_info[14];
			$detail_vars['refund'] = $check_info[15];
			$detail_vars['refund_cc'] = $check_info[23];
			$detail_vars['remaining'] = $check_info[16];
			$detail_vars['change'] = $check_info[17];
			$detail_vars['cash_applied'] = ($check_info[5] - $check_info[17]);
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$detail_vars['loc_id'] = $_REQUEST['loc_id'];
			$detail_vars['order_id'] = $check_info[0];
			$detail_vars['check'] = $check_info[9];
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}

			$check_for_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' LIMIT 1", $detail_vars);
			if (@mysqli_num_rows($check_for_details) > 0) {
				$update_details = lavu_query("UPDATE `split_check_details` SET $q_update WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $detail_vars);
			} else {
				$create_details = lavu_query("INSERT INTO `split_check_details` ($q_fields) VALUES ($q_values)", $detail_vars);
			}

			$discount = 0;
			$gratuity = 0;
			$cash = 0;
			$card = 0;
			$gift_certificate = 0;
			$refund = 0;
			$refund_cc = 0;
			$change = 0;
			$cash_applied = 0;

			$get_detail_totals = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` <= '[check]'", $detail_vars);
			if (@mysqli_num_rows($get_detail_totals) > 0) {
				while ($extract = mysqli_fetch_array($get_detail_totals)) {
					$discount += $extract['discount'];
					$gratuity += $extract['gratuity'];
					$cash += $extract['cash'];
					$card += $extract['card'];
					$gift_certificate += $extract['gift_certificate'];
					$refund += $extract['refund'];
					$refund_cc += $extract['refund_cc'];
					$change += $extract['change'];
					$cash_applied += $extract['cash_applied'];
				}
			}

			$q_update = "";
			$order_vars = array();
			$order_vars['discount'] = number_format($discount, $decimal_places, '.', '');
			$order_vars['gratuity'] = number_format($gratuity, $decimal_places, '.', '');
			$order_vars['cash_paid'] = number_format($cash, $decimal_places, '.', '');
			if ($location_info['integrateCC'] != "1") {
				$order_vars['card_paid'] = number_format($card, $decimal_places, '.', '');
			}
			$order_vars['gift_certificate'] = number_format($gift_certficate, $decimal_places, '.', '');
			$order_vars['refunded'] = number_format($refund, $decimal_places, '.', '');
			$order_vars['refunded_cc'] = number_format($refund_cc, $decimal_places, '.', '');
			$order_vars['change_amount'] = number_format($cash, $decimal_places, '.', '');
			$order_vars['cash_applied'] = number_format($cash_applied, $decimal_places, '.', '');
			$order_vars['guests'] = $check_info[4];
			$keys = array_keys($order_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$order_vars['location_id'] = $_REQUEST['loc_id'];
			$order_vars['order_id'] = $check_info[0];

			$update_order = lavu_query("UPDATE `orders` SET $q_update, `check_has_printed` = '1' WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $order_vars);

			echo '{"json_status":"success"}';
			exit;

		}

	} else if ($mode == 16) { // update number of guests for order

		$gratuity = "";
		if ($_REQUEST['gratuity'] != "DONOTUPDATE") {
			$gratuity = ", gratuity = '".$_REQUEST['gratuity']."'";
		}

		$update_order = lavu_query("UPDATE `orders` SET `guests` = '".$_REQUEST['guests']."'".$gratuity." WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 17) { // save order contents by POST method, may need to keep around for 'adding to tab' functionality

		// ***** order_item indexes *****
		//
		//  0 - item
		//  1 - price
		//  2 - quantity
		//  3 - options
		//  4 - special
		//  5 - print
		//  6 - modify price
		//  7 - check number
		//  8 - seat number
		//  9 - item id
		// 10 - printer
		// 11 - apply taxrate
		// 12 - custom taxrate
		// 13 - modifier list id
		// 14 - forced modifier group id
		// 15 - forced modifiers price
		// 16 - course number
		// 17 - open item
		// 18 - allow deposit
		// 19 - deposit info

		if (!isset($_POST['do_not_clear'])) {
			$clear_current_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '".$_POST['loc_id']."' AND `order_id` = '".$_POST['order_id']."'");
		}

		$order_contents = explode("|*|", $_POST['order_contents']);

		foreach ($order_contents as $oc) {
			$order_item = explode("|o|", $oc);
			$save_order_item = lavu_query("INSERT INTO `order_contents` (`loc_id`, `order_id`, `item`, `price`, `quantity`, `options`, `special`, `modify_price`, `print`, `check`, `seat`, `item_id`, `printer`, `apply_taxrate`, `custom_taxrate`, `modifier_list_id`, `forced_modifier_group_id`, `forced_modifiers_price`, `course`, `open_item`, `subtotal`, `allow_deposit`, `deposit_info`) VALUES ('".$_POST['loc_id']."', '".$_POST['order_id']."', '".$order_item[0]."', '".$order_item[1]."', '".$order_item[2]."', '".$order_item[3]."', '".$order_item[4]."', '".$order_item[6]."', '".$order_item[5]."', '".$order_item[7]."', '".(isset($order_item[8])?$order_item[8]:"1")."', '".(isset($order_item[9])?$order_item[9]:"0")."', '".(isset($order_item[10])?$order_item[10]:"0")."', '".(isset($order_item[11])?$order_item[11]:"")."', '".(isset($order_item[12])?$order_item[12]:"0")."', '".(isset($order_item[13])?$order_item[13]:"0")."', '".(isset($order_item[14])?$order_item[14]:"0")."', '".(isset($order_item[15])?$order_item[15]:"0")."', '".(isset($order_item[16])?$order_item[16]:"0")."', '".(isset($order_item[17])?$order_item[17]:"0")."', '".($order_item[1] * $order_item[2])."', '".(isset($order_item[18])?$order_item[18]:"0")."', '".(isset($order_item[19])?$order_item[19]:"")."')");
		}

		echo "1";
		exit;

	} else if ($mode == 18) { // clock server in - deprecated - not in use by app - noted 2/22/2011

		$insert_punch = lavu_query("INSERT INTO `clock_punches` (`location`, `location_id`, `punch_type`, `server`, `server_id`, `time`, `hours`, `punched_out`) VALUES ('".getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title")."', '".$_REQUEST['loc_id']."', 'Clocked In', '".$_REQUEST['server_name']."', '".$_REQUEST['server_id']."', '".$_REQUEST['time']."', '0', '0')");
		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Server Clocked In', '".$_REQUEST['loc_id']."', '0', '".$_REQUEST['time']."', '".$_REQUEST['server_name']."', '".$_REQUEST['server_id']."', now())");

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 19) { // clock server out - deprecated - not in use by app - noted 2/22/2011

		$get_punch_in_time = lavu_query("SELECT * FROM `clock_punches` WHERE `_deleted` != '1' AND `server_id` = '".$_REQUEST['server_id']."' AND `punched_out` = '0' ORDER BY `time` DESC LIMIT 1");
		if (@mysqli_num_rows($get_punch_in_time) > 0) {
			$extract = mysqli_fetch_array($get_punch_in_time);
		} else {
			echo '{"json_status":"noClockIn"}';
			exit;
		}

		$update_punched_out = lavu_query("UPDATE `clock_punches` SET `punched_out` = '1' WHERE `id` = '".$extract['id']."'");

		$clockInArray = explode(" ", $extract['time']);
		$clockInDate = explode("-", $clockInArray[0]);
		$clockInTime = explode(":", $clockInArray[1]);
		$inStamp = mktime($clockInTime[0], $clockInTime[1], $clockInTime[2], $clockInDate[1], $clockInDate[2], $clockInDate[0]);

		$clockOutArray = explode(" ", $_REQUEST['time']);
		$clockOutDate = explode("-", $clockOutArray[0]);
		$clockOutTime = explode(":", $clockOutArray[1]);
		$outStamp = mktime($clockOutTime[0], $clockOutTime[1], $clockOutTime[2], $clockOutDate[1], $clockOutDate[2], $clockOutDate[0]);

		$hours = (($outStamp - $inStamp) / 3600);

		$insert_punch = lavu_query("INSERT INTO `clock_punches` (`location`, `location_id`, `punch_type`, `server`, `server_id`, `time`, `hours`, `punched_out`) VALUES ('".getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title")."', '".$_REQUEST['loc_id']."', 'Clocked Out', '".$_REQUEST['server_name']."', '".$_REQUEST['server_id']."', '".$_REQUEST['time']."', '".number_format($hours, 3)."', '1')");

		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Server Clocked Out', '0', '".$_REQUEST['time']."', '".$_REQUEST['server_name']."', '".$_REQUEST['server_id']."', now())");

		echo '{"json_status":"success","time_in":"'.$clockInArray[1].'","hours":"'.number_format($hours, 3).'"}';
		exit;

	} else if ($mode == 20) { // reopen closed order

		$device_time = (isset($_REQUEST['device_time']))?$_REQUEST['device_time']:date("Y-m-d H:i:s", time());

		$auth_by = convertPIN($_REQUEST['PINused'], "name");
		$auth_by_id = convertPIN($_REQUEST['PINused']);

		$get_original_close_time = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");
		$extract = mysqli_fetch_array($get_original_close_time);

		$update_order = lavu_query("UPDATE `orders` SET `closed` = '0000-00-00 00:00:00', `tablename` = '".$_REQUEST['set_tablename']."', `reopened_datetime` = '".$device_time."' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");

		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Reopened (Original close: ".$extract['closed'].")', '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', now(), '".$auth_by."', '".$auth_by_id."', now())");

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 21) { // open cash drawer and log it, unless it is called from checkout with no receipt or when the cash button is pressed

		if (!isset($_REQUEST['do_not_log'])) {

			$auth_by = convertPIN($_REQUEST['PINused'], "name");
			$auth_by_id = convertPIN($_REQUEST['PINused']);

			$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Drawer Opened', '0', now(), '".$auth_by."', '".$auth_by_id."', now())");
		}

		$use_direct_printing = (isset($_REQUEST['use_direct_printing']))?$_REQUEST['use_direct_printing']:"0";

		if ($use_direct_printing != "1") {
			$dd = "";
			if ($_REQUEST['dev_dir'] != "") {
				$dd = "/..";
			}
			$hloc = "..".$dd."/print/index.php?location=".str_replace(" ", "%20", $_REQUEST['location'])."&print=".urlencode("drawer#".$_REQUEST['register'].":open");
			//mail("corey@meyerwind.com","print",$hloc,"From:corey@poslavu.com");
			header("Location: $hloc");
			//header("Location: ..".$dd."/print/index.php?location=".str_replace(" ", "%20", $_REQUEST['location'])."&print=drawer:open");
		}
		exit;

	} else if ($mode == 22) { // log change to order after check has printed

		$change_info = explode("|*|", $_REQUEST['change_info']);
		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('".$change_info[0]."', '".$_REQUEST['loc_id']."', '".$change_info[1]."', '".$change_info[2]."', '".$change_info[3]."', '".$change_info[4]."', now())");

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo '"json_status":"success"';
		exit;

	} else if ($mode == 23) { // retrieve multiple check details

		$check_details_array = array();

		$get_check_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' ORDER BY `check` ASC");
		if (@mysqli_num_rows($get_check_details) > 0) {
			while ($data_obj = mysqli_fetch_object($get_check_details)) {
				$check_details_array[] = $data_obj;
			}

		} else {
			echo '{"json_status":"nr"}';
			exit;
		}

		echo '{"json_status":"success","check_details":'.lavu_json_encode($check_details_array).'}';
		exit;

	} else if ($mode == 24) { // save multiple check details during close

		$check_details_array = explode("|*|", $_REQUEST['check_info']);

		$subtotal_array = explode("|o|", $check_details_array[0]);
		$discount_array = explode("|o|", $check_details_array[1]);
		$discount_sh_array = explode("|o|", $check_details_array[2]);
		$tax_array = explode("|o|", $check_details_array[3]);
		$gratuity_array = explode("|o|", $check_details_array[4]);
		$cash_array = explode("|o|", $check_details_array[5]);
		$card_array = explode("|o|", $check_details_array[6]);
		$gift_certificate_array = explode("|o|", $check_details_array[7]);
		$refund_array = explode("|o|", $check_details_array[8]);
		$remaining_array = explode("|o|", $check_details_array[9]);
		$total_array = explode("|o|", $check_details_array[10]);
		$change_array = explode("|o|", $check_details_array[11]);
		$card_desc_array = explode("|o|", $check_details_array[12]);
		$transaction_id_array = explode("|o|", $check_details_array[13]);
		$discount_values_array = array();
		if (isset($check_details_array[14])) {
			$discount_values_array = explode("|o|", $check_details_array[14]);
		}
		$discount_types_array = array();
		if (isset($check_details_array[16])) {
			$discount_types_array = explode("|o|", $check_details_array[16]);
		}
		$refund_cc_array = array();
		if (isset($check_details_array[17])) {
			$refund_cc_array = explode("|o|", $check_details_array[17]);
		}

		for ($i = 0; $i < count($subtotal_array); $i++) {

			$check_for_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' AND `check` = '".($i + 1)."'");
			if (@mysqli_num_rows($check_for_details) > 0) {

				$update_details = lavu_query("UPDATE `split_check_details` SET `subtotal` = '".$subtotal_array[$i]."', `discount` = '".$discount_array[$i]."', `discount_sh` = '".$discount_sh_array[$i]."', `discount_value` = '".(isset($discount_values_array[$i])?$discount_values_array[$i]:"0")."', `discount_type` = '".(isset($discount_types_array[$i])?$discount_types_array[$i]:"0")."', `tax` = '".$tax_array[$i]."', `gratuity` = '".$gratuity_array[$i]."', `gratuity_percent` = '".(isset($check_details_array[15])?$check_details_array[15]:"0")."', `cash` = '".$cash_array[$i]."', `card` = '".$card_array[$i]."', `gift_certificate` = '".$gift_certificate_array[$i]."', `refund` = '".$refund_array[$i]."', `refund_cc` = '".$refund_cc_array[$i]."', `remaining` = '".$remaining_array[$i]."', `check_total` = '".$total_array[$i]."', `change` = '".$change_array[$i]."', `card_desc` = '".$card_desc_array[$i]."', `transaction_id` = '".$transaction_id_array[$i]."' WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' AND `check` = '".($i + 1)."'");

			} else {

				$create_details = lavu_query("INSERT INTO `split_check_details` (`loc_id`, `order_id`, `check`, `subtotal`, `discount`, `discount_sh`, `tax`, `gratuity`, `cash`, `card`, `gift_certificate`, `refund`, `refund_cc`, `remaining`, `check_total`, `change`, `card_desc`, `transaction_id`, `discount_value`, `gratuity_percent`, `discount_type`) VALUES ('".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".($i + 1)."', '".$subtotal_array[$i]."', '".$discount_array[$i]."', '".$discount_sh_array[$i]."', '".$tax_array[$i]."', '".$gratuity_array[$i]."', '".$cash_array[$i]."', '".$card_array[$i]."', '".$gift_certificate_array[$i]."', '".$refund_array[$i]."', '".$refund_cc_array[$i]."', '".$remaining_array[$i]."', '".$total_array[$i]."', '".$change_array[$i]."', '".$card_desc_array[$i]."', '".$transaction_id_array[$i]."', '".(isset($discount_values_array[$i])?$discount_values_array[$i]:"0")."', '".(isset($check_details_array[15])?$check_details_array[15]:"0")."', '".(isset($discount_types_array[$i])?$discount_types_array[$i]:"0")."')");
			}
		}

		$clean_up_extra_details = lavu_query("DELETE FROM `split_check_details` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' AND `check` > '".count($subtotal_array)."'");

		echo '{"json_status":"success"}';
		exit;

	} else if ($mode == 25) { // save order in quick serve mode, returns order id (this additional mode may prove unnecessary) ***** is not currently being called by app RJG 11-01-2010 - kept alive for older versions of app ********* 

		// ***** order_ifo indexes *****
		//
		//  0 - opened
		//  1 - closed
		//  2 - subtotal
		//  3 - taxrate
		//  4 - tax
		//  5 - total
		//  6 - server
		//  7 - tablename
		//  8 - send_status
		//  9 - guests
		// 10 - server id
		// 11 - number of checks
		// 12 - discount amount
		// 13 - discount shorthand
		// 14 - gratuity amount
		// 15 - cash amount
		// 16 - card amount
		// 17 - gift certificate amount
		// 18 - change amount
		// 19 - card description
		// 20 - transaction ID
		// 21 - gratuity percent
		// 22 - PIN used

		$order_info = explode("|*|", $_REQUEST['order_info']);

		$auth_by = convertPIN($order_info[22], "name");
		$auth_by_id = convertPIN($order_info[22]);

		$save_new_order = lavu_query("INSERT INTO `orders` (`order_id`, `location`, `location_id`, `opened`, `closed`, `subtotal`, `taxrate`, `tax`, `total`, `server`, `server_id`, `tablename`, `send_status`, `discount`, `discount_sh`, `gratuity`, `gratuity_percent`, `card_gratuity`, `cash_paid`, `card_paid`, `gift_certificate`, `change_amount`, `void`, `cashier`, `cashier_id`, `auth_by`, `auth_by_id`, `guests`, `email`, `permission`, `check_has_printed`, `no_of_checks`, `card_desc`, `transaction_id`, `cash_tip`) VALUES ('".assignNext("order_id", "orders", "location_id", $_REQUEST['loc_id'])."', '".getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title")."', '".$_REQUEST['loc_id']."', '".$order_info[0]."', '".$order_info[1]."', '".$order_info[2]."', '".$order_info[3]."', '".$order_info[4]."', '".$order_info[5]."', '".$order_info[6]."', '".$order_info[10]."', '".$order_info[7]."', '".$order_info[8]."', '".$order_info[12]."', '".$order_info[13]."', '".$order_info[14]."', '".$order_info[21]."', '0.00', '".$$order_info[15]."', '".$order_info[16]."', '".$order_info[17]."', '".$order_info[18]."', '0', '".$order_info[6]."', '".$order_info[10]."', '".$auth_by."', '".$auth_by_id."', '".$order_info[9]."', '', '0', '0', '".$order_info[11]."', '".$order_info[19]."', '".$order_info[20]."', '0.00')");

		$order_id = ConnectionHub::getConn("rest")->InsertID();

		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Saved', '".$_REQUEST['loc_id']."', '".$order_id."', '".$order_info[0]."', '".$order_info[6]."', '".$order_info[10]."', now())");

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo '{"json_status":"success","order_id":"'.$order_id.'"}';
		exit();

	} else if ($mode == 26) { // reload settings

		$user_data_array = array();

		//mlavu_select_db("poslavu_MAIN_db");
		$get_restaurant_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `company_code` = '[1]'", $_REQUEST['cc']);
		if (mysqli_num_rows($get_restaurant_data) > 0) {
			$extract_r = mysqli_fetch_array($get_restaurant_data);

			//lavu_select_db("poslavu_".$company_info['data_name']."_db");

			$get_user_info = lavu_query("SELECT * FROM `users` WHERE `id` = '".$_REQUEST['server_id']."'");
			if (@mysqli_num_rows($get_user_info) > 0) {
				$extract_u = mysqli_fetch_array($get_user_info);
			} else {
				echo '{"json_status":"NoU"}';
				exit;
			}

			$valid_PINs = "";
			$get_valid_PINs = lavu_query("SELECT `PIN` FROM `users` WHERE `access_level` > '0'");
			while ($extract2 = mysqli_fetch_array($get_valid_PINs)) {
				$valid_PINs = $valid_PINs."|".$extract2['PIN']."|";
			}

			$valid_admin_PINs = "";
			$get_valid_admin_PINs = lavu_query("SELECT `PIN` FROM `users` WHERE `access_level` > '1'");
			while ($extract2 = mysqli_fetch_array($get_valid_admin_PINs)) {
				$valid_admin_PINs = $valid_admin_PINs."|".$extract2['PIN']."|";
			}

			$get_all_user_data = lavu_query("SELECT * FROM `users` ORDER BY `l_name`, `f_name` ASC");
			while ($data_obj = mysqli_fetch_object($get_all_user_data)) {
				$user_data_array[] = $data_obj;
			}

			$locations_array = array();
			$get_locations = lavu_query("SELECT * FROM `locations` WHERE `id` = '".$_REQUEST['loc_id']."'");
			if (@mysqli_num_rows($get_locations) > 0) {
				while ($data_obj = mysqli_fetch_object($get_locations)) {
					$locations_array[] = $data_obj;
				}

				$get_menu_id = lavu_query("SELECT `menu_id` FROM `locations` WHERE `id` = '".$_REQUEST['loc_id']."'");
				$extract_m = mysqli_fetch_array($get_menu_id);
				$menu_id = $extract_m['menu_id'];

			} else {

				echo '{"json_status":"NoL"}';
				exit;
			}

			$table_setup_array = array();
			$get_table_setup = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '".$_REQUEST['loc_id']."' LIMIT 1");
			if (@mysqli_num_rows($get_table_setup) > 0) {
				$table_setup_array = mysqli_fetch_object($get_table_setup);
			} else {
				echo '{"json_status":"NoTS"}';
				exit;
			}

			$discount_types_array = array();
			$get_discount_types = lavu_query("SELECT * FROM `discount_types` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `_deleted` != '1' ORDER BY `title` ASC");
			while ($data_obj = mysqli_fetch_object($get_discount_types)) {
				$discount_types_array[] = $data_obj;
			}

			$reports_list_return_string = '"reports_list":[{"title":"Register Summary","mode":"register","filename":"till_report.php"},{"title":"Server Summary","mode":"server","filename":"till_report.php"}]';

			$printers_return_string = getSupportedPrinters($_REQUEST['loc_id']);

			$all_menu_data_return_string = loadMenuData($menu_id, $_REQUEST['loc_id']);

			$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Settings reloaded', '0', '0', now(), '".$extract['f_name']." ".$extract['l_name']."', '".$extract['id']."', now())");

			$update_last_activity = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `data_name` = '[1]'", $company_info['data_name']);

			// load components if necessary

			$component_data_return_string = "";

			$comp_pack = (isset($_REQUEST['comp_pack']))?$_REQUEST['comp_pack']:"0";

			if ($comp_pack != "0") {
				$component_data_return_string = loadComponents($comp_pack);
			}

			if (isset($_REQUEST['UDID'])) {
				mlavu_query("UPDATE `poslavu_MAIN_db`.`client_devices` SET `needs_reload` = '0' WHERE `UDID` = '".$_REQUEST['UDID']."'");
			}

			$tack_on_build = "";
			if (isset($_REQUEST['build'])) {
				$tack_on_build = ":".$_REQUEST['build'];
			}

			echo '{"json_status":"success","company_code":"'.lsecurity_name($extract_r['company_code'], $extract_r['id']).$tack_on_build.'","logo_img":"'.$extract_r['logo_img'].'","demo":"'.$extract_r['demo'].'","company_id":"'.$extract_r['id'].'","company_name":"'.$extract_r['company_name'].'","valid_PINs":"'.$valid_PINs.'","valid_admin_PINs":"'.$valid_admin_PINs.'","location":'.lavu_json_encode($locations_array).',"all_user_data":'.lavu_json_encode($user_data_array).',"quick_serve":"'.$extract_u['quick_serve'].'","access_level":"'.$extract_u['access_level'].'","PIN":"'.$extract_u['PIN'].'","f_name":"'.$extract_u['f_name'].'","l_name":"'.$extract_u['l_name'].'","table_setup":'.lavu_json_encode($table_setup_array).',"discount_types":'.lavu_json_encode($discount_types_array).','.$reports_list_return_string.','.$printers_return_string.','.$all_menu_data_return_string.$component_data_return_string.'}';
			exit;

		} else {

			echo '{"json_status":"NoR"}';
			exit;
		}

	} else if ($mode == 27) { // reload location settings only

		$locations_array = array();

		$get_net_paths = lavu_query("SELECT * FROM `locations` WHERE `id` = '".$_REQUEST['loc_id']."' LIMIT 1");
		if (@mysqli_num_rows($get_net_paths) > 0) {
			$extract = mysqli_fetch_array($get_net_paths);

			echo '{"json_status":"success","net_path":"'.$extract['net_path'].'","net_path_print":"'.$extract['net_path_print'].'"}';
			exit;

		} else {

			echo '{"json_status":"NoL"}';
			exit;
		}

	} else if ($mode == 28) { // save order, combines modes 5 and 17, should replace them altogether in the future, returns order id

		// ***** order_ifo indexes *****
		//
		//  0 - order id
		//  1 - opened
		//  2 - closed
		//  3 - subtotal
		//  4 - taxrate
		//  5 - tax
		//  6 - total
		//  7 - server
		//  8 - tablename
		//  9 - send_status
		// 10 - guests
		// 11 - server id
		// 12 - number of checks
		// 13 - multiple tax rates
		// 14 - to tab or not to tab
		// 15 - deposit status
		// 16 - register

		//ini_set('display_errors',1);
		//error_reporting(E_ALL|E_STRICT);

		$order_info = explode("|*|", $_POST['order_info']);
		$order_id = "0";

		if ($order_info[0] == "0") {

			$check_for_previous_save = lavu_query("SELECT `order_id` FROM `orders` WHERE `location_id` = '".$_POST['loc_id']."' AND `opened` = '".$order_info[1]."' AND `total` = '".$order_info[6]."' AND `server` = '".$order_info[7]."' AND `tablename` = '".$order_info[8]."' LIMIT 1");
			if (mysqli_num_rows($check_for_previous_save) > 0) {
				$extract = mysqli_fetch_array($check_for_previous_save);
				$order_id = $extract['order_id'];
				$update_order = lavu_query("UPDATE `orders` SET `void` = '0', `opened` = '".$order_info[1]."', `server` = '".$order_info[7]."', `server_id` = '".$order_info[11]."', `tablename` = '".$order_info[8]."', `subtotal` = '".$order_info[3]."', `tax` = '".$order_info[5]."', `total` = '".$order_info[6]."', `send_status` = '".$order_info[9]."', `guests` = '".$order_info[10]."', `no_of_checks` = '".$order_info[12]."' WHERE `location_id` = '".$_POST['loc_id']."' AND `order_id` = '".$order_id."'");
			} else {
				$check_for_zeroed_open = lavu_query("SELECT * FROM `orders` WHERE `opened` = '0000-00-00 00:00:00' AND `location_id` = '[1]' ORDER BY `id` ASC LIMIT 1", $_REQUEST['loc_id']);
				if (@mysqli_num_rows($check_for_zeroed_open) > 0) {
					$extract = mysqli_fetch_array($check_for_zeroed_open);
					$order_id = $extract['order_id'];
					$update_order = lavu_query("UPDATE `orders` SET `void` = '0', `opened` = '".$order_info[1]."', `server` = '".$order_info[7]."', `server_id` = '".$order_info[11]."', `tablename` = '".$order_info[8]."', `subtotal` = '".$order_info[3]."', `tax` = '".$order_info[5]."', `total` = '".$order_info[6]."', `send_status` = '".$order_info[9]."', `guests` = '".$order_info[10]."', `no_of_checks` = '".$order_info[12]."', `deposit_status` = '".(isset($order_info[15])?$order_info[15]:"0")."' WHERE `location_id` = '".$_POST['loc_id']."' AND `order_id` = '".$order_id."'");
				} else {
					$order_id = assignNext("order_id", "orders", "location_id", $_POST['loc_id']);
					$save_new_order = lavu_query("INSERT INTO `orders` (`order_id`, `location`, `location_id`, `opened`, `closed`, `subtotal`, `taxrate`, `tax`, `total`, `server`, `server_id`, `tablename`, `send_status`, `discount`, `discount_sh`, `gratuity`, `gratuity_percent`, `card_gratuity`, `cash_paid`, `card_paid`, `gift_certificate`, `change_amount`, `void`, `cashier`, `cashier_id`, `auth_by`, `auth_by_id`, `guests`, `email`, `permission`, `check_has_printed`, `no_of_checks`, `card_desc`, `transaction_id`, `multiple_tax_rates`, `tab`, `deposit_status`, `cash_tip`, `register`) VALUES ('".$order_id."', '".str_replace("'", "''", getFieldInfo($_POST['loc_id'], "id", "locations", "title"))."', '".$_POST['loc_id']."', '".$order_info[1]."', '".$order_info[2]."', '".$order_info[3]."', '".$order_info[4]."', '".$order_info[5]."', '".$order_info[6]."', '".$order_info[7]."', '".$order_info[11]."', '".$order_info[8]."', '".$order_info[9]."', '0.00', '', '0.00', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0', '', '0', '', '0', '".$order_info[10]."', '', '0', '0', '".$order_info[12]."', '', '', '".(isset($order_info[13])?$order_info[13]:"0")."', '".(isset($order_info[14])?$order_info[14]:"0")."', '".(isset($order_info[15])?$order_info[15]:"0")."', '0.00', '".(isset($order_info[16])?$order_info[16]:"")."')");
				}
			}

		} else {

			$update_order = lavu_query("UPDATE `orders` SET `void` = '0', `opened` = '".$order_info[1]."', `server` = '".$order_info[7]."', `server_id` = '".$order_info[11]."', `tablename` = '".$order_info[8]."', `subtotal` = '".$order_info[3]."', `tax` = '".$order_info[5]."', `total` = '".$order_info[6]."', `send_status` = '".$order_info[9]."', `guests` = '".$order_info[10]."', `no_of_checks` = '".$order_info[12]."', `tab` = '".$order_info[14]."', `deposit_status` = '".(isset($order_info[15])?$order_info[15]:"0")."' WHERE `location_id` = '".$_POST['loc_id']."' AND `order_id` = '".$order_info[0]."'");

			$order_id = $order_info[0];
		}

		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('Order Saved', '".$_POST['loc_id']."', '".$order_id."', '".$order_info[1]."', '".$order_info[7]."', '".$order_info[11]."', now())");

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		$update_last_activity = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `data_name` = '[1]'", $company_info['data_name']);

		// ***** order_item indexes *****
		//
		//  0 - item
		//  1 - price
		//  2 - quantity
		//  3 - options
		//  4 - special
		//  5 - print
		//  6 - modify price
		//  7 - check number
		//  8 - seat number
		//  9 - item id
		// 10 - printer
		// 11 - apply taxrate
		// 12 - custom taxrate
		// 13 - modifier list id
		// 14 - forced modifier group id
		// 15 - forced modifiers price
		// 16 - course number
		// 17 - open item
		// 18 - allow deposit
		// 19 - deposit info

		$clear_current_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '".$_POST['loc_id']."' AND `order_id` = '".$order_id."'");

		$order_contents = explode("|*|", $_POST['order_contents']);

		foreach ($order_contents as $oc) {

			$order_item = explode("|o|", $oc);
			$save_order_item = lavu_query("INSERT INTO `order_contents` (`loc_id`, `order_id`, `item`, `price`, `quantity`, `options`, `special`, `modify_price`, `print`, `check`, `seat`, `item_id`, `printer`, `apply_taxrate`, `custom_taxrate`, `modifier_list_id`, `forced_modifier_group_id`, `forced_modifiers_price`, `course`, `open_item`, `subtotal`, `allow_deposit`, `deposit_info`, `subtotal_with_mods`) VALUES ('".$_POST['loc_id']."', '".$order_id."', '".str_replace("'", "''", $order_item[0])."', '".$order_item[1]."', '".$order_item[2]."', '".str_replace("'", "''", $order_item[3])."', '".str_replace("'", "''", $order_item[4])."', '".$order_item[6]."', '".$order_item[5]."', '".$order_item[7]."', '".(isset($order_item[8])?$order_item[8]:"1")."', '".(isset($order_item[9])?$order_item[9]:"0")."', '".(isset($order_item[10])?$order_item[10]:"0")."', '".(isset($order_item[11])?$order_item[11]:"")."', '".(isset($order_item[12])?$order_item[12]:"0")."', '".(isset($order_item[13])?$order_item[13]:"0")."', '".(isset($order_item[14])?$order_item[14]:"0")."', '".(isset($order_item[15])?$order_item[15]:"0")."', '".(isset($order_item[16])?$order_item[16]:"0")."', '".(isset($order_item[17])?$order_item[17]:"0")."', '".($order_item[1] * $order_item[2])."', '".(isset($order_item[18])?$order_item[18]:"0")."', '".(isset($order_item[19])?str_replace("'", "''", $order_item[19]):"")."', '".($order_item[2] * ($order_item[1] + $order_item[6] + $order_item[15]))."')");
		}

		echo "1:".$order_id;
		exit;

	} else if ($mode == 29) { // quick query to create a blank order and return the order_id

		$new_id = assignNext("order_id", "orders", "location_id", $_REQUEST['loc_id']);
		$create_order = lavu_query("INSERT INTO `orders` (`order_id`, `location`, `location_id`, `register`) VALUES ('".$new_id."', '".getFieldInfo($_REQUEST['loc_id'], "id", "locations", "title")."', '".$_REQUEST['loc_id']."', '".(isset($_REQUEST['register'])?$_REQUEST['register']:"")."')");
		echo $new_id;
		exit;

	} else if ($mode == 30) { // load orders with open deposits

		$order_info = array();
		$order_contents = array();
		$check_details = array();

		$get_orders = lavu_query("SELECT * FROM `orders` WHERE `location_id` ='".$_REQUEST['loc_id']."' AND `deposit_status` = '2' AND `closed` = '0000-00-00 00:00:00' AND `void` != '1' ORDER BY `opened` ASC");
		if (@mysqli_num_rows($get_orders) > 0) {
			while ($data_obj = mysqli_fetch_object($get_orders)) {
				$order_info[] = $data_obj;
				$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."' ORDER BY `id` ASC");
				while ($data_obj2 = mysqli_fetch_object($get_contents)) {
					$order_contents[] = $data_obj2;
				}
				$get_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$data_obj->order_id."'");
				while ($data_obj3 = mysqli_fetch_object($get_details)) {
					$check_details[] = $data_obj3;
				}
			}

		} else {
			echo '{"json_status":"NoMatch"}';
			exit;
		}

		echo '{"json_status":"success","order_info":'.lavu_json_encode($order_info).',"order_contents":'.lavu_json_encode($order_contents).',"check_details":'.lavu_json_encode($check_details).'}';
		exit;

	} else if ($mode == 31) { // close open deposit as unpaid

		$get_items = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' AND `allow_deposit` != '0'");
		if (@mysqli_num_rows($get_items) > 0) {
			while ($extract = mysqli_fetch_array($get_items)) {
				$new_deposit_info = $extract['deposit_info']."\nCLOSED AS UNPAID on ".$_REQUEST['now']." by ".$_REQUEST['server'];
				$update_item = lavu_query("UPDATE `order_contents` SET `deposit_info` = '".$new_deposit_info."' WHERE `id` = '".$extract['id']."'");
			}
			$update_deposit_status = lavu_query("UPDATE `orders` SET `deposit_status` = '5' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");

			echo "1";
			exit;

		} else {

			echo "0";
			exit;
		}

	} else if ($mode == 32) { // load credit card transaction details or recorded payments and refunds

		$submode = (isset($_REQUEST['sm']))?$_REQUEST['sm']:1;

		if ($submode == 1) { // credit card transactions

			$cc_transactions_array = array();

			$get_cc_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' AND `pay_type` = 'Card' AND `got_response` = '1' AND `action` = 'Sale' AND `voided` = '0' ORDER BY `id` ASC");
			if (@mysqli_num_rows($get_cc_transactions) > 0) {
				while ($data_obj = mysqli_fetch_object($get_cc_transactions)) {
					$cc_transactions_array[] = $data_obj;
				}
			}

			echo '{"json_status":"success","cc_transactions":'.lavu_json_encode($cc_transactions_array).'}';
			exit;

		} else { // recorded payments and refunds

			$recorded_payments_array = array();

			//$filter = "";
			//if ($location_info['integrateCC'] == "1") {
			//	$filter = " AND (`pay_type` = 'Cash' OR (`pay_type` = 'Card' AND `action` = 'Refund') OR `pay_type` = 'Gift Certificate')";
			//}

			$get_recorded_payments = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'".$filter." AND `voided` = '0' ORDER BY `id` ASC");
			if (@mysqli_num_rows($get_recorded_payments) > 0) {
				while ($data_obj = mysqli_fetch_object($get_recorded_payments)) {
					$recorded_payments_array[] = $data_obj;
				}
			}

			echo '{"json_status":"success","recorded_payments":'.lavu_json_encode($recorded_payments_array).'}';
			exit;
		}

	} else if ($mode == 33) { // save credit card transaction details - deprecated - this now occurs in the gateway functions script

		// deprecated as of 1-11-2011 - function accomplished in gateway_funcitons.php immediately following a successful transaction

/*		$save_cc_transaction = lavu_query("INSERT INTO `cc_transactions` (`order_id`, `check`, `amount`, `card_desc`, `transaction_id`, `refunded`, `processed`, `loc_id`, `auth_code`, `card_type`, `datetime`) VALUES ('".$_REQUEST['order_id']."', '".$_REQUEST['check']."', '".$_REQUEST['amount']."', '".$_REQUEST['card_desc']."', '".$_REQUEST['transaction_id']."', '0', '0', '".$_REQUEST['loc_id']."', '".$_REQUEST['auth_code']."', '".$_REQUEST['card_type']."', now())");
		$update_order = lavu_query("UPDATE `orders` SET `card_paid` = (card_paid + ".$_REQUEST['amount']."), `card_desc` = '".$_REQUEST['card_desc']."', `transaction_id` = '".$_REQUEST['transaction_id']."' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");
		$check_for_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' AND `check` = '".$_REQUEST['check']."'");
		if (@mysqli_num_rows($check_for_details) > 0) {
			$update_details = lavu_query("UPDATE `split_check_details` SET `card` = (card + ".$_REQUEST['amount']."), `card_desc` = '".$_REQUEST['card_desc']."', `transaction_id` = '".$_REQUEST['transaction_id']."' WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."' AND `check` = '".$_REQUEST['check']."'");
		} else {
			$create_details = lavu_query("INSERT INTO `split_check_details` (`loc_id`, `order_id`, `check`, `card`, `card_desc`, `transaction_id`) VALUES ('".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$_REQUEST['check']."', '".$_REQUEST['card']."', '".$_REQUEST['card_desc']."', '".$_REQUEST['transaction_id']."')");
		}*/

		echo "1";
		exit;

	} else if ($mode == 34) { // record refund

		$refund_notes = $_REQUEST['time']." - ".$_REQUEST['type']." refund by ".$_REQUEST['server_name']." for ".number_format($_REQUEST['amount'], $decimal_places, ".", "")." - ".$_REQUEST['reason'];

		$get_info = lavu_query("SELECT * FROM `orders` WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");
		$extract = mysqli_fetch_array($get_info);
		if ($_REQUEST['type'] == "Cash") {
			$original_refund_notes = $extract['refund_notes'];
			$line_break = "";
			if ($original_refund_notes != "") {
				$line_break = "\n";
			}
			$update_order = lavu_query("UPDATE `orders` SET `refunded` = (`refunded` + ".$_REQUEST['amount']."), `refund_notes` = '".$original_refund_notes.$line_break.$refund_notes."', `refunded_by` = '".$_REQUEST['server_id']."' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");
		} else if ($_REQUEST['type'] == "Card") {
			$original_refund_notes = $extract['refund_notes_cc'];
			$line_break = "";
			if ($original_refund_notes != "") {
				$line_break = "\n";
			}
			$update_order = lavu_query("UPDATE `orders` SET `refunded_cc` = (`refunded_cc` + ".$_REQUEST['amount']."), `refund_notes_cc` = '".$original_refund_notes.$line_break.$refund_notes."', `refunded_by_cc` = '".$_REQUEST['server_id']."' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$_REQUEST['order_id']."'");
		}

		if ($_REQUEST['refund_pnref'] != "") {
			$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET `refunded` = '1', `refund_notes` = '".$refund_notes."', `refunded_by` = '".$_REQUEST['server_id']."', `refund_pnref` = '".$_REQUEST['refund_pnref']."' WHERE `transaction_id` = '".$_REQUEST['pnref']."' AND `order_id` = '".$_REQUEST['order_id']."'");
		}

		$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`) VALUES ('".$_REQUEST['type']." refund processed for ".number_format($_REQUEST['amount'], $decimal_places, ".", "")."', '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now())");

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		echo "1";
		exit;

	} else if ($mode == 35) { // retrieve item by UPC, change quantity for qmode = inventoryQuantities

		$get_item = lavu_query("SELECT * FROM `menu_items` WHERE `UPC` = '[1]' LIMIT 1", $_REQUEST['UPC']);
		if (@mysqli_num_rows($get_item) > 0) {

			if ($_REQUEST['smode'] == "order") {

				$data_obj = mysqli_fetch_object($get_item);
				echo '{"json_status":"add_to_order","item_info":'.lavu_json_encode($data_obj).'}';

			} else if ($_REQUEST['smode'] == "inventoryItems") {

				$extract = mysqli_fetch_array($get_item);
				echo '{"json_status":"item_info","item_id":"'.$extract['id'].'","item_name":"'.$extract['name'].'","item_price":"'.$extract['price'].'","description":"'.$extract['description'].'","count":"'.$extract['inv_count'].'"}';

			} else if ($_REQUEST['smode'] == "inventoryQuantities") {

				$extract = mysqli_fetch_array($get_item);

				$quantity_adj = (isset($_REQUEST['q_adj']))?$_REQUEST['q_adj']:"1";
				if ($_REQUEST['qmode'] == "minus") {
					$new_quantity = ($extract['inv_count'] - $quantity_adj);
				} else {
					$new_quantity = ($extract['inv_count'] + $quantity_adj);
				}

				$update_item = lavu_query("UPDATE `menu_items` SET `inv_count` = '[1]', last_modified_date = now() WHERE `id` = '[2]'", $new_quantity, $extract['id']);

				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `time`, `user`, `user_id`, `server_time`, `item_id`) VALUES ('Inventory Adjustment: ".$_REQUEST['qmode']." ".$quantity_adj."', '".$_REQUEST['loc_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now(), '".$extract['id']."')");

				echo '{"json_status":"inventory_change","item_name":"'.$extract['name'].'","old_quantity":"'.$extract['inv_count'].'","new_quantity":"'.$new_quantity.'"}';
			}

		} else {

			if ($_REQUEST['smode'] == "inventoryItems") {
				echo '{"json_status":"item_info","item_id":"0","item_name":"NEW ITEM","item_price":"","description":"","count":"0"}';
			} else {
				echo '{"json_status":"not_found"}';
			}
		}

		exit();

	} else if ($mode == 36) { // clear order and contents, called after adding contents to existing tab

		$clear_order = lavu_query("UPDATE `orders` SET `opened` = '0000-00-00 00:00:00' WHERE `order_id` = '[1]'", $_REQUEST['order_id']);
		$clear_contents = lavu_query("DELETE FROM `order_contents` WHERE `order_id` = '[1]'", $_REQUEST['order_id']);
		echo "1";
		exit();

	} else if ($mode == 37) { // save or create item from barcode scan

		if ($_REQUEST['iid'] == "0") {
			deBug("INSERT INTO `menu_items` (`category_id`, `menu_id`, `name`, `price`, `description`, `inv_count`, `UPC`) VALUES ('112', '11', '".str_replace("'", "''", $_REQUEST['iname'])."', '".number_format((float)$_REQUEST['iprice'], $decimal_places, ".", "")."', '".str_replace("'", "''", $_REQUEST['idesc'])."', '".(int)$_REQUEST['icount']."', '".$_REQUEST['UPC']."')");
			$create_item = lavu_query("INSERT INTO `menu_items` (`category_id`, `menu_id`, `name`, `price`, `description`, `inv_count`, `UPC`) VALUES ('112', '11', '".str_replace("'", "''", $_REQUEST['iname'])."', '".number_format((float)$_REQUEST['iprice'], $decimal_places, ".", "")."', '".str_replace("'", "''", $_REQUEST['idesc'])."', '".(int)$_REQUEST['icount']."', '".$_REQUEST['UPC']."')");
			$new_id = ConnectionHub::getConn("rest")->InsertID();
			echo '{"json_status":"item_created","new_id":"'.$new_id.'"}';
		} else {
			deBug("UPDATE `menu_items` SET `name` = '".str_replace("'", "''", $_REQUEST['iname'])."', `price` = '".number_format((float)$_REQUEST['iprice'], $decimal_places, ".", "")."', `description` = '".str_replace("'", "''", $_REQUEST['idesc'])."', `inv_count` = '".(int)$_REQUEST['icount']."' WHERE `id` = '".$_REQUEST['iid']."'");
			$update_item = lavu_query("UPDATE `menu_items` SET `name` = '".str_replace("'", "''", $_REQUEST['iname'])."', `price` = '".number_format((float)$_REQUEST['iprice'], $decimal_places, ".", "")."', `description` = '".str_replace("'", "''", $_REQUEST['idesc'])."', `inv_count` = '".(int)$_REQUEST['icount']."' WHERE `id` = '".$_REQUEST['iid']."'");
			echo '{"json_status":"item_saved"}';
		}

		exit();

	} else if ($mode == 38) { // nullify needs_reload field

		mlavu_query("UPDATE `poslavu_MAIN_db`.`client_devices` SET `needs_reload` = '0' WHERE `UDID` = '".$_REQUEST['UDID']."'");
		echo "1";
		exit();

	} else if ($mode == 39) { // clear payments and refunds

		$reopened_datetime = "";
		$get_order_info = lavu_query("SELECT * FROM `orders` WHERE `order_id` = '".$_REQUEST['order_id']."' AND `location_id` = '".$_REQUEST['loc_id']."'");
		if (@mysqli_num_rows($get_order_info) > 0) {
			$extract = mysqli_fetch_assoc($get_order_info);
			$reopened_datetime = $extract['reopened_datetime'];
		}

		$previous = (isset($_REQUEST['previous']))?$_REQUEST['previous']:0;
		$dt_filter = "";
		if ($previous == 0) {
			$dt_filter = " AND `datetime` > '$reopened_datetime'";
		}

		$clear_cash = lavu_query("DELETE FROM `cc_transactions` WHERE `order_id` = '".$_REQUEST['order_id']."' AND `check` = '".$_REQUEST['check']."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Cash'$dt_filter");
		if ($location_info['integrateCC'] != "1") {
			$clear_card = lavu_query("DELETE FROM `cc_transactions` WHERE `order_id` = '".$_REQUEST['order_id']."' AND `check` = '".$_REQUEST['check']."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Card'$dt_filter'");
		}
		$clear_gift_certificate = lavu_query("DELETE FROM `cc_transactions` WHERE `order_id` = '".$_REQUEST['order_id']."' AND `check` = '".$_REQUEST['check']."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `pay_type` = 'Gift Certificate'$dt_filter");

		echo "1";
		exit();

	} else if ($mode == 40) { // a new, better way of recording payments and discounts

		// ***** check_info indexes *****
		//
		//  0 - order id
		//  1 - discount amount
		//  2 - discount label
		//  3 - gratuity amount
		//  4 - guests
		//  5 - cash amount
		//  6 - card amount
		//  7 - gift certificate amount
		//  8 - check count
		//  9 - active check
		// 10 - card description (last four)
		// 11 - transaction_id
		// 12 - subtotal
		// 13 - tax amount
		// 14 - check total
		// 15 - refund amount
		// 16 - remaining amount
		// 17 - change amount
		// 18 - discount value
		// 19 - gratuity percent
		// 20 - register
		// 21 - discount type
		// 22 - datetime
		// 23 - refund_cc amount
		// 24 - server name
		// 25 - server id
		// 26 - reopened datetime

		$check_info = explode("|*|", $_REQUEST['check_info']);

		// ***** payments_and_refunds_array indexes *****
		//
		// 0 - order id
		// 1 - check
		// 2 - amount
		// 3 - total collected
		// 4 - pay type
		// 5 - datetime
		// 6 - action
		// 7 - refund notes
		// 8 - server name
		// 9 - server id

		$payments_and_refunds_array = explode("|*|", $_REQUEST['pnr_details']);

		$clear_filter = "";
		if ($location_info['integrateCC'] == "1") {
			$clear_filter = " AND `pay_type` != 'Card'";
		}

		$clear_previous = lavu_query("DELETE FROM `cc_transactions` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$check_info[0]."' AND `check` = '".$check_info[9]."'".$clear_filter);

		$paid_total = 0;
		if ($location_info['integrateCC'] == "1") {
			$get_previous_card_paid = lavu_query("SELECT SUM(`amount`) as `amount` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$check_info[0]."' AND `pay_type` = 'Card' AND `voided` != '1' AND `action` = 'Sale'");
			$extract = mysqli_fetch_assoc($get_previous_card_paid);
			$paid_total = $extract['amount'];

			$get_previous_card_refunds = lavu_query("SELECT SUM(`amount`) as `amount` WHERE `loc_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$check_info[0]."' AND `pay_type` = 'Card' AND `voided` != '1' AND `action` = 'Refund'");
			$extract = mysqli_fetch_assoc($get_previous_card_refunds);
			$paid_total -= $extract['amount'];

		} else {

			foreach ($payments_and_refunds_array as $payment_or_refund) {
				$detail_array = explode("|o|", $payment_or_refund);
				if ($detail_array[4] == "Card") {
					$record_detail = lavu_query("INSERT INTO `cc_transactions` (`loc_id`, `order_id`, `check`, `register`, `amount`, `total_collected`, `pay_type`, `datetime`, `action`, `refund_notes`, `server_name`, `server_id`) VALUES ('".$_REQUEST['loc_id']."', '".$detail_array[0]."', '".$detail_array[1]."', '".$check_info[20]."', '".$detail_array[2]."', '".$detail_array[3]."', '".$detail_array[4]."', '".$detail_array[5]."', '".$detail_array[6]."', '".$detail_array[7]."', '".$detail_array[8]."', '".$detail_array[9]."')");
					if ($detail_array[6] == "Sale") {
						$paid_total += $detail_array[2];
					} else {
						$paid_total -= $detail_array[2];
					}
				}
			}
		}

		foreach ($payments_and_refunds_array as $payment_or_refund) {
			$detail_array = explode("|o|", $payment_or_refund);
			if ($detail_array[4] == "Gift Certificate") {
				$record_detail = lavu_query("INSERT INTO `cc_transactions` (`loc_id`, `order_id`, `check`, `register`, `amount`, `total_collected`, `pay_type`, `datetime`, `action`, `refund_notes`, `server_name`, `server_id`) VALUES ('".$_REQUEST['loc_id']."', '".$detail_array[0]."', '".$detail_array[1]."', '".$check_info[20]."', '".$detail_array[2]."', '".$detail_array[3]."', '".$detail_array[4]."', '".$detail_array[5]."', '".$detail_array[6]."', '".$detail_array[7]."', '".$detail_array[8]."', '".$detail_array[9]."')");
				$paid_total += $detail_array[2];
			}
		}

		foreach ($payments_and_refunds_array as $payment_or_refund) {
			$detail_array = explode("|o|", $payment_or_refund);
			if (($detail_array[4] == "Cash") && ($detail_array[6] == "Refund")) {
				$record_detail = lavu_query("INSERT INTO `cc_transactions` (`loc_id`, `order_id`, `check`, `register`, `amount`, `total_collected`, `pay_type`, `datetime`, `action`, `refund_notes`, `server_name`, `server_id`) VALUES ('".$_REQUEST['loc_id']."', '".$detail_array[0]."', '".$detail_array[1]."', '".$check_info[20]."', '".$detail_array[2]."', '".$detail_array[3]."', '".$detail_array[4]."', '".$detail_array[5]."', '".$detail_array[6]."', '".$detail_array[7]."', '".$detail_array[8]."', '".$detail_array[9]."')");
				$paid_total -= $detail_array[2];
			}
		}

		$last_payment = false;
		foreach ($payments_and_refunds_array as $payment_or_refund) {
			$detail_array = explode("|o|", $payment_or_refund);
			if (($detail_array[4] == "Cash") && ($detail_array[6] == "Sale")) {
				$change = 0;
				$amount_key = 3;
				if (($detail_array[5] >= $check_info[26]) || ($check_info[26] == "")) {
					$amount_key = 2;
				}
				if (($paid_total + $detail_array[$amount_key]) >= $check_info[14]) {
					$change = (($paid_total + $detail_array[$amount_key]) - $check_info[14]);
					$detail_array[3] = ($detail_array[2] - $change);
					$last_payment = true;
				}
				$record_detail = lavu_query("INSERT INTO `cc_transactions` (`loc_id`, `order_id`, `check`, `register`, `amount`, `total_collected`, `change`, `pay_type`, `datetime`, `action`, `refund_notes`, `server_name`, `server_id`) VALUES ('".$_REQUEST['loc_id']."', '".$detail_array[0]."', '".$detail_array[1]."', '".$check_info[20]."', '".$detail_array[2]."', '".$detail_array[3]."', '$change', '".$detail_array[4]."', '".$detail_array[5]."', '".$detail_array[6]."', '".$detail_array[7]."', '".$detail_array[8]."', '".$detail_array[9]."')");
				$paid_total += $detail_array[$amount_key];
				if ($last_payment == true) {
					break;
				}
			}
		}

		$item_details_array = explode("|*|", $_REQUEST['item_details']);
		$get_order_contents = lavu_query("SELECT * FROM `order_contents` WHERE `order_id` = '".$check_info[0]."' AND `loc_id` = '".$_REQUEST['loc_id']."' AND `item` != 'SENDPOINT' ORDER BY `id` ASC");
		if (@mysqli_num_rows($get_order_contents) > 0) {
			$row = 0;
			while ($extract = mysqli_fetch_assoc($get_order_contents)) {
				$details = explode(":", $item_details_array[$row]);
				$update_item = lavu_query("UPDATE `order_contents` SET `discount_amount` = '".$details[0]."', `discount_value` = '".$details[1]."', `discount_type` = '".$details[2]."', `after_discount` = '".$details[3]."', `tax_amount` = '".$details[4]."' WHERE `id` = '".$extract['id']."'");
				$row++;
			}
		}

		if ($check_info[8] == 1) {

			$record_card_paid = "";
			if ($location_info['integrateCC'] != "1") {
				$record_card_paid = ", `card_paid` = '".$check_info[6]."'";
			}

			$update_order = lavu_query("UPDATE `orders` SET `refunded` = '".(isset($check_info[15])?$check_info[15]:number_format(0, $decimal_places, ".", ""))."', `refunded_cc` = '".(isset($check_info[23])?$check_info[23]:number_format(0, $decimal_places, ".", ""))."', `discount` = '".$check_info[1]."', `discount_sh` = '".$check_info[2]."', `discount_value` = '".(isset($check_info[18])?$check_info[18]:"0")."', `discount_type` = '".(isset($check_info[21])?$check_info[21]:"0")."', `gratuity` = '".$check_info[3]."', `cash_paid` = '".$check_info[5]."', `cash_applied` = '".($check_info[5] - $check_info[17])."'".$record_card_paid.", `gift_certificate` = '".$check_info[7]."', `guests` = '".$check_info[4]."', `check_has_printed` = '1' WHERE `location_id` = '".$_REQUEST['loc_id']."' AND `order_id` = '".$check_info[0]."'");

			echo '{"json_status":"success"}';
			exit;

		} else {

			$q_update = "";
			$q_fields = "";
			$q_values = "";
			$detail_vars = array();
			$detail_vars['discount'] = $check_info[1];
			$detail_vars['discount_sh'] = $check_info[2];
			$detail_vars['discount_value'] = $check_info[18];
			$detail_vars['discount_type'] = $check_info[21];
			$detail_vars['gratuity'] = $check_info[3];
			$detail_vars['gratuity_percent'] = $check_info[19];
			$detail_vars['cash'] = $check_info[5];
			if ($location_info['integrateCC'] != "1") {
				$detail_vars['card'] = $check_info[6];
			}
			$detail_vars['gift_certificate'] = $check_info[7];
			$detail_vars['card_desc'] = $check_info[10];
			$detail_vars['transaction_id'] = $check_info[10];
			$detail_vars['subtotal'] = $check_info[12];
			$detail_vars['tax'] = $check_info[13];
			$detail_vars['check_total'] = $check_info[14];
			$detail_vars['refund'] = $check_info[15];
			$detail_vars['refund_cc'] = $check_info[23];
			$detail_vars['remaining'] = $check_info[16];
			$detail_vars['change'] = $check_info[17];
			$detail_vars['cash_applied'] = ($check_info[5] - $check_info[17]);
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$detail_vars['loc_id'] = $_REQUEST['loc_id'];
			$detail_vars['order_id'] = $check_info[0];
			$detail_vars['check'] = $check_info[9];
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}

			$check_for_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' LIMIT 1", $detail_vars);
			if (@mysqli_num_rows($check_for_details) > 0) {
				$update_details = lavu_query("UPDATE `split_check_details` SET $q_update WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $detail_vars);
			} else {
				$create_details = lavu_query("INSERT INTO `split_check_details` ($q_fields) VALUES ($q_values)", $detail_vars);
			}

			$discount = 0;
			$gratuity = 0;
			$cash = 0;
			$card = 0;
			$gift_certificate = 0;
			$refund = 0;
			$refund_cc = 0;
			$change = 0;
			$cash_applied = 0;

			$get_detail_totals = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` <= '[check]'", $detail_vars);
			if (@mysqli_num_rows($get_detail_totals) > 0) {
				while ($extract = mysqli_fetch_array($get_detail_totals)) {
					$discount += $extract['discount'];
					$gratuity += $extract['gratuity'];
					$cash += $extract['cash'];
					$card += $extract['card'];
					$gift_certificate += $extract['gift_certificate'];
					$refund += $extract['refund'];
					$refund_cc += $extract['refund_cc'];
					$change += $extract['change'];
					$cash_applied += $extract['cash_applied'];
				}
			}

			$q_update = "";
			$order_vars = array();
			$order_vars['discount'] = number_format($discount, $decimal_places, '.', '');
			$order_vars['gratuity'] = number_format($gratuity, $decimal_places, '.', '');
			$order_vars['cash_paid'] = number_format($cash, $decimal_places, '.', '');
			if ($location_info['integrateCC'] != "1") {
				$order_vars['card_paid'] = number_format($card, $decimal_places, '.', '');
			}
			$order_vars['gift_certificate'] = number_format($gift_certficate, $decimal_places, '.', '');
			$order_vars['refunded'] = number_format($refund, $decimal_places, '.', '');
			$order_vars['refunded_cc'] = number_format($refund_cc, $decimal_places, '.', '');
			$order_vars['change_amount'] = number_format($cash, $decimal_places, '.', '');
			$order_vars['cash_applied'] = number_format($cash_applied, $decimal_places, '.', '');
			$order_vars['guests'] = $check_info[4];
			$keys = array_keys($order_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$order_vars['location_id'] = $_REQUEST['loc_id'];
			$order_vars['order_id'] = $check_info[0];

			$update_order = lavu_query("UPDATE `orders` SET $q_update, `check_has_printed` = '1' WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $order_vars);

			echo '{"json_status":"success"}';
			exit;
		}

	}

?>
