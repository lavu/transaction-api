<?php

use PaymentGateways\CardConnect\TenantAccount;
use PaymentGateways\CardConnect\Gateway;

require_once(dirname(__FILE__) . '/../constants.php');
require_once(dirname( __FILE__ ).'/../../orm/ConfigLocal.php');
require_once(dirname(__FILE__) . '/../gateway_lib/cardconnect/tenant_account.php');
require_once(dirname(__FILE__) . '/../gateway_lib/cardconnect/gateway.php');
require_once(dirname(__FILE__) . '/../../../inc/papi/jwtPapi.php');

class fmg_placeholder {

	var $id;
	var $title;
	var $include_lists;
	var $_deleted;

	function fmg_placeholder() {

		$this->id = "9999999";
		$this->title = "PLACEHOLDER_GROUP_EMPTY";
		$this->include_lists = "";
		$this->_deleted = 0;
	}
}

function surround_with_single_quote( $item ) {
	return "'{$item}'";
}

function testNewLSVPN($company_code, $company_code_key) {

	$lsvpn = curl_init("https://lsvpn.poslavu.com/hello.php?cc={$company_code}_key_{$company_code_key}");
	curl_setopt($lsvpn, CURLOPT_RETURNTRANSFER, "1");
	curl_setopt($lsvpn, CURLOPT_SSL_VERIFYPEER, false); // comment this line when certificate issues are resolved for poslavu.com
	$lsvpn_response = curl_exec($lsvpn);

	return $lsvpn_response == "Hello";
}

function netPathEmergencyOverride($data_obj) {

	// can change $data_obj->net_path and/or $data_obj->use_net_path

	/*if (($data_obj->net_path == "http://cloud1.poslavu.com") && $data_name!="DEV")
	{
		$data_obj->net_path = "https://admin.poslavu.com";
		$data_obj->use_net_path = "0";
	}*/

	return $data_obj;
}

function priceFormat($value) {

	global $decimal_places;

	return number_format((float)$value, $decimal_places, ".", "");
}

function determineDeviceTime($location_info, $post_vars) {

	$return_time = date("Y-m-d H:i:s");
	$tz_convert = true;

	if (isset($post_vars['device_time'])) {
		$datetime_parts = explode(" ", $post_vars['device_time']);
		$date_parts = explode("-", $datetime_parts[0]);
		$time_parts = explode(":", $datetime_parts[1]);
		$device_ts = mktime((int)$time_parts[0], (int)$time_parts[1], (int)$time_parts[2], (int)$date_parts[1], (int)$date_parts[2], (int)$date_parts[0]);
		/*if (($date_parts[0] - date("Y")) > 500) {
			$tz_convert = false;
			$return_time = $post_vars['device_time'];
		} else*/ if (abs(time() - $device_ts) < 2678400) {
			$tz_convert = false;
			$return_time = date("Y-m-d H:i:s", $device_ts);
		}
	}

	$timezone = (isset($location_info['timezone']))?$location_info['timezone']:"";
	if ($tz_convert && $timezone!="") {
		$return_time = localize_datetime($return_time, $location_info['timezone']);
	}

	return $return_time;
}

function getModifierUnitCost($mod_id, $mod_type) {

	$unit_cost = 0;
	if ($mod_type == "forced") {
		$tablename = "forced_modifiers";
	} else {
		$tablename = "modifiers";
	}

	if( ($get_unit_cost = lavu_query("SELECT `cost` FROM `[1]` WHERE `id` = '[2]'", $tablename, $mod_id)) !== FALSE ){
		if (mysqli_num_rows($get_unit_cost) > 0) {
			$mod_info = mysqli_fetch_assoc($get_unit_cost);
			$unit_cost = $mod_info['cost'];
		}
		mysqli_free_result( $get_unit_cost );
	}

	return $unit_cost;
}

function getCheckCount($loc_id, $order_id) {

	$return_string = "1";
	if( ($get_check_count = lavu_query("SELECT `no_of_checks` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id)) !== FALSE ) {
		if (mysqli_num_rows($get_check_count)) {
			$info = mysqli_fetch_assoc($get_check_count);
			$return_string = $info['no_of_checks'];
		}
		mysqli_free_result( $get_check_count );
	}

	return $return_string;
}

function decimalize($number) {

	return (float)str_replace(",", ".", (string)$number);
}

function checkPaymentStatus() {

	global $location_info;
	global $company_info;

	$title = "";
	$message = "";
	$kickout = false;

	if( ($get_status_info = lavu_query("SELECT `value` AS `due_ts`, `value3` AS `trial_status` FROM `config` WHERE `location` = '[1]' AND `type` = 'system_setting' AND `setting` = 'payment_status'", $location_info['id'])) !== FALSE ){
		if (mysqli_num_rows($get_status_info) > 0) {
			$info = mysqli_fetch_assoc($get_status_info);
			$due_ts = $info['due_ts'];
			if ($due_ts!="") {

				$diff_days = floor(($due_ts - time()) / 60 / 60 / 24) + 1;
				$t_status = $info['trial_status'];
				if ($t_status=="2" || ($t_status=="1" && $diff_days<0)) $title = speak("Trial Expired");
				else if ($t_status=="1") $title = speak("Trial Due to Expire");
				else $title = speak("Billing Issue");

				if ($diff_days < 0) {
					$kickout = true;
					if ($t_status == "0" || empty($company_info['distro_code'])) $message = speak("Please resolve the billing issue for this account to resume use of POS Lavu.")." ".speak("Please call us at 855-767-5288.");
					else $message = speak("Please contact your specialist to resume use of POS Lavu.");
				} else if ($t_status == "1") {
					if ($diff_days > 1) $message = $diff_days . ' ' . speak("days left.");
					else if ($diff_days == 1) $message = speak("1 day left.");
					else if ($diff_days == 0) $message = speak("Today is the last day!");
				} else {
					if ($t_status == "0") $message = speak("Please call billing at 855-767-5288 to resolve this issue.");
					else $message = speak("Please contact your specialist to resolve this issue.");
					if ($diff_days > 1) $message .= " ".speak("We are extending the use of this account by ".$diff_days." days to provide time for resolution.");
					else if ($diff_days == 1) $message .= " ".speak("Only 1 day left to use POS Lavu without resolution.");
					else if ($diff_days == 0) $message .= " ".speak("Today is the last day to use this account without resolution.");
				}
			}
		}
		mysqli_free_result( $get_status_info );
	}

	if ($message!="" && !$kickout) {
		$now_ts = time();
		if (sessvar("pay_status_warning_ts")) {
			$pswt = sessvar("pay_status_warning_ts");
			if (($now_ts - $pswt) < 28800) {
				$title = "";
				$message = "";
			}
		}
		if ($message != "") set_sessvar("pay_status_warning_ts", $now_ts);
	}

	/*global $data_name;

	if ($message != "") {
		error_log("checkPaymentStatus ($data_name): ".$title." ... ".$message." ... ".$kickout);
		$temp_log_type = $kickout?"payment_status_kickout":"payment_status_warning";
		$temp_log = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`temp_info` (`type`, `dataname`, `loc_id`, `datetime`, `info`) VALUES ('[1]', '[2]', '[3]', '[4]', '[5]')", $temp_log_type, $data_name, $location_info['id'], date("Y-m-d H:i:s"), "Mode ".$_REQUEST['m']." - ".$title." - ".$message);
	}*/

	return array("title"=>$title, "message"=>$message, "kickout"=>$kickout);
}

function roleFromRoleID($role_id) {

	$role = "";

	if (!empty($role_id)) {
		$get_role = lavu_query("SELECT `title` FROM `emp_classes` WHERE `id` = '[1]'", $role_id);
		if (mysqli_num_rows($get_role) > 0) {
			$info = mysqli_fetch_assoc($get_role);
			$role = $info['title'];
		}
	}

	return $role;
}

function loadLocations($loc_id, $data_name, &$menu_id)
{
	global $location_info;
	$specific_location = $loc_id?" WHERE `id` = '[1]'":"";
	$locations_array = array();
	$addNewIntegrationFields = 0;

	$exclude = array("integration1", "integration2", "integration3", "integration4", "integration5", "integration6", "integration7", "integration8", "api_key", "api_token");
	$refQuery = lavu_query("select * from poslavu_".$data_name."_db.locations where _disabled=0 limit 1");
	$row = mysqli_fetch_assoc($refQuery);

	if(isset($row['integration9'])){
		$addNewIntegrationFields++;
		array_push($exclude, 'integration9');
	}

	if(isset($row['integration10'])){
		$addNewIntegrationFields++;
		array_push($exclude, 'integration10');
	}

	if ( ($get_locations = lavu_query("SELECT ".create_field_list("locations", $exclude).", IF((AES_DECRYPT(`integration3`,'".integration_keystr()."') != '' AND AES_DECRYPT(`integration4`,'".integration_keystr()."') != ''), '1', '0') AS `integration3and4` FROM `locations`".$specific_location." ORDER BY `state`, `city`, `title` ASC", $loc_id)) !== FALSE )
	{
		if (mysqli_num_rows($get_locations) > 0)
		{

			$use_data = false;
			while ($data_obj = mysqli_fetch_object($get_locations))
			{
				$pathArr = determineNetPath($data_obj);
				$data_obj->use_net_path = $pathArr['use_net_path'];
				$data_obj->net_path = $pathArr['net_path'];
				$data_obj->use_direct_printing = "1";
				if ($data_obj->product_level == "1")
				{
					$data_obj->product_level = "2";
				}
				$integration_info = get_integration_from_location($data_obj->id, "poslavu_".$data_name."_db");
				$data_obj->int_1 = lc_encode($integration_info['integration1'], "jc");
				$data_obj->int_2 = lc_encode($integration_info['integration2'], "jc");
				$data_obj->int_3 = lc_encode($integration_info['integration3'], "jc");
				$data_obj->int_4 = lc_encode($integration_info['integration4'], "jc");
				$data_obj->int_5 = lc_encode($integration_info['integration5'], "jc");
				$data_obj->int_6 = lc_encode($integration_info['integration6'], "jc");
				$data_obj->int_7 = lc_encode($integration_info['integration7'], "jc");
				$data_obj->int_8 = lc_encode($integration_info['integration8'], "jc");

				if($addNewIntegrationFields==2){
					$data_obj->int_9 = lc_encode($integration_info['integration9'], "jc");
					$data_obj->int_10 = lc_encode($integration_info['integration10'], "jc");
				}

				/* As we are not doing  anything in below function call I am commenting below code */
				//$data_obj = netPathEmergencyOverride($data_obj);
				$use_data = array_map("replaceNullWithEmptyString", (array)$data_obj);
				// Adding this condition because we need papi encrypted data only for paypal gateway
				// Please refer ticket LP-8840
				// Below block of code being used by POS 4.X, But it will not break POS 3.X, Becuase iOS code
				// for POS 3.X is not using this new variable
				$manage_item_availability_val = $location_info['manage_item_availability'];
				if (($use_data['gateway'] == 'PayPal') || (strpos(sessvar('location_info')['modules'], 'extensions.payment.+paypal') !== false) || $manage_item_availability_val) {
					$dataname = $location_info['data_name'];
					$locationId = $use_data['id'];
					$use_data['ENCRYPTED_DATANAME'] = generateEncryptedDataname($dataname, $locationId);
					if (($use_data['gateway'] == 'PayPal') || (strpos(sessvar('location_info')['modules'], 'extensions.payment.+paypal') !== false)) {
						$use_data['PAPI_PAYPAL_AUTH_TOKEN'] = generatePapiAuthToken($dataname, $locationId);
						$use_data['PAYPAL_REFRESH_TOKEN_URL'] = getenv('PAYPAL_REFRESH_TOKEN_URL');
						$use_data['PAPI_API'] = getenv('PAPI_API');
						$use_data['PAYPAL_ENV'] = getenv('PAYPAL_ENV');
					}
				} elseif ( $use_data['gateway'] === 'LavuPay' ) {
					// data_name is global
					$locationId = $use_data['id'];
					$use_data['PAPI_AUTH_TOKEN'] = generatePapiAuthToken($data_name, $locationId);
					$use_data['PAPI_URL'] = getenv('PAPI_API');
					$tenantAccount = new TenantAccount($data_name, $locationId);
					$defaultGateway = $tenantAccount->defaultGateway();
					if ( $defaultGateway->gatewayName === 'freedompay' ) {
						$use_data['gateway'] = $defaultGateway->gatewayName;
						$use_data['gatewayData'] = $defaultGateway->asNormalizedArray();
					}
				}
				$locations_array[] = $use_data;
			}

			if ($use_data)
			{
				$menu_id = $use_data['menu_id'];
			}
		}
		else
		{
			lavu_echo('{"json_status":"NoL"}');
		}

		mysqli_free_result( $get_locations );
	}
	else
	{
		lavu_echo('{"json_status":"NoL"}');
	}

	return $locations_array;
}

/**
 * Generates a JWT string for the user to call PAPI
 *
 * @param string         $dataname
 * @param string         $locationId
 *
 * @return string A signed JWT
 */
function generatePapiAuthToken($dataname, $locationId) {
	$jwtPapi = generateJwtPapi($dataname, $locationId);
	return $jwtPapi->generatePapiAuthToken();
}

/**
 * Encrypts dataname
 *
 * @param string         $dataname
 * @param string         $locationId
 *
 * @return string encrypted dataname
 */
function generateEncryptedDataname($dataname, $locationId) {
	$jwtPapi = generateJwtPapi($dataname, $locationId);
	return $jwtPapi->genEncryptedDataname();
}

/**
 * Instantiates jwtPapi
 *
 * @param string         $dataname
 * @param string         $locationId
 *
 * @return jwtPapi
 */
function generateJwtPapi($dataname, $locationId) {
	$args = array(
		'dataname' => $dataname,
		'locationid' => $locationId
	);
	return new jwtPapi($args);
}

function determineNetPath($location_and_data_obj)
{
	global $company_code;//Defined in json_connect
	global $company_code_key;//Defined in json_connect
	global $activeUserInfo;//Stored in db.users._use_tunnel, set at begining of mode2.

	$net_path = $location_and_data_obj->net_path; // have net_path and use_net_path default to values stored in database; sending a nil value to the app will result in a crash
	$use_net_path = $location_and_data_obj->use_net_path;
	$is_lavu_user = ($activeUserInfo['f_name']=="LavuAdmin" || strpos(strtolower($activeUserInfo['username']), "lavu")===0);
	$useTunnel = $activeUserInfo['_use_tunnel'];

	if ($useTunnel && $is_lavu_user)
	{
		//We lookup the ip proxy table
		$proxyTable;
		//We import the proxyIPTable map in a way that should never cause error of this running script.
		ob_start();
		include_once( dirname(__FILE__).'/redirect_tables.php' );
		$garbage = ob_get_clean();
		if(function_exists( 'getProxyIPTable' )){
			$proxyTable = getProxyIPTable();
//error_log("Loaded proxy table with count:  " . count($proxyTable));
		}else{
			$proxyTable = array("https://lsvpn.poslavu.com/", "http://www.poslavu.com/lsvpn/");
			error_log("Could not find proxy_ip_table, file does not exist or error loading it. Loading default lsvpns.");
		}

		if($useTunnel == 1){
//error_log("Using tunnel 1 " . $proxyTable[$useTunnel]);
			if(testNewLSVPN($company_code, $company_code_key)){
				$use_net_path = "1";
				$net_path = $proxyTable[1];
			}
			else{
				$use_net_path = "1";
				$net_path = $proxyTable[2];
			}
		}else if($useTunnel == 2){
//error_log("Using tunnel 2 ". $proxyTable[$useTunnel]);
			$use_net_path = "1";
			$net_path = $proxyTable[2];
		}else if($useTunnel > 2){
			if( !empty($proxyTable[$useTunnel]) ){
//error_log("Using tunnel " . $useTunnel . " " . $proxyTable[$useTunnel]);
				$use_net_path = "1";
				$net_path = $proxyTable[$useTunnel];
			}
		}
	}

	if (!empty($use_net_path)) $use_net_path = "1";

//error_log('net_path::', $net_path);
	return array('net_path' => $net_path, 'use_net_path' => $use_net_path);
}

function loadWebExtensionInfo($web_extensions, $active_modules) {

	if (count($web_extensions) > 0) {
		if( ($get_web_extensions = mlavu_query("SELECT `id`, `title`, `c_ids`, `c_xs`, `c_ys`, `c_ws`, `c_hs`, `ipod_c_xs`, `ipod_c_ys`, `ipod_c_ws`, `ipod_c_hs`, `inapp_admin`, `force_admin_server`, `native_border`, `gift_options`, `loyalty_options` , `void_refunds`, `void_issues_reloads` FROM `poslavu_MAIN_db`.`component_layouts` WHERE `id` IN (".implode(",", $web_extensions).") AND `_deleted` = '0'")) !== FALSE ){
			if (mysqli_num_rows($get_web_extensions) > 0) {
				$return_obj = new stdClass();
				$cids = array();
				while ($info = mysqli_fetch_assoc($get_web_extensions)) {
					$return_obj->{(string)$info['id']} = $info;
					$cids = array_merge($cids, explode(",", $info['c_ids']));
				}

				$components_obj = new stdClass();
				if( ($get_components = mlavu_query("SELECT `id`, `type`, `allow_scroll`, `name`, `filename` FROM `poslavu_MAIN_db`.`components` WHERE `id` IN (".implode(",", $cids).")")) !== FALSE ){
					if (mysqli_num_rows($get_components) > 0) {
						while ($c_info = mysqli_fetch_assoc($get_components)) {
							$components_obj->{(string)$c_info['id']} = $c_info;
						}
					}
					mysqli_free_result( $get_components );
				}
				$return_obj->components = $components_obj;

				return $return_obj;
			}
			mysqli_free_result( $get_Web_extensions );
		}
	}

	return new stdClass();
}

function loadMenuData($menu_id, $loc_id, $for_lavu_togo, &$result) { // load all menu data and more; includes groups, categories, items, forced/optional modifiers, printer groups, quick pay buttons, tax rates, tax exemptions, etc...

	global $data_name;
	global $location_info;
	global $active_modules;

	$got_a_cateogory = FALSE;

	$menu_groups_array = array();
	$menu_categories_array = array();
	$menu_items_array = array();
	$menu_items = "";
	$used_modifier_lists = "";

	$active_field = ($for_lavu_togo == "1")?"ltg_display":"active";

	$limit = "";
	$universal_order = "ASC";
	$lavu_lite = false;
	if (isset($post_vars['lavu_lite']) && $post_vars['lavu_lite']=="1") {
		$limit = " LIMIT 1";
		$universal_order = "DESC";
		$lavu_lite = true;
	}

	if (reqvar("sequential_menu_load") == "1") {

		// grab menu_groups and menu_categories id list

		if( ($get_menu_groups = lavu_query("SELECT `id`, `group_name`, `orderby` FROM `menu_groups` WHERE `menu_id` = '".$menu_id."' AND `_deleted` != '1' ORDER BY (`group_name` = 'Universal') ".$universal_order.", `_order` ASC".$limit)) !== FALSE ){
			if (mysqli_num_rows($get_menu_groups) > 0) {

				$used_o_mod_list_ids = array();
				while ($data_obj_MG = mysqli_fetch_object($get_menu_groups)) {

					if ($data_obj_MG->group_name == "Universal") $menu_groups_array[] = $data_obj_MG;
					$sort_type = $data_obj_MG->orderby;
					$order_by = ($sort_type == "manual")?" ORDER BY `_order` ASC":" ORDER BY `name` ASC";

					if( ($get_menu_categories = lavu_query("SELECT `id`, `modifier_list_id`, `allow_ordertype_tax_exempt` FROM `menu_categories` WHERE `menu_id` = '".$menu_id."' AND `group_id` = '".$data_obj_MG->id."' AND `_deleted` != '1'".$order_by)) !== FALSE ){
						if (mysqli_num_rows($get_menu_categories) > 0) {

							if ($data_obj_MG->group_name != "Universal") $menu_groups_array[] = $data_obj_MG;
							$got_a_category = TRUE;

							while ($data_obj = mysqli_fetch_object($get_menu_categories)) {
								$menu_categories_array[] = array($data_obj->id, $data_obj_MG->id);
								$o_mod_list_id = $data_obj->modifier_list_id;
								if (!in_array($o_mod_list_id, $used_o_mod_list_ids) && $o_mod_list_id!=0) $used_o_mod_list_ids[] = $o_mod_list_id;
							}
						}
						mysqli_free_result( $get_menu_categories );
					}
				}
				$result['modifier_lists'] = $used_o_mod_list_ids;

			} else {

				lavu_echo('{"json_status":"NoGroups"}');
			}

			mysqli_free_result( $get_menu_groups );

		} else {

			lavu_echo('{"json_status":"NoGroups"}');
		}

		if (!$got_a_category) {
			lavu_echo('{"json_status":"NoC"}');
		}

		$menu_items_rtn_str = "";
		$forced_modifier_list_rtn_str = "";

		$old_o_mod_rtn_str = "";
		$old_happy_hours_rtn_str = "";

	} else {
		global $o_happyHoursGetCategoryOrItem;
		require_once(resource_path()."/../areas/happyhours/php/happyhours_getcategoryoritem.php");

		if ( ($get_menu_groups = lavu_query("SELECT `id`, `group_name`, `orderby` FROM `menu_groups` WHERE `menu_id` = '".$menu_id."' AND `_deleted` != '1' ORDER BY (`group_name` = 'Universal') ".$universal_order.", `_order` ASC".$limit)) !== FALSE ) {

			if (mysqli_num_rows($get_menu_groups) > 0) {
				while ($data_obj_MG = mysqli_fetch_object($get_menu_groups)) {
					$sort_type = $data_obj_MG->orderby;
					if($data_obj_MG->group_name == "Universal")
					{
						$menu_groups_array[] = $data_obj_MG;
					}
					$order_by = ($sort_type == "manual")?" ORDER BY `_order` ASC":" ORDER BY `name` ASC";

					if ( ($get_menu_categories = lavu_query("SELECT `id`, `group_id`, `name`, `image`, `storage_key` , `description`, `active`, `_order`, `print`, `last_modified_date`, `printer`, `modifier_list_id`, `apply_taxrate`, `custom_taxrate`, `forced_modifier_group_id`, `print_order`, `super_group_id`, `tax_inclusion`, `happyhour`, `tax_profile_id`, `price_tier_profile_id`, `no_discount`, `enable_reorder`, `allow_ordertype_tax_exempt` FROM `menu_categories` WHERE `menu_id` = '".$menu_id."' AND `group_id` = '".$data_obj_MG->id."' AND `$active_field` = '1' AND `_deleted` != '1'".$order_by)) !== FALSE ){

						if ( mysqli_num_rows($get_menu_categories) > 0 ) {
							if($data_obj_MG->group_name != "Universal")
							{
								$menu_groups_array[] = $data_obj_MG;
							}
							$got_a_category = TRUE;
							while ($data_obj = mysqli_fetch_object($get_menu_categories)) {

								if ($data_obj->printer == "") {
									$data_obj->print = "0";
								}

								$image_path = "/mnt/poslavu-files/images/".$data_name."/categories/".$data_obj->image;
								if (!file_exists($image_path) || is_dir($image_path)) $data_obj->image = "";

								if ((!strstr($used_modifier_lists, "|".$data_obj->modifier_list_id."|")) && ($data_obj->modifier_list_id != 0)) {
									$used_modifier_lists .= "|".$data_obj->modifier_list_id."|";
								}

								// get the menu items and happy hours
								$check_for_combo = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'disable_combo_bulider' AND `location` = '1'");
								$combo_info = mysqli_fetch_assoc($check_for_combo);
								$column = "`id`, `category_id`, `name`, `price`, `description`, `image`,  `storage_key`, `active`, `print`, `quick_item`, `last_modified_date`, `printer`, `apply_taxrate`, `custom_taxrate`, `modifier_list_id`, `forced_modifier_group_id`, `open_item`, `hidden_value`, `hidden_value2`, `allow_deposit`, `UPC`, `hidden_value3`, `inv_count`, `super_group_id`, `tax_inclusion`, `happyhour`, `tax_profile_id`, `track_86_count`, `price_tier_profile_id`, `no_discount`, `product_code`, `tax_code` ,`combo`, `comboitems`, `currency_type`, `allow_ordertype_tax_exempt`, `kitchen_nickname`, `unavailable_until` - IF(`unavailable_until` > 0, 300, 0) AS `unavailable_until`";
								$comboCond = "";
								if ($combo_info['value'] != 1) {
									$comboCond = " AND `combo` != '1'";
								}
								$menu_query = "SELECT " . $column . " FROM `menu_items` WHERE `menu_id` = '".$menu_id."' AND `category_id` = '".$data_obj->id."' AND `$active_field` = '1' AND `_deleted` != '1'" . $comboCond;
								$a_menu_items = array();
								if (($get_menu_items = lavu_query($menu_query.$order_by)) !== FALSE ) {
									if ( mysqli_num_rows($get_menu_items) > 0) {
										while ($row = mysqli_fetch_assoc($get_menu_items)){
											$a_menu_items[] = $row;
										}
									}
									mysqli_free_result( $get_menu_items );
								} else {
									lavu_echo(onNoItems($data_obj->name));
								}
								$item_hhs = $o_happyHoursGetCategoryOrItem->get_all_items_from_category_happyhours($data_obj->id, (array)$data_obj, $a_menu_items, TRUE);

								if (count($a_menu_items) > 0) {

									$menu_categories_array[] = $data_obj;
									for ($i = 0; $i < count($a_menu_items); $i++) {
										$data_obj2 = (object)$a_menu_items[$i];

										if (isset($data_obj2->name)) $data_obj2->name = str_pad($data_obj2->name, 2, " ", STR_PAD_RIGHT);

										if (($data_obj2->printer == "") || ($data_obj->printer == "" && $data_obj2->printer == "0")) $data_obj2->print = "0";


										$image_path = "/mnt/poslavu-files/images/".$data_name."/items/".$data_obj2->image;
										if (!file_exists($image_path) || is_dir($image_path)) $data_obj2->image = "";


										if (isset($item_hhs[$data_obj2->id])) $data_obj2->happyhour = $item_hhs[$data_obj2->id];
										else $data_obj2->happyhour = "";

										$menu_items_array[] = $data_obj2;
										if ((!strstr($used_modifier_lists, "|".$data_obj2->modifier_list_id."|")) && ($data_obj2->modifier_list_id != 0)) $used_modifier_lists .= "|".$data_obj2->modifier_list_id."|";
									}
								}
							}
							mysqli_free_result( $get_menu_categories );
						}
					} else {
						lavu_echo('{"json_status":"NoC"}');
					}
				}

			}
			mysqli_free_result( $get_menu_groups );

		} else {

			lavu_echo('{"json_status":"NoGroups"}');
		}

		if (!$got_a_category) {
			lavu_echo('{"json_status":"NoC"}');
		}
		foreach($menu_items_array as $menu_combo)
		{
			if ($menu_combo->combo=='1'){
				$menu_combo->forced_modifier_group_id='';
				$menu_combo->modifier_list_id='0';
				$menu_combo->comboitems=getmenuccomboddetails($menu_combo->id); }
		}
		$result['menu_items'] = $menu_items_array;

		$forced_modifier_list_array = array();
		$forced_modifier_list_array[] = array(
			"id"		=> "99999999",
			"title"		=> "place_holder",
			"type"		=> "choice",
			"modifiers"	=>	array(
				array(
					"id"		=> "0",
					"title"		=> "MOD ERROR",
					"cost"		=> "0.00",
					"detour"	=> "0"
				)
			)
		);
		if( ($get_forced_modifier_lists = lavu_query("SELECT `id`, `title`, `type`, `type_option` FROM `forced_modifier_lists` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id)) !== FALSE ){
			if (mysqli_num_rows($get_forced_modifier_lists) > 0) {
				$happyHourForcedModifierListsIds = array();
				$f_m_l= lavu_query("SELECT `id`, `happyhours_id` FROM `forced_modifier_lists` WHERE `_deleted` != 1");
				while ($f_m_l_h = mysqli_fetch_assoc($f_m_l)) {
					$happyHourForcedModifierListsIds[$f_m_l_h['id']] = $f_m_l_h['happyhours_id'];
				}
				$lists_array = array();
				$get_forced_modifiers = lavu_query("SELECT `id`, `list_id`, `title`, `cost`, `detour`, `extra`, `extra2`, `extra3`, `extra4`, `extra5`, `product_code`, `tax_code`,`happyhours_id`, `ordertype_id`, `unavailable_until` - IF(`unavailable_until` > 0, 300, 0) AS `unavailable_until` FROM `forced_modifiers` WHERE `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC");
				while ($extract2 = mysqli_fetch_assoc($get_forced_modifiers)) {
					if(is_null($extract2['ordertype_id'])){
						$extract2['ordertype_id'] = "";
					}
					$la_key = "l".$extract2['list_id'];
					if (!isset($lists_array[$la_key])) {
						$lists_array[$la_key] = array();
					}
					if ($extract2['happyhours_id'] != 0) {
						$f_happyhours=$o_happyHoursGetCategoryOrItem->get_all_details_id_happyhours($extract2['happyhours_id'], 'forced');
					} else {
						$extract2_list_id=$extract2['list_id'];
						if (isset($happyHourForcedModifierListsIds[$extract2_list_id]) && $happyHourForcedModifierListsIds[$extract2_list_id] != '0') {
						$f_happyhours=$o_happyHoursGetCategoryOrItem->get_all_details_id_happyhours($happyHourForcedModifierListsIds[$extract2_list_id], 'forced');
						} else { $f_happyhours=''; }
					}
					$extract2['happyhour']=$f_happyhours;
					$lists_array[$la_key][] = $extract2;
				}
				while ($extract = mysqli_fetch_assoc($get_forced_modifier_lists)) {
					$la_key = "l".$extract['id'];
					if (!isset($lists_array[$la_key])) {
						$lists_array[$la_key] = array();
					}
					list($PizzaForcedMods, $pizzaForcedModsServingPrice) = getPizzaForcedMods($extract['id'], $data_name);
					$extract['PizzaForcedMods'] = $PizzaForcedMods;
					$extract['PizzaForcedModsServingPrice'] = $pizzaForcedModsServingPrice;
					$extract['modifiers'] = $lists_array[$la_key];
					$forced_modifier_list_array[] = $extract;
				}
			}
			mysqli_free_result( $get_forced_modifier_lists );
		}
		$result['forced_modifier_lists'] = $forced_modifier_list_array;

		$coded_modifier_list_array = array();
		$happyHourModifierCategories = array();
		if (!$lavu_lite && $used_modifier_lists!="") {

			$used_modifier_lists_array = explode("|", trim($used_modifier_lists, "|"));
			$o_m_c = lavu_query("SELECT `id`, `happyhours_id` FROM `modifier_categories` WHERE `_deleted` != 1") ;
			while ($o_m_l_h = mysqli_fetch_assoc($o_m_c)) {
				$happyHourModifierCategories[$o_m_l_h['id']] = $o_m_l_h['happyhours_id'];
			}
			foreach ($used_modifier_lists_array as $ml) {
				if (($get_modifiers = lavu_query("SELECT `id`, `title`, `cost`, `product_code`, `tax_code`,`happyhours_id`, `unavailable_until` - IF(`unavailable_until` > 0, 300, 0) AS `unavailable_until` FROM `modifiers` WHERE `category` = '".$ml."' AND `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC")) !== FALSE) {
					if (mysqli_num_rows($get_modifiers) > 0) {
						$modifiers_list = array();
						$modifiers_string = "";
						$count = 0;
						while ($extract = mysqli_fetch_assoc($get_modifiers)) {
							$modifiers_string .= $extract['id']."|o|".$extract['title']."|o|".$extract['cost'];
							if ($extract['happyhours_id'] != 0) {
								$f_happyhours = $o_happyHoursGetCategoryOrItem->get_all_details_id_happyhours($extract['happyhours_id'], 'optional');
							} else {
								if (isset($happyHourModifierCategories[$ml]) && $happyHourModifierCategories[$ml] != '0') {
									$f_happyhours = $o_happyHoursGetCategoryOrItem->get_all_details_id_happyhours($happyHourModifierCategories[$ml], 'optional');
								} else { $f_happyhours = ''; }
							}
							$extract['happyhour'] = $f_happyhours;
							$modifiers_list[] = $extract;
							$count++;
							if ($count < mysqli_num_rows($get_modifiers)) {
								$modifiers_string .= "|*|";
							}
						}
						$coded_modifier_list_array[] = array(
							"id"			=> $ml,
							"modifiers"		=> $modifiers_string,
							"modifiers_o"	=> $modifiers_list
						);
					}

					mysqli_free_result($get_modifiers);
				}
			}
		}
		$result['modifier_lists'] = $coded_modifier_list_array;
		$result['modifications'] = "";
		$result['happy_hours'] = array();
	}

	$fmg_array = array();
	$fmg_array[] = new fmg_placeholder();
	if (($get_forced_modifier_groups = lavu_query("SELECT `id`, `title`, `include_lists` FROM `forced_modifier_groups` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id)) !== FALSE) {
		if (mysqli_num_rows($get_forced_modifier_groups) > 0) {
			while ($data_obj = mysqli_fetch_object($get_forced_modifier_groups)) {
				$fmg_array[] = $data_obj;
			}
		}
		mysqli_free_result( $get_forced_modifier_groups );
	}
	$result['forced_modifier_groups'] = $fmg_array;

	$result['printer_groups'] = array(
		"ph1" => "ignore",
		"ph2" => "ignore"
	);
	if (($get_printer_groups = lavu_query("SELECT `id`, `value`, `value2`, `value3`, `value4`, `value5`, `value6` FROM `config` WHERE `type` = 'printer_group' AND `location` = '".$loc_id."'")) !== FALSE) {
		if (mysqli_num_rows($get_printer_groups) > 0) {
			while ($extract = mysqli_fetch_assoc($get_printer_groups)) {
				$result['printer_groups']['pg_'.$extract['id']] = '|'.$extract['value'].'|'.$extract['value2'].'|'.$extract['value3'].'|'.$extract['value4'].'|'.$extract['value5'].'|'.$extract['value6'].'|';
				// would rather this be like this:
				// $result['printer_groups']['pg_'.$extract['id']] = array(
				// 	$extract['value'],
				// 	$extract['value2'],
				// 	$extract['value3'],
				// 	$extract['value4'],
				// 	$extract['value5'],
				// 	$extract['value6']
				// );
			}
		}
		mysqli_free_result( $get_printer_groups );
	}

	$result['qp_titles'] = array(
		'$1',
		'$5',
		'$10',
		'$20'
	);
	$result['qp_values'] = array(
		'1',
		'5',
		'10',
		'20'
	);
	if (($get_quick_pay_buttons = lavu_query("SELECT * FROM `quick_pay_buttons` WHERE `loc_id` = '".$loc_id."' LIMIT 1")) !== FALSE) {
		if (mysqli_num_rows($get_quick_pay_buttons) > 0) {
			$qp_info = mysqli_fetch_assoc($get_quick_pay_buttons);
			$result['qp_titles'] = array(
				$qp_info['title1'],
				$qp_info['title2'],
				$qp_info['title3'],
				$qp_info['title4']
			);
			$result['qp_values'] = array(
				$qp_info['value1'],
				$qp_info['value2'],
				$qp_info['value3'],
				$qp_info['value4']
			);
		}
		mysqli_free_result( $get_quick_pay_buttons );
	}

	$result['qt_calcs'] = array(
		"0.18",
		"0.20",
		"0.22"
	);

	if (($get_quick_tip_buttons = lavu_query("SELECT * FROM `quick_tip_buttons` WHERE `loc_id` = '".$loc_id."' LIMIT 1")) !== FALSE) {
		if (mysqli_num_rows($get_quick_tip_buttons) > 0) {
			$qt_info = mysqli_fetch_assoc($get_quick_tip_buttons);
			$result['qt_calcs'] = array(
				$qt_info['calc1'],
				$qt_info['calc2'],
				$qt_info['calc3']
			);
		}
		mysqli_free_result ($get_quick_tip_buttons );
	}

	$payment_types = array();
	$payment_types[] = array(
		"id"				=> "3",
		"type"				=> "Gift Certificate",
		"special"			=> "",
		"get_info"			=> "0",
		"info_label"		=> "",
		"info_type"			=> "",
		"min_access"		=> "1",
		"no_auto_close"		=> "0",
		"disables_rounding" => "0",
		"hide"				=> "0",
		"can_convert"		=> "0",
		"can_convert_to"	=> "0",
	);
	$web_extensions = array();
	if (!$lavu_lite) {
		if (($get_payment_types = lavu_query("SELECT `id`, `type`, `special`, `get_info`, `info_label`, `info_type`, `min_access`, `no_auto_close`, `disables_rounding`, `hide`, `can_convert`, `can_convert_to` FROM `payment_types` WHERE `loc_id` = '[1]' AND `_deleted` != '1' ORDER BY `_order` ASC", $loc_id)) !== FALSE) {
			if (mysqli_num_rows($get_payment_types) > 0) {
				while ($ptype = mysqli_fetch_assoc($get_payment_types)) {
					$payment_types[] = $ptype;
					$special = $ptype['special'];
					if (substr($special, 0, 18)=="payment_extension:" || substr($special, 0, 15)=="gift_extension:" || substr($special, 0, 18)=="loyalty_extension:") {
						$pe_parts = explode(":", $special);
						$layout_id = $pe_parts[1];
						if (!empty($layout_id)) {
							if (!in_array($layout_id, $web_extensions)) {
								$web_extensions[] = $layout_id;
							}
						}
					}
				}
			}
			mysqli_free_result( $get_payment_types );
		}
	}
	$result['payment_types'] = $payment_types;
	$result['web_extensions'] = loadWebExtensionInfo($web_extensions, $active_modules);

	$cc_type_array = array();
	$should_check_cc_types = true;

	if (($check_config_cc_types = lavu_query("SELECT `value` AS `type` FROM `config` WHERE `location` = '[1]' AND `setting` = 'payment_methods_card' AND `_deleted` = '0' ORDER BY `value6` ASC", $loc_id)) !== FALSE) {
		if (mysqli_num_rows($check_config_cc_types) > 0) {
			$should_check_cc_types = false;
			while ($info = mysqli_fetch_assoc($check_config_cc_types)) {
				$cc_type_array[] = $info;
			}
		}
		mysqli_free_result( $check_config_cc_types );
	}

	if ($should_check_cc_types) {
		if (($get_cc_types = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`cc_types` ORDER BY `type` ASC")) !== FALSE) {
			if (mysqli_num_rows($get_cc_types) > 0) {
				while ($data_obj = mysqli_fetch_object($get_cc_types)) {
					$cc_type_array[] = $data_obj;
				}
			}
			mysqli_free_result( $get_cc_types );
		}
	}
	$result['cc_types'] = $cc_type_array;

	$tax_rate_array = array();

	$result['tax_rates'] = array(
		array(
			"id"			=> "0",
			"title"			=> "Tax",
			"calc"			=> $location_info['taxrate'],
			"type"			=> "p",
			"stack"			=> "y",
			"tier_type1"	=> "",
			"tier_value1"	=> "",
			"force_tier1"	=> "0",
			"calc2"			=> "",
			"force_apply"	=> "",
		)
	);

	if (($get_tax_rates = lavu_query("SELECT * FROM `tax_rates` WHERE `loc_id` = '[1]' AND `_deleted` != '1' AND `profile_id` = '0'", $loc_id)) !== FALSE) {
		if (mysqli_num_rows($get_tax_rates) > 0) {
			while ($data_obj = mysqli_fetch_object($get_tax_rates)) {
				$tax_rate_array[] = $data_obj;
			}
			$result['tax_rates'] = $tax_rate_array;
		}
		mysqli_free_result( $get_tax_rates );
	}
	$tax_profiles_array = array();
	$tax_profiles_array['X'] = array();

	if (($get_tax_profiles = lavu_query("SELECT `id` FROM `tax_profiles` WHERE `loc_id` = '[1]'", $loc_id)) !== FALSE) {
		$get_tax_rates = lavu_query("SELECT * FROM `tax_rates` WHERE `_deleted` = '0' ORDER BY `id` ASC");
		while ($data_obj = mysqli_fetch_object($get_tax_rates)) {
			$tax_array[$data_obj->profile_id][] = $data_obj;
		}
		while ($info = mysqli_fetch_assoc($get_tax_profiles)) {
			if (isset($tax_array[$info['id']]) && $tax_array[$info['id']] != '') {
				$tax_profiles_array[(string)$info['id']] = $tax_array[$info['id']];
			}
		}
		mysqli_free_result( $get_tax_profiles );
	}
	$result['tax_profiles'] = $tax_profiles_array;

	$tax_exemption_array = array();
	$get_tax_exemptions = lavu_query("SELECT * FROM `tax_exemptions` WHERE `loc_id` = '".$loc_id."' AND `_deleted` != '1'");
	while ($data_obj = mysqli_fetch_object($get_tax_exemptions)) {
		$tax_exemption_array[] = $data_obj;
	}
	$result['tax_exemptions'] = $tax_exemption_array;
	$result['active_modules'] = $active_modules->getModList();

	if (($get_roles = lavu_query("SELECT `id`, `title` FROM `emp_classes` WHERE `_deleted` = '0'")) !== FALSE) {
		if (mysqli_num_rows($get_roles) > 0) {
			$roles_array = array();
			while ($info = mysqli_fetch_assoc($get_roles)) {
				$roles_array[$info['id']] = $info['title'];
			}
			$result['roles'] = $roles_array;
		}
		mysqli_free_result( $get_roles );
	}

	$order_tags_object = new stdClass();
	$order_tags_ordered_ids = array();
	if( ($get_order_tags = lavu_query("SELECT * FROM `order_tags` ORDER BY `id` ASC")) !== FALSE ){
		if (mysqli_num_rows($get_order_tags) > 0) {
			while ($info = mysqli_fetch_assoc($get_order_tags)) {
				$order_tags_object->$info['id'] = $info;
				$order_tags_ordered_ids[] = $info['id'];
			}
		}
		mysqli_free_result( $get_order_tags );
	}
	$result['order_tags'] = $order_tags_object;
	$result['order_tags_ordered_ids'] = $order_tags_ordered_ids;

	$discount_types_object = new stdclass();
	if( ($get_order_discount_types = lavu_query("SELECT * FROM `discount_types` WHERE `loc_id` = '[1]' AND `_deleted` != '1' ORDER BY `_order`, `title` ASC", $loc_id )) !== FALSE ){
		if( mysqli_num_rows( $get_order_discount_types ) > 0 ){
			while( $info = mysqli_fetch_assoc( $get_order_discount_types ) ) {
				$id = $info['id'];
				$discount_types_object->$id = $info;
			}
		}
	}
	$result['discount_types'] = $discount_types_object;

	$table_setup_array = array();
	if( ($get_table_setup = lavu_query("SELECT `id`, `coord_x`, `coord_y`, `shapes`, `widths`, `heights`, `names`, `rotate`, `title`, `revenue_centers` FROM `tables` WHERE `loc_id` = '[1]' AND `_deleted` = '0' ORDER BY `_order`, `title` ASC", $_REQUEST['loc_id'])) !== FALSE ){
		if (mysqli_num_rows($get_table_setup) > 0) {
			while ($data_obj = mysqli_fetch_object($get_table_setup)) {
				$table_setup_array[] = $data_obj;
			}
		} else {
			lavu_echo('{"json_status":"NoTS"}');
		}

		mysqli_free_result( $get_table_setup );

	} else {

		lavu_echo('{"json_status":"NoTS"}');
	}
	$result['table_setup'] = $table_setup_array;

	$table_shapes_array = array();
	//$table_shapes_array['square'] = array("empty"=>"table_square.png","active"=>"table_square_hl.png","printed"=>"table_square_p.png"); // [state]_color can be used to set text color
	//$table_shapes_array['circle'] = array("empty"=>"table_circle.png","active"=>"table_circle_hl.png","printed"=>"table_circle_p.png");
	//$table_shapes_array['diamond'] = array("empty"=>"table_diamond.png","active"=>"table_diamond_hl.png","printed"=>"table_diamond_p.png");
	//$table_shapes_array['slant_left'] = array("empty"=>"table_slant_left.png","active"=>"table_slant_left_hl.png","printed"=>"table_slant_left_p.png");
	//$table_shapes_array['slant_right'] = array("empty"=>"table_slant_right.png","active"=>"table_slant_right_hl.png","printed"=>"table_slant_right_p.png");
	$table_shapes_array['square_new'] = array( "empty"=>"table_square_new.png", "active"=>"table_square_new_hl.png", "printed"=>"table_square_new_p.png" );

	$result['table_shapes'] = $table_shapes_array;

	$price_tier_profiles = array();
	if ($active_modules->hasModule("dining.price_tiers")) {
		if (($get_profiles = lavu_query("SELECT `id`, `value2` FROM `config` WHERE `location` = '[1]' AND `type` = 'price_tier_profile' AND `_deleted` = '0'", $_REQUEST['loc_id'])) !== FALSE) {
			if (mysqli_num_rows($get_profiles) > 0) {
				while ($pinfo = mysqli_fetch_assoc($get_profiles)) {
					if (($get_tiers = lavu_query("SELECT `value` AS `min_value`, `value3` AS `max_value`, `value4` AS `type`, `value5` AS `op_value` FROM `config` WHERE `location` = '[1]' AND `type` = 'price_tier' AND `value2` = '[2]' AND `_deleted` = '0' ORDER BY (`value` * 1) ASC", $_REQUEST['loc_id'], $pinfo['id'])) !== FALSE) {
						if (mysqli_num_rows($get_tiers) > 0) {
							$price_tier_profiles[$pinfo['id']] = array();
							while ($tinfo = mysqli_fetch_assoc($get_tiers)) {
								$tinfo['price_effect'] = $pinfo['value2'];
								$price_tier_profiles[$pinfo['id']][$tinfo['min_value']] = $tinfo;
							}
						}
						mysqli_free_result( $get_tiers );
					}
				}
			}
			mysqli_free_result( $get_profiles );
		}
	}

	$result['menu_groups'] = $menu_groups_array;
	$result['menu_categories'] = $menu_categories_array;
	$result['price_tier_profiles'] = $price_tier_profiles;

	$meal_periods_array = array();
	$get_meal_periods = lavu_query("SELECT `type_id`, `day_of_week`, `start_time`, `end_time` FROM `meal_periods` WHERE `_deleted` = '0'");
	if ($get_meal_periods) {
		if (mysqli_num_rows($get_meal_periods) > 0) {
			while ($info = mysqli_fetch_assoc($get_meal_periods)) {
				$meal_periods_array[] = $info;
			}
		}
	}
	$result['meal_periods'] = $meal_periods_array;

	if (reqvar("get_platform_list", false)) {

		$use_platform_list = iOSplatformList();
		if (count($use_platform_list) >= 53) { // should have at least this many (platforms available as of 2/11/2016)
			$result['ios_platform_list'] = $use_platform_list;
		}
	}
}

function loadMenuCategory($post_vars) {

	global $data_name;

	$response_array = array();

	global $o_happyHoursGetCategoryOrItem;
	require_once(resource_path()."/../areas/happyhours/php/happyhours_getcategoryoritem.php");

	if( ($get_category_info = lavu_query("SELECT `id`, `group_id`, `name`, `image`, `description`, `active`, `_order`, `print`, `last_modified_date`, `printer`, `modifier_list_id`, `apply_taxrate`, `custom_taxrate`, `forced_modifier_group_id`, `print_order`, `super_group_id`, `tax_inclusion`, `happyhour`, `tax_profile_id`, `price_tier_profile_id`, `no_discount`, `enable_reorder`, `allow_ordertype_tax_exempt` FROM `menu_categories` WHERE `id` = '[1]'", $post_vars['category_id'])) !== FALSE ) {
		if (mysqli_num_rows($get_category_info) > 0) {

			$data_obj = mysqli_fetch_object($get_category_info);

			if ($data_obj->printer == "") {
				$data_obj->print = "0";
			}

			$image_path = "/mnt/poslavu-files/images/".$data_name."/categories/".$data_obj->image;
			if (!file_exists($image_path) || is_dir($image_path)) $data_obj->image = "";

			$sort_type = "alpha";
			$get_group_info = lavu_query("SELECT `orderby` FROM `menu_groups` WHERE `id` = '[1]'", $data_obj->group_id);
			if ($get_group_info !== FALSE) {
				if (mysqli_num_rows($get_group_info) > 0) {
					$group_info = mysqli_fetch_assoc($get_group_info);
					$sort_type = $group_info['orderby'];
				}
			} else {
				$response_array['json_status'] = "NoGroups";
			}
			$order_by = ($sort_type == "manual")?" ORDER BY `_order` ASC":" ORDER BY `name` ASC";

			$data_obj->item_order = array();
			$item_array = array();
			$send_item_array = array();
			$used_o_mod_list_ids = array();

			$get_menu_items = lavu_query("SELECT `id`, `category_id`, `name`, `price`, `description`, `image`, `active`, `print`, `quick_item`, `last_modified_date`, `printer`, `apply_taxrate`, `custom_taxrate`, `modifier_list_id`, `forced_modifier_group_id`, `open_item`, `hidden_value`, `hidden_value2`, `allow_deposit`, `UPC`, `hidden_value3`, `inv_count`, `super_group_id`, `tax_inclusion`, `happyhour`, `tax_profile_id`, `track_86_count`, `price_tier_profile_id`, `no_discount`, `allow_ordertype_tax_exempt` FROM `menu_items` WHERE `category_id` = '".$data_obj->id."' AND `_deleted`!='1'".$order_by);
			if ($get_menu_items !== FALSE) {
				if (mysqli_num_rows($get_menu_items) > 0) {
					while ($row = mysqli_fetch_assoc($get_menu_items)) {
						$item_array[] = $row;
					}
				}
			} else {
				$response_array['json_status'] = "NoItems";
				$response_array['for_category'] = $data_obj->name;
			}
			$item_hhs = $o_happyHoursGetCategoryOrItem->get_all_items_from_category_happyhours($data_obj->id, (array)$data_obj, $item_array, TRUE);

			if (count($item_array) > 0) {

				for ($i = 0; $i < count($item_array); $i++) {

					$menu_item = $item_array[$i];
					$item_id = $menu_item['id'];

					if (isset($menu_item['name'])) {
						$menu_item['name'] = str_pad($menu_item['name'], 2, " ", STR_PAD_RIGHT);
					}
					if ($menu_item['printer']=="" || ($data_obj->printer=="" && $menu_item['printer']=="0")) {
						$menu_item['print'] = "0";
					}

					$image_path = "/mnt/poslavu-files/images/".$data_name."/items/".$menu_item['image'];
					if (!file_exists($image_path) || is_dir($image_path)) {
						$menu_item['image'] = "";
					}

					$menu_item['happyhour'] = (isset($item_hhs[$item_id]))?$item_hhs[$item_id]:"";

					$data_obj->item_order[] = $item_id;
					$send_item_array[$item_id] = $menu_item;
					$o_mod_list_id = $data_obj->modifier_list_id;
					if (!in_array($o_mod_list_id, $used_o_mod_list_ids) && $o_mod_list_id!=0) {
						$used_o_mod_list_ids[] = $o_mod_list_id;
					}
				}
			}
			$response_array['json_status'] = "success";
			$response_array['category'] = $data_obj;
			$response_array['items'] = $send_item_array;
			$response_array['modifier_lists'] = $used_o_mod_list_ids;
		} else {

			$response_array['json_status'] = "NoC";
		}

	} else {

		$response_array['json_status'] = "NoC";
	}

	return json_encode($response_array);
}

function loadModifiers($post_vars) {

	$response_array = array();

	switch ($post_vars['type']) {

		case "forced":

			$list_array = array();
			$list_array[] = array(
				"id"		=> "99999999",
				"title"		=> "place_holder",
				"type"		=> "choice",
				"modifiers"	=> array(
					array(
						"id"		=> "0",
						"title"		=> "MOD ERROR",
						"cost"		=> "0.00",
						"detour"	=> "0"
					)
				)
			);

			if (($get_forced_modifier_lists = lavu_query("SELECT `id`, `title`, `type`, `type_option` FROM `forced_modifier_lists` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $post_vars['menu_id'])) !== FALSE) {
				if (mysqli_num_rows($get_forced_modifier_lists) > 0) {

					$lists_array = array();
					$get_forced_modifiers = lavu_query("SELECT `id`, `list_id`, `title`, `cost`, `detour`, `extra`, `extra2`, `extra3`, `extra4`, `extra5` FROM `forced_modifiers` WHERE `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC");
					if ($get_forced_modifiers !== FALSE) {
						if (mysqli_num_rows($get_forced_modifiers) > 0) {
							while ($fmod = mysqli_fetch_assoc($get_forced_modifiers)) {
								$la_key = "l".$fmod['list_id'];
								if (!isset($lists_array[$la_key])) {
									$lists_array[$la_key] = array();
								}
								$lists_array[$la_key][] = array( "id"=>$fmod['id'], "title"=>$fmod['title'], "cost"=>$fmod['cost'], "detour"=>$fmod['detour'], "extra"=>$fmod['extra'], "extra2"=>$fmod['extra2'], "extra3"=>$fmod['extra3'], "extra4"=>$fmod['extra4'], "extra5"=>$fmod['extra5'] );
							}

							while ($fmod_list = mysqli_fetch_assoc($get_forced_modifier_lists)) {
								$la_key = "l".$fmod_list['id'];
								if (!isset($lists_array[$la_key])) {
									$lists_array[$la_key] = array();
								}
								$list_array[] = array( "id"=>$fmod_list['id'], "title"=>$fmod_list['title'], "type"=>$fmod_list['type'], "modifiers"=> $lists_array[$la_key] );
							}
							$response_array['json_status'] = "success";
							$response_array["forced_modifier_lists"] = $list_array;
						} 
					} else {
						$response_array['json_status'] = "NoForcedModifier";
					}
				}
				mysqli_free_result( $get_forced_modifier_lists );
			} else {
				$response_array['json_status'] = "NoForcedModifierList";
			}
			break;

		case "optional":

			$list_array = array();
			$list_ids = explode(",", $post_vars['list_ids']);
			$last_list_id = "";

			if (($get_modifiers = lavu_query("SELECT `id`, `title`, `cost`, `category` FROM `modifiers` WHERE `category` IN ('".implode("','", $list_ids)."') AND `_deleted` != '1' ORDER BY `category` ASC, `_order` ASC, `title` ASC")) !== FALSE) {
				if (mysqli_num_rows($get_modifiers) > 0) {

					$list_array = array();
					$mods = array();
					while ($mod = mysqli_fetch_assoc($get_modifiers)) {
						if ($mod['category']!=$last_list_id && $last_list_id!="") {
							$list_array[] = array( "id"=>$last_list_id, "modifiers"=>implode("|*|", $mods) );
							$mods = array();
						}
						$last_list_id = $mod['category'];
						$mods[] = $mod['id']."|o|".$mod['title']."|o|".$mod['cost'];
					}
					if ($last_list_id != "") {
						$list_array[] = array( "id"=>$last_list_id, "modifiers"=>implode("|*|", $mods) );
					}
					$response_array['json_status'] = "success";
					$response_array["modifier_lists"] = $list_array;
				}
				mysqli_free_result( $get_modifiers );
			} else {
				$response_array['json_status'] = "NoModifier";
			}
			break;

		default:

			$response_array['json_status'] = "InvalidType";

			break;
	}

	return json_encode($response_array);
}

function loadComponents($comp_pack, $activeModules, &$result) { // load components associated with assigned spin-off package

	global $company_info;
	global $location_info;
	global $data_name;

	$layout_ids_string = "";
	$component_layouts_string = "";
	$components_string = "";
	$order_addons_string = "";
	$init_comp = array("name"=>"");

	if ( ($get_layout_ids = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_packages` WHERE `id` = '[1]'", $comp_pack)) !== FALSE ){
		if (mysqli_num_rows($get_layout_ids) > 0) {
			$extract = mysqli_fetch_assoc($get_layout_ids);
			$layout_ids_array = explode(",", $extract['layout_ids']);
			$tab_ids = array();
			$order_addons = array();
			$use_order_addons = array();

			if (isset($extract['modules_required']) && !empty($extract['modules_required']) && !$activeModules->hasModule($extract['modules_required'])) {
				return "";
			}

			$add_ids = array();
			if (!empty($extract['order_addons']))
			{
				$oa = explode("|*|", $extract['order_addons']);
				for ($i = 0; $i < count($oa); $i++)
				{
					$this_addon = explode("|o|", $oa[$i]);
					$should_exclude_addon = (isset($location_info['disable_order_add_on_'.$this_addon[0]]))?$location_info['disable_order_add_on_'.$this_addon[0]]:"0";
					if ($should_exclude_addon == "0")
					{
						$add_ids[] = $this_addon[0];
						if ($this_addon[0]=="19" || $this_addon[0]=="30")
						{ // Customer AddOn or loyaltySwipe
							$this_addon[2] = (isset($location_info['require_customer_for_checkout']))?$location_info['require_customer_for_checkout']:$this_addon[2];
							$this_addon[3] = (isset($location_info['auto_launch_customer_selector']))?$location_info['auto_launch_customer_selector']:$this_addon[3];
							$order_addons[$this_addon[0]] = implode("|o|", $this_addon);
						}
						else if ($this_addon[0] * 1 >=90)
						{ // Additional, newer add-ons
							$order_addons[$this_addon[0]] = implode("|o|", $this_addon);
						}

					}
				}
				$layout_ids_array = array_merge($layout_ids_array, $add_ids);
			}

			if (!empty($extract['init_comp'])) {
				if( ($get_init_comp = mlavu_query("SELECT `name`, `filename` FROM `components` WHERE `id` = '[1]'", $extract['init_comp'])) !== FALSE ){
					if (mysqli_num_rows($get_init_comp)) {
						$init_comp = mysqli_fetch_assoc($get_init_comp);
					}
					mysqli_free_result( $get_init_comp );
				}
			}

			$component_layouts_array = array();
			$component_ids_array = array();
			$components_array = array();
			foreach ($layout_ids_array as $layout_id)
			{
				if ($layout_id == "")
				{
					error_log("skipping blank layout_id for ".$data_name." - comp_pack: ".$comp_pack." - order_addons: ".$extract['order_addons']);
					continue;
				}

				if ($layout_id == "0")
				{
					$tab_ids[] = "0";
					continue;
				}

				$get_layout_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_layouts` WHERE `id` = '[1]'", $layout_id);
				$extract = mysqli_fetch_assoc($get_layout_data);
				if ($company_info['dev'] == "1")
				{
					foreach (array("background", "c_ids", "c_xs", "c_ys", "c_ws", "c_hs", "c_zs", "c_types") as $field)
					{
						if (!empty($extract['dev_'.$field]))
						{
							$extract[$field] = $extract['dev_'.$field];
						}
					}
				}

				$skip_this = (isset($extract['modules_required']) && !empty($extract['modules_required']) && !$activeModules->hasModule($extract['modules_required']));
				if (!$skip_this)
				{
					if (!in_array($layout_id, $add_ids))
					{
						$tab_ids[] = $layout_id;
					}
					else
					{
						$use_order_addons[] = $order_addons[$layout_id];
					}

					$component_layouts_array[] = $extract;
					$these_component_ids = explode(",", $extract['c_ids']);
					foreach ($these_component_ids as $component_id)
					{
						if (!in_array($component_id, $component_ids_array))
						{
							$component_ids_array[] = $component_id;
							$get_component_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`components` WHERE `id` = '[1]'", $component_id);
							$extract2 = mysqli_fetch_assoc($get_component_data);
							$components_array[] = $extract2;
						}
					}
				}
			}

			$result['layout_ids']			= $tab_ids;
			$result['component_layouts']		= $component_layouts_array;
			$result['components']			= $components_array;
			$result['order_addons']			= $use_order_addons;
			$result['init_comp']				= $init_comp;
			$result['secondary_currency']	= getSecondaryCurrency();

		}

		mysqli_free_result( $get_layout_ids );

	} else {

		lavu_echo('{"json_status":"MissingCompPack"}');
	}
}

function shouldAllowTablesideCheckout($device_model) {

	if (strstr($device_model, "iPad")) {
		return "0";
	} else {
		return "1"; // allow for iPods and iPhones
	}
}

function fixForSmartPrinter($special_json) {

	$special_info = json_decode($special_json);
	if ($special_info->model == "TM-T88V-i") {
		$special_info->lavu_controller = "1";
	}

	return json_encode($special_info);
}

function getSupportedPrinters($loc_id, &$result) { // other location settings from `config` table also piggy back here

	$app_build = (isset($_REQUEST['B']))?$_REQUEST['B']:"";

	$supported_printers_id_array = array();
	$printer_redirects = new stdClass();
	$result['printers'] = array();

	$result['config_settings'] = array(
		"no_settings" => "none"
	);
	$result['terminal_list'] = array(
		array(
			"receipt" => "Receipt"
		)
	);
	$result['supported_printers'] = array(
		array (
			"no_printers" => "none"
		)
	);

	//LP-6899 query optimization
	if (($get_config_settings = lavu_query("SELECT * FROM `config` WHERE `location` = '[1]' AND `_deleted` != '1' AND type IN ('printer','printer_redirects','location_config_setting','admin_activity')", $loc_id)) !== FALSE) {

		if (mysqli_num_rows($get_config_settings) > 0) {

			$result['config_settings'] = array();
			$result['config_settings']['s3_uri'] = getenv('S3IMAGEURL');
			$result['terminal_list'] = array();
			$print_all_items = array();
			$result['customer_facing_display_image'] = getCustomerFacingDisplayDetails();
			$result['config_settings']['allow_tableside_checkout'] = shouldAllowTablesideCheckout($_REQUEST['model']);

			while ($extract_cs = mysqli_fetch_assoc($get_config_settings)) {

				$type = $extract_cs['type'];

				if ($type == "printer") {

					$p_name = strtolower($extract_cs['setting']);

					$result['config_settings'][$p_name.'-name']								= $extract_cs['value2'];
					$result['config_settings'][$p_name.'-ip']								= $extract_cs['value3'];
					$result['config_settings'][$p_name.'-port']								= $extract_cs['value5'];
					$result['config_settings'][$p_name.'_command_set']						= $extract_cs['value6'];
					$result['config_settings'][$p_name.'_cpl_standard']						= $extract_cs['value7'];
					$result['config_settings'][$p_name.'_cpl_emphasize']						= $extract_cs['value8'];
					$result['config_settings'][$p_name.'_lsf']								= $extract_cs['value9'];
					$result['config_settings'][$p_name.'_raster_capable']					= $extract_cs['value10'];
					$result['config_settings'][$p_name.'_perform_status_check']				= (strstr($extract_cs['setting'], "receipt")?"0":$extract_cs['value11']);
					$result['config_settings'][$p_name.'_use_etb']							= $extract_cs['value12'];
					$result['config_settings'][$p_name.'_skip_ping']							= $extract_cs['value13'];
					$result['config_settings'][$p_name.'_sleep_seconds']						= $extract_cs['value14'];
					$result['config_settings'][$p_name.'_beeper']							= $extract_cs['value15'];
					$result['config_settings'][$p_name.'_special_info']						= fixForSmartPrinter($extract_cs['value_long2']);
					$result['config_settings'][$p_name.'_print_individual_item_tickets']		= $extract_cs['value_long'];
					$result['config_settings'][$p_name.'_enable_dual_cash_drawer'] 	= $extract_cs['value17'];
					if($extract_cs['value16'] != 0){
						$print_all_items[] = $extract_cs['setting'];
					}

					$result['printers'][] = array(
						"name"							=> $extract_cs['value2'],
						"ip"							=> $extract_cs['value3'],
						"port"							=> $extract_cs['value5'],
						"command_set"					=> $extract_cs['value6'],
						"cpl_standard"					=> $extract_cs['value7'],
						"cpl_emphasize" 				=> $extract_cs['value8'],
						"lsf"							=> $extract_cs['value9'],
						"raster_capable"				=> $extract_cs['value10'],
						"perform_status_check"			=> (strstr($extract_cs['setting'], "receipt")?"0":$extract_cs['value11']),
						"use_etb"						=> $extract_cs['value12'],
						"skip_ping"						=> $extract_cs['value13'],
						"sleep_seconds"					=> $extract_cs['value14'],
						"beeper"						=> $extract_cs['value15'],
						"special_info"					=> fixForSmartPrinter($extract_cs['value_long2']),
						"print_individual_item_tickets"	=> $extract_cs['value_long'],
						"enable_dual_cash_drawer"		=> $extract_cs['value17']
					);

					if (!in_array($extract_cs['value6'], $supported_printers_id_array)) {
						$supported_printers_id_array[] = $extract_cs['value6'];
					}
					if (substr($extract_cs['setting'], 0, 7) == "receipt") {
						$result['terminal_list'][] = array(
							"setting"	=> $extract_cs['setting'],
							"value"		=> $extract_cs['value2']
						);
					}
				} else if ($type == "printer_redirects") {
					$printer_redirects->$extract_cs['id'] = array(
						"name"		=> $extract_cs['setting'],
						"redirects"	=> $extract_cs['value']
					);
				} else if ($type=="location_config_setting" || $type=="admin_activity") {
					$cs_key = $extract_cs['setting'];
					$cs_val = $extract_cs['value'];

					if ($cs_key=="dining_room_auto_refresh" && $cs_val>0 && appVersionCompareValue(reqvar("app_version", ""))<=30004) { // disable dining room refresh for single terminal locations
						$get_active_device_count = lavu_query("SELECT COUNT(`id`) FROM `devices` WHERE `loc_id` = '[1]' AND `inactive` = '0'", $loc_id);
						$dvc_count = mysqli_result($get_active_device_count, 0);
						if ($dvc_count <= 1) {
							global $data_name;
							error_log("disabling dining_room_auto_refresh (".$cs_val.") for ".$data_name);
							$cs_val = 0;
						}
					}

					$result['config_settings'][ $cs_key ] = $cs_val;
				}
			}
			$result['config_settings']['print_all_items'] = $print_all_items;
		}

		mysqli_free_result( $get_config_settings );
	}


	$result['printer_redirects'] = $printer_redirects;

	$mfilters = array();
	$mcount = 0;
	$spid_filter = "";
	foreach ($supported_printers_id_array as $spid) {
		$mcount++;
		$mfilters['p_'.$mcount] = $spid;
		$spid_filter .= " OR `id` = '[p_$mcount]'";
	}

	if (($get_supported_printers = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`supported_printers` WHERE `id` = '0'".$spid_filter, $mfilters)) !== FALSE) {
		if (mysqli_num_rows($get_supported_printers) > 0) {
			$result['supported_printers'] = array();
			while ($extract_sp = mysqli_fetch_object($get_supported_printers)) {
				$result['supported_printers'][] = $extract_sp;
			}
		}
		mysqli_free_result( $get_supported_printers );
	}
}

function getAvailableLanguagePacks(&$result)
{
	$language_pack_array = array();
	$language_pack_array = getRedisAvailableLanguagePacks();
	if (empty($language_pack_array)) {
		$get_language_packs = mlavu_query("SELECT `id`, `language` FROM `poslavu_MAIN_db`.`language_packs` WHERE `_deleted` = '0' ORDER BY `language` ASC");
		while ($lang_pack = mysqli_fetch_assoc($get_language_packs))
		{
			$language_pack_array[] = $lang_pack;
		}
		setRedisAvailableLanguagePacks($language_pack_array);
		}
	$result['available_language_packs'] = $language_pack_array;
}

function generateLanguagePack($loc_id, $pack_id, &$result, $apply_to_session=FALSE, $server_id = '')
{
	static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));

	$language_pack = array();
	// We also need english language records for tag, So that commented below block of code
	/*
	if ($pack_id == "1")
	{
		$language_pack = array(
			'STANDARD POSLAVU ENGLISH' => 'STANDARD POSLAVU ENGLISH'
		);
	}
	*/
	if ($pack_id == "0")
	{
		$language_pack = array(
			'CUSTOM LANGUAGE PACK' => 'CUSTOM LANGUAGE PACK'
		);
		$get_custom_pack = lavu_query("SELECT * FROM `custom_dictionary` WHERE `loc_id` = '[1]' AND (`used_by` = '' OR `used_by` = 'app' OR `used_by` = 'cp')", $loc_id);
		while ($extract = mysqli_fetch_assoc($get_custom_pack))
		{
			if ($extract['tag'] != '') {
				$key = $extract['tag'];
			} else {
				$key = str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['word_or_phrase']);
			}
			$translation = $extract['translation'];
			if ($extract['tag'] != '' && $translation == '') {
				$translation = $extract['word_or_phrase'];
			}
			$val = str_replace($jsonReplaces[0], $jsonReplaces[1], $translation);

			$language_pack[$key] = stripslashes($val);
		}
		//TODO: We will remove hard coded tag
		if (!isset($language_pack['POS4_UPGRADE_NOTIFICATION'])) {
			$tag = 'POS4_UPGRADE_NOTIFICATION';
			$get_language_name = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '1' AND `tag` = '[1]'", $tag);
			$extract = mysqli_fetch_assoc($get_language_name);
			$language_pack[$tag] = stripslashes($extract['word_or_phrase']);
		}
	}
	else
	{
		$get_language_name = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` = '[1]'", $pack_id);
		$extract = mysqli_fetch_assoc($get_language_name);
		$language_pack[ strtoupper($extract['language']).' LANGUAGE PACK' ] = strtoupper($extract['language']).' LANGUAGE PACK';
		$get_language_pack = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '[1]' AND (`used_by` = '' OR `used_by` = 'app' OR `used_by` = 'cp')", $pack_id);
		while ($extract = mysqli_fetch_assoc($get_language_pack))
		{
			if ($extract['tag'] != '') {
				$key = $extract['tag'];
			} else {
				$key = str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['word_or_phrase']);
			}
			$translation = $extract['translation'];
			if ($extract['tag'] != '' && $translation == '') {
				$translation = $extract['word_or_phrase'];
			}
			$val = str_replace($jsonReplaces[0], $jsonReplaces[1], $translation);

			$language_pack[$key] = stripslashes($val);
		}
	}

	if ($apply_to_session)
	{
		setLanguageDictionary($language_pack, $loc_id, 'app');
		setActiveLanguagePackId($pack_id, $server_id, $loc_id);
	}

	$result['language_pack'] = $language_pack;
}

function get_column_names($table_name) {

	$result = lavu_query("SHOW COLUMNS FROM `$table_name`");
	$column_names = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$column_names[] = $row['Field'];
	}

	return $column_names;
}

function create_field_list($table_name, $exclude) {

$column_names = get_column_names($table_name);
$field_list = "";

	foreach($column_names as $name) {
		if (!in_array($name, $exclude)) {
			if($field_list == "")
				$field_list = "`$name`";
			else
				$field_list .= ", `$name`";
			}
		}

return $field_list;
}

function deviceHoldingThisOrder($loc_id, $order_id) {

	$device_id = false;

	if ($order_id!="" && $order_id!="0") {
		if( ($check_devices = lavu_query("SELECT `UUID` FROM `devices` WHERE `loc_id` = '[1]' AND `holding_order_id` = '[2]' AND `inactive` = '0'", $loc_id, $order_id)) !== FALSE ){
			if (mysqli_num_rows($check_devices) > 0) {
				$info = mysqli_fetch_assoc($check_devices);
				$device_id = $info['UUID'];
			}
			mysqli_free_result( $check_devices );
		}
	}

	return $device_id;
}

function setOrderHeldByDevice($loc_id, $order_id, $device_id) {

	if ($order_id == "0") return true;
	else {
		$update = lavu_query("UPDATE `devices` SET `holding_order_id` = '' WHERE `loc_id` = '[1]' AND `holding_order_id` = '[2]'", $loc_id, $order_id);
		if ($update) {
			$update = lavu_query("UPDATE `devices` SET `loc_id` = '[1]', `holding_order_id` = '[2]' WHERE `UUID` = '[3]'", $loc_id, $order_id, $device_id);
			return $update;
		} else return false;
	}
}

function build_item_vars($details, $insert=false) {

	global $decimal_places;

	// item_details indexes
	//
	//  0 - discount amount
	//  1 - discount value
	//  2 - discount type
	//  3 - after discount
	//  4 - tax amount
	//  5 - total with tax
	//  6 - included tax rate
	//  7 - included tax amount
	//  8 - tax rate 1
	//  9 - tax amount 1
	// 10 - tax rate 2
	// 11 - tax amount 2
	// 12 - tax rate 3
	// 13 - tax amount 3
	// 14 - tax subtotal 1
	// 15 - tax subtotal 2
	// 16 - tax subtotal 3
	// 17 - void
	// 18 - quantity
	// 19 - discount id
	// 20 - item discount id
	// 21 - item discount shorthand
	// 22 - item discount value
	// 23 - item discount type
	// 24 - item discount amount
	// 25 - split factor
	// 26 - tax name 1
	// 27 - tax name 2
	// 28 - tax name 3
	// 29 - tax exempt
	// 30 - exemption id
	// 31 - exemption name
	// 32 - included tax name
	// 33 - hidden data 6
	// 34 - options (forced modifiers)
	// 35 - idiscount info
	// 36 - icid
	// 37 - check
	// ---------- 2.2.1+ ----------
	// 38 - item
	// 39 - price
	// 40 - special
	// 41 - print
	// 42 - modifiy price
	// 43 - seat
	// 44 - item_id
	// 45 - printer
	// 46 - apply_taxrate
	// 47 - custom_taxrate
	// 48 - modifier_list_id
	// 49 - forced_modifier_group_id
	// 50 - forced_modifiers_price
	// 51 - course
	// 52 - open_item
	// 53 - allow_deposit
	// 54 - deposit_info
	// 55 - device_time
	// 56 - notes
	// 57 - hidden_data1
	// 58 - hidden_data2
	// 59 - hidden_data3
	// 60 - hidden_data4
	// 61 - tax_inclusion
	// 62 - sent
	// 63 - checked_out
	// 64 - hidden_data5
	// 65 - price_override
	// 66 - original_price
	// 67 - override_id
	// 68 - category_id
	// 69 - ioid
	// 70 - after_gratuity
	// ---------- 2.3.5+ ----------
	// 71 - tax_rate4
	// 72 - tax4
	// 73 - tax_subtotal4
	// 74 - tax_name4
	// 75 - tax_rate5
	// 76 - tax5
	// 77 - tax_subtotal5
	// 78 - tax_name5
	// 79 - tax_freeze
	// 80 - super_group_id

	$item_vars = array();
	$item_vars['discount_amount'] = priceFormat(decimalize($details[0]));
	$item_vars['discount_value'] = decimalize($details[1]);
	$item_vars['discount_type'] = $details[2];
	$item_vars['after_discount'] = decimalize($details[3]);
	$item_vars['tax_amount'] = priceFormat(max(0, decimalize($details[4])));
	$item_vars['total_with_tax'] = priceFormat(decimalize($details[5]));
	$item_vars['itax_rate'] = decimalize($details[6]);
	$item_vars['itax'] = priceFormat(decimalize($details[7]));
	$item_vars['tax_rate1'] = decimalize($details[8]);
	$item_vars['tax1'] = priceFormat(decimalize($details[9]));
	$item_vars['tax_rate2'] = decimalize($details[10]);
	$item_vars['tax2'] = priceFormat(decimalize($details[11]));
	$item_vars['tax_rate3'] = decimalize($details[12]);
	$item_vars['tax3'] = priceFormat(decimalize($details[13]));
	$item_vars['tax_subtotal1'] = priceFormat(decimalize($details[14]));
	$item_vars['tax_subtotal2'] = priceFormat(decimalize($details[15]));
	$item_vars['tax_subtotal3'] = priceFormat(decimalize($details[16]));
	$item_vars['void'] = decimalize($details[17]);
	$item_vars['quantity'] = decimalize($details[18]);
	$item_vars['discount_id'] = $details[19];
	$item_vars['idiscount_id'] = $details[20];
	$item_vars['idiscount_sh'] = $details[21];
	$item_vars['idiscount_value'] = decimalize($details[22]);
	$item_vars['idiscount_type'] = $details[23];
	$item_vars['idiscount_amount'] = priceFormat(decimalize($details[24]));
	$item_vars['split_factor'] = $details[25];
	$item_vars['tax_name1'] = $details[26];
	$item_vars['tax_name2'] = $details[27];
	$item_vars['tax_name3'] = $details[28];
	$item_vars['tax_exempt'] = $details[29];
	$item_vars['exemption_id'] = $details[30];
	$item_vars['exemption_name'] = $details[31];
	$item_vars['itax_name'] = $details[32];
	$item_vars['hidden_data6'] = (isset($details[33])?$details[33]:"");
	if (isset($details[34])) $item_vars['options'] = str_replace("RACER(colon)", "RACER:", $details[34]);
	$item_vars['idiscount_info'] = (isset($details[35])?$details[35]:"");
	if (isset($details[36])) $item_vars['icid'] = $details[36];
	if (isset($details[37])) $item_vars['check'] = $details[37];
	if (count($details) >= 71) {
		$item_vars['item'] = $details[38];
		$item_vars['price'] = $details[39];
		$item_vars['special'] = $details[40];
		$item_vars['print'] = $details[41];
		$item_vars['modify_price'] = decimalize($details[42]);
		$item_vars['seat'] = $details[43];
		$item_vars['item_id'] = $details[44];
		$item_vars['printer'] = $details[45];
		$item_vars['apply_taxrate'] = $details[46];
		$item_vars['custom_taxrate'] = $details[47];
		$item_vars['modifier_list_id'] = $details[48];
		$item_vars['forced_modifier_group_id'] = $details[49];
		$item_vars['forced_modifiers_price'] = decimalize($details[50]);
		$item_vars['course'] = $details[51];
		$item_vars['open_item'] = $details[52];
		$item_vars['subtotal'] = ($item_vars['quantity'] * $item_vars['price']);
		$item_vars['allow_deposit'] = $details[53];
		$item_vars['deposit_info'] = $details[54];
		$item_vars['device_time'] = $details[55];
		$item_vars['notes'] = $details[56];
		$item_vars['hidden_data1'] = $details[57];
		$item_vars['hidden_data2'] = $details[58];
		$item_vars['hidden_data3'] = $details[59];
		$item_vars['hidden_data4'] = $details[60];
		$item_vars['tax_inclusion'] = $details[61];
		$item_vars['sent'] = $details[62];
		if ($insert) $item_vars['checked_out'] = $details[63];
		$item_vars['hidden_data5'] = $details[64];
		$item_vars['price_override'] = $details[65];
		$item_vars['original_price'] = $details[66];
		$item_vars['override_id'] = $details[67];
		$item_vars['category_id'] = $details[68];
		$item_vars['ioid'] = $details[69];
		$item_vars['after_gratuity'] = decimalize($details[70]);
	}
	if (count($details) >= 79) {
		$item_vars['tax_rate4'] = decimalize($details[71]);
		$item_vars['tax4'] = decimalize($details[72]);
		$item_vars['tax_subtotal4'] = decimalize($details[73]);
		$item_vars['tax_name4'] = $details[74];
		$item_vars['tax_rate5'] = decimalize($details[75]);
		$item_vars['tax5'] = decimalize($details[76]);
		$item_vars['tax_subtotal5'] = decimalize($details[77]);
		$item_vars['tax_name5'] = $details[78];
	}

	return $item_vars;
}

function update_order_item_details($item_details, $order_id, $loc_id) {

	global $decimal_places;

	$item_details_array = explode("|*|", $item_details);

	$reset_last_mod_device = false;
	$used_icids = array();

	$row = 0;
	$can_perform_inserts = false;

	if( ($get_order_contents = lavu_query("SELECT `id` FROM `order_contents` WHERE `order_id` = '[1]' AND `loc_id` = '[2]' AND `item` != 'SENDPOINT' ORDER BY `id` ASC", $order_id, $loc_id)) !== FALSE ){
		if (mysqli_num_rows($get_order_contents) > 0) {
			while ($order_contents_info = mysqli_fetch_assoc($get_order_contents)) {
				if (isset($item_details_array[$row])) {
					$delimiter = ":";
					if (strstr($item_details_array[$row], "|o|")) $delimiter = "|o|";
					$details = explode($delimiter, $item_details_array[$row]);
					if (count($details) >= 70) $can_perform_inserts = true;
					$item_vars = build_item_vars($details, false);
					$use_icid = $item_vars['icid'];
					while (in_array($use_icid, $used_icids)) {
						$reset_last_mod_device = true;
						$use_icid = newInternalID();
					}
					$used_icids[] = $use_icid;
					$item_vars['icid'] = $use_icid;
					$keys = array_keys($item_vars);
					$q_update = "";
					foreach ($keys as $key) {
						if ($q_update != "") $q_update .= ", ";
						$q_update .= "`$key` = '[$key]'";
					}
					$item_vars['id'] = $order_contents_info['id'];
					$update_item = lavu_query("UPDATE `order_contents` SET $q_update WHERE `id` = '[id]'", $item_vars);
					$row++;
				}
			}
		}
		mysqli_free_result( $get_order_contents );
	}

	if ($can_perform_inserts) {
		for ($i = $row; $i < count($item_details_array); $i++) {
			$delimiter = ":";
			if (strstr($item_details_array[$i], "|o|")) $delimiter = "|o|";
			$details = explode($delimiter, $item_details_array[$i]);
			$item_vars = build_item_vars($details, true);
			$use_icid = $item_vars['icid'];
			while (in_array($use_icid, $used_icids)) {
				$reset_last_mod_device = true;
				$use_icid = newInternalID();
			}
			$used_icids[] = $use_icid;
			$item_vars['icid'] = $use_icid;
			$item_vars['loc_id'] = $loc_id;
			$item_vars['order_id'] = $order_id;
			$q_fields = "";
			$q_values = "";
			$keys = array_keys($item_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$insert = lavu_query("INSERT INTO `order_contents` ($q_fields) VALUES ($q_values)", $item_vars);
		}
	}

	if ($reset_last_mod_device) {

		$last_mod_ts = time();
		$last_modified = determineDeviceTime($location_info, $_REQUEST);

		$update_order = lavu_query("UPDATE `orders` SET `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'JC FUNCTIONS' WHERE `order_id` = '[3]' AND `location_id` = '[4]'", $last_mod_ts, $last_modified, $order_id, $loc_id);
	}
}

function get_loyaltree_purchase_info($loc_id, $check, $order_id, $email=false) {

	require_once(dirname(__FILE__).'/../../../inc/loyalty/loyaltree/LoyalTreeIntegration.php');
	LoyalTreeIntegration::loyalTreeDebug("get_loyaltree_purchase_info");  // had $parent_process but was an undefined var

	global $company_id;
	global $location_info;
	global $data_name;
	global $device_time;
	global $post_vars;
	global $data_syncer;
	global $decimal_places;
	global $smallest_money;
	global $loyaltree_debug;

	$loyaltree_debug = (locationSetting("loyaltree_debug", 0) == "1");

	$chain_id = $location_info['loyaltree_chain_id'];
	$loyaltree_id = $location_info['loyaltree_id'];
	if ($chain_id=="" && $loyaltree_id!="")
	{
		$chain_id = "0";
	}

	$split_check_details = lavu_query("SELECT `id`, `loyaltree_info` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $check);

	if (mysqli_num_rows($split_check_details))
	{
		$split_check_detail = mysqli_fetch_assoc($split_check_details);

		if (!empty($split_check_detail['loyaltree_info']))
		{
			$loyaltree_info = cleanJSON($split_check_detail['loyaltree_info']);

			LoyalTreeIntegration::loyalTreeDebug("get_loyaltree_purchase_info() found loyaltree_info - ".$loyaltree_info);

			return $loyaltree_info;
		}
	}

	$order_summary = reqvar('order_summary', '');
	if (function_exists('json_decode'))
	{
		$order_summary = json_decode($order_summary, true);
	}

	// TO DO : add support to LoyalTreeIntegration for skipping db look-ups for "payments" when data is passed in
	$args = array(
		'action'        => 'purchase',
		'dataname'      => $data_name,
		'restaurantid'  => $company_id,
		'order_id'      => $order_id,
		'loc_id'        => $loc_id,
		'check'         => $check,
		'email'         => (empty($email) ? '' : $email),
		'app'           => (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP),
		'send_async'    => (isset($location_info['loyaltree_app']) && $location_info['loyaltree_app'] == LoyalTreeConfig::WHITELABEL_APP),
		'text'          => (isset($location_info['loyaltree_text']) && !empty($location_info['loyaltree_text'])) ? $location_info['loyaltree_text'] : '',
		'time_zone'     => $location_info['timezone'],
		'businessid'    => $location_info['loyaltree_id'],
		'chainid'       => $location_info['loyaltree_chain_id'],
		'storeid'       => $company_id.$location_info['id'],
		'transactionid' => $order_id."-".$check,
		'device_time'   => $device_time,
		'ioid'          => (isset($post_vars['ioid']) ? $post_vars['ioid'] : false),
		'items'         => $order_summary['items'],
		'payments'      => $order_summary['payments'],
		'serverid'      => $order_summary['serverid'],
		'table'         => $order_summary['table'],
		'guest'         => $order_summary['guest'],
		'totalamount'   => $order_summary['totalamount'],
	);

	$response = LoyalTreeIntegration::purchase($args);

	if (!strstr($response, '"json_status":"error"'))
	{
		$args['scd_id'] = $split_check_detail['id'];
		$args['loyaltree_info'] = $response;

		if (!empty($args['scd_id']))
		{
			$save_response = lavu_query("UPDATE `split_check_details` SET `loyaltree_info` = '[loyaltree_info]' WHERE `id` = '[scd_id]'", $args);

			$ioid = $args['ioid'];
			if (!empty($ioid))
			{
				$update_order = lavu_query("UPDATE `orders` SET `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = '[3]' WHERE `ioid` = '[4]'", time(), $args['device_time'], reqvar("UUID", "RAR"), $ioid);
			}

			if (is_a($data_syncer, 'LdsDataSyncer'))
			{
				$data_syncer->addInternalUpdate($args['ioid'], 'check_details', 'loyaltree_info', $response, 'check', $args['check']);
			}
		}
	}

	return cleanJSON($response);
}

/**
 * Ensure the order_summary array has the values necessary to process LoyalTree
 */
function validateOrderSummary($order_summary, $data)
{
	global $data_name;

	if ($order_summary['loyaltree_info'] == "1")
	{
		return true;
	}

	foreach (array('loyaltree_info', 'serverid', 'table', 'guest', 'totalamount', 'items', 'payments') as $key)
	{
		if (!isset($order_summary[$key]))
		{
			return false;
		}
	}

	foreach (array('items', 'payments') as $key)
	{
		if (!is_array($order_summary[$key]))
		{
			return false;
		}
	}

	return true;
}

function processLoyalTree($data, $items, $payments, $email, $use_order_summary)
{
	global $data_syncer;

	require_once(dirname(__FILE__).'/../../../inc/loyalty/loyaltree/LoyalTreeIntegration.php');

	LoyalTreeIntegration::loyalTreeDebug("processLoyalTree - using order_summary? ".($use_order_summary?"YES":"NO"));

	foreach ($payments as $i => $payment)
	{
		$payments[$i]['current'] = "1";
	}

	$data['action']   = 'purchase';
	$data['loc_id']   = $loc_id;
	$data['order_id'] = $order_id;
	$data['app']      = (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP);
	$data['text']     = (isset($location_info['loyaltree_text']) && !empty($location_info['loyaltree_text'])) ? $location_info['loyaltree_text'] : '';

	$response = LoyalTreeIntegration::purchase($data);

	if (!$use_order_summary && !strstr($response, '"json_status":"error"'))
	{
		if (!empty($data['scd_id']))
		{
			$save_response = lavu_query("UPDATE `split_check_details` SET `loyaltree_info` = '[1]' WHERE `id` = '[2]'", $response, $data['scd_id']);

			$ioid = $data['ioid'];
			if (!empty($ioid))
			{
				$update_order = lavu_query("UPDATE `orders` SET `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = '[3]' WHERE `ioid` = '[4]'", time(), $data['device_time'], reqvar("UUID", "RAR"), $ioid);
			}

			if (is_a($data_syncer, 'LdsDataSyncer'))
			{
				$data_syncer->addInternalUpdate($data['ioid'], 'check_details', 'loyaltree_info', $response, 'check', $data['check']);
			}
		}
	}

	return $response;
}

function replaceNullWithEmptyString($var)
{
	if ($var == NULL)
	{
		return "";
	}
	else
	{
		return $var;
	}
}

function cleanJSON($json_string)
{
	/**To avoid POS crash issue due to null value in json string
	Replace <null> with ""
	Replace "" with \"\"
	**/
	if($json_string){
		$json_string = preg_replace("/\"\<null\>\"/", '""', $json_string);
		$json_string = preg_replace("/:NULL|:null/", ':""', $json_string);
		$json_string = str_replace('\":""', '\":\"\"', $json_string);
	}

	$search_array = array();
	$replace_array = array();
	for ($c = 0; $c < 32; $c++)
	{
		if ($c != 13)
		{
			$search_array[] = chr($c);
			if ($c == 0) $replace_array[] = "";
			else $replace_array[] = sprintf('\u00%02X', dechex($c));
		}
		/*if ($c==10) {
			$search_array[] = chr($c);
			$replace_array[] = "[--CR--]";
		}*/
	}

	$str = str_replace($search_array, $replace_array, $json_string);

	// seek out and remove invalid unicode sequences
	$str = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
		'|(?<=^|[\x00-\x7F])[\x80-\xBF]+'.
		'|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
		'|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
		'|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/',
		'', $str);

	$str = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
		'|\xED[\xA0-\xBF][\x80-\xBF]/S','', $str);

	return $str;
}

function closeOrder($post_vars) // performs inventory adjustments and other special functions
{
	// mode == close_order

	global $data_name;
	global $location_info;
	global $decimal_places;
	global $smallest_money;

	$order_info = array();
	if (($get_order_info = lavu_query("SELECT * FROM `orders` WHERE `ioid` = '[1]'", $post_vars['ioid'])) !== FALSE)
	{
		if (mysqli_num_rows($get_order_info) > 0)
		{
			$order_info = mysqli_fetch_assoc($get_order_info);
		}
		mysqli_free_result( $get_order_info );
	}

	if (count($order_info) == 0) // can't properly perform order closure if order record hasn't made it to the server
	{
		return "0";
	}

		//  0 - closed time
		//  1 - discount
		//  2 - discount label
		//  3 - cash paid
		//  4 - card paid
		//  5 - alt paid
		//  6 - server name
		//  7 - tax amount
		//  8 - total
		//  9 - PIN used
		// 10 - tax rate // empty as of 4/09/2013
		// 11 - send to email
		// 12 - gratuity
		// 13 - cashier ID
		// 14 - paid by gift certificate
		// 15 - gratuity percent
		// 16 - refund amount
		// 17 - refund gift certificate amount
		// 18 - reopened datetime
		// 19 - subtotal
		// 20 - deposit status
		// 21 - active register
		// 22 - discount value
		// 23 - discount type
		// 24 - refund cc amount
		// 25 - rounded amount
		// 26 - auto gratuity is taxed
		// 27 - active register name
		// 28 - tax exempt
		// 29 - exemption id
		// 30 - exemption name
		// 31 - alt refund amount

	// build close_info from order_info to be backward compatible with process_order_item() functions
	$close_info = array(
		$order_info['closed'], // 0
		$order_info['discount'],
		$order_info['discount_sh'],
		$order_info['cash_paid'],
		$order_info['card_paid'],
		$order_info['alt_paid'],
		$order_info['cashier'],
		$order_info['tax'],
		$order_info['total'],
		"",
		"", // 10
		$order_info['email'],
		$order_info['gratuity'],
		$order_info['cashier_id'],
		$order_info['gift_certificate'],
		$order_info['gratuity_percent'],
		$order_info['refunded'],
		$order_info['refunded_gc'],
		$order_info['reopened_datetime'],
		$order_info['subtotal'],
		$order_info['deposit_status'], // 20
		$order_info['register'],
		$order_info['discount_value'],
		$order_info['discount_type'],
		$order_info['refunded_cc'],
		$order_info['rounding_amount'],
		$order_info['auto_gratuity_is_taxed'],
		$order_info['register_name'],
		$order_info['tax_exempt'],
		$order_info['exemption_id'],
		$order_info['exemption_name'], // 30
		$order_info['alt_refunded']
	);

	$deposit_order_ids = array();
	$total_deposit_amount = 0;

	$cpc = $location_info['component_package_code'];
	$poi_path = dirname(__FILE__)."/../../components/".$cpc."/lib/process_order_item.php";
	if (!empty($cpc) && file_exists($poi_path))
	{
		require_once($poi_path);
	}

	$check_count = (int)$order_info['no_of_checks'];
	$order_contents_by_check = array();
	for ($c = 1; $c <= $check_count; $c++)
	{
		$order_contents_by_check[$c] = array();
	}

	$newInventoryConfigured = false;
	$migrationStatus = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'INVENTORY_MIGRATION_STATUS'");
	$migrationResult = mysqli_fetch_assoc($migrationStatus);
	if($migrationResult['value'] != "Legacy"){
        $newInventoryConfigured = true;
    }
    $timeZone = sessvar('location_info')['timezone'];
    if (!empty($timeZone)) {
	    $getDateTimeFromTZ = new DateTimeZone($timeZone);
	    $currDateTime = new DateTime(NULL, $getDateTimeFromTZ);
	    $dateTimeStr = $currDateTime->format("Y-m-d H:i:s");
    } else {
		$dateTimeStr = date("Y-m-d H:i:s");
    }

	if (($get_order_contents = lavu_query("SELECT `order_contents`.`total_with_tax` AS `total_with_tax`, `order_contents`.`check` AS `check`, `order_contents`.`idiscount_info` AS `idiscount_info`, `order_contents`.`id` AS `content_id`, `order_contents`.`icid` AS `icid`, `order_contents`.`checked_out` AS `checked_out`, `order_contents`.`item_id` AS item_id, `order_contents`.`subtotal_with_mods` as subtotal_with_mods, `order_contents`.`allow_deposit` as allow_deposit, `order_contents`.`deposit_info` as deposit_info, `order_contents`.`item` as item, `order_contents`.`options` as options, `order_contents`.`quantity` as quantity, `menu_items`.`hidden_value` as hidden_value, `menu_items`.`hidden_value2` as hidden_value2 FROM `order_contents` LEFT JOIN `menu_items` ON `menu_items`.`id` = `order_contents`.`item_id` WHERE `order_contents`.`ioid` = '[1]'", $post_vars['ioid'])) !== FALSE)
	{
        $inventoryItemsQuantitys = array();
		if (mysqli_num_rows($get_order_contents) > 0)
		{
			$should_update_deposit_status = 0;
			$icids = array();
			$item_ids = array();
			$item_quantity = array();
			while ($extract = mysqli_fetch_assoc($get_order_contents))
			{
				$icid = $extract['icid'];
				// Avoid to add quantity of ingredients which is currently deducted, So storing all icid in array.
				$icids[$icid] = $icid;
				if ($extract['item']!='SENDPOINT' && $extract['item_id']!='0')
				{
					if ($extract['check'] == "0")
					{
						foreach ($order_contents_by_check as $index => $ewh)
						{
							$order_contents_by_check[$index][] = $extract;
						}
					}
					else
					{
						$order_contents_by_check[$extract['check']][] = $extract;
					}

					$item_qty = decimalize($extract['quantity']);
					$item_id = $extract['item_id'];
					$item_ids[$item_id] = $item_id;
                    $item_quantity[$item_id] += $item_qty;

					if ($extract['checked_out'] < $item_qty) {
                        $set_checked_out = $item_qty;
                        $item_qty -= $extract['checked_out'];
                        if (!$newInventoryConfigured) {
                            if (($get_item_info = lavu_query("SELECT `ingredients` FROM `menu_items` WHERE `id` = '$item_id'")) !== FALSE) {
                                if (mysqli_num_rows($get_item_info) > 0) {
                                    $usage_counts = array();
                                    $item_info = mysqli_fetch_assoc($get_item_info);
                                    if ($item_info['ingredients'] != "") {
                                        $ing_list = explode(",", $item_info['ingredients']);
                                        for ($i = 0; $i < count($ing_list); $i++) {
                                            $ing_info = explode("x", $ing_list[$i]);
                                            $ing_id = trim($ing_info[0]);
                                            $ing_precision = 0;
                                            if ($ing_id > 0) {
                                                $ing_qty = 1;
                                                if (count($ing_info) > 1) {
                                                    $ing_qty = trim($ing_info[1]);
                                                }
                                                if (array_key_exists($ing_id, $usage_counts)) {
                                                    $usage_counts[$ing_id] += ($ing_qty * $item_qty);
                                                } else {
                                                    $usage_counts[$ing_id] = ($ing_qty * $item_qty);
                                                }
                                                $decimal_char = $location_info['decimal_char'];
                                                $decimal_char = ($decimal_char == '') ? '.' : $decimal_char;
                                                if (strpos((string)$ing_qty, $decimal_char) !== FALSE) {
                                                    $ing_precision = strlen(substr(strrchr((string)$ing_qty, $decimal_char), 1));
                                                }
                                            }
                                        }
                                    }

                                    if (($get_mods_used = lavu_query("SELECT `mod_id`, `qty`, `type` FROM `modifiers_used` WHERE `ioid` = '[1]' AND `icid` = '[2]'", $post_vars['ioid'], $icid)) !== FALSE) {
                                        if (mysqli_num_rows($get_mods_used) > 0) {
                                            while ($mod_used = mysqli_fetch_assoc($get_mods_used)) {
                                                $mod_table = "forced_modifiers";
                                                if ($mod_used['type'] == "optional") {
                                                    $mod_table = "modifiers";
                                                }

                                                if (($get_mod_info = lavu_query("SELECT `ingredients` FROM `$mod_table` WHERE `id` = '[1]'", $mod_used['mod_id'])) !== FALSE) {
                                                    if (mysqli_num_rows($get_mod_info) > 0) {
                                                        $mod_info = mysqli_fetch_assoc($get_mod_info);
                                                        if ($mod_info['ingredients'] != "") {
                                                            $m_ing_list = explode(",", $mod_info['ingredients']);
                                                            for ($m = 0; $m < count($m_ing_list); $m++) {
                                                                $m_ing_info = explode("x", $m_ing_list[$m]);
                                                                $m_ing_id = trim($m_ing_info[0]);
                                                                if ($m_ing_id > 0) {
                                                                    $m_ing_qty = 1;
                                                                    if (count($m_ing_info) > 1) {
                                                                        $m_ing_qty = trim($m_ing_info[1]);
                                                                    }
                                                                    if (array_key_exists($m_ing_id, $usage_counts)) {
                                                                        $usage_counts[$m_ing_id] += ($m_ing_qty * $mod_used['qty']);
                                                                    } else {
                                                                        $usage_counts[$m_ing_id] = ($m_ing_qty * $mod_used['qty']);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    mysqli_free_result($get_mod_info);
                                                }
                                            }
                                        }

                                        mysqli_free_result($get_mods_used);
                                    }

                                    $ing_ids = array_keys($usage_counts);
                                    foreach ($ing_ids as $ing_id) {
                                        $usage_qty = 0;
                                        $usage_id = false;
                                        if (($get_prior_usage = lavu_query("SELECT * FROM `ingredient_usage` WHERE `loc_id` = '[1]' AND `orderid` = '[2]' AND `itemid` = '[3]' AND `ingredientid` = '[4]'  AND (content_id = '[5]' OR (`icid` = '[6]' AND `icid` != ''))", $post_vars['loc_id'], $post_vars['order_id'], $item_id, $ing_id, $extract['content_id'], $icid)) !== FALSE) {
                                            if ($get_prior_usage && mysqli_num_rows($get_prior_usage) > 0) {
                                                $usage_info = mysqli_fetch_assoc($get_prior_usage);
                                                $usage_qty = $usage_info['qty'];
                                                $usage_id = $usage_info['id'];
                                            }

                                            mysqli_free_result($get_prior_usage);
                                        }

                                        $used_qty = ($usage_counts[$ing_id] - $usage_qty);
                                        if ($used_qty != 0) {
                                            $s_rndstr_start = ($ing_precision > 0) ? "ROUND" : "";
                                            $s_rndstr_end = ($ing_precision > 0) ? ", " . $ing_precision : "";
                                            lavu_query("UPDATE `ingredients` SET `qty` = {$s_rndstr_start}(`qty` - ($used_qty * 1){$s_rndstr_end}) WHERE `id` = '[1]'", $ing_id);
                                            if ($usage_id) {
                                                lavu_query("UPDATE `ingredient_usage` SET `qty` = {$s_rndstr_start}(`qty` + ($used_qty * 1){$s_rndstr_end}) where `id` = '[1]'", $usage_id);
                                            } else {
                                                $unit_cost = "";
                                                if (($get_unit_cost = lavu_query("SELECT `cost` FROM `ingredients` WHERE `id` = '[1]'", $ing_id)) !== FALSE) {
                                                    if (mysqli_num_rows($get_unit_cost) > 0) {
                                                        $uc_info = mysqli_fetch_assoc($get_unit_cost);
                                                        $unit_cost = $uc_info['cost'];
                                                    }

                                                    mysqli_free_result($get_unit_cost);
                                                }

                                                $q_fields = "";
                                                $q_values = "";
                                                $i_vars = array(
                                                    'ts' => time(),
                                                    'date' => $dateTimeStr,
                                                    'orderid' => $post_vars['order_id'],
                                                    'itemid' => $item_id,
                                                    'ingredientid' => $ing_id,
                                                    'qty' => $used_qty,
                                                    'loc_id' => $post_vars['loc_id'],
                                                    'cost' => $unit_cost,
                                                    'content_id' => $extract['content_id'],
                                                    'icid' => $icid,
                                                );
                                                $keys = array_keys($i_vars);
                                                foreach ($keys as $key) {
                                                    if ($q_fields != "") {
                                                        $q_fields .= ", ";
                                                        $q_values .= ", ";
                                                    }
                                                    $q_fields .= "`$key`";
                                                    $q_values .= "'[$key]'";
                                                }
                                                lavu_query("INSERT INTO `ingredient_usage` ({$q_fields}, `server_time`) VALUES ({$q_values}, now())", $i_vars);
                                            }
                                        }
                                    }
                                }

                                mysqli_free_result($get_item_info);
                            }
                        }

						if (!empty($cpc) && function_exists('process_order_item'))
						{
							process_order_item($post_vars['loc_id'], $post_vars['order_id'], array_merge($extract, array("order_customer_id"=>$order_info['original_id'])), $close_info, $data_name);
						}
					}
					else
					{
						$set_checked_out = $extract['checked_out'];
					}

					if (($extract['allow_deposit']=="1" && $post_vars['deposit_status']!="1") || $post_vars['deposit_status']=="4")
					{
						$should_update_deposit_status = 2;
						$total_deposit_amount = ($total_deposit_amount + $extract['total_with_tax']);
					}
					else if ($extract['allow_deposit'] == "1")
					{
						$should_update_deposit_status = 3;
					}

					$update_item = lavu_query("UPDATE `order_contents` SET `checked_out` = '[1]' WHERE `id`  = '[2]'", $set_checked_out, $extract['content_id']);
				}
			}
			// delete any extra ingredients that still appear in the table, but are no longer tied to the order.
            if(!$newInventoryConfigured) {
                $extra_ingredients = lavu_query("SELECT * FROM `ingredient_usage` WHERE `loc_id` = '[1]' AND `orderid` ='[2]' AND `itemid` IN ('" . implode("', '", $item_ids) . "') AND `icid` NOT IN ('" . implode("', '", $icids) . "')", $post_vars['loc_id'], $post_vars['order_id']);
                if ($extra_ingredients) {
                    while ($ingredient_usage = mysqli_fetch_assoc($extra_ingredients)) {
                        lavu_query("UPDATE `ingredients` SET `qty` = (`qty` + '[1]') WHERE `id` = '[2]'", $ingredient_usage['qty'], $ingredient_usage['ingredientid']);
                    }
                }
            }
		}

		mysqli_free_result( $get_order_contents );
	}

	// For offline mode Inventory
    if ($newInventoryConfigured) {
        require_once(dirname(__FILE__) . '/../../cp/resources/inventory_functions.php');
	    $offlineInventorySync = false;
        if (($get_inventory_action_info = lavu_query("SELECT * FROM `action_log` WHERE `ioid` = '[1]' AND `action` = 'Inventory Deliver Order'", $post_vars['ioid'])) !== FALSE)
        {
            if (mysqli_num_rows($get_inventory_action_info) == 0)
            {
                if (($get_inventory_action_info = lavu_query("SELECT * FROM `action_log` WHERE `ioid` = '[1]' AND `action` = 'Inventory Process Order'", $post_vars['ioid'])) !== FALSE)
                {
                    if (mysqli_num_rows($get_inventory_action_info) > 0)
                    {
                        $existAddedItemArr = array();
                        $existRemovedItemArr = array();
                        while ($inventoryActionInfo = mysqli_fetch_assoc($get_inventory_action_info)) {
                            $menu_order = json_decode($inventoryActionInfo['details'], true);
                            $addedItemArr = $menu_order[0]['added'];
                            $removedItemArr = $menu_order[0]['removed'];
                            foreach ($addedItemArr as $addedTtemType => $addedIdsQuantity) {
                                if(!empty($addedIdsQuantity)) {
                                    foreach ($addedIdsQuantity as $addedIdQuantity) {
                                        $existAddedItemArr[$addedTtemType][$addedIdQuantity['id']] += $addedIdQuantity['quantity'];
                                    }
                                }
                            }
                            foreach ($removedItemArr as $itemType => $idsQuantity) {
                                if(!empty($idsQuantity)) {
                                    foreach ($idsQuantity as $idQuantity) {
                                        if ($itemType == 'item_id') {
                                            $existRemovedItemArr['item_id'][$idQuantity['id']] += $idQuantity['quantity'];
                                        } else if ($itemType == 'forced_modifier_id') {
                                            $existRemovedItemArr['forced_modifier_id'][$idQuantity['id']] += $idQuantity['quantity'];
                                        } else if ($itemType == 'optional_modifier_id') {
                                            $existRemovedItemArr['optional_modifier_id'][$idQuantity['id']] += $idQuantity['quantity'];
                                        }
                                    }
                                }
                            }
                        }

                        $finalReserveQuantity['action'] = '';
                        foreach($existAddedItemArr as $itemType => $idQuantity) {
                            $key = 0;
                            foreach ($idQuantity as $id => $qty) {
                                $finalReserveQuantity[$itemType][$key]['id'] = $id;
                                $finalReserveQuantity[$itemType][$key]['quantity'] = $qty - $existRemovedItemArr[$itemType][$id];
                            }
                        }

                        if (!empty($finalReserveQuantity)) {
                            $removedReserveQuantity = array();
                            $removedReserveQuantity[0]['removed']['action'] = '';
                            $removedReserveQuantity[0]['added']['item_id'] = array();
                            $removedReserveQuantity[0]['added']['forced_modifier_id'] = array();
                            $removedReserveQuantity[0]['added']['optional_modifier_id'] = array();
                            $removedReserveQuantity[0]['removed'] = $finalReserveQuantity;

                            process_order(json_encode($removedReserveQuantity, JSON_NUMERIC_CHECK));
                        }
                    }
                    $offlineInventorySync = true;
                }
            }
        }

        if ($offlineInventorySync) {
            $mod_details = array();
            $mod_type_ids = array();
            if (($get_mods_used = lavu_query("SELECT `mod_id`, `qty`, `type` FROM `modifiers_used` WHERE `ioid` = '[1]'", $post_vars['ioid'])) !== FALSE) {
                if (mysqli_num_rows($get_mods_used) > 0) {
                    while ($mod_used = mysqli_fetch_assoc($get_mods_used)) {

                        $mod_details[$mod_used['type']][$mod_used['mod_id']]['qty'] = $mod_used['qty'];
                        $mod_type_ids[$mod_used['type']]['mod_id'][] = $mod_used['mod_id'];
                    }
                }
			}

            // Create delivery order details
            $deliverOrder = array();
            $key = 0;
            foreach ($item_ids as $mItem) {
                $deliverOrder[0]['added']['item_id'][$key]['id'] = $mItem;
                $deliverOrder[0]['added']['item_id'][$key]['quantity'] = $item_quantity[$mItem];
                $key++;
            }

            foreach ($mod_type_ids['forced']['mod_id'] as $key => $fmid) {
                $deliverOrder[0]['added']['forced_modifier_id'][$key]['id'] = $fmid;
                $deliverOrder[0]['added']['forced_modifier_id'][$key]['quantity'] = $mod_details['forced'][$fmid]['qty'];
            }

            foreach ($mod_type_ids['optional']['mod_id'] as $key => $omid) {
                $deliverOrder[0]['added']['optional_modifier_id'][$key]['id'] = $omid;
                $deliverOrder[0]['added']['optional_modifier_id'][$key]['quantity'] = $mod_details['optional'][$omid]['qty'];
            }

            $deliverOrder[0]['removed']['item_id'] = array();
            $deliverOrder[0]['removed']['forced_modifier_id'] = array();
            $deliverOrder[0]['removed']['optional_modifier_id'] = array();

            $deliverOrderStr = json_encode($deliverOrder, JSON_NUMERIC_CHECK);
            updateQuantityOnHand($deliverOrderStr);
            $details_short = $post_vars['app_name'] . ' ' . $post_vars['app_version'] . ' (' . $post_vars['app_build'] . ') ' . 'via '.$_SERVER['HTTP_HOST'];
            lavu_query("INSERT INTO `action_log` (`action`,
                                    `loc_id`,
                                    `order_id`,
                                    `time`, `user`,
                                    `user_id`,
                                    `server_time`,
                                    `details`,
                                    `device_udid`,
                                    `details_short`,
                                    `ialid`,
                                    `ioid`)
                        VALUES ('Inventory Deliver Order',
                                '" . lavu_query_encode($post_vars['loc_id']) . "',
                                '" . lavu_query_encode($post_vars['order_id']) . "',
                                '".$post_vars['device_time']."',
                                '[1]',
                                '".lavu_query_encode($post_vars['user_id'])."',
                                '" . lavu_query_encode($post_vars['server_time']) . "',
                                '" . lavu_query_encode($deliverOrderStr) . "',
                                '" . lavu_query_encode(determineDeviceIdentifier($post_vars)) . "',
                                '" . lavu_query_encode($details_short) . "' ,
                                '" . lavu_query_encode(newInternalID()) . "',
                                '" . lavu_query_encode($post_vars['ioid']) . "')",
                                lavu_query_encode($post_vars['auth_by']));
        }

    }
	// apply appropriate LavuKart special event deposit status

	if (isset($should_update_deposit_status) && $should_update_deposit_status != 0)
	{
		$set_as_opened = "";
		$record_deposit_amount = "";
		if ($post_vars['deposit_status'] == "3")
		{
			$log_message = "Remaining payment received for Group Event";
			$should_update_deposit_status = 3;
			//foreach ($deposit_order_ids as $doi) {
				//$update_order = lavu_query("UPDATE `orders` SET `deposit_status` = '3' WHERE `order_id` = '".$doi."' AND `location_id` = '".$post_vars['loc_id']."'");
			//}
		}
		else
		{
			if ($should_update_deposit_status == 2)
			{
				$log_message = "Deposit received for Group Event";
				$set_as_opened = ", `closed` = '0000-00-00 00:00:00'";
			}
			else if ($should_update_deposit_status == 3)
			{
				$log_message = "Full payment received for Group Event";
			}
			$record_deposit_amount = ", `deposit_amount` = '".priceFormat($total_deposit_amount)."', `subtotal_without_deposit` = '".priceFormat($close_info[8] - $total_deposit_amount)."'";
			$closed_action = "Deposit paid";
		}

		$last_mod_ts = time();
		$last_modified = determineDeviceTime($location_info, $post_vars);

		$update_deposit_status = lavu_query("UPDATE `orders` SET `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'JC FUNCTIONS', `deposit_status` = '".$should_update_deposit_status."'".$set_as_opened.$record_deposit_amount." WHERE `order_id` = '[3]' AND location_id = '[4]'", $last_mod_ts, $last_modified, $post_vars['order_id'], $post_vars['loc_id']);

		if ($cpc == "lavukart")
		{
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$log_message."', '0', '".$post_vars['order_id']."', now(), '".$close_info[13]."', '0', '".$post_vars['loc_id']."')");
		}
	}

	$receipt_string_array = explode("|*|", (isset($post_vars['receipts'])?$post_vars['receipts']:""));

	require_once(__DIR__."/../email_func.php");

	for ($c = 1; $c <= cappedCheckCount($check_count); $c++)
	{
		$previously_checked_out = "0";
		if (($check_for_details = lavu_query("SELECT * FROM `split_check_details` WHERE `ioid` = '[1]' AND `check` = '[2]'", $post_vars['ioid'], $c)) !== FALSE)
		{
			if (mysqli_num_rows($check_for_details) > 0)
			{
				$check_details = mysqli_fetch_assoc($check_for_details);
				$previously_checked_out = $check_details['checked_out'];

				if ($check_details['email']!="" && isset($receipt_string_array[$i]) && $receipt_string_array[$i]!="")
				{
					email_receipt($post_vars['loc_id'], $post_vars['order_id'], $c, $receipt_string_array[($c - 1)], $check_details['email'], $post_vars['server_id'], $post_vars['server'], $check_count);
				}

				$order_vars['checked_out'] = $order_info['checked_out'];
				if (loyalTreeEnabledForLocation($location_info))
				{
					get_loyaltree_purchase_info($post_vars['loc_id'], $c, $post_vars['order_id'], false, "close_order");
				}
				processCheckAtCheckOut($check_details, $order_info, $location_info, $order_contents_by_check[$c]);
			}

			mysqli_free_result( $check_for_details );
		}
	}

	$clean_up_extra_details = lavu_query("DELETE FROM `split_check_details` WHERE `ioid` = '[1]' AND `check` > '[2]'", $post_vars['ioid'], $check_count);

	if (($check_payments = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `order_id` = '0' AND `ioid` = '[1]'", $post_vars['ioid'])) !== FALSE)
	{
		if (mysqli_num_rows($check_payments) > 0)
		{
			while ($info = mysqli_fetch_assoc($check_payments))
			{
				$update_order_id = lavu_query("UPDATE `cc_transactions` SET `order_id` = '[1]' WHERE `id` = '[2]'", $post_vars['order_id'], $info['id']);
			}
		}

		mysqli_free_result( $check_payments );
	}

    /*
     * updating signature for Norway Restaurants order
     *
     */
    $nor_way = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'norway_mapping'");
    $norway_values = mysqli_fetch_assoc($nor_way);
    $norway_status=$norway_values['value'];

    if($norway_status=='1')
    {
        updateSignatureForNorwayOrder($post_vars['order_id'], $order_info);
    }
    /*
     * end of signature for Norway Restaurants order
     *
     */

	return "1";
}

function updateSignatureForNorwayOrder($currentOrderId, $order_info = '')
{

    $last_signature = "0";
    $get_last_order_signature = lavu_query("SELECT `signature` FROM `orders` where location_id = " .reqvar("loc_id","0"). " and signature != '' order by id DESC LIMIT 1");
    if ($get_last_order_signature!==FALSE && mysqli_num_rows($get_last_order_signature))
    {
        $last_order_record = mysqli_fetch_assoc($get_last_order_signature);
        $last_signature = $last_order_record['signature'];

        if ($last_order_record['signature'] != '') {
            $last_signature = $last_order_record['signature'];
        }
    }

    if ($order_info == '')
    {
        $get_current_order_info = lavu_query("SELECT `closed`,`total`,`subtotal`,`discount` FROM `orders` where order_id='" .$currentOrderId."' AND location_id = " .reqvar("loc_id","0"). " order by id DESC LIMIT 1");
        if (mysqli_num_rows($get_current_order_info))
        {
            $order_info = mysqli_fetch_assoc($get_current_order_info);
        }
    }

    $closedDayTime = explode(" ",$order_info['closed']);

    $signatureData = implode(";",
        array($last_signature,
            $closedDayTime[0],
            $closedDayTime[1],
            $currentOrderId,
            $order_info['total'],
            ($order_info['subtotal'] - $order_info['discount'])
        )
    );
        $resultArray = getNorwaySignature($signatureData);
        $update_signature = lavu_query("UPDATE orders SET signature='".$resultArray['signature']."',key_version='".$resultArray['keyVersion']."' WHERE order_id='".$currentOrderId."'");

}

// start NORWAY SIGNATURE KEY FUNCTIONALITY ************************************************************** //
/*
 * LP-2494 for Norway Fiscal: Outstanding Issues / Enhancements
 *Function for add signature to the action log table when any order related transaction occured.
 *this signeture functionality adding in to the xml file during report genaration
 */
function updateSignatureForNorwayActionLog($currentActionLogId)
{
	// Norway settings enabled status
	$norwayConfig = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'norway_mapping'");
	$norwayConfigStatus = mysqli_fetch_assoc($norwayConfig);

	// Norway settings getting  vat id and location id
	$norwayConfigQuery = lavu_query("SELECT `value`,`location` FROM `config` WHERE `setting` = 'norway_vat_id'");
	$norwayConfigValues = mysqli_fetch_assoc($norwayConfigQuery);

	if($norwayConfigStatus['value'] == "1" && $norwayConfigValues['value'] != "")   // if condition for Norway settings enabled status check START
	{
		$norwayConfigStatus = $norwayConfigStatus['value'];  // Enable Status = 1
		$businessid = $norwayConfigValues['value'];  // vat ID

		$last_signature = "0";
		$order_details = "";
		// get the details from action log table
		$get_last_transaction = lavu_query("SELECT `action`,`details`, `server_time`, `order_id` FROM `action_log` where `loc_id` = " .$norwayConfigValues['location']. " and id = '".$currentActionLogId."' order by id DESC LIMIT 1");
		if ($get_last_transaction!==FALSE && mysqli_num_rows($get_last_transaction))  //  get the action log details START
		{
			$last_transaction_record = mysqli_fetch_assoc($get_last_transaction);
			if ($last_transaction_record['details'] != '' ) {  // check if order details is not empty start
				$order_details = $last_transaction_record['details'];
				$match = array();
				preg_match_all('!\d+(\,|\.)+\d*!', $order_details ,$match);  // check for numeric degit
				if ($match[0][0] !='' && $last_transaction_record['order_id'] != ''){   // check for currency number format in match case START
					$server_time = $last_transaction_record['server_time'];
					$signatureData = implode(";",
							array($server_time,     			  // action_log's server time
							$businessid,				  //business id is Norway vat ID
							$currentActionLogId          //action_log's last insert id	 or name as sequencenr number
							)
						);
				//error_log('inpuat data:'.print_r($signatureData,1).'----');
				$resultArray = getNorwaySignature($signatureData);
				$update_signature = lavu_query("UPDATE action_log SET signature='".$resultArray['signature']."',key_version='".$resultArray['keyVersion']."' WHERE id='".$currentActionLogId."'");

				} // if condition for check for currency number format in match case CLOSED
			} // if condition for  check if order details is not empty  CLOSED
		} // if condition close for get the action log details CLOSED
	} // if condition for Norway settings enabled status check CLOSED
}

/*
 * LP-2494 for Norway Fiscal: Outstanding Issues / Enhancements
 *Function for create signature pattern with given parameter
 *this signeture functionality addon functionality of function updateSignatureForNorwayActionLog()
 */
function getNorwaySignature($signatureData) {


	include_once(__DIR__.'/../../norway_reports/phpseclib1.0.5/Crypt/RSA.php');
	include_once(__DIR__.'/../../norway_reports/phpseclib1.0.5/Math/BigInteger.php');

	// Create an Instance
	$rsa = new Crypt_RSA();
	$hash = new Crypt_Hash('sha1');

	//getting private key file content
	$norwayConfigQuery = mlavu_query("SELECT AES_DECRYPT(private_key,'imJiHMFKAmQebLh3k6K1') AS private_key,id FROM norway_keys WHERE private_key !='' LIMIT 1");
	$norwayConfigValues = mysqli_fetch_assoc($norwayConfigQuery);

	if ($norwayConfigValues['private_key'] != "") {
		$privatekey = $norwayConfigValues['private_key'];
		$keyVersion = $norwayConfigValues['id'];
	}

	//error_log('private key contenet-----'.$privatekey.'-------');
	$rsa->loadKey($privatekey);
	$rsa->setSignatureMode(CRYPT_RSA_SIGNATURE_PKCS1);
	$hashed = $hash->hash($signatureData); // Hash using SHA1

	$encrypted = $rsa->sign($hashed); // Sign Data
	$encrypted = base64_encode($encrypted); //do base64 encoding
	//error_log('created signature------'.$encrypted.'---');

	return array('signature'=>$encrypted, 'keyVersion'=>$keyVersion);

}



// END NORWAY SIGNATURE KEY FUNCTIONALITY ******************************************************************* //

function processCheckAtCheckOut($check_info, $order_info, $location_info, $check_contents)
{
	global $hold_cc;
	global $data_name;
	global $device_time;
	global $post_vars;

	$ci = $check_info;
	$oi = $order_info;
	$li = $location_info;

	if ($li['enable_mercury_loyalty'])
	{
		if ((substr($ci['loyalty_info'], 0, 15)=="mercury_loyalty" || substr($ci['discount_info'], 0, 15)=="mercury_loyalty") && empty($oi['checked_out']))
		{ // mercury loyalty add points / redeem coupon
			$loyalty_info = explode("|", $ci['loyalty_info']);
			$discount_info = explode("|", $ci['discount_info']);

			//error_log("count loyalty_info: ".count($loyalty_info));

			require_once(dirname(__FILE__)."/../gateway_lib/mercury_loyalty/comm/setup/mercury_loyalty.php");
			require_once(dirname(__FILE__)."/../gateway_lib/mercury_loyalty/comm/response/mercury_responsehandler.php");

			$mercuryFactory = new MLoyalty();
			if (isset($li['mercury_loyalty_api_key']))
			{
				$mercuryFactory->setAPIKey($li['mercury_loyalty_api_key']);
			}
			if (isset($li['mercury_loyalty_api_secret']))
			{
				$mercuryFactory->setAPISecret($li['mercury_loyalty_api_secret']);
			}

			$comm = $mercuryFactory->createGetProgramDescription();

			if(!$comm)
			{
				$error_message = "Unable to Load Mercury Loyalty at this Time.";
			}
			else
			{
				$response = $comm->process();
				$responseHandler = new MResponseHandler();
				$result = $responseHandler->parse($response);
				if ($result !== null)
				{
					$programactive = $result->Options['isprogramactive'];
					if(!$programactive)
					{
						$error_message = "The Specified Loyalty Program is not Currently Active.";
					}
					else
					{
						$isLoyalty;
						if (count($loyalty_info)>=4 && $loyalty_info[0]=="mercury_loyalty")
						{ //Customer Loyalty Info / Add Credits
							$isLoyalty = true;
							$set_units = $ci['check_total'];
							if (strtolower($loyalty_info[4]) == "visit")
							{
								$set_units = ($oi['guests'] / $oi['no_of_checks']);
							}

							$comm = $mercuryFactory->createAddCredits();
							if (!$comm)
							{
								$error_message = "failed to add points ";
							}
							else
							{
								$comm->addVariable("customeridentifier", $loyalty_info[3]);
								//$comm->addVariable("TestMode", true); //For Testing (Doesn't Add to Total)
								$comm->addVariable("client", $li['title']);
								$comm->addVariable("description", "Food Stuffs");
								$comm->addVariable("employee_id", $oi['cashier_id']);
								$comm->addVariable("station_id", $ci['register_name']);
								$comm->addVariable("ticket_id", $ci['order_id']);
								$comm->addVariable("revenue", $ci['check_total']);
								$comm->addVariable("units", $set_units);

								$response = $comm->process();
								$responseHandler = new MResponseHandler();
								$result = $responseHandler->parse($response);
							}

							if (count($discount_info)>=3 && $discount_info[0]=="mercury_loyalty")
							{ //Redeem Coupon
								$comm = $mercuryFactory->createRedeemCoupon();
								if (!$comm)
								{
									$error_message .= "failed to redeem coupon";
								}
								else
								{
									$comm->addVariable("customeridentifier", $loyalty_info[3]);
									$comm->addVariable("code", $discount_info[2]);
									//$comm->addVariable("cardnumber", "0000");
									$comm->addVariable("deviceId", $ci['register_name']);
									$basket = new MSKUBasket();

									//$item = new MSKUBasketItem();
									//$basket->addItemToBasket($item);
									//$item->__set("Quantity","1");
									//$item->__set("SKU","Makin Bacon");
									//$item->__set("UnitPrice","10.0");

									$comm->addVariable(null, $basket, false);

									$response = $comm->process();
									$responseHandler = new MResponseHandler();
									$result = $responseHandler->parse($response);
								}
							}
						}

						//close transaction
					}
				}
				else
				{
				}
			}
		}

		if (loyalTreeEnabledForLocation($li) && empty($oi['checked_out']))
		{ // mercury loyalty add points / redeem coupon
			$loyalty_info = explode("|", $ci['loyalty_info']);
			$discount_info = explode("|", $ci['discount_info']);

			$set_units = 0;
			if (count($loyalty_info)>=3 && $loyalty_info[0]=="heartland_loyalty")
			{ //Customer Loyalty Info / Add Credits
				$set_units = floor($ci['check_total']);
				if (strtolower($loyalty_info[4]) == "visit")
				{
					$set_units = floor($oi['guests'] / $oi['no_of_checks']);
				}
			}
			if (0 < $set_units)
			{
				require_once(dirname(__FILE__)."/../gateway_lib/heartland_loyalty/heartland_add_points.php");
				heartlandAddPoints($li ,$loyalty_info,$discount_info,$set_units);
				error_log(__FILE__." ".__FUNCTION__." : heartlandAddPoints() : $set_units ".$post_vars['app_version']);
			}
		}
	}

	if (loyalTreeEnabledForLocation($li) && empty($ci['loyaltree_info']))
	{
		$host = ($_SERVER['HTTP_HOST'] == "admin.poslavu.com")?"admin.poslavu.com":"localhost";
		$url = $host."/lib/receipt_append_results.php?PHPSESSID=".session_id();

		//error_log($url);

		$vars = array();
		//error_log("about to set the mode email is ".$ci['email']);

			$vars['m'] = "loyaltree_qr_code";
		$vars['cc'] = $hold_cc;
		$vars['data_name'] = $data_name;
		$vars['version'] = reqvar("app_version", "");
		$vars['build'] = reqvar("app_build", "");
		$vars['ioid'] = $order_info['ioid'];
		$vars['loc_id'] = $li['id'];
		$vars['order_id'] = $oi['order_id'];
		$vars['check'] = $ci['check'];
		$vars['device_time'] = $device_time;
		$vars['prepare_image'] = "0";

		$var_string = "";
		foreach ($vars as $key => $val)
		{
			if ($var_string != "")
			{
				$var_string .= "&";
			}
			$var_string .= $key."=".rawurlencode($val);
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $var_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close ($ch);
	}

	// Pepper Powered Lavu Loyalty
	if (!empty($location_info['pepper_enabled']) && !empty($order_info['original_id']) && substr($order_info['original_id'], 0, 4) == '112|') {
		require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperIntegration.php');

		// Get Pepper userId for checked-in user stored in orders.original_id by the webview
		$original_id = explode('|o|', $order_info['original_id']);
		$pepper_data = isset($original_id[4]) ? json_decode($original_id[4], true) : array();
		$loyaltyInfo = isset($pepper_data['loyaltyInfo']) ? $pepper_data['loyaltyInfo'] : array();

		$pepper_args = array(
			'dataname'     => $data_name,
			'order_id'     => $order_info['order_id'],
			'check'        => (isset($check_info['check']) ? $check_info['check'] : '1'),
			'locationId'   => $location_info['pepper_locationid'],
			'userId'       => $original_id[3],
			'register'     => $order_info['register'],
			'registerId'   => $location_info['pepper_registerid'],
			'orderId'      => (isset($loyaltyInfo['orderId']) ? $loyaltyInfo['orderId'] : ''),  // Include orderId if there is already created an uncompleted Pepper order for this Lavu order from app|points payment methods during POS checkout
			'extract-data' => true,  // Re-query db for order data
			'update-order' => true,  // Finalize Pepper order
		);

		// Send Order to Pepper
		$results = PepperIntegration::sendOrder($pepper_args);

		// Save results in split_check_details
		$results_saved = lavu_query("UPDATE `split_check_details` SET `loyalty_info` = 'pepper_loyalty|[createdOrder]|[createdTransaction]|[updatedOrder]|[orderId]|[transactionId]' WHERE `order_id` = '[order_id]' AND `check` = '[check]' AND `loyalty_info` = ''", array_merge($pepper_args, $results));
	}
}

function processPepperVoidOrRefund($vars)
{
	global $data_name;
	global $location_info;

	require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperIntegration.php');

	// Get Pepper userId for checked-in user stored in orders.original_id by the webview
	$original_id = explode('|o|', $vars['original_id']);
	$pepper_data = isset($original_id[4]) ? json_decode($original_id[4], true) : array();
	$loyaltyInfo = isset($pepper_data['loyaltyInfo']) ? $pepper_data['loyaltyInfo'] : array();

	$pepper_args = array(
		'dataname'     => $data_name,
		'order_id'     => $vars['order_id'],
		'check'        => (isset($check_info['check']) ? $check_info['check'] : '1'),
		'locationId'   => $location_info['pepper_locationid'],
		'userId'       => $original_id[3],
		'register'     => $vars['register'],
		'registerId'   => $location_info['registerId'],
		'orderId'      => (isset($loyaltyInfo['orderId']) ? $loyaltyInfo['orderId'] : ''),  // Include orderId if there is already created an uncompleted Pepper order for this Lavu order from app|points payment methods during POS checkout
		'extract-data' => true,  // Queries db for cc_txn data
	);

	// Send Order to Pepper
	$results = PepperIntegration::refundTransactions($pepper_args);

	if ($results['success'])
	{
		/// TO DO : // Save results in split_check_details
		///$results_saved = lavu_query("UPDATE `split_check_details` SET `loyalty_info` = 'pepper_loyalty|[createdOrder]|[createdTransaction]|[updatedOrder]|[orderId]|[transactionId]' WHERE `order_id` = '[order_id]' AND `check` = '[check]' AND `loyalty_info` = ''", array_merge($pepper_args, $results));
	}

	return $results['success'];
}

function evenlySplitValue($amount, $check, $check_count)
{
	global $location_info;

	$decshift = pow(10, $location_info['disable_decimal']);
	$tmp_amt = floor(($amount * $decshift) / $check_count);
	$remainder = (($amount * $decshift) - ($tmp_amt * $check_count));
	if ($check > $remainder)
	{
		$remainder = 0;
	}
	else
	{
		$remainder = 1;
	}
	$amount = (($tmp_amt + $remainder) / $decshift);

	return $amount;
}

function writeActionLogEntry($post_vars) {

	// mode == 64

	global $location_info;

	$q_fields = "";
	$q_values = "";
	$l_vars = array();
	$l_vars['action'] = $post_vars['action'];
	$l_vars['loc_id'] = $location_info['id'];
	$l_vars['order_id'] = $post_vars['order_id'];
	$l_vars['check'] = $post_vars['check'];
	$l_vars['time'] = $post_vars['device_time'];
	$l_vars['user'] = $post_vars['server_name'];
	$l_vars['user_id'] = $post_vars['server_id'];
	$l_vars['details'] = $post_vars['details'];
	$l_vars['device_udid'] = determineDeviceIdentifier($post_vars);
	$keys = array_keys($l_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}
	$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);
	// START LP-2494
	include_once('../../lib/jcvs/jc_func8.php');
	$currentActionLogId = lavu_insert_id();
	updateSignatureForNorwayActionLog($currentActionLogId);
	// END  LP-2494
	return "1";
}
function get_addon_list($mode) {

	$addon_list = array();

	$path = dirname(__FILE__)."/jc_addons/".$mode;
	if (is_dir($path)) {
		if ($dir = opendir($path)) {
			while (false !== ($file = readdir($dir))) {
				if (substr($file, -4) == ".php") $addon_list[] = str_replace(".php", "", $file);
			}
			closedir($dir);
		} else error_log("Unable to open addon path: ".$path);
	}

	return $addon_list;
}
function process_jc_addons($jc_addons, $active_modules, $mode, $boa, $jc_result="") { // boa = "before or after"

	global $company_info;
	global $location_info;
	global $device_time;

	//error_log("process_jc_addons: ".$mode." ".$boa);
	//error_log("jc_addons: ".print_r($jc_addons, true));

	foreach ($jc_addons as $addon) {

		//error_log("addon: ".$addon);

		$check_module = str_replace("_", ".", $addon);

		if ($active_modules->hasModule($check_module)) {

			//error_log("has module: ".$check_module);

			$addon_path = dirname(__FILE__)."/jc_addons/".$mode."/".$addon.".php";

			//error_log("addon_path: ".$addon_path);

			if (file_exists($addon_path)) {
				//error_log("including ".$addon_path);
				require_once($addon_path);
			}

			if (function_exists($boa."_".$addon)) {
				//error_log("should call ".$boa."_".$addon);
				$jc_result = call_user_func($boa."_".$addon, $jc_result);
			}

			//error_log("jc_result: ".$jc_result);

		} //else error_log("doesn't have module: ".$check_module);
	}

	return $jc_result;
}

function ERScurl($url, $vars) {

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
	$response = curl_exec($ch);

	return $response;
}

function performExternalLogin($client_app, $group, $minlevel) {

	global $device_time;

	$post_url = "https://default_general.ershost.com/view/update_poslavu_order/";

	$sendstr = "user=".$_REQUEST['un']."&pwd=".$_REQUEST['pw']."&group=".$group."&minlevel=".$minlevel;
	$postvars = "poslavu_order_id=&poslavu_action=validate_login&poslavu_info=" . str_replace(array("&", "="), array("(pass_and)", "(pass_eq)"), $sendstr);

	$response = ERScurl($post_url, $postvars);

	$result = explode("|", $response);
	if (count($result) < 2) {
		lavu_echo('{"json_status":"Invalid","ers":"empty response"}');
	}

	if ($result[0] != "1") {
		lavu_echo('{"json_status":"Invalid","ers":"invalid login","message":"'.$result[1].'"}');
	}

	$resp_vars = array();
	$rp_pairs = explode("&", $result[2]);
	foreach ($rp_pairs as $pair) {
		$pair_parts = explode("=", $pair);
		$resp_vars[$pair_parts[0]] = $pair_parts[1];
	}

	$data_name = $resp_vars['poslavu_dataname'];

	$get_company_info = mlavu_query("SELECT `id`, `data_name`, `company_name`, `company_code`, `logo_img`, `receipt_logo_img`, `dev`, `demo`, `disabled`, `chain_id`, `lavu_lite_server` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $data_name);
	if ($get_company_info === FALSE || mysqli_num_rows($get_company_info) < 1) {
		lavu_echo('{"json_status":"NoRestaurants"}');
	}

	$c_info = mysqli_fetch_assoc($get_company_info);
	if ($c_info['disabled']=="1" || $c_info['disabled'] == "2" || $c_info['disabled'] == "3"){
		lavu_echo('{"json_status":"AccountDisabled"}');
	}
	mysqli_free_result( $get_company_info );

	$u_data = array();
	$u_data['id'] = $resp_vars['id'];
	$u_data['f_name'] = $resp_vars['firstname'];
	$u_data['l_name'] = $resp_vars['lastname'];
	$u_data['access_level'] = $resp_vars['group_level'];
	$u_data['service_type'] = "2";
	$u_data['lavu_admin'] = "0";
	$u_data['PIN'] = "1234";

	lavu_connect_db($c_info['id'], "poslavu_".$data_name."_db");

	$loc_info = array();
	$loc_info['id'] = "1";
	if (($get_location_info = lavu_query("SELECT `id`, `net_path`, `use_net_path`, `net_path_print` FROM `locations` ORDER BY `id` ASC LIMIT 1")) !== FALSE) {
		if (mysqli_num_rows($get_location_info) > 0) {
			$loc_info = mysqli_fetch_assoc($get_location_info);
		}
		mysqli_free_result( $get_location_info );
	}
	if (($get_config_info = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `type` = 'location_config_setting' AND `setting` IN ('ers_netpath', 'lavu_task_netpath', 'lavu_task_api')")) !== FALSE) {
		if (mysqli_num_rows($get_config_info) > 0) {
			while ($cc_info = mysqli_fetch_assoc($get_config_info)) {
				$loc_info[$cc_info['setting']] = $cc_info['value'];
			}
		}
		mysqli_free_result( $get_config_info );
	}

	include_once(dirname(__FILE__)."/redirect_tables.php");
	if ($client_app == "Lavu Task") {
		if (function_exists("getLavuTaskWebpathTable")) {
			$lt_paths = getLavuTaskWebpathTable();
			if (!empty($loc_info['ers_netpath'])) {
				$loc_info['lavu_task_netpath'] = $loc_info['ers_netpath'];
				$loc_info['lavu_task_api'] = $lt_paths['ers'];
			} else {
				$loc_info['lavu_task_netpath'] = $loc_info['net_path'];
				$loc_info['lavu_task_api'] = $lt_paths['lavu'];
			}
		}
	}

	$q_fields = "";
	$q_values = "";
	$log_vars = array();
	$log_vars['action'] = $client_app." User Logged In";
	$log_vars['loc_id'] = $loc_info['id'];
	$log_vars['order_id'] = "0";
	$log_vars['time'] = $device_time;
	$log_vars['user'] = $u_data['f_name']." ".$u_data['l_name'];
	$log_vars['user_id'] = $u_data['id'];
	$log_vars['details'] = $client_app;
	$log_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
	$keys = array_keys($log_vars);
	foreach ($keys as $key) {
		if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
		$q_fields .= "`$key`";
		$q_values .= "'[$key]'";
	}
	$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

	//Code added by Ravi Tiwari on 1st April 2019
	$c_info['receipt_logo_img'] = $c_info['receipt_logo_img']?$c_info['receipt_logo_img']:"poslavu_bw.png";
	$c_info['logo_img'] = $c_info['logo_img'] ? $c_info['logo_img'] : "poslavu.png";

	if($c_info['logo_img'] != "poslavu.png"){
		$location_logo_uri = $_SERVER['REQUEST_SCHEME']."/".$_SERVER['SERVER_NAME']."/images/".$data_name."/";
		$S3_logo_uri = '';
	}else{
		$S3_logo_uri = getenv('S3STATICIMAGE').'images/';
		$location_logo_uri = '';
	}

	if($c_info['receipt_logo_img'] != "poslavu_bw.png"){
		$location_logo_uri = $_SERVER['REQUEST_SCHEME']."/".$_SERVER['SERVER_NAME']."/images/".$data_name."/";
		$S3_logo_uri = '';
	}else{
		$S3_logo_uri = getenv('S3STATICIMAGE').'images/';
		$location_logo_uri = '';
	}

	$response = '{"json_status":"Valid","server_id":"'.$u_data['id'].'","quick_serve":"1","service_type":"2","lavu_admin":"0","loc_info":'.json_encode($loc_info).',"all_user_data":'.json_encode(array($u_data)).',"access_level":"'.$u_data['access_level'].'","PIN":"1234","f_name":"'.$u_data['f_name'].'","l_name":"'.$u_data['l_name'].'","company_id":"'.$c_info['id'].'","company_name":"'.$c_info['company_name'].'","company_code":"'.lsecurity_name($c_info['company_code'], $c_info['id']).":".$_REQUEST['build'].'","data_name":"'.$c_info['data_name'].'","logo_img":"'.$c_info['logo_img'].'","receipt_logo_img":"'.$c_info['receipt_logo_img'].'", "S3_logo_uri":"'.$S3_logo_uri.'", "location_logo_uri":"'.$location_logo_uri.'", dev":"'.$c_info['dev'].'","demo":"'.$c_info['demo'].'","lavu_lite_server":"'.$c_info['lavu_lite_server'].'","valid_PINs":"1234","valid_admin_PINs":"1234"}';

	lavu_echo(cleanJSON($response));
}

function getERSuserInfo($user_id) {

	$user_info = array();

	$post_url = "https://default_general.ershost.com/view/update_poslavu_order/";

	$sendstr = "user_id=".$user_id."&group=all";
	$postvars = "poslavu_order_id=&poslavu_action=validate_login&poslavu_info=" . str_replace(array("&", "="), array("(pass_and)", "(pass_eq)"), $sendstr);

	$response = ERScurl($post_url, $postvars);

	$result = explode("|", $response);
	if (count($result) >= 1) {
		if ($result[0] == "1") {
			$resp_vars = array();
			$rp_pairs = explode("&", $result[2]);
			foreach ($rp_pairs as $pair) {
				$pair_parts = explode("=", $pair);
				$resp_vars[$pair_parts[0]] = $pair_parts[1];

				$user_info['id'] = $resp_vars['id'];
				$user_info['f_name'] = $resp_vars['firstname'];
				$user_info['l_name'] = $resp_vars['lastname'];
				$user_info['access_level'] = $resp_vars['group_level'];
				$user_info['service_type'] = "2";
				$user_info['lavu_admin'] = "0";
				$user_info['PIN'] = "1234";
			}
		}
	}

	return $user_info;
}

function determineDevicePrefix($loc_id, $uuid, $app_name, $model)
{
	global $location_info;
	global $device_time;

	$vars = array(
		'app_name'		=> $app_name,
		'device_time'	=> $device_time,
		'loc_id'		=> $loc_id,
		'model'			=> $model,
		'remote_ip'		=> reliableRemoteIP(),
		'UUID'			=> $uuid
	);

	if (($check_devices = lavu_query("SELECT `id`, `loc_id`, `prefix`, `app_name` FROM `devices` WHERE `UUID` = '[1]'", $uuid)) !== FALSE)
	{
		$matches = mysqli_num_rows($check_devices);
		if ($matches > 0)
		{
			$device_info = mysqli_fetch_assoc($check_devices);

			$vars['id']		= $device_info['id'];
			$vars['prefix'] = $device_info['prefix'];

			if ($device_info['app_name'] != $app_name)
			{
				$vars['from_app'] = $device_info['app_name'];

				$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('Switched apps', now(), '[device_time]', '0', '[remote_ip]', '[UUID]', '[model]', 'from [from_app]', 'to [app_name]', '[loc_id]')", $vars);
			}

			if ($device_info['loc_id'] != $loc_id)
			{
				$vars['from_location']	= getFieldInfo($device_info['loc_id'], "id", "locations", "title");
				$vars['to_location']	= $location_info['title'];

				$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('Switched locations', now(), '[device_time]', '0', '[remote_ip]', '[UUID]', '[model]', 'from [from_location]', 'to [to_location]', '[loc_id]')", $vars);
			}

			$update_record = lavu_query("UPDATE `devices` SET `loc_id` = '[loc_id]', `prefix` = '[prefix]', `app_name` = '[app_name]', `model` = '[model]', `last_checkin` = now(), `device_time` = '[device_time]', `remote_ip` = '[remote_ip]', `inactive` = '0', `holding_order_id` = '', `needs_reload` = '0' WHERE `id` = '[id]'", $vars);

			if ($matches > 1)
			{
				$delete_duplicates = lavu_query("DELETE FROM `devices` WHERE `UUID` = '[1]' AND `id` != '[2]'", $uuid, $vars['id']);
			}
		}
		else
		{
			$vars['prefix'] = getLowestPrefix($loc_id);

			$create_record = lavu_query("INSERT INTO `devices` (`loc_id`, `UUID`, `prefix`, `app_name`, `model`, `last_checkin`, `device_time`, `remote_ip`, `first_checkin`) VALUES ('[loc_id]', '[UUID]', '[prefix]', '[app_name]', '[model]', now(), '[device_time]', '[remote_ip]', '[device_time]')", $vars);
		}

		mysqli_free_result( $check_devices );
	}

	return (string)$vars['prefix'];
}

function getLowestPrefix($loc_id)
{
	//$ninety_days_ago = date("Y-m-d H:i:s", (time() - 7776000));
	$prefixes = array();
	//if( ($get_prefixes = lavu_query("SELECT `prefix` FROM `devices` WHERE `loc_id` = '[1]' AND `last_checkin` >= '[2]'", $loc_id, $ninety_days_ago)) !== FALSE ){
	if (($get_prefixes = lavu_query("SELECT `prefix` FROM `devices`")) !== FALSE)
	{
		if (mysqli_num_rows($get_prefixes) > 0)
		{
			while ($info = mysqli_fetch_assoc($get_prefixes))
			{
				$prefixes[] = $info['prefix'];
			}
		}

		mysqli_free_result( $get_prefixes );
	}

	$p = 1;
	for ($p = 1; $p <= 999; $p++)
	{
		if (!in_array($p, $prefixes))
		{
			break;
		}
	}

	return $p;
}

function loadOrderList($criteria)
{
	global $data_name;
	global $location_info;
	global $location_lds_debug;
	global $lavu_query_should_log;

	$auto_send		= arrayValueForKey($criteria, "auto_send");
	$device_time	= $criteria['device_time'];
	$exclude_ioids	= (isset($criteria['exclude_ioids']))?$criteria['exclude_ioids']:array();
	$for_server_id	= (!empty($criteria['for_server_id']))?$criteria['for_server_id']:"all";
	$loc_id			= $criteria['loc_id'];
	$ltg			= $criteria['ltg'];
	$order_types	= (string)arrayValueForKey($criteria, "order_types", ""); // LP-5589, 4.0+
	$page			= (int)$criteria['page'];
	$regular_pull	= $criteria['regular_pull'];
	$search_string	= $criteria['search_string'];
	$search_type	= $criteria['search_type'];
	$server_ids		= (string)arrayValueForKey($criteria, "server_ids", ""); // LP-5589, 4.0+
	$showing_ioids 	= (!empty($criteria['showing_ioids']))?$criteria['showing_ioids']:FALSE;
	$sort_mode		= (string)arrayValueForKey($criteria, "sort_mode", ""); // LP-5589, 4.0+
	$submode		= $criteria['submode'];
	$allLtgOrders   = $submode === 'all-ltg-orders' ? true : false;
	$showAlertOnPos = $criteria['show_alert_on_pos'];
	if ($allLtgOrders) {
		$submode = "open";
	}
	$upcoming_ltg	= (!empty($criteria['upcoming_ltg']))?$criteria['upcoming_ltg']:FALSE;
	$datetime_start	= (string)arrayValueForKey($criteria, "datetime_start", ""); // LP-5589, 4.0+
	$datetime_end	= (string)arrayValueForKey($criteria, "datetime_end", ""); // LP-5589, 4.0+
	$date_range		= (string)arrayValueForKey($criteria, "date_range", ""); // LP-5589, 4.0+, OrderListDateRange
	$date_range     = trim($date_range);

	if ($date_range==0) {
	    $date_range="";
	}

	$json_status		= "NoMatch";
	$order_list			= array();
	$total_count		= 0;
	$first_order_data	= array();

	// LP-3129 - Skip standard order selection if the request is only performing a check for orders that need to be sent automatically.
	if (($auto_send == "1") && ($ltg != "1") && ($showAlertOnPos != 1))
	{
		return array(
			'json_status'	=> $json_status,
			'order_list'	=> $order_list,
			'PHPSESSID'		=> session_id(),
			'total_count'	=> $total_count?$total_count:0,
			'ts'			=> (string)time()
		);
	}

	$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
	$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

	$now_parts = explode(" ", $device_time);
	$now_date = explode("-", $now_parts[0]);
	$now_time = explode(":", $now_parts[1]);

	if (($now_time[0].$now_time[1]) < ($se_hour.$se_min))
	{
		$use_ts = mktime($se_hour, $se_min, 0, $now_date[1], ($now_date[2] - 1), $now_date[0]);
		$start_datetime = date("Y-m-d H:i:s", $use_ts);
	}
	else
	{
		$start_datetime = $now_parts[0]." ".$se_hour.":".$se_min.":00";
	}

	$void_int = ($submode == "voided")?"1":"0";
	if ($submode == "merged")
	{
		$void_int = "2";
	}

	$dining_room = in_array($submode, array("tables", "tabs", "tables_and_tabs", "ltg"));

	$vars = array();
	$vars['loc_id'] = $loc_id;
	$select_where = "SELECT (use_select_field) FROM `orders` WHERE `order_id` NOT LIKE '777%' ";
	if ($showAlertOnPos == 0 && $upcoming_ltg == 1) {
		$select_where .= "AND `order_id` NOT LIKE '%7-%' ";
	}
	if ($showAlertOnPos == 1 && $upcoming_ltg == 0) {
		$select_where .= "AND `server` IN ('MenuDrive','Loyalty App') ";
	}
	$select_where .= " AND `location_id` = '[loc_id]' AND `ioid` != '' AND `opened` != '0000-00-00 00:00:00' AND `opened` != '' AND ((";

	$closed_search = (in_array($submode, array( "closed", "voided", "merged" )));
	$isReskin = reqvar("reskin",0);
	$dateRangeCond = ($isReskin) ? 1:($date_range > 0);
	$use_date_range = ($dateRangeCond && (strlen($datetime_start) > 0) && (strlen($datetime_end) > 0))?true:false;
	if ($dining_room || in_array($submode, array("open", "open_deposits")))
	{
		if ($allLtgOrders) {
			$select_where .= "(not_reopened_but_closed)";
		} else {
			$select_where .= "(not_closed_or_reopened)";
		}
		switch ($submode)
		{
			case "tables": $select_where .= " AND `tab` != '1' AND `tablename` NOT LIKE 'Lavu ToGo%' AND `tablename` != ''"; break;
			case "tabs": $select_where .= " AND (`tab` = '1' OR `tabname` != '')"; break;
			case "open_deposits": $select_where .= " AND `deposit_status` != '0'"; break;
			case "ltg": $select_where .= " AND `tablename` LIKE 'Lavu To Go%'"; break;
			default: break;
		}
		if (in_array($submode, array( "tables", "tabs", "tables_and_tabs", "ltg" )))
		{
			$select_where .= " AND `tablename` NOT LIKE 'Quick Serve%'";
		}

		if ($use_date_range)  // LP-8165, 4.0+
		{
			// specific start and end date/times determined by caller
			$select_where .= " AND `opened` >= '".$datetime_start."' AND `opened` < '".$datetime_end."'";
		}
	}
	else if ($closed_search)
	{
		$select_where .= "`order_id` NOT LIKE 'Paid%' AND ((`reopened_datetime` = '' AND `reclosed_datetime` = '') OR (`reopened_datetime` != '' AND `reclosed_datetime` != ''))";

		if ($use_date_range)  // LP-5589, 4.0+
		{
			// specific start and end date/times determined by caller

			$select_where .= " AND `closed` >= '".$datetime_start."' AND `closed` < '".$datetime_end."'";
		}
		else if(!$isReskin) // default to current business day, used prior to 4.0+
		{
			if (!in_array($search_type, array( "last_four", "order_contents", "order_id" )))
			{
				$select_where .= " AND `closed` >= '".$start_datetime."'";
			}
		}
	}

	$three_months_ts = mktime($now_time[0], $now_time[1], $now_time[2], ($now_date[1] - 3), $now_date[2], $now_date[0]);
	$three_months_ago = date("Y-m-d H:i:s", $three_months_ts);

	$six_months_ts = mktime($now_time[0], $now_time[1], $now_time[2], ($now_date[1] - 6), $now_date[2], $now_date[0]);
	$six_months_ago = date("Y-m-d H:i:s", $six_months_ts);

	switch ($search_type)
	{
		case "last_four":

			if ($closed_search)
			{
				$select_where .= " AND `closed` >= '".$six_months_ago."'";
			}

			$matching_ioids = array();
			$s_vars = array(
				'last_four'			=> $search_string,
				'loc_id'			=> $loc_id,
				'six_months_ago'	=> $six_months_ago
			);

			$perform_search = lavu_query("SELECT `ioid` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `card_desc` = '[last_four]' AND `datetime` >= '[six_months_ago]'", $s_vars);
			while ($s_info = mysqli_fetch_assoc($perform_search))
			{
				$this_ioid = $s_info['ioid'];
				if (!in_array($this_ioid, $matching_ioids))
				{
					$matching_ioids[] = $s_info['ioid'];
				}
			}

			if (count($matching_ioids) > 0)
			{
				$select_where .= " AND `ioid` IN ('".implode("','", array_map('lavu_query_encode',$matching_ioids))."')";
			}
			else
			{
				$select_where .= " AND `ioid` = 'NO MATCHING TRANSACTIONS'";
			}

			break;

		case "order_contents":

			$vars['search_string'] = $search_string;

			if ($closed_search)
			{
				$select_where .= " AND `closed` >= '".$three_months_ago."'";
			}

			$select_where .= " AND (SUBSTRING(`opened`, 6, 5) LIKE '%[search_string]%' OR `togo_name` LIKE '%[search_string]%' OR `email` LIKE '%[search_string]%' OR `tablename` LIKE '%[search_string]%' OR `tabname` LIKE '%[search_string]%'";
			if ($closed_search)
			{
				$select_where .= " OR SUBSTRING(`closed`, 6, 5) LIKE '%[search_string]%'";
			}

			$matching_ioids = array();
			$s_vars = array(
				'loc_id'			=> $loc_id,
				'search_string'		=> $search_string,
				'three_months_ago'	=> $three_months_ago
			);

			$perform_search = lavu_query("SELECT `order_contents`.`ioid` FROM `orders` LEFT JOIN `order_contents` ON `order_contents`.`ioid` = `orders`.`ioid` WHERE `orders`.`order_id` NOT LIKE '777%' AND `orders`.`opened` >= '[three_months_ago]' AND `orders`.`location_id` = '[loc_id]' AND `order_contents`.`loc_id` = `orders`.`location_id` AND (`order_contents`.`item` LIKE '%[search_string]%' OR `order_contents`.`deposit_info` LIKE '%[search_string]%' OR `order_contents`.`options` LIKE '%[search_string]%' OR `order_contents`.`special` LIKE '%[search_string]%') GROUP BY `orders`.`ioid`", $s_vars);
			while ($s_info = mysqli_fetch_assoc($perform_search))
			{
				$this_ioid = $s_info['ioid'];
				if (!in_array($this_ioid, $matching_ioids))
				{
					$matching_ioids[] = $s_info['ioid'];
				}
			}

			if (count($matching_ioids) > 0)
			{
			    $select_where .= " OR `ioid` IN ('".implode("','", array_map('lavu_query_encode',$matching_ioids))."')";
			}

			$select_where .= ")";

			break;

		case "order_id":

			$vars['search_order_id'] = $search_string;
			$select_where .= " AND `order_id` = '[search_order_id]'";

			break;

		case "all": // LP-5589, 4.0+

			if (strlen($search_string) > 0)
			{
				$vars['search_string'] = $search_string;

				$search_datetime_end = $datetime_end;
				$search_datetime_start = $datetime_start;

				if (!$closed_search)
				{
					$search_datetime_end = date("Y-m-d H:i:s", (time() + (60 * 60 * 24))); // 24 hours from now
					$search_datetime_start = $six_months_ago;
				}

				$matching_ioids = array();
				$s_vars = array(
					'datetime_end'		=> $search_datetime_end,
					'datetime_start'	=> $search_datetime_start,
					'search_string'		=> $search_string,
					'loc_id'			=> $loc_id,
					'six_months_ago'	=> $six_months_ago,
					'three_months_ago'	=> $three_months_ago,
				);
				$dateTimeStr = "`datetime` >= '[six_months_ago]' ";
				if ($s_vars['datetime_start'] != '' && $s_vars['datetime_end'] != '') {
					$dateTimeStr = "`datetime` >= '[datetime_start]' AND `datetime` < '[datetime_end]' ";
				}
				$search_transactions = lavu_query("SELECT `ioid` FROM `cc_transactions` WHERE `loc_id` = '[loc_id]' AND `card_desc` = '[search_string]' AND " . $dateTimeStr, $s_vars);
				while ($s_info = mysqli_fetch_assoc($search_transactions))
				{
					$this_ioid = $s_info['ioid'];
					if (!in_array($this_ioid, $matching_ioids))
					{
						$matching_ioids[] = $s_info['ioid'];
					}
				}

				$datetime_field = "closed";
				if (!$closed_search)
				{
					$datetime_field = "opened";
					$s_vars['datetime_start'] = $three_months_ago;
				}
				$dateTimeStr = "`orders`.`opened` >= '[three_months_ago]' ";
				if ($s_vars['datetime_start'] != '' && $s_vars['datetime_end'] != '') {
					$dateTimeStr = "`orders`.`".$datetime_field."` >= '[datetime_start]' AND `orders`.`".$datetime_field."` < '[datetime_end]' ";
				}
				$search_contents = lavu_query("SELECT `order_contents`.`ioid` FROM `orders` LEFT JOIN `order_contents` ON `order_contents`.`ioid` = `orders`.`ioid` WHERE `orders`.`order_id` NOT LIKE '777%' AND " . $dateTimeStr . " AND `orders`.`location_id` = '[loc_id]' AND `order_contents`.`loc_id` = `orders`.`location_id` AND (`order_contents`.`item` LIKE '%[search_string]%' OR `order_contents`.`deposit_info` LIKE '%[search_string]%' OR `order_contents`.`options` LIKE '%[search_string]%' OR `order_contents`.`special` LIKE '%[search_string]%') GROUP BY `orders`.`ioid`", $s_vars);
				while ($s_info = mysqli_fetch_assoc($perform_search))
				{
					$this_ioid = $s_info['ioid'];
					if (!in_array($this_ioid, $matching_ioids))
					{
						$matching_ioids[] = $s_info['ioid'];
					}
				}

				$select_where .= " AND (SUBSTRING(`".$datetime_field."`, 6, 5) LIKE '%[search_string]%' OR `togo_name` LIKE '%[search_string]%' OR `email` LIKE '%[search_string]%' OR `tablename` LIKE '%[search_string]%' OR `tabname` LIKE '%[search_string]%' OR `order_id` LIKE '%[search_string]%' OR `card_desc` LIKE '%[search_string]%'";

				if (count($matching_ioids) > 0)
				{
					$select_where .= " OR `ioid` IN ('".implode("','", $matching_ioids)."')";
				}

				$select_where .= ")";
			}

			break;

		default:

			break;
	}

	if ($for_server_id != "all")
	{
		$vars['server_id'] = $for_server_id;
		//$vars['server_id'] = 0;
		$select_where .= " AND `server_id` = '[server_id]'";
	}

	if (strlen($server_ids) > 0) // LP-5589, 4.0+
	{
		$vars['server_ids'] = $server_ids;
		$select_where .= " AND `server_id` IN ([server_ids])";
	}

	if (strlen($order_types) > 0) // LP-5589, 4.0+
	{
		$vars['order_types'] = str_replace(",", "','", $order_types);
		$select_where .= " AND `togo_status` IN ('[order_types]')";
	}

	$order_by = " ORDER BY `opened` DESC";
	$limit_int = 20;
	if ($regular_pull)
	{
		$limit_int = 250;
	}
	else if ($ltg == "1" || $showAlertOnPos == "1")
	{
		$select_where .= " AND `togo_status` IN (SELECT `id` FROM `order_tags` WHERE `name` in ('".LavuToGo."','".PICKUP_LABEL."', '".DELIVERY_LABEL."', '".DINE_IN_LABEL."'))";
		$order_by = " ORDER BY `opened` DESC";
		$limit_int = 8;

		if ($upcoming_ltg || $showAlertOnPos)
		{
			$order_by = " ORDER BY `togo_time` ASC";
			$page = 0;
			$vars['ten_min_ago'] = date("Y-m-d H:i:s", mktime($now_time[0], ($now_time[1] - 10), $now_time[2], $now_date[1], $now_date[2], $now_date[0]));
			$vars['twenty_four_hr_from_now'] = date("Y-m-d H:i:s", mktime(($now_time[0] + 24), $now_time[1], $now_time[2], $now_date[1], $now_date[2], $now_date[0]));
			$select_where .= " AND `togo_time` > '[ten_min_ago]' AND `togo_time` <= '[twenty_four_hr_from_now]'";
		}
	}
	$limit_clause = ($page > 0)?" LIMIT ".(($page - 1) * $limit_int).", ".$limit_int:"";

	if ($sort_mode == "2") // 2 = OrderListSortToGoTime, LP-5589, 4.0+
	{
		$select_where .= " AND `togo_time` != ''";
	}

	$select_where .= " AND `void` = '".$void_int."')";

	if ($showing_ioids!="" && !in_array($search_type, array( "last_four", "order_contents", "order_id" )))
	{
		$ioid_array = explode(",", $showing_ioids);
		$select_where .= " OR `ioid` IN ('".implode("','", array_map('lavu_query_encode',$ioid_array))."')";
	}

	if (count($exclude_ioids) > 0)
	{
		$select_where .= " AND `ioid` NOT IN ('".implode("','", array_map('lavu_query_encode',$exclude_ioids))."')";
	}

	$select_where .= ")";

	//`opened`, `tablename`, `tabname`, `order_id`, `check_has_printed`, `server_id`
	$select_fields = "`ioid`, `order_id`, `tablename`, `alt_tablename`, `tabname`, `opened`, `closed`, `last_modified`, `last_mod_device`, `total`, `server`, `togo_status`, `togo_type`, `togo_time`, `active_device`, `sync_tag`, `last_mod_ts`, `order_status`";

	// We need tab value for order merge functionality
	if ($submode == 'open') {
		$select_fields .= ", `tab`";
	}

	$full_query = "";
	if ($dining_room || in_array($submode, array("open", "open_deposits")))
	{
		$notReopend = "`reopened_datetime` != '' AND `reclosed_datetime` = ''";
		if ($allLtgOrders) {
			$full_query = str_replace("(not_reopened_but_closed)", "`closing_device` = ''", $select_where)." UNION ".str_replace("(not_reopened_but_closed)", $notReopend, $select_where);
		} else {
			$full_query = str_replace("(not_closed_or_reopened)", "`closed` = '0000-00-00 00:00:00'", $select_where)." UNION ".str_replace("(not_closed_or_reopened)", $notReopend, $select_where);
		}

		if ($dining_room)
		{
			$select_fields = "`ioid`, `order_id`, `tablename`, `tabname`, `opened`, `closed`, `reopened_datetime`, `reclosed_datetime`, `last_modified`, `check_has_printed`, `server_id`, `active_device`, `sync_tag`, `last_mod_ts`, `order_status`";
		}
		else if ($regular_pull)
		{
			$select_fields = "`ioid`, `opened`, `last_modified`, `sync_tag`, `last_mod_ts`, `active_device`, `closed`, `reopened_datetime`, `reclosed_datetime`, `tab`, `tablename`, `order_status`";
		} else if ($upcoming_ltg) {
 			$select_fields = "`ioid`, `order_id`, `opened`, `last_modified`, `total`, `togo_type`, `togo_time`, `sync_tag`, `last_mod_ts`, `order_status`";
			$select_fields .= ",case togo_status when '3' then 'Pickup' when '4' then 'Delivery' else 'Dine-In' end as `order_type`, `order_status`";
 		}
	}
	else if (in_array($submode, array( "closed", "voided", "merged" )))
	{
		$order_by = " ORDER BY `closed` DESC";
		$full_query = $select_where;
	}

	if (strlen($sort_mode) > 0)  // LP-5589, 4.0+
	{
		switch ($sort_mode)
		{
			case "1": // OrderListSortOldest

				$order_by = str_replace(" DESC", " ASC", $order_by);
				break;

			case "2": // OrderListSortToGoTime

				$order_by = " ORDER BY `togo_time` ASC";
				break;

			default:
				break;
		}
	}

	//error_log("count_query: ".$full_query);

	if (in_array($submode, array( "open", "open_deposits", "closed", "voided", "merged" )) && isset($location_info['order_list_cell_fields']))
	{
		$olcf = explode(",", $location_info['order_list_cell_fields']);
		foreach ($olcf as $field)
		{
			if (strlen($field)>0 && !strstr($select_fields, "`".$field."`"))
			{
				$select_fields .= ", `".$field."`";
			}
		}
	}

	if (!$dining_room && !$regular_pull && !$upcoming_ltg && $page>0)
	{
		$get_total_count = lavu_query(str_replace("(use_select_field)", "`id`", $full_query." GROUP BY `ioid`"), $vars);
		$total_count = mysqli_num_rows($get_total_count);
	}

	//error_log("page_query: ".$full_query.$order_by.$limit_clause);

	$location_lds_debug = false;
	if (isset($location_info['lds_debug']))
	{
		$location_lds_debug = ($location_info['lds_debug'] == "1");
	}
	$lavu_query_should_log = $location_lds_debug;
	$get_order_list = lavu_query(str_replace("(use_select_field)", $select_fields, $full_query).$order_by.$limit_clause, $vars);
	if (mysqli_num_rows($get_order_list) > 0)
	{
		$listed_ioids = array();

		$json_status = "success";
		while ($order_stub = mysqli_fetch_assoc($get_order_list))
		{

			$order_stub['cloud_load'] = "1";
			$order_id = $order_stub['order_id'];
			// Below block of code is being used for getting menu items for merging order functionality
			// This request will come with submode == open
			if ($submode == 'open') {
				$order_ids[] = $order_id;
			}
			if ($upcoming_ltg || $showAlertOnPos)
			{
				if (isset($order_list[$order_id]))
				{
					$use_stub = authoritativeOrderStub($order_list[$order_id], $order_stub);
					if ($use_stub == 1)
					{
						continue;
					}
				}

				$order_list[$order_id] = $order_stub;
			}
			else
			{
				$ioid = $order_stub['ioid'];

				$listed_index = array_search($ioid, $listed_ioids);
				if ($listed_index !== FALSE)
				{
					$compare_stub = $order_list[$listed_index];
					$use_stub = authoritativeOrderStub($compare_stub, $order_stub);
					if ($use_stub == 1)
					{
						continue;
					}

					array_splice($listed_ioids, $listed_index);
					array_splice($order_list, $listed_index);
				}

				$listed_ioids[]	= $ioid;
				$order_list[]	= $order_stub;
			}
		}

		if (reqvar("get_first_order_full_data", false) == "1")
		{
			$first_stub = $order_list[0];
			$first_order_data = loadOrderByIOID($first_stub['ioid'], $mode);
			if ($first_order_data['status'] == "success")
			{
				$order_list[0] = $first_order_data[$first_stub['ioid']]['order'];
			}
		}
	}
	else if (!$get_order_list)
	{
		error_log("mysqli_error (".$data_name."): ".lavu_dberror());
	}

	if ($dining_room || $upcoming_ltg || $page==0)
	{
		$total_count = count($order_list);
	}

	// Below block of code is being used for getting menu items for merging order functionality
	// This request will come with submode == open
	if (!empty($order_ids) && ($submode == 'open' || $submode == 'ltg')) {
		$orderItems = array();
		$order_str = "'" . implode ( "', '", $order_ids ) . "'";
		$orderContents = lavu_query("SELECT order_id, GROUP_CONCAT(item ORDER BY item SEPARATOR ', ' ) AS items FROM order_contents WHERE  `order_id` IN (" . $order_str . ") GROUP BY order_id ORDER BY item");
		while ($ocontents_read = mysqli_fetch_assoc($orderContents)) {
			$orderItems[$ocontents_read['order_id']] = $ocontents_read['items'];
		}

		$updateOrderList = ['open_orders' => [], 'closed_orders' => []];
		foreach ($order_list as $key => $detials) {
			$itemName = (isset($orderItems[$detials['order_id']])) ? $orderItems[$detials['order_id']] : '';
			if ($allLtgOrders) {
				$orderType = 'closed_orders';
				if (isset($order_list[$key]['order_status']) && $order_list[$key]['order_status'] === 'open') {
					$orderType = 'open_orders';
				}
				$updateOrderList[$orderType][$key] = $detials;
				$updateOrderList[$orderType][$key]['items'] = $itemName;
			} else {
				$order_list[$key]['items'] = $itemName;
			}
		}
	}
	$response = array(
		"json_status"	=> $json_status,
		"order_list"	=> $allLtgOrders ? $updateOrderList : $order_list,
		"PHPSESSID"		=> session_id(),
		"total_count"	=> $total_count?$total_count:0,
		"ts"			=> (string)time()
	);

	if (count($first_order_data) > 0)
	{
		$response['first_order_data'] = $first_order_data[$first_stub['ioid']];
	}

	return $response;
}

function authoritativeOrderStub($stub1, $stub2)
{
	if (is_array($stub1) && !is_array($stub2))
	{
		return 1;
	}

	if (!is_array($stub1) && is_array($stub2))
	{
		return 2;
	}

	$sync_tag1 = arrayValueForKey($stub1, "sync_tag");
	$sync_tag2 = arrayValueForKey($stub2, "sync_tag");

	if ($sync_tag1 > $sync_tag2)
	{
		return 1;
	}
	else if ($sync_tag1 < $sync_tag2)
	{
		return 2;
	}

	$last_mod_ts1 = arrayValueForKey($stub1, "last_mod_ts");
	$last_mod_ts2 = arrayValueForKey($stub2, "last_mod_ts");

	if ($last_mod_ts1 > $last_mod_ts2)
	{
		return 1;
	}
	else if ($last_mod_ts1 < $last_mod_ts2)
	{
		return 2;
	}

	return 1;
}

function loadOrderByIOID($ioid, $mode)
{
	if (is_array($ioid)) {
		$ioid = implode("','", $ioid);
	}

	global $cc_companyid;
	global $data_name;
	global $loc_id;

	$status_key = ($mode == "load_order")?"json_status":"status";

	$loadOrder = array();
	$ioIds = array();
	if (empty($ioid))
	{
		$loadOrder[$status_key] = "EmptyIOID";
	}
	else
	{
		if (($get_order = lavu_query("SELECT * FROM `orders` WHERE `ioid` IN ('".$ioid."') ORDER BY `sync_tag` DESC, `last_mod_ts` DESC, `id` DESC")) !== FALSE)
		{
			if (mysqli_num_rows($get_order) > 0)
			{
				$orderDataContent = array();
				if ($ioid) {
					//Order Content Data LP-10676
					if (($get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `ioid` IN ('".$ioid."') ")) !== FALSE) {
						if (mysqli_num_rows($get_contents) > 0) {
							while ($info = mysqli_fetch_assoc($get_contents))
							{
								$orderDataContent[$info['ioid']][] = $info;
							}
						}
						mysqli_free_result( $get_contents );
					}

					//Order modifiers used data
					if (($get_modifiers = lavu_query("SELECT * FROM `modifiers_used` WHERE `ioid` IN ('".$ioid."') ")) !== FALSE) {
						if (mysqli_num_rows($get_modifiers) > 0) {
							$orderDataMods = array();
							while ($infomod = mysqli_fetch_assoc($get_modifiers))
							{
								$orderDataMods[$infomod['ioid']][] = $infomod;
							}
						}
						mysqli_free_result( $get_modifiers );
					}

					//Order cc_transaction data
					$orderDataTrans = array();
					if (($get_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `ioid` IN ('".$ioid."') AND `action` != ''")) !== FALSE) {
						if (mysqli_num_rows($get_transactions) > 0) {
							while ($infotrans = mysqli_fetch_assoc($get_transactions))
							{
								$orderDataTrans[$infotrans['ioid']][] = $infotrans;
							}
						}
						mysqli_free_result( $get_transactions );
					}

					//Order alternate payment data
					$orderAlternateTrans = new stdClass();
					if (($get_alt_tx_totals = lavu_query("SELECT * FROM `alternate_payment_totals` WHERE `ioid` IN ('".$ioid."') ")) !== FALSE) {
						if (mysqli_num_rows($get_alt_tx_totals) > 0) {
							while ($infoalter = mysqli_fetch_object($get_alt_tx_totals))
							{
								$chk = (string)$infoalter->check;
								$ptid = (string)$infoalter->pay_type_id;
								$nioid = (string)$infoalter->ioid;
								if (!isset($orderAlternateTrans->{$nioid}->{$chk})) {
									$orderAlternateTrans->$nioid->{$chk} = array();
								}
								$orderAlternateTrans->$nioid->{$chk}[$ptid] = $infoalter;
							}
						}
						mysqli_free_result( $get_alt_tx_totals );
					}

					//Order split check data
					$orderSplitCheck = new stdClass();
					if (($get_check_details = lavu_query("SELECT * FROM `split_check_details` WHERE `ioid` IN ('".$ioid."') ")) !== FALSE) {
						if (mysqli_num_rows($get_check_details) > 0) {
							while ($infosplit = mysqli_fetch_object($get_check_details))
							{
								$sioid = (string)$infosplit->ioid;
								if (!isset($orderSplitCheck->$sioid->{(string)$infosplit->check})) {
									$orderSplitCheck->$sioid->{(string)$infosplit->check} = array();
								}
								$orderSplitCheck->$sioid->{(string)$infosplit->check} = $infosplit;
							}
						}
						mysqli_free_result( $get_check_details );
					}

					//Order transaction extend data
					$orderTransExtend = array();
					if (($get_transactions_ext = lavu_query("SELECT cc_ext.* FROM `cc_transactions_ext` AS cc_ext LEFT JOIN `cc_transactions` AS cc ON cc_ext.`transaction_id` = cc.`transaction_id` WHERE cc.`pay_type` = 'Cash' AND cc.`ioid` IN ('".$ioid."')")) !== FALSE) {
						if (mysqli_num_rows($get_transactions_ext) > 0) {
							while ($infoextend = mysqli_fetch_assoc($get_transactions_ext))
							{
								$orderTransExtend[$ioid][] = $infoextend;
							}
						}
						mysqli_free_result( $get_transactions_ext );
					}
				}

				while ($rowod = mysqli_fetch_assoc($get_order)) {
					//IOID wise response build
					$ioIds[] = $rowod['ioid'];
					$loadOrder[$rowod['ioid']] = array();
					$orderInfo = array();
					$orderInfo[$status_key] = 'success';
					//Order data
					$orderInfo['order'] = array();
					$od = array();
					$rowod['needs_sync'] = "0";
					if (substr($rowod['location'], 0, 1) != "{")
					{
						$rowod['location'] = "{".$cc_companyid.":".$data_name.":".$loc_id."}";
					}
					$od = $rowod;
					$orderInfo['order'] = $od;
					//Order content data
					$orderInfo['contents'] = array();
					if (count($orderDataContent[$rowod['ioid']]) > 0) {
						$orderInfo['contents'] = $orderDataContent[$rowod['ioid']];
					}
					//Order modifier data
					$orderInfo['modifiers'] = array();
					if (count($orderDataMods[$rowod['ioid']]) > 0) {
						$orderInfo['modifiers'] = $orderDataMods[$rowod['ioid']];
					}
					//Order transaction data
					$orderInfo['transactions'] = array();
					if (count($orderDataTrans[$rowod['ioid']]) > 0) {
					$orderInfo['transactions'] = $orderDataTrans[$rowod['ioid']];
					}
					//Order alternate payment data
					$orderInfo['alternate_tx_totals'] = new stdClass();
					if (count($orderAlternateTrans->{$rowod['ioid']}) > 0) {
					$orderInfo['alternate_tx_totals'] = $orderAlternateTrans->{$rowod['ioid']};
					}
					//Order split check data
					$orderInfo['check_details'] = new stdClass();
					if (count($orderSplitCheck->{$rowod['ioid']}) > 0) {
					$orderInfo['check_details'] = $orderSplitCheck->{$rowod['ioid']};
					}
					//Order transaction extend data
					$orderInfo['transactionsExtend'] = array();
					if (count($orderTransExtend[$rowod['ioid']]) > 0) {
					$orderInfo['transactionsExtend'] = $orderTransExtend[$rowod['ioid']];
					}
					//Check for mode load_order
					if ($mode == "load_order")
					{
						$orderInfo['PHPSESSID'] = session_id();
					}
					$loadOrder[$rowod['ioid']] = $orderInfo;
				}
			mysqli_free_result( $get_order );
			}
		}
	}
	//Previous IOID's
	$ioidPrevious = explode("','", $ioid);
	//Non existing ioid filter by db
	$ioidFinal = array_diff($ioidPrevious, $ioIds);
	$ioidFinal = array_values($ioidFinal);
	if (count($ioidFinal) > 0) {
		foreach ($ioidFinal as $ioidVal) {
		$loadOrder[$ioidVal][$status_key] = 'NoMatch';
		if ($mode == "load_order") {
			$loadOrder[$ioidVal]['PHPSESSID'] = session_id();
		}
		}
	}

	return $loadOrder;
}

function pullOrders($loc_id, $max_orders, $pull_ioids, $pull_open_orders, $last_pull_ts, $uuid)
{
	global $location_info;

	$response = array( "json_status"=>"success" );

	if ($max_orders == 0) {
		$max_orders = 5;
	}
	$available_pull_slots = ($max_orders - count($pull_ioids));

	$exclude_from_open_orders_check = $pull_ioids;
	$recently_changed_order_list = array();

	if ($last_pull_ts > 0)
	{
		$check_recently_changed = lavu_query("SELECT `ioid`, `last_modified`, `sync_tag`, `last_mod_ts`, `active_device`, `closed`, `reopened_datetime`, `reclosed_datetime`, `tab`, `tablename` FROM `orders` WHERE `order_id` NOT LIKE '777%' AND `location_id` = '[1]' AND `ioid` != '' AND `opened` != '0000-00-00 00:00:00' AND `opened` != '' AND `pushed_ts` >= '[2]' AND `last_mod_device` != '[3]' AND `ioid` NOT IN ('".implode("','", $pull_ioids)."') ORDER BY `last_modified` DESC", $loc_id, $last_pull_ts, $uuid);
		if ($check_recently_changed) {
			while ($info = mysqli_fetch_assoc($check_recently_changed))
			{
				$ioid = $info['ioid'];
				$exclude_from_open_orders_check[] = $ioid;
				$is_open = ($info['closed']=="" || $info['closed']=="0000-00-00 00:00:00" || (!empty($info['reopened_datetime']) && empty($info['reclosed_datetime'])));
				$recently_changed_order_list[] = array(
					$ioid,
					$info['last_modified'],
					$info['sync_tag'],
					$info['last_mod_ts'],
					$info['active_device'],
					$info['closed'],
					$info['reopened_datetime'],
					$info['reclosed_datetime'],
					$is_open,
					$info['tab'],
					$info['tablename']
				);
			}
		}
	}

	$open_order_list = array();
	if ($pull_open_orders == 1)
	{
		$device_time = determineDeviceTime($location_info, $_REQUEST);

		$criteria = array(
			"loc_id"		=> $loc_id,
			"device_time"	=> $device_time,
			"submode"		=> "open",
			"search_type"	=> "",
			"search_string"	=> "",
			"for_server_id"	=> "all",
			"page"			=> 1,
			"ltg"			=> "0",
			"upcoming_ltg"	=> false,
			"showing_ioids"	=> "",
			"regular_pull"	=> true,
			"exclude_ioids" => $exclude_from_open_orders_check
		);

		$ool_info = loadOrderList($criteria);
		if (!empty($ool_info['order_list']))
		{
			$open_order_list = $ool_info['order_list'];
		}
	}

	$recent_order_count = count($recently_changed_order_list);
	$recent_order_pull_slots = 0;

	$open_order_count = count($open_order_list);
	$open_order_pull_slots = 0;

	if ($available_pull_slots > 0)
	{
		if ($recent_order_count > 0)
		{
			$recent_order_pull_slots = MAX(ceil($available_pull_slots / 2), ($available_pull_slots - $open_order_count));
		}
		$open_order_pull_slots = ($available_pull_slots - $recent_order_pull_slots);
	}

	$order_data = array();
	$compare_ioids = array();
	$orderDataInfo = array();
	//Order info LP-10676
	$order_data = loadOrderByIOID($pull_ioids, "load_order_list");
	$recentIoids = array();
	for ($r = 0; $r < $recent_order_count; $r++)
	{ // pick from most recently changed at beginning of list
		$rcolr = $recently_changed_order_list[$r];
		$ioid = $rcolr[0];

		if ($r < $recent_order_pull_slots)
		{
			$recentIoids[] = $ioid;
		}
		else
		{
			$last_modified		= $rcolr[1];
			$sync_tag			= $rcolr[2];
			$last_mod_ts		= $rcolr[3];
			$active_device		= $rcolr[4];
			$closed				= $rcolr[5];
			$reopened_datetime	= $rcolr[6];
			$reclosed_datetime	= $rcolr[7];

			$info_string = $last_modified.";".$sync_tag.";".$last_mod_ts.";".$active_device.";".$closed.";".$reopened_datetime.";".$reclosed_datetime;
			if ($pull_open_orders == 1)
			{
				$is_open		= $rcolr[8];
				$is_tab			= $rcolr[9];
				$tablename		= $rcolr[10];

				$info_string .= ";".($is_open?"1":"0").";".$is_tab.";".$tablename;
			}

			$compare_ioids[$ioid] = $info_string;
		}
	}

	//Recent Orders LP-10676
	$recentOrders = array();
	$recentOrderData = array();
	$countRecentIoids = count($recentIoids);
	if ($recent_order_count > 0 && $countRecentIoids > 0) {
		$recentOrderData = loadOrderByIOID($recentIoids, "load_order_list");
		$order_data = array_merge($order_data, $recentOrderData);
	}

	//Open Order Ioid's LP-10676
	$openOrderIds = array();
	$openOrderData = array();
	for ($o = ($open_order_count - 1); $o >= 0; $o--)
	{ // pick from first opened at end of list
		$oolo = $open_order_list[$o];
		$ioid = $oolo['ioid'];
		if (($open_order_count - $o) < $open_order_pull_slots)
		{
			$openOrderIds[] = $ioid;
		}
		else
		{
			$last_modified		= $oolo['last_modified'];
			$sync_tag			= $oolo['sync_tag'];
			$last_mod_ts		= $oolo['last_mod_ts'];
			$active_device		= $oolo['active_device'];
			$closed				= $oolo['closed'];
			$reopened_datetime	= $oolo['reopened_datetime'];
			$reclosed_datetime	= $oolo['reclosed_datetime'];

			$info_string = $last_modified.";".$sync_tag.";".$last_mod_ts.";".$active_device.";".$closed.";".$reopened_datetime.";".$reclosed_datetime;
			if ($pull_open_orders == 1) {

				$is_open		= ($oolo['closed']=="" || $oolo['closed']=="0000-00-00 00:00:00" || (!empty($oolo['reopened_datetime']) && empty($oolo['reclosed_datetime'])));
				$is_tab			= $oolo['tab'];
				$tablename		= $oolo['tablename'];

				$info_string .= ";".($is_open?"1":"0").";".$is_tab.";".$tablename;
			}

			$compare_ioids[$ioid] = $info_string;
		}
	}

	//Open Order Data LP-10676
	$countOpenOrderIoid = count($openOrderIds);
	if ($open_order_count > 0 && $countOpenOrderIoid > 0) {
		$openOrderData = loadOrderByIOID($openOrderIds, "load_order_list");
		$order_data = array_merge($order_data, $openOrderData);
	}

	if (count($order_data) > 0)
	{
		$response['order_data'] = $order_data;
	}

	if (count($compare_ioids) > 0)
	{
		$response['compare_ioids'] = $compare_ioids;
	}

	return $response;
}

function claimAndPullIOID($ioid, $device_id)
{
	global $device_time;

	$now_ts = time();
	$claimPullData = array();
	$update_order = lavu_query("UPDATE `orders` SET `owned_by_uuid` = '[1]', `last_mod_device` = '[1]', `last_modified` = '[2]', `last_mod_ts` = '[3]', `pushed_ts` = '[3]' WHERE `ioid` = '[4]'", $device_id, $device_time, $now_ts, $ioid);

	$order_data = loadOrderByIOID($ioid, "ownership_request");
	$claimPullData = $order_data[$ioid];
	$claimPullData['order']['owned_by_uuid']	= $device_id;
	$claimPullData['order']['last_mod_device']	= $device_id;
	$claimPullData['order']['last_modified']	= $device_time;
	$claimPullData['order']['last_mod_ts']		= $now_ts;
	$claimPullData['order']['pushed_ts']		= $now_ts;

	return $claimPullData;
}

function pullNextOrderForAutoSend($loc_id, $uuid) // LP-3129
{
	$minimum_last_pushed_ts = (time() - 3600); // Only consider order records that have been updated within the last hour.

	$check_auto_send_orders = lavu_query("SELECT `id`, `ioid`, `auto_send_status` FROM `orders` WHERE `location_id` = '[1]' AND `void` != '3' AND `send_status` = '1' AND `auto_send_status` LIKE '1%' AND `pushed_ts` > '".$minimum_last_pushed_ts."' ORDER BY `pushed_ts` ASC", $loc_id);
	if ($check_auto_send_orders === FALSE)
	{
		return FALSE;
	}

	if (mysqli_num_rows($check_auto_send_orders) == 0)
	{
		return FALSE;
	}

	$checked_ioids = array();
	$auto_send_ioid = FALSE;

	while ($order_stub = mysqli_fetch_assoc($check_auto_send_orders))
	{
		$row_id	= $order_stub['id'];
		$ioid	= $order_stub['ioid'];

		if (in_array($ioid, $checked_ioids))
		{
			continue;
		}

		$checked_ioids[] = $ioid;

		if (($auto_send_status != "1") && !autoSendStatusIsExpired($auto_send_status))
		{
			continue;
		}

		$double_check_auto_send_status = lavu_query("SELECT `send_status`, `auto_send_status` FROM `orders` WHERE `id` = '[1]'", $row_id);
		if ($check_auto_send_orders === FALSE)
		{
			error_log("double_check_auto_send_status query failed");
			continue;
		}

		if (mysqli_num_rows($double_check_auto_send_status) == 0)
		{
			continue;
		}

		$dc_order_stub = mysqli_fetch_assoc($double_check_auto_send_status);
		if ($dc_order_stub['send_status'] != "1")
		{
			continue;
		}

		$dc_auto_send_status = $dc_order_stub['auto_send_status'];
		if (($dc_auto_send_status != "1") && !autoSendStatusIsExpired($dc_auto_send_status))
		{
			continue;
		}

		$claim_for_auto_send = lavu_query("UPDATE `orders` SET `auto_send_status` = '[1]' WHERE `ioid` = '[2]'", "1:".$uuid.":".time(), $ioid);
		if ($claim_for_auto_send === FALSE)
		{
			continue;
		}

		$auto_send_ioid = $ioid;
	}

	if (!$auto_send_ioid)
	{
		return FALSE;
	}

	$combined_order_info = loadOrderByIOID($auto_send_ioid, "load_order_list");
	if ($combined_order_info[$auto_send_ioid]['status'] == "success")
	{
		return $combined_order_info[$auto_send_ioid];
	}

	return FALSE;
}

function autoSendStatusIsExpired($auto_send_status)
{
	$auto_send_status_parts = explode(":", $auto_send_status);
	if (count($auto_send_status_parts) <= 2)
	{
		return TRUE;
	}

	$status_flag	= $auto_send_status_parts[0];
	$auto_send_uuid	= $auto_send_status_parts[1];
	$auto_send_ts	= $auto_send_status_parts[2];

	if ($status_flag != "1")
	{
		return FALSE;
	}

	if ($auto_send_ts < (time() - 300)) // Unresolved auto send claims expire after 5 minutes
	{
		return TRUE;
	}

	return FALSE;
}

function getLocationDevices($loc_id)
{
	$devices_info = array();

	$get_devices_info = lavu_query("SELECT `UUID`, `model`, `name`, `ip_address`, `prefix`, `app_name`, `last_checkin`, `poslavu_version` FROM `devices` WHERE `loc_id` = '[1]' AND `inactive` = '0'", $loc_id);
	if (mysqli_num_rows($get_devices_info) > 0)
	{
		while ($di = mysqli_fetch_object($get_devices_info))
		{
			$devices_info[$di->UUID] = array($di->model, $di->name, $di->ip_address, $di->app_name, $di->last_checkin, $di->poslavu_version);
		}
	}

	return $devices_info;
}

function killLavuSession()
{
	session_destroy();

	$_SESSION = array();
}

function cappedCheckCount($check_count) {

	// the app caps new checks at 50, but some extra margin for merges may be desirable

	return (int)MAX(MIN(100, ($check_count * 1)), 1);
}

function debugFlagsForDataname($data_name, $loc_id)
{
	$get_debug_flags = lavu_query("SELECT `value` FROM `config` WHERE `type` = 'system_setting' AND `setting` = 'debug_flags' AND `location` = '[1]'", $loc_id);
	if ($get_debug_flags)
	{
		if (mysqli_num_rows($get_debug_flags) > 0)
		{
			$info = mysqli_fetch_assoc($get_debug_flags);

			return $info['value'];
		}
	}

	return "";
}

function updateActiveDevice($claim, $ioid, $order_id, $uuid, $last_modified, $app_version)
{
	global $data_name;

	$did_update = false;

	if (!empty($ioid) && !empty($uuid))
	{
		$ok_to_update = false;

		$check_last_modified = lavu_query("SELECT `id`, `last_modified` FROM `orders` WHERE `ioid` = '[1]' ORDER BY `sync_tag` DESC, `last_mod_ts` DESC, `id` DESC", $ioid);
		if ($check_last_modified)
		{
			$row_count = mysqli_num_rows($check_last_modified);

			if ($row_count > 0)
			{
				$info = mysqli_fetch_assoc($check_last_modified);

				$claim_only_prefix		= "CLAIM-ONLY-";
				$compare_last_modified	= str_replace($claim_only_prefix, "", $last_modified);
				$db_last_modified		= str_replace($claim_only_prefix, "", $info['last_modified']);

				$ok_to_update = ($compare_last_modified > $db_last_modified);

				if ($row_count > 1)
				{
					error_log("multiple rows found for IOID ".$ioid." at ".$data_name);

					$delete_extra = lavu_query("DELETE FROM `orders` WHERE `ioid` = '[1]' AND `id` != '[2]'", $ioid, $info['id']);
				}
			}
		}

		if ($ok_to_update)
		{
			$claim_uuid = $claim?$uuid:"";

			lavu_query("UPDATE `orders` SET `active_device` = '[1]', `pushed_ts` = '[2]', `last_modified` = '[3]' WHERE `ioid` = '[4]'", $claim_uuid, time(), $last_modified, $ioid);
		}
	}
}

function clearLeftoverOrderClaims($device_time, $uuid, $not_ioid="")
{
	global $app_version;

	if (appVersionCompareValue($app_version) < 30200)
	{
		return;
	}

	$device_time	= "CLAIM-ONLY-".$device_time;
	$exclude_ioid	= (empty($not_ioid))?"":" AND `ioid` != '[4]'";

	lavu_query("UPDATE `orders` SET `active_device` = '', `pushed_ts` = '[1]', `last_modified` = '[2]' WHERE `active_device` = '[3]'".$exclude_ioid, time(), $device_time, $uuid, $not_ioid);
}

function getListOfOrdersWithoutTransactionID()
{
	$result = lavu_query("SELECT * FROM `orders` WHERE `transaction_id`='' AND `closed` != '0000-00-00 00:00:00'");
	$returnArr = array();
	while ($currRow = mysqli_fetch_assoc($result))
	{
		$returnArr[] = $currRow;
	}

	return $returnArr;
}

function getLastZReportID($register, $register_name)
{
    $result = lavu_query("SELECT MAX(`report_id`) as `lastZReport` FROM `fiscal_reports` WHERE `register` = '".$register."' OR `register`='".$register_name."'");
    $returnArr = array();
    $returnArr['report_id'] = "0";
    while($currRow = mysqli_fetch_assoc($result))
	{
        error_log(print_r($currRow, 1));
        if (!empty($currRow['lastZReport']))
		{
            $returnArr['report_id'] = $currRow['lastZReport'];
        }
    }

	return json_encode($returnArr);
}

function getMissingZReportIDs($current_report_id, $register, $register_name)
{
	$missing_ids = array();

	$get_past_records = lavu_query("SELECT COUNT(`id`) AS `count`, `report_id` FROM `fiscal_reports` WHERE `report_id` >= '1' AND (`register` = '".$register."' OR `register` = '".$register_name."') GROUP BY `report_id` ORDER BY `report_id` ASC");
	if ($get_past_records)
	{
		$found_ids = array();

		while ($info = mysqli_fetch_assoc($get_past_records))
		{
			// eventually use `count` as a rough validation, as we expect there to be 5 rows for each type of report?

			$found_ids[] = (int)$info['report_id'];
		}

		for ($r = 1; $r < $current_report_id; $r++)
		{
			if (!in_array($r, $found_ids))
			{
				$missing_ids[] = $r;
				if (count($missing_ids) >= 50)
				{
					break;
				}
			}
		}
	}

	return $missing_ids;
}

function getInvoiceDetails ( $invoiceNumberArr ) {
    if ( count( $invoiceNumberArr ) > 0 ) {
        $invoiceNumberStr = implode("','", $invoiceNumberArr);
        $invoiceNumberStr = "'".$invoiceNumberStr."'";
        $result = lavu_query("select A.order_id, B.refund, B.transaction_id, A.`original_id` from orders as A left join `fiscal_transaction_log` as B on ( A.`order_id`=B.order_id AND B.type in ('TiqueFacturaA','TiqueNotaCreditoA')) where B.transaction_id in (".$invoiceNumberStr.")");
        $returnArr = array();
        while ($currRow = mysqli_fetch_assoc($result) ) {
            if ( !empty($currRow['original_id']) ) {
                $rowParts = explode('|o|',$currRow['original_id']);
                $cuitDataObj = json_decode($rowParts[5]);
                $returnArr[$currRow['transaction_id']]['name'] = (isset($rowParts[2])) ? $rowParts[2] : "Unavailable";
                $returnArr[$currRow['transaction_id']]['refund'] = (isset($currRow['refund'])) ? $currRow['refund'] : "0.00";
                $returnArr[$currRow['transaction_id']]['CUIT'] = (isset($cuitDataObj->field10)) ? $cuitDataObj->field10 : "Unavailable";
            }
        }
    }
    return $returnArr;
}

function insertNewZReport( $ZReportJSON ) {
    $ZReport = json_decode($ZReportJSON, true);
    $invoiceNumberArr = Array();
    $invoiceInformationArr = Array();

    if ( count($ZReport) > 0 ) {
        foreach ($ZReport as $reportData) {
            foreach ($reportData as $eachRow) {
                if (($eachRow['report_type']=="TiqueFacturaA" || $eachRow['report_type']=="TiqueNotaCreditoA")  && isset($eachRow['first_invoice']) && $eachRow['first_invoice'] != 'Unavailable') {
                    $invoiceNumberArr[] = $eachRow['first_invoice'];
                }
            }
        }
        if ( count($invoiceNumberArr) > 0 ) {
            $invoiceInformationArr = getInvoiceDetails($invoiceNumberArr);
        }
    }

    foreach ( $ZReport as $oneReport ) {
        foreach ( $oneReport as $value ) {
            $ZReportDetails = $value;
            $report_type = $ZReportDetails['report_type'];
            if ( $ZReportDetails['report_type'] == 'others' ) {
            	   $PVnumber = $ZReportDetails['numberpos'];
                continue;
            }

			$register = isset($ZReportDetails['register_name']) ? $ZReportDetails['register_name'] : "Unavailable";
            $firstInvoice = isset($ZReportDetails['first_invoice']) ? $ZReportDetails['first_invoice'] : "Unavailable";
            $lastInvoice = isset($ZReportDetails['last_invoice']) ? $ZReportDetails['last_invoice'] : "Unavailable";
            $fiscalDate = substr($ZReportDetails['Date'], 0, 2) . "-" . substr($ZReportDetails['Date'], 2, 2) . "-" . substr($ZReportDetails['Date'], 4, 2) . " " . substr($ZReportDetails['Date'], 7, 2) . ":" . substr($ZReportDetails['Date'], 9, 2) . ":" . substr($ZReportDetails['Date'], 11, 2);
            $reportDate = date("Y-m-d H:i:s", strtotime($fiscalDate));
            $net_total = $total = $tax = $refund_total = $refund_net = $refund_tax = $untax_refund ="0.00";

            if ($report_type=="TiqueNotaCreditoB" || $report_type=="TiqueNotaCreditoA") {
            	   $refund_total= isset($ZReportDetails['total']) ? $ZReportDetails['total'] : "0.00";
            	   $refund_net = isset($ZReportDetails['net_total']) ? $ZReportDetails['net_total'] : "0.00";
            	   $refund_tax = isset($ZReportDetails['tax']) ? $ZReportDetails['tax'] : "0.00";
            }
            else {
            	   if ($report_type=="TiqueFacturaA") {
            	       $refund_total = isset($invoiceInformationArr[$ZReportDetails['first_invoice']]['refund']) ? $invoiceInformationArr[$ZReportDetails['first_invoice']]['refund'] : "0.00";
            	   }
            	   $net_total = isset($ZReportDetails['net_total']) ? $ZReportDetails['net_total'] : "0.00";
            	   $total = isset($ZReportDetails['total']) ? $ZReportDetails['total'] : "0.00";
            	   $tax = isset($ZReportDetails['tax']) ? $ZReportDetails['tax'] : "0.00";
            	   if ($report_type == "Consolidated") {
            	       $refund_total = isset($ZReportDetails['refund_total']) ? $ZReportDetails['refund_total'] : "0.00";
            	       $refund_net = isset($ZReportDetails['refund_net']) ? $ZReportDetails['refund_net'] : "0.00";
            	       $refund_tax = isset($ZReportDetails['refund_tax']) ? $ZReportDetails['refund_tax'] : "0.00";
            	   }
            }

            if ($report_type=="TiqueFacturaA" || $report_type=="TiqueNotaCreditoA") {
                $name = $invoiceInformationArr[$firstInvoice]['name'];
                $cuit = $invoiceInformationArr[$firstInvoice]['CUIT'];
            }
            else {
                   $name = "Consumidor Final";
                   $cuit = "Consumidor Final";
            }

            $untax_total = isset($ZReportDetails['untax_total']) ? $ZReportDetails['untax_total'] : "0.00";

            if ( isset($ZReportDetails['untax_refund']) && $ZReportDetails['untax_refund']!="" ) {
            	   $untax_refund = $ZReportDetails['untax_refund'];
            }

            if ($reportDate === false) {
                $reportDate = date("Y-m-d H:i:s", strtotime($ZReportDetails['Date']));
            }

			$queryString = "Insert into `fiscal_reports` set " .
                "`register` = '" . $register .
                "',`pv_number` = '" . $PVnumber .
                "',`first_invoice` = '" . $firstInvoice .
                "',`last_invoice` = '" . $lastInvoice .
                "',`fiscal_day` = '" . $ZReportDetails['Date'] .
                "',`datetime` = '" . $reportDate .
                "',`net_total` = '" . $net_total .
                "',`untax_total` = '" . $untax_total .
                "',`total` = '" . $total .
                "',`tax` = '" . $tax .
                "',`refund_total` = '" . $refund_total .
                "',`refund_net` = '" . $refund_net .
                "',`refund_tax` = '" . $refund_tax .
                "',`untax_refund` = '" . $untax_refund .
                "',`report_type` = '" . $ZReportDetails['report_type'] .
                "',`report_id` = '" . $ZReportDetails['report_id'] .
                "',`client_name` = '" . $name .
                "',`CUIT` = '" . $cuit."'";
            lavu_query($queryString);
            error_log("LAVUDB ERROR ".lavu_dberror());
        }
    }

    $boolenaTosendEMail =false;

    $row_query = lavu_query("SELECT value AS email FROM `config` WHERE `setting` = 'business_emailid' AND _deleted=0 AND type='location_config_setting'");

    if ($row_query->num_rows)
    {
        $row_read = mysqli_fetch_assoc($row_query);
        if ($row_read['email'] != "")
		{
            $boolenaTosendEMail = true;

            //get manager name
            $row_query = lavu_query("SELECT value AS name FROM `config` WHERE `setting` = 'business_primary_contact' AND _deleted=0 AND type='location_config_setting'");
            if ($row_query->num_rows)
            {
                $array = mysqli_fetch_assoc($row_query);
                $row_read['name'] = $array['name'];
            }

        }
    }

    //email
    if ($boolenaTosendEMail)
	{
        $htmlContent    = buildArgentinaZReportHtml($ZReport, $ZReportDetails['Date'], $ZReportDetails['report_id']);
        $fileName       = str_replace(" ","_",$ZReportDetails['Date'])."_".$ZReportDetails['report_id'];
        $baseUrl        = createHTMLtoPDF($htmlContent, $fileName);
        if ($baseUrl == '')
		{
            error_log('Unable to create PDF file using wkhtmltopdf');
        }
		else
		{
			composeArgentinaZReportEmail($baseUrl, $fileName, $row_read);
		}
    }

	lavu_echo("1");
}

function buildArgentinaZReportHtml($ZReport, $date, $reportId) {

    $others = $tiqueFacturaA = $tiqueFacturaB = $footer = '';

    if ($date != '') {
        $fiscalDate = substr($date, 0, 2) . "-" . substr($date, 2, 2) . "-" . substr($date, 4, 2) . " " . substr($date, 7, 2) . ":" . substr($date, 9, 2) . ":" . substr($date, 11, 2);
        $reportDate = date("d/m/Y", strtotime($fiscalDate));
        $reportHour = date("H:i:s", strtotime($fiscalDate));

        if ($reportDate === false) {
            $reportDate = date("d/m/Y", strtotime($ZReportDetails['Date']));
        }

        if ($reportHour === false) {
            $reportHour = date("H:i:s", strtotime($ZReportDetails['Date']));
        }

    }

    foreach ( $ZReport as $oneReport ) {

        foreach ( $oneReport as $value ) {

            $ZReportDetails = $value;

            if ($ZReportDetails['first_invoice'] == 'Unavailable' && $ZReportDetails['last_invoice'] == 'Unavailable') {
                continue;
            }


            //$register = isset($ZReportDetails['register'])?$ZReportDetails['register']:"Unavailable";
            $firstInvoice = isset($ZReportDetails['first_invoice'])?$ZReportDetails['first_invoice']:"Unavailable";
            $lastInvoice = isset($ZReportDetails['last_invoice'])?$ZReportDetails['last_invoice']:"Unavailable";

            $report_type = $ZReportDetails['report_type'];

            switch($report_type) {

                case "others":
                    $startDateActivities = substr($ZReportDetails['start_date_activities'],4,2)."/".substr($ZReportDetails['start_date_activities'],2,2)."/20".substr($ZReportDetails['start_date_activities'],0,2);
                    $header = '<html><head><style> table { font-size: 10px; } </style>
                                    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
                                    </head><body><table width="700"><tr><td colspan="2">' . $ZReportDetails['business_name']. '</td></tr>
                    				<tr><td colspan="2">' . speak("C.U.I.T Nro.") . ': '. $ZReportDetails['CUIT'].'</td></tr>
                    				<tr><td colspan="2">' . speak("Ing. Brutos") . ': '. $ZReportDetails['ingrown'].'</td></tr>
                    				<tr><td colspan="2">' . speak("Inicio de Actividades") . ': '. $startDateActivities.'</td></tr>
                    				<tr><td colspan="2">' . $ZReportDetails['ResponsabilidadIVA'].'</td></tr>
                    				<tr><td colspan="4"><hr></td></tr>
                    				<tr><td colspan="4" style="text-align:center;"><b>'.speak('INFORME DIARIO DE CIERRE ').'№ 000000'.$reportId.'</b></td></tr>
                                    <tr><td colspan="4" style="text-align:center;">' . 'P.V.№ '.$ZReportDetails['numberpos'].'</b></td></tr>
                    				<tr><td>' . speak("Fecha") . ': '.$reportDate.'</td><td>' . speak ("Hora") . ': '.$reportHour.'</td></tr>
                    				<tr><td colspan="4"><hr></td></tr>
                    				<tr><td colspan="2">' . speak("FECHA DE LA JORNADA") . ': '.$reportDate.'</td></tr>
                                    <tr><td colspan="2">&nbsp;</td></tr>
                    				<tr><td colspan="2" style="text-align:center;"><b>' . speak("COMPROBANTES FISCALES") . '</b></td></tr>
                    				<tr><td colspan="2" style="text-align:center;">&nbsp;</td></tr>';
                    break;

                case "TiqueFacturaA":
                    $tiqueFacturaA = '<tr><td colspan="2" style="text-align:center;"><b>TIQUE FACTURA "A"</b></td></tr>
								 <tr><td colspan="2"><table  width="100%">
								 <tr><td>' . speak("PRIMER COMPROBANTE") . '</td><td>' . $firstInvoice . '</td><td>&nbsp;&nbsp;&nbsp;</td><td>' . speak("ULTIMO COMPROBANTE") . '</td><td>' . $lastInvoice . '</td></tr>
								 <tr><td>' . speak("GRAVADO") . '</td><td>' . $ZReportDetails ['net_total'] . '</td><td>&nbsp;&nbsp;&nbsp;</td><td>' . speak("NO GRAVADO") . '</td><td>'.$ZReportDetails ['untax_total'].'</td></tr>
								 <tr><td>' . speak("EXENTO") . '</td><td>0.00</td><td>&nbsp;&nbsp;&nbsp;</td><td>' . speak("DESCUENTOS") . '</td><td>0.00</td></tr>
                                 <tr><td colspan="4">&nbsp;</td></tr>
                                 <tr><td colspan="4">' . speak("DISCRIMINATION DEL IVA") . '</td></tr>
                                 <tr><td colspan="2">21.00%</td><td colspan="2">'.$ZReportDetails ['tax'].'</td></tr>
                                 <tr><td><b>'.speak('TOTAL DEL IMPORTE DE IVA').'</b></td><td>&nbsp;</td><td>&nbsp;</td><td><b>'.$ZReportDetails ['tax'].'</b></td></tr>
                                 <tr><td colspan="4">&nbsp;</td></tr>
                                 <tr><td><b>'.speak('IMPORTE TOTAL').'</b></td><td>&nbsp;</td><td>&nbsp;</td><td><b>'.$ZReportDetails ['total'].'</b></td></tr>
                                 <tr><td colspan="4">&nbsp;</td></tr>
								 </table>
								</td>
							</tr>';
                    break;

                case "TiqueFacturaB":
                    $tiqueFacturaB = '<tr><td colspan="2" style="text-align:center"><b>TIQUE FACTURA "B"</b></td></tr>
								 <tr><td colspan="2"><table  width="100%">
								 <tr><td>' . speak("PRIMER COMPROBANTE") . '</td><td>' . $firstInvoice . '</td><td>&nbsp;&nbsp;&nbsp;</td><td>' . speak("ULTIMO COMPROBANTE") . '</td><td>' . $lastInvoice . '</td></tr>
								 <tr><td>' . speak("GRAVADO") . '</td><td>' . $ZReportDetails ['net_total'] . '</td><td>&nbsp;&nbsp;&nbsp;</td><td>' . speak("NO GRAVADO") . '</td><td>'.$ZReportDetails ['untax_total'].'</td></tr>
								 <tr><td>' . speak("EXENTO") . '</td><td>0.00</td><td>&nbsp;&nbsp;&nbsp;</td><td>' . speak("DESCUENTOS") . '</td><td>0.00</td></tr>
                                 <tr><td colspan="4">&nbsp;</td></tr>
                                 <tr><td colspan="4">' . speak("DISCRIMINATION DEL IVA") . '</td></tr>
                                 <tr><td colspan="2">21.00%</td><td colspan="2">'.$ZReportDetails ['tax'].'</td></tr>
                                 <tr><td><b>'.speak('TOTAL DEL IMPORTE DE IVA').'</b></td><td>&nbsp;</td><td>&nbsp;</td><td><b>'.$ZReportDetails ['tax'].'</b></td></tr>
                                 <tr><td colspan="4">&nbsp;</td></tr>
                                 <tr><td><b>'.speak('IMPORTE TOTAL').'</b></td><td>&nbsp;</td><td>&nbsp;</td><td><b>'.$ZReportDetails ['total'].'</b></td></tr>
                                 <tr><td colspan="4">&nbsp;</td></tr>
								 </table>
								</td>
							</tr>';
                    break;

                case "Consolidated":
                    $consolidated = '<tr><td colspan="2" align="center">&nbsp;</td></tr>
								<tr><td colspan="2" style="text-align:center;"><b>'.speak('INFORMACION GLOBAL').'</b></td></tr>
								<tr><td colspan="2"><table  width="100%">
									<tr><td>' . speak("GRAVADO") . '</td><td>' . $ZReportDetails ['net_total'] . '</td><td>&nbsp;&nbsp;&nbsp;</td><td>' . speak("NO GRAVADO") . '</td><td>'.$ZReportDetails ['untax_total'].'</td></tr>
									<tr><td>' . speak("EXENTO") . '</td><td>0.00</td><td>&nbsp;&nbsp;&nbsp;</td><td>' . speak("DESCUENTOS") . '</td><td>0.00</td></tr>
								    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr><td colspan="4">' . speak("DISCRIMINATION DEL IVA") . '</td></tr>
                                    <tr><td>21.00%</td><td colspan="3" style="text-align:left;">'.$ZReportDetails ['tax'].'</td></tr>
                                    <tr><td colspan="3"><b>'.speak('TOTAL DEL IMPORTE DE IVA').'</b></td><td>'.$ZReportDetails ['tax'].'</td></tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr><td><b>'.speak('IMPORTE TOTAL DE COMPROBANTES FISCALES').'</b></td><td>&nbsp;</td><td>&nbsp;</td><td><b>'.$ZReportDetails ['total'].'</b></td></tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr><td colspan="4" style="text-align:center;"><b>'.speak('COMPROBANTES NO FISCALES HOMOLOGADOS').'</b></td></tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr><td colspan="3" style="text-align:center;"><b>'.speak('IMPORTE FINAL DE COMPROBANTES NO FISCALES').'</b></td><td><b>0.00</b></td></tr>
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                    <tr><td colspan="3" style="text-align:center;"><b>'.speak('CANTIDAD DE BLOQUEOS EN LA JORNADA').' 0</b></td><td>&nbsp;</td></tr>
								</table></td>
								</tr>';
            }
        }
    }

    $footer = '<tr><td colspan="2">&nbsp;</td></tr>
						<tr><td colspan="2">&nbsp;</td></tr>
						<td colspan="2"><i>' . speak("This Z Report is generated by Lavu POS for Internal Purpose") . '</i></td></tr>
						</tr>
						</table></body></html>';

    return $header.$tiqueFacturaA.$tiqueFacturaB.$consolidated.$footer;
}

function createHTMLtoPDF($htmlContent, $zreportFileName) {
    global $data_name;
    $seperator = DIRECTORY_SEPARATOR;

    $mainDir = $_SERVER['DOCUMENT_ROOT'] . $seperator .'images'.$seperator.'abi_fiscal_exports';
    if(!file_exists($mainDir)) {
        mkdir($mainDir, 0777, true);
    }
    error_log("Main DIr:".$mainDir);
    if(!file_exists($mainDir.$seperator.$data_name)) {
        mkdir($mainDir.$seperator.$data_name, 0777, true);
    }

    $dataDir = $mainDir.$seperator.$data_name;
    $tempFile = $dataDir.$seperator."lastzreport.html";
    $fp = fopen($tempFile,"w");
    error_log("Data DIr:".$dataDir);
    file_put_contents($tempFile, $htmlContent);
    fclose($fp);

    error_log("Temp File:".$tempFile);

    $baseUrl = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST']. $seperator. 'images' .$seperator . 'abi_fiscal_exports' . $seperator . $data_name;

    $fullUrl = $baseUrl. $seperator . 'lastzreport.html';

    error_log("xvfb-run ".$seperator.'usr'.$seperator.'bin'.$seperator.'wkhtmltopdf '.$fullUrl. ' '.$dataDir.$seperator.$zreportFileName.'.pdf');

    $var = exec("xvfb-run ".$seperator.'usr'.$seperator.'bin'.$seperator.'wkhtmltopdf '.$fullUrl. ' '.$dataDir.$seperator.$zreportFileName.'.pdf');

    return (file_exists( $dataDir.$seperator.$zreportFileName.'.pdf')) ? $baseUrl : '';

}

function composeArgentinaZReportEmail($baseUrl, $zReportfileName, $details) {
    global $data_name;
    $from       = "Lavu POS <receipts@lavusys.com>";
    $subject    = ucwords(str_replace("_"," ",$data_name))." ".speak("Z report");
    $message    = speak("Hi")." ".$details['manager'].",<br /><br />".speak("Please find the Z report Attached.");
    sendEmailWithAttachment($baseUrl, $zReportfileName.".pdf", $details['email'], $subject, $message);
}

function sendEmailWithAttachment($baseUrl, $fileName, $mailto, $subject, $message, $headers = null) {

    $file = $baseUrl. "/" . $fileName;

    $content = file_get_contents($file);
    $content = chunk_split(base64_encode($content));

    // a random hash will be necessary to send mixed content
    $separator = md5(time());

    // carriage return type (RFC)
    $eol = "\r\n";

    // main header (multipart mandatory)
    if ($headers == NULL) {
        $headers  = "From: Lavu POS <receipts@lavusys.com>" . $eol;
        $headers .= "MIME-Version: 1.0" . $eol;
        $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
        $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
        $headers .= "This is a MIME encoded message." . $eol;
    } else {
        $headers = implode($eol, $headers);
    }

    // message
    $body = "--" . $separator . $eol;
    $body .= "Content-Type: text/html; charset=\"UTF-8\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8bit" . $eol;
    $body .= $message . $eol;

    // attachment
    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: application/octet-stream; name=\"" . $fileName. "\"" . $eol;
    $body .= "Content-Transfer-Encoding: base64" . $eol;
    $body .= "Content-Disposition: attachment" . $eol;
    $body .= $content . $eol;
    $body .= "--" . $separator . "--";

    //SEND Mail
    if (mail($mailto, $subject, $body, $headers)) {
        error_log("Email sent!");
    } else {
        error_log(print_r( error_get_last(), 1) );
    }
}

function getCheck2OrderJoinOfChecksWithoutFiscalReceipt($loc_id)
{
	//Pull all checks without a fiscal receipt id.  Return it as json.  This information will be used to reconcile all past orders that have not been fiscally printed.
	$nonFiscallyRecordedChecksQuery = <<<HEREDOCQUERY
	SELECT `orders`.`order_id` as `order_id`,
       `orders`.`transaction_id` as `transaction_id`,
       `split_check_details`.`check` as `check`,
	   `split_check_details`.`print_info` as `print_info`,
       `orders`.`closed` as `closed`,
       `orders`.`ioid` as `ioid`
    FROM `orders`
	JOIN `split_check_details` ON `orders`.`ioid` = `split_check_details`.`ioid`
	JOIN `devices` ON `orders`.`closing_device` = `devices`.`UUID`
	JOIN `cc_transactions` ON `orders`.`ioid` = `cc_transactions`.`ioid`
	WHERE `orders`.`location_id` = '[1]' AND `orders`.`transaction_id` = '' AND `orders`.`closed` <> "0000-00-00 00:00:00" AND `orders`.`closed` <> '' AND `split_check_details`.`print_info` = '' ORDER BY `orders`.`closed` DESC
HEREDOCQUERY;

	$nonFiscallyRecordedChecksQuery = str_replace("\n", " ", $nonFiscallyRecordedChecksQuery);

	$nonFiscallyRecordedChecksResult = lavu_query($nonFiscallyRecordedChecksQuery, $loc_id);

	$returnList = array();
	while($currRow = mysqli_fetch_assoc($nonFiscallyRecordedChecksResult)){
		$returnList[$currRow['order_id'].'|'.$currRow['check']] = $currRow;
	}
	$returnJsonString = json_encode($returnList);
	return $returnJsonString;
}

/*
 * Function to get the Transition full details
 */
function getTransitionFullDetails($uuid, $server_id, $date1 , $register)
{
	$transitionOptions = array (
			array (
					0 => "Paid In",
					1 => "till_customization_settings_paid_in"
			),
			array (
					0 => "Paid Out",
					1 => "till_customization_settings_paid_out"
			),
			array (
					0 => "Cash",
					1 => "till_customization_settings_cash"
			),
			array (
					0 => "Card",
					1 => "till_customization_settings_credit_card"
			),
			array (
					0 => "Gift Certifcate",
					1 => "till_customization_settings_gift_certificate"
			),
			array (
					0 => "Till In",
					1 => "till_customization_settings_till_in"
			),
			array (
					0 => "Till Out",
					1 => "till_customization_settings_till_out"
			)
	);

	$dateTimePart = explode(" ",$date1);

	foreach ( $transitionOptions as $oneOption ) {

		$filed1 = $oneOption[0];
		$filed2 = $oneOption[1];

		$get_cash_data_info = lavu_query ( "SELECT
												setting,
												value,
												value2
											FROM cash_data
												WHERE `type`='location_config_setting' AND
												`setting`='[1]' AND
												`value3` = 'summary_item'",
											$filed2 );
		$get_cash_data = mysqli_fetch_object ( $get_cash_data_info );
		$data [$filed1] [summary] = $get_cash_data->value;
		$data [$filed1] [tip] = $get_cash_data->value2;

		if(is_null($data [$filed1] [summary])) {
			$data [$filed1] [summary]='';
		}

		if(is_null($data [$filed1] [tip])){
			$data [$filed1] [tip] = '';
		}


		switch($filed1) {
			case 'Paid In':
			case 'Paid Out':
				$get_amount_tip = lavu_query ( "SELECT
													SUM(total_collected) AS amount,
													SUM(tip_amount) AS tip
												FROM cc_transactions
													WHERE order_id='[1]' AND
													datetime LIKE '[2] %' AND
													register_name ='[3]'",
												$filed1,
												$dateTimePart[0],
												$register);
				break;
			case 'Till In':
				$get_amount_tip = lavu_query ( "SELECT
													SUM(SUBSTRING_INDEX(value, ':',1)) AS amount,
													SUM(value_med) AS tip
												FROM cash_data
													WHERE setting like '%:[1]' AND
													date='[2] ' AND
													setting Like 'till_in_out_%' ",
													$register,
												$dateTimePart[0] );
				break;
			case 'Till Out':
				$get_amount_tip = lavu_query ( "SELECT
												SUM(SUBSTRING_INDEX(value, ':',-1)) AS amount,
													SUM(value_med) AS tip
												FROM cash_data
													WHERE setting like '%:[1]' AND
													date='[2] ' AND
													setting Like 'till_in_out_%' ",
																	$register,
																	$dateTimePart[0] );
				break;
			default:
				$get_amount_tip = lavu_query ( "SELECT
													SUM(total_collected) AS amount,
                                                    SUM(tip_amount) AS tip
                                                 FROM cc_transactions
                                                    WHERE pay_type='[1]' AND
                                                    datetime LIKE '[2]%' AND
                                                    register_name ='[3]'AND order_id NOT IN ('Paid In','Paid Out')",
                                                   $filed1,
                                                   $dateTimePart[0],
                                                   $register );

		}

		$get_amounts_tip = mysqli_fetch_object ( $get_amount_tip );

		if ($get_amounts_tip->amount != "") {
			$total_mount = $get_amounts_tip->amount;
		} else {
			$total_mount = '0';
		}

		if ($get_amounts_tip->tip != "") {
			$total_tip = $get_amounts_tip->tip;
		} else {
			$total_tip = '0';
		}

		$data [$filed1] [summary_amount] = $total_mount;
		$data [$filed1] [tip_amount] = $total_tip;
	}
	return $data;
}
function getmenuccomboddetails($combo_id){
	$main=array();$data1=array();
	$query1="select *from combo_items where combo_menu_id={$combo_id} and parent_id=0 and  _deleted !=1";
	$return1=lavu_query($query1);
	while ($row = mysqli_fetch_assoc($return1)){$data1[] = $row; }
	$i=0;
	foreach($data1 as $dat1){
		$query2="select * from  combo_items where combo_menu_id={$combo_id} and parent_id={$dat1[id]} and add_option =1  and _deleted !=1";
		$return2=lavu_query($query2); $data2=array();
		while ($row2 = mysqli_fetch_assoc($return2)){ $data2[] = $row2;}
		if(count($data2)>0){
			$main[$i][option][0]=getmenudetailes($dat1[menu_item_id],$dat1[id]);
			$main[$i][option][0][sub_option]=suboptioncheck($dat1[id]);
			$j=1;
			foreach($data2 as $dat2){ $main[$i][option][$j]=getmenudetailes($dat2[menu_item_id],$dat2[id]);
			$main[$i][option][$j][sub_option]=suboptioncheck($dat2[id]);
			$j++;}
		}
		else{ $main[$i]=getmenudetailes($dat1[menu_item_id],$dat1[id]);
		$main[$i][sub_option]=suboptioncheck($dat1[id]);}
		$i++;}
		return json_encode($main);
}
function suboptioncheck($id){
	$subdata=array();$data4=array();
	$query4="select * from combo_items where  parent_id={$id} and add_sub_option =1  and _deleted !=1";
	$return4=lavu_query($query4);
	if( mysqli_num_rows($return4) > 0) {
		while ($row4 = mysqli_fetch_assoc($return4)){ $data4[] = $row4; }
		$j=0;
		foreach($data4 as $dat4){
			$subdata[]=getmenudetailes($dat4[menu_item_id],$dat4[id]);
			$j++;}
	}
	return $subdata;
}
function getmenudetailes($menu_id, $combo_id){
	$a_menu_items = array();
	$get_menu_items = lavu_query("SELECT `id`, `category_id`, `name`, `price`, `description`, `image`, `active`, `print`, `quick_item`, `last_modified_date`, `printer`, `apply_taxrate`, `custom_taxrate`, `modifier_list_id`, `forced_modifier_group_id`, `open_item`, `hidden_value`, `hidden_value2`, `allow_deposit`, `UPC`, `hidden_value3`, `inv_count`, `super_group_id`, `tax_inclusion`, `happyhour`, `tax_profile_id`, `track_86_count`, `price_tier_profile_id`, `no_discount`, `product_code`, `tax_code`, `allow_ordertype_tax_exempt` FROM  `menu_items` WHERE `id` = {$menu_id} ");
	$menu_items = mysqli_fetch_assoc($get_menu_items);
	if (sizeof($menu_items)>0) { 
		$menu_items['combo_item_price']=$menu_items['price']; 
	} else
	{  
		$menu_items['combo_item_price']='0';
	}
	$menu_items['price'] = "0";
	$get_combo_items=lavu_query("select *from combo_items where id={$combo_id}");
	$combo_items = mysqli_fetch_assoc($get_combo_items);
	$menu_items['combo_qty']= $combo_items['qty'];
	$menu_items['combo_name']= $combo_items['custom_category_name'];
	if ($combo_items['combo_header']==1) {  
		$menu_items['name'] = $menu_items['combo_name'];  
		$menu_items['combo_qty']= $combo_items['header_qty'];  
	}
	$menu_items['combo_adjustment_price']= $combo_items['price_adjustment'];
	$cat_details = getcombotaxbycategory($menu_items['category_id']);
	if($menu_items['tax_profile_id'] == '' && $combo_items['combo_header'] < 1 && $menu_items['category_id'] != '')
	{
		$menu_items['apply_taxrate'] = $cat_details['apply_taxrate'];
		$menu_items['custom_taxrate'] = $cat_details['custom_taxrate'];
		$menu_items['tax_inclusion'] = $cat_details['tax_inclusion'];
		$menu_items['tax_profile_id'] = $cat_details['tax_profile_id'];
	}
	if (($cat_details['active'] == 0 || $menu_items['active'] == 0) && $menu_items['forced_modifier_group_id'] == 'C') {
		$menu_items['forced_modifier_group_id'] = $cat_details['forced_modifier_group_id'];
	}
	if (($cat_details['active'] == 0 || $menu_items['active'] == 0) && $menu_items['modifier_list_id'] == 0) {
		$menu_items['modifier_list_id'] = $cat_details['modifier_list_id'];
	}
	$menu_items['option'] = array();
	$menu_items['sub_option'] = array();
	return $menu_items;
}

function getcombotaxbycategory($catid)
{
	$get_category_details = lavu_query("SELECT `apply_taxrate`, `custom_taxrate`, `tax_inclusion`,`tax_profile_id`, `active`, `forced_modifier_group_id`, `modifier_list_id` FROM  `menu_categories` WHERE `id` = '".$catid."' ");
	$category_profile = mysqli_fetch_assoc($get_category_details);
	$cat_profile_arr['apply_taxrate'] = $category_profile['apply_taxrate'];
	$cat_profile_arr['custom_taxrate'] = $category_profile['custom_taxrate'];
	$cat_profile_arr['tax_inclusion'] = $category_profile['tax_inclusion'];
	$cat_profile_arr['tax_profile_id'] = $category_profile['tax_profile_id'];
	$cat_profile_arr['tax_code'] = $category_profile['tax_code'];
	$cat_profile_arr['active'] = $category_profile['active'];
	$cat_profile_arr['forced_modifier_group_id'] = $category_profile['forced_modifier_group_id'];
	$cat_profile_arr['modifier_list_id'] = $category_profile['modifier_list_id'];

	return $cat_profile_arr;

}


/* Function  for genrating invoice xml for uruguay
 * tax authority and
 * approval of invoice
 * */
function toCreateXmlUruguay($device_time,$xml_data){
	global $data_name;
	global $location_info;
	$date1=substr($device_time,0,10);
	$xml_data=json_decode($xml_data,1);
	$transaction_type=strtolower($xml_data['transaction_type']);
	if($xml_data['is_refunded']==1)
	{
		$transaction_type_mode = $transaction_type.'_refund';
	}
	else
	{
		$transaction_type_mode=$transaction_type;
	}
	$order_check= explode("#",$xml_data['orderId']);
	$order_id=trim($order_check[0]); $check_no=trim($order_check[1]);
	$country_name = strtolower($location_info['country']);
	$get_restaurant_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` where data_name='$data_name'");
	$get_restaurant = mysqli_fetch_assoc($get_restaurant_data);
	if($get_restaurant['dev']=='0'){ $env='prod'; }else{ $env='test'; }
	$get_components = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`fiscal_integration_config` where LCASE(country)='".$country_name."' and env='$env'");
	if (mysqli_num_rows($get_components) > 0) {
		$c_info = mysqli_fetch_assoc($get_components);
		mysqli_free_result( $get_components );
	}
	$get_invoice_type=lavu_query("SELECT * FROM `fiscal_invoice_type` where invoice_type='".$transaction_type_mode."'");
	if (mysqli_num_rows($get_invoice_type) > 0)
	{
		$invoice_info = mysqli_fetch_assoc($get_invoice_type);
		mysqli_free_result( $get_invoice_type );
	}
	$invoice_format=$invoice_info['format'];
	$invoice_start_index=$invoice_info['start_index'];
	$invoice_type_id = $invoice_info['fiscal_invoice_type_id'];
	$check_fiscal_exists=lavu_query("SELECT `fiscal_invoice_id` from `fiscal_invoice` where order_id='$order_id' and check_no='$check_no' and invoice_type_id = '$invoice_type_id'");
	if (mysqli_num_rows($check_fiscal_exists) > 0)
	{
		$get_fiscal_id_exists = mysqli_fetch_assoc($check_fiscal_exists);
		mysqli_free_result( $check_fiscal_exists );
		$invoice_last_id = $get_fiscal_id_exists['fiscal_invoice_id'];
	}
	else
	{
		$invoice_last_id=lavu_insert_id(lavu_query("insert into fiscal_invoice (`order_id`,`invoice_type_id`, `check_no`,`approval_req_date`,`approval_status`,`created_date`) values('$order_id','$invoice_type_id','$check_no','$device_time','0','$device_time')"));
	}
	$products_list=$xml_data[items];

	if($xml_data['is_refunded']==1)
	{
		$form = 2;
	}
	else
	{
		$form = 1;
	}

	if($transaction_type=='b2b')
	{
		$tax_included='1';
		if($xml_data['is_refunded']==0){ $invoice_type = '111';  }
		else{ $invoice_type = '112'; }
	}
	else if($transaction_type=='b2c' && $xml_data['isrefunded']==0)
	{
		$tax_included='1';
		if($xml_data['is_refunded']==0){ $invoice_type = '101';  }
		else{ $invoice_type = '102'; }
	}
	require_once(dirname(__FILE__).'/../xml_mapper/uruguay/business.php');
	//Calling the XML Template for Respective Country
	$header_content = $header;
	$header_content= str_replace('{{date}}',$date1,$header_content);
	$header_content= str_replace('{{orderid}}',$xml_data['orderId'],$header_content);
	$header_content= str_replace('{{form}}',$form,$header_content);
	$header_content= str_replace('{{tax_included}}',$tax_included,$header_content);
	$products_contents = $product_details;
	$count = 1;
	$subtotal_3 = 0;
	$subtotal_2 = 0;
	$subtotal_1 = 0;
	foreach($products_list as $products)
	{
		if($xml_data['is_Void']==1)
		{
			if($products['is_voided']==1)
			{
				$productstr=str_replace('{{product_name}}',str_replace('&','&amp;',$products['itemName']),$products_contents);
				$productstr=str_replace('{{qty}}',$products['quantity'],$productstr);
				$productstr=str_replace('{{unit_price}}',$products['itemPrice'],$productstr);
				$productstr=str_replace('{{product_total}}',$products['subtotal'],$productstr);
				$productstr=str_replace('{{product_subtotal}}',$products['subtotal'],$productstr);
				$productstr=str_replace('{{product_dicount}}',$products['discount'],$productstr);
				$productstr=str_replace('{{slno}}',$count,$productstr);
				$productstr=str_replace('{{tax_code}}',$products['taxCode'],$productstr);
			}
		}
		else
		{
			$productstr=str_replace('{{product_name}}',str_replace('&','&amp;',$products['itemName']),$products_contents);
			$productstr=str_replace('{{qty}}',$products['quantity'],$productstr);
			$productstr=str_replace('{{unit_price}}',$products['itemPrice'],$productstr);
			$productstr=str_replace('{{product_total}}',$products['subtotal'],$productstr);
			$productstr=str_replace('{{product_subtotal}}',$products['subtotal'],$productstr);
			$productstr=str_replace('{{product_dicount}}',$products['discount'],$productstr);
			$productstr=str_replace('{{slno}}',$count,$productstr);
			$productstr=str_replace('{{tax_code}}',$products['taxCode'],$productstr);
		}
		$final_productstr.= $productstr;
		if($xml_data['is_refunded']==0)
		{
			if($products['taxCode']==3)
			{
				$subtotal_3 = $subtotal_3 + $products['itemtotal_without_tax'];
			}
			if($products['taxCode']==2)
			{
				$subtotal_2 = $subtotal_2 + $products['itemtotal_without_tax'];
			}
			if($products['taxCode']==1)
			{
				$subtotal_1 = $subtotal_1 + $products['itemtotal_without_tax'];
			}
		}
		else
		{
			$subtotal_1 = $xml_data['refunded_Amount'];
		}
		$count++;
	}
	$business_data = '';
	if($invoice_type_id==1 || $invoice_type_id==3)
	{
		$business_info = $xml_data['custoner_info'];
		$business_content = $business_details;
		$business_data=str_replace('{{city}}',$business_info['city'],$business_content);
		$business_data=str_replace('{{zipcode}}',$business_info['postal_code'],$business_data);
		$business_data=str_replace('{{company_name}}',$business_info['company'],$business_data);
		$business_data=str_replace('{{address}}',$business_info['street_address'],$business_data);
		$business_data=str_replace('{{company_number}}',$business_info['extra1'],$business_data);
	}
	$order_content = $order_details;
	$order_data= str_replace('{{subtotal_3}}',$subtotal_3,$order_content);
	$order_data= str_replace('{{subtotal_2}}',$subtotal_2,$order_data);
	$order_data= str_replace('{{subtotal_1}}',$subtotal_1,$order_data);
	$order_data= str_replace('{{invoicetype}}',$invoice_type,$order_data);
	if($xml_data['is_refunded']==1)
	{
		$get_fiscal_details=lavu_query("SELECT * FROM `fiscal_invoice` where order_id='$order_id' and check_no='$check_no' and approval_status='1' and invoice_type_id = '$invoice_type_id'");
		if (mysqli_num_rows($get_fiscal_details) > 0)
		{
			$fiscal_info = mysqli_fetch_assoc($get_fiscal_details);
			mysqli_free_result( $get_fiscal_details );
		}
		$get_ref_data=json_decode($fiscal_info['ref_data'],1);

		$refund_content = $refund_details;
		$refund_data= str_replace('{{ref_invoice}}',$get_ref_data['NUMERO'],$refund_content);
		$refund_data= str_replace('{{ref_serial}}',$get_ref_data['SERIE'],$refund_data);
		$refund_data= str_replace('{{ref_invoice_type}}',$get_ref_data['TIPOCFE'],$refund_data);
	}
	$footer_content = $footer;
	$main_xml=$header_content;
	$main_xml.=$final_productstr;
	$main_xml.=$business_data;
	$main_xml.=$order_data;
	$main_xml.=$refund_data;
	$main_xml.=$footer_content;

	$endurl = $c_info['endpoint_url'];
	/*Calling the API*/
	$xmldata = createapicall($main_xml,$endurl);
	$xmlparser = xml_parser_create();

	/*xml_parse_into_struct() function parses XML data into an array
	 * $values will return array for xml tags
	 */
	xml_parse_into_struct($xmlparser,$xmldata,$xmlvalues);
	xml_parser_free($xmlparser);
	$api_reponse_json = json_encode($xmlvalues);



	$flag_xml=0;
	foreach($xmlvalues as $xmldata)
	{
		if($xmldata['tag']=='CODIGO' && $xmldata['value']==0)
		{
			$flag_xml = 1;
		}
		if($xmldata['tag']=='DESCRIPCION' && $xmldata['value']=='Exito')
		{
			$flag_xml = 1;
		}
		if($xmldata['tag']=='SERIE')
		{
			$serie_val = $xmldata['value'];
		}
		if($xmldata['tag']=='NUMERO')
		{
			$numero_val = $xmldata['value'];
		}
		if($xmldata['tag']=='TIPOCFE')
		{
			$invoicetype_val = $xmldata['value'];
		}
	}
	$ref_data='{"SERIE":"'.$serie_val.'","NUMERO":"'.$numero_val.'","TIPOCFE":"'.$invoicetype_val.'"}';
	lavu_query("insert into fiscal_invoice_approval_actl (`fiscal_invoice_id`,`approval_info`,`approval_status`,`created_date`,`request_info`) values('$invoice_last_id','$api_reponse_json','$flag_xml','$device_time','$main_xml')");

	if($flag_xml==1)
	{
			$count_invoice=lavu_query("select count(fiscal_invoice_id)as no from fiscal_invoice where invoice_type_id = '$invoice_type_id' and invoice_no!=''");
			if (mysqli_num_rows($count_invoice) > 0){	$count_invoice_no = mysqli_fetch_assoc($count_invoice);	 $invoice_count=$count_invoice_no['no']; }
			else{ $invoice_count='0'; }
			$next_id = ($invoice_start_index+$invoice_count);
			$invoice_format = str_replace('<order_id>',$order_id,$invoice_format);
			$invoice_format = str_replace('<next_id>',$next_id,$invoice_format);

		lavu_query("update fiscal_invoice set invoice_no='$invoice_format', approval_status='1', ref_data='$ref_data', update_date='$device_time' where fiscal_invoice_id='$invoice_last_id'");
		$qr_xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://servicios/">';
		$qr_xml.= '<soapenv:Header/>';
		$qr_xml.='<soapenv:Body>';
		$qr_xml.='<ser:getImprimible>';
		$qr_xml.='<arg0>'.$serie_val.'</arg0>';
		$qr_xml.='<arg1>'.$numero_val.'</arg1>';
		$qr_xml.='<arg2>'.$invoicetype_val.'</arg2>';
		$qr_xml.='<arg3>jpg</arg3>';
		$qr_xml.='</ser:getImprimible>';
		$qr_xml.='</soapenv:Body>';
		$qr_xml.='</soapenv:Envelope>';

		$qr_xmldata = createapicall($qr_xml,$endurl);

		$xmlparser_qr = xml_parser_create();

		/*xml_parse_into_struct() function parses XML data into an array
		 * $values will return array for xml tags
		 */
		xml_parse_into_struct($xmlparser_qr,$qr_xmldata,$qrvalues);

		xml_parser_free($xmlparser_qr);
		foreach($qrvalues as $qr)
		{
			if($qr['tag']=='IMPRIMIBLE')
			{
				$reponse_arr['qrcode'] = $qr['value'];
				$reponse_arr['status']= 'success';
				$reponse_arr['fiscal_transaction_no']= str_replace('_','-',$invoice_format);
				lavu_query("update fiscal_invoice set qr_code='".$reponse_arr['qrcode']."' where fiscal_invoice_id='$invoice_last_id'");
			}
		}
	}
	else {
		if(!empty($xmlvalues))
		{
			foreach($xmlvalues as $xmldata)
			{

				if($xmldata['tag']=='DESCRIPCION')
				{
					$reponse_arr['error_message'] = $xmldata['value'];
				}

			}
		}
		else
		{
			global $modules;
			if ($modules->hasModule("reports.fiscal.uruguay"))
			{
				$reponse_arr['error_message'] = 'Please set Country name to Uruguay';
			}
			else
			{
				$reponse_arr['error_message'] = 'Some Error Occurred';
			}
		}
		$reponse_arr['qrcode'] = 0;
		$reponse_arr['status'] = 'failure';
	}
	return $reponse_arr;
	exit;
}

/* fucntion for API calling for
 * approval of invvoice
 */
function createapicall($xmldata,$endurl)
{
	$url = $endurl;
	$post_string = $xmldata;
	$soap_do = curl_init();
	curl_setopt($soap_do, CURLOPT_URL,            $url );
	curl_setopt($soap_do, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($soap_do, CURLOPT_TIMEOUT,        20);
	curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($soap_do, CURLOPT_POST,           true );
	curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $post_string);
	curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array('Content-Type: text/xml; charset=utf-8', 'Content-Length: '.strlen($post_string) ));
	$result = curl_exec($soap_do);
	$err = curl_error($soap_do);
	return $result;
}

function format_sacoa_response($rtn_response)
{
	$rtn = array();
	if($rtn_response['flag'] == 1)
	{
		//  0 - success flag ("1")
		//  1 - [transaction_id]
		//  2 - [card_desc] (last_four)
		//  3 - response message
		//  4 - [auth_code]
		//  5 - [card_type]
		//  6 - [record_no]
		//  7 - [amount]
		//  8 - [order_id]
		//  9 - [ref_data]
		// 10 - [process_data]
		// 11 - new balance
		// 12 - bonus message
		// 13 - [first_four]
		// 14 - [last_mod_ts]

		$rtn[] = $rtn_response['flag']; // 0
		$rtn[] = $rtn_response['txnid']; // transaction ID
		$rtn[] = $rtn_response['card_desc'];
		$rtn[] = $rtn_response['message'];
		$rtn[] = $rtn_response['auth_code'];
		$rtn[] = $rtn_response['card_type'];
		$rtn[] = $rtn_response['record_no'];
		$rtn[] = $rtn_response['amount'];
		$rtn[] = $rtn_response['order_id'];
		$rtn[] = $rtn_response['ref_data'];
		$rtn[] = $rtn_response['process_data']; // 10
		$rtn[] = $rtn_response['new_balance'];
		$rtn[] = $rtn_response['tickets'];
		$rtn[] = $rtn_response['first_four'];
		$rtn[] = $rtn_response['last_mod_ts'];
	}
 	else
	{
		$rtn[] = $rtn_response['flag'];
		$rtn[] = $rtn_response['message'];
		$rtn[] = $rtn_response['txnid'];
		$rtn[] = $rtn_response['order_id'];
		$rtn[] = "";
		$rtn[] = $rtn_response['response_title'];
	}
		//echo "<pre>"; echo implode("|",$rtn); echo "</pre>";
	return implode("|",$rtn);
}
/*
 *Method for api Customer Pick Number genaration
 */
function toCreatePickupSequence($device_time, $loc_id, $server_id, $order_id)
{
			global $location_info;
			//time analysis
			$post_vars['device_time'] = $device_time;
			$transactiontime = determineDeviceTime($location_info, $post_vars);

			$dayStartEnd = str_pad($location_info['day_start_end_time'], 4, '0', STR_PAD_LEFT);
			$start_hours = date('H', strtotime($dayStartEnd));
			$start_minutes = date('i', strtotime($dayStartEnd));
			$currentdate=date('Y-m-d', strtotime($transactiontime));
			$store_start_time = date('Y-m-d H:i:s', strtotime('+'.$start_hours.' hour +'.$start_minutes.' minutes', strtotime($currentdate)));
			$store_closed_time =  date('Y-m-d H:i:s', strtotime('+23 hour +59 minutes +59 seconds', strtotime($store_start_time)));
			// get the for number format
			$numformat = lavu_query("select value from config where setting='pickup_number_format'");
			if (mysqli_num_rows($numformat) > 0)
			{
				$num_format = mysqli_fetch_assoc($numformat);
				$numberformat = $num_format['value'];
			}

			$numberformat_value = 0;
			if($numberformat==3)
				$numberformat_value = '100'; //001
			if($numberformat==4)
				$numberformat_value = '1000'; //0001
			if($numberformat==5)
				$numberformat_value = '10000'; //00001

			$insert = lavu_query("INSERT INTO `pickup_seq`(`server_id`,`order_id`, `pickup_num`, `startdate`,`enddate`,`created_date`) VALUES
					('$server_id','$order_id',
						(
							if(
								(select p.pickup_num from pickup_seq as p where '$transactiontime' BETWEEN p.startdate and p.enddate and p.startdate='$store_start_time' and p.enddate='$store_closed_time' and p.pickup_num BETWEEN 1 and '$numberformat_value' order by p.id desc limit 0,1),
								(
									if(
										( (select s.pickup_num from pickup_seq as s where '$transactiontime' BETWEEN s.startdate and s.enddate and s.startdate='$store_start_time' and s.enddate='$store_closed_time' order by s.id desc limit 0,1) >='$numberformat_value' ),
										0,
										(select b.pickup_num from pickup_seq as b where  '$transactiontime' BETWEEN b.startdate and b.enddate and b.startdate='$store_start_time' and b.enddate='$store_closed_time' order by b.id desc limit 0,1)
										)
								),
						0)
				+1 ),
				'$store_start_time','$store_closed_time','$transactiontime' )");

		$pickupno = lavu_query("select `pickup_num`,`order_id` FROM `pickup_seq` where `server_id`='$server_id' order by `id` desc limit 0,1");
		if (mysqli_num_rows($pickupno) > 0) {
			$pickup_no = mysqli_fetch_assoc($pickupno);
			$pickup_num=$pickup_no['pickup_num'];
		}
		else {
			$pickup_num ='0';
		}

		$pickup_num = str_pad($pickup_num, $numberformat, '0', STR_PAD_LEFT); //001

		if ($pickup_num > 0) {
			$reponse_arr['pickup_num'] = $pickup_num;
			$reponse_arr['order_id'] = $order_id;
			$reponse_arr['status']= 'success';
		} else {
			$reponse_arr['error_message'] = 'Some Error Occurred';
			$reponse_arr['status']= 'failure';
		}
		return $reponse_arr;
}// End of pickup number
function getCustomerFacingDisplayDetails()
{
    $query="SELECT id,value,value2 FROM `config` WHERE `setting`='CustomerFacingDisplay'";
    $return=lavu_query($query);
    while ($row = mysqli_fetch_assoc($return)){ $data[$row[value]] = $row[value2]; }
    $customer_facing_display=array();
    if(count($data)>0){
        $customer_facing_display[time_interval]=$data[time_interval];
        $customer_facing_display[display_logo]=$data[display_logo];
        $customer_facing_display[background_image]=$data[background_image];
        $customer_facing_display[screen_saver][screen_saver_1]=$data[screen_saver1];
        $customer_facing_display[screen_saver][screen_saver_2]=$data[screen_saver2];
        $customer_facing_display[screen_saver][screen_saver_3]=$data[screen_saver3];
        $customer_facing_display[screen_saver][screen_saver_4]=$data[screen_saver4];
        $customer_facing_display[promotion][promotion_1]=$data[promotion1];
        $customer_facing_display[promotion][promotion_2]=$data[promotion2];
        $customer_facing_display[promotion][promotion_3]=$data[promotion3];
        $customer_facing_display[promotion][promotion_4]=$data[promotion4];
    }
    return $customer_facing_display;
}

function getLIPCredentials($loc_id){
    $integrationKeyStr = integration_keystr();
    $queryStr = "SELECT AES_DECRYPT(`integration9`,'[1]') as data9, ".
        "AES_DECRYPT(`integration10`,'[1]') as data10 ".
        "FROM `locations` WHERE `id` = '[2]'";
    $result = lavu_query($queryStr, $integrationKeyStr, $loc_id);
    if(!$result){ error_log('Mysql error in '.__FILE__.' -sirur- error:'.mysql_error()); };
    $row = mysqli_fetch_assoc($result);
    return $row;
}

function getTransactionStatusLip($ref_id, $terminal_id, $loc_id, $order_id = '', $check = '', $ioid = '', $reskin = false){
    $row = getLIPCredentials($loc_id);
    $endurl = "https://econduitapp.com/services/api.asmx/checkStatus?command=sale&key=".$row['data9']."&password=".$row['data10']."&terminalId=".$terminal_id."&refID=".$ref_id."&date=".date(mdY);

    $LIP_do = curl_init();
    curl_setopt($LIP_do, CURLOPT_URL,            $endurl);
    curl_setopt($LIP_do, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($LIP_do, CURLOPT_TIMEOUT,        20);
	curl_setopt($LIP_do, CURLOPT_RETURNTRANSFER, true );
	$curlResponse = curl_exec($LIP_do);
	curl_close($LIP_do);
	$result = json_decode($curlResponse, 1);

    if ($curlResponse === FALSE) {
        $result = ($reskin) ? array("ResultCode" => "unknown", "Message" => curl_error( $LIP_do ) ) : array( "cURLError" => curl_error( $LIP_do ) );
	}
	$orderData = ($reskin) ? array('order_id' => $order_id, 'check' => $check, 'transaction_id' => $ref_id, 'ref_data' => $terminal_id, 'ioid' => $ioid) : array();
	$result = json_encode(array_merge($result, $orderData));
    return $result;
}

function getLipTransactionId($order_id, $check, $check_total_amount = 0, $transaction_amount = 0, $terminal_id = '', $reskin = false){
	$result = array();
	$allow = false;
	// For POS 4.0
	if ($reskin) {
		$select_query = "select `transaction_id`, `amount` from `lip_order_transaction` where `order_id` = '".$order_id."' AND `check` = '".$check."' AND `amount` > 0 order by id desc";
		$result_select = lavu_query($select_query);
		$numRec = mysqli_num_rows($result_select);
		if ($numRec > 0) {
			$lipTransactionDetails = array();
			while ($row = mysqli_fetch_assoc($result_select)){
				$lipTransactionDetails['amount'][] = $row['amount'];
				$lipTransactionDetails['transaction_id'][] = $row['transaction_id'];
			}
			$totalLipAmount = array_sum($lipTransactionDetails['amount']);
			$allow = (($check_total_amount - $totalLipAmount) >= $transaction_amount);
			foreach ($lipTransactionDetails['transaction_id'] as $transactionId) {
				$transactionIdQuery = "select `transaction_id` from `cc_transactions` where order_id = '".$order_id."' AND `check` = '".$check."' AND `transaction_id`='".$transactionId."'";
				$resultTransactionIdQuery = lavu_query($transactionIdQuery);
				$numRowTransactionId = mysqli_num_rows($resultTransactionIdQuery);
				if( $numRowTransactionId == 0 ){
					$result = array('order_id' => $order_id, 'transaction_id' => $row['transaction_id'], 'check' => $check, 'status' => 'false');
					break;
				}
			}
		}
	} else {
		// For POS 3.x
		$select_query = "select `transaction_id` from `lip_order_transaction` where `order_id` = '".$order_id."' AND `check` = '".$check."' order by id desc limit 1";
		$result_select = lavu_query($select_query);
		$numRec = mysqli_num_rows($result_select);
		if($numRec > 0){
			$row = mysqli_fetch_assoc($result_select);
			$transactionIdQuery = "select `transaction_id` from `cc_transactions` where order_id = '".$order_id."' AND `check` = '".$check."' AND `transaction_id`='".$row['transaction_id']."'";
			$resultTransactionIdQuery = lavu_query($transactionIdQuery);
			$numRowTransactionId = mysqli_num_rows($resultTransactionIdQuery);
			if( $numRowTransactionId == 0 ){
				$result = array('transaction_id' => $row['transaction_id'], 'status' => 'false');
			}
		}
	}

    if (empty($result) || $allow) {
        $result = createLipTransactionId($order_id, $check, $transaction_amount, $terminal_id, $reskin);
    }
    return json_encode($result);
}

function createLipTransactionId($order_id, $check, $transaction_amount = 0, $terminal_id = '', $reskin = false){
	$result = array();
	$lipTransactionId = date("ymd").date("His").str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);
	// For POS 4.0
	if ($reskin) {
		if ($order_id != 0 && $order_id != '' && $transaction_amount > 0 && $terminal_id != '') {
			lavu_query("INSERT INTO `lip_order_transaction` (`order_id`, `check`, `transaction_id`, `created_date`, `amount`, `terminal_id`) VALUES ('".$order_id."', '".$check."', '".$lipTransactionId."', NOW(),'" . $transaction_amount . "','" . $terminal_id . "')");
			$result = array('order_id' => $order_id, 'transaction_id' => $lipTransactionId, 'check' => $check, 'status' => 'true');
		}
	} else {
		// For POS 3.x
		lavu_query("INSERT INTO `lip_order_transaction` (`order_id`, `check`, `transaction_id`, `created_date`) VALUES ('".$order_id."', '".$check."', '".$lipTransactionId."', NOW())");
		$result = array('transaction_id' => $lipTransactionId, 'status' => 'true');
	}
    return $result;
}

/**
 * This function is used to update lip_order_transaction table amount. For POS 4.0
 * @param  $order_id
 * @param  $check
 * @param  $amount
 * @param  $ref_id
 * return true/false with order details.
 */
function refundVoidDeleteTransactionLIP($order_id, $check, $amount, $ref_id, $delete = 0) {
	if ($delete) {
		$output = lavu_query("DELETE FROM `lip_order_transaction` WHERE `transaction_id` = '[1]'", $ref_id);
	} else {
		$output = lavu_query("UPDATE `lip_order_transaction` set amount = amount - '[1]' WHERE `order_id` = '[2]' AND `check` = '[3]' AND `transaction_id` = '[4]'", $amount, $order_id, $check, $ref_id);
	}
    $result = array('order_id' => $order_id, 'transaction_id' => $ref_id, 'check' => $check, 'amount' => $amount, 'status' => $output);
    return $result;
}

function getFiscalSetting(&$message) {
	$query = mysqli_fetch_assoc(lavu_query("SELECT `setting`, `value` FROM `config` WHERE `setting` IN ('enable_fiscal_receipt_templates', 'enable_fiscal_invoice') AND `_deleted` = 0"));
	$return=lavu_query($query);
	while ($row = mysqli_fetch_assoc($return)){
		$message[$row['setting']] = $row['value'];
	}
}

function getSecondaryCurrency() {
    $result = array(
                    'secondary_currency_code' => '0',
                    'secondary_currency_code_id' => '0',
                    'exchange_rate_id' => '0',
                    'exchange_rate_value' => '0'
                    );
    $secondaryCurrencyQuery = lavu_query("select value2 from config where setting='secondary_currency_code'");
    if ( mysqli_num_rows($secondaryCurrencyQuery) > 0 ) {
        $secondaryCurrencyInfo = mysqli_fetch_assoc($secondaryCurrencyQuery);

        if ($secondaryCurrencyInfo['value2'] != '' && $secondaryCurrencyInfo['value2'] != '0') {
        	$result['secondary_currency_code_id'] = $secondaryCurrencyInfo['value2'];
            $country_query=mlavu_query("select alphabetic_code, name from poslavu_MAIN_db.country_region where id=".$secondaryCurrencyInfo['value2']);
            $countryInfo=mysqli_fetch_assoc($country_query);
            $result['secondary_currency_code'] = "[".$countryInfo['alphabetic_code']."]".$countryInfo['name'];
        }

        $pcurrency_query=lavu_query("select value2 from config where  setting='primary_currency_code'");
        $primaryCurrencyInfo = mysqli_fetch_assoc($pcurrency_query);

        $select_query = "select id, rate
                         from secondary_exchange_rates
                         where DATE(`effective_date`) <= '".date('Y-m-d')."'
                               and `_deleted`='0'
                               and `from_currency_id`='".$primaryCurrencyInfo['value2']."'
                               and  `to_currency_id`='".$secondaryCurrencyInfo['value2']."'
                               AND custom=0
                               order by `effective_date` DESC
                               limit 1 ";
        $result_select = lavu_query($select_query);
        if ( mysqli_num_rows($result_select) > 0 ) {
            $row = mysqli_fetch_assoc($result_select);
            $result['exchange_rate_id'] = $row['id'];
            $result['exchange_rate_value'] = $row['rate'];
        }
    }
    return $result;
}

function insertCustomSecondaryExchangeRate($exchangeRateData) {
    $result = array('secondary_exchange_rate_id' => 0);
    $select_query = "select value2 from `config` WHERE `type` = 'location_config_setting' and setting='primary_currency_code' ";
    $result_select = lavu_query($select_query);
    $numRec = mysqli_num_rows($result_select);
    $returnId = 0;
    if($numRec > 0){
        $row = mysqli_fetch_assoc($result_select);
        $queryCountryName = "select name from `country_region` WHERE `id` = ".$row['value2'];
        $resultCountryName = mlavu_query($queryCountryName);
        $rowCountryName = mysqli_fetch_assoc($resultCountryName);

        $selectOldRate  = "select id from `secondary_exchange_rates` WHERE `from_currency_id` = ".$row['value2'];
        $selectOldRate .=" AND `to_currency_id` =".$exchangeRateData['secondary_currency_id'];
        $selectOldRate .=" AND `rate` =".number_format($exchangeRateData['rate'],3);
        $selectOldRate .=" AND DATE(`effective_date`) = '".date('Y-m-d')."' order by id desc LIMIT 1";

        $resultOldRate = lavu_query($selectOldRate);
        $numOldRate = mysqli_num_rows($resultOldRate);

        if($numOldRate==0){
            $query="INSERT into secondary_exchange_rates SET ";
            $query.=" `from_currency_id`='".$row['value2']."',";
            $query.=" `from_currency_code`='".$rowCountryName['name']."',";
            $query.=" `to_currency_id`='".$exchangeRateData['secondary_currency_id']."',";
            $query.=" `to_currency_code`='".$exchangeRateData['secondary_currency_code']."',";
            $query.=" `rate`='".$exchangeRateData['rate']."',";
            $query.=" `effective_date`='".date('Y-m-d H:i:s')."',";
            $query.=" `custom`=1, `_deleted`=0";
            $resultInsert = lavu_query($query);
            if($resultInsert){
                $returnId = lavu_insert_id();
            }
        }else{
            $rowExchangeIdInfo = mysqli_fetch_assoc($resultOldRate);
            $returnId = $rowExchangeIdInfo['id'];
        }
        $result = array('secondary_exchange_rate_id' => $returnId);
    }
    return $result;
}

function getDepositReceipt($depositId) {
    require_once(__DIR__."/../deposit/Receipt.php");
    require_once(__DIR__."/../deposit/ReceiptTemplate.php");
    $receiptJson = json_encode ( array( ) );
    if ( is_null( $depositId ) == false ) {
        //Deposit Receipt Print
        $stdObj = new stdClass();
        $stdObj->depositId = $depositId;
        $depositObj = new Deposit\Receipt( $stdObj );
        $deposit = $depositObj->getDepositInfo();
        if ( is_null( $depositId ) == false ) {
            $receiptObj = new Deposit\ReceiptTemplate($deposit);
            //Pass deposit information in JSON form to iOS App.
            $receiptJson = $receiptObj->printReceipt();
        }
    }
    return $receiptJson;
}

function getFiscalTpl($data_name) {
	$data['action'] = 'gettplfields';
	$data['mode'] = 'fiscal_receipt_tpl_pos';
	$data['dataname']=$data_name;
	include_once(__DIR__."/FiscalReceiptTplApi.php");
	$apiObj = new FiscalReceiptTplApi($data_name, $data);
	$result = $apiObj->doProcess();
	unset($apiObj);
	return $result;
}

/**
 * This function is used to get dtt integration setting from config table.
 * @param $device_uuid
 * @return dtt setting details
 */
function getDttIntegration($device_uuid) {
	$dttResult = array();
	$dttData = lavu_query('SELECT con.id, con.value, con.value2, con.value3, dtt.config_id, dtt.device_uuid FROM config con LEFT JOIN dtt_pairs dtt ON con.id = dtt.config_id where con.`setting` = "dtt_integration" AND con._deleted = 0');
	if (mysqli_num_rows($dttData) > 0) {
		while($dttInfo = mysqli_fetch_assoc($dttData)) {
			if ($dttInfo['config_id'] != null && $dttInfo['device_uuid'] == $device_uuid) {
				$dttInfo['pair_status'] = 1;
				$dttResult[$dttInfo['id']] = $dttInfo;
			} else if (!isset($dttResult[$dttInfo['id']]['pair_status'])) {
				$dttResult[$dttInfo['id']] = $dttInfo;
			}

			unset($dttResult[$dttInfo['id']]['config_id']);
			unset($dttResult[$dttInfo['id']]['device_uuid']);
		}
	}

	return $dttResult;
}

/**
 * This function is used to pair/unpair on the particular camera on the basis of pairing details.
 * @param  $deviceUUID
 * @param  $serverId
 * @param  $deviceTime
 * @param  $pair
 * @param  $unpair
 * return pairing status.
 */
function getDttPairing($deviceUUID, $serverId, $deviceTime, $pair, $unpair) {
	$pairedResponse = 1;
	$unpairedResponse = 1;
	$settingStatus = array('fail' => 'Something went wrong, unable to save DTT setting.');

	if(!empty($pair)) {
		$pairValues = array();
		foreach ($pair as $key=> $configId) {
			$deviceTime = date('Y-m-d H:i:s', strtotime($deviceTime));
			$pairValues[] = "('".$configId."', '".$deviceUUID."', '".$serverId."', '".$deviceTime."')";
		}
		$pairValues = implode(", ", $pairValues);
		$pairedResponse = lavu_query('INSERT INTO `dtt_pairs` (`config_id`, `device_uuid`, `created_by`, `created_date`) VALUES ' . $pairValues);
	}

	if (!empty($unpair)) {
		$unpairValues = implode(", ", $unpair);
		$unpairedResponse = lavu_query('DELETE FROM `dtt_pairs` WHERE `device_uuid` = "'.$deviceUUID.'" and `config_id` in ('.$unpairValues.')');
	}

	if ($pairedResponse && $unpairedResponse) {
		$settingStatus = array('success' => 'DTT settings successfully saved.');
	}

	return $settingStatus;
}

/**
 * This function is used to get pending eConduit orders transactions. For POS 4.0
 * @param  $order_id
 * @param  $ioid
 * return all pending transations of given order id.
 */
function getPendingEconduitOrders($order_id, $ioid) {
	$select_query = "select `order_id`, `check`, `transaction_id`, `amount`, `terminal_id` from `lip_order_transaction` where `order_id` = '".$order_id."' AND `amount` > 0";
	$result_select = lavu_query($select_query);
	$numRec = mysqli_num_rows($result_select);
	$result = array();
    if ($numRec > 0) {
		while ($data = mysqli_fetch_array($result_select))
		{
			$check = $data['check'];
			$transactionId = $data['transaction_id'];
			$amount = $data['amount'];
			$terminal_id = $data['terminal_id'];
			$transactionIdQuery = "select `transaction_id` from `cc_transactions` where order_id = '".$order_id."' AND `check` = '".$check."' AND `transaction_id`= '" . $transactionId . "' AND ioid = '" . $ioid . "'" ;
			$resultTransactionIdQuery = lavu_query($transactionIdQuery);
			$numRowTransactionId = mysqli_num_rows($resultTransactionIdQuery);
			if( $numRowTransactionId == 0 ){
				$result[] = array('order_id' => $order_id, 'transaction_id' => $transactionId, 'check' => $check, 'amount' => $amount, 'ioid' => $ioid, 'terminal_id' => $terminal_id);
			}
		}
	}
	return json_encode($result);
}
/**
 * This function is used to get OLM configuration variables
 * @param  $message
 * return all OLM configuration variables.
 */
function loadOlmConfigurationData(&$message) {
	#
	$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
	$message['olm_online_frequency'] = getenv('OLM_ONLINE_FREQUENCY');
	$message['olm_offline_frequency'] = getenv('OLM_OFFLINE_FREQUENCY');
	$message['olm_health_check_uri'] = $protocol.$_SERVER['SERVER_NAME']."/".getenv('OLM_HEALTH_CHECK_URI');
}

/**
* This function is used to get Country Region Details status is active.
*
* @return array list of country region details.
*/
function getCountryRegionDetails() {
	lavu_query("SET NAMES 'utf8'");
	$country_query =lavu_query(" SELECT id, alphabetic_code, name, monetary_symbol, currency_separator, country_code, currency  FROM `poslavu_MAIN_db`.`country_region` WHERE  active='1'");
	$count = 0;
	while ($countryInfo = mysqli_fetch_assoc($country_query)) {
		$return[$count]['name'] = $countryInfo['name'];
		$return[$count]['alphabetic_code'] = $countryInfo['alphabetic_code'];
		$return[$count]['monetary_symbol'] =  trim($countryInfo['monetary_symbol']);
		$return[$count]['currency_separator'] = $countryInfo['currency_separator'];
		$return[$count]['country_code'] = $countryInfo['country_code'];
		$return[$count]['currency'] =  $countryInfo['currency'];
		$return[$count]['id'] =  $countryInfo['id'];
		$count++;
	}
	return $return;
}

/**
* This function is used to get pizza forced mods.
* @param  $id integer forced modifier list id
* @param  $data_name string location name
* @return $modsStr $partialServingPrice array pipe separated list of pizza forced mods and partial serving price.
*/
function getPizzaForcedMods($id, $data_name)
{
	$modsStr = '';
	$partialServingPrice = '';
	if ($id) {
		$obj = new ConfigLocal($data_name);
		$result = $obj->getInfo('value AS creator_id', 'setting = "pizza creator" AND value6 = ' . $id);
		if ($result) {
			$creatorId = $result[0]['creator_id'];
			$whereCond = '`value7` = "' . $creatorId . '"';
			if ($creatorId == '0') {
				$whereCond = '(' . $whereCond . ' OR `value7` = "")';
			}
			$result = $obj->getInfo('value2 AS mod_id, value10 AS serving_price', 'setting = "pizza_mods" AND ' . $whereCond . ' ORDER BY `value8`, `value6`');
			$modsIds = array();
			$partialServingPrices = array();
			if (!empty($result)) {
				foreach ($result as $mods) {
					$modsIds[] = $mods['mod_id'];
					$partialServingPrices[] = $mods['mod_id'] . '-' . $mods['serving_price'];
				}
				$modsStr             = implode('|', $modsIds);
				$partialServingPrice = implode('|', $partialServingPrices);
			}
		}
	}
	return array($modsStr, $partialServingPrice);
}

/**
* This function is used to return no items response.
* @param  $category string Category name
* @return json string with status and category name
*/
function onNoItems($category)
{
	return json_encode(array('json_status' => 'NoItems', 'for_category' => $category));
}

/**
* This function is used to return config setting with default Dual Cash Drawer Access level (Ref. LP-11738)
* @param  $message array array of config setting
* @return array $messsage with dual cash drawer PIN prompt setting access lavel value
*/
function setDefaultDualDrawerAccessLevel(&$message) {
	if (!isset($message['config_settings']['level_to_access_dual_cash_drawer_pin'])) {
		$message['config_settings']['level_to_access_dual_cash_drawer_pin'] = 2;
	}
	return $message;
}