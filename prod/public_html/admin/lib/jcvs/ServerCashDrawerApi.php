<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/cp/resources/core_functions.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/orm/ServerCashDrawer.php');

class ServerCashDrawerApi{
		private $errCode;
		private $res;
		private $req;
		public $ormObj;
		public $resFormat;
		public $action;

		public function __construct($dataname, $reqData = array()){
			$this->dataname=$dataname;
			$this->resFormat='json';
			if (!empty($reqData) ) {
				$this->req = $reqData;
				$this->action = $reqData['action'];
				$this->ormObj = new ServerCashDrawer($this->dataname);
				$this->ormObj->printer = $reqData['printer'];
				$this->ormObj->drawer = $reqData['drawer'];
				$this->ormObj->serverId = $reqData['serverId'];
				$this->ormObj->assignedBy = $reqData['assignedBy'];
				$this->ormObj->createdBy = $reqData['createdBy'];
				$this->ormObj->updatedBy = $reqData['updatedBy'];
				$this->ormObj->updatedDate = $reqData['updatedDate'];
				$this->ormObj->_deleted = $reqData['deleted'];
				$this->ormObj->resFormat = $reqData['resformat'];
			}
		}

		/*public function setMsg($tag,$status,$replace=''){
			$err=getErrMessage($tag,$replace);
			$this->res['status']=$status;
			$this->res['message']=$err['body'];
			$this->res['response_title']=$err['title'];
		}*/

		public function setMsg($tag,$status,$msg,$replace=''){
			$this->res['data'] = $msg['data'];
			$this->res['response_title'] = $msg['title'];
			$this->res['message'] = $msg['body'];
			$this->res['status'] = $status;

		}


		private function getCashDrawer(){
			$whereCond = '';
			$seperator = '';
			if ($this->ormObj->printer) {
				$whereCond .= 'printer = "' . $this->ormObj->printer . '"';
				$seperator = ' AND ';
			}

			if ($this->ormObj->serverId) {
				$whereCond .= $seperator . 'server_id = "' . $this->ormObj->serverId . '"';
				$seperator = ' AND ';
			}

			if ($this->ormObj->drawer) {
				$whereCond .= $seperator . 'drawer = "' . $this->ormObj->drawer . '"';
			}

			if ($this->ormObj->updatedDate) {
				$whereCond .= $seperator . 'updated_date LIKE "' . $this->ormObj->updatedDate . '%"';
			}
			$data = $this->ormObj->getInfo("", $whereCond);
			$this->setMsg('',1, array('data'=>$data));
			return 1;	
		}

		private function captureCashDrawer(){
			$data=$this->getCashDrawer();
			if (!empty($this->res['data'])) {
				$this->setMsg('', 1, array('data'=>$data));
				return 1;
			} else {
				$tmpObj = clone $this->ormObj;
				$tmpObj->drawer = 1;
				$tmpObj->insertDb();
				$tmpObj->drawer = 2;
				$tmpObj->insertDb();
				$tmpObj = null;
				$this->setMsg('DCD_MSG_3', 1, array('body'=>'Cash drawer not found hence created new entry', 'title'=>'capture cashdrawer'));
			}
		}

		private function assignServer(){
			$data = $this->ormObj->getInfo('id', 'printer = "' . $this->ormObj->printer . '" AND server_id = "' . $this->ormObj->serverId . '" AND drawer = "' . $this->ormObj->drawer . '"', '', '', true);
			if ($data) {
				$this->ormObj->_deleted = 0;
				$result=$this->ormObj->updateDb('printer = "' . $this->ormObj->printer . '" AND drawer = "' . $this->ormObj->drawer . '" AND server_id = "' . $this->ormObj->serverId . '"');
				$this->setMsg('DCD_MSG_1', 1, array('body'=>'Server unassigned successfuly', 'title'=>'Success'));//Server unassigned assigned successfully
			} else {
				$result = $this->ormObj->insertDb();
				$this->setMsg('DCD_MSG_1', 1, array('body'=>'Server assigned successfuly', 'title'=>'Success'));//Server assigned successfully
			}
			return 1;
			
		}

		private function unAssignServer(){
			$this->ormObj->_deleted = 1;
			$result=$this->ormObj->updateDb('printer = "' . $this->ormObj->printer . '" AND drawer = "' . $this->ormObj->drawer . '" AND server_id = "' . $this->ormObj->serverId . '"');
			$this->setMsg('DCD_MSG_1', 1, array('body'=>'Server unassigned successfuly', 'title'=>'Success'));//Server unassigned assigned successfully
			return 1;

		}

		private function getDualCashDrawerUserInfo(){
			global $location_info;
			$local_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
			$date = date('Y-m-d', strtotime($local_time));
			$data = $this->ormObj->getInfo('server_id, printer, drawer', 'updated_date LIKE "' . $date . '%"');
			$drawerInfo = array();
			$response = array();
			$status = 1;
			$body = '';
			$title = '';
			if ($data) {
				foreach ($data as $details) {
					if ($details['server_id'] > 0 ) {
						$drawerInfo[$details['server_id']][] = $details;
					}
				}

				foreach ($drawerInfo as $server_id => $serverDrawer) {
					foreach ($serverDrawer as $key => $info) {
						$response['server_id'][$server_id][$key]['printer'] = $info['printer'];
						$response['server_id'][$server_id][$key]['drawer'] = $info['drawer'];
					}
				}
			}

			if ($this->ormObj->hasError) {
				$status = 0;
				$body = 'Something wrong';
				$title = 'Error';
			}

			$this->setMsg('', $status, array('body' => $body, 'title' => $title, 'data' => $response));
		}
		/**
		 * Get printer name which is assigned to specific user and drawer
		 * @input param null
		 * @return array
		 */
		private function getDrawerPrinter() {
			global $location_info;
			$local_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
			$date = date('Y-m-d', strtotime($local_time));
			$whereCond = 'server_id = "' . $this->ormObj->serverId . '" AND drawer = "' . $this->ormObj->drawer . '" AND updated_date LIKE "' . $date . '%" ORDER BY updated_date DESC LIMIT 1';
			$data = $this->ormObj->getInfo("", $whereCond);
			$this->setMsg('',1, array('data'=>$data));
			return 1;
		}

		public function doProcess(){
			$functions=array(
							'getcashdrawer'=>'getCashDrawer',
							'capturecashdrawer'=>'captureCashDrawer',
							'assignserver'=>'assignServer',
							'get_dual_cash_drawer_user_info'=>'getDualCashDrawerUserInfo',
							'unassignserver'=>'unAssignServer',
							'getdrawerprinter'=>'getDrawerPrinter'
							);

			if ($functions[$this->action]) {
				if ( ($this->action == 'assignserver' && ($this->req['printer'] == '' || $this->req['drawer'] == '') ) ||
				   (($this->action == 'capturecashdrawer' || $this->action == 'getcashdrawer') && $this->req['printer'] == '') || ($this->action == 'getdrawerprinter' && ($this->req['serverId'] == '' || $this->req['drawer'] == '')) ) {
					$this->setMsg('DCD_ERR_1',0,array('body'=>'Invalid Request','title'=>'Failed')); //Invalid request
				} else {
					$this->{$functions[$this->action]}();
				}
			} else {
				$this->setMsg('DCD_ERR_1');
			}

			if ($this->resFormat=='json') {
				return json_encode($this->res);
			}

			return $this->res;

		}

}

?>
