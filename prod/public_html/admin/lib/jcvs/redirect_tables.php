<?php
	//Author:     Brian D.
	//Purpose:    To act as a configuration file that maps the _use_tunnel variable to the corresponding table.
	//            The key is the integer index of the tunnel select + 1 that's recently been added to manage.
	//            Thus no tunnel is 0, the first tunnel option is 1, etc.
	//Notes:      By extracting this out into its own file, this file may contain syntax errors without breaking the critical
	//            functionality of jc_inc7/jc_functions.  This is a .php file so that if it ends up on LLS it will be
	//            encrypted.
	/*
	function getProxyIPTable(){
		$proxyIPTable = array();
		$proxyIPTable[00]  = null;
		$proxyIPTable[01]  = "http://lsvpn.poslavu.com/"; // set to https when certificate issue is resolved for poslavu.com
		$proxyIPTable[02]  = "http://www.poslavu.com/lsvpn/";
		$proxyIPTable[03]  = "http://10.0.3.31:22021/";//RSSH Tunnel 1
		$proxyIPTable[04]  = "http://10.0.3.31:22022/";//RSSH Tunnel 2
		$proxyIPTable[05]  = "http://10.0.3.31:22023/";//...
		$proxyIPTable[06]  = "http://10.0.3.31:22024/";
		$proxyIPTable[07]  = "http://10.0.3.31:22025/";
		$proxyIPTable[08]  = "http://10.0.3.31:22026/";
		$proxyIPTable[09]  = "http://10.0.3.31:22027/";
		$proxyIPTable[10] = "http://216.243.114.158:22028/";
		$proxyIPTable[11] = "http://216.243.114.158:22029/";//RSSH Tunnel 9
		$proxyIPTable['tunnels_proxy_external_ip'] = '216.243.114.158';
		$proxyIPTable['tunnels_proxy_internal_ip'] = '10.0.3.31';
		return $proxyIPTable;
	}
	*/
/* 	error_log('here'); */

	function getProxyIPTable(){
		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`tunnels`");
		$proxyIPTable = array();
		while($currRow = mysqli_fetch_assoc($result)){
			$currProxyIP = $currRow['proxy_ip'];
			$currProxyIPWithPort = !empty($currRow['proxy_port']) ? $currProxyIP.':'.$currRow['proxy_port'] : $currProxyIP;
			$proxyIPTable[$currRow['order_key']] = $currProxyIPWithPort;
		}
		$proxyIPTable['tunnels_proxy_external_ip'] = '216.243.114.158';
		$proxyIPTable['tunnels_proxy_internal_ip'] = '10.0.3.31';
		return $proxyIPTable;
	}

	//Appending this to the end of netpath will yield the full path to reach the LavuTask api.
	function getLavuTaskWebpathTable(){
		$webpaths = array();
		$webpaths['lavu'] = '/components/lavu_task/task_api.php'; //Start with dir character '/'
		$webpaths['ers'] = '/cp/resources/views/maintenance/maintenanceGatewayAPI.php'; //Start with dir character '/'
		return $webpaths;
	}
?>