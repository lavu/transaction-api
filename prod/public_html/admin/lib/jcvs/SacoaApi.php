<?php 

include_once($_SERVER['DOCUMENT_ROOT'].'/cp/resources/core_functions.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/lib/jcvs/Extensions.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/orm/ConfigLocal.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/orm/GatewayAuthToken.php');

class SacoaApi{
		private	$errCode;
		public 	$apiHost;
		public	$apiUsername;
		public	$apiPass;
		public 	$extensionId;
		public 	$dataname;
		public 	$cardInfo=array();
		public 	$extensionObj;
		public  $extensionTitle='Sacoa Playcard';
		public 	$gatewayAuthTokenObj;
		public	$conversionRate;	
		public	$credits=0;

		public function init(){
		
			$this->extensionObj = new Extension($this->extensionTitle);
	
			$this->errCode = array(
				'1001' => 'Failure',
				'1002' => 'Success'
			);
			
			if(!$this->apiHost){
				$this->setApiHost();
			}

                     	$this->endpoints=array(
                                'login' => $this->apiHost.'/login',
                                'sell' => $this->apiHost.'/pos/cardSell',
                                'recharge' => $this->apiHost.'/card/Recharge',
                                'balance' => $this->apiHost.'/card/balance',
                                'debit' => $this->apiHost.'/card/debit',
                                'transfer' => $this->apiHost.'/card/transfer',
                                'consolidate' => $this->apiHost.'/card/consolidate',
                                'redeem' => $this->apiHost.'/redemption/ticketsReturn'
                        );
			
		}

		//TODO: WRITE SOMETHING IN THIS
		function __construct(){
		}
   
    		function setErr(&$errMsg,$tag,$replace=''){
    			$err=getErrMessage($tag,$replace);
    			$errMsg['message']=$err['body'];
    			$errMsg['response_title']=$err['title'];	
    		}

		function setConversionRate(){
                        //get conversion rate                                       
                        $obj=new ConfigLocal($this->dataname);
                        $result=$obj->getInfo('setting,value','setting IN ("sacoa_conversion_rate")');
                        $this->conversionRate=($result[0]['value']!='' && $result[0]['value'])?$result[0]['value']:1;
		}

		function setApiHost(){
			if(!$this->dataname || !$this->extensionObj->id) return;
			$obj=new GatewayAuthToken($this->dataname);
			$result=$obj->getInfo('extra_info','dataname="'.$this->dataname.'" AND extension_id='.$this->extensionObj->id);
			$arr=json_decode($result[0]['extra_info'],1);
			$this->apiHost=$arr['service_url'];
		}

		function sacoaApi($request, $endurl, $login = ""){
			$soap_do = curl_init();
			curl_setopt($soap_do, CURLOPT_URL,            $endurl);
			curl_setopt($soap_do, CURLOPT_RETURNTRANSFER, true );
			curl_setopt($soap_do, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($soap_do, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($soap_do, CURLOPT_POST,           true );
			curl_setopt($soap_do, CURLOPT_POSTFIELDS,    $request);
			curl_setopt($soap_do, CURLOPT_HTTPHEADER,     array('Content-Type: application/json; charset=utf-8', 'Content-Length: '.strlen($request) ));
			$result = curl_exec($soap_do);
			$err = curl_error($soap_do);
			if(strstr($err,"Connection timed out")){
				$result='{"statusCode":503,"body":{"errorCode":"503","message":"Failed to connect"}}';
				return $result;
			}

			$data = json_decode($result, 1);
			if($login == 1 && $data['body']['authorized'] == 1){
				//$token_generated_date = date('Y-m-d H:i:s',time());
				$tokenExpiryDate = $this->extensionObj->extraInfo['token_expiry_duration'];
				$token = $data['body']['token'];
				$location_name = $this->dataname;
				$location_id = 1;
				$deleted = 0;
				$check_token_query = mlavu_query("SELECT * from `gateway_auth_tokens` WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $this->dataname, $this->extensionObj->id);
				$Numrows = mysqli_num_rows($check_token_query);
				if($Numrows > 0){
					$update_token = mlavu_query("UPDATE `gateway_auth_tokens` SET `token` = '[1]', `token_expiration_date` = DATE_ADD(now(),INTERVAL ".$tokenExpiryDate." MINUTE), `token_generated_date` = now() WHERE `dataname` = '[2]' AND `extension_id` = '[3]'", $token, $this->dataname, $this->extensionObj->id);
				} else {
					$emp_extra_info = json_encode(array("service_url" => $this->apiHost, "empId" => $data['body']['empId'])); //employee id
					$insert_token = mlavu_query("INSERT INTO `gateway_auth_tokens`(`dataname`, `extension_id`, `merchant_id`, `token`, `token_expiration_date`, `permission_granted`, `location_name`, `location_id`, `token_generated_date`, `extra_info`, `_deleted`)
 				VALUES ('".$this->dataname."', '".$this->extensionObj->id."', '', '".$token."', DATE_ADD(now(),INTERVAL ".$tokenExpiryDate." MINUTE), '1', '".$this->dataname."', '".$location_id."', now(), '".$emp_extra_info."' , '".$deleted."')");
				}
			}
			
			return $result;
		}

		function validateCardNumber($card_number){
			$result = mlavu_query("SELECT * FROM `poslavu_".$this->dataname."_db`.`user_cards` WHERE `card_number` = '[1]' AND `is_deleted` = 0", $card_number);
			$Numrows = mysqli_num_rows($result);
			if($Numrows > 0){
				$rec=mysqli_fetch_assoc($result);
				$this->cardInfo[$card_number]['status']=$rec['status'];
				return 1;
			}else{
				return 0;
			}
		}

		function issueNewCard($customer_id, $card_number, $amount, $server_name)
		{
			$rtn_response = array();
			$validate_response = $this->validateCardNumber($card_number);
			if($validate_response == 0){
				$check_token_expiry = mlavu_query("SELECT * FROM `gateway_auth_tokens` WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $this->dataname, $this->extensionObj->id);
				$data = mysqli_fetch_assoc($check_token_expiry);
				$extra_info = json_decode($data['extra_info'], 1);
				$token = $data['token'];

				//get-set conversion rate
				$this->setConversionRate();
				$this->credits=$this->conversionRate * $amount;
				
				$request_params = array("posId" => 1, "empId" => $extra_info['empId'], "cardNumber" => $card_number, "token" => $token, "ticketAmount" => 0, "extEmpId" => $customer_id, 
						"credits" => $this->credits, "bonus" => 0, "status" => "O", "description" => "");
				$request = json_encode($request_params);
				$endurl = $this->endpoints['sell'];
				$result = $this->sacoaApi($request, $endurl, "");
				$response = json_decode($result, 1);
				
				$apiErrCodes = array("Token Expired", "Invalid Token");
				if(in_array($response['body'], $apiErrCodes) || $response['statusCode'] != 200){
					$rtn_response['flag'] = "0";
					if($response['body']['errorCode'] == 5){
						$this->setErr($rtn_response,'SACOA_ERR_3');
					}else if($response['body']['errorCode'] == 503){
						$this->setErr($rtn_response,'SACOA_ERR_7');
					}else{
						$this->setErr($rtn_response,'SACOA_ERR_2');
						$rtn_response['message'] = is_array($response['body']) ? $response['body']['msg'] : $response['body'];
					}
					$rtn_response['txnid'] = "";
					$rtn_response['order_id'] = "";
				}else{
					$created_datetime = date('Y-m-d H:i:s');
					//  0 - success flag ("1")
					//  1 - [transaction_id]
					//  2 - [card_desc] (last_four)
					//  3 - response message
					//  4 - [auth_code]
					//  5 - [card_type]
					//  6 - [record_no]
					//  7 - [amount]
					//  8 - [order_id]
					//  9 - [ref_data]
					// 10 - [process_data]
					// 11 - new balance
					// 12 - bonus message
					// 13 - [first_four]
					// 14 - [last_mod_ts]
					$rtn_response['flag'] = "1"; // 0
					$rtn_response['txnid'] = ""; // transaction ID
					$rtn_response['card_desc'] = substr($card_number, -4, 4);
					$rtn_response['message'] = $response['body']['card'];
					$rtn_response['auth_code'] = "";
					$rtn_response['card_type'] = "Sacoa Card";
					$rtn_response['record_no'] = "";
					$rtn_response['amount'] = $amount;
					$rtn_response['order_id'] = "";
					$rtn_response['ref_data'] = '{"conversion_rate":'.$this->conversionRate.'}';
					$rtn_response['process_data'] = "sacoa_card"; // 10
					$rtn_response['new_balance'] = $response['body']['credits'];
					$rtn_response['tickets'] = $response['body']['tickets'];
					$rtn_response['first_four'] = substr($card_number, 0, 4);
					$rtn_response['last_mod_ts'] = strtotime($created_datetime);
					//$get_server = mlavu_query("SELECT f_name, l_name FROM `poslavu_".$this->dataname."_db`.`users WHERE `id` = '[1]' AND `active` = '[2]' AND `_deleted` != 1", $server_id, $active);
					$check_cardnumber_query = mlavu_query("SELECT * FROM `poslavu_".$this->dataname."_db`.`user_cards` WHERE `customer_id` = '[1]' AND `card_number` = '[2]' AND `status` = 1", $customer_id, $card_number);
					$Numrows = mysqli_num_rows($check_cardnumber_query);
					if($Numrows > 0){
						$update_user = mlavu_query("UPDATE `poslavu_".$this->dataname."_db`.`user_cards` SET `updated_date` = '[1]', `updated_by` = '[2]' WHERE `customer_id` = '[3]'", $created_datetime, $server_name, $customer_id);
					}else{
						$insert_user = mlavu_query("INSERT INTO `poslavu_".$this->dataname."_db`.`user_cards`(`id`, `customer_id`, `card_number`, `status`, `is_deleted`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES(0, '".$customer_id."', '".$card_number."', 1, 0, '".$created_datetime."', '".$server_name."', '".$created_datetime."', '".$server_name."')");
					} 
				}
				//echo "<pre>";print_r($rtn_response);echo "</pre>";
				return $rtn_response;
			}else{
				$this->setErr($rtn_response,'SACOA_ERR_4');
				$rtn_response['flag'] = "0";
				$rtn_response['txnid'] = "";
				$rtn_response['order_id'] = "";
				return $rtn_response; 
			}
		}
            
		function reloadCard($customer_id, $card_number, $amount, $card_action)
		{
			$rtn_response = array();
			$validate_response = $this->validateCardNumber($card_number);
			
			if($validate_response == 1 && $this->cardInfo[$card_number]['status']){
				if($this->cardInfo[$card_number]['status']==1){
					$check_token_expiry = mlavu_query("SELECT * FROM `gateway_auth_tokens` WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $this->dataname, $this->extensionObj->id);
					$data = mysqli_fetch_assoc($check_token_expiry);
					$extra_info = json_decode($data['extra_info'], 1);
					$token = $data['token'];
					
					//get-set conversion rate
					$this->setConversionRate();
					$this->credits=$this->conversionRate * $amount;
	
					$request_params = array("posId" => 1, "empId" => $extra_info['empId'], "cardNumber" => $card_number, "token" => $token, "credits" => $this->credits, "bonus" => 0,
							"courtesy" => 0, "status" => "x", "payment" => 0, "minutes" => "", "deposit" => 0, "oldPassport" => 0 , "extEmpId" => $customer_id);
					$request = json_encode($request_params);
					$endurl = $this->endpoints['recharge'];
					$result = $this->sacoaApi($request, $endurl, "");
	
					$response = json_decode($result, 1);
					$apiErrCodes = array("Token Expired", "Invalid Token");
					if(in_array($response['body'], $apiErrCodes) || $response['statusCode'] != 200){
						if($response['body']['errorCode']==503){
							$this->setErr($rtn_response,'SACOA_ERR_7');
						}else{
							$this->setErr($rtn_response,'SACOA_ERR_2');
							$rtn_response['flag'] = "0";
							$rtn_response['message'] = is_array($response['body']) ? $response['body']['msg'] : $response['body'];
							$rtn_response['txnid'] = "";
							$rtn_response['order_id'] = "";
						}
					}else{
						$created_datetime = date('Y-m-d H:i:s');
						//  0 - success flag ("1")
						//  1 - [transaction_id]
						//  2 - [card_desc] (last_four)
						//  3 - response message
						//  4 - [auth_code]
						//  5 - [card_type]
						//  6 - [record_no]
						//  7 - [amount]
						//  8 - [order_id]
						//  9 - [ref_data]
						// 10 - [process_data]
						// 11 - new balance
						// 12 - bonus message
						// 13 - [first_four]
						// 14 - [last_mod_ts]
						$rtn_response['flag'] = "1"; // 0
						$rtn_response['txnid'] = ""; // transaction ID
						$rtn_response['card_desc'] = substr($card_number, -4, 4);
						$rtn_response['message'] = $response['body']['card'];
						$rtn_response['auth_code'] = "";
						$rtn_response['card_type'] = "Sacoa Card";
						$rtn_response['record_no'] = "";
						$rtn_response['amount'] = $amount;
						$rtn_response['order_id'] = "";
						$rtn_response['ref_data'] = '{"conversion_rate":'.$this->conversionRate.'}';
						$rtn_response['process_data'] = "sacoa_card"; // 10
						$rtn_response['new_balance'] = $response['body']['credits'];
						$rtn_response['tickets'] = $response['body']['tickets'];
						$rtn_response['first_four'] = substr($card_number, 0, 4);
						$rtn_response['last_mod_ts'] = strtotime($created_datetime);
						//echo "<pre>";print_r($rtn_response);echo "</pre>";
						return $rtn_response;
					}
				}else{
					$this->setErr($rtn_response,'SACOA_ERR_5');
					$rtn_response['flag'] = "0";
					$rtn_response['txnid'] = "";
					$rtn_response['order_id'] = "";
					return $rtn_response;
				}
			}else{
				$this->setErr($rtn_response,'SACOA_ERR_3');
				$rtn_response['flag'] = "0";
				$rtn_response['txnid'] = "";
				$rtn_response['order_id'] = "";
				return $rtn_response; 
			}
		}
		
		function viewBalance($customer_id, $card_number){
			$rtn_response = array();

			$validate_response = $this->validateCardNumber($card_number);
			if($validate_response == 1 && $this->cardInfo[$card_number]['status']){
				$check_token_expiry = mlavu_query("SELECT * FROM `gateway_auth_tokens` WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $this->dataname, $this->extensionObj->id);
				$data = mysqli_fetch_assoc($check_token_expiry);
				$extra_info = json_decode($data['extra_info'], 1);
				$token = $data['token'];

				$request_params = array("posId" => 1, "empId" => $extra_info['empId'], "cardNumber" => $card_number, "token" => $token, "extEmpId" => $customer_id);
				$request = json_encode($request_params);
				$endurl = $this->endpoints['balance'];
				$result = $this->sacoaApi($request, $endurl, "");
				
				$response = json_decode($result, 1);
				$apiErrCodes = array("Token Expired", "Invalid Token");
				if(in_array($response['body'], $apiErrCodes) || $response['statusCode'] != 200){
					if($response['body']['errorCode']==503){
						$this->setErr($rtn_response,'SACOA_ERR_7');
					}else{
						$this->setErr($rtn_response,'SACOA_ERR_2');
						$rtn_response['flag'] = "0";
						$rtn_response['message'] = is_array($response['body']) ? $response['body']['msg'] : $response['body'];
						$rtn_response['txnid'] = "";
						$rtn_response['order_id'] = "";
					}
				}else{
					$created_datetime = date('Y-m-d H:i:s');
					//  0 - success flag ("1")
					//  1 - [transaction_id]
					//  2 - [card_desc] (last_four)
					//  3 - response message
					//  4 - [auth_code]
					//  5 - [card_type]
					//  6 - [record_no]
					//  7 - [amount]
					//  8 - [order_id]
					//  9 - [ref_data]
					// 10 - [process_data]
					// 11 - new balance
					// 12 - bonus message
					// 13 - [first_four]
					// 14 - [last_mod_ts]
					$total_response = array($response['body']['card'], $response['body']['credits'], $response['body']['bonus'], $response['body']['courtesy'], $response['body']['minutes'], $response['body']['tickets']);
					$response_mesg = implode("-", $total_response);
					$rtn_response['flag'] = "1"; // 0
					$rtn_response['txnid'] = ""; // transaction ID
					$rtn_response['card_desc'] = substr($card_number, -4, 4);
					$rtn_response['message'] = $response_mesg;
					$rtn_response['auth_code'] = "";
					$rtn_response['card_type'] = "Sacoa Card";
					$rtn_response['record_no'] = "";
					$rtn_response['amount'] = $response['body']['credits'];
					$rtn_response['order_id'] = "";
					$rtn_response['ref_data'] = "";
					$rtn_response['process_data'] = "sacoa_card"; // 10
					$rtn_response['new_balance'] = "";
					$rtn_response['tickets'] = $response['body']['tickets'];
					$rtn_response['first_four'] = "";
					$rtn_response['last_mod_ts'] = strtotime($created_datetime);
				}
				//echo "<pre>";print_r($rtn_response);echo "</pre>";
				return $rtn_response;
			}else{
				$this->setErr($rtn_response,'SACOA_ERR_3');
				$rtn_response['flag'] = "0";
				$rtn_response['txnid'] = "";
				$rtn_response['order_id'] = "";
				return $rtn_response;
			}
		}
		
		function debitAmount($customer_id, $card_number, $amount){
			$rtn_response = array();
			$validate_response = $this->validateCardNumber($card_number);
			if($validate_response == 1 && $this->cardInfo[$card_number]['status']){
				if($this->cardInfo[$card_number]['status']==1){
					$check_token_expiry = mlavu_query("SELECT * FROM `gateway_auth_tokens` WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $this->dataname, $this->extensionObj->id);
					$data = mysqli_fetch_assoc($check_token_expiry);
					$extra_info = json_decode($data['extra_info'], 1);
					$token = $data['token'];
                               		
					//get-set conversion rate
                                	$this->setConversionRate();
                                	$this->credits=$this->conversionRate * $amount;			
		
					$request_params = array("posId" => 1, "empId" => $extra_info['empId'], "cardNumber" => $card_number, "token" => $token, "credits" => $this->credits, 
					"bonus" => 0,  "courtesy" => 0, "status" => "V", "payment" => 0, "minutes" => "", "deposit" => 0, "oldPassports" => 0, "debitCode" => 101, "extEmpId" => $customer_id);
					$request = json_encode($request_params);
					$endurl = $this->endpoints['debit'];
					$result = $this->sacoaApi($request, $endurl, "");
					$response = json_decode($result, 1);
					$apiErrCodes = array("Token Expired", "Invalid Token");
					$created_datetime = date('Y-m-d H:i:s');
					if ($response['statusCode'] != 200 && stripos(strtolower($response['body']['msg']), 'Credits underflow') !== false) {
						$result = $this->viewBalance($customer_id, $card_number, $this->extensionObj->id);
						$this->setErr($rtn_response,'SACOA_ERR_6',array('val1'=>$result['amount']));
						$rtn_response['flag'] = "0";
						$rtn_response['txnid'] = "";
						$rtn_response['order_id'] = "";
					}else if(in_array($response['body'], $apiErrCodes) || $response['statusCode'] != 200){
						if($response['body']['errorCode']==503){
							$this->setErr($rtn_response,'SACOA_ERR_7');
						}else{
							$this->setErr($rtn_response,'SACOA_ERR_2');
							$rtn_response['flag'] = "0";
							$rtn_response['txnid'] = "";
							$rtn_response['order_id'] = "";
						}
					}else{
						//  0 - success flag ("1")
						//  1 - [transaction_id]
						//  2 - [card_desc] (last_four)
						//  3 - response message
						//  4 - [auth_code]
						//  5 - [card_type]
						//  6 - [record_no]
						//  7 - [amount]
						//  8 - [order_id]
						//  9 - [ref_data]
						// 10 - [process_data]
						// 11 - new balance
						// 12 - bonus message
						// 13 - [first_four]
						// 14 - [last_mod_ts]
						$total_response = array($response['body']['card'], $response['body']['credits'], $response['body']['bonus'], $response['body']['courtesy'], $response['body']['minutes'], $response['body']['tickets']);
						$response_mesg = implode("-", $total_response);
						$rtn_response['flag'] = "1"; // 0
						//$rtn_response['txnid'] = ""; // transaction ID
						$rtn_response['txnid'] = $this->getTransactionId();
						//$rtn_response['card_desc'] = substr($card_number, -4, 4);
						$rtn_response['card_desc'] = $card_number;
						$rtn_response['message'] = $response_mesg;
						$rtn_response['auth_code'] = "";
						$rtn_response['card_type'] = "Sacoa Card";
						$rtn_response['record_no'] = "";
						$rtn_response['amount'] = $amount;
						$rtn_response['order_id'] = "";
						$rtn_response['ref_data'] = '{"conversion_rate":'.$this->conversionRate.'}';
						$rtn_response['process_data'] = "sacoa_card"; // 10
						$rtn_response['new_balance'] = $response['body']['credits'];
						$rtn_response['tickets'] = $response['body']['tickets'];
						$rtn_response['first_four'] = substr($card_number, 0, 4);;
						$rtn_response['last_mod_ts'] = strtotime($created_datetime);
					}
					//echo "<pre>";print_r($rtn_response);echo "</pre>";
					return $rtn_response;
				}else{
					$this->setErr($rtn_response,'SACOA_ERR_5');
					$rtn_response['flag'] = "0";
					$rtn_response['txnid'] = "";
					$rtn_response['order_id'] = "";
					return $rtn_response;
				}
			}else{
				$this->setErr($rtn_response,'SACOA_ERR_3');
				$rtn_response['flag'] = "0";
				$rtn_response['txnid'] = "";
				$rtn_response['order_id'] = "";
				return $rtn_response;
			}
		}
		
		function transferAmount($source_card, $target_card, $amount, $server_name){
			$rtn_response = array();
			$created_datetime = date('Y-m-d H:i:s');
			if($source_card != $target_card)
			{
				$validate_response = $this->validateCardNumber($source_card);
				if($validate_response == 1 && $this->cardInfo[$source_card]['status'])
				{ 
					$check_token_expiry = mlavu_query("SELECT * FROM `gateway_auth_tokens` WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $this->dataname, $this->extensionObj->id);
					$data = mysqli_fetch_assoc($check_token_expiry);
					$extra_info = json_decode($data['extra_info'], 1);
					$token = $data['token'];
					$validate_response = $this->validateCardNumber($target_card);
					$get_customerid_query = mlavu_query("SELECT `customer_id` FROM `poslavu_".$this->dataname."_db`.`user_cards` WHERE `card_number` = '[1]' AND `status` = 1 AND `is_deleted` = 0",  $source_card);
					$get_customerid = mysqli_fetch_assoc($get_customerid_query);
					$customer_id = $get_customerid['customer_id'];
					//print_r($get_customerid);
					if($validate_response == 0){ // transfer
						//Request: { "posId":1, "empId":1 , "cardNumber":300000, "token":"0f23d9b5c5....", "targetCard":300001, "extEmpId":1    } 
						$request_params = array("posId" => 1, "empId" => $extra_info['empId'], "cardNumber" => $source_card, "token" => $token, "targetCard" => $target_card, "extEmpId" => $customer_id);
						$request = json_encode($request_params);
						$endurl = $this->endpoints['transfer'];
					}else{ // consolidated
						//echo 'Request: { "posId":1, "empId":1 , "cardNumber":300000, "token":"0f23d9b5c5....", " sourceCards":[300001,300002] , "extEmpId":1 }';
						$request_params = array("posId" => 1, "empId" => $extra_info['empId'], "cardNumber" => $target_card, "token" => $token, "sourceCards" => [$source_card], "extEmpId" => $customer_id);
						$request = json_encode($request_params);
						$endurl = $this->endpoints['consolidate'];
					}
					$result = $this->sacoaApi($request, $endurl, "");
					$response = json_decode($result, 1);
					//print_r($response);
					if(array_key_exists("success", $response['body']) && $response['body']['success'] == 1){
						$update_user = lavu_query("UPDATE `user_cards` SET `status` = 2, `updated_date` = '".$created_datetime."', `updated_by` = '".$server_name."' WHERE `customer_id` = '".$customer_id."' AND `card_number` = '".$source_card."'");
						$insert_user = mlavu_query("INSERT INTO `poslavu_".$this->dataname."_db`.`user_cards`(`id`, `customer_id`, `card_number`, `status`, `is_deleted`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES(0, '".$customer_id."', '".$target_card."', 1, 0, '".$created_datetime."', '".$server_name."', '".$created_datetime."', '".$server_name."')");
					}
	 				$apiErrCodes = array("Token Expired", "Invalid Token");
	 				if(in_array($response['body'], $apiErrCodes) || $response['statusCode'] != 200){
	 					if($response['body']['errorCode']==503){
	 						$this->setErr($rtn_response,'SACOA_ERR_7');
	 					}else{
		 					$this->setErr($rtn_response,'SACOA_ERR_2');
							$rtn_response['flag'] = "0";
							$rtn_response['message'] = is_array($response['body']) ? $response['body']['msg'] : $response['body'];
							$rtn_response['txnid'] = "";
							$rtn_response['order_id'] = "";
	 					}
					}else{
						//  0 - success flag ("1")
						//  1 - [transaction_id]
						//  2 - [card_desc] (last_four)
						//  3 - response message
						//  4 - [auth_code]
						//  5 - [card_type]
						//  6 - [record_no]
						//  7 - [amount]
						//  8 - [order_id]
						//  9 - [ref_data]
						// 10 - [process_data]
						// 11 - new balance
						// 12 - bonus message
						// 13 - [first_four]
						// 14 - [last_mod_ts]
						$total_response = array($response['body']['card'], $response['body']['credits'], $response['body']['bonus'], $response['body']['courtesy'], $response['body']['minutes'], $response['body']['tickets']);
						$response_mesg = implode("-", $total_response);
						$rtn_response['flag'] = "1"; // 0
						$rtn_response['txnid'] = ""; // transaction ID
						$rtn_response['card_desc'] = substr($card_number, -4, 4);
						$rtn_response['message'] = $response_mesg;
						$rtn_response['auth_code'] = "";
						$rtn_response['card_type'] = "Sacoa Card";
						$rtn_response['record_no'] = "";
						$rtn_response['amount'] = $response['body']['credits'];
						$rtn_response['order_id'] = "";
						$rtn_response['ref_data'] = "";
						$rtn_response['process_data'] = "sacoa_card"; // 10
						$rtn_response['new_balance'] = "";
						$rtn_response['tickets'] = $response['body']['tickets'];
						$rtn_response['first_four'] = "";
						$rtn_response['last_mod_ts'] = strtotime($created_datetime);
					}
					//echo "<pre>";print_r($rtn_response);echo "</pre>";
					return $rtn_response;
				}else{
					$this->setErr($rtn_response,'SACOA_ERR_8');
					$rtn_response['flag'] = "0";
					$rtn_response['txnid'] = "";
					$rtn_response['order_id'] = "";
					return $rtn_response;
				}
			}else{
				$this->setErr($rtn_response,'SACOA_ERR_9');
				$rtn_response['flag'] = "0";
				$rtn_response['txnid'] = "";
				$rtn_response['order_id'] = "";
				return $rtn_response;
			}
		}
		
		function refundAmount($order_id, $transaction_id, $refund_amount, $server_name){
			$rtn_response = array();
			$get_card_number_query = mlavu_query("SELECT `card_desc` FROM `poslavu_".$this->dataname."_db`.`cc_transactions` WHERE `order_id` = '[1]' AND `process_data` = '[2]' AND `transaction_id` = '[3]'", $order_id, 'sacoa_card', $transaction_id);
			$get_card_number = mysqli_fetch_assoc($get_card_number_query);
			$card_number = $get_card_number['card_desc'];
			if($card_number){
				$validate_response = $this->validateCardNumber($card_number);
				if($validate_response == 1 && $this->cardInfo[$card_number]['status']){
					$check_token_expiry = mlavu_query("SELECT * FROM `gateway_auth_tokens` WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $this->dataname, $this->extensionObj->id);
						$data = mysqli_fetch_assoc($check_token_expiry);
					$extra_info = json_decode($data['extra_info'], 1);
					$token = $data['token'];
		
                               		//get-set conversion rate
                                	$this->setConversionRate();
                                	$this->credits=$this->conversionRate * $refund_amount;
			
					$request_params = array("posId" => 1, "empId" => $extra_info['empId'], "cardNumber" => $card_number, "token" => $token, "credits" => $this->credits, "bonus" => 0,
							"courtesy" => 0, "status" => "x", "payment" => 0, "minutes" => "", "deposit" => 0, "oldPassport" => 0 , "extEmpId" => $customer_id);
					$request = json_encode($request_params);
					$endurl = $this->endpoints['recharge'];
					$result = $this->sacoaApi($request, $endurl, "");
					$response = json_decode($result, 1);
					//print_r($response);
					$apiErrCodes = array("Token Expired", "Invalid Token");
					if(in_array($response['body'], $apiErrCodes) || $response['statusCode'] != 200){
						if($response['body']['errorCode']==503){
							$this->setErr($rtn_response,'SACOA_ERR_7');
						}else{
							$this->setErr($rtn_response,'SACOA_ERR_2');
							$rtn_response['flag'] = "0";
							$rtn_response['message'] = is_array($response['body']) ? $response['body']['msg'] : $response['body'];
							$rtn_response['txnid'] = "";
							$rtn_response['order_id'] = "";
						}
					}else{
						$created_datetime = date('Y-m-d H:i:s');
						//  0 - success flag ("1")
						//  1 - [transaction_id]
						//  2 - [card_desc] (last_four)
						//  3 - response message
						//  4 - [auth_code]
						//  5 - [card_type]
						//  6 - [record_no]
						//  7 - [amount]
						//  8 - [order_id]
						//  9 - [ref_data]
						// 10 - [process_data]
						// 11 - new balance
						// 12 - bonus message
						// 13 - [first_four]
						// 14 - [last_mod_ts]
						$rtn_response['flag'] = "1"; // 0
						$rtn_response['txnid'] = ""; // transaction ID
						$rtn_response['card_desc'] = substr($card_number, -4, 4);
						$rtn_response['message'] = $response['body']['card'];
						$rtn_response['auth_code'] = "";
						$rtn_response['card_type'] = "Sacoa Card";
						$rtn_response['record_no'] = "";
						$rtn_response['amount'] = $refund_amount;
						$rtn_response['order_id'] = $order_id;
						$rtn_response['ref_data'] = '{"conversion_rate":'.$this->conversionRate.'}';
						$rtn_response['process_data'] = "sacoa_card"; // 10
						$rtn_response['new_balance'] = $response['body']['credits'];
						$rtn_response['tickets'] = $response['body']['tickets'];
						$rtn_response['first_four'] = substr($card_number, 0, 4);
						$rtn_response['last_mod_ts'] = strtotime($created_datetime);
					}
					//echo "<pre>";print_r($rtn_response);echo "</pre>";
					return $rtn_response;
				}else{
					$this->setErr($rtn_response,'SACOA_ERR_3');
					$rtn_response['flag'] = "0";
					$rtn_response['txnid'] = "";
					$rtn_response['order_id'] = "";
					return $rtn_response;
				}
			}else{
				$this->setErr($rtn_response,'SACOA_ERR_10',array('val1'=>$order_id));
				$rtn_response['flag'] = "0";
				$rtn_response['txnid'] = "";
				$rtn_response['order_id'] = "";
				return $rtn_response;
			}
		}
		
		function ticketsRedemption($customer_id, $card_number, $amount){
			$rtn_response = array();
			$validate_response = $this->validateCardNumber($card_number);
			if($validate_response == 1 && $this->cardInfo[$card_number]['status']){
				$check_token_expiry = mlavu_query("SELECT * FROM `gateway_auth_tokens` WHERE `dataname` = '[1]' AND `extension_id` = '[2]'", $this->dataname, $this->extensionObj->id);
				$data = mysqli_fetch_assoc($check_token_expiry);
				$extra_info = json_decode($data['extra_info'], 1);
				$token = $data['token'];

				$request_params = array("posId" => 1, "empId" => $extra_info['empId'], "cardNumber" => $card_number, "token" => $token, "ticketAmount" => $amount, "extEmpId" => $customer_id);
				$request = json_encode($request_params);
				$endurl = $this->endpoints['redeem'];
				$result = $this->sacoaApi($request, $endurl, "");
				
				$response = json_decode($result, 1);
				$apiErrCodes = array("Token Expired", "Invalid Token");
				if(in_array($response['body'], $apiErrCodes) || $response['statusCode'] != 200){
					$rtn_response['flag'] = "0";
					$rtn_response['txnid'] = "";
					$rtn_response['order_id'] = "";
					if($response['body']['errorCode']==5){
						$this->setErr($rtn_response,'SACOA_ERR_11');
					}else if($response['body']['errorCode']==503){
						$this->setErr($rtn_response,'SACOA_ERR_7');
					}else{
						$this->setErr($rtn_response,'SACOA_ERR_2');
						$rtn_response['message'] = is_array($response['body']) ? $response['body']['msg'] : $response['body'];
					}
				}else{
					$created_datetime = date('Y-m-d H:i:s');
					//  0 - success flag ("1")
					//  1 - [transaction_id]
					//  2 - [card_desc] (last_four)
					//  3 - response message
					//  4 - [auth_code]
					//  5 - [card_type]
					//  6 - [record_no]
					//  7 - [amount]
					//  8 - [order_id]
					//  9 - [ref_data]
					// 10 - [process_data]
					// 11 - new balance
					// 12 - bonus message
					// 13 - [first_four]
					// 14 - [last_mod_ts]
					//$total_response = array($response['body']['card'], $response['body']['credits'], $response['body']['bonus'], $response['body']['courtesy'], $response['body']['minutes'], $response['body']['tickets']);
					//$response_mesg = implode("-", $total_response);
					$rtn_response['flag'] = "1"; // 0
					$rtn_response['txnid'] = $this->getTransactionId(); // transaction ID
					$rtn_response['card_desc'] = substr($card_number, -4, 4);
					//$rtn_response['message'] = $response_mesg;
					$rtn_response['message'] = "";
					$rtn_response['auth_code'] = "";
					$rtn_response['card_type'] = "Sacoa Card";
					$rtn_response['record_no'] = "";
					$rtn_response['amount'] = "";
					$rtn_response['order_id'] = "";
					$rtn_response['ref_data'] = "";
					$rtn_response['process_data'] = "sacoa_card"; // 10
					$rtn_response['new_balance'] = $response['body']['tickets'];
					$rtn_response['bonus_message'] = "";
					$rtn_response['first_four'] = substr($card_number, 0, 4);
					$rtn_response['last_mod_ts'] = strtotime($created_datetime);
				}
				//echo "<pre>";print_r($rtn_response);echo "</pre>";
				return $rtn_response;
			}else{
				$this->setErr($rtn_response,'SACOA_ERR_3');
				$rtn_response['flag'] = "0";
				$rtn_response['txnid'] = "";
				$rtn_response['order_id'] = "";
				return $rtn_response;
			}
		}
		
		function getTransactionId(){
			return bin2hex(openssl_random_pseudo_bytes(8));
		}
}
?>
