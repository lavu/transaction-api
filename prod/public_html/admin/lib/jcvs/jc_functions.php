<?php

	class fmg_placeholder {

		var $id;
		var $title;
		var $include_lists;
		var $_deleted;

		function __construct() {

			$this->id = "9999999";
			$this->title = "PLACEHOLDER_GROUP_EMPTY";
			$this->include_lists = "";
			$this->_deleted = 0;
		}
	}

	function run_rfix($order_id, $loc_id){
		ob_start();
		require_once(resource_path()."/rfix_functions.php");
		process_fix_order($order_id, $loc_id);
		ob_get_clean();
	}

	function surround_with_single_quote( $item ) {
		return "'{$item}'";
	}

	function testNewLSVPN($company_code, $company_code_key) {

		$lsvpn = curl_init("https://lsvpn.poslavu.com/hello.php?cc={$company_code}_key_{$company_code_key}");
		curl_setopt($lsvpn, CURLOPT_RETURNTRANSFER, "1");
		curl_setopt($lsvpn, CURLOPT_SSL_VERIFYPEER, false); // comment this line when certificate issues are resolved for poslavu.com
		$lsvpn_response = curl_exec($lsvpn);

		return $lsvpn_response == "Hello";
	}

	function netPathEmergencyOverride($data_obj) {

		// can change $data_obj->net_path and/or $data_obj->use_net_path

		/*if (($data_obj->net_path == "http://cloud1.poslavu.com") && $data_name!="DEV")
		{
			$data_obj->net_path = "https://admin.poslavu.com";
			$data_obj->use_net_path = "0";
		}*/

		return $data_obj;
	}

	function priceFormat($value) {

		global $decimal_places;

		return number_format((float)$value, $decimal_places, ".", "");
	}

	function determineDeviceTime($location_info, $post_vars) {

		$return_time = date("Y-m-d H:i:s");
		$tz_convert = true;

		if (isset($post_vars['device_time'])) {
			$datetime_parts = explode(" ", $post_vars['device_time']);
			$date_parts = explode("-", $datetime_parts[0]);
			$time_parts = explode(":", $datetime_parts[1]);
			$device_ts = mktime((int)$time_parts[0], (int)$time_parts[1], (int)$time_parts[2], (int)$date_parts[1], (int)$date_parts[2], (int)$date_parts[0]);
			/*if (($date_parts[0] - date("Y")) > 500) {
				$tz_convert = false;
				$return_time = $post_vars['device_time'];
			} else*/ if (abs(time() - $device_ts) < 2678400) {
				$tz_convert = false;
				$return_time = date("Y-m-d H:i:s", $device_ts);
			}
		}

		$timezone = (isset($location_info['timezone']))?$location_info['timezone']:"";
		if ($tz_convert && $timezone!="") $return_time = localize_datetime($return_time, $location_info['timezone']);

		return $return_time;
	}

	function getModifierUnitCost($mod_id, $mod_type) {

		$unit_cost = 0;
		if ($mod_type == "forced") $tablename = "forced_modifiers";
		else $tablename = "modifiers";

		$get_unit_cost = lavu_query("SELECT `cost` FROM `[1]` WHERE `id` = '[2]'", $tablename, $mod_id);
		if (mysqli_num_rows($get_unit_cost) > 0) {
			$mod_info = mysqli_fetch_assoc($get_unit_cost);
			$unit_cost = $mod_info['cost'];
		}

		return $unit_cost;
	}

	function getCheckCount($loc_id, $order_id) {

		$return_string = "1";
		$get_check_count = lavu_query("SELECT `no_of_checks` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id);
		if (mysqli_num_rows($get_check_count)) {
			$info = mysqli_fetch_assoc($get_check_count);
			$return_string = $info['no_of_checks'];
		}

		return $return_string;
	}

	function decimalize($number) {

		return (float)str_replace(",", ".", (string)$number);
	}

	function saveAlternatePaymentTotals($loc_id, $order_id, $ioid, $check, $payment_totals) {

		// added process_fill_alt_transaction_totals() to rfix_funcitons...

		/*$clear_previous = lavu_query("DELETE FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $loc_id, $order_id, $check);

		$payment_totals_array = explode("|*|", $payment_totals);
		foreach ($payment_totals_array as $total_info) {
			$total_info_array = explode("|o|", $total_info);
			$q_fields = "";
			$q_values = "";
			$p_vars = array();
			$p_vars['loc_id'] = $loc_id;
			$p_vars['order_id'] = $order_id;
			$p_vars['ioid'] = $ioid;
			$p_vars['check'] = $check;
			$p_vars['pay_type_id'] = $total_info_array[4];
			$p_vars['type'] = $total_info_array[0];
			$p_vars['amount'] = $total_info_array[1];
			$p_vars['tip'] = $total_info_array[2];
			$p_vars['refund_amount'] = $total_info_array[3];
			$keys = array_keys($p_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$record_it = lavu_query("INSERT INTO `alternate_payment_totals` ($q_fields) VALUES ($q_values)", $p_vars);
		}*/
	}

	function checkPaymentStatus() {

		global $location_info;
		global $company_info;

		$title = "";
		$message = "";
		$kickout = false;

		$get_status_info = lavu_query("SELECT `value` AS `due_ts`, `value3` AS `trial_status` FROM `config` WHERE `location` = '[1]' AND `type` = 'system_setting' AND `setting` = 'payment_status'", $location_info['id']);
		if (mysqli_num_rows($get_status_info) > 0) {
			$info = mysqli_fetch_assoc($get_status_info);
			$due_ts = $info['due_ts'];
			if ($due_ts!="") {

				$diff_days = floor(($due_ts - time()) / 60 / 60 / 24) + 1;
				$t_status = $info['trial_status'];
				if ($t_status=="2" || ($t_status=="1" && $diff_days<0)) $title = speak("Trial Expired");
				else if ($t_status=="1") $title = speak("Trial Due to Expire");
				else $title = speak("Billing Issue");

				if ($diff_days < 0) {
					$kickout = true;
					if ($t_status == "0" || empty($company_info['distro_code'])) $message = speak("Please resolve the billing issue for this account to resume use of POS Lavu.")." ".speak("Please call us at 855-767-5288.");
					else $message = speak("Please contact your specialist to resume use of POS Lavu.");
				} else if ($t_status == "1") {
					if ($diff_days > 1) $message = $diff_days. ' ' . speak("days left.");
					else if ($diff_days == 1) $message = speak("1 day left.");
					else if ($diff_days == 0) $message = speak("Today is the last day!");
				} else {
					if ($t_status == "0") $message = speak("Please call billing at 855-767-5288 to resolve this issue.");
					else $message = speak("Please contact your specialist to resolve this issue.");
					if ($diff_days > 1) $message .= " ".speak("We are extending the use of this account by ".$diff_days." days to provide time for resolution.");
					else if ($diff_days == 1) $message .= " ".speak("Only 1 day left to use POS Lavu without resolution.");
					else if ($diff_days == 0) $message .= " ".speak("Today is the last day to use this account without resolution.");
				}
			}
		}

		if ($message!="" && !$kickout) {
			$now_ts = time();
			if (sessvar("pay_status_warning_ts")) {
				$pswt = sessvar("pay_status_warning_ts");
				if (($now_ts - $pswt) < 28800) {
					$title = "";
					$message = "";
				}
			}
			if ($message != "") set_sessvar("pay_status_warning_ts", $now_ts);
		}

		/*global $data_name;

		if ($message != "") {
			error_log("checkPaymentStatus ($data_name): ".$title." ... ".$message." ... ".$kickout);
			$temp_log_type = $kickout?"payment_status_kickout":"payment_status_warning";
			$temp_log = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`temp_info` (`type`, `dataname`, `loc_id`, `datetime`, `info`) VALUES ('[1]', '[2]', '[3]', '[4]', '[5]')", $temp_log_type, $data_name, $location_info['id'], date("Y-m-d H:i:s"), "Mode ".$_REQUEST['m']." - ".$title." - ".$message);
		}*/

		return array("title"=>$title, "message"=>$message, "kickout"=>$kickout);
	}

	function loadLocations($loc_id, $data_name, &$menu_id) {

		$specific_location = $loc_id?" WHERE `id` = '[1]'":"";
		$addNewIntegrationFields = 0;
		$locations_array = array();

		$exclude = array("integration1", "integration2", "integration3", "integration4", "integration5", "integration6", "integration7", "integration8", "api_key", "api_token", "ml_pw");

		$refQuery = lavu_query("select * from poslavu_".$data_name."_db.locations where _disabled=0 limit 1");
		$row = mysqli_fetch_assoc($refQuery);

		if(isset($row['integration9'])){
			$addNewIntegrationFields++;
			array_push($exclude, 'integration9');
		}

		if(isset($row['integration10'])){
			$addNewIntegrationFields++;
			array_push($exclude, 'integration10');
		}

		$get_locations = lavu_query("SELECT ".create_field_list("locations", $exclude).", IF((AES_DECRYPT(`integration3`,'".integration_keystr()."') != '' AND AES_DECRYPT(`integration4`,'".integration_keystr()."') != ''), '1', '0') AS `integration3and4` FROM `locations`".$specific_location." ORDER BY `state`, `city`, `title` ASC", $loc_id);

		// Use this in case you need to load ml_pw in location settings: if (AES_DECRYPT(`ml_pw`, 'p2r_key') IS NULL), '', AES_DECRYPT(`ml_pw`, 'p2r_key')) as `ml_pw`

		if (mysqli_num_rows($get_locations) > 0) {
			$use_data = false;
			while ($data_obj = mysqli_fetch_object($get_locations)) {

				$pathArr = determineNetPath($data_obj);
				$data_obj->use_net_path = $pathArr['use_net_path'];
				$data_obj->net_path = $pathArr['net_path'];
				//error_log("Net path->" . $data_obj->net_path);

				$data_obj->use_direct_printing = "1";

				if ($data_obj->product_level=="1") $data_obj->product_level = "2";

				$integration_info = get_integration_from_location($data_obj->id, "poslavu_".$data_name."_db");
				$data_obj->int_1 = lc_encode($integration_info['integration1'], "jc");
				$data_obj->int_2 = lc_encode($integration_info['integration2'], "jc");
				$data_obj->int_3 = lc_encode($integration_info['integration3'], "jc");
				$data_obj->int_4 = lc_encode($integration_info['integration4'], "jc");
				$data_obj->int_5 = lc_encode($integration_info['integration5'], "jc");
				$data_obj->int_6 = lc_encode($integration_info['integration6'], "jc");
				$data_obj->int_7 = lc_encode($integration_info['integration7'], "jc");
				$data_obj->int_8 = lc_encode($integration_info['integration8'], "jc");

				if($addNewIntegrationFields==2){
					$data_obj->int_9 = lc_encode($integration_info['integration9'], "jc");
					$data_obj->int_10 = lc_encode($integration_info['integration10'], "jc");
				}

				$data_obj = netPathEmergencyOverride($data_obj);

				$locations_array[] = $data_obj;
				$use_data = $data_obj;
			}
			if ($use_data) $menu_id = $use_data->menu_id;
		} else {
			echo '{"json_status":"NoL"}';
			lavu_exit();
		}
		return $locations_array;
	} 

	function determineNetPath($location_and_data_obj){
		global $company_code;//Defined in json_connect
		global $company_code_key;//Defined in json_connect
		global $activeUserInfo;//Stored in db.users._use_tunnel, set at begining of mode2.
		$net_path = $location_and_data_obj->net_path; // have net_path and use_net_path default to values stored in database; sending a nil value to the app will result in a crash
		$use_net_path = $location_and_data_obj->use_net_path;
		$is_lavu_user = ($activeUserInfo['f_name']=="LavuAdmin" || strpos(strtolower($activeUserInfo['username']), "lavu")===0);
		$useTunnel = $activeUserInfo['_use_tunnel'];

		$lls = (in_array($use_net_path, array("1", "2")) && !strstr($net_path, "poslavu.com") && !strstr($net_path, "lavulite.com"));

		if ($useTunnel && $is_lavu_user)
		{
			//We lookup the ip proxy table
			$proxyTable;
			//We import the proxyIPTable map in a way that should never cause error of this running script.
			ob_start();
			include_once( dirname(__FILE__).'/redirect_tables.php' );
			$garbage = ob_get_clean();
			if(function_exists( 'getProxyIPTable' )){
				$proxyTable = getProxyIPTable();
//error_log("Loaded proxy table with count:  " . count($proxyTable));
			}else{
				$proxyTable = array("https://lsvpn.poslavu.com/", "http://www.poslavu.com/lsvpn/");
				error_log("Could not find proxy_ip_table, file does not exist or error loading it. Loading default lsvpns.");
			}

			if($useTunnel == 1){
//error_log("Using tunnel 1 " . $proxyTable[$useTunnel]);
				if(testNewLSVPN($company_code, $company_code_key)){
					$use_net_path = "1";
					$net_path = $proxyTable[1];
				}
				else{
					$use_net_path = "1";
					$net_path = $proxyTable[2];
				}
			}else if($useTunnel == 2){
//error_log("Using tunnel 2 ". $proxyTable[$useTunnel]);
				$use_net_path = "1";
				$net_path = $proxyTable[2];
			}else if($useTunnel > 2){
				if( !empty($proxyTable[$useTunnel]) ){
//error_log("Using tunnel " . $useTunnel . " " . $proxyTable[$useTunnel]);
					$use_net_path = "1";
					$net_path = $proxyTable[$useTunnel];
				}
			}
		}

		if (!empty($use_net_path)) $use_net_path = "1";

		//error_log('net_path::', $net_path);
		return array('net_path' => $net_path, 'use_net_path' => $use_net_path);
	}

	function loadWebExtensionInfo($web_extensions, $active_modules) {

		$rtn_str = "{}";

		if (count($web_extensions) > 0) {

			$get_web_extensions = mlavu_query("SELECT `id`, `title`, `c_ids`, `c_xs`, `c_ys`, `c_ws`, `c_hs`, `ipod_c_xs`, `ipod_c_ys`, `ipod_c_ws`, `ipod_c_hs`, `inapp_admin` FROM `poslavu_MAIN_db`.`component_layouts` WHERE `id` IN (".implode(",", $web_extensions).") AND `_deleted` = '0'");
			if (mysqli_num_rows($get_web_extensions) > 0) {
				$return_array = array();
				$cids = array();
				while ($info = mysqli_fetch_assoc($get_web_extensions)) {
					$return_array[$info['id']] = $info;
					$cids = array_merge($cids, explode(",", $info['c_ids']));
				}

				$components_array = array();
				$get_components = mlavu_query("SELECT `id`, `type`, `allow_scroll`, `name`, `filename` FROM `poslavu_MAIN_db`.`components` WHERE `id` IN (".implode(",", $cids).")");
				if (mysqli_num_rows($get_components) > 0) {
					while ($c_info = mysqli_fetch_assoc($get_components)) {
						$components_array[$c_info['id']] = $c_info;
					}
				}
				$return_array['components'] = $components_array;

				$rtn_str = json_encode($return_array);
			}
		}

		return $rtn_str;
	}

	function loadMenuData($menu_id, $loc_id, $for_lavu_togo) { // load all menu data and more; includes groups, categories, items, forced/optional modifiers, printer groups, quick pay buttons, tax rates, tax exemptions, etc...

		global $data_name;
		global $location_info;
		global $active_modules;

		$menu_categories_array = array();
		$menu_items_array = array();
		$menu_groups_array = array();
		$menu_items = "";

		$printer_groups_return_string = '{"ph1":"ignore",';
		$get_printer_groups = lavu_query("SELECT `id`, `value`, `value2`, `value3`, `value4`, `value5`, `value6` FROM `config` WHERE `type` = 'printer_group' AND `location` = '".$loc_id."'");
		if (mysqli_num_rows($get_printer_groups) > 0) {
			while ($extract = mysqli_fetch_assoc($get_printer_groups)) {
				$printer_groups_return_string .= '"pg_'.$extract['id'].'":"|'.$extract['value'].'|'.$extract['value2'].'|'.$extract['value3'].'|'.$extract['value4'].'|'.$extract['value5'].'|'.$extract['value6'].'|",';
			}
		}
		$printer_groups_return_string .= '"ph2":"ignore"}';

		$used_modifier_lists = "";

		$got_a_cateogory = FALSE;

		$active_field = "active";
		if ($for_lavu_togo == "1") $active_field = "ltg_display";

		$limit = "";
		$universal_order = "ASC";
		$lavu_lite = false;
		if (isset($post_vars['lavu_lite']) && $post_vars['lavu_lite']=="1") {
			$limit = " LIMIT 1";
			$universal_order = "DESC";
			$lavu_lite = true;
		}

		global $o_happyHoursGetCategoryOrItem;
		require_once(resource_path()."/../areas/happyhours/php/happyhours_getcategoryoritem.php");

		$get_menu_groups = lavu_query("SELECT * FROM `menu_groups` WHERE `menu_id` = '".$menu_id."' AND `_deleted` != '1' ORDER BY (`group_name` = 'Universal') ".$universal_order.", `_order` ASC".$limit);
		if (mysqli_num_rows($get_menu_groups) > 0) {
			while ($data_obj_MG = mysqli_fetch_object($get_menu_groups)) {
				$sort_type = $data_obj_MG->orderby;
				if($data_obj_MG->group_name == "Universal")
				{
					$menu_groups_array[] = $data_obj_MG;
				}
				if ($sort_type == "manual") $order_by = " ORDER BY `_order` ASC";
				else $order_by = " ORDER BY `name` ASC";
				$get_menu_categories = lavu_query("SELECT * FROM `menu_categories` WHERE `menu_id` = '".$menu_id."' AND `group_id` = '".$data_obj_MG->id."' AND `$active_field` = '1' AND `_deleted` != '1'".$order_by);
				if (mysqli_num_rows($get_menu_categories) > 0) {
					if($data_obj_MG->group_name != "Universal")
					{
						$menu_groups_array[] = $data_obj_MG;
					}
					$got_a_category = TRUE;
					while ($data_obj = mysqli_fetch_object($get_menu_categories)) {

						if ($data_obj->printer == "") $data_obj->print = "0";

						$image_path = "/mnt/poslavu-files/images/".$data_name."/categories/".$data_obj->image;
						if (!file_exists($image_path) || is_dir($image_path)) $data_obj->image = "";

						if ((!strstr($used_modifier_lists, "|".$data_obj->modifier_list_id."|")) && ($data_obj->modifier_list_id != 0)) $used_modifier_lists .= "|".$data_obj->modifier_list_id."|";

						$get_menu_items = lavu_query("SELECT * FROM `menu_items` WHERE `menu_id` = '".$menu_id."' AND `category_id` = '".$data_obj->id."' AND `$active_field` = '1' AND `_deleted`!='1'".$order_by);

						// get the menu items and happy hours
						$a_menu_items = array();
						if ($get_menu_items !== FALSE && mysqli_num_rows($get_menu_items) > 0) {
							while ($row = mysqli_fetch_assoc($get_menu_items))
								$a_menu_items[] = $row;
						}
						$item_hhs = $o_happyHoursGetCategoryOrItem->get_all_items_from_category_happyhours($data_obj->id, (array)$data_obj, $a_menu_items, TRUE);

						if (count($a_menu_items) > 0) {
							$menu_categories_array[] = $data_obj;
							for ($i = 0; $i < count($a_menu_items); $i++) {
								$data_obj2 = (object)$a_menu_items[$i];

								if (isset($data_obj2->name)) $data_obj2->name = str_pad($data_obj2->name, 2, " ", STR_PAD_RIGHT);

								if (($data_obj2->printer == "") || ($data_obj->printer == "" && $data_obj2->printer == "0")) $data_obj2->print = "0";

								$image_path = "/mnt/poslavu-files/images/".$data_name."/items/".$data_obj2->image;
								if (!file_exists($image_path) || is_dir($image_path)) $data_obj2->image = "";

								if (isset($item_hhs[$data_obj2->id])) $data_obj2->happyhour = $item_hhs[$data_obj2->id];
								else $data_obj2->happyhour = "";

								$menu_items_array[] = $data_obj2;
								if ((!strstr($used_modifier_lists, "|".$data_obj2->modifier_list_id."|")) && ($data_obj2->modifier_list_id != 0)) $used_modifier_lists .= "|".$data_obj2->modifier_list_id."|";
							}
						} else {
							//echo '{"json_status":"NoItems","for_category":"'.$data_obj->name.'"}';
							//lavu_exit();
						}
					}
				}
			}

		} else {

			echo '{"json_status":"NoGroups"}';
			lavu_exit();
		}

		if (!$got_a_category) {
			echo '{"json_status":"NoC"}';
			lavu_exit();
		}

		$forced_modifier_group_array = array();
		$forced_modifier_group_array[] = new fmg_placeholder();
		$get_forced_modifier_groups = lavu_query("SELECT * FROM `forced_modifier_groups` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id);
		if (mysqli_num_rows($get_forced_modifier_groups) > 0) {
			while ($data_obj = mysqli_fetch_object($get_forced_modifier_groups)) {
				$forced_modifier_group_array[] = $data_obj;
			}
		}

		$forced_modifier_list_array = array();
		$forced_modifier_list_array[] = '{"id":"99999999","title":"place_holder","type":"choice","modifiers":[{"id":"0","title":"MOD ERROR","cost":"0.00","detour":"0"}]}';
		$get_forced_modifier_lists = lavu_query("SELECT `id`, `title`, `type` FROM `forced_modifier_lists` WHERE `_deleted` != '1' AND (`menu_id` = '[1]' OR `menu_id` = '0')", $menu_id);
		if (mysqli_num_rows($get_forced_modifier_lists) > 0) {

			$lists_array = array();
			$get_forced_modifiers = lavu_query("SELECT `id`, `list_id`, `title`, `cost`, `detour`, `extra`, `extra2`, `extra3`, `extra4`, `extra5` FROM `forced_modifiers` WHERE `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC");
			while ($extract2 = mysqli_fetch_assoc($get_forced_modifiers)) {
				$la_key = "l".$extract2['list_id'];
				if (!isset($lists_array[$la_key])) $lists_array[$la_key] = array();
				$lists_array[$la_key][] = '{"id":"'.$extract2['id'].'","title":"'.str_replace('"', '\"', $extract2['title']).'","cost":"'.$extract2['cost'].'","detour":"'.$extract2['detour'].'","extra":"'.$extract2['extra'].'","extra2":"'.$extract2['extra2'].'","extra3":"'.$extract2['extra3'].'","extra4":"'.$extract2['extra4'].'","extra5":"'.$extract2['extra5'].'"}';
			}

			while ($extract = mysqli_fetch_assoc($get_forced_modifier_lists)) {
				$la_key = "l".$extract['id'];
				if (!isset($lists_array[$la_key])) $lists_array[$la_key] = array();
				$forced_modifier_list_array[] = '{"id":"'.$extract['id'].'","title":"'.str_replace('"', '\"', $extract['title']).'","type":"'.$extract['type'].'","modifiers":['.join(",", $lists_array[$la_key]).']}';
			}
		}
		$forced_modifier_list_return_string = '"forced_modifier_lists":['.join(",", $forced_modifier_list_array).']';

		$coded_modifier_list_array = array();
		if (!$lavu_lite && $used_modifier_lists!="") {
			$used_modifier_lists_array = explode("|", trim($used_modifier_lists, "|"));
			foreach ($used_modifier_lists_array as $ml) {
				$get_modifiers = lavu_query("SELECT `id`, `title`, `cost` FROM `modifiers` WHERE `category` = '".$ml."' AND `_deleted` != '1' ORDER BY `_order` ASC, `title` ASC");
				if (mysqli_num_rows($get_modifiers) > 0) {
					$modifiers_string = "";
					$count = 0;
					while ($extract = mysqli_fetch_assoc($get_modifiers)) {
						$modifiers_string .= $extract['id']."|o|".$extract['title']."|o|".$extract['cost'];
						$count++;
						if ($count < mysqli_num_rows($get_modifiers)) {
							$modifiers_string .= "|*|";
						}
					}
					$coded_modifier_list_array[] = '{"id":"'.$ml.'","modifiers":"'.str_replace('"', '\"', $modifiers_string).'"}';
				}
			}
		}
		$modifier_list_return_string = '"modifier_lists":['.join($coded_modifier_list_array, ",").']';

		$get_quick_pay_buttons = lavu_query("SELECT * FROM `quick_pay_buttons` WHERE `loc_id` = '".$loc_id."' LIMIT 1");
		if (mysqli_num_rows($get_quick_pay_buttons) > 0) {
			$qp_info = mysqli_fetch_assoc($get_quick_pay_buttons);
			$qp_titles = '["'.$qp_info['title1'].'","'.$qp_info['title2'].'","'.$qp_info['title3'].'","'.$qp_info['title4'].'"]';
			$qp_values = '["'.$qp_info['value1'].'","'.$qp_info['value2'].'","'.$qp_info['value3'].'","'.$qp_info['value4'].'"]';
		} else {
			$qp_titles = '["$1","$5","$10","$20"]';
			$qp_values = '["1","5","10","20"]';
		}

		$get_quick_tip_buttons = lavu_query("SELECT * FROM `quick_tip_buttons` WHERE `loc_id` = '".$loc_id."' LIMIT 1");
		if (mysqli_num_rows($get_quick_tip_buttons) > 0) {
			$qt_info = mysqli_fetch_assoc($get_quick_tip_buttons);
			$qt_calcs = '["'.$qt_info['calc1'].'","'.$qt_info['calc2'].'","'.$qt_info['calc3'].'"]';
		} else $qt_calcs = '["0.18","0.20","0.22"]';

		$payment_types_return_string = '{"id":"3","type":"Gift Certificate","special":"","get_info":"0","info_label":"","info_type":"","min_access":"1","no_auto_close":"0"}';
		if (!$lavu_lite) {
			$get_payment_types = lavu_query("SELECT `id`, `type`, `special`, `get_info`, `info_label`, `info_type`, `min_access`, `no_auto_close` FROM `payment_types` WHERE `loc_id` = '[1]' AND `_deleted` != '1' ORDER BY `_order` ASC", $loc_id);
			if (mysqli_num_rows($get_payment_types) > 0) {
				while ($ptype = mysqli_fetch_assoc($get_payment_types)) {
					$payment_types_return_string .= ',{"id":"'.$ptype['id'].'","type":"'.json_escape($ptype['type']).'","special":"'.$ptype['special'].'","get_info":"'.$ptype['get_info'].'","min_access":"'.$ptype['min_access'].'","no_auto_close":"'.$ptype['no_auto_close'].'","info_label":"'.$ptype['info_label'].'","info_type":"'.$ptype['info_type'].'"}';
					if (substr($ptype['special'], 0, 18) == "payment_extension:") {
						$pe_parts = explode(":", $ptype['special']);
						if (!empty($pe_parts[1])) $web_extensions[] = $pe_parts[1];
					}
				}
			}
		}

		$web_extensions_return_string = loadWebExtensionInfo($web_extensions, $active_modules);

		$cc_type_array = array();
		$check_config_cc_types = lavu_query("SELECT `value` AS `type` FROM `config` WHERE `location` = '[1]' AND `setting` = 'payment_methods_card' AND `_deleted` = '0' ORDER BY `value6` ASC", $loc_id);
		if (mysqli_num_rows($check_config_cc_types) > 0) {
			while ($info = mysqli_fetch_assoc($check_config_cc_types)) {
				$cc_type_array[] = $info;
			}
		} else {
			$get_cc_types = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`cc_types` ORDER BY `type` ASC");
			if (mysqli_num_rows($get_cc_types) > 0) {
				while ($data_obj = mysqli_fetch_object($get_cc_types)) {
					$cc_type_array[] = $data_obj;
				}
			}
		}
		$cc_type_return_string = json_encode($cc_type_array);

		$tax_rate_array = array();
		$get_tax_rates = lavu_query("SELECT * FROM `tax_rates` WHERE `loc_id` = '[1]' AND `_deleted` != '1' AND `profile_id` = '0'", $loc_id);
		if (mysqli_num_rows($get_tax_rates) > 0) {
			while ($data_obj = mysqli_fetch_object($get_tax_rates)) {
				$tax_rate_array[] = $data_obj;
			}
			$tax_rates_return_string = json_encode($tax_rate_array);
		} else $tax_rates_return_string = '[{"title":"Tax","calc":"'.$location_info['taxrate'].'","type":"p","stack":"y","tier_type1":"","tier_value1":"","force_tier1":"0","calc2":"","force_apply":""}]';

		$tax_profiles_array = array();
		$tax_profiles_array['X'] = array();
		$get_tax_profiles = lavu_query("SELECT `id` FROM `tax_profiles` WHERE `loc_id` = '[1]'", $loc_id);
		while ($info = mysqli_fetch_assoc($get_tax_profiles)) {
			$get_tax_rates = lavu_query("SELECT * FROM `tax_rates` WHERE `profile_id` = '[1]' AND `_deleted` = '0'", $info['id']);
			if (mysqli_num_rows($get_tax_rates) > 0) {
				$tax_array = array();
				while ($data_obj = mysqli_fetch_object($get_tax_rates)) {
					$tax_array[] = $data_obj;
				}
				$tax_profiles_array[(string)$info['id']] = $tax_array;
			}
		}

		$tax_exemption_array = array();
		$get_tax_exemptions = lavu_query("SELECT * FROM `tax_exemptions` WHERE `loc_id` = '".$loc_id."' AND `_deleted` != '1'");
		while ($data_obj = mysqli_fetch_object($get_tax_exemptions)) {
			$tax_exemption_array[] = $data_obj;
		}
		$tax_exemptions_return_string = json_encode($tax_exemption_array);

		$modifications = "";

		$happy_hours_array = array();
		/*$get_happy_hours = lavu_query("SELECT * FROM `happyhours` WHERE `_deleted` != '1'");
		while ($data_obj = mysqli_fetch_object($get_happy_hours)) {
			$happy_hours_array[$data_obj->id] = $data_obj;
		}*/
		$happy_hours_return_string = json_encode($happy_hours_array);

		//error_log("happyhours: ".print_r($happy_hours_array, true));
		//error_log("happyhours string: ".$happy_hours_return_string);

		$active_modules_return_string = json_encode($active_modules->getModList());

		$roles_return_string = "{}";
		$get_roles = lavu_query("SELECT `id`, `title` FROM `emp_classes` WHERE `_deleted` = '0'");
		if (mysqli_num_rows($get_roles) > 0) {
			$roles_array = array();
			while ($info = mysqli_fetch_assoc($get_roles)) {
				$roles_array[$info['id']] = $info['title'];
			}
			$roles_return_string = json_encode($roles_array);
		}

		$order_tags_return_string = "{}";
		$otoi_return_string = "[]";
		$get_order_tags = lavu_query("SELECT * FROM `order_tags`");
		if (mysqli_num_rows($get_order_tags) > 0) {
			$order_tags_array = array();
			$order_tags_ordered_ids = array();
			while ($info = mysqli_fetch_assoc($get_order_tags)) {
				$order_tags_array[$info['id']] = $info;
				$order_tags_ordered_ids[] = $info['id'];
			}
			$order_tags_return_string = json_encode($order_tags_array);
			$otoi_return_string = json_encode($order_tags_ordered_ids);
		}

		$table_setup_array = array();
		$get_table_setup = lavu_query("SELECT * FROM `tables` WHERE `loc_id` = '[1]' AND `_deleted` = '0' ORDER BY `_order`, `title` ASC", $_REQUEST['loc_id']);
		if (mysqli_num_rows($get_table_setup) > 0) {
			while ($data_obj = mysqli_fetch_object($get_table_setup)) {
				if ($data_obj->tables_json == NULL) $data_obj->tables_json = "";
				$table_setup_array[] = $data_obj;
			}
		} else {
			echo '{"json_status":"NoTS"}';
			lavu_exit();
		}
		$table_setup_string = json_encode($table_setup_array);

		$table_shapes_array = array();
		//$table_shapes_array['square'] = array("empty"=>"table_square.png","active"=>"table_square_hl.png","printed"=>"table_square_p.png"); // [state]_color can be used to set text color
		//$table_shapes_array['circle'] = array("empty"=>"table_circle.png","active"=>"table_circle_hl.png","printed"=>"table_circle_p.png");
		//$table_shapes_array['diamond'] = array("empty"=>"table_diamond.png","active"=>"table_diamond_hl.png","printed"=>"table_diamond_p.png");
		//$table_shapes_array['slant_left'] = array("empty"=>"table_slant_left.png","active"=>"table_slant_left_hl.png","printed"=>"table_slant_left_p.png");
		//$table_shapes_array['slant_right'] = array("empty"=>"table_slant_right.png","active"=>"table_slant_right_hl.png","printed"=>"table_slant_right_p.png");
		$table_shapes_array['square_new'] = array("empty"=>"table_square_new.png","active"=>"table_square_new_hl.png","printed"=>"table_square_new_p.png");
		$table_shapes_string = json_encode($table_shapes_array);

		$price_tier_profiles = array();
		if ($active_modules->hasModule("dining.price_tiers")) {
			$get_profiles = lavu_query("SELECT `id`, `value2` FROM `config` WHERE `location` = '[1]' AND `type` = 'price_tier_profile' AND `_deleted` = '0'", $_REQUEST['loc_id']);
			if (mysqli_num_rows($get_profiles) > 0) {
				while ($pinfo = mysqli_fetch_assoc($get_profiles)) {
					$get_tiers = lavu_query("SELECT `value` AS `min_value`, `value3` AS `max_value`, `value4` AS `type`, `value5` AS `op_value` FROM `config` WHERE `location` = '[1]' AND `type` = 'price_tier' AND `value2` = '[2]' AND `_deleted` = '0' ORDER BY (`value` * 1) ASC", $_REQUEST['loc_id'], $pinfo['id']);
					if (mysqli_num_rows($get_tiers) > 0) {
						$price_tier_profiles[$pinfo['id']] = array();
						while ($tinfo = mysqli_fetch_assoc($get_tiers)) {
							$tinfo['price_effect'] = $pinfo['value2'];
							$price_tier_profiles[$pinfo['id']][$tinfo['min_value']] = $tinfo;
						}
					}
				}
			}
		}
		$price_tier_profiles_string = json_encode($price_tier_profiles);

		return '"payment_types":['.$payment_types_return_string.'],"web_extensions":'.$web_extensions_return_string.',"cc_types":'.$cc_type_return_string.',"tax_rates":'.$tax_rates_return_string.',"tax_profiles":'.json_encode($tax_profiles_array).',"tax_exemptions":'.$tax_exemptions_return_string.',"qp_titles":'.$qp_titles.',"qp_values":'.$qp_values.',"qt_calcs":'.$qt_calcs.',"printer_groups":'.$printer_groups_return_string.',"forced_modifier_groups":'.json_encode($forced_modifier_group_array).','.$forced_modifier_list_return_string.',"modifications":"'.$modifications.'",'.$modifier_list_return_string.',"menu_categories":'.json_encode($menu_categories_array).',"menu_items":'.json_encode($menu_items_array).',"menu_groups":'.json_encode($menu_groups_array).',"happy_hours":'.$happy_hours_return_string.',"active_modules":'.$active_modules_return_string.',"roles":'.$roles_return_string.',"order_tags":'.$order_tags_return_string.',"order_tags_ordered_ids":'.$otoi_return_string.',"table_shapes":'.$table_shapes_string.',"table_setup":'.$table_setup_string.',"price_tier_profiles":'.$price_tier_profiles_string;
	}

	function loadComponents($comp_pack, $activeModules) { // load components associated with assigned spin-off package

		global $company_info;
		global $location_info;

		$layout_ids_string = "";
		$component_layouts_string = "";
		$components_string = "";
		$order_addons_string = "";
		$init_comp = array("name"=>"");

		$get_layout_ids = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_packages` WHERE `id` = '[1]'", $comp_pack);
		if (mysqli_num_rows($get_layout_ids) > 0) {
			$extract = mysqli_fetch_assoc($get_layout_ids);
			$layout_ids_array = explode(",", $extract['layout_ids']);
			$tab_ids = array();
			$order_addons = array();
			$use_order_addons = array();

			if(isset($extract['modules_required']) && !empty($extract['modules_required']) && !$activeModules->hasModule($extract['modules_required'])) {
				return "";
			}

			$add_ids = array();
			if (!empty($extract['order_addons'])) {
				$oa = explode("|*|", $extract['order_addons']);
				for ($i = 0; $i < count($oa); $i++) {
					$this_addon = explode("|o|", $oa[$i]);
					$should_exclude_addon = (isset($location_info['disable_order_add_on_'.$this_addon[0]]))?$location_info['disable_order_add_on_'.$this_addon[0]]:"0";
					if ($should_exclude_addon == "0") {
						$add_ids[] = $this_addon[0];
						if ($this_addon[0]=="19" || $this_addon[0]=="30") { // Customer AddOn or loyaltySwipe
							$this_addon[2] = (isset($location_info['require_customer_for_checkout']))?$location_info['require_customer_for_checkout']:$this_addon[2];
							$this_addon[3] = (isset($location_info['auto_launch_customer_selector']))?$location_info['auto_launch_customer_selector']:$this_addon[3];
							$order_addons[$this_addon[0]] = implode("|o|", $this_addon);
						}
					}
				}
				$layout_ids_array = array_merge($layout_ids_array, $add_ids);
			}

			if (!empty($extract['init_comp'])) {
				$get_init_comp = mlavu_query("SELECT `name`, `filename` FROM `components` WHERE `id` = '[1]'", $extract['init_comp']);
				if (mysqli_num_rows($get_init_comp)) {
					$init_comp = mysqli_fetch_assoc($get_init_comp);
				}
			}

			$component_layouts_array = array();
			$component_ids_array = array();
			$components_array = array();
			foreach ($layout_ids_array as $layout_id) {
				if ($layout_id == "0") $tab_ids[] = "0";
				else {
					$get_layout_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_layouts` WHERE `id` = '[1]'", $layout_id);
					$extract = mysqli_fetch_assoc($get_layout_data);
					if ($company_info['dev'] == "1") {
						foreach (array("background", "c_ids", "c_xs", "c_ys", "c_ws", "c_hs", "c_zs", "c_types") as $field) {
							if (!empty($extract['dev_'.$field])) $extract[$field] = $extract['dev_'.$field];
						}
					}
					$skip_this = (isset($extract['modules_required']) && !empty($extract['modules_required']) && !$activeModules->hasModule($extract['modules_required']));
					if (!$skip_this) {
						if (!in_array($layout_id, $add_ids)) $tab_ids[] = $layout_id;
						else $use_order_addons[] = $order_addons[$layout_id];
						$component_layouts_array[] = '{"id":"'.$layout_id.'","title":"'.$extract['title'].'","background":"'.$extract['background'].'","c_ids":"'.$extract['c_ids'].'","c_xs":"'.$extract['c_xs'].'","c_ys":"'.$extract['c_ys'].'","c_ws":"'.$extract['c_ws'].'","c_hs":"'.$extract['c_hs'].'","c_zs":"'.$extract['c_zs'].'","c_types":"'.$extract['c_types'].'","reload_behavior":"'.$extract['reload_behavior'].'","kiosk_mode":"'.$extract['kiosk_mode'].'"}';
						$these_component_ids = explode(",", $extract['c_ids']);
						foreach ($these_component_ids as $component_id) {
							if (!in_array($component_id, $component_ids_array)) {
								$component_ids_array[] = $component_id;
								$get_component_data = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`components` WHERE `id` = '[1]'", $component_id);
								$extract2 = mysqli_fetch_assoc($get_component_data);
								$components_array[] = '{"id":"'.$component_id.'","title":"'.$extract2['title'].'","type":"'.$extract2['type'].'","html":"'.$extract2['html'].'","allow_scroll":"'.$extract2['allow_scroll'].'","name":"'.$extract2['name'].'","filename":"'.$extract2['filename'].'"}';
							}
						}
					}
				}
			}

			$layout_ids_string = ',"layout_ids":"'.implode(",", $tab_ids).'"';
			$component_layouts_string = ',"component_layouts":['.join($component_layouts_array, ",").']';
			$components_string = ',"components":['.join($components_array, ",").']';
			$order_addons_string = ',"order_addons":'.json_encode($use_order_addons);
			$init_comp_string = ',"init_comp":'.json_encode($init_comp);

			return $layout_ids_string.$component_layouts_string.$components_string.$order_addons_string.$init_comp_string;

		} else {

			echo '{"json_status":"MissingCompPack"}';
			lavu_exit();
		}

		return "";
	}

	function shouldAllowTablesideCheckout($device_model) {

		if (strstr($device_model, "iPad")) return "0";
		else return "1"; // allow for iPods and iPhones
	}

	function fixForSmartPrinter($special_json) {

		$special_info = lavu_json_decode($special_json);
		if ($special_info[0]['model'] == "TM-T88V-i") $special_info[0]['lavu_controller'] = "1";

		return lavu_json_encode($special_info[0]);
	}

	function getSupportedPrinters($loc_id) { // other location settings from `config` table also piggy back here

		$app_build = (isset($_REQUEST['B']))?$_REQUEST['B']:"";

		$supported_printers_id_array = array();
		$csr_str = '{"no_settings":"none"}'; // config settings return string
		$terminal_list_return_string = '[{"receipt":"Receipt"}]';
		$kpr_str = '{';
		$get_config_settings = lavu_query("SELECT * FROM `config` WHERE `location` = '[1]' AND `_deleted` != '1' ORDER BY `setting` ASC", $loc_id);
		if (mysqli_num_rows($get_config_settings) > 0) {
			$csr_str = '{"allow_tableside_checkout":"'.shouldAllowTablesideCheckout($_REQUEST['model']).'"';
			$terminal_list_return_string = '[';
			while ($extract_cs = mysqli_fetch_assoc($get_config_settings)) {
				$type = $extract_cs['type'];
				if ($type == "printer") {
					if ($csr_str != "{") { $csr_str .= ","; }
					$p_name = strtolower($extract_cs['setting']);
					$csr_str .= '"'.$p_name.'-name":"'.json_escape($extract_cs['value2']).'",';
					$csr_str .= '"'.$p_name.'-ip":"'.$extract_cs['value3'].'",';
					$csr_str .= '"'.$p_name.'-port":"'.$extract_cs['value5'].'",';
					//$csr_str .= '"'.$p_name.'_drawer_code":"'.$extract_cs['value4'].'",';
					$csr_str .= '"'.$p_name.'_command_set":"'.$extract_cs['value6'].'",';
					$csr_str .= '"'.$p_name.'_cpl_standard":"'.$extract_cs['value7'].'",'; // characters per line for standard font
					$csr_str .= '"'.$p_name.'_cpl_emphasize":"'.$extract_cs['value8'].'",'; // characters per line for emphasize and double height fonts
					$csr_str .= '"'.$p_name.'_lsf":"'.$extract_cs['value9'].'",'; // line spacing factor
					$csr_str .= '"'.$p_name.'_raster_capable":"'.$extract_cs['value10'].'",'; // raster capable flag
					$csr_str .= '"'.$p_name.'_perform_status_check":"'.(strstr($extract_cs['setting'], "receipt")?"0":$extract_cs['value11']).'",'; // perform status check
					$csr_str .= '"'.$p_name.'_use_etb":"'.$extract_cs['value12'].'",'; // whether or not to use etb to tell if print is busy
					$csr_str .= '"'.$p_name.'_skip_ping":"'.$extract_cs['value13'].'",'; // whether or not to skip ping
					$csr_str .= '"'.$p_name.'_sleep_seconds":"'.$extract_cs['value14'].'",'; // time to wait between consecutive stream writes
					$csr_str .= '"'.$p_name.'_special_info":"'.json_escape(fixForSmartPrinter($extract_cs['value_long2'])).'",'; // specialized info about the printer
					$csr_str .= '"'.$p_name.'_print_individual_item_tickets":"'.$extract_cs['value_long'].'"'; // all items will be printed on individual tickets
					if (!in_array($extract_cs['value6'], $supported_printers_id_array)) {
						$supported_printers_id_array[] = $extract_cs['value6'];
					}
					if (substr($extract_cs['setting'], 0, 7) == "receipt") {
						if ($terminal_list_return_string != "[") { $terminal_list_return_string .= ","; }
						$terminal_list_return_string .= '{"setting":"'.$extract_cs['setting'].'","value":"'.$extract_cs['value2'].'"}';
					}
				} else if ($type == "printer_redirects") {
					if ($kpr_str != "{") $kpr_str .= ",";
					$sttng = $extract_cs['setting'];
					$vl = $extract_cs['value'];
					$kpr_str .= '"'.$extract_cs['id'].'":{"name":"'.json_escape($sttng).'","redirects":"'.json_escape($vl).'"}';
				} else if ($type=="location_config_setting" || $type=="admin_activity") {
					if ($csr_str != "{") $csr_str .= ",";
					$sttng = $extract_cs['setting'];
					$vl = $extract_cs['value'];

					if ($sttng=="dining_room_auto_refresh" && $vl>0) { // disable dining room refresh for single terminal locations
						$get_active_device_count = lavu_query("SELECT COUNT(`id`) FROM `devices` WHERE `loc_id` = '[1]' AND `inactive` = '0'", $loc_id);
						$dvc_count = mysqli_result($get_active_device_count, 0);
						if ($dvc_count <= 1) {
							global $data_name;
							error_log("disabling dining_room_auto_refresh (".$cs_val.") for ".$data_name);
							$vl = 0;
						}
					}

					$csr_str .= '"'.$sttng.'":"'.json_escape($vl).'"';
				}
			}
			$csr_str .= '}';
			$terminal_list_return_string .= ']';
		}
		$kpr_str .= '}';

		$mfilters = array();
		$mcount = 0;
		$spid_filter = "";
		foreach ($supported_printers_id_array as $spid) {
		  $mcount++;
		  $mfilters['p_'.$mcount] = $spid;
			$spid_filter .= " OR `id` = '[p_$mcount]'";
		}

		$supported_printers_return_string = '[{"no_printers":"none"}]';
		$printer_commands_array = array();
		$get_supported_printers = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`supported_printers` WHERE `id` = '0'".$spid_filter, $mfilters);
		if (mysqli_num_rows($get_supported_printers) > 0) {
			while ($extract_sp = mysqli_fetch_object($get_supported_printers)) {
				$printer_commands_array[] = $extract_sp;
			}
			$supported_printers_return_string = json_encode($printer_commands_array);
		}

		return '"supported_printers":'.$supported_printers_return_string.',"config_settings":'.$csr_str.',"terminal_list":'.$terminal_list_return_string.',"printer_redirects":'.$kpr_str;
	}

	function getAvailableLanguagePacks() {

		$language_pack_array = array();
		$get_language_packs = mlavu_query("SELECT `id`, `language` FROM `poslavu_MAIN_db`.`language_packs` ORDER BY `language` ASC");
		while ($lang_pack = mysqli_fetch_assoc($get_language_packs)) {
			$language_pack_array[] = '{"id":"'.$lang_pack['id'].'","language":"'.$lang_pack['language'].'"}';
		}

		return '['.join($language_pack_array, ",").']';
	}

	function generateLanguagePack($loc_id, $pack_id) {

		static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));

		$language_pack_string = '{';
		if ($pack_id == "1") {
			$language_pack_string .= '"STANDARD POSLAVU ENGLISH":"STANDARD POSLAVU ENGLISH"';
		} else if ($pack_id == "0") {
			$language_pack_string .= '"CUSTOM LANGUAGE PACK":"CUSTOM LANGUAGE PACK"';
			$get_custom_pack = lavu_query("SELECT * FROM `custom_dictionary` WHERE `loc_id` = '[1]' AND `translation` != ''", $loc_id);
			while ($extract = mysqli_fetch_assoc($get_custom_pack)) {
				$language_pack_string .= ',"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['word_or_phrase']).'":"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['translation']).'"';
			}
		} else {
			$get_language_name = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` = '[1]'", $pack_id);
			$extract = mysqli_fetch_assoc($get_language_name);
			$language_pack_string .= '"'.strtoupper($extract['language']).' LANGUAGE PACK":"'.strtoupper($extract['language']).' LANGUAGE PACK"';
			$get_language_pack = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '[1]' AND `translation` != '' AND (`used_by` = '' OR `used_by` = 'app')", $pack_id);
			while ($extract = mysqli_fetch_assoc($get_language_pack)) {
				$language_pack_string .= ',"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['word_or_phrase']).'":"'.str_replace($jsonReplaces[0], $jsonReplaces[1], $extract['translation']).'"';
			}
		}
		$language_pack_string .= '}';

		return $language_pack_string;
	}

	function get_column_names($table_name) {

		$result = lavu_query("SHOW COLUMNS FROM `$table_name`");
		$column_names = array();
		while ($row = mysqli_fetch_assoc($result)) {
			$column_names[] = $row['Field'];
		}

		return $column_names;
	}

	function create_field_list($table_name, $exclude) {

    $column_names = get_column_names($table_name);
    $field_list = "";

		foreach($column_names as $name) {
			if (!in_array($name, $exclude)) {
				if($field_list == "")
					$field_list = "`$name`";
				else
					$field_list .= ", `$name`";
				}
			}

    return $field_list;
	}

	function deviceHoldingThisOrder($loc_id, $order_id) {

		$device_id = false;

		if ($order_id!="" && $order_id!="0") {
			$check_devices = lavu_query("SELECT `UUID` FROM `devices` WHERE `loc_id` = '[1]' AND `holding_order_id` = '[2]' AND `inactive` = '0'", $loc_id, $order_id);
			if (mysqli_num_rows($check_devices) > 0) {
				$info = mysqli_fetch_assoc($check_devices);
				$device_id = $info['UUID'];
			}
		}

		return $device_id;
	}

	function setOrderHeldByDevice($loc_id, $order_id, $device_id) {

		if ($order_id == "0") return true;
		else {
			$update = lavu_query("UPDATE `devices` SET `holding_order_id` = '' WHERE `loc_id` = '[1]' AND `holding_order_id` = '[2]'", $loc_id, $order_id);
			if ($update) {
				$update = lavu_query("UPDATE `devices` SET `loc_id` = '[1]', `holding_order_id` = '[2]' WHERE `UUID` = '[3]'", $loc_id, $order_id, $device_id);
				return $update;
			} else return false;
		}
	}

	/*function recordItemCountMismatchAtCheckout($ipad_item_count, $db_item_count, $order_id) {

		global $data_name;

		$info = "iPad: ".$ipad_item_count." - DB: ".$db_item_count." - Order ID: ".$order_id;
		$record = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`temp_info` (`type`, `dataname`, `datetime`, `info`) VALUES ('item_count_mismatch_at_checkout', '[1]', '".date("Y-m-d H:i:s")."', '[2]')", $data_name, $info);

		if ($record) error_log("recordItemCountMismatchAtCheckout() recorded successfully");
		else error_log("!!! recordItemCountMismatchAtCheckout() failed to record !!!");
	}*/

	function build_item_vars($details, $insert=false) {

		global $decimal_places;

		// item_details indexes
		//
		//  0 - discount amount
		//  1 - discount value
		//  2 - discount type
		//  3 - after discount
		//  4 - tax amount
		//  5 - total with tax
		//  6 - included tax rate
		//  7 - included tax amount
		//  8 - tax rate 1
		//  9 - tax amount 1
		// 10 - tax rate 2
		// 11 - tax amount 2
		// 12 - tax rate 3
		// 13 - tax amount 3
		// 14 - tax subtotal 1
		// 15 - tax subtotal 2
		// 16 - tax subtotal 3
		// 17 - void
		// 18 - quantity
		// 19 - discount id
		// 20 - item discount id
		// 21 - item discount shorthand
		// 22 - item discount value
		// 23 - item discount type
		// 24 - item discount amount
		// 25 - split factor
		// 26 - tax name 1
		// 27 - tax name 2
		// 28 - tax name 3
		// 29 - tax exempt
		// 30 - exemption id
		// 31 - exemption name
		// 32 - included tax name
		// 33 - hidden data 6
		// 34 - options (forced modifiers)
		// 35 - idiscount info
		// 36 - icid
		// 37 - check
		// ---------- 2.2.1+ ----------
		// 38 - item
		// 39 - price
		// 40 - special
		// 41 - print
		// 42 - modifiy price
		// 43 - seat
		// 44 - item_id
		// 45 - printer
		// 46 - apply_taxrate
		// 47 - custom_taxrate
		// 48 - modifier_list_id
		// 49 - forced_modifier_group_id
		// 50 - forced_modifiers_price
		// 51 - course
		// 52 - open_item
		// 53 - allow_deposit
		// 54 - deposit_info
		// 55 - device_time
		// 56 - notes
		// 57 - hidden_data1
		// 58 - hidden_data2
		// 59 - hidden_data3
		// 60 - hidden_data4
		// 61 - tax_inclusion
		// 62 - sent
		// 63 - checked_out
		// 64 - hidden_data5
		// 65 - price_override
		// 66 - original_price
		// 67 - override_id
		// 68 - category_id
		// 69 - ioid
		// 70 - after_gratuity
		// ---------- 2.3.5+ ----------
		// 71 - tax_rate4
		// 72 - tax4
		// 73 - tax_subtotal4
		// 74 - tax_name4
		// 75 - tax_rate5
		// 76 - tax5
		// 77 - tax_subtotal5
		// 78 - tax_name5
		// 79 - tax_freeze
		// 80 - super_group_id

		$item_vars = array();
		$item_vars['discount_amount'] = priceFormat(decimalize($details[0]));
		$item_vars['discount_value'] = decimalize($details[1]);
		$item_vars['discount_type'] = $details[2];
		$item_vars['after_discount'] = decimalize($details[3]);
		$item_vars['tax_amount'] = priceFormat(max(0, decimalize($details[4])));
		$item_vars['total_with_tax'] = priceFormat(decimalize($details[5]));
		$item_vars['itax_rate'] = decimalize($details[6]);
		$item_vars['itax'] = priceFormat(decimalize($details[7]));
		$item_vars['tax_rate1'] = decimalize($details[8]);
		$item_vars['tax1'] = priceFormat(decimalize($details[9]));
		$item_vars['tax_rate2'] = decimalize($details[10]);
		$item_vars['tax2'] = priceFormat(decimalize($details[11]));
		$item_vars['tax_rate3'] = decimalize($details[12]);
		$item_vars['tax3'] = priceFormat(decimalize($details[13]));
		$item_vars['tax_subtotal1'] = priceFormat(decimalize($details[14]));
		$item_vars['tax_subtotal2'] = priceFormat(decimalize($details[15]));
		$item_vars['tax_subtotal3'] = priceFormat(decimalize($details[16]));
		$item_vars['void'] = decimalize($details[17]);
		$item_vars['quantity'] = decimalize($details[18]);
		$item_vars['discount_id'] = $details[19];
		$item_vars['idiscount_id'] = $details[20];
		$item_vars['idiscount_sh'] = $details[21];
		$item_vars['idiscount_value'] = decimalize($details[22]);
		$item_vars['idiscount_type'] = $details[23];
		$item_vars['idiscount_amount'] = priceFormat(decimalize($details[24]));
		$item_vars['split_factor'] = $details[25];
		$item_vars['tax_name1'] = $details[26];
		$item_vars['tax_name2'] = $details[27];
		$item_vars['tax_name3'] = $details[28];
		$item_vars['tax_exempt'] = $details[29];
		$item_vars['exemption_id'] = $details[30];
		$item_vars['exemption_name'] = $details[31];
		$item_vars['itax_name'] = $details[32];
		$item_vars['hidden_data6'] = (isset($details[33])?$details[33]:"");
		if (isset($details[34])) $item_vars['options'] = str_replace("RACER(colon)", "RACER:", $details[34]);
		$item_vars['idiscount_info'] = (isset($details[35])?$details[35]:"");
		if (isset($details[36])) $item_vars['icid'] = $details[36];
		if (isset($details[37])) $item_vars['check'] = $details[37];
		if (count($details) >= 71) {
			$item_vars['item'] = $details[38];
			$item_vars['price'] = $details[39];
			$item_vars['special'] = $details[40];
			$item_vars['print'] = $details[41];
			$item_vars['modify_price'] = decimalize($details[42]);
			$item_vars['seat'] = $details[43];
			$item_vars['item_id'] = $details[44];
			$item_vars['printer'] = $details[45];
			$item_vars['apply_taxrate'] = $details[46];
			$item_vars['custom_taxrate'] = $details[47];
			$item_vars['modifier_list_id'] = $details[48];
			$item_vars['forced_modifier_group_id'] = $details[49];
			$item_vars['forced_modifiers_price'] = decimalize($details[50]);
			$item_vars['course'] = $details[51];
			$item_vars['open_item'] = $details[52];
			$item_vars['subtotal'] = ($item_vars['quantity'] * $item_vars['price']);
			$item_vars['allow_deposit'] = $details[53];
			$item_vars['deposit_info'] = $details[54];
			$item_vars['device_time'] = $details[55];
			$item_vars['notes'] = $details[56];
			$item_vars['hidden_data1'] = $details[57];
			$item_vars['hidden_data2'] = $details[58];
			$item_vars['hidden_data3'] = $details[59];
			$item_vars['hidden_data4'] = $details[60];
			$item_vars['tax_inclusion'] = $details[61];
			$item_vars['sent'] = $details[62];
			if ($insert) $item_vars['checked_out'] = $details[63];
			$item_vars['hidden_data5'] = $details[64];
			$item_vars['price_override'] = $details[65];
			$item_vars['original_price'] = $details[66];
			$item_vars['override_id'] = $details[67];
			$item_vars['category_id'] = $details[68];
			$item_vars['ioid'] = $details[69];
			$item_vars['after_gratuity'] = decimalize($details[70]);
		}
		if (count($details) >= 79) {
			$item_vars['tax_rate4'] = decimalize($details[71]);
			$item_vars['tax4'] = decimalize($details[72]);
			$item_vars['tax_subtotal4'] = decimalize($details[73]);
			$item_vars['tax_name4'] = $details[74];
			$item_vars['tax_rate5'] = decimalize($details[75]);
			$item_vars['tax5'] = decimalize($details[76]);
			$item_vars['tax_subtotal5'] = decimalize($details[77]);
			$item_vars['tax_name5'] = $details[78];
		}

		return $item_vars;
	}

	function update_order_item_details($item_details, $order_id, $loc_id) {

		global $location_info;
		global $decimal_places;

		$item_details_array = explode("|*|", $item_details);
		$get_order_contents = lavu_query("SELECT `id` FROM `order_contents` WHERE `order_id` = '[1]' AND `loc_id` = '[2]' AND `item` != 'SENDPOINT'", $order_id, $loc_id);
		$row_count = mysqli_num_rows($get_order_contents);

		//if (count($item_details_array) != $row_count) recordItemCountMismatchAtCheckout(count($item_details_array), $row_count, $order_id);

		/**/

		$reset_last_mod_device = false;
		$used_icids = array();

		$row = 0;
		$can_perform_inserts = false;
		if ($row_count > 0) {
			while ($order_contents_info = mysqli_fetch_assoc($get_order_contents)) {
				if (isset($item_details_array[$row])) {
					$delimiter = ":";
					if (strstr($item_details_array[$row], "|o|")) $delimiter = "|o|";
					$details = explode($delimiter, $item_details_array[$row]);
					if (count($details) >= 70) $can_perform_inserts = true;
					$item_vars = build_item_vars($details, false);
					$use_icid = $item_vars['icid'];
					while (in_array($use_icid, $used_icids)) {
						$reset_last_mod_device = true;
						$use_icid = newInternalID();
					}
					$used_icids[] = $use_icid;
					$item_vars['icid'] = $use_icid;
					$keys = array_keys($item_vars);
					$q_update = "";
					foreach ($keys as $key) {
						if ($q_update != "") $q_update .= ", ";
						$q_update .= "`$key` = '[$key]'";
					}
					$item_vars['id'] = $order_contents_info['id'];
					$update_item = lavu_query("UPDATE `order_contents` SET $q_update WHERE `id` = '[id]'", $item_vars);
					$row++;
				}
			}
		}

		if ($can_perform_inserts) {
			for ($i = $row; $i < count($item_details_array); $i++) {
				$delimiter = ":";
				if (strstr($item_details_array[$i], "|o|")) $delimiter = "|o|";
				$details = explode($delimiter, $item_details_array[$i]);
				$item_vars = build_item_vars($details, true);
				$use_icid = $item_vars['icid'];
				while (in_array($use_icid, $used_icids)) {
					$reset_last_mod_device = true;
					$use_icid = newInternalID();
				}
				$used_icids[] = $use_icid;
				$item_vars['icid'] = $use_icid;
				$item_vars['loc_id'] = $loc_id;
				$item_vars['order_id'] = $order_id;
				$q_fields = "";
				$q_values = "";
				$keys = array_keys($item_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$insert = lavu_query("INSERT INTO `order_contents` ($q_fields) VALUES ($q_values)", $item_vars);
			}
		}

		if ($reset_last_mod_device) {

			$last_mod_ts = time();
			$last_modified = determineDeviceTime($location_info, $_REQUEST);

			$update_order = lavu_query("UPDATE `orders` SET `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'JC FUNCTIONS' WHERE `order_id` = '[3]'", $last_mod_ts, $last_modified, $order_id);
		}
	}

	function get_loyaltree_purchase_info($loc_id, $check, $order_id, $email=false, $num_checks){

		require_once(dirname(__FILE__).'/../../../inc/loyalty/loyaltree/LoyalTreeIntegration.php');

		global $company_info;
		global $location_info;
		global $data_name;

		$args = array(
			'action'       => 'purchase',
			'dataname'     => $data_name,
			'restaurantid' => $company_info['id'],
			'loc_id'       => $loc_id,
			'order_id'     => $order_id,
			'check'        => $check,
			'email'        => (($email === false) ? '' : $email),
			'app'          => (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP),
			'send_async'   => (isset($location_info['loyaltree_app']) && $location_info['loyaltree_app'] == LoyalTreeConfig::WHITELABEL_APP),
			'text'         => (isset($location_info['loyaltree_text']) && !empty($location_info['loyaltree_text'])) ? $location_info['loyaltree_text'] : '',
		);

		$split_check_details = lavu_query("SELECT `id`, `loyaltree_info` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $args);

		if (mysqli_num_rows($split_check_details))
		{
			$split_check_detail = mysqli_fetch_assoc($split_check_details);

			if (!empty($split_check_detail['loyaltree_info']))
			{
				return cleanJSON($split_check_detail['loyaltree_info']);
			}

			$response = LoyalTreeIntegration::purchase($args);

			if (!strstr($response, '"json_status":"error"'))
			{
				$args['scd_id'] = $split_check_detail['id'];
				$args['loyaltree_info'] = $response;
				$save_response = lavu_query("UPDATE `split_check_details` SET `loyaltree_info` = '[loyaltree_info]' WHERE `id` = '[scd_id]'", $args);
			}

			return cleanJSON($response);
		}
	}

	function cleanJSON($json_string) {
        
	/**To avoid POS crash issue due to null value in json string
        Replace <null> with ""
        Replace "" with \"\"
        **/
        if($json_string){
                $json_string = preg_replace("/\"\<null\>\"/", '""', $json_string);
                $json_string = preg_replace("/:NULL|:null/", ':""', $json_string);
                $json_string = str_replace('\":""', '\":\"\"', $json_string);
        }


       		/**To avoid POS crash issue due to null value in json string
       	 	Replace <null> with ""
        	Replace "" with \"\"
        	**/
        	if($json_string){
                	$json_string = preg_replace("/\"\<null\>\"/", '""', $json_string);
                	$json_string = preg_replace("/:NULL|:null/", ':""', $json_string);
                	$json_string = str_replace('\":""', '\":\"\"', $json_string);
        	}

		$search_array = array();
		$replace_array = array();
		for ($c = 0; $c < 32; $c++) {
			if ($c!=13) {
				$search_array[] = chr($c);
				$replace_array[] = "";
			}
			/*if ($c==10) {
				$search_array[] = chr($c);
				$replace_array[] = "[--CR--]";
			}*/
		}

		//return str_replace($search_array, $replace_array, $json_string);
		//return iconv("UTF-8", "UTF-8//IGNORE", str_replace($search_array, $replace_array, $json_string));

		$str = str_replace($search_array, $replace_array, $json_string);

		// seek out and remove invalid unicode sequences
		$str = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
			'|(?<=^|[\x00-\x7F])[\x80-\xBF]+'.
			'|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
			'|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
			'|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/',
			'', $str);

		$str = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
			'|\xED[\xA0-\xBF][\x80-\xBF]/S','', $str);

		return $str;
	}
	function closeOrder($post_vars) { // log order closure after checkout, performs inventory adjustments and special item

		// mode == 10

		global $data_name;
		global $location_info;
		global $decimal_places;
		global $smallest_money;

		if (!empty($post_vars['item_details'])) { // prevent auto close with missing items (when quantity set to '') - still unsure of cause of untimely call to mode 10 - RJG 8/30/2013

			$device_time = determineDeviceTime($location_info, $post_vars);

			// ***** close_info indexes *****
			//
			//  0 - closed time
			//  1 - discount
			//  2 - discount label
			//  3 - cash paid
			//  4 - card paid
			//  5 - alt paid
			//  6 - server name
			//  7 - tax amount
			//  8 - total
			//  9 - PIN used
			// 10 - tax rate // empty as of 4/09/2013
			// 11 - send to email
			// 12 - gratuity
			// 13 - cashier ID
			// 14 - paid by gift certificate
			// 15 - gratuity percent
			// 16 - refund amount
			// 17 - refund gift certificate amount
			// 18 - reopened datetime
			// 19 - subtotal
			// 20 - deposit status
			// 21 - active register
			// 22 - discount value
			// 23 - discount type
			// 24 - refund cc amount
			// 25 - rounded amount
			// 26 - auto gratuity is taxed
			// 27 - active register name
			// 28 - tax exempt
			// 29 - exemption id
			// 30 - exemption name
			// 31 - alt refund amount

			if ($post_vars['order_id'] == "0") return "0|invalid_order_id";

			$close_info = explode("|*|", $post_vars['close_info']);

			$auth_by = convertPIN($close_info[9], "name");
			$auth_by_id = convertPIN($close_info[9]);

			$order_info = array();
			$get_order_info = lavu_query("SELECT `closed`, `no_of_checks`, `checked_out`, `ioid`, `original_id` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $post_vars['loc_id'], $post_vars['order_id']);
			if (mysqli_num_rows($get_order_info) > 0) $order_info = mysqli_fetch_assoc($get_order_info);

			update_order_item_details(str_replace("RACER:", "RACER(colon)", $post_vars['item_details']), $post_vars['order_id'], $post_vars['loc_id']);

			$get_totals_from_contents = lavu_query("SELECT SUM(`itax`) AS `itax`, SUM(`idiscount_amount`) AS `idiscount_amount` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND (`quantity` * 1) > 0", $post_vars['loc_id'], $post_vars['order_id']);
			$total_data = mysqli_fetch_assoc($get_totals_from_contents);

			$now_ts = time();

			$q_update = "";
			$order_vars = array();
			$order_vars['subtotal'] = decimalize($close_info[19]);
			$order_vars['tax'] = decimalize($close_info[7]);
			$order_vars['total'] = priceFormat(decimalize($close_info[8]));
			if ($close_info[18]!="0000-00-00 00:00:00" && $close_info[18]!="") {
				$closed_action = "Order Reclosed";
				$order_vars['order_status'] = "reclosed";
				$order_vars['reclosed_datetime'] = $device_time;
				$order_vars['reclosing_device'] = determineDeviceIdentifier($post_vars);
				$order_vars['recloser'] = $close_info[6];
				$order_vars['recloser_id'] = $close_info[13];
				if ($order_info['closed']=="" || $order_info['closed']=="0000-00-00 00:00:00") $order_vars['closed'] = $device_time;
			} else {
				$closed_action = "Order Closed";
				$order_vars['order_status'] = "closed";
				$order_vars['closed'] = $device_time;
				$order_vars['closing_device'] = determineDeviceIdentifier($post_vars);
				$order_vars['cashier'] = $close_info[6];
				$order_vars['cashier_id'] = $close_info[13];
			}
			$order_vars['idiscount_amount'] = $total_data['idiscount_amount'];
			$order_vars['itax'] = $total_data['itax'];
			$order_vars['discount'] = decimalize($close_info[1]);
			$order_vars['discount_sh'] = $close_info[2];
			$order_vars['discount_value'] = decimalize($close_info[22]);
			$order_vars['discount_type'] = $close_info[23];
			$order_vars['gratuity'] = decimalize($close_info[12]);
			$order_vars['gratuity_percent'] = decimalize($close_info[15]);
			$order_vars['email'] = $close_info[11];
			$order_vars['register'] = $close_info[21];
			$order_vars['register_name'] = $close_info[27];
			$order_vars['auto_gratuity_is_taxed'] = $close_info[26];
			$order_vars['tax_exempt'] = $close_info[28];
			$order_vars['exemption_id'] = $close_info[29];
			$order_vars['exemption_name'] = $close_info[30];
			$order_vars['checked_out'] = "1";
			$order_vars['last_mod_ts'] = $now_ts;
			$order_vars['pushed_ts'] = $now_ts;
			$order_vars['last_modified'] = $device_time;
			$order_vars['last_mod_device'] = determineDeviceIdentifier($post_vars);
			$order_vars['last_mod_register_name'] = $close_info[27];
			$order_vars['active_device'] = "";
			$order_vars['original_id'] = (isset($_REQUEST['original_id']))?$_REQUEST['original_id']:"";
			$keys = array_keys($order_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$order_vars['location_id'] = $post_vars['loc_id'];
			$order_vars['order_id'] = $post_vars['order_id'];

			$update_closed = lavu_query("UPDATE `orders` SET $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $order_vars);

			$deposit_order_ids = array();
			$total_deposit_amount = 0;

			$poi_path = dirname(__FILE__)."/../../components/".$location_info['component_package_code']."/lib/process_order_item.php";
			if (($location_info['component_package_code'] != "") && file_exists($poi_path)) require_once($poi_path);

			$order_contents_by_check = array();
			for ($c = 1; $c <= (int)$order_info['no_of_checks']; $c++) {
				$order_contents_by_check[$c] = array();
			}

			$relative_row = 0;
			$newInventoryConfigured = false;
            $migrationStatus = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'INVENTORY_MIGRATION_STATUS'");
            $migrationResult = mysqli_fetch_assoc($migrationStatus);
            if($migrationResult['value'] != "Legacy"){
                $newInventoryConfigured = true;
            }
			$get_order_contents = lavu_query("SELECT `order_contents`.`total_with_tax` AS `total_with_tax`, `order_contents`.`check` AS `check`, `order_contents`.`idiscount_info` AS `idiscount_info`, `order_contents`.`id` AS `content_id`, `order_contents`.`icid` AS `icid`, `order_contents`.`checked_out` AS `checked_out`, `order_contents`.`item_id` AS item_id, `order_contents`.`subtotal_with_mods` as subtotal_with_mods, `order_contents`.`allow_deposit` as allow_deposit, `order_contents`.`deposit_info` as deposit_info, `order_contents`.`item` as item, `order_contents`.`options` as options, `order_contents`.`quantity` as quantity, `menu_items`.`hidden_value` as hidden_value, `menu_items`.`hidden_value2` as hidden_value2 FROM `order_contents` LEFT JOIN `menu_items` ON `menu_items`.`id` = `order_contents`.`item_id` WHERE `order_contents`.`loc_id` = '[1]' AND `order_contents`.`order_id` = '[2]'", $post_vars['loc_id'], $post_vars['order_id']);
			if (mysqli_num_rows($get_order_contents) > 0) {
				$should_update_deposit_status = 0;
				$a_icids = array();
				while ($extract = mysqli_fetch_assoc($get_order_contents)) {
					$a_icids[] = $extract['icid'];
					$relative_row++;
					if ($extract['item']!='SENDPOINT' && $extract['item_id']!='0') {

						if ($extract['check'] == "0") {
							foreach ($order_contents_by_check as $index => $ewh) {
								$order_contents_by_check[$index][] = $extract;
							}
						} else $order_contents_by_check[$extract['check']][] = $extract;

						$item_qty = decimalize($extract['quantity']);

						if ($extract['checked_out'] < $item_qty) {

							$set_checked_out = $item_qty;
							$item_id = $extract['item_id'];
							$item_qty -= $extract['checked_out'];
							$get_item_info = lavu_query("SELECT `ingredients` FROM `menu_items` WHERE `id` = '$item_id'");
							if (mysqli_num_rows($get_item_info) > 0) {
								$usage_counts = array();
								$item_info = mysqli_fetch_assoc($get_item_info);
								if ($item_info['ingredients'] != "") {
									$ing_list = explode(",", $item_info['ingredients']);
									for ($i = 0; $i < count($ing_list); $i++) {
										$ing_info = explode("x", $ing_list[$i]);
										$ing_id = trim($ing_info[0]);
										$ing_precision = 0;
										if ($ing_id > 0) {
											$ing_qty = 1;
											if (count($ing_info) > 1) { $ing_qty = trim($ing_info[1]); }
											if (in_array($ing_id, $usage_counts)) {
												$usage_counts[$ing_id] += ($ing_qty * $item_qty);
											} else {
												$usage_counts[$ing_id] = ($ing_qty * $item_qty);
											}
											$decimal_char = $location_info['decimal_char'];
											$decimal_char = ($decimal_char == '') ? '.' : $decimal_char;
											if (strpos((string)$ing_qty, $decimal_char) !== FALSE)
												$ing_precision = strlen(substr(strrchr((string)$ing_qty, $decimal_char), 1));
										}
									}
								}

								$get_mods_used = lavu_query("SELECT `mod_id`, `qty`, `type` FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `row` = '[3]'", $post_vars['loc_id'], $post_vars['order_id'], $relative_row);
								if (mysqli_num_rows($get_mods_used) > 0) {
									while ($mod_used = mysqli_fetch_assoc($get_mods_used)) {
										$mod_table = "forced_modifiers";
										if ($mod_used['type'] == "optional") { $mod_table = "modifiers"; }
										$get_mod_info = lavu_query("SELECT `ingredients` FROM `$mod_table` WHERE `id` = '[1]'", $mod_used['mod_id']);
										if (mysqli_num_rows($get_mod_info) > 0) {
											$mod_info = mysqli_fetch_assoc($get_mod_info);
											if ($mod_info['ingredients'] != "") {
												$m_ing_list = explode(",", $mod_info['ingredients']);
												for ($m = 0; $m < count($m_ing_list); $m++) {
													$m_ing_info = explode("x", $m_ing_list[$m]);
													$m_ing_id = trim($m_ing_info[0]);
													if ($m_ing_id > 0) {
														$m_ing_qty = 1;
														if (count($m_ing_info) > 1) { $m_ing_qty = trim($m_ing_info[1]); }
														if (in_array($m_ing_id, $usage_counts)) {
															$usage_counts[$m_ing_id] += ($m_ing_qty * $mod_used['qty']);
														} else {
															$usage_counts[$m_ing_id] = ($m_ing_qty * $mod_used['qty']);
														}
													}
												}
											}
										}
									}
								}

								$ing_ids = array_keys($usage_counts);
								foreach ($ing_ids as $ing_id) {
									$get_prior_usage = lavu_query("SELECT * FROM `ingredient_usage` WHERE `loc_id` = '[1]' AND `orderid` = '[2]' AND `itemid` = '[3]' AND `ingredientid` = '[4]'  AND (content_id = '[5]' OR (`icid` = '[6]' AND `icid` != ''))", $post_vars['loc_id'], $post_vars['order_id'], $item_id, $ing_id, $extract['content_id'], $extract['icid']);
									if ($get_prior_usage && mysqli_num_rows($get_prior_usage) > 0) {
										$usage_info = mysqli_fetch_assoc($get_prior_usage);
										$usage_qty = $usage_info['qty'];
										$usage_id = $usage_info['id'];
									} else  {
										$usage_qty = 0;
										$usage_id = false;
									}

									if($get_prior_usage){
										mysqli_free_result( $get_prior_usage );
									}

									$used_qty = ($usage_counts[$ing_id] - $usage_qty);
									if ($used_qty != 0 && !$newInventoryConfigured) {
										$s_rndstr_start = ($ing_precision > 0) ? "ROUND" : "";
										$s_rndstr_end = ($ing_precision > 0) ? ", $ing_precision" : "";
										lavu_query("UPDATE `ingredients` SET `qty` = {$s_rndstr_start}(`qty` - ($used_qty * 1){$s_rndstr_end}) WHERE `id` = '[1]'", $ing_id);
										if ($usage_id) {
											lavu_query("UPDATE `ingredient_usage` SET `qty` = {$s_rndstr_start}(`qty` + ($used_qty * 1){$s_rndstr_end}) where `id` = '[1]'", $usage_id);
										} else {
											$unit_cost = "";
											$get_unit_cost = lavu_query("SELECT `cost` FROM `ingredients` WHERE `id` = '[1]'", $ing_id);
											if (mysqli_num_rows($get_unit_cost) > 0) {
												$uc_info = mysqli_fetch_assoc($get_unit_cost);
												$unit_cost = $uc_info['cost'];
											}
											$q_fields = "";
											$q_values = "";
											$i_vars = array();
											$i_vars['ts'] = time();
											$i_vars['date'] = date("Y-m-d H:i:s");
											$i_vars['orderid'] = $post_vars['order_id'];
											$i_vars['itemid'] = $item_id;
											$i_vars['ingredientid'] = $ing_id;
											$i_vars['qty'] = $used_qty;
											$i_vars['loc_id'] = $post_vars['loc_id'];
											$i_vars['cost'] = $unit_cost;
											$i_vars['content_id'] = $extract['content_id'];
											$i_vars['icid'] = $extract['icid'];
											$keys = array_keys($i_vars);
											foreach ($keys as $key) {
												if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
												$q_fields .= "`$key`";
												$q_values .= "'[$key]'";
											}
											lavu_query("INSERT INTO `ingredient_usage` ({$q_fields}, `server_time`) VALUES ({$q_values}, now())", $i_vars);
										}
									}
								}

							}

							if (($location_info['component_package_code'] != "") && function_exists('process_order_item')) process_order_item($post_vars['loc_id'], $post_vars['order_id'], array_merge($extract, array("order_customer_id"=>$order_info['original_id'])), $close_info, $data_name);

						} else $set_checked_out = $extract['checked_out'];

						if ($extract['allow_deposit']=="1" || $close_info[20]=="4") {
							$should_update_deposit_status = 2;
							$total_deposit_amount = ($total_deposit_amount + $extract['total_with_tax']);
						} else if ($extract['allow_deposit'] == "1") $should_update_deposit_status = 3;

						$set_hidden_data = "";

						$update_item = lavu_query("UPDATE `order_contents` SET `checked_out` = '[1]'{$set_hidden_data} WHERE `id`  = '[2]'", $set_checked_out, $extract['content_id']);
					}
				}

				//Delete any extra ingredients that still appear in the table, but are no longer tied to the order.

				$a_icids = array_map( "surround_with_single_quote", $a_icids);
				$extra_ingredients = lavu_query("SELECT * FROM `ingredient_usage` WHERE `loc_id` = '[1]' AND `orderid` ='[2]' AND `itemid` = '[3]' AND `icid` NOT IN (" . implode(", ", $a_icids) . ")",$post_vars['loc_id'], $post_vars['order_id'], $item_id);
				{
					while ($ingredient_usage = mysqli_fetch_assoc( $extra_ingredients )) {
                        if (!$newInventoryConfigured) {
                            lavu_query("UPDATE `ingredients` SET `qty` = (`qty` + '[1]') WHERE `id` = '[2]'", $ingredient_usage['qty'], $ingredient_usage['ingredientid']);
                        }
                    }
				}
			}

			// apply appropriate LavuKart special event deposit status

			if ($should_update_deposit_status != 0) {
				$set_as_opened = "";
				$record_deposit_amount = "";
				if ($close_info[20] == "3") {
					$log_message = "Remaining payment received for Group Event";
					$should_update_deposit_status = 3;
					//foreach ($deposit_order_ids as $doi) {
						//$update_order = lavu_query("UPDATE `orders` SET `deposit_status` = '3' WHERE `order_id` = '".$doi."' AND `location_id` = '".$post_vars['loc_id']."'");
					//}
				} else {
					if ($should_update_deposit_status == 2) $log_message = "Deposit received for Group Event";
					else if ($should_update_deposit_status == 3) $log_message = "Full payment received for Group Event";
					$set_as_opened = ", `closed` = '0000-00-00 00:00:00'";
					$record_deposit_amount = ", `deposit_amount` = '".priceFormat($total_deposit_amount)."', `subtotal_without_deposit` = '".priceFormat($close_info[8] - $total_deposit_amount)."'";
					$closed_action = "Deposit paid";
				}

				$last_mod_ts = time();
				$last_modified = determineDeviceTime($location_info, $post_vars);

				$update_deposit_status = lavu_query("UPDATE `orders` SET `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'JC FUNCTIONS', `deposit_status` = '".$should_update_deposit_status."'".$set_as_opened.$record_deposit_amount." WHERE `order_id` = '[3]' AND location_id = '[4]'", $last_mod_ts, $last_modified, $post_vars['order_id'], $post_vars['loc_id']);

				if ($location_info['component_package_code'] == "lavukart")
				{
					$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$log_message."', '0', '".$post_vars['order_id']."', now(), '".$close_info[13]."', '0', '".$post_vars['loc_id']."')");
				}
			}

			$check_details_array = explode("|*|", $post_vars['check_details']);
			$receipt_string_array = explode("|*|", (isset($post_vars['receipts'])?$post_vars['receipts']:""));

			$subtotal_array = explode("|o|", $check_details_array[0]);
			$discount_array = explode("|o|", $check_details_array[1]);
			$discount_sh_array = explode("|o|", $check_details_array[2]);
			$tax_array = explode("|o|", $check_details_array[3]);
			$gratuity_array = explode("|o|", $check_details_array[4]);
			$cash_array = explode("|o|", $check_details_array[5]);
			$card_array = explode("|o|", $check_details_array[6]);
			$gift_certificate_array = explode("|o|", $check_details_array[7]);
			$refund_array = explode("|o|", $check_details_array[8]);
			$remaining_array = explode("|o|", $check_details_array[9]);
			$total_array = explode("|o|", $check_details_array[10]);
			$email_array = explode("|o|", $check_details_array[11]);
			$discount_ids_array = explode("|o|", $check_details_array[12]);
			$refund_gc_array = explode("|o|", $check_details_array[13]);
			$discount_values_array = explode("|o|", $check_details_array[14]);
			$discount_types_array = explode("|o|", $check_details_array[16]);
			$refund_cc_array = explode("|o|", $check_details_array[17]);
			$rounding_amounts_array = explode("|o|", $check_details_array[18]);
			$alt_paid_array = explode("|o|", $check_details_array[19]);
			$alt_refunded_array = explode("|o|", $check_details_array[20]);
			$rfid_flag_array = explode("|o|", $check_details_array[21]);
			$discount_infos_array = explode("|o|", $check_details_array[22]);
			$registers_array = explode("|o|", $check_details_array[23]);
			$register_names_array = explode("|o|", $check_details_array[24]);
			$loyalty_infos_array = explode("|o|", $check_details_array[25]);

			require_once(__DIR__."/../email_func.php");

			for ($i = 0; $i < count($subtotal_array); $i++) {

				$get_totals_from_contents = lavu_query("SELECT SUM(`itax`) AS `itax`, SUM(`idiscount_amount`) AS `idiscount_amount` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND (`quantity` * 1) > 0", $post_vars['loc_id'], $post_vars['order_id'], ($i + 1));
				$total_data = mysqli_fetch_assoc($get_totals_from_contents);

				$q_update = "";
				$q_fields = "";
				$q_values = "";
				$detail_vars = array();
				$detail_vars['subtotal'] = decimalize($subtotal_array[$i]);
				$detail_vars['itax'] = $total_data['itax'];
				$detail_vars['tax'] = decimalize($tax_array[$i]);
				$detail_vars['check_total'] = decimalize($total_array[$i]);
				$detail_vars['idiscount_amount'] = $total_data['idiscount_amount'];
				$detail_vars['discount'] = decimalize($discount_array[$i]);
				$detail_vars['discount_sh'] = $discount_sh_array[$i];
				$detail_vars['discount_value'] = decimalize($discount_values_array[$i]);
				$detail_vars['discount_type'] = $discount_types_array[$i];
				$detail_vars['discount_id'] = $discount_ids_array[$i];
				$detail_vars['discount_info'] = $discount_infos_array[$i];
				$detail_vars['gratuity'] = ((float)$detail_vars['check_total'] == 0)?"0":decimalize($gratuity_array[$i]);
				$detail_vars['gratuity_percent'] = decimalize($check_details_array[15]);
				$detail_vars['cash'] = decimalize($cash_array[$i]);
				if ($location_info['integrateCC'] != "1") $detail_vars['card'] = decimalize($card_array[$i]);
				$detail_vars['gift_certificate'] = decimalize($gift_certificate_array[$i]);
				$detail_vars['refund'] = decimalize($refund_array[$i]);
				$detail_vars['refund_cc'] = decimalize($refund_cc_array[$i]);
				$detail_vars['refund_gc'] = decimalize($refund_gc_array[$i]);
				$detail_vars['remaining'] = decimalize($remaining_array[$i]);
				$detail_vars['email'] = $email_array[$i];
				$detail_vars['change'] = 0;
				$detail_vars['cash_applied'] = decimalize($cash_array[$i]);
				$detail_vars['rounding_amount'] = decimalize($rounding_amounts_array[$i]);
				$detail_vars['alt_paid'] = decimalize($alt_paid_array[$i]);
				$detail_vars['alt_refunded'] = decimalize($alt_refunded_array[$i]);
				$detail_vars['checked_out'] = "1";
				if (isset($check_details_array[23])) $detail_vars['register'] = $registers_array[$i];
				if (isset($check_details_array[24])) $detail_vars['register_name'] = $register_names_array[$i];
				if (isset($check_details_array[25])) $detail_vars['loyalty_info'] = $loyalty_infos_array[$i];
				$keys = array_keys($detail_vars);
				foreach ($keys as $key) {
					if ($q_update != "") { $q_update .= ", "; }
					$q_update .= "`$key` = '[$key]'";
				}
				$detail_vars['loc_id'] = $post_vars['loc_id'];
				$detail_vars['order_id'] = $post_vars['order_id'];
				$detail_vars['check'] = ($i + 1);
				$keys = array_keys($detail_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}

				$previously_checked_out = "0";
				$check_for_details = lavu_query("SELECT `id`, `checked_out`, `loyaltree_info` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $post_vars['loc_id'], $post_vars['order_id'], $detail_vars['check']);
				if (mysqli_num_rows($check_for_details) > 0)  {
					$saved_info = mysqli_fetch_assoc($check_for_details);
					$previously_checked_out = $saved_info['checked_out'];
					$update_details = lavu_query("UPDATE `split_check_details` SET $q_update WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $detail_vars);
					$detail_vars['loyaltree_info'] = $saved_info['loyaltree_info'];
				} else 
					$create_details = lavu_query("INSERT INTO `split_check_details` ($q_fields) VALUES ($q_values)", $detail_vars);

				if (($rfid_flag_array[$i] == 1) && ($email_array[$i] != "") && isset($receipt_string_array[$i]) && ($receipt_string_array[$i] != ""))
					email_receipt($post_vars['loc_id'], $post_vars['order_id'], ($i + 1), $receipt_string_array[$i], $email_array[$i], $close_info[13], $close_info[6], count($subtotal_array));

				$order_vars['checked_out'] = $order_info['checked_out'];
				if(isset($location_info['enable_loyaltree']) && 1 == $location_info['enable_loyaltree'] && isset($location_info['loyaltree_id']) && isset($location_info['loyaltree_chain_id'])){
					//error_log("The detail vars are: ". print_r($order_info,1));
					get_loyaltree_purchase_info($post_vars['loc_id'], $detail_vars['check'], $post_vars['order_id'],false, $order_info['no_of_checks']);
					$check_for_details = lavu_query("SELECT `id`, `checked_out`, `loyaltree_info` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]'", $post_vars['loc_id'], $post_vars['order_id'], $detail_vars['check']);
					if (mysqli_num_rows($check_for_details) > 0)  {
						$saved_info = mysqli_fetch_assoc($check_for_details);
						$detail_vars['loyaltree_info'] = $saved_info['loyaltree_info'];
					}
				}
				processCheckAtCheckOut($detail_vars, array_merge($order_info, $order_vars), $location_info, $order_contents_by_check[($i + 1)]);
			}

			$clean_up_extra_details = lavu_query("DELETE FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` > '[3]'", $post_vars['loc_id'], $post_vars['order_id'], count($subtotal_array));

			$check_payments = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `order_id`	= '0' AND `ioid` = '[1]'", $order_info['ioid']);
			if (mysqli_num_rows($check_payments) > 0) {
				while ($info = mysqli_fetch_assoc($check_payments)) {
					$update_order_id = lavu_query("UPDATE `cc_transactions` SET `order_id` = '[1]' WHERE `id` = '[2]'", $post_vars['order_id'], $info['id']);
				}
			}

			$q_fields = "";
			$q_values = "";
			$l_vars = array();
			$l_vars['action'] = $closed_action;
			$l_vars['details'] =  "Total: ".$order_vars['total'];
			$l_vars['loc_id'] = $post_vars['loc_id'];
			$l_vars['order_id'] = $post_vars['order_id'];
			$l_vars['time'] = $close_info[0];
			$l_vars['user'] = $close_info[6];
			$l_vars['user_id'] = $close_info[13];
			$l_vars['device_udid'] = determineDeviceIdentifier($post_vars);
			$keys = array_keys($l_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

			run_rfix($post_vars['order_id'], $post_vars['loc_id']);

			return "1";

		} else {

			error_log("!!! empty item details detected during call to mode 10 - data_name: ".$data_name." !!!");
			return "0|Untimely auto-close has been prevented.|Empty Item Details";
		}
	}

	function processCheckAtCheckOut($check_info, $order_info, $location_info, $check_contents) {
		//babeerror_log("process check at checkout");
		global $hold_cc;
		global $data_name;
		global $company_info;

		$ci = $check_info;
		$oi = $order_info;
		$li = $location_info;

		if($li['enable_mercury_loyalty']){
			if ((substr($ci['loyalty_info'], 0, 15)=="mercury_loyalty" || substr($ci['discount_info'], 0, 15)=="mercury_loyalty") && empty($oi['checked_out'])) { // mercury loyalty add points / redeem coupon

				$loyalty_info = explode("|", $ci['loyalty_info']);
				$discount_info = explode("|", $ci['discount_info']);

				//error_log("count loyalty_info: ".count($loyalty_info));

				require_once(dirname(__FILE__)."/../gateway_lib/mercury_loyalty/comm/setup/mercury_loyalty.php");
				require_once(dirname(__FILE__)."/../gateway_lib/mercury_loyalty/comm/response/mercury_responsehandler.php");

				$mercuryFactory = new MLoyalty();
				if (isset($li['mercury_loyalty_api_key'])) $mercuryFactory->setAPIKey($li['mercury_loyalty_api_key']);
				if (isset($li['mercury_loyalty_api_secret'])) $mercuryFactory->setAPISecret($li['mercury_loyalty_api_secret']);

				$comm = $mercuryFactory->createGetProgramDescription();

				if(!$comm) 
					$error_message = "Unable to Load Mercury Loyalty at this Time.";
				else{
					$response = $comm->process();
					$responseHandler = new MResponseHandler();
					$result = $responseHandler->parse($response);
					if($result !== null){
						$programactive = $result->Options['isprogramactive'];
						if(!$programactive)
							$error_message = "The Specified Loyalty Program is not Currently Active.";
						else{
							$isLoyalty;
							if (count($loyalty_info)>=4 && $loyalty_info[0]=="mercury_loyalty"){ //Customer Loyalty Info / Add Credits
								$isLoyalty = true;
								$set_units = $ci['check_total'];
								if (strtolower($loyalty_info[4]) == "visit")
									$set_units = ($oi['guests'] / $oi['no_of_checks']);

								$comm = $mercuryFactory->createAddCredits();
								if (!$comm) $error_message = "failed to add points ";
								else {

									$comm->addVariable("customeridentifier", $loyalty_info[3]);
									//$comm->addVariable("TestMode", true); //For Testing (Doesn't Add to Total)
									$comm->addVariable("client", $li['title']);
									$comm->addVariable("description", "Food Stuffs");
									$comm->addVariable("employee_id", $oi['cashier_id']);
									$comm->addVariable("station_id", $ci['register_name']);
									$comm->addVariable("ticket_id", $ci['order_id']);
									$comm->addVariable("revenue", $ci['check_total']);
									$comm->addVariable("units", $set_units);

									$response = $comm->process();
									$responseHandler = new MResponseHandler();
									$result = $responseHandler->parse($response);
								}

								if(count($discount_info)>=3 && $discount_info[0]=="mercury_loyalty"){ //Redeem Coupon
									$comm = $mercuryFactory->createRedeemCoupon();
									if (!$comm) $error_message .= "failed to redeem coupon";
									else {

										$comm->addVariable("customeridentifier", $loyalty_info[3]);
										$comm->addVariable("code", $discount_info[2]);
										//$comm->addVariable("cardnumber", "0000");
										$comm->addVariable("deviceId", $ci['register_name']);
										$basket = new MSKUBasket();

										//$item = new MSKUBasketItem();
										//$basket->addItemToBasket($item);
										//$item->__set("Quantity","1");
										//$item->__set("SKU","Makin Bacon");
										//$item->__set("UnitPrice","10.0");

										$comm->addVariable(null, $basket, false);

										$response = $comm->process();
										$responseHandler = new MResponseHandler();
										$result = $responseHandler->parse($response);
									}
								}
							}

							//close transaction

						}
					}
					else{
					}
				}
			}
			if ((substr($ci['loyalty_info'], 0, 17)=="heartland_loyalty" || substr($ci['discount_info'], 0, 17)=="heartland_loyalty") && empty($oi['checked_out'])) { // mercury loyalty add points / redeem coupon
				$loyalty_info = explode("|", $ci['loyalty_info']);
				$discount_info = explode("|", $ci['discount_info']);
				//
				//
				$set_units = 0;
				if (count($loyalty_info)>=3 && $loyalty_info[0]=="heartland_loyalty"){ //Customer Loyalty Info / Add Credits
					$set_units = floor($ci['check_total']);
					if (strtolower($loyalty_info[4]) == "visit"){
						$set_units = floor($oi['guests'] / $oi['no_of_checks']);
					}
				}
				if(0 < $set_units){
					require_once(dirname(__FILE__)."/../gateway_lib/heartland_loyalty/heartland_add_points.php");
					heartlandAddPoints($li ,$loyalty_info,$discount_info,$set_units);
					error_log(__FILE__." ".__FUNCTION__." : heartlandAddPoints() : $set_units ".$_REQUEST['version']);
				}
			}
		}
		if($li['loyaltree_chain_id']==""){
			$li['loyaltree_chain_id']='0';
		}
		if ($li['enable_loyaltree']=="1" && ( ($li['loyaltree_username']!="" &&  $li['loyaltree_password']!="") || ($li['loyaltree_id']!="" && $li['loyaltree_chain_id']!="")) && empty($ci['loyaltree_info'])) {


			$dev_dir = ($company_info['dev'] == "1")?"/dev":"";
			$host = ($_SERVER['HTTP_HOST'] == "admin.poslavu.com")?"admin.poslavu.com":"localhost";
			$url = $host.$dev_dir."/lib/receipt_append_results.php?PHPSESSID=".session_id();

			//error_log($url);

			$vars = array();
			//error_log("about to set the mode email is ".$ci['email']); 

			//if(isset($ci['email']) && $ci['email']){
			//	$vars['m']='loyaltree_email_points';
			//	$vars['email']= $ci['email'];
			//}
			//else
				$vars['m'] = "loyaltree_qr_code";

			$vars['cc'] = $hold_cc;
			$vars['data_name'] = $data_name;
			$vars['version'] = $_REQUEST['version'];
			$vars['build'] = $_REQUEST['build'];
			$vars['loc_id'] = $li['id'];
			$vars['order_id'] = $oi['order_id'];
			$vars['check'] = $ci['check'];
			$vars['device_time'] = $_REQUEST['device_time'];
			$vars['prepare_image'] = "0";

			$var_string = "";
			foreach ($vars as $key => $val) {
				if ($var_string != "") $var_string .= "&";
				$var_string .= $key."=".rawurlencode($val);
			}

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $var_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close ($ch);
		}
		// Pepper Powered Lavu Loyalty
		if (!empty($location_info['pepper_enabled']) && !empty($order_info['original_id']) && substr($order_info['original_id'], 0, 4) == '112|') {
			require_once('/home/poslavu/public_html/inc/loyalty/pepper/PepperIntegration.php');

			// Get Pepper userId for checked-in user stored in orders.original_id by the webview
			$original_id = explode('|o|', $order_info['original_id']);
			$pepper_data = isset($original_id[4]) ? json_encode($original_id[4]) : array();
			$loyaltyInfo = isset($pepper_data['loyaltyInfo']) ? $pepper_data['loyaltyInfo'] : array();

			$pepper_args = array(
				'dataname'     => $data_name,
				'order_id'     => $order_info['order_id'],
				'check'        => (isset($check_info['check']) ? $check_info['check'] : '1'),
				'locationId'   => $location_info['pepper_locationid'],
				'userId'       => $original_id[3],
				'register'     => $order_info['register'],
				'registerId'   => $location_info['pepper_registerid'],
				'orderId'      => (isset($loyaltyInfo['orderId']) ? $loyaltyInfo['orderId'] : ''),  // Include orderId if there is already created an unfinalized Pepper order for this Lavu order from app|points payment methods during POS checkout
				'extract-data' => true,  // Re-query db for order data
				'update-order' => true,  // Finalize Pepper order
			);

			// Send Order to Pepper
			$results = PepperIntegration::sendOrder($pepper_args);

			// Save results in split_check_details
			$results_saved = lavu_query("UPDATE `split_check_details` SET `loyalty_info` = 'pepper_loyalty|[createdOrder]|[createdTransaction]|[updatedOrder]|[orderId]|[transactionId]' WHERE `order_id` = '[order_id]' AND `check` = '[check]' AND `loyalty_info` = ''", $results);
		}
	}
	function logSend($post_vars) { // update send_status for open order, save send point, log send

		// mode == 11

		global $location_info;

		$device_time = determineDeviceTime($location_info, $post_vars);

		// ***** order_info indexes *****
		//
		//  0 order id
		//  1 table
		//  2 server who opened order (name)
		//  3 order total
		//  4 time
		//  5 server who sent order (name)
		//  6 server who sent order (id)
		//  7 server who opened order (id)

		mlavu_close_db();

		$order_info = explode("|*|", $post_vars['order_info']);

		$logged_offline = (isset($post_vars['logged_offline']))?$post_vars['logged_offline']:"0";

		if ($logged_offline == "0") {

			$sending_course = (isset($post_vars['sending_course']))?$post_vars['sending_course']:"0";
			$max_course = (isset($post_vars['max_course']))?$post_vars['max_course']:"0";

			$now_ts = time();

			$q_array = array();
			if (($sending_course == 0) || ($sending_course >= $max_course)) $q_array[] = "`send_status` = '0'";
			$q_array[] = "`last_course_sent` = '[3]'";
			$q_array[] = "`last_mod_ts` = '".$now_ts."'";
			$q_array[] = "`pushed_ts` = '".$now_ts."'";
			$q_array[] = "`last_modified` = '".$device_time."'";
			$q_array[] = "`last_mod_device` = '".reqvar("UUID", "")."'";

			if (count($q_array) > 0) $update_send_status = lavu_query("UPDATE `orders` SET ".join(", ", $q_array)." WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $post_vars['loc_id'], $order_info[0], $sending_course);

			$filter = "";
			if ($sending_course != 0) $filter = " AND (`course` = '[3]'";
			if (!empty($_REQUEST['icids_sent'])) {
				$filter .= (empty($filter))?" AND (":" OR ";
				$filter .= "`icid` IN ('".str_replace(",","','",$_REQUEST['icids_sent'])."')";
			}
			if (!empty($filter)) $filter .= ")";

			$update_items_sent = lavu_query("UPDATE `order_contents` SET `sent` = '1' WHERE `loc_id` = '[1]' AND `order_id` = '[2]'".$filter, $post_vars['loc_id'], $order_info[0], $sending_course);
		}

		//deBug("...sending_course: ".$sending_course."\n\nmax_course: ".$max_course."\n\n".print_r($q_array, true));

		$ltg = "";
		if (isset($post_vars['lavu_togo'])) $ltg = "Lavu ToGo - ";

		$add_send_point = (isset($post_vars['add_send_point']))?$post_vars['add_send_point']:"1";
		if ($add_send_point=="1" || $logged_offline=="1") {

			$items_sent = (isset($post_vars['items_sent']))?$post_vars['items_sent']:"";

			$q_fields = "";
			$q_values = "";
			$sl_vars = array();
			$sl_vars['order_id'] = $order_info[0];
			$sl_vars['location'] = $location_info['title'];
			$sl_vars['location_id'] = $post_vars['loc_id'];
			$sl_vars['tablename'] = $order_info[1];
			$sl_vars['total'] = decimalize($order_info[3]);
			$sl_vars['server'] = $order_info[2];
			$sl_vars['server_id'] = $order_info[7];
			$sl_vars['send_server'] = $ltg.$order_info[5];
			$sl_vars['send_server_id'] = $order_info[6];
			$sl_vars['time_sent'] = $order_info[4];
			$sl_vars['device_udid'] = determineDeviceIdentifier($post_vars);
			$sl_vars['items_sent'] = $items_sent;
			$sl_vars['course'] = $sending_course;
			$perform_insert = true;
			if (isset($_POST['islid'])) {
				$sl_vars['islid'] = $_POST['islid'];
				$check_send_log = lavu_query("SELECT `id` FROM `send_log` WHERE `order_id` = '[order_id]' AND `islid` = '[islid]'", $sl_vars);
				if (mysqli_num_rows($check_send_log) > 0) $perform_insert = false;
			}
			if (isset($_POST['resend'])) $sl_vars['resend'] = $_POST['resend'];
			if (isset($_POST['redirection'])) $sl_vars['redirection'] = $_POST['redirection'];
			if ($perform_insert) {
				$keys = array_keys($sl_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$update_send_log = lavu_query("INSERT INTO `send_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $sl_vars);

				if ($logged_offline!="1" && appVersionCompareValue($_REQUEST['version'])<20309) {
					$q_fields = "";
					$q_values = "";
					$sp_vars = array();
					$sp_vars['loc_id'] = $post_vars['loc_id'];
					$sp_vars['order_id'] = $order_info[0];
					$sp_vars['item'] = "SENDPOINT";
					$sp_vars['price'] = "0";
					$sp_vars['quantity'] = $order_info[6];
					$sp_vars['options'] = $ltg.$order_info[5];
					$sp_vars['special'] = $order_info[4];
					$sp_vars['modify_price'] = "0";
					$sp_vars['print'] = "0";
					$sp_vars['check'] = "0";
					$sp_vars['seat'] = "0";
					$sp_vars['course'] = $sending_course;
					$sp_vars['item_id'] = "0";
					$sp_vars['printer'] = "0";
					$sp_vars['modifier_list_id'] = "0";
					$sp_vars['device_time'] = $order_info[4];
					$sp_vars['idiscount_info'] = "";
					$keys = array_keys($sp_vars);
					foreach ($keys as $key) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
					$insert_send_point = lavu_query("INSERT INTO `order_contents` ($q_fields, `server_time`) VALUES ($q_values, now())", $sp_vars);
				}

				$q_fields = "";
				$q_values = "";
				$l_vars = array();
				if ($sending_course == 0) $l_vars['action'] = "Order Sent to Kitchen";
				else {
					$l_vars['action'] = "Course Sent to Kitchen";
					$l_vars['details'] = $sending_course;
				}
				$l_vars['loc_id'] = $post_vars['loc_id'];
				$l_vars['order_id'] = $order_info[0];
				$l_vars['time'] = $order_info[4];
				$l_vars['user'] = $order_info[5];
				$l_vars['user_id'] = $order_info[6];
				$l_vars['device_udid'] = determineDeviceIdentifier($post_vars);
				$keys = array_keys($l_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

				// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
			}
		}

		return "1";
	}

	function voidOrder($post_vars) { // perform order void
		//error_log("in void order");
		//ini_set("display_errors",1);
		// mode == 12
		global $company_info;
		global $location_info;
		global $data_name;

		$device_time = determineDeviceTime($location_info, $post_vars);

		// ****** void_info indexes ******
		//
		//  0 - time
		//  1 - server name
		//  2 - PIN used
		//  3 - reopened datetime
		//  4 - reclosed datetime
		//  5 - void reason
		//  6 - register name

		mlavu_close_db();

		//error_log("the request data is: ". print_r($_REQUEST,1) );
		//error_log("the post vars are: ".print_r($post_vars,1));
		$void_info = explode("|*|", $post_vars['void_info']);
		//error_log(print_r($void_info,1));
		$auth_by = (!empty($post_vars['auth_by']))?$post_vars['auth_by']:convertPIN($post_vars['PINused'], "name");
		$auth_by_id = (!empty($post_vars['auth_by_id']))?$post_vars['auth_by_id']:convertPIN($post_vars['PINused']);

		$device_id = determineDeviceIdentifier($post_vars);

		$now_ts = time();

		$v_vars = array();
		$v_vars['cashier'] = $void_info[1];
		$v_vars['auth_by'] = $auth_by;
		$v_vars['auth_by_id'] = $auth_by_id;
		if (($void_info[3] == "") && ($void_info[4] == "")) {
			$v_vars['closed'] = $void_info[0];
			$v_vars['closing_device'] = $device_id;
		} else if (($void_info[3] != "") && ($void_info[4] == "")) {
			$v_vars['reclosed_datetime'] = $void_info[0];
			$v_vars['reclosing_device'] = $device_id;
		}
		$v_vars['order_status'] = "voided";
		$v_vars['void'] = "1";
		$v_vars['void_reason'] = $void_info[5];
		$v_vars['last_mod_ts'] = $now_ts;
		$v_vars['pushed_ts'] = $now_ts;
		$v_vars['last_modified'] = $device_time;
		$v_vars['last_mod_device'] = $device_id;
		$v_vars['last_mod_register_name'] = (isset($void_info[6]))?$void_info[6]:"";
		$v_vars['active_device'] = "";
		if (isset($void_info[7])) $v_vars['subtotal'] = $void_info[7];
		if (isset($void_info[8])) $v_vars['tax'] = $void_info[8];
		if (isset($void_info[9])) $v_vars['total'] = $void_info[9];

		$update = "";

		buildUpdate($v_vars, $update);

		$num_tries = 0;
		$find_info = false;
		$v_vars['location_id'] = $post_vars['loc_id'];
		$v_vars['order_id'] = $post_vars['order_id'];

		$record_void = lavu_query("UPDATE `orders` SET ".$update." WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $v_vars);
		$get_check_info = lavu_query("SELECT `discount_info`,`order_id`,`check` FROM `split_check_details` WHERE `loc_id` = '[location_id]' AND `order_id` = '[order_id]'", $v_vars);
		while ($check_info = mysqli_fetch_assoc($get_check_info)) {
			//error_log("check info is: ". print_r($check_info,1));
			//while( ($num_tries < 5) && !$find_info){
				if (substr($check_info['discount_info'], 0, 15) == "loyaltree|check") {
					///error_log(" in here found discount info ");
					$loyaltree_info = explode("|", $check_info['discount_info']);
					$args = array(
						'action'      => 'reinstate',
						'dataname'    => $data_name,
						'loc_id'      => $post_vars['loc_id'],
						'order_id'    => $post_vars['order_id'],
						'check'       => $check_info['check'],
						'server_id'   => $auth_by_id,
						'rid'         => $loyaltree_info[2],
						'isdeal'      => $loyaltree_info[3],
						'reason'      => 'Order voided',
						'no_echo'     => '1',
						'app'         => (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP),
					);
					$response = LoyalTreeIntegration::reinstate($args);
					$find_info=true;
				}
				//}else{
					//error_log("couldnt find discount info");
					//$num_tries++;
					//$find_info =false;
					//sleep(2);
				//}
			//}
		}

		$get_item_info = lavu_query("SELECT `idiscount_info`,`check` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $post_vars['loc_id'], $post_vars['order_id']);
		while ($item_info = mysqli_fetch_assoc($get_item_info)) {
			if (substr($item_info['idiscount_info'], 0, 17) == "loyaltree|lt_item") {
				$loyaltree_info = explode("|", $item_info['idiscount_info']);
				$args = array(
					'action'      => 'reinstate',
					'dataname'    => $data_name,
					'loc_id'      => $post_vars['loc_id'],
					'order_id'    => $post_vars['order_id'],
					'check'       => $check_info['check'],
					'server_id'   => $auth_by_id,
					'rid'         => $loyaltree_info[2],
					'isdeal'      => $loyaltree_info[3],
					'reason'      => 'Order voided',
					'no_echo'     => '1',
					'app'         => (isset($location_info['loyaltree_app']) ? $location_info['loyaltree_app'] : LoyalTreeConfig::BLUELABEL_APP),
				);
				error_log("about to do reinstate: js_functions 2239 data is ". print_r($args, true));
				$response = LoyalTreeIntegration::reinstate($args);
			}
		}

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = "Order Voided".(isset($void_info[9])?" - Total: ".priceFormat($void_info[9]):"");
		$log_vars['loc_id'] = $post_vars['loc_id'];
		$log_vars['order_id'] = $post_vars['order_id'];
		$log_vars['time'] = $void_info[0];
		$log_vars['user'] = $auth_by;
		$log_vars['user_id'] = $auth_by_id;
		$log_vars['device_udid'] = determineDeviceIdentifier($post_vars);
		$log_vars['details'] = $void_info[5];
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		return "1";
	}

	function logItemEdits($post_vars) { // log item edits after send

		// mode == 13

		global $location_info;

		// ****** item_info indexes ******
		//
		//  0 - note
		//  1 - order id
		//  2 - item name
		//  3 - options
		//  4 - special
		//  5 - quantity
		//  6 - time
		//  7 - server name
		//  8 - server ID
		//  9 - PIN used
		// 10 - modify_price

		mlavu_close_db();

		$item_info = explode("|*|", $post_vars['item_info']);

		$auth_by = (!empty($post_vars['auth_by']))?$post_vars['auth_by']:convertPIN($item_info[9], "name");

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['note'] = $item_info[0];
		$log_vars['order_id'] = $item_info[1];
		$log_vars['location'] = $location_info['title'];
		$log_vars['location_id'] = $post_vars['loc_id'];
		$log_vars['item'] = $item_info[2];
		$log_vars['options'] = $item_info[3];
		$log_vars['special'] = $item_info[4];
		$log_vars['modify_price'] = decimalize($item_info[10]);
		$log_vars['quantity'] = decimalize($item_info[5]);
		$log_vars['time'] = $item_info[6];
		$log_vars['server'] = $item_info[7];
		$log_vars['server_id'] = $item_info[8];
		$log_vars['auth_by'] = $auth_by;
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$update_log = lavu_query("INSERT INTO `edit_after_send_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		$type = (isset($post_vars['type']))?$post_vars['type']:"sent";

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = "Item changed after item $type - ".$item_info[0];
		$log_vars['loc_id'] = $post_vars['loc_id'];
		$log_vars['order_id'] = $item_info[1];
		$log_vars['time'] = $item_info[6];
		$log_vars['user'] = $item_info[7];
		$log_vars['user_id'] = $item_info[8];
		$log_vars['details'] = $item_info[5]." ".$item_info[2];
		$log_vars['device_udid'] = determineDeviceIdentifier($post_vars);
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		return "1";
	}

	function logOpenCashDrawer($post_vars)
	{
		// mode == 21

		global $location_info;

		$device_time = determineDeviceTime($location_info, $post_vars);

		mlavu_close_db();

		if (!isset($post_vars['do_not_log'])) {

			$auth_by = (!empty($post_vars['auth_by']))?$post_vars['auth_by']:convertPIN($post_vars['PINused'], "name");
			$auth_by_id = (!empty($post_vars['auth_by_id']))?$post_vars['auth_by_id']:convertPIN($post_vars['PINused']);

			$q_fields = "";
			$q_values = "";
			$log_vars = array();
			$log_vars['action'] = "Drawer Opened";
			$log_vars['loc_id'] = $post_vars['loc_id'];
			$log_vars['order_id'] = (isset($post_vars['order_id']))?$post_vars['order_id']:"0";
			$log_vars['time'] = $device_time;
			$log_vars['user'] = $auth_by;
			$log_vars['user_id'] = $auth_by_id;
			$log_vars['device_udid'] = determineDeviceIdentifier($post_vars);
			if (isset($post_vars['at_checkout'])) $log_vars['details'] = "At checkout";
			else if (isset($post_vars['details'])) $log_vars['details'] = $post_vars['details'];
			$keys = array_keys($log_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		}

		$use_direct_printing = (isset($post_vars['use_direct_printing']))?$post_vars['use_direct_printing']:"0";

		if ($use_direct_printing != "1") {
			$dd = "";
			if ($post_vars['dev_dir'] != "") $dd = "/..";
			$hloc = "..".$dd."/print/index.php?location=".str_replace(" ", "%20", $post_vars['location'])."&print=".urlencode("drawer#".$post_vars['register'].":open");
			//mail("corey@meyerwind.com","print",$hloc,"From:corey@poslavu.com");
			header("Location: $hloc");
			//header("Location: ..".$dd."/print/index.php?location=".str_replace(" ", "%20", $_REQUEST['location'])."&print=drawer:open");
		}

		return "1";
	}
	function logOrderChangeAfterCheckPrinted($post_vars) {

		// mode == 22

		mlavu_close_db();

		$change_info = explode("|*|", $post_vars['change_info']);

		$q_fields = "";
		$q_values = "";
		$log_vars = array();
		$log_vars['action'] = $change_info[0];
		$log_vars['loc_id'] = $post_vars['loc_id'];
		$log_vars['order_id'] = $change_info[1];
		$log_vars['time'] = $change_info[2];
		$log_vars['user'] = $change_info[3];
		$log_vars['user_id'] = $change_info[4];
		$log_vars['details'] = $change_info[5];
		$log_vars['device_udid'] = (!empty($change_info[6]))?$change_info[6]:determineDeviceIdentifier($post_vars);
		$keys = array_keys($log_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		return "1";
	}

	function evenlySplitValue($amount, $check, $check_count) {

		global $location_info;

		$decshift = pow(10, $location_info['disable_decimal']);
		$tmp_amt = floor(($amount * $decshift) / $check_count);
		$remainder = (($amount * $decshift) - ($tmp_amt * $check_count));
		if ($check > $remainder) $remainder = 0;
		else $remainder = 1;
		$amount = (($tmp_amt + $remainder) / $decshift);

		return $amount;
	}
	function roundMoney($amount) {

		global $location_info;

		$decshift = pow(10, $location_info['disable_decimal']);
		$tmp_amt = round($amount * $decshift);
		$amount = ($tmp_amt / $decshift);

		return $amount;
	}

	function saveOrder($post_vars)
	{
		// save order (info, contents, and modifier usage), combines modes 5 and 17, should replace them altogether in the future, echos order id and list of contents row ids
		// mode == 28

		//LP-9341
		unset($post_vars['ltg_dining_option']);
		unset($post_vars['ltg_dining_options']);


		global $location_info;
		global $server_order_prefix;

		$device_time = determineDeviceTime($location_info, $post_vars);

		// ***** order_info indexes *****
		//
		//  0 - order id
		//  1 - opened
		//  2 - closed
		//  3 - subtotal
		//  4 - taxrate // empty as of 4/09/2013
		//  5 - tax
		//  6 - total
		//  7 - server
		//  8 - tablename
		//  9 - send_status
		// 10 - guests
		// 11 - server id
		// 12 - number of checks
		// 13 - multiple tax rates
		// 14 - to tab or not to tab
		// 15 - deposit status
		// 16 - register
		// 17 - register name
		// 18 - tax exempt
		// 19 - exemption id
		// 20 - exemption name
		// 21 - alternate tablename
		// 22 - past tablename list
		// 23 - togo status
		// 24 - togo name
		// 25 - togo phone
		// 26 - customer email
		// 27 - togo time
		// 28 - tabname
		// 29 - internal order id (ioid)

		//ini_set('display_errors',1);
		//error_reporting(E_ALL|E_STRICT);

		mlavu_close_db();

		$loc_id				= $post_vars['loc_id'];
		$order_info			= explode("|*|", $post_vars['order_info']);
		$order_id			= $order_info[0];
		$internal_order_id	= (isset($order_info[29]))?$order_info[29]:"";

		$device_id = determineDeviceIdentifier($post_vars);

		$now_ts = time();

		$q_fields = "";
		$q_values = "";
		$q_update = "";
		$o_vars = array(
			'subtotal'					=> decimalize($order_info[3]),
			'tax'						=> decimalize($order_info[5]),
			'total'						=> decimalize($order_info[6]),
			'server'						=> $order_info[7],
			'tablename'					=> $order_info[8],
			'send_status'				=> $order_info[9],
			'guests'						=> $order_info[10],
			'server_id'					=> $order_info[11],
			'no_of_checks'				=> $order_info[12],
			'multiple_tax_rates'		=> $order_info[13],
			'tab'						=> $order_info[14],
			'deposit_status'			=> $order_info[15],
			'register'					=> $order_info[16],
			'register_name'				=> $order_info[17],
			'tax_exempt'					=> $order_info[18],
			'exemption_id'				=> $order_info[19],
			'exemption_name'			=> $order_info[20],
			'alt_tablename'				=> $order_info[21],
			'past_names'					=> (isset($order_info[22]))?$order_info[22]:"",
			'togo_status'				=> (isset($order_info[23]))?$order_info[23]:"",
			'togo_name'					=> (isset($order_info[24]))?$order_info[24]:"",
			'togo_phone'					=> (isset($order_info[25]))?$order_info[25]:"",
			'email'						=> (isset($order_info[26]))?$order_info[26]:"",
			'togo_time'					=> (isset($order_info[27]))?$order_info[27]:"",
			'last_mod_ts'				=> $now_ts,
			'pushed_ts'					=> $now_ts,
			'last_modified'				=> $device_time,
			'last_mod_device'			=> $device_id,
			'last_mod_register_name'	=> $order_info[17],
			'ioid'						=> $internal_order_id,
			'active_device'				=> (substr($device_id, 0, 4)=="216.")?"":$device_id,
			'original_id'				=> reqvar("original_id", "")
		);

		if (!empty($order_info[28]))
		{
			$o_vars['tabname']			= $order_info[28];
		}

		if (isset($post_vars['set_opened']))
		{
			$o_vars['opened']			= $post_vars['set_opened'];
		}

		if (isset($post_vars['set_closed']))
		{
			$o_vars['closed']			= $post_vars['set_closed'];
		}

		if (isset($post_vars['set_reopened']))
		{
			$o_vars['reopened_datetime']	= $post_vars['set_reopened'];
		}

		if (isset($post_vars['set_reclosed']))
		{
			$o_vars['reclosed_datetime']	= $post_vars['set_reclosed'];
		}

		if ($o_vars['togo_status'] == 1)
		{
			$o_vars['active_device']		= "";
		}

		if (isset($post_vars['set_void']))
		{
			$o_vars['void']				= $post_vars['set_void'];
		}

		if (isset($post_vars['set_void_reason']))
		{
			$o_vars['void_reason']		= $post_vars['set_void_reason'];
		}

		if (isset($post_vars['set_auth_by']))
		{
			$o_vars['auth_by']			= $post_vars['auth_by'];
		}

		if (isset($post_vars['set_auth_by_id']))
		{
			$o_vars['auth_by_id']		= $post_vars['auth_by_id'];
		}

		$order_status = determineOrderStatusForOrder($o_vars);
		if (!empty($order_status))
		{
			$o_vars['order_status']		= $order_status;
		}

		$update = "";

		buildUpdate($o_vars, $update);

		$o_vars['location']				= $location_info['title'];
		$o_vars['location_id']			= $loc_id;

		if (!isset($post_vars['set_closed']))
		{
			$o_vars['closed']			= $order_info[2];
		}

		$error_occurred = false;

		$force_held_order_save = (isset($post_vars['force_held_order_save']))?$post_vars['force_held_order_save']:"0";

		if ($order_id=="0" || $order_id=="")
		{
			$create_new_order_id = true;

			$ps_vars = array(
				'loc_id'		=> $loc_id,
				'opened'		=> $order_info[1],
				'server_id'	=> $order_info[11],
				'tablename'	=> $order_info[8],
				'ioid'		=> $internal_order_id
			);

			$ps_filter = "`opened` = '[opened]' AND `server_id` = '[server_id]' AND `tablename` = '[tablename]'";
			if (!empty($internal_order_id))
			{
				$ps_filter = "`ioid` = '[ioid]'";
			}

			$check_for_previous_save = lavu_query("SELECT `order_id` FROM `orders` WHERE `location_id` = '[loc_id]' AND ".$ps_filter."  LIMIT 1", $ps_vars);
			if (mysqli_num_rows($check_for_previous_save) > 0)
			{
				$previous_info = mysqli_fetch_assoc($check_for_previous_save);
				$order_id = $previous_info['order_id'];

				if ($order_id!="0" && $order_id!="")
				{
					$create_new_order_id = false;

					$holding_device = deviceHoldingThisOrder($_REQUEST['loc_id'], $order_id);

					if ($holding_device && $holding_device!=$device_id && $force_held_order_save!="1")
					{
						return "0:held_by:".str_replace(":", "[c", $holding_device);
					}
					else
					{
						$save_action = "Order Saved";
						$update_order = lavu_query("UPDATE `orders` SET `void` = '0', ".$update." WHERE `location_id` = '[location_id]' AND `order_id` = '$order_id'", $o_vars);
					}
				}
			}

			if ($create_new_order_id)
			{
				$save_action = "Order Opened";

				if (!isset($post_vars['set_opened']))
				{
					$o_vars['opened']		= $device_time;
				}
				$o_vars['order_status']		= "open";
				$o_vars['opening_device']	= $device_id;
				$o_vars['discount']			= "0";
				$o_vars['gratuity']			= "0";

				$fields = "";
				$values = "";

				buildInsertFieldsAndValues($o_vars, $fields, $values);

				$device_prefix = "";
				if (isset($location_info['use_device_prefix']) && $location_info['use_device_prefix']=="1")
				{
					$device_prefix = getDevicePrefix();
				}

				$order_prefix = $server_order_prefix.$device_prefix;

				$order_id = assignNextWithPrefix("order_id", "orders", "location_id", $loc_id, $order_prefix."-");
				$save_new_order = lavu_query("INSERT INTO `orders` (".$fields.", `order_id`) VALUES (".$values.", '".$order_id."')", $o_vars);
				if (!$save_new_order)
				{
					return "0";
				}

				$row_id = lavu_insert_id();

				usleep(rand(200,1000));
				$dupe_free = false;
				while (!$dupe_free)
				{
					$dupe_check = lavu_query("SELECT `id` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id);
					if (mysqli_num_rows($dupe_check) > 1)
					{
						usleep(rand(0,1000));
						$order_id = assignNextWithPrefix("order_id", "orders", "location_id", $loc_id, $order_prefix."-");
						$update_order_id = lavu_query("UPDATE `orders` SET `order_id` = '[1]' WHERE `id` = '[2]'", $order_id, $row_id);
					} 
					else
					{
						$dupe_free = true;
					}
				}
			}

			setOrderHeldByDevice($loc_id, $order_id, $device_id);
		}
		else
		{
			$o_vars['order_id'] = $order_info[0];
			$order_id = $order_info[0];

			$log_tablename_change = false;
			$get_previous_info = lavu_query("SELECT `tablename`, `ioid` FROM `orders` WHERE`location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);
			if (mysqli_num_rows($get_previous_info) > 0)
			{
				$previous_info = mysqli_fetch_assoc($get_previous_info);

				$holding_device = deviceHoldingThisOrder($_REQUEST['loc_id'], $order_id);

				if (empty($internal_order_id) && !empty($previous_info['ioid']))
				{
					$internal_order_id = $previous_info['ioid'];
				}

				if ($holding_device && $holding_device!=$device_id && $force_held_order_save!="1")
				{
					return "0:held_by:".str_replace(":", "[c", $holding_device);
				}
				else
				{
					if ($previous_info['tablename'] != $o_vars['tablename'])
					{
						$log_tablename_change = "Table name changed from ".$previous_info['tablename']." to ".$o_vars['tablename'];
					}

					$save_action = "Order Saved";

					$update_order = lavu_query("UPDATE `orders` SET `void` = '0', ".$update." WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $o_vars);

					if ($update_order)
					{
						$l_vars = array(
							'loc_id'		=> $loc_id,
							'order_id'		=> $order_id,
							'time'			=> $device_time,
							'server_time'	=> UTCDateTime(true),
							'user'			=> $order_info[7],
							'user_id'		=> $order_info[11],
							'device_udid'	=> $device_id
						);

						if ($log_tablename_change)
						{
							$fields = "";
							$values = "";

							buildInsertFieldsAndValues($l_vars, $fields, $values);

							$log_this = lavu_query("INSERT INTO `action_log` (".$fields.") VALUES (".$values.")", $l_vars);

							// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
						}
					}
					else
					{
						return "0";
					}
				}
			}
			else
			{
				$save_action = "Order Opened";

				$o_vars['order_status']		= "open";
				$o_vars['opening_device']	= $device_id;
				$o_vars['discount']			= "0";
				$o_vars['gratuity']			= "0";

				$fields = "";
				$values = "";

				buildInsertFieldsAndValues($o_vars, $fields, $values);

				$save_offline_order = lavu_query("INSERT INTO `orders` (".$fields.") VALUES (".$values.")", $o_vars);
				if (!$save_offline_order)
				{
					return "0";
				}
			}
		}

		$l_vars = array(
			'action'			=> $save_action,
			'details'		=> "Total: ".priceFormat($o_vars['total']),
			'loc_id'			=> $loc_id,
			'order_id'		=> $order_id,
			'time'			=> $device_time,
			'server_time'	=> UTCDateTime(true),
			'user'			=> reqvar("server", $order_info[7]),
			'user_id'		=> reqvar("server_id", $order_info[11]),
			'device_udid'	=> $device_id
		);

		$fields = "";
		$values = "";

		buildInsertFieldsAndValues($l_vars, $fields, $values);

		$log_this = lavu_query("INSERT INTO `action_log` (".$fields.") VALUES (".$values.")", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		$new_row_ids = array();
		$check_details = array();
		$icid_index = array();

		$tracked_86_items = (isset($_REQUEST['86_items']))?lavu_json_decode($_REQUEST['86_items']):array();
		if (count($tracked_86_items) == 1)
		{
			$tracked_86_items = $tracked_86_items[0];
		}

		$reset_last_mod_device = false;

		if (!empty($post_vars['order_contents']))
		{
			// ***** order_item indexes *****
			//
			//  0 - item
			//  1 - price
			//  2 - quantity
			//  3 - options
			//  4 - special
			//  5 - print
			//  6 - modify price
			//  7 - check number
			//  8 - seat number
			//  9 - item id
			// 10 - printer
			// 11 - apply taxrate
			// 12 - custom taxrate
			// 13 - modifier list id
			// 14 - forced modifier group id
			// 15 - forced modifiers price
			// 16 - course number
			// 17 - open item
			// 18 - allow deposit
			// 19 - deposit info
			// 20 - void flag
			// 21 - device time
			// 22 - notes
			// 23 - hidden data 1
			// 24 - hidden data 2
			// 25 - hidden data 3
			// 26 - hidden data 4
			// 27 - tax inclusion
			// 28 - sent
			// 29 - tax exempt
			// 30 - exemption id
			// 31 - exemption name
			// 32 - checked out
			// 33 - hidden data 5
			// 34 - price override
			// 35 - original price
			// 36 - override id
			// 37 - idiscount id
			// 38 - idiscount sh
			// 39 - idiscount value
			// 40 - idiscount type
			// 41 - idiscount amount
			// 42 - hidden data 6
			// 43 - idiscount info
			// 44 - category id
			// 45 - internal order id (ioid)
			// 46 - internal contents id (icid)
			// ---------- 2.2.1+ ----------
			// 47 - discount_amount
			// 48 - discount_value
			// 49 - discount_type
			// 50 - after_discount
			// 51 - tax_amount
			// 52 - total_with_tax
			// 53 - itax_rate
			// 54 - itax
			// 55 - tax_rate1
			// 56 - tax1
			// 57 - tax_rate2
			// 58 - tax2
			// 59 - tax_rate3
			// 60 - tax3
			// 61 - tax_subtotal1
			// 62 - tax_subtotal2
			// 63 - tax_subtotal3
			// 64 - discount_id
			// 65 - split_factor
			// 66 - tax_name1
			// 67 - tax_name2
			// 68 - tax_name3
			// 69 - itax_name
			// 70 - after_gratuity
			// ---------- 2.3.5+ ----------
			// 71 - tax_rate4
			// 72 - tax4
			// 73 - tax_subtotal4
			// 74 - tax_name4
			// 75 - tax_rate5
			// 76 - tax5
			// 77 - tax_subtotal5
			// 78 - tax_name5
			// 79 - tax_freeze
			// 80 - super_group_id

			$clear_current_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id);

			$order_contents = explode("|*|", $post_vars['order_contents']);

			$used_icids = array();

			foreach ($order_contents as $oc)
			{
				$order_item = explode("|o|", $oc);
				$item_id = $order_item[9];
				$q_fields = "";
				$q_values = "";
				$i_vars = array();
				$i_vars['loc_id'] = $loc_id;
				$i_vars['order_id'] = $order_id;
				$i_vars['item'] = $order_item[0];
				$i_vars['price'] = decimalize($order_item[1]);
				$i_vars['quantity'] = decimalize($order_item[2]);
				$i_vars['options'] = $order_item[3];
				$i_vars['special'] = $order_item[4];
				$i_vars['print'] = $order_item[5];
				$i_vars['modify_price'] = decimalize($order_item[6]);
				$i_vars['check'] = $order_item[7];
				$i_vars['seat'] = $order_item[8];
				$i_vars['item_id'] = $item_id;
				$i_vars['printer'] = $order_item[10];
				$i_vars['apply_taxrate'] = $order_item[11];
				$i_vars['custom_taxrate'] = $order_item[12];
				$i_vars['modifier_list_id'] = $order_item[13];
				$i_vars['forced_modifier_group_id'] = $order_item[14];
				$i_vars['forced_modifiers_price'] = decimalize($order_item[15]);
				$i_vars['course'] = $order_item[16];
				$i_vars['open_item'] = $order_item[17];
				$i_vars['subtotal'] = (decimalize($order_item[1]) * decimalize($order_item[2]));
				$i_vars['allow_deposit'] = $order_item[18];
				$i_vars['deposit_info'] = $order_item[19];
				$i_vars['subtotal_with_mods'] = (decimalize($order_item[2]) * (decimalize($order_item[1]) + decimalize($order_item[6]) + decimalize($order_item[15])));
				$i_vars['void'] = $order_item[20];
				$i_vars['device_time'] = $order_item[21];
				$i_vars['notes'] = $order_item[22];
				$i_vars['hidden_data1'] = $order_item[23];
				$i_vars['hidden_data2'] = $order_item[24];
				$i_vars['hidden_data3'] = $order_item[25];
				$i_vars['hidden_data4'] = $order_item[26];
				$i_vars['hidden_data5'] = (isset($order_item[33])?$order_item[33]:"");
				$i_vars['hidden_data6'] = (isset($order_item[42])?$order_item[42]:"");
				$i_vars['tax_inclusion'] = $order_item[27];
				$i_vars['sent'] = $order_item[28];
				$i_vars['tax_exempt'] = $order_item[29];
				$i_vars['exemption_id'] = $order_item[30];
				$i_vars['exemption_name'] = $order_item[31];
				$i_vars['checked_out'] = $order_item[32];
				$i_vars['price_override'] = (isset($order_item[34])?$order_item[34]:"0");
				$i_vars['original_price'] = (isset($order_item[35])?$order_item[35]:"");
				$i_vars['override_id'] = (isset($order_item[36])?$order_item[36]:"0");
				$i_vars['idiscount_id'] = (isset($order_item[37])?$order_item[37]:"");
				$i_vars['idiscount_sh'] = (isset($order_item[38])?$order_item[38]:"");
				$i_vars['idiscount_value'] = (isset($order_item[39])?decimalize($order_item[39]):"");
				$i_vars['idiscount_type'] = (isset($order_item[40])?$order_item[40]:"");
				$i_vars['idiscount_amount'] = (isset($order_item[41])?decimalize($order_item[41]):"");
				$i_vars['idiscount_info'] = (isset($order_item[43])?$order_item[43]:"");
				if (isset($order_item[44])) $i_vars['category_id'] = $order_item[44];
				$i_vars['ioid'] = (!empty($order_item[45])?$order_item[45]:$internal_order_id);
				$use_icid = (!empty($order_item[46])?$order_item[46]:newInternalID());
				while (in_array($use_icid, $used_icids)) {
					$reset_last_mod_device = true;
					$use_icid = newInternalID();
				}
				$used_icids[] = $use_icid;
				$i_vars['icid'] = $use_icid;
				$i_vars['server_time'] = UTCDateTime(true);

				if (isset($order_item[47])) $i_vars['discount_amount'] = decimalize($order_item[47]);
				if (isset($order_item[48])) $i_vars['discount_value'] = decimalize($order_item[48]);
				if (isset($order_item[49])) $i_vars['discount_type'] = $order_item[49];
				if (isset($order_item[50])) $i_vars['after_discount'] = decimalize($order_item[50]);
				if (isset($order_item[51])) $i_vars['tax_amount'] = decimalize($order_item[51]);
				if (isset($order_item[52])) $i_vars['total_with_tax'] = decimalize($order_item[52]);
				if (isset($order_item[53])) $i_vars['itax_rate'] = decimalize($order_item[53]);
				if (isset($order_item[54])) $i_vars['itax'] = decimalize($order_item[54]);
				if (isset($order_item[55])) $i_vars['tax_rate1'] = decimalize($order_item[55]);
				if (isset($order_item[56])) $i_vars['tax1'] = decimalize($order_item[56]);
				if (isset($order_item[57])) $i_vars['tax_rate2'] = decimalize($order_item[57]);
				if (isset($order_item[58])) $i_vars['tax2'] = decimalize($order_item[58]);
				if (isset($order_item[59])) $i_vars['tax_rate3'] = decimalize($order_item[59]);
				if (isset($order_item[60])) $i_vars['tax3'] = decimalize($order_item[60]);
				if (isset($order_item[61])) $i_vars['tax_subtotal1'] = decimalize($order_item[61]);
				if (isset($order_item[62])) $i_vars['tax_subtotal2'] = decimalize($order_item[62]);
				if (isset($order_item[63])) $i_vars['tax_subtotal3'] = decimalize($order_item[63]);
				if (isset($order_item[64])) $i_vars['discount_id'] = $order_item[64];
				if (isset($order_item[65])) $i_vars['split_factor'] = $order_item[65];
				if (isset($order_item[66])) $i_vars['tax_name1'] = $order_item[66];
				if (isset($order_item[67])) $i_vars['tax_name2'] = $order_item[67];
				if (isset($order_item[68])) $i_vars['tax_name3'] = $order_item[68];
				if (isset($order_item[69])) $i_vars['itax_name'] = $order_item[69];
				if (isset($order_item[70])) $i_vars['after_gratuity'] = decimalize($order_item[70]); // actually holds item gratuity value
				if (isset($order_item[71])) $i_vars['tax_rate4'] = decimalize($order_item[71]);
				if (isset($order_item[72])) $i_vars['tax4'] = decimalize($order_item[72]);
				if (isset($order_item[73])) $i_vars['tax_subtotal4'] = decimalize($order_item[73]);
				if (isset($order_item[74])) $i_vars['tax_name4'] = $order_item[74];
				if (isset($order_item[75])) $i_vars['tax_rate5'] = decimalize($order_item[75]);
				if (isset($order_item[76])) $i_vars['tax5'] = decimalize($order_item[76]);
				if (isset($order_item[77])) $i_vars['tax_subtotal5'] = decimalize($order_item[77]);
				if (isset($order_item[78])) $i_vars['tax_name5'] = $order_item[78];

				$fields = "";
				$values = "";

				buildInsertFieldsAndValues($i_vars, $fields, $values);

				$save_order_item = lavu_query("INSERT INTO `order_contents` (".$fields.") VALUES (".$values.")", $i_vars);
				if (!$save_order_item)
				{
					$error_occurred = true;
				}
				$new_row_ids[] = lavu_insert_id();

				$cstart = $i_vars['check'];
				$cend = $i_vars['check'];
				if ($cstart == 0)
				{
					$cstart = 1;
					$cend = $o_vars['no_of_checks'];
				}

				for ($c = $cstart; $c <= $cend; $c++)
				{
					$this_check_details = (isset($check_details[$c]))?$check_details[$c]:array("subtotal"=>0,"tax"=>0,"discount"=>0,"gratuity"=>0);
					$add_subtotal = $i_vars['subtotal_with_mods'];
					$add_tax = 0;
					for ($t = 1; $t <= 5; $t++) {
						$add_tax += $i_vars['tax_rate'.$t] * $i_vars['tax_subtotal'.$t];
					}
					$add_discount = $i_vars['discount_amount'];
					$add_gratuity = $i_vars['after_gratuity']; // after_gratuity is a misnomer, this value is actually the gratuity value associated with the item
					if ($i_vars['check'] == "0") {
						$add_subtotal = evenlySplitValue($add_subtotal, $c, $cend);
						$add_tax = evenlySplitValue($add_tax, $c, $cend);
						$add_discount = evenlySplitValue($add_discount, $c, $cend);
						$add_gratuity = evenlySplitValue($add_gratuity, $c, $cend);
					}
					$this_check_details['subtotal'] += $add_subtotal;
					$this_check_details['tax'] += $add_tax;
					$this_check_details['discount'] += $add_discount;
					$this_check_details['gratuity'] += $add_gratuity;
					$check_details[$c] = $this_check_details;
				}

				$icid_index[] = $i_vars['icid'];

				if (isset($tracked_86_items[$item_id]) && !$error_occurred)
				{
					$update_inv_count = lavu_query("UPDATE `menu_items` SET `inv_count` = (`inv_count` - ([1] * 1)) WHERE `id` = '[2]'", $tracked_86_items[$item_id], $item_id);
					$clear_temp_count = lavu_query("UPDATE `config` SET `value` = '', `value2` = '', `value3` = '' WHERE `type` = 'temp_inv_count' AND `setting` = '[1]'", $item_id);
					unset($tracked_86_items[$item_id]);
				}
			}
		}

		if (!$error_occurred)
		{
			$t8i_keys = array_keys($tracked_86_items);
			foreach ($t8i_keys as $key)
			{
				$update_inv_count = lavu_query("UPDATE `menu_items` SET `inv_count` = (`inv_count` - ([1] * 1)) WHERE `id` = '[2]'", $tracked_86_items[$key], $key);
				$clear_temp_count = lavu_query("UPDATE `config` SET `value` = '', `value2` = '', `value3` = '' WHERE `type` = 'temp_inv_count' AND `setting` = '[1]'", $key);
			}
		}

		if ($reset_last_mod_device)
		{
			$update_order = lavu_query("UPDATE `orders` SET `last_mod_ts` = '[1]', `pushed_ts` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'JC FUNCTIONS' WHERE `ioid` = '[3]'", $internal_order_id, time(), $device_time);
		}

		for ($c = 1; $c <= cappedCheckCount($o_vars['no_of_checks']); $c++)
		{
			$d_vars = array();
			$d_vars['subtotal'] = $check_details[$c]['subtotal'];
			$d_vars['tax'] = roundMoney($check_details[$c]['tax']);
			$d_vars['discount'] = $check_details[$c]['discount'];
			$d_vars['gratuity'] = $check_details[$c]['gratuity'];
			$before_gratuity = ($d_vars['subtotal'] + $d_vars['tax'] - $d_vars['discount']);
			if ($before_gratuity == 0)
			{
				$d_vars['gratuity'] = "0";
				$d_vars['check_total'] = "0";
			}
			else
			{
				$d_vars['check_total'] = ($before_gratuity + $d_vars['gratuity']);
			}

			$update = "";

			buildUpdate($d_vars, $update);

			$d_vars['loc_id'] = $loc_id;
			$d_vars['order_id'] = $order_id;
			$d_vars['ioid'] = $internal_order_id;
			$d_vars['check'] = $c;

			$fields = "";
			$values = "";

			buildInsertFieldsAndValues($d_vars, $fields, $values);

			$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $d_vars);
			if (mysqli_num_rows($check_for_details) == 0)
			{
				$insert = lavu_query("INSERT INTO `split_check_details` (".$fields.") VALUES (".$values.")", $d_vars);
			}
			else
			{
				$info = mysqli_fetch_assoc($check_for_details);
				$d_vars['id'] = $info['id'];
				$update = lavu_query("UPDATE `split_check_details` SET ".$update." WHERE `id` = '[id]'", $d_vars);
			}
		}

		$clean_up_extra_details = lavu_query("DELETE FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` > '[3]'", $loc_id, $order_id, $o_vars['no_of_checks']);

		if (!empty($post_vars['modifiers_used']))
		{
			// ***** modifier indexes *****
			//
			//  0 - mod id
			//  1 - type
			//  2 - content row
			//  3 - quantity
			//  4 - unit cost
			//  5 - icid (internal contents id)

			$clear_current_modifiers = lavu_query("DELETE FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id);

			$order_modifiers = explode("|*|", $post_vars['modifiers_used']);

			foreach ($order_modifiers as $om) 
			{
				$modifier_info = explode("|o|", $om);

				$m_vars = array();
				$m_vars['loc_id'] = $loc_id;
				$m_vars['order_id'] = $order_id;
				$m_vars['mod_id'] = $modifier_info[0];
				$m_vars['type'] = $modifier_info[1];
				$m_vars['row'] = $modifier_info[2];
				$m_vars['qty'] = $modifier_info[3];
				$m_vars['ioid'] = $internal_order_id;
				if (!empty($modifier_info[5]))
				{
					$icid = $modifier_info[5];
				}
				else
				{
					$icid = $icid_index[((int)$m_vars['row'] - 1)];
				}
				$m_vars['icid'] = $icid;
				if (!empty($modifier_info[4]))
				{
					$unit_cost = $modifier_info[4];
				}
				else
				{
					$unit_cost = getModifierUnitCost($modifier_info[0], $modifier_info[1]);
				}
				$m_vars['cost'] = ($unit_cost * $modifier_info[3]);
				$m_vars['unit_cost'] = $unit_cost;

				$fields = "";
				$values = "";

				buildInsertFieldsAndValues($m_vars, $fields, $values);

				$save_modifier_info = lavu_query("INSERT INTO `modifiers_used` (".$fields.") VALUES (".$values.")", $m_vars);
			}
		}

		run_rfix($order_id, $loc_id);

		if (!$error_occurred)
		{
			return "1:".$order_id.":".join(",", $new_row_ids); //.":alert_message:TESTING%3A:1... 2... 3..."; // use percent escapes to print :
		}
		else
		{
			return "0"; //:::alert_message:TESTING:1... 2... 3...";
		}
	}

	function savePaymentsAndDiscounts($post_vars) { // a new, better way of recording payments and discounts

		// mode == 40

		global $data_name;
		global $location_info;
		global $decimal_places;
		global $smallest_money;

		$row_ids = array();
		if (!empty($post_vars['item_details'])) { // prevent auto close with missing items (when quantity set to '') - still unsure of cause of untimely call to mode 10 - RJG 9/3/2013

			$device_time = determineDeviceTime($location_info, $post_vars);

			mlavu_close_db();

			// ***** check_info indexes *****
			//
			//  0 - order id
			//  1 - discount amount
			//  2 - discount label
			//  3 - gratuity amount
			//  4 - guests
			//  5 - cash amount
			//  6 - card amount
			//  7 - gift certificate amount
			//  8 - check count
			//  9 - active check
			// 10 - checked out flag
			// 11 - refund gift certificate amount
			// 12 - subtotal
			// 13 - tax amount
			// 14 - check total
			// 15 - refund amount
			// 16 - remaining amount
			// 17 - discount type id
			// 18 - discount value
			// 19 - gratuity percent
			// 20 - register
			// 21 - discount type
			// 22 - datetime
			// 23 - refund_cc amount
			// 24 - server name
			// 25 - server id
			// 26 - reopened datetime
			// 27 - rounded amount
			// 28 - register name
			// 29 - alt amount
			// 30 - alt refund amount
			// 31 - tax exempt
			// 32 - exemption id
			// 33 - exemption name
			// 34 - discount info
			// 35 - register
			// 36 - register name
			// 37 - internal order id (ioid)
			// 38 - loyalty info
			// 39 - rounding exception

			$check_info = explode("|*|", $post_vars['check_info']);
			$internal_order_id = (isset($check_info[37]))?$check_info[37]:"";

			// ***** payments_and_refunds_array indexes *****
			//
			//  0 - order id
			//  1 - check
			//  2 - amount
			//  3 - total collected
			//  4 - pay type
			//  5 - datetime
			//  6 - action
			//  7 - refund notes
			//  8 - server name
			//  9 - server id
			// 10 - register
			// 11 - change
			// 12 - got response
			// 13 - row id
			// 14 - register name
			// 15 - pay type id
			// 16 - card desc
			// 17 - info
			// 18 - info label
			// 19 - for deposit
			// 20 - card type
			// 21 - tip amount
			// 22 - RFID
			// 23 - customer ID
			// 24 - is deposit
			// 25 - internal ID
			// 26 - first four
			// 27 - transaction id
			// 28 - preauth id
			// 29 - auth code
			// 30 - record_no
			// 31 - ref_data
			// 32 - process_data
			// 33 - mpshc_pid
			// 34 - auth flag
			// 35 - voided
			// 36 - voided by
			// 37 - void notes
			// 38 - void pnref
			// 39 - pin used id
			// 40 - internal order id (ioid)
			// 41 - more info
			// 42 - gateway
			// 43 - reader
			// 44 - signature flag
			// 45 - transtype
			// 46 - swipe grade
			// 47 - expiration date

			$payments_and_refunds_array = explode("|*|", $post_vars['pnr_details']);

			$check_has_printed = (isset($post_vars['check_has_printed']))?$post_vars['check_has_printed']:0;

			$device_id = determineDeviceIdentifier($post_vars);

			$last_payment = false;
			foreach ($payments_and_refunds_array as $payment_or_refund) {
				if ($payment_or_refund != "") {
					$detail_array = explode("|o|", $payment_or_refund);

					$total_collected = decimalize($detail_array[3]);

					$q_fields = "";
					$q_values = "";
					$q_update = "";
					$pnr_vars = array();
					$pnr_vars['check'] = $detail_array[1];
					$pnr_vars['amount'] = decimalize($detail_array[2]);
					$pnr_vars['total_collected'] = $total_collected;
					$pnr_vars['tip_amount'] = decimalize($detail_array[21]);
					if ($pnr_vars['tip_amount'] == "") $pnr_vars['tip_amount'] = "0";
					if (isset($detail_array[27])) $pnr_vars['transaction_id'] = $detail_array[27];
					if (isset($detail_array[34])) $pnr_vars['auth'] = $detail_array[34];
					if (isset($detail_array[35])) $pnr_vars['voided'] = $detail_array[35];
					if (isset($detail_array[36])) $pnr_vars['voided_by'] = $detail_array[36];
					if (isset($detail_array[37])) $pnr_vars['void_notes'] = $detail_array[37];
					if (isset($detail_array[38])) $pnr_vars['void_pnref'] = $detail_array[38];
					$pnr_vars['refund_notes'] = $detail_array[7];
					$keys = array_keys($pnr_vars);
					foreach ($keys as $key) {
						if ($q_update != "") { $q_update .= ", "; }
						$q_update .= "`$key` = '[$key]'";
					}
					$pnr_vars['loc_id'] = $post_vars['loc_id'];
					$pnr_vars['order_id'] = $detail_array[0];
					$pnr_vars['register'] = $check_info[20];
					$pnr_vars['register_name'] = $check_info[28];
					$pnr_vars['change'] = decimalize($detail_array[11]);
					$pnr_vars['got_response'] = $detail_array[12];
					$pnr_vars['pay_type'] = $detail_array[4];
					$pnr_vars['pay_type_id'] = $detail_array[15];
					$pnr_vars['datetime'] = $detail_array[5];
					$pnr_vars['action'] = $detail_array[6];
					$pnr_vars['server_name'] = $detail_array[8];
					$pnr_vars['server_id'] = $detail_array[9];
					$pnr_vars['card_desc'] = $detail_array[16];
					$pnr_vars['info'] = $detail_array[17];
					$pnr_vars['info_label'] = $detail_array[18];
					$pnr_vars['for_deposit'] = $detail_array[19];
					$pnr_vars['is_deposit'] = (isset($detail_array[24]))?$detail_array[24]:0;
					$pnr_vars['card_type'] = $detail_array[20];
					$pnr_vars['rf_id'] = $detail_array[22];
					$pnr_vars['customer_id'] = $detail_array[23];
					$pnr_vars['device_udid'] = $device_id;
					$pnr_vars['internal_id'] = (isset($detail_array[25]))?$detail_array[25]:"";
					$pnr_vars['first_four'] = (isset($detail_array[26]))?$detail_array[26]:"";
					$pnr_vars['preauth_id'] = (isset($detail_array[28]))?$detail_array[28]:"";
					$pnr_vars['auth_code'] = (isset($detail_array[29]))?$detail_array[29]:"";
					$pnr_vars['record_no'] = (isset($detail_array[30]))?str_replace("[PLUS]", "+", $detail_array[30]):"";
					$pnr_vars['ref_data'] = (isset($detail_array[31]))?$detail_array[31]:"";
					$pnr_vars['process_data'] = (isset($detail_array[32]))?$detail_array[32]:"";
					$pnr_vars['mpshc_pid'] = (isset($detail_array[33]))?$detail_array[33]:"";
					$pnr_vars['pin_used_id'] = (isset($detail_array[39]))?$detail_array[39]:"";
					$pnr_vars['ioid'] = $internal_order_id;
					$pnr_vars['more_info'] = (isset($detail_array[41]))?$detail_array[41]:"";
					$pnr_vars['gateway'] = (isset($detail_array[42]))?$detail_array[42]:"";
					$pnr_vars['reader'] = (isset($detail_array[43]))?$detail_array[43]:"";
					if (isset($details_array[44])) $pnr_vars['signature'] = $details_array[44];
					if (isset($details_array[45])) $pnr_vars['transtype'] = $details_array[45];
					if (isset($details_array[46])) $pnr_vars['swipe_grade'] = $details_array[46];
					if (isset($details_array[47])) $pnr_vars['exp'] = $details_array[47];
					$keys = array_keys($pnr_vars);
					foreach ($keys as $key) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
					$pnr_vars['id'] = $detail_array[13];

					if ($pnr_vars['id'] == 0) {
						$check_by_internal_id = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `internal_id` = '[1]'", $pnr_vars['internal_id']);
						if (mysqli_num_rows($check_by_internal_id) > 0) {
							$info = mysqli_fetch_assoc($check_by_internal_id);
							$pnr_vars['id'] = $info['id'];
							$update_detail = lavu_query("UPDATE `cc_transactions` SET $q_update WHERE `id` = '[id]'", $pnr_vars);
							$row_ids[] = $info['id'];
						} else {
							$record_detail = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $pnr_vars);
							$row_ids[] = lavu_insert_id();

							$q_fields = "";
                            $l_vars = array();
							$q_values = "";
							$l_vars = array();
							$l_vars['action'] = (($detail_array[6] == "Sale")?"Payment":"Refund")." Applied";
							$l_vars['loc_id'] = $post_vars['loc_id'];
							$l_vars['order_id'] = $detail_array[0];
							$l_vars['check'] = $detail_array[1];
							$l_vars['time'] = $device_time;
							$l_vars['user'] = $detail_array[8];
							$l_vars['user_id'] = $detail_array[9];
							$l_vars['details'] =  "Check ".$detail_array[1]." - ".$detail_array[4]." - ".priceFormat($total_collected);
							$l_vars['device_udid'] = $device_id;
							$keys = array_keys($l_vars);
							foreach ($keys as $key) {
								if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
								$q_fields .= "`$key`";
								$q_values .= "'[$key]'";
							}
							$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

							// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
						}
					} else {
						$update_detail = lavu_query("UPDATE `cc_transactions` SET $q_update WHERE `id` = '[id]'", $pnr_vars);
						$row_ids[] = $pnr_vars['id'];
					}
				} else $row_ids[] = "";
			}

			update_order_item_details(str_replace("RACER:", "RACER(colon)", $post_vars['item_details']), $check_info[0], $post_vars['loc_id']);

			if (!empty($post_vars['alternate_payments'])) {
				saveAlternatePaymentTotals($post_vars['loc_id'], $check_info[0], $internal_order_id, $check_info[9], $post_vars['alternate_payments']);
			}

			if ($check_info[8] == 1) {

				$get_totals_from_contents = lavu_query("SELECT SUM(`itax`) AS `itax`, SUM(`idiscount_amount`) AS `idiscount_amount` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND (`quantity` * 1) > 0", $post_vars['loc_id'], $check_info[0]);
				$total_data = mysqli_fetch_assoc($get_totals_from_contents);

				$now_ts = time();

				$q_update = "";
				$order_vars = array();
				$order_vars['no_of_checks'] = "1";
				$order_vars['itax'] = $total_data['itax'];
				$order_vars['subtotal'] = decimalize($check_info[12]);
				$order_vars['tax'] = decimalize($check_info[13]);
				$order_vars['total'] = decimalize($check_info[14]);
				$order_vars['idiscount_amount'] = $total_data['idiscount_amount'];
				$order_vars['discount'] = decimalize($check_info[1]);
				$order_vars['discount_sh'] = $check_info[2];
				$order_vars['discount_value'] = decimalize($check_info[18]);
				$order_vars['discount_type'] = $check_info[21];
				$order_vars['discount_id'] = $check_info[17];
				$order_vars['discount_info'] = $check_info[34];
				$order_vars['gratuity'] = decimalize($check_info[3]);
				$order_vars['gratuity_percent'] = decimalize($check_info[19]);
				$order_vars['rounding_amount'] = decimalize($check_info[27]);
				$order_vars['guests'] = $check_info[4];
				$order_vars['check_has_printed'] = $check_has_printed;
				$order_vars['tax_exempt'] = $check_info[31];
				$order_vars['exemption_id'] = $check_info[32];
				$order_vars['exemption_name'] = $check_info[33];
				$order_vars['last_mod_ts'] = $now_ts;
				$order_vars['pushed_ts'] = $now_ts;
				$order_vars['last_modified'] = $device_time;
				$order_vars['last_mod_device'] = determineDeviceIdentifier($post_vars);
				$order_vars['last_mod_register_name'] = $check_info[28];
				if (isset($post_vars['agit'])) $order_vars['auto_gratuity_is_taxed'] = $post_vars['agit'];
				if (isset($post_vars['server_id'])) $order_vars['cashier_id'] = $post_vars['server_id'];
				$keys = array_keys($order_vars);
				foreach ($keys as $key) {
					if ($q_update != "") { $q_update .= ", "; }
					$q_update .= "`$key` = '[$key]'";
				}
				$order_vars['location_id'] = $post_vars['loc_id'];
				$order_vars['order_id'] = $check_info[0];
				$order_vars['ioid'] = $internal_order_id;

				$update_order = lavu_query("UPDATE `orders` SET $q_update WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $order_vars);
			}

			$get_totals_from_contents = lavu_query("SELECT SUM(`itax`) AS `itax`, SUM(`idiscount_amount`) AS `idiscount_amount` FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND (`quantity` * 1) > 0", $post_vars['loc_id'], $check_info[0], $check_info[9]);
			$total_data = mysqli_fetch_assoc($get_totals_from_contents);

			$q_update = "";
			$q_fields = "";
			$q_values = "";
			$detail_vars = array();
			$detail_vars['itax'] = $total_data['itax'];
			$detail_vars['subtotal'] = decimalize($check_info[12]);
			$detail_vars['tax'] = decimalize($check_info[13]);
			$detail_vars['check_total'] = decimalize($check_info[14]);
			$detail_vars['idiscount_amount'] = $total_data['idiscount_amount'];
			$detail_vars['discount'] = decimalize($check_info[1]);
			$detail_vars['discount_sh'] = $check_info[2];
			$detail_vars['discount_value'] = decimalize($check_info[18]);
			$detail_vars['discount_type'] = $check_info[21];
			$detail_vars['discount_id'] = $check_info[17];
			$detail_vars['discount_info'] = $check_info[34];
			$detail_vars['gratuity'] = decimalize($check_info[3]);
			$detail_vars['gratuity_percent'] = decimalize($check_info[19]);
			$detail_vars['card_desc'] = $check_info[10];
			$detail_vars['transaction_id'] = $check_info[10];
			$detail_vars['rounding_amount'] = decimalize($check_info[27]);
			$detail_vars['remaining'] = decimalize($check_info[16]);
			$detail_vars['change'] = 0;
			$detail_vars['checked_out'] = $check_info[10];
			if (isset($check_info[35])) $detail_vars['register'] = $check_info[35];
			if (isset($check_info[36])) $detail_vars['register_name'] = $check_info[36];
			if (isset($check_info[38])) $detail_vars['loyalty_info'] = $check_info[38];
			if (isset($check_info[39])) $detail_vars['rounding_exception'] = $check_info[39];
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$detail_vars['loc_id'] = $post_vars['loc_id'];
			$detail_vars['order_id'] = $check_info[0];
			$detail_vars['check'] = $check_info[9];
			$detail_vars['ioid'] = $internal_order_id;
			$keys = array_keys($detail_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}

			$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' LIMIT 1", $detail_vars);
			if (mysqli_num_rows($check_for_details) > 0) {
				$update_details = lavu_query("UPDATE `split_check_details` SET $q_update WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $detail_vars);
			} else {
				$create_details = lavu_query("INSERT INTO `split_check_details` ($q_fields) VALUES ($q_values)", $detail_vars);
			}

			$check_count = $check_info[8];

			if ($check_count > 1)
			{
				$detail_vars['check_count'] = $check_count;

				$idiscount = 0;
				$discount = 0;
				$itax = 0;
				$gratuity = 0;
				$cash = 0;
				$card = 0;
				$gift_certificate = 0;
				$refund = 0;
				$refund_cc = 0;
				$refund_gc = 0;
				$change = 0;
				$cash_applied = 0;
				$rounded_amount = 0;

				$get_detail_totals = lavu_query("SELECT `idiscount_amount`, `discount`, `itax`, `gratuity`, `cash`, `card`, `gift_certificate`, `refund`, `refund_cc`, `cash_applied`, `rounding_amount` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` <= '[check_count]'", $detail_vars);
				if (mysqli_num_rows($get_detail_totals) > 0)
				{
					while ($extract = mysqli_fetch_assoc($get_detail_totals))
					{
						$idiscount += $extract['idiscount_amount'];
						$discount += $extract['discount'];
						$itax += $extract['itax'];
						$gratuity += $extract['gratuity'];
						$rounded_amount += $extract['rounding_amount'];
					}
				}

				$now_ts = time();

				$q_update = "";
				$order_vars = array();
				$order_vars['no_of_checks'] = $check_info[8];
				$order_vars['idiscount_amount'] = priceFormat($idiscount);
				$order_vars['discount'] = priceFormat($discount);
				$order_vars['itax'] = priceFormat($itax);
				$order_vars['gratuity'] = priceFormat($gratuity);
				$order_vars['gratuity_percent'] = decimalize($check_info[19]);
				$order_vars['rounding_amount'] = priceFormat($rounded_amount);
				$order_vars['guests'] = $check_info[4];
				$order_vars['tax_exempt'] = $check_info[31];
				$order_vars['exemption_id'] = $check_info[32];
				$order_vars['exemption_name'] = $check_info[33];
				$order_vars['last_mod_ts'] = $now_ts;
				$order_vars['pushed_ts'] = $now_ts;
				$order_vars['last_modified'] = $device_time;
				$order_vars['last_mod_device'] = determineDeviceIdentifier($post_vars);
				$order_vars['last_mod_register_name'] = $check_info[28];
				if (isset($post_vars['agit'])) $order_vars['auto_gratuity_is_taxed'] = $post_vars['agit'];
				if (isset($post_vars['server_id'])) $order_vars['cashier_id'] = $post_vars['server_id'];
				$keys = array_keys($order_vars);
				foreach ($keys as $key) {
					if ($q_update != "") { $q_update .= ", "; }
					$q_update .= "`$key` = '[$key]'";
				}
				$order_vars['location_id'] = $post_vars['loc_id'];
				$order_vars['order_id'] = $check_info[0];
				$order_vars['ioid'] = $internal_order_id;

				$update_order = lavu_query("UPDATE `orders` SET $q_update, `check_has_printed` = '$check_has_printed' WHERE `location_id` = '[location_id]' AND `order_id` = '[order_id]'", $order_vars);
			}
			run_rfix($check_info[0], $post_vars['loc_id']);
		} else error_log("!!! empty item details detected during call to mode 40 - data_name: ".$data_name." !!!");
		return '{"json_status":"success","pnr_row_ids":"'.join("|", $row_ids).'"}';
	}
	function logPaymentReassignments($post_vars) { // log payment reassignment after item movement results in check disappearance, also updates check numbers for integrated card transactions

		// mode == 41

		mlavu_close_db();

		if ($_REQUEST['pmoved'] != "") {
			$dlm = strstr($_REQUEST['pmoved'], "|*|")?"|*|":",";
			$pnrs = explode($dlm, $_REQUEST['pmoved']);
			foreach ($pnrs as $por) {
				$por_parts = explode("|o|", trim($por));
				$update_record = lavu_query("UPDATE `cc_transactions` SET `check` = '[1]' WHERE `id` = '[2]'", $por_parts[4], $por_parts[0]);
				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`, `details`) VALUES ('Payments check reassigned', '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now(), '".$por_parts[1]." ".$por_parts[2]." (".$por_parts[3]." to ".$por_parts[4].")')");
			}

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		}

		if ($_REQUEST['cmoved'] != "") {
			$dlm = strstr($_REQUEST['cmoved'], "|*|")?"|*|":",";
			$transactions = explode($dlm, $_REQUEST['cmoved']);
			foreach ($transactions as $t) {
				$t_parts = explode("|o|", trim($t));
				$update_transaction = lavu_query("UPDATE `cc_transactions` SET `check` = '[1]' WHERE `transaction_id` = '[2]' AND `auth_code` = '[3]'", $t_parts[5], $t_parts[0], $t_parts[1]);
				$log_this = lavu_query("INSERT INTO `action_log` (`action`, `loc_id`, `order_id`, `time`, `user`, `user_id`, `server_time`, `details`) VALUES ('Payments check reassigned', '".$_REQUEST['loc_id']."', '".$_REQUEST['order_id']."', '".$_REQUEST['time']."', '".str_replace("'","''",$_REQUEST['server_name'])."', '".$_REQUEST['server_id']."', now(), '".$t_parts[2]." ".$t_parts[3]." (".$t_parts[4]." to ".$t_parts[5].")')");
			}

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
		}

		return "1";
	}
	function saveSinglePayment($post_vars) { // save single payment or refund and return new row id

		// mode == 42

		global $location_info;

		$device_time = determineDeviceTime($location_info, $post_vars);

		// ***** pnr_details indexes *****
		//
		//  0 - order id
		//  1 - check
		//  2 - amount
		//  3 - total collected
		//  4 - pay type
		//  5 - datetime
		//  6 - action
		//  7 - refund notes
		//  8 - server name
		//  9 - server id
		// 10 - register
		// 11 - change
		// 12 - got response
		// 13 - row id
		// 14 - register name
		// 15 - pay type id
		// 16 - card desc
		// 17 - info
		// 18 - info label
		// 19 - for deposit
		// 20 - card type
		// 21 - tip amount
		// 22 - RFID
		// 23 - customer ID
		// 24 - is deposit
		// 25 - internal ID
		// 26 - first four
		// 27 - transaction id
		// 28 - preauth id
		// 29 - auth code
		// 30 - record_no
		// 31 - ref_data
		// 32 - process_data
		// 33 - mpshc_pid
		// 34 - auth flag
		// 35 - voided
		// 36 - voided by
		// 37 - void notes
		// 38 - void pnref
		// 39 - pin used id
		// 40 - internal order id (ioid)
		// 41 - more info
		// 42 - gateway
		// 43 - reader
		// 44 - signature flag
		// 45 - transtype
		// 46 - swipe grade
		// 47 - expiration date

		mlavu_close_db();

		$detail_array = explode("|*|", $post_vars['pnr_details']);
		$internal_order_id = (isset($detail_array[40]))?$detail_array[40]:"";

		$total_collected = decimalize($detail_array[3]);
		$device_id = determineDeviceIdentifier($post_vars);

		$q_fields = "";
		$q_values = "";
		$q_update = "";
		$pnr_vars = array();
		$pnr_vars['check'] = $detail_array[1];
		$pnr_vars['amount'] = decimalize($detail_array[2]);
		$pnr_vars['total_collected'] = $total_collected;
		$pnr_vars['tip_amount'] = decimalize($detail_array[21]);
		if ($pnr_vars['tip_amount'] == "") $pnr_vars['tip_amount'] = "0";
		if (isset($detail_array[27])) $pnr_vars['transaction_id'] = $detail_array[27];
		if (isset($detail_array[34])) $pnr_vars['auth'] = $detail_array[34];
		if (isset($detail_array[35])) $pnr_vars['voided'] = $detail_array[35];
		if (isset($detail_array[36])) $pnr_vars['voided_by'] = $detail_array[36];
		if (isset($detail_array[37])) $pnr_vars['void_notes'] = $detail_array[37];
		if (isset($detail_array[38])) $pnr_vars['void_pnref'] = $detail_array[38];
		$pnr_vars['refund_notes'] = $detail_array[7];
		$keys = array_keys($pnr_vars);
		foreach ($keys as $key) {
			if ($q_update != "") { $q_update .= ", "; }
			$q_update .= "`$key` = '[$key]'";
		}
		$pnr_vars['loc_id'] = $post_vars['loc_id'];
		$pnr_vars['order_id'] = $detail_array[0];
		$pnr_vars['register'] = $detail_array[10];
		$pnr_vars['register_name'] = $detail_array[14];
		$pnr_vars['change'] = decimalize($detail_array[11]);
		$pnr_vars['got_response'] = $detail_array[12];
		$pnr_vars['pay_type'] = $detail_array[4];
		$pnr_vars['pay_type_id'] = $detail_array[15];
		$pnr_vars['datetime'] = $detail_array[5];
		$pnr_vars['action'] = $detail_array[6];
		$pnr_vars['server_name'] = $detail_array[8];
		$pnr_vars['server_id'] = $detail_array[9];
		$pnr_vars['card_desc'] = $detail_array[16];
		$pnr_vars['info'] = $detail_array[17];
		$pnr_vars['info_label'] = $detail_array[18];
		$pnr_vars['for_deposit'] = $detail_array[19];
		$pnr_vars['is_deposit'] = (isset($detail_array[24]))?$detail_array[24]:0;
		$pnr_vars['card_type'] = $detail_array[20];
		$pnr_vars['rf_id'] = $detail_array[22];
		$pnr_vars['customer_id'] = $detail_array[23];
		$pnr_vars['device_udid'] = $device_id;
		$pnr_vars['internal_id'] = (isset($detail_array[25]))?$detail_array[25]:"";
		$pnr_vars['first_four'] = (isset($detail_array[26]))?$detail_array[26]:"";
		$pnr_vars['preauth_id'] = (isset($detail_array[28]))?$detail_array[28]:"";
		$pnr_vars['auth_code'] = (isset($detail_array[29]))?$detail_array[29]:"";
		$pnr_vars['record_no'] = (isset($detail_array[30]))?str_replace("[PLUS]", "+", $detail_array[30]):"";
		$pnr_vars['ref_data'] = (isset($detail_array[31]))?$detail_array[31]:"";
		$pnr_vars['process_data'] = (isset($detail_array[32]))?$detail_array[32]:"";
		$pnr_vars['mpshc_pid'] = (isset($detail_array[33]))?$detail_array[33]:"";
		$pnr_vars['pin_used_id'] = (isset($detail_array[39]))?$detail_array[39]:"";
		$pnr_vars['ioid'] = $internal_order_id;
		$pnr_vars['more_info'] = (isset($detail_array[41]))?$detail_array[41]:"";
		$pnr_vars['gateway'] = (isset($detail_array[42]))?$detail_array[42]:"";
		$pnr_vars['reader'] = (isset($detail_array[43]))?$detail_array[43]:"";
		if (isset($detail_array[44])) $pnr_vars['signature'] = $detail_array[44];
		if (isset($detail_array[45])) $pnr_vars['transtype'] = $detail_array[45];
		if (isset($detail_array[46])) $pnr_vars['swipe_grade'] = $detail_array[46];
		if (isset($detail_array[47])) $pnr_vars['exp'] = $detail_array[47];
		$keys = array_keys($pnr_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$pnr_vars['last_mod_ts'] = time();
		$pnr_vars['last_modified'] = $device_time;

		$row_id = "";
		if (!empty($pnr_vars['internal_id'])) {
			$check_for_record = lavu_query("SELECT `id` FROM `cc_transactions` WHERE `internal_id` = '[1]'", $pnr_vars['internal_id']);
			if (mysqli_num_rows($check_for_record) > 0) {
				$info = mysqli_fetch_assoc($check_for_record);
				$pnr_vars['id'] = $info['id'];
				$update_record = lavu_query("UPDATE `cc_transactions` SET $q_update WHERE `id` = '[id]'", $pnr_vars);
				$row_id = $info['id'];
			}
		}

		$actn = $pnr_vars['action'];
		if ($actn=="Void" && !empty($_REQUEST['orig_internal_id'])) $update_voided = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `voided_by` = '[1]', `void_notes` = '[2]' WHERE `internal_id` = '[3]'", $pnr_vars['server_id'], $pnr_vars['void_notes'], $_REQUEST['orig_internal_id']);

		if ($row_id == "") {

			$record_detail = lavu_query("INSERT INTO `cc_transactions` ($q_fields, `server_time`) VALUES ($q_values, now())", $pnr_vars);
			if (!$record_detail) return '{"json_status":"failed","message":"Failed to create transaction record"}';

			$row_id = lavu_insert_id();

			if ($actn=="Sale" && $pnr_vars['pay_type_id']=="1") {
				$update_order = " `cash_paid` = (`cash_paid` + [total_collected]), `cash_applied` = (`cash_applied` + [total_collected])";
				$update_check = " `cash` = (`cash` + [total_collected]), `cash_applied` = (`cash_applied` + [total_collected])";
				$c_fields = "`cash`, `cash_applied`";
				$c_values = "'[total_collected]', '[total_collected]'";
			} else if ($actn=="Refund" && $pnr_vars['pay_type_id']=="1") {
				$update_order = " `cash_paid` = (`cash_paid` - [amount]), `cash_applied` = (`cash_applied` - [amount]), `refunded` = (`refunded` + [amount])";
				$update_check = " `cash` = (`cash` - [amount]), `cash` = (`cash` - [amount]), `refund` = (`refund` + [amount])";
				$c_fields = "`cash`, `cash_applied`, `refunded`";
				$c_values = "'[amount]', '[amount]', '[amount]'";
			} else if ($actn=="Sale" && $pnr_vars['pay_type_id']=="2") {
				$update_order = " `card_paid` = (`card_paid` + [amount]), `card_gratuity` = (`card_gratuity` + [tip_amount])";
				$update_check = " `card` = (`card` + [amount])";
				$c_fields = "`card`";
				$c_values = "'[amount]'";
			} else if (($actn=="Void" || $actn=="Refund") && $pnr_vars['pay_type_id']=="2") {
				$update_order = " `card_paid` = (`card_paid` - [amount]), `refunded_cc` = (`refunded_cc` + [amount]), `card_gratuity` = (`card_gratuity` - [tip_amount])";
				$update_check = " `card` = (`card` - [amount]), `refund_cc` = (`refund_cc` + [amount])";
				$c_fields = "`card`, `refund_cc`";
				$c_values = "'[amount]', '[amount]'";
			} else if ($actn=="Sale" && $pnr_vars['pay_type_id']=="3") {
				$update_order = " `gift_certificate` = (`gift_certificate` + [total_collected])";
				$update_check = " `gift_certificate` = (`gift_certificate` + [total_collected])";
				$c_fields = "`gift_certificate`";
				$c_values = "'[amount]'";
			} else if ($actn=="Refund" && $pnr_vars['pay_type_id']=="3") {
				$update_order = " `gift_certificate` = (`gift_certificate` - [amount]), `refunded_gc` = (`refunded_gc` + [amount])";
				$update_check = " `gift_certificate` = (`gift_certificate` - [amount]), `refund_gc` = (`refund_gc` + [amount])";
				$c_fields = "`gift_certificate`, `refund_gc`";
				$c_values = "'[amount]', '[amount]'";
			} else if ($actn == "Sale") {
				$update_order = " `alt_paid` = (`alt_paid` + [amount])";
				$update_check = " `alt_paid` = (`alt_paid` + [amount])";
				$c_fields = "`alt_paid`";
				$c_values = "'[amount]'";
			} else if ($actn == "Refund") {
				$update_order = " `alt_paid` = (`alt_paid` - [amount]), `alt_refunded` = (`alt_refunded` + [amount])";
				$update_check = " `alt_paid` = (`alt_paid` - [amount]), `alt_refunded` = (`alt_refunded` + [amount])";
				$c_fields = "`alt_paid`, `alt_refunded`";
				$c_values = "'[amount]', '[amount]'";
			}
			$update_order .= ",`last_mod_ts` = '[last_mod_ts]', `pushed_ts` = '[last_mod_ts]', `last_modified` = '[last_modified]', `last_mod_device` = '[device_udid]', `last_mod_register_name` = '[register_name]'";

			$update_order = lavu_query("UPDATE `orders` SET $update_order WHERE `location_id` = '[loc_id]' AND `order_id` = '[order_id]'", $pnr_vars);

			$q_fields = "";
			$q_values = "";
			$l_vars = array();
			$l_vars['action'] = (($actn == "Sale")?"Payment":"Refund")." Applied";
			$l_vars['loc_id'] = $post_vars['loc_id'];
			$l_vars['order_id'] = $detail_array[0];
			$l_vars['check'] = $detail_array[1];
			$l_vars['time'] = $detail_array[5];
			$l_vars['user'] = $detail_array[8];
			$l_vars['user_id'] = $detail_array[9];
			$l_vars['details'] =  "Check ".$detail_array[1]." - ".$detail_array[4]." - ".priceFormat($total_collected);
			$l_vars['device_udid'] = $device_id;
			$keys = array_keys($l_vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

			// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

			if (!empty($post_vars['alternate_payments'])) saveAlternatePaymentTotals($post_vars['loc_id'], $detail_array[0], $internal_order_id, $detail_array[1], $post_vars['alternate_payments']);

			if (!empty($post_vars['check_details'])) {

				// ***** check_details indexes *****
				//
				// 	0 - subtotal
				//  1 - discount amount
				//  2 - discount sh
				//  3 - tax amount
				//  4 - gratuity amount
				//  5 - gratuity percent
				//  6 - remaining amount
				//  7 - check total amount
				//  8 - discount value
				//  9 - discount type
				// 10 - rounded amount
				// 11 - checked out flag
				// 12 - alt payment total
				// 13 - alt refund total
				// 14 - checked out register
				// 15 - checked out register name
				// 16 - rounding exception

				$check_details = explode("|*|", $post_vars['check_details']);

				$q_fields = "";
				$q_values = "";
				$c_vars = array();
				$c_vars['loc_id'] = $post_vars['loc_id'];
				$c_vars['order_id'] = $detail_array[0];
				$c_vars['check'] = $detail_array[1];
				$c_vars['subtotal'] = decimalize($check_details[0]);
				$c_vars['discount'] = decimalize($check_details[1]);
				$c_vars['discount_sh'] = $check_details[2];
				$c_vars['tax'] = decimalize($check_details[3]);
				$c_vars['gratuity'] = decimalize($check_details[4]);
				$c_vars['gratuity_percent'] = decimalize($check_details[5]);
				$c_vars['remaining'] = decimalize($check_details[6]);
				$c_vars['check_total'] = decimalize($check_details[7]);
				$c_vars['discount_value'] = decimalize($check_details[8]);
				$c_vars['discount_type'] = $check_details[9];
				$c_vars['rounding_amount'] = decimalize($check_details[10]);
				$c_vars['checked_out'] = $check_details[11];
				if (isset($detail_array[12])) $c_vars['alt_paid'] = $check_details[12];
				if (isset($detail_array[13])) $c_vars['alt_refunded'] = $check_details[13];
				if (isset($detail_array[14])) $c_vars['register'] = $check_details[14];
				if (isset($detail_array[15])) $c_vars['register_name'] = $check_details[15];
				if (isset($detail_array[16])) $c_vars['rounding_exception'] = $check_details[16];
				$keys = array_keys($c_vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$c_vars['amount'] = decimalize($detail_array[2]);
				$c_vars['total_collected'] = decimalize($detail_array[3]);
				$c_vars['ioid'] = $internal_order_id;

				$check_for_details = lavu_query("SELECT `id` FROM `split_check_details` WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]' LIMIT 1", $pnr_vars);
				if (mysqli_num_rows($check_for_details) > 0) {
					$update_details = lavu_query("UPDATE `split_check_details` SET $update_check WHERE `loc_id` = '[loc_id]' AND `order_id` = '[order_id]' AND `check` = '[check]'", $pnr_vars);
				} else {
					$create_details = lavu_query("INSERT INTO `split_check_details` ($c_fields, $q_fields) VALUES ($c_values, $q_values)", $c_vars);
				}
			}

			if (!empty($post_vars['void_internal_id'])) {

				$get_original_transaction = lavu_query("SELECT `id`, `action`, `loc_id`, `order_id`, `check`, `pay_type`, `amount` FROM `cc_transactions` WHERE `internal_id` = '[1]'", $post_vars['void_internal_id']);
				if (mysqli_num_rows($get_original_transaction) > 0) {
					$tinfo = mysqli_fetch_assoc($get_original_transaction);
					$update_cc_transaction = lavu_query("UPDATE `cc_transactions` SET `voided` = '1', `void_notes` = '[1]', `voided_by` = '[2]', `void_pnref` = '[3]' WHERE `id` = '[4]'", $pnr_vars['void_notes'], $pnr_vars['server_id'], $pnr_vars['transaction_id'], $tinfo['id']);

					$q_fields = "";
					$q_values = "";
					$l_vars = array();
					$l_vars['action'] = (($tinfo['action'] == "Sale")?"Payment":"Refund")." Voided";
					$l_vars['loc_id'] = $tinfo['loc_id'];
					$l_vars['order_id'] = $tinfo['order_id'];
					$l_vars['check'] = $tinfo['check'];
					$l_vars['time'] = $device_time;
					$l_vars['user'] = $pnr_vars['server_name'];
					$l_vars['user_id'] = $pnr_vars['server_id'];
					$l_vars['details'] = "Check ".$tinfo['check']." - ".$tinfo['pay_type']." - ".priceFormat($tinfo['amount']);
					$l_vars['device_udid'] = determineDeviceIdentifier($post_vars);;
					$keys = array_keys($l_vars);
					foreach ($keys as $key) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}

					$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

					// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
				}
			}
		}

		return '{"json_status":"success","pnr_row_id":"'.$row_id.'"}';
	}

	function writeActionLogEntry($post_vars) {

		// mode == 64

		global $location_info;

		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = $post_vars['action'];
		$l_vars['loc_id'] = $location_info['id'];
		$l_vars['order_id'] = $post_vars['order_id'];
		$l_vars['check'] = $post_vars['check'];
		$l_vars['time'] = $post_vars['device_time'];
		$l_vars['user'] = $post_vars['server_name'];
		$l_vars['user_id'] = $post_vars['server_id'];
		$l_vars['details'] = $post_vars['details'];
		$l_vars['device_udid'] = determineDeviceIdentifier($post_vars);
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro

		return "1";
	}

	function rowString($info, $keys) {

		$str = "";
		foreach ($keys as $key) {
			if ($str != "") $str .= "[_col_]";
			$str .= $info[$key];
		}

		return $str;
	}
	function logCurrentOrderState($order_info, $device_id, $type) {
		error_log("logCurrentOrderState");

		$loc_id = $order_info['location_id'];
		$order_id = $order_info['order_id'];

		$keys = array_keys($order_info);
		$order_fields = implode("[_col_]", $keys);
		$order_values = "";
		foreach ($keys as $key) {
			if ($order_values != "") $order_values .= "[_col_]";
			$order_values .= $order_info[$key];
		}

		$keys = array();
		$check_details_values = "";
		$get_check_details = lavu_query("SELECT * FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' ORDER BY `check` ASC", $loc_id, $order_id);
		while ($info = mysqli_fetch_assoc($get_check_details)) {
			if (count($keys) == 0) {
				$keys = array_keys($info);
				$check_details_fields = implode("[_col_]", $keys);
			}
			if ($check_details_values != "") $check_details_values .= "[_row_]";
			$check_details_values .= rowString($info, $keys);
		}

		$keys = array();
		$order_contents_values = "";
		$get_contents = lavu_query("SELECT * FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` = '[2]'", $loc_id, $order_id);
		while ($info = mysqli_fetch_assoc($get_contents)) {
			if (count($keys) == 0) {
				$keys = array_keys($info);
				$order_contents_fields = implode("[_col_]", $keys);
			}
			if ($order_contents_values != "") $order_contents_values .= "[_row_]";
			$order_contents_values .= rowString($info, $keys);
		}

		$keys = array();
		$modifiers_values = "";
		$get_modifiers = lavu_query("SELECT * FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `qty` != 0", $loc_id, $order_id);
		while ($info = mysqli_fetch_assoc($get_modifiers)) {
			if (count($keys) == 0) {
				$keys = array_keys($info);
				$modifiers_fields = implode("[_col_]", $keys);
			}
			if ($modifiers_values != "") $modifiers_values .= "[_row_]";
			$modifiers_values .= rowString($info, $keys);
		}

		$keys = array();
		$transactions_values = "";
		$get_cc_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `voided` = '0' AND `action` != 'Void' AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $loc_id, $order_id);
		while ($info = mysqli_fetch_assoc($get_cc_transactions)) {
			if (count($keys) == 0) {
				$keys = array_keys($info);
				$transactions_fields = implode("[_col_]", $keys);
			}
			if ($transactions_values != "") $transactions_values .= "[_row_]";
			$transactions_values .= rowString($info, $keys);
		}

		$keys = array();
		$alt_payments_values = "";
		$get_alternate_payment_totals = lavu_query("SELECT `ioid`, `check`, `pay_type_id`, `type`, `amount`, `tip`, `refund_amount` FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' ORDER BY `check` ASC", $loc_id, $order_id);
		while ($info = mysqli_fetch_assoc($get_alternate_payment_totals)) {
			if (count($keys) == 0) {
				$keys = array_keys($info);
				$alt_payments_fields = implode("[_col_]", $keys);
			}
			if ($alt_payments_values != "") $alt_payments_values .= "[_row_]";
			$alt_payments_values .= rowString($info, $keys);
		}

		$data_string = "[_order_fields_start_]".$order_fields."[_order_fields_end_]";
		$data_string .= "[_order_values_start_]".$order_values."[_order_values_end_]";
		$data_string .= "[_check_details_fields_start_]".$check_details_fields."[_check_details_fields_end_]";
		$data_string .= "[_check_details_values_start_]".$check_details_values."[_check_details_values_end_]";
		$data_string .= "[_order_contents_fields_start_]".$order_contents_fields."[_order_contents_fields_end_]";
		$data_string .= "[_order_contents_values_start_]".$order_contents_values."[_order_contents_values_end_]";
		if ($modifiers_values != "") {
			$data_string .= "[_modifiers_fields_start_]".$modifiers_fields."[_modifiers_fields_end_]";
			$data_string .= "[_modifiers_values_start_]".$modifiers_values."[_modifiers_values_end_]";
		}
		if ($transactions_values != "") {
			$data_string .= "[_transactions_fields_start_]".$transactions_fields."[_transactions_fields_end_]";
			$data_string .= "[_transactions_values_start_]".$transactions_values."[_transactions_values_end_]";
		}
		if ($alt_payments_values != "") {
			$data_string .= "[_alt_payments_fields_start_]".$alt_payments_fields."[_alt_payments_fields_end_]";
			$data_string .= "[_alt_payments_values_start_]".$alt_payments_values."[_alt_payments_values_end_]";
		}

		$q_fields = "";
		$q_values = "";
		$l_vars = array();
		$l_vars['action'] = $type." Order Snapshot";
		$l_vars['loc_id'] = $loc_id;
		$l_vars['order_id'] = $order_id;
		$l_vars['check'] = "";
		$l_vars['time'] = determineDeviceTime($location_info, array());
		$l_vars['user'] = $_REQUEST['server_name'];
		$l_vars['user_id'] = $_REQUEST['server_id'];
		$l_vars['details'] = $data_string;
		$l_vars['details_short'] = "hide";
		$l_vars['device_udid'] = $device_id;
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);

		// LP-1171 - Do not call updateSignatureForNorwayActionLog() - Norway Fiscal will not be utilized by Lavu Retro
	}

	function get_addon_list($mode) {

		$addon_list = array();

		$path = dirname(__FILE__)."/jc_addons/".$mode;
		if (is_dir($path)) {
			if ($dir = opendir($path)) {
				while (false !== ($file = readdir($dir))) {
					if (substr($file, -4) == ".php") $addon_list[] = str_replace(".php", "", $file);
				}
				closedir($dir);
			} else error_log("Unable to open addon path: ".$path);
		}

		return $addon_list;
	}
	function process_jc_addons($jc_addons, $active_modules, $mode, $boa, $jc_result="") { // boa = "before or after"

		global $company_info;
		global $location_info;
		global $device_time;

		//error_log("process_jc_addons: ".$mode." ".$boa);
		//error_log("jc_addons: ".print_r($jc_addons, true));

		foreach ($jc_addons as $addon) {

			//error_log("addon: ".$addon);

			$check_module = str_replace("_", ".", $addon);

			if ($active_modules->hasModule($check_module)) {

				//error_log("has module: ".$check_module);

				$addon_path = dirname(__FILE__)."/jc_addons/".$mode."/".$addon.".php";

				//error_log("addon_path: ".$addon_path);

				if (file_exists($addon_path)) {
					//error_log("including ".$addon_path);
					require_once($addon_path);
				}

				if (function_exists($boa."_".$addon)) {
					//error_log("should call ".$boa."_".$addon);
					$jc_result = call_user_func($boa."_".$addon, $jc_result);
				}

				//error_log("jc_result: ".$jc_result);

			} //else error_log("doesn't have module: ".$check_module);
		}

		return $jc_result;
	}

	function determine86counts($active_modules, $loc_id) {

		$rtrnstr = ',"86_counts":{}';

		if ($active_modules->hasModule("dining.86countdown")) {

				$temp_counts = array();
				$get_temp_counts = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `location` = '[1]' AND `type` = 'temp_inv_count' AND `value2` > '[2]'", $loc_id, date("Y-m-d H:i:s", (time() - 180)));
				while ($info = mysqli_fetch_assoc($get_temp_counts)) {
					$temp_counts[$info['setting']] = $info['value'];
				}

				$temp_str = "";
				$check_menu_items = lavu_query("SELECT `id`, `track_86_count`, `inv_count` FROM `menu_items` WHERE `track_86_count` != '' AND `_deleted` != '1'");
				while ($info = mysqli_fetch_assoc($check_menu_items)) {
					if ($info['track_86_count'] == "86count") {
						if ($temp_str != "") $temp_str .= ",";
						$temp_str .= '"'.$info['id'].'":"'.(isset($temp_counts[$info['id']])?$temp_counts[$info['id']]:$info['inv_count']).'"';
					}
				}

				$rtrn_str = ',"86_counts":{'.$temp_str.'}';
		}

		return $rtrn_str;
	}

	function ERScurl($url, $vars) {

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		$response = curl_exec($ch);

		return $response;
	}

	function performExternalLogin($client_app, $group, $minlevel) {

		global $device_time;

		$post_url = "https://default_general.ershost.com/view/update_poslavu_order/";

		$sendstr = "user=".$_REQUEST['un']."&pwd=".$_REQUEST['pw']."&group=".$group."&minlevel=".$minlevel;
		$postvars = "poslavu_order_id=&poslavu_action=validate_login&poslavu_info=" . str_replace(array("&", "="), array("(pass_and)", "(pass_eq)"), $sendstr);

		$response = ERScurl($post_url, $postvars);

		$result = explode("|", $response);
		if (count($result) < 2)
			$response = '{"json_status":"Invalid","ers":"empty response"}';
		else {

			if ($result[0] != "1")
				$response = '{"json_status":"Invalid","ers":"invalid login","message":"'.$result[1].'"}';
			else {

				$resp_vars = array();
				$rp_pairs = explode("&", $result[2]);
				foreach ($rp_pairs as $pair) {
					$pair_parts = explode("=", $pair);
					$resp_vars[$pair_parts[0]] = $pair_parts[1];
				}

				$data_name = $resp_vars['poslavu_dataname'];

				$get_company_info = mlavu_query("SELECT `id`, `data_name`, `company_name`, `company_code`, `logo_img`, `receipt_logo_img`, `dev`, `demo`, `disabled`, `chain_id`, `lavu_lite_server` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $data_name);
				if (mysqli_num_rows($get_company_info) < 1)
					$response = '{"json_status":"NoRestaurants"}';
				else {

					$c_info = mysqli_fetch_assoc($get_company_info);
					if ($c_info['disabled']=="1" || $c_info['disabled'] == "2" || $c_info['disabled'] == "3")
						$response = '{"json_status":"AccountDisabled"}';
					else {

						$u_data = array();
						$u_data['id'] = $resp_vars['id'];
						$u_data['f_name'] = $resp_vars['firstname'];
						$u_data['l_name'] = $resp_vars['lastname'];
						$u_data['access_level'] = $resp_vars['group_level'];
						$u_data['service_type'] = "2";
						$u_data['lavu_admin'] = "0";
						$u_data['PIN'] = "1234";

						lavu_connect_byid($c_info['id'], "poslavu_".$data_name."_db");

						$loc_info = array();
						$loc_info['id'] = "1";
						$get_location_info = lavu_query("SELECT `id`, `net_path`, `use_net_path`, `net_path_print` FROM `locations` LIMIT 1");
						if (mysqli_num_rows($get_location_info) > 0) {
							$loc_info = mysqli_fetch_assoc($get_location_info);
						}
						$get_config_info = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `type` = 'location_config_setting' AND `setting` IN ('ers_netpath', 'lavu_task_netpath', 'lavu_task_api')");
						if (mysqli_num_rows($get_config_info) > 0) {
							while ($cc_info = mysqli_fetch_assoc($get_config_info)) {
								$loc_info[$cc_info['setting']] = $cc_info['value'];
							}
						}

						include_once(dirname(__FILE__)."/redirect_tables.php");
						if ($client_app == "Lavu Task") {
							if (function_exists("getLavuTaskWebpathTable")) {
								$lt_paths = getLavuTaskWebpathTable();
								if (!empty($loc_info['ers_netpath'])) {
									$loc_info['lavu_task_netpath'] = $loc_info['ers_netpath'];
									$loc_info['lavu_task_api'] = $lt_paths['ers'];
								} else {
									$loc_info['lavu_task_netpath'] = $loc_info['net_path'];
									$loc_info['lavu_task_api'] = $lt_paths['lavu'];
								}
							}
						}

						$q_fields = "";
						$q_values = "";
						$log_vars = array();
						$log_vars['action'] = $client_app." User Logged In";
						$log_vars['loc_id'] = $loc_info['id'];
						$log_vars['order_id'] = "0";
						$log_vars['time'] = $device_time;
						$log_vars['user'] = $u_data['f_name']." ".$u_data['l_name'];
						$log_vars['user_id'] = $u_data['id'];
						$log_vars['details'] = $client_app;
						$log_vars['device_udid'] = determineDeviceIdentifier($_REQUEST);
						$keys = array_keys($log_vars);
						foreach ($keys as $key) {
							if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
							$q_fields .= "`$key`";
							$q_values .= "'[$key]'";
						}
						$log_this = lavu_query("INSERT INTO `action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $log_vars);

						$response = '{"json_status":"Valid","server_id":"'.$u_data['id'].'","quick_serve":"1","service_type":"2","lavu_admin":"0","loc_info":'.json_encode($loc_info).',"all_user_data":'.json_encode(array($u_data)).',"access_level":"'.$u_data['access_level'].'","PIN":"1234","f_name":"'.$u_data['f_name'].'","l_name":"'.$u_data['l_name'].'","company_id":"'.$c_info['id'].'","company_name":"'.$c_info['company_name'].'","company_code":"'.lsecurity_name($c_info['company_code'], $c_info['id']).":".$_REQUEST['build'].'","data_name":"'.$c_info['data_name'].'","logo_img":"'.$c_info['logo_img'].'","receipt_logo_img":"'.$c_info['receipt_logo_img'].'","dev":"'.$c_info['dev'].'","demo":"'.$c_info['demo'].'","lavu_lite_server":"'.$c_info['lavu_lite_server'].'","valid_PINs":"1234","valid_admin_PINs":"1234"}';
					}
				}
			}
		}

		echo cleanJSON($response);
		lavu_exit();
	}

	function getERSuserInfo($user_id) {

		$user_info = array();

		$post_url = "https://default_general.ershost.com/view/update_poslavu_order/";

		$sendstr = "user_id=".$user_id."&group=all";
		$postvars = "poslavu_order_id=&poslavu_action=validate_login&poslavu_info=" . str_replace(array("&", "="), array("(pass_and)", "(pass_eq)"), $sendstr);

		$response = ERScurl($post_url, $postvars);

		$result = explode("|", $response);
		if (count($result) >= 1) {
			if ($result[0] == "1") {
				$resp_vars = array();
				$rp_pairs = explode("&", $result[2]);
				foreach ($rp_pairs as $pair) {
					$pair_parts = explode("=", $pair);
					$resp_vars[$pair_parts[0]] = $pair_parts[1];

					$user_info['id'] = $resp_vars['id'];
					$user_info['f_name'] = $resp_vars['firstname'];
					$user_info['l_name'] = $resp_vars['lastname'];
					$user_info['access_level'] = $resp_vars['group_level'];
					$user_info['service_type'] = "2";
					$user_info['lavu_admin'] = "0";
					$user_info['PIN'] = "1234";
				}
			}
		}

		return $user_info;
	}

	function allow236accessLevelShift($data_name) {

		return in_array($data_name, array("7_mares","alcatraz_tours","amorina_pizza","bear_flag","bistro_bobette","bistro_st_germ","blue_ribbon_ta","b_town_diner","cedrics","cornerstone_ca","crab_corner_sw","craftworks_tap","el_azteca","eureka_media_g1","figidini_woodf","fresh_cafe_dow","gallo_wine_roo","grasshoppers","ipho_noodle_ho","island_salad_2","isola_bella1","lickity_split_","loie_fullers","maloneys","millers_grille1","opus_espresso_","paris_bistro1","pasion_latin_f","relish1","rustic_burger1","sababa_mediter","salt_cure","seriously_cupc","shuckers_bar_a1","taste_of_phill","the_main_chees","the_old_place1","the_wellness_e","three_crowns_g","tipsy_coffee_h","vacas_meat_sb"));
	}

	function determineDevicePrefix($loc_id, $uuid, $app_name, $model) {

		global $location_info;
		global $device_time;

		$vars = array();
		$vars['loc_id'] = $loc_id;
		$vars['UUID'] = $uuid;
		$vars['app_name'] = $app_name;
		$vars['model'] = $model;
		$vars['device_time'] = $device_time;
		$vars['remote_ip'] = (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];

		$check_devices = lavu_query("SELECT `id`, `loc_id`, `prefix`, `app_name` FROM `devices` WHERE `UUID` = '[1]'", $uuid);
		$matches = mysqli_num_rows($check_devices);
		if ($matches > 0) {

			$device_info = mysqli_fetch_assoc($check_devices);
			$vars['id'] = $device_info['id'];
			$vars['prefix'] = $device_info['prefix'];

			if ($device_info['app_name'] != $app_name) {
				$vars['from_app'] = $device_info['app_name'];
				$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('Switched apps', now(), '[device_time]', '0', '[remote_ip]', '[UUID]', '[model]', 'from [from_app]', 'to [app_name]', '[loc_id]')", $vars);
			}

			if ($device_info['loc_id'] != $loc_id) {
				$vars['prefix'] = getLowestPrefix($loc_id);
				$vars['from_location'] = getFieldInfo($device_info['loc_id'], "id", "locations", "title");
				$vars['to_location'] = $location_info['title'];
				$log_this = lavu_query("INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('Switched locations', now(), '[device_time]', '0', '[remote_ip]', '[UUID]', '[model]', 'from [from_location]', 'to [to_location]', '[loc_id]')", $vars);
			} else if ($device_info['inactive'] == "1") $vars['prefix'] = getLowestPrefix($vars['loc_id']);

			$update_record = lavu_query("UPDATE `devices` SET `loc_id` = '[loc_id]', `prefix` = '[prefix]', `app_name` = '[app_name]', `model` = '[model]', `last_checkin` = now(), `device_time` = '[device_time]', `remote_ip` = '[remote_ip]', `inactive` = '0', `holding_order_id` = '', `needs_reload` = '0' WHERE `id` = '[id]'", $vars);

			if ($matches > 1) $delete_duplicates = lavu_query("DELETE FROM `devices` WHERE `UUID` = '[1]' AND `id` != '[2]'", $uuid, $vars['id']);

		} else {

			$vars['prefix'] = getLowestPrefix($loc_id);
			$create_record = lavu_query("INSERT INTO `devices` (`loc_id`, `UUID`, `prefix`, `app_name`, `model`, `last_checkin`, `device_time`, `remote_ip`) VALUES ('[loc_id]', '[UUID]', '[prefix]', '[app_name]', '[model]', now(), '[device_time]', '[remote_ip]')", $vars);
		}

		return $vars['prefix'];
	}

	function getLowestPrefix($loc_id) {

		//$ninety_days_ago = date("Y-m-d H:i:s", (time() - 7776000));
		//$get_prefixes = lavu_query("SELECT `prefix` FROM `devices` WHERE `loc_id` = '[1]' AND `last_checkin` >= '[2]'", $loc_id, $ninety_days_ago);
		$get_prefixes = lavu_query("SELECT `prefix` FROM `devices`");
		$prefixes = array();
		if (mysqli_num_rows($get_prefixes) > 0) {
			while ($info = mysqli_fetch_assoc($get_prefixes)) {
				$prefixes[] = $info['prefix'];
			}
		}

		$p = 1;
		for ($p = 1; $p <= 999; $p++) {
			if (!in_array($p, $prefixes)) break;
		}

		return $p;
	}

	function killLavuSession() {

		session_destroy();

		$_SESSION = array();
	}

	function cappedCheckCount($check_count) {

		// the app caps new checks at 50, but some extra margin for merges may be desirable

		return (int)MAX(MIN(100, ($check_count * 1)), 1);
	}
?>
