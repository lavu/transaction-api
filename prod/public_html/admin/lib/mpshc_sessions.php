<?php

	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

	// use database table `callback_sessions` to store session data
	function storeCallbackSession($payment_id, $session_data)
	{
		$save_info = array(
			'payment_id'		=> $payment_id,
			'date_time'		=> UTCDateTime(true),
			'session_data'	=> json_encode($session_data)
		);

		$save_session = lavu_query("INSERT INTO `callback_sessions` (`type`, `order_identifier`, `date_time`, `session_data`) VALUES ('mpshc', '[payment_id]', '[date_time]', '[session_data]')", $save_info);
		if (!$save_session)
		{
			error_log("mpshc storeCallbackSession failed: ".lavu_dberror());
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	// retrieve session data from database table `callback_sessions`
	function retrieveCallbackSession($dataname, $payment_id)
	{
		if (empty($dataname))
		{
			error_log("mpshc retrieveCallbackSession failed: empty dataname");
			return array();
		}

		lavu_connect_dn($dataname, "poslavu_".$dataname."_db");

		$vars = array(
			'payment_id'		=> $payment_id,
			'min_date_time'	=> UTCDateTime(true, (time() - 900)) // 15 minutes
		);

		$num = rand(0, 99);
		if ($num % 5)
		{
			$clean_sessions = lavu_query("DELETE FROM `callback_sessions` WHERE `type` = 'mpshc' AND `date_time` <= '[min_date_time]'", $vars);
			if (!$clean_sessions)
			{
				error_log("mpshc retrieveCallbackSession sessions cleanup failed: ".lavu_dberror());
			}
		}

		$load_session = lavu_query("SELECT * FROM `callback_sessions` WHERE `type` = 'mpshc' AND `order_identifier` = '[payment_id]' AND `date_time` > '[min_date_time]'", $vars);
		if (!$load_session)
		{
			error_log("mpshc retrieveCallbackSession failed: ".lavu_dberror());
			return array();
		}

		if (mysqli_num_rows($load_session) == 0)
		{
			error_log("mpshc retrieveCallbackSession failed: no session found for payment_id");
			return array();
		}

		$callback_session = mysqli_fetch_assoc($load_session);

		$delete_session = lavu_query("DELETE FROM `callback_sessions` WHERE `id` = '[id]'", $callback_session);

		if (empty($callback_session['session_data']))
		{
			error_log("mpshc retrieveCallbackSession failed: empty session_data");
			return array();
		}
		
		return json_decode($callback_session['session_data'], true);
	}
?>
