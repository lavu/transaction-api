<?php

	session_start();

	$dev_flag = reqsessvar("is_dev", "0");
	$native_bg = reqsessvar("native_bg", "0");
	
	require_once("info_inc.php");
	$res_path = dirname($_SERVER['REQUEST_URI']) . "/management/resources/";
	$res_img_path = dirname($_SERVER['REQUEST_URI']) . "/management/";

	require_once(dirname(__FILE__).'/modules.php');
	$modulesStr = (isset($location_info['modules']))?$location_info['modules']:""; // location_info inherited from info_inc
	$is_LLS = file_exists('/poslavu-local/local/settings.txt');
	$modules = getActiveModules($modulesStr, $location_info['product_level'], $is_LLS, $location_info['component_package']);
	$modulesList = $modules->getModList();

	function req_cc($cc, $cc_companyid) {

		return lsecurity_name($cc,$cc_companyid);
	}

	function reqsessvar($str, $default="") {
	
		$val = $default;
		if (isset($_REQUEST[$str])) $val = $_REQUEST[$str];
		else if (isset($_SESSION["mt_".$str])) $val = $_SESSION["mt_".$str];
		if ($val != "") $_SESSION["mt_".$str] = $val;
	
		return $val;
	}

	function recordUsage($data_name, $access_level, $area, $is_mgmt_user) {
	
		$info = $area." (".$access_level.")".($is_mgmt_user?" MGMT":"");
		$temp_info_table = "`poslavu_MAIN_db`.`temp_info`";
	
		$check_previous = mlavu_query("SELECT `id` FROM ".$temp_info_table." WHERE `type` = 'management_area_usage' AND `dataname` = '[1]' AND `info` = '[2]'", $data_name, $info);
		if (mysqli_num_rows($check_previous) > 0) {
			$info = mysqli_fetch_assoc($check_previous);
			$update = mlavu_query("UPDATE ".$temp_info_table." SET `count` = ((`count` * 1) + 1), `datetime` = now() WHERE `id` = '[1]'", $info['id']);
		} else {
			$insert = mlavu_query("INSERT INTO ".$temp_info_table." (`type`, `dataname`, `datetime`, `count`, `info`) VALUES ('management_area_usage', '[1]', now(), '1', '[2]')", $data_name, $info);
		}
	}
	
	ob_start();
	require_once(dirname(__FILE__)."/webview_special_functions.php");
	$webspefun = ob_get_contents();
	ob_end_clean();

	//-
	//reenableScrollingIfAble();
	//-
	
	if (isset($_GET['simapp']) && $_GET['simapp']==1)
	{
		$cc					= $_GET['cc'];
		$loc_id				= $_GET['loc_id'];
		$server_id			= 0;
		$username			= "test";
		$register			= 0;
		$register_name		= "test";
		$poslavu_version	= "cp";
		$device_time		= date("Y-m-d H:i:s");
		$simapp				= 1;	
	}
	else
	{
		$cc						= reqvar("cc", "");
		$loc_id					= reqvar("loc_id", "");
		$server_id				= reqvar("server_id", 0);
		$username				= reqvar("username", "");
		$register				= reqvar("register", "");
		$register_name			= reqvar("register_name", "");
		
		$poslavu_version		= reqvar("app_version", "");
		if (empty($poslavu_version)) {
			$poslavu_version	= reqvar("version_number", "1.5.6");
		}
		
		$device_time			= /*reqvar("device_time", null);*/null;
		if (empty($device_time)) { 
			$device_time		= date("Y-m-d H:i:s", time());
			$tz = $location_info['timezone'];
			if (!empty($tz)) {
				$device_time 	= localize_datetime($device_time, $tz);
			}
		}

		$lavu_lite				= "0";
		$simapp					= 0;
		$lls					= (((int)$location_info['use_net_path'] > 0) && !strstr($location_info['net_path'], "lavu"));
		
		if (empty($username) && !empty($server_id)) {

			$get_username = lavu_query("SELECT `username` FROM `users` WHERE `id` = '[1]'", $server_id);
			if (mysqli_num_rows($get_username) > 0) {
	
				$info = mysqli_fetch_assoc($get_username);
				$username = $info['username'];
			}
		}
	}

	$a_fwd_vars = array(
		"cc"			=> req_cc($cc, $cc_companyid),
		"loc_id"		=> $loc_id,
		"server_id"		=> $server_id,
		"username"		=> $username,
		"register"		=> $register,
		"register_name"	=> urlencode($register_name),
		"device_time"	=> $device_time,
		"app_version"	=> $poslavu_version,
		"lavu_lite"		=> $lavu_lite,
		"native_bg"		=> $native_bg
	);
	
	$fwd_vars = "";
	foreach ($a_fwd_vars as $key => $val) {
		if (!empty($fwd_vars)) {
			$fwd_vars .= "&";
		}
		$fwd_vars .= $key . "=" . $val;
	}

	$device_UUID = reqvar("UUID", "");
	if ($device_UUID=="" && isset($_SESSION['UUID'])) $device_UUID = $_SESSION['UUID'];
	$_SESSION['UUID'] = $device_UUID;
	
	$device_MAC = reqvar("MAC", "");
	if ($device_MAC=="" && isset($_SESSION['MAC'])) $device_MAC = $_SESSION['MAC'];
	$_SESSION['MAC'] = $device_MAC;

	$pack_id = reqvar("pack_id", "");
	$fwd_vars .= '&pack_id='.$pack_id; //Append pack_id what user selected in POS
	$active_language_pack = getWebViewLanguagepack($pack_id, $location_info['use_language_pack'], $loc_id);

	function reenableScrollingIfAble() {

        $isBlockScrollingEnabled = is_array($available_DOcmds) && in_array('set_scrollable', $available_DOcmds);
        if ($isBlockScrollingEnabled){
            echo "<script> setTimeout( function(){ window.location = '_DO:cmd=set_scrollable&set_to=1'; }, 400 ); </script>";
        }
	}
	
	function display_time_($dt) {
		$dt = explode(" ",$dt);
		$date = trim($dt[0]);
		$time = ($dt[1]);
		$date = explode("-",$date);
		$time = explode(":",$time);
		$ts = mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]);
		return date("g:i:s a",$ts);
	}
	
	function getvar($str, $def=false) {
		return (isset($_REQUEST[$str]))?$_REQUEST[$str]:$def;
	}
		
	function display_price($p, $dp) {
	
		global $location_info;
		
		$decimal_char = ".";
		if ($location_info['decimal_char'] != "") $decimal_char = $location_info['decimal_char'];
	
		if ($location_info['left_or_right'] == "right") return number_format($p, $dp, $decimal_char, ",")." ".$location_info['monitary_symbol'];
		else return $location_info['monitary_symbol']." ".number_format($p, $dp, $decimal_char, ",");
	}
				
	$display = "";
	$resp = "";
	
	$access_level = 0;
	$location_query = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $loc_id);
	if(mysqli_num_rows($location_query))
	{
		$location_read = mysqli_fetch_assoc($location_query);
		$server_query = lavu_query("SELECT * FROM `users` WHERE `id`='[1]'", $server_id);
		if(mysqli_num_rows($server_query))
		{
			$server_read = mysqli_fetch_assoc($server_query);
			$access_level = $server_read['access_level'];
			//echo "Logged in: " . $server_read['username'] . " <a onclick='location.reload(true)'>(refresh)</a>";
		}
	}
	
	$level_for_mgmt = (isset($location_info['level_to_access_management_area']))?(int)$location_info['level_to_access_management_area']:2;
	$is_mgmt_user = ($access_level >= $level_for_mgmt);
	
	$level_for_transfer_table = (isset($location_info['level_to_access_transfer_table']))?(int)$location_info['level_to_access_transfer_table']:3;
	$is_access_transfer_table = ($level_for_transfer_table <= $location_info['level_to_access_management_area']) && ($level_for_transfer_table <= $location_info['level_to_access_manager_functions']) && ($access_level >= $level_for_transfer_table);
	
	/*if ($access_level<max(2,(int)$location_info['level_to_access_management_area'] && $simapp<1)
	{
		if (!$simapp) echo "<script language='javascript'>window.location = \"_DO:cmd=alert&message=".rawurlencode("In-app server functions coming soon!")."\";</script>";
	
		exit();
	}
	else
	{*/
		$display = "";
	//}

	$mode = getvar("mode","menu");

	if($mode=="menu")
	{
		require_once(dirname(__FILE__) . "/management/mgmt_menu.php");
	}
	else if($mode=="shifts")
	{
		require_once(dirname(__FILE__) . "/management/shifts.php");
	}
	else if($mode=="clock_punches")
	{
		require_once(dirname(__FILE__) . "/management/clock_punches.php");
	}
	else if($mode=="sales_totals")
	{
		
		$submode = (isset($_GET['submode']))?$_GET['submode']: speak("Closed On");
		require_once("management/sales_totals.php");
		$display .= "<u>".speak("Management").": ".speak("Sales Totals")."</u>";
		$display .= "<table cellspacing=4 cellpadding=4>";
		
		$stabs = array("Closed On","Created On");//,"Payments");
		for($i=0; $i<count($stabs); $i++)
		{
			if($stabs[$i]==$submode)
				$bgcolor = "#f4f4f4";
			else
				$bgcolor = "#bbbbbb";
			$display .= "<td bgcolor='$bgcolor' style='border:solid 1px #aaaabb' onclick='window.location = \"inapp_management.php?mode=sales_totals&submode={$stabs[$i]}&{$fwd_vars}\"'>";
			$display .= speak($stabs[$i]);
			$display .= "</td>";
		}
		
		$display .= "</table>";
		if($submode=="Closed On")
			$display .= output_sales_totals($loc_id,"closed");
		else if($submode=="Created On")
			$display .= output_sales_totals($loc_id,"opened");
		else if($submode=="Payments")
			$display .= output_payments($loc_id);
	}
	else if($mode=="table_details")
	{
		require_once(dirname(__FILE__) . "/management/table_details.php");
	}
	else if($mode=="view_tables")
	{
		require_once(dirname(__FILE__) . "/management/view_tables.php");
	}
	else if($mode=="print_queue")
	{
		require_once(dirname(__FILE__) . "/../local/print_status.php");
	}
	else if($mode=="cc_auths")
	{
		require_once(dirname(__FILE__) . "/management/cc_auths.php");
	}
	else if($mode=="cc_settle")
	{
		require_once(dirname(__FILE__) . "/management/cc_settle.php");
	}
	else if($mode=="cash_reconciliation" || $mode=="server_reconciliation")
	{
		$userName = $_REQUEST['username'];
		$user_query = "SELECT `f_name`, `l_name` FROM `users` WHERE `username` = '[1]'";
		$user_result = lavu_query($user_query, $userName);
		$assoc_arr = mysqli_fetch_assoc($user_result);

		$real_name = $assoc_arr['f_name'] . " " . $assoc_arr['l_name'];
		$real_name = str_replace('<', '&lt;', $real_name);
		recordUsage($data_name, $access_level, $mode, $is_mgmt_user);
		require_once(dirname(__FILE__) . "/management/cash_reconciliation.php");

	}
	else if($mode=="adjust_inventory"){
    	require_once(dirname(__FILE__) . "/management/adjust_inventory.php");
	}
	else if($mode=="payments_in_vs_labor"){
		require_once(dirname(__FILE__) . "/management/payments_in_vs_labor.php");
	}
	else if($mode=="server_tips"){
    	require_once(dirname(__FILE__) . "/management/tips_wizard/tips_wizard.php");
	}
	else if($mode=="deliveries"){
			recordUsage($data_name, $access_level, $mode, $is_mgmt_user);
    	require_once(dirname(__FILE__) . "/management/deliveries.php");
	}
	else if($mode=="manager_log"){
		require_once(dirname(__FILE__) . "/management/log.php");
	}
	else if($mode=="lavu_loyalty"){
		recordUsage($data_name, $access_level, $mode, $is_mgmt_user);
		if ($native_bg == 1) {
			$ll_mode = 'admin';
		}
    	require_once(dirname(__FILE__) . "/management/lavu_loyalty.php");
	}
	else if($mode=="tip_entry"){
		require_once(dirname(__FILE__) . "/management/tip_entry.php");
	}
	else if($mode=='econduit'){
		require_once(dirname(__FILE__) . "/management/econduit.php");
	}
	else if($mode=='labor_report'){
		require_once(dirname(__FILE__) . "/management/labor_report.php");
	}
	else if($mode=='cash_drop'){
		require_once(dirname(__FILE__) . "/management/cash_drop.php");
	}
	else if($mode=='vault_management'){
		require_once(dirname(__FILE__) . "/management/vault_management.php");
	}
	else if($mode=='change_bank'){
		require_once(dirname(__FILE__) . "/management/change_bank.php");
	}
	else if($mode=='bank_deposit'){
		require_once(dirname(__FILE__) . "/management/bank_deposit.php");
	}
	else if($modules->hasModule('managment.'.$mode)){
		require_once(dirname(__FILE__) . "/management/".$mode.".php");
	}
	

if($mode=="cash_reconciliation" || $mode == "server_reconciliation")

{
	$hdr = "";

	/*Takes in an array of key-value pairs and turns them into input fields of id and value for an html form to submit*/
	function tillFormSubmissionVars($a_fwd_vars){
		$submissionInputs = "";
		foreach($a_fwd_vars as $key=>$val){
			$submissionInputs .= "<input type='hidden' id='$key' value='$val'/>";
		}
		return $submissionInputs;
	}

	/*Load location info from the database.*/
	load_location_info($loc_id);
//	get_last_till_record_today($register, $register_name, $today, $monetary_symbol, $loc_id);
	global $tillinout;
	$tillinout = true;

}
//this did to hide change bank and bank deposit for LP-4140
//TODO: once QA is done, remove preceeding __ from below text
else if($mode=="change_bank" || $mode == "bank_deposit")
{
	if(!isset($header_title)) $header_title = ucwords(str_replace("_"," ",$mode));
	if(!isset($subheader)) $subheader = "";

	$hdr = "";
	$hdr .= "<br><br>";
	$hdr .= "<table cellspacing=0 cellpadding=0><tr><td>";

	$hdr .= "<table style='border:solid 2px #4e775c' bgcolor='7ea78c' width='770'><tr><td width='20%'>";
	$hdr .= "<input type='button' value='<< ". speak("Vault Management Menu") . "' onclick='window.location = \"inapp_management.php?mode=vault_management&".$fwd_vars."\"'>";
	$hdr .= "</td><td width='60%' align='center' style='color:#ffffff'>";
	$hdr .= "<b>" . speak($header_title) . "</b>";
	$hdr .= "</td><td width='20%'>&nbsp;</td>";
	$hdr .= "</tr></table>";

	$hdr .= "</td></tr>";
	$hdr .= "<tr><td align='center'>";
	if($subheader != "")
	{
		$hdr .= "<table style='border-left:solid 1px #aaaaaa; border-bottom:solid 1px #aaaaaa; border-right:solid 1px #aaaaaa;' width='720' bgcolor='#eeeeee' cellspacing=0 cellpadding=0><tr><td align='center'>";
		$hdr .= $subheader;
		$hdr .= "</td></tr></table>";
	}
	$hdr .= "</td></tr>";
	$hdr .= "</table>";

	$hdr .= "<br><br>";

	//$display = "<div style='width:984px; height:586px; overflow:auto'>" . $hdr . $display . "</div>";
	$display = $hdr . $display;
}
else if($mode!="menu")
{
	if(!isset($header_title)) $header_title = ucwords(str_replace("_"," ",$mode));
	if(!isset($subheader)) $subheader = "";

	$hdr = "";
	$hdr .= "<br><br>";
	$hdr .= "<table cellspacing=0 cellpadding=0><tr><td>";

	$hdr .= "<table style='border:solid 2px #4e775c' bgcolor='7ea78c' width='770'><tr><td width='20%'>";
	$hdr .= "<input type='button' value='<< " . speak("Management Menu") . "' onclick='window.location = \"inapp_management.php?mode=menu&".$fwd_vars."\"'>";
	$hdr .= "</td><td width='60%' align='center' style='color:#ffffff'>";
	$hdr .= "<b>" . speak($header_title) . "</b>";
	$hdr .= "</td><td width='20%'>&nbsp;</td>";
	$hdr .= "</tr></table>";

	$hdr .= "</td></tr>";
	$hdr .= "<tr><td align='center'>";
	if($subheader != "")
	{
		$hdr .= "<table style='border-left:solid 1px #aaaaaa; border-bottom:solid 1px #aaaaaa; border-right:solid 1px #aaaaaa;' width='720' bgcolor='#eeeeee' cellspacing=0 cellpadding=0><tr><td align='center'>";
		$hdr .= $subheader;
		$hdr .= "</td></tr></table>";
	}
	$hdr .= "</td></tr>";
	$hdr .= "</table>";

	$hdr .= "<br><br>";

	//$display = "<div style='width:984px; height:586px; overflow:auto'>" . $hdr . $display . "</div>";
	$display = $hdr . $display;
}

?>        

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Management</title>
		<style>
			body { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; }
			.cct_labels { color:#999999; font-size:11px; }
			.cct_info { color:#666666; font-size:11px; }
			.error_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#CC0000; }
			.info_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; }
<?php
	if ($native_bg == "0") {
		echo ".mt_panel_top { background-image:url(\"management/images/top_management.png\"); background-repeat:no-repeat; width:980px; height:50px; }";
		echo ".mt_panel_mid { background-image:url(\"management/images/middle_management.png\"); background-repeat:repeat-y; width:980px; height:564px; }";
		echo ".mt_panel_bottom { background-image:url(\"management/images/bottom_management.png\"); background-repeat:no-repeat; width:980px; height:30px; }";
	} else {
		echo ".mt_panel_top { width:986px; height:30px; }";
		echo ".mt_panel_mid { width:986px; height:564px; }";
		echo ".mt_panel_bottom { width:986px; height:30px; }";
	}
?>
			.small_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; }
			.tbot { border-top:solid 2px #777777; text-align:right; font-weight:bold; }
			.title_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:18px; color:#000000; }
			.ttop { border-bottom:solid 2px #777777; text-align:center; font-weight:bold; }
			.mtpanel_top2 { }
			.mtpanel_mid2 { }
			.mtpanel_bottom2 { }
		</style>
		<script language='javascript'>
			
				function refreshPage() {
					window.location = "?<?php echo "mode=".$mode."&".$fwd_vars; ?>";
				}
		</script>
		
		<?php echo $webspefun; ?>
	</head>

	<body> <!-- background='management/images/iambg2.png' -->
		<center>
        	<?php
				if(isset($suppress_white_bg) && $suppress_white_bg)
				{
					echo $display;
				}
				else
				{
					?>
					<table cellspacing='0' cellpadding='0' width='<?php echo ($native_bg == "0")?"980":"986"; ?>px'>
						<!--<tr><td background='management/images/iambg3.png'>&nbsp;</td></tr>-->
						<tr><td class='mt_panel_top'>&nbsp;</td></tr> <!-- iambg3 -->
						<tr>
							<td class='mt_panel_mid' align='center' valign='top'>
								<table cellspacing='0' cellpadding='3' width='<?php echo ($native_bg == "0")?"960":"966"; ?>px'>
									<tr><td align='center'><?php echo $display; ?></td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td><?php echo $resp; ?></td></tr>
									<tr><td>&nbsp;</td></tr>
								</table>
							</td>
						</tr>
						<tr><td class='mt_panel_bottom' align='center'>
                        	<table width='848' background=''><tr><td style='height:42px'>&nbsp;</td></tr></table> <!-- iambg4 -->
                        </td></tr>
					</table>
                    <?php
				}
			?>
		</center>
	</body>
</html>
