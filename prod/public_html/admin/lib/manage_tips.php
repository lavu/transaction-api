<?php
	ini_set("max_execution_time", 9000);
	ini_set("ignore_user_abort", 1);

	session_start();

	$cc = "";
	if (isset($_REQUEST['cc']))
	{
		$cc = $_REQUEST['cc'];
	}

	$force_load_location = TRUE;

	require_once(__DIR__."/info_inc.php");
	require_once(__DIR__."/gateway_functions.php");
	require_once(__DIR__."/mpshc_functions.php");

	ob_start();
	require_once(__DIR__."/webview_special_functions.php");
	$webspefun = ob_get_contents();
	ob_end_clean();

	mlavu_close_db();

	$pack_id = reqvar("pack_id", "");
	$active_language_pack = getWebViewLanguagepack($pack_id, $location_info['id']);

	$lavu_query_should_log = TRUE;

	function getvar($str, $def=FALSE) {
		return (isset($_REQUEST[$str]))?$_REQUEST[$str]:$def;
	}

	function reqsessvar($str, $default="") {

		$val = $default;
		if (isset($_REQUEST[$str])) $val = $_REQUEST[$str];
		else if (isset($_SESSION["mt_".$str])) $val = $_SESSION["mt_".$str];
		if ($val != "") $_SESSION["mt_".$str] = $val;

		return $val;
	}

	//Edited by Brian D. We want to use their UTF-8 monitary symbol.
	function display_price($p, $decimal_places) {
		$locRow = get_location_row();
		$monitarySymbol = !empty($locRow) && !empty($locRow['monitary_symbol']) ? $locRow['monitary_symbol'] : "$";
		$decimal_char = isset($locRow['decimal_char']) ? $locRow['decimal_char'] : '.';
		$thousands_char	= ($decimal_char == ".")?",":".";
		return $monitarySymbol.number_format((float)$p, $decimal_places, $decimal_char, $thousands_char);
	}

	//Added by Brian D.  Need to get monitary symbol for display.
	function get_location_row()
	{
		global $got_loc_id; //Included from info_inc.php
		static $locRow = FALSE;
		if($locRow)
			return $locRow;
		$query = "SELECT * FROM `locations` WHERE `id`='[1]'";
		$result = lavu_query($query, $got_loc_id);
		if(!$result) {error_log("mysql error in ".__FILE__." -3i3wr- mysql error:" . lavu_dberror()); }
		else{ $locRow = mysqli_fetch_assoc($result); }
		return $locRow;
	}

	function display_time_($dt) {
		$dt = explode(" ",$dt);
		$date = trim($dt[0]);
		$time = ($dt[1]);
		$date = explode("-",$date);
		$time = explode(":",$time);
		$ts = mktime($time[0],$time[1],$time[2],$date[1],$date[2],$date[0]);
		return date("n/j g:i:s a",$ts);
	}

	function currentLocalTime()
	{
		global $location_info;

		$current_time = date("Y-m-d H:i:s", time());
		$tz = $location_info['timezone'];
		if (!empty($tz))
		{
			$current_time = localize_datetime(date("Y-m-d H:i:s"), $tz);
		}

		return $current_time;
	}

	// check to see if this sale has a matching refund transaction
	// took this code from ValidateOrderAndCCTransactionsTables.php
	// using it here and in end_of_day_new.php
	function findSaleWithRefund($all_check_ccts, $cc_transaction_rows){
		$foundMatch = FALSE;
		//Loop through all of the rows to see if the transaction_id exists.
		foreach($all_check_ccts as $check_cct){
			//If we find a match, make a note of it and break from the inner for-loop
			if(!empty($cc_transaction_rows['internal_id']) && !empty($check_cct['refund_pnref'])
				&& $cc_transaction_rows['internal_id'] === $check_cct['refund_pnref']) {
				$foundMatch = TRUE;
			}
		}
		return $foundMatch;
	}

	function manageTipsTargetURL() {

		$post_to = "";
		$domain = $_SERVER['SERVER_NAME'];
		if (strstr($domain, "lavu")) {

			$protocol = "http";

			switch ($domain) {
				case "admin.poslavu.com":
				case "autocloud.poslavu.com":
				case "cloud1.poslavu.com":
				case "cloud2.poslavu.com":
				case "cloud3.poslavu.com":
				case "cloud4.lavutogo.com":
				case "dcloud1.poslavu.com":
				case "cg1.lavusys.com":
				case "te1.lavusys.com":
					$protocol = "https";
					$domain = "te1.lavusys.com";
					break;
				case "cg2.lavusys.com":
				case "te2.lavusys.com":
					$protocol = "https";
					$domain = "te2.lavusys.com";
					break;
				case "cg3.lavu.com":
				case "te3.lavu.com":
					$protocol = "https";
					$domain = "te3.lavu.com";
					break;
				case "cg4.lavu.com":
				case "te4.lavu.com":
					$protocol = "https";
					$domain = "te4.lavu.com";
					break;
				default:
					break;
			}

			$post_to = $protocol."://".$domain."/lib/manage_tips.php?PHPSESSID=".session_id();
		}

		return $post_to;
	}

	$resp = "";

	$integrateCC		= $location_info['integrateCC'];
	$gateway_debug		= $location_info['gateway_debug'];

	$opened_or_closed	= reqvar("opened_or_closed", "opened");
	$day_adj			= reqvar("day_adj", 0);

	$loc_id				= reqsessvar("loc_id");
	$server_id			= reqsessvar("server_id");
	$register			= reqsessvar("register");
	$register_name		= reqsessvar("register_name");
	$device_time		= reqsessvar("device_time");
	$device_id			= reqsessvar("UUID");
	$native_bg			= reqsessvar("native_bg", "0");
	$app_name	 		= reqsessvar("app_name", "POSLavu Client");
	$app_version		= reqsessvar("app_version");
	$server_name 		= reqsessvar("server_name");
	$sortOrderBy 		= reqsessvar("orderBy");

	if (empty($app_version))
	{
		$app_version	= reqsessvar("version_number");
	}
	if (empty($app_version))
	{
		$app_version	= "1.5.6";
	}
	$app_build			= reqsessvar("app_build", "");

	if (empty($device_time))
	{
		$device_time = date("Y-m-d H:i:s", time());
	}
	$tz = $location_info['timezone'];
	if (!empty($tz))
	{
		$device_time = localize_datetime(date("Y-m-d H:i:s"), $tz);
	}

	$show_all_servers = FALSE;
	if (isset($_REQUEST['show_all_servers']))
	{
		$show_all_servers = ($_REQUEST['show_all_servers']=="1")?TRUE:FALSE;
	}

	$should_post = TRUE;
	if (isset($_REQUEST['do_not_post']))
	{
		$should_post = ($_REQUEST['do_not_post']=="1")?FALSE:TRUE;
	}

	$found_unprocessed_transactions = FALSE;
	$send_alert = "";

	$market_type = $location_info['market_type'];
	$send_debug_messages = $location_info['gateway_debug'];

	$smallest_money = (1 / pow(10, $decimal_places));

	function get_alt_total($loc_id, $order_id, $type_id)
	{
		$total_query = lavu_query("SELECT SUM(`amount`) AS `sum_total` FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `pay_type_id` = '[3]'", $loc_id, $order_id, $type_id);
		if (mysqli_num_rows($total_query))
		{
			$total_read = mysqli_fetch_assoc($total_query);
			return $total_read['sum_total'];
		}

		return 0;
	}

	$cc_pay_type_ids = array("2");
	$alt_payment_types = array();
	$alt_paid_grand_totals = array();
	$alt_column = speak("Gift");
	$alt_m_title = speak("Gift Total");
	$get_alt_payment_types = lavu_query("SELECT * FROM `payment_types` WHERE `loc_id` = '[1]' AND `id` > '3' AND `_deleted` != '1' ORDER BY `_order` ASC, `type` ASC", $location_info['id']);
	if (@mysqli_num_rows($get_alt_payment_types) > 0)
	{
		$alt_payment_types[] = array(3,speak("Gift Certificate"));
		$alt_paid_grand_totals[] = 0;
		while ($alt_payment_info = mysqli_fetch_assoc($get_alt_payment_types))
		{
			$alt_payment_types[] = array($alt_payment_info['id'], $alt_payment_info['type']);
			$alt_paid_grand_totals[] = 0;
			if ($alt_payment_info['special'] == "payment_extension:31")
			{
				$cc_pay_type_ids[] = $alt_payment_info['id'];
			}
		}
		$alt_column = speak("Other");
		$alt_m_title = speak("Other Total");
	}

	$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
	$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

	$day_adj = (isset($_REQUEST['day_adj']))?$_REQUEST['day_adj']:0;

	$idatetime = explode(" ", $device_time);
	$idate = explode("-", $idatetime[0]);
	$itime = explode(":", $idatetime[1]);
	$yts = mktime($itime[0], $itime[1], $itime[2], $idate[1], (($idate[2] - 1) - $day_adj), $idate[0]);
	$nowts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] - $day_adj), $idate[0]);
	$tts = mktime($itime[0], $itime[1], $itime[2], $idate[1], (($idate[2] + 1) - $day_adj), $idate[0]);

	if ($itime[0].$itime[1].$itime[2] >= $se_hour.$se_min."00")
	{
		$use_date = date("Y-m-d", $nowts);
		$end_date = date("Y-m-d", $tts);
	}
	else
	{
		$use_date = date("Y-m-d", $yts);
		$end_date = date("Y-m-d", $nowts);
	}
	// based on date format setting
	$date_setting = "date_format";
	$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
	if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
		$date_format = array("value" => 2);
	}else
		$date_format = mysqli_fetch_assoc( $location_date_format_query );

	$udate = explode("-",$use_date);
	$uyear = $udate[0];
	$umonth = $udate[1];
	$uday = $udate[2];
	//$udate_display = date("n/j/Y",mktime(0,0,0,$umonth,$uday,$uyear));

	$end_hour = $se_hour;
	$end_min = str_pad(($se_min - 1), 2, "0", STR_PAD_LEFT);
	if ($end_min < 0)
	{
		$end_hour = str_pad(($se_hour - 1), 2, "0", STR_PAD_LEFT);
		$end_min = 59;
	}
	if ($end_hour < 0)
	{
		$end_hour = 23;
		$end_date = date("Y-m-d", $nowts);
	}

	$change_format = explode("-", $end_date);
	switch ($date_format['value']){
		case 1:$udate_display_format = date("j/n/Y",mktime(0,0,0,$umonth,$uday,$uyear));
		$display_use_date_format = $uday."-".$umonth."-".$uyear;
		$display_end_date_format = $change_format[2]."-".$change_format[1]."-".$change_format[0];
		break;
		case 2:$udate_display_format = date("n/j/Y",mktime(0,0,0,$umonth,$uday,$uyear));
		$display_use_date_format = $umonth."-".$uday."-".$uyear;
		$display_end_date_format = $change_format[1]."-".$change_format[2]."-".$change_format[0];
		break;
		case 3:$udate_display_format = date("Y/n/j",mktime(0,0,0,$umonth,$uday,$uyear));
		$display_use_date_format = $uyear."-".$umonth."-".$uday;
		$display_end_date_format = $change_format[0]."-".$change_format[1]."-".$change_format[2];
		break;
	}

	$start_datetime = "$use_date $se_hour:$se_min:00";
	$end_datetime = "$end_date $end_hour:$end_min:59";
	$start_datetime_display = "$display_use_date_format $se_hour:$se_min:00";
	$end_datetime_display = "$display_end_date_format $end_hour:$end_min:59";


	$dbname = 'poslavu_'.$data_name.'_db';
	$get_user_row = mysqli_fetch_assoc(mlavu_query("SELECT `id`, `settings` FROM `$dbname`.`users` WHERE `id`='[1]' AND `_deleted` != 1", $server_id));
	$json_data = json_decode($get_user_row['settings']);
	if (is_null($get_user_row['settings']) || $get_user_row['settings'] == "NULL" || !isset($sortOrderBy)) {
		$sortOrderBy = 'DESC';
	} else {
		foreach ($json_data as $value) {
			$sortOrderBy = $value;
		}
	}
	$sortingListDisplay = displaySortOrderList($sortOrderBy, $dbname);

	$upper_display = "<tr><td align='center'><span class='title_text'><b>Orders for $udate_display_format</b></span></td></tr>";
	if ($show_all_servers){
		$upper_display .= "<tr><td align='center'><span class='title_text'>".speak("All Servers")."</span></tr>";
	}
	$upper_display .= "<tr><td align='center'><span class='small_text'>($start_datetime_display to $end_datetime_display)</span></td></tr>";

	$upper_display .= "<tr><td align='center' style='padding:4px 4px 4px 4px'>";
	if ($show_all_servers)
	{
		$upper_display .= "<script language='javascript'>
				function setDayAdj(dayAdj) {
					document.getElementById('day_adj').value = dayAdj;
					document.getElementById('do_not_post').value = '1';
					document.getElementById('limit_page').value = '1';
					submitTips();
				}
			</script>";
		if ($day_adj < 7)
		{
			$upper_display .= "<input type='button' style='font-size:14px' value='<< Previous Day' onclick='setDayAdj(".($day_adj + 1).");'>";
		}
	}
	$upper_display .= " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button class='btn_light_long' ontouchstart='setOpenedOrClosed(\"".$opened_or_closed."\");'><b>".speak("View orders ".(($opened_or_closed == "opened")?"closed":"opened")." today")."</b></button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
	if ($show_all_servers)
	{
		if ($day_adj > 0)
		{
			if ($day_adj < 7)
			{
				$upper_display .= " &nbsp;&nbsp;&nbsp;&nbsp; ";
			}
			$upper_display .= "<input type='button' style='font-size:14px' value='Next Day >>' onclick='setDayAdj(".($day_adj - 1).");'>";
		}
	}
	$upper_display .= "</td></tr>";

	$display = "<tr><td>&nbsp;</td></tr>";
	$mid = "";
	$todays_cash_tips = 0;
	$cash_tip_info = array();
	$check_todays_cash_tips = lavu_query("SELECT `id`, `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $loc_id, $server_id, $use_date);
	if (mysqli_num_rows($check_todays_cash_tips) > 0)
	{
		$cash_tip_info = mysqli_fetch_assoc($check_todays_cash_tips);
		$todays_cash_tips = $cash_tip_info['value'];
	}

	$cash_tip_mode = (int)$location_info['cash_tips_as_daily_total'];

	if ($cash_tip_mode==1 && !$show_all_servers)
	{
		if (isset($_REQUEST['todays_cash_tips']))
		{
			$set_cash_tips = $_REQUEST['todays_cash_tips'];
			$todays_cash_tips = number_format((float)$set_cash_tips, $decimal_places, ".", "");
			if (isset($cash_tip_info['id']) && $cash_tip_info['id']>0)
			{
				$update_cash_tips = lavu_query("UPDATE `cash_data` SET `value` = '[1]' WHERE `id` = '[2]'", $todays_cash_tips, $cash_tip_info['id']);
			}
			else
			{
				$save_cash_tips = lavu_query("INSERT INTO `cash_data` (`type`, `loc_id`, `server_id`, `date`, `value`) VALUES ('server_cash_tips', '[1]', '[2]', '[3]', '[4]')", $loc_id, $server_id, $use_date, $todays_cash_tips);
			}
			ensureCashTipsInsertionForCCTransaction();
			shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $server_id $loc_id $data_name > /dev/null 2>/dev/null &");
		}

		$cash_tip_str = "";
		if ((float)$todays_cash_tips > 0)
		{
			$cash_tip_str = $todays_cash_tips;
		}
	}
	else if ($cash_tip_mode == 0)
	{
		$cashtip_list = getvar('cashtip_list');
		if ($cashtip_list && $should_post)
		{
			$error_encountered = FALSE;
			$cashtip_list = explode("|",$cashtip_list);
			$sumCashTip = 0.0;
			for ($c = 0; $c < count($cashtip_list); $c++)
			{
				$cashtip_identifier = $cashtip_list[$c];
				$debug .= $c." / ".count($cashtip_list)." - ".$cashtip_identifier." - ".getvar('set_cashtip_'.$cashtip_identifier, "na")."\n\n";
				$set_cashtip = getvar('set_cashtip_'.$cashtip_identifier, "na");
				if ($set_cashtip != "na")
				{
					$set_cashtip = str_replace("$","",$set_cashtip);
					if (is_numeric($set_cashtip))
					{
						$id_parts = explode("_", $cashtip_identifier);
						$where_clause = " `order_id` = '[4]' AND `location_id` = '[5]'";
						$where_identifier = $id_parts[0];
						if (!empty($id_parts[1]))
						{
							$where_clause = " `ioid` = '[4]'";
							$where_identifier = $id_parts[1];
						}

						$cash_tips = str_replace("'","''", number_format($set_cashtip, $decimal_places, ".", ""));
						lavu_query("UPDATE `orders` SET `cash_tip` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'MANAGE TIPS', `last_mod_ts` = '[3]', `pushed_ts` = '[3]' WHERE ".$where_clause, $cash_tips, $device_time, time(), $where_identifier, $loc_id);
						//lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `ioid` = '[3]' AND `pay_type` = 'Cash' AND `action` = 'Sale' AND `check` = 1 ", $cash_tips, time(), $id_parts[1]);
						$sumCashTip += $set_cashtip;
					}
					else if ($set_cashtip != "")
					{
						$resp .= "<tr><td>Error: Invalid cash tip value \"".$set_cashtip."\" for Order ID ".$id_parts[0]."</td></tr>";
						$error_encountered = TRUE;
					}
				}
			}
			shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $server_id $loc_id $data_name > /dev/null 2>/dev/null &");
			if (!$error_encountered)
			{
				$resp .= "<tr><td align='center'>All cash tips successfully updated.</td></tr>";
			}
		}
	}

	$cardtip_list = getvar('cardtip_list');
	$cctip_list = getvar("cctip_list");

	$process_info = array(
		'card_amount'		=> "",
		'card_cvn'			=> "",
		'card_number'		=> "",
		'company_id'		=> $company_info['id'],
		'data_name'			=> $company_info['data_name'],
		'device_time'		=> $device_time,
		'device_udid'		=> $device_id,
		'exp_month'			=> "",
		'exp_year'			=> "",
		'ext_data'			=> "",
		'for_deposit'		=> "",
		'is_deposit'		=> "",
		'loc_id'			=> $loc_id,
		'mag_data'			=> "",
		'name_on_card'		=> "",
		'reader'			=> "",
		'register'			=> $register,
		'register_name'		=> $register_name,
		'server_id'			=> $server_id,
		'server_name'		=> "",
		'set_pay_type'		=> "Card",
		'set_pay_type_id'	=> "2"
	);

	if ($cardtip_list && $should_post)
	{
		$error_encountered = FALSE;
		$cardtip_list = explode("|",$cardtip_list);

		if ($location_info['integrateCC'] == "1")
		{
			$integration_info = get_integration_from_location($loc_id, "poslavu_".$data_name."_db");

			$func_path = __DIR__."/gateway_lib/".strtolower($integration_info['gateway'])."_func.php";
			if (file_exists($func_path))
			{
				require_once($func_path);
			}

			$process_info['username']		= $integration_info['integration1'];
			$process_info['password']		= $integration_info['integration2'];
			$process_info['integration3']	= $integration_info['integration3'];
			$process_info['integration4']	= $integration_info['integration4'];
			$process_info['integration5']	= $integration_info['integration5'];
		}

		$used_preauth_ids = array(); // safety net to prevent auths from being captured more than once

		for ($c = 0; $c < count($cardtip_list); $c++)
		{
			$cardtip_identifier = $cardtip_list[$c];
			$a_tip_has_changed = FALSE;

			$set_cardtip = getvar('set_cardtip_'.$cardtip_identifier, "na");
			if ($set_cardtip != "na")
			{
				$set_cardtip = str_replace("$","",$set_cardtip);
				if (is_numeric($set_cardtip) || ($set_cardtip == "") || ($set_cardtip == "multi"))
				{
					$error_code = 0;
					$error_info = 0;

					$check_got_response = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";

					$id_parts = explode("_", $cardtip_identifier);
					$where_clause = " `order_id` = '[1]' AND `loc_id` = '[2]'";
					$where_identifier = $id_parts[0];
					if (!empty($id_parts[1]))
					{
						$where_clause = " `ioid` = '[1]'";
						$where_identifier = $id_parts[1];
					}

					$get_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE ".$where_clause."$check_got_response AND `pay_type_id` = '2' AND `action` = 'Sale' AND `voided` != '1' AND `order_id` NOT LIKE '777%' ORDER BY `check`, `id` ASC", $where_identifier, $loc_id);
					$transaction_count = mysqli_num_rows($get_transactions);
					if ($transaction_count == 1)
					{
						$found_unprocessed_transactions = TRUE;
						$transaction_info = mysqli_fetch_assoc($get_transactions);
						$apply_tip = number_format((float)$set_cardtip, $decimal_places, ".", "");

						$process_info['username']	= $integration_info['integration1'];
						$process_info['password']	= $integration_info['integration2'];
						$process_info['ioid']		= $transaction_info['ioid'];
						$process_info['order_id']	= $transaction_info['order_id'];
						$process_info['check']		= $transaction_info['check'];
						$process_info['pnref']		= $transaction_info['transaction_id'];

						$use_gateway = $transaction_info['gateway']; // THIS IS ADDED TO TAKE THE GATEWAY FROM CCTRANSACTIONS TABLE RATHER THEN LOCATIONS TABLE
						if (empty($use_gateway)) {
							$use_gateway = $integration_info['gateway']; // Fall back on location setting if `gateway` was not written to the transaction record
						}

						if (((float)$apply_tip != (float)$transaction_info['tip_amount']) || ($transaction_info['auth'] == 1))
						{
							$a_tip_has_changed = TRUE;
							if ($location_info['integrateCC'] == "1")
							{
								$compare_record = $transaction_info['preauth_id'].$transaction_info['record_no'];
								if (!in_array($compare_record, $used_preauth_ids) || $compare_record=="")
								{
									$used_preauth_ids[] = $compare_record;
									if ($use_gateway == "PayPal" && $transaction_info['transtype'] == "Auth")
									{
										$get_main_tips = mlavu_query("SELECT `id`, `status` FROM `cc_tip_transactions` WHERE `dataname` = '[1]' AND `order_id` = '[2]' AND transaction_id='[3]'", $process_info['data_name'], $process_info['order_id'],$transaction_info['transaction_id']);
										$tip_rows = mysqli_num_rows($get_main_tips);

										$resp = "<tr><td align='center'>All card tips have been successfully submitted. Depending on the volume of tips submitted and the processing platform, it may take a few minutes for the transaction adjustments to be completed.<br>Please check the 'Batch Tips Processin' page to view the status of submitted tips.</td></tr>";

										$vars = array();
										$vars['dataname'] = $process_info['data_name'];
										$vars['order_id'] = $process_info['order_id'];
										$vars['transaction_id'] = $transaction_info['transaction_id'];
										$idatetime = date("Y-m-d H:i:s");
										$locationInfoArray = $location_info;
										if (isset($locationInfoArray['ltg_pizza_info']) ) {
										    unset($locationInfoArray['ltg_pizza_info']);
										}
										$transaction_info_array = array( "location_info" => $locationInfoArray, "integration_info" => $integration_info, "process_info" => $process_info, "transaction_info" => $transaction_info, "tip_amount" => $apply_tip, "last_tip" => $transaction_info['tip_amount'] );
										$vars['transaction_info'] = json_encode($transaction_info_array);
										$vars['status'] = 3; // default in pending -> 3 status
										$vars['created_date'] = $idatetime;
										$vars['updated_date'] = $idatetime;
										$inProcessQueue = FALSE;

										if ($tip_rows < 1)
										{
											mlavu_query("INSERT INTO `cc_tip_transactions` (`dataname`, `order_id`,`transaction_id`, `transaction_info`, `status`, `created_date`, `updated_date`) VALUES ('[dataname]', '[order_id]', '[transaction_id]','[transaction_info]', '[status]', '[created_date]', '[updated_date]')", $vars);
											$inProcessQueue = TRUE;
										}
										else if ($tip_rows == 1)
										{
											$main_tips_data = mysqli_fetch_assoc($get_main_tips);
											if ($main_tips_data['status'] == "pending")
											{
												mlavu_query("UPDATE `cc_tip_transactions` SET `transaction_info` = '[1]', `updated_date` = '[2]' where `transaction_id`='[3]' ", $vars['transaction_info'], $vars['updated_date'], $transaction_info['transaction_id'] );
												$inProcessQueue = TRUE;
											}
										}

										if ($inProcessQueue)
										{
											updateCCRecord($transaction_info['transaction_id'], $apply_tip);
										}
									}
									else
									{
										$set_cardtip = process_capture_or_tip($location_info, $integration_info, $process_info, $transaction_info, $apply_tip, "", TRUE);
									}
								}
							}
							else
							{
								lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $apply_tip, time(), $transaction_info['id']);
							}
						}
						else
						{
							$set_cardtip = $transaction_info['tip_amount'];
							if ($location_info['integrateCC'] == "1")
							{
								$resp .= "<tr><td align='center'>Order #".$transaction_info['order_id']." - ".$transaction_info['card_type']." ...".$transaction_info['card_desc']." - Tip: $apply_tip - Tip unchanged</td></tr>";
							}
						}
					}
					else if ($transaction_count > 1)
					{
						$found_unprocessed_transactions = TRUE;
						$tip_total = 0;
						$cctip_list_array = explode("|", $cctip_list);
						while ($transaction_info = mysqli_fetch_assoc($get_transactions))
						{
							$process_info['username'] = $integration_info['integration1'];
							$process_info['password'] = $integration_info['integration2'];
							$process_info['ioid'] = $transaction_info['ioid'];
							$process_info['order_id'] = $transaction_info['order_id'];
							$process_info['check'] = $transaction_info['check'];
							$process_info['pnref'] = $transaction_info['transaction_id'];

							$use_gateway = $transaction_info['gateway']; // THIS IS ADDED TO TAKE THE GATEWAY FROM CCTRANSACTIONS TABLE RATHER THEN LOCATIONS TABLE
							if (empty($use_gateway))
							{
								$use_gateway = $integration_info['gateway']; // Fall back on location setting if `gateway` was not written to the transaction record
							}

							for ($i = 0; $i < count($cctip_list_array); $i++)
							{
								$cct_id = $cctip_list_array[$i];

								if ($transaction_info['id'] == $cct_id)
								{
									$apply_tip = getvar('set_cctip_'.$cct_id, "na");
									if ($apply_tip != "na")
									{
										$apply_tip = number_format((float)str_replace("$","",$apply_tip), $decimal_places, ".", "");

										if (((float)$apply_tip != (float)$transaction_info['tip_amount']) || ($transaction_info['auth'] == 1)) {
											$a_tip_has_changed = TRUE;
											if ($location_info['integrateCC'] == "1")
											{
												$compare_record = $transaction_info['preauth_id'].$transaction_info['record_no'];
												if (!in_array($compare_record, $used_preauth_ids) || $compare_record=="")
												{
													$used_preauth_ids[] = $compare_record;
													if ($use_gateway == "PayPal" && $transaction_info['transtype'] == "Auth")
													{
														$get_main_tips = mlavu_query("SELECT `id`, `status` FROM `cc_tip_transactions` WHERE `dataname` = '[1]' AND `order_id` = '[2]' AND transaction_id='[3]' ", $process_info['data_name'], $process_info['order_id'], $transaction_info['transaction_id']);
														$tip_rows = mysqli_num_rows($get_main_tips);

														$resp = "<tr><td align='center'>All card tips have been successfully submitted. Depending on the volume of tips submitted and the processing platform, it may take a few minutes for the transaction adjustments to be completed.<br>Please check the 'Batch Tips Processing' page to view the status of submitted tips.</td></tr>";

														$vars = array();
														$vars['dataname'] = $process_info['data_name'];
														$vars['order_id'] = $process_info['order_id'];
														$idatetime = date("Y-m-d H:i:s");
														$locationInfoArray = $location_info;
														if (isset($locationInfoArray['ltg_pizza_info']) ) {
														    unset($locationInfoArray['ltg_pizza_info']);
														}
														$transaction_info_array = array("location_info" => $locationInfoArray, "integration_info" => $integration_info, "process_info" => $process_info, "transaction_info" => $transaction_info, "tip_amount" => $apply_tip,"last_tip"=>$transaction_info['tip_amount']);
														$vars['transaction_info'] = json_encode($transaction_info_array);
														$vars['status'] = 3; // default in pending -> 3 status
														$vars['created_date'] = $idatetime;
														$vars['updated_date'] = $idatetime;
														$vars['transaction_id'] = $transaction_info['transaction_id'];

														$inProcessQueue = FALSE;
														if ($tip_rows < 1)
														{
															mlavu_query("INSERT INTO `cc_tip_transactions` (`dataname`, `order_id`,`transaction_id`, `transaction_info`, `status`, `created_date`, `updated_date`) VALUES ('[dataname]', '[order_id]', '[transaction_id]', '[transaction_info]', '[status]', '[created_date]', '[updated_date]')", $vars);
															$inProcessQueue = TRUE;

														}
														else if ($tip_rows == 1)
														{
															$main_tips_data = mysqli_fetch_assoc($get_main_tips);
															if ($main_tips_data['status'] == "pending")
															{
																mlavu_query("UPDATE `cc_tip_transactions` SET `transaction_info` = '[1]', `updated_date` = '[2]' where transaction_id='[3]'", $vars['transaction_info'], $vars['updated_date'], $transaction_info['transaction_id']);
																$inProcessQueue = TRUE;
															}
														}

														if ($inProcessQueue)
														{
															updateCCRecord($transaction_info['transaction_id'],$apply_tip);
														}
													}
													else
													{
														$tip_total += process_capture_or_tip($location_info, $integration_info, $process_info, $transaction_info, $apply_tip, "", TRUE);
													}
												}
											}
											else
											{
												$tip_total += $apply_tip;
												lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $apply_tip, time(), $transaction_info['id']);
											}
										}
										else
										{
											$tip_total += $transaction_info['tip_amount'];
											if ($location_info['integrateCC'] == "1")
											{
												$resp .= "<tr><td align='center'>Order #".$transaction_info['order_id']." - ".$transaction_info['card_type']." ...".$transaction_info['card_desc']." - Tip: $apply_tip - Tip unchanged</td></tr>";
											}
										}
									}
								}
							}
						}

						$set_cardtip = $tip_total;
					}

					if ($a_tip_has_changed)
					{
						$where_clause = " `order_id` = '[4]' AND `location_id` = '[5]'";
						$where_identifier = $id_parts[0];
						if (!empty($id_parts[1]))
						{
							$where_clause = " `ioid` = '[4]'";
							$where_identifier = $id_parts[1];
						}

						lavu_query("UPDATE `orders` SET `card_gratuity` = '[1]', `last_modified` = '[2]', `last_mod_device` = 'MANAGE TIPS', `last_mod_ts` = '[3]', `pushed_ts` = '[3]' WHERE ".$where_clause, str_replace("'","''", number_format((float)$set_cardtip, $decimal_places, ".", "")), currentLocalTime(), time(), $where_identifier, $loc_id);
					}

					switch ($error_code)
					{
						case 1:

							$resp .= "<tr><td align='center'>Error: Failed to load order ID $error_info...</td></tr>";
							$error_encountered = TRUE;
							break;

						case 2:

							$resp .= "<tr><td align='center'>Error: Failed to load transactions for order ID $error_info...</td></tr>";
							$error_encountered = TRUE;
							break;

						case 3:

							$resp .= "<tr><td align='center'>Error: Unrecognized payment gateway...</td></tr>";
							$error_encountered = TRUE;
							break;

						case 4:

							$resp .= "<tr><td align='center'>Gateway error: $error_info...</td></tr>";
							$error_encountered = TRUE;
							break;

						default:
							break;
					}
				}
				else if ($set_cardtip != "")
				{
					$resp .= "<tr><td>Error: Invalid card tip value \"".$set_cardtip."\" for Order ID ".$id_parts[0]."</td></tr>";
					$error_encountered = TRUE;
				}
			}
			else
			{
				$resp .= "<tr><td>Error: Invalid card tip value \"".$set_cardtip."\" for Order ID ".$id_parts[0]."</td></tr>";
				$error_encountered = TRUE;
			}
		}
		shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $server_id $loc_id $data_name > /dev/null 2>/dev/null &");
		if (!$found_unprocessed_transactions && ($location_info['integrateCC'] == "1"))
		{
			$resp .= "<tr><td align='center'>All the transactions listed above have been marked as settled so no tip adjustments have been sent to the gateway.</td></tr>
				<tr><td align='center'>Tip adjustments may possibly still be performed from the gateway's virtual terminal.</td></tr>
				<tr><td align='center'>&nbsp;</td></tr>";
		}
		else if (!$error_encountered)
		{
			$resp .= "<tr><td align='center'>All card tips successfully updated with no errors.</td></tr>";
		}
		else if ($error_encountered)
		{
			$resp .= "<tr><td align='center'>One or more errors occurred while trying to update tip amounts. Please take note.</td></tr>";

			$compare_version = (int)str_pad(str_replace(".", "", $poslavu_version), 4, "0", STR_PAD_RIGHT);
			if ($compare_version >= 1994)
			{
				$send_alert = "<script language='javascript'>window.location = '_DO:cmd=alert&title=Error&message=One or more errors occurred while trying to update tip amounts. Please view details at the bottom of this page.'</script>";
			}
		}
	}

	$display .= "<form id='manage_tips_form' name='manage_tips_form' method='post' action='".manageTipsTargetURL()."'>";

	$server_filter = $show_all_servers?"":" AND `or1`.`server_id` = '$server_id'";

	$where_and_order_by =  " WHERE `or1`.`".$opened_or_closed."` >= '".$start_datetime."' AND `or1`.`".$opened_or_closed."` <= '".$end_datetime."' AND `or1`.`void` = '0' AND `or1`.`location_id` = '[1]'".$server_filter." AND `or1`.`order_id` NOT LIKE '777%' GROUP BY `or1`.`order_id` ORDER BY `or1`.`".$opened_or_closed."` $sortOrderBy";

	$limit_int = 0;
	$limit_page = (int)reqvar("limit_page", "0");
	$limit_clause = "";
	$paging_code = "";

	$limit_int = 0;
	if ($location_info['manage_tips_orders_per_page'])
	{
		$mttpp = (int)$location_info['manage_tips_orders_per_page'];
		if ($mttpp==10 || $mttpp==25)
		{
			$limit_int = $mttpp;
		}
	}

	// if paging is enabled
	if ($limit_int > 0)
	{
		$limit_page = MAX($limit_page, 1);
		$limit_clause = " LIMIT ".(($limit_page - 1) * $limit_int).", ".$limit_int;

		$get_total_count = lavu_query("SELECT `id` FROM `orders` AS `or1`".$where_and_order_by, $loc_id);
		$total_count = mysqli_num_rows($get_total_count);

		if ($total_count > $limit_int)
		{
			$total_pages = ceil($total_count / $limit_int);

			$paging_code .= "<script language='javascript'>
				function setLimitPage(page) {
					document.getElementById('limit_page').value = page;
					document.getElementById('do_not_post').value = '1';
					submitTips();
				}
			</script>";
			$paging_code .= "<tr><td align='center'><br>";
			$paging_code .= "<table>";
			$paging_code .= "<tr>";
			$paging_code .= "<td width='50px' align='right'>";
			if ($limit_page > 1)
			{
				$paging_code .= "<img src='images/btn_green_previous.png' ontouchstart='setLimitPage(".($limit_page - 1).");'>";
			}
			$paging_code .= "</td>";
			$paging_code .= "<td align='center' valign='middle' class='paging_text'> Page ".$limit_page." of ".$total_pages." </td>";
			$paging_code .= "<td width='50px' align='left'>";
			if ($total_pages > $limit_page)
			{
				$paging_code .= "<img src='images/btn_green_next.png' ontouchstart='setLimitPage(".($limit_page + 1).");'>";
			}
			$paging_code .= "</td>";
			$paging_code .= "</tr>";
			$paging_code .= "</table>";
			$paging_code .= "</td></tr>";
		}
	}

	$order_query = lavu_query("SELECT `cc`.`gateway`,
if(count(*) > 0, sum(IF(`cc`.`transtype`='Return', -1*`cc`.`tip_amount`, `cc`.`tip_amount`)), '0.00') as tip_amount, `or1`.* FROM `orders` as `or1` JOIN `cc_transactions` as `cc` on `or1`.`order_id` = `cc`.`order_id`".$where_and_order_by.$limit_clause, $loc_id);
	if (@mysqli_num_rows($order_query) > 0)
	{
		$display .= "<tr>
			<td align='center'>
				<table>
					<tr>
						<td align='center'>";

		if ($cash_tip_mode==1 && !$show_all_servers)
		{
			$display .= "Today's Cash Tips: <input type='tel'".use_number_pad_if_available("todays_cash_tips", "Today's Cash Tips", "money")." style='text-align:center; font-size:16px' id='todays_cash_tips' name='todays_cash_tips' value='$cash_tip_str' size='10'><br><br>";
		}

		$display .= "<table cellspacing='0' cellpadding='2'>
									<tr>
										<td class='ttop'>".speak("Order ID")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("Opened")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("Table")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("SubTotal")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("Tax")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("Total")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("Due")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("Cash Paid")."</td><td class='ttop'>&nbsp;</td>";
		if ($cash_tip_mode == 0)
		{
			$display .= "<td class='ttop'>".speak("Cash Tip")."</td><td class='ttop'>&nbsp;</td>";
		}

		$display .= "<td class='ttop'>".speak("Card Paid")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("Card Tip")."</td><td class='ttop'>&nbsp;</td>
										<td class='ttop'>".speak("Card Total")."</td><td class='ttop'>&nbsp;</td>
									</tr>";

		$t_subtotal			= 0;
		$t_discount			= 0;
		$t_gift_certificate	= 0;
		$t_auto_grat		= 0;
		$t_tax				= 0;
		$t_itax				= 0;
		$t_total			= 0;
		$t_cash				= 0;
		$t_cash_tip			= 0;
		$t_card				= 0;
		$t_due				= 0;
		$t_card_tip			= 0;
		$t_card_total		= 0;

		$cashtipjs_list	= "";
		$midjs_list		= "";
		$midjs			= "";
		$cctjs_list		= "";
		$hidden_fields	= "";

		$row_count = 0;

		while ($order_read = mysqli_fetch_assoc($order_query))
		{
			$row_count++;

			$show_multiple_ccts = FALSE;
			$show_single_cct = FALSE;
			$method_paid = "";
			$order_identifier = $order_read['order_id']."_".$order_read['ioid'];

			if ($order_read['cash_paid'] > 0)
			{
				$method_paid .= "Cash";
			}

			if ($order_read['card_paid'] > 0)
			{
				if ($method_paid != "")
				{
					$method_paid .= ", ";
				}
				$method_paid .= "Card";
			}

			if (isset($order_read['cash_applied']) && (string)$order_read['cash_applied'] != "")
			{
				$cash_applied = ($order_read['cash_applied'] * 1);
			}
			else if (isset($order_read['cash_paid']))
			{
				$cash_applied = ($order_read['cash_paid'] * 1);
			}

			$alt_paid_total = 0;
			$alt_paid_amounts = array();
			$alt_paid_amounts[] = array(speak("Gift Certificate"), display_price($order_read['gift_certificate'], $decimal_places));
			$alt_paid_grand_totals[0] += $order_read['gift_certificate'];
			for ($ap = 1; $ap < count($alt_payment_types); $ap++)
			{
				$ptype = $alt_payment_types[$ap];
				$this_total = get_alt_total($loc_id, $order_read['order_id'], $ptype[0]);
				$alt_paid_total += $this_total;
				$alt_paid_amounts[] = array($ptype[1], display_price($this_total, $decimal_places));
				$alt_paid_grand_totals[$ap] += $this_total;
			}

			$amount_due = $order_read['total'] - $order_read['gift_certificate'] - $order_read['card_paid'] - $cash_applied - $alt_paid_total;
			if (($amount_due > (0 - ($smallest_money / 2))) && ($amount_due < ($smallest_money / 2)))
			{
				$amount_due = 0;
			}

			$set_cash_tip = number_format((float)$order_read['cash_tip'], $decimal_places, ".", "");
			if ($set_cash_tip == "")
			{
				$set_cash_tip = 0;
			}

			$set_card_tip = number_format((float)$order_read['card_gratuity'], $decimal_places, ".", "");

			if($set_card_tip=="")
			{
				$set_card_tip = 0;
			}

			$t_subtotal			+= $order_read['subtotal'];
			$t_discount			+= $order_read['discount'];
			$t_tax				+= $order_read['tax'];
			$t_itax				+= $order_read['itax'];
			$t_total			+= $order_read['total'];
			$t_due				+= $amount_due;
			$t_gift_certificate	+= $order_read['gift_certificate'];
			$t_cash				+= ($order_read['cash_paid'] - $order_read['change_amount']);
			$t_cash_tip			+= $set_cash_tip;
			if (($order_read['gateway'] == 'PayPal' || $order_read['gateway'] == '') && $order_read['alt_paid'] != '0.00') {
				$t_card                         += $order_read['alt_paid'] + $order_read['card_paid'];
				$t_card_tip                     += $set_card_tip;
				$t_card_total           += $order_read['alt_paid'] + $order_read['card_paid'] + $set_card_tip;
			} else {
				$t_card				+= $order_read['card_paid'];
				$t_card_tip			+= $set_card_tip;
				$t_card_total		+= $order_read['card_paid'] + $set_card_tip;
			}
			$t_auto_grat		+= $order_read['gratuity'];

			$gift_color = ($order_read['gift_certificate'] > 0)?"#007841":"#bbbbbb";
			$cash_color = ($order_read['cash_paid'] > 0)?"#00aa00":"#bbbbbb";
			$card_color = ($order_read['card_paid'] > 0)?"#00aa00":"#bbbbbb";

			$display_gift_certificate	= display_price($order_read['gift_certificate'], $decimal_places);
			$display_cash_paid			= display_price($cash_applied, $decimal_places);
			if (($order_read['gateway'] == 'PayPal' || $order_read['gateway'] == '') && $order_read['alt_paid'] != '0.00') {
				$display_card_paid                      = display_price($order_read['alt_paid'] + $order_read['card_paid'], $decimal_places);
				$display_card_total                     = display_price(($order_read['alt_paid'] + $order_read['card_paid'] + $set_card_tip), $decimal_places);
			} else {
				$display_card_paid			= display_price($order_read['card_paid'], $decimal_places);
				$display_card_total			= display_price(($order_read['card_paid'] + $set_card_tip), $decimal_places);
			}
			$display_gift_certificate	= "<font color='$gift_color'>$display_gift_certificate</font>";
			$display_cash_paid			= "<font color='$cash_color'>$display_cash_paid</font>";
			$display_card_paid			= "<font color='$card_color'>$display_card_paid</font>";
			$display_card_total			= "<font color='$card_color'>$display_card_total</font>";

			$row_color = (($row_count % 2) == 0)?"#EDEDED":"#FFFFFF";
			$display .= "<tr bgcolor='".$row_color."'>
				<td class='info_text' align='center'>" . $order_read['order_id'] . "</td><td>&nbsp;</td>
				<td class='info_text' align='right'>" . display_time_($order_read['opened']) . "</td><td>&nbsp;</td>
				<td class='info_text'>" . $order_read['tablename'] . "</td><td>&nbsp;</td>
				<td class='info_text' align='right'><font color='#000055'>" . display_price($order_read['subtotal'], $decimal_places) . "</font></td><td>&nbsp;</td>
				<td class='info_text' align='right'><font color='#005500'>" . display_price($order_read['tax'], $decimal_places) . "</font></td><td>&nbsp;</td>
				<td class='info_text' align='right'><font color='#0000aa'>" . display_price($order_read['total'], $decimal_places) . "</font></td><td>&nbsp;</td>
				<td class='info_text' align='right'>";

			$font_color = ($amount_due > 0)?"#dd0000":"#aaaaaa";
			$cash_tip_value = "";
			if ((float)$set_cash_tip >= $smallest_money)
			{
				$cash_tip_value = $set_cash_tip;
			}

			$display .= "<font color='$font_color'>" . display_price($amount_due, $decimal_places) . "</font>
				</td>
				<td>&nbsp;</td>
				<td class='info_text' align='right'>" . $display_cash_paid . "</td><td>&nbsp;</td>";
			if ($cash_tip_mode == 0)
			{
				$display .= "<td class='info_text' align='right' colspan='2'><input type='tel'".use_number_pad_if_available("input_cash_tip_".$order_identifier, "Cash Tip", "money")." size='6' style='text-align:right; font-size:16px' id='input_cash_tip_$order_identifier' name='set_cashtip_$order_identifier' value='$cash_tip_value' onfocus='if(this.value==\"$0.00\" || this.value==\"0.00\" || this.value==\"0\") this.value = \"\"; '></td>";
			}
			$display .= "<td class='info_text' align='right'>" . $display_card_paid . "</td><td>&nbsp;</td>";

			if ($cashtipjs_list != "")
			{
				$cashtipjs_list .= "|";
			}
			$cashtipjs_list .= "$order_identifier";

			$check_got_response = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";

			$where_clause = " `order_id` = '[1]' AND `loc_id` = '[2]'";
			$where_identifier = $order_read['order_id'];
			if (!empty($order_read['ioid']))
			{
				$where_clause = 	" `ioid` = '[1]'";
				$where_identifier = $order_read['ioid'];
			}

			$check_cct = lavu_query("SELECT * FROM `cc_transactions` WHERE ".$where_clause." AND `pay_type_id` IN ('".implode("','", $cc_pay_type_ids)."') AND (`action` = 'Sale' OR `action` = 'Refund') AND `voided` != '1'$check_got_response AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $where_identifier, $loc_id);
			// create an array of all records
			while ($check = mysqli_fetch_assoc($check_cct))
			{
				$all_check_ccts[] = $check;
			}

			// reset the pointer back so we can still access this
			mysqli_data_seek($check_cct, 0);

			if (@mysqli_num_rows($check_cct) > 1)
			{
				$show_multiple_ccts = TRUE;
				$display .= "<td class='info_text' align='right'>$".$set_card_tip."</td><td>&nbsp;</td>";
				$hidden_fields .= "<input type='hidden' name='set_cardtip_$order_identifier' value='multi'>";
				if ($midjs_list != "")
				{
					$midjs_list .= "|";
				}
				$midjs_list .= "$order_identifier";
			}
			else if ((@mysqli_num_rows($check_cct) == 1) || ($order_read['card_paid'] > 0))
			{
				$cct_info	= mysqli_fetch_assoc($check_cct);
				$gateway	= $cct_info['gateway'];
				$auth		= $cct_info['auth'];
				$processed	= $cct_info['processed'];

				$show_single_cct = TRUE;
				$display .= "<td class='info_text' align='right' colspan='2'>";
				if ($processed=="1" || $auth==2 || in_array($gateway, array("iZettle", "MIT", "Moneris","Square")))
				{
					$display .= "$".$set_card_tip;
				}
				else
				{
					$card_tip_value = "";
					if ((float)$set_card_tip >= $smallest_money)
					{
						$card_tip_value = $set_card_tip;
					}

					// check for a matching refund for this transaction
					$foundMatchingRefund = findSaleWithRefund($all_check_ccts, $cct_info);
					if ($foundMatchingRefund)
					{
						// if the sale was refunded, show uneditable label
						$display .= "<span class='cct_info'>$".number_format((float)$cct_info['tip_amount'], $decimal_places, ".", "")."</span>";
					}
					else
					{
						// shoe editable textfield

						// if the gateway is eConduit and the iFrame flag is set, we want to submit a tip adjustment
						$iFrameTipAdjust	= useIFrameTipAdjust($gateway);
						$tipAdjustCallback	= !$iFrameTipAdjust ? "setFieldValue" : "loadEconduitTipAdjustOrCapture";
						$econduit_command	= ($auth == "0")?"tipadjust":"capture";

						$extraArgs = array(
							$cct_info['ref_data'],
							$cct_info['transaction_id'],
							$cct_info['id'],
							$cct_info['amount'],
							$order_read['ioid'],
							currentLocalTime(),
							$loc_id,
							$data_name,
							$set_card_tip,
							$econduit_command
						);
						$display .= "<input type='tel'".use_number_pad_if_available("input_card_tip_".$order_identifier, "Card Tip", "money", $tipAdjustCallback, $extraArgs)." size='6' style='text-align:right; font-size:16px' id='input_card_tip_$order_identifier' name='set_cardtip_$order_identifier' value='$card_tip_value' onfocus='if(this.value==\"$0.00\" || this.value==\"0.00\" || this.value==\"0\") this.value = \"\"; '>";
					}

					if ($midjs_list!="")
					{
						$midjs_list .= "|";
					}
					$midjs_list .= "$order_identifier";
				}
				$display .= "</td>";
			}
			else
			{
				if ($order_read['gateway'] == 'PayPal' && $order_read['alt_paid'] != '0.00') {
					$display .= "<td class='info_text' align='center'>$".number_format((float)$order_read['tip_amount'], $decimal_places, ".", "")."</td><td>&nbsp;</td>";
				} else {
					$display .= "<td class='info_text' align='center'>-</td><td>&nbsp;</td>";
				}
			}

			$display .= "<td class='info_text' id='show_card_total_".$order_identifier."' align='right'>".$display_card_total."</td><td>&nbsp;</td>
			</tr>";

			if (mysqli_num_rows($check_cct))
			{
				mysqli_data_seek($check_cct, 0);
			}

			if ($show_multiple_ccts || $show_single_cct)
			{
				$colspan = ($cash_tip_mode == 1)?20:18;
				$display .= "<tr bgcolor='".$row_color."'>
					<td colspan='1'>&nbsp</td>
					<td align='right' colspan='".$colspan."'>
						<table cellspacing='0' cellpadding='2'>";

				while ($extract = mysqli_fetch_array($check_cct))
				{
					$gateway	= $extract['gateway'];
					$auth		= $extract['auth'];
					$action		= $extract['action'];
					$processed	= $extract['processed'];
                    $tip_value  = number_format((float)$extract['tip_amount'], $decimal_places, ".", "");

					$display .= "<tr>";
					$first_column_content = FALSE;

					if ($auth == "2")
					{
						$first_column_content = TRUE;
						$display .= "<td class='cct_labels'><font color='#FF3300'>AuthForTab</font></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
					}

					if ($action == "Refund")
					{
						$display .= "<td class='cct_labels'><font color='#C90000'>REFUND</font></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>";
					}
					else if (!$first_column_content)
					{
						$display .= "<td></td><td></td>";
					}

					$display .= "<td class='cct_labels'>".(!empty($extract['card_type'])?$extract['card_type']." ...":speak("Card Transaction"))."<span class='cct_info'>".(!empty($extract['card_desc'])?$extract['card_desc']:"")."</span></td><td width='10'></td>";
					$display .= (!empty($extract['transaction_id'])?"<td class='cct_labels'>".speak("Ref #").": <span class='cct_info'>".$extract['transaction_id']."</span></td><td width='10'></td>":"");
					$display .= (!empty($extract['auth_code'])?"<td class='cct_labels'>".speak("Auth Code").": <span class='cct_info'>".$extract['auth_code']."</span></td><td width='10'></td>":"<td></td><td></td>");
					$display .= "<td class='cct_labels' width='100px'>".speak("Amount").": <span class='cct_info'>$".number_format($extract['amount'], 2)."</span></td><td width='1'></td>";

					$label = ($action == "Refund")?speak("Refunded Tip"):speak("Tip");
					if ($show_multiple_ccts && ((($processed=="1" || $auth=="2" || in_array($gateway, array("iZettle", "MIT", "Moneris"))) && $action=="Sale") || $action=="Refund"))
					{
                        $display .= "<td class='cct_labels'>".$label.": <span class='cct_info'>$".$tip_value."</span></td>";
                    }
					else if ($show_multiple_ccts && $processed=="0" && $action=="Sale")
					{
						// check for a matching refund for this transaction
						$foundMatchingRefund = findSaleWithRefund($all_check_ccts, $extract);
						if ($foundMatchingRefund)
						{
							// if the sale was refunded, show uneditable label
							$display .= "<td class='cct_labels'>".$label.": <span class='cct_info'>$".$tip_value."</span></td>";
						}
						else
						{
							// show editable textfield

							// if the gateway is eConduit and the iFrame flag is set, we want to submit a tip adjustment using iFrame
							$iFrameTipAdjust	= useIFrameTipAdjust($gateway);
							$tipAdjustCallback	= !$iFrameTipAdjust ? "setFieldValue" : "loadEconduitTipAdjustOrCapture";
							$econduit_command	= ($auth == "0")?"tipadjust":"capture";

							$extraArgs = array(
								$extract['ref_data'],
								$extract['transaction_id'],
								$extract['id'],
								$extract['amount'],
								$order_read['ioid'],
								currentLocalTime(),
								$loc_id,
								$data_name,
								$tip_value,
								$econduit_command
							);

							$display .= "<td class='cct_labels'>".speak("Tip").": <input type='tel'".use_number_pad_if_available("input_cctip_".$extract['id'], "Card Tip", "money", $tipAdjustCallback, $extraArgs)." size='6' style='text-align:right; font-size:16px' id='input_cctip_".$extract['id']."' name='set_cctip_".$extract['id']."' onfocus='if(this.value==\"$0.00\" || this.value==\"0.00\" || this.value==\"0\") this.value = \"\"; ' value='".$tip_value."'></td>";
						}

						if ($cctjs_list != "")
						{
							$cctjs_list .= "|";
						}
						$cctjs_list .= $extract['id'];
					}
					else
					{
						$display .= "<td class='cct_labels'>".$label.": <span class='cct_info'>$".$tip_value."</span></td>";
					}

					$display .= "</tr>";
				}

				$display .= "</table>
					</td><td colspan='4'>&nbsp;</td>
				</tr>";
			}
		}

		$display .= "<tr>
			<td class='tbot' colspan='5' align='right'>TOTALS:</td><td class='tbot'>&nbsp;</td>
			<td class='tbot'>".display_price($t_subtotal, $decimal_places)."</td><td class='tbot'>&nbsp;</td>
			<td class='tbot'>".display_price($t_tax, $decimal_places)."</td><td class='tbot'>&nbsp;</td>
			<td class='tbot'>".display_price($t_total, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";

		$font_color = ($t_due > 0)?"#dd0000":"#000000";

		$display .= "<td class='tbot'><font color='$font_color'>".display_price($t_due, $decimal_places)."</font></td><td class='tbot'>&nbsp;</td>
				<td class='tbot'>".display_price($t_cash, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		if ($cash_tip_mode == 0)
		{
			$display .= "<td class='tbot'>".display_price($t_cash_tip, $decimal_places)."</td><td class='tbot'>&nbsp;</td>";
		}
		$display .= "<td class='tbot'>".display_price($t_card, $decimal_places)."</td><td class='tbot'>&nbsp;</td>
				<td class='tbot'>".display_price($t_card_tip, $decimal_places)."</td><td class='tbot'>&nbsp;</td>
				<td class='tbot' id='show_card_total'>".display_price($t_card_total, $decimal_places)."</td><td class='tbot'>&nbsp;</td>
			</tr>
		</table>";

		if ($native_bg == "0")
		{
			$submit_click_code = "document.getElementById(\"manage_tips_form\").submit(); this.onclick=\"\";";
			$display .= "<table width='100%'>
				<tr>
					<td align='right'><br><br><img src='images/btn_refresh.png' onclick='refreshPage()'></td>
					<td style='padding:1px 20px 1px 20px'>&nbsp;</td>
					<td align='left'><br><br><img src='images/btn_save_changes.png' onclick='$submit_click_code'></td>
				</tr>
			</table>";
		}
		$display .= "</td>
					</tr>
				</table>
			</td>
		</tr>";
	}
	else
	{
		$display .= "<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td align='center'>".($show_all_servers?speak("No orders found for this day")."...":speak("You have no orders for today")."...")."</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>";
	}

	$dev_flag = reqvar("is_dev", "0");

	if ($cash_tip_mode == 0)
	{
		$hidden_fields .= "<input type='hidden' name='cashtip_list' value='$cashtipjs_list'>";
	}
	$hidden_fields .= "<input type='hidden' name='cardtip_list' value='$midjs_list'>
		<input type='hidden' name='cctip_list' value='$cctjs_list'>
		<input type='hidden' name='cc' value='$cc'>
		<input type='hidden' name='loc_id' value='$loc_id'>
		<input type='hidden' name='server_id' value='$server_id'>
		<input type='hidden' name='register' value='$register'>
		<input type='hidden' name='UUID' value='".$device_id."'>
		<input type='hidden' name='app_name' value='".$app_name."'>
		<input type='hidden' name='app_version' value='".$app_version."'>
		<input type='hidden' name='app_build' value='".$app_build."'>
		<input type='hidden' name='is_dev' value='".$dev_flag."'>
		<input type='hidden' name='available_DOcmds' value='".implode(",", $available_DOcmds)."'>
		<input type='hidden' id='show_all_servers' name='show_all_servers' value='".($show_all_servers?"1":"0")."'>
		<input type='hidden' id='day_adj' name='day_adj' value='".$day_adj."'>
		<input type='hidden' id='native_bg' name='native_bg' value='".$native_bg."'>
		<input type='hidden' id='do_not_post' name='do_not_post' value='0'>
		<input type='hidden' id='opened_or_closed' name='opened_or_closed' value='".$opened_or_closed."'>
		<input type='hidden' id='limit_page' name='limit_page' value='".$limit_page."'>";

	$display .= $hidden_fields."</form>";

	$lls = (in_array($location_info['use_net_path'], array("1", "2")) && !strstr($location_info['net_path'], "poslavu.com") && !strstr($location_info['net_path'], "lavulite.com"));
	// may decide to allow for non-LLS clients

	if (!$show_all_servers && $lls)
	{
		$get_server_info = lavu_query("SELECT `access_level`, `PIN` FROM `users` WHERE `id` = '[1]'", $server_id);
		if (mysqli_num_rows($get_server_info) > 0)
		{
			$server_info = mysqli_fetch_assoc($get_server_info);
			if ($server_info['access_level']>=2 && !empty($server_info['PIN']))
			{
				$resp .= "<script language='javascript'>
					function checkPINforAllServers() {
						if (document.getElementById('pin_for_all_servers').value == '".$server_info['PIN']."') {
								document.getElementById('show_all_servers').value = '1';
								document.getElementById('do_not_post').value = '1';
								submitTips();
						} else window.location = '_DO:cmd=alert&message=Invalid PIN. Please try again.';
					}
				</script>
				<tr><td>&nbsp;</td></tr>
				<tr><td align='center' style='padding-bottom:7px'>Enter your PIN to edit tips for all servers:</td></tr>
				<tr><td align='center'><input id='pin_for_all_servers' type='tel'".use_number_pad_if_available("pin_for_all_servers", "Enter Your PIN", "PIN")." size='8' style='text-align:center; font-size:16px'> <input type='button' style='font-size:16px' value=' &nbsp;Go&nbsp; ' onclick='checkPINforAllServers();'></td></tr>";
			}
		}
	}

	function ensureCashTipsInsertionForCCTransaction()
	{
		global $use_date, $todays_cash_tips, $location_info, $server_id;
		global $data_name, $register, $register_name;
		/*Added by Leif G*/
		require_once(__DIR__."/Orders777.php");
		$order777 = new Orders777($location_info['cash_tips_as_daily_total'], $data_name, $register, $register_name, $server_id, $use_date, $location_info, $todays_cash_tips);
		$order777->ensure777CashTipsOrderInsertion();
		/*End of adding things by Leif G.*/
	}
	
	function getSquareInstructions(){
		global $data_name;
		$query = mlavu_query("select id from restaurants where data_name='".$data_name."' and `modules` like '%extensions.payment.+square%'");
		if(mysqli_num_rows($query)>0){
			return "All Card Tips must be entered in the Square POS application. Please open the Square app, navigate to the Transactions page, and enter the applicable Card Tip for each transaction.";
		}else{
			return "";
		}
	}

	function displaySortOrderList($sortOrderBy, $dbname) {
		global $server_id;

		$sdisplay = "<tr><td><select id='select-ordering' name='select_sort_order' style='width:150px;height:30px;margin-top:7px;' onchange='changeOrdering(this.value, ".$server_id.", \"".$dbname."\")'>";
		$sdisplay .= "<option value='' disabled='disabled'>".speak("Sort orders by")."</option>";
		$selected = "selected";

		if (isset($sortOrderBy) && $sortOrderBy == 'DESC') {
			$sdisplay .= "<option value='DESC' $selected>".speak("Most Recent")."</option>";
		} else {
			$sdisplay .= "<option value='DESC'>".speak("Most Recent")."</option>";
		}
		if (isset($sortOrderBy) && $sortOrderBy == 'ASC') {
			$sdisplay .= "<option value='ASC' $selected>".speak("Oldest")."</option>";
		} else {
			$sdisplay .= "<option value='ASC'>".speak("Oldest")."</option>";
		}
		$sdisplay .= "</select></td></tr>";

		return $sdisplay;
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Manage Tips</title>
		<style>
			body { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; }
			.cct_labels { color:#999999; font-size:11px; }
			.cct_info { color:#666666; font-size:11px; }
			.error_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#CC0000; }
			.info_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:11px; }
			.paging_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:19px; padding:3px 15px 3px 15px; }
<?php
	if ($native_bg == "0")
	{
		echo ".mt_panel_top { background-image:url(\"images/mt_panel_top.png\"); background-repeat:no-repeat; width:980px; height:30px; }";
		echo ".mt_panel_mid { background-image:url(\"images/mt_panel_mid.png\"); background-repeat:repeat-y; width:980px; }";
		echo ".mt_panel_bottom { background-image:url(\"images/mt_panel_bottom.png\"); background-repeat:no-repeat; width:980px; height:30px; }";
	}
	else
	{
		echo ".mt_panel_top { width:984px; height:30px; }";
		echo ".mt_panel_mid { width:984px; }";
		echo ".mt_panel_bottom { width:984px; height:30px; }";
	}
?>
			.small_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#333333; }
			.tbot { border-top:solid 2px #777777; text-align:right; font-weight:bold; }
			.title_text { font-family:Verdana, Arial, Helvetica, sans-serif; font-size:17px; color:#000000; }
			.ttop { border-bottom:solid 2px #777777; text-align:center; font-weight:bold; }

			.btn_light_long {
				color:#7D7D7D;
				font-family:Arial, Helvetica, sans-serif;
				font-size:13px;
				width:200px;
				height:37px;
				background:url("images/btn_wow_200x37.png");
				border:hidden;
				padding-top:2px;
			}

			.iFrameOverlay {
				display:none;
				position:fixed;
				top:0;
				left:0;
				width:100%;
				height:100%;
				background-color:#EFEFEF;
				opacity:0.99;
			}

		</style>
		<script language='javascript'>
			function refreshPage() {
				window.location = '?<?php echo "cc=".$cc."&loc_id=".$loc_id."&show_all_servers=".($show_all_servers?"1":"0")."&do_not_post=".($should_post?"0":"1")."&day_adj=".$day_adj."&opened_or_closed=".$opened_or_closed."&native_bg=".$native_bg."&limit_page=".$limit_page."&orderBy=".$sortOrderBy; ?>';
			}

			function changeOrdering(oid, serverid, dataname) {
				var url = 'update_users_row.php?user_id=' + serverid + '&selected_option=' + oid + '&cc=' + dataname;
				var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
				xhr.open('GET', url, true);
				xhr.onreadystatechange = function () {
					if (xhr.readyState > 3 && xhr.status == 200) {
						if (xhr.responseText == 'True') {
							window.location = '?<?php echo "cc=".$cc."&loc_id=".$loc_id."&show_all_servers=".($show_all_servers?"1":"0")."&do_not_post=".($should_post?"0":"1")."&day_adj=".$day_adj."&opened_or_closed=".$opened_or_closed."&native_bg=".$native_bg."&limit_page=".$limit_page."&orderBy="; ?>'+ oid;
							return true;
						}
					}
				};
				xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
				xhr.send();
			}
			function submitTips() {
				document.manage_tips_form.submit();
			}

			function setOpenedOrClosed(ooc) {
				document.getElementById('do_not_post').value = '1';
				document.getElementById('opened_or_closed').value = (ooc == 'opened')?'closed':'opened';
				document.getElementById('limit_page').value = '1';
				submitTips();
			}
		</script>
		<script src="../cp/scripts/ajax_prototype.js"></script>
		<script src="../components/payment_extensions/eConduit/eConduitTipAdjust.js"></script>
		<?php echo $webspefun; ?>

	</head>

	<body>
		<center>
			<div id='eConduitOverlay' class='iFrameOverlay'>
				<button class='btn_light_long' ontouchstart='hideActivityShade();'>
					<b>Close Overlay</b>
				</button>
			</div>
			<table cellspacing='0' cellpadding='0' width='<?php echo ($native_bg == "0")?"980":"986"; ?>px'>
				<?php echo $sortingListDisplay; ?>
				<tr><td>&nbsp;</td></tr>
				<tr><td class='mt_panel_top'></td></tr>
				<tr>
					<td class='mt_panel_mid' align='center'>
						<table cellspacing='0' cellpadding='3' width='<?php echo ($native_bg == "0")?"960":"966"; ?>px'>
							<?php echo $upper_display.$paging_code.$display; ?>
							<tr><td>&nbsp;</td></tr>
							<?php echo $resp; ?>
							<tr><td>&nbsp;</td></tr>
							<tr><td align='center'><?php echo getSquareInstructions();?></td></tr>
						</table>
					</td>
				</tr>
				<tr><td class='mt_panel_bottom'></td></tr>
			</table>
		</center>
	</body>
	<?php
		if ($send_alert != "")
		{
			echo $send_alert;
		}
	?>
</html>
