<?php
if (!isset($_REQUEST['cc'])) $_REQUEST['cc'] = $_REQUEST['dn'];

$dev = (strstr(dirname(__FILE__),"/dev/")) ? TRUE : FALSE;
if ($dev)
	ini_set("display_errors","1");

if (!isset($dontprint)) {
	session_start();
	require_once(dirname(__FILE__) ."/info_inc.php");
}
require_once(dirname(__FILE__).'/../'.($dev ? '../' : '').'cp/resources/core_functions.php');


$testing = FALSE;
$tillname = speak("till shift");
$tillsummary = speak("Till Summary");
$till_table = "cash_data";
set_register_server_till();
global $mode;
global $isRegisterTill;

if(isset($_REQUEST['isRegisterTill'])){
	$isRegisterTill = $_REQUEST['isRegisterTill'];
}

global $isDailyTill;
if(isset($_REQUEST['isDailyTill']) && $_REQUEST['isDailyTill'] == TRUE){
	$isDailyTill = TRUE;
}else{
	$isDailyTill = FALSE;
}
global $real_name;
if(isset($_REQUEST['real_name'])){
	$real_name = urldecode($_REQUEST['real_name']);
}
global $printSingleTill;
if(isset($_REQUEST['printSingleTill'])){
	$printSingleTill = $_REQUEST['printSingleTill'];
}

global $drawer;
$drawer = (isset($_REQUEST['drawer']) && $_REQUEST['drawer'] != '') ? $_REQUEST['drawer'] : 0;

if (!isset($dontprint)) {
	$report = get_till_report_to_print($isRegisterTill, $isDailyTill, $drawer);
	echo $report;
}


// temporaarily include to record the number of times locations are using the print functionality from cash_reconciliation/server_reconciliation
	$from_mgmt_area = (isset($_REQUEST['from_mgmt_area']))?$_REQUEST['from_mgmt_area']:"0";
	if ($from_mgmt_area == "1") {
	
		$info = "transaction_summary (".$_REQUEST['ts_access_level'].")";
		$check_previous = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`temp_info` WHERE `type` = 'management_area_usage' AND `dataname` = '[1]' AND `info` = '[2]'", $data_name, $info);
		if (mysqli_num_rows($check_previous) > 0) {
			$info = mysqli_fetch_assoc($check_previous);
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`temp_info` SET `count` = ((`count` * 1) + 1), `datetime` = now() WHERE `id` = '[1]'", $info['id']);
		} else $insert = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`temp_info` (`type`, `dataname`, `datetime`, `count`, `info`) VALUES ('management_area_usage', '[1]', now(), '1', '[2]')", $data_name, $info);
	}
//


// checks for get variable to update the sessvar "till_summary_server_till" with
// updates sessvar "till_summary_server_till", $register, $register_name, $register_id
function set_register_server_till() {
	$b_do_redirect = FALSE;

	// check for get variables to change the register/server till selection
	if (isset($_GET['server_till'])) {

		// adjust the server till and set it in a sessvar
		$i_server_id = (int)$_GET['server_till'];
		$_SESSION['till_summary_server_till'] = $i_server_id;

		// redirect to not include the getvar
		$b_do_redirect = TRUE;
		$a_location_vars = $_GET;
		unset($a_location_vars['server_till']);
		$s_location = http_build_query($a_location_vars, '', '&');
	}

	// check if the mode is "cash_reconciliation" or "server_reconciliation"
	if (isset($_GET['mode'])) {
		if ($_GET['mode'] == 'cash_reconciliation' && get_register_server_till() != 0) {
			$_SESSION['till_summary_server_till'] = 0;
		}
		if ($_GET['mode'] == 'server_reconciliation') {

			// get and set the current user id, if it isn't set already
			$i_active_server_id = (isset($_GET['server_id']) ? (int)$_GET['server_id'] : 0);
			if (get_register_server_till() == 0) {
				if ($i_active_server_id !== 0) {
					$_SESSION['till_summary_server_till'] = $i_active_server_id;
				}
			}

			// check the user's access level
			// if the access level < 4, force the user to use their own server till instead of whatever other server's till they are trying to access
			// resets the current user id
			if ($i_active_server_id !== get_register_server_till()) {
				$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('users', array('active'=>'1', '_deleted'=>'0', 'id'=>$i_active_server_id), FALSE, array('selectclause'=>'`id`,`access_level`,`username`,CONCAT(`f_name`,\' \',`l_name`) AS `fullname`', 'orderbyclause'=>'ORDER BY `fullname`'));
				if (count($a_users) > 0) {
					$b_active_server_is_admin = active_server_is_admin($a_users);
					if (!$b_active_server_is_admin)
						$_SESSION['till_summary_server_till'] = $i_active_server_id;
				}
			}
		}
	}

	// check for a server/register selection
	if (get_register_server_till() !== 0) {

		// get the user
		$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('users', array('id'=>get_register_server_till()), FALSE, array('selectclause'=>'`username`'));
		if (count($a_users) == 0)
			return;
		$a_user = $a_users[0];

		// set the register
		global $register;
		global $register_name;
		global $register_id;
		$register = 'server';
		$register_name = $a_user['username'];
		$register_id = -1;
	}

	if ($b_do_redirect)
		header("Location: ?$s_location");
}
//@return: the currently selected server, or 0 if the register is selected
function get_register_server_till() {
	$i_register_server_id = (isset($_SESSION['till_summary_server_till']) ? (int)$_SESSION['till_summary_server_till'] : 0);
	return $i_register_server_id;
}

// checks if the active server is an admin
// @$a_users: so that the users don't have to be loaded from the db, again
// @return: TRUE if the active server is found and has access level 4, FALSE otherwise
global $active_server_is_admin_users;
$active_server_is_admin_users = NULL;
function active_server_is_admin($a_users = NULL) {

	// get the users
	// return FALSE if the users can't be found
	global $active_server_is_admin_users;
	if ($a_users !== NULL && is_array($a_users))
		$active_server_is_admin_users = $a_users;
	if ($active_server_is_admin_users === NULL) {
		$active_server_is_admin_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('users', array('active'=>'1', '_deleted'=>'0'), FALSE, array('selectclause'=>'`id`,`access_level`', 'orderbyclause'=>'ORDER BY `fullname`'));
	}
	if (count($active_server_is_admin_users) == 0)
		return FALSE;

	// get the current user id
	$i_active_server_id = (isset($_GET['server_id']) ? (int)$_GET['server_id'] : 0);

	// check if the access level == 4
	foreach($active_server_is_admin_users as $a_user) {
		if ((int)$a_user['id'] == $i_active_server_id)
			if ((int)$a_user['access_level'] == 4)
				return TRUE;
	}

	return FALSE;
}

// for testing
function mytime() {
	$i_time = time();
	if (isset($_SESSION['till_summary_adjust_day'])) {
		$i_day_adjust = (int)$_SESSION['till_summary_adjust_day'];
		$i_time += 60*60*24*$i_day_adjust;
	}
	return $i_time;//-60*60*24;
}

function mystrtotime($string) {
	$i_time = strtotime($string);
	if (isset($_SESSION['till_summary_adjust_day'])) {
		$i_day_adjust = (int)$_SESSION['till_summary_adjust_day'];
		$i_time += 60*60*24*$i_day_adjust;
	}
	return $i_time;//-60*60*24;
}

// copy all values from table1 to table2 where setting is like   'till_customization_settings_%'   or   'till_in_out_%'
// table1 and table2, in this case, are the config and cash_data tables
function copy_from_old_tables_to_new_tables() {
	global $till_table;
	$old_till_table = "config";
	$ltill_table = ConnectionHub::getConn('rest')->escapeString($till_table);

	if ($ltill_table == $old_till_table)
		return;

	$cols1_query = lavu_query("SHOW COLUMNS FROM `[1]`", $old_till_table);
	$cols2_query = lavu_query("SHOW COLUMNS FROM `[1]`", $ltill_table);

	$cols1 = array();
	$matching_cols = array();
	while ($cols1_read = mysqli_fetch_assoc($cols1_query))
		$cols1[] = $cols1_read['Field'];
	while ($cols2_read = mysqli_fetch_assoc($cols2_query)) {
		$col_name = $cols2_read['Field'];
		if (in_array($col_name, $cols1)) {
			$matching_cols[] = $col_name;
		}
	}
	$insert_cols_string = "`".implode("`,`",$matching_cols)."`";
	$vals_cols_string = "'[".implode("]','[", $matching_cols)."]'";

	$setting_names = array('till_customization_settings_%', 'till_in_out_%');
	foreach($setting_names as $name) {
		$old_vals_query = lavu_query("SELECT * FROM `[1]` WHERE `setting` LIKE '[2]'",$old_till_table,$name);
		while ($old_vals_read = mysqli_fetch_assoc($old_vals_query)) {
			$old_vals_read['new_till_table'] = $ltill_table;
			$query_string = "INSERT INTO `[new_till_table]` ($insert_cols_string) VALUES ($vals_cols_string)";
			lavu_query($query_string, $old_vals_read);
			lavu_query("DELETE FROM `[1]` WHERE `id`='[2]'",$old_till_table,$old_vals_read['id']);
		}
	}
}

function till_display_price($p)
{
	global $till_display_price_location;

	$dp = $till_display_price_location['disable_decimal'];
	$decimal_char = $till_display_price_location['decimal_char'];
	$left_or_right = $till_display_price_location['left_or_right'];
	$monetary_symbol = $till_display_price_location['monetary_symbol'];

	if ($left_or_right == "right") return number_format($p, $dp, $decimal_char, ",")." ".htmlspecialchars($monetary_symbol);
	else return htmlspecialchars($monetary_symbol)." ".number_format($p, $dp, $decimal_char, ",");
}

// check if the page is being loaded for server reconciliation (as opposed to cash reconciliation)
function is_server_settings() {
	if (	(get_register_server_till() !== 0 || (isset($_GET['mode']) && $_GET['mode'] == 'edit_transaction_summary_items' && isset($_GET['server']) && $_GET['server'] == '1')) ||
			(isset($_GET['server_reconciliation'])) || $_GET['drawer'] > 0)
		return TRUE;
	return FALSE;
}

// check if the page is being loaded for Bank Deposit options (as opposed to cash reconciliation)
function is_bankdeposit_settings() {
    if (	(get_register_server_till() !== 0 || (isset($_GET['mode']) && $_GET['mode'] == 'edit_transaction_summary_items' && isset($_GET['server']) && $_GET['server'] == '1')) ||
        (isset($_GET['bank_deposit'])))
        //return TRUE; comented this one and added "return FALSE" to test the functionality of 'DEPOSIT'
        return FALSE;
        return FALSE;
}

function get_transaction_method_preferences($drawer = 0) {
	global $till_table;
	$apply_transaction_methods = array("paid_in"=>array("+","ignore"), "paid_out"=>array("-","ignore"), "cash"=>array("+","ignore"), "gift_certificate"=>array("ignore","ignore"), "credit_card"=>array("ignore","-"), "cash_deposits"=>array("+","ignore"), "cash_deposit_refunds"=>array("-","ignore"), "card_deposits"=>array("ignore","ignore"), "card_deposit_refunds"=>array("ignore","ignore"));

	// check if this is supposed to be for server settings
	$s_server_settings = '';
	if (is_server_settings()) {
		$s_server_settings = 'server_';
		$apply_transaction_methods = array("paid_in"=>array("+","ignore"), "paid_out"=>array("-","ignore"), "cash"=>array("+","-"), "gift_certificate"=>array("ignore","ignore"), "credit_card"=>array("ignore","-"), "cash_deposits"=>array("+","ignore"), "cash_deposit_refunds"=>array("-","ignore"), "card_deposits"=>array("ignore","ignore"), "card_deposit_refunds"=>array("ignore","ignore"));
	}
	
	// check if this is supposed to be for server settings
	if (is_bankdeposit_settings()) {
	    $s_server_settings = 'bankdeposit_';
	    $apply_transaction_methods = array("paid_in"=>array("+","ignore"), "paid_out"=>array("-","ignore"), "cash"=>array("+","ignore"), "credit_card"=>array("ignore","-"), "cash_deposits"=>array("+","ignore"), "cash_deposit_refunds"=>array("-","ignore"), "card_deposits"=>array("ignore","-"), "card_deposit_refunds"=>array("ignore","-"), "gift_certificate"=>array("ignore","ignore"));
	}
	// get the actual values, if they exist
	$options1_query = lavu_query("SELECT * FROM `".$till_table."` WHERE `type`='location_config_setting' AND `setting` LIKE '{$s_server_settings}till_customization_settings_%' AND `value3` = 'summary_item' AND drawer = '" . $drawer . "'");
	if ($options1_query) {
		while ($row = mysqli_fetch_assoc($options1_query)) {
			foreach ($apply_transaction_methods as $name=>$method) {
				if ($row['setting'] == "{$s_server_settings}till_customization_settings_".$name) {
					// payment
					$apply_transaction_methods[$name][0] = $row['value'];
					// tips
					$apply_transaction_methods[$name][1] = ($row['value2'] === "" ? "ignore" : $row['value2']);
				}
			}
		}
	}
	return $apply_transaction_methods;
}

function save_transaction_method_preferences($apply_transaction_methods, $drawer) {

	// default values
	global $till_table;
	$where_clause = "`type`='location_config_setting' AND `setting`='[server_settings]till_customization_settings_[name]'";
	$set_clause = "`value`='[value]',`value2`='[value2]',`value3`='summary_item',`drawer`='[drawer]'";
	$insert_clause = "(`setting`,`type`,`value`,`value2`,`value3`, `drawer`)";
	$values_clause = "('[server_settings]till_customization_settings_[name]','location_config_setting','[value]','[value2]','summary_item', '[drawer]')";

	// check if this is supposed to be for server settings
	$s_server_settings = '';
	if (is_server_settings())
		$s_server_settings = 'server_';
	
    // check if this is supposed to be for bank deposit settings
    if (is_bankdeposit_settings())
        $s_server_settings = 'bankdeposit_';
	
		
	$where_clause = str_replace("[server_settings]", $s_server_settings, $where_clause);
	$values_clause = str_replace("[server_settings]", $s_server_settings, $values_clause);

	// do the update
	foreach ($apply_transaction_methods as $name=>$values) {
		$vars = array();
		$vars['name'] = $name;
		$vars['value'] = $values[0];
		$vars['value2'] = $values[1];
		$vars['server_settings'] = isset($_GET['server_reconciliation']);//Added by Brian D.
		$vars['drawer'] = $drawer;
		$exists_query = lavu_query("SELECT `id` FROM `".$till_table."` WHERE ".$where_clause, $vars);
		$exists = FALSE;
		if ($exists_query !== FALSE) {
			if (mysqli_num_rows($exists_query) > 0) {
				$exists = TRUE;
			 }
		}
		else{
			error_log("Error in constructed query in transaction_summaries.");
		}
		if ($exists === TRUE) {
			lavu_query("UPDATE `".$till_table."` SET ".$set_clause." WHERE ".$where_clause, $vars);
		} else {
			lavu_query("INSERT INTO `".$till_table."` ".$insert_clause." VALUES ".$values_clause, $vars);
		}
	}
	syncDownCashDataTillSettingsIfLLS();
}


//Function created by Brian D.  Have to do special syncing for this.
function syncDownCashDataTillSettingsIfLLS(){
	global $dev;

	$isLLS = sessvar('isLLS');
	if($isLLS){
		return;
	}

	//Inserted by Brian D.
	////We include these here, since if it is an lls this code would cause a fatal error.
	require_once(dirname(__FILE__).'/../'.($dev ? '../' : '').'cp/resources/msync_functions.php');
	require_once(dirname(__FILE__).'/../'.($dev ? '../' : '').'cp/resources/json.php');

	$query = "SELECT * FROM `cash_data` WHERE `setting` in (".
			       "'server_till_customization_settings_paid_in',".
			       "'server_till_customization_settings_paid_out',".
			       "'server_till_customization_settings_cash',".
			       "'server_till_customization_settings_gift_certificate',".
			       "'server_till_customization_settings_credit_card',".
			       "'till_customization_settings_paid_in',".
			       "'till_customization_settings_paid_out',".
			       "'till_customization_settings_cash',".
			       "'till_customization_settings_gift_certificate',".
			       "'till_customization_settings_credit_card')";

	$result = lavu_query($query);
	$valArr = array();
	while($curr = mysqli_fetch_assoc($result)){
		unset($curr['id']);
		$valArr[] = $curr;
	}

	$allInsertUpdates = array();
	foreach($valArr as $curr){
		$currentEntry = array();
		$currentEntry['table'] = 'cash_data';
		$currentEntry['update_if'] = array('setting' => $curr['setting']);
		$currentEntry['set'] = $curr;
		$allInsertUpdates[] = $currentEntry;
	}

	$encodedJSON = LavuJson::json_encode($allInsertUpdates);

	//schedule_msync($dataname,$locationid,$sync_setting,$sync_value="",$sync_value_long="",$sync_value2="");
	$dataname = sessvar("admin_dataname");
	$locationID = sessvar("locationid");
	schedule_msync($dataname, $locationID, 'update_or_insert_query', '', $encodedJSON);
}


// report options must include currdate, starttime_string, nextdaydate, endtime_string, and register_name
function get_cc_transactions_reports($report_options, $drawer) {

	global $extra_display_string;
	global $cc;
	global $dualDrawerEnabled;

	# set defaults here
	$retval = array(
		"cash"=>array(0,0),
		"paid_in"=>array(0,0),
		"paid_out"=>array(0,0),
		"credit_card"=>array(0,0),
		"gift_certificate"=>array(0,0),
	    "cash_deposits"=>array(0,0),
	    "cash_deposit_refunds"=>array(0,0),
	    "card_deposits"=>array(0,0),
	    "card_deposit_refunds"=>array(0,0)
	);

	// request the query vars
	$b_server_reconciliation = is_server_settings();
	$s_select_server = ($b_server_reconciliation || $dualDrawerEnabled) ? "AND `server_id`='[server_id]'" : "AND `register_name` = '[register_name]'";
	$s_select_server .= " AND drawer = '" . $drawer . "'";
	$cc_transactions_query_string = "SELECT `id`,`order_id`,`tip_amount`,`card_type`,`pay_type_id`,`total_collected`,`action`,`datetime` FROM `cc_transactions` WHERE `voided`='0' AND `datetime` >= '[currdate] [starttime_string]' AND `datetime` < '[nextdaydate] [endtime_string]' {$s_select_server}";
	
	//start Lp-6191
	
	$b_server_reconciliationDeposit = is_server_settings();
	$s_select_serverDeposit = ($b_server_reconciliationDeposit) ? "AND `cc_transactions`.`server_id`='[server_id]'" : "AND `cc_transactions`.`register_name` = '[register_name]'";
	
	$deposit_transaction = "SELECT `cc_transactions`.`id`,`cc_transactions`.`order_id`,`cc_transactions`.`refunded`,`cc_transactions`.`pay_type`,`cc_transactions`.`tip_amount`,`cc_transactions`.`card_type`,`cc_transactions`.`transtype`,`cc_transactions`.`pay_type_id`,`cc_transactions`.`total_collected` as 'Amount',`cc_transactions`.`action`,`cc_transactions`.`datetime` FROM `cc_transactions` JOIN `deposit_information` ON `cc_transactions`.`order_id` = `deposit_information`.`transaction_id` WHERE `cc_transactions`.`voided`='0' AND `cc_transactions`.`datetime` >= '[currdate] [starttime_string]' AND `cc_transactions`.`datetime` < '[nextdaydate] [endtime_string]' AND `cc_transactions`.`drawer` = '" . $drawer . "' {$s_select_serverDeposit}";
	
	
	$cc_report_options = array();
	foreach ($report_options as $key=>$value)
		if (gettype($value) != gettype(array()))
			$cc_report_options[$key] = $value;

	// get the date range
	$cc_report_options["currdate"] = date("Y-m-d", strtotime($cc_report_options["currdate"]));
	$cc_report_options["nextdaydate"] = date("Y-m-d", strtotime($cc_report_options["currdate"]));
	if (strtotime("2013-01-01 ".$report_options['starttime_string']) < strtotime("2013-01-01 ".date('H:i:s',$report_options['day_start_end_time'])))
		$cc_report_options["currdate"] = date("Y-m-d", strtotime($cc_report_options["currdate"].' +1 day'));
	if (strtotime("2013-01-01 ".$report_options['endtime_string']) < strtotime("2013-01-01 ".date('H:i:s',$report_options['day_start_end_time'])))
		$cc_report_options["nextdaydate"] = date("Y-m-d", strtotime($cc_report_options["currdate"].' +1 day'));

	//Override date for dual cash drawer because when transaction happend after 5 pm then not getting records
	if ( $dualDrawerEnabled && (strtotime(date("Y-m-d") . ' ' . $report_options['endtime_string']) <  strtotime(date("Y-m-d") . ' ' . $report_options['starttime_string']))) {
		$cc_report_options["nextdaydate"] = date("Y-m-d", strtotime($cc_report_options["currdate"].' +1 day'));
	}

	// do a query to the database for cc_transactions
	$cc_transactions_query = lavu_query($cc_transactions_query_string, $cc_report_options);
	
	//depoist==================================
	$cashAmount = $cashRefundAmount = $cardAmount = $cardRefundAmount = 0;
	$deposit_transactions_query = lavu_query($deposit_transaction, $cc_report_options);
	if ($deposit_transactions_query) {
	    if (mysqli_num_rows($deposit_transactions_query) > 0) {
	        
	        $retvalDeposit = array(
	            "cash_deposits"=>array(0,0),
	            "cash_deposit_refunds"=>array(0,0),
	            "card_deposits"=>array(0,0),
	            "card_deposit_refunds"=>array(0,0)
	        );
	        
	        while ($rows = mysqli_fetch_assoc($deposit_transactions_query)) {
	            $paytype =  $rows['pay_type'];
	            $refunded =  $rows['refunded'];
	            //$tip = $rows['tip_amount'];
	            $amount = $rows['Amount'];
	            
	            if ($rows['pay_type'] == "cash" && $rows['refunded'] == 0) {
	                $cashAmount += abs($amount);
	            } else if ($rows['pay_type'] == "cash" && $rows['refunded'] == 1) {
	                $cashRefundAmount += abs($amount);
	            } else if ($rows['pay_type'] == "card" && $rows['transtype'] == 'deposit') {
	                $cardAmount += abs($amount);
	            } else if ($rows['pay_type'] == "card" && $rows['transtype'] == 'deposit_refund') {
	                $cardRefundAmount += abs($amount);
	            }
	        }
	        
	        $retvalDeposit["cash_deposits"][0] = $cashAmount;
	        $retvalDeposit["cash_deposits"][1] = 0;
	        $retvalDeposit["cash_deposit_refunds"][0] = $cashRefundAmount;
	        $retvalDeposit["cash_deposit_refunds"][1] = 0;
	        $retvalDeposit["card_deposits"][0] = $cardAmount;
	        $retvalDeposit["card_deposits"][1] = 0;
	        $retvalDeposit["card_deposit_refunds"][0] = $cardRefundAmount;
	        $retvalDeposit["card_deposit_refunds"][1] = 0;
	        
	    }
	}
	
	//end deposit ===========================
	
	
	
	// debug
	if (isset($_GET['yourmom'])) {
		echo "2013-01-01 ".$report_options['endtime_string']."<br />";
		echo "2013-01-01 ".$report_options['day_start_end_time']."<br />";
		foreach($cc_report_options as $k=>$v)
			$cc_transactions_query_string = str_replace("[$k]", $v, $cc_transactions_query_string);
		echo($cc_transactions_query_string);
	}

	// get the payment types
	$a_payment_types = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('payment_types', NULL, FALSE);

	if ($cc_transactions_query) {
		if (mysqli_num_rows($cc_transactions_query) > 0) {
			$retval = array(
				"cash"=>array(0,0),
				"paid_in"=>array(0,0),
				"paid_out"=>array(0,0),
				"credit_card"=>array(0,0),
				"gift_certificate"=>array(0,0)
			);
			foreach($a_payment_types as $a_payment_type)
				$retval[$a_payment_type['type']] = array(0,0);
			while ($row = mysqli_fetch_assoc($cc_transactions_query)) {
				$value = $row['total_collected'];
				$tip = $row['tip_amount'];
				$action = $row['action'];
				if ($action != "Refund" && $action != "Void" && $action != "Sale") {
					error_log("Action type is blank for cc_transaction id " . $row['id']);
					continue;
				}

				//$extra_display_string .= print_r($row, TRUE) . "<br />";

				if ($action == "Refund") {
					$value = -1*$value;
				} else if ($action == "Void") {
					$value = 0;
					$tip = 0;
				} else if ($action == "Sale") {
				}
				$value = $value*1;

				if ($row['order_id'] == "Paid In") {
					$retval["paid_in"][0] += abs($value);
					$retval["paid_in"][1] += $tip;
				} else if ($row['order_id'] == "Paid Out") {
					$retval["paid_out"][0] += abs($value);
					$retval["paid_out"][1] += $tip;
				} else if ($row['pay_type_id'] == "1") {
					$retval["cash"][0] += $value;
					$retval["cash"][1] += $tip;
				} else if ($row['pay_type_id'] == "2") {
					$retval["credit_card"][0] += $value;
					$retval["credit_card"][1] += $tip;
				} else if ($row['pay_type_id'] == "3") {
					$retval["gift_certificate"][0] += $value;
					$retval["gift_certificate"][1] += $tip;
				} else {
					$b_payment_id_matches = FALSE;
					foreach ($a_payment_types as $a_payment_type) {
						if ((int)$row['pay_type_id'] == (int)$a_payment_type['id']) {
							$s_payment_type = $a_payment_type['type'];
							if (stristr($s_payment_type, speak('cash'))) {
								$retval["cash"][0] += $value;
								$retval["cash"][1] += $tip;
							} else {
								$retval[$s_payment_type][0] += $value;
								$retval[$s_payment_type][1] += $tip;
							}
							$b_payment_id_matches = TRUE;
							break;
						}
					}
					if ($b_payment_id_matches === FALSE)
						error_log("unknown pay_type_id ".$row['pay_type_id']);
				}
			}
			list ($cash_tip, $card_tip) = get_server_cash_credit_tips($cc_report_options);
			$retval['cash'][1] += $cash_tip;
		}
	}
	
	if (count($retvalDeposit) > 0) {
	    $retval = array_merge($retval, $retvalDeposit);
	}
	
	return $retval;
}

function get_location_timezone($loc_id) {
	$location_timezone = "America/Denver";
	$options2_query = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $loc_id);
	if ($options2_query) {
		while ($row = mysqli_fetch_assoc($options2_query)) {
			if ($row['id'] == $loc_id) {
				$day_start_end_time = $row['day_start_end_time'];
				$location_timezone = $row['timezone'];
			}
		}
	}

	return $location_timezone;
}

function get_day_start_end_time($loc_id) {
	$day_start_end_time = "0000";
	$options2_query = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $loc_id);
	if ($options2_query) {
		while ($row = mysqli_fetch_assoc($options2_query)) {
			if ($row['id'] == $loc_id) {
				$day_start_end_time = $row['day_start_end_time'];
				$location_timezone = $row['timezone'];
			}
		}
	}

	return $day_start_end_time;
}

function set_query_operators_and_start_and_endtime(&$starttime, &$endtime, &$startors, &$endors, $day_start_end_time_local){
	$endors = "AND";
	if ($endtime === NULL) {
		if ($starttime === NULL) {
			$endtime = $day_start_end_time_local;
			$endors = "OR";
		} else {
			$endtime = $starttime;
			$endors = "OR";
		}
	}
	if (gettype($endtime) === gettype("string")) {
		$endtime = strtotime($endtime);
	}
	$startors = "AND";
	if ($starttime === NULL) {
		$starttime = $day_start_end_time_local;
		$startors = "OR";
	}
	if (gettype($starttime) === gettype("string")) {
		$starttime = strtotime($starttime);
	}
}

// returns an array of options, with defaults if the options don't exist
// loads them from the config table, type=location_config_setting, setting like 'till_customization_settings%'
function get_report_query_variables($register_id, $register_name, $date, $monetary_symbol, $loc_id, $starttime = NULL, $endtime = NULL, $isRegisterTill=FALSE, $drawer) {
	global $location_timezone;
	global $day_start_end_time;
	global $real_name;
	# set the defaults here
	$include_cc_tips = TRUE;
	$apply_transaction_methods = get_transaction_method_preferences($drawer);

	if (!isset($location_timezone))
		$location_timezone = get_location_timezone($loc_id);
	if (!isset($day_start_end_time))
		$day_start_end_time = get_day_start_end_time($loc_id);

	$time = str_pad($day_start_end_time, 4, "0", STR_PAD_LEFT);
	$time = date("Y-m-d", mytime()) . " " . substr($time,0,2) . ":" . substr($time,2,2) . ":00";
	$day_start_end_time_local = strtotime($time);
	$location_time = mystrtotime(localize_datetime(date("y-m-d H:i:s"), $location_timezone));

	# for the query
	# set defaults for parameters not loaded

	set_query_operators_and_start_and_endtime($starttime, $endtime, $startors, $endors, $day_start_end_time_local);
	# get the current restaurant day
	if (strtotime("2013-01-01 ".date("H:i:s",$location_time)) >= strtotime("2013-01-01 ".date("H:i:s",$day_start_end_time_local))) {
		$currdate_time = strtotime($date);
	} else {
		$currdate_time = strtotime($date)-60*60*24;
	}
	$currdate = date( "y-m-d", $currdate_time );

	$setting1 = "till_in_out_[currdate]_";
	if($isRegisterTill){
		$setting1 .= "register:" . $register_name;
	}else{
		$setting1 .= "server:" . $register_name;
	}


	if(isset($starttime) && isset($endtime) && date("H:i:s", $starttime) > date("H:i:s", $endtime)){ //if end-time occurs after midnight and start time occurs before midnight, this will happen.
		$tmp = $starttime;
		$starttime = $endtime;
		$endtime = $tmp;
	}

	# get the time spans
	$timereq = "(`value2` <= '[endtime_string]' ".$startors." `value2` >= '[starttime_string]')";
	$timereq .= " AND (`value3` = 'NULL' OR (`value3` >= '[starttime_string]' ".$endors." `value3` <= '[endtime_string]'))";
	$timereq .= " AND drawer = '" . $drawer . "'";

	$retval = array(
		"include_cc_tips"=>$include_cc_tips,
		"day_start_end_time"=>$day_start_end_time_local,
		"location_time"=>$location_time,
		"location_timezone"=>$location_timezone,
		"location_id"=>$loc_id,
		"register_id"=>$register_id,
		"register_name"=>$register_name,
		"setting1"=>$setting1,
		"timereq"=>$timereq,
		"currdate"=>$currdate,
		"currdate_time"=>$currdate_time,
		"starttime_string"=>date("H:i:s", $starttime),
		"endtime_string"=>date("H:i:s", $endtime));
	foreach ($apply_transaction_methods as $key=>$value) {
		$retval["apply_method_".$key] = $value;
	}

	return $retval;
}

// returns an empty array if no tills are found
// if starttime and endtime are NULL then this function operates on the current calendar date.
function get_tills_between_times($register_id, $register_name, $date, $monetary_symbol, $loc_id, $starttime=NULL, $endtime=NULL, $isRegisterTill=FALSE, $isDailyTill=FALSE, $drawer) {
	global $till_table;
	global $real_name;
	global $printSingleTill;
	global $dualDrawerEnabled;
	global $active_access_level;

	if($isRegisterTill){
	}else {
		$register_name = $real_name;
	}
	$register_id = ($dualDrawerEnabled && $active_access_level >= 3) ? '' : $real_name;
	$report_options = get_report_query_variables($register_id, $register_name, $date, $monetary_symbol, $loc_id, $starttime, $endtime, $isRegisterTill, $drawer);
	$retval = array();
	$query_options = createQueryOptionsArray($report_options);
	removeDuplicateTills($till_table, $report_options, $query_options, $drawer); //Remove duplicate tills from the database.

	$setting1 = "till_in_out_[currdate]_";
	if($isRegisterTill){ //register reconciliation
		$setting1 .= "register:[register_name]%"; //should have format of till_in_out_[currdate]_register:[register_name]";
	}else{ //server reconciliation
		$setting1 .= "server:[register_id]%";
	}

	if(!$isDailyTill) {
		$tills_query_string = "SELECT * FROM `" . $till_table . "` WHERE `setting` LIKE '" . $setting1 . "' AND ".$report_options['timereq'];
	}else {
		$tills_query_string = "SELECT * FROM `" . $till_table . "` WHERE `setting` LIKE '" . $setting1 . "' AND drawer = '" . $drawer . "'";
	}

	//Grabs any tills from the current date within time range betwixt starttime and endtime.
	$tills_query = lavu_query($tills_query_string, $query_options); //Replace square-bracketed items with respective elements from query_options, and perform the query.

	//If there are any tills from the current date betwixt the start time and end time, then create associate arrays for them and add each one as an element of the returned value.
	if ($tills_query) {
		while ($row = mysqli_fetch_assoc($tills_query)) {
			$row['totals'] = $row['value'];
			$row['timein'] = $row['value2'];
			$row['timeout'] = $row['value3'];
			$row['bills'] = $row['value4'];
			$row['coins'] = $row['value5'];
			$row['denominations'] = $row['value6'];
			$row['drawer'] = $row['drawer'];

			if(count(explode("_",$row['setting'])) > 1){
				$parts = explode("_",$row['setting']);
				$lastPart = $parts[count($parts)-1];
				$nameParts = explode(":",$lastPart);
				$row['server_name'] = $nameParts[0];
			}
			if (count(explode(":",$row['setting'])) > 1) {
				$parts = explode(":",$row['setting']);
				$row['register_name'] = $parts[1];
			}
			$retval[] = $row;
		}
	}
	//return rows from the cash_data table corresponding with the current date between start time and end time, or for the whole day if they are set to null.
	return $retval;
}

function createQueryOptionsArray($report_options){
	$query_options = array();
	foreach ($report_options as $key=>$value) { //Create associative array from non-array elements in report_options. (everything but apply_method elements)
		if (gettype($value) != gettype(array())) {
			$query_options[$key] = $value;
		}
	}
	return $query_options;
}

function removeDuplicateTills($till_table, $report_options, $query_options, $drawer){
	// remove DUPLICATE TILLS with NULL timeout fields - This will remove ALL duplicates.
    $tills_query_string = "SELECT * FROM `".$till_table."` WHERE `setting` LIKE '".lavu_query_encode($report_options['setting1'])."' AND `value3`='NULL' AND drawer = '" . $drawer . "'"; //should read "select * from `cash_data` where setting like  'till_in_out_[currdate]_[register_id]:register_name' and `value3`=='NULL'"
	$tills_query = lavu_query($tills_query_string, $query_options); //Perform the query, and replace currdate, register_id, and register_name with the respective items from report_options.
	if ($tills_query) { //IF there is a till option for the current date.
		if (mysqli_num_rows($tills_query) >= 2) { //If there are more than one rows for the current date.
			mysqli_fetch_assoc($tills_query); //save the first row
			while ($row = mysqli_fetch_assoc($tills_query)) { //Grab each row, and set as $row, and then delete that row from the database (so erase everything if there are more than one row.
				$query_options['id']=$row['id'];
				lavu_query("DELETE FROM `".$till_table."` WHERE `setting` = '".$report_options['setting1']."' AND `value3`='NULL' AND `id`='[id]'", $query_options);
			}
		}
	}
}

// if the user is supposed to see the report
// starttime and endtime should be integer values, like what is returned from strtotime
function draw_report($register_id, $register_name, $date, $monetary_symbol="doesntmatter", $loc_id, $starttime = NULL, $endtime = NULL, $drawspacinglines=FALSE, $isRegisterTill, $isDailyTill = FALSE, $forPrinting=FALSE, $drawer) {
	global $testing;
	global $tillname;
	global $cc;
	global $isPreviousDay;
	global $dualDrawerEnabled;
	if ($starttime === "NULL") { $starttime = NULL; }
	if ($endtime === "NULL") { $endtime = NULL; }
	$report_options = get_report_query_variables($register_id, $register_name, $date, $monetary_symbol, $loc_id, $starttime, $endtime, FALSE, $drawer);
	$retval = "";
	$retvaltime = 0;
	$tills = get_tills_between_times($register_id, $register_name, $date, $monetary_symbol, $loc_id, $starttime, $endtime, $isRegisterTill, $isDailyTill, $drawer);

	if (count($tills) == 0) {
		$st = strtotime($report_options['starttime_string']);
		$et = strtotime($report_options['starttime_string']);
		$ststring = date("h:i a", $st);
		$etstring = date("h:i a", $et);
		$displaytime = speak("between")." ".$ststring." ".speak("and")." ".$etstring." ";
		if ($ststring == $etstring)
			$displaytime = "";
		return speak("No tills could be found")." ".$displaytime.speak("for")." ".date("j M Y",$report_options['currdate_time']).".";
	}

	foreach ($tills as $row) {
		if ($isDailyTill || ($starttime == null && $endtime == null) || ($starttime == $row['timein'] && ($row['timeout'] == 'NULL' || $row['timeout'] == $endtime))) {
			$retval .= "<table style='background-color:#f8f8f8;border:1px solid gray;margin:5px 0 15px 0;'>";
			// till in/till out data
			$denominations = get_denominations_from_till($row);
			$counts = get_bills_coins_from_till($row);
			$totals = get_totals_from_till($row);
			$times = get_times_from_till($row);
			// times
			$times['timein'] = date("g:i a", strtotime($times['timein']));
			$times['timeout'] = $times['timeout'] === "NULL" ? speak("now") : date("g:i a", strtotime($times['timeout']));
			$times['server_name'] = $row['server_name'];
			// get the total from the transactions
			$total_from_transactions = 0;
			$apply_methods = array();
			$report_values_options = $report_options;
			$report_values_options['starttime_string'] = date("H:i:s", strtotime($row['timein']));
			if ($row['timeout'] != "now" && $row['timeout'] != "NULL") {
				$report_values_options['endtime_string'] = date("H:i:s", strtotime($row['timeout']));
			} else {
				$report_values_options['endtime'] = $report_values_options['location_time'];
				$report_values_options['endtime_string'] = date("H:i:s", $report_values_options['endtime']);
			}
			$report_values_options['register_name'] = $row['register_name'];
			$report_values_options['server_id'] = ($dualDrawerEnabled || $drawer > 0) ? $row['server_id'] : get_register_server_till();
			$report_values = get_cc_transactions_reports($report_values_options, $drawer);
			foreach ($report_options as $key => $value) {
				if (count(explode("apply_method_", $key)) > 1) {
					$underscore_name = explode("apply_method_", $key);
					$underscore_name = $underscore_name[1];
					$name = explode("_", $underscore_name);
					foreach ($name as $key => $name_part)
						$name[$key] = ucfirst($name_part);
					$name = implode(" ", $name);
					$name = ucfirst($name);

					if ($value[0] == "+") {
						$apply_methods[] = array("+", $name . " Transactions", $report_values[$underscore_name][0]);
						$total_from_transactions += $report_values[$underscore_name][0];
					}
					if ($value[1] == "+") {
						$apply_methods[] = array("+", $name . " Tips", $report_values[$underscore_name][1]);
						$total_from_transactions += $report_values[$underscore_name][1];
					}

					if ($value[0] == "-") {
						$apply_methods[] = array("-", $name . " Transactions", $report_values[$underscore_name][0]);
						$total_from_transactions -= $report_values[$underscore_name][0];
					}
					if ($value[1] == "-") {
						$apply_methods[] = array("-", $name . " Tips", $report_values[$underscore_name][1]);
						$total_from_transactions -= $report_values[$underscore_name][1];
					}
				}
			}
			// title line
			if ($register_name === '') {
				$register_name = $row['register_name'];
			}

			global $real_name, $usersInfo;

			$tillSummary = ($row['drawer'] > 0) ? ucfirst($times['server_name']) . ' - ' . $register_name . ' (Cash Drawer ' . $row['drawer'] . ')' : ucfirst($times['server_name']) . ', ' . $register_name;
			if ($forPrinting) {
				$tillSummary = ($row['drawer'] > 0) ? ucfirst($times['server_name']) . ', ' . $register_name . '#Cash Drawer ' . $row['drawer'] : ucfirst($times['server_name']) . ', ' . $register_name;
				$times_display =  $tillSummary . " " . speak("Till-in at") . " " . $times['timein'] . " " . speak("Till-out at") . " " . $times['timeout'];

			} else {
				$times_display = $tillSummary . " - " . speak("Till-in at") . " <strong>" . $times['timein'] . "</strong> - " . speak("Till-out at") . " <strong>" . $times['timeout'] . "</strong>";

			}

			if ($times['timeout'] == 'now') {
				if ($forPrinting) {
					$times_display =  $tillSummary . " " . speak("Till-in at") . " " . $times['timein'];
				} else {
					$times_display =  $tillSummary . " - " . speak("Till-in at") . " <strong>" . $times['timein'] . "</strong>";
				}
			}

			if ($forPrinting) {
				$retval .= "<tr><td>" . $times_display . "</td><td></td><td>   </td><td></td></tr>";
			} else {
				$retval .= "<tr><td>" . $times_display . ":</td><td></td><td>   </td><td></td></tr>";
			}

			if ($drawspacinglines === TRUE) {
				$retval .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td></tr>";
			}
			// total in
			$retval .= "<tr style='background-color:#eee'><td></td><td>" . speak("Total Till In") . " </td><td style='padding:0 5px 0 5px;text-align:center;'>  </td><td style='padding:0 5px 0 5px;'><font style='text-decoration:underline'>" . till_display_price($totals['totalin']) . "</font></td></tr>";
			// summaries
			foreach ($apply_methods as $am) {
				$retval .= "<tr><td></td><td>" . speak($am[1]) . " </td><td style='padding:0 5px 0 5px;text-align:center;'>" . $am[0] . " </td><td style='padding:0 5px 0 5px;'><font style='text-decoration:underline'>" . till_display_price(abs($am[2])) . "</font></td></tr>";
			}
			// total that there should be in the drawer
			$retval .= "<tr><td></td><td>" . speak("Calculated Value") . " </td><td style='padding:0 5px 0 5px;text-align:center;'>= </td><td style='padding:0 5px 0 5px;'><font style='text-decoration:underline'>" . till_display_price($total_from_transactions + $totals['totalin']) . "</font></td></tr>";
			// if the till has been tilled out
			if ($row['timeout'] != "now" && $row['timeout'] != "NULL") {
				// total out
				$retval .= "<tr style='background-color:#eee'><td></td><td>" . speak("Total Till Out") . " </td><td style='padding:0 5px 0 5px;text-align:center;'>  </td><td style='padding:0 5px 0 5px;'><font style='text-decoration:underline'>" . till_display_price($totals['totalout']) . "</font></td></tr>";
				// over/under
				$overshort = ($total_from_transactions + $totals['totalin']) - $totals['totalout'];
				if ($overshort < 0) $symbol = "-"; else $symbol = "";
				if ($overshort > 0)
					$overshort_string = speak("Short");
				else if ($overshort < 0)
					$overshort_string = speak("Over");
				else
					$overshort_string = speak("Balanced");
				$overshort = abs($overshort);
				$retval .= "<tr style='background-color:#eee'><td></td><td>" . speak("Over/Short") . " </td><td style='padding:0 5px 0 5px;text-align:center;'>= </td><td style='padding:0 5px 0 5px;'><font style='text-decoration:underline'>" . till_display_price($overshort) . "</font> " . $overshort_string . "</td></tr>";
			}

			if ($drawspacinglines === TRUE) {
				$retval .= "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;&nbsp;&nbsp;</td></tr>";
			}
			$retval .= "</table>";
		}
	}
	if ($testing) {
		$retval .= "<br /><br />";
		$retval .= "testing"."<br /><br />";
		$retval .= "day_start_end_time: ".date("Y-m-d h:i:s", $report_options['day_start_end_time'])."<br /><br />";
		$retval .= "location_time: ".date("Y-m-d h:i:s", $report_options['location_time'])."<br /><br />";
		$retval .= "location_timezone: ".$report_options['location_timezone']."<br /><br />";
		$retval .= $tills_query_string."<br /><br />";
	}

	return $retval;
}

function get_decimal_char($location) {
	$decimal_query = lavu_query("SELECT `decimal_char` FROM `locations` WHERE `id` = '[1]'", $location);
	$retval = ".";
	if ($decimal_query)
		while ($row = mysqli_fetch_assoc($decimal_query))
			$retval = $row['decimal_char'];
	if (strlen($retval) == 0)
		$retval = ".";
	return $retval;
}

function get_monetary_symbol($location) {
	$decimal_query = lavu_query("SELECT `monitary_symbol` FROM `locations` WHERE `id` = '[1]'", $location);
	if ($decimal_query)
		while ($row = mysqli_fetch_assoc($decimal_query))
			return $row['monitary_symbol'];
	return "$";
}

function get_disable_decimal($location) {
	$decimal_query = lavu_query("SELECT `disable_decimal` FROM `locations` WHERE `id` = '[1]'", $location);
	if ($decimal_query)
		while ($row = mysqli_fetch_assoc($decimal_query))
			return $row['disable_decimal'];
	return "2";
}

function get_left_or_right($location) {
	$decimal_query = lavu_query("SELECT `left_or_right` FROM `locations` WHERE `id` = '[1]'", $location);
	if ($decimal_query)
		while ($row = mysqli_fetch_assoc($decimal_query))
			return $row['left_or_right'];
	return "left";
}

function get_timezone($location) {
	$decimal_query = lavu_query("SELECT `timezone` FROM `locations` WHERE `id` = '[1]'", $location);
	if ($decimal_query)
		while ($row = mysqli_fetch_assoc($decimal_query))
			return $row['timezone'];
	return "";
}

function get_denominations_from_till($latest_till) {
	$denominations = explode(":", $latest_till['denominations']);
	$denominations['bills'] = explode("/", $denominations[0]);
	$denominations['coins'] = explode("/", $denominations[1]);
	return $denominations;
}

function get_bills_coins_from_till($latest_till) {
	$billsinout = explode(":", $latest_till['bills']);
	$billsin = explode("/", $billsinout[0]);
	$billsout = explode("/", $billsinout[1]);
	$coinsinout = explode(":", $latest_till['coins']);
	$coinsin = explode("/", $coinsinout[0]);
	$coinsout = explode("/", $coinsinout[1]);
	$retval = array();
	$retval['bills_in'] = $billsin;
	$retval['billsout'] = $billsout;
	$retval['coinsin'] = $coinsin;
	$retval['coinsout'] = $coinsout;
	return $retval;
}

function get_totals_from_till($latest_till) {
	$retval = explode(":", $latest_till['totals']);
	$retval['totalin'] = $retval[0];
	$retval['totalout'] = $retval[1];
	return $retval;
}

function get_times_from_till($latest_till) {
	$retval = array();
	$retval['timein'] = $latest_till['timein'];
	$retval['timeout'] = $latest_till['timeout'];
	return $retval;
}

function str_replace_htmltags($needle, $beforetag, $aftertag, $haystack) {
	$haystack = str_replace("</$needle>", "", $haystack);
	$parts = explode("<$needle", $haystack);
	if (count($parts) == 1)
		return $haystack;

	$retval = $parts[0];
	for ($i = 1; $i < count($parts); $i++) {
		while (substr($parts[$i], 0, 1) !== ">" && strlen($parts[$i]) > 0)
			$parts[$i] = substr($parts[$i], 1);
		if (strlen($parts[$i]) > 0)
			$parts[$i] = substr($parts[$i], 1);
		$retval .= $beforetag . $parts[$i] . $aftertag;
	}

	return $retval;
}

function load_location_info($loc_id) {
	global $till_display_price_location;
	$till_display_price_location = array();
	$till_display_price_location['disable_decimal'] = get_disable_decimal($loc_id)*1;
	$till_display_price_location['decimal_char'] = get_decimal_char($loc_id);
	$till_display_price_location['monetary_symbol'] = get_monetary_symbol($loc_id);
	$till_display_price_location['left_or_right'] = get_left_or_right($loc_id);
	$till_display_price_location['timezone'] = get_timezone($loc_id);
}

function get_till_vars_for_printing() {
	$shift_info = array();
	$vars_str = (isset($_REQUEST['shift_info']))?$_REQUEST['shift_info']:"";
	if ($vars_str != "") {
		$var_parts = explode("(and)",$vars_str);
		for($n=0; $n<count($var_parts); $n++) {
			$vparts = explode("(eq)",$var_parts[$n]);
			if (count($vparts) > 1 && trim($vparts[0])!="") $shift_info[trim($vparts[0])] = trim($vparts[1]);
		}
	}

	foreach($shift_info as $k=>$v)
		$shift_info[$k] = urldecode($v);

	return $shift_info;
}

function get_var_if_existing_from_array($array, $varname, $default="") {
	if (isset($array[$varname]))
		return $array[$varname];
	else
		return $default;
}

function get_till_report_to_print($isRegisterTill=FALSE, $isDailyTill, $drawer) {
	$register = urldecode($_REQUEST['register']);
	$loc_id = urldecode($_REQUEST['loc_id']);
	$vars=get_till_vars_for_printing();
	$today = get_var_if_existing_from_array($vars, "today");
	$monetary_symbol = get_var_if_existing_from_array($vars, "ms");
	$post_timein = get_var_if_existing_from_array($vars, "timein");
	$post_timeout = get_var_if_existing_from_array($vars, "timeout");
	$register_name = get_var_if_existing_from_array($vars, "rn");
	$server = get_var_if_existing_from_array($vars, "reg", $register);
	load_location_info($loc_id);
	$report = draw_report($server, $register_name, $today, $monetary_symbol, $loc_id, $post_timein, $post_timeout, FALSE, $isRegisterTill, $isDailyTill, true, $drawer);
	$report = str_replace("1|server:", "1|{$register}:", $report);
	$report = str_replace(":", "[c", $report);
	$report = str_replace("#", ":", $report);
	$report = str_replace_htmltags("table", "", ":", $report);
	$report = str_replace_htmltags("td", "", "", $report);
	$report = str_replace_htmltags("tr", "", ":", $report);
	$report = str_replace_htmltags("font", "", "", $report);
	$report = str_replace_htmltags("table", "", "", $report);
	$report = str_replace_htmltags("br", "", "", $report);
	$report = str_replace("   ", " *   ", $report);
	$report = str_replace(" + ", " *+  ", $report);
	$report = str_replace(" - ", " *-  ", $report);
	$report = str_replace(" = ", " *=  ", $report);
	$report = "1|".$register.":".$report;
	return $report;
}

function get_till_report_for_reports($isRegisterTill) {
	global $locationid;
	$register = NULL;
	$register_name = NULL;
	$today = date("y-m-d");
	$monetary_symbol = get_monetary_symbol($locationid);
	$loc_id = $locationid;
	$post_timein = NULL;
	$post_timeout = NULL;
	load_location_info($loc_id);
	$report = draw_report($register, $register_name, $today, $monetary_symbol, $loc_id, $post_timein, $post_timeout, TRUE, $isRegisterTill, false, false);
	$report = str_replace_htmltags("table", "", "\n", $report);
	$report = str_replace_htmltags("td", "", "", $report);
	$report = str_replace_htmltags("font", "<font>", "</font>", $report);
	$report = str_replace_htmltags("tr", "<tr><td style='text-align:right;'>", "</td></tr>\n", $report);
	$report = str_replace(" + ", "</td><td>", $report);
	$report = str_replace(" - ", "</td><td>", $report);
	$report = str_replace(" = ", "</td><td>", $report);
	$report = str_replace("   ", "</td><td>", $report);
	//$report = str_replace(":", ":<br />\n", $report);
	return $report;
}

// given the options 'currdate', 'starttime_string', 'nextdaydate', 'endtime_string', and 'server_id' in $report_options,
// return the cash tips in `orders` as a float for the given server id at the given time
// @return: the cash tips as a float
function get_server_cash_credit_tips(&$report_options) {
	global $isRegisterTill;
	$cash_tip = 0.0;
	$card_gratuity = 0.0;

	if($isRegisterTill){
		// get the orders and find their cash tips
		$orders_query_string = "SELECT `cash_tip`, `card_gratuity` FROM `orders` WHERE `closed` >= '[currdate] [starttime_string]' AND `closed` < '[nextdaydate] [endtime_string]' AND `register_name`='[register_name]'";
	}else{
		// get the orders and find their cash tips
		$orders_query_string = "SELECT `cash_tip`, `card_gratuity` FROM `orders` WHERE `closed` >= '[currdate] [starttime_string]' AND `closed` < '[nextdaydate] [endtime_string]' AND `server_id`='[server_id]'";
	}

	$orders_query = lavu_query($orders_query_string, $report_options);
	if ($orders_query !== FALSE && mysqli_num_rows($orders_query) > 0) {
		while ($a_order = mysqli_fetch_assoc($orders_query)) {
			// add in the cash tips and card tips
			$cash_tip += (float)$a_order['cash_tip'];
			$card_gratuity += (float)$a_order['card_gratuity'];
		}
	}

	return array($cash_tip, $card_gratuity);
}

function getAllActiveUser() {
	$usersDetail = array();
	$user_query = lavu_query('SELECT id, username, CONCAT(f_name, " ", l_name) AS fullname , access_level  FROM `users` WHERE _deleted = 0 AND active = 1 ORDER BY fullname');
	while ($user_read = mysqli_fetch_assoc($user_query)) {
		$usersDetail[$user_read['id']]['id'] = $user_read['id'];
		$usersDetail[$user_read['id']]['access_level'] = $user_read['access_level'];
		$usersDetail[$user_read['id']]['username'] = $user_read['username'];
		$usersDetail[$user_read['id']]['fullname'] = trim(str_replace('<', '&lt;', $user_read['fullname']));
	}
	return $usersDetail;
}
?>
