<?php

session_start();

require_once(__DIR__."/NewRelicHelper.php");

$NewRelic = new NewRelicHelper();
$NewRelic->trackRequestStart();
$NewRelic->track("client_ip", getClientIPServer());
require_once(__DIR__."/lds/LdsDataSyncer.php");

$dataSyncer = new LdsDataSyncer();
$dataSyncer->sync($_REQUEST);
$dataSyncer->echoResponse();

lavu_exit(); // calls NewRelicHelper's trackRequestEnd()