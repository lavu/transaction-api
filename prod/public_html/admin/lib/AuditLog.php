<?php
	/*
	 * This class created for log(insert the records into audit table) the records if any updated in table 
	 */
	require_once(dirname(__FILE__)."/info_inc.php");
	class AuditLog
	{
		/*
		 * This method will get the realated data and insert into audit table
		 * param one is dataname, param 2 is an array in that keys are id,id_type,table_name,user_id,updated_for and param 3 if any where condition(id=23) 
		 */
		function doAudit($dataname,$args,$wherecondition=''){
			
			global $data_name;
			$data_name = $dataname;
			if(isset($args) && count($args)>=3){
				$resultdata = $this->getDatafromtable($args,$wherecondition);
				$args['info'] = $resultdata;
				$this->InsertAuditLog($args);
			}
			else {
				return "invalid parameters";
			}
		}
		/*
		 * this method will get the data from respective table and condition
		 */
		function getDatafromtable($args,$whereCondition=''){
				$where = ($whereCondition!='')?" where ".$whereCondition:" where ".$args['id_type']."='".$args['id']."'";				
				$query = lavu_query("select * from ".$args['table_name'].$where);
				$row = mysqli_fetch_assoc($query);
				return json_encode($row);
		}
		/*
		 * this method will insert the data into audit log table
		 */
		function insertAuditLog($args)
		{
			//Check for $args['info'] Exist & its Length
			if (isset($args['info']) && count($args['info']) > 0) {
			$infoData = '';
			$cardNumber = '';
			$infoData = json_decode($args['info'], true);
			//Encrypt Card Number as per LP-9967
			//Check for Card Number Exist,Card Number Value Should Not be Empty,Card Number is Equal to 16
			if (isset($infoData['process_info']['card_number']) && $infoData['process_info']['card_number'] != '' && strlen($infoData['process_info']['card_number']) == 16) {
			//Replace Middle 8 Digits with XXXXXXXX
			$cardNumber = substr_replace($infoData['process_info']['card_number'], 'XXXXXXXX', 4, 8);
			//Assign Newly Encrypted Card Numer to Array
			$infoData['process_info']['card_number'] = $cardNumber;
			}
			//Check For Security Code and Replace with Empty
			if (isset($infoData['process_info']['card_cvn']) && $infoData['process_info']['card_cvn'] != '') {
			$infoData['process_info']['card_cvn'] = '';
			}
			//Set Newly Assigned Value Back to the Array
			$args['info'] = json_encode($infoData);
			}
			$args['created_date'] = date("Y-m-d H:i:s");
			lavu_query("insert into audit_log (id, id_type, table_name, info, updated_for, updated_by, created_date) values('[id]', '[id_type]', '[table_name]', '[info]', '[updated_for]', '[user_id]', '[created_date]')", $args);
		}
		
		function gatewayAuditLog($backtrace, $type, $log, $gateway, $orderId, $serverId) {

			$index = 1;
			$backtraceStr = '';
			foreach ($backtrace as $node) {
				$backtraceStr .= "$index. ".basename($node['file']) .":" .$node['function'] ."(" .$node['line'].")\n";
				$index++;
			}
			$auditArray['backtrace'] = $backtraceStr;
			if ($type =='Request') {
				$auditArray['process_info'] = $log;
			}
			else {
				$auditArray['process_response'] = $log;
			}
			$auditJson = json_encode($auditArray);

			$auditLogs = array(
					"id" => time(),
					"id_type" => $gateway." ". $type,
					"table_name" => "cc_transactions",
					"info" => $auditJson,
					"updated_for" => "$orderId",
					"updated_by" => "Gateway",
					"user_id" => "$serverId",
					"created_date" => date('Y-m-d')

			);

			$this->insertAuditLog($auditLogs);
		}
	}
?>
