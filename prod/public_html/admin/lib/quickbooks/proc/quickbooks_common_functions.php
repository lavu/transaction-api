<?php


	////////////////////////////////////////////////////////////////////////
	//  OTHER QUERY FUNCTIONS
	function getTaxProfilesForGivenIDsIDAsKey($idsArray){
		$idsArrayInts = array();
		foreach($idsArray as $currID){
			$idsArrayInts[$currID] = intval($currID);
		}
		$ids_final = array_values($idsArrayInts);
		//Safe because intval() has been called on everything inside array.
		$inStatement = "('" . implode("','", $idsArrayInts) . "')";
		$result = lavu_query("SELECT * FROM `tax_profiles` WHERE `id` IN ".$inStatement);
		validateQResult($result, __FILE__, 'lvynw4');
		$resultRows = array();
		while($curr = mysqli_fetch_assoc($result)){
			$resultRows[$curr['id']] = $curr;
		}
		return $resultRows;
	}

	function getOrderRow($orderID){
		$queryStr = "SELECT `orders`.`id`, `orders`.`gratuity`, `orders`.`discount_type`, `orders`.`discount_value`, `orders`.`discount_id`, `discount_types`.`title`, `orders`.`void`, `orders`.`void_reason`, `orders`.`auth_by` ".
			        "FROM `orders` LEFT JOIN `discount_types` ON `orders`.`discount_id`=`discount_types`.`id` WHERE `order_id`='[1]'";
		$result = lavu_query($queryStr, $orderID);
		if(!$result){
			error_log("Mysql error in ".__FILE__." -ae4w4- error:".lavu_dberror());
			return false;
		}
		if(!mysqli_num_rows($result)){
			error_log("Error in ".__FILE__." getOrderRow function did not find row in the database.");
			return false;
		}
		return mysqli_fetch_assoc($result);
	}

	function getLocationRow(){
		static $loc_row = null;
		if($loc_row){ return $loc_row; }
		$result = lavu_query("SELECT * FROM `locations` ORDER BY `id` ASC LIMIT 1");
		if(!$result){
			error_log("MYSQL Error in __FILE__ -iwu4e- error:".lavu_dberror());
		}
		$loc_row = mysqli_fetch_assoc($result);
		return $loc_row;
	}
	////////////////////////////////////////////////////////////////////////
	// ORDER CONTENT FUNCTIONS
	function selectRowsFromOrderContentsByOrderID_OrderByTaxProfile($order_id){
		//$result = lavu_query("SELECT * FROM `order_contents` WHERE `order_id`='[1]' AND `item` <> 'SENDPOINT'", $order_id);

		$query = "SELECT `order_contents`.*, `discount_types`.`title` as discount_title, `discount_types`.`label` as discount_label FROM `order_contents` ".
				 "LEFT JOIN `discount_types` ON `order_contents`.`idiscount_id`=`discount_types`.`id` ".
				 "WHERE `order_id`='[1]' AND `item`<>'SENDPOINT'";
		$result = lavu_query($query, $order_id);

		$isValidQ = validateQResult($result, __FILE__, 'a87see4', 1);
		if(!$isValidQ){
			echo "isValidQ is false";
			return false;
		}
		if(mysqli_num_rows($result) == 0){
			echo "num of rows: 0";
			return false;
		}
		$rowsArr = array();
		while($currRow = mysqli_fetch_assoc($result)){
			$rowsArr[] = $currRow;
		}
		return $rowsArr;
	}


	function getAllUsedTaxProfilesAsTaxProfIDs($order_contents){
		$taxProfilesUsedByID = array();
		foreach($order_contents as $currOrderContentRow){
			$applyTaxrateFieldParts = explode('::', $currOrderContentRow['apply_taxrate']);
			$taxProfilesUsedByID[$applyTaxrateFieldParts[0]] = $applyTaxrateFieldParts[0];
		}
		return array_values($taxProfilesUsedByID);
	}


	function orderContentsTaxProfileFieldsManuallyJoined($taxProfileRowsWhereKeyIsID, &$order_content_rows){
		foreach($order_content_rows as &$currentOrderContentRow){
			$currOrderRowTaxProfileField = $currentOrderContentRow['apply_taxrate'];
			$currOrderRowTaxProfileFieldParts = explode("::", $currOrderRowTaxProfileField);
			$currOrderRowTaxProfileID = $currOrderRowTaxProfileFieldParts[0];
			$currOrderRowTaxProfileRow = $taxProfileRowsWhereKeyIsID[$currOrderRowTaxProfileID];
			$currentOrderContentRow['tax_profile_name'] = $currOrderRowTaxProfileRow['title'];
			$currentOrderContentRow['tax_profile_id'] = $currOrderRowTaxProfileRow['id'];
		}
	}


	function getOrderContentRowsWithoutTaxProfile($order_content_rows){
		$non_taxed_order_rows = array();
		foreach($order_content_rows as $currentOrderContentRow){
			$currOrderRowTaxProfileField = $currentOrderContentRow['apply_taxrate'];
			$currOrderRowTaxProfileFieldParts = explode("::", $currOrderRowTaxProfileField);
			$currOrderRowTaxProfileID = $currOrderRowTaxProfileFieldParts[0];
			//clear$currOrderRowTaxProfileRow = $taxProfileRowsWhereKeyIsID[$currOrderRowTaxProfileID];
			if(empty($currOrderRowTaxProfileID)){
				$non_taxed_order_rows[] = $currentOrderContentRow;
			}
		}
		return $non_taxed_order_rows;
	}

	function groupOrderContentRowsByTaxProfile($order_contents){
		$taxProfAsKeyToArrayOfOrderContents = array();
		foreach($order_contents as $currOrderContentRow){
			if( empty($currOrderContentRow['tax_profile_name']) ){
				continue;
			}
			$currTaxProfID = $currOrderContentRow['tax_profile_name'];
			if( !isset($taxProfAsKeyToArrayOfOrderContents[$currTaxProfID]) ){
				$taxProfAsKeyToArrayOfOrderContents[$currTaxProfID] = array();
			}
			$taxProfAsKeyToArrayOfOrderContents[ $currOrderContentRow['tax_profile_name'] ][] = $currOrderContentRow;
		}
		return $taxProfAsKeyToArrayOfOrderContents;
	}


	function orderContentRowsToInventoryArray($orderContents, $incomeAccount, $costOfGoodsAccount, $assetAccount){
		$inventoryArray = array();
		foreach ($orderContents as $currOrderContentRow){
			$currInventoryItemArr = array();
			$currInventoryItemsArr['inventory_item_ref_full_name'] = $currOrderContentRow['item'];
			//$currInventoryItemsArr['sales_description'] = 'o_id:'  . $currOrderContentRow['order_id'] . ' fml:' . $currOrderContentRow['modifier_list_id'];//Whatever else helps link it.
			$currInventoryItemsArr['sales_description'] = "";
			$currInventoryItemsArr['income_account_ref_full_name'] = $incomeAccount;
			$currInventoryItemsArr['cogs_account_ref_full_name']   = $costOfGoodsAccount;
			$currInventoryItemsArr['asset_account_ref_full_name']  = $assetAccount;
			$inventoryArray[] = $currInventoryItemsArr;
		}
		return $inventoryArray;
	}




	function groupedOrderContentRowsToInvoiceLinesArray($order_contents_grouped_by_tax, $subtotalItemFullName, $discountRefFullName, $orderContentsQBParentItemNameOrFalse){

		$invoiceLinesArr = array();
		foreach($order_contents_grouped_by_tax as $currTaxProfileNameAsKey => $currTaxProfileGroupOfOrderContents){
			$invoiceLinesForBin = orderContentRowsToInvoiceLinesArray($currTaxProfileGroupOfOrderContents, $subtotalItemFullName, $discountRefFullName, $orderContentsQBParentItemNameOrFalse);
			$invoiceLinesArr = array_merge($invoiceLinesArr, $invoiceLinesForBin);
			//Subtotal item
			$invoiceLinesArr[] = array('item_full_name' => $subtotalItemFullName, 'description' => 'Subtotal for current tax.');
			//Subtotal tax item
			$invoiceLinesArr[] = array('item_full_name' => $currTaxProfileNameAsKey, 'description' => 'Heres the tax amount from the tax profile.');
		}
		return $invoiceLinesArr;
	}
	function orderContentRowsToInvoiceLinesArray($orderContents, $subtotalItemFullName, $discountRefFullName, $orderContentsQBParentItemNameOrFalse, $non_taxed = false){
		/*      "invoice_lines":[{"item_full_name":"hot_dog","description":"HD-Description for Invoice Line","quantity":"2","rate":"2.99"},
			                     {"item_full_name":"hamburger","description":"Ham-Description for Invoice Line","quantity":"1","rate":"6.99"},
			                     {"item_full_name":"soda","description":"Soda-Description for Invoice Line","quantity":"3","rate":"1.99"}]
		*/
		$qbParentItemStr = $orderContentsQBParentItemNameOrFalse ? $orderContentsQBParentItemNameOrFalse.":" : "";//If this is a subitem in quickbooks, then its parent is part of its full name.
	    $invoiceLinesArr = array();
	    foreach($orderContents as $orderContentRow){
	    	$currInvoiceLine = array();
	    	$currInvoiceLine['item_full_name'] = $qbParentItemStr . $orderContentRow['item'];
	    	$currInvoiceLine['description'] = "";
	    	$currInvoiceLine['quantity'] = $orderContentRow['quantity'];
	    	//$currInvoiceLine['rate'] = calculateOrderContentsRowNet($orderContentRow); //$orderContentRow['total_with_tax'] - $orderContentRow['tax_amount'];
	    	$currInvoiceLine['rate'] = $orderContentRow['price'] + $orderContentRow['modify_price'] + $orderContentRow['forced_modifiers_price'];
	    	$currInvoiceLine['is_menu_item'] = 1;
	    	if($non_taxed){
	    		$currInvoiceLine['tax_code'] = 'Non';
	    	}
	    	$invoiceLinesArr[] = $currInvoiceLine;
	    	//Apply item-level (order_content level) discounts, if applicable.
	    	appendItemLevelDiscountToInvoiceLinesArrIfApplicable($orderContentRow, $invoiceLinesArr, $discountRefFullName);
	    }
	    return $invoiceLinesArr;
	}
	function appendItemLevelDiscountToInvoiceLinesArrIfApplicable(&$orderContentRow, &$invoiceLinesArr, $discountRefFullName){
		if( empty($orderContentRow['idiscount_id']) || empty($orderContentRow['idiscount_value']) || empty($orderContentRow['idiscount_type']) )
			return;
		$currInvoiceLine_discount = array();
		$currInvoiceLine_discount['item_full_name'] = $discountRefFullName;
		$currInvoiceLine_discount['description'] = 'Discount title: '.$orderContentRow['discount_title'];

		if($orderContentRow['idiscount_type'] == 'p'){
			$currInvoiceLine_discount['rate_percent'] = $orderContentRow['idiscount_value']*100;
		}else{
			$currInvoiceLine_discount['rate'] = $orderContentRow['idiscount_value'];
		}
		$invoiceLinesArr[] = $currInvoiceLine_discount;
	}

	function appendOrderLevelSubtotalAndDiscountInvoiceLinesIfApplicable(&$mainPackageArr, &$orderRow, $subtotalItemFullName, $discountRefFullName, $needsSubtotal = false){

		if( empty($orderRow['discount_type']) || empty($orderRow['discount_value']) || empty($orderRow['discount_id'])){
			return;
		}
		$subtotalInvoiceLine = array();
		$subtotalInvoiceLine['item_full_name'] = $subtotalItemFullName;
		$subtotalInvoiceLine['description'] = "";

		$orderLevelDiscountInvoiceLine = array();
		$orderLevelDiscountInvoiceLine['item_full_name'] = $discountRefFullName;
		$orderLevelDiscountInvoiceLine['description'] = "Order level discount.";
		if($orderRow['discount_type'] == 'p'){
			$orderLevelDiscountInvoiceLine['rate_percent'] = $orderRow['discount_value']*100;
		}
		else{
			$orderLevelDiscountInvoiceLine['rate'] = $orderRow['discount_value'];
		}
		if($needsSubtotal){
			$mainPackageArr['invoice_lines'][] = $subtotalInvoiceLine;
		}
		$mainPackageArr['invoice_lines'][] = $subtotalInvoiceLine;
		$mainPackageArr['invoice_lines'][] = $orderLevelDiscountInvoiceLine;
	}

	/*
	function calculateOrderContentsRowNet($orderContentRow){
		$net = ($orderContentRow['price'] + $orderContentRow['modify_price'] + $orderContentRow['forced_modifiers_price']) * $orderContentRow['quantity'] -
				($orderContentRow['discount_amount'] + $orderContentRow['idiscount_amount'] + ($orderContentRow['quantity'] > 0 ? $orderContentRow['itax'] : 0));
		return $net;
	}
*/



	////////////////////////////////////////////////////////////////////////
	// CC_TRANSACTION FUNCTIONS
	function getCCTransactionRowsForOrderID($order_id){
		$result = lavu_query("SELECT * FROM `cc_transactions` WHERE `order_id`='[1]'", $order_id);
		$isValidQ = validateQResult($result,__FILE__,'-3afwsdhs-');
		if(!$isValidQ){
			echo "isValidQ is false ingetCCTransactions function";
			return false;
		}
		$rowsArr = array();
		while($currRow = mysqli_fetch_assoc($result)){
			$rowsArr[] = $currRow;
		}
		return $rowsArr;
	}








	////////////////////////////////////////////////////////////////////////
	// Discount functions
	// /*
	// Order Level:
	/*
	Dollar
	discount_id = 2
	discount_value = 32.5 (FOR CALCULATION)
	discount_type = d
	discount = 32.5
	discount_sh = $ 32.50 (IGNORE)

	Percentage
	discount_id =
	discount_type = p
	discount_value = 0.10 (FOR CALCULATION)
	discount = 25.00
	discount_sh = 10% Discount (IGNORE)

	Order Contents Level:
	idiscount_id = 1
	idiscount_type = p
	idiscount_value = 0.5
	idiscount_amount = 3.72
	idscount_sh = 50% off
	discount_id = IGNORE
	discount_type = IGNORE
	discount_value = IGNORE
	discount = IGNORE
	discount_sh = IGNORE
	*/
	function getOrderLevelDiscountFields($orderID){

	}






	////////////////////////////////////////////////////////////////////////
	// QUICKBOOKS SETTINGS FUNCTIONS
	function getRowFromConfigForQuickbooksSetting($setting){
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='[1]'", $setting);
		if(validateQResult($result, __FILE__, '-eo85hgei-')){
			if(mysqli_num_rows($result)){
				return mysqli_fetch_assoc($result);
			}
		}
		return false;
	}
	//To be included in a library later, code that is often repeated.
	function validateQResult($result, $file, $code){
		if(!$result){
			$error = "MySQL error in $file -$code- , mysql error:[".lavu_dberror()."]";
			error_log($error);
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////
	// MISC. UTIL FUNCTIONS
	function condenseOverflowingOrderIDToBe_11_chars($orderID){
		//TODO MAKE INTO REAL ALG. DONT CHEAT
		return substr($orderID, 0, 11);
	}

	function returnArraySortedByField($arr, $field){
		$mapForward = array();
		foreach($arr as $key => $val){
			$mapForward[$key] = $arr[$key][$field];
		}
		asort($mapForward);
		$sortedArr = array();
		foreach($mapForward as $key => $val){
			$sortedArr[$key] = $arr[$key];
		}
		return $sortedArr;
	}



?>
