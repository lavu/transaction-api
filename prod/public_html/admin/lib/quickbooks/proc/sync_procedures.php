<?php


//We run from the web browser first, so we tie it to the demo_brian_d account
//If we see this we know we are in dev anyways so we connect
ini_set('display_errors',1);
require_once(dirname(__FILE__).'/quickbooks_common_functions.php');
require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery_quickbooks.php');
require_once('/home/poslavu/public_html/admin/cp/resources/json.php');



//IF WE ARE DOING IT ORDER BY ORDER.
//sync_order('1-13', 'demo_brian_d');
function sync_order($order_id, $pDataname, $device_time){


//echo "Performing sync_order on: ".$order_id."\n";

    lavu_connect_dn($pDataname, 'poslavu_'.$pDataname.'_db');

	global $server_name; $server_name = 'lavu';
	global $security_key; $security_key = '915204710443208127187736920';
	global $security_token; $security_token = '271602091522081443877390471';
	global $dataname; $dataname = $pDataname;
	global $loc_row; $loc_row = getLocationRow();


	//taxGratuityCalculationScheme, advanced location setting, saves to location row: tax_auto_gratuity
	// 0 => Tax before Gratuity (non-taxed): Item Lines summed, subtotalled, tax applied to subtotal, summed, autogratuity (non-taxed) based on items + their tax.
	// 1 => Gratuity before tax (taxed): Item Lines summed to subtotal, gratuity applied, subtotalled, tax applied.
	// 2 => Both based on subtotal (non-taxed): Items subtotaled into subtotal A.  Tax applied to A.  Gratuity based on A without tax.
	global $taxGratuityCalculationScheme; $taxGratuityCalculationScheme = $loc_row['tax_auto_gratuity'];


	//Collect data
	$orderRow = getOrderRow($order_id);
	$order_contents = selectRowsFromOrderContentsByOrderID_OrderByTaxProfile($order_id);//Contains both taxed and tax-exempt order_contents.
	$allUsedTaxProfilesAsIDs = getAllUsedTaxProfilesAsTaxProfIDs($order_contents);
	$cc_transaction_rows = getCCTransactionRowsForOrderID($order_id);
	//organize
	$taxProfileRowsWhereKeyIsID = getTaxProfilesForGivenIDsIDAsKey($allUsedTaxProfilesAsIDs);
	orderContentsTaxProfileFieldsManuallyJoined($taxProfileRowsWhereKeyIsID, $order_contents);//Mutatively adds tax_prof-field to $order_contents.
	$order_contents_non_taxed = getOrderContentRowsWithoutTaxProfile($order_contents);
	$order_contents_grouped_by_tax = groupOrderContentRowsByTaxProfile($order_contents); //Does NOT contain tax-exempt order_contents.


	//Important... THE DEFAULT SETTING VALUES STORED IN CONFIG.
	$qbDefaultsFromConfig = getQBDefaultSettingsFromConfig();

	//Now we can build the json api query package, formulate into array, json encode then send.
	$mainPackageArr = array();

	$mainPackageArr['device_time'] = $device_time;

	//Default Customer
	$mainPackageArr['customer_ref_full_name'] = $qbDefaultsFromConfig['generalCustomerName'];//$generalCustomerName;

	// 'Accounts Receivable' account.
	$mainPackageArr['ar_account_ref_full_name'] = $qbDefaultsFromConfig['ar_account'];///$ar_account;

	//Inventory Items Parent Item
	if( !empty($qbDefaultsFromConfig['inventoryItemsParentItem']) ){ $mainPackageArr['inventory_items_parent'] = $qbDefaultsFromConfig['inventoryItemsParentItem']; };

	//Inventory Items (To be added as items if DNE within qb's)
	$mainPackageArr['inventory_items'] = orderContentRowsToInventoryArray($order_contents, $qbDefaultsFromConfig['incomeAccount'], $qbDefaultsFromConfig['cogsAccount'], $qbDefaultsFromConfig['assetAccount']);
	$mainPackageArr['date'] = date('Y-m-d'); //TODO See if it would be wise to localize this.
	$mainPackageArr['po_number'] = $order_id;

	//If items of 2 or more different tax profiles.
	if( count($order_contents_grouped_by_tax) > 1 ){
		$mainPackageArr['item_sales_tax_ref_full_name'] = $qbDefaultsFromConfig['zeroTaxItem'];
		$mainPackageArr['invoice_lines'] = groupedOrderContentRowsToInvoiceLinesArray($order_contents_grouped_by_tax,
													$qbDefaultsFromConfig['subtotalItem'], $qbDefaultsFromConfig['discountRefFullName'], $qbDefaultsFromConfig['inventoryItemsParentItem']);
		appendOrderLevelSubtotalAndDiscountInvoiceLinesIfApplicable($mainPackageArr, $orderRow, $qbDefaultsFromConfig['subtotalItem'], $qbDefaultsFromConfig['discountRefFullName'], true);
	}else{
		// TODO: FINISH THIS PART, USE THE ACTUAL TAX PROFILE THAT IS STORED IN THE ORDER CONTENTS ETC.
		$mainPackageArr['item_sales_tax_ref_full_name'] = $qbDefaultsFromConfig['itemSalesTax'];

		//$mainPackageArr['invoice_lines'] = orderContentRowsToInvoiceLinesArray($order_contents, $subtotalItem, $discountRefFullName, $inventoryItemsParentItem);
		$orderContentsUnderSingleGroupArr = array_values($order_contents_grouped_by_tax);
		$orderContentsUnderSingleGroup = $orderContentsUnderSingleGroupArr[0];
		$mainPackageArr['invoice_lines'] = orderContentRowsToInvoiceLinesArray($orderContentsUnderSingleGroup,
												$qbDefaultsFromConfig['subtotalItem'], $qbDefaultsFromConfig['discountRefFullName'], $qbDefaultsFromConfig['inventoryItemsParentItem'] );

		appendOrderLevelSubtotalAndDiscountInvoiceLinesIfApplicable($mainPackageArr,
												$orderRow, $qbDefaultsFromConfig['subtotalItem'], $qbDefaultsFromConfig['discountRefFullName']);
	}



	//IF GRATUITY EXISTS.
	//    0 => Tax before Gratuity (non-taxed)      1 => Gratuity before tax (taxed)       2 => Both based on subtotal (non-taxed)
	if($taxGratuityCalculationScheme == '0' || $taxGratuityCalculationScheme == '1' || $taxGratuityCalculationScheme == '2'){
		if($orderRow['gratuity'] > 0){
			$gratuityInvoiceLine = array();
			$gratuityInvoiceLine['item_full_name'] = $qbDefaultsFromConfig['autoGratuityFullName'];
			$gratuityInvoiceLine['description'] = '';
			$gratuityInvoiceLine['quantity'] = '1';
			$gratuityInvoiceLine['rate'] = $orderRow['gratuity'];
			$gratuityInvoiceLine['is_menu_item'] = '0';
			$gratuityInvoiceLine['is_gratuity_line'] = '1';
			if ($taxGratuityCalculationScheme == '0' || $taxGratuityCalculationScheme == '2') { $gratuityInvoiceLine['tax_code'] = 'Non'; }
			$mainPackageArr['invoice_lines'][] = $gratuityInvoiceLine;
		}
	}





	//IF ENTIRE ORDER IS TAX EXEMPT, THEN WE JUST MAKE ALL ITEMS TAX EXEMPT.
	if(!empty($orderRow['tax_exempt'])){  //TODO, find if tax exempt order, put here <-------------------------------
		$invoiceLines = &$mainPackageArr['invoice_lines'];
		taxExemptAllItemsInInvoiceLinesMutatively($invoiceLines);
	}

	//We check for individual order_contents items that are taxed exempt.  These are appended to the invoice.
	if(in_array(0, $allUsedTaxProfilesAsIDs)){
		$invoiceLines = &$mainPackageArr['invoice_lines'];
		addTaxExemptedInvoiceLines($invoiceLines, $order_contents_non_taxed, $qbDefaultsFromConfig);
	}


//echo "Invoice Lines:" . print_r($invoiceLines,1);


	//If voided, we reset to keep the logic to void an order separate so we may treat it modularly.
	ifOrderVoidedSetInvoiceLineAmountsToZero($orderRow, $mainPackageArr);

	//A payment to each (non-refund) cc_transaction row.
	$mainPackageArr['receive_payment_and_method_amounts_arr'] = createPaymentsFromCCTransactions($cc_transaction_rows, $order_row);

	//Post Array
	$postArray['function'] = 'ensureItems_createInvoice_addPayments';
	$postArray['data_name'] = $dataname;
	$postArray['server_name'] = $server_name;
	$postArray['security_key'] = $security_key;
	$postArray['security_token'] = $security_token;
	$postArray['security_hash'] = sha1( $postArray['server_name'] . $postArray['function'] . $postArray['data_name'] . $postArray['security_token'] );
	$postArray['response_url'] = '';
	$postArray['func_args'] = LavuJson::json_encode($mainPackageArr);




	//-CURL-
	$postData = http_build_query($postArray);
	$isDev = strpos(__FILE__, '/dev/') !== false;
	$url = getQBsAPIPathFromDB($isDev) . '/qbv1/'.'qb_api_service/qbapi.php';
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	$api_call_response = curl_exec($ch);
	error_log("GOT RESPONSE FROM QB_API: " . $api_call_response);
	curl_close($ch);

	//JSON-ify.
	$apiResponseObj = LavuJson::json_decode($api_call_response, 1);

	return array("original_response" => $api_call_response, "response_object" => $apiResponseObj);
}

//Linear Function Chain for `sync_order` (These are called in order top to bottom)
//--getQBDefaultSettingsFromConfig
//Returns an array with all the needed quickbooks config fields.
function getQBDefaultSettingsFromConfig(){
	$settingsArr = array();

	$generalCustomerNameArr = getRowFromConfigForQuickbooksSetting('quickbooks_default_customer_ref_full_name');
	$settingsArr['generalCustomerName'] = $generalCustomerNameArr['value'];
	$cogsAccountArr   = getRowFromConfigForQuickbooksSetting('quickbooks_default_cogs_account_ref_full_name');
	$settingsArr['cogsAccount'] = $cogsAccountArr['value'];
	$incomeAccountArr = getRowFromConfigForQuickbooksSetting('quickbooks_default_income_account_ref_full_name');
	$settingsArr['incomeAccount'] = $incomeAccountArr['value'];
	$assetAccountArr  = getRowFromConfigForQuickbooksSetting('quickbooks_default_asset_account_ref_full_name');
	$settingsArr['assetAccount'] = $assetAccountArr['value'];
	$itemSalesTaxArr  = getRowFromConfigForQuickbooksSetting('quickbooks_default_item_sales_tax_ref_full_name');
	$settingsArr['itemSalesTax'] = $itemSalesTaxArr['value'];
	$zeroTaxItemArr = getRowFromConfigForQuickbooksSetting('quickbooks_default_zero_tax_ref_full_name');
	$settingsArr['zeroTaxItem'] = $zeroTaxItemArr['value'];
	$subtotalItemArr = getRowFromConfigForQuickbooksSetting('quickbooks_default_subtotal_ref_full_name');
	$settingsArr['subtotalItem'] = $subtotalItemArr['value'];
	$discountRefFullNameArr = getRowFromConfigForQuickbooksSetting('quickbooks_default_discount_ref_full_name');
	$settingsArr['discountRefFullName'] = $discountRefFullNameArr['value'];
	$autoGratuityArr = getRowFromConfigForQuickbooksSetting('quickbooks_default_autogratuity_ref_full_name');
	$settingsArr['autoGratuityFullName'] = $autoGratuityArr['value'];

	//Inventory Items Parent
	$inventoryItemsParentItemArr = getRowFromConfigForQuickbooksSetting('quickbooks_default_menu_items_parent');
	$inventoryItemsParentItem = $inventoryItemsParentItemArr['value'];
	$inventoryItemsParentItem = trim($inventoryItemsParentItem);
	$inventoryItemsParentItem = !empty( $inventoryItemsParentItem ) ? $inventoryItemsParentItem : false;
	$settingsArr['inventoryItemsParentItem'] = $inventoryItemsParentItem;

	//For payments
	$ar_account_arr = getRowFromConfigForQuickbooksSetting('quickbooks_default_accounts_receivable_full_name');
	$settingsArr['ar_account'] = $ar_account_arr['value'];

	return $settingsArr;
}

function createPaymentsFromCCTransactions($cc_transaction_rows, $order_row){
	$cardTypeMapFromBackendToQuickbooks = array('AMEX' => 'American Express',
												'VISA' => 'Visa',
												'MASTERCARD' => 'MasterCard',
												'DISCOVER' => 'Discover');
	$receivePaymentAndMethodAmountsArr = array();
	foreach($cc_transaction_rows as $currCCTransactionRow){
		//If it is a refund we put it into a different array and continue
		if($currCCTransactionRow['action'] != 'Sale'){ continue; }

		//Here we set what the payment method will be based on the cc_trans row.
		$paymentRefFullName;
		if($currCCTransactionRow['pay_type'] == "Cash"){
			$paymentRefFullName = "Cash";
		}
		else if($currCCTransactionRow['pay_type'] == 'Card'){
			$cardType = $currCCTransactionRow['card_type'];
			$cardTypeUpper = strtoupper($cardType);
			if( isset($cardTypeMapFromBackendToQuickbooks[$cardTypeUpper]) ){
				$paymentRefFullName = $cardTypeMapFromBackendToQuickbooks[$cardTypeUpper];
			}else{
				$paymentRefFullName = empty($cardType) ? 'Generic Credit Card' : $cardType;
			}
		}
		$currPayment = array();
		$currPayment['servers_payment_id'] = $currCCTransactionRow['id'];
		$currPayment['total_amount'] = $currCCTransactionRow['amount']; //---------------------------- HERE PERFORM A GUARD IF IT IS VOIDED.------------ < < <
		$currPayment['payment_method_ref_full_name'] = $paymentRefFullName;
		$receivePaymentAndMethodAmountsArr[] = $currPayment;
	}
	return $receivePaymentAndMethodAmountsArr;
}

function createCreditsFromRefundCCTransactions(){
	/* TODO */
}

function ifOrderVoidedSetInvoiceLineAmountsToZero(&$order_row, &$mainSendPackage){
	$mainSendPackage['invoice_is_void'] = "0";
	if( empty($order_row['void']) ){ return; }
	$invoiceLines = &$mainSendPackage['invoice_lines'];
	foreach($invoiceLines as &$currInvoiceLine){
			if( empty($currInvoiceLine['is_menu_item']) ){ continue; }
			if( empty($currInvoiceLine['is_gratuity_line'])){ continue; }
			// The invoice lines prior to this loop have the following fields: item_full_name, description, quantity, and rate.
			// We are trying to set total amount=0 with respect to 'voiding' the order.
	    	$currInvoiceLine['description'] = "-VOIDED- previous rate:" . $currInvoiceLine['rate'] . ". " . $currInvoiceLine['description'];
	    	$currInvoiceLine['rate'] = "0.00";
	}
	$mainSendPackage['invoice_is_void'] = '1';
	$mainSendPackage['invoice_memo'] = "Void authorized by: ".$order_row['auth_by'].". Reason for voiding order: ".$order_row['void_reason'].". ";
}

//----------------------------------------------------------------------------------------------------------------------
// Other helper functions.
function getQBsAPIPathFromDB($isDev){//https://api.poslavu.com/lib/quickbooks/qbapi/
    if($isDev){
	   $result = mlavu_query("SELECT * FROM `poslavu_LAVU_QUICKBOOKS_db`.`config` WHERE `setting`='qb_api_server_url_dev'");
    }else{
       $result = mlavu_query("SELECT * FROM `poslavu_LAVU_QUICKBOOKS_db`.`config` WHERE `setting`='qb_api_server_url'"); 
    }
    if(!$result){ error_log("MySQL error in ".__FILE__." -98w45h- error: ".mlavu_dberror()); } //TODO javascript alert error or something.
    if(!mysqli_num_rows($result)){ error_log("Could not find quickbooks api uri from db. In file ".__FILE__.""); }
    $row = mysqli_fetch_assoc($result);
    return $row['value'];
}

function taxExemptAllItemsInInvoiceLinesMutatively(&$invoice_lines){
	foreach($invoice_lines as &$curr_invoice_line){
		if( !empty($curr_invoice_line['is_menu_item']) ){
			$curr_invoice_line['tax_code'] = 'Non';
		}
	}
}

function addTaxExemptedInvoiceLines(&$invoiceLines, $order_contents_non_taxed, $qbDefaultsFromConfig){
	/*
		$mainPackageArr['invoice_lines'] = orderContentRowsToInvoiceLinesArray($order_contents,
												$qbDefaultsFromConfig['subtotalItem'], $qbDefaultsFromConfig['discountRefFullName'], $qbDefaultsFromConfig['inventoryItemsParentItem'] );

		appendOrderLevelSubtotalAndDiscountInvoiceLinesIfApplicable($mainPackageArr,
												$orderRow, $qbDefaultsFromConfig['subtotalItem'], $qbDefaultsFromConfig['discountRefFullName']);
	 */
	//function orderContentRowsToInvoiceLinesArray($orderContents, $subtotalItemFullName, $discountRefFullName, $orderContentsQBParentItemNameOrFalse, $taxExempt = false){ /// <---------------USE THIS
	$taxExemptItemsInvoiceLines = orderContentRowsToInvoiceLinesArray($order_contents_non_taxed,
												$qbDefaultsFromConfig['subtotalItem'], $qbDefaultsFromConfig['discountRefFullName'], $qbDefaultsFromConfig['inventoryItemsParentItem'], true );
	foreach($taxExemptItemsInvoiceLines as $currLine){
		$invoiceLines[] = $currLine;
	}
}
























?>
