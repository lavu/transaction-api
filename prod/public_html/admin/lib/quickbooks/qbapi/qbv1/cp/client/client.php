<?php
    session_start();
    ini_set('display_errors',1);
    if(!empty($_REQUEST['sign_out'])){
        $_SESSION['is_signed_in'] = false;
        session_destroy();
    }
    if( empty($_SESSION['is_signed_in']) ){
        
        require_once(dirname(__FILE__).'/client_login.php');
        if(empty($_SESSION['is_signed_in'])){
            exit;
        }
    }
    //print_r($_SESSION);
    //session_destroy();
?>
<html>
<body>
</body>
<script language='javascript' type='text/javascript' src='../../common_code/vkbeautify.0.99.00.beta.js'></script>
<script type='text/javascript'>

    //document.body.innerHTML =  '<pre>' + sanitizeXML( vkbeautify.xml('<hello world><qbxml></qbxml></hello world>') ) + '</pre>';
    //return;
    
<?php
    echo "var data_name = '".$_SESSION['data_name']."';\n";
    echo "var sec_hash = '".sha1($_SESSION['data_name']."trees")."';\n";
    echo "var server = '".$_SESSION['server']."';\n";
?>
    
    var topHeaderPanel = document.createElement('div');
    //topHeaderPanel.style.height = '100px';
    topHeaderPanel.style.backgroundColor = '#CCC';
    topHeaderPanel.innerHTML = "<a style='float:right' href='?sign_out=true'>Sign Out</a> <br>Server: "+server+"<br>Dataname: "+data_name+"<br>";
    
    var backgroundDiv = document.createElement('div');
    backgroundDiv.style.display = 'inline-block';
    
    var displayDiv = document.createElement('div');
    displayDiv.style.border = '2px solid gray';
    displayDiv.style.display = 'inline-block';
    displayDiv.style.textAlign = 'center';
    displayDiv.style.margin = '0px auto';
    
    var ordersListDiv = document.createElement('div');
    ordersListDiv.style.display = 'inline-block';
    ordersListDiv.style.border = '1px solid black';
    ordersListDiv.style.width = '300px';
    ordersListDiv.style.height = '650px';
    ordersListDiv.style.overflowY = 'scroll';
    
    var ordersListTable = document.createElement('table');
    ordersListTable.setAttribute('Border', "0");
    ordersListTable.setAttribute('cell-padding','0');
    ordersListTable.style.width = '100%';
    //ordersListTable.style.height = '100%';
    ordersListTable.style.border = '1px solid green';
    
    var orderDetailsDiv = document.createElement('div');
    orderDetailsDiv.style.display = 'inline-block';
    orderDetailsDiv.style.width = '900px';
    orderDetailsDiv.style.height = '650px';
    orderDetailsDiv.style.border = '1px solid green';
    orderDetailsDiv.style.overflow = 'hidden';
    
    
    document.body.appendChild(backgroundDiv);
    backgroundDiv.appendChild(topHeaderPanel);
    backgroundDiv.appendChild(displayDiv);
    displayDiv.appendChild(ordersListDiv);
    ordersListDiv.appendChild(ordersListTable);
    displayDiv.appendChild(orderDetailsDiv);
    
    //ajax('client_requests.php', 'function=get_queue_orders_by_date_range&start_date_time=0&end_date_time=9999999&server=lavu&data_name=demo_brian_d', receiveOrdersListDataAndLoadOrdersListTable);
    ajax('client_requests.php', 
            'function=get_queue_orders_by_date_range&start_date_time=0&end_date_time=9999999&server='+server+'&data_name='+data_name+'&sec_hash='+sec_hash, 
            receiveOrdersListDataAndLoadOrdersListTable);
    
    var currentlySelectedOrderIDListElement = null;
    var currentlySelectedRequestResponseTitleElement = null;
    
    function receiveOrdersListDataAndLoadOrdersListTable(responseText){
        
        var rowIndexData = JSON.parse(responseText);
        
        var titleRow = document.createElement('tr');
        titleRow.style.textAlign = 'center';
        titleRow.vAlign = 'bottom';
        
        var td1 = document.createElement('td');
        td1.innerHTML = 'Order ID';
        td1.style.borderBottom = '1px solid black';
        
        var td2 = document.createElement('td');
        td2.innerHTML = 'Device Time';
        td2.style.borderBottom = '1px solid black';
        
        titleRow.appendChild(td1);
        titleRow.appendChild(td2);
        ordersListTable.appendChild(titleRow);
        
        for(var i = 0; i < rowIndexData['rows'].length; i++){
            var nextTR = document.createElement('tr');
            nextTR.style.border='1px solid blue';
            nextTR.queueID = rowIndexData['rows'][i]['id'];
            nextTR.onclick = function(){ 
                ajax('client_requests.php', 'function=get_specified_queue&server='+server+'&data_name='+data_name+'&sec_hash='+sec_hash+'&queue_row_id='+this.queueID, handleQueueRowDetailsUpdate);
                if(currentlySelectedOrderIDListElement)
                    currentlySelectedOrderIDListElement.style.backgroundColor = '#FFF';
                currentlySelectedOrderIDListElement = this;
                currentlySelectedOrderIDListElement.style.backgroundColor = '#DFF';
            };
            nextTR.style.fontSize = '16px';
            nextTR.style.textAlign = 'center';
            nextTR.style.cursor = 'pointer';
            nextTR.style.height = '30px';
            
            var td_1 = document.createElement('td');
            td_1.innerHTML = rowIndexData['rows'][i]['order_id'];
            td_1.vAlign = 'bottom';
            td_1.style.borderBottom = '1px solid blue';
            td_1.style.width = '20%';
            
            var td_2 = document.createElement('td');
            td_2.innerHTML = "&nbsp;"+rowIndexData['rows'][i]['device_time'];
            td_2.vAlign = 'bottom';
            td_2.style.borderBottom = '1px solid blue';
            td_2.style.width = '80%';
            
            nextTR.appendChild(td_1);
            nextTR.appendChild(td_2);
            ordersListTable.appendChild(nextTR);
        }
    }
    
    
    ///////      D E T A I L S      A R E A 
    
    function handleQueueRowDetailsUpdate(responseText){

        responseText = responseText.replace(/_NEWLINE_/g,"\\n");
        
        
        //orderDetailsDiv.innerHTML = "<pre>"+responseText+"</pre>";
        //return;
        
        
        var data = JSON.parse(responseText.toString());
        var sendRequestXMLsOnly = getXMLsOnly(data['sendRequestXMLs'] ,'::::::::PARTIAL_START::::::::','::::::::PARTIAL_METADATA:::::');
        var receiveResponsesOnly = getXMLsOnly(data['receiveResponseXMLs'] ,'::::::::PARTIAL_START::::::::','::::::::PARTIAL_METADATA:::::');
        
        //Parsing the xmls and pulling the qbxml main node's name.
        var sendRequests = getListOfSendRequestsFromXML(sendRequestXMLsOnly);
        var receiveResponses = getListOfReceiveResponsesFromXML(receiveResponsesOnly);
        
        //Remove GUI
        recursivelyRemoveChildren(orderDetailsDiv, 0);
        
        //Build GUI
        var topPanelDiv = document.createElement('div');
        topPanelDiv.style.margin = '0px auto';
        topPanelDiv.style.backgroundColor = '#ddd';
        topPanelDiv.style.height = '300px';
        topPanelDiv.style.width = '100%';
        topPanelDiv.style.overflowY = 'scroll';
        topPanelDiv.style.border = '1px solid green';
        
        var mainAreaDiv = document.createElement('div');
        mainAreaDiv.style.height = '650px';
        mainAreaDiv.style.backgroundColor = '#8F8';
        
        var debugXMLDisplayDiv = document.createElement('div');
        debugXMLDisplayDiv.style.display = 'inline-block';
        debugXMLDisplayDiv.style.width = '100%';
        debugXMLDisplayDiv.style.height = '350px';
        debugXMLDisplayDiv.style.backgroundColor = '#CFF';
        debugXMLDisplayDiv.style.textAlign = 'left';
        debugXMLDisplayDiv.style.overflow = 'scroll';
       
        
        
        //The information table
        var informationTable = document.createElement('table');
        informationTable.style.width = '100%';
        var t_td1, t_td2, t_td3, t_td4, curr_row;
        
        //Row  --------------
        curr_row = document.createElement('tr');
        //td1
        t_td1 = document.createElement('td');//td 1
        t_td1.innerHTML = 'Dataname';
        t_td1.style.border = '1px solid black';
        t_td1.style.width = '15%';
        curr_row.appendChild(t_td1);
        //td2
        t_td2 = document.createElement('td');//td 2
        t_td2.innerHTML = data_name;
        t_td2.style.border = '1px solid black';
        t_td2.style.width = '35%';
        curr_row.appendChild(t_td2);
        //td3
        t_td3 = document.createElement('td');//td 3
        t_td3.innerHTML = 'Server';
        t_td3.style.border = '1px solid black';
        t_td3.style.width = '15%';
        curr_row.appendChild(t_td3);
        //td4
        t_td4 = document.createElement('td');//td 4
        t_td4.innerHTML = server;
        t_td4.style.border = '1px solid black';
        t_td4.style.width = '35%';
        curr_row.appendChild(t_td4);
        informationTable.appendChild(curr_row);
        
        //Row  --------------
        curr_row = document.createElement('tr');//tr 1
        //td1
        t_td1 = document.createElement('td');//td 1
        t_td1.innerHTML = 'Order ID';
        t_td1.style.border = '1px solid black';
        t_td1.style.width = '15%';
        curr_row.appendChild(t_td1);
        //td2
        t_td2 = document.createElement('td');//td 2
        t_td2.innerHTML = data['order_id'];
        t_td2.style.border = '1px solid black';
        t_td2.style.width = '35%';
        curr_row.appendChild(t_td2);
        //td3
        t_td3 = document.createElement('td');//td 3
        t_td3.innerHTML = 'Device Time';
        t_td3.style.border = '1px solid black';
        t_td3.style.width = '15%';
        curr_row.appendChild(t_td3);
        //td4
        t_td4 = document.createElement('td');//td 4
        t_td4.innerHTML = data['device_time'];
        t_td4.style.border = '1px solid black';
        t_td4.style.width = '35%';
        curr_row.appendChild(t_td4);
        informationTable.appendChild(curr_row);
        
        //Row  --------------
        curr_row = document.createElement('tr');//tr 2
        //td1
        t_td1 = document.createElement('td');//td 1
        t_td1.innerHTML = 'TXN ID';
        t_td1.style.border = '1px solid black';
        t_td1.style.width = '15%';
        curr_row.appendChild(t_td1);
        //td2
        t_td2 = document.createElement('td');//td 2
        t_td2.innerHTML = data['txn_id'];
        t_td2.style.border = '1px solid black';
        t_td2.style.width = '35%';
        curr_row.appendChild(t_td2);
        //td3
        t_td3 = document.createElement('td');//td 3
        t_td3.innerHTML = 'Invoice Number';
        t_td3.style.border = '1px solid black';
        t_td3.style.width = '15%';
        curr_row.appendChild(t_td3);
        //td4
        t_td4 = document.createElement('td');//td 4
        t_td4.innerHTML = data['invoice_number'];
        t_td4.style.border = '1px solid black';
        t_td4.style.width = '35%';
        curr_row.appendChild(t_td4);
        informationTable.appendChild(curr_row);
        
        //Row  --------------
        curr_row = document.createElement('tr');//tr 3
        //td1
        t_td1 = document.createElement('td');//td 1
        t_td1.innerHTML = 'Queue Ticket';
        t_td1.style.border = '1px solid black';
        t_td1.style.width = '15%';
        curr_row.appendChild(t_td1);
        //td2
        t_td2 = document.createElement('td');//td 2
        t_td2.innerHTML = data['ticket_session_identifier'];
        t_td2.style.border = '1px solid black';
        t_td2.style.width = '35%';
        curr_row.appendChild(t_td2);
        //td3
        t_td3 = document.createElement('td');//td 3
        t_td3.innerHTML = 'Server Time Created';
        t_td3.style.border = '1px solid black';
        t_td3.style.width = '15%';
        curr_row.appendChild(t_td3);
        //td4
        t_td4 = document.createElement('td');//td 4
        t_td4.innerHTML = data['server_date_time_created'];
        t_td4.style.border = '1px solid black';
        t_td4.style.width = '35%';
        curr_row.appendChild(t_td4);
        informationTable.appendChild(curr_row);
        
        //Row  --------------
        var curr_row = document.createElement('tr');//tr 4
        //td1
        t_td1 = document.createElement('td');//td 1
        t_td1.innerHTML = 'Server Time Pulled';
        t_td1.style.border = '1px solid black';
        t_td1.style.width = '15%';
        curr_row.appendChild(t_td1);
        //td2
        t_td2 = document.createElement('td');//td 2
        t_td2.innerHTML = data['server_date_time_pulled'];
        t_td2.style.border = '1px solid black';
        t_td2.style.width = '35%';
        curr_row.appendChild(t_td2);
        //td3
        t_td3 = document.createElement('td');//td 3
        t_td3.innerHTML = 'Server Time Closed';
        t_td3.style.border = '1px solid black';
        t_td3.style.width = '15%';
        curr_row.appendChild(t_td3);
        //td4
        t_td4 = document.createElement('td');//td 4
        t_td4.innerHTML = data['server_date_time_closed'];
        t_td4.style.border = '1px solid black';
        t_td4.style.width = '35%';
        curr_row.appendChild(t_td4);
        informationTable.appendChild(curr_row);
        
        //Row  --------------
        var curr_row = document.createElement('tr');//tr 5
        //td1
        t_td1 = document.createElement('td');//td 1
        t_td1.innerHTML = 'Action Index';
        t_td1.style.border = '1px solid black';
        t_td1.style.width = '15%';
        curr_row.appendChild(t_td1);
        //td2
        t_td2 = document.createElement('td');//td 2
        t_td2.innerHTML = data['action_index'];
        t_td2.style.border = '1px solid black';
        t_td2.style.width = '35%';
        curr_row.appendChild(t_td2);
        //td3
        t_td3 = document.createElement('td');//td 3
        t_td3.innerHTML = 'Total Actions';
        t_td3.style.border = '1px solid black';
        t_td3.style.width = '15%';
        curr_row.appendChild(t_td3);
        //td4
        t_td4 = document.createElement('td');//td 4
        t_td4.innerHTML = data['total_actions'];
        t_td4.style.border = '1px solid black';
        t_td4.style.width = '35%';
        curr_row.appendChild(t_td4);
        informationTable.appendChild(curr_row);
        
        
        
        topPanelDiv.appendChild(informationTable);
        
        
        
        
        var requestResponseListTable = document.createElement('table');
        requestResponseListTable.style.width='100%';
        for(var i = 0; i < sendRequests.length || i < receiveResponses.length; i++){
            var currRow = document.createElement('tr');
            currRow.style.cursor = 'pointer';
            //Request
            var td1 = document.createElement('td');
            td1.style.border = '1px solid black';
            td1.style.width = '50%';
            td1.style.backgroundColor = '#BFB';
            td1.index = i;
            td1.setAttribute('colspan', '2');
            td1.onclick = function(){
                var innerHTML = sendRequestXMLsOnly[this.index];
                innerHTML = vkbeautify.xml(innerHTML);
                innerHTML = sanitizeXML(innerHTML);
                innerHTML = "<pre>"+innerHTML+"</pre>";
                debugXMLDisplayDiv.innerHTML = innerHTML;
                
                if(currentlySelectedRequestResponseTitleElement)
                    currentlySelectedRequestResponseTitleElement.style.backgroundColor = '#BFB';
                currentlySelectedRequestResponseTitleElement = this;
                currentlySelectedRequestResponseTitleElement.style.backgroundColor = '#FBB';
            };
            td1.innerHTML = i < sendRequests.length ? sendRequests[i] : 'DNE';
            currRow.appendChild(td1);
            //Response
            var td2 = document.createElement('td');
            td2.style.border = '1px solid black';
            td2.style.width = '40%';
            td2.style.backgroundColor = '#BFB';
            td2.index = i;
            td2.setAttribute('colspan', '1');
            td2.onclick = function(){
               var innerHTML = receiveResponsesOnly[this.index];
               innerHTML = vkbeautify.xml(innerHTML);
               innerHTML = sanitizeXML(innerHTML);
               innerHTML = "<pre>"+innerHTML+"</pre>";
               debugXMLDisplayDiv.innerHTML = innerHTML;
               
                if(currentlySelectedRequestResponseTitleElement)
                    currentlySelectedRequestResponseTitleElement.style.backgroundColor = '#BFB';
                currentlySelectedRequestResponseTitleElement = this;
                currentlySelectedRequestResponseTitleElement.style.backgroundColor = '#FBB';
            };
            td2.innerHTML = i < receiveResponses.length ? receiveResponses[i] : 'DNE';
            currRow.appendChild(td2);
            //Response Status
            var td3 = document.createElement('td');
            td3.style.border = '1px solid black';
            td3.style.width = '10%';
            td3.index = i;
            td3.setAttribute('colspan', '1');
            currRow.appendChild(td3);
            
            requestResponseListTable.appendChild(currRow);
        }
        topPanelDiv.appendChild(requestResponseListTable);

        //Append the subcomponents
        mainAreaDiv.appendChild(topPanelDiv);
        mainAreaDiv.appendChild(debugXMLDisplayDiv);
        orderDetailsDiv.appendChild(mainAreaDiv);
    }
    
    
    
    //function handleRequestForXMLDisplay
    
    
    function displayXML(xmlStr, debugXMLDisplayDiv){
        debugXMLDisplayDiv.innerHTML = sanitizeXML(xmlStr);
    }
    
    
    //UTILITY FUNCTIONS BELOW.
    function sanitizeXML(xmlstr){
        return xmlstr.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'/g,'&apos;').replace(/"/g,'&quot;');
    }
    function ajax(url, urlEncodedPostVars, responseFunction){
        var xmlhttp;
        if(window.XMLHttpRequest){
            xmlhttp = new XMLHttpRequest();
        }
        else{
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
            if(xmlhttp.readyState==4 && xmlhttp.status==200){
                responseFunction(xmlhttp.responseText);
            }
        }
        xmlhttp.open("POST", url, true);
        xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xmlhttp.send(urlEncodedPostVars);
    }
    function getXMLsOnly(sendRequestXMLsArrWMeta, start_delimiter, meta_delimiter){
        var xmlsOnly = [];
        sendRequestXMLsArrWMetaArr = sendRequestXMLsArrWMeta.split(start_delimiter);
        for(var i = 1; i < sendRequestXMLsArrWMetaArr.length; i++){
            var currXMLOnly = sendRequestXMLsArrWMetaArr[i].split(meta_delimiter);
            xmlsOnly.push(currXMLOnly[0]);
        }
        return xmlsOnly;
    }
    function getListOfSendRequestsFromXML(sendRequestXMLs){
        var nodeNameArr = [];
        for(var i = 0; i < sendRequestXMLs.length; i++){
            var currXMLText = sendRequestXMLs[i];
            
            //We have to remove any info verion info tags.  <qMark ... qMark>.
            currXMLText = currXMLText.replace(/<\?.*\?>/g, "");
            if(window.DOMParser){
                parser=new DOMParser();
                xmlDoc=parser.parseFromString(currXMLText,"text/xml");
            }
            else{ // IE
                xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async=false;
                xmlDoc.loadXML(currXMLText);
            }
            var currNodeName = xmlDoc.getElementsByTagName("QBXMLMsgsRq")[0].childNodes[0].nodeName;
            nodeNameArr.push(currNodeName);
        }
        return nodeNameArr;
    }
    function getListOfReceiveResponsesFromXML(receiveResponseXMLs){
        var nodeNameArr = [];
        for(var i = 0; i < receiveResponseXMLs.length; i++){
            var currXMLText = receiveResponseXMLs[i];
            
            //We have to remove any info verion info tags.  <qMark ... qMark>.
            currXMLText = currXMLText.replace(/<\?.*\?>/g, "");
            if(window.DOMParser){
                parser=new DOMParser();
                xmlDoc=parser.parseFromString(currXMLText,"text/xml");
            }
            else{ // IE
                xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async=false;
                xmlDoc.loadXML(currXMLText);
            }
            var currNodeName = xmlDoc.getElementsByTagName("QBXMLMsgsRs")[0].childNodes[1].nodeName;
            nodeNameArr.push(currNodeName);
        }
        return nodeNameArr;
    }
    
    function recursivelyRemoveChildren(rootElem, depth){
        while(rootElem.childNodes.length){  recursivelyRemoveChildren(rootElem.childNodes[0], depth + 1); }
        if(depth) { rootElem.parentNode.removeChild(rootElem);}
    }
</script>
</html>