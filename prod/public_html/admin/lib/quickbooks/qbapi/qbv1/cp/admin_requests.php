<?php
    
    
    require_once(dirname(__FILE__).'/shared_functions.php'); 
    
    $function = $_REQUEST['function'];
    if($function == 'get_super_admin_monitoring_data'){
        $returnArr = get_super_admin_monitoring_data();
        echo LavuJson::json_encode($returnArr);
        exit;
    }
    
    function get_super_admin_monitoring_data(){
        $returnArr = array();
        $returnArr['queue_rows_count'] = getTotalQueueCount();
        $returnArr['distinct_data_names'] = getDistinctDatanamesOnQueue();
        return $returnArr;
    }
    
    
?>