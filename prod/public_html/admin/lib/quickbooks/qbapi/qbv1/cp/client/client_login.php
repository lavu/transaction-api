<?php
    if( !empty($_POST['server'])       &&
        !empty($_POST['data_name'])    &&
        !empty($_POST['qb_user_name']) &&
        !empty($_POST['qb_password']) )
    {
        require_once(dirname(__FILE__).'/../shared_functions.php');
        $pwhash = sha1($_REQUEST['qb_password'].'JosephStalin');
        $getAccountQueryStr = "SELECT * FROM `accounts` WHERE `data_name`='[1]' AND `user_name`='[2]' AND `user_password_hash`='[3]'";
        $result = sec_query($getAccountQueryStr, $_REQUEST['server'].'_'.$_REQUEST['data_name'], $_REQUEST['qb_user_name'], $pwhash);
        if(!$result){
            echo "MySQL ERROR";
            exit;
        }
        else if(mysqli_num_rows($result)){
            foreach($_POST as $key => $val)
                $_SESSION[$key] = $val;
            $_SESSION['is_signed_in'] = true;
            return;
        }else{
            echo 'account not found';
            exit;
        }
    }
?>

<html>
<body>
<script type='text/javascript'>
    var backgroundDiv = document.createElement('div');
    backgroundDiv.style.border = '1px solid gray';
    backgroundDiv.style.backgroundColor = '#CCC';
    backgroundDiv.style.display = 'inline-block';
    backgroundDiv.style.width = '500px';
    backgroundDiv.style.height = '300px';
    
    var submit_form = document.createElement('form');
    submit_form.setAttribute('method', 'post');
    
    var server_input = document.createElement('input');
    server_input.style.border = '1px solid gray';
    server_input.style.backgroundColor = '#EEE';
    server_input.style.width = '300px';
    server_input.style.height = '30px';
    server_input.setAttribute('placeholder', 'Server');
    server_input.setAttribute('name', 'server');
    
    var dn_input = document.createElement('input');
    dn_input.style.border = '1px solid gray';
    dn_input.style.backgroundColor = '#EEE';
    dn_input.style.width = '300px';
    dn_input.style.height = '30px';
    dn_input.setAttribute('placeholder', 'Dataname');
    dn_input.setAttribute('name', 'data_name');
    
    var un_input = document.createElement('input');
    un_input.style.border = '1px solid gray';
    un_input.style.backgroundColor = '#EEE';
    un_input.style.width = '300px';
    un_input.style.height = '30px';
    un_input.setAttribute('placeholder', 'Quickbooks Username');
    un_input.setAttribute('name', 'qb_user_name');
    
    var pw_input = document.createElement('input');
    pw_input.style.border = '1px solid gray';
    pw_input.style.backgroundColor = '#EEE';
    pw_input.style.width = '300px';
    pw_input.style.height = '30px';
    pw_input.setAttribute('type', 'password');
    pw_input.setAttribute('placeholder', 'Quickbooks Password');
    pw_input.setAttribute('name', 'qb_password');
    
    var submit = document.createElement('button');
    submit.style.border = '1px solid gray';
    submit.style.backgroundColor = '#EEE';
    submit.style.width = '200px';
    submit.style.height = '40px';
    submit.onclick = function() {submit_form.submit();};
    submit.innerHTML = 'Submit';
    
    backgroundDiv.appendChild(submit_form);
    submit_form.innerHTML += '<br>&nbsp;&nbsp;&nbsp;';
    submit_form.appendChild(server_input);
    submit_form.innerHTML += '<br><br>&nbsp;&nbsp;&nbsp;';
    submit_form.appendChild(dn_input);
    submit_form.innerHTML += '<br><br>&nbsp;&nbsp;&nbsp;';
    submit_form.appendChild(un_input);
    submit_form.innerHTML += '<br><br>&nbsp;&nbsp;&nbsp;';
    submit_form.appendChild(pw_input);
    submit_form.innerHTML += '<br><br>&nbsp;&nbsp;&nbsp;';
    submit_form.appendChild(submit);
    document.body.appendChild(backgroundDiv);

</script>
</body>
</html>