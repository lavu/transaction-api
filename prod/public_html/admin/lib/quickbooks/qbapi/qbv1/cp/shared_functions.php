<?php
    //require_once('/home/eventrentalsystems/public_html/cp/functions.php');
    require_once(dirname(__FILE__).'/../common_code/json.php');
    //$Conn = ers_connect_db();
    require_once(dirname(__FILE__).'/../common_code/quickbook_functions.php');
    
    
    function getTotalQueueCount(){
        $result = sec_query("SELECT COUNT(*) AS `total_rows` FROM `queue`");
        if(!$result){error_log("MySQL error in ".__FILE__." -vfcn3- error:".sec_dberror()); return false;}
        $row = mysqli_fetch_assoc($result);
        return $row['total_rows'];
    }
    
    function getDistinctDatanamesOnQueue($filter = false){
        $result = sec_query("SELECT DISTINCT(`data_name`) FROM `queue` ORDER BY `id` DESC");
        if(!$result){error_log("MySQL error in ".__FILE__." -te4opj- error:".sec_dberror()); return false;}
        $rows = array();
        while($currRow = mysqli_fetch_assoc($result)){
            $rows[] = $currRow['data_name'];
        }
        return $rows;
    }
    
    
    function getOrdersTypeRowsForDatanameAndDateRange($server_dataname, $dateTimeStart, $dateTimeEnd){
        $query = "SELECT `id`,`device_time`, `order_id` FROM `queue` WHERE `data_name`='[1]' AND `device_time` >= '[2]' AND `device_time` <= '[3]'";
        $result = sec_query($query, $server_dataname, $dateTimeStart, $dateTimeEnd);
        if(!$result){ error_log("MySQL error in ".__FILE__." -igfryvf- error: ".sec_dberror()); return false;}
        $rows = array();
        while($currRow = mysqli_fetch_assoc($result)){
            $rows[] = $currRow;
        }
        return $rows;
    }
    
    function getQueueRowByID($id){
        $result = sec_query("SELECT * FROM `queue` WHERE `id`='[1]'",$id);
        if(!$result){ error_log("MySQL error in ".__FILE__." -igfryvf- error: ".sec_dberror()); return false;}
        if(mysqli_num_rows($result) == 0){
            return false;
        }
        else{
            $row = mysqli_fetch_assoc($result);
            return $row;
        }
    }
?>