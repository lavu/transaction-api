<html>
<body>

<h1>
Setting up Quickbooks for LavuPOS.
</h1>

The Lavu Quickbooks Integration has been completed to allow for the syncing down of your POS orders into Quickbooks-Invoices.
The following steps must be done for the integration to be complete: 

<br>
<h3>
1.) Setup a User and Password for Lavu within Quickbooks.
</h3>

Go to Company->Setup Users And Passwords.  Enter a unique user name using only alpha-numeric characters and underscores.  The
next window will ask for access privileges for the new user.  We want to give the new user only the access privileges necessary 
for current and foreseeable integrations.
<h4> 
Select "Selected areas of Quickbooks"
</h4>

This will continue a 9 page wizard of access options for the user.

<h4>
1.) For 'Sales Accounts Receivables' - select "Selective Access -> Create transactions and create reports"
</h4> 
<h4>
2.) For 'Purchases and Accounts Payable' - select "Selective Access -> Create transactions and create reports"
</h4>
<h4>
3.) For 'Checks and Credit Cards' - select "No Access"
</h4>
<h4>
4.) For 'Time Tracking' - select "Selective Access -> Create transactions and create reports"
</h4>
<h4>
5.) For 'Payroll' - select "Selective Access -> Create transactions and create reports"
</h4
<h4>
6.) For 'Sensitive Account Settings' - select "No Access"
</h4>
<h4>
7.) For 'Sensitive Reporting' - select "No Access"
</h4>
<h4>
8.) For 'Changing/Deleting transactions' - select "Yes" And 'Before Closing Date' - select whichever you prefer.
</h4>
<h4>
9.) Just continue.
</h4>

<br>
<h3>
2.) Telling Quickbooks to use sales tax and setting up a default sales tax 
</h3>
<h4>
Edit Menu -> preferences -> Sales Tax (menu on left)-> Company Preferences Tab -> "Do You Charge sales Tax" -> Yes
</h4>

<br>
<h3>
3.) Setting the necessary accounts and saving their names to Lavu backend.
</h3>
<h4>
Go to 'Chart of Accounts'.  There are 3 account names that we need to save as settings in the Quickbooks Integration settings page.  Write down the 'Name' of these accounts as they will
be inserted into the Quickbooks Integration settings page.
</h4>
<h4>
-Make sure exists or create an account to handle Accounts Receivable (should already exist as "Accounts Receivable").
</h4>
<h4>
-Make sure exists or create a current asset account for menu items (may already exist as "Food Inventory").
</h4>
<h4>
-Make sure exists or create a COGS (cost of goods sold) account for the food items (may already exist as "Food Purchases"). 
</h4>

<br>
<h3>
4.) Setting up Zero Tax item.  When multiple tax profiles are applied, they are applied on an item/subtotal basis.  In Quickbooks we have to still apply a tax to the overall invoice, the common and only way to handle this is by creating a 'Zero Tax' item.  
</h3>
<h4>
-Go to Items and Services and create new tax item, make sure they tax rate is 0%, and take note of its name.
</h4>


<br>
<h3>
5.) Set up a Discount Item.
</h3>
<h4>
-Go to 'Items and Services' and create a discount item.  Leave its 'Amount or %' equal to 0.  It will be set on the invoice on a line item basis.  You may want to create a Contra-Income account 
in the Chart of Accounts to set it to.
</h4>


<br>
<h3>
6.) Set up a Subtotal item
</h3>
<h4>
-Go to 'Items and Services' and create a Subtotal item.  Subtotals are used on invoices when applying discounts and/or multiple tax rates to individual/groups of items.  I would
just name it Subtotal as that makes sense when displayed on the invoice.
</h4>

<h3>
7.) Create a Tax Item for every tax profile group.  Currently only 1 tax rate per tax profile (EXPLAIN THIS MORE).  Make sure the tax item has the same exact name as the tax profile group.
</h3>


</body>
</html>