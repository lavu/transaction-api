<?php

    require_once(dirname(__FILE__).'/../shared_functions.php');
    
    //TODO, do validations for data_name, server-vars, function, hash and/or whatever else.
    $function = $_REQUEST['function'];
    
    if($function == 'get_queue_orders_by_date_range'){
        get_queue_orders_by_date_range();
    }
    else if($function == 'get_specified_queue'){
        get_specified_queue();
    }
    
    function get_queue_orders_by_date_range(){
        
        //TODO validate arguments
        
        //TODO ALSO ADD TO EACH OF THE ROWS IF NOT YET PULLED, IN PROGRESS, ISSUE, OR FINISHED.
        $startDateTime = $_REQUEST['start_date_time'];
        $endDateTime = $_REQUEST['end_date_time'];
        $dataName = $_REQUEST['server'].'_'.$_REQUEST['data_name'];
        $rows = getOrdersTypeRowsForDatanameAndDateRange($dataName, $startDateTime, $endDateTime);
        $returnObj = array();
        $returnObj['rows'] = $rows;
        $returnStr = LavuJson::json_encode($returnObj);
        echo $returnStr;
    }
    
    
    function get_specified_queue(){
        //TODO Make sure that the specified queue row is in fact owned by the caller.
        $rowID = $_REQUEST['queue_row_id'];
        $queueRow = getQueueRowByID($rowID);
        
        //We need to replace all newlines in the fields, LavuJson doesn't yet escape string inner parts.
        foreach($queueRow as $key => $val){
            $queueRow[$key] = str_replace("\n", "_NEWLINE_", $val);
        }
        
        $returnStr = LavuJson::json_encode($queueRow);
        echo $returnStr;
    }
?>