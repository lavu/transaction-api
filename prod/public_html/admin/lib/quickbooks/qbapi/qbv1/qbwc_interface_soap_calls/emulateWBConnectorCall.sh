if [[ "$1" != *".xml" ]]; then
    echo "Missing first argument, which should be an .xml file that emulates a QBWC call";
    exit;
fi
curl -H "Content-Type: text/xml; charset=utf-8" -H "SOAPAction:" -d @$1 -X POST https://qbook.eventrentalsystems.com/prod/quickbooks/qb_api_service/QBWCService.php