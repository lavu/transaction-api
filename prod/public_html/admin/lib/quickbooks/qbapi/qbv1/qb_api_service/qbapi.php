<?php


    ini_set('display_errors',1);
    $script_start_time = date('Y-m-d H:i:s');
    $script_start_micro = microtime(true);
    
    require_once(dirname(__FILE__).'/../common_code/quickbook_functions.php');
    require_once(dirname(__FILE__).'/qbapi_arg_validations.php');
    require_once(dirname(__FILE__).'/qbapi_qbxml_factory.php');
    
    
    
    //Interface to other networks for placing events on the queue with suffisticated callback.
    $trueIfGoodElseResponseStr = validateRequestSecurityAndFunction(); //Checks superglobals.
    if($trueIfGoodElseResponseStr !== true){
        echo $trueIfGoodElseResponseStr;
        exit;
    }
    
    //What process the calling server is requesting.
    $api_function = 'qbapi_'.$_REQUEST['function'];

    //Passed to the function.
    $funcArgs = getFunctionArgumentsFromRequest();
    
    //The row from the table 'subscribing_server'
    $serverRow = getServerRow($_REQUEST['server_name'], $_REQUEST['security_key']);

    //For Custom/Exact QBXML
    // TODO... HERE WE CAN FORM AN EXACT REQUEST FROM THE ARRAY IF SPECIFIED.
    
    
    //Call the function.  And send back what it returns.
    //echo $api_function($funcArgs); //<-- this is a naive way to do this.  Nothing goes into error log when done this way.
    if($api_function == 'qbapi_add_or_get_account'){
        qbapi_add_or_get_account($funcArgs);
    }
    else if($api_function == 'qbapi_invoiceAdd'){
        qbapi_invoiceAdd($funcArgs);
    }
    else if($api_function == 'qbapi_customerAdd'){
        qbapi_customerAdd($funcArgs);
    }
    else if($api_function == 'qbapi_inventoryItemAdd'){
        qbapi_inventoryItemAdd($funcArgs);
    }
    else if($api_function == 'qbapi_addInventoryItems'){
        qbapi_addInventoryItems($funcArgs);
    }
    else if($api_function == 'qbapi_invoiceAddComplex'){
        qbapi_invoiceAddComplex($funcArgs);
    }
    else if($api_function == 'qbapi_ensureItems_createInvoice'){
        qbapi_ensureItems_createInvoice($funcArgs);
    }
    else if($api_function == 'qbapi_receivePaymentAdd'){
        qbapi_receivePaymentAdd($funcArgs);
    }
    else if($api_function == 'qbapi_ensureItems_createInvoice_addPayment'){ //single payments
        qbapi_ensureItems_createInvoice_addPayment($funcArgs);
    }
    else if($api_function == 'qbapi_ensureItems_createInvoice_addPayments'){ //Multiple payments
        qbapi_ensureItems_createInvoice_addPayments($funcArgs);
    }
    else if($api_function == 'qbapi_testMacro'){
        qbapi_testMacro();
    }
    exit;
    

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  A C C O U N T - R E L A T E D    C A L L S
    //
    function qbapi_add_or_get_account($funcArgs){
        //TODO VALIDATE FUNC ARGS.
        
        //First see if account exists.
        $accountRowsForDataname = getAccountRow_s_WithDataname($_REQUEST['server_name'].'_'.$_REQUEST['data_name']);
        if(!is_array($accountRowsForDataname)){
            $err = "ERROR ".__FILE__." -oisuhfv-";
            echo $err; sec_dberror($err);
            return;
        }
        else if(empty($accountRowsForDataname)){
            //$insertInfo = tryToInsertNewAccountForQBIntegration($user_name, $password_2be_hashed, $dataname, $qbw_file_path);
            $insertInfo = tryToInsertNewAccountForQBIntegration($funcArgs['user_name'], $funcArgs['password'],
                                                        $_REQUEST['server_name'].'_'.$_REQUEST['data_name'], $funcArgs['qbw_file_path']);
            if($insertInfo['status'] != 'success'){
                //TODO
                echo '{"status":"failed","fail_reason":"'.$insertInfo['fail_reason'].'"}';
                return;
            }
            $jsonReturnArray = array('status'=>'success', 'add_or_get' => 'add', 'account_row' => $insertInfo['insert_array']);
            $returnJSON = LavuJson::json_encode($jsonReturnArray);
            echo $returnJSON;
            exit;
        }
        else{
            //TODO IF count $accountRowsForDataname > 1, report error.
            $rowToReturn = $accountRowsForDataname[0];
            $rowToReturn['user_name'] = $funcArgs['user_name'];
            $rowToReturn['user_password_hash'] = $funcArgs['password'];
            updateAccountUsernameAndPassword($_REQUEST['server_name'].'_'.$_REQUEST['data_name'], $funcArgs['user_name'], $funcArgs['password']);
            $jsonReturnArray = array('status'=>'success', 'add_or_get' => 'get', 'account_row' => $rowToReturn);
            $returnJSON = LavuJson::json_encode($jsonReturnArray);
            echo $returnJSON;
            exit;
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Q U E U E - R O W - I N S E R T -- A P I - F U N C T I O N S 
    //
    
    //---------------------------------------------------------
    // -- invoiceAdd
    //
    //    TOP LEVEL
    //    keys: customer_ref_full_name
    //          customer_ref_list_id
    //          date
    //          reference_number
    //          item_sales_tax_ref_full_name
    //          item_sales_tax_ref_list_id
    //          invoice_lines #array
    //              ->item_full_name
    //              ->item_list_id
    //              ->description
    //              ->quantity
    //              ->rate
    function qbapi_invoiceAdd($funcArgs){
        global $serverRow;
        //Checks if $funcArgs is capable (has all the needed members, etc.) for InvoiceAdd
        validate_parameters_exit_on_fail_invoiceAdd($funcArgs);
        $invoiceAddXML = get_InvoiceAdd_xml($funcArgs, false);
        $insertID = pushRequestOntoQueue(array($invoiceAddXML), array('simplyClose'), $serverRow['server_name'].'_'.$_REQUEST['data_name'], $errorOut);
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }
    

    //    -------------CustomerAdd
    //   
    //    TOP LEVEL
    //    keys: customer_ref_full_name
    //          company_name
    //          first_name
    //          last_name
    //          bill_address #array
    //              ->addr1
    //              ->city
    //              ->state
    //              ->postal_code
    //              ->country
    //          phone
    //          email
    function qbapi_customerAdd($funcArgs){
        global $serverRow;
        //Checks if $funcArgs is capable (has all the needed members, etc.) for CustomerAdd
        validate_parameters_exit_on_fail_customer_ref($funcArgs);
        $customerAddXML = get_CustomerAdd_xml($funcArgs, false);
        $insertID = pushRequestOntoQueue(array($customerAddXML), array('simplyClose'), 
                                            $serverRow['server_name'].'_'.$_REQUEST['data_name'], $errorOut);
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }

    //    -------------InventoryItemAdd
    //   
    //    TOP LEVEL
    //    keys: inventory_item_ref_full_name
    //          sales_description
    //          sales_price
    //          income_account_ref_full_name
    //          income_account_ref_list_id
    //          cogs_account_ref_full_name
    //          cogs_account_ref_list_id
    //          asset_account_ref_full_name
    //          asset_account_ref_list_id
    //
    function qbapi_inventoryItemAdd($funcArgs){
        global $serverRow;
        validate_parameters_exit_on_fail_inventory_item_add($funcArgs);
        $inventoryItemAddXML = get_InventoryItemAdd_xml($funcArgs, false);
        $insertID = pushRequestOntoQueue(array($inventoryItemAddXML), array('simplyClose'),
                                            $serverRow['server_name'].'_'.$_REQUEST['data_name'], $errorOut);
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }



    //    -------------addInventoryItems
    //   
    //    TOP LEVEL
    //    keys: items #array
    //              data #dictionary
    //                  ->inventory_item_ref_full_name
    //                  ->sales_description
    //                  ->sales_price
    //                  ->income_account_ref_full_name
    //                  ->income_account_ref_list_id
    //                  ->cogs_account_ref_full_name
    //                  ->cogs_account_ref_list_id
    //                  ->asset_account_ref_full_name
    //                  ->asset_account_ref_list_id
    //
    function qbapi_addInventoryItems($funcArgs){
        /*
        global $serverRow;
        validate_parameters_exit_on_fail_sync_inventory_items($funcArgs);
        $inventoryItemAddXML = get_InventoryItemAdd_xml($funcArgs, true);
        $insertID = pushRequestOntoQueue(array($inventoryItemAddXML), array('simplyClose'),
                                            $serverRow['server_name'].'_'.$_REQUEST['data_name']);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
        */
    }
    
    
    
    //    -------------InvoiceAdd (complex) ----- This is what we want to call for syncing down orders.
    //   
    //    TOP LEVEL
    //    keys: customer_ref_full_name (or)
    //          customer_ref_list_id
    //          inventory_items #array  (to make sure exist first, by full_name)
    //          reference_number (if fits we use order-id, else we base64 encode)
    //          date
    //          item_sales_tax_ref_full_name (or)
    //          item_sales_tax_ref_list_id
    //          invoice_lines #array  
    //              ->item_full_name  (or)
    //              ->item_list_id
    //              ->description
    //              ->quantity
    //              ->rate
    function qbapi_invoiceAddComplex($funcArgs){
        global $serverRow;
        validate_parameters_exit_on_fail_customer_ref($funcArgs);
        validate_parameters_exit_on_fail_invoiceAdd($funcArgs);
        $customerAddXML = get_CustomerAdd_xml($funcArgs, false);
        // TODO SYNC INVOICE ITEMS THAT ARE IN THE LIST. CHECK IF THEY EXIST, MODIFY IF NECESSARY, ETC.
        $invoiceAddXML  = get_InvoiceAdd_xml($funcArgs, false);
        $insertID = pushRequestOntoQueue( array( $customerAddXML, $invoiceAddXML), 
                                          array( 'simplyClose', 'call_server_finished_invoiceAddComplex'),
                                          $serverRow['server_name'].'_'.$_REQUEST['data_name'], $errorOut);
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    //////////////////////////////////////////
    //
    //------------------CustomerAdd
    //
    //    TOP LEVEL
    //    keys: customer_ref_full_name
    //          company_name
    //          first_name
    //          last_name
    //          bill_address #array
    //              ->addr1
    //              ->city
    //              ->state
    //              ->postal_code
    //              ->country
    //          phone
    //          email
    //
    //------------------addInventoryItems
    //   
    //    TOP LEVEL
    //    keys: items #array
    //              data #dictionary
    //                  ->inventory_item_ref_full_name
    //                  ->sales_description
    //                  ->sales_price
    //                  ->income_account_ref_full_name
    //                  ->income_account_ref_list_id
    //                  ->cogs_account_ref_full_name
    //                  ->cogs_account_ref_list_id
    //                  ->asset_account_ref_full_name
    //                  ->asset_account_ref_list_id
    //
    //------------------invoiceAdd
    //
    //    TOP LEVEL
    //    keys: customer_ref_full_name
    //          customer_ref_list_id
    //          date
    //          po_number
    //          item_sales_tax_ref_full_name
    //          item_sales_tax_ref_list_id
    //          invoice_lines #array
    //              ->item_full_name
    //              ->item_list_id
    //              ->description
    //              ->quantity
    //              ->rate
    function qbapi_ensureItems_createInvoice($funcArgs){
        global $serverRow;
        validate_parameters_exit_on_fail_customer_ref($funcArgs);
        validate_parameters_exit_on_fail_invoiceAdd($funcArgs);
        $customerAddXML = get_CustomerAdd_XML($funcArgs, false);
        $inventoryItemsAddArr = get_InventoryItemAdd_xmls($funcArgs, false);
        $invoiceAddXML  = get_InvoiceAdd_xml($funcArgs, false);
        //Now we want to make payments on that invoice from cc_transactions... but for now we just hard code for example.
        //echo "\n\nCustomer add xml:[$customerAddXML]\n\n\ninventoryItemsAddArr:[".print_r($inventoryItemsAddArr,1)."]\n\n\ninvoiceAddXML:[$invoiceAddXML]";
        $qbxmlRequestsArr = array_merge( array($customerAddXML), $inventoryItemsAddArr, array($invoiceAddXML) );
        $insertID = pushRequestOntoQueue( $qbxmlRequestsArr,
                                  array( 'simplyClose', 'simplyClose', 'simplyClose'),
                                  $serverRow['server_name'].'_'.$_REQUEST['data_name'], $errorOut);
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    function qbapi_ensureItems_createInvoice_addPayments($funcArgs){
        global $serverRow;
        
        $dataName = $serverRow['server_name'].'_'.$_REQUEST['data_name'];
        //TODO VALIDATIONS of funcArgs
        // IMPORTANT check for po_number!!! And everything else.
        
        
        
        //We make sure that this order is currently being processed, if it is we send back an wait status.
        checkIfOrderInvoiceForCurrentOrderIDIsInProgressSendWaitMessageAndExitIfIs($dataName, $funcArgs['po_number']);
        //Row(s should really be only 1, but in case) that have not been pulled by QBWC and are same order_id.  
        //This insert will render it invalidated.
        $rowsToInvalidate = getUnprocessedQueueRowsWithSameDatanameAndOrderID($dataName, $funcArgs['po_number']);
        $idsOfRowsToInvalidate = getIDsFromRows($rowsToInvalidate);
        invalidateUnprocessedQueueRowsWithSameDatanameAndOrderID($dataName, $funcArgs['po_number']);  
        set_overridden_by_toZeroWherePointedToRecentlyInvalidatedRow($dataName, $funcArgs['po_number'], $idsOfRowsToInvalidate);
    
        
        //// //We mark any order invoice rows that have not yet been processed as invalid since they both are no longer relevant
        //// //and also would cause an error (they would move/create payments that this row would not know about etc.).
        //// invalidateNotYetProcessedInvoiceQueueRowsWithSameOrderIDBacktrackOverriddenByOnPreviousRows($serverRow['server_name'].'_'.$_REQUEST['data_name'], $funcArgs['po_number']);
        

        //If we sync the same order id, we send orders to delete the old one, then we proceed sending the new one.
        $queueRowsWithSameOrderIDHasNotBeenOverriddenDesc = 
            getOtherQueueRowsWithSameOrderIDOrderedByIDDesc($serverRow['server_name'].'_'.$_REQUEST['data_name'], 
                                                            $funcArgs['po_number']);
        
        //We create a delete xml for every queue row with the same order_id and no `overridden_by` set.
        $deleteInvoiceXMLs = array();
        foreach($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc as $currRowToMarkOverridden){
            $txnID2Delete = $currRowToMarkOverridden['txn_id'];
            $deleteInvoiceXML = get_deleteInvoiceXML($txnID2Delete, false);
            $deleteInvoiceXMLs[] = $deleteInvoiceXML;
        }
        
        //All server side payment ids that are being passed.
        $allAPIPassedServerSidePaymentIDs = getAllServerSidePaymentIDsPassedInFuncArgs($funcArgs);
    
        //Finding payments that will need to be modified.
        $allPreviouslySavedServerSidePaymentIDs = 
            getAllServerSidePaymentIDsFromRowsOverridden($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc);
        
        //2 overlapping sets of: trans_ids sent from server (set A), and 'trans_ids' saved (set B)
        // A/B -> ReceivePaymentAdd(s)
        // A^B -> ReceivePaymentMod(s)
        // B/(A^B) -> Payments saved, but not included by api call... these are in error.
        
        
        //IDs of payments that will be added. A/B
        $serverSidePaymentIDs_4_ReceivePaymentAdd = array_diff($allAPIPassedServerSidePaymentIDs, $allPreviouslySavedServerSidePaymentIDs);
        $receivePaymentsAddFilterArr = arrayOfFilterValuesToKeyedArrayEqualToSameValues_falseIfNotArray($serverSidePaymentIDs_4_ReceivePaymentAdd);
        //IDs of payments that will be modified. A^B
        $serverSidePaymentIDs_4_ReceivePaymentMod = array_diff($allAPIPassedServerSidePaymentIDs, $serverSidePaymentIDs_4_ReceivePaymentAdd);
        $receivePaymentsModFilterArr = arrayOfFilterValuesToKeyedArrayEqualToSameValues_falseIfNotArray($serverSidePaymentIDs_4_ReceivePaymentMod);
        //IDs of payments that were saved but not in current api call. B/(A^B)
        $serverSidePaymentIDs_savedButNotInAPICall_error = array_diff($allPreviouslySavedServerSidePaymentIDs, $allAPIPassedServerSidePaymentIDs);


        
        //--------------THE QBXMLS
        $sendRequestXMLsArr = array();
        // 1.) Delete any with same order_id that has not been overridden.
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $deleteInvoiceXMLs); 
        $sendRequestXMLsArr[] = get_CustomerAdd_XML($funcArgs, false);
        // --- 2.) The customer, add.  If already exists, then no harm done.
        $inventoryAddXMLs = get_InventoryItemAdd_xmls($funcArgs, false);
        // --- 3.) The inventory items add.  If already exist, then no harm done.
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $inventoryAddXMLs);
        $invoiceAddActionIndex = count($sendRequestXMLsArr); 
        // --- 4.) The Invoice Add.  This one should not fail.
        $sendRequestXMLsArr[] = get_InvoiceAdd_xml($funcArgs, false);
        // --- 5.) ReceivePaymentMod(s).
        $serverPaymentIDsMods_OUT = array();
        $serverPaymentID_2_map_variables = get_server_paymentID_qb_vars($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc);
        $receivePaymentsModXMLsArr = get_mult_ReceivePaymentMod_xmls($funcArgs, $serverPaymentID_2_map_variables, 
                                                $receivePaymentsModFilterArr, $invoiceAddActionIndex, false, $serverPaymentIDsMods_OUT);
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $receivePaymentsModXMLsArr);
        // --- 6.) ReceivePaymentAdd(s) on invoice.
        $serverPaymentIDsAdded_OUT = array(); //Out variable will contain (in the case of lavu) the cc_transaction ids. (the id of the payments server side).
        $receivePaymentsAddXMLsArr = get_mult_ReceivePaymentAdd_xmls($funcArgs, $receivePaymentsAddFilterArr, 
                                                $invoiceAddActionIndex, false, $serverPaymentIDsAdded_OUT);
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $receivePaymentsAddXMLsArr);
        
        
        
        //--------------THE RESPONSE METHODS
        $responseHandlerFunctions = array();
        // 1.) For the delete qbxml.
        foreach($deleteInvoiceXMLs as $v){ $responseHandlerFunctions[] = 'doNothing'; }
        // 2.) The customer add.
        $responseHandlerFunctions[] = 'doNothing';
        // 3.) Inventory items add.
        foreach($inventoryAddXMLs as $v){ $responseHandlerFunctions[] = 'doNothing'; }
        // 4.) InvoiceAdd
        $responseHandlerFunctions[] = (count($receivePaymentsModXMLsArr) == 0 && count($receivePaymentsAddXMLsArr) == 0) ? 'handleInvoiceAddAndClose' : 'handleInvoiceAdd';
        // 5.) ReceivePaymentMods
        for($i = 0; $i < count($receivePaymentsModXMLsArr) - 1; $i++){ $responseHandlerFunctions[] = 'handleReceivePaymentMod';}
        if(count($receivePaymentsModXMLsArr) > 0){
            $responseHandlerFunctions[] = count($receivePaymentsAddXMLsArr) == 0 ? "handleReceivePaymentModAndClose" : "handleReceivePaymentMod";
        }
        // 6.) ReceivePaymentAdd
        for($i = 0; $i < (count($receivePaymentsAddXMLsArr) - 1); $i++){ $responseHandlerFunctions[] = 'handleReceivePaymentAdd'; }
        if(count($receivePaymentsAddXMLsArr) > 0){ 
            $responseHandlerFunctions[] = 'handleReceivePaymentAddAndClose'; 
        }
        
        //Create Queue Row.
        $serverSidePaymentIDsJsonMap = createServerQuickbooksIDJoinedMap(array_merge($serverPaymentIDsMods_OUT, $serverPaymentIDsAdded_OUT), $invoiceAddActionIndex+1);
        $insertID = pushRequestOntoQueue($sendRequestXMLsArr,
                                         $responseHandlerFunctions,
                                         $serverRow['server_name'].'_'.$_REQUEST['data_name'], 
                                         $errorOut, 
                                         $funcArgs['po_number'],
                                         $serverSidePaymentIDsJsonMap);
                                         
        //TODO MAKE SURE THIS PART WORKS.
        //All previous queue rows with same dataname and orderID have been deleted through the qbxml, now we mark they have been overridden by this invoice.
        markAllOverriddenRowsWithOverridersID($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc, $insertID);
        
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }
    */
    
    
    
    
    function qbapi_ensureItems_createInvoice_addPayments($funcArgs){
        //We changed this to process in a lazy way. QBXML is built on initialization when pulled by Web Connector.
        global $serverRow;
        $dataName = $serverRow['server_name'].'_'.$_REQUEST['data_name'];
        $initialAPICallArr = array();
        $initialAPICallArr['data_name'] = $dataName;
        $initialAPICallArr['_POST'] = $_POST;
        $initialAPICallArr['subscr_server_id'] = $serverRow['id'];
        $initialAPICallArr['funcArgs'] = $funcArgs;
        $apiFunction = 'qbapi_ensureItems_createInvoice_addPayments';
        $error = null;
        $insertID = pushLazyOrderSaveRequestOntoQueue($dataName, $initialAPICallArr, $apiFunction, $error, $funcArgs['po_number'], $funcArgs['device_time']);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }

    
    
    
    
    function qbapi_add_default_items_and_accounts($funcArgs){
        //Function args:
        /*
            quickbooks_default_accounts_receivable_full_name
            quickbooks_default_asset_account_ref_full_name
            quickbooks_default_cogs_account_ref_full_name
            quickbooks_default_customer_ref_full_name
            quickbooks_default_discount_ref_full_name
            quickbooks_default_income_account_ref_full_name
            quickbooks_default_item_sales_tax_ref_full_name
            quickbooks_default_quick_serve_togo_status
            quickbooks_default_subtotal_ref_full_name
            quickbooks_default_tax_subtotal_ref_full_name
            quickbooks_default_zero_tax_ref_full_name
        */
        //TODO FINISH THIS... They need to set it up manually first.
    }
    
    
    
    
    
    
    
    
    
    //This is mostly just for proof of concept, this will actually be put inside the invoice add stuff.
    function qbapi_receivePaymentAdd($funcArgs){
        global $serverRow;
        //TODO PERFORM VALIDATIONS OF: funcArgs
        $receivePaymentsAddXML = get_ReceivePaymentAdd_xml($funcArgs, $sanitizedXML = false);
        $insertID = pushRequestOntoQueue( array($receivePaymentsAddXML), array('simplyClose'), $serverRow['server_name'].'_'.$_REQUEST['data_name'], $errorOut);
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }
    
    function qbapi_testMacro(){
        global $serverRow;
        $testXML = get_testXMLForMacros();
        $insertID = pushRequestOntoQueue( array($testXML), array('simplyClose'), $serverRow['server_name'].'_'.$_REQUEST['data_name'], $errorOut);
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }
    
    
?>
