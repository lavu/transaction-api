<?php


    // We will need to change all error_logs using regex in sublime
    /*
        REGEX:  ^(.*)(error_log\((.*)\))(.*)$
        REPLA:  $1bd_appendLog($3, 'errors.log')$4
    */
    
    require_once(dirname(__FILE__).'/../common_code/quickbook_functions.php');
    
    require_once(dirname(__FILE__).'/responseHandlers.php');
    
    
    require_once(dirname(__FILE__).'/../common_code/QBXMLClasses.php');
    
    //C:\Qkbks back up\brian_test_company.qbw
    
    //For tracking specific ip address.
    $remoteIP = $_SERVER['REMOTE_ADDR'];
    $logIfIP  = '184.155.187.82';
    $logging  = $remoteIP == '184.155.187.82' || $remoteIP == '74.95.18.90';
    $verboseLogging = $logging && false;
    //bd_appendToLog("current ip: " . $remoteIP, 'learning_soap.log');
    
    
        
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Q U I C K B O O K S - W E B - C O N N E C T O R   I N T E R F A C E    F U N C T I O N S
    //
    
	//p.62 (In the QBWC programming guide)  BTW...http://wiki.consolibyte.com/wiki/doku.php/quickbooks_web_connector is always better resource.
	// -- is called second.
    function authenticateImpl($strUserName, $strPassword){
        global $logging; 
        logSoapFunctionHeaderIfLogging('authenticateImpl', false, $strUserName, $strPassword);

        
        //Corey wants no restrictions on user names, thus user name is not unique.  This is the same user name as setup as the quickbooks user.
        //Under this condition we may have multiple 'Admin's or 'John's as username and we differentiate them by their password.  
        //-- We will then have to check for username/pw uniqueness at time of integration setup.
        
        $accountsRowOrFalse = getAccountWithMatchedUsernamePassword($strUserName, $strPassword);
        if(empty($accountsRowOrFalse)){
            return "NVU"; 
        }
        if(empty($accountsRowOrFalse['data_name']) || empty($accountsRowOrFalse['id'])){
            error_log("ers_qbook.accounts does not have data_name/id set on id:".$accountsRowOrFalse['id']);
            return "NVU";
        }
        $sessionTicket = createAndInsertTicketSequenceIdentifier($accountsRowOrFalse['id'], $accountsRowOrFalse['data_name']);
        $accountsRowOrFalse = getAccountWithMatchedUsernamePassword($strUserName, $strPassword);//So we reload the session's Ticket
        
        if(!$sessionTicket){
            bd_appendToLog("error setting ticket", 'learning_soap.log');
            error_log("Error in ".__FILE__." could not set the session ticket. Possible sec_dberror():".sec_dberror());
            return "NVU"; 
        }
        if($logging){
            bd_appendToLog("-authentication is returning...\n--dataname:".$accountsRowOrFalse['data_name']."\n--ticket:".
                                $accountsRowOrFalse['ticket_session_identifier'].
                                "\n--company file:[".$accountsRowOrFalse['qbw_file_path'].']','learning_soap.log');
        }
        return array( $sessionTicket, $accountsRowOrFalse['qbw_file_path'] );
    }
    
    //p.66 -- is called first.
    function clientVersionImpl($version){
        global $logging; 
        logSoapFunctionHeaderIfLogging('clientVersionImpl', $ticketOrFalse=false, $username=false, $password=false, $version);
    }
    
    //p.68
    function closeConnectionImpl($ticket){
        global $logging; 
        logSoapFunctionHeaderIfLogging('closeConnectionImpl',$ticket);
        clearTicketIdentifierOnAccount($ticket);
    }
    
    //p.69
    function connectionErrorImpl($ticket, $hresult, $message){
        global $logging;
        logSoapFunctionHeaderIfLogging('connectionErrorImpl',$ticket);
    }
    
    //p.72
    function getLastErrorImpl($ticket){
        global $logging; 
        logSoapFunctionHeaderIfLogging('receiveResponseXMLImpl',$ticket);
    }
    
    
    //p.77
    //Returns -1 if error, else a number 0-100 of the percentage done on the server's part.
    // This is to receive the response from the quickbooks program.
    function receiveResponseXMLImpl($ticket, $response, $hresult, $message){
        global $logging, $verboseLogging, $queueRow, $actionIndexAtStart;
bd_appendToLog('marker0','learning_soap.log');
        logSoapFunctionHeaderIfLogging('receiveResponseXMLImpl',$ticket);
bd_appendToLog('marker1','learning_soap.log');
        //Pull the 2 db rows-- account and queue.
        $accountRow = getAccountRowForTicket($ticket);
        $queueRow   = getQueueRowForTicket($ticket);
        $actionIndexAtStart = $queueRow['action_index'];
bd_appendToLog('marker5','learning_soap.log');    
        //Append all the incomming data to the array that will update the queue row in the database
        $newResponseText = append_qbxmlresponse_and_meta_to_string($ticket, $response, $hresult, $message,
                                                                    $queueRow['receiveResponseXMLs']);
        $queueRow['receiveResponseXMLs'] = $newResponseText;
bd_appendToLog('marker10','learning_soap.log');

        //Regular interface procedure logging.
        performLoggingForReceiveResponseXMLImpl($ticket, $response, $hresult, $message, $accountRow);
        
        //FIND AND PERFORM HANDLER FUNCTION
        $handler_function = getCorrespondingResponseHandlerFromRow();
        $doesHandlerExist = checkIfHandlingFunctionExistsPutExceptionOnQueueIfDoesnt($handler_function);
        if($doesHandlerExist){
            //Takes copy of accountRow and reference of queueRow.
            $handler_function($accountRow, $queueRow, $ticket, $response, $hresult, $message);
        }else{
            throwExceptionOntoQueue('Handler function does not exist: ' . $handler_function);
        }
error_log("queue row txn_id".$queueRow['txn_id']);
        //Lastly, we increment the action_index and return the % finished.
        $queueRow['action_index'] = $queueRow['action_index'] + 1;
        
        //We update the database row so the response is stored.
        updateRowOnQueue();
        
        //Calculate the percentage finished we are with this dataname and return it.
        $percentFinishedWithDataname = calculatePercentFinishedWithDataname($accountRow);
    	return $percentFinishedWithDataname;
    }

    //p.80
    //Sends xml formatted command to the quickbooks program. 
    function sendRequestXMLImpl($ticket, $strHCPResponse, $strCompanyFileName, $qbXMLCountry, $qbXMLMajorVers, $qbXMLMinorVers){
        global $logging, $verboseLogging, $queueRow, $actionIndexAtStart;
        logSoapFunctionHeaderIfLogging('sendRequestXMLImpl', $ticket);

        //Pull the account row and queue row.
        $accountRow = getAccountRowForTicket($ticket);

        //We see if we are continuing the processing on a queue row, else we pull the next one if waiting, else we exit.
        $queueRow = getQueueRowWithTicket($ticket);
        if(!$queueRow){
            $queueRow = pullFromQueue($accountRow['data_name']);//Returns next row in `queue` table for dataname if there is one.
            if($queueRow){
                error_log('New queue row, immediately calling unfolding');
                unfoldQueueRowAndSave2DBIfNeeded($queueRow);
                error_log('unfolding done.');
            }else{
                error_log('No new queue row');   
            }
        }
        
        
        
        $actionIndexAtStart = $queueRow['action_index'];

        //logging
        performLoggingForSendRequestXMLImpl($ticket, $strHCPResponse, $strCompanyFileName, $qbXMLCountry, $qbXMLMajorVers, $qbXMLMinorVers, $accountRow);

        //If nothing to process, we chill for now.
        error_log('marker1');
        if(empty($queueRow)){ bd_appendToLog("There are no rows on queue for ".$accountRow['data_name'],'learning_soap.log'); return ""; }  
        error_log('marker2');
        
        //Sets the `server_date_time_pulled` and `ticket` on the queue row
        initializeRowIfActionIndexEqZero($accountRow);

        //Grabs the current sendRequestsXML (xml part only) and performs the back substitution on the back-reference-expressions.
        //Stores the back-reference-expressions it found and their evaluation as a print_r of an array stored in `back_reference_substitution_history`
        $sendXMLDataStrNonEscaped = getBackReferenceSubstitutionOfCurrentQBXMLRequestByActionIndex();

        //Changes '<' to &lt;, '>' to &gt, etc.
        $sendXMLDataStrEscaped = SimpleXMLElementEnhanced::xml_sanitize($sendXMLDataStrNonEscaped);
        
        //log output.
        if($verboseLogging){ bd_appendToLog("Sending QBXML data:[".$sendXMLDataStr."]", 'learning_soap.log'); }

        return $sendXMLDataStrEscaped;
    }
    // END INTERFACE METHODS
    //-------------------------------------------------------------------------------------------------------------------------------------------
    
    
    
    
    
    
    
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  F U N C T I O N S     F O R    L I V E    L O G G I N G . . .
    //
    function logSoapFunctionHeaderIfLogging($functionTitle, $ticketOrFalse=false, $username=false, $password=false, $clientVersion=false){
        global $logging;
        $versionString = "";
        if($clientVersion){
             bd_appendToLog("\n\n".date('Y-m-d H:i:s'), 'learning_soap.log');
             $versionString = "Version:".$clientVersion;   
        }
        $usernamePasswordStr = !empty($username) ? " Username:$username Password:$password" : "";
        if($logging){
            bd_appendToLog("----------------------------------------------------------------------------------------------------------", 'learning_soap.log');
            if($ticketOrFalse)
                bd_appendToLog("$functionTitle called... $versionString $usernamePasswordStr Ticket:$ticketOrFalse", 'learning_soap.log');
            else
                bd_appendToLog("$functionTitle called... $versionString $usernamePasswordStr", 'learning_soap.log');
        }
    }
    
    function performLoggingForReceiveResponseXMLImpl($ticket, $response, $hresult, $message, $accountRow){
        global $logging, $verboseLogging, $queueRow;
        if($logging){
            $actionIndex = $queueRow['action_index'];
            bd_appendToLog("ActionIndex:$actionIndex  Ticket:$ticket", 'learning_soap.log');
            if(!empty($accountRow)){
                bd_appendToLog("-Pulled account row...", 'learning_soap.log');
                bd_appendToLog("--dataname:".$accountRow['data_name']." user_name:".$accountRow['user_name'], 'learning_soap.log');
            }else{
                bd_appendToLog("Row doesn't exist for that ticket, or mysql error occured.  sec_dberror():" .sec_dberror(), 'learning_soap.log');
            }
            if(!empty($queueRow)){
                bd_appendToLog("-Pulled queue row has id:[".$queueRow['id']."]", 'learning_soap.log');
            }
            else{
                bd_appendToLog("queueRowFromTicket is empty", 'learning_soap.log');
            }
        }
    }
    
    function performLoggingForSendRequestXMLImpl($ticket, $strHCPResponse, $strCompanyFileName, $qbXMLCountry, $qbXMLMajorVers, $qbXMLMinorVers, $accountRow){
        global $logging, $verboseLogging, $queueRow;
        if($logging){
            $actionIndex = $queueRow['action_index'];
            bd_appendToLog("ActionIndex:$actionIndex  Ticket:$ticket", 'learning_soap.log');
            $queueRowId = $queueRow ? $queueRow['id'] : "There's no queue rows for ".$accountRow['data_name'];
            bd_appendToLog("\naction_index:$actionIndex ticket:$ticket", 'learning_soap.log');
            if(!empty($accountRow)){
                bd_appendToLog("-Pulled account row...", 'learning_soap.log');
                bd_appendToLog("--dataname:".$accountRow['data_name']." user_name:".$accountRow['user_name'], 'learning_soap.log');
                bd_appendToLog("-Row Pulled From Queue id:[".$queueRowId.']', 'learning_soap.log');
            }else{
                bd_appendToLog("Row doesn't exist for that ticket, or mysql error occured.  sec_dberror():" .sec_dberror(), 'learning_soap.log');
            }
        }        
    }
    


?>
