<?php

    require_once(dirname(__FILE__).'/QBXMLClasses.php');
    require_once(dirname(__FILE__).'/json.php');
    require_once(dirname(__FILE__).'/logging/bd_logging.php');

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  S Y S T E M    F U N C T I O N S
    //
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery_quickbooks.php');


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  F U N C T I O N S     F O R    A C C O U N T    M A N A G E M E N T    A N D    G E N E R A L    C O N N E C T I O N    M A N A G E M E N T.
    //
    function tryToInsertNewAccountForQBIntegration($user_name, $password_2be_hashed, $dataname, $qbw_file_path){
        $doesRowAlreadyExist = getAccountWithMatchedUsernamePassword($user_name, $password_2be_hashed);
        $doesRowAlreadyExist = $doesRowAlreadyExist ? true : false;
        if($doesRowAlreadyExist){ return array('status' => 'failed', 'fail_reason' => 'username_password_pair_already_exists'); }
        $password_hash = sha1($password_2be_hashed.'JosephStalin');
        $fileID_GUID = generateUniqueGUID($user_name.$dataname);
        $ticket_session_identifier = generateUniqueQBWCTicket();
        $insertArr = array('user_name' => $user_name,
                           'user_password_hash' => $password_hash,
                           'data_name' => $dataname,
                           'ticket_session_identifier' => $ticket_session_identifier,
                           'qbw_file_path' => $qbw_file_path,
                           'file_id' => $fileID_GUID);
        $insertQuery  = "INSERT INTO `poslavu_QUICKBOOKS_db`.`accounts` (`user_name`,`user_password_hash`, `data_name`,`ticket_session_identifier`,`qbw_file_path`,`file_id`) ";
        $insertQuery .= "VALUES ('[user_name]','[user_password_hash]','[data_name]','[ticket_session_identifier]','[qbw_file_path]','[file_id]')";
        $result = sec_query($insertQuery, $insertArr);
        if(!$result){ error_log("MySQL error in ".__FILE__.' error:' . lavu_dberror()); return array('status'=>'failed','fail_reason'=>'db_error'); }
        if(!ConnectionHub::getConn("rest")->InsertID()){
            echo 'failed insert'; error_log("MySQL error in ".__FILE__.' error:' . lavu_dberror());
            return array('status'=>'failed','fail_reason'=>'db_error');
        }
        $insertArr['id'] = ConnectionHub::getConn("rest")->InsertID();
        return array('status'=>'success', 'insert_array'=>$insertArr);
    }

    function updateAccountUsernameAndPassword($data_name, $user_name, $password_toBeHashed){
        $hashedPassword = sha1($password_toBeHashed.'JosephStalin');
        $result = sec_query("UPDATE `poslavu_QUICKBOOKS_db`.`accounts` SET `user_name`='[1]', `user_password_hash`='[2]' WHERE `data_name`='[3]'", $user_name, $hashedPassword, $data_name);
        if(!$result){ error_log("MySQL error in ".__FILE__." -378wntc- error:".lavu_dberror()); }
        return $result ? true : false;
    }

    //There should only be one row, but in case there's an error or many of them.
    function getAccountRow_s_WithDataname($serverDatanamePairStr){
        $result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`accounts` WHERE `data_name`='[1]'", $serverDatanamePairStr);
        if(!$result){
            error_log("MySQL error in ".__FILE__." -ntv78- error:".lavu_dberror());
            return false;
        }
        $rows = array();
        while($currRow = mysqli_fetch_assoc($result)){
            $rows[] = $currRow;
        }
        return $rows;
    }

    function getAccountWithMatchedUsernamePassword($user_name, $password){
        $result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`accounts` where `user_name`='[1]' AND `user_password_hash`='[2]'", $user_name, sha1($password.'JosephStalin'));
        if(!$result){
            error_log("Error in quickbooks/BrianQBInterface/qb_utility_functions.php mysql error:".lavu_dberror());
            return false;
        }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        return false;
    }

    function getAccountRowForTicket($ticket){
        $result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`accounts` where `ticket_session_identifier`='[1]'", $ticket);
        if(!$result){
            error_log("MySQL Error in ".__FILE__." error: " . lavu_dberror());
            return false;
        }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        return false;
    }

    function createAndInsertTicketSequenceIdentifier($accountsRowID, $dataname){
        $newTicketSequenceIdentifier = generateUniqueQBWCTicket();
        if(empty($newTicketSequenceIdentifier)){
            return false;
        }
        $result = sec_query("UPDATE `poslavu_QUICKBOOKS_db`.`accounts` SET `ticket_session_identifier`='[1]' WHERE `id`='[2]'", $newTicketSequenceIdentifier,$accountsRowID);
        if(!$result){ error_log('MySQL error in'.__FILE__.' lavu_dberror:'.lavu_dberror()); }
        return $newTicketSequenceIdentifier;
    }

    function clearTicketIdentifierOnAccount($ticket){
        $account = getAccountRowForTicket($ticket);
        $result = sec_query("UPDATE `poslavu_QUICKBOOKS_db`.`accounts` SET `ticket_session_identifier`='' WHERE `id`='[1]'", $account['id']);
        if(!$result){ error_log('MySQL error in '.__FILE__.' mysql error:'.lavu_dberror()); return false; }
        return ConnectionHub::getConn("rest")->affectedRows();
    }

    function generateUniqueQBWCTicket(){
        $hashPart1 = date('Y-m-d H:m:s').mt_rand();
        $potentialTicketID = sha1($hashPart1);
        $accounts_result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`accounts` WHERE `ticket_session_identifier`='[1]'",$potentialTicketID);
        $queue_result    = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `ticket_session_identifier`='[1]'",$potentialTicketID);
        if(!$accounts_result){error_log('Error -af- in generateUniqueQBWCTicket() in '.__FILE__.' mysql error:' . lavu_dberror()); return false;}
        if(!$queue_result){   error_log('Error -fe- in generateUniqueQBWCTicket() in '.__FILE__.' mysql error:' . lavu_dberror()); return false;}
        while(mysqli_num_rows($accounts_result) || mysqli_num_rows($queue_result)){
            $hashPart1 .= 'j';
            $potentialTicketID = sha1($hashPart1);
            $accounts_result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`accounts` WHERE `ticket_session_identifier`='[1]'",$potentialTicketID);
            $queue_result    = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `ticket_session_identifier`='[1]'",   $potentialTicketID);
            if(!$accounts_result){error_log('Error -sdf- in generateUniqueQBWCTicket() in '.__FILE__.' mysql error:' . lavu_dberror()); return false;}
            if(!$queue_result){   error_log('Error -few- in generateUniqueQBWCTicket() in '.__FILE__.' mysql error:' . lavu_dberror()); return false;}
        }
        return $potentialTicketID;
    }


    function generateUniqueGUID($extraSalt = 'blah'){
        $hashPart = date('Y-m-d H:m:s').mt_rand();
        $sha1 = strtoupper( sha1($hashPart.$extraSalt) );
        $finalGUIDStr = '{'.substr($sha1, 0, 8).'-'.substr($sha1,8,4).'-'.substr($sha1,12,4).'-'.substr($sha1,16,4).'-'.substr($sha1,20,12).'}';
        return $finalGUIDStr;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  G E N E R A L  (NOT SPECIFIC TO ANY FUNCTION/ALG.)  F U N C T I O N S    F O R    Q U E U E    P R O C E S S I N G . . .
    //

    function pushRequestOntoQueue($sanitizedXMLsArr, $partialTransactionResponseHandlers, $data_name, &$error, $orderID='',$cc_transIDs4Payments=''){
        $sanitizedXMLsDelimStr = getTransactionDelimiter() . implode(getTransactionDelimiter(), $sanitizedXMLsArr);
        $handlingFunctionsStr  = getTransactionDelimiter() . implode("\n".getTransactionDelimiter(), $partialTransactionResponseHandlers);

        $qStr  = "INSERT INTO `poslavu_QUICKBOOKS_db`.`queue` ".
                 "(`ticket_session_identifier`,`data_name`,`sendRequestXMLs`,`server_date_time_created`,`receivedResponseHandlers`,`order_id`,`server2QBsPaymentIDMap`,`total_actions`) ";
        $qStr .= "VALUES (null, '[1]','[2]','[3]','[4]','[5]','[6]','".count($sanitizedXMLsArr)."')";

        $result = sec_query($qStr, $data_name, $sanitizedXMLsDelimStr, date('Y-m-d H:i:s'), $handlingFunctionsStr, $orderID, $cc_transIDs4Payments);
        if(!$result){ $error = "MySQL error in ".__FILE__." -h5wiuh- lavu_dberror():".lavu_dberror(); error_log($error); return false; }
        return ConnectionHub::getConn("rest")->InsertID();
    }

    //Builds the XML array when the WebConnector calls, at last moment, such that there are no unprocessed rows with same order id.
    function pushLazyOrderSaveRequestOntoQueue($data_name, $initialAPICallArr, $apiFunction, &$error, $orderID, $device_time){
        $initialAPICallStr = LavuJson::json_encode($initialAPICallArr,1);
        $qStr  = "INSERT INTO `poslavu_QUICKBOOKS_db`.`queue` ".
                 "(`ticket_session_identifier`,`data_name`,`server_date_time_created`,`initial_api_call`, `order_id`, `device_time`) ".
                 "VALUES (null, '[1]', '[2]', '[3]', '[4]', '[5]')";
        $result = sec_query($qStr, $data_name, date('Y-m-d H:i:s'), $initialAPICallStr, $orderID, $device_time);
        if(!$result){ $error = "MySQL error in ".__FILE__." -e8567h- lavu_dberror():".lavu_dberror(); error_log($error); return false; }
        return ConnectionHub::getConn("rest")->InsertID();
    }

    function getTransactionDelimiter(){
        return "::::::::PARTIAL_START::::::::\n";
    }

    function getMetadataDelimiter(){
        return "::::::::PARTIAL_METADATA:::::\n";
    }

    function getNumberOfQueueRowsThatNeedProcessing($data_name){
        $result = sec_query("SELECT COUNT(*) as `count` FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `data_name`='[1]' AND `invalidated`='0'", $data_name);
        if(!$result || !mysqli_num_rows($result)){error_log('MySQL error -fea- in '.__FILE__.' lavu_dberror():'.lavu_dberror()); return false;}
        $countArr = mysqli_fetch_assoc($result);
        return $countArr['count'];
    }

    function pullFromQueue($data_name){//Called when 'sendRequestXMLImpl' is reached meaning to pull the job to send to QBs.
        $query = "SELECT * FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `data_name`='[1]' AND `server_date_time_pulled`='' AND `invalidated`='0' order by `id` ASC LIMIT 1";
        $result = sec_query($query, $data_name);
        if(!$result){
            error_log('MySQL error -f36- in '.__FILE__.' lavu_dberror():'.lavu_dberror());
            bd_appendToLog("MYSQL ERROR:".lavu_dberror(), 'learning_soap.log');
            return false;}
        if(!mysqli_num_rows($result)){ bd_appendToLog("No Row To Pull For $data_name", 'learning_soap.log'); return false; }//No Jobs To Perform.
        return mysqli_fetch_assoc($result);
    }

    function getQueueRowWithTicket($ticket){
        $result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `ticket_session_identifier`='[1]'", $ticket);
        if(!$result){ error_log('MySQL error -fk53- in '.__FILE__.' lavu_dberror():'.lavu_dberror()); return false; }
        if(!mysqli_num_rows($result)){ return false; }//Bad Ticket
        return mysqli_fetch_assoc($result);
    }

    function updateRowOnQueue($specified_columns = false){
        global $queueRow;
        $id = $queueRow['id'];
        $prevRowArr = getQueueRowForID($id);
        if(empty($id)){ error_log("Error: Row array does not have id in ".__FILE__); return false; }
        $queryStr = "UPDATE `poslavu_QUICKBOOKS_db`.`queue` SET ";
        if(empty($specified_columns)){
            foreach($queueRow as $key => $val){
                if($key == 'id') continue;
                $queryStr .= "`$key`='[$key]',";
            }
        }else{
            $columnsUpdatedInt = 0;
            foreach($queueRow as $key => $val){
                if($key == 'id') continue;
                if(in_array($key, $specified_columns)){
                    $queryStr .= "`$key`='[$key]',";
                    $columnsUpdatedInt++;
                }
            }
            if($columnsUpdatedInt != count($specified_columns)){
                error_log('updateRowOnQueue has been called with arg:$specified_columns containing: '.
                    print_r($specified_columns,1).
                    ' and all columns from $specified_columns do not exist in the queue column list: '.
                     print_r(array_keys($queueRow),1) );
            }
        }
        $queryStr = substr($queryStr, 0, -1).' ';
        $queryStr .= "WHERE `id` = '[id]' ";
        $result = sec_query($queryStr, $queueRow);
        if(!$result){
            bd_appendToLog('Mysql error in '.__FILE__.' -3gr- lavu_dberror:'.lavu_dberror(), 'learning_soap.log');
            bd_appendToLog('Query string built for error -3gr-:'.$queryStr, 'learning_soap.log');
            error_log('Mysql error in '.__FILE__.' -3gr- lavu_dberror:'.lavu_dberror());
            return false;
        }
        return true;
    }

    function setServerTimeOnQueueField($field){
        global $queueRow;

        $row_id = $queueRow['id'];
        $dateTime = date('Y-m-d H:i:s');
        $result = sec_query("UPDATE `poslavu_QUICKBOOKS_db`.`queue` SET `[1]`='[2]' WHERE `id`='[3]'", $field, $dateTime, $row_id);
        if(!$result){
            $errorMsg = 'MySQL error -kc02- in '.__FILE__.' lavu_dberror():'.lavu_dberror();
            error_log($errorMsg);
            echo 'ERROR:' . $errorMsg;
            return false;
        }

        return ConnectionHub::getConn("rest")->affectedRows();
    }

    function getQueueRowForTicket($ticket){
        $result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`queue` where `ticket_session_identifier`='[1]'", $ticket);
        if(!$result){
            error_log("MySQL Error in ".__FILE__." -asf3- error: " . lavu_dberror());
            return false;
        }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        return false;
    }

    function throwExceptionOntoQueue($exception){
        global $isLogging, $queueRow;
        $appendText = "EXCEPTION AT ACTION_INDEX:".$queueRow['action_index']."\n".$exception . "\n\n";
        $queueRow['exceptions'] .= $appendText;
        if($isLogging){
            bd_appendToLog("Appending Exception:".$appendText, 'learning_soap.log');
        }
        updateRowOnQueue(array('exceptions'));
    }

    function appendToPhpErrorLogColumn($error_log_messages){
        global $isLogging, $queueRow;
        if($isLogging){
            bd_appendToLog("Appending to phpErrorLogColumn".$error_log_messages, 'learning_soap.log');
        }
        $queueRow['php_error_log'] .= "\n" . $exception . "\n";
        updateRowOnQueue(array('php_error_log'));
    }

    function getQueueRowForID($rowID){
        $result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `id`='[1]'", $rowID);
        if(!$result){
            bd_appendToLog('MySQL error -f7hagi- in '.__FILE__.' lavu_dberror():'.lavu_dberror(), 'learning_soap.log');
            error_log('MySQL error -f7hagi- in '.__FILE__.' lavu_dberror():'.lavu_dberror()); return false;
        }
        if(!mysqli_num_rows($result)){ return false; }//Bad Ticket
        return mysqli_fetch_assoc($result);
    }

    function appendRowToBackreferenceSubstitutionHistory($backreferenceTokenMapForThisIndex){
        global $queueRow;
        if(empty($backreferenceTokenMapForThisIndex)){ return; }//limit output where necessary.
        $queueRow = getQueueRowForID($queueRow['id']);
        $textToAppend = "Back Substitution Token Map for action_index:".$queueRow['action_index'].":\n".print_r($backreferenceTokenMapForThisIndex,1)."\n\n";
        $fullText = $queueRow['back_reference_substitution_history'] . $textToAppend;
        $result = sec_query("UPDATE `poslavu_QUICKBOOKS_db`.`queue` SET `back_reference_substitution_history`='[1]' WHERE `id`='[2]'", $fullText, $queueRow['id']);
        if(!$result){
            error_log("MySQL Error in ".__FILE__." -hjfut- error: " . lavu_dberror());
            echo "MySQL Error in ".__FILE__." -hjfut- error: " . lavu_dberror();
            return false;
        }
    }

    function appendInfoLineToBackreferenceSubstitutionHistory($infoLine){
        global $queueRow;
        //$queueRow = getQueueRowForID($queueRow['id']);
        $textToAppend = "NOTICE FOR ACTION_INDEX:".$queueRow['action_index']."\n{\n".$infoLine."\n}\n\n";
        $fullText = $queueRow['back_reference_substitution_history'] . $textToAppend;
        $result = sec_query("UPDATE `poslavu_QUICKBOOKS_db`.`queue` SET `back_reference_substitution_history`='[1]' WHERE `id`='[2]'", $fullText, $queueRow['id']);
        if(!$result){
            error_log("MySQL Error in ".__FILE__." -5e5j5- error: " . lavu_dberror());
            echo "MySQL Error in ".__FILE__." -5e5j5- error: " . lavu_dberror();
            return false;
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  G E N E R A L     H E L P E R    S U B R O U T I N E S    F O R    Q U E U E    P R O C E S S I N G . . .
    //
    function splitColumnDataIntoValuesAndMetadataArr($columnDataStr){
        $valuesOnlyArr = array();
        $metadataOnlyArr = array();
        $valMetaPairs = explode(getTransactionDelimiter(), $columnDataStr);
        $valMetaPairs = array_splice($valMetaPairs, 1);
        foreach($valMetaPairs as $currPairStr){
            $currPairArr = explode(getMetadataDelimiter(), $currPairStr);
            $currValue = $currPairArr[0];
            $currMetad = count($currPairArr) == 2 ? $currPairArr[1] : "";
            $valuesOnlyArr[] = $currValue;
            $metadataOnlyArr[] = $currMetad;
        }
        return array("values_arr" => $valuesOnlyArr, 'metadata_arr' => $metadataOnlyArr);
    }

    function combineValueMetadataSplitArraysIntoColumnText(&$splitArr){
        $metadataArr = $splitArr['metadata_arr'];
        $valuesArr = $splitArr['values_arr'];
        $indexCount = count($valuesArr);//SHOULD EQUAL metadataCount raise exception if doesnt.
        $stringBuilder = '';
        for($i = 0; $i < $indexCount; $i++){
            $stringBuilder .= getTransactionDelimiter();
            $stringBuilder .= $valuesArr[$i];
            $stringBuilder .= getMetadataDelimiter();
            $stringBuilder .= $metadataArr[$i];
        }
        return $stringBuilder;
    }

    function updateDatabaseAppendMetadataIntoColumnAtIndex($metaData, $columnName, $index){
        global $queueRow;
        $columnDataStr = $queueRow[$columnName];
        $columnDataValueMetaArrs = splitColumnDataIntoValuesAndMetadataArr($columnDataStr);
        $columnDataValueMetaArrs['metadata_arr'][$index] .= $metaData;
        $updatedColumnStr = combineValueMetadataSplitArraysIntoColumnText($columnDataValueMetaArrs);
        $queueRow[$columnName] = $updatedColumnStr;
        return updateRowOnQueue($columnName);
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  F U N C T I O N S     F O R     B A C K _ R E F E R E N C E _ E X P R E S S I O N    S U B S T I T U T I O N S
    //

    //Looks at current qbXMLRequest by the current `action_index`, detects any back-reference-expressions and performs substitution by evaluation on qbXMLResponses.
    //This is called at sendXMLRequest time first thing so we may perform the lookup-substitutions relative to former qbXMLResponses on this same queue row.
    $global_back_reference_token_map; //Global Access Variable.
    $global_qbxml_responses_array;
    function getBackReferenceSubstitutionOfCurrentQBXMLRequestByActionIndex(){
        global $queueRow;
        global $global_back_reference_token_map;
        global $global_qbxml_responses_array;
        global $logDebuggingCallbackReferenceSubstitution;

        //inits
        $global_back_reference_token_map = array();
        $global_qbxml_responses_array = array();

        //Retrieve the dynmically returned qbXMLResponses
        $xmlResponsesDelimitedStr = $queueRow['receiveResponseXMLs'];
        $responsesValsMetasArr = splitColumnDataIntoValuesAndMetadataArr($xmlResponsesDelimitedStr);
        $global_qbxml_responses_array = $responsesValsMetasArr['values_arr'];

        //Retrieve the current qbXMLRequest
        $xmlRequestsDelimitedStr = $queueRow['sendRequestXMLs'];
        $requestsValsMetasArr = splitColumnDataIntoValuesAndMetadataArr($xmlRequestsDelimitedStr);
        $requestsXMLs = $requestsValsMetasArr['values_arr'];
        $currentXMLRequest = $requestsXMLs[$queueRow['action_index']];

        //Finally we do the processing
        //Notice grabBackReferenceKeys performs a global access mutation on $global_back_reference_token_map adding all the backreference expressions as key, '' vals.
        preg_replace_callback('/\{\[\(.*?\)\]\}/', 'grabBackReferenceKeys', $currentXMLRequest);
        $global_back_reference_token_map = loadBackReferenceValuesIntoTokenMap($global_qbxml_responses_array, $global_back_reference_token_map);
        $currentXMLRequest_xmlOnly = performStringReplaceOnQBXMLRequestStr($currentXMLRequest, $global_back_reference_token_map);
        appendRowToBackreferenceSubstitutionHistory($global_back_reference_token_map);

        return $currentXMLRequest_xmlOnly;
    }

    //1.)
    function grabBackReferenceKeys($callbackArr){
        global $global_qbxml_responses_array;
        global $global_back_reference_token_map;
        $global_back_reference_token_map[$callbackArr[0]] = '';
    }
    //2.)
    function loadBackReferenceValuesIntoTokenMap($global_qbxml_responses_array, $global_back_reference_token_map){
        global $queueRow;
        foreach($global_back_reference_token_map as $key => $val){
            $innerContentsOfCurrBackRefExp = substr($key, 3, -3);
            $innerContentPartsOfBackRefExp = explode("||",$innerContentsOfCurrBackRefExp);
            $backreferenceFound = false;
            foreach($innerContentPartsOfBackRefExp as $currBackRefOptionStr){
                $currOptionsDerivedValue = backreferenceExpressionOptionToValue($currBackRefOptionStr, $global_qbxml_responses_array);
                if($currOptionsDerivedValue !== false){
                    $global_back_reference_token_map[$key] = $currOptionsDerivedValue;
                    $backreferenceFound = true; break;
                }
            }
            if(!$backreferenceFound){ throwExceptionOntoQueue("All options have failed for back-reference expression:$key");}
        }
        return $global_back_reference_token_map;
    }
    //2.a)
    function backreferenceExpressionOptionToValue($back_reference_option_str, $global_qbxml_responses_array){
        global $queueRow;
        $currBackRefOptionSplit = explode(':', $back_reference_option_str);
        if(count($currBackRefOptionSplit) == 1){
            if(preg_match('/^((&apos;)|(&quot;)).*((&apos;)|(&quot;))$/', $back_reference_option_str)){
                $currBackOptionStr = substr($currBackOptionStr, 6, -6);
                //$global_back_reference_token_map[$key] = $currBackOptionStr;
                return $currBackOptionStr;
            }
            return false;
        }
        else{
            $responseArrIndex = trim($currBackRefOptionSplit[0]);
            $firstColonIndex  = strpos($back_reference_option_str,':');
            $drillDownPartStr = trim(substr($back_reference_option_str, $firstColonIndex+1));
            $drillDownPartStr = str_replace('&apos;',"'",$drillDownPartStr);
            $drillDownPartStr = str_replace('&quot;',"'",$drillDownPartStr);
            $drillDownPartStr = substr($drillDownPartStr, 2, -2);//Cut off [' and '] from ends.
            $drillDownPartsArr = explode("']['", $drillDownPartStr);
            $final_val = getDrillDownValueFromXML_orFalse($global_qbxml_responses_array[$responseArrIndex], $drillDownPartsArr, $responseArrIndex);
            return $final_val;
        }
    }
    //2.a.1)
    function getDrillDownValueFromXML_orFalse($xmlStr, $drillDownSequenceArr, $responseArrIndex){
        global $queueRow;
        $xmlObj = new SimpleXMLElementEnhanced($xmlStr);
        if(!$xmlObj->parsedSuccessfully()){
            $exceptionString = "SimpleXMLElementEnhanced could not parse the xml string:\n$xmlStr";
            throwExceptionOntoQueue($exceptionString);
            return false;
        }
        $nodeDerivedByDrillDownSequence = $xmlObj->getNodeAtPathOrFalse($drillDownSequenceArr);
        $drillDownDebug = $xmlObj->getLastPathDrilldownDebug();
        appendInfoLineToBackreferenceSubstitutionHistory("Drilling on response index:".$responseArrIndex."\n"."Index drill down info:".$drillDownDebug);
        $value = $nodeDerivedByDrillDownSequence ? $nodeDerivedByDrillDownSequence->getInnerText() : false;
        return $value;
    }
    //3.)
    function performStringReplaceOnQBXMLRequestStr($qbXMLRequestStr, $backReferenceTokenMap){
        foreach($backReferenceTokenMap as $key => $val){
            $qbXMLRequestStr = str_replace($key, $val, $qbXMLRequestStr);
        }
        return $qbXMLRequestStr;
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  F A C T O R E D - S U B R O U T I N E S  (MOSTLY SPECIFIC TO DIFFERING PROCESS/FUNCTION/ALGORYTHM(s),
    //                                                                       BUT EXTRACTED OUT FOR CLEANLINESS/INTELLIGIBILITY)
    //
    //
    // Subroutine functions for the QBWC interface method: receiveResponseXMLImpl()
    //
    function append_qbxmlresponse_and_meta_to_string($ticket, $response, $hresult, $message, $textToAppendTo){
        global $verboseLogging, $logging;
        $textToAppendTo .= getTransactionDelimiter();
        $textToAppendTo .= $response;
        $textToAppendTo .= getMetadataDelimiter();
        $textToAppendTo .= "TICKET[".$ticket."]\n";
        $textToAppendTo .= "HRESULT[".$hresult."]\n";
        $textToAppendTo .= "MESSAGE[".$message."]\n";

        return $textToAppendTo;
    }
    function getCorrespondingResponseHandlerFromRow(){
        global $verboseLogging, $logging, $queueRow;
        $handler_functions_str = $queueRow['receivedResponseHandlers'];
        $handler_functions_arr = explode(getTransactionDelimiter(),$handler_functions_str);
        $handler_functions_arr = array_slice($handler_functions_arr, 1);
        $handler_function_pre  = $handler_functions_arr[$queueRow['action_index']];
        $handler_function = trim($handler_function_pre).'_TransactionHandler';
        if($logging){
            bd_appendToLog('Handler function found as:'.$handler_function,'learning_soap.log');
        }
        return $handler_function;
    }
    function checkIfHandlingFunctionExistsPutExceptionOnQueueIfDoesnt($responseHandlingFunction){
        global $queueRow;
        if(!function_exists($responseHandlingFunction)){
            throwExceptionOntoQueue('Could not find the response handling function when `action_index` was:'.$queueRow['action_index']);
            return false;
        }
        return true;
    }
    function calculatePercentFinishedWithDataname($accountRowFromTicket){
        //TODO FINISH THIS PART
        return 22;
    }
    //
    // Subroutine functions for the QBWC interface method: receiveResponseXMLImpl()
    //
    function initializeRowIfActionIndexEqZero($accountRow){
        global $queueRow;
        //If this is the first QBXML for this queue row.
        if($queueRow['action_index'] == 0){
            $queueRow['server_date_time_pulled'] = date('Y-m-d H:i:s');//We set the new fields and update
            $queueRow['ticket_session_identifier'] = $accountRow['ticket_session_identifier'];
            updateRowOnQueue();//We pulled this array added fields now updating it.
        }
    }
    function getSendRequestXMLFromQueueRowAtActionIndex(){
        global $queueRow;
        //We pull the xml element from the xml array stored in receiveResponseXMLs.
        $XMLArrayDelimitedStr = $queueRow['sendRequestXMLs'];
        $XMLArrayMetaWithDataArr = array_slice(explode(getTransactionDelimiter(),$XMLArrayDelimitedStr),1);
        $XMLMetaWithDataStr = $XMLArrayMetaWithDataArr['action_index'];
        $XMLMetaWithDataArr = array_slice(explode(getMetadataDelimiter(),$XMLArrayMetaWithDataStr),1);
        $sendXMLDataStr = $XMLMetaWithDataArr[0];
        return $sendXMLDataStr;
    }
    function arrayOfFilterValuesToKeyedArrayEqualToSameValues_falseIfNotArray($filterArray){
        if(!$filterArray || !is_array($filterArray)){ return false; }
        $filterByKeyArray = array();
        foreach($filterArray as $val){
            $filterByKeyArray[$val] = $val;
        }
        return $filterByKeyArray;
    }
    //////////////////////////////////////////////////////////////////////////////////
    // O R D E R    I D    B A S E 6 4    E N C O D I N G   T H I N G S.
    // Order id base 64 encoding functions.
    // 0-9, then a-z, then A-Z, then +, then /.  e.g. 0=0, a=10,A=a+26
    function lavu_int_2_base64str($number){
        if(!is_numeric($number)) return false;
        $intVal = intval($number);
        $builderStr = "";
        while($intVal > 63){
            $remainder = $intVal % 64;
            $builderStr = lavu_base64_int_2_char($remainder) . $builderStr;
            $intVal = intval($intVal/64);
        }
        $builderStr = lavu_base64_int_2_char($intVal) . $builderStr;
        return $builderStr;
    }
    function lavu_base64str_2_int($base64Str){
        $base64Str = ''.$base64Str;
        $intBuilder = 0;
        for($i = 0; $i < strlen($base64Str); $i++){
            $currChar = substr($base64Str, $i, 1);
            $intBuilder *= 64;
            $intBuilder += lavu_base64_char_2_int($currChar);
        }
        return $intBuilder;
    }
    function lavu_base64_int_2_char($lt64int){
        $lt64int = intval($lt64int);
        if($lt64int < 10)  return '' . $lt64int;
        if($lt64int < 36)  return '' . chr( $lt64int - 10 + ord('a') );
        if($lt64int < 62)  return '' . chr( $lt64int - 36 + ord('A') );
        if($lt64int == 62) return '+';
        if($lt64int == 63) return '/';
    }
    function lavu_base64_char_2_int($char){
        $asciiValue = ord($char);
        if($char == '+') return 62;
        if($char == '/') return 63;
        if($asciiValue >= ord('0') && $asciiValue <= ord('9')) return $asciiValue - ord('0');
        if($asciiValue >= ord('a') && $asciiValue <= ord('z')) return $asciiValue - ord('a') + 10;
        if($asciiValue >= ord('A') && $asciiValue <= ord('Z')) return $asciiValue - ord('A') + 36;

    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Q B A P I - F U N C T I O N S
    //
    //  F U N C T I O N S   A S   P E R T A I N S   O T H E R   S E R V E R S
    //
    //  I N T E G R A T I N G   W I T H   T H I S   Q B   S E R V E R.
    //
    //  Called as a precaution for all qbapi.php calls.
    function validateRequestSecurityAndFunction(){
        //Validate existence of needed parameters.
        if(  empty($_REQUEST)                  ) return '{"status":"failed","reason":"missing all arguments"}';
        if(  empty($_REQUEST['server_name'])   ) return '{"status":"failed","reason":"missing server_name argument"}';
        if( !isset($_REQUEST['data_name'])     ) return '{"status":"failed","reason":"missing data_name argument"}';//Left blank if not needed, but should still be set.
        if(  empty($_REQUEST['security_key'])  ) return '{"status":"failed","reason":"missing security_key argument"}';
        if(  empty($_REQUEST['security_hash']) ) return '{"status":"failed","reason":"missing security_hash argument"}';
        if(  empty($_REQUEST['function'])      ) return '{"status":"failed","reason":"missing function argument"}';
        if( !isset($_REQUEST['response_url'])  ) return '{"status":"failed","reason":"missing response_url argument"}';//Left blank for no response, but should still be set.
        if( !empty($_REQUEST['function'])  && empty($_REQUEST['func_args'])) return '{"status":"failed","reason":"if function is specified, must provide func_args JSON string."}';
        if( !empty($_REQUEST['func_args']) && LavuJson::json_decode($_REQUEST['func_args'], 1) === null) return '{"status":"failed","reason":"func_args json deserialization has failed -3fw-."}';
        $serverRow = getServerRow($_REQUEST['server_name'], $_REQUEST['security_key']);
        if( !$serverRow ) return '{"status":"failed","reason":"invalid server_name security_key pair"}';
        //All Servers and Clients do not have enough info to configure a bullshit sec_key.
        $dynHashEmbryo=$_REQUEST['server_name'].$_REQUEST['function'].$_REQUEST['data_name'].$serverRow['server_sec_token'];
        $expSecHash = sha1($dynHashEmbryo);

        if($_REQUEST['security_hash'] != $expSecHash) return '{"status":"failed","reason":"incorrect security hash"}';

        $api_function = $_REQUEST['function'];
        $api_function = 'qbapi_'.$api_function;
        if(!function_exists($api_function)){
            return "qbapi function:[$api_function] does not exist";
        }

        return true;
    }
    //Server from the `subscribing_servers` table.
    function getServerRow($serverName, $serverSecKey){
        global $logging;
        $queryStr = "SELECT * FROM `poslavu_QUICKBOOKS_db`.`subscribing_servers` WHERE `server_name`='[1]' AND `server_sec_key`='[2]'";
        $result = sec_query($queryStr, $serverName, $serverSecKey);
        if(!$result){
            error_log("Mysql error in ".__FILE__." -3hrg749- mysql error:".lavu_dberror());
            bd_appendToLog("Mysql error in ".__FILE__." -3hrg749- mysql error:".lavu_dberror(), 'learning_soap.log');
            return false;
        }
        if(mysqli_num_rows($result)==0){
            if($logging)
                bd_appendToLog("Mysql error in ".__FILE__." -3hrg749- mysql error:".lavu_dberror(), 'learning_soap.log');
            return false;
        }
        else{
            return mysqli_fetch_assoc($result);
        }
    }
    function getFunctionArgumentsFromRequest(){
        $returnArr = LavuJson::json_decode($_REQUEST['func_args'],1);
        if($returnArr == null){
            echo '{"status":"failed","reason":"func_args json deserialization has failed -8g4-."}';
            exit;
        }
        return $returnArr;
    }
    function returnFrom_qbapi_queueRowInsertRequest($insertID){
        global $script_start_time, $script_start_micro;
        $script_end_time = date('Y-m-d H:i:s');
        $script_end_micro = microtime(true);
        $totalMicroTime = $script_end_micro - $script_start_micro;
        $startTimeEndTimeJSONStr = '"api_start_time":"'.$script_start_time.'","api_end_time":"'.$script_end_time.'",';
        if(isset($_REQUEST['debug_show_full_row']) && $_REQUEST['debug_show_full_row']){
            $queueRow = getQueueRowForID($insertID);
            $queueRowJSON = LavuJson::json_encode($queueRow);
            $queueRowJSON = str_replace('"', '\\"', $queueRowJSON);
            echo '{"status":"success",'.$startTimeEndTimeJSONStr.'"total_exec_time_micro":"'.$totalMicroTime.'","queue_row":"'.$queueRowJSON.'"}';
        }else{
            echo '{"status":"success",'.$startTimeEndTimeJSONStr.'"total_exec_time_micro":"'.$totalMicroTime.'","queue_row":"'.$insertID.'"}';
        }
        exit;
    }
    function checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID,$failInfo){
        global $script_start_time, $script_start_micro;
        $script_end_time = date('Y-m-d H:i:s');
        $script_end_micro = microtime(true);
        if(empty($insertID)){
            $returnJSON = '{"status":"failed",'.
                           '"fail_reason":"returned insert id was empty",'.
                           '"more_info":"'.$failInfo.'",'.
                           '"api_start_time":"'.$script_start_time.'",'.
                           '"api_end_time":"'.$script_end_time.'",'.
                           '"total_exec_time_micro":"'.$totalMicroTime.'"'.
                           '}';

            echo $returnJSON;
            exit;
        }
        return;
    }

    function getOtherQueueRowsWithSameOrderIDOrderedByIDDesc($data_name, $orderID){
        $query = "SELECT * FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `data_name`='[1]' AND `order_id`='[2]' AND `overridden_by`='0' AND `invalidated`='0' ".
                 "ORDER BY `id` DESC";
        $result = sec_query($query,$data_name, $orderID);
        if(!$result){
            echo "mysql error: ".lavu_dberror();
            error_log("mysql error in ".__FILE__." error:".lavu_dberror());
            return false;
        }
        $rows = array();
        while($currRow = mysqli_fetch_assoc($result)){
            $rows[] = $currRow;
        }
        return $rows;
    }

    function markAllOverriddenRowsWithOverridersID($rows, $overridersID){
        $ids = array();
        foreach($rows as $curr){
            $ids[] = intval($curr['id']);
        }
        $inStatement = "('" . implode($ids, "','") . "')";
        $queryStr = "UPDATE `poslavu_QUICKBOOKS_db`.`queue` SET `overridden_by`='[1]' WHERE `id` in $inStatement";
        $result = sec_query($queryStr, $overridersID);
        if(!$result){
            error_log("SQL error in ".__FILE__." error: " . lavu_dberror());
            return false;
        }
        return ConnectionHub::getConn("rest")->affectedRows();
    }

    function createServerQuickbooksIDJoinedMap($paymentIDs, $startingActionIndex){
        $paymentIDsMapJsonObj = array();
        $i = $startingActionIndex;
        foreach($paymentIDs as $currPaymentID){
            //Small bug in LavuJson encode, totally workable.  All keys cannot be integers, else a numerical array will result instead of an associative array.
            $paymentIDsMapJsonObj['_'.$i++] = array('serv_id'=>intval($currPaymentID));//we set the action_index, and the server_id, the response will set the quickbooks txnid.
        }
        return LavuJson::json_encode($paymentIDsMapJsonObj);
    }

    function getAllServerSidePaymentIDsFromRowsOverridden($overriddenQueueRows){//Should really only be one row, but just in case...
        $allServerSidePaymentIDs = array();//in lavu's case cc_transactions
        foreach($overriddenQueueRows as $currQueueRow){
            $paymentIDMap = LavuJson::json_decode($currQueueRow['server2QBsPaymentIDMap'], 1);
            if(empty($paymentIDMap)){ /* TODO throw exception!!! */ continue;}
            foreach($paymentIDMap as $underscoredActionIndexAsKey => $idMapObjectArr){//serversPaymentIDAsKey has '_' prepended so in json_enc/dec doesn't turn into numerical array.
                if(!empty($idMapObjectArr['serv_id']))
                    $allServerSidePaymentIDs[] = $idMapObjectArr['serv_id'];
            }
        }
        return $allServerSidePaymentIDs;
    }

    function getAllServerSidePaymentIDsPassedInFuncArgs($api_funcArgs){
        $idsArr = array();
        $multiPaymentAmountAndMethodArr = $api_funcArgs['receive_payment_and_method_amounts_arr'];
        foreach($multiPaymentAmountAndMethodArr as $curr){
            $idsArr[] = $curr['servers_payment_id'];
        }
        return $idsArr;
    }

    function get_server_paymentID_qb_vars($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc){
        $queueRowsAsc = array_reverse($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc);
        $serverID2VarsMap = array();
        foreach($queueRowsAsc as $currQueueRow){
            $currQueueRowPaymentIDMapJSONStr = $currQueueRow['server2QBsPaymentIDMap'];
            $currQueueRowPaymentIDMap = LavuJson::json_decode($currQueueRowPaymentIDMapJSONStr, 1);
            foreach($currQueueRowPaymentIDMap as $actionIndexAsKey => $varMapArr){
                if(empty($varMapArr['serv_id']) || empty($varMapArr['qb_txn']) || empty($varMapArr['edit_sequence'])){
                    // TODO If any of the expected variables are not there, raise an exception on the queue row.
                }
                $serverID2VarsMap[$varMapArr['serv_id']]['qb_txn'] = $varMapArr['qb_txn'];
                $serverID2VarsMap[$varMapArr['serv_id']]['edit_sequence'] = $varMapArr['edit_sequence'];
            }
        }
        return $serverID2VarsMap;
    }

    ///////////////////////////////////////////////////
    // FOR INVALIDATING OLDER - NON-PROCESSED ROWS.

    function getUnprocessedQueueRowsWithSameDatanameAndOrderID($dataName, $orderID){
        $result = sec_query("SELECT * FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `data_name`='[1]' AND `order_id`='[2]' AND `server_date_time_pulled`=''", $dataName, $orderID);
        if(!$result){ error_log('mysql error in '.__FILE__.' -shopjdht- error:'.lavu_dberror()); }
        $rows = array();//Should only contain 1 in the end, TODO put some kind of error/warning if there's more.
        while($currRow = mysqli_fetch_assoc($result)){
            $rows[] = $currRow;
        }
        //TODO HERE GIVE ERROR/WARNING IF count > 1.
        return $rows;
    }
    function getIDsFromRows($rows){
        $returnArr = array();
        foreach($rows as $curr){
            $returnArr[] = $curr['id'];
        }
        return $returnArr;
    }
    function invalidateUnprocessedQueueRowsWithSameDatanameAndOrderID($dataName, $orderID){
        $result = sec_query("UPDATE `poslavu_QUICKBOOKS_db`.`queue` SET `invalidated`='1' WHERE `data_name`='[1]' AND `order_id`='[2]' AND `server_date_time_pulled`=''", $dataName, $orderID);
        if(!$result){ error_log('mysql error in '.__FILE__.' -asef3a- error:'.lavu_dberror()); }
        return ConnectionHub::getConn("rest")->affectedRows();
    }
    function set_overridden_by_toZeroWherePointedToRecentlyInvalidatedRow($dataName, $orderID, $queueIDs){
        if(empty($queueIDs))
            return;
        $queueIDsInts = array();
        foreach($queueIDs as $currID){ $queueIDsInts[] = intval($currID); }
        $inStatementList = "('".implode("','",$queueIDsInts)."')";
        $queryStr = "UPDATE `poslavu_QUICKBOOKS_db`.`queue` SET `overridden_by`='0' WHERE `data_name`='[1]' AND ".
                    "`order_id`='[2]' AND `overridden_by` IN ".$inStatementList; //Safe, provably integers only.
        $result = sec_query($queryStr, $dataName, $orderID);
        if(!$result){ error_log("MySQL error in ".__FILE__." -aeuyrg- error:".lavu_dberror()); }
    }


    // If a queue row is being inserted for a specific order_id and that order_id is in the middle of being processed, then we need to wait for the
    // row to fully finish processing before building the new queue row.
    function checkIfOrderInvoiceForCurrentOrderIDIsInProgressSendWaitMessageAndExitIfIs($dataName, $orderID){
        if(isOrderIDCurrentlyProcessing($dataName, $orderID)){
            echo '{"status":"busy","order_id":"'.$orderID.'","message":"Order cannot currently be put on queue since waiting on Quickbook response on currently running queue row, try again later."}';
            exit;
        }
    }
    function isOrderIDCurrentlyProcessing($dataName, $orderID){
        $result = sec_query("SELECT `id` FROM `poslavu_QUICKBOOKS_db`.`queue` WHERE `data_name`='[1]' AND `order_id`='[2]' AND `server_date_time_pulled`<>'' AND `server_date_time_closed`=''", $dataName, $orderID);
        if(!$result){ error_log('MySQL error in '.__FILE__.' -fjesrs- error:'.lavu_dberror());}
        if(mysqli_num_rows($result)){
            $row = mysqli_fetch_assoc($result);
            return $row['id'];
        }
    }
    function unfoldQueueRowAndSave2DBIfNeeded(&$queueRow){
        require_once(dirname(__FILE__).'/../qb_api_service/qbapi_unfolding_functions.php');
        if($queueRow['action_index'] == 0 &&
           $queueRow['total_actions'] == 0 &&
           $queueRow['initial_api_call'] != ""){
           $originalAPICall = LavuJson::json_decode($queueRow['initial_api_call'],1);
           $function = $originalAPICall['_POST']['function'];
           if($function == 'ensureItems_createInvoice_addPayments'){
               unfold_ensureItems_createInvoice_addPayments($queueRow, $originalAPICall);
           }
        }
    }

    function moneyFormatForQB($numericValue){
        error_log("moneyFormatInput:$numericValue");
        $output = money_format('%!^.2n',$numericValue);
        error_log("output formatted:$output");
        return $output;
    }



    ///Unfolding functions
    function mutativelyUpdateRowWithFinalUnfoldingSave2DB(&$queueRow, $sendRequestXMLsArr, $responseHandlerFunctions, $poNumber, $serverSidePaymentIDsJsonMap){
        /*
        $sanitizedXMLsDelimStr = getTransactionDelimiter() . implode(getTransactionDelimiter(), $sanitizedXMLsArr);
        $handlingFunctionsStr  = getTransactionDelimiter() . implode("\n".getTransactionDelimiter(), $partialTransactionResponseHandlers);
        $qStr  = "INSERT INTO `queue` ".
                 "(`ticket_session_identifier`,`data_name`,`sendRequestXMLs`,`server_date_time_created`,`receivedResponseHandlers`,`order_id`,`server2QBsPaymentIDMap`,`total_actions`) ";
        $qStr .= "VALUES (null, '[1]','[2]','[3]','[4]','[5]','[6]','".count($sanitizedXMLsArr)."')";
        $result = sec_query($qStr, $data_name, $sanitizedXMLsDelimStr, date('Y-m-d H:i:s'), $handlingFunctionsStr, $orderID, $cc_transIDs4Payments);
        if(!$result){ $error = "MySQL error in ".__FILE__." -h5wiuh- lavu_dberror():".lavu_dberror(); error_log($error); return false; }
        return ConnectionHub::getConn("rest")->InsertID();
        */
        $XMLsDelimStr = getTransactionDelimiter() . implode(getTransactionDelimiter(), $sendRequestXMLsArr);
        $handlingFunctionsStr  = getTransactionDelimiter() . implode("\n".getTransactionDelimiter(), $responseHandlerFunctions);

        $queueRow['sendRequestXMLs'] = $XMLsDelimStr;
        $queueRow['receivedResponseHandlers'] = $handlingFunctionsStr;
        $queueRow['total_actions'] = count($sendRequestXMLsArr);
        $queueRow['order_id'] = $poNumber;
        $queueRow['server2QBsPaymentIDMap'] = $serverSidePaymentIDsJsonMap;

        updateRowOnQueue();
    }

?>
