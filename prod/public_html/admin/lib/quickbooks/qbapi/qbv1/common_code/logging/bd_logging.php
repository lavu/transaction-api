<?php
    // logging
    global $bd_loggingDir;
    $bd_loggingDir = dirname(__FILE__) . "/bd_logs";
    $globals_max_log_size_bytes = 1042 * 1042 * 4;

    function bd_writeToLog($strVal, $logName = false){
        global $bd_loggingDir;
        $logName = $logName ? $logName :'default.log';
        $fp = fopen($bd_loggingDir . '/' . $logName, 'w');
        fwrite($fp, $strVal . "\n");
        fclose($fp);
        chmod($bd_loggingDir . "/" . $logName,0777);
    }

    function bd_appendToLog($strVal, $logName = false){
        global $bd_loggingDir;
        $logName = $logName ? $logName :'default.log';
        $fp = fopen($bd_loggingDir . '/' . $logName, 'a');
        fwrite($fp, $strVal . "\n");
        fclose($fp);
        chmod($bd_loggingDir . "/" . $logName,0777);
    }

    function bd_checkLogSize($logName){ //If log is too big, cuts it in half
        global $globals_max_log_size_bytes;
        global $bd_loggingDir;
        $fullLogPath    = $bd_loggingDir . '/' . $logName;
        $firstHalfPath  = $bd_loggingDir . '/xaa';
        $secondHalfPath = $bd_loggingDir . '/xab';
        $sizeInBytes = filesize($fullLogPath);

        if($sizeInBytes && $sizeInBytes > $globals_max_log_size_bytes){
            $splitSize = ceil($sizeInBytes/2);
            chdir(__DIR__ . "/log");
            lavuShellExec("split -b ".lavuShellEscapeArg($splitSize)." ".lavuShellEscapeArg($fullLogPath) );
            mv($secondHalfPath,$fullLogPath);
            chmod($fullLogPath,0777);
            unlink($firstHalfPath);
            chdir(__DIR__ . "/..");
        }
    }

    function repeatString($string, $count){
        $buffer = '';
        for($i=0;$i<$count;$i++){
            $buffer .= $string;
        }
        return $buffer;
    }
?>