<?php


    /*
      T H I S   I S   A N    E X A M P L E    D O C U M E N T A T I O N    P A G E,
      V I E W    I N    W E B    B R O W S E R.
    */
    require_once(dirname(__FILE__).'/QBXMLClasses.php');
    
    //An example XML document that may need to be parsed.
    $myXML = ''.
        //Should work with or without XML header information
        '<?xml version="1.0" encoding="utf-8"?>'.
        '<?QBXML version="2.0"?>'.
        '<QBXML>'.
        	'<QBXMLMsgsRq onError="stopOnError">'.
        		'<CustomerAddRq requestID="15">'.
        			'<CustomerAdd>'.
        				'<Name>20706 - Eastern XYZ University</Name>'.
        				'<CompanyName>Eastern XYZ University</CompanyName>'.
        				'<FirstName>Keith</FirstName>'.
        				'<LastName>Palmer</LastName>'.
        				'<BillAddress>'.
        					'<Addr1>Eastern XYZ University</Addr1>'.
        					'<Addr2>College of Engineering</Addr2>'.
        					'<Addr3>123 XYZ Road</Addr3>'.
        					'<City>Storrs-Mansfield</City>'.
        					'<State>CT</State>'.
        					'<PostalCode>06268</PostalCode>'.
        					'<Country>United States</Country>'.
        				'</BillAddress>'.
        				'<Phone>860-634-1602</Phone>'.
        				'<AltPhone>860-429-0021</AltPhone>'.
        				'<Fax>860-429-5183</Fax>'.
        				'<Email>keith@consolibyte.com</Email>'.
        				'<Contact>Keith Palmer</Contact>'.
        			'</CustomerAdd>'.
        		'</CustomerAddRq>'.
        	'</QBXMLMsgsRq>'.
        '</QBXML>';

    //The following keeps the new lines so it is easier on screen to read.
    $xmlForDisplay = '        <?xml version="1.0" encoding="utf-8"?>
        <?QBXML version="2.0"?>
        <QBXML>
        <QBXMLMsgsRq onError="stopOnError">
        	<CustomerAddRq requestID="15">
        		<CustomerAdd>
        			<Name>20706 - Eastern XYZ University</Name>
        			<CompanyName>Eastern XYZ University</CompanyName>
        			<FirstName>Keith</FirstName>
        			<LastName>Palmer</LastName>
        			<BillAddress>
        				<Addr1>Eastern XYZ University</Addr1>
        				<Addr2>College of Engineering</Addr2>
        				<Addr3>123 XYZ Road</Addr3>
        				<City>Storrs-Mansfield</City>
        				<State>CT</State>
        				<PostalCode>06268</PostalCode>
        				<Country>United States</Country>
        			</BillAddress>
        			<Phone>860-634-1602</Phone>
        			<AltPhone>860-429-0021</AltPhone>
        			<Fax>860-429-5183</Fax>
        			<Email>keith@consolibyte.com</Email>
        			<Contact>Keith Palmer</Contact>
        		</CustomerAdd>
        	</CustomerAddRq>
        </QBXMLMsgsRq>
        </QBXML>';
    
    echo '<br>';
    echo '<h2>SimpleXMLElementEnhanced Class Documentation.</h2>';
    echo '<h5>The below use cases are for parsing XML data being sent from Quickbooks.  I only implemented the \'Read Only\' aspects of XML parsing that are vital to the QB project.  Eventually, this class will be fully designed for write operations.  Thus the class as used below is almost entirely for parsing and navigating XML, not for generating XML.  For generating XML (when we are sending an XML request to QB) we use the specific class designed for the specific request.  This request class inherits from SimpleXMLElementEnhancedRequestAbstract and uses an incoming embedded array structure to produce the object that then generates itself into XML.  These individual classes are declared in the same file QBXMLClasses and are after the SimpleXMLElementEnhanced definition, and begins with "class addCustomer extends SimpleXMLElementEnhancedRequestAbstract"<br>--It is also noteworthy that these \'request\' type classes do ultimately inherit from SimpleXMLElementEnhanced, so many of the following methods should work in a correct context.</h5><br><br>';
    echo "<b><i>Our Sample XML that we will be parsing:</b></i><br><pre>".SimpleXMLElementEnhanced::xml_sanitize($xmlForDisplay)."</pre><br>";
    
    echo "<b><i>Here we load the xml document into an object and print it out using the asSanitizedXML() method:</i></b><br>";
    echo '<u>Creation code: <i>$SimpleXMLElementEnhanced_obj = new SimpleXMLElementEnhanced($myXML);</i></u><br>';
    echo '<u>Output the XML from the object as such: <i>$SimpleXMLElementEnhanced_obj->asSanitizedXML();</i></u><br>';
    $SimpleXMLElementEnhanced_obj = new SimpleXMLElementEnhanced($myXML);
    echo "<font size='2'>".$SimpleXMLElementEnhanced_obj->asSanitizedXML()."</font>";
    echo "<br><br><br>";
    
    echo "<b><i>Root Node- Now we take a look at the the root node in our 'XMLElementEnhanced' object, Name Attributes Inner Text and Children:</i></b><br>";
    echo 'Name: &nbsp;' . $SimpleXMLElementEnhanced_obj->getName() . '&nbsp; ----- &nbsp;($SimpleXMLElementEnhanced_obj->getName())<br>';
    echo 'Attributes: &nbsp;' . print_r($SimpleXMLElementEnhanced_obj->getAttributes(), 1) . '&nbsp; ----- &nbsp;$SimpleXMLElementEnhanced_obj->getAttributes())<br>';
    echo 'Inner Text: &nbsp;[' . $SimpleXMLElementEnhanced_obj->getInnerText() . ']&nbsp; ----- &nbsp;($SimpleXMLElementEnhanced_obj->getInnerText())<br>';
    echo 'Children Node List: &nbsp;' . print_r($SimpleXMLElementEnhanced_obj->getChildrenNodeList(), 1) . '&nbsp; ----- &nbsp;($SimpleXMLElementEnhanced_obj->getChildrenNodeList())<br>';
    echo '<i>This looks accurate.  There\'s no attributes nor inner text, the name of the node is correct, and there\'s only one child:&lt;QBXMLMsgsRq&gt;</i><br><br><br>';
    
    echo "<b><i>Accessing Children- Now we find the child QBXMLMsgsRq using the overloaded '[]' arrayaccess operator (e.g. ".'$'."SimpleXMLElementEnhanced_obj['QBXMLMsgsRq'])</b></i><br>";
    $qbxmlMsgsRq_node = $SimpleXMLElementEnhanced_obj['QBXMLMsgsRq'];
    echo "Name: &nbsp;" . $qbxmlMsgsRq_node->getName() . '<br>';
    echo "Attributes: &nbsp;" . print_r($qbxmlMsgsRq_node->getAttributes(), 1) . "<br>";
    echo "Inner Text: &nbsp;[" . $qbxmlMsgsRq_node->getInnerText() . "]<br>";
    echo "Children Node List: &nbsp;" . print_r($qbxmlMsgsRq_node->getChildrenNodeList(), 1) . "<br>";
    echo "<i>Again accurate.  There's no attributes nor inner text, the name of the node is correct, and there's only one child:&lt;QBXMLMsgsRq&gt;</i><br><br><br>";
    
    echo "<b><i>Drilling Down To Element- Here we drill down to find the contents of CustomerAdd and display its children list the drill down would like like this:</i></b><i><br>";
    echo '$customerAddNode = $SimpleXMLElementEnhanced_obj["QBXMLMsgsRq"]["CustomerAddRq"]["CustomerAdd"]</i><br>';
    $customerAddNode = $SimpleXMLElementEnhanced_obj["QBXMLMsgsRq"]["CustomerAddRq"]["CustomerAdd"];
    echo "Name: &nbsp;" . $customerAddNode->getName() . '<br>';
    echo "Attributes: &nbsp;" . print_r($customerAddNode->getAttributes(), 1) . "<br>";
    echo "Inner Text: &nbsp;[" . $customerAddNode->getInnerText() . "]<br>";
    echo "Children Node List: &nbsp;" . print_r($customerAddNode->getChildrenNodeList(), 1) . "<br><br><br>";
    
    echo "<b><i>Displaying Inner Text - Here we drill down and get the inner text of an element.</i></b><i><br>";
    echo '$lastName = $SimpleXMLElementEnhanced_obj["QBXMLMsgsRq"]["CustomerAddRq"]["CustomerAdd"]["LastName"]->getInnerText();</i><br>';
    $lastName = $SimpleXMLElementEnhanced_obj["QBXMLMsgsRq"]["CustomerAddRq"]["CustomerAdd"]["LastName"]->getInnerText();
    echo 'Outputs: '.$lastName;
?>