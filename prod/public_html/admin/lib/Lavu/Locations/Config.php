<?php
namespace Lavu\Location;

use Lavu\DB;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/../DB/LavuModel.php");

class Config extends DB\LavuModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'config';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}