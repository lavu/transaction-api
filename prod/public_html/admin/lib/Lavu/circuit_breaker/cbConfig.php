<?php
const PAPI_SERVICE  = "LavuPay";
const TGATE_SERVICE = "BridgePay";

/**
 * Function getRegisteredServices
 * Return list of registered services
 */
function getRegisteredServices(){
    #LavuPay - cardconnect using PAPI
    $papiThreshold       = getenv('CB_PAPI_FAILURETHRESHOLD');
    $papiRetryTimePeriod = getenv('CB_PAPI_RETRYTIMEPERIOD');
    $papiService         = array(
                            'servicename'      => PAPI_SERVICE,
                            'failureThreshold' => $papiThreshold,
                            'retryTimePeriod'  => $papiRetryTimePeriod,
                           );

    #BridgePay - Tgate
    $bridgePayThreshold  = getenv('CB_TGATE_FAILURETHRESHOLD');
    $bridgePayTimePeriod = getenv('CB_TGATE_RETRYTIMEPERIOD');
    $bridgePayService    = array(
                            'servicename'      => TGATE_SERVICE,
                            'failureThreshold' => $bridgePayThreshold,
                            'retryTimePeriod'  => $bridgePayTimePeriod,
                           );
    $services            = array(
                            PAPI_SERVICE  => $papiService,
                            TGATE_SERVICE => $bridgePayService,
                           );
    return $services;
}

?>