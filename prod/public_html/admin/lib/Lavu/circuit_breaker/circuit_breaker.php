<?php
namespace Lavu\CircuitBreaker;
use RedisConnection;
use Exception;

require_once(__DIR__."/../../../error_config.php");
require_once(__DIR__."/../../../api_private/Redis/RedisConnection.php");
include_once('cbConfig.php');
class CircuitBreaker
{
    private $servicesConfig;
    private $serviceSettings;
    protected $serviceName;
    const CB_DEFAULT_STATE = "close";
    public function  __construct($serviceName)
    {
        $this->serviceName    = $serviceName;
        $this->servicesConfig = getRegisteredServices();
        #Checke either service is configured or not
        if (!$this->serviceExists()) {
            #If not configured then throw exception
            throw new Exception("Please first configure the service in cbConfig.php");
        }
        #update the circuit-breaker settings, If user changes any setting in config
        $settings = $this->getServiceSettings();
        if (empty($settings)) {
            $settings = array(
                         'failurecount' => 0,
                         'circuitstate' => self::CB_DEFAULT_STATE,
                         'lastopentime' => date("Y-m-d H:i:s"),
                         'servicename'  => $this->servicesConfig[$this->servicename]['servicename'],
                        );
        }
        #Update settings from config
        $settings['failureThreshold'] = $this->servicesConfig[$this->servicename]['failureThreshold'];
        $settings['retryTimePeriod']  = $this->servicesConfig[$this->servicename]['retryTimePeriod'];
        #Update values in redis
        $this->serviceSettings = $settings;
        $this->setServiceSettings($settings);
    }

    /**
     * Function isCircuitClosed
     * Checks state(open or close) of circuit
     */
    public function isCircuitClosed()
    {
        #If circuit is close then return true
        if (isset($this->serviceSettings['circuitstate']) && $this->serviceSettings['circuitstate'] == self::CB_DEFAULT_STATE) {
            return true;
        }
        #if circuit is open so first we need to check either service timeout has passed. If not passed then return false else close the circuit
        $timepassed = $this->calculateTimeDifference(date("Y-m-d H:i:s"), $this->servicesettings['lastopentime']);
        #Time out has reached so close the circuit again to give it one more chance
        if ($timepassed > $this->serviceSettings['retryTimePeriod']) {
            #update service settings and save in Redis
            $this->serviceSettings['failurecount'] = $this->serviceSettings['failureThreshold']-1;
            $this->serviceSettings['circuitstate'] = self::CB_DEFAULT_STATE;
            $this->setServiceSettings($this->serviceSettings);
            return true;
        }
        #If timeout has not reached so keep circuit open
        return false;
    }

    /**
     * Function serviceFailed
     *
     * Reports that service has failed
     *
     */
    public function serviceFailed()
    {
        #Check if max failures for the service have been reached
        if ($this->serviceSettings['failurecount'] >= $this->serviceSettings['failureThreshold']) {
            #If max failures reached then open the circuit and update fields
            $this->serviceSettings['lastopentime'] = date("Y-m-d H:i:s");
            $this->serviceSettings['circuitstate'] = 'open';
            $this->setServiceSettings($this->serviceSettings);
        } else {
            #Increase failure count and save settings
            $this->serviceSettings['failurecount'] = $this->serviceSettings['failurecount']+1;
            $this->setServiceSettings($this->serviceSettings);
        }
    }

    /**
     * Function serviceSuccess
     *
     * Reports service has response
     *
     */
    public function serviceSuccess()
    {
        #If service succeeded so reset failure count and close circuit
        $this->serviceSettings['failurecount'] = 0;
        $this->serviceSettings['circuitstate'] = self::CB_DEFAULT_STATE;
        $this->setServiceSettings($this->serviceSettings);
    }

    /**
     * Function calculateTimeDifference
     *
     * Calculates difference in seconds between two dates
     *
     */
    private function calculateTimeDifference($date1, $date2)
    {
        $seconds = strtotime($date1) - strtotime($date2);
        return $seconds;
    }
    /**
     * Function serviceExists
     *
     * Checks either service is configured in config.php or not
     *
     */
    private function serviceExists()
    {
        if (array_key_exists($this->serviceName, $this->servicesConfig)) {
            return true;
        }
        # If Service is not configutred so return false
        return false;
    }

    /**
     * Function getServiceSettings
     *
     * Gets service settings from Redis
     *
     */
    private function getServiceSettings()
    {
        $settings = RedisConnection::RedisStringQuery("getString", $this->serviceName);
        if ($settings != "" || $settings != null) {
            return (array)json_decode($settings);
        }
        return array();
    }
    /**
     * Function getServiceSettings
     *
     * Sets service settings to Redis
     *
     */
    private function setServiceSettings($settings)
    {
        $data = json_encode($settings);
        RedisConnection::RedisStringQuery("setString", $this->serviceName, $data);
    }
}
?>
