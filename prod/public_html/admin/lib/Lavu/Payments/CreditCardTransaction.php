<?php
namespace Lavu\Payments;

use Lavu\DB;
use Lavu\Support\DateTimeFormat;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/../DB/LavuModel.php");
require_once(dirname(__FILE__) . "/../Support/DateTimeFormat.php");
require_once(dirname(__FILE__) . "/PaymentMethodType.php");

class CreditCardTransaction extends DB\LavuModel
{
    /**
     * Represents the earliest date to use on queries
     * that have a lower boundary.
     *
     * @var string
     */
    const DEFAULT_LOWER_BOUND_DATE = '-30 day';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cc_transactions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Query to retrieve _uncaptured_ transactions for orders closed
     * after a specific date, if none provided the function utilizes
     * the DEFAULT_LOWER_BOUND_DATE for the lower bound.
     *
     * @param  string       $locationId  Location's unique identifier
     * @param  string|null  $closedAfter Orders closed after this datetime
     * @return \Illuminate\Database\Eloquent\Builder
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public static function uncaptured($locationId=1, $closedAfter=null)
    {
        if (!isset($after)) {
            $after = date(
                DateTimeFormat::MYSQL_DATETIME,
                strtotime(self::DEFAULT_LOWER_BOUND_DATE)
            );
        }

        return self::query()
                ->leftJoin('orders', function($join)
                    {
                        $join->on('orders.order_id', '=', 'cc_transactions.order_id');
                        $join->on('orders.location_id', '=', 'cc_transactions.loc_id');
                    })
                ->where('cc_transactions.loc_id', '=', $locationId)
                ->where('cc_transactions.pay_type_id', '=', PaymentMethodType::CARD)
                ->where('cc_transactions.auth', '!=', '0')
                ->where('cc_transactions.got_response', '=', '1')
                ->where('cc_transactions.voided', '=', '0')
                ->where('orders.closed', '>', $after)
                ->where('orders.void', '=', '0');
    }

    /**
     * Query to retrieve transactions for orders closed
     * between a time boundary. If no boundary is provided
     * function defaults to DEFAULT_LOWER_BOUND_DATE for
     * the _lower boundary_ and to current datetime for the
     * _top boundary_.
     *
     * @param  string       $locationId Location's unique identifier
     * @param  string|null  $start      Orders closed after this datetime
     * @param  string|null  end         Orders closed before this datetime
     * @return \Illuminate\Database\Eloquent\Builder
     *
     */
    public static function between($locationId=1, $start=null, $end=null)
    {
        if (!isset($start)) {
            $start = date(
                DateTimeFormat::MYSQL_DATETIME,
                strtotime(self::DEFAULT_LOWER_BOUND_DATE)
            );
        }

        if (!isset($end)) {
            $end = date(DateTimeFormat::MYSQL_DATETIME, time());
        }

        return self::query()
                ->leftJoin('orders', function($join)
                    {
                        $join->on('orders.order_id', '=', 'cc_transactions.order_id');
                        $join->on('orders.location_id', '=', 'cc_transactions.loc_id');
                    })
                ->where('cc_transactions.loc_id', '=', $locationId)
                ->where('cc_transactions.pay_type_id', '=', PaymentMethodType::CARD)
                ->where('cc_transactions.got_response', '=', '1')
                ->where('cc_transactions.voided', '=', '0')
                ->where('orders.closed', '>=', $start)
                // ->where('orders.closed', '<=', $end)
                ->where('orders.void', '=', '0');
    }
}