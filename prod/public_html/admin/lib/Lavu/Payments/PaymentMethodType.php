<?php
namespace Lavu\Payments;

use MyCLabs\Enum\Enum;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");

class PaymentMethodType extends Enum
{
    const CASH = '1';
    const CARD = '2';
    const GIFT_CERTIFICATE = '3';
}