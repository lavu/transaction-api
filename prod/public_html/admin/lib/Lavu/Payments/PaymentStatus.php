<?php
namespace Lavu\Payments;

use MyCLabs\Enum\Enum;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");

class PaymentStatus extends Enum
{
    // Statuses returned from payment API
    const AUTHORIZED = 'A';
    const CAPTURED = 'C';
    const ACCEPTED = 'Y';
    const REJECTED = 'N';
    const DECLINED = 'D';
}