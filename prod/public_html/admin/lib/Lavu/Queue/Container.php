<?php
namespace Lavu\Queue;

use Illuminate\Container\Container as IlluminateContainer;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");

class Container extends IlluminateContainer
{
  public function isDownForMaintenance() {
    return false;
  }
}