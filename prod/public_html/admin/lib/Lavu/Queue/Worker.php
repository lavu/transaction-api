<?php
namespace Lavu\Queue;

use Illuminate\Queue\Worker as IlluminateWorker;
use Lavu\Queue\Queue;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/Queue.php");

class Worker
{
    private $queue;
    private $worker;

    public function __construct()
    {
        $this->queue = new Queue();
        $this->setWorker($this->queue);
    }

    /**
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function daemon()
    {
        $this->worker->daemon('redis', Queue::queueName());
    }

    /**
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function pop()
    {
        error_log("Attempting to pop a job from the " . Queue::queueName() . " queue");
        $this->worker->pop('redis', Queue::queueName());
    }

    private function setWorker($queue)
    {
        $container    = $this->queue->getContainer();
        $events       = $container['Illuminate\Contracts\Events\Dispatcher'];
        $handler      = null;
        $queueManager = $queue->getQueueManager();
        $this->worker = new IlluminateWorker($queueManager, $handler, $events);
    }
}