<?php
namespace Lavu\Queue;

use Illuminate\Queue\Capsule\Manager as IlluminateQueue;
use Illuminate\Redis\Database as Redis;
use Illuminate\Encryption\Encrypter;
use Illuminate\Events\Dispatcher;
use Illuminate\Events\EventServiceProvider;
use App\Jobs\BatchJob;

require_once(dirname(__FILE__) . "/../../../app/Jobs/BatchJob.php");
require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/Container.php");

class Queue
{
    const CONNECTION_TYPE = 'redis';

    /**
     * The container instance.
     *
     * @var \Illuminate\Container\Container
     */
    private $container;

    /**
     * The queue manager instance.
     *
     * @var \Illuminate\Queue\Capsule\Manager
     */
    private $illuminateQueue;

    /**
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function __construct(Container $container = null)
    {
        $this->setupContainer($container ?: new Container);

        $this->illuminateQueue = new IlluminateQueue($this->container);
        $redisConfig = [
            'driver'     => 'redis',
            'connection' => 'default',
            'queue'      => Queue::queueName(),
        ];
        $this->illuminateQueue->addConnection($redisConfig, Queue::CONNECTION_TYPE);
    }

    public static function queueName()
    {
        return getenv('ENVIRONMENT') . ':' . getenv('JOB_QUEUE_GROUPING');
    }

    /**
     * Setup the container instance.
     *
     * @param  \Illuminate\Contracts\Container\Container  $container
     * @return void
     */
    private function setupContainer($container)
    {
        // check for existing binding of redis in the container before adding a new one
        $container->bind(Queue::CONNECTION_TYPE, function () {
            $redisConnectionConfig = [
                'host' => getenv('REDIS_SOCKET_IP'),
                'port' => getenv('REDIS_SOCKET_PORT')
            ];
            return new Redis(['default' => $redisConnectionConfig]);
        });
        (new EventServiceProvider($container))->register();
        $container->instance('Illuminate\Contracts\Events\Dispatcher', new Dispatcher($container));
        $container->bind('encrypter', function() {
            return new Encrypter('foobar');
        });
        $container->bind('BatchJob', function($container) {
            return new BatchJob($container);
        });
        $this->container = $container;
    }

    /**
     * Get the queue manager instance.
     *
     * @return \Illuminate\Queue\QueueManager
     */
    public function getQueueManager()
    {
        return $this->illuminateQueue->getQueueManager();
    }

    public function getContainer()
    {
        return $this->container;
    }

    private function redisConnection()
    {
        return $this->getQueueManager()->connection(Queue::CONNECTION_TYPE);
    }

    /**
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function later($delay, $job, $data = '')
    {
        $this->redisConnection()->later($delay, $job, $data);
        error_log('Pushed a delayed instance of ' . $job . ' to redis.');
    }

    public function push($job, $data)
    {
        $this->redisConnection()->push($job, $data);
        error_log('Pushed an instance of ' . $job . ' to redis.');
    }
}