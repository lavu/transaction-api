<?php
namespace Lavu\Support;

/**
 * This class maintains a few pre-defined date format
 * constants.
 *
 * To understand what each _format_ character does refer
 * to PHP manual: https://www.php.net/manual/en/function.date.php
 */
class DateTimeFormat
{
    /**
     * Represents the format utilized by MySQL for DATE values.
     * MySQL retrieves and displays DATE values in 'YYYY-MM-DD' format.
     *
     * https://dev.mysql.com/doc/refman/5.5/en/datetime.html
     *
     * @var string
     */
    const MYSQL_DATE = 'Y-m-d';

    /**
     * Represents the format utilized by MySQL for DATETIME values.
     * MySQL retrieves and displays DATETIME values in 'YYYY-MM-DD hh:mm:ss' format.
     *
     * https://dev.mysql.com/doc/refman/5.5/en/datetime.html
     *
     * @var string
     */
    const MYSQL_DATETIME = 'Y-m-d H:i:s';

    /**
     * ISO 8601
     * In general, ISO 8601 applies to representations and formats of dates in
     * the Gregorian (and potentially proleptic Gregorian) calendar, of times
     * based on the 24-hour timekeeping system (with optional UTC offset), of
     * time intervals, and combinations thereof.
     *
     * more info: https://en.wikipedia.org/wiki/ISO_8601
     * Example: 2019-06-21T20:04:32+00:00
     *
     * @var string
     */
    const ISO8601 = 'Y-m-d\TH:i:sP';
}