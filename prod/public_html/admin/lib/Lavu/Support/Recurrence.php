<?php
namespace Lavu\Support;
use \RRule\RSet;
use DateTime;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");

class Recurrence
{
    /**
     * Gets the next occurrence of the RRULE after now.
     *
     * @param string $rruleString
     *
     * Suppress StaticAccess warning
     *
     * @return DateTime of occurrence
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public static function getNextOccurrence($rruleString)
    {
        $occurrences = self::getNextNOccurrences($rruleString, 1);
        return $occurrences[0];
    }

    /**
     * Takes an rruleString and returns the specified count number of occurrences
     *
     * @param array $rruleString
     * @param int   $count
     *
     * @return array of DateTime
     *
     * Suppress StaticAccess warning
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public static function getNextNOccurrences($rruleString, $count)
    {
        $now = new DateTime;
        $set = self::generateRruleSet($rruleString);
        return $set->getOccurrencesBetween($now, null, $count);
    }

    /**
     * Constructs an instance of RSet
     *
     * @param string        $rruleString
     *
     * @return RSet
     */
    public static function generateRruleSet($rruleString)
    {
        return new RSet($rruleString);
    }
}