<?php
namespace Lavu\DB;

use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Grammars\MySqlGrammar;
use Illuminate\Database\Query\Processors\MySqlProcessor;

/**
 * This class acts as an _adapter_ between Laravel's
 * DB\Connection and Lavu's DB connection.
 *
 * Suppress ToomanyMethods warnings for
 * this class as it needs to implement ConnectionInterface.
 *
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class Connection implements ConnectionInterface
{
    const SQL_PLACEHOLDER_REGEX = '/\s\?/';

    public function __construct()
    {
        $this->queryGrammar = new MySqlGrammar();
        $this->postProcessor = new MySqlProcessor();
    }

    /**
     * Begin a fluent query against a database table.
     *
     * @param  string  $table
     * @return \Illuminate\Database\Query\Builder
     */
    public function table($table)
    {

    }
    /**
     * Get a new raw query expression.
     *
     * @param  mixed  $value
     * @return \Illuminate\Database\Query\Expression
     */
    public function raw($value)
    {

    }
    /**
     * Run a select statement and return a single result.
     *
     * @param  string  $query
     * @param  array   $bindings
     * @param  bool  $useReadPdo
     * @return mixed
     */
    public function selectOne($query, $bindings=[], $useReadPdo=true)
    {

    }
    /**
     * Run a select statement against the database via Lavu's
     * DB connection implementation.
     *
     * @param  string  $query
     * @param  array   $bindings
     * @param  bool  $useReadPdo
     * @return array
     */
    public function select($query, $bindings=[], $useReadPdo=true)
    {
        $result = lavu_query(
            $this->toLavuQuery($query),
            (!empty($bindings) ? $bindings : null)
        );

        $resultsArray = array();
        while ($row = $result->fetch_assoc()) {
            $resultsArray[] = $row;
        }

        return $resultsArray;
    }
    /**
     * Run a select statement against the database and returns a generator.
     *
     * @param  string  $query
     * @param  array  $bindings
     * @param  bool  $useReadPdo
     * @return \Generator
     */
    public function cursor($query, $bindings=[], $useReadPdo=true)
    {

    }
    /**
     * Run an insert statement against the database.
     *
     * @param  string  $query
     * @param  array   $bindings
     * @return bool
     */
    public function insert($query, $bindings=[])
    {

    }
    /**
     * Run an update statement against the database.
     *
     * @param  string  $query
     * @param  array   $bindings
     * @return int
     */
    public function update($query, $bindings=[])
    {
        return lavu_query(
            $this->toLavuQuery($query),
            (!empty($bindings) ? $bindings : null)
        );
    }
    /**
     * Run a delete statement against the database.
     *
     * @param  string  $query
     * @param  array   $bindings
     * @return int
     */
    public function delete($query, $bindings=[])
    {

    }
    /**
     * Execute an SQL statement and return the boolean result.
     *
     * @param  string  $query
     * @param  array   $bindings
     * @return bool
     */
    public function statement($query, $bindings=[])
    {

    }
    /**
     * Run an SQL statement and get the number of rows affected.
     *
     * @param  string  $query
     * @param  array   $bindings
     * @return int
     */
    public function affectingStatement($query, $bindings=[])
    {

    }
    /**
     * Run a raw, unprepared query against the PDO connection.
     *
     * @param  string  $query
     * @return bool
     */
    public function unprepared($query)
    {

    }
    /**
     * Prepare the query bindings for execution.
     *
     * @param  array  $bindings
     * @return array
     */
    public function prepareBindings(array $bindings)
    {

    }
    /**
     * Execute a Closure within a transaction.
     *
     * @param  \Closure  $callback
     * @param  int  $attempts
     * @return mixed
     *
     * @throws \Throwable
     */
    public function transaction(Closure $callback, $attempts=1)
    {

    }
    /**
     * Start a new database transaction.
     *
     * @return void
     */
    public function beginTransaction()
    {
    }
    /**
     * Commit the active database transaction.
     *
     * @return void
     */
    public function commit()
    {
    }
    /**
     * Rollback the active database transaction.
     *
     * @return void
     */
    public function rollBack()
    {
    }
    /**
     * Get the number of active transactions.
     *
     * @return int
     */
    public function transactionLevel()
    {
    }
    /**
     * Execute the given callback in "dry run" mode.
     *
     * @param  \Closure  $callback
     * @return array
     */
    public function pretend(Closure $callback)
    {
    }

    /**
     * Get the query grammar used by the connection.
     *
     * @return \Illuminate\Database\Query\Grammars\Grammar
     */
    public function getQueryGrammar()
    {
        return $this->queryGrammar;
    }

    /**
     * Get the query post processor used by the connection.
     *
     * @return \Illuminate\Database\Query\Processors\Processor
     */
    public function getPostProcessor()
    {
        return $this->postProcessor;
    }

    /**
     * Returns a LavuQuery friendly string, Lavu's db connector
     * expects parameters to come in the form _'[<int>]'_.
     *
     * @param string $query QueryBuilder generated query
     * @return string
     */
    private function toLavuQuery($query)
    {
        $out = preg_replace_callback(
            self::SQL_PLACEHOLDER_REGEX,
            function() {
                static $occurrence = 0;
                $replacement = " '[" . $occurrence . "]' ";
                $occurrence++;
                return $replacement;
            },
            $query
        );

        return $out;
    }
}