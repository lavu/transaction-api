<?php
namespace Lavu\DB;

use Illuminate\Database\Eloquent;

require_once(dirname(__FILE__) . "/../../../vendor/autoload.php");
require_once(dirname(__FILE__) . "/Connection.php");

/**
 * This class extends Eloquent\Model so that we can
 * override the approach Eloquent\Model utilizes to
 * retrieve a connection to the DB.
 *
 * For Lavu purposes we want to use the existing
 * DB implementation until we can migrate to a
 * full ORM.
 */
class LavuModel extends Eloquent\Model
{
    /**
     * Resolve a connection instance.
     *
     * @return \Illuminate\Database\Connection
     */
    public static function resolveConnection()
    {
        return new Connection();
    }
}