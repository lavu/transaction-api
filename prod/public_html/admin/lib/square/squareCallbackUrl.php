<?php
	session_start();

	require_once( "../gateway_functions.php");
	require_once("../../components/payment_extensions/square/SquareCore.php");
	require_once("../../components/payment_extensions/square/SquarePaymentHandler.php");
	$data_name = $_SESSION["poslavu_234347273_admin_dataname"];

	$SquareCore = new SquareCore('', $data_name);

	$apiCredentials = $SquareCore->tokenHelper->getOAuthCredentials();//'poslavu_square'
	//$extensionId = $SquareCore->tokenHelper->getExtensionID();
	$connectHost = SQUARE_OAUTH_HOST;
	$applicationId = $apiCredentials['applicationID'];
	$applicationSecret = $apiCredentials['secretID'];

	# Headers to provide to OAuth API endpoints
	$oauthRequestHeaders = array (
		'Authorization' => 'Client ' . $applicationSecret,
		'Accept' => 'application/json',
		'Content-Type' => 'application/json'
	);

	function callback() {
		global $connectHost, $oauthRequestHeaders, $applicationId, $applicationSecret, $data_name;

		# Extract the returned authorization code from the URL
	//$_GET['code']='sq0cgp-jrz81jHWb_s_mByDDYtJuw';
	//$_GET['state'] = md5($data_name);

	// When the request comes from CP3, the dataname will already come hashed
		// with `-is_cp3` as part of the `state` in the URI.
		// So we do the same here and compare the hashes.
		$md5_state = md5($data_name."-is_cp3");
		$is_cp3 = $_GET['state'] == $md5_state;


		if ($_GET['state'] == md5($data_name) || $is_cp3) {
			$authorizationCode = $_GET['code'];
			if ($authorizationCode) {
				# Provide the code in a request to the Obtain Token endpoint
				$oauthRequestBody = array(
					'client_id' => $applicationId,
					'client_secret' => $applicationSecret,
					'code' => $authorizationCode
				);

				$curlvar = curl_init();

				curl_setopt($curlvar, CURLOPT_URL,            $connectHost . 'oauth2/token' );
				curl_setopt($curlvar, CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt($curlvar, CURLOPT_POST,           1 );
				curl_setopt($curlvar, CURLOPT_POSTFIELDS,     $oauthRequestBody);
				curl_setopt($curlvar, CURLOPT_HTTPHEADER,     $oauthRequestHeaders);

				$response=curl_exec ($curlvar);
					//echo $response = '{  "access_token": "sq0atp-srDNe8gYNkJaHxpGbFO_Sw",  "token_type": "bearer",  "expires_at": "2017-10-20T09:59:00Z",  "merchant_id": "B1RXFN8M2VQ4Z"}';
				//echo  $response = '{  "access_token": "sq0atp-wmg0ISTYbGBOTcRyA6lTjA",  "token_type": "bearer",  "expires_at": "2017-10-21T09:01:09Z",  "merchant_id": "HP813TQ610Y9H"}';
					error_log("Square return URL Response: ".print_r($response,true));
				$respArr = json_decode($response,true);

				# Extract the returned access token from the response body
				if (array_key_exists('access_token',$respArr)) {

					error_log('Access token: ' . $respArr['access_token']);
					$SquarePaymentHandler = new SquarePaymentHandler($data_name, $respArr['access_token']);
					$accountDetails = $SquarePaymentHandler->getMerchantDetails();
					$respArr['account_details'] = json_encode($accountDetails);
					//print_r($loc);
					//$locationsJson = json_encode($loc);

					$tokenHelper = new SquareTokenHelper();

					$tokenHelper->updateOrInsertOAuthToken($data_name,$respArr);
					$res_status = 'success';
					# The response from the Obtain Token endpoint did not include an access token. Something went wrong.
				} else {
					error_log('Code exchange failed!');
					$res_status = 'failed';
				}

				# The request to the Redirect URL did not include an authorization code. Something went wrong.
			} else {
				error_log('Authorization failed!');
				$res_status = 'auth_failed';
			}
		} else {
			error_log('Authorization failed! Invalid Data Name');
			$res_status = 'invalid_request';
		}

		$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
		$lavu_url = $actual_link."/cp/index.php?mode=extensions_all&square=".$res_status;

		// If the state has is_cp3, then redirect to cp3
		if ($is_cp3) {
			header('Location: '.$_SERVER['CP3_URL'].'/extensions/featured/square?square='.$res_status);
		} else {
			// Proceed with old PHP CP
			header('Location: '.$lavu_url);
		}
	}
	# Execute the callback
	callback();

?>