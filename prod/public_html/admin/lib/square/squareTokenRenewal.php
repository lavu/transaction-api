<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 10/12/17
 * Time: 5:15 PM
 */
require_once( __DIR__."/../gateway_functions.php");
require_once(__DIR__."/../../components/payment_extensions/square/SquareCore.php");
require_once(__DIR__."/../../components/payment_extensions/square/SquarePaymentHandler.php");
$defaultDay = 2;
$day = (isset($argv[1]) && $argv[1] != '') ? $argv[1] : $defaultDay;
$SquareCore = new SquareCore();
$renewalTokens = $SquareCore->tokenHelper->getTokensForRenewal($day);
if(!empty($renewalTokens)) {
	foreach ($renewalTokens as $tokenDetails) {
		$SquareCore->tokenHelper->setEnvironment($tokenDetails['dataname']);
		$SquarePaymentHandler = new SquarePaymentHandler($tokenDetails['dataname']);
		if (isset($tokenDetails['token']) && $tokenDetails['token'] != '') {
			$tokenResponse = $SquarePaymentHandler->renewSquareToken($tokenDetails['token']);
			if (!isset($tokenResponse['error'])) {
				if ($tokenResponse['access_token'] != '') {
					$response = $SquareCore->tokenHelper->updateAccessToken($tokenDetails['dataname'], $tokenResponse['access_token'], $tokenDetails['extension_id']);
					if ($response) {
						error_log("Success:- " . $tokenDetails['dataname'] . "square access token " . $tokenResponse['access_token'] . " renewed.");
					}
				}
			} else {
				error_log("Error:- \"" . $tokenDetails['dataname'] . "\" access token renewal failed. Message:- " . $tokenResponse['error']);
			}
		} else {
			error_log("Error:- \"" . $tokenDetails['dataname'] . "\" has no access token.");
		}
	}
}
