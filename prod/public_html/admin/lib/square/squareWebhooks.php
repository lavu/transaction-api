<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 2/26/18
 * Time: 2:54 PM
 */

# Listening for webhook notifications from the Square Connect API
# See Webhooks Overview for more information:
# https://docs.connect.squareup.com/api/connect/v1/#webhooks-overview

require_once("../../components/payment_extensions/square/SquareTokenHelper.php");
require_once("../../components/payment_extensions/square/SquarePaymentHandler.php");

# Application's webhook signature key, available from application dashboard
$webhookTokens = array('6D9W1sl0ADHaTpGj2_WaIA','cOj0lMK-m9EFmmq26BW8RA');
$callbackToken = $_REQUEST['token'];

	/**
	 * Retrieves payments by the IDs provided in webhook notifications.
	 *
	 * Note that you need to set your application's webhook URL from your application dashboard
	 * to receive these notifications. In this sample, if your host's base URL is
	 * http://example.com, you'd set your webhook URL to http://example.com/webhooks.php.
	 */
	function webhookCallback() {
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			# Get the JSON body and HMAC-SHA1 signature of the incoming POST request
			$callbackBody = file_get_contents('php://input');
			//$callbackSignature = getallheaders()['X-Square-Signature'];
			# Validate the signature
			# Load the JSON body into an associative array
			$callbackBodyJson = json_decode($callbackBody);
			if (!isValidCallback()) {
				# Fail if the signature is invalid
				error_log("Webhook event with invalid signature detected!");
				return;
			}
			# If the notification indicates a PAYMENT_UPDATED event...
			if (property_exists($callbackBodyJson, 'event_type') and $callbackBodyJson->event_type == 'PAYMENT_UPDATED') {
				# Get the ID of the updated payment
				$paymentId = $callbackBodyJson->entity_id;
				# Get the ID of the payment's associated location
				$locationId = $callbackBodyJson->location_id;
				# Get the merchant ID of the payment's associated location
				$merchantId = $callbackBodyJson->merchant_id;
				$tokenHelper = new SquareTokenHelper();
				$squareLocationToken = $tokenHelper->getLocationName($merchantId, $locationId);
				$token = $squareLocationToken['token'];
				$dataname = $squareLocationToken['dataname'];
				if ($token != '' && $dataname != '' && $locationId != '') {
					$SquarePaymentHandler = new SquarePaymentHandler($dataname, $token, $locationId);
					# Send a request to the Retrieve Payment endpoint to get the updated payment's full details
					$v1TransactionDetails = $SquarePaymentHandler->retrieveV1TransactionDetails($paymentId);
					$payment_url = $v1TransactionDetails['transaction']['payment_url'];
					$last_string = strrpos($payment_url, '/') + 1; // +1 so we don't  the space in our result
					$transaction_id = substr($payment_url, $last_string);
					$v2TransactionDetails = $SquarePaymentHandler->retrieveV2TransactionDetails($transaction_id);
					# Perform an action based on the returned payment (in this case, simply log it)
					error_log("Square Webhook Transaction Details== ".json_encode($v2TransactionDetails));
					if (isset($v2TransactionDetails['transaction'])) {
						$tokenHelper->processTransaction($v1TransactionDetails['transaction'], $v2TransactionDetails['transaction'], $dataname, false);
					} else {
						error_log("Not getting transaction details");
					}
				}
			}
		} else {
			error_log("Received a non-POST request");
		}
	}

	# Validates HMAC-SHA1 signatures included in webhook notifications to ensure notifications came from Square
	//function isValidCallback($callbackBody, $callbackSignature) {
	function isValidCallback() {
		global $webhookTokens, $callbackToken;
		return in_array($callbackToken, $webhookTokens);
	}

	# Process the callback
	webhookCallback();
