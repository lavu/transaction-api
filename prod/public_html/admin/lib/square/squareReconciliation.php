<?php
require_once("/home/poslavu/public_html/admin/components/payment_extensions/square/SquareTokenHelper.php");
require_once("/home/poslavu/public_html/admin/components/payment_extensions/square/SquarePaymentHandler.php");


Class SquareReconcile
{ 
	function processReconcile($reqdata)
	{
		$tokenHelper = new SquareTokenHelper();
		$transaction_id = $reqdata['transaction_id'];
		$location_id = $reqdata['location_id'];
		$data_name = $reqdata['data_name'];
		$start_date = $reqdata['start_date'];
		$end_date = $reqdata['end_date'];
		$user_id = $reqdata['user_id'];

		//If getTransactionDetails api could not fetch transaction details, Then we fetch it at the time of reconciliation
        if ($transaction_id != '' && $location_id != '' && $data_name != '') {
            $SquarePaymentHandler = new SquarePaymentHandler($data_name,'', $location_id);
	        $v1TransactionResponse = $SquarePaymentHandler->retrieveV1TransactionDetails($transaction_id);
	        if (isset($v1TransactionResponse['error'])) {
		        return 'failed';
	        }
	        $v2TransactionResponse = $SquarePaymentHandler->retrieveV2TransactionDetails($transaction_id);
            if (isset($v2TransactionResponse['transaction'])) {
	            return $tokenHelper->processTransaction($v1TransactionResponse['transaction'], $v2TransactionResponse['transaction'], $data_name);
            } else {
	            return 'pending';
            }
        } else if ($transaction_id == '' && $location_id == '' && $data_name != '') {
	        $SquarePaymentHandler = new SquarePaymentHandler($data_name);
	        $transactionsResponse = $SquarePaymentHandler->getTransactionswithInPeriod($start_date, $end_date);
	        return $SquarePaymentHandler->capturedTransactionsToReconcile($transactionsResponse, $data_name, $user_id);
        }
	}

    /*
     * update the table with the respective status based on id
     */
    function updateRecordStatus($status,$id)
    {

        mlavu_query("update gateway_sync_request set status='[1]',`updated_date`='[3]' where id='[2]'", $status, $id, date('Y-m-d H:i:s'));
    }
    
    function updateDepositStatus($dataarr)
    {
		$dataName = 'poslavu_'.$dataarr['data_name'].'_db';
		$transaction_id = $dataarr['transaction_id'];
		$ccQuery = mlavu_query("select `order_id` from `$dataName`.`cc_transactions` where  `transaction_id` = '$transaction_id'");
		$ccResult = mysqli_fetch_assoc($ccQuery);
		$ccOrderId = $ccResult['order_id'];
		
		mlavu_query("update `$dataName`.`deposit_information` set `pay_status` = 1 where `transaction_id` = '$ccOrderId'");
    }

}  
