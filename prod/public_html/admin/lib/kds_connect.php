<?php
	//error_log(__FILE__ . ": " . json_encode($_REQUEST));
	session_start();
	header("Content-type: application/json");

	require_once("info_inc.php");
	require_once("ml_connect.php");

	$mode = $_POST['mode'];
	$locationRow = getLocationRow();

	$serverDateTime = date('Y-m-d H:i:s');
	$localizedDateTime = localize_datetime(date('Y-m-d H:i:s'), $locationRow['timezone']);
	$requestTS = time();

	//error_log("Server Date Time:".$serverDateTime);
	//error_log("Localized Date Time:".$localizedDateTime);

	if($mode == 'save_bump'){
		echo "Received kds_data:".$_POST['kds_data'];
		$kds_data = json_decode($_POST['kds_data'],1);

		//error_log("kds data: " . $_POST['kds_data']);

		//Saving the ticket bump
        $insertArr = array();
        //$insertArr['setting'] = 'ticket_lifetime';
        $insertArr['order_id'] = $kds_data['orderId'];
        $insertArr['server_name'] = $kds_data['serverName'];
        $insertArr['device_time_created'] = $kds_data['device_time_created'];
        $insertArr['device_time_bumped'] = $kds_data['device_time_bumped'];
        $insertArr['ticket_duration_secs'] = strtotime($kds_data['device_time_bumped']) - strtotime($kds_data['device_time_created']);
        $insertArr['data1']  = json_encode($kds_data['bumpedData']);
        $insertArr['server_date_time'] = $serverDateTime;
        $insertArr['loc_date_time'] = $localizedDateTime;

        $insertStr = "(`".implode("`,`",array_keys($insertArr))."`) VALUES ('[".implode("]','[",array_keys($insertArr))."]')";

		$insertResult = lavu_query("INSERT INTO `kds_bumped_tickets` ".$insertStr, $insertArr);

		if(!$insertResult){
			//error_log("sql query error in ".__FILE__." query:".$insertStr." error:".mysqli_error());
			return;
		}

		//Saving the individual items bump
		$itemsArr = $kds_data['bumpedData']['orderItemsCollection'];
		$itemsIndividualBumpDataByIndex = $kds_data['bumpedData']['itemLevelBumpInfo'];
		$itemsBumpInsertQuery = "INSERT INTO `kds_bumped_items` (`item_id`,`device_time_sent`,`item_prep_time_secs`,`item_checked_time`,`bumped_ticket_id`,`quantity`) VALUES ";
		$rowsInsertArr = array();

		foreach ($itemsIndividualBumpDataByIndex as $bumpedItem) {
			//Get Variables
			$item_id = $bumpedItem['item_id'];
			$item_quantity = $bumpedItem['quantity'];
			$device_time_sent = $kds_data['device_time_created'];
			$item_checked_time = !empty($bumpedItem['item_checked_time']) ? $bumpedItem['item_checked_time'] : "";

			if(!empty($bumpedItem) && isset($bumpedItem['item_prep_time_secs'])) {
				$item_prep_time_secs = $bumpedItem['item_prep_time_secs'];
			} else {
				$item_prep_time_secs = strtotime($kds_data['device_time_bumped']) - strtotime($kds_data['device_time_created']);
			}
			$bumped_ticket_id = lavu_insert_id();

			//Create part of insert row.
			$insertItemArr = array($item_id,$device_time_sent,$item_prep_time_secs,$item_checked_time,$bumped_ticket_id,$item_quantity);
			$rowsInsertArr[] = "('".implode("','",$insertItemArr)."')";
		}

		// we only want to do the insert if there are records
		if (count($itemsIndividualBumpDataByIndex) > 0) {
			$itemsBumpInsertQuery .= implode(",", $rowsInsertArr);
			$itemsBumpResult = lavu_query($itemsBumpInsertQuery);
			//error_log("kds bump input: " . $itemsBumpInsertQuery);
		}
	} else if($mode == 'poll_request') {
		$kds_data = json_decode($_POST['kds_data'],1);

		$lastRequestTS = $kds_data['last_request_TS'] - 1;

		//error_log("kds data: " . print_r($kds_data,1));

		$activeOrderIDs = $kds_data['active_order_ids'];
		$activeOrderIDsSanitized = array_map(function($curr){ return lavu_query_encode($curr); }, $activeOrderIDs);
		$activeOrderIDsStrList = "('".implode("','", $activeOrderIDsSanitized)."')";
		$orders = array();

		//error_log("active order ID List: " . print_r($activeOrderIDsSanitized,1));

		// get order data
		$activeOrderResults = lavu_query("SELECT `order_id`,`ioid`,`last_mod_ts`,`order_status`, `original_id`, `sync_tag` FROM `orders` WHERE `order_id` IN " . $activeOrderIDsStrList . " AND `pushed_ts` >= " . $lastRequestTS . " ORDER BY `pushed_ts` DESC LIMIT 25");
		//error_log("SELECT `order_id`,`ioid`,`last_mod_ts`,`order_status`, `original_id`, `sync_tag` FROM `orders` WHERE `order_id` IN " . $activeOrderIDsStrList . " AND `pushed_ts` >= " . $lastRequestTS);
		//error_log(json_encode($activeOrderResults));
		//
		// if (!$activeOrderResults) {
		// 	error_log("mysqli error: " . mysqli_error());
		// }
		//error_log("number of rows: " . mysqli_num_rows($activeOrderResults));

		while($currOrderRow = mysqli_fetch_assoc($activeOrderResults)){
			$order = array();

			//error_log(json_encode($currOrderRow));
			// check all split checks in this order for remaining amount owed
			$order_ioid = "'".$currOrderRow['ioid']."'";
			$zeroRemaining = 0.00;
			$splitCheckRemaining = lavu_query("SELECT `id` FROM `split_check_details` WHERE `ioid` = $order_ioid AND `remaining` > $zeroRemaining");
			// if there is no amount remaining, order has been paid in full
			$currOrderRow['paid_in_full'] = mysqli_num_rows($splitCheckRemaining) > $zeroRemaining ? 'no' : 'yes';
			$order['order'] = $currOrderRow;

			// flag to let KDS know this is an order update
			$order['kds_comm_mode'] = "order_status_update";

			// get menu items data
			$itemUpdatesArr = array();
			$orderMenuItemResults = lavu_query("SELECT `course`,`icid`,`item`,`quantity`,`last_change_ts`,`last_mod_ts`, `notes`, `options`, `quantity`, `seat`, `special`, `void` FROM `order_contents` WHERE `ioid` = $order_ioid");
			//error_log("ioid: ".$order_ioid);
			//error_log("SELECT `course`,`icid`,`item`,`quantity`,`last_change_ts`,`last_mod_ts`, `notes`, `options`, `quantity`, `seat`, `special`, `void` FROM `order_contents` WHERE `ioid` = $order_ioid");
			// if (!$orderMenuItemResults) {
			// 	error_log("mysqli error: " . mysqli_error());
			// }
			while($orderMenuItemsRow = mysqli_fetch_assoc($orderMenuItemResults)){
				// get modifier data
				$item_icid = "'".$orderMenuItemsRow['icid']."'";
				//error_log("icid: ".$item_icid);
				$orderModifierResults = lavu_query("SELECT `mod_id`,`qty`,`type` FROM `modifiers_used` WHERE `icid` = $item_icid");
				while($orderModifierRow = mysqli_fetch_assoc($orderModifierResults)){
					// add mod_data to menu item
					$orderMenuItemsRow['mod_data'] = $orderModifierRow;
					//error_log("full menu items: " . print_r($orderMenuItemsRow,1));
				}
				// add menu item to order updates array
				$itemUpdatesArr[] = $orderMenuItemsRow;
				//error_log("menu items: " . print_r($orderMenuItemsRow,1));
			}
			// add everything to item updates
			$order['item_updates'] = $itemUpdatesArr;
			$orders[] = $order;
			//error_log("line: ".__LINE__);
			//error_log("orders: " . print_r($orders,1));
		}
		// send all the item update data
		$returnArr = array("poll_timer_secs" => "10", "order_updates" => $orders, "request_TS" => $requestTS);
		echo json_encode($returnArr);
	} else if($mode == 'load_kds_data'){
		//Menu Items
		$menuItemsResult = lavu_query("SELECT `id`,`name`,`ingredients`,`description` FROM `menu_items` WHERE `_deleted`='0'");
		$menuItemsArrRaw = array();
		while($currRow = mysqli_fetch_assoc($menuItemsResult)){
			$menuItemsArrRaw[$currRow['id']] = $currRow;
		}

		//marshall menu items data
		$menuItemsArr = array();
		foreach($menuItemsArrRaw as $menuItemID => $menuItemRow){
			$ingredientsArrRaw = explode(',', $menuItemRow['ingredients']);
			$ingredientsArr = array();
			foreach($ingredientsArrRaw as $currIDxAmount){
				$currIngredientArr = explode('x', $currIDxAmount);//76 x 1,79 x 2,92 x 1
				$ingredientID = trim($currIngredientArr[0]);
				$ingredientAmount = trim($currIngredientArr[1]);
				if(!empty($ingredientID) && !empty($ingredientAmount)){
					$ingredientsArr[] = array('ingredient_id' => $ingredientID,'ingredient_amount' => $ingredientAmount );
				}
			}
			$menuItemsArr[$menuItemID] = array( 'id' => $menuItemRow['id'], 'name' => $menuItemRow['name'],'description' => $menuItemRow['description'], 'ingredients' => $ingredientsArr);
		}

		//Ingredients
		$ingredientsResult = lavu_query("SELECT * FROM `ingredients` WHERE `_deleted`='0'");
		$ingredientsArr = array();
		while($currRow = mysqli_fetch_assoc($ingredientsResult)){
			$ingredientsArr[$currRow['id']] = $currRow;
		}

		//forced_modifiers
		$forcedModifiersResult = lavu_query("SELECT `id`,`title`,`ingredients` FROM `forced_modifiers` WHERE `_deleted`='0'");
		$forcedModifiersArr = array();
		while($currRow = mysqli_fetch_assoc($forcedModifiersResult)){
			$forcedModifiersArr[$currRow['id']] = $currRow;
		}

		//optional_modifiers
		$optionalModifiersResult = lavu_query("SELECT `id`,`title`,`ingredients` FROM `modifiers` WHERE `_deleted`='0'");
		$optionalModifiersArr = array();
		while($currRow = mysqli_fetch_assoc($optionalModifiersResult)){
			$optionalModifiersArr[$currRow['id']] = $currRow;
		}
		//error_log("menu: " . print_r($forcedModifiersArr,1));
		$returnArr = array('menu_items' => $menuItemsArr, 'ingredients' => $ingredientsArr, 'forced_modifiers' => $forcedModifiersArr, 'optional_modifiers' => $optionalModifiersArr);

		echo json_encode($returnArr);
	}

	function getLocationRow(){
		$queryStr = "SELECT * FROM `locations`";
		$result = lavu_query($queryStr);
		if(!$result){
			error_log("Could not access location row in file:".__FILE__);
		}
		return mysqli_fetch_assoc($result);
	}
