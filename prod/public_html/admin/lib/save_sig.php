<?php

session_start();

require_once(__DIR__."/info_inc.php");
require_once(__DIR__."/gateway_functions.php");
require_once(__DIR__."/mpshc_functions.php");
require_once("/home/poslavu/private_html/rs_upload.php");


$timezone = locationSetting("timezone");

$capture_amount			= (float)reqvar("pac", 0);
$check					= reqvar("check", "");
$device_time			= reqvar("dt", DateTimeForTimeZone($timezone));
$order_id				= reqvar("order_id", "");
$location_id			= reqvar("loc_id", "");
$poslavu_version		= reqvar("appV", "");
$auth_code = reqvar("auth_code", "");
if (empty($poslavu_version))
{
	$poslavu_version	= reqvar("poslavu_version", "");
}
$server_id				= reqvar("server_id", "");
$server_full_name		= reqvar("sfn", "");
$base_amount			= (float)reqvar("amt", "");
$tip_amount				= (float)reqvar("tip", 0);
$tip_only				= (reqvar("tip_only", "") == "1");
$tip_updated			= TRUE;
$transaction_id			= reqvar("t_id", "");
$record_no				= reqvar("rn", "");
$ref_data				= reqvar("rd", "");
$process_data			= reqvar("pd", "");
$gateway				= reqvar("gateway", "");

// Convert request variable names to be picked up by actionLogItGW()
$_REQUEST['app_build']		= reqvar("appB", "");
$_REQUEST['app_name']		= reqvar("appN", "");
$_REQUEST['app_version']	= $poslavu_version;

$tx_vars = array(
	"check"				=> $check,
	"loc_id"			=> $location_id,
	"order_id"			=> $order_id,
	"transaction_id"	=> $transaction_id
);

$lavu_query_should_log = TRUE;

$q_string = "";
$keys = array_keys($tx_vars);
foreach ($keys as $key)
{
	if ($q_string != "")
	{
		$q_string .= " AND ";
	}
	$q_string .= "`".$key."` = '[".$key."]'";
}

$internal_id = reqvar("iid", "");
if (!empty($internal_id))
{
	$tx_vars['internal_id'] = $internal_id;
	$q_string = "`internal_id` = '[internal_id]'";
}

$return_pnref = $transaction_id;
$last_mod_ts = time();

$start_time = microtime(TRUE);

if ((is_numeric($tip_amount) && $tip_amount>0) || (is_numeric($capture_amount) && $capture_amount>0))
{
	$db_name = "poslavu_".$data_name."_db";
	$integration_info = get_integration_from_location($location_id, $db_name);

	if (empty($gateway))
	{
		$gateway = $integration_info['gateway'];
	}

	require_once(__DIR__."/gateway_lib/Transaction_Controller.php");

	$process_info = array(
		'base_amount'		=> $base_amount,
		'capture_amount'	=> $capture_amount,
		'card_amount'		=> "",
		'card_cvn'			=> "",
		'card_number'		=> "",
		'check'				=> $check,
		'company_id'		=> $company_info['id'],
		'data_name'			=> $data_name,
		'device_time'		=> $device_time,
		'device_udid'		=> determineDeviceIdentifier($_REQUEST),
		'exp_month'			=> "",
		'exp_year'			=> "",
		'ext_data'			=> "",
		'for_deposit'		=> "",
		'gateway'			=> $gateway,
		'integration3'		=> $integration_info['integration3'],
		'integration4'		=> $integration_info['integration4'],
		'integration5'		=> $integration_info['integration5'],
		'integration6'		=> $integration_info['integration6'],
		'integration7'		=> $integration_info['integration7'],
		'integration8'		=> $integration_info['integration8'],
		'integration9'		=> $integration_info['integration9'],
		'integration10'		=> $integration_info['integration10'],
		'loc_id'			=> $location_id,
		'mag_data'			=> "",
		'more_info'			=> $tip_amount,
		'name_on_card'		=> "",
		'order_id'			=> $order_id,
		'password'			=> $integration_info['integration2'],
		'process_data'		=> $process_data,
		'pnref'				=> $transaction_id,
		'reader'			=> "",
		'record_no'			=> $record_no,
		'ref_data'			=> $ref_data,
		'register'			=> "",
		'register_name'		=> "",
		'server_full_name'	=> $server_full_name,
		'server_id'			=> $server_id,
		'server_name'		=> "",
		'set_pay_type'		=> "Card",
		'set_pay_type_id'	=> "2",
		'username'			=> $integration_info['integration1']
	);

	$transactionController = new Transaction_Controller($gateway, $process_info);

	$get_transaction = lavu_query("SELECT * FROM `cc_transactions` WHERE ".$q_string." ORDER BY `id` DESC LIMIT 1", $tx_vars);
	if (mysqli_num_rows($get_transaction) > 0)
	{
		$tx_info = mysqli_fetch_assoc($get_transaction);

		$process_info['register']		= $tx_info['register'];
		$process_info['register_name']	= $tx_info['register_name'];
		$process_info['server_name']	= $tx_info['server_name'];

		if (in_array($gateway, array( "Authorize.net", "BluePay", "CyberSource", "Elavon", "Magensa" )))
		{
			$tip_updated = TRUE;
			$update_tip_amount = lavu_query("UPDATE `cc_transactions` SET `tip_amount` = '[1]', `signature` = '1', `last_mod_ts` = '[2]' WHERE `id` = '[3]'", $tip_amount, $last_mod_ts, $tx_info['id']);
			total_card_tips($location_id, $order_id, $device_time);
		}
		else
		{
			$transtype = ($tx_info['auth'] == 1)?"PreAuthCapture":"Adjustment";

			if ($capture_amount	> 0)
			{
				$transtype = "PreAuthCaptureForTab";
				$process_info['card_amount'] = $capture_amount;

				$base_amount = $capture_amount;
			}

			// LP-3759
			$reauthorize_preauth = (reqvar("ra", "") == "1");
			if ($reauthorize_preauth)
			{
				$transtype = "ReauthorizePreAuth";
			}

			$process_info['transtype']	= $transtype;
			$process_info['tip_amount']	= $tip_amount;
			$process_info['authcode']	= $tx_info['auth_code'];

			// don't contact gateway yet for Authorize.net, BluePay, CyberSource, and Magensa transactions
			$response = "";
			switch ($gateway)
			{
				case "eConduit":

					// LP-3759
					break;

				case "EVOSnap" :

					$process_info['transtype'] = "PreAuthCapture";
					break;

				case "Heartland" :

					break;

				case "MerchantWARE" :

					break;

				case "Mercury" :

					if ($tx_info['mpshc_pid'] != "")
					{
						$process_info['username'] = $integration_info['integration3'];
						$process_info['password'] = $integration_info['integration4'];
						$response = mpshc_add_tip($transtype, $integration_info['integration3'], $integration_info['integration4'], $data_name, $location_id, $order_id, $tx_info['amount'], $tip_amount, $tx_info['transaction_id'], $tx_info['auth_code'], $tx_info['record_no'], $poslavu_version, $tx_info['register'], $server_id, $tx_info['ref_data'], $tx_info['card_type'], $tx_info['card_desc']);
					}

					break;

				case "RedFin" :

					$process_info['ext_data'] = "<TipAmt>".$tip_amount."</TipAmt>";
					break;

				case "TGate" :

					$process_info['ext_data'] = "<TipAmt>".$tip_amount."</TipAmt>";
					if (isset($tx_info['auth_code']))
					{
						$process_info['authcode'] = $tx_info['auth_code'];
					}
					break;

				case "USAePay" :

					break;

				case "PayPal" :

					break;

				case "LavuPay":
					break;

				default :

					$response = "0|Error: Unknown gateway...";
					break;
			}

			if (empty($response))
			{
				$transactionController->process_transaction($process_info, $location_info);
				$response = $transactionController->getResponse();
			}

			$response_parts = explode("|", $response);
			if ($response_parts[0] == "1")
			{
				$last_mod_ts		= time();
				$record_ref_no		= "";
				$update_amount		= "";
				$update_processed 	= "";

				if (in_array($transtype, array( "PreAuthCapture", "PreAuthCaptureForTab", "ReauthorizePreAuth" )))
				{
					$return_pnref = $response_parts[1];
					$record_ref_no = ", `transaction_id` = '".$response_parts[1]."', `record_no` = '".$response_parts[6]."'";

					if (count($response_parts) > 9)
					{
						$record_ref_no .= ", `ref_data` = '".$response_parts[9]."'";
					}

					if (count($response_parts) > 10)
					{
						$record_ref_no .= ", `process_data` = '".rawurldecode($response_parts[10])."'";
					}

					$update_amount = "`amount` = '[1]', `total_collected` = '[1]', ";

					if (!$reauthorize_preauth && $gateway=="eConduit")
					{
						$update_processed = "`processed` = '1', ";
					}
				}

				$set_auth = "0";
				if ($reauthorize_preauth)
				{
					$set_auth = "1";
				}

				if (!$tip_only)
				{
					$record_ref_no .= ", `signature` = '1'";
				}

				$update_transaction_record = lavu_query("UPDATE `cc_transactions` SET ".$update_amount.$update_processed."`tip_amount` = '[2]', `auth` = '".$set_auth."', `last_mod_ts` = '[3]'".$record_ref_no." WHERE `id` = '[4]'", $base_amount, $tip_amount, $last_mod_ts, $tx_info['id']);
				total_card_tips($location_id, $order_id, $device_time);

				$respmsg = $response_parts[3];
			}
			else
			{
				$tip_updated = FALSE;
				$respmsg = $response_parts[1];
			}
		}
	}
	else
	{
		$tip_updated = FALSE;
		$respmsg = "Unable to pull up referenced transaction info.";
	}
}
else if (!$tip_only)
{
	$tx_vars['last_mod_ts'] = $last_mod_ts;
	$update_signature_flag = lavu_query("UPDATE `cc_transactions` SET `signature` = '1', `last_mod_ts` = '[last_mod_ts]' WHERE ".$q_string." ORDER BY `id` DESC LIMIT 1", $tx_vars);
}

$echo_response = "";

if ($tip_only)
{
	if ($tip_updated)
	{
		$echo_response = "1|".$return_pnref."|".$last_mod_ts;
		$_GET['succeed'] = 'Tip Update';
	}
	else
	{
		$echo_response = "2|".$respmsg."|".$return_pnref."|".$last_mod_ts;
		$_GET['succeed'] = 'Tip Not Update';
	}
}
else if (!empty($_FILES))
{
	foreach($_FILES as $file_name => $file_array)
	{
		if (is_uploaded_file($file_array['tmp_name']))
		{
			$save_dir = "../images/".$data_name."/signatures"; // data_name inherited from info_inc

			if (!is_dir($save_dir))
			{
				$made_it = mkdir ($save_dir, 0755, TRUE);
				if (!$made_it)
				{
					error_log("failed to create ".$save_dir." ... ".error_get_last());
				}
			}
			$AWSkey = rs_move_uploaded_file($file_array['tmp_name'], $save_dir."/".basename($_FILES['signature_image']['name']), basename($_FILES['signature_image']['name']), false, false, $data_name, false);
			if($AWSkey && strlen($location_id) && strlen($order_id))
			{
				lavu_query("UPDATE " . "poslavu_" . $data_name . "_db" . ".cc_transactions SET storage_key_signature='[1]' WHERE loc_id='[2]' AND order_id='[3]' AND `check`='[4]' AND auth_code='[5]'", $AWSkey, $location_id, $order_id, $check, $auth_code);
			}

			if ($tip_updated)
			{
				$echo_response = "1|".$return_pnref."|".$last_mod_ts;
				$_GET['succeed'] = "Tip Update";
			}
			else
			{
				$tx_vars['last_mod_ts'] = $last_mod_ts;
				$update_signature_flag = lavu_query("UPDATE `cc_transactions` SET `signature` = '1', `last_mod_ts` = '[last_mod_ts]' WHERE ".$q_string." ORDER BY `id` DESC LIMIT 1", $tx_vars);

				$echo_response = "2|".$respmsg."|".$return_pnref."|".$last_mod_ts;
				$_GET['succeed'] = "Tip Not Update";
			}
		}
		else
		{
			$echo_response = "Upload failed...";
			$_GET['failed'] = "Upload Failed, Temporary File Not Found";
		}
	}
}
else
{
	$echo_response = "file not found in post ... ";
	$_GET['failed'] = "No Files Specified Via Signature Upload";
}

$end_time = microtime(TRUE);
$process_time = round((($end_time - $start_time) * 1000), 2)."ms";

echo $echo_response;

$comm_vars_log_file = log_comm_vars($data_name, $location_id, "save_sig");

if (!empty($comm_vars_log_file))
{
	$fp = fopen($comm_vars_log_file, "a");
	fwrite($fp, number_format($end_time, 1, ".", "")." - LOCATION: ".$location_id." - RESPONSE: ".$echo_response."[--CR--]Exec time (".$_REQUEST['YIG']."): ".$process_time."[--CR--][--CR--]\n");
	fclose($fp);
}

function total_card_tips($loc_id, $order_id, $device_time)
{
	global $data_name; // globalized for lavu_query logging
	global $location_info;

	$tip_total = 0;

	$check_got_response = ($location_info['integrateCC'] == "1")?" AND `got_response` = '1'":"";
	$check_transactions = lavu_query("SELECT * FROM `cc_transactions` WHERE `order_id` = '[1]' AND `loc_id` = '[2]' AND `voided` != '1'".$check_got_response." AND `pay_type_id` = '2' AND (`action` = 'Sale' OR `action` = 'Refund') AND `tip_for_id` = '0' ORDER BY `check`, `id` ASC", $order_id, $loc_id);
	if (mysqli_num_rows($check_transactions) > 0)
	{
		while ($info = mysqli_fetch_assoc($check_transactions))
		{
			$tip_amount = (float)$info['tip_amount'];

			switch ($info['action'])
			{
				case "Sale":

					$tip_total += $tip_amount;
					break;

				case "Refund":

					$tip_total -= $tip_amount;
					break;

				default:
					break;
			}
		}
	}

	$update_order = lavu_query("UPDATE `orders` SET `card_gratuity` = '[1]', `last_mod_ts` = '[2]', `pushed_ts` = '[2]', `last_modified` = '[3]', `last_mod_device` = '[4]' WHERE `location_id` = '[5]' AND `order_id` = '[6]'", (string)$tip_total, time(), $device_time, determineDeviceIdentifier($_REQUEST), $loc_id, $order_id);
}

function quit($status, $msg, $return)
{
	global $data_name;

	$_GET['status'] = $msg;
	log_comm_vars($data_name, reqvar("loc_id", ""), "save_sig");

	return $return;
}
