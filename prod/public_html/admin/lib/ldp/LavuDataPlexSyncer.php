<?php

error_reporting(E_ALL);

require_once(__DIR__."/../ldp/SyncTagAcknowledgements.php");

class LavuDataPlexSyncer
{
	private $debug;
	private $ldp_debug;
	private $req_id;

	private $ldp_settings;
	private $dataname;
	private $company_info; // for device check in only
	private $location_info; // for device check in and pending posts only
	private $location_id;
	private $device_id;
	private $device_time;

	private $order_tables_to_sync;

	private $conflicted_ioids;
	private $closure_cancellations;
	private $internal_updates;
	private $response;

	private $syncTagAcknowledgements;

	public function __construct()
	{
		global $ldp_debug;
		global $NewRelic;

		$this->debug		= FALSE;
		$this->ldp_debug 	= $ldp_debug;
		$this->req_id		= urlvar("YIG");
		$this->NewRelic 	= $NewRelic;

		$this->order_tables_to_sync = array(
			// json_key				=> table
			"order"					=> "orders",
			"contents"				=> "order_contents",
			"modifiers"				=> "modifiers_used",
			"check_details"			=> "split_check_details",
			"transactions"			=> "cc_transactions",
			"alternate_tx_totals"	=> "alternate_payment_totals"
		);

		$this->conflicted_ioids			= array();
		$this->closure_cancellations	= array();
		$this->internal_updates			= array();
		$this->response					= array();

		$this->syncTagAcknowledgements = new SyncTackAcknowledgements();
	}

	public function echoResponse()
	{
		$response_json = $this->sendResponseJson($this->response);

		$this->syncDebug("response: ".print_r($response_json, TRUE));

		echo $response_json;
	}

	public function __destruct() { }

	private function initialize($request)
	{
		global $data_name;
		global $company_info;
		global $location_info;
		global $device_time;

		$this->ldp_settings = array();
		$settings_file = __DIR__."/ldp_settings.txt";
		if (file_exists($settings_file))
		{
			$settings = file($settings_file);
			foreach ($settings as $setting)
			{
				if (strstr($setting, "="))
				{
					$key_val = explode("=", $setting);
					$key = $key_val[0];
					if (!empty($key))
					{
						$this->ldp_settings[$key] = $key_val[1];
					}
				}
			}
		}

		$this->dataname			= $data_name;
		$this->company_info		= $company_info;
		$this->location_info	= $location_info;
		$this->location_id		= $location_info['id'];
		$this->device_id		= $request['UUID'];
		$this->device_time		= $device_time;

		$company_id = $company_info['id'];
		$loc_id = $this->location_id;
		if (!empty($company_id) && !empty($loc_id))
		{
			set_sessvar("location_info_".$company_id."_".$loc_id, $location_info);
		}

		$this->response['ldp_status'] = "success";
		ConnectionHub::getConn('rest')->selectDN($this->dataname);
	}

	private function useDatanameLocation($dataname, $loc_id)
	{
		$changed = FALSE;

		if (!empty($dataname) && !empty($loc_id))
		{
			if ($dataname != $this->dataname)
			{
				$changed = TRUE;
				ConnectionHub::getConn('rest')->selectDN($dataname);
			}

			$this->dataname = $dataname;
			$this->location_id = $loc_id;

			global $data_name; // globalized for lavu_query logging
			$data_name = $this->dataname;
		}

		return $changed;
	}

	private function getLocationInfo($company_id, $loc_id)
	{
		$sess_key = "location_info_".$company_id."_".$loc_id;

		if (sessvar($sess_key))
		{
			$location_info = sessvar($sess_key);
		}
		else
		{
			$get_location_info = lavu_query("SELECT * FROM `locations` WHERE `id` = '[1]'", $loc_id, NULL, NULL, NULL, NULL, NULL, FALSE);
			$location_info = mysqli_fetch_assoc($get_location_info);
			$get_location_config = lavu_query("SELECT `setting`, `value` FROM `config` WHERE `type` = 'location_config_setting' AND `location` = '[1]'", $loc_id, NULL, NULL, NULL, NULL, NULL, FALSE);

			while ($config_info = mysqli_fetch_assoc($get_location_config))
			{
				$location_info[$config_info['setting']] = $config_info['value'];
			}
			set_sessvar($sess_key, $location_info);
		}

		return $location_info;
	}

	/**
	 * Process data plex elements as they are encountered in request
	 *
	 * @param request Array of HTTP Request vars
	 */
	public function process($request)
	{
		$this->initialize($request);

		if (!empty($this->dataname)) {
			$this->NewRelic->track("dataname", $this->dataname);
		}

		if (isset($request['device_check_in']))
		{
			$this->processDeviceCheckIn($request['device_check_in']);
		}

		// no further need for a connection to MAIN
		mlavu_close_db();

		if (isset($request['reconcile_86_counts']))
		{
			$this->processEightySixCountCheck($request['reconcile_86_counts']);
		}

		if (isset($request['lavu_to_go']))
		{
			$this->checkLavuToGoOrders();
		}

		if (isset($request['auto_send']))
		{
			if (!isset($this->response['lavu_to_go_result']))
			{
				$this->response['lavu_to_go_result'] = array( 'status' => "success" );
			}

			$auto_send_order = pullNextOrderForAutoSend($this->location_id, $this->device_id);
			if (is_array($auto_send_order))
			{
				$this->response['lavu_to_go_result']['auto_send_order'] = $auto_send_order;
			}
		}

		if (isset($request['pull']))
		{
			$this->pullOrders($request['pull']);
		}

		if (isset($request['push']))
		{
			$push_data = json_decode($request['push'], TRUE);

			$this->syncTagAcknowledgements->initialize($push_data);
			$this->syncTagAcknowledgements->check($this);

			$this->processOrderAndLogPush($push_data);
		}

		$this->checkForOrderOwnershipRequests();

		$minimum_interval = (isset($this->ldp_settings['minimum_interval']))?(int)$this->ldp_settings['minimum_interval']:5;
		if ($minimum_interval > 5)
		{
			$this->response['minimum_interval'] = (string)$minimum_interval;
		}

		$this->echoResponse();
	}

	/**
	 * Process device check in
	 *
	 * @param device_check_in_json String of JSON data to be decoded and processed
	 */
	private function processDeviceCheckIn($device_check_in_json)
	{
		global $data_name;
		global $company_info;
		global $location_info;
		global $lfKickout;
		global $lfMessage;
		global $lfTitle;

		$device_check_in = json_decode($device_check_in_json);

		$dci_response = array(
			'dci_status'		=> "success",
			'needs_reload'		=> "0",
			'server_version'	=> currentLavuBackendVersion()
		);

		$lfKickout = 0;

		// bulk of device check in operations are only included when the user is in diningRoom, order, orderList,
		// manageTips, or managementScreen or 5 minutes has elapsed since the last full device check in

		if (isset($device_check_in->device_name))
		{
			$server_id	= $device_check_in->server_id;
			$net_path	= $location_info['net_path'];
			$lls		= (in_array($location_info['use_net_path'], array("1", "2")) && !strstr($net_path, "lavu"));
			$remote_ip	= (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];

			require_once(__DIR__."/../deviceValidations.php");

			$UUID =			reqvar("UUID");
			$app_name =		reqvar("app_name", "");
			$app_version =	reqvar("app_version", "");
			$app_build =	reqvar("app_build", "");

			$dvc = array(
				"check_in_engine"	=> "dataplex",
				"company_id"		=> $company_info['id'],
				"data_name"			=> $this->dataname,
				"loc_id"			=> reqvar("loc_id"),
				"UUID"				=> $UUID,
				"device_name"		=> $device_check_in->device_name,
				"device_model"		=> $device_check_in->device_model,
				"specific_model"	=> $device_check_in->specific_model,
				"app_name"			=> $app_name,
				"poslavu_version"	=> $app_version." (".$app_build.")",
				"system_name"		=> $device_check_in->system_name,
				"system_version"	=> $device_check_in->system_version,
				"remote_ip"			=> $remote_ip,
				"local_ip"			=> $device_check_in->local_ip,
				"usage_type"		=> $device_check_in->usage_type,
				"device_time"		=> $this->device_time,
				"lavu_admin"		=> reqvar("lavu_admin"),
				"reseller_admin"	=> isResellerAdminDevice($UUID)?"1":"0",
				"SSID"				=> $device_check_in->SSID,
				"last_checkin_ts"	=> time()
			);

			// check for existing device record and create one if necessary; log any relevant changes to Lavu version, iOS, etc...

			$check_device_record = lavu_query("SELECT `id`, `loc_id`, `prefix`, `system_version`, `poslavu_version`, `app_name`, `usage_type`, `needs_reload`, `inactive`, `first_checkin`, `holding_order_id` FROM `devices` WHERE `UUID` = '[1]'", $dvc['UUID']);
			$matches = mysqli_num_rows($check_device_record);
			if ($matches > 0)
			{
				$this_device = mysqli_fetch_assoc($check_device_record);
				$dvc['id']					= $this_device['id'];
				$dvc['prefix']				= $this_device['prefix'];
				$dvc['holding_order_id']	= $this_device['holding_order_id'];

				$admin_action_log_insert = "INSERT INTO `admin_action_log` (`action`, `server_time`, `time`, `user_id`, `ipaddress`, `data`, `detail1`, `detail2`, `detail3`, `loc_id`) VALUES ('[aal_action]', now(), '[device_time]', '0', '[remote_ip]', '[UUID]', '[device_name]', '[detail2]', '[detail3]', '[loc_id]')";

				if ($dvc['system_version'] != $this_device['system_version'])
				{
					$dvc['aal_action']	= "iOS version changed";
					$dvc['detail2']		= "from ".$this_device['system_version'];
					$dvc['detail3']		= "to ".$dvc['system_version'];
					lavu_query($admin_action_log_insert, $dvc);
				}

				if ($dvc['app_name'] == $this_device['app_name'])
				{
					if ($dvc['poslavu_version'] != $this_device['poslavu_version'])
					{
						$dvc['aal_action']	= $dvc['app_name']." version changed";
						$dvc['detail2']		= "from ".$this_device['poslavu_version'];
						$dvc['detail3']		= "to ".$dvc['poslavu_version'];
						lavu_query($admin_action_log_insert, $dvc);
					}

					if ($dvc['usage_type'] != $this_device['usage_type'])
					{
						$dvc['aal_action']	= "Usage type changed";
						$dvc['detail2']		= "from ".$this_device['usage_type'];
						$dvc['detail3']		= "to ".$dvc['usage_type'];
						lavu_query($admin_action_log_insert, $dvc);
					}
				}
				else
				{
					$dvc['aal_action']	= "Switched apps";
					$dvc['detail2']		= "from ".$this_device['app_name'];
					$dvc['detail3']		= "to ".$dvc['app_name'];
					lavu_query($admin_action_log_insert, $dvc);
				}

				if ($dvc['loc_id'] != $this_device['loc_id'])
				{
					$dvc['holding_order_id'] = "";

					$dvc['aal_action'] = "Switched locations";
					$dvc['detail2'] = "from ".getFieldInfo($this_device['loc_id'], "id", "locations", "title");
					$dvc['detail3'] = "to ".$location_info['title'];
					lavu_query($admin_action_log_insert, $dvc);
				}
				else if ($this_device['inactive'] == "1")
				{
					$dvc['holding_order_id'] = "";
				}

				if ($matches > 1)
				{
					lavu_query("DELETE FROM `devices` WHERE `UUID` = '[1]' AND `id` != '[2]'", $dvc['UUID'], $this_device['id']);
				}

				$dvc['one_month_ago'] = date("Y-m-d H:i:s", (time() - 2419200));
				$check_for_prefix_duplicates = lavu_query("SELECT `id` FROM `devices` WHERE `loc_id` = '[loc_id]' AND `id` != '[id]' AND `prefix` = '[prefix]' AND `last_checkin` >= '[one_month_ago]'", $dvc);
				if (mysqli_num_rows($check_for_prefix_duplicates) > 0)
				{
					$new_prefix = getLowestPrefix($dvc['loc_id']);
					if (!empty($new_prefix))
					{
						$dvc['prefix'] = $new_prefix;
					}
				}

				$fill_in_first_checkin = (empty($this_device['first_checkin']))?", `first_checkin` = '[device_time]'":"";

				$update_record = lavu_query("UPDATE `devices` SET `check_in_engine` = '[check_in_engine]', `company_id` = '[company_id]', `data_name` = '[data_name]', `loc_id` = '[loc_id]', `prefix` = '[prefix]', `name` = '[device_name]', `model` = '[device_model]', `specific_model` = '[specific_model]', `system_name` = '[system_name]', `system_version` = '[system_version]', `UDID` = '-', `poslavu_version` = '[poslavu_version]', `last_checkin` = now(), `last_checkin_ts` = '[last_checkin_ts]', `device_time` = '[device_time]', `app_name` = '[app_name]', `ip_address` = '[local_ip]', `UUID` = '[UUID]', `lavu_admin` = '[lavu_admin]', `inactive` = '0', `remote_ip` = '[remote_ip]', `SSID` = '[SSID]', `usage_type` = '[usage_type]', `reseller_admin` = '[reseller_admin]'".$fill_in_first_checkin." WHERE `id` = '[id]'", $dvc);

				$dci_response['needs_reload'] = $lls?"0":$this_device['needs_reload'];
			}
			else
			{
				$dvc['prefix'] = getLowestPrefix($dvc['loc_id']);
				lavu_query("INSERT INTO `devices` (`check_in_engine`, `company_id`, `data_name`, `loc_id`, `name`, `model`, `specific_model`, `system_name`, `system_version`, `UDID`, `poslavu_version`, `last_checkin`, `last_checkin_ts`, `device_time`, `app_name`, `ip_address`, `prefix`, `UUID`, `lavu_admin`, `inactive`, `remote_ip`, `SSID`, `usage_type`, `reseller_admin`, `first_checkin`) VALUES ('[check_in_engine]', [company_id]', '[data_name]', '[loc_id]', '[device_name]', '[device_model]', '[specific_model]', '[system_name]', '[system_version]', '-', '[poslavu_version]', now(), '[last_checkin_ts]', '[device_time]', '[app_name]', '[local_ip]', '[prefix]', '[UUID]', '[lavu_admin]', '0', '[remote_ip]', '[SSID]', '[usage_type]', '[reseller_admin]', '[device_time]')", $dvc);
			}

			$dci_response['device_prefix'] = $dvc['prefix'];

			// set devices as inactive if they haven't checked in for 7 days
			lavu_query("UPDATE `devices` SET `inactive` = '1', `holding_order_id` = '' WHERE `last_checkin` < '[1]'", date("Y-m-d H:i:s", (time() - 604800))); // 7 days

			// pull updated list of all location devices
			$get_devices_info = lavu_query("SELECT `UUID`, `model`, `name`, `ip_address`, `prefix`, `app_name`, `last_checkin`, `poslavu_version` FROM `devices` WHERE `loc_id` = '[1]' AND `inactive` = '0'", $dvc['loc_id']);
			if (mysqli_num_rows($get_devices_info) > 0)
			{
				$devices_info = array();
				while ($di = mysqli_fetch_object($get_devices_info))
				{
					$devices_info[$di->UUID] = array($di->model, $di->name, $di->ip_address, $di->app_name, $di->last_checkin, $di->poslavu_version);
				}
				$dci_response['devices_info'] = $devices_info;
			}

			// check central database for expected next order id given the device's assigned prefix
			$server_prefix = $lls?"8":"1";
			$full_prefix = $server_prefix.str_pad($dci_response['device_prefix'], 3, "0", STR_PAD_LEFT)."-";
			$dci_response['next_order_id'] = str_replace($full_prefix, "", assignNextWithPrefix("order_id", "orders", "location_id", $dvc['loc_id'], $full_prefix));

			// check LLS main timestamp to be included in About section in app
			if ($lls && file_exists("lls-main-timestamp"))
			{
				$lines = file("lls-main-timestamp");
				$dci_response['lls_main_timestamp'] = "LLS ".str_replace(array("-", ":", " "), array("", "", "-"), $lines[0]);
			}

			// report activity to central MAIN database
			$LCU = sessvar("LaCeUp"); // last central update
			if ((empty($LCU) || (time() > ((int)$LCU + 3600))) && !$lls)
			{
				set_sessvar("LaCeUp", time());
				mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now(), `app_name` = '[1]', `version_number` = '[2]', `build_number` = '[3]' WHERE `id` = '[4]'", $app_name, $app_version, $app_build, $dvc['company_id']);
				register_connect("jc_inc", $dvc['company_id'], $dvc['data_name'], $dvc['loc_id'], $dvc['device_model'], $dvc['UUID']);
			}

			// check disabled status of company account
			if (!$lls)
			{
				$check_disabled_flag = mlavu_query("SELECT `disabled` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[company_id]' AND `disabled` != '0'", $dvc);
				if (mysqli_num_rows($check_disabled_flag) > 0)
				{
					$response = array();
					$lfKickout = 2;
					$dci_response['dci_status']		= "LocationFlag";
					$dci_response['flag_title']		= "Account Inactive";
					$dci_response['flag_message']	= "Please contact support.";
					$dci_response['flag_kickout']	= "2";
				}
			}

			// no further need for a connection to MAIN
			mlavu_close_db();

			// check for device usage compliance
			if ($dci_response['dci_status']=="success" && $dvc['lavu_admin']!="1" && $dvc['reseller_admin']!="1" && (rand(0, 99) % 3)==0)
			{
				$lfMessage = "";
				$lfTitle = "";

				if (!isLicenseCompliant($data_name, $dvc['device_model'], $dvc['usage_type']))
				{
					if ($lfKickout > 0)
					{
						$dci_response = array( "flag_kickout" => $lfKickout );
					}
					$dci_response['dci_status']		= "LocationFlag";
					$dci_response['flag_message']	= $lfMessage ;
					$dci_response['flag_title']		= $lfTitle;
				}
			}

			// check to see if the account is properly paid for
			if ($dci_response['dci_status'] == "success")
			{
				$pay_stat = checkPaymentStatus();
				if (!empty($pay_stat['message']))
				{
					if ($pay_stat['kickout'])
					{
						$dci_response = array();
						$lfKickout = 1;
						$dci_response['flag_kickout'] = "1";
					}
					$dci_response['dci_status']		= "LocationFlag";
					$dci_response['flag_message']	= $pay_stat['message'];
					$dci_response['flag_title']		= $pay_stat['title'];
				}
			}

			// check for device targeted messages
			if ($dci_response['dci_status'] == "success")
			{
				$deliver_messages = array();
				$check_device_messages = lavu_query("SELECT `id`, `ts`, `title`, `message`, `udid`, `read_by` FROM `device_messages` WHERE `read` = ''");
				// limit by time since creation?
				if (mysqli_num_rows($check_device_messages) > 0)
				{
					$ts = time();
					$dt = date("Y-m-d H:i:s", $ts);
					$read_by_id = $server_id;
					$got_user_count = 0;
					while ($info = mysqli_fetch_assoc($check_device_messages))
					{
						$update_it = "";
						if ($info['udid'] == $dvc['UUID'])
						{
							$deliver_messages[] = $info;
							$update_it = " `read` = '".$dt."', `read_ts` = '".$ts."', `read_by` = '|".$read_by_id."|'";
						}
						else if ($info['udid'] == "all_servers")
						{
							if (!strstr($info['read_by'], "|".$read_by_id."|"))
							{
								$update_it = " `read_ts` = '".$ts."', `read_by` = '".$info['read_by']."|".$read_by_id."|'";
								$deliver_messages[] = $info;
							}

							if ((time() - $info['ts']) > 345600)
							{
								if ($update_it != "")
								{
									$update_it .= ", ";
								}
								$update_it .= " `read` = '".$dt."'";
							}
						}

						if ($update_it != "")
						{
							$mark_read = lavu_query("UPDATE `device_messages` SET".$update_it." WHERE `id` = '[1]'", $info['id']);
						}
					}
				}

				$msg_count = count($deliver_messages);
				if ($msg_count > 0)
				{
					$title = "Alert Messages";
					$message = "";
					foreach ($deliver_messages as $msg)
					{
						if ($message != "")
						{
							$message .= "\n\n\n";
						}

						if ($msg_count == 1)
						{
							$title = $msg['title'];
						}
						else
						{
							$message .= strtoupper($msg['title'])."\n\n";
						}

						$message .= $msg['message'];
					}
					$dci_response['dci_status']		= "LocationFlag";
					$dci_response['flag_title']		= $title;
					$dci_response['flag_message']	= str_replace(array(chr(13), chr(10), "\n"), array("", "[--CR--]", "[--CR--]"), $message);
					$dci_response['flag_kickout']	= "0";
				}
			}

			// check for server cash data for the given date
			if ($lfKickout == 0)
			{
				$date_info = get_use_date_end_date($this->location_info, $this->device_time);
				$check_todays_cash_tips = lavu_query("SELECT `id`, `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $dvc['loc_id'], $server_id, $date_info[0]);
				if (mysqli_num_rows($check_todays_cash_tips) > 0)
				{
					$info = mysqli_fetch_assoc($check_todays_cash_tips);
					$dci_response['cash_tips'] = $info['value'];
				}
			}
		}

		// check for clocked in users - this is meant to always be included in a device check in request, such as while the app is resting on the PIN screen
		if ($lfKickout == 0)
		{
			$check_clocked_in_users = lavu_query("SELECT `id` FROM `users` WHERE `clocked_in` = '1'");
			if (mysqli_num_rows($check_clocked_in_users) > 0)
			{
				$clocked_in_users = array();
				while ($info = mysqli_fetch_assoc($check_clocked_in_users))
				{
					$clocked_in_users[] = $info['id'];
				}

				$dci_response['clocked_in_servers'] = $clocked_in_users;
			}
		}

		$this->response['device_check_in_result'] = $dci_response;
	}

	/**
	 * Process 86 count differences and return updated counts
	 *
	 * @param eighty_six_diffs_json String of JSON data to be decoded and processed
	 */
	private function processEightySixCountCheck($eighty_six_diffs_json)
	{
		$eighty_six_diffs = json_decode($eighty_six_diffs_json, TRUE);

		$counts = array();

		$check_menu_items = lavu_query("SELECT `id`, `track_86_count`, `inv_count` FROM `menu_items` WHERE `track_86_count` != '' AND `_deleted` != '1'");
		while ($info = mysqli_fetch_assoc($check_menu_items))
		{
			if ($info['track_86_count'] == "86count")
			{
				$iid = $info['id'];
				$count = $info['inv_count'];
				if (isset($eighty_six_diffs[$iid]))
				{
					$d = (int)$eighty_six_diffs[$iid];
					if ($d != 0)
					{
						$count -= $d;
						$update_item = lavu_query("UPDATE `menu_items` SET `inv_count` = '[1]' WHERE `id` = '[2]'", $count, $iid);
					}
				}
				$counts[$iid] = $count;
			}
		}

		$this->response['reconcile_86_counts_result']['counts'] = $counts;
	}

	public function checkLavuToGoOrders()
	{
		global $data_name;
		global $location_info;

		$criteria = array(
			"loc_id"		=> $this->location_id,
			"device_time"	=> determineDeviceTime($location_info, $_REQUEST),
			"submode"		=> "open",
			"search_type"	=> "",
			"search_string"	=> "",
			"for_server_id"	=> "all",
			"page"			=> "0",
			"ltg"			=> "1",
			"upcoming_ltg"	=> "1",
			"showing_ioids"	=> "",
			"regular_pull"	=> FALSE
		);

		$ltg_result = loadOrderList($criteria);
		unset($ltg_result['PHPSESSID']);

		$ltg_result['status'] = $ltg_result['json_status'];
		unset($ltg_result['json_status']);

		$this->response['lavu_to_go_result'] = $ltg_result;
	}

	public function pullOrders($pull_json)
	{
		global $cc_companyid;
		global $data_name;
		global $location_info;
		global $loc_id;

		$pull_data = json_decode($pull_json, TRUE);

		$loc_id				= $this->location_id;
		$max_orders			= (int)(isset($pull_data['max_orders']))?$pull_data['max_orders']:5;
		$pull_ioids			= (!empty($pull_data['pull_ioids']))?explode(",", $pull_data['pull_ioids']):array();
		$pull_open_orders	= (int)(isset($pull_data['pull_open_orders']))?$pull_data['pull_open_orders']:"0";
		$last_pull_ts		= $pull_data['last_pull_ts'];
		$uuid				= reqvar("UUID", "");

		$pull_start_time = microtime(TRUE);

		$this->syncDebug("pull start - max orders: ".$max_orders." - pull_open_orders: ".$pull_open_orders." - last pull ts: ".$last_pull_ts);
		if (count($pull_ioids) > 0)
		{
			 $this->syncDebug("pull_ioids: ".json_encode($pull_ioids));
		}

		$pull_response = pullOrders($loc_id, $max_orders, $pull_ioids, $pull_open_orders, $last_pull_ts, $uuid);

		$pull_result = array( 'ts' => (string)time() );

		$order_data_count = 0;
		$compare_ioids_count = 0;

		$pull_result['status'] = (isset($pull_response['json_status']))?$pull_response['json_status']:"";
		if ($pull_result['status'] == "success")
		{
			$order_data = (isset($pull_response['order_data']))?$pull_response['order_data']:array();
			$compare_ioids = (isset($pull_response['compare_ioids']))?$pull_response['compare_ioids']:array();

			$order_data_count = count($order_data);
			$compare_ioids_count = count($compare_ioids);

			// compare this pull against any incoming pushes for any of the included ioids
			// filter out ioids that generate a merge conflict and move to portion of reponse to feed in-app merge resolution tool

			if ($order_data_count > 0)
			{
				$pull_result['order_data'] = $order_data;
			}

			if ($compare_ioids_count > 0)
			{
				$pull_result['compare_ioids'] = $compare_ioids;
			}
		}

		$this->response['pull_result'] = $pull_result;

		$pull_end_time = microtime(TRUE);
		$pull_process_time = round((($pull_end_time - $pull_start_time) * 1000), 2) . "ms";

		$this->syncDebug("pull end - order_data_count: ".$order_data_count." - compare_ioids_count: ".$compare_ioids_count." - duration: ".$pull_process_time);
	}

	/**
	 * Process order updates, pending posts, and log entries being pushed by the app
	 *
	 * @param push_data Associative Array to be processed
	 */
	public function processOrderAndLogPush($push_data)
	{
		global $post_vars;
		global $data_name;
		global $company_id;
		global $hold_cc;
		global $location_info;
		global $decimal_places;
		global $smallest_money;
		global $device_time;
		global $data_syncer;

		$push_result = new stdClass();

		if (count($this->syncTagAcknowledgements->responses) > 0)
		{
			$push_result->sync_tag_ack_responses = $this->syncTagAcknowledgements->responses;
		}

		$failed_ioids = array();
		$this->conflicted_ioids = reqvar("conflicted_ioids", array());

		// check for and handle incoming order updates
		if (isset($push_data['orders']))
		{
			// sync_sequence defines the order in which the various tables should be synced. The most important
			// consideration is that the order record should be updated last to help eliminate the possibility,
			// be it ever so minute, that a device looking for updates will determine it needs to pull an order
			// before all portions of the order have been updated.
			$sync_sequence = array(
				"contents",
				"modifiers",
				"transactions",
				"alternate_tx_totals",
				"check_details",
				"order"
			);

			$orders_result = array();

			foreach ($push_data['orders'] as $ioid => $order_data)
			{
				$order = $this->fixOrderStatus($order_data['order']);

				$order_id		= $order['order_id'];
				$check_count	= (int)$order['no_of_checks'];

				$this->order_id = $order_id;

				$this->checkCodedLocation($order['location'], $order['location_id']);

				$push_start_time = microtime(TRUE);

				$this->syncDebug("order push start - order_id: ".$order_id." - ioid: ".$ioid);

				if ($this->checkForSyncConflict($order_data))
				{
					continue;
				}

				// Go over our set list of tables according to the sync_sequence to sync and check for rows.
				foreach ($sync_sequence as $json_key)
				{
					$table_name = $this->order_tables_to_sync[$json_key];

					// Only process tables that have rows to sync, though `modifiers_used` needs to be allowed
					// to continue in case all modifiers have been removed from an order.
					if (empty($order_data[$json_key]) && $table_name!="modifiers_used")
					{
						continue;
					}

					// Place the associative array for 'order' inside of an array to make it match how the
					// rest of the tables get stored so we can iterate over it the same way in the next loop.
					if ($json_key == "order")
					{
						$order_data[$json_key] = array($order);
					}

					$should_delete_existing_rows = (!in_array($table_name, array( "orders", "order_contents", "cc_transactions" )));
					// This check makes sure we only run the deletes once.  And if there are no rows
					// to delete then our attempt will just whiff harmlessly in the air - but we still
					// save some time/effort by skipping the check for whether there are pre-existing
					// rows to delete. Lastly, this check was put inside of the inner-most loop so we
					// only do the delete when there are rows to insert.
					if ($should_delete_existing_rows)
					{
						$this->deletePreexistingOrderRows($table_name, $ioid, $check_count);
					}

					$error_encountered = FALSE;
					foreach ($order_data[$json_key] as $key => $row)
					{
						switch ($table_name)
						{
							case "alternate_payment_totals":

								foreach ($row as $ptid => $details)
								{
									$details['loc_id']		= $this->location_id;
									$details['order_id']	= $order_id;
									$details['check']		= $key;
									$details['pay_type_id']	= $ptid;
									$details['ioid']		= $ioid;

									if (!$this->insertOrUpdateRow($table_name, $details, $ioid))
									{
										$error_encountered = TRUE;
									}
								}

								break;

							case "split_check_details":

								$check_number = (int)$row['check'];
								if ($check_number > $check_count)
								{
									// left over check detail record
									break;
								}

							default:

								if (!$this->insertOrUpdateRow($table_name, $row, $ioid))
								{
									$error_encountered = TRUE;
								}

								break;
						}
					}
				}

				if ($error_encountered)
				{
					$failed_ioids[] = $ioid;
				}
				else
				{
					$orders_result[$ioid] = "success";
				}

				$push_end_time = microtime(TRUE);
				$push_process_time = round((($push_end_time - $push_start_time) * 1000), 2) . "ms";

				$this->syncDebug("order push end - order_id: ".$order_id." - ioid: ".$ioid." - succeeded: ".($error_encountered?"no":"yes")." - duration: ".$push_process_time);
			}

			if (count($orders_result) > 0)
			{
				if (count($this->internal_updates) > 0)
				{
					$orders_result['internal_updates'] = $this->internal_updates;
				}
				$push_result->orders_result = $orders_result;
			}
		}

		// check for and handle incoming pending posts
		if (isset($push_data['pending_posts']))
		{
			$prepared_globals = FALSE;
			$processed_ppids = array();
			$pending_posts_result = array();

			foreach ($push_data['pending_posts'] as $ioid => $pending_posts)
			{
				if (!in_array($ioid, $failed_ioids))
				{
					foreach ($pending_posts as $pending_post)
					{
						$post_id = $pending_post['id'];
						$post_string = $pending_post['post_string'];

						if (!empty($ioid) && !empty($post_id) && !empty($post_string))
						{
							$success = FALSE;

							if (!in_array($post_id, $processed_ppids))
							{
								$processed_ppids[] = $post_id;

								$device_time = reqvar("device_time", FALSE);

								$post_vars = array();
								$post_parts = explode("&", $post_string);
								foreach ($post_parts as $pstprt)
								{
									$eqpos = strpos($pstprt, "=");
									$key = substr($pstprt, 0, $eqpos);
									$value = rawurldecode(substr($pstprt, ($eqpos + 1)));
									if ($key=="device_time" && $device_time)
									{
										$value = $device_time;
									}
									$post_vars[$key] = $value;
								}

								if ((in_array($ioid, $failed_ioids) || in_array($ioid, $this->conflicted_ioids)) && $post_vars['m']=="close_order")
								{
									$pending_posts_result[$ioid][(string)$post_id] = (in_array($ioid, $this->closure_cancellations))?"cancelled":"delayed";
									continue;
								}

								$dn_changed = $this->useDatanameLocation($post_vars['dataname'], $post_vars['loc_id']);
								if ($dn_changed && $prepared_globals)
								{
									$prepared_globals = FALSE;
								}

								if (!$prepared_globals)
								{
									$data_name = $this->dataname;
									$company_id = $post_vars["company_id"];
									$hold_cc = $post_vars["cc"];
									$location_info = $this->getLocationInfo($company_id, $post_vars['loc_id']);
									$this->location_info = $location_info;
									$this->location_id = $location_info['id'];
									$decimal_places = $location_info['disable_decimal'];
									$smallest_money = (1 / pow(10, $decimal_places));
									$prepared_globals = TRUE;
								}

								$device_time = determineDeviceTime($location_info, $post_vars);
								$data_syncer = $this;

								$pp_mode = $post_vars['m'];
								$pp_start_time = microtime(TRUE);

								$this->syncDebug("pending post start - ".$pp_mode." (".$post_id.") - ioid: ".$ioid);

								switch ($pp_mode)
								{
									case "close_order":

										$success = (closeOrder($post_vars) == "1");
										break;

									case "email_receipt":

										require_once(__DIR__."/../email_func.php");
										$success = (substr(sendEmailReceipt($post_vars), 0, 2) == "1|");
										break;

									default:
										break;
								}

								$pp_end_time = microtime(TRUE);
								$pp_process_time = round((($pp_end_time - $pp_start_time) * 1000), 2) . "ms";

								$this->syncDebug("pending post end - ".$pp_mode." (".$post_id.") - ioid: ".$ioid." - succeeded: ".($success?"yes":"no")." - duration: ".$pp_process_time);

								if ($success)
								{
									if (!isset($pending_posts_result[$ioid]))
									{
										$pending_posts_result[$ioid] = array();
									}
									$pending_posts_result[$ioid][(string)$post_id] = "success";
								}
							}
						}
					}
				}
			}

			if (count($pending_posts_result) > 0)
			{
				$push_result->pending_posts_result = $pending_posts_result;
			}
		}

		// check for and handle incoming action_log entries
		if (isset($push_data['action_log']))
		{
			$action_log = $push_data['action_log'];

			if (count($action_log) > 0)
			{
				$action_log_result = array();

				foreach ($action_log as $entry)
				{
					$action = $entry['action'];
					$ialid = $entry['ialid'];
					if (in_array($entry['ioid'], $this->conflicted_ioids) && in_array($action, array("Order Closed", "Order Reclosed")))
					{
						$action_log_result[$ialid] = ($action=="Order Closed" && in_array($ioid, $this->closure_cancellations))?"cancelled":"delayed";
						continue;
					}

					$this->useDatanameLocation($entry['dataname'], $entry['loc_id']);
					if ($this->insertOrUpdateRow("action_log", $entry, $ialid, TRUE))
					{
						$success = TRUE;
						if (in_array($action, array("Paid In", "Paid Out")))
						{
							$tx = $entry['tx'];
							$success = ($this->insertOrUpdateRow('cc_transactions', $tx, $tx['internal_id'], TRUE));
							if ($success)
							{
								$this->updatePaidInPaidOutPlaceHolder($action);
							}
						}

						if ($success)
						{
							$action_log_result[$ialid] = "success";
						}
					}
				}

				if (count($action_log_result) > 0)
				{
					$push_result->action_log_result = $action_log_result;
				}
			}
		}

		// check for and handle incoming send log entries
		if (isset($push_data['send_log']))
		{
			$send_log = $push_data['send_log'];

			if (count($send_log) > 0)
			{
				$send_log_result = array();

				foreach ($send_log as $entry)
				{
					$this->useDatanameLocation($entry['dataname'], $entry['loc_id']);
					$islid = $entry['islid'];
					if ($this->insertOrUpdateRow("send_log", $entry, $islid))
					{
						$send_log_result[$islid] = "success";
					}
				}

				if (count($send_log_result) > 0)
				{
					$push_result->send_log_result = $send_log_result;
				}
			}
		}

		$this->response['push_result'] = $push_result;
	}

	/**
	 * Check for the coded location identifier so the dataname and location id properties can be properly
	 * updated and the proper database connection can be established to perform syncing for a particular
	 * record.
	 *
	 * @param coded_location String with the format "{[company_id]:[dataname]:[location_id]}"
	 * @param location_id Integer value to specific the location if not found in coded_location
	 */
	public function checkCodedLocation($coded_location, $location_id)
	{
		if (substr($coded_location, 0, 1) != "{")
		{
			return;
		}

		$cdl = explode(":", trim($coded_location, "{}"));
		if (count($cdl) < 2)
		{
			return;
		}

		$use_dn = $cdl[1];
		$use_lid = (!empty($cdl[2]))?$cdl[2]:$location_id;
		if (empty($use_lid))
		{
			$use_lid = $this->location_id;
		}
		$this->useDatanameLocation($use_dn, $use_lid);
	}

	/**
	 * The order_status field was introduced and implemented in Lavu POS 3.1.4 and is meant to allow the replacement
	 * of complicated order list queries with simpler ones that won't require UNIONs to take advantage of indexing.
	 * This function is provided to help ensure the order_status field is properly set for versions of the app
	 * prior to version 3.1.4
	 *
	 * @param order_stub Associative array containing order data directly relating to a row in the orders table
	 */
	private function fixOrderStatus($order_stub)
	{
		$app_version = reqvar("app_version", "");
		$acvc = appVersionCompareValue($app_version);

		$original_order_status = arrayValueForKey($order_stub, "order_status");

		if (($acvc >= 30104) && !empty($original_order_status))
		{ // no need for correction
			return $order_stub;
		}

		$fixed_order_status = determineOrderStatusForOrder($order_stub);

		$order_stub['order_status'] = $fixed_order_status;

		return $order_stub;
	}

	/**
	 * Check for potential sync conflicts and seek automatic resolution
	 * or report as conflicted and unresolved
	 *
	 * @param dvc_order_data Array of combined order data being pushed up by app
	 */
	private function checkForSyncConflict(&$dvc_order_data)
	{
		global $location_info;

		$order = $dvc_order_data['order'];
		$ioid = $order['ioid'];

		$check_order_stub = lavu_query("SELECT `order_id`, `total`, `sync_tag`, `last_mod_ts`, `last_mod_device`, `closed` FROM `orders` WHERE `ioid` = '[1]'", $ioid);
		if (mysqli_num_rows($check_order_stub) > 0)
		{
			$db_order = mysqli_fetch_assoc($check_order_stub);

			$db_sync_tag		= (int)$db_order['sync_tag'];
			$db_last_mod_ts		= $db_order['last_mod_ts'];
			$db_last_mod_device	= $db_order['last_mod_device'];
			$db_closed			= $db_order['closed'];

			$order_sync_tag			= (int)$order['sync_tag'];
			$order_last_mod_ts		= $order['last_mod_ts'];
			$order_last_mod_device	= $order['last_mod_device'];
			$order_closed			= $order['closed'];

			if (locationSetting("lds_conflict_detection_check_same_device") == "1")
			{
				$current_device = reqvar("UUID", "");

				$this->syncDebug("checking device ids for ioid ".$ioid." - current_device: ".$current_device." - db_last_mod_device: ".$db_last_mod_device." - order_last_mod_device: ".$order_last_mod_device);

				if (($db_last_mod_device == $current_device) && ($order_last_mod_device == $current_device))
				{
					// all device ids match so the order coming from the app can be accepted
					return FALSE;
				}
			}

			if ($db_sync_tag > $order_sync_tag)
			{
				$this->syncConflictDetected($dvc_order_data, $db_order, "sync_tag out of sequence");
				return TRUE;
			}

			if ($db_last_mod_ts > $order_last_mod_ts)
			{
				$this->syncConflictDetected($dvc_order_data, $db_order, "modified out of sequence");
				return TRUE;
			}

			if (empty($order_closed) || $order_closed=="0000-00-00 00:00:00")
			{
				if (!empty($db_closed) && $db_closed!="0000-00-00 00:00:00")
				{
					$this->syncConflictDetected($order_data, $db_order, "update would nullify previous close");
					return TRUE;
				}
			}
			else if ($order_closed>$db_closed && !empty($db_closed) && $db_closed!="0000-00-00 00:00:00")
			{
				$this->syncConflictDetected($order_data, $db_order, "update would replace previous close");
				return TRUE;
			}
		}

		return FALSE;

		/*$is_conflicted = FALSE; // original data plex conflict detection and resolution strategy

		if (isset($this->response['pull_result'])) {

			$order = $dvc_order_data['order'];
			$ioid = $order['ioid'];

			$pull_result = $this->response['pull_result'];
			$pr_order_data = (isset($pull_result['order_data']))?$pull_result['order_data']:array();
			$pr_compare_ioids = (isset($pull_result['compare_ioids']))?$pull_result['compare_ioids']:array();

			if (in_array($ioid, array_keys($pr_order_data))) {

				$db_order_data = $pr_order_data[$ioid];

				if (!$this->autoMerge($dvc_order_data, $db_order_data, "pulling")) {
					$is_conflicted = TRUE;
					$this->registerConflictedOrder($ioid, $db_order_data);
				}

			} else if (in_array($ioid, array_keys($pr_compare_ioids))) {

				$db_order_data = loadOrderByIOID($ioid, "sync_conflict");
				if (!$this->autoMerge($dvc_order_data, $db_order_data, "scheduling_pull")) {
					$is_conflicted = TRUE;
					$this->registerConflictedOrder($ioid, $db_order_data);
				}
			}
		}

		return $is_conflicted;*/
	}

	private function syncConflictDetected($order_data, $db_order, $conflict)
	{
		$order		= $order_data['order'];
		$ioid		= $order['ioid'];
		$order_id	= $order['order_id'];

		require_once(dirname(__FILE__)."/../jcvs/jc_func8.php");
		$db_combined_order_data = loadOrderByIOID($ioid, "sync_conflict");

		$this->syncDebug("sync conflict detected - order_id: ".$order_id ." - ioid: ".$ioid." - conflict: ".$conflict);

		if ($this->ldp_debug)
		{
			$this->ksortRecursive($order_data);

			$remote_ip = (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];

			$lg_str = date("Y-m-d H:i:s", time())." - LDP - ".$this->dataname." - ".reqvar("app_version", "")." ".reqvar("app_build", "")." - ".$conflict."[--CR--]";
			$lg_str .= "LOCATION: ".		$this->location_id	." - ";
			$lg_str .= "DEVICE: ".		reqvar("UUID", "")	." - ";
			$lg_str .= "REMOTE IP: ".	$remote_ip			." - ";
			$lg_str .= "IOID: ".			$ioid				."[--CR--]";
			$lg_str .= "DEVICE ORDER: ".	$order['order_id'].", ".$order['total'].", ".$order['sync_tag'].", ".$order['last_mod_ts'].", ".$order['last_mod_device'].", ".$order['closed']."[--CR--]";
			$lg_str .= "DB ORDER: ".		$db_order['order_id'].", ".$db_order['total'].", ".$db_order['sync_tag'].", ".$db_order['last_mod_ts'].", ".$db_order['last_mod_device'].", ".$db_order['closed']."[--CR--]";
			$lg_str .= "DEVICE JSON: ".json_encode($order_data)."[--CR--]";

			$this->ksortRecursive($db_combined_order_data[$ioid]);
			$lg_str .= "DB JSON: ".json_encode($db_combined_order_data[$ioid]);

			write_log_entry("sync_conflict", $this->dataname, $lg_str."\n", "/home/poslavu/logs/company/".$this->dataname."/");
		}

		$this->registerConflictedOrder($ioid, $db_combined_order_data[$ioid]);
	}

	private function ksortRecursive(&$array, $sort_flags = SORT_REGULAR)
	{
		if (!is_array($array))
		{
			return FALSE;
		}

		ksort($array, $sort_flags);

		foreach ($array as &$arr)
		{
			$this->ksortRecursive($arr, $sort_flags);
		}

		return TRUE;
	}

	/**
	 * Compare order data coming from device with the order data coming from the database and
	 * attempt to automatically reconcile any differences
	 *
	 * @param dvc_order_data Associative array of combined order data being pushed up by device
	 * @param db_order_data Associative array of combined order data as it exists in the database
	 */
	private function autoMerge(&$dvc_order_data, $db_order_data, $pull_status) {

		$use_order = "device";

		if (!isset($dvc_order_data['order']) || !isset($db_order_data['order'])) {
			return FALSE;
		}
		$dvc_order = $dvc_order_data['order'];
		$db_order = $db_order_data['order'];

		if (!isset($dvc_order_data['contents']) || !isset($db_order_data['contents'])) {
			return FALSE;
		}
		$dvc_contents = $dvc_order_data['contents'];
		$db_contents = $db_order_data['contents'];

		if (!isset($dvc_order_data['modifiers']) || !isset($db_order_data['modifiers'])) {
			return FALSE;
		}
		$dvc_modifiers = $dvc_order_data['modifiers'];
		$db_modifiers = $db_order_data['modifiers'];

		$dvc_order_state = $this->orderState($dvc_order);
		$db_order_state = $this->orderState($db_order);
		$state_matches = ($dvc_order_state == $db_order_state);

		$root_contents = array();
		$dvc_additional_contents = array();
		$db_additional_contents = array();

		$dvc_more_recent = ($dvc_order['last_mod_ts'] > $db_order['last_mod_ts']);

		if (!$this->reconcileOrderContents($dvc_contents, $db_contents, $root_contents, $dvc_additional_contents, $db_additional_contents, $dvc_more_recent)) {
			return FALSE;
		}

		$dvc_add_count = count($dvc_additional_contents);
		$db_add_count = count($db_additional_contents);

		if (!$state_matches) {

			if (!$this->reconcileOrderState($dvc_order_state, $db_order_state, $dvc_add_count, $db_add_count, $use_order)) {
				return FALSE;
			}

		} else if (!$dvc_more_recent) {

			$use_order = "database";
		}

		$use_order_data = $dvc_order_data;
		$use_additional_contents = $db_additional_contents;
		$other_order_modifiers = $db_order_data['modifiers'];
		$other_order_transactions = $db_order_data['transactions'];
		if ($use_order == "database") {
			$use_order_data = $db_order_data;
			$use_additional_contents = $dvc_additional_contents;
			$other_order_modifiers = $dvc_order_data['modifiers'];
			$other_order_transactions = $dvc_order_data['transactions'];
		}

		$use_order_stub		= $use_order_data['order'];
		$use_order_contents	= $use_order_data['contents'];
		$use_modifiers		= $use_order_data['modifiers'];
		$use_transactions	= $use_order_data['transactions'];
		$use_alt_tx_totals	= $use_order_data['alternate_tx_totals'];
		$use_check_details	= $use_order_data['check_details'];

		$check_count = (int)$use_order_stub['no_of_checks'];

		$added_icids = array();

		foreach ($use_additional_contents as $item) {

			$item_check = (string)$item['check'];

			if (strstr($item_check, "|")) {

				$item_checks = trim(explode("||", $item_check), "|");
				if (count($item_checks) > $check_count) {
					return FALSE;
				}

				foreach ($item_checks as $check) {
					if ((int)$check > $check_count) {
						return FALSE;
					}
				}

			} else if ($item_check == "0") {

				if ($check_count == 1) {
					$item['check'] = "1";
				}

			} else if ((int)$item_check > $check_count) {

				return FALSE;
			}

			$use_order_contents[] = $item;
			$added_icids[] = $item['icid'];
		}

		foreach ($other_order_modifiers as $modifier) {
			if (in_array($modifier['icid'], $added_icids)) {
				$use_modifiers[] = $modifier;
			}
		}

		$tx_internal_ids = array();
		foreach ($use_transactions as $tx) {
			$tx_internal_ids[] = $tx['internal_id'];
		}

		$added_transactions = FALSE;
		foreach ($other_order_transactions as $tx) {
			$itid = $tx['internal_id'];
			if (!empty($itid)) {
				$index = array_search($itid, $tx_internal_ids);
				if ($index !== FALSE) {
					$compare_tx = $use_transactions[$index];
					if ((int)$tx['last_mod_ts'] > (int)$compare_tx['last_mod_ts']) {
						$use_transactions[$index] = $tx;
						$added_transactions = TRUE;
					}
				} else {
					$use_transactions[] = $tx;
					$added_transactions = TRUE;
				}
			}
		}

		if (count($use_additional_contents)>0 || $added_transactions) {
			$use_order_stub['RECALCULATE'] = "1";
		}

		$dvc_order_data = $use_order_data;

		$ioid = $use_order_stub['ioid'];
		$pull_result = &$this->response['pull_result'];

		switch ($pull_status) {

			case "scheduling_pull":

				unset($pull_result['compare_ioids'][$ioid]);

			case "pulling":

				$pr_slot = &$pull_result['order_data'][$ioid];
				$pr_slot['order']				= $use_order_stub;
				$pr_slot['contents']				= $use_order_contents;
				$pr_slot['modifiers']			= $use_modifiers;
				$pr_slot['transactions']			= $use_transactions;
				$pr_slot['alternate_tx_totals']	= $use_alt_tx_totals;
				$pr_slot['check_details']		= $use_check_details;
				break;

			default:
				break;
		}

		return TRUE;
	}

	/**
	 * Compare content items between a device version and database version of an order
	 *
	 * @param dvc_contents Array of contents belonging to device order
	 * @param db_contents Array of contents belonging to database order
	 * @param root Array of contents considered to be shared between device and database orders
	 * @param dvc_additional Array of contents found only on the device order
	 * @param db_additional Array of contents found only on the database order
	 * @param dvc_more_recent Boolean value indicating whether or not the device record is the more recently modified record
	 */
	private function reconcileOrderContents($dvc_contents, $db_contents, &$root, &$dvc_additional, &$db_additional, $dvc_more_recent) {
		$reconciled = TRUE;

		$building_root = TRUE;
		$max_count = MAX(count($dvc_contents), count($db_contents));
		for ($i = 0; $i < $max_count; $i++) {

			if (isset($dvc_contents[$i]) && isset($db_contents[$i])) {

				if ($building_root) {

					$dvc_or_db = $this->compareItems($dvc_contents[$i], $db_contents[$i]);
					switch ($dvc_or_db) {
						case "dvc":
							$root[] = $dvc_contents[$i];
							break;
						case "db":
							$root[] = $db_contents[$i];
							break;
						case "either":
							$root[] = $dvc_more_recent?$dvc_contents[$i]:$db_contents[$i];
							break;
						case "neither":
							$building_root = FALSE;
							$dvc_additional[] = $dvc_contents[$i];
							$db_additional[] = $db_contents[$i];
							break;
						default:
							$reconciled = FALSE;
							break;
					}

					if (!$reconciled) {
						break;
					}

				} else {

					$dvc_additional[] = $dvc_contents[$i];
					$db_additional[] = $db_contents[$i];
				}

			} else if (isset($dvc_contents[$i])) {

				$dvc_additional[] = $dvc_contents[$i];

			} else {

				$db_additional[] = $db_contents[$i];
			}
		}

		if ($reconciled && count($dvc_additional)>0 && count($db_additional)>0) {

			$dvc_item_icids = array();
			$dvc_item_ids = array();
			foreach ($dvc_additional as $item) {
				$dvc_item_icids[] = $item['icid'];
				$dvc_item_ids[] = $item['item_id'];
			}

			foreach ($db_additional as $item) {
				if (in_array($item['item_id'], $dvc_item_ids)) {
					$reconciled = FALSE;
					break;
				}
				if (in_array($item['icid'], $dvc_item_ids)) {
					$reconciled = FALSE;
					break;
				}
			}
		}

		return $reconciled;
	}

	/**
	 * Compare two item records to check for equivalency and report which version should be favored
	 *
	 * @param dvc_item Associative array of item record from device order contents
	 * @param db_item Associative array of item record from database order contents
	 * @param favor_voided Boolean value to determine if voided records should always replace non-voided records
	 */
	private function compareItems($dvc_item, $db_item) {

		if ($dvc_item['icid'] != $db_item['icid']) {
			return "neither";
		}

		if ($dvc_item['item_id'] != $db_item['item_id']) {
			return "neither";
		}

		// may eventually use last_mod_ts to determine which item record to keep if not equivalent

		if ((float)$dvc_item['quantity'] != (float)$db_item['quantity']) {
			return FALSE;
		}

		if ($dvc_item['check'] != $db_item['check']) {
			return FALSE;
		}

		if ((float)$dvc_item['price'] != (float)$db_item['price']) {
			return FALSE;
		}

		if ($dvc_item['options'] != $db_item['options']) {
			return FALSE;
		}

		if ($dvc_item['special'] != $db_item['special']) {
			return FALSE;
		}

		if ($dvc_item['notes'] != $db_item['notes']) {
			return FALSE;
		}

		if ($dvc_item['seat'] != $db_item['seat']) {
			return FALSE;
		}

		if ($dvc_item['course'] != $db_item['course']) {
			return FALSE;
		}

		if ($dvc_item['hidden_data1'] != $db_item['hidden_data1']) {
			return FALSE;
		}

		// favor sent item
		if ((int)$dvc_item['sent']==0 && (int)$db_item['sent']==1) {
			return "db";
		}

		return "either";
	}

	/**
	 * Determine the state of a given order record: open, closed, reopened, or reclosed
	 *
	 * @param order Associative array of order stub
	 */
	private function orderState($order) {

		$state = "open";

		$closed = $order['closed'];
		$reopened = $order['reopened_datetime'];
		$reclosed = $order['reclosed_datetime'];

		if (!empty($closed) && $closed!="0000-00-00 00:00:00") {
			$state = "closed";
			if (!empty($reopened) && $reopened!="0000-00-00 00:00:00") {
				$state = (!empty($reclosed) && $reclosed!="0000-00-00 00:00:00")?"reclosed":"reopened";
			}
		}

		return $state;
	}

	/**
	 * Determine whether or not differing order states can be ignored to allow a merge to be performed
	 *
	 * @param dvc_state String indicating the state of the device order
	 * @param db_state String indicating the sate of the database order
	 * @param dvc_add_count Integer indicating the number of items found in the device order that were not found in the database order
	 * @param db_add_count Integer indicating the number of items found in the database order that were not found in the device order
	 * @param use_order String used to communicate back to the calling function which record (device or database) should be used
	 */
	private function reconcileOrderState($dvc_state, $db_state, $dvc_add_count, $db_add_count, &$use_order) {

		$reconciled = TRUE;

		switch ($dvc_state) {

			case "open":

				switch ($db_state) {

					case "closed":
					case "reopened":
					case "reclosed":

						$reconciled = ($dvc_add_count == 0);
						$use_order = "database";
						break;

					default:

						$reconciled = FALSE;
						break;
					}

				break;

			case "closed":

				switch ($db_state) {

					case "open":

						$reconciled = ($db_add_count == 0);
						break;

					case "reopened":
					case "reclosed":

						$reconciled = ($dvc_add_count == 0);
						$use_order = "database";
						break;

					default:

						$reconciled = FALSE;
						break;
				}

				break;

			case "reopened":

				switch ($db_state) {

					case "open":
					case "closed":

						$reconciled = ($db_add_count == 0);
						break;

					case "reclosed":

						$reconciled = ($dvc_add_count == 0);
						$use_order = "database";
						break;

					default:

						$reconciled = FALSE;
						break;
				}

				break;

			case "reclosed":

				switch ($db_state) {

					case "open":
					case "closed":
					case "reopened":

						$reconciled = ($db_add_count == 0);
						break;

					default:

						$reconciled = FALSE;
						break;
				}

				break;

			default:

				$reconciled = FALSE;
				break;

		}

		return $reconciled;
	}

	/**
	 * Send both versions of a conflicted order back down to the device to be manually resolved
	 *
	 * @param ioid String of internal order id of confliced order
	 * @param db_order_data Associative array of combined order data as it exists in the database
	 */
	private function registerConflictedOrder($ioid, $db_order_data)
	{
		if (!isset($this->response['conflicted_orders']))
		{
			$this->response['conflicted_orders'] = array();
		}
		$this->response['conflicted_orders'][$ioid] = $db_order_data;

		if (!in_array($ioid, $this->conflicted_ioids))
		{
			$this->conflicted_ioids[] = $ioid;
		}
	}

	/**
	 * Check for pending order ownership requests (found in config table)
	 */
	private function checkForOrderOwnershipRequests()
	{
		$uuid = reqvar("UUID", "");
		if (empty($uuid))
		{
			return;
		}

		$open_ioid = reqvar("open_ioid", "");
		$now_ts = time();

		$granted_requests = array();

		$check_requests = lavu_query("SELECT `id`, `value` AS `ioid`, `value2` AS `requestor` FROM `config` WHERE `type` = 'ownership_request' AND `setting` = '[1]' AND `value4` >= '[2]'", $uuid, ($now_ts - 30));
		if ($check_requests)
		{
			while ($request = mysqli_fetch_assoc($check_requests))
			{
				$status = "0";

				$ioid = $request['ioid'];
				if ($ioid == $open_ioid)
				{
					$status = "9"; // OwnershipRequestErrorOrderIsBeingEdited
				}

				if (isset($granted_requests[$ioid]))
				{
					$status = "14";	// OwnershipRequestErrorWrongDevice
				}

				// check server record to see if order.owned_by_uuid == uuid

				$write_response = lavu_query("UPDATE `config` SET `value3` = '[1]' WHERE `id` = '[2]'", $status, $request['id']);
				if ($write_response && $status=="0")
				{
					$granted_requests[$ioid] = $request['requestor'];
				}
			}
		}

		if (count($granted_requests) > 0)
		{
			$this->response['granted_ownership_requests'] = $granted_requests;
		}
	}

	/**
	 * Delete all pre-existing rows for the ioid that we're syncing (per Lavu standard syncing convention).
	 *
	 * @param table Database table name that we're syncing.
	 * @param order_id Order ID.
	 * @param location_id Location ID.
	 * @param ioid Unique order instance identifier needed in case of error.
	 */
	private function deletePreexistingOrderRows($table, $ioid, $check_count)
	{
		$rows_deleted = 0;

		// Checking for blank values for the vars in the where clause should've already
		// happened by now, but I opted for paranoia since we're deleting stuff.
		if (empty($ioid))
		{
			return $rows_deleted;
		}

		$sql = "";
		switch ($table)
		{
			case "modifiers_used":
			case "alternate_payment_totals":

				$sql = "DELETE FROM `".$table."` WHERE `ioid` = '[ioid]'";
				break;

			case "split_check_details":

				$sql = "DELETE FROM `".$table."` WHERE `check` > '[check_count]'";
				break;

			case "orders":
				// Don't clear out order records so that serial_no and sync_tag incrementation stays sound.
			case "order_contents":
				// Don't want to clear out order_contents in order to help facilitate sync conflict detection and resolution
			case "cc_transactions":
				// Don't want to clear out transaction records that may have been written directly to the server but never made it to the mobile device
			default:

				return 0;
				break;
		}

		$this->logDebug("LDP: ioid=".$ioid." sql=".$sql);

		$result = lavu_query($sql, array("ioid" => $ioid, "check_count" => $check_count));
		if ($result === FALSE)
		{
			$this->logMsg("LDP: query error (".$this->dataname."): ".lavu_dberror());
			return $rows_deleted;
		}

		$rows_deleted = ConnectionHub::getConn('rest')->affectedRows();
		$this->logDebug("LDP: rows_deleted=".$rows_deleted);

		return $rows_deleted;
	}

	/**
	 * Insert the new rows for the order being synced.
	 *
	 * @param table Database table name that we're syncing.
	 * @param row Order ID.
	 * @param ioid Unique order instance identifier needed in case of error.
	 *
	 * returns TRUE or FALSE, depending on the success of the query
	 */
	private function insertOrUpdateRow($table, $row, $ioid, $allow_empty_order_ids=FALSE)
	{
		$preserve_fields = array(); // When the populated value in the database takes precedence over an empty value in the post.
		switch ($table)
		{
			case "split_check_details":

				$preserve_fields = array( "loyaltree_info" );
				break;

			default:
				break;
		}

		$row_last_modified	= (isset($row['last_modified']))?$row['last_modified']:"";
		$pushed_ts = time();

		$cols	= "";
		$vals	= "";
		$update = "";
		$vars	= array();

		foreach ($row as $col => $val)
		{
			if (!$this->shouldIgnoreColumn($col, $table))
			{
				switch ($table)
				{
					case "orders":

						switch ($col)
						{
							case "last_modified":

								$val = str_replace("CLAIM-ONLY-", "", $val);
								break;

							case "sync_tag_ack":

								// To ensure that any sync_tag_ack value coming from the app is never committed to the database.
								$val = "";
								break;

							default:
								break;
						}

						break;
				}

				$vars[$col] = $val;
			}
		}

		if ($table == "orders")
		{
			// sync_tag starts at 1 if unset
			$sync_tag = ((int)$vars['sync_tag'] + 1);

			if ($this->syncTagAcknowledgements->enabled())
			{
				$vars['sync_tag_ack'] = time().",".$this->device_id;
			}
			else
			{
				$vars['sync_tag'] = $sync_tag;
				$vars['sync_tag_ack'] = "";
			}

			$vars['pushed_ts'] = $pushed_ts;
		}

		$loc_id_key = ($table=="orders" || $table=="send_log")?"location_id":"loc_id";
		if (empty($vars[$loc_id_key]))
		{
			$vars[$loc_id_key] = $this->location_id;
		}

		if (!$allow_empty_order_ids)
		{
			if (empty($vars['ioid']))
			{
				$vars['ioid'] = $ioid;
			}

			if ($table!="action_log" && $table!="send_log")
			{
				$vars['order_id'] = $this->order_id;
			}
		}

		$update_id = FALSE;
		$order_serial_no = "";
		if ($table=="orders" && !empty($vars['ioid']))
		{
			$check_existing = lavu_query("SELECT `id`, `serial_no`, `last_modified`, `active_device` FROM `orders` WHERE `ioid` = '[ioid]' ORDER BY `sync_tag` DESC, `last_mod_ts` DESC, `id` DESC", $vars);
			if ($check_existing !== FALSE)
			{
				$row_count = mysqli_num_rows($check_existing);

				if ($row_count > 0)
				{
					$existing_record = mysqli_fetch_assoc($check_existing);

					$update_id			= $existing_record['id'];
					$order_serial_no	= $existing_record['serial_no'];

					if ($row_count > 1)
					{
						$this->clearDuplicateRecords("orders", $update_id, array( 'ioid' => $vars['ioid'] ));
					}

					$db_last_modified = $existing_record['last_modified'];
					if (substr($db_last_modified, 0, 11) == "CLAIM-ONLY-")
					{
						$row_last_modified = str_replace("CLAIM-ONLY-", "", $row_last_modified);
						$db_last_modified = str_replace("CLAIM-ONLY-", "", $db_last_modified);
						$db_active_device = $existing_record['active_device'];

						if (!empty($vars['active_device']) && empty($db_active_device) && ($db_last_modified >= $row_last_modified))
						{
							$vars['active_device'] = "";
						}
					}
				}
			}
		}
		else if ($table=="order_contents" && !empty($vars['icid']))
		{
			$check_existing = lavu_query("SELECT `id`, `last_mod_ts` FROM `order_contents` WHERE `icid` = '[icid]'", $vars);
			if ($check_existing !==FALSE)
			{
				$row_count = mysqli_num_rows($check_existing);

				if ($row_count > 0)
				{
					$existing_record = mysqli_fetch_assoc($check_existing);

					$update_id = $existing_record['id'];

					if ($row_count > 1)
					{
						$this->clearDuplicateRecords("order_contents", $update_id, array( 'icid' => $vars['icid'] ));
					}

					if ((int)$vars['last_mod_ts'] <= (int)$existing_record['last_mod_ts'])
					{
						// record already up to date
						return TRUE;
					}
				}
			}
		}
		else if ($table=="cc_transactions" && !empty($vars['internal_id']))
		{
			$check_existing = lavu_query("SELECT `id`, `last_mod_ts` FROM `cc_transactions` WHERE `internal_id` = '[internal_id]'", $vars);
			if ($check_existing !== FALSE)
			{
				$row_count = mysqli_num_rows($check_existing);

				if ($row_count > 0)
				{
					$existing_record = mysqli_fetch_assoc($check_existing);

					$update_id = $existing_record['id'];

					if ($row_count > 1)
					{
						$this->clearDuplicateRecords("cc_transactions", $update_id, array( 'internal_id' => $vars['internal_id'] ));
					}

					if ((int)$vars['last_mod_ts'] <= (int)$existing_record['last_mod_ts'])
					{
						// record already up to date
						return TRUE;
					}
				}
			}
		}
		else if ($table=="split_check_details")
		{
			$check_fields = (count($preserve_fields) > 0)?",`".implode("`,`", $preserve_fields)."`":"";
			$check_existing = lavu_query("SELECT `id`".$check_fields." FROM `split_check_details` WHERE `ioid` = '[ioid]' AND `check` = '[check]' ORDER BY `id` DESC", $vars);
			if ($check_existing !== FALSE)
			{
				$row_count = mysqli_num_rows($check_existing);

				if ($row_count)
				{
					$existing_record = mysqli_fetch_assoc($check_existing);
					foreach ($preserve_fields as $pf)
					{
						if (empty($vars[$pf]) && !empty($existing_record[$pf]))
						{
							unset($vars[$pf]);
							$this->addInternalUpdate($ioid, "check_details", $pf, $existing_record[$pf], "check", $vars['check']);
						}
					}
					$update_id = $existing_record['id'];

					if ($row_count > 1)
					{
						$this->clearDuplicateRecords("split_check_details", $update_id, array( 'ioid' => $vars['ioid'], 'check' => $vars['check'] ));
					}
				}
			}
		}
		else if ($table=="action_log" && !empty($vars['ialid']))
		{
			$check_existing = lavu_query("SELECT `id` FROM `action_log` WHERE `ialid` = '[ialid]'", $vars );
			if ($check_existing!==FALSE && mysqli_num_rows($check_existing))
			{
				$existing_record = mysqli_fetch_assoc($check_existing);
				$update_id = $existing_record['id'];
			}
			else if ($vars['action'] == "Order Closed")
			{
				$check_for_previous_close = lavu_query("SELECT `id` FROM `action_log` WHERE `ioid` = '[ioid]' AND `action` = 'Order Closed'", $vars);
				if ($check_for_previous_close!==FALSE && mysqli_num_rows($check_for_previous_close))
				{
					$this->syncDebug("second close detected and rejected - order_id: ".$vars['order_id']." - ioid: ".$vars['ioid']);
					return 1;
				}
			}
		}
		else if ($table=="send_log" && !empty($vars['islid']))
		{
			$check_existing = lavu_query("SELECT `id` FROM `send_log` WHERE `islid` = '[islid]'", $vars );
			if ($check_existing!==FALSE && mysqli_num_rows($check_existing))
			{
				$existing_record = mysqli_fetch_assoc($check_existing);
				$update_id = $existing_record['id'];
			}
		}

		if (in_array($table, array("action_log", "send_log")) && $update_id)
		{
			// these records do not change, so there's no need to update existing record;
			// return 1 so the push_result can report success
			return TRUE;
		}

		$update	= "";
		$cols	= "";
		$vals	= "";

		if ($update_id)
		{
			buildUpdate($vars, $update);
		}
		else
		{
			buildInsertFieldsAndValues($vars, $cols, $vals);
		}

		if ($table=="orders" && !$update_id)
		{
			$get_last_serial_no = lavu_query("SELECT `serial_no` FROM `orders` ORDER BY `serial_no` DESC LIMIT 1");
			if ($get_last_serial_no!==FALSE && mysqli_num_rows($get_last_serial_no))
			{
				$last_record = mysqli_fetch_assoc($get_last_serial_no);
				$order_serial_no = ($last_record['serial_no'] + 1);
				$vars['serial_no'] = $order_serial_no;
				$cols .= ", `serial_no`";
				$vals .= ", '[serial_no]'";
			}
		}

		$sql = $update_id?"UPDATE `".$table."` SET ".$update." WHERE `id` = '".$update_id."'":"INSERT INTO `".$table."` (".$cols.") VALUES (".$vals.")";

		$this->logDebug("sql=".$sql);

		$result = lavu_query($sql, $vars);
		$success = ($result !== FALSE);
		if (!$success)
		{
			$this->logMsg("DataPlex - ".$this->dataname." - req_id: ".$this->req_id." - query error: ".lavu_dberror());
		}

		if ($table=="orders" && ($update_id || $success))
		{ // Send serial_no, sync_tag, and pushed_ts back to app to update internal record.
			$this->addInternalUpdate($ioid, "orders", "serial_no", (string)$order_serial_no);
			$this->addInternalUpdate($ioid, "orders", "sync_tag", (string)$sync_tag);
			$this->addInternalUpdate($ioid, "orders", "pushed_ts", (string)$pushed_ts);
		}

		$ubcoc = reqvar("ubcoc");
		if ($success && $table=="orders" && $ubcoc=="1")
		{
			$this->useBackwardCompatibleOrderClaim($row);
		}

		return $success;
	}

	private function shouldIgnoreColumn($col, $table)
	{
		switch ($col)
		{
			case "id":
			case "needs_sync":

				return TRUE;
				break;

			case "dataname":

				return ($table=="orders" || $table=="action_log" || $table=="send_log");

			case "lastmod":
			case "lastsync":
			case "order_hash":
			case "reopen_datetime":
			case "serial_no":

				return ($table == "orders");
				break;

			case "check_item_count":
			case "checkTaxExemption":
			case "deposit_amount":
			case "deposit_remaining":
			case "displayDiscount":
			case "empty_check_flag":
			case "iTaxExemptAdjustments":
			case "overpaid_amount":
			case "rawSubtotal":
			case "rfid_payment_flag":
			case "tax_fraction":

				return ($table == "split_check_details");
				break;

			case "send_to_kds":

				return ($table == "order_contents");
				break;

			case "DO_NOT_DRAW_ORDER":
			case "NEW PAYMENT":
			case "ORIGINAL_TX":
			case "orig_action":
			case "orig_internal_id":
			case "server":
			case "take_signature":
			case "take_tip":

				return ($table == "cc_transactions");
				break;

			case "company_id":
			case "dataname":

				return ($table=="action_log" || $table=="send_log");
				break;

			case "tx":

				return ($table == "action_log");
				break;

			case "loc_id":

				return ($table == "send_log");
				break;

			default:

				break;
		}

		return FALSE;
	}

	private function clearDuplicateRecords($table, $keep_id, $wheres)
	{
		$where_clause = "";
		foreach ($wheres as $key => $val)
		{
			if (!empty($where_clause))
			{
				$where_clause .= " AND ";
			}
			$where_clause .= "`".$key."` = '[".$key."]'";
		}

		if (empty($where_clause))
		{
			return;
		}

		$wheres['id'] = $keep_id;

		$clear_duplicate_records = lavu_query("DELETE FROM `".$table."` WHERE ".$where_clause." AND `id` != '[id]'", $wheres);

		$result = ($clear_duplicate_records !== FALSE)?"success":lavu_dberror();

		$this->syncDebug("clearDuplicateRecords where ".$where_clause." - keep_id: ".$keep_id." - result: ".$result);
	}

	private function updatePaidInPaidOutPlaceHolder($paid_in_or_out)
	{
		$check_placeholder = lavu_query("SELECT `id` FROM `orders` WHERE `location_id` = '[1]' AND `order_id` = '[2]'", $this->location_id, $paid_in_or_out);
		if (mysqli_num_rows($check_placeholder) > 0)
		{
			$info = mysqli_fetch_assoc($check_placeholder);
			$query = "UPDATE `orders` SET `closed` = '[1]' WHERE `id` = '[2]'";
			lavu_query($query, date("Y-m-d H:i:s"), $info['id']);
		}
		else
		{
			$query = "INSERT INTO `orders` (`location_id`, `order_id`, `opened`, `closed`) VALUES ('[1]', '[2]', '[3]', '[3]')";
			$create_placeholder = lavu_query($query, $this->location_id, $paid_in_or_out, date("Y-m-d H:i:s"));
		}
	}

	public function addInternalUpdate($ioid, $table, $set_field, $set_value, $match_field="", $match_value="")
	{
		$iu = &$this->internal_updates;

		if (!isset($iu[$ioid])) $iu[$ioid] = array();
		if (!isset($iu[$ioid][$table])) $iu[$ioid][$table] = array();

		if ($table == "orders")
		{
			$iu[$ioid][$table][] = array($set_field => $set_value);
		}
		else
		{
			if (!isset($iu[$ioid][$table][$match_field])) $iu[$ioid][$table][$match_field] = array();
			if (!isset($iu[$ioid][$table][$match_field][$match_value])) $iu[$ioid][$table][$match_field][$match_value] = array();

			$iu[$ioid][$table][$match_field][$match_value][] = array("$set_field"=>$set_value);
		}
	}

	private function useBackwardCompatibleOrderClaim($order)
	{
		$diff = (time() - (int)$order['last_mod_ts']);
		if ($diff < 30)
		{
			$order_id = $order['order_id'];
			$clear_holding_order_id = lavu_query("UPDATE `devices` SET `holding_order_id` = '' WHERE `holding_order_id` = '[1]'", $order_id);

			$uuid = reqvar("UUID");
			$active_device = $order['active_device'];

			if ($active_device==$uuid && !empty($active_device))
			{
				$set_holding_order_id = lavu_query("UPDATE `devices` SET `holding_order_id` = '[1]' WHERE `UUID` = '[2]'", $order_id, $uuid);
			}
		}
	}

	/**
	 * Output response message to LDP sync queue manager process.
	 *
	 * @param statuses Array with IOID keys and values are status arrays.
	 */
	private function sendResponseJson($response)
	{
		$response_json = json_encode($response);
		$this->logDebug($response_json);

		return $response_json;
	}

	/**
	 * Print logging message.
	 *
	 * @param msg Message to log.
	 */
	private function logMsg($msg)
	{
		// TO DO : check for cli to know whether to do echo if
		error_log($msg);
	}

	/**
	 * Print logging message, if debug flag member is set.
	 *
	 * @param msg Message to log.
	 */
	private function logDebug($msg)
	{
		if ( $this->debug ) $this->logMsg($msg);
	}

	/**
	 * Print logging message, if debug flag member is set.
	 *
	 * @param msg Message to log.
	 */
	public function syncDebug($msg)
	{
		if ( $this->ldp_debug ) $this->logMsg("DataPlex - ".$this->dataname." - req_id: ".$this->req_id." - ".$msg);
	}
}
