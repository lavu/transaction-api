<?php

class SyncTackAcknowledgements
{
	private $acknowledgements;

	public $responses;

	public function __construct()
	{
		$this->acknowledgements	= array();
		$this->responses		= array();
	}

	public function initialize($sync_data)
	{
		if (isset($sync_data['sync_tag_acks']))
		{
			$this->acknowledgements = $sync_data['sync_tag_acks'];
		}
	}

	/**
	 * Convience function for determining whether or not sync_tag acknowledgements should be used and respected.
	 */
	public function enabled()
	{
		global $location_info;

		return ((int)locationSetting("disable_sync_tag_ack") != 1);
	}

	/**
	 * Check incoming request for sync_tag acknowledgements and update the appropriate order record(s).
	 * Each ack for a given ioid will be an array of the coded location, location id, and sync_tag value
	 */
	public function check($caller)
	{
		if (!$this->enabled())
		{
			if (count($this->acknowledgements) > 0)
			{
				$this->addResponse("all", "disabled", "disabled");
			}
			return;
		}

		$caller->syncDebug("SyncTagAcknowledgements->check()");

	    $sync_tag = "";
		foreach ($this->acknowledgements as $ioid => $ack)
		{
			if (count($ack) < 3)
			{
				// The ack does not contain the required info.
				$this->addResponse($ioid, $sync_tag, "incomplete");
				continue;
			}

			$coded_location	= $ack[0];
			$location_id	= $ack[1];
			$sync_tag		= (int)$ack[2];

			$caller->checkCodedLocation($coded_location, $location_id);

			$get_order_stub = lavu_query("SELECT `id`, `sync_tag`, `pushed_ts` FROM `orders` WHERE `ioid` = '[1]' ORDER BY `sync_tag` DESC, `last_mod_ts` DESC, `id` DESC LIMIT 1", $ioid);
			if (!$get_order_stub)
			{
				$this->addResponse($ioid, $sync_tag, "db_error");
				continue;
			}

			if (mysqli_num_rows($get_order_stub) == 0)
			{
				$this->addResponse($ioid, $sync_tag, "order_not_found");
				continue;
			}

			$db_order = mysqli_fetch_assoc($get_order_stub);

			$db_id			= $db_order['id'];
			$db_sync_tag	= (int)$db_order['sync_tag'];
			$db_pushed_ts	= (int)$db_order['pushed_ts'];

			if ($sync_tag <= $db_sync_tag)
			{
				// No need to update order record if sync_tag won't be incremented.
				$this->addResponse($ioid, $sync_tag, "no_change");
				continue;
			}

			// Ensure that that the pushed_ts value will be incremented by at least 1.
			$set_pushed_ts = MAX(time(), ($db_pushed_ts + 1));

			$update_order_stub = lavu_query("UPDATE `orders` SET `sync_tag` = '[1]', `sync_tag_ack` = '', `pushed_ts` = '[2]' WHERE `id` = '[3]'", $sync_tag, $set_pushed_ts, $db_id);
			if (!$update_order_stub)
			{
				$this->addResponse($ioid, $sync_tag, "db_error");
				continue;
			}

			$this->addResponse($ioid, $sync_tag, "success");
		}
	}

	/**
	 * Report on the success or failure of the acceptance of a given sync_tag for an order
	 *
	 * @param ioid internal order id specifying the subject order record
	 * @param sync_tag Integer value of the accepted sync_tag
	 * @param status String success|no_change|incomplete|db_error|order_not_found
	 */
	private function addResponse($ioid, $sync_tag, $status)
	{
		$star = &$this->responses;

		if (!isset($star[$ioid]))
		{
			$status_code = 0;
			switch ($status)
			{
				case "no_change":

					$status_code = 1;
					break;

				case "incomplete":

					$status_code = 2;
					break;

				case "db_error":

					$status_code = 3;
					break;

				case "order_not_found":

					$status_code = 4;
					break;

				case "disabled":

					$status_code = 5;
					break;

				case "success":
				default:
					break;
			}

			$star[$ioid] = array(
				'status'		=> $status,
				'status_code'	=> (string)$status_code,
				'sync_tag'		=> (string)$sync_tag
			);
		}
	}
}