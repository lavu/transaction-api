<?php

	require_once(__DIR__."/../cp/resources/core_functions.php");
	
	global $heartland_server_url;
	global $merchantware_subdomain;
	global $mercury_gift_card_server_url;
	global $mercury_secondary_url;
	global $mercury_server_url;
	global $mpshc_server_url;
	global $tgate_gift_card_server_url;
	global $tgate_loyalty_card_server_url;
	global $tgate_server_url;
	global $usaepay_sandbox;
	global $usaepay_test_mode;

	if (lavuDeveloperStatus()) {

		//$heartland_server_url = "https://posgateway.cert.secureexchange.net/Hps.Exchange.PosGateway/PosGatewayService.asmx";
		$heartland_server_url	= "https://cert.api2.heartlandportico.com/Hps.Exchange.PosGateway/PosGatewayService.asmx";
		//https://posgateway.cert.secureexchange.net/Hps.Exchange.PosGateway.UAT/PosGatewayService.asmx
		//$heartland_stateful_url = "https://posgateway.cert.secureexchange.net/Hps.Exchange.Posgateway.stateful/PosgatewayService.asmx";
		$heartland_stateful_url	= "https://cert.api2.heartlandportico.com/Hps.Exchange.Posgateway.stateful/PosgatewayService.asmx";

		$merchantware_subdomain = "staging";

		$mercury_server_url				= "w1.mercurycert.net";
		$mercury_secondary_url			= "w1.mercurycert.net";
		$mercury_gift_card_server_url	= "w1.mercurycert.net";
		$mpshc_server_url				= "hc.mercurycert.net";

		$tgate_server_url				= "https://gatewaystage.itstgate.com/SmartPayments/transact3.asmx/ProcessCreditCard";
		$tgate_gift_card_server_url		= "https://gatewaystage.itstgate.com/SmartPayments/transact3.asmx/ProcessGiftCard";
		$tgate_loyalty_card_server_url	= "https://gatewaystage.itstgate.com/SmartPayments/transact3.asmx/ProcessLoyaltyCard";

		$usaepay_sandbox = true;
		$usaepay_test_mode = 0;

	} else {

		$heartland_server_url = "https://api2.heartlandportico.com/Hps.Exchange.PosGateway/PosGatewayService.asmx";

		$merchantware_subdomain = "ps1";

		$mercury_server_url				= "w1.mercurypay.com";
		$mercury_secondary_url			= "w2.backuppay.com";
		$mercury_gift_card_server_url	= "w1.mercurypay.com";
		$mercury_qr_server_url			= "w1.mercurypay.com";
		$mercury_paypal_server_url		= "w2.backuppay.com"; //"w1.mercurypay.com";
		$mpshc_server_url				= "hc.mercurypay.com";

		$tgate_server_url				= "https://gateway.itstgate.com:10221/SmartPayments/transact3.asmx/ProcessCreditCard";
		$tgate_gift_card_server_url		= "https://gateway.itstgate.com:10221/SmartPayments/transact3.asmx/ProcessGiftCard";
		$tgate_loyalty_card_server_url	= "https://gateway.itstgate.com:10221/SmartPayments/transact3.asmx/ProcessLoyaltyCard";

		$usaepay_sandbox = false;
		$usaepay_test_mode = 0;
	}
?>
