<?php

	//session_start(); // don't use session so that pay period start date will always be current

	$json_session = true;
	
	require_once("info_inc.php");

	function cleanJSON($json_string) {
	
		$search_array = array();
		$replace_array = array();
		for ($c = 0; $c < 32; $c++) {
			if ($c!=10 && $c!=13) {
				$search_array[] = chr($c);
				$replace_array[] = "";
			}
			if ($c==10) {
				$search_array[] = chr($c);
				$replace_array[] = "[--CR--]";
			}
		}
		
		//return str_replace($search_array, $replace_array, $json_string);
		return iconv("UTF-8", "UTF-8//IGNORE", str_replace($search_array, $replace_array, $json_string));
	}
	
	if(lsecurity_id($company_code_key,$data_name)!=$company_info['id'] && lsecurity_id1($company_code_key)!=$company_info['id'])
	{
		session_destroy();
		//lavu_exit();
	}

	log_comm_vars($data_name, $_REQUEST['loc_id'], "RECEIPT APPEND RESULTS");
	
	// expect request variables: m, cc, data_name, version, build, loc_id, order_id, check, device_time

	function showAmount($amount) {
	
		global $location_info;
		
		$amount = str_replace(",","",$amount);

		if ($location_info['left_or_right'] == "right") {
			return number_format($amount, $location_info['disable_decimal'], $location_info['decimal_char'], ",").$location_info['monitary_symbol'];
		} else {
			return $location_info['monitary_symbol'].number_format($amount, $location_info['disable_decimal'], $location_info['decimal_char'], ",");
		}
	}
	
	$mode = (isset($_REQUEST['m']))?$_REQUEST['m']:"";
	
	$device_time = (isset($_REQUEST['device_time']))?$_REQUEST['device_time']:date("Y-m-d H:i:s");
	if ($location_info['timezone'] != "") $device_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);

	$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
	$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

	$idatetime = explode(" ", $device_time);
	$idate = explode("-", $idatetime[0]);
	$itime = explode(":", $idatetime[1]);
	$yts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] - 1), $idate[0]);
	$nowts = mktime($itime[0], $itime[1], $itime[2], $idate[1], $idate[2], $idate[0]);
	$tts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] + 1), $idate[0]);

	if ($itime[0].$itime[1].$itime[2] >= $se_hour.$se_min."00") {
		$use_date = date("Y-m-d", $nowts);
		$end_date = date("Y-m-d", $tts);
		$today_ts = $nowts;
	} else {
		$use_date = date("Y-m-d", $yts);
		$end_date = date("Y-m-d", $nowts);
		$today_ts = $yts;
	}
	
	$udate = explode("-",$use_date);
	$uyear = $udate[0];
	$umonth = $udate[1];
	$uday = $udate[2];
	$udate_display = date("n/j/Y",mktime(0,0,0,$umonth,$uday,$uyear));
	
	$end_hour = $se_hour;
	$end_min = str_pad(($se_min - 1), 2, "0", STR_PAD_LEFT);
	if ($end_min < 0) {
		$end_hour = str_pad(($se_hour - 1), 2, "0", STR_PAD_LEFT);
		$end_min = 59;
	}
	if ($end_hour < 0) {
		$end_hour = 23;
		$end_date = date("Y-m-d", $nowts);
	}
	
	$start_datetime = "$use_date $se_hour:$se_min:00";
	$end_datetime = "$end_date $end_hour:$end_min:59";

	if ($mode == "meal_plan") {

		///echo "this|o|[L]|*|is|o|[C]|*|just|o|[R]|*|a|o|[C]|*|test|o|[L]|*||*|";
		//echo "this|o|this|*|is|o|is|*|just|o|just|*|a|o|a|*|test|o|test|*||*|";
				
		$get_payment_type_ids = lavu_query("SELECT `id` FROM `payment_types` WHERE `loc_id` = '[1]' AND `special` = 'rf_id'", $_REQUEST['loc_id']);
		if (@mysqli_num_rows($get_payment_type_ids) > 0) {
			$payment_type_ids = array();
			while ($info = mysqli_fetch_assoc($get_payment_type_ids)) {
				$payment_type_ids[] = $info['id'];
			}
			
			$ppdatetime = explode(" ", $location_info['apple_pay_periods']);
			$ppdate = explode("-", $ppdatetime[0]);
			$pptime = explode(":", $ppdatetime[1]);
			$ppts = mktime($pptime[0], $pptime[1], $pptime[2], $ppdate[1], $ppdate[2], $ppdate[0]);

			$get_customer_info = lavu_query("SELECT `cc_transactions`.`customer_id` AS `customer_id`, `customer_accounts`.`f_name` AS `f_name`, `customer_accounts`.`l_name` AS `l_name` FROM `cc_transactions` LEFT JOIN `customer_accounts` ON `customer_accounts`.`id` = `cc_transactions`.`customer_id` WHERE `cc_transactions`.`loc_id` = '[1]' AND `cc_transactions`.`order_id` = '[2]' AND `cc_transactions`.`check` = '[3]' AND `cc_transactions`.`pay_type_id` IN (".implode(",", $payment_type_ids).") ", $_REQUEST['loc_id'], $_REQUEST['order_id'], $_REQUEST['check']);	
			if (@mysqli_num_rows($get_customer_info) > 0) {
				while ($info = mysqli_fetch_assoc($get_customer_info)) {
					
					$period_total = 0;
					$day_total = 0;
					
					$get_period_transactions = lavu_query("SELECT `total_collected`, `datetime` FROM `cc_transactions` WHERE `datetime` >= '[1]' AND `datetime` <= '[2]' AND `customer_id` = '[3]' AND `pay_type_id` IN (".implode(",", $payment_type_ids).")", $location_info['apple_pay_periods'], $end_datetime, $info['customer_id']);
					while ($t_info = mysqli_fetch_assoc($get_period_transactions)) {
						$period_total += $t_info['total_collected'];
						if ($t_info['datetime'] >= $start_datetime) $day_total += $t_info['total_collected'];
					}
					
					echo "Meal Plan totals for ".$info['f_name']." ".$info['l_name']."|o|[C]|*||*|Today (".date("F j", $today_ts)."):|o|".showAmount($day_total)."|*|Period (Since ".date("M j h:ia", $ppts)."):|o|".showAmount($period_total)."|*|";					
				}
			}
		}
		
		lavu_exit();

	} else if ($mode == "loyaltree_qr_code") {
	
		require_once(resource_path()."/loyal_tree.php");

		//header( 'Location: http://admin.poslavu.com/images/qr_code.jpg' );

		if ($location_info['loyaltree_username']!="" && $location_info['loyaltree_password']) {
	
			$get_order_info = lavu_query("SELECT `orders`.`cashier_id` AS `cashier_id`, `orders`.`tablename` AS `tablename`, `orders`.`guests` AS `guests`, `split_check_details`.`check_total` AS `check_total`, `split_check_details`.`loyaltree_info` AS `loyaltree_info`, `split_check_details`.`id` AS `scd_id`,`orders`.`total` as `loyaltree_total` FROM `orders` LEFT JOIN `split_check_details` ON (`split_check_details`.`loc_id` = `orders`.`location_id` AND `split_check_details`.`order_id` = `orders`.`order_id` AND `split_check_details`.`check` = '[3]') WHERE `orders`.`location_id` = '[1]' AND `orders`.`order_id` = '[2]'", $_REQUEST['loc_id'], $_REQUEST['order_id'], $_REQUEST['check']);
			if (mysqli_num_rows($get_order_info) > 0) {
		
				$order_info = mysqli_fetch_assoc($get_order_info);
						
				if ($order_info['loyaltree_info'] != "") {
				
					echo cleanJSON($order_info['loyaltree_info']);
				
					//mail("richard@greenkeyconcepts.com","LOYALTREE purchase response" , $order_info['loyaltree_info']);
	
				} else {
				
					$data = array();
					$data['dataname'] = $data_name;
					$data['uname'] = $location_info['loyaltree_username'];
					$data['pword'] = $location_info['loyaltree_password'];
					$data['storeid'] = $company_info['id'].$location_info['id'];
					$data['serverid'] = $order_info['cashier_id'];
					$data['table'] = $order_info['tablename'];
					$data['guest'] = $order_info['guests'];
					$data['transactionid'] = $_REQUEST['order_id']."-".$_REQUEST['check'];
					$data['totalamount'] = $order_info['payment']['amount'];
					if( $data_name=='utterly_delici' || stristr($data_name,"loyaltree"))
						error_log("receipt append Results: ".print_r($data,1));
					$get_contents = lavu_query("SELECT `order_contents`.`item_id` AS `id`, `menu_items`.`category_id` AS `category_id`, `order_contents`.`quantity` AS `quantity`, `order_contents`.`subtotal` AS `price`, (`order_contents`.`forced_modifiers_price` + `order_contents`.`modify_price`) AS `modifiers`, (`order_contents`.`discount_amount` + `order_contents`.`idiscount_amount`) AS `discount`, `order_contents`.`itax` AS `itax`, `order_contents`.`server_time` AS `time` FROM `order_contents` LEFT JOIN `menu_items` ON `menu_items`.`id` = `order_contents`.`item_id` WHERE `order_contents`.`item` != 'SENDPOINT' AND `order_contents`.`quantity` > 0 AND `order_contents`.`loc_id` = '[1]' AND `order_contents`.`order_id` = '[2]' AND (`order_contents`.`check` = '[3]' OR (`order_contents`.`check` = '0' AND '[3]' = '1'))", $_REQUEST['loc_id'], $_REQUEST['order_id'], $_REQUEST['check']);
					if (mysqli_num_rows($get_contents) > 0) {				
						$items = array();
						while ($contents_info = mysqli_fetch_assoc($get_contents)) {
							$items[] = $contents_info;
						}
						
						$get_payments = lavu_query("SELECT `id` AS `paymentid`, `total_collected` AS `amount`, `voided` AS `void`, `pay_type` AS `type`, `server_time` AS `paymenttime` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND `check` = '[3]' AND `action` = 'Sale'", $_REQUEST['loc_id'], $_REQUEST['order_id'], $_REQUEST['check']);
						$payments = array();
						while ($payments_info = mysqli_fetch_assoc($get_payments)) {
							$payments_info['current'] = "1";
							$payments[] = $payments_info;
						}
	
						$response = LoyalTree::purchase($data, $items, $payments);
						echo cleanJSON($response);
						if (!strstr($response, '"json_status":"error"')) $save_response = lavu_query("UPDATE `split_check_details` SET `loyaltree_info` = '[1]' WHERE `id` = '[2]'", $response, $order_info['scd_id']);
						//mail("richard@greenkeyconcepts.com","LOYALTREE purchase response" , $response);
					}
				}
			}	
		}

		lavu_exit();
	}

?>