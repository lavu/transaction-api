<?php

	require_once("../info_inc.php");
	$pack_id = reqvar("pack_id", "");
	$active_language_pack = getWebViewLanguagepack($pack_id, $loc_id);

?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Tip Out Wrapper</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}

.title_text {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 34px;
	color: #999999;
}
-->
		</style>
	</head>
	
	<body>
		<table width='574' height='539' border='0' cellpadding='0' cellspacing='0' style='background:URL(images/bg_popup_574x539.png); background-repeat:no-repeat; background-position:top center;'>
			<tr>
				<td width='21' height='69'>&nbsp;</td>
				<td width='530' height='69'>
					<table width='530' border='0' cellpadding='0' cellspacing='0'>
						<tr><td height='18' colspan='3'></td></tr>
						<tr>
							<td width='48' height='48'>&nbsp;</td>
							<td width='434' height='48' align='center'><span class='title_text'><?php echo speak("TIP OUT"); ?></span></td>
							<td width='48' height='48' align='center' style='background:URL(images/btn_popup_close2.png); background-repeat:no-repeat; background-position:center;' onclick='location = "_DO:cmd=close_overlay";'>&nbsp;</td>
						</tr>
						<tr><td colspan='3'><hr style='color:#F0F0EE; background-color:#F0F0EE;'></td></tr>
					</table>
				</td>
				<td width='23' height='69'>&nbsp;</td>
			</tr>
			<tr><td width='574' height='473' colspan='3'>&nbsp;</td></tr>
		</table>
	</body>
</html>
