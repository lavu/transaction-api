<?php
	require_once("../info_inc.php");
	$pack_id = reqvar("pack_id", "");
	$active_language_pack = getWebViewLanguagepack($pack_id, $loc_id);
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Tip Out</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
}

.sm_txt {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #111111;
}

.recommended {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #AAAAAA;
}

.tip_input {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 17px;
	color:#000066;
	border:none;
	background-image: URL(images/blank.png);
	-webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.1), 0 0 16px rgba(0,0,0,0.1);
	text-align:center;
}

.form_button {
	width: 113px;
	height: 37px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #8f8f8f;
	border: none;
	background-image: URL(images/btn_wow_113x37.png);
	background-repeat: none;
	background-color: transparent;
}

-->
		</style>
		
		<script language'javascript'>
			
			function setFieldValue(formid, fieldname, setto) {
			
				if(setto==''){
					setto='0';
				}	
				document.getElementById(formid).elements[fieldname].value = parseFloat(setto).toFixed(<?php echo $location_info['disable_decimal']; ?>);
			}
			
		</script>
<!-- Tipout toggle styles -->

<style>
input[type="checkbox"] { 
	position: absolute;
	opacity: 0;
}

/* Normal Track */
input[type="checkbox"].ios-switch + div {
	vertical-align: middle;
	width: 40px;	height: 20px;
	border: 1px solid rgba(0,0,0,.4);
	border-radius: 999px;
	background-color: rgba(0, 0, 0, 0.1);
	-webkit-transition-duration: .4s;
	-webkit-transition-property: background-color, box-shadow;
	box-shadow: inset 0 0 0 0px rgba(0,0,0,0.4);
	margin: 15px 1.2em 15px 2.5em;
}

/* Big Track */
input[type="checkbox"].bigswitch.ios-switch + div {
	width: 50px;	height: 25px;
}

/* Green Track */
input[type="checkbox"].green.ios-switch:checked + div {
	background-color: #00e359;
	border: 1px solid rgba(0, 162, 63,1);
	box-shadow: inset 0 0 0 10px rgba(0,227,89,1);
}

/* Normal Knob */
input[type="checkbox"].ios-switch + div > div {
	float: left;
	width: 18px; height: 18px;
	border-radius: inherit;
	background: #ffffff;
	-webkit-transition-timing-function: cubic-bezier(.54,1.85,.5,1);
	-webkit-transition-duration: 0.4s;
	-webkit-transition-property: transform, background-color, box-shadow;
	-moz-transition-timing-function: cubic-bezier(.54,1.85,.5,1);
	-moz-transition-duration: 0.4s;
	-moz-transition-property: transform, background-color;
	box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.3), 0px 0px 0 1px rgba(0, 0, 0, 0.4);
	pointer-events: none;
	margin-top: 1px;
	margin-left: 1px;
}


/* Big Knob */
input[type="checkbox"].bigswitch.ios-switch + div > div {
	width: 23px; height: 23px;
	margin-top: 1px;
}

/* Green Knob */
input[type="checkbox"].green.ios-switch:checked + div > div
{
        -webkit-transform: translate(25px, 0);
        -moz-transform: translate(25px, 0);
        transform: translate3d(25px, 0);
        box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.3), 0 0 0 1px rgba(0, 162, 63,1);
}


</style>		
	</head>

<?php

	function showAmount($amount, $sym=true) {
		global $location_info;

		$symbol = "";
		if ($sym) $symbol = $location_info['monitary_symbol'];
		$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
		if ($location_info['decimal_char'] != "") {
			$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
			$thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char']==".") ? "," : '.';
		}
		$thousands_sep = ",";
		if( !empty($thousands_char) ) {
			$thousands_sep = $thousands_char;
		}

		if ($location_info['left_or_right'] == "right") return number_format($amount, $precision, $decimal_char, $thousands_sep).$symbol;
		else return $symbol.number_format($amount, $precision, $decimal_char, $thousands_sep);
	}
	/*echo "<pre>";
	print_r($_POST);
	echo "</pre>";*/
	
	//$step = (isset($_REQUEST['step']))?$_REQUEST['step']:1;

	$display = "";
	$get_server_info = lavu_query("SELECT `users`.`l_name` AS `l_name`, `users`.`f_name` AS `f_name`, `users`.`employee_class` AS `primary_role`, `users`.`role_id` AS `roles`, `emp_classes`.`id` AS `class_id`, `emp_classes`.`title` AS `class_title`, `emp_classes`.`tipout_rules` AS `tipout_rules`, `emp_classes`.`enable_tip_sharing` AS `enable_sharing`, `emp_classes`.`tip_sharing_rule` AS `tip_sharing_rule`, `emp_classes`.`tip_sharing_period` AS `time_for_tip_sharing`,`emp_classes`.`tipout_break`,`tiprefund_rules`,`refund_type` FROM `users` LEFT JOIN `emp_classes` ON `emp_classes`.`id` = `users`.`employee_class` WHERE `users`.`id` = '[1]' LIMIT 1", $_REQUEST['server_id']);

	if (mysqli_num_rows($get_server_info) > 0) {
		$server_info = mysqli_fetch_assoc($get_server_info);
		$emp_class_id = $server_info['primary_role'];
		$device_time = (isset($_REQUEST['device_time']))?$_REQUEST['device_time']:date("Y-m-d H:i:s");
		if ($location_info['timezone'] != "") $device_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
		
		$date_info = get_use_date_end_date($location_info, $device_time);
		
		$todays_cash_tips = false;
		$check_todays_cash_tips = lavu_query("SELECT `id`, `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $_REQUEST['loc_id'], $_REQUEST['server_id'], $date_info[0]);
		if (mysqli_num_rows($check_todays_cash_tips) > 0) {
			$cash_tip_info = mysqli_fetch_assoc($check_todays_cash_tips);
			$todays_cash_tips = $cash_tip_info['value'];
		}
		$can_submit = false;

		$display .= "<table cellspacing='0' cellpadding='3'>";
		$display .= "<tr><td class='sm_txt' align='center' style='padding-top:0px'>Server: ".$server_info['f_name']." ".$server_info['l_name']."</td></tr>";

		$roles = explode(",", $server_info['roles']);
		if (count($roles) > 1) {
			$check_punch_for_active_role = lavu_query("SELECT `role_id` FROM `clock_punches` WHERE `_deleted` != '1' AND `server_id` = '[1]' AND `location_id` = '[2]' ORDER BY `time` DESC LIMIT 1",  $_REQUEST['server_id'], $_REQUEST['loc_id']);
			if (mysqli_num_rows($check_punch_for_active_role) > 0) {
				$p_info = mysqli_fetch_assoc($check_punch_for_active_role);
				if ($p_info['role_id'] != $server_info['primary_role']) {
					$get_class = lavu_query("SELECT `title`, `tipout_rules`,`enable_tip_sharing`, `tip_sharing_rule`, `tip_sharing_period`, `tipout_break`, `tiprefund_rules`, `refund_type` FROM `emp_classes` WHERE `id` = '[1]'", $p_info['role_id']);
					if (mysqli_num_rows($get_class) > 0) {
						$c_info = mysqli_fetch_assoc($get_class);
						$server_info['class_title'] = $c_info['title'];
						$server_info['tipout_rules'] = $c_info['tipout_rules'];
						$server_info['enable_sharing'] = $c_info['enable_tip_sharing'];
						$server_info['tip_sharing_rule'] = $c_info['tip_sharing_rule'];
						$server_info['time_for_tip_sharing'] = $c_info['tip_sharing_period'];
						$server_info['tipout_break'] = $c_info['tipout_break'];
						$server_info['tiprefund_rules'] = $c_info['tiprefund_rules'];
						$server_info['refund_type'] = $c_info['refund_type'];
						$server_info['class_id'] = $p_info['role_id'];
						$emp_class_id = $p_info['role_id'];
					}
				}
			}
		}
		
		if (empty($server_info['class_title'])) $display .= "<tr><td align='center'><br>Unable to retrieve employee class data...</td></tr>";
		else if (empty($server_info['tipout_rules'])) $display .= "<tr><td align='center'><br>No tipout rules defined for employee class: ".$server_info['class_title']."</td></tr>";
		else {			
			/*$get_totals = lavu_query("SELECT SUM(`subtotal` + `idiscount_amount`) AS `total_sales`, SUM(`cash_applied`) AS `cash_sales`, SUM(`card_gratuity` + `ag_card`) AS `card_tips` FROM `orders` WHERE `closed` >= '[1]' AND `closed` < '[2]' AND `location_id` = '[3]' AND `orders`.`server_id` = '[4]' AND `void` = '0'", $date_info[2], $date_info[3], $_REQUEST['loc_id'], $_REQUEST['server_id']);
			$totals = mysqli_fetch_assoc($get_totals);*/
			/****Refunds Data*****/
			//select order_id FROM `orders` WHERE `closed` >= '[1]' AND `closed` < '[2]' AND `location_id` = '[3]' AND `server_id` = '[4]'
			$refunds_query = lavu_query("select sum(`total_collected`) as refunded,sum(`tip_amount`) as tiprefund from cc_transactions where `cc_transactions`.`action`='Refund' and `order_id` in(select `orders`.`order_id` FROM `orders` LEFT JOIN `clock_punches` as cp ON `cp`.`server_id`= `orders`.`server_id`
			WHERE `orders`.`closed` >= '[1]' AND `orders`.`closed` < '[2]' AND `orders`.`location_id` = '[3]' AND `orders`.`server_id` = '[4]' AND `orders`.`closed` >= `cp`.`time` AND `orders`.`closed` < if(`cp`.`time_out` <> '', `cp`.`time_out`, '[2]') AND `cp`.`time` >= '[1]' AND `cp`.`time` < '[2]' AND `orders`.`void` = '0' AND `cp`.`role_id`= '[5]')", $date_info[2], $date_info[3], $_REQUEST['loc_id'], $_REQUEST['server_id'], $emp_class_id);
			if (mysqli_num_rows($refunds_query) > 0) {
				$refunds = mysqli_fetch_assoc($refunds_query);
				$refundcard = $refunds['refunded'];
				$refundtips = $refunds['tiprefund'];
			}
				//"SELECT `idiscount_amount`, `subtotal`, `cash_applied`, `cash_tip`,`gratuity`, `card_gratuity`,`card_paid` FROM `orders` WHERE `closed` >= '[1]' AND `closed` < '[2]' AND `location_id` = '[3]' AND `server_id` = '[4]' AND `void` = '0'"
			$order_query = lavu_query("select `orders`.`idiscount_amount`, `orders`.`subtotal`, `orders`.`cash_applied`, `orders`.`cash_tip`, `orders`.`gratuity`, `orders`.`card_gratuity`, `orders`.`card_paid` from `orders` 	LEFT JOIN `clock_punches` as cp on `cp`.`server_id`= `orders`.`server_id` where orders.`location_id` = '[3]' AND orders.`server_id` = '[4]' AND orders.`closed` >= '[1]' AND orders.`closed` < '[2]' AND orders.closed >= cp.time and orders.closed < if(cp.time_out <> '', cp.time_out, '[2]')	AND cp.`time` >= '[1]' AND cp.`time` < '[2]' AND orders.`void` = '0' AND cp.role_id= '[5]' ", $date_info[2], $date_info[3], $_REQUEST['loc_id'], $_REQUEST['server_id'], $emp_class_id);
			$order_count = mysqli_num_rows($order_query);
			if ($order_count > 0) {
				
				$can_submit = true;
				
				$totals = array();
				$totals['total_sales'] = 0;
				$totals['cash_sales'] = 0;
				$totals['card_tips'] = 0;
				$totals['card_sales'] = 0;
				$totals['cash_tip'] = 0;
				while ($order_read = mysqli_fetch_assoc($order_query)) {
					$totals['total_sales'] += ($order_read['subtotal'] + $order_read['idiscount_amount'] - $order_read['itax']);
					$totals['cash_sales'] += $order_read['cash_applied'];
					$totals['card_sales'] += $order_read['card_paid'];
					$totals['cash_tip'] += $order_read['cash_tip'];
					
					$use_ag = $order_read['gratuity'];
					$ag_cash = 0;
					$ag_card = 0;
					$ag_other = 0;
					if($order_read['cash_applied'] >= $use_ag) $ag_cash = $use_ag;
					else {
						$ag_cash = $order_read['cash_applied'];
						$use_ag -= $ag_cash * 1;
						if($order_read['card_paid'] >= $use_ag) $ag_card = $use_ag;
						else {
							$ag_card = $order_read['card_paid'];
							$use_ag -= $ag_card * 1;
							$use_other_paid = ($order_read['gift_certificate'] + $order_read['alt_paid']);
							if ($use_other_paid >= $use_ag) $ag_other = $use_ag;
							else {
								$ag_other = $use_other_paid;
								$use_ag -= $ag_other * 1;
							}
						}
					}

					$totals['card_tips'] += ($order_read['card_gratuity'] + $ag_card);
				}
	
				//$estimated = " (Est)";
				//$cash_tips = (0.18 * $totals['cash_sales']);
				$estimated = "";
				$cash_tips = $totals['cash_tip'];
				if ((float)$_REQUEST['cash_tips'] > 0) {
					$cash_tips += $_REQUEST['cash_tips'] - $todays_cash_tips;
				}
			
				$display .= "<tr><td class='sm_txt' align='center'>Employee Class: ".$server_info['class_title']."<br><br></td></tr>";
				//$display .= "<tr><td align='center'>&nbsp;</td></tr>";
				$display .= "<tr><td align='center'>";
				$display .= "<table cellspacing='0' cellpadding='3'>";
				$display .= "<tr><td align='right'><span class='sm_txt'>Total Sales:</span></td><td align='left'>".showAmount($totals['total_sales'])."</td><tr>";
				$display .= "<tr><td align='right'><span class='sm_txt'>Cash Sales:</span></td><td align='left'>".showAmount($totals['cash_sales'])."</td><tr>";
				$display .= "<tr><td align='right'><span class='sm_txt'>Card Sales:</span></td><td align='left'>".showAmount($totals['card_sales'])."</td><tr>";
				$display .= "<tr><td align='right'><span class='sm_txt'>Cash Tips".$estimated.":</span></td><td align='left'>".showAmount($cash_tips)."</td><tr>";
				$display .= "<tr><td align='right'><span class='sm_txt'>Card Tips:</span></td><td align='left'>".showAmount($totals['card_tips'])."</td><tr>";
				$display .= "<tr><td align='right'><span class='sm_txt'>Card Refunds:</span></td><td align='left'>".showAmount($refundcard)."</td><tr>";
				$display .= "<tr><td align='right'><span class='sm_txt'>Card Tips Refunds:</span></td><td align='left'>".showAmount($refundtips)."</td><tr>";
				$display .= "</table>";
				$display .= "</td></tr>";
				$display .= "<tr><td>&nbsp;</td></tr>";
				$total_tip = $cash_tips + $totals['card_tips'];
				
				
				$display .= "<tr><td align='center'>";
				$display .= "<form id='go_form' action='' method='POST'>";
				$display .= "<input type='hidden' id='total_tip' name='total_tip' value= '".$total_tip."' >";
				$display .= "<input type='hidden' name = 'server_name' id ='server_name' value='".$server_info['f_name']." ".$server_info['l_name']."' >";
				$display .= "<input type='hidden' name = 'server_id' id ='server_id' value='".$_REQUEST['server_id']."' >";
				$display .= "<input type='hidden' name = 'class_name' id ='class_name' value='".$server_info['class_title']."' >";
				$display .= "<input type='hidden' name = 'class_id' id ='class_id' value='".$server_info['class_id']."' >";
				$display .= "<input type='hidden' name = 'tip_sharing_rule' id ='tip_sharing_rule' value='".$server_info['tip_sharing_rule']."' >";
				$display .= "<input type='hidden' name = 'time_for_tip_sharing' id ='time_for_tip_sharing' value='".$server_info['time_for_tip_sharing']."' >";
				$display .= "<input type='hidden' name = 'cash_tip' id ='cash_tip' value='".$cash_tips."' >";
				$display .= "<input type='hidden' name = 'card_tip' id ='card_tip' value='".$totals['card_tips']."' >";
				$display .= "<input type='hidden' name = 'enable_sharing' id ='enable_sharing' value='".$server_info['enable_sharing']."' >";
				$display .= "<table cellspacing='0' cellpadding='5'>";
			
				$emp_classes = $emp_classes_break = array();
				$get_employee_classes = lavu_query("SELECT DISTINCT `ec`.`id`, `ec`.`title`, `ec`.`tipout_break` FROM `emp_classes` AS `ec` LEFT JOIN `clock_punches` AS `cp` ON `cp`.`role_id` = `ec`.`id` WHERE `cp`.`punched_out`='0'");
				if (mysqli_num_rows($get_employee_classes) > 0) {
					while ($info = mysqli_fetch_assoc($get_employee_classes)) {
						$emp_classes[$info['id']] = trim($info['title']);
					}
				}
				$refundpercent = $server_info["tiprefund_rules"];
				$refund_type = $server_info["refund_type"];
				$tipout_rules = explode(",", $server_info['tipout_rules']);
				$tipout_break = explode(",", $server_info['tipout_break']);
				$refundcalc = ($refund_type=='Card Sales') ? $totals['card_sales'] : $totals['card_tips'];
				$labelTips = ($refund_type=='Card Sales') ? "Card Sales" : "Card Tips";
				$set_card_refund = ($refundcalc * ($refundpercent / 100));
				$tipout_ids = '';
				$valid_count = 0;
				$decimalPlaces = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
				foreach ($tipout_rules as $tipskey=>$tr) {
					if (!empty($tr)) {
						$rules = explode("|", $tr);
						$cid1 = (array_key_exists($rules[2], $emp_classes)) ? $rules[2] : 0;
						$cid2 = (array_key_exists($rules[0], $emp_classes)) ? $rules[0] : 0;
						$p = $rules[1];
						$breakrole = $tipout_break[$tipskey];
						$typeofcalc = $rules['3']; //tips or sales
						$subtypeofcalc = $rules['4']; //totaltips/card/cash or totalsales/supergroup 
						$tipscalc = '';
						if($subtypeofcalc == 'totaltips'){
							$tipscalc = $cash_tips + $totals['card_tips'];
							$tipscash = '0.00';
							$tipscard = '0.00';
						}
						else if($subtypeofcalc == 'cashtipsonly'){
							$tipscash = $cash_tips;
							$tipscard = '0.00';
						}
						else if($subtypeofcalc == 'cardtipsonly'){
							$tipscalc = $totals['card_tips'];
						}
						elseif($subtypeofcalc == 'totalsales'){
							$tipscalc = $totals['total_sales'];
						}
						else if($subtypeofcalc == 'supergroups'){
						    //$rules = rtrim($rules,',');
							$supr_ids = array_slice($rules,'5');
							if (!empty($supr_ids)) {
							$sg_id = rtrim(implode(',',$supr_ids),',');
							//"select if(count(*) > 0, concat(format(cast(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) )as decimal(30,5)),2)), '0.00') as gross from `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `super_groups` ON IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id) = `super_groups`.`id` where `super_groups`.`id` in (".$sg_id.") AND orders.closed >= '[1]' and orders.currency_type = '0' and orders.closed < '[2]' and orders.void = '0' and order_contents.item != 'SENDPOINT' and order_contents.loc_id = '[3]' and `orders`.`server_id`='[4]' group by super_groups.title order by (sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) )*1) desc"
							$supergroupquery = lavu_query("select if(count(*) > 0, concat(format(cast(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) )as decimal(30,5)),2)), '0.00') as gross from `orders` LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `super_groups` ON IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id) = `super_groups`.`id` LEFT JOIN `clock_punches` as cp on `cp`.`server_id`= `orders`.`server_id` where `super_groups`.`id` in (".$sg_id.") AND orders.closed >= '[1]' and orders.currency_type = '0' and orders.closed < '[2]' and orders.void = '0' and order_contents.item != 'SENDPOINT' and order_contents.loc_id = '[3]' and `orders`.`server_id`='[4]' AND `orders`.closed >= cp.time and `orders`.closed < if(cp.time_out <> '', cp.time_out, '[2]') AND cp.`time` >= '[1]' AND cp.`time` < '[2]'  AND cp.role_id = '[5]' group by super_groups.title order by (sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0',order_contents.itax, 0) )*1) desc",$date_info[2], $date_info[3], $_REQUEST['loc_id'], $_REQUEST['server_id'], $emp_class_id);
							if (mysqli_num_rows($supergroupquery) > 0) {
								while ($supergroupinfo = mysqli_fetch_assoc($supergroupquery)) {
									$tipscalc += $supergroupinfo['gross'];
								}
							}
							}
						}
						if (!empty($p) && (!empty($cid1) || !empty($cid2))) {
							$valid_count++;
							if (empty($cid1)) $cid1 = 0;
							if (empty($cid2)) $cid2 = 0;
							//if (isset($_POST['posted']) && ($valid_count == ($step - 1))) {
							if (isset($_POST['posted'])){
								$postdata = $_POST['emp'];
								//echo "<pre>";print_r($postdata);echo "</pre>";
								if(count($postdata['enable'])>0){
									//foreach($postdata['enable'] as $key=>$val){
											$save_cash_tipout = $postdata['cash_tipout'][$tipskey];
											$save_card_tipout = $postdata['card_tipout'][$tipskey];
											$save_sales_tipout = $postdata['sales_tipout'][$tipskey];
											$supergroup_id = '';
											//print_R($postdata);
											$q_fields = "";
											$q_values = "";
											$q_update = "";
											$vars = array();
											if(!empty($postdata['supergroups_used'][$tipskey])){
												$vars['supergroups_used'] = json_encode($postdata['supergroups_used'][$tipskey]);
											}
											$vars['percentage'] = number_format(($p / 100), 3);
											$cash_tip = $save_cash_tipout;
											$cash_tip = str_replace(",",".",$cash_tip);
											$card_tip = $save_card_tipout;
											$card_tip = str_replace(",",".",$card_tip);
											$vars['value'] = $cash_tip;//showAmount($save_cash_tipout, false);
											$vars['value2'] = $card_tip;//showAmount($save_card_tipout, false);
											
											if($subtypeofcalc == 'totaltips'){
												$vars['cash_percentage'] = number_format(($save_cash_tipout / $cash_tips + $totals['card_tips']), 3);
												$vars['card_percentage'] = number_format(($save_card_tipout / $cash_tips + $totals['card_tips']), 3);
											}
											else if($subtypeofcalc == 'cashtipsonly'){
												$vars['cash_percentage'] = number_format(($save_cash_tipout / $cash_tips), 3);
												$vars['card_percentage'] = '0.00';
											}
											else if($subtypeofcalc == 'cardtipsonly'){
												$vars['cash_percentage'] = '0.00';
												$vars['card_percentage'] = number_format(($save_card_tipout / $totals['card_tips']), 3);
											}
											$vars['cash_tips'] = showAmount($cash_tips, false);
											$vars['card_tips'] = showAmount($totals['card_tips'], false);
											$vars['type'] = $emp_classes[$cid1];
											$vars['type2'] = $emp_classes[$cid2];
											$vars['emp_class_id'] = $emp_class_id;
											
											$sale_tipout = $save_sales_tipout;
											$sale_tipout = str_replace(",",".",$sale_tipout);
											$vars['sales_tipout'] = $sale_tipout;//showAmount($save_sales_tipout,false);
											$vars['breakdown_type'] = $breakrole;
											$vars['breakdown_detail'] = '';
											$keys = array_keys($vars);
											foreach ($keys as $key) {
													if ($q_update != "") $q_update .= ", ";
													$q_update .= "`$key` = '[$key]'";
											}
											$vars['loc_id'] = $_REQUEST['loc_id'];
											$vars['typeid'] = $cid1;
											$vars['typeid2'] = $cid2;
											$vars['server_id'] = $_REQUEST['server_id'];
											$vars['date'] = $date_info[0];
											if ($vars['value'] != "" && $vars['value2'] != "") {
												$final_amount = $vars['value'] + $vars['value2'];//($_POST['total_tip']*$p)/100;
											} else {
												$final_amount = $vars['sales_tipout'];
											}
											if ($cid1 != 0 && $cid2 != 0) {
												$final_amount = $final_amount/2;
											}

											$userdetail=array();
											$classArr = array($cid1,$cid2);
											if ($breakrole == "evenly") {
												foreach ($classArr as $classId) {
													$user_query = lavu_query("SELECT `users`.`id`,`users`.`f_name`,`users`.`l_name`,`clock_punches`.`time`,`clock_punches`.`hours`,`clock_punches`.`time_out` FROM `users` JOIN `clock_punches` ON `users`.`id` = `clock_punches`.`server_id` WHERE `users`.`_deleted` = 0 AND `clock_punches`.`role_id` in ($classId) AND `clock_punches`.`time` >= '".$date_info[2]."' AND `clock_punches`.`time` < '".$date_info[3]."' ");
													$user_count = mysqli_num_rows($user_query);
													$amount =  $final_amount/$user_count;
													while ($user_info = mysqli_fetch_assoc($user_query)) {
														$userdetail[]= array(
																'userid' => $user_info['id'],
																'username' => $user_info['f_name']." ".$user_info['l_name'],
																'amount' => round($amount,2));
													}
											
												}
												$breakdown_detail = array('breakdownType' => $breakrole, 'totalAmount'=>$_POST['total_tip'], 'userDetail' => $userdetail);
												$vars['breakdown_detail'] = json_encode($breakdown_detail);
											} else {
												$userBreakUpArr = array();
												$hrs = 0;
												foreach ($classArr as $classId) {
													$user_query = lavu_query("SELECT `users`.`id`,`users`.`f_name`,`users`.`l_name`,`clock_punches`.`time`,`clock_punches`.`hours`,`clock_punches`.`time_out` FROM `users` JOIN `clock_punches` ON `users`.`id` = `clock_punches`.`server_id` WHERE `users`.`_deleted` = 0 AND `clock_punches`.`role_id` in ($classId) AND `clock_punches`.`time` >= '".$date_info[2]."' AND `clock_punches`.`time` < '".$date_info[3]."' ");
													$user_count = mysqli_num_rows($user_query);
													$userInfo = array();
													$hrs = 0;
													while ($row = mysqli_fetch_assoc($user_query)) {
														$userid = $row['id'];
														if ($row['time_out'] != "") {
															$hrs += $row['hours'];
														} else {
															$inTime = $row['time'];
															$timezone = locationSetting("timezone");
															if (! empty($timezone)) {
																$today  = DateTimeForTimeZone($timezone);
															}
															$inTime= strtotime($inTime);
															$today= strtotime($today);
															$userHours = round(abs($today-$inTime)/60/60, $decimalPlaces);
															$hrs += $userHours;
															$row['hours'] = $userHours;
														}
														if (!isset($userInfo[$userid])) {
															$userInfo[$userid]['id'] = $row['id'];
															$userInfo[$userid]['f_name'] = $row['f_name'];
															$userInfo[$userid]['l_name'] = $row['l_name'];
															$userInfo[$userid]['hours'] = $row['hours'];
														} else {
															$userInfo[$userid]['hours'] = $userInfo[$userid]['hours'] + $row['hours'];
														}
													}
													$perHours = $final_amount/$hrs;
													$userdetailHour = array();
													foreach ($userInfo as $hkey => $hvals) {
														$userdetailHour[]= array(
																'userid' => $hvals['id'],
																'username' => $hvals['f_name']." ".$hvals['l_name'],
																'amount' => round($hvals['hours']*$perHours,2));
													}
													if (!empty($userBreakUpArr) && (count($userBreakUpArr)>0)) {
														$userBreakUpArr = array_merge($userBreakUpArr,$userdetailHour);
													} else {
														$userBreakUpArr = $userdetailHour;
													}
											
												}
												$breakdown_detail = array('breakdownType' => $breakrole, 'totalAmount'=>$_POST['total_tip'], 'userDetail' => $userBreakUpArr);
												$vars['breakdown_detail'] = json_encode($breakdown_detail);
											}
											$keys = array_keys($vars);
											foreach ($keys as $key) {
													if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
													$q_fields .= "`$key`";
													$q_values .= "'[$key]'";
											}
											$check = lavu_query("SELECT `id` FROM `tipouts` WHERE `loc_id` = '[loc_id]' AND `typeid` = '[typeid]' AND `typeid2` = '[typeid2]' AND `server_id` = '[server_id]' AND  `date` = '[date]' AND `emp_class_id`='[emp_class_id]'", $vars);
											if (mysqli_num_rows($check) > 0) {
												$info = mysqli_fetch_assoc($check);
												$vars['id'] = $info['id'];
												$tipout_ids .= $vars['id'].",";
												$update = lavu_query("UPDATE `tipouts` SET $q_update WHERE `id` = '[id]'", $vars);
											}
											else{ 
												$create = lavu_query("INSERT INTO `tipouts` ($q_fields) VALUES ($q_values)", $vars);
												$tipout_ids .= lavu_insert_id().",";
											}
											$empIds = lavu_query("select `id` from `emp_classes` where `_deleted` = 0 ");
											while ($empid = mysqli_fetch_assoc($empIds)) {
											    $ids .= $empid['id'].",";
											}
											$ids = rtrim($ids, ",");
											$today_timestamp = date("Y-m-d");
											$empInfo_query = lavu_query("select * from `emp_classes_temp` where `_deleted` = 0 AND `class_updated_date` ='".$today_timestamp."' and `emp_class_id` in ($ids)");
											
											while ($empData = mysqli_fetch_assoc($empInfo_query)) {
												$update_emp_class = "UPDATE `emp_classes` SET `title` = '".$empData['title']."' ,`type` = '".$empData['type']."', `_deleted` = '".$empData['_deleted']."',`tipout_rules` = '".$empData['tipout_rules']."' ,`tiprefund_rules` = '".$empData['tiprefund_rules']."', `refund_type` = '".$empData['refund_type']."', `enable_tip_sharing` = '".$empData['enable_tip_sharing']."',`tip_sharing_rule` = '".$empData['tip_sharing_rule']."', `include_server_owes` = '".$empData['include_server_owes']."', `tip_sharing_period` = '".$empData['tip_sharing_period']."', `tipout_break` = '".$empData['tipout_break']."' where `id` = '".$empData['emp_class_id']."'";
											    lavu_query($update_emp_class);
											    
											    $update_emp_temp_class = "UPDATE `emp_classes_temp` SET `_deleted` = 1 where `emp_class_id` = '".$empData['emp_class_id']."' AND `class_updated_date`  = '".$today_timestamp."'";
											    lavu_query($update_emp_temp_class);
											}
									//}
								}
						}
						

							//if ($valid_count == $step) {
								$class1 = $emp_classes[$cid1];
								$class2 = $emp_classes[$cid2];
								$slash = "";
								if (!empty($class1) && !empty($class2)) $slash = " / ";
								
								$set_sales_tipout = ($tipscalc * ($p / 100));
								
								if($subtypeofcalc == 'totaltips'){
									$set_cash_tipout = ($cash_tips ) * ($p / 100);
									$set_card_tipout = ( $totals['card_tips']) * ($p / 100);
								}
								else if($subtypeofcalc == 'cashtipsonly'){
									$set_cash_tipout = ($cash_tips * ($p / 100));
									$set_card_tipout = '0.00';
								}
								else if($subtypeofcalc == 'cardtipsonly'){
									$set_cash_tipout = '0.00';
									$set_card_tipout = ($totals['card_tips'] * ($p / 100));
								}
								
								
								$check_saved = lavu_query("SELECT `value`, `value2`,`sales_tipout` FROM `tipouts` WHERE `loc_id` = '[1]' AND `typeid` = '[2]' AND `typeid2` = '[3]' AND `server_id` = '[4]' AND `date` = '[5]' AND `emp_class_id`='[6]' ORDER BY `id` DESC LIMIT 1", $_REQUEST['loc_id'], $cid1, $cid2, $_REQUEST['server_id'], $date_info[0],$emp_class_id);
								if (mysqli_num_rows($check_saved) > 0) {
									$info = mysqli_fetch_assoc($check_saved);
									$set_cash_tipout = $info['value'];
									$set_card_tipout = $info['value2'];
									$set_sales_tipout = $info['sales_tipout'];
								}
																
								$display .= "<tr><td align='center' colspan='3' style='padding:0px 5px 5px 5px;'>".$class1.$slash.$class2." (".$p."%)</td><td><label><input type='checkbox' name = 'emp[enable][".$tipskey."]' id='emp[enable][".$tipskey."]' class=\"ios-switch green  bigswitch\" checked /><div><div></div></div></label></td></tr>";
								if($typeofcalc == 'sales'){
									$display .= ($subtypeofcalc=='supergroups')? "<tr><td align='right'><span class='sm_txt'>Super Group Sales:</span></td><td align='left'>".showAmount(($tipscalc ))."</td></tr>": "";
									
									$supergrphidden = ($subtypeofcalc=='supergroups')? "<input type='hidden' readonly name='emp[supergroups_used][".$tipskey."]' value='".json_encode($supr_ids)."' />":"";
									$display .= "<tr><td align='right'><span class='sm_txt'>Sales Tipout:</span></td><td align='left'>".$supergrphidden."<input class='tip_input' name='emp[sales_tipout][".$tipskey."]' type='text' size='8' value='".showAmount($set_sales_tipout, false)."' readonly ontouchstart='window.location = \"_DO:cmd=input_number&for_form=go_form&for_field=emp[sales_tipout][".$tipskey."]&title=Sales Tipout\";'></td><td class='recommended' align='left'>".showAmount(($tipscalc * ($p / 100)), false)."</td></tr>";
								}
								else{
									
									if($subtypeofcalc == 'totaltips'){
										$display .= "<tr><td align='right'><span class='sm_txt'>Cash Tips:</span></td><td align='left'><input class='tip_input' name='emp[cash_tipout][".$tipskey."]' type='text' size='8' value='".showAmount($set_cash_tipout, false)."' readonly ontouchstart='window.location = \"_DO:cmd=input_number&for_form=go_form&for_field=emp[cash_tipout][".$tipskey."]&title=Cash Tips\";'></td><td class='recommended' align='left'>".showAmount(($cash_tips * ($p / 100)), false)."</td></tr>";
										$display .= "<tr><td align='right'><span class='sm_txt'>Card Tips:</span></td><td align='left'><input class='tip_input' name='emp[card_tipout][".$tipskey."]' type='text' size='8' value='".showAmount($set_card_tipout, false)."' readonly ontouchstart='window.location = \"_DO:cmd=input_number&for_form=go_form&for_field=emp[card_tipout][".$tipskey."]&title=Card Tips\";'></td><td class='recommended' align='left'>".showAmount(($totals['card_tips'] * ($p / 100)), false)."</td></tr>";
									}
									else if($subtypeofcalc == 'cashtipsonly'){
										$display .= "<tr><td align='right'><span class='sm_txt'>Cash Tips:</span></td><td align='left'><input class='tip_input' name='emp[cash_tipout][".$tipskey."]' type='text' size='8' value='".showAmount($set_cash_tipout, false)."' readonly ontouchstart='window.location = \"_DO:cmd=input_number&for_form=go_form&for_field=emp[cash_tipout][".$tipskey."]&title=Cash Tips\";'></td><td class='recommended' align='left'>".showAmount(($cash_tips * ($p / 100)), false)."</td></tr>";
									}
									else if($subtypeofcalc == 'cardtipsonly'){
										$display .= "<tr><td align='right'><span class='sm_txt'>Card Tips:</span></td><td align='left'><input class='tip_input' name='emp[card_tipout][".$tipskey."]' type='text' size='8' value='".showAmount($set_card_tipout, false)."' readonly ontouchstart='window.location = \"_DO:cmd=input_number&for_form=go_form&for_field=emp[card_tipout][".$tipskey."]&title=Card Tips\";'></td><td class='recommended' align='left'>".showAmount(($totals['card_tips'] * ($p / 100)), false)."</td></tr>";
									}
									
									
								}
								$display .= "<tr><td colspan='3'><input class='tip_input' name='emp[breakdown_type][".$tipskey."]' type='hidden' size='8' value='".$breakrole."'>&nbsp;</td></tr>";
							//}
						}
					}
				}
				/*******Refund Insert*******/
				if (isset($_POST['enable_sharing']) && $_POST['enable_sharing'] == 1) {
					shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $_REQUEST[server_id] $_REQUEST[loc_id] $_REQUEST[cc] > /dev/null 2>/dev/null &");
				}

				if(!empty($_POST['tiprefund_enable'])){
					//echo "Total=====".$_POST['tip_refund']."/".$refundcalc;
					$r_vars = array();
					$refund_amount = $_POST['tip_refund'];
					$Refundamount = str_replace(",",".",$refund_amount);
					$r_vars['amount'] =$Refundamount; //showAmount($_POST['tip_refund'], false);
					$r_vars['refund_percentage'] = number_format($_POST['tip_refund']/$refundcalc, 3);
					$r_vars['created_date'] = $date_info[0];
				//	$r_vars['tipout_ids'] = rtrim($tipout_ids,',');
					$r_vars['emp_class_id'] = $emp_class_id;
					$r_vars['loc_id'] = $_REQUEST['loc_id'];
					$r_vars['server_id'] = $_REQUEST['server_id'];
					$r_keys = array_keys($r_vars);
					$q_fields = '';
					$q_values = '';
					foreach ($r_keys as $rkey) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$rkey`";
						$q_values .= "'[$rkey]'";
					}
					$q_update = '';
					foreach ($r_keys as $rkey) {
						if ($q_update != "") $q_update .= ", ";
						$q_update .= "`$rkey` = '[$rkey]'";
					}
					
					$updateOrInsert = 0;
					$check = lavu_query("SELECT `id`,`emp_class_id` FROM `tipout_refunds` WHERE `emp_class_id`=[1] and `created_date` = '[2]' and `server_id` = '[3]'", $emp_class_id, $date_info[0], $_REQUEST['server_id']);
					if (mysqli_num_rows($check) > 0) {
						$info = mysqli_fetch_assoc($check);
						$r_vars['id'] = $info['id'];
						$update = lavu_query("UPDATE `tipout_refunds` SET $q_update WHERE `id` = '[id]'", $r_vars);
						$updateOrInsert = 1;
					}
					else{
						lavu_query("INSERT INTO `tipout_refunds` ($q_fields) VALUES ($q_values)", $r_vars);
					}
				}
				if(isset($_POST['posted'])){
					echo "<script language='javascript'>window.location = '_DO:cmd=close_overlay&action=print_server_summary';</script>";
					exit();
				}
				//if ($step == "final") {
				/*if ($step == ($valid_count + 1)) {
					echo "<script language='javascript'>window.location = '_DO:cmd=close_overlay&action=print_server_summary';</script>";
					exit();
				}*/
				//echo $refundcalc ."--".$refundpercent;
				$check_saved = lavu_query("SELECT `amount` FROM `tipout_refunds` WHERE `emp_class_id`=[1] and `created_date` = '[2]' and `server_id` = '[3]' ORDER BY `id` DESC LIMIT 1",$emp_class_id,$date_info[0], $_REQUEST['server_id']);
				if (mysqli_num_rows($check_saved) > 0) {
					$info = mysqli_fetch_assoc($check_saved);
					$set_card_refund = $info['amount'];
				}
				//echo "Calcc-".$refundcalc ."--".$refundpercent;
				$display .= "<tr><td align='center' colspan='3' style='padding:0px 5px 5px 5px;'>Tip Refunds (".$refundpercent."%)</td><td><label><input type='checkbox' name = 'tiprefund_enable' id='tiprefund_enable' class=\"ios-switch green  bigswitch\" checked /><div><div></div></div></label></td></tr>
										<tr><td align='right'><span class='sm_txt'>".$labelTips.":</span></td><td align='left'><input class='tip_input' name='tip_refund' type='text' size='8' value='".showAmount($set_card_refund, false)."' readonly  ontouchstart='window.location = \"_DO:cmd=input_number&for_form=go_form&for_field=tip_refund&title=Card Tips\";'></td><td class='recommended' align='left'>".showAmount(($refundcalc * ($refundpercent / 100)), false)."</td></tr>";
				$back_button = "";
				/*if ($step!=1 && $valid_count>1) $back_button = "<input type='button' class='form_button' ontouchstart='document.getElementById(\"back_form\").submit();' value='<< Back'> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";*/
				$display .= "<tr><td colspan='3' align='center'>".$back_button."<input type='button' class='form_button' ontouchstart='document.getElementById(\"go_form\").submit();' value='Submit'></td></tr>";
				$hidden_fields = "";
				$hf_names = array("cc", "dn", "loc_id", "server_id", "server_name", "type", "lavu_lite");
				foreach ($hf_names as $hf) {
					$hidden_fields .= "<input type='hidden' name='".$hf."' value='".$_REQUEST[$hf]."'>";
				}
				
				$display .= "</table>";
				//$last_step = ($step - 1);
				//if ($step == $valid_count) $step = "final";
				//else $step++;
				//$step++;
				$display .= "<input type='hidden' name='step' value='".$step."'>";
				$display .= "<input type='hidden' name='posted' value='1'>";
				$display .= $hidden_fields;
				$display .= "</form>";
				/*if (!empty($back_button)) {
					$display .= "<form id='back_form' action='' method='POST'>";
					$display .= "<input type='hidden' name='step' value='".$last_step."'>";
					$display .= $hidden_fields;
					$display .= "</form>";
				}*/
				$display .= "</td></tr>";
								
			} else $display .= "<tr><td align='center'><br>".speak("No closed orders found for today").": ".$date_info[0]."</td></tr>";
		}
		
		if (!$can_submit) $display .= "<tr><td align='center'><br><br><input type='button' class='form_button' ontouchstart='window.location = \"_DO:cmd=close_overlay&action=print_server_summary\"' value='Continue'></td></tr>";
		$display .= "</table>";

	} else $display = "Error: Unable to retrieve employee data...";
?>

	<body>
		<table width='532' cellspacing='0' cellpadding='0'>
			<tr><td>&nbsp;</td></tr>
			<tr><td align='center'><?php echo $display; ?></td></tr>
			<tr><td>&nbsp;</td></tr>
		</table>
	</body>
</html>
