<?php

session_start();
require_once(__DIR__."/../info_inc.php");

$db_name = "poslavu_".$data_name."_db";

set_sessvar('locationid', $loc_id);
set_sessvar('admin_database', $db_name);
set_sessvar('admin_dataname', $data_name);
set_sessvar('admin_companyid', $company_info['id']);
set_sessvar('admin_company_name', $company_info['company_name']);

header("Location: https://".$_SERVER['HTTP_HOST']."/components/payment_extensions/eConduit/client_boarding.php");