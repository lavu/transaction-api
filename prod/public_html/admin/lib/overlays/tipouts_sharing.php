<?php
    /* This query is used to get employee class tipout rules and user details. */
	$get_server_info = lavu_query("SELECT `users`.`l_name` AS `l_name`, `users`.`f_name` AS `f_name`, `users`.`employee_class` AS `primary_role`, `users`.`role_id` AS `roles`, `emp_classes`.`id` AS `class_id`, `emp_classes`.`title` AS `class_title`, `emp_classes`.`tipout_rules` AS `tipout_rules`, `emp_classes`.`enable_tip_sharing` AS `enable_sharing`, `emp_classes`.`tip_sharing_rule` AS `tip_sharing_rule`, `emp_classes`.`tip_sharing_period` AS `time_for_tip_sharing`,`emp_classes`.`tipout_break`,`tiprefund_rules`,`refund_type` FROM `users` LEFT JOIN `emp_classes` ON `emp_classes`.`id` = `users`.`employee_class` WHERE `users`.`id` = '[1]' LIMIT 1", $_REQUEST['report_server_id']);

	if (mysqli_num_rows($get_server_info) > 0) {
		$server_info = mysqli_fetch_assoc($get_server_info);
		$emp_class_id = $server_info['primary_role'];
		$device_time = (isset($_REQUEST['device_time']))?$_REQUEST['device_time']:date("Y-m-d H:i:s");
		if ($location_info['timezone'] != "") {
			$device_time = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
		}
		$date_info = get_use_date_end_date($location_info, $device_time);

		/*To get cash tips of server */
		$todays_cash_tips = false;
		$check_todays_cash_tips = lavu_query("SELECT `id`, `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $_REQUEST['loc_id'], $_REQUEST['report_server_id'], $date_info[0]);
		if (mysqli_num_rows($check_todays_cash_tips) > 0) {
			$cash_tip_info = mysqli_fetch_assoc($check_todays_cash_tips);
			$todays_cash_tips = $cash_tip_info['value'];
		}
		/* To Check and get Active server information, its used forther calculation in tipsharing*/
		$roles = explode(",", $server_info['roles']);
		if (count($roles) > 1) {
			$check_punch_for_active_role = lavu_query("SELECT `role_id` FROM `clock_punches` WHERE `_deleted` != '1' AND `server_id` = '[1]' AND `location_id` = '[2]' ORDER BY `time` DESC LIMIT 1", $_REQUEST['report_server_id'], $_REQUEST['loc_id']);
			if (mysqli_num_rows($check_punch_for_active_role) > 0) {
				$p_info = mysqli_fetch_assoc($check_punch_for_active_role);
				if ($p_info['role_id'] != $server_info['primary_role']) {
					$get_class = lavu_query("SELECT `title`, `tipout_rules`,`enable_tip_sharing`, `tip_sharing_rule`, `tip_sharing_period`, `tipout_break`, `tiprefund_rules`, `refund_type` FROM `emp_classes` WHERE `id` = '[1]'", $p_info['role_id']);
					if (mysqli_num_rows($get_class) > 0) {
						$c_info = mysqli_fetch_assoc($get_class);
						$server_info['class_title'] = $c_info['title'];
						$server_info['tipout_rules'] = $c_info['tipout_rules'];
						$server_info['enable_sharing'] = $c_info['enable_tip_sharing'];
						$server_info['tip_sharing_rule'] = $c_info['tip_sharing_rule'];
						$server_info['time_for_tip_sharing'] = $c_info['tip_sharing_period'];
						$server_info['tipout_break'] = $c_info['tipout_break'];
						$server_info['tiprefund_rules'] = $c_info['tiprefund_rules'];
						$server_info['refund_type'] = $c_info['refund_type'];
						$server_info['class_id'] = $p_info['role_id'];
						$emp_class_id = $p_info['role_id'];
					}
				}
			}
		}

		/* Start TipSharing calculation process */
		if (!empty($server_info['class_title']) && !empty($server_info['tipout_rules'])) {
			/*To get Refunds Data*/
			$refunds_query = lavu_query("select sum(`total_collected`) as refunded,sum(`tip_amount`) as tiprefund from cc_transactions where `cc_transactions`.`action`='Refund' and `order_id` in(select `orders`.`order_id` FROM `orders` LEFT JOIN `clock_punches` as cp ON `cp`.`server_id`= `orders`.`server_id`
			WHERE `orders`.`closed` >= '[1]' AND `orders`.`closed` < '[2]' AND `orders`.`location_id` = '[3]' AND `orders`.`server_id` = '[4]' AND `orders`.`closed` >= `cp`.`time` AND `orders`.`closed` < if(`cp`.`time_out` <> '', `cp`.`time_out`, '[2]') AND `cp`.`time` >= '[1]' AND `cp`.`time` < '[2]' AND `orders`.`void` = '0' AND `cp`.`role_id`= '[5]')", $date_info[2], $date_info[3], $_REQUEST['loc_id'], $_REQUEST['report_server_id'], $emp_class_id);
			if (mysqli_num_rows($refunds_query) > 0) {
				$refunds = mysqli_fetch_assoc($refunds_query);
				$refundcard = $refunds['refunded'];
				$refundtips = $refunds['tiprefund'];
			}
			/*To get order details of server*/
			$order_query = lavu_query("select `orders`.`idiscount_amount`, `orders`.`subtotal`, `orders`.`cash_applied`, `orders`.`cash_tip`, `orders`.`gratuity`, `orders`.`card_gratuity`, `orders`.`card_paid` from `orders` 	LEFT JOIN `clock_punches` as cp on `cp`.`server_id`= `orders`.`server_id` where orders.`location_id` = '[3]' AND orders.`server_id` = '[4]' AND orders.`closed` >= '[1]' AND orders.`closed` < '[2]' AND orders.closed >= cp.time and orders.closed < if(cp.time_out <> '', cp.time_out, '[2]')	AND cp.`time` >= '[1]' AND cp.`time` < '[2]' AND orders.`void` = '0' AND cp.role_id= '[5]' ", $date_info[2], $date_info[3], $_REQUEST['loc_id'], $_REQUEST['report_server_id'], $emp_class_id);
			$order_count = mysqli_num_rows($order_query);
			if ($order_count > 0) {
				$totals = array();
				$totals['total_sales'] = 0;
				$totals['cash_sales'] = 0;
				$totals['card_tips'] = 0;
				$totals['card_sales'] = 0;
				$totals['cash_tip'] = 0;
				while ($order_read = mysqli_fetch_assoc($order_query)) {
					$totals['total_sales'] += ($order_read['subtotal'] + $order_read['idiscount_amount'] - $order_read['itax']);
					$totals['cash_sales'] += $order_read['cash_applied'];
					$totals['card_sales'] += $order_read['card_paid'];
					$totals['cash_tip'] += $order_read['cash_tip'];
					
					$use_ag = $order_read['gratuity'];
					$ag_cash = 0;
					$ag_card = 0;
					$ag_other = 0;
					if ($order_read['cash_applied'] >= $use_ag) {
						$ag_cash = $use_ag;
					} else {
						$ag_cash = $order_read['cash_applied'];
						$use_ag -= $ag_cash * 1;
						if($order_read['card_paid'] >= $use_ag) $ag_card = $use_ag;
						else {
							$ag_card = $order_read['card_paid'];
							$use_ag -= $ag_card * 1;
							$use_other_paid = ($order_read['gift_certificate'] + $order_read['alt_paid']);
							if ($use_other_paid >= $use_ag) {
								$ag_other = $use_ag;
							}
							else {
								$ag_other = $use_other_paid;
								$use_ag -= $ag_other * 1;
							}
						}
					}

					$totals['card_tips'] += ($order_read['card_gratuity'] + $ag_card);
				}

				$estimated = "";
				$cash_tips = $totals['cash_tip'];
				if ((float)$_REQUEST['cash_tips'] > 0) {
					$cash_tips += $_REQUEST['cash_tips'] - $todays_cash_tips;
				}

				$total_tip = $cash_tips + $totals['card_tips'];
				$emp_classes = $emp_classes_break = array();
				$get_employee_classes = lavu_query("SELECT `id`, `title`,`tipout_break` FROM `emp_classes`");
				if (mysqli_num_rows($get_employee_classes) > 0) {
					while ($info = mysqli_fetch_assoc($get_employee_classes)) {
						$emp_classes[$info['id']] = trim($info['title']);
					}
				}
				$refundpercent = $server_info["tiprefund_rules"];
				$refund_type = $server_info["refund_type"];
				$tipout_rules = explode(",", $server_info['tipout_rules']);
				$tipout_break = explode(",", $server_info['tipout_break']);
				$refundcalc = ($refund_type=='Card Sales') ? $totals['card_sales'] : $totals['card_tips'];
				$set_card_refund = ($refundcalc * ($refundpercent / 100));
				$tipout_ids = '';
				$valid_count = 0;
				/*Tipout / Tip sharing calculation on the basis of rules. */
				foreach ($tipout_rules as $tipskey=>$tr) {
					if (!empty($tr)) {
						$rules = explode("|", $tr);
						$cid1 = $rules[2];
						$cid2 = $rules[0];
						$p = $rules[1];
						$breakrole = $tipout_break[$tipskey];
						$typeofcalc = $rules['3']; //tips or sales
						$subtypeofcalc = $rules['4']; //totaltips/card/cash or totalsales/supergroup
						$tipscalc = '';
						if ($subtypeofcalc == 'totaltips') {
							$tipscalc = $cash_tips + $totals['card_tips'];
							$tipscash = '0.00';
							$tipscard = '0.00';
						} else if($subtypeofcalc == 'cashtipsonly') {
							$tipscash = $cash_tips;
							$tipscard = '0.00';
						} else if($subtypeofcalc == 'cardtipsonly') {
							$tipscalc = $totals['card_tips'];
						} elseif($subtypeofcalc == 'totalsales') {
							$tipscalc = $totals['total_sales'];
						} else if($subtypeofcalc == 'supergroups') {
							$supr_ids = array_slice($rules, '5');
							if (!empty($supr_ids)) {
							$sg_id = rtrim(implode(',', $supr_ids), ',');
							$supergroupquery = lavu_query("select if(count(*) > 0, concat(format(cast(sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) )as decimal(30,5)),2)), '0.00') as gross from `orders` LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id` AND `orders`.`location_id` = `order_contents`.`loc_id` LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id` LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id` LEFT JOIN `super_groups` ON IF (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id) = `super_groups`.`id` LEFT JOIN `clock_punches` as cp on `cp`.`server_id`= `orders`.`server_id` where `super_groups`.`id` in (".$sg_id.") AND orders.closed >= '[1]' and orders.currency_type = '0' and orders.closed < '[2]' and orders.void = '0' and order_contents.item != 'SENDPOINT' and order_contents.loc_id = '[3]' and `orders`.`server_id`='[4]' AND `orders`.closed >= cp.time and `orders`.closed < if(cp.time_out <> '', cp.time_out, '[2]') AND cp.`time` >= '[1]' AND cp.`time` < '[2]' AND cp.role_id = '[5]' group by super_groups.title order by (sum((order_contents.price + order_contents.modify_price + order_contents.forced_modifiers_price)*order_contents.quantity - if( order_contents.quantity > '0', order_contents.itax, 0) )*1) desc", $date_info[2], $date_info[3], $_REQUEST['loc_id'], $_REQUEST['report_server_id'], $emp_class_id);
							if (mysqli_num_rows($supergroupquery) > 0) {
								while ($supergroupinfo = mysqli_fetch_assoc($supergroupquery)) {
									$tipscalc += $supergroupinfo['gross'];
								}
							}
							}
						}
						if (!empty($p) && (!empty($cid1) || !empty($cid2))) {
							$valid_count++;
							if (empty($cid1)) $cid1 = 0;
							if (empty($cid2)) $cid2 = 0;

							$class1 = $emp_classes[$cid1];
							$class2 = $emp_classes[$cid2];
							/*calculate Tipout date on the basis of Tipout rules*/
							$set_sales_tipout = ($tipscalc * ($p / 100));

							if ($subtypeofcalc == 'totaltips') {
								$set_cash_tipout = ($cash_tips ) * ($p / 100);
								$set_card_tipout = ( $totals['card_tips']) * ($p / 100);
							} else if($subtypeofcalc == 'cashtipsonly') {
								$set_cash_tipout = ($cash_tips * ($p / 100));
								$set_card_tipout = '0.00';
							} else if($subtypeofcalc == 'cardtipsonly') {
								$set_cash_tipout = '0.00';
								$set_card_tipout = ($totals['card_tips'] * ($p / 100));
							}

							/*Get Tipout date from table, if already submitted.*/
							$check_saved = lavu_query("SELECT `value`, `value2`,`sales_tipout` FROM `tipouts` WHERE `loc_id` = '[1]' AND `typeid` = '[2]' AND `typeid2` = '[3]' AND `server_id` = '[4]' AND `date` = '[5]' AND `emp_class_id`='[6]' ORDER BY `id` DESC LIMIT 1", $_REQUEST['loc_id'], $cid1, $cid2, $_REQUEST['report_server_id'], $date_info[0],$emp_class_id);
							if (mysqli_num_rows($check_saved) > 0) {
								$info = mysqli_fetch_assoc($check_saved);
								$set_cash_tipout = $info['value'];
								$set_card_tipout = $info['value2'];
								$set_sales_tipout = $info['sales_tipout'];
							}

							if ($typeofcalc == 'sales') {
								$supergroups_used[$tipskey] = ($subtypeofcalc=='supergroups')? json_encode($supr_ids):"";
								$sales_tipouts[$tipskey] = $set_sales_tipout;
							} else {
								if ($subtypeofcalc == 'totaltips') {
									$cash_tipout[$tipskey] = $set_cash_tipout;
									$card_tipout[$tipskey] = $set_card_tipout;
								} else if ($subtypeofcalc == 'cashtipsonly') {
									$cash_tipout[$tipskey] = $set_cash_tipout;
								} else if ($subtypeofcalc == 'cardtipsonly') {
									$card_tipout[$tipskey] = $set_card_tipout;
								}
							}

							$save_cash_tipout = $cash_tipout[$tipskey];
							$save_card_tipout = $card_tipout[$tipskey];
							$save_sales_tipout = $sales_tipouts[$tipskey];
							$supergroup_id = '';
							$q_fields = "";
							$q_values = "";
							$q_update = "";
							$vars = array();
							if (!empty($supergroups_used[$tipskey])) {
								$vars['supergroups_used'] = json_encode($supergroups_used[$tipskey]);
							}
							$vars['percentage'] = number_format(($p / 100), 3);
							$cash_tip = $save_cash_tipout;
							$cash_tip = str_replace(",", ".", $cash_tip);
							$card_tip = $save_card_tipout;
							$card_tip = str_replace(",", ".", $card_tip);
							$vars['value'] = $cash_tip;
							$vars['value2'] = $card_tip;
							
							if ($subtypeofcalc == 'totaltips') {
								$vars['cash_percentage'] = number_format(($save_cash_tipout / $cash_tips + $totals['card_tips']), 3);
								$vars['card_percentage'] = number_format(($save_card_tipout / $cash_tips + $totals['card_tips']), 3);
							} else if ($subtypeofcalc == 'cashtipsonly') {
								$vars['cash_percentage'] = number_format(($save_cash_tipout / $cash_tips), 3);
								$vars['card_percentage'] = '0.00';
							} else if($subtypeofcalc == 'cardtipsonly') {
								$vars['cash_percentage'] = '0.00';
								$vars['card_percentage'] = number_format(($save_card_tipout / $totals['card_tips']), 3);
							}

							$vars['cash_tips'] = $cash_tips;
							$vars['card_tips'] = $totals['card_tips'];
							$vars['type'] = $emp_classes[$cid1];
							$vars['type2'] = $emp_classes[$cid2];
							$vars['emp_class_id'] = $emp_class_id;
							
							$sale_tipout = $save_sales_tipout;
							$sale_tipout = str_replace(",", ".", $sale_tipout);
							$vars['sales_tipout'] = $sale_tipout;
							$vars['breakdown_type'] = $breakrole;
							$vars['breakdown_detail'] = '';
							$keys = array_keys($vars);
							foreach ($keys as $key) {
									if ($q_update != "") $q_update .= ", ";
									$q_update .= "`$key` = '[$key]'";
							}
							$vars['loc_id'] = $_REQUEST['loc_id'];
							$vars['typeid'] = $cid1;
							$vars['typeid2'] = $cid2;
							$vars['server_id'] = $_REQUEST['report_server_id'];
							$vars['date'] = $date_info[0];
							if ($vars['value'] != "" && $vars['value2'] != "") {
								$final_amount = $vars['value'] + $vars['value2'];
							} else {
								$final_amount = $vars['sales_tipout'];
							}
							if ($rules[0]!='' && $rules[2]!='') {
								$final_amount = $final_amount/2;
							}
							$userdetail=array();
							$classArr = array($cid1,$cid2);
							/*Tipout breakdown process on the basis of Tipout rules, This breakdown will apply only Active users.*/
							if ($breakrole == "evenly") {
								foreach ($classArr as $classId) {
									$user_query = lavu_query("SELECT `users`.`id`,`users`.`f_name`,`users`.`l_name`,`clock_punches`.`time`,`clock_punches`.`hours`,`clock_punches`.`time_out` FROM `users` JOIN `clock_punches` ON `users`.`id` = `clock_punches`.`server_id` WHERE `users`.`_deleted` = 0 AND `clock_punches`.`role_id` in ($classId) AND `clock_punches`.`time` >= '".$date_info[2]."' AND `clock_punches`.`time` < '".$date_info[3]."' ");
									$user_count = mysqli_num_rows($user_query);
									$amount =  $final_amount/$user_count;
									while ($user_info = mysqli_fetch_assoc($user_query)) {
										$userdetail[]= array(
												'userid' => $user_info['id'],
												'username' => $user_info['f_name']." ".$user_info['l_name'],
												'amount' => round($amount,2));
									}
							
								}
								$breakdown_detail = array('breakdownType' => $breakrole, 'totalAmount' => $total_tip, 'userDetail' => $userdetail);
								$vars['breakdown_detail'] = json_encode($breakdown_detail);
							} else {
								$userBreakUpArr = array();
								$hrs = 0;
								foreach ($classArr as $classId) {
									$user_query = lavu_query("SELECT `users`.`id`,`users`.`f_name`,`users`.`l_name`,`clock_punches`.`time`,`clock_punches`.`hours`,`clock_punches`.`time_out` FROM `users` JOIN `clock_punches` ON `users`.`id` = `clock_punches`.`server_id` WHERE `users`.`_deleted` = 0 AND `clock_punches`.`role_id` in ($classId) AND `clock_punches`.`time` >= '".$date_info[2]."' AND `clock_punches`.`time` < '".$date_info[3]."' ");
									$user_count = mysqli_num_rows($user_query);
									$userInfo = array();
									$hrs = 0;
									while ($row = mysqli_fetch_assoc($user_query)) {
										$userid = $row['id'];
										if ($row['time_out'] != "") {
											$hrs += $row['hours'];
										} else {
											$inTime = $row['time'];
											$timezone = locationSetting("timezone");
											if (! empty($timezone)) {
												$today  = DateTimeForTimeZone($timezone);
											}
											$inTime= strtotime($inTime);
											$today= strtotime($today);
											$userHours = round(abs($today-$inTime)/60/60);
											$hrs += $userHours;
											$row['hours'] = $userHours;
										}
										if (!isset($userInfo[$userid])) {
											$userInfo[$userid]['id'] = $row['id'];
											$userInfo[$userid]['f_name'] = $row['f_name'];
											$userInfo[$userid]['l_name'] = $row['l_name'];
											$userInfo[$userid]['hours'] = $row['hours'];
										} else {
											$userInfo[$userid]['hours'] = $userInfo[$userid]['hours'] + $row['hours'];
										}
									}
									$perHours = $final_amount/$hrs;
									$userdetailHour = array();
									foreach ($userInfo as $hkey => $hvals) {
										$userdetailHour[]= array(
												'userid' => $hvals['id'],
												'username' => $hvals['f_name']." ".$hvals['l_name'],
												'amount' => round($hvals['hours']*$perHours,2));
									}
									if (!empty($userBreakUpArr) && (count($userBreakUpArr)>0)) {
										$userBreakUpArr = array_merge($userBreakUpArr, $userdetailHour);
									} else {
										$userBreakUpArr = $userdetailHour;
									}
							
								}
								$breakdown_detail = array('breakdownType' => $breakrole, 'totalAmount' => $total_tip, 'userDetail' => $userBreakUpArr);
								$vars['breakdown_detail'] = json_encode($breakdown_detail);
							}
							/*Insert / update tipout details in tipouts table.*/
							$keys = array_keys($vars);
							foreach ($keys as $key) {
									if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
									$q_fields .= "`$key`";
									$q_values .= "'[$key]'";
							}

							$check = lavu_query("SELECT `id` FROM `tipouts` WHERE `loc_id` = '[loc_id]' AND `typeid` = '[typeid]' AND `typeid2` = '[typeid2]' AND `server_id` = '[server_id]' AND  `date` = '[date]' AND `emp_class_id`='[emp_class_id]'", $vars);
							if (mysqli_num_rows($check) > 0) {
								$info = mysqli_fetch_assoc($check);
								$vars['id'] = $info['id'];
								$tipout_ids .= $vars['id'].",";
								$update = lavu_query("UPDATE `tipouts` SET $q_update WHERE `id` = '[id]'", $vars);
							}
							else { 
								$create = lavu_query("INSERT INTO `tipouts` ($q_fields) VALUES ($q_values)", $vars);
								$tipout_ids .= lavu_insert_id().",";
							}

							$empIds = lavu_query("select `id` from `emp_classes` where `_deleted` = 0 ");
							while ($empid = mysqli_fetch_assoc($empIds)) {
							    $ids .= $empid['id'].", ";
							}

							$ids = rtrim($ids, ", ");
							$today_timestamp = date("Y-m-d");
							$empInfo_query = lavu_query("select * from `emp_classes_temp` where `_deleted` = 0 AND `class_updated_date` ='".$today_timestamp."' and `emp_class_id` in ($ids)");
							
							while ($empData = mysqli_fetch_assoc($empInfo_query)) {
								$update_emp_class = "UPDATE `emp_classes` SET `title` = '".$empData['title']."' ,`type` = '".$empData['type']."', `_deleted` = '".$empData['_deleted']."',`tipout_rules` = '".$empData['tipout_rules']."' ,`tiprefund_rules` = '".$empData['tiprefund_rules']."', `refund_type` = '".$empData['refund_type']."', `enable_tip_sharing` = '".$empData['enable_tip_sharing']."',`tip_sharing_rule` = '".$empData['tip_sharing_rule']."', `include_server_owes` = '".$empData['include_server_owes']."', `tip_sharing_period` = '".$empData['tip_sharing_period']."', `tipout_break` = '".$empData['tipout_break']."' where `id` = '".$empData['emp_class_id']."'";
							    lavu_query($update_emp_class);
							    
							    $update_emp_temp_class = "UPDATE `emp_classes_temp` SET `_deleted` = 1 where `emp_class_id` = '".$empData['emp_class_id']."' AND `class_updated_date`  = '".$today_timestamp."'";
							    lavu_query($update_emp_temp_class);
							}
						}
					}
				}

				/*To check Tip refund is available to the server.*/
				$check_saved = lavu_query("SELECT `amount` FROM `tipout_refunds` WHERE `emp_class_id`=[1] and `created_date` = '[2]' and `server_id` = '[3]' ORDER BY `id` DESC LIMIT 1", $emp_class_id, $date_info[0], $_REQUEST['report_server_id']);
				if (mysqli_num_rows($check_saved) > 0) {
					$info = mysqli_fetch_assoc($check_saved);
					$set_card_refund = $info['amount'];
				}

				/*Insert / update Tip refund rule to the server.*/
				if ($refundpercent != '') {
					$r_vars = array();
					$Refundamount = str_replace(",", ".", $set_card_refund);
					$r_vars['amount'] =$Refundamount;
					$r_vars['refund_percentage'] = number_format($Refundamount/$refundcalc, 3);
					$r_vars['created_date'] = $date_info[0];
					$r_vars['emp_class_id'] = $emp_class_id;
					$r_vars['loc_id'] = $_REQUEST['loc_id'];
					$r_vars['server_id'] = $_REQUEST['report_server_id'];
					$r_keys = array_keys($r_vars);
					$q_fields = '';
					$q_values = '';
					foreach ($r_keys as $rkey) {
						if ($q_fields != "") {
							$q_fields .= ", ";
							$q_values .= ", ";
						}

						$q_fields .= "`$rkey`";
						$q_values .= "'[$rkey]'";
					}

					$q_update = '';
					foreach ($r_keys as $rkey) {
						if ($q_update != "") {
							$q_update .= ", ";
						}

						$q_update .= "`$rkey` = '[$rkey]'";
					}

					$updateOrInsert = 0;
					$check = lavu_query("SELECT `id`,`emp_class_id` FROM `tipout_refunds` WHERE `emp_class_id`=[1] and `created_date` = '[2]' and `server_id` = '[3]'", $emp_class_id, $date_info[0], $_REQUEST['report_server_id']);
					if (mysqli_num_rows($check) > 0) {
						$info = mysqli_fetch_assoc($check);
						$r_vars['id'] = $info['id'];
						$update = lavu_query("UPDATE `tipout_refunds` SET $q_update WHERE `id` = '[id]'", $r_vars);
						$updateOrInsert = 1;
					} else {
						lavu_query("INSERT INTO `tipout_refunds` ($q_fields) VALUES ($q_values)", $r_vars);
					}

				}
			
			}

		}

	}

?>