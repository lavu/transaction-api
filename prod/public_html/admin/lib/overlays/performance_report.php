<?php

	require_once("../info_inc.php");
	$pack_id = reqvar("pack_id", "");
	$active_language_pack = getWebViewLanguagepack($pack_id, $loc_id);

?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Performance Report</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles_dark {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 28px;
	font-weight: bold;
	color: #2f3e55;
}
.info_text1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #233756;
}
.info_text2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #5A75A0;
}
.info_text3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #233756;
}
.info_text_big {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 30px;
	color: #233756;
}
.count_big {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 50px;
	color: #FFFFFF;
}
.no_messages {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 30px;
	color: #415472;
	cursor:pointer;
}
.messages_link {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 30px;
	color: #4179d2;
	cursor:pointer;
}

-->
		</style>
	</head>

<?php

	$now_datetime = (isset($_REQUEST['device_time']))?$_REQUEST['device_time']:date("Y-m-d H:i:s");
	if ($location_info['timezone'] != "") {
		$now_datetime = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
	}
	
	$now_parts = explode(" ", $now_datetime);
	$now_date = explode("-", $now_parts[0]);
	$now_time = explode(":", $now_parts[1]);

	$se_hour = substr(str_pad($location_info['day_start_start_time'], 4, "0", STR_PAD_LEFT), 0, 2);
	$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

	if (($now_time[0].$now_time[1]) < ($se_hour.$se_min)) {
		$use_ts = mktime($se_hour, $se_min, 0, $now_date[1], ($now_date[2] - 1), $now_date[0]);
		$e_datetime = date("Y-m-d H:i:s", $use_ts);
	} else {
		$e_datetime = $now_parts[0]." $se_hour:$se_min:00";
	}
	
	$e_dt_parts = explode(" ", $e_datetime);
	$e_date = explode("-", $e_dt_parts[0]);
	$e_time = explode(":", $e_dt_parts[1]);
	$e_ts = mktime($e_time[0], $e_time[1], 0, $e_date[1], $e_date[2], $e_date[0]);
	
	for ($d = 0; $d < 7; $d++) {
		$start_ts = mktime($e_time[0], $e_time[1], 0, $e_date[1], ($e_date[2] - $d), $e_date[0]);
		if (date("N", $start_ts) == "1") {
			$start_datetime = date("Y-m-d H:i:s", $start_ts);
			break;
		}
	}

	$vars = array();
	$vars['loc_id'] = $_REQUEST['loc_id'];
	$vars['server_id'] = $_REQUEST['server_id'];
	$vars['server_name'] = $_REQUEST['server_name'];
	$vars['start_datetime'] = $start_datetime;
	$vars['end_datetime'] = $now_datetime;
	$vars['menu_id'] = $location_info['menu_id'];

	$display = "";
	
	$get_category_info = lavu_query("SELECT * FROM `menu_categories` WHERE `name` = 'Memberships' AND `menu_id` = '[menu_id]'", $vars);
	$category_info = mysqli_fetch_assoc($get_category_info);
	$vars['category_id'] = $category_info['id'];
	
	
	$get_performance_info = lavu_query("SELECT COUNT(`order_contents`.`id`) AS `count`, 
	`order_contents`.`price` AS `field0`,
	`order_contents`.`quantity` AS `field1`,
	`orders`.`void` AS `field2`,
	`orders`.`opened` AS `field3`,
	`orders`.`location_id` AS `field4`,
	`order_contents`.`loc_id` AS `field5`,
	`orders`.`cashier` AS `field6`,
	`orders`.`cashier_id` AS `field7`,
	`order_contents`.`item_id` AS `field8`,
	`order_contents`.`void` AS `field9`,
	`menu_categories`.`id` AS `category_id`,
	`menu_categories`.`name` AS `category_name`,
	concat('$',format(sum(`order_contents`.`subtotal`),2)) AS `field12`,
	concat('$',format(sum( (`order_contents`.`forced_modifiers_price` + `order_contents`.`modify_price`) * `order_contents`.`quantity`),2)) AS `field13`,
	concat('$',format(sum(`order_contents`.`subtotal_with_mods`),2)) AS `subtotal`,
	concat('$',format(sum(`order_contents`.`discount_amount`),2)) AS `discount`,
	concat('$',format(sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount`),2)) AS `after_discount`,
	concat('$',format(sum(`order_contents`.`tax_amount`),2)) AS `tax`,
	concat('$',format(sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` + `order_contents`.`tax_amount`),2)) AS `field18`,
	concat('$',format(sum(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` + `order_contents`.`tax_amount`),2)) AS `total`
	FROM `order_contents`
	LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id`
	LEFT JOIN `orders` ON `order_contents`.`order_id` = `orders`.`order_id`
	LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id`
	WHERE `orders`.`cashier_id` = '[server_id]' 
	AND `orders`.`void` != 1 
	AND `orders`.`opened` >= '[start_datetime]' 
	AND `orders`.`opened` <= '[end_datetime]' 
	AND `orders`.`location_id` = '[loc_id]' 
	AND `order_contents`.`loc_id` = `orders`.`location_id` 
	AND `order_contents`.`item_id` > 0 
	AND `order_contents`.`void` != 1
	AND `menu_items`.`category_id` = '[category_id]'
	GROUP BY `category_id` 
	ORDER BY `count` DESC", $vars);
	//ORDER BY SUM(`order_contents`.`subtotal_with_mods`) ASC", $vars);
	if (@mysqli_num_rows($get_performance_info) > 0) {
		$display = "<table cellspacing='0' cellpadding='4'>";
			/*<tr class='info_text1'><td width='10px'>&nbsp;</td><td align='center'><b>Category</b></td><td align='center'><b># Sold</b></td><td width='10px'>&nbsp;</tr>
			<tr><td colspan='4'><hr color='#5A75A0'></td></tr>";*/
		while ($p_info = mysqli_fetch_assoc($get_performance_info)) {
			//$display .= "<tr class='info_text2'><td></td><td align='center'>".$p_info['category_name']."</td><td align='center'>".$p_info['count']."</td><td></td></tr>";
			$display = "<tr><td align='center'><span class='info_text_big'>".$p_info['category_name']." Sold:</span></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td align='center'>
					<table cellspacing='0' cellpadding='0'>
						<tr><td width='130px' height='130px' style='background:URL(images/blue_burst.png); background-repeat:no-repeat; background-position:center;' align='center' valign='middle' class='count_big'>".$p_info['count']."</td></tr>
					</table>
				</td>
			</tr>";
		}
		///$display .= "<tr><td colspan='4'><hr color='#5A75A0'></td></tr>
	} else {
		$display = "<span class='info_text_big'>" . speak('No membership sales to report') . "...</span>";
	}
	
	$messages_code = "<span class='no_messages'>" . speak('You have no messages') . "...</span>";
	$check_messages = lavu_query("SELECT * FROM `messages` WHERE `loc_id` = '[loc_id]' AND `server_id` = '[server_id]' AND `type` = 'lk_server' AND `_deleted` != '1' ORDER BY `server_time` DESC LIMIT 50", $vars);
	if (@mysqli_num_rows($check_messages) > 0) {
		$messages_link = "View Messages";
		while ($message_info = mysqli_fetch_assoc($check_messages)) {
			if ($message_info['read'] == "0") {
				$messages_link_text = "You have new messages!";
			}
		}
		$messages_code = "<a onclick='location = \"_DO:cmd=load_url&filenames=messages_wrapper.php;messages.php&dims=;&scroll=;?type=lk_server\";' class='messages_link'>$messages_link_text</a>";
	}

?>

	<body>
		<table width="542" cellspacing="0" cellpadding="0">
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td align='center' class='subtitles_dark'><?php echo $_REQUEST['server_name']; ?></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td align='center'><?php echo $display; ?></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td align='center'><?php echo $messages_code; ?></td></tr>
			<tr><td>&nbsp;</td></tr>
			</table>
	</body>
</html>