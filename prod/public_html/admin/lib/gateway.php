<?php
    session_start();
	ini_set('display_errors', 0);
	require_once("info_inc.php");
	require_once("gateway_functions.php");

	$transtype				= reqvar("transtype", "Sale");
	$send_debug_messages	= $location_info['gateway_debug'];
	$comm_vars_log_file		= "";

	if (lsecurity_id($company_code_key) == $company_info['id'])
	{	
		$start_time = microtime(true);

		if ($send_debug_messages)
		{
			$comm_vars_log_file = log_comm_vars($company_code, $got_loc_id, "gateway");
		}

		$location_id		= reqvar("loc_id", false);
		$mag_data			= reqvar("mag_data", false);
		$card_number		= reqvar("card_number", false);
		$card_expiration	= reqvar("card_expiration", false);
		$card_cvn			= reqvar("CVN", "");
		$card_amount		= reqvar("card_amount", false);
		$tip_amount			= reqvar("tip_amount", "");
		$refund_tip_amount	= reqvar("refund_tip_amount", 0);
		$card_reader		= strtolower(reqvar("reader", ""));
		$card_present		= reqvar("card_present", "Y");
		$encrypted			= reqvar("encrypted", "0");
		$pnref				= reqvar("pnref", "");
		$authcode			= reqvar("authcode", "");
		$check				= reqvar("check", "1");
		$register			= reqvar("register", "");
		$register_name		= reqvar("register_name", "");
		$server_id			= reqvar("server_id", 0);
		$server_name		= reqvar("server_name", "");
		$server_full_name	= reqvar("server_full_name", $server_name);
		$more_info			= reqvar("more_info", "");
		$poslavu_version	= reqvar("app_version", "");
		$poslavu_build		= reqvar("app_build", "");
		$device_time		= reqvar("device_time", date("Y-m-d H:i:s"));
		$set_pay_type		= reqvar("pay_type", "Card");
		$set_pay_type_id	= reqvar("pay_type_id", "2");
		$for_deposit		= reqvar("for_deposit", 0);
		$is_deposit			= reqvar("is_deposit", 0);
		$internal_order_id	= reqvar("ioid", "");
		$internal_tx_id		= reqvar("internal_id", "");
		$orig_internal_id	= reqvar("orig_internal_id", "");
        $refund_notes		= reqvar("refund_notes", "");
        $void_notes			= reqvar("void_notes", "");
        $split_tender_id	= reqvar("split_tender_id", "");
        $reskin             = reqvar("reskin", "");
        $request_gateway     = reqvar("gateway", "");

		if (empty($mag_data)) $mag_data = false;
		if (empty($poslavu_version)) $poslavu_version = reqvar("version", "1.5.6");
		if (empty($poslavu_build)) $poslavu_build = reqvar("build", "20110214-1");

		$timezone = locationSetting("timezone");
		if (!empty($timezone))
		{
			$device_time = DateTimeForTimeZone($timezone);
		}

		$device_udid = determineDeviceIdentifier($_REQUEST);
		
		$name_on_card = "";
		$exp_month = "";
		$exp_year = "";
		// Peeper Refund transtion
		if ($set_pay_type == "Loyalty App" && $transtype == "Return") {
			$res = toRefundAmountPepperOrder($_REQUEST);
			echo $res;
			lavu_exit();
		}
		
		if (in_array($transtype, array("Sale", "Auth", "AuthForTab")) && !$mag_data && empty($card_number) && !in_array($location_info['gateway'], array('Square'))) {
			echo "0|No card data returned due to a communication error between iOS and the card reader.\n\nPlease follow these steps to restart POS Lavu:\n1. Double tap home button\n2. Swipe up on POS Lavu screen to kill the process\n3. Tap POS Lavu icon to restart\n4. Retry card swipe||".$_REQUEST['order_id']."||Card Reader Error|||";
			lavu_exit();
		}

		if(!in_array($location_info['gateway'], array('Square'))) {
            if ($card_reader == "llsswiper") $encrypted = "2";
            if (!empty($mag_data)) {
                $parsedCardInfo = parseCardSwipe($mag_data, $card_reader, $encrypted);
                $card_number = $parsedCardInfo['number'];
                $name_on_card = $parsedCardInfo['name'];
                $exp_month = $parsedCardInfo['exp_month'];
                $exp_year = $parsedCardInfo['exp_year'];
            } else {
                $exp_month = substr($card_expiration, 0, 2);
                $exp_year = substr($card_expiration, 2, 2);
            }

            if (!$mag_data && (substr($card_number, 0, 1) == "E")) $card_number = interpret(substr($card_number, 1));

        }
		if ($data_name) $db_name = "poslavu_".$data_name."_db";

		$response = "";

		if(!isset($authcode) || $authcode == ''){
			$authcode = reqvar("authcode", "");
		}

		$process_info = array(
			"mag_data"			=> $mag_data,
			"reader"			=> $card_reader,
			"encrypted"			=> $encrypted,
			"card_number"		=> $card_number,
			"card_cvn"			=> $card_cvn,
			"name_on_card"		=> $name_on_card,
			"exp_month"			=> $exp_month,
			"exp_year"			=> $exp_year,
			"card_amount"		=> (float)$card_amount,
			"refund_tip_amount"	=> $refund_tip_amount,
			"tip_amount"		=> $tip_amount,
			"transtype"			=> $transtype,
			"loc_id"			=> $location_id,
			"order_id"			=> reqvar("order_id", ""),
			"check"				=> $check,
			"pnref"				=> $pnref,
			"authcode"			=> $authcode,
			"ext_data"			=> "",
			"data_name"			=> $data_name,
			"register"			=> $register,
			"register_name"		=> $register_name,
			"device_udid"		=> $device_udid,
			"server_id"			=> $server_id,
			"server_name"		=> $server_name,
			"server_full_name"	=> $server_full_name,
			"info"				=> "",
			"more_info"			=> $more_info,
			"device_time"		=> $device_time,
			"set_pay_type"		=> $set_pay_type,
			"set_pay_type_id"	=> $set_pay_type_id,
			"for_deposit"		=> $for_deposit,
			"is_deposit"		=> $is_deposit,
			"item_db_id"		=> reqvar("item_db_id", false),
			"ioid"				=> $internal_order_id,
			"itid"				=> $internal_tx_id,
			"idempotent_id"		=> reqvar('idempotentId', ''),
			"session_key"		=> reqvar('sessionKey', ''),
			"payment_key"       => reqvar('paymentKey', ''),
			"poslavu_version"	=> $poslavu_version,
			"poslavu_build"		=> $poslavu_build,
			"card_present"		=> $card_present,
			"auth_by"			=> (isset($_REQUEST['PINused']))?convertPIN($_REQUEST['PINused'], "name"):$server_name,
			"auth_by_id"		=> (isset($_REQUEST['PINused']))?convertPIN($_REQUEST['PINused']):$server_id,
			"orig_internal_id"	=> $orig_internal_id,
			"split_tender_id"	=> $split_tender_id,
			"reskin"			=> $reskin,
			"request_gateway"   => $request_gateway,

		);


		$gateway_addons = false;
	
		if ($data_name && $location_id && is_numeric($location_id)) {
		
			if (file_exists(dirname(__FILE__)."/gateway_lib/addon_functions.php")) {
		
				require_once(dirname(__FILE__)."/gateway_lib/addon_functions.php");
		
				$lls = false;
				if (in_array($location_info['use_net_path'], array("1", "2")) && !strstr($location_info['net_path'], "poslavu.com") && !strstr($location_info['net_path'], "lavulite.com")) $lls = true;
	
				if (!isset($location_info['modules'])) $location_info['modules'] = "";
				if (!isset($company_info['signup_package'])) $company_info['signup_package'] = "2";
				if (!isset($company_info['package_status'])) $company_info['package_status'] = "2";
		
				require_once(dirname(__FILE__)."/modules.php");
				$active_modules = getActiveModules($location_info['modules'], (!empty($company_info['signup_package'])?$company_info['signup_package']:$company_info['package_status']), $lls);
				
				$gateway_addons = get_gateway_addons($transtype);
				process_gateway_addons($gateway_addons, $active_modules, $transtype, "before", "");
			}
			
			$kiosk = 0;
			$orderGateway = '';
			/* check for kiosk order. */
			if (preg_match("/^5-(.*)/i", trim($process_info['order_id'])) > 0) {
			    $kiosk = 1;
			    $orderGateway = getKioskGateway($process_info['order_id'], $location_id, $db_name);
			}

			if ($location_info['integrateCC'] == "1" || ($kiosk && $orderGateway!='')) {
				$decimal_places = $location_info['disable_decimal'];

				if ($kiosk) {
					$integration_data = getIntegrationFromKioskSettings($location_id, $orderGateway, $db_name);
				} else {
					$integration_data = get_integration_from_location($location_id, $db_name);
				}

				mlavu_close_db();

				if ($integration_data) {

					$process_info['gateway']		= $integration_data['gateway'];
					$process_info['username']		= $integration_data['integration1'];
					$process_info['password']		= $integration_data['integration2'];
					$process_info['integration3']	= $integration_data['integration3'];
					$process_info['integration4']	= $integration_data['integration4'];
					$process_info['integration5']	= $integration_data['integration5'];
					$process_info['integration6']	= $integration_data['integration6'];
					$process_info['company_id']		= $company_info['id'];
					$process_info['transtype']		= empty($transtype)?$location_info['cc_transtype']:$transtype;
					$process_info['kiosk']			= $kiosk;

					if ($process_info['username']=="dev" && $process_info['password']=="dev" && $card_number=="4111111111111111") {
	
						$response = "0|Approved (Dev Test)";
		
					} else {

						@$GLOBALS['log_gateway'] = $integration_data['gateway']; // for RequestTimingLogger

						$func_path = dirname(__FILE__)."/gateway_lib/Transaction_Controller.php";
						if (file_exists($func_path)) require_once($func_path);						
						$transactionController = new Transaction_Controller($integration_data['gateway'], $process_info);
						$transactionController->process_transaction($process_info, $location_info);
						$response = $transactionController->getResponse($send_debug_messages, $kiosk);
					}

				} else $response = "0|Integration Data Incomplete";

			} else $response = "0|Card Integration Turned Off";

		} else $response = "0|Base Data Incomplete 01 $data_name && $location_id ";

	} else $response = "0|Connection Authentication Error";

	if ($response == "") $response = "0|Data Incomplete";

	if ($gateway_addons) $response = process_gateway_addons($gateway_addons, $active_modules, $transtype, "after", $response);

	echo $response;

	$end_time = microtime(true);
	$process_time = round((($end_time - $start_time) * 1000), 2)."ms";

	if (!empty($comm_vars_log_file))
	{
		$fp = fopen($comm_vars_log_file, "a");
		fwrite($fp, number_format($end_time, 1, ".", "")." - LOCATION: ".$location_id." - RESPONSE: ".$response."[--CR--]Exec time (".$_REQUEST['YIG'].".".$_REQUEST['XIG']."): ".$process_time."[--CR--][--CR--]\n");
		fclose($fp);
	}

	if ($send_debug_messages) error_log("sent gateway response: ".$data_name." ".$integration_data['gateway']." ".$_REQUEST['YIG']." ".$process_time);

	lavu_exit();
?>
