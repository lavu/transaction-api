<?php

$available_DOcmds = (isset($_REQUEST['available_DOcmds']))?$_REQUEST['available_DOcmds']:"";
if ($available_DOcmds=="" && isset($_SESSION['available_DOcmds']))
{
	$available_DOcmds = $_SESSION['available_DOcmds'];
}
$_SESSION['available_DOcmds'] = $available_DOcmds;

$available_DOcmds = explode(",", $available_DOcmds);

function use_number_pad_if_available($field_id, $title, $type, $callback='setFieldValue', $extraArgs = array())
{
	global $available_DOcmds;

	$extraArgsStr = "";
	foreach ($extraArgs as $arg)
	{
		$extraArgsStr = $extraArgsStr.", %27".$arg."%27";
	}

	if (in_array("number_pad", $available_DOcmds))
	{
		return " READONLY onclick='window.location = \"_DO:cmd=number_pad&type=".$type."&max_digits=15&title=".rawurlencode($title)."&js=".$callback."(this, %27".$field_id."%27,%27ENTERED_NUMBER%27".$extraArgsStr.")\"'";
	}

	return "";
}

function use_available_confirm($action, $title, $message)
{
	global $available_DOcmds;

	if (in_array("confirm", $available_DOcmds))
	{
		return "window.location = \"_DO:cmd=confirm&js_cmd=".$action."&title=".rawurlencode($title)."&message=".rawurlencode($message)."\"";
	}

	return  "if (confirm(\"".str_replace("[--CR--]", " ", $message)."\")) ".$action.";";
}

?><script language='javascript'>

	var isEmbeddedWebView = true;

	function setFieldValue(sender, field_id, value)
	{
		document.getElementById(field_id).value = value;
	}

</script>