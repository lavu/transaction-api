<?php

if (!isset($_REQUEST['cc'])) $_REQUEST['cc'] = $_REQUEST['dn'];

session_start();
require_once("info_inc.php");
require_once(dirname(__FILE__) . "/Orders777.php");

$loc_id				= reqvar("loc_id", "");

$lavu_query_should_log = TRUE;

log_comm_vars($data_name, $loc_id, "TILL REPORT");

$mode				= reqvar("m", "");
$register			= reqvar("register", "receipt");
if (empty($register) || $register=="Not set") {
	 $register		= "receipt";
}
$register_name		= reqvar("register_name", "receipt");
if (empty($register_name)) {
	$register_name	= "receipt";
}
$report_server_id	= reqvar("report_server_id", "");
$report_server_name	= reqvar("report_server_name", "");
$admin_user_id		= reqvar("admin_user_id", "");
$datetime			= reqvar("datetime", "");
$set_date			= reqvar("set_date", "");

if( isset( $_REQUEST['debug'] ) && $_REQUEST['debug'] == '1' ){
	if( isset( $_REQUEST['date'] ) ){
		$set_date = $_REQUEST['date'];
	}
}

$isNorway = false;
$nor_way = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'norway_mapping'");
$norway_values = mysqli_fetch_assoc($nor_way);
$norway_status=$norway_values['value'];
if($norway_status=='1'){
	$isNorway = true;
}

// based on date format setting
$date_setting = "date_format";
$location_date_format_query = lavu_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
	$date_format = array("value" => 2);
}else
	$date_format = mysqli_fetch_assoc( $location_date_format_query );

// to set correct server name until after version 2.1.2+
$get_server_info = lavu_query("SELECT `f_name`, `l_name` FROM `users` WHERE `id` = '[1]'", $report_server_id);
if (mysqli_num_rows($get_server_info) > 0)
{
	$info = mysqli_fetch_assoc($get_server_info);
	$report_server_name = $info['f_name']." ".$info['l_name'];
}
//

$decimal_places = $location_info['disable_decimal'];
$smallest_money = (1 / pow(10, $decimal_places));

/*******************************************************************************
 *              L O C A L   D A T E T I M E   I N F O R M A T I O N            *
 ******************************************************************************/

if ($location_info['timezone'] != "") $datetime = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);

$se_hour = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
$se_min = substr(str_pad($location_info['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

if ($datetime != "") {

	$idatetime = explode(" ", $datetime);
	$idate = explode("-", $idatetime[0]);
	$itime = explode(":", $idatetime[1]);
	$yts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] - 1), $idate[0]);
	$nowts = mktime($itime[0], $itime[1], $itime[2], $idate[1], $idate[2], $idate[0]);
	$tts = mktime($itime[0], $itime[1], $itime[2], $idate[1], ($idate[2] + 1), $idate[0]);

	if ($itime[0].$itime[1].$itime[2] >= $se_hour.$se_min."00") {
		$use_ts = $nowts;
		$use_date = date("Y-m-d", $nowts);
		$end_date = date("Y-m-d", $tts);
	} else {
		$use_ts = $yts;
		$use_date = date("Y-m-d", $yts);
		$end_date = date("Y-m-d", $nowts);
	}

} else {

	$nowts = time();
	if (date("His") >= $se_hour.$se_min."00") {
		$use_ts = $nowts;
		$use_date = date("Y-m-d");
		$end_date = date("Y-m-d", ($nowts + 86400));
	} else {
		$use_ts = ($nowts - 86400);
		$use_date = date("Y-m-d", $use_ts);
		$end_date = date("Y-m-d");
	}
}

if ($set_date!="") {
	$use_date = substr($set_date,0,10);
	//$sdatetime = explode(" ", $set_date);
	$sdate = explode("-", $use_date);
	//$stime = explode(":", $sdatetime[1]);
	$use_ts = mktime($se_hour, $se_min, 59, $sdate[1], $sdate[2], $sdate[0]);

	//$use_ts = mktime()
	$end_date = date("Y-m-d", $use_ts + 86400);
	//echo lsecurity_name("dive_inn", "3344");
}

//$business_date = date("l, n/j/Y", $use_ts);
//$print_datetime = date("D, n/j/Y g:i a", $nowts);

switch ($date_format['value']){
	case 1:$business_date = date("l, j/n/Y", $use_ts);
	$print_datetime = date("D, j/n/Y g:i a", $nowts);
	break;
	case 2:$business_date = date("l, n/j/Y", $use_ts);
	$print_datetime = date("D, n/j/Y g:i a", $nowts);
	break;
	case 3:$business_date = date("l, Y/n/j", $use_ts);
	$print_datetime = date("D, Y/n/j g:i a", $nowts);
	break;
}

$udate = explode("-",$use_date);
$uyear = $udate[0];
$umonth = $udate[1];
$uday = $udate[2];
$udate_display = date("n/j/Y",mktime(0,0,0,$umonth,$uday,$uyear));

$end_hour = $se_hour;
$end_min = str_pad(($se_min - 1), 2, "0", STR_PAD_LEFT);
if ($end_min < 0) {
	$end_hour = str_pad(($se_hour - 1), 2, "0", STR_PAD_LEFT);
	$end_min = 59;
}
if ($end_hour < 0) {
	$end_hour = 23;
	$end_date = date("Y-m-d", $nowts);
}

$start_datetime = "$use_date $se_hour:$se_min:00";
$end_datetime = "$end_date $end_hour:$end_min:59";

$actual_day_start = $start_datetime;
$actual_day_end = $end_datetime;



$rp_array = array();
$rp_array[] = array("mode"=>"all","start"=>"","end"=>"");
if(isset($company_code))
{
	// $rp_array[] = array("mode"=>"all","start"=>"","end"=>"");
	 $start_clock_time = $start_datetime; // Use Start of Day for First Shift
	 $clock_query = lavu_query("select * from `clock_punches` where `server_id`='[1]' and `time_out`>='[2]' and `time_out`<'[3]' order by `time_out` asc",$report_server_id,$start_datetime,$end_datetime);
	 while($clock_read = mysqli_fetch_assoc($clock_query))
	 {
		 $rp_array[] = array("mode"=>"shift","start"=>$start_clock_time,"end"=>$clock_read['time_out'],"role_id"=>$clock_read['role_id']);
		 $start_clock_time = $clock_read['time_out'];
	 }
	 if(count($rp_array) > 1)
	 {
		 $rp_array[count($rp_array)-1]['end'] = $end_datetime; // Use End of Day for Last Shift
	 }
}

$print_string = "";
$found_orders = false;
for($rp=0; $rp<count($rp_array); $rp++)
{
	$rp_details = $rp_array[$rp];
	/*******************************************************************************
	 *                          S H I F T   I N F O                                *
	 ******************************************************************************/

	$should_check_for_shifts = false;
	$shift_info = array();
	$vars_str = (isset($_REQUEST['shift_info']))?$_REQUEST['shift_info']:"";
	if ($vars_str != "") {
		$var_parts = explode("(and)",$vars_str);
		for($n=0; $n<count($var_parts); $n++) {
			$vparts = explode("(eq)",$var_parts[$n]);
			if (count($vparts) > 1 && trim($vparts[0])!="") $shift_info[trim($vparts[0])] = trim($vparts[1]);
		}
	} else if ($mode!="server" && $mode!="server_single") $should_check_for_shifts = true;
	if (!isset($location_info['register_summary_function']) || $location_info['register_summary_function']=="all_day") $should_check_for_shifts = false;

	if ($should_check_for_shifts)	{
		$type = (isset($shift_info['type']))?$shift_info['type']:"register";
		if ($mode == "x_report") $type = "house";
		$check_for_shifts = lavu_query("SELECT * FROM `shifts` WHERE `loc_id` = '[1]' AND `register` = '[2]' AND `datetime` > '[3]' AND `datetime` < '[4]' AND `type` = '[5]' ORDER BY `id` DESC", $loc_id, $register, $start_datetime, $end_datetime, $type);
		if (mysqli_num_rows($check_for_shifts) > 0) {
			$info = mysqli_fetch_assoc($check_for_shifts);
			$shift_info['shift_number'] = (mysqli_num_rows($check_for_shifts) + 1);
			$shift_info['shift_start'] = $info['datetime'];
			$shift_info['type'] = $type;
		}
	}

	if (isset($shift_info['shift_start'])) $start_datetime = $shift_info['shift_start'];
	if (isset($shift_info['shift_end'])) $end_datetime = $shift_info['shift_end'];

	if($rp_details['mode']=="shift")
	{
		$start_datetime = $rp_details['start'];
		$end_datetime = $rp_details['end'];
	}

	/*******************************************************************************
	 *              S E R V E R   S U M M A R Y   C A S H   D A T A                *
	 ******************************************************************************/

	$todays_cash_deposits_str = "";
	$todays_cash_tips = number_format(0, $decimal_places, ".", "");
	if ($mode=="server" || $mode=="server_single")
	{
		$cash_tip_info = array();
		$check_todays_cash_tips = lavu_query("SELECT `id`, `value` FROM `cash_data` WHERE `type` = 'server_cash_tips' AND `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $loc_id, $report_server_id, $use_date);
		if (mysqli_num_rows($check_todays_cash_tips) > 0)
		{
			$cash_tip_info = mysqli_fetch_assoc($check_todays_cash_tips);
			$todays_cash_tips = $cash_tip_info['value'];
		}

		if ($mode == "server_single")
		{
			$server_cash_info = explode("|", $_REQUEST['server_cash_info']);
			if (abs((float)$server_cash_info[0]) >= $smallest_money)
			{
				$q_fields = "";
				$q_values = "";
				$vars = array();
				$vars['type'] = "server_register_deposit";
				$vars['loc_id'] = $loc_id;
				$vars['server_id'] = $report_server_id;
				$vars['date'] = $use_date;
				$vars['value'] = number_format((float)$server_cash_info[0], $decimal_places, ".", "");
				$vars['value2'] = $register;
				$vars['value3'] = $register_name;
				$vars['device_time'] = $date_time;
				$vars['server_time'] = date("Y-m-d H:i:s");
				$keys = array_keys($vars);
				foreach ($keys as $key) {
					if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
					$q_fields .= "`$key`";
					$q_values .= "'[$key]'";
				}
				$save_deposit = lavu_query("INSERT INTO `cash_data` ($q_fields) VALUES ($q_values)", $vars);
			}

			if ($server_cash_info[1] > 0)
			{
				$todays_cash_tips = number_format((float)$server_cash_info[1], $decimal_places, ".", "");
				if (isset($cash_tip_info['id']) && $cash_tip_info['id']>0)
				{
					$update_cash_tips = lavu_query("UPDATE `cash_data` SET `value` = '[1]' WHERE `id` = '[2]'", $todays_cash_tips, $cash_tip_info['id']);
				}
				else
				{
					$save_cash_tips = lavu_query("INSERT INTO `cash_data` (`type`, `loc_id`, `server_id`, `date`, `value`) VALUES ('server_cash_tips', '[1]', '[2]', '[3]', '[4]')", $loc_id, $report_server_id, $use_date, $todays_cash_tips);
				}

				$orders777 = new Orders777($location_info['server_manage_tips'], $data_name, $register, $register_name, $report_server_id, $use_date, $location_info, $todays_cash_tips);
				$orders777->ensure777CashTipsOrderInsertion();
				shell_exec("php /home/poslavu/private_html/scripts/tipSharing.php $_REQUEST[report_server_id] $_REQUEST[loc_id] $_REQUEST[cc]");
			}
		}

		if ($location_info['server_summary_cash_deposit'] == "1")
		{
			$get_cash_deposits = lavu_query("SELECT SUM(`value`) AS `value`, `value2`, `value3` FROM `cash_data` WHERE `type` = 'server_register_deposit' AND `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]' GROUP BY `value2` ORDER BY `value3` ASC", $loc_id, $report_server_id, $use_date);
			if (mysqli_num_rows($get_cash_deposits) > 0)
			{
				$total = 0;
				$todays_cash_deposits_str = " :";
				$todays_cash_deposits_str .= " - - - Todays Cash Deposits - - - :";
				while ($info = mysqli_fetch_assoc($get_cash_deposits))
				{
					$todays_cash_deposits_str .= $info['value3']." [c *".showAmount($info['value'])." :";
					$total += $info['value'];
				}
				$todays_cash_deposits_str .= "Total [c *".showAmount($total)." : ";
			}
		}
	}

	/**********************************************************************************************
	 * This condition is allow tip sharing, when server_summary_tipout_overlay prompt is disabled.
	 *********************************************************************************************/
	if (isset($location_info['server_summary_tipout_overlay']) && $location_info['server_summary_tipout_overlay'] == 0 && ($mode == 'server' || $mode == 'server_single')) {
		require_once(dirname(__FILE__) . "/overlays/tipouts_sharing.php");
	}

	/*********************************************************************************
	 * Tip Refund when Tip sharing is not assigned
	 ********************************************************************************/
	$getCardTipRefundInfo = lavu_query("select `emp_classes`.`tiprefund_rules`, `tip_sharing`.`server_id`, `tip_sharing`.`emp_class_id`, `tip_sharing`.`card_tip`, `tip_sharing`.`created_date` FROM `emp_classes` LEFT JOIN `tip_sharing` ON `emp_classes`.`id` = `tip_sharing`.`emp_class_id` WHERE `emp_classes`.`tipout_rules` = '' AND	`emp_classes`.`refund_type` = 'Card Tips' AND `tip_sharing`.`location_id` ='[1]' AND `tip_sharing`.`created_date` ='[2]' AND `tip_sharing`.`server_id` = '[3]' AND `tip_sharing`.`card_tip` != '' AND `emp_classes`.`tiprefund_rules` != ''", $loc_id, $use_date, $report_server_id);

			if (mysqli_num_rows($getCardTipRefundInfo) > 0)
			{
				while ($info = mysqli_fetch_assoc($getCardTipRefundInfo))
				{
					$empclassId = $info['emp_class_id'];
					$serverId = $info['server_id'];
					$tipRefundRule = $info['tiprefund_rules'];
					$cardTip = $info['card_tip'];

					$r_vars = array();
					$r_vars['amount'] = ($tipRefundRule * $cardTip) / 100 ;
					$r_vars['refund_percentage'] = '0.000';
					$r_vars['created_date'] = $info['created_date'];
					$r_vars['emp_class_id'] = $empclassId;
					$r_vars['loc_id'] = $_REQUEST['loc_id'];
					$r_vars['server_id'] = $serverId;
					$r_keys = array_keys($r_vars);
					$q_fields = '';
					$q_values = '';
					foreach ($r_keys as $rkey) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$rkey`";
						$q_values .= "'[$rkey]'";
					}
					$q_update = '';
					foreach ($r_keys as $rkey) {
						if ($q_update != "") $q_update .= ", ";
						$q_update .= "`$rkey` = '[$rkey]'";
					}

					$updateOrInsert = 0;
					$check = lavu_query("SELECT `id`,`emp_class_id` FROM `tipout_refunds` WHERE `emp_class_id`=[1] and `created_date` = '[2]'", $empclassId, $use_date);
					if (mysqli_num_rows($check) > 0) {
						$info = mysqli_fetch_assoc($check);
						$r_vars['id'] = $info['id'];
						$update = lavu_query("UPDATE `tipout_refunds` SET $q_update WHERE `id` = '[id]'", $r_vars);
						$updateOrInsert = 1;
					}
					else{
						lavu_query("INSERT INTO `tipout_refunds` ($q_fields) VALUES ($q_values)", $r_vars);
					}
				}
			}

	/*******************************************************************************
	 *               S T A R T   B U I L D I N G   R E P O R T S                   *
	 ******************************************************************************/

	$alt_payment_types = array();
	$get_alt_payment_types = lavu_query("SELECT * FROM `payment_types` WHERE `loc_id` = '[1]' AND `id` > '3' AND `_deleted` != '1' ORDER BY `_order` ASC, `type` ASC", $loc_id);
	if (@mysqli_num_rows($get_alt_payment_types) > 0)
	{
		while ($alt_payment_info = mysqli_fetch_assoc($get_alt_payment_types))
		{
			$alt_payment_types[] = array($alt_payment_info['id'], $alt_payment_info['type']);
		}
	}

	if ($print_string!="")
	{
		$print_string .= "[new_print_job]:";
	}

	$start_print_string = "";
	$start_print_string .= "receipt:";
	if ($register != "")
	{
		$start_print_string = "receipt#".$register.":";
	}
	if (isset($_REQUEST['new_format']) && ($_REQUEST['new_format'] == 1))
	{
		$start_print_string = $register.":";
	}
	$print_string .= $start_print_string;

	$print_string .= "---------------------------:";

	$report_sequence = array();
	if ($mode == "register")
	{
		$report = "Register Summary";
		$report_sequence[] = array("mode"=>$mode, "register"=>$register);
	}
	else if ($mode=="server" || $mode=="server_single")
	{
		$report_title = "Server Summary";
		$report_sequence[] = array("mode"=>$mode, "server_id"=>$report_server_id, "server_name"=>$report_server_name);
		$server_name_line = $report_server_name;
		if (empty($server_name_line))
		{
			$get_server_info = lavu_query("SELECT `f_name`, `l_name` FROM `users` WHERE `id` = '[1]'", $report_server_id);
			if (@mysqli_num_rows($get_server_info) > 0)
			{
				$server_info = mysqli_fetch_array($get_server_info);
				$server_name_line = $server_info['f_name']." ".$server_info['l_name']." :";
			}
		}
	}
	else if ($mode == "x_report")
	{
		$report_title = "X Report";
		$register_list = getRegisterList($loc_id);
		foreach ($register_list as $rl)
		{
			$report_sequence[] = array("group"=>"REGISTERS","mode"=>"register", "register"=>$rl[0], "register_name"=>$rl[1]);
		}
		$server_list = getServerList($loc_id);
		foreach ($server_list as $sl)
		{
			$report_sequence[] = array("group"=>"SERVERS","mode"=>"server", "server_id"=>$sl[0], "server_name"=>$sl[2]." ".$sl[1]);
		}
	}
	else if ($mode == "z_report")
	{
		$report_title = "Z Report";
		$report_sequence[] = array("mode"=>"z_report");
	}

	if (count($rp_array) > 1)
	{
		$print_string .= "     $report_title:";
		if ($rp_details['mode'] == "shift")
		{
			$print_string .= "------ Shift " . $rp . " ------:";
			$time_types = array("start","end");

			for ($t = 0; $t < count($time_types); $t++)
			{
				$print_string .= "Shift ".ucwords($time_types[$t])." - ";
				$t_datetime_parts = explode(" ",$rp_details[$time_types[$t]]);
				$t_date_parts = explode("-",$t_datetime_parts[0]);
				$t_time_parts = explode(":",$t_datetime_parts[1]);
				$t_ts = mktime($t_time_parts[0], $t_time_parts[1], $t_time_parts[2], $t_date_parts[1], $t_date_parts[2], $t_date_parts[0]);

				$print_string .= str_replace(":","[c",date("m/d h:ia",$t_ts));
				$print_string .= ":";
			}

			//$print_string .= "Shift End[c "; . $rp_details['end'];
		}
		else if($rp_details['mode']=="all")
		{
			$print_string .= "------ All Shifts ------:";
		}
		$print_string .= " :";
	}
	else
	{
		$print_string .= "     $report_title: :";
	}
	$print_string .= $business_date." :";
	$print_string .= "(Printed - ".str_replace(":", "[c", $print_datetime).") :";

	if (isset($shift_info['shift_number']) && $shift_info['shift_number']!="")
	{
		$print_string .= " - Shift " . $shift_info['shift_number']." :";
	}
	$print_string .= " :";
	if (!empty($server_name_line))
	{
		$print_string .= $server_name_line." :";
	}
	$print_string .= "Register[c *$register : :";
	$print_string .= "---------------------------:";

	$last_group = "";
	foreach ($report_sequence as $this_report)
	{
		// get the following variables
		$cond = ""; // mode dependent
		//$start_datetime = ""; // changed if mode is z_report, maintain previously set value otherwise
		//$end_datetime = ""; // changed if mode is z_report, maintain previously set value otherwise
		$query_supergroup = ""; // used by x_report to get the supergroup title and stats
		$query_open_orders = ""; // used to get all open orders (for counting the open orders)
		$vars = array(); // date_start, date_end, register, report_server_id, loc_id
		setLargeQueryVars($this_report, $actual_day_start, $actual_day_end, $loc_id, $cond, $start_datetime, $end_datetime, $query_supergroup, $query_open_orders, $vars);

		// print the header for $this_report (eg title, grouping, and subtitle)
		// modifies $print_string and $subtitle_string
		addHeaderStrings($this_report, $mode, $last_group, $print_string, $subtitle_string);

		$till_report_type = (isset($location_info['till_report_type'])) ? $location_info['till_report_type'] : "sales";
		if ($till_report_type=="payments" && $this_report['mode']!="x_report" && $this_report['mode']!="z_report")
		{
			$found_orders = true;

			// print recent payments, their types, and amounts
			addPaymentHistoryStrings($cond, $vars, $subtitle_string, $mode, $print_string);
		}
		elseif($this_report['mode']=="z_report" && strtolower($location_info['country'])=='uruguay'){
			$uruguay_zreports=createZreport($_REQUEST);
			if($uruguay_zreports=='fail')
			{
				$message = '{"status": "fail","Message": "Z-Report not created. No records found"}';
				echo $message;	exit;
			}else{
				//echo "<pre>"; print_r($uruguay_zreports); //	exit;
				$print_string="";
				$decimal_query = lavu_query("select disable_decimal from `locations`");
				$decimal_assoc = mysqli_fetch_assoc($decimal_query);
				$decimal_number = $decimal_assoc['disable_decimal'];
			foreach($uruguay_zreports['data'] as $date =>$uruguay_zreport){
				//echo "<pre>"; print_r($uruguay_zreport); //exit;
				$print_string.="  Z Report::";
				$print_string .= "".$location_info['title'].":";
				$print_string .= "R.U.T No-".$location_info['irs_no']." :";
				$print_string .= "Z Report ID- ".$uruguay_zreports[report_id][$date]." :";
				$print_string .= $date." :";
				$print_string .= "(Printed - ".str_replace(":", "[c", $print_datetime).") :";
				$total_net=0.00;
				$total_tax=0.00;
				$total_vocher=0;
				$full_total=0.00;
				foreach($uruguay_zreport as $key => $zreport){
					$fiscal_title_query =lavu_query("SELECT * FROM `fiscal_invoice_type` WHERE `invoice_type` = '$key'");
					$fiscal_title=mysqli_fetch_assoc($fiscal_title_query);
					$print_string .= " - - - - $fiscal_title[invoice_label] - - - - :";
					$print_string .= " :First Invoice [c *".$zreport[first]." :";
					$print_string .= "Last Invoice [c *".$zreport[last]." :";
					$print_string .= "Voucher Generated [c *".$zreport[vouchers]." :";
					$print_string .= "Net [c *".$zreport[net]." :";
					$print_string .= "Tax [c  *".$zreport[tax]." :";
					$print_string .= "Total [c *".$zreport[total]." :";
					$total_vocher+=$zreport[vouchers];
					if($key=='b2c_refund' || $key=='b2b_refund')
					{
						$total_net=$total_net-$zreport[net];
						$total_tax=$total_tax-$zreport[tax];
						$full_total=$full_total-$zreport[total];
					}else{
						$total_net=$total_net+$zreport[net];
						$total_tax=$total_tax+$zreport[tax];
						$full_total=$full_total+$zreport[total];
					}
				}
				$print_string .= " - - - - Grand Total - - - - :";
				$print_string .= "Voucher Generated [c *".$total_vocher." :";
				$print_string .= "Net [c *".number_format($total_net, $decimal_number, '.', '')." :";
				$print_string .= "Tax [c *".number_format($total_tax, $decimal_number, '.', '')." :";
				$print_string .= "Total [c *".number_format($full_total, $decimal_number, '.', '')." :";
				$print_string .= " - - - - - - - - - - - - - - - :";
			}
				echo "1|receipt:---------------------------:".$print_string; exit;
			}

		}
		else
		{
			addPaymentHistoryStrings_PaidInOut($cond, $vars, $subtitle_string, $mode, $print_string);

			$subTotalQuery = lavu_query("select if(count(*) > 0, SUM(order_contents.subtotal_with_mods - order_contents.itax), '0.00') as `gross` from `orders` left join `order_contents` on `orders`.`order_id` = `order_contents`.`order_id` and `orders`.`location_id` = `order_contents`.`loc_id` left join `menu_items` on `order_contents`.`item_id` = `menu_items`.`id` left join `menu_categories` on `menu_items`.`category_id` = `menu_categories`.`id` left join `super_groups` on if (menu_items.super_group_id = '', menu_categories.super_group_id, menu_items.super_group_id) = `super_groups`.`id` where orders.closed >= '[date_start]' and orders.closed < '[date_end]' and orders.void = '0' AND order_contents.quantity > '0' and order_contents.item != 'SENDPOINT' and order_contents.loc_id = '[loc_id]' ".$cond, $vars);
			$subTotalRead = mysqli_fetch_assoc($subTotalQuery);
			$subTotals = $subTotalRead['gross'];

			$gettingOrdersDataQuery = lavu_query("select sum(`split_check_details`.`rounding_amount`) as `rounding_amount` from `orders` left join `split_check_details` on `orders`.`order_id` = `split_check_details`.`order_id` and `orders`.`location_id` = `split_check_details`.`loc_id` where `orders`.`closed` >='[date_start]' and `orders`.`closed` < '[date_end]' and `orders`.`void` = '0' and `orders`.`location_id` = '[loc_id]' and `orders`.`order_id` not like 'Paid%' ".$cond, $vars);
			$gettingOrdersDataRead = mysqli_fetch_assoc($gettingOrdersDataQuery);
			$roundingAmt = $gettingOrdersDataRead['rounding_amount'];

			$gettingTaxQuery = lavu_query("SELECT `order_contents`.`tax_inclusion`, `order_contents`.`tax_rate1` as tax_rate, `order_contents`.`tax_exempt`, count(order_contents.tax_inclusion) as count, SUM(`order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5`) AS `tax_amount` FROM `orders` LEFT JOIN `order_contents` ON `order_contents`.`order_id` = `orders`.`order_id` WHERE `orders`.`closed` >='[date_start]' AND `orders`.`closed` < '[date_end]' AND `orders`.`void` = '0' ".$cond." AND `orders`.`location_id` = '[loc_id]' AND `orders`.`order_id` NOT LIKE 'Paid%' AND `order_contents`.`quantity` > '0' GROUP BY `order_contents`.`tax_inclusion`, `order_contents`.`tax_exempt` ORDER BY tax_rate DESC", $vars);
			$taxAmt = 0;
			if (mysqli_num_rows($gettingTaxQuery)) {
				while ($get_total_taxes = mysqli_fetch_assoc($gettingTaxQuery)) {
					if ($get_total_taxes['tax_inclusion'] == 0 && $get_total_taxes['tax_exempt'] == 0) {
						$taxAmt = $taxAmt + $get_total_taxes['tax_amount'];
					} elseif ($get_total_taxes['tax_inclusion'] == 1 && $get_total_taxes['tax_exempt'] == 0) {
						$taxAmt = $taxAmt + $get_total_taxes['tax_amount'];
					}
				}
			}

			$gettingTaxExemptionsQuery = lavu_query("SELECT SUM(`order_contents`.`tax1` + `order_contents`.`tax2` + `order_contents`.`tax3` + `order_contents`.`tax4` + `order_contents`.`tax5`) AS `tax_exempt_amount` FROM `order_contents` LEFT JOIN `orders` ON `orders`.`order_id` = `order_contents`.`order_id` WHERE `orders`.`closed` >='[date_start]' AND `orders`.`closed` < '[date_end]' AND `orders`.`void` = '0' AND `orders`.`location_id` = '[loc_id]' AND `orders`.`order_id` NOT LIKE 'Paid%' AND `order_contents`.`tax_exempt` = 1 AND `order_contents`.`tax_inclusion` = 1 ".$cond, $vars);
			$gettingTaxExemptionsRead = mysqli_fetch_assoc($gettingTaxExemptionsQuery);
			$taxExemptAmt = ($gettingTaxExemptionsRead['tax_exempt_amount'] != 0) ? '-'.$gettingTaxExemptionsRead['tax_exempt_amount'] : $gettingTaxExemptionsRead['tax_exempt_amount'];

			$order_query = lavu_query("SELECT `tablename`, `location_id`, `order_id`, `ioid`, `guests`, `idiscount_amount`, (`subtotal` - `itax`) as `subtotal`, `total`, (`itax` + `tax`) as `tax`, `discount`, `cash_applied`, `card_paid`, `gift_certificate`, `alt_paid`, `gratuity`, `card_gratuity`, `cash_tip`, `void`, `no_of_checks`, `togo_delivery_fees` FROM `orders` WHERE `closed` >= '[date_start]' AND `closed` <= '[date_end]' AND `location_id` = '[loc_id]' ".$cond, $vars);

			$order_count = mysqli_num_rows($order_query);
			if ($order_count > 0)
			{
				$found_orders = true;

				$print_string .= $subtitle_string;

				// get a summary of all orders
				$a_immutables = array();
				$a_mutables = array(
					'cash_paid'			=> 0,
					'card_paid'			=> 0,
					'cash_tips'			=> 0,
					'card_tips'			=> 0,
					'discounts'			=> 0,
					'gift_certificates'	=> 0,
					'gratuity'			=> 0,
					'guests'			=> 0,
					'idiscounts_total'	=> 0,
					'other_tip_info'	=> array(),
					'other_tips'		=> 0,
					'relevant_orders'	=> array(),
					'subtotal'			=> 0,
					't_ag_cash'			=> 0,
					't_ag_card'			=> 0,
					't_ag_other'		=> 0,
					'tax'				=> 0,
					'total'				=> 0,
					'void_count'		=> 0,
					'void_total'		=> 0,
					'togo_delivery_fees'=> 0
				);

				while ($order_read = mysqli_fetch_assoc($order_query))
				{
					processOrderGetTotals($order_read, $a_immutables, $a_mutables, $alt_paid);
				}

				$cash_paid			= sprintf("%0.4f", $a_mutables["cash_paid"]);
				$card_paid			= sprintf("%0.4f", $a_mutables["card_paid"]);
				$cash_tips			= sprintf("%0.4f", $a_mutables["cash_tips"]);
				$card_tips			= sprintf("%0.4f", $a_mutables["card_tips"]);
				$discounts			= sprintf("%0.4f", $a_mutables["discounts"]);
				$idiscounts_total	= sprintf("%0.4f", $a_mutables["idiscounts_total"]);
				$gratuity			= sprintf("%0.4f", $a_mutables["gratuity"]);
				$guests				= $a_mutables["guests"];
				$gift_certificates	= sprintf("%0.4f", $a_mutables["gift_certificates"]);
				$other_tip_info		= $a_mutables["other_tip_info"]; // array
				$other_tips			= sprintf("%0.4f",$a_mutables["other_tips"]);
				$relevant_orders	= $a_mutables["relevant_orders"]; // array
				$subtotal			= sprintf("%0.4f", $a_mutables["subtotal"]);
				$t_ag_cash			= sprintf("%0.4f", $a_mutables["t_ag_cash"]);
				$t_ag_card			= sprintf("%0.4f", $a_mutables["t_ag_card"]);
				$t_ag_other			= sprintf("%0.4f", $a_mutables["t_ag_other"]);
				$tax				= sprintf("%0.4f", $a_mutables["tax"]);
				$total				= sprintf("%0.4f", $a_mutables["total"]);
				$void_count			= $a_mutables["void_count"];
				$void_total			= sprintf("%0.4f", $a_mutables["void_total"]);
				$togo_delivery_fees = $a_mutables["togo_delivery_fees"];

				// add in the totals of cc transactions tips to
				// other_tips, other_tip_info
				$check_other_tips = lavu_query("SELECT `pay_type_id`, `pay_type`, `action`, `tip_amount` FROM `cc_transactions` WHERE `loc_id` = '[1]' AND (`pay_type_id` * 1) > 2 AND `order_id` IN ('".implode("','", $relevant_orders)."') AND (`tip_amount` * 1) != 0", $loc_id);
				if (mysqli_num_rows($check_other_tips) > 0)
				{
					while ($info = mysqli_fetch_assoc($check_other_tips))
					{
						processCCTransactionGetTips($info, $other_tips, $other_tip_info);
					}
				}

				//START LP-6232-Dual Cash Register: Reporting: View (Print) Server Summary
                if ($mode=="server" || $mode=="server_single") {
					$cc_query = lavu_query("SELECT `cc_transactions`.`drawer`,`cc_transactions`.`register`,sum(`cc_transactions`.`amount`) as `amount` FROM `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` AND `cc_transactions`.`loc_id` = `orders`.`location_id` 
					WHERE `orders`.`closed` >= '[1]' AND `orders`.`closed` <= '[2]' AND `orders`.`location_id` = '[3]' AND `orders`.`server_id` = '[4]' AND `cc_transactions`.`pay_type_id` = '1' AND `orders`.`void` = '0' AND (`cc_transactions`.`action` = 'Sale' OR `cc_transactions`.`action` = 'Refund') AND `cc_transactions`.`voided` != '1'  GROUP BY `cc_transactions`.`drawer`, `cc_transactions`.`register` ORDER BY `cc_transactions`.`register`, `cc_transactions`.`drawer`", $vars['date_start'],$vars['date_end'],$vars['loc_id'],$vars['report_server_id']);
                } else if ( $mode == "register") {
					$cc_query = lavu_query("SELECT `cc_transactions`.`drawer`,`cc_transactions`.`register`,sum(`cc_transactions`.`amount`) as `amount` FROM `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` AND `cc_transactions`.`loc_id` = `orders`.`location_id`
					WHERE `orders`.`closed` >= '[1]' AND `orders`.`closed` <= '[2]' AND `orders`.`location_id` = '[3]' AND `cc_transactions`.`register` = '[4]' AND `cc_transactions`.`pay_type_id` = '1' AND `orders`.`void` = '0' AND (`cc_transactions`.`action` = 'Sale' OR `cc_transactions`.`action` = 'Refund') AND `cc_transactions`.`voided` != '1' GROUP BY `cc_transactions`.`drawer`", $vars['date_start'],$vars['date_end'],$vars['loc_id'],$vars['register']);
                }

				if (mysqli_num_rows($cc_query) > 0) {
					$registers = array();
					$print_string .= ": - - - - Cash Drawers - - - - :";
					while ($info = mysqli_fetch_assoc($cc_query)) {
						$info['drawer'] = ($info['drawer'] == 0)?1:$info['drawer'];
						$registers[$info['register']][$info['drawer']] += $info['amount'] - $info['refunded'];
					}            
					foreach ( $registers as $register=>$drawer ){
						foreach ($drawer as $key=>$totalAmount){
							$print_string .= $register."-Drawer #".$key."[c *".showAmount($totalAmount)." :";
						}
					}            
					$print_string .= "---------------------------:";
				}

				//END LP-6232-Dual Cash Register: Reporting: View (Print) Server Summary

				// print the counts about the orders
				$oo_query = lavu_query($query_open_orders, $vars);
				$open_order_count = mysqli_num_rows($oo_query);
				$perPerson = sprintf('%0.4f', (($subtotal - $discounts) / (int)$guests));
				$print_string .= " :
Guest Count [c *".(int)$guests." :
Per Person Avg [c *".showAmount($perPerson)." :
 :
Void Count [c *".$void_count." :
Void Total [c *".showAmount($void_total)." :
 :
Orders Still Open [c *".$open_order_count." :";

				//start LP-5324
				if ($this_report['mode']=="server" || $this_report['mode']=="server_single") {

					$ordersList = lavu_query("SELECT `order_id`, ROUND((`cash_paid` + `card_paid` + `gift_certificate` + `alt_paid` - `total`) , 3) as overpaid_amount,
                    ROUND(`total` - (`cash_paid` + `card_paid` + `gift_certificate` + `alt_paid` ) , 3 ) as `underpaid_amount`,
					if ((ROUND((`cash_paid` + `card_paid` + `gift_certificate` + (`alt_paid` - `alt_refunded` )) , 3) > total),1,0) as 'overpaid',
					if ((ROUND((`cash_paid` + `card_paid` + `gift_certificate` + (`alt_paid` - `alt_refunded` )) , 3) < total),1,0) as 'underpaid'
					from `orders`
					WHERE ((ROUND((`cash_paid` + `card_paid` + `gift_certificate` + (`alt_paid` - `alt_refunded` ) ) , 3) > `total`) OR (ROUND((`cash_paid` + `card_paid` + `gift_certificate` + (`alt_paid` - `alt_refunded` ) - `total`) , 3) < `total`))  AND `location_id` = '[1]' AND `server_id` = '[2]' AND `closed` >= '[3]' and `closed` < '[4]' and `void` = '0' ", $vars['loc_id'], $vars['report_server_id'], $vars['date_start'], $vars['date_end'] );

					if (mysqli_num_rows($ordersList) > 0) {
						$overpaid_string ="";
						$underpaid_string ="";
						while ($order_info = mysqli_fetch_assoc($ordersList)) {
							if ($order_info['overpaid'] == 1){
								$overpaid_string .= $order_info['order_id']."* ".showAmount($order_info['overpaid_amount'])." :";
							}
							elseif($order_info['underpaid'] == 1){
								$underpaid_string .= $order_info['order_id']."* ".showAmount($order_info['underpaid_amount'])." :";
							}
						 }
						 if ($overpaid_string){
						    $print_string .= " : - - - - Overpaid Orders - - - - : Order ID"."* "."Amount"." :".$overpaid_string;
						 }
						 if ($underpaid_string){
						     $print_string .= " : - - - - Underpaid Orders - - - - : Order ID"."* "."Amount"." :".$underpaid_string;
						 }
						 $print_string .= "---------------------------:";
					}
				}
				//end LP-5324
$subtotalResult = sprintf('%0.4f', ($subTotals - $idiscounts_total));
$totalResult = sprintf('%0.4f', ($subTotals - ($idiscounts_total + $discounts) + $tax + $roundingAmt + $taxExemptAmt));
				// print the totals paid in each category
				$print_string .= " :
Total Sales [c *".showAmount($subTotals)." :
Item Discounts [c *".showAmount($idiscounts_total)." :
Subtotal [c *".showAmount($subtotalResult)." :
Check Discounts [c *".showAmount($discounts)." :
Tax [c *".showAmount($tax)." :
Tax Exemptions (Included Tax) [c *".showAmount($taxExemptAmt)." :
Rounding Amount [c *".showAmount($roundingAmt)." :
Total [c *".showAmount($totalResult)." :";

				// print totals paid for each pay type
				$print_string .= " :
Cash Sales [c *".showAmount($cash_paid)." :
Card Sales [c *".showAmount($card_paid)." :
Gift Certificates [c *".showAmount($gift_certificates)." :";
				for ($ap = 0; $ap < count($alt_payment_types); $ap++) {
					$ptype = $alt_payment_types[$ap];
					$print_string .= $ptype[1]." [c *".showAmount(get_alt_total($loc_id, $ptype[0], $vars, $mode)). " :";
				}

				// fix the print string
				$print_string = str_replace(array("\n","\r"), "", $print_string);
				$print_string = str_replace("				", "", $print_string);

				if (($this_report['mode']=="server" || $this_report['mode']=="server_single") && $mode!="x_report" && ($location_info['integrateCC']=="1" || $location_info['get_card_info']!="0")) {
					$cc_query = lavu_query("SELECT `amount`, `tip_amount`, `card_type`, `action` FROM `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id` AND `cc_transactions`.`loc_id` = `orders`.`location_id` WHERE `orders`.`closed` >= '[date_start]' AND `orders`.`closed` <= '[date_end]' AND `orders`.`location_id` = '[loc_id]' AND `orders`.`server_id` = '[report_server_id]' AND `cc_transactions`.`pay_type_id` = '2' AND `orders`.`void` = '0' AND `cc_transactions`.`action` != '' AND `cc_transactions`.`action` != 'Void' AND `cc_transactions`.`voided` != '1' ORDER BY `card_type` ASC", $vars);
					if (mysqli_num_rows($cc_query) > 0) {
						$print_string .= " :";
						$print_string .= " - - - - CC Breakdown - - - - :";
						$cc_info = array();
						$cc_sale_count = 0;
						$cc_return_count = 0;
						$cc_total = 0;
						while ($info = mysqli_fetch_assoc($cc_query)) {
							$type = $info['card_type'];
							if (!isset($cc_info[$type])) $cc_info[$type] = array(0, 0, 0, 0);
							if ($info['action']=="Sale") {
								$cc_sale_count++;
								$cc_info[$type][0]++;
								$cc_info[$type][2] += $info['amount'];
								$cc_info[$type][3] += $info['tip_amount'];
								$cc_total += ($info['amount'] + $info['tip_amount']);
							} else if ($info['action']=="Refund") {
								$cc_return_count++;
								$cc_info[$type][1]++;
								$cc_info[$type][2] -= $info['amount'];
								$cc_info[$type][3] -= $info['tip_amount'];
								$cc_total -= ($info['amount'] + $info['tip_amount']);
							}
						}
						$card_types = array_keys($cc_info);
						foreach ($card_types as $type) {
							$print_string .= $type." Sale Count [c *".$cc_info[$type][0]." :";
							if ($cc_info[$type][1] > 0) $print_string .= $type." Return Count [c *".$cc_info[$type][1]." :";
							$print_string .= $type." Amount [c *".showAmount($cc_info[$type][2])." :";
							$print_string .= $type." Tips [c *".showAmount($cc_info[$type][3])." :";
							$ccInfoTotal = sprintf('%0.4f', ($cc_info[$type][2] + $cc_info[$type][3]));
							$print_string .= $type." Total [c *".showAmount($ccInfoTotal)." : :";
						}
						$cc_total = sprintf('%0.4f', $cc_total);
						$print_string .= "All CC Sale Count [c *".$cc_sale_count." :";
						if ($cc_return_count > 0) $print_string .= "All CC Return Count [c *".showAmount($cc_return_count)." :";
						$print_string .= "All CC Total [c *".showAmount($cc_total)." :";
					}
				}

				if ($this_report['mode']=="z_report") {
					$should_settle_batch = ($location_info['z_report_triggers_cc_batch_settlement']=="1");

					$q_fields = "";
					$q_values = "";
					$l_vars = array();
					$l_vars['action'] = "Ran Z Report";
					$l_vars['user'] = $_REQUEST['admin_username'];
					$l_vars['user_id'] = $_REQUEST['admin_user_id'];
					$l_vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
					$l_vars['order_id'] = "0";
					$totalSalesResult = sprintf('%o.4f', ($subtotal + $idiscounts_total));
					$l_vars['data'] = "Total Sales: ".showAmount($totalSalesResult);
					$l_vars['detail1'] = "Card Sales: ".showAmount($card_paid);
					$l_vars['detail2'] = "Settle CC Batch:";
					$l_vars['detail3'] = $should_settle_batch?"Yes":"No";
					$l_vars['loc_id'] = $loc_id;
					$l_vars['time'] = localize_datetime(date("Y-m-d H:i:s"), $location_info['timezone']);
					$keys = array_keys($l_vars);
					foreach ($keys as $key) {
						if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
						$q_fields .= "`$key`";
						$q_values .= "'[$key]'";
					}
					$log_this = lavu_query("INSERT INTO `admin_action_log` ($q_fields, `server_time`) VALUES ($q_values, now())", $l_vars);
					$last_inserted_admin_action_log_id = lavu_insert_id();

					if ($should_settle_batch)
					{
						require_once(__DIR__."/gateway_functions.php");
						require_once(__DIR__."/mpshc_functions.php");

						$process_info = array();
						$process_info['mag_data'] = "";
						$process_info['reader'] = "";
						$process_info['card_number'] = "";
						$process_info['card_cvn'] = "";
						$process_info['name_on_card'] = "";
						$process_info['exp_month'] = "";
						$process_info['exp_year'] = "";
						$process_info['card_amount'] = "";
						$process_info['company_id'] = $company_info['id'];
						$process_info['loc_id'] = $loc_id;
						$process_info['ext_data'] = "";
						$process_info['data_name'] = $data_name;
						$process_info['register'] = $register;
						$process_info['register_name'] = $register_name;
						$process_info['device_udid'] = determineDeviceIdentifier($_REQUEST);
						$process_info['server_id'] = $_REQUEST['admin_user_id'];
						$process_info['server_name'] = $_REQUEST['admin_name'];
						$process_info['device_time'] = $datetime;
						$process_info['set_pay_type'] = "";
						$process_info['set_pay_type_id'] = "";
						$process_info['for_deposit'] = "";

						$dataname = $data_name;
						$companyid = $company_info['id'];
						$locationid = $loc_id;
						$serverid = $_REQUEST['admin_user_id'];
						$username = $_REQUEST['admin_username'];

						require_once(resource_path()."/../areas/reports/settle_batch.php");

						$print_string .= " :";
						$print_string .= " - - - CC Batch - - - : ";
						if (isset($till_report_info['result'])) $print_string .= processBatchResults($till_report_info);
						else $print_string .= "No Info :";
					}
				}

				if (!($mode=="x_report" && $this_report['mode']=="register")) {
					$print_string .= " :";
					$print_string .= " - - - - Auto Gratuity - - - - :";
					$print_string .= "Cash [c *".showAmount($t_ag_cash)." :";
					$print_string .= "Card [c *".showAmount($t_ag_card)." :";
					$print_string .= "Other [c *".showAmount($t_ag_other)." :";
					$totalAgResult = sprintf('%0.4f', ($t_ag_cash + $t_ag_card + $t_ag_other));
					$print_string .= "Total [c *".showAmount($totalAgResult)." :";

					$print_string .= " :";
					$print_string .= " - - - - Tips - - - - :";
					//$print_string .= "Cash Tips [c *".showAmount($todays_cash_tips + $cash_tips)." :";
					if($_REQUEST['m'] == "register"){
						//DON'T REPORT CASH TIPS
					}
					else{
				        if ( isset($vars['report_server_id']) && $vars['report_server_id']!="" ) {
				            $queryResult = lavu_query("SELECT SUM(`cash_tip`) as cashTips  FROM `orders` WHERE `closed` >= '[1]' AND `closed` < '[2]' AND `location_id` = '[3]' AND `server_id` = '[4]' AND `void` = '0'", $vars['date_start'], $vars['date_end'], $vars['loc_id'], $vars['report_server_id']);
				        }
				        else {
				            $queryResult = lavu_query("SELECT SUM(`cash_tip`) as cashTips  FROM `orders` WHERE `closed` >= '[1]' AND `closed` < '[2]' AND `location_id` = '[3]' AND `void` = '0'", $vars['date_start'], $vars['date_end'], $vars['loc_id']);
				        }
				        $infoTips = mysqli_fetch_assoc($queryResult);
				        if( $infoTips['cashTips'] > 0 ){
				            $cash_tips = floatval($infoTips['cashTips']);
				        }
					    $print_string .= "Cash Tips [c *".showAmount($cash_tips)." :";
					}
					
					//Card Tip Calculation
					$totals = array();
					$totals['card_tips'] = 0;
					if ( isset($vars['report_server_id']) && $vars['report_server_id']!="" ) {
					   $order_query = lavu_query("SELECT `card_gratuity` FROM `orders` WHERE `closed` >= '[1]' AND `closed` < '[2]' AND `location_id` = '[3]' AND `server_id` = '[4]' AND `void` = '0'", $vars['date_start'], $vars['date_end'], $vars['loc_id'], $vars['report_server_id']);
					}
					else if ( $_REQUEST['m'] == "register" ) {
					    $registerCond = "";
					    if ($_REQUEST['register']=="Not set" || $_REQUEST['register']=="receipt" || (isset($_REQUEST['register']) && $_REQUEST['register']!="" )) {
					        $registerCond = "AND (`orders`.`register` = '[4]' OR `orders`.`register` = 'Not set')";
					        $order_query = lavu_query("SELECT `card_gratuity` FROM `orders` WHERE `closed` >= '[1]' AND `closed` < '[2]' AND `location_id` = '[3]'  AND `void` = '0' $registerCond ", $vars['date_start'], $vars['date_end'], $vars['loc_id'], $vars['register']);
					    }
					}
					else {
					    $order_query = lavu_query("SELECT `card_gratuity` FROM `orders` WHERE `closed` >= '[1]' AND `closed` < '[2]' AND `location_id` = '[3]' AND `void` = '0'", $vars['date_start'], $vars['date_end'], $vars['loc_id']);
					}
					$order_count = mysqli_num_rows($order_query);
					
					if ($order_count > 0) {
					    while ($order_read = mysqli_fetch_assoc($order_query)) {
					        $totals['card_tips'] += $order_read['card_gratuity'];
					    }
					    $card_tips = sprintf('%0.4f', $totals['card_tips']);
					}
					//End Card Tip Calculation
					
					$print_string .= "Card Tips [c *".showAmount($card_tips)." :";
					$payPalTip = 0;
					foreach ($other_tip_info as $key => $val) {
						if ($key == 'PayPal') {
							$payPalTip = $payPalTip + $val;
                        }
						$print_string .= $key." Tips [c *".showAmount($val)." :";
					}
					//$print_string .= "Total Tips [c *".showAmount($todays_cash_tips + $cash_tips + $card_tips + $other_tips)." :";

					$print_string .= $todays_cash_deposits_str;
				}

				if ($this_report['mode']=="server" || $this_report['mode']=="server_single") {
					//$check_tipouts = lavu_query("SELECT `type`,`type2`, `typeid2`, `value`,`value2` FROM `tipouts` WHERE `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $vars['loc_id'], $vars['report_server_id'], substr($vars['date_start'], 0, 10));// old query backup 
					$check_tipouts = lavu_query("SELECT `type`, `typeid` ,`percentage`, `type2`, `typeid2`, `value`,`value2`,`sales_tipout` FROM `tipouts` WHERE `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $vars['loc_id'], $vars['report_server_id'], substr($vars['date_start'], 0, 10));
					if (mysqli_num_rows($check_tipouts) > 0) {
						$print_string .= " :";
						$print_string .= " - - - - Tip Out - - - - :";
						$totalval = 0;
						$totalval2 = 0;
						while ($tpt_info = mysqli_fetch_assoc($check_tipouts)) {
							$val = $tpt_info['value'];
							if($tpt_info['value2'] > 0){
								$val = $tpt_info['value'] + $tpt_info['value2'];
							}
							if($val == 0){
								$val = $tpt_info['sales_tipout'];
							}
							if ($tpt_info['type'] !='' && $tpt_info['type2'] != ''){
								$val = ($val / 2);
							}
							$val = sprintf('%0.4f', $val);
							if (!empty($tpt_info['typeid'])) {
								$changeVal = $val;
								$totalval += $val;
								$print_string .= $tpt_info['type']."(".($tpt_info['percentage']*100)."%)[c ".showAmount($changeVal)." :";
							}

							if (!empty($tpt_info['typeid2'])) {
								$changeVal2 = $val;
								$totalval2 += $changeVal2;
								$print_string .= $tpt_info['type2']."(".($tpt_info['percentage']*100)."%)[c ".showAmount($changeVal2)." :";
							}
						}
					}
				}
				
				$breakdown_tipouts = lavu_query("SELECT `breakdown_detail` FROM `tipouts` WHERE `loc_id` = '[1]' AND `server_id` = '[2]' AND `date` = '[3]'", $vars['loc_id'], $vars['report_server_id'], substr($vars['date_start'], 0, 10));
				if (mysqli_num_rows($breakdown_tipouts) > 0) {
					$print_string .= " :";
					$breakdown_string = '';
					//$print_string .= " - - - - TipOut Breakdown - - - - :";
					while ($tpt_info = mysqli_fetch_assoc($breakdown_tipouts)) {
						$info = $tpt_info['breakdown_detail'];
						$userData = json_decode($info, true);
						 
						foreach ($userData  as $userKey => $userVals) {
							if (is_array($userVals)) {
								foreach ($userVals  as $key => $breakdownVals) {
									if ($breakdownVals['amount'] > 0) {
										$breakdown_string .= $breakdownVals['username']."[c".showAmount($breakdownVals['amount'])." :";
									}
								}
							}
						}
					}
				}
				if ($breakdown_string != '') {
					$print_string .= " - - - - TipOut Breakdown - - - - :";
					$print_string .= $breakdown_string;
				}
				
				//start Tips Summary LP-5836
				if ($this_report['mode']=="server" || $this_report['mode']=="server_single" || $this_report['mode']=="x_report") {
				    $refund_amount = lavu_query("SELECT sum(`amount`) as `amount` FROM `tipout_refunds` WHERE `loc_id` = '[1]' AND `created_date` = '[2]' AND `server_id` = '[3]'", $vars['loc_id'], substr($vars['date_start'], 0, 10), $vars['report_server_id']);
					if (mysqli_num_rows($refund_amount) > 0) {
						$refund_info = mysqli_fetch_assoc($refund_amount);
					}
						$print_string .= " : - - - - Tips Summary - - - - :";
						$print_string .= " Gross Tips [c ".showAmount($cash_tips + $card_tips)." :";
						$print_string .= " Tip Refunds [c ".showAmount($refund_info['amount'])." :"; //Tip Refunds (Returns)
						$print_string .= " Tip Outs [c ".showAmount($totalval + $totalval2 + $sales_tipout)." :";  //Tip Outs (total of all the tip outs given out to all job roles, example: busser, kitchen boy etc)
						$print_string .= " Net Tips [c ".showAmount(($cash_tips + $card_tips) - ($refund_info['amount']) - ($totalval + $totalval2))." :"; //Net Tips = Gross Tips - Tip Refunds - Tip Out (total)
				}
				//End LP-5836

				//$owed_to_house = ($cash_paid - $t_ag_cash - $t_ag_card - $t_ag_other - $card_tips);
				if (isset($location_info['server_summary_include_owed']) && $location_info['server_summary_include_owed']=="1" && ($this_report['mode']=="server" || $this_report['mode']=="server_single"|| $this_report['mode']=="x_report")) {
					//$print_string .= " :";
					//$owed_to_house = ($cash_paid - $t_ag_cash);
					$servers_tip='';
					if ($rp_details['role_id']!='') {
						$server_query = lavu_query("SELECT include_server_owes FROM `emp_classes` WHERE  `id` = '[1]' ",$rp_details['role_id']);
						$server_read = mysqli_fetch_assoc($server_query);
						$servers_tip = $server_read['include_server_owes'];
					}
					//$owed_to_house = ($cash_paid - $card_tips);
					if ($servers_tip=='1') {
						$tipouts = $totalval + $totalval2;
						if ($card_tips > $cash_paid) {
							$owed_to_house = $card_tips+ $cash_tips - $tipouts;
						} else{
							$owed_to_house = $cash_paid + $cash_tips - $tipouts;
						}
						if ($owed_to_house < 0) $print_string .= "House owes Server [c *".showAmount(abs($owed_to_house))." :";
						else $print_string .= "Server owes House [c *".showAmount($owed_to_house)." :";
						$owed_to_house_with_ag = ($cash_paid - $card_tips - $gratuity);
						if($owed_to_house_with_ag * 1 != $owed_to_house * 1)
						{
							if ($owed_to_house_with_ag < 0) $print_string .= "House owes Server (AG to Server) [c *".showAmount(abs($owed_to_house_with_ag))." :";
							else $print_string .= "Server owes House (AG to Server) [c *".showAmount($owed_to_house_with_ag)." :";
						}
					} else {
						$owed_to_house = ($cash_paid - $card_tips - $payPalTip);
						if ($owed_to_house < 0) {
							$print_string .= "House owes Server [c *".showAmount(abs($owed_to_house))." :";
						} else {
							$print_string .= "Server owes House [c *".showAmount($owed_to_house)." :";
						}
						
						$owed_to_house_with_ag = ($cash_paid - $card_tips - $gratuity);
						if($owed_to_house_with_ag * 1 != $owed_to_house * 1)
						{
							if ($owed_to_house_with_ag < 0) {
								$print_string .= "House owes Server (AG to Server) [c *".showAmount(abs($owed_to_house_with_ag))." :";
							} else {
								$print_string .= "Server owes House (AG to Server) [c *".showAmount($owed_to_house_with_ag)." :";
							}
						}
					}
				}
				
				//LP-5837 changes
				$print_string .= " :";
				if ($mode == "z_report") {
				    $z_tipouts = lavu_query("SELECT `type`, `value`, `value2`, `type2`,`sales_tipout` FROM `tipouts` WHERE `loc_id` = '[1]' AND `date` = '[2]'", $vars['loc_id'], substr($vars['date_start'], 0, 10));
				    if (mysqli_num_rows($z_tipouts) > 0) {
				        $print_string .= " :";
				        $print_string .= " - - - - Tip Out - - - - :";
				        while ($ztpt_info = mysqli_fetch_assoc($z_tipouts)) {
				            $val1 = $ztpt_info['value'];
				            $val2 = $ztpt_info['value2'];
				            $type = $ztpt_info['type'];
				            $type2 = $ztpt_info['type2'];
				            $val = $val1 + $val2;
				            if($val == 0){
				                $val = $ztpt_info['sales_tipout'];
				            }
				            if($type2!=""){
				                $type = $type.",".$type2;
				            }
				            $print_string .= $type."[c ".showAmount($val)." :";
				        }
				    }
				    $print_string .= " :";
				    $tipRefund = lavu_query("SELECT SUM(`amount`) as 'totalRefund' from `tipout_refunds` WHERE `loc_id` = '[1]' AND `created_date` = '[2]'", $vars['loc_id'], substr($vars['date_start'], 0, 10));
				    if (mysqli_num_rows($tipRefund) > 0) {
				        $print_string .= " :";
				        $print_string .= " - - - - Tip Refund - - - - :";
				        $refund_tip = mysqli_fetch_assoc($tipRefund);
				        $refundTxt = "Refund total";
				        $refundAmount = $refund_tip['totalRefund'];
				        $print_string .= $refundTxt."[c ".showAmount($refundAmount)." :";
				    }
					if ($togo_delivery_fees > 0) {
					    $print_string .= " :";
					    $print_string .= " - - - - Charge - - - - :";
					    $togoDeliveryFeeTxt = "LTG Delivery Fee Total";
					    $print_string .= $togoDeliveryFeeTxt."[c ".showAmount($togo_delivery_fees)." :";
					}
				}

				$print_string .= " :";
				if ($mode != "x_report") {
					$sg_query = lavu_query($query_supergroup,$vars);
					if (mysqli_num_rows($sg_query) > 0) {
						$sg_header_shown = false;
						while ($sg_read = mysqli_fetch_assoc($sg_query)) {
							//field6 - supergroup title
							//field14 - subtotal
							//field15 - discount amount
							//field16 - subtotal after discount
							//field17 - tax amount
							//field18 = total
							if (trim($sg_read['field6'])=="") {
								$sg_read['field6'] = "No Super Group";
							}

							if (trim($sg_read['field6'])!="") {
								if (!$sg_header_shown) {
									$sg_header_shown = true;
									$print_string .= "---------------------------:";
									$print_string .= " :";
									$print_string .= "     SUPER GROUPS :";
								}
								$print_string .= ":";
								$print_string .= " - - - - ".$sg_read['field6']." - - - - :";
								$subtotal_xreport = sprintf('%0.4f',($sg_read['field14'] - $sg_read['itax']));
								$print_string .= "Subtotal [c *".showAmount($subtotal_xreport)." :";
								$print_string .= "Discount [c *".showAmount($sg_read['field15'])." :";
								$afterDiscount = sprintf('%0.4f',($sg_read['field16'] - $sg_read['itax']));
								$print_string .= "After Discount [c *".showAmount($afterDiscount)." :";
								$print_string .= "Tax [c *".showAmount($sg_read['field17'] + $sg_read['itax'])." :";
								$print_string .= "Total [c *".showAmount($sg_read['field18'])." :";
							}
						}
					}
				}
				$print_string .= " :";
				$print_string .= " :";
				$print_string .= " :";
				$print_string .= " :";
				$print_string .= "---------------------------:";

			}
		}
	}
}

if (!$found_orders) $print_string = "ALERT|No orders found for this day";

/*******************************************************************************
 *                      P R I N T   T H E   R E P O R T                        *
 ******************************************************************************/

if(isset($_REQUEST['debug'])) {
	echo "start datetime: " . $start_datetime . "<br>end datetime: " . $end_datetime . "<br><br>";
	$print_string = str_replace(":","<br>",$print_string);
	$print_string = str_replace("[c",":",$print_string);
	$print_string = str_replace("*","\t",$print_string);
	$print_string = str_replace("[s","*",$print_string);
	echo $print_string;
} else if (!$isNorway) {
    echo "1|".$print_string;
}
//echo $print_string;
//die;
//echo $data_name;
if($isNorway && ($mode == "x_report" || $mode == 'z_report')) {
    include __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'cp'. DIRECTORY_SEPARATOR . 'areas' . DIRECTORY_SEPARATOR . 'norwayReport.php';
}

exit();

/*******************************************************************************
 *                            F U N C T I O N S                                *
 ******************************************************************************/

function showAmount($amount) {

	global $location_info;

	$amount = str_replace(",","",$amount);

	$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
	//$thousands_char = isset($location_info['thousands_char']) ? $location_info['thousands_char'] : '.';
	$thousands_char	= ($decimal_char == ".")?",":".";

	/*if ($location_info['left_or_right'] == "right") return number_format((float)$amount, $location_info['disable_decimal'], $location_info['decimal_char'], ",").$location_info['monitary_symbol'];
	else return $location_info['monitary_symbol'].number_format((float)$amount, $location_info['disable_decimal'], $location_info['decimal_char'], ",");*/
	if ($location_info['left_or_right'] == "right") return number_format((float)$amount, $location_info['disable_decimal'], $decimal_char, $thousands_char).$location_info['monitary_symbol'];
	else return $location_info['monitary_symbol'].number_format((float)$amount, $location_info['disable_decimal'], $decimal_char, $thousands_char);
}

function get_alt_total($loc_id, $type_id, $vars, $mode) {
	//error_log( "Mode: {$mode}" );
	$vars['type_id'] = $type_id;
	$cond = " AND `orders`.`register` = '[register]'";
	if (($vars['register'] == "Not set") || ($vars['register'] == "receipt")) $cond = "AND (`orders`.`register` = '[register]' OR `orders`.`register` = 'Not set')";
	if ($mode=="server" || $mode=="server_single") $cond = " AND `orders`.`server_id` = '[report_server_id]'";
	if( $mode == 'z_report' ){
		$cond = "";
	}
	$total_query = lavu_query("SELECT SUM(CASE WHEN `cc_transactions`.`action`='Refund' THEN (`cc_transactions`.`total_collected` * -1) WHEN `cc_transactions`.`action`='Sale' THEN (`cc_transactions`.`total_collected`) ELSE 0 END) AS `sum_total` FROM `cc_transactions` LEFT JOIN `orders` ON `orders`.`order_id` = `cc_transactions`.`order_id` WHERE `orders`.`closed` >= '[date_start]' AND `orders`.`closed` < '[date_end]' AND `orders`.`void` = '0' AND `cc_transactions`.`voided` = '0' AND `cc_transactions`.`loc_id` = '[loc_id]' AND `cc_transactions`.`pay_type_id` = '[type_id]' $cond", $vars);
	if (mysqli_num_rows($total_query) > 0) {
		$total_read = mysqli_fetch_assoc($total_query);
		return $total_read['sum_total'];
	}
	else return 0;
}

function getRegisterList($loc_id) {

	$register_list = array(array("receipt","Register 1"));
	$get_registers = lavu_query("SELECT `setting`, `value2` FROM `config` WHERE `location` = '[1]' AND `type` = 'printer' AND `setting` LIKE 'receipt%' AND `_deleted` != '1' ORDER BY `setting` ASC", $loc_id);
	if (@mysqli_num_rows($get_registers) > 0) {
		$register_list = array();
		while ($info = mysqli_fetch_assoc($get_registers)) {
			$register_list[] = array($info['setting'], $info['value2']);
		}
	}

	return $register_list;
}

function getServerList($loc_id) {

	$server_list = array();
	$get_servers = lavu_query("SELECT `id`, `l_name`, `f_name` FROM `users` WHERE `_deleted` != '1' AND (`loc_id` = '0' OR `loc_id` = '[1]' OR `access_level` = '4') ORDER BY `l_name` ASC, `f_name` ASC", $loc_id);
	while ($info = mysqli_fetch_assoc($get_servers)) {
		$server_list[] = array($info['id'], $info['l_name'], $info['f_name']);
	}

	return $server_list;
}

function processBatchResults($info) {

	$str = "";
	$add2key = array();
	$add2key[] = "";
	if (isset($info['result_hc'])) $add2key[] = "_hc";

	foreach ($add2key as $a2k) {
		$str .= " :";
		if ($a2k == "_hc") $str .= "HOSTED CHECKOUT :";
		if ($info['result'.$a2k] == "1") {
			$str .= "Success :";
			if (!empty($info['batch_no'.$a2k])) $str .= $info['batch_no'.$a2k]." :";
			if (!empty($info['batch_total'.$a2k])) $str .= "Batch Total [c *".showAmount($info['batch_total'.$a2k])." :";
			if (!empty($info['details'.$a2k])) {
				$details = explode(":.:", $info['details'.$a2k]);
				$str .= "Batch Item Count [c *".$details[0]." :";
				$str .= "Net Batch Total [c *".showAmount($details[1])." :";
				$str .= "Credit Purchase Count [c *".$details[2]." :";
				$str .= "Credit Purchase Amount [c *".showAmount($details[3])." :";
				$str .= "Credit Return Count [c *".$details[4]." :";
				$str .= "Credit Return Amount [c *".showAmount($details[5])." :";
				$str .= "Debit Purchase Count [c *".$details[6]." :";
				$str .= "Debit Purchase Amount [c *".showAmount($details[7])." :";
				$str .= "Debit Return Count [c *".$details[8]." :";
				$str .= "Debit Return Amount [c *".showAmount($details[9])." :";
			}
		} else $str .= str_replace(":", " [c ", $info['response'.$a2k])." :";
	}

	return $str;
}

function setLargeQueryVars($this_report, $actual_day_start, $actual_day_end, $loc_id, &$cond, &$start_datetime, &$end_datetime, &$query_supergroup, &$query_open_orders, &$vars) {
	$cond = "AND `orders`.`register` = '[register]'";
	if ($this_report['register']=="Not set" || $this_report['register']=="receipt") $cond = "AND (`orders`.`register` = '[register]' OR `orders`.`register` = 'Not set')";
	if ($this_report['mode']=="server" || $this_report['mode']=="server_single") $cond = "AND `orders`.`server_id` = '[report_server_id]'";
	if ($this_report['mode']=="z_report") {
		$cond = "";
		$start_datetime = $actual_day_start;
		$end_datetime = $actual_day_end;
	}

	$query_supergroup = "SELECT `orders`.`opened` AS `field0`,`order_contents`.`price` AS `field1`,`order_contents`.`quantity` AS `field2`,`order_contents`.`modify_price` AS `field3`,`menu_items`.`super_group_id` AS `field4`,`menu_categories`.`super_group_id` AS `field5`,`super_groups`.`title` AS `field6`,`orders`.`location_id` AS `field7`,`orders`.`void` AS `field8`,`order_contents`.`loc_id` AS `field9`,`orders`.`cashier` AS `field10`,`order_contents`.`item` AS `field11`,`order_contents`.`item_id` AS `field12`,SUM( (`order_contents`.`forced_modifiers_price` + `order_contents`.`modify_price`) * `order_contents`.`quantity`) AS `field13`,SUM(`order_contents`.`subtotal_with_mods`) AS `field14`,SUM(`order_contents`.`discount_amount` + `order_contents`.`idiscount_amount`) AS `field15`,SUM(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` - `order_contents`.`idiscount_amount`) AS `field16`,SUM(`order_contents`.`tax_amount`) AS `field17`,SUM(`order_contents`.`subtotal_with_mods` - `order_contents`.`discount_amount` - `order_contents`.`idiscount_amount` + `order_contents`.`tax_amount`) AS `field18`, SUM(`order_contents`.`itax`) AS `itax` FROM `orders`LEFT JOIN `order_contents` ON `orders`.`order_id` = `order_contents`.`order_id`LEFT JOIN `menu_items` ON `order_contents`.`item_id` = `menu_items`.`id`LEFT JOIN `menu_categories` ON `menu_items`.`category_id` = `menu_categories`.`id`LEFT JOIN `super_groups` ON IF (`menu_items`.`super_group_id` = '', `menu_categories`.`super_group_id`, `menu_items`.`super_group_id`) = `super_groups`.`id`WHERE`orders`.`closed` >= '[date_start]' AND`orders`.`closed` < '[date_end]' AND`orders`.`location_id` = '[loc_id]' AND`orders`.`void` = 0 AND`order_contents`.`quantity`*1 > 0 AND`order_contents`.`loc_id` = `orders`.`location_id` AND`order_contents`.`item_id` > 0 $cond group by`field6`ORDER BY`field6` asc";

	$query_open_orders = "SELECT `opened` FROM `orders` WHERE (`closed` = '0000-00-00 00:00:00' OR (`reopened_datetime` != '' AND `reclosed_datetime` = '')) AND `void` = '0' AND `opened` <= '[date_end]' AND `opened` != '0000-00-00 00:00:00' AND `location_id` = '[loc_id]' $cond";

	$vars = array();
	$vars['date_start'] = $start_datetime;
	$vars['date_end'] = $end_datetime;
	$vars['register'] = $this_report['register'];
	$vars['report_server_id'] = $this_report['server_id'];
	$vars['loc_id'] = $loc_id;
}

function addHeaderStrings($this_report, $mode, &$last_group, &$print_string, &$subtitle_string) {
	if ($mode == "x_report") {

		// print the break between "REGISTERS" and "SERVERS"
		if ($this_report['group'] != $last_group) {
			$last_group = $this_report['group'];
			$print_string .= " :";
			$print_string .= "[s[s[s[s[s[s ".$last_group." [s[s[s[s[s[s :";
			$print_string .= " :";
			$print_string .= "---------------------------:";
		}
		// print the heading of the register/server
		$subtitle_string = " :";
		if ($this_report['mode'] == "register") {
			$subtitle_string .= "------ ".$this_report['register_name']." ------ :";
			$subtitle_string .= "     (".$this_report['register'].") :";
		}
		else if ($this_report['mode'] == "server")
		{
			$subtitle_string .= "------ ".$this_report['server_name']." ------ :";
		}
	}
}

function addPaymentHistoryStrings($cond, $vars, $subtitle_string, $mode, &$print_string) {
	$query = "SELECT `cc_transactions`.`server_name` AS `field0`,`cc_transactions`.`datetime` AS `field1`,`cc_transactions`.`loc_id` AS `field2`,`orders`.`void` AS `field3`,`cc_transactions`.`register` AS `field4`,`cc_transactions`.`amount` AS `field5`,IF(`cc_transactions`.`for_deposit`>0,'deposit','') AS `field6`,`orders`.`location_id` AS `field7`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`amount`)), sum(`cc_transactions`.`total_collected`))) AS `field8`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`tip_amount`)), sum(`cc_transactions`.`tip_amount`))) AS `field9`,`cc_transactions`.`pay_type` AS `field10`,`cc_transactions`.`card_type` AS `field11`,`cc_transactions`.`action` AS `field12`,IF(`cc_transactions`.`voided`='1',sum(`cc_transactions`.`total_collected`),0) AS `field13` from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where `cc_transactions`.`datetime` >= '[date_start]' AND `cc_transactions`.`datetime` <= '[date_end]' AND `cc_transactions`.`loc_id` = '[loc_id]' AND `orders`.`void` = 0 ".$cond." AND (`orders`.`location_id` = `cc_transactions`.`loc_id` OR `cc_transactions`.`order_id` = \"Paid In\" OR `cc_transactions`.`order_id` = \"Paid Out\") and (`cc_transactions`.`action` = 'Sale' OR `cc_transactions`.`action` = 'Refund') and `cc_transactions`.`voided` != 1 group by concat(`cc_transactions`.`pay_type_id`,' ',`cc_transactions`.`card_type`,`cc_transactions`.`action`,`cc_transactions`.`voided`) order by `field10` asc";

	$pay_query = lavu_query($query, $vars);
	if (mysqli_num_rows($pay_query)) {
		$print_string .= $subtitle_string;
		while($pay_read = mysqli_fetch_assoc($pay_query)) {
			// field10: payment type
			// field11: card type
			// field8: total amount paid
			$action = $pay_read['field12'];
			$print_string .= trim($pay_read['field10']." ".(($action == "Refund")?"Refund ":"").$pay_read['field11'])." [c *".showAmount($pay_read['field8'])." :";
		}
	}
	else if (in_array($mode, array("register", "server", "server_single")))
	{
		$print_string .= "No Payments found : :";
	}

	$print_string .= " :";
	$print_string .= " :";
	$print_string .= " :";
	$print_string .= " :";
	$print_string .= "---------------------------:";
}

function addPaymentHistoryStrings_PaidInOut($cond, $vars, $subtitle_string, $mode, &$print_string) {
	$use_cond = $cond;
	$use_cond = str_replace("`orders`.`register`","`cc_transactions`.`register`",$use_cond);

	$query = "SELECT `orders`.`order_id` as `order_id`,`cc_transactions`.`server_name` AS `field0`,`cc_transactions`.`datetime` AS `field1`,`cc_transactions`.`loc_id` AS `field2`,`orders`.`void` AS `field3`,`cc_transactions`.`register` AS `field4`,`cc_transactions`.`amount` AS `field5`,IF(`cc_transactions`.`for_deposit`>0,'deposit','') AS `field6`,`orders`.`location_id` AS `field7`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`amount`)), sum(`cc_transactions`.`total_collected`))) AS `field8`,IF(`cc_transactions`.`voided`='1',0,IF(`cc_transactions`.`action`='Refund', (0 - sum(`cc_transactions`.`tip_amount`)), sum(`cc_transactions`.`tip_amount`))) AS `field9`,`cc_transactions`.`pay_type` AS `field10`,`cc_transactions`.`card_type` AS `field11`,`cc_transactions`.`action` AS `field12`,IF(`cc_transactions`.`voided`='1',sum(`cc_transactions`.`total_collected`),0) AS `field13` from `cc_transactions` LEFT JOIN `orders` ON `cc_transactions`.`order_id` = `orders`.`order_id`where `cc_transactions`.`datetime` >= '[date_start]' AND `cc_transactions`.`datetime` <= '[date_end]' AND `cc_transactions`.`loc_id` = '[loc_id]' AND `orders`.`void` = 0 ".$use_cond." AND (`orders`.`location_id` = `cc_transactions`.`loc_id` AND (`cc_transactions`.`order_id` = \"Paid In\" OR `cc_transactions`.`order_id` = \"Paid Out\")) and `cc_transactions`.`action` != \"\" and `cc_transactions`.`voided` != 1 group by concat(`cc_transactions`.`pay_type`,' ',`cc_transactions`.`card_type`,`cc_transactions`.`action`,`cc_transactions`.`voided`) order by `field10` asc";

	//$test_query =  $query;
	//foreach($vars as $vkey => $vval)
	//{
	//	$test_query = str_replace("[".$vkey."]",$vval,$test_query);
	//}
	//mail("corey@lavu.com","Query",$test_query,"From:query@poslavu.com");

	$pstr = "";
	$iofound = 0;
	$pipototal = 0;
	$pay_query = lavu_query($query, $vars);
	if (mysqli_num_rows($pay_query)) {
		$pstr .= $subtitle_string;
		while($pay_read = mysqli_fetch_assoc($pay_query)) {
			// field10: payment type
			// field11: card type
			// field8: total amount paid
			if($pay_read['order_id']=="Paid In" || $pay_read['order_id']=="Paid Out")
			{
				$iofound ++;
				$pstr .= $pay_read['order_id'] . " - " . trim($pay_read['field10']." ".$pay_read['field11'])." [c ".showAmount($pay_read['field8'])." :";
				$pipototal += (str_replace(",","",$pay_read['field8'])) * 1;
			}
		}
		if($pipototal > 0 || $pipototal < 0)
		{
			$pstr .= "PI-PO Total [c *".showAmount($pipototal)." :";
		}
	}
	//else if (in_array($mode, array("register", "server", "server_single")))
	if($iofound > 0)
	{
		$print_string .= $pstr;
		$print_string .= " :";
		$print_string .= "---------------------------:";
	}

}

function processOrderGetTotals($order_read, $a_immutables, &$a_mutables, &$alt_paid)
{
	switch ($order_read['void'])
	{
		case "0":

			$ioid			= $order_read['ioid'];
			$order_id		= $order_read['order_id'];
			$cash_tip_mode	= locationSetting("cash_tips_as_daily_total", "0");

			// Is the record we're looking at a 777 placeholder record?
			if ((substr($order_id, 0, 3)=="777") && ($cash_tip_mode == "1") && $order_read['tablename']=="tips_submission_daily_total") // Cash tips as daily total
			{
				$a_mutables["cash_tips"] += $order_read['cash_tip'];
			}
			else if (substr($order_id, 0, 3)!="777") // Standard order record
			{
				$alt_paid += $order_read['alt_paid'];

				$cash_applied	= $order_read['cash_applied'];
				$card_paid		= $order_read['card_paid'];
				$gc_paid		= $order_read['gift_certificate'];

				$a_mutables["cash_paid"]			+= $cash_applied;
				$a_mutables["card_paid"]			+= $card_paid;
				$a_mutables["discounts"]			+= $order_read['discount'];
				$a_mutables["gift_certificates"]	+= $gc_paid;
				$a_mutables["gratuity"]				+= $order_read['gratuity'];
				$a_mutables["guests"]				+= $order_read['guests'];
				$a_mutables["idiscounts_total"]		+= $order_read['idiscount_amount'];
				$a_mutables["relevant_orders"][]	= $order_id;
				$a_mutables["subtotal"]				+= $order_read['subtotal'];
				$a_mutables["tax"]					+= $order_read['tax'];
				$a_mutables["total"]				+= $order_read['total'];
				$a_mutables["cash_tips"] 			+= $order_read['cash_tip'];
				$a_mutables["togo_delivery_fees"] 	+= $order_read['togo_delivery_fees'];

				// Check the order's cc_transaction records and get a sum of tip_amount for card tips
				$getCardTipsQuery = lavu_query("SELECT SUM(`tip_amount`) AS `tips`, `action` as transactionType FROM `cc_transactions` WHERE `ioid` = '[1]' AND `order_id` = '[2]' AND `pay_type_id` = '2' AND `voided` = '0' AND `action` in ('Sale','Refund') AND `order_id` NOT LIKE '777%'", $ioid, $order_id);
				if (mysqli_num_rows($getCardTipsQuery)){
					$ccTipsRow = mysqli_fetch_assoc($getCardTipsQuery);
					if(strtolower($ccTipsRow['transactionType'])=='sale'){
						$a_mutables['card_tips'] += $ccTipsRow['tips'];
					}
					else if(strtolower($ccTipsRow['transactionType'])=='refund'){
						$a_mutables['card_tips'] -= $ccTipsRow['tips'];
					}
				}

				$calc_ag_by_order_stub = TRUE;
				$check_count = (int)$order_read['no_of_checks'];

				if ((int)$check_count > 1)
				{
					$get_check_details = lavu_query("SELECT `gratuity`, `cash`, `card`, `gift_certificate`, `alt_paid` FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` = '[2]' AND  `check` <= '[3]' ", $order_read['location_id'], $order_id, $check_count);
					if ($get_check_details!==FALSE && mysqli_num_rows($get_check_details)>0)
					{
						$calc_ag_by_order_stub = FALSE;
						while ($info = mysqli_fetch_assoc($get_check_details))
						{
							$ag_cash = 0;
							$ag_card = 0;
							$ag_other = 0;

							distributeAutoGratuity($info['gratuity'], $info['cash'], $info['card'], $info['gift_certificate'], $info['alt_paid'], $ag_cash, $ag_card, $ag_other);

							$a_mutables["t_ag_cash"] += $ag_cash;
							$a_mutables["t_ag_card"] += $ag_card;
							$a_mutables["t_ag_other"] += $ag_other;
						}
					}
				}

				if ($calc_ag_by_order_stub)
				{
					$ag_cash = 0;
					$ag_card = 0;
					$ag_other = 0;

					distributeAutoGratuity($order_read['gratuity'], $cash_applied, $card_paid, $gc_paid, $order_read['alt_paid'], $ag_cash, $ag_card, $ag_other);

					$a_mutables["t_ag_cash"] += $ag_cash;
					$a_mutables["t_ag_card"] += $ag_card;
					$a_mutables["t_ag_other"] += $ag_other;
				}
			}

			break;

		case "1":

			$a_mutables["void_count"]++;
			$a_mutables["void_total"] += $order_read['subtotal'];
			break;

		default:
			break;
	}
}

function processCCTransactionGetTips($info, &$other_tips, &$other_tip_info) {
	$pt = $info['pay_type'];
	if (!isset($other_tip_info[$pt])) $other_tip_info[$pt] = 0;
	$amt = $info['tip_amount'];
	if ($info['action'] == "Sale") {
		$other_tips += $amt;
		$other_tip_info[$pt] += $amt;
	} else {
		$other_tips -= $amt;
		$other_tip_info[$pt] -= $amt;
	}
}

function createZreport($data){
	global $location_info;
	$returnarray[b2b]=array( "title"=>"b2b","first" => "No Transactions","last" => "No Transactions","vouchers" => "0","net" => "0.00","tax" => "0.00","total" => "0.00");
	$returnarray[b2c]=array( "title"=>"b2c","first" => "No Transactions","last" => "No Transactions","vouchers" => "0","net" => "0.00","tax" => "0.00","total" => "0.00");
	$returnarray[b2b_refund]=array( "title"=>"b2b_refund","first" => "No Transactions","last" => "No Transactions","vouchers" => "0","net" => "0.00","tax" => "0.00","total" => "0.00");
	$returnarray[b2c_refund]=array( "title"=>"b2c_refund","first" => "No Transactions","last" => "No Transactions","vouchers" => "0","net" => "0.00","tax" => "0.00","total" => "0.00");
	$chk_reports = lavu_query("select MAX(`datetime`) as maxdt from `fiscal_reports`");
	if(mysqli_num_rows($chk_reports)>0)
	{
		$maxdt = mysqli_fetch_assoc($chk_reports);
		if($maxdt['maxdt']!='' || $maxdt['maxdt']!=NULL)
		{
			$fiscal_day=$maxdt['maxdt'];
			$where = " update_date>'$maxdt[maxdt]' and ";
		}
		else
		{
			$where = ' ' ;
		}
	}
	else
	{
		$where = " ";
	}
	//echo "select *, fiscal_invoice_type.invoice_type from `fiscal_invoice` right join `fiscal_invoice_type` ON `fiscal_invoice`.`invoice_type_id` = `fiscal_invoice_type`.`fiscal_invoice_type_id` where ".$where." `fiscal_invoice`.`approval_status`='1'";
	$fiscal_invoice_query = lavu_query("select *, fiscal_invoice_type.invoice_type from `fiscal_invoice` right join `fiscal_invoice_type` ON `fiscal_invoice`.`invoice_type_id` = `fiscal_invoice_type`.`fiscal_invoice_type_id` where ".$where." `fiscal_invoice`.`approval_status`='1'");
	while($get_invoice_details = mysqli_fetch_assoc($fiscal_invoice_query))
	{
		$refdata = json_decode($get_invoice_details['ref_data'],true);
		//		print_r($refdata);
		$inv_dt = substr($get_invoice_details['update_date'],0,10);
	//	$invoice_details[$get_invoice_details['invoice_type']][$inv_dt][] = array('order_id'=>$get_invoice_details['order_id'],'update_date'=>$get_invoice_details['update_date'],'invoice_no'=>$refdata['NUMERO']);
		$invoice_details[$inv_dt][$get_invoice_details['invoice_type']][] = array('order_id'=>$get_invoice_details['order_id'],'update_date'=>$get_invoice_details['update_date'],'invoice_no'=>$refdata['NUMERO']);
		$order_id_data.="'".$get_invoice_details['order_id']."'".",";
		$listof_dates[]=$inv_dt;
	}
	$listof_dates=array_unique($listof_dates);
	foreach($listof_dates as $list_date){
		$returndata[$list_date]=$returnarray;
	}
	$order_id_data=rtrim($order_id_data,',');
	//echo "Fiscal Invoice";
	//echo "<pre>";print_r($invoice_details); exit;
	//echo json_encode($invoice_details);

	foreach($invoice_details as $dt_key=>$details)
	{
		$j=1;
		$order_arr='';
		foreach($details as $type_key=>$type)
		{
			$orderids = '';
			$first_invoice = '';
			$last_invoice = '';
			$i=1;
			foreach($type as $key1=>$val)
			{
				if($i>=count($type))
				{
					$orderids.="'".$val['order_id']."'";
				}
				else
				{
					$orderids.="'".$val['order_id']."'".',';
				}
				if($i==1)
				{
					$first_invoice = $val['invoice_no'];
				}
				if($i>=count($type))
				{
					$last_invoice = $val['invoice_no'];
				}
				$i++;
				if($j>=count($details) && $i>=count($type))
				{
					$sale_date = $val['update_date'];
				}
				else {
					$sale_date = $dt_key.substr($data['device_time'], 10);
				}
			}
			$order_arr[$type_key] = array('order_id'=>$orderids,'first_invoice'=>$first_invoice,'last_invoice'=>$last_invoice,'sale_date'=>$sale_date);
			$j++;
		}

		$combinedetails[$dt_key]=$order_arr;
	}
	$decimal_query = lavu_query("select disable_decimal from `locations`");
	$decimal_assoc = mysqli_fetch_assoc($decimal_query);
	$decimal_number = $decimal_assoc['disable_decimal'];
	$get_max_report_id = lavu_query("select MAX(`report_id`)as max_id from `fiscal_reports` ");
	$set_report_id = mysqli_fetch_assoc($get_max_report_id);
	$report_id = $set_report_id['max_id'];
	if(!empty($combinedetails))
	{
		foreach($combinedetails as $key=>$order_det)
		{
			$report_id=$report_id+1;
			$in_type = array( 'b2c','b2b','b2c_refund','b2b_refund');
			foreach($in_type as $key1)
			{
				$set_det=$order_det[$key1];
				if(count($order_det[$key1])>0){
				$orderid_string = $set_det['order_id'];
				//echo "select sum(`subtotal`) as subtotal, sum(`tax`) as tax, sum(`total`) as total from `orders` where `order_id` in(".$orderid_string.")";  //die;
				$prep_order_query = lavu_query("select sum(`subtotal`) as subtotal, sum(`tax`) as tax, sum(`total`) as total, sum(`refunded`) as refunded from `orders` where `order_id` in(".$orderid_string.")");
				$fetch_order_det = mysqli_fetch_assoc($prep_order_query);
				$refund_order_query= lavu_query("select sum(`total_collected`) as refunded from cc_transactions where `cc_transactions`.`action`='Refund' and `order_id` in(".$orderid_string.")");
				$refund_order_data = mysqli_fetch_assoc($refund_order_query);
				$report_type = $key;
				$subtotal =  number_format($fetch_order_det['subtotal'], 2, '.', '');
				$tax = number_format($fetch_order_det['tax'], 2, '.', '');
				$total =number_format( $fetch_order_det['total'], 2, '.', '');
				$firstInvoice = $set_det['first_invoice'];
				$lastInvoice = $set_det['last_invoice'];
				$refund_net = $fetch_order_det['refunded'];
				$returndata[$key][$key1][first]=$firstInvoice;
				$returndata[$key][$key1][last]=$lastInvoice;
				$returndata[$key][$key1][vouchers]=$lastInvoice-$firstInvoice+1;
				if($key1=='b2c_refund' || $key1=='b2b_refund')
				{
					if(isset($refund_order_data['refunded'])){
					    $refund_net = number_format($refund_order_data['refunded'], 2, '.', '');
					    $refund_total = number_format($refund_order_data['refunded'], 2, '.', '');
					}else{
						$refund_net = '0.00';
						$refund_total = '0.00';
					}
					$refund_tax = '0.00';
					$subtotal = '0.00';
					$total = '0.00';
					$tax = '0.00';
					$returndata[$key][$key1][net]=number_format($refund_net, $decimal_number, '.', '');
					$returndata[$key][$key1][tax]=number_format($refund_tax, $decimal_number, '.', '');
					$returndata[$key][$key1][total]=number_format($refund_total, $decimal_number, '.', '');
				}
				else
				{
					$refund_net = '0.00';
					$refund_total = '0.00';
					$refund_tax = '0.00';
					$returndata[$key][$key1][net]=number_format($subtotal, $decimal_number, '.', '');
					$returndata[$key][$key1][tax]=number_format($tax, $decimal_number, '.', '');
					$returndata[$key][$key1][total]=number_format($total, $decimal_number, '.', '');
				}

				$queryString = "Insert into `fiscal_reports` set `first_invoice` = '" . $firstInvoice .
				"',`last_invoice` = '" . $lastInvoice .
				"',`fiscal_day` = '" . $set_det['sale_date'] .
				"',`datetime` = '" .$set_det['sale_date'] .
				"',`net_total` = '" . $subtotal.
				"',`untax_total` = '" . $subtotal .
				"',`total` = '" . $total .
				"',`tax` = '" . $tax .
				"',`refund_total` = '".$refund_total.
				"',`refund_net` = '".$refund_net.
				"',`refund_tax` = '".$refund_tax.
				"',`untax_refund` = '".$refund_net.
				"',`report_type` = '" . $key1 .
				"',`report_id` = '".$report_id."'";
			}
			else{
			$f_day=$key.substr($data['device_time'], 10);
				$queryString = "Insert into `fiscal_reports` set `first_invoice` = 'No Transactions',`last_invoice` = 'No Transactions',`fiscal_day` = '".$f_day."',
						`datetime` = '".$f_day."',`net_total` = '0.00',`untax_total` = '0.00',`total` = '0.00',`tax` = '0.00',`refund_total` = '0.00',
								`refund_net` = '0.00',`refund_tax` = '0.00',`untax_refund` = '0.00',`report_type` = '".$key1."',`report_id` = '".$report_id."'";
			}
			//echo $queryString.'</br>';
			lavu_query($queryString);
		}
		$return_reportid[$key]=$report_id;
		}
		$returnfulldata[data]=$returndata;
		$returnfulldata[report_id]=$return_reportid;
		$message = $returnfulldata;
	}
	else {
		$message = 'fail';
	}
	return $message;
}

function checkClockInUser($empId, $requestDate) {
	$addOneDay = date('Y-m-d', strtotime($requestDate . ' +1 day'));
	$startDate = $requestDate.' 00:00:00';
	$endDate   = $addOneDay.' 00:00:00';
	$user_query = lavu_query("SELECT `users`.`id`,`users`.`f_name`,`users`.`l_name`,`clock_punches`.`time`,`clock_punches`.`hours`,`clock_punches`.`time_out` FROM `users` JOIN `clock_punches` ON `users`.`id` = `clock_punches`.`server_id` WHERE `users`.`_deleted` = 0 AND `clock_punches`.`role_id` in ($empId) AND `clock_punches`.`time` >= '".$startDate."' AND `clock_punches`.`time` < '".$endDate."' ");
	$user_count = mysqli_num_rows($user_query);
	return $user_count;
}

?>
