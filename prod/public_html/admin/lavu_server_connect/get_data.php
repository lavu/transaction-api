<?php
	ini_set('display_errors',1);
	session_start();
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/json.php");
	$maindb = "poslavu_MAIN_db";
	
	if(isset($_GET['nc_testing'])){
		$_POST['session_id']='12345678';
		$_POST['request_purpose']='get_sched';
		$_POST['user_id']='1';
		$_POST['function']="get_schedule";
		$_POST['dataname']="utterly_delici";
		
	}
	if(isset($_POST['request_purpose'])){
		if( isset($_POST['username']) && isset ($_POST['password'])){
			echo login($_POST['username'],$_POST['password'], $_POST['request_purpose']); 
			return;
		}else if( isset($_POST['session_id']) && valid_session($_POST['session_id'])){
			if( isset($_POST['dataname'])){
				do_connect($_POST['dataname']);
				if( isset($_POST['function'])){
					$fun_name = $_POST['function'];
					unset($POST['function']);
					echo $fun_name($_POST);
				}else
					echo '{"error":"no function name specified."}';
			}else{
				echo '{"error":"no dataname name specified."}';
			}
		}else
			echo '{"error": "username password combo not sent"}';
	}else 
		echo '{"error:"not connecting from server app"}';
	
	function get_schedule($args){
		$user_id    = $args['user_id'];
		$start      = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
		$start_date = date('Y-m-d', $start);
		$end_date   = date('Y-m-d', strtotime('next saturday', $start));
		$ret        = array();
		echo $start_date." ". $end_date;
		$query = lavu_query("SELECT * FROM `user_schedules` where ( (`start_datetime` BETWEEN '[1]' AND '[2]') OR (`end_datetime` BETWEEN '[1]' AND '[2]') OR ('[1]' BETWEEN `start_datetime` AND `end_datetime`) OR ('[2]' BETWEEN `start_datetime` AND `end_datetime`))  AND `user_id`='[3]'", $start_date, $end_date, $user_id);
		while($res = mysqli_fetch_assoc($query)){
			$ret[] = $res;
		}
		if(!$ret)
			$ret = array("user_schedule"=>new stdClass());
		else
			$ret = array("user_schedule"=>$ret);
		$ret['request_purpose']= $args['request_purpose'];
		return LavuJson::json_encode($ret);
	}
	function get_messages($args){
		$user_id = $args['user_id'];
	}
	function login($username, $password, $request_purpose){
		global $maindb;
		$cust_query = mlavu_query("select * from `[2]`.`customer_accounts` where `username`='[1]'",$username,$maindb);
		if(!mysqli_num_rows($cust_query)){
			return '{"request_purpose":"'.$request_purpose.'","error":"invalid username"}';
		}
		$cust_read = mysqli_fetch_assoc($cust_query);
		$custid    = $cust_read['id'];
		$dataname  = $cust_read['dataname'];
		$rdb 	   = "poslavu_".$dataname."_db";
		$s_pass_check = "AND (`password`=PASSWORD('[pass]') OR `password`=OLD_PASSWORD('[pass]'))";

		$user_query = mlavu_query("SELECT * FROM `[rest_db]`.`users` WHERE `access_level`>='[min_login_level]' AND `_deleted`!='1' AND `active`='1' AND `username`='[username]' {$s_pass_check}", array("username"=>$username, "pass"=>$password, "rest_db"=>$rdb, "min_login_level"=>$lavu_minimum_login_level));
		if($user_query !== FALSE && mysqli_num_rows($user_query)){
			$rest_query = mlavu_query("select * from `[2]`.`restaurants` where `data_name`='[1]'",$dataname,$maindb);
			if(mysqli_num_rows($rest_query)){
				$rest_read = mysqli_fetch_assoc($rest_query);
				if($rest_read['disabled']=="1" || $rest_read['disabled']=="2")
					$company_disabled = true;
				else
					$company_disabled = false;
					
				$companyid 		= $rest_read['id'];
				$version_number = $rest_read['version_number'];
				$dev 			= $rest_read['dev'];
				$company_name 	= $rest_read['company_name'];
			}else{
				$company_disabled = true;
				$companyid 	  	  = 0;
			}
			if($company_disabled){
				update_cp_login_status(false,$_POST['username']);
				return '{"request_purpose":"'.$request_purpose.'","error":"company has been disabled. Please contact support to continue service"}';
			}else{
				$user_read = mysqli_fetch_assoc($user_query);
				admin_login($dataname,$user_read['id'],$user_read['username'],trim($user_read['f_name']." ".$user_read['l_name']),
							$user_read['email'],$companyid, $version_number, $dev, $user_read['access_level'], $company_name, 
							$user_read['phone'], $user_read['loc_id'], $user_read['lavu_admin']);
				set_sessvar('admin_dataname', $dataname);
				set_sessvar('locationid', $user_read['loc_id']);
				update_cp_login_status(true,$_POST['username']);

				if(isset($_POST['stay_logged_in'])){
					$autokey = session_id() . rand(1000,9999);
					setcookie("poslavu_cp_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
					mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
				}
				$user_data= LavuJson::json_encode(get_user_data($dataname, $username));
				return '{"request_purpose":"'.$request_purpose.'","session_id":"12345678", "data_name":"'.$dataname.'", "user_data":'.$user_data.'}';
			}
		}else{
			return '{"request_purpose":"'.$request_purpose.'","error":"invalid username/password combination"}';
		}
	}
	function get_user_data($dataname, $username){
		$res= mysqli_fetch_assoc(mlavu_query("select `username`,`id`,`f_name`,`l_name`,`email`,`access_level` from `poslavu_[1]_db`.`users` where `username`='[2]'", 
			  $dataname, $username));
		return $res;
		
	}
	function valid_session($session_id){
		if($session_id=='12345678'){
			return true;
		}else{
			return false;
		}
	}
	function do_connect($dataname){
		lavu_connect_dn($dataname, "poslavu_".$dataname."_db");
	}
	function update_cp_login_status($success,$username){
		global $maindb;
		if($success)
			$set_field = "succeeded";
		else
			$set_field = "failed";
		$ipaddress = $_SERVER['REMOTE_ADDR'];
		$mints = time() - (60 * 15);
		$currentts = time();
		$currentdate = date("Y-m-d H:i:s");
		$li_query = mlavu_query("select * from `[1]`.`login_log` where `ipaddress`='[2]' and `[3]`>'0' and `ts`>='[4]'",$maindb,$ipaddress,$set_field,$mints);
		if(mysqli_num_rows($li_query)){
			$li_read = mysqli_fetch_assoc($li_query);
			$li_id   = $li_read['id'];
			mlavu_query("update `[2]`.`login_log` set `ts`='[3]', `date`='[4]', `users`=CONCAT(`users`,',[1]'), `[5]`=`[5]`+1 where `id`='[6]'",
						$username,$maindb,$currentts,$currentdate,$set_field,$li_id);
		}else{
			mlavu_query("insert into `[2]`.`login_log` (`ts`,`date`,`[3]`,`ipaddress`,`users`) values ('[4]','[5]','1','[6]','[1]')",
						 $username,$maindb,$set_field,$currentts,$currentdate,$ipaddress);
		}
	}
	function get_cp_login_count($field){
		global $maindb;
		$ipaddress = $_SERVER['REMOTE_ADDR'];
		$mints 	   = time() - (60 * 15);
		$li_query  = mlavu_query("select * from `[1]`.`login_log` where `ipaddress`='[2]' and `[3]`>'0' and `ts`>='[4]'",
					$maindb,$ipaddress,$field,$mints);
		if(mysqli_num_rows($li_query)){
			$li_read = mysqli_fetch_assoc($li_query);
			return $li_read[$field];
		}else 
			return false;
	}
?>
