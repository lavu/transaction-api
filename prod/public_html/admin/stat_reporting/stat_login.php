<?php
	$dataname;
	{
		require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
		//ini_set('memory_limit', '128M');
		
		/**
		 *  Constructing an Array of Variables to pass into the various queries, to ensure that we are using the built-in
		 *  SQL escape string functions. This makes these string safe to query, almost regardless of what they contain.
		 */
		$params = Array( 
						'username' => $username,
						'password' => $password);
						
		
		/**
		 * This section pulls the username association with datanames as referenced by our MAIN database.  If the username
		 * does not exist within the main database, that means this user cannot login to the backend, and therefore should
		 * not have access to the functionalities of this application.
		 *
		 *
		 */
		$datanameQuery = "SELECT `dataname` FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `username`='[username]'";
		$datanameresult = mlavu_query($datanameQuery,$params);
		
		/**
		 * Checks to see if the username specified does exist, if it doesn't, it will defer to the LavuLite server for processing
		 */
		if(! mysqli_num_rows($datanameresult))
		{
			$forward = "";
			foreach($_REQUEST as $key=>$val)
			{
				if(stristr($key, "lv_") !== FALSE)
				{
					if($forward != "")
						$forward .= "&";
					$forward .= $key . "=" . $val;
				}
			}
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://admin.lavulite.com/stat_reporting/stat_reporting.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $forward);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
			echo $response;
			exit();
			
			//statusFailed("Unable to Login, Most Likely the User Name Was Incorrect, or the User Name Specified Does Not Have Access.");
		}
		
		
		/**
		 * Will have Data at this point, since we passed the empty data check.  Now it's time to retrieve the dataname, and
		 * add it to our params list for further queries.
		 */
		 
		$datanamearray = mysqli_fetch_assoc($datanameresult);
		global $dataname;
		$dataname = $datanamearray['dataname'];
		
		$params['dataname'] = $dataname;
		
		/**
		 * At this point, we know the dataname, so we know the database to pull from.  All that remains is to check the 
		 * specified credentials against that user table.
		 */
		$loginQuery = "SELECT * FROM `poslavu_[dataname]_db`.`users` WHERE `username`='[username]' AND (`password`=PASSWORD('[password]') OR `password`=OLD_PASSWORD('[password]')) AND `_deleted` = '0' AND `access_level` >= 3";
		$loginQueryResult = mlavu_query($loginQuery, $params);
		
		/**
		 * Check to see if the credentials yielded a login.  We won't actually login the user, we will just use this as a check
		 * to see if they should be allowed access to the precious, precious datas.
		 *
		 * Username may exist on LavuLite... Need to check that before failing.
		 */
		if(! mysqli_num_rows($loginQueryResult))
		{
			$forward = "";
			foreach($_REQUEST as $key=>$val)
			{
				if(stristr($key, "lv_") !== FALSE)
				{
					if($forward != "")
						$forward .= "&";
					$forward .= $key . "=" . $val;
				}
			}
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://admin.lavulite.com/stat_reporting/stat_reporting.php");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $forward);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);
			//echo $response;
			//exit();
			if(stripos($response, '"status" : "failed"') !== FALSE) //Lavu Lite Failed as well
				statusFailed("Unable to Login, Username or Password is Incorrect");	
			echo $response;
			exit();
		}
			
		/**
		 * Congrats, they are 'authenticated' (read we will no longer try to stop them), it is now time to pull information
		 * for the app.
		 */
	}

?>