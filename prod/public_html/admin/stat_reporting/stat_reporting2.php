<?php

//ini_set('display_errors', 'On');

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

function getPost($key){
	return isset($_REQUEST[$key]) ? $_REQUEST[$key] : "";
}



function createJSONReportTable($leftColumnTitle, $leftColumnValues, $rightColumnTitle,  $embeddedRightColumnValues){
	
	//We need to wrap each entry in leftColumnValues and embeddedRightColumnValues in quotes for the JSON.
	for($i = 0; $i < count($leftColumnValues); $i++){
		$leftColumnValues[$i] = '"' . $leftColumnValues[$i] . '"';
	}
	for($i = 0; $i < count($embeddedRightColumnValues); $i++){
		$currArray = $embeddedRightColumnValues[$i];
		for($j = 0; $j < count($currArray); $j++){
			$currArray[$j] = '"' . $currArray[$j] . '"';
		} 
		$embeddedRightColumnValues[$i] = $currArray;
	}
	
	//We open the table object
	$str = '{';
		//The left and right titles
		$str .=  '"left_title":"' . $leftColumnTitle . '",';
		$str .= '"right_title":"' . $rightColumnTitle . '",';
		//The left column vals
		$str .= '"left_values":';
		$str .= '[';
		$str .= implode(",", $leftColumnValues);
		$str .= '],';
		//The right column vals
		$str .= '"right_values_embedded_array":';
		$str .= '[';
		$i = 0;  $total = count($embeddedRightColumnValues);
		foreach($embeddedRightColumnValues as $currPageArray){
			//We open the currPageArray
			$str .= '[';
			$str .= implode(",", $currPageArray);
			$str .= ']';
			$str .= ($i++ == ($total - 1)) ? "" : ",";
		}
		$str .= ']';
	//We close the table object
	$str .= '}';
	
	
	return $str;
}



//<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~
// W E    G E T    T H E    P O S T    V A R I A B L E S.
$cmd          = getPost('lv_cmd');
$username     = getPost('lv_user');
$password     = getPost('lv_password');
$locationID   = getPost('lv_locationID');
//<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~

if($username == 'demo.brian.d'){
	error_log("Hello");
}


if($cmd == 'sign_in'){

}
else if($cmd == 'test_data'){
	//Total Revenue 
	$totalRevenues    = array('"5.56"', '"98.25"', '"178.42"', '"1383.13"', '"42404.42"', '"1234567890123.24"');
	
	//Table 01
	$tbl_1_leftTitle  = "SUPERGROUPS";
	$tbl_1_rightTitle = "TOTAL SALES";
	$tbl_1_LeftVals   = array("FOOD","BEVERAGE","ALCOHOL","MISC");
	$tbl_1_pg_1       = array("467.78",  "34.43",   "61.98" , "7.18");    //pg1 is for today
	$tbl_1_pg_2       = array("646.13",  "236.24",  "123.35", "24.26");   //pg2 is for yesterday
	$tbl_1_pg_3       = array("1267.78", "535.62",  "221.61", "52.23");   //pg3 is for a week
	$tbl_1_pg_4       = array("1557.78", "953.94",  "300.26", "77.16");	  //pg4 is for 30 day
	$tbl_1_pages      = array($tbl_1_pg_1, $tbl_1_pg_2, $tbl_1_pg_3, $tbl_1_pg_4);
	
	//Table 02
	$tbl_2_leftTitle  = "MENU GROUPS";
	$tbl_2_rightTitle = "TOTAL SALES";
	$tbl_2_LeftVals   = array("Breakfast", "Lunch", "Dinner", "After Hours");
	$tbl_2_pg_1       = array("123.78",    "5.43",    "254.98",      "456.18");
	$tbl_2_pg_2       = array("234.13",    "455.24",  "543.35",      "65456.26");
	$tbl_2_pg_3       = array("354.78",    "613.42",  "634.61",      "454564.23");
	$tbl_2_pg_4       = array("1557.78",   "953.54",  "9976.26",     "8673456.16");
	$tbl_2_pages      = array($tbl_2_pg_1, $tbl_2_pg_2, $tbl_2_pg_3, $tbl_2_pg_4);
	
	//Table 03
	$tbl_3_leftTitle  = "MENU GROUPS";
	$tbl_3_rightTitle = "TOTAL SALES";
	$tbl_3_LeftVals   = array("Breakfast", "Lunch", "Dinner", "After Hours");
	$tbl_3_pg_1       = array("146.78",          "94.65",   "523.56",      "856.16");
	$tbl_3_pg_2       = array("2457.13",        "365.63",  "2343.95",      "1345.47");
	$tbl_3_pg_3       = array("45674567.78",    "597.52",  "2654.89",      "52345.18");
	$tbl_3_pg_4       = array("156756757.78",   "993.65",  "457626",       "623452.43");
	$tbl_3_pages      = array($tbl_3_pg_1, $tbl_3_pg_2, $tbl_3_pg_3, $tbl_3_pg_4);
	
	//Start the global object.
	$str  = '{';  
		//Our first object is the array of tables
		$str .= '"tables":';
		    $str .= '[';
	    	//Just for now lets output the first table
	    		$str .= createJSONReportTable($tbl_1_leftTitle, $tbl_1_LeftVals, $tbl_1_rightTitle, $tbl_1_pages);
	    		$str .= ",";
	    		$str .= createJSONReportTable($tbl_2_leftTitle, $tbl_2_LeftVals, $tbl_2_rightTitle, $tbl_2_pages);
	    		$str .= ",";
	    		$str .= createJSONReportTable($tbl_3_leftTitle, $tbl_3_LeftVals, $tbl_3_rightTitle, $tbl_3_pages);
	    	$str .= '],';
	    	$str .= ' "status" : "success" ,';
	    	$str .= ' "revenue_totals": [' . implode(",", $totalRevenues) . '] ,';
	    	$str .= ' "location_id" : "' . $locationID . '" ,';
	    	$str .= ' "reports_screen_title" : ' . '"LocNameHere"'; 
	
	//End the global object.    
	$str .= '}';
	echo $str;
}
else if($cmd == 'test_get_location_names'){
    //echo 'marker';
    if($username == "a"){
    	echo '{"status" : "success", "location_names" : [ "MyOnlyLocation" , "12" ] }';
    }
    else if($username == "b"){
    	echo '{ "status" : "success" , "location_names" : [ "Homminum Epsum" , "1", "Sumerum Colorum" , "4" , "Jesic Zaid" , "5" , "Forumem Larempus", "8"] }';
    }
    else{
    	echo '{ "status" : "success" , "location_names" : [ "Smiths Delicatessen" , "1", "Smiths Pizza Shop" , "4" , "Smiths Liquor" , "5"] }';
    }
}



?>
