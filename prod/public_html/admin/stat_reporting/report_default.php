<?php

	require_once(dirname(__FILE__)."/../cp/reqserv/server_load_functions.php");
	global $b_use_main_server;
	$b_use_main_server = !check_lockout_status(1,FALSE);

	function getSecondsBehind( ) {
		$query_result = mlavu_query("select * from `poslavu_MAIN_db`.`updatelog` where `type`='repstatus db1'");
		if(! $query_result ){
			echo "Mysql Error: " . mlavu_dberror();
		}

		$result = mysqli_fetch_assoc($query_result);
		mysqli_free_result( $query_result );
		$details = explode("\n", $result['details'] );

		$sync_status = array();

		foreach( $details as $k => $v ) {
			$arr_mapping = explode('=', $v);
			$sync_status[ $arr_mapping[0] ] = $arr_mapping[1];
		}

		return $sync_status['Seconds_Behind_Master'] * 1;
	}

	function formatSecondsBehind() {
		global $b_use_main_server;

		if ($b_use_main_server) {
			return "Current";
		}

		$seconds_behind = getSecondsBehind();
		$delay_text = "";
		$plural = "";

		if($seconds_behind > 0 ){
			if($seconds_behind != 1 ){
				$plural = 's';
			} else {
				$plural = '';
			}
			$delay_text = "{$seconds_behind} Second{$plural} Behind";
		}

		$minutes_behind = (int)($seconds_behind / 60);
		if( $minutes_behind ) {
			if($minutes_behind != 1 ){
				$plural = 's';
			} else {
				$plural = '';
			}
			$delay_text = "{$minutes_behind} Minute{$plural} Behind";
		}

		$hours_behind = (int)( $minutes_behind / 60 );
		if( $hours_behind ) {
			if($hours_behind != 1 ){
				$plural = 's';
			} else {
				$plural = '';
			}
			$delay_text = "{$hours_behind} Hour{$plural} Behind";
		}

		if ($delay_text == "")
			$delay_text = "Current";

		return $delay_text;
	}

	function checkset($var, $def="0")
	{
		isset($var) ? $var : $def;
	}

	global $dataname;
	global $b_use_main_server;
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once('/home/poslavu/public_html/admin/sa_cp/show_report.php');
	if (!$b_use_main_server)
		lavu_connect_dn($dataname, 'poslavu_' . $dataname . '_db', false);
	else
		lavu_connect_dn($dataname,  'poslavu_' . $dataname . '_db');


	$mon_left_sym = "$ ";
	$mon_right_sym = "";
	$mon_comma = ",";
	$mon_decimal = ".";
	$mon_no_of_decimals = 2;
	$date_close_hour_offset = 0;
	$date_close_min_offset = 0;
	{  // GETTING MONITARY INFORMATION
		$formatQuery = "SELECT `decimal_char`, `monitary_symbol`, `left_or_right`, `disable_decimal`, `day_start_end_time` FROM `locations` WHERE `id` LIKE '[1]'";
		$formatResult = lavu_query($formatQuery, $loc_id);
		if($formatResult !== FALSE && mysqli_num_rows($formatResult))
		{
			$formats = mysqli_fetch_assoc($formatResult);
			if($formats['left_or_right'] == 'left')
			{
				$mon_left_sym = $formats['monitary_symbol'] . " ";
				$mon_right_sym = "";
			}
			else
			{
				$mon_right_sym = " " . $formats['monitary_symbol'];
				$mon_left_sym = "";
			}

			if($formats['decimal_char'] == ',')
			{
				$mon_comma = ".";
				$mon_decimal = ",";
			}

			$mon_no_of_decimals = $formats['disable_decimal'] * 1;

			$time = $formats['day_start_end_time'];
			if($time * 1)
			{
				$date_close_hour_offset = substr($time, 0,2)*1;
				$date_close_min_offset = substr($time,2,2)*1;
			}
		}else{
			error_log($dataname.': Mysql error:'.lavu_dberror());
		}

	}

	$tables = "";
	$rev_tots = "";
	$dateRanges = "";

	{
		$start_offset = Array(
								 0,
								 -1,
								 -6,
								 -29);

		$duration_offset = Array(
								  1,
								  1,
								  7,
								  30);

		$_GET['date_reports_by'] = 'closed';
		{
			// SUPER GROUP REPORT
			$categoiesHead = "Super Groups";
			$categoriesFields = Array();
			$totalsHead = "";
			$totalsPages = Array();


			for($i = 0; $i < count($start_offset); $i++)
				$totalsPages[$i] = Array();

			//echo $reportTime . "<br />";

			for($i = count($start_offset) - 1; $i >= 0; $i--)
			{
				$_GET['setdate'] = date('Y-m-d', mktime(date('H',strtotime($reportTime)) -$date_close_hour_offset, date('i',strtoTime($reportTime))-$date_close_min_offset,0,date('m',strtotime($reportTime)),date('d',strtotime($reportTime)) + $start_offset[$i],date('Y',strtotime($reportTime))));
				$_GET['day_duration'] = $duration_offset[$i];

				$reportQuery =  show_report('poslavu_' . $dataname . '_db','poslavu_MAIN_db','unified','',25,false,$loc_id,false,true);
				//echo $reportQuery;
				$reportQuery = str_ireplace("= '%'", "LIKE '%'", $reportQuery);
				//echo $reportQuery;
				//exit;
/*				
if($dataname == 'cafe_nate'){
	error_log( 'DEBUG QUERY: ' . $reportQuery);
}
*/				
				
				$reportResult = lavu_query($reportQuery);
				//if(!mysqli_num_rows($reportResult))
					//statusFailed("Failed To Retrieve Data: " . $start_offset[$i] . " - " . $duration_offset[$i]);)

				while( $reportArray = mysqli_fetch_assoc($reportResult) )
				{
					//print_r($reportArray);
					if($reportArray['Group'] == "")
						$reportArray['Group'] = "Not Assigned";


					if($i == count($start_offset) - 1 )
					{
						$categoriesFields[] = $reportArray['Group'];
						for($j = 0; $j < count($start_offset); $j++)
							$totalsPages[$j][$reportArray['Group']] = $mon_left_sym . number_format(0.00, $mon_no_of_decimals, $mon_decimal, $mon_comma) . $mon_right_sym;
					}
					$totalsPages[$i][$reportArray['Group']] = $mon_left_sym . number_format($reportArray['Total'], $mon_no_of_decimals, $mon_decimal, $mon_comma) . $mon_right_sym;
				}

				for($j = 0; $j < count($categoriesFields); $j++)
				{
					if(!isset($totalsPages[$i][$categoriesFields[$j]]))
						$totalsPages[$i][$categoriesFields[$j]] = $mon_left_sym . number_format(0.00, $mon_no_of_decimals, $mon_decimal, $mon_comma) . $mon_right_sym;
				}

			//createJSONReportTable(LEFT HEADER, FIELD NAMES OF SIZE X, RIGHT HEADER,  ARRAY OF SIZE 6 OF ARRAY OF SIZE X)
			}


			$catFields = Array();
			foreach($categoriesFields as $key => $value)
			{
				if($value == "Not Assigned")
					continue;

				$catFields[] = $value;
			}
			$catFields[] = "Not Assigned";

			$totalsPages1 = Array();
			for($i = 0; $i < count($start_offset); $i++)
			{
				$totalsPages1[$i] = Array();
				foreach($catFields as $key => $value)
				{
					$totalsPages1[$i][$value] = $totalsPages[$i][$value];
				}
			}

			if($tables != "")
				$tables .= ", ";
			$tables .= createJSONReportTable($categoiesHead, $catFields, $totalsHead, $totalsPages1);

		}

		{  // AT A GLANCE REPORT
			$categoryHead = "At a Glance";
			$totalsHead = "";
			$categorySections = Array();

			$totalsPages = Array();
			for($i = 0; $i < count($start_offset); $i++)
				$totalsPages[$i] = Array();

			for($i = 0; $i < count($start_offset); $i++)
			{
				$_GET['setdate'] = date('Y-m-d', mktime(date('H',strtotime($reportTime)) -$date_close_hour_offset, date('i',strtoTime($reportTime))-$date_close_min_offset,0,date('m',strtotime($reportTime)),date('d',strtotime($reportTime)) + $start_offset[$i],date('Y',strtotime($reportTime))));
				$_GET['day_duration'] = $duration_offset[$i];

				$reportQuery =  show_report('poslavu_' . $dataname . '_db','poslavu_MAIN_db','unified','',128,false,$loc_id,false,true);
				$reportQuery = str_ireplace("= '%'", "LIKE '%'", $reportQuery);

				//echo $reportQuery . "<br />";

				$reportResult = lavu_query($reportQuery);

				if(mysqli_num_rows($reportResult))
				{
					$res = mysqli_fetch_assoc($reportResult);

					//print_r($res);
					//lavu_close_db();
					//exit();

					$skip = Array(
									"tab",
									"card_gratuity",
									"void",
									"closed",
									"card_gratuity",
									"card_paid",
									"location_id",
									"total",
									"gratuity"
									);

					foreach($res as $key => $value)
					{
						if(in_array($key, $skip))
							continue;

						if($key != "Total Revenue$"){
							if($i == 0)
								$categorySections[] = str_replace("#","",str_replace("$","",str_replace("%", "", $key)));

							if(strstr($key, "%"))
								$totalsPages[$i][] =number_format($value*100, $mon_no_of_decimals, $mon_decimal, $mon_comma) . "%";
							else if(strstr($key, "$"))
							{
								$totalsPages[$i][] = $mon_left_sym . number_format($value, $mon_no_of_decimals, $mon_decimal, $mon_comma) . $mon_right_sym;
							}
							else if(strstr($key, "#"))
								$totalsPages[$i][] = round($value,2);
							else
							{
								$totalsPages[$i][] = number_format($value, $mon_no_of_decimals, $mon_decimal, $mon_comma);
							}
						}


						if($key == "Total Revenue$")
						{
							if($i == 0)
								$categorySections[] = "Daily Average";

							if($rev_tots != "")
								$rev_tots .= ", ";

							$rev_tots .= '"' . sprintf("%.2f",$value) . '"';

							if($dateRanges != "")
								$dateRanges .= ", ";
							$sdate = date('Y-m-d H:i:s', mktime(date('H',strtotime($reportTime)) -$date_close_hour_offset, date('i',strtoTime($reportTime))-$date_close_min_offset,0,date('m',strtotime($reportTime)),date('d',strtotime($reportTime)) + $start_offset[$i],date('Y',strtotime($reportTime))));
							$edate = date('Y-m-d H:i:s', mktime(date('H',strtotime($reportTime)) -$date_close_hour_offset, date('i',strtoTime($reportTime))-$date_close_min_offset,0,date('m',strtotime($reportTime)),date('d',strtotime($reportTime)) + $start_offset[$i] + $duration_offset[$i],date('Y',strtotime($reportTime))));

							//echo $sdate;

							if($i == 1 || $i == 0)
								$dateRanges .= '"' . date("M d",strtotime($sdate)) . ' (' . formatSecondsBehind() . ')'. '"';
							else
								$dateRanges .= '"' . date("M d",strtotime($sdate)) . ' - ' . date("M d",strtotime($edate)) . ' (' . formatSecondsBehind() . ')' .'"';

							$totalsPages[$i][] = $mon_left_sym . number_format($value/$duration_offset[$i], $mon_no_of_decimals, $mon_decimal, $mon_comma) . $mon_right_sym;
						}
						else if($key == "Guest Count")
						{
							if($i == 0)
								$categorySections[] = "Avg. Guests/Day";

							$totalsPages[$i][] = sprintf("%.2f",round($value/$duration_offset[$i],2));
						}


					}
				}
				else
				{
					foreach($categorySections as $key => $value)
						$totalsPages[$i][] = "0.00";
				}
			}

			if($tables != "")
				$tables .= ", ";
			$tables .= createJSONReportTable($categoryHead, $categorySections, $totalsHead, $totalsPages);

		}
		
		{  //REVENUE BREAKDOWN REPORT
			$categoryHead = "Revenue Breakdown";
			$totalsHead = "";
			$categorySections = Array();

			$totalsPages = Array();

			for($i = 0; $i < count($start_offset); $i++)
			{
				$_GET['setdate'] = date('Y-m-d', mktime(date('H',strtotime($reportTime)) -$date_close_hour_offset, date('i',strtoTime($reportTime))-$date_close_min_offset,0,date('m',strtotime($reportTime)),date('d',strtotime($reportTime)) + $start_offset[$i],date('Y',strtotime($reportTime))));
				$_GET['day_duration'] = $duration_offset[$i];

				$reportQuery =  show_report('poslavu_' . $dataname . '_db','poslavu_MAIN_db','unified','',129,false,$loc_id,false,true);
				$reportQuery = str_ireplace("= '%'", "LIKE '%'", $reportQuery);

				//echo $reportQuery . "<br />";

				$reportResult = lavu_query($reportQuery);

				if(mysqli_num_rows($reportResult))
				{
					$res = mysqli_fetch_assoc($reportResult);
					$totalsPages[$i] = Array();

							$skip = Array(
									"location_id",
									"closed"
									);

					foreach($res as $key => $value)
					{
						if(in_array($key, $skip))
							continue;

						if($i == 0)
							$categorySections[] = str_replace("$", "", $key);

						$totalsPages[$i][] = $mon_left_sym . number_format($value, $mon_no_of_decimals, $mon_decimal, $mon_comma) . $mon_right_sym;
					}
				}
			}

			if($tables != "")
				$tables .= ", ";
			$tables .= createJSONReportTable($categoryHead, $categorySections, $totalsHead, $totalsPages);

		}

		if(1)
		{  //LABOR REPORT
			ob_start(); // put output to screen in buffer incase there is a warning (for example missing db column in account)
		
			// Load Labor Report Library
			$_GET['output_mode'] = "data";
			$_GET['run_report_start_datetime'] = "2000-00-00 00:00:00";
			$_GET['run_report_end_datetime'] = "2000-00-00 00:00:00";
			require(dirname(__FILE__) . "/../cp/areas/labor_report.php");
		
			$lbr_date = lbr_date_start_datetime();
			$lbr_end_date = lbr_date_add($lbr_date,1);
			
			// Load Labor Report For Day
			$_GET['output_mode'] = "data";
			$_GET['run_report_start_datetime'] = $lbr_date;
			$_GET['run_report_end_datetime'] = $lbr_end_date;
			require(dirname(__FILE__) . "/../cp/areas/labor_report.php");
			$day_data = $data;
			
			// Load Labor Report For Yesterday
			$_GET['run_report_start_datetime'] = lbr_date_subtract($lbr_date,1);
			$_GET['run_report_end_datetime'] = $lbr_date;
			require(dirname(__FILE__) . "/../cp/areas/labor_report.php");
			$yesterday_data = $data;
			
			// Load Labor Report For Last 7 Days
			//$_GET['run_report_start_datetime'] = lbr_date_subtract($lbr_date,7);
			//$_GET['run_report_end_datetime'] = $lbr_end_date;
			//require(dirname(__FILE__) . "/../cp/areas/labor_report.php");
			//$sevendays_data = $data;
			
			// Load Labor Report For Last 30 Days			
			//$_GET['run_report_start_datetime'] = lbr_date_subtract($lbr_date,30);
			//$_GET['run_report_end_datetime'] = $lbr_end_date;
			//require(dirname(__FILE__) . "/../cp/areas/labor_report.php");
			//$thirtydays_data = $data;
			
			$laborTitle = "Time";
			$totalsTitle = "Est Labor Cost";
			
			$laborSections = array();
			$totalPages = array();
			$totalsPages[0] = array(); // today
			$totalsPages[1] = array(); // yesterday
			$totalsPages[2] = array(); // week
			$totalsPages[3] = array(); // month
			
			$arrcount = count($day_data);
			for($x=0; $x<$arrcount; $x++)
			{
				$valstr = trim($mon_left_sym) . number_format($day_data[$x]['labor_cost'], $mon_no_of_decimals, $mon_decimal, $mon_comma) . trim($mon_right_sym);
				
				if(trim($day_data[$x]['labor_calc'])!="")
					$valstr .= " (" . $day_data[$x]['labor_calc'] . ")";
				if(strpos($day_data[$x]['title'],"/")!==false && strpos($day_data[$x]['title'],":")===false)
					$laborSections[] = "Day";
				else
					$laborSections[] = $day_data[$x]['title'];
				$totalsPages[0][] = $valstr; // today
			}
			for($x=0; $x<$arrcount; $x++)
			{
				if(isset($yesterday_data[$x]) && isset($yesterday_data[$x]['labor_calc']))
				{
					$valstr = trim($mon_left_sym) . number_format($yesterday_data[$x]['labor_cost'], $mon_no_of_decimals, $mon_decimal, $mon_comma) . trim($mon_right_sym);
					if(trim($yesterday_data[$x]['labor_calc'])!="")
						$valstr .= " (" . $yesterday_data[$x]['labor_calc'] . ")";
				}
				else $valstr = "---";
				
				$totalsPages[1][] = $valstr; // yesterday
			}
			for($x=0; $x<$arrcount; $x++)
			{
				$totalsPages[2][] = "---";//$sevendays_data[$x]['labor_cost']; // week
			}
			for($x=0; $x<$arrcount; $x++)
			{
				$totalsPages[3][] = "---";//$thirtydays_data[$x]['labor_cost']; // month
			}
			
			if($tables != "")
				$tables .= ", ";
				
			ob_end_clean();
			$tables .= createJSONReportTable($laborTitle, $laborSections, $totalsTitle, $totalsPages);
			
			//if($dataname=="the_daily_grin1")
			//{
			//	$jrdata = createJSONReportTable($laborTitle, $laborSections, $totalsTitle, $totalsPages);
			//	$fname = "/home/poslavu/testlog/jsonreport.txt";
			//	$fp = fopen($fname,"w");
			//	fwrite($fp,$jrdata);
			//	fclose($fp);
			//}
		}


		$locationName = "ALL LOCATIONS";
		if($loc_id != "%")
		{
			$locationNameQuery = "SELECT `title` FROM `locations` WHERE `id` = [1]";
			$locationResult = lavu_query($locationNameQuery, $loc_id);
			if($locationResult !== FALSE && mysqli_num_rows($locationResult))
			{
				$locArray = mysqli_fetch_assoc($locationResult);
				$locationName = str_ireplace('"', '\\"', $locArray['title']) ;
			}
		}

		lavu_close_db();


		echo "{";
			echo '"status" : "success", ';
			echo '"tables" : [' . $tables . '], ';
			echo '"location_id" : "' . $loc_id . '", ';
			echo '"revenue_totals" : [' . $rev_tots . '], ';
			echo '"date_interval" : [' . $dateRanges . '], ';
			echo '"reports_screen_title" : "' . $locationName . '", ';
			echo '"monitary_symbol" : "'.trim($mon_left_sym).'"';
		echo "}";
	}
?>
