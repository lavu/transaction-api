<?php
	{
		global $dataname;
		require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
		require_once('/home/poslavu/public_html/admin/sa_cp/show_report.php');
		lavu_connect_dn($dataname, 'poslavu_' . $dataname . '_db', false);
		
		$locationQuery = "SELECT * FROM `locations`";
		
		$locationResults = lavu_query($locationQuery);
		
		if(!mysqli_num_rows($locationResults))
			statusFailed("Unable to Find any Locations");
		
		$locationNames = Array();
		$locationIDs = Array();
		while ($loc = mysqli_fetch_assoc($locationResults))
		{
			$locationNames[] = $loc['title'];
			$locationIDs[] = $loc['id'];
		}
		
		$locations = "";
		for($i = 0; $i < count($locationNames); $i++)
		{
			if($locations != "")
				$locations .= ", ";
			
			$locations .= '"' . str_ireplace('"', '\\"', $locationNames[$i]) . '", "' . $locationIDs[$i] . '"';
		}
		
		echo "{";
			echo '"status" : "success", ';
			echo '"location_names" : ' ."[" . $locations . "]";
		echo "}";
	}
?>