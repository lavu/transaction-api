<?php

//ini_set('display_errors', 'On');
if (getenv('DEV') == '1' && !defined('DEV')) {
	define('DEV', 1);
} else if (!defined('DEV')) {
	define('DEV', 0);
}


function getPost($key, $def=""){
	return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $def;
}



function createJSONReportTable($leftColumnTitle, $leftColumnValues, $rightColumnTitle,  $embeddedRightColumnValues){
	
	//We need to wrap each entry in leftColumnValues and embeddedRightColumnValues in quotes for the JSON.
	for($i = 0; $i < count($leftColumnValues); $i++){
		$leftColumnValues[$i] = '"' . $leftColumnValues[$i] . '"';
	}
	for($i = 0; $i < count($embeddedRightColumnValues); $i++){
		$currArray = $embeddedRightColumnValues[$i];
		foreach($currArray as $key => $value){
			$currArray[$key] = '"' . $currArray[$key] . '"';
		} 
		$embeddedRightColumnValues[$i] = $currArray;
	}
	
	//We open the table object
	$str = '{';
		//The left and right titles
		$str .=  '"left_title":"' . $leftColumnTitle . '",';
		$str .= '"right_title":"' . $rightColumnTitle . '",';
		//The left column vals
		$str .= '"left_values":';
		$str .= '[';
		$str .= implode(",", $leftColumnValues);
		$str .= '],';
		//The right column vals
		$str .= '"right_values_embedded_array":';
		$str .= '[';
		$i = 0;  $total = count($embeddedRightColumnValues);
		foreach($embeddedRightColumnValues as $currPageArray){
			//We open the currPageArray
			$str .= '[';
			$str .= implode(",", $currPageArray);
			$str .= ']';
			$str .= ($i++ == ($total - 1)) ? "" : ",";
		}
		$str .= ']';
	//We close the table object
	$str .= '}';
	
	
	return $str;
}

function statusFailed($reason)
{
	echo '{ "status" : "failed", "reason" : "' . $reason . '" }';
	exit();
}

date_default_timezone_set("America/Chicago");

//<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~
// W E    G E T    T H E    P O S T    V A R I A B L E S.
$cmd        = getPost('lv_cmd');
$username   = getPost('lv_user');
$password   = getPost('lv_password');
$loc_id     = getPost('lv_locationID', "%");
$reportTime = getPost('lv_request_time',date('Y-m-d H:i:s'));

$prevTime   = getPost('lv_last_received_time',date('Y-m-d H:i:s'));
//<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~

if($cmd == 'sign_in'){
	require_once("stat_login.php");
	require_once("report_default.php");
}
else if($cmd == 'test_data'){

	//Total Revenue 
	$totalRevenues    = array('"5.56"', '"98.25"', '"178.42"', '"1383.13"', '"42404.42"', '"1234567890123.24"');
	
	//Table 01
	$tbl_1_leftTitle  = "SUPERGROUPS";
	$tbl_1_rightTitle = "TOTAL SALES";
	$tbl_1_LeftVals   = array("FOOD","BEVERAGE","ALCOHOL","MISC");
	$tbl_1_pg_1       = array("467.78",  "34.43",   "61.98" , "7.18");    //pg1 is for today
	$tbl_1_pg_2       = array("646.13",  "236.24",  "123.35", "24.26");   //pg2 is for yesterday
	$tbl_1_pg_3       = array("1267.78", "535.62",  "221.61", "52.23");   //pg3 is for a week
	$tbl_1_pg_4       = array("1557.78", "953.94",  "300.26", "77.16");	  //pg4 is for 30 day
	$tbl_1_pg_5       = array("4267.78", "1355.32", "321.91", "177.93");  //pg5 is for 60 day
	$tbl_1_pg_6       = array("6437.78", "5355.26", "521.01", "227.12");  //pg6 is for 90 day
	$tbl_1_pages      = array($tbl_1_pg_1, $tbl_1_pg_2, $tbl_1_pg_3, $tbl_1_pg_4, $tbl_1_pg_5, $tbl_1_pg_6);
	
	//Table 02
	$tbl_2_leftTitle  = "MENU GROUPS";
	$tbl_2_rightTitle = "TOTAL SALES";
	$tbl_2_LeftVals   = array("Breakfast", "Lunch", "Dinner", "After Hours");
	$tbl_2_pg_1       = array("123.78",    "5.43",    "254.98",      "456.18");
	$tbl_2_pg_2       = array("234.13",    "455.24",  "543.35",      "65456.26");
	$tbl_2_pg_3       = array("354.78",    "613.42",  "634.61",      "454564.23");
	$tbl_2_pg_4       = array("1557.78",   "953.54",  "9976.26",     "8673456.16");
	$tbl_2_pg_5       = array("6321.78",   "1545.36", "3534535.91",  "66456456.93");
	$tbl_2_pg_6       = array("8565.78",   "3545.23", "77777333.01", "274564564.12");
	$tbl_2_pages      = array($tbl_2_pg_1, $tbl_2_pg_2, $tbl_2_pg_3, $tbl_2_pg_4, $tbl_2_pg_5, $tbl_2_pg_6);
	
	
	//Start the global object.
	$str  = '{';  
		//Our first object is the array of tables
		$str .= '"tables":';
		    $str .= '[';
	    	//Just for now lets output the first table
	    		$str .= createJSONReportTable($tbl_1_leftTitle, $tbl_1_LeftVals, $tbl_1_rightTitle, $tbl_1_pages);
	    		$str .= ",";
	    		$str .= createJSONReportTable($tbl_2_leftTitle, $tbl_2_LeftVals, $tbl_2_rightTitle, $tbl_2_pages);
	    	$str .= ']';
	    	$str .= '"revenue_totals": [' . implode(",", $totalRevenues) . ']'; 
	    	$str .= ', "status" : "success"';
	
	//End the global object.    
	$str .= '}';
	echo $str;
}
else if($cmd == 'get_location_names')
{
	require_once("stat_login.php");
	require_once("report_locations.php");
}



?>

