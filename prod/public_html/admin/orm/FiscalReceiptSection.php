<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalReceiptSection Extends OrmCommon {
	public $fiscal_receipt_section_id;
	public $title;
	public $createdDate;
	public $createdBy;
	public $updatedDate;
	public $updatedBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='fiscal_receipt_sections';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
	}

	function updateDb($whereCond) {

		if($whereCond == '') return '';

		$set = ' `updated_date` = NOW()';
		$set .= ($this->title) ? ', `title` = "' . $this->title . '"' : '';
		$set .= ($this->updatedBy) ? ', `updated_by` = "' . $this->updatedBy . '"' : '';

		$cond = ' WHERE '.$whereCond;

		$result = lavu_query('UPDATE '.$this->table.' SET '.$set.$cond);

		return $result;
	}

	function insertDb() {
		lavu_query('INSERT INTO '.$this->table.' (`title`, ``created_date`, `created_by`, _deleted) VALUES("'.$this->title.'",NOW(), "'.$this->createdBy.'" , 0)');
	}

}
