<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 4/16/18
 * Time: 12:30 PM
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalReceiptTpl Extends OrmCommon {
	public $fiscal_receipt_tpl_id;
	public $title;
	public $regionId;
	public $format;
	public $createdDate;
	public $createdBy;
	public $updatedDate;
	public $updatedBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='fiscal_receipt_tpl';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
		if($reqData){
			$this->fiscal_receipt_tpl_id = $reqData['$fiscal_receipt_tpl_id'];
			$this->title = $reqData['title'];
			$this->regionId = $reqData['regionId'];
			$this->format = $reqData['format'];
			$this->createdBy = $reqData['createdBy'];
			$this->updatedBy = $reqData['updatedBy'];
			$this->_deleted = $reqData['_deleted'];
		}
	}

	function updateDb($whereCond) {

		if($whereCond == '') return '';

		$set = ' `updated_date` = NOW()';
		$set .= ($this->title) ? ', `title` = "' . $this->title . '"' : '';
		$set .= ($this->regionId) ? ', `fiscal_receipt_tpl_region_id` = "' . $this->regionId . '"' : '';
		$set .= ($this->format) ? ', `format` = "' . $this->format . '"' : '';
		$set .= ($this->updatedBy) ? ', `updated_by` = "' . $this->updatedBy . '"' : '';

		$cond = ' WHERE '.$whereCond;

		$result = lavu_query('UPDATE '.$this->table.' SET '.$set.$cond);

		return $result;
	}

	function insertDb() {
		lavu_query('INSERT INTO '.$this->table.' (`title`, `fiscal_receipt_tpl_region_id`, `format`, `created_date`, `created_by`, _deleted) VALUES("'.$this->title.'", "'.$this->regionId.'", "'.$this->format.'", NOW(), "'.$this->createdBy.'" , 0)');
	}

}