<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalReceiptInvoiceLocal Extends OrmCommon {
	public $fiscalReceiptInvoiceId;
	public $serverId;
	public $orderId;
	public $invoiceNumber;
	public $prePostFix;
	public $createdDate;
	public $createdBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='fiscal_receipt_invoice';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
		$this->table=$this->getDbName($dataname).'.'.$this->table;
	}

}