<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalInvoiceTypeLocal Extends OrmCommon {
	public $fiscalInvoiceTypeId;
	public $createdDate;
	public $createdBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='fiscal_invoice_type';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
		$this->table=$this->getDbName($dataname).'.'.$this->table;
	}

}