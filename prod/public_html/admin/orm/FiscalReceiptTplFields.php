<?php
/**
 * Author: OM
 * Date: 4/17/18
 * Time: 1:30 PM
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalReceiptTplField Extends OrmCommon {
	public $tplFieldId;
	public $fieldId;
	public $tplId;
	public $label;
	public $display;
	public $seq;
	public $required;
	public $showLabel;
	public $allowEdit;
	public $createdDate;
	public $createdBy;
	public $updatedDate;
	public $updatedBy;
	public $type;
	public $macros;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='fiscal_receipt_tpl_fields';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
		if($reqData){
			$this->tplFieldId = $reqData['fiscal_receipt_tpl_field_id'];
			$this->fieldId = $reqData['fiscal_receipt_field_id'];
			$this->tplId = $reqData['fiscal_receipt_tpl_id'];
			$this->label = $reqData['label'];
			$this->display = $reqData['display'];
			$this->seq = $reqData['seq'];
			$this->required = $reqData['required'];
			$this->type = $reqData['type'];
			$this->macros = $reqData['macros'];
			$this->createdBy = $reqData['createdBy'];
			$this->updatedBy = $reqData['updatedBy'];
			$this->_deleted = $reqData['_deleted'];
		}
	}

	function updateDb($whereCond) {

		if($whereCond == '') return '';

		$set = ' `updated_date` = NOW()';
		$set .= ($this->fieldId) ? ', `fiscal_receipt_field_id` = "' . $this->fieldId . '"' : '';
		$set .= ($this->tplId) ? ', `fiscal_receipt_tpl_id` = "' . $this->tplId . '"' : '';
		$set .= ($this->label) ? ', `label` = "' . $this->label . '"' : '';
		$set .= ($this->display) ? ', `display` = "' . $this->display . '"' : '';
		$set .= ($this->seq) ? ', `seq` = "' . $this->seq . '"' : '';
		$set .= ($this->macros) ? ', `macros` = "' . $this->macros . '"' : '';
		$set .= ($this->allowEdit) ? ', `allow_edit` = "' . $this->allowEdit . '"' : '';		
		$set .= ($this->required) ? ', `required` = "' . $this->required . '"' : '';
		$set .= ($this->showLabel) ? ', `show_label` = "' . $this->showLabel . '"' : '';		
		$set .= ($this->updatedBy) ? ', `updated_by` = "' . $this->updatedBy . '"' : '';

		$cond = ' WHERE '.$whereCond;

		$result = lavu_query('UPDATE '.$this->table.' SET '.$set.$cond);

		return $result;
	}

	function insertDb() {
		lavu_query('INSERT INTO '.$this->table.' (`fiscal_receipt_tpl_field_id`,`fiscal_receipt_field_id`, `fiscal_receipt_tpl_id`, `label`, `display` , `seq`, `required`,`show_label`,`allow_edit`, `created_date`, `created_by`, `_deleted`,`type`,`macros`) VALUES('.$this->tplFieldId.'"'.$this->fieldId.'", "'.$this->tplId.'", "'.$this->label.'", "'.$this->display.'", "'.$this->seq.'", "'.$this->required.'","'.$this->showLabel.'",NOW(), "'.$this->createdBy.'" , 0,"'.$this->type.'","'.$this->macros.'")');
	}

}
