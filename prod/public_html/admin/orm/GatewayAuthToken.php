<?php
/**
Author: OM
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class GatewayAuthToken  Extends OrmCommon {
	public $id;
	public $extensionId;
	public $merchantId;
	public $token;
	public $tokenExpirationDate;
	public $permissionGranted;
	public $locationName;
	public $locationId;
	public $tokenGeneratedDate;
	public $extraInfo;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='gateway_auth_tokens';
	public $cols=array('id','dataname','extension_id','merchant_id','token','token_expiration_date','permission_granted','location_name','location_id','token_generated_date','extra_info','_deleted');	

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
	}

	function updateDb($whereCond) {

		if($whereCond == '') return '';

		$set = ' `updated_date` = NOW()';

		$cond = ' WHERE '.$whereCond;

		$result = lavu_query('UPDATE '.$this->table.' SET '.$set.$cond);

		return $result;
	}

	//function insertDb() {}

}
