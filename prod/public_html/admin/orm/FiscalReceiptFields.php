<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 4/16/18
 * Time: 12:58 PM
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalReceiptFields Extends OrmCommon {
	public $fiscal_receipt_field_id;
	public $title;
	public $label;
	public $type;
	public $format;
	public $status;
	public $description;
	public $createdDate;
	public $createdBy;
	public $updatedDate;
	public $updatedBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='fiscal_receipt_fields';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
		if($reqData){
			$this->fiscal_receipt_field_id = $reqData['fiscal_receipt_field_id'];
			$this->title = $reqData['name'];
			$this->label = $reqData['label'];
			$this->type = $reqData['type'];
			$this->status = $reqData['status'];
			$this->createdBy = $reqData['createdBy'];
			$this->updatedBy = $reqData['updatedBy'];
			$this->_deleted = $reqData['_deleted'];
		}
	}

	function updateDb($whereCond) {

		if($whereCond == '') return '';

		$set = ' `updated_date` = NOW()';
		$set .= ($this->title) ? ', `name` = "' . $this->name . '"' : '';
		$set .= ($this->label) ? ', `label` = "' . $this->label . '"' : '';
		$set .= ($this->type) ? ', `type` = "' . $this->type . '"' : '';
		$set .= ($this->description) ? ', `description` = "' . $this->description . '"' : '';		
		$set .= ($this->status) ? ', `status` = "' . $this->status . '"' : '';
		$set .= ($this->updatedBy) ? ', `updated_by` = "' . $this->updatedBy . '"' : '';

		$cond = ' WHERE '.$whereCond;

		$result = lavu_query('UPDATE '.$this->table.' SET '.$set.$cond);

		return $result;
	}

	function insertDb() {
		lavu_query('INSERT INTO '.$this->table.' (`title`, `label`, `type`, `status`, `description`,`created_date`, `created_by`, _deleted) VALUES("'.$this->title.'", "'.$this->label.'", "'.$this->type.'", "'.$this->status.'","'.$this->description.'", NOW(), "'.$this->createdBy.'" , 0)');
	}

}