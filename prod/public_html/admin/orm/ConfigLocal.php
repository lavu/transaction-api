<?php

require_once(dirname(__FILE__) . "/OrmCommon.php");
class ConfigLocal Extends OrmCommon {
	public $id;
	public $setting;
	public $value;
	public $createdDate;
	public $createdBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='config';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
		$this->table=$this->getDbName($dataname).'.'.$this->table;
	}

}
