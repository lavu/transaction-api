<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalReceiptTplRegion Extends OrmCommon {
	public $fiscal_receipt_tpl_region_id;
	public $region;
	public $country;
	public $countryId;
	public $createdDate;
	public $createdBy;
	public $updatedDate;
	public $updatedBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='fiscal_receipt_tpl_region';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
	}

	function updateDb($whereCond) {

		if($whereCond == '') return '';

		$set = ' `updated_date` = NOW()';
		$set .= ($this->region) ? ', `region` = "' . $this->region . '"' : '';
		$set .= ($this->country) ? ', `country` = "' . $this->country . '"' : '';
		$set .= ($this->countryId) ? ', `country_id` = "' . $this->countryId . '"' : '';
		$set .= ($this->updatedBy) ? ', `updated_by` = "' . $this->updatedBy . '"' : '';

		$cond = ' WHERE '.$whereCond;

		$result = lavu_query('UPDATE '.$this->table.' SET '.$set.$cond);

		return $result;
	}

	function insertDb() {
		lavu_query('INSERT INTO '.$this->table.' (`region`, `country`, `country_id`, ``created_date`, `created_by`, _deleted) VALUES("'.$this->region.'", "'.$this->country.'", "'.$this->country_id.'",NOW(), "'.$this->createdBy.'" , 0)');
	}

}
