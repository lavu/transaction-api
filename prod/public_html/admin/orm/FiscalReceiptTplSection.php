<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalReceiptTplSection Extends OrmCommon {
	public $tplSectionId;
	public $tplId;
	public $sectionId;
	public $allowEdit;
	public $createdDate;
	public $createdBy;
	public $updatedDate;
	public $updatedBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='fiscal_receipt_tpl_sections';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
	}

	function updateDb($whereCond) {
		return 0;
	}

	function insertDb() {
		return 0;
	}

}
