<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 4/17/18
 * Time: 1:30 PM
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class FiscalReceiptTplLocal Extends OrmCommon {
	public $tplFieldId;
	public $fieldId;
	public $tplId;
	public $label;
	public $display;
	public $value;
	public $createdDate;
	public $createdBy;
	public $updatedDate;
	public $updatedBy;
	public $_deleted;
	public $hasError = 0;
	public $dataname = '';
	public $table = 'fiscal_receipt_tpl';

	public function __construct($dataname, $reqData = ''){
		$this->dataname = $dataname;
		$this->table = $this->getDbName($dataname) . '.' . $this->table;
		if ($reqData) {
			$this->tplFieldId = $reqData['fiscal_receipt_tpl_field_id'];
			$this->tplId = $reqData['fiscal_receipt_tpl_id'];
			$this->value = $reqData['value'];
			$this->label = $reqData['label'];
			$this->display = $reqData['display'];
			$this->createdBy = $reqData['created_by'];
			$this->updatedBy = $reqData['updated_by'];
			$this->_deleted = $reqData['_deleted'];
		}
	}

	public function replaceDb($whereCond=''){
		if($this->tplFieldId && $this->tplId){
			if($whereCond==''){
				$whereCond='fiscal_receipt_tpl_field_id='.$this->tplFieldId.' AND fiscal_receipt_tpl_id='.$this->tplId;
			}
			$this->updateDb($whereCond);
		} else {
			$this->insertDb();
		}
	}

	public function updateDb($whereCond) {

		if ($whereCond == '') return '';

		$set = '';
		$set .= ($this->tplId) ? ', `fiscal_receipt_tpl_id` = "' . $this->tplId . '"' : '';
		$set .= ($this->value) ? ', `value` = "' . $this->value . '"' : '';
		$set .= ($this->label) ? ', `label` = "' . $this->label . '"' : '';
		$set .= ($this->display) ? ', `display` = "' . $this->display . '"' : '';
		$set .= ($this->updatedBy) ? ', `updated_by` = "' . $this->updatedBy . '"' : '';
		
		if ($set == '') {
			return 0;
		}

		$set .= ', `updated_date` = NOW()';

		$result = lavu_query('UPDATE '.$this->table.' SET '.$set.' WHERE '.$whereCond);

		return $result;
	}

	public function deleteDb($whereCond) {

		if ($whereCond == '') return '';

		$result = lavu_query('DELETE FROM ' . $this->table . ' WHERE ' . $whereCond);

		return $result;
	}

	public function insertDb() {
		if($this->tplFieldId && $this->tplId){
			lavu_query('INSERT INTO '.$this->table.' (`fiscal_receipt_tpl_field_id`,`fiscal_receipt_tpl_id`,`value`, `label`, `display`,`created_date`, `created_by`,`updated_by`,`_deleted`) VALUES('.$this->tplFieldId.',"'.$this->tplId.'","'.$this->value.'","'.$this->label.'", "'.$this->display.'",NOW(), '.$this->createdBy.','.$this->updatedBy.', 0)');
			return 1;
		}else{
			return 0;
		}
	}

}