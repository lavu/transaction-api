<?php
class OrmCommon{

	public $dbPrefix = "poslavu_";
	public $dbPostfix = "_db";

	public function getInfo($fields, $whereCond = '', $index = '', $order = '', $all = false){
		$cond = ($all) ? 1 : " _deleted ='0'";
		$cond = ($whereCond) ? $cond . ' AND ' . $whereCond : $cond;
		$fields = ($fields != '') ? $fields : '*';
		$dbResult = $this->getData($fields, $cond, $order);

		$recs = array();
		if ($dbResult) {
			while($rec = mysqli_fetch_assoc($dbResult)){
				if($index){
					$recs[$rec[$index]] = $rec;
				}else{
					$recs[] = $rec;
				}
			}
		} else {
			return '';
		}

		return $recs;

	}

	public function getDbName($dataname){
		return $this->dbPrefix.$this->dataname.$this->dbPostfix;
	}

	private function getData($fields, $whereCond='', $order=''){
		$result = mlavu_query('SELECT ' . $fields . ' FROM ' . $this->table . (($whereCond) ? ' WHERE '. $whereCond : '').(($order)?' ORDER BY '.$order:''));
		if(mysqli_num_rows($result)){
			return $result;
		}
	}


}
