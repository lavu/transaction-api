<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class ServerCashDrawer Extends OrmCommon{

	public $id;
	public $printer;
	public $drawer;
	public $serverId;
	public $assignedBy;
	public $createdDate;
	public $createdBy;
	public $updatedDate;
	public $updatedBy;
	public $_deleted = 0;
	public $hasError = 0;
	public $dataname = '';
	public $table = 'server_cash_drawer';

	public function __construct($dataname){
		$this->dataname = $dataname;
		$this->table = $this->getDbName($dataname) . '.' . $this->table;
	}
	
	function updateDb($whereCond){
	
		if ($whereCond == '') return '';

		$set = ' updated_date = NOW()';
		$set .= ($this->printer) ? ', printer = "' . $this->printer . '"' : '';
		$set .= ($this->drawer) ? ', drawer = "' . $this->drawer . '"' : '';
		$set .= ($this->serverId || $this->serverId == 0) ? ', server_id = "' . $this->serverId . '"' : '';
		$set .= ($this->assignedBy) ? ', assigned_by = "' . $this->assignedBy . '"' : '';
		$set .= ($this->updatedBy) ? ', updated_by = "' . $this->updatedBy . '"' : '';
		$set .= ($this->_deleted == 0 || $this->_deleted == 1) ? ', _deleted = "' . $this->_deleted . '"' : '';

		$cond = ' WHERE ' . $whereCond;

		lavu_query('UPDATE ' . $this->table . ' SET ' . $set . $cond);

		return 1;
	}

	function insertDb(){
		lavu_query('INSERT INTO '.$this->table.' (printer, drawer, server_id, assigned_by, created_date, created_by, _deleted) VALUES("'.$this->printer.'", "'.$this->drawer.'", "'.$this->serverId.'", "'.$this->assignedBy.'", NOW(),"'.$this->createdBy.'", 0)');
	}

}
?>
