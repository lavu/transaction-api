<?php

include_once($_SERVER['DOCUMENT_ROOT'].'/orm/OrmCommon.php');

class AdminActionLog Extends OrmCommon {
	public $id;
	public $setting;
	public $value;
	public $createdDate;
	public $createdBy;
	public $_deleted;
	public $hasError=0;
	public $dataname='';
	public $table='admin_action_log';

	public function __construct($dataname, $reqData = ''){
		$this->dataname=$dataname;
		$this->table=$this->getDbName($dataname).'.'.$this->table;
	}

}
