<?php 
	session_start();
	putenv("TZ=America/Chicago");
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	ini_set("display_errors","1");
	
	$ll_mode = "visitor";
	$found = false;
	$requri_parts = explode("/",$_SERVER['REQUEST_URI']);
	$dgc=$requri_parts[4];
	if($dgc!='') { $dg='#'.$dgc; }
	else { $dg='#ffffff'; }
	if(isset($requri_parts[3]))
	{
		$foldername = $requri_parts[3];
		if(trim($foldername)!="")
		{
			$rest_query = mlavu_query("select `data_name`,`company_name`,`chain_id` from `poslavu_MAIN_db`.`restaurants` where `data_name` LIKE '[1]'",$foldername);
			if(mysqli_num_rows($rest_query))
			{
				$found = true;
				$rest_read = mysqli_fetch_assoc($rest_query);
				$data_name = $rest_read['data_name'];
				$fname = dirname(__FILE__)."/../lib/management/lavu_loyalty_webgiftversion.php";

				$chain_name = "";
				$chain_id = $rest_read['chain_id'];
				if(trim($chain_id)!="")
				{
					$chain_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurant_chains` where `id`='[1]'",$chain_id);
					if(mysqli_num_rows($chain_query))
					{
						$chain_read = mysqli_fetch_assoc($chain_query);
						$chain_name = $chain_read['name'];
					}
				}
				if(trim($chain_name)=="") $chain_name = $rest_read['company_name'];
				$show_chain_name = strip_tags($chain_name);

				//echo "<!DOCTYPE html PUBLIC \"-//WAPFORUM//DTD XHTML Mobile 1.0//EN\" \"http://www.wapforum.org/DTD/xhtml-mobile10.dtd\">\n";
				//echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1\">";
				echo "<html>";
				echo "<style>";
				echo "	body, table { font-family:Verdana,Arial; color:#444444; font-size:12px; margin:0px;} ";
				echo "	h2 { font-family:Verdana,Arial; color:#335533; font-size:20px; } ";
				echo "</style>";

				$is_mobile_device = false;
				$useragent = $_SERVER['HTTP_USER_AGENT'];
				if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) $is_mobile_device = true;

				require_once($fname);

				if($is_mobile_device)
				{
					echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=.7, minimum-scale=.7\">";
					$top_str = "";
					$btm_str = "";
				}
				else
				{
					$top_str = "<table cellspacing=0 cellpadding=0 width='100%' height='100%'><tr><td align='center' valign='middle' width='100%' height='100%' bgcolor='".$dg."'>";
					$btn_str = "</td></tr></table>";
				}

				echo "<center>";
				echo $top_str;
				echo "<table cellspacing=0 cellpadding=8 >";
				//echo "<tr><td style='border:solid 2px #aaaaaa' align='left' bgcolor='#FFFFFF'>";
				//echo "<font style='color:#000000; font-size:14px; font-family:Verdana,Arial'>Company: <b>$show_chain_name</b></font>";
				//echo "</td></tr>";
				echo "<tr><td  align='center' valign='top' >";
				echo $display;
				echo "</td></tr>";
				echo "</table>";
				echo $btm_str;
				echo "</center>";

				echo "<script type='text/javascript'>";
				echo "	document.getElementById('card_num_input').focus(); ";
				echo "</script>";
				echo "</html>";
			}
		}

		if(!$found)
		{
			header('HTTP/1.0 404 Not Found');
			echo "<h1>404 Not Found</h1>";
			exit();
		}
	}
?>