<?php
	/*
		This is the entry area for using a web sign in.
	 */
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/session_functions.php");
	standarizeSessionDomain();
	session_start();
	putenv("TZ=America/Chicago");
	require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
	// error_log("---------------");
	// foreach($_GET as $key => $val) error_log("_GET: $key = $val");
	// foreach($_POST as $key => $val) error_log("_POST: $key = $val");
	// foreach($_SESSION as $key => $val) error_log("_SESSION: $key = $val");
	// foreach($_SERVER as $key => $val) error_log("_SERVER: $key = $val");

	$rdb = admin_info("database");
	$loggedin_id = admin_info("loggedin");
	$data_name = sessvar("admin_dataname");
	$locationid = sessvar("locationid");
	$locinfo = sessvar('location_info');
	$comPackage = $locinfo['component_package_code'];
	$maindb = "poslavu_MAIN_db";
	$in_lavu = 1;
	$lavu_minimum_login_level = 1;
	$switch_to_history = postvar('switch_to_history', 0);
	// error_log($switch_to_history);

	//------------------------------- Functions also defined in admin.poslavu.com index.php file (yes it is repeated code =O )-------------------//
	function check_lockout_status($lockout_level){
		return true;
		$lockfname = "/home/poslavu/private_html/connects_lockout.txt";
		$fp = fopen($lockfname,"r");
		$lockout_status = fread($fp,filesize($lockfname));
		fclose($fp);
		if($lockout_status!=""){
			$lockout_parts = explode("|",$lockout_status);
			if(count($lockout_parts) > 1 && $lockout_parts[0] * 1 >= $lockout_level && $lockout_parts[1] * 1 > time() - 300){
				echo "We apologize for the inconvenience but the POSLavu BackEnd is temporarily Unavailable. Please check back later.";
				exit();
			}
		}
	}

	function is_dev(){
		if(strpos($_SERVER['REQUEST_URI'],"v2/")!==false){
			return true;
		}else{
			return false;
		}
	}

	function is_lavu_admin(){
		if (admin_info("lavu_admin") == 1){
			return true;
		}else{
			return false;
		}
	}

	function getLocationInfo($locId){
		$locationsResult = lavu_query("SELECT * FROM `locations` WHERE `id`='[1]'", $locId);
		if($locationsResult && mysqli_num_rows($locationsResult) ){
			return mysqli_fetch_assoc($locationsResult);
		}else{
			return false;
		}
	}

	function setDefaultLocation() {
		if(admin_info("loc_id")==0 || !is_numeric(admin_info("loc_id"))){
			$location_query = lavu_query("select * from `locations` WHERE `_disabled` != '1' order by `title` asc limit 1");
			if(mysqli_num_rows($location_query)){
				$location_read = mysqli_fetch_assoc($location_query);
				set_sessvar("locationid",$location_read['id']);
				set_sessvar("admin_loc_id",$location_read['id']);
			}
		}
		return admin_info("loc_id");
	}

	/***DB CONNECTS ****/
	check_lockout_status(2);
	lavu_auto_login();
	lavu_connect_byid(admin_info("companyid"),$rdb);
	setDefaultLocation();
	/***********END DB CONNECTS *****/

	if(isset($_POST['logout'])){admin_logout();}
	$mode = (isset($_POST['mode']))?$_POST['mode']:"individual_card";
	if(!admin_loggedin()){
		$content_status .= "&nbsp;";
		ob_start();
		define( 'LAVU_MINIMUM_LOGIN_LEVEL', 1 );
		header('Location: /cp/index.php'); // Refer LP-8841
		exit;
		require_once("/home/poslavu/public_html/admin/cp/areas/login.php");
		$outputHTML  = ob_get_clean();
		//$outputHTML .= "<script type='text/javascript'>document.getElementById('header').innerHTML='Employee Login'; document.title='Employee Login';</script>";
		// $outputHTML = str_replace( '<span class="section_title">Log in</span>', '<span class="section_title">Employee Login</span>', $outputHTML );
		$outputHTML = str_replace("styles/","/cp/styles/",$outputHTML);
		$outputHTML = str_replace("scripts/","/cp/scripts/",$outputHTML);
		echo $outputHTML;
		exit();
	}else{
		// G L O B A L   V A R I A B L E S...
		$loc_id = admin_info("loc_id");
		$location_info = getLocationInfo($loc_id);

		#code added by Ravi Tiwari to list card history with lazyload
		if(isset($_REQUEST['last_gift_cards_id']) && $_REQUEST['last_gift_cards_id']!=''){
			require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
			#Get chain Id
			$chain_id = "";
			$last_gift_cards_id = $_REQUEST['last_gift_cards_id'];

			$account_query = mlavu_query("SELECT `chain_id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $data_name);
			if(mysqli_num_rows($account_query) > 0){
				$account_read = mysqli_fetch_assoc($account_query);
				$chain_id = $account_read['chain_id'];
			}
			#Get cart history data
			if(strlen($chain_id) > 1) {
				require_once('/home/poslavu/public_html/admin/sa_cp/manage_chain_functions.php');
				$chainedDataname = array();
				$chainedRestaurants =get_restaurants_by_chain($chain_id );	#get all chained restaurants
				foreach($chainedRestaurants as $restaurants ) {
					$chainedDataname[] = $restaurants['data_name'];
				}

				$select_query = mlavu_query("SELECT `chainid`, `name`, `balance`,`id`, `created_datetime`, `points`, `expires`, `history`, `dataname` FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted` = 0 AND `inactive` =0 AND (`dataname` IN('".implode("','", $chainedDataname)."') OR `chainid`='[1]') AND id < '".$_REQUEST['last_gift_cards_id']."' ORDER BY id DESC LIMIT 0,100 ", $chain_id);
			}
			else {
				$select_query = mlavu_query("SELECT `chainid`, `name`, `balance`,`id`, `created_datetime`, `points`, `expires`, `history`, `dataname` FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted` = 0 AND `inactive` =0 AND `dataname`='[1]' AND id < '".$_REQUEST['last_gift_cards_id']."' ORDER BY id DESC LIMIT 0,100 ", $data_name, $chain_id);
			}
			
			$response="";

			if(mysqli_num_rows($select_query) > 0)
			{
				while($card_read = mysqli_fetch_assoc($select_query)) {
					$response.="<tr class='data_tr active_data' id='".$card_read['id']."'><td class='data_td'>".$card_read['name']."</td><td class='data_td' style='text-align: right;'>".number_format($card_read['balance'], 2)."</td><td class='data_td'>".$card_read['points']."</td><td class='data_td'>".$card_read['created_datetime']."</td></tr>";
				}
			}
			echo $response;
		}else{
			require_once("entry.php"); //success! Let Entry.php handle navigation & html
		}
		
	}


?>
