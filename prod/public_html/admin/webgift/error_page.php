<!doctype HTML>
<html class="no-ie modern" lang="en">
<head>
	<title>Admin Login</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=8;IE=Edge"/>
	<link rel="stylesheet" type="text/css" href="../cp/styles/fontcustom/fontcustom.css">
	<link rel="stylesheet" type="text/css" href="../cp/styles/lavu-custom-input-elements.css">
	<link rel="stylesheet" type="text/css" href="../cp/styles/loginStyles.css">
	<script type="text/javascript" src="../cp/scripts/customelements.js"></script>
	<script type="text/javascript" src="../cp/scripts/lavu-custom-input-elements.js"></script>
	<script type="text/javascript" src="../cp/scripts/login.js"></script>
	<script type="text/javascript">
		var f = function( event ) {
			window.setTimeout(function(){
				var e = document.querySelector('lavu-container,[isa=lavu-container]');
				e.setAttribute('show','');
			}, 100);
		}
		document.addEventListener('DOMContentLoaded', f, false );
	</script>
</head>
<body>
	<iframe name="proxy_submit" hidden style="display:hidden;">
	</iframe>
	<lavu-container>
		<lavu-top-container>
			<a href="https://www.lavu.com"><lavu-logo></lavu-logo></a>
		</lavu-top-container>
		<lavu-bottom-container>
			<lavu-relative>
				<lavu-section id="login_section" class="section" show>
					<span class="section_title">You don't have access to this functionality!</span>
				</lavu-section>
			</lavu-relative>
		<lavu-bottom-container>
	</lavu-container>
</body>
<![CDATA[EXTRA_SCRIPTS_TO_RUN]]>
</html>