<?php

function getTabbedMenuHTML(){
	global $mode;

	ob_start();
	$t1color = "#aaaaaa";	//"#98b624";
	$t2color = "#aaaaaa";

	if($mode=="history"){
		$t2color = "#98b624";
	}else{
		$t1color = "#98b624";
	}
	?>
	<form method='post' id='tabform'>
	<input type='hidden' id='mode' name='mode' value='<?php echo $mode; ?>'>
	<input type='hidden' id='card_id' name='card_id' value='' />
	</form>
	<table cellpadding="0" cellspacing="1">
		<tr>
			<td style='width:250px;height:30px;background-color:<?php echo $t1color?>;color:#fff;text-align:center;vertical-align:middle;cursor:pointer'
						onclick="document.getElementById('mode').value = 'individual_card'; document.getElementById('tabform').submit();">
				Manage Individual Cards
			</td>
			<td style='width:200px;height:30px;background-color:<?php echo $t2color?>;color:#fff;text-align:center;vertical-align:middle;cursor:pointer'
						onclick="document.getElementById('mode').value = 'history'; document.getElementById('tabform').submit();">
				History & Tracking
			</td>
		</tr>
	</table>
	<?php
	$outputHTMLReturn = ob_get_clean();
	return $outputHTMLReturn;
}
// Top HTML

if(!$_POST['is_ajax_request']){
	$tabbedMenuHTML = getTabbedMenuHTML();
	$fullname = admin_info("fullname");
	$header = <<<HTMLd
	<style>
	body, table { font-family:Verdana,Arial; font-size:12px; }
	</style>
	<body bgcolor='#d4d6d4'>
	<center>
	<table cellpadding=12 cellspacing=0 bgcolor='#ffffff' align='center' border='0px'>
		<tr>
			<td align='left' bgcolor='#eeeeee'>
				<div>$tabbedMenuHTML</div>
			</td>
			<!--<td align='right' bgcolor='#eeeeee'>
				<div><font style='color:#777777'>Logged In:</font>
				<font style='color:#444488'> $fullname </font>&nbsp;&nbsp;&nbsp;&nbsp;
				<form style='display:inline-block' method='POST'>
				<input type='submit' style='font-size:10px' value='Logout' name='logout'></div>
				</form>
	</td>--></tr>
	<tr><td align='middle' valign='top' height='600' colspan='80'>
HTMLd;
	echo $header;
}

if($switch_to_history){
	$mode = 'history';
}
$_SESSION['management_session'] = true;
if($mode == 'history'){
	$fname = dirname(__FILE__)."/../lib/management/lavu_loyalty_history.php";
}else { //individual card/default
	$fname = dirname(__FILE__)."/../lib/management/lavu_loyalty.php";
}
// error_log("fname: $fname");
$ll_mode = "admin";
if(is_file($fname)){
	if(!$_POST['is_ajax_request']){
		echo "<div style='padding:8px'>";
	}
	require_once($fname);
	echo "$display";
	if(!$_POST['is_ajax_request']){
		echo "</div>";
	}
}else if($mode=="reset_password"){
	echo "<table><tr><td width='800' align='center'><br><br><br><br>Password has been reset.<br><br><input type='button' value='Continue >>' onclick='window.location = \"/\"' /></td></tr></table>";
}

if(!$_POST['is_ajax_request']){
	// Bottom HTML
	echo "</td></tr></table>";
	echo "<div style='position:fixed; left:0px; top:50px; width:100%; height:100%; z-index:100; visibility:hidden' id='floating_window'>";
	echo "<table width='100%'><tr><td align='center'>";
	echo "<table style='border:solid 2px #777777' cellpadding=6 cellspacing=0 bgcolor='#ffffff'>";
	echo "<tr><td align='right'><b><font style='color:#aaaaaa; font-size:20px; cursor:pointer' onclick='close_floating_window()'>X</font></b></td></tr>";
	echo "<tr>";
	echo "<td width='480' height='320' id='floating_window_content' align='center' valign='top'>";
	echo "&nbsp;";
	echo "</td>";
	echo "</tr>";
	echo "</table>";
	echo "</td></tr></table>";
	echo "</div>";
	echo "<script language='javascript'>";
	echo "function open_floating_area(load_area,load_vars) { ";
	echo "	document.getElementById('floating_window_content').innerHTML = '<table width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"center\"><img src=\"http://hotel.lavuhotel.com/cp/images/animated-progress.gif\" /></td></tr></table>'; ";
	echo "	document.getElementById('floating_window').style.visibility = 'visible'; ";
	echo "	runGenericAjaxRequestDennedyStyle(load_area,load_vars,'floating_window_content'); ";
	echo "} ";
	echo "function open_floating_window(set_content) { ";
	echo "	document.getElementById('floating_window_content').innerHTML = set_content; ";
	echo "	document.getElementById('floating_window').style.visibility = 'visible'; ";
	echo "} ";
	echo "function close_floating_window() { ";
	echo "	document.getElementById('floating_window_content').innerHTML = ''; ";
	echo "	document.getElementById('floating_window').style.visibility = 'hidden'; ";
	echo "} ";
	echo "function find_and_eval_script_tags(estr,script_start,script_end) ";
	echo "{ ";
	echo "  var xhtml = estr.split(script_start); ";
	echo "	for(var n=1; n<xhtml.length; n++) ";
	echo "	{ ";
	echo "		var xhtml2 = xhtml[n].split(script_end); ";
	echo "		if(xhtml2.length > 1) ";
	echo "		{ ";
	echo "			run_xhtml = xhtml2[0]; ";
	echo "			eval(run_xhtml); "; //WHAT. WHY!?
	echo "		} ";
	echo "	} ";
	echo "} ";
	echo "</script>";
	echo "</center>";
	echo "</body>";
}
?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript">
    $("#data_div").scroll(function() {
      var is_complete = $('#table tr').last().data('complete');
      if ($("#data_div").scrollTop() + $("#data_div").height() >= $("#data_div")[0].scrollHeight && !is_complete) {
        var last_id = $(".data_tr").last().attr("id");
        loadMoreData(last_id);
        var latest_id = $(".data_tr").last().attr("id");
        if (last_id === latest_id) {
          $(".data_tr").last().data('complete', true);
        }
      }
    });
    function loadMoreData(last_id){
      $.ajax({
		url: 'index.php?last_gift_cards_id=' + last_id,
		type: "get",
		beforeSend: function(){
			$('#ajax_load').show();
		}
    	})
        .done(function(data){
			$('#ajax_load').hide();
			$("#table").append(data);
		})
        .fail(function(jqXHR, ajaxOptions, thrownError) {
			console.log('server not responding...');
		});
    }
</script>
