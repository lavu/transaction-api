<?php

class MarshallingArrays{

	public static function resultToArray($mysqliResult, $columnValueAsKey = false){
		if(empty($mysqliResult)){
			return false;
		}
		$resultArr = array();
		while($curr = mysqli_fetch_assoc($mysqliResult)){
			if(!empty($columnValueAsKey) && is_string($columnValueAsKey)){
				$resultArr[$curr[$columnValueAsKey]] = $curr;
			}else{
				$resultArr[] = $curr;
			}
		}
		return $resultArr;
	}

	public static function resultToInsertIDUpdatedRows($mysqliResult, $useAdmin = false){
		$insertID = ($useAdmin) ? DBConnection::adminInsertID() : DBConnection::clientInsertID();
		$rowsUpdated = ($useAdmin) ? DBConnection::adminAffectedRows() : DBConnection::clientAffectedRows();
		$returnArr = array("InsertID" => $insertID, "RowsUpdated" => $rowsUpdated, "Status" => "S", "msg" => array("InsertID" => $insertID, "RowsUpdated" => $rowsUpdated));  // OLO TO DO : see if any OLO front-end code uses msg so we can get rid of it
		return $returnArr;
	}

	public static function resultFirstRow($mysqliResult){
		if(empty($mysqliResult)){ return false; }
		if(mysqli_num_rows($mysqliResult) == 0){ return false; }
		return mysqli_fetch_assoc($mysqliResult);
	}

	//Note array1 and array2 should be the same size
	public static function arrayZip($array1, $array2){
		return array_map(null, $array1, $array2);
	}

	public static function arrayKeyZip($array1, $array2){
		$keyZippedArr = array();
		for($i = 0; $i < count($array1); $i++){
			$keyZippedArr[$array1[$i]] = $array2[$i];
		}
		return $keyZippedArr;
	}

	//Takes an array, and returns another array with embedded array of group size.
	// e.g.  arrayGroup( array(1,2,3,4,5,6,7,8), 3) => array( array(1,2,3), array(4,5,6), array(7,8))
	public static function arrayGroup($array, $perGroup){
		$groupedArr = array();
		for($i = 0; $i < count($array); $i++){
			if($groupedArr[floor($i/$perGroup)]){
				$groupedArr[floor($i/$perGroup)] = array();
			}
			$groupedArr[floor($i/$perGroup)][] = $array[$i];
		}
		return $groupedArr;
	}

	public static function stringWrap($array, $leftStr, $rightStr){
		$strWrapArr = array();
		foreach($array as $currString){
			$strWrapArr[] = $leftStr.$currString.$rightStr;
		}
		return $strWrapArr;
	}

	public static function insertBetween($value, $array){
		$combinedArray = array();
		for($i = 0; $i < count($array); $i++){
			$combinedArr[] = $array[$i];
			if($i < count($array) - 1){
				$combinedArr[] = $value;
			}
		}
		return $combinedArray;
	}

	public static function subArrayByKey($array, $key){
		$subArrayByKey = array();
		foreach($array as $subArray){
			if(!is_array($subArray)){
				continue;
			}
			$subArrayByKey[] = $subArray[$key];
		}
		return $subArrayByKey;
	}

	public static function overwriteArray($array, $replacements){
		$overwrittenArr = $array;
		foreach($replacements as $key => $val){
			$overwrittenArr[$key] = $val;
		}
		return $overwrittenArr;
	}

	public static function numericArrayToAssociativeByKey($array, $key){
		$assocArray = array();
		foreach($array as $currArr){
			$newKey = $currArr[$key];
			$assocArray[$newKey] = $currArr;
		}
		return $assocArray;
	}

	public static function unsetOptionalArguments($array){
		foreach($array as $key => $value){
			if($value == API_MISSING_OPTIONAL_FIELD){
				unset($array[$key]);
			}
		}
		return $array;
	}


}


class MA extends MarshallingArrays{}
