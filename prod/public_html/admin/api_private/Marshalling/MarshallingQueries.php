<?php

require_once(__DIR__.'/../Database/DBConnection.php');

class MarshallingQueries{

	//$columns An array of strings representing the columns of the insert.
	//$valueRows an array of arrays, where the embedded arrays are the rows to insert.
	public static function sanitizeColumnAndRows($columns, $valueRows, $includeEncapsulation = true){
		$includeEncapsulation = isset($includeEncapsulation) ? $includeEncapsulation : true;
		$encapsulationChars = $includeEncapsulation ? array("`","'") : array("","");
		$sanitizedColumns = self::sanitizeValuesOfArray($columns, $encapsulationChars[0]);
		$sanitizedValueRows = self::sanitizeArrayOfRows($valueRows, $encapsulationChars[1]);
		return array("columns" => $sanitizedColumns, "rows" => $sanitizedValueRows);
	}

	public static function sanitizeValuesOfArray($arr, $encapsulationChar){
		$values = array_values($arr);
		$returnArr = array();
		foreach($values as $currValue){
			$returnArr[] = $encapsulationChar.DBConnection::sanitize($currValue).$encapsulationChar;
		}
		return $returnArr;
	}

	public static function sanitizeArrayOfRows($arrOfRows, $encapsulationChar){
		$arrOfRows = array_values($arrOfRows);
		$sanitizedRows = array();
		foreach($arrOfRows as $currInsertRow){
			$sanitizedRow = self::sanitizeValuesOfArray($currInsertRow, $encapsulationChar);
			$sanitizedRows[] = $sanitizedRow;
		}
		return $sanitizedRows;
	}

	public static function buildSanitizedInsert($table, $columns, $valueRows){
		$sanitizedTable = DBConnection::sanitize($table);
		$sanitizedInsertParts = self::sanitizeColumnAndRows($columns, $valueRows);
		$sanitizedColumns = $sanitizedInsertParts['columns'];
		$sanitizedRows = $sanitizedInsertParts['rows'];
		$sanitizedRowOfStrings = array();
		foreach($sanitizedRows as $currArr){
			$sanitizedRowOfStrings[] = "(".implode(',',$currArr).")";
		}
		$sanitizedQuery = "INSERT INTO $sanitizedTable (".implode(',', $sanitizedColumns).") VALUES " .implode(',',$sanitizedRowOfStrings);
		return $sanitizedQuery;
	}

	public static function buildSanitizedUpdate($table, $setColumnKey2ValueArr, $endClause){
		$columns = array_keys($setColumnKey2ValueArr);
		$rows = array(array_values($setColumnKey2ValueArr));
		$sanitizedColumnsAndValues = self::sanitizeColumnAndRows($columns, $rows, true);
		$columns = $sanitizedColumnsAndValues['columns'];
		$values  = $sanitizedColumnsAndValues['rows'][0];
		$setClause = '';
		for($i = 0; $i < count($columns); $i++){
			$setClause .= $columns[$i] . '=' . $values[$i];
			if($i < count($columns) - 1){
				$setClause .= ',';
			}
		}
		$sanitizedQuery = "Update ".$table." SET ".$setClause."
		".$endClause;
		return $sanitizedQuery;
	}

	public static function buildSanitizedListInParenthesis($array, $encapsulationChar){
		$sanitizedArrays = self::sanitizeArrayOfRows(array($array), $encapsulationChar);
		$sanitizedArray = $sanitizedArrays[0];
		$inClause = '('.implode(",", $sanitizedArray).')';
		return $inClause;
	}

	public static function buildSanitizedColumnInParenthesis($array, $encapsulationChar)
	{
		$sanitizedArray = self::sanitizeValuesOfArray($array, $encapsulationChar);
		$selectedColumn = implode(",", $sanitizedArray);
		return $selectedColumn;
	}

	public static function buildSanitizedWhereCondition($table, $columns, $values){
		$sanitizedInsertParts = self::sanitizeColumnAndRows($columns, array($values));
		$sanitizedColumns = $sanitizedInsertParts['columns'];
		$sanitizedVals = $sanitizedInsertParts['rows'][0];
		$whereStr = "";
		$and = "";
		foreach($sanitizedColumns as $key => $column) {
			if ($sanitizedVals[$key] != "") {
				$whereStr .= $and . $column . ' = ' . $sanitizedVals[$key];
				$and = ' AND ';
			}
		}
		$sanitizedQuery = 'SELECT * FROM ' . $table . ' WHERE ' . $whereStr . ' AND `_deleted` = 0';
		return $sanitizedQuery;
	}

	public static function buildSanitizedSelect($table, $columnNameArr, $whereColumnArr, $dataname, $fieldExpression=array())
	{
		$columnNames= self::sanitizeValuesOfArray(array_values($columnNameArr), "`", "'");

		if (!empty($fieldExpression)) {
			$columnNames = array_merge($columnNames, $fieldExpression);
		}

		if($dataname != ""){
			if($dataname != "lavutogo"){
				$dataname = 'poslavu_'.$dataname.'_db';
			}
			$table = $dataname."`.`".$table."`";
		}
		else{
			$table = $table."`";
		}
		$coNames = implode(', ', $columnNames);
		$sanitizedQuery = "SELECT " .  $coNames . " FROM `" . $table;

		if(!empty($whereColumnArr)){
			$whereColumns = array_keys($whereColumnArr);
			$whereRows = array(array_values($whereColumnArr));
			$sanitizedColumnsAndValues = self::sanitizeColumnAndRows($whereColumns, $whereRows, true);
			$columns = $sanitizedColumnsAndValues['columns'];
			$values = $sanitizedColumnsAndValues['rows'][0];
			$whereClause = '';
			for ($i = 0; $i < count($columns); $i++) {
				if(strpos($values[$i], "like:") !== false ){
					$likeVar = ' like "%' . rtrim(explode(":",$values[$i])[1], "'") . '%" ';
					$whereClause .= $columns[$i] . $likeVar;
				} elseif (strpos($values[$i], "in:") !== false) {
					$indata = explode(':', trim($values[$i], "'"));
					for ($numb=1; $numb < count($indata); $numb++) {
						 $inVar .= "'".$indata[$numb]."',";
					}
					$inVar = " in (".trim($inVar, ',').") ";
					$whereClause .= $columns[$i] . $inVar;
				} else {
					$whereClause .= $columns[$i] . '=' . $values[$i];
				}

				if ($i < count($columns) - 1) {
					$whereClause .= ' AND ';
				}
			}
			$sanitizedQuery .= " WHERE " . $whereClause;
		}
		return $sanitizedQuery;
	}

	public static function buildSanitizedSelectAll($table, $whereColumnArr, $dataname = '')
	{
		$whereColumns = array_keys($whereColumnArr);
		$whereRows = array(array_values($whereColumnArr));
		$sanitizedColumnsAndValues = self::sanitizeColumnAndRows($whereColumns, $whereRows, true);
		$columns = $sanitizedColumnsAndValues['columns'];
		$values = $sanitizedColumnsAndValues['rows'][0];
		$whereClause = '';
		for ($i = 0; $i < count($columns); $i++) {
			$whereClause .= $columns[$i] . '=' . $values[$i];
			if ($i < count($columns) - 1) {
				$whereClause .= ' AND ';
			}
		}
		if($dataname != ''){
			if($dataname == 'lavutogo')
				$table = $dataname."`.`".$table;
			else
				$table = "poslavu_".$dataname."_db`.`".$table;
		}
		$sanitizedQuery = "SELECT * FROM `" . $table . "` WHERE " . $whereClause;
		return $sanitizedQuery;
	}

	public static function buildSanitizedPagination($tableName, $whereColumnArr, $paginationArr){
		$columns = array_keys($whereColumnArr);
		$rows = array(array_values($whereColumnArr));
		$sanitizedColumnsAndValues = self::sanitizeColumnAndRows($columns, $rows, true);
		$columns = $sanitizedColumnsAndValues['columns'];
		$values  = $sanitizedColumnsAndValues['rows'][0];

		$whereClause = '';
		for($i = 0; $i < count($columns); $i++){
			if(strpos($values[$i], "like:") !== false ){
				$likeVar = ' like "%' . rtrim(explode(":",$values[$i])[1], "'") . '%" ';
				$whereClause .= $columns[$i] . $likeVar;
			}
			else{
				$whereClause .= $columns[$i] . '=' . $values[$i];
			}
			if($i < count($columns) - 1){
				$whereClause .= ' AND ';
			}
		}
		$sanitizedQuery = "SELECT COUNT(*) `row_count` FROM `". $tableName. "` WHERE ". $whereClause;

		$result = DBConnection::clientQuery($sanitizedQuery);
		$paginationResult = MarshallingArrays::resultToArray($result);
		$totalRecord = (int)array_values($paginationResult[0])[0];
		$totalPage= ceil($totalRecord/$paginationArr['per_page']);
		$nextPage = ($paginationArr['current_page'] >= $totalPage) ? '' :  $paginationArr['current_page'] +1;
		$sanitizedPagination =  array("total_entries"=>$totalRecord,
									  "total_pages"=>$totalPage,
									  "current_page"=> $paginationArr['current_page'],
									  "next_page"=> $nextPage,
									  "previous_page"=>$paginationArr['current_page'] - 1,
									  "per_page"=>$paginationArr['per_page']);
		return $sanitizedPagination;
	}

	public static function buildSanitizedWhere($whereColumnArr, $operatorArr)
	{
		$whereColumns = array_keys($whereColumnArr);
		$whereRows = array(array_values($whereColumnArr));
		$operatorValues = array_values($operatorArr);
		$sanitizedColumnsAndValues = self::sanitizeColumnAndRows($whereColumns, $whereRows, true);
		$columns = $sanitizedColumnsAndValues['columns'];
		$values = $sanitizedColumnsAndValues['rows'][0];
		$whereClause = '';
		for ($i = 0; $i < count($columns); $i++) {
			if($operatorValues[$i] == 'Like') {
				$whereClause .= $columns[$i] . '  '. $operatorValues[$i]. " %". $values[$i]."%";
			} else {
				$whereClause .= $columns[$i] . '  '. $operatorValues[$i] . " ". $values[$i];
			}
		  }
		$sanitizedWhere = $whereClause;
		return $sanitizedWhere;
	}

	public static function convertIDAndCountToCommaSeparatedIDs($idInfo){
		$rowsUpdated = $idInfo['RowsUpdated'];
		$firstID = (int)$idInfo['InsertID'];
		$ids = $firstID;
		for($i = 1; $i < $rowsUpdated; $i++){
			$ids .= ",".($firstID + $i);
		}
		return $ids;
	}
}

class MQ extends MarshallingQueries{}
