
Welcome to the API.

Rules for writing code in the api:
1.) All 'Action' methods will be simplified to sequence of methods that may or may not throw APIExceptions without additional logic. 
2.) All methods will only do one, unit-testable thing. 
3.) All complicated or redundant logic will be abstracted into general case solving patterns within the Marshalling directory.
4.) Nothing will be done more than once, and everything will have single point of responsibility. 
5.) All development will be unit and/or integration test driven. 
6.) A best effort will be made that all possible Exceptions will be made and thrown when a request or the API is in an error state. 
7.) Everything will be designed before implemented.




Private API JSON-Requests:

At the top level of the private api will be the following keys:

'Component': One of the following: Customers, Inventory, Order, Reports, Accounts, Billing, Location, Logging, Menu
'Auth': A dictionary containing: user_id, dataname
'Action': The action that will be performed
'Params': The json encoded arguments that are forwarded as the $args for the action.

Thus an example API call may look like:
{
	"Component":"Orders",
	"Auth":{"Dataname":"china_the_hut","user_id":5},
	"Action":{"getOrderByOrderID"},
	"Params":{"order_id":"1005-283"}
}


Basic Code flow (From above example):
The APILogs will be queried for the next apiRequest number, and this will be sent with any type of response.
The 'Component' (e.g. 'Orders') will be read from the request, and DataController with be a new 'OrdersDataController'.
The base class, DataControllerBase, receives the reflection of the instanciating class and determines all available actions.
The action 'getOrderByOrderID' is instantiated in the OrdersDataController method 'action_getOrderByOrderID' and the 'Params'
are sent to the Action method in the '$args' variable.

Actions:
The general things an action_... should do.
1.) Verify that all the required parameters have been passed, throw exception if not.
2.) Perform all needed data get queries via the components corresponding query class.
3.) Verify that all the needed data is gathered and in good state, throw an exception if not.
4.) Perform whatever marshalling is needed on the data.
5.) If data is in good shape, perform whatever inserts/updates that need to happend. (We save this for last to ensure a safe store),
    and return the insert_id, and rows affected within the response.
6.) If data is being returned we set the that data into the response's "ResponseData" field.


Then after the 'Action' the api.php page will read the response from the DataController, and return the final JSON response.
If an exception is ever thrown within the process, the catch will return the appropriate "Status":"Failed" response via the APIException.




Private API JSON-Responses:

Api requests fall into one of four categories: Get, Update/Insert, or 'Do-Something'.


Responses are top level classified as successful or failed based on the top level return: 'Status'


====  {'Status':"Failed"} ====
If status is 'Failed', then there will also be a 'reason' attached.
E.g. {"Status":"Failed","Reason":"Component was not specified.", "RequestID":42}


====  {"Status":"Success","RequestID":42,"DataResponse":DATA} ====


If status is 'Success', then their will be a 'DataResponse' field.
If it is a Get request DataResponse will be the field that will contain the requested data.
If it is an Update/Insert request, the DataResponse will contain information on how many updates occured and will look like:

{"Status":"Success","RequestID":42,"DataResponse":{"InsertID":42,"RowsUpdated":7}}

{"Status":"Success","RequestID":42,"DataResponse":{RowsUpdated":7}}

If it is a 'Do-Something' request, the DataResponse will depend on the action.















