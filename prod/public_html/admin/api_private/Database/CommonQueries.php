<?php

class CommonQueries
{//Admin only queries
	//Schema functions
	public static $recordLimit = 200;

	public static function allTablesInDatabase($database){
		//TODO
	}

    public static function allColumnsInTable($database, $table)
    {
        $result = DBConnection::adminQuery("SELECT * FROM `information_schema`.`columns` WHERE `TABLE_NAME` = 'sanitized[0]' AND `TABLE_SCHEMA`='sanitized[1]'", $database, $table);
        return MA::resultToArray($result);
    }

    public static function getSQLUser($sqlUser)
    {
        $result = DBConnection::adminQuery("SELECT * FROM `mysql`.`user` WHERE `User` = '[1]'", $sqlUser);
        return MA::resultFirstRow($result);
    }

	//CLIENT QUERIES
	//Get Functions
	public static function getRowByID($tableName, $id, $dataname = '', $columns = array())
	{
		// If name of columns array is empty then we will fetch all column record
		$selCoulumn = '*';
		if (!empty($columns) ) {
			$selCoulumn = implode(', ', $columns);
		}
		if ($dataname != "") {
			$databaseName =($dataname == "lavutogo")?
				$dataname : 'poslavu_'.$dataname.'_db';
			$result = DBConnection::transferLocationQuery("SELECT " . $selCoulumn . " FROM `[1]`.`[2]` WHERE `id`='[3]'", $databaseName, $tableName, $id);
		}
		else {
			$result = DBConnection::clientQuery("SELECT " . $selCoulumn . " FROM `[1]` WHERE `id`='[2]'", $tableName, $id);}
		return MA::resultFirstRow($result);
	}

    public static function getExternalLocationInventoryItemRowByTransferItemNumber($number)
    {
        $result = DBConnection::transferLocationQuery("SELECT * FROM `inventory_items` WHERE `id` = (SELECT `inventoryItemID` FROM `transfer_items` WHERE `id`='{$number}')");
        return MA::resultFirstRow($result);
    }

    public static function getLastInsertID($tableName)
    {
        $result = DBConnection::clientQuery("SELECT `id` FROM `[1]` ORDER BY id DESC LIMIT 1", $tableName);
        return MA::resultFirstRow($result);
    }

    public static function getRowsWhereColumn($tableName, $whereColumnArr, $orderBY = '', $otherClause = '', $dataname = '')
    {
        $query = MQ::buildSanitizedSelectAll($tableName, $whereColumnArr, $dataname);
        if (is_array($orderBY)) {
            $orderByCol = "";
            foreach ($orderBY as $key => $val) {
                $sanatizeKey = MQ::sanitizeValuesOfArray(array($key), '`');
                $orderByCol .= $sanatizeKey[0] . " " . $val;
            }
            $query .= ' ORDER BY ' . $orderByCol;
        }
        if (is_array($otherClause)) {
            $limitClause = "";
            foreach ($otherClause as $key => $val) {
                //$sanatizeKey = MQ::sanitizeValuesOfArray(array($key),'`');
                $limitClause .= $key . " " . $val;

            }
            $query .= ' ' . $limitClause;
        }
        $result = DBConnection::clientQuery($query);
        $returnResult = MA::resultToArray($result);
        return $returnResult;
    }

    public static function getRowsWhereColumnIn($tableName, $column, $idArr, $dataname = '', $columns = array())
    {
	    // If name of columns array is empty then we will fetch all column record
	    $selCoulumn = '*';
	    if (!empty($columns) ) {
		    $selCoulumn = implode(', ', $columns);
	    }
        $inClause = "WHERE `" . $column . "` IN " . MQ::buildSanitizedListInParenthesis($idArr, "'");
        if ($dataname) {
            $databaseName = "poslavu_" . $dataname . "_db";
            $result = DBConnection::transferLocationQuery("SELECT " . $selCoulumn . " FROM `[1]`.`[2]` " . $inClause, $databaseName, $tableName);
        } else {
            $result = DBConnection::clientQuery("SELECT " . $selCoulumn . " FROM `[1]` " . $inClause, $tableName);
        }
        return MA::resultToArray($result);
    }

    public static function getNonDeletedRowsWhereColumnIn($tableName, $column, $idArr, $columns = array())
    {
	    // If name of columns array is empty then we will fetch all column record
	    $selCoulumn = '*';
	    if (!empty($columns) ) {
	        $selCoulumn = implode(', ', $columns);
	    }
        $inClause = "WHERE `" . $column . "` IN " . MQ::buildSanitizedListInParenthesis($idArr, "'");
        $result = DBConnection::clientQuery("SELECT " . $selCoulumn . " FROM `[1]` " . $inClause . " AND `_deleted` = 0", $tableName);
        return MA::resultToArray($result);
    }

    public static function insertArray($tableName, $columnNames, $arrayOfRows, $usesPosDb = true)
    {
        $query = MarshallingQueries::buildSanitizedInsert($tableName, $columnNames, $arrayOfRows);
        $result = ($usesPosDb) ? DBConnection::clientQuery($query) : DBConnection::adminQuery($query);
        return $result;
    }

	public static function insertTransferArray($tableName, $columnNames, $arrayOfRows){
		//buildSanitizedInsert($table, $columns, $valueRows)
		$query = MarshallingQueries::buildSanitizedInsert($tableName, $columnNames, $arrayOfRows);
		$result = DBConnection::transferLocationQuery($query);
		return $result;
	}
	public static function updateArray($tableName, $column2Value, $endClause, $usesPosDb = true){
		$query = MarshallingQueries::buildSanitizedUpdate($tableName, $column2Value, $endClause);
		$result = ($usesPosDb) ? DBConnection::clientQuery($query) : DBConnection::adminQuery($query);
		return $result;
	}

	public static function updateTransferLocationArray($tableName, $column2Value, $endClause){
		$query = MarshallingQueries::buildSanitizedUpdate($tableName, $column2Value, $endClause);
		$result = DBConnection::transferLocationQuery($query);
		return $result;
	}

	// UPDATED by jaiprakesh
	//Last updated by Dhruvpal on 29-01-2019
	public static function getAllRows($tableName, $dataname = '', $columns = array(), $pid = array())
	{	
		// If name of columns array is empty then we will fetch all column record
		$selCoulumn = '*';
		if (!empty($columns) ) {
			$selCoulumn = implode(', ', $columns);
		}
		//check for page number
		$flg = 0;
		$limitStr = '';
		if(isset($pid['Items'][0]['id']) && $pid['Items'][0]['id'] != '') {
			$flg ++;
			$start_from = '';
			$pageNum = '';
			$searchStr = '';
			$limit = '';
			$page = '';
			$offset = '';
			$pageNum = $pid['Items'][0]['id'];
			$limit = isset($pid['Items'][1]['id']) && $pid['Items'][1]['id'] != '' ? $pid['Items'][1]['id'] : self::$recordLimit;
			$start_from = ($pageNum-1) * $limit;	
			$limitStr = " LIMIT ".$start_from. ", ".$limit;
		}
		
		if ($dataname ){
			$databaseName =($dataname == "lavutogo") ? $dataname : 'poslavu_'.$dataname.'_db';
				$result = DBConnection::clientQuery("SELECT " . $selCoulumn . " FROM ".$databaseName.".".$tableName.$limitStr);
			}
	        else {
	        	 $result = DBConnection::clientQuery("SELECT " . $selCoulumn . " FROM ".$tableName.$limitStr);
	        }
        	return MarshallingArrays::resultToArray($result);
    }

	/**
	* @param $tableName
	* @param array $columns name of only those columns which we required instead of whole column
	* @return array
	*/
	public static function getNonDeletedAllRows($tableName, $columns = array()){
		$chunk_size = 200;
		$done = 0;
		$keep_asking_for_data = true;
		$returnResult = array();
		// If name of columns array is empty then we will fetch all column record
		$selCoulumn = '*';
		if (!empty($columns) ) {
			$selCoulumn = implode(', ', $columns);
		}

		do {
			$result = DBConnection::clientQuery("SELECT  " . $selCoulumn . "  FROM `[1]` WHERE  `_deleted` = 0 LIMIT [2], [3]", $tableName, $done, $chunk_size);
			$num_rows = mysqli_num_rows($result);
			if ($num_rows) {
				$done += $num_rows;
				while($row = MarshallingArrays::resultToArray($result)){
					$returnResult = array_merge($returnResult, $row);
				}
			} else {
				$keep_asking_for_data = false;
			}
			mysqli_free_result($result);
		} while($keep_asking_for_data);
		return $returnResult;
	}

	public static function addOrSubtractFromColumn($tableName, $columnName, $amount, $factor, $whereClause)
		{if(empty($whereClause)){
			throw new APIException("Missing where clause in addSubtractFromColumn.");
		}
		// 1 = incrementing, -1 = decrementing - but could be used as a scale factor for amount
		$amount = $factor * $amount;$result = DBConnection::clientQuery("UPDATE `[1]` SET `[2]`=round(IFNULL(`[2]` , 0) + [3],3) " . $whereClause,$tableName,$columnName,$amount);
		$rowsUpdated = DBConnection::clientAffectedRows();
		return array("RowsUpdated" => $rowsUpdated);
	}

	public static function getRowsWithRequiredColumns($tableName, $columnNameArr, $whereColumnArr, $orderBY='', $otherClause='', $dataname = '', $fieldExpression=array())
                {//echo "<pre>";
                //print_r($otherClause);
		$data_name=$dataname;
		$query = MQ::buildSanitizedSelect($tableName, $columnNameArr, $whereColumnArr, $data_name, $fieldExpression);
		$limitClause = '';
		if(is_array($orderBY) ) {
			$orderByCol = "";
			foreach($orderBY as $key=>$val) {
				$sanatizeKey = MQ::sanitizeValuesOfArray(array($key),'`');
				$orderByCol .= $sanatizeKey[0]." ".$val;
			}
			$query .= ' ORDER BY ' . $orderByCol;
		}
		if(is_array($otherClause) ) {
			$limitClaluse="";
			foreach($otherClause as $key=>$val) {
				//$sanatizeKey = MQ::sanitizeValuesOfArray(array($key),'`');
				$limitClause .= $key." ".$val;

			}
			$query .= ' '.$limitClause;
		}

	        //echo $query."<br>";
		if ($dataname != '') {
			$result = DBConnection::transferLocationQuery($query);
		} else {
			$result = DBConnection::clientQuery($query);
		}
		$returnResult =  MA::resultToArray($result);
		return $returnResult;
	}

    public static function getRowsWithRequiredColumnsMainDB($tableName, $columnNameArr, $whereColumnArr, $orderBY = '', $otherClause = '')
    {
        $query = MQ::buildSanitizedSelect($tableName, $columnNameArr, $whereColumnArr, 'MAIN');
        $limitClause = '';
        if (is_array($orderBY)) {
            $orderByCol = "";
            foreach ($orderBY as $key => $val) {
                $sanatizeKey = MQ::sanitizeValuesOfArray(array($key), '`');
                $orderByCol .= $sanatizeKey[0] . " " . $val;
            }
            $query .= ' ORDER BY ' . $orderByCol;
        }
        if (is_array($otherClause)) {
            $limitClaluse = "";
            foreach ($otherClause as $key => $val) {
                //$sanatizeKey = MQ::sanitizeValuesOfArray(array($key),'`');
                $limitClause .= $key . " " . $val;

			}
			$query .= ' '.$limitClause;
		}
	        //echo $query."<br>";
		$result = DBConnection::adminQuery($query);
		$returnResult =  MA::resultToArray($result);
		return $returnResult;
	}

    public static function getRowsWithRequiredColumnsOtherDB($tableName, $columnNameArr, $whereColumnArr, $orderBY = '', $otherClause = '', $database = '')
    {
        $query = MQ::buildSanitizedSelect($tableName, $columnNameArr, $whereColumnArr, $database);
        $limitClause = '';
        if (is_array($orderBY)) {
            $orderByCol = "";
            foreach ($orderBY as $key => $val) {
                $sanatizeKey = MQ::sanitizeValuesOfArray(array($key), '`');
                $orderByCol .= $sanatizeKey[0] . " " . $val;
            }
            $query .= ' ORDER BY ' . $orderByCol;
        }
        if (is_array($otherClause)) {
            $limitClaluse = "";
            foreach ($otherClause as $key => $val) {
                //$sanatizeKey = MQ::sanitizeValuesOfArray(array($key),'`');
                $limitClause .= $key . " " . $val;

			}
			$query .= ' '.$limitClause;
		}
	        //echo $query."<br>";
		$result = DBConnection::adminQuery($query);
		$returnResult =  MA::resultToArray($result);
		return $returnResult;
	}


    public static function getRowCountForPagination ($tableName,$whereClauseArr){
        $whereClause = MQ::buildSanitizedWhere($whereClauseArr );
        $result = DBConnection::clientQuery("Select COUNT(*) FROM `[1]` WHERE [2]", $tableName, $whereClause);
         return $result;
    }

    public static function getRowValueWhere($tableName,$whereClauseArr,$operatorArr)
        {$opretorArr = array("like");
        $whereClause = MQ::buildSanitizedWhere($whereClauseArr ,$operatorArr);
        $result = DBConnection::clientQuery("Select COUNT(*) FROM `[1]` WHERE [2]", $tableName, $whereClause);
        return $result;}

    public static function getWhereColumnEquals($tableName, $column, $equalVal)
    {
        $query = "SELECT * FROM `[1]` WHERE `[2]`=[3]";
        $result = DBConnection::clientQuery($query, $tableName, $column, $equalVal);
        return MarshallingArrays::resultToArray($result);
    }

    public static function getNonDeletedWhereColumnEquals($tableName, $column, $equalVal)
    {
        $query = "SELECT * FROM `[1]` WHERE `[2]`=[3] AND `_deleted` = 0";
        $result = DBConnection::clientQuery($query, $tableName, $column, $equalVal);
        return MarshallingArrays::resultToArray($result);
    }

    // If more column in where clause
    public static function getWhereColumnsEquals($tableName, $columns, $equalVals)
    {
        $query = MarshallingQueries::buildSanitizedWhereCondition($tableName, $columns, $equalVals);
        $result = DBConnection::clientQuery($query, $tableName);
        return MarshallingArrays::resultToArray($result);
    }

    public static function getWhereColumnEqualsSum($tableName, $column, $equalVal, $sumColumn, $aliases = '')
    {
        $aliases = ($aliases != "") ? " AS " . $aliases : '';
        $query = "SELECT SUM($sumColumn)" . $aliases . " FROM `[1]` WHERE `[2]`=[3]";
        $result = DBConnection::clientQuery($query, $tableName, $column, $equalVal);
        return MarshallingArrays::resultToArray($result);
    }

    public static function getWhereColumnEqualsAnyValueOfFieldInValuesArray($table, $column, $values, $field)
    {
        if (is_array($values)) {
            $orValues = $values[0][$field];
            for ($i = 1; $i < count($values); $i++) {
                $orValues .= " OR {$column}={$values[$i][$field]}";
            }
            $query_string = "SELECT * FROM `{$table}` WHERE `{$column}` = " . $orValues;
            $result = DBConnection::clientQuery($query_string);
            return MarshallingArrays::resultToArray($result);
        } else {
            return "Failure to perform query. Parameter 'values' needs to be an array. values:" . print_r($values, 1);
        }
    }

    public static function getSumWithWhereColumnIn($tableName, $column, $idArray, $sumColumn, $aliases = '')
    {
        $response = array();
        if (!empty($idArray)) {
            $inList = implode(",", $idArray);
            $aliases = ($aliases != "") ? " AS " . $aliases : '';
            $query = "SELECT [2], SUM($sumColumn)" . $aliases . " FROM `[1]` WHERE `[2]` IN ([3]) GROUP BY [2]";
            $result = DBConnection::clientQuery($query, $tableName, $column, $inList);
            $response = MarshallingArrays::resultToArray($result);
        }
        return $response;
    }

    public static function _deletedFlagIs1($table, $id)
    {
        $query = "SELECT `_deleted` FROM `{$table}` WHERE `id` = {$id}";
        $deleted = DBConnection::clientQuery($query);
        $arr = MarshallingArrays::resultToArray($deleted);
        if ($arr[0]['_deleted'] == 1) {
            return true;
        }
        return false;
    }

	//Update functions


    //ReplaceInto functions


	//Update functions

	//Copy functions
	public static function copyRowInTable($tableName, $id, $appendFields = array()){
		$rowQuery = "SELECT * FROM `$tableName` WHERE `id`=[1]";
		$result = mysqli_fetch_assoc(DBConnection::clientQuery($rowQuery,$id));
		$keys = array();
		$row = array();
		foreach($result as $key=>$val){
			if($key !== 'id'){
				$keys[] = $key;
				if (isset($appendFields[$key])) $val .= $appendFields[$key];
				$row[0][$key] = $val;
			}
		}
		$result = self::insertArray($tableName, $keys, $row);
		return $result;
	}

	// Delete function is used to hard delete from table
	public static function deleteRowInTable($tableName, $column = '', $idArr = array()){
		$inClause = ($column != '') ? "WHERE `".$column."` IN ".MQ::buildSanitizedListInParenthesis($idArr, "'") : '';
		$result = DBConnection::clientQuery("DELETE FROM `[1]` ".$inClause, $tableName);
		return $result;
	}

	/**This function makes sure the transfer numbers are always unique to the origin location.*/
	public static function copyTransferRowInTable($tableName, $id, $restaurantID=null, $newTransferID=null){
		$rowQuery = "SELECT * FROM `$tableName` WHERE `id`=[1]";
		$result = mysqli_fetch_assoc(DBConnection::clientQuery($rowQuery,$id));
		$keys = array();
		$row = array();
		$field = null;
		foreach($result as $key=>$val){
			if($key !== 'id'){
				$keys[] = $key;
				//this is used after the transfer has been copied, so that the new transfer items get set to the new
				//transfer id.
				if (isset($newTransferID) && $key === 'transferID'){
					$row[0][$key] = $newTransferID;
				}else{
					$row[0][$key] = $val;
				}
			}
			if($key === 'transferNumber'){
				$field = 'transferNumber';
			}else if ($key === 'transferItemNumber'){
				$field = 'transferItemNumber';
			}
		}
		self::insertArray($tableName, $keys, $row);
		$lastID = null;
		$updateArray = null;
		if($field === 'transferNumber'){
			$lastID = CQ::getLastInsertID('inventory_transfer')['id'];
			$updateArray = array('transferNumber'=>$restaurantID."-".$lastID);
			CQ::updateArray('inventory_transfer',$updateArray,'WHERE `id`='.$lastID);
		}
		if($field === 'transferItemNumber'){
			$lastID = CQ::getLastInsertID('transfer_items')['id'];
			$updateArray = array('transferItemNumber'=>$restaurantID."-".$lastID);
			CQ::updateArray('transfer_items',$updateArray,'WHERE `id`='.$lastID);
		}

		return $lastID;
	}

	public static function copyRowToExternalTransferLocationTable($tableName, $recordId) {
		$rowQuery = "SELECT * FROM `$tableName` WHERE `id`=[1]";
		$result = mysqli_fetch_assoc(DBConnection::clientQuery($rowQuery,$recordId));
		$keys = array();
		$row = array();
		foreach($result as $key=>$val){
			if($key !== 'id'){
				$keys[] = $key;
				$row[0][$key] = $val;
			}
		}
		$result = self::insertTransferArray($tableName, $keys, $row);
		return $result;
	}

	/*
	* ------------------------------------------------------------------
	* Returns the list of users address on userId form lavutogo database
	* ------------------------------------------------------------------
	*
	* Looks for user stored delivery addresses from `users_address` table for
	* lavutogo users
	*
	* @param integer $userId  users id from lavu main database
	* @param array $lookForColumns  get data of selective columns
	* @since Method is made available in OLO 2.0.1
	* @return array  list of user address used for delivery
	*/
	public static function getListOfUserAddress($userId, $lookForColumns = []) {
		$userAddresTable = 'users_address';
		$where = [ 'user_id' => $userId, '_deleted' => '0' ];
		if(sizeof($lookForColumns) < 1) {
			$lookForColumns = [
				'id',
				'user_id',
				'name',
				'address_1',
				'address_2',
				'country',
				'state',
				'city',
				'zipcode',
				'is_primary'
			];
		}

		return self::getRowsWithRequiredColumnsOtherDB(
			$userAddresTable,
			$lookForColumns,
			$where,
			['is_primary' => ' DESC'],
			'',
			'lavutogo'
		);
	}

	/*
	* ------------------------------------------------------------------
	* Returns boolean true/false if the provided user is lavutogo user
	* ------------------------------------------------------------------
	*
	* Searches for user id in lavutogo database from `users` table
	*
	* @param integer $userId  users id from lavu main database
	* @since Method is made available in OLO 2.0.1
	* @return boolean
	*
	* ------------------------------------------------------------------
	* @todo
	* ------------------------------------------------------------------
	* This function as potential to scan user with user name or email or
	* any of the column. Also right now it does a normal select with id
	* for slection could have better quality in next enhancement version
	*/
	public static function isLavuToGoUser($userId) {
		$tableUsers = 'users';
		$where = ['id' => $userId];
		$lookForColumns = ['id'];
		$usersData = self::getRowsWithRequiredColumnsOtherDB(
			$tableUsers,
			$lookForColumns,
			$where,
			'',
			'',
			'lavutogo'
		);
		return sizeof( $usersData ) >= 1;
	}

	/*
	* ------------------------------------------------------------------
	* Insert user address information
	* ------------------------------------------------------------------
	*
	* @param array $data  list of column data to be saved
	* @since Method is made available in OLO 2.0.1
	* @return array
	*/
	public static function insertLavuToGoUserAddress($data) {
		$userAddresTable = 'lavutogo.users_address';
		$currentDate = date('Y-m-d H:i:s');
		$data['created_date'] = $currentDate;
		$data['last_modified_date'] = $currentDate;
		$columnNames = array_keys($data);
		$insertValues[] = array_values($data);
		$data['is_primary'] = sizeof(self::getListOfUserAddress($data['user_id'])) >= 1 ? $data['is_primary'] : 1;
		if((int) $data['is_primary'] && !empty($data['user_id'])) {
			self::unsetAllPrimaryAddress($data['user_id']);
		}
		$insertedRecord = self::insertArray(
			$userAddresTable,
			$columnNames,
			$insertValues,
			false
		);
		return MA::resultToInsertIDUpdatedRows( $insertedRecord, true );
	}

	public static function unsetAllPrimaryAddress($userId) {
		$userAddresTable = 'lavutogo.users_address';
		$currentDate = date('Y-m-d H:i:s');
		return self::updateArray($userAddresTable, [
			'is_primary' =>0,
			'last_modified_date'=>$currentDate
		],
			"where user_id='".$userId."' AND is_primary='1'", false
		);
	}
	
	/*
	* ------------------------------------------------------------------
	* Returns boolean true/false if restuarant is enabled lavuTogo
	* ------------------------------------------------------------------
	*
	* Searches for setting = use_lavu_togo from location database from `config` table
	* Uses client query to connect with location database
	* @param $version int which version to check for LTGV1 or LTGV2
	* @return boolean
	*
	* ------------------------------------------------------------------
	* @todo
	* ------------------------------------------------------------------
	* This function is commonly working of LTG
	*/
	public static function isLavuToGoEnabled($version = 2) {
		$setting = (int)$version === 1 ? 'use_lavu_togo' : 'use_lavu_togo_2';
		$whereClause = ['setting'=> $setting, '_deleted'=> 0];
		$selectColumns = ['value'];
		$data = self::getRowsWithRequiredColumns('config', $selectColumns, $whereClause);
		return isset($data[0]['value']) && (int)$data[0]['value'] === 1 ? true : false;
	}

	public static function updateUserMealPlanId($args) {
		$mealPlanUserId = "";
		$queryObject = clavuQuery("SELECT `id` FROM `MealPlanUser` WHERE `email` = '[1]' AND  `_deleted` ='0'", $args['email']);

   		if (mysqli_num_rows($queryObject) > 0) {
        	$mealPlanUserRow = mysqli_fetch_assoc($queryObject);
        	$tableName = 'lavutogo.users_info';
        	$set = [
        		'mealplan_user_id' => $mealPlanUserRow['id']
			];
			if(isset($args['updateRecordById'])) {
				$where = "where id='".$args['updateRecordById']."'";
			} else {
				$where = "where email='".$args['email']."'";
			}
        	self::updateArray($tableName, $set, $where, false);
    	    $mealPlanUserId = isset($mealPlanUserRow['id']) ? $mealPlanUserRow['id'] : "";
    	}

    	return $mealPlanUserId;
	}

	/*
	* ------------------------------------------------------------------
	* Rounding adjustment according to Lavu POS Calculation
	* ------------------------------------------------------------------
	*
	* Reads advance location setting on locationId for rounding configuration
	* @param float $amount
	* @param string $dataname
	* @param integer $locationId
	* @return array
	*
	* ------------------------------------------------------------------
	* @todo
	* ------------------------------------------------------------------
	* Resolve reported bug if found any or future implementation changes
	*/
	public static function roundingAdjustment($amount, $dataname, $locationId) {
		$roundingDetails = [
			"amount" => $amount,
			"roundingAmount" => 0,
		];
		$settings = self::getRowsWithRequiredColumns(
			'locations',
			[
				"rounding_factor` as `roundingFactor", 
				"round_up_or_down` as `roudUpDown",
				"disable_decimal` as `decimalPlaces"
			], 
			[
				'id' => $locationId
			], '', '', $dataname
		);

		$settings = isset($settings[0]) ? $settings[0]: [];
		$typeUpwards = 'upwards';
		$typeDownwards = 'downwards';

		if(
			isset($settings['roundingFactor']) && !empty($settings['roundingFactor'])
		) {
			$remainder = (float) round(fmod($amount, $settings['roundingFactor']), $settings['decimalPlaces']);
			$roundNumber = (float) round(abs($remainder - $settings['roundingFactor']), $settings['decimalPlaces']);
			if($remainder > 0 && $roundNumber > 0) {
				switch (strtolower($settings['roudUpDown'])) {
					case 'up':
						$updateType = $typeUpwards;
					break;

					case 'down':
						$updateType = $typeDownwards ;
					break;

					default:
						if($remainder > ($roundNumber/2)) {
							$updateType = $typeUpwards;
						} else {
							$updateType = $typeDownwards ;
						}
					break;
				}

				switch ($updateType) {
					case $typeUpwards: 
						$roundingDetails = [
							"amount" => $amount + $roundNumber,
							"roundingAmount" => $roundNumber + $previouslyAdjusted,
						];
					break;
					case $typeDownwards:
						$roundingDetails = [
							"amount" => $amount - $remainder,
							"roundingAmount" => "-".abs($previouslyAdjusted - $remainder),
						];
					break;
					default:
						// do nothing
					break;
				}
			}
		}
		return $roundingDetails;
	}

	/**
	 * Get records from poslavu_MAIN_db.main_config table this config is applicable for application level
	 * input:- $setting string like ALIVE_REDIS_SERVER
	 * output:- array
	 */
	public static function getMainConfig($setting, $deleted = 0)
	{
		$result = DBConnection::adminQuery("SELECT value FROM `poslavu_MAIN_db`.`main_config` WHERE `setting` = '[1]' AND `_deleted` = '" . $deleted . "'", $setting);
		return MA::resultFirstRow($result);
	}

	/**
	 * Get all alive redis server. Value will be updated by cron script private_html/scripts/redis_health_check.php
	 * this script will run in every 30 seconds
	 * input:-
	 * output:- array
	 */
	public static function getAliveRedisServer()
	{
		return self::getMainConfig('ALIVE_REDIS_SERVER');
	}
	/**
	 * Get Inventory Migration Status
	 * @return array
	 */
	public static function getInventoryMigrationStatus()
	{
		$columnArr = array('value');
		$whereColumnArr = array('setting' => 'INVENTORY_MIGRATION_STATUS', '_deleted' => 0);
		$inventoryMigrationStatus = self::getRowsWithRequiredColumns('config', $columnArr, $whereColumnArr, '');
		return $inventoryMigrationStatus;
	}
}

class CQ extends CommonQueries{}
