<?php

require_once('/home/poslavu/private_html/myinfo.php');

class DBConnection{

	private static $connections;
	private static $lastQuery;

	public static function startClientConnection($dataname){
		$dataname = strtolower($dataname);
		$mainRestTableName = isset($dataname[0]) && ord($dataname[0]) >= ord('a') &&  ord($dataname[0]) <= ord('z') ? 'rest_'.$dataname[0] : 'rest_OTHER';
		$clientResult = self::adminQuery("SELECT concat('usr_pl_', `companyid`) as user, AES_DECRYPT(`data_access`,'lavupass') as password FROM `poslavu_MAINREST_db`.`[1]` WHERE `data_name`='[2]'",$mainRestTableName, $dataname);
		if (!$clientResult) {
			/* TODO LOG ERROR */
			return false;
		}
		if (mysqli_num_rows($clientResult) == 0) { /* TODO LOG CLIENT NOT FOUND */
			error_log(__FILE__ . " " . __LINE__ . " Client Rows returned 0");
		}
		$clientRow = mysqli_fetch_assoc($clientResult);
		$clientConnection = mysqli_connect(my_poslavu_host(), $clientRow['user'], $clientRow['password']);
		//$clientConnection = mysqli_connect(my_poslavu_host(), my_poslavu_username(), my_poslavu_password()); /*don't commit*/
		if(empty($clientConnection)){
			throw new APIException("Could not connect to client database.");
		}
		if(!empty(self::$connections['client']) && self::$connections['client'] !== $clientConnection){
			mysqli_close(self::$connections['client']);
		}
		self::$connections['client'] = $clientConnection;
		$databaseName = in_array($dataname, array('lavutogo')) ? $dataname : 'poslavu_'.$dataname.'_db';  // SOMEDAY : replace hard-coded check for non-poslavu_<dataname>_db databases
		$selectDBSuccess = mysqli_select_db($clientConnection, $databaseName);
		return self::$connections['client'];
	}

	public static function startTransferConnection($restaurant_id){
		$result = DBConnection::adminQuery("SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = [1]",$restaurant_id);
		$resArr = MarshallingArrays::resultToArray($result);
		$dataname = $resArr[0]['data_name'];
		$mainRestTableName = ord($dataname[0]) >= ord('a') &&  ord($dataname[0]) <= ord('z') ? 'rest_'.$dataname[0] : 'rest_OTHER';
		$transferLocationResult = self::adminQuery("SELECT concat('usr_pl_', `companyid`) as user, AES_DECRYPT(`data_access`,'lavupass') as password FROM `poslavu_MAINREST_db`.`[1]` WHERE `data_name`='[2]'",$mainRestTableName, $dataname);
		if(!$transferLocationResult){ /* TODO LOG ERROR */ return false; }
		if(mysqli_num_rows($transferLocationResult) == 0){ /* TODO LOG CLIENT NOT FOUND */
			error_log(__FILE__ . " " . __LINE__ . " Transfer Location Rows returned 0");
		}
		$transferLocationRow = mysqli_fetch_assoc($transferLocationResult);
		$transferLocationConnection = mysqli_connect(my_poslavu_host(), $transferLocationRow['user'], $transferLocationRow['password']);
		if(empty($transferLocationConnection)){
			throw new APIException("Could not connect to client database.");
		}
		if(!empty(self::$connections['transferLocation']) && self::$connections['transferLocation'] !== $transferLocationConnection){
			mysqli_close(self::$connections['transferLocation']);
		}
		self::$connections['transferLocation'] = $transferLocationConnection;
		$selectDBSuccess = mysqli_select_db($transferLocationConnection, 'poslavu_'.$dataname.'_db');
		return self::$connections['transferLocation'];
	}

	public static function closeTransferConnection(){
		mysqli_close(self::$connections['transferLocation']);
	}

	public static function startAPITestingConnection(){
		if(empty(self::$connections)){
			self::$connections = array();
		}
		$apiIntegrationTestConnection = mysqli_connect(my_poslavu_host(), 'API_TESTS', 'EVERYTHING_BE_TESTED');
		self::$connections['client'] = $apiIntegrationTestConnection;
		$selectDBSuccess = mysqli_select_db(self::$connections['client'], 'poslavu_API_INTEGRATION_TESTS_db');
		return self::$connections['client'];
	}

	public static function getConnectionByType($connectionType){

		if(empty(self::$connections))
		{
			self::$connections = array();
		}
		if(!empty(self::$connections[$connectionType]))
		{
			return self::$connections[$connectionType];
		}

			$usr =  my_poslavu_username();
			$pass = my_poslavu_password();
			$host = my_poslavu_host();
			$connection = mysqli_connect($host, $usr, $pass);
		if($connectionType === 'admin'){
			if($connection) {self::$connections['admin'] = $connection; }
			else{ /*LOG ERROR */}
			return $connection;
		}else if($connectionType === 'client'){
			if( empty(self::$connections['client'] )){
				//TODO LOG ERROR.
				return false;
			}
			return self::$connections['client'];
		}else if($connectionType === 'replication'){
			$usr  = my_poslavu_username();
			$pass = my_poslavu_password();
			$host = my_poslavu_replication_host();
			if($connection) {self::$connections['replication'] = $connection; }
			else{ /*LOG ERROR */}
			return $connection;
		}else if($connectionType === 'local_replication'){
			$usr  = my_poslavu_local_replication_username();
			$pass = my_poslavu_local_replication_password();
			$host = my_poslavu_local_replication_host();
			if($connection) {self::$connections['replication'] = $connection; }
			else{ /*LOG ERROR */}
			return $connection;
		}else if($connectionType === 'transferLocation'){
			if($connection) {self::$connections['transferLocation'] = $connection; }
			else{ /*LOG ERROR */}
			return $connection;
		}
	}

	public static function transferLocationQuery($query){
		$args = func_get_args();
		$query = $args[0];
		unset($args[0]);
		$transferLocationConnection = self::getConnectionByType('transferLocation');
		return self::query($transferLocationConnection, $query, $args);
	}

	public static function clientQuery($query){
		$args = func_get_args();
		$query = $args[0];
		unset($args[0]);
		$clientConnection = self::getConnectionByType('client');
		return self::query($clientConnection, $query, $args);
	}

	public static function adminQuery($query){
		$args = func_get_args();
		$query = $args[0];
		unset($args[0]);
		$mainConnection = self::getConnectionByType('admin');
		return self::query($mainConnection, $query, $args);
	}

	private static function query($conn, $query, $args){
		$startMicrosecs = microtime();
		$queryArgs = $args;
		if( isset($queryArgs[1]) && is_array($queryArgs[1])){
			$queryArgs = $queryArgs[1];
		}
		foreach ($queryArgs as $key => $value) {
			$query = str_replace('['.$key.']', mysqli_real_escape_string($conn, $value), $query);
		}
		$result = mysqli_query($conn, $query);
		if(!$result){
			error_log("Query has error....");
			error_log("Query:".$query);
			error_log("QUERY ERROR:".mysqli_error($conn));
		}
		$totalTime = microtime() - $startMicrosecs;
		return $result;
	}

	public static function clientDBError(){
		$clientConn = self::getConnectionByType('client');
		return mysqli_error($clientConn);
	}

	public static function transferLocationDBError(){
		$clientConn = self::getConnectionByType('transferLocation');
		return mysqli_error($clientConn);
	}


	public static function adminDBError(){
		$adminConn = self::getConnectionByType('admin');
		return mysqli_error($adminConn);
	}

	public static function adminInsertID(){
		return mysqli_insert_id(self::getConnectionByType('admin'));
	}

	public static function clientInsertID(){
		return mysqli_insert_id(self::getConnectionByType('client'));
	}

	public static function transferLocationInsertID(){
		return mysqli_insert_id(self::getConnectionByType('transferLocation'));
	}

	public static function adminAffectedRows(){
		return mysqli_affected_rows(self::getConnectionByType('admin'));
	}

	public static function clientAffectedRows(){
		return mysqli_affected_rows(self::getConnectionByType('client'));
	}

	public static function transferLocationAffectedRows(){
		return mysqli_affected_rows(self::getConnectionByType('transferLocation'));
	}

	public static function sanitize($arg){
		$mainConnection = DBConnection::getConnectionByType('admin');
		if(is_array($arg)){
			$returnArr = array();
			foreach($arg as $currKey => $currVal){
				$returnArr[$currKey] = mysqli_escape_string($mainConnection, $currVal);
			}
			return $returnArr;
		}else{
			return mysqli_real_escape_string($mainConnection, $arg);
		}
	}

	public static function redisQuery($query){
		$args = func_get_args();
		$query = $args[0];
		unset($args[0]);
		$clientConnection = self::getConnectionByType('redis');
		return self::query($clientConnection, $query, $args);
	}
}

class DBC extends DBConnection{}

