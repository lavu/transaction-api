<?php

class ComplexQueries{




	public static function CopyJoinedRowsByIDs($parentTable, $childTable, $childIDField, $idsArr){
		$toCopyParentRows = CQ::getRowsWhereColumnIn($parentTable, 'id', $idsArr);
		$toCopyChildRows = CQ::getRowsWhereColumnIn($childTable, $childIDField, $idsArr);

		//TODO verify that the count from the select matches the number in the returned array.
		$original2CopyIDMap = array();
		$parentRows2Insert = array();
		$i = 0;
		foreach($toCopyParentRows as $curr2CopyParentRow){
			$origID = $curr2CopyParentRow['id'];
			$original2CopyIDMap[$origID] = $i++; //Used for finding the copied row.
			unset($curr2CopyParentRow['id']);
			$parentRows2Insert[] = $curr2CopyParentRow;
		}
		$columnNames = array_keys($parentRows2Insert[0]);
		$parentInsertResult = CQ::insertArray($parentTable, $columnNames, $parentRows2Insert);
		//TODO verify result.
		$parentInsertIDUpdateRows = MA::resultToInsertIDUpdatedRows($parentInsertResult);
		$insertedParentIDs = $parentInsertIDUpdateRows;
		$insertID = $parentInsertIDUpdateRows['InsertID'];
		$i = 0;
		$finalOriginal2CopyIDMap = array();
		foreach($original2CopyIDMap as $previousRowID => $copiedIndex){
			$finalOriginal2CopyIDMap[$previousRowID] = $insertID + $original2CopyIDMap[$previousRowID];
		}
		$childRows2Insert = array();
		foreach($toCopyChildRows as $currToCopyChildRow){
			$originalParentID = $currToCopyChildRow[$childIDField];
			$currToCopyChildRow[$childIDField] = $finalOriginal2CopyIDMap[$previousRowID];
			unset($currToCopyChildRow['id']);
			$childRows2Insert[] = $currToCopyChildRow;
		}
		$columnNames = array_keys($childRows2Insert[0]);
		$result = CQ::insertArray($childTable, $columnNames, $childRows2Insert);

		$insertedChildIDs = MA::resultToInsertIDUpdatedRows($parentInsertResult);

		$results = array("parentIDs"=>$insertedParentIDs, "childIDs"=>$insertedChildIDs);

		return $results;
	}

	public static function selectRowsFromTableJoins($parentTable, $childTable, $childIDField, $idsArr){
		$toCopyParentRows = CQ::getRowsWhereColumnIn($parentTable, 'id', $idsArr);
		$toCopyChildRows = CQ::getRowsWhereColumnIn($childTable, $childIDField, $idsArr);

		//TODO verify that the count from the select matches the number in the returned array.
		$original2CopyIDMap = array();
		$parentRows2Insert = array();
		$i = 0;
		foreach($toCopyParentRows as $curr2CopyParentRow){
			$origID = $curr2CopyParentRow['id'];
			$original2CopyIDMap[$origID] = $i++; //Used for finding the copied row.
			unset($curr2CopyParentRow['id']);
			$parentRows2Insert[] = $curr2CopyParentRow;
		}
		$columnNames = array_keys($parentRows2Insert[0]);
		$parentInsertResult = CQ::insertArray($parentTable, $columnNames, $parentRows2Insert);
		//TODO verify result.
		$parentInsertIDUpdateRows = MA::resultToInsertIDUpdatedRows($parentInsertResult);
		$insertID = $parentInsertIDUpdateRows['InsertID'];
		$i = 0;
		$finalOriginal2CopyIDMap = array();
		foreach($original2CopyIDMap as $previousRowID => $copiedIndex){
			$finalOriginal2CopyIDMap[$previousRowID] = $insertID + $original2CopyIDMap[$previousRowID];
		}
		$childRows2Insert = array();
		foreach($toCopyChildRows as $currToCopyChildRow){
			$originalParentID = $currToCopyChildRow[$childIDField];
			$currToCopyChildRow[$childIDField] = $finalOriginal2CopyIDMap[$previousRowID];
			unset($currToCopyChildRow['id']);
			$childRows2Insert[] = $currToCopyChildRow;
		}
		$columnNames = array_keys($childRows2Insert[0]);
		$result = CQ::insertArray($childTable, $columnNames, $childRows2Insert);
		$childInsertIDUpdateRows = MA::resultToInsertIDUpdatedRows($parentInsertResult);
	}


	public static function selectJoinedRowsByIDs($parentTable, $childTableArr, $joinTableArr, $idsArr){
		$parentTable = array_key();
		//TODO verify that the count from the select matches the number in the returned array.
		$original2CopyIDMap = array();
		$parentRows2Insert = array();
		$i = 0;
		foreach($toCopyParentRows as $curr2CopyParentRow){
			$origID = $curr2CopyParentRow['id'];
			$original2CopyIDMap[$origID] = $i++; //Used for finding the copied row.
			unset($curr2CopyParentRow['id']);
			$parentRows2Insert[] = $curr2CopyParentRow;
		}
		$columnNames = array_keys($parentRows2Insert[0]);
		$parentInsertResult = CQ::insertArray($parentTable, $columnNames, $parentRows2Insert);
		//TODO verify result.
		$parentInsertIDUpdateRows = MA::resultToInsertIDUpdatedRows($parentInsertResult);
		$insertID = $parentInsertIDUpdateRows['InsertID'];
		$i = 0;
		$finalOriginal2CopyIDMap = array();
		foreach($original2CopyIDMap as $previousRowID => $copiedIndex){
			$finalOriginal2CopyIDMap[$previousRowID] = $insertID + $original2CopyIDMap[$previousRowID];
		}
		$childRows2Insert = array();
		foreach($toCopyChildRows as $currToCopyChildRow){
			$originalParentID = $currToCopyChildRow[$childIDField];
			$currToCopyChildRow[$childIDField] = $finalOriginal2CopyIDMap[$previousRowID];
			unset($currToCopyChildRow['id']);
			$childRows2Insert[] = $currToCopyChildRow;
		}
		$columnNames = array_keys($childRows2Insert[0]);
		$result = CQ::insertArray($childTable, $columnNames, $childRows2Insert);
		$childInsertIDUpdateRows = MA::resultToInsertIDUpdatedRows($parentInsertResult);
	}


	// column2ValueReplacementsByID: [ id1:{"column1":"value2"}, id2:{"column2":"value2"}, id3:{}] etc.
	public static function CopyWithValueReplacements($table, $column2ValueReplacementsByID){
		$insertIDs = array();

		$ids = array_keys($column2ValueReplacementsByID);
		$toCopyRows = CQ::getRowsWhereColumnIn($table, 'id', $ids);
		$toInsertRowsByID = MA::numericArrayToAssociativeByKey($toCopyRows, 'id');
		$toInsertRowsByIDRemovedID = array_map( function($currArr){ unset($currArr['id']); $currArr['name'] .= " - Copy"; return $currArr;}, $toInsertRowsByID);

		$toInsertRowsByIDRemovedIDReplacedColumns = array();
		foreach($toInsertRowsByIDRemovedID as $key => $currRow){
			$toInsertRowsByIDRemovedIDReplacedColumns[$key] = MA::overwriteArray($toInsertRowsByIDRemovedID[$key], $column2ValueReplacementsByID[$key]);
		}

		foreach($toInsertRowsByIDRemovedIDReplacedColumns as $idKey => $insertAssoc){
			$result = CQ::insertArray('inventory_items', array_keys($insertAssoc), array(array_values($insertAssoc)) );
			$insertIDs[] = DBConnection::clientInsertID();
		}
		return $insertIDs;
	}
}