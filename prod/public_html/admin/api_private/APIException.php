<?php

/*
class Exception
{
    protected $message = 'Unknown exception';   // exception message
    private   $string;                          // __toString cache
    protected $code = 0;                        // user defined exception code
    protected $file;                            // source filename of exception
    protected $line;                            // source line of exception
    private   $trace;                           // backtrace
    private   $previous;                        // previous exception if nested exception

    public function __construct($message = null, $code = 0, Exception $previous = null);

    final private function __clone();           // Inhibits cloning of exceptions.

    final public  function getMessage();        // message of exception
    final public  function getCode();           // code of exception
    final public  function getFile();           // source filename
    final public  function getLine();           // source line
    final public  function getTrace();          // an array of the backtrace()
    final public  function getPrevious();       // previous exception
    final public  function getTraceAsString();  // formatted string of trace

    // Overrideable
    public function __toString();               // formatted string for display
}
*/

class APIException extends Exception{

	public function __construct($failReason){
		parent::__construct($failReason);
	}

	public function jsonFailResponse($apiRequestNumber){
		$failResponse = array();
		$failResponse['Status'] = 'Failed';
		$failResponse['Reason'] = $this->message;
		$failResponse['RequestID'] = $apiRequestNumber;
		return json_encode($failResponse);
	}

}