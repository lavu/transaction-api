<?php


class APITrace{

	private static $traceArr;

	public static function functionTrace(){
		$stacktrace = debug_backtrace();

		//At each trace call, we want to know 1.) Current method, 2.) Where called from, 3.) arguments passed.

		$calledFromFile = $stacktrace[2]['file'];
		$calledFromFunction = $stacktrace[2]['function'];
		$calledFromLine = $stacktrace[2]['line'];
		$calledFromClass = $stacktrace[2]['class'];

		$file = $stacktrace[1]['file'];
		$class = $stacktrace[1]['class'];
		$functionName = $stacktrace[1]['function'];
		$functionArgs = $stacktrace[1]['args'];
		$traceLine = $stacktrace[1]['line'];

		$log  = "\nTrace:\n";
		$log .= "From: [line:".$calledFromLine."] ".$calledFromFile."\n"."class:".$calledFromClass." function:".$calledFromFunction."\n";
		$log .= "To:   [line:".$traceLine."] ".$file."\nclass:".$class." function:".$functionName." arguments:".json_encode($functionArgs)."\n";
		echo $log;
	}

}

