<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 7/4/18
 * Time: 4:50 PM
 */
require_once(__DIR__.'/../Database/DBConnection.php');
require_once(__DIR__.'/../Database/CommonQueries.php');
require_once(__DIR__.'/../Marshalling/MarshallingArrays.php');
require_once('/home/poslavu/public_html/admin/lib/redis/Sentinel.php');

class RedisConnection
{

	private static $connections;
	private static $redisTTL = 86400;

	public static function startRedisConnection()
	{
		// Default key expire time is 24 hours but we can change it from DB and store into session
		// TODO:- Right migration script to add redis_ttl into DB and store into session
		self::$redisTTL = (isset($_SESSION['redis_ttl'])) ? $_SESSION['redis_ttl'] : self::$redisTTL;
		$redisHost = CommonQueries::getAliveRedisServer();
		$redisHosts = explode(',', $redisHost['value']);
		$sentinel = new \Jenner\RedisSentinel\Sentinel();
		foreach ($redisHosts as $redisHost) {
			try {
				$redisHost = trim($redisHost);
				$sentinel->connect($redisHost, my_poslavu_redis_port());
				$sentinelDetails = $sentinel->masters();
				$masterName = $sentinelDetails[0]['name'];
				$address = $sentinel->getMasterAddrByName($masterName);
				if (class_exists('Redis')) {
					$redis = new Redis();
					$redis->connect($address['ip'], $address['port']);
					$redis->auth(my_poslavu_redis_password());
					self::$connections = $redis;
					return self::$connections;
				} else {
					throw new Exception('Redis module is not available');
				}
			} catch (Exception $e) {
				error_log('Redis Error: ' .$redisHost . ' ' . $e->getMessage());
			} catch (RedisException $e) {
				error_log('Redis Error: ' . $e->getMessage());
			}
		}
		return null;
	}

	public static function getConnection()
	{
		if (empty(self::$connections)) {
			self::$connections = self::startRedisConnection();
		}

		return self::$connections;
	}

	public static function RedisHashQuery($method, $key = '', $data = array()) {
		$conn = self::getConnection();
		if ($conn) {
			$keyExist = ($key && self::isKeyExist($conn, $key)) ? true : false;
			$response = self::{$method}($conn, $key, $data);
			if (!$keyExist && $key) {
				$conn->expire($key, self::$redisTTL);
			}
			return $response;
		}
		return null;
	}

	public static function RedisStringQuery($method, $key = '', $data = '') {
		$conn = self::getConnection();
		if ($conn) {
			$keyExist = ($key && self::isKeyExist($conn, $key)) ? true : false;
			$response = self::{$method}($conn, $key, $data);
			if (!$keyExist && $key) {
				$conn->expire($key, self::$redisTTL);
			}
			return $response;
		}
		return null;
	}

	private static function getString($conn, $key)
	{
		return $conn->get($key);
	}

	private static function setString($conn, $key, $data)
	{
		return $conn->set($key, $data);
	}

	private static function isKeyExist($conn, $key)
	{
		return $conn->exists($key);
	}

	private function setHash($conn, $key, $data = array())
	{
		if (is_array($data)) {
			return $conn->hMset($key, $data);
		}
	}

	private static function getHashAll($conn, $key)
	{
		return $conn->hGetAll($key);
	}

	public static function getAllKeys($conn)
	{
		return $conn->keys('*');
	}

	public static function checkRedisKeys($conn, $key, $data)
	{
		$allKeys = self::getAllKeys($conn);
		return array_intersect($data, $allKeys);
	}

	public static function deleteRedisKeys($conn, $key, $data)
	{
		if (empty($data)) {
			$data = self::getAllKeys($conn);
		}
		if (!empty($data)) {
			foreach ($data as $key) {
				$conn->del($key);
				$response[] = $key;
			}
		}
		return $response;
	}

	public static function closeRedisConnection($conn)
	{
		$conn->close();
		$conn = null;
	}
}