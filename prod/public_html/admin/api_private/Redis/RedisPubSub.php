<?php
/**
 * Created by PhpStorm.
 * User: Irshad Ahmad
 * Date: 17/07/19
 * Time: 1:00 PM
 */
class RedisPubSub {

	private static $redisInstance = null; // variable to check existance of an object
	private $redisConnection = null;

	/**
	* It will initiate Redis connection using Environment variables.
	* @param
	* @return boolean
	*/
	private function __construct() {
		try {
			if (class_exists('Redis')) {
				$serverIP = getenv('REDIS_SOCKET_IP');
				$serverPort = getenv('REDIS_SOCKET_PORT');
				//$serverAuth = getenv('REDIS_SOCKET_PASSWORD');
				$redis = new Redis();
				$redis->connect($serverIP, $serverPort);
				//$redis->auth($serverAuth);
				$this->redisConnection = $redis;

			} else {
				throw new Exception('Redis module is not available');
			}
		} catch (Exception $e) {
			error_log('Redis Error: ' .$serverIP. ' ' . $e->getMessage());
		}
	}

	/**
	* It will check Redis connection.
	* @param
	* @return boolean
	*/
	public static function redisConnection() {
		if (self::$redisInstance == null) {
			self::$redisInstance = new RedisPubSub();
		}
		return self::$redisInstance;
	}

	/**
	* It will check Redis connection and pass data to Redis socket to publish.
	* @param $key string
	* @param $data array
	* @return json|null
	*/
	public static function publishData($key, $data) {
		$redisObj = self::redisConnection();
		$redisConn = $redisObj->redisConnection;

		if ($redisConn->socket) {
			return $redisConn->publish($key, $data);
		} else {
			$serverIP = getenv('REDIS_SOCKET_IP');
			error_log('Redis connection error: ' .$serverIP. ' is not connected.');
		}
	}

	//to prevent cloning of an instance
	private function __clone() {}

	//To prevent unserializing of an instance
	private function __wakeup() {}

}
