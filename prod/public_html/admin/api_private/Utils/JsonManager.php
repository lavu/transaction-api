<?php

class JsonManager{
	
	public static function encodeSpecialCharacters($object){
		return json_encode($object, JSON_HEX_QUOT);
    }
    
    public static function urlJsonDecode($object){
		return json_decode(urldecode($object),1);
    }
}