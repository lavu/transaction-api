<?php

/*
 *
*/
include_once (dirname(__FILE__)."/../PizzaCreator/PizzaSettings.php");
include_once (dirname(__FILE__)."/../Image/Image.php");
include_once (dirname(__FILE__)."/../../Utils/JsonManager.php");

class ItemsDataController extends DataControllerBase
{

	public static $inStock = "InStock";
	public static $outStock = "OutStock";
	public static $modifyArr = [];
    //This will be used to reduce timestamp with prescribed mins(300 sec = 5 min) while sending it to POS
    public static $fieldExpression  = array("`unavailable_until` - IF(`unavailable_until` > 0, 300, 0) AS `unavailable_until`");

    public function __construct($request, $dataname)
    {
        parent::__construct(new ReflectionClass(__CLASS__), $request, $dataname);
    }

    protected function action_GetAccountByCredentials($args){
        $actionName = substr(__FUNCTION__, 7);
        $userRowFromMainDB = getMainDBCustomerAccountUsingCredentials($username, $password);
        $this->verifyNotEmpty($userRowFromMainDB,$actionName,"Could not find username in main db.");
        DBConnection::startClientConnection($userRowFromMainDB['dataname']);
        $userRowInClientDB = ADCQ::getUserRowFromClientDB($username, $password);
        $this->verifyNotEmpty($userRowInClientDB,"Username and Password match not found.");
    }

    protected static function action_getHappyHoursList()
    {
    	$select_arr = array('id', 'start_time', 'end_time', 'weekdays', 'property', 'extended_properties');
    	$happyhour_list = CQ::getRowsWithRequiredColumns('happyhours', $select_arr, array("_deleted"=>0), '', '');
    	return $happyhour_list;
    }

    public static function action_getOptionalModifierById($id)
    {
    	$select_arr = array('id', 'cost');
    	$optinal_list = CQ::getRowsWithRequiredColumns('modifiers', $select_arr, array("_deleted"=>0, "id" => $id), '', '');
    	return $optinal_list;
    }

    public static function action_getForcedModifierById($id)
    {
    	$select_arr = array('id', 'cost');
    	$optinal_list = CQ::getRowsWithRequiredColumns('forced_modifiers', $select_arr, array("_deleted"=>0, "id" => $id), '', '');
    	return $optinal_list;
    }

    /*
    protected static function action_getHappyHours($cat_id, $happy_hour_str, $happy_hour_list){
    	$happy_hours_data = array();
    	//echo "<pre>"; print_r($happy_hour_list);
    	if(!empty($happy_hour_list)){
    		$happyhour_cat_arr_list = CQ::getRowsWithRequiredColumns('menu_categories', array("happyhour"), array("_deleted"=>0,"ltg_display"=>1,"id"=>$cat_id), '', '');
    		//print_r($happyhour_cat_arr[0]['happyhour']);exit;
    		if($happyhour_cat_arr_list[0]['happyhour'] != ""){
    			$happy_hour_cat_arr_tmp = explode("|", $happyhour_cat_arr_list[0]['happyhour']);
    			$happy_hour_cat_arr = array();
    			foreach ($happy_hour_cat_arr_tmp as $hhrs_cat){
    				$happy_hour_cat_arr[] = explode(":",$hhrs_cat)[0];
    			}
    		}
    		//print_r($happy_hour_cat_arr);

    		if($happy_hour_str != ""){ // item level
    			$happy_hour_item_arr_tmp = explode("|",$happy_hour_str);
    			//echo "<br>:hours start:<br>";
    			foreach ($happy_hour_item_arr_tmp as $hhr_item){
    				$happy_hour_item_str_arr = explode(":", $hhr_item);

    				if($happy_hour_item_str_arr[1] == "e"){
    					$happy_hours_data[$happy_hour_item_str_arr[0]] = $happy_hour_item_str_arr;
    				}
    				if(($key = array_search($happy_hour_item_str_arr[0], $happy_hour_cat_arr)) !== false) {
    				 	unset($happy_hour_cat_arr[$key]);
    				}

    			}
    			//echo "<br>:hours end:<br>";
    			//print_r($happy_hour_cat_arr);
    			//print_r($happy_hours_data);
    		}
    		//echo "<br>keys items:<br>";print_r(array_keys($happy_hours_data));
    		$happy_hour_cat_arr_tmp = array();
    		foreach ($happy_hour_list as $hhours){
    			 if(in_array($hhours['id'], $happy_hour_cat_arr))
    			 {
    			 	$hhours['property'] = explode("/", $hhours['property']);
    			 		if($hhours['property'][0] == "p"){
    			 			$hhours['property'][0] = "price";
    			 		}
    			 		else if($hhours['property'][0] == "a"){
    			 			$hhours['property'][0] = "available";
    			 			if($hhours['property'][1] != "a")
    			 				$hhours['property'][0] = "unavailable";
    			 			unset($hhours['property'][1]);
    			 		}
    			 		unset($hhours['extended_properties']);
    			 		$happy_hour_cat_arr_tmp[$hhours['id']] = $hhours;
    			 }

    			 if(in_array($hhours['id'], array_keys($happy_hours_data)))
    			 {
    			 	$hhours['property'] = explode("/", $happy_hours_data[$hhours['id']][2]);
    			 	if($hhours['property'][0] == "p"){
    			 		$hhours['property'][0] = "price";
    			 	}
    			 	else if($hhours['property'][0] == "a"){
    			 		$hhours['property'][0] = "available";
    			 		if($hhours['property'][1] != "a")
    			 			$hhours['property'][0] = "unavailable";
    			 			unset($hhours['property'][1]);
    			 	}
    			 	unset($hhours['extended_properties']);
    			 	$happy_hour_cat_arr_tmp[$hhours['id']] = $hhours;
    			 }
    		}
    		$happy_hours_data = $happy_hour_cat_arr_tmp;

    		unset($happy_hour_cat_arr_tmp);
    		unset($happy_hour_cat_arr);
    		unset($happyhour_cat_arr_list);
    	}
    	print_r($happy_hours_data);exit;
    	return $happy_hours_data;
    }

    */

	protected function action_getItemsList($args)
	{
    	//print_r($args);exit;
        $whereColumnArr = $args['categoryID'];
		$dineinoptions = $args['dining_option'];
        $whereColumn = $args['categoryID'];
        $whereColumnDefault = array('_deleted' => 0, 'ltg_display' => 1, "dining_option" => "like:".$dineinoptions);
        //$whereColumnDefault = array('_deleted' => 0, 'ltg_display' => 1);
        $whereColumnArr = array_merge($whereColumn, $whereColumnDefault);
        $paginationArr = $args['pagination'];
        $data_name = $args['dataname'];
        if (!isset($paginationArr['total_page'])) {
            $paginationMetaData = MQ::buildSanitizedPagination('menu_items', $whereColumnArr, $paginationArr);
        }
        $menuItemsColNameArr = array("id", "name", "price", "category_id", "ingredients", "description", "forced_modifier_group_id", "modifier_list_id", "image", "dining_option", "nutrition", "heart_healthy", "contains_gluten", "contains_dairy", "hidden_value3","track_86_count", "storage_key");
        $catIdQuery = 'SELECT `group_id` FROM `menu_categories` WHERE `id` = "'.$whereColumnArr['category_id'].'" AND `_deleted` != 1';
        $catResultObj = DBConnection::clientQuery($catIdQuery);
        $catResult =  MA::resultToArray($catResultObj);
        $groupIdQuery = 'SELECT `orderby` FROM `menu_groups` WHERE `id` = "'.$catResult[0]['group_id'].'" AND `_deleted` != 1';
        $groupResultObj = DBConnection::clientQuery($groupIdQuery);
        $groupResult = MA::resultToArray($groupResultObj);
        $orderBy = ($groupResult[0]['orderby'] == 'manual') ? array("_order"=>"Asc") :  $orderBy = array("name"=>"Asc");
        $current_page=$paginationArr['current_page'];
        $per_page=$paginationArr['per_page'];
        if($current_page==1){
         $otherClause=array("limit"=>"0,$per_page");
        } else {
         $pages=$current_page*$per_page;
         $pages2=($pages-$per_page);
         $otherClause=array("limit"=>"$pages2,$per_page");
        }
        $menuItemRowsResultArr = array();
        $menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnArr, $orderBy, $otherClause, '', self::$fieldExpression);
        $whereConfigDataNameColumnArr = array('ltg_use_opt_mods_2');
        $configData = CQ::getRowsWhereColumnIn('config', "setting", $whereConfigDataNameColumnArr, $data_name);
        //$happy_hour_list = ItemsDataController::action_getHappyHoursList(); // fetching all happy hours master data

        $i = 0;
      	//echo "<pre>";print_r($menuItemRowsResult);exit();
        if (!empty($menuItemRowsResult)) {
			$inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
			$pizzaSettings = new PizzaSettings($data_name);
            foreach ($menuItemRowsResult as $val) {
				//---------------------------------------------------------------------------------
				// Get Pizza Creator per Menu Item
				//---------------------------------------------------------------------------------
				$itemPizzaCreatorConfigData = $pizzaSettings->getPizzaCreator($val);
				if(isset($itemPizzaCreatorConfigData)){
					$menuItemRowsResult[$i]['use_pizza_creator'] = true;
					$menuItemRowsResult[$i]['pizza_creator'] = $itemPizzaCreatorConfigData;
				} else
					$menuItemRowsResult[$i]['use_pizza_creator'] = false;
				//---------------------------------------------------------------------------------

            	$return = ItemsDataController::getItemInventory($menuItemRowsResult[$i]['id']);
            	//echo $i."id-".$menuItemRowsResult[$i]['id'];
            	$menuItemRowsResult[$i]['availability'] = self::$inStock;
            	if($return['stock'] == "continue"){
            		$menuItemRowsResult[$i]['availability'] = self::$outStock;
            		//$i++;
            		//continue;
            	}
                $ingredientsVal = "";
                // LP-4364 START
                $menunutritionStr = "";
                if($val['nutrition'])
                {
                    $menunutrition = explode(",", $val['nutrition']);
                    foreach($menunutrition as $nutVal)
                    {
                        if(substr($nutVal,-1) == "1")
                        {
                            $nutVal = substr($nutVal,0, strlen($nutVal)-4);
                            $menunutritionStr .= $nutVal.",";
                        }
                    }
                }
                $menuItemRowsResult[$i]['nutrition'] = rtrim($menunutritionStr,',');
                // LP-4364 END
                /*$idArr = array();
                if ($menuItemRowsResult[$i]['ingredients'] != "") {
                    $ingredients = $val['ingredients'];
                    $ingredientsArr = explode(",", $ingredients);
                    foreach ($ingredientsArr as $ingVal) {
                        $id = explode("x", $ingVal);
                        $idArr[] = $id[0];
                    }
                    $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                    $ingredientsVal = "";
                    foreach ($menuItemIngredientsRowsResult as $menuIngval) {
                        $ingredientsVal .= $menuIngval['title'] . ",";
                    }
                    $ingredientsVal = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);
                }
				*/
                /*
                 * Ingradients for menuitems
                 */
                $ingredientsVal = ItemsDataController::getIngredientsById($val['id'], 'menu', $inventoryMigrationStatus);

                /*
                if(!empty($happy_hour_list)){
                	$happy_hour = ItemsDataController::action_getHappyHours($menuItemRowsResult[$i]['category_id'], $menuItemRowsResult[$i]['happyhour'], $happy_hour_list);
                }
                else
                	$menuItemRowsResult[$i]['happyhour'] = ""; // No Happy hours

                exit;
                */

                $total_award = 0;
                $special2 = strtolower($menuItemRowsResult[$i]['hidden_value3']); // special details 2
                $price = $menuItemRowsResult[$i]['price']; // item price
                $award_parts = explode("award ",$special2);
                if(count($award_parts) > 1)
                {
                	$point_parts = explode(" point",$award_parts[1]);
                	if(count($point_parts) > 1)
                	{
                		$point_str = trim($point_parts[0]);

                		if(is_numeric($point_str))
                		{
                			$min_cost = 0;
                			$min_cost_parts = explode("minimum cost ",$point_parts[1]);
                			if(count($min_cost_parts) > 1)
                			{
                				$min_cost_str = trim(str_replace("of ","",$min_cost_parts[1]));
                				if(is_numeric($min_cost_str))
                				{
                					$min_cost = $min_cost_str * 1;
                				}
                			}

                			if($price * 1 >= $min_cost * 1)
                			{
                				$total_award += $point_str * 1;
                			}
                		}
                	}
                }

				$menuItemRowsResult[$i]['award_points'] = $total_award;

				$menuItemImage = ItemsDataController::getMenuItemImage($menuItemRowsResult[$i]["storage_key"], $menuItemRowsResult[$i]["image"], $data_name);
				$menuItemRowsResult[$i]["image"] = $menuItemImage->image;
				$menuItemRowsResult[$i]["item_image_present"] = $menuItemImage->itemImagePresent;

                $menuItemRowsResult[$i]['ingredients'] = $ingredientsVal;
                $menuItemRowsResult[$i]['images'] = array("title" => "", "protien" => "", "thumbnail" => $menuItemRowsResult[$i]["image"],
                    "medium" => $menuItemRowsResult[$i]["image"]);
                $modifierType = "";

                //print_r($menuItemRowsResult);
                //echo $val['id'];
                if ($val['forced_modifier_group_id'] != "") {
                    if (strtolower($val['forced_modifier_group_id']) != 'c' && strtolower(substr($val['forced_modifier_group_id'], 0, 1)) == "f") {
                        $modifierType = "i";
                        $whereColumnArr = array("id" => substr($val['forced_modifier_group_id'], 1), "_deleted" => 0);
                        $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
                        $forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
                        if (!empty($forcedModifierListRowResult)) {
                            $caption = "Choose your " . $forcedModifierListRowResult[0]['title'];
                            $forcedModifierListRowResult[0]['caption'] = $caption;
                            $forcedModifierColumnArr = array("id", "cost", "title", "ingredients", "nutrition","detour");
                            $orderBy = array("_order"=>"Asc");
                            $whereColumnArr = array("list_id" => $forcedModifierListRowResult[0]['id'], "_deleted" => 0);
                            $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                            if (!empty($forcedModifierRowResult)) {
                            	$outofstockmodifiers = 0;
                                foreach ($forcedModifierRowResult as $val) {
                                	self::$modifyArr = [];
                                	if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
                                	 	$return = ItemsDataController::getForcedModifierInventory($val['id']);
	                                	if($return['stock'] == "continue"){
	                                		//$i++;
	                                		$outofstockmodifiers++;
	                                		continue;
	                                	}
                                	}
                                	// LP-4364 START
                                	$forcedModifierStr = "";
                                	if($val['nutrition'])
                                	{
                                		$forcedModifier = explode(",", $val['nutrition']);
                                		foreach($forcedModifier as $fmnutVal)
                                		{
                                			if(substr($fmnutVal,-1) == "1")
                                			{
                                				$fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
                                				$forcedModifierStr .= $fmnutVals.",";
                                			}
                                		}
                                	}
                                	$val['nutrition'] = rtrim($forcedModifierStr,',');
                                	// LP-4364 END
                                	if ($val['id'] != "") {
                                        /*$ingredients = $val['ingredients'];
                                        $ingredientsArr = explode(",", $ingredients);
                                        foreach ($ingredientsArr as $ingredientVal) {
                                            $id = explode("x", $ingredientVal);
                                            $idArr[] = $id[0];
                                        }
                                        $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                        $ingredientsVal = "";
                                        foreach ($menuItemIngredientsRowsResult as $ingrVal) {
                                            $ingredientsVal .= $ingrVal['title'] . ",";
                                        }
                                        $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                        $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                                        $val['ingredients'] = $ingredientsStr;
                                        unset($menuItemIngredientsRowsResult);
                                    }

                                    $val['status'] = "";
                                    if(strtolower(substr($val['detour'], 0, 1)) == "g" && $forcedModifierListRowResult[0]['type'] != "checklist") {
                                       $val['doption'] = self::get_group_modifiers(substr($val['detour'], 1), $inventoryMigrationStatus);
                                    }
                                    else{
										if($val['detour'] && $forcedModifierListRowResult[0]['type'] != "checklist"){
											$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
											$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
											$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
											$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
										}
										//echo "346<br>".$val['detour'];
										$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
										if(!empty($dlist) && $forcedModifierListRowResult[0]['type'] != "checklist"){
											$dopt_n = array_merge($forcedModifierdListRowResult[0],array('option'=>$dlist));
											$val['doption'][]= $dopt_n;
										}
                                    }
									$forcedModifierRowArr[] = $val;
                                    unset($menuItemModifierNutritionArr);
                                }
                               	if($forcedModifierListRowResult[0]['type']=='choice' && (count($forcedModifierRowArr)>0 && count($forcedModifierRowArr)==$outofstockmodifiers)){
                                	$menuItemRowsResult[$i]['availability'] = self::$outStock;
                                }
                            }
                        }
                    } else if (strtolower(substr($val['forced_modifier_group_id'], 0, 1)) != "f" && strtolower($val['forced_modifier_group_id']) != "c") {
                        $modifierType = "C";
                        unset($forcedModifierListRowResult);
                        unset($forcedModifierGroupRowResult);
                        //print_r($forcedModifierGroupRowResult);
                        $whereColumnArr = array("id" => $val['forced_modifier_group_id'], "_deleted" => 0);
                        $forcedModifierListColumnArr = array("id", "title", "include_lists");
                        $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_groups', $forcedModifierListColumnArr, $whereColumnArr,'');
                        if (!empty($forcedModifierGroupRowResult)) {
                        	$outofstockmodifiers = 0;
                            $inlcList = explode("|", $forcedModifierGroupRowResult[0]['include_lists']);
                             foreach($inlcList as $grpVal) {
                                $whereColumnArr = array("id" => $grpVal, "_deleted" => 0);
                                $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
                                $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
                                 if (!empty($forcedModifierGroupRowResult)) {
                                    $caption = "Choose your " . $forcedModifierGroupRowResult[0]['title'];
                                    $forcedModifierGroupRowResult[0] ['caption'] = $caption;
                                    $forcedModifierColumnArr = array("id", 'cost', "title", "ingredients", "nutrition","detour");
                                    $orderBy = array("_order");
                                    $whereColumnArr = array("list_id" => $forcedModifierGroupRowResult[0]['id'], "_deleted" => 0);
                                     $orderBy = array("_order"=>"Asc");
                                    $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                                    if (!empty($forcedModifierRowResult)) {
                                        foreach ($forcedModifierRowResult as $val) {
                                        	if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
                                                $return = ItemsDataController::getForcedModifierInventory($val['id']);
                                            	if($return['stock'] == "continue"){
                                            		//$i++;
                                            		$outofstockmodifiers++;
                                            		//continue;}
                                            	}
                                            }
                                        	// LP-4364 START
                                        	$forcedModifierStr = "";
                                        	if($val['nutrition'])
                                        	{
                                        		$forcedModifier = explode(",", $val['nutrition']);
                                        		foreach($forcedModifier as $fmnutVal)
                                        		{
                                        			if(substr($fmnutVal,-1) == "1")
                                        			{
                                        				$fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
                                        				$forcedModifierStr .= $fmnutVals.",";
                                        			}
                                        		}
                                        	}
                                        	$val['nutrition'] = rtrim($forcedModifierStr,',');
                                        	// LP-4364 END
                                            if ($val['id'] != "") {
                                                /*$ingredients = $val['ingredients'];
                                                $ingredientsArr = explode(",", $ingredients);
                                                foreach ($ingredientsArr as $ingredientVal) {
                                                    $id = explode("x", $ingredientVal);
                                                    $idArr[] = $id[0];
                                                }
                                                $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                                $ingredientsVal = "";
                                                foreach ($menuItemIngredientsRowsResult as $ingVal) {
                                                    $ingredientsVal .= $ingVal['title'] . ",";
                                                }
                                                $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                                $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                                                $val['ingredients'] = $ingredientsStr;
                                                unset($menuItemIngredientsRowsResult);
                                            }

                                            $val['status'] = "";
                                            if(strtolower(substr($val['detour'], 0, 1)) == "g")
                                            {
                                               $val['doption'] = self::get_group_modifiers(substr($val['detour'], 1), $inventoryMigrationStatus);
                                            }
                                            else
                                            {
                                            	if($val['detour'] && $forcedModifierGroupRowResult[0]['type'] != "checklist"){
                                            		$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
                                            		$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
                                            		$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
                                            		$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
                                            	}
                                                $dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
                                            	if(!empty($dlist) && $forcedModifierGroupRowResult[0]['type'] != "checklist"){
                                            		$dopt_n = array_merge($forcedModifierdListRowResult[0],array('option'=>$dlist));
                                            		$val['doption'][]= $dopt_n;
                                            	}
                                            }
                                            $forcedModifierGroupRowResultArr[] = $val;
                                            unset($menuItemModifierNutritionArr);
                                        }

                                    }
                                   if($forcedModifierGroupRowResult[0]['type']=='choice' && (count($forcedModifierGroupRowResultArr)>0 && count($forcedModifierGroupRowResultArr)==$outofstockmodifiers)){
                                    	$menuItemRowsResult[$i]['availability'] = self::$outStock;
                                    }
                                }
                                $forcedModifierGroupRowResult[0]['option'] = $forcedModifierGroupRowResultArr;
                                $forcedModifierGroupRowArr[] = $forcedModifierGroupRowResult[0];
                                unset($forcedModifierGroupRowResultArr);
                            }
                            $forcedModifierRowArr = $forcedModifierGroupRowArr;
                            unset($forcedModifierGroupRowArr);
                        }
                    } else {
                        $modifierType = "G";
                        unset($forcedModifierListRowResult);
                        unset($forcedModifierGroupRowResult);
                        $forcedModifierGroupRowResultArr = array();
                        $whereColumnArr = array("id" => $val['category_id'], "_deleted" => 0);
                        $forcedModifierGroupColumnArr = array("forced_modifier_group_id");
                        $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('menu_categories', $forcedModifierGroupColumnArr, $whereColumnArr);
                        if(strtolower(substr($forcedModifierGroupRowResult[0]['forced_modifier_group_id'], 0, 1)) == "f") {
                            $modifierType = "i";
                            $whereColumnArr = array("id" =>substr($forcedModifierGroupRowResult[0]['forced_modifier_group_id'], 1), "_deleted" => 0);
                            $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
                            $forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
                            if (!empty($forcedModifierListRowResult)) {
                                $caption = "Choose your " . $forcedModifierListRowResult[0]['title'];
                                $forcedModifierListRowResult[0]['caption'] = $caption;
                                $forcedModifierColumnArr = array("id", "cost", "title", "ingredients", "nutrition","detour");
                                $orderBy = array("_order"=>"Asc");
                                $whereColumnArr = array("list_id" => $forcedModifierListRowResult[0]['id'], "_deleted" => 0);
                                $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                                if (!empty($forcedModifierRowResult)) {
                                	$outofstockmodifiers = 0;
                                    foreach ($forcedModifierRowResult as $val) {
                                    	if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
                                            $return = ItemsDataController::getForcedModifierInventory($val['id']);
                                        	if($return['stock'] == "continue"){
                                        		//$i++;
                                        		$outofstockmodifiers++;
                                        		//continue;}
                                        	}
                                        }
                                    	// LP-4364 START
                                    	$forcedModifierStr = "";
                                    	if($val['nutrition'])
                                    	{
                                    	    $forcedModifier = explode(",", $val['nutrition']);
                                    	    foreach($forcedModifier as $fmnutVal)
                                    	    {
                                    	        if(substr($fmnutVal,-1) == "1")
                                    	        {
                                    	            $fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
                                    	            $forcedModifierStr .= $fmnutVals.",";
                                    	        }
                                    	    }
                                    	}
                                    	$val['nutrition'] = rtrim($forcedModifierStr,',');
                                    	// LP-4364 END
                                        if ($val['id'] != "") {
                                            /*$ingredients = $val['ingredients'];
                                            $ingredientsArr = explode(",", $ingredients);
                                            foreach ($ingredientsArr as $ingredientVal) {
                                                $id = explode("x", $ingredientVal);
                                                $idArr[] = $id[0];
                                            }
                                            $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                            $ingredientsVal = "";
                                            foreach ($menuItemIngredientsRowsResult as $ingrVal) {
                                                $ingredientsVal .= $ingrVal['title'] . ",";
                                            }
                                            $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                            $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                                            $val['ingredients'] = $ingredientsStr;
                                            unset($menuItemIngredientsRowsResult);
                                        }

                                        $val['status'] = "";
                                        if(strtolower(substr($val['detour'], 0, 1)) == "g")
                                        {
                                            $val['doption'] = self::get_group_modifiers(substr($val['detour'], 1), $inventoryMigrationStatus);
                                        }
                                        else
                                        {
											if($val['detour'] && $forcedModifierListRowResult[0]['type'] != "checklist"){
												$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
												$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
												$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
												$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
											}
											$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
											if(!empty($dlist) && $forcedModifierListRowResult[0]['type'] != "checklist"){
												$dopt_n = array_merge($forcedModifierdListRowResult[0],array('option'=>$dlist));
												$val['doption'][]= $dopt_n;
											}
                                        }
                                        $forcedModifierRowArr[] = $val;
                                        unset($menuItemModifierNutritionArr);
                                    }
                                }

                                if($forcedModifierListRowResult[0]['type']=='choice' && (count($forcedModifierRowArr)>0 && count($forcedModifierRowArr)==$outofstockmodifiers)){
                                	$menuItemRowsResult[$i]['availability'] = self::$outStock;
                                }
                            }
                        } else{
                            $modifierType = "C";
                            $whereColumnArr = array("id" => $forcedModifierGroupRowResult[0]['forced_modifier_group_id'], "_deleted" => 0);
                            $forcedModifierListColumnArr = array("id", "title", "include_lists");
                            $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_groups', $forcedModifierListColumnArr, $whereColumnArr);
                            if (!empty($forcedModifierGroupRowResult)) {
                                $inlcList = explode("|", $forcedModifierGroupRowResult[0]['include_lists']);
                                foreach($inlcList as $grpVal) {
                                    $whereColumnArr = array("id" => $grpVal, "_deleted" => 0);
                                    $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
                                    $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr);
                                    if (!empty($forcedModifierGroupRowResult)) {
                                    	$outofstockmodifiers = 0;
                                        $caption = "Choose your " . $forcedModifierGroupRowResult[0]['title'];
                                        $forcedModifierGroupRowResult[0] ['caption'] = $caption;
                                        $forcedModifierColumnArr = array("id", 'cost', "title", "ingredients", "nutrition","detour");
                                        $orderBy = array("_order");
                                        $whereColumnArr = array("list_id" => $forcedModifierGroupRowResult[0]['id'], "_deleted" => 0);
                                        $orderBy = array("_order"=>"Asc");
                                        $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                                        if (!empty($forcedModifierRowResult)) {
                                            foreach ($forcedModifierRowResult as $val) {
                                            	if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
                                                    $return = ItemsDataController::getForcedModifierInventory($val['id']);
                                                	if($return['stock'] == "continue"){
                                                		//$i++;
                                                		$outofstockmodifiers++;
                                                		//continue;}
                                                	}
                                                }
                                                if ($val['id'] != "") {
                                                    /*$ingredients = $val['ingredients'];
                                                    $ingredientsArr = explode(",", $ingredients);
                                                    foreach ($ingredientsArr as $ingredientVal) {
                                                        $id = explode("x", $ingredientVal);
                                                        $idArr[] = $id[0];
                                                    }
                                                    $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                                    $ingredientsVal = "";
                                                    foreach ($menuItemIngredientsRowsResult as $ingVal) {
                                                        $ingredientsVal .= $ingVal['title'] . ",";
                                                    }
                                                    $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                                    $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                                                    $val['ingredients'] = $ingredientsStr;
                                                    unset($menuItemIngredientsRowsResult);
                                                }

                                                $val['status'] = "";
                                                if(strtolower(substr($val['detour'], 0, 1)) == "g")
                                                {
												   $val['doption'] = self::get_group_modifiers(substr($val['detour'], 1), $inventoryMigrationStatus);
                                                }
                                                else
                                                {
													if($val['detour'] && $forcedModifierGroupRowResult[0]['type'] != "checklist"){
														$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
														$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
														$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
														$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
													}
													$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
													if(!empty($dlist) && $forcedModifierGroupRowResult[0]['type'] != "checklist"){
														$dopt_n = array_merge($forcedModifierdListRowResult[0],array('option'=>$dlist));
														$val['doption'][]= $dopt_n;
													}
                                                }
												$forcedModifierGroupRowResultArr[] = $val;
                                                unset($menuItemModifierNutritionArr);
                                            }
                                        }
                                       if($forcedModifierGroupRowResult[0]['type']=='choice' && (count($forcedModifierGroupRowResultArr)>0 && count($forcedModifierGroupRowResultArr)==$outofstockmodifiers)){
                                        	$menuItemRowsResult[$i]['availability'] = self::$outStock;
                                        }
                                    }

                                    $forcedModifierGroupRowResult[0]['option'] = $forcedModifierGroupRowResultArr;
                                    $forcedModifierGroupRowArr[] = $forcedModifierGroupRowResult[0];
                                    unset($forcedModifierGroupRowResultArr);
                                }
                                $forcedModifierRowArr = $forcedModifierGroupRowArr;
                                unset($forcedModifierGroupRowArr);
                            }
                        }
                    }
                }
                $optionModifierRowArr = array();
                $modifier_list_id = $menuItemRowsResult[$i]['modifier_list_id'] ;
                if ($modifier_list_id == 0) {
					$whereColumnArr = array("id" => $menuItemRowsResult[$i]['category_id'], "_deleted" => 0);
					$optionalModifierGroupColumnArr = array("modifier_list_id");
					$optionalModifierGroupRowResult = CQ::getRowsWithRequiredColumns('menu_categories', $optionalModifierGroupColumnArr, $whereColumnArr);
					if ($optionalModifierGroupRowResult[0]['modifier_list_id'] != 0) {
						$modifier_list_id = $optionalModifierGroupRowResult[0]['modifier_list_id'] ;
					}
                }
                if ($modifier_list_id != 0 && $configData[0]["value"]==1) {
                    $whereColumnArr = array("id" => $modifier_list_id, "_deleted" => 0);
                    $optionModifierListColumnArr = array("id", "title");
                    $optionModifierListRowResult = CQ::getRowsWithRequiredColumns('modifier_categories', $optionModifierListColumnArr, $whereColumnArr);
                     if (!empty($optionModifierListRowResult)) {
                        $caption = "Choose your " . $optionModifierListRowResult[0]['title'];
                        $optionModifierListRowResult[0] ['caption'] = $caption;
                        $optionModifierColumnArr = array("id", "title", "cost", "ingredients", "nutrition");
                        $orderBy = array("_order"=>"Desc");
                        $whereColumnArr = array("category" => $optionModifierListRowResult[0]['id'], "_deleted" => 0);
                        $optionModifierRowResult = CQ::getRowsWithRequiredColumns('modifiers', $optionModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                        //print_r($optionModifierRowResult);
                        foreach ($optionModifierRowResult as $val) {
                        	$return = self::getOptionalModifierInventory($val['id']);
                        	if($return == "continue"){
                        		$i++;
                        		continue;
                        	}
                        	// LP-4364 START
                        	$forcedModifierStr = "";
                        	if($val['nutrition'])
                        	{
                        		$forcedModifier = explode(",", $val['nutrition']);
                        		foreach($forcedModifier as $fmnutVal)
                        		{
                        			if(substr($fmnutVal,-1) == "1")
                        			{
                        				$fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
                        				$forcedModifierStr .= $fmnutVals.",";
                        			}
                        		}
                        	}
                        	$val['nutrition'] = rtrim($forcedModifierStr,',');
                        	// LP-4364 END
                            if ($val['id'] != "") {
                               /* $ingredients = $val['ingredients'];
                                $ingredientsArr = explode(",", $ingredients);
                                foreach ($ingredientsArr as $ingredientVal) {
                                    $id = explode("x", $ingredientVal);
                                    $idArr[] = $id[0];
                                }
                                $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                $ingredientsVal = "";
                                foreach ($menuItemIngredientsRowsResult as $ingVal) {
                                    $ingredientsVal .= $ingVal['title'] . ",";
                                }
                                $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'm', $inventoryMigrationStatus);
                                $val['ingredients'] = $ingredientsStr;
                                unset($menuItemIngredientsRowsResult);
                            }
                            $val['status'] = "";
                            $val["type"] = "checklist";
                            $optionModifierRowArr[] = $val;
                            unset($menuItemModifierNutritionArr);
                        }
                         $optionModifierListRowResult [0]['option']= $optionModifierRowArr;
                    }
                }
                if (!empty($forcedModifierListRowResult)) {
                    $forcedModifierListRowResultArr = $forcedModifierListRowResult[0];
                } else {
                    $forcedModifierListRowResultArr = array();
                }
                if(!empty($forcedModifierRowArr) &&  $modifierType == "G") {
                    $forcedModifierListRowResultArr = $forcedModifierRowArr;
                    $menuItemRowsResult[$i]['forced'] = $forcedModifierListRowResultArr;
                }
                elseif (!empty($forcedModifierRowArr) &&  $modifierType != "G") {
                	$forcedModifierListRowResultArr['option'] = $forcedModifierRowArr;
                	$menuItemRowsResult[$i]['forced'][] = $forcedModifierListRowResultArr;
                } else {
                    $menuItemRowsResult[$i]['forced'] = array();
                }

                unset($forcedModifierRowArr);


                if (!empty($optionModifierListRowResult)) {
                    $optionalModifierListRowResultArr = $optionModifierListRowResult[0];
                } else {
                    $optionalModifierListRowResultArr = array();
                }
                if (!empty($optionModifierRowArr)) {
                    $optionalModifierListRowResultArr['option'] = $optionModifierRowArr;
                    $menuItemRowsResult[$i]['optional'][]= $optionalModifierListRowResultArr;
                } else {
                    $menuItemRowsResult[$i]['optional'] = array();
                }
                $menuItemRowsResultArr[] = $menuItemRowsResult[$i];
                $i++;
            }
        }
	 	$category_id=$args['categoryID']['category_id'];
        $whereColumnArr =array("id"=>$category_id);
        $whereColumn =array("id"=>$category_id);
        $whereColumnDefault = array('_deleted' => 0, 'ltg_display' => 1);
        $whereColumnArr = array_merge($whereColumn, $whereColumnDefault);
        $categoryImageColNameArr = array("id","group_id", "name","olo_image","storage_key","image");
        $orderBy = array("_order"=>"Asc");
		$categoryImage = CQ::getRowsWithRequiredColumns('menu_categories', $categoryImageColNameArr, $whereColumnArr);
		$categoryImage = ItemsDataController::getCategoryImage($categoryImage[0]["storage_key"], $categoryImage[0]["image"], $data_name);
		$resultArr = array("pagination" => $paginationMetaData, "bannerImage"=>$categoryImage->image,"Items" => $menuItemRowsResultArr);
        return $resultArr;
    }

    public static function getItemInventory($args, $itemQty = ''){
    	//echo "Qty--".$itemQty;
    	$itemId = $args;
    	$whereColumnArr = array('id' => $itemId, '_deleted' => 0, 'ltg_display' => 1);
    	$menuItemsColNameArr = array("id", "inv_count", "track_86_count");
    	$menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnArr);
    	$continue = "forward";
    	$instock_count = '';
    	if(!empty($menuItemRowsResult)) {
    		foreach ($menuItemRowsResult as $val){
    			if($menuItemRowsResult[0]['track_86_count'] == '86count' && $itemQty !='' && $menuItemRowsResult[0]['inv_count']<$itemQty){
    				$continue = "continue";
    				$instock_count = $menuItemRowsResult[0]['inv_count'];
    			}
    			elseif($menuItemRowsResult[0]['track_86_count'] == '86count' && $menuItemRowsResult[0]['inv_count'] <= 0){
    						$continue = "continue";
    						$instock_count = $menuItemRowsResult[0]['inv_count'];
    			}elseif($menuItemRowsResult[0]['track_86_count'] == "86countInventory"){
    				//echo $menuItemRowsResult[0]['id'];
    				$whereInventoryColumnArr = array('menuItemID' => $menuItemRowsResult[0]['id'], '_deleted' => 0);
    				$menuInventoryItemsColNameArr = array("inventoryItemID", "unitID", "quantityUsed", "reservation");
    				$menuInventorytemRowsResult = CQ::getRowsWithRequiredColumns('menuitem_86', $menuInventoryItemsColNameArr, $whereInventoryColumnArr, '');
    				//print_r($menuInventorytemRowsResult);
    				if(!empty($menuInventorytemRowsResult)){
    					foreach ($menuInventorytemRowsResult as $inv_val){
	    					$whereColumnArr = array('id' => $inv_val['inventoryItemID'], '_deleted' => 0);
	    					$menuItemsColNameArr = array("quantityOnHand", "lowQuantity");
	    					$menuItemRowsResult = CQ::getRowsWithRequiredColumns('inventory_items', $menuItemsColNameArr, $whereColumnArr, '');
	    					//print_r($menuItemRowsResult);
	    					if(!empty($menuItemRowsResult) && $itemQty == ''){
	    						if($menuItemRowsResult[0]['quantityOnHand'] <= $menuItemRowsResult[0]['lowQuantity']){
	    							$continue = "continue";
	    							$instock_count = $menuItemRowsResult[0]['quantityOnHand']-$menuItemRowsResult[0]['lowQuantity'];
	    						}
	    					}
	    					else if(!empty($menuItemRowsResult)){
	    						if($itemQty >= 1){
	    							//echo $menuItemRowsResult[0]['quantityOnHand'] - (($inv_val['quantityUsed'] * $itemQty) + $inv_val['reservation']);exit;
	    							if($menuItemRowsResult[0]['quantityOnHand'] - (($inv_val['quantityUsed'] * $itemQty) + $inv_val['reservation']) >= 0){
	    								if(($menuItemRowsResult[0]['quantityOnHand'] - (($inv_val['quantityUsed'] * $itemQty) + $inv_val['reservation'])) < $menuItemRowsResult[0]['lowQuantity']){
	    									$continue = "continue";
	    									$instock_count = $menuItemRowsResult[0]['quantityOnHand']-$menuItemRowsResult[0]['lowQuantity'];
	    								}
	    							}
	    							else{
	    								$continue = "continue";
	    								$instock_count = $menuItemRowsResult[0]['quantityOnHand']-$menuItemRowsResult[0]['lowQuantity'];
	    							}
	    						}
	    						else{
	    							$continue = "continue";
	    							$instock_count = $menuItemRowsResult[0]['quantityOnHand']-$menuItemRowsResult[0]['lowQuantity'];
	    						}
	    					}
    					}
    				}
    			}

    		}
    	}
    	//return $continue;
    	return array('stock'=>$continue,'instock'=>$instock_count);
    }

    public static function getForcedModifierInventory($args, $itemQty = ''){
    	$forcedModifierId = $args;
    	$continue = "forward";
    	$instock = array();
    	$whereInventoryColumnArr = array('forced_modifierID' => $forcedModifierId, '_deleted' => 0);
    	//print_R($whereInventoryColumnArr);
    	$menuInventoryItemsColNameArr = array("inventoryItemID", "unitID","quantityUsed","reservation");
    	$menuInventorytemRowsResult = CQ::getRowsWithRequiredColumns('forced_modifier_86', $menuInventoryItemsColNameArr, $whereInventoryColumnArr, '');
    	if(!empty($menuInventorytemRowsResult)){
    		foreach ($menuInventorytemRowsResult as $inv_val){
    			//print_R($inv_val);
	    		$whereColumnArr = array('id' => $inv_val['inventoryItemID'], '_deleted' => 0);
	    		$menuItemsColNameArr = array("quantityOnHand", "lowQuantity");
	    		$menuItemRowsResult = CQ::getRowsWithRequiredColumns('inventory_items', $menuItemsColNameArr, $whereColumnArr, '');
	    		//print_r($menuItemRowsResult);
	    		if(!empty($menuItemRowsResult) && $itemQty == ''){
	    			if($menuItemRowsResult[0]['quantityOnHand'] <= $menuItemRowsResult[0]['lowQuantity']){
	    				$continue = "continue";
	    				$instock[] = $menuItemRowsResult[0]['quantityOnHand']-$menuItemRowsResult[0]['lowQuantity'];
	    			}
	    		}
	    		else if(!empty($menuItemRowsResult)){
	    			if($itemQty >= 1){
	    				if($menuItemRowsResult[0]['quantityOnHand'] - (($inv_val['quantityUsed'] * $itemQty) + $inv_val['reservation']) > 0){
	    					if(($menuItemRowsResult[0]['quantityOnHand'] - (($inv_val['quantityUsed'] * $itemQty) + $inv_val['reservation'])) < $menuItemRowsResult[0]['lowQuantity']){
	    						$continue = "continue";
	    						$instock[] = $menuItemRowsResult[0]['quantityOnHand']-$menuItemRowsResult[0]['lowQuantity'];
	    					}
	    				}
	    				else{
	    					$continue = "continue";
	    					$instock[] = $menuItemRowsResult[0]['quantityOnHand']-$menuItemRowsResult[0]['lowQuantity'];
	    				}
	    			}
	    			else{
	    				$continue = "continue";
	    				$instock[] = $menuItemRowsResult[0]['quantityOnHand']-$menuItemRowsResult[0]['lowQuantity'];
	    			}
	    		}
    		}
    	}
    	//return $continue;
    	return array('stock'=>$continue,'instock'=>min($instock));
    }

    protected static function getOptionalModifierInventory($optionalModifierId, $itemQty = '') {
    	$continue = "forward";
    	$whereInventoryColumnArr = array('modifierID' => $optionalModifierId, '_deleted' => 0);
    	$menuInventoryItemsColNameArr = array("inventoryItemID", "unitID");
    	$menuInventorytemRowsResult = CQ::getRowsWithRequiredColumns('modifier_86', $menuInventoryItemsColNameArr, $whereInventoryColumnArr, '');
    	if(!empty($menuInventorytemRowsResult)){
    		foreach ($menuInventorytemRowsResult as $inv_val){
	    		$whereColumnArr = array('id' => $inv_val['inventoryItemID'], '_deleted' => 0);
	    		$menuItemsColNameArr = array("quantityOnHand", "lowQuantity");
	    		$menuItemRowsResult = CQ::getRowsWithRequiredColumns('inventory_items', $menuItemsColNameArr, $whereColumnArr, '');
	    		if($itemQty == ''){
	    			if($menuItemRowsResult[0]['quantityOnHand'] <= $menuItemRowsResult[0]['lowQuantity']){
	    				$continue = "continue";
	    			}
	    		}
	    		else{
	    			if($itemQty >= 1){
	    				if($menuItemRowsResult[0]['quantityOnHand'] - (($inv_val['quantityUsed'] * $itemQty) + $inv_val['reservation']) > 0){
	    					if(($menuItemRowsResult[0]['quantityOnHand'] - (($inv_val['quantityUsed'] * $itemQty) + $inv_val['reservation'])) < $menuItemRowsResult[0]['lowQuantity']){
	    						$continue = "continue";
	    					}
	    				}
	    				else{
	    					$continue = "continue";
	    				}

	    			}
	    			else{
	    				$continue = "continue";
	    			}
	    		}
    		}
    	}
    	return $continue;
    }

    public static function action_getItemDetail($args, $inventoryMigrationStatus = array())
    {
    	//print_r($args);exit;
        $whereColumnArr = $args['menuId'];
        $whereColumnDefault = array('_deleted' => 0, 'ltg_display' => 1);
        $whereColumnArr = array_merge($whereColumnArr, $whereColumnDefault);
        $data_name = $args['dataname'];
        $itemQty = $args['itemQty'];
        $menuItemsColNameArr = array("id", "name", "price", "category_id",  "description", "nutrition","ingredients", "forced_modifier_group_id", "modifier_list_id", "dining_option", "image", "heart_healthy", "contains_gluten", "contains_dairy","track_86_count", "storage_key");
        $orderBy = array("_order"=>"Desc");
        $menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
        $i = 0;
        if(!empty($menuItemRowsResult)) {
            if (empty($inventoryMigrationStatus)) {
                $inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
            }
            foreach ($menuItemRowsResult as $val) {
            	$return = ItemsDataController::getItemInventory($menuItemRowsResult[$i]['id'], $itemQty);
            	//exit;
            	$menuItemRowsResult[$i]['availability'] = self::$inStock;
            	if($return['stock'] == "continue"){
            		$menuItemRowsResult[$i]['availability'] = self::$outStock;
            		//$i++;
            		//continue;
            	}
                $ingredientsVal = "";
               /* if ($menuItemRowsResult[$i]['ingredients'] != "") {
                    $ingredients = $menuItemRowsResult[$i]['ingredients'];
                    $ingredientsArr = explode(",", $ingredients);
                    foreach ($ingredientsArr as $ingVal) {
                        $id = explode("x", $ingVal);
                        $idArr[] = $id[0];
                    }
                    $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                    $ingredientsVal = "";
                    foreach ($menuItemIngredientsRowsResult as $menuIngVal) {
                        $ingredientsVal .= $menuIngVal['title'] . ",";
                    }
                    $ingredientsVal = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);
                }*/
				$ingredientsVal = ItemsDataController::getIngredientsById($val['id'], 'menu', $inventoryMigrationStatus);

				$menuItemImage = ItemsDataController::getMenuItemImage($menuItemRowsResult[$i]["storage_key"], $menuItemRowsResult[$i]["image"], $data_name);
				$menuItemRowsResult[$i]["image"] = $menuItemImage->image;
				$menuItemRowsResult[$i]["item_image_present"] = $menuItemImage->itemImagePresent;

                $menuItemRowsResult[$i]['ingredients'] = $ingredientsVal;
                $menuItemRowsResult[$i]['images'] = array("title" => "", "protien" => "", "thumbnail" => $menuItemRowsResult[$i]["image"],
                    "medium" => $menuItemRowsResult[$i]["image"]);
                if ($val['forced_modifier_group_id']) {
                    if (strtolower($val['forced_modifier_group_id']) != 'c' && strtolower(substr($val['forced_modifier_group_id'], 0, 1)) == "f") {
                        $modifierType = "i";
                        $whereColumnArr = array("id" => substr($val['forced_modifier_group_id'], 1), "_deleted" => 0);
                        $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
                        $forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr);
                        if (!empty($forcedModifierListRowResult)) {
                            $caption = "Choose your " . $forcedModifierListRowResult[0]['title'];
                            $forcedModifierListRowResult[0]['caption'] = $caption;
                            $forcedModifierColumnArr = array("id", "cost", "title", "ingredients", "nutrition","detour");
                            $orderBy = array("_order");
                            $whereColumnArr = array("list_id" => $forcedModifierListRowResult[0]['id'], "_deleted" => 0);
                            $orderBy = array("_order"=>"Asc");
                            $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                            if (!empty($forcedModifierRowResult)) {
                            	$outofstockmodifiers = 0;
                                foreach ($forcedModifierRowResult as $val) {
                                	if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])) {
                                        $return = ItemsDataController::getForcedModifierInventory($val['id'], $itemQty);
                                    	if($return['stock'] == "continue"){
                                    		//$i++;$outofstockmodifiers++;
                                    		$outofstockmodifiers++;
                                    			//continue;}
                                    	}
                                	}
                                        if ($val['id'] != "") {
                                            $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                                            $val['ingredients'] = $ingredientsStr;
                                            unset($menuItemIngredientsRowsResult);
                                        }

                                        $val['status'] = "";
										if($val['detour'] && $forcedModifierListRowResult[0]['type'] != "checklist"){
											$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
											$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
											$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
											$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
										}
										$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
										if(!empty($dlist) && $forcedModifierListRowResult[0]['type'] != "checklist"){
											$val['doption'] = $forcedModifierdListRowResult[0];
											$val['doption']['option'] = $dlist;
										}
                                       $forcedModifierRowArr[] = $val;
                                        unset($menuItemModifierNutritionArr);
                                    if($forcedModifierListRowResult[0]['type']=='choice' && (count($forcedModifierRowArr)>0 && count($forcedModifierRowArr)==$outofstockmodifiers)){
                                    	$menuItemRowsResult[$i]['availability'] = self::$outStock;
                                    }
                                }
                            }
                        }
                    } else if (strtolower(substr($val['forced_modifier_group_id'], 0, 1)) != "f" && strtolower($val['forced_modifier_group_id']) != "c") {
                        $modifierType = "C";
                        unset($forcedModifierListRowResult);
                        unset($forcedModifierGroupRowResult);
                        $whereColumnArr = array("id" => $val['forced_modifier_group_id'], "_deleted" => 0);
                        $forcedModifierListColumnArr = array("id", "title", "include_lists");
                        $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_groups', $forcedModifierListColumnArr, $whereColumnArr,'');
                        if (!empty($forcedModifierGroupRowResult)) {
                            $inlcList = explode("|", $forcedModifierGroupRowResult[0]['include_lists']);
                            foreach($inlcList as $grpVal) {
                                $whereColumnArr = array("id" => $grpVal, "_deleted" => 0);
                                $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
                                $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
                                if (!empty($forcedModifierGroupRowResult)) {
                                    $caption = "Choose your " . $forcedModifierGroupRowResult[0]['title'];
                                    $forcedModifierGroupRowResult[0] ['caption'] = $caption;
                                    $forcedModifierColumnArr = array("id", 'cost', "title", "ingredients", "nutrition","detour");
                                    $orderBy = array("_order");
                                    $whereColumnArr = array("list_id" => $forcedModifierGroupRowResult[0]['id'], "_deleted" => 0);
                                    $orderBy = array("_order"=>"Asc");
                                    $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                                    if (!empty($forcedModifierRowResult)) {
                                    	$outofstockmodifiers=0;
                                        foreach ($forcedModifierRowResult as $val) {
                                        	if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
                                                $return = ItemsDataController::getForcedModifierInventory($val['id'], $itemQty);
                                            	if($return['stock'] == "continue"){
                                            		//$i++;$outofstockmodifiers++;
                                            		$outofstockmodifiers++;
                                            			//continue;}
                                            	}
                                            }
                                            if ($val['id'] != "") {
                                                /*$ingredients = $val['ingredients'];
                                                $ingredientsArr = explode(",", $ingredients);
                                                foreach ($ingredientsArr as $ingredientVal) {
                                                    $id = explode("x", $ingredientVal);
                                                    $idArr[] = $id[0];
                                                }
                                                $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                                $ingredientsVal = "";
                                                foreach ($menuItemIngredientsRowsResult as $ingVal) {
                                                    $ingredientsVal .= $ingVal['title'] . ",";
                                                }
                                                $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                                $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                                                $val['ingredients'] = $ingredientsStr;
                                                unset($menuItemIngredientsRowsResult);
                                            }

                                            $val['status'] = "";
											if($val['detour'] && $forcedModifierListRowResult[0]['type'] != "checklist"){
												$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
												$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
												$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
												$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
											}
											$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
											if(!empty($dlist) && $forcedModifierListRowResult[0]['type'] != "checklist"){
												$val['doption'] = $forcedModifierdListRowResult[0];
												$val['doption']['option'] = $dlist;
											}
                                            $forcedModifierGroupRowResultArr[] = $val;
                                            unset($menuItemModifierNutritionArr);
                                        }
                                        if($forcedModifierGroupRowResult[0]['type']=='choice' && (count($forcedModifierGroupRowResultArr)>0 && count($forcedModifierGroupRowResultArr)==$outofstockmodifiers)){
                                        	$menuItemRowsResult[$i]['availability'] = self::$outStock;
                                        }
                                    }
                                }
                                $forcedModifierGroupRowResult[0]['option'] = $forcedModifierGroupRowResultArr;
                                $forcedModifierGroupRowArr[] = $forcedModifierGroupRowResult[0];
                                unset($forcedModifierGroupRowResultArr);
                            }
                            $forcedModifierRowArr = $forcedModifierGroupRowArr;
                        }
                    } else {
                        $modifierType = "G";
                        unset($forcedModifierListRowResult);
                        unset($forcedModifierGroupRowResult);
                        $forcedModifierGroupRowResultArr = array();
                        $whereColumnArr = array("id" => $val['category_id'], "_deleted" => 0);
                        $forcedModifierGroupColumnArr = array("forced_modifier_group_id");
                        $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('menu_categories', $forcedModifierGroupColumnArr, $whereColumnArr);
                        if(strtolower(substr($forcedModifierGroupRowResult[0]['forced_modifier_group_id'], 0, 1)) == "f") {
                            $modifierType = "i";
                            $whereColumnArr = array("id" =>substr($forcedModifierGroupRowResult[0]['forced_modifier_group_id'], 1), "_deleted" => 0);
                            $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
                            $forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
                            if (!empty($forcedModifierListRowResult)) {
                                $caption = "Choose your " . $forcedModifierListRowResult[0]['title'];
                                $forcedModifierListRowResult[0]['caption'] = $caption;
                                $forcedModifierColumnArr = array("id", "cost", "title", "ingredients", "nutrition","detour");
                                $orderBy = array("_order"=>"Asc");
                                $whereColumnArr = array("list_id" => $forcedModifierListRowResult[0]['id'], "_deleted" => 0);
                                $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                                if (!empty($forcedModifierRowResult)) {
                                	$outofstockmodifiers = 0;
                                    foreach ($forcedModifierRowResult as $val) {
                                    	if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
                                            $return = ItemsDataController::getForcedModifierInventory($val['id'], $itemQty);
                                        	if($return['stock'] == "continue"){
                                        		//$i++;$outofstockmodifiers++;
                                        		$outofstockmodifiers++;
                                        			//continue;}
                                        	}
                                        }
                                        if ($val['id'] != "") {
                                            /*$ingredients = $val['ingredients'];
                                            $ingredientsArr = explode(",", $ingredients);
                                            foreach ($ingredientsArr as $ingredientVal) {
                                                $id = explode("x", $ingredientVal);
                                                $idArr[] = $id[0];
                                            }
                                            $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                            $ingredientsVal = "";
                                            foreach ($menuItemIngredientsRowsResult as $ingrVal) {
                                                $ingredientsVal .= $ingrVal['title'] . ",";
                                            }
                                            $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                            $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                                            $val['ingredients'] = $ingredientsStr;
                                            unset($menuItemIngredientsRowsResult);
                                        }

                                        $val['status'] = "";
										if($val['detour'] && $forcedModifierListRowResult[0]['type'] != "checklist"){
											$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
											$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
											$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
											$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
										}
										$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
										if(!empty($dlist) && $forcedModifierListRowResult[0]['type'] != "checklist"){
											$val['doption'] = $forcedModifierdListRowResult[0];
											$val['doption']['option'] = $dlist;
										}
										$forcedModifierRowArr[] = $val;
                                        unset($menuItemModifierNutritionArr);
                                    }
                                    if($forcedModifierListRowResult[0]['type']=='choice' && (count($forcedModifierRowArr)>0 && count($forcedModifierRowArr)==$outofstockmodifiers)){
                                    	$menuItemRowsResult[$i]['availability'] = self::$outStock;
                                    }
                                }
                            }
                        } else{
                            $modifierType = "C";
                            $whereColumnArr = array("id" => $forcedModifierGroupRowResult[0]['forced_modifier_group_id'], "_deleted" => 0);
                            $forcedModifierListColumnArr = array("id", "title", "include_lists");
                            $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_groups', $forcedModifierListColumnArr, $whereColumnArr);
                            if (!empty($forcedModifierGroupRowResult)) {
                                $inlcList = explode("|", $forcedModifierGroupRowResult[0]['include_lists']);
                                foreach($inlcList as $grpVal) {
                                    $whereColumnArr = array("id" => $grpVal, "_deleted" => 0);
                                    $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
                                    $forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr);
                                    if (!empty($forcedModifierGroupRowResult)) {
                                        $caption = "Choose your " . $forcedModifierGroupRowResult[0]['title'];
                                        $forcedModifierGroupRowResult[0] ['caption'] = $caption;
                                        $forcedModifierColumnArr = array("id", 'cost', "title", "ingredients", "nutrition","detour");
                                        $orderBy = array("_order");
                                        $whereColumnArr = array("list_id" => $forcedModifierGroupRowResult[0]['id'], "_deleted" => 0);
                                        $orderBy = array("_order"=>"Asc");
                                        $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
                                        if (!empty($forcedModifierRowResult)) {
                                        		$outofstockmodifiers=0;
                                            foreach ($forcedModifierRowResult as $val) {
                                            	if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
                                                    $return = ItemsDataController::getForcedModifierInventory($val['id'], $itemQty);
                                                	if($return['stock'] == "continue"){
                                                		//$i++;$outofstockmodifiers++;
                                                		$outofstockmodifiers++;
                                                			//continue;}
                                                	}
                                                }
                                                if ($val['id'] != "") {
                                                    /*$ingredients = $val['ingredients'];
                                                    $ingredientsArr = explode(",", $ingredients);
                                                    foreach ($ingredientsArr as $ingredientVal) {
                                                        $id = explode("x", $ingredientVal);
                                                        $idArr[] = $id[0];
                                                    }
                                                    $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                                    $ingredientsVal = "";
                                                    foreach ($menuItemIngredientsRowsResult as $ingVal) {
                                                        $ingredientsVal .= $ingVal['title'] . ",";
                                                    }
                                                    $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                                    $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                                                    $val['ingredients'] = $ingredientsStr;
                                                    unset($menuItemIngredientsRowsResult);
                                                }

                                                $val['status'] = "";
												if($val['detour'] && $forcedModifierListRowResult[0]['type'] != "checklist"){
													$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
													$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
													$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
													$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
												}
												$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
												if(!empty($dlist) && $forcedModifierListRowResult[0]['type'] != "checklist"){
													$val['doption'] = $forcedModifierdListRowResult[0];
													$val['doption']['option'] = $dlist;
												}
                                                $forcedModifierGroupRowResultArr[] = $val;
                                                unset($menuItemModifierNutritionArr);
                                            }
                                            if($forcedModifierGroupRowResult[0]['type']=='choice' && (count($forcedModifierGroupRowResultArr)>0 && count($forcedModifierGroupRowResultArr)==$outofstockmodifiers)){
                                            	$menuItemRowsResult[$i]['availability'] = self::$outStock;
                                            }
                                        }
                                    }
                                    $forcedModifierGroupRowResult[0]['option'] = $forcedModifierGroupRowResultArr;
                                    $forcedModifierGroupRowArr[] = $forcedModifierGroupRowResult[0];
                                    unset($forcedModifierGroupRowResultArr);
                                }
                                $forcedModifierRowArr = $forcedModifierGroupRowArr;
                            }
                        }
                    }
                }
                $optionModifierRowArr = array();
                $modifier_list_id = $menuItemRowsResult[$i]['modifier_list_id'] ;
                if ($modifier_list_id == 0) {
                	$whereColumnArr = array("id" => $menuItemRowsResult[$i]['category_id'], "_deleted" => 0);
                	$optionalModifierGroupColumnArr = array("modifier_list_id");
                	$optionalModifierGroupRowResult = CQ::getRowsWithRequiredColumns('menu_categories', $optionalModifierGroupColumnArr, $whereColumnArr);
                	if ($optionalModifierGroupRowResult[0]['modifier_list_id'] != 0) {
                		$modifier_list_id = $optionalModifierGroupRowResult[0]['modifier_list_id'] ;
                	}
                }

                if ($modifier_list_id != 0) {
                    $whereColumnArr = array("id" => $modifier_list_id, "_deleted" => 0);
                    $optionModifierListColumnArr = array("id", "title");
                    $optionModifierListRowResult = CQ::getRowsWithRequiredColumns('modifier_categories', $optionModifierListColumnArr, $whereColumnArr);
                    if (!empty($optionModifierListRowResult)) {
                        $caption = "Choose your " . $optionModifierListRowResult[0]['title'];
                        $optionModifierListRowResult[0] ['caption'] = $caption;
                        $optionModifierColumnArr = array("id", "title", "cost", "ingredients", "nutrition");
                        $whereColumnArr = array("category" => $optionModifierListRowResult[0]['id'], "_deleted" => 0);
                        $optionModifierRowResult = CQ::getRowsWithRequiredColumns('modifiers', $optionModifierColumnArr, $whereColumnArr, '', '', '', self::$fieldExpression);
                        foreach ($optionModifierRowResult as $val) {
                        	$return = self::getOptionalModifierInventory($optionModifierRowResult[$i]['id'], $itemQty);
                        	if($return == "continue"){
                        		$i++;
                        		continue;
                        	}
                            if ($val['id'] != "") {
                               /* $ingredients = $val['ingredients'];
                                $ingredientsArr = explode(",", $ingredients);
                                foreach ($ingredientsArr as $ingredientVal) {
                                    $id = explode("x", $ingredientVal);
                                    $idArr[] = $id[0];
                                }
                                $menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
                                $ingredientsVal = "";
                                foreach ($menuItemIngredientsRowsResult as $ingVal) {
                                    $ingredientsVal .= $ingVal['title'] . ",";
                                }
                                $ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);*/
                                $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'm', $inventoryMigrationStatus);
                                $val['ingredients'] = $ingredientsStr;
                                unset($menuItemIngredientsRowsResult);
                            }

                            $val['status'] = "";
							if($val['detour'] && $forcedModifierListRowResult[0]['type'] != "checklist"){
								$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
								$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
								$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
								$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
							}
							$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
							if(!empty($dlist) && $forcedModifierListRowResult[0]['type'] != "checklist"){
								$val['doption'] = $forcedModifierdListRowResult[0];
								$val['doption']['option'] = $dlist;
							}
							$val["type"] = "checklist";
                            $optionModifierRowArr[] = $val;
                            unset($menuItemModifierNutritionArr);
                        }
                    }
                }
                if (!empty($forcedModifierListRowResult)) {
                    $forcedModifierListRowResultArr = $forcedModifierListRowResult[0];
                } else {
                    $forcedModifierListRowResultArr = array();
                }
                if (!empty($forcedModifierRowArr) && $modifierType == "G") {
                    $forcedModifierListRowResultArr = $forcedModifierRowArr;
                    $menuItemRowsResult[$i]['forced'] = $forcedModifierListRowResultArr;
                } elseif (!empty($forcedModifierRowArr) && $modifierType != "G") {
                    $forcedModifierListRowResultArr['option'] = $forcedModifierRowArr;
                    $menuItemRowsResult[$i]['forced'][] = $forcedModifierListRowResultArr;
                } else {
                    $menuItemRowsResult[$i]['forced'] = array();
                }

                unset($forcedModifierRowArr);


                if (!empty($optionModifierListRowResult)) {
                    $optionalModifierListRowResultArr = $optionModifierListRowResult[0];
                } else {
                    $optionalModifierListRowResultArr = array();
                }
                if (!empty($optionModifierRowArr)) {
                    $optionalModifierListRowResultArr['option'] = $optionModifierRowArr;
                    $menuItemRowsResult[$i]['optional'][] = $optionalModifierListRowResultArr;
                } else {
                    $menuItemRowsResult[$i]['optional'] = array();
                }
                $i++;
            }
            $resultArr = array("Items" => $menuItemRowsResult);
        } else {
            $resultArr = array("Items" => array());
        }
        return $resultArr;
    }

    protected function action_getFirstGroupCategory($args) {
        $menuGroupColNameArr = array("id");
        $whereGroupColumnArr = array("_deleted" => 0);
        $orderBy = array("id"=>"ASC");
        $optionGroupListRowResult = CQ::getRowsWithRequiredColumns('menu_groups', $menuGroupColNameArr, $whereGroupColumnArr, $orderBy);
        $menuCategoriesNameArr = array("id");
        $whereCategoriesColumnArr = array("group_id" => $optionGroupListRowResult[0]['id'], "_deleted" => 0);
        $orderBy = array("id"=>"ASC");
        $optionCategoryListRowResult = CQ::getRowsWithRequiredColumns('menu_categories', $menuCategoriesNameArr, $whereCategoriesColumnArr, $orderBy);
        $resultArr = array("group_id" => $optionGroupListRowResult[0]['id'], 'category_id' => $optionCategoryListRowResult[0][id]);
        return $resultArr;
    }

    public static function pullForcedModifiersDetail($arr, $inventoryMigrationStatus = array()){
		$result = array();
		if($arr['id'] != ""){
			if(strtolower(substr($arr['id'], 0, 1)) == 'g'){
				$result[] = ItemsDataController::getGropuModifiersWithDetour(substr($arr['id'],1), $inventoryMigrationStatus);
			} else {
				$forcedModifierColumnArr = array("id", "list_id", "cost", "title", "ingredients", "nutrition", "detour");
				$orderBy = array("_order"=>"Asc");
				$whereColumnArr = array("list_id" => substr($arr['id'],1), "_deleted" => 0);
				$forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
				if (!empty($forcedModifierRowResult)) {
					if (empty($inventoryMigrationStatus)) {
						$inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
					}
					foreach ($forcedModifierRowResult as $val) {
						$return = ItemsDataController::getForcedModifierInventory($val['id']);
						if($return['stock'] != "continue"){
							$forcedModifierStr = "";
							if($val['nutrition'])
							{
								$forcedModifier = explode(",", $val['nutrition']);
								foreach($forcedModifier as $fmnutVal)
								{
									if(substr($fmnutVal,-1) == "1")
									{
										$fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
										$forcedModifierStr .= $fmnutVals.",";
									}
								}
							}
							$val['nutrition'] = rtrim($forcedModifierStr,',');
							if ($val['id'] != "") {
								$ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
								$val['ingredients'] = $ingredientsStr;
								unset($menuItemIngredientsRowsResult);
							}
							$val['status'] = "";
							// checking and adding detour forced modifiers options
							if($val['detour'] !='' &&  $arr['id']!=$val['detour']){
								$prevDetourType = $forcedModifierListRowResult[0]['type'];

								if(strtolower(substr($val['detour'], 0, 1)) == 'g'){
									if(in_array($val['detour'],self::$modifyArr))
									{
										return;
									}
									self::$modifyArr[] = $val['detour'];
									//echo $val['detour'];
									$groupModInfo = ItemsDataController::getGropuModifiersWithDetour(substr($val['detour'],1), $inventoryMigrationStatus);
									//print_r($groupModInfo);
									$newfinfo = array();
									foreach($groupModInfo as $key=>$value){
										$newfinfo = array();
										foreach($value['option'] as $k=>$finfo){
											if($finfo['detour']){
												$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $finfo['detour']));
												if(strtolower(substr($finfo['detour'], 0, 1))!='g'){
													$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
													$whereListColumnArr = array("id" => substr($finfo['detour'],1), "_deleted" => 0);
													$forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereListColumnArr,'');
													$forcedModifierListRowResult[0]['caption'] =  "Choose your ".$forcedModifierListRowResult[0]['title'];

													$forcedModifierPrevListColumnArr = array("type");
													$whereListPrevColumnArr = array("id" => $forcedModifierRowResult[0]['list_id'], "_deleted" => 0);
													$forcedModifierPrevListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierPrevListColumnArr, $whereListPrevColumnArr,'');

													if(!empty($dlist) && $forcedModifierPrevListRowResult[0]['type'] != "checklist"){
														$dopt_n = array_merge($forcedModifierListRowResult[0],array('option'=>$dlist));
													}
													$finfo['doption'][] = $dopt_n;
													$value['option'][$k] = $finfo;
												}
												else
												{
													$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
													$whereListColumnArr = array("id" => substr($finfo['detour'],1), "_deleted" => 0);
													$forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereListColumnArr,'');

													//$forcedModifierListRowResult[0]['caption'] =  "Choose your ssuppi".$forcedModifierListRowResult[0]['title'];

													$forcedModifierPrevListColumnArr = array("type");
													$whereListPrevColumnArr = array("id" => $forcedModifierRowResult[0]['list_id'], "_deleted" => 0);
													$forcedModifierPrevListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierPrevListColumnArr, $whereListPrevColumnArr,'');


													$finfo['doption'] = $dlist[0];
													$value['option'][$k] = $finfo;
												}
											}
											else{
												$value['option'][$k] = $finfo;
											}
										}

										$groupModInfo[$key] = $value;
									}

									$val['doption']= $groupModInfo;
								}
								else{
									if(in_array($val['detour'],self::$modifyArr))
									{
										return;
									}
									self::$modifyArr[] = $val['detour'];
									$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']));
									if($val['detour']){
										$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
										$whereListColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
										$forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereListColumnArr,'');
										$forcedModifierListRowResult[0]['caption'] =  "Choose your ".$forcedModifierListRowResult[0]['title'];
									}
									$forcedModifierPrevListColumnArr = array("type");
									$whereListPrevColumnArr = array("id" => $forcedModifierRowResult[0]['list_id'], "_deleted" => 0);
									$forcedModifierPrevListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierPrevListColumnArr, $whereListPrevColumnArr,'');

									if(!empty($dlist) && $forcedModifierPrevListRowResult[0]['type'] != "checklist"){
										$dopt_n = array_merge($forcedModifierListRowResult[0],array('option'=>$dlist));
										$val['doption'][]= $dopt_n;
									}
								}
							}
							$result[] = $val;
						}
					}
				}
			}
		}
		return $result;
	}

	public static function action_getItemDetailHistory($args) {
		$whereColumnArr = $args['menuId'];
		$whereColumnDefault = array('_deleted' => 0, 'ltg_display' => 1);
		$whereColumnArr = array_merge($whereColumnArr, $whereColumnDefault);
		$data_name = $args['dataname'];
		$itemQty = $args['itemQty'];
		$menuItemsColNameArr = array("id", "name", "price", "category_id", "description", "nutrition", "ingredients", "forced_modifier_group_id", "modifier_list_id", "dining_option", "image", "heart_healthy", "contains_gluten", "contains_dairy", "track_86_count", "storage_key");
		$orderBy = array("_order" => "Desc");
		$menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
		$i = 0;
		if (!empty($menuItemRowsResult)) {
			$inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
			foreach ($menuItemRowsResult as $val) {
				$return = ItemsDataController::getItemInventory($menuItemRowsResult[$i]['id'], $itemQty);
				$menuItemRowsResult[$i]['availability'] = self::$inStock;
				if ($return['stock'] == "continue") {
					$menuItemRowsResult[$i]['availability'] = self::$outStock;
				}
				$ingredientsVal = "";
				$ingredientsVal = ItemsDataController::getIngredientsById($val['id'], 'menu', $inventoryMigrationStatus);

				$menuItemImage = ItemsDataController::getMenuItemImage($menuItemRowsResult[$i]["storage_key"], $menuItemRowsResult[$i]["image"], $data_name);
				$menuItemRowsResult[$i]["image"] = $menuItemImage->image;
				$menuItemRowsResult[$i]["item_image_present"] = $menuItemImage->itemImagePresent;

				$menuItemRowsResult[$i]['ingredients'] = $ingredientsVal;
				$menuItemRowsResult[$i]['images'] = array(
					"title" => "", "protien" => "", "thumbnail" => $menuItemRowsResult[$i]["image"],
					"medium" => $menuItemRowsResult[$i]["image"]
				);
				if ($val['forced_modifier_group_id']) {
					if (strtolower($val['forced_modifier_group_id']) != 'c' && strtolower(substr($val['forced_modifier_group_id'], 0, 1)) == "f") {
						$modifierType = "i";
						$whereColumnArr = array("id" => substr($val['forced_modifier_group_id'], 1), "_deleted" => 0);
						$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
						$forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr);
						if (!empty($forcedModifierListRowResult)) {
							$caption = "Choose your " . $forcedModifierListRowResult[0]['title'];
							$forcedModifierListRowResult[0]['caption'] = $caption;
							$forcedModifierColumnArr = array("id", "cost", "title", "ingredients", "nutrition", "detour");
							$orderBy = array("_order");
							$ingredientsArr = explode(",", $args['fId']);
							foreach ($ingredientsArr as $ingredientVal) {
								$id = explode("x", $ingredientVal);
								$idArr[] = $id[0];
							}
							$orderBy = array("_order" => "Asc");
							$forcedModifierRowResult = CQ::getRowsWhereColumnIn('forced_modifiers', 'id', $idArr);
							if (!empty($forcedModifierRowResult)) {
								$outofstockmodifiers = 0;
								foreach ($forcedModifierRowResult as $val) {
									if (isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])) {
										$return = ItemsDataController::getForcedModifierInventory($val['id'], $itemQty);
										if ($return['stock'] == "continue") {
											$outofstockmodifiers++;
										}
									}
									if ($val['id'] != "") {
										$ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
										$val['ingredients'] = $ingredientsStr;
										unset($menuItemIngredientsRowsResult);
									}
									$val['isChecked'] = "true";
									$val['status'] = "";
									if (!empty($dlist)) {
										$val['doption'] = $dlist;
									}
									$forcedModifierRowArr[] = $val;
									unset($menuItemModifierNutritionArr);
								}

								if ($forcedModifierListRowResult[0]['type'] == 'choice' && (count($forcedModifierRowArr) > 0 && count($forcedModifierRowArr) == $outofstockmodifiers)) {
									$menuItemRowsResult[$i]['availability'] = self::$outStock;
								}
							}
						}
					} else if (strtolower(substr($val['forced_modifier_group_id'], 0, 1)) != "f" && strtolower($val['forced_modifier_group_id']) != "c") {
						$modifierType = "C";
						unset($forcedModifierListRowResult);
						unset($forcedModifierGroupRowResult);
						$whereColumnArr = array("id" => $val['forced_modifier_group_id'], "_deleted" => 0);
						$forcedModifierListColumnArr = array("id", "title", "include_lists");
						$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_groups', $forcedModifierListColumnArr, $whereColumnArr, '');
						if (!empty($forcedModifierGroupRowResult)) {
							$inlcList = explode("|", $forcedModifierGroupRowResult[0]['include_lists']);
							foreach ($inlcList as $grpVal) {
								$whereColumnArr = array("id" => $grpVal, "_deleted" => 0);
								$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
								$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr, '');
								if (!empty($forcedModifierGroupRowResult)) {
									$caption = "Choose your " . $forcedModifierGroupRowResult[0]['title'];
									$forcedModifierGroupRowResult[0]['caption'] = $caption;
									$forcedModifierColumnArr = array("id", 'cost', "title", "ingredients", "nutrition", "detour");
									$ingredientsArr = explode(",", $args['fId']);
									foreach ($ingredientsArr as $ingredientVal) {
										$id = explode("x", $ingredientVal);
										$idArr[] = $id[0];
									}
									$orderBy = array("_order" => "Asc");
									$forcedModifierRowResult = CQ::getRowsWhereColumnIn('forced_modifiers', 'id', $idArr);

									if (!empty($forcedModifierRowResult)) {
										$outofstockmodifiers = 0;
										foreach ($forcedModifierRowResult as $val) {
											if (isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])) {
												$return = ItemsDataController::getForcedModifierInventory($val['id'], $itemQty);
												if ($return['stock'] == "continue") {
													$outofstockmodifiers++;
												}
											}
											if ($val['id'] != "") {
												$ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
												$val['ingredients'] = $ingredientsStr;
												unset($menuItemIngredientsRowsResult);
											}
											$val['isChecked'] = "true";
											$val['status'] = "";
											if (!empty($dlist)) {
												$val['doption'] = $dlist;
											}
											$forcedModifierGroupRowResultArr[] = $val;
											unset($menuItemModifierNutritionArr);
										}
										if ($forcedModifierGroupRowResult[0]['type'] == 'choice' && (count($forcedModifierGroupRowResultArr) > 0 && count($forcedModifierGroupRowResultArr) == $outofstockmodifiers)) {
											$menuItemRowsResult[$i]['availability'] = self::$outStock;
										}
									}
								}
								$forcedModifierGroupRowResult[0]['option'] = $forcedModifierGroupRowResultArr;
								$forcedModifierGroupRowArr[] = $forcedModifierGroupRowResult[0];
								unset($forcedModifierGroupRowResultArr);
							}
							$forcedModifierRowArr = $forcedModifierGroupRowArr;
						}
					} else {
						$modifierType = "G";
						unset($forcedModifierListRowResult);
						unset($forcedModifierGroupRowResult);
						$forcedModifierGroupRowResultArr = array();
						$whereColumnArr = array("id" => $val['category_id'], "_deleted" => 0);
						$forcedModifierGroupColumnArr = array("forced_modifier_group_id");
						$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('menu_categories', $forcedModifierGroupColumnArr, $whereColumnArr);
						if (strtolower(substr($forcedModifierGroupRowResult[0]['forced_modifier_group_id'], 0, 1)) == "f") {
							$modifierType = "i";
							$whereColumnArr = array("id" => substr($forcedModifierGroupRowResult[0]['forced_modifier_group_id'], 1), "_deleted" => 0);
							$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
							$forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr, '');
							if (!empty($forcedModifierListRowResult)) {
								$caption = "Choose your " . $forcedModifierListRowResult[0]['title'];
								$forcedModifierListRowResult[0]['caption'] = $caption;
								$ingredientsArr = explode(",", $args['fId']);
								foreach ($ingredientsArr as $ingredientVal) {
									$id = explode("x", $ingredientVal);
									$idArr[] = $id[0];
								}
								$orderBy = array("_order" => "Asc");
								$forcedModifierRowResult = CQ::getRowsWhereColumnIn('forced_modifiers', 'id', $idArr);

								if (!empty($forcedModifierRowResult)) {
									$outofstockmodifiers = 0;
									foreach ($forcedModifierRowResult as $val) {
										if (isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])) {
											$return = ItemsDataController::getForcedModifierInventory($val['id'], $itemQty);
											if ($return['stock'] == "continue") {
        										//$i++;$outofstockmodifiers++;
												$outofstockmodifiers++;
        											//continue;}
											}
										}
										if ($val['id'] != "") {
											$ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
											$val['ingredients'] = $ingredientsStr;
											unset($menuItemIngredientsRowsResult);
										}
										$val['isChecked'] = "true";
										$val['status'] = "";
										if (!empty($dlist)) {
											$val['doption'] = $dlist;
										}
										$forcedModifierRowArr[] = $val;
										unset($menuItemModifierNutritionArr);
									}
									if ($forcedModifierListRowResult[0]['type'] == 'choice' && (count($forcedModifierRowArr) > 0 && count($forcedModifierRowArr) == $outofstockmodifiers)) {
										$menuItemRowsResult[$i]['availability'] = self::$outStock;
									}
								}
							}
						} else {
							$modifierType = "C";
							$whereColumnArr = array("id" => $forcedModifierGroupRowResult[0]['forced_modifier_group_id'], "_deleted" => 0);
							$forcedModifierListColumnArr = array("id", "title", "include_lists");
							$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_groups', $forcedModifierListColumnArr, $whereColumnArr);
							if (!empty($forcedModifierGroupRowResult)) {
								$inlcList = explode("|", $forcedModifierGroupRowResult[0]['include_lists']);
								foreach ($inlcList as $grpVal) {
									$whereColumnArr = array("id" => $grpVal, "_deleted" => 0);
									$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
									$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr);
									if (!empty($forcedModifierGroupRowResult)) {
										$caption = "Choose your " . $forcedModifierGroupRowResult[0]['title'];
										$forcedModifierGroupRowResult[0]['caption'] = $caption;
										$ingredientsArr = explode(",", $args['fId']);
										$forcedModifierColumnArr = array("id", 'cost', "title", "ingredients", "nutrition", "detour");
										$orderBy = array("_order");
										$whereColumnArr = array("id" => $args['fId'], "_deleted" => 0);
										$orderBy = array("_order" => "Asc");
										$forcedModifierRowResult = CQ::getRowsWhereColumnIn('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);

										foreach ($ingredientsArr as $ingredientVal) {
											$id = explode("x", $ingredientVal);
											$idArr[] = $id[0];
										}
										$orderBy = array("_order" => "Asc");
										$forcedModifierRowResult = CQ::getRowsWhereColumnIn('forced_modifiers', 'id', $idArr);

										if (!empty($forcedModifierRowResult)) {
											$outofstockmodifiers = 0;
											foreach ($forcedModifierRowResult as $val) {
												if (isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])) {
													$return = ItemsDataController::getForcedModifierInventory($val['id'], $itemQty);
													if ($return['stock'] == "continue") {
														$outofstockmodifiers++;
													}
												}
												if ($val['id'] != "") {
													$ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
													$val['ingredients'] = $ingredientsStr;
													unset($menuItemIngredientsRowsResult);
												}
												$val['isChecked'] = "true";
												$val['status'] = "";
												if (!empty($dlist)) {
													$val['doption'] = $dlist;
												}
												$forcedModifierGroupRowResultArr[] = $val;
												unset($menuItemModifierNutritionArr);
											}
											if ($forcedModifierGroupRowResult[0]['type'] == 'choice' && (count($forcedModifierGroupRowResultArr) > 0 && count($forcedModifierGroupRowResultArr) == $outofstockmodifiers)) {
												$menuItemRowsResult[$i]['availability'] = self::$outStock;
											}
										}
									}
									$forcedModifierGroupRowResult[0]['option'] = $forcedModifierGroupRowResultArr;
									$forcedModifierGroupRowArr[] = $forcedModifierGroupRowResult[0];
									unset($forcedModifierGroupRowResultArr);
								}
								$forcedModifierRowArr = $forcedModifierGroupRowArr;
							}
						}
					}
				}
				$optionModifierRowArr = array();
				if ($menuItemRowsResult[$i]['modifier_list_id'] != 0) {
					$whereColumnArr = array("id" => $menuItemRowsResult[$i]['modifier_list_id'], "_deleted" => 0);
					$optionModifierListColumnArr = array("id", "title");
					$optionModifierListRowResult = CQ::getRowsWithRequiredColumns('modifier_categories', $optionModifierListColumnArr, $whereColumnArr);
					if (!empty($optionModifierListRowResult)) {
						$caption = "Choose your " . $optionModifierListRowResult[0]['title'];
						$optionModifierListRowResult[0]['caption'] = $caption;
						$optionModifierColumnArr = array("id", "title", "cost", "ingredients", "nutrition");
						$whereColumnArr = array("id" => $args['mId'], "_deleted" => 0);
						$orderBy = array("_order" => "Asc");
						$optionalModfiersiddArr = explode(",", $args['mId']);
						$optionModifierRowResult = CQ::getRowsWhereColumnIn('modifiers', 'id', $optionalModfiersiddArr);
						foreach ($optionModifierRowResult as $val) {
							$return = self::getOptionalModifierInventory($val['id'], $itemQty);
							if ($return == "continue") {
								$menuItemRowsResult[$i]['availability'] = self::$outStock;
							}
							if ($val['id'] != "") {
								$ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'm', $inventoryMigrationStatus);
								$val['ingredients'] = $ingredientsStr;
								unset($menuItemIngredientsRowsResult);
							}
							$val['isChecked'] = "true";
							$val['status'] = "";
							if (!empty($dlist)) {
								$val['doption'] = $dlist;
							}
							$val["type"] = "checklist";
							$optionModifierRowArr[] = $val;
							unset($menuItemModifierNutritionArr);
						}
					}
				}
				if (!empty($forcedModifierListRowResult)) {
					$forcedModifierListRowResultArr = $forcedModifierListRowResult[0];
				} else {
					$forcedModifierListRowResultArr = array();
				}
				if (!empty($forcedModifierRowArr) && $modifierType == "G") {
					$forcedModifierListRowResultArr = $forcedModifierRowArr;
					$menuItemRowsResult[$i]['forced'] = $forcedModifierListRowResultArr;
				} elseif (!empty($forcedModifierRowArr) && $modifierType != "G") {
					$forcedModifierListRowResultArr['option'] = $forcedModifierRowArr;
					$menuItemRowsResult[$i]['forced'][] = $forcedModifierListRowResultArr;
				} else {
					$menuItemRowsResult[$i]['forced'] = array();
				}

				unset($forcedModifierRowArr);


				if (!empty($optionModifierListRowResult)) {
					$optionalModifierListRowResultArr = $optionModifierListRowResult[0];
				} else {
					$optionalModifierListRowResultArr = array();
				}
				if (!empty($optionModifierRowArr)) {
					$optionalModifierListRowResultArr['option'] = $optionModifierRowArr;
					$menuItemRowsResult[$i]['optional'][] = $optionalModifierListRowResultArr;
				} else {
					$menuItemRowsResult[$i]['optional'] = array();
				}
				$i++;
			}
			$resultArr = array("Items" => $menuItemRowsResult);
		} else {
			$resultArr = array("Items" => array());
		}
		return $resultArr;
	}

    /*
     * get Ingradients from inventory
     */
    public static function getIngredientsById($id, $type, $inventoryMigrationStatus){
    	if(!empty($inventoryMigrationStatus) && $inventoryMigrationStatus[0]['value']!='Legacy'){
	    	if($type=='menu'){
	    		$query = "select group_concat(invitems.name) as datastr from inventory_items as invitems join menuitem_86 as m86 on invitems.id=m86.inventoryItemID where m86.menuItemID=".$id." and m86._deleted=0 and invitems._deleted=0";
	    	}
	    	else if($type=='fm'){
	    		$query = "select group_concat(invitems.name) as datastr from inventory_items as invitems join forced_modifier_86 as f86 on invitems.id=f86.inventoryItemID where f86.forced_modifierID=".$id." and f86._deleted=0 and invitems._deleted=0";
	    	}
	    	else if($type == 'm'){
	    		$query = "select group_concat(invitems.name) as datastr from inventory_items as invitems join modifier_86 as m86 on invitems.id=m86.inventoryItemID where m86.modifierID=".$id." and m86._deleted=0 and invitems._deleted=0";
	    	}
	    	$ingradientsQry = DBConnection::clientQuery($query);
	    	$result =  MA::resultToArray($ingradientsQry);
	    	return $result[0]['datastr'];
    	}
    	else{
    		$ingredientsStr ="";
    		if($type=='menu'){
    			$menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', array('ingredients'), array('id'=>$id), '', '');
    			$idArr = array();
    			if ($menuItemRowsResult[0]['ingredients'] != "") {
    				$ingredients = $menuItemRowsResult[0]['ingredients'];
    				$ingredientsArr = explode(",", $ingredients);
    				foreach ($ingredientsArr as $ingVal) {
    					$id = explode("x", $ingVal);
    					$idArr[] = $id[0];
    				}
    				$menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
    				$ingredientsVal = "";
    				foreach ($menuItemIngredientsRowsResult as $menuIngval) {
    					$ingredientsVal .= $menuIngval['title'] . ",";
    				}
    				$ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);
    			}
    		}
    		else if($type=='fm'){
    			$whereColumnArr = array("id" => $id, "_deleted" => 0);
    			$forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', array('ingredients'), $whereColumnArr, '');
    			$ingredients = $forcedModifierRowResult[0]['ingredients'];
    			$ingredientsArr = explode(",", $ingredients);
    			foreach ($ingredientsArr as $ingredientVal) {
    				$id = explode("x", $ingredientVal);
    				$idArr[] = $id[0];
    			}
    			$menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
    			$ingredientsVal = "";
    			foreach ($menuItemIngredientsRowsResult as $ingrVal) {
    				$ingredientsVal .= $ingrVal['title'] . ",";
    			}
    			$ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);
    		}
    		else if($type == 'm'){
    			$whereColumnArr = array("id" => $id, "_deleted" => 0);
    			$optionModifierRowResult = CQ::getRowsWithRequiredColumns('modifiers', array('ingredients'), $whereColumnArr, '');
    			$ingredients = $optionModifierRowResult[0]['ingredients'];
    			$ingredientsArr = explode(",", $ingredients);
    			foreach ($ingredientsArr as $ingredientVal) {
    			 	$id = explode("x", $ingredientVal);
    			 	$idArr[] = $id[0];
    			}
    			$menuItemIngredientsRowsResult = CQ::getRowsWhereColumnIn('ingredients', 'id', $idArr);
    			$ingredientsVal = "";
    			foreach ($menuItemIngredientsRowsResult as $ingVal) {
    				$ingredientsVal .= $ingVal['title'] . ",";
    			}
    			$ingredientsStr = substr($ingredientsVal, 0, strlen($ingredientsVal) - 1);
    		}
    		return $ingredientsStr;
    	}

    }

    public function get_group_modifiers($grp_id, $inventoryMigrationStatus = array())
    {
    	$whereColumnArr = array("id" => $grp_id, "_deleted" => 0);
    	$forcedModifierListColumnArr = array("id", "title", "include_lists");
    	$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_groups', $forcedModifierListColumnArr, $whereColumnArr,'');
    	if (!empty($forcedModifierGroupRowResult)) {
    		if (empty($inventoryMigrationStatus)) {
    			$inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
    		}
    		$outofstockmodifiers = 0;
    		$inlcList = explode("|", $forcedModifierGroupRowResult[0]['include_lists']);
    		foreach($inlcList as $grpVal) {
    			$whereColumnArr = array("id" => $grpVal, "_deleted" => 0);
    			$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
    			$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
    			if (!empty($forcedModifierGroupRowResult)) {
    				$caption = "Choose your " . $forcedModifierGroupRowResult[0]['title'];
    				$forcedModifierGroupRowResult[0] ['caption'] = $caption;
    				$forcedModifierColumnArr = array("id", 'cost', "title", "ingredients", "nutrition","detour");
    				$orderBy = array("_order");
    				$whereColumnArr = array("list_id" => $forcedModifierGroupRowResult[0]['id'], "_deleted" => 0);
    				$orderBy = array("_order"=>"Asc");
    				$forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '' , self::$fieldExpression);
    				if (!empty($forcedModifierRowResult)) {
    					foreach ($forcedModifierRowResult as $val) {

    						if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
    							$return = ItemsDataController::getForcedModifierInventory($val['id']);
    							if($return == "continue"){
    								$outofstockmodifiers++;
    							}
    						}
    						$forcedModifierStr = "";
    						if($val['nutrition'])
    						{
    							$forcedModifier = explode(",", $val['nutrition']);
    							foreach($forcedModifier as $fmnutVal)
    							{
    								if(substr($fmnutVal,-1) == "1")
    								{
    									$fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
    									$forcedModifierStr .= $fmnutVals.",";
    								}
    							}
    						}
    						$val['nutrition'] = rtrim($forcedModifierStr,',');
    						if ($val['id'] != "") {
    							$ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
    							$val['ingredients'] = $ingredientsStr;
    							unset($menuItemIngredientsRowsResult);
    						}

    						$val['status'] = "";
							if(in_array($val['detour'],self::$modifyArr))
    						{
    							return ;
    						}
    						self::$modifyArr[] = $val['detour'];

    						if(strtolower(substr($val['detour'], 0, 1)) == "g" && $forcedModifierGroupRowResult[0]['type'] != "checklist")
    						{
    							$val['doption'] = self::get_group_modifiers(substr($val['detour'],1), $inventoryMigrationStatus);
    						}
    						else
    						{
    							if($val['detour'] && $forcedModifierGroupRowResult[0]['type'] != "checklist"){

    								$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
    								$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
    								$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
    								$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
    							}
    							$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
    							if(!empty($dlist) && $forcedModifierGroupRowResult[0]['type'] != "checklist"){
    								$dopt_n = array_merge($forcedModifierdListRowResult[0],array('option'=>$dlist));
    								$val['doption'][]= $dopt_n;
    							}
    						}
    						$forcedModifierGroupRowResultArr[] = $val;
    						unset($menuItemModifierNutritionArr);
    					}

    				}
    				if($forcedModifierGroupRowResult[0]['type']=='choice' && (count($forcedModifierGroupRowResultArr)>0 && count($forcedModifierGroupRowResultArr)==$outofstockmodifiers)){
    					$menuItemRowsResult[$i]['availability'] = self::$outStock;
    				}
    			}
    			$forcedModifierGroupRowResult[0]['option'] = $forcedModifierGroupRowResultArr;
    			$forcedModifierGroupRowArr[] = $forcedModifierGroupRowResult[0];
    			unset($forcedModifierGroupRowResultArr);
    		}
    		$forcedModifierRowArr = $forcedModifierGroupRowArr;
    		return $forcedModifierGroupRowArr;
    	}

    }


    public function getGropuModifiersWithDetour($grp_id, $inventoryMigrationStatus = array())
    {
    	$whereColumnArr = array("id" => $grp_id, "_deleted" => 0);
    	$forcedModifierListColumnArr = array("id", "title", "include_lists");
    	$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_groups', $forcedModifierListColumnArr, $whereColumnArr,'');
    	if (!empty($forcedModifierGroupRowResult)) {
    		if (empty($inventoryMigrationStatus)) {
    			$inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
    		}
    		$outofstockmodifiers = 0;
    		$inlcList = explode("|", $forcedModifierGroupRowResult[0]['include_lists']);
    		$resultArr = array();
    		foreach($inlcList as $grpVal) {
    			$whereColumnArr = array("id" => $grpVal, "_deleted" => 0);
    			$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
    			$forcedModifierGroupRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
    			if (!empty($forcedModifierGroupRowResult)) {
    				$caption = "Choose your " . $forcedModifierGroupRowResult[0]['title'];
    				$forcedModifierGroupRowResult[0] ['caption'] = $caption;
    				$forcedModifierColumnArr = array("id", 'cost', "title", "ingredients", "nutrition","detour");
    				$orderBy = array("_order");
    				$whereColumnArr = array("list_id" => $forcedModifierGroupRowResult[0]['id'], "_deleted" => 0);
    				$orderBy = array("_order"=>"Asc");
    				$forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);
    				if (!empty($forcedModifierRowResult)) {
    					foreach ($forcedModifierRowResult as $val) {
    						if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
    							$return = ItemsDataController::getForcedModifierInventory($val['id']);
    							if($return == "continue"){
    								$outofstockmodifiers++;
    							}
    						}
    						$forcedModifierStr = "";
    						if($val['nutrition'])
    						{
    							$forcedModifier = explode(",", $val['nutrition']);
    							foreach($forcedModifier as $fmnutVal)
    							{
    								if(substr($fmnutVal,-1) == "1")
    								{
    									$fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
    									$forcedModifierStr .= $fmnutVals.",";
    								}
    							}
    						}
    						$val['nutrition'] = rtrim($forcedModifierStr,',');
    						// LP-4364 END
    						if ($val['id'] != "") {
    							$ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
    							$val['ingredients'] = $ingredientsStr;
    							unset($menuItemIngredientsRowsResult);
    						}

    						$forcedModifierGroupRowResultArr[] = $val;
    						unset($menuItemModifierNutritionArr);
    					}

    				}
    				if($forcedModifierGroupRowResult[0]['type']=='choice' && (count($forcedModifierGroupRowResultArr)>0 && count($forcedModifierGroupRowResultArr)==$outofstockmodifiers)){
    					$menuItemRowsResult[$i]['availability'] = self::$outStock;
    				}
    			}
    			$forcedModifierGroupRowResult[0]['option'] = $forcedModifierGroupRowResultArr;
    			$forcedModifierGroupRowArr[] = $forcedModifierGroupRowResult[0];
    			unset($forcedModifierGroupRowResultArr);
    		}
    		return $forcedModifierGroupRowArr;
    	}

    }

    protected function action_getDetourDetails($args)
    {
    	$return_arr = ItemsDataController::pullForcedModifiersDetail(array('id' => $args['detour_id']));
    	if(substr($args['detour_id'],0,1) === 'g')
    	{
    		return $return_arr[0];
    	}
    	else{
    		$whereColumnArr = array("id" => substr($args['detour_id'], 1), "_deleted" => 0);
    		$forcedModifierListColumnArr = array("id", "title", "type", "type_option");
    		$forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
    		if (!empty($forcedModifierListRowResult)) {
    			$caption = "Choose your " . $forcedModifierListRowResult[0]['title'];
    			$forcedModifierListRowResult[0]['caption'] = $caption;
    			$forcedModifierColumnArr = array("id", "cost", "title", "ingredients", "nutrition","detour");
    			$orderBy = array("_order"=>"Asc");
    			$whereColumnArr = array("list_id" => $forcedModifierListRowResult[0]['id'], "_deleted" => 0);
    			$forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr, $orderBy, '', '', self::$fieldExpression);

    			if (!empty($forcedModifierRowResult)) {
    				$outofstockmodifiers = 0;
    				foreach ($forcedModifierRowResult as $val) {
    					if(isset($val['id']) && !empty($menuItemRowsResult[$i]['track_86_count'])){
    						$return = ItemsDataController::getForcedModifierInventory($val['id']);
    						if($return['stock'] == "continue"){
    							//$i++;
    							$outofstockmodifiers++;
    							continue;
    						}
    					}
    					// LP-4364 START
    					$forcedModifierStr = "";
    					if($val['nutrition'])
    					{
    						$forcedModifier = explode(",", $val['nutrition']);
    						foreach($forcedModifier as $fmnutVal)
    						{
    							if(substr($fmnutVal,-1) == "1")
    							{
    								$fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
    								$forcedModifierStr .= $fmnutVals.",";
    							}
    						}
    					}
    					$val['nutrition'] = rtrim($forcedModifierStr,',');

    					$val['status'] = "";

    					if(strtolower(substr($val['detour'], 0, 1)) == "g" && $forcedModifierListRowResult[0]['type'] != "checklist")
    					{
    						$val['doption'] = self::get_group_modifiers(substr($val['detour'],1), $inventoryMigrationStatus);
    					}
    					else{
    						if($val['detour'] && $forcedModifierListRowResult[0]['type'] != "checklist"){
    							$forcedModifierdListColumnArr = array("id", "title", "type", "type_option");
    							$whereListdColumnArr = array("id" => substr($val['detour'],1), "_deleted" => 0);
    							$forcedModifierdListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierdListColumnArr, $whereListdColumnArr,'');
    							$forcedModifierdListRowResult[0]['caption'] =  "Choose your ".$forcedModifierdListRowResult[0]['title'];
    						}
    						//echo "346<br>".$val['detour'];
    						$dlist = ItemsDataController::pullForcedModifiersDetail(array('id' => $val['detour']), $inventoryMigrationStatus);
    						if(!empty($dlist) && $forcedModifierListRowResult[0]['type'] != "checklist"){
    							$dopt_n = array_merge($forcedModifierdListRowResult[0],array('option'=>$dlist));
    							$val['doption'][]= $dopt_n;
    						}
    					}
    					$forcedModifierRowArr[] = $val;
    					unset($menuItemModifierNutritionArr);
    				}
    				if($forcedModifierListRowResult[0]['type']=='choice' && (count($forcedModifierRowArr)>0 && count($forcedModifierRowArr)==$outofstockmodifiers)){
    					$menuItemRowsResult[$i]['availability'] = self::$outStock;
    				}
    			}
    		}
    		 $forcedModifierListRowResult[0]['option'] = $forcedModifierRowArr;
    		 return $forcedModifierListRowResult;
    	}
    }
    protected function action_getItemInventoryStatus($arg) {
		$dataname = $arg[dataname];
		$items_details = JsonManager::urlJsonDecode($arg[item_contents]);
    	$result = array();
    	if (isset($items_details['item_id'])) {
	    	foreach ($items_details['item_id'] as $item_details) {
	    		$return = self::getItemInventory($item_details['id'], $item_details['quantity']);
	    		if ($return['stock'] == "continue") {
	    			$item_details ['status'] = self::$outStock;
	    		} else {
	    			$item_details ['status'] = self::$inStock;
	    		}
	    		$result['item_id'][] = $item_details;
	    	}
    	}
    	if (isset($items_details['forced_modifier_id'])) {
	    	foreach ($items_details['forced_modifier_id'] as $forced_modifier_details) {
	    		$return = self::getForcedModifierInventory($forced_modifier_details['id'], $forced_modifier_details['quantity']);
	    		if ($return['stock'] == "continue") {
	    			$forced_modifier_details ['status'] = self::$outStock;
	    		} else {
	    			$forced_modifier_details ['status'] = self::$inStock;
	    		}
	    		$result['forced_modifier_id'][] = $forced_modifier_details;
	    	}
    	}
    	if (isset($items_details['optional_modifier_id'])) {
	    	foreach ($items_details['optional_modifier_id'] as $optional_modifier) {
	    		$return = self::getOptionalModifierInventory($optional_modifier['id'], $optional_modifier['quantity']);
	    		if ($return == "continue") {
	    			$optional_modifier ['status'] = self::$outStock;
	    		} else {
 	    			$optional_modifier ['status'] = self::$inStock;
	    		}
	    		$result['optional_modifier_id'][] = $optional_modifier;
	    	}
		}
    	return $result;
	}

	protected function getMenuItemImage($storageKey, $imageName, $dataName){
		$image = new Image(Image::MENU_ITEM_TYPE, $storageKey, $imageName, $dataName);
		return $image->getImage();
	}

	protected function getCategoryImage($storageKey, $imageName, $dataName){
		$image = new Image(Image::CATEGORY_TYPE, $storageKey, $imageName, $dataName);
		return $image->getImage();
	}
}
