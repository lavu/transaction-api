<?php

class RedisDataController extends DataControllerBase{
	private $inventoryDetails;
	private $table86Count;
	private $menuitems;
	private $forcedmodifiers;
	private $optionalmodifiers;
	private $categorymenus;
	private $comboitems;

	public function __construct($request, $dataname)
	{
		parent::__construct(new ReflectionClass(__CLASS__), $request, $dataname, false);
		$locationid = (sessvar('location_info')['id']) ? sessvar('location_info')['id'] : sessvar('location_id');
		$locationid = ($locationid) ? $locationid : 1;
		$this->m_dataname = strtolower($dataname);
		$this->inventoryDetails = $this->m_dataname.'_'.$locationid.'_inv_details';
		$this->table86Count = $this->m_dataname.'_'.$locationid.'_86count_';
		$this->menuitems = $this->m_dataname.'_'.$locationid.'_menuitems';
		$this->forcedmodifiers = $this->m_dataname.'_'.$locationid.'_forcedmodifiers';
		$this->optionalmodifiers = $this->m_dataname.'_'.$locationid.'_optionalmodifiers';
		$this->categorymenus = $this->m_dataname.'_'.$locationid.'_categorymenus';
		$this->comboitems = $this->m_dataname.'_'.$locationid.'_comboitems';
	}

	protected function action_getInventory($args = array()) {
		$inventoryDetails = RedisQueries::getStringData($this->inventoryDetails);
		$response = array();
		if (!empty($inventoryDetails) ) {
			$response = json_decode($inventoryDetails, 1);
		}
		if (!empty($args['Items']) ) {
			$response = array_intersect_key($response, $args['Items']);
		}
		return $response;
	}

	protected function action_setInventory($args = array())
	{
		if (!empty($args['Items']) ) {
			$finalRequest = $args['Items'];
		} else {
			$finalRequest = array();
		}
		$response = RedisQueries::setStringData($this->inventoryDetails, json_encode($finalRequest) );
		return $response;
	}

	protected function action_getInventory86Count($args = array()) {
		$key = $this->table86Count.$args['Items']['table86'];
		$table86CountDetails = RedisQueries::getStringData($key);
		$response = array();
		if (!empty($table86CountDetails) ) {
			$response = json_decode($table86CountDetails, 1);
		}
		if (!empty($args['Items']['ids']) ) {
			$response = array_intersect_key($response, $args['Items']['ids']);
		}
		return $response;
	}

	protected function action_setInventory86Count($args = array()) {
		$table86 = $args['Items']['table86'];
		$key = $this->table86Count.$args['Items']['table86'];
		$response = array();
		if (!empty($args['Items']['ids']) ) {
			$args['Items']['table86'] = $table86;
			$response = RedisQueries::setStringData($key, json_encode($args['Items']['ids']) );
		}
		return $response;
	}

	protected function action_getAllRedisKeys() {
		$response = RedisQueries::getAllKeys();
		return $response;
	}

	protected function action_checkRedisKeys($args = array()) {
		$keys = $args['Items'];
		$redisKeys = RedisQueries::checkRedisKeys($keys);
		return $redisKeys;
	}

	public static function action_deleteRedisKeys($args = array()) {
		$keys = $args['Items'];
		$redisKeys = RedisQueries::deleteRedisKeys($keys);
		return $redisKeys;
	}

	protected function action_getMenuItems($args = array()) {
		$key = $this->menuitems;
		$menuitems = RedisQueries::getStringData($key);
		$response = array();
		if ($menuitems != '') {
			$response = json_decode($menuitems, 1);
		}
		if (!empty($args['Items']['ids']) ) {
			$response = array_intersect_key($response, $args['Items']['ids']);
		}
		return $response;
	}

	protected function action_getForcedModifiers($args = array()) {
		$key = $this->forcedmodifiers;
		$forcedModifiers = RedisQueries::getStringData($key);
		$response = array();
		if ($forcedModifiers != '') {
			$response = json_decode($forcedModifiers, 1);
		}
		if (!empty($args['Items']['ids']) ) {
			$response = array_intersect_key($response, $args['Items']['ids']);
		}
		return $response;
	}

	protected function action_getModifiers($args = array()) {
		$key = $this->optionalmodifiers;
		$modifiers = RedisQueries::getStringData($key);
		$response = array();
		if ($modifiers != '') {
			$response = json_decode($modifiers, 1);
		}
		if (!empty($args['Items']['ids']) ) {
			$response = array_intersect_key($response, $args['Items']['ids']);
		}
		return $response;
	}

	protected function action_setMenuItems($args = array()) {
		$response = array();
		if (!empty($args['Items']['ids']) ) {
			$key = $this->menuitems;
			$response = RedisQueries::setStringData($key, json_encode($args['Items']['ids']) );
		}
		return $response;
	}

	protected function action_setForcedModifiers($args = array()) {
		$key = $this->forcedmodifiers;
		$response = array();
		if (!empty($args['Items']['ids']) ) {
			$response = RedisQueries::setStringData($key, json_encode($args['Items']['ids']) );
		}
		return $response;
	}

	protected function action_setModifiers($args = array()) {
		$key = $this->optionalmodifiers;
		$response = array();
		if (!empty($args['Items']['ids']) ) {
			$cacheResponse = self::action_getModifiers($args);
			$finalRequest = array_replace($cacheResponse, $args['Items']['ids']);
			$response = RedisQueries::setStringData($key, json_encode($finalRequest) );
		}
		return $response;
	}

	protected function action_getCategoryMenus($args = array()) {
		$key = $this->categorymenus;
		$categorymenus = RedisQueries::getStringData($key);
		$response = array();
		if ($categorymenus != '') {
			$response = json_decode($categorymenus, 1);
		}
		if (!empty($args['Items']) ) {
			$ids = array_flip($args['Items']);
			$response = array_intersect_key($response, $ids);
		}
		return $response;
	}

	protected function action_setCategoryMenus($args = array()) {
		$key = $this->categorymenus;
		$response = array();
		if (!empty($args['Items']) ) {
			$cacheResponse = self::action_getCategoryMenus();
			$finalRequest = array_replace($cacheResponse, $args['Items']);
			$response = RedisQueries::setStringData($key, json_encode($finalRequest) );
		}
		return $response;
	}

	protected function action_deleteMenuCategoryKey() {
		$keys = array($this->categorymenus, $this->comboitems);
		$redisKeys = RedisQueries::deleteRedisKeys($keys);
		return $redisKeys;
	}

	protected function action_getComboItems($args = array()) {
		$key = $this->comboitems;
		$comboitems = RedisQueries::getStringData($key);
		$response = array();
		if ($comboitems != '') {
			$response = json_decode($comboitems, 1);
		}
		if (!empty($args['Items']['ids']) ) {
			$response = array_intersect_key($response, $args['Items']['ids']);
		}
		return $response;
	}

	protected function action_setComboItems($args = array()) {
		$key = $this->comboitems;
		$response = array();
		if (!empty($args['Items']['ids']) ) {
			$cacheResponse = self::action_getComboItems();
			$finalRequest = array_replace($cacheResponse, $args['Items']['ids']);
			$response = RedisQueries::setStringData($key, json_encode($finalRequest) );
		}
		return $response;
	}

	public static function action_getLanguageDictionary($args = array()) {
		$key = $args['Items']['key'];
		$response = array();
		if ($key != '') {
			$response = urlencode(serialize(RedisQueries::getHashData($key)));
		}
		return $response;
	}

	public static function action_setLanguageDictionary($args = array()) {
		$argsDecode = unserialize(urldecode(json_decode($args['Items'])));
		$key = $argsDecode['key'];
		$data = $argsDecode['data'];
		$response = array();
		if ($key != '' && !empty($data) ) {
			$response = RedisQueries::setHashData($key, $data);
		}
		return $response;
	}

	public static function action_getCacheData($args = array()) {
		$key = $args['Items']['key'];
		if ($key != '') {
			$response = RedisQueries::getStringData($key);
		}
		return $response;
	}

	public static function action_setCacheData($args = array()) {
		$key = $args['Items']['key'];
		$data = $args['Items']['data'];
		$response = array();
		if ($key != '' && $data != '') {
			$response = RedisQueries::setStringData($key, $data);
		}
		return $response;
	}
}

