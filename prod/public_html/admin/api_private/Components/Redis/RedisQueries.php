<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 7/3/18
 * Time: 6:15 PM
 */

//Class of static methods to provide abstraction to query calls.
//All methods should be static, and return the data.
class RedisQueries
{
	public static function getHashData($key)
	{
		return RedisConnection::RedisHashQuery('getHashAll', $key);
	}

	public static function setHashData($key, $data)
	{
		return RedisConnection::RedisHashQuery('setHash', $key, $data);
	}

	public static function getStringData($key)
	{
		return RedisConnection::RedisStringQuery('getString', $key);
	}

	public static function setStringData($key, $data)
	{
		return RedisConnection::RedisStringQuery('SetString', $key, $data);
	}

	public static function getAllKeys()
	{
		return RedisConnection::RedisStringQuery('getAllKeys');
	}

	public static function checkRedisKeys($data)
	{
		return RedisConnection::RedisHashQuery('checkRedisKeys', '', $data);
	}

	public static function deleteRedisKeys($data)
	{
		return RedisConnection::RedisHashQuery('deleteRedisKeys', '', $data);
	}
}