<?php


	public static runReport(){


	$nonFiscallyRecordedChecksQuery = <<<HEREDOCQUERY
	SELECT `orders`.`order_id` as `order_id`, 
       `orders`.`transaction_id` as `transaction_id`, 
       `split_check_details`.`check` as `check`, 
       `orders`.`closed` as `closed`,
       `orders`.`ioid` as `ioid` 
    FROM `orders` JOIN `split_check_details` ON `orders`.`order_id` = `split_check_details`.`order_id`
	JOIN `devices` ON `orders`.`closing_device` = `devices`.`UUID`
	JOIN `cc_transactions` ON `orders`.`order_id`=`cc_transactions`.`order_id`
	WHERE `orders`.`transaction_id`='' AND `orders`.`closed` <> "0000-00-00 00:00:00" AND `orders`.`closed` <> '' AND `split_check_details`.`print_info`='' ORDER BY `closed` DESC
HEREDOCQUERY;

	

	}