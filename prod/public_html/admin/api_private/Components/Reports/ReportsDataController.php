<?php

interface LavuReportRunner{
	function runQueries();
	function getData();
}



class ReportsDataController extends DataControllerBase{

	private $m_currentReportName;

	public function __construct($request, $dataname){
		parent::__construct(__CLASS__,$request,$dataname);
	}

	//For Inventory objects that can be represented by a single table row.
	protected function loadObjectSchemaMap($args){
	}



	//ALL INTERNAL HELPER METHODS MUST BE PRIVATE!!!
	private function verifyReportExists($reportName){
		if(!is_dir(__DIR__.'/'.$reportName)){
			throw new InvalidReportException();
		}
		if(!is_file(__DIR__.'/'.$reportName.'/'.$reportName.'.php')){
			throw new InvalidReportException();
		}
	}


	//ALL API CALLS MUST BE PROTECTED
	protected function action_RunReport($args){
		$actionName = substr(__FUNCTION__, 7);

		//verifyActionArguments($actionName, $args, $requiredArgs)
		$this->verifyActionArguments($actionName, $args, array("ReportName"));
		$this->verifyReportExists($args['ReportName']);
		$this->m_currentReportName = $args['ReportName'];
	}

}

class InvalidReportException extends exception{

}