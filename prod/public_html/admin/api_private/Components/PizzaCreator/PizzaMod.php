<?php

class PizzaMod
{
	const WHOLE_SERVING = 'whole';
	const LEFT_SERVING = 'left';
	const RIGHT_SERVING = 'right';

	public $id;
    public $location;
	public $name;
	public $partial_serving_pricing;
	public $displayOrder;
	public $forced_modifier_list_id;
	public $forced_modifier_list_title;
	public $forced_modifier_list_type;
	public $forced_modifiers=[];

	public function __construct($modRow)
	{
	  $this->id = $modRow['id'];
	  $this->location = $modRow['location'];
	  $this->name = $modRow['value'];
	  $this->partial_serving_pricing = $modRow['value10'];
	  $this->page = $modRow['value8'];
	  $this->displayOrder = $modRow['value6'];
	  $this->setForcedModifierListContentById($modRow['value2']);
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Set the list of Modifiers of a Pizza Mod.
	// This method follow same pattern as how Modifiers are being parsed in ItemsDataController class -> action_getItemsList method.
	//-----------------------------------------------------------------------------------------------------------------------------------
	protected function setForcedModifierListContentById($forced_modifier_list_id){
		$whereColumnArr = array("id" => $forced_modifier_list_id, "_deleted" => 0);
        $forcedModifierListColumnArr = array("id", "title", "type", "type_option");
        $forcedModifierListRowResult = CQ::getRowsWithRequiredColumns('forced_modifier_lists', $forcedModifierListColumnArr, $whereColumnArr,'');
        if (!empty($forcedModifierListRowResult)) {
			$this->forced_modifier_list_id = $forcedModifierListRowResult[0]['id'];
			$this->forced_modifier_list_title = $forcedModifierListRowResult[0]['title'];
			$this->forced_modifier_list_type = $forcedModifierListRowResult[0]['type'];
			$this->forced_modifier_list_type_option = $forcedModifierListRowResult[0]['type_option'];

            $forcedModifierColumnArr = array("id", "cost", "title", "ingredients", "nutrition","detour");
            $orderBy = array("_order"=>"Asc");
            $whereColumnArr = array("list_id" => $forced_modifier_list_id, "_deleted" => 0);
            $forcedModifierRowResult = CQ::getRowsWithRequiredColumns('forced_modifiers', $forcedModifierColumnArr, $whereColumnArr,$orderBy);

			if (!empty($forcedModifierRowResult)) {
                $inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
                foreach ($forcedModifierRowResult as $val) {
                    // LP-4364 START
					$forcedModifierStr = "";
                    if($val['nutrition'])
                    {
                        $forcedModifier = explode(",", $val['nutrition']);
                        foreach($forcedModifier as $fmnutVal)
                        {
                            if(substr($fmnutVal,-1) == "1")
                            {
                                $fmnutVals = substr($fmnutVal,0, strlen($fmnutVal)-4);
                                $forcedModifierStr .= $fmnutVals.",";
                            }
                        }
                    }
                    $val['nutrition'] = rtrim($forcedModifierStr,',');
					// LP-4364 END

                    if ($val['id'] != "") {
                        $ingredientsStr = ItemsDataController::getIngredientsById($val['id'], 'fm', $inventoryMigrationStatus);
                        $val['ingredients'] = $ingredientsStr;
                    }
				}
			}
			$this->forced_modifiers = $forcedModifierRowResult;
		}
	}

	//----------------------------------------------------------------------------------
	//	Parse Modifier Builder Content and return formatted Info.
	//----------------------------------------------------------------------------------
	public static function getModifierBuilderInfo($modifierBuilderContent){
		$builderAmount = $modifierBuilderContent['amount'];
		$builderServing = $modifierBuilderContent['serving'];
		return (object) array(
			'modifierInfo' => '{"portion":"'.$builderServing.'","heaviness":"'.$builderAmount.'"}',
			'modifierCost' => $modifierBuilderContent['cost']
		);
	}
}