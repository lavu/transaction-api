<?php
include_once 'PizzaCreator.php';

class PizzaSettings
{
	protected $pizza_settings;
	protected $data_name;

	public function __construct($dataName)
	{
		$this->data_name = $dataName;
		$this->pizza_settings = $this->getPizzaSettings();
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Return Pizza Creator linked to Menu Item. It will include all settings, lists and modifiers related to the menu item.
	//-----------------------------------------------------------------------------------------------------------------------------------
	public function getPizzaCreator($menuItem){
		//------------------------------------------------------------------------------
		// This code was taken from LVT 1.0
		//------------------------------------------------------------------------------
		$pizzaCreatorConfigData=null;
		$forcedModifierGroupID = $menuItem['forced_modifier_group_id'];
		$forcedModifierListID = str_replace('f', '', $forcedModifierGroupID);
		if(strtolower(trim($forcedModifierListID)) == 'c'){
			$itemsCategory = $this->getMenuCategoryByID($menuItem['category_id'], $this->data_name);
			if(!empty($itemsCategory)){
				$forcedModifierGroupID = $itemsCategory[0]['forced_modifier_group_id'];
				$forcedModifierListID = intval(str_replace('f', '', $forcedModifierGroupID));
			}
		}
		//------------------------------------------------------------------------------
		$pizzaCreatorConfigRows = $this->getPizzaConfigByForcedModifierGroupId($forcedModifierListID);
		if(count($pizzaCreatorConfigRows)>0){
			$pizzaCreatorConfigData = new PizzaCreator($pizzaCreatorConfigRows[0]);
			$pizzaModConfigRows = $this->getPizzaModsByPizzaCreatorId($pizzaCreatorConfigData->pizza_creator_id);
			$pizzaCreatorConfigData->setPizzaMods($pizzaModConfigRows);
		}
		return $pizzaCreatorConfigData;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Return a JSON with all pizza configurations
	//-----------------------------------------------------------------------------------------------------------------------------------
	protected function getPizzaSettings(){
		$pizzaSettings=null;
		$wherePizzaCreatotConfigNameColumnArr = array('ltg_pizza_info'); 
		$pizzaCreatotConfig = CQ::getRowsWhereColumnIn('config', "setting", $wherePizzaCreatotConfigNameColumnArr, $this->data_name);
		if(!empty($pizzaCreatotConfig))
			$pizzaSettings = @json_decode( $pizzaCreatotConfig[0]['value'], 1);			
		return $pizzaSettings;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Return the Pizza Creator Setting of a Menu Item, using forced_modifier_group_id
	//-----------------------------------------------------------------------------------------------------------------------------------
	protected function getPizzaConfigByForcedModifierGroupId($forcedModifierGroupId){
		$result=[];
		if(isset($this->pizza_settings)){
			$result = array_values(array_filter($this->pizza_settings['pizza_creators'], function ($value) use ($forcedModifierGroupId) {
				$currRowsFModListID = empty($value['value6']) ? 0 : $value['value6'];
				return ($value['_deleted'] == 0 && $currRowsFModListID == $forcedModifierGroupId);
			}));
		}
		return $result;
	}

	//-----------------------------------------------------------------------------------------------------------------------------------
	// Return all Pizza Mods of a Pizza Creator
	//-----------------------------------------------------------------------------------------------------------------------------------
	protected function getPizzaModsByPizzaCreatorId($pizzaCreatorId){
		$result=[];
		if(isset($this->pizza_settings)){
			$result = array_values(array_filter($this->pizza_settings['pizza_mods'], function ($value) use ($pizzaCreatorId) {
				return ($value['_deleted'] == 0 && $value['value7'] == $pizzaCreatorId);
			}));
		}
		return $result;
	}

	protected function getMenuCategoryByID($menuCategoryID){
		$whereMenuCategoryNameColumnArr = array($menuCategoryID); 
		return CQ::getRowsWhereColumnIn('menu_categories', "id", $whereMenuCategoryNameColumnArr, $this->data_name);
	}
}