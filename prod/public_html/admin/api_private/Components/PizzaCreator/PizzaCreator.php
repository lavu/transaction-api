<?php
include_once 'PizzaMod.php';

class PizzaCreator
{
	public $id;
	public $pizza_creator_id;
    public $location;
	public $name;
	public $created;
	public $forced_modifier_group_id;
	public $pizza_mods=[];
	
	public function __construct($pizzaCreatorRow)
	{
		$this->id = $pizzaCreatorRow['id'];
		$this->pizza_creator_id = empty($pizzaCreatorRow['value']) ? 0 : $pizzaCreatorRow['value'];
		$this->location = $pizzaCreatorRow['location'];
		$this->name = $pizzaCreatorRow['value2'];
		$this->created = $pizzaCreatorRow['value4'];
		$this->forced_modifier_group_id = $pizzaCreatorRow['value6'];
	}

	public function setPizzaMods($pizzaMods){
		foreach($pizzaMods as $modRow){
			$this->pizza_mods[] = new PizzaMod($modRow);
		}
	}

	//----------------------------------------------------------------------------------
	//	Accept selected modifiers and a dictionary with modifiers Builder Content, 
	//	and calculate the fields used in the saving process of the Order.
	//----------------------------------------------------------------------------------
	public static function calculateOrderContentsFields($selectedModifiers, $modifiersBuilderContent, $useLineBreaks=false){
		$pizzaOptions='';
		$pizzaForcedModifiersPrice = 0;
			
		$general='';
		$whole='';
		$left='';
		$right='';
		foreach($selectedModifiers as $mod_checked) {
			$mod_data = CQ::getRowsWithRequiredColumns('forced_modifiers', array('title', 'cost'), array('id' => $mod_checked));
			if(!empty($mod_data)){
				$title=$mod_data[0]['title'];
				$builderServing='';
				$builderAmount='';
				$builderCost=$mod_data[0]['cost'];
				if(isset($modifiersBuilderContent[(string)$mod_checked])){
					$modifierBuilderContent = $modifiersBuilderContent[(string)$mod_checked];
					$builderAmount = $modifierBuilderContent['amount'];
					$builderServing = $modifierBuilderContent['serving'];
					$builderCost = $modifierBuilderContent['cost'];	
				}
				switch ($builderServing) {
					case PizzaMod::WHOLE_SERVING:
						$whole.=' - '.$title.', '.$builderAmount.($useLineBreaks ? "\n" : '');
						break;
					case PizzaMod::LEFT_SERVING:
						$left.=' - '.$title.', '.$builderAmount.($useLineBreaks ? "\n" : '');
						break;
					case PizzaMod::RIGHT_SERVING:
						$right.=' - '.$title.', '.$builderAmount.($useLineBreaks ? "\n" : '');
						break;
					default:
						($general === '') ? $general.=$title.($useLineBreaks ? "\n" : '') : $general.=' - '.$title.($useLineBreaks ? "\n" : '');
						break;
				}
				$pizzaForcedModifiersPrice+=$builderCost;
			}
		}

		if ($general !== '')
			$pizzaOptions.=$general.($useLineBreaks ? '' : ' ');
		if ($whole !== '')
			$pizzaOptions.=($useLineBreaks ? "\n".'WHOLE'."\n".$whole : '( WHOLE: '.$whole.' ) ');
		if ($left !== '')
			$pizzaOptions.=($useLineBreaks ? "\n".'LEFT SIDE'."\n".$left : '( LEFT SIDE: '.$left.' ) ');
		if ($right !== '')
			$pizzaOptions.=($useLineBreaks ? "\n".'RIGHT SIDE'."\n".$right : '( RIGHT SIDE: '.$right.' ) ');

		return (object) array(
			'pizzaOptions' => $pizzaOptions, 
			'pizzaForcedModifiersPrice' => $pizzaForcedModifiersPrice
		);
	}
}