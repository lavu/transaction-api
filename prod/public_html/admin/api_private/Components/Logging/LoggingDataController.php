<?php
ini_set('display_errors', 1);

class LoggingDataController extends DataControllerBase{

	public function __construct($request, $dataname){
		parent::__construct(new ReflectionClass(__CLASS__),$request,$dataname);
	}

	protected function action_userSignin($args){
	    $username = $args['regDetails'][0]['username'];
		$pwd = $args['regDetails'][0]['password'];
		$pwdd=md5($pwd);
		$whereColumnArr=array("username"=>$username,"password"=>$pwdd);
		$columnNameArr=array("user_id");
		$result = CQ::getRowsWithRequiredColumns('users',$columnNameArr,$whereColumnArr,'');
		 if(isset($result[0]['user_id'])){
		 	$users_login_history = array("user_id"=>$result[0]['user_id'],"loggedin_time"=>"","loggedout_time"=>"",
		 			"loggin_status"=>"S");
		 	$insertRows = array();
		 	$columnNames = array_keys($users_login_history);
		 	$insertRows[0]=array_values($users_login_history);
		    CQ::insertArray('users_login_history', $columnNames, $insertRows);
                         $url = 'https://qatest.poslavu.com/olo_api/gen/reToken';
                                          $data = array("name" => $username,"prod" => "olo");
                                          $ch=curl_init($url);
                                          $data_string = json_encode($data);
                                          curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json'));
                                          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                                          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                                          $jwt_response = curl_exec($ch);
                                          $jwt = json_decode($jwt_response,true);
                                return $return = array("Status"=>"S", "msg"=>"Login Successfully", "jwt"=>$jwt['jwt'], "tokenId"=>$jwt['tokenId']);
		 	return $return = array("Status"=>"S", "msg"=>"Login Successfully");
		 }
		 else {
		 	$whereColumnArr=array("username"=>$username);
		 	$columnNameArr=array("user_id");
		 	$result = CQ::getRowsWithRequiredColumns('users',$columnNameArr,$whereColumnArr,'');
		 	if(isset($result[0]['user_id'])){
		 		$users_login_history = array("user_id"=>$result[0]['user_id'],"loggedin_time"=>"","loggedout_time"=>"",
		 				"loggin_status"=>"F");
		 		$insertRows = array();
		 		$columnNames = array_keys($users_login_history);
		 		$insertRows[0]=array_values($users_login_history);
		 		CQ::insertArray('users_login_history', $columnNames, $insertRows);
		 		return $return = array("Status"=>"F", "msg"=>"Invalid username or password");
		 	}
		 	 
		 	return $return = array("Status"=>"F", "msg"=>"Invalid username or password");
		 }
		 
	}

	protected  function action_userRegistration($args){
		  $pwd=$args['regDetails'][0]['password'];
		  $cpwd=$args['cpwd']['cpwd'];
		if($pwd!=$cpwd){
			return $return = array("Status"=>"F", "msg"=>"Password and Confrim password should be equal");
		}
		if($pwd==$cpwd){
			$columnNames = array_keys($args['regDetails'][0]);
			$insertRows = array();
			foreach($args['regDetails'] as $currRow){
				$insertRows[] = array_values($currRow);
			}
			$result = CQ::insertArray('users', $columnNames, $insertRows);
			$res= MA::resultToInsertIDUpdatedRows($result);
			if($res['msg']['InsertID']>0){
				return $return = array("Status"=>"S", "msg"=>"Register Successfully", "InsertID" =>$res['msg']['InsertID']);
				 
			}
			else {
				return $return = array("Status"=>"F", "msg"=>"username or email already exist");
			}
		}		
		
	}

    protected  function action_myAccount($args){
		$user_id=$args['users_info'][0]['user_id'];
		$whereColumnArr=array("user_id"=>$user_id);
		$columnNameArr=array("user_id");
		$result = CQ::getRowsWithRequiredColumns('users_info',$columnNameArr,$whereColumnArr,'');
		if(isset($result[0]['user_id'])){
			$fname=$args['users_info'][0]['first_name'];
			$lname=$args['users_info'][0]['last_name'];
			$email=$args['users_info'][0]['email'];
			$address=$args['users_info'][0]['address'];
			$city=$args['users_info'][0]['city'];
			$state=$args['users_info'][0]['state'];
			$zipcode=$args['users_info'][0]['zipcode'];
			$phone=$args['users_info'][0]['phone'];
			$mobile=$args['users_info'][0]['mobile'];
			$preferred_language=$args['users_info'][0]['preferred_language'];
			$updateltg = array(
					"first_name" => $fname,
					"last_name" =>$lname,
					"email"=>$email,
					"address" =>$address,
					"city"=>$city,
					"state" => $state,
					"zipcode" =>$zipcode,
					"phone"=>$phone,
					"mobile"=>$mobile,
					"preferred_language"=>""
			);
			//Update DATA in lavutogo users_info table
			$result = CQ::updateArray ( 'users_info', $updateltg, "where `user_id`='" . $user_id . "'");
			return MA::resultToInsertIDUpdatedRows($result);
			//Update DATA END
		} else {
				$columnNames = array_keys($args['users_info'][0]);
				$insertRows = array();
				foreach($args['users_info'] as $currRow){
					$insertRows[] = array_values($currRow);
				}
				$result = CommonQueries::insertArray('users_info', $columnNames, $insertRows);
				return MA::resultToInsertIDUpdatedRows($result);
			}
	
    }		

}
