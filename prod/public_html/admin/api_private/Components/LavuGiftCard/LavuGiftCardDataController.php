<?php

class LavuGiftCardDataController extends DataControllerBase{
    
    public function __construct($request, $dataname){
        parent::__construct(new ReflectionClass(__CLASS__),$request,$dataname);
    } 
    
    protected static function action_getLavuGiftCardDetails($args){
    	//echo "<pre>";print_r($args); exit;
    	
    	$card_no = $args['lavu_card'];
    	$dataname = $args['dataname'];
    	
    	$order_id = $args['orderId'];
    	
    	if(!isset($args['languageid']) || $args['languageid'] == '') {
    	    $args['languageid'] = '1';
    	}
    	 
    	if(!isset($order_id) || $order_id == "")
    	{
    		return array("error" => apiSpeak($args['languageid'], "Order Id is required"), "status" => "F");
    	}
    	else if(!isset($card_no) || empty($card_no)){
    	    return array("error" => apiSpeak($args['languageid'], "Lavu Card No is required"), "status" => "F");
    	}
    	 
    	$order_arr = OrdersQueries::checkOrder($order_id);
    	 
    	if(empty($order_arr)){
    	    return array("Status"=>"F", "error" => apiSpeak($args['languageid'], "Invalid Order Id"));
    	}
    	else{
    		$order_id = $order_arr[0]['order_id'];
    		$order_total = $order_arr[0]['amount'];
    		if($order_total <= 0){
    		    return array("error" => apiSpeak($args['languageid'], "Zero order amount. So cannot proceed to Lavu Gift Card"), "status" => "F");
    		}
    	}
    	
    	$whereColumnArr= array("name" => $card_no, "dataname" => $dataname);
    	//$result = CQ::getRowsWhereColumn('lavu_gift_cards', $whereColumnArr, '', '', "MAIN");
    	$colsArray = array('id','expires','inactive','points');
    	$result = CQ::getRowsWithRequiredColumnsMainDB('lavu_gift_cards',$colsArray,$whereColumnArr);
    	
    	$card_balance = 0;
    	//echo "<pre>";print_r($result); exit;
    	$resultArr = array();
    	if(!empty($result))
    	{
    		$datetime = time();
    		$card_dbid = $result[0]['id'];
    		$gift_expires = $result[0]['expires'];
    		$inactive = $result[0]['inactive'];
    		if($inactive == 1){
    		    $resultArr = array("error" => apiSpeak($args['languageid'], "Lavu Gift Card is inactive"), "status" => 0);
    		}
    		else{
    			if(empty($gift_expires)){
    				$expired = false;
    			}
    			else{
    				//$location_info = LocationDataController::action_getLocationInfo(1, $dataname); // fetching location info
    				//echo "<pre>";print_r($location_info);
    				//$datetime = LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]);
    				$datetime = date("Y-m-d H:i:s");
    				$expired = (strtotime($gift_expires) - strtotime($datetime)) < 0;
    			}
    			if($expired){
    			    $resultArr = array("error" => apiSpeak($args['languageid'], "Lavu Gift Card is expired"), "status" => 0);
    			}
    			else {
    				$card_balance = $result[0]['balance'] * 1;
    				$resultArr = array("lavu_gift_card_balance" => $card_balance, "status" => 1);
    			}
    		}    		
    	}
    	else{
    	    $resultArr = array("error" => apiSpeak($args['languageid'], "Lavu Gift Card doesnot exists"), "status" => 0);
    	}
    	 
    	return $resultArr;
    }
    
    public static function UTCDateTime($add_z, $ts="")
	{
		if ($ts == "")
		{
			$ts = time();
		}

		$utc_tz = new DateTimeZone("UTC");
		$datetime = new DateTime(NULL, $utc_tz);
		$datetime->setTimestamp($ts);

		$datetime_str = $datetime->format("Y-m-d H:i:s");
		if ($add_z)
		{
			$datetime_str .= "Z";
		}

		return $datetime_str;
	}
	
    public static function DateTimeForTimeZone($timezone, $ts="")
	{
                //echo "timezone: ".$timezone."<br>";
		if ($ts == "")
		{
			$ts = time();
		}

		if (empty($timezone))
		{
			return LavuGiftCardDataController::UTCDateTime(TRUE, $ts);
		}

		$tz = new DateTimeZone($timezone);
		$datetime = new DateTime(NULL, $tz);
		$datetime->setTimestamp($ts);

		$datetime_str = $datetime->format("Y-m-d H:i:s");
                //echo "datatime: ".$datetime_str."<br>";
                //exit;

		return $datetime_str;
	}
    
	public static function action_checkAwardPoints($args){
		$card_no = $args['lavu_card'];
		$dataname = $args['dataname'];
		$order_id = $args['orderId'];
		 
		if(!isset($order_id) || $order_id == "")
		{
			return array("error" => "Empty Order Id", "status" => "F");
		}
		else if(!isset($card_no) || empty($card_no)){
			return array("error" => "Empty Lavu Card No", "status" => "F");
		}
		 
		$order_arr = OrdersQueries::checkOrder($order_id);
		//print_r($order_arr);exit;
		 
		if(empty($order_arr)){
			return array("Status"=>"F", "error"=>"Wrong order Id");
		}
		else{
			$order_id = $order_arr[0]['order_id'];
			 $order_total = $order_arr[0]['amount'];
			if($order_total > 0){
				$order_information = json_decode($order_arr[0]['order_information'], true);
				$orderItems = array();
				foreach($order_information['order_contents_information'] as $order_contents){
					$orderItems[$order_contents['item_id']] = $order_contents['quantity'];
				}
			}
			else{
				return array("error" => "Zero Order amount. so can't proceed to Loyalty points.", "status" => "F");
			}
		}
		 
		$whereColumnArr= array("name" => $card_no, "dataname" => $dataname);
		//$result = CQ::getRowsWhereColumn('lavu_gift_cards', $whereColumnArr, '', '', "MAIN");
		$colsArray = array('id','expires','inactive','points');
		$result = CQ::getRowsWithRequiredColumnsMainDB('lavu_gift_cards',$colsArray,$whereColumnArr);

		$points_balance = 0;
		
		$resultArr = array();
		if(!empty($result))
		{
			$card_dbid = $result[0]['id'];
			$gift_expires = $result[0]['expires'];
			$inactive = $result[0]['inactive'];
			if($inactive == 1){
				$resultArr = array("error" => "Lavu Loyalty Number is Inactive.", "status" => 0);
			}
			else{
				if(empty($gift_expires)){
					$expired = false;
				}
				else{
					//$location_info = LocationDataController::action_getLocationInfo(1, $dataname); // fetching location info
					//echo "<pre>";print_r($location_info);
					//$datetime = LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]);
					$datetime = date("Y-m-d H:i:s");
					$expired = (strtotime($gift_expires) - strtotime($datetime)) < 0;
				}
				if($expired){
					$resultArr = array("error" => "Lavu Loyalty Number is Expired.", "status" => 0);
				}
				else {
					$points_balance = $result[0]['points'] * 1;
					$resultArr = array("loyality_points" => $points_balance, "status" => 1);
		
					$menu_data = $orderItems;//array("474" => 3); // menu items id and qty
					$menu_items_data = CQ::getRowsWhereColumnIn("menu_items","id",array_keys($menu_data));
					$total_award = 0;
					foreach ($menu_items_data as $menu_item)
					{
						$special2 = strtolower($menu_item['hidden_value3']); // special details 2
						$price = $menu_item['price']; // item price
						$award_parts = explode("award ",$special2);
						if(count($award_parts) > 1)
						{
							$point_parts = explode(" point",$award_parts[1]);
							if(count($point_parts) > 1)
							{
								$point_str = trim($point_parts[0]);
		
								if(is_numeric($point_str))
								{
									$min_cost = 0;
									$min_cost_parts = explode("minimum cost ",$point_parts[1]);
									if(count($min_cost_parts) > 1)
									{
										$min_cost_str = trim(str_replace("of ","",$min_cost_parts[1]));
										if(is_numeric($min_cost_str))
										{
											$min_cost = $min_cost_str * 1;
										}
									}
		
									if($price * 1 >= $min_cost * 1)
									{
										$total_award += $point_str * $menu_data[$menu_item['id']];
									}
								}
							}
						}
					}
					 
					$resultArr['awardpoints'] = $total_award;					
		
				}
			}
		}
		else{
			$resultArr = array("error" => "Lavu Loyalty Number not exists.", "status" => 0);
		}
		 
		return $resultArr;
	}
	
	public static function action_checkLoyaltyDiscountPoints($args)
	{
		//echo "<pre>";print_r($args); exit;		 
		$card_no = $args['lavu_card'];
		$dataname = $args['dataname'];
		$order_id = $args['orderId'];
		$discountId = $args['discountId'];
		 
		$order_arr = OrdersQueries::checkOrder($order_id);
		 
		if(empty($order_arr)){
			return array("status"=>0, "error"=>"Wrong order Id");
		}
		else{
			$order_id = $order_arr[0]['order_id'];
			$order_total = $order_arr[0]['amount'];
			if($order_total > 0){
				$order_information = json_decode($order_arr[0]['order_information'], true);
				$orderItems = array();
				foreach($order_information['order_contents_information'] as $order_contents){
					$orderItems[$order_contents['item_id']] = $order_contents['quantity'];
				}
			}
			else{
				return array("error" => "Zero Order amount. so can't proceed to Loyalty points.", "status" => 0);
			}
		}
		 
		$whereColumnArr= array("name" => $card_no, "dataname" => $dataname);
		//$result = CQ::getRowsWhereColumn('lavu_gift_cards', $whereColumnArr, '', '', "MAIN");
		$colsArray = array('id','expires','inactive','points');
		$result = CQ::getRowsWithRequiredColumnsMainDB('lavu_gift_cards',$colsArray,$whereColumnArr);
		 
		$points_balance = 0;
		//echo "<pre>";print_r($rowsResult); exit;
		$resultArr = array();
		if(!empty($result))
		{
			$card_dbid = $result[0]['id'];
			$gift_expires = $result[0]['expires'];
			$inactive = $result[0]['inactive'];
			if($inactive == 1){
				$resultArr = array("error" => "Lavu Loyalty Number is Inactive.", "status" => 0);
			}
			else{
				if(empty($gift_expires)){
					$expired = false;
				}
				else{
					//$location_info = LocationDataController::action_getLocationInfo(1, $dataname); // fetching location info
					//echo "<pre>";print_r($location_info);
					//$datetime = LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]);
					$datetime = date("Y-m-d H:i:s");
					$expired = (strtotime($gift_expires) - strtotime($datetime)) < 0;
				}
				if($expired){
					$resultArr = array("error" => "Lavu Loyalty Number is Expired.", "status" => 0);
				}
				else {
					$points_balance = $result[0]['points'] * 1;
						
					$discountTypesObj = DBConnection::clientQuery("SELECT * FROM `discount_types` WHERE id= '$discountId' AND `special`='lavu_loyalty' AND `_deleted`='0' AND `loyalty_cost`<= '".$points_balance."' AND `min_access`<='4'");
					$discount_types =  MA::resultToArray($discountTypesObj);
					if(!empty($result))
					{
						$discount_type_arr = array();
						foreach ($discount_types as $discount_type){
							$discount_value = '';
							$reward_use_or_unlock = $discount_type['loyalty_use_or_unlock'];
							$reward_cost = $discount_type['loyalty_cost'];
							$unlock_data='';
							if($reward_use_or_unlock=='2'){ $unlock_data = 'unlock'; }
								
							if($discount_type['code'] == 'p') {
								$tmp = floatval($discount_type['calc']) * 100;
								$reward_text = $tmp . "%";
								$discount_value = $order_total * floatval($discount_type['calc']);
							}
							else{
								$reward_text = LavuGiftCardDataController::ext_display_money_with_symbol($discount_type['calc']);
								$discount_value = $discount_type['calc'];
							}
	
							$reward_text = $discount_type['label'] . ": " . $reward_text . " off ".$unlock_data." at " . $reward_cost . " points";
								
							$discount_type_arr[] = array("discount_id" => $discount_type['id'], "title" => $reward_text, "discount_points" => $reward_cost,"discount_value" => $discount_value);
						}
						$resultArr = array("error" => $discount_type_arr, "status" => 1);
					}
					else{
						$resultArr = array("error" => "Invalid Discount Id", "status" => 0);
					}
				}
			}
		}
		else{
			$resultArr = array("error" => "Lavu Loyalty Number not exists.", "status" => 0);
		}
		 
		return $resultArr;
	}
	
    protected function action_getLoyaltyPointDetails($args) {
    	$card_no = $args['lavu_card'];
    	$dataname = $args['dataname'];
    	$order_id = $args['orderId'];
    	$orderItemsDetails = $args['cart_contents'];
    	
    	if(!isset($args['languageid']) || $args['languageid'] == '') {
    	    $args['languageid'] = '1';
    	}
    	
    	if (!isset($order_id) || $order_id == "") {
    	    return array("error" => apiSpeak($args['languageid'], "Order Id is required"), "status" => "F");
    	} else if (!isset($card_no) || empty($card_no)) {
    	    return array("error" => apiSpeak($args['languageid'], "Lavu Card No is required"), "status" => "F");
    	}
    	
    	$order_arr = OrdersQueries::checkOrder($order_id);
    	
    	if (empty($order_arr)) {
    	    return array("Status"=>"F", "error"=>apiSpeak($args['languageid'], "Invalid Order Id"));
    	} else {    		
    		$order_id = $order_arr[0]['order_id'];
    	    $order_total = $order_arr[0]['amount'];
    		if ($order_total > 0) {
    			$order_information = json_decode($order_arr[0]['order_information'], true);
    			$orderItems = array();
    			$i=1;
    			foreach ($order_information['order_contents_information'] as $order_contents) {
    				if (array_key_exists($order_contents['item_id'], $orderItems) !== false) {
    					$orderItems[$order_contents['item_id']] = $order_contents['quantity']+$i;
    					$i++;
    				} else {
    					$orderItems[$order_contents['item_id']] = $order_contents['quantity'];
    				}
    			}
    		} else {
    		    return array("error" => apiSpeak($args['languageid'], "Zero order amount. So cannot proceed to Loyalty Points"), "status" => "F");
    		}
    	}
    	
    	$whereColumnArr= array("name" => $card_no, "dataname" => $dataname);    	
    	$colsArray = array('id','expires','inactive','points');
    	$result = CQ::getRowsWithRequiredColumnsMainDB('lavu_gift_cards',$colsArray,$whereColumnArr);
    	
    	$points_balance = 0;
    	$resultArr = array();
    	if (!empty($result)) {
    		$card_dbid = $result[0]['id'];
    		$gift_expires = $result[0]['expires'];
    		$inactive = $result[0]['inactive'];
    		if ($inactive == 1) {
    		    $resultArr = array("error" => apiSpeak($args['languageid'], "Lavu Loyalty Number is inactive"), "status" => 0);
    		} else {
	    		if (empty($gift_expires)) {
	    			$expired = false;
	    		} else {
	    			$datetime = date("Y-m-d H:i:s");
	    			$expired = (strtotime($gift_expires) - strtotime($datetime)) < 0;
				}
				
	    		if ($expired) {
	    		    $resultArr = array("error" => apiSpeak($args['languageid'], "Lavu Loyalty Number is expired"), "status" => 0);
	    		} else {
	    			$points_balance = $result[0]['points'] * 1;
	    			$resultArr = array("loyality_points" => $points_balance, "status" => 1);
	    			$menu_data = $orderItems;//array("474" => 3); // menu items id and qty
	    			$menu_items_data = CQ::getRowsWhereColumnIn("menu_items", "id", array_keys($menu_data));
	    			$total_award = 0;
	    			foreach ($menu_items_data as $menu_item) {
	    				$special2 = strtolower($menu_item['hidden_value3']); // special details 2
	    				$price = $menu_item['price']; // item price
	    				$award_parts = explode("award ",$special2);
	    				if (count($award_parts) > 1) {
	    					$point_parts = explode(" point",$award_parts[1]);
	    					if (count($point_parts) > 1) {
	    						$point_str = trim($point_parts[0]);

	    						if (is_numeric($point_str)) {
	    							$min_cost = 0;
	    							$min_cost_parts = explode("minimum cost ",$point_parts[1]);
	    							if (count($min_cost_parts) > 1) {
	    								$min_cost_str = trim(str_replace("of ","",$min_cost_parts[1]));
	    								if (is_numeric($min_cost_str)) {
	    										$min_cost = $min_cost_str * 1;
	    								}
	    							}

	    							if ($price * 1 >= $min_cost * 1) {
	    								$total_award += $point_str * $menu_data[$menu_item['id']];
	    							}
	    						}
	    					}
	    				}
	    			}

	    			$resultArr['awardpoints'] = $total_award;
	    			foreach($orderItemsDetails as $orderItemsDetail)
	    			{
	    				if($orderItemsDetail['itemid'] != '' && $orderItemsDetail['qty'] >= 1){

	    					$itemdetail = OrdersQueries::itemdetails($orderItemsDetail['itemid'], $orderItemsDetail['qty'],	$orderItemsDetail['fmods_checked'], $orderItemsDetail['omods_checked'], $orderItemsDetail['special']);
	    					$itemdetails[] = $itemdetail;
	    				}
	    			}
	    			if(!empty($result) && $args['discount_id']>0)
	    			{
	    				return $this->getDiscountById($resultArr,$args['discount_id'],$itemdetails);
	    			}
	    			$discountTypesObj = DBConnection::clientQuery("SELECT * FROM `discount_types` WHERE `special`='lavu_loyalty' AND `_deleted`='0' AND `loyalty_cost`<= '".$points_balance."' AND `min_access`<='4'");
	    			$discount_types =  MA::resultToArray($discountTypesObj);
	    			$discount_type_arr = array();
	    			if(!empty($discount_types))
	    			{
	    				foreach ($discount_types as $discount_type){
	    					$discount_value = '';
	    					$reward_use_or_unlock = $discount_type['loyalty_use_or_unlock'];
	    					$reward_cost = $discount_type['loyalty_cost'];
	    					$unlock_data='';
	    					if($reward_use_or_unlock=='2'){ $unlock_data = 'unlock'; }

	    					if($discount_type['code'] == 'p') {
	    						$tmp = floatval($discount_type['calc']) * 100;
	    						$reward_text = $tmp . "%";
	    						$discount_value = $order_total * floatval($discount_type['calc']);
	    					}
	    					else{
	    						$reward_text = LavuGiftCardDataController::ext_display_money_with_symbol($discount_type['calc']);
	    						$discount_value = $discount_type['calc'];
	    					}

	    					$reward_text = $discount_type['label'] . ": " . $reward_text . " off ".$unlock_data." at " . $reward_cost . " points";

	    					$discount_type_arr[] = array("discount_id" => $discount_type['id'], "title" => $reward_text, "discount_points" => $reward_cost,"discount_value" => $discount_value);
	    				}
	    				
	    			}
	    			$resultArr['discounttypes'] = $discount_type_arr;
	    					$itemdetail = OrdersQueries::itemdetails($orderItemsDetail['itemid'], $orderItemsDetail['qty'],	$orderItemsDetail['fmods_checked'], $orderItemsDetail['omods_checked'], $orderItemsDetail['special']);
	    					$itemdetails[] = $itemdetail;
	    				}

	    			}

	    			$taxinform = OrdersDataController::displaytax($itemdetails,$tmp);
	    			$calc = OrdersDataController::caltotal($taxinform);
	    			$resultArr['taxdetails'] = $taxinform;
	    			if(count($resultArr['discounttypes'])>0){
	    				$resultArr['discounttypes'][0]['discount_value'] = $calc['discount'];
	    			}
    		}
    		else{
    		    $resultArr = array("error" => apiSpeak($args['languageid'], "Lavu Loyalty Number doesnot exists"), "status" => 0);
			}

    	return $resultArr;
    }

    function getDiscountById($result,$id,$itemdetails){
    	$discountTypesObj = DBConnection::clientQuery("SELECT * FROM `discount_types` WHERE `id`='".$id."' AND `_deleted`='0'");
    	$discount_types =  MA::resultToArray($discountTypesObj);
    	$discount_type_arr = array();
    	if(!empty($discount_types)){
	    	//foreach ($discount_types as $discount_type){
	    		$discount_value = '';
	    		$reward_use_or_unlock = $discount_types[0]['loyalty_use_or_unlock'];
	    		$reward_cost = $discount_types[0]['loyalty_cost'];
	    		$unlock_data='';
	    		if($reward_use_or_unlock=='2'){ $unlock_data = 'unlock'; }

	    		if($discount_types[0]['code'] == 'p') {
	    			$tmp = floatval($discount_types[0]['calc']) * 100;
	    			$reward_text = $tmp . "%";
	    			$discount_value = $order_total * floatval($discount_types[0]['calc']);
	    		}
	    		else{
	    			$reward_text = LavuGiftCardDataController::ext_display_money_with_symbol($discount_types[0]['calc']);
	    			$discount_value = $discount_types[0]['calc'];
	    		}

	    		$reward_text = $discount_types[0]['label'] . ": " . $reward_text . " off ".$unlock_data." at " . $reward_cost . " points";

	    		/***Tax Calc****/
	    		$taxinform = OrdersDataController::displaytax($itemdetails,$tmp);
	    		$calc = OrdersDataController::caltotal($taxinform);
	    		$result['taxdetails'] = $taxinform;
	    		/*******/
	    		$discount_type_arr = array("discount_id" => $discount_types[0]['id'], "title" => $reward_text, "discount_points" => $reward_cost,"discount_value" => $calc['discount']);
	    	//}
    	}
    	$result['discounttypes'] = $discount_type_arr;
    	return $result;
    }
    public static function ext_display_money_with_symbol($amt) {
    	
    	$location_info = CQ::getAllRows('locations');
		global $location_info;
		$amt = str_replace("$","",str_replace(",","",$amt));
		
		$monetary_symbol = $location_info[0]['monitary_symbol'];
		$decimal_char = $location_info[0]['decimal_char'];
		if ($monetary_symbol == "") $monetary_symbol = "$";
		$left_or_right = $location_info[0]['left_or_right'];
		if ($left_or_right == "") $left_or_right = "left";
		
		$rtn_str = "";
		if ($left_or_right == "left") $rtn_str .= $monetary_symbol;
		$str_number = LavuGiftCardDataController::ext_display_money($amt);
		if(isset($decimal_char) && $decimal_char!="." && $decimal_char!="")
		{
			$str_number = str_replace(".",$decimal_char,str_replace(",","",$str_number));
		}
		$rtn_str .= $str_number . "";
		if ($left_or_right == "right") $rtn_str .= $monetary_symbol;
		
		return $rtn_str;
	}
	
	public static function ext_display_money($amt) {
		global $location_info;
		
		$decimal_places = $location_info[0]['disable_decimal'];
		if(!is_numeric($decimal_places)) $decimal_places = 2;
		
		return number_format($amt,$decimal_places);
	}

}
