<?php

//Class of static methods to provide abstraction to query calls.
//All methods should be static, and return the data if possible instead of mysqli_result.
class AccountsQueries{

	public static function getMainDBCustomerAccountUsingCredentials($username, $password){
		$result = DBConnection::adminQuery("SELECT * FROM `poslavu_MAIN_db`.`customer_accounts` WHERE `username`='[1]'", $username);
		return resultFirstRow($result);
	}

	public static function getUserRowFromClientDB($username, $password){
		$result = DBConnection::clientQuery("SELECT * FROM `users` WHERE `username`='[1]' AND `password`=PASSWORD('[2]')", $username, $password);
		return resultFirstRow($result);
	}


}