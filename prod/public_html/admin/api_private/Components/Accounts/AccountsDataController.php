<?php

class AccountsDataController extends DataControllerBase{
	
	public function __construct($request, $dataname){
		parent::__construct(new ReflectionClass(__CLASS__),$request,$dataname);
	}



	private function userSessionLogin($userRowFromDB){
		if(session_status() == PHP_SESSION_NONE){
			session_start();
		}else{
			//TODO THROW SESSION ALREADY STARTED EXCEPTION.
		}
	}


	/*
		Takes username and password.
		If username and password authenticate a session will be started
		for the user loggin.
	*/
	protected function action_AuthenticateSession($args){
		$actionName = substr(__FUNCTION__, 7);

		//Make sure api has all required params/args
		$requiredParameters = array('username','password');
		$this->verifyActionArguments("AuthenticateSession", $args, $requiredParameters);
		//Get user row from poslavu_MAIN_db.customer_accounts
		$userRowFromMainDB = ADCQ::getMainDBCustomerAccountUsingCredentials($username, $password);
		$this->verifyNotEmpty($userRowFromMainDB,$actionName,"Could not find username in main db.");
		//Connect to client db based on the dataname specified
		DBConnection::startClientConnection($userRowFromMainDB['dataname']);
		$userRowInClientDB = ADCQ::getUserRowFromClientDB($username, $password);
		$this->verifyNotEmpty($userRowInClientDB,"Username and Password match not found.");
		$this->userSessionLogin($userRowInClientDB);

	}

	protected function action_GetAccountByCredentials($args){
		$actionName = substr(__FUNCTION__, 7);
		$userRowFromMainDB = getMainDBCustomerAccountUsingCredentials($username, $password);
		$this->verifyNotEmpty($userRowFromMainDB,$actionName,"Could not find username in main db.");
		DBConnection::startClientConnection($userRowFromMainDB['dataname']);
		$userRowInClientDB = ADCQ::getUserRowFromClientDB($username, $password);
		$this->verifyNotEmpty($userRowInClientDB,"Username and Password match not found.");		
	}


}