<?php

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/8/17
 * Time: 10:35 AM
 */
class EnterpriseDataController extends DataControllerBase
{
	public function __construct($request, $dataname){
		parent::__construct(new ReflectionClass(__CLASS__),$request,$dataname);
	}

	protected function action_getCountryNamesFull(){
		$result = EnterpriseQueries::getCountryNamesFull();
		return $result;
	}

	protected function action_getCountryNamesAbbreviated(){
		$result = EnterpriseQueries::getCountryNamesAbbreviated();
		return $result;
	}

	protected function action_getCountries(){
		$result = EnterpriseQueries::getCountries();
		return $result;
	}

	protected function action_getRestaurantByID($args){
		$argItems = $args['Items'];
		$result = EnterpriseQueries::getRestaurantByID($argItems);
		return $result;
	}

	protected function action_getRestaurantIDAndTitle(){
		$result = EnterpriseQueries::getRestaurantIDAndTitleByDataname($this->m_dataname);
		return $result;
	}

	protected function action_getAllChainedRestaurants(){
		$result = EnterpriseQueries::getRestaurantChainsByDataname($this->m_dataname);
		return $result;
	}

	protected function action_getChainIDByRestaurantID($args){
		$restaurantID = $args['Items']['id'];
		$result = EnterpriseQueries::getChainIDByRestaurantID($restaurantID);
		return $result;
	}

	protected function action_listChainedLocations(){
		$result = EnterpriseQueries::listChainedLocations($this->m_dataname);
		return $result;
	}

	protected function action_getLanguageInfo(){
	    $result = EnterpriseQueries::getLanguagePackInfo();
	    return $result;
    }

    protected function action_getAllChainedLocations(){
        $result = array();
        if(sessvar('admin_access_level')>=4) {
            $locationResponse = EnterpriseQueries::getRestaurantChainsByDataname($this->m_dataname);
            if (!empty($locationResponse)) {
                foreach ($locationResponse as $key => $locationDetails) {
                    $result[$key]['id'] = $locationDetails['id'] . '-1';
                    $ext = (count($locationResponse) > 1) ? ' (#'.($locationDetails['id'] + 100000).')' : '';
                    $result[$key]['name'] = $locationDetails['name'] . $ext;
                    $result[$key]['dataname'] = $locationDetails['dataname'];
                }
            }
        }

        $currentId = sessvar('admin_companyid') . '-1';
        $currentLoc = sessvar('location_info')['title'];
        $currentDataName = sessvar('admin_dataname');
        if (empty($result)) {
            $result[0]['id'] = $currentId;
            $result[0]['name'] = $currentLoc;
            $result[0]['dataname'] = $currentDataName;
        }

        $response = array();
        $response['current'][0]['id'] = $currentId;
        $response['current'][0]['name'] = $currentLoc;
        $response['current'][0]['dataname'] = $currentDataName;
        $response['all'] = $result;
        return $response;
    }

    protected function action_changeLocation($args){
        $response = array();
        $restaurantId = $args['Items'][0]['id'];
        // sanitize the input
        if (strpos((string)$restaurantId, '-') !== FALSE) {
            $restaurant_id = explode('-', $restaurantId);
            $restaurantId = $restaurant_id[0];
        }

        $verify = self::action_verifyCanAccessRestaurantID($this->m_dataname, admin_info('dataname'), $restaurantId);
        if ($verify) {
            $response = self::action_changeActiveChainAccount($restaurantId);
        }

        return $response;
    }

    protected function action_verifyCanAccessRestaurantID($dnLoggedin, $dnAdminInfo, $restaurantId) {

        if ($dnLoggedin == '') {
            error_log('Dataname not logged in');
        }

        // get the location id of $dn_loggedin
        $a_loggedin_restaurant = EnterpriseQueries::getRestaurantIDAndTitleByDataname($dnLoggedin);
        $s_loggedin_restaurant_id = $a_loggedin_restaurant[0]['restaurantid'];

        if ($s_loggedin_restaurant_id == $restaurantId) {
            return TRUE;
        }

        // check that the restaurant exists
        $a_restaurant = EnterpriseQueries::getRestaurantByID($restaurantId);
        if (count($a_restaurant) == 0) {
            error_log("User trying to access a restaurant that doesn't exist (restaurant id $restaurantId)");
            exit();
        }

        // get the restaurants this user has access to
        $a_restaurants = EnterpriseQueries::getRestaurantChainsByDataname($this->m_dataname);

        // check that $dn_loggedin is a part of the same chain as $restaurant_id
        $b_found_dn = FALSE;
        foreach($a_restaurants as $a_other_rest) {
            if ($a_other_rest['dataname'] == $this->m_dataname) {
                $b_found_dn = TRUE;
                break;
            }
        }

        if (!$b_found_dn) {
            error_log("User trying to access restaurant that it doesn't belong to (username ".admin_info('username').", restaurant id $restaurantId)");
            exit();
        }

        return TRUE;
    }

    protected function action_changeActiveChainAccount($toCompanyid, $locId = 1)
    {
        $from_company_name = admin_info('company_name');
        $from_dataname = admin_info('dataname');
        $users_login_id = admin_info("loggedin");
        $company_info = EnterpriseQueries::getRestaurantByID(array('id' => $toCompanyid))[0];
        if (!empty($company_info))
        {
            session_start();
            DBConnection::startClientConnection($company_info['data_name']);
            $languageId = EnterpriseQueries::getUserLanguageId($users_login_id);
            $use_language_pack = $languageId[0]['use_language_pack'];
            if ($use_language_pack < 0 ) {
                $languageId = EnterpriseQueries::getLanguagePackInfo();
                $use_language_pack = $languageId[0]['id'];
            }
            $locationInfo = EnterpriseQueries::getLocationInfo();
            foreach ($locationInfo as $locationDetails) {
                if ($company_info['id'] == $toCompanyid && $locationDetails['id'] == $locId) {
                    $configDetails = EnterpriseQueries::getLocationConfigDetails($locId);
                    $location_info = $locationDetails;
                    if ($use_language_pack < 0)
                    {
                        $use_language_pack = $locationDetails['use_language_pack'];
                    }
                }
            }
            foreach ($configDetails as $cDetails)
            {
                $location_info[$cDetails['setting']] = $cDetails['value'];
            }
            set_sessvar("location_info", $location_info);
            set_sessvar("locationid", $location_info['id']);
            if ($use_language_pack > 0) {
                global $active_language_pack_id;
                $active_language_pack_id = $use_language_pack;
                $active_language_pack = getLanguageDictionary($locId);
                if (empty($active_language_pack)) {
                    unset_sessvar("active_language_pack");
                    $active_language_pack = EnterpriseQueries::loadLanguagePack($use_language_pack, $locId);
                    setLanguageDictionary($active_language_pack, $locId);
                }
                //set_sessvar("active_language_pack", $active_language_pack);
                //set_sessvar("active_language_pack_id", $use_language_pack);
            }
            set_sessvar("admin_dataname",$company_info['data_name']);
            set_sessvar("admin_database","poslavu_".$company_info['data_name']."_db");
            set_sessvar("admin_companyid",$company_info['id']);
            set_sessvar("admin_chain_id",$company_info['chain_id']);
            set_sessvar("admin_version",$company_info['version_number']);
            set_sessvar("admin_dev",$company_info['dev']);
            set_sessvar("admin_company_name",$company_info['company_name']);
            set_sessvar("admin_loc_id", $locId);
            setActiveLanguagePackId($use_language_pack, $users_login_id, $locId);
            //set_sessvar("active_language_pack_id", $use_language_pack);
            $set_username = admin_info('username');
            if (strstr($set_username, ":"))
            {
                $split_un = explode(":", $set_username);
                $set_username = $split_un[1];
            }

            if ($company_info['id'] != admin_info('root_company_id'))
            {
                $set_username = $company_info['data_name'].":".$set_username;
            }

            set_sessvar("admin_username",$set_username);

            EnterpriseQueries::logChangeActiveChainAccount("To", $company_info['company_name'], $company_info['data_name']);

            EnterpriseQueries::logChangeActiveChainAccount("From", $from_company_name, $from_dataname);
        }

        return $company_info;
    }


}