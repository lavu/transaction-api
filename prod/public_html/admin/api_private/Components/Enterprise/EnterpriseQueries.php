<?php

/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 5/8/17
 * Time: 10:35 AM
 */
class EnterpriseQueries
{
	public static function getCountryNamesFull(){
		$query_string = "SELECT `name` FROM `poslavu_MAIN_db`.`countries`";
		$result = DBConnection::adminQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getCountryNamesAbbreviated(){
		$query_string = "SELECT `code` FROM `poslavu_MAIN_db`.`countries`";
		$result = DBConnection::adminQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getCountries(){
		$query_string = "SELECT * FROM `poslavu_MAIN_db`.`countries`";
		$result = DBConnection::adminQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getRestaurantByID($args) {
		// Couldn't do a SELECT * because the binary chars in data_access and jkey were causing problems
		// with the results appearing blank, so I just tried to pick the fields most likely to be used
		$query_string = "SELECT `id`, `data_name`, `company_name`, `created`, `last_activity`, `chain_id`, `chain_name`, `email`, `phone`, `distro_code`, `modules`, `disabled`, `dev`, `demo`, `version_number`, `test`  FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = [id]";
		$result = DBConnection::adminQuery($query_string, $args);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getRestaurantIDAndTitleByDataname($dataname) {
		$query_string = "SELECT `id` AS `restaurantid`, `company_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` ='" . $dataname."'";
		$result = DBConnection::adminQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getChainIDByRestaurantID($id){
		$query_string = "SELECT `chain_id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = " . $id;
		$result = DBConnection::adminQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getRestaurantChainsByDataname($dataname){ //given a dataname, get all restaurants in the same chain as the dataname, including the restaurant supplied.
		$thisChainIDQuery = "SELECT `chain_id` FROM `poslavu_MAIN_db`.`restaurants` where `data_name` = '".$dataname."' LIMIT 1";
		$chainID = MarshallingArrays::resultToArray(DBConnection::adminQuery($thisChainIDQuery));
		$chainID = $chainID[0]['chain_id'];
		if(isset($chainID) && $chainID != "")
		{
			$query_string = " SELECT `id`, `company_name` AS `name`, `data_name` AS dataname FROM `poslavu_MAIN_db`.`restaurants` WHERE `chain_id` = " . $chainID;
			$result = DBConnection::adminQuery($query_string);
			return MarshallingArrays::resultToArray($result);
		}
		else return "mysql-result: failure - Please ensure the current restaurant is associated with a restaurant chain.";
	}

	public static function listChainedLocations($dataname){
		$thisChainIDQuery = "SELECT `chain_id` FROM `poslavu_MAIN_db`.`restaurants` where `data_name` = '".$dataname."' LIMIT 1";
		$chainID = MarshallingArrays::resultToArray(DBConnection::adminQuery($thisChainIDQuery));
		$chainID = $chainID[0]['chain_id'];
		if($chainID == ""){
			return array();
		}
		$query = "SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` where `chain_id` = ".$chainID;
		$ids = MarshallingArrays::resultToArray(DBConnection::adminQuery($query));
		return $ids;
	}

	public static function getLanguagePackInfo(){
		$query = "SELECT `use_language_pack` FROM `locations` WHERE `id` = 1";
		$result = DBConnection::clientQuery($query);
		$resultArray = MarshallingArrays::resultToArray($result);

		$languagePackID = $resultArray[0]['use_language_pack'];
		$languageQuery = "SELECT * FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` = $languagePackID";
		$languageResult = DBConnection::adminQuery($languageQuery);
		return MarshallingArrays::resultToArray($languageResult);
	}

	public static function logChangeActiveChainAccount($to_or_from, $cname, $dname)
	{
		$l_vars = array();
		$q_fields = "";
		$q_values = "";
		$l_vars['action'] = "Switched Chain Account";
		$l_vars['user'] = admin_info('username');
		$l_vars['user_id'] = admin_info('loggedin');
		$l_vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$l_vars['order_id'] = "0";
		$l_vars['data'] = admin_info('fullname');
		$l_vars['detail1'] = "Access level: ".admin_info('access_level');
		$l_vars['detail2'] = $to_or_from." ".$cname." (".$dname.")";
		$l_vars['loc_id'] = sessvar("locationid");
		$keys = array_keys($l_vars);
		foreach ($keys as $key) {
			if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
			$q_fields .= "`$key`";
			$q_values .= "'[$key]'";
		}
		$data_name = admin_info('dataname');
		$l_vars['dataname'] = $data_name;
		$query = "INSERT into `admin_action_log` ($q_fields, `server_time`) values($q_values, now())";
		$result = DBConnection::clientQuery($query, $l_vars);
		if($result){
			return true;
		}
		else return false;
		if ($to_or_from == "To") {
			$query = "UPDATE `poslavu_MAIN_db`.`restaurants` SET `last_activity` = now() WHERE `data_name` = '". $dname ."'";
			$result = DBConnection::clientQuery($query);
		}
	}

	public static function getLocationInfo(){
		$query = "SELECT * FROM `locations` WHERE `_disabled` != '1' ORDER BY `title` ASC";
		$result = DBConnection::clientQuery($query);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getLocationConfigDetails($locId = 1){

			$query = "SELECT `setting`, `value` FROM  `config` WHERE `type` = 'location_config_setting' AND `location` = '". $locId ."'";
			$result = DBConnection::clientQuery($query);
			return MarshallingArrays::resultToArray($result);
	}

	public static function getUserLanguageId($loggedinId){
		$query = "SELECT `use_language_pack` FROM `users` WHERE `id` = '".$loggedinId . "'";
		$result = DBConnection::clientQuery($query);
		return MarshallingArrays::resultToArray($result);
	}

	public static function loadLanguagePack($pack_id, $loc_id) // take a given language pack ID and load it
	{
		$active_language_pack = array();
		if ($pack_id != 1)
		{
			if ($pack_id == 0)
			{
				$query = "SELECT * FROM `custom_dictionary` WHERE `loc_id` = '". $loc_id . "' AND (`used_by` = '' OR `used_by` = 'cp')";
				$result = DBConnection::clientQuery($query);
			}
			else
			{
				$query = "SELECT * FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '". $pack_id . "' AND (`used_by` = '' OR `used_by` = 'cp')";
				$result = DBConnection::adminQuery($query);
			}
			$resultArray = MarshallingArrays::resultToArray($result);
			foreach ($resultArray as  $languageData)
			{
				if ($languageData['tag'] != '') {
					$key = $languageData['tag'];
				} else {
					$key = $languageData['word_or_phrase'];
				}
				$active_language_pack[$key] = $languageData['translation'];
			}
		}

		return $active_language_pack;
	}
}