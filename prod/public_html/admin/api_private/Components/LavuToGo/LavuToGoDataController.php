<?php
class LavuToGoDataController extends DataControllerBase {

	public function __construct($request) {
		$this->m_databaseName = 'poslavu_MAIN_db';
		$clientConnection = DBConnection::getConnectionByType('admin');

		$reflectionClass = new ReflectionClass(__CLASS__);

		$this->m_request = $request;
		$this->m_actionsAvailable = $this->getAvailableActions($reflectionClass);
		$this->m_api_response_arr = array();
	}

	private function getAvailableActions($reflectionClass){
		$availableActions = array();
		$reflectionMethods = $reflectionClass->getMethods();
		foreach($reflectionMethods as $currReflectionMethod){
			if($currReflectionMethod->isProtected() && $currReflectionMethod->class != 'DataControllerBase'){
				if(strpos($currReflectionMethod->name, 'action_') === 0){
					$availableActions[] = $currReflectionMethod->name;
				}
			}
		}
		return $availableActions;
	}

	protected function action_getDataName($args) {
	    $onFailure = [ "status" => "F", "msg" => "Lavutogoname doesnot exists"];
		$lavuToGoName = isset($args['lavu_togo_name']['lavu_togo_name']) && strlen(trim($args['lavu_togo_name']['lavu_togo_name'])) > 0 ? $args['lavu_togo_name']['lavu_togo_name'] : false;

		if ($lavuToGoName) {
			$commonQueries = new CommonQueries();
		    $whereColumns = ["lavu_togo_name" => $lavuToGoName];
		    $selectColumns = [
		    	"dataname",
		    	"restaurantid",
		    	"locationid",
		    	"title",
		    	"address",
		    	"city",
		    	"state",
		    	"zip",
		    	"country",
		    	"phone",
		    	"email",
		    	"lavu_togo_name"
		    ];

		    $locationDetails = $commonQueries->getRowsWithRequiredColumnsMainDB('restaurant_locations', $selectColumns, $whereColumns);

		    if (empty($locationDetails)) {
		        return $onFailure;
		    } else {
		    	$companyDetails = array();
			foreach ($locationDetails as $restaurantData) {
				$companyDetails[] = (object) LocationDataController::getLocationAddressCompanySettings($restaurantData);
			}
		        return $companyDetails;
		    }
		} else {
		    return $onFailure;
		}
	}

	/* userSignin Function */
	protected function action_userSignin($args){
		//echo "<pre>";print_r($args);exit;
		require_once(__DIR__.'/../../../cp/resources/mealplan_functions.php');
		
		if(!isset($args['signInDetails']['languageid']) || $args['signInDetails']['languageid'] == '') {
		    $args['signInDetails']['languageid'] = '1';
		}
		if(!isset($args['signInDetails']['email']) || empty($args['signInDetails']['email'])){
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['signInDetails']['languageid'], "Email Address is required"));
		}
		if(!isset($args['signInDetails']['password']) || empty($args['signInDetails']['password'])){
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['signInDetails']['languageid'], "Password is required"));
		}
		if (!filter_var($args['signInDetails']['email'], FILTER_VALIDATE_EMAIL)) {
		    return $return = array("Status"=>"F", "msg"=>$args['signInDetails']['email'] .' '. apiSpeak($args['signInDetails']['languageid'], "is not a valid email address"));
		}
		
		$whereColumnArr=array("email"=>$args['signInDetails']['email']);
		$columnNameArr=array("email","id");
		$result = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
		if(empty($result)){
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['signInDetails']['languageid'], "Incorrect Email Address"));
		}
		$whereColumnArr=array("password"=>md5($args['signInDetails']['password']));
		$columnNameArr=array("password");
		$result = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
		if(empty($result)){
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['signInDetails']['languageid'], "Incorrect Password"));
		}
		$email = $args['signInDetails']['email'];
		$pwd = $args['signInDetails']['password'];
		$pwdd=md5($pwd);
		$whereColumnArr=array("email"=>$email,"password"=>$pwdd);
		$columnNameArr=array("id");
		$result = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
		if(isset($result[0]['id'])){
			$users_login_history = array("user_id"=>$result[0]['id'],"loggedin_time"=>"","loggedout_time"=>"","loggin_status"=>"S","created_date"=>"");
			$insertRows = array();
			$columnNames = array_keys($users_login_history);
			$insertRows[0]=array_values($users_login_history);
			CQ::insertArray('lavutogo.users_login_history', $columnNames, $insertRows, false);
			$whereColumnArr=array("user_id"=>$result[0]['id']);
			$columnNameArr=array("id", "user_id", "first_name", "last_name", "email", "address", "city", "state", "zipcode", "phone", "mobile", "preferred_language", "mealplan_user_id");
			$result = CQ::getRowsWithRequiredColumnsOtherDB('users_info',$columnNameArr,$whereColumnArr,'','','lavutogo');

			if(!empty($result)) {
				require_once(__DIR__.'/../../../../inc/olo/jwt/jwt.php');
				$jwtOlo = new jwtOlo();
				$jwtJson = $jwtOlo->getJwt($result[0]);
				$jwt = json_decode($jwtJson, true);

				$token_expire='0';
				$token_created_date=time();
				$update_token=array("token"=>$jwt['tokenId'],"token_expire"=>'0',"token_created_date"=>$token_created_date);
				$userResult = CQ::updateArray('lavutogo.users', $update_token, "where `id`='" .$result[0]['user_id']. "'", false);
				// LP-4302 - Add mealplan info to userSignIn response
				if (empty($result[0]['mealplan_user_id'])) {
                    $mealPlanId = CQ::updateUserMealPlanId([
						"email" => $args['signInDetails']['email'],
						'updateRecordById' => $result[0]['id']
					]);
                    $result[0]['mealplan_user_id'] = empty($mealPlanId) ? "" : $mealPlanId;
                }
                $result[0]['mealplan'] = isset($result[0]['mealplan_user_id']) ? mealPlanEnable($args['dataname'], $result[0]['mealplan_user_id']) : "";              
                return $return = array("Status"=>"S", "msg"=>apiSpeak($args['signInDetails']['languageid'], "Login Successful") ,"UserAccountDetails"=>$result[0], "jwt"=>$jwt['jwt'], "tokenId"=>$jwt['tokenId']);
			}
			else {
			    return $return = array("Status"=>"S", "msg"=>apiSpeak($args['signInDetails']['languageid'], "Login Successful"));
			}
		}
		else {
			$whereColumnArr=array("email"=>$email);
			$columnNameArr=array("id");
			$result = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
			if(isset($result[0]['id'])) {
				$users_login_history = array("user_id"=>$result[0]['id'],"loggedin_time"=>"","loggedout_time"=>"","loggin_status"=>"F","created_date"=>"");
				$insertRows = array();
				$columnNames = array_keys($users_login_history);
				$insertRows[0]=array_values($users_login_history);
				CQ::insertArray('lavutogo.users_login_history', $columnNames, $insertRows, false);
				return $return = array("Status"=>"F", "msg"=>apiSpeak($args['signInDetails']['languageid'], "Invalid Email Address or Password"));
			}
		}

	} /* End userSignin Function */


	// userRegistration Function
	protected  function action_userRegistration($args) {
		$commonQueries = new CommonQueries();
		if(!isset($args['regDetails']['languageid']) || $args['regDetails']['languageid'] == '') {
		    $args['regDetails']['languageid'] = '1';
		}
		if(!isset($args['regDetails']['fname']) || empty($args['regDetails']['fname'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['regDetails']['languageid'], "First Name is required"));
		}
		if(!isset($args['regDetails']['lname']) || empty($args['regDetails']['lname'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['regDetails']['languageid'], "Last Name is required"));
		}
		if(!isset($args['regDetails']['mobile']) || empty($args['regDetails']['mobile'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['regDetails']['languageid'], "Telephone Number is required"));
		}
		if(!isset($args['regDetails']['email']) || empty($args['regDetails']['email'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['regDetails']['languageid'], "Email Address is required"));
		}
		if (!filter_var($args['regDetails']['email'], FILTER_VALIDATE_EMAIL)) {
		    return $return = array("Status"=>"F", "msg"=>$args['regDetails']['email']. ' ' .apiSpeak($args['regDetails']['languageid'], "is not a valid email address"));
		}
		if(!isset($args['regDetails']['password']) || empty($args['regDetails']['password'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['regDetails']['languageid'], "Password is required"));
		}
		if(!isset($args['cpwd']['cpwd']) || empty($args['cpwd']['cpwd'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['regDetails']['languageid'], "Confirm Password is required"));
		}
		$whereColumnArr=array("email"=>$args['regDetails']['email']);
		$columnNameArr=array("email");
		$result = $commonQueries->getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
		if(!empty($result)) {
		    return $return = array("Status"=>"F", "msg"=>$args['regDetails']['email']. ' '.apiSpeak($args['regDetails']['languageid'], "already exists"));
		}
		$pwd=$args['regDetails']['password'];
		$cpwd=$args['cpwd']['cpwd'];
		if($pwd!=$cpwd){
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['regDetails']['languageid'], "Password and Confrim Password doesnot match"));
		}
		// Validate the new password START LP-4675
		$specialchar = preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=Â§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@_!-]{8,15}$/', $pwd);
		if(!$specialchar || strlen($pwd) < 8) {
		    $msg= apiSpeak($args['regDetails']['languageid'], 'Invalid password format');
			return $return = array("Status"=>"F", "msg"=>$msg);
		}
		// Validate the new password END LP-4675
		if($pwd==$cpwd){
			$insertRows=array(
				'email'=>$args['regDetails']['email'],
				'username'=>$args['regDetails']['email'],
				'password'=>md5($args['regDetails']['password']),
				'login_attempt'=>$args['regDetails']['login_attempt'],
				'login_status'=>$args['regDetails']['login_status'],
				'token'=>$args['regDetails']['token'],
				'token_expire'=>0,
				'token_created_date'=>'',
				'created_date'=>$args['regDetails']['created_date'],
				'last_modified_date'=>$args['regDetails']['last_modified_date']
			);

			$columnNames = array_keys($insertRows);
			$inser[0] = array_values($insertRows);
			$result = $commonQueries->insertArray('lavutogo.users', $columnNames, $inser, false);
			$res= MA::resultToInsertIDUpdatedRows($result,true);
			if(count($result)>0){
				$whereColumnArr=array("email"=>$args['regDetails']['email']);
				$columnNameArr=array("id");
				$userResultRow = $commonQueries->getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
				$insertRows_usersInfo=array(
					'user_id'=>$userResultRow[0]['id'],
					'email'=>$args['regDetails']['email'],
					'first_name'=>$args['regDetails']['fname'],
					'last_name'=>$args['regDetails']['lname'],
					'address' => '',
                    'state' => '',
                    'city' => '',
                    'zipcode' => 0,
                    'mobile' => $args['regDetails']['mobile'],
                    'created_date' => $args['regDetails']['created_date'],
                    'last_modified_date' => $args['regDetails']['last_modified_date'],
                    'preferred_language' => 0,
					'mobile'=>$args['regDetails']['mobile']
				);

				if(isset($args['addressInformation']) && !empty($args['addressInformation'])) {
					if (!empty($args['addressInformation']['address_1'])) {
	                    $insertRows_usersInfo['address'] = implode(', ', [
	                    	$args['addressInformation']['address_1'],
	                    	$args['addressInformation']['address_2']
	                    ]);
	                }
	                if(!empty($args['addressInformation']['city'])) {
	                    $insertRows_usersInfo['city'] = $args['addressInformation']['city'];
	                }
	                if(!empty($args['addressInformation']['state'])) {
	                    $insertRows_usersInfo['state'] = $args['addressInformation']['state'];
	                }
	                if(!empty($args['addressInformation']['zipcode'])) {
	                    $insertRows_usersInfo['zipcode'] = $args['addressInformation']['zipcode'];
	                }
				}
				$commonQueries->updateUserMealPlanId($args['regDetails']['email']);
				$columnNamesUsersInfo = array_keys($insertRows_usersInfo);
				$inserUsersInfo[0] = array_values($insertRows_usersInfo);
				$result = $commonQueries->insertArray('lavutogo.users_info', $columnNamesUsersInfo, $inserUsersInfo, false);
				if (isset($args['addressInformation']) && !empty($args['addressInformation'])) {
                    $args['addressInformation']['user_id'] = $userResultRow[0]['id'];
                    $commonQueries->insertLavuToGoUserAddress($args['addressInformation']);
                }

                return $return = array("Status"=>"S", "msg"=>apiSpeak($args['regDetails']['languageid'], "Registered Successfully"), "InsertID" =>$res['msg']['InsertID']);
			}
			else {
			    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['regDetails']['languageid'], "Email Address already exists"));
			}
		}

	}  //End userRegistration Function

		protected function action_getAuthSession($args){
			$whereColumnArr=array("id"=>$args['authSession']['userId']);
			$columnNameArr=array("token", "token_expire", "token_created_date");
			$userResultRow = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
			if(!empty($userResultRow)){
				if($userResultRow[0]['token_expire'] == 0){
					$tokenTimeStamp = time();
					//echo $tokenTimeStamp."-".$userResultRow[0]['token_created_date'];
					$tokenTimeStampDiff = $tokenTimeStamp - $userResultRow[0]['token_created_date'];
					if($tokenTimeStamp >= 900000){
						$updateTimeStamp = array("token_created_date" => time());
						$result = CQ::updateArray('lavutogo.users', $updateTimeStamp, "where `id`='" . $args['authSession']['userId'] . "'", false);
					}else{
						$updateTokenStatus = array("token_expire" => 1);
						$result = CQ::updateArray('lavutogo.users', $updateTokenStatus, "where `id`='" . $args['authSession']['userId'] . "'", false);
						return $return = array("Status"=>"F", "msg"=>"Session expired. Please login again");
					}
				}else{
					return $return = array("Status"=>"F", "msg"=>"Session expired. Please login again");
				}
			}
			return "authenticated";
		}

	protected  function action_myAccount($args){
	    if(!isset($args['users_info']['languageid']) || $args['users_info']['languageid'] == '') {
	        $args['users_info']['languageid'] = '1';
	    }
		$user_id=$args['users_info']['user_id'];
		$cpwd=$args['users_info']['curpwd'];
		$newpwd=$args['users_info']['newpwd'];
		$conpwd=$args['users_info']['conpwd'];
		
		if(isset($cpwd)){
			if(!isset($cpwd)){
			    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['users_info']['languageid'], "Current Password is required"));
			}
			if(!isset($newpwd) || empty($newpwd)){
			    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['users_info']['languageid'], "New Password is required"));
			}
			if(!isset($conpwd) || empty($conpwd)){
			    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['users_info']['languageid'], "Confirm New Password is required"));
			}
			if($cpwd==$newpwd){
			    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['users_info']['languageid'], "Current Password and New Password shouldnot match"));
			}
			$pwd=md5($args['users_info']['curpwd']);
			$whereColumnArr=array("password"=>$pwd,"id"=>$args['users_info']['user_id']);
			$columnNameArr=array("password");
			$result = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
			if(empty($result)){
				return $return = array("Status"=>"F", "msg"=>apiSpeak($args['users_info']['languageid'], "Invalid Current Password"));
			}

			if($args['users_info']['newpwd']!=$args['users_info']['conpwd']){
			    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['users_info']['languageid'], "New Password and Confirm New Password doesnot match"));
			}
			// Validate the new password START LP-4675
			$specialchar = preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=Â§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@_!-]{8,15}$/', $newpwd);
			if(!$specialchar || strlen($newpwd) < 8) {
			    $msg = apiSpeak($args['users_info']['languageid'], 'Invalid password format');
				return $return = array("Status"=>"F", "msg"=>$msg);
			}
			// Validate the new password END LP-4675
			$update_users=array(
					"password" =>md5($args['users_info']['newpwd'])

			);

			$whereColumnArr=array("user_id"=>$user_id);
			$columnNameArr=array("user_id");
			$result = CQ::getRowsWithRequiredColumnsOtherDB('users_info',$columnNameArr,$whereColumnArr,'','','lavutogo');
				//print_r($resuilt);
			if(isset($result[0]['user_id'])){
				//echo "hi";
				$fname=$args['users_info']['first_name'];
				$lname=$args['users_info']['last_name'];
				$email=$args['users_info']['email'];
				$address=$args['users_info']['address'];
				$city=$args['users_info']['city'];
				$state=$args['users_info']['state'];
				$zipcode=$args['users_info']['zipcode'];
				$phone=$args['users_info']['phone'];
				$mobile=$args['users_info']['mobile'];
				$preferred_language=$args['users_info']['preferred_language'];
				$updateltg = array(
					"first_name" =>$fname,
					"last_name" =>$lname,
					"email"=>$email,
					"address" =>$address,
					"city"=>$city,
					"state" => $state,
					"zipcode" =>$zipcode,
					"phone"=>$phone,
					"mobile"=>$mobile,
					"preferred_language"=>$preferred_language
				);
				$result = CQ::updateArray('lavutogo.users_info', $updateltg, "where `user_id`='" . $user_id . "'", false);
				$res= MA::resultToInsertIDUpdatedRows($result,true);
				if($res['msg']['RowsUpdated']>0){
					$result = CQ::updateArray('lavutogo.users', $update_users, "where `id`='" .$args['users_info']['user_id'] . "'", false);
					$whereColumnArr=array("user_id"=>$user_id);
					$columnNameArr=array("user_id","first_name","last_name","email","address","city","state","zipcode","phone","mobile","preferred_language");
					$myAccountDetails = CQ::getRowsWithRequiredColumnsOtherDB('users_info',$columnNameArr,$whereColumnArr,'','','lavutogo');
					if(!empty($result)) {
					    return $return = array("Status"=>"S", "msg"=>apiSpeak($args['users_info']['languageid'], "Account details updated successfully"), "UserAccountDetails"=>$myAccountDetails[0],"RowsUpdated" =>$res['msg']['RowsUpdated']);
					}
				}
				else {
					$result = CQ::updateArray('lavutogo.users', $update_users, "where `id`='" .$args['users_info']['user_id'] . "'", false);
					$res= MA::resultToInsertIDUpdatedRows($result,true);
					if($res['msg']['RowsUpdated']>0){
						$whereColumnArr=array("user_id"=>$user_id);
						$columnNameArr=array("user_id","first_name","last_name","email","address","city","state","zipcode","phone","mobile","preferred_language");
						$myAccountDetails = CQ::getRowsWithRequiredColumnsOtherDB('users_info',$columnNameArr,$whereColumnArr,'','','lavutogo');
						if(!empty($result)) {
						    return $return = array("Status"=>"S",  "msg"=>apiSpeak($args['users_info']['languageid'], "Account preference updated successfully"), "UserAccountDetails"=>$myAccountDetails[0],"RowsUpdated" =>$res['msg']['RowsUpdated']);
						}
					}
				}
			}
			else {
			    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['users_info']['languageid'], "Some error occured. Please try again"));
			}
		}
		if (!isset($args['users_info']['user_email']) && !isset($args['users_info']['curpwd'])&& !isset($args['users_info']['conpwd'])&& !isset($args['users_info']['newpwd'])) {
			$failure = array(
				"Status"=>"F",
			    "msg"=>apiSpeak($args['users_info']['languageid'], "User details not found"),
				"RowsUpdated" => 0
			);

			$user_id=$args['users_info']['user_id'];
			$whereColumnArr=array("user_id"=>$user_id);
			$columnNameArr=array("user_id");
			$result = CQ::getRowsWithRequiredColumnsOtherDB('users_info',$columnNameArr,$whereColumnArr,'','','lavutogo');
			if (isset($result[0]['user_id'])) {
				$fname=$args['users_info']['first_name'];
				$lname=$args['users_info']['last_name'];
				$email=$args['users_info']['email'];
				$address=$args['users_info']['address'];
				$city=$args['users_info']['city'];
				$state=$args['users_info']['state'];
				$zipcode=$args['users_info']['zipcode'];
				$phone=$args['users_info']['phone'];
				$mobile=$args['users_info']['mobile'];
				$preferred_language=$args['users_info']['preferred_language'];
				$updateltg = array(
					"first_name" =>$fname,
					"last_name" =>$lname,
					"email"=>$email,
					"address" =>$address,
					"city"=>$city,
					"state" => $state,
					"zipcode" =>$zipcode,
					"phone"=>$phone,
					"mobile"=>$mobile,
					"preferred_language"=>$preferred_language
				);
				//Update DATA in lavutogo users_info table
				$result = CQ::updateArray('lavutogo.users_info', $updateltg, "where `user_id`='" . $user_id . "'", false);
				$res = MA::resultToInsertIDUpdatedRows($result, true);
				CQ::updateArray('lavutogo.users', [
						"email" => $email,
						"username" => $email,
					], "where `id`='" . $user_id . "'", false
				);
				$whereColumnArr=array("user_id" => $user_id);
				$columnNameArr=array(
					"user_id",
					"first_name",
					"last_name",
					"email",
					"address",
					"city",
					"state",
					"zipcode",
					"phone",
					"mobile",
					"preferred_language",
					"mealplan_user_id"
				);
				$myAccountDetails = CQ::getRowsWithRequiredColumnsOtherDB('users_info', $columnNameArr, $whereColumnArr, '', '', 'lavutogo');
				
				if (!empty($result)) {
					// LP-4302 - Add mealplan info to userSignIn response
					$myAccountDetails[0]['mealplan'] = $this->getMealPlanInfo($myAccountDetails[0]['mealplan_user_id']);
					return array(
						"Status"=> "S",
					    "msg"=> $res['msg']['RowsUpdated'] > 0 ? apiSpeak($args['users_info']['languageid'], 'Account details updated successfully') : apiSpeak($args['users_info']['languageid'], 'No change in account details since last update'),
						"UserAccountDetails"=> $myAccountDetails[0],
						"RowsUpdated" => $res['msg']['RowsUpdated']
					);
				} else {
					return $failure;
				}
			}
			else {
				return $failure;
			}

		}

	}

	private function getMealPlanInfo($userId) {
		require_once('/home/poslavu/public_html/admin/cp/resources/mealplan_functions.php');
		$mealPlanUser = getUserByID($userId);
		$mealPlanInfo = array(
			'userId'          => $userId,
			'enabled'         => !$mealPlanUser['_deleted'],
			'badge_number'    => (empty($mealPlanUser['badge_id']) ? '0' : $mealPlanUser['badge_id']),
			'stipend_balance' => (double) getStipendBalanceForUser($mealPlanUser),
			'card_balance'    => (double) $mealPlanUser['cash_balance'],
		);
		return $mealPlanInfo;
	}
	/* START user Forgot password Function */
	protected function action_forgotPassword($args){
	    if(isset($args['forgotPasswordDetails']['languageid']) && $args['forgotPasswordDetails']['languageid'] != '') {
	        $languageid = $args['forgotPasswordDetails']['languageid'];
	    }
	    else {
	        $languageid = 1;
	    }
		$template_content = file_get_contents ( '/home/poslavu/private_html/email_template/reset_password_tpl.html');
	    if(!isset($args['forgotPasswordDetails']['email']) || empty($args['forgotPasswordDetails']['email'])){
	        return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid,"Email Address is required"));
	    }
	    if (!filter_var($args['forgotPasswordDetails']['email'], FILTER_VALIDATE_EMAIL)) {
	        return $return = array("Status"=>"F", "msg"=>$args['forgotPasswordDetails']['email'] .' '.apiSpeak($languageid,"is not a valid email address"));
	    }
	    $whereColumnArr=array("email"=>$args['forgotPasswordDetails']['email']);
	    $columnNameArr=array("email","id");
	    $result = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
	    if(empty($result)){
	        return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid,"Incorrect Email Address"));
	    }
	    if(!isset($args['forgotPasswordDetails']['dataname']) || empty($args['forgotPasswordDetails']['dataname']))
	    {
	        return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Dataname is required"));
	    }

	    $email = $args['forgotPasswordDetails']['email'];
	    $whereColumnArr=array("email"=>$email);
	    $columnNameArr=array("id");
	    $result = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');

	    $whereUsercol = array("user_id" => $result[0]['id']);
	    $columnNameUser=array("first_name");
	    $resultInfo = CQ::getRowsWithRequiredColumnsOtherDB('users_info',$columnNameUser,$whereUsercol,'','','lavutogo');

	    if(isset($result[0]['id'])){
	        if(isset($args['forgotPasswordDetails']['dataname']))
	        {
	            $dataname= $args['forgotPasswordDetails']['dataname'];
	            // get lavo to go name
	            $restaurantsDataNameNameArr = array("lavu_togo_name");
	            $whereDataNameColumnArr = array("dataname"=>$dataname);
	            $restaurantDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsDataNameNameArr, $whereDataNameColumnArr, '','','MAIN');
	            $lavu_togo_name = $restaurantDataNameRowsResult[0]['lavu_togo_name'];
	        }else{
	            return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Some error occured. Please try again"));
	        }
	        //get the config settings for token expire time
	        $whereConfigDataNameColumnArr = array('ltg_password_token_expire');
	        $restaurantsConfigDataNameNameArr = array("setting");
	        $restaurantConfigDataNameRowsResult = CQ::getRowsWhereColumnIn('config', $restaurantsConfigDataNameNameArr, $whereConfigDataNameColumnArr, $dataname);

	        $token_created_date=time();
	        $character_array = array_merge(range('a', 'z'), range(0, 9));
	        $string = "";
	        for($i = 0; $i < 25; $i++) {
	            $string .= $character_array[rand(0, (count($character_array) - 1))];
	        }
	        $cryptKey =$string.time();
	        $token = base64_encode($cryptKey);
	        $expairydays = $restaurantConfigDataNameRowsResult[0]['value'];
	        $token_expire= $token_created_date + (60*60*24) * $expairydays;
	        $token_created_date=time();

	        $update_token=array("token"=>$token,"token_expire"=>$token_expire,"token_created_date"=>$token_created_date);
	        $Result = CQ::updateArray ( 'lavutogo.users', $update_token, "where `id`='" .$result[0]['id']. "'",false);
	        //email body

	        $emailto = $email;
	        $subject = 'Reset Password';
	        $from = 'noreply@lavu.com';
	        $headers  = 'MIME-Version: 1.0' . "\r\n";
	        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	        // Create email headers
	        $headers .= 'From: '.$from."\r\n".
	        'X-Mailer: PHP/' . phpversion();

	        $siteurl_arr = parse_url($_SERVER['HTTP_REFERER']);
			$actual_link = $siteurl_arr['scheme']. "://".$siteurl_arr["host"];
			$reset_link = $actual_link.'/'.$lavu_togo_name.'/password/reset/'.$token;
	        $search_keys = array('{{username}}','{{reset_link}}');
	        $replace_keys = array($resultInfo[0]['first_name'],$reset_link);
	        $emailcontent= str_replace($search_keys,$replace_keys,$template_content);
	        // Sending email
	        if(@mail($emailto, $subject, $emailcontent, $headers)){
	            return $return = array("Status"=>"S", "msg"=>apiSpeak($languageid, "Mail sent successfully to your registered Email Address"));
	        }
	        else{
	            return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Some error occured while sending email"));
	        }
	    }
	} /* End user Forgot password Function */

	/* START user Update password Function */
	protected  function action_updatePassword($args){
	    $languageid = 1;
	    if(isset($args['updatePasswordDetails']['languageid']) && $args['updatePasswordDetails']['languageid'] != '') {
	        $languageid = $args['updatePasswordDetails']['languageid'];
	    }
	    $token=$args['updatePasswordDetails']['token'];
	    $newpwd=$args['updatePasswordDetails']['newpwd'];
	    $conpwd=$args['updatePasswordDetails']['conpwd'];

	    $whereColumnArr=array("token"=>$token);
	    $columnNameArr=array("token_expire", "token_created_date");
	    $userResultRow = CQ::getRowsWithRequiredColumnsOtherDB('users',$columnNameArr,$whereColumnArr,'','','lavutogo');
	    if(!empty($userResultRow)){
	        $tokenTimeStamp = time();
	        $tokenTimeStampDiff = $tokenTimeStamp - $userResultRow[0]['token_created_date'];
	        $expiretimestamp = 1800; //30mins
	        if($tokenTimeStampDiff > $expiretimestamp)
	        {
	            return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Password reset link is expired"));
	        }

	        //START OTHER CONDITION
	        // Validate the new password START LP-4675
	        $specialchar = preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=Â§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@_!-]{8,15}$/', $newpwd);
	        if(!$specialchar || strlen($newpwd) < 8) {
	            $msg = apiSpeak($languageid, 'Invalid password format');
	        	return $return = array("Status"=>"F", "msg"=>$msg);
	        }
	        // Validate the new password END LP-4675
	        if(!isset($newpwd) || empty($newpwd)){
	            return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "New Password is required"));
	        }
	        if(!isset($conpwd) || empty($conpwd)){
	            return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Confirm New Password is required"));
	        }
	        if($args['updatePasswordDetails']['newpwd']!=$args['updatePasswordDetails']['conpwd']){
	            return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "New Password and Confirm New Password doesnot match"));
	        }
	        $update_users=array(
	            "password" =>md5($args['updatePasswordDetails']['newpwd']),
	            "token_expire" =>0
	        );
	        $result = CQ::updateArray ( 'lavutogo.users', $update_users, "where `token`='" .$token . "'",false);
	        $res= MA::resultToInsertIDUpdatedRows($result,true);
	        if($res['msg']['RowsUpdated']>0){
	            if(!empty($result)) {
	                return $return = array("Status"=>"S",  "msg"=>apiSpeak($languageid, "Account details updated successfully"));
	            }
	        }
	        else {
	            return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Some error occured. Please try again"));
	        }
	    }
	}/* End user Update password Function */
	
	/* START user MealPlan Details Function */
	protected function action_mealPlanDetails($args){
	    $user_id=$args['user_id'];
	    $whereColumnArr=array("user_id"=>$user_id);
	    $columnNameArr=array("user_id","first_name","last_name","email","mealplan_user_id");
	    $commonQueries = new CommonQueries();
	    $myAccountDetails = $commonQueries-> getRowsWithRequiredColumnsOtherDB('users_info',$columnNameArr,$whereColumnArr,'','','lavutogo');
	    $return = empty($myAccountDetails[0]['mealplan_user_id']) ? '' : $this->getMealPlanInfo($myAccountDetails[0]['mealplan_user_id']);
	    return $return;
	}/* End user MealPlan Details Function */

	/*
	* Generates users stored address list
	*
	* @param array $params  params should consist of userId and tokenId
	* @since Method is made available in 2.0.1
	* @return array
	*/
	protected function action_getUserAddressList($params) {
	    $languageid = 1;
	    if(isset($params['languageid']) && $params['languageid'] != '') {
	        $languageid = $params['languageid'];
	    }
		$commonQueries = new CommonQueries();
		$response = [ 'status' => 'success' ];

		$userId = isset($params['userId']) ? trim( $params['userId'] ): '';
		$tokenId = isset($params['tokenId']) ? trim( $params['tokenId'] ) : '';

		if(empty($userId))
		{
			$response['status'] = 'failure';
			$response['msg'] = apiSpeak($languageid, 'User Id is required');
		}
		else if(empty($tokenId))
		{
		    $response['status'] = 'failure';
		    $response['msg'] = apiSpeak($languageid, 'Token Id is required');
		}
		else
		{
			$response['data'] = $commonQueries->getListOfUserAddress($userId);
		}

		return $response;
	}

	/* to delete user address */
 	protected function action_deleteAddress($args) {
 	    $languageid = 1;
 	    if(isset($args['languageid']) && $args['languageid'] != '') {
 	        $languageid = $args['languageid'];
 	    }
 		$commonQueries = new CommonQueries();
		$marshallingArrays = new MarshallingArrays();
		$response = [ 'status' => 'failure' ];
 		$userId = isset($args['user_id']) ? trim( $args['user_id'] ): '';
		$tokenId = isset($args['tokenId']) ? trim( $args['tokenId'] ) : '';
		$recordId = isset($args['id']) ? $args['id'] : '';
		$data['_deleted'] = '1';
		$userAddresTable = 'users_address';
		$recordDetails = $commonQueries->updateArray(
						'lavutogo.'.$userAddresTable, 
						$data, 
						"where `id`='".$recordId."' AND user_id='".$userId."'", 
						false
					);
		$deleteStatus = $marshallingArrays->resultToInsertIDUpdatedRows($recordDetails,true);
		$response['status'] = 'success';
	        if( $deleteStatus['RowsUpdated'] > 0 )
	        {
	            $response['msg'] = apiSpeak($languageid, 'Address deleted successfully');
	        } else
	        {
	            $response['msg'] = apiSpeak($languageid, 'Some error occured. Please try again');
	        }	

	        if($response['status'] === 'success'){
			$response['data'] = $commonQueries->getListOfUserAddress($userId);
		 }	
		return $response;
 	}

 	/*
	* Save or update user address
	*
	* The function allows users to update record base on id & users id or save data on user id.
	* 
	* @params array $params  accepts list of data to update or save, user_id and tokenId is required to perform any actions
	* @since Method is made available in OLO 2.0.1 
	* @return array  returns save status and related data on request
	*/

	protected function action_saveAddress($params) {
	    $languageid = 1;
	    if(isset($params['languageid']) && $params['languageid'] != '') {
	        $languageid = $params['languageid'];
	    }
		$commonQueries = new CommonQueries();
		$marshallingArrays = new MarshallingArrays();
		$response = [ 'status' => 'failure' ];

		$userId = isset($params['user_id']) ? trim( $params['user_id'] ): '';
		$tokenId = isset($params['tokenId']) ? trim( $params['tokenId'] ) : '';
		$userListOfAddress = $commonQueries->getListOfUserAddress($userId);
		$isValidAddress = false;
		$addressNameExist = false;
		$addressName = strtolower(preg_replace('/\s+/', '', $params['name']));
		
		if(sizeof( $userListOfAddress ) > 0){
			foreach ($userListOfAddress as $value) {
				if(!$isValidAddress && (int)$value['id'] === (int)$params['id'])
				{
					$isValidAddress = true;
				}
				if( (int)$params['id'] !== (int)$value['id'] && $addressName === strtolower(preg_replace('/\s+/', '', $value['name'])))
				{
					$addressNameExist = true;
				}
			}
		}

		if( empty($userId) )
		{
		    $response['msg'] = apiSpeak($languageid, 'User Id is required');
		}
		else if( empty($tokenId) )
		{
		    $response['msg'] = apiSpeak($languageid, 'Token Id is required');
		}
		else
		{
			$userAddresTable = 'users_address';
			$data = $params;
			unset($data['tokenId']);
			unset($data['languageid']);
			if(!$commonQueries->isLavuToGoUser($userId))
			{
			    $response['msg'] = apiSpeak($languageid, 'User details not found');

			}else if( isset($params['id']) && $params['id'] !== '0')
			{

				if(!$isValidAddress)
				{
				    $response['msg'] = apiSpeak($languageid, 'Address not found');

				}else if($addressNameExist)
				{
				    $response['msg'] = apiSpeak($languageid, 'Address Name already exists');

				}else
				{
					$recordId = $params['id'];
					$currentDate = date('Y-m-d H:i:s');
					if((int) $params['is_primary'] === 1){
						$recordDetails = $commonQueries->unsetAllPrimaryAddress($userId);
					}

					$data['last_modified_date'] = $currentDate;
					$recordDetails = $commonQueries->updateArray(
						'lavutogo.'.$userAddresTable, 
						$data, 
						"where `id`='".$recordId."' AND user_id='".$userId."'", 
						false
					);

	        		$updateStatus = $marshallingArrays->resultToInsertIDUpdatedRows($recordDetails,true);

	        		$response['status'] = 'success';
	        		if( $updateStatus['RowsUpdated'] > 0 )
	        		{
	        		    $response['msg'] = apiSpeak($languageid, 'Address details updated successfully');
	        		}else
	        		{
	        		    $response['msg'] = apiSpeak($languageid, 'No change in address details since last update');
	        		}
				}
			}else
			{
				if($addressNameExist) {
				    $response['msg'] = apiSpeak($languageid, 'Address Name already exists');
				}else {
					$testParams = $data;
					foreach ($testParams as $key => $value) {
						$value = trim($value);
						if(!$value) {
							unset($testParams[$key]);
						}
					}
					if(sizeof($testParams) > 1) {
						$insertStatus= $commonQueries->insertLavuToGoUserAddress($data);
						if( $insertStatus['RowsUpdated'] > 0 ) {
		        			$response['status'] = 'success';
		        			$response['msg'] = apiSpeak($languageid, 'Address details added successfully');
		        			$data['id'] = $insertStatus['InsertID'];
		        		}else {
		        		    $response['msg'] = apiSpeak($languageid, 'Some error occured. Please try again');
		        		}
					}else {
					    $response['msg'] = apiSpeak($languageid, 'Invalid Address details');
					}
					
				}
			}
		}

		if($response['status'] === 'success'){
			$response['data'] = $commonQueries->getListOfUserAddress($userId);
		}
		return $response;
	}
}