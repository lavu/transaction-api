<?php
/*
* DataController for ORDER
*/

include_once (dirname(__FILE__)."/../PizzaCreator/PizzaCreator.php");
include_once (dirname(__FILE__)."/../PizzaCreator/PizzaMod.php");

include_once (dirname(__FILE__)."/../../Utils/JsonManager.php");
class OrdersDataController extends DataControllerBase {
	public static $decimal_char;
	public static $disable_decimal;
	public static $rounding_factor;
	public static $round_up_or_down;
    public $location_info =  array();
	public static $TYPE_FORCED_MODIFIERS = "TYPE_FORCED_MODIFIERS";
	public static $TYPE_OPTIONAL_MODIFIERS = "TYPE_OPTIONAL_MODIFIERS";
	public static $lavuToGoName = "Lavu To Go";
	public static $lavuToGoNameUnpaid = "Lavu To Go UNPAID";

	public function __construct($request, $dataname) {
		parent::__construct(new ReflectionClass(__CLASS__), $request, $dataname);
		// load global location decimal data
		$res = self::getDecimalData(1);
        global $location_info;
        $location_info['decimal_char'] = $res[0]['decimal_char'];
        $location_info['disable_decimal'] = $res[0]['disable_decimal'];
        $location_info['monitary_symbol'] = $res[0]['monitary_symbol'];
        $location_info['left_or_right'] = $res[0]['left_or_right'];
	}

	public static function getLocationData($dataname) {
		$whereColumnArr=array("_disabled"=>0);
		$columnNameArr=array(
			"id",
			"decimal_char",
			"disable_decimal",
			"rounding_factor",
			"round_up_or_down",
			"monitary_symbol"
		);
		$res = CQ::getRowsWithRequiredColumns('locations', $columnNameArr,$whereColumnArr, 'id', '', $dataname);
		return array_shift($res);
	}

	private function reserveItem86($itemArr) {
		foreach($itemArr as $item) {
			//Menu item 86 count reservation
			$reserve_status = self::reserveMenuItem86(array("itemid" => $item['itemid'], "qty" => $item['qty']));
			if($reserve_status) {
				//Forced Medifier 86 count reservation
				self::reserveForced86(array(
					"itemid" => $item['fmods_checked'],
					"qty" => $item['qty']
				));
				//optional Modifier 86 count reservation
				self::reserveOptional86(array(
					"itemid" => $item['omods_checked'],
					"qty" => $item['qty']
				));
			}
		}
	}

	private static function cleanReserveItems($order_id) {
		$order_details = OrdersQueries::checkOrder($order_id);
		$order_info = json_decode($order_details[0]['order_information'])->order_contents_information;
		foreach ($order_info as $info) {
			self::cleanReserveMenuItem86(array(
				"itemid" => $info->item_id,
				"qty" => $info->quantity
			)); // removing reservation in menuitem86
			if($info->modifier_list_id != '') {
				self::cleanReserveOptionalItem86(array(
					"itemid" => $info->modifier_list_id,
					"qty" => $info->quantity
				)); // removing reservation in optional modifiers item86
			}
			if($info->forced_modifier_list_id != '') {
				self::cleanReserveForcedItem86(array(
					"itemid" => $info->forced_modifier_list_id,
					"qty" => $info->quantity
				)); // removing reservation in forced modifiers item86
			}
		}
	}

	private static function cleanReserveOptionalItem86($item) {
		if(!empty($item) && !empty($item['itemid'])) {
			$menuItemRowsResult = CQ::getRowsWhereColumnIn('modifier_86', 'modifierID', array($item['itemid']));
			if(!empty($menuItemRowsResult)) {
				foreach ($menuItemRowsResult as $val) {
					$setColumnArr= array("reservation" => abs(($val['quantityUsed'] * $item['qty']) - $val['reservation']));
					CQ::updateArray ('modifier_86', $setColumnArr, "where `modifierID` = '" . $val['modifierID'] . "' and `inventoryItemID` = '" . $val['inventoryItemID'] . "' ");
				}
			}
		}
	}

	private static function cleanReserveMenuItem86($item) {
		$whereInventoryColumnArr = array('menuItemID' => $item['itemid'], '_deleted' => 0);
		$menuInventoryItemsColNameArr = array("inventoryItemID", "unitID", "quantityUsed", "reservation", "menuItemID");
		$menuInventorytemRowsResult = CQ::getRowsWithRequiredColumns('menuitem_86', $menuInventoryItemsColNameArr, $whereInventoryColumnArr);
		if(!empty($menuInventorytemRowsResult)) {
			foreach ($menuInventorytemRowsResult as $inv_val) {
				$setColumnArr= array("reservation" => abs(($inv_val['quantityUsed'] * $item['qty']) - $inv_val['reservation']));
				CQ::updateArray('menuitem_86', $setColumnArr, "where `menuItemID` = '" . $inv_val['menuItemID'] . "' and `inventoryItemID` = '" . $inv_val['inventoryItemID'] . "' ");
			}
		}
	}

	private static function cleanReserveForcedItem86($item) {
		if(!empty($item) && !empty($item['itemid'])) {
			$menuItemRowsResult = CQ::getRowsWhereColumnIn('forced_modifier_86', 'forced_modifierID', array($item['itemid']));
			if(!empty($menuItemRowsResult)) {
				foreach ($menuItemRowsResult as $val) {
					$setColumnArr= array("reservation" => abs(($val['quantityUsed'] * $item['qty']) - $val['reservation']));
					CQ::updateArray('forced_modifier_86', $setColumnArr, "where `forced_modifierID` = '" . $val['forced_modifierID'] . "' and `inventoryItemID` = '" . $val['inventoryItemID'] . "' ");
				}
			}
		}
	}

	private static function reserveMenuItem86($item) {
		$whereColumnArr = array('id' => $item['itemid'], '_deleted' => 0, 'ltg_display' => 1);
		$menuItemsColNameArr = array("id", "inv_count", "track_86_count");
		$menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnArr);
		$reserve_status = 0;
		if(!empty($menuItemRowsResult)) {
			foreach($menuItemRowsResult as $val) {
				if($menuItemRowsResult[0]['track_86_count'] == '86count' && $menuItemRowsResult[0]['inv_count'] > 0) {
					$reserve_status = 1;
				} elseif($menuItemRowsResult[0]['track_86_count'] == "86countInventory") {
					$reserve_status = 1;
				}
				if($reserve_status == 1) {
					$whereInventoryColumnArr = array('menuItemID' => $menuItemRowsResult[0]['id'], '_deleted' => 0);
					$menuInventoryItemsColNameArr = array("inventoryItemID", "unitID", "quantityUsed", "reservation", "menuItemID");
					$menuInventorytemRowsResult = CQ::getRowsWithRequiredColumns('menuitem_86', $menuInventoryItemsColNameArr, $whereInventoryColumnArr, '');
					if(!empty($menuInventorytemRowsResult)) {
						foreach($menuInventorytemRowsResult as $inv_val) {
							$whereInventoryColumnArr = array('menuItemID' => $menuItemRowsResult[0]['id'], '_deleted' => 0);
							$menuInventoryItemsColNameArr = array("inventoryItemID", "unitID", "quantityUsed", "reservation", "menuItemID");
							$setColumnArr= array("reservation" => ($inv_val['quantityUsed'] * $item['qty']) + $inv_val['reservation']);
							CQ::updateArray('menuitem_86', $setColumnArr, "where `menuItemID` = '" . $inv_val['menuItemID'] . "' and `inventoryItemID` = '" . $inv_val['inventoryItemID'] . "' ");
						}
					}
				}
			}
		}
		return $reserve_status;
	}

	private static function reserveForced86($item) {
		if(!empty($item['itemid'])) {
			$menuItemRowsResult = CQ::getRowsWhereColumnIn('forced_modifier_86', 'forced_modifierID', $item['item_id']);
			if(!empty($menuItemRowsResult)) {
				foreach($menuItemRowsResult as $val) {
					$setColumnArr= array("reservation" => ($val['quantityUsed'] * $item['qty']) + $val['reservation']);
					CQ::updateArray('forced_modifier_86', $setColumnArr, "where `forced_modifierID` = '" . $val['forced_modifierID'] . "' and `inventoryItemID` = '" . $val['inventoryItemID'] . "' ");
				}
			}
		}
	}

	private static function reserveOptional86($item){
		if(!empty($item['itemid'])) {
			$menuItemRowsResult = CQ::getRowsWhereColumnIn('modifier_86', 'modifierID', $item['item_id']);
			if(!empty($menuItemRowsResult)) {
				foreach($menuItemRowsResult as $val) {
					$setColumnArr= array("reservation" => ($val['quantityUsed'] * $item['qty']) + $val['reservation']);
					CQ::updateArray('forced_modifier_86', $setColumnArr, "where `modifierID` = '" . $val['modifierID'] . "' and `inventoryItemID` = '" . $val['inventoryItemID'] . "' ");
				}
			}
		}
	}

	protected function action_getOrderHistory($args) {
	    if(!isset($args['userDetails']['languageid']) || $args['userDetails']['languageid'] == '') {
	        $args['userDetails']['languageid'] = '1';
	    }
		$current_page=$args['pagination']['current_page'];
	  	$next_page=$args['pagination']['next_page'];
	  	$per_page=$args['pagination']['per_page'];
	  	$otherClause='';
	  	$paginationArr = $args['pagination'];
	  	$whereColumnArr=array("consumer_id"=> $args['userDetails']['user_id']);
		if (!isset($paginationArr['total_page'])) {
			$condition = '%approved":"1%';
			$sql="select count(*) as total from lavutogo.orders where consumer_id=".$args['userDetails']['user_id']." and transaction_information like '".$condition."'";
			$resultObj = DBConnection::adminQuery($sql);
			$result =  MA::resultToArray($resultObj);
			$totalRecord=$result[0]['total'];
			$totalPage= ceil($totalRecord/$per_page);
			$nextPage=$current_page+1;
			$pagination=array(
				"total_entries"=>$result[0]['total'],
				"total_pages"=>$totalPage,
				"current_page"=>$current_page,
				"next_page"=>$nextPage,
				"previous_page"=>$current_page - 1,
				"per_page"=>$per_page
			);
		}

		$current_page=$current_page;
		$per_page=$per_page;
		if($current_page==1) {
			$otherClause=array("limit"=>"0,$per_page");
		} else {
			$pages=$current_page*$per_page;
			$pages2=($pages-$per_page)+1;
			$otherClause=array("limit"=>"$pages2,$per_page");
		}
		if(!isset($args['userDetails']['user_id']) || empty($args['userDetails']['user_id'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['userDetails']['languageid'],"User Id is required"));
		}

		if(!isset($args['dataname']) || empty($args['dataname'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($args['userDetails']['languageid'],"Dataname is required"));
		}

		$orderdetails = OrdersQueries::userOrders($args['userDetails']['user_id'], $otherClause);
		if(!empty($orderdetails)) {
			$ordersData = array();
			foreach ($orderdetails as $valArr) {
                $orderData =json_decode($valArr['order_information'],true);
                $orderData['description'] = json_decode($valArr['description'],true);
				$orderData['dinetype'] ="Delivery";
				$orderData['created_at'] = $valArr['created_at'];
				$resorderinfo = self::formattedItemsData($orderData, $args['dataname'], $args['userDetails']['languageid']);
				$ordersData[] = $resorderinfo;
			}
			return array("pagination"=>$pagination,"orderHistory"=>$ordersData);
		} else {
		    return array("Status"=>"F", "msg"=>apiSpeak($args['userDetails']['languageid'],"User details not found"));
		}
	}

	protected function action_reOrder($args) {
	    if(!isset($args['languageid']) || $args['languageid'] == '') {
	        $args['languageid'] = '1';
	    }
		if (!isset($args['order_id']) || empty($args['order_id'])) {
		    return  array("Status"=>"F", "msg"=>apiSpeak($args['languageid'], "Order Id is required"));
		}
		if (!isset($args['user_id']) || empty($args['user_id'])) {
		    return  array("Status"=>"F", "msg"=>apiSpeak($args['languageid'], "User Id is required"));
		}
		if (!isset($args['dataname']) || empty($args['dataname'])) {
		    return  array("Status"=>"F", "msg"=>apiSpeak($args['languageid'], "Dataname is required"));
		}
		if (!isset($args['dinetype']) || empty($args['dinetype'])) {
		    return  array("Status"=>"F", "msg"=>apiSpeak($args['languageid'], "Order Type is required"));
		}
		$ordercolumnArr = array("order_id","consumer_id","amount","order_information", "created_at");
		$whereColumnArr = array('order_id'=>$args['order_id'],'consumer_id'=>$args['user_id']);
		$orderItemRowsResult = CQ::getRowsWithRequiredColumns('orders', $ordercolumnArr, $whereColumnArr, "", "", "lavutogo");
		if (count($orderItemRowsResult)<=0) {
		    return  array("Status"=>"F", "msg"=>apiSpeak($args['languageid'], "Invalid Order Id"));
		}

		$orderData = json_decode($orderItemRowsResult[0]['order_information'],true);
		$data = self::formattedReorderItemsData($orderData, $args['dataname'], $args['dinetype']);
		if ($data == 'fail') {
		    return  array("Status"=>"F", "msg"=>apiSpeak($args['languageid'], "One or more of the items on this order are no longer available"));
		} else {

			$ordersData ['order_id'] = $args['order_id'];
			$ordersData ['status'] = "S";
			$ordersData['order_contents_information'] = $data;
		}
		return $ordersData;
	}

	protected function formattedReorderItemsData($args, $dataname, $dinetype) {
		$error_msg = 'fail';
		$inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
		foreach ($args['order_contents_information'] as $itemdetail) {
			$item = array();
			$item['item_id'] = $itemdetail['item_id'];
			$whereColumnArr = array("id" => $itemdetail['item_id'], "_deleted" => 0, "ltg_display" => 1);
			$colNameArr = array("name", "price", "category_id", "nutrition", "description", "dining_option");
			$menuItem = CQ::getRowsWithRequiredColumns('menu_items', $colNameArr, $whereColumnArr, '', '', $dataname);
			$item['item_id'] = $itemdetail['item_id'];
			$item['quantity'] = $itemdetail['quantity'];
			$item['name'] = $menuItem[0]['name'];
			$item['price'] = $menuItem[0]['price'];
			$item['description'] = $menuItem[0]['description'];
			$item['forced'] = array();
			$item['optional'] = array();
			if (strpos($menuItem[0]['dining_option'], $dinetype) === false) {
				return  $error_msg;
			}
			if ($itemdetail['forced_modifier_list_id'] != '') {
				$fm_count = count(explode(",", $itemdetail['forced_modifier_list_id']));
				$reqWhere = "in:".str_replace(",", ":", $itemdetail['forced_modifier_list_id']);
				$colNameArr = array("id", "title", "cost", "list_id", "nutrition", "product_code");
				$whereColumnArr = array("id" => $reqWhere, "_deleted"=> "0");
				$fm_details = CQ::getRowsWithRequiredColumns('forced_modifiers', $colNameArr, $whereColumnArr, '', '', $dataname);
				$item['forced_count'] = $fm_count;
				if (count($fm_details) == $fm_count) {
					$item['forced']['option'] = $fm_details;
				} else {
					return  $error_msg;
				}
			}
			if ($itemdetail['modifier_list_id'] != '') {
				$opm_count = count(explode(",", $itemdetail['modifier_list_id']));
				$reqWhere = "in:".str_replace(",", ":", $itemdetail['modifier_list_id']);
				$colNameArr = array("id", "title", "cost", "category", "nutrition", "product_code");
				$whereColumnArr = array("id" => $reqWhere, "_deleted"=> "0");
				$opm_details = CQ::getRowsWithRequiredColumns('modifiers', $colNameArr, $whereColumnArr, '', '', $dataname);
				if (count($opm_details) == $opm_count) {
					$item['optional'] = $opm_details;
				} else {
					return  $error_msg;
				}
			}
			$data['menuId'] =  array(id =>$itemdetail['item_id']);
			$data['dataname'] =  $dataname;
			$data['itemQty'] =  $itemdetail['quantity'];
			$details = ItemsDataController::action_getItemDetail($data, $inventoryMigrationStatus);
			$item['item_details'] = $details['Items'][0];
			$itemsData[] = $item;
		}
		return $itemsData;
	}

	protected function formattedItemsData($args, $dataname, $languageid = 1) {
		$ordersData = array();
		$respnseitemdetails = array();
        $taxinform=array();
		foreach ($args['order_contents_information'] as $itemdetail) {
			$ordercolumnArr = array("title");
			$whereColumnArr = array("dataname"=>$args['ltg_name']);
			$orderItemRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $ordercolumnArr, $whereColumnArr, "", "");
			$itemargs = array("menuId" =>array("id" => $itemdetail['item_id']),"dataname" => $dataname,"mId"=>$itemdetail['modifier_list_id'],"fId"=>$itemdetail['forced_modifier_list_id'] ); //format to accept data
			$itempayload = json_encode(ItemsDataController::action_getItemDetailHistory($itemargs));
			$itempayloadData = json_decode($itempayload)->Items;
			$itempayloadData[0]->special = $itemdetail['special'];

			foreach($itempayloadData as $Item) {
				$Item->price = $itemdetail['price'];
				$Item->forcedModiferPrice = isset($itemdetail['forced_modifiers_price']) ? $itemdetail['forced_modifiers_price'] : '';

				if(isset($itemdetail['use_pizza_creator']) && $itemdetail['use_pizza_creator']){
					//	Set text format for Pizza modifiers details
					$pizzaFields = PizzaCreator::calculateOrderContentsFields(explode(",", $itemdetail['forced_modifier_list_id']), $itemdetail['mods_builder_content'], true);
					$Item->forcedModifers = $pizzaFields->pizzaOptions;
				}
				else
					$Item->forcedModifers = isset($itemdetail['options']) ? $itemdetail['options'] : '';

				$Item->optionalModiferPrice = isset($itemdetail['modify_price']) ? $itemdetail['modify_price'] : '';
				$Item->optionalModifers = isset($itemdetail['moptions']) ? $itemdetail['moptions'] : '';
			}

			$resorderinfo ['reorder'] = 'Available';
			foreach ($itempayloadData as $key1 => $singlr_item) {
				if ($singlr_item->availability == 'OutStock') {
					$resorderinfo ['reorder'] = "Not Available";
					$resorderinfo ['reorder_msg'] = apiSpeak($languageid, "One or more of the items on this order are no longer available");
					break;
				}
			}
			$respnseitemdetails[] = $itempayloadData[0];

			$taxreqarr[] = $itemdetail;

			$discountInfo=array();
			if(!empty($arg['description']['discount_info'])) {
			    $discountInfo=$arg['description']['discount_info'];
			}

			$resorderinfo = array(
				"order_id" => $args['order_id'],
				"cc" => "",
				"title"=>$orderItemRowsResult[0]['title'],
				"loc_id" => $args['loc_id'],
		        "discounttypes"=>$discountInfo,//LP-5034
				"order_date" => $args['created_at'],
				"order_type" => $args['dinetype'],
				"description"=>$args['description'],
				"ltg_name" => $dataname,
				"order_contents_information" => $respnseitemdetails,
			);
		}
		$resorderinfo['taxdetails'] = $args['taxdetails'];
		$resorderinfo['subtotal'] = $args['subtotal'];
		$resorderinfo['total'] = $args['total'];
		$resorderinfo['tax'] = $args['tax'];
		$resorderinfo['roundingAmount'] = isset($args['rounding_amount']) ? $args['rounding_amount'] : 0;

  		return $resorderinfo;
	}

	protected static function reqvar($str, $def=false) {
		if(isset($_POST[$str])) {
					return $_POST[$str];
		} else if(isset($_GET[$str])) {
			return $_GET[$str];
		} else if(isset($_REQUEST[$str])) {
			return $_REQUEST[$str];
		} else {
			return $def;
		}
	}

	protected function action_getItemsTaxDetails($args) {
	    if(!isset($args['orderItemsDetails']['languageid']) || $args['orderItemsDetails']['languageid'] == '') {
	        $languageid = '1';
	    }
	    else {
	        $languageid = $args['orderItemsDetails']['languageid'];
	    }
	    if(!isset($args['orderItemsDetails']) || empty($args['orderItemsDetails'])) {
	        return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Order Items Details are required"));
		}

		$orderLoc = $args['orderDetails']['loc_id'];
		$orderItemsDetails = $args['orderItemsDetails'];

		$today = date("Y-m-d H:i:s");
		$subtotal = 0.00;
		$gtotal = 0.00;
		$salestax = 0.00;
		$perItemtotal = 0.00;
		$common_tax_rate = 0.00;
		$itemdetails = array();

		foreach($orderItemsDetails as $orderItemsDetail) {
			if(!empty($orderItemsDetail['itemid'])) {
				$itemdetail = OrdersQueries::itemdetails($orderItemsDetail['itemid'], $orderItemsDetail['qty'], $orderItemsDetail['fmods_checked'], $orderItemsDetail['omods_checked']);
				$itemdetails[] = $itemdetail;
			}
		}

		$taxinform = self::displaytax($itemdetails);
		$get_totals = self::caltotal($taxinform);
		$resorderinfo = array(
			"subtotal" => self::displayFormatedCurrency($get_totals['subtotal']),
			"total" => self::displayFormatedCurrency($get_totals['grand_total']),
			"taxdetails" =>  $taxinform
		);

		return $resorderinfo;
	}

	protected function cal_price($number) {
		return (float)str_replace(",", ".", (string)$number);
	}

	protected static function getDOptions($doption) {
		$result = array();
		foreach ($doption as $option) {
			$result[] = $option->id;
			if(isset($option->doption)) {
				$result = array_merge($result,self::getDOptions($option->doption));
			}
		}
		return $result;
	}

	protected function action_myOrderitems($args) {
	    if(isset($args['orderDetails']['languageid']) && $args['orderDetails']['languageid'] != '') {
	        $languageid = $args['orderDetails']['languageid'];
	    }
	    else {
	        $languageid = 1;
	    }
		if(!isset($args['orderItemsDetails']) || empty($args['orderItemsDetails'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Order Items Details are required"));
		}

		if(!isset($args['dataname']) || empty($args['dataname'])) {
		    return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Dataname is required"));
		}

		$user_id = isset($args['orderDetails']['user_id']) ? $args['orderDetails']['user_id'] : 0;
		$delivery_fee = isset($args['orderDetails']['delivery_fee']) ? $args['orderDetails']['delivery_fee'] : 0;
		$order_type = isset($args['orderDetails']['order_type']) ? $args['order_type']['order_type'] : "";
		$orderLoc = $args['orderDetails']['loc_id'];
		$orderItemsDetails = $args['orderItemsDetails'];
		$dataname = $args['dataname'];
		DBConnection::startClientConnection($dataname);

		$order_id = $args['orderDetails']['order_id'];

		if(isset($args['orderDetails']['order_id']) && !empty($args['orderDetails']['order_id'])) {
			$order_id = $args['orderDetails']['order_id'];
			$order_arr = OrdersQueries::checkOrder($order_id);
			if(!isset($order_arr[0]['order_id']) && empty($order_arr[0]['order_id'])) {
			    return $return = array("Status"=>"F", "msg"=>apiSpeak($languageid, "Invalid Order Id"));
			} else {
				$order_id = $order_arr[0]['order_id'];
			}
		}

		$subtotal = 0.00;
		$gtotal = 0.00;
		$salestax = 0.00;
		$perItemtotal = 0.00;
		$dborderinfoJSON = "";
		$orderinfo =array();
		$common_tax_rate = 0.00;
		$itemdetails = array();
		$respnseitemdetails = array();
		$item_count = 0;
		$item_status = 1;
		$locationInfo = LocationDataController::action_getLocationInfo($orderLoc, $dataname);
		$inventoryMigrationStatus = CQ::getInventoryMigrationStatus();
		foreach($orderItemsDetails as $orderItemsDetail) {
			if($orderItemsDetail['itemid'] != '' && $orderItemsDetail['qty'] >= 1) {
				$itemsdetail = OrdersQueries::itemdetails($orderItemsDetail['itemid'], $orderItemsDetail['qty'], $orderItemsDetail['fmods_checked'], $orderItemsDetail['omods_checked'], $orderItemsDetail['special']);

				//-------------------------------------------------------------------------
				//	If menu item use Pizza Creator, 'Option' column has to be fullfilled with the details of the modifiers selected including the details of the selection.
				//	Also the price of the item needs to be calculated considering the Pizza Creator selection. ( Modifier Builder Content )
				//-------------------------------------------------------------------------
				$pizzaOptions=null;
				$itemsdetail['use_pizza_creator'] = $orderItemsDetail['use_pizza_creator'];
				if( $orderItemsDetail['use_pizza_creator'] && isset($orderItemsDetail['fmods_checked']) ) {
					$pizzaFields = PizzaCreator::calculateOrderContentsFields($orderItemsDetail['fmods_checked'], $orderItemsDetail['mods_builder_content']);
					$pizzaOptions = $pizzaFields->pizzaOptions;
					$itemsdetail['forced_modifiers_price'] = $pizzaFields->pizzaForcedModifiersPrice;
					$itemsdetail['mods_builder_content'] = $orderItemsDetail['mods_builder_content'];
				}
				//-------------------------------------------------------------------------

				$args1 = array(
					"menuId" => array("id" => $orderItemsDetail['itemid']),
					"dataname" =>$dataname,
					"itemQty" => $orderItemsDetail['qty']
				);
				$itempayload = JsonManager::encodeSpecialCharacters(ItemsDataController::action_getItemDetail($args1, $inventoryMigrationStatus));

				$itempayloadData = json_decode($itempayload)->Items;

				foreach($itempayloadData as $Item) {
					$Item->price = $itemsdetail['price'] + $itemsdetail['modify_price'] + $itemsdetail['forced_modifiers_price'];

					$itemsdetail['category_id'] = $Item->category_id;
					$itemsdetail['discount_type'] = $Item->no_discount;
					if(isset($orderItemsDetail['fmods_checked'])) {
						if(!empty($Item->forced)) {
							$foptions = array();
							foreach($Item->forced as $forcedItem) {
								foreach($forcedItem->option as $forcedItemOption) {
									$foptions[] = $forcedItemOption->id;
									if(isset($forcedItemOption->doption)) {
										$foptions = array_merge($foptions,self::getDOptions($forcedItemOption->doption));
									}
									if(in_array($forcedItemOption->id, $orderItemsDetail['fmods_checked'])) {
										$forcedItemOption->status = "defaultChecked";
									} else {
										$forcedItemOption->status = "";
									}
								}
							}

							foreach ($orderItemsDetail['fmods_checked'] as $foption_chk) {
								if(!in_array($foption_chk, $foptions)) {
									$item_status = 0;
								}
							}
						}
						$itemsdetail['options'] = self::getModifiersTitle($orderItemsDetail['fmods_checked'], self::$TYPE_FORCED_MODIFIERS);
					}

					if(isset($orderItemsDetail['omods_checked'])) {
						if(!empty($Item->optional)) {
							$ooptions = array();
							foreach($Item->optional as $optItem) {
								foreach($optItem->option as $optItemOption) {
									$ooptions[] = $optItemOption->id;
									if(in_array($optItemOption->id, $orderItemsDetail['omods_checked'])) {
										$optItemOption->status = "defaultChecked";
										$itemsdetail['moptions'] .= trim($optItemOption->title). ", ";
									} else {
										$optItemOption->status = "";

									}
								}
							}

							foreach ($orderItemsDetail['omods_checked'] as $ooption_chk) {
								if(!in_array($ooption_chk, $ooptions)) {
									$item_status = 0;
								}
							}

						} else if(!empty($orderItemsDetail['omods_checked'])) {
							$itemsdetail['moptions'] = self::getModifiersTitle($orderItemsDetail['omods_checked'], self::$TYPE_OPTIONAL_MODIFIERS);
						}
			         }
					$itemsdetail['moptions'] = trim($itemsdetail['moptions'], ", ");

					//If pizza options are defined, use it
					if(isset($pizzaOptions))
						$itemsdetail['options'] = $pizzaOptions;

					$itemdetails[] = $itemsdetail;
					$respnseitemdetails[] = $Item;
			 	}
			}
		}

		$dataarr = array();
		$tCal = 0;
		$sTotal = 0;
		$taxinform=array();
		$calculatedtaxamount = 0.00;
		$items_data = array();
		foreach($itemdetails as $itemdetail) {
			$items_data[] = self::buildItemString($itemdetail);
		}

		$taxinform = self::displaytax($itemdetails);
		$get_totals = self::caltotal($taxinform);
		$ltg_minimum_order_amount = 0;
		$roundingAdjustment = CQ::roundingAdjustment(self::saveFormatedCurrency($get_totals['grand_total']), $dataname, $orderLoc);
		$get_totals['grand_total'] = $roundingAdjustment['amount'];
		$ramount = $roundingAdjustment['roundingAmount'];
		$rowsResult = CQ::getRowsWithRequiredColumns('config', array("value"), array("setting" => "ltg_minimum_order_price"));

		if(!empty($rowsResult)) {
			$ltg_minimum_order_amount = $rowsResult[0]['value'];
		}
		$loc_id	= 1;
		$datetime = substr(LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]), 0, -1);

		$now_ts = strtotime($datetime);
		$uniq = rand(1000,9999);
		$iodate = date("YmdHis", $now_ts);
		$device_id = $_SERVER['REMOTE_ADDR'];
		$nowtime = $datetime;
		$device_time = $nowtime;
		$o_vars = array(
			'subtotal'					=> self::saveFormatedCurrency($get_totals['subtotal']),
			'tax'						=> self::saveFormatedCurrency($get_totals['calculatedtaxamount']),
			'total'						=> self::saveFormatedCurrency($get_totals['grand_total']),
			'rounding_amount'			=> $ramount,
			'server' 					=> self::$lavuToGoName,
			'tablename'					=> self::$lavuToGoNameUnpaid,
			'send_status' 				=> 1,
			'guests' 					=> 1,
			'server_id' 				=> 0,
			'no_of_checks' 				=> 1,
			'multiple_tax_rates' 		=> "",
			'tab'						=> 0,
			'deposit_status'			=> 0,
			'register'					=> self::$lavuToGoName,
			'register_name' 			=> self::$lavuToGoName,
			'tax_exempt'				=> 0,
			'exemption_id' 				=> "",
			'exemption_name'			=> "",
			'alt_tablename' 			=> "",
			'past_names'				=> "",
			'togo_status'				=> 1,
			'togo_name'					=> "",
			'togo_phone'				=> "",
			'email'						=> "",
			'togo_time'					=> "",
			'last_mod_ts'				=> $now_ts,
			'pushed_ts'					=> $now_ts,
			'last_mod_device'			=> $device_id,
			'last_mod_register_name'	=> self::$lavuToGoName,
			'active_device'				=> (substr($device_id, 0, 4)=="216.")?"":$device_id,
			'original_id'				=> self::reqvar("original_id", "")
		);
		$o_vars["closed"] = "0000-00-00 00:00:00";
		$o_vars["void"] = 3;

		if ($o_vars['togo_status'] == 1) {
			$o_vars['active_device'] = "";
		}

		$action = "";
		if(empty($order_id)) {
			$order_id = OrdersQueries::generateOrderId();

			$ioid_details =  self::action_generateIoid(array(
				"dataname" => $dataname,
				"order_id" => $order_id,
				"dateStr" => $iodate
			));
			$o_vars['ioid']	= $ioid_details['ioid'];
			$orderinfo = array(
				"order_id" =>  $order_id,
				"cc" => "",
				"loc_id" => $orderLoc,
				"subtotal" => self::displayFormatedCurrency($get_totals['subtotal']),
				"total" => self::displayFormatedCurrency($get_totals['grand_total']),
				"taxdetails" => $taxinform,
				"tax" =>  self::displayFormatedCurrency($get_totals['calculatedtaxamount']) ,
				"today_date" => $datetime,
				"togo_date" => '',
				"togo_time" => '',
				"ltg_name" => $dataname,
				"customer_information" => '',
				"order_contents_information" => $itemdetails,
			);
			$o_vars['discount']	= "0";
			$resorderinfo = array(
				"order_id" =>  $order_id,
				"cc" => "",
				"loc_id" => $orderLoc,
				"subtotal" => self::displayFormatedCurrency($get_totals['subtotal']),
				"total" => self::displayFormatedCurrency($get_totals['grand_total']),
				"rounding_amount" => $ramount,
				"taxdetails" => $taxinform,
				"tax" =>  self::displayFormatedCurrency($get_totals['calculatedtaxamount']) ,
				"today_date" => $datetime,
				"togo_date" => '',
				"togo_time" => '',
				"ltg_name" => $dataname,
				"customer_information" => '',
				"order_contents_information" => $respnseitemdetails,
			);
			$dborderinfoJSON = JsonManager::encodeSpecialCharacters($orderinfo);
			$insertltg = array(
				"order_id" => $order_id,
				"created_at" => $datetime,
				"consumer_id" => $user_id,
				"merchant_id" => 1,
				"amount" => $o_vars['total'],
				"description" => "Null",
				"transaction_information" => $orderinfo['cc'],
				"order_information" => $dborderinfoJSON,
				"debug_information" => "Null",
			);
			$columnNames = array_keys($insertltg);
			$rowValue = array_values($insertltg);
			$result = CQ::updateArray('lavutogo.orders', $insertltg, " where `order_id` = '" . $order_id . "'",false);

			$locationDataname = array("dataname" => $resorderinfo['ltg_name']);
           	$o_vars['location']     = self::generateLocation($locationDataname);
			$o_vars['location_id'] = $orderLoc;
			$o_vars['order_status']		= "open";
			$o_vars['opening_device']	= $device_id;
			$o_vars['gratuity']			= "0";
			$o_vars['order_id']			= $order_id;
			$o_vars['opened']           = $args['orderDetails']['open_date'];

			// inserting order in order table
			 $result = CQ::insertArray('orders', array_keys($o_vars), array(array_values($o_vars)));
			$action = "Order Opened";
		} else {
			$orderinfo = array(
				"order_id" => $order_id,
				"cc" => "",
				"loc_id" => $orderLoc,
				"subtotal" => self::displayFormatedCurrency($get_totals['subtotal']),
				"total" => self::displayFormatedCurrency($get_totals['grand_total']),
				"taxdetails" => $taxinform,
				"tax" =>  self::displayFormatedCurrency($get_totals['calculatedtaxamount']) ,
				"today_date" => $datetime,
				"togo_date" => '',
				"togo_time" => '',
				"ltg_name" => $dataname,
				"customer_information" => '',
				"order_contents_information" => $itemdetails,
			);

			$dborderinfoJSON = JsonManager::encodeSpecialCharacters($orderinfo);

			$resorderinfo = array(
				"order_id" => $order_id,
				"cc" => "",
				"loc_id" => $orderLoc,
				"subtotal" => self::displayFormatedCurrency($get_totals['subtotal']),
				"total" => self::displayFormatedCurrency($get_totals['grand_total']),
				"rounding_amount" => $ramount,
				"taxdetails" => $taxinform,
				"tax" =>  self::displayFormatedCurrency($get_totals['calculatedtaxamount']) ,
				"today_date" => $datetime,
				"togo_date" => '',
				"togo_time" => '',
				"ltg_name" => $dataname,
				"customer_information" => '',
				"order_contents_information" => $respnseitemdetails,
			);

			$resorderinfo[] = array(
				"order_id" =>  $order_id
			);

			$updateltg = array(
				"amount" => $o_vars['total'],
				"description" => "Null",
				"transaction_information" => $orderinfo['cc'],
				"order_information" => $dborderinfoJSON,
				"debug_information" => "Null",
			);

			$result = CQ::updateArray('lavutogo.orders', $updateltg, "where `order_id` = '" . $args ['orderDetails'] ['order_id'] . "'",false);

			// order table update
			$result = CQ::updateArray('orders', $o_vars, "where `order_id` = '" . $args ['orderDetails'] ['order_id'] . "'" );
			$action = "Order Saved";
		}

		$final_arr[] = $resorderinfo;
		$l_vars = array(
			'action'		=> $action,
			'details'		=> "Total: ".$o_vars['total'],
			'loc_id'		=> $orderLoc,
			'order_id'		=> $order_id,
			'time'			=> $device_time,
			'server_time'	=> date("Y-m-d H:i:s"),
			'user'			=> self::$lavuToGoName,
			'user_id'		=> 0,
			'device_udid'	=> $device_id
		);

		// inserting order action in action_log table
		CQ::insertArray('action_log', array_keys($l_vars), array(array_values($l_vars)));

		if(!empty($items_data)) {
			// Deleting existing order items
			DBConnection::clientQuery("DELETE FROM `[1]` WHERE `order_id`='[2]'", "order_contents", $order_id);
			// Deleting existing order item modifiers
			DBConnection::clientQuery("DELETE FROM `[1]` WHERE `order_id`='[2]'", "modifiers_used", $order_id);
			//To check "Automatically combine equivalent items on ticket" setting is enabled/disabled.
			if ($locationInfo['group_equivalent_items']) {
				$items_data = self::groupEquivalentOrderItems($items_data);
			}
			$i = 0;
			foreach($items_data as $item_data) {
				$item_data['ioid'] = $ioid_details['ioid'];
				$item_data['order_id'] = $order_id;
				$item_data['loc_id'] = 1;
				$item_data['auto_saved'] = 0;
				$item_data['tax_freeze'] = '';
				$item_data['super_group_id'] = '';
				$item_data['last_mod_ts'] = $now_ts;
				$item_data['last_change_ts'] = $now_ts;
				$item_data['split_details'] = '';
				CQ::insertArray('order_contents', array_keys($item_data), array(array_values($item_data))); // inserting order items

				// saving modifiers data start
				$mod_details = $itemdetails[$i];
				$mod_details['order_id'] = $order_id;
				$mod_details['ioid'] = $ioid;
				$mod_details['icid'] = $item_data['icid'];

				self::action_saveModifiersUsed($mod_details);
				//saving modifiers data end
				$i++;
			}
		}

		$d_vars = array();
		$d_vars['subtotal'] = self::displayFormatedCurrency($get_totals['subtotal']);
		$d_vars['tax'] = self::displayFormatedCurrency($get_totals['calculatedtaxamount']);
		$d_vars['discount'] = 0;
		$d_vars['gratuity'] = 0;
		$before_gratuity = ($d_vars['subtotal'] + $d_vars['tax'] - $d_vars['discount']);
		if ($before_gratuity == 0) {
			$d_vars['gratuity'] = "0";
			$d_vars['check_total'] = "0";
		} else {
			$d_vars['check_total'] = ($before_gratuity + $d_vars['gratuity']);
		}

		$result = CQ::getRowsWithRequiredColumns ("split_check_details", array("id"), array("order_id"=>$order_id, "check"=> 1));

		if(!empty($result)) {
			CQ::updateArray ( 'split_check_details', $d_vars, "where `order_id` = '" . $order_id . "'" ); // updating split_check_details
		} else {
			$d_vars['loc_id'] = $orderLoc;
			$d_vars['order_id'] = $order_id;
			$d_vars['ioid'] = $ioid;
			$d_vars['check'] = 1;

			CQ::insertArray('split_check_details', array_keys($d_vars), array(array_values($d_vars))); // inserting split_check_details
		}

		return $resorderinfo;
	}

	public static function orderUpdate($orderItemsDetails, $order_id, $dataname, $orderLoc = 1){
		$subtotal = 0.00;
		$gtotal = 0.00;
		$salestax = 0.00;
		$perItemtotal = 0.00;
		$dborderinfoJSON = "";
		$orderinfo =array();
		$common_tax_rate = 0.00;
		$itemdetails = array();
		$respnseitemdetails = array();
		$item_count = 0;
		$item_status = 1;
		$locationInfo = LocationDataController::action_getLocationInfo($orderLoc, $dataname);
		foreach($orderItemsDetails as $orderItemsDetail) {
			if($orderItemsDetail['itemid'] != '' && $orderItemsDetail['qty'] >= 1) {
				$itemdetail = OrdersQueries::itemdetails($orderItemsDetail['itemid'], $orderItemsDetail['qty'],	$orderItemsDetail['fmods_checked'], $orderItemsDetail['omods_checked'], $orderItemsDetail['special']);

				//-------------------------------------------------------------------------
				//	If menu item use Pizza Creator, 'Option' column has to be fullfilled with the details of the modifiers selected including the details of the selection.
				//	Also the price of the item needs to be calculated considering the Pizza Creator selection. ( Modifier Builder Content )
				//-------------------------------------------------------------------------
				$pizzaOptions=null;
				$itemdetail['use_pizza_creator'] = $orderItemsDetail['use_pizza_creator'];
				if( $orderItemsDetail['use_pizza_creator'] && isset($orderItemsDetail['fmods_checked']) ) {
					$pizzaFields = PizzaCreator::calculateOrderContentsFields($orderItemsDetail['fmods_checked'], $orderItemsDetail['mods_builder_content']);
					$pizzaOptions = $pizzaFields->pizzaOptions;
					$itemdetail['forced_modifiers_price'] = $pizzaFields->pizzaForcedModifiersPrice;
					$itemdetail['mods_builder_content'] = $orderItemsDetail['mods_builder_content'];
				}
				//-------------------------------------------------------------------------

				$args = array("menuId" =>array(
					"id" => $orderItemsDetail['itemid']),
					"dataname" =>$dataname,
					"itemQty" => $orderItemsDetail['qty']
				);
				$itempayload = JsonManager::encodeSpecialCharacters(ItemsDataController::action_getItemDetail($args));

				$itempayloadData = json_decode($itempayload)->Items;

				foreach($itempayloadData as $Item) {
					if($Item->availability == ItemsDataController::$outStock) {
						$item_status = 0;
						continue;
					}

					$Item->price = self::saveFormatedCurrency($itemdetail['price'] + $itemdetail['modify_price'] + $itemdetail['forced_modifiers_price']);

					$itemdetail['category_id'] = $Item->category_id;
					$itemdetail['discount_type'] = $Item->no_discount;
					if(isset($orderItemsDetail['fmods_checked'])) {
						if(!empty($Item->forced)) {
							$foptions = array();
							foreach($Item->forced as $forcedItem) {
								foreach($forcedItem->option as $forcedItemOption) {
									$foptions[] = $forcedItemOption->id;
									if(isset($forcedItemOption->doption)) {
										$foptions = array_merge($foptions,self::getDOptions($forcedItemOption->doption));
									}
									if(in_array($forcedItemOption->id, $orderItemsDetail['fmods_checked'])) {
										$forcedItemOption->status = "defaultChecked";
									} else {
										$forcedItemOption->status = "";
									}
								}
							}
							foreach ($orderItemsDetail['fmods_checked'] as $foption_chk) {
								if(!in_array($foption_chk, $foptions)) {
									$item_status = 0;
								}
							}
						}
						$itemdetail['options'] = self::getModifiersTitle($orderItemsDetail['fmods_checked'], self::$TYPE_FORCED_MODIFIERS);
					}

					if(isset($orderItemsDetail['omods_checked'])) {
						if(!empty($Item->optional)){
							$ooptions = array();
							foreach($Item->optional as $optItem)
							{
								foreach($optItem->option as $optItemOption)
								{
									$ooptions[] = $optItemOption->id;
									if(in_array($optItemOption->id, $orderItemsDetail['omods_checked']))
									{
										$optItemOption->status = "defaultChecked";
										$itemdetail['moptions'] .= trim($optItemOption->title). ", ";
									}
									else
									{
										$optItemOption->status = "";

									}
								}

							}

							foreach ($orderItemsDetail['omods_checked'] as $ooption_chk) {
								if(!in_array($ooption_chk, $ooptions))
								{
									$item_status = 0;
								}
							}
						} else if(!empty($orderItemsDetail['omods_checked'])) {
							$itemdetail['moptions'] = self::getModifiersTitle($orderItemsDetail['omods_checked'], self::$TYPE_OPTIONAL_MODIFIERS);
						}
					}
					$itemdetail['moptions'] = trim($itemdetail['moptions'], ", ");

					//If pizza options are defined, use it
					if(isset($pizzaOptions))
						$itemdetail['options'] = $pizzaOptions;

					$itemdetails[] = $itemdetail;
					$respnseitemdetails[] = $Item;
			}
		}
	}
	foreach($itemdetails as $itemdetail) {
		$items_data[] = self::buildItemString($itemdetail);
	}
	$taxinform = self::displaytax($itemdetails);

	$get_totals = self::caltotal($taxinform);
	$roundingAdjustment = CQ::roundingAdjustment(self::saveFormatedCurrency($get_totals['grand_total']), $dataname, $orderLoc);
	$get_totals['grand_total'] = $roundingAdjustment['amount'];
	$ramount = $roundingAdjustment['roundingAmount'];
	$displayTax = array();
	foreach($taxinform as $taxdeta) {
    	$displayTax[]= $taxdeta;
	}
	$loc_id	= 1;
	$datetime = substr(LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]),0,-1);

	$now_ts = strtotime($datetime);
	$uniq = rand(1000,9999);
	$iodate = date("YmdHis", $now_ts);
	$device_id = $_SERVER['REMOTE_ADDR'];
	$nowtime = $datetime;
	$device_time = $nowtime;
	$o_vars = array(
		'subtotal'					=> self::saveFormatedCurrency($get_totals['subtotal']),
		'tax'						=> self::saveFormatedCurrency($get_totals['calculatedtaxamount']),
		'total'						=> self::saveFormatedCurrency($get_totals['grand_total']),
		'rounding_amount'			=> $ramount,
		'server'					=> self::$lavuToGoName,
		'tablename'					=> self::$lavuToGoNameUnpaid,
		'send_status'				=> 1, // Order Auto-Send
		'auto_send_status'			=> 1, // Order Auto-Send
		'guests'					    => 1,
		'server_id'					=> 0,
		'no_of_checks'				=> 1,
		'multiple_tax_rates'		    => "",
		'tab'						=> 0,
		'deposit_status'			    => 0,
		'register'					=> self::$lavuToGoName,
		'register_name'				=> self::$lavuToGoName,
		'tax_exempt'				    => 0,
		'exemption_id'				=> "",
		'exemption_name'			    => "",
		'alt_tablename'				=> "",
		'past_names'				    => "",
		'togo_status'				=> 1,
		'togo_name'					=> "",
		'togo_phone'				    => "",
		'email'						=> "",
		'togo_time'					=> "",
		'last_mod_ts'				=> $now_ts,
		'pushed_ts'					=> $now_ts,
		'last_mod_device'			=> $device_id,
		'last_mod_register_name' 	=> self::$lavuToGoName,
		'active_device'				=> (substr($device_id, 0, 4)=="216.")?"":$device_id,
		'original_id'				=> self::reqvar("original_id", "")
	);

	//$o_vars["opened"] = $nowtime;
	$o_vars["closed"] = "0000-00-00 00:00:00";
	$o_vars["void"] = 3;

	if ($o_vars['togo_status'] == 1)
	{
		$o_vars['active_device'] = "";
	}

	$action = "";


	$orderinfo = array(
		"order_id" => $order_id,
		"cc" => "",
		"loc_id" => $orderLoc,
		"subtotal" => self::displayFormatedCurrency($get_totals['subtotal']),
		"total" => self::displayFormatedCurrency($get_totals['grand_total']),
		"rounding_amount" => $ramount,
		"taxdetails" => $taxinform,
		"tax" =>  self::displayFormatedCurrency($get_totals['calculatedtaxamount']) ,
		"today_date" => $datetime,
		"togo_date" => '',
		"togo_time" => '',
		"ltg_name" => $dataname,
		"customer_information" => '',
		"order_contents_information" => $itemdetails,
	);

	$dborderinfoJSON = JsonManager::encodeSpecialCharacters($orderinfo);

	// order_api table update
	$updateltg = array(
		"amount" => $o_vars['total'],
		"description" => '',
		"transaction_information" => $orderinfo['cc'],
		"order_information" => $dborderinfoJSON,
		"debug_information" => '',
	);
	CQ::updateArray('lavutogo.orders', $updateltg, "where `order_id` = '" . $order_id . "'",false);

	// order table update
	$result = CQ::updateArray ( 'orders', $o_vars, "where `order_id` = '" . $order_id . "'" );

	$action = "Order Updated";


	$l_vars = array(
		'action'		=> $action,
		'details'		=> "Total: ".$o_vars['total'],
		'loc_id'		=> $orderLoc,
		'order_id'		=> $order_id,
		'time'			=> $device_time,
		'server_time'	=> date("Y-m-d H:i:s"),
		'user'			=> self::$lavuToGoName,
		'user_id'		=> 0,
		'device_udid'	=> $device_id
	);

	// inserting order action in action_log table
	CQ::insertArray('action_log', array_keys($l_vars), array(array_values($l_vars)));

	if(!empty($items_data)) {
		// Deleting existing order items
		DBConnection::clientQuery("DELETE FROM `[1]` WHERE `order_id`='[2]'", "order_contents", $order_id);
		// Deleting existing order item modifiers
		DBConnection::clientQuery("DELETE FROM `[1]` WHERE `order_id`='[2]'", "modifiers_used", $order_id);
		//To check "Automatically combine equivalent items on ticket" setting is enabled/disabled.
		if ($locationInfo['group_equivalent_items']) {
			$items_data = self::groupEquivalentOrderItems($items_data);
		}
		$i = 0;
		$apply_taxrate = '';

		foreach($items_data as $item_data) {
			$whereColumnArr=array("order_id"=>$order_id);
			$columnNameArr=array("ioid");
			$res = CQ::getRowsWithRequiredColumns('orders',$columnNameArr,$whereColumnArr);
			$item_data['ioid'] = $res[0]['ioid'];
			$item_data['order_id'] = $order_id;
			$item_data['loc_id'] = 1;
			$item_data['auto_saved'] = 0;
			$item_data['tax_freeze'] = '';
			$item_data['super_group_id'] = '';
			$item_data['last_mod_ts'] = $now_ts;
			$item_data['last_change_ts'] = $now_ts;
			$item_data['split_details'] = '';
			CQ::insertArray('order_contents', array_keys($item_data), array(array_values($item_data))); // inserting order items
			$mod_details = $itemdetails[$i];
			$mod_details['order_id'] = $order_id;
			$mod_details['ioid'] = $ioid;
			$mod_details['icid'] = $item_data['icid'];

			self::action_saveModifiersUsed($mod_details);
			$i++;
		}
        CQ::updateArray ( 'orders', array("taxrate" => $apply_taxrate), "where `order_id` = '" . $order_id . "'" );
	}

	return array("status" => 1, "result" => "updated");

	}

	public static function action_saveModifiersUsed($order_details) {
		// Optional Modifiers Used data insertion
		if($order_details['modifier_list_id'] != "") {
			$omod_ids = explode(",", $order_details['modifier_list_id']);
			if(!empty($omod_ids)) {
				foreach($omod_ids as $omod_id) {
					$m_vars = array();
					$m_vars['loc_id'] = 1;
					$m_vars['order_id'] = $order_details['order_id'];
					$m_vars['mod_id'] = $omod_id;
					$m_vars['type'] = "optional";
					$m_vars['row'] = 0;
					$m_vars['qty'] = $order_details['quantity'];
					$m_vars['icid'] = $order_details['icid'];
					$omod_data = CQ::getRowsWithRequiredColumns('modifiers', array('title', 'cost', 'category'), array('id' => $omod_id));
					$m_vars['cost'] = ($omod_data[0]['cost'] * $order_details['quantity']);
					$m_vars['unit_cost'] = $omod_data[0]['cost'];
					$m_vars['ioid'] = $order_details['ioid'];
					$m_vars['title'] = $omod_data[0]['title'];
					$m_vars['list_id'] = $omod_data[0]['category'];
					CQ::insertArray('modifiers_used', array_keys($m_vars), array(array_values($m_vars)));
				}
			}
		}

		// Forced Modifiers Used data insertion
		if($order_details['forced_modifier_list_id'] != "") {
			$fmod_ids = explode(",", $order_details['forced_modifier_list_id']);
			if(!empty($fmod_ids)) {
				foreach ($fmod_ids as $fmod_id) {
					$m_vars = array();
					$m_vars['loc_id'] = 1;
					$m_vars['order_id'] = $order_details['order_id'];
					$m_vars['mod_id'] = $fmod_id;
					$m_vars['type'] = "forced";
					$m_vars['row'] = 0;
					$m_vars['qty'] = $order_details['quantity'];
					$m_vars['icid'] = $order_details['icid'];
					$fmod_data = CQ::getRowsWithRequiredColumns('forced_modifiers', array('title', 'cost', 'list_id'), array('id' => $fmod_id));
					$modCost=$fmod_data[0]['cost'];

					//	If exist data for modifiers created with a Builder(Pizza Creator), is necesary to use Builder cost.
					if(isset($order_details['mods_builder_content']) ){
						$mods_builder_content = $order_details['mods_builder_content'];
						//	If modifier has Builder content, cost will be calculated by the Modifier Builder.
						if(isset($mods_builder_content[(string)$fmod_id])){
							$modifierBuilderInfo = PizzaMod::getModifierBuilderInfo($mods_builder_content[(string)$fmod_id]);
							$m_vars['custom_mod_info'] = $modifierBuilderInfo->modifierInfo;
							$modCost = $modifierBuilderInfo->modifierCost;
						}
					}

					$m_vars['cost'] = ($modCost * $order_details['quantity']);
					$m_vars['unit_cost'] = $modCost;
					$m_vars['ioid'] = $order_details['ioid'];
					$m_vars['title'] = $fmod_data[0]['title'];
					$m_vars['list_id'] = $fmod_data[0]['list_id'];
					CQ::insertArray('modifiers_used', array_keys($m_vars), array(array_values($m_vars)));
				}
			}
		}
	}

	public static function gettaxProfilename($id) {
		$whereColumnArr= array("id" => $id, "_deleted" => 0);
		$ColNameArr = array("title");
		$rowsResult = CQ::getRowsWithRequiredColumns('tax_rates', $ColNameArr, $whereColumnArr, '');
		return $rowsResult[0]['title'];
	}

	private static function buildItemString($itemdetail) {
		$vars = array();
		$perItemtotal = self::saveFormatedCurrency($itemdetail['price'] + $itemdetail['forced_modifiers_price'] + $itemdetail['modify_price']);

		$taxinfo = self::displaytax($itemdetail);
		$taxrate_arr = explode(";;",$itemdetail['apply_taxrate']);
		$t=1;
		$taxType = $itemdetail['tax_inclusion'];
		foreach($taxrate_arr as $apply_tax) {
			$profile_tax = 0;
			//15::GST State::0.10::0::::::::18;;15::GST Central::0.20::0::::::::19
			//3::tax 1::.2::0::0::0::3;;3::tax 2::.4::0::0::0::4
			$taxDetails = explode("::",$apply_tax);
			$taxRate = $taxDetails[2];
			$subtotal += self::saveFormatedCurrency($perItemtotal);
			$taxRateSum += $taxRate;
			$taxSum = self::saveFormatedCurrency($taxRateSum);
			$taxrate_column = 'tax_rate'.$t;
			$tax_column = 'tax'.$t;
			$profile_tax = self::gettaxProfilename($taxDetails[6]);
			$vars['tax_name'.$t] = $profile_tax;
			$vars[$taxrate_column] = $taxRate;

			if($taxType == 1) {
				$cal_tax = ($perItemtotal*100)/(100+($taxRate*100));
				$cal_tax = $perItemtotal - $cal_tax;
				$vars[$tax_column] = $cal_tax;
				$total_cal_tax += $cal_tax;
				$vars['tax_amount'] = '0.00';
				$vars['itax'] = $total_cal_tax;
				$vars['itax_rate'] = $taxRateSum;
				$subtotal_wo_tax = $perItemtotal - $total_cal_tax;
			} else {
				$cal_tax = ($perItemtotal*($taxRate*100))/100;
				$vars[$tax_column] = $cal_tax;
				$total_cal_tax += $cal_tax;
				$vars['tax_amount'] = $total_cal_tax;
				$subtotal_wo_tax = $perItemtotal;
			}

			$t++;
		}
		$vars['tax_subtotal1'] = $subtotal_wo_tax;
		$vars['tax_subtotal2'] = $subtotal_wo_tax;
		$total_with_tax = $subtotal_wo_tax + $total_cal_tax;
		$vars['total_with_tax'] = self::saveFormatedCurrency($total_with_tax);
		$vars['item'] = $itemdetail['item'];
		$vars['price'] = self::saveFormatedCurrency($itemdetail['price']);
		$vars['quantity'] = $itemdetail['quantity'];
		$vars['options'] = $itemdetail['options'];
		$vars['special'] = isset($itemdetail['moptions']) ? trim($itemdetail['moptions'], ", ") : "";
		$vars['print'] = $itemdetail['print'];
		$vars['print_order'] = $itemdetail['print_order'];
		$vars['modify_price'] = self::saveFormatedCurrency($itemdetail['modify_price']);
		$vars['check'] = 1;
		$vars['seat'] = 1;
		$vars['item_id'] = $itemdetail['item_id'];
		$vars['printer'] = $itemdetail['printer'];
		$vars['apply_taxrate'] = $itemdetail['apply_taxrate'];
		$vars['custom_taxrate'] = $itemdetail['custom_taxrate'];
		$vars['modifier_list_id'] = $itemdetail['modifier_list_id'] == ""?0:$itemdetail['modifier_list_id'];
		$vars['forced_modifier_group_id'] = $itemdetail['forced_modifier_group_id'];
		$vars['forced_modifiers_price'] = self::saveFormatedCurrency($itemdetail['forced_modifiers_price']);
		$vars['course'] = 1;
		$vars['open_item'] = 0;
		$vars['subtotal'] = self::saveFormatedCurrency($itemdetail['price'] * $itemdetail['quantity']);
		$vars['allow_deposit'] = 0;
		$vars['deposit_info'] = '';
		$vars['subtotal_with_mods'] = self::saveFormatedCurrency($perItemtotal * $itemdetail['quantity']);
		$vars['void'] = 0;
		$vars['device_time'] = date("Y-m-d H:i:s");
		$vars['notes'] = isset($itemdetail['special'])?$itemdetail['special']:"";
		$vars['hidden_data1'] = '';
		$vars['hidden_data2'] = '';
		$vars['hidden_data3'] = '';
		$vars['hidden_data4'] = '';
		$vars['tax_inclusion'] = $taxType;
		$vars['sent'] = 0;
		$vars['tax_exempt'] = 0;
		$vars['exemption_id'] = 0;
		$vars['exemption_name'] = '';
		$vars['checked_out'] = 0;
		$vars['hidden_data5'] = '';
		$vars['price_override'] = 0;
		$vars['original_price'] = '';
		$vars['override_id'] = 0;
		$vars['idiscount_id'] = '';
		$vars['idiscount_sh'] = '';
		$vars['idiscount_value'] = '';
		$vars['idiscount_type'] = '';
		$vars['idiscount_amount'] = '';
		$vars['hidden_data6'] = '';
		$vars['idiscount_info'] = '';
		$vars['category_id'] = $itemdetail['category_id'];
		$vars['product_code'] = self::getMenuFieldDetailsById('product_code',$itemdetail['item_id']);
		$vars['tax_code'] = self::getMenuFieldDetailsById('tax_code',$itemdetail['item_id']);
		//$vars['ioid'] = '';

		$randomString = str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);
		$str = "10-".$randomString."-".date("YmdHis");

		$vars['icid'] = $str;
		$vars['server_time'] = date("Y-m-d H:i:s");

		$vars['discount_amount'] = '';
		$vars['discount_value'] = '';
		$vars['discount_type'] = $itemdetail['discount_type'] == '1' ? 'no' : $itemdetail['discount_type'];
		$vars['after_discount'] = '';
		$vars['discount_id'] = '';
		$vars['split_factor'] = 1;
		$vars['after_gratuity'] = '';
		return $vars;
	}

	private function vars_to_str($vars) {
		$str = "";
		foreach ($vars as $key => $val) {
			if ($str != "") {
				$str .= "&";
			}
			$str .= urlencode($key)."=".urlencode($val);
		}

		return $str;
	}

	protected function action_deleteOrder($args) {
		$orderID = $args['order_id'];
		$updateArray = array();
		$updateArray['_deleted'] =1;
		$result = CQ::updateArray('orders', $updateArray, "where `order_id`='" . $orderID . "'");
		$rowsUpdated = DBConnection::clientAffectedRows();
		return array("RecordUpdated" => $orderID);
	}

	protected function action_generateIoid($args) {
		$orderId = $args['order_id'];
		$data_name = $args['data_name'];
		$whereColumnArr = array("dataname" => $data_name, "disabled" => 0);
		$restaurantColumnArr = array("restaurantid", "locationid");
		$restaurantLocationRowResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantColumnArr, $whereColumnArr, '', '');
		$orderId = explode('-', $orderId);
		$randomString = str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);
		$dateStr = $args['dateStr'];
		$ioIdStr = array(
			"ioid"=>$restaurantLocationRowResult[0]['restaurantid']."".$restaurantLocationRowResult[0]['locationid']."".$orderId[1]."-".$randomString."-".$dateStr,
			"kds_printer_id"=>1
		);
		return $ioIdStr;

	}

    protected function generateLocation($args) {
        $dataname = $args['dataname'];
        $whereColumnArr = array("dataname" => $dataname, "disabled" => 0);
        $restaurantColumnArr = array("restaurantid", "locationid");
        $restaurantLocationRowResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantColumnArr, $whereColumnArr, '', '');
        $locationStr = '{'.$restaurantLocationRowResult[0]['restaurantid'].":".$dataname.":".$restaurantLocationRowResult[0]['locationid'].'}';
        return $locationStr;
    }

	public static function truncate_number_old( $number, $disable_decimal) {
		if (is_int($number) && ($disable_decimal == "" || $disable_decimal  == 0)) {
		  return sprintf('%0.2f', $number);
		} else {
          return $number;
        }
		if(!is_int($number) && !is_string($number) && ($disable_decimal == "" || $disable_decimal == 0)) {
			return round($number, 2);
		}
	}

	public static function truncate_number( $number, $disable_decimal) {
	    $number = trim($number);
	    $disable_decimal = trim($disable_decimal);
	    if (is_int($number) && ($disable_decimal == "" || $disable_decimal  == 0)) {
	    	return sprintf('%0.2f', $number);
	    } elseif(!is_int($number) && !is_string($number) && ($disable_decimal == "" || $disable_decimal == 0)) {
	    	return round($number, 2);
	    } elseif($disable_decimal != "" || $disable_decimal > 0) {
	      	return sprintf('%0.2f', $number);
	    } else {
	        return $number;
	    }
	 }

	public static function getDecimalData($id){
		$whereColumnArr=array("id"=>$id);
		$columnNameArr=array("decimal_char","disable_decimal","rounding_factor","round_up_or_down");
		$res = CQ::getRowsWithRequiredColumns('locations',$columnNameArr,$whereColumnArr);
		return $res;
	}

	public static function decimalize($price) {
		if(self::$decimal_char == ",") {
			$price = str_replace(".",",",$price);
		}
        $price = number_format((float)$price, 2, '.', '');
		$price  = self::truncate_number($price, self::$disable_decimal);
		return $price;
	}

	public static function roundingPrice($price) {
		$price = self::saveFormatedCurrency($price);
		$rounding_factor = strtolower(self::$rounding_factor);
		$round_up_or_down = strtolower(self::$round_up_or_down);
		$oprice = $price;
		if(!empty($rounding_factor) ) {
			$mod = fmod($price,$rounding_factor);
			if(!empty($round_up_or_down)) {
				if($round_up_or_down == strtolower("Up")) {
					$price = ($price - $mod + $rounding_factor);
				}
				else if($round_up_or_down == strtolower("Down")) {
					$price = ($price - $mod);
				}
			} else {
				if ($mod >= ($rounding_factor / 2)) {
					$price = ($price - $mod + $rounding_factor);
				} else {
					$price = ($price - $mod);
				}

			}
		}
		return array("price" => $price, "rounding_amount" => abs($oprice-$price));
	}

    public static function displayFormatedCurrency($monitory_amount, $dont_use_monetary_symbol = false) {
        global $location_info;
		$needToAddMinus = "";
		$number = $monitory_amount;

		$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
		if($location_info['decimal_char'] != "") {
		  $decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
		  $thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char']==".") ? "," : '.';
		}

		$thousands_sep = ",";
		if(!empty($thousands_char)) {
		  $thousands_sep = $thousands_char;
		}

		if(strpos($number,"-") !== false) {
			$find_minus = explode("-", $number);
			$needToAddMinus = "-";
		}

		if($monitary_symbol === "") {
		  $monitary_symbol = isset($location_info['monitary_symbol']) ? htmlspecialchars($location_info['monitary_symbol']) : '';
		}
		$left_or_right = isset($location_info['left_or_right']) ? $location_info['left_or_right'] : 'left';

		$left_side = "";
		$right_side = "";

		if (!$dont_use_monetary_symbol) {
			if ($left_or_right == 'left') {
				$left_side = $monitary_symbol;
			} else {
				$right_side = $monitary_symbol;
			}
		}
		$output = number_format($number, $precision, $decimal_char, $thousands_sep);
		return $output;
    }

	public static function saveFormatedCurrency($monitory_amount, $dont_use_monetary_symbol = false) {
		global $location_info;
		$needToAddMinus = "";
		$number = $monitory_amount;
		if(strpos($number,",") != false && strpos($number,".") != false && (strpos($number,".") > strpos($number,","))) {
		    $number =str_replace(",","",$number);
		} elseif(strpos($number,",") != false && strpos($number,".") != false) {
		    $number =str_replace(".","",$number);
		    $number =str_replace(",",".",$number);
		}

		$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
		$output = number_format($number, $precision, '.','');
		return $output;
	}


	public function displaytax($itemdetails, $discount=0) {
		$dataarr = array();
		$tCal = 0;
		$sTotal = 0;
		$eTax=0;
		$taxinform=array();
		$calculatedtaxamount = 0.00;
		if (!empty($itemdetails)) {
			// make into multidimensional
			if (!isset($itemdetails[0])) {
				$temp = $itemdetails;
				unset($itemdetails);
				$itemdetails[0] = $temp;
			}

			foreach ($itemdetails as $itemdetail) {
				$itemdetail['price'] = self::saveFormatedCurrency($itemdetail['price']);
				$itemdetail['forced_modifiers_price'] = self::saveFormatedCurrency($itemdetail['forced_modifiers_price']);
				$itemdetail['modify_price'] = self::saveFormatedCurrency($itemdetail['modify_price']);
				$perItemtotal = $itemdetail['price'] + $itemdetail['forced_modifiers_price'] + $itemdetail['modify_price'];
				$taxrate_arr = explode(";;", $itemdetail['apply_taxrate']);
				$discount_amount = self::saveFormatedCurrency(($perItemtotal * $discount)/100);
				$taxType = $itemdetail['tax_inclusion'];
				if ((int)$taxType === 1) {
					$calculatedTotalTax = self::calculatedTotalTax($taxrate_arr);
					$toCheckAlStack = self::toCheckAlStack($taxrate_arr);
				    if($toCheckAlStack === 'yes') {
						$calculatedStacktaxrate= self::calculatedStacktaxrate($taxrate_arr, self::saveFormatedCurrency($perItemtotal-$discount_amount));
				    }
				}

				$tax_amounts=0;
				foreach ($taxrate_arr as $apply_tax) {
					$taxDetails = explode("::",$apply_tax);
					$taxRate = explode(",",$taxDetails[2]);
					$taxTitle = explode(",",$taxDetails[1]);
					$taxIdArr = explode(",",$taxDetails[0]);
					$taxStack = explode(",",$taxDetails[3]);
					$subtotal += self::saveFormatedCurrency((float)$perItemtotal);
					$taxRateSum = array_sum($taxRate);
					$taxSum = self::saveFormatedCurrency($taxRateSum);
					if ($taxType == 1) {
						$taxSum = self::saveFormatedCurrency(($calculatedTotalTax*100)+100);
						$totalTaxRate = self::saveFormatedCurrency(($perItemtotal*100)/$taxSum);
					} else {
						$loop=0;
						foreach ($taxRate as $val) {
							if ($val>0) {
								if ($taxStack[$loop]==1) {
									$totalExTaxRate[] = self::saveFormatedCurrency(($perItemtotal+$tax_amounts) * $val);
									$totalExSubTotal[]= $perItemtotal+$tax_amounts;
									$tax_amounts+=($perItemtotal+$tax_amounts) * $val;
								} else {
									$totalExTaxRate[] = $perItemtotal * $val;
									$totalExSubTotal[]= $perItemtotal;
									$tax_amounts+=$perItemtotal * $val;
								}
							} else {
								$totalExTaxRate[] = 0;
								$totalExSubTotal[]= $perItemtotal;
							}
							$loop++;
						}
					}
					$tax = array_combine($taxTitle, $taxRate);
					$tTax = 0;
					$tData = 0;
					$tCal = 0;
					$sTotal = 0;
					$i = 0;
					$taxTypeStr ='';
					if (!empty($tax)) {
						foreach($tax as $key => $data) {
							if($data !== 0 && $data !== '') {
								$tTitle = $key;
								$tProfileID = $taxIdArr[$i];
								$tTax = self::saveFormatedCurrency($data*100);
								$tData = ($data*100)."%";
								if($taxType == 1) {
									$taxTypeStr = "itax";
									if($toCheckAlStack=='yes') {
										$sTotal = self::saveFormatedCurrency($calculatedStacktaxrate);
										$tCalArr[$tTitle] = self::saveFormatedCurrency(($calculatedStacktaxrate*$tTax)/100);
										$calculatedStacktaxrate = self::saveFormatedCurrency($calculatedStacktaxrate + $tCalArr[$tTitle]);
									} else {
		 								$tCal = self::saveFormatedCurrency(($totalTaxRate*$tTax)/100);
		 								$sTotal = self::saveFormatedCurrency($totalTaxRate);
		 								$sTotal = self::saveFormatedCurrency($sTotal * $itemdetail['quantity']);
		 								$sTotal = self::saveFormatedCurrency($sTotal - self::saveFormatedCurrency($sTotal * ($discount/100)));
		 								$totalTaxRate = self::saveFormatedCurrency(($perItemtotal*100)/$taxSum);
		 								$totalTaxRate = self::saveFormatedCurrency($totalTaxRate - self::saveFormatedCurrency($totalTaxRate * ($discount/100)));
		 								$tCalArr[$tTitle] = self::saveFormatedCurrency($tCalArr[$tTitle] + ($totalTaxRate*$tTax)/100);
									}
									$tCal += self::saveFormatedCurrency($tCalArr[$tTitle]);
								} else {
									$taxTypeStr = "etax";
									$tCalArr[$tTitle] = $totalExTaxRate[$i];
									$taxCalculatedDiscount = $tCalArr[$tTitle] * ($discount/100);
									$tCalArr[$tTitle] = self::saveFormatedCurrency($tCalArr[$tTitle] - $taxCalculatedDiscount);
									$tCalArr[$tTitle] = $tCalArr[$tTitle] - $taxCalculatedDiscount;
									$sTotal = self::saveFormatedCurrency($totalExSubTotal[$i]);
									$sTotal = self::saveFormatedCurrency($sTotal);
									$subTotalDiscount = self::saveFormatedCurrency($sTotal * ($discount/100));
									$sTotal = $sTotal - $subTotalDiscount;
									$tCal += self::saveFormatedCurrency($tCalArr[$tTitle]);
								}

								if(array_key_exists($tProfileID, $taxinform[$taxTypeStr])) {
									$sTotal = self::saveFormatedCurrency($taxinform[$taxTypeStr][$tProfileID]['subtotal']) + self::saveFormatedCurrency($sTotal);
									$taxAmount = self::saveFormatedCurrency($sTotal * ($tData / 100));
									if($taxinform[$taxTypeStr][$tProfileID]['type'] == $taxTyeStr) {
										$tCal = $tCal + $taxAmount;
										$taxinform[$taxTypeStr][$tProfileID][] = array(
											'title' => $tTitle,
											'subtotal' => self::saveFormatedCurrency($sTotal),
											'calculatedtaxamount' => $taxAmount,
											'rate' => $tData,
											'type' => $taxTypeStr,
											'discount' => $discount_amount
										);
									} else {
										$taxinform[$taxTypeStr][$tProfileID][] = array(
											'title' => $tTitle,
											'subtotal' => self::saveFormatedCurrency($sTotal),
											'calculatedtaxamount' => $taxAmount,
											'rate' => $tData,
											'type' => $taxTypeStr,
											'discount' => $discount_amount
										);
									}
								} else {

									$taxAmount = self::saveFormatedCurrency($sTotal * ($tData / 100));
									$tCal = $tCal + $taxAmount;
									$taxinform[$taxTypeStr][$tProfileID][] = array(
										'title' => $tTitle,
										'subtotal' => self::saveFormatedCurrency($sTotal),
										'calculatedtaxamount' => $taxAmount,
										'rate' => $tData,
										'type' => $taxTypeStr,
										'discount' => $discount_amount
									);
								}
								unset($tCalArr);
							}
							$i++;
						}
					}
					unset($taxRate);
					$calculatedtaxamount = 0;
					unset($taxTitle);
					$taxSum = 0;
					$totalTaxRate = 0;
					unset($totalExTaxRate);
					unset($totalExSubTotal);
					unset($tax);
				}
			}
		}
		$tax_arr = self::sum_tax_arr($taxinform);
		return $tax_arr;
	}

     public function caltotal($taxinform)
     {
		$actual_subtotal = 0;
		$POINT_ONE = 0.01;
     	if(!empty($taxinform))
     	{
			$disc_count = 0;
	     	foreach($taxinform as $key=>$val) {
	     		$itax_count = 0;
	     		foreach($val as $ks=>$vs) {
	     			$i = 1;
	     			foreach($vs as $k=>$v) {
	     				if($key == 'etax') {
	     					$eTaxes[] = $v['calculatedtaxamount'];
	     					$estotal[] = $taxinform[$key][$ks][$k]['subtotal'];
	     					$disc[] = $taxinform[$key][$ks][$k]['discount'];
	     					if(count($vs)>1 && $i>1) {
	     						$itax_count += $taxinform[$key][$ks][$k]['subtotal'];
	     						$disc_count += $taxinform[$key][$ks][$k]['discount'];
	     					}
	     				}
	     				if($key == 'itax') {
	     					$iTaxes[] = $v['calculatedtaxamount'];
	     					$istotal[] = $taxinform[$key][$ks][$k]['subtotal'];
	     					$disc[] = $taxinform[$key][$ks][$k]['discount'];
	     					if(count($vs)>1 && $i>1)
	     					{
	     						$itax_count += $taxinform[$key][$ks][$k]['subtotal'] + $v['calculatedtaxamount'];
	     						$disc_count += $taxinform[$key][$ks][$k]['discount'];
	     					}
	     				}
						$i++;

	     			}
	     		}
	     		if($key == 'etax') {
	     			$subtotal = array_sum($estotal);
	     			$total_eTax = array_sum($eTaxes);
	     			if($itax_count>0) {
	     				$subtotal = $subtotal - $itax_count;
	     			}
	     			$gtotal += $subtotal + $total_eTax;
	     		} else {
	     		    $subtotal_each = $istotal[0];
	     			$total_iTax = array_sum($iTaxes);
					$subtotal = $subtotal_each + $total_iTax;
					$nextNumber = round($subtotal);
					$isPointOneDifference = abs(round($nextNumber - $subtotal, 2)) === $POINT_ONE;
					if($isPointOneDifference) {
						$subtotal = $nextNumber;
					}
					$gtotal += $subtotal;
				 }
	     		$actual_subtotal = $actual_subtotal + $subtotal;
	     		$total_Disc = array_sum($disc);
	     		$total_Disc = $total_Disc - $disc_count;
	     	}
	     }

	     $total_arr['subtotal'] = self::displayFormatedCurrency($actual_subtotal);
	     $total_arr['grand_total'] = self::displayFormatedCurrency($gtotal);
	     $total_arr['calculatedtaxamount'] = self::displayFormatedCurrency($total_eTax);
	     $total_arr['discount'] = self::displayFormatedCurrency($total_Disc);
		 $total_arr['discount'] = $total_Disc;
	     return $total_arr;
     }

     public function sum_tax_arr($data)
		{
			$formatted_array = [];
			$keys = array_keys($data);
			for($i=0; $i< count($data); $i++)
			{
				foreach($data[$keys[$i]] as $key=>$val)
				{
					$profile_id = '';
					foreach($val as $nkey=>$nval)
					{
						if($profile_id == $key)
						{
							if(isset( $formatted_array[$keys[$i]][$key][$nval['title']] ))
							{
								$formatted_array[$keys[$i]][$key][$nval['title']]['title'] = $nval['title'];
								$formatted_array[$keys[$i]][$key][$nval['title']]['subtotal'] += $nval['subtotal'];
								$afterSumSubTotal = self::saveFormatedCurrency($formatted_array[$keys[$i]][$key][$nval['title']]['subtotal']);
								$taxAmount = self::saveFormatedCurrency($afterSumSubTotal * ($nval['rate'] / 100));
								$formatted_array[$keys[$i]][$key][$nval['title']]['calculatedtaxamount'] = $taxAmount;
								$formatted_array[$keys[$i]][$key][$nval['title']]['discount'] += $nval['discount'];
								$formatted_array[$keys[$i]][$key][$nval['title']]['rate'] = $nval['rate'];
								$formatted_array[$keys[$i]][$key][$nval['title']]['type'] = $nval['type'];
							}
							else
							{
								$formatted_array[$keys[$i]][$key][$nval['title']] = $nval;
							}

						}
						else
						{
							$formatted_array[$keys[$i]][$key][$nval['title']] = $nval;
							$profile_id = $key;
						}
					}
				}

			}
			return $formatted_array;
		}

		public static function getForcedModifiersTitlesStringByIds($idsArr) {
			if(!empty($idsArr)){
				$inClause = "WHERE `id` IN " . MQ::buildSanitizedListInParenthesis($idsArr, "'");
				$query = "select group_concat(TRIM(`title`) SEPARATOR ' - ') as title from `forced_modifiers` " . $inClause;
				$resQry = DBConnection::clientQuery($query);
				$result =  MA::resultToArray($resQry);
				if(!empty($result[0]['title'])){
					return $result[0]['title'];
				}
				else {
					return "";
				}
			}
		}

		public static function getMenuFieldDetailsById($field,$id){
		    $whereColumnArr= array("id" => $id, "_deleted" => 0);
		    $colNameArr = array($field);
		    $commonQueries = new CommonQueries();
		    $rowsResult = $commonQueries ->getRowsWithRequiredColumns('menu_items', $colNameArr, $whereColumnArr, '');
		    return $rowsResult[0][$field];
		}

		public static function calculatedTotalTax($taxrate_arr) {
		    $totalTaxRate='0';
		    foreach ($taxrate_arr as $apply_tax) {
		        $taxDetails = explode("::",$apply_tax);
		        $taxRate = $taxDetails[2];
		        $totalTaxRate =   $totalTaxRate+$taxRate;
		    }
		    return $totalTaxRate;
		}

		public static function toCheckAlStack($taxrate_arr) {
			$satck='yes';
			foreach ($taxrate_arr as $apply_tax) {
				$taxDetails = explode("::",$apply_tax);
				if ($taxDetails[3]==0) {
					$satck='no';
				}
			}
			return $satck;
		}

		public static function calculatedStacktaxrate($taxrate_arr, $amount) {
			foreach ($taxrate_arr as $apply_tax) {
				$taxDetails = explode("::",$apply_tax);
				$taxRate = $taxDetails[2];
				$amount = $amount/(1+$taxRate);
			}
			return $amount;
		}

		public static function getModifiersTitle($idList, $type) {
			$isForced = $type === self::$TYPE_FORCED_MODIFIERS;
			$table = $isForced ? 'forced_modifiers' : 'modifiers';
			$concat = $isForced ? ' - ' : ', ';
			if (!is_array($idList)) {
				$idList = explode(',', $param);
			}
			if (!empty($idList)) {
				$orderBy = "ORDER BY FIELD(`id`, ".MQ::buildSanitizedColumnInParenthesis($idList, "'").")";
				$modifiersSelection = OrdersQueries::selectAllIdsQuery($idList, $table);
				$query = "select group_concat(TRIM(`title`) ".$orderBy." SEPARATOR '".$concat."') as title from ".$modifiersSelection;
				$resQry = DBConnection::clientQuery($query);
				$result =  MA::resultToArray($resQry);
				if (isset($result[0]) && !empty($result[0]['title'])) {
					return $result[0]['title'];
				}
			}
			return "";
		}

		/**
		* This function is used to group order item details with item quantity.
		*
		* @param array $orderItemsDetails.
		*
		* @return array $groupOrderItemsList.
		*/
		public static function groupEquivalentOrderItems($orderItemsDetails) {
			$groupOrderItemsList = array();
			$qtyArray = array();
			foreach ($orderItemsDetails as $orderItems) {
				$combineData = $orderItems['item_id'].$orderItems['options'].$orderItems['special'];
				$qtyArray[$combineData][] = $orderItems['quantity'];
				$groupOrderItemsList[$combineData] = $orderItems;
				$groupOrderItemsList[$combineData]['quantity'] = array_sum($qtyArray[$combineData]);
			}

			return $groupOrderItemsList;
		}
}
