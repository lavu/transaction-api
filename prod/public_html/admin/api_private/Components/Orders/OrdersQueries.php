<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
//Class of static methods to provide abstraction to query calls.
//All methods should be static, and return the data if possible instead of mysqli_result.

include_once (dirname(__FILE__)."/../../Utils/JsonManager.php");
class OrdersQueries {

	public static function userOrders($userid, $otherClause) {
		$condition = '%approved":"1%';
		$limit = $otherClause['limit'];
		$sql = "select order_id,created_at, consumer_id,description, amount, order_information from lavutogo.orders where consumer_id=" . $userid . " and transaction_information like '" . $condition . "' order by created_at desc limit " . $limit;
		$resultObj = DBConnection::adminQuery($sql);
		return $resultObj;

	}

	public static function checkOrder($orderid) {

		$whereColumnArr = array("order_id" => $orderid);
		$ColNameArr = array("id", "order_id", "order_information", "amount", "transaction_information");

		$rowsResult = CQ::getRowsWithRequiredColumnsOtherDB('orders', $ColNameArr, $whereColumnArr, "", "", "lavutogo");

		return $rowsResult;
	}

	/**
	 * Returns a query to Select all rows containing given ids from a given table.
	 * Important! This query will return repeated rows in the result when repeated Ids are passed in the parameters.
 	*/
	public static function selectAllIdsQuery($ids, $table) {
		$modifiersSelection = "(";
		for($i=0; $i< count($ids); $i++) {
			if ($i > 0)
				$modifiersSelection .= " UNION ALL ";
			$modifiersSelection .= "select * from `".$table."` WHERE `id` = " . $ids[$i];
		}
		$modifiersSelection .= ") as result_rows";
		return $modifiersSelection;
	}

	public static function itemdetails($item_id, $quantity, $fmchecked, $omchecked, $special = "") {
		$itemquery = "SELECT `mi`.`id` ,`mi`.`name`, `mi`.`price`, `mi`.`category_id`, `mi`.`menu_id`, `mi`.`description`,
				`mi`.`active`, `mi`.`apply_taxrate`, `mi`.`modifier_list_id`, `mi`.`forced_modifier_group_id`, `mi`.`ingredients`,
				`mi`.`open_item`, `mi`.`super_group_id` , `mi`.`tax_inclusion`, `mi`.`tax_profile_id`, `mi`.`print`, IF(`mi`.`printer`!= '0', `mi`.`printer`, `mc`.`printer`) AS `printer`,
				`mi`.`custom_taxrate`,`mc`.`apply_taxrate` AS `category_apply_taxrate`,`mc`.`custom_taxrate` AS `category_custom_taxrate`,`mc`.`tax_inclusion` AS `category_tax_inclusion`,
				`mc`.`tax_profile_id` AS `category_tax_profile_id`, `mc`.`print_order` as `print_order` ";

		if (!empty($omchecked)) {
			$modifiersSelection = self::selectAllIdsQuery($omchecked, 'modifiers');
			$itemquery .= ", (SELECT sum(cost) from ".$modifiersSelection.") as `modifiers_price` ";
		}
		if (!empty($fmchecked)) {
			$forcedModifiersSelection = self::selectAllIdsQuery($fmchecked, 'forced_modifiers');
			$itemquery .= ", (SELECT sum(cost) from ".$forcedModifiersSelection.") as `forced_modifier_cost` ";
		}


		$itemquery .= " FROM `menu_items` AS `mi`
				LEFT JOIN `menu_categories` as `mc` ON `mi`.`category_id` = `mc`.`id`
				WHERE  `mi`.`id` = '" . $item_id . "' ";

		$result = DBConnection::clientQuery($itemquery);
		$resultArr = MA::resultToArray($result);
		unset($result);

		foreach ($resultArr as $valArr) {

			$tax_rate = 0.00;
			$taxrate_title = "";
			$tax_profile_id = 0;
			$apply_taxrate = '';
			$apply_taxrate1 = '';
			$apply_taxrate2 = '';

			$tax_rate = OrdersQueries::taxrate($valArr);
			/**
			 * Format for Apply Tax Rate
			 * tax_profiles->id::tax_profiles->title::tax_rates->calc::tax_rates->stack::tax_rates->tier_type::tax_rates->tier_value::tax_rates->id
			 */
			if ($valArr['tax_profile_id'] != '') {
				if ($valArr['apply_taxrate'] == 'Custom' && $valArr['custom_taxrate'] != '') {
					$tax_profile_id = '0';
				} else {
					$tax_profile_id = $valArr['tax_profile_id'];
				}
			} elseif ($valArr['tax_profile_id'] == '' && $valArr['category_tax_profile_id'] !== '') {
				if ($valArr['category_apply_taxrate'] == 'Custom' && $valArr['category_custom_taxrate'] != '') {
					$tax_profile_id = '0';
				} else {
					$tax_profile_id = $valArr['category_tax_profile_id'];
				}
			} else {
				$tax_profile_id = '0';
			}
			$taxrate_title = explode(',', $tax_rate['taxrate_title']);
			$tax_rates = explode(',', $tax_rate['tax_rate']);
			$tax_id = explode(',', $tax_rate['tax_id']);
			$tax_stack = explode(',', $tax_rate['tax_stack']);

			for ($j = 0; $j <= count($taxrate_title) - 1; $j++) {
				$tax_prof = $taxrate_title[$j];
				$rates = $tax_rates[$j];
				$tx_id = $tax_id[$j];
				$tx_stack = $tax_stack[$j];
				if ($tax_profile_id != '0') {

					$tax_prof = $tax_prof;
				} else {
					$tax_prof = 'Tax';
				}

				if ($valArr['apply_taxrate'] == 'Custom' && $valArr['custom_taxrate'] != '') {
					$rates = $valArr['custom_taxrate'];
				} else if ($rates != '') {
					$rates = $rates;
				} else {
					$rates = '0.00';
				}
				if ($tx_id != '' && $valArr['apply_taxrate'] != 'Custom') {
					$tx_id = $tx_id;
				} else {
					$tx_id = '0';
					$tx_stack = '0';
				}
				if ($j == (count($taxrate_title) - 1)) {
					$apply_taxrate .= $tax_profile_id . '::' . $tax_prof . '::' . $rates . '::' . $tx_stack . '::::::::' . $tx_id;
				} else {
					$apply_taxrate .= $tax_profile_id . '::' . $tax_prof . '::' . $rates . '::' . $tx_stack . '::::::::' . $tx_id . ';;';
				}

			}
			$tax_inclusion = $tax_rate['tax_type'];

			$fmcost = isset($valArr['forced_modifier_cost']) ? $valArr['forced_modifier_cost'] : 0;
			$omcost = isset($valArr['modifiers_price']) ? $valArr['modifiers_price'] : 0;
			$result = array(
				"item" => $valArr['name'],
				"price" => $valArr['price'],
				"quantity" => $quantity,
				"options" => '',
				"special" => $special,
				"modify_price" => $omcost,
				"print" => $valArr['print'],
				"print_order"=>$valArr["print_order"],
				"check_number" => 0,
				"seat_number" => 0,
				"item_id" => $valArr['id'],
				"printer" => $valArr['printer'],
				"apply_taxrate" => $apply_taxrate,//$valArr['tax_profile_id']."::".$tax_rate['taxrate_title']."::".$tax_rate['tax_rate']."::".$tax_rate['tax_type']."::".$tax_rate['tax_id']."::0" ,
				"custom_taxrate" => "",
				"modifier_list_id" => implode(",", $omchecked),						//option
				"forced_modifier_group_id" => $valArr['forced_modifier_group_id'],
				"forced_modifiers_price" => $fmcost,
				"forced_modifier_list_id" => implode(",", $fmchecked),						//Force modifier option
				"course_number" => 1,
				"open_item" => $valArr['open_item'],
				"allow_deposit" => 1,
				"void" => 0,
				"device_time" => date("Y-m-d H:i:s"),
				"tax_inclusion" => $tax_inclusion,
				"sent" => 0,
				"tax_exempt" => 0,
				"checked_out" => 0,
				"price_override" => 0
			);
		}

		return $result;
	}

	// order id genarate
	public static function generateOrderId() {
		$query = "SELECT MAX(CONVERT(SUBSTRING_INDEX(order_id, '2-', -1),UNSIGNED INTEGER)) AS `order_id` FROM `lavutogo`.`orders` where `order_id` like '2-%' ";//"SELECT MAX(SUBSTRING_INDEX(order_id, '2-LavuToGo-', -1)) AS `order_id` FROM `lavutogo`.`orders` ";

		$result = DBConnection::adminQuery($query);
		$order_data = MA::resultFirstRow($result);
		if (!empty($order_data)) {
			$order_id = $order_data['order_id'] + 1;
			$order_id = "2-" . $order_id;
			$columnNames = array('order_id');
			$insertValue = array(
				'order_id' => $order_id
			);
			$rowValue = array_values($insertValue);
			CQ::insertArray('`lavutogo`.`orders`', $columnNames, array($rowValue), false);
		} else {
			$order_id = "2-1";
		}


		return $order_id;
	}

	public static function getTaxData($profile_id) {

		$whereColumnArr = array("profile_id" => $profile_id, "_deleted" => 0);
		$ColNameArr = array("title", "calc", "id", "stack");
		$rowsResult = CQ::getRowsWithRequiredColumns('tax_rates', $ColNameArr, $whereColumnArr, '');

		return $rowsResult;
	}

	public static function checkLocationTax() {
		$whereColumnArr = array("id" => 1);
		$locationColNameArr = array("taxrate", "tax_inclusion");
		$rowsResult = CQ::getRowsWithRequiredColumns('locations', $locationColNameArr, $whereColumnArr, '');
		return $rowsResult;
	}

	public static function taxrate($args) {
		//  tax logic
		$tax_type = "";
		$tax_rate = "";
		$taxrate_title = "";
		$tax_id = "";
		$tax_stack = "";

		if ($args['apply_taxrate'] == 'Custom') {
			if ($args['custom_taxrate'] != '') {
				$tax_rate = $args['custom_taxrate'];
				$taxrate_title = $args['apply_taxrate'];
				$tax_id = 0;
				$tax_stack = 0;
			} elseif ($args['custom_taxrate'] == '' && $args['tax_profile_id'] != '') {
				$tax_data = OrdersQueries::getTaxData($args['tax_profile_id']);
			} elseif ($args['custom_taxrate'] == '' && $args['tax_profile_id'] == '' && $args['category_apply_taxrate'] == 'Custom' && $args['category_custom_taxrate'] != '') {
				$tax_rate = $args['category_custom_taxrate'];
				$taxrate_title = $args['category_apply_taxrate'];
				$tax_id = 0;
				$tax_stack = 0;
			} elseif ($args['custom_taxrate'] == '' && $args['tax_profile_id'] == '' && ($args['category_apply_taxrate'] == 'Custom' || $args['category_apply_taxrate'] != 'Custom') && $args['category_tax_profile_id'] != '') {
				$tax_data = OrdersQueries::getTaxData($args['category_tax_profile_id']);
			} else {
				$tax_rate = 0.00;
				$taxrate_title = "";
				$tax_id = '';
				$tax_stack = '';
			}

		} else {
			if ($args['tax_profile_id'] != '') {
				$tax_data = OrdersQueries::getTaxData($args['tax_profile_id']);
			} elseif ($args['tax_profile_id'] == '' && $args['category_apply_taxrate'] == 'Custom' && $args['category_custom_taxrate'] != '') {
				$tax_rate = $args['category_custom_taxrate'];
				$taxrate_title = $args['category_apply_taxrate'];
				$tax_id = 0;
				$tax_stack = 0;
			} elseif ($args['tax_profile_id'] == '' && ($args['category_apply_taxrate'] == 'Custom' || $args['category_apply_taxrate'] != 'Custom') && $args['category_tax_profile_id'] != '') {
				$tax_data = OrdersQueries::getTaxData($args['category_tax_profile_id']);
			} else {
				$tax_rate = 0.00;
				$taxrate_title = "";
				$tax_id = '';
				$tax_stack = '';
			}
		}

		if (!empty($tax_data)) {
			foreach ($tax_data as $tax) {
				$tax_rate .= $tax['calc'] . ",";
				$taxrate_title .= $tax['title'] . ",";
				$tax_id .= $tax['id'] . ",";
				$tax_stack .= $tax['stack'] . ",";
			}
			$tax_rate = substr($tax_rate, 0, -1);
			$taxrate_title = substr($taxrate_title, 0, -1);
			$tax_id = substr($tax_id, 0, -1);
			$tax_stack = substr($tax_stack, 0, -1);
		}


		// check tax_inclusion
		// O means Exclusive
		// 1 Means Inclusive
		if ($args['tax_inclusion'] == '1') {
			$tax_type = "1";
		} else if ($args['tax_inclusion'] == '0') {
			$tax_type = "0";
		} else {
			// category level
			if ($args['category_tax_inclusion'] == '0') {
				$tax_type = "0";
			} else if ($args['category_tax_inclusion'] == '1') {
				$tax_type = "1";
			} else {
				$value = OrdersQueries::checkLocationTax();
				if ($value[0]['tax_inclusion'] == "1") {
					$tax_type = "1";
				} else {
					$tax_type = "0";
				}

			}
		}

		$tax_data = array("tax_rate" => $tax_rate, "taxrate_title" => $taxrate_title, "tax_type" => $tax_type, "tax_id" => $tax_id, "tax_stack" => $tax_stack);

		return $tax_data;
	}

	public static function displayFormatedCurrency($monitory_amount, $dont_use_monetary_symbol = false) {
		global $location_info;
		$needToAddMinus = "";
		$number = $monitory_amount;

		$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
		if ($location_info['decimal_char'] != "") {
			$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
			$thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char'] == ".") ? "," : '.';
		}
		$thousands_sep = ",";
		if (!empty($thousands_char)) {
			$thousands_sep = $thousands_char;
		}
		if (strpos($number, "-") !== false) {
			$find_minus = explode("-", $number);
			$needToAddMinus = "-";
		}
		$monitary_symbol = isset($location_info['monitary_symbol']) ? htmlspecialchars($location_info['monitary_symbol']) : '';
		$left_or_right = isset($location_info['left_or_right']) ? $location_info['left_or_right'] : 'left';

		$left_side = "";
		$right_side = "";

		if (!$dont_use_monetary_symbol) {
			if ($left_or_right == 'left') {
				$left_side = $monitary_symbol;
			} else {
				$right_side = $monitary_symbol;
			}
		}
		$output = number_format($number, $precision, $decimal_char, $thousands_sep);
        //echo $output;
		return $output;
	}

	public static function saveFormatedCurrency($monitory_amount, $dont_use_monetary_symbol = false) {
		global $location_info;
		$needToAddMinus = "";
		$number = $monitory_amount;
		if (strpos($number, ",") != false && strpos($number, ".") != false && (strpos($number, ".") > strpos($number, ","))) {
			$number = str_replace(",", "", $number);
		} elseif (strpos($number, ",") != false && strpos($number, ".") != false) {
			$number = str_replace(".", "", $number);
			$number = str_replace(",", ".", $number);
		}
		if ($location_info['decimal_char'] != "") {
			$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
			$thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char'] == ".") ? "," : '.';
		}

		$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
		$output = number_format($number, $precision, $decimal_char, $thousands_char);
		return $output;
	}

	public static function getOptionalModifiers($optionalModifiers, $select = [], $where = []) {
		return CQ::getNonDeletedRowsWhereColumnIn('modifiers', 'id', $optionalModifiers, $select);
	}

	public static function updateOrderInformation($orderId, $params, $otherColumns = []) {
		$result = false;
		if(is_array($params) && !empty($orderId)) {
			$orderDetails = self::checkOrder($orderId);
			if (isset($orderDetails[0])) {
				$orderInformation = json_decode($orderDetails[0]['order_information'], true);
				foreach ($params as $key => $value) {
					if (isset($orderInformation[$key])) {
						$orderInformation[$key] = $value;
					}
				}
				$updateColumns = array_merge($otherColumns, ['order_information' => JsonManager::encodeSpecialCharacters($orderInformation)]);
				CQ::updateArray('lavutogo.orders', $updateColumns, "where `order_id` = '" .$orderId. "'", false);
				$result = $updateColumns;
			}
		}
		return $result;
	}
}
