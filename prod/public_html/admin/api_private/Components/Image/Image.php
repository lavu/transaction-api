<?php

class Image
{
	const IMAGE_DEFAULT_SERVER_URL = "https://admin.poslavu.com/images/";

	const IMAGE_FILE_URL = "/home/poslavu/public_html/admin/images/";

	const DEFAULT_ITEM_IMAGE_URL = "https://admin.poslavu.com/images/lavutogo/items/DefaultImage.png";
	const DEFAULT_CATEGORY_IMAGE_URL = "https://admin.poslavu.com/images/lavutogo/categories/DefaultImage.png";

	const CATEGORY_TYPE = "categoryType";
	const MENU_ITEM_TYPE = "menuItemType";

	protected $_storageKey;
	protected $_imageName;
	protected $_dataName;
	protected $_type;
	
	public function __construct($type, $storageKey, $imageName, $dataName)
	{
		$this->_storageKey = $storageKey;
		$this->_imageName = $imageName;
		$this->_dataName = $dataName;
		$this->_type = $type;
	}

	//	If storageKey exist, will return AWS image path, else will continue returning same as before.
	//	Will maintain logic that existed before to return the data that frontend is expecting
	//	Frontend is expecting image with the full url
	//	Frontend is expecting itemImagePresent as Y or N
	public function getImage(){
		$image = $this->getDefaultImageUrl();
		$itemImagePresent = 'N';
		if($this->_storageKey){
			$image = getenv('S3IMAGEURL') . $this->_storageKey;
			$itemImagePresent = 'Y';
		} else if( $this->_imageName && file_exists($this->getFileUrl()) ){
			$image = $this->getImageUrl();
			$itemImagePresent = 'Y';
		}
		return (object) array(
			'image' => $image,
			'itemImagePresent' => $itemImagePresent
		);
	}

	protected function getDefaultImageUrl(){
		switch ($this->_type) {
			case self::CATEGORY_TYPE:
				return self::DEFAULT_CATEGORY_IMAGE_URL;
			case self::MENU_ITEM_TYPE:
				return self::DEFAULT_ITEM_IMAGE_URL;
			default:
				return '';
		}
	}

	protected function getImageUrl(){
		return self::IMAGE_DEFAULT_SERVER_URL . $this->getImagePath();
	}

	protected function getFileUrl(){
		return self::IMAGE_FILE_URL . $this->getImagePath();
	}

	protected function getImagePath(){
		switch ($this->_type) {
			case self::CATEGORY_TYPE:
				return $this->getCategoryPath();
			case self::MENU_ITEM_TYPE:
				return $this->getMenuItemPath();
			default:
				return '';
		}
	}

	protected function getCategoryPath(){
		return $this->_dataName . "/categories/full/" . $this->_imageName;
	}

	protected function getMenuItemPath(){
		return $this->_dataName . "/items/full/" . $this->_imageName;
	}
}