<?php
require_once dirname( __FILE__ ).'/../../../lib/constants.php';
require_once( __DIR__.'/../../../../inc/olo/braintree/BraintreeConnect.php');

include_once (dirname(__FILE__)."/../../Utils/JsonManager.php");

//error_reporting(E_ALL);
//ini_set('display_errors',1);
/*
 * DataController for Payment
*/
class PaymentDataController extends DataControllerBase{
	public static $loc_info = array();

	public static $restaurant_id;
	private static $CQ;
	public static $decimal_char;
	public static $disable_decimal;
	public static $rounding_factor;
	public static $round_up_or_down;
	private static $lavu_channel = "Lavu_SP";
	private $onSuccessMessage = LTG_ON_ORDER_CHECKOUT_MESSAGE;
	private $braintreeConnect;

	public function __construct($request, $dataname)
	{
		parent::__construct(new ReflectionClass(__CLASS__), $request, $dataname);
		$res = self::getDecimalData(1, $dataname);
		self::$decimal_char = $res[0]['decimal_char'];
		self::$disable_decimal = $res[0]['disable_decimal'];
		self::$rounding_factor = $res[0]['rounding_factor'];
		self::$round_up_or_down = $res[0]['round_up_or_down'];

		self::$loc_info = OrdersDataController::getLocationData($dataname);
		$commonQueries = new CommonQueries();

		// initialize a common query object
		if(!isset(self::$CQ)) {
			self::$CQ = $commonQueries;
		}
		// Set restaurant_id (needed for original_id values on payments)
		$restaurants = $commonQueries->getRowsWithRequiredColumnsMainDB(
			'restaurants',
			array('id'),
			array('data_name' => $dataname),
			'',
			''
		);

		$restaurant = array_shift($restaurants);
		self::$restaurant_id = $restaurant['id'];

		$payment_result = $this->getPaymentInfo(1, $dataname); // fetch paypal merchant data
		$this->braintreeConnect = new BraintreeConnect($this->getPaymentInfo(1, $dataname));

		/*
		* Set on success message on object creation
		*/
		$rowsResult = $commonQueries->getRowsWithRequiredColumns('config', array("value"), array("setting" => "ltg_on_checkout_success_message", "type" => "location_config_setting"));

		if($rowsResult)
		{
			$this->onSuccessMessage = trim($rowsResult[0]['value']);
		}
	}

	private function getPaymentInfo($loc_id, $dataname){
		$res = CQ::getRowsWithRequiredColumns('locations',array("ltg_payment_info"),array("id"=>$loc_id),'','',$dataname);
		$info = json_decode($res[0]['ltg_payment_info'], 1);
		return $info['paypal'];
	}

	protected function action_initiateCOD($args) {
	    $languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;
		$dataname = isset($args['dataname'])?$args['dataname']:'';
		$order_id = isset($args['order_id'])?$args['order_id']:'';
		$user_id = isset($args['user_id'])?$args['user_id']:0;
		$order_total = isset($args['payable_amount'])?$args['payable_amount']:'';
		$order_type = isset($args['order_type'])?trim($args['order_type']):'';
		$order_type = str_replace('-','',strtolower($order_type));
		$togo_email = $email = isset($args['email_id'])?$args['email_id']:'';
		$togo_phone = $phoneNumber = isset($args['phone_number'])?$args['phone_number']:'';
		$first_name = isset($args['first_name'])?$args['first_name']:'';
		$last_name = isset($args['last_name'])?$args['last_name']:'';
		$togo_name = $first_name." ".$last_name;
		$customer_address = isset($args['customer_address'])?$args['customer_address']:'';
		$lavu_pay_type = isset($args['lavu_pay_type'])?$args['lavu_pay_type']:'';
		$lavu_card_number = isset($args['lavu_card_number'])?$args['lavu_card_number']:'';
		$discount_amount = isset($args['lavu_pay_amount'])?$args['lavu_pay_amount']:'';
		$discountId = isset($args['discount_id'])?$args['discount_id']:'';
		$tip_type = isset($args['tip_type'])?$args['tip_type']:'';
		$tipAmount = isset($args['tip_amount'])?$args['tip_amount']:0;
		$delivery_fee = isset($args['delivery_fee'])?$args['delivery_fee']:0;
		$orderItemsDetails = $args['orderItemsDetails'];
		$dining_date = $args['dining_date'];
		$dining_time = $args['dining_time'];
		$special_instructions = isset($args['delivery_instructions'])?trim($args['delivery_instructions']):'';
		$togo_status= $this->toGetOrderTypeIdBaseOnOrderType($args['order_type']);
		$location_id = isset($args['location_id']) ? $args['location_id'] : 1;
		$payment = array();
		$error = array();
		$orderUpdate = array();
		$ltg_minimum_order_amount = 0;
		$rowsResult = CQ::getRowsWithRequiredColumns('config', array("value"), array("setting" => "ltg_minimum_delivery_amount"));

		if (!empty($rowsResult)) {
			$ltg_minimum_delivery_amount = $rowsResult[0]['value'];
		}
		if ($order_type == 'delivery' && (($order_total - $delivery_fee) < $ltg_minimum_delivery_amount)) {
		    $error[] = apiSpeak($languageid, "Order Amount should be greater than Minimum Delivery Amount");
			return array("status"=>"F", "msg"=>$error);
		}

		if(empty($order_id))
		{
		    $error[] = apiSpeak($languageid, "Order Id is required");
		}

		if(empty($dataname))
		{
		    $error[] = apiSpeak($languageid, "Dataname is required");
		}

		if(empty($orderItemsDetails))
		{
		    $error[] = apiSpeak($languageid, "Order Items Details are required");
		}

		if(empty($order_total))
		{
		    $error[] = apiSpeak($languageid, "Order Amount is required");
		}

		if(empty($order_type))
		{
		    $error[] = apiSpeak($languageid, "Order Type is required");
		}

		if(empty($email))
		{
		    $error[] = apiSpeak($languageid, "Email Address is required");
		}

		if(empty($phoneNumber))
		{
		    $error[] = apiSpeak($languageid, "Telephone Number is required");
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		    $error[] = apiSpeak($languageid, "Invalid Email Address");
		}

		if(empty($first_name))
		{
		    $error[] = apiSpeak($languageid, "First Name is required");
		}

		if(empty($last_name))
		{
		    $error[] = apiSpeak($languageid, "Last Name is required");
		}

		if (!in_array($order_type, array(DELIVERY, PICKUP, DINE_IN))) {
		    $error[] = apiSpeak($languageid, "Invalid Order Type");
		}

		if ($order_type == DELIVERY) {
			if(!isset($customer_address) || empty($customer_address))
			{
			    $error[] = apiSpeak($languageid, "Customer Address is required");
			}
		}

		if($delivery_fee < 0){
		    $error[] = apiSpeak($languageid, "Invalid Delivery Fee");
		}

		$order_arr = OrdersQueries::checkOrder($order_id);

		if(!isset($order_arr[0]['order_id']) && empty($order_arr[0]['order_id'])){
		    $error[] = apiSpeak($languageid, "Invalid Order Id");
		}

		if(isset($order_arr[0]['transaction_information']) && !empty($order_arr[0]['transaction_information'])){
			$transaction_information = json_decode($order_arr[0]['transaction_information'],1);
			if($transaction_information['approved'] == 1){
			    $error[] = apiSpeak($languageid, "Order is closed");
			}
		}

		/*
	 	*inventory checking
	 	*/
		$paramsInvCheck = array('order_id'=>$order_id, 'languageid'=>$languageid);
		$returnInvCheck = $this->inventoryCheckWhenCheckout($paramsInvCheck);
		if($returnInvCheck!=''){
			return $returnInvCheck;
		}

		// updating order before payment
		$order_result = OrdersDataController::orderUpdate($orderItemsDetails, $order_id, $dataname, $location_id);

		if($order_result['status'] == "F"){
		    return array("status" => "F", "details" => $order_result['result'], "msg" => apiSpeak($languageid, "Some items are out of stock"));
		}

		if(!empty($lavu_pay_type)){

			if($lavu_pay_type != "LP" && $lavu_pay_type != "GC"){
			    $error[] = apiSpeak($languageid, "Invalid Pay Type");
			}
			else{
				if(empty($args['lavu_card_number'])){ // validating lavu gift card number
				    $error[] = apiSpeak($languageid, "Lavu Card No is required");
				}

				if($lavu_pay_type == "LP"){ // Loyalty Points
					if($args['lavu_add_points'] == 1){ // award points
						if($discount_amount <= 0){
						    $error[] = apiSpeak($languageid, "Invalid Award Points");
						}
						else{
							$lavu_award_points_details = $this->action_checkAwardPoints(array("lavu_card" => $lavu_card_number, "dataname" => $dataname, "orderId" => $order_id, "languageid" => $languageid));
							//print_r($lavu_award_points_details);exit;
							if($lavu_award_points_details['status'] == "F"){
								$error[] = $lavu_award_points_details['error'];
							}
							else if($lavu_award_points_details['status'] == "1")
							{
								if($lavu_award_points_details['awardpoints'] > 0){
									if((float)$lavu_award_points_details['awardpoints'] != (float)$discount_amount){
									    $error[] = apiSpeak($languageid, "Invalid Award Points");
									}
									else{
										$lavu_award_points = $lavu_award_points_details['awardpoints'];
									}
								}
							}
						}
					}
					else if($args['lavu_add_points'] == 0){ // discount points
						if(empty($discountId))
						{
						    $error[] = apiSpeak($languageid, "Loyalty Points Discount Id is required");
						}
						else{
							if($discount_amount <= 0){
							    $error[] = apiSpeak($languageid, "Invalid Discount Amount");
							}
							else{
							    $discount_details = $this->action_checkLoyaltyDiscountPoints(array("lavu_card" => $lavu_card_number, "dataname" => $dataname, "orderId" => $order_id, "discountId" => $discountId, "orderItemsDetails" => $orderItemsDetails, "languageid" => $languageid));
								//print_r($discount_details);exit;
								if($discount_details['status'] == "F"){
									$error[] = $discount_details['error'];
								}
								else if($discount_details['status'] == "1")
								{
									if(empty($discount_details['discounttypes'])){
									    $error[] = apiSpeak($languageid, "Invalid Discount Id");
									}
									else if((float)trim($discount_details['discounttypes'][0]['discount_value']) != (float)$discount_amount){
									    $error[] = apiSpeak($languageid, "Invalid Discount Value");
									}
								}
							}
						}
					}
				}
				else if($lavu_pay_type == "GC"){ // gift card
					if($discount_amount <= 0){
					    $error[] = apiSpeak($languageid, "Invalid Lavu Gift Card Amount");
					}
					else{
						$GiftCardDetails = $this->action_getLavuGiftCardDetails(array('lavu_card' => $lavu_card_number,'dataname' => $dataname, 'languageid' => $languageid));
						//print_r($GiftCardDetails);
						if($GiftCardDetails['status'] == "F"){
							$error[] = $GiftCardDetails['error'];
						}
						else if($GiftCardDetails['status'] == "1")
						{
							if($GiftCardDetails['lavu_gift_card_balance'] <= 0){
							    $error[] = apiSpeak($languageid, "Insufficient Lavu Gift Card Amount");
							}
							else if($GiftCardDetails['lavu_gift_card_balance'] != $discount_amount){
							    $error[] = apiSpeak($languageid, "Invalid Lavu Gift Card Amount");
							}
						}
					}
				}
			}
		}

		if(!empty($tip_type)){ // Tip Validation
			if($tip_type != "C" && $tip_type != "P"){
			    $error[] = apiSpeak($languageid, "Invalid Tip Type");
			}
			else{
				if($tipAmount <= 0){
				    $error[] = apiSpeak($languageid, "Invalid Tip Amount");
				}
			}
		}

		if(!empty($error)){
		error_log(print_r($error,true));
			return array("status"=>"F", "msg"=>$error);
		}
		$order_arr = OrdersQueries::checkOrder($order_id);
		$o_rowsResult = CQ::getRowsWithRequiredColumns('orders', array("subtotal","total"), array("order_id" => $order_id));
		if($discountId =='')
		{
			$orderAmount= $order_arr[0]['amount'];
		}
		else {
			$orderAmount= $o_rowsResult[0]['total'];
		}
		$res = self::getDecimalData(1, $dataname);
		self::$decimal_char = $res[0]['decimal_char'];
		self::$disable_decimal = $res[0]['disable_decimal'];
		self::$rounding_factor = $res[0]['rounding_factor'];
		self::$round_up_or_down = $res[0]['round_up_or_down'];

		$isLavuAddPoints = (int) $args['lavu_add_points'];//add lavu award points if this value is having 1 or for 0 deduct the available balance from card

		//echo "order amount: ",$orderAmount,"<br>","Tip amount : ",$tipAmount,"<br>","Discount amount: ",$discount_amount,"<br>";
		//echo "Payable amt: ",$order_total,"<br>"; exit;
		//error_log("order amount: ".$orderAmount."<br>"."Tip amount : ".$tipAmount."<br>"."Discount amount: ".$discount_amount."<br>");
		//error_log("Payable amt: ".$order_total);

		//error_log($orderAmount+$tipAmount." = ".$order_total + ($isLavuAddPoints == 1 ? 0 : $discount_amount));
		$tipAmount = self::saveFormatedCurrency($tipAmount);
		$left_total = trim($orderAmount+$tipAmount) - ($isLavuAddPoints == 1 ? 0 : $discount_amount);
		$right_total = trim($order_arr[0]['amount'] - ($isLavuAddPoints == 1 ? 0 : $discount_amount));
		$right_total = $right_total + $tipAmount;

		if(trim($left_total) != trim($right_total)){
			error_log("Order amount is not valid.");
			return array('status'=>"F",'msg'=> array(apiSpeak($languageid, 'Invalid Order Amount')));
		}

		$whereColumnDiscnt= array("id" => $discountId);
		$resultDiscnt = CQ::getRowsWhereColumn('discount_types', $whereColumnDiscnt, '', '', $dataname);

		$whereColumnOrder=array("order_id"=>$order_id);
		$columnNameOrder=array("subtotal","tax","total");
		$resOrder = CQ::getRowsWithRequiredColumns('orders',$columnNameOrder,$whereColumnOrder);

		$discnt_p = $resultDiscnt[0]['calc']*100;



		if($isLavuAddPoints == 0 && ($lavu_pay_type == "GC" || $lavu_pay_type == "LP")){
			if($lavu_pay_type == "GC"){
				$lavu_gc_card_amt = $discount_amount;
			}
			/*if($tmp_order_total <= 0){
				$order_total = $order_total + $discount_amount;
				$discount_amount = $order_total;
			}
			else{
				$order_total = $order_total + $discount_amount;
			}*/
		}

		$rowsResult = CQ::getRowsWithRequiredColumns('config', array("value"), array("setting" => "ltg_minimum_order_price"));

		if(!empty($rowsResult)){
			$ltg_minimum_order_amount = $rowsResult[0]['value'];
		}

		/*if($order_total < $ltg_minimum_order_amount){
			return $return = array("status" => "F", "msg" => "Order Total should be greater than Minimum Order Amount");
		}*/

		if($isLavuAddPoints == 0 && ($lavu_pay_type == "GC")){
			if($order_total > $lavu_gc_card_amt){
				//$order_total = $order_total - $lavu_gc_card_amt;
				$discount_amount = $lavu_gc_card_amt;
			}
			else{
				//$discount_amount = $order_total;
				$order_total = 0;

			}
		}
		//echo "order total: ".$order_total."<br>"."discount: ".$discount_amount."<br>";
		//exit;

		if($lavu_pay_type!=''){
			if($lavu_pay_type=='LP'){

				//print_r($discount_details);exit;
				if($isLavuAddPoints == 1){
					$loyaltyDetails['loyalty_type'] = 'a';
					$loyaltyDetails['awardpoints'] = $discount_amount;
				}
				else if($isLavuAddPoints == 0){
					if(!empty($discount_details['discounttypes'])){
						$loyaltyDetails['loyalty_type'] = ($discount_details['discounttypes'][0]['loyalty_use_or_unlock']==1)?'d':'';
						$loyaltyDetails['discount_points'] = $discount_details['discounttypes'][0]['discount_points'];

						if($discount_details['discounttypes'][0]['discount_value']>=$orderAmount){
							$payableAmount = $discount_details['discounttypes'][0]['discount_value']-$orderAmount;
						}
						if($discount_details['discounttypes'][0]['discount_value']<=$orderAmount){
							$payableAmount = $orderAmount-$discount_details['discounttypes'][0]['discount_value'];
						}
						$orderUpdate['discount_id'] = $discount_details['discounttypes'][0]['discount_id'];
						$orderUpdate['discount'] = $discount_amount;
						$orderUpdate['discount_value'] = $discount_amount;
						$orderUpdate['discount_sh'] = $discount_details['discounttypes'][0]['title'];
						$orderUpdate['discount_type'] = $discount_details['discounttypes'][0]['discount_type'];
					}
				}
			}
			else if($lavu_pay_type=='GC'){
				$loyaltyDetails['loyalty_type'] = 'GC';
				 $loyaltyDetails['discount_amount'] = $discount_amount;
			}

			$loyaltyDetails['cardnumber'] =  $lavu_card_number;
			$loyaltyDetails['dataname'] =  $dataname;

		}
		// print_r($loyaltyDetails);exit;

		//LP-5034
		$discountdata = array();
		if(!empty($discount_details['discounttypes'][0]))
		    $discountdata = $discount_details['discounttypes'][0];

		if(empty($dining_time))
			$dining_time = date("h:i A");

		$locationOrderUpdate['phone_number'] = $phoneNumber;
		$locationOrderUpdate['email_id'] = $email;
		$locationOrderUpdate['first_name'] = $first_name;
		$locationOrderUpdate['last_name'] = $last_name;
		$locationOrderUpdate['delivery_instructions'] = $args['delivery_instructions'];
		$locationOrderUpdate['customer_address'] = $customer_address;
		$locationOrderUpdate['order_type'] = $order_type;
		$locationOrderUpdate['delivery_fee'] = $delivery_fee;
		$locationOrderUpdate['dining_time'] = $dining_time;
		$locationOrderUpdate['dining_date'] = $dining_date;
		$locationOrderUpdate['discount_info'] = $discountdata;//LP-5034
		$locationOrderUpdate['tip_amount'] = $tipAmount;
		$locationOrderUpdate['payment_type'] = 'cash';
		$locationOrderUpdate['card_type'] = 'cash';
		$locationOrderUpdate['user_id'] = $user_id;

		$location_info = LocationDataController::action_getLocationInfo(1, $dataname);
		$local_datetime = substr(LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]),0,-1); //date("Y-m-d H:i:s");

		$newInventory = self::isNewInventory($dataname);
		if ($newInventory) {
			self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'add', $location_id);
		}

		try{

			$ioid_result = CQ::getRowsWithRequiredColumns('orders', array("ioid"), array("order_id" => $order_id));

			// inserting cc_transactions data
			if($lavu_pay_type=='GC'){
				if(!empty($discount_amount) && $discount_amount > 0){
					$cc_vars = array();
					$cc_vars['amount'] = self::saveFormatedCurrency($discount_amount);
					$cc_vars['card_type'] = "LAVU GIFT";
					$cc_vars['currency_code'] = "";
					$cc_vars['datetime'] = $local_datetime;
					$cc_vars['ioid']    = $ioid_result[0]['ioid'];
					$cc_vars['last_mod_ts']  = time();
					$cc_vars['loc_id']   = $location_id;
					$cc_vars['order_id']   = $order_id;
					$cc_vars['pay_type_id']  = 3;
					$cc_vars['pay_type']   = 'LAVU GIFT';
					$cc_vars['tip_amount']  = self::saveFormatedCurrency($tipAmount);
					$cc_vars['total_collected'] = self::saveFormatedCurrency($discount_amount);
					$cc_vars['transaction_id'] = "";
					$cc_vars['voided']   = 0;
					$orderUpdate['alt_paid'] = self::saveFormatedCurrency($discount_amount);
					$orderUpdate['total'] = self::saveFormatedCurrency($discount_amount);
				}
			}

			if($lavu_pay_type=='') { // no discount types or gift card
				$order_total = $order_total;
			}

			$order_info = json_decode($order_arr[0]['order_information'],true);
			$taxinform = OrdersDataController::displaytax($order_info['order_contents_information'],$discnt_p);
			$get_totals = OrdersDataController::caltotal($taxinform);
			$newsubtotal = self::saveFormatedCurrency($get_totals['subtotal']);
			$order_total = self::saveFormatedCurrency($get_totals['grand_total'] + $delivery_fee);
			$roundingAdjustment = CQ::roundingAdjustment($order_total, $dataname, $location_id);
			$order_total = $roundingAdjustment['amount'];
			$roundingAmount = $roundingAdjustment['roundingAmount'];

			if($order_total > 0){
				$cc_vars = array();
				$res = self::getDecimalData(1, $dataname);
				self::$decimal_char = $res[0]['decimal_char'];
				self::$disable_decimal = $res[0]['disable_decimal'];
				self::$rounding_factor = $res[0]['rounding_factor'];
				self::$round_up_or_down = $res[0]['round_up_or_down'];
				$cc_vars['amount'] = self::saveFormatedCurrency($order_total);
				$cc_vars['card_type'] = "";
				$cc_vars['currency_code'] = "";
				$cc_vars['datetime'] = $local_datetime;
				$cc_vars['ioid']    = $ioid_result[0]['ioid'];
				$cc_vars['last_mod_ts']  = time();
				$cc_vars['loc_id']   = $location_id;
				$cc_vars['order_id']   = $order_id;
				$cc_vars['pay_type']   = 'Cash';
				$cc_vars['pay_type_id']  = 1;
				$cc_vars['tip_amount']  = self::saveFormatedCurrency($tipAmount);
				$cc_vars['total_collected'] = self::saveFormatedCurrency($order_total);
				$cc_vars['transaction_id'] = "";
				$cc_vars['voided']   = 0;
				$orderUpdate['total'] = self::saveFormatedCurrency($order_total);
				$orderUpdate['cash_paid'] = self::saveFormatedCurrency($order_total);
				$orderUpdate['cash_applied'] = self::saveFormatedCurrency($order_total);
			}
			if (!empty($dining_time)) {
				$togotime = $dining_date.' '.$dining_time;
			} else{
				$togotime = date('Y-m-d h:i A', strtotime($dining_date));
			}
			$orderUpdate['togo_time'] = $togotime;
			$orderUpdate['void'] = 0;
			$orderUpdate['subtotal'] = $newsubtotal;
			$orderUpdate['rounding_amount'] = $roundingAmount;
			$orderUpdate['tax'] = self::saveFormatedCurrency($get_totals['calculatedtaxamount']);
			$orderUpdate['cash_tip'] = self::saveFormatedCurrency($tipAmount);
			$orderUpdate['togo_phone'] = $togo_phone;
			$orderUpdate['togo_name'] = $togo_name;
			$orderUpdate['togo_type'] = 'LTG';
			$orderUpdate['email'] = $togo_email;
			$orderUpdate['togo_delivery_fees'] = $delivery_fee;
			$orderUpdate['tablename'] = "Lavu To Go";
			$orderUpdate['special_instructions'] = $special_instructions;
			$orderUpdate['delivery_address'] = $customer_address;
			$orderUpdate['tabname'] =  "Lavu To Go - ".$first_name." ".$last_name;
			$orderUpdate['last_modified'] = date("Y-m-d H:i:s");
			$orderUpdate['togo_status'] = $togo_status;
			$orderUpdate['original_id'] = self::generateOriginalId($args);

			// updating order table
			CQ::updateArray ( 'orders', $orderUpdate, "where `order_id` = '" . $order_id . "' and `void` = '3' and `order_status` = 'open' " );
			$orderUpdateDetails = OrdersQueries::updateOrderInformation($order_id, [
				'rounding_amount' => $roundingAmount,
				'total' => $orderUpdate['total'],
				'subtotal' => $orderUpdate['subtotal'],
				'tax' =>  $orderUpdate['tax'],
				'taxdetails' => $taxinform
			], [
				"amount" => $orderUpdate['total']
			]);
			$order_result = CQ::getRowsWhereColumn('orders', array("order_id" => $order_id));
			$split_vars = array(
					"discount_sh" => $order_result[0]['discount_sh'],
					"alt_paid" => self::saveFormatedCurrency($order_result[0]['alt_paid']),
					"subtotal" => self::saveFormatedCurrency($order_result[0]['subtotal']),
					"check_total" => self::saveFormatedCurrency($order_result[0]['total']),
					"ioid" => $order_result[0]['ioid'],
					"itax" => $order_result[0]['itax'],
					"email" => $togo_email,
					"discount_id" => $order_result[0]['discount_id'],
					"tax" => $order_result[0]['tax'],
					"tax_info" => $order_result[0]['taxrate'],
					"discount_value" => $discount_details['discounttypes'][0]['discount'],
					"rounding_amount" => $roundingAmount,
					"discount" => $order_result[0]['discount'],
					"discount_type" => $order_result[0]['discount_type'],
					"loc_id" => $location_id
			);

			//updating split check details
			$this->splitCheckUpdate($order_id, $split_vars);


			//updating address details in lavutogo orders table
			$this->UpdateOrderAdrressDetails($dataname,$locationOrderUpdate, $order_id, $location_info);

			 if($lavu_pay_type=='LP' || $lavu_pay_type=='GC'){
				 $this->changeLoyaltyPoints($loyaltyDetails);
			 }

			// action logs
			$l_vars = array(
					'action'		=> "Order Paid",
					'details'		=> "Total: ".$order_total,
					'loc_id'		=> $location_id,
					'order_id'		=> $order_id,
					'time'			=> time(),
					'server_time'	=> date("Y-m-d H:i:s"),
					'user'			=> "Lavu To Go",
					'user_id'		=> 0,
					'device_udid'	=> ""
			);

			if ($newInventory) {
				self::updateInventoryQuantity($dataname, $args, $type = 'inventory', $action = 'add', $location_id);
			}
			//updateing menu item inventory track details.
			self::updateMenuitemCountInventory($orderItemsDetails);//LP-5042

			// inserting order action in action_log table
			CQ::insertArray('action_log', array_keys($l_vars), array(array_values($l_vars)));
		}
		catch (Exception $ex){
			$msg = 'Your transaction has been failed due to internal server error for Order: '.$order_id;
			//error_log($msg);
			if ($newInventory) {
				self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
			}
			CQ::updateArray ( 'orders', array("void" => 1,"void_reason" => $msg), "where `order_id` = '" . $order_id . "' ");
			return array('status'=>"F",'msg'=>$msg);
		}

		$orderinformationDetails = json_decode($orderUpdateDetails['order_information'], true);
		// START EMAIL ON COD @email
		$emailto = $email;
		$messagearr = array ();
		$customer = 'Customer';
		// $togo_name contains the user first name and second name
		if ($togo_name != '') {
			$customer = $togo_name;
		}
		$messagearr ['customer'] = $customer;
		$messagearr ['order_id'] = $order_id;
		$messagearr ['subtotalamount'] = $orderUpdate ['subtotal'];
		$messagearr ['totalamount'] = PaymentDataController::displayFormatedCurrency(self::saveFormatedCurrency($orderUpdate['total'] + $tipAmount));
		$messagearr ['taxamount'] = $orderUpdate ['tax'];
		$messagearr ['monitary_symbol'] = $location_info ['monitary_symbol'];
		$messagearr ['left_or_right'] = $location_info ['left_or_right'];
		$messagearr ['store_title'] = $location_info ['title'];
		$messagearr ['taxinform'] = $taxinform;
		$messagearr['store_image'] = $location_info['title'];
		$messagearr['dataname'] = $dataname;
		$messagearr['delivery_instructions'] =$special_instructions;

		if ($location_info ['left_or_right'] == 'left') {
			$mon_left_sym = $location_info ['monitary_symbol'] . " ";
			$mon_right_sym = "";
		} else {
			$mon_right_sym = " " . $location_info ['monitary_symbol'];
			$mon_left_sym = "";
		}
		$messagearr ['orderItems'] = $orderinformationDetails ['order_contents_information'];
		$messagearr ['restaurant_address'] = $location_info['address'];
		$messagearr ['restaurant_contact_number'] = $location_info['phone'];
		$messagearr ['rounding_amount'] = $roundingAmount;
		PaymentDataController::sendMail ( $emailto, $messagearr );
		// END EMAIL ON COD

		return array('status'=>'S', 'msg'=> apiSpeak($languageid, $this->onSuccessMessage), "order_id" => $order_id);
	}

	private function updateDiscountForOrderContents($order_id, $orderUpdate){
		$order_items = CQ::getRowsWhereColumn('order_contents', array("order_id" => $order_id));
		foreach($order_items as $item){

			$subtotal = $item['subtotal_with_mods'];
			$update_item = array();
			$update_item['discount_id'] = $orderUpdate['discount_id'];
			$update_item['discount_type'] = $orderUpdate['discount_type'];
			$update_item['discount_value'] = $orderUpdate['discount'];
			if($orderUpdate['discount_type'] == "d"){
				$update_item['discount_amount'] = abs($subtotal - $orderUpdate['discount']);
			}
			else if($orderUpdate['discount_type'] == "p"){
				$update_item['discount_amount'] = ($subtotal * floatval($orderUpdate['discount']));
			}

			$update_item['after_discount'] = $subtotal - $update_item['discount_amount'];
			$update_item['total_with_tax'] = $subtotal + $item['tax_amount'];

			//print_r($update_item);
			// updating order discount
			CQ::updateArray ( 'order_contents', $update_item, "where `id` = '" . $item['id'] . "' " );
		}
	}

	protected function action_initiateWalletPayment($args){
		$dataname = isset($args['dataname'])?$args['dataname']:'';
		$order_id = isset($args['order_id'])?$args['order_id']:'';
		$orderItemsDetails = isset($args['orderItemsDetails'])?$args['orderItemsDetails']:'';
		$payment_method_nonce = isset($args['payment_method_nonce'])?$args['payment_method_nonce']:'';
		$user_id = isset($args['user_id'])?$args['user_id']:0;
		$order_total = isset($args['payable_amount'])?$args['payable_amount']:'';
		$order_type = isset($args['order_type'])?$args['order_type']:'';
		$order_type = str_replace('-','',strtolower($order_type));
		$togo_email = $email = isset($args['email_id'])?$args['email_id']:'';
		$togo_phone = $phoneNumber = isset($args['phone_number'])?$args['phone_number']:'';
		$first_name = isset($args['first_name'])?$args['first_name']:'';
		$last_name = isset($args['last_name'])?$args['last_name']:'';
		$togo_name = $first_name." ".$last_name;
		$customer_address = isset($args['customer_address'])?$args['customer_address']:'';
		$lavu_pay_type = isset($args['lavu_pay_type'])?$args['lavu_pay_type']:'';
		$lavu_card_number = isset($args['lavu_card_number'])?$args['lavu_card_number']:'';
		$discount_amount = isset($args['lavu_pay_amount'])?$args['lavu_pay_amount']:'';
		$discountId = isset($args['discount_id'])?$args['discount_id']:'';
		$tip_type = isset($args['tip_type'])?$args['tip_type']:'';
		$tipAmount = isset($args['tip_amount'])?$args['tip_amount']:0;
		$delivery_fee = isset($args['delivery_fee'])?$args['delivery_fee']:0;
		$dining_date = $args['dining_date'];
		$dining_time = $args['dining_time'];
		$special_instructions = isset($args['delivery_instructions'])?trim($args['delivery_instructions']):'';
		$location_id = isset($args['location_id']) ? $args['location_id'] : 1;
		$togo_status= $this->toGetOrderTypeIdBaseOnOrderType($args['order_type']);
		$payment = array();
		$error = array();
		$orderUpdate = array();

		if(empty($order_id))
		{
			$error[] = "Order Id empty";
		}

		if(empty($payment_method_nonce)){
			$error[] = "Payment Menthod Nonce empty";
		}

		if(empty($dataname))
		{
			$error[] = "Dataname empty";
		}

		if(empty($orderItemsDetails))
		{
			$error[] = "Order Items Details are empty";
		}

		if(empty($order_total))
		{
			$error[] = "Order Amount empty";
		}

		if(empty($order_type))
		{
			$error[] = "Order Type empty";
		}

		if(empty($email))
		{
			$error[] = "Email ID empty";
		}

		if(empty($phoneNumber))
		{
			$error[] = "Phone Number empty";
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$error[] = "Email ID not valid";
		}

		if(empty($first_name))
		{
			$error[] = "First Name empty";
		}

		if(empty($last_name))
		{
			$error[] = "Last Name empty";
		}

		if (!in_array($order_type, array(DELIVERY, PICKUP, DINE_IN))) {
			$error[] = "Order Type not valid";
		}

		if ($order_type == DELIVERY) {
			if(!isset($customer_address) || empty($customer_address))
			{
				$error[] = "Customer Address empty";
			}
		}
		if($delivery_fee < 0){
			$error[] = "Invalid delivery fee";
		}

		$order_arr = OrdersQueries::checkOrder($order_id);

		if(!isset($order_arr[0]['order_id']) && empty($order_arr[0]['order_id'])){
			$error[] = "Wrong order Id";
		}
		if(isset($order_arr[0]['transaction_information']) && !empty($order_arr[0]['transaction_information'])){
			$transaction_information = json_decode($order_arr[0]['transaction_information'],1);
			if($transaction_information['approved'] == 1){
				$error[] = "Order is closed";
			}
		}
		/*
		*inventory checking
		*/
		$paramsInvCheck = array('order_id'=>$order_id);
		$returnInvCheck = $this->inventoryCheckWhenCheckout($paramsInvCheck);
		if($returnInvCheck!=''){
			return $returnInvCheck;
		}
		// updating order before payment
		$order_result = OrdersDataController::orderUpdate($orderItemsDetails, $order_id, $dataname, $location_id);
		//print_r($order_arr);exit;
		if($order_result['status'] == "F"){
			return array("status" => "F", "details" => $order_result['result'], "msg" => "Some items are out of stock");
		}

		if(!empty($lavu_pay_type)){

			if($lavu_pay_type != "LP" && $lavu_pay_type != "GC"){
				$error[] = "Invalid Pay Type";
			}
			else{
				if(empty($args['lavu_card_number'])){ // validating lavu gift card number
					$error[] = "Gift Card Empty";
				}

				if($lavu_pay_type == "LP"){ // Loyalty Points
					if($args['lavu_add_points'] == 1){ // award points
						if($discount_amount <= 0){
							$error[] = "Invalid award points";
						}
						else{
							$lavu_award_points_details = $this->action_checkAwardPoints(array("lavu_card" => $lavu_card_number, "dataname" => $dataname, "orderId" => $order_id));
							//print_r($lavu_award_points_details);exit;
							if($lavu_award_points_details['status'] == "F"){
								$error[] = $lavu_award_points_details['error'];
							}
							else if($lavu_award_points_details['status'] == "1")
							{
								if($lavu_award_points_details['awardpoints'] > 0){
									if((float)$lavu_award_points_details['awardpoints'] != (float)$discount_amount){
										$error[] = "Invalid Award points";
									}
									else{
										$lavu_award_points = $lavu_award_points_details['awardpoints'];
									}
								}
							}
						}
					}
					else if($args['lavu_add_points'] == 0){ // discount points
						if(empty($discountId))
						{
							$error[] = "Loyalty Points Discount Id empty";
						}
						else{
							if($discount_amount <= 0){
								$error[] = "Invalid Discount Amount";
							}
							else{
								$discount_details = $this->action_checkLoyaltyDiscountPoints(array("lavu_card" => $lavu_card_number, "dataname" => $dataname, "orderId" => $order_id, "discountId" => $discountId));
								//print_r($discount_details);
								if($discount_details['status'] == "F"){
									$error[] = $discount_details['error'];
								}
								else if($discount_details['status'] == "1")
								{
									if(empty($discount_details['discounttypes'])){
										$error[] = "Invalid Discount Id";
									}
									else if((float)trim($discount_details['discounttypes'][0]['discount_value']) != (float)$discount_amount){
										$error[] = "Invalid Discount value";
									}
								}
							}
						}
					}
				}
				else if($lavu_pay_type == "GC"){ // gift card
					if($discount_amount <= 0){
						$error[] = "Invalid Lavu Gift Card Amount";
					}
					else{
						$GiftCardDetails = $this->action_getLavuGiftCardDetails(array('lavu_card' => $lavu_card_number,'dataname' => $dataname));
						//print_r($GiftCardDetails);
						if($GiftCardDetails['status'] == "F"){
							$error[] = $GiftCardDetails['error'];
						}
						else if($GiftCardDetails['status'] == "1")
						{
							if($GiftCardDetails['lavu_gift_card_balance'] <= 0){
								$error[] = "Insufficient Lavu Gift Card Amount";
							}
							else if($GiftCardDetails['lavu_gift_card_balance'] != $discount_amount){
								$error[] = "Invalid Lavu Gift Card Amount";
							}
						}
					}
				}
			}
		}

		if(!empty($tip_type)){ // Tip Validation
			if($tip_type != "C" && $tip_type != "P"){
				$error[] = "Invalid Tip Type";
			}
			else{
				if($tipAmount <= 0){
					$error[] = "Invalid Tip Amount";
				}
			}
		}


		$ltg_minimum_order_amount = 0;


		if(!empty($error)){
			return array("status"=>"F", "msg"=>$error);
		}

		$order_arr = OrdersQueries::checkOrder($order_id);

		//print_r($order_arr);exit;
		$o_rowsResult = CQ::getRowsWithRequiredColumns('orders', array("subtotal","total"), array("order_id" => $order_id));
		if($discountId =='')
		{
			$orderAmount= $order_arr[0]['amount'];
		}
		else {
			$orderAmount= $o_rowsResult[0]['total'];
		}


		$orderAmount= $order_arr[0]['amount'];//actual order amount

		$isLavuAddPoints = (int) $args['lavu_add_points'];//add lavu award points if this value is having 1 or for 0 deduct the available balance from card


		$tipAmount = self::saveFormatedCurrency($tipAmount);
		$left_total = trim($orderAmount+$tipAmount) - ($isLavuAddPoints == 1 ? 0 : $discount_amount);
		$right_total = trim($order_arr[0]['amount'] - ($isLavuAddPoints == 1 ? 0 : $discount_amount));
		$right_total = $right_total + $tipAmount;

		if(trim($left_total) != trim($right_total)){
			return array('status'=>"F",'msg'=> 'Order amount is not valid.');
		}

		if($isLavuAddPoints == 0 && ($lavu_pay_type == "GC" || $lavu_pay_type == "LP")){
			if($lavu_pay_type == "GC"){
				$lavu_gc_card_amt = $discount_amount;
			}
			/*if($tmp_order_total <= 0){
				$order_total = $order_total + $discount_amount;
				$discount_amount = $order_total;
			}
			else{
				$order_total = $order_total + $discount_amount;
			}*/
		}

		$rowsResult = CQ::getRowsWithRequiredColumns('config', array("value"), array("setting" => "ltg_minimum_order_price"));

		if(!empty($rowsResult)){
			$ltg_minimum_order_amount = $rowsResult[0]['value'];
		}


		if($isLavuAddPoints == 0 && ($lavu_pay_type == "GC")){
			if($order_total > $lavu_gc_card_amt){
				//$order_total = $order_total - $lavu_gc_card_amt;
				$discount_amount = $lavu_gc_card_amt;
			}
			else{
				$discount_amount = $order_total;
				//$order_total = 0;
			}
		}

		if($lavu_pay_type!=''){
			if($lavu_pay_type=='LP'){

				if($isLavuAddPoints == 1){
					$loyaltyDetails['loyalty_type'] = 'a';
					$loyaltyDetails['awardpoints'] = $discount_amount;
				}
				else if($isLavuAddPoints == 0){
					if(!empty($discount_details['discounttypes'])){
						$loyaltyDetails['loyalty_type'] = ($discount_details['discounttypes'][0]['loyalty_use_or_unlock']==1)?'d':'';
						$loyaltyDetails['discount_points'] = $discount_details['discounttypes'][0]['discount_points'];

						if($discount_details['discounttypes'][0]['discount_value']>=$orderAmount){
							$payableAmount = $discount_details['discounttypes'][0]['discount_value']-$orderAmount;
						}
						if($discount_details['discounttypes'][0]['discount_value']<=$orderAmount){
							$payableAmount = $orderAmount-$discount_details['discounttypes'][0]['discount_value'];
						}
						$orderUpdate['discount_id'] = $discount_details['discounttypes'][0]['discount_id'];
						$orderUpdate['discount'] = $discount_details['discounttypes'][0]['discount'];
						$orderUpdate['discount_value'] = $discount_amount;
						$orderUpdate['discount_sh'] = $discount_details['discounttypes'][0]['title'];
						$orderUpdate['discount_type'] = $discount_details['discounttypes'][0]['discount_type'];
					}
				}
			}
			else if($lavu_pay_type=='GC'){
				$loyaltyDetails['loyalty_type'] = 'GC';
				$loyaltyDetails['discount_amount'] = $discount_amount;
			}

			$loyaltyDetails['cardnumber'] =  $lavu_card_number;
			$loyaltyDetails['dataname'] =  $dataname;

		}

		//LP-5034
		$discountdata = array();
		if(!empty($discount_details['discounttypes'][0]))
		    $discountdata = $discount_details['discounttypes'][0];

		if(empty($dining_time))
		    	$dining_time = date("h:i A");

		$locationOrderUpdate['phone_number'] = $phoneNumber;
		$locationOrderUpdate['email_id'] = $email;
		$locationOrderUpdate['first_name'] = $first_name;
		$locationOrderUpdate['last_name'] = $last_name;
		$locationOrderUpdate['delivery_instructions'] = $args['delivery_instructions'];
		$locationOrderUpdate['customer_address'] = $customer_address;
		$locationOrderUpdate['order_type'] = $order_type;
		$locationOrderUpdate['delivery_fee'] = $delivery_fee;
		$locationOrderUpdate['dining_time'] = $dining_time;
		$locationOrderUpdate['dining_date'] = $dining_date;
		$locationOrderUpdate['tip_amount'] = $tipAmount;
		$locationOrderUpdate['discount_info'] = $discountdata;//LP-5034
		$locationOrderUpdate['user_id'] = $user_id;
		$local_datetime = date("Y-m-d H:i:s");
		$locationOrderUpdate['user_id'] = $user_id;
		$ioid_result = CQ::getRowsWithRequiredColumns('orders', array("ioid"), array("order_id" => $order_id));

		if($lavu_pay_type==''){ // no discount types or gift card
			$order_total = $order_total;
		}
		//error_log("Order Total: ".$order_total);

		$newInventory = self::isNewInventory($dataname);

		if ($newInventory) {
			self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'add', $location_id);
		}

		try{
			if($order_total >= 1){// min $1 for paypal

				$sale = array(
						"amount" => $order_total,
						"paymentMethodNonce" => $payment_method_nonce,
						"orderId" => $order_id,
						"options" => [
								"paypal" => [
										// "customField" => $args["PayPal custom field"],
										// "description" => $args["Description for PayPal email receipt"],
								],
						],
						'channel'  => self::$lavu_channel
				);

				$result = !$this->braintreeConnect->hasError ? $this->braintreeConnect->doSale($sale) : [
					"status" => false,
					"message" => $this->braintreeConnect->errorMessage
				];

				//To check in local uncomment below lines and comment above line
				//$result['success'] = true;
				//$result['transaction']= array("id"=>"upendar","amount"=>$sale['amount'],'currencyIsoCode'=>'USD');
			}
			else{
				if ($newInventory) {
					self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
				}
				return array('status'=>"F",'msg'=>'To process a payment with PayPal, the minimum order amount is $1.');
			}

		}
		catch(Exception $ex){
			if ($newInventory) {
				self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
			}
			return array('status'=>"F",'msg'=>'Paypal auth failed at server '.$ex);
		}
		$apiResponse = "";
		$result = json_decode(json_encode($result),true);
		if(!empty($result)){
			if($result['success']===true){
				$apiResponse =  $result['transaction'];
			}
			else if(isset($result['errors']) && $result['message'] != ''){
				if ($newInventory) {
					self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
				}
				$msg = 'Your Order is failed due to '.$result['message'];
				CQ::updateArray ( 'orders', array("void" => 1,"void_reason" => $msg), "where `order_id` = '" . $order_id . "' ");
				return array('status'=>"F",'msg'=>$msg);
			}
		}
		$local_datetime = date("Y-m-d H:i:s");


		try{

			$ioid_result = CQ::getRowsWithRequiredColumns('orders', array("ioid"), array("order_id" => $order_id));

			// inserting cc_transactions data
			if($lavu_pay_type=='GC'){
				if(!empty($discount_amount) && $discount_amount > 0){
					//$pay_type_result = CQ::getRowsWithRequiredColumns('payment_types', array("id"), array("type" => "LAVU GIFT", "_deleted" => 0));

					$cc_vars = array();
					$cc_vars['amount'] = $discount_amount;
					$cc_vars['card_type'] = "LAVU GIFT";
					$cc_vars['currency_code'] = "";
					$cc_vars['datetime'] = $local_datetime;
					$cc_vars['ioid']    = $ioid_result[0]['ioid'];
					$cc_vars['last_mod_ts']  = time();
					$cc_vars['loc_id']   = $location_id;
					$cc_vars['order_id']   = $order_id;
					$cc_vars['pay_type']   = 'LAVU GIFT';
					$cc_vars['pay_type_id']  = 3;
					$cc_vars['tip_amount']  = $tipAmount;
					$cc_vars['total_collected'] = $discount_amount;
					$cc_vars['transaction_id'] = "";
					$cc_vars['voided']   = 0;

					//print_r($cc_vars);exit();
					$this->saveTransaction($order_id, $cc_vars);

					$orderUpdate['alt_paid'] = $discount_amount;
					$orderUpdate['total'] = $discount_amount;
				}
			}

			if(isset($result['success']) && !empty($apiResponse)){
				//$pay_type_result = CQ::getRowsWithRequiredColumns('payment_types', array("id"), array("type" => "Cash", "_deleted" => 0));

				$cc_vars = array();
				$cc_vars['amount'] = $apiResponse['amount'];
				//$cc_vars['card_type'] = $apiResponse['creditCard']['cardType'];
				$cc_vars['currency_code'] = $apiResponse['currencyIsoCode'];
				$cc_vars['datetime'] = $local_datetime;
				$cc_vars['ioid']    = $ioid_result[0]['ioid'];
				$cc_vars['last_mod_ts']  = time();
				$cc_vars['loc_id']   = $location_id;
				$cc_vars['order_id']   = $order_id;
				$cc_vars['pay_type']   = 'Paypal';
				$cc_vars['pay_type_id']  = "";  // TO DO : query for pay_type ID for PayPal
				$cc_vars['tip_amount']  = $tipAmount;
				$cc_vars['total_collected'] = $order_total;
				$cc_vars['transaction_id'] = $apiResponse['id'];
				$cc_vars['voided']   = 0;

				//print_r($cc_vars);exit();
				$this->saveTransaction($order_id, $cc_vars);

				$orderUpdate['total'] = (isset($orderUpdate['total'])?$orderUpdate['total']:0) + $order_total;

				if ($newInventory) {
					self::updateInventoryQuantity($dataname, $args, $type = 'inventory', $action = 'add', $location_id);
				}
			}

			//$orderUpdate['order_status'] = "closed";
			$orderUpdate['void'] = 0;
			//$orderUpdate['closed'] = $local_datetime;
			$orderUpdate['cash_tip'] = $tipAmount;
			$orderUpdate['togo_phone'] = $togo_phone;
			$orderUpdate['togo_name'] = $togo_name;
			$orderUpdate['togo_type'] = 'LTG';
			$orderUpdate['email'] = $togo_email;
			$orderUpdate['togo_delivery_fees'] = $delivery_fee;
			$orderUpdate['tablename'] = "Lavu To Go PAID";
			$orderUpdate['special_instructions'] = $special_instructions;
			$orderUpdate['togo_status']=$togo_status;
			$orderUpdate['delivery_address'] = $customer_address;
			$orderUpdate['last_modified'] = date("Y-m-d H:i:s");
			$orderUpdate['original_id'] = self::generateOriginalId($args);
			$roundingAmount = isset($args['rounding_amount']) && $args['rounding_amount'] !== 0 ? $args['rounding_amount'] : '0.00';
			$orderUpdate['rounding_amount'] = $roundingAmount;
			// updating order table
			CQ::updateArray ( 'orders', $orderUpdate, "where `order_id` = '" . $order_id . "' and `void` = '3' and `order_status` = 'open' " );

			$order_result = CQ::getRowsWhereColumn('orders', array("order_id" => $order_id));
			$split_vars = array(
					"discount_sh" => $order_result[0]['discount_sh'],
					"alt_paid" => $order_result[0]['alt_paid'],
					"cash_applied" => $order_total,
					"subtotal" => $order_result[0]['subtotal'],
					"check_total" => $order_result[0]['total'],
					"ioid" => $order_result[0]['ioid'],
					"itax" => $order_result[0]['itax'],
					"email" => $togo_email,
					"discount_id" => $order_result[0]['discount_id'],
					"tax" => $order_result[0]['tax'],
					"cash" => $order_total,
					"tax_info" => $order_result[0]['taxrate'],
					"discount_value" => $order_result[0]['discount_value'],
					"rounding_amount" => $roundingAmount,
					"discount" => $order_result[0]['discount'],
					"discount_type" => $order_result[0]['discount_type'],
					"loc_id" => $location_id
			);

			//updating split check details
			$this->splitCheckUpdate($order_id, $split_vars);

			if(isset($orderUpdate['discount_id'])){
				// update discounts for order contents
				$this->updateDiscountForOrderContents($order_id, $orderUpdate);
			}

			if($lavu_pay_type=='LP' || $lavu_pay_type=='GC'){
				$this->changeLoyaltyPoints($loyaltyDetails);
			}

			// action logs

			$l_vars = array(
					'action'		=> "Order Paid",
					'details'		=> "Total: ".$order_total,
					'loc_id'		=> $location_id,
					'order_id'		=> $order_id,
					'time'			=> time(),
					'server_time'	=> date("Y-m-d H:i:s"),
					'user'			=> "Lavu To Go",
					'user_id'		=> 0,
					'device_udid'	=> ""
			);

			// inserting order action in action_log table
			CQ::insertArray('action_log', array_keys($l_vars), array(array_values($l_vars)));
		}
		catch (Exception $ex){
			if ($newInventory) {
				self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
			}
			$msg = 'Your transaction has been failed due to internal server error for Order: '.$order_id;
			CQ::updateArray ( 'orders', array("void" => 1,"void_reason" => $msg), "where `order_id` = '" . $order_id . "' ");
			return array('status'=>"F",'msg'=>$msg);
		}

		// START EMAIL ON walletpayment @email
		$location_info = LocationDataController::action_getLocationInfo(1, $dataname);
		$emailto = $email;
		$messagearr = array ();
		$customer = 'Customer';
		// $togo_name contains the user first name and second name
		if ($togo_name != '') {
			$customer = $togo_name;
		}
		//updating address details in lavutogo orders table
		$this->UpdateOrderAdrressDetails($dataname,$locationOrderUpdate, $order_id, $location_info);
		$messagearr ['customer'] = $customer;
		$messagearr ['order_id'] = $order_id;
		$messagearr ['subtotalamount'] = $orderUpdate ['subtotal'];
		$messagearr ['totalamount'] = PaymentDataController::displayFormatedCurrency(self::saveFormatedCurrency($orderUpdate['total'] + $tipAmount));
		$messagearr ['taxamount'] = $orderUpdate ['tax'];
		$messagearr ['monitary_symbol'] = $location_info ['monitary_symbol'];
		$messagearr ['left_or_right'] = $location_info ['left_or_right'];
		$messagearr ['store_title'] = $location_info ['title'];
		$messagearr ['taxinform'] = $taxinform;
		$messagearr['store_image'] = $location_info['title'];
		$messagearr['dataname'] = $dataname;
		$messagearr['delivery_instructions'] = $special_instructions;

		if ($location_info ['left_or_right'] == 'left') {
			$mon_left_sym = $location_info ['monitary_symbol'] . " ";
			$mon_right_sym = "";
		} else {
			$mon_right_sym = " " . $location_info ['monitary_symbol'];
			$mon_left_sym = "";
		}
		$messagearr ['orderItems'] = $new_order_info ['order_contents_information'];
		$messagearr ['restaurant_address'] = $location_info['address'];
		$messagearr ['restaurant_contact_number'] = $location_info['phone'];
		$messagearr ['rounding_amount'] = $roundingAmount;
		PaymentDataController::sendMail ( $emailto, $messagearr );
		// END EMAIL ON Wallet payment
		return array('status'=>'S', 'msg'=> $this->onSuccessMessage, "order_id" => $order_id);

	}


	protected function action_initiateCardPayment($args){
	    $languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;
		$dataname = isset($args['dataname'])?$args['dataname']:'';
		$order_id = isset($args['order_id'])?$args['order_id']:'';
		$orderItemsDetails = isset($args['orderItemsDetails'])?$args['orderItemsDetails']:'';
		$payment_method_nonce = isset($args['payment_method_nonce'])?$args['payment_method_nonce']:'';
		$user_id = isset($args['user_id'])?$args['user_id']:0;
		$order_total = isset($args['payable_amount'])?$args['payable_amount']:'';
		$order_type = isset($args['order_type'])?$args['order_type']:'';
		$order_type = str_replace('-','',strtolower($order_type));
		$togo_email = $email = isset($args['email_id'])?$args['email_id']:'';
		$togo_phone = $phoneNumber = isset($args['phone_number'])?$args['phone_number']:'';
		$first_name = isset($args['first_name'])?$args['first_name']:'';
		$last_name = isset($args['last_name'])?$args['last_name']:'';
		$togo_name = $first_name." ".$last_name;
		$customer_address = isset($args['customer_address'])?$args['customer_address']:'';
		$lavu_pay_type = isset($args['lavu_pay_type'])?$args['lavu_pay_type']:'';
		$lavu_card_number = isset($args['lavu_card_number'])?$args['lavu_card_number']:'';
		$discount_amount = isset($args['lavu_pay_amount'])?$args['lavu_pay_amount']:'';
		$discountId = isset($args['discount_id'])?$args['discount_id']:'';
		$tip_type = isset($args['tip_type'])?$args['tip_type']:'';
		$tipAmount = isset($args['tip_amount'])?$args['tip_amount']:0;
		$delivery_fee = isset($args['delivery_fee'])?$args['delivery_fee']:0;
		$orderItemsDetails = $args['orderItemsDetails'];
		$dining_date = $args['dining_date'];
		$dining_time = $args['dining_time'];
		$closed_date = $args['closed_date'];
		$special_instructions = isset($args['delivery_instructions'])?trim($args['delivery_instructions']):'';
		$togo_status= $this->toGetOrderTypeIdBaseOnOrderType($args['order_type']);

		$location_id = $args['location_id'];
		$payment = array();
		$error = array();
		$orderUpdate = array();
		$ltg_minimum_order_amount = 0;

		// Update void to 0 assuming an error coming up!
		CQ::updateArray ( 'orders', array("void" => 3), "where `order_id` = '" . $order_id . "' ");

		$rowsResult = CQ::getRowsWithRequiredColumns('config', array("value"), array("setting" => "ltg_minimum_delivery_amount"));
		if (!empty($rowsResult)) {
			$ltg_minimum_delivery_amount = $rowsResult[0]['value'];
		}
		if ($order_type == 'delivery' && (($order_total - $delivery_fee) < $ltg_minimum_delivery_amount)) {
		    $error[] = apiSpeak($languageid, "Order Amount should be greater than Minimum Delivery Amount");
			return array("status"=>"F", "msg"=>$error);
		}

		if(empty($order_id))
		{
		    $error[] = apiSpeak($languageid, "Order Id is required");
		}

		if(empty($payment_method_nonce)){
		    $error[] = apiSpeak($languageid, "Payment Method Nonce is required");
		}

		if(empty($dataname))
		{
		    $error[] = apiSpeak($languageid, "Dataname is required");
		}

		if(empty($orderItemsDetails))
		{
		    $error[] = apiSpeak($languageid, "Order Items Details are required");
		}

		if(empty($order_total))
		{
		    $error[] = apiSpeak($languageid, "Order Amount is required");
		}

		if(empty($order_type))
		{
		    $error[] = apiSpeak($languageid, "Order Type is required");
		}

		if(empty($email))
		{
		    $error[] = apiSpeak($languageid, "Email Address is required");
		}

		if(empty($phoneNumber))
		{
		    $error[] = apiSpeak($languageid, "Telephone Number is required");
		}

		if(empty($first_name))
		{
		    $error[] = apiSpeak($languageid, "First Name is required");
		}

		if(empty($last_name))
		{
		    $error[] = apiSpeak($languageid, "Last Name is required");
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		    $error[] = apiSpeak($languageid, "Invalid Email Address");
		}

		if (!in_array($order_type, array(DELIVERY, PICKUP, DINE_IN))) {
		    $error[] = apiSpeak($languageid, "Invalid Order Type");
		}

		if ($order_type == DELIVERY) {
			if(!isset($customer_address) || empty($customer_address))
			{
			    $error[] = apiSpeak($languageid, "Customer Address is required");
			}
		}

		if($delivery_fee < 0){
		    $error[] = apiSpeak($languageid, "Invalid Delivery Fee");
		}

		$order_arr = OrdersQueries::checkOrder($order_id);
		if(!isset($order_arr[0]['order_id']) && empty($order_arr[0]['order_id'])){
		    $error[] = apiSpeak($languageid, "Invalid Order Id");
		}
		if(isset($order_arr[0]['transaction_information']) && !empty($order_arr[0]['transaction_information'])){
			$transaction_information = json_decode($order_arr[0]['transaction_information'],1);
			if($transaction_information['approved'] == 1){
			    $error[] = apiSpeak($languageid, "Order is closed");
			}
		}

		// Ensure we have a PayPal payment_type
		$payment_types = CQ::getRowsWithRequiredColumns('payment_types', array('id', 'type'), array("_deleted" => 0, "type" => 'PayPal'), '', '', $dataname);
		if (empty($payment_types)) {
			$payment_types = CQ::getRowsWithRequiredColumns('payment_types', array('id', 'type'), array("_deleted" => 0, "type" => 'Card'), '', '', $dataname);
		}
		$payment_type = array_shift($payment_types);

		/*
		*inventory checking
		*/
		//commented this code due to frontend code not yet ready
		$paramsInvCheck = array('order_id'=>$order_id, 'languageid'=>$languageid);
		$returnInvCheck = $this->inventoryCheckWhenCheckout($paramsInvCheck);
		if($returnInvCheck!=''){
			return $returnInvCheck;
		}
		// updating order before payment
		$order_result = OrdersDataController::orderUpdate($orderItemsDetails, $order_id, $dataname, $location_id);

		if($order_result['status'] == "F"){
		    return array("status" => "F", "details" => $order_result['result'], "msg" => apiSpeak($languageid, "Some items are out of stock"));
		}

		if(!empty($lavu_pay_type)){

			if($lavu_pay_type != "LP" && $lavu_pay_type != "GC"){
			    $error[] = apiSpeak($languageid, "Invalid Pay Type");
			}
			else{
				if(empty($args['lavu_card_number'])){ // validating lavu gift card number
				    $error[] = apiSpeak($languageid, "Lavu Card No is required");
				}

				if($lavu_pay_type == "LP"){ // Loyalty Points
					if($args['lavu_add_points'] == 1){ // award points
						if($discount_amount <= 0){
						    $error[] = apiSpeak($languageid, "Invalid Award Points");
						}
						else{
						    $lavu_award_points_details = $this->action_checkAwardPoints(array("lavu_card" => $lavu_card_number, "dataname" => $dataname, "orderId" => $order_id, "languageid" => $languageid));
							//print_r($lavu_award_points_details);exit;
							if($lavu_award_points_details['status'] == "F"){
								$error[] = $lavu_award_points_details['error'];
							}
							else if($lavu_award_points_details['status'] == "1")
							{
								if($lavu_award_points_details['awardpoints'] > 0){
									if((float)$lavu_award_points_details['awardpoints'] != (float)$discount_amount){
									    $error[] = apiSpeak($languageid, "Invalid Award Points");
									}
									else{
										$lavu_award_points = $lavu_award_points_details['awardpoints'];
									}
								}
							}
						}
					}
					else if($args['lavu_add_points'] == 0){ // discount points
						if(empty($discountId))
						{
						    $error[] = apiSpeak($languageid, "Loyalty Points Discount Id is required");
						}
						else{
							if($discount_amount <= 0){
							    $error[] = apiSpeak($languageid, "Invalid Discount Amount");
							}
							else{
							    $discount_details = $this->action_checkLoyaltyDiscountPoints(array("lavu_card" => $lavu_card_number, "dataname" => $dataname, "orderId" => $order_id, "discountId" => $discountId, "orderItemsDetails" => $orderItemsDetails, "languageid" => $languageid));
								//print_r($discount_details);
								if($discount_details['status'] == "F"){
									$error[] = $discount_details['error'];
								}
								else if($discount_details['status'] == "1")
								{
									if(empty($discount_details['discounttypes'])){
									    $error[] = apiSpeak($languageid, "Invalid Discount Id");
									}
									/*else if((float)$discount_details['discounttypes'][0]['discount_value'] != (float)$discount_amount){
										$error[] = "Invalid Discount value";
									}*/
								}
							}
						}
					}
				}
				else if($lavu_pay_type == "GC"){ // gift card
					if($discount_amount <= 0){
					    $error[] = apiSpeak($languageid, "Invalid Lavu Gift Card Amount");
					}
					else{
					    $GiftCardDetails = $this->action_getLavuGiftCardDetails(array('lavu_card' => $lavu_card_number,'dataname' => $dataname, "languageid" => $languageid));
						//print_r($GiftCardDetails);
						if($GiftCardDetails['status'] == "F"){
							$error[] = $GiftCardDetails['error'];
						}
						else if($GiftCardDetails['status'] == "1")
						{
							if($GiftCardDetails['lavu_gift_card_balance'] <= 0){
							    $error[] = apiSpeak($languageid, "Insufficient Lavu Gift Card Amount");
							}
							else if($GiftCardDetails['lavu_gift_card_balance'] != $discount_amount){
							    $error[] = apiSpeak($languageid, "Invalid Lavu Gift Card Amount");
							}
						}
					}
				}
			}
		}

		if(!empty($tip_type)){ // Tip Validation
			if($tip_type != "C" && $tip_type != "P"){
			    $error[] = apiSpeak($languageid, "Invalid Tip Type");
			}
			else{
				if($tipAmount <= 0){
				    $error[] = apiSpeak($languageid, "Invalid Tip Amount");
				}
			}
		}

		if(!empty($error)){
			return array("status"=>"F", "msg"=>$error);
		}



		$order_arr = OrdersQueries::checkOrder($order_id);
		$o_rowsResult = CQ::getRowsWithRequiredColumns('orders', array("subtotal", "total"), array("order_id" => $order_id));
		if($discountId =='')
		{
			$orderAmount= $order_arr[0]['amount'];
		}
		else {
			$orderAmount= $o_rowsResult[0]['total'];
		}

		$isLavuAddPoints = (int) $args['lavu_add_points'];//add lavu award points if this value is having 1 or for 0 deduct the available balance from card
		$tipAmount = self::saveFormatedCurrency($tipAmount);
		$left_total = trim($orderAmount+$tipAmount) - ($isLavuAddPoints == 1 ? 0 : $discount_amount);
		$right_total = trim($order_arr[0]['amount'] - ($isLavuAddPoints == 1 ? 0 : $discount_amount));
		$right_total = $right_total + $tipAmount;

		if(trim($left_total) != trim($right_total)){
			return array('status'=>"F",'msg'=> 'Order amount is not valid.');
		}

		$whereColumnDiscnt= array("id" => $discountId);
		$resultDiscnt = CQ::getRowsWhereColumn('discount_types', $whereColumnDiscnt, '', '', $dataname);
		$whereColumnOrder=array("order_id"=>$order_id);
		$columnNameOrder=array("subtotal","tax","total");
		$resOrder = CQ::getRowsWithRequiredColumns('orders',$columnNameOrder,$whereColumnOrder);
		$discnt_p = $resultDiscnt[0]['calc']*100;

		if($isLavuAddPoints == 0 &&  $lavu_pay_type == "LP"){
			if($lavu_pay_type == "GC"){
				$lavu_gc_card_amt = $discount_amount;
			}
		}

		$rowsResult = CQ::getRowsWithRequiredColumns('config', array("value"), array("setting" => "ltg_minimum_order_price"));

		if(!empty($rowsResult)){
			$ltg_minimum_order_amount = $rowsResult[0]['value'];
		}

		if ($isLavuAddPoints == 0 && $lavu_pay_type == "GC") {
			if ($order_total > $lavu_gc_card_amt) {
				$discount_amount = $lavu_gc_card_amt;
			} else {
				$discount_amount = $order_total;
			}
		}

		if ($lavu_pay_type!='') {
			if ($lavu_pay_type=='LP') {
					if ($isLavuAddPoints == 1) {
						$loyaltyDetails['loyalty_type'] = 'a';
						$loyaltyDetails['awardpoints'] = $discount_amount;
					} else if ($isLavuAddPoints == 0) {
						if(!empty($discount_details['discounttypes'])){
							$loyaltyDetails['loyalty_type'] = ($discount_details['discounttypes'][0]['loyalty_use_or_unlock']==1)?'d':'';
							$loyaltyDetails['discount_points'] = $discount_details['discounttypes'][0]['discount_points'];

							if($discount_details['discounttypes'][0]['discount_value']>=$orderAmount){
								$payableAmount = $discount_details['discounttypes'][0]['discount_value']-$orderAmount;
							}
							if($discount_details['discounttypes'][0]['discount_value']<=$orderAmount){
								$payableAmount = $orderAmount-$discount_details['discounttypes'][0]['discount_value'];
							}
							$orderUpdate['discount_id'] = $discount_details['discounttypes'][0]['discount_id'];
							$orderUpdate['discount'] = $discount_amount;
							$orderUpdate['discount_value'] = $discount_amount;
							$orderUpdate['discount_sh'] = $discount_details['discounttypes'][0]['title'];
							$orderUpdate['discount_type'] = $discount_details['discounttypes'][0]['discount_type'];
						}
					}
			} else if ($lavu_pay_type=='GC') {
				$loyaltyDetails['loyalty_type'] = 'GC';
				 $loyaltyDetails['discount_amount'] = $discount_amount;
			}

			$loyaltyDetails['cardnumber'] =  $lavu_card_number;
			$loyaltyDetails['dataname'] =  $dataname;

		}

		$discountdata = array();
		if (!empty($discount_details['discounttypes'][0])) {
		    $discountdata = $discount_details['discounttypes'][0];
		}

	    if (empty($dining_time)) {
	    	$dining_time = date("h:i A");
		}
		$locationOrderUpdate['phone_number'] = $phoneNumber;
		$locationOrderUpdate['email_id'] = $email;
		$locationOrderUpdate['first_name'] = $first_name;
		$locationOrderUpdate['last_name'] = $last_name;
		$locationOrderUpdate['delivery_instructions'] = $args['delivery_instructions'];
		$locationOrderUpdate['customer_address'] = $customer_address;
		$locationOrderUpdate['order_type'] = $order_type;
		$locationOrderUpdate['delivery_fee'] = $delivery_fee;
		$locationOrderUpdate['dining_time'] = $dining_time;
		$locationOrderUpdate['dining_date'] = $dining_date;
		$locationOrderUpdate['tip_amount'] = $tipAmount;
		$locationOrderUpdate['discount_info'] = $discountdata;//LP-5034
		$locationOrderUpdate['payment_type'] = 'card';
		$locationOrderUpdate['card_type'] = 'PayPal';
		$locationOrderUpdate['user_id'] = $user_id;
		$local_datetime = date("Y-m-d H:i:s");
		$locationOrderUpdate['user_id'] = $user_id;
		$ioid_result = CQ::getRowsWithRequiredColumns('orders', array("ioid"), array("order_id" => $order_id));

		if ($lavu_pay_type=='') { // no discount types or gift card
			$order_total = $order_total;
		}

		$newInventory = self::isNewInventory($dataname);

		if ($newInventory) {
			self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'add', $location_id);
		} try {
			if ($order_total > 1) {
				$sale = array(
					"amount" => $order_total,
					"paymentMethodNonce" => $payment_method_nonce,
					"orderId" => $order_id,
					"options" => [
						"paypal" => [
							// "customField" => $args["PayPal custom field"],
							// "description" => $args["Description for PayPal email receipt"],
						],
                        "submitForSettlement" => true
					],
					'channel'  => self::$lavu_channel
				);

				$result = !$this->braintreeConnect->hasError ? $this->braintreeConnect->doSale($sale) : [
					"status" => false,
					"message" => $this->braintreeConnect->errorMessage
				];
			} else {
				if ($newInventory) {
					self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
				}
				return array('status'=>"F",'msg'=>apiSpeak($languageid, 'To process a payment with PayPal, the minimum order amount is $1'));
			}
		} catch(Exception $ex) {
			if ($newInventory) {
				self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
			}
			// print_r($ex);
			return array('status'=>"F",'msg'=>apiSpeak($languageid, 'PayPal auth failed at server').' '.$ex);
		}
		$apiResponse = "";
		$result = json_decode(json_encode($result),true);
		if (!empty($result)) {
			if ($result['success']===true) {
				$apiResponse =  $result['transaction'];
			} else if(isset($result['errors']) && $result['message'] != '') {
				if ($newInventory) {
					self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
				}
				$msg = apiSpeak($languageid, 'Your payment failed due to').' '.$result['message'];
				CQ::updateArray ( 'orders', array("void" => 3,"void_reason" => $msg), "where `order_id` = '" . $order_id . "' ");
				return array('status'=>"F",'msg'=>$msg);
			}
		}

		$local_datetime = date("Y-m-d H:i:s");
		$order_info = json_decode($order_arr[0]['order_information'], true);
		$taxinform = OrdersDataController::displaytax($order_info['order_contents_information'], $discnt_p);
		$get_totals = OrdersDataController::caltotal($taxinform);
		$newsubtotal = self::saveFormatedCurrency($get_totals['subtotal']);
		$order_total = self::saveFormatedCurrency($get_totals['grand_total'] + $delivery_fee);
		$roundingAdjustment = CQ::roundingAdjustment($order_total, $dataname, $location_id);
		$order_total = $roundingAdjustment['amount'];
		$roundingAmount = $roundingAdjustment['roundingAmount'];

		try {
			$ioid_result = CQ::getRowsWithRequiredColumns('orders', array("ioid"), array("order_id" => $order_id));

			// inserting cc_transactions data
			if ($lavu_pay_type=='GC') {
				if(!empty($discount_amount) && $discount_amount > 0){
					$cc_vars = array();
					$cc_vars['amount'] = $order_total;
					$cc_vars['card_type'] = "LAVU GIFT";
					$cc_vars['currency_code'] = "";
					$cc_vars['datetime'] = $local_datetime;
					$cc_vars['ioid']    = $ioid_result[0]['ioid'];
					$cc_vars['last_mod_ts']  = time();
					$cc_vars['loc_id']   = $location_id;
					$cc_vars['order_id']   = $order_id;
					$cc_vars['pay_type']   = 'LAVU GIFT';
					$cc_vars['pay_type_id']  = 3;
					$cc_vars['tip_amount']  = self::saveFormatedCurrency($tipAmount);
					$cc_vars['total_collected'] = self::saveFormatedCurrency($discount_amount);
					$cc_vars['transaction_id'] = "";
					$cc_vars['voided']   = 0;
					$this->saveTransaction($order_id, $cc_vars);

					$orderUpdate['alt_paid'] = self::saveFormatedCurrency($order_total);
					$orderUpdate['total'] = self::saveFormatedCurrency($order_total);
				}
			}

			if (isset($result['success']) && !empty($apiResponse)) {
				$cc_vars = array();
				$cc_vars['amount'] = self::saveFormatedCurrency($order_total);
				if (isset($apiResponse['creditCard'])) {
					$card =	"creditCard";
					$cc_vars['card_desc'] = $apiResponse[$card]['last4'];
					$cc_vars['card_type'] = $apiResponse[$card]['cardType'];
					$cc_vars['card_holder_name'] = $apiResponse[$card]['cardholderName'];
				}
				$cc_vars['currency_code'] = $apiResponse['currencyIsoCode'];
				$cc_vars['datetime'] = $local_datetime;
				$cc_vars['ioid']    = $ioid_result[0]['ioid'];
				$cc_vars['last_mod_ts']  = time();
				$cc_vars['loc_id']   = $location_id;
				$cc_vars['order_id']   = $order_id;
				$cc_vars['pay_type']   = 'PayPal';
				$cc_vars['pay_type_id']  = $payment_type['id'];
				$cc_vars['tip_amount']  = self::saveFormatedCurrency($tipAmount);
				$cc_vars['total_collected'] = self::saveFormatedCurrency($order_total);
				$cc_vars['transaction_id'] = $apiResponse['id'];
				$cc_vars['voided']   = 0;
				$this->saveTransaction($order_id, $cc_vars);

				$orderUpdate['alt_paid'] = self::saveFormatedCurrency($order_total);
				$orderUpdate['total'] = self::saveFormatedCurrency($order_total);
			}
			if (!empty($dining_time)) {
				$togotime = $dining_date.' '.$dining_time;
			} else {
				$togotime = date('Y-m-d h:i A', strtotime($dining_date));
			}
			$orderUpdate['togo_time'] = $togotime;
			$orderUpdate['order_status'] = "closed";
			$orderUpdate['void'] = 0;
			$orderUpdate['closed'] = $closed_date;
			$orderUpdate['togo_phone'] = $phoneNumber;
			$orderUpdate['togo_name'] = $togo_name;
			$orderUpdate['togo_type'] = 'LTG';
			$orderUpdate['email'] = $togo_email;
			$orderUpdate['togo_delivery_fees'] = $delivery_fee;
			$orderUpdate['tablename'] = "Lavu To Go PAID";
			$orderUpdate['special_instructions'] = $special_instructions;
			$orderUpdate['delivery_address'] = $customer_address;
			$orderUpdate['togo_status']=$togo_status;
			$orderUpdate['subtotal'] = $newsubtotal;
			if ($isLavuAddPoints == 1) {
				$orderUpdate['subtotal'] = self::saveFormatedCurrency($newsubtotal);
			}
			$orderUpdate['tax'] = self::saveFormatedCurrency($get_totals['calculatedtaxamount']);
			$orderUpdate['last_modified'] = date("Y-m-d H:i:s");
			$orderUpdate['original_id'] = self::generateOriginalId($args);
			$orderUpdate['rounding_amount'] = $roundingAmount;

			// updating order table
			CQ::updateArray ( 'orders', $orderUpdate, "where `order_id` = '" . $order_id . "' and `void` = '3' and `order_status` = 'open' " );
			$orderUpdateDetails = OrdersQueries::updateOrderInformation($order_id, [
				'rounding_amount' => $roundingAmount,
				'total' => $orderUpdate['total'],
				'subtotal' => $orderUpdate['subtotal'],
				'tax' =>  $orderUpdate['tax'],
				'taxdetails' => $taxinform
			], ['amount' => $orderUpdate['total']]);

			$order_result = CQ::getRowsWhereColumn('orders', array("order_id" => $order_id));

			$split_vars = array(
					"discount_sh" => $order_result[0]['discount_sh'],
					"alt_paid" => self::saveFormatedCurrency($order_result[0]['alt_paid']),
					"subtotal" => self::saveFormatedCurrency($order_result[0]['subtotal']),
					"check_total" => self::saveFormatedCurrency($order_result[0]['total']),
					"ioid" => $order_result[0]['ioid'],
					"itax" => $order_result[0]['itax'],
					"email" => $togo_email,
					"discount_id" => $order_result[0]['discount_id'],
					"tax" => $order_result[0]['tax'],
					"tax_info" => $order_result[0]['taxrate'],
					"discount_value" => $discount_details['discounttypes'][0]['discount'],
					"rounding_amount" => $roundingAmount,
					"discount" => $order_result[0]['discount'],
					"discount_type" => $order_result[0]['discount_type'],
					"loc_id" => $location_id
			);

			//updating split check details
			$this->splitCheckUpdate($order_id, $split_vars);

			if ($lavu_pay_type=='LP' || $lavu_pay_type=='GC') {
				$this->changeLoyaltyPoints($loyaltyDetails);
			}

			// action logs
			$l_vars = array(
				'action'		=> "Order Paid",
				'details'		=> "Total: ".$order_total,
				'loc_id'		=> $location_id,
				'order_id'		=> $order_id,
				'time'			=> time(),
				'server_time'	=> date("Y-m-d H:i:s"),
				'user'			=> "Lavu To Go",
				'user_id'		=> 0,
				'device_udid'	=> ""
			);

			// inserting order action in action_log table
			CQ::insertArray('action_log', array_keys($l_vars), array(array_values($l_vars)));

			if ($newInventory) {
				self::updateInventoryQuantity($dataname, $args, $type = 'inventory', $action = 'add', $location_id);
			}
		} catch (Exception $ex) {
			if ($newInventory) {
				self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
			}
			$msg = apiSpeak($languageid, 'Your transaction has failed due to an internal server error for Order').' '.$order_id;
			CQ::updateArray ( 'orders', array("void" => 3,"void_reason" => $msg), "where `order_id` = '" . $order_id . "' ");
			return array('status'=>"F",'msg'=>$msg);
		}

		// START EMAIL ON CardPayment @email
		$location_info = LocationDataController::action_getLocationInfo(1, $dataname);
		$emailto = $email;
		$messagearr = array ();
		$customer = 'Customer';

		// $togo_name contains the user first name and second name
		if ($togo_name != '') {
			$customer = $togo_name;
		}
		//updating address details in lavutogo orders table
		$this->UpdateOrderAdrressDetails($dataname,$locationOrderUpdate, $order_id, $location_info);
		$orderinformationDetails = json_decode($orderUpdateDetails['order_information'], true);
		$messagearr ['customer'] = $customer;
		$messagearr ['order_id'] = $order_id;
		$messagearr ['subtotalamount'] = $orderUpdate ['subtotal'];
		$messagearr ['totalamount'] = PaymentDataController::displayFormatedCurrency(self::saveFormatedCurrency($orderUpdate['total'] + $tipAmount));
		$messagearr ['taxamount'] = $orderUpdate ['tax'];
		$messagearr ['monitary_symbol'] = $location_info ['monitary_symbol'];
		$messagearr ['left_or_right'] = $location_info ['left_or_right'];
		$messagearr ['store_title'] = $location_info ['title'];
		$messagearr ['taxinform'] = $taxinform;
		$messagearr ['store_image'] = $location_info['title'];
		$messagearr ['dataname'] = $dataname;
		$messagearr ['delivery_instructions'] = $special_instructions;
		if ($location_info ['left_or_right'] == 'left') {
			$mon_left_sym = $location_info ['monitary_symbol'] . " ";
			$mon_right_sym = "";
		} else {
			$mon_right_sym = " " . $location_info ['monitary_symbol'];
			$mon_left_sym = "";
		}
		$messagearr ['orderItems'] = $orderinformationDetails['order_contents_information'];
		$messagearr ['restaurant_address'] = $location_info['address'];
		$messagearr ['restaurant_contact_number'] = $location_info['phone'];
		$messagearr ['rounding_amount'] = $roundingAmount;
		PaymentDataController::sendMail($emailto, $messagearr);

		return array('status'=>'S', 'msg'=> apiSpeak($languageid, $this->onSuccessMessage), "order_id" => $order_id);
	}

	protected function action_initiateMealPlanPayment($args){
	    $languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;
		$closed_date = $args['closed_date'];
		// Validate OLO user
		if($args['user_id'] != 0){
			$user_result = CQ::getRowsWithRequiredColumnsOtherDB('users',array("email"),array("_deleted" => 0, "id" => $args['user_id']),'','','lavutogo');
			if(empty($user_result)){
			    $error[] = apiSpeak($languageid, "User details not found");
			}
			else{
				$togo_email = $user_result[0]['email'];
				unset($user_result);
				$userinfo_result = CQ::getRowsWithRequiredColumnsOtherDB('users_info',array("mealplan_user_id"),array("_deleted" => 0, "user_id" => $args['user_id']),'','','lavutogo');
				$userinfo = array_shift($userinfo_result);

				if (empty($userinfo['mealplan_user_id'])) {
				    $error[] = apiSpeak($languageid, "User doesnot have Meal Plan");
				}
			}
		}

		// Validate args
		$error = $this->validatePaymentArgs($args);

		// Validate order
		$orders = OrdersQueries::checkOrder($args['order_id']);
		$order = array_shift($orders);
		$order_errors = $this->validateOrder($order, $languageid);
		$error = array_merge($error, $order_errors);

		// Set args to local vars
		$dataname = isset($args['dataname'])?$args['dataname']:'';
		$order_id = isset($args['order_id'])?$args['order_id']:'';
		$orderItemsDetails = isset($args['orderItemsDetails'])?$args['orderItemsDetails']:'';
		$payment_method_nonce = isset($args['payment_method_nonce'])?$args['payment_method_nonce']:'';
		$user_id = isset($args['user_id'])?$args['user_id']:0;
		$order_total = isset($args['payable_amount'])?$args['payable_amount']:'';
		$order_type = isset($args['order_type'])?$args['order_type']:'';
		$togo_email = $email = isset($args['email_id'])?$args['email_id']:'';
		$togo_phone = $phoneNumber = isset($args['phone_number'])?$args['phone_number']:'';
		$first_name = isset($args['first_name'])?$args['first_name']:'';
		$last_name = isset($args['last_name'])?$args['last_name']:'';
		$togo_name = $first_name." ".$last_name;
		$customer_address = isset($args['customer_address'])?$args['customer_address']:'';
		$lavu_pay_type = isset($args['lavu_pay_type'])?$args['lavu_pay_type']:'';
		$lavu_card_number = isset($args['lavu_card_number'])?$args['lavu_card_number']:'';
		$discount_amount = isset($args['lavu_pay_amount'])?$args['lavu_pay_amount']:'';
		$discountId = isset($args['discount_id'])?$args['discount_id']:'';
		$tip_type = isset($args['tip_type'])?$args['tip_type']:'';
		$tipAmount = isset($args['tip_amount'])?$args['tip_amount']:0;
		$delivery_fee = isset($args['delivery_fee'])?$args['delivery_fee']:0;
		$dining_date = isset($args['dining_date']) ? $args['dining_date'] : '';
		$dining_time = isset($args['dining_time']) ? $args['dining_time'] : '';
		$special_instructions = isset($args['delivery_instructions'])?trim($args['delivery_instructions']):'';
		$togo_status= $this->toGetOrderTypeIdBaseOnOrderType($args['order_type']);
		$location_id = isset($args['location_id']) ? $args['location_id'] : 1;

		// updating order before payment
		$order_result = OrdersDataController::orderUpdate($orderItemsDetails, $order_id, $dataname, $location_id);
		if ($order_result['status'] == "F") {
		    return array("status" => "F", "details" => $order_result['result'], "msg" => apiSpeak($languageid, "Some items are out of stock"));
		}

		if(!empty($lavu_pay_type)){

			if($lavu_pay_type != "LP" && $lavu_pay_type != "GC"){
			    $error[] = apiSpeak($languageid, "Invalid Pay Type");
			}
			else{
				if(empty($args['lavu_card_number'])){ // validating lavu gift card number
				    $error[] = apiSpeak($languageid, "Lavu Card No is required");
				}

				if($lavu_pay_type == "LP"){ // Loyalty Points
					if($args['lavu_add_points'] == 1){ // award points
						if($discount_amount <= 0){
						    $error[] = apiSpeak($languageid, "Invalid Award Points");
						}
						else{
						    $lavu_award_points_details = $this->action_checkAwardPoints(array("lavu_card" => $lavu_card_number, "dataname" => $dataname, "orderId" => $order_id, "languageid" => $languageid));
							//print_r($lavu_award_points_details);exit;
							if($lavu_award_points_details['status'] == "F"){
								$error[] = $lavu_award_points_details['error'];
							}
							else if($lavu_award_points_details['status'] == "1")
							{
								if($lavu_award_points_details['awardpoints'] > 0){
									if((float)$lavu_award_points_details['awardpoints'] != (float)$discount_amount){
									    $error[] = apiSpeak($languageid, "Invalid Award Points");
									}
									else{
										$lavu_award_points = $lavu_award_points_details['awardpoints'];
									}
								}
							}
						}
					}
					else if($args['lavu_add_points'] == 0){ // discount points
						if(empty($discountId))
						{
						    $error[] = apiSpeak($languageid, "Loyalty Points Discount Id is required");
						}
						else{
							if($discount_amount <= 0){
							    $error[] = apiSpeak($languageid, "Invalid Discount Amount");
							}
							else{
							    $discount_details = $this->action_checkLoyaltyDiscountPoints(array("lavu_card" => $lavu_card_number, "dataname" => $dataname, "orderId" => $order_id, "discountId" => $discountId, "orderItemsDetails" => $orderItemsDetails, "languageid" => $languageid));
								//print_r($discount_details);exit;
								if($discount_details['status'] == "F"){
									$error[] = $discount_details['error'];
								}
								else if($discount_details['status'] == "1")
								{
									if(empty($discount_details['discounttypes'])){
									    $error[] = apiSpeak($languageid, "Invalid Discount Id");
									}
									else if((float)trim($discount_details['discounttypes'][0]['discount_value']) != (float)$discount_amount){
									    $error[] = apiSpeak($languageid, "Invalid Discount Value");
									}
								}
							}
						}
					}
				}
				else if($lavu_pay_type == "GC"){ // gift card
					if($discount_amount <= 0){
					    $error[] = apiSpeak($languageid, "Invalid Lavu Gift Card Amount");
					}
					else{
					    $GiftCardDetails = $this->action_getLavuGiftCardDetails(array('lavu_card' => $lavu_card_number,'dataname' => $dataname, "languageid" => $languageid));
						//print_r($GiftCardDetails);
						if($GiftCardDetails['status'] == "F"){
							$error[] = $GiftCardDetails['error'];
						}
						else if($GiftCardDetails['status'] == "1")
						{
							if($GiftCardDetails['lavu_gift_card_balance'] <= 0){
							    $error[] = apiSpeak($languageid, "Insufficient Lavu Gift Card Amount");
							}
							else if($GiftCardDetails['lavu_gift_card_balance'] != $discount_amount){
							    $error[] = apiSpeak($languageid, "Invalid Lavu Gift Card Amount");
							}
						}
					}
				}
			}
		}

		// Tip Validation
		if (!empty($tip_type)) {
			if ($tip_type != "C" && $tip_type != "P") {
			    $error[] = apiSpeak($languageid, "Invalid Tip Type");
			}
			else {
				if ($tipAmount <= 0) {
				    $error[] = apiSpeak($languageid, "Invalid Tip Amount");
				}
			}
		}

		// Ensure we have a MEAL PLAN payment_type
		$payment_types = CQ::getRowsWithRequiredColumns('payment_types', array('id', 'type'), array("_deleted" => 0, "type" => 'MEAL PLAN'), '', '', $dataname);

		$payment_type = array_shift($payment_types);
		if (empty($payment_type)) {
		    $error[] = apiSpeak($languageid, "Meal Plan Payment Type is required");
		}

		// Exit if error(s)
		if(!empty($error)){
			return array("status"=>"F", "msg"=>$error);
		}

		$order_arr = OrdersQueries::checkOrder($order_id);
		$o_rowsResult = CQ::getRowsWithRequiredColumns('orders', array("subtotal","total"), array("order_id" => $order_id));
		if($discountId =='')
		{
			$orderAmount= $order_arr[0]['amount'];
		}
		else {
			$orderAmount= $o_rowsResult[0]['total'];
		}

		$res = self::getDecimalData(1, $dataname);
		self::$decimal_char = $res[0]['decimal_char'];
		self::$disable_decimal = $res[0]['disable_decimal'];
		self::$rounding_factor = $res[0]['rounding_factor'];
		self::$round_up_or_down = $res[0]['round_up_or_down'];

		$isLavuAddPoints = (int) $args['lavu_add_points'];//add lavu award points if this value is having 1 or for 0 deduct the available balance from card

		$tipAmount = self::saveFormatedCurrency($tipAmount);
		$left_total = trim($orderAmount+$tipAmount) - ($isLavuAddPoints == 1 ? 0 : $discount_amount);
		$right_total = trim($order_arr[0]['amount'] - ($isLavuAddPoints == 1 ? 0 : $discount_amount));
		$right_total = $right_total + $tipAmount;

		if ($left_total != $right_total) {
		    return array('status'=>"F", 'msg'=> apiSpeak($languageid, 'Invalid Order Amount'));
		}

		$whereColumnDiscnt= array("id" => $discountId);
		$resultDiscnt = CQ::getRowsWhereColumn('discount_types', $whereColumnDiscnt, '', '', $dataname);

		$whereColumnOrder=array("order_id"=>$order_id);
		$columnNameOrder=array("subtotal","tax","total");
		$resOrder = CQ::getRowsWithRequiredColumns('orders',$columnNameOrder,$whereColumnOrder);

		$discnt_p = $resultDiscnt[0]['calc']*100;

		if($isLavuAddPoints == 0 && ($lavu_pay_type == "GC" || $lavu_pay_type == "LP")){
			if($lavu_pay_type == "GC"){
				$lavu_gc_card_amt = $discount_amount;
			}
		}

		// Get LTG minimum order amount
		$ltg_minimum_order_amount = 0;
		$rowsResult = CQ::getRowsWithRequiredColumns('config', array('value'), array('setting' => 'ltg_minimum_order_price'));
		if (!empty($rowsResult)) {
			$ltg_minimum_order_amount = $rowsResult[0]['value'];
		}

		if($isLavuAddPoints == 0 && ($lavu_pay_type == "GC")){
			if($order_total > $lavu_gc_card_amt){
				//$order_total = $order_total - $lavu_gc_card_amt;
				$discount_amount = $lavu_gc_card_amt;
			}
			else{
				//$discount_amount = $order_total;
				$order_total = 0;

			}
		}

		if ($lavu_pay_type!='') {
			if ($lavu_pay_type=='LP') {
				if ($isLavuAddPoints == 1) {
					$loyaltyDetails['loyalty_type'] = 'a';
					$loyaltyDetails['awardpoints'] = $discount_amount;
				} else if ($isLavuAddPoints == 0) {
					if (!empty($discount_details['discounttypes'])) {
						$loyaltyDetails['loyalty_type'] = ($discount_details['discounttypes'][0]['loyalty_use_or_unlock']==1)?'d':'';
						$loyaltyDetails['discount_points'] = $discount_details['discounttypes'][0]['discount_points'];

						if ($discount_details['discounttypes'][0]['discount_value']>=$orderAmount) {
							$payableAmount = $discount_details['discounttypes'][0]['discount_value']-$orderAmount;
						}
						if ($discount_details['discounttypes'][0]['discount_value']<=$orderAmount) {
							$payableAmount = $orderAmount-$discount_details['discounttypes'][0]['discount_value'];
						}
						$discountDetArr['discount_id'] = $discount_details['discounttypes'][0]['discount_id'];
						$discountDetArr['discount'] = $discount_amount;
						$discountDetArr['discount_value'] = $discount_amount;
						$discountDetArr['discount_sh'] = $discount_details['discounttypes'][0]['title'];
						$discountDetArr['discount_type'] = $discount_details['discounttypes'][0]['discount_type'];
					}
				}

			} else if($lavu_pay_type=='GC') {
				$loyaltyDetails['loyalty_type'] = 'GC';
				$loyaltyDetails['discount_amount'] = $discount_amount;
			}

				$loyaltyDetails['cardnumber'] =  $lavu_card_number;
				$loyaltyDetails['dataname'] =  $dataname;
			}

				$discountdata = array();
				if (!empty($discount_details['discounttypes'][0])) {
					$discountdata = $discount_details['discounttypes'][0];
				}

				if (empty($dining_time)) {
					$dining_time = date("h:i A");
				}

				$locationOrderUpdate['phone_number'] = $phoneNumber;
				$locationOrderUpdate['email_id'] = $email;
				$locationOrderUpdate['first_name'] = $first_name;
				$locationOrderUpdate['last_name'] = $last_name;
				$locationOrderUpdate['delivery_instructions'] = $args['delivery_instructions'];
				$locationOrderUpdate['customer_address'] = $customer_address;
				$locationOrderUpdate['order_type'] = $order_type;
				$locationOrderUpdate['delivery_fee'] = $delivery_fee;
				$locationOrderUpdate['dining_date'] = $dining_date;
				$locationOrderUpdate['dining_time'] = $dining_time;
				$locationOrderUpdate['discount_info'] = $discountdata;
				$locationOrderUpdate['tip_amount'] = $tipAmount;
				$locationOrderUpdate['payment_type'] = 'Meal Plan';
				$locationOrderUpdate['card_type'] = 'Meal Plan';
				$locationOrderUpdate['user_id'] = $user_id;
				$local_datetime = date("Y-m-d H:i:s");
				$locationOrderUpdate['user_id'] = $user_id;
				$ioid_results = CQ::getRowsWithRequiredColumns('orders', array("ioid"), array("order_id" => $order_id));
				$ioid_result = array_shift($ioid_results);
				$ioid = $ioid_result['ioid'];

				$location_info = LocationDataController::action_getLocationInfo(1, $dataname);
				$local_datetime = substr(LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]),0,-1); //date("Y-m-d H:i:s");

				if ($lavu_pay_type=='') { // no discount types or gift card
					$order_total = $order_total + $delivery_fee;
				}

				// Process transaction
				$mealPlanInfo = $this->getMealPlanInfo($userinfo['mealplan_user_id']);

				$newInventory = self::isNewInventory($dataname);
				if ($newInventory) {
					self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'add', $location_id);
				}

				try {
					$data_name = $dataname;
					require_once('/home/poslavu/public_html/admin/cp/resources/mealplan_functions.php');
					$result = useMealPlan( array('userId' => $userinfo['mealplan_user_id'], 'amount' => $order_total) );

					if (isset($result['stipend_balance'])) $mealPlanInfo['stipend_balance'] = $result['stipend_balance'];
					if (isset($result['cash_balance'])) $mealPlanInfo['card_balance'] = $result['cash_balance'];  // Notice: cash_balance => card_balance
				}
				catch(Exception $ex){
					if ($newInventory) {
						self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
					}
					error_log('Meal Plan failed '.$ex->getMessage());
					return array('status'=>"F", 'msg'=>apiSpeak($languageid, 'Meal Plan failed').' '.$ex->getMessage());
				}

				if (empty($result['success'])) {
					if ($newInventory) {
						self::updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'removed', $location_id);
					}
					$msg = apiSpeak($languageid, 'Your order failed due to').' '.$result['msg'];
					error_log($msg);
					CQ::updateArray('orders', array("void" => 1,"void_reason" => $msg), "where `order_id` = '" . $order_id . "' ");
					return array('status' => 'F', 'msg' => $msg);
				}

				$order_info = json_decode($order_arr[0]['order_information'], true);
				$taxinform = OrdersDataController::displaytax($order_info['order_contents_information'], $discnt_p);
				$get_totals = OrdersDataController::caltotal($taxinform);
				$newsubtotal = self::saveFormatedCurrency($get_totals['subtotal']);
				$order_total = self::saveFormatedCurrency($get_totals['grand_total'] + $delivery_fee);
				$roundingAdjustment = CQ::roundingAdjustment($order_total, $dataname, $location_id);
				$order_total = self::saveFormatedCurrency($roundingAdjustment['amount']);
				$roundingAmount = $roundingAdjustment['roundingAmount'];

				// Inserting cc_transactions rows
				try {
					$this->saveTransaction($order_id, array(
							'loc_id'          => $location_id,
							'order_id'        => $order_id,
							'amount'          => $order_total,
							'card_type'       => $payment_type['type'],
							'currency_code'   => '',
							'datetime'        => $local_datetime,
							'ioid'            => $ioid,
							'last_mod_ts'     => time(),
							'pay_type'        => $payment_type['type'],
							'pay_type_id'     => $payment_type['id'],
							'tip_amount'      => $tipAmount,
							'total_collected' => $order_total,
							'transaction_id'  => '',
							'voided'          => 0,
					));
					$originalId = '';
					$originalId = self::generateOriginalId($args);

					// Update order
					$orderUpdate = array(
							'total'              => $order_total,
							'subtotal'			 => $newsubtotal,
							'tax'				 => self::saveFormatedCurrency($get_totals['calculatedtaxamount']),
							'cash_paid' 		 => $order_total,
							'cash_applied' 		 => $order_total,
							'void'               => 0,
							'other_tip'          => $tipAmount,
							'togo_phone'         => $phoneNumber,
							'togo_name'          => $togo_name,
							'togo_time'          => date('Y-m-d H:i:s', strtotime($dining_time)),
							'togo_type'          => $order_type,
							'email'              => $togo_email,
							'togo_delivery_fees' => $delivery_fee,
							'tablename'          => 'Lavu To Go PAID',
							'special_instructions'  => $special_instructions,
							'togo_status'        => $togo_status,
							'tabname'            => "Lavu To Go - ".$first_name." ".$last_name,
							'delivery_address'   => $customer_address,
							'order_status'       => "closed",
							'closed'             => $closed_date,
							'original_id'		 => $originalId,
							'rounding_amount'	 => $roundingAmount
					);
					$orderUpdate['last_modified'] = date("Y-m-d H:i:s");
					$orderUpdate = (!empty($discountDetArr) && count($discountDetArr) > 0)  ? array_merge($orderUpdate,$discountDetArr) : $orderUpdate;
					CQ::updateArray('orders', $orderUpdate, "where `order_id` = '{$order_id}' and `void` = '3' and `order_status` = 'open'");
					$orderUpdateDetails = OrdersQueries::updateOrderInformation($order_id, [
						'rounding_amount' => $roundingAmount,
						'total' => $orderUpdate['total'],
						'subtotal' => $orderUpdate['subtotal'],
						'tax' =>  $orderUpdate['tax'],
						'taxdetails' => $taxinform
					], [
						"amount" => $orderUpdate['total'],
					]);
					$orders = CQ::getRowsWhereColumn('orders', array('order_id' => $order_id));
					$order = array_shift($orders);

					// Update split_check_details
					$split_vars = array(
							"discount_sh" => $order['discount_sh'],
							"alt_paid" => self::saveFormatedCurrency($order['alt_paid']),
							"subtotal" => self::saveFormatedCurrency($orderUpdate['subtotal']),
							"check_total" => self::saveFormatedCurrency($orderUpdate['total']),
							"ioid" => $order['ioid'],
							"itax" => $order['itax'],
							"email" => $togo_email,
							"discount_id" => $order['discount_id'],
							"tax" => $order['tax'],
							"tax_info" => $order['taxrate'],
							"discount_value" => $discount_details['discounttypes'][0]['discount'],
							"rounding_amount" => $roundingAmount,
							"discount" => $order['discount'],
							"discount_type" => $order['discount_type'],
							"loc_id" => $location_id
					);
					$this->splitCheckUpdate($order_id, $split_vars);

					// update discounts for order contents
					if(isset($orderUpdate['discount_id'])){
						$this->updateDiscountForOrderContents($order_id, $orderUpdate);
					}

					if($lavu_pay_type=='LP' || $lavu_pay_type=='GC'){
						$this->changeLoyaltyPoints($loyaltyDetails);
					}

					//updating address details in lavutogo orders table
					$this->UpdateOrderAdrressDetails($dataname, $locationOrderUpdate, $order_id, $location_info);

					// inserting order action in action_log table
					$l_vars = array(
							'action'        => "Order Paid",
							'details'       => "Total: ".$order_total,
							'loc_id'        => $location_id,
							'order_id'      => $order_id,
							'time'          => time(),
							'server_time'   => date("Y-m-d H:i:s"),
							'user'          => "Lavu To Go",
							'user_id'       => 0,
							'device_udid'   => ""
					);
					CQ::insertArray('action_log', array_keys($l_vars), array(array_values($l_vars)));

					self::updateInventoryQuantity($dataname, $args, $type = 'inventory', $action = 'add', $location_id);
				}
				catch (Exception $ex){
				    $msg = apiSpeak($languageid, 'Your transaction has failed due to an internal server error for Order').' '.$order_id;
					error_log($msg);
					CQ::updateArray ( 'orders', array("void" => 1,"void_reason" => $msg), "where `order_id` = '" . $order_id . "' ");
					return array('status'=>"F",'msg'=>$msg);
				}

				$orderinformationDetails = json_decode($orderUpdateDetails['order_information'], true);

				// START EMAIL ON MEAL PLAN @email
				$emailto = $email;
				$messagearr = array ();
				$customer = 'Customer';
				// $togo_name contains the user first name and second name
				if ($togo_name != '') {
				    $customer = $togo_name;
				}
				$messagearr ['customer'] = $customer;
				$messagearr ['order_id'] = $order_id;
				$messagearr ['subtotalamount'] = $orderUpdate['subtotal'];
				$messagearr ['totalamount'] = PaymentDataController::displayFormatedCurrency(self::saveFormatedCurrency($orderUpdate['total'] + $tipAmount));
				$messagearr ['taxamount'] = $orderUpdate['tax'];
				$messagearr ['monitary_symbol'] = $location_info ['monitary_symbol'];
				$messagearr ['left_or_right'] = $location_info ['left_or_right'];
				$messagearr ['store_title'] = $location_info ['title'];
				$messagearr ['taxinform'] = $taxinform;
				$messagearr['store_image'] = $location_info['title'];
				$messagearr['dataname'] = $dataname;
				$messagearr['delivery_instructions'] =$special_instructions;

				if ($location_info ['left_or_right'] == 'left') {
				    $mon_left_sym = $location_info ['monitary_symbol'] . " ";
				    $mon_right_sym = "";
				} else {
				    $mon_right_sym = " " . $location_info ['monitary_symbol'];
				    $mon_left_sym = "";
				}
				$messagearr ['orderItems'] = $orderinformationDetails['order_contents_information'];
				$messagearr ['restaurant_address'] = $location_info['address'];
				$messagearr ['restaurant_contact_number'] = $location_info['phone'];
				$messagearr ['rounding_amount'] = $roundingAmount;

				PaymentDataController::sendMail ( $emailto, $messagearr );
				// END EMAIL ON MEAL PLAN

				return array('status'=>'S', 'msg'=> apiSpeak($languageid, $this->onSuccessMessage), "order_id" => $order_id, 'mealplan' => $mealPlanInfo);
	}

	protected function action_initiateMealPlanAddFunds($args) {

	    $languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;
		// Min $1 for paypal
		if ((double) $args['payable_amount'] < 1.00) {
		    return array('status'=>"F",'msg'=>apiSpeak($languageid, 'To process a payment with PayPal, the minimum order amount is $1'));
		}

		// Validate OLO user
		if($args['user_id'] != 0){
			$user_result = CQ::getRowsWithRequiredColumnsOtherDB('users',array("email"),array("_deleted" => 0, "id" => $args['user_id']),'','','lavutogo');
			if(empty($user_result)){
			    $error[] = apiSpeak($languageid, "User details not found");
			}
			else{
				unset($user_result);
				$userinfo_result = CQ::getRowsWithRequiredColumnsOtherDB('users_info',array("mealplan_user_id"),array("_deleted" => 0, "user_id" => $args['user_id']),'','','lavutogo');
				$userinfo = array_shift($userinfo_result);
				if (empty($userinfo['mealplan_user_id'])) {
				    $error[] = apiSpeak($languageid, "User doesnot have Meal Plan");
				}
			}
		}

		// TO DO : more validation (?)

		// Exit if error(s)
		if(!empty($error)){
			return array("status"=>"F", "msg"=>$error);
		}

		$result = array('success' => false);
		try {
			$sale = array(
				'paymentMethodNonce' => $args['payment_method_nonce'],  // for a guaranteed success during testing, use: 'fake-valid-nonce'
				'amount'             => $args['payable_amount'],
				'options'            => array(/*'submitForSettlement' => true,*/ 'paypal' => array()),
				'channel'  => self::$lavu_channel
			);
			$braintreeResult = !$this->braintreeConnect->hasError ? $this->braintreeConnect->doSale($sale) : [
				"status" => false,
			];
		}
		catch (Exception $e) {
			error_log($e->getMessage());
			return array('status'=>"F",'msg'=>$e->getMessage());
		}

		$mealplan = $this->getMealPlanInfo($userinfo['mealplan_user_id']);

		// Add funds to meal plan card
		if ($braintreeResult->success) {
			addCashBalance(array('userId' => $userinfo['mealplan_user_id'], 'amount' => $args['payable_amount']));
			$return_status = 1;
			$return_msg = apiSpeak($languageid, 'Your funds have been successfully added');
		}
		else {
			$return_status = 'F';
			$return_msg = $braintreeResult->message;
		}

		// Get current meal plan card balance to return
		$mealplan = $this->getMealPlanInfo($userinfo['mealplan_user_id']);

		return array('status' => $return_status, 'msg'=> $return_msg, 'mealplan' => $mealplan);
	}


	protected function paymentResponseSave($result,$paymentStatus){
		if($paymentStatus===false){
			// $result['transaction']['orderId']=$args['order_id'];
			// $result['transaction']['amount']=$args['payable_amount'];
			$result['transaction']['id']='';
		}
		$order_info = $this->GetOrderInfo($dataname = "demo_training_1", $result['transaction']['orderId'], $location_id=1);
		$order_id = $order_info[0]['order_id'];
		$ioid = $order_info[0]['ioid'];
		$order_info = $this->saveTransaction($dataname, $ioid, $location_id=1, $result,$paymentStatus);
		$split_check = $this->splitCheckUpdate($dataname, $ioid, $location_id=1,$result,$paymentStatus);
		$ress = $this->UnvoidOrder($dataname,$card_paid='',$gift_paid='test', $order_id,$paymentStatus);

	}

	protected function updateGiftCard($lavuCardNumber,$lavuPayAmount, $dataname){
		$updateGiftCardQuery ="update poslavu_MAIN_db.`lavu_gift_cards` set balance='".$lavuPayAmount."' where `name` = '".$lavuCardNumber."' and `dataname` = '".$dataname."' ";
		$resultObj = DBConnection::adminQuery($updateGiftCardQuery);
		return $resultObj;
	}

	//    Order saving methods starts

	protected function  GetOrderInfo($dataname, $order_id, $location_id){
		$get_order_info ="SELECT `id`, `order_id`, `subtotal`, `tax`, `total`, `card_paid`, `void`, `location_id`, `email`, `togo_time`, `togo_name`, `ioid`,
			`active_device`, `location_id`, `order_status` FROM `orders` WHERE `order_id`= '$order_id' AND `location_id` = '$location_id'
			ORDER BY `opened` DESC LIMIT 1";
		$resultObj = DBConnection::clientQuery($get_order_info);
		$result =  MA::resultToArray($resultObj);
		//echo "<pre>";print_r($result); exit;
		return $result;

	}

	// Based on core_functions's newInternalID()
	protected function newInternalID() {

		$prefix = self::$restaurant_id.self::$loc_info['id']."0";
		$randomString = str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);

		$str = $prefix."-".$randomString."-".date("YmdHis");

		return $str;
	}


	protected function saveTransaction($order_id, $o_vars){

		$default = array(
		  'action'            => "Sale",
		  'ag_amount'       => "",
		  'auth'    => "0",
		  'auth_code'   => "",
		  'batch_no'   => "",
		  'card_desc'   => "",
		  'change'   => "0",
		  'check'    => "1",
		  'customer_id'  => "0",
		  'device_udid'  => "",
		  'exp'    => "",
		  'first_four'  => "",
		  'for_deposit'  => "0",
		  'gateway'   => "",
		  'got_response'  => "1",
		  'info'    => "",
		  'info_label'  => "",
		  'internal_id' => $this->newInternalID(),
		  'is_deposit'  => "0",
		  'more_info'   => "",
		  'mpshc_pid'   => "",
		  'pin_used_id'  => "0",
		  'preauth_id'  => "",
		  'processed'   => "0",
		  'process_data'  => "",
		  'record_no'   => "",
		  'reader'   => "",
		  'refunded'   => "0",
		  'refunded_by'  => "0",
		  'refund_notes'  => "",
		  'refund_pnref'  => "",
		  'ref_data'   => "",
		  'register'   => "",
		  'register_name'  => "",
		  'rf_id'    => "",
		  'server_id'   => "0",  // TO DO : query for OLO user
		  'server_name'  =>"",
		  'server_time'  => gmdate('Y-m-d H:i:s') . 'Z',
		  'signature'   => "0",
		  'split_tender_id' => "0",
		  'swipe_grade'  => "",
		  'temp_data'   => "",
		  'tip_for_id'  => "0",
		  'transtype'   => "Sale",
		  'voice_auth'  => "0",
		  'voided_by'   => "0",
		  'void_notes'  => "",
		  'void_pnref'  => "",
		  'extra_info'  =>'1'
		);
		$o_vars = array_merge($default, $o_vars);

		return CQ::insertArray('cc_transactions', array_keys($o_vars), array(array_values($o_vars)));
	}

	protected function splitCheckUpdate($order_id,$arr){
		$result = CQ::updateArray ( 'split_check_details', $arr, "where `order_id` = '" . $order_id . "'" );
	}

	protected function UpdateOrderAdrressDetails($dataname, $orderDetails, $order_id, $location_info =""){
		if ($orderDetails['order_type'] != 'delivery' && $orderDetails['customer_address'] == '') {
			$orderDetails['customer_address'] = $location_info ['title'].",".$location_info ['address'].",".$location_info ['city'].",".$location_info ['state'];
		}
		$user_id = $orderDetails['user_id'];
		$objDescription = JsonManager::encodeSpecialCharacters($orderDetails);
		$transaction_info = '{"approved":"1"}';
		$sql ="UPDATE lavutogo.`orders` SET `consumer_id` = '$user_id', `description` ='$objDescription', `transaction_information` = '$transaction_info'  WHERE `order_id` = '$order_id'";
		$result = DBConnection::adminQuery($sql);
	}

	function UTCDateTime($add_z, $ts="")
	{
		if ($ts == "")
		{
			$ts = time();
		}

		$utc_tz = new DateTimeZone("UTC");
		$datetime = new DateTime(NULL, $utc_tz);
		$datetime->setTimestamp($ts);

		$datetime_str = $datetime->format("Y-m-d H:i:s");
		if ($add_z)
		{
			$datetime_str .= "Z";
		}

		return $datetime_str;
	}

	//    Order saving methods end

	protected function action_getLavuGiftCardDetails($args){
		//echo "<pre>";print_r($args); exit;
		$card_no = $args['lavu_card'];
		$dataname = $args['dataname'];
		$languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;

		$query = "select * from `poslavu_MAIN_db`.`lavu_gift_cards` where deleted = 0 AND `name` = '".$card_no."' and `dataname` = '".$dataname."' ";
		//echo $query;exit;
		$resultObj = DBConnection::adminQuery($query);
		$result =  MA::resultToArray($resultObj);
		//echo "<pre>";print_r($resultArr); exit;
		/*$whereDataNameColumnArr = array("name" => $card_no, "dataname" => $dataname);

		$result = CQ::getRowsWhereColumn('lavu_gift_cards', $whereDataNameColumnArr, '');
		*/
		$card_balance = 0;
		//echo "<pre>";print_r($result); exit;
		$resultArr = array();
		if(!empty($result))
		{
		 $datetime = time();
		 $card_dbid = $result[0]['id'];
		 $gift_expires = $result[0]['expires'];
		 $inactive = $result[0]['inactive'];
		 if($inactive == 1){
		     $resultArr = array("error" => apiSpeak($languageid, "Lavu Gift Card is inactive"), "status" => "F");
		 }
		 else{
		  if(empty($gift_expires)){
		   $expired = false;
		  }
		  else{
		   $expired = (strtotime($gift_expires) - $datetime) < 0;
		  }
		  if($expired){
		      $resultArr = array("error" => apiSpeak($languageid, "Lavu Gift Card is expired"), "status" => "F");
		  }
		  else {
			$card_balance = $result[0]['balance'] * 1;
			$resultArr = array("lavu_gift_card_balance" => $card_balance, "status" => 1);
		  }
		 }
		}
		else{
		    $resultArr = array("error" => apiSpeak($languageid, "Lavu Gift Card doesnot exists"), "status" => "F");
		}

		return $resultArr;
	}

	private function changeLoyaltyPoints($loyaltyDetails){
		//print_r($loyaltyDetails);
		$card_number = $loyaltyDetails['cardnumber'];
		$dataname = $loyaltyDetails['dataname'];
		$giftArr = CQ::getRowsWithRequiredColumnsMainDB('lavu_gift_cards', array("points", "balance"), array("dataname"=>$dataname, "name" => $card_number), "", "");
		//print_r($giftArr);exit;
		$gift_vars = array();
		if($loyaltyDetails['loyalty_type'] == "d"){
			$gift_vars['points'] = $giftArr[0]['points'] - $loyaltyDetails['discount_points'];
		}
		elseif ($loyaltyDetails['loyalty_type'] == "a"){
			$gift_vars['points'] = $giftArr[0]['points'] + $loyaltyDetails['awardpoints'];
		}
		elseif ($loyaltyDetails['loyalty_type'] == "GC"){
			$gift_vars['balance'] = $giftArr[0]['balance'] - $loyaltyDetails['discount_amount'];
		}
		if(!empty($gift_vars))

			$result = CQ::updateArray ( 'poslavu_MAIN_db.lavu_gift_cards', $gift_vars, "where `dataname` = '" . $dataname . "' AND `name` = '" . $card_number . "'",false );

	}
	protected function action_checkAwardPoints($args){
		$card_no = $args['lavu_card'];
		$dataname = $args['dataname'];
		$order_id = $args['orderId'];
		$languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;

		if(!isset($order_id) || $order_id == "")
		{
		    return array("error" => apiSpeak($languageid, "Order Id is required"), "status" => "F");
		}
		else if(!isset($card_no) || empty($card_no)){
		    return array("error" => apiSpeak($languageid, "Lavu Card No is required"), "status" => "F");
		}

		$order_arr = OrdersQueries::checkOrder($order_id);

		if(empty($order_arr)){
		    return array("status"=>"F", "msg"=>apiSpeak($languageid, "Invalid Order Id"));
		}
		else{
			$order_id = $order_arr[0]['order_id'];
			$order_total = $order_arr[0]['amount'];
			if($order_total > 0){
				$order_information = json_decode($order_arr[0]['order_information'], true);
				$orderItems = array();
				$i=1;
				foreach($order_information['order_contents_information'] as $order_contents){
					if(array_key_exists($order_contents['item_id'],$orderItems)!=false){
						$orderItems[$order_contents['item_id']] = $order_contents['quantity']+$i;
						$i++;
					}
					else{
						$orderItems[$order_contents['item_id']] = $order_contents['quantity'];
					}
				}
			}
			else{
			    return array("error" => apiSpeak($languageid, "Zero order amount. So cannot proceed to Loyalty Points"), "status" => "F");
			}
		}

		$whereColumnArr= array("name" => $card_no, "dataname" => $dataname);
		//$result = CQ::getRowsWhereColumn('lavu_gift_cards', $whereColumnArr, '', '', "MAIN");
		$colsArray = array('id','expires','inactive','points');
		$result = CQ::getRowsWithRequiredColumnsMainDB('lavu_gift_cards',$colsArray,$whereColumnArr);

		$points_balance = 0;

		$resultArr = array();
		if(!empty($result))
		{
			$card_dbid = $result[0]['id'];
			$gift_expires = $result[0]['expires'];
			$inactive = $result[0]['inactive'];
			if($inactive == 1){
			    $resultArr = array("error" => apiSpeak($languageid, "Lavu Loyalty Number is inactive"), "status" => "F");
			}
			else{
				if(empty($gift_expires)){
					$expired = false;
				}
				else{
					$location_info = LocationDataController::action_getLocationInfo(1, $dataname); // fetching location info
					//echo "<pre>";print_r($location_info);
					$datetime = substr(LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]),0,-1);
					//$datetime = date("Y-m-d H:i:s");
					$expired = (strtotime($gift_expires) - strtotime($datetime)) < 0;
				}
				if($expired){
				    $resultArr = array("error" => apiSpeak($languageid, "Lavu Loyalty Number is expired"), "status" => "F");
				}
				else {
					$points_balance = $result[0]['points'] * 1;
					$resultArr = array("loyality_points" => $points_balance, "status" => 1);

					$menu_data = $orderItems;//array("474" => 3); // menu items id and qty
					$menu_items_data = CQ::getRowsWhereColumnIn("menu_items","id",array_keys($menu_data));
					$total_award = 0;
					foreach ($menu_items_data as $menu_item)
					{
						$special2 = strtolower($menu_item['hidden_value3']); // special details 2
						$price = $menu_item['price']; // item price
						$award_parts = explode("award ",$special2);
						if(count($award_parts) > 1)
						{
							$point_parts = explode(" point",$award_parts[1]);
							if(count($point_parts) > 1)
							{
								$point_str = trim($point_parts[0]);

								if(is_numeric($point_str))
								{
									$min_cost = 0;
									$min_cost_parts = explode("minimum cost ",$point_parts[1]);
									if(count($min_cost_parts) > 1)
									{
										$min_cost_str = trim(str_replace("of ","",$min_cost_parts[1]));
										if(is_numeric($min_cost_str))
										{
											$min_cost = $min_cost_str * 1;
										}
									}

									if($price * 1 >= $min_cost * 1)
									{
										$total_award += $point_str * $menu_data[$menu_item['id']];
									}
								}
							}
						}
					}

					$resultArr['awardpoints'] = $total_award;

				}
			}
		}
		else{
		    $resultArr = array("error" => apiSpeak($languageid, "Lavu Loyalty Number doesnot exists"), "status" => "F");
		}

		return $resultArr;
	}

	protected function action_checkLoyaltyDiscountPoints($args)
	{
		// echo "<pre>";print_r($args); exit;
		$card_no = $args['lavu_card'];
		$dataname = $args['dataname'];
		$order_id = $args['orderId'];
		$discountId = $args['discountId'];
		$orderItemsDetails = $args['orderItemsDetails'];
		$languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;
		if(!isset($order_id) || $order_id == "")
		{
		    return array("error" => apiSpeak($languageid, "Order Id is required"), "status" => "F");
		}
		else if(!isset($card_no) || empty($card_no)){
		    return array("error" => apiSpeak($languageid, "Lavu Card No is required"), "status" => "F");
		}
		else if(!isset($discountId) || empty($discountId)){
		    return array("error" => apiSpeak($languageid, "Loyalty Discount Id is required"), "status" => "F");
		}

		$order_arr = OrdersQueries::checkOrder($order_id);
		//print_r($order_arr);
		if(empty($order_arr)){
		    return array("status"=>"F", "msg"=>apiSpeak($languageid, "Invalid Order Id"));
		}
		else{
			$order_id = $order_arr[0]['order_id'];
			$order_total = $order_arr[0]['amount'];
			if($order_total > 0){
				$order_information = json_decode($order_arr[0]['order_information'], true);
				$orderItems = array();
				foreach($order_information['order_contents_information'] as $order_contents){
					$orderItems[$order_contents['item_id']] = $order_contents['quantity'];
				}
			}
			else{
			    return array("error" => apiSpeak($languageid, "Zero order amount. So cannot proceed to Loyalty Points"), "status" => "F");
			}
		}

		$whereColumnArr= array("name" => $card_no, "dataname" => $dataname);
		//$result = CQ::getRowsWhereColumn('lavu_gift_cards', $whereColumnArr, '', '', "MAIN");
		$colsArray = array('id','expires','inactive','points');
		$result = CQ::getRowsWithRequiredColumnsMainDB('lavu_gift_cards',$colsArray,$whereColumnArr);

		$points_balance = 0;
		//echo "<pre>";print_r($rowsResult); exit;
		$resultArr = array();
		if(!empty($result))
		{
			$card_dbid = $result[0]['id'];
			$gift_expires = $result[0]['expires'];
			$inactive = $result[0]['inactive'];
			if($inactive == 1){
			    $resultArr = array("error" => apiSpeak($languageid, "Lavu Loyalty Number is inactive"), "status" => "F");
			}
			else{
				if(empty($gift_expires)){
					$expired = false;
				}
				else{
					$location_info = LocationDataController::action_getLocationInfo(1,$dataname); // fetching location info
					//echo "<pre>";print_r($location_info);
					$datetime = substr(LavuGiftCardDataController::DateTimeForTimeZone($location_info["timezone"]),0,-1);
					//$datetime = date("Y-m-d H:i:s");
					$expired = (strtotime($gift_expires) - strtotime($datetime)) < 0;
				}
				if($expired){
				    $resultArr = array("error" => apiSpeak($languageid, "Lavu Loyalty Number is expired"), "status" => "F");
				}
				else{
					$points_balance = $result[0]['points'] * 1;

					$discountTypesObj = DBConnection::clientQuery("SELECT * FROM `discount_types` WHERE id= '$discountId' AND `special`='lavu_loyalty' AND `_deleted`='0' AND `loyalty_cost`<= '".$points_balance."' AND `min_access`<='4'");
					$discount_types =  MA::resultToArray($discountTypesObj);
					if(!empty($result))
					{
						$discount_type_arr = array();
						foreach ($discount_types as $discount_type){
							$discount_value = '';
							$reward_use_or_unlock = $discount_type['loyalty_use_or_unlock'];
							$reward_cost = $discount_type['loyalty_cost'];
							$unlock_data='';
							if($reward_use_or_unlock=='2'){ $unlock_data = 'unlock'; }

							if($discount_type['code'] == 'p') {
								$tmp = floatval($discount_type['calc']) * 100;
								$reward_text = $tmp . "%";
								$discount_value = $order_total * floatval($discount_type['calc']);

							}
							else{
								$reward_text = $this->ext_display_money_with_symbol($discount_type['calc']);
								$discount_value = $discount_type['calc'];
							}
							$discount_label = '';
							if(!empty($discount_type['label']))
							{
								$discount_label = $discount_type['label'].": ";
							}
							$txt = "at ";
							if(!empty($unlock_data)){
								$txt = $unlock_data." at ";
							}
							$reward_text = $discount_label . $reward_text . " off ".$txt.$reward_cost . " points";

							$discount_type_arr[] = array("discount_id" => $discount_type['id'], "title" => $reward_text, "discount" => $discount_type['calc'], "discount_type" => $discount_type['code'], "discount_points" => $reward_cost,"discount_value" => $discount_value, "discount_p" => $tmp,'loyalty_use_or_unlock'=>$discount_type['loyalty_use_or_unlock']);
						}
						$resultArr['discounttypes'] = $discount_type_arr;
						$resultArr['status'] = 1;

						foreach($orderItemsDetails as $orderItemsDetail)
						{
							if($orderItemsDetail['itemid'] != '' && $orderItemsDetail['qty'] >= 1){

								$itemdetail = OrdersQueries::itemdetails($orderItemsDetail['itemid'], $orderItemsDetail['qty'],	$orderItemsDetail['fmods_checked'], $orderItemsDetail['omods_checked'], $orderItemsDetail['special']);
								$itemdetails[] = $itemdetail;
							}
						}
						$taxinform = OrdersDataController::displaytax($itemdetails,$tmp);
						$calc = OrdersDataController::caltotal($taxinform);
						$resultArr['discounttypes'][0]['discount_value'] = $calc['discount'];

					}
					else{
					    $resultArr['error'] = apiSpeak($languageid, "Invalid Discount Id");
						$resultArr['status'] = "F";
					}
					//echo "<pre>";print_r($resultArr); exit;

				}
			}
		}
		else{
		    $resultArr = array("error" => apiSpeak($languageid, "Lavu Loyalty Number doesnot exists"), "status" => "F");
		}

		return $resultArr;
	}

	public static function getDecimalData($id, $dataname){

		$whereColumnArr=array("id"=>$id);
		$columnNameArr=array("decimal_char","disable_decimal","rounding_factor","round_up_or_down");
		$res = CQ::getRowsWithRequiredColumns('locations',$columnNameArr,$whereColumnArr,'','',$dataname);
		return $res;
	}

	public static function decimalize($price) {
		if(self::$decimal_char == ",")
		{
				$price = str_replace(".",",",$price);
		}
		//error_log("price: ".$price);
		//error_log("decimal points".self::$disable_decimal);
		$price = number_format((float)$price, 2, '.', '');
		$price  = self::truncate_number($price, self::$disable_decimal);
		return $price;
	}

	public static function truncate_number( $number, $disable_decimal) {
		//echo $number." - ".$disable_decimal."/n";
		$number = trim($number);
		$disable_decimal = 0; //trim(disable_decimal);
		if (is_int($number) && ($disable_decimal == "" || $disable_decimal  == 0)) {
				return sprintf('%0.2f', $number); //$number;
		} elseif(!is_int($number) && !is_string($number) && ($disable_decimal == "" || $disable_decimal == 0)){
				return round($number, 2);  //$string[0].".".$extString;
		} elseif($disable_decimal != "" || $disable_decimal > 0){
				return sprintf('%0.2f', $number);
		} else {
			return $number;
		}
	 }

	public static function truncate_number_old( $number, $disable_decimal) {
		// Zero causes issues, and no need to truncate
		if (is_int($number) && ($disable_decimal == "" || $disable_decimal  == 0)) {
			//echo sprintf('%0.2f', $number);
			return sprintf('%0.2f', $number); //$number;
		} else{
			return $number;
		$rounding_factor = strtolower(self::$rounding_factor);
		$round_up_or_down = strtolower(self::$round_up_or_down);
		$oprice = $price;
		error_log("Rounding Factor:".$rounding_factor);
		 error_log("Rounding Dir:".$round_up_or_down);
		//echo $rounding_factor,"<br>";
		//echo $round_up_or_down,"<br>";

		if(!empty($rounding_factor) ){ // rounding applied
			$mod = fmod($price,$rounding_factor);
			error_log("mod value: ".$mod);
			if(!empty($round_up_or_down)){
				if($round_up_or_down == strtolower("Up")){
						$price = ($price - $mod + $rounding_factor);
				}
				else if($round_up_or_down == strtolower("Down")){
						$price = ($price - $mod);
				}
			}
			else{
				if ($mod >= ($rounding_factor / 2))
				{
						$price = ($price - $mod + $rounding_factor);
				}
				else
				{
						$price = ($price - $mod);
				}
			}
		}
		error_log(array("price" => $price, "rounding_amount" => abs($oprice-$price)), true);
		return array("price" => $price, "rounding_amount" => abs($oprice-$price));
		}
	}

	public static function saveFormatedCurrency($monitory_amount, $dont_use_monetary_symbol = false){
		global $location_info;
		$needToAddMinus = "";
		$number = $monitory_amount;
		if(strpos($number,",") != false && strpos($number,".") != false && (strpos($number,".") > strpos($number,","))){
			$number =str_replace(",","",$number);
		}elseif(strpos($number,",") != false && strpos($number,".") != false){
			$number =str_replace(".","",$number);
			$number =str_replace(",",".",$number);
		}
		if($location_info['decimal_char'] != ""){
			$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
		}

		$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
		$output = number_format($number, $precision, $decimal_char,'');
		return $output;
	}

	private function validatePaymentArgs($args) {

		$error = array();
		$languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;

		if(empty($args['order_id']))
		{
		    $error[] = apiSpeak($languageid, "Order Id is required");
		}

		if(empty($args['payment_method_nonce'])){
		    $error[] = apiSpeak($languageid, "Payment Method Nonce is required");
		}

		if(empty($args['dataname']))
		{
		    $error[] = apiSpeak($languageid, "Dataname is required");
		}

		if(empty($args['orderItemsDetails']))
		{
		    $error[] = apiSpeak($languageid, "Order Items Details are required");
		}

		if(empty($args['order_total']))
		{
		    $error[] = apiSpeak($languageid, "Order Amount is required");
		}

		if(empty($args['order_type']))
		{
		    $error[] = apiSpeak($languageid, "Order Type is required");
		}

		if(empty($args['email']))
		{
		    $error[] = apiSpeak($languageid, "Email Address is required");
		}

		if(empty($args['phoneNumber']))
		{
		    $error[] = apiSpeak($languageid, "Telephone Number is required");
		}

		if (!filter_var($args['email'], FILTER_VALIDATE_EMAIL)) {
		    $error[] = apiSpeak($languageid, "Invalid Email Address");
		}

		if (!in_array($args['order_type'], array(DELIVERY, PICKUP, DINE_IN))) {
		    $error[] = apiSpeak($languageid, "Invalid Order Type");
		}

		if ($args['order_type'] == DELIVERY) {
			if(!isset($args['customer_address']) || empty($args['customer_address']))
			{
			    $error[] = apiSpeak($languageid, "Customer Address is required");
			}
		}

		if($args['delivery_fee'] < 0){
		    $error[] = apiSpeak($languageid, "Invalid Delivery Fee");
		}

		return $error;
	}

	private function validateOrder($order, $languageid = 1) {
		if (empty($order) || empty($order['order_id'])) {
		    $error[] = apiSpeak($languageid, "Invalid Order Id");
		}
		if (!empty($order['transaction_information'])) {
			$transaction_information = json_decode($order['transaction_information'], true);
			if ($transaction_information['approved'] == 1) {
			    $error[] = apiSpeak($languageid, "Order is closed");
			}
		}

		return $error;
	}

	private function getMealPlanInfo($userId) {
		require_once('/home/poslavu/public_html/admin/cp/resources/mealplan_functions.php');
		$mealPlanUser = getUserByID($userId);
		$mealPlanInfo = array(
			'userId'          => $userId,
			'enabled'         => !$mealPlanUser['_deleted'],
			'badge_number'    => (empty($mealPlanUser['badge_id']) ? '?' : $mealPlanUser['badge_id']),
			'stipend_balance' => (double) getStipendBalanceForUser($mealPlanUser),
			'card_balance'    => (double) $mealPlanUser['cash_balance'],
		);
		return $mealPlanInfo;
	}

	private function isNewInventory($dataname) {
		$configs = CQ::getRowsWithRequiredColumns('config', array('value'), array('setting' => 'INVENTORY_MIGRATION_STATUS', '_deleted' => '0'), '', '', $dataname);
		$config = array_shift($configs);
		if ($config['value'] == 'Completed_Ready' || $config['value'] == 'Skipped_Ready') {
			return true;
		}
		return false;
	}

	private function updateInventoryQuantity($dataname, $args, $type = 'reserve', $action = 'add', $location_id = 1) {

		if (!empty($args['orderItemsDetails'])) {
			foreach ($args['orderItemsDetails'] as $key => $itemDetails) {
				if ($itemDetails['itemid'] != '') {
					/*
					 * patch added for to inventory don't track
					 */
					//$whereColumnArr = array('id' => $itemDetails['itemid'], '_deleted' => 0, 'ltg_display' => 1);
					//$menuItemsColNameArr = array("id", "inv_count", "track_86_count");
					//$menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnArr);
					//if($menuItemRowsResult[0]['track_86_count']!=''){
						$request['item_id'][$key]['id'] = $itemDetails['itemid'];
						$request['item_id'][$key]['quantity'] = $itemDetails['qty'];
					//}
					//else continue;
				}
				$fmods_checked = $itemDetails['fmods_checked'];
				if (!empty($fmods_checked)) {
					foreach($fmods_checked as $fmod) {
						$request['forced_modifier_id'][] = array('id' => $fmod,'quantity'=>$itemDetails['qty']);
					}
				}
				$omods_checked = $itemDetails['omods_checked'];
				if (!empty($omods_checked)) {
					foreach($omods_checked as $omod) {
						$request['optional_modifier_id'][] = array('id' => $omod,'quantity'=>$itemDetails['qty']);
					}
				}
			}
		}

		session_start();
		$_SESSION['poslavu_234347273_admin_dataname'] = $dataname;
		$_SESSION['poslavu_234347273_admin_location_id'] = $location_id;
		$count86ViewCompiler = new Inventory86CountViewCompiler();
		if ($type == 'reserve') {
			if ($action == 'removed') {
				$orderItems[0]['removed'] = $request;
				$orderItems[0]['removed']['action'] = '';
			} else {
				$orderItems[0]['added'] = $request;
			}
			$response = $count86ViewCompiler->updateReserveQuantity($orderItems);
		}

		if ($type == 'inventory') {
			if ($action == 'removed') {
				$orderItems[0]['removed'] = $request;
				$orderItems[0]['removed']['action'] = '';
			} else {
				$orderItems[0]['added'] = $request;
			}
			$response = $count86ViewCompiler->updateDeliverQuantity($orderItems);
		}
		return $response;
	}

	public static function getItemRow( $params, $lessSpace = false, $isBold = false ){
		$rowStylerType = 'line-height:120%;color:#000000;font-family:"Open Sans", Helvetica, Arial, sans-serif;padding-right: 20px;padding-left: 20px;padding-top: 10px;padding-bottom: 10px;';
		$lessPadding = '';
		if( $lessSpace !== false ){
			switch($lessSpace) {
				case 'stick-to-parent':
					$lessPadding = 'padding-top: 0px !important;padding-bottom: 0px !important;margin-top: -10px !important;';
				break;
				default:
					$lessPadding = 'padding-top: 4px !important;padding-bottom: 4px !important;';
				break;
			}
		}
		$bold = $isBold === true ? 'font-weight: bold;' : '';

		return '<div style="background-color:transparent;'.$bold.'">
	        <div class="block-grid mixed-two-up olo-table-row-container" style="margin: 0 auto;min-width: 320px;max-width: 620px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #FFFFFF;">
	          <div class="olo-table-row-item" style="border-collapse:collapse;display:table;width:100%;background-color:#FFFFFF;">
	            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="background-color:transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width: 620px;"><tr class="layout-full-width" style="background-color:#FFFFFF;"><![endif]-->

	                <!--[if (mso)|(IE)]><td align="center" width="413" style=" width:413px; padding-right: 0px; padding-left: 0px; padding-top:10px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
	              <div class="col num8 olo-table-row-item-one" style="display: table-cell;vertical-align: top;min-width: 320px;max-width: 408px;">
	                <div class="olo-item-data" style="background-color: transparent;width: 100% !important;padding-top: 8px;">
	                <!--[if (!mso)&(!IE)]><!-->
	                <div class="olo-item-row-enhancer" style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top:10px;padding-bottom:10px;padding-right: 10px;padding-left: 20px;'.$lessPadding.'"><!--<![endif]-->
	                      <div class="olo-div-maker">
	                      	<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 0px;"><![endif]-->
	                      	<div style="'.$rowStylerType.'">
	                      		<div class="olo-row-styler-insider" style="font-size:12px;line-height:14px;font-family:Open Sans, Helvetica, Arial, sans-serif;color:#000000;text-align:left;">
	                            <p style="margin: 0;font-size: 14px;line-height: 17px;">
	                              <span style="color: rgb(0, 0, 0);font-size: 14px;line-height: 16px;">
								  <a style="color:#71777D;text-decoration: none;color: rgb(0, 0, 0);white-space: pre-wrap;" href="javascript:void(0);" target="_blank" rel="noopener" data-mce-selected="1">'.$params[0].'</a>
	                              </span></p>
	                            </div>
	                      	</div>
	                      	<div class="olo-row-help-block" style="font-size: 12px;word-wrap: break-word;line-height: 12px;">'.$params[2].'</div>
	                      	<!--[if mso]></td></tr></table><![endif]-->
	                      </div>
	                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
	                </div>
	              </div>
	                <!--[if (mso)|(IE)]></td><td align="center" width="207" style=" width:207px; padding-right: 0px; padding-left: 0px; padding-top:10px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><![endif]-->
	              <div class="col num4 olo-table-row-item-two" style="display: table-cell;vertical-align: top;max-width: 320px;min-width: 204px;">
	                <div class="olo-table-row-item-first" style=" background-color: transparent;width: 100% !important;">
	                <!--[if (!mso)&(!IE)]><!-->
	                <div class="olo-table-row-item-second" style="border-top: 0px solid transparent;border-left: 0px solid transparent;border-bottom: 0px solid transparent;border-right: 0px solid transparent;padding-top:8px; padding-bottom:0px;padding-right: 0px;padding-left: 0px;"><!--<![endif]-->
	                      <div class="olo-div-maker">
	  	                     <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;'.$lessPadding.'"><![endif]-->
	                      	<div class="olo-price-row" style="line-height:120%;color:#000000;font-family:Open Sans, Helvetica, Arial, sans-serif; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 10px;'.$lessPadding.'">
	                      		<div class="olo-table-row-item-third" style="font-size:12px;line-height:14px;font-family:Open Sans, Helvetica, Arial, sans-serif;color:#000000;text-align:left;"><p style="margin: 0;font-size: 14px;line-height: 17px;" class="olo-price">'.$params[1].'</p></div>
	                      	</div>
	  	                     <!--[if mso]></td></tr></table><![endif]-->
	                      </div>
	                <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
	                </div>
	              </div>
	            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
	          </div>
	        </div>
      </div>';
	}

	public static function getMonitorySymbolSettings( $settings ){
		$symbolDetails = [
			'left' => $settings['left_or_right'] === 'left' ? $settings['monitary_symbol']: '',
			'right' => $settings['left_or_right'] === 'right' ? $settings['monitary_symbol']: '',
		];

		$commonQueries = new CommonQueries();
		$rowsResult = $commonQueries ->getRowsWithRequiredColumns('config',
			["value"],
			["setting" => "monetary_symbol_on_receipts"]
		);
		if(!empty($rowsResult) && $rowsResult[0]['value']!='1'){
		        $symbolDetails['left'] = "";
		        $symbolDetails['right'] = "";
		}

		return (object) $symbolDetails;
	}

	public static function parsePrice( $monitorySymbolSettings, $price )
	{
		return $monitorySymbolSettings->left.PaymentDataController::displayFormatedCurrency($price).$monitorySymbolSettings->right;
	}

	/* sending mail to user START */
	public static function sendMail($emailto, $messagearr) {
		$emailTemplate = file_get_contents ( '/home/poslavu/private_html/email_template/email_template.html');

		$monitorySymbolDetails = self::getMonitorySymbolSettings( $messagearr );
		$mon_left_sym = $monitorySymbolDetails->left;
		$mon_right_sym = $monitorySymbolDetails->right;

		$orderDate = '';
		$tipamount = '';
		$delivery_fee = '';

		$whereColumnArr = array ( "order_id" => $messagearr ['order_id'] );
		$ColNameArr = array ( "description" );

		$rowsResult = CQ::getRowsWithRequiredColumnsOtherDB ( 'orders',
			$ColNameArr, $whereColumnArr, '', '', "lavutogo"
		);

		if (! empty ( $rowsResult [0] ['description'] )) {
			$desc_time = json_decode ( $rowsResult [0] ['description'], true );

			$orderDate = empty ( $desc_time ['dining_date'] ) ? date ('d M Y')." ".$desc_time ['dining_time'] : date ( 'M d, Y',  strtotime($desc_time ['dining_date'])) . " " . $desc_time ['dining_time'];

			/********Tip work*******/
			$card_tip= "";
			$cash_tip = "";
			$tipamount = "";
			if ($desc_time ['tip_amount'] > 0) {
				$tipamount = self::getItemRow(['Tip amount Paid', self::parsePrice( $monitorySymbolDetails, $desc_time ['tip_amount']) ], true);
			}
			if(!empty($tipamount) && $desc_time['payment_type']=='card'){
				$card_tip = $tipamount;
			} else {
				$cash_tip = $tipamount;
				//$messagearr ['totalamount'] = $messagearr ['totalamount']-$desc_time ['tip_amount'];
			}
			/******************/
			$delivery_fee = "";
			if ($desc_time ['delivery_fee'] > 0) {
				$delivery_fee = self::getItemRow(['Delivery fees', self::parsePrice( $monitorySymbolDetails, $desc_time ['delivery_fee']) ], true);

			}
			//Discount : 50% Off At 10 Points
			$discount = '';
			if ($desc_time['discount_info']['discount_p'] > 0) {
				$discount = self::getItemRow([$desc_time['discount_info']['title'], self::parsePrice($monitorySymbolDetails, $desc_time['discount_info']['discount_value'] ) ], true);
			}
		}
		if ($messagearr ['taxamount'] > 0) {
			$messagearr ['taxamount'] = self::parsePrice( $monitorySymbolDetails , $messagearr ['taxamount'] );
		} else {
			$messagearr ['taxamount'] = '';
		}

		$taxProfileToHtml = [
			'etax' => '',
			'itax' => '',
		];

		$etax_disp = $itax_disp = '' ;
		$taxPrice = $iTaxPrice = 0 ;
		foreach ( $messagearr ['taxinform'] as $key => $taxtype ) {
			foreach ( $taxtype as $key1 => $taxval ) {
				foreach ( $taxval as $key2 => $tx_val ) {
					if($tx_val ['rate']>0){
						if($etax_disp === '' && $tx_val['type'] == 'etax'){
							$etax_disp = true;
						}

						if($itax_disp === '' && $tx_val['type'] == 'itax'){
							$itax_disp = true;
						}

						$formattedTaxTitle = $tx_val ['title'] . ' (' . $tx_val ['rate'] . ' of ' . self::parsePrice( $monitorySymbolDetails , $tx_val ['subtotal']) . ')';
						switch($tx_val['type']){
							case 'etax' :
							$taxPrice += $tx_val ['calculatedtaxamount'];
							$taxProfileToHtml['etax'] .= self::getItemRow([ $formattedTaxTitle , self::parsePrice( $monitorySymbolDetails ,$tx_val ['calculatedtaxamount'] ) ],true);
							break;

							case 'itax' :
							$iTaxPrice += $tx_val ['calculatedtaxamount'];
							$taxProfileToHtml['itax'] .= self::getItemRow([ $formattedTaxTitle , self::parsePrice( $monitorySymbolDetails ,$tx_val ['calculatedtaxamount'] ) ],true);
							break;

							default :
								// do nothing
							break;
						}
					}
				}
			}
		}

		if($etax_disp !== '' && $taxPrice !== 0 ){
			$etax_disp = self::getItemRow( ['Tax' ,  '' ], false, true);
			$etax_disp .= $taxProfileToHtml['etax'];
		}

		if($itax_disp !== '' && $iTaxPrice !== 0 ){
			$itax_disp = self::getItemRow( ['Included Tax' , '' ], false, true);
			$itax_disp .= $taxProfileToHtml['itax'];
		}

		$itemArray = [];
		foreach ($messagearr['orderItems'] as $itemdetail) {
			$itemargs = array( "menuId" => array(
				"id" => $itemdetail['item_id']),
				"dataname" => $messagearr['dataname'],
				"mId"=>$itemdetail['modifier_list_id'],
				"fId"=>$itemdetail['forced_modifier_list_id']
			);

			$itempayload = (object) ItemsDataController::action_getItemDetailHistory( $itemargs );
			foreach ($itempayload->Items as $Item) {
				$itemPrice = (float)$itemdetail['price'];
				$itemName = self::cleanString($itemdetail['item'].$itemdetail['options'].$itemdetail['moptions'], '');
				if (!isset($itemArray[$itemName])) {
					$itemArray[$itemName] = [
						'price' => $itemPrice,
						'count' => 1,
						'name' => $itemdetail['item']
					];
				} else {
					$itemArray[$itemName]['price'] += $itemPrice;
					$itemArray[$itemName]['count'] += 1;
				}
				$item_instruction = '';
				if (isset($itemdetail['special']) && !empty($itemdetail['special'])) {
					$item_instruction = '** '.$itemdetail['special'];
				}
				$itemArray[$itemName]['instructions'] = $item_instruction;

				if (!isset($itemArray[$itemName]['options']) && !empty($itemdetail['options'])) {
					$itemArray[$itemName]['options']['price'] = (float)$itemdetail['forced_modifiers_price'];

					$modifiersDescription = $itemdetail['options'];
					if(isset($itemdetail['use_pizza_creator']) && $itemdetail['use_pizza_creator']){
						//	Set text format for Pizza modifiers details
						$pizzaFields = PizzaCreator::calculateOrderContentsFields(explode(",", $itemdetail['forced_modifier_list_id']), $itemdetail['mods_builder_content'], true);
						$modifiersDescription = $pizzaFields->pizzaOptions;
					}

					$itemArray[$itemName]['options']['html'] = self::getItemRow([' - ' . $modifiersDescription, self::parsePrice($monitorySymbolDetails, (float)$itemdetail['forced_modifiers_price'])], 'stick-to-parent');
				}

				if (!isset($itemArray[$itemName]['moptions']) && !empty($itemdetail['moptions'])) {
					$itemArray[$itemName]['moptions']['price'] = (float)$itemdetail['modify_price'];
					$itemArray[$itemName]['moptions']['html'] = self::getItemRow([' * ' . $itemdetail['moptions'], self::parsePrice($monitorySymbolDetails, (float)$itemdetail['modify_price'])], 'stick-to-parent');
				}
			}
		}

		$orderItems = '';
		$subtotal = 0.00;
		foreach ($itemArray as $key => $value) {
			$itemTitle = $value['count'].' x&#160;' .$value['name'];
			$itemPrice = self::parsePrice($monitorySymbolDetails, $value['price']);
			$orderItems .= self::getItemRow([$itemTitle, $itemPrice , $value['instructions']]);
			if(isset($itemArray[$key]['options'])) {
				$subtotal += $itemArray[$key]['options']['price'];
				$orderItems .= $itemArray[$key]['options']['html'];
			}

			if(isset($itemArray[$key]['moptions'])) {
				$subtotal += $itemArray[$key]['moptions']['price'];
				$orderItems .= $itemArray[$key]['moptions']['html'];
			}

			$subtotal += (float)$value['price'];
		}

		$whereDataNameColumnArr = array("data_name" => $messagearr['dataname']);
		$restaurantsDataNameNameArr = array("logo_img");
		$restaurantDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants',
			$restaurantsDataNameNameArr,
			$whereDataNameColumnArr, '',''
		);

		$location_logo = 'https://admin.poslavu.com/images/'.$messagearr['dataname'].'/'.$restaurantDataNameRowsResult[0]['logo_img'];
		$subtotal = self::parsePrice($monitorySymbolDetails, $subtotal);

		$total_amount = self::parsePrice($monitorySymbolDetails,$messagearr ['totalamount']);

		if ($desc_time ['order_type'] == 'dinein') {
			$desc_time ['order_type'] = 'Dine-in';
		} else {
			$desc_time ['order_type'] = ucfirst($desc_time ['order_type']);
		}

		$grandTotalRow = '';
		if (isset($messagearr['rounding_amount']) && $messagearr['rounding_amount'] !== 0) {
			$grandTotalRow .= self::getItemRow( ['Rounding adjustment',  self::parsePrice($monitorySymbolDetails, $messagearr['rounding_amount'])], false, false);
		}
		$grandTotalRow .= self::getItemRow( ['Total', $total_amount],false,true);
		$switchKeys = array(
			'{{user_name}}' => $messagearr['customer'],
			'{{lavutogo_name}}' => $messagearr ['store_title'],
			'{{mode_of_deliver}}' => $desc_time ['order_type'],
			'{{order_id}}' => $messagearr ['order_id'],
			'{{order_time}}' => $orderDate,
			'{{order_details}}' => $orderItems,
			'{{itax}}' => $itax_disp,
			'{{order_subtotal}}' => self::getItemRow( ['Subtotal',  $subtotal], true, true),
			'{{etax}}' => $etax_disp,
			'{{card_tip}}' => $card_tip,
			'{{cash_tip}}' => $cash_tip,
			'{{delivery_fee}}' => $delivery_fee,
			'{{total_amount_raw}}' => $total_amount,
			'{{total_amount}}' => $grandTotalRow,
			'{{store_image}}' => $location_logo,
			'{{discount_amount}}' => $discount,
			'{{restaurant_address}}' => $messagearr['restaurant_address'],
			'{{restaurant_contact_number}}' => $messagearr['restaurant_contact_number'],
		);

		$emailcontent = str_replace(
			array_keys( $switchKeys ),
			$switchKeys,
			$emailTemplate
		);

		if(isset($messagearr ['delivery_instructions']) && !empty($messagearr ['delivery_instructions']))
		{
			$delivery_instruction = '<div class="col num12"><div class="olo-row-styler" style="line-height:120%;color:#000000;font-family:Open Sans, Helvetica, Arial, sans-serif;padding-right: 20px;padding-left: 20px;padding-top: 10px;padding-bottom: 10px;"><strong>Special Instructions </strong><br>'.$messagearr ['delivery_instructions'].'</div></div>';
			$emailcontent = str_replace('{{delivery_instruction}}',$delivery_instruction,$emailcontent);
		}
		else{
			$emailcontent = str_replace('{{delivery_instruction}}','',$emailcontent);
		}
		header('Content-type: text/html;');
		$from = 'Lavu To Go <noreply@lavutogoreceipt.com>';
		$headers = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: ' . $from . "\r\n" . 'X-Mailer: PHP/' . phpversion ();
		$subject = "Order from ".$messagearr['store_title'];
		$result = mail ( $emailto, $subject, $emailcontent, $headers );
		return $result;
	}

	/* sending mail to user END */
	/*
	 * get forcedmodifier detais from forcedmodifiers, forcedmodifiers list
	 */
	function getForcedModifierDetialsbyId($id){
		if (empty($id)) return null;
		$query = "select f.id, f.cost, f.title, f.list_id,flist.type from forced_modifiers as f join forced_modifier_lists as flist on f.list_id=flist.id where f.id=".$id." and f._deleted=0";
		$resQry = DBConnection::clientQuery($query);
		$result =  MA::resultToArray($resQry);
		return $result;
	}
	function inventoryCheckWhenCheckout($args)
	{
	    $languageid = (isset($args['languageid']) && $args['languageid'] != '') ? $args['languageid'] : 1;
		$outofStockItems = array();
		$outofStockForcedModifiers = array();
		$ordercolumnArr = array("order_id","consumer_id","amount","order_information", "created_at");
		$whereColumnArr = array('order_id'=>$args['order_id']);
		$orderItemRowsResult = CQ::getRowsWithRequiredColumnsOtherDB('orders', $ordercolumnArr, $whereColumnArr, "", "", "lavutogo");
		$orderData = json_decode($orderItemRowsResult[0]['order_information'],true);
		//print_r($orderData);
		if(isset($orderData['order_contents_information']) && count($orderData['order_contents_information'])>0){
			$fmid_arr = array();
			$omid_arr = array();
			$itemid_list = array();
			$items_data = array();
			$inv_items_list = array();
			$perItem = array();
			foreach($orderData['order_contents_information'] as $itemdetail)
			{
				$inv_items_str =  "";
				$whereColumnArr = array('id' => $itemdetail['item_id'], '_deleted' => 0, 'ltg_display' => 1);
				$menuItemsColNameArr = array("id", "inv_count", "track_86_count");
				$menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnArr);
				if($menuItemRowsResult[0]['track_86_count']!=''){
					$itemres = $this->getIngradiantsByIdArr(array($itemdetail['item_id']),"I");
					if(!empty($itemres)){
						$inv_items_list = array_merge($inv_items_list,explode(",",$itemres));
						$inv_items_str = $itemres.",";
					}
					if($itemdetail['forced_modifier_list_id'] != ""){
						$fm_itemres = $this->getIngradiantsByIdArr(explode(',',$itemdetail['forced_modifier_list_id']),"F");
						if(!empty($fm_itemres)) {
							$inv_items_list = array_merge($inv_items_list,explode(",",$fm_itemres));
							$inv_items_str .= $fm_itemres.",";
						}
					}
					if($itemdetail['modifier_list_id'] != ""){
						$om_itemres = $this->getIngradiantsByIdArr(explode(',',$itemdetail['modifier_list_id']),"M");
						if(!empty($om_itemres)){
							$inv_items_list = array_merge($inv_items_list,explode(",",$om_itemres));
							$inv_items_str .= $om_itemres.",";
						}
					}
					$itemsQtyArr[] = array($itemdetail['item_id']=>array_count_values($inv_items_list));
					$itemid_list[] = $itemdetail['item_id'];
					$items_data[$itemdetail['item_id']]=$itemdetail['item'];
					$perItem[$itemdetail['item_id']] = $inv_items_str;

					if($itemdetail['forced_modifier_list_id'] != ""){
						$fmlist = explode(',',$itemdetail['forced_modifier_list_id']);
						$fmid_arr[$itemdetail['item_id']] = $fmlist;
					}
					if($itemdetail['modifier_list_id'] != ""){
						$omlist = explode(',',$itemdetail['modifier_list_id']);
						$omid_arr[$itemdetail['item_id']] = $omlist;
					}
				}

			}
			$items_count_arr = array_count_values($itemid_list);
			if(!empty($items_count_arr))
			{
				foreach($items_count_arr as $item_id=>$itemqty){
					$itkey = array_count_values($inv_items_list);//$this->searchByKey($itemsQtyArr,$item_id);
					//print_R($itkey);
					$invertory_status = $this->getItemInventoryStatus($item_id,$itkey,$itemqty,$perItem[$item_id]);
					if(!empty($invertory_status) && $invertory_status['stock'] == "continue"){
						$instock = $invertory_status['instock'];
						$outofStockItems[] = array("itemId"=>$item_id,"itemName"=>$items_data[$item_id],"notavailable"=>"I","instock"=>$instock);
					}
					else if(!empty($fmid_arr)){
						$fmdata = $fmid_arr[$item_id];
						if(count($fmdata)>0){
							$outstockModifiers='';
							$outstockModifierNames =array();
							$fm_stock_arr= array();
							$fm_count_arr = array_count_values($fmdata);
							foreach($fm_count_arr as $fm_id=>$mQty){
								$forcedModifierListRowResult = $this->getForcedModifierDetialsbyId($fm_id);
								if(!empty($forcedModifierListRowResult) && $forcedModifierListRowResult[0]['type']=='choice'){
									//echo $itemqty*$mQty;
									$returnforcedModifier =  $this->getForcedModifierInventoryStatus($fm_id,$itkey,$itemqty*$mQty,$perItem[$item_id]);
									if(!empty($returnforcedModifier)&& $returnforcedModifier['stock'] == "continue"){
										$outstockModifiers = $fm_id.",";
										$outstockModifierNames = $forcedModifierListRowResult[0]['title'].",";
										$fm_stock_arr[] = array($fm_id=>$returnforcedModifier['instock']);
									}
								}
							}
							if(count($fm_stock_arr)>0){
								$outofStockItems[] = array("itemId"=>$item_id,"itemName"=>$items_data[$item_id],"notavailable"=>"M",'instock'=>$fm_stock_arr);
							}
						}
					}
					if(!empty($invertory_status) && $invertory_status['stock'] != "continue" && !empty($omid_arr)){
						$omdata = $omid_arr[$item_id];
						if(count($omdata)>0){
							$om_stock_arr= array();
							$om_count_arr = array_count_values($omdata);
							foreach($om_count_arr as $om_id=>$mQty){
									$returnforcedModifier =  $this->getModifierInventoryStatus($om_id,$itkey,$itemqty*$mQty,$perItem[$item_id]);
									if(!empty($returnforcedModifier)&& $returnforcedModifier['stock'] == "continue"){
										$om_stock_arr[] = array($fm_id=>$returnforcedModifier['instock']);
									}
								}
								if(count($om_stock_arr)>0){
									$outofStockItems[] = array("itemId"=>$item_id,"itemName"=>$items_data[$item_id],"notavailable"=>"M",'instock'=>$om_stock_arr);
								}
							}
						}
					}
				}
			}
			if(count($outofStockItems)>0 || $$outofStockForcedModifiers>0){
			    return array("status" => "F", "outofstock" => $outofStockItems, "msg" => apiSpeak($languageid, "Some items are out of stock"));
			}
		}
		function getModifierInventoryStatus($itemId,$invdata,$itemQty,$needQtydata){
			$needQtyArr = explode(',',rtrim($needQtydata,","));
			$needQty = count($needQtyArr);
			$continue = "forward";
			$instock_count = array();
			$inv_res = $this->check86InventoryByType($itemId, $invdata,$itemQty,$needQty,"M");
			$continue = $inv_res['stock'];
			$instock_count= $inv_res['inv_count'];
			//return $continue;
			return ($continue=='continue')? array('stock'=>$continue,'instock'=>(min($instock_count))): array('stock'=>$continue);
		}
		function getModifierDetialsbyId($id){
			if (empty($id)) return null;
			$query = "select o.id, o.cost, o.title, o.category,olist.type from modifiers as o join modifier_categories as olist on o.category=olist.id where o.id=".$id." and o._deleted=0";
			$resQry = DBConnection::clientQuery($query);
			$result =  MA::resultToArray($resQry);
			return $result;
		}

/*
* display Currency format
	 * */
	public static function displayFormatedCurrency($monitory_amount, $dont_use_monetary_symbol = false){
		global $location_info;
		$needToAddMinus = "";
		$number = $monitory_amount;

		$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
		$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
		$thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char']==".") ? "," : '.';
		$thousands_sep = ",";
		if( !empty($thousands_char) ) {
			$thousands_sep = $thousands_char;
		}
		if(strpos($number,"-") !== false){
			$find_minus = explode("-", $number);
			$needToAddMinus = "-";
		}
		$monitary_symbol = isset($location_info['monitary_symbol']) ? htmlspecialchars($location_info['monitary_symbol']) : '';
		$left_or_right = isset($location_info['left_or_right']) ? $location_info['left_or_right'] : 'left';

		$left_side = "";
		$right_side = "";

		if (!$dont_use_monetary_symbol) {
			if ($left_or_right == 'left') {
				$left_side = $monitary_symbol;
			} else {
				$right_side = $monitary_symbol;
			}
		}
		$output = number_format($number, $precision, $decimal_char, $thousands_sep);
		//echo $output;
		return $output;
	}
	function toGetOrderTypeIdBaseOnOrderType($order_type){
		$order_type = strtolower(str_replace(array('-',' '), array('',''), $order_type));
		$query = "SELECT `id` from `order_tags` WHERE REPLACE(LOWER(`name`),' ','') = '".$order_type."'";
		$resultObj = DBConnection::clientQuery($query);
		$result =  MA::resultToArray($resultObj);
		return $result[0]['id'];
	}

	/*LP-5042
	 * updateMenuitemCountInventory
	 */
	function updateMenuitemCountInventory($orderItemsDetails){
	    $itemsInfo=array();
	    foreach($orderItemsDetails as $key => $value){
	        $itemsInfo[$value['itemid']] += $value['qty'];
	    }

	    foreach($itemsInfo as $itemId => $itemCount){
	        $item_rowResult = CQ::getRowsWithRequiredColumns('menu_items', array("inv_count","track_86_count"), array("id" => $itemId));
	        if($item_rowResult[0]['track_86_count'] == '86count' && $item_rowResult[0]['inv_count'] > 0){
	            $inv_count = $item_rowResult[0]['inv_count'];
	            $count86 = $inv_count - $itemCount;
	            if($count86 < 0){
	                $count86 = 0;
	            }
	            $menuItem_86update = array('inv_count' => $count86);
	            CQ::updateArray ('menu_items', $menuItem_86update, "where `id` = '" . $itemId . "'" );
	        }
	    }
	}
	/*
	 * LP-5660 - new inventory methods to check availability when checkout
	 *
	 */
	function getIngradiantsByIdArr($idArray,$type){
		switch ($type){
			case "I":
				$whereColumn = 'menuItemID';
				$tablename = 'menuitem_86';
				break;
			case "F":
				$whereColumn = 'forced_modifierID';
				$tablename = 'forced_modifier_86';
				break;
			case "M":
				$whereColumn = 'modifierID';
				$tablename = 'modifier_86';
				break;
		}
		$inList = implode(",", $idArray);
		$query = "SELECT group_concat(mi.`inventoryItemID`) as invid FROM `$tablename` mi WHERE mi.`$whereColumn` IN ($inList) AND  mi.`_deleted` != 1";
		$result = DBConnection::clientQuery($query);
		$return_resp = MarshallingArrays::resultToArray($result);
		return $return_resp[0]['invid'];
	}

	function getItemInventoryStatus($itemId,$invdata,$itemQty,$needQtydata){
		$needQtyArr = explode(',',rtrim($needQtydata,","));
		$needQty = count($needQtyArr);
		$whereColumnArr = array('id' => $itemId, '_deleted' => 0, 'ltg_display' => 1);
		$menuItemsColNameArr = array("id", "inv_count", "track_86_count");
		$menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnArr);
		$continue = "forward";
		$instock_count = array();
		if(!empty($menuItemRowsResult)) {
			foreach ($menuItemRowsResult as $val){
				if($menuItemRowsResult[0]['track_86_count'] == '86count' && $itemQty !='' && $menuItemRowsResult[0]['inv_count']<$itemQty){
					$continue = "continue";
					$instock_count[] = $menuItemRowsResult[0]['inv_count'];
				}
				elseif($menuItemRowsResult[0]['track_86_count'] == '86count' && $menuItemRowsResult[0]['inv_count'] <= 0){
					$continue = "continue";
					$instock_count[] = $menuItemRowsResult[0]['inv_count'];
				}elseif($menuItemRowsResult[0]['track_86_count'] == "86countInventory"){
					//print_R($invdata); echo "need---".$needQty;
					$inv_res = $this->check86InventoryByType($itemId,$invdata,$itemQty,$needQty,"I");
				//echo "Item--".	print_r($inv_res);
					$continue = $inv_res['stock'];
					$instock_count= $inv_res['inv_count'];
				}
			}
		}
		$response = ($continue=='continue')? array('stock'=>$continue,'instock'=>(min($instock_count))): array('stock'=>$continue);
		return $response;
	}

	function getForcedModifierInventoryStatus($itemId,$invdata,$itemQty,$needQtydata){
		$needQtyArr = explode(',',rtrim($needQtydata,","));
		$needQty = count($needQtyArr);
		$continue = "forward";
		$instock_count = array();
		$inv_res = $this->check86InventoryByType($itemId, $invdata,$itemQty,$needQty,"F");
		$continue = $inv_res['stock'];
		$instock_count= $inv_res['inv_count'];
		//return $continue;
		return ($continue=='continue')? array('stock'=>$continue,'instock'=>(min($instock_count))): array('stock'=>$continue);
	}
	function check86InventoryByType($itemId,$invdata,$itemQty,$needQty,$type){
	//echo "need--".$needQty."---".$itemId;
		$continue = "forward";
		$instock_count = array();
		//print_r($invdata);
		if(!empty($invdata)){
			foreach ($invdata as $id=>$invQty){//echo $invQty;
				$itemReqQty = $invQty;
				$menuItemRowsResult = $this->getItemInvDataByIdType($id,$itemId,$type);
				//print_r($menuItemRowsResult);
				if(!empty($menuItemRowsResult)&&count($menuItemRowsResult)>0){
					if($itemReqQty >= 1){//print_R($menuItemRowsResult);
						if($menuItemRowsResult[0]['quantityOnHand'] - (($menuItemRowsResult[0]['quantityUsed'] * $itemReqQty) + $menuItemRowsResult[0]['reservation']) >= 0){
							if(($menuItemRowsResult[0]['quantityOnHand'] - (($menuItemRowsResult[0]['quantityUsed'] * $itemReqQty) + $menuItemRowsResult[0]['reservation'])) < $menuItemRowsResult[0]['lowQuantity']){
								$continue = "continue";
								$instock_count[] = intval(($menuItemRowsResult[0]['quantityOnHand']-($menuItemRowsResult[0]['lowQuantity']+$menuItemRowsResult[0]['reservation']))/($needQty*$menuItemRowsResult[0]['quantityUsed']));
							}
						}
						else{
							$continue = "continue";
							$instock_count[] = intval(($menuItemRowsResult[0]['quantityOnHand']-($menuItemRowsResult[0]['lowQuantity']+$menuItemRowsResult[0]['reservation']))/($needQty*$menuItemRowsResult[0]['quantityUsed']));
						}
					}
					else{
						$continue = "continue";
						$instock_count[] = intval(($menuItemRowsResult[0]['quantityOnHand']-($menuItemRowsResult[0]['lowQuantity']+$menuItemRowsResult[0]['reservation']))/($needQty*$menuItemRowsResult[0]['quantityUsed']));
					}
				}
			}
		}
		return array('stock'=>$continue,'inv_count'=>$instock_count);
	}
	function getItemInvDataByIdType($invId,$menuItemid,$type){
		switch ($type){
			case "I":
				$whereColumn = 'menuItemID';
				$tablename = 'menuitem_86';
				break;
			case "F":
				$whereColumn = 'forced_modifierID';
				$tablename = 'forced_modifier_86';
				break;
			case "M":
				$whereColumn = 'modifierID';
				$tablename = 'modifier_86';
				break;
		}
		$query = "SELECT mi.`$whereColumn` as 'objectID', mi.`reservation`, mi.`quantityUsed`, mi.`unitID` as '86Unit',
		it.`id` as 'inventoryItemID', it.`quantityOnHand`, it.`salesUnitID`, it.`lowQuantity`, it.`reOrderQuantity`
		FROM `inventory_items` it
		JOIN `$tablename` mi ON mi.`inventoryItemID` = it.`id`
		WHERE mi.`$whereColumn` = '".$menuItemid."' and mi.`inventoryItemID` = $invId
		AND it.`_deleted` != 1
		AND mi.`_deleted` != 1";
		$result = DBConnection::clientQuery($query);
		return MarshallingArrays::resultToArray($result);
	}

	public function generateOriginalId($args)
	{
		$user_id = isset($args['user_id'])?$args['user_id']:0;
		$orderType = isset($args['order_type'])?trim($args['order_type']):'';
		$orderType = str_replace('-','',strtolower($orderType));
		$email = isset($args['email_id'])?$args['email_id']:'';
		$phoneNumber = isset($args['phone_number'])?$args['phone_number']:'';
		$first_name = isset($args['first_name'])?$args['first_name']:'';
		$last_name = isset($args['last_name'])?$args['last_name']:'';
		$street_address = isset($args['streetAddress'])?$args['streetAddress']:'';
		$dining_date = $args['dining_date'];
		$dining_time = $args['dining_time'];
		$city = isset($args['city'])?$args['city']:'';
		$state = isset($args['state'])?$args['state']:'';
		$postalCode = isset($args['postalCode'])?$args['postalCode']:'';
		$country = isset($args['country'])?$args['country']:'';
		$infoArr = array();
		$infoArr['First_Name'] = $first_name;
		$infoArr['Last_Name'] = $last_name;
		$infoArr['Email'] = $email;
		$infoArr['Phone_Number'] = $phoneNumber;
		$infoArr['is_delivering'] = '1';
		$infoArr['delivery_time'] = $dining_date." ".$dining_time;
		$infoArr['customer_id'] = $user_id;
		$infoArr['address'] = array();
		$infoArr['address']['street'] = $street_address;
		$infoArr['address']['city'] = $city;
		$infoArr['address']['state'] = $state;
		$infoArr['address']['zip'] = $postalCode;
		$infoArr['address']['country'] = $country;
		$infoArr['Street_Address'] = $street_address;
		$infoArr['City'] = $city;
		$infoArr['State'] = $state;
		$infoArr['Zip_Code'] = $postalCode;

		$deliveryInfo = $orderType === 'delivery' ? $infoArr : [];
		$originalIDArr = array(
			'deliveryInfo' => $deliveryInfo,
			'print_info' => $this->getPrintInfo($infoArr)
		);

		$deliveryInfoJSONStr = json_encode($originalIDArr);
		$originalId = "19|o|Customer|o|$first_name $last_name|o|$user_id|o|$deliveryInfoJSONStr";

		$customerInfo = array();
		$customerInfo['f_name'] = $first_name;
		$customerInfo['l_name'] = $last_name;
		$customerInfo['date_created'] = $dining_date." ".$dining_time;
		$customerInfo['locationid'] = "1";
		$customerInfo['last_activity'] = $dining_date." ".$dining_time;
		$customerInfo['id'] = $user_id;
		$customerInfo['field3'] = $street_address;
		$customerInfo['field2'] = "";
		$customerInfo['field1'] = "";
		$customerInfo['field4'] = $city;
		$customerInfo['field5'] = $state;
		$customerInfo['field6'] = $postalCode;
		$customerInfo['field7'] = "";
		$customerInfo['field8'] = "";
		$customerInfo['field9'] = "";
		$customerInfo['field10'] = "";
		$customerInfo['field11'] = "";
		$customerInfo['field12'] = "";
		$customerInfo['field13'] = "";
		$customerInfo['field14'] = "";
		$customerInfo['field15'] = "";
		$customerInfo['waiver_expiration'] = "";
		$customerInfo['field16'] = "";
		$customerInfo['_deleted'] = "0";
		$customerInfo['field17'] = "";
		$customerInfo['field18'] = "";
		$customerInfo['field19'] = "";
		$customerInfo['field20'] = "";
		$customerInfo['field21'] = "";
		$customerInfo['field22'] = "";

		$customerInfoJson = json_encode($customerInfo);

		$originalId .="|o|$customerInfoJson";
		return $originalId;
	}

	private function getPrintInfo($customerData) {
		$printInformation = [];
		$lookFor = [
			'First_Name',
			'Last_Name',
			'Email',
			'Phone_Number',
			'Street_Address',
			'Street_Address',
			'City',
			'State',
			'Zip_Code'
		];
		foreach ($customerData as $key => $data) {
			if (in_array($key, $lookFor)) {
				if (strtolower($key) === "street_address") {
					$key = 'Street_Address_1';
				}
				if (!empty($data)) {
					$printInformation[$key] = [
						"field" => str_replace("_", " ", $key),
						"fields_db_name" => $key,
						"print" => 'b',
						"info" => $data
					];
				}
			}
		}

		return array_values($printInformation);
	}

	public static function cleanString($string, $replace = '-') {
		$string = str_replace(' ', $replace, $string); // Replaces all spaces with hyphens.
		return strtolower(preg_replace('/[^A-Za-z0-9\-.]/', $replace, $string)); // Removes special chars.
	}
}
