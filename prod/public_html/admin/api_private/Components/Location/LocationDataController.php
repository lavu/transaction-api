<?php
require_once( __DIR__.'/../../../../inc/olo/braintree/BraintreeConnect.php');
class LocationDataController extends DataControllerBase {
	public function __construct($request, $dataname) {
		parent::__construct(new ReflectionClass(__CLASS__),$request,$dataname);

		if ($dataname != 'lavutogo') {
			$res = self::getDecimalData(1, $dataname);
			global $location_info;
			$location_info['decimal_char'] = $res['decimal_char'];
			$location_info['disable_decimal'] = $res['disable_decimal'];
			$location_info['monitary_symbol'] = $res['monitary_symbol'];
			$location_info['left_or_right'] = $res['left_or_right'];
		}
	}
	protected function action_getlavutogoLocationAddress($args) {
		$lavu_togo_name = $args['lavu_togo_name'];
		$whereDataNameColumnArr = array("lavu_togo_name" => $lavu_togo_name['lavu_togo_name']);
		$restaurantsDataNameNameArr = array("id", "lavu_togo_name", "dataname", "restaurantid", "locationid");
		$restaurantDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsDataNameNameArr, $whereDataNameColumnArr, '','','MAIN');
		$companyDetails = array();
		foreach ($restaurantDataNameRowsResult as $restaurantData) {
			$companyDetails [] = $this->getLocationAddressCompanySettings($restaurantData);
		}
		$data['dataname']=$companyDetails[0]['dataname'];
		require_once(__DIR__ .'/../../../../inc/papi/jwtPapi.php');
		$jwtPapiObj = new jwtPapi($data);
		$companyDetails[0]['enc_dataname'] = $jwtPapiObj->genEncryptedDataname();
		return $companyDetails;
	}


	// function for retrive store LOCATION BY PARTICULAR ADDRESS

	protected function action_getLocationAddress($args)
	{
		$lavu_togo_name = $args['lavu_togo_name'];

		$whereDataNameColumnArr = array("lavu_togo_name" => $lavu_togo_name['lavu_togo_name']);
		$restaurantsDataNameNameArr = array("id", "lavu_togo_name", "dataname", "restaurantid", "locationid");
		$restaurantDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsDataNameNameArr, $whereDataNameColumnArr, '');
		$companyDetails == array();
		foreach ($restaurantDataNameRowsResult as $restaurantData) {
			$companyDetails [] = $this->getLocationAddressCompanySettings($restaurantData);
		}
		$restaurantDataNameRowsResult = $companyDetails;
		if(!empty($restaurantDataNameRowsResult)) {

			$whereColumnArr = array("id" => $restaurantDataNameRowsResult[0]['restaurantid']);
			$restaurantsAddrColNameArr = array("chain_id");
			$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereColumnArr, '');


			if (!empty($restaurantAddrChainRowsResult) && $restaurantAddrChainRowsResult[0]['chain_id']) {

				$restId = $restaurantAddrChainRowsResult[0]['chain_id'];

				if ($restId != '') {
					$whereAddrColumnArr = array("chain_id" => $restId);
					$restaurantsAddrColNameArr = array("id",);
					$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereAddrColumnArr, '');

					foreach ($restaurantAddrChainRowsResult as $val) {

						$whereAddrColumnArr = array("restaurantid" => $val['id']);
						$restaurantsAddrColNameArr = array("id", "lavu_togo_name", "dataname", "restaurantid", "locationid");
						$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsAddrColNameArr, $whereAddrColumnArr, '');
						$companyDetails = array();
						foreach ($restaurantAddrChainRowsResult as $restaurantData) {
							$companyDetails [] = $this->getLocationAddressCompanySettings($restaurantData);
						}
						$restaurantAddrChainRowsResult = $companyDetails;
						if (!empty($restaurantAddrChainRowsResult)) {
							$locationArr[] = $restaurantAddrChainRowsResult[0];
						}

					}
				}
			}

			if (!empty($locationArr)) {
				$resultArr = array("address" => $locationArr);
			} else {
				$resultArr = array("address" =>$restaurantDataNameRowsResult[0]);
			}
		}

		return $resultArr;
	}


	protected function action_getDeliveryAddress($args)
	{
		$streetAddress = $args['streetAddress'];
		$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&address=".$streetAddress['streetAddress'];
		$url = str_replace(' ', '%20', $url);
		$ch = curl_init($url);
		// define options
		$optArray = array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true
		);
		// apply those options
		curl_setopt_array($ch, $optArray);
		// execute request and get response
		$result = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result,true);
		if ($response['status'] == "ZERO_RESULTS"){
			return $result = array("Status"=>"F", "msg"=>"Not a valid address");
		}
		$destinationLat = $response['results'][0]['geometry']['location']['lat'];
		$destinationLng = $response['results'][0]['geometry']['location']['lng'];
		$restaurantDataNameRowsResult = LocationQueries::restaurantLocationsDetails($args['dataname']);

		if (
			isset($restaurantDataNameRowsResult) &&
			isset($restaurantDataNameRowsResult[0]['timezone']) &&
			!empty($restaurantDataNameRowsResult[0]['timezone'])
		) {
			$timestamp = time();
			$date = new DateTime("now" , new DateTimeZone($restaurantDataNameRowsResult[0]['timezone']));
			date_timestamp_set($date, $timestamp);
			$currentDate = date_format($date, 'H:i');
  		}

		$whereConfigDataNameColumnArr = array('ltg_delivery_radius', 'ltg_delivery_min', 'minimum_notice_time_2');
		$restaurantsConfigDataNameNameArr = array("setting");
		$restaurantConfigDataNameRowsResult = CQ::getRowsWhereColumnIn('config', "setting", $whereConfigDataNameColumnArr, $restaurantDataNameRowsResult[0]['dataname']);
		$minimumOrderNoticeTime = 0;
		foreach ($restaurantConfigDataNameRowsResult as $getKey=>$maxVal)
			{
				if ($maxVal['setting'] === 'minimum_notice_time_2') {
				 	$minimumOrderNoticeTime = $maxVal['value'];
				}
			}
			//addting of minimum Order Notice Time into current time
 		$totalDeliveryTime = date("H:i", strtotime($currentDate . "+".$minimumOrderNoticeTime."minutes"));
		if(!empty($restaurantDataNameRowsResult)) {
			$whereColumnArr = array("id" => $restaurantDataNameRowsResult[0]['restaurantid']);
			$restaurantsAddrColNameArr = array("chain_id");
			$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereColumnArr, '');
			//if (!empty($restaurantAddrChainRowsResult) && $restaurantAddrChainRowsResult[0]['chain_id']) {
			//restId = $restaurantAddrChainRowsResult[0]['chain_id'];
			if (!empty($restaurantDataNameRowsResult)) {
				//print_r($restaurantDataNameRowsResult);
				$whereAddrColumnArr = array();
				/*if ($restaurantAddrChainRowsResult[0]['chain_id']){
					$whereAddrColumnArr['chain_id'] = $restaurantAddrChainRowsResult[0]['chain_id'];
				}else{*/
					$whereAddrColumnArr['id'] = $restaurantDataNameRowsResult[0]['restaurantid'];
				//}
				if ($whereAddrColumnArr != '') {
					$restaurantsAddrColNameArr = array("id",'data_name');
					$restaurantResChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereAddrColumnArr, '');
					$sequence = 0;
					foreach ($restaurantResChainRowsResult as $val) {
						unset($optArray);
						unset($result);
						unset($response);
						unset($whereAddrColumnArr);
						unset($restaurantsAddrColNameArr);
						unset($restaurantAddrChainRowsResult);
						$whereAddrColumnArr = array("restaurantid" => $val['id']);
						$restaurantsAddrColNameArr = array("id", "dataname", "restaurantid", "locationid", "lavu_togo_name", "title", "address", "city", "state","country", "zip");
						$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsAddrColNameArr, $whereAddrColumnArr, '');
						if (!empty($restaurantAddrChainRowsResult) && $restaurantAddrChainRowsResult[0]['lavu_togo_name'] != '') {
							$locationAddress = $restaurantAddrChainRowsResult[0]['address'].", ".$restaurantAddrChainRowsResult[0]['city'].", ".$restaurantAddrChainRowsResult[0]['state'];

							DBConnection::startClientConnection($val['data_name']);
							DBConnection::startClientConnection($val['data_name']);
							$whereConfigDataNamesColumnArr = array('ltg_delivery_radius','ltg_max_distance_miles');
							$restaurantConfigDataNamesRowsResult = CQ::getRowsWhereColumnIn('config', "setting", $whereConfigDataNamesColumnArr, $val['data_name']);
							$allowedMile = 1;
						if(isset($restaurantConfigDataNamesRowsResult) && !empty($restaurantConfigDataNamesRowsResult)) {
							foreach ($restaurantConfigDataNamesRowsResult as $getKey=>$maxVal)
							{
								if ($maxVal['setting'] === 'ltg_max_distance_miles')
								{
									$maxDeliveryMile = $maxVal['value'];
								}
							}
						}
							$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&address=".$locationAddress;
							$url = str_replace(' ', '%20', $url);
							$ch = curl_init($url);
							// define options
							$optArray = array(
									CURLOPT_URL => $url,
									CURLOPT_RETURNTRANSFER => true
							);
							// apply those options
							curl_setopt_array($ch, $optArray);
							// execute request and get response
							$result = curl_exec($ch);
							curl_close($ch);
							$response = json_decode($result,true);
							$sourceLat = $response['results'][0]['geometry']['location']['lat'];
							$sourceLng = $response['results'][0]['geometry']['location']['lng'];
							unset($optArray);
							unset($result);
							unset($response);
							$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&origins=".$sourceLat.",".$sourceLng."&destinations=".$destinationLat.",".$destinationLng."&mode=driving&TrafficModel=optimistic&sensor=false&language=en-EN&units=imperial";
							$url = str_replace(' ', '%20', $url);
							$ch = curl_init($url);
							// define options
							$optArray = array(
									CURLOPT_URL => $url,
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_SSL_VERIFYPEER => false
							);
							// apply those options
							curl_setopt_array($ch, $optArray);
							// execute request and get response
							$result = curl_exec($ch);
							curl_close($ch);
							$response = json_decode($result,true);
							$distance = $response['rows'][0]['elements'][0]['distance']['value'];
							if ($maxDeliveryMile < round(($distance/1609.34), 2))
							{
								$allowedMile = 0;
							}
							if ($response['rows'][0]['elements'][0]['status'] == "OK" && $allowedMile === 1) {
								$duration = $response['rows'][0]['elements'][0]['duration']['value'];
								$i=0;
								$location_radius_fees = json_decode($restaurantConfigDataNamesRowsResult[0]['value'],true);
								krsort($location_radius_fees);
								$last_key = end(array_keys($location_radius_fees));
								foreach($location_radius_fees as $key=>$val){
									$val = preg_replace('/[^a-zA-Z0-9_.]/', '', $val);
								    if ($key <= (int)($distance/1609.34) || $last_key == $key) {
										  $distance += $sequence;
										  $duration = round($duration/60);
										  $delivery_time = date("H:i", strtotime($totalDeliveryTime . "+".$duration."minutes"));
										  $distance_cal = round(($distance/1609.34),2);
										  $restaurantAddrChainRowsResult[0]['sourcelat'] = $sourceLat;
										  $restaurantAddrChainRowsResult[0]['sourcelng'] = $sourceLng;
										  $distanceUnit = ( $distance_cal <= 1 ) ? " mile" : " miles";
										  $restaurantAddrChainRowsResult[0]['delivery_distance'] = $distance_cal.$distanceUnit;
										  $restaurantAddrChainRowsResult[0]['delivery_time'] = date("h:i A", strtotime($delivery_time));
										  $restaurantAddrChainRowsResult[0]['delivery_fees'] = (int)$val;
										  $locationSource[$distance] = $restaurantAddrChainRowsResult[0];
										  unset($whereConfigDataNamesColumnArr);
										  unset($restaurantConfigDataNamesRowsResult);
										  $sequence+=1;
										  break;
										}
								unset($distanceArr);
								unset($durationArr);
								unset($location_radius_fees);
								unset($key);
								$i++;

							}
						}
					}
				}
					$locationDestinationArr[] = array("destinationaddress"=>$streetAddress['streetAddress'], "destinationlat"=>$destinationLat, "destinationlng"=>$destinationLng);
				}
			}
			ksort($locationSource);
			$locationSourceArr = array_values($locationSource);
			if (!empty($locationSource)) {
				$resultArr = array("sourceaddress"=>$locationSourceArr, "destinationaddress" => $locationDestinationArr);
			} else {
				$resultArr = array("sourceaddress"=> array(), "destinationaddress" =>array());
			}
		}
		return $resultArr;
	}

	protected function action_getDataName($args) {
		$lavu_togo_name = $args['lavu_togo_name'];
		$whereColumns = array("lavu_togo_name" => $lavu_togo_name['lavu_togo_name']);
		$selectColumns = array("dataname", "restaurantid", "locationid", "title", "address", "city", "state", "zip", "country", "phone", "email", "lavu_togo_name");
		$locationDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $selectColumns, $whereColumns);
		if (empty($locationDataNameRowsResult)){
			return array("status"=>"F", "msg"=>"Lavutogoname doesn't exists");
		}
		return $locationDataNameRowsResult;
	}

	protected function action_getDataNameDetails($args) {
		$lavu_togo_name = $args['lavu_togo_name'];
		$whereColumns = array("lavu_togo_name" => $lavu_togo_name['lavu_togo_name']);
		$selectColumns = array("dataname", "restaurantid", "locationid", "title", "address", "city", "state", "zip", "country", "phone", "email", "lavu_togo_name");
		$locationDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $selectColumns, $whereColumns);
		if (empty($locationDataNameRowsResult)){
			return array("status"=>"F", "msg"=>"Lavutogoname doesn't exists");
		}
		return $locationDataNameRowsResult;
	}

	public static function action_getLocationInfo($id, $dataname) {
		$res = CQ::getRowByID('locations',$id, $dataname);
		$res[dataname] = $dataname;
		$res = self::getLocationAddressCompanySettings($res);
		return $res;
	}

	private function getBraintreeToken($config) {
		if (!empty($config) && isset($config['paypal'])) {
			$braintreeConnect = new BraintreeConnect($config['paypal']);
			if ($braintreeConnect->hasError) {
				return false;
			}
			return $braintreeConnect->getClientToken();
		} else {
			return false;
		}
	}

	protected function action_getLocationConfig($args) {
		$commonQuerries = new CommonQueries();
		$lavuToGoName = $args['lavutogoname'];
        $dataname = $args['dataname'];
		if (!$commonQuerries->isLavuToGoEnabled()) {
			return ["status"=> "F", "msg"=> "Lavutogo is not enabled"];
		}
		// Use lavu_togo_name to get dataname from `restaurant_locations` table
		$columnILocationNameArr = array(
			"locationid",
			"dataname"
		);
		$whereLocationDataNamesColumnArr = array(
			'dataname' => $dataname,
			'lavu_togo_name' => $lavuToGoName
		);

		$restaurantLocationDataNamesRowsResult = $commonQuerries->getRowsWithRequiredColumnsMainDB(
			'restaurant_locations',
			$columnILocationNameArr,
			$whereLocationDataNamesColumnArr
		);

		$locationId = $restaurantLocationDataNamesRowsResult[0]['locationid'];

		// Get config values from `locations` table
		$whereColumnArr = array("id"=> $locationId);
		$columnNameArr = array(
			"monitary_symbol",
			"left_or_right",
			"decimal_char",
			"ltg_payment_info",
			"disable_decimal",
			"rounding_factor",
			"round_up_or_down",
			"timezone"
		);
		$res = CQ::getRowsWithRequiredColumns(
			'locations',
			$columnNameArr,
			$whereColumnArr,
			'',
			'',
			$dataname
		);

		// Get config values from `restaurants` table
		$columnRestaurantsArr = array("logo_img", "company_name", "modules");
		$whereRestaurantsColumnArr = array("data_name" => $dataname);
		$restautrantsRowResult = $commonQuerries->getRowsWithRequiredColumnsMainDB(
			'restaurants',
			$columnRestaurantsArr,
			$whereRestaurantsColumnArr
		);
		// Put allowed config settings in location config return value
		$configArr = array();
		$allowedConfigArr = array(
		   "max_advance_days_2",
		   "minimum_notice_time_2",
		   "enable_togo_polling_2",
		   "ltg_phone_number_2",
		   "ltg_phone_carrier_2",
		   "ltg_email_2",
		   "ltg_tip",
		   "ltg_dine_in_option_2",
		   "min_delivery_amount",
		   "ltg_language_pack",
		   "lavu_togo_days_open",
		   "primary_currency_code",
		   "ltg_delivery_fees",
		   "ltg_minimum_order_price",
		   "lavu_togo_theme",
		   "ltg_delivery_time",
		   "ltg_delivery_option",
		   "ltg_start_end_time",
		   "ltg_minimum_delivery_amount",
		   "ltg_timezone",
		   "ltg_on_checkout_display_restaurant_info",
		   "ltg_on_checkout_success_message",
		   "ltg_on_embed_typform_feedback",
		   "ltg_google_analytics_tracking_id",
		   "ltg_google_tag_management_id",
		   "ltg_treat_site_single_location",
		   "ltg_payment_option_2",
		   "enable_allow_asap_orders_2",
		   "manage_item_availability",
		);
		$pay_info = [];
		$clientToken = false;
		$paypal_status = 0;
		if(isset($res[0]) && !empty($res[0]['ltg_payment_info'])) {
			$pay_info = json_decode($res[0]['ltg_payment_info'],true);
			$paypal_status = isset($pay_info['paypal']['status']) ? $pay_info['paypal']['status'] : 0;

			if((int)$paypal_status === 1) {
				$clientToken = $this->getBraintreeToken($pay_info);
			}
		}

		$configArr = array();
		$columnNameArr = array("setting", "value");
		$reqWhere = "in:".implode(":", $allowedConfigArr);
		$whereColumnArr = array("setting" => $reqWhere, "type" => "location_config_setting", "_deleted"=> "0");
		$result = $commonQuerries->getRowsWithRequiredColumns('config', $columnNameArr, $whereColumnArr, '', '', $dataname);
		foreach($result as $confVal) {
			$val = $confVal['setting'];
			if ($val == 'ltg_start_end_time') {
				$configArr[$val] = trim($confVal['value']);
				if ($configArr[$val]) {
					$ltgRestaurantTimings = json_decode($configArr[$val], true);
					foreach ($ltgRestaurantTimings as $day => $data) {
						if (isset($ltgRestaurantTimings[$day]['starttime'])) {
							unset($ltgRestaurantTimings[$day]['starttime']);
							if ($ltgRestaurantTimings[$day]['endtime']) {
								unset($ltgRestaurantTimings[$day]['endtime']);
							}
							$ltgRestaurantTimings[$day][0] = $data;
						} else {
							$ltgRestaurantTimings[$day] = $data;
						}
					}
					$configArr[$val] = json_encode($ltgRestaurantTimings);
				}
			} else {
				$configArr[$val] = $confVal['value'];
			}
		}
		if (isset($configArr['min_delivery_amount'])) {
			$configArr['min_delivery_amount'] = self::displayFormatedCurrency($configArr['min_delivery_amount']);
		}
		if (isset($configArr['ltg_minimum_order_price'])) {
			$configArr['ltg_minimum_order_price'] = self::displayFormatedCurrency($configArr['ltg_minimum_order_price']);
		}
		$configArr['monitary_symbol'] = $res[0]['monitary_symbol'];
		$configArr['monitory_symbol_position'] = $res[0]['left_or_right'];
		$configArr['decimal_separator'] = $res[0]['decimal_char'];
		$configArr['disable_decimal'] = $res[0]['disable_decimal'];
		$configArr['rounding_factor'] = $res[0]['rounding_factor'];
		$configArr['round_up_or_down'] = $res[0]['round_up_or_down'];
		$configArr['ltg_timezone'] = isset($res[0]['timezone']) ? $res[0]['timezone'] : '';
		$configArr['title'] = $restautrantsRowResult[0]['company_name'];
		$configArr['logo_img'] = "https://admin.poslavu.com/images/".$restaurantLocationDataNamesRowsResult[0]['dataname']."/".$restautrantsRowResult[0]['logo_img'];
		$configArr['locationid'] = $restaurantLocationDataNamesRowsResult[0]['locationid'];
		$configArr['max_advance_days'] = $configArr['max_advance_days_2'];
		$configArr['minimum_notice_time'] = $configArr['minimum_notice_time_2'];
		$configArr['enable_togo_polling'] = $configArr['enable_togo_polling_2'];
		$configArr['enable_allow_asap_orders'] = $configArr['enable_allow_asap_orders_2'];
		$configArr['manage_item_availability'] = ($configArr['manage_item_availability'] === '1');
		$configArr['ltg_phone_number'] = $configArr['ltg_phone_number_2'];
		$configArr['ltg_phone_carrier'] = $configArr['ltg_phone_carrier_2'];
		$configArr['ltg_email'] = $configArr['ltg_email_2'];
		$configArr['ltg_dine_in_option'] = $configArr['ltg_dine_in_option_2'];
		$configArr['lavu_gift_card_status'] = strpos( $restautrantsRowResult[0]['modules'], "extensions.payment.lavu.+loyalty" ) ? 'true' : 'false';

		// Put paypal config in location config return value
		if(!empty($pay_info)) {
			foreach($pay_info as $pmnt => $pm_methodval) {
				if($pmnt === 'paypal') {
					$new_paymnt_arr[$pmnt]['paypal_token'] = $clientToken;
					if($clientToken !== false && (int)$paypal_status === 1) {
						$new_paymnt_arr[$pmnt]['paypal_token'] = $clientToken;
						$new_paymnt_arr[$pmnt]['paypal_status'] = 1;
					} else {
						$new_paymnt_arr[$pmnt]['paypal_status'] = 0;
					}
				} else {
					$new_paymnt_arr[$pmnt] = $pm_methodval;
				}
			}
			$configArr['ltg_payment_info'] = $new_paymnt_arr;
		} else {
			$configArr['ltg_payment_info'] = "";
		}
		if(!isset($configArr['ltg_on_checkout_display_restaurant_info'])) {
			$configArr['ltg_on_checkout_display_restaurant_info'] = 1;
		}

		if (!isset($configArr['ltg_on_checkout_display_restaurant_info'])) {
			$configArr['ltg_on_checkout_display_restaurant_info'] = 1;
		}

		if (!isset($configArr['ltg_on_checkout_success_message'])) {
			$configArr['ltg_on_checkout_success_message'] = LTG_ON_ORDER_CHECKOUT_MESSAGE;
		}

		$paymentOptions = array(
			'prepay_only' => false,
			'payment_upon_receipt_only' => false,
			'prepay_and_payment_upon_receipt' => false,
		);
		if (isset($configArr['ltg_payment_option_2'])) {
			switch ($configArr['ltg_payment_option_2']) {
				case '1':
					$paymentOptions['prepay_only'] = true;
					break;
				case '2':
					$paymentOptions['payment_upon_receipt_only'] = true;
					break;
				case '3':
					$paymentOptions['prepay_and_payment_upon_receipt'] = true;
					break;

			}
		}

		$configArr['ltg_payment_option_2'] = $paymentOptions;
		return $configArr;
	}

	protected function action_getDeliveryLocationFees($args){
		$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&address=".$args['streetAddress']['locationAddress'];
		$url = str_replace(' ', '%20', $url);
		$ch = curl_init($url);
		$optArray = array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result,true);
		$sourceLat = $response['results'][0]['geometry']['location']['lat'];
		$sourceLng = $response['results'][0]['geometry']['location']['lng'];
		$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&address=".$args['streetAddress']['streetAddress'];
		$url = str_replace(' ', '%20', $url);
		$ch = curl_init($url);
		$optArray = array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result,true);
		$destinationLat = $response['results'][0]['geometry']['location']['lat'];
		$destinationLng = $response['results'][0]['geometry']['location']['lng'];
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&origins=".$sourceLat.",".$sourceLng."&destinations=".$destinationLat.",".$destinationLng."&mode=driving";
		$url = str_replace(' ', '%20', $url);
		$ch = curl_init($url);
		$optArray = array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false
		);
		curl_setopt_array($ch, $optArray);
		$result = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result,true);
		if ($response['rows'][0]['elements'][0]['status'] == "OK") {
			$distance = $response['rows'][0]['elements'][0]['distance']['value'];
			$duration = $response['rows'][0]['elements'][0]['duration']['value'];
			$whereConfigDataNamesColumnArr = array('ltg_delivery_radius');
			$restaurantConfigDataNamesRowsResult = CQ::getRowsWhereColumnIn('config', 'setting', $whereConfigDataNamesColumnArr, $args['dataname']);
			$location_radius_fees[] = json_decode($restaurantConfigDataNamesRowsResult[0]['value'],true);
			foreach ($location_radius_fees[0] as $key=>$val){
				$key = $key*1000;
				if ($distance <= $key){
					return $deliveryfees[] = array("delivery_fees"=>$val);
					if ($taxdeta['title'] != ''){
						$displayTax[]= $taxdeta;
					}
				}
				return $deliveryfees[] = array("delivery_fees"=>0);  // OLO TO DO : seems like this should be outside one curly bracket
			}
		}
	}

	public static function getDecimalData($loc_id, $dataname){
		$whereColumnArr = array('id' => $loc_id);
		$columnNameArr = array('decimal_char', 'disable_decimal', 'left_or_right', 'monitary_symbol', 'rounding_factor', 'round_up_or_down');
		$res = CQ::getRowsWithRequiredColumns('locations', $columnNameArr, $whereColumnArr, '', '', $dataname);
		return array_shift($res);
	}


	public static function displayFormatedCurrency($monitory_amount, $dont_use_monetary_symbol = false){
		global $location_info;
		$needToAddMinus = "";
		$number = $monitory_amount;

		$precision = isset($location_info['disable_decimal']) ? $location_info['disable_decimal'] : 2;
		$decimal_char = isset($location_info['decimal_char']) ? $location_info['decimal_char'] : '.';
		$thousands_char = (isset($location_info['decimal_char']) && $location_info['decimal_char']==".") ? "," : '.';
		$thousands_sep = ",";
		if ( !empty($thousands_char) ) {
			$thousands_sep = $thousands_char;
		}
		if (strpos($number,"-") !== false){
			$find_minus = explode("-", $number);
			$needToAddMinus = "-";
		}
		$monitary_symbol = isset($location_info['monitary_symbol']) ? htmlspecialchars($location_info['monitary_symbol']) : '';
		$left_or_right = isset($location_info['left_or_right']) ? $location_info['left_or_right'] : 'left';

		$left_side = "";
		$right_side = "";

		if (!$dont_use_monetary_symbol) {
			if ($left_or_right == 'left') {
				$left_side = $monitary_symbol;
			} else {
				$right_side = $monitary_symbol;
			}
		}
		$output = number_format((float)$number, $precision, $decimal_char, $thousands_sep);
		return $output;
	}

	protected function action_getPickUpDineInLocationAddress($args)
	{
		$streetAddress = $args['streetAddress'];
		$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&address=".$streetAddress['streetAddress'];
		$url = str_replace(' ', '%20', $url);
		$ch = curl_init($url);
		// define options
		$optArray = array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true
		);
		// apply those options
		curl_setopt_array($ch, $optArray);
		// execute request and get response
		$result = curl_exec($ch);
		curl_close($ch);
		$response = json_decode($result,true);
		$destinationLat = $response['results'][0]['geometry']['location']['lat'];
		$destinationLng = $response['results'][0]['geometry']['location']['lng'];
		$restaurantDataNameRowsResult = LocationQueries::restaurantLocationsDetails($args['dataname']);
		if (
			isset($restaurantDataNameRowsResult) &&
			isset($restaurantDataNameRowsResult[0]['timezone']) &&
			!empty($restaurantDataNameRowsResult[0]['timezone'])
		) {
			$timestamp = time();
			$date = new DateTime("now" , new DateTimeZone($restaurantDataNameRowsResult[0]['timezone']));
			date_timestamp_set($date, $timestamp);
			$currentDate = date_format($date, 'H:i');
  		}//end
		$dataname = $restaurantDataNameRowsResult[0]['dataname'];
		DBConnection::startClientConnection($dataname);
		$whereConfigDataNameColumnArr = array('ltg_delivery_radius', 'ltg_delivery_min','minimum_notice_time_2','ltg_treat_site_single_location');
		$restaurantsConfigDataNameNameArr = array("setting");
		$restaurantConfigDataNameRowsResult = CQ::getRowsWhereColumnIn('config', "setting", $whereConfigDataNameColumnArr, $dataname);
		$minimumOrderNoticeTime = 0;
		$totalDeliveryTime= 0;
		$treatAsSingleLocation = false;
		if(!empty($restaurantConfigDataNameRowsResult)) {
		foreach ($restaurantConfigDataNameRowsResult as $getKey=>$maxVal)
			{
				if ($maxVal['setting'] === 'minimum_notice_time_2') {
				 	$minimumOrderNoticeTime = $maxVal['value'];
				}
				if($maxVal['setting'] === 'ltg_treat_site_single_location' && $maxVal['value'] == '1') {
					$treatAsSingleLocation = true;
				}
			}
 		}
 		//addting of minimum Order Notice Time into current time
 		$totalDeliveryTime = date("H:i", strtotime($currentDate ."+".$minimumOrderNoticeTime."minutes"));
		$resultArr = array();
		$locationSource = array();
		if (!empty($restaurantDataNameRowsResult)) {
			$whereColumnArr = array("id" => $restaurantDataNameRowsResult[0]['restaurantid']);
			$restaurantsAddrColNameArr = array("chain_id");
			$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereColumnArr, '');
			if (!empty($restaurantDataNameRowsResult)) {
				$whereAddrColumnArr = array();
				if($restaurantAddrChainRowsResult[0]['chain_id'] && !$treatAsSingleLocation){
					$whereAddrColumnArr['chain_id'] = $restaurantAddrChainRowsResult[0]['chain_id'];
				}else{
					$whereAddrColumnArr['id'] = $restaurantDataNameRowsResult[0]['restaurantid'];
				}
				if ($whereAddrColumnArr != '') {
					$sequence = 1;
					$restaurantsAddrColNameArr = array("id",'data_name');
					$restaurantResChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereAddrColumnArr, '');
					foreach ($restaurantResChainRowsResult as $val) {
						$whereConfigDataNameColumnArr = array('use_lavu_togo_2');
						$restaurantsConfigDataNameNameArr = array("setting");
						$restaurantConfigDataNameRowsResult = CQ::getRowsWhereColumnIn('config', "setting", $whereConfigDataNameColumnArr, $val['data_name']);
 						if(isset($restaurantConfigDataNameRowsResult[0]) && $restaurantConfigDataNameRowsResult[0]['value'] == '1'){
							unset($optArray);
							unset($result);
							unset($response);
							unset($whereAddrColumnArr);
							unset($restaurantsAddrColNameArr);
							unset($restaurantAddrChainRowsResult);
							$whereAddrColumnArr = array("restaurantid" => $val['id']);
							$restaurantsAddrColNameArr = array("id", "dataname", "restaurantid", "locationid", "lavu_togo_name", "title", "address", "city", "state", "country", "zip");
							$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsAddrColNameArr, $whereAddrColumnArr, '');
							$companyDetails = array();
							if(!empty($restaurantAddrChainRowsResult)) {
							foreach ($restaurantAddrChainRowsResult as $restaurantData) {
								$companyDetails [] = $this->getLocationAddressCompanySettings($restaurantData);
							}
							$restaurantAddrChainRowsResult =  $companyDetails;
						}
						if (!empty($restaurantAddrChainRowsResult) && $restaurantAddrChainRowsResult[0]['lavu_togo_name'] != '') {
							$locationAddress = $restaurantAddrChainRowsResult[0]['address'].", ".$restaurantAddrChainRowsResult[0]['city'].", ".$restaurantAddrChainRowsResult[0]['state'];
							DBConnection::startClientConnection($val['data_name']);
							$whereConfigDataNamesColumnArr = array('ltg_delivery_radius');
							$restaurantConfigDataNamesRowsResult = CQ::getRowsWhereColumnIn('config', "setting", $whereConfigDataNamesColumnArr, $val['data_name']);
							$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&address=".$locationAddress;
							$url = str_replace(' ', '%20', $url);
							$ch = curl_init($url);
							// define options
							$optArray = array(
									CURLOPT_URL => $url,
									CURLOPT_RETURNTRANSFER => true
							);
							// apply those options
							curl_setopt_array($ch, $optArray);
							// execute request and get response
							$result = curl_exec($ch);
							curl_close($ch);
							$response = json_decode($result,true);
							$sourceLat = $response['results'][0]['geometry']['location']['lat'];
							$sourceLng = $response['results'][0]['geometry']['location']['lng'];
							unset($optArray);
							unset($result);
							unset($response);
							$url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&origins=".$sourceLat.",".$sourceLng."&destinations=".$destinationLat.",".$destinationLng."&mode=driving&TrafficModel=optimistic&sensor=false&language=en-EN&units=imperial";
							$url = str_replace(' ', '%20', $url);
							$ch = curl_init($url);
							// define options
							$optArray = array(
									CURLOPT_URL => $url,
									CURLOPT_RETURNTRANSFER => true,
									CURLOPT_SSL_VERIFYPEER => false
							);
							// apply those options
							curl_setopt_array($ch, $optArray);
							// execute request and get response
							$result = curl_exec($ch);
							curl_close($ch);
							$response = json_decode($result,true);
							if ($response['rows'][0]['elements'][0]['status'] == "OK") {
								$distance = $response['rows'][0]['elements'][0]['distance']['value'];
								$duration = $response['rows'][0]['elements'][0]['duration']['value'];
								$distance += $sequence;
								$duration = round($duration/60);
								$delivery_time = date("H:i", strtotime($totalDeliveryTime . "+".$duration."minutes"));
								$distance_cal = round(($distance/1609.34),2);
								$restaurantAddrChainRowsResult[0]['sourcelat'] = $sourceLat;
								$restaurantAddrChainRowsResult[0]['sourcelng'] = $sourceLng;
								$distanceUnit = ( $distance_cal <= 1 ) ? " mile" : " miles";
								$restaurantAddrChainRowsResult[0]['delivery_distance'] = $distance_cal.$distanceUnit;
								$restaurantAddrChainRowsResult[0]['delivery_time'] = date("h:i A", strtotime($delivery_time));
								$locationSource[$distance] = $restaurantAddrChainRowsResult[0];
								unset($whereConfigDataNamesColumnArr);
								unset($restaurantConfigDataNamesRowsResult);
								$sequence+=1;
							}
						}
					}
				}
					$locationDestinationArr[] = array("destinationaddress"=>$streetAddress['streetAddress'], "destinationlat"=>$destinationLat, "destinationlng"=>$destinationLng);
				}
			}
			ksort($locationSource);
			$locationSourceArr = array_values($locationSource);
			if (!empty($locationSource)) {
				$resultArr = array("sourceaddress"=>$locationSourceArr, "destinationaddress" => $locationDestinationArr);
			} else {
				$resultArr = array("sourceaddress"=> array(), "destinationaddress" =>array());
			}
		}
		return $resultArr;
	}

	protected function action_getLocationAddressByLavutogoName($args)
	{
		$whereDataNameColumnArr = array("dataname" => $args['dataname']);
		$restaurantsDataNameNameArr = array("id", "dataname", "restaurantid", "locationid", "title", "address", "city", "state", "country", "zip");
		$restaurantDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsDataNameNameArr, $whereDataNameColumnArr, '');
		$dataname = $args['dataname'];
		DBConnection::startClientConnection($dataname);
		$resultArr = array();
		$locationSource = array();
		if (!empty($restaurantDataNameRowsResult)) {
			$whereColumnArr = array("id" => $restaurantDataNameRowsResult[0]['restaurantid']);
			$restaurantsAddrColNameArr = array("chain_id");
			$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereColumnArr, '');
			if (!empty($restaurantDataNameRowsResult)) {
				$whereAddrColumnArr = array();
				if ($restaurantAddrChainRowsResult[0]['chain_id']){
					$whereAddrColumnArr['chain_id'] = $restaurantAddrChainRowsResult[0]['chain_id'];
				}else{
					$whereAddrColumnArr['id'] = $restaurantDataNameRowsResult[0]['restaurantid'];
				}
				if ($whereAddrColumnArr != '') {
					$restaurantsAddrColNameArr = array("id",'data_name');
					$restaurantResChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsAddrColNameArr, $whereAddrColumnArr, '');
					foreach ($restaurantResChainRowsResult as $val) {
						unset($whereAddrColumnArr);
						unset($restaurantsAddrColNameArr);
						unset($restaurantAddrChainRowsResult);
						$whereAddrColumnArr = array("restaurantid" => $val['id']);
						$restaurantsAddrColNameArr = array("id", "dataname", "restaurantid", "locationid", "lavu_togo_name", "title", "address", "city", "state", "country", "zip");
						$restaurantAddrChainRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsAddrColNameArr, $whereAddrColumnArr, '');
						$companyDetails = array();
						foreach ($restaurantAddrChainRowsResult as $restaurantData) {
							$companyDetails [] = $this->getLocationAddressCompanySettings($restaurantData);
						}
						$restaurantAddrChainRowsResult= $companyDetails;
						if (!empty($restaurantAddrChainRowsResult) && $restaurantAddrChainRowsResult[0]['lavu_togo_name'] != '') {
							$locationAddress = $restaurantAddrChainRowsResult[0]['address'].", ".$restaurantAddrChainRowsResult[0]['city'].", ".$restaurantAddrChainRowsResult[0]['state'];
							DBConnection::startClientConnection($val['data_name']);
							$whereConfigDataNamesColumnArr = array('ltg_dine_in_option_2');
							$restaurantConfigDataNamesRowsResult = CQ::getRowsWhereColumnIn('config', "setting", $whereConfigDataNamesColumnArr, $val['data_name']);

							$url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCCqQ2sLa9lwXoRJ7IDVEAN0unQEu81C4k&address=".$locationAddress;
							$url = str_replace(' ', '%20', $url);
							$ch = curl_init($url);
							// define options
							$optArray = array(
									CURLOPT_URL => $url,
									CURLOPT_RETURNTRANSFER => true
							);
							// apply those options
							curl_setopt_array($ch, $optArray);
							// execute request and get response
							$result = curl_exec($ch);
							curl_close($ch);
							$response = json_decode($result,true);
							$sourceLat = $response['results'][0]['geometry']['location']['lat'];
							$sourceLng = $response['results'][0]['geometry']['location']['lng'];
							unset($optArray);
							unset($result);
							unset($response);

							$restaurantAddrChainRowsResult[0]['diningOption'] = $restaurantConfigDataNamesRowsResult[0]['value'];
							$restaurantAddrChainRowsResult[0]['sourcelat'] = $sourceLat;
							$restaurantAddrChainRowsResult[0]['sourcelng'] = $sourceLng;
							$locationSource[] = $restaurantAddrChainRowsResult[0];
							unset($whereConfigDataNamesColumnArr);
							unset($restaurantConfigDataNamesRowsResult);
						}
					}
				}
			}
			ksort($locationSource);
			$locationSourceArr = array_values($locationSource);
			if (!empty($locationSource)) {
				$resultArr = array("restuarentLocations"=>$locationSourceArr);
			} else {
				$resultArr = array("restuarentLocations"=> array());
			}
		}
		return $resultArr;
	}

	protected function action_getMealPlanInfo($args) {
		require_once(__DIR__.'/../../../cp/resources/mealplan_functions.php');
		$mealPlanInfo = isset($args) ? mealPlanEnable($args['dataname'], $args['userId']) : '';
		return $mealPlanInfo;
	}

	public static function getLocationAddressCompanySettings($return) {
		$dataname = $return[dataname];
		$whereDataNameColumnArr = array("data_name" => $dataname);
		$restaurantsCompanyNameArr = array("id", "company_name");
		$restaurantDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurants', $restaurantsCompanyNameArr, $whereDataNameColumnArr, '', '', 'MAIN');
		$return[title] = $restaurantDataNameRowsResult[0][company_name];
		$columnNameArr = array('setting', 'value');
		$whereColumnArr = array('business_address', 'business_city', 'business_state', 'business_country', 'business_zip', 'business_emailid', 'business_phone');
		$configData = CQ::getRowsWhereColumnIn('config', 'setting', $whereColumnArr, $dataname, $columnNameArr);
		foreach ($configData as $data) {
			if ($data[value] != '') {
				if ($data[setting]== 'business_emailid') {
					$return[email] = $data[value];
				} else {
					$return[substr($data[setting],'9')] = $data[value];
				}
			}
		}
		return $return;
	}

	/**
	 * This function is used to get language pack details.
	 * @param array $args
	 * @return array language pack details.
	 */
	protected function action_getlavutogoLanguagePack($args) {
		$whereDataNameColumnArr = array("lavu_togo_name" => $args['lavu_togo_name']);
		$restaurantsDataNameNameArr = array("id", "lavu_togo_name", "dataname", "restaurantid", "locationid");
		$restaurantDataNameRowsResult = CQ::getRowsWithRequiredColumnsMainDB('restaurant_locations', $restaurantsDataNameNameArr, $whereDataNameColumnArr, '','','MAIN');
		$languagePackDetails = array();
		foreach ($restaurantDataNameRowsResult as $restaurantData) {
			$languagePackDetails[] = $this->getLanguagePackDetails($restaurantData);
		}

		return $languagePackDetails;
	}

	/**
	 * This function is used to get language pack details.
	 * @param array $return
	 * @return array language pack details.
	 */
	public static function getLanguagePackDetails($return) {
		$dataname = $return[dataname];
		$whereColumns = array($return[locationid]);
		$columnsArr = array("use_language_pack");
		$languagePackQuery = CQ::getRowsWhereColumnIn('locations', 'id', $whereColumns, $dataname, $columnsArr);
		$whereColumns = array("id" => ($languagePackQuery[0]['use_language_pack'] == 0 ? 1 : $languagePackQuery[0]['use_language_pack']) , "_deleted" => 0);
		$columnsArr = array("id", "language", "english_name", "google_code", "apple_code");
		$languagePack = CQ::getRowsWithRequiredColumnsMainDB('language_packs', $columnsArr,$whereColumns, '', '', 'MAIN');
		return $languagePack;
	}
}