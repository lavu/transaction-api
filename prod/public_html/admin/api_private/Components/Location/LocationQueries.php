<?php

//Class of static methods to provide abstraction to query calls.
//All methods should be static, and return the data if possible instead of mysqli_result.
class LocationQueries {
    public static $tableRestaurantLocation = 'restaurant_locations';

	public static function restaurantLocationsDetails($dataname) {
        return CQ::getRowsWithRequiredColumnsMainDB(
            self::$tableRestaurantLocation,
            [
                "id",
                "dataname",
                "restaurantid",
                "locationid",
                "title",
                "address",
                "city",
                "state",
                "country",
                "zip",
                "timezone"
            ],
            ["dataname" => $dataname]
        );
    }
}