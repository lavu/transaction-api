<?php

/*
 *
 */

class MenuDataController extends DataControllerBase
{

    public function __construct($request, $dataname)
    {
        parent::__construct(new ReflectionClass(__CLASS__), $request, $dataname);
    }

    /*protected function action_GetAccountByCredentials($args){
        $actionName = substr(__FUNCTION__, 7);
        $userRowFromMainDB = getMainDBCustomerAccountUsingCredentials($username, $password);
        $this->verifyNotEmpty($userRowFromMainDB,$actionName,"Could not find username in main db.");
        DBConnection::startClientConnection($userRowFromMainDB['dataname']);
        $userRowInClientDB = ADCQ::getUserRowFromClientDB($username, $password);
        $this->verifyNotEmpty($userRowInClientDB,"Username and Password match not found.");
    }*/


    protected function action_getMenus($args) {
        $query_string = "SELECT * FROM 
        (
            SELECT `mg`.`id`, 
                `group_name` AS `text`, 
                -1 AS `parentid`,
                'group' AS `type`,
                `mg`.`_order`,
                `mg`.`orderby`,
                COUNT(`mi`.`id`) as totalNumberOfItems
            FROM `menu_groups` `mg`
            JOIN `menu_categories` `mc`
            ON `mc`.`group_id` = `mg`.`id`
            JOIN `menu_items` `mi`
            ON `mc`.`id` = `mi`.`category_id`
            WHERE `mg`.`_deleted` = 0
            AND `group_name` != 'Universal'
            AND `mc`.`ltg_display` = 1 AND `mc`.`_deleted` = 0
            AND `mi`.`_deleted` = 0 
            AND `mi`.`ltg_display` = 1
            GROUP BY `mg`.`id`
            UNION 
            SELECT `mc`.`id`, 
                `mc`.`name` AS `text`, 
                `mc`.`group_id` AS `parent_id`,
                'category' AS `type`,
                `mc`.`_order`,
                'order' AS `orderby`,
                COUNT(`mi`.`id`) as totalNumberOfItems
            FROM `menu_categories` `mc`
            JOIN `menu_groups`
            ON `mc`.`group_id` = `menu_groups`.`id`
            JOIN `menu_items` `mi`
            ON `mc`.`id` = `mi`.`category_id`
            WHERE `menu_groups`.`_deleted` = 0 
            AND `mc`.`_deleted` = 0
            AND `mc`.`ltg_display` = 1
            AND `mi`.`_deleted` = 0 
            AND `mi`.`ltg_display` = 1
            GROUP BY `mc`.`id`
        )  `mgc` WHERE `mgc`.`totalNumberOfItems` > 0 order by `mgc`.`_order` ASC";
        $result = DBConnection::clientQuery($query_string);
        $data['menu_category']=MA::resultToArray($result);
        $data['universal_category']=$this->toGetUniversalMenuCategoriesList();
        return $data;
    }
    

    protected function action_checkItemExist($args)
    {
        $languageid = 1;
        if(isset($args['languageid']) && $args['languageid'] != '') {
            $languageid = $args['languageid'];
        }
    	$dineinoptions = $args['dining_type'];
    	$whereColumnDefault = array('_deleted' => 0, 'ltg_display' => 1, 'id' => $args['item_id'], "dining_option" => "like:".$dineinoptions);
    	$menuItemsColNameArr = array("id","name");
    	$orderBy = '';
    	$menuItemRowsResult = CQ::getRowsWithRequiredColumns('menu_items', $menuItemsColNameArr, $whereColumnDefault,$orderBy);
    	if(!empty($menuItemRowsResult))
    	{
    	    $exist = apiSpeak($languageid, "Item exists");
    		$status = 1;
    	}
    	else
    	{
    	    $exist = apiSpeak($languageid, "Item doesnot exists");
    		$status = 0;
    	}
    	$resultArr = array("status" => $status, "msg" => $exist);
    	return $resultArr;
    }
    
    function toGetUniversalMenuCategoriesList() {
        $query_string = "SELECT * FROM (
            SELECT `mc`.`id`,
               `mc`.`name`,
                0  AS `parent_id`,
               `mc`.`_order`,
               'category' AS `type`,
               `mg`.`orderby`,
               COUNT(`mi`.`id`) as totalNumberOfItems
        FROM `menu_categories` `mc`
        JOIN `menu_groups` `mg` ON `mc`.`group_id` = `mg`.`id`
        JOIN `menu_items` `mi`
        ON `mc`.`id` = `mi`.`category_id`
        WHERE `mg`.`_deleted` = 0
        AND `mc`.`_deleted` = 0
        AND `mc`.`ltg_display` = 1
        AND `group_name` = 'Universal'
        AND `mg`.menu_id = 1
        AND `mi`.`_deleted` = 0 
        AND `mi`.`ltg_display` = 1
        GROUP BY `mc`.`id`) UNIMENU where `totalNumberOfItems` > 0 ";
        $result = DBConnection::clientQuery($query_string);
        return MA::resultToArray($result);
    }

}
