<?php

//Class of static methods to provide abstraction to query calls.
//All methods should be static, and return the data if possible instead of mysqli_result.
class MenuQueries{

    public static function getMenuInfo($args)
    {

        /*
         * "DataResponse":
        {
        "MenuGroups":{
            "1":{...},
            "2":{...}...,
        }
        "MenuCategories":{
             "5":{...},
            "6":{...,"MenuGroup":3,...}...,
         }
        "MenuItems":{
            "2":{...},
            "3":{..., "MenuCategory":4,...}
           }
         }
         */
        $query_string = "SELECT * FROM 
	(SELECT `id`, 
		`group_name` as `text`, 
		-1 AS `parentid` 
	FROM `menu_groups` `mg` 
	WHERE `_deleted` = 0
	UNION
	SELECT `mc`.`id`, 
		`mc`.`name`, 
		`mc`.`group_id` AS `parent_id` 
	FROM `menu_categories` `mc` 
	JOIN `menu_groups` 
	ON `mc`.`group_id` = `menu_groups`.`id` 
	WHERE `menu_groups`.`_deleted` = 0
		)  `mgc`";
        $result = DBConnection::clientQuery($query_string);
        return MA::resultToArray($result);
    }

	
}

