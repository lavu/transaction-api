<?php

abstract class DataControllerBase{

	protected $m_dataname;
	protected $m_databaseName;
	protected $m_locationRow;
	protected $m_clientConnection;
	protected $m_request;
	protected $m_actionsAvailable;
	protected $m_componentActionMethods;
	protected $m_processed_response;
	protected $m_objectTypeSchemaMap;
	protected $m_api_response_arr;
	protected $m_dataResponse;



	protected function __construct($reflectionClass, $requestArr, $dataname, $flag = true){
		if ($flag) {
			if (empty($dataname))
			{
				$this->m_databaseName = 'poslavu_MAIN_db';
				$clientConnection = DBConnection::getConnectionByType('admin');
			}
			else
			{
				$this->m_dataname = $dataname;
				$this->m_databaseName = 'poslavu_'.$this->m_dataname.'_db';
				$clientConnection = DBConnection::getConnectionByType('client', $dataname);
				if (empty($clientConnection)) { //The connection may be started by Integration Tests.
					DBConnection::startClientConnection($dataname);
				}
			}
		}
		$this->m_request = $requestArr;
		$this->m_actionsAvailable = $this->getAvailableActions($reflectionClass);
		$this->m_api_response_arr = array();
	}

	public function runAPIRequest(){
		$this->handleRequest();
	}

	private function handleRequest(){
		$action = $this->m_request['Action'];
		$actionFull = 'action_'.$action;
		$args = !empty($this->m_request['Params']) && is_array($this->m_request['Params']) ? $this->m_request['Params'] : array();
		if(in_array($actionFull, $this->m_actionsAvailable)){
			$this->m_dataResponse = $this->$actionFull($args);
		}
	}

	public function getProcessedResponse(){
		$this->m_processed_response = array();
		$this->m_processed_response['DataResponse'] = $this->m_dataResponse;
		return $this->m_processed_response;
	}

	private function getAvailableActions($reflectionClass){
		$availableActions = array();
		$reflectionMethods = $reflectionClass->getMethods();
		foreach($reflectionMethods as $currReflectionMethod){
			if($currReflectionMethod->isProtected() && $currReflectionMethod->class != 'DataControllerBase'){
				if(strpos($currReflectionMethod->name, 'action_') === 0){
					$availableActions[] = $currReflectionMethod->name;
				}
			}
		}
		return $availableActions;
	}


	//Exception handling verification methods.
	protected function verifyNotEmpty($shouldNotBeEmpty, $actionName, $message){
		if(empty($shouldNotBeEmpty)){
			throw new IsEmptyException($actionName, $message);
		}
	}
	protected function verifyActionArguments($actionName, $args, $requiredArgs){
		$parameters = array_keys($args);
		foreach($requiredArgs as $currRequired){
			if(!in_array($currRequired, $parameters)){
				throw new ActionMissingParametersException("Missing required parameter:".$currRequired);
			}
			if(empty($args[$currRequired])){
				throw new ActionEmptyArgumentException("Argument for parameter ".$currRequired." is empty");
			}
		}
	}


	protected function verifySchema(){ /*todo*/ }
}
		
