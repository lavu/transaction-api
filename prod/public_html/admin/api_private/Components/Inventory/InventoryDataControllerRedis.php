<?php
class InventoryDataController extends DataControllerBase
{

	private static $m_unitSystemArray = null;

	public function __construct($request, $dataname)
	{
		parent::__construct(new ReflectionClass(__CLASS__), $request, $dataname);
	}

	private function getEntities($request, $args)
	{
		$entities = array();
		$requestItems = isset($request['Items']) ? $request['Items'] : array();

		for ($i = 0; $i < count($requestItems); $i++) {
			$entity = $requestItems[$i];
			$iid = isset($entity['id']) ? $entity['id'] : null;
			if (!empty($iid)) {
				$entity = CQ::getRowByID($args['entityDbTable'], $iid);
				$entityItems = CQ::getRowsWhereColumnIn($args['entityItemsDbTable'], $args['entityParentKey'], array($iid));
				$entity[$args['entityItemsKey']] = $entityItems;
			}

			$entities[] = $entity;
		}

		return $entities;
	}

	private function updateEntities($request, $args)
	{
		$rowsUpdated = 0;
		$updateOk = true;
		$requestItems = isset($request['Items']) ? $request['Items'] : array();

		for ($i = 0; $i < count($requestItems); $i++) {
			$entity = $requestItems[$i];
			$iid = isset($entity['id']) ? $entity['id'] : null;

			if (!empty($iid)) {

				// So we do not try to update the id field
				unset($entity['id']);

				// Do not proceed with updates if one has failed
				$updateOk = $updateOk && CQ::updateArray($args['entityDbTable'], $entity, " WHERE `id` = {$iid} AND `_deleted` = 0");

				if ($updateOk) {
					if (!empty($entity[$request['entityItemsKey']])) {
						$entityItems = array('Items' => $entity[$request['entityItemsKey']]);
						$result = self::action_updateEntityItems($entityItems, $args);
						if (!empty($result['RowsUpdated']) && $result['RowsUpdated'] > 0) {
							$rowsUpdated++;
						}
					} else {
						++$rowsUpdated;
					}
				}
			}

		}

		return array('RowsUpdated' => $rowsUpdated);
	}

	/**
	 * @param $id
	 * @param $inventoryItem
	 * @return array
	 */
	protected function getPriceTimelineForItem($inventoryItem)
	{
		$priceTimeLine = array();
		$oldestDate = "0000-00-00 00:00:00";
		$this->getPurchaseOrderValueData($inventoryItem, $oldestDate, $priceTimeLine);
		$this->getTransferValueData($inventoryItem, $oldestDate, $priceTimeLine);
		$this->getRWPOValueData($inventoryItem, $oldestDate, $priceTimeLine);
		$itemCreationInformation = InventoryQueries::getCreateInventoryItemCostDataByID($inventoryItem['id']);
		if ($itemCreationInformation) {
			$itemCreationInformation = $itemCreationInformation[0];
			if ($itemCreationInformation['quantity'] != 0) {
				if ($itemCreationInformation['receiveDate']) {
					if ($itemCreationInformation['unitID'] === $inventoryItem['salesUnitID']) {
						$salesQuantity = $itemCreationInformation['quantity'];
						$salesCost = $itemCreationInformation['unitCost'];
					} else {
						$salesQuantity = self::unitSystemConversion($inventoryItem['salesUnitID'], $itemCreationInformation['unitID'],
							$itemCreationInformation['quantity']);
						if ($salesQuantity != 0) {
							$salesCost = ($itemCreationInformation['unitCost'] * $itemCreationInformation['quantity']) / $salesQuantity;
						} else {
							$salesCost = 0;
						}
					}
					$itemCreationInformation['salesQuantity'] = $salesQuantity;
					$itemCreationInformation['salesCost'] = $salesCost;
					$itemCreationInformation['date'] = $itemCreationInformation['receiveDate'];
					$priceTimeLine[] = $itemCreationInformation;
				}
			}
		}

		//Now that we have all of the events that could have affected our prices, we sort them by date and then loop through.
		$priceTimeLine = self::sortByDescendingDate($priceTimeLine);
		return $priceTimeLine;
	}

	private function updateEntityItems($args)
	{
		$rowsUpdated = 0;
		$updateOk = true;
		$entityItems = isset($args['Items']) ? $args['Items'] : array();

		for ($i = 0; $i < count($entityItems); $i++) {
			$entityItem = $entityItems[$i];
			$iid = $entityItem['id'];

			if (!empty($iid)) {
				$updateOk = $updateOk && CQ::updateArray($args['entityItemsDbTable'], $entityItem, " WHERE `id` = {$iid} AND `_deleted` = 0");
				if ($updateOk) {
					$rowsUpdated++;
				}
			}
		}

		return array('RowsUpdated' => $rowsUpdated);
	}

	private static function sortByAscendingDate($result)
	{ //TODO: Where should this belong?
		if (array_key_exists('date', $result)) {
			usort($result, function ($aDate, $bDate) { //Sort by ascending date. i.e. earlier date first.\

				return strtotime($aDate['date']) - strtotime($bDate['date']);

			});
		} else if (array_key_exists('datetime', $result)) {
			usort($result, function ($aDate, $bDate) { //Sort by ascending date. i.e. earlier date first.\

				return strtotime($aDate['datetime']) - strtotime($bDate['datetime']);

			});
		}
		return $result;
	}

	private static function sortByDescendingDate($result)
	{ //TODO: Where should this belong?
		usort($result, function ($aDate, $bDate) { //Sort by descending date. i.e. latest date first.
			return strtotime($bDate['date']) - strtotime($aDate['date']);
		});
		return $result;
	}

	protected function action_getValue()
	{
		$items = self::action_getAllInventoryItems();
		$returnArray = array();
		foreach ($items as $item) {
			$returnArray[$item['id']] = self::getValueByID($item['id']);
		}
		return $returnArray;
	}

	protected function action_getItemValue($args)
	{
		$items = $args['Items'];
		$return = array();
		foreach ($items as $item) {
			$return[$item['id']]['value'] = $this->getValueByID($item['id']);
		}
		return $return;
	}

	protected static function unitSystemConversion($toID, $fromID, $value)
	{
		if (!isset(self::$m_unitSystemArray)) {
		   self::$m_unitSystemArray = self::action_getStandardUnitConversion();
		}
		$conversion = InventoryQueries::getUnitConversion($fromID, $toID, self::$m_unitSystemArray);
		$returnValue = $value * $conversion;
		return $returnValue;
	}

	protected static function roundConversion($toID, $fromID, $value){
		$convertedValue = self::unitSystemConversion($toID, $fromID, $value);
		return round($convertedValue * 2)/2;
	}

	protected function action_getCostTimeline($args){
		$items = array();
		$quantities = $args['Items']['quantities'];
		if (!empty($args['Items'])) {
			$ids = $args['Items']['itemIDs'];
			foreach ($ids as $id) {
				$currentItem = InventoryQueries::getInventoryItemByID($id);
				if (array_key_exists($id, $quantities)) {
					$currentItem[0]['quantityOnHand'] += $quantities[$id];
				}
				$items[] = $currentItem[0];
			}
		} else {
			$items = InventoryQueries::getAllInventoryItems();
		}
		$priceTimeLine = array();
		foreach ($items as $item) {
			$itemPriceTimeLine = array();
			//$itemPriceTimeLine['inventoryItemID'] = $item['id'];
			$oldestDate = "0000-00-00 00:00:00";
			$this->getPurchaseOrderValueData($item, $oldestDate, $itemPriceTimeLine);
			$this->getTransferValueData($item, $oldestDate, $itemPriceTimeLine);
			$this->getRWPOValueData($item, $oldestDate, $itemPriceTimeLine);
			$itemCreationInformation = InventoryQueries::getCreateInventoryItemCostDataByID($item['id']);
			if ($itemCreationInformation) {
				$itemCreationInformation = $itemCreationInformation[0];
				if ($itemCreationInformation['quantity'] != 0) {
					if ($itemCreationInformation['receiveDate']) {
						if ($itemCreationInformation['unitID'] === $item['salesUnitID']) {
							$salesQuantity = $itemCreationInformation['quantity'];
							$salesCost = $itemCreationInformation['unitCost'];
						} else {
							$salesQuantity = self::unitSystemConversion($item['salesUnitID'], $itemCreationInformation['unitID'],
								$itemCreationInformation['quantity']);
							if ($salesQuantity != 0) {
								$salesCost = ($itemCreationInformation['unitCost'] * $itemCreationInformation['quantity']) / $salesQuantity;
							} else {
								$salesCost = 0;
							}
						}
						$itemCreationInformation['salesQuantity'] = $salesQuantity;
						$itemCreationInformation['salesCost'] = $salesCost;
						$itemCreationInformation['date'] = $itemCreationInformation['receiveDate'];
						$itemPriceTimeLine[] = $itemCreationInformation;
					}
				}
			}

			$itemPriceTimeLine = self::sortByDescendingDate($itemPriceTimeLine);
			$priceTimeLine[$item['id']] = $itemPriceTimeLine;
		}
		return $priceTimeLine;
	}

	protected function getValueByID($id)
	{
		$inventoryItem = InventoryQueries::getInventoryItemByID($id);
		if (!$inventoryItem) {
			return null;
		}
		$priceTimeLine = $this->getPriceTimelineForItem($inventoryItem[0]);
		$totalCost = 0;
		$quantity = $inventoryItem[0]['quantityOnHand'];
		foreach ($priceTimeLine as $costEvent) {
			if ($quantity - $costEvent['salesQuantity'] < 0) {
				$totalCost += $costEvent['salesCost'] * $quantity;
				break;
			}

			$totalCost += $costEvent['salesCost'] * $costEvent['salesQuantity'];
			$quantity = $quantity - $costEvent['salesQuantity'];
		}
		return $totalCost;
	}

	protected function getRWPOValueData($item, &$oldestDate, &$priceTimeLine){
		$iid = $item['id'];
		$currentRWPOID = null;
		if (!empty($priceTimeLine)) {
			$currentReceiveWOPO = InventoryQueries::getNextReceiveWithoutPO($currentRWPOID, $iid);
			if ($currentReceiveWOPO[0]) {
				while ($currentReceiveWOPO && $currentReceiveWOPO[0]['receiveDate'] >= $oldestDate) {
					if ($currentRWPOID === $currentReceiveWOPO[0]['id']) {
						break;
					}
					$currentRWPOID = $currentReceiveWOPO[0]['id'];

					$currentReceiveWOPO = $currentReceiveWOPO[0];
					if ($currentReceiveWOPO['unitID'] === $item['salesUnitID']) {
						$salesQuantity = $currentReceiveWOPO['quantity'];
						$salesCost = $currentReceiveWOPO['cost'];
					} else {
						$salesQuantity = self::unitSystemConversion($item['salesUnitID'], $currentReceiveWOPO['unitID'],
							$currentReceiveWOPO['quantity']);
						$salesCost = ($currentReceiveWOPO['cost'] * $currentReceiveWOPO['quantity']) / $salesQuantity;
					}

					$currentReceiveWOPO['salesCost'] = $salesCost;
					$currentReceiveWOPO['salesQuantity'] = $salesQuantity;
					$currentReceiveWOPO['date'] = $currentReceiveWOPO['receiveDate'];
					$priceTimeLine[] = $currentReceiveWOPO;

					$currentReceiveWOPO = InventoryQueries::getNextReceiveWithoutPO($currentRWPOID, $iid);
				}
			}
		} else {
			$quantity = $item['quantityOnHand'];
			while ($quantity > 0) {
				$currentRWPO = InventoryQueries::getNextReceiveWithoutPO($currentRWPOID, $iid);
				if ($currentRWPO) {
					$currentRWPO = $currentRWPO[0];
					if (empty($currentRWPO['id']) || $currentRWPO['id'] === $currentRWPOID) {
						break;
					} else {
						$currentRWPOID = $currentRWPO['id'];
					}
					if ($currentRWPO['unitID'] === $item['salesUnitID']) {
						$salesQuantity = $currentRWPO['quantity'];
						$salesCost = $currentRWPO['cost'];
					} else {
						$salesQuantity = self::unitSystemConversion($item['salesUnitID'], $currentRWPO['unitID'],
							$currentRWPO['quantity']);
						$salesCost = ($currentRWPO['cost'] * $currentRWPO['quantity']) / $salesQuantity;
					}

					$currentRWPO['salesCost'] = $salesCost;
					$currentRWPO['salesQuantity'] = $salesQuantity;
					$currentRWPO['date'] = $currentRWPO['receiveDate'];
					$priceTimeLine[] = $currentRWPO;
					$oldestDate = $currentRWPO['receiveDate'];
					$quantity = $quantity - $salesQuantity;
				} else {
					break;
				}
			}
		}
	}

	protected function getPurchaseOrderValueData($item, &$oldestDate, &$priceTimeLine){
	   $quantity = $item['quantityOnHand'];
	   $currentPOID = null;
		$iid = $item['id'];
		while ($quantity > 0) {
			$currentPurchaseOrder = InventoryQueries::getNextPurchaseOrder($currentPOID, $iid);
			if ($currentPurchaseOrder) {
				$currentPurchaseOrder = $currentPurchaseOrder[0];
				if (empty($currentPurchaseOrder['id']) || $currentPurchaseOrder['id'] === $currentPOID) {
					break;
				} else {
					$currentPOID = $currentPurchaseOrder['id'];
				}
				if ($currentPurchaseOrder['purchaseUnitID'] === $item['salesUnitID']) {
					$salesQuantity = $currentPurchaseOrder['quantityReceived'];
					$salesCost = $currentPurchaseOrder['purchaseUnitCost'];
				} else {
					$salesQuantity = self::unitSystemConversion($item['salesUnitID'], $currentPurchaseOrder['purchaseUnitID'],
						$currentPurchaseOrder['quantityReceived']);
					if ($salesQuantity != 0) {
						$salesCost = ($currentPurchaseOrder['purchaseUnitCost'] * $currentPurchaseOrder['quantityReceived']) / $salesQuantity;
					} else {
						$salesCost = 0;
					}
				}

				$currentPurchaseOrder['salesCost'] = $salesCost;
				$currentPurchaseOrder['salesQuantity'] = $salesQuantity;
				$currentPurchaseOrder['date'] = $currentPurchaseOrder['receiveDate'];
				$priceTimeLine[] = $currentPurchaseOrder;
				$quantity = $quantity - $salesQuantity;
				$oldestDate = $currentPurchaseOrder['receiveDate'];
			} else {
				break;
			}
		}
	}

	protected function getTransferValueData($item, &$oldestDate, &$priceTimeLine){

		$enterpriseIDAndTitleResult = EnterpriseQueries::getRestaurantIDAndTitleByDataname($this->m_dataname);
		$restaurantID = $enterpriseIDAndTitleResult[0]['restaurantid'];
		$currentTransferID = null;
		$iid = $item['id'];
		//fill transfers in where possible.
		if (!empty($priceTimeLine)) {
			$currentTransfer = InventoryQueries::getNextTransfer($currentTransferID, $iid, $restaurantID);
			while ($currentTransfer[0]['receiveDate'] >= $oldestDate) {
				if (empty($currentTransfer[0]['transferID']) || $currentTransfer[0]['transferID'] === $currentTransferID) {
					break;
				} else {
					$currentTransferID = $currentTransfer[0]['id'];
				}
				$currentTransfer = $currentTransfer[0];
				if ($currentTransfer['transferUnitID'] === $item['salesUnitID']) {
					$salesQuantity = $currentTransfer['quantityReceived'];
					$salesCost = $currentTransfer['transferUnitCost'];
				} else {
					$salesQuantity = self::unitSystemConversion($item['salesUnitID'], $currentTransfer['transferUnitID'],
						$currentTransfer['quantityReceived']);
					$salesCost = ($currentTransfer['transferUnitCost'] * $currentTransfer['quantityReceived']) / $salesQuantity;
				}

				$currentTransfer['salesCost'] = $salesCost;
				$currentTransfer['salesQuantity'] = $salesQuantity;
				$currentTransfer['date'] = $currentTransfer['receiveDate'];
				$priceTimeLine[] = $currentTransfer;
				$currentTransfer = InventoryQueries::getNextTransfer($currentTransferID, $iid, $restaurantID);
				if (empty($currentTransfer['transferID'])) {
					break;
				}
			}
		} else if (empty($priceTimeLine)) {
			$quantity = $item['quantityOnHand'];
			while($quantity > 0){
				$currentTransfer = InventoryQueries::getNextTransfer($currentTransferID, $iid, $restaurantID);
				if (!empty($currentTransfer[0]) && !empty($currentTransfer[0]['transferID'])) {
					$currentTransfer = $currentTransfer[0];
					if ($currentTransfer['transferID'] === $currentTransferID) {
						break;
					} else {
						$currentTransferID = $currentTransfer['transferID'];
					}
					if ($currentTransfer['transferUnitID'] === $item['salesUnitID']) {
						$salesQuantity = $currentTransfer['quantityReceived'];
						$salesCost = $currentTransfer['transferUnitCost'];
					} else {
						$salesQuantity = self::unitSystemConversion($item['salesUnitID'], $currentTransfer['transferUnitID'],
							$currentTransfer['quantityReceived']);
						$salesCost = ($currentTransfer['transferUnitCost'] * $currentTransfer['quantityReceived']) / $salesQuantity;
					}

					$currentTransfer['salesCost'] = $salesCost;
					$currentTransfer['salesQuantity'] = $salesQuantity;
					$currentTransfer['date'] = $currentTransfer['receiveDate'];
					$priceTimeLine[] = $currentTransfer;
					$oldestDate = $currentTransfer['receiveDate'];
					$quantity = $quantity - $salesQuantity;
				} else {
					break;
				}
			}
		}
	}

	protected function action_getCostData(){
		$fifoPO = $this->getPurchaseOrderCostData();
		$fifoTransfer = $this->getTransferCostData();
		$fifoNoPO = $this->getReceiveWithoutPOCostData();
		$fifoCreateItem = $this->getCreateInventoryItemCostData();
		$result = array();
		$result = array_merge($result, $fifoPO);
		$result = array_merge($result, $fifoTransfer);
		$result = array_merge($result, $fifoNoPO);
		$result = array_merge($result, $fifoCreateItem);
		$result = self::sortByDescendingDate($result);
		return $result;
	}

	protected function getCreateInventoryItemCostData(){
		$result = InventoryQueries::getCreateInventoryItemCostData();
		$result = self::sortByDescendingDate($result);
		return $result;
	}

	protected function getPurchaseOrderCostData(){
		$result = InventoryQueries::getPurchaseOrderCostData();
		$result = self::sortByDescendingDate($result);
		return $result;
	}

	protected function getTransferCostData(){
		$result = InventoryQueries::getTransferCostData();
		$result = self::sortByDescendingDate($result);
		return $result;
	}

	protected function getReceiveWithoutPOCostData(){
		$result = InventoryQueries::getReceiveWithoutPOCostData();
		$result = self::sortByDescendingDate($result);
		return $result;
	}

	protected function getWasteCostData(){
		$result = InventoryQueries::getWasteCostData();
		$result = self::sortByDescendingDate($result);
		return $result;
	}

	protected function action_getAuditsByActionsAndDates($args) {
		foreach ($args['Items'] as $itemArgs) {
			$audits = InventoryQueries::getAuditsByActionsAndDates($itemArgs);
			$result[] = self::sortByAscendingDate($audits);
		}
		return $result;
	}

	//ALL API CALLS MUST BE PROTECTED
	protected function action_createInventoryItems($args){
	   return $this->action_addInventoryItems($args);
	}

	protected function action_addInventoryItems($args){
		$insertRows = $args['Items'];
		foreach ($insertRows as &$item) {
			/*remove conversions for lowQuantity
			if (!empty($item['lowQuantity'])) {
				$item['lowQuantity'] = self::unitSystemConversion($item['salesUnitID'], $item['recentPurchaseUnitID'], $item['lowQuantity']);
			}
			*/
			if (!empty($item['reOrderQuantity'])) {
				$item['reOrderQuantity'] = self::unitSystemConversion($item['salesUnitID'], $item['recentPurchaseUnitID'], $item['reOrderQuantity']);
			}
		}

		$response = InventoryQueries::insertInventoryItems($insertRows);
		if (!$response) {
			throw new APIException("Failed to insert inventory items.");
		}
		if (!empty($response) && $response['RowsUpdated'] > 0) {
			$redisController = new InventoryRedisController();
			$inventoryCache = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
			$cachInventoryDetails = $inventoryCache + $response['Inventory'];
			$redisController->setInventory($cachInventoryDetails);
			unset($response['Inventory']);
		}
		if (empty($response)) {
			return MA::resultToInsertIDUpdatedRows($response);
		}
		return $response;
		}

	protected function action_addSingleInventoryItem($args){
		$insertRows = $args['Items'];
		foreach ($insertRows as &$item) {
			/*remove lowQuantity conversion
			if (!empty($item['lowQuantity'])) {
				$item['lowQuantity'] = self::unitSystemConversion($item['salesUnitID'], $item['recentPurchaseUnitID'], $item['lowQuantity']);
			}
			*/
			if (!empty($item['reOrderQuantity'])) {
				$item['reOrderQuantity'] = self::unitSystemConversion($item['salesUnitID'], $item['recentPurchaseUnitID'], $item['reOrderQuantity']);
			}
		}
		$rowResult = InventoryQueries::insertInventoryItem($args);
		return json_encode($rowResult);
	}

	protected function action_getInventoryItemCounts($args){
		$resultArr = InventoryQueries::getInventoryItemCounts();
		return $resultArr;
	}


	protected function action_getAllSalesUnitsOfMeasure(){
		$resultArr = InventoryQueries::getAllSalesUnitsOfMeasure();
		return $resultArr;
	}

	protected function action_getAllStandardUnitsOfMeasure(){
		$resultArr = InventoryQueries::getAllStandardUnitsOfMeasure();
		return $resultArr;
	}

	protected function action_getAllPurchaseUnitsOfMeasure(){
		$resultArr = InventoryQueries::getAllPurchaseUnitsOfMeasure();
		return $resultArr;
	}
	/*
	getAllStorageLocations()
	*/
	protected function action_getAllStorageLocations(){
		$resultArr = InventoryQueries::getAllStorageLocations();
		return $resultArr;
	}
	protected function action_getAllStorageLocationsWithArchives(){
		$resultArr = InventoryQueries::getAllStorageLocationsWithArchives();
		return $resultArr;
	}

	protected function action_addVendors($args){
		$columnNames = array_keys($args['Vendors'][0]);
		$insertRows = array();
		foreach ($args['Vendors'] as $currRow) {
			$insertRows[] = array_values($currRow);
		}
		$result = CommonQueries::insertArray('vendors', $columnNames, $insertRows);
		return MA::resultToInsertIDUpdatedRows($result);
	}

	protected function action_archiveVendors($args){
		$ids = array();
		if (empty($args['IDs'])) {
			foreach ($args['Items'] as $currItem) {
				$ids[] = $currItem['id'];
			}
		}
		$inClause = MQ::buildSanitizedListInParenthesis($ids, "'");
		$result = CQ::updateArray('vendors', array('_deleted' => 1), ' WHERE `id` IN' . $inClause);
		$rowsAffected = DBConnection::clientAffectedRows();
		return array("RowsUpdated" => $rowsAffected);
	}

	protected function action_copyVendors($args){
		$rowsInserted = 0;
		$insertIDs = array();
		$vendors = isset($args['Items']) ? $args['Items'] : array();
		foreach ($vendors as $i => $vendor) {
			CommonQueries::copyRowInTable('vendors', $vendor['id'], array('name' => ' Copy'));
			$newVendorID = DBConnection::clientInsertID();
			$insertIDs[] = array('id' => $newVendorID);
			++$rowsInserted;
		}
		return array('RowsInserted'=>$rowsInserted, 'InsertIDs' => $insertIDs);
	}

	protected function action_createVendors($args){  //TODO Refactor
		$items = $args['Items'];
		$returnData = array();
		foreach ($items as $currItem) {
			$fargs = array("Vendors" => array($currItem));
			$returnData[] = $this->action_addVendors($fargs);
		}
		return $returnData;
	}

	protected function action_getVendors($request) {
		$args = array(
			'entityName'         => 'vendors',
			'entityDbTable'      => 'vendors',
			'entityItemsKey'     => 'vendorItems',
			'entityParentKey'    => 'vendorID',
			'entityItemsDbTable' => 'vendor_items',
		);
		return $this->getEntities($request, $args);
	}

	protected function action_updateVendors($request) {
		$args = array(
			'entityName'         => 'vendors',
			'entityDbTable'      => 'vendors',
			'entityItemsKey'     => 'vendorItems',
			'entityItemsDbTable' => 'vendor_items',
		);
		return $this->updateEntities($request, $args);
	}

	protected function action_updateVendorItems($args) {
		$items = $args['Items'];
		$result = null;
		$rowsEffected = 0;
		foreach ($items as $i=>$item) {
			$result = CQ::updateArray('vendor_items', $item, ' WHERE `id` = '.$item['id']);
			$rowsEffected += DBConnection::clientAffectedRows();

		}
		return array("RowsUpdated" => $rowsEffected);

	}


	protected function action_createVendorItems($args){
		return $this->action_addVendorItems($args);
	}

	protected function action_addVendorItems($args)
	{
		$insertRows = array();
		foreach ($args['Items'] as $currRow) {
			$columnNames = array_keys($currRow);
			$insertRows[0] = array_values($currRow);
			$result = CommonQueries::insertArray('vendor_items', $columnNames, $insertRows);
		}
		return MA::resultToInsertIDUpdatedRows($result);
	}

	protected function action_getVendorItemsByVendorID($args){
		if (!empty($args['Items'])) {
			$vendorItems = CQ::getWhereColumnEqualsAnyValueOfFieldInValuesArray('vendor_items', 'vendorID', $args['Items'], 'id');
			$costData = $this->action_getCostData();
			for ($i = 0; $i<count($vendorItems); $i++) {
				$item = $vendorItems[$i];
				for ($j = 0; $j<count($costData); $j++) {
					$cd = $costData[$j];
					if ((int)$cd['inventoryItemID'] == (int)$item['inventoryItemID']) {
						$vendorItems[$i]['suggestedQuantity'] = $cd['quantity'];
						break;
					}
				}
				if (!isset($vendorItems[$i]['suggestedQuantity'])) {
					$vendorItems[$i]['suggestedQuantity'] = "";
				}
				$inventoryItemValues = InventoryQueries::getInventoryItemByID($vendorItems[$i]['inventoryItemID']);
				if (is_array($inventoryItemValues) && count($inventoryItemValues) > 0) {
					$vendorItems[$i]['recentPurchaseUnitID'] = $inventoryItemValues[0]['recentPurchaseUnitID'];
					$vendorItems[$i]['name'] = $inventoryItemValues[0]['name'];
					$vendorItems[$i]['recentPurchaseCost'] = $inventoryItemValues[0]['recentPurchaseCost'];
				} else {
					$vendorItems[$i]['recentPurchaseUnitID'] = "";
					$vendorItems[$i]['name'] = "";
					$vendorItems[$i]['recentPurchaseCost'] = "";
				}
			}
			return $vendorItems;
		} else {
			return "error, arguments are empty.";
		}
	}

	protected function action_getExternalInventoryItems($args){
		$externalID = $args['Items'][0]['id'];
		//error_log(__FILE__ . " " . print_r($externalID,1));
		DBConnection::startTransferConnection($externalID);
		$result = InventoryQueries::getExternalInventoryItems();
		return MarshallingArrays::resultToArray($result);
	}

	//  Inventory
	protected function action_getAllManageViewRows($args){
		$rowResult = InventoryQueries::getAllManageViewRows();
		return $rowResult;
	}

	protected function action_getInventoryItemByID($args){
		$id = $args['Items'][0]['id'];
		//$this->verifyActionArguments("AuthenticateSession", $args, $requiredParameters);
		$rowResult = CQ::getRowByID('inventory_items', $id);
		/*Remove lowquantity conversion
		if (!empty($rowResult['lowQuantity'])) {
			$rowResult['lowQuantity'] = round(self::unitSystemConversion($rowResult['recentPurchaseUnitID'], $rowResult['salesUnitID'], $rowResult['lowQuantity']));
		}
		*/
		if (!empty($rowResult['reOrderQuantity'])) {
			$rowResult['reOrderQuantity'] = self::roundConversion($rowResult['recentPurchaseUnitID'], $rowResult['salesUnitID'], $rowResult['reOrderQuantity']);
		}

		return $rowResult;
	}

	protected function action_archiveInventoryItemsByID($args){
		$resultArray = array();

		$ids = array();
		foreach ($args['Items'] as $currItem) {
			$ids[] = $currItem['id'];
		}

		$inClause = MQ::buildSanitizedListInParenthesis($ids, "'");
		$result = CQ::updateArray('inventory_items', array('_deleted' => 1), ' WHERE `id` IN ' . $inClause);
		$rowsAffected = DBConnection::clientAffectedRows();
		$resultArray["InventoryItemRowsArchived"] = $rowsAffected;

		$inClause = MQ::buildSanitizedListInParenthesis($ids, "'");
		$result = CQ::updateArray('vendor_items', array('_deleted' => 1), ' WHERE `inventoryItemID` IN ' . $inClause);
		$viRowsAffected = DBConnection::clientAffectedRows();
		$resultArray["VendorItemRowsArchived"] = $viRowsAffected;
		return $resultArray;
	}





	protected function action_copyPurchaseOrders($args){
		$ids = isset($args['IDs']) ? $args['IDs'] : array();  //getRowsWhereColumnIn($tableName, $column, $idArr)
		//$this->verifyArrayOfIntegers($ids); //TODO

		//TODO FOR ALL THE 'Items' as arguments, refactor tests and implementation.
		if (empty($ids)) {
			$items = $args['Items'];
			$ids = MA::subArrayByKey($items, 'id');
		}
		$insertedIDs = ComplexQueries::CopyJoinedRowsByIDs('inventory_purchaseorder', 'purchaseorder_items', 'purchaseOrderID', $ids);
		$lastID = $insertedIDs['parentIDs']['InsertID'];
		CQ::updateArray('inventory_purchaseorder', array(
			'receiveDate' => '0000-00-00 00:00:00',
			'orderDate' => gmdate("Y-m-d H:i:s")
		), 'WHERE `id`= ' . $lastID);

		$purchaseOrderItems = CQ::getWhereColumnEquals("purchaseorder_items", "purchaseOrderID", $lastID);
		foreach ($purchaseOrderItems as $poItem) {
			CQ::updateArray('purchaseorder_items', array(
				'quantityReceived'=>0
			), 'WHERE `id`= ' . $poItem['id']);
		}

		$POIds =  MarshallingQueries::convertIDAndCountToCommaSeparatedIDs($insertedIDs['parentIDs']);
		$POIIds =  MarshallingQueries::convertIDAndCountToCommaSeparatedIDs($insertedIDs['childIDs']);

		return array("InsertedPurchaseOrderIDs"=>$POIds, "InsertedPurchaseOrderItemIDs"=>$POIIds);
	}



	protected function action_copyInventoryItems($args){
		$ids = empty($args['IDs']) ? array() : $args['IDs'];
		$optionalReplaces = empty($args['Replaces']) ? array() : $args['Replaces'];
		if (empty($ids)) {
			$ids = array();
			$optionalReplaces = array();
			foreach ($args['Items'] as $currItem) {
				$id = $currItem['id'];
				$ids[] = $id;
				unset($currItem['id']);
				$currItem = MA::unsetOptionalArguments($currItem);
				$optionalReplaces[$id] = $currItem;
			}
		}
		$column2ValueReplacementsByID = array();
		foreach ($ids as $currID) {
			$column2ValueReplacementsByID[$currID] = isset($optionalReplaces[$currID]) ? $optionalReplaces[$currID] : array();
		}
		$insertIDs = ComplexQueries::CopyWithValueReplacements('inventory_items', $column2ValueReplacementsByID);
		return array("InsertIDs" => $insertIDs);
	}

	private function insertAudits($audits) {
		$rowResult = array();
		for ($i = 0; $i < count($audits); $i++) {

			if ($audits[$i]['action'] === 'waste' || $audits[$i]['action'] === 'waste_overage') {
				$rowResult[] = InventoryQueries::insertAudit($audits[$i]);
			} else {
				$item = InventoryQueries::getInventoryItemByID($audits[$i]['inventoryItemID']);
				$costPerSalesUnit = $this->getCostForItemID($audits[$i]['inventoryItemID']);
				if ($audits[$i]['action'] === 'receiveWithoutPO') {
					$rowResult[] = InventoryQueries::insertAudit($audits[$i]);
				} else if ($audits[$i]['action'] === '86count') {
					$audits[$i]['cost'] = $this->getCostForSalesItemID($audits[$i]['inventoryItemID'], $audits[$i]['quantity']);
					if (isset($audits[$i]['unitID']) && $audits[$i]['unitID'] === 0) {
						$audits[$i]['unitID'] = $item[0]['salesUnitID'];
					}
					$rowResult[] = InventoryQueries::insertAudit($audits[$i]);
				} else {
					if (!empty($item[0])) {
						if ($item[0]['salesUnitID'] != $audits[$i]['unitID']) {
							$amountUsed = self::unitSystemConversion($item[0]['salesUnitID'], $audits[$i]['unitID'], $audits[$i]['quantity']);
							$audits[$i]['cost'] = $costPerSalesUnit * $amountUsed;
						} else {
							$audits[$i]['cost'] = $this->getCostForSalesItemID($audits[$i]['inventoryItemID'], $audits[$i]['quantity']);
						}
					}
					$rowResult[] = InventoryQueries::insertAudit($audits[$i]);
				}
			}
		}
		return $rowResult;
	}

	/**
	 * This function is used to calculate used inventory items 86count details.
	 * @param number $id
	 * @param number $orderQuantity
	 * @return number $totalcostItems
	 */
	protected function getCostForSalesItemID($id, $orderQuantity) {
		$totalcostItems = 0;
		$item = InventoryQueries::getInventoryItemByID($id);
		if (!$item) {
			return 0;
		}
		$settingQuery = InventoryQueries::getInventorySetting("inventory_costing_method");
		if ($settingQuery[0]['value'] == 'average_cost_periodic') {
			$valueForItem = $this->getValueByID($id);
			if ($item[0]['quantityOnHand'] > 0) {
				$totalcostItems = $valueForItem / $item[0]['quantityOnHand'];
			}
		}
		else if ($settingQuery[0]['value'] == 'fifo') {
			$timeLine = InventoryQueries::getInventoryAuditInfo($id);
			$itemId = array($id);
			$previousSaleInfo = InventoryQueries::getInventory86count($itemId);
			$previousSale = isset($previousSaleInfo[0]['total_sales']) ? $previousSaleInfo[0]['total_sales'] : 0;

			$purchaseQuantity = 0;
			$remainQuantity = 0;
			foreach ($timeLine as $costEvent) {
				$purchaseQuantity += $costEvent['quantity'];
				if ($previousSale >= $purchaseQuantity) {
					continue;
				} else {
					$availableItem = $purchaseQuantity - $previousSale;
					$remainQuantity = ($orderQuantity > $availableItem) ? $availableItem : $orderQuantity;
					$totalcostItems += $remainQuantity * $costEvent['cost'];
					$orderQuantity -= $remainQuantity;
					$previousSale += $availableItem;

					if ($orderQuantity <= 0) {
						break;
					}
				}
			}
		}
		 else{
			$totalcostItems = $totalQty * $item[0]['recentPurchaseCost'];
		}
		return $totalcostItems;
	}

	protected function action_createUnitCategories($args){
		$rowResult = InventoryQueries::insertUnitCategories($args['Items']);
		return json_encode($rowResult);
	}

	protected function action_createReceiveWithoutPOAudit($args){
		return json_encode($this->insertAudits($args['Items']));
	}

	protected function action_createReconciliationAudit($args){
		return json_encode($this->insertAudits($args['Items']));
	}

	protected function action_createTransfersUpdateAudit($args){
		return json_encode($this->insertAudits($args['Items']));
	}

	protected function action_createTransfersReceivedAudit($args){
		return json_encode($this->insertAudits($args['Items']));
	}

	protected function action_createWasteAudit($args){
		return json_encode($this->insertAudits($args['Items']));
	}

	protected function action_createAudits($args){
		return json_encode($this->insertAudits($args['Items']));
	}

	protected function action_getAudits(){
		$rowResult = CQ::getAllRows('inventory_audit');
		return $rowResult;
	}

	protected function action_getCriteriaAudits($args){
		$whereCoulmns = array();
		$whereVals = array();
		$columnNames = array_keys($args['Items'][0]);
		if (!empty($args['Items'])) {
			foreach ($args['Items'] as $items ) {
				$whereVals = array_values($items);
				$rowResult = CQ::getWhereColumnsEquals('inventory_audit', $columnNames, $whereVals);
				$inventoryAudits[] = $rowResult;
			}
		}
		return $inventoryAudits;
	}

	protected function action_addPurchaseOrdersOLD($args){
		$rowResult = InventoryQueries::insertPurchaseOrders($args);
		return json_encode($rowResult);
	}

	protected function action_addPurchaseOrders($args) {
		$returnData = array();

		$purchaseOrders = isset($args['Items']) ? $args['Items'] : null;

		foreach ($purchaseOrders as $i => $purchaseOrder) {
			// Save the array element containing Inventory PurchaseOrderItems so we can delete the key and leave only insertable elements
			$purchaseOrderItems = $purchaseOrder['purchaseOrderItems'];
			unset($purchaseOrder['purchaseOrderItems']);

			// Insert Inventory PurchaseOrder row
			$purchaseOrderColumnNames  = array_keys($purchaseOrder);
			$purchaseOrderColumnValues = array_values($purchaseOrder);
			$insertOk = CQ::insertArray('inventory_purchaseorder', $purchaseOrderColumnNames, array($purchaseOrderColumnValues));

			// Get inserted Inventory PurchaseOrder row ID
			$purchaseOrderID = DBConnection::clientInsertID();

			// Insert Inventory PurchaseOrderItems
			if ( $insertOk && $purchaseOrderID ) {
				$purchaseOrderItemIDs = array();
				foreach ($purchaseOrderItems as $j => $purchaseOrderItem) {
					$purchaseOrderItem['purchaseOrderID'] = $purchaseOrderID;
					$purchaseOrderItemColumnNames  = array_keys($purchaseOrderItem);
					$purchaseOrderItemColumnValues = array_values($purchaseOrderItem);
					$insertItemOk = CQ::insertArray('purchaseorder_items', $purchaseOrderItemColumnNames, array($purchaseOrderItemColumnValues));

					// Get inserted Inventory PurchaseOrderItem row ID
					$purchaseOrderItemID = DBConnection::clientInsertID();
					$insertedPurchaseOrderItemIDs[] = array('id' => $purchaseOrderItemID);
				}

				// Return inserted row IDs in structure paralleling delivery
				$returnData[] = array('id' => $purchaseOrderID, 'items' => $insertedPurchaseOrderItemIDs);
			}
		}

		return array('RowsInserted' => $returnData);
	}

	protected function action_createPurchaseOrders($args){
		return $this->action_addPurchaseOrders($args);
	}
	protected function action_addPurchaseOrdersItems($args){
		$rowResult = InventoryQueries::insertPurchaseOrdersItems($args);
		return json_encode($rowResult);
	}

	protected function action_createPurchaseOrderWithItems($args){

		//Attempt to insert the purchase order.
		$purchaseOrder = $args['PurchaseOrder'];
		$columnNames = array_keys($purchaseOrder);
		$rowValue = array_values($purchaseOrder);
		$result = CQ::insertArray('inventory_purchaseorder', $columnNames, array($rowValue));
		if (!$result) {
			throw new APIException("Failed to insert purchase order items.");
		}
		$idResultAndRowsUpdated = resultToInsertIDUpdatedRows($result);
		$purchaseOrderInsertID = $idResultAndRowsUpdated['InsertID'];
		$rowsUpdated = $idResultAndRowsUpdated['RowsUpdated'];
		if (intval($purchaseOrderInsertID) < 1) {
			throw new APIException("Failed to insert purchase order items.");
		}

		//Attempt to insert the purchase order items, tying them to their parent purchase order that was just inserted.
		$purchaseOrderItems = $args['PurchaseOrderItems'];
		$purchaseOrderItemsColumnNames = array_keys($purchaseOrderItems[0]);
		$purchaseOrderItemRows = array();
		foreach ($purchaseOrderItemsColumnNames as $currPurchaseOrderItemRow) {
			$purchaseOrderItemRows[] = array_values($currPurchaseOrderItemRow);
		}
		foreach ($purchaseOrderItemRows as &$currRow) {
			$currRow['purchaseOrderID'] = $purchaseOrderInsertID;
		}
		$result = CQ::insertArray('purchaseorder_items', $purchaseOrderItemsColumnNames, array($purchaseOrderItemRows));
		if (!$result) {
			//TODO undo original insert of parent purchase order, maybe just mark as deleted.
			throw new APIException("Failed to insert purchase order items.");
		}
		$idResultAndRowsUpdated = resultToInsertIDUpdatedRows($result);
		$itemsInsertID = $idResultAndRowsUpdated['InsertID'];
		$rowsUpdated = $idResultAndRowsUpdated['RowsUpdated'];
		if (!$itemsInsertID || ! $rowsUpdated) {
			//TODO UNDO PURCHASE ORDER INSERT, OR EXPLAIN ERROR, ETC.
			throw new APIException("Failed to insert purchase order items.");
		}

		return array('PurchaseOrder' => array('InsertID' => $purchaseOrderInsertID), 'PurchaseOrderItems' => array('InsertID' => $itemsInsertID, 'Updated' => $rowsUpdated));
	}


	protected function action_createPurchaseOrderItems($args){
		return $this->action_addPurchaseOrderItems($args);
	}
	protected function action_addPurchaseOrderItems($args){
		$columnNames = array_keys($args['Items'][0]);
		$insertRows = array();
		foreach ($args['Items'] as $currRow) {
			$insertRows[] = array_values($currRow);
		}
		$response = CommonQueries::insertArray('purchaseorder_items', $columnNames, $insertRows);
		if (!$response) {
			throw new APIException("Failed to insert inventory items.");
		}
		return MA::resultToInsertIDUpdatedRows($response);
	}


	protected function action_createUnits($args){
		$items = $args['Items'];
		$fargs = array('Units' => $items);
		return $this->action_addInventoryUnits($fargs);
	}
	protected function action_addInventoryUnits($args){
		$columnNames = array_keys($args['Units'][0]);
		$insertRows = array();
		foreach ($args['Units'] as $currRow) {
			$insertRows[] = array_values($currRow);
		}
		CommonQueries::insertArray('inventory_unit', $columnNames, $insertRows);
		return array("RowsInserted" => count($insertRows));
	}

	protected function action_linkVendorItemsToInventoryItems($args){
		//"Items":[{"inventoryItemID":"5","vendorItemID":"1"},{"inventoryItemID":"5","vendorItemID":"2"},{"inventoryItemID":"5","vendorItemID":"3"}]
		$items = $args['Items'];
		foreach ($items as $currItem) {
			$fargs = array();
			$fargs['inventoryItemID'] = $currItem['inventoryItemID'];
			$fargs['vendorID'] = $currItem['vendorID'];
			$fargs['SKU'] = $currItem['SKU'];
			$fargs['BIN'] = $currItem['BIN'];
			$fargs['barcode'] = $currItem['barcode'];
			InventoryQueries::insertVendorItem($fargs);
		}
	}

	protected function action_getLinkedVendorItemsForInventoryItems($args){
		$args = $args['Items'];
		$rowResults = array();
		for ($i = 0; $i < count($args); $i++) {
			$iid = (isset($args[$i]['id'])) ? $args[$i]['id'] : $args[$i];
			//$this->verifyActionArguments("AuthenticateSession", $args, $requiredParameters);
			$rowResults[] = InventoryQueries::getLinkedVendorItemsByItemID($iid);
		}
		return $rowResults;
	}

	protected function action_linkVendorItemToInventoryItem($args){
		$result = InventoryQueries::insertVendorItem($args);
	   return $result;
	}

	protected function action_updateInventoryItems($args){
		$args = $args['Items'];
		$result = array();
		$redisController = new InventoryRedisController();
		$inventoryCache = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
		for ($i = 0; $i < count($args); $i++) { //May update multiple rows, so each loop is a row.
			$inventoryItemID = $args[$i]['id'];
			$inventoryItemIDs[]['id'] = $inventoryItemID;
			$isArchived = CQ::_deletedFlagIs1('inventory_items', $inventoryItemID);
			if (($isArchived  === true && $args[$i]['_deleted'] == 0) || $isArchived === false) {
				$inventoryItemRow = CQ::getRowByID('inventory_items', $inventoryItemID);
				$this->verifyNotEmpty($inventoryItemRow, 'updateInventoryItem', 'Missing InventoryItemID');
				$updateArray = $args[$i];
				if (empty($inventoryItemRow['primaryVendorID']) && !empty($updateArray['primaryVendorID'])) {
					$vendorItem = array(
						'inventoryItemID' => $inventoryItemRow['id'],
						'vendorID' => $updateArray['primaryVendorID'],
						'SKU' => '',
						'BIN'=>'',
						'bardcode'=>''
					);
					$newVendorItem = InventoryQueries::insertVendorItem($vendorItem);
				}
				if (!isset($updateArray['salesUnitID'])) {
					$updateArray['salesUnitID'] = $inventoryItemRow['salesUnitID'];
				}
				if (!isset($updateArray['recentPurchaseUnitID']) ) {
					$updateArray['recentPurchaseUnitID'] = $inventoryItemRow['recentPurchaseUnitID'];
				}
				if ($updateArray['salesUnitID'] != $updateArray['recentPurchaseUnitID'] && isset($updateArray['quantityOnHand'])) {
					$updateArray['quantityOnHand'] = self::unitSystemConversion($updateArray['salesUnitID'], $updateArray['recentPurchaseUnitID'], $updateArray['quantityOnHand']);
				}
				if ($inventoryItemRow['salesUnitID'] != $updateArray['salesUnitID']) {
					$reOrderQuantityPurchaseUnit = self::roundConversion($updateArray['recentPurchaseUnitID'], $inventoryItemRow['salesUnitID'], $inventoryItemRow['reOrderQuantity']);
					$reOrderQuantityToConvert = isset($updateArray['reOrderQuantity'])?$updateArray['reOrderQuantity']:$reOrderQuantityPurchaseUnit;
					$updateArray['reOrderQuantity'] = self::unitSystemConversion($updateArray['salesUnitID'], $updateArray['recentPurchaseUnitID'], $reOrderQuantityToConvert);
				}
				else if (isset($updateArray['reOrderQuantity'])) {
					if ($updateArray['reOrderQuantity'] != self::roundConversion($inventoryItemRow['salesUnitID'], $inventoryItemRow['recentPurchaseUnitID'], $inventoryItemRow['reOrderQuantity']) ) {
						$updateArray['reOrderQuantity'] = self::unitSystemConversion($updateArray['salesUnitID'], $updateArray['recentPurchaseUnitID'], $updateArray['reOrderQuantity']);
					}
				}
				//Check to see if a vendor item exists for hte new primary vendor, if not, then we create one so that it can be easily added to a purchase order.
				if (isset($updateArray['primaryVendorID']) && $updateArray['primaryVendorID'] != $inventoryItemRow['primaryVendorID']) {
					$exists = false;
					$vendorItems = InventoryQueries::getLinkedVendorItemsByItemID($inventoryItemRow['id']);
					foreach ($vendorItems as $vendorItem) {
						if ($vendorItem['vendorID'] == $updateArray['primaryVendorID']) {
							$exists = true;
						}
					}
					if (!$exists) {
						$vendorItem = array(
							'inventoryItemID' => $inventoryItemRow['id'],
							'vendorID' => $updateArray['primaryVendorID'],
							'SKU' => '',
							'BIN'=>'',
							'bardcode'=>''
						);
						$newVendorItem = InventoryQueries::insertVendorItem($vendorItem);
					}
				}
				$result[] = CQ::updateArray('inventory_items', $updateArray, "where `id`='" . intval($inventoryItemID) . "'");
				unset($inventoryCache[$updateArray['id']]);
			} else {
				$result[] = array("failure"=>"InventoryItem(s) is/are archived. Archived items may not be updated without un-archiving them. ID: ". $inventoryItemID);
			}
		}
		$redisController->setInventory($inventoryCache);
		return $result;
	}


	protected function action_createStorageLocations($args){
		$fargs = array('StorageLocations' => $args['Items']);
		$this->action_addStorageLocations($fargs);
	}
	protected function action_addStorageLocations($args){
		$columnNames = array_keys($args['StorageLocations'][0]);
		$insertRows = array();
		foreach ($args['StorageLocations'] as $currRow) {
			$insertRows[] = array_values($currRow);
		}
		CommonQueries::insertArray('storage_locations', $columnNames, $insertRows);
	}



	protected function action_createCategories($args){
		$fargs = array('InventoryCategories' => $args['Items']);
		$this->action_addInventoryCategories($fargs);
	}
	protected function action_addInventoryCategories($args){
		$columnNames = array_keys($args['InventoryCategories'][0]);
		$insertRows = array();
		foreach ($args['InventoryCategories'] as $currRow) {
			$insertRows[] = array_values($currRow);
		}
		$result = CommonQueries::insertArray('inventory_categories', $columnNames, $insertRows);
		return MA::resultToInsertIDUpdatedRows($result);
	}


	//  Category
	protected function action_getAllCategories(){
		$rowResult = InventoryQueries::getAllCategories();
		return $rowResult;
	}
	protected function action_getAllCategoriesWithArchives(){
		$rowResult = InventoryQueries::getAllCategoriesWithArchives();
		return $rowResult;
	}

	//  Stock
	protected function action_getAllStocks(){
		$rowResult = InventoryQueries::getAllStocks();
		return $rowResult;
	}

	//  Vendor
	protected function action_getAllVendors($args = array()){
		$rowResult = InventoryQueries::getAllVendors($args);
		return $rowResult;
	}
	protected function action_getAllVendorsWithArchives(){
		$rowResult = InventoryQueries::getAllVendorsWithArchives();
		return $rowResult;
	}

	// Used by action_addQuantitiesToInventoryItems(), action_wasteInventoryItems()
	protected function action_addOrSubtractQuantitiesToInventoryItems($args, $factor){
		$items = $args['Items'];
		$rowsUpdated = 0;
		$redisController = new InventoryRedisController();
		$cachInventoryDetails = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
		foreach ($items as $currItem) {
			if (isset($currItem['unitID'])) {
				$inventoryItem = InventoryQueries::getInventoryItemByID($currItem['id']);
				$sUOM = $inventoryItem[0]['salesUnitID'];
				if ($sUOM != $currItem['unitID']) {
					$currItem['quantity'] = self::unitSystemConversion($sUOM, $currItem['unitID'], $currItem['quantity']);
				}
			}

			if (isset($currItem['id'])) {
				$result = CQ::addOrSubtractFromColumn('inventory_items', 'quantityOnHand', $currItem['quantity'], $factor, ' where `id`='.intval($currItem['id']));
				if ($result['RowsUpdated'] > 0) {
					unset($cachInventoryDetails[$currItem['id']]);
				}
			}
			$rowsUpdated += DBConnection::clientAffectedRows();
		}
		$redisController->setInventory($cachInventoryDetails);
		return $rowsUpdated;
	}

	protected function action_addQuantitiesToInventoryItems($args){
		$rowsUpdated = $this->action_addOrSubtractQuantitiesToInventoryItems($args, 1);
		return array("RowsUpdated" => $rowsUpdated);
	}

	protected function action_wasteInventoryItems($args){
		$rowsUpdated = $this->action_addOrSubtractQuantitiesToInventoryItems($args, -1);
		return array("RowsUpdated" => $rowsUpdated);
	}

	protected function action_getInventoryItemsByID($args){
		$id = $args['Items'][0]['id'];
		//$this->verifyActionArguments("AuthenticateSession", $args, $requiredParameters);
		$rowResult = CQ::getRowByID('inventory_items', $id);
		/*removing conversions on lowQuantity
		 * if (!empty($rowResult['lowQuantity'])) {
			$rowResult['lowQuantity'] = round(self::unitSystemConversion($rowResult['recentPurchaseUnitID'], $rowResult['salesUnitID'], $rowResult['lowQuantity']),2);
		}*/
		if (!empty($rowResult['reOrderQuantity'])) {
			$rowResult['reOrderQuantity'] = self::roundConversion($rowResult['recentPurchaseUnitID'], $rowResult['salesUnitID'], $rowResult['reOrderQuantity']);
		}
		$rowResult['convertedQuantityOnHand'] = self::unitSystemConversion($rowResult['recentPurchaseUnitID'], $rowResult['salesUnitID'], $rowResult['quantityOnHand']);
		return $rowResult;
	}

	// Get inventory items for more than one ids
	protected function action_getInventoryItemsByIDs($args){
		$response = array();
		if (!empty($args['Items'])) {
			$idsArr = array();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}
			$response = CQ::getRowsWhereColumnIn('inventory_items', 'id', $idsArr);

		}
		foreach ($response as &$item) {
			/*removing conversions on lowquantity
			if (!empty($item['lowQuantity'])) {
				$item['lowQuantity'] = round(self::unitSystemConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['lowQuantity']),2);
			}
			*/
			if (!empty($item['reOrderQuantity'])) {
				$item['reOrderQuantity'] = self::roundConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['reOrderQuantity']);
			}
		}
		return $response;
	}

	protected function action_getVendorsByID($args){
		$id = $args['Items'][0]['id'];
		//$this->verifyActionArguments("AuthenticateSession", $args, $requiredParameters);
		$rowResult = CQ::getRowByID('vendors', $id);
		return $rowResult;
	}

	protected function action_getInventoryWasteCosts($args){
		$yesterdayStrDate = strtotime("-1 day", time());
		$startDate = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m', $yesterdayStrDate), date('d', $yesterdayStrDate), date('Y', $yesterdayStrDate)));
		$endDate = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m'), date('d'), date('Y')));
		$rowResults = InventoryQueries::getAuditsByAction('waste', $startDate, $endDate);
		$todayDate = date('Y-m-d');
		$yesterdayDate = date('Y-m-d', $yesterdayStrDate);
		$wasteCostToday = 0.00;
		$wasteCostYesterday = 0.00;
		foreach ($rowResults as $i => $audit) {
			$auditDate = substr($audit['datetime'], 0, 10);
			if ($auditDate == $todayDate) {
				$wasteCostToday += floatval($audit['cost']);
			}
			else if ($auditDate == $yesterdayDate) {
				$wasteCostYesterday += floatval($audit['cost']);
			}
		}
		return array('wasteCostToday' => $wasteCostToday, 'wasteCostYesterday' => $wasteCostYesterday);
	}


	protected function action_getAllTransfers($args = array()){
		$rowResult = InventoryQueries::getAllTransfers($args);
		return $rowResult;
	}

	protected function action_getAllPendingTransfers(){
		$rowResult = InventoryQueries::getAllPendingTransfers();
		return $rowResult;
	}

	protected function action_getAllReceivedTransfers(){
		$rowResult = InventoryQueries::getAllReceivedTransfers();
		return $rowResult;
	}

	protected function action_getAllInventoryItems($args = array()){
		$rowResult = InventoryQueries::getAllInventoryItems($args);
		foreach ($rowResult as &$item) {
			/*removing conversions on low quantity
			if (!empty($item['lowQuantity'])) {
				$item['lowQuantity'] = round(self::unitSystemConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['lowQuantity']),2);
			}
			*/
			if (!empty($item['reOrderQuantity'])) {
				$item['reOrderQuantity'] = self::roundConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['reOrderQuantity']);
			}
			$purchaseUnitQuantity = self::unitSystemConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['quantityOnHand']);

			if ($purchaseUnitQuantity > 0) {
				$item['wasteCost'] = (self::getCostForItemID($item['id']) * $item['quantityOnHand'])/$purchaseUnitQuantity;
			}
			else{
				$item['wasteCost'] = 0;
			}

		}

		return $rowResult;
	}

	protected function action_getAllArchivedInventoryItems(){
		$rowResult = CQ::getWhereColumnEquals('inventory_items', '_deleted', 1);
		return $rowResult;
	}

	protected function action_getOrderedItemsForVendorID($args){
		$rowResult = array();
		$vendors = isset($args['Items']) ? $args['Items'] : array();
		for ($i = 0; $i < count($vendors); $i++) {
			$vendor = $vendors[$i];
			$rowResult[] = InventoryQueries::getOrderedItemsForVendorID($vendor['id']);
		}
		return $rowResult;
	}

	protected function action_getAllVendorItems(){
		$rowResult = InventoryQueries::getAllVendorItems();
		return $rowResult;
	}

	protected function action_getPurchaseOrderItems($args){
		$rowResult = array();
		$pos = isset($args['Items']) ? $args['Items'] : array();
		for ($i = 0; $i < count($pos); $i++) {
			$po = $pos[$i];
			$rowResult = InventoryQueries::getItemsforPurchaseOrder($po['id']);
		}
		return $rowResult;
	}

	protected function action_getPurchaseOrderDetails($args){
		$Items = $args['Items'];
		$resultArray = array();
		foreach ($Items as $item) {
			$resultArray[$item['id']] = InventoryQueries::getPurchaseOrderDetails($item['id']);
		}

		$resultArray[$item['id']] = InventoryPurchaseOrderViewCompiler::addStatusesToPurchaseOrders($resultArray[$item['id']]);
		return $resultArray;
	}
	/**
	 * Get all units records based upon categoryID
	 * @param $args [{"id":682},{"id":685}] list of categoryID
	 * @return json string {
		"DataResponse": {
		"DataResponse": [
		{
			"id": "11",
			"name": "PostmanUnitA",
			"symbol": "PUA",
			"categoryID": "34",
			"conversion": "1",
			"_deleted": "0"
		}
	 */
	protected function action_getAllUnitsByCategory($args){
		$id = $args['Items'][0]['id'];
		$rowResult = InventoryQueries::getAllUnitsByCategory($id);
		return $rowResult;
	}

	protected function action_getUnitCategories(){
		$rowResult = CQ::getAllRows('unit_category');
		return $rowResult;
	}

	protected function action_getAllUnits(){
		$rowResult = InventoryQueries::getAllUnits();
		return $rowResult;
	}

	protected function action_getAllPurchaseOrderItems(){
		$rowResult = CQ::getAllRows('purchaseorder_items');
		return $rowResult;
	}

	protected function action_getPurchaseOrder($args){
		$id = $args['Items'][0]['id'];
		$rowResult = CQ::getRowByID("inventory_purchaseorder", $id);
		return $rowResult;
	}

	protected function action_getItemsForPurchaseOrder($args){
		$id = $args['Items'][0]['id'];
		$rowResult = InventoryQueries::getItemsForPurchaseOrder($id);
		return $rowResult;
	}
	protected function action_getAllPurchaseOrders($args = array()){
		$rowResult = CQ::getAllRows('inventory_purchaseorder','','',$args);
		return $rowResult;
	}

	protected function action_getPurchaseOrdersByDate($args){
		$date = $args['date'];
		$filter = $args['filter'];
		$rowResult = InventoryQueries::getPurchaseOrdersByDate($date, $filter);
		return $rowResult;
	}

	protected function action_getExpectedPurchaseOrders($args){
		$expectedReceivableOrdersResult = InventoryQueries::getExpectedPurchaseOrders();
		$totalItemCost = 0.00;
		$totalItemsQuantity = 0;
		foreach ($expectedReceivableOrdersResult as $expectedReceivableOrders) {
			$totalItemCost += floatval($expectedReceivableOrders['expectedReceivable']) * floatval($expectedReceivableOrders['purchaseUnitCost']);
			$totalItemsQuantity += $expectedReceivableOrders['expectedReceivable'];
		}
		$rowResult['totalItemCost'] = $totalItemCost;
		$rowResult['purchaseOrderItems'] = $totalItemsQuantity;
		return $rowResult;
	}

	protected function action_receivePurchaseOrders($args){
		$purchaseOrders = $args['Items'];
		$rowsUpdated = InventoryQueries::receivePurchaseOrders($purchaseOrders);
		return array("RowsUpdated" => $rowsUpdated);
	}

	protected function action_updatePurchaseOrders($args){
		$purchaseOrders = $args['Items'];
		$rowsUpdated = InventoryQueries::updatePurchaseOrderStatus($purchaseOrders);
		return array("RowsUpdated" => $rowsUpdated);
	}

	protected function action_getAllPurchaseOrdersForVendorID($args){
		$rowResult = array();
		$vendors = isset($args['Items']) ? $args['Items'] : array();
		for ($i = 0; $i < count($vendors); $i++) {
			$vendor = $vendors[$i];
			$rowResult = InventoryQueries::getAllPurchaseOrdersForVendorID($vendor['id']);
		}

		return $rowResult;
	}

	protected function action_getAllLowStockInventoryItems(){
		$rowResult = InventoryQueries::getAllLowStockItems();
		return $rowResult;
	}

	protected function action_getAllItemsForReOrder(){
		$rowResult = InventoryQueries::getAllItemsForReOrder();
		return $rowResult;
	}

	protected function action_getAllTransferItems(){
		$rowResult = InventoryQueries::getAllTransferItems();
		return $rowResult;
	}

	protected function action_getTransferItemByID($args){
		$result = array();
		foreach ($args['Items'] as $itemArgs) {  // May update multiple rows, so each loop is a row.
			$result[] = InventoryQueries::getTransferItemByID($itemArgs['id']);
		}
		return $result;
	}

	protected function action_getTransferItemByTransferIDAndInventoryItemID($args){
		$result = array();
		foreach ($args['Items'] as $itemArgs) {  // May update multiple rows, so each loop is a row.
			$result[] = InventoryQueries::getTransferItemByTransferIDAndInventoryItemID($itemArgs);
		}
		return $result;
	}

	protected function action_getTransferByID($args){
		$results = array();

		$transfers = isset($args['Items']) ? $args['Items'] : array();
		for ($i = 0; $i < count($transfers); $i++) {
			$transfer = $transfers[$i];
			$transferID = isset($transfer['id']) ? $transfer['id'] : 0;
			$transfer = InventoryQueries::getTransferByID($transferID);
			$transfer['transferItems'] = InventoryQueries::getTransferItemsByTransferID($transferID);
			$results[] = $transfer;
		}
		return $results;
	}

	protected function action_getCostingMethod(){
		$result = InventoryQueries::getCostingMethod();
		return $result;
	}

	protected function action_getMonetarySymbol(){
		$result = InventoryQueries::getMonetarySymbol();
		return $result;
	}

	protected function action_getMonetarySymbolAlignment(){
		$result = InventoryQueries::getMonetarySymbolAlignment();
		return $result;
	}

	protected function action_getMonetaryDecimalPrecision(){
		$result = InventoryQueries::getMonetaryDecimalPrecision();
		return $result;
	}

	protected function action_getMonetaryDecimalSeparator(){
		$result = InventoryQueries::getMonetaryDecimalSeparator();
		return $result;
	}

	protected function action_getMonetaryThousandsSeparator(){
		$result = InventoryQueries::getMonetaryThousandsSeparator();
		return $result;
	}

	protected function action_getMonetaryInformation(){
		$result = InventoryQueries::action_getMonetaryInformation();
		return $result;
	}

	// InventoryTransfers

	protected function action_createTransfers($args) {
		$returnData = array();
		$inventoryTransfers = isset($args['Items']) ? $args['Items'] : null;
		$enterpriseIDAndTitleResult =  EnterpriseQueries::getRestaurantIDAndTitleByDataname($this->m_dataname);
		$restaurantID = $enterpriseIDAndTitleResult[0]['restaurantid'];
		foreach ($inventoryTransfers as $i => $inventoryTransfer) {
			if ($inventoryTransfer['accepted'] == 0 || $inventoryTransfer['accepted'] == 1) {
				$inventoryTransfer['accepted_ts'] = gmdate("Y-m-d H:i:s");
			}

			// Save the array element containing InventoryTransferItems so we can delete the key and leave only insertable elements
			$inventoryTransferItems = $inventoryTransfer['transferItems'];

			$originRestaurantID = $inventoryTransfer['originRestaurantID'];
			$destinationRestaurantID = $inventoryTransfer['destinationRestaurantID'];
			$useID = null;
			if ($destinationRestaurantID == $restaurantID) {
				$useID = $originRestaurantID;
			} else {
				$useID = $destinationRestaurantID;
			}
			DBConnection::startTransferConnection($useID);

			unset($inventoryTransfer['transferItems']);

			// Insert InventoryTransfer row
			$inventoryTransferColumnNames  = array_keys($inventoryTransfer);
			$inventoryTransferColumnValues = array_values($inventoryTransfer);
			$inventoryTransfer['transferNumber'] = "";
			$insertOk = CQ::insertArray('inventory_transfer', $inventoryTransferColumnNames, array($inventoryTransferColumnValues));
			$inventoryTransferID = DBConnection::clientInsertID();// Get inserted InventoryTransfer row ID This needs to be called after the insertArray is called.
			$lastTransferID = (CQ::getLastInsertID('inventory_transfer')['id']);
			$inventoryTransfer['transferNumber'] = $restaurantID."-".$lastTransferID;
			$updateArray = array('transferNumber'=>$inventoryTransfer['transferNumber']);
			$updateOk = CQ::updateArray('inventory_transfer', $updateArray, "WHERE `id` = {$lastTransferID}");
			$inventoryTransferColumnNames  = array_keys($inventoryTransfer);
			$inventoryTransferColumnValues = array_values($inventoryTransfer);

			$foreignInsertItemOK = CQ::insertTransferArray('inventory_transfer', $inventoryTransferColumnNames, array($inventoryTransferColumnValues));
			$inventoryTransferForeignID = DBConnection::transferLocationInsertID();
			// Insert InventoryTransferItems
			if ($insertOk &&  $updateOk && $inventoryTransferID) {
				$insertedTransferItemIDs = array();
				$itemsToBeDeducted = array();
				$itemsToBeDeducted["Items"] = array();
				foreach ($inventoryTransferItems as $j => $inventoryTransferItem) {
					$inventoryTransferItem['transferID'] = $inventoryTransferID;
					$inventoryTransferItemColumnNames  = array_keys($inventoryTransferItem);
					$inventoryTransferItemColumnValues = array_values($inventoryTransferItem);
					$inventoryTransferItem['transferItemNumber'] = "";
					$insertItemOk = CQ::insertArray('transfer_items', $inventoryTransferItemColumnNames, array($inventoryTransferItemColumnValues));
					$inventoryTransferItemID = DBConnection::clientInsertID();

					$lastID = (CQ::getLastInsertID('transfer_items')['id']);
					$inventoryTransferItem['transferItemNumber'] = $restaurantID."-".$lastID;
					$updateArray = array('transferItemNumber'=>$inventoryTransferItem['transferItemNumber']);
					$updateOk = CQ::updateArray('transfer_items', $updateArray, "WHERE `id` = {$lastID}");
					$inventoryTransferItemColumnNames  = array_keys($inventoryTransferItem);
					$inventoryTransferItemColumnValues = array_values($inventoryTransferItem);
					// Get inserted InventoryTransferItem row ID
					$foreignInsertItemOK = CQ::insertTransferArray('transfer_items', $inventoryTransferItemColumnNames, array($inventoryTransferItemColumnValues));
					$inventoryForeignLocationTransferItemID = DBConnection::transferLocationInsertID();
					$updateItemArray = array("transferID"=>$inventoryTransferForeignID);

					$updateOk = CQ::updateTransferLocationArray('transfer_items', $updateItemArray, "WHERE `id` = {$inventoryForeignLocationTransferItemID}");
					$insertedTransferItemIDs[] = array('id' => $inventoryTransferItemID, 'foreignLocationItemID'=>$inventoryForeignLocationTransferItemID);
					$currentItem = array (
							'id'        => $inventoryTransferItem['inventoryItemID'],
							'quantity'  => $inventoryTransferItem['quantityTransferred'],
							'unitID'    => $inventoryTransferItem['transferUnitID']
						);
					array_push($itemsToBeDeducted["Items"], $currentItem);
				}
				$rowsUpdated = $this->action_addOrSubtractQuantitiesToInventoryItems($itemsToBeDeducted, -1);
				// Return inserted row IDs in structure paralleling delivery
				$returnData[] = array('id' => $inventoryTransferID, 'foreignLocation_id'=>$inventoryTransferForeignID,'items' => $insertedTransferItemIDs);
			} else {
				return ("failed to perform transfer insert");
			}
		}
		DBConnection::closeTransferConnection();
		return array('RowsInserted' => $returnData);
	}

	protected function action_getInventoryItemsByTransferItemNumbers($args){
		$item = isset($args['Items']) ? $args['Items'] : null;
		$explodedTransferItemNumber = explode("-", $item['transferItemNumber']);
		$restID = $explodedTransferItemNumber[0];
		$itemID = $explodedTransferItemNumber[1];

		DBConnection::startTransferConnection($restID);
		$resultArray = CQ::getExternalLocationInventoryItemRowByTransferItemNumber($itemID);
		foreach ($resultArray as &$item) {
			/* Removing conversions on lowQuantity
			if (!empty($item['lowQuantity'])) {
				$item['lowQuantity'] = round(self::unitSystemConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['lowQuantity']),2);
			}
			*/
			if (!empty($item['reOrderQuantity'])) {
				$item['reOrderQuantity'] = self::roundConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['reOrderQuantity']);
			}
		}
		return $resultArray;
	}

	protected function action_getInventoryItemsByName($args){
		$names = isset($args['Items']) ? $args['Items'] : null;
		$result = null;
		$name = $names['name'];
		$result = InventoryQueries::getInventoryItemByName($name);
		$result = MarshallingArrays::resultToArray($result);
		if (count($result) <= 0) {
			$result = "Item_DNE";
		} else {
			foreach ($result as &$item) {
				/*removing conversion on lowQuantity
				if (!empty($item['lowQuantity'])) {
					$item['lowQuantity'] = round(self::unitSystemConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['lowQuantity']),2);
				}
				*/
				if (!empty($item['reOrderQuantity'])) {
					$item['reOrderQuantity'] = self::roundConversion($item['recentPurchaseUnitID'], $item['salesUnitID'], $item['reOrderQuantity']);
				}
			}
		}
		return $result;
	}

	protected function action_copyTransfers($args){
		$inventoryTransfers = isset($args['Items']) ? $args['Items'] : null;
		$rowsInserted = 0;
		$externalRowsInserted = 0;
		$result = array();
		$enterpriseIDAndTitleResult =  EnterpriseQueries::getRestaurantIDAndTitleByDataname($this->m_dataname);
		$restaurantID = $enterpriseIDAndTitleResult[0]['restaurantid'];

		foreach ($inventoryTransfers as $i => $inventoryTransfer) {
			$origID = $inventoryTransfer['id'];
			$transferID = CommonQueries::copyTransferRowInTable('inventory_transfer', $origID, $restaurantID); //copy transfer, then copy the transfer items and link to the transfer id.
			$transferItems = CQ::getWhereColumnEquals('transfer_items', 'transferID', $origID);
			++$rowsInserted;

			$transfer = CQ::getWhereColumnEquals('inventory_transfer', 'id', $origID);

			$originRestaurantID = $transfer[0]['originRestaurantID'];
			$destinationRestaurantID = $transfer[0]['destinationRestaurantID'];
			$useID = null;
			if ($destinationRestaurantID == $restaurantID) {
				$useID = $originRestaurantID;
			} else {
				$useID = $destinationRestaurantID;
			}
			DBConnection::startTransferConnection($useID);
			CommonQueries::copyRowToExternalTransferLocationTable('inventory_transfer', $transferID); //copy transfer, then copy the transfer items and link to the transfer id.
			$externalTransferID = DBConnection::transferLocationInsertID();
			++$externalRowsInserted;

			$transferItemIDs = array();
			$externalTransferItemIDs = array();
			foreach ($transferItems as $j => $transferItem) {
				$insertedItemID = CommonQueries::copyTransferRowInTable('transfer_items', $transferItem['id'], $restaurantID, $transferID);
				$transferItemIDs[] = $insertedItemID;
				++$rowsInserted;
				CommonQueries::copyRowToExternalTransferLocationTable('transfer_items', $insertedItemID);
				$externalTransferItemIDs[] = DBConnection::transferLocationInsertID();
				++$externalRowsInserted;
			}
			$result[] = array('transferID'=>$transferID, 'transferItemIDs'=>implode(',', $transferItemIDs),
				'externalTransferID'=>$externalTransferID, 'externalTransferItemIDs'=>implode(',', $externalTransferItemIDs));
		}
		DBConnection::closeTransferConnection();
		return array('RowsInserted'=>$result);
	}

	protected function action_receiveTransfers($args){
		$transferRowsUpdated = 0;
		$transferItemRowsUpdated = 0;
		$externalTransferRowsUpdated = 0;
		$externalTransferItemRowsUpdated = 0;
		$updateOk = true;
		$inventoryTransfers = isset($args['Items']) ? $args['Items'] : null;

		$updatedTransfers = array();
		$transferUpdateArray = array(
			'received_ts'=>gmdate("Y-m-d H:i:s"),
			'sent'=>1,
			'received'=>1,
			'accepted'=>1
		);
		foreach ($inventoryTransfers as $i => $inventoryTransfer) {
			$originRestaurantID = $inventoryTransfer['originRestaurantID'];
			DBConnection::startTransferConnection($originRestaurantID);
			$inventoryTransfer['quantityReceived'] = $inventoryTransfer['quantity'];
			$inventoryTransferId = $inventoryTransfer['transferID'];

			$transferNumber = $inventoryTransfer['transferNumber'];
			$transferItemNumber = $inventoryTransfer['transferItemNumber'];

			if (!empty($inventoryTransferId)) {
				if (!in_array($inventoryTransferId, $updatedTransfers)) { //only update each transfer once.
					$updatedTransfers[] = $inventoryTransferId;
					// Doing the && will prevent updates from being attempted after the first failure
					$updateOk = $updateOk && CQ::updateArray('inventory_transfer', $transferUpdateArray, " WHERE `transferNumber` = '{$transferNumber}' AND `_deleted` = 0");  // TO DO : sanitize $inventoryTransferId
					if ($updateOk) {
						++$transferRowsUpdated;
					}
					$updateOk = $updateOk && CQ::updateTransferLocationArray('inventory_transfer', $transferUpdateArray, " WHERE `transferNumber` = '{$transferNumber}'");  // TO DO : sanitize $inventoryTransferId

					if ($updateOk) {
						++$externalTransferRowsUpdated;
					}
				}

				$transferItemUpdateArray = array(
					'quantityReceived' => $inventoryTransfer['quantity'],
					'transferUnitCost' => $inventoryTransfer['cost'],
				);
				if (isset($inventoryTransfer['unitID'])) {
					$transferItemUpdateArray['transferUnitID'] = $inventoryTransfer['unitID'];
				}
				$updateOk = $updateOk && CQ::updateArray('transfer_items', $transferItemUpdateArray, " WHERE `transferItemNumber` = '{$transferItemNumber}' AND `_deleted` = 0");  // TO DO : sanitize $inventoryTransferId
				if ($updateOk) {
					++$transferItemRowsUpdated;
				}
				$updateOk = $updateOk && CQ::updateTransferLocationArray('transfer_items', $transferItemUpdateArray, " WHERE `transferItemNumber` = '{$transferItemNumber}'");  // TO DO : sanitize $inventoryTransferId
				if ($updateOk) {
					++$externalTransferItemRowsUpdated;
				}
				if (isset($inventoryTransfer['unitID'])) {
					$unitArray = array();
					$id = $inventoryTransfer['unitID'];
					$unitArray['_deleted'] = 0;
					CQ::updateArray('inventory_unit', $unitArray, " WHERE `id` = '$id'");
					$inventory_unit = CQ::getRowByID('inventory_unit', $id);
					if (isset($inventory_unit['unitID'])) {
						$id = $inventory_unit['unitID'];
						CQ::updateArray('custom_units_measure', $unitArray, " WHERE `id` = '$id'");
					}
				}
			}
		}
		DBConnection::closeTransferConnection();
		return array(
			'TransferRowsUpdated' => $transferRowsUpdated,
			'ExternalTransferRowsUpdated' => $externalTransferRowsUpdated,
			'ItemRowsUpdated' => $transferItemRowsUpdated,
			'ExternalTransferItemRowsUpdated' => $externalTransferItemRowsUpdated
		);
	}

	protected function action_updateTransfers($args) {
		$rowsUpdated = 0;
		$externalRowsUpdated = 0;
		$updateOk = true;
		$inventoryTransfers = isset($args['Items']) ? $args['Items'] : null;

		$enterpriseIDAndTitleResult =  EnterpriseQueries::getRestaurantIDAndTitleByDataname($this->m_dataname);
		$restaurantID = $enterpriseIDAndTitleResult[0]['restaurantid'];

		foreach ($inventoryTransfers as $i => $inventoryTransfer) {

			$currentTransfer = InventoryQueries::getTransferByID($inventoryTransfer['id']);
			if ($currentTransfer) {
				if ($currentTransfer['sent'] != 1 && $inventoryTransfer['sent'] == 1) {
					$currentTransferItems = InventoryQueries::getTransferItemsByTransferID($inventoryTransfer['id']);
					foreach ($currentTransferItems as $tItem) {
						$row = array(
							'inventoryItemID' => $tItem['inventoryItemID'],
						'action' => 'transferSent',
							'quantity' => $tItem['quantityTransferred'],
							'unitID' => $tItem['transferUnitID'],
							'cost' => $tItem['transferUnitCost'],
							'userID' => (int)sessvar("admin_loggedin"),
							'transferItemID' => $tItem['id'],
							'datetime' => gmdate("Y-m-d H:i:s")
					);
					InventoryQueries::insertAudit($row);

					}
				}
			}
			$originRestaurantID = $inventoryTransfer['originRestaurantID'];
			$destinationRestaurantID = $inventoryTransfer['destinationRestaurantID'];
			$useID = null;

			if ($inventoryTransfer['accepted'] == 0 || $inventoryTransfer['accepted'] == 1) {
				$inventoryTransfer['accepted_ts'] = gmdate("Y-m-d H:i:s");
			}

			if ($destinationRestaurantID == $restaurantID) {
				$useID = $originRestaurantID;
			} else {
				$useID = $destinationRestaurantID;
			}
			DBConnection::startTransferConnection($useID);

			$tid = $inventoryTransfer['id'];
			$tNum = $inventoryTransfer['transferNumber'];
			if (!empty($tid)) {
				$transferOnly = $inventoryTransfer;
				unset($transferOnly['transferItems']);
				unset($transferOnly['id']);
				$updateOk = $updateOk && CQ::updateArray('inventory_transfer', $transferOnly, " WHERE `id` = {$tid} AND `_deleted` = 0");
				if ($updateOk) {
					++$rowsUpdated;
				}
				$updateOk = $updateOk && CQ::updateTransferLocationArray('inventory_transfer', $transferOnly, " WHERE `transferNumber` = '{$tNum}' AND `_deleted` = 0");
				if ($updateOk) {
					++$externalRowsUpdated;
				}
			}
			if (!empty($inventoryTransfer['transferItems'])) {
				$tItems = array('Items'=>$inventoryTransfer['transferItems']);
				$rowsUpdated += self::action_updateTransferItems($tItems)['RowsUpdated'];
				$externalRowsUpdated += self::action_updateTransferItems($tItems, true)['RowsUpdated'];
			}
		}
		DBConnection::closeTransferConnection();
		return array('RowsUpdated' => $rowsUpdated, 'ExternalRowsUpdated'=>$externalRowsUpdated);
	}

	protected function action_updateTransferItems($args, $externalDB = false) {
		$rowsUpdated = 0;
		$updateOk = true;
		$externalRowsUpdated = 0;
		$inventoryTransferItems = isset($args['Items']) ? $args['Items'] : null;
		// Update InventoryTransfersItems
		foreach ($inventoryTransferItems as $j => $inventoryTransferItem) {
			if (!empty($inventoryTransferItem['id'])) {
				if (!$externalDB) {
					$audit = InventoryQueries::getAuditsByAction('sendTransfer');

					$inventoryTransferItemId = $inventoryTransferItem['id'];
					$updateOk = $updateOk && CQ::updateArray('transfer_items', $inventoryTransferItem, " WHERE `id` = {$inventoryTransferItemId} AND `_deleted` = 0");
					if ($updateOk) {
						$rowsUpdated++;
					}
				} else if (DBConnection::getConnectionByType('transferLocation')) {
					$inventoryTransferItemNumber = $inventoryTransferItem['transferItemNumber'];
					unset($inventoryTransferItem['id']);
					$updateOk = $updateOk && CQ::updateTransferLocationArray('transfer_items', $inventoryTransferItem, " WHERE `transferItemNumber` = '{$inventoryTransferItemNumber}' AND `_deleted` = 0");
					if ($updateOk) {
						$externalRowsUpdated++;
					}
				}

			}
		}

		return array('RowsUpdated' => $rowsUpdated, 'ExternalRowsUpdated'=>$externalRowsUpdated);
	}

	/*
	protected function action_getObjectById($args){
		return array("PROOF OF OVERRIDE");
	}
	*/
	//ALL INTERNAL HELPER METHODS MUST BE PRIVATE!!!

	protected function action_create86CountMenuItems($args){
		$columnNames = array_keys($args['Items'][0]);
		$insertRows = array();
		$count86Request = array();
		$table86Name =  'menuitem_86';
		$table86Column = 'menuItemID';
		$menuItemID = $args['Items'][0][$table86Column];
		$newInventoryIds = array();
		$menu86CountArr = array();
		foreach ($args['Items'] as $key => $currRow) {
			$count86Request[$currRow['inventoryItemID']][$table86Column] = $currRow[$table86Column];
			$count86Request[$currRow['inventoryItemID']]['inventoryItemID'] = $currRow['inventoryItemID'];
			$count86Request[$currRow['inventoryItemID']]['quantityUsed'] += $currRow['quantityUsed'];
			$count86Request[$currRow['inventoryItemID']]['criticalItem'] = $currRow['criticalItem'];
			$count86Request[$currRow['inventoryItemID']]['unitID'] = $currRow['unitID'];
			$count86Request[$currRow['inventoryItemID']]['reservation'] = $currRow['reservation'];
			$newInventoryIds[$currRow['inventoryItemID']] = $currRow['inventoryItemID'];
			$menu86CountArr[$menuItemID][$key]['objectID'] = $menuItemID;
			$menu86CountArr[$menuItemID][$key]['inventoryItemID'] = $currRow['inventoryItemID'];
			$menu86CountArr[$menuItemID][$key]['quantityUsed'] = $currRow['quantityUsed'];
			$menu86CountArr[$menuItemID][$key]['86Unit'] = $currRow['unitID'];
		}
		if (count($count86Request > 0)) {
			$inventoryIDs = array_keys($count86Request);
			$existInventoryData = CQ::getWhereColumnEquals($table86Name, $table86Column, $menuItemID);
			$existInventoryIds = array();
			if (!empty($existInventoryData)) {
				foreach ($existInventoryData as $existInventorys) {
					$existInventoryIds[$existInventorys['inventoryItemID']] = $existInventorys['inventoryItemID'];
				}
			}
			$updateInventoryIds = array_intersect($existInventoryIds, $inventoryIDs);
			$deletedInventoryIds = array_diff($existInventoryIds, $inventoryIDs);
			$insertInventoryIds = array_diff($inventoryIDs, $existInventoryIds);

			if (!empty($updateInventoryIds)) {
				foreach ($updateInventoryIds as $updateInvId) {
					$updateArgs[Items][] = $count86Request[$updateInvId];
				}
				$updateResponse = $this->action_update86CountMenuItems($updateArgs);
			}
			if (!empty($deletedInventoryIds)) {
				$inDeleteClause = MQ::buildSanitizedListInParenthesis($deletedInventoryIds, "'");
				$deleteResponse = CQ::updateArray($table86Name, array('_deleted' => 1), ' WHERE ' . $table86Column .' = ' . $menuItemID . ' AND inventoryItemID IN ' . $inDeleteClause) ;
			}
			if (!empty($insertInventoryIds)) {
				foreach ($insertInventoryIds as $insertInvId) {
					$insertRows[] = $count86Request[$insertInvId];
				}
				$insertResponse = CommonQueries::insertArray($table86Name, $columnNames, $insertRows);
			}
			if (!empty($menu86CountArr)) {
				$redisController = new InventoryRedisController();
				$redisController->setInventory86Count($table86Name, $menu86CountArr);
			}
		}
		return true;
	}

	protected function action_create86CountForcedModifiers($args){
		$columnNames = array_keys($args['Items'][0]);
		$insertRows = array();
		$count86Request = array();
		$table86Name =  'forced_modifier_86';
		$table86Column = 'forced_modifierID';
		$forceModifierID = $args['Items'][0]['forced_modifierID'];
		$newInventoryIds = array();
		$forcedModifier86CountArr = array();
		foreach ($args['Items'] as $key => $currRow) {
			$count86Request[$currRow['inventoryItemID']][$table86Column] = $currRow[$table86Column];
			$count86Request[$currRow['inventoryItemID']]['inventoryItemID'] = $currRow['inventoryItemID'];
			$count86Request[$currRow['inventoryItemID']]['quantityUsed'] += $currRow['quantityUsed'];
			$count86Request[$currRow['inventoryItemID']]['criticalItem'] = $currRow['criticalItem'];
			$count86Request[$currRow['inventoryItemID']]['unitID'] = $currRow['unitID'];
			$count86Request[$currRow['inventoryItemID']]['reservation'] = $currRow['reservation'];
			$newInventoryIds[$currRow['inventoryItemID']] = $currRow['inventoryItemID'];
			$forcedModifier86CountArr[$forceModifierID][$key]['objectID'] = $forceModifierID;
			$forcedModifier86CountArr[$forceModifierID][$key]['inventoryItemID'] = $currRow['inventoryItemID'];
			$forcedModifier86CountArr[$forceModifierID][$key]['quantityUsed'] = $currRow['quantityUsed'];
			$forcedModifier86CountArr[$forceModifierID][$key]['86Unit'] = $currRow['unitID'];
		}
		if (count($count86Request > 0)) {
			$inventoryIDs = array_keys($count86Request);
			$existInventoryData = CQ::getWhereColumnEquals($table86Name, $table86Column, $forceModifierID);
			$existInventoryIds = array();
			if (!empty($existInventoryData)) {
				foreach ($existInventoryData as $existInventorys) {
					$existInventoryIds[$existInventorys['inventoryItemID']] = $existInventorys['inventoryItemID'];
				}
			}
			$updateInventoryIds = array_intersect($existInventoryIds, $inventoryIDs);
			$deletedInventoryIds = array_diff($existInventoryIds, $inventoryIDs);
			$insertInventoryIds = array_diff($inventoryIDs, $existInventoryIds);
			if (!empty($updateInventoryIds)) {
				foreach ($updateInventoryIds as $updateInvId) {
					$updateArgs['Items'][] = $count86Request[$updateInvId];
				}
				$updateResponse = $this->action_update86CountForcedModifiers($updateArgs);
			}
			if (!empty($deletedInventoryIds)) {
				$inDeleteClause = MQ::buildSanitizedListInParenthesis($deletedInventoryIds, "'");
				$deleteResponse = CQ::updateArray($table86Name, array('_deleted' => 1), ' WHERE ' . $table86Column . ' = ' . $forceModifierID . ' AND inventoryItemID IN ' . $inDeleteClause) ;
			}
			if (!empty($insertInventoryIds)) {
				foreach ($insertInventoryIds as $insertInvId) {
					$insertRows[] = $count86Request[$insertInvId];
				}
				$insertResponse = CommonQueries::insertArray($table86Name, $columnNames, $insertRows);
			}
			if (!empty($forcedModifier86CountArr)) {
				$redisController = new InventoryRedisController();
				$redisController->setInventory86Count($table86Name, $forcedModifier86CountArr);
			}
		}
		return true;
	}

	protected function action_create86CountModifiers($args){
		$columnNames = isset($args['Items']) ? array_keys($args['Items'][0]) : array();
		$insertRows = array();
		$count86Request = array();
		$table86Name =  'modifier_86';
		$table86Column = 'modifierID';
		$modifierID = $args['Items'][0]['modifierID'];
		$newInventoryIds = array();
		$modifier86CountArr = array();
		foreach ($args['Items'] as $key => $currRow) {
			$count86Request[$currRow['inventoryItemID']][$table86Column] = $currRow[$table86Column];
			$count86Request[$currRow['inventoryItemID']]['inventoryItemID'] = $currRow['inventoryItemID'];
			$count86Request[$currRow['inventoryItemID']]['quantityUsed'] += $currRow['quantityUsed'];
			$count86Request[$currRow['inventoryItemID']]['criticalItem'] = $currRow['criticalItem'];
			$count86Request[$currRow['inventoryItemID']]['unitID'] = $currRow['unitID'];
			$count86Request[$currRow['inventoryItemID']]['reservation'] = $currRow['reservation'];
			$newInventoryIds[$currRow['inventoryItemID']] = $currRow['inventoryItemID'];
			$modifier86CountArr[$modifierID][$key]['objectID'] = $modifierID;
			$modifier86CountArr[$modifierID][$key]['inventoryItemID'] = $currRow['inventoryItemID'];
			$modifier86CountArr[$modifierID][$key]['quantityUsed'] = $currRow['quantityUsed'];
			$modifier86CountArr[$modifierID][$key]['86Unit'] = $currRow['unitID'];
		}
		if (count($count86Request > 0)) {
			$inventoryIDs = array_keys($count86Request);
			$existInventoryData = CQ::getWhereColumnEquals($table86Name, $table86Column, $modifierID);
			$existInventoryIds = array();
			if (!empty($existInventoryData)) {
				foreach ($existInventoryData as $existInventorys) {
					$existInventoryIds[$existInventorys['inventoryItemID']] = $existInventorys['inventoryItemID'];
				}
			}
			$updateInventoryIds = array_intersect($existInventoryIds, $inventoryIDs);
			$deletedInventoryIds = array_diff($existInventoryIds, $inventoryIDs);
			$insertInventoryIds = array_diff($inventoryIDs, $existInventoryIds);
			if (!empty($updateInventoryIds)) {
				foreach ($updateInventoryIds as $updateInvId) {
					$updateArgs[Items][] = $count86Request[$updateInvId];
				}
				$updateResponse = $this->action_update86CountModifiers($updateArgs);
			}
			if (!empty($deletedInventoryIds)) {
				$inDeleteClause = MQ::buildSanitizedListInParenthesis($deletedInventoryIds, "'");
				$deleteResponse = CQ::updateArray($table86Name, array('_deleted' => 1), ' WHERE ' . $table86Column . ' = ' . $modifierID . ' AND inventoryItemID IN ' . $inDeleteClause) ;
			}

			if (!empty($insertInventoryIds)) {
				foreach ($insertInventoryIds as $insertInvId) {
					$insertRows[] = $count86Request[$insertInvId];
				}
				$insertResponse = CommonQueries::insertArray($table86Name, $columnNames, $insertRows);
			}
			if (!empty($modifier86CountArr)) {
				$redisController = new InventoryRedisController();
				$redisController->setInventory86Count($table86Name, $modifier86CountArr);
			}
		}
		return true;
	}

	/**
	 * Get Stock status of menu_items
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getMenuItemsStatus($args)
	{
		list($itemsStatus,) = self::action_getMenuItemsStatusWrapper($args);
		return $itemsStatus;
	}

	/**
	 * Get Stock status of forced modifier status
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getForcedModifiersStatus($args)
	{
		list($forcedModifierStatus,) = self::action_getForcedModifiersStatusWrapper($args);
		return $forcedModifierStatus;
	}

	/**
	 * Get Stock status of modifiers
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getModifiersStatus($args)
	{
		list($modifierStatus,) = self::action_getModifiersStatusWrapper($args);
		return $modifierStatus;
	}

	/**
	 * Get Stock status of menu_items
	 * @param $args [{"id":682},{"id":685}]
	 * @param $tempInventoryQuantity array("id"=>1)
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getMenuItemsStatusWrapper($args, $tempInventoryQuantity = array())
	{
		$itemIds = array();
		$comboItemIds = array();
		$tableName = 'menu_items';
		$table86 = 'menuitem_86';
		$table86Column = 'menuItemID';
		$inStockItemIds = array();
		$menuLevelItemResponse = array();
		$tempInventoryFlag = false;
		$inStock = 'IN_STOCK';
		$outStock = 'OUT_STOCK';
		if (!empty($args['Items'])) {
			$idsArr = array();
			$tempInventoryFlag = true;
			$redisController = new InventoryRedisController();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}

			$cacheResponse = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getMenuItems());
			$dbCacheIds = array_diff_key(array_flip($idsArr), $cacheResponse);
			$dbResponse = array();
			if (!empty($dbCacheIds)) {
				//Fetch only given column value to avoid out of memory error
				$selColumns = array('id', 'track_86_count', 'inv_count', 'combo');
				$dbResponse = CQ::getNonDeletedRowsWhereColumnIn($tableName, 'id', array_keys($dbCacheIds), $selColumns);
			}
			$cacheRequest = array();
			if (!empty($dbResponse)) {
				foreach ($dbResponse as $details) {
					$cacheRequest[$details['id']] = $details;
				}
			}
			$cacheDbResponse = $cacheResponse +  $cacheRequest;
			$redisController->setMenuItems($cacheDbResponse);
			foreach ($idsArr as $id) {
				$menuItemResponse[] = isset($cacheDbResponse[$id]) ? $cacheDbResponse[$id] : array();
			}

			if (empty($menuItemResponse)) {
				$response['error'] = 'Item not found';
				return array($response, $tempInventoryQuantity);
			}

			$menuItemTrack86Count = array();
			$comboIds = array();
			$menuIds = array();
			$noTrackMenuItems = array();
			foreach ($menuItemResponse as $responseDetails) {
				if (isset($responseDetails['id'])) {
					$menuItemTrack86Count[$responseDetails['id']]['type'] = $responseDetails['track_86_count'];
					$menuItemTrack86Count[$responseDetails['id']]['count'] = $responseDetails['inv_count'];
					$menuIds[] = $responseDetails['id'];
					if ($responseDetails['combo'] == 1) {
						$comboIds[] =  $responseDetails['id'];
					}
				}
			}

			foreach ($args['Items'] as $key => $idQuantity ) {
				if (in_array($idQuantity['id'], $menuIds)) {
					if (in_array($idQuantity['id'], $comboIds)) {
						$comboItemIds[$key]['id'] = $idQuantity['id'];
						if (!isset($comboItemIds[$key]['quantity'])) {
							$comboItemIds[$key]['quantity'] = 0;
						}
						$comboItemIds[$key]['quantity'] = $idQuantity['quantity'];
					} else {
						if ($menuItemTrack86Count[$idQuantity['id']]['type'] == '86countInventory') {
							$itemIds['ids'][] = $idQuantity['id'];
							if (!isset($itemIds['quantity'][$idQuantity['id']])) {
								$itemIds['quantity'][$idQuantity['id']] = 0;
							}
							$itemIds['quantity'][$idQuantity['id']] += $idQuantity['quantity'];
						} else if ($menuItemTrack86Count[$idQuantity['id']]['type'] == '86count') {
							if ($idQuantity['quantity'] > $menuItemTrack86Count[$idQuantity['id']]['count']) {
								$menuLevelItemResponse[$idQuantity['id']] = $outStock;
							} else {
								$menuLevelItemResponse[$idQuantity['id']] = $inStock;
							}
						} else {
							$inStockItemIds[$idQuantity['id']] = $inStock;
							$noTrackMenuItems[] = $idQuantity['id'];
						}
					}
				}
			}
		} else {
			//Fetch only given column value to avoid out of memory error
			$selColumns = array('id', 'track_86_count', 'inv_count', 'combo');
			$menuItemRows = CQ::getNonDeletedAllRows($tableName, $selColumns);
			if (!empty($menuItemRows)) {
				foreach ($menuItemRows as $key => $menuItemArr) {
					if ($menuItemArr['combo'] == 1) {
						$comboItemIds[$key]['id'] = $menuItemArr['id'];
						$comboItemIds[$key]['quantity'] = 1;
					} else {
						if ($menuItemArr['track_86_count'] == '86countInventory') {
							$itemIds['ids'][]  = $menuItemArr['id'];
							$itemIds['quantity'][$menuItemArr['id']] = 1;
						} else if ($menuItemArr['track_86_count'] == '86count') {
							if ($menuItemArr['inv_count'] >= 1) {
								$menuLevelItemResponse[$menuItemArr['id']] = $inStock;
							} else {
								$menuLevelItemResponse[$menuItemArr['id']] = $outStock;
							}
						} else {
							$inStockItemIds[$menuItemArr['id']] = $inStock;
							$noTrackMenuItems[] = $menuItemArr['id'];
						}
					}
				}
			}
		}

		$comboItemResponse = array();
		$menuItemresponse = array();

		if (!empty($itemIds)) {
			list($menuItemresponse, $tempInventoryQuantity) = self::getStockStatus($itemIds, $table86, $table86Column, $tempInventoryFlag, $tempInventoryQuantity);
		}

		if (!empty($comboItemIds)) {
			list($comboItemResponse, $tempInventoryQuantity) = self::getComboItemsStockStatus($comboItemIds, false, $tempInventoryQuantity, $menuItemresponse);
		}

		$response = $inStockItemIds + $menuItemresponse + $comboItemResponse + $menuLevelItemResponse;

		return array($response, $tempInventoryQuantity, $noTrackMenuItems);
	}

	/**
	 * Get Stock status of forced modifier status
	 * @param $args [{"id":682},{"id":685}]
	 * @param $tempInventoryQuantity array("id"=>1)
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getForcedModifiersStatusWrapper($args, $tempInventoryQuantity = array())
	{
		$itemIds = array();
		$tempInventoryFlag = false;
		if (!empty($args['Items'])) {
			$tempInventoryFlag = true;
			foreach ($args['Items'] as $key => $idQuantity ) {
				$itemIds['ids'][] = $idQuantity['id'];
				if (!isset($itemIds['quantity'][$idQuantity['id']])) {
					$itemIds['quantity'][$idQuantity['id']] = 0;
				}
				$itemIds['quantity'][$idQuantity['id']] += $idQuantity['quantity'];
			}
		} else {
			//Fetch only given column value to avoid out of memory error
			$selColumns = array('id');
			$menuItemRows = CQ::getNonDeletedAllRows('forced_modifiers', $selColumns);
			if (!empty($menuItemRows)) {
				foreach ($menuItemRows as $key => $menuItemArr) {
					$itemIds['ids'][]  = $menuItemArr['id'];
					$itemIds['quantity'][$menuItemArr['id']] = 1;
				}
			}
		}

		list($response, $tempInventoryQuantity) = self::getStockStatus($itemIds, 'forced_modifier_86', 'forced_modifierID', $tempInventoryFlag, $tempInventoryQuantity);

		return array($response, $tempInventoryQuantity);
	}

	/**
	 * Get Stock status of modifiers
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getModifiersStatusWrapper($args, $tempInventoryQuantity = array())
	{
		$itemIds = array();
		$tempInventoryFlag = false;
		if (!empty($args['Items'])) {
			$tempInventoryFlag = true;
			foreach ($args['Items'] as $key => $idQuantity) {
				$itemIds['ids'][] = $idQuantity['id'];
				if (!isset($itemIds['quantity'][$idQuantity['id']])) {
					$itemIds['quantity'][$idQuantity['id']] = 0;
				}
				$itemIds['quantity'][$idQuantity['id']] += $idQuantity['quantity'];
			}
		} else {
			//Fetch only given column value to avoid out of memory error
			$selColumns = array('id');
			$menuItemRows = CQ::getNonDeletedAllRows('modifiers', $selColumns);
			if (!empty($menuItemRows)) {
				foreach ($menuItemRows as $key => $menuItemArr) {
					$itemIds['ids'][]  = $menuItemArr['id'];
					$itemIds['quantity'][$menuItemArr['id']] = 1;
				}
			}
		}

		list($response, $tempInventoryQuantity) = self::getStockStatus($itemIds, 'modifier_86', 'modifierID', $tempInventoryFlag, $tempInventoryQuantity);

		return array($response, $tempInventoryQuantity);
	}

	/**
	 * Get Stock status according to ingredients quantity availability
	 * @param $inventoryItems [740] => Array
	(
	[1] => Array
	(
	[menuItemID] => 740
	[inventoryItemID] => 1
	[quantityUsed] => 4
	[reservation] => 4
	)

	[35] => Array
	(
	[menuItemID] => 740
	[inventoryItemID] => 35
	[quantityUsed] => 2
	[reservation] => 2
	)
	 * }
	 * @return array
	 */
	// Used by action_getMenuItemsStatus(), action_getModifiersStatus, action_getForcedModifiersStatus()
	protected function getStockStatus($itemIds, $tableName, $whereColumn, $tempInventoryFlag = false, $tempInventoryQuantity = array())
	{
		$response = array();
		if (!empty($itemIds['ids'])) {
			$ids = $itemIds['ids'];
			$inventoryItems = array();
			$redisController = new InventoryRedisController();
			$cacheResponse = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory86Count($tableName));
			$dbCacheIds = array_diff_key(array_flip($ids), $cacheResponse);
			$dbResponse = array();
			if (!empty($dbCacheIds)) {
				$dbResponse = InventoryQueries::getStockStatus(array_keys($dbCacheIds), $tableName, $whereColumn);
			}
			$cache86CountRequest = array();
			if (!empty($dbResponse)) {
				foreach ($dbResponse as $details) {
					$cache86CountRequest[$details['objectID']][] = $details;
				}
			}
			$cacheDbResponse = $cacheResponse + $cache86CountRequest;
			$dbCacheIds = array_diff_key(array_flip($ids), $cacheDbResponse);
			if (!empty($dbCacheIds)) {
				$dbCacheIds = array_flip($dbCacheIds);
				foreach ($dbCacheIds as $id) {
					$cacheDbResponse[$id] = array();
				}
			}
			$redisController->setInventory86Count($tableName, $cacheDbResponse);
			foreach ($ids as $id) {
				if (isset($cacheDbResponse[$id]) ) {
					$inventoryItems[] = $cacheDbResponse[$id];
				}
			}
			$inventoryItemStatus = array();
			$inventoryIds = array();
			//loop through our menu usage and tally up the total reserved quantities for each inventory item
			foreach ($inventoryItems as $inventoryItemsDetails) {
				foreach ($inventoryItemsDetails as $inventoryItemArr) {
					$inventoryIds[$inventoryItemArr['inventoryItemID']] = $inventoryItemArr['inventoryItemID'];
				}
			}
			$cachInventoryDetails = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
			$dbInventoryIds = array_diff_key($inventoryIds, $cachInventoryDetails);
			$dbInventoryDetails = array();
			$args = array();
			if (!empty($dbInventoryIds)) {
				$inventoryReserveQuantity = InventoryQueries::getTotal86ReserveQuantity($dbInventoryIds);
				foreach ($inventoryReserveQuantity as $inventoryReserveArr) {
					$inventoryItemStatus[$inventoryReserveArr['inventoryItemID']] = $inventoryReserveArr['reserve'];
					$args['Items'][]['id'] = $inventoryReserveArr['inventoryItemID'];
				}
				$inventoryDetails = self::action_getInventoryItemsByIDs($args);
				foreach ($inventoryDetails as $iDetails) {
					$cachInventoryDetails[$iDetails['id']]['quantityOnHand'] = $iDetails['quantityOnHand'];
					$cachInventoryDetails[$iDetails['id']]['reserve'] = $inventoryItemStatus[$iDetails['id']];
					$cachInventoryDetails[$iDetails['id']]['salesUnitID'] = $iDetails['salesUnitID'];
					$cachInventoryDetails[$iDetails['id']]['lowQuantity'] = $iDetails['lowQuantity'];
					$cachInventoryDetails[$iDetails['id']]['reOrderQuantity'] = $iDetails['reOrderQuantity'];
				}
			}
			if (!empty($cachInventoryDetails)) {
				$redisController->setInventory($cachInventoryDetails);
			}
			$finalInventoryDetails = $cachInventoryDetails;

			//Figure out whether or not we have enough inventory to make each menu item.
			$statusResponse = array();
			$inStock = "IN_STOCK";
			$lowStock = "LOW_STOCK";
			$outStock = "OUT_STOCK";
			$menuIds = array();

			foreach ($inventoryItems as $inventoryItemsDetails) {
				foreach ($inventoryItemsDetails as $inventoryItemArr) {
					$status = $inStock;
					$id = $inventoryItemArr['objectID'];
					$menuIds[$id] = $id;
					if (!isset($statusResponse[$id])) {
						$quantityUsed = $inventoryItemArr['quantityUsed'] * $itemIds['quantity'][$id];
						// If we are checking stock of already check inventory then we will have to add those quantity in this check
						if ($tempInventoryFlag) {
							$quantityUsed = $quantityUsed + (int)$tempInventoryQuantity[$inventoryItemArr['inventoryItemID']];
							$tempInventoryQuantity[$inventoryItemArr['inventoryItemID']] = $quantityUsed;
						}
						if (!isset($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['reserve'])) {
							$finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['reserve'] = 0;
						}
						if (!isset($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'])) {
							$finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] = 0;
						}
						if (!isset($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['salesUnitID'])) {
							$finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['salesUnitID'] = 0;
						}
						if (!isset($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['lowQuantity'])) {
							$finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['lowQuantity'] = 0;
						}
						$reserveQuantity = $finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['reserve'];
						if ($reserveQuantity != 0 && $inventoryItemArr['86Unit'] != $finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['salesUnitID']) {
							$reserveQuantity = self::unitSystemConversion($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['salesUnitID'], $inventoryItemArr['86Unit'], $reserveQuantity);
						}
						$theoreticalQuantityOnHand = $finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] - $reserveQuantity - $finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['lowQuantity'];

						if ($inventoryItemArr['86Unit'] != $finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['salesUnitID']) {
							$quantityUsed = self::unitSystemConversion($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['salesUnitID'], $inventoryItemArr['86Unit'], $quantityUsed);
						}

						if ($quantityUsed > $theoreticalQuantityOnHand) {
							$status = $outStock; //if we can't sell it, out of stock
							$statusResponse[$id] = $status;
							$response[$id] = $status;
						} else if ($theoreticalQuantityOnHand - $quantityUsed <= $inventoryItemArr['quantityUsed']) {
							$status = $lowStock; //if we can sell at most 2 more of this item, flag it as low stock
						}
						//set the response for the menu item based on the most severe status of required inventory items.
						if (isset($response[$id])) {
							if ($status === $lowStock && $response[$id] === $inStock) {
								$response[$id] = $status;
							} else if ($status === $inStock && $response[$id] === $lowStock) {
								$response[$id] = $response[$id];
							}
						} else {
							$response[$id] = $status;
						}
					}
				}
			}
			$not86Menu = array_diff($itemIds['ids'], $menuIds);

			if (!empty($not86Menu)) {
				foreach ($not86Menu as $id) {
					$response[$id] = $inStock;
				}
			}
		} else {
			return array('error' => 'Record not found');
		}
		return array($response, $tempInventoryQuantity);
	}

	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	*/
	protected function action_deductMenuItemQuantity($args){
		return $this->action_deduct86CountQuantity($args, 'menuitem_86', 'menuItemID', -1);
	}

	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	 */
	protected function action_deductModifierItemQuantity($args){
		return $this->action_deduct86CountQuantity($args, 'modifier_86', 'modifierID', -1);
	}

	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	 */
	protected function action_deductForcedModifierItemQuantity($args){
		return $this->action_deduct86CountQuantity($args, 'forced_modifier_86', 'forced_modifierID', -1);
	}

	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	 */
	protected function action_deductMenuItemQuantityOnHand($args){
		return $this->action_deduct86CountQuantity($args, 'menuitem_86', 'menuItemID', -1, false);
	}

	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	 */
	protected function action_deductModifierItemQuantityOnHand($args){
		return $this->action_deduct86CountQuantity($args, 'modifier_86', 'modifierID', -1, false);
	}

	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	 */
	protected function action_deductForcedModifierItemQuantityOnHand($args){
		return $this->action_deduct86CountQuantity($args, 'forced_modifier_86', 'forced_modifierID', -1, false);
	}

	/**
	 * @param $args
	 * @param $factor 1
	 * @return int
	 */
	protected function action_addQuantityOnHand($args){
		$items = $args['Items'];
		$rowsUpdated = 0;
		if (!empty($items)) {
			$redisController = new InventoryRedisController();
			$cachInventoryDetails = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
			foreach ($items as $currItem) {
				$inventoryIds[$currItem['inventoryItemID']] = $currItem['inventoryItemID'];
			}
			$dbInventoryIds = array_diff_key($inventoryIds, $cachInventoryDetails);
			$args = array();
			if (!empty($dbInventoryIds)) {
				$inventoryReserveQuantity = InventoryQueries::getTotal86ReserveQuantity($dbInventoryIds);
				foreach ($inventoryReserveQuantity as $inventoryReserveArr) {
					$inventoryItemStatus[$inventoryReserveArr['inventoryItemID']] = $inventoryReserveArr['reserve'];
					$args['Items'][]['id'] = $inventoryReserveArr['inventoryItemID'];
				}
				$inventoryDetails = self::action_getInventoryItemsByIDs($args);
				foreach ($inventoryDetails as $iDetails) {
					$cachInventoryDetails[$iDetails['id']]['quantityOnHand'] = $iDetails['quantityOnHand'] - $iDetails['inventoryOverage'];
					$cachInventoryDetails[$iDetails['id']]['reserve'] = $inventoryItemStatus[$iDetails['id']];
					$cachInventoryDetails[$iDetails['id']]['salesUnitID'] = $iDetails['salesUnitID'];
					$cachInventoryDetails[$iDetails['id']]['lowQuantity'] = $iDetails['lowQuantity'];
					$cachInventoryDetails[$iDetails['id']]['reOrderQuantity'] = $iDetails['reOrderQuantity'];
				}
			}
			foreach ($items as $currItem) {
				$args['Items'][0]['id'] = $currItem['inventoryItemID'];
				$inventoryDetails = $this->action_getInventoryItemsByID($args);
				$cacheQuantityOnHand = $currItem['quantity'];
				$extraQuantity = 0;
				if ($inventoryDetails['inventoryOverage'] < $currItem['quantity']) {
					$extraQuantity = $currItem['quantity'] - $inventoryDetails['inventoryOverage'];
					$currItem['quantity'] = $inventoryDetails['inventoryOverage'];
				}

				if ($inventoryDetails['inventoryOverage'] > 0) {
					$result = CQ::addOrSubtractFromColumn('inventory_items', 'inventoryOverage', $currItem['quantity'], -1, ' where `id`=' . $currItem['inventoryItemID'] . ' AND `_deleted` = 0');
				}

				if ($extraQuantity > 0 || $inventoryDetails['inventoryOverage'] == 0) {
					$result = CQ::addOrSubtractFromColumn('inventory_items', 'quantityOnHand', $extraQuantity, 1, ' where `id`=' . $currItem['inventoryItemID']);
				}
				$cachInventoryDetails[$currItem['inventoryItemID']]['quantityOnHand'] += $cacheQuantityOnHand;
				$rowsUpdated += DBConnection::clientAffectedRows();
			}
		}
		if ($rowsUpdated > 0) {
			$redisController->setInventory($cachInventoryDetails);
			return array("RowsUpdated" => $rowsUpdated);
		} else {
			$errorMessage = $rowsUpdated . " row(s) updated in inventory table.";
			return array("Message" => $errorMessage);
		}
	}

	/**
	 * When order sent to kitchen then we will deduct quantityUsed from reservation of 86Count table as well as from
	 * quantityOnHand of inventory_items table
	 * @param $args
	 * @param $tableName (86Count table name)
	 * @param $columnName menuItemID, modifierID, forced_modifierID
	 * @param $factor -1
	 * @param $reserve true|false if true deduct from reserve as well as quantity hand, false means only deduct from quantity hand
	 * @return int
	 */
	// Used by action_deductMenuItemQuantity(), action_deductModifierItemQuantity(), action_deductForcedModifierItemQuantity()
	protected function action_deduct86CountQuantity($args, $tableName, $columnName, $factor, $reserve = true){
		$items = $args['Items'];
		$rows86Updated = 0;
		$rowsUpdated = 0;
		if (!empty($items)) {
			$redisController = new InventoryRedisController();
			$cachInventoryDetails = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
			foreach ($items as $currItem) {
				$inventoryIds[$currItem['inventoryItemID']] = $currItem['inventoryItemID'];
			}
			$dbInventoryIds = array_diff_key($inventoryIds, $cachInventoryDetails);
			$args = array();
			if (!empty($dbInventoryIds)) {
				$inventoryReserveQuantity = InventoryQueries::getTotal86ReserveQuantity($dbInventoryIds);
				foreach ($inventoryReserveQuantity as $inventoryReserveArr) {
					$inventoryItemStatus[$inventoryReserveArr['inventoryItemID']] = $inventoryReserveArr['reserve'];
					$args['Items'][]['id'] = $inventoryReserveArr['inventoryItemID'];
				}
				$inventoryDetails = self::action_getInventoryItemsByIDs($args);
				foreach ($inventoryDetails as $iDetails) {
					$cachInventoryDetails[$iDetails['id']]['quantityOnHand'] = $iDetails['quantityOnHand'] - $iDetails['inventoryOverage'];
					$cachInventoryDetails[$iDetails['id']]['reserve'] = $inventoryItemStatus[$iDetails['id']];
					$cachInventoryDetails[$iDetails['id']]['salesUnitID'] = $iDetails['salesUnitID'];
					$cachInventoryDetails[$iDetails['id']]['lowQuantity'] = $iDetails['lowQuantity'];
					$cachInventoryDetails[$iDetails['id']]['reOrderQuantity'] = $iDetails['reOrderQuantity'];
				}
			}
			foreach ($items as $currItem) {
				if ($reserve) {
					$result = CQ::addOrSubtractFromColumn($tableName, 'reservation', $currItem['quantity'], $factor, ' where ' . $columnName . ' = ' . $currItem['id'] . ' and inventoryItemID = ' . $currItem['inventoryItemID'] . ' AND `_deleted` = 0');
					$rows86Updated += $result['RowsUpdated'];
				}
				$args['Items'][0]['id'] = $currItem['inventoryItemID'];
				$inventoryDetails = $this->action_getInventoryItemsByID($args);
				$extraQuantity = 0;
				$cacheQuantity = $currItem['quantity'];
				if ($inventoryDetails['quantityOnHand'] < $currItem['quantity']) {
					$extraQuantity = $currItem['quantity'] - $inventoryDetails['quantityOnHand'];
					$currItem['quantity'] = $inventoryDetails['quantityOnHand'];
				}

				if ($inventoryDetails['quantityOnHand'] > 0) {
					$result = CQ::addOrSubtractFromColumn('inventory_items', 'quantityOnHand', $currItem['quantity'], $factor, ' where `id`=' . $currItem['inventoryItemID'] . ' AND `_deleted` = 0');
				}
				if ($extraQuantity > 0 || $inventoryDetails['quantityOnHand'] == 0) {
					$result = CQ::addOrSubtractFromColumn('inventory_items', 'inventoryOverage', $extraQuantity, 1, ' where `id`=' . $currItem['inventoryItemID'] . ' AND `_deleted` = 0');
				}

				if ($reserve) {
					if ($factor == 1) {
						$cachInventoryDetails[$currItem['inventoryItemID']]['reserve'] += $cacheQuantity;
						$cachInventoryDetails[$currItem['inventoryItemID']]['quantityOnHand'] += $cacheQuantity;
					} else {
						$newReserve = $cachInventoryDetails[$currItem['inventoryItemID']]['reserve'] - $cacheQuantity;
						$cachInventoryDetails[$currItem['inventoryItemID']]['reserve'] = ($newReserve >= 0) ? $newReserve : 0;
						$cachInventoryDetails[$currItem['inventoryItemID']]['quantityOnHand'] -= $cacheQuantity;
					}
				} else {
					if ($factor == 1) {
						$cachInventoryDetails[$currItem['inventoryItemID']]['quantityOnHand'] += $cacheQuantity;
					} else {
						$cachInventoryDetails[$currItem['inventoryItemID']]['quantityOnHand'] -= $cacheQuantity;
					}
				}

				$rowsUpdated += $result['RowsUpdated'];
			}
		}

		if ($rowsUpdated > 0) {
			$redisController = new InventoryRedisController();
			$redisController->setInventory($cachInventoryDetails);
			return array("RowsUpdated" => $rowsUpdated);
		} else {
			$errorMessage = $rows86Updated . " row(s) updated in 86Count table but " . $rowsUpdated . " row(s) updated in inventory table.";
			return array("Message" => $errorMessage);
		}

	}

	/**
	 * @param $args
	 * @param $factor 1
	 * @return int
	 */
	protected function action_addMenuItemReserveQuantity($args){
		return $this->action_86CountReserveQuantity($args, 'menuitem_86', 'menuItemID', 1);
	}

	/**
	 * @param $args
	 * @param $factor 1
	 * @return int
	 */
	protected function action_addModifierReserveQuantity($args){
		return $this->action_86CountReserveQuantity($args, 'modifier_86', 'modifierID', 1);
	}

	/**
	 * @param $args
	 * @param $factor 1
	 * @return int
	 */
	protected function action_addforcedModifierReserveQuantity($args){
		return $this->action_86CountReserveQuantity($args, 'forced_modifier_86', 'forced_modifierID', 1);
	}

	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	 */
	protected function action_removedMenuItemReserveQuantity($args){
		return $this->action_86CountReserveQuantity($args, 'menuitem_86', 'menuItemID', -1);
	}

	protected function action_receiveWithoutPO($args){
		$items = $args['Items'];
		$rowsUpdated = 0;
		$redisController = new InventoryRedisController();
		$cachInventoryDetails = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
		foreach ($items as $item) {
			$currentItem = InventoryQueries::getInventoryItemByID($item['id']);

			if (!$currentItem) {
				continue;
			}

			$inventoryItemUpdate = array(
				"recentPurchaseUnitID" => $item['unitID'],
				"recentPurchaseCost" => $item['costPerUnit'],
			);
			$inventoryItemResult = CQ::updateArray('inventory_items', $inventoryItemUpdate, "WHERE `id` = " . $item['id']);
			if ($inventoryItemResult) {
				$rowsUpdated++;
			}
			$convertedQuantityToAdd = self::unitSystemConversion($currentItem[0]['salesUnitID'], $item['unitID'], $item['quantity']);

			$inventoryOverage = $currentItem[0]['inventoryOverage'];

			if ($inventoryOverage > 0) {
				if ($convertedQuantityToAdd > $inventoryOverage) {
					$convertedQuantityToAdd = $convertedQuantityToAdd - $inventoryOverage;
					$newInventoryOverage = $inventoryOverage;
				} else {
					$newInventoryOverage =  $convertedQuantityToAdd;
					$convertedQuantityToAdd = 0;
				}

				$updateoverAgeQuantityResult = CQ::addOrSubtractFromColumn('inventory_items', 'inventoryOverage', $newInventoryOverage, -1, "WHERE `id` = " . $item['id']);
				if ($updateoverAgeQuantityResult) {
					$rowsUpdated++;
				}
			}
			if ($convertedQuantityToAdd > 0) {
				$updateQuantityResult = CQ::addOrSubtractFromColumn('inventory_items', 'quantityOnHand', $convertedQuantityToAdd, 1, "WHERE `id` = " . $item['id']);
				if ($updateQuantityResult) {
					$rowsUpdated++;
				}
			}
			unset($cachInventoryDetails[$item['id']]);
		}

		if ($rowsUpdated > 0) {
			$redisController->setInventory($cachInventoryDetails);
		}
		return array("rowsUpdated"=>$rowsUpdated);
	}
	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	 */
	protected function action_removedModifierReserveQuantity($args){
		return $this->action_86CountReserveQuantity($args, 'modifier_86', 'modifierID', -1);
	}

	/**
	 * @param $args
	 * @param $factor -1
	 * @return int
	 */
	protected function action_removedforcedModifierReserveQuantity($args){
		return $this->action_86CountReserveQuantity($args, 'forced_modifier_86', 'forced_modifierID', -1);
	}

	/**
	 * When order placed or removed then we will update reservation column with quantityUsed in 86Count table
	 * Add quantity into reservation column of 86Count table when add order
	 * @param $args
	 * @param $tableName (86Count table name)
	 * @param $columnName menuItemID, modifierID, forced_modifierID
	 * @param $factor -1
	 * @return int
	 */
	// Used by action_addMenuItemReserveQuantity(), action_addModifierReserveQuantity(), action_addForcedModifierReserveQuantity()
	// Used by action_removedMenuItemReserveQuantity(), action_addModifierReserveQuantity(), action_addForcedModifierReserveQuantity()
	protected function action_86CountReserveQuantity($args, $tableName, $columnName, $factor){
		$items = $args['Items'];
		$rows86Updated = 0;
		$redisController = new InventoryRedisController();
		$cachInventoryDetails = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
		if (!empty($items)) {
			foreach ($items as $currItem) {
				$inventoryIds[$currItem['inventoryItemID']] = $currItem['inventoryItemID'];
			}
			$dbInventoryIds = array_diff_key($inventoryIds, $cachInventoryDetails);
			$args = array();
			if (!empty($dbInventoryIds)) {
				$inventoryReserveQuantity = InventoryQueries::getTotal86ReserveQuantity($dbInventoryIds);
				foreach ($inventoryReserveQuantity as $inventoryReserveArr) {
					$inventoryItemStatus[$inventoryReserveArr['inventoryItemID']] = $inventoryReserveArr['reserve'];
					$args['Items'][]['id'] = $inventoryReserveArr['inventoryItemID'];
				}
				$inventoryDetails = self::action_getInventoryItemsByIDs($args);
				foreach ($inventoryDetails as $iDetails) {
					$cachInventoryDetails[$iDetails['id']]['quantityOnHand'] = $iDetails['quantityOnHand'] - $iDetails['inventoryOverage'];
					$cachInventoryDetails[$iDetails['id']]['reserve'] = $inventoryItemStatus[$iDetails['id']];
					$cachInventoryDetails[$iDetails['id']]['salesUnitID'] = $iDetails['salesUnitID'];
					$cachInventoryDetails[$iDetails['id']]['lowQuantity'] = $iDetails['lowQuantity'];
					$cachInventoryDetails[$iDetails['id']]['reOrderQuantity'] = $iDetails['reOrderQuantity'];
				}
			}
			foreach ($items as $currItem) {
				$result = CQ::addOrSubtractFromColumn($tableName, 'reservation', $currItem['quantity'], $factor, ' WHERE ' . $columnName . ' = ' . $currItem['id'] . ' AND inventoryItemID = ' . $currItem['inventoryItemID'] . ' AND `_deleted` = 0');
				if ($factor == 1) {
					$cachInventoryDetails[$currItem['inventoryItemID']]['reserve'] += $currItem['quantity'];
				} else {
					$newReserve = $cachInventoryDetails[$currItem['inventoryItemID']]['reserve'] - $currItem['quantity'];
					$newReserve = ($newReserve >= 0) ? $newReserve : 0;
					$cachInventoryDetails[$currItem['inventoryItemID']]['reserve'] = $newReserve;
				}
				$rows86Updated += $result['RowsUpdated'];
			}
		}
		if ($rows86Updated > 0 ) {
			$redisController->setInventory($cachInventoryDetails);
			return array("RowsUpdated" => $rows86Updated);
		} else {
			$errorMessage = $rows86Updated . " row(s) updated in 86Count table.";
			return array("Message" => $errorMessage);
		}

	}

	/**
	 * Get menuitem_86 record
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
		"id": "3",
		"menuItemID": "740",
		"inventoryItemID": "35",
		"quantityUsed": "2",
		"criticalItem": "0",
		"unitID": "1",
		"reservation": "4",
		"_deleted": "0"
		}
	 */
	protected function action_get86CountMenuItem($args)
	{
		return $this->action_get86CountTable('menuitem_86', 'menuItemID', $args);
	}

	/**
	 * Get modifier_86 record
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
		"id": "3",
		"menuItemID": "740",
		"inventoryItemID": "35",
		"quantityUsed": "2",
		"criticalItem": "0",
		"unitID": "1",
		"reservation": "4",
		"_deleted": "0"
		}
	 */
	protected function action_get86CountModifier($args)
	{
		return $this->action_get86CountTable('modifier_86', 'modifierID', $args);
	}

	/**
	 * Get forced_modifier_86 record
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
		"id": "3",
		"forced_modifierID": "740",
		"inventoryItemID": "35",
		"quantityUsed": "2",
		"criticalItem": "0",
		"unitID": "1",
		"reservation": "4",
		"_deleted": "0"
		}
	 */
	protected function action_get86CountForcedModifier($args)
	{
		return $this->action_get86CountTable('forced_modifier_86', 'forced_modifierID', $args);
	}

	/**
	 * Get 86 Count table records based on table name
	 * @param $tableName menuitem_86
	 * @param $column menuItemID
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
		"id": "3",
		"forced_modifierID": "740",
		"inventoryItemID": "35",
		"quantityUsed": "2",
		"criticalItem": "0",
		"unitID": "1",
		"reservation": "4",
		"_deleted": "0"
		}
	 */
	protected function action_get86CountTable($tableName, $column, $args)
	{
		$response = array();
		if (!empty($args['Items'])) {
			$idsArr = array();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}
			$response = CQ::getNonDeletedRowsWhereColumnIn($tableName, $column, $idsArr);

		}
		return $response;
	}

	/**
	 * Delete hard delete rows from menuitem_86 table
	 * @param $args [{"id":682},{"id":685}]
	 * @return bool true/false
	 */
	protected function action_delete86CountRowsMenuItems($args)
	{
		$response = array();
		if (!empty($args['Items'])) {
			$idsArr = array();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}
			return CQ::deleteRowInTable('menuitem_86', 'menuItemID', $idsArr);
		}

	}

	/**
	 * Delete hard delete rows from modifier_86 table
	 * @param $args [{"id":682},{"id":685}]
	 * @return bool true/false
	 */
	protected function action_delete86CountRowsModifiers($args)
	{
		$response = array();
		if (!empty($args['Items'])) {
			$idsArr = array();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}
			return CQ::deleteRowInTable('modifier_86', 'modifierID', $idsArr);
		}
	}

	/**
	 * Delete hard delete rows from forced_modifier_86 table
	 * @param $args [{"id":682},{"id":685}]
	 * @return bool true/false
	 */
	protected function action_delete86CountRowsForcedModifiers($args)
	{
		$response = array();
		if (!empty($args['Items'])) {
			$idsArr = array();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}
			return CQ::deleteRowInTable('forced_modifier_86', 'forced_modifierID', $idsArr);
		}
	}

	/**
	 * Delete soft delete rows from menuitem_86 table means update _deleted = 1
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
		"RowsUpdated": "2",
		}
	 */
	protected function action_delete86CountMenuItems($args)
	{
		if (!empty($args['Items'])) {
			$ids = array();
			foreach ($args['Items'] as $values) {
				$ids[] =  $values['id'];
			}
			$inClause = MQ::buildSanitizedListInParenthesis($ids, "'");
			$result = CQ::updateArray('menuitem_86', array('_deleted' => 1), ' WHERE `menuItemID` IN ' . $inClause);
			$rowsAffected = DBConnection::clientAffectedRows();
			return array("RowsUpdated" => $rowsAffected);
		}
	}

	/**
	 * Delete soft delete rows from modifier_86 table means update _deleted = 1
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
		"RowsUpdated": "2",
		}
	 */
	protected function action_delete86CountModifiers($args)
	{
		if (!empty($args['Items'])) {
			$ids = array();
			foreach ($args['Items'] as $values) {
				$ids[] =  $values['id'];
			}
			$inClause = MQ::buildSanitizedListInParenthesis($ids, "'");
			$result = CQ::updateArray('modifier_86', array('_deleted' => 1), ' WHERE `modifierID` IN ' . $inClause);
			$rowsAffected = DBConnection::clientAffectedRows();
			return array("RowsUpdated" => $rowsAffected);
		}
	}

	/**
	 * Delete soft delete rows from forced_modifier_86 table means update _deleted = 1
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
		"RowsUpdated": "2",
		}
	 */
	protected function action_delete86CountForcedModifiers($args)
	{
		if (!empty($args['Items'])) {
			$ids = array();
			foreach ($args['Items'] as $values) {
				$ids[] =  $values['id'];
			}
			$inClause = MQ::buildSanitizedListInParenthesis($ids, "'");
			$result = CQ::updateArray('forced_modifier_86', array('_deleted' => 1), ' WHERE `forced_modifierID` IN ' . $inClause);
			$rowsAffected = DBConnection::clientAffectedRows();
			return array("RowsUpdated" => $rowsAffected);
		}
	}

	/**
	 * Delete soft delete rows from menuitem_86 table means update _deleted = 1
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
	"RowsUpdated": "2",
	}
	 */
	protected function action_update86CountMenuItems($args)
	{
		$rowsAffected = 0;
		if (!empty($args['Items'])) {
			foreach ($args['Items'] as $key => $values) {
				$menuItemID = $values['menuItemID'];
				$inventoryItemID = $values['inventoryItemID'];
				CQ::updateArray('menuitem_86', array('_deleted'=>0, 'quantityUsed'=> $values['quantityUsed']), " WHERE `menuItemID` = $menuItemID AND `inventoryItemID` = $inventoryItemID" );
				$rowsAffected += DBConnection::clientAffectedRows();
			}
			return array("RowsUpdated" => $rowsAffected);
		}
	}

	/**
	 * Delete soft delete rows from modifier_86 table means update _deleted = 1
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
	"RowsUpdated": "2",
	}
	 */
	protected function action_update86CountModifiers($args)
	{
		$rowsAffected = 0;
		if (!empty($args['Items'])) {
			$ids = array();
			foreach ($args['Items'] as $values) {
				$modifierID = $values['modifierID'];
				$inventoryItemID = $values['inventoryItemID'];
				CQ::updateArray('modifier_86', array('_deleted'=>0, 'quantityUsed'=> $values['quantityUsed']), " WHERE `modifierID` = $modifierID AND `inventoryItemID` = $inventoryItemID" );
				$rowsAffected += DBConnection::clientAffectedRows();
			}
			return array("RowsUpdated" => $rowsAffected);
		}
	}

	/**
	 * Update a forced modifiers row
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{
	"RowsUpdated": "2",
	}
	 */
	protected function action_update86CountForcedModifiers($args)
	{

		$rowsAffected = 0;
		if (!empty($args['Items'])) {
			$ids = array();
			foreach ($args['Items'] as $values) {
				$forced_modifierID = $values['forced_modifierID'];
				$inventoryItemID = $values['inventoryItemID'];
				CQ::updateArray('forced_modifier_86', array('_deleted'=>0, 'quantityUsed'=> $values['quantityUsed']), " WHERE `forced_modifierID` = $forced_modifierID AND `inventoryItemID` = $inventoryItemID" );
				$rowsAffected += DBConnection::clientAffectedRows();
			}
			return array("RowsUpdated" => $rowsAffected);
		}
	}

	/**
	 * @return json string
	 */
	protected function action_getAllWasteReasons(){
		$rowResult = CQ::getAllRows('inventory_waste_reasons');
		return $rowResult;
	}

	protected function action_getInventorySettings(){
		$inventory_settings = InventoryQueries::getInventorySettings();
		$rows = MarshallingArrays::resultToArray($inventory_settings);
		return $rows;
	}

	/*Menu Items*/
	protected function action_getAllMenuItems(){
		$result = InventoryQueries::getAllMenuItems();
		return $result;
	}

	protected function action_getMenuItemsByInventoryItemID($args){
		$ids = $args['Items'];
		$result = InventoryQueries::getMenuItemsByInventoryItemID($ids);
		return $result;
	}

	/*Modifiers*/
	protected function action_getAllModifiers(){
		$result = InventoryQueries::getAllModifiers();
		return $result;
	}

	protected function action_getAllModifiersByInventoryItemID($args){
		$ids = $args['Items'];
		$result = InventoryQueries::getAllModifiersByInventoryItemID($ids);
		return $result;
	}

	/*Forced Modifiers*/
	protected function action_getAllForcedModifiers(){
		$result = InventoryQueries::getAllForcedModifiers();
		return $result;
	}

	protected function action_getAllForcedModifiersByInventoryItemID($args){
		$ids = $args['Items'];
		$result = InventoryQueries::getAllForcedModifiersByInventoryItemID($ids);
		return $result;
	}

	protected function action_getVendorsByName($args){
		return InventoryQueries::getVendorsByName($args['Items']);
	}

	protected function action_getCustomUnitsOfMeasure(){
		return InventoryQueries::getCustomUnitsOfMeasure();
	}

	protected function action_getSalesRevenueForDateRange($args){
		$vars = $args['Items'];
		$rowResult = InventoryQueries::getSalesRevenueForDateRange($vars);
		return $rowResult;
	}

	protected function action_getUsers($args){
		$vars = $args['Items'];
		$rowResult = InventoryQueries::getUsers($vars);
		return $rowResult;
	}

	protected function action_getLegacyInventoryItems(){
		$inProgress = InventoryQueries::getConfigSetting("INVENTORY_MIGRATION_STATUS", "In Progress");
		if ($inProgress) {
			return $inProgress;
		}
		$result = InventoryQueries::getLegacyInventoryItems();
		foreach ($result as &$item) {
			if ($item['quantity'] <= 0) {
				$item['quantity'] = 0;
			}
		}
		return $result;
	}

	protected static function action_getStandardUnitConversion(){
		$result = InventoryQueries::getAllUnitIDsConversions();
		return $result;
	}

	protected function action_getUnitConversion($args){
		$pUOM = $args['Items']['pUOM'];
		$sUOM = $args['Items']['sUOM'];
		$sUOMS = $args['Items']['sUOMS'];
		$result = InventoryQueries::getUnitConversion($pUOM, $sUOM, $sUOMS);
		return $result;
	}

	/**
	 * Get Stock status of menu_items based on 86Count lowQuantity
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getMenuItemsLowQuantityStatus($args)
	{
		$itemIds = array();
		$comboItemIds = array();
		$tableName = 'menu_items';
		$table86 = 'menuitem_86';
		$table86Column = 'menuItemID';
		$inStockItemIds = array();
		if (!empty($args['Items'])) {
			$idsArr = array();
			$redisController = new InventoryRedisController();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}
			$cacheResponse = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getMenuItems());
			$dbCacheIds = array_diff_key(array_flip($idsArr), $cacheResponse);
			$dbResponse = array();
			if (!empty($dbCacheIds)) {
				//Fetch only given column value to avoid out of memory error
				$selColumns = array('id', 'track_86_count', 'inv_count', 'combo');
				$dbResponse = CQ::getNonDeletedRowsWhereColumnIn($tableName, 'id', array_keys($dbCacheIds), $selColumns);
			}
			$cacheRequest = array();
			if (!empty($dbResponse)) {
				$cacheRequest = array();
				foreach ($dbResponse as $details) {
					$cacheRequest[$details['id']] = $details;
				}

			}
			$cacheDbResponse = $cacheResponse +  $cacheRequest;
			$redisController->setMenuItems($cacheDbResponse);
			foreach ($idsArr as $id) {
				if (isset($cacheDbResponse[$id])) {
					$menuItemResponse[] = $cacheDbResponse[$id];
				}
			}
			if (empty($menuItemResponse)) {
				$response['error'] = 'Item not found';
				return array($response);
			}
			$menuItemTrack86Count = array();
			$comboIds = array();
			foreach ($menuItemResponse as $responseDetails) {
				$menuItemTrack86Count[$responseDetails['id']] = $responseDetails['track_86_count'];
				if ($responseDetails['combo'] == 1) {
					$comboIds[] =  $responseDetails['id'];
				}
			}

			foreach ($args['Items'] as $key => $idQuantity ) {
				if (in_array($idQuantity['id'], $comboIds)) {
					$comboItemIds[$key]['id'] = $idQuantity['id'];
					$comboItemIds[$key]['quantity'] = $idQuantity['quantity'];
				} else {
					if (isset($menuItemTrack86Count[$idQuantity['id']]) && $menuItemTrack86Count[$idQuantity['id']] == '86countInventory') {
						$itemIds['ids'][] = $idQuantity['id'];
						if (!isset($itemIds['quantity'][$idQuantity['id']])) {
							$itemIds['quantity'][$idQuantity['id']] = 0;
						}
						$itemIds['quantity'][$idQuantity['id']] += $idQuantity['quantity'];
					} else {
						$inStockItemIds[$idQuantity['id']] = 'IN_STOCK';
					}
				}
			}
		} else {
			//Fetch only given column value to avoid out of memory error
			$selColumns = array('id', 'track_86_count', 'combo');
			$menuItemRows = CQ::getNonDeletedAllRows($tableName, $selColumns);
			if (!empty($menuItemRows)) {
				foreach ($menuItemRows as $key => $menuItemArr) {
					if ($menuItemArr['combo'] == 1) {
						$comboItemIds[$key]['id'] = $menuItemArr['id'];
						$comboItemIds[$key]['quantity'] = 1;
					} else {
						if ($menuItemArr['track_86_count'] == '86countInventory') {
							$itemIds['ids'][] = $menuItemArr['id'];
							$itemIds['quantity'][$menuItemArr['id']] = 1;
						} else {
							$inStockItemIds[$menuItemArr['id']] = 'IN_STOCK';
						}
					}
				}
			}
		}

		$comboItemResponse = array();
		$menuItemresponse = array();
		if (!empty($itemIds)) {
			list($menuItemresponse,) = self::getStockLowQuantityStatus($itemIds, $table86, $table86Column);
		}

		if (!empty($comboItemIds)) {
			list($comboItemResponse,) = self::getComboItemsStockStatus($comboItemIds, true);
		}
		$response = $inStockItemIds + $menuItemresponse + $comboItemResponse;
		return $response;
	}

	/**
	 * Get Stock status of forced modifier status based on 86Count lowQuantity
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getForcedModifiersLowQuantityStatus($args)
	{
		$itemIds = array();
		$tableName = 'forced_modifiers';
		if (!empty($args['Items'])) {
			$idsArr = array();
			$redisController = new InventoryRedisController();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}
			$cacheResponse = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getForcedModifiers());
			$dbCacheIds = array_diff_key(array_flip($idsArr), $cacheResponse);
			$dbResponse = array();
			if (!empty($dbCacheIds)) {
				//Fetch only given column value to avoid out of memory error
				$selColumns = array('id', 'title');
				$dbResponse = CQ::getNonDeletedRowsWhereColumnIn($tableName, 'id', array_keys($dbCacheIds), $selColumns);
			}
			$cacheRequest = array();
			if (!empty($dbResponse)) {
				foreach ($dbResponse as $details) {
					$cacheRequest[$details['id']] = $details;
				}
			}
			$cacheDbResponse = $cacheResponse +  $cacheRequest;
			$redisController->setForcedModifiers($cacheDbResponse);
			foreach ($idsArr as $id) {
				$forcedModifierResponse[] = $cacheDbResponse[$id];
			}

			foreach ($forcedModifierResponse as $responseDetails) {
				$forceModifierIdsTitles[$responseDetails['id']] = $responseDetails['title'];
			}
			foreach ($args['Items'] as $key => $idQuantity ) {
				$itemIds['ids'][] = $idQuantity['id'];
				if (!isset($itemIds['quantity'][$idQuantity['id']])) {
					$itemIds['quantity'][$idQuantity['id']] = 0;
				}
				$itemIds['quantity'][$idQuantity['id']] += $idQuantity['quantity'];
				$idsNamesMap[$idQuantity['id']] = $forceModifierIdsTitles[$idQuantity['id']];
			 }
		} else {
			//Fetch only given column value to avoid out of memory error
			$selColumns = array('id', 'title');
			$forcedModifierItemRows = CQ::getNonDeletedAllRows($tableName, $selColumns);
			if (!empty($forcedModifierItemRows)) {
				foreach ($forcedModifierItemRows as $key => $forcedModifierItemArr) {
					$itemIds['ids'][]  = $forcedModifierItemArr['id'];
					$itemIds['quantity'][$forcedModifierItemArr['id']] = 1;
					$idsNamesMap[$forcedModifierItemArr['id']] = $forcedModifierItemArr['title'];
				}
			}
		}
		list($response) = self::getStockLowQuantityStatus($itemIds, 'forced_modifier_86', "forced_modifierID", $idsNamesMap, false);
		$finalResponse = array();
		foreach ($response as $details) {
			$finalResponse[$details['title']] = $details['status'];
		}
		return $finalResponse;
	}

	/**
	 * Get Stock status of modifiers based on 86Count lowQuantity
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getModifiersLowQuantityStatus($args)
	{
		$itemIds = array();
		$tableName = 'modifiers';
		if (!empty($args['Items'])) {
			$idsArr = array();
			$redisController = new InventoryRedisController();
			foreach ($args['Items'] as $values) {
				$idsArr[] =  $values['id'];
			}
			$cacheResponse = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getModifiers());
			$dbCacheIds = array_diff_key(array_flip($idsArr), $cacheResponse);
			$dbResponse = array();
			if (!empty($dbCacheIds)) {
				//Fetch only given column value to avoid out of memory error
				$selColumns = array('id', 'title');
				$dbResponse = CQ::getNonDeletedRowsWhereColumnIn($tableName, 'id', array_keys($dbCacheIds), $selColumns);
			}
			$cacheRequest = array();
			if (!empty($dbResponse)) {
				foreach ($dbResponse as $details) {
					$cacheRequest[$details['id']] = $details;
				}
			}
			$cachDbResponse = $cacheResponse +  $cacheRequest;
			$redisController->setModifiers($cachDbResponse);
			foreach ($idsArr as $id) {
				$modifierResponse[] = $cachDbResponse[$id];
			}
			foreach ($modifierResponse as $responseDetails) {
				$modifierIdsTitles[$responseDetails['id']] = $responseDetails['title'];
			}
			foreach ($args['Items'] as $key => $idQuantity ) {
				$itemIds['ids'][] = $idQuantity['id'];
				if (!isset($itemIds['quantity'][$idQuantity['id']])) {
					$itemIds['quantity'][$idQuantity['id']] = 0;
				}
				$itemIds['quantity'][$idQuantity['id']] += $idQuantity['quantity'];
				$idsNamesMap[$idQuantity['id']] = $modifierIdsTitles[$idQuantity['id']];
			}
		} else {
			//Fetch only given column value to avoid out of memory error
			$selColumns = array('id', 'title');
			$modifierItemRows = CQ::getNonDeletedAllRows($tableName, $selColumns);
			if (!empty($modifierItemRows)) {
				foreach ($modifierItemRows as $key => $modifierItemArr) {
					$itemIds['ids'][]  = $modifierItemArr['id'];
					$itemIds['quantity'][$modifierItemArr['id']] = 1;
					$idsNamesMap[$modifierItemArr['id']] = $modifierItemArr['title'];
				}
			}
		}
		list($response) = self::getStockLowQuantityStatus($itemIds, 'modifier_86', 'modifierID', $idsNamesMap, false);
		$finalResponse = array();
		foreach ($response as $details) {
			$finalResponse[$details['title']] = $details['status'];
		}
		return $finalResponse;
	}

	/**
	 * Get Stock status according to ingredients quantity and 86count (Low Quantity) availability
	 * @param $inventoryItems [740] => Array
	(
	[1] => Array
	(
	[menuItemID] => 740
	[inventoryItemID] => 1
	[quantityUsed] => 4
	[lowQuantity] => 4
	)

	[35] => Array
	(
	[menuItemID] => 740
	[inventoryItemID] => 35
	[quantityUsed] => 2
	[lowQuantity] => 2
	)
	 * }
	 * @return array
	 */
	// Used by action_getMenuItemsLowQuantityStatus(), action_getModifiersLowQuantityStatus, action_getForcedModifiersLowQuantityStatus()
	protected function getStockLowQuantityStatus($itemIds, $tableName, $whereColumn, $idsNamesMap = array(), $idMap = true)
	{
		$response = array();
		if (!empty($itemIds)) {
			$ids = $itemIds['ids'];
			$redisController = new InventoryRedisController();
			$cacheResponse = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory86Count($tableName));
			$dbCacheIds = array_diff_key(array_flip($ids), $cacheResponse);
			$inventoryItems = array();
			$dbResponse = array();
			if (!empty($dbCacheIds)) {
				$dbResponse = InventoryQueries::getStockStatus(array_keys($dbCacheIds), $tableName, $whereColumn);
			}
			$cache86CountRequest = array();

			if (!empty($dbResponse)) {
				foreach ($dbResponse as $details) {
					$cache86CountRequest[$details['objectID']][] = $details;
				}
				$redisController->setInventory86Count($tableName, $cache86CountRequest);
			}

			$cacheDbResponse = $cacheResponse + $cache86CountRequest;
			foreach ($ids as $id) {
				if (isset($cacheDbResponse[$id]) ) {
					$inventoryItems[] = $cacheDbResponse[$id];
				}
			}
			$inventoryItemStatus = array();
			$inventoryIds = array();
			//loop through our menu usage and tally up the total reserved quantities for each inventory item
			foreach ($inventoryItems as $inventoryItemsDetails) {
				foreach ($inventoryItemsDetails as $inventoryItemArr) {
					$inventoryIds[$inventoryItemArr['inventoryItemID']] = $inventoryItemArr['inventoryItemID'];
				}
			}

			$cachInventoryDetails = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
			$dbInventoryIds = array_diff_key($inventoryIds, $cachInventoryDetails);
			$dbInventoryDetails = array();
			if (!empty($dbInventoryIds)) {
				$inventoryReserveQuantity = InventoryQueries::getTotal86ReserveQuantity($dbInventoryIds);
				foreach ($inventoryReserveQuantity as $inventoryReserveArr) {
					$inventoryItemStatus[$inventoryReserveArr['inventoryItemID']] = $inventoryReserveArr['reserve'];
					$args['Items'][]['id'] = $inventoryReserveArr['inventoryItemID'];
				}
				$inventoryDetails = self::action_getInventoryItemsByIDs($args);
				foreach ($inventoryDetails as $iDetails) {
					$cachInventoryDetails[$iDetails['id']]['quantityOnHand'] = $iDetails['quantityOnHand'] - $iDetails['inventoryOverage'];
					$cachInventoryDetails[$iDetails['id']]['reserve'] = $inventoryItemStatus[$iDetails['id']];
					$cachInventoryDetails[$iDetails['id']]['salesUnitID'] = $iDetails['salesUnitID'];
					$cachInventoryDetails[$iDetails['id']]['lowQuantity'] = $iDetails['lowQuantity'];
					$cachInventoryDetails[$iDetails['id']]['reOrderQuantity'] = $iDetails['reOrderQuantity'];
				}
			}
			if (!empty($cachInventoryDetails)) {
				$redisController->setInventory($cachInventoryDetails);
			}
			$finalInventoryDetails = $cachInventoryDetails;

			//Figure out whether or not we have enough inventory to make each menu item.
			$statusResponse = array();
			$outStockIds = array();
			$inStock = 'IN_STOCK';
			$lowStock = 'LOW_STOCK';
			$outStock = 'OUT_STOCK';
			$menuIds = array();
			foreach ($inventoryItems as $inventoryItemsDetails) {
				foreach ($inventoryItemsDetails as $inventoryItemArr) {
					$id = $inventoryItemArr['objectID'];
					$menuIds[$id] = $id;
					if (!isset($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'])) {
						$finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] = 0;
					}
					if (!isset($outStockIds[$id])) {
						if ($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['lowQuantity'] != 0) {
							$threshold = $inventoryItemArr['quantityUsed'] * 2;
							if (($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] <= $inventoryItemArr['quantityUsed']) || ($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] <= $finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['lowQuantity'])) {
								$statusResponse[$id][] = $outStock;
								$outStockIds[$id] = $outStock;
							} else if (($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] <= ($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['lowQuantity'] + $threshold)) && ($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] > $finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['lowQuantity'])) {
								$statusResponse[$id][] = $lowStock;
							} else {
								$statusResponse[$id][] = $inStock;
							}
						} else {
							if (($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] - $inventoryItemArr['quantityUsed']) == 0) {
								$statusResponse[$id][] = $lowStock;
							} else if (($finalInventoryDetails[$inventoryItemArr['inventoryItemID']]['quantityOnHand'] - $inventoryItemArr['quantityUsed']) < 0) {
								$statusResponse[$id][] = $outStock;
								$outStockIds[$id] = $outStock;
							} else {
								$statusResponse[$id][] = $inStock;
							}
						}
					}
				}
			}

			$not86Menu = array_diff($itemIds['ids'], $menuIds);
			if (!empty($not86Menu)) {
				foreach ($not86Menu as $id) {
					$statusResponse[$id][] = $inStock;
				}
			}
			foreach ($statusResponse as $id => $statusArr) {
				if (in_array($outStock, $statusArr)) {
					$status = $outStock;
				} else if (!in_array($outStock, $statusArr) && in_array($lowStock, $statusArr)) {
					$status = $lowStock;
				} else {
					$status = $inStock;
				}
				if ($idMap) {
					$response[$id] = $status;
				} else {
					$response[$id]['title'] = $idsNamesMap[$id];
					$response[$id]['status'] = $status;
				}
			}
		} else {
			return array('error' => 'Record not found');
		}
		return array($response);
	}
	/**
	 * Get Stock status of forced modifier status based on 86Count lowQuantity
	 * @param $args [{"id":682},{"id":685}]
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function action_getInventorysReorderStatus($args)
	{
		$inventoryItemIds = array();
		if (!empty($args['Items'])) {
			foreach ($args['Items'] as $key => $ids) {
				$inventoryItemIds[$key]['id'] = $ids['id'];
			}
		} else {
			//Fetch only given column value to avoid out of memory error
			$selColumns = array('id');
			$inventoryItemRows = CQ::getAllRows('inventory_items', '', $selColumns);
			if (!empty($inventoryItemRows)) {
				foreach ($inventoryItemRows as $key => $itemArr) {
					$inventoryItemIds[$key]['id'] = $itemArr['id'];
				}
			}
		}
		$response = self::getStockReorderStatus($inventoryItemIds);
		return $response;
	}


	/**
	 * Get Stock status according to reorder quantity and make alert
	 * @param $inventoryId=Array
	(
	[0] => Array
	(
	[id] => 740
	)

	[1] => Array
	(
	[id] => 741
	)
	 * )
	 * @return array
	 */
	// Used by action_getInventorysReorderStatus()
	protected function getStockReorderStatus($inventoryIds)
	{
		$response = array();
		foreach ($inventoryIds as $key => $idsArr) {
			$lowStock = 0;
			$outStock = 0;
			$iid = $idsArr['id'];
			if ($iid > 0) {
				//Fetch only given column value to avoid out of memory error
				$selColumns = array('quantityOnHand', 'reOrderQuantity');
				$inventoryItemRow = CQ::getRowByID('inventory_items', $iid, '', $selColumns);
				if (!empty($inventoryItemRow)) {
					if ($inventoryItemRow['quantityOnHand'] <= $inventoryItemRow['reOrderQuantity']) {
						$response[$iid] = 'LOW_STOCK';
					} else {
						$response[$iid] = 'IN_STOCK';
					}
				}
			}
		}
		return $response;
	}

	protected function action_pauseMigration($args){
		$result = array();
		$Items = $args['Items'];
		if (empty($Items[0]['id']) || !isset($Items[0]['id'])) {
			if (isset($Items[0]['id'])) {
				foreach ($Items as $key => $value) {
					unset($value["id"]);
				}
			}
		   $result =  self::action_addInventoryItems($args);
		   InventoryQueries::createOrUpdateConfigSetting("INVENTORY_MIGRATION_STATUS", "In Progress", "inventory_settings");
		}
		else {
			$result = self::action_updateInventoryItems($args);
		}
		return $result;
	}

	protected function action_resumeMigration($args){
		$result = InventoryQueries::getAllInventoryItems();
		return $result;
	}
	protected function action_finalizeMigration($args){

		$result = array();
		$Items = $args['Items'];
		if (empty($Items[0]['id']) || !isset($Items[0]['id'])) {
			if (isset($Items[0]['id'])) {
				foreach ($Items as $key => $value) {
					unset($value["id"]);
				}
			}
			$result =  self::action_addInventoryItems($args);
			InventoryQueries::createOrUpdateConfigSetting("INVENTORY_MIGRATION_STATUS", "Completed", "inventory_setting");
		}
		else {
			$result = self::action_updateInventoryItems($args);
			InventoryQueries::createOrUpdateConfigSetting("INVENTORY_MIGRATION_STATUS", "Completed", "inventory_setting");
		}
		return $result;
	}

	protected function action_startMigration($args){
			$setting = "INVENTORY_MIGRATION_STATUS";
			$value = $args['Items'][0]['status'];
			$type = "inventory_setting";
			InventoryQueries::createOrUpdateConfigSetting($setting, $value, $type );
	}

	protected function action_getConfigSetting($args){
		$setting = $args['Items'][0]['setting'];
		return InventoryQueries::getConfigSetting($setting, "");
	}

	protected function action_createOrUpdateConfigSetting($args){
		$setting = $args['Items'][0]['setting'];
		$value = $args['Items'][0]['value'];
		$type = $args['Items'][0]['type'];
		InventoryQueries::createOrUpdateConfigSetting($setting, $value, $type);
	}

	//Returns the cost per item for inventory_item with the given ID.
	protected function getCostForItemID($id){
		$item = InventoryQueries::getInventoryItemByID($id);
		$costPerItem = 0;
		if (!$item) {
			return 0;
		}
		$settingQuery = InventoryQueries::getInventorySetting("inventory_costing_method");
		if ($settingQuery[0]['value'] == 'average_cost_periodic') {
			$valueForItem = $this->getValueByID($id);
			if ($item[0]['quantityOnHand'] > 0) {
				$costPerItem = $valueForItem / $item[0]['quantityOnHand'];
			} else {
				$costPerItem = 0;
			}
		}
		else if ($settingQuery[0]['value'] == 'fifo') {
			$timeLine = $this->getPriceTimelineForItem($item[0]);
			$quantity = $item[0]['quantityOnHand'];
			foreach ($timeLine as $costEvent) {
				$quantity = $quantity - $costEvent['salesQuantity'];
				if ($quantity <= 0) {
					$costPerItem = $costEvent['salesCost'];
					break;
				}
			}
		}
		 else{
			$costPerItem = $item[0]['recentPurchaseCost'];
		}

		return $costPerItem;
	}

	/**
	 * Get Stock status of combo menu items
	 * @param $comboItemIds [{"id":682},{"id":685}]
	 * @param $lowStock If true then check Status based on lowQuantity else reserve quantity
	 * @param $tempInventoryQuantity stored used menu item quantity
	 * @param $menuItemsStatus menu item id with stock status
	 * @return json string {"DataResponse":{"DataResponse":{"682":"LOW_STOCK","685":"OUT_STOCK"}},"RequestID":0,
	 * "Status":"Success"}
	 */
	protected function getComboItemsStockStatus($comboItemIds, $lowStockFlag = false, $tempInventoryQuantity = array(), $menuItemsStatus = array())
	{
		$tableName = 'combo_items';
		$whereCoulmn = 'combo_menu_id';
		$outStock = 'OUT_STOCK';
		$lowStock = 'LOW_STOCK';
		$inStock = 'IN_STOCK';
		$comboItemIdsQuantity = array();
		$comboItemStatus = array();
		if (!empty($comboItemIds)) {
			$redisController = new InventoryRedisController();
			$idsArr = array();
			foreach ($comboItemIds as $values) {
				$idsArr[] =  $values['id'];
				$comboItemIdsQuantity[$values['id']] = $values['quantity'];
			}

			$cacheResponse = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getComboItems());
			$dbCacheIds = array_diff_key(array_flip($idsArr), $cacheResponse);
			$dbResponse = array();
			if (!empty($dbCacheIds)) {
				//Fetch only given column value to avoid out of memory error
				$selColumns = array('id', 'qty', 'combo_menu_id', 'parent_id', 'menu_item_id', );
				$dbResponse = CQ::getNonDeletedRowsWhereColumnIn($tableName, $whereCoulmn, $idsArr, $selColumns);
			}
			$cacheRequest = array();
			if (!empty($dbResponse)) {
				$cacheRequest = array();
				foreach ($dbResponse as $details) {
					$cacheRequest[$details['combo_menu_id']][] = $details;
				}

				$redisController->setComboItems($cacheRequest);
			}
			$cacheDbResponse = $cacheResponse +  $cacheRequest;

			foreach ($idsArr as $id) {
				if (isset($cacheDbResponse[$id])) {
					foreach ($cacheDbResponse[$id] as $cacheDbResponseDetails) {
						$comboItemResponse[] = $cacheDbResponseDetails;
					}
				}
			}
			$comboMenuStockStatus = array();
			foreach ($comboItemResponse as $key => $comboDetails) {
				$quantity = ($comboDetails['qty'] > 0 ) ? $comboItemIdsQuantity[$comboDetails['combo_menu_id']] * $comboDetails['qty'] : $comboItemIdsQuantity[$comboDetails['combo_menu_id']];
				if ($comboDetails['parent_id'] == 0 && $comboDetails['menu_item_id'] > 0) {
					if (!isset($menuItemsStatus[$comboDetails['menu_item_id']])) {
						$comboMenuItems[$comboDetails['combo_menu_id']]['main']['ids'][$comboDetails['menu_item_id']] = $comboDetails['menu_item_id'];
						if (!isset($comboMenuItems[$comboDetails['combo_menu_id']]['main']['quantity'][$comboDetails['menu_item_id']])) {
							$comboMenuItems[$comboDetails['combo_menu_id']]['main']['quantity'][$comboDetails['menu_item_id']] = 0;
						}
						$comboMenuItems[$comboDetails['combo_menu_id']]['main']['quantity'][$comboDetails['menu_item_id']] += $quantity;
					} else {
						$comboMenuStockStatus[$comboDetails['combo_menu_id']][] = $menuItemsStatus[$comboDetails['menu_item_id']];
					}
				}

				if ($comboDetails['parent_id'] > 0 && $comboDetails['menu_item_id'] > 0) {
					if (!isset($menuItemsStatus[$comboDetails['menu_item_id']])) {
						$comboMenuItems[$comboDetails['combo_menu_id']]['optional']['ids'][$comboDetails['menu_item_id']] = $comboDetails['menu_item_id'];
						if (!isset($comboMenuItems[$comboDetails['combo_menu_id']]['optional']['quantity'][$comboDetails['menu_item_id']])) {
							$comboMenuItems[$comboDetails['combo_menu_id']]['optional']['quantity'][$comboDetails['menu_item_id']] = 0;
						}
						$comboMenuItems[$comboDetails['combo_menu_id']]['optional']['quantity'][$comboDetails['menu_item_id']] += $quantity;
					} else {
						$comboMenuStockStatus[$comboDetails['combo_menu_id']][] = $menuItemsStatus[$comboDetails['menu_item_id']];
					}
				}
			}
			if (!empty($comboMenuStockStatus)) {
			foreach ($comboMenuStockStatus as $comboid => $comboStatus) {
					if (in_array($outStock, $comboStatus)) {
						$comboItemStatus[$comboid] = $outStock;
					} else if (in_array($lowStock, $comboStatus)) {
						$comboItemStatus[$comboid] = $lowStock;
					} else {
						$comboItemStatus[$comboid] = $inStock;
					}
				}
			}

			if (!empty($comboMenuItems)) {
				foreach ($comboMenuItems as $comboId => $comboItems) {
					$itemIds = array();
					if (!empty($comboItems['main']['ids'])) {
						foreach ($comboItems['main']['ids'] as $id) {
							$itemIds['Items'][$id]['id'] = $id;
							if (!isset($itemIds['Items'][$id]['quantity'])) {
								$itemIds['Items'][$id]['quantity'] = 0;
							}
							$itemIds['Items'][$id]['quantity'] += $comboMenuItems[$comboId]['main']['quantity'][$id];
						}
					}
					if (!empty($comboItems['optional']['ids'])) {
						foreach ($comboItems['optional']['ids'] as $oId) {
							$itemIds['Items'][$oId]['id'] = $oId;
							if (!isset($itemIds['Items'][$oId]['quantity'])) {
								$itemIds['Items'][$oId]['quantity'] = 0;
							}
							$itemIds['Items'][$oId]['quantity'] += $comboMenuItems[$comboId]['optional']['quantity'][$oId];
						}
					}

					if (!empty($itemIds['Items'])) {
						if ($lowStockFlag) {
							$itemsStatus = self::action_getMenuItemsLowQuantityStatus($itemIds);
						} else {
							list($itemsStatus, $tempInventoryQuantity) = self::action_getMenuItemsStatusWrapper($itemIds, $tempInventoryQuantity);
						}

						if (is_array($itemsStatus) && !empty($itemsStatus)) {
							if (!empty($comboItems['main']['ids'])) {
								$mainItemsStatus = array_intersect_key($itemsStatus, $comboItems['main']['ids']);
								if (in_array($outStock, $mainItemsStatus)) {
									$comboItemStatus[$comboId] = $outStock;
								} else if (in_array($lowStock, $mainItemsStatus)) {
									$comboItemStatus[$comboId] = $lowStock;
								} else {
									if (isset($comboItemStatus[$comboId]) && $comboItemStatus[$comboId] == $outStock) {
										$comboItemStatus[$comboId] = $outStock;
									} else if (isset($comboItemStatus[$comboId]) && $comboItemStatus[$comboId] == $lowStock) {
										$comboItemStatus[$comboId] = $lowStock;
									} else {
										$comboItemStatus[$comboId] = $inStock;
									}
								}
							}

							if (!empty($comboItems['optional']['ids'])) {
								$optionalItemsStatus = array_intersect_key($itemsStatus, $comboItems['optional']['ids']);
								if (isset($comboItemStatus[$comboId]) && $comboItemStatus[$comboId] != $outStock) {
									$totalCount = count($optionalItemsStatus);
									$outStockCount = count(array_keys($optionalItemsStatus, $outStock));
									$inStockCount = count(array_keys($optionalItemsStatus, $inStock));
									if ($totalCount == $outStockCount) {
										$comboItemStatus[$comboId] = $outStock;
									} else if ($totalCount > $outStockCount) {
										if ($totalCount == $inStockCount && $comboItemStatus[$comboId] == $inStock) {
											if (isset($comboItemStatus[$comboId]) && $comboItemStatus[$comboId] == $outStock) {
												$comboItemStatus[$comboId] = $outStock;
											} else if (isset($comboItemStatus[$comboId]) && $comboItemStatus[$comboId] == $lowStock) {
												$comboItemStatus[$comboId] = $lowStock;
											} else {
												$comboItemStatus[$comboId] = $inStock;
											}
										} else if ($totalCount == $inStockCount && $comboItemStatus[$comboId] == $lowStock) {
											$comboItemStatus[$comboId] = $lowStock;
										} else {
											$comboItemStatus[$comboId] = $lowStock;
										}
									}
								} else {
									$totalCount = count($optionalItemsStatus);
									$outStockCount = count(array_keys($optionalItemsStatus, $outStock));
									$inStockCount = count(array_keys($optionalItemsStatus, $inStock));
									if ($totalCount == $outStockCount) {
										$comboItemStatus[$comboId] = $outStock;
									} else if ($totalCount > $outStockCount && in_array($lowStock, $optionalItemsStatus)) {
										$comboItemStatus[$comboId] = $lowStock;
									} else if ($totalCount == $inStockCount) {
										if (isset($comboItemStatus[$comboId]) && $comboItemStatus[$comboId] == $outStock) {
											$comboItemStatus[$comboId] = $outStock;
										} else if (isset($comboItemStatus[$comboId]) && $comboItemStatus[$comboId] == $lowStock) {
											$comboItemStatus[$comboId] = $lowStock;
										} else {
											$comboItemStatus[$comboId] = $inStock;
										}
									} else {
										$comboItemStatus[$comboId] = $lowStock;
									}
								}
							}
						}
					} else {
						$comboItemStatus[$comboId] = $inStock;
					}
				}
			}
		}

		return array($comboItemStatus, $tempInventoryQuantity);
	}

	/**
	 * Get Stock status of menu_items, forced modifier and optional modifier, If same inventory used in these.
	 * @param Input:- [{"item_id":[{"id":740,"quantity":2}, {"id":741,"quantity":1}],"forced_modifier_id":[{"id":745,"quantity":2},{"id":746,"quantity":3}],"optional_modifier_id":[{"id":747,"quantity":2},{"id":748,"quantity":3}]}]
	 *   Output:- json string {"DataResponse":{"DataResponse":{"item_id":[{"id":740,"status":"IN_STOCK"},{"id":741,"status":"IN_STOCK"}],"forced_modifier_id":[{"id":745,"status":"OUT_STOCK"},{"id":746,"status":"IN_STOCK"}],"optional_modifier_id":[{"id":747,"status":"IN_STOCK"},{"id":748,"status":"OUT_STOCK"}]}},"RequestID":0,"Status":"Success"}
	 */
	protected function action_getMenuStatus($menu)
	{
		$tempInventoryQuantity = array();
		$args = array();
		$itemsResponse = array();
		$forceResponse = array();
		$optionalResponse = array();
		if (!empty($menu['Items'][0])) {
			if (isset($menu['Items'][0]['item_id'])) {
				$args['Items'] = $menu['Items'][0]['item_id'];
				$menuId = $menu['Items'][0]['item_id'][0]['id'];
				if (!empty($args['Items'])) {
					list($itemsStatus, $tempInventoryQuantity, $noTrackMenuItems) = self::action_getMenuItemsStatusWrapper($args, $tempInventoryQuantity);
					if (!isset($itemsStatus['error'])) {
						if (isset($args['Items'][0]['icid'])) {
							foreach ($args['Items'] as $menuDetails) {
								$menuIcIds[$menuDetails['id']] = $menuDetails['icid'];
							}
						}
						$key = 0;
						foreach ($itemsStatus as $id => $status) {
							$itemsResponse[$key]['id'] = $id;
							$itemsResponse[$key]['status'] = $status;
							if (isset($menuIcIds[$id]) && $menuIcIds[$id]) {
								$itemsResponse[$key]['icid'] = $menuIcIds[$id];
							}
							$key++;
						}
						$response['item_id'] = $itemsResponse;
					}
				}
			}

			if (isset($menu['Items'][0]['forced_modifier_id'])) {
				$args['Items'] = $menu['Items'][0]['forced_modifier_id'];
				if (!empty($args['Items'])) {
					if (in_array($menuId, $noTrackMenuItems)) {
						foreach ($args['Items'] as $key => $forcedModifierDetails) {
							$forceResponse[$key]['id'] = $forcedModifierDetails['id'];
							$forceResponse[$key]['status'] = 'IN_STOCK';
						}
					} else {
						list($forceStatus, $tempInventoryQuantity) = self::action_getForcedModifiersStatusWrapper($args, $tempInventoryQuantity);
						$key = 0;
						foreach ($forceStatus as $id => $status) {
							$forceResponse[$key]['id'] = $id;
							$forceResponse[$key]['status'] = $status;
							$key++;
						}
					}
					$response['forced_modifier_id'] = $forceResponse;
				}
			}

			if (isset($menu['Items'][0]['optional_modifier_id'])) {
				$args['Items'] = $menu['Items'][0]['optional_modifier_id'];
				if (!empty($args['Items'])) {
					if (in_array($menuId, $noTrackMenuItems)) {
						foreach ($args['Items'] as $key => $modifierDetails) {
							$optionalResponse[$key]['id'] = $modifierDetails['id'];
							$optionalResponse[$key]['status'] = 'IN_STOCK';
						}
					} else {
						list($optionalStatus,) = self::action_getModifiersStatusWrapper($args, $tempInventoryQuantity);
						$key = 0;
						foreach ($optionalStatus as $id => $status) {
							$optionalResponse[$key]['id'] = $id;
							$optionalResponse[$key]['status'] = $status;
							$key++;
						}
					}
					$response['optional_modifier_id'] = $optionalResponse;
				}
			}
		}

		return $response;
	}

	protected function action_getVendorItemsByVendorAndInventoryID($args){
		$inventoryID = $args['Items']['inventory_id'];
		$vendorID = $args['Items']['vendor_id'];
		$rowResults = InventoryQueries::getVendorItemsByVendorAndInventoryID($inventoryID, $vendorID);
		return $rowResults;
	}

	/**
	 * Get all menu items id, forced modifier id and optional modifier id which belongs to given category ids.
	 * @param Input:- array('Items' => array(0 => array('id'=> 38), 1 => array('id'=> 39))
	 * Output:- array('menu_item'=>array(0=>array('id'->783) 1=>array('id'=>746), 'forced_modifier'=>array(0=>array('id'->283) 1=>array('id'=>146), 'optional_modifier'=>array(0=>array('id'->233) 1=>array('id'=>246))
	 */
	protected function action_getMenuByCategory($categoryIds)
	{
		foreach ($categoryIds['Items'] as $values) {
			$idsArr[] =  $values['id'];
		}

		$response = array();
		$redisController = new InventoryRedisController();
		$cacheResponse = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getCategoryMenus($idsArr));
		$dbCacheIds = array_diff_key(array_flip($idsArr), $cacheResponse);
		$forcedModifierDBResponse = array();
		$optionalModifierDBResponse = array();
		$dbResponse = array();
		if (!empty($dbCacheIds)) {
			$idsArr = array_flip($dbCacheIds);
			$selColumns = array('id', 'category_id', 'modifier_list_id', 'forced_modifier_group_id');
			$menuItemResponse = CQ::getNonDeletedRowsWhereColumnIn('menu_items', 'category_id', $idsArr, $selColumns);
			$optionalModifiersCategoryIds = array();
			$commonForcedModifierCategory = array();
			$forcedModifierCategoryIds = array();
			$forcedModifierListIds = array();
			foreach ($menuItemResponse as $key => $details) {
				$dbResponse[$details['category_id']]['menu_item'][$key] = $details['id'];
				if ($details['modifier_list_id'] > 0) {
					$optionalModifiersCategoryIds[$details['category_id']][] = $details['modifier_list_id'];
				}

				if ($details['forced_modifier_group_id'] != 'C') {
					if (is_numeric($details['forced_modifier_group_id']) ) {
						$forcedModifierCategoryIds[$details['category_id']][] = $details['forced_modifier_group_id'];
					} else {
						$forcedModifierListIds[$details['category_id']][] = substr($details['forced_modifier_group_id'], 1);
					}
				} else {
					$commonForcedModifierCategory[$details['category_id']] = $details['category_id'];
				}
			}

			if (!empty($commonForcedModifierCategory)) {
				$selColumns = array('id', 'modifier_list_id', 'forced_modifier_group_id');
				$menuCategoriesResponse = CQ::getNonDeletedRowsWhereColumnIn('menu_categories', 'id', $idsArr, $selColumns);
				foreach ($menuCategoriesResponse as $categoryDetails) {
					if ($categoryDetails['modifier_list_id'] > 0) {
						$optionalModifiersCategoryIds[$categoryDetails['id']][] = $categoryDetails['modifier_list_id'];
					}
					if ($categoryDetails['forced_modifier_group_id'] != '0') {
						if (is_numeric($categoryDetails['forced_modifier_group_id']) ) {
							$forcedModifierCategoryIds[$categoryDetails['id']][] = $categoryDetails['forced_modifier_group_id'];
						} else {
							$forcedModifierListIds[$categoryDetails['id']][] = substr($categoryDetails['forced_modifier_group_id'], 1);
						}
					}
				}
			}

			foreach ($idsArr as $catId) {
				if (!empty($forcedModifierCategoryIds[$catId])) {
					$selColumns = array('id', 'include_lists');
					$forcedModifierListResponse = CQ::getNonDeletedRowsWhereColumnIn('forced_modifier_groups', 'id', $forcedModifierCategoryIds[$catId], $selColumns);
					foreach ($forcedModifierListResponse as $listIds) {
						if ($listIds['include_lists'] != '') {
							$listIdsArr = explode('|', $listIds['include_lists']);
							$forcedModifierListIds[$catId] = array_merge($forcedModifierListIds[$catId], $listIdsArr);
						}
					}
				}
			}

			foreach ($idsArr as $catId) {
				if (!empty($optionalModifiersCategoryIds[$catId])) {
					$optionalModifiersCategoryIds[$catId] = array_unique($optionalModifiersCategoryIds[$catId]);
					$selColumns = array('id');
					$optionalModifierDBResponse[$catId] = CQ::getNonDeletedRowsWhereColumnIn('modifiers', 'category', $optionalModifiersCategoryIds[$catId], $selColumns);
				}
			}

			foreach ($idsArr as $catId) {
				if (!empty($optionalModifierDBResponse[$catId])) {
					foreach ($optionalModifierDBResponse[$catId] as $key => $opModifier) {
						$dbResponse[$catId]['optional_modifier'][$key] = $opModifier['id'];
					}
				}
			}

			foreach ($idsArr as $catId) {
				if (!empty($forcedModifierListIds[$catId])) {
					$forcedModifierListIds[$catId] = array_unique($forcedModifierListIds[$catId]);
					$selColumns = array('id');
					$forcedModifierDBResponse[$catId] = CQ::getNonDeletedRowsWhereColumnIn('forced_modifiers', 'list_id', $forcedModifierListIds[$catId], $selColumns);
				}
			}

			foreach ($idsArr as $catId) {
				if (!empty($forcedModifierDBResponse[$catId])) {
					foreach ($forcedModifierDBResponse[$catId] as $key => $foModifier) {
						$dbResponse[$catId]['forced_modifier'][$key] = $foModifier['id'];
					}
				}
			}
		}
		if (!empty($dbResponse)) {
			$redisController->setCategoryMenus($dbResponse);
		}
		if(is_array($cacheResponse) && is_array($dbResponse)){
			$response = $cacheResponse +  $dbResponse;	
		}
		$menuItemResponse = array();
		$forcedModifierResponse = array();
		$optionalModifierResponse = array();
		foreach ($response as $details) {
			if (isset($details['menu_item'])) {
				$menuItemResponse = array_merge($menuItemResponse, $details['menu_item']);
			}
			if (isset($details['forced_modifier'])) {
				$forcedModifierResponse = array_merge($forcedModifierResponse, $details['forced_modifier']);
			}
			if (isset($details['optional_modifier'])) {
				$optionalModifierResponse = array_merge($optionalModifierResponse, $details['optional_modifier']);
			}
		}
		$finalResponse['menu_item'] = array_unique($menuItemResponse);
		$finalResponse['forced_modifier'] = array_unique($forcedModifierResponse);
		$finalResponse['optional_modifier'] = array_unique($optionalModifierResponse);
		return $finalResponse;
	}

	/**
	 * Delete and make db and csv backup of inventory_items, inventory_audit, menuitem_86, forced_modifier_86
	 * modifier_86 tables
	 */
	protected function action_deleteAllInventory()
	{
		if (admin_info("lavu_admin") ) {
			$zipname = $this->m_dataname . '_inventory.zip';
			$zipFilePath = '/tmp/' . $zipname;
			$tableName = 'inventory_items';
			$dataRows = CQ::getAllRows($tableName);
			if (!empty($dataRows)) {
				$csvHeaders = array_keys($dataRows[0]);
				$tableColumn = $csvHeaders;
				$tableColumn[0] = 'inventoryItemID';
				self::deleteAndBackupData($dataRows, $tableName, $csvHeaders, $tableColumn, true, true, true, $zipFilePath);
			}

			$tableName = 'inventory_audit';
			$dataRows = array();
			$csvHeaders = array();
			$tableColumn = array();
			self::deleteAndBackupData($dataRows, $tableName, $csvHeaders, $tableColumn, true, true, true, $zipFilePath);

			$tableName = 'menuitem_86';
			$dataRows = array();
			$csvHeaders = array();
			$tableColumn = array();
			self::deleteAndBackupData($dataRows, $tableName, $csvHeaders, $tableColumn, true, true, true, $zipFilePath);

			$tableName = 'forced_modifier_86';
			$dataRows = array();
			$csvHeaders = array();
			$tableColumn = array();
			self::deleteAndBackupData($dataRows, $tableName, $csvHeaders, $tableColumn, true, true, true, $zipFilePath);

			$tableName = 'modifier_86';
			$dataRows = array();
			$csvHeaders = array();
			$tableColumn = array();
			self::deleteAndBackupData($dataRows, $tableName, $csvHeaders, $tableColumn, true, true, true, $zipFilePath);
			self::downloadZip($zipname, $zipFilePath);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $dataRows Records of table
	 * @param $tableName Table name
	 * @param array $csvHeaders Header name for csv file header
	 * @param array $tableFields Table column name
	 * @param bool $csvBackup for csv backup file
	 * @param bool $dbBackup for database backup
	 * @param bool $deleteTable delete table after backup
	 * @param $zipFilePath temporary zip file path
	 * @return bool|null true | false
	 */
	protected function deleteAndBackupData($dataRows, $tableName, $csvHeaders = array(), $tableFields = array(), $csvBackup = false, $dbBackup = false, $deleteTable = false, $zipFilePath)
	{
		$dataRows = (!empty($dataRows)) ? $dataRows : CQ::getAllRows($tableName);
		if (!empty($dataRows)) {
			$dataDBArrays = array();
			$dataCSVStr = (!empty($csvHeaders)) ? implode(',', $csvHeaders) . "\n" : implode(',', array_keys($dataRows[0])) . "\n";
			$tableFields = (!empty($tableFields)) ? $tableFields : array_keys($dataRows[0]);
			$tableFields[] = 'backup_by';
			foreach ($dataRows as $details) {
				$dataStr = implode(',', $details);
				$dataCSVStr .= $dataStr . "\n";
				$details['backup_by'] = $_SESSION['posadmin_loggedin'];
				$dataDBArrays[] = $details;
			}
			if ($dbBackup) {
				$backupTableName = $tableName . '_backup';
				$dbResponse = CQ::insertArray($backupTableName, $tableFields, $dataDBArrays);
			}
			if ($csvBackup) {
				$filename = $tableName . '.csv';
				$zip = new ZipArchive;
				$zip->open($zipFilePath, ZipArchive::CREATE);
				$zipReponse = $zip->addFromString( $filename, $dataCSVStr );
				$zip->close();
			}

			if ($deleteTable && $zipReponse && $dbResponse) {
				CQ::deleteRowInTable($tableName);
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $zipname zip file name
	 * @param $zipFilePath zip file name with path
	 */
	protected function downloadZip($zipname, $zipFilePath)
	{
		header('Content-Type: application/zip');
		header('Content-disposition: attachment; filename='.$zipname);
		header('Content-Length: ' . filesize($zipFilePath));
		readfile($zipFilePath);
		@unlink($zipFilePath);
	}


}