<?php

//Class of static methods to provide abstraction to query calls.
//All methods should be static, and return the data if possible instead of mysqli_result.
class InventoryQueries
{
	public static $recordLimit = 200;

	/**For Costing, we need inventoryItemID, unitCost, date, and quantity*/
	public static function getPurchaseOrderCostData()
	{
		$query_string = "
			SELECT 
				`purchaseorder_items`.`quantityReceived` AS `quantity`,
				`purchaseorder_items`.`purchaseUnitCost` AS `unitCost`,
				`inventory_purchaseorder`.`receiveDate` AS `date`,
				`vendor_items`.`inventoryItemID` AS `inventoryItemID`,
				'purchaseOrder' AS `receivedAs`
			
			FROM `purchaseorder_items`
				LEFT JOIN `inventory_purchaseorder` ON `purchaseorder_items`.`purchaseOrderID` = `inventory_purchaseorder`.`id`
				LEFT JOIN `vendor_items` ON `purchaseorder_items`.`vendorItemID` = `vendor_items`.`id`
			  
			WHERE
					`purchaseorder_items`.`_deleted` = 0
				AND `inventory_purchaseorder`.`_deleted` = 0
				AND `purchaseorder_items`.`quantityReceived` > 0
				AND `inventory_purchaseorder`.`receiveDate` != '0000-00-00 00:00:00'
			  
			ORDER BY `inventory_purchaseorder`.`receiveDate` DESC
			";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getExternalInventoryItems(){
		$query_string = "SELECT * FROM `inventory_items`";
		$result = DBConnection::transferLocationQuery($query_string);
		return $result;
	}

	/**For Costing, we need inventoryItemID, unitCost, date, and quantity*/
	public static function getTransferCostData()
	{
		$query_string = "
			SELECT
				`transfer_items`.`quantityReceived` AS `quantity`,
				`transfer_items`.`transferUnitCost` AS `unitCost`,
				`inventory_transfer`.`lastAction` AS `date`,
				`transfer_items`.`inventoryItemID` AS `inventoryItemID`,
				'transfer' AS `receivedAs`
				
			FROM `transfer_items`
				LEFT JOIN `inventory_transfer` ON `transfer_items`.`transferID` = `inventory_transfer`.`id` 
		    
		    WHERE
		        `transfer_items`.`_deleted` = 0
		    AND `inventory_transfer`.`_deleted` = 0
		    AND `transfer_items`.`quantityReceived` > 0
		    AND `inventory_transfer`.`lastAction` != '0000-00-00 00:00:00'
		    
		    ORDER BY `inventory_transfer`.`lastAction` DESC
		";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	/**For Costing, we need inventoryItemID, unitCost, date, and quantity*/
	public static function getReceiveWithoutPOCostData()
	{
		$query_string = "
			SELECT 
				`inventory_audit`.`quantity` AS `quantity`,
				`inventory_audit`.`cost` AS `unitCost`,
				`inventory_audit`.`datetime` AS `date`,
				`inventory_audit`.`inventoryItemID` AS `inventoryItemID`,
				'withoutPurchaseOrder' AS `receivedAs`
			FROM `inventory_audit`
			
			WHERE `inventory_audit`.`action` = 'receiveWithoutPO'
				AND `_deleted` = 0
				AND `inventory_audit`.`datetime` != '0000-00-00 00:00:00'
				AND `inventory_audit`.`quantity` > 0
				
			ORDER BY `inventory_audit`.`datetime` DESC
		";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	/**For Costing, we need inventoryItemID, unitCost, date, and quantity*/
	public static function getCreateInventoryItemCostData()
	{
		$query_string = "
			SELECT 
				`inventory_audit`.`quantity` AS `quantity`,
				`inventory_audit`.`cost` AS `unitCost`,
				`inventory_audit`.`datetime` AS `date`,
				`inventory_audit`.`inventoryItemID` AS `inventoryItemID`,
				'createInventoryItem' AS `receivedAs`
			FROM `inventory_audit`
			
			WHERE `inventory_audit`.`action` = 'createInventoryItem'
				AND `_deleted` = 0
				AND `inventory_audit`.`datetime` != '0000-00-00 00:00:00'
				AND `inventory_audit`.`quantity` > 0
				
			ORDER BY `inventory_audit`.`datetime` DESC
		";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

    public static function getCreateInventoryItemCostDataByID($id)
    {
        $query_string = "
			SELECT 
				`inventory_audit`.`quantity` AS `quantity`,
				`inventory_audit`.`cost` AS `unitCost`,
				`inventory_audit`.`unitID` AS `unitID`,
				`inventory_audit`.`datetime` AS `receiveDate`
			FROM `inventory_audit`
			
			WHERE `inventory_audit`.`action` = 'createInventoryItem'
				AND `_deleted` = 0
				AND `inventory_audit`.`datetime` != '0000-00-00 00:00:00'
				AND `inventory_audit`.`inventoryItemID` = $id
				
			ORDER BY `inventory_audit`.`datetime` DESC
		";
        $result = DBConnection::clientQuery($query_string);
        return MarshallingArrays::resultToArray($result);
    }

	/**For Costing, we need inventoryItemID, unitCost, date, and quantity*/
	public static function getWasteCostData()
	{
		$query_string = "
			SELECT 
				`inventory_audit`.`quantity` AS `quantity`,
				`inventory_audit`.`cost` AS `unitCost`,
				`inventory_audit`.`datetime` AS `date`,
				`inventory_audit`.`inventoryItemID` AS `inventoryItemID`,
				'waste' AS `receivedAs`
			FROM `inventory_audit`
			
			WHERE `inventory_audit`.`action` = 'waste' OR `inventory_audit`.`action` = 'waste_overage'
				AND `_deleted` = 0
				AND `inventory_audit`.`datetime` != '0000-00-00 00:00:00'
				AND `inventory_audit`.`quantity` > 0
				
			ORDER BY `inventory_audit`.`datetime` DESC
		";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getCostingMethod()
	{
		$query_string = "SELECT * FROM `inventory_settings` WHERE `setting` = 'inventory_costing_method'";
		$result = DBConnection::clientQuery($query_string);
		$result = MarshallingArrays::resultToArray($result);
		if ($result && $result[0])
		{
			return $result[0];
		} else
		{
			return "error: mysql-failure";
		}
	}


	public static function insertInventoryItems($insertRows){
		require_once(__DIR__."/../../../../admin/cp/resources/core_functions.php");
		$firstRowID = null;
		$count = 0;
		$sUOMS = self::getAllUnitIDsConversions();
		for($i = 0; $i < count($insertRows); $i++)
		{
			$quantityOnHand = $insertRows[$i]['quantityOnHand'];
			$conversion = self::getUnitConversion($insertRows[$i]['salesUnitID'], $insertRows[$i]['recentPurchaseUnitID'], $sUOMS);
			$purchaseQuantity = $insertRows[$i]['quantityOnHand'] * $conversion;
			$totalCost = $insertRows[$i]['recentPurchaseCost'] * $purchaseQuantity;
			$costPerSalesUnit = $totalCost / $insertRows[$i]['quantityOnHand'];

			if ($insertRows[$i]['salesUnitID'] != $insertRows[$i]['recentPurchaseUnitID']) {

				$conversion = self::getUnitConversion($insertRows[$i]['recentPurchaseUnitID'], $insertRows[$i]['salesUnitID'], $sUOMS);
				if (!empty($conversion)) {
					$insertRows[$i]['quantityOnHand'] = $insertRows[$i]['quantityOnHand'] * $conversion;
				}
			}
			self::insertSingleInventoryItem($insertRows[$i]);
			$id = DBConnection::clientInsertID();

			if (!isset($firstRowID))
			{
				$firstRowID = $id;
			}

			$auditRow = array(
				"inventoryItemID"=>$id,
				"quantity"=>$quantityOnHand,
				"unitID"=>$insertRows[$i]['recentPurchaseUnitID'],
				"cost"=>$insertRows[$i]['recentPurchaseCost'],
				"userID"=>(int)sessvar("admin_loggedin"),
				"action"=>"createInventoryItem",
				"datetime"=>gmdate("Y-m-d H:i:s"),
				"userNotes"=>"Create Inventory Item"
			);
			InventoryQueries::insertAudit($auditRow);
			$inventoryCache[$id]['quantityOnHand'] = $insertRows[$i]['quantityOnHand'];
			$inventoryCache[$id]['reserve'] = 0;
			$inventoryCache[$id]['salesUnitID'] = $insertRows[$i]['salesUnitID'];
			$inventoryCache[$id]['lowQuantity'] = $insertRows[$i]['lowQuantity'];
			$inventoryCache[$id]['reOrderQuantity'] = $insertRows[$i]['reOrderQuantity'];
			$count += 1;
		}
		return array("InsertID" => $firstRowID, "RowsUpdated" => $count, "Inventory" => $inventoryCache);
	}

	public static function insertSingleInventoryItem($row){
		$result = CommonQueries::insertArray('inventory_items',array_keys($row),array($row));
		return $result;
	}

	//Insert functions
	public static function insertInventoryItem($row)
	{
		$query_string = "INSERT INTO `inventory_items` (`name`,`categoryID`,`unitID`,`quantity`,`lowQuantity`,`reOrderQuantity`,`critical_item`,`_deleted`, `recentPurchaseCost`)VALUES('[1]',[2],[3],[4],[5],[6],[7],[8],[9])";
		$result = DBConnection::clientQuery($query_string, $row['name'], $row['categoryID'], $row['unitID'], $row['quantity'], $row['lowQuantity'],
			$row['reOrderQuantity'], $row['critical_item'], $row['_deleted'], $row['recentPurchaseCost']);
		if ($result)
		{
			return "success";
		} else
		{//TODO What else to do/return on failure?
			return "error: mysql-failure";
		}
	}

	public static function insertPurchaseOrders($row)
	{
		foreach (array('invoiceNumber', 'vendorID', 'order_date', 'receive_date', 'sent', 'accepted', '_deleted') as $field)
		{
			if (!isset($row[$field])) $row[$field] = '';
		}
		$query_string = "INSERT INTO `inventory_purchaseorder` (`invoiceNumber`,`vendorID`,`order_date`,`receive_date`,`sent`,`accepted`,`_deleted`)VALUES([1],[2],[3],[4],[5],[6],[7])";
		$result = DBConnection::clientQuery($query_string, $row['invoiceNumber'], $row['vendorID'], $row['order_date'], $row['receive_date'], $row['sent'], $row['accepted'], $row['_deleted']);
		if ($result)
		{
			return "success";//
		} else
		{//TODO What else to do/return on failure?
			return "error: mysql-failure";
		}
	}

	public static function insertUnitCategories($rows)
	{
		$result = false;
		if (is_array($rows[0]))
		{
			$result = CQ::insertArray('unit_category', array_keys($rows[0]), $rows);
		}
		if ($result)
		{
			return "success";
		}

		//TODO What else to do/return on failure?
		return "error: mysql-failure";

	}

	public static function insertAudit($row)
	{
		$timeZone = sessvar('location_info')['timezone'];

		$dateTimeStr = DateTimeForTimeZone($timeZone);
		if (isset($row['datetime']) && $dateTimeStr != "") {
			$row['datetime'] = $dateTimeStr;
		}

		$result = CQ::insertArray('inventory_audit', array_keys($row), array($row));
		if ($result)
		{
			return "success";
		} else
		{//TODO What else to do/return on failure?
			return "error: mysql-failure";
		}
	}

	public static function insertPurchaseOrdersItems($row)
	{
		$query_string = "INSERT INTO `purchaseorder_items` (`purcahseOrderID`,`vendorItemID`,`purchaseUnitID`,`quantityOrdered`,`purchaseUnitCost`,`quantityReceived`,`SKU`,`_delete`)VALUES([1],[2],[3],[4],[5],[6],[7])";
		$result = DBConnection::clientQuery($query_string, $row['purcahseOrderID'], $row['vendorItemID'], $row['purchaseUnitID'], $row['quantityOrdered'], $row['purchaseUnitCost'], $row['quantityReceived'], $row['SKU'], $row['_deleted']);
		if ($result)
		{
			return "success";//
		} else
		{//TODO What else to do/return on failure?
			return "error: mysql-failure";
		}
	}

	// Util methods for monetary methods

	public static function getLocation()
	{
		$locations = CommonQueries::getRowsWhereColumnIn('locations', '_disabled', array('0'));
		$location = array_shift($locations);  // null if no locations

		return $location;
	}

	public static function getLocationField($location = false, $field)
	{
		if ($location === false)
		{
			$location = self::getLocation();
		}

		return isset($location[$field]) ? $location[$field] : null;
	}

	// Monetary methods

	public static function getMonetarySymbol($location = false)
	{
		return self::getLocationField($location, 'monitary_symbol');
	}

	public static function getMonetarySymbolAlignment($location = false)
	{
		return self::getLocationField($location, 'left_or_right');
	}

	public static function getMonetaryDecimalPrecision($location = false)
	{
		return self::getLocationField($location, 'disable_decimal');
	}

	public static function getMonetaryDecimalSeparator($location = false)
	{
		return self::getLocationField($location, 'decimal_char');
	}

	public static function getMonetaryThousandsSeparator($location = false)
	{
		$decimal_separator = self::getMonetaryDecimalSeparator();
		$thousands_separator = ($decimal_separator == ',') ? '.' : ',';
		return $thousands_separator;
	}

	public static function action_getMonetaryInformation($location = false)
	{
		if ($location === false)
		{
			$location = self::getLocation();
		}

		$monetary_info = array(
			'decimal_precision' => self::getMonetaryDecimalPrecision($location),
			'decimal_separator' => self::getMonetaryDecimalSeparator($location),
			'thousands_separator' => self::getMonetaryThousandsSeparator($location),
			'monetary_symbol' => self::getMonetarySymbol($location),
			'monetary_symbol_alignment' => self::getMonetarySymbolAlignment($location),
		);

		return $monetary_info;
	}

	public static function getAllSalesUnitsOfMeasure()
	{
		$query_string = "SELECT `iu`.* FROM `inventory_unit` iu
					WHERE `categoryID` != 3
					AND `iu`.`id` IN (SELECT DISTINCT `salesUnitID` FROM `inventory_items` WHERE `_deleted` = 0)";
		$standardUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query_string));
		$standardIDs = array();
		foreach($standardUnits as $i=>$unit){
			$standardIDs[] = $unit['unitID'];
		}

		$sUOMS = self::getStandardUnitsOfMeasure($standardIDs);

		$query_string = "SELECT `cUOM`.*, `iu`.*  
			FROM `inventory_unit` iu
				LEFT JOIN `custom_units_measure` cUOM
					ON `cUOM`.`id` = `iu`.`unitID`
					WHERE `iu`.`categoryID` = 3
					AND `iu`.`id` IN (SELECT DISTINCT `salesUnitID` FROM `inventory_items` WHERE `_deleted` = 0)";
		$customUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query_string));

		$result = array_merge($sUOMS, $customUnits);
		return $result;
	}

	public static function getCustomUnitsOfMeasure(){
		$query_string = "SELECT `cUOM`.*, `iu`.*  
			FROM `inventory_unit` iu
				LEFT JOIN `custom_units_measure` cUOM
					ON `cUOM`.`id` = `iu`.`unitID`
					WHERE `iu`.`categoryID` = 3";
		$customUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query_string));
		return $customUnits;
	}

	private static function getStandardUnitsOfMeasure($idArray){
        $idArray[] = 16; //Expecting 16 will be always for 'each' in standard unit measure table
		if(is_array($idArray) && count($idArray)>0){
			$whereClause = " WHERE `id` IN (" . implode($idArray, ',').")";
			$query = "SELECT * FROM `poslavu_MAIN_db`.`standard_units_measure`".$whereClause;
			return MarshallingArrays::resultToArray(DBConnection::adminQuery($query));
		}else{
			return array();
		}
	}

	public static function getAllStandardUnitsOfMeasure(){
		$query = "SELECT * FROM `poslavu_MAIN_db`.`standard_units_measure`";
		return MarshallingArrays::resultToArray(DBConnection::adminQuery($query));
	}

	public static function getAllPurchaseUnitsOfMeasure()
	{
		$query_string = "SELECT `iu`.* FROM `inventory_unit` iu
					WHERE `categoryID` != 3
					AND `iu`.`id` IN (SELECT DISTINCT `recentPurchaseUnitID` FROM `inventory_items` WHERE `_deleted` = 0)";
		$standardUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query_string));
		$standardIDs = array();
		foreach($standardUnits as $i=>$unit){
			$standardIDs[] = $unit['unitID'];
		}
		$sUOMS = self::getStandardUnitsOfMeasure($standardIDs);

		$query_string = "SELECT `cUOM`.*, `iu`.*
			FROM `inventory_unit` iu
				LEFT JOIN `custom_units_measure` cUOM
					ON `cUOM`.`id` = `iu`.`unitID`
					WHERE `iu`.`categoryID` = 3
					AND `iu`.`id` IN (SELECT DISTINCT `recentPurchaseUnitID` FROM `inventory_items` WHERE `_deleted` = 0)";
		$customUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query_string));

		$result = array_merge($sUOMS, $customUnits);
		return $result;
	}

	/**
	 * Description:- Get all active categories of items
	 * Input:- null
	 * Output:- json string
	 */
	public static function getAllCategories()
	{
		$query_string = "SELECT *

			FROM `inventory_categories`

			WHERE
			`inventory_categories`.`_deleted` = 0
			";

		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getAllCategoriesWithArchives()
	{
		$query_string = "SELECT * FROM `inventory_categories`";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	#===============================================#
	# Function Name : getAllCount			        #	
	# Author        : Dhruvpalsinh D Chudasama      #
	# Purpose       : To get count of all tables    #
	# Date          : 30-01-2019                    #
	# Updated       : 14-02-2019                    # 
	#===============================================#
	public static function getAllCount($tablename,$deleted)
	{
		$query_string = "SELECT count(*) as count 		
			FROM $tablename
			WHERE `_deleted` = $deleted";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);	
	}
	/**
	 * Description:- count transfer count
	 * Input:- $params
	 * Output:- return total transaction
	 * Issue: LP-10195
	 */
	public static function getAllTransfersCount($params = array())
	{
		if (!empty($params) && isset($params['Items'])) {
			$params = $params['Items'];
		}

		$where = "";
		$subqueryWhere = "";
		#Transaction Stages search filter options
		if(isset($params['stage']) && $params['stage'] != ''){
			$tranferStage = explode(",",$params['stage']);
			$accepted = array();
			if(in_array('accepted', $tranferStage)){
				$accepted[] = 1;
			}
			if(in_array('declined', $tranferStage)){
				$accepted[] = 0;
			}
			#if stage filter is selected as accepted or decliend or both
			if(!empty($accepted)){
				$where.=" (`inventory_transfer`.`accepted` IN(".$accepted.")";
			}
			#if stage filter is selected as sent
			if(in_array('sent', $tranferStage)){
				$where.=" OR `inventory_transfer`.`sent` = 1";
			}
			#if stage filter is selected as pending
			if(in_array('pending', $tranferStage)){
				$where.=" OR (`inventory_transfer`.`sent` = 0 AND `inventory_transfer`.`received` = 0 AND `inventory_transfer`.`accepted` = 0)";
			}
			#if stage filter is selected as partial
			if(in_array('partial', $tranferStage)){
				$subqueryWhere.=" AND (`transfer_items`.`quantityReceived` <  `transfer_items`.`quantityTransfered`";
			}
			#if stage filter is selected as closed
			if(in_array('closed', $tranferStage)){
				$subqueryWhere.=" OR `transfer_items`.`quantityReceived` >= `transfer_items`.`quantityTransfered`";
			}
			if($subqueryWhere != ""){
				$subqueryWhere.=")";
			}
			if($where == ""){
				$where = " AND ".$where.")";
			}	
		}
		#Location involved Stages search filter options
		if(isset($params['originRestaurantID']) && $params['originRestaurantID'] != ''){
			$where.=" AND (`inventory_transfer`.`originRestaurantID` IN(".$params['originRestaurantID'].") OR `inventory_transfer`.`destinationRestaurantID` IN(".$params['originRestaurantID']."))";
		}
		#For transfer search filter
		if(isset($params['transferNumber']) && $params['transferNumber'] != ''){
			$where.=" AND `inventory_transfer`.`transferNumber` LIKE '%".$params['transferNumber']."%'";
		}
		
		$query_string = "SELECT COUNT(*) AS transferCount, (SELECT COUNT(*) FROM `transfer_items` WHERE `inventory_transfer`.`id` = `transfer_items`.`transferID` $subqueryWhere) 
			AS `transferItemsCount`
			FROM `inventory_transfer`
			WHERE `inventory_transfer`.`_deleted` = 0 $where ";

		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	/**
	 * Description:- Get all active Transfers
	 * Input:- null
	 * Output:- json string
	 */
	public static function getAllTransfers($params = array())
	{	
		if(!empty($params)){
			$params = $params['Items'];
		}
		$limitStr = '';
		if(isset($params['limit']) && $params['limit'] != '') {
			$limit = $params['limit'];
			$limitStr = " LIMIT ".$limit;
		} 
		$where = "";
		$subqueryWhere = "";
		#Transaction Stages search filter options
		if(isset($params['stage']) && $params['stage'] != ''){
			$tranferStage = explode(",",$params['stage']);
			$accepted = array();
			if(in_array('accepted', $tranferStage)){
				$accepted[] = 1;
			}
			if(in_array('declined', $tranferStage)){
				$accepted[] = 0;
			}
			#if stage filter is selected as accepted or decliend or both
			if(!empty($accepted)){
				$where.=" (`inventory_transfer`.`accepted` IN(".$accepted.")";
			}
			#if stage filter is selected as sent
			if(in_array('sent', $tranferStage)){
				$where.=" OR `inventory_transfer`.`sent` = 1";
			}
			#if stage filter is selected as pending
			if(in_array('pending', $tranferStage)){
				$where.=" AND (`inventory_transfer`.`sent` = 0 AND `inventory_transfer`.`received` = 0 AND `inventory_transfer`.`accepted` = -1)";
			}
			#if stage filter is selected as partial
			if(in_array('partial', $tranferStage)){
				$subqueryWhere.=" AND (`transfer_items`.`quantityReceived` <  `transfer_items`.`quantityTransfered`";
			}
			#if stage filter is selected as closed
			if(in_array('closed', $tranferStage)){
				$subqueryWhere.=" OR `transfer_items`.`quantityReceived` >= `transfer_items`.`quantityTransfered`";
			}
			if($subqueryWhere != ""){
				$subqueryWhere.=")";
			}
			if($where == ""){
				$where = " AND ".$where.")";
			}	
		}
		#Location involved Stages search filter options
		if(isset($params['originRestaurantID']) && $params['originRestaurantID'] != ''){
			$where.=" AND (`inventory_transfer`.`originRestaurantID` IN(".$params['originRestaurantID'].") OR `inventory_transfer`.`destinationRestaurantID` IN(".$params['originRestaurantID']."))";
		}
		#For transfer search filter
		if(isset($params['transferNumber']) && $params['transferNumber'] != ''){
			$where.=" AND `inventory_transfer`.`transferNumber` LIKE '%".$params['transferNumber']."%'";
		}
		
		$query_string = "SELECT *, (SELECT COUNT(*) FROM `transfer_items` WHERE `inventory_transfer`.`id` = `transfer_items`.`transferID` $subqueryWhere) 
			AS `transferItemsCount`
			FROM `inventory_transfer`
			WHERE `inventory_transfer`.`_deleted` = 0 $where ";

		$order_by = $params['order_by'] ? $params['order_by'] : "ASC";
		$sort_by = $params['sort_by'] ? $params['sort_by'] : "transferNumber";

		#if sort by field is except location/stage
		$excludeSortingFields = array("location","transfer_stage");
		if(isset($sort_by) &&  !in_array($sort_by,$excludeSortingFields)){
			$query_string.=" ORDER BY `inventory_transfer`.$sort_by $order_by $limitStr";	
		}
		
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getTransferByID($id)
	{
		$query_string = "SELECT * FROM `inventory_transfer` WHERE `id` = {$id}";
		$result = DBConnection::clientQuery($query_string);
		$result = MarshallingArrays::resultToArray($result);
		$result = $result[0];
		return $result;
	}

	public static function getTransferItemByID($id)
	{
		$query_string = "SELECT * FROM `transfer_items` WHERE `id` = {$id}";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getTransferItemsByTransferID($id)
	{
		$query_string = "SELECT * FROM `transfer_items` WHERE `transferID` = {$id}";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getTransferItemByTransferIDAndInventoryItemID($args)
	{
		// The quantity comparison is to help us get TransferItems that haven't been fully received (in case there are somehow multiple TransferItems for the same InventoryItemID in one Transfer)
		$query_string = "SELECT * FROM `transfer_items` WHERE `transferID` = [transferID] AND `inventoryItemID` = [inventoryItemID] AND `quantityReceived` != `quantityTransferred`";
		$result = DBConnection::clientQuery($query_string, $args);
		return MarshallingArrays::resultToArray($result);
	}

	/**
	 * Description:- Get all pending Transfers
	 * Input:- null
	 * Output:- json string
	 */
	public static function getAllPendingTransfers()
	{
		$query_string = "SELECT *
			FROM `inventory_transfer`
			WHERE
				`inventory_transfer`.`_deleted` = 0
			AND `inventory_transfer`.`received` = 0
			AND `inventory_transfer`.`sent` = 0
			";

		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	/**
	 * Description:- Get all received Transfers
	 * Input:- null
	 * Output:- json string
	 */
	public static function getAllReceivedTransfers()
	{
		$query_string = "SELECT *

			FROM `inventory_transfer`

			WHERE
				`inventory_transfer`.`_deleted` = 0
			AND
				`inventory_transfer`.`received` = 1
			";

		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	/**
	 * Description:- Get all Stocks id of items
	 * Input:- null
	 * Output:- json string
	 */
	public static function getInventoryItemCounts()
	{
		$query_string = "SELECT
			concat(51 * floor(`inventory_items`.`quantityOnHand` / 51), '-', 51 * floor(`inventory_items`.`quantityOnHand` / 51) + 50)	AS `name`,
			`inventory_items`.`id`          																				AS `id`

			FROM `inventory_items`

			WHERE
			`inventory_items`.`_deleted` = 0
			GROUP BY 1
			ORDER BY `inventory_items`.`quantityOnHand`;
			";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	/**
	 * Description:- Get all Vendors
	 * Input:- null
	 * Output:- json string
	 */
	public static function getAllVendors($paged = array())
	{
		$limitStr = '';
		if(isset($paged['Items'][0]['id']) && $paged['Items'][0]['id'] != '') {
			$pageId = $paged['Items'][0]['id'];
			$limit = isset($paged['Items'][1]['id']) && $paged['Items'][1]['id'] != '' ? $paged['Items'][1]['id'] : self::$recordLimit;
			$start_from = ($pageId-1) * $limit;	
			$limitStr = " LIMIT ".$start_from. " ,".$limit;
		}
		$query_string = "SELECT * FROM `vendors` WHERE `vendors`.`_deleted` = 0 $limitStr";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getAllVendorsWithArchives()
	{
		$query_string = "SELECT * FROM `vendors`";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	/** Description:- Get all Storage Locations
	 * Input:- null
	 * Output:- json string
	 */
	public static function getAllStorageLocations()
	{
		$query_string = "SELECT *
            FROM `storage_locations`
            WHERE
            `storage_locations`.`_deleted` = 0
            ";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getAllStorageLocationsWithArchives()
	{
		$query_string = "SELECT * FROM `storage_locations`";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getInventoryItemByName($name){
		$query_string = "SELECT * FROM `inventory_items` WHERE `name`='[1]'";
		$result = DBConnection::clientQuery($query_string,$name);
		return $result;
	}

	/**
	 * Description - Get all Inventory Items Count
	 * Input - null
	 * Output JSON object containing all inventory items count
	 * By Dhruvpalsinh Chudasama on 30-01-2019
	 */

	public static function getAllInventoryItemsCount($params=array())
	{
		$query_string = "SELECT count(`inventory_items`.id) as inventoryitemcount from `inventory_items` left join `inventory_categories` on `inventory_items`.`categoryID`=`inventory_categories`.`id`  where `inventory_items`.`_deleted` = 0"; 
		
		if(isset($params['_deleted']) && $params['_deleted'] != ''){
			$query_string.=" AND `inventory_categories`.`_deleted` =  {$params['_deleted']} ";
		}else{
			$query_string.=" AND `inventory_categories`.`_deleted` =  0";
		}

		if(isset($params['name']) && $params['name'] != ''){
			$query_string.=" AND `inventory_items`.`name` LIKE '%{$params['name']}%' ";
		}
		if(isset($params['categoryID']) && $params['categoryID'] != ''){
			$query_string.=" AND `inventory_items`.`categoryID` IN(".$params['categoryID'].")";
		}
		
		if(isset($params['primaryVendorID']) && $params['primaryVendorID'] != ''){
			$query_string.=" AND `inventory_items`.`primaryVendorID` IN(".$params['primaryVendorID'].")";
		}
		
		# min - max stock filters
		$low_stock_min = isset($params['low_stock_min']) ? $params['low_stock_min'] : '';
		$low_stock_max = isset($params['low_stock_max']) ? $params['low_stock_max'] : '';
		
		if(isset($params['stock_unit']) && $params['stock_unit'] != ""){
			$query_string.=" AND `inventory_items`.`recentPurchaseUnitID` = '".$params['stock_unit']."'";
		}


		#value of low_stock will be either 1 or 0
		if(isset($params['low_stock']) && $params['low_stock']){
			$query_string.=" AND (`inventory_items`.`quantityOnHand` <= `inventory_items`.`reOrderQuantity`)";
		 }
		
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	/**
	 * Description - Get all Inventory Items
	 * Input - null
	 * Output JSON object containing all inventory items
	 */
	public static function getAllInventoryItems($params = array())
	{	
		$params = isset($params['Items']) ? $params['Items'] : array();
		
		$limitStr = '';
		$limit = '';
		//Adding join query for getting inventory_items with inventory_categories (jira LP-968)
		if(isset($params['limit']) && $params['limit'] != '') {
			$limit = $params['limit'];	
		} 
		$low_stock_min = isset($params['low_stock_min']) ? $params['low_stock_min'] : 0;
		$low_stock_max = isset($params['low_stock_max']) ? $params['low_stock_max'] : 0;

		if($low_stock_min == 0 && $low_stock_max == 0 && $limit != "" && isset($params['short_by']) && $params['short_by'] != "quantityOnHand"){
			$limitStr = " LIMIT ".$limit;
		}

		//$query_string = "SELECT `inventory_items`.* from `inventory_items` left join `inventory_categories` on `inventory_items`.`categoryID`=`inventory_categories`.`id` where `inventory_items`.`_deleted` = 0 ";

		$query_string = "SELECT `inventory_items`.* , `vendors`.`name` as vendorName, `inventory_categories`.`name` as categoryName, `storage_locations`.`name` as storageLocationName  FROM `inventory_items` 
		LEFT JOIN `inventory_categories` ON `inventory_items`.`categoryID`=`inventory_categories`.`id` 
		LEFT JOIN vendors ON vendors.id = inventory_items.primaryVendorID
		LEFT JOIN storage_locations ON storage_locations.id =  inventory_items.storageLocation WHERE `inventory_items`.`_deleted` = 0 ";

		if(isset($params['name']) && $params['name'] != ''){
			$query_string.=" AND `inventory_items`.`name` LIKE '%{$params['name']}%' ";
		}
		if(isset($params['categoryID']) && $params['categoryID'] != ''){
			$query_string.=" AND `inventory_items`.`categoryID` IN(".$params['categoryID'].")";
		}

		if(isset($params['primaryVendorID']) && $params['primaryVendorID'] != ''){
			$query_string.=" AND `inventory_items`.`primaryVendorID` IN(".$params['primaryVendorID'].")";
		}

		# min - max stock filters
		if(isset($params['stock_unit']) && $params['stock_unit'] != ""){
			$query_string.=" AND `inventory_items`.`recentPurchaseUnitID` = '".$params['stock_unit']."'";
		}
		

		#value of low_stock will be either 1 or 0
		if(isset($params['low_stock']) && $params['low_stock']){
		   $query_string.=" AND (`inventory_items`.`quantityOnHand` <= `inventory_items`.`reOrderQuantity`)";
		}

		$query_string.=" AND `inventory_categories`.`_deleted` = 0 ";

		$order_by = (isset($params['order_by']) && $params['order_by'] != '') ? $params['order_by'] : "ASC";
		$short_by = (isset($params['short_by']) && $params['short_by'] != '') ? $params['short_by'] : "name";

		#if sort by field is except cost/value
		$excludeSortingFields = array("storageLocation","cost","categoryID","primaryVendorID","unitSymbol");
		if(isset($short_by) &&  !in_array($short_by,$excludeSortingFields) ){
			$query_string.=" ORDER BY `inventory_items`.$short_by $order_by $limitStr";	
		}

		//echo json_encode(array("query"=>$query_string));die;
		$result = DBConnection::clientQuery($query_string);
		$inventoryDataSet = MarshallingArrays::resultToArray($result);
		$inventoryData = array();
		$inventoryResultData = array();
		$array_index = 0;
		// If Min or Max or both search filter is selected
		$sUOMS = self::getAllUnitIDsConversions();

		if(isset($params['stock_unit']) && $params['stock_unit'] != "" && ($low_stock_min != 0 || $low_stock_max != 0)){
			$recordCount = count($inventoryDataSet);
			for($x=0;$x<$recordCount;$x++){
				$inventoryData = $inventoryDataSet[$x];
				$quantityOnHand = $inventoryData['quantityOnHand'];
				if($inventoryData['recentPurchaseUnitID'] != $inventoryData['salesUnitID']){
					
					//conversion from  quantity(purchase) to sale quantity to compare in database
					$saleConversion = self::getUnitConversion($inventoryData['recentPurchaseUnitID'] , $inventoryData['salesUnitID'] , $sUOMS);

					//conversion from  quantity(sale) to sale purchase to display 
					$perConversion = self::getUnitConversion($inventoryData['salesUnitID'] , $inventoryData['recentPurchaseUnitID'] , $sUOMS);

					$quantityOnHandMin = $low_stock_min ? strval($low_stock_min * $saleConversion) : 0; #convert filter min quantity(purchase) to sale quantity
					$quantityOnHandMax = $low_stock_max ? strval($low_stock_max * $saleConversion) : 0; #convert filter max quantity(purchase) to sale quantity

					if (!empty($saleConversion)) {
						$searchFlag = 0;
						if(($low_stock_min != 0 && $low_stock_max != 0) || ($low_stock_min == 0 && $low_stock_max != 0)){
							if($quantityOnHand >= $quantityOnHandMin && $quantityOnHand <= $quantityOnHandMax){
								$searchFlag = 1;
							}
						}elseif($low_stock_min != 0 && $low_stock_max == 0){
							if($quantityOnHand >= $quantityOnHandMin){
								$searchFlag = 1;
							}
						}
						if($searchFlag){
							$inventoryDataSet[$x]['perQuantityOnHand'] = ($quantityOnHand*$perConversion);
							$inventoryResultData[$array_index] = $inventoryDataSet[$x];
							$array_index ++;
						}
					}

				}else{
					$searchFlag = 0;
					if(($low_stock_min != 0 && $low_stock_max != 0) || ($low_stock_min == 0 && $low_stock_max != 0)){
						if($quantityOnHand >= $low_stock_min && $quantityOnHand <= $low_stock_max){
							$searchFlag = 1;
						}
					}elseif($low_stock_min != 0 && $low_stock_max == 0){
						if($quantityOnHand >= $low_stock_min){
							$searchFlag = 1;
						}
					}
					if($searchFlag){
						$inventoryDataSet[$x]['perQuantityOnHand'] = $quantityOnHand;
						$inventoryResultData[$array_index] = $inventoryDataSet[$x];
						$array_index ++;
					}	
				}
			}
			return $inventoryResultData;
		}else{
			$recordCount = count($inventoryDataSet);
			for($x=0;$x<$recordCount;$x++){
				$inventoryData = $inventoryDataSet[$x];
				if($inventoryData['recentPurchaseUnitID'] != $inventoryData['salesUnitID']){
					//conversion from  quantity(sale) to sale purchase to display 
					$perConversion = self::getUnitConversion($inventoryData['salesUnitID'] , $inventoryData['recentPurchaseUnitID'] , $sUOMS);
					if (!empty($perConversion)) {
						$inventoryData['perQuantityOnHand'] = ($inventoryData['quantityOnHand'] *$perConversion);
					}
				}else{
					$inventoryData['perQuantityOnHand'] = $inventoryData['quantityOnHand'];
				}
				$inventoryDataSet[$x] = $inventoryData;
			}
			return $inventoryDataSet;
		}
	}

	/**
	 * params: $unitID //purchage unit id
	 * params: $categoryID
	 * This function will call return unit measour symbol
	 * return $inventoryItems
	 */
	public static function getUnitMeasureSymbol($unitID,$categoryID){
		$standardIDs    =       array();
		$standardIDs[] = $unitID;
		$sUOMS = self::getStandardUnitsOfMeasure($standardIDs);
		if(count($sUOMS) == 2){
				return $sUOMS[0];       #unit symbol found in main db standard unit measure table
		}
		$query_string = "SELECT `cUOM`.*, `iu`.*
				FROM `inventory_unit` iu
						LEFT JOIN `custom_units_measure` cUOM
								ON `cUOM`.`id` = `iu`.`unitID`
								WHERE `iu`.`unitID` = '".$unitID."' AND `iu`.`categoryID`='".$categoryID."'
								AND  `iu`.`_deleted` != 1 AND  `cUOM`.`_deleted` != 1";
		$result = MarshallingArrays::resultToArray(DBConnection::clientQuery($query_string));
		return (!empty($result)) ? $result[0] : $result;
	}

	/**
	 * Description - Gets all vendor_item rows for a given item
	 */
	public static function getAllVendorItems()
	{
		$query_string = "SELECT vi.`id`, vi.`inventoryItemID`, vi.`vendorID`, vi.`SKU`,
                          vi.`barcode`, vi.`BIN`, vi.`serialNumber`, ii.`name`, ii.`categoryID`, 
                          ii.`salesUnitID`, ii.`recentPurchaseUnitID`, ii.`recentPurchaseQuantity`, ii.`quantityOnHand`,
                          ii.`recentPurchaseCost`, ii.`lowQuantity`, ii.`reOrderQuantity`,
                          ii.`criticalItem`, ii.`primaryVendorID` from 
                          `vendor_items` vi LEFT JOIN `inventory_items` ii 
                          ON vi.`inventoryItemID` = ii.`id` where vi.`_deleted` = 0";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getPurchaseOrderDetails($id){
	    $purchaseOrderQuery = "SELECT * FROM `inventory_purchaseorder` WHERE `id` = '". $id . "'";
	    $POResult = DBConnection::clientQuery($purchaseOrderQuery);
	    $POResultArray = MarshallingArrays::resultToArray($POResult);
	    $queryString = "SELECT * FROM `purchaseorder_items` where `purchaseOrderID` = '" . $id . "'";
	    $result = DBConnection::clientQuery($queryString);
	    $resultArray = MarshallingArrays::resultToArray($result);
	    foreach($resultArray as &$resultItem) {
            $vendorItemID = $resultItem['vendorItemID'];
            $vendorDetailsQuery = "SELECT ii.`id` as 'inventoryItemID', ii.`name` as 'inventoryItemName' FROM `inventory_items` ii join `vendor_items` vi ON ii.`id` = vi.`inventoryItemID` WHERE vi.`id` = $vendorItemID";
            $vendorDetailResults = DBConnection::clientQuery($vendorDetailsQuery);
            $vendorArray = MarshallingArrays::resultToArray($vendorDetailResults);
            if (isset($vendorArray[0]['inventoryItemID'])) {
                $resultItem['inventoryItemID'] = $vendorArray[0]['inventoryItemID'];
            }
            if (isset($vendorArray[0]['inventoryItemName'])) {
                $resultItem['inventoryItemName'] = $vendorArray[0]['inventoryItemName'];
            }
        }
	    $POResultArray[0]['purchaseOrderItems'] = $resultArray;
	    return $POResultArray;
    }

	public static function getAllUnitsByCategory($id)
	{
		if($id == 3)
		{ // custom units
			$query = "SELECT * FROM `inventory_unit` LEFT JOIN ON `custom_units_measure` WHERE `inventory_unit`.`id` = `custom_units_measure`.`id`";
			$customUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
			return $customUnits;
		}else{
			$query = "SELECT * FROM `inventory_unit` WHERE `categoryID` = {$id}";
			$standardUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
			$standardIDs = array();
			foreach($standardUnits as $i=>$unit){
				$standardIDs[] = $unit['unitID'];
			}
			return self::getStandardUnitsOfMeasure($standardIDs);
		}
	}

	public static function getItemsforPurchaseOrder($id)
	{
		$query_string = "SELECT * from `purchaseorder_items` where `_deleted` = 0 and `purchaseOrderID` = '" . $id . "'";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getOrderedItemsForVendorID($id)
	{
		$query_string = "SELECT vi.`id` as `vendorItemID`, vi.`SKU` AS `SKU`, ii.`recentPurchaseQuantity` AS `quantityOrdered`,ii.`recentPurchaseUnitID` AS `purchaseUnitID`, ii.`recentPurchaseCost` AS `purchaseUnitCost`, ii.`name` AS `name`		
			FROM `vendor_items` vi
			LEFT JOIN `inventory_items` ii ON (vi.`inventoryItemID` = ii.`id`)
			WHERE vi.`vendorId` = {$id}
			AND ii.`_deleted` = 0
			AND ii.`recentPurchaseQuantity` IS NOT NULL
			ORDER BY vi.`id` DESC";

		$result = DBConnection::clientQuery($query_string);
		$returnResult =  MarshallingArrays::resultToArray($result);

		return $returnResult;
	}

	public static function getPurchaseOrdersByDate($date, $filter)
	{
		$where_statement = "WHERE `_deleted != 0";
		if ($filter == 'created')
		{
			$where_statement .= " AND `orderDate` = '" . $date . "'";
		} else if ($filter == 'received')
		{
			$where_statement .= " AND `receiveDate` = '" . $date . "'";
		}

		$query_string = "SELECT * from `inventory_purchaseorder` " . $where_statement;
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getExpectedPurchaseOrders()
	{
        $query_string = "SELECT (pi.`quantityOrdered` - IFNULL(pi.`quantityReceived`, 0)) AS expectedReceivable, pi.`purchaseUnitCost` FROM `purchaseorder_items` pi LEFT JOIN `inventory_purchaseorder` ip ON pi.`purchaseOrderID` = ip.`id` WHERE ip.`_deleted` = 0 AND pi.`quantityOrdered` > IFNULL(pi.`quantityReceived`, 0)";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function updatePurchaseOrders($purchaseOrders)
	{
		return self::receivePurchaseOrders($purchaseOrders);
	}

	public static function updatePurchaseOrderStatus($purchaseOrders)
	{
		$rowsUpdated = 0;
		$updateOk = true;
		// Update PurchaseOrders
		foreach ($purchaseOrders as $i => $purchaseOrder)
		{
			if (isset($purchaseOrder['purchaseOrderItems']))
			{
				unset($purchaseOrder['purchaseOrderItems']);
			}
			if (!empty($purchaseOrder['id']))
			{
				// So we're not updating id
				$purchaseOrderId = $purchaseOrder['id'];
				unset($purchaseOrder['id']);
				// Doing the && will prevent updates from being attempted after the first failure
				$updateOk = $updateOk && CQ::updateArray('inventory_purchaseorder', $purchaseOrder, " WHERE `id` = {$purchaseOrderId} AND `_deleted` = 0");  // TO DO : sanitize $purchaseOrderId
				if ($updateOk)
				{
					$rowsUpdated++;
				}
			}
		}
		return $rowsUpdated;
	}

	public static function getPurchaseOrderItemsByID($id){
	    return CQ::getWhereColumnEquals("purchaseorder_items", "id", $id);
    }

    public static function getVendorItemByID($id){
        $query = "SELECT * FROM `vendor_items` WHERE `id` = $id";
        $result = DBConnection::clientQuery($query);
        return MarshallingArrays::resultToArray($result);
    }

	public static function receivePurchaseOrders($purchaseOrders)
	{
	    $sUOMS = self::getAllUnitIDsConversions();
		$rowsUpdated = 0;
		$updateOk = true;
        $inventoryRowsUpdated = 0;
		$cachInventoryDetails = array();

		//TODO:- Remove once caching is working properly
		if (sessvar("ENABLED_REDIS")) {
			$redisController = new InventoryRedisController();
			$cachInventoryDetails = RequestFormatterUtil::decodeLavuAPIResponse($redisController->getInventory());
		}
		// Update PurchaseOrders
		foreach ($purchaseOrders as $i => $purchaseOrder)
		{
			if (!empty($purchaseOrder['id']))
			{
				// So we can use $purchaseOrder in CQ::updateArray()
				$purchaseOrderItems = $purchaseOrder['purchaseOrderItems'];
				unset($purchaseOrder['purchaseOrderItems']);

				// So we're not updating id
				$purchaseOrderId = $purchaseOrder['id'];
				unset($purchaseOrder['id']);

				// Doing the && will prevent updates from being attempted after the first failure
				$updateOk = $updateOk && CQ::updateArray('inventory_purchaseorder', $purchaseOrder, " WHERE `id` = {$purchaseOrderId} AND `_deleted` = 0");  // TO DO : sanitize $purchaseOrderId
				if ($updateOk)
				{
					$rowsUpdated++;
				}
			}
			// Update PurchaseOrderItems
			foreach ($purchaseOrderItems as $j => $purchaseOrderItem)
			{

				if (!empty($purchaseOrderItem['id']))
				{
				    //Get the current state of the purchase order item, check for quantity received.  This is to make sure we are receiving the correct amount.
                    $currentPOItem = self::getPurchaseOrderItemsByID($purchaseOrderItem['id']);
                    if($currentPOItem[0]['quantityReceived'] !== null){
                       $purchaseOrderItem['quantityReceived'] = $purchaseOrderItem['quantityReceived'] - $currentPOItem[0]['quantityReceived'];
                    }
					$inventoryItemUpdate = array(
						'recentPurchaseQuantity'=>$purchaseOrderItem['quantityOrdered'],
						'recentPurchaseCost'=>$purchaseOrderItem['purchaseUnitCost'],
						'recentPurchaseUnitID'=>$purchaseOrderItem['purchaseUnitID'],
                        '_deleted'=>'0'
					);
					$vendorItemID = $currentPOItem[0]['vendorItemID'];
					$vendorItem = self::getVendorItemByID($vendorItemID);
					$inventoryItemId = $vendorItem[0]['inventoryItemID'];
                    $currentInventoryItem = self::getInventoryItemByID($inventoryItemId);
                    $getUnitConversion = self::getUnitConversion($purchaseOrderItem['purchaseUnitID'], $currentInventoryItem[0]['salesUnitID'], $sUOMS);
                    $salesQuantityReceived = $purchaseOrderItem['quantityReceived'] * $getUnitConversion;
					$inventoryItemResult = CQ::updateArray('inventory_items',$inventoryItemUpdate,"WHERE `id`=".$inventoryItemId);

					if($inventoryItemResult){
						$rowsUpdated++;
					}
					$purchaseOrderItemResult = CQ::addOrSubtractFromColumn('purchaseorder_items', 'quantityReceived', $purchaseOrderItem['quantityReceived'], 1, "WHERE `id`='".$purchaseOrderItem['id']."'");
					if($purchaseOrderItemResult){
					    $rowsUpdated++;
                    }

                    $currentItem = self::getInventoryItemByID($inventoryItemId);
                    $inventoryOverage = $currentItem[0]['inventoryOverage'];
                    if ($inventoryOverage > 0) {
                        if ($salesQuantityReceived > $inventoryOverage) {
                            $salesQuantityReceived = $salesQuantityReceived - $inventoryOverage;
                            $newInventoryOverage = $inventoryOverage;
                        } else {
                            $newInventoryOverage =  $salesQuantityReceived;
                            $salesQuantityReceived = 0;
                        }

                        $updateoverAgeQuantityResult = CQ::addOrSubtractFromColumn('inventory_items', 'inventoryOverage', $newInventoryOverage, -1, "WHERE `id` = ".$inventoryItemId);
                        if($updateoverAgeQuantityResult){
                            $inventoryRowsUpdated++;
                        }
                    }
                    if ($salesQuantityReceived > 0) {
                        $updateQuantityResult = CQ::addOrSubtractFromColumn('inventory_items', 'quantityOnHand', $salesQuantityReceived, 1, "WHERE `id` = " . $inventoryItemId);
                        if ($updateQuantityResult) {
	                        //TODO:- Remove once caching is working properly
	                        if (sessvar("ENABLED_REDIS")) {
		                        unset($cachInventoryDetails[$inventoryItemId]);
	                        }
	                        $inventoryRowsUpdated++;
                        }
                    }

					if(!$inventoryRowsUpdated){
						error_log(__FILE__ . " " . __LINE__ . " " . print_r("Received inventory items quantity not updated.",1));
					}
				}
			}
		}

		$redisController->setInventory($cachInventoryDetails);

		return array($rowsUpdated);
	}

	public static function updateTransfers($transfers)
	{
		return self::receiveTransfers($transfers);
	}

	public static function receiveTransfers($transfers)
	{
		$rowsUpdated = 0;
		$updateOk = true;

		// Update Transfers
		foreach ($transfers as $i => $transfer)
		{
			if (!empty($transfer['id']))
			{
				// So we can use $transfer in CQ::updateArray()
				$transferItems = $transfer['transferItems'];
				unset($transfer['transferItems']);

				// So we're not updating id
				$transferId = $transfer['id'];
				unset($transfer['id']);

				// Doing the && will prevent updates from being attempted after the first failure
				$updateOk = $updateOk && CQ::updateArray('inventory_transfer', $transfer, " WHERE `id` = {$transferId} AND `_deleted` = 0");  // TO DO : sanitize $transferId
				if ($updateOk)
				{
					$rowsUpdated++;
				}
			}

			// Update TransferItems
			foreach ($transferItems as $j => $transferItem)
			{
				if (!empty($transferItem['id']))
				{
					$transferItemId = $transferItem['id'];
					$updateOk = $updateOk && CQ::updateArray('transfer_items', $transferItem, " WHERE `id` = {$transferItemId} AND `_deleted` = 0");
					if ($updateOk)
					{
						$rowsUpdated++;
					}
				}
			}
		}

		return $rowsUpdated;
	}

	public static function getAllPurchaseOrdersForVendorID($id)
	{
		$query_string = "SELECT * FROM `inventory_purchaseorder` WHERE `_deleted` = 0 and `vendorID` = '" . $id . "' ORDER BY `id` desc";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getAllLowStockItems()
	{
        $query_string = $query_string = "SELECT * FROM `inventory_items` WHERE `inventory_items`.`quantityOnHand` <= `inventory_items`.`reOrderQuantity` AND `_deleted`!= 1";
        $result = DBConnection::clientQuery($query_string);
        return MarshallingArrays::resultToArray($result);
	}

	public static function getAllItemsForReOrder()
	{
	    $vendors = self::getAllVendors();
	    if(empty($vendors)){
	        $query_string = $query_string = "SELECT * FROM `inventory_items` WHERE `inventory_items`.`quantityOnHand` <= `inventory_items`.`reOrderQuantity` AND `_deleted`!= 1";

        }
        else {
            $query_string = "SELECT * FROM `inventory_items` WHERE `inventory_items`.`quantityOnHand` <= `inventory_items`.`reOrderQuantity` AND `_deleted`!= 1 AND `primaryVendorID` > 0 ";
        }
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getAllTransferItems()
	{
		$query_string = "select * from `transfer_items` where `_deleted` = 0";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

    public static function getAuditsByAction($action, $startDate = '', $endDate = '')
    {
        $whereClause = '';
        if ($startDate && $endDate) {
            $whereClause = ' AND datetime >= "' . $startDate . '" AND datetime <= "' . $endDate . '"';
        }
        $query_string = "SELECT * FROM `inventory_audit` WHERE `_deleted` = 0 AND `action` = '{$action}'" . $whereClause;
        $result = DBConnection::clientQuery($query_string);
        return MarshallingArrays::resultToArray($result);
    }

	public static function getAuditsByActionsAndDates($args)
	{
		$timeZone = sessvar('location_info')['timezone'];
		//Audit Start Date,Start Time,End Date,End Time
		$auditStart = explode(" ", $args['startDate']);
		$auditEnd = explode(" ", $args['endDate']);
		$audit_start_date = $auditStart[0];
		$audit_start_time = $auditStart[1];
		$audit_end_date = $auditEnd[0];
		$audit_end_time = $auditEnd[1];
		$mode = '';
		$mode = $args['mode'];
		//Day Mode
		if ($mode == null || $mode == '' || $mode == 'day') {
			//If Time Seleted & End Time Greater Than Start Time
			if ($audit_start_time != $audit_end_time && $audit_end_time > $audit_start_time) {
			$where_clause = "(DATE(`datetime`) BETWEEN '$audit_start_date' AND '$audit_end_date') AND (TIME(`datetime`) BETWEEN '$audit_start_time' AND '$audit_end_time')";
			} else {
			//If Time Not Selected
			$where_clause = " `datetime` BETWEEN '".$args['startDate']."' AND '".$args["endDate"]."'";
			}
		} else {
			//Month & Year Mode
			$where_clause = " `datetime` BETWEEN '".$args['startDate']."' AND '".$args["endDate"]."'";
		}

		$actionList = MarshallingQueries::buildSanitizedListInParenthesis($args['actions'], "'");
		$query = "SELECT * FROM `inventory_audit` WHERE `_deleted` = 0 AND `action` IN{$actionList} AND $where_clause";
		$result = DBConnection::clientQuery($query);
		$auditArray = MarshallingArrays::resultToArray($result);
		if ($timeZone != "") {
            foreach($auditArray as &$audit){
                $dt = DateTime::createFromFormat("Y-m-d H:i:s", $audit['datetime'], new DateTimeZone("GMT"));
                $dt->setTimeZone(new DateTimeZone($timeZone));
                $audit['datetime'] = $dt->format("Y-m-d H:i:s");
            }
        }
        return $auditArray;
	}


	public static function getInventoryItemByID($id)
	{
		$query_string = "SELECT * FROM `inventory_items` WHERE `id` = {$id}";
		$result = DBConnection::clientQuery($query_string);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getInventorySettings()
	{
		$query_string = "	
			SELECT *, CASE WHEN `inventory_settings`.`setting` = 'use_first_inventory' 
					THEN 
						(SELECT `t2`.`name` FROM `inventory_settings` `t1`, `inventory_categories` `t2` 
						WHERE EXISTS (SELECT `name` 
				              FROM `inventory_categories`
				              WHERE `t1`.`setting` = 'use_first_inventory'
				              AND `t2`.`id` = `t1`.`value`)) 
				    ELSE ''
				END 
			    AS `name` FROM `inventory_settings`";
		$result = DBConnection::clientQuery($query_string);
		return $result;
	}

	public static function getInventoryItemByIDForReals($id){
	    $query = "SELECT * FROM `inventory_items` where `id` = '$id'";
	    $result = DBConnection::clientQuery($query);
	    $resultArray = mysqli_fetch_all($result);
	    return MarshallingArrays::resultToArray($result);
    }

	public static function getMenuItemsByInventoryItemID($idArray)
	{
		$whereClause = "WHERE `menuitem_86`.`_deleted` = 0 AND `menuitem_86`.`inventoryItemID` = " . $idArray[0]['id'];
		for($i = 1; $i < count($idArray); $i++){
			$whereClause .= " OR `menuitem_86`.`inventoryItemID` = " . $idArray[$i]['id'];
		}

		$query = "SELECT `menu_items`.`name`, `menu_items`.`id`, `menu_items`.`_deleted` FROM `menu_items`
		LEFT JOIN `menuitem_86` ON `menuitem_86`.`menuItemID` = `menu_items`.`id` " . $whereClause;

		$result = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
		return $result;
	}

	public static function getAllMenuItems()
	{
		$query = "SELECT `menu_items`.`name`, `menu_items`.`id`, `menu_items`.`_deleted` FROM `menu_items`";
		$result = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
		return $result;
	}


	public static function getAllModifiers()
	{
		$query = "SELECT * FROM `modifiers`";
		$result = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
		return $result;
	}

	public static function getAllForcedModifiers()
	{
		$query = "SELECT * FROM `forced_modifiers`";
		$result = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
		return $result;
	}

	public static function getAllModifiersByInventoryItemID($idArray)
	{
		$whereClause = "WHERE `modifier_86`.`_deleted` = 0 AND `modifier_86`.`inventoryItemID` = " . $idArray[0]['id'];
		for($i = 1; $i < count($idArray); $i++){
			$whereClause .= " OR `modifier_86`.`inventoryItemID` = " . $idArray[$i]['id'];
		}

		$query = "SELECT * FROM `modifiers`
		LEFT JOIN `modifier_86` ON `modifier_86`.`modifierID` = `modifiers`.`id` " . $whereClause;
		$result = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
		return $result;
	}

	public static function getAllForcedModifiersByInventoryItemID($idArray)
	{
		$whereClause = "WHERE `forced_modifier_86`.`_deleted` = 0 AND `forced_modifier_86`.`inventoryItemID` = " . $idArray[0]['id'];
		for($i = 1; $i < count($idArray); $i++){
			$whereClause .= " OR `forced_modifier_86`.`inventoryItemID` = " . $idArray[$i]['id'];
		}

		$query = "SELECT * FROM `forced_modifiers`
		LEFT JOIN `forced_modifier_86` ON `forced_modifier_86`.`forced_modifierID` = `forced_modifiers`.`id` " . $whereClause;
		$result = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
		return $result;
	}

	public static function getAllUnits(){
		$query_string = "SELECT `iu`.* FROM `inventory_unit` iu
					WHERE `categoryID` != 3 AND `_deleted` != 1";
		$standardUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query_string));
		$standardIDs = array();
		foreach($standardUnits as $i=>$unit){
			$standardIDs[] = $unit['unitID'];
		}
		$sUOMS = self::getStandardUnitsOfMeasure($standardIDs);

		$query_string = "SELECT `cUOM`.*, `iu`.*
			FROM `inventory_unit` iu
				LEFT JOIN `custom_units_measure` cUOM
					ON `cUOM`.`id` = `iu`.`unitID`
					WHERE `iu`.`categoryID` = 3
					AND  `iu`.`_deleted` != 1 AND  `cUOM`.`_deleted` != 1";
		$customUnits = MarshallingArrays::resultToArray(DBConnection::clientQuery($query_string));

		$result = array_merge($sUOMS, $customUnits);
		return $result;
	}

	public static function getLinkedVendorItemsByItemID($id){
	    if(!$id){
	        return null;
        }
	    $query = "SELECT `vi`.inventoryItemID as 'inventoryItemID', `vi`.vendorID as 'vendorID', `vi`.`id` as 'vendorItemID',
		`PI`.`purchaseUnitID` as 'purchaseUnitID', `PI`.`quantityReceived` as 'quantity', 
		`PI`.`purchaseUnitCost` as 'purchaseUnitCost', `V`.`name` as 'vendorName'
	                FROM `vendor_items` vi
	                JOIN (SELECT `pi`.`id` as POItemID , max(`po`.`receiveDate`), `pi`.`vendorItemID`,
	                            `pi`.`quantityReceived`, `pi`.`purchaseUnitCost`, 
	                            `pi`.`purchaseUnitID` 
	                            FROM `purchaseorder_items` pi JOIN `inventory_purchaseorder` po
	                            ON `pi`.`purchaseOrderID` = `po`.`id`
	                            WHERE `pi`.`_deleted` != 1
	                            GROUP BY `pi`.`vendorItemID`) PI
	                  ON `PI`.`vendorItemID` = `vi`.`id`
	                  JOIN `vendors` V
 	                  ON `V`.`id` = `vi`.`vendorID`
	                  WHERE `vi`.`inventoryItemID` = $id
	                  AND `vi`.`_deleted` != 1";
        $result =  MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
        if(empty($result)){
            $query = "SELECT `vi`.inventoryItemID as 'inventoryItemID', `vi`.vendorID as 'vendorID', `vi`.id as 'vendorItemID',
            `v`.name as 'vendorName' FROM `vendor_items` vi JOIN `vendors` v ON `v`.`id` = `vi`.`vendorID` WHERE `vi`.`inventoryItemID` = $id AND `vi`.`_deleted` != 1";
            $result =  MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
            $purchaseOrderPlaceholder = array();
            $purchaseOrderPlaceholder['purchaseUnitID'] = null;
            $purchaseOrderPlaceholder['purchaseUnitCost'] = null;
            $purchaseOrderPlaceholder['quantity'] = null;
            for($i = 0; $i < count($result); $i++){
                $result[$i] = array_merge($result[$i], $purchaseOrderPlaceholder);
            }
        }
        return $result;
    }

	public static function getVendorsByName($names){
		$inClause = " IN ('".str_replace("'", "\\'", $names[0])."'";
		for($i = 1; $i < count($names); $i++){
			$inClause .= ", '".str_replace("'","\\'", $names[$i])."'";
		}
		$inClause .= ")";
		$query = "SELECT * FROM `vendors` WHERE `name`".$inClause;
		$result =  MarshallingArrays::resultToArray(DBConnection::clientQuery($query));
		return $result;
	}

	public static function getSalesRevenueForDateRange($args){
		$startDate = $args['startDate'];
		$endDate = $args['endDate'];
		$startTime = $args['startTime'];
		$endTime = $args['endTime'];
		$mode = $args['mode'];
		//Start Date,Start Time,End Date,End Time
		$startDate = explode(" ", $startDate);
		$endDate = explode(" ", $endDate);
		$foodbev_start_date = $startDate[0];
		$foodbev_start_time = $startDate[1];
		$foodbev_end_date = $endDate[0];
		$foodbev_end_time = $endDate[1];
		$datetime_WhereStr = '';
		//Day Mode
		if ($mode == null || $mode == '' || $mode == 'day') {
			//Check Time Seleted && End Time Greater than Start Time
			if ($foodbev_start_time != $foodbev_end_time && $foodbev_end_time > $foodbev_start_time) {
			$datetime_WhereStr = "(DATE(`datetime`) BETWEEN '$foodbev_start_date' AND '$foodbev_end_date') AND (TIME(`datetime`) BETWEEN '$foodbev_start_time' AND '$foodbev_end_time')";
			} else {
			//If Time Not Selected
			$datetime_WhereStr = "`datetime` BETWEEN '".$args['startDate']."' AND '".$args['endDate']."'";
			}
		} else {
			//Month & Year Mode
			$datetime_WhereStr = "`datetime` BETWEEN '".$args['startDate']."' AND '".$args['endDate']."'";
		}
		$query = "SELECT SUM(IF(`action`='Refund', -1 * `amount`, `amount`)) AS `revenue` FROM `cc_transactions` WHERE $datetime_WhereStr AND `action` IN('Sale', 'Refund') AND `order_id` NOT IN('Paid In', 'Paid Out') AND `order_id` NOT LIKE '777%' AND `voided` = 0";
		$result = DBConnection::clientQuery($query);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getUsers($args){
		$query = "SELECT * FROM `users`";
		$result = DBConnection::clientQuery($query);
		return MarshallingArrays::resultToArray($result);
	}

	public static function getLegacyInventoryItems(){

        $categories = CQ::getAllRows('ingredient_categories');
        if(empty($categories)){
            //error_log("No categories");
            $backupQuery = "SELECT `title` as 'name', `qty` as 'quantity', `cost` FROM `ingredients` WHERE `_deleted` != 1";
            $backupResult = DBConnection::clientQuery($backupQuery);
            return MarshallingArrays::resultToArray($backupResult);
        }
	    $query  = "SELECT i.`title` as 'name', i.`qty` as 'quantity', i.`cost` as 'cost' 
                    FROM `ingredients` i  join `ingredient_categories` ic 
                    ON i.`category` =  ic.`id` 
                    WHERE i.`_deleted` != 1 AND ic.`_deleted` !=1";
	    $result = DBConnection::clientQuery($query);
	    $resultArray = MarshallingArrays::resultToArray($result);
        return $resultArray;




    }

    public static function getAllCustomUnitsofMeasure(){

	    $unitIDConversions = array();
	    $unitIDConversions[16] = 1;
        $query = "SELECT `iu`.`id` as 'id', `cUOM`.`conversion` as 'conversion' FROM `custom_units_measure` cUOM join `inventory_unit` iu on `cUOM`.`id` = `iu`.`unitID` ";
        $cUOMS = MarshallingArrays::resultToArray(DBConnection::clientQuery($query));

        foreach($cUOMS as $unitDetails){
            $unitIDConversions[$unitDetails['id']] = $unitDetails['conversion'];
        }
        return $unitIDConversions;
    }

    public static function getAllUnitIDsConversions() {
        $sUOMS = self::getAllStandardUnitsOfMeasure();
        $unitIdConversion = array();
        foreach($sUOMS as $unitDetails) {
            $unitIdConversion[$unitDetails['id']] = $unitDetails['conversion'];
        }
        return $unitIdConversion;
    }

    //Return whatever we need to multiply a value in the FROMUNIT to convert it into the TO Unit.
    public static function getUnitConversion($fromUnitID, $toUnitID, $sUOMS) {
	    $unitConversion = 1;
	    if(empty($fromUnitID) || empty($toUnitID)){
	        return 1; //unit conversion invalid, return 1
        }
        if(isset($sUOMS[$fromUnitID]) && isset($sUOMS[$toUnitID])){
            if ($fromUnitID != $toUnitID) {
                if($sUOMS[$fromUnitID] == 1) {
                    $unitConversion = $sUOMS[$toUnitID]; //from Unit is unitary for the category, multiply by the To Unit conversion
                }
                else if($sUOMS[$fromUnitID] == 0){
                    $baseConversion = 1;
                    $unitConversion = $baseConversion * $sUOMS[$toUnitID];
                }
                else {
                    $baseConversion = 1 / $sUOMS[$fromUnitID];
                    $unitConversion = $baseConversion * $sUOMS[$toUnitID];
                }
            }
        }
        else{
            //If the conversion doesn't exist in the standard units, we need to check custom units.
            $customUnits = self::getAllCustomUnitsOfMeasure();
            $purchaseConversion = isset($customUnits[$fromUnitID])?$customUnits[$fromUnitID]:1; //from this unit
            $salesConversion = isset($customUnits[$toUnitID])?$customUnits[$toUnitID]:1; //to this unit
            if ($purchaseConversion != $salesConversion) {
                if($purchaseConversion == 1) { //if we are converting from EACH to another unit, gotta return 1/the conversion factore
                    if($salesConversion !== 0){
                        $unitConversion = 1/$salesConversion;
                    }
                }
                else if($purchaseConversion == 0){
                    $baseConversion = 1;
                    $unitConversion = $baseConversion * $salesConversion;
                }
                else if($salesConversion == 1){
                    $unitConversion = $purchaseConversion;
                }
                else {
                    $baseConversion = 1 / $salesConversion;
                    $unitConversion = $baseConversion * $purchaseConversion;
                }
            }

        }

	    return $unitConversion;

    }

    public static function createConfigSetting($setting, $value, $type){

	    $query = "INSERT into `config` (`setting`, `value`, `type`) values('[1]', '[2]', '[3]')";

	    $result = DBConnection::clientQuery($query, $setting, $value, $type);
        if($result){
            return "success";
        }
	}

	public static function insertVendorItem($args){
        $query = "INSERT INTO `vendor_items` (`inventoryItemID`, `vendorID`, `BIN`, `SKU`, `barcode`) values('[1]', '[2]','[3]','[4]','[5]')";
        $result = DBConnection::clientQuery($query, $args['inventoryItemID'], $args['vendorID'], $args['BIN'], $args['SKU'], $args['barcode']);
        if($result){
            return true;
        }
        else return false;
    }
	public static function getConfigSetting($setting, $value){
        $query = "SELECT * FROM `config` WHERE `setting` = '[1]'";
        if($value != ""){
            $query .= " AND `value` = '[2]'";
            $result = DBConnection::clientQuery($query, $setting, $value);
        }
        else{
            $result = DBConnection::clientQuery($query, $setting);
        }
        if($result){
            return MarshallingArrays::resultToArray($result);
        }
        else return false;
    }


    public static function updateConfigSetting($setting, $value){
        $query = "UPDATE `config` SET `value` = '[1]' WHERE `setting` = '[2]'";
        $result = DBConnection::clientQuery($query, $value, $setting);
        if($result){
            return MarshallingArrays::resultToArray($result);
        }
        else return false;
    }

    public static function createOrUpdateConfigSetting($setting, $value, $type = ""){
        $exists = self::getConfigSetting($setting, "");
        if($exists){
            self::updateConfigSetting($setting, $value);
        }
        else{
            self::createConfigSetting($setting, $value, $type);
        }
    }

    public static function getNextPurchaseOrder($lastPOID, $itemID){
        //$vendorItems = self::getLinkedVendorItemsByItemID($itemID);
        $vendorItemQuery = "SELECT * from `vendor_items` where `inventoryItemID` = $itemID";
        $vendorResult = DBConnection::clientQuery($vendorItemQuery);
        $vendorItems = MarshallingArrays::resultToArray($vendorResult);
        if($vendorItems){
            $vendorArray= array();
            foreach($vendorItems as $item){
                $vendorArray[$item['id']] = $item['id'];
            }
            if(count($vendorArray) > 1){
                $vendorItemsString = "IN (".implode(",", $vendorArray).")";
            }
            else{
                $vendorItemsString = " = '".implode(",", $vendorArray)."'";
            }

        }
        else{
            return false;
        }

        if(!$lastPOID){
            $query =  "SELECT pi.`id`, pi.`purchaseUnitID`, pi.`quantityReceived`, po.`receiveDate`, pi.`purchaseOrderID` as 'purchaseOrderID',
                      pi.`purchaseUnitCost`, pi.`vendorItemID`
                      FROM `purchaseorder_items` pi join `inventory_purchaseorder` po ON po.`id` = pi.`purchaseOrderID`  
                      WHERE  po.`receiveDate` != 00-00-0000 AND pi.`vendorItemID`$vendorItemsString
                      ORDER BY `pi`.`purchaseOrderID` DESC LIMIT 0, 1";
        }
        else{
            $query = "SELECT pi.`id`, pi.`purchaseUnitID`, pi.`quantityReceived`, po.`receiveDate`, pi.`purchaseOrderID` as 'purchaseOrderID',
                      pi.`purchaseUnitCost`, pi.`vendorItemID`
                      FROM `purchaseorder_items` pi join `inventory_purchaseorder` po ON po.`id` = pi.`purchaseOrderID`  
                      WHERE  po.`receiveDate` != 00-00-0000 AND pi.`vendorItemID` $vendorItemsString AND po.`id` < $lastPOID
                      ORDER BY `pi`.`purchaseOrderID` DESC LIMIT 0,1";
        }
        $result = DBConnection::clientQuery($query);
        if($result){
            return MarshallingArrays::resultToArray($result);
        }
        else return false;
    }

    public static function getNextTransfer($lastTransferID, $itemID, $restaurantID){

        if(!$lastTransferID){

            $query = "SELECT ti.`transferID` as 'transferID', it.`received_ts` as 'receiveDate', ti.`inventoryItemID`,
                      ti.`quantityReceived`, ti.`transferUnitID`, ti.`transferUnitCost`
                      FROM `transfer_items` ti join `inventory_transfer` it
                      ON ti.`transferID` = it.`id`
                      WHERE it.`destinationRestaurantID` = $restaurantID AND 
                      it.`received_ts` != 00-00-0000 AND ti.`inventoryItemID` = $itemID
                      ORDER BY `ti`.`transferID` DESC LIMIT 0, 1";
        }
        else{
            $query = "SELECT ti.`transferID` as 'transferID', it.`received_ts`, ti.`inventoryItemID`,
                      ti.`quantityReceived`, ti.`transferUnitID`, ti.`transferUnitCost`
                      FROM `transfer_items` ti join `inventory_transfer` it
                      ON ti.`transferID` = it.`id`
                      WHERE it.`destinationRestaurantID` = $restaurantID 
                      AND it.`received_ts` != 00-00-0000
                      AND ti.`inventoryItemID` = $itemID
                      AND it.`id` < $lastTransferID
                      ORDER BY `ti`.`transferID` DESC LIMIT 0, 1";
        }
        $result = DBConnection::clientQuery($query);
        if($result){
            return MarshallingArrays::resultToArray($result);
        }
        else return false;
    }

    public static function getNextReceiveWithoutPO($lastAuditID, $itemID){
        if(!$lastAuditID){
            $query = "SELECT `datetime` as 'receiveDate', `id`, `unitID`, `cost`, `quantity` FROM `inventory_audit` WHERE `action` = 'receiveWithoutPO'  AND
                  `inventoryItemID` = '$itemID' ORDER BY `id` DESC LIMIT 0, 1";
        }
        else{
            $query = "SELECT `datetime` as 'receiveDate', `id`, `unitID`, `cost`, `quantity` FROM `inventory_audit` WHERE `action` = 'receiveWithoutPO'  AND
                  `inventoryItemID` = '$itemID' AND `id` < $lastAuditID ORDER BY `id` DESC LIMIT 0, 1";
        }
        $result = DBConnection::clientQuery($query);
        if($result){
            return MarshallingArrays::resultToArray($result);
        }
        else return false;
    }

    public static function getNextReconciliationData($lastAuditID, $itemID){
        if(!$lastAuditID){
            $query = "SELECT `datetime` as 'receiveDate', `id`, `unitID`, `cost`, `quantity`, `userNotes` FROM `inventory_audit` WHERE `action` = 'reconciliation'  AND
                  `inventoryItemID` = '$itemID' ORDER BY `id` DESC LIMIT 0, 1";
        }
        else{
            $query = "SELECT `datetime` as 'receiveDate', `id`, `unitID`, `cost`, `quantity`, `userNotes` FROM `inventory_audit` WHERE `action` = 'reconciliation'  AND
                  `inventoryItemID` = '$itemID' AND `id` < $lastAuditID ORDER BY `id` DESC LIMIT 0, 1";
        }
        $result = DBConnection::clientQuery($query);
        if($result){
            return MarshallingArrays::resultToArray($result);
        }
        else return false;
    }

    public static function getInventorySetting($setting){
        $query = "SELECT `value` from `inventory_settings` WHERE `setting` = '$setting'";
        $result = DBConnection::clientQuery($query);
        return MarshallingArrays::resultToArray($result);
   }

   //gets low stock amounts with menu object names.  Slightly less efficient than pure ID checking
    public static function getLowStockStatus($idArray, $tablename, $whereColumn, $objectTableName){
        $objectType = "menuItemID";
        $nameColumn = 'name';
        if($objectTableName === "modifiers"){
            $objectType = "modifierID";
            $nameColumn = 'title';
        }
        else if($objectTableName === "forced_modifiers"){
            $objectType = "forced_modifierID";
            $nameColumn = 'title';
        }

        $inList = implode(",", $idArray);
        $query = "SELECT items.`$nameColumn` as 'objectName', mi.`$whereColumn` as 'objectID', mi.`reservation`, mi.`quantityUsed`, mi.`unitID` as '86Unit', 
                  it.`id` as 'inventoryItemID', it.`quantityOnHand`, it.`salesUnitID`, it.`lowQuantity`, it.`reOrderQuantity`
                  FROM `inventory_items` it 
                  JOIN `$tablename` mi ON mi.`inventoryItemID` = it.`id`
                  JOIN `$objectTableName` items ON items.`id` = mi.`$objectType`
                  WHERE mi.`$whereColumn` IN ($inList) 
                   AND it.`_deleted` != 1 
                   AND mi.`_deleted` != 1";
        $result = DBConnection::clientQuery($query);
        return MarshallingArrays::resultToArray($result);
    }

    //name agnostic stock status checking, requires one less join than the status method that includes names.
   public static function getStockStatus($idArray, $tablename, $whereColumn){

        $inList = implode(",", $idArray);
	   if (sessvar("ENABLED_REDIS")) {
		   $query = "SELECT `$whereColumn` as 'objectID', `reservation`, `quantityUsed`, `unitID` as '86Unit', 
                  `inventoryItemID` as 'inventoryItemID' FROM `$tablename`
                  WHERE `$whereColumn` IN ($inList) 
                   AND `_deleted` != 1";
	   } else {
		   $query = "SELECT mi.`$whereColumn` as 'objectID', mi.`reservation`, mi.`quantityUsed`, mi.`unitID` as '86Unit', 
                  it.`id` as 'inventoryItemID', it.`quantityOnHand`, it.`salesUnitID`, it.`lowQuantity`, it.`reOrderQuantity`
                  FROM `inventory_items` it 
                  JOIN `$tablename` mi ON mi.`inventoryItemID` = it.`id`
                  WHERE mi.`$whereColumn` IN ($inList) 
                   AND it.`_deleted` != 1 
                   AND mi.`_deleted` != 1";
	   }
        $result = DBConnection::clientQuery($query);
        return MarshallingArrays::resultToArray($result);
   }

    public static function getTotal86ReserveQuantity($idArray) {
       $response = array();
       if(!empty($idArray)) {
           $inList = implode(",", $idArray);
           $query = "SELECT `inventoryItemID`, SUM(total) AS reserve FROM (SELECT `inventoryItemID`, SUM(`reservation`) AS total FROM `menuitem_86` 
                      WHERE `_deleted` = 0  GROUP BY `inventoryItemID` 
                      UNION ALL
                      SELECT `inventoryItemID`, SUM(`reservation`) AS total FROM forced_modifier_86 WHERE `_deleted` = 0 GROUP BY inventoryItemID 
                      UNION ALL 
                      SELECT `inventoryItemID`, SUM(`reservation`) AS total FROM `modifier_86` WHERE `_deleted` = 0 
                      GROUP BY `inventoryItemID` ) temp
                    WHERE `inventoryItemID` IN ([1])  GROUP BY inventoryItemID";
           $result = DBConnection::clientQuery($query, $inList);
           $response = MarshallingArrays::resultToArray($result);
        }
        return $response;
    }

	/**
	 * Description :- Get vendor items details by passing inventory item id and vendor id
	 * @param $itemID inventory item id
	 * @param $vId vendor item id
	 * @return array|bool|null
	 */
	public static function getVendorItemsByVendorAndInventoryID($inventoryID, $vendorID){
		$vendorItems = array();
		if($inventoryID && $vendorID) {
			$vendorItemQuery = "SELECT * from `vendor_items` where `inventoryItemID` = '[1]' AND `vendorID` = '[2]'";
			$vendorResult = DBConnection::clientQuery($vendorItemQuery, $inventoryID, $vendorID);
			$vendorItems = MarshallingArrays::resultToArray($vendorResult);
		}
		return $vendorItems;
	}

	/**
	 * This function is used to get inventory id sales count.
	 * @param array $inventoryIDs list
	 * @return array of inventory id and total sales.
	 */
	public static function getInventory86count($inventoryIdList) {
		$inventoryID = implode("','",$inventoryIdList);
		$query_string = "SELECT `inventoryItemID`, SUM(`inventory_audit`.`quantity`) AS total_sales FROM `inventory_audit` WHERE `inventory_audit`.`action` = '86count' AND `_deleted` = 0
	AND `inventory_audit`.`datetime` != '0000-00-00 00:00:00' AND `inventory_audit`.`inventoryItemID` IN('".$inventoryID."') GROUP BY  `inventoryItemID`";
		$result = DBConnection::clientQuery($query_string);
		$totalSales = MarshallingArrays::resultToArray($result);
		return $totalSales;
	}

	/**
	 * This function is used to getInventoryAuditInfo.
	 * @param number $inventoryID
	 * @return Array InventoryAuditInfo
	 */
	public static function getInventoryAuditInfo($inventoryID) {
		$query_string = "SELECT `quantity`, `cost`, `datetime` as `date` FROM `inventory_audit` WHERE `_deleted` = 0	AND `datetime` != '0000-00-00 00:00:00' AND `inventoryItemID` ='[1]' AND `action` IN('createInventoryItem','receiveWithoutPO') order BY datetime ASC";
		$result = DBConnection::clientQuery($query_string, $inventoryID);
		$resultData = MarshallingArrays::resultToArray($result);
		return $resultData;
	}
}

