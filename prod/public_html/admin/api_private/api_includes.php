<?php
/**
 * Created by PhpStorm.
 * User: Leif
 * Date: 3/16/17
 * Time: 11:26 AM
 */
header("Access-Control-Allow-Origin: *");  // TO DO : see if this affects inventory
header("Access-Control-Allow-Methods: *");
header("Content-Type: application/json");

spl_autoload_register(function ($className) {

	$class2ImportMap = array();

	// Check session if ENABLED_REDIS flag do not exist then check in DB.
	// This is used for if hit will come from LTG
	if (!sessvar_isset('ENABLED_REDIS')) {
		isCacheEnabled();
	}
	//Logging
	$class2ImportMap['APILog'] = __DIR__.'/APILogging/APILog.php';

	//Exceptions
	$class2ImportMap['APIException'] = __DIR__.'/APIException.php';

	//Base classes
	$class2ImportMap['DataControllerBase'] = __DIR__.'/Components/DataControllerBase.php';

	//Database
	$class2ImportMap['CommonQueries'] = __DIR__.'/Database/CommonQueries.php';
	$class2ImportMap['CQ'] = __DIR__.'/Database/CommonQueries.php';
	$class2ImportMap['DBConnection'] = __DIR__.'/Database/DBConnection.php';
	$class2ImportMap['DBC'] = __DIR__.'/Database/DBConnection.php';
	$class2ImportMap['ComplexQueries'] = __DIR__.'/Database/ComplexQueries.php';

	//Marshalling classes
	$class2ImportMap['MarshallingArrays'] = __DIR__.'/Marshalling/MarshallingArrays.php';
	$class2ImportMap['MA'] = __DIR__.'/Marshalling/MarshallingArrays.php';
	$class2ImportMap['MarshallingStrings'] = __DIR__.'/Marshalling/MarshallingStrings.php';
	$class2ImportMap['MS'] = __DIR__.'/Marshalling/MarshallingStrings.php';
	$class2ImportMap['MarshallingQueries'] = __DIR__.'/Marshalling/MarshallingQueries.php';
	$class2ImportMap['MQ'] = __DIR__.'/Marshalling/MarshallingQueries.php';

	//86CountViewCompiler
	$class2ImportMap['Inventory86CountViewCompiler'] = __DIR__.'/../components/Inventory/RequestCompilations/Inventory86CountViewCompiler.php';
	$class2ImportMap['Inventory86CountController'] = __DIR__.'/../components/Inventory/InventoryObjectControllers/Inventory86CountController.php';
	$class2ImportMap['Inventory86CountRequestFormatter'] = __DIR__.'/../components/Inventory/RequestFormatters/Inventory86CountRequestFormatter.php';

	//Component DataControllers and their corresponding query classes.
	$componentList = array('Accounts','Billing','Customers','Enterprise','Inventory','Items','LavuGiftCard','LavuToGo','Location','Logging','Menu','Orders','Reports','Payment');
	//TODO:- Remove once caching is working properly and add 'Redis' into array
	if (sessvar("ENABLED_REDIS")) {
		$class2ImportMap['RedisConnection'] = __DIR__.'/Redis/RedisConnection.php';
		$class2ImportMap['InventoryRedisController'] = __DIR__.'/../components/Inventory/InventoryObjectControllers/InventoryRedisController.php';
		$componentList[] = 'Redis';
	}

	foreach($componentList as $currComponent){
		$class2ImportMap[$currComponent.'DataController'] = __DIR__.'/Components/'.$currComponent.'/'.$currComponent.'DataController.php';
		$class2ImportMap[$currComponent.'Queries'] = __DIR__.'/Components/'.$currComponent.'/'.$currComponent.'Queries.php';
	}

	//TODO:- Remove once caching is working properly and add 'Redis' into array rename InventoryDataControllerRedis.php
	// to InventoryDataController.php
	if (sessvar("ENABLED_REDIS")) {
		$class2ImportMap['InventoryDataController'] = __DIR__.'/Components/Inventory/InventoryDataControllerRedis.php';
    }

	if(!empty($class2ImportMap[$className])){
		require_once( $class2ImportMap[$className] );
	}

});
