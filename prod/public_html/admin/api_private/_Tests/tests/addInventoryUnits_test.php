<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');


class addInventoryUnits_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		$this->connectToDatabase();
		$this->refreshTableSchema('inventory_unit');
	}

	protected function runTests(){

		$unitsArr = array();
		$unitsArr[] = array('id'=>1, 'name' => 'Kilogram', 'symbol' => "kg", 'conversion' =>	1);
		$unitsArr[] = array('id'=>2, 'name' => 'Pounds', 'symbol' => "lbs", 'conversion' =>	2.20462);
		$unitsArr[] = array('id'=>3, 'name' => 'Liter', 'symbol' => "L", 'conversion' =>	1);
		$unitsArr[] = array('id'=>4, 'name' => 'Ounce', 'symbol' => "oz", 'conversion' =>	33.814);
		$unitsArr[] = array('id'=>5, 'name' => 'Meter', 'symbol' => "m", 'conversion' =>	1);
		$unitsArr[] = array('id'=>6, 'name' => 'Foot', 'symbol' => "'", 'conversion' =>	3.28084);
		$unitsArr[] = array('id'=>7, 'name' => 'Sauce Bottles', 'symbol' => "Bottle", 'conversion' => 0.25);
		$unitsArr[] = array('id'=>8, 'name' => 'Cups', 'symbol' => "C", 'conversion' =>	4.22675);

		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'addInventoryUnits';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['Units'] = $unitsArr;
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}
}

$test = new addInventoryUnits_test();