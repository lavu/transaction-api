<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');

class CreatePurchaseOrders_test extends BaseTest{

    protected $tableColumnName;

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

		$this->connectToDatabase();

		$this->refreshTableSchema('purchaseorder_items');

        $tableColumnName = CommonQueries::allColumnsInTable('poslavu_INVENTORY_db','purchaseorder_items');

        $values = array();
        $values[] = array(NULL, 1, 1, 1,1,10,10,'abc', 0);
        $values[] = array(NULL, 2, 1,2,2,20,20,'abc',  0);
        $values[] = array(NULL, 2, 1,1,1,20,10,'abc9', 0);
        $values[] = array(NULL, 1, 1,1,1,10,10,'abc', 0);
        $values[] = array(NULL, 1, 1,1,1,10,10,'abc',  0);

         CommonQueries::insertArray('inventory_purchaseorder', $tableColumnName, $values);

        //DBConnection::clientQuery($purchaseOrderInsert);
		//DBConnection::clientQuery($purchaseOrderItemsInsert);
	}

	protected function runTests(){

/*

One or more purchaise order items will be inserted,
One inventory purchase order will be duplicated.
*/

        $apiRequest = array();
        $apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
        $apiRequest['Auth']['user_id'] = 1;
        $apiRequest['Action'] = 'insertPurchaseOrdersItems';
        $apiRequest['Params'] = array();
        $apiRequest['Params']['PurchaseOrderItems'] = array(6);
         $apiRequest['Component'] = 'Inventory';
        $apiRequestJSON = json_encode($apiRequest);
        Internal_API::apiQuery($apiRequestJSON);

	}

	protected function tearDown(){

	}

}

$obj = new CreatePurchaseOrders_test();
