<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');


class addVendorItems_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		$this->connectToDatabase();
		$this->refreshTableSchema('vendor_items');
	}

	protected function runTests(){


		/*
		$inventoryItemsJSON = '[{"name":"SpecialSauce","quantity":"2","lowQuantity":"1","reOrderQuantity":"2","sales_unitID":"3","categoryID":"1","primary_vendor_id":"1","storage_location":"1","critical_item":"1","_deleted":0},'.
							   '{"name":"Crackers","quantity":"2","lowQuantity":"1","reOrderQuantity":"2","sales_unitID":"3","categoryID":"1","primary_vendor_id":"1","storage_location":"1","critical_item":"1","_deleted":0}]';
		*/
		$vendorItemsJSON = '[{"inventoryItemID":"1","vendorID":"2","minOrderQuantity":"25","SKU":"ABCDEFG12345","barcode":"1312651726354","BIN":"123412341","_deleted":"0"},'.
							'{"inventoryItemID":"8","vendorID":"3","minOrderQuantity":"15","SKU":"SDKJFHGDSLKF","barcode":"08720945876","BIN":"280943508967","_deleted":"0"}]';			   
		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'addVendorItems';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['Items'] = json_decode($vendorItemsJSON,1);
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}
}

$test = new addVendorItems_test();