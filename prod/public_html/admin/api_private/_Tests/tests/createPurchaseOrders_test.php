<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');

class CreatePurchaseOrders_test extends BaseTest{

    protected $tableColumnName;

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

		$this->connectToDatabase();

		$this->refreshTableSchema('inventory_purchaseorder');

        $tableColumnName = CommonQueries::allColumnsInTable('poslavu_INVENTORY_db','inventory_purchaseorders');

        $values = array();
        $values[] = array(NULL, 1, '2017-03-30 11:13:15', '2017-03-30 12:11:28', 1, 0);
        $values[] = array(NULL, 2, '2017-03-31 12:58:55', '2017-03-31 12:11:25', 1, 0);
        $values[] = array(NULL, 2, '2017-03-31 13:00:59', '2017-03-30 11:13:19', 1, 0);
        $values[] = array(NULL, 1, '2017-03-30 11:13:24', '2017-03-30 12:11:28', 1, 0);
        $values[] = array(NULL, 1, '2017-03-31 13:00:42', '2017-03-31 13:00:37', 1, 0);


        CommonQueries::insertArray('inventory_purchaseorder', $tableColumnName, $values);

        //DBConnection::clientQuery($purchaseOrderInsert);
		//DBConnection::clientQuery($purchaseOrderItemsInsert);
	}

	protected function runTests(){

/*

One or more purchaise order items will be inserted,
One inventory purchase order will be duplicated.
*/


		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'insertPurchaseOrders';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['PurchaseOrder'] = array(6);
 		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);

	}

	protected function tearDown(){

	}

}

$obj = new CreatePurchaseOrders_test();
