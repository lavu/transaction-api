<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');

class CopyInventoryItems_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

		$this->connectToDatabase();

		$this->refreshTableSchema('inventory_items');

		$inventoryItemsInsert = <<<INVENTORY_ITEMS_INSERT
INSERT INTO `inventory_items` (`id`, `name`, `quantity`, `lowQuantity`, `reOrderQuantity`, `sales_unitID`, `categoryID`, `primary_vendor_id`, `storage_location`, `critical_item`, `_deleted`)
VALUES
	(1, 'Chicken Thigh', 30, 7, 12, 1, 1, 0, 1, 1, 0),
	(2, 'Rice', 150, 30, 20, 7, 2, 1, 2, 1, 0),
	(3, 'Sriracha', 20, 10, 10, 3, 3, 2, 3, 1, 0),
	(4, 'Lickin\' Chick\'n', 30, 7, 12, 1, 1, 0, 1, 1, 0),
	(5, 'Rice a roni', 150, 30, 20, 7, 2, 1, 2, 1, 0);
INVENTORY_ITEMS_INSERT;

		DBConnection::clientQuery($inventoryItemsInsert);
	}


	protected function runTests(){

/*

One or more purchaise order items will be inserted,
One inventory purchase order will be duplicated.
*/
		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'copyInventoryItems';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['IDs'] = array(1, 3);
		$apiRequest['Params']['Replaces'] = array();
		$apiRequest['Params']['Replaces'][3] = array('lowQuantity' => 42, 'reOrderQuantity' => 42);
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}

}

$obj = new CopyInventoryItems_test();