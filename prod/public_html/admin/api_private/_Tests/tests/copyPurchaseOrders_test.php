<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');

class CopyPurchaseOrders_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

		$this->connectToDatabase();

		$this->refreshTableSchema('inventory_purchaseorder');
		$this->refreshTableSchema('purchaseorder_items');

		$purchaseOrderInsert = <<<PURCHASEORDERINSERT
INSERT INTO `inventory_purchaseorder` (`id`, `vendorID`, `order_date`, `receive_date`, `received`, `_deleted`)
VALUES
	(1, 0, '2017-03-30 11:13:15', '2017-03-30 12:11:28', 1, 0),
	(2, 1, '2017-03-31 12:58:55', '2017-03-31 12:11:25', 1, 0),
	(3, 2, '2017-03-31 13:00:59', '2017-03-30 11:13:19', 1, 0),
	(4, 1, '2017-03-30 11:13:24', '2017-03-30 12:11:28', 1, 0),
	(5, 1, '2017-03-31 13:00:42', '2017-03-31 13:00:37', 1, 0),
	(6, 0, '2017-03-31 13:00:42', '2017-03-31 13:00:37', 1, 0);
PURCHASEORDERINSERT;


		$purchaseOrderItemsInsert = <<<purchaseOrderItemsInsert
INSERT INTO `purchaseorder_items` (`id`, `SKU`, `purchaseUnitCost`, `quantityOrdered`, `quantityReceived`, `purchaseOrderID`, `vendorItemID`, `purchaseUnitID`, `_deleted`)
VALUES
	(1, 'SKU_1a2B3c4D', 3, 10, 10, 1, 0, 0, 0),
	(2, 'SKU_11223345', 4.5, 50, 500, 1, 1, 1, 0),
	(3, 'SKU_1a2B3c4D', 2.75, 5, 5, 4, 1, 0, 0),
	(4, 'SKU_00001111', 1.25, 30, 30, 1, 2, 6, 0),
	(5, 'SKU_00001111', 1.25, 30, 30, 6, 3, 6, 0),
	(6, 'SKU_00001111', 1.25, 30, 30, 6, 4, 5, 0),
	(7, 'SKU_00001111', 1.25, 30, 30, 6, 5, 4, 0);
purchaseOrderItemsInsert;

		DBConnection::clientQuery($purchaseOrderInsert);
		DBConnection::clientQuery($purchaseOrderItemsInsert);
	}

	protected function runTests(){

/*

One or more purchaise order items will be inserted,
One inventory purchase order will be duplicated.
*/


		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'copyPurchaseOrders';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['IDs'] = array(6);
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);

	}

	protected function tearDown(){

	}

}

$obj = new CopyPurchaseOrders_test();
