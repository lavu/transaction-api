<?php

require_once(__DIR__.'/../../../BaseIntegrationTest.php');



class test_action_getAllManageViewRows extends BaseIntegrationTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		
		/*
		global $inventoryCategoriesInsert, $inventoryItemsInsert, $unitCategoryInsert, $inventoryUnitInsert, $inventoryPurchaseOrderInsert;
		global $purchaseOrderInsert, $storageLocationsInsert, $vendorItemsInsert, $vendersInsert;

		//Clear tables.
		//inventory_categories
		DBConnection::clientQuery("DELETE FROM inventory_categories");
		DBConnection::clientQuery("DELETE FROM inventory_items");
		DBConnection::clientQuery("DELETE FROM unit_category");
		DBConnection::clientQuery("DELETE FROM inventory_unit");
		DBConnection::clientQuery("DELETE FROM inventory_purchaseorder");
		DBConnection::clientQuery("DELETE FROM purchaseorder_items");
		DBConnection::clientQuery("DELETE FROM storage_locations");
		DBConnection::clientQuery("DELETE FROM vendor_items");
		DBConnection::clientQuery("DELETE FROM vendors");
		
		//Insert expected data.
		DBConnection::clientQuery($inventoryCategoriesInsert);
		DBConnection::clientQuery($inventoryItemsInsert);
		DBConnection::clientQuery($unitCategoryInsert);
		DBConnection::clientQuery($inventoryUnitInsert);
		DBConnection::clientQuery($inventoryPurchaseOrderInsert);
		DBConnection::clientQuery($purchaseOrderInsert);
		DBConnection::clientQuery($storageLocationsInsert);
		DBConnection::clientQuery($vendorItemsInsert);
		DBConnection::clientQuery($vendersInsert);
		*/
		$sqlContents = file_get_contents(__DIR__.'/setup.sql');
		$queries = explode(';', $sqlContents);
		foreach($queries as $currQuery){
			//$currQuery = trim($currQuery);
			if(empty($currQuery)){
				continue;
			}
			DBConnection::clientQuery($currQuery);
		}
	}

	protected function runTests(){
		$this->test__action_getAllManageViewRows_01();
	}

	protected function tearDown(){
		/*
		DBConnection::clientQuery("DELETE FROM inventory_items");
		DBConnection::clientQuery("DELETE FROM inventory_purchaseorder");
		DBConnection::clientQuery("DELETE FROM purchaseorder_items");
		DBConnection::clientQuery("DELETE FROM vendor_items");
		*/
	}

	protected function test__action_getAllManageViewRows_01(){

		$requestJSON = <<<JSON_REQUEST
{
	"ApiVersion": 1,
	"Component": "Inventory",
	"Action": "getAllManageViewRows",
	"Auth": {
		"dataname": "API_INTEGRATION_TESTS",
		"user_id": "1"
	},
	"Object": "vendor_items",
	"Params": {
		"Items": []
	}
}
JSON_REQUEST;

		$apiResult = Internal_API::apiQuery($requestJSON);
		echo print_r($apiResult,1);
	}
}


$test = new test_action_getAllManageViewRows();

