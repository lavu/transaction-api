# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: demo.lavu.com (MySQL 5.6.34-79.1)
# Database: poslavu_demoleif_db
# Generation Time: 2017-04-10 18:09:24 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table inventory_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_categories`;

CREATE TABLE `inventory_categories` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `inventory_categories` WRITE;
/*!40000 ALTER TABLE `inventory_categories` DISABLE KEYS */;

INSERT INTO `inventory_categories` (`id`, `name`, `description`, `_deleted`)
VALUES
	(0,'Condiments','Ketchup, mustard, hotsauce, etc. Condiments are given with order.',0),
	(1,'Poultry','Poultry Fowl Chicken, Turkey, etc.',0),
	(2,'Grain','Wheat, Rice, Oat, Cornmeal, Barley or other cereal grains',0),
	(3,'Sauce','Sauces Hot sauce, barbecue sauce, marinades, etc.',0);

/*!40000 ALTER TABLE `inventory_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table inventory_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_items`;

CREATE TABLE `inventory_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `lowQuantity` double DEFAULT NULL,
  `reOrderQuantity` double DEFAULT NULL,
  `purchaseCost_Average` double DEFAULT NULL,
  `purchaseCostSum_FIFO` double DEFAULT NULL,
  `saleCost_Average` double DEFAULT NULL,
  `saleCostSum_FIFO` double DEFAULT NULL,
  `sales_unitID` int(11) DEFAULT NULL,
  `categoryID` int(11) DEFAULT NULL,
  `primary_vendor_id` int(11) DEFAULT NULL,
  `storage_location` int(11) DEFAULT NULL,
  `critical_item` int(11) DEFAULT NULL,
  `_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK` (`categoryID`,`sales_unitID`,`primary_vendor_id`,`storage_location`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `inventory_items` WRITE;
/*!40000 ALTER TABLE `inventory_items` DISABLE KEYS */;

INSERT INTO `inventory_items` (`id`, `name`, `quantity`, `lowQuantity`, `reOrderQuantity`, `purchaseCost_Average`, `purchaseCostSum_FIFO`, `saleCost_Average`, `saleCostSum_FIFO`, `sales_unitID`, `categoryID`, `primary_vendor_id`, `storage_location`, `critical_item`, `_deleted`)
VALUES
	(0,'Chicken Thigh',30,7,12,NULL,35,NULL,NULL,1,1,0,1,1,0),
	(1,'Rice',150,30,20,NULL,70.15,NULL,NULL,7,2,1,2,1,0),
	(2,'Sriracha',20,10,10,NULL,62.32,NULL,NULL,3,3,2,3,1,0),
	(3,'Lickin\' Chick\'n',30,7,12,NULL,35,NULL,NULL,1,1,0,1,1,0),
	(4,'Rice a roni',150,30,20,NULL,70.15,NULL,NULL,7,2,1,2,1,0);

/*!40000 ALTER TABLE `inventory_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table inventory_purchaseorder
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_purchaseorder`;

CREATE TABLE `inventory_purchaseorder` (
  `id` int(11) NOT NULL DEFAULT '0',
  `vendorID` int(11) DEFAULT NULL,
  `order_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `receive_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `received` int(11) DEFAULT NULL,
  `_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK` (`vendorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `inventory_purchaseorder` WRITE;
/*!40000 ALTER TABLE `inventory_purchaseorder` DISABLE KEYS */;

INSERT INTO `inventory_purchaseorder` (`id`, `vendorID`, `order_date`, `receive_date`, `received`, `_deleted`)
VALUES
	(0,0,'2017-03-30 11:13:15','2017-03-30 12:11:28',1,0),
	(1,1,'2017-03-31 12:58:55','2017-03-31 12:11:25',1,0),
	(2,2,'2017-03-31 13:00:59','2017-03-30 11:13:19',1,0),
	(3,1,'2017-03-30 11:13:24','2017-03-30 12:11:28',1,0),
	(4,1,'2017-03-31 13:00:42','2017-03-31 13:00:37',1,0),
	(5,0,'2017-03-31 13:00:42','2017-03-31 13:00:37',1,0);

/*!40000 ALTER TABLE `inventory_purchaseorder` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table inventory_unit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inventory_unit`;

CREATE TABLE `inventory_unit` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `symbol` varchar(255) DEFAULT NULL,
  `categoryID` int(11) DEFAULT NULL,
  `conversion` double DEFAULT NULL,
  `_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK` (`categoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `inventory_unit` WRITE;
/*!40000 ALTER TABLE `inventory_unit` DISABLE KEYS */;

INSERT INTO `inventory_unit` (`id`, `name`, `symbol`, `categoryID`, `conversion`, `_deleted`)
VALUES
	(0,'Kilogram','kg',0,1,0),
	(1,'Pounds','lbs',0,2.20462,0),
	(2,'Liter','L',1,1,0),
	(3,'Ounce','oz',1,33.814,0),
	(4,'Meter','m',2,1,0),
	(5,'Foot','\'',2,3.28084,0),
	(6,'Sauce Bottle','bottles',1,0.25,0),
	(7,'Cups','C',1,4.22675,0);

/*!40000 ALTER TABLE `inventory_unit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table purchaseorder_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `purchaseorder_items`;

CREATE TABLE `purchaseorder_items` (
  `id` int(11) NOT NULL DEFAULT '0',
  `SKU` varchar(255) DEFAULT NULL,
  `purchaseUnitCost` double DEFAULT NULL,
  `quantityOrdered` int(11) DEFAULT NULL,
  `quantityReceived` int(11) DEFAULT NULL,
  `quantityInStock` int(11) NOT NULL DEFAULT '0',
  `purchaseOrderID` int(11) DEFAULT NULL,
  `vendorItemID` int(11) DEFAULT NULL,
  `purchaseUnitID` int(11) DEFAULT NULL,
  `_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK` (`vendorItemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `purchaseorder_items` WRITE;
/*!40000 ALTER TABLE `purchaseorder_items` DISABLE KEYS */;

INSERT INTO `purchaseorder_items` (`id`, `SKU`, `purchaseUnitCost`, `quantityOrdered`, `quantityReceived`, `quantityInStock`, `purchaseOrderID`, `vendorItemID`, `purchaseUnitID`, `_deleted`)
VALUES
	(0,'SKU_1a2B3c4D',3,10,10,10,0,0,0,0),
	(1,'SKU_11223345',4.5,50,500,500,0,1,1,0),
	(2,'SKU_1a2B3c4D',2.75,5,5,5,4,0,0,0),
	(3,'SKU_00001111',1.25,30,30,30,0,2,6,0),
	(4,'SKU_00001111',1.25,30,30,30,5,3,6,0),
	(5,'SKU_00001111',1.25,30,30,30,5,4,5,0),
	(6,'SKU_00001111',1.25,30,30,30,5,5,4,0);

/*!40000 ALTER TABLE `purchaseorder_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table storage_locations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `storage_locations`;

CREATE TABLE `storage_locations` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `storage_locations` WRITE;
/*!40000 ALTER TABLE `storage_locations` DISABLE KEYS */;

INSERT INTO `storage_locations` (`id`, `name`, `description`, `_deleted`)
VALUES
	(0,'Pantry','Dry Storage',0),
	(1,'Freezer','Cold Storage',0),
	(2,'Storage Shack','Non-Perishable Bulk Storage',0),
	(3,'Line-Shelf','Stored at the cooking line',0);

/*!40000 ALTER TABLE `storage_locations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table vendor_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vendor_items`;

CREATE TABLE `vendor_items` (
  `id` int(11) NOT NULL DEFAULT '0',
  `inventoryItemID` int(11) DEFAULT NULL,
  `vendorID` int(11) DEFAULT NULL,
  `minOrderQuantity` double DEFAULT NULL,
  `SKU` varchar(255) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `BIN` varchar(255) DEFAULT NULL,
  `_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK` (`inventoryItemID`,`vendorID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `vendor_items` WRITE;
/*!40000 ALTER TABLE `vendor_items` DISABLE KEYS */;

INSERT INTO `vendor_items` (`id`, `inventoryItemID`, `vendorID`, `minOrderQuantity`, `SKU`, `barcode`, `BIN`, `_deleted`)
VALUES
	(0,0,0,5,'SKU_1a2B3c4D',NULL,NULL,0),
	(1,1,1,4,'SKU_11223345',NULL,NULL,0),
	(2,2,2,10,'SKU_00001111',NULL,NULL,0),
	(3,3,0,10,'SKU_00001112',NULL,NULL,0),
	(4,4,0,10,'SKU_00001112',NULL,NULL,0),
	(5,5,0,10,'SKU_00001112',NULL,NULL,0);

/*!40000 ALTER TABLE `vendor_items` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table vendors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vendors`;

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `contactName` varchar(255) DEFAULT NULL,
  `contactPhone` varchar(255) DEFAULT NULL,
  `contactEmail` varchar(255) DEFAULT NULL,
  `contactFax` varchar(255) DEFAULT NULL,
  `_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `vendors` WRITE;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;

INSERT INTO `vendors` (`id`, `name`, `contactName`, `contactPhone`, `contactEmail`, `contactFax`, `_deleted`)
VALUES
	(0,'Leif\'s MEGA Produce','Leif Guillermo','555-555-5550','leif@lavu.com',NULL,0),
	(1,'Elisa\'s Vegan Superstore','Elisa Valdez','555-555-5551','elisa.valdez@lavu.com',NULL,0),
	(2,'LouAnn\'s Foo-Mart','LouAnn Hunt','555-555-5552','louann.hunt@lavu.com',NULL,0);

/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
