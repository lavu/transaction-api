<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');


class addVendors_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		$this->connectToDatabase();
		$this->refreshTableSchema('vendors');
	}

	protected function runTests(){


		/*
	id	name	contactName	contactPhone	contactEmail	contactFax	_deleted
1	Elisa's Vegan Superstore	Elisa Valdez	555-555-5551	elisa.valdez@lavu.com	NULL	0
2	LouAnn's Foo-Mart	LouAnn Hunt	555-555-5552	louann.hunt@lavu.com	NULL	0
		*/
		$vendorsArr = array();
		$vendorsArr[] = array("name" => "Elisa's Vegan Superstore","contactName" => "Elisa Valdez","contactPhone" => "555-555-5551","contactEmail" => "elisa.valdez@lavu.com");
		$vendorsArr[] = array("name" => "LouAnn's Foo-Mart","contactName" => "LouAnn","contactPhone" => "555-555-5552","contactEmail" => "louann.hunt@lavu.com");
		
		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'addVendors';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['Vendors'] = $vendorsArr;
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}
}

$test = new addVendors_test();



