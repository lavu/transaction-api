<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');

class ArchiveInventoryItems_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

		$this->connectToDatabase();
		DBConnection::clientQuery("DROP TABLE `inventory_items`");

		$showTableResult = DBConnection::adminQuery("SHOW CREATE TABLE `poslavu_INVENTORY_db`.`inventory_items`");
		$showTableArr = mysqli_fetch_assoc($showTableResult);
		DBConnection::clientQuery($showTableArr['Create Table']);
		
		


		$columns = array('id','name','quantity','lowQuantity','reOrderQuantity','sales_unitID','categoryID','primary_vendor_id','storage_location','critical_item','_deleted');
		$values = array();
		$values[] = array(1,'test',1,1,1,1,1,1,1,1,0);
		$values[] = array(2,'test',1,1,1,1,1,1,1,1,0);
		$values[] = array(3,'test',1,1,1,1,1,1,1,1,0);
		$values[] = array(4,'test',1,1,1,1,1,1,1,1,0);
		$values[] = array(5,'test',1,1,1,1,1,1,1,1,0);
		
		//insertArray($tableName, $columnNames, $arrayOfRows)
		$insertResult = CommonQueries::insertArray('inventory_items', $columns, $values);
	}

	protected function runTests(){


		/*
$testRequest = array();
$getObjectByIDRequest['Auth'] = array("dataname" => 'demoleif', 'user_id' => 1);
$testRequest['ApiVersion'];
$testRequest['Action'] = 'addInventoryItems';
$testRequest['Component'] = 'Inventory';
$testRequest['Object'] = 'InventoryItem';
$testRequest['Params'] = array();
$testRequest['Params']["Items"] = array($testItem_01, $testItem_02);
*/
		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'archiveInventoryItems';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['IDs'] = array(1,3,5);
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}

}

$obj = new ArchiveInventoryItems_test();

/*


('id','name','quantity','lowQuantity','reOrderQuantity','sales_unitID','categoryID','primary_vendor_id','storage_location','critical_item','_deleted')



	int(11)	NO	PRI	NULL	auto_increment
	varchar(255)	NO			
	double	NO		NULL	
	double	NO		NULL	
	double	NO		NULL	
	int(11)	NO		NULL	
	int(11)	NO	MUL	NULL	
	int(11)	NO		NULL	
	int(11)	NO		NULL	
	int(11)	NO		NULL	
	tinyint(1)	NO		0	
*/