<?php


require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');


class addStorageLocations_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		$this->connectToDatabase();
		$this->refreshTableSchema('storage_locations');
	}

	protected function runTests(){


		$addStorageArr = array();
		$addStorageArr[] = array('id' => 1, 'description' => 'Pantry	Dry Storage');
		$addStorageArr[] = array('id' => 2, 'description' => 'Freezer	Cold Storage');
		$addStorageArr[] = array('id' => 3, 'description' => 'Storage Shack	Non-Perishable Bulk Storage');
		$addStorageArr[] = array('id' => 4, 'description' => 'Line-Shelf	Stored at the cooking line');


		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'addStorageLocations';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['StorageLocations'] = $addStorageArr;
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}
}

$test = new addStorageLocations_test();