<?php


require_once(__DIR__.'/../../Marshalling/MarshallingQueries.php');


$columns = array('test1','test2','test3');
$rows = array();
$rows[] = array('a','b','c');
$rows[] = array('d','e','f');
$rows[] = array('g','h','i');
$table = 'table';

$sanitizedQuery = MarshallingQueries::buildSanitizedInsert($table, $columns, $rows);
echo $sanitizedQuery."\n";

//public static function buildSanitizedUpdate($table, $setColumnKey2ValueArr, $whereClauseToEnd)
$keyValPair = array('column1' => 'val1', 'column2' => 'val2');
$sanitizedQuery = MarshallingQueries::buildSanitizedUpdate($table, $keyValPair, "where `id` in ('1','2','3')");
echo $sanitizedQuery."\n";