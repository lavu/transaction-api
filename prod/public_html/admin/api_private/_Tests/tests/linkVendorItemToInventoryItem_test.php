<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');


class linkVendorItemToInventoryItem_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		$this->connectToDatabase();
		$this->refreshTableSchema('vendor_items');
		$this->refreshTableSchema('inventory_items');

		$inventoryItemsInsert = <<<INV
INSERT INTO `inventory_items` (`id`, `name`, `quantity`, `lowQuantity`, `reOrderQuantity`, `sales_unitID`, `categoryID`, `primary_vendor_id`, `storage_location`, `critical_item`, `_deleted`)
VALUES
	(1, 'Rice', 150, 30, 20, 7, 2, 1, 2, 1, 0),
	(2, 'Sriracha', 20, 10, 10, 3, 3, 2, 3, 1, 0),
	(3, 'Lickin\' Chick\'n', 30, 7, 12, 1, 1, 0, 1, 1, 0),
	(4, 'Rice a roni', 150, 30, 20, 7, 2, 1, 2, 1, 0);
INV;

		$vendorItemsInsert = <<<VEN
INSERT INTO `vendor_items` (`id`, `inventoryItemID`, `vendorID`, `minOrderQuantity`, `SKU`, `barcode`, `BIN`, `_deleted`)
VALUES
	(1, 1, 1, 4, 'SKU_11223345', '', '', 0),
	(2, 2, 2, 10, 'SKU_00001111', '', '', 0),
	(3, 3, 0, 10, 'SKU_00001112', '', '', 0),
	(4, 4, 0, 10, 'SKU_00001112', '', '', 0),
	(5, 5, 0, 10, 'SKU_00001112', '', '', 0);
VEN;

		DBConnection::clientQuery($inventoryItemsInsert);
		DBConnection::clientQuery($vendorItemsInsert);
	}


	protected function runTests(){
		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'linkVendorItemToInventoryItem';
		//		$vendorItemID = $args['VendorItemID'];
		//		$inventoryItemID = $args['InventoryItemID'];
		$apiRequest['Params'] = array();
		$apiRequest['Params']['VendorItemID'] = 1;
		$apiRequest['Params']['InventoryItemID'] = 4;
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}
}

$test = new linkVendorItemToInventoryItem_test();