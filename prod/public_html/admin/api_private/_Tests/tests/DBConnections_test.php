<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');

class DBConnection_test extends BaseTest{

	private $testConnection;

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		$this->testConnection = DBConnection::startAPITestingConnection();
	}

	protected function runTests(){
		$this->testClientConnection();
		$this->testClientQueries();
		$this->testAdminQueries();
		$this->testSanitize();
	}

	protected function tearDown(){
		DBConnection::clientQuery("Drop table `test_table`");
	}


	private function testClientConnection(){
		$this->assertNotEmpty('DBConnection::startAPITestingConnection() should return a mysqli connection object.', $this->testConnection);
	}


	private function testClientQueries(){
		
		//Create table
		DBConnection::clientQuery("CREATE TABLE IF NOT EXISTS `test_table` ( `id` int(11) NOT NULL AUTO_INCREMENT, `myColumn` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=innodb;");
		
		//Insert
		$result = DBConnection::clientQuery("INSERT INTO `test_table` (`myColumn`) VALUES ('one'), ('two')");
		$this->assertNotEmpty("DBConnection::clientQuery ensuring mysqli result returned on insert", $result);

		//Insert and get insert_id, and affected rows.
		$result = DBConnection::clientQuery("INSERT INTO `test_table` (`myColumn`) VALUES ('three'), ('four')");
		$insertID = DBConnection::clientInsertID() . '';
		$this->assertStringEquals('DBConnection::clientInsertID making sure returns the insert id', '3', $insertID);
		$affectedRows = DBConnection::clientAffectedRows();
		$this->assertStringEquals('DBConnection::clientInsertID making sure returns the insert id', '2', $affectedRows);

		//Test DBError
		DBConnection::clientQuery("Jibberish");
		$errorString = DBConnection::clientDBError();
		$position = strpos($errorString, "Jibberish");
		$this->assertNotEmpty("DBConnection::clientDBError('Jibberish') making sure error reported", $position);


		DBConnection::clientQuery("Drop table `test_table`");
	}

	private function testAdminQueries(){
		
		//Create table
		DBConnection::adminQuery("CREATE TABLE IF NOT EXISTS `poslavu_API_INTEGRATION_TESTS_db`.`test_table` ( `id` int(11) NOT NULL AUTO_INCREMENT, `myColumn` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=innodb;");
		
		//Insert
		$result = DBConnection::adminQuery("INSERT INTO `poslavu_API_INTEGRATION_TESTS_db`.`test_table` (`myColumn`) VALUES ('one'), ('two')");
		$this->assertNotEmpty("DBConnection::adminQuery ensuring mysqli result returned on insert", $result);

		//Insert and get insert_id, and affected rows.
		$result = DBConnection::adminQuery("INSERT INTO `poslavu_API_INTEGRATION_TESTS_db`.`test_table` (`myColumn`) VALUES ('three'), ('four')");
		$insertID = DBConnection::adminInsertID() . '';
		$this->assertStringEquals('DBConnection::adminInsertID making sure returns the insert id', '3', $insertID);
		$affectedRows = DBConnection::adminAffectedRows();
		$this->assertStringEquals('DBConnection::adminInsertID making sure returns the insert id', '2', $affectedRows);
		DBConnection::clientQuery("Drop table `test_table`");

		//Test DBError
		DBConnection::adminQuery("Jibberish");
		$errorString = DBConnection::adminDBError();
		$position = strpos($errorString, "Jibberish");
		$this->assertNotEmpty("DBConnection::adminDBError('Jibberish') making sure error reported", $position);
	}

	private function testSanitize(){
		DBConnection::clientQuery("CREATE TABLE IF NOT EXISTS `test_table` ( `id` int(11) NOT NULL AUTO_INCREMENT, `myColumn` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=innodb;");
		$var = "A'B";
		$sanitized = DBConnection::sanitize($var);
		$this->assertStringEquals("DBConnection::sanitize();", $sanitized, "A\'B");
	}
}

new DBConnection_test();