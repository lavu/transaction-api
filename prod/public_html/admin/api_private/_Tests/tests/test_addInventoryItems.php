<?php


require_once(__DIR__.'/../../../api.php');

//function apiQuery($jsonRequest, $dataname)

/*
$getObjectByIDRequest['Auth'] = array("dataname" => 'demoleif', 'user_id' => 1);
$getObjectByIDRequest['ApiVersion'] = 1;
$getObjectByIDRequest['Component'] = 'Inventory';
$getObjectByIDRequest['Object'] = 'inventory_categories';
$getObjectByIDRequest['Action'] = 'getObjectById';
$getObjectByIDRequest['Params'] = array("id" => 1);
*/

$testItem_01 = array();
$testItem_01['name'] = 'Test';
$testItem_01['quantity'] = 1;
$testItem_01['lowQuantity'] = 1;
$testItem_01['reOrderQuantity'] = 1;
$testItem_01['sales_unitID'] = 1;
$testItem_01['categoryID'] = 1;
$testItem_01['primary_vendor_id'] = 1;
$testItem_01['critical_item'] = 1;

$testItem_02 = array();
$testItem_02['name'] = 'TEST2';
$testItem_02['quantity'] = 2;
$testItem_02['lowQuantity'] = 2;
$testItem_02['reOrderQuantity'] = 2;
$testItem_02['sales_unitID'] = 2;
$testItem_02['categoryID'] = 2;
$testItem_02['primary_vendor_id'] = 2;
$testItem_02['critical_item'] = 1;

$testRequest = array();
$getObjectByIDRequest['Auth'] = array("dataname" => 'demoleif', 'user_id' => 1);
$testRequest['ApiVersion'];
$testRequest['Action'] = 'addInventoryItems';
$testRequest['Component'] = 'Inventory';
$testRequest['Object'] = 'InventoryItem';
$testRequest['Params'] = array();
$testRequest['Params']["Items"] = array($testItem_01, $testItem_02);

$jsonRequest = json_encode($testRequest);
$response = apiQuery($jsonRequest, 'demoleif');
