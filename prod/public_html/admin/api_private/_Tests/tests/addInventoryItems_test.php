<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');


class addInventoryItems_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		$this->connectToDatabase();
		$this->refreshTableSchema('inventory_items');
	}

	protected function runTests(){

		$inventoryItemsJSON = '[{"name":"SpecialSauce","quantity":"2","lowQuantity":"1","reOrderQuantity":"2","sales_unitID":"3","categoryID":"1","primary_vendor_id":"1","storage_location":"1","critical_item":"1","_deleted":0},'.
							   '{"name":"Crackers","quantity":"2","lowQuantity":"1","reOrderQuantity":"2","sales_unitID":"3","categoryID":"1","primary_vendor_id":"1","storage_location":"1","critical_item":"1","_deleted":0}]';
		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'addInventoryItems';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['Items'] = json_decode($inventoryItemsJSON,1);
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}
}

$test = new addInventoryItems_test();