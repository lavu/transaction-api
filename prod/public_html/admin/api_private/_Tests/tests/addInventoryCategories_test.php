<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');


class addInventoryCategories_test extends BaseTest{

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){
		$this->connectToDatabase();
		$this->refreshTableSchema('inventory_categories');
	}

	protected function runTests(){

		$inventoryCategories = array();
		$inventoryCategories[] = array('id' => 1, 'description' =>	'Condiments	Ketchup, mustard, hotsauce, etc. Condiments are given with order.');	
		$inventoryCategories[] = array('id' => 2, 'description' =>	'Poultry	Poultry; Fowl; Chicken, Turkey, etc.');	
		$inventoryCategories[] = array('id' => 3, 'description' =>	'Grain	Wheat, Rice, Oat, Cornmeal, Barley or other cereal grains');	
		$inventoryCategories[] = array('id' => 4, 'description' =>	'Sauce	Sauces; Hot sauce, barbecue sauce, marinades, etc.');	

		$apiRequest = array();
		$apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
		$apiRequest['Auth']['user_id'] = 1;
		$apiRequest['Action'] = 'addInventoryCategories';
		$apiRequest['Params'] = array();
		$apiRequest['Params']['InventoryCategories'] = $inventoryCategories;
		$apiRequest['Component'] = 'Inventory';
		$apiRequestJSON = json_encode($apiRequest);
		Internal_API::apiQuery($apiRequestJSON);
	}

	protected function tearDown(){

	}
}

$test = new addInventoryCategories_test();