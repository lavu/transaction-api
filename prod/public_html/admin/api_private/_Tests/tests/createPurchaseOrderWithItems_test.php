<?php

require_once(__DIR__.'/../BaseTest.php');
require_once(__DIR__.'/../../api_includes.php');
require_once(__DIR__.'/../../api.php');

class createPurchaseOrderWithItems_test extends BaseTest{

    protected $tableColumnName;

	public function __construct(){
		parent::__construct();
	}

	protected function buildUp(){

		$this->connectToDatabase();

		$this->refreshTableSchema('purchaseorder_items');

        $tableColumnName = CommonQueries::allColumnsInTable('poslavu_INVENTORY_db','purchaseorder_items');

        $values = array();
        $values[] = array(NULL, 1, 1, 1,1,10,10,'abc', 0);
        $values[] = array(NULL, 2, 1,2,2,20,20,'abc',  0);
        $values[] = array(NULL, 2, 1,1,1,20,10,'abc9', 0);
        $values[] = array(NULL, 1, 1,1,1,10,10,'abc', 0);
        $values[] = array(NULL, 1, 1,1,1,10,10,'abc',  0);

         CommonQueries::insertArray('inventory_purchaseorder', $tableColumnName, $values);

        //DBConnection::clientQuery($purchaseOrderInsert);
		//DBConnection::clientQuery($purchaseOrderItemsInsert);
	}

	protected function runTests(){

		$purchaseOrderInsert = array('id'=>1, 'vendorID' => 1 'order_date' => '2017-03-31 18:58:55', 'receive_date' => '2017-03-31 18:11:25', 'received' => 1);

		$purchaseOrderItems   = array();
		$purchaseOrderItems[] = array('id'=>1, 'SKU' =>	'SKU_11223345', 'quantityOrdered' => '4.5', 'quantityReceived' =>	'500', 'purchaseOrderID' =>	'', 'vendorItemID' =>	'1', 'purchaseUnitID' => '1');
		$purchaseOrderItems[] = array('id'=>2, 'SKU' =>	'SKU_1a2B3c4D', 'quantityOrdered' => '2.75', 'quantityReceived' =>	'5',   'purchaseOrderID' =>	'', 'vendorItemID' =>	'0', 'purchaseUnitID' => '0');
		$purchaseOrderItems[] = array('id'=>3, 'SKU' =>	'SKU_00001111', 'quantityOrdered' => '1.25', 'quantityReceived' =>	'30',  'purchaseOrderID' =>	'', 'vendorItemID' =>	'2', 'purchaseUnitID' => '6');
		$purchaseOrderItems[] = array('id'=>4, 'SKU' =>	'SKU_00001111', 'quantityOrdered' => '1.25', 'quantityReceived' =>	'30',  'purchaseOrderID' =>	'', 'vendorItemID' =>	'3', 'purchaseUnitID' => '6');
		$purchaseOrderItems[] = array('id'=>5, 'SKU' =>	'SKU_00001111', 'quantityOrdered' => '1.25', 'quantityReceived' =>	'30',  'purchaseOrderID' =>	'', 'vendorItemID' =>	'4', 'purchaseUnitID' => '5');
		$purchaseOrderItems[] = array('id'=>6, 'SKU' =>	'SKU_00001111', 'quantityOrdered' => '1.25', 'quantityReceived' =>	'30',  'purchaseOrderID' =>	'', 'vendorItemID' =>	'5', 'purchaseUnitID' => '4');

        $apiRequest = array();
        $apiRequest['Auth']['dataname'] = 'API_INTEGRATION_TESTS';
        $apiRequest['Auth']['user_id'] = 1;
        $apiRequest['Action'] = 'addPurchaseOrderWithItems';
        $apiRequest['Params'] = array();
        $apiRequest['Params']['PurchaseOrder'] = $purchaseOrderInsert;
        $apiRequest['Params']['PurchaseOrderItems'] = $purchaseOrderItems;
        $apiRequest['Component'] = 'Inventory';
        $apiRequestJSON = json_encode($apiRequest);
        Internal_API::apiQuery($apiRequestJSON);

	}

	protected function tearDown(){

	}

}

$obj = new createPurchaseOrderWithItems_test();