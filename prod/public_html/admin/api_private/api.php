<?php
/*
	JSON Format

	{
		"component": String - Inventory, Customers, CombinedOrder, Billing, etc.
		"function": String - function supported by specified component
		"parameters": Associative Array - All required parameters / argument pairs.  Parameter being the key, argument the value.  May include optional parameters.

	}
*/

require_once(__DIR__. "/../cp/resources/core_functions.php");
isCacheEnabled();
require_once(__DIR__. "/api_includes.php");

class Internal_API{

	public static function apiQuery($jsonRequest){
		APILog::log2Desktop($jsonRequest);
		//TODO Log request
		//TODO Get request id
		
		$requestID = 0; //TODO SET THIS AS THE N'th API REQUEST.
		header("Access-Control-Allow-Origin: *");  // OLO TO DO : see if this affects Inventory 2.0
		header("Access-Control-Allow-Methods: *");
		header("Content-Type: application/json");
		try{

			$isValid = self::validateJSONRequest($jsonRequest, $requestID);

			$requestDecodedData = json_decode($jsonRequest,1);
			$dataname = $requestDecodedData['Auth']['dataname'];
			$componentName = $requestDecodedData["Component"];
			$actionName = $requestDecodedData["Action"];
			//$componentName = ucfirst(strtolower($componentName));  // OLO TO DO : see if this affects Inventory 2.0
			$componentDataController = null;

			if($componentName === 'Accounts'){
				$componentDataController = new AccountDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Billing'){
				$componentDataController = new BillingDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Customers'){
				$componentDataController = new CustomersDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Inventory'){
				$componentDataController = new InventoryDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Enterprise'){
				$componentDataController = new EnterpriseDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Location'){
				$componentDataController = new LocationDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'LavuGiftCard' || $componentName === 'Lavugiftcard'){
				$componentDataController = new LavuGiftCardDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'LavuToGo' || $componentName === 'Lavutogo'){
				$componentDataController = new LavuToGoDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Logging'){
				$componentDataController = new LoggingDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Menu'){
				$componentDataController = new MenuDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Orders'){
				$componentDataController = new OrdersDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Reports'){
				$componentDataController = new ReportsDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Items'){
				$componentDataController = new ItemsDataController($requestDecodedData, $dataname);
			}
			else if($componentName === 'Payment'){
				$componentDataController = new PaymentDataController($requestDecodedData, $dataname);
			}
			else if ($componentName === 'Redis') {
				if (class_exists('RedisDataController')) {
					$componentDataController = new RedisDataController($requestDecodedData, $dataname);
				}
			}
			else{
				throw new APIException('Component specified was either missing or not valid.');
			}

			if(empty($componentDataController)){
				throw new APIException('Component request could not be built');
			}
			$componentDataController->runAPIRequest();
			$returnDataArr = $componentDataController->getProcessedResponse();
			$jsonResponse = json_encode($returnDataArr);
			if(empty($jsonResponse)){
				self::apiFail('No valid component set in api request.'); //TODO SET API REQUEST ID.
			}

			$apiReturn = array();
			$apiReturn['DataResponse'] = $componentDataController->getProcessedResponse();
			$apiReturn['RequestID'] = $requestID;
			$apiReturn['Status'] = "Success";

			// OLO-specific logic that will break other consumers of the internal API, like Inventory 2.0
			if (isset($requestDecodedData['Src']) && $requestDecodedData['Src'] == 'Olo') {
				$apiReturn['DataResponse'] = $apiReturn['DataResponse']['DataResponse'];

				$apiJSONReturn = json_encode($apiReturn);
				if ($apiReturn['DataResponse'] != 'authenticated') {
					echo $apiJSONReturn;
				}
			}
			else {
				if($actionName == 'getAllVendors') {
					$requestvendorcount = InventoryQueries::getAllCount('vendors',0);
					$vendorcount = $requestvendorcount[0]['count'];
					$apiReturn['totalRecords'] = $vendorcount;
					$apiJSONReturn = json_encode($apiReturn);
				} else {
					$apiJSONReturn = json_encode($apiReturn);
				}	
			}
			return $apiJSONReturn;
		}catch(APIException $apiFailException){
			error_log("Internal API exception: ". $apiFailException->getMessage());
			return $apiFailException->jsonFailResponse($requestID);
		}
	}

	private static function validateJSONRequest($jsonRequest, $requestID){
		$requestDecodedData = json_decode($jsonRequest,1);
		if(empty($requestDecodedData)){
			throw new APIException('API JSON request could not be decoded.');
		}
		if(empty($requestDecodedData['Auth']) || empty($requestDecodedData['Auth']['dataname'])){
			throw new APIException('Dataname must be specified in Auth.');
		}
		return true;
	}

}
