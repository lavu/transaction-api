<?php
	if(can_access("customers"))
	{
		ini_set('memory_limit','64M');
		ini_set("display_errors","1");
		//require_once("/home/poslavu/public_html/demo/authnet_functions.php");
		require_once(dirname(__FILE__) ."/billing/authnet_functions.php");
		require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
		require_once("/home/poslavu/public_html/admin/cp/resources/msync_functions.php");

		$alert_message = "";

		function link_signups_to_rests()
		{
			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `restaurantid`=''");
			while($signup_read = mysqli_fetch_assoc($signup_query))
			{
				$dataname = $signup_read['dataname'];
				if($dataname!="")
				{
					$rest_query = mlavu_query("select id as id from `poslavu_MAIN_db`.`restaurants` where `data_name`='[2]' and (`signupid`='' or `signupid`='0')",$signup_read['id'],$dataname);
					while($rest_read = mysqli_fetch_assoc($rest_query))
					{
						$restaurantid = $rest_read['id'];

						mlavu_query("update `poslavu_MAIN_db`.`signups` set `restaurantid`='[1]' where `id`='[2]'",$restaurantid,$signup_read['id']);
						mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `signupid`='[1]' where `data_name`='[2]' and (`signupid`='' or `signupid`='0')",$signup_read['id'],$dataname);
					}
				}
			}
		}

		/*function urlvar($var,$def=false)
		{
			return (isset($_GET[$var]))?$_GET[$var]:$def;
		}
		function postvar($var,$def=false)
		{
			return (isset($_POST[$var]))?$_POST[$var]:$def;
		}*/

		function memvar($var,$def=false)
		{
			$uvar = urlvar($var,"____na____");
			if($uvar=="____na____")
			{
				if(isset($_SESSION[$var]))
					return $_SESSION[$var];
				else
					return $def;
			}
			else
			{
				$_SESSION[$var] = $uvar;
				return $uvar;
			}
		}
		function payment_status_option_list()
		{
			$pslist = array();
			$pslist[] = array("Paid","#008800");
			$pslist[] = array("Paid - Paypal","#00aa00");
			$pslist[] = array("Paid - Check","#00aa00");
			$pslist[] = array("Refunded","#884400");
			$pslist[] = array("Unpaid","#aa0000","Trial","#440000");
			$pslist[] = array("Due", "#aa0000");
			$pslist[] = array("Allow","#000088");
			$pslist[] = array("Dev","#000088");
			$pslist[] = array("Waived","#0000aa");
			return $pslist;
		}
		function optcolor($opt,$in_trial=false)
		{
			$clr = "#000000";
			$options = payment_status_option_list();
			for($i=0; $i<count($options); $i++)
			{
				if($opt==$options[$i][0])
				{
					if($in_trial && isset($options[$i][3]))
						$clr = $options[$i][3];
					else
						$clr = $options[$i][1];
				}
			}
			return $clr;
		}
		function opttitle($opt,$in_trial=false)
		{
			$title = $opt;
			$options = payment_status_option_list();
			for($i=0; $i<count($options); $i++)
			{
				if($opt==$options[$i][0])
				{
					if($in_trial && isset($options[$i][2]))
						$title = $options[$i][2];
				}
			}
			return $title;
		}
		function poslavu_package_list()
		{
			$vars = array();
			$vars[] = array("Silver",1);
			$vars[] = array("Gold",2);
			$vars[] = array("Platinum",3);
			$vars[] = array("Dev",24);
			return $vars;
		}
		function poslavu_package_name($packageid)
		{
			if($packageid==1) return "Silver";
			else if($packageid==2) return "Gold";
			else if($packageid==3) return "Platinum";
			else if($packageid==24) return "Dev";
			else return $packageid;
		}
		function create_tablist($set_link,$tablist,$submode)
		{
			echo "<table>";
			for($i=0; $i<count($tablist); $i++)
			{
				$set_submode = strtolower(str_replace(" ","_",$tablist[$i]));
				if($submode==$set_submode)
					$bgcolor = "#ffffff";
				else
					$bgcolor = "#dddde2";
				echo "<td style='padding:6px; border:solid 2px #777777; cursor:pointer' onclick='window.location = \"".$set_link.$set_submode."\"' bgcolor='$bgcolor'>".$tablist[$i]."</td>";
			}
			echo "</table>";
		}

		$submode = urlvar("submode","listall");
		$listmode = memvar("listmode","search");
		$search_term = urlvar("search_term","");
		$rowid = urlvar("rowid");
		$join = "";
		$as_list = "";

		$tablist = array("Search","All Enabled","All Disabled","Signups","Disabled Signups","Trials");
		if(!isset($_GET['notabs']))
			create_tablist("index.php?mode=manage_customers&submode=listall&listmode=",$tablist,$listmode);

		$search_cols = array("`restaurants`.`disabled`","`restaurants`.`company_name`","`restaurants`.`company_code`","`restaurants`.`email`","`restaurants`.`phone`","`signups`.`company`","`signups`.`firstname`","`signups`.`lastname`","`signups`.`phone`","`signups`.`city`","`signups`.`state`","`signups`.`zip`");
		if($submode=="query")
		{
			require_once(dirname(__FILE__) . "/tools/other.php");
		}
		else if($submode=="convert_product")
		{
			require_once(dirname(__FILE__) . "/tools/convert_product.php");
		}
		else if($submode=="lunch")
		{
			require_once(dirname(__FILE__) . "/tools/lunch.php");
		}
		else if($submode=="clear_empty" || $submode=="clear_action_logs")
		{
			require_once(dirname(__FILE__) . "/tools/clear_empty.php");
		}
		else if($submode=="general_tools")
		{
			require_once(dirname(__FILE__) . "/tools/general_tools.php");
		}
		else if($submode=="listall")
		{
			$orderby = "`id` ASC";
			if($listmode=="all_enabled")
				$cond = "`disabled`!=1";
			else if($listmode=="all_disabled")
				$cond = "`disabled`=1";
			else if($listmode=="search")
			{
				$join = " LEFT JOIN `poslavu_MAIN_db`.`signups` ON `poslavu_MAIN_db`.`restaurants`.`signupid` = `poslavu_MAIN_db`.`signups`.`id`";
				$cond = "`restaurants`.`id`<1";
				$orderby = "`company_name` ASC";
				$esc_search_term = str_replace("'","''",$search_term);

				if (strlen($search_term) >= 3) {

					$cond = "";
					$cond .= "concat(`signups`.`firstname`,' ',`signups`.`lastname`) LIKE '%".$esc_search_term."%'";
					for($s=0; $s<count($search_cols); $s++)
					{
						if($cond!="")
							$cond .= " or ";
						$search_col_name = str_replace("`","",str_replace(".","_",$search_cols[$s]));
						$as_list .= $search_cols[$s] . " as " . $search_col_name . ", ";
						$cond .= $search_cols[$s]." LIKE '%".$esc_search_term."%'";
					}
					$cond .= " OR `restaurants`.`chain_name` LIKE '%".$esc_search_term."%'";

				} else if (strlen($search_term > 0)) {

					$cond = "";
					$cond .= "`signups`.`firstname` = '".$esc_search_term."' OR `signups`.`lastname` = '".$esc_search_term."'";
					for($s=0; $s<count($search_cols); $s++)
					{
						if($cond!="")
							$cond .= " or ";
						$search_col_name = str_replace("`","",str_replace(".","_",$search_cols[$s]));
						$as_list .= $search_cols[$s] . " as " . $search_col_name . ", ";
						$cond .= $search_cols[$s]." = '".$esc_search_term."'";
					}
					$cond .= " OR `restaurants`.`chain_name` = '".$esc_search_term."'";
				}
				//echo "Search coming soon !";
				echo "<script language='javascript'>";
				echo "function search_kp(evt) { ";
				echo "  var evt = (evt) ? evt : ((event) ? event : null); ";
				echo "  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); ";
				echo "  if ((evt.keyCode == 13) && (node.type=='text'))  {submit_search_term(); return false;} ";
				echo "} ";
				echo "function submit_search_term() { ";
				//echo "  if(document.getElementById(\"search_term\").value.length < 3) ";
				//echo "     alert(\"Search term must be at least 3 characters\"); ";
				//echo "  else ";
				echo "     window.location = \"index.php?mode=manage_customers&submode=$submode&listmode=search&search_term=\" + document.getElementById(\"search_term\").value; ";
				echo "} ";
				echo "</script>";
				echo "<br><br><input type='text' name='search_term' id='search_term' onkeypress='return search_kp(event)'> <input type='button' value='Search' onclick='submit_search_term()' class='btn'>";
				echo "<script language='javascript'>";
				echo "document.getElementById('search_term').focus(); ";
				echo "</script>";

				echo "<br><br>";
			}
			else if($listmode=="signups")
			{
				$cond = "`disabled`!=1 and `signupid`>0";
				$orderby = "`company_name` ASC";
			}
			else if($listmode=="disabled_signups")
			{
				$cond = "`disabled`=1 and `signupid`>0";
				$orderby = "`company_name` ASC";
			}
			else if($listmode=="trials")
			{
				link_signups_to_rests();

				$join = " LEFT JOIN `poslavu_MAIN_db`.`signups` ON `poslavu_MAIN_db`.`restaurants`.`signupid` = `poslavu_MAIN_db`.`signups`.`id`";
				$cond = "`disabled`!=1 and `arb_license_start`>='".date("Y-m-d")."' and `signupid`>0";
				$orderby = "`arb_license_start` ASC";
			}
			$cond .= " and `demo`!=1";

			echo "<table><tr><td>&nbsp;</td></tr>";
			echo "<tr style='color:#444444;'><td align='center'><b>Company Name</b></td><td align='center'><b>Chain</b></td><td align='center'><b>Status</b></td><td align='center'><b>Level</b></td><td align='center' width='100px'><b>Last Activity</b></td>";

			if($listmode=="search")
			{
				for($s=0; $s<count($search_cols); $s++)
				{
					$search_col_parts = explode(".",$search_cols[$s]);
					if(count($search_col_parts) > 1)
					{
						$search_col_title = trim($search_col_parts[1],"`");
						$search_col_type = trim($search_col_parts[0],"`");
					}
					else
					{
						$search_col_title = $search_cols[$s];
					}
					echo "<td align='center'><b>" . $search_col_title . "</td>";
				}
			}
			else
			{
				echo "<td align='center' width='200px'><b>Account Type</b> ";
				if(isset($_GET['show_account_type'])) $show_account_type = $_GET['show_account_type'];
				else if(!isset($_SESSION['show_account_type'])) $show_account_type = false;
				else $show_account_type = $_SESSION['show_account_type'];
				$_SESSION['show_account_type'] = $show_acount_type;
				if($show_account_type) echo " <a href='index.php?mode=manage_customers&show_account_type=0'>(close)</a>";
				else echo " <a href='index.php?mode=manage_customers&show_account_type=1'>(open)</a>";

				echo "</td>";
			}
			echo "</tr>";
			echo "<tr><td colspan='25'><hr color='#999999'></td></tr>";
			$cust_query = mlavu_query("SELECT *, ".$as_list."`poslavu_MAIN_db`.`restaurants`.`id` as `id` from `poslavu_MAIN_db`.`restaurants`".$join." where $cond ORDER BY $orderby");
			if(mysqli_num_rows($cust_query))
			{
				while($cust_read = mysqli_fetch_assoc($cust_query))
				{

					$dbname = "poslavu_".$cust_read['company_code']."_db";
					$user_query = mlavu_query("SELECT * from `$dbname`.`users` where `access_level`>=3");
					//if(mysqli_num_rows($user_query))
					//{
						$show_package = $cust_read['package_status'];
						$in_trial_license = false;
						$in_trial_hosting = false;
						$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$cust_read['company_code']);
						if(mysqli_num_rows($signup_query))
						{
							$signup_read = mysqli_fetch_assoc($signup_query);
							if($signup_read['arb_license_start']!="" && date("Y-m-d") < $signup_read['arb_license_start'])
								$in_trial_license = true;
							if($signup_read['arb_hosting_start']!="" && date("Y-m-d") < $signup_read['arb_hosting_start'])
								$in_trial_hosting = true;
							if($show_package=="")
							{
								if($signup_read['package']==1) $show_package = "Silver";
								else if($signup_read['package']==2) $show_package = "Gold";
								else if($signup_read['package']==3) $show_package = "Platinum";
							}
							if($listmode=="trials") $show_package = trim($signup_read['arb_license_start'] . " <font color='#000077'>" . $show_package . "</font>");
						}
						if ($cust_read['test'] == "1") {
							$type = "test";
						} else if ($cust_read['demo'] == "1") {
							$type = "demo";
						} else if ($cust_read['reseller'] == "1") {
							$type = "reseller";
						} else {
							$type = "client";
						}
						$activity_color = "#222222";
						if ($cust_read['last_activity'] >= date("Y-m-d H:i:s", (time() - 432000))) {
							$activity_color = "#588a3a; font-weight:bold;";
						}

						echo "<tr onmouseover='this.bgColor = \"#ccddff\"' onmouseout='this.bgColor = \"#ffffff\"'>";
						echo "<td style='padding-right:6px; cursor:pointer;' onclick='window.location = \"index.php?mode=manage_customers&submode=details&rowid=".$cust_read['id']."\"'>".$cust_read['company_name']."</td>";
						echo "<td style='padding-right:6px;'><font size='-1' color='#555555'>".$cust_read['chain_name']."</font></td>";
						//echo "<td style='padding-right:6px'>".$cust_read['company_code']."</td>";
						echo "<td style='padding-right:6px; color:".optcolor($cust_read['license_status'],$in_trial_license)."'>".opttitle($cust_read['license_status'],$in_trial_license)."</td>";
						echo "<td style='padding-right:6px'>".$show_package."</td>";
						echo "<td style='padding-right:6px' align='center'><span style='font-size:11px; color:$activity_color;'><nobr>".substr($cust_read['last_activity'], 0, 10);
						echo " - created: " . $cust_read['created'] . "</nobr>";
						echo "</span>";
						echo "</td>";
						if($listmode=="search")
						{
							for($s=0; $s<count($search_cols); $s++)
							{
								$search_col_name = str_replace("`","",str_replace(".","_",$search_cols[$s]));
								echo "<td style='padding-right:6px'>" . $cust_read[$search_col_name] . "</td>";
							}
						}
						else
						{
							if($show_account_type)
								//echo "<td style='padding-left:16px'><iframe src='account_type_selector.php?company_id=".$cust_read['id']."&type=$type' width='350px' height='30px' frameborder='0' scrolling='no'></iframe></td>";
								echo "<td style='padding-left:16px'><div id='divcomp".$cust_read['id']."'><script language='javascript'>showSelector(\"comp\", \"".$cust_read['id']."\", \"0\", \"$type\", \"\");</script></td>";
						}
						if($listmode=="trials")
						{
							echo "<td valign='top'>".$cust_read['phone']."</td>";
							echo "<td valign='top'>".str_replace("\n","<br>",$cust_read['notes']);
							echo "</td>";
						}
						echo "</tr>";
					//}
				}

				echo "<tr><td colspan='25'><hr color='#999999'></td></tr>";
			}
			echo "</table>";
		}
		else if($submode=="local")
		{
			require_once(dirname(__FILE__) . "/manage_local.php");
		}
		else if($submode=="details")
		{
			function payment_status_option($option,$value="")
			{
				return "<option value='$option'".(($value==$option)?" selected":"").">$option</option>";
			}
			function payment_status_select($rowid,$type,$value="")
			{
				$str = "";
				$str .= "<select name='status_select_$type' onchange='window.location = \"index.php?mode=manage_customers&submode=details&rowid=$rowid&update_status=".$type."_status&set_status=\" + this.value'>";
				$str .= payment_status_option("",$value);
				if($type=="package")
					$options = poslavu_package_list();
				else
					$options = payment_status_option_list();
				for($i=0; $i<count($options); $i++)
					$str .= payment_status_option($options[$i][0],$value);
				$str .= "</select>";
				return $str;
			}

			$update_status = urlvar("update_status","null");
			$update_pstatus = urlvar("update_pstatus",null);
			$set_status = urlvar("set_status","null");
			$set_pstatus = urlvar("set_pstatus", null);
			$set_notes = postvar("set_notes","null");
			$loginmode = urlvar("loginmode");
			$cplogin = urlvar("cplogin");
			$ccode = urlvar("ccode");

			if($loginmode && $cplogin && $ccode)
			{
				ini_set("display_errors","1");
				execute_cplogin($loggedin,$cplogin,$ccode,$loginmode);
			}

			if (!empty($_POST['set_new_chain_name'])) {
				require_once(dirname(__FILE__).'/manage_chain_functions.php');
				$s_success = create_new_restaurant_chain($_POST['set_new_chain_name'], $rowid, $loggedin);

				if (strpos($s_success, 'success|') === 0) {
					$a_parts = explode('|', $s_success);
					$_GET['set_restaurant_chain'] = $a_parts[1];
					error_log($_GET['set_restaurant_chain']);
				} else if ($s_success == 'failed to create chain') {
					echo "<script langauge='javascript'>alert(\"Error: failed to create new restaurant chain...\");</script>";
				} else if ($s_success == 'duplicate chain exists') {
					echo "<script langauge='javascript'>alert(\"Unable to create restaurant chain. There is already a restaurant chain named ".$_POST['set_new_chain_name']."...\");</script>";
				}
			}

			if (isset($_GET['set_restaurant_chain']) && (is_numeric($_GET['set_restaurant_chain']) || $_GET['set_restaurant_chain']=="")) {
				require_once(dirname(__FILE__).'/manage_chain_functions.php');
				assign_chain_to_restaurant($_GET['set_restaurant_chain'], $rowid);
			}

			$update_signup_code = "";
			$s_fields = array();
			$s_fields['signups'] = array("firstname","lastname","email","phone","address","city","state","zip");
			$s_fields['restaurants'] = array("email","phone");
			if(isset($_POST['signup_update']))
			{
				$update_signup_type = $_POST['signup_update'];

				for($n=0; $n<count($s_fields[$update_signup_type]); $n++)
				{
					$sfield = $s_fields[$update_signup_type][$n];
					$set_value = $_POST["signup_".$sfield];
					if($update_signup_code!="")
						$update_signup_code .= ",";
					$update_signup_code .= "`$sfield`='".str_replace("'","''",$set_value)."'";
				}
			}
			else if($set_notes!="null")
			{
				$update_status = "notes";
				$set_status = $set_notes;
			}
			else if(isset($_GET['cancel_billing']))
			{
				$cancel_arbs = explode(",",$_GET['cancel_billing']);
				for($i=0; $i<count($cancel_arbs); $i++)
				{
					$arbid = $cancel_arbs[$i];

					$vars['refid'] = 16;
					$vars['subscribeid'] = $arbid;
					$cancel_info = manage_recurring_billing("delete",$vars);

					echo "Canceling ARB ID: " . $arbid . "<br>";
				}
				$get_arb = true;
				$get_arb_ids = $_GET['cancel_billing'];
			}
			if($update_status && $set_status!="null")
			{
				$r = mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `[1]`='[2]' where `id`='[3]'",$update_status,$set_status,$rowid);
			}
			else if($update_pstatus && $set_pstatus !== null)
			{
				$r = mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `[1]`='[2]' where `restaurantid`='[3]'", $update_pstatus, $set_pstatus, $rowid);

				// Log billing change
				if ($update_pstatus == 'proposed_billing_change' && ConnectionHub::getConn('poslavu')->affectedRows() > 0)
				{
					$log_event['action'] = 'Set Proposed Billing Change';
					$log_event['details'] = "SA_CP user set Proposed Billing Change to {$set_pstatus}";
					$log_event['datetime'] = date("Y-m-d H:i:s");
					$log_event['restaurantid'] = $rowid;
					$log_event['dataname'] = admin_info('dataname');
					$log_event['username'] = $loggedin;
					$log_event['fullname'] = $loggedin_fullname;
					$log_event['ipaddress'] = isset($_SERVER['X_FORWARDED_FOR']) ? $_SERVER['X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
					$result = mlavu_query("INSERT `poslavu_MAIN_db`.`billing_log` (`datetime`, `dataname`, `restaurantid`, `username`, `fullname`, `ipaddress`, `action`, `details`) VALUES ('[datetime]', '[dataname]', '[restaurantid]', '[username]', '[fullname]', '[ipaddress]', '[action]', '[details]')", $log_event);
					if ($result === false) error_log( __FILE__ ." got MySQL error during billing_log table insert: ". mlavu_dberror() );
				}
			}

			$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$rowid);
			if(mysqli_num_rows($cust_query))
			{
				$cust_read = mysqli_fetch_assoc($cust_query);
				if($update_signup_type=="restaurants" && $update_signup_code!="")
				{
					mlavu_query("update `poslavu_MAIN_db`.`restaurants` set ".$update_signup_code." where `id`='[1]'",$cust_read['id']);
					$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$rowid);
					$cust_read = mysqli_fetch_assoc($cust_query);
				}

				$data_name = $cust_read['data_name'];

				if(isset($_GET['disable_for_testing']) || isset($_GET['enable_for_testing']))
				{
					$use_letter = strtolower(substr($data_name,0,1));
					if(ord($use_letter) >= ord("a") && ord($use_letter) <= ord("z"))
						$tname = "rest_" . $use_letter;
					else
						$tname = "rest_OTHER";
					$da_query = mlavu_query("select AES_DECRYPT(`data_access`,'lavupass') as `daccess`, `id` from `poslavu_MAINREST_db`.`[1]` where `data_name`='[2]'",$tname,$data_name);
					if(mysqli_num_rows($da_query))
					{
						$da_read = mysqli_fetch_assoc($da_query);

						$new_data_access = $da_read['daccess'];
						$new_data_access = str_replace("disabled_","",$new_data_access); // dont let it get double disabled
						if(isset($_GET['disable_for_testing']))
						{
							$new_data_access = "disabled_" . $new_data_access;
							echo "Disabling for testing...<br>";
						}
						else
						{
							echo "Enabling for testing...<br>";
						}
						//echo $da_read['id'] . " - " . $new_data_access . "<br>";
						mlavu_query("update `poslavu_MAINREST_db`.`[1]` set `data_access`=AES_ENCRYPT('[2]','lavupass') where `data_name`='[3]' and `id`='[4]' limit 1",$tname,$new_data_access,$data_name,$da_read['id']);
					}
				}

				if (!is_dir("/home/poslavu/logs/company/".$data_name)) {
					mkdir("/home/poslavu/logs/company/".$data_name,0755);
				}

				$sd_loc = 0;
				$set_disabled = "";
				if (isset($_REQUEST['Disable_location'])) {
					$sd_loc = $_REQUEST['Disable_location'];
					$set_disabled = "1";
				}
				if (isset($_REQUEST['Enable_location'])) {
					$sd_loc = $_REQUEST['Enable_location'];
					$set_disabled = "0";
				}
				if ($set_disabled != "") mlavu_query("UPDATE `poslavu_[1]_db`.`locations` SET `_disabled` = '[2]' WHERE `id` = '[3]'", $data_name, $set_disabled, $sd_loc);

				$duplicate_location_id = (isset($_REQUEST['duplicate_location']))?$_REQUEST['duplicate_location']:0;
				if ($duplicate_location_id != 0) {
					$get_location_info = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`locations` WHERE `id` = '[2]'", $data_name, $duplicate_location_id);
					if (mysqli_num_rows($get_location_info) > 0) {
						$dlocation = mysqli_fetch_assoc($get_location_info);

						$get_menu_name = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`menus` WHERE `id` = '[2]'", $data_name, $dlocation['menu_id']);
						$menu_info = mysqli_fetch_assoc($get_menu_name);

						$create_new_menu = mlavu_query("INSERT INTO `poslavu_[1]_db`.`menus` (`title`, `created_by`) VALUES ('New Menu', 'Lavu Admin')", $data_name);
						$new_menu_id = mlavu_insert_id();

						$dlocation['title'] = $dlocation['title']." COPY";
						$dlocation['id'] = "NULL";
						$old_menu_id = $dlocation['menu_id'];
						$dlocation['menu_id'] = $new_menu_id;
						$q_fields = "";
						$q_values = "";
						$keys = array_keys($dlocation);
						foreach ($keys as $key) {
							if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
							$q_fields .= "`$key`";
							$q_values .= "'[$key]'";
						}
						$dlocation['data_name'] = $cust_read['data_name'];
						$create_new = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`locations` ($q_fields) VALUES ($q_values)", $dlocation);
						$new_loc_id = mlavu_insert_id();

						$not_set_placeholder = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`config` (`location`, `type`, `setting`, `value2`) VALUES ('".$new_loc_id."', 'Not set placeholder', 'Not set', 'Not set')", $dlocation);

						$update_menu_name = mlavu_query("UPDATE `poslavu_[1]_db`.`menus` SET `title` = 'Menu for Location #[2]' WHERE `id` = '[3]'", $data_name,  $new_loc_id, $new_menu_id);
						$create_tables_row = mlavu_query("INSERT INTO `poslavu_[1]_db`.`tables` (`loc_id`) VALUES ('[2]')", $data_name, $new_loc_id);

						$translate_ingredient_category_ids = array();
						$get_ingredient_categories = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`ingredient_categories` WHERE `loc_id` = '[2]'", $data_name, $duplicate_location_id);
						while ($ingc_vars = mysqli_fetch_assoc($get_ingredient_categories)) {
							$old_ingc_id = $ingc_vars['id'];
							$ingc_vars['id'] = "NULL";
							$ingc_vars['loc_id'] = $new_loc_id;
							$q_fields = "";
							$q_values = "";
							$keys = array_keys($ingc_vars);
							foreach ($keys as $key) {
								if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
								$q_fields .= "`$key`";
								$q_values .= "'[$key]'";
							}
							$ingc_vars['data_name'] = $cust_read['data_name'];
							$create_ingredient_category = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`ingredient_categories` ($q_fields) VALUES ($q_values)", $ingc_vars);
							$new_ingc_id = mlavu_insert_id();
							$translate_ingredient_category_ids[$old_ingc_id] = $new_ingc_id;
						}

						$translate_ingredient_ids = array();
						$get_ingredients = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`ingredients` WHERE `loc_id` = '[2]'", $data_name, $duplicate_location_id);
						while ($ing_vars = mysqli_fetch_assoc($get_ingredients)) {
							$old_ing_id = $ing_vars['id'];
							$ing_vars['id'] = "NULL";
							$ing_vars['loc_id'] = $new_loc_id;
							$ing_vars['category'] = $translate_ingredient_category_ids[$ing_vars['category']];
							$ing_vars['qty'] = "0";
							$q_fields = "";
							$q_values = "";
							$keys = array_keys($ing_vars);
							foreach ($keys as $key) {
								if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
								$q_fields .= "`$key`";
								$q_values .= "'[$key]'";
							}
							$ing_vars['data_name'] = $data_name;
							$create_ingredient = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`ingredients` ($q_fields) VALUES ($q_values)", $ing_vars);
							$new_ing_id = mlavu_insert_id();
							$translate_ingredient_ids[$old_ing_id] = $new_ing_id;
						}

						$translate_forced_modifier_list_ids = array();
						$get_forced_modifier_lists = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`forced_modifier_lists` WHERE `menu_id` = '[2]' AND `_deleted` != '1'", $data_name, $old_menu_id);
						while ($f_vars = mysqli_fetch_assoc($get_forced_modifier_lists)) {
							$old_list_id = $f_vars['id'];
							$f_vars['id'] = "NULL";
							$f_vars['menu_id'] = $new_menu_id;
							$q_fields = "";
							$q_values = "";
							$keys = array_keys($f_vars);
							foreach ($keys as $key) {
								if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
								$q_fields .= "`$key`";
								$q_values .= "'[$key]'";
							}
							$f_vars['data_name'] = $data_name;
							$create_list = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`forced_modifier_lists` ($q_fields) VALUES ($q_values)", $f_vars);
							$new_list_id = mlavu_insert_id();
							$translate_forced_modifier_list_ids[$old_list_id] = $new_list_id;

							$get_forced_modifiers = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`forced_modifiers` WHERE `list_id` = '[2]' AND `_deleted` != '1'", $data_name, $old_list_id);
							while ($m_vars = mysqli_fetch_assoc($get_forced_modifiers)) {

								if ($m_vars['ingredients'] != "") {
									$ing_list = explode(",", $m_vars['ingredients']);
									$new_ingredients = "";
									for($i = 0; $i < count($ing_list); $i++) {
										if (strstr($ing_list[$i], "x")) {
											$ing_info = explode("x", $ing_list[$i]);
											if ($new_ingredients != "") { $new_ingredients .= ","; }
											$new_ingredients .= $translate_ingredient_ids[trim($ing_info[0])]." x ".trim($ing_info[1]);
										}
									}
									$m_vars['ingredients'] = $new_ingredients;
								}

								$m_vars['id'] = "NULL";
								$m_vars['list_id'] = $new_list_id;
								$q_fields = "";
								$q_values = "";
								$keys = array_keys($m_vars);
								foreach ($keys as $key) {
									if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
									$q_fields .= "`$key`";
									$q_values .= "'[$key]'";
								}
								$m_vars['data_name'] = $data_name;
								$create_list = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`forced_modifiers` ($q_fields) VALUES ($q_values)", $m_vars);
							}
						}

						$translate_forced_modifier_group_ids = array();
						$get_forced_modifier_groups = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`forced_modifier_groups` WHERE `menu_id` = '[2]' AND `_deleted` != '1'", $data_name, $old_menu_id);
						while ($g_vars = mysqli_fetch_assoc($get_forced_modifier_groups)) {

							if ($g_vars['include_lists'] != "") {
								$id_array = explode("|", $g_vars['include_lists']);
								$new_id_array = array();
								foreach ($id_array as $id) {
									$new_id_array[] = $translate_forced_modifier_list_ids[$id];
								}
								$g_vars['include_lists'] = implode("|", $new_id_array);
							}

							$old_group_id = $g_vars['id'];
							$g_vars['id'] = "NULL";
							$g_vars['menu_id'] = $new_menu_id;
							$q_fields = "";
							$q_values = "";
							$keys = array_keys($g_vars);
							foreach ($keys as $key) {
								if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
								$q_fields .= "`$key`";
								$q_values .= "'[$key]'";
							}
							$g_vars['data_name'] = $data_name;
							$create_group = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`forced_modifier_groups` ($q_fields) VALUES ($q_values)", $g_vars);
							$new_group_id = mlavu_insert_id();
							$translate_forced_modifier_group_ids[$old_group_id] = $new_group_id;
						}

						$old_list_ids = array_keys($translate_forced_modifier_list_ids);
						$old_group_ids = array_keys($translate_forced_modifier_group_ids);
						foreach ($translate_forced_modifier_list_ids as $new_list_id) {
							foreach ($old_list_ids as $old_list_id) {
								$update_detours = mlavu_query("UPDATE `poslavu_[1]_db`.`forced_modifiers` SET `detour` = 'l[2]' WHERE `detour` = 'l[3]' AND `list_id` = '[4]'", $data_name, $translate_forced_modifier_list_ids[$old_list_id], $old_list_id, $new_list_id);
							}
							foreach ($old_group_ids as $old_group_id) {
								$update_detours = mlavu_query("UPDATE `poslavu_[1]_db`.`forced_modifiers` SET `detour` = 'g[2]' WHERE `detour` = 'g[3]' AND `list_id` = '[4]'", $data_name, $translate_forced_modifier_group_ids[$old_group_id], $old_group_id, $new_list_id);
							}
						}

						$translate_optional_modifier_category_ids = array();
						$get_optional_modifier_categories = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`modifier_categories` WHERE `menu_id` = '[2]' AND `_deleted` != '1'", $cust_read['data_name'], $old_menu_id);
						while ($o_vars = mysqli_fetch_assoc($get_optional_modifier_categories)) {
							$old_category_id = $o_vars['id'];
							$o_vars['id'] = "NULL";
							$o_vars['menu_id'] = $new_menu_id;
							$q_fields = "";
							$q_values = "";
							$keys = array_keys($o_vars);
							foreach ($keys as $key) {
								if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
								$q_fields .= "`$key`";
								$q_values .= "'[$key]'";
							}
							$o_vars['data_name'] = $cust_read['data_name'];
							$create_list = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`modifier_categories` ($q_fields) VALUES ($q_values)", $o_vars);
							$new_category_id = mlavu_insert_id();
							$translate_optional_modifier_category_ids[$old_category_id] = $new_category_id;

							$get_optional_modifiers = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`modifiers` WHERE `category` = '[2]' AND `_deleted` != '1'", $cust_read['data_name'], $old_category_id);
							while ($m_vars = mysqli_fetch_assoc($get_optional_modifiers)) {

								if ($m_vars['ingredients'] != "") {
									$ing_list = explode(",", $m_vars['ingredients']);
									$new_ingredients = "";
									for($i = 0; $i < count($ing_list); $i++) {
										if (strstr($ing_list[$i], "x")) {
											$ing_info = explode("x", $ing_list[$i]);
											if ($new_ingredients != "") { $new_ingredients .= ","; }
											$new_ingredients .= $translate_ingredient_ids[trim($ing_info[0])]." x ".trim($ing_info[1]);
										}
									}
									$m_vars['ingredients'] = $new_ingredients;
								}

								$m_vars['id'] = "NULL";
								$m_vars['category'] = $new_category_id;
								$q_fields = "";
								$q_values = "";
								$keys = array_keys($m_vars);
								foreach ($keys as $key) {
									if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
									$q_fields .= "`$key`";
									$q_values .= "'[$key]'";
								}
								$m_vars['data_name'] = $cust_read['data_name'];
								$create_list = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`modifiers` ($q_fields) VALUES ($q_values)", $m_vars);
								$new_mod_id = mlavu_insert_id();
							}
						}

						$translate_menu_group_ids = array();
						$get_menu_groups = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`menu_groups` WHERE `menu_id` = '[2]'", $cust_read['data_name'], $old_menu_id);
						while ($g_vars = mysqli_fetch_assoc($get_menu_groups)) {
							$old_group_id = $g_vars['id'];
							$g_vars['id'] = "NULL";
							$g_vars['menu_id'] = $new_menu_id;
							$q_fields = "";
							$q_values = "";
							$keys = array_keys($g_vars);
							foreach ($keys as $key) {
								if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
								$q_fields .= "`$key`";
								$q_values .= "'[$key]'";
							}
							$g_vars['data_name'] = $cust_read['data_name'];
							$create_group = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`menu_groups` ($q_fields) VALUES ($q_values)", $g_vars);
							$new_group_id = mlavu_insert_id();
							$translate_menu_group_ids[$old_group_id] = $new_group_id;
						}

						$get_categories = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`menu_categories` WHERE `menu_id` = '[2]'", $cust_read['data_name'], $old_menu_id);
						while ($c_vars = mysqli_fetch_assoc($get_categories)) {
							$old_category_id = $c_vars['id'];
							$c_vars['id'] = "NULL";
							$c_vars['menu_id'] = $new_menu_id;
							$c_vars['group_id'] = $translate_menu_group_ids[$c_vars['group_id']];
							$c_vars['last_modified_date'] = date("Y-m-d H:i:s");
							if ($c_vars['modifier_list_id'] != "") {
								$c_vars['modifier_list_id'] = $translate_optional_modifier_category_ids[$c_vars['modifier_list_id']];
							}
							if ($c_vars['forced_modifier_group_id'] != "") {
								if (strstr($c_vars['forced_modifier_group_id'], "f")) {
									$c_vars['forced_modifier_group_id'] = "f".$translate_forced_modifier_list_ids[str_replace("f", "", $c_vars['forced_modifier_group_id'])];
								} else {
									$c_vars['forced_modifier_group_id'] = $translate_forced_modifier_group_ids[$c_vars['forced_modifier_group_id']];
								}
							}
							$q_fields = "";
							$q_values = "";
							$keys = array_keys($c_vars);
							foreach ($keys as $key) {
								if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
								$q_fields .= "`$key`";
								$q_values .= "'[$key]'";
							}
							$c_vars['data_name'] = $cust_read['data_name'];
							$copy_category = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`menu_categories` ($q_fields) VALUES ($q_values)", $c_vars);
							$new_category_id = mlavu_insert_id();
							$get_items = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`menu_items` WHERE `menu_id` = '[2]' AND `category_id` = '[3]'", $cust_read['data_name'], $old_menu_id, $old_category_id);
							while ($i_vars = mysqli_fetch_assoc($get_items)) {
								$i_vars['id'] = "NULL";
								$i_vars['category_id'] = $new_category_id;
								$i_vars['menu_id'] = $new_menu_id;
								$i_vars['last_modified_date'] = date("Y-m-d H:i:s");
								if ($i_vars['modifier_list_id'] != "") {
									$i_vars['modifier_list_id'] = $translate_optional_modifier_category_ids[$i_vars['modifier_list_id']];
								}
								if ($i_vars['forced_modifier_group_id'] != "") {
									if (strstr($i_vars['forced_modifier_group_id'], "f")) {
										$i_vars['forced_modifier_group_id'] = "f".$translate_forced_modifier_list_ids[str_replace("f", "", $i_vars['forced_modifier_group_id'])];
									} else {
										$i_vars['forced_modifier_group_id'] = $translate_forced_modifier_group_ids[$i_vars['forced_modifier_group_id']];
									}
								}

								if ($i_vars['ingredients'] != "") {
									$ing_list = explode(",", $i_vars['ingredients']);
									$new_ingredients = "";
									for($i = 0; $i < count($ing_list); $i++) {
										if (strstr($ing_list[$i], "x")) {
											$ing_info = explode("x", $ing_list[$i]);
											if ($new_ingredients != "") { $new_ingredients .= ","; }
											$new_ingredients .= $translate_ingredient_ids[trim($ing_info[0])]." x ".trim($ing_info[1]);
										}
									}
									$i_vars['ingredients'] = $new_ingredients;
								}

								$q_fields = "";
								$q_values = "";
								$keys = array_keys($i_vars);
								foreach ($keys as $key) {
									if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
									$q_fields .= "`$key`";
									$q_values .= "'[$key]'";
								}
								$i_vars['data_name'] = $cust_read['data_name'];
								$copy_item = mlavu_query("INSERT INTO `poslavu_[data_name]_db`.`menu_items` ($q_fields) VALUES ($q_values)", $i_vars);
							}
						}
					}
				}
				$kart_component_present = false;
				$comp_present_query = mlavu_query("select id from `poslavu_[1]_db`.`locations` where `component_package`=1",$cust_read['data_name'], null, null, null, null, null, false);
				if(mysqli_num_rows($comp_present_query))
				{
					$kart_component_present = true;
				}

								//--- create new chain popup - START ---//

				echo "<div id='main_content_area'>";

				function js_setvar($varname,$varvalue)
				{
					echo "var $varname = \"".str_replace("\"","&quot;",$varvalue)."\"; ";
				}

				?>

				<script language="javascript">

					<?php
						js_setvar("current_datetime",date("Y-m-d H:i:s"));
						js_setvar("show_datetime",date("m/d h:i a"));
						js_setvar("current_loggedin_fullname",$loggedin_fullname);
					?>

					function create_new_chain(the_field, current_value, Aurl) {

						the_field.value = current_value;

						var str = "";
						str += "<form name='new_chain_form' method='post' action='" + Aurl + "\'>";
						str += "<span style='font-family:Verdana,Arial; font-size:12px'>";
						str += "<b>Create New Restaurant Chain</b>";
						str += "<br>at " + show_datetime + " by " + current_loggedin_fullname;
						str += "<br><br>";
						str += "<input type='text' name='set_new_chain_name' id='set_new_chain_name' size='45'>";
						str += "<br><br>";
						str += "<input type='button' value='Submit' onclick='document.new_chain_form.submit();'>";
						str += "<br><br>";
						str += "</span>";
						str += "</form>";

						show_info2(str);
						document.getElementById('set_new_chain_name').focus();
					}

				</script>

				<?php

				require_once(dirname(__FILE__) . "/billing/floating_window.php");
				echo create_info_layer2(400,160);
				echo "</div>";

				//--- create new chain popup - END ---//

				//echo "<style>";
				//echo ".label { text-align:right; color:#999999; vertical-align:top; } ";
				//echo "</style>";
				echo "<br><a href='index.php?mode=manage_customers'><< Customer List</a><br>";
				echo "<form name='form1' method='post' action='index.php?mode=manage_customers&submode=details&rowid=$rowid'>";
				echo "<table cellpadding=12><td style='border:solid 1px black' valign='top'>";

				echo "<div style='float:left'>";
				echo "<table>";
				echo "<tr><td class='label label-info'>Company Name: </td><td width='300'>" . $cust_read['company_name'] . "</td></tr>";
				echo "<tr><td class='label label-info'>Data Name: </td><td>" . $cust_read['company_code'] . "</td></tr>";
				echo "<tr><td class='label label-info'>Account Created: </td><td>" . $cust_read['created'] . "</td></tr>";
				echo "<tr><td class='label label-info'>Last Activity: </td><td>" . $cust_read['last_activity'] . "</td></tr>";
				echo "<tr><td class='label label-info'>Chain: </td><td><select onchange='if (this.value == \"0\") { create_new_chain(this, \"".$cust_read['chain_id']."\", \"index.php?mode=manage_customers&submode=details&rowid=$rowid\"); } else { window.location = \"index.php?mode=manage_customers&submode=details&rowid=$rowid&set_restaurant_chain=\" + this.value; }'>";
				echo "<option value=''> - - None - - </option>";
				echo "<option value='0'>Create New Chain</option>";
				$get_chains = mlavu_query("SELECT `id`, `name` FROM `poslavu_MAIN_db`.`restaurant_chains` ORDER BY `name` ASC", null, null, null, null, null, null, false);
				while ($chain_info = mysqli_fetch_assoc($get_chains)) {
					$selected = "";
					if ($chain_info['id'] == $cust_read['chain_id']) $selected = " SELECTED";
					echo "<option value='".$chain_info['id']."'$selected>".$chain_info['name']."</option>";
				}
				echo "</select></td></tr>";
				echo "</table>";

				// DO NOT display notes area if blank (use newer note loggin below)
				// nevermind that, don't display the old textarea at all!!!!
				/*if ( $cust_read['notes'] != '' ) {

					echo "<br>Notes:";
					echo "<br><textarea rows='6' cols='60' name='set_notes'>".$cust_read['notes']."</textarea>";
					echo "<br><input type='button' value='Save Notes' onclick='document.form1.submit()'>";
				}*/


				echo "<br>Distro Code: <input type='text' name='distro_code' id='distro_code' value=\"".str_replace("\"","&qout;",$cust_read['distro_code'])."\"><input type='button' value='Save Distro Code' onclick='window.location = \"index.php?mode=manage_customers&submode=details&rowid=$rowid&update_status=distro_code&set_status=\" + document.getElementById(\"distro_code\").value'>";
				if(can_access("modules"))
				{
					require_once(dirname(__FILE__)."/../lib/modules.php");
					//$cust_read['package_status'];
					//$cust_read['modules']

					$modules = $cust_read['modules'];
					if(isset($_GET['set_modules']) && str_ireplace("_", ".", $_GET['set_modules']) != $modules)
					{ //Update the query
						$update_modules_to = str_ireplace("_", ".", $_GET['set_modules']);

						mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `modules`='[1]' WHERE `id`='[2]' AND `data_name`='[3]'", $update_modules_to, $cust_read['id'], $cust_read['data_name']);

						// save modules info in config table so it's available to LLS
						$get_location_ids = mlavu_query("SELECT `id` FROM `poslavu_".$cust_read['data_name']."_db`.`locations`");
						while ($linfo = mysqli_fetch_assoc($get_location_ids)) {
							$check_config = mlavu_query("SELECT `id` FROM `poslavu_".$cust_read['data_name']."_db`.`config` WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting` = 'modules'", $linfo['id']);
							if (mysqli_num_rows($check_config) > 0) {
								$cinfo = mysqli_fetch_assoc($check_config);
								$update_config = mlavu_query("UPDATE `poslavu_".$cust_read['data_name']."_db`.`config` SET `value` = '[1]' WHERE `id` = '[2]'", $update_modules_to, $cinfo['id']);
							} else $insert_config = mlavu_query("INSERT INTO `poslavu_".$cust_read['data_name']."_db`.`config` (`location`, `type`, `setting`, `value`) VALUES ('[1]', 'location_config_setting', 'modules', '[2]')", $linfo['id'], $update_modules_to);
						}

						$modules = $update_modules_to;
					}

					$module_array = array("*" => "*");
					foreach($available_modules as $key => $mod)
					{
						$sections = explode(".", $mod);
						$acc = "";
						for($i = 0; $i < count($sections); $i++)
						{
							if($acc != "")
								$acc .= ".";
							$acc .= $sections[$i];
							if($i < (count($sections) - 1))
								$module_array[$acc . ".*"] = $acc . ".*";
						}
						$module_array[$mod] = $mod;
					}
					echo "<br /><br />Module List:<br />";
					echo "<input id=\"modules\" type=\"hidden\" value=\"" . $modules . "\" style=\"width: 250px;\" onchange=\"updateModuleList($(this).val()));\" />";
					echo "<div id='module_list' style='width: 250px; overflow: auto;'>";
					echo "<pre>";
					$mods = explode(",", $modules);
					for($i = 0; $i < count($mods); $i++)
					{
						echo $mods[$i] . "\n";
					}
					echo "</pre>";
					echo "</div>";
					echo "<input type='button' onclick='window.location=\"index.php?mode=manage_customers&submode=details&rowid=".$rowid."&set_modules=\" + encodeURIComponent(document.getElementById(\"modules\").value)' value='Save Modules'>";
					echo "<div style='width: 400px; height: 100px; overflow: auto;'>";
						foreach($module_array as $mod)
						{
							echo "<div onclick='enableModule(\"$mod\");' style='text-align: center; border: 1px solid black; background: #bbbbbb; display: inline-block; width: 15px; height: 15px;' >+</div>";
							echo "<div onclick='disableModule(\"$mod\");' style='text-align: center; border: 1px solid black; background: #bbbbbb; display: inline-block; width: 15px; height: 15px;' >-</div>";
							echo "<div onclick='deleteModule(\"$mod\");' style='text-align: center; border: 1px solid black; background: #bbbbbb; display: inline-block; width: 15px; height: 15px;' >D</div>";
							echo "&nbsp;<label>$mod</label><br />";
						}
					echo "</div>";

										echo '
					<script type="text/javascript" />

						function modifyModule(str, symbol)
						{
							var parts = str.split(".");
							var result = "";
							for(var i = 0; i < parts.length; i++)
							{
								if(i > 0)
									result += ".";
								if(i === parts.length-1)
									result += symbol;
								result += parts[i];
							}

							return result;
						}

						function addModule(str, symbol)
						{
							deleteModule(str);
							var mods = $("#modules").val().split(",");
							mods.push(modifyModule(str, symbol));
							mods.sort();
							var result = "";
							for(var i = 0; i < mods.length; i++)
							{
								if(result !== "")
									result += ",";
								result += mods[i];
							}
							$("#modules").val(result);

							updateModuleList($("#modules").val());
						}

						function enableModule(str)
						{
							addModule(str, "+");
						}

						function disableModule(str)
						{
							addModule(str, "-");
						}

						function deleteModule(str)
						{
							var mods = $("#modules").val().split(",");
							var result = "";
							var str1 = str.replace("-","").replace("\\\\+","");

							if(mods[0] === "")
								return;

							if(str1.charAt(str1.length - 1) === "*")
							{
								str1 = str1.substr(0, str1.length-1) + "\\\\*";
							}

							for(var i = 0; i < mods.length; i++)
							{
								var enabled = true;
								var reg = new RegExp("^" + str1);
								var orig = mods[i];
								var mod = mods[i].replace("-","").replace("\\+","");

								if(mods[i].match("-"))
								{
									enabled = false;
									//mods[i] = mods[i].replace("-","");

								}
								else if(mods[i].match("\\\\+"))
								{
									enabled = true;
									//mods[i] = mods[i].replace("\\+","");
								}

								if(!(reg.exec(mod) &&  reg.exec(mod)[0] === mod))
								{
									if(result !== "")
										result += ",";

									result += orig;
								}
							}
							$("#modules").val(result);
							updateModuleList($("#modules").val());
						}

						function updateModuleList(str)
						{
							var mods = str;
							var result = "<pre>";
							result += str.replace(/,/g,"\n");
							result += "</pre>";

							$("#module_list").html(result);
						}
					</script>
					';

					//echo "<pre>" . print_r($_SESSION, true) . "</pre>";
				}

				$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$cust_read['id'], null, null, null, null, null, false);
				$status_read = mysqli_fetch_assoc($status_query);
				if (mysqli_num_rows($status_query))
				{
					if($status_read['trial_end']!="" && $status_read['license_applied']=="")
					{
						if(isset($_GET['extend_trial']) && is_numeric($_GET['extend_trial']) && $_GET['extend_trial'] > 0)
						{
							echo "<script language='javascript'>";
							$extend_parts = explode(" ",$status_read['trial_end']);
							$extend_parts = explode("-",$extend_parts[0]);
							if(count($extend_parts) > 2)
							{
								$new_extend_ts = mktime(0,0,0,$extend_parts[1],$extend_parts[2] + 3,$extend_parts[0]);
								$new_extend_datetime = date("Y-m-d H:i:s",$new_extend_ts);

								mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `trial_end`='[1]' where `id`='[2]' limit 1",$new_extend_datetime,$status_read['id']);
							}

							echo "window.location.replace(\"index.php?mode=manage_customers&submode=details&rowid=".$cust_read['id']."\"); ";
							echo "</script>";
							exit();
						}

						echo "<br>In Trial - Expires " . $status_read['trial_end'] . " <a style='cursor:pointer; color:#000077' onclick='if(confirm(\"Are you sure you want to extend this trial for 3 days?\")) window.location = \"index.php?mode=manage_customers&submode=details&rowid=".$cust_read['id']."&extend_trial=3\"'>(extend 3 days)</a>";
					}
				}

				echo "<br><br><a href='index.php?mode=$mode&submode=$submode&rowid=$rowid&loginmode=cp&cplogin=".$cust_read['id']."&ccode=".$cust_read['company_code']."' target='_blank'>Client Control Panel</a>";
				echo "<br><br><a href='index.php?mode=customers&submode=$submode&loginmode=create_temp&cplogin=".$cust_read['id']."&ccode=".$cust_read['company_code']."' target='_blank'>Create Temp Account</a>";
				echo "<br><br><a href='index.php?mode=$mode&submode=search_logs&rowid=$rowid'>Search Logs</a>";

				//if(can_access("technical")) {
					//echo "<br><br><a href='index.php?mode=$mode&submode=turn_on_tunnel&rowid=$rowid'>Turn on LS Ipad Tunnel</a>";
					echo "<br><br><a href='index.php?mode=$mode&submode=sync_database&rowid=$rowid'>Sync Database Structure</a>";
					echo "<br><br><a href='index.php?mode=$mode&submode=optimize_indexes&rowid=$rowid'>Optimize by Indexing</a>";
					echo "<br><br><a href='index.php?mode=$mode&submode=convert_to_innodb&rowid=$rowid'>Convert to InnoDB</a>";
					if($kart_component_present || isset($_GET['kart']))
					{
						echo "<br><br><a href='index.php?mode=$mode&submode=set_deposit_status&rowid=$rowid'>Set Deposit Status for Transactions</a>";
						echo "<br><br><a href='index.php?mode=$mode&submode=sync_database&rowid=$rowid&comps=24&from=DEVKART&dbt=/home/poslavu/public_html/admin/cp/components/lavukart/tables.dbt'>Sync Kart Structure</a>";
					}
					echo "<br><br><a href='index.php?mode=$mode&submode=clear_logs&rowid=$rowid'>Clear Logs older than 1 Week</a>";
					echo "<br><br><a href='index.php?mode=$mode&submode=user_restore&rowid=$rowid&dataname={$cust_read['data_name']}'>User Restore Tool</a>";
					echo "<br><br><a href='index.php?mode=$mode&submode=revert_inv_1&rowid=$rowid&dataname={$cust_read['data_name']}'>Revert to INV 1.0</a>";
					echo "<br><br><a href='index.php?mode=$mode&submode=old_inventory_ingredients&rowid=$rowid&dataname={$cust_read['data_name']}'>Old Inventory Ingredients</a>";
				//}

				// NOTES AREA (for accounts)
				echo "<h4>Account Notes</h4>";
				echo "<div style='float:left; display:block;'>";
				echo "<form name='notes_form' method='post' action='index.php?mode=manage_customers&submode=details&rowid=$rowid'>";
				echo "<input type='hidden' name='support_ninja' value='".$_SESSION['posadmin_loggedin']."' />";

				echo "<textarea name='notes_message' rows='2' cols='70'></textarea><br />";
				echo "<label for='ticket_id'>Ticket ID:</label><input type='text' name='ticket_id'>&nbsp;<input type='submit' name='sub' value='Save Note' onclick='document.notes_form.submit()' />";
				echo "</form>";
				echo "</div>";

				// query for notes
				$notes_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurant_notes` WHERE `restaurant_id`=[1] ORDER BY id DESC",$cust_read['id'], null, null, null, null, null, false);

				if(mysqli_num_rows($notes_query))
				{

					echo "<div style='display:block; float:left; clear:both; width:700px; height:300px; margin:10px 0; overflow:auto;'>";
					echo "<table cellpadding='2' cellspacing='0'>";
					echo "<tr>";
					echo "<th>Note</th>";
					echo "<th>Created On</th>";
					echo "<th>Ticket ID</th>";
					echo "<th>Support Ninja</th>";
					echo "</tr>";

					$i = 1;
					while( $notes_read = mysqli_fetch_assoc($notes_query) ) {

						if ( $i % 2 == 0 )
							$row_color = "#ececec";
						else
							$row_color = "white";

						echo "<tr bgcolor='$row_color'>";
						echo "<td>".$notes_read['message']."</td>";
						echo "<td>".$notes_read['created_on']."</td>";
						echo "<td><a href='http://support.poslavu.com/scp/index.php?query=".$notes_read['ticket_id']."' target='_blank'>".$notes_read['ticket_id']."</a></td>";
						echo "<td>".$notes_read['support_ninja']."</td>";
						echo "</tr>";

						$i++; //increment counter
					}

					echo "</table>";
					echo "</div>";

				}

				if ( $_POST['notes_message'] ) {

					// TODO
					// insert notes into table
					mlavu_query("INSERT INTO `poslavu_MAIN_db`.`restaurant_notes` (`restaurant_id`,`message`,`support_ninja`,`ticket_id`,`created_on`) VALUES ('[1]','[2]','[3]','[4]',now())",$cust_read['id'],$_POST['notes_message'],$_POST['support_ninja'],$_POST['ticket_id']);

					echo "<script type='text/javascript'>window.location.replace(\"index.php?mode=manage_customers&submode=details&rowid=$rowid\");</script>";
				}

				echo "</div>";

				if($update_signup_type=="signups" && $update_signup_code!="")
				{
					mlavu_query("update `poslavu_MAIN_db`.`signups` set ".$update_signup_code." where `dataname`='[1]'",$cust_read['company_code']);
				}

				echo "<div style='float:left; padding-left:20px; display:block;'>";
				echo "<table>";
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$cust_read['company_code'], null, null, null, null, null, false);
				if(mysqli_num_rows($signup_query))
				{
					$signup_read = mysqli_fetch_assoc($signup_query);

					$signup_read = lookup_arb_info($signup_read,$cust_read['company_code']);

					if(isset($_GET['extend_billing']))
					{
						$result = manage_extend_billing($signup_read['id'],$_GET['extend_billing']);
						if($result)
						{
							$signup_read = $result;
							echo "Extension successful";
						}
						else
						{
							echo "Failed to extend";
						}
					}

					$full_address = trim($signup_read['address'] . "<br>" . $signup_read['city'] . " " . $signup_read['state'] . "<br>" . $signup_read['zip']);
					$full_name = trim($signup_read['firstname'] . " " . $signup_read['lastname']);
					$email = $signup_read['email'];
					$phone = $signup_read['phone'];
					$signupid = $signup_read['id'];

					if(can_access("technical"))
					{
						echo "<tr><td colspan=2><a target='_blank' href='https://admin.poslavu.com/sa_cp/billing/index.php?dn=".$cust_read['data_name']."'>View Billing Status</a></td></tr>";
					}
					echo "<tr><td class='label label-info'>License:</td><td>".$signup_read['arb_license_status'];
					if($signup_read['package']!="") echo " (" . poslavu_package_name($signup_read['package']) . ")";
					echo "</td></tr>";
					echo "<tr><td class='label label-info'>Starts:</td><td>";

					echo "<table cellspacing=0 cellpadding=0><td valign='top'>";
					echo $signup_read['arb_license_start'];
					echo "</td><td width='40'>&nbsp;</td><td>";
					if($signup_read['arb_license_start'] > date("Y-m-d"))
						echo "<table bgcolor='#bbbbbb' style='border:solid 1px #888888' cellspacing=1 cellpadding=1><td style='padding-left:6px; padding-right:12px'><input type='button' onclick='ext_days = document.getElementById(\"extend_count\").value; if(ext_days==\"\") alert(\"Choose how many days to extend first dude!\"); else {if(confirm(\"Are you sure you want to extend by 5 days?\")) window.location = \"index.php?mode=manage_customers&submode=details&rowid=$rowid&extend_billing=\" + ext_days;}' value='Extend' style='font-size:10px;'><select id='extend_count' style='font-size:10px'><option value=''></option><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option></select></td></table>";
					echo "</td>";
					echo "</table>";

					echo "</td>";
					if($signup_read['arb_license_id']!="" && $signup_read['arb_license_status']=="active")
						echo "<td><a style='cursor:pointer' onclick='if(confirm(\"Are you sure you want to cancel the license payment?\")) window.location = \"index.php?mode=manage_customers&submode=details&rowid=$rowid&cancel_billing=".$signup_read['arb_license_id']."\"'>(cancel)</a></td>";
					echo "</tr>";
					$signup_update_table = "signups";
				}
				else
				{
					echo "<tr><td colspan=4><select onchange='window.location = \"index.php?mode=manage_customers&submode=details&rowid=$rowid&create_signup_record=\" + this.value'>";
					echo "<option value=''>!! No Signup Record !!</option>";
					echo "<option value='1'>Set to Silver</option>";
					echo "<option value='2'>Set to Gold</option>";
					echo "<option value='3'>Set to Platinum</option>";
					echo "</select>";
					echo "</td></tr>";
					//$signup_read = mysqli_fetch_assoc($signup_query);
					$full_address = "";
					$full_name = "";
					$email = $cust_read['email'];
					$phone = $cust_read['phone'];
					$signupid = "";
					$signup_update_table = "restaurants";
				}
				echo "<tr><td class='label label-info'>Status:</td><td>".payment_status_select($rowid,"license",$cust_read['license_status'])."</td></tr>";
				echo "<tr><td class='label label-info'>Level:</td><td>".payment_status_select($rowid,"package",$cust_read['package_status'])."</td></tr>";

				echo "<tr><td>&nbsp;</td><td width='300'>&nbsp;</td></tr>";
				if(mysqli_num_rows($signup_query))
				{
					echo "<tr>";
					echo "<td class='label label-info'>Hosting:</td><td>".$signup_read['arb_hosting_status']."</td>";
					echo "</tr>";
					echo "<tr>";
					echo "<td class='label label-info'>Starts:</td><td>".$signup_read['arb_hosting_start']."</td>";
					if($signup_read['arb_hosting_id']!="" && $signup_read['arb_hosting_status']=="active")
						echo "<td align='left'><a style='cursor:pointer' onclick='if(confirm(\"Are you sure you want to cancel hosting?\")) window.location = \"index.php?mode=manage_customers&submode=details&rowid=$rowid&cancel_billing=".$signup_read['arb_hosting_id']."\"'>(cancel)</a></td>";
					echo "</tr>";
				}
				echo "<tr><td class='label label-info'>Status:</td><td>".payment_status_select($rowid,"hosting",$cust_read['hosting_status'])."</td></tr>";
				echo "<tr><td>&nbsp;</td></tr>";

				if(isset($_GET['edit_signup_data']))
				{
					for($n=0; $n<count($s_fields[$signup_update_table]); $n++)
					{
						$fname = $s_fields[$signup_update_table][$n];
						if($signup_update_table=="signups")
							$fval = $signup_read[$fname];
						else if($signup_update_table=="restaurants")
							$fval = $cust_read[$fname];
						echo "<tr><td class='label label-info'>".ucfirst($fname)."</td><td>";
						echo "<input type='text' name='signup_".$fname."' value=\"".str_replace("\"","",$fval)."\">";
						echo "</td></tr>";
					}
					echo "<tr><td></td><td><input type='hidden' name='signup_update' value='".$signup_update_table."'><input type='submit' value='Submit'>";
				}
				else
				{
					echo "<tr><td class='label label-info'>Name:</td><td>".$full_name."</td></tr>";
					echo "<tr><td class='label label-info'>Email:</td><td>".$email."</td></tr>";
					echo "<tr><td class='label label-info'>Phone:</td><td>".$phone."</td></tr>";
					echo "<tr><td class='label label-info'>Address:</td><td>".$full_address."</td></tr>";
					if($signup_update_table!="")
						echo "<tr><td></td><td><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid&edit_signup_data=1'>(edit)</a></td></tr>";
				}

				// For Sales campaign in which Lavu88 customers are persuaded to switch to Lavu|LavuEnterprise
				if ($signup_read['package'] == '8')
				{
					$proposed_billing_change_option_text = '';
					if (!empty($status_read['proposed_billing_change'])) {
						$proposed_billing_change = json_decode($status_read['proposed_billing_change'], true);
						$proposed_billing_change_option_text = $proposed_billing_change['name'];
					}
					echo "<tr><td>&nbsp;</td></tr>";
					echo "<tr><td class='label label-info'>Proposed<br>Billing<br>Change:</td><td>";
					echo "<select name='proposed_billing_change' id='proposed_billing_change' style='width:100px' onchange='document.getElementById(\"save_proposed_billing_change\").style.visibility = \"visible\";'>";
					echo "<option value=''></option>";
					if (!empty($status_read['proposed_billing_change'])) echo "<option value='{$proposed_billing_change['name']}' selected>{$proposed_billing_change_option_text}</option>";
					echo "<option value='{\"name\":\"Lavu $69/mo (Month-to-Month)\", \"package\":\"25\"}'>Lavu $69/mo (Month-to-Month)</option>";
					echo "<option value='{\"name\":\"Lavu $59/mo (1yr Agreement)\", \"package\":\"25\", \"annual_agreement\":\"1\"}'>Lavu $59/mo (1yr Agreement)</option>";
					echo "<option value='{\"name\":\"Lavu with 2 Terminals $108/mo (1yr Agreement)\", \"package\":\"25\", \"annual_agreement\":\"1\", \"custom_max_ipads\":\"2\"}'>Lavu with 2 Terminals $108/mo (1yr Agreement)</option>";
					//echo "<option value='{\"name\":\"Lavu $708/yr (1yr Pre-Pay at $59/mo)\", \"package\":\"25\", \"bill_frequency\":\"yearly\"}'>Lavu $708/yr (1yr Pre-Pay at $59/mo)</option>";
					echo "<option value='{\"name\":\"Lavu Enterprise $345/mo (Month-to-Month)\", \"package\":\"26\"}'>Lavu Enterprise $345/mo (Month-to-Month)</option>";
					echo "<option value='{\"name\":\"Lavu Enterprise $256/mo (1yr Agreement)\", \"package\":\"26\", \"annual_agreement\":\"1\"}'>Lavu Enterprise $256/mo (1yr Agreement)</option>";
					//echo "<option value='{\"name\":\"Lavu Enterprise $3,072/yr (1yr Pre-Pay at $256/mo)\", \"package\":\"26\", \"bill_frequency\":\"yearly\"}'>Lavu Enterprise $3,072/yr (1yr Pre-Pay at $256/mo)</option>";
					echo "</select><input type='button' name='save_proposed_billing_change' id='save_proposed_billing_change' value='Save' style='visibility:hidden;margin:5px'onclick='window.location = \"index.php?mode=manage_customers&submode=details&rowid=$rowid&update_pstatus=proposed_billing_change&set_pstatus=\" + document.getElementById(\"proposed_billing_change\").options[document.getElementById(\"proposed_billing_change\").selectedIndex].value'>";
					echo "</td></tr>";
				}

				echo "</table>";

				//------------------ Enable / Disable Account --------------//
				$account_disabled = $cust_read['disabled'];
				if(isset($_GET['disable']))
				{
					mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `disabled`='1' where id='[1]'",$_GET['disable']);
					$account_disabled = "1";
					echo "<br><b><font color='#550000'>This account has been disabled</font></b>";
				}
				else if(isset($_GET['enable']))
				{
					mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `disabled`='' where id='[1]'",$_GET['enable']);
					$account_disabled = "0";
					echo "<br><b><font color='#005500'>This account has been enabled</font></b>";
				}
				echo "<br><br><table>";
				echo "<tr>";
				if($account_disabled=="1"||$account_disabled=="2")
				{
					$enable_title = "(enable)";
					$enable_action = "enable";
					$enable_color = "#008800";
				}
				else
				{
					$enable_title = "(disable)";
					$enable_action = "disable";
					$enable_color = "#880000";
				}
				echo "<td><a style='cursor:pointer; color:$enable_color' onclick='if(confirm(\"".ucfirst($enable_action)."?\")) window.location = \"index.php?mode=manage_customers&submode=details&rowid=".$cust_read['id']."&$enable_action=".$cust_read['id']."\"'>$enable_title</a></td>";
				echo "</tr>";
				echo "</table>";
				//----------------------------------------------------------//

				echo "</div>";

				echo "</td></table>";
				echo "</form>";

				// payment_request: email,restaurantid,signupid,license_amount,hosting_amount,action,notes,key,id
				$payment_action = postvar("payment_action");
				if ($payment_action)
				{
					$send_to_email = postvar("send_to_email");
					$license_amount = postvar("license_amount");
					$license_amount = str_replace('$', '', $license_amount);
					$license_amount = str_replace(',', '', $license_amount);
					$hosting_amount = postvar("hosting_amount");
					$hosting_amount = str_replace('$', '', $hosting_amount);
					$hosting_amount = str_replace(',', '', $hosting_amount);
					$set_notes = postvar("notes");
					$set_key = rand(11111,99999) . rand(11111,99999) . rand(11111,99999);
					$set_notify_email = postvar("notify_email");
					$set_shipping = postvar("shipping");

					$vars = array();
					$vars['email'] = $send_to_email;
					$vars['restaurantid'] = $cust_read['id'];
					$vars['signupid'] = $signupid;
					$vars['license_amount'] = $license_amount;
					$vars['hosting_amount'] = $hosting_amount;
					$vars['action'] = $payment_action;
					$vars['notes'] = $set_notes;
					$vars['key'] = $set_key;
					$vars['notify_email'] = $set_notify_email;
					$vars['shipping'] = $set_shipping;
					$vars['dataname'] = $cust_read['data_name'];
					$vars['createuser'] = $loggedin;
					$vars['distro_code'] = $cust_read['distro_code'];

					$payment_link = "https://register.poslavu.com/make_payment/?key={$set_key}&session=". session_id();

					// LP-349 -- Added fix for blank createtime field
					mlavu_query("insert into `poslavu_MAIN_db`.`payment_request` (`email`,`restaurantid`,`signupid`,`license_amount`,`hosting_amount`,`action`,`notes`,`key`,`notify_email`,`shipping`,`dataname`,`distro_code`,`createuser`,`createtime`) values ('[email]','[restaurantid]','[signupid]','[license_amount]','[hosting_amount]','[action]','[notes]','[key]','[notify_email]','[shipping]','[dataname]','[distro_code]','[createuser]',now())",$vars);

					$message = "Please follow the link below to make your payment:\n\n". $payment_link;
					mail($send_to_email,"Payment request from Lavu, Inc.", $message, "From: sales@lavu.com");
					echo "message sent!";
				}

				// LP-349 -- Allow users to delete unpaid payment_requests
				$delpmtreq = urlvar("delpmtreq");
				if ($delpmtreq)
				{
					mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_request` SET `_deleted`='1' WHERE `id`='[1]' AND `dataname`='[2]' AND `id`!='' AND `dataname`!='' AND `_deleted`=''", $delpmtreq, $cust_read['data_name']);
					if (ConnectionHub::getConn('poslavu')->affectedRows() > 0) echo "deleted payment_request {$delpmtreq}!";
					else echo "could not delete payment_request {$delpmtreq}!";
				}

				$payment_requests = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_request` WHERE (`dataname`='[data_name]' OR `restaurantid`='[id]') AND `_deleted`='' ORDER BY `id` DESC", $cust_read);
				if ($payment_requests === false) error_log(__FILE__ ." got db error for SA_CP payment_request for dataname={$cust_read['data_name']}: ". mlavu_dberror());

				echo "<table><tr><td valign='top'>";
				echo "<table width='100%' cellpadding='0' cellspacing='0'><tr><td>";

				// 1st column shows usual Payment Request entry form
				echo "<form name='request_payment' method='post' action='index.php?mode=manage_customers&submode=details&rowid=$rowid#payment_request'>";
				echo "<table cellpadding=12 bgcolor='#e2e2e2'><td style='border:solid 1px black' valign='top'>";
				echo "<a id='payment_request'>";
				echo "<table>";
				echo "<tr><td colspan='2' align='center'>Send Payment Link Request</td></tr>";
				echo "<tr><td class='label label-info'>Send To Email:</td><td><input type='text' name='send_to_email'></td></tr>";
				echo "<tr><td class='label label-info'>Charge Amount:</td><td><input type='text' name='license_amount' type='number' min='1' step='any' required></td></tr>";
				echo "<tr><td class='label label-info'>Action:</td><td>";
				echo "<select name='payment_action'>";
				echo "<option value='Zuora Quote Payment Link'>Zuora Quote Payment Link</option>";
				echo "</select>";
				echo "</td></tr>";
				echo "<tr><td class='label label-info'>Notes:</td><td>";
				echo "<textarea name='notes' cols='32' rows='3'></textarea>";
				echo "</td></tr>";
				echo "<tr><td class='label label-info'>Notify Email:</td><td><input type='text' name='notify_email' value='{$loggedin_email}'></td></tr>";
				echo "<tr><td>&nbsp;</td><td><input type='submit' value='Submit'></td></tr>";
				echo "</table>";
				echo "</table>";
				echo "</form>";

				echo "</td><td valign='top'>";

				// 2nd column shows Payment Requests for that customer
				echo "<table width='100%' cellpadding='2' cellspacing='2' align='center'>";
				echo "<tr><colspan='7'>Payment Requests</th></tr>";

				if ( mysqli_num_rows($payment_requests) <= 0 )
				{
					echo "<tr><td colspan='7' align='center'>(No Payment Requests for this account)</td></tr>";
				}
				else
				{
					echo "<tr bgcolor='#cccceee'><td class='label label-info'>Amount</td><td class='label label-info'>Hosting</td><td class='label label-info'>Send To</td><td class='label label-info'>Notify</td><td class='label label-info'>Creation Info</td><td class='label label-info'></td><td style='background-color:white'></td></tr>";
					while ( $pmtreq = mysqli_fetch_assoc($payment_requests) )
					{
						$pmtreq['status-out'] = ( (empty($pmtreq['license_amount']) || !empty($pmtreq['license_paid'])) && (empty($pmtreq['hosting_amount']) || !empty($pmtreq['hosting_paid'])) && (empty($pmtreq['points_amount']) || !empty($pmtreq['points_paid'])) ) ? 'ENTERED CC' : '';
						$pmtreq['delete_payment_link-out'] = htmlspecialchars( "/sa_cp/?mode=manage_customers&submode=details&rowid=$rowid&delpmtreq={$pmtreq['id']}#payment_request" );
						$pmtreq['payment_link-out'] = htmlspecialchars( "https://register.poslavu.com/make_payment/?key=". $pmtreq['key'] ."&session=". session_id() );
						$pmtreq['createinfo-out'] = htmlspecialchars( $pmtreq['createtime'] );
						if ($pmtreq['createuser']) $pmtreq['createinfo-out'] .= htmlspecialchars( " by {$pmtreq['createuser']}" );
						$pmtreq['license_amount-out'] = htmlspecialchars( number_format($pmtreq['license_amount'], 2) );
						$pmtreq['hosting_amount-out'] = htmlspecialchars( number_format($pmtreq['hosting_amount'], 2) );
						$pmtreq['email-out'] = htmlspecialchars( $pmtreq['email'] );
						$pmtreq['notify_email-out'] = htmlspecialchars( $pmtreq['notify_email'] );
						$pmtreq['notes-out'] = htmlspecialchars( empty($pmtreq['notes']) ? '' : '"'. $pmtreq['notes'] .'"' );
						$pmtreq['refid-out'] = htmlspecialchars( (empty($pmtreq['refid']) ? '' : 'Zuora Account Id '. $signup_read['zAccountId']) );
						$paid_css = empty($pmtreq['status-out']) ? '' : ";text-decoration:line-through;'";
						$delbtn_css = empty($pmtreq['status-out']) ? '' : "display:none'";

						echo "<tr>";
						echo "<td style='{$paid_css}'>{$pmtreq['license_amount-out']}</td>";
						echo "<td style='{$paid_css}'>{$pmtreq['hosting_amount-out']}</td>";
						echo "<td style='{$paid_css}'>{$pmtreq['email-out']}</td>";
						echo "<td style='{$paid_css}'>{$pmtreq['notify_email-out']}</td>";
						echo "<td style='{$paid_css}'>{$pmtreq['createinfo-out']}</td>";
						echo "<td style='{$paid_css}'><a target='_blank' href=\"{$pmtreq['payment_link-out']}\">link</a></td>";
						echo "<td>{$pmtreq['status-out']}</td></tr>";
						echo "<tr><td colspan='6' style='{$paid_css}'><em>{$pmtreq['notes-out']}</em></td>";
						echo "<td>{$pmtreq['refid-out']}<a style='text-decoration:none;{$delbtn_css}' href=\"{$pmtreq['delete_payment_link-out']}\">❌</a></td></tr>";
						echo "</tr><tr><td colspan='6'></td></tr>";
					}
				}
				echo "</table>";
				echo "</table>";

				echo "</td></tr></table>";
				echo "</td><td valign='top'>";

				/*$profile_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_profile` where `restaurantid`='[1]'",$cust_read['id']);
				while($profile_read = mysqli_fetch_assoc($profile_query))
				{
					echo "Payment Profile: " . $profile_read['type'];

					$payment_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_history` where `profileid`='[1]' order by datetime desc",$profile_read['id']);
					if(mysqli_num_rows($payment_query))
					{
						echo "<table>";
						while($payment_read = mysqli_fetch_assoc($payment_query))
						{
							if($payment_read['result']==1) $show_result = "<font color='#008800'><b>Succeeded</b></font>";
							else $show_result = "<font color='#880000'><b>Failed</b></font>";

							echo "<tr>";
							echo "<td>" . $payment_read['datetime'] . "</td>";
							echo "<td>&nbsp;&nbsp;$" . number_format($payment_read['amount'],2) . "&nbsp;&nbsp;</td>";
							echo "<td>" . $payment_read['notes'] . "</td>";
							echo "<td>" . $show_result . "</td>";
							echo "</tr>";
						}
						echo "</table><br><br>";
					}
				}*/

				$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]' or `dataname`='[2]'",$cust_read['id'],$cust_read['data_name'], null, null, null, null, false);
				if(mysqli_num_rows($status_query))
				{
					$status_read = mysqli_fetch_assoc($status_query);
					$license_applied = $status_read['license_applied'];
					$license_type = $status_read['license_type'];
					$license_resellerid = $status_read['license_resellerid'];
					$trial_end = $status_read['trial_end'];
					$actual_trial_end = $trial_end;
					if($license_applied!="")
						$trial_end = "";

					if($actual_trial_end=="" && $license_applied=="") // not created by a distro
					{
					}
					else if($license_applied!="")
					{
						$distro_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `id`='[1]'",$license_resellerid);
						if(mysqli_num_rows($distro_query))
						{
							$distro_read = mysqli_fetch_assoc($distro_query);
							$license_distro_name = trim($distro_read['f_name'] . " " . $distro_read['l_name'] . " (".$distro_read['username'].")");
						}
						else $license_distro_name = "#" . $license_resellerid;
						echo "Distro " . $license_distro_name . " Applied " . $license_type . " License<br>";
					}
				}

				$payment_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' order by datetime desc",$cust_read['id'], null, null, null, null, null, false);
				if(mysqli_num_rows($payment_query))
				{
					echo "<table>";
					while($payment_read = mysqli_fetch_assoc($payment_query))
					{
						if($payment_read['x_response_code']=="1") $show_result = "<font color='#008800'><b>Succeeded</b></font>";
						else $show_result = "<font color='#880000'><b>Failed</b></font>";

						$type_style = "";
						if($payment_read['x_type']=="auth_capture" && $payment_read['x_response_code']!="1") $type_style = "bgcolor='#880000' style='color:#ffffff'";
						else if($payment_read['x_type']=="auth_capture") $type_style = "bgcolor='#008800' style='color:#ffffff'";
						else if($payment_read['x_type']=="credit") $type_style = "bgcolor='#000088' style='color:#ffffff'";


						echo "<tr>";
						echo "<td ".$type_style.">" . $payment_read['x_type'] . "</td>";
						echo "<td style='color:#8888cc'>&nbsp;&nbsp;" . $payment_read['datetime'] . "&nbsp;&nbsp;</td>";
						echo "<td align='right'>" . $payment_read['x_card_type'] . " " . $payment_read['x_account_number'] . "</td>";
						echo "<td>&nbsp;&nbsp;$" . number_format($payment_read['x_amount'],2) . "&nbsp;&nbsp;</td>";
						echo "<td>" . $payment_read['x_description'] . "</td>";
						echo "<td>" . $show_result . "</td>";
						echo "</tr>";
					}
					echo "</table><br><br>";
				}

				echo "</td></tr></table>";

				echo "Locations:";
				echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px black'>";
				echo "<tr bgcolor='#66FF99'>";
				echo "<td colspan='8'></td>";
				echo "<td colspan='3' align='center'>Debug</td>";
				echo "<td colspan='4'></td>";
				echo "</tr>";
				echo "<tr bgcolor='#66FF99'>";
				echo "<td align='center' valign='bottom'>ID</td>";
				echo "<td align='center' valign='bottom'>Name</td>";
				echo "<td align='center' valign='bottom'>City</td>";
				echo "<td align='center' valign='bottom'>State</td>";
				echo "<td align='center' valign='bottom'>Country</td>";
				echo "<td align='center' valign='bottom'>Phone</td>";
				echo "<td align='center' valign='bottom'>Manager</td>";
				echo "<td align='center' valign='bottom'>Gateway</td>";
				echo "<td align='center' valign='bottom' style='font-size:9px'>Gateway</td>";
				echo "<td align='center' valign='bottom' style='font-size:9px'>API</td>";
				echo "<td align='center' valign='bottom' style='font-size:9px'>Sync</td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "<td></td>";
				echo "</tr>";

				$repo_dir = "lls-zendguard";
				if (isset($_GET['encoder'])) $repo_dir = "lls-".$_GET['encoder'];

				if(isset($_GET['request_local_server_info']) && isset($_GET['locid']))
				{
					//$success = mlavu_query("insert into `poslavu_[1]_db`.`config` (`location`,`setting`,`value`) values ('[2]','read file structure','')",$cust_read['data_name'],$_GET['locid']);
					schedule_msync($cust_read['data_name'],$_GET['locid'],"read file structure");
				}
				else if(isset($_GET['request_sync_version'])) {
					mlavu_query("DELETE FROM `[database]`.`config` WHERE `setting`='last sync finished' AND `type`='log'",
						array('database'=>'poslavu_'.$_GET['dn'].'_db'));
				}
				else if(isset($_GET['remove_pending']))
				{
					mlavu_query("delete from `poslavu_[1]_db`.`config` where `id`='[2]'",$cust_read['data_name'],$_GET['remove_pending']);
				}
				else if(isset($_GET['update_sync_files']) && isset($_GET['locid']))
				{
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/health_check.php","../local/health_check.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/sync_exec.php","../local/sync_exec.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/sync.php","../local/sync.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/lsvpn.php","../local/lsvpn.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/run_sync","../local/run_sync");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"sync database");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"read file structure");

					//echo "<br><br>".print_r($_GET, true)."<br><br>".$repo_dir."<br><br>";

					//schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/lsvpn.php","../local/lsvpn.php");
					//schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/health_check.php","../local/health_check.php");
					//schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/resources/rfix_functions.php","../cp/resources/rfix_functions.php");
					//schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/sync_exec.php","../local/sync_exec.php");
					//schedule_msync($cust_read['data_name'],$_GET['locid'],"sync database");
				}
				else if( isset($_GET['install_lls_kds']) && isset($_GET['locid']) ){
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/sync_exec.php","../local/sync_exec.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/kds.php","../local/kds.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/kds_functions.php","../local/kds_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"create_directory","../images/lls_kds","");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/back_button.png","../images/lls_kds/back_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/bump_button.png","../images/lls_kds/bump_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/bump_button_disabled.png","../images/lls_kds/bump_button_disabled.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/bump_push_button.png","../images/lls_kds/bump_push_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/done_button.png","../images/lls_kds/done_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/forward_button.png","../images/lls_kds/forward_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/history_button.png","../images/lls_kds/history_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/mapper_button.png","../images/lls_kds/mapper_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/page_button.png","../images/lls_kds/page_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/resume_button.png","../images/lls_kds/resume_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/settings_button.png","../images/lls_kds/settings_button.png");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/images/lls_kds/settings_button_disabled.png","../images/lls_kds/settings_button_disabled.png");

					schedule_msync($cust_read['data_name'],$_GET['locid'],"sync database");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"read file structure");
				}
				else if(isset($_GET['update_to_208']) && isset($_GET['locid']))
				{

					//echo "<br><br>".print_r($_GET, true)."<br><br>".$repo_dir."<br><br>";

					schedule_msync($cust_read['data_name'],$_GET['locid'],"sync database");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/local_functions.php","local_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/resources/core_functions.php","../cp/resources/core_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/resources/lavuquery.php","../cp/resources/lavuquery.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/modules.php","../lib/modules.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/jcvs/jc_functions.php","../lib/jcvs/jc_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/jcvs/jc_inc7.php","../lib/jcvs/jc_inc7.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/deviceValidations.php","../lib/deviceValidations.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/receipt_append_results.php","../lib/receipt_append_results.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/till_report.php","../lib/till_report.php");
				}
				else if(isset($_GET['update_for_transfer_tables']) && isset($_GET['locid']))
				{
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/local_functions.php","local_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/resources/core_functions.php","../cp/resources/core_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/inapp_management.php","../lib/inapp_management.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/management/table_details.php","../lib/management/table_details.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/management/view_tables.php","../lib/management/view_tables.php");
				}
				else if(isset($_GET['update_for_uncaptured_transactions']) && isset($_GET['locid']))
				{
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/local_functions.php","local_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/resources/core_functions.php","../cp/resources/core_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/webview_special_functions.php","../lib/webview_special_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/inapp_management.php","../lib/inapp_management.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/management/mgmt_menu.php","../lib/management/mgmt_menu.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/management/cc_auths.php","../lib/management/cc_auths.php");
				}
				else if(isset($_GET['update_gateway_for_heartland']) && isset($_GET['locid']))
				{

					//echo "<br><br>".print_r($_GET, true)."<br><br>".$repo_dir."<br><br>";

					schedule_msync($cust_read['data_name'],$_GET['locid'],"sync database");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/local_functions.php","local_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/resources/core_functions.php","../cp/resources/core_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/areas/reports/settle_batch.php","../cp/areas/reports/settle_batch.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway.php","../lib/gateway.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway_functions.php","../lib/gateway_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway_settings.php","../lib/gateway_settings.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/manage_tips.php","../lib/manage_tips.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/save_sig.php","../lib/save_sig.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway_lib/heartland.php","../lib/gateway_lib/heartland.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway_lib/heartland_func.php","../lib/gateway_lib/heartland_func.php");
				}
				else if(isset($_GET['update_gateway_for_usaepay']) && isset($_GET['locid']))
				{

					//echo "<br><br>".print_r($_GET, true)."<br><br>".$repo_dir."<br><br>";

					schedule_msync($cust_read['data_name'],$_GET['locid'],"sync database");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/local/local_functions.php","local_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/resources/core_functions.php","../cp/resources/core_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/cp/areas/reports/settle_batch.php","../cp/areas/reports/settle_batch.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway.php","../lib/gateway.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway_functions.php","../lib/gateway_functions.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway_settings.php","../lib/gateway_settings.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/manage_tips.php","../lib/manage_tips.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/save_sig.php","../lib/save_sig.php");
					schedule_msync($cust_read['data_name'],$_GET['locid'],"copy remote file",$repo_dir."/lib/gateway_lib/usaepay.php","../lib/gateway_lib/usaepay.php");
				}
				else if(isset($_GET['sync_local_database_structure']) && isset($_GET['locid']))
				{
					echo "Syncing Local Database Structure...<br>";
					schedule_msync($cust_read['data_name'],$_GET['locid'],"sync database");
				}
				else if(isset($_GET['retrieve_local_file']) && isset($_GET['locid']))
				{
					schedule_msync($cust_read['data_name'],$_GET['locid'],"retrieve local file",$_GET['retrieve_local_file']);
				}
				else if(isset($_GET['export_all_remote_data']) && isset($_GET['locid']))
				{
					$export_dir = "/home/poslavu/public_html/admin/images/".$cust_read['data_name']."/export";
					//if(!is_dir(
					//exec("mysqldump -u poslavu -pA8n8d8y8 poslavu_".$cust_read['data_name']."_db > ".$export_dir."/full_dump.sql");
				}

				$loc_id_lookup = array();
				$loc_query = mlavu_query("select * from `poslavu_[1]_db`.`locations`  order by `title` asc", $cust_read['data_name'], null, null, null, null, null, false);
				if(mysqli_num_rows($loc_query))
				{
					while($loc_read = mysqli_fetch_assoc($loc_query))
					{
						echo "<tr>";
						echo "<td>" . $loc_read['id'] . "</td>";
						echo "<td>" . $loc_read['title'] . "</td>";
						echo "<td>" . $loc_read['city'] . "</td>";
						echo "<td>" . $loc_read['state'] . "</td>";
						echo "<td>" . $loc_read['country'] . "</td>";
						echo "<td>" . $loc_read['phone'] . "</td>";
						echo "<td>" . $loc_read['manager'] . "</td>";
						echo "<td>" . $loc_read['gateway'] . "</td>";
						$show_location_name = str_replace("'", "&acute;", $loc_read['title']);
						echo "<td style='padding:8px 4px 0px 10px'><div id='divlocgateway_debug".$loc_read['id']."'><script language='javascript'>showSelector(\"loc\", \"".$cust_read['id']."\", \"".$loc_read['id']."\", \"gateway_debug\", \"\");</script></td>";
						echo "<td style='padding:8px 4px 0px 10px'><div id='divlocapi_debug".$loc_read['id']."'><script language='javascript'>showSelector(\"loc\", \"".$cust_read['id']."\", \"".$loc_read['id']."\", \"api_debug\", \"\");</script></td>";
						echo "<td style='padding:8px 4px 0px 10px'><div id='divloclds_debug".$loc_read['id']."'><script language='javascript'>showSelector(\"loc\", \"".$cust_read['id']."\", \"".$loc_read['id']."\", \"lds_debug\", \"\");</script></td>";
						echo "<td><input type='button' value='Turn on LS Ipad Tunnel' style='font-size:12px' onclick='window.open(\"index.php?mode=$mode&submode=turn_on_tunnel&notabs=1&rowid=$rowid&locid=".$loc_read['id']."\",\"\",\"width=420,height=320,resizable=yes,toolbar=no\")'></td>";
						echo "<td><input type='button' value='Request Local Server Info (if relevant)' style='font-size:10px' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$loc_read['id']."&request_local_server_info=1\"' /></td>";
						//echo "<td><input type='button' value='Manage Local Server Info (if relevant)' style='font-size:10px' onclick='window.location = \"?mode=manage_customers&submode=local&rowid=$rowid&locid=".$loc_read['id']."\"' /></td>";
						//echo "<td style='padding-left:5px'><input type='button' onclick='if (confirm(\"Are you sure want to duplicate $show_location_name?\")) { document.location= \"index.php?mode=manage_customers&submode=details&rowid=$rowid&duplicate_location=".$loc_read['id']."\"; }' value='Duplicate'></td>";
						$enable_disable = "Disable";
						if ($loc_read['_disabled'] == "1") $enable_disable = "Enable";
						echo "<td style='padding-left:5px'><input type='button' onclick='document.location= \"index.php?mode=manage_customers&submode=details&rowid=$rowid&".$enable_disable."_location=".$loc_read['id']."\";' value='$enable_disable'></td>";
						echo "<td style='padding-left:5px'><input type='button' value='Request Sync Version' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$loc_read['id']."&dn=".$cust_read['data_name']."&request_sync_version=1\"' /></td>";
						echo "</tr>";
						$loc_id_lookup[$loc_read['id']] = $loc_read['title'];
					}
				}
				else
				{
					echo "<tr><td align='center' colspan='7'>No locations found</td></tr>";
				}
				echo "</table>";

				echo "<br>Devices:";
				echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px black'>";
				echo "<tr bgcolor='#cccceee'>";
				echo "<td></td>";
				echo "<td align='center' valign='bottom'>Name / IP</td>";
				echo "<td align='center' validn='bottom'>SSID</td>";
				echo "<td align='center' valign='bottom'>Model</td>";
				echo "<td align='center' valign='bottom'>iOS</td>";
				echo "<td align='center' valign='bottom'>App</td>";
				echo "<td align='center' valign='bottom'>Version (Build)</td>";
				echo "<td align='center' valign='bottom'>Location</td>";
				echo "<td align='center' valign='bottom'>Order Prefix</td>";
				echo "<td align='center' valign='bottom'>Device Identifier</td>";
				echo "<td align='center' valign='bottom'>Last CheckIn</td>";
				echo "<td align='center' valign='bottom'>Device Time</td>";
				echo "<td align='center' valign='bottom'>Check-in Engine</td>";
				echo "<td align='center' valign='bottom'>Usage Type</td>";
				echo "<td align='center' valign='bottom'>Holding Order</td>";
				echo "</tr>";

				$ddb = "poslavu_".$cust_read['data_name']."_db";

				$query = "SELECT `[2]`.`devices`.`lavu_admin` AS `lavu_admin`, ";
				$query .= "`[2]`.`devices`.`name` AS `name`, ";
				$query .= "`[2]`.`devices`.`ip_address` AS `ip_address`, ";
				$query .= "`[2]`.`devices`.`SSID` AS `SSID`, ";
				$query .= "`[2]`.`devices`.`model` AS `model`, ";
				$query .= "`[2]`.`devices`.`specific_model` AS `specific_model`, ";
				$query .= "`[2]`.`devices`.`system_version` AS `system_version`, ";
				$query .= "`[2]`.`devices`.`app_name` AS `app_name`, ";
				$query .= "`[2]`.`devices`.`poslavu_version` AS `poslavu_version`, ";
				$query .= "`[2]`.`devices`.`loc_id` AS `loc_id`, ";
				$query .= "`[2]`.`locations`.`title` AS `location_name`, ";
				$query .= "`[2]`.`devices`.`prefix` AS `prefix`, ";
				$query .= "`[2]`.`devices`.`UDID` AS `UDID`, ";
				$query .= "`[2]`.`devices`.`MAC` AS `MAC`, ";
				$query .= "`[2]`.`devices`.`UUID` AS `UUID`, ";
				$query .= "`[2]`.`devices`.`last_checkin` AS `last_checkin`, ";
				$query .= "`[2]`.`devices`.`device_time` AS `device_time`, ";
				$query .= "`[2]`.`devices`.`check_in_engine` AS `check_in_engine`, ";
				$query .= "`[2]`.`devices`.`usage_type` AS `usage_type`, ";
				$query .= "`[2]`.`devices`.`holding_order_id` AS `holding_order_id` ";
				$query .= "FROM `[2]`.`devices` ";
				$query .= "LEFT JOIN `[2]`.`locations` ";
				$query .= "ON `[2]`.`locations`.`id` = `[2]`.`devices`.`loc_id` ";
				$query .= "WHERE `[2]`.`devices`.`company_id` = '[1]' ";
				$query .= "AND `[2]`.`devices`.`inactive` != '1' ";
				$query .= "AND `[2]`.`devices`.`last_checkin` >= '".date("Y-m-d H:i:s", (time() - 604800))."' ";
				$query .= "ORDER BY `[2]`.`locations`.`title` ASC, ";
				$query .= "`[2]`.`devices`.`app_name` ASC, ";
				$query .= "`[2]`.`devices`.`last_checkin` DESC";

				$device_query = mlavu_query($query, $cust_read['id'], $ddb, NULL, NULL, NULL, NULL, FALSE);
				if (!$device_query)
				{
					echo mlavu_dberror();
				}
				else
				{
					if (mysqli_num_rows($device_query))
					{
						$platform_list = iOSplatformList();
						$last_loc_id = 0;
						while ($dvc_rd = mysqli_fetch_assoc($device_query))
						{
							if (($last_loc_id != 0) && ($dvc_rd['loc_id'] != $last_loc_id))
							{
								echo "<tr><td colspan='15'><hr color='#555555'></td></tr>";
							}

							$display_ip = "";
							if ($dvc_rd['ip_address'] != "")
							{
								 $display_ip = "<br>".$dvc_rd['ip_address'];
							}

							$display_model = $dvc_rd['model'];
							if ($dvc_rd['specific_model'] == "Simulator")
							{ 
								$display_model .= " - Simulator";
							}
							else if (!empty($dvc_rd['specific_model']))
							{
								$display_model = $dvc_rd['specific_model'];
							}

							if (isset($platform_list[$display_model]))
							{
								$display_model = $platform_list[$display_model];
							}

							$display_identifier = $dvc_rd['UDID'];
							if ($dvc_rd['UUID'] != "")
							{ 
								$display_identifier = $dvc_rd['UUID'];
							}
							else if ($dvc_rd['MAC'] != "")
							{ 
								$display_identifier = $dvc_rd['MAC'];
							}

							$check_in_engine = ($dvc_rd['check_in_engine'] == "dataplex")?"DataPlex":"JSON Connect";

							echo "<td>" . (($dvc_rd['lavu_admin'] == "1")?"*":"") . "</td>";
							echo "<td valign='top'>" . $dvc_rd['name'] . $display_ip . "</td>";
							echo "<td valign='top'>" . $dvc_rd['SSID'] . "</td>";
							echo "<td valign='top'>" . $display_model . "</td>";
							echo "<td valign='top'>" . $dvc_rd['system_version'] . "</td>";
							echo "<td valign='top'>" . $dvc_rd['app_name'] . "</td>";
							echo "<td valign='top'>" . $dvc_rd['poslavu_version'] . "</td>";
							echo "<td valign='top'>" . $dvc_rd['location_name'] . "</td>";
							echo "<td valign='top' align='center'>" . str_pad($dvc_rd['prefix'], 2, "0", STR_PAD_LEFT) . "</td>";
							echo "<td valign='top'>" . $display_identifier . "</td>";
							echo "<td valign='top'>" . $dvc_rd['last_checkin'] . "</td>";
							echo "<td valign='top'>" . $dvc_rd['device_time'] . "</td>";
							echo "<td valign='top' align='center'>" . $check_in_engine . "</td>";
							echo "<td valign='top' align='center'>" . $dvc_rd['usage_type'] . "</td>";
							echo "<td valign='top' align='center'>" . $dvc_rd['holding_order_id'] . "</td>";
							echo "</tr>";

							$last_loc_id = $dvc_rd['loc_id'];
						}
					}
					else
					{
						echo "<tr><td align='center' colspan='13'>No Devices found</td></tr>";
					}
				}
				echo "</table><br><br>";

				if ($alert_message != "")
				{
					echo "<script language='javascript'>alert(\"".str_replace('"', '&quot;', $alert_message)."\");</script>";
				}

				function get_vars_from_str($str)
				{
					$varset = array();
					$varsplit = explode("&",$str);
					for($v=0; $v<count($varsplit); $v++)
					{
						$cursplit = explode("=",$varsplit[$v]);
						if(count($cursplit) > 1)
						{
							$varset[trim($cursplit[0])] = $cursplit[1];
						}
					}
					return $varset;
				}

				function show_folders_with_parent($rdb,$url_path,$parent,$paths,$level=0)
				{
					global $rowid;
					global $locserv_read;
					global $repo_dir;
					global $cust_read;

					if($level > 16) return "";

					$varset = get_vars_from_str($url_path);
					if(isset($varset['locid']))
						$locationid = $varset['locid'];
					else $locationid = "";

					$path_level = array();
					for($i=0; $i<count($paths); $i++)
					{
						if($paths[$i][0]==$parent)
						{
							$path_level[] = array_merge($paths[$i],array($i));
						}
					}

					if(count($path_level) < 1)
					{
						return "";
					}
					else
					{
						$str = "";
						for($i=0; $i<count($path_level); $i++)
						{
							$parent_folder = $path_level[$i][0];
							$filename = $path_level[$i][1];
							$basename = $path_level[$i][2];
							$filesize = $path_level[$i][3];
							$filedate = $path_level[$i][4];
							$fileowner = (isset($path_level[$i][5]))?$path_level[$i][5]:"";
							$id = $path_level[$i][6];
							$folder_id = "folder_".$level."_".$id;

							if($filesize=="" && $filedate=="")
							{
								$folder_icon = "icon_folder.jpg";
								$extra_code = " style='cursor:pointer' onclick='current_display = document.getElementById(\"$folder_id\").style.display; if(current_display==\"table-row\") set_display = \"none\"; else set_display = \"table-row\"; document.getElementById(\"$folder_id\").style.display = set_display'";
								if($filename!=$cust_read['data_name'] && $filename!="backup")
									$get_subdirs = true;
							}
							else
							{
								$folder_icon = "icon_blank.jpg";
								$extra_code = "";
								$get_subdirs = false;
							}

							$file_ext = "";
							if(strrpos($basename,"."))
								$file_ext = substr($basename,strrpos($basename,".") + 1);

							$str .= "<tr style='display:$set_display'>";
							$str .= "<td style='width:500px; overflow:hidden'>";
							$str .= "  <table cellspacing=0 cellpadding=2><tr>";
							if($level > 0)
								$str .= "<td style='width:".(30 * $level)."px'>&nbsp;</td>";
							$str .= "  <td valign='middle'><img src='images/$folder_icon' border='0' ".$extra_code."/></td><td valign='middle' ".$extra_code.">" . $basename;
							if($filesize <= 300000 && ($file_ext!="" || strpos($basename,"htaccess")!==false))
							{
								$file_query = mlavu_query("select `value`, `value2`, `value4` from `[1]`.`config` where `location`='[2]' and `setting`='retrieved_local_file' and TRIM(`value`)='[3]'",$rdb,$locationid,trim($filename));
								if(mysqli_num_rows($file_query))
								{
									$file_read = mysqli_fetch_assoc($file_query);
									$str .= " <a href='".$url_path."&filedate=".$filedate."&read_retrieved_file=$filename' title='Updated: ".$file_read['value2']." from ".$file_read['value4']."'><font color='#000088'>(read)</font></a>";
								}
								$str .= " <a href='".$url_path."&retrieve_local_file=$filename'><font color='#008800'>(retrieve)</font></a>";

								$repo_copy_from = str_replace("/poslavu-local/",$repo_dir."/",$filename);
								$repo_copy_to = str_replace("/poslavu-local/","../",$filename);
								//if(isset($_GET['test1']))
								$str .= " <a href='?mode=manage_customers&submode=details&rowid=$rowid&locid=".$locserv_read['location']."&_copy_local_files=$repo_copy_to&_copy_remote_files=$repo_copy_from'>(update from repo)</a>";
								//onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&_copy_local_files=\" + document.getElementById(\"copy_local_files\").value + \"&_copy_remote_files=\" + document.getElementById(\"copy_remote_files\").value'
							}
							$str .= "</td>";
							$str .= "  </tr></table>";
							$str .= "</td>";
							$str .= "<td style='width:70px'>" . $filesize . "</td>";
							$str .= "<td style='width:160px'><nobr>";
							$str .= $filedate;
							if($fileowner!="") $str .= " " . $fileowner . "&nbsp;";
							$str .= "</nobr></td>";
							//$str .= "<td style='width:160px'>" . $fileowner . "</td>";
							$str .= "</tr>";

							if($get_subdirs)
							{
								$str .= "<tr style='display:none' id='$folder_id'>";
								$str .= "<td colspan='3'>";
								$str .= "<table cellspacing=0 cellpadding=0>";
								$str .= show_folders_with_parent($rdb,$url_path,$filename,$paths,$level+1);
								$str .= "</table>";
								$str .= "</td>";
								$str .= "</tr>";
							}
						}
						if($level==0)
							return "<table style='border:solid 1px black' cellspacing=0 cellpadding=0>" . $str . "</table>";
						else return $str;
					}
				}

				/*function create_timestamp_from_datetime($dt)
				{
					$dt = explode(" ",$dt);
					if(count($dt) > 1)
					{
						$dateparts = explode("-",$dt[0]);
						$timeparts = explode(":",$dt[1]);
						if(count($dateparts) > 2 && count($timeparts) > 2)
							return mktime($timeparts[0],$timeparts[1],$timeparts[2],$dateparts[1],$dateparts[2],$dateparts[0]);
					}
					return 0;
				}*/

				$basepath = "n/a";
				$paths = array();
				$locserv_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='local_file_structure'", $cust_read['data_name'], null, null, null, null, null, false);
				while($locserv_read = mysqli_fetch_assoc($locserv_query))
				{
					echo "<br><b>Local Server File Structure - Location " . $locserv_read['location'] . "</b> <a href='?mode=manage_customers&submode=$submode&rowid=$rowid'>(refresh)</a>";
					echo "<br>Ip Address: " . $locserv_read['value'];
					echo "<br>Last Read: " . $locserv_read['value2'];

					$lastsync_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='last sync request'", $cust_read['data_name']);
					if(mysqli_num_rows($lastsync_query))
					{
						$lastsync_read = mysqli_fetch_assoc($lastsync_query);
						//echo "<br>Last Sync: " . $lastsync_read['value'];

						$lastsync_time = $lastsync_read['value'];
						if(strpos($lastsync_time,"/"))
						{
							$lastsync_parts = explode("/",$lastsync_time);
							$lastsync_time = $lastsync_parts[0];
							$lastsync_interval = $lastsync_parts[1];
						}
						else $lastsync_interval = 60;
						//echo "<br>Last Sync TS: " . $lastsync_time;
						//echo "<br>Last Sync Interval: " . $lastsync_interval;
						//$lastsync_ts = create_timestamp_from_datetime($lastsync_read['value']);

						$elapsed_sync = time() - $lastsync_time * 1;
						if($elapsed_sync >= 60)
						{
							if($elapsed_sync >= 60 * 60 * 24)
							{
								$days_ago = floor($elapsed_sync / (60 * 60 * 24));
								$time_ago = $days_ago . " day";
								if($days_ago > 1) $time_ago .= "s";
								$time_ago .= " ago";
							}
							else if($elapsed_sync >= 60 * 60)
							{
								$hours_ago = floor($elapsed_sync / (60 * 60));
								$time_ago = $hours_ago . " hour";
								if($hours_ago > 1) $time_ago .= "s";
								$time_ago .= " ago";
							}
							else
							{
								$minutes_ago = floor($elapsed_sync / 60);
								$time_ago = $minutes_ago . " minute";
								if($minutes_ago > 1) $time_ago .= "s";
								$time_ago .= " ago";
							}
							echo "<br>Last Time Synced: " . date("Y-m-d h:i:s a",$lastsync_read['value']) . " (".$time_ago.")";
						}
						else
						{
							$until_next_sync = ($lastsync_interval - $elapsed_sync);
							if($until_next_sync < 0) $until_next_sync = 0;
							$next_sync_id = "next_sync_" . $locserv_read['id'];
							$sync_left_display = "display_" . $next_sync_id;
							$sync_left_var = "var_" . $next_sync_id;
							$sync_left_function = "function_" . $next_sync_id;
							echo "<table cellspacing=0 cellpadding=0><tr><td style='color:#000088'>Next Sync Expected:&nbsp;</td><td><div style='color:#000088' id='$sync_left_display'><b>" . $until_next_sync . "</b></div></td><td style='color:#000088'>&nbsp;seconds</td></tr></table>";
							echo "<script language='javascript'>";
							echo " $sync_left_var = ".$until_next_sync."; ";
							echo " function ".$sync_left_function."() { ";
							echo "  $sync_left_var = $sync_left_var - 1; ";
							echo "  document.getElementById('$sync_left_display').innerHTML = '<b>' + ".$sync_left_var." + '</b>'; ";
							echo "  if($sync_left_var > 0) { ";
							echo "    setTimeout('".$sync_left_function."()', 1000); ";
							echo "  } ";
							echo " } ";
							echo " setTimeout('".$sync_left_function."()', 1000); ";
							echo "</script>";
						}
					}

					if($locserv_read['value3']!="")
					{
						$varset = get_vars_from_str($locserv_read['value3']);

						$remote_dbinfo = get_database_info("poslavu_".$cust_read['data_name']."_db");
						$remote_table_count = $remote_dbinfo['table_count'];
						$remote_field_count = $remote_dbinfo['field_count'];
						$remote_fields = $remote_dbinfo['fields'];

						if(isset($varset['field_sync_count']))
							$db_field_balance = ($varset['field_count'] - $varset['field_sync_count']);
						else $db_field_balance = $varset['field_count'];

						if(isset($varset['table_count'])) echo "<br><font color='#008800'>Database Tables: <b>" . $varset['table_count'] . " / $remote_table_count</b></font>";
						if(isset($varset['field_count']))
						{
							echo "<br><font color='#008800'>Database Fields: <b>" . $db_field_balance;
							if(isset($varset['field_sync_count']) && $varset['field_sync_count'] > 0)
								echo " <font color='#008800'>(+".$varset['field_sync_count'].")</font>";
							echo " / $remote_field_count</b></font>";
						}
						if(isset($varset['field_sync_count'])) echo "<br><font color='#008800'>Fields used for Syncing: <b>" . $varset['field_sync_count'] . "</b></font>";
						if(isset($varset['table_name_list']))
						{
							echo "<br><a style='cursor:pointer' onclick='if(document.getElementById(\"local_table_list\").style.display==\"block\") document.getElementById(\"local_table_list\").style.display = \"none\"; else document.getElementById(\"local_table_list\").style.display = \"block\"'><font color='#008800'>List Local Tables</font></a>";
							echo "<br><div style='display:none' id='local_table_list'><table>";
							$table_name_list = explode(",",urldecode($varset['table_name_list']));
							for($n=0; $n<count($table_name_list); $n++)
							{
								$table_name_parts = explode(" - ",$table_name_list[$n]);
								$ltname = $table_name_parts[0];
								if(isset($table_name_parts[1]))
									$local_table_field_count = $table_name_parts[1];
								else $local_table_field_count = 0;
								if(isset($table_name_parts[2]))
									$local_table_field_sync_count = $table_name_parts[2];
								else $local_table_field_sync_count = 0;
								if(isset($remote_fields[$ltname]) && isset($remote_fields[$ltname][0]))
									$remote_table_field_count = count($remote_fields[$ltname][0]);
								else $remote_table_field_count = 0;

								$local_table_field_balance = $local_table_field_count - $local_table_field_sync_count;
								if($local_table_field_balance * 1 != $remote_table_field_count * 1)
									$use_style = "style='font-weight:bold'";
								else
									$use_style = "";

								echo "<tr>";
								echo "<td align='right' $use_style>" . $ltname . "</td>";
								echo "<td $use_style>" . $local_table_field_balance;
								if($local_table_field_sync_count > 0) echo " <font color='#008800'>(+" . $local_table_field_sync_count . ")</font>";
								echo "</td>";
								echo "<td $use_style>" . $remote_table_field_count . "</td>";
								echo "</tr>";
							}
							echo "</table></div>";
						}
					}

					if(isset($_GET['view_local_query']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						$locid = $_GET['locid'];
						$local_query_options = "";
						$local_query_value = "";

						if(isset($_GET['view_local_query_id']))
							$view_local_query_id = $_GET['view_local_query_id'];
						else
							$view_local_query_id = 0;
						$last_local_query = mlavu_query("select * from `[1]`.`config` where `location`='[2]' and `setting`='local_query_result' order by id desc limit 20","poslavu_" . $cust_read['data_name'] . "_db",$_GET['locid']);
						while($last_local_read = mysqli_fetch_assoc($last_local_query))
						{
							if($view_local_query_id==0)
								$view_local_query_id = $last_local_read['id'];
							$local_query_options .= "<option value='".$last_local_read['id']."'";
							if($view_local_query_id==$last_local_read['id'])
							{
								$local_query_value = $last_local_read['value_long'];
								$local_query_options .= " selected";
							}
							$local_query_options .= ">" . $last_local_read['value2'] . " (" . $last_local_read['value4'] . ") " . $last_local_read['value'] . "</option>";
						}

						echo "<br><br>View Queries:<br><br>";
						echo "<select onchange='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$locid&view_local_query=1&view_local_query_id=\" + this.value'>";
						echo $local_query_options;
						echo "</select>";
						echo "<div style='overflow:auto; width:800px; height:400px; border:solid 1px #888888'>";
						echo $local_query_value;
						echo "</div><br><br>";
					}
					else if(isset($_GET['run_health_check']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						schedule_msync($cust_read['data_name'],$locserv_read['location'],"health check","","");
					}
					else if(isset($_GET['create_log_dir']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						schedule_msync($cust_read['data_name'],$locserv_read['location'],"create_log_dir","","");
					}
					else if(isset($_GET['create_directory']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						$dir = $_GET['create_directory'];
						$path = "../".$dir;
						if ($dir == "gateway_lib") $path = "../lib/".$dir;
						if ($dir == "reports") {
							schedule_msync($cust_read['data_name'],$locserv_read['location'],"create_directory","../cp/areas","");
							$path = "../cp/areas/".$dir;
						}
						schedule_msync($cust_read['data_name'],$locserv_read['location'],"create_directory",$path,"");
					}
					else if(isset($_GET['perform_records_check']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						if(isset($_GET['special_command']))
						{
							$special_command = $_GET['special_command'];
							$special_command_details = (isset($_GET['special_command_details']))?$_GET['special_command_details']:"";

							schedule_lls_special_command($cust_read['data_name'],$locserv_read['location'],$special_command,$special_command_details);
						}
					}
					else if(isset($_GET['download_item_images']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{

						//Here we do a query to generate the list of menu-items or menu-categories, and we get the list of ids from this list.
						//We get the number of menu items that have an image attached.

						$process_info = array();
						$process_info[] = array("tablename"=>"menu_items","directory"=>"items","command"=>"download_item_images");
						$process_info[] = array("tablename"=>"menu_categories","directory"=>"categories","command"=>"download_category_images");

						foreach ($process_info as $p_info) {

							$countQueryStr = "SELECT `id`,`image` FROM `".$p_info['tablename']."` WHERE `name` != ''";
							$qResult = lavu_query($countQueryStr);

							$dirBase = "/home/poslavu/public_html/admin/images/".$cust_read['data_name']."/".$p_info['directory']."/main/";
							$imageIDs = array();
							while($currItem = mysqli_fetch_assoc($qResult)){
								if(is_file($dirBase . $currItem['image'])){
									$imageIDs[] = $currItem['id'];
								}
							}

							$imagesPerSync = 10;
							$batchArr = array();
							$totalIDs = count($imageIDs);
							$numberOfBatches = ceil($totalIDs/$imagesPerSync);

							for($i=0;$i<$numberOfBatches;$i++){
								$batchArr[$i] = array();
								for($j=0; ($j<$imagesPerSync) && ($i*$imagesPerSync + $j < $totalIDs); $j++){
									$batchArr[$i][] = $imageIDs[$i*$imagesPerSync + $j];
								}
							}

							for($i=0;$i<$numberOfBatches;$i++){
								$currArr = $batchArr[$i];
								$list = implode(',' ,$currArr);
								schedule_lls_special_command($cust_read['data_name'],$locserv_read['location'],$p_info['command'],$list);
							}
						}

						echo "<script language='javascript'>window.location.replace('index.php?mode=manage_customers&submode=details&rowid=$rowid')</script>";
						exit();

					}
					else if(isset($_GET['lls_command_index_tables']) && isset($_GET['locid']) && $_GET['locid'] == $locserv_read['location']){
						$company_dataname = $cust_read['data_name'];
						$company_database = "poslavu_".$company_dataname."_db";

						$itlist = array();
						$itlist[] = "orders: opened,closed,order_id,reopened_datetime,reclosed_datetime,ioid";
						$itlist[] = "order_contents: order_id,item_id,icid";
						$itlist[] = "cc_transactions: order_id,datetime,internal_id";
						$itlist[] = "split_check_details: order_id";
						$itlist[] = "modifiers_used: order_id";
						$itlist[] = "ingredient_usage: orderid,itemid,ingredientid";
						$itlist[] = "send_log: order_id,islid";
						$itlist[] = "config: setting";
						$itlist[] = "cash_data: setting";
						$itlist[] = "med_customers: f_name,l_name";
						$itlist[] = "med_action_log: customer_id,order_id,restaurant_time,device_time";

						$arguments = $company_database . "|" . implode(";", $itlist);
						schedule_msync($cust_read['data_name'],$locserv_read['location'],"copy remote file","lls-ioncube/local/health_check.php","../local/health_check.php");
						schedule_lls_special_command($cust_read['data_name'],$locserv_read['location'],"index_tables",$arguments);

						echo "<script language='javascript'>window.location.replace('index.php?mode=manage_customers&submode=details&rowid=$rowid')</script>";
						exit();
					}
					else if(isset($_GET['catchup_syncing']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						function mlavu_schedule_remote_to_local_sync($rdb,$locationid,$sync_setting,$sync_value,$sync_value_long="")
						{
							$exist_query = mlavu_query("select * from `[1]`.`config` where `setting`='[2]' and `location`='[3]' and `value`='[4]' and `value_long`='[5]'",$rdb,$sync_setting,$locationid,$sync_value,$sync_value_long);
							if(mysqli_num_rows($exist_query))
							{
							}
							else mlavu_query("insert into `[1]`.`config` (`setting`,`location`,`value`,`value_long`,`type`) values('[2]','[3]','[4]','[5]','log')",$rdb,$sync_setting,$locationid,$sync_value,$sync_value_long);
						}

						$set_last_sync = "";
						$year = date("Y");
						$month = date("m");
						$day = date("d");
						$hour = date("H");
						$minute = date("i");
						$second = date("s");

						$set_last_sync = date("Y-m-d H:i:s",mktime($hour + 1,$minute,$second,$month,$day,$year));
						$closed_before = date("Y-m-d H:i:s",mktime($hour,$minute,$second,$month,$day - 2,$year));

						$run_local_query = "update orders set `lastsync`='$set_last_sync', `lastmod`='' where `closed`<'$closed_before' and `closed`!='0000-00-00 00:00:00' and `lastsync` < `lastmod`";
						mlavu_schedule_remote_to_local_sync("poslavu_" . $cust_read['data_name'] . "_db",$_GET['locid'],"run local query","config",$run_local_query);
					}
					else if(isset($_GET['run_local_query']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						function mlavu_schedule_remote_to_local_sync($rdb,$locationid,$sync_setting,$sync_value,$sync_value_long="")
						{
							$exist_query = mlavu_query("select * from `$rdb`.`config` where `setting`='[1]' and `location`='[2]' and `value`='[3]' and `value_long`='[4]'",$sync_setting,$locationid,$sync_value,$sync_value_long);
							if(mysqli_num_rows($exist_query))
							{
							}
							else mlavu_query("insert into `$rdb`.`config` (`setting`,`location`,`value`,`value_long`,`type`) values('[1]','[2]','[3]','[4]','log')",$sync_setting,$locationid,$sync_value,$sync_value_long);
						}

						if(isset($_POST['local_query_command']))
						{
							$run_local_query = $_POST['local_query_command'];
							if(strpos(strtolower($run_local_query),"where")===false || (strpos(strtolower($run_local_query),"select")!==false && strpos(strtolower($run_local_query),"limit")===false))
							{
								echo "<br>Query Not Allowed:<br>".$run_local_query."<br>";
							}
							else
							{
								mlavu_schedule_remote_to_local_sync("poslavu_" . $cust_read['data_name'] . "_db",$_GET['locid'],"run local query","config",$run_local_query);
							}
						}
						else
						{
							$locid = $_GET['locid'];
							echo "<form name='run_local_query_".$locid."' method='post' action='?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$locid&run_local_query=1'>";
							echo "<br>Run Local Query";
							echo "<script language='javascript' src='resources/textarea_tab.js'></script>";
							echo "<br><textarea name='local_query_command' onkeydown='return catchTab(this,event)' cols='60' rows='8'></textarea>";
							echo "<br><input type='button' value='Submit Query' onclick='if(confirm(\"Are you sure you want to execute this query?\")) document.run_local_query_".$locid.".submit()'>";
							echo "<br>";
						}
					}
					else if(isset($_GET['read_retrieved_file']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'] && isset($_GET['filedate']))
					{
						$local_filedate = $_GET['filedate'];
						$file_query = mlavu_query("select * from `[1]`.`config` where `location`='[2]' and `setting`='retrieved_local_file' and TRIM(`value`)='[3]'","poslavu_".$cust_read['data_name']."_db",$locserv_read['location'],trim($_GET['read_retrieved_file']));
						if(mysqli_num_rows($file_query))
						{
							$file_read = mysqli_fetch_assoc($file_query);

							if(isset($_POST['file_contents']) && isset($_POST['file_name']) && $_POST['file_name']==$_GET['read_retrieved_file'])
							{
								mlavu_query("update `[1]`.`config` set `value_long`='[2]', `value5`='write', `value6`='[3]' where `id`='[4]'","poslavu_".$cust_read['data_name']."_db",$_POST['file_contents'],date("Y-m-d H:i:s") . " - " . $_SERVER['REMOTE_ADDR'],$file_read['id']);
								//echo "update `[1]`.`config` set `value_long`='[2]', `value5`='write', `value6`='[3]' where `id`='[4]'<br>";
							}
							else
							{
								if($file_read['value2'] < $local_filedate)
									$file_readonly = true;
								else $file_readonly = false;

								$locid = $_GET['locid'];
								echo "<form name='update_local_file_".$locid."' method='post' action='?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$locid&filedate=$local_filedate&read_retrieved_file=".$_GET['read_retrieved_file']."'>";
								echo "<br>File Contents: " . $file_read['value'] . " (retrieved / updated " . $file_read['value2'] . " from ".$file_read['value4'].")";
								echo "<script language='javascript' src='resources/textarea_tab.js'></script>";
								echo "<br>Last Updated on Local Server: " . $local_filedate;
								echo "<br><input type='hidden' name='file_name' value='".$_GET['read_retrieved_file']."'>";
								echo "<br><textarea name='file_contents' onkeydown='return catchTab(this,event)' cols='100' rows='30'";
								if($file_readonly) echo " readonly disabled";
								echo ">";
								echo str_replace("<","&lt;",str_replace("&","&amp;",$file_read['value_long']));
								echo "</textarea>";
								if(!$file_readonly)
									echo "<br><input type='button' value='Upload Changes' onclick='if(confirm(\"Are you sure you want to upload your changes?\")) document.update_local_file_".$locid.".submit()'>";
								echo "<br>";
							}
						}
					}

					$lls_encoder = "zendguard";
					if (strpos($locserv_read['value_long'], "ioncube_loader")) $lls_encoder = "ioncube";

					$repo_dir = "lls-".$lls_encoder;

					$paths = array();
					$structure = explode("\n",$locserv_read['value_long']);
					for($i=0; $i<count($structure); $i++)
					{
						$structure_parts = explode("|",$structure[$i]);
						$fname = $structure_parts[0];
						if(count($structure_parts) > 2)
						{
							$fsize = $structure_parts[1];
							$fdate = $structure_parts[2];
							$fowner = (isset($structure_parts[3]))?$structure_parts[3]:"";
						}
						else
						{
							$fsize = "";
							$fdate = "";
							$fowner = "";
						}

						$parent_folder = substr($fname,0,strrpos($fname,"/"));
						$basename = substr($fname,strrpos($fname,"/") + 1);

						$include = true; // filter out log files older than 30 days
						if ((strstr($fname, "json_log_") || (strstr($fname, "mysql_") && strstr($fname, ".log")) || strstr($fname, "gateway_vars_") || (strstr($fname, "gateway_") && strstr($fname, ".log")) || strstr($fname, "/gateway_debug/") || (strstr($fname, "auto_correct_") && strstr($fname, ".log"))) && ((int)str_replace(array(" ", "-", ":"), array("", "", ""), $fdate) < (int)date("YmdHis", (time() - 2592000)))) $include = false;

						if ($include) $paths[] = array($parent_folder,$fname,$basename,$fsize,$fdate,$fowner);

						if(($basepath=="n/a" || strlen($parent_folder) < strlen($basepath)) && trim($fname)!="")
						{
							$basepath = $parent_folder;
						}
						/*$fname_parts = explode("/",$fname);
						for($n=1; $n<count($fname_parts); $n++)
						{
							$folder = $fname_parts[$n];
							if(!isset($path_part[$folder]))
						}*/
					}

					echo show_folders_with_parent("poslavu_".$cust_read['data_name']."_db","?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location'],$basepath,$paths);
					if(isset($_GET['_copy_remote_files']) && isset($_GET['_copy_local_files']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						schedule_msync($cust_read['data_name'],$locserv_read['location'],"copy remote file",$_GET['_copy_remote_files'],$_GET['_copy_local_files']);
						//$success = mlavu_query("insert into `poslavu_[1]_db`.`config` (`location`,`setting`,`value`,`value_long`) values ('[2]','copy remote file','[3]','[4]')",$cust_read['data_name'],$locserv_read['location'],$_GET['_copy_remote_files'],$_GET['_copy_local_files']);
						//if($success) echo "<br><b>Copying Files: </b>" . $_GET['_copy_remote_files'] . " to " . $_GET['_copy_local_files'] . "<br>";
					}
					else if(isset($_GET['update_garage_files']) && $_GET['update_garage_files']==1 && $_GET['locid']==$locserv_read['location'])
					{
						$garage_files = array("kart_addKart.php","kart_addKartWrapper.php","karts_form2.js","karts_formCSS2.css","karts_formInclude.php","kart_info.php","karts_info_wrapper.php","karts_list.php","karts_list_wrapper.php","karts_removeKart.php","karts_submitAddKartToDB.php","karts_submitAddKartToDB.php","karts_updateKart.php","part_functions.php","parts_addPart.php","parts_addPartWrapper.php","part_alphabetizer.php","part_form_select_element2.css","parts_info.php","part_info_wrapper.php","parts_list.php","parts_list_wrapper.php","parts_pit_forms1.js","parts_removePart.php","parts_updatePart3.php");
						for($n=0; $n<count($garage_files); $n++)
						{
							$set_remote_filename = "garage/" . $garage_files[$n];
							$set_local_filename = "../components/lavukart/" . $garage_files[$n];
							schedule_msync($cust_read['data_name'],$locserv_read['location'],"copy remote file",$set_remote_filename,$set_local_filename);
						}
					}
					else if(isset($_GET['remove_local_files']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						schedule_msync($cust_read['data_name'],$locserv_read['location'],"remove local file",$_GET['remove_local_files']);
						//$success = mlavu_query("insert into `poslavu_[1]_db`.`config` (`location`,`setting`,`value`) values ('[2]','remove local file','[3]')",$cust_read['data_name'],$locserv_read['location'],$_GET['remove_local_files']);
						//if($success) echo "<br><b>Removing Files: </b>" . $_GET['remove_local_files'] . "<br>";
					}
					else if(isset($_GET['upload_signature']) && isset($_GET['locid']) && $_GET['locid']==$locserv_read['location'])
					{
						if(isset($_GET['upload_signature_type']) && $_GET['upload_signature_type']=="waiver")
							$upload_type = "upload waiver";
						else
							$upload_type = "upload signature";
						schedule_msync($cust_read['data_name'],$locserv_read['location'],$upload_type,$_GET['upload_signature']);
						//$success = mlavu_query("insert into `poslavu_[1]_db`.`config` (`location`,`setting`,`value`) values ('[2]','remove local file','[3]')",$cust_read['data_name'],$locserv_read['location'],$_GET['remove_local_files']);
						//if($success) echo "<br><b>Removing Files: </b>" . $_GET['remove_local_files'] . "<br>";
					}

					//echo "<br>dn: " . $cust_read['data_name'] . " " . $locserv_read['location'] . "<br>";
					$pending_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `location`='[2]' and (`setting`='copy remote file' or `setting`='create_log_dir' or `setting`='create_directory' or `setting`='table updated' or `setting`='health check' or `setting`='records_check' or `setting`='remove local file' or `setting`='upload waiver' or `setting`='upload signature' or `setting`='read file structure' or `setting`='run local query' or `setting`='open tunnel' or `setting`='sync database' or `setting`='retrieve local file' or (`setting`='retrieved_local_file' and `value5`='write'))", $cust_read['data_name'],$locserv_read['location']);
					$pending_count = mysqli_num_rows($pending_query);
					if($pending_count > 0)
					{
						echo "<br><table bgcolor='#8888AA' cellpadding=4>";
						echo "<tr><td bgcolor='#000088' style='color:#ffffff' align='center' colspan='3'>Pending Actions (".$pending_count." Total)</td></tr>";
						while($pending_read = mysqli_fetch_assoc($pending_query))
						{
							if($pending_read['setting']=="retrieved_local_file" && $pending_read['value5']=="write")
							{
								$show_setting = "update retrieved file";
								$show_value = $pending_read['value'];
							}
							else
							{
								$show_setting = $pending_read['setting'];
								$show_value = $pending_read['value'];
								$show_value2 = $pending_read['value2'];
								$show_value_long = $pending_read['value_long'];

								if($show_setting=="records_check" && $show_value=="extra_command")
								{
									$show_setting = "extra_command";
									$show_value = $show_value2;
								}
							}
							echo "<tr>";
							echo "<td>";
							echo "<a href='index.php?mode=manage_customers&submode=details&rowid=$rowid&remove_pending=".$pending_read['id']."'>(remove)</a>";
							echo "</td>";
							echo "<td bgcolor='#ffffff'>".$show_setting."</td><td bgcolor='#ffffff'>".$show_value."</td><td bgcolor='#ffffff'>".$show_value_long."</td>";
							echo "</tr>";
						}
						echo "</table>";
					}

					if (can_access("technical"))	echo "<br>Copy Files From Repo:  <input type='text' name='copy_remote_files' id='copy_remote_files'> to local/<input type='text' name='copy_local_files' id='copy_local_files'><input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&encoder=".$lls_encoder."&_copy_local_files=\" + document.getElementById(\"copy_local_files\").value + \"&_copy_remote_files=\" + document.getElementById(\"copy_remote_files\").value'>";
					echo "<br>Remove Files:  lib/<input type='text' name='remove_local_files' id='remove_local_files'><input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&remove_local_files=\" + document.getElementById(\"remove_local_files\").value'>";
					echo "<br>Upload Signature:  <input type='text' name='upload_signature' id='upload_signature'><input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&upload_signature=\" + document.getElementById(\"upload_signature\").value + \"&upload_signature_type=\" + document.getElementById(\"upload_signature_type\").value'><select name='upload_signature_type' id='upload_signature_type'><option value='regular'>Regular</option><option value='waiver'>Waiver</option></select>";
					echo "<br>Sync Local Database Structure:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&sync_local_database_structure=1\"'>";
					echo "<br>";
					if (($cust_read['data_name']!="the_fat_cow" && $cust_read['data_name']!="honey_salt") || can_access("special_lls_file_copy")) {
						echo "<br>Update Sync Files:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&update_sync_files=1&encoder=".$lls_encoder."\"'>";
						//echo "<br>Update For 2.1:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&update_to_208=1&encoder=".$lls_encoder."\"'>";
						//echo "<br>Update For Transfer Tables:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&update_for_transfer_tables=1&encoder=".$lls_encoder."\"'>";
						//echo "<br>Update For Uncaptured Transactions (Management):  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&update_for_uncaptured_transactions=1&encoder=".$lls_encoder."\"'>";
						//echo "<br>Update Gateway Scripts for Heartland:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&update_gateway_for_heartland=1&encoder=".$lls_encoder."\"'>";
						//echo "<br>Update Gateway Scripts for USAePay:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&update_gateway_for_usaepay=1&encoder=".$lls_encoder."\"'>";
						echo "<br>";
					}
					if( can_access("lls_kds") ) {
						echo "<br />Install LLS KDS/Update sync_exec: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode={$submode}&rowid={$rowid}&locid={$locserv_read['location']}&install_lls_kds&encoder={$lls_encoder}\"' />";
					}
					echo "<br>Catchup Syncing:  <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&catchup_syncing=1\"'>";

//update orders set `lastsync`='2012-05-03 18:00:00' where closed<'2012-05-02 00:00:00'
					echo "<br>Run Local Query: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&run_local_query=1\"'>";
					echo "<br>View Local Queries: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&view_local_query=1\"'>";
					echo "<br>Perform Health Check: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&run_health_check=1\"'>";
					echo "<br>Create Log Dir: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&create_log_dir=1\"'>";
					if (can_access("special_lls_file_copy")) echo "<br>Create Gateway Lib Dir: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&create_directory=gateway_lib\"'>";
					if (can_access("special_lls_file_copy")) echo "<br>Create Reports Dir: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&create_directory=reports\"'>";
					if (can_access("special_lls_file_copy")) echo "<br>Create Test Dir: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&create_directory=test\"'>";
					echo "<br>Download Item Images to LLS: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&download_item_images=1\"'>";
					echo "<br>Optimize LLS Table Index: <input type='button' value='Submit' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&lls_command_index_tables\"'>";

					echo "<br>View Syncing and Records Check Status: <input type='button' value='Submit' onclick='window.open(\"index.php?mode=manage_customers&submode=view_records_check_status&notabs=1&rowid=$rowid&locid=".$loc_read['id']."\",\"\",\"width=800,height=600,resizable=yes,scrolling=yes,scrollbars=yes,toolbar=no\")'>";//window.open(\"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&view_records_check_status=1\",\"\",\"width=800,height=600,resizable=yes,scorllbars=yes,toolbar=no\")'>";
					//echo "<br>Export Remote Database: <input type='button' value='Submit' onclick='if(confirm(\"Are you Sure?\")) window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=".$locserv_read['location']."&export_all_remote_data=1\"'>";
				}



				/*echo "<table>";
				for($n=0; $n<count($paths); $n++)
				{
					echo "<tr><td>" . $paths[$n][0] . "</td><td width='20'>&nbsp;</td><td>" . $paths[$n][1] . "</td><td>" . $paths[$n][2] . "</td></tr>";
				}
				echo "</table>";*/

			}
		}
		else if ($submode == "clear_logs")
		{
			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rowid);
			$company_read = mysqli_fetch_assoc($company_query);
			$company_dataname = $company_read['data_name'];
			$company_db_name = "poslavu_".$company_dataname."_db";

			$earliest_log_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 7,date("Y")));
			$earliest_gateway_date = date("Y-m-d",mktime(0,0,0,date("m") - 2,date("d"),date("Y")));
			echo "<br><br>Clearing Logs Before <b>".$earliest_log_date."</b> (gateway before ".$earliest_gateway_date."):<br>";
			$flist = array();
			$cdir = "/home/poslavu/logs/company/".$company_dataname;
			$logdir_fp = opendir($cdir);
			while($read_logfile = readdir($logdir_fp))
			{
				$flist[] = $read_logfile;
			}

			sort($flist);
			for($i=0; $i<count($flist); $i++)
			{
				$sep = strrpos($flist[$i],"_");
				if($sep)
				{
					$ftype = substr($flist[$i],0,$sep);
					$frest = substr($flist[$i],$sep + 1);

					$dotpos = strpos($frest,".");
					if($dotpos) $fdate = substr($frest,0,$dotpos);
					else $fdate = $frest;

					$date_parts = explode("-",$fdate);
					if(count($date_parts) > 1)
					{
						$year = $date_parts[0];
						$month = $date_parts[1];
						if(count($date_parts) > 2)
							$day = $date_parts[2];
						else
							$day = 1;
					}
					$logdate = date("Y-m-d",mktime(0,0,0,$month,$day,$year));
					if($ftype=="gateway" && $logdate >= $earliest_gateway_date)
						$keep_log = true;
					else if($logdate >= $earliest_log_date)
						$keep_log = true;
					else
						$keep_log = false;
					if($keep_log) {$setcolor = "#008800";$phrase = "Keeping";}
					else {$setcolor = "#880000";$phrase = "Removing";}

					echo "<br><font color='$setcolor'>$phrase</font> - " . $ftype . ": " . $fdate;
					if(!$keep_log)
					{
						$log_fullpath = $cdir . "/" . $flist[$i];
						echo " <font color='#999999' style='font-size:10px'>" . $log_fullpath . "</font>";
						unlink($log_fullpath);
					}
				}
			}
		}
		else if ($submode == "convert_to_innodb")
		{
			function convert_tables_to_innodb($company_database)
			{
				$db_tables = array();
				$table_query = mlavu_query("SHOW table status from `[1]`",$company_database);
				while($table_read = mysqli_fetch_row($table_query))
				{
					$table_name = $table_read[0];
					$table_engine = $table_read[1];
					$db_tables[] = array("name"=>$table_name,"engine"=>$table_engine);
				}
				echo "Convert $company_database to InnoDB...<br><br>";

				for($i=0; $i<count($db_tables); $i++)
				{
					$table_name = $db_tables[$i]['name'];
					$table_engine = $db_tables[$i]['engine'];
					echo $table_name . " - " . $table_engine;

					if($table_engine=="MyISAM" && $company_database!="mysql")
					{
						$success = mlavu_query("alter table `[1]`.`[2]` ENGINE=INNODB",$company_database,$table_name);
						if($success) echo " <font color='#008800'><b>(Innodb transfer success)</b></font>";
						else echo " <font color='#880000'><b>(Innodb transfer failed)</b></font>";
					}
					echo "<br>";
				}
			}

			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rowid);
			$company_read = mysqli_fetch_assoc($company_query);
			$company_dataname = $company_read['data_name'];
			$company_database = "poslavu_".$company_dataname."_db";
			convert_tables_to_innodb($company_database);

			echo "<br><br><br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";
		}
		else if ($submode == "optimize_indexes")
		{
			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rowid);
			$company_read = mysqli_fetch_assoc($company_query);
			$company_dataname = $company_read['data_name'];
			$company_database = "poslavu_".$company_dataname."_db";

			$itlist = generateIndexList($company_dataname);

			echo setup_tables_for_indexing($company_database,$itlist);

			echo "<br><br><br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";
		}
		else if ($submode == "view_records_check_status" && isset($_GET['locid']))
		{
			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rowid);
			$company_read = mysqli_fetch_assoc($company_query);

			$switch_point = 0;
			$cdb = "poslavu_" . $company_read['data_name'] . "_db";
			$rc_query = mlavu_query("select * from `[1]`.`config` where `setting`='records check endpoint' order by id desc limit 1",$cdb,$_GET['locid']);
			if(mysqli_num_rows($rc_query))
			{
				$rc_read = mysqli_fetch_assoc($rc_query);
				$traverse_current_position = $rc_read['value'];
				$traverse_total_count = $rc_read['value2'];
				$traverse_check_count = $rc_read['value4'];
				$recent_check_count = $rc_read['value5'];

				$records_check_status_id = (isset($_GET['records_check_status_id']))?$_GET['records_check_status_id']:"";

				echo "<select onchange='window.location = \"index.php?mode=manage_customers&submode=view_records_check_status&notabs=1&rowid=".$_GET['rowid']."&locid=".$_GET['locid']."&records_check_status_id=\" + this.value'>";
				$rclist_query = mlavu_query("select * from `[1]`.`config` where LEFT(`setting`,20)='records check status' order by id desc",$cdb,$_GET['locid']);
				while($rclist_read = mysqli_fetch_assoc($rclist_query))
				{
					echo "<option value='".$rclist_read['id']."'";
					if($records_check_status_id==$rclist_read['id'])
						echo " selected";
					echo ">".$rclist_read['setting']."</option>";
				}
				echo "</select><br><br>";

				echo "Records check at " . $traverse_current_position . " / " . $traverse_total_count;
				if($traverse_check_count > 0 && $traverse_total_count > 0)
				{
					echo " (" . number_format(($traverse_total_count / $traverse_check_count / 6),2)  . " hours for a complete traverse)";
				}

				echo "<br>";
			}

			if($records_check_status_id=="")
				$rc_query = mlavu_query("select * from `[1]`.`config` where LEFT(`setting`,20)='records check status' order by id desc limit 1",$cdb,$_GET['locid']);
			else
				$rc_query = mlavu_query("select * from `[1]`.`config` where LEFT(`setting`,20)='records check status' and `id`='[2]' order by id desc limit 1",$cdb,$records_check_status_id);

			if(mysqli_num_rows($rc_query))
			{
				$rc_read = mysqli_fetch_assoc($rc_query);
				/*foreach($rc_read as $key => $val)
				{
					echo $key . " = " . $val . "<br><br>";
				}*/

				echo "Datetime: " . date("m/d h:i:s a",$rc_read['value']);
				echo "<br>Ipaddress: " . $rc_read['value2'];
				$match_parts = explode("<RESULT>",$rc_read['value_long']);
				$result_count = (count($match_parts) - 1);
				echo "<br>Result Count: " . $result_count;
				if($result_count > $recent_check_count)
				{
					$switch_point = $result_count - $recent_check_count;
				}
				echo "<br><br>";
				echo "<table>";
				$rc_count = 0;
				for($i=1; $i<count($match_parts); $i++)
				{
					$rc_count++;
					if($rc_count > $switch_point && $switch_point > 0)
					{
						echo "<tr><td colspan='6' bgcolor='#cccccc'>&nbsp;</td></tr>";
						$switch_point = 0;
					}
					$mparts = explode("<ORDERID>",$match_parts[$i]);
					$m_result = $mparts[0];
					$mparts = explode("<REMOTE_ORDER>",$mparts[1]);
					$m_order_id = $mparts[0];
					$mparts = explode("<LOCAL_ORDER>",$mparts[1]);
					$m_remote_order = $mparts[0];
					$m_local_order = $mparts[1];

					echo "<tr>";

					echo "<td>" . $m_result . "</td>";
					echo "<td>&nbsp;<font color='#008800'>" . $m_order_id . "</font></td>";
					echo "<td>&nbsp;<font color='#aaaaaa'>" . $m_local_order . "</font></td>";
					echo "<td>&nbsp;<font color='#888888'>" . $m_remote_order . "</font></td>";

					echo "</tr>";
				}
				echo "</table>";
			}
		}
		else if ($submode == "turn_on_tunnel" && isset($_GET['locid']))
		{
			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rowid);
			$company_read = mysqli_fetch_assoc($company_query);

			$cdb = "poslavu_" . $company_read['data_name'] . "_db";
			mlavu_query("update `[1]`.`locations` set `use_net_path`='2' where `use_net_path`='1' and `id`='[2]' limit 1",$cdb,$_GET['locid']);

			schedule_msync($company_read['data_name'],$_GET['locid'],"open tunnel");

			//echo "<br><br>Requesting Local Server Ipad Tunnel for " . $company_read['company_name'] . "...";
			echo "<center><iframe width='400' height='300' frameborder='0' src='http://www.poslavu.com/lsvpn/show_status.php?cc=".$company_read['data_name']."&ccname=".$company_read['company_name']."'></iframe></center>";
			//echo "<br><br><br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";
			//echo "<input type='button' window.open(\"http://www.poslavu.com/lsvpn/show_status.php?cc=".$company_read['data_name']."\",\"\",\"width=800,height=600,scrolling=yes,toolbar=no,scrollbars=yes\"); ";
		}
		else if ($submode == "set_deposit_status")
		{
			echo "<br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";

			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rowid);
			$company_read = mysqli_fetch_assoc($company_query);
			$company_dataname = $company_read['data_name'];
			$company_database = "poslavu_".$company_dataname."_db";

			$order_query = mlavu_query("select * from `[1]`.`orders` where `deposit_status` > 0",$company_database);
			echo "deposit orders found: " . mysqli_num_rows($order_query);

			while($order_read = mysqli_fetch_assoc($order_query))
			{
				$order_id = $order_read['order_id'];
				$loc_id = $order_read['location_id'];

				mlavu_query("update `[1]`.`cc_transactions` set `for_deposit`='1' where `order_id`='[2]' and `loc_id`='[3]'",$company_database,$order_id,$loc_id);
			}
		}
		else if ($submode == "sync_database")
		{
			echo "<style>";
			echo ".label { text-align:right; color:#999999; vertical-align:top; } ";
			echo "</style>";
			echo "<br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";

			//echo "<br><br>Temporarily disabled...";
			//exit();

			//$compvars = "comps=1&from=DEVKART&dbt=/home/poslavu/public_html/admin/components/lavukart/tables.dbt";
			$component_package = (isset($_GET['comps']))?$_GET['comps']:false;
			$model_db = (isset($_GET['from']))?$_GET['from']:"DEV";
			$dbt_file = (isset($_GET['dbt']))?$_GET['dbt']:"core_tables.dbt";
			$make_changes = true;

			//echo "Sync Database is temporarily disabled.  It will be enabled again on Monday<br><br>";
			//exit();
			echo "model db: $model_db <br>component package: $component_package<br>dbt file: $dbt_file<br><br>";

			$dev_db_info = get_database_info('poslavu_'.$model_db.'_db');
			$dev_tables = $dev_db_info['tables'];
			$dev_fields = $dev_db_info['fields'];

			$fp = fopen($dbt_file,"r");
			$coredata = fread($fp, filesize($dbt_file));
			fclose($fp);

			$core_tables = array();
			$coredata = explode("}",$coredata);
			for($i=0; $i<count($coredata); $i++)
			{
				$corerow = explode("{",$coredata[$i]);
				$corerow = trim($corerow[0]);
				if($corerow!="")
				{
					$core_tables[] = $corerow;
				}
			}

			$log = "";
			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rowid);
			$company_read = mysqli_fetch_assoc($company_query);
			$company_db_name = "poslavu_".$company_read['data_name']."_db";
			$company_db_info = get_database_info($company_db_name);
			$company_tables = $company_db_info['tables'];
			$company_fields = $company_db_info['fields'];

			$check_company = false;
			if($component_package) {
				$comp_query = mlavu_query("select * from `$company_db_name`.`locations` where `component_package`='".str_replace("'","''",$component_package)."'");
				if(mysqli_num_rows($comp_query))
					$check_company = true;
			}
			else $check_company = true;

			if($check_company)
			{
				for($i=0; $i<count($dev_tables); $i++)
				{
					$tablename = $dev_tables[$i];
					$process_table = false;
					for($n=0; $n<count($core_tables); $n++)
					{
						if($core_tables[$n]==$tablename)
							$process_table = true;
					}

					if($process_table)
					{
						if(isset($_GET['archived']))
						{
							if($tablename=="orders" || $tablename=="cc_transactions" || $tablename=="order_contents")
							{
								$dev_fields["archived_".$tablename] = $dev_fields[$tablename];
								$tablename = "archived_".$tablename;
							}
						}
						//echo "<br>" . $tablename;
						if(!isset($company_fields[$tablename])) // create new table
						{
							$fieldlist = $dev_fields[$tablename][0];
							$fields = $dev_fields[$tablename][1];

							//CREATE TABLE `poslavu_DEV_db`.`test_table` (
							//`t1` VARCHAR( 255 ) NOT NULL ,
							//`t2` INT NOT NULL ,
							//`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY
							//) ENGINE = MYISAM

							$tcode = "";
							$icode = "";
							for($n=0; $n<count($fieldlist); $n++)
							{
								$fieldname = $fieldlist[$n];
								$fieldtype = $fields[$fieldname]['type'];
								$fieldnull = $fields[$fieldname]['null'];
								$fieldkey = $fields[$fieldname]['key'];
								if(trim($fieldnull)!="") $notnull = true; else $notnull = false;
								if(trim($fieldkey)=="PRI") $usekey = true; else $usekey = false;
								if(trim($fieldkey)=="MUL") $mulkey = true; else $mulkey = false;

								if($tcode!="") $tcode .= ", ";
								$tcode .= "`$fieldname` $fieldtype";
								if($notnull) $tcode .= " NOT NULL";
								if($usekey) $tcode .= " AUTO_INCREMENT PRIMARY KEY";
								if($mulkey) $icode .= ", INDEX ($fieldname)";
							}
							$tcode .= $icode;
							$query = "CREATE TABLE `$company_db_name`.`$tablename` ($tcode) ENGINE = INNODB";
							if($make_changes)
							{
								$str = "<br><font color='blue'>Executing:</font> $query";
								$success = mlavu_query($query);
								if($success) $str .= " <b><font color='green'>(Success)</font></b>";
								else $str .= " <b><font color='red'>(Failed)</font></b>";
								$str .= "<br>";
								echo $str;
								$log .= $str;
							}
							else echo "<br><br>" . $query;
						}
						else
						{
							$fieldlist = $dev_fields[$tablename][0];
							$fields = $dev_fields[$tablename][1];

							for($n=0; $n<count($fieldlist); $n++)
							{
								$fieldname = $fieldlist[$n];
								$fieldtype = $fields[$fieldname]['type'];

								if($fieldname=="id" && strpos(strtolower($fieldtype),"int")===false && $fields[$fieldname]['key']=="PRI")
								{
									//echo "FOUND NON INT ID: $tablename . $fieldname ".$fields[$fieldname]['key']."<br>";
									$fieldtype = "int(11)";
								}

								if(!isset($company_fields[$tablename][1][$fieldname])) // create new column
								{
									$query = "ALTER TABLE `$company_db_name`.`$tablename` ADD `$fieldname` $fieldtype NOT NULL";
									if($make_changes)
									{
										$str = "<br><font color='blue'>Executing:</font> $query";
										$success = mlavu_query($query);
										if($success) $str .= " <b><font color='green'>(Success)</font></b>";
										else $str .= " <b><font color='red'>(Failed)</font></b>";
										$str .= "<br>";
										echo $str;
										$log .= $str;
									}
									else echo "<br><br>" . $query;
								}
								else
								{
									// check column type
									$company_fieldtype = $company_fields[$tablename][1][$fieldname]['type'];
									if($fieldtype!=$company_fieldtype)
									{
										if($fieldname=="id")
											$query = "ALTER TABLE `$company_db_name`.`$tablename` CHANGE `$fieldname` `$fieldname` int(11) auto_increment NOT NULL";
										else
											$query = "ALTER TABLE `$company_db_name`.`$tablename` CHANGE `$fieldname` `$fieldname` $fieldtype NOT NULL";

										if($make_changes)
										{
											$str = "<br><font color='blue'>Executing:</font> $query";
											$success = mlavu_query($query);
											if($success) $str .= " <b><font color='green'>(Success)</font></b>";
											else $str .= " <b><font color='red'>(Failed)</font></b>";
											$str .= "<br>";
											echo $str;
											$log .= $str;
										}
										else echo "<br><br>" . $query;
									}
								}
							}
						}
					}
				}
			}

			mlavu_query("insert into `poslavu_MAIN_db`.`updatelog` (`type`,`date`,`details`,`account`) values ('single database sync (".$company_read['company_name'].")','".date("Y-m-d H:i:s")."','".str_replace("'","''",$log)."','".str_replace("'","''",account_loggedin())."')");

			echo "<br><br><br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";

		}
		else if ($submode == "search_logs")
		{
			require_once("sacp_search_logs.php");
		}
		else if ($submode == "old_search_logs")
		{
			require_once("old_sacp_search_logs.php");
		}
		else if ($submode == "user_restore")
		{
			require_once("user_restore.php");
		}
		else if ($submode == "revert_inv_1")
		{
			require_once("revert_inv_1.php");
		}
		else if ($submode == "old_inventory_ingredients")
		{
			require_once("old_inventory_ingredients.php");
		}
	}

	function manage_extend_billing($signupid,$add_days)
	{
		$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where id='[1]'",$signupid);
		if(mysqli_num_rows($signup_query))
		{
			$signup_read = mysqli_fetch_assoc($signup_query);

			function add_days_to_date($sdate,$days)
			{
				$sdate_parts = explode("-",$sdate);
				if(count($sdate_parts) > 2)
				{
					return date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + $days,$sdate_parts[0]));
				}
				else return false;
			}

			$arb_license_id = $signup_read['arb_license_id'];
			$arb_hosting_id = $signup_read['arb_hosting_id'];
			$arb_license_newdate = add_days_to_date($signup_read['arb_license_start'],$add_days);
			$arb_hosting_newdate = add_days_to_date($signup_read['arb_hosting_start'],$add_days);

			$extend_arbs = array();
			$extend_arbs[] = array($arb_license_id,$arb_license_newdate,"arb_license_start");
			$extend_arbs[] = array($arb_hosting_id,$arb_hosting_newdate,"arb_hosting_start");

			$success = false;
			for($i=0; $i<count($extend_arbs); $i++)
			{
				$arbid = $extend_arbs[$i][0];
				$edate = $extend_arbs[$i][1];
				$set_field = $extend_arbs[$i][2];

				$vars['refid'] = 16;
				$vars['subscribeid'] = $arbid;
				$vars['start_date'] = $edate;
				//echo $vars['subscribeid'] . "<br>" . $vars['start_date'] . "<br>";
				$extend_info = manage_recurring_billing("update_start",$vars);
				if($extend_info['success'])
				{
					mlavu_query("update `poslavu_MAIN_db`.`signups` set `$set_field`='[1]' where `id`='[2]'",$edate,$signupid);
					$success = true;
				}
				else
				{
					$success = false;
					//echo "Failed: " . $extend_info['error'] . "<br>";
				}
				//echo "<br>response: " . str_replace("<","&lt;",$extend_info['response']) . "<br>";
			}
			if($success)
			{
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where id='[1]'",$signupid);
				if(mysqli_num_rows($signup_query))
				{
					return mysqli_fetch_assoc($signup_query);
				}
			}
		}
		return false;
	}

	function set_arb_info_dates($signup_read,$dataname)
	{
		if($signup_read['arb_license_start']=="" || $signup_read['arb_hosting_start']=="")
		{
			$sdate = $signup_read['date'];
			$sdate_parts = explode("-",$sdate);
			if(count($sdate_parts) > 2)
			{
				$license_start = date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + 14,$sdate_parts[0]));
				$hosting_start = date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + 30,$sdate_parts[0]));

				mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_license_start`='[1]', `arb_hosting_start`='[2]' where `id`='[3]'",$license_start,$hosting_start,$signup_read['id']);
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$dataname);
				$signup_read = mysqli_fetch_assoc($signup_query);
				//echo "applying: $license_start $hosting_start<br>";
			}
		}
		return $signup_read;
	}

	function lookup_arb_info($signup_read,$dataname="")
	{
		$str = "";
		$arb_active_ids = array();
		$str .= "reading:";

		if($dataname!="")
			$signup_read = set_arb_info_dates($signup_read,$dataname);

		if(isset($signup_read['arb_license_id']) && trim($signup_read['arb_license_id'])!="")
		{
			$vars['refid'] = 16;
			$vars['subscribeid'] = $signup_read['arb_license_id'];
			$hosting_info = manage_recurring_billing("info",$vars);
			if($hosting_info['success'])
			{
				$hosting_status = get_tag_value("Status",$hosting_info['response']);
				mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_license_status`='[1]' where id='[2]'",$hosting_status,$signup_read['id']);
				if($hosting_status=="expired") $hosting_status = "charged";
				else if($hosting_status=="active") $arb_active_ids[] = $signup_read['arb_license_id'];
				$str .= " - " . $hosting_status;
			}
			else
				$str .= " - not found";
			$str .= "<br>";
		}
		if(isset($signup_read['arb_hosting_id']) && trim($signup_read['arb_hosting_id'])!="")
		{
			$vars['refid'] = 16;
			$vars['subscribeid'] = $signup_read['arb_hosting_id'];
			$hosting_info = manage_recurring_billing("info",$vars);
			if($hosting_info['success'])
			{
				$hosting_status = get_tag_value("Status",$hosting_info['response']);
				mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_hosting_status`='[1]' where id='[2]'",$hosting_status,$signup_read['id']);
				if($hosting_status=="expired") $hosting_status = "charged";
				else if($hosting_status=="active") $arb_active_ids[] = $signup_read['arb_hosting_id'];
				$str .= " - " . $hosting_status;
			}
			else
				$str .= " - not found";
		}
		$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `id`='[1]'",$signup_read['id'], null, null, null, null, null, false);
		return mysqli_fetch_assoc($signup_query);
	}

	// log into customer accounts via super admin
	function execute_cplogin($loggedin,$cplogin,$ccode,$loginmode)
	{
		$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]' and `company_code`='[2]'",$cplogin,$ccode);
		if(mysqli_num_rows($cust_query))
		{
			$cust_read = mysqli_fetch_assoc($cust_query);

			require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

			if($loginmode=="create_temp")
			{
				global $loggedin;

				$username_info = get_temp_username($loggedin);
				$use_username = $username_info['use_username'];
				$user_exists = $username_info['user_exists'];
				$old_dataname = $username_info['old_dataname'];

				$vars = array();
				$vars['username'] = $use_username;
				$vars['password'] = rand(1111,9999);
				$vars['dataname'] = $ccode;
				$vars['restaurant'] = $cplogin;
				$vars['type'] = "lavuadmin";
				$vars['userid'] = $user_exists;

				$vars['f_name'] = "LavuAdmin";
				$vars['l_name'] = $use_username;
				$vars['email'] = "info@poslavu.com";
				$vars['access_level'] = 4;
				$vars['PIN'] = "8228";
				$vars['quick_serve'] = 0;

				$data_name = $ccode;
				$lavu_query_should_log = true;

				$new_db = "poslavu_" . $ccode . "_db";
				if($user_exists)
				{
					mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `username`='[username]' ,`dataname`='[dataname]',`restaurant`='[restaurant]',`type`='[type]' where `id`='[userid]'",$vars);
				}
				else
				{
					mlavu_query("insert into `poslavu_MAIN_db`.`customer_accounts` (`username`,`dataname`,`restaurant`,`type`) values ('[username]','[dataname]','[restaurant]','[type]')",$vars);
				}

				$use_pin = "";
				$try_pin = rand(11111,99999);
				while($use_pin=="")
				{
					$pin_query = mlavu_query("select * from `$new_db`.`users` where `PIN`='[1]'",$try_pin);
					if(mysqli_num_rows($pin_query))
					{
						$try_pin = rand(11111,99999);
					}
					else
					{
						$use_pin = $try_pin;
					}
				}
				$vars['password'] = $use_pin;
				$vars['PIN'] = $use_pin;

				mlavu_query("insert into `$new_db`.`users` (`company_code`,`username`,`f_name`,`l_name`,`password`,`email`,`access_level`,`PIN`,`quick_serve`, `lavu_admin`, `active`) values ('[dataname]','[username]','[f_name]','[l_name]',PASSWORD('[password]'),'[email]','[access_level]','[PIN]','[quick_serve]', '1', '1')",$vars);

				$get_loc_info = mlavu_query("SELECT `id`, `use_net_path`, `net_path` FROM `$new_db`.`locations` LIMIT 1");
				if (mysqli_num_rows($get_loc_info) > 0) {
					$location_info = mysqli_fetch_assoc($get_loc_info);
					if ((int)$location_info['use_net_path']>0 && !strstr($location_info['net_path'], "poslavu.com")) {
						$exist_query = mlavu_query("select * from ".$new_db.".`config` where `setting`='[1]' and `location`='[2]' and `value`='[3]' and `value_long`='[4]'","table updated",$location_info['id'],"users","");
						if(mysqli_num_rows($exist_query))
						{
						}
						else mlavu_query("insert into ".$new_db.".`config` (`setting`,`location`,`value`,`value_long`,`type`) values('[1]','[2]','[3]','[4]','log')","table updated",$location_info['id'],"users","");
					}
				}

				echo "Temp Account for ".$ccode.":";
				echo "<br>username: " . $vars['username'];
				echo "<br>password & PIN: " . $vars['password'];
				echo "<script language='javascript'>";
				echo "window.resizeTo(360,320); ";
				echo "</script>";
			}
			else if($loginmode=="cp")
			{
				$dataname = $cust_read['company_code'];
				$userid = 1000;
				$username = "poslavu_admin";
				$full_username = "POSLavu Admin";
				$email = "info@poslavu.com";
				$companyid = $cust_read['id'];
				echo $dataname;
				echo $cust_read['id'];
				echo $cust_read['dev'];
				$result= admin_login($dataname, $userid, $username, $full_username, $email, $companyid, "", $cust_read['dev'], "4", "", "", "0", "1", $cust_read['chain_id']);
				echo $result;
				echo "<script language='javascript'>";

				$cpURL = '/cp/index.php';

				// Users with an already openned session will need to log in again to store their 
				// pasword as a session variable
				if (!isset($_SESSION['posadmin_password'])) {
					echo "alert('Please log out from your current manage session in order to enable single sign-on')</script>";
					exit();
				}

				$data_string = json_encode([
					'username' => $_SESSION['posadmin_loggedin'],
					'password' => $_SESSION['posadmin_password'],
					'dataname' => $dataname,
					'skipPHP' => false,
				]);

				$ch = curl_init($_SERVER['HUBQL_URL']."/loginAdmin");
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, [
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string),
				]);
				$result = curl_exec($ch);
				curl_close($ch);
				$data = json_decode($result);

				if ($data->status === 200) {
					$cpDomain = getStandardDomain();

					echo '
						var expires = new Date(new Date().getTime() + '.$_SERVER['CP3_SESSION_EXPIRE_DAYS'].' * 24 * 60 * 60 * 1000);
						var cookie = "LAVU_TOKEN='.$data->token.';expires=" + expires.toGMTString() + ";path=/;domain='.$cpDomain.'";
						document.cookie = cookie;
					';
				} else if ($data->status === 401) {
					echo "alert('".$data->message."'); window.close(); </script>";
					exit();
				}

				if (isNewControlPanelActive()) {
					$cpURL = $_SERVER['CP3_URL']."/home";
				}

				echo "window.location.replace('".$cpURL."'); ";
				echo "</script>";
			}
			exit();
		}
	}
?>
