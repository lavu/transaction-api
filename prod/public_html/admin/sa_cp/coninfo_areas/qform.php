<?php
	$qformurl = "http://salestools.lavu.com/quote-form.html";
	$qbaseurl = "http://salestools.lavu.com";
	$fbaseurl = "https://admin.poslavu.com/sa_cp/coninfo_areas/qform.php";
	
	$ch = curl_init($qformurl);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	//curl_setopt($ch, CURLOPT_POST, 1);
	//curl_setopt($ch, CURLOPT_POSTFIELDS, "");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($ch);
	curl_close($ch);
	
	$rpc = array();
	$rpaths = array("/files/","/uploads/");
	$rpc[] = array("action=\"","action=\"$fbaseurl?submit=1\" wbaction=\"");
		
	for($r=0; $r<count($rpaths); $r++)
	{
		$rstr = $rpaths[$r];
		$rstr_to = $qbaseurl . $rstr;
		$rpc[] = array($rstr, $rstr_to);
	}
	for($r=0; $r<count($rpc); $r++)
	{
		$rstr = $rpc[$r][0];
		$rstr_to = $rpc[$r][1];
		
		$response = str_replace(".com".$rstr,"[----com_temprpc----]",$response);
		$response = str_replace($rstr_to,"[----temprpc----]",$response);
		$response = str_replace($rstr,$rstr_to,$response);
		$response = str_replace("[----temprpc----]",$rstr_to,$response);
		$response = str_replace("[----com_temprpc----]",".com".$rstr,$response);
	}
	$response = str_replace("http://","https://",$response);
	
	function find_tag_contents($tag,$str)
	{
		$parts = array();
		$str_parts = explode("<".$tag,$str);
		for($n=1; $n<count($str_parts); $n++)
		{
			$inner_parts = explode(">",$str_parts[$n],2);
			if(count($inner_parts)>1)
			{
				$parts[] = trim($inner_parts[0]);
			}
		}
		return $parts;
	}
	
	function find_tag_property($str,$prop)
	{
		$delims = array();
		$delims[] = array("=\"","\"");
		$delims[] = array("= \"","\"");
		$delims[] = array(" =\"","\"");
		$delims[] = array(" = \"","\"");
		$delims[] = array("='","'");
		$delims[] = array("= '","'");
		$delims[] = array(" ='","'");
		$delims[] = array(" = '","'");
		for($d=0; $d<count($delims); $d++)
		{
			$dstart = $delims[$d][0];
			$dend = $delims[$d][1];
			$str_parts = explode($prop . $dstart,$str);
			if(count($str_parts) > 1)
			{
				$str_parts = explode($dend,$str_parts[1],2);
				if(count($str_parts) > 1)
				{
					return trim($str_parts[0]);
				}
			}
		}
		return "";
	}
	
	function find_text_inputs($str,$label,$label_type)
	{
		$inputs = array();
		$tag_contents = find_tag_contents("input",$str);
		for($i=0; $i<count($tag_contents); $i++)
		{
			//echo "<textarea rows='1' cols='120'>" . str_replace("<","&lt;",$tag_contents[$i]) . "</textarea><br>";
			$tstr = find_tag_property($tag_contents[$i],"id");
			//echo "found: $tstr<br>";
			if($tstr!="") $inputs[] = array("id"=>$tstr);
		}
		return array("label"=>$label,"label_type"=>$label_type,"inputs"=>$inputs);
	}
	
	$input_list = array();
	$response_parts = explode("<label class=\"wsite-form-label",$response);
	for($i=1; $i<count($response_parts); $i++)
	{
		$lparts = explode("</label>",$response_parts[$i],2);
		if(count($lparts) > 1)
		{
			$outer_part = $lparts[1];
			$lparts = explode(">",$lparts[0],2);
			if(count($lparts)>1)
			{
				$labelstr = trim(strip_tags(str_replace("*","",$lparts[1])));
				//echo $labelstr . "<br>";
				
				$slparts = explode("<label class=\"wsite-form",$outer_part);
				//echo $labelstr . "<br>";
				//echo "<textarea rows=8 cols=120'>" . str_replace("<","&lt;",$slparts[0]) . "</textarea><br>";
				$input_list[] = find_text_inputs($slparts[0],$labelstr,"primary");
				//for($z=0; $z<count($input_list); $z++) echo $input_list[$z]['inputs']['id'] . "<br>";
				
				for($n=1; $n<count($slparts); $n++)
				{
					$islparts = explode("</label>",$slparts[$n]);
					if(count($islparts) > 1)
					{
						$isl_outer_part = $islparts[1];
						$islparts = explode(">",$islparts[0],2);
						if(count($islparts)>1)
						{
							$sublabelstr = trim(strip_tags(str_replace("*","",$islparts[1])));
							//echo "-------- " . $sublabelstr . "<br>";
							$input_list[] = find_text_inputs($isl_outer_part,$sublabelstr,"sub");
						}
					}
				}
			}
		}
	}
	
	for($i=0; $i<count($input_list); $i++)
	{
		$iarr = $input_list[$i];
		$label_type = $iarr['label_type'];
		$label = $iarr['label'];
		echo $label . " ";
		for($n=0; $n<count($iarr['inputs']); $n++)
		{
			echo " >>>>>>>> " . $iarr['inputs'][$n]['id'];
		}
		echo "<br>";
	}
	
	//echo $response;
?>
