<?php
	if($rmode!="")
	{
		function valid_usa_state_abbrev($state_str)
		{
			$state_str = strtolower(trim($state_str));
			$valid_states = "AL:Alabama,
			AK:Alaska,
			AZ:Arizona,
			AR:Arkansas,
			CA:California,
			CO:Colorado, 
			CT:Connecticut, 
			DE:Delaware, 
			DC:District Of Columbia, 
			FL:Florida, 
			GA:Georgia, 
			HI:Hawaii, 
			ID:Idaho, 
			IL:Illinois, 
			IN:Indiana, 
			IA:Iowa, 
			KS:Kansas, 
			KY:Kentucky, 
			LA:Louisiana, 
			ME:Maine, 
			MD:Maryland, 
			MA:Massachusetts, 
			MI:Michigan, 
			MN:Minnesota, 
			MS:Mississippi, 
			MO:Missouri, 
			MT:Montana, 
			NE:Nebraska, 
			NV:Nevada, 
			NH:New Hampshire, 
			NJ:New Jersey, 
			NM:New Mexico, 
			NY:New York, 
			NC:North Carolina, 
			ND:North Dakota, 
			OH:Ohio, 
			OK:Oklahoma, 
			OR:Oregon, 
			PA:Pennsylvania, 
			RI:Rhode Island, 
			SC:South Carolina, 
			SD:South Dakota, 
			TN:Tennessee, 
			TX:Texas, 
			UT:Utah, 
			VT:Vermont, 
			VA:Virginia, 
			WA:Washington, 
			WV:West Virginia, 
			WI:Wisconsin, 
			WY:Wyoming";
			$state_parts = explode(",",$valid_states);
			for($n=0; $n<count($state_parts); $n++)
			{
				$inner_state_parts = explode(":",$state_parts[$n]);
				if(count($inner_state_parts)==2)
				{
					$abbrev = strtolower(trim($inner_state_parts[0]));
					$statename = strtolower(trim($inner_state_parts[1]));
					
					//echo "abbrev: $abbrev - statename: $statename - state_str: $state_str<br>";
					if($state_str==$abbrev || $state_str==$statename)
					{
						return $abbrev;
					}
				}
			}
			return "";
		}
		
		$rsubmode = (isset($_GET['rsubmode']))?$_GET['rsubmode']:"";
		if($rsubmode=="get-data")
		{
			$last_id = 0;
			$last_id_count = 0;
			$min_last_activity = date("Y-m-d H:i:s",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
			if(isset($_SESSION['last_id']))
			{
				$last_id = $_SESSION['last_id'];
				$last_id_count = $_SESSION['last_id_count'];
			}
			else
			{
				$last_id_query = mlavu_query("select `restid` * 1 as `restid` from `poslavu_MAIN_db`.`menu_usage_data` order by `restid` desc, `id` desc limit 1");
				if(mysqli_num_rows($last_id_query))
				{
					$last_id_read = mysqli_fetch_assoc($last_id_query);
					$last_id = $last_id_read['restid'] * 1;
				}
			}
			echo "last_id: $last_id<br>";
			
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `last_activity` >= '[1]' and `id` * 1 > '[2]' order by id asc limit 1",$min_last_activity,$last_id);
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				$rest_id = $rest_read['id'];
				$rest_dataname = $rest_read['data_name'];
				$rest_db = "poslavu_" . $rest_dataname . "_db";
				
				$loc_query = mlavu_query("select * from `[1]`.`locations` order by id desc limit 1",$rest_db);
				if(mysqli_num_rows($loc_query))
				{
					$loc_read = mysqli_fetch_assoc($loc_query);
					$loc_state = $loc_read['state'];
					
					$state_abbrev = valid_usa_state_abbrev($loc_state);
					if($state_abbrev!="")
					{		
						echo "State: $loc_state<br>";
						mlavu_query("delete from `poslavu_MAIN_db`.`menu_usage_data` where `restid`='[1]' and `type`='menu_item'",$rest_id);
						$content_result = mlavu_query("insert into `poslavu_MAIN_db`.`menu_usage_data` select NULL as `id`,`closed` as `datetime`,`order_contents`.`loc_id` as `loc_id`,`order_contents`.`order_id` as `order_id`,`order_contents`.`id` as `source_id`,`quantity` as `qty`,'menu_item' as `type`,`order_contents`.`subtotal` as `cost`,`price` as `unit_cost`,`item` as `title`,`menu_categories`.`id` as `parent_id`,`menu_categories`.`name` as `parent_title`,'[1]' as `rest_id`,`item_id` as `type_id`,'[2]' as `dataname`,'[3]' as `state` from `[4]`.`order_contents` left join `[4]`.`orders` on `order_contents`.`order_id`=`orders`.`order_id` left join `[4]`.`menu_categories` on `order_contents`.`category_id`=`menu_categories`.`id` where `item` NOT LIKE 'SENDPOINT' and `item`!='' and `closed` >= '2014-00-00 00:00:00' order by `closed` asc",$rest_id,$rest_dataname,$state_abbrev,$rest_db);
						$count = ConnectionHub::getConn('poslavu')->affectedRows();
						if($count==-1)
						{
							echo mlavu_dberror();
						}
						echo "Count ($rest_id - $rest_dataname): ".number_format($count,0)."<br>";
						$_SESSION['last_id_count'] = $last_id_count + 1;
					}
					else echo "Not a Valid USA State ($rest_id - $rest_dataname): $loc_state<br>";
					echo "<br>Processed this time: " . ($last_id_count) . "<br>";
					$_SESSION['last_id'] = $rest_id;
				}
			}
			
			if(($last_id_count) <= 200)
			{
				echo "<script type='text/javascript'>";
				echo "	function reload_page() { ";
				echo "		window.location = '/sa_cp/index.php?mode=intrep&rmode=menu-usage&rnd=".rand(1000,9999)."'; ";
				echo "	} ";
				echo "	setTimeout('reload_page()',2000); ";
				echo "</script>";
			}
		}
		else
		{
			function create_search_select_dropdown($options,$def="")
			{
				$optionstr = "";
				$optionstr .= "<option value=''>----select----</option>";
				for($i=0; $i<count($options); $i++)
				{
					$opt_title = $options[$i]['title'];
					$opt_value = str_replace(" ","_",strtolower($opt_title));
					$optionstr .= "<option value='$opt_value'";
					if($opt_value==$def) $optionstr .= " selected";
					$optionstr .= ">$opt_title</option>";
				}
				return $optionstr;
			}
			
			function search_select_value($value,$options,$fieldname,$def="")
			{
				for($i=0; $i<count($options); $i++)
				{
					$opt_title = $options[$i]['title'];
					$opt_value = str_replace(" ","_",strtolower($opt_title));
					if($value==$opt_value)
					{
						return $options[$i][$fieldname];
					}
				}
				return $def;
			}
			
			$search_types = array();
			$search_types[] = array("title"=>"Menu Items","name"=>"menu_item");
			$search_types[] = array("title"=>"Modifiers","name"=>"modifier");
			$search_types[] = array("title"=>"Ingredients","name"=>"ingredient");

			$search_fields = array();
			$search_fields[] = array("title"=>"Item Name or Category Name","fields"=>"title,parent_title");
			$search_fields[] = array("title"=>"Item Name","fields"=>"title");
			$search_fields[] = array("title"=>"Category Name","fields"=>"parent_title");
			
			$groupby_fields = array();
			$groupby_fields[] = array("title"=>"Item Name and Category Name","groupby"=>"title,parent_title");
			$groupby_fields[] = array("title"=>"Item Name","groupby"=>"title");
			$groupby_fields[] = array("title"=>"Category Name","groupby"=>"parent_title");
			$groupby_fields[] = array("title"=>"State","groupby"=>"state");
			$groupby_fields[] = array("title"=>"Month","groupby"=>"left(`datetime`,7) as `month`");
			$groupby_fields[] = array("title"=>"Day","groupby"=>"left(`datetime`,10) as `day`");
			$groupby_fields[] = array("title"=>"Location","groupby"=>"dataname");

			$orderby_fields = array();
			$orderby_fields[] = array("title"=>"Count","orderby"=>"count");
			$orderby_fields[] = array("title"=>"Quantity","orderby"=>"qty");
			$orderby_fields[] = array("title"=>"Cost","orderby"=>"cost");
			$orderby_fields[] = array("title"=>"Average Cost","orderby"=>"average_cost");
			
			$validate_fields = array("searchtype","searchby","searchterm","groupby");
			$type_value = (isset($_POST['type']))?$_POST['type']:"menu_items";
			$searchby_value = (isset($_POST['searchby']))?$_POST['searchby']:"item_name_or_category_name";
			$searchterm = (isset($_POST['searchterm']))?$_POST['searchterm']:"";
			$groupby_value = (isset($_POST['groupby']))?$_POST['groupby']:"";
			$orderby_value = (isset($_POST['orderby']))?$_POST['orderby']:"count";
						
			echo "<script type='text/javascript'>";
			echo "	function search_click() { ";
			for($n=0; $n<count($validate_fields); $n++)
			{
				$field = $validate_fields[$n];
				echo "if(document.search.$field.value=='') alert('Please provide values for all fields to perform a search'); else ";
			}
			echo "		document.search.submit(); ";
			echo "	} ";
			echo "</script>";
			echo "<form name='search' method='post' action='/sa_cp/index.php?mode=$mode&rmode=$rmode&rsubmode=$rsubmode'>";
			echo "	<table cellspacing=0 cellpadding=6>";
			echo "		<tr><td align='right' valign='top'>Search Type</td><td valign='top'>";
			echo "			<select name='searchtype'>".create_search_select_dropdown($search_types,$type_value)."</select>";
			echo "		</td></tr>";
			echo "		<tr><td align='right' valign='top'>Search By</td><td valign='top'>";
			echo "			<select name='searchby'>".create_search_select_dropdown($search_fields,$searchby_value)."</select>";
			echo "		</td></tr>";
			echo "		<tr><td align='right' valign='top'>Search Term</td><td valign='top'>";
			
			/*echo "<table>";
			for($r=1; $r<=3; $r++)
			{
				if($r > 1)
				{
					echo "<tr>";
					echo "<td align='center' style='color:#aaaaaa'>-or-</td>";
					echo "</tr>";
				}
				echo "<tr>";
				for($c=1; $c<=3; $c++)
				{
					if($c > 1)
					{
						echo "<td valign='middle' style='color:#aaaaaa'>-and-</td>";
					}
					echo "<td><textarea rows='2' cols='25' name='searchterm'>".str_replace("<","&lt;",$searchterm)."</textarea></td>";
				}
				echo "</tr>";
			}
			echo "</table>";*/
			
			echo "			<textarea rows='2' cols='25' name='searchterm'>".str_replace("<","&lt;",$searchterm)."</textarea>";
			echo "		</td></tr>";
			echo "		<tr><td align='right' valign='top'>Group By</td><td valign='top'>";
			echo "			<select name='groupby'>".create_search_select_dropdown($groupby_fields,$groupby_value)."</select>";
			echo "		</td></tr>";
			echo "		<tr><td align='right' valign='top'>Order By</td><td valign='top'>";
			echo "			<select name='orderby'>".create_search_select_dropdown($orderby_fields,$orderby_value)."</select>";
			echo "		</td></tr>";
			echo "		<tr><td align='right' valign='top'>&nbsp;</td><td valign='top'>";
			echo "			<input type='button' value='Search >>' onclick='search_click()' />";
			echo "		</td></tr>";
			echo "	</table>";
			echo "</form>";
		
			$conn = ConnectionHub::getConn('poslavu');

			if($type_value!="" && $searchby_value!="" && $searchterm!="" && $groupby_value!="")
			{
				$tablename = search_select_value($type_value,$search_types,"name");
				$wherefields = search_select_value($searchby_value,$search_fields,"fields");
				$groupbycond = search_select_value($groupby_value,$groupby_fields,"groupby");
				//echo "table: $tablename<br>fields: $wherefields<br>groupby: $groupbycond<br>searchterm: $searchterm<br>";
				
				$where_fieldlist = explode(",",$wherefields);
				$search_fieldlist = array();
				for($n=0; $n<count($where_fieldlist); $n++) $search_fieldlist[$where_fieldlist[$n]] = "1";
				$select_fieldlist = $where_fieldlist;
				$groupby_code = "";
				if(strpos($groupbycond," as "))
				{
					$groupby_parts = explode(" as ",$groupbycond);
					if(count($groupby_parts) > 1)
					{
						$groupby_field = trim(str_replace("`","",$groupby_parts[1]));
						$select_fieldlist = array_merge(array($groupbycond),$select_fieldlist);
						$groupby_code = "`".$conn->escapeString($groupby_field)."`";
					}
				}
				else
				{
					$groupby_fieldlist = explode(",",$groupbycond);
					$append_selectlist = array();
					for($n=0; $n<count($groupby_fieldlist); $n++)
					{
						$append_selectlist[] = trim($groupby_fieldlist[$n]);
						if($groupby_code!="") $groupby_code .= ", ";
						$groupby_code .= "`".$conn->escapeString($groupby_fieldlist[$n])."`";
					}
					$select_fieldlist = array_merge($append_selectlist,$select_fieldlist);
				}
				
				if($groupby_code!="")
				{
					$selectstr = "";
					for($n=0; $n<count($select_fieldlist); $n++)
					{
						if($selectstr!="") $selectstr .= ", ";
						if(strpos($select_fieldlist[$n],"`")!==false)
							$selectstr .= $select_fieldlist[$n];
						else if(strpos($select_fieldlist[$n],".")!==false)
							$selectstr .= $select_fieldlist[$n];
						else
							$selectstr .= "`" . $select_fieldlist[$n] . "`";
					}
					if($selectstr!="")
					{
						$selectstr .= ", count(*) as `count`, sum(`qty`) as `qty`, sum(`cost`) as `cost`, (sum(`cost`) / sum(`qty`)) as `average_cost`";
						$wherestr = "";
						for($n=0; $n<count($where_fieldlist); $n++)
						{
							if($wherestr!="") $wherestr .= " or ";
							$wherestr .= "`" . $conn->escapeString(trim(str_replace("`","",$where_fieldlist[$n]))) . "` LIKE '".$conn->escapeString($searchterm)."'";
						}
						$wherestr = "`type`='".$conn->escapeString($tablename)."' and ($wherestr)";

						$orderby_code = trim(strtolower($orderby_value));
						for($n=0; $n<count($orderby_fields); $n++)
						{
							if(trim(strtolower($orderby_value))==trim(strtolower($orderby_fields[$n]['title']))) $orderby_code = $orderby_fields[$n]['orderby'];
						}
	
						$groupbystr = $groupby_code;
						$orderbystr = "`".$conn->escapeString($orderby_code)."` desc";
						$tablename = "`poslavu_MAIN_db`.`menu_usage_data`";
						
						$start_ts = time();
						
						$rowstr = "";
						$headerstr = "";
						$query ="select $selectstr from $tablename where $wherestr group by $groupbystr order by $orderbystr limit 1000";
						echo "<br><table cellspacing=0 cellpadding=8 bgcolor='#eeeeee' style='border:solid 1px #888888'><tr><td>" . $query . "</td></tr></table><br>";
						$success = $result_query = mlavu_query($query);
						if(!$success)
						{
							echo mlavu_dberror() . "<br>";
						}
						while($result_read = mysqli_fetch_assoc($result_query))
						{
							if($headerstr=="")
							{
								$headerstr .= "<tr>";
								foreach($result_read as $k => $v)
								{
									$headerstr .= "<td class='hcol'>" . ucwords(str_replace("_"," ",$k)) . "</td>";
								}
								$headerstr .= "</tr>";
							}
							$rowstr .= "<tr>";
							$rowcolcount = 0;
							foreach($result_read as $k => $v)
							{
								$rowcolcount++;
								if($rowcolcount==1) 
								{
									$rclass = "rcolfirst";
									$showv = $v;
								}
								else if(strpos($k,"cost")!==false) 
								{
									$showv = "$" . number_format($v,2);
									$rclass = "rcolnumber";
								}
								else if(strpos($k,"count")!==false || strpos($k,"qty")!==false) 
								{
									$showv = number_format($v,0);
									$rclass = "rcolnumber";
								}
								else 
								{
									$showv = $v;
									$rclass = "rcol";
								}
								
								//&& strpos(strtolower($v),str_replace("%","",strtolower($searchterm)))!==false
								if(isset($search_fieldlist[$k]) && strpos($searchterm,"%")!==false) $showv = $searchterm;
								$rowstr .= "<td class='$rclass'>";
								$rowstr .= $showv;
								$rowstr .= "</td>";
							}
							$rowstr .= "</tr>";
						}
						$end_ts = time();
						$elapsed = $end_ts - $start_ts;
						
						if($elapsed >= 0)
						{
							echo "Query Time: ~" . $elapsed . " second";
							if($elapsed != 1) echo "s";
							echo "<br><br>";
						}
						
						echo "<style>";
						echo "	.hcol { font-family:Verdana,Arial; font-size:12px; color:#555555; text-align:center; border-bottom:solid 1px #aaaaaa;} ";
						echo "	.rcol { font-family:Verdana,Arial; font-size:12px; color:#555555; text-align:left; border-right:solid 1px #aaaaaa; } ";
						echo "	.rcolnumber { font-family:Verdana,Arial; font-size:12px; color:#555555; text-align:right; border-right:solid 1px #aaaaaa; } ";
						echo "	.rcolfirst { font-family:Verdana,Arial; font-size:12px; color:#555555; text-align:left; border-right:solid 1px #aaaaaa; border-left:solid 1px #aaaaaa; } ";
						echo "</style>";
						echo "<table cellspacing=0 cellpadding=4 style='border-bottom:solid 1px #aaaaaa;'>";
						echo $headerstr;
						echo $rowstr;
						echo "</table>";
					}
				}
			}
		}		
	}
?>
