<?php

require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");

// Get report data

list($min_date, $max_date) = getDateRange($_REQUEST);
update1pmtRuleTable($min_date, $max_date);

// Display the report

$rsubmode = create_submode_nav(array("new-business", "upsells"), $rmode);  // $rmode declared in coninfo.php
displayDatePicker($min_date, $max_date, $_REQUEST);

switch ($rsubmode)
{
	case "new-business":
	case "":
		displayNewBusinessReport($min_date, $max_date);
		break;

	case "upsells":
		displayUpsellsReport($min_date, $max_date);
		break;

	default:
		echo "<p>Unrecognized submode <strong><pre>{$rsubmode}</pre></strong></p>";
		break;
}

exit;


/**
 * Functions
 */

function getDateRange($args)
{
	// Check $args for date fields, otherwise use defaults (KPI Metrics run from Thu - Fri)
	$min_date = isset($args['min_date']) ? $args['min_date'] : date('Y-m-d', strtotime('last Thursday'));
	$max_date = isset($args['max_date']) ? $args['max_date'] : date('Y-m-d', strtotime('next Friday'));

	return array($min_date, $max_date);
}

function displayDatePicker($min_date, $max_date, $vars)
{
	$mode = isset($vars['mode']) ? htmlentities($vars['mode']) : '';
	$rmode = isset($vars['rmode']) ? htmlentities($vars['rmode']) : '';
	$rsubmode = isset($vars['rsubmode']) ? htmlentities($vars['rsubmode']) : '';

	echo <<<HTML
		<form name='booked_revenue_form' style='margin-left:4em;'>
		<input type='hidden' name='mode' value='{$mode}'>
		<input type='hidden' name='rmode' value='{$rmode}'>
		<input type='hidden' name='rsubmode' value='{$rsubmode}'>
		<label for='min_date'>Start Date</label><input type='text' size='13' name='min_date' id='min_date' class='tcal tcalInput' style='margin:auto 1em;' value='{$min_date}'>
		<label for='max_date'>End Date</label><input type='text' size='13' name='max_date' id='max_date' class='tcal tcalInput' style='margin:auto 1em' value='{$max_date}'>
		<input type="submit" id="submit" value="Submit" style='font-size:10px'>
		<ul>
			<li style="color:#999; font-size:10px; font-style:italics;">End Date is non-inclusive (so you may need to add 1 day to your End Date)</li>
			<li style="color:#999; font-size:10px; font-style:italics;">1pmt Payment Date includes customer payment dates and reseller license applied dates</li>
		</ul>
		</form><br>
		<link type='text/css' rel='stylesheet' href='/cp/resources/tigra/tcal.css'>
		<script type='text/javascript' src='/cp/resources/tigra/tcal.js'></script>
		<script>
			A_TCALCONF = {
				'cssprefix'  : 'tcal',
				'cssposition': 'absolute',
				'months'     : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				'weekdays'   : ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				'longwdays'  : ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
				'yearscroll' : true, // show year scroller
				'weekstart'  : 0, // first day of week: 0-Su or 1-Mo
				'prevyear'   : 'Previous Year',
				'nextyear'   : 'Next Year',
				'prevmonth'  : 'Previous Month',
				'nextmonth'  : 'Next Month',
				'format'     : 'Y-m-d' // 'd-m-Y', Y-m-d', 'l, F jS Y'
			};
			var min_date = document.getElementById("min_date");
			function cal_select_min_date(dateStr) {
				min_date.value = dateStr;
			}
			var max_date = document.getElementById("max_date");
			function cal_select_max_date(dateStr) {
				max_date.value = dateStr;
			}
		</script>
HTML;
}

function displayExportTextArea($exportstr)
{
	echo "<br><br><br><br><textarea rows='12' cols='120' style='font-size:8px'>". htmlspecialchars($exportstr) ."</textarea><br><br>";
}

function displayNewBusinessReport($min_date, $max_date)
{
	$recs = '';
	$header_fields = array(
		'datetime' => '1st Payment Date',
		'company_name' => 'Customer Name',
		'restaurant_id' => 'Rest ID',
		'dataname' => 'Dataname',
		'distro_code' => 'Distro Code',
		'payment_type' => 'Payment Type',
		'amount' => 'Revenue',
		'monthly_fee' => 'Monthly Fee',
		'signup_address' => 'Signup Address',
		'signup_city' => 'Signup City',
		'signup_state' => 'Signup State',
		'signup_zip' => 'Signup Zip',
		'signup_country' => 'Signup Country',
		'location_address' => 'Location Address',
		'location_city' => 'Location City',
		'location_state' => 'Location State',
		'location_zip' => 'Location Zip',
		'location_country' => 'Location Country',
	);

	echo "<table style='font-size:10px; white-space:nowrap; text-align:right'><tr style='font-size:8px; background-color:#eeffee'>";
	foreach ($header_fields as $header_field => $header_label) {
		echo "<td>{$header_label}</td>";
		$recs .= $header_label ."\t";
	}
	echo "</tr>";
	$recs .= "\n";

	$months = array();
	$rests_found = array();

	mlavu_select_db('poslavu_MAIN_db');
	$pay_query = mlavu_query("SELECT 1pmt.*, r.`company_name`, r.`data_name` AS `dataname`, r.`distro_code`, s.`address` AS `signup_address`, s.`city` AS `signup_city`, s.`state` AS `signup_state`, s.`zip` AS `signup_zip`, s.`country` AS `signup_country`, s.`package`, s.`signup_package`, ps.`custom_monthly_hosting`, ps.`annual_agreement` FROM `poslavu_MAIN_db`.`billing_1pmt` 1pmt LEFT OUTER JOIN `poslavu_MAIN_db`.`restaurants` r ON (1pmt.`restaurant_id` = r.`id`) LEFT OUTER JOIN `poslavu_MAIN_db`.`signups` s ON (r.`data_name` = s.`dataname` and s.`dataname` != '') LEFT OUTER JOIN `payment_status` ps ON (s.`dataname` = ps.`dataname`) WHERE `datetime` >= '[min_date]' AND `datetime` < '[max_date]' ORDER BY `datetime`", array('min_date' => $min_date, 'max_date' => $max_date)); 
	if ($pay_query === false) die(__FILE__ ." got database error: ". mlavu_dberror()); 

	$total_count = 0;
	while ($pay_read = mysqli_fetch_assoc($pay_query))
	{
		$restid = $pay_read['restaurant_id'];
		
		if(!isset($rests_found[$restid]))
		{
			$yrmo = substr($pay_read['datetime'], 0, 7);
			if(!isset($months[$yrmo])) $months[$yrmo] = array();
			$months[$yrmo][] = $restid;
			$rests_found[$restid] = 1;

			lavu_connect_byid($restid, "poslavu_{$pay_read['dataname']}_db");
			$location_q = lavu_query("SELECT `id` AS `location_id`, `address` AS `location_address`, `city` AS `location_city`, `state` AS `location_state`, `zip` AS `location_zip`, `country` AS `location_country` FROM `locations` WHERE `_disabled` IN('', '0') ORDER BY `id` DESC");
			$location = ($location_q === false) ? array() : mysqli_fetch_array($location_q);
			$pay_read = array_merge($pay_read, $location);
		}

		//$pay_read['datetime'] = date('n/d/Y H:i:s', strtotime($pay_read['datetime']));
		$pay_read['monthly_fee'] = getMonthlyFee($pay_read, $min_date, $max_date);

		echo "<tr>";
		foreach ($header_fields as $header_field => $header_label) {
			echo "<td>{$pay_read[$header_field]}</td>";
			$recs .= $pay_read[$header_field] ."\t";
		}
		echo "</tr>";
		$recs .= "\n";

		$total_count++;
	}

	echo "</table>";
	echo "<p><strong>Total:</strong> {$total_count}</p>";

	displayExportTextArea($recs);
}

function displayUpsellsReport($min_date, $max_date)
{
	$recs = '';
	$header_fields = array(
		'datetime' => '1st Payment Date',
		'restaurant_id' => 'Rest ID',
		'company_name' => 'Customer Name',
		'dataname' => 'Dataname',
		'distro_code' => 'Distro Code',
		'payment_type' => 'Payment Type',
		'amount' => 'Revenue',
		'monthly_fee' => 'Monthly Fee',
	);

	echo "<table style='font-size:10px; white-space:nowrap; text-align:right'><tr style='font-size:8px; background-color:#eeffee'>";
	foreach ($header_fields as $header_field => $header_label) {
		echo "<td>{$header_label}</td>";
		$recs .= $header_label ."\t";
	}
	echo "</tr>";
	$recs .= "\n";

	$months = array();
	$rests_found = array();

	mlavu_select_db('poslavu_MAIN_db');
	$pay_query = mlavu_query("SELECT pr.`datetime`, 1pmt.`restaurant_id`, r.`company_name`, r.`data_name` AS 'dataname', r.`distro_code`, pr.`payment_type`, pr.`x_amount` AS 'amount', s.`package`, s.`signup_package`, ps.`custom_monthly_hosting`, ps.`annual_agreement` FROM `billing_1pmt` 1pmt LEFT OUTER JOIN `payment_responses` pr on (1pmt.`restaurant_id` = pr.`match_restaurantid`) LEFT OUTER JOIN `restaurants` r on (pr.`match_restaurantid` = r.`id`) LEFT OUTER JOIN `poslavu_MAIN_db`.`signups` s ON (r.`data_name` = s.`dataname` and s.`dataname` != '') LEFT OUTER JOIN `payment_status` ps on (r.`data_name` = ps.`dataname`)  WHERE !(1pmt.`datetime` >= '[min_date]' and 1pmt.`datetime` < '[max_date]') AND pr.`datetime` >= '[min_date]' AND pr.`datetime` < '[max_date]' AND 1pmt.`datetime` < '[min_date]' AND pr.`x_type` = 'auth_capture' AND pr.`x_response_code` = '1' AND pr.`x_description` like 'POSLavu Payment%' ORDER BY pr.`datetime`", array('min_date' => $min_date, 'max_date' => $max_date)); 
	if ($pay_query === false) die(__FILE__ ." got database error: ". mlavu_dberror()); 

	$total_count = 0;
	while ($pay_read = mysqli_fetch_assoc($pay_query))
	{
		$restid = $pay_read['resturant_id'];
		
		if(!isset($rests_found[$restid]))
		{
			$yrmo = substr($pay_read['datetime'], 0, 7);
			if(!isset($months[$yrmo])) $months[$yrmo] = array();
			$months[$yrmo][] = $restid;
			$rests_found[$restid] = 1;
		}

		//$pay_read['datetime'] = date('n/d/Y H:i:s', strtotime($pay_read['datetime']));
		$pay_read['monthly_fee'] = getMonthlyFee($pay_read, $min_date, $max_date);

		echo "<tr>";
		foreach ($header_fields as $header_field => $header_label) {
			echo "<td>{$pay_read[$header_field]}</td>";
			$recs .= $pay_read[$header_field] ."\t";
		}
		echo "</tr>";
		$recs .= "\n";

		$total_count++;
	}

	echo "</table>";
	echo "<p><strong>Total:</strong> {$total_count}</p>";

	displayExportTextArea($recs);
}

function getMonthlyFee($rec, $min_date, $max_date)
{
	global $o_package_container;

	$monthly_fee = 0.00;

	mlavu_select_db('poslavu_MAIN_db');
	$invoice_q = mlavu_query("SELECT * FROM `invoices` WHERE `type`='hosting' AND `dataname`='[dataname]' ORDER BY `due_date`", $rec); 
	if ($invoice_q === false) die(__FILE__ ." got database error: ". mlavu_dberror());

	// We first try getting the original monthly fee for the account from the first Hosting invoice.
	if (mysqli_num_rows($invoice_q))
	{
		$invoice = mysqli_fetch_assoc($invoice_q);
		$monthly_fee = (float) $invoice['amount'];
	}
	// If a Hosting invoice doesn't exist yet, we calculate the monthly fee from hosting amount + monthly_parts
	else
	{
		$hosting_amount = 0.00;
		$monthly_part_amount = 0.00;

		// Use custom_hosting_amount, if we have a value for it
		if (isset($rec['custom_monthly_hosting']) && $rec['custom_monthly_hosting'] != '') {
			$hosting_amount = (float) $rec['custom_monthly_hosting'];
		}
		// Otherwise get the hosting amount for the package level (using the signup_package, if available)
		else {
			if (!isset($o_package_container)) $o_package_container = new package_container();
			$package_level = !empty($rec['signup_package']) ? (int) $rec['signup_package'] : (int) $rec['package'];
			$hosting_amount = $o_package_container->get_recurring_by_attribute('level', $package_level);
		}

		// Tally any monthly_parts effective for our date range
		$rec['min_date'] = $min_date;
		$rec['max_date'] = $max_date;
		$monthly_parts = mlavu_query("SELECT * FROM `monthly_parts` WHERE `dataname`='[dataname]' AND ((`mp_start_day` < '[max_date]' OR `mp_start_day` = '') AND (`mp_end_day` >= '[min_date]' OR `mp_end_day` = ''))", $rec);
		if ($monthly_parts === false) die(__FILE__ ." got database error: ". mlavu_dberror());
		while ($monthly_part = mysqli_fetch_assoc($monthly_parts)) {
			$monthly_part_amount += (float) $monthly_part['amount'];
		}

		$monthly_fee = $hosting_amount + $monthly_part_amount;
	}

	return (string) $monthly_fee;
}

function update1pmtRuleTable()
{
	mlavu_select_db('poslavu_MAIN_db');

	// Get max datetime in 1pmt rule table and compare to payment_responses

	$newest_1pmt_payment_responses = mlavu_query("SELECT MAX(`datetime`) FROM `billing_1pmt_payments` WHERE `reseller_license_id`=''");
	if ($newest_1pmt_payment_responses === false) die(__FILE__ ." got database error: ". mlavu_dberror());
	$newest_1pmt_payment_response = mysqli_fetch_row($newest_1pmt_payment_responses);

	$newest_1pmt_reseller_licenses = mlavu_query("SELECT MAX(`datetime`) FROM `billing_1pmt_payments` WHERE `reseller_license_id`!=''");
	if ($newest_1pmt_reseller_licenses === false) die(__FILE__ ." got database error: ". mlavu_dberror());
	$newest_1pmt_reseller_license = mysqli_fetch_row($newest_1pmt_reseller_licenses);

	$newest_payment_responses = mlavu_query("SELECT MAX(`datetime`) FROM `payment_responses` WHERE x_response_code = '1' and x_type = 'auth_capture' and !(x_account_number = 'XXXX6781' and x_description like '%POSLavu -%') and !(batch_status = 'N' and datetime >= '2014-02-01' and datetime < '2014-08-15') and match_restaurantid not in('', 'na', 'x', '[1]') and match_restaurantid not like 'DS_%'");
	if ($newest_payment_responses === false) die(__FILE__ ." got database error: ". mlavu_dberror());
	$newest_payment_response = mysqli_fetch_row($newest_payment_responses);

	$newest_reseller_licenses = mlavu_query("SELECT MAX(`applied`) FROM `reseller_licenses` WHERE applied not in('', 'payment_link') and cast(cost as decimal(30,2)) != 0.00 and restaurantid != '-1'");
	if ($newest_reseller_licenses === false) die(__FILE__ ." got database error: ". mlavu_dberror());
	$newest_reseller_license = mysqli_fetch_row($newest_reseller_licenses);

	// See if there are any payment_responses or applied reseller_licenses newer than out newest payment in the 1pmt rule table

	$newest_1pmt_payment_response_datetime = $newest_1pmt_payment_response[0];  // TO DO : rename
	$newest_1pmt_payment_response_ts = strtotime($newest_1pmt_payment_response_datetime);

	$newest_1pmt_reseller_license_datetime = $newest_1pmt_reseller_license[0];  // TO DO : rename
	$newest_1pmt_reseller_license_ts = strtotime($newest_1pmt_reseller_license_datetime);

	$newest_payment_response_datetime = $newest_payment_response[0];
	$newest_payment_response_ts = strtotime($newest_payment_response_datetime);

	$newest_reseller_license_datetime = $newest_reseller_license[0];
	$newest_reseller_license_ts = strtotime($newest_applied_license_datetime);

	$new_payment_response = ($newest_payment_response_ts > $newest_1pmt_payment_response_ts);
	$new_reseller_license = ($newest_reseller_license_ts > $newest_1pmt_reseller_license_ts);

	echo "<!--p><em style='color:#999; font-size:10px;'>new_payment_response={$new_payment_response} new_reseller_license={$new_reseller_license}</em></p-->";

	if ($new_payment_response || $new_reseller_license)
	{
		echo "<!--p><em style='color:#999; font-size:10px;'>Updating 1pmt rule table...</em></p-->";

		// Pull any payment_responses or applied reseller_licenses since our respective latests into `billing_1pmt_payments_unordered`

		$sql = <<<SQL
			insert into billing_1pmt_payments_unordered (restaurant_id, datetime, payment_response_id, reseller_license_id, payment_type, amount)
			 (select match_restaurantid as restaurant_id, datetime, id as payment_response_id, '' as reseller_license_id, payment_type, x_amount as amount
			  from payment_responses
			  where x_response_code = '1'
			  and x_type = 'auth_capture'
			  and !(x_account_number = 'XXXX6781' and x_description like '%POSLavu -%')
			  and !(batch_status = 'N' and datetime >= '2014-02-01' and datetime < '2014-08-15')
			  and match_restaurantid not in('', 'na', 'x', '[1]')
			  and match_restaurantid not like 'DS_%'
			  and datetime > '[1]')
			  union
			 (select restaurantid as restaurant_id, applied, '' as payment_response_id, id as reseller_license_id, 'License' as payment_type, cost as amount
			  from reseller_licenses
			  where applied not in('', 'payment_link')
			  and cast(cost as decimal(30,2)) != 0.00
			  and restaurantid != '-1'
			  and applied > '[2]')
SQL;
		$sql = str_replace(array("\n", "\t"), " ", $sql);  // Remove newlines because it messes up query logging.  Tabs may be ok but removing them just to be sure.
		$res = mlavu_query($sql, $newest_1pmt_payment_response_datetime, $newest_1pmt_reseller_license_datetime);
		if ($res === false) die(__FILE__ ." line ". __LINE__ ." got database error: ". mlavu_dberror());

		// Clear out and re-order the `billing_1pmt_payments` table

		$res = mlavu_query("DELETE FROM `billing_1pmt_payments`");
		if ($res === false) die(__FILE__ ." line ". __LINE__ ." got database error: ". mlavu_dberror());

		$sql = <<<SQL
			insert into billing_1pmt_payments (restaurant_id, datetime, payment_response_id, reseller_license_id, payment_type, amount, payment_num, ignorethis)
			select restaurant_id, datetime, payment_response_id, reseller_license_id, payment_type, amount,
			 @payment_num := if( @prev_restaurant_id = restaurant_id, @payment_num + 1, 1) as payment_num,
			 @prev_restaurant_id := restaurant_id as ignorethis
			from billing_1pmt_payments_unordered, (select @payment_num := 0) localvar_subselect1, (select @prev_restaurant_id := '') localvar_subselect2
			order by 1, 2
SQL;
		$sql = str_replace(array("\n", "\t"), " ", $sql);  // Remove newlines because it messes up query logging.  Tabs may be ok but removing them just to be sure.
		mlavu_query($sql);
		if ($res === false) die(__FILE__ ." line ". __LINE__ ." got database error: ". mlavu_dberror());

		// Pull any new 1pmt rule payments

		$sql = <<<SQL
			insert into billing_1pmt (restaurant_id, datetime, payment_response_id, reseller_license_id, payment_type, amount)
			select restaurant_id, datetime, payment_response_id, reseller_license_id, payment_type, amount
			from billing_1pmt_payments
			where payment_num = '1'
			and ( (datetime > '[1]' and reseller_license_id = '')
			   or (datetime > '[2]' and reseller_license_id != '') )
SQL;
		$sql = str_replace(array("\n", "\t"), " ", $sql);  // Remove newlines because it messes up query logging.  Tabs may be ok but removing them just to be sure.
		$res = mlavu_query($sql, $newest_1pmt_payment_response_datetime, $newest_1pmt_reseller_license_datetime);
		if ($res === false) die(__FILE__ ." line ". __LINE__ ." got database error: ". mlavu_dberror());

		echo "<p><em style='color:#999; font-size:10px;'>Finished updating 1pmt rule table</em></p>";
	}
	else
	{
		echo "<!--p><em style='color:#999; font-size:10px;'>No update needed for 1pmt rule table</em></p-->";
	}
}

