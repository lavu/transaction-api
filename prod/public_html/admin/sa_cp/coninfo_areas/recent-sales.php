<?php
	if($rmode!="")
	{
		require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
		if(1)
		{
			$months = array();
			$rests_found = array();
			$pay_query = mlavu_query("select `date`,`time`,`x_amount`,`match_restaurantid` from `poslavu_MAIN_db`.`payment_responses` where x_amount * 1 > 0 and x_type='auth_capture' and x_response_code='1' and match_restaurantid * 1 > 0"); 
			while($pay_read = mysqli_fetch_assoc($pay_query))
			{
				$day = $pay_read['date'];
				$month = substr($day,0,7);
				$restid = $pay_read['match_restaurantid'];
				
				if(!isset($rests_found[$restid]))
				{
					if(!isset($months[$month])) $months[$month] = array();
					$months[$month][] = $restid;
					
					$rests_found[$restid] = 1;
				}
			}
			
			echo "* Recent Sales is now measured by counting the number of \"First Payments\" that occur each month.<br><br>";
			echo "<table>";
			for($m=0; $m<=24; $m++)
			{
				$month = date("Y-m",mktime(0,0,0,date("m")-$m,date("d"),date("Y")));
				$month_value = "-";
				if(isset($months[$month])) $month_value = count($months[$month]);
				if($month==date("Y-m")) $projected = floor($month_value / date("d") * 31);
				else $projected = $month_value;
				echo "<tr><td align='right'>$month</td><td align='right'>$month_value</td>";
				echo "<td>";
				
				$setwidth = 0;
				if($month_value!="-") $setwidth = $month_value * 3 . "px";
				
				echo "<table cellspacing=0 cellpadding=0><tr>";
				echo "<td style='width:$setwidth' bgcolor='#aabbcc'>&nbsp;</td>";
				if($projected > $month_value) 
				{
					$setwidth2 = ($projected - $month_value) * 3 . "px";
					echo "<td style='width:$setwidth2' bgcolor='#ccddee'>&nbsp;</td>";
					echo "<td align='right' style='color:#000077'>&nbsp;<b>$projected</b></td>";
				}
				echo "</tr></table>";
				
				echo "</td>";
				echo "</tr>";
			}
			echo "</table>";
		}
		else
		{
			require_once(dirname(__FILE__) . "/../tools/monthly.php");
			
			$_GET['submode'] = "general_tools";
			$_GET['tool'] = "monthly_payments";
			$_GET['view_all'] = "recent";
			output_monthly_reports();
		}
	}
?>
