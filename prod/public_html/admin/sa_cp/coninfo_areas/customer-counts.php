<?php
	if($rmode!="")
	{
		//http://admin.poslavu.com/sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=restactivity&next
		
		$exportstr = "";
		$can_view_report = false;
		$auto_reload = false;
		$start_run_time = time();
		$last_month = "2012-00";
		$last_month_query = mlavu_query("select `range` from `poslavu_MAIN_db`.`restdata` where `type`='reportdata - month total sales' and `data`!='' and `data2`!='' order by `range` desc limit 1");
		if(mysqli_num_rows($last_month_query))
		{
			$last_month_read = mysqli_fetch_assoc($last_month_query);
			$last_month = $last_month_read['range'];
		}
		//$last_month = "2015-11";
		$last_month_parts = explode("-",$last_month);
		if(count($last_month_parts)==2)
		{
			$last_month_year = $last_month_parts[0];
			$next_month = date("Y-m",mktime(0,0,0,$last_month_parts[1] + 1,1,$last_month_year));
			
			if($next_month < date("Y-m"))
			{
				$exist_arr = array();
				$exist_query = mlavu_query("select `id`,`dataname` from `poslavu_MAIN_db`.`restdata` where `type`='[1]' and `range`='[2]'","reportdata - month total sales",$next_month);
				while($exist_read = mysqli_fetch_assoc($exist_query))
				{
					$exist_arr[$exist_read['dataname']] = $exist_read;
				}
				
				echo "updating stored data for " . $next_month . "....<br>";
				$order_count_list = array();
				$success_count = 0;
				$success_count_updated = 0;
				$success_count_inserted = 0;
				$count_query = mlavu_query("select `dataname`,sum(`data`) as `total`,sum(`data2`) as `txn_total` from `poslavu_MAIN_db`.`restdata` where `type`='daily sales' and `range`>='[1]-01' and `range`<='[1]-31' group by `dataname` order by `total` desc",$next_month);
				while($count_read = mysqli_fetch_assoc($count_query))
				{
					if(is_numeric($count_read['total']))
					{
						$set_data = $count_read['total'];
						$set_data2 = $count_read['txn_total'];
						
						if(isset($exist_arr[$count_read['dataname']]))
						{
							$exist_read = $exist_arr[$count_read['dataname']];
							$success = mlavu_query("update `poslavu_MAIN_db`.`restdata` set `data`='[1]',`data2`='[2]' where `id`='[3]'",$set_data,$set_data2,$exist_read['id']);
							if($success) $success_count_updated++;
						}
						else
						{
							$success = mlavu_query("insert into `poslavu_MAIN_db`.`restdata` (`type`,`range`,`dataname`,`data`,`data2`) values ('[1]','[2]','[3]','[4]','[5]')","reportdata - month total sales",$next_month,$count_read['dataname'],$set_data,$set_data2);
							if($success) $success_count_inserted++;
						}
						if($success) $success_count++;
					}
				}
				echo "records updated: " . $success_count_updated . "<br>";
				echo "records inserted: " . $success_count_inserted . "<br>";
				if($success_count > 0)
				{
					$auto_reload = true;
				}
			}
			else
			{
				$can_view_report = true;
			}
		}
		
		$update_payment_data = false;//true;
		if(isset($_GET['update_payment_data'])) $update_payment_data = true;
		if($update_payment_data)
		{
			ini_set("display_errors",1);
			ini_set("memory_limit","256M");
			echo "Updating Payment Data...<br>";
			$success_count = 0;
			$success_count_updated = 0;
			$success_count_inserted = 0;
			
			$exist_arr = array();
			$exist_query = mlavu_query("select `id`,`dataname`,`range` from `poslavu_MAIN_db`.`restdata` where `type`='[1]'","reportdata - month total sales",$next_month);
			while($exist_read = mysqli_fetch_assoc($exist_query))
			{
				$exist_arr["D".$exist_read['dataname'] . "_R" . $exist_read['range']] = $exist_read;
			}
			
			$rlist = array();
			$rdlist = array();
			$rlist_query = mlavu_query("select `id`,`data_name` from `poslavu_MAIN_db`.`restaurants`");
			while($rlist_read = mysqli_fetch_assoc($rlist_query))
			{
				$rlist[$rlist_read['id']] = $rlist_read;
				if(trim($rlist_read['data_name'])!="")
				{
					$rdlist[$rlist_read['data_name']] = $rlist_read;
				}
			}
			
			$signup_query = mlavu_query("select `package` from `poslavu_MAIN_db`.`signups` where `dataname`!=''");
			while($signup_read = mysqli_fetch_assoc($signup_query))
			{
				if(isset($rdlist_read[$signup_read['dataname']]))
				{
					$pnum = strtolower($signup_read['package']);
					$ptype = "";
					if($pnum=="88") $ptype = "Lavu88";
					else if($pnum=="3") $ptype = "Platinum";
					else if(is_numeric($pnum))
					{
						$pdig = $pnum % 10;
						if($pdig==1) $ptype = "Silver";
						else if($pdig==2) $ptype = "Gold";
						else if($pdig==3) $ptype = "Pro";
						else $ptype = "Other";
					}
					else if(strpos($pnum,"sil")!==false) $ptype = "Silver";
					else if(strpos($pnum,"88")!==false) $ptype = "Lavu88";
					else if(strpos($pnum,"gol")!==false) $ptype = "Gold";
					else if(strpos($pnum,"pla")!==false) $ptype = "Platinum";
					else if(strpos($pnum,"pro")!==false) $ptype = "Pro";
					else $ptype = "Other";
					
					$rdlist_read[$signup_read['dataname']]['package'] = $ptype;
				}
			}
						
			$plist = array();
			$pay_query = mlavu_query("select count(*) as `count`,sum(`x_amount`) as total,`match_restaurantid`,left(`date`,7) as `month` from `poslavu_MAIN_db`.`payment_responses` where x_amount > 0 && x_type='auth_capture' and x_response_code='1' and match_restaurantid!='' and `match_restaurantid` NOT LIKE 'DS%' and match_restaurantid!='516' and match_restaurantid!='2716' and match_restaurantid!='x' and match_restaurantid NOT LIKE '[%' group by match_restaurantid, `month` order by `month` asc, `match_restaurantid` asc");
			while($pay_read = mysqli_fetch_assoc($pay_query))
			{
				$restid = $pay_read['match_restaurantid'];
				if(is_numeric($restid) && isset($rlist[$restid]))
				{
					$dataname = $rlist[$restid]['data_name'];
					$month = $pay_read['month'];
					$pcount = $pay_read['count'];
					
					if($dataname!="" && $pcount > 0)
					{
						if(!isset($plist[$dataname])) $plist[$dataname] = $pcount;
						else $plist[$dataname] += $pcount;
						if(1)
						{
							$set_data_long = $plist[$dataname];
							if(isset($exist_arr["D".$dataname."_R".$month]))
							{
								$exist_read = $exist_arr["D".$dataname."_R".$month];
								$success = mlavu_query("update `poslavu_MAIN_db`.`restdata` set `data_long`='[1]' where `id`='[2]'",$set_data_long,$exist_read['id']);
								if($success) $success_count_updated++;
								//echo $month . " - " . $plist[$dataname] . " (exists)<br>";
							}
							else
							{
								$success = mlavu_query("insert into `poslavu_MAIN_db`.`restdata` (`type`,`range`,`dataname`,`data`,`data2`,`data_long`) values ('[1]','[2]','[3]','[4]','[5]','[6]')","reportdata - month total sales",$month,$dataname,"","",$set_data_long);
								if($success) $success_count_inserted++;
								//echo $month . " - " . $plist[$dataname] . "<br>";
							}
						}
					}
				}
			}
			echo "txn records updated: " . $success_count_updated . "<br>";
			echo "txn records inserted: " . $success_count_inserted . "<br>";
		}
		
		$rlist = array();
		$rdlist = array();
		$rlist_query = mlavu_query("select `id`,`data_name`,`company_name`,`disabled` from `poslavu_MAIN_db`.`restaurants`");
		while($rlist_read = mysqli_fetch_assoc($rlist_query))
		{
			$rlist[$rlist_read['id']] = $rlist_read;
			if(trim($rlist_read['data_name'])!="")
			{
				$rdlist[$rlist_read['data_name']] = $rlist_read;
			}
		}
		
		$signup_query = mlavu_query("select `package`,`dataname` from `poslavu_MAIN_db`.`signups` where `dataname`!=''");
		while($signup_read = mysqli_fetch_assoc($signup_query))
		{
			if(isset($rdlist[$signup_read['dataname']]))
			{
				$pnum = strtolower($signup_read['package']);
				//echo $pnum . "<br>";
				$ptype = "";
				if($pnum=="88" || $pnum=="8") $ptype = "Lavu88";
				else if($pnum=="3") $ptype = "Platinum";
				else if(is_numeric($pnum))
				{
					$pdig = $pnum % 10;
					if($pdig==1) $ptype = "Silver";
					else if($pdig==2) $ptype = "Gold";
					else if($pdig==3) $ptype = "Pro";
					else if($pdig==8) $ptype = "Lavu88";
					else $ptype = "Other";
				}
				else if(strpos($pnum,"sil")!==false) $ptype = "Silver";
				else if(strpos($pnum,"88")!==false) $ptype = "Lavu88";
				else if(strpos($pnum,"gol")!==false) $ptype = "Gold";
				else if(strpos($pnum,"pla")!==false) $ptype = "Platinum";
				else if(strpos($pnum,"pro")!==false) $ptype = "Pro";
				else $ptype = "Other";
				
				$rdlist[$signup_read['dataname']]['package'] = $ptype;
			}
		}
		
		$ldata = array();
		$lic_query = mlavu_query("select restaurantid,applied from poslavu_MAIN_db.reseller_licenses");
		while($lic_read = mysqli_fetch_assoc($lic_query))
		{
			$restid = $lic_read['restaurantid'];
			if(is_numeric($restid) && isset($rlist[$restid]))
			{
				$dataname = $rlist[$restid]['data_name'];
				$month = substr($lic_read['applied'],0,7);
				if(isset($ldata[$dataname]))
				{
					if($month < $ldata[$dataname]) $ldata[$dataname] = $month;
				}
				else
					$ldata[$dataname] = $month;
			}
		}
		
		$psdata = array();
		$pstat_query = mlavu_query("select dataname,restaurantid,cancel_date,cancel_reason from poslavu_MAIN_db.payment_status");
		while($pstat_read = mysqli_fetch_assoc($pstat_query))
		{
			$psdata[$pstat_read['dataname']] = $pstat_read;
		}
		
		function value_bar($val,$mult,$color,$val2=0,$color2="",$show_val=false)
		{
			$str = "";
			if($show_val)
			{
				$str .= "<td align='right'><b>";
				if(is_numeric($val) && $val * 1 > 0) $str .= $val; else $str .= "&nbsp;";
				$str .= "</b></td>"; 
			}
			
			$str .= "<td align='left'>";
			$str .= "<table cellpadding=0 cellspacing=0><tr><td style='width:" . floor($val * $mult / 2000) . "px; height:16px; background-color:$color'>&nbsp;</td>";
			if($val2 > $val) $str .= "<td style='width:" . floor((($val2 - $val) * $mult) / 2000) . "px; height:16px; background-color:$color2'>&nbsp;</td>";
			$str .= "</tr></table>";
			$str .= "</td>";
			return $str;
		}

		$track_type_list = array();
		$str = "";
		if($can_view_report)
		{
			$cdata = array();
			$mp = (isset($_GET['mp']))?$_GET['mp']:1;
			if(!is_numeric($mp) || $mp < 0 || $mp > 100) $mp = 1;
			
			if(!isset($_GET['list_contact_info']))
			{
				echo "Filter: <select onchange='window.location = \"?mode=coninfo&rmode=$rmode&mp=\" + this.value'>";
				for($p=1; $p<=3; $p++)
				{
					echo "<option value='$p'";
					if($p==$mp) echo " selected";
					echo ">";
					echo "Minimum $p Payment";
					if($p!=1) echo "s";
					echo "</option>";
				}
				echo "</select>";
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				$flastm = date("Y-m",mktime(0,0,0,date("m")-1,1,date("Y"))); // need data from month before to properly calculate
				$flastlink = "index.php?mode=$mode&rmode=$rmode&list_contact_info=not_disabled&list_contact_month=$flastm";
				echo "<input type='button' style='font-size:10xp' value='List Not Disabled' onclick='window.location = \"$flastlink\"' />";
				echo "<br><br>";
				
				echo "<style>";
				echo "	.legend_desc { font-size:10px; color:#888888; width:300px; } ";
				echo "	.legend_abbrev { font-size:10px; font-weight:bold; text-align:right; vertical-align:top; } ";
				echo "	.legend_title { font-size:10px; color:#444444; text-align:right; vertical-align:top; } ";
				echo "</style>";
			
				echo "<table style='border:solid 1px #888888' bgcolor='#f6f6f6' cellspacing=0 cellpadding=4>";
				echo "<tr><td class='legend_abbrev' style='color:#669966'>P+A</td><td class='legend_title'>Paid + Active:</td><td class='legend_desc'>Customer has paid in the given month and is actively using the system</td></tr>";
				echo "<tr><td class='legend_abbrev' style='color:#444444'>Po</td><td class='legend_title'>Paid Only:</td><td class='legend_desc'>Customer has paid in the given month but is NOT actively using the system</td></tr>";
				echo "<tr><td class='legend_abbrev' style='color:#666688'>Ao</td><td class='legend_title'>Active Only:</td><td class='legend_desc'>Customer is actively using the system, but has not made a payment in the given month, although they have made payments in the past.  Common for situations where the bill is either pre-payed or payed the following month</td></tr>";
				echo "</table>";
				echo "<br><br>";
			}
			
			$startm = "2012-01";
			$tstr = "";
			$active_previous = array('all'=>array());
			$track_type_list = array();
			$track_list_count = 0;
			$all_cdata = array();
			
			$m = $startm;
			$m_parts = explode("-",$m);
			$m = date("Y-m",mktime(0,0,0,$m_parts[1]-1,1,$m_parts[0])); // need data from month before to properly calculate
			while($m < date("Y-m"))
			{
				$count_query = mlavu_query("select `dataname`,`data` as `order_count`,`data2` as `txn_count`,`data_long` as `payments_count` from `poslavu_MAIN_db`.`restdata` where `type`='reportdata - month total sales' and `range`='[1]' order by `order_count` desc",$m);
				while($count_read = mysqli_fetch_assoc($count_query))
				{
					$dataname = $count_read['dataname'];
					$new_payments = 0;
					if(isset($cdata[$datname]))
					{
						$order_months = $cdata[$dataname]['order_months'];
						$txn_months = $cdata[$dataname]['txn_months'];
						$pay_months = $cdata[$dataname]['pay_months'];
					}
					else 
					{
						$order_months = 0;
						$txn_months = 0;
						$pay_months = 0;
					}
					
					if($count_read['order_count'] > 0)
					{
						$order_months++;
					}
					if($count_read['txn_count'] > 0)
					{
						$txn_months++;
						if(!isset($rdlist[$dataname]['last_txn_month']) || (isset($rdlist[$dataname]['last_txn_month']) && $rdlist[$dataname]['last_txn_month'] < $m))
						{
							$rdlist[$dataname]['last_txn_month'] = $m;
						}
					}
					if(isset($cdata[$dataname]['pay_months']) && $cdata[$dataname]['pay_months'] > $count_read['payments_count'])
					{
						$count_read['payments_count'] = $cdata[$dataname]['pay_months'];
					}
					if($count_read['payments_count'] > 0)
					{
						$pay_months = $count_read['payments_count'];
						if(isset($cdata[$dataname]['pay_months']))
						{
							$new_payments = $pay_months - $cdata[$dataname]['pay_months'];
						}
						else
						{
							$new_payments = $pay_months;
						}
					}
					
					$cdata[$dataname] = array("order_months"=>$order_months,"txn_months"=>$txn_months,"pay_months"=>$pay_months,"month"=>$m,"month_txns"=>$count_read['txn_count'],"month_payments"=>$new_payments,"month_orders"=>$count_read['order_count']);
					if(!isset($all_cdata[$dataname]) && isset($psdata[$dataname]))
					{
						$all_cdata[$dataname] = $psdata[$dataname];
					}
				}
				
				$count = 0;
				$count_paid_and_using = 0;
				$count_paid_not_using = 0;
				$count_using_not_paid = 0;
				$count_not_using_not_paid_not_canceled = 0;
				$list_not_canceled = array();
				
				$count_ltypes = array();
				$count_ltypes['lavu88'] = 0;
				$count_ltypes['silver'] = 0;
				$count_ltypes['gold'] = 0;
				$count_ltypes['pro'] = 0;
				$count_ltypes['platinum'] = 0;
				$count_ltypes['other'] = 0;
				
				//$track_type_list = array();
				
				$set_active_previous = array('all'=>array());
				$new_customers = array();
				$dropped_customers = array();
				
				foreach($cdata as $ckey => $cval)
				{
					$paycount = $cval['pay_months'];
					$order_months = $cval['order_months'];

					if(isset($_GET['tst']) && $ckey=="route_66") { echo $ckey . " - $m PC: $paycount MP: " . $cval['month_payments'] . " || " . $cval['month_orders'] . "<br>"; }
					
					if(isset($ldata[$ckey]) && $m >= $ldata[$ckey]) $paycount++;
					if($paycount >= $mp && $order_months >= 0 && $cval['month']==$m && ($cval['month_payments'] > 0 || $cval['month_orders'] > 0))
					{
						//if($cval['month']==$m && ($cval['month_payments'] > 0 || $cval['month_orders'] > 0))
						$count++;
						$set_active_previous['all'][$ckey] = 1;
						if(count($active_previous['all']) > 0)
						{
							if(!isset($active_previous['all'][$ckey])) 
							{
								$new_customers[$ckey] = array();
								if(isset($_GET['list_contact_info']) && strtolower($_GET['list_contact_info'])=="starting" && isset($_GET['list_contact_month']) && strtolower($_GET['list_contact_month'])==$m) $track_type_list[] = $ckey;
							}
						}
						
						if($cval['month']==$m)
						{
							if(isset($_GET['tst']) && $ckey=="route_66") { echo $ckey . " - $m - MP:" . $cval['month_payments'] . "/".$cval['pay_months']." MO: " . $cval['month_orders'] . "<br>"; }
							if($cval['month_payments'] > 0 && $cval['month_orders'] > 0) 
							{
								$count_paid_and_using++;
								if(isset($_GET['list_contact_info']) && $_GET['list_contact_info']=="paid_and_using" && isset($_GET['list_contact_month']) && strtolower($_GET['list_contact_month'])==$m)
								{
									$track_type_list[] = $ckey;
								}
							}
							else if($cval['month_payments'] > 0 && $cval['month_orders'] < 1) 
							{
								$count_paid_not_using++;
								if(isset($_GET['list_contact_info']) && $_GET['list_contact_info']=="paid_not_using" && isset($_GET['list_contact_month']) && strtolower($_GET['list_contact_month'])==$m)
								{
									$track_type_list[] = $ckey;
								}
							}
							else if($cval['month_payments'] < 1 && $cval['month_orders'] > 0) 
							{
								$count_using_not_paid++;
								if(isset($_GET['list_contact_info']) && $_GET['list_contact_info']=="using_not_paid" && isset($_GET['list_contact_month']) && strtolower($_GET['list_contact_month'])==$m)
								{
									$track_type_list[] = $ckey;
								}
							}
						}
						if(isset($rdlist[$ckey]) && isset($rdlist[$ckey]['package']))
						{
							$packagelower = trim(strtolower($rdlist[$ckey]['package']));
							if(isset($count_ltypes[$packagelower]))
							{
								$count_ltypes[$packagelower]++;
								if(!isset($set_active_previous[$packagelower])) $set_active_previous[$packagelower] = array();
								$set_active_previous[$packagelower][$ckey] = 1;
								if(isset($_GET['list_contact_info']))
								{
									if(strtolower($_GET['list_contact_info'])==$packagelower && isset($_GET['list_contact_month']) && strtolower($_GET['list_contact_month'])==$m)
									{
										$track_type_list[] = $ckey;
									}
								}
							}
						}
					}
					else
					{
						if(count($active_previous['all']) > 0)
						{
							if(isset($active_previous['all'][$ckey])) 
							{
								$dropped_customers[$ckey] = array();
								if(isset($_GET['list_contact_info']) && strtolower($_GET['list_contact_info'])=="ended" && isset($_GET['list_contact_month']) && strtolower($_GET['list_contact_month'])==$m) $track_type_list[] = $ckey;
							}
						}
						
						if(isset($all_cdata[$ckey]))
						{							
							$is_disabled = $rdlist[$ckey]['disabled'];
							if($is_disabled * 1 > 0)
							{
							}
							else if($all_cdata[$ckey]['cancel_date'] > "2000-01-01 00:00:00" && $all_cdata[$ckey]['cancel_date'] <= $m . "-31 23:59:59")
							{
							}
							else if($paycount >= $mp)
							{
								$count_not_using_not_paid_not_canceled++;
								
								if(isset($_GET['list_contact_info']) && $_GET['list_contact_info']=="not_canceled" && isset($_GET['list_contact_month']) && strtolower($_GET['list_contact_month'])==$m)
								{
									$track_type_list[] = $ckey;
								}
							}
						}
					}
					if(isset($rdlist[$ckey]))
					{							
						if(isset($_GET['list_contact_info']) && $_GET['list_contact_info']=="not_disabled" && isset($_GET['list_contact_month']) && strtolower($_GET['list_contact_month'])==$m)
						{
							$is_disabled = $rdlist[$ckey]['disabled'];
							if($is_disabled * 1 > 0)
							{
							}
							else if(isset($all_cdata) && isset($all_cdata[$ckey]) && $all_cdata[$ckey]['cancel_date'] >= "2000-01-01 00:00:00" && $all_cdata[$ckey]['cancel_date'] <= $m . "-31 23:59:59")
							{
							}
							else if($paycount >= $mp)
							{
								$track_type_list[] = $ckey;
							}
						}
					}
				}
				$active_previous = $set_active_previous;
				
				$rowstr = "";
				$rowstr .= "<tr><td align='right'>$m</td>";

				$rowstr .= "<td style='color:#88bb88; cursor:pointer' onclick='window.location = \"?mode=$mode&rmode=$rmode&list_contact_info=paid_and_using&list_contact_month=$m\"'>P+A:$count_paid_and_using</td>";
				$rowstr .= "<td style='color:#aaaaaa; cursor:pointer' onclick='window.location = \"?mode=$mode&rmode=$rmode&list_contact_info=paid_not_using&list_contact_month=$m\"'>Po:$count_paid_not_using</td>";
				$rowstr .= "<td style='color:#888899; cursor:pointer' onclick='window.location = \"?mode=$mode&rmode=$rmode&list_contact_info=using_not_paid&list_contact_month=$m\"'>Ao:$count_using_not_paid</td>";
				$rowstr .= "<td style='color:#888888; cursor:pointer' onclick='window.location = \"?mode=$mode&rmode=$rmode&list_contact_info=not_canceled&list_contact_month=$m\"'>Nc:$count_not_using_not_paid_not_canceled</td>";
				
				$rowstr .= "<td>$count</td>";
				$rowstr .= value_bar($count,100,"#88bb99");
				
				$rowstr .= "<td width='100'>&nbsp;</td>";
				foreach($count_ltypes as $ltype => $ltypecount)
				{
					$rowstr .= "<td style='cursor:pointer' onclick='window.location = \"?mode=$mode&rmode=$rmode&list_contact_info=$ltype&list_contact_month=$m\"'>$ltype:$ltypecount</td>";
				}
				$rowstr .= "<td style='cursor:pointer' onclick='window.location = \"?mode=$mode&rmode=$rmode&list_contact_info=starting&list_contact_month=$m\"'>Started:" . count($new_customers) . "</td>";
				$rowstr .= "<td style='cursor:pointer' onclick='window.location = \"?mode=$mode&rmode=$rmode&list_contact_info=ended&list_contact_month=$m\"'>Stopped:" . count($dropped_customers) . "</td>";
				
				$rowstr .= "</tr>";		
				
				if($m >= $startm)
				{
					$tstr = $rowstr . $tstr;
				}
				$m_parts = explode("-",$m);
				$m = date("Y-m",mktime(0,0,0,$m_parts[1]+1,1,$m_parts[0]));
			}
			
			$track_list_count = count($track_type_list);
			
			if(isset($_GET['list_contact_info']))
			{
				echo "<input type='button' style='font-size:10px' value='<< Back' onclick='window.location = \"?mode=$mode&rmode=$rmode\"' />";
				echo "&nbsp;&nbsp;&nbsp;&nbsp;Listing Contact Info for Level: <b><font style='color:#007700'>" . ucwords($_GET['list_contact_info']) . "";
				if(isset($_GET['list_contact_month'])) echo " - " . $_GET['list_contact_month'];
				echo "</font></b>";
				echo "<br><br>Results: " . $track_list_count;
				echo "<br><br>";
				function showfval($fieldvalue)
				{
					$show_fieldvalue = $fieldvalue;
					if(strlen($show_fieldvalue) > 25) $show_fieldvalue = substr($show_fieldvalue,0,25) . "...";
					return $show_fieldvalue;
				}
				
				$tstr = "";
				$exportstr = "";
				$countn = 0;
				for($n=0; $n<count($track_type_list); $n++)
				{
					$dataname = $track_type_list[$n];
					$rsp_query = mlavu_query("select * from poslavu_MAIN_db.payment_responses where x_amount * 1 > 0 and match_restaurantid='[1]' and x_response_code='1' order by id desc limit 1",$rdlist[$dataname]['id']);
					if(mysqli_num_rows($rsp_query))
					{
						$rsp_read = mysqli_fetch_assoc($rsp_query);
						$last_amount = $rsp_read['x_amount'];
						$last_date = $rsp_read['date'];
					}
					else
					{
						$last_amount = "---";
						$last_date = "---";
					}
					$signup_query = mlavu_query("select concat(`firstname`,' ',`lastname`) as `contact_name`,`email`,`phone`,`address`,`city`,`state`,`zip`,`country` from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$dataname);
					if(mysqli_num_rows($signup_query))
					{
						$signup_read = mysqli_fetch_assoc($signup_query);
						$countn++;
						$company_name = $rdlist[$dataname]['company_name'];
						$is_disabled = $rdlist[$dataname]['disabled'];
						if($is_disabled * 1 > 0) $is_disabled = "Disabled"; else $is_disabled = "Enabled";
						$tvals = array(); 
						$tvals[] = $countn;
						$tvals[] = $dataname;
						$tvals[] = $is_disabled;
						if(isset($all_cdata[$dataname]) && isset($all_cdata[$dataname]['cancel_date']))
						{
							$cancelstr = $all_cdata[$dataname]['cancel_date'];
							if(strlen($cancelstr) > 10)
							{
								$tvals[] = "Canceled " . substr($cancelstr,0,10);
							}
							else
							{
								$tvals[] = "&nbsp;";
							}
						}
						else $tvals[] = "&nbsp;";
						$tvals[] = $company_name;
						$tvals[] = $last_amount;
						$tvals[] = "Pay: $last_date";
						if(isset($rdlist[$dataname]['last_txn_month']))
						{
							$tvals[] = "Ord: " . $rdlist[$dataname]['last_txn_month'];
						}
						else
						{
							$tvals[] = "No Orders";
						}
						foreach($signup_read as $k => $v) $tvals[] = $v;
												
						$tstr .= "<tr>";
						for($z=0; $z<count($tvals); $z++) $tstr .= "<td>" . showfval($tvals[$z]) . "</td>";
						$tstr .= "</tr>";
						
						if($exportstr!="") $exportstr .= "\n";
						for($z=0; $z<count($tvals); $z++) $exportstr .= $tvals[$z] . "\t";
					}
				}
			}
			
			echo "<table style='border:solid 1px #777777; font-size:10px' bgcolor='#f6f6f6'>";
			echo $tstr;
			echo "</table>";
			echo "<br>";
			
			if($exportstr!="")
			{
				echo "<br><textarea rows='12' cols='120'>" . str_replace("<","&lt;",$exportstr) . "</textarea><br><br>";
			}
			
			/*$cdata = array();
			for($n=0; $n<24; $n++)
			{
				$month = date("Y-m",mktime(0,0,0,date("m") - $n - 1,1,date("Y")));
				
				$order_count_list = array();
				$count_query = mlavu_query("select `dataname`,`data` as `order_count`,`data2` as `txn_count`,`data_long` as `payments_count` from `poslavu_MAIN_db`.`restdata` where `type`='reportdata - month total sales' and `range`='[1]' order by `order_count` desc",$month);
				while($count_read = mysqli_fetch_assoc($count_query))
				{
					if($count_read['order_count'] > 0)
					{
						$order_count_list[] = $count_read;
						$cdata[$dataname] = array("orders"=>$count_read['order_count'],"txns"=>$count_read['txn_count'],"paid"=>$count_read['payments_count']);
					}
				}
				
				if(count($order_count_list) > 0)
				{
					$str .= "Order List ($month): " . count($order_count_list) . "<br>";
				}
			}*/
		}
		
		$end_run_time = time();
		$elapsed = $end_run_time - $start_run_time;
		echo "Elapsed Time: $elapsed second";
		if($elapsed != 1) echo "s";
		echo "<br><br>";
		echo $str;
		
		if($auto_reload)
		{
			echo "<script type='text/javascript'>";
			echo "	function auto_reload() { window.location = '?mode=coninfo&rmode=$rmode&reload'; } ";
			echo "	setTimeout('auto_reload()',2000); ";
			echo "</script>";
		}
		exit();		
	}
	else if(1==2)
	{
		$min_txns = 1;
		if(isset($_GET['month'])) $check_month = $_GET['month']; else $check_month = "";
		$check_month_parts = explode("|",$check_month);
		$check_month = $check_month_parts[0];
		if(count($check_month_parts)==2)
		{
			if(is_numeric($check_month_parts[1]) && $check_month_parts[1]>=0 && $check_month_parts[1]<=100)
			{
				$min_txns = $check_month_parts[1];
			}
		}
		
		$sessvar_name = "can_include_list_min".$min_txns;
		if(!isset($_SESSION[$sessvar_name]))
		{
			$_SESSION[$sessvar_name] = array();
		}
		
		$start_run_time = time();
		
		echo "<font style='color:#888888; font-size:14px'>Please note that this report pulls alot of data and can take up to a minute to run for each month</font>";
		echo "<br>";
		echo "<table cellspacing=0 cellpadding=20><tr><td>";
		echo "<select onchange='window.location = \"index.php?mode=coninfo&rmode=$rmode&month=\" + this.value'>";
		echo "<option value=''>---Select Month---</option>";
		for($m=1; $m<32; $m++)
		{
			$smonth = date("Y-m",mktime(0,0,0,date("m") - $m,1,date("Y")));
			echo "<option value='$smonth|$min_txns'";
			if($smonth==$check_month) echo " selected";
			echo ">$smonth</option>";
		}
		echo "</select>";
		echo "<select onchange='window.location = \"index.php?mode=coninfo&rmode=$rmode&month=\" + this.value'>";
		echo "<option value='$check_month|1'".($min_txns==1?" selected":"").">Require at least one txn</option>";
		echo "<option value='$check_month|2'".($min_txns==2?" selected":"").">Require at least two txns</option>";
		echo "<option value='$check_month|3'".($min_txns==3?" selected":"").">Require at least three txns</option>";
		echo "</select>";
		echo "<br><br>";
		
		if($check_month > "2000-01" && $check_month < "3000-01")
		{
			$min_orders_to_include = 1;
			
			$total_count = 0;
			$with_orders_count = 0;
			$with_payments_count = 0;
			$with_payment_or_order_count = 0;
			$require_payment_to_include = true;
			$rest_id_list = '';
						
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `lavu_lite_server`='' and `notes` NOT LIKE '%AUTO-DELETED%'");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				if($require_payment_to_include)
				{
					$rstr = "R" . $rest_read['id'];
					if(isset($_SESSION[$sessvar_name][$rstr]))
					{
						if($_SESSION[$sessvar_name][$rstr]=="yes")
						{
							$can_include = true;
						}
						else
						{
							$can_include = false;
						}
					}
					else
					{
						$can_include = false;
						$paid_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_amount` * 1 > 8",$rest_read['id']);// and `x_type`='auth_capture' and `x_response_code`='1'",$rest_read['id']);
						if(mysqli_num_rows($paid_query))
						{
							$paid_read = mysqli_fetch_assoc($paid_query);
							if($paid_read['count'] >= $min_txns)
							{
								$can_include = true;
							}
						}
						if($min_txns < 2)
						{
							if(!$can_include)
							{
								$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `restaurantid`='[1]'",$rest_read['id']);
								if(mysqli_num_rows($lic_query))
								{
									$can_include = true;
								}
							}
						}
						
						if($can_include)
						{
							$setstr = "yes";
						}
						else
						{
							$setstr = "no";
						}
						$_SESSION[$sessvar_name][$rstr] = $setstr;
					}
				}
				else
				{
					$can_include = true;
				}
				
				if($can_include)
				{
					$pcountstr = "pcount_".$check_month."_" . $rest_read['data_name'];
					if(isset($_SESSION[$sessvar_name][$pcountstr]))
					{
						$found_payment_count = $_SESSION[$sessvar_name][$pcountstr];
					}
					else
					{
						$paid_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_amount` * 1 > 8 and `x_response_code`='1' and `x_type`='auth_capture' and `date`>='[2]-01' and `date`<='[2]-31'",$rest_read['id'],$check_month);
						if(mysqli_num_rows($paid_query))
						{
							$paid_read = mysqli_fetch_assoc($paid_query);
							$paid_count = $paid_read['count'];
							
							$found_payment_count = $paid_count;
							$_SESSION[$sessvar_name][$pcountstr] = $paid_count;
						}
						else
						{
							$found_payment_count = 0;
						}
					}
					
					if($found_payment_count > 0)
					{
						$with_payments_count++;
					}
					
					$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
					$ocountstr = "count" . $min_orders_to_include . "_" . $check_month . "_" . $rest_read['data_name'];
					
					if(isset($_SESSION[$sessvar_name][$ocountstr]))
					{
						$order_count = $_SESSION[$sessvar_name][$ocountstr];
					}
					else
					{
						$order_count = 0;
						$order_count_query = mlavu_query("select sum(`data`) as `count` from `poslavu_MAIN_db`.`restdata` where `dataname`='[1]' and `range`>='[2]-01' and `range`<='[2]-31'",$rest_read['data_name'],$check_month);
						if(mysqli_num_rows($order_count_query))
						{
							$order_count_read = mysqli_fetch_assoc($order_count_query);
							$order_count = $order_count_read['count'];
							//echo $rest_read['data_name'] . " - " . $order_count . "<br>";
						}
						
						/*$order_count_query = mlavu_query("select count(*) as `count` from `[1]`.`orders` where `closed` >= '[2]-01 00:00:00' and `closed` <= '[2]-31 24:00:00'",$rdb,$check_month);
						if(mysqli_num_rows($order_count_query))
						{
							$order_count_read = mysqli_fetch_assoc($order_count_query);
							$order_count = $order_count_read['count'];
						}*/
						$_SESSION[$sessvar_name][$ocountstr] = $order_count;
					}
						
					if($order_count >= $min_orders_to_include)
					{
						$found_orders = true;
					}
					else
					{
						$found_orders = false;
					}
					
					if($found_orders)
					{
						$with_orders_count++;
					}
					
					if($found_payment_count > 0 || $found_orders)
					{
						$with_payment_or_order_count++;
						$rest_id_list = empty($rest_id_list) ? $rest_read['id'] : $rest_id_list .", ". $rest_read['id'];
					}
					$total_count++;
				}
			}
			
			$end_run_time = time();
			$elapsed = $end_run_time - $start_run_time;
			
			echo "<font style='color:#007700'>Date: " . $check_month . "</font><br>";
			//echo "Total Count: $total_count<br>";
			echo "With Orders Count: $with_orders_count";
			echo " <font style='color:#999999'>(Requires at least ";
			if($min_txns==1) echo "one"; else if($min_txns==2) echo "two"; else if($min_txns==3) echo "three"; else echo $min_txns;
			echo " successful payment";
			if($min_txns!=1) echo "s";
			echo " in a previous month)</font>";
			echo "<br>";
			echo "With Payments Count: $with_payments_count<br>";
			echo "<b><font style='color:#000088'>With Payments or Orders Count: $with_payment_or_order_count</font></b><br>";
			echo "Report Run Time: $elapsed seconds<br>";
			//echo "<br><br>Restaurant IDs: $rest_id_list<br>";
		}
		
		echo "</td></tr></table>";
	}
?>
