<?php
	if($rmode!="")
	{
		if(!isset($_SESSION['can_include_list']))
		{
			$_SESSION['can_include_list'] = array();
		}
		
		if(isset($_GET['month'])) $check_month = $_GET['month']; else $check_month = "";
		
		echo "<font style='color:#888888; font-size:14px'>Please note that this report pulls alot of data and can take up to a minute to run for each month</font>";
		echo "<br><br>";
		echo "<table cellspacing=0 cellpadding=20><tr><td>";
		echo "<select onchange='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=active_in_month&month=\" + this.value'>";
		echo "<option value=''>---Select Month---</option>";
		for($m=0; $m<32; $m++)
		{
			$smonth = date("Y-m",mktime(0,0,0,date("m") - $m,1,date("Y")));
			echo "<option value='$smonth'";
			if($smonth==$check_month) echo " selected";
			echo ">$smonth</option>";
		}
		echo "</select>";
		echo "<br><br>";
		
		if($check_month > "2000-01" && $check_month < "3000-01")
		{
			$min_orders_to_include = 1;
			
			$total_count = 0;
			$with_orders_count = 0;
			$with_payments_count = 0;
			$with_payment_or_order_count = 0;
			$require_payment_to_include = true;
			$rest_id_list = '';
			
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `lavu_lite_server`='' and `notes` NOT LIKE '%AUTO-DELETED%'");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				if($require_payment_to_include)
				{
					$rstr = "R" . $rest_read['id'];
					if(isset($_SESSION['can_include_list'][$rstr]))
					{
						if($_SESSION['can_include_list'][$rstr]=="yes")
						{
							$can_include = true;
						}
						else
						{
							$can_include = false;
						}
					}
					else
					{
						$can_include = false;
						$paid_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_amount` * 1 > 8",$rest_read['id']);// and `x_type`='auth_capture' and `x_response_code`='1'",$rest_read['id']);
						if(mysqli_num_rows($paid_query))
						{
							$paid_read = mysqli_fetch_assoc($paid_query);
							if($paid_read['count'] > 0)
							{
								$can_include = true;
							}
						}
						if(!$can_include)
						{
							$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `restaurantid`='[1]'",$rest_read['id']);
							if(mysqli_num_rows($lic_query))
							{
								$can_include = true;
							}
						}
						
						if($can_include)
						{
							$setstr = "yes";
						}
						else
						{
							$setstr = "no";
						}
						$_SESSION['can_include_list'][$rstr] = $setstr;
					}
				}
				else
				{
					$can_include = true;
				}
				
				if($can_include)
				{
					$pcountstr = "pcount_".$check_month."_" . $rest_read['data_name'];
					if(isset($_SESSION['can_include_list'][$pcountstr]))
					{
						$found_payment_count = $_SESSION['can_include_list'][$pcountstr];
					}
					else
					{
						$paid_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_amount` * 1 > 8 and `x_response_code`='1' and `x_type`='auth_capture' and `date`>='[2]-01' and `date`<='[2]-31'",$rest_read['id'],$check_month);
						if(mysqli_num_rows($paid_query))
						{
							$paid_read = mysqli_fetch_assoc($paid_query);
							$paid_count = $paid_read['count'];
							
							$found_payment_count = $paid_count;
							$_SESSION['can_include_list'][$pcountstr] = $paid_count;
						}
						else
						{
							$found_payment_count = 0;
						}
					}
					
					if($found_payment_count > 0)
					{
						$with_payments_count++;
					}
					
					$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
					$ocountstr = "count" . $min_orders_to_include . "_" . $check_month . "_" . $rest_read['data_name'];
					
					if(isset($_SESSION['can_include_list'][$ocountstr]))
					{
						$order_count = $_SESSION['can_include_list'][$ocountstr];
					}
					else
					{
						$order_count = 0;
						$order_count_query = mlavu_query("select sum(`data`) as `count` from `poslavu_MAIN_db`.`restdata` where `dataname`='[1]' and `range`>='[2]-01' and `range`<='[2]-31'",$rest_read['data_name'],$check_month);
						if(mysqli_num_rows($order_count_query))
						{
							$order_count_read = mysqli_fetch_assoc($order_count_query);
							$order_count = $order_count_read['count'];
							//echo $rest_read['data_name'] . " - " . $order_count . "<br>";
						}
						
						/*$order_count_query = mlavu_query("select count(*) as `count` from `[1]`.`orders` where `closed` >= '[2]-01 00:00:00' and `closed` <= '[2]-31 24:00:00'",$rdb,$check_month);
						if(mysqli_num_rows($order_count_query))
						{
							$order_count_read = mysqli_fetch_assoc($order_count_query);
							$order_count = $order_count_read['count'];
						}*/
						$_SESSION['can_include_list'][$ocountstr] = $order_count;
					}
						
					if($order_count >= $min_orders_to_include)
					{
						$found_orders = true;
					}
					else
					{
						$found_orders = false;
					}
					
					if($found_orders)
					{
						$with_orders_count++;
					}
					
					if($found_payment_count > 0 || $found_orders)
					{
						$with_payment_or_order_count++;
						$rest_id_list = empty($rest_id_list) ? $rest_read['id'] : $rest_id_list .", ". $rest_read['id'];
					}
					$total_count++;
				}
			}
			
			echo "Date: " . $check_month . "<br>";
			echo "Total Count: $total_count<br>";
			echo "With Orders Count: $with_orders_count<br>";
			echo "With Payments Count: $with_payments_count<br>";
			echo "With Payments or Orders Count: $with_payment_or_order_count<br>";
			echo "<br><br>Restaurant IDs: $rest_id_list<br>";
		}
		
		echo "</td></tr></table>";
	}
?>
