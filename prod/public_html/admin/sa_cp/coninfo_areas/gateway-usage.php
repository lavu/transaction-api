<?php
	if($rmode!="")
	{		
		function value_bar($val,$mult,$color,$val2=0,$color2="")
		{
			$str = "";
			if(1==2)
			{
				$str .= "<td align='right'><b>";
				//if($val2 > $val) $str .= $val2;
				//else $str .= $val;
				if(is_numeric($val) && $val * 1 > 0)
				{
					$str .= $val;
				}
				else $str .= "&nbsp;";
				$str .= "</b></td>"; 
			}
			
			//echo "<td align='right'><font style='color:#008800'>" . number_format($pay_total,0) . "</font></td>"; 
			$str .= "<td align='left'>";
			$str .= "<table cellpadding=0 cellspacing=0><tr><td style='width:" . floor($val * $mult / 2) . "px; height:16px; background-color:$color'>&nbsp;</td>";
			if($val2 > $val)
			{
				$str .= "<td style='width:" . floor((($val2 - $val) * $mult) / 2) . "px; height:16px; background-color:$color2'>&nbsp;</td>";
				
				//if($val1 / $val2 < 0.75) $str .= "<td>".$val2."</td>";
			}
			$str .= "</tr></table>";
			$str .= "</td>";
			return $str;
		}
		
		$rsubmode = create_submode_nav(array("gateway-usage-counts","gateway-usage-list","gateway-stats","usa-gateway-counts"),$rmode);
		
		if($rsubmode=="gateway-usage-counts")
		{
			echo "<table cellspacing=0 cellpadding=4>";
			$gate_query = mlavu_query("select count(*) as `count`,`gateway` from `poslavu_MAIN_db`.`restaurant_locations` left join `poslavu_MAIN_db`.`restaurants` on restaurant_locations.restaurantid=restaurants.id where `restaurants`.`last_activity`>'2015-10-01 00:00:00' and `data_name`!='DEV' and `data_name`!='lavu_inc' group by `gateway` order by `count` desc limit 400");
			while($gate_read = mysqli_fetch_assoc($gate_query))
			{
				$count = $gate_read['count'];
				$gateway = $gate_read['gateway'];
				
				if(trim($gateway)!="")
				{
					echo "<tr>";
					echo "<td align='right'>$gateway</td><td>$count</td>";
					echo value_bar($count,1,"#ccddff");
					echo "</tr>";
				}
			}
			echo "</table>";
		}
		else if($rsubmode=="usa-gateway-counts")
		{
			function valid_usa_state_abbrev($state_str)
			{
				$state_str = strtolower(trim($state_str));
				$valid_states = "AL:Alabama,
				AK:Alaska,
				AZ:Arizona,
				AR:Arkansas,
				CA:California,
				CO:Colorado, 
				CT:Connecticut, 
				DE:Delaware, 
				DC:District Of Columbia, 
				FL:Florida, 
				GA:Georgia, 
				HI:Hawaii, 
				ID:Idaho, 
				IL:Illinois, 
				IN:Indiana, 
				IA:Iowa, 
				KS:Kansas, 
				KY:Kentucky, 
				LA:Louisiana, 
				ME:Maine, 
				MD:Maryland, 
				MA:Massachusetts, 
				MI:Michigan, 
				MN:Minnesota, 
				MS:Mississippi, 
				MO:Missouri, 
				MT:Montana, 
				NE:Nebraska, 
				NV:Nevada, 
				NH:New Hampshire, 
				NJ:New Jersey, 
				NM:New Mexico, 
				NY:New York, 
				NC:North Carolina, 
				ND:North Dakota, 
				OH:Ohio, 
				OK:Oklahoma, 
				OR:Oregon, 
				PA:Pennsylvania, 
				RI:Rhode Island, 
				SC:South Carolina, 
				SD:South Dakota, 
				TN:Tennessee, 
				TX:Texas, 
				UT:Utah, 
				VT:Vermont, 
				VA:Virginia, 
				WA:Washington, 
				WV:West Virginia, 
				WI:Wisconsin, 
				WY:Wyoming";
				$state_parts = explode(",",$valid_states);
				for($n=0; $n<count($state_parts); $n++)
				{
					$inner_state_parts = explode(":",$state_parts[$n]);
					if(count($inner_state_parts)==2)
					{
						$abbrev = strtolower(trim($inner_state_parts[0]));
						$statename = strtolower(trim($inner_state_parts[1]));
						
						//echo "abbrev: $abbrev - statename: $statename - state_str: $state_str<br>";
						if($state_str==$abbrev || $state_str==$statename)
						{
							return $abbrev;
						}
					}
				}
				return "";
			}
			
			$gate_counts = array();
			$all_gate_counts = array();
			$gate_query = mlavu_query("select `state`,`gateway` from `poslavu_MAIN_db`.`restaurant_locations` left join `poslavu_MAIN_db`.`restaurants` on restaurant_locations.restaurantid=restaurants.id where `restaurants`.`last_activity`>'2015-10-01 00:00:00' and `data_name`!='DEV' and `data_name`!='lavu_inc'");
			while($gate_read = mysqli_fetch_assoc($gate_query))
			{
				$gateway = $gate_read['gateway'];
				$loc_state = $gate_read['state'];
				if(trim($gateway)=="")
				{
					$gateway = "No Gateway";
				}
			
				if(valid_usa_state_abbrev($loc_state)!="")
				{
					if(!isset($gate_counts[$gateway])) $gate_counts[$gateway] = array("gateway"=>$gateway,"count"=>0);
					$gate_counts[$gateway]['count']++;
				}			
				if(!isset($all_gate_counts[$gateway])) $all_gate_counts[$gateway] = array("gateway"=>$gateway,"count"=>0);
				$all_gate_counts[$gateway]['count']++;
			}
			
			for($i=0; $i<2; $i++)
			{
				if($i==0)
				{
					echo "Verified USA with Recent Activity: (has a valid state value)<br>";
					$gcounts = $gate_counts;
				}
				else
				{
					echo "All Accounts with Recent Activity:<br>";
					$gcounts = $all_gate_counts;
				}
				
				$narr = array();
				foreach($gcounts as $gkey => $ginfo)
				{
					$newkey = 100000 + $ginfo['count'] * 1;
					$newkey = $newkey . substr($gkey,0,4);
					
					$narr[$newkey] = $ginfo;
				}
				
				ksort($narr);
				$narr = array_reverse($narr);
				
				echo "<table cellspacing=0 cellpadding=4>";
				foreach($narr as $gkey => $ginfo)
				{
					$gateway = $ginfo['gateway'];
					$count = $ginfo['count'];
					if($gateway=="No Gateway") {$style = " style='font-weight:bold; background-color:#cceecc'";}
					else $style = "";
					echo "<tr>";
					echo "<td align='right'".$style.">$gateway</td><td".$style.">$count</td>";
					echo value_bar($count,1,"#ccddff");
					echo "</tr>";
				}
				echo "</table>";		
				echo "<br><br>";
			}
		}
		else if($rsubmode=="gateway-stats")
		{
			$count_arr = array();
			$count_arr['k000'] = array();
			$count_arr['k100'] = array();
			$count_arr['k200'] = array();
			$count_arr['k300'] = array();
			$count_arr['k400'] = array();
			$count_arr['k500'] = array();
			$total_arr = array();
			$total_arr['k000'] = array("count"=>0,"vol"=>0);
			$total_arr['k100'] = array("count"=>0,"vol"=>0);
			$total_arr['k200'] = array("count"=>0,"vol"=>0);
			$total_arr['k300'] = array("count"=>0,"vol"=>0);
			$total_arr['k400'] = array("count"=>0,"vol"=>0);
			$total_arr['k500'] = array("count"=>0,"vol"=>0);
			$gate_arr = array();
		
			$year_query = mlavu_query("select sum(`data`) as `total`,sum(`data_long`) as `count`,`data2` as `gateway`,`dataname` from `poslavu_MAIN_db`.`restdata` where `type`='reportdata - month card txns' and `range`>='2015-01' and `range`<='2015-09'");
			if(mysqli_num_rows($year_query))
			{
				$year_read = mysqli_fetch_assoc($year_query);
				$year_total = $year_read['total'];
				
				$estimated_year_total = $year_total / 3 * 4;
			}
		
			$total_vol = 0;
			$total_order_count = 0;
			$total_count = 0;
			$min_month = "2014-10";
			$max_month = "2015-09";
			$txn_query = mlavu_query("select sum(`data`) as `total`,sum(`data_long`) as `count`,`data2` as `gateway`,`dataname` from `poslavu_MAIN_db`.`restdata` where `type`='reportdata - month card txns' and `range`>='[1]' and `range`<='[2]' group by `dataname` order by `dataname` asc",$min_month,$max_month);
			while($txn_read = mysqli_fetch_assoc($txn_query))
			{
				$total = $txn_read['total'] * 1;
				if($total >= 1000)
				{
					if($total >= 500000) $ckey = 'k500';
					else if($total >= 400000) $ckey = 'k400';
					else if($total >= 300000) $ckey = 'k300';
					else if($total >= 200000) $ckey = 'k200';
					else if($total >= 100000) $ckey = 'k100';
					else $ckey = 'k000';
					
					$gkey = $txn_read['gateway'];
					
					$count_arr[$ckey][] = $txn_read;
					$total_arr[$ckey]['count']++;
					$total_arr[$ckey]['order_count']+=$txn_read['count'];
					$total_arr[$ckey]['vol']+=$total;
					
					if(!isset($gate_arr[$gkey])) $gate_arr[$gkey] = array('count'=>0,'order_count'=>0,'vol'=>0);
					$gate_arr[$gkey]['count']++;
					$gate_arr[$gkey]['order_count']+=$txn_read['count'];
					$gate_arr[$gkey]['vol']+=$total;
					
					$total_count++;
					$total_order_count += $txn_read['count'];
					$total_vol += $total;
					//echo $txn_read['dataname'] . " - " . number_format($txn_read['total'],2) . "<br>";
				}
			}
			
			echo "<style>";
			echo "	.tname { text-align:right; color:#888888; } ";
			echo "</style>";
			echo "Locations Included: " . $total_count . "<br>";
			echo "Total Volume: $" . number_format($total_vol,2) . "<br>";
			echo "Estimated 2015 Volume: $" . number_format($estimated_year_total,2) . "<br>";
			echo "Average Volume: $" . number_format($total_vol / $total_count,2) . "<br>";
			echo "Average Txn: $" . number_format($total_vol / $total_order_count,2) . "<br>";
			
			echo "<br>Location Counts:";
			echo "<table>";
			echo "<tr><td class='tname'>500K and up</td><td>" . count($count_arr['k500']) . "</td></tr>";
			echo "<tr><td class='tname'>400k up to 500</td><td>" . count($count_arr['k400']) . "</td></tr>";
			echo "<tr><td class='tname'>300K up to 400</td><td>" . count($count_arr['k300']) . "</td></tr>";
			echo "<tr><td class='tname'>200K up to 300</td><td>" . count($count_arr['k200']) . "</td></tr>";
			echo "<tr><td class='tname'>100K up to 200</td><td>" . count($count_arr['k100']) . "</td></tr>";
			echo "<tr><td class='tname'>Below 100k</td><td>" . count($count_arr['k000']) . "</td></tr>";
			echo "</table>";

			echo "<br>Total Volumes:";
			echo "<table>";
			echo "<tr><td class='tname'>500K and up</td><td>$" . number_format($total_arr['k500']['vol']) . "</td></tr>";
			echo "<tr><td class='tname'>400k up to 500</td><td>$" . number_format($total_arr['k400']['vol']) . "</td></tr>";
			echo "<tr><td class='tname'>300K up to 400</td><td>$" . number_format($total_arr['k300']['vol']) . "</td></tr>";
			echo "<tr><td class='tname'>200K up to 300</td><td>$" . number_format($total_arr['k200']['vol']) . "</td></tr>";
			echo "<tr><td class='tname'>100K up to 200</td><td>$" . number_format($total_arr['k100']['vol']) . "</td></tr>";
			echo "<tr><td class='tname'>Below 100k</td><td>$" . number_format($total_arr['k000']['vol']) . "</td></tr>";
			echo "</table>";

			echo "<br>Average Volumes:";
			echo "<table>";
			echo "<tr><td class='tname'>500K and up</td><td>$" . number_format($total_arr['k500']['vol'] / $total_arr['k500']['count']) . "</td></tr>";
			echo "<tr><td class='tname'>400k up to 500</td><td>$" . number_format($total_arr['k400']['vol'] / $total_arr['k400']['count']) . "</td></tr>";
			echo "<tr><td class='tname'>300K up to 400</td><td>$" . number_format($total_arr['k300']['vol'] / $total_arr['k300']['count']) . "</td></tr>";
			echo "<tr><td class='tname'>200K up to 300</td><td>$" . number_format($total_arr['k200']['vol'] / $total_arr['k200']['count']) . "</td></tr>";
			echo "<tr><td class='tname'>100K up to 200</td><td>$" . number_format($total_arr['k100']['vol'] / $total_arr['k100']['count']) . "</td></tr>";
			echo "<tr><td class='tname'>Below 100k</td><td>$" . number_format($total_arr['k000']['vol'] / $total_arr['k000']['count']) . "</td></tr>";
			echo "</table>";
			
			echo "<br>Average Txns:";
			echo "<table>";
			echo "<tr><td class='tname'>500K and up</td><td>$" . number_format($total_arr['k500']['vol'] / $total_arr['k500']['order_count']) . "</td></tr>";
			echo "<tr><td class='tname'>400k up to 500</td><td>$" . number_format($total_arr['k400']['vol'] / $total_arr['k400']['order_count']) . "</td></tr>";
			echo "<tr><td class='tname'>300K up to 400</td><td>$" . number_format($total_arr['k300']['vol'] / $total_arr['k300']['order_count']) . "</td></tr>";
			echo "<tr><td class='tname'>200K up to 300</td><td>$" . number_format($total_arr['k200']['vol'] / $total_arr['k200']['order_count']) . "</td></tr>";
			echo "<tr><td class='tname'>100K up to 200</td><td>$" . number_format($total_arr['k100']['vol'] / $total_arr['k100']['order_count']) . "</td></tr>";
			echo "<tr><td class='tname'>Below 100k</td><td>$" . number_format($total_arr['k000']['vol'] / $total_arr['k000']['order_count']) . "</td></tr>";
			echo "</table>";
			
			echo "<br>Gateway Volumes:";
			echo "<table>";
			foreach($gate_arr as $k => $v)
			{
				echo "<tr><td class='tname'>$k</td><td>$" . number_format($v['vol']) . "</td></tr>";
			}
			echo "</table>";
		}
		else if($rsubmode=="gateway-usage-list")
		{
			$exportstr = "";
			function show_gcol($str)
			{
				if(trim($str)=="") return "&nbsp;";
				else if(strlen($str) > 20) return substr($str,0,18)."...";
				else return $str;
			}
			$last_month = date("Y-m",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
			
			$txn_arr = array();
			$txn_query = mlavu_query("select `id`,`dataname`,`data` as `ordercount`,`data2` as `txncount` from `poslavu_MAIN_db`.`restdata` where `type`='reportdata - month total sales' and `range`='[1]'",$last_month);
			while($txn_read = mysqli_fetch_assoc($txn_query))
			{
				$txn_dataname = $txn_read['dataname'];
				if(trim($txn_dataname)!="")
				{
					$txn_arr[$txn_dataname] = $txn_read;
				}
			}
			
			$rnames = array();
			$rname_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`!=''");
			while($rname_read = mysqli_fetch_assoc($rname_query))
			{
				$distro_code = $rname_read['username'];
				$rnames[$distro_code] = $rname_read;
			}
			
			echo "<style>";
			echo "	.tcol { font-family:Verdana,Arial; font-size:10px; color:#555555; text-align:right; border-right:solid 1px #aaaaaa; } ";
			echo "	.tcolright { font-family:Verdana,Arial; font-size:10px; color:#555555; text-align:right; } ";
			echo "</style>";
			echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #aaaaaa'>";
			$gate_query = mlavu_query("select `company_name`,`title` as `location_title`,`restaurants`.`data_name` as `data_name`,`state`,`country`,`distro_code`,`gateway` from `poslavu_MAIN_db`.`restaurant_locations` left join `poslavu_MAIN_db`.`restaurants` on restaurant_locations.restaurantid=restaurants.id where `restaurants`.`last_activity`>'2015-10-01 00:00:00' and `data_name`!='DEV' and `data_name`!='lavu_inc' and `gateway`!='' order by `gateway` asc,distro_code asc,data_name asc");
			while($gate_read = mysqli_fetch_assoc($gate_query))
			{
				$g_companyname = show_gcol($gate_read['company_name']);
				$g_locationtitle = show_gcol($gate_read['location_title']);
				$g_dataname = $gate_read['data_name'];
				$gateway = $gate_read['gateway'];
				$distro_code = $gate_read['distro_code'];
				$ordercount = 0;
				$txncount = 0;
				if(isset($txn_arr[$g_dataname]))
				{
					$ordercount = $txn_arr[$g_dataname]['ordercount'];
					$txncount = $txn_arr[$g_dataname]['txncount'];
				}
				$distro_company = $distro_code;
				if(isset($rnames[$distro_code]))
				{
					$distro_company = $rnames[$distro_code]['company'];
				}
				$distro_company = show_gcol($distro_company);
								
				if(trim($gateway)!="")
				{
					echo "<tr>";
					echo "<td class='tcol'>$g_companyname</td><td>&nbsp;</td>";
					echo "<td class='tcol'>$g_locationtitle</td><td>&nbsp;</td>";
					echo "<td class='tcol'>$g_dataname</td><td>&nbsp;</td>";
					echo "<td class='tcol'>".show_gcol($gate_read['state'])."</td><td>&nbsp;</td>";
					echo "<td class='tcol'>".show_gcol($gate_read['country'])."</td><td>&nbsp;</td>";
					echo "<td class='tcol'>$distro_company</td><td>&nbsp;</td>";
					echo "<td class='tcol'>$gateway</td><td>&nbsp;</td>";
					echo "<td class='tcol'>";
					if($txncount * 1 > 0)
						echo "$" . number_format($txncount,2);
					else
						echo "&nbsp;";
					echo "</td><td>&nbsp;</td>";
					echo "<td class='tcolright'>";
					
					/*$data_updated = 0;
					$rdb = "poslavu_" . $g_dataname . "_db";
					$txn_query = mlavu_query("select count(*) as `count`, sum(total_collected) as total,left(datetime,7) as `month` from `[1]`.`cc_transactions` where pay_type='Card' and transtype='Sale' group by `month` order by `month` desc",$rdb);
					while($txn_read = mysqli_fetch_assoc($txn_query))
					{
						$data_updated++;
						$set_data = $txn_read['total'];
						$set_data2 = $gateway;
						$set_data_long = $txn_read['count'];
						$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`restdata` where `type`='[1]' and `range`='[2]' and `dataname`='[3]'","reportdata - month card txns",$txn_read['month'],$g_dataname);
						if(mysqli_num_rows($exist_query))
						{
							$exist_read = mysqli_fetch_assoc($exist_query);
							$success = mlavu_query("update `poslavu_MAIN_db`.`restdata` set `data`='[1]',`data2`='[2]',`data_long`='[3]' where `id`='[4]'",$set_data,$set_data2,$set_data_long,$exist_read['id']);
						}
						else
						{
							$success = mlavu_query("insert into `poslavu_MAIN_db`.`restdata` (`type`,`range`,`dataname`,`data`,`data2`,`data_long`) values ('[1]','[2]','[3]','[4]','[5]','[6]')","reportdata - month card txns",$txn_read['month'],$g_dataname,$set_data,$set_data2,$set_data_long);
						}
					}*/
					echo "</td>";
					echo "</tr>";
					
					$exportstr .= $g_companyname . "\t" . $g_locationtitle . "\t" . $g_dataname . "\t" . $distro_code . "\t" . $gate_read['state'] . "\t" . $gate_read['country'] . "\t" . $distro_company . "\t" . $gateway . "\n";
				}
			}
			echo "</table>";
			echo "<br>";
			if($exportstr!="")
			{
				echo "<textarea rows='20' cols='120'>";
				echo $exportstr;
				echo "</textarea>";
				echo "<br>";
			}
		}
	}
?>
