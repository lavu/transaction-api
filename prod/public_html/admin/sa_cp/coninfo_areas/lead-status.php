<?php
	if($rmode!="")
	{
		$min_datetime = date("Y-m-d",mktime(0,0,0,date("m")-3,date("d"),date("Y"))) . " 00:00:00";
		$max_datetime = date("Y-m-d") . " 23:59:59";
		
		$showfields = array("company_name","data_name","created","lead_source","lead_status");
	
		$rlist = array();
		$rdlist = array();
		$rlist_query = mlavu_query("select `id`,`data_name` from `poslavu_MAIN_db`.`restaurants`");
		while($rlist_read = mysqli_fetch_assoc($rlist_query))
		{
			$rlist[$rlist_read['id']] = $rlist_read;
			if(trim($rlist_read['data_name'])!="")
			{
				$rdlist[$rlist_read['data_name']] = $rlist_read;
			}
		}
		$ldata = array();
		$lic_query = mlavu_query("select restaurantid,applied from poslavu_MAIN_db.reseller_licenses");
		while($lic_read = mysqli_fetch_assoc($lic_query))
		{
			$restid = $lic_read['restaurantid'];
			if(is_numeric($restid) && isset($rlist[$restid]))
			{
				$dataname = $rlist[$restid]['data_name'];
				$month = substr($lic_read['applied'],0,7);
				if(isset($ldata[$dataname]))
				{
					if($month < $ldata[$dataname]) $ldata[$dataname] = $month;
				}
				else
					$ldata[$dataname] = $month;
			}
		}
			
		$tstr = "";
		$exportstr = "";
		$lead_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_leads` where `created`>='[1]' and `created`<='[2]' and `company_name`!='e' order by `created` desc",$min_datetime,$max_datetime);
		while($lead_read = mysqli_fetch_assoc($lead_query))
		{
			$set_lead_status = "";
			$lead_data_name = $lead_read['data_name'];
			if($lead_data_name!="") 
			{
				if(isset($rdlist[$lead_data_name]['id']))
				{
					$lead_restid = $rdlist[$lead_data_name]['id'];
					$set_lead_status = "Trial";
					if(isset($ldata[$lead_data_name])) $set_lead_status = "--- Went Live ---";
					$pay_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and x_type='auth_capture' and x_response_code='1' and x_amount *1 > 0",$lead_restid);
					if(mysqli_num_rows($pay_query))
					{
						$pay_read = mysqli_fetch_assoc($pay_query);
						$pay_count = $pay_read['count'];
						if($pay_count > 0)
						{
							$set_lead_status = "--- Went Live ---";
						}
					}
				}
			}
			
			$lead_read['lead_status'] = $set_lead_status;
			$tstr .= "<tr>";
			for($f=0; $f<count($showfields); $f++)
			{
				$fieldname = $showfields[$f];
				$fieldvalue = $lead_read[$fieldname];
				$show_fieldvalue = $fieldvalue;
				if(strlen($show_fieldvalue) > 25) $show_fieldvalue = substr($show_fieldvalue,0,25) . "...";
				$tstr .= "<td align='right'>$show_fieldvalue</td>";
				if($f > 0) $exportstr .= "\t";
				$exportstr .= $fieldvalue;
			}
			$exportstr .= "\n";
			$tstr .= "</tr>";
		}
		
		echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888'>";
		echo $tstr;
		echo "</table>";
		
		echo "<br>";
		if($exportstr!="")
		{
			echo "<textarea rows='20' cols='120'>";
			echo $exportstr;
			echo "</textarea>";
			echo "<br>";
		}
	}
?>