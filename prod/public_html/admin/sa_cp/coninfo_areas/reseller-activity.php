<?
	if($rmode!="")
	{
		$exportstr = "";
		
		$rsubmode = create_submode_nav(array("current-balance","points-awarded","points-redeemed","purchases"),$rmode);
		
		if($rsubmode=="current-balance")
		{
			$total_value = 0;
			$total_awarded = 0;
			$tstr = "";
			$points_query = mlavu_query("select username,country,credits from `poslavu_MAIN_db`.`resellers` where `isCertified`='1' and `username` NOT LIKE 'lavusales505' and `username`!='' and `credits`!='' order by `credits` * 1 desc");
			while($points_read = mysqli_fetch_assoc($points_query))
			{
				$rname = $points_read['username'];
				$current_points = $points_read['credits'];
				$country = $points_read['country'];
				
				$awarded = 0;
				$min_datetime = date("Y-m-d",mktime(0,0,0,date("m")-1,date("d"),date("Y"))) . " 00:00:00";
				$max_datetime = date("Y-m-d") . " 00:00:00";
				$award_query = mlavu_query("select sum(`credits_applied`) as `credits_applied` from `poslavu_MAIN_db`.`reseller_billing_log` where `resellername`='[1]' and `datetime`>='[2]' and `datetime`<='[3]'",$rname,$min_datetime,$max_datetime);
				if(mysqli_num_rows($award_query))
				{
					$award_read = mysqli_fetch_assoc($award_query);
					$awarded = $award_read['credits_applied'];
				}
				
				$tstr .= "<tr>";
				$tstr .= "<td align='right'>" . $rname . "</td>";
				$tstr .= "<td>" . $current_points . "</td>";
				$tstr .= "<td>" . $awarded . "</td>";
				$tstr .= "</tr>";
				$total_value += $current_points * 1;
				$total_awarded += $awarded * 1;
				
				$exportstr .= $rname . "\t" . $country . "\t" . $current_points . "\t" . $awarded . "\n";
			}
			
			echo "Total Balance: " . number_format(floor($total_value),0) . "<br>";
			echo "Awarded in last Month: " . number_format(floor($total_awarded),0) . "<br>";
			echo "<br><br>";
			echo "<table>";
			echo $tstr;
			echo "</table>";
		}
		else if($rsubmode=="points-awarded")
		{
			$cond = "";
			$points_query = mlavu_query("select username,country,credits from `poslavu_MAIN_db`.`resellers` where `isCertified`='1' and `username` NOT LIKE 'lavusales505' and `username`!='' and `credits`!='' order by `credits` * 1 desc");
			while($points_read = mysqli_fetch_assoc($points_query))
			{
				$rname = $points_read['username'];
				if($cond!="") $cond .= ",";
				$cond .= "'" . ConnectionHub::getConn('poslavu')->escapeString($rname) . "'";
			}
			
			if($cond!="")
			{
				$total_awarded = 0;
				
				//echo "select sum(`credits_applied`) as `credits_applied`,left(`datetime`,7) as `month` from `poslavu_MAIN_db`.`reseller_billing_log` where `resellername` IN ($cond) and `datetime`>='$min_datetime' and `datetime`<='2015-12-31 23:59:59' group by `month` order by `month` desc<br>";
				
				$tstr = "";
				$exportstr = "";
				$min_datetime = "2014-01-01 00:00:00";
				$max_datetime = "2015-12-31 23:59:59";
				$award_query = mlavu_query("select sum(`credits_applied`) as `credits_applied`,left(`datetime`,7) as `month` from `poslavu_MAIN_db`.`reseller_billing_log` where `resellername` IN ($cond) and `datetime`>='[2]' and `datetime`<='[3]' group by `month` order by `month` desc","",$min_datetime,$max_datetime);
				while($award_read = mysqli_fetch_assoc($award_query))
				{
					$month = $award_read['month'];
					$awarded = $award_read['credits_applied'];
					$tstr .= "<tr><td align='right'>$month</td><td>$awarded</td></tr>";
					$exportstr .= $month . "\t" . $awarded . "\n";
					$total_awarded += $awarded * 1;
				}
				
				echo "Total Awarded: $total_awarded<br><br>";
				echo "<table cellspacing=0 cellpadding=3 style='border:solid 1px #aaaaaa'>";
				echo $tstr;
				echo "</table>";
			}
		}
		else if($rsubmode=="points-redeemed" || $rsubmode=="purchases")
		{
			//$min_datetime = "2013-01-01 00:00:00";
			//$max_datetime = "2013-12-30 23:59:59";
			$min_datetime = "2015-01-01 00:00:00";
			$max_datetime = "2015-09-30 23:59:59";
			$total_value = 0;
			$total_prevalue = 0;
			$total_license = 0;
			$total_upsell = 0;
			
			$rslist = array();
			$country_query = mlavu_query("select `username`,`country` from `poslavu_MAIN_db`.`resellers` where `username`!=''");
			while($country_read = mysqli_fetch_assoc($country_query))
			{
				//echo $country_read['username'] . "<br>";
				$rslist[$country_read['username']] = $country_read;
			}
			
			$tstr = "";
			$last_name = "";
			if($rsubmode=="points-redeemed")
			{
				$spent_term = "Redeemed";
				$value_is_money = false;
				$extra_cols = array();
				$purchase_query = mlavu_query("select `resellername`,`purchased`,`value` from `poslavu_MAIN_db`.`reseller_licenses` where `notes` LIKE 'PURCHASED WITH POINTS%' and `purchased`>='[1]' and `purchased`<='[2]' order by `resellername` asc, `purchased` asc",$min_datetime,$max_datetime);
			}
			else if($rsubmode=="purchases")
			{
				$spent_term = "Spent";
				$value_is_money = true;
				$extra_cols = array("license_value");
				$purchase_query = mlavu_query("select `resellername`,`purchased`,`type`,`cost` as `value`,`value` as `license_value` from `poslavu_MAIN_db`.`reseller_licenses` where `notes` NOT LIKE 'PURCHASED WITH POINTS%' and `purchased`>='[1]' and `purchased`<='[2]' and `cost` > 0 order by `resellername` asc, `purchased` asc",$min_datetime,$max_datetime);
			}
			while($purchase_read = mysqli_fetch_assoc($purchase_query))
			{
				$resellername = $purchase_read['resellername'];
				$value = $purchase_read['value'];
				$purchased = $purchase_read['purchased'];
				$total_value += $value * 1;
				
				$show_name = "&nbsp;";
				$tdclass = "regular";
				if($resellername != $last_name)
				{
					$show_name = $resellername;
					if($last_name!="") $tdclass = "bordertop";
				}
				
				if($value_is_money)
					$show_value = "$" . number_format($value,2);
				else
					$show_value = $value;
				$extra_export_str = "";
				
				$tstr .= "<tr>";
				$tstr .= "<td align='right' class='$tdclass'>" . $show_name . "</td>";
				$tstr .= "<td class='$tdclass' style='color:#aaaaaa'>" . $purchased . "</td>";
				$tstr .= "<td class='$tdclass' align='right'>" . $show_value . "</td>";
				for($n=0; $n<count($extra_cols); $n++)
				{
					$colname = $extra_cols[$n];
					$evalue = $purchase_read[$colname];
					if($colname=="license_value")
					{
						$total_prevalue += $evalue * 1;
						$show_evalue = "$" . number_format($evalue,2);
						/*$show_ltype = "upsell";
						$evalueint = floor($evalue * 1);
						if($evalueint==895 || $evalueint == 1495  || $evalueint>=2495)
						{
							$show_ltype = "license";
						}*/
						$show_ltype = "license";
						if(strpos($purchase_read['type']," to ")!==false)
						{
							$show_ltype = "upsell";
						}
						if($show_ltype=="license") $total_license += $value * 1;
						else if($show_ltype=="upsell") $total_upsell += $value * 1;
						
						$tstr .= "<td class='$tdclass' align='right'style='color:#aabbcc'>" . $show_evalue . "</td>";
						$tstr .= "<td class='$tdclass' align='right'style='color:#aabbcc'>" . $show_ltype . "</td>";
						$extra_export_str .= "\t" . $evalue . "\t" . $show_ltype;
					}
					else
					{
						$show_evalue = $evalue;
						$tstr .= "<td class='$tdclass' align='right'style='color:#aabbcc'>" . $show_evalue . "</td>";
					}
				}
				$tstr .= "</td>";
				
				$exportstr .= $resellername . "\t";
				if(isset($rslist[$resellername]))
				{
					$exportstr .= $rslist[$resellername]['country'] . "\t";
				}
				else
				{
					$exportstr .= "\t";
				}
				$exportstr .= $purchased . "\t" . $value . $extra_export_str . "\n";
				$last_name = $resellername;
			}
			echo "Total ".$spent_term.": ";
			if($value_is_money)
				echo "$" . number_format($total_value,2);
			else
				echo number_format(floor($total_value),0);
				
			if($total_prevalue > 0)
			{
				echo "<br>";
				echo "Total Value Before Discounts: $" . number_format($total_prevalue,2);
				echo "<br>";
				echo "Total Discounts: $" . number_format($total_prevalue - $total_value,2);
				echo "<br>";
				echo "Total New Licenses: $" . number_format($total_license,2);
				echo "<br>";
				echo "Total Upsells: $" . number_format($total_upsell,2);
			}
			echo "<br><br>";
			echo "<style>";
			echo "	.bordertop { border-top:solid 1px #aaaaaa; } ";
			echo "	.regular { } ";
			echo "</style>";
			echo "<table cellspacing=0 cellpadding=3 style='border:solid 1px #aaaaaa'>";
			echo $tstr;
			echo "</table>";
		}
		else
		{
			$min_datetime = "2015-08-16 00:00:00";
			$max_datetime = date("Y-m-d H:i:s");
			
			$total_past_balance = 0;
			
			$exportstr = "";
			
			echo "<table>";
			$points_query = mlavu_query("select username,credits from `poslavu_MAIN_db`.`resellers` where `isCertified`='1' and `username`!='' and `credits`!='' order by `credits` * 1 desc");
			while($points_read = mysqli_fetch_assoc($points_query))
			{
				$rname = $points_read['username'];
				$current_points = $points_read['credits'];
				$awarded = 0;
				
				$award_query = mlavu_query("select sum(`credits_applied`) as `credits_applied` from `poslavu_MAIN_db`.`reseller_billing_log` where `resellername`='[1]' and `datetime`>='[2]' and `datetime`<='[3]'",$rname,$min_datetime,$max_datetime);
				if(mysqli_num_rows($award_query))
				{
					$award_read = mysqli_fetch_assoc($award_query);
					$awarded = $award_read['credits_applied'];
				}
				
				$purchase_query = mlavu_query("select sum(`value`) as `purchased` from `poslavu_MAIN_db`.`reseller_licenses` where resellername='[1]' and `notes` LIKE 'PURCHASED WITH POINTS%' and `purchased`>='[2]' and `purchased`<='[3]'",$rname,$min_datetime,$max_datetime);
				if(mysqli_num_rows($purchase_query))
				{
					$purchase_read = mysqli_fetch_assoc($purchase_query);
					$purchased = $purchase_read['purchased'];
				}
				
				$aug15points = ($current_points * 1) - ($awarded * 1) + ($purchased * 1);
				$total_past_balance += ($aug15points * 1);
				
				echo "<tr>";
				echo "<td align='right'>" . $rname . "</td>";
				echo "<td>" . $current_points . "</td>";
				echo "<td>- " . $awarded . "</td>";
				echo "<td>+ " . $purchased . "</td>";
				echo "<td>= " . $aug15points . "</td>";
				echo "</tr>";
				
				$exportstr .= $rname . "\t" . $current_points . "\t-\t" . $awarded . "\t+\t" . $purchased . "\t=\t" . $aug15points . "\n";
			}
			echo "</table>";
			echo "<br>Total Past Balance " . $total_past_balance . "<br>";
		}
		
		echo "<br>";
		if($exportstr!="")
		{
			echo "<textarea rows='20' cols='120'>";
			echo $exportstr;
			echo "</textarea>";
			echo "<br>";
		}
	}
?>
