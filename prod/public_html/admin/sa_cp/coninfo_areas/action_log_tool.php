<?php
	if($rmode!="")
	{
		$page = (isset($_GET['page']))?$_GET['page']:1;
		if($page==1) $_SESSION['mcount_start_ts'] = time();
		$pagesize = 10;//00;
		if(isset($_SESSION['mcount']) && $page > 1)
		{
			$mcount = $_SESSION['mcount'];
		}
		else
		{
			$mcount = array();
			for($i=1; $i<=14; $i++)
			{
				$title = $i . " Month";
				if($i > 1) $title .= "s";
				$title .= " Ago";
				$datetime = date("Y-m-d H:i:s",mktime(0,0,0,date("m")-$i,date("d"),date("Y")));
				$mcount[] = array("title"=>$title,"count"=>0,"datetime"=>$datetime);
			}
			$mcount[] = array("title"=>"All","count"=>0,"datetime"=>"0000-00-00 00:00:00");
		}
		
		
		set_time_limit(1200);
		
		function get_db_server_ip()
		{
			return "192.168.100.82";
		}
		//echo ($page-1)*$pagesize.",".$pagesize . "<br>";
		//exit();
		
		$astr = "";
		$start_ts = time();
		ob_start();
		$rest_query = mlavu_query("select `data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `notes` NOT LIKE '%AUTO-DELETED%' and `lavu_lite_server`='' limit ".($page-1)*$pagesize.",".$pagesize);
		while($rest_read = mysqli_fetch_assoc($rest_query))
		{
			$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
			for($i=0; $i<count($mcount); $i++)
			{
				$min_datetime = $mcount[$i]['datetime'];
				$log_query = mlavu_query("select count(*) as `count` from `[1]`.`action_log` where `server_time`>='[2]'",$rdb,$min_datetime);
				if(mysqli_num_rows($log_query))
				{
					$log_read = mysqli_fetch_assoc($log_query);
					$mcount[$i]['count'] += $log_read['count'];					
				}
			}
			
			//--------------------------------------- Start Code to remove action logs --------------------------------//
			$del_count = 0;
			$delete_records_before = "2014-11-01 00:00:00";
			if($delete_records_before!="")
			{
				$del_count_query = mlavu_query("select count(*) as `count` from `[1]`.`action_log` where `server_time`<'[2]'",$rdb,$delete_records_before);
				if(mysqli_num_rows($del_count_query))
				{
					$del_count_read = mysqli_fetch_assoc($del_count_query);
					$del_count = $del_count_read['count'];
				}
			}
			$astr .= "checking $rdb - found $del_count records to remove<br>";
			if($del_count > 0)
			{
				$exportname =  $rdb . "--action_log.sql";
				$dumpdir = "/home/poslavu/logs/export/" . substr($rest_read['data_name'],0,1);
				if(!is_dir($dumpdir)) mkdir($dumpdir,0755);
				$dumpfile = $dumpdir . "/" . $exportname;
				
				$dumpcmd = "mysqldump -u poslavu -pA8n8d8y8 -h ".lavuShellEscapeArg(get_db_server_ip())." --skip-lock-tables --where=\"server_time<'".ConnectionHub::getConn('poslavu')->escapeString($delete_records_before)."'\" ".lavuShellEscapeArg($rdb)." action_log > ".lavuShellEscapeArg($dumpfile);
				$astr .= " <b>exporting $rdb...</b><br>";
				//lavuExec($dumpcmd);
				exec($dumpcmd);
				if(is_file($dumpfile))
				{
					exec("gzip $dumpfile");
					$del_success = mlavu_query("delete from `[1]`.`action_log` where `server_time`<'[2]'",$rdb,$delete_records_before);
					$del_success_count = ConnectionHub::getConn('poslavu')->affectedRows();
					$opt_success = mlavu_query("optimize table `[1]`.`action_log`",$rdb);
					$astr .= "deleting ($del_success_count)...";
					if($del_success) $astr .= " (success)";
					$astr .= "<br>";
					$astr .= "optimizing...";
					if($opt_success) $astr .= " (success)";
					$astr .= "<br>";
				}
			}
			//---------------------------------------------------------------------------------------------------------//
		}
		$obstr = ob_get_contents();
		ob_end_clean();
		
		echo $astr;
		
		$end_ts = time();
		$elapsed = $end_ts - $start_ts;
		$mins = ($elapsed - $elapsed % 60) / 60;
		$seconds = $elapsed % 60;
		echo "Elapsed: $mins Minutes and $seconds Seconds<br>";
		
		$elapsed = $end_ts - $_SESSION['mcount_start_ts'];
		$mins = ($elapsed - $elapsed % 60) / 60;
		$seconds = $elapsed % 60;		
		echo "Total Elapsed: $mins Minutes and $seconds Seconds<br>";
		echo "Records so far: " . ($page) * $pagesize . " / ~15,000<br>";
				
		echo "<table cellspacing=0 cellpadding=6>";
		for($i=0; $i<count($mcount); $i++)
		{
			if(isset($mcount[$i]['count']) && isset($mcount[count($mcount)-1]['count']) && $mcount[count($mcount)-1]['count'] > 0)
			{
				$percent = $mcount[$i]['count'] / $mcount[count($mcount)-1]['count'] * 100;
			}
			else $percent = 0;
			
			echo "<tr>";
			echo "<td align='right'>" . $mcount[$i]['title'] . "</td>";
			//echo "<td>" . $mcount[$i]['datetime'] . "</td>";
			echo "<td align='right'>" . number_format($mcount[$i]['count'],0) . "</td>";
			echo "<td align='right'>" . number_format($percent,2) . "%</td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "<script type='text/javascript'>";
		echo "function next_page() { ";
		echo "	window.location = '/sa_cp/index.php?mode=$mode&rmode=$rmode&rsubmode=$rsubmode&page=".($page + 1)."'; ";
		echo "} ";
		echo "setTimeout('next_page()',2000); ";
		echo "</script>";
		echo "<br><input type='button' value='Next >>' onclick='next_page()' /><br>";
		
		$_SESSION['mcount'] = $mcount;
	}
?>
