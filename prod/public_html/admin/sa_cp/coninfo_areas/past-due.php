<?php
	if($rmode!="")
	{
		$start_run_time = time();

		$rlist = array();
		$rdlist = array();
		$rlist_query = mlavu_query("select `id`,`data_name`,`disabled` from `poslavu_MAIN_db`.`restaurants` where `dev`='0' and `demo`='0'");
		while($rlist_read = mysqli_fetch_assoc($rlist_query))
		{
			$rlist[$rlist_read['id']] = $rlist_read;
			if(trim($rlist_read['data_name'])!="")
			{
				$rdlist[$rlist_read['data_name']] = $rlist_read;
			}
		}
		
		$ldata = array();
		$lic_query = mlavu_query("select restaurantid,applied from poslavu_MAIN_db.reseller_licenses");
		while($lic_read = mysqli_fetch_assoc($lic_query))
		{
			$restid = $lic_read['restaurantid'];
			if(is_numeric($restid) && isset($rlist[$restid]))
			{
				$dataname = $rlist[$restid]['data_name'];
				$month = substr($lic_read['applied'],0,7);
				if(isset($ldata[$dataname]))
				{
					if($month < $ldata[$dataname]) $ldata[$dataname] = $month;
				}
				else
					$ldata[$dataname] = $month;
			}
		}
		
		$dlist = array();
		$due_query = mlavu_query("select `dataname`,`due_info` from `poslavu_MAIN_db`.`payment_status` where `due_info` LIKE '%due%' and `dataname`!=''");
		while($due_read = mysqli_fetch_assoc($due_query))
		{
			$dlist[$due_read['dataname']] = $due_read['due_info'];
		}

		function pd_get_due_prop($str,$prop,$type="string")
		{
			$dparts = explode($prop,$str);
			$val = "";
			if(count($dparts) > 1)
			{
				$dparts = explode("|",$dparts[1]);
				$val = $dparts[0];
			}
			
			if($type=="number")
			{
				$val = str_replace(",","",str_replace("$","",$val));
				return $val * 1;
			}
			else return $val;
		}
		
		$exportstr = "";
		$total_owed = 0;
		$total_hosting_owed = 0;
		$total_other_owed = 0;
		$tstr = "";
		$active_query = mlavu_query("select dataname,sum(`data`) as `order_count` from `poslavu_MAIN_db`.`restdata` where `type`='daily sales' and `range`>='2015-08-15' and `range`<='2015-10-15' and `data` > 0 group by dataname");
		while($active_read = mysqli_fetch_assoc($active_query))
		{
			$dataname =  $active_read['dataname'];
			if(isset($rdlist[$dataname])&&isset($dlist[$dataname]))
			{
				$disabled = $rdlist[$dataname]['disabled'];
				$due_info = $dlist[$dataname];
				
				if($disabled=="" || $disabled=="0" || !$disabled)
				{
					$due_amount = pd_get_due_prop($due_info,"Total:","number");
					if($due_amount > 0)
					{						
						$due_hosting = pd_get_due_prop($due_info,"Hosting:","number");
						$due_since = trim(pd_get_due_prop($due_info,"Due since"));
						
						if($due_since > "2014-01-01")
						{
							$has_payments = false;
							if(isset($ldata[$dataname]))
							{
								$has_payments = true;
							}
							else
							{
								$restid = $rdlist[$dataname]['id'];
								if($restid * 1 > 0)
								{
									$p_query = mlavu_query("select `id` from `payment_responses` where `match_restaurantid`='[1]' and `x_amount` * 1 > 0 and x_type='auth_capture' and x_response_code='1' order by id desc limit 1",$restid);
									if(mysqli_num_rows($p_query))
									{
										$has_payments = true;
									}
								}
							}
							
							if($has_payments)
							{
								$tstr .= "<tr>";
								$tstr .= "<td align='right'>$dataname</td>";
								$tstr .= "<td align='right' style='color:#aaaaaa'>$" . number_format($due_hosting,2) . "</td>";
								$tstr .= "<td align='right' style='color:#aaaaaa'>$" . number_format($due_amount * 1 - $due_hosting * 1,2) . "</td>";
								$tstr .= "<td align='right'>$" . number_format($due_amount,2) . "</td>";
								$tstr .= "<td>" . $due_since . "</td>";
								$tstr .= "</td>";
								
								$total_owed += $due_amount * 1;
								$total_hosting_owed += $due_hosting * 1;
								$total_other_owed += ($due_amount * 1 - $due_hosting * 1);
								if($exportstr!="") $exportstr .= "\n";
								$exportstr .= $dataname . "\t" . $due_hosting . "\t" . ($due_amount - $due_hosting) . "\t" . $due_amount . "\t" . $due_since;
							}
						}
					}
				}
			}
		}
		
		$end_run_time = time();
		$elapsed = $end_run_time - $start_run_time;
		echo "Elapsed Time: $elapsed second";
		if($elapsed != 1) echo "s";
		echo "<br><br>";
		//echo "active count: " . mysqli_num_rows($active_query) . "<br>";
		echo "Total Owed: ". number_format($total_owed,2) . "<br>";
		echo "Total Owed (hosting only): ". number_format($total_hosting_owed,2) . "<br>";
		echo "Total Owed (non-hosting only): ". number_format($total_other_owed,2) . "<br>";
		echo "<br>";
		echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #777777'>";
		echo $tstr;
		echo "</table>";
		
		echo "<br>";
		if($exportstr!="")
		{
			echo "<textarea rows='20' cols='120'>";
			echo $exportstr;
			echo "</textarea>";
			echo "<br>";
		}
	}
?>
