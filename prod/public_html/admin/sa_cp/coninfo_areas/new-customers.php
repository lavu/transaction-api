<?php
	if($rmode!="")
	{
		$month_range = (isset($_GET['month_range']))?$_GET['month_range']:1;
		if(!is_numeric($month_range) || $month_range > 120)
		{
			return;
		}
		
		echo "<select name='month_range' onchange='window.location = \"/sa_cp/index.php?mode=$mode&rmode=$rmode&month_range=\" + this.value' />";
		for($i=1; $i<=6; $i++)
		{
			echo "<option value='$i'";
			if($i==$month_range) echo " selected";
			echo ">$i Month";
			if($i > 1) echo "s";
			echo "</option>";
		}
		echo "</select>";
		echo "<br><br>";
		
		$min_datetime = date("Y-m-d",mktime(0,0,0,date("m")-$month_range,date("d"),date("Y"))) . " 00:00:00";
		$max_datetime = date("Y-m-d") . " 23:59:59";
		
		$showfields = array("company_name","data_name","distro_code","email","created","x_amount","date","terminals");//,"lead_source","lead_status");
	
		$rlist = array();
		$rdlist = array();
		$rlist_query = mlavu_query("select `id`,`data_name`,`distro_code`,`company_name`,`created` from `poslavu_MAIN_db`.`restaurants`");
		while($rlist_read = mysqli_fetch_assoc($rlist_query))
		{
			$rlist[$rlist_read['id']] = $rlist_read;
			if(trim($rlist_read['data_name'])!="")
			{
				$rdlist[$rlist_read['data_name']] = $rlist_read;
			}
		}
		//ini_set("display_errors","1");
		$sulist = array();
		$signup_query = mlavu_query("select `dataname`,`package`,`email` from `poslavu_MAIN_db`.`signups` where `dataname`!=''");
		while($signup_read = mysqli_fetch_assoc($signup_query))
		{
			$sulist[$signup_read['dataname']] = $signup_read;
		}
		$pslist = array();
		$payment_status_query = mlavu_query("select `dataname`, `custom_max_ipads` from `poslavu_MAIN_db`.`payment_status` where `dataname`!=''");
		while($payment_status_read = mysqli_fetch_assoc($payment_status_query))
		{
			$pslist[$payment_status_read['dataname']] = $payment_status_read;
		}
		$ldata = array();
		$lic_query = mlavu_query("select restaurantid,applied from poslavu_MAIN_db.reseller_licenses");
		while($lic_read = mysqli_fetch_assoc($lic_query))
		{
			$restid = $lic_read['restaurantid'];
			if(is_numeric($restid) && isset($rlist[$restid]))
			{
				$dataname = $rlist[$restid]['data_name'];
				$month = substr($lic_read['applied'],0,7);
				if(isset($ldata[$dataname]))
				{
					if($month < $ldata[$dataname]) $ldata[$dataname] = $month;
				}
				else
					$ldata[$dataname] = $month;
			}
		}
		
		$total_count = 0;
		$rests_included = array();
		$min_date = substr($min_datetime,0,10);
		$max_date = substr($max_datetime,0,10);
		$terminalsByPackage = array('1' => '1', '2' => '2', '3' => '10', '4' => '0', '5' => '1', '6' => '2', '7' => '3', '8' => '1', '9' => '1', '10' => '2', '11' => '1', '12' => '2', '13' => '10', '14' => '2', '15' => '3', '16' => '1', '17' => '1', '18' => '2', '19' => '3', '20' => '1', '21' => '1', '22' => '2', '23' => '3', '25' => '1', '26' => '5');
		$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' and `date`<='[2]' and `x_type`='auth_capture' and `x_response_code`='1' and `match_restaurantid`!='' and `x_amount` * 1 > 0 order by `date` asc",$min_date,$max_date);
		while($pay_read = mysqli_fetch_assoc($pay_query))
		{
			$restid = $pay_read['match_restaurantid'];
			if(isset($rlist[$restid]))
			{
				$rest_read = $rlist[$restid];
				$rest_dataname = $rest_read['data_name'];
				
				$su_email = "";
				if(isset($sulist[$rest_dataname]) && isset($sulist[$rest_dataname]['email']))
				{
					$su_email = $sulist[$rest_dataname]['email'];
				}
				$rest_read['email'] = $su_email;
				$terminals = "1";
				if(isset($sulist[$rest_dataname]['package']))
				{
					if (isset($pslist[$rest_dataname]['custom_max_ipads']) && trim(($pslist[$rest_dataname]['custom_max_ipads']))!="")
					{
						$terminals = $pslist[$rest_dataname]['custom_max_ipads'];
					}
					else if (isset($terminalsByPackage[$sulist[$rest_dataname]['package']]))
					{
						$terminals = $terminalsByPackage[$sulist[$rest_dataname]['package']];
					}
				}
				$rest_read['terminals'] = $terminals;

				if(trim($rest_dataname)!="")
				{
					if(!isset($rests_included[$rest_dataname]))
					{
						$rests_included[$rest_dataname] = $pay_read['date'];
						$prev_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `date`<'[2]' and `x_type`='auth_capture' and `x_response_code`='1' and `match_restaurantid`!='' and `x_amount` * 1 > 0",$restid,$min_date);
						if(mysqli_num_rows($prev_query))
						{
							$prev_read = mysqli_fetch_assoc($prev_query);
							$prev_count = $prev_read['count'];
							
							if($prev_count * 1 < 1)
							{
								$tstr .= "<tr>";
								for($f=0; $f<count($showfields); $f++)
								{
									$fieldname = $showfields[$f];
									if(isset($rest_read[$fieldname]) || isset($pay_read[$fieldname]))
									{
										if(isset($rest_read[$fieldname]))
										{
											$fieldvalue = $rest_read[$fieldname];
										}
										else
										{
											$fieldvalue = $pay_read[$fieldname];
										}
										$show_fieldvalue = $fieldvalue;
										if(strlen($show_fieldvalue) > 25) $show_fieldvalue = substr($show_fieldvalue,0,25) . "...";
										$tstr .= "<td align='right'>$show_fieldvalue</td>";
										if($f > 0) $exportstr .= "\t";
										$exportstr .= $fieldvalue;
									}
								}
								$exportstr .= "\n";
								$tstr .= "</tr>";
								$total_count++;
							}
						}
					}
				}
			}	
		}
			
		/*$tstr = "";
		$exportstr = "";
		$lead_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_leads` where `created`>='[1]' and `created`<='[2]' and `company_name`!='e' order by `created` desc",$min_datetime,$max_datetime);
		while($lead_read = mysqli_fetch_assoc($lead_query))
		{
			$set_lead_status = "";
			$lead_data_name = $lead_read['data_name'];
			if($lead_data_name!="") 
			{
				if(isset($rdlist[$lead_data_name]['id']))
				{
					$lead_restid = $rdlist[$lead_data_name]['id'];
					$set_lead_status = "Trial";
					if(isset($ldata[$lead_data_name])) $set_lead_status = "--- Went Live ---";
					$pay_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and x_type='auth_capture' and x_response_code='1' and x_amount *1 > 0",$lead_restid);
					if(mysqli_num_rows($pay_query))
					{
						$pay_read = mysqli_fetch_assoc($pay_query);
						$pay_count = $pay_read['count'];
						if($pay_count > 0)
						{
							$set_lead_status = "--- Went Live ---";
						}
					}
				}
			}
			
			$lead_read['lead_status'] = $set_lead_status;
			$tstr .= "<tr>";
			for($f=0; $f<count($showfields); $f++)
			{
				$fieldname = $showfields[$f];
				$fieldvalue = $lead_read[$fieldname];
				$show_fieldvalue = $fieldvalue;
				if(strlen($show_fieldvalue) > 25) $show_fieldvalue = substr($show_fieldvalue,0,25) . "...";
				$tstr .= "<td align='right'>$show_fieldvalue</td>";
				if($f > 0) $exportstr .= "\t";
				$exportstr .= $fieldvalue;
			}
			$exportstr .= "\n";
			$tstr .= "</tr>";
		}*/
		
		echo "First Payments: (does not include reseller-licenses)<br>";
		echo "Count: $total_count<br>";
		echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888'>";
		echo $tstr;
		echo "</table>";
		
		echo "<br>";
		if($exportstr!="")
		{
			echo "<textarea rows='20' cols='120'>";
			echo $exportstr;
			echo "</textarea>";
			echo "<br>";
		}
	}
?>
