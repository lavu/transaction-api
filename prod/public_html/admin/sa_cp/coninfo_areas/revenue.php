<?php
	if($rmode!="")
	{
		function value_bar($val,$mult,$color,$val2=0,$color2="")
		{
			$str = "";
			if(1==2)
			{
				$str .= "<td align='right'><b>";
				//if($val2 > $val) $str .= $val2;
				//else $str .= $val;
				if(is_numeric($val) && $val * 1 > 0)
				{
					$str .= $val;
				}
				else $str .= "&nbsp;";
				$str .= "</b></td>"; 
			}
			
			//echo "<td align='right'><font style='color:#008800'>" . number_format($pay_total,0) . "</font></td>"; 
			$str .= "<td align='left'>";
			$str .= "<table cellpadding=0 cellspacing=0><tr><td style='width:" . floor($val * $mult / 2000) . "px; height:16px; background-color:$color'>&nbsp;</td>";
			if($val2 > $val)
			{
				$str .= "<td style='width:" . floor((($val2 - $val) * $mult) / 2000) . "px; height:16px; background-color:$color2'>&nbsp;</td>";
				
				//if($val1 / $val2 < 0.75) $str .= "<td>".$val2."</td>";
			}
			$str .= "</tr></table>";
			$str .= "</td>";
			return $str;
		}
		
		function add_condition($field,$cond)
		{
			$conn = ConnectionHub::getConn('poslavu');
			if($cond=="exists") return " and `".$conn->escapeString($field)."`!=''";
			else if($cond=="blank") return " and `".$conn->escapeString($field)."`=''";
			else return "";
		}
		
		function add_license_amounts_only_condition()
		{
			$lic_amounts = array();
			$lic_amounts[] = array(895,"license",1);
			$lic_amounts[] = array(716,"license",1);
			$lic_amounts[] = array(1495,"license",2);
			$lic_amounts[] = array(3495,"license",3);
			$lic_amounts[] = array(2495,"license",2);
			$lic_amounts[] = array(600,"upgrade",1);
			$lic_amounts[] = array(1000,"upgrade",1);
			$lic_amounts[] = array(1600,"upgrade",2);
			$lic_amounts[] = array(2000,"upgrade",2);
			$lic_amounts[] = array(2600,"upgrade",2);
			$lic_amounts[] = array(4500,"buyin");
			$lic_amounts[] = array(9000,"buyin");
			$discount_amounts = array(0,10,15,20,25,30,35,40,50);
			
			$cond = "";
			for($i=0; $i<count($lic_amounts); $i++)
			{
				for($n=0; $n<count($discount_amounts); $n++)
				{
					$amt = number_format($lic_amounts[$i][0] * 1 - $lic_amounts[$i][0] * $discount_amounts[$n] / 100,2);
					$amt = str_replace(",","",$amt);
					$amt = $amt * 1;
					if($cond!="") $cond .= " or ";
					$cond .= "`x_amount`*1='".ConnectionHub::getConn('poslavu')->escapeString($amt)."'*1";
				}
			}
			return " and ($cond)";
		}
					
		echo "<table><tr>";
		for($y=0; $y<2; $y++)
		{
			$year = 2014 + $y;
			$min_date = $year . "-01-01";
			$max_date = $year . "-12-25";
			echo "<td valign='top'>"; // For Year Cols
			
			echo "<table><tr><td align='right'>";
			
			$rev_query = mlavu_query("select sum(`x_amount`) as `total_rev` from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' and `date`<='[2]' and x_response_code='1' and x_type='auth_capture' and (`batch_status`!='N' or `batch_action` LIKE 'covered by%') and `batch_action` NOT LIKE 'used to cover%'",$min_date,$max_date);
			if(mysqli_num_rows($rev_query))
			{
				$rev_read = mysqli_fetch_assoc($rev_query);
				$total_rev = $rev_read['total_rev'];
				
				$refunds_query = mlavu_query("select sum(`x_amount`) as `total_credit` from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' and `date`<='[2]' and x_response_code='1' and x_type='credit'",$min_date,$max_date);
				if(mysqli_num_rows($refunds_query))
				{
					$refunds_read = mysqli_fetch_assoc($refunds_query);
					$total_rev -= $refunds_read['total_credit'] * 1;
				}
				
				if($year==date("Y"))
					$projected_rev = $total_rev * 1 / (date("z") + 1) * 365;
				else
					$projected_rev = "";
				
				echo "<table cellpadding=4 cellspacing=0>";
				echo "<tr><td valign='top' align='right' style='color:#888888'>Total Revenue for $year (processed through Lavu authnet account):</td><td valign='top' align='right'><b>$" . number_format($total_rev,2) . "</b></td></tr>";
				if($year==date("Y"))
				{
					echo "<tr><td valign='top' align='right' style='color:#888888'>Projected:</td><td valign='top' align='right'>$" . number_format($projected_rev,2) . "</td></tr>";
				}
				else
				{
					echo "<tr><td>&nbsp;</td></tr>";
				}
				echo "</table>";
				echo "<br><br>";
			}
						
			$ranges = array();
			$ranges[] = array("All",0,9999999,"","");
			//$ranges[] = array("Recurring",0,9999999,"x_subscriptionid","exists");
			//$ranges[] = array("Non-Recurring",0,9999999,"x_subscriptionid","blank");
			$ranges[] = array("Under 400",0,400);
			$ranges[] = array("400 and Up",400,999999);
			$ranges[] = array("Verified License Amounts Only",0,9999999,"license_amounts_only","");
			
			echo "<table cellpadding=4 cellspacing=0>";
			for($n=0; $n<count($ranges); $n++)
			{
				$rtitle = $ranges[$n][0];
				$rlow = $ranges[$n][1];
				$rhigh = $ranges[$n][2];
				$rcfield = $ranges[$n][3];
				$rccond = $ranges[$n][4];
				
				if($rcfield=="license_amounts_only")
				{
					$condstr = add_license_amounts_only_condition();
				}
				else
				{
					$condstr = add_condition($rcfield,$rccond);
				}
				
				$unbatched_data = array();
				$unbatched_query = mlavu_query("select sum(`x_amount`) as `total_unbatched`,left(`date`,7) as `month` from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' and `date`<='[2]' and x_response_code='1' and x_type='auth_capture' and `batch_status`='N' and `x_amount` * 1 >= '[3]' * 1 and `x_amount` * 1 < '[4]' * 1".$condstr." group by `month` order by `month` desc",$min_date,$max_date,$rlow,$rhigh);
				while($unbatched_read = mysqli_fetch_assoc($unbatched_query))
				{
					$month = $unbatched_read['month'];
					$total = $unbatched_read['total_unbatched'];
					
					$unbatched_data[$month] = $total;
				}
		
				$cov_data = array();
				$cov_query = mlavu_query("select sum(`x_amount`) as `total_cov`,left(`date`,7) as `month` from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' and `date`<='[2]' and x_response_code='1' and x_type='auth_capture' and `batch_action` LIKE 'used to cover%' and `x_amount` * 1 >= '[3]' * 1 and `x_amount` * 1 < '[4]' * 1".$condstr." group by `month` order by `month` desc",$min_date,$max_date,$rlow,$rhigh);
				while($cov_read = mysqli_fetch_assoc($cov_query))
				{
					$month = $cov_read['month'];
					$total = $cov_read['total_cov'];
					
					$cov_data[$month] = $total;
				}
				
				$ref_data = array();
				$refunds_query = mlavu_query("select sum(`x_amount`) as `total_credit`,left(`date`,7) as `month` from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' and `date`<='[2]' and x_response_code='1' and x_type='credit' and `x_amount` * 1 >= '[3]' * 1 and `x_amount` * 1 < '[4]' * 1".$condstr." group by `month` order by `month` desc",$min_date,$max_date,$rlow,$rhigh);
				//echo "select sum(`x_amount`) as `total_credit` from `poslavu_MAIN_db`.`payment_responses` where `date`>='$min_date' and `date`<='$max_date' and x_response_code='1' and x_type='credit' and `x_amount` * 1 >= '$rlow' * 1 and `x_amount` * 1 < '$rhigh' * 1 group by `month` order by `month` desc<br>";
				while($refunds_read = mysqli_fetch_assoc($refunds_query))
				{
					$month = $refunds_read['month'];
					$total = $refunds_read['total_credit'];
					
					//echo "credit: " . $month . " - " . $total . "<br>";
					$ref_data[$month] = $total;
				}
				
				echo "<tr><td colspan='12' bgcolor='#8899aa' style='color:#ffffff;' align='center'><b>$rtitle</b></td></tr>";
				$rev_data = array();
				$rev_query = mlavu_query("select sum(`x_amount`) as `total_rev`,left(`date`,7) as `month` from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' and `date`<='[2]' and x_response_code='1' and x_type='auth_capture' and (`batch_status`!='N') and `x_amount` * 1 >= '[3]' * 1 and `x_amount` * 1 < '[4]' * 1".$condstr." group by `month` order by `month` desc",$min_date,$max_date,$rlow,$rhigh);
				while($rev_read = mysqli_fetch_assoc($rev_query))
				{
					$month = $rev_read['month'];
					$total_rev = floor($rev_read['total_rev']);
					$base_rev = $total_rev;
					$unbatched_amount = 0;
					$coverage_amount = 0;
					if(isset($unbatched_data[$month]))
					{
						$unbatched_amount = $unbatched_data[$month] * 1;
						$total_rev = $total_rev * 1 + $unbatched_data[$month] * 1;
					}
					if(isset($cov_data[$month]))
					{
						$coverage_amount = $cov_data[$month] * 1;
						$total_rev = $total_rev * 1 - $cov_data[$month] * 1;
					}
					$before_refunds =  $total_rev;
					if(isset($ref_data[$month]))
					{
						$refund_amount = $ref_data[$month] * 1;
						$total_rev = $total_rev * 1 - $ref_data[$month] * 1;
					}
					if($month==date("Y-m"))
						$projected_rev = floor($total_rev * 1 / (date("d")) * 31);
					else
						$projected_rev = floor($total_rev);
					$rev_data[$month] = array("month"=>$month,"base_rev"=>$base_rev,"unbatched"=>$unbatched_amount,"coverage"=>$coverage_amount,"before_refunds"=>$before_refunds,"refunds"=>$refund_amount,"total_rev"=>$total_rev,"projected_rev"=>$projected_rev);
				}
				
				for($m=1; $m<=12; $m++)
				{
					$pmonth = $m;
					if($m < 10) $pmonth = "0" . $m;
					
					$month = $year . "-" . $pmonth;
					echo "<tr>";
					echo "<td align='right' style='color:#7777aa'>$month</td>";
					
					if(isset($rev_data[$month]))
					{
						$total_rev = $rev_data[$month]['total_rev'];
						$projected_rev = $rev_data[$month]['projected_rev'];
						$base_rev = $rev_data[$month]['base_rev'];
						$unbatched = $rev_data[$month]['unbatched'];
						$coverage = $rev_data[$month]['coverage'];
						$before_refunds = $rev_data[$month]['before_refunds'];
						$refunds = $rev_data[$month]['refunds'];
						
						/*echo "<td align='right' style='color:#777777'>$" . number_format($base_rev,2) . "</td>";
						echo "<td align='right' style='color:#aaaaaa'>+ $" . number_format($unbatched,2) . "</td>";
						echo "<td align='right' style='color:#aaaaaa'>- $" . number_format($coverage,2) . "</td>";
						echo "<td align='right' style='color:#777777'>= $" . number_format($before_refunds,2) . "</td>";
						echo "<td align='right' style='color:#aaaaaa'>- $" . number_format($refunds,2) . "</td>";*/
						
						echo "<td align='right' style='color:#777777'>= $" . number_format($total_rev,2) . "</td>";
						echo "<td align='right'>$" . number_format($projected_rev,2) . "</td>";
						echo value_bar($total_rev,1,"#88bbcc",$projected_rev,"#ccddff");
					}
					else
					{
						echo "<td colspan='12'>&nbsp;</td>";
					}
					
					echo "</tr>";
				}
				echo "<tr><td>&nbsp</td></tr>";
			}
			echo "</table>";
			echo "</tr></table>";
			
			//echo "<br><br>";
			echo "</td><td valign='top'>"; // For Year Cols
		}
		echo "<br><br>";
		
		echo "</td></tr></table>";
	}
?>
