<?php
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";

	if (!isset($_POST['ajax']))
		echo draw_artp_javascript_functions();
	else
		echo draw_artp_ajax_response();
		
	function draw_artp_javascript_functions() {
		$s_retval = "";
		$s_retval .= '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>';
		$s_retval .= '<script type="text/javascript">'.file_get_contents(dirname(__FILE__).'/tools/ajax_functions.js').'</script>';
		$s_retval .= '<script type="text/javascript">
	function remove_change_attached_restaurant_payment_ajax_div() {
		var s_id = "change_attached_restaurant_payment_ajax_div";
		$("#"+s_id).stop(false,true);
		$("#"+s_id).animate({opacity:0},300,function(){
			$("#"+s_id).remove();
			});
	}
	function draw_change_attached_restaurant_payment_ajax_div(link_id,payment_response_id) {
		var s_id = "change_attached_restaurant_payment_ajax_div";
		while ($("#"+s_id).length > 0)
			$("#"+s_id).remove;
		$("#"+link_id).after("<div id=\""+s_id+"\" style=\"padding:50px;position:fixed;top:100px;left:100px;background-color:white;border:1px solid black;z-index:100;\">Restaurant dataname: <input type=\"textbox\" size=\"40\" name=\"dataname\" /><input type=\"button\" onclick=\"submit_change_attached_restaurant_payment_ajax_div();\" value=\"Submit\" /><input type=\"button\" onclick=\"remove_change_attached_restaurant_payment_ajax_div();\" value=\"Cancel\" /><input type=\"hidden\" name=\"link_id\" value=\""+link_id+"\" /><input type=\"hidden\" name=\"payment_response_id\" value=\""+payment_response_id+"\"></div>");
		$("#"+s_id).css({opacity:0});
		$("#"+s_id).animate({opacity:1},300);
	}
	function submit_change_attached_restaurant_payment_ajax_div() {
		var s_id = "change_attached_restaurant_payment_ajax_div";
		var dataname = $("#"+s_id).children("input[name=dataname]").val();
		var link_id = $("#"+s_id).children("input[name=link_id]").val();
		var payment_response_id = $("#"+s_id).children("input[name=payment_response_id]").val();
		send_change_attached_restaurant_payment_ajax(link_id,payment_response_id,dataname);
		remove_change_attached_restaurant_payment_ajax_div();
	}
	function change_attached_restaurant_payment_ajax(link_id,payment_response_id) {
		draw_change_attached_restaurant_payment_ajax_div(link_id,payment_response_id);
	}
	function send_change_attached_restaurant_payment_ajax(link_id,payment_response_id,restaurant_dataname) {
		var jlink = $("#"+link_id);
		var jrow = jlink.parent().parent();
		var s_retval = send_ajax_request("/sa_cp/assign_restaurant_to_payment.php",{ajax:1,payment_response_id:payment_response_id,restaurant_dataname:restaurant_dataname});
		var a_retval = s_retval.split("[*note*]");
		if (a_retval[0] == "success")
			jrow.html(a_retval[1]);
		else
			alert("Error: "+a_retval[1]);
	}
</script>';
		return $s_retval;
	}
	
	function draw_artp_ajax_response() {
		global $maindb;
		
		// get post data
		$i_payment_response_id = (isset($_POST['payment_response_id'])) ? (int)$_POST['payment_response_id'] : 0;
		$s_dataname = (isset($_POST['restaurant_dataname'])) ? $_POST['restaurant_dataname'] : '';
		if ($i_payment_response_id == 0 || $s_dataname == '')
			return "failure[*note*]Please provide a payment response id and a restaurant dataname";
		
		// check that the restaurant exists
		$restaurant_query = mlavu_query("SELECT `id` FROM `[maindb]`.`restaurants` WHERE `data_name`='[dataname]' LIMIT 1",
			array("maindb"=>$maindb, "dataname"=>$s_dataname));
		if (!$restaurant_query)
			return "failure[*note*]That dataname wasn't found";
		if (mysqli_num_rows($restaurant_query) == 0)
			return "failure[*note*]That dataname wasn't found";
		$a_restaurant = mysqli_fetch_assoc($restaurant_query);
		$s_restaurant_id = $a_restaurant['id'];
		
		// check that the payment response exists (and doesn't have a match id)
		$pr_query = mlavu_query("SELECT `match_restaurantid` FROM `[maindb]`.`payment_responses` WHERE `id`='[id]' LIMIT 1",
			array("maindb"=>$maindb, "id"=>$i_payment_response_id));
		if (!$pr_query)
			return "failure[*note*]That payment response wasn't found";
		if (mysqli_num_rows($pr_query) == 0)
			return "failure[*note*]That payment response wasn't found";
		$a_payment_response = mysqli_fetch_assoc($pr_query);
		$match_restaurantid = $a_payment_response['match_restaurantid'];
		if (trim($match_restaurantid) != "")
			return "failure[*note*]That payment response already has a restaurant assigned to it";
		
		// assign the payment response id
		mlavu_query("UPDATE `[maindb]`.`payment_responses` SET `match_restaurantid`='[match_id]' WHERE `id`='[id]'",
			array("maindb"=>$maindb, "match_id"=>$s_restaurant_id, "id"=>$i_payment_response_id));
		if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
			return "failure[*note*]Database couldn't be written to";
		
		ob_start();
		$_GET['mode'] = 'payment_history';
		$_GET['get_single_row_id'] = $i_payment_response_id;
		require_once(dirname(__FILE__)."/index.php");
		$s_contents = ob_get_contents();
		ob_end_clean();
		$s_row = substr($s_contents, strpos($s_contents, "<tr bgcolor="));
		$s_row = substr($s_row, strpos($s_row, ">"));
		$s_row = substr($s_row, 0, strpos($s_row, "</tr>"));
		return "success[*note*]".$s_row;
	}
?>
