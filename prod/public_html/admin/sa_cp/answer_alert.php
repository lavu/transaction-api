<?php
	if(isset($_POST['Digits']))
	{
		$dig = $_POST['Digits'];
		
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo '<Response>';
		echo '<Say voice="man">Thank you.';
		if($dig=="8")
		{
			echo '  The alert has been suppressed';
		}
		echo "</Say>";
		echo '</Response>';
	}
	else
	{
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
		echo '<Response>';
		echo '<Gather method="POST" numDigits="1" action="answer_alert.php">';
		//echo '<Say voice="woman" loop="3">Beware The Pigeons.</Say>';
		//echo '<Play>w_pigeon_w.wav</Play>';
		//echo '<Play>w_pigeon_w.wav</Play>';
		//echo '<Play>w_pigeon_w.wav</Play>';
		echo '<Say voice="woman">An Alert has been generated at Lavu</Say>';
		echo '<Say voice="man">Press 8 to suppress this alert</Say>';
		echo '</Gather>';
		echo '</Response>';
	}
?>
