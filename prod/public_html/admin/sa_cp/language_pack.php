<?php

require_once("../cp/resources/gt_functions.php");
require_once("../cp/resources/language_dictionary.php");

if (!function_exists("speak"))
{
	function speak($str)
	{
		return $str;
	}
}

if (can_access("language_pack"))
{
	ini_set("display_errors", "1");

	$display			= "";
	$form_posted		= (isset($_POST['posted']));	
	$language_pack_id	= (isset($_GET['pack_id'])) ? $_GET['pack_id'] : false;
	$msg				= reqvar("msg", "");

	if ($language_pack_id)
	{
		$get_pack_info = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` = '[1]'", $language_pack_id);
		$pack_info = mysqli_fetch_assoc($get_pack_info);
		$languageName = $pack_info['english_name'];

		$display .= "<br><a href='index.php?mode=language_pack'>Back to Language List</a><br>";
		$display .= '<div style="width:100%; margin-left:20px; margin-top:20px">
			<span> Please upload csv file. <a href="/sa_cp/language.csv" target="_blank">Click here</a> to see csv file format</span><br/>
			<span> Please upload json file. <a href="/sa_cp/language_json.json" target="_blank">Click here</a> to see json file format</span>
            <form action="languageUpload.php" method="post" name="upload_language" id = "upload_language_form" enctype="multipart/form-data">
				<div style="width:60%;margin-top:10px;margin-bottom:10px;">
					<div style="float:left; width:30%;">
						  <label>Select File</label>
					</div>
                            <div style="float:right; width:70%;">
                                <input type="file" name="file" id="file" class="input-large">
                            </div>
                </div><br>
                        <!-- Button -->
				<div  style="width:60%;margin-top:20px;">
					<div style="text-align:center; width:30%; margin-top:20px;">
						<input type="hidden" name="pack_id" value = "' . $language_pack_id. '">
						<button type="button" onClick="uploadLanguagePack()" name="Update">Update</button>
					</div>
                </div>
			</form>
		</div>';

		if ($msg) {
			switch ($msg) {
				case 1:
					$display .= '<br><span style="color:red; margin-left:20px">File format is not valid.</span>';
					$display .= '<br><span style="color:red; margin-left:20px">It should be in the following sequence "word_or_phrase, translation, used_by, tag".</span>';
					break;
				case 2:
					$display .= '<br><span style="color:green; margin-left:20px">Language Dictionary Updated Successfully.</span>';
					break;
				case 3:
					$display .= '<br><span style="color:red; margin-left:20px">Please upload csv or or json file.</span>';
					break;
			}
		}

		$display .= "<br><br><span style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 18px;'><font color='#000033'>".$pack_info['language']."</font> - <font color='#003300'>".$pack_info['english_name']."</font></span><br><br>";

		$first_letter = (isset($_GET['fl']))?$_GET['fl']:"A";
		$letter_list = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Other");
		$display .= "Words or phrases starting with: &nbsp;";
		foreach ($letter_list as $letter)
		{
			if ($letter != $first_letter)
			{ 
				$display .= "<a href='index.php?mode=language_pack&pack_id=".$language_pack_id."&fl=".$letter."'>";
			}

			$display .= $letter;

			if ($letter != $first_letter)
			{ 
				$display .= "</a>&nbsp;&nbsp;";
			}
			else
			{ 
				$display .= "&nbsp;&nbsp";
			}
		}

		$display .= "<br><br>";

		$field = "";

		$qtitle_category = FALSE;
		$qtitle_item = "Translation";

		$category_tablename = "";
		$inventory_tablename = "language_dictionary";
		$category_fieldname = "";
		$inventory_fieldname = "id";
		$inventory_select_condition = " AND `pack_id` = '".$language_pack_id."'";

		if ($first_letter == "Other")
		{
			foreach ($letter_list as $letter) 
			{
				if ($letter != "Other")
				{
					$inventory_select_condition .= " AND `word_or_phrase` NOT LIKE '".$letter."%'";
				}
			}
		}
		else
		{ 
			$inventory_select_condition .= " AND `word_or_phrase` LIKE '".$first_letter."%'";
		}

		$inventory_primefield = "word_or_phrase";
		$category_primefield = "";

		$used_by_options = array(
			array( "Front End/Back End", "" ),
			array( "Front End", "app" ),
			array( "Back End", "cp" )
		);

		$qfields = array();
		$qfields[] = array( "word_or_phrase", "word_or_phrase", "Word or Phrase", "text" );
		$qfields[] = array( "translation", "translation", "Translation", "text" );
		if ($language_pack_id == 1)
		{
			$qfields[] = array("used_by", "used_by", "Used By", "select", $used_by_options);
		}
		$qfields[] = array("id", "id");	
		$qdbfields = get_qdbfields($qfields);
	}
	else
	{
		$field = "";

		$qtitle_category = FALSE;
		$qtitle_item = "Language";

		$category_tablename = "";
		$inventory_tablename = "language_packs";
		$category_fieldname = "";
		$inventory_fieldname = "id";
		if (can_access("technical"))
		{
			$inventory_select_condition = "";
		}
		else
		{
			$inventory_select_condition = " AND `id` != '1'";
		}
		$inventory_primefield = "english_name";
		$category_primefield = "";

		$qfields = array();
		$qfields[] = array( "english_name", "english_name", "English Name", "input", array( "font-size:13px;' size='30" ) );
		$qfields[] = array( "language", "language", "Language Name", "input", array( "font-size:13px;' size='30" ) );		
		$qfields[] = array( "id", "id");	
		$qdbfields = get_qdbfields($qfields);						
	}

	$cat_count = 1;

	if ($form_posted)
	{
		$item_count = 1;
		while (isset($_POST["ic_".$cat_count."_".$inventory_primefield."_".$item_count]))
		{						
			$item_value = array(); // get posted values
			for($n = 0; $n < count($qfields); $n++)
			{
				$qname = $qfields[$n][0];
				$qtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:FALSE;
				$set_item_value = $_POST['ic_'.$cat_count.'_'.$qname.'_'.$item_count];

				if ($qtitle && $set_item_value==$qtitle)
				{
					$set_item_value = "";
				}

				if ($qname=="cost") 
				{
					$set_item_value = str_replace("$","",$set_item_value);
					$set_item_value = str_replace(",","",$set_item_value);
				}

				$item_value[$qname] = $set_item_value;
			}

			$item_id = $item_value['id'];
			$item_delete = $item_value['delete'];
			$item_name = $item_value[$inventory_primefield];

			if ($item_delete=="1")
			{
				delete_poslavu_dbrow($inventory_tablename, $item_id);
			}
			else if ($item_name != "")
			{
				$dbfields = array();
				if ($language_pack_id)
				{
					$dbfields[] = array( "pack_id", $language_pack_id );
				}

				for ($n = 0; $n < count($qdbfields); $n++)
				{
					$qname = $qdbfields[$n][0];
					$qfield = (isset($qdbfields[$n][1]))?$qdbfields[$n][1]:$qname;
					$dbfields[] = array( $qfield, $item_value[$qname] );
				}

				update_poslavu_dbrow($inventory_tablename, $dbfields, $item_id);
			}								
			$item_count++;
		}
		deleteAllLanguagePack();
	}

	$details_column = "";//"Zip Codes";
	$send_details_field = "";//"zip_codes";

	$qinfo = array();
	$qinfo["qtitle_category"] = $qtitle_category;
	$qinfo["qtitle_item"] = $qtitle_item;
	$qinfo["category_tablename"] = $category_tablename;
	$qinfo["inventory_tablename"] = $inventory_tablename;
	$qinfo["category_fieldname"] = $category_fieldname;
	$qinfo["inventory_fieldname"] = $inventory_fieldname;
	$qinfo["inventory_select_condition"] = $inventory_select_condition;
	$qinfo["inventory_primefield"] = $inventory_primefield;
	$qinfo["category_primefield"] = $category_primefield;
	$qinfo["details_column"] = $details_column;
	$qinfo["send_details"] = $send_details_field;
	$qinfo["field"] = $field;

	$qinfo["category_filter_by"] = $category_filter_by;
	$qinfo["category_filter_value"] = $category_filter_value;
	$qinfo["inventory_filter_by"] = "loc_id";
	$qinfo["inventory_filter_value"] = $locationid;

	$field_code = create_dimensional_form($qfields, $qinfo, $cfields);

	$display .= "<table cellspacing='0' cellpadding='3'><tr><td style='border:2px solid #DDDDDD'>";
	$display .= "<form name='ing' method='post' action=''>";
	$display .= "<input type='submit' value='Save' style='width:120px'><br>&nbsp;";
	$display .= "<input type='hidden' name='posted' value='1'>";
	$display .= $field_code;
	$display .= "&nbsp;<br><input type='submit' value='Save' style='width:120px'>";
	$display .= "</form>";
	$display .= "</td></tr></table>";

	echo $display; 
}

function cat_row_code($qtitle_category, $cfields, $name="",$id="", $category_read=FALSE, $cinfo=FALSE)
{
	$cat_row_code = "<div id='icrow_(cat)_0' style='display:block'>";
	$cat_row_code .= "<table cellspacing=0 cellpadding=0 width='100%'><tr>";
	$cat_row_code .= "<td align='left' width='360'>";

	$cat_row_code .= "<table cellspacing=0 cellpadding=0><tbody><td>";
	$cat_row_code .= gt_input("ic_(cat)_category",$qtitle_category." Name","#252f6b","font-weight:bold' size='30",$name)."</td>";		
	$ival = array();
	$cat_row_code .= "</td>";
	$cat_row_code .= row_code_fields("ic_(cat)_col_[qname]",$cfields,$ival,$category_read);
	$cat_row_code .= "</tbody></table>";

	$cat_row_code .= gt_hidden("ic_(cat)_categoryid",$id);
	$cat_row_code .= gt_hidden("ic_(cat)_delete_0",0);
	$cat_row_code .= "</td>";

	if($cinfo && is_array($cinfo))
	{
		$details_column = $cinfo['details_column'];
		$send_details = $cinfo['send_details'];
		$category_primefield = $cinfo['category_primefield'];

		if($details_column)
		{				
			if($send_details) $send_details = "ic_(cat)_col_".$send_details;
			//$cat_row_code .= $category_primefield;
			$cat_row_code .= "<td>".gt_details("ic_(cat)_col_details",$details_column,"ic_(cat)_category",$send_details)."</td>";
		}		
	}

	$cat_row_code .= "<td align='right'>&nbsp;<a style='cursor:pointer' onclick='click_delete((cat),0)'><font color='#880000'><b>X</b></font></a>&nbsp;</td>";
	$cat_row_code .= "</tr></table>";
	$cat_row_code .= "</div>";
	$cat_row_code .= "<div id='icrow_del_(cat)_0' style='display:none'>";
	$cat_row_code .= "<table width='100%'><tr><td id='icrow_delinfo_(cat)_0' style='text-decoration: line-through;' width='360'>&nbsp;</td><td width='10'><a style='cursor:pointer' onclick='click_undelete((cat),0)'><font color='#008800'><b>O</b></font></a></td></tr></table>";
	$cat_row_code .= "</div>";
	return $cat_row_code;
}

function row_code_fields($set_qcol_name,$qdbfields,$ival,$inventory_read)
{
	global $language_pack_id;

	$item_row_code = "";
	$item_row_code_hidden = "";
	// Added here to remove same menu item query call multiple times LP-7689
	if ($ival['id']) {
		// Added here to remove same menu item query call multiple times LP-7689 By Hayat
		$item_query = lavu_query("select currency_type, combo, forced_modifier_group_id, modifier_list_id  from menu_items where id = '[1]'", $ival['id']);
		$item_details = mysqli_fetch_assoc($item_query);
	}
	for($i=0; $i<count($qdbfields); $i++)
	{
		$qname = $qdbfields[$i][0];
		$qfield = (isset($qdbfields[$i][1]))?$qdbfields[$i][1]:$qname;
		$qtitle = (isset($qdbfields[$i][2]))?$qdbfields[$i][2]:$qname;
		$qtype = (isset($qdbfields[$i][3]))?$qdbfields[$i][3]:FALSE;
		$qprop = (isset($qdbfields[$i][4]))?$qdbfields[$i][4]:array();
		$qextraval = (isset($qdbfields[$i][5]))?$qdbfields[$i][5]:FALSE;

		$qprop_value = $qprop;
		for($n=0; $n<8; $n++)
		{
			if(!isset($qprop[$n])) $qprop[$n] = "";
		}

		if($qtype=="set_hidden" && $qprop && !is_array($qprop))
		{
			$ival[$qname] = $qprop;
		}
		else if($inventory_read)
		{
			$ival[$qname] = $inventory_read[$qfield];
			if($ival[$qname]!="" && $qname=="cost") $ival[$qname] = "$" . number_format($ival[$qname],"2",".",",");
		}
		else if($qtype=="hidden" && $qprop && !is_array($qprop))
		{
			$ival[$qname] = $qprop;
		}
		else
		{
			$ival[$qname] = "";
		}

		if ($qextraval && isset($ival[$qextraval]))
		{
			 $show_qextraval = $ival[$qextraval];
		}
		else
		{
			$show_qextraval = "";
		}

		$qcol_name = str_replace("[qname]", $qname, $set_qcol_name);
		$qrow_name = str_replace($qname, "name", $qcol_name);

		$read_only = "";
		if (($language_pack_id != 1) && ($i == 0))
		{
			$read_only = " readonly='readonly'";
		}

		if ($qtype == "input")
		{
			$item_row_code .= "<td>".gt_input($qcol_name, $qtitle, "#1c591c", $qprop[0], $ival[$qname])."</td>";
		}
		else if ($qtype == "text")
		{
			$item_row_code .= "<td>".gt_text($qcol_name, $qtitle, "#1c591c", "font-size: 15px;", $ival[$qname], $read_only)."</td>";
		}
		else if ($qtype=="hidden" || $qtype=="set_hidden")
		{
			$item_row_code_hidden .= gt_hidden($qcol_name, $ival[$qname]);
		}
		else if ($qtype == "picture")
		{
			$item_row_code .= "<td>".gt_picture($qcol_name, $ival[$qname], $qprop)."</td>";
		}
		else if ($qtype == "select")
		{
			$item_row_code .= "<td>".gt_select($qcol_name, $qprop_value, $ival[$qname], "#1c591c", '', '', $item_details)."</td>";
		}
		else if ($qtype == "special")
		{
			$item_row_code .= "<td>".gt_special($qcol_name, $qtitle, $ival[$qname], $qrow_name, "#1c591c", $qprop[0], $qprop[1], $qprop[2], $qprop[3], $show_qextraval, $item_details)."</td>";
		}
	}

	if ($item_row_code_hidden != "")
	{
		$item_row_code .= "<td>" . $item_row_code_hidden . "</td>";
	}

	return $item_row_code;
}

function item_row_code($qdbfields, $details_column=FALSE, $send_details=FALSE, $inventory_primefield=FALSE, $field=FALSE, $inventory_read=FALSE, $allow_reorder=FALSE)
{
	global $language_pack_id;

	$ival = array();
	if ($inventory_read)
	{
		$ival['id'] = $inventory_read['id'];
	}
	else
	{
		$ival['id'] = "";
	}

	$item_row_code = "";
	$item_row_code .= "<div id='icrow_(cat)_(item)' name='icrow_(cat)_(item)' style='display:block' onmouseover='if(1==2) this.style.borderBottom = \"solid 1px #777777\"' onmouseout='if(1==2) this.style.borderBottom = \"solid 0px #ffffff\"'>";
	$item_row_code .= "<table cellspacing=0 cellpadding=0><tbody><tr>";

	if ($allow_reorder)
	{
		$item_row_code .= "<td>";
		$item_row_code .= "<table cellspacing=0 cellpadding=0><tr><td width='24' height='16' background='images/movebox.jpg' onselectstart='return false;' onmousedown='return moveitem_mousedown(\"(cat)\",\"(item)\");' onmouseup='moveitem_mouseup(\"(cat)\",\"(item)\")' style='cursor:move'/>";
		if ($inventory_read)
		{
			if(isset($_GET['test'])) $row_order_type = "text' style='width:24px"; else $row_order_type = "hidden";
			$item_row_code .= "&nbsp;&nbsp;<input type='$row_order_type' name='icrow_(cat)_(item)_order' id='icrow_(cat)_(item)_order' value='".$inventory_read['_order']."'>";
		}
		else
		{
			$item_row_code .= "&nbsp;";
		}
		$item_row_code .= "</td></tr></table>";
		$item_row_code .= "</td>";
	}

	$item_row_code .= row_code_fields("ic_(cat)_[qname]_(item)", $qdbfields, $ival, $inventory_read);

	/*$efields = explode(",",query_result("settings","`setting`='extra item fields'","value"));
	for($i=0; $i<count($efields); $i++)
	{
		$efield = trim($efields[$i]);
		if($efield!="")
		{
			$fieldval = get_multi_field_value($efield, $extra_fields,"");
			$item_row_code .= "<td>".gt_input("ic_(cat)_extra".$i."_(item)",$efield,"#1c591c","' size='8",$fieldval)."</td>";
		}
	}*/

	if ($details_column)
	{
		if ($send_details)
		{
			$send_details = "ic_(cat)_".$send_details."_(item)";
		}
		$item_row_code .= "<td>".gt_details("ic_(cat)_details_(item)", $details_column, "ic_(cat)_".$inventory_primefield."_(item)", $send_details)."</td>";
	}
	//$item_row_code .= "<td>&nbsp;<a style='cursor:pointer' onclick='click_delete((cat),(item))'><font color='#880000'><b>X</b></font></a>&nbsp;</td>";
	if (!$language_pack_id)
	{
		$item_row_code .= "<td>&nbsp;<a style='cursor:pointer; text-decoration:none;' href='index.php?mode=language_pack&pack_id=(item_id)'><font color='#880000'><b>Edit</b></font></a>&nbsp;</td>";
	}
	$item_row_code .= "</tr></tbody></table>";
	$item_row_code .= gt_hidden("ic_(cat)_id_(item)", $ival['id']);
	$item_row_code .= gt_hidden("ic_(cat)_delete_(item)", 0);
	$item_row_code .= "</div>";
	$item_row_code .= "<div id='icrow_del_(cat)_(item)' name='icrow_del_(cat)_(item)' style='display:none'>";
	$item_row_code .= "<table width='100%'><tbody><tr><td id='icrow_delinfo_(cat)_(item)' style='text-decoration: line-through;'>&nbsp;</td><td width='10'><a style='cursor:pointer' onclick='click_undelete((cat),(item))'><font color='#008800'><b>O</b></font></a></td></tr></tbody></table>";
	$item_row_code .= "</div>";

	return $item_row_code;
}

function get_qdbfields($qfields)
{
	$qdbfields = array();
	for ($n = 0; $n < count($qfields); $n++)
	{
		if (count($qfields[$n]) > 1)
		{
			$qdbtitle = (isset($qfields[$n][2]))?$qfields[$n][2]:"";
			$qdbtype = (isset($qfields[$n][3]))?$qfields[$n][3]:"";
			$qdbprop = (isset($qfields[$n][4]))?$qfields[$n][4]:array();
			$qdbextraval = (isset($qfields[$n][5]))?$qfields[$n][5]:"";
			$qdbfields[] = array($qfields[$n][0], $qfields[$n][1], $qdbtitle, $qdbtype, $qdbprop, $qdbextraval);
		}
	}

	return $qdbfields;
}

function wizard_button_new($img, $title, $prop, $w, $h, $clr="#000000")
{
	$str = "";
	$str .= "<table cellspacing=0 cellpadding=0 style='background: URL(".$img."); background-repeat:none; background-position:top left; width:".$w."px; height:".$h."px; cursor:pointer' ".$prop.">";
	$str .= "<td width='26'>&nbsp;</td>";
	$str .= "<td style='font-size:10px; font-weight:bold; color:".$clr."'>".$title."</td>";
	$str .= "</table>";

	return $str;
	//return "<a style='cursor:pointer; color:#0000aa' $prop><img src='$img' border='0' /></a>";
}

function create_dimensional_form($qfields, $qinfo, $cfields=FALSE)
{
	global $language_pack_id;

	$qtitle_category = $qinfo['qtitle_category'];
	$qtitle_item = $qinfo['qtitle_item'];
	$category_tablename = $qinfo['category_tablename'];
	$inventory_tablename = $qinfo['inventory_tablename'];
	$category_fieldname = $qinfo['category_fieldname'];
	$inventory_fieldname = $qinfo['inventory_fieldname'];
	$inventory_select_condition = $qinfo['inventory_select_condition'];
	$inventory_primefield = $qinfo['inventory_primefield'];
	$details_column = $qinfo['details_column'];
	$category_primefield = $qinfo['category_primefield'];
	$field = $qinfo['field'];
	if(isset($qinfo['qformname'])) $qformname = $qinfo['qformname']; else $qformname = "";

	$category_filter_by = $qinfo['category_filter_by'];
	$category_filter_value = $qinfo['category_filter_value'];
	$category_filter_extra = (isset($qinfo['category_filter_extra']))?$qinfo['category_filter_extra']:"";
	$inventory_filter_by = $qinfo['inventory_filter_by'];
	$inventory_filter_value = $qinfo['inventory_filter_value'];

	$category_details_column = (isset($qinfo['category_details_column']))?$qinfo['category_details_column']:FALSE;
	$category_send_details = (isset($qinfo['category_send_details']))?$qinfo['category_send_details']:FALSE;

	$cinfo = array();
	$cinfo['details_column'] = $category_details_column;
	$cinfo['send_details'] = $category_send_details;
	$cinfo['category_primefield'] = $category_primefield;

	$send_details = (isset($qinfo['send_details']))?$qinfo['send_details']:FALSE;
	$datasource = (isset($qinfo['datasource']))?$qinfo['datasource']:"database";
	$datafields = (isset($qinfo['datafields']))?$qinfo['datafields']:array();
	$data = (isset($qinfo['data']))?$qinfo['data']:array();

	$category_orderby = (isset($qinfo['category_orderby']))?$qinfo['category_orderby']:$category_primefield;
	$inventory_orderby = (isset($qinfo['inventory_orderby']))?$qinfo['inventory_orderby']:$inventory_primefield;
	if($category_orderby=="_order") $category_allow_reorder = TRUE;
	if($inventory_orderby=="_order") $inventory_allow_reorder = TRUE;

	$qdbfields = get_qdbfields($qfields);
	$use_categories = $qtitle_category;

	$whole_cat_code_start = "<table cellspacing=0 style='border:solid 2px #777777'><tbody>";
	$whole_cat_code_start .= "<tr><td colspan='2' bgcolor='#bbbbbb'>(cat_row_code)</td></tr>";
	$whole_cat_code_start .= "<tr><td>";
	$whole_cat_code_start .= "<div id='ic_area_cat(cat_count)'>";
	$whole_cat_code_end = "</div>";
	$whole_cat_code_end .= "<div id='ic_cat(cat_count)_addbtn'>";
	$whole_cat_code_end .= wizard_button_new("../images/new_item_button_old.png",speak("Add ".$qtitle_item),"onclick='add_new_item((cat_count))'",122,30,"#006700");
	//$whole_cat_code_end .= "<a style='cursor:pointer; color:#0000aa' onclick='add_new_item((cat_count))'><img src='images/new_item_button.png' border='0' /></a>";
	$whole_cat_code_end .= "</div>";
	$whole_cat_code_end .= "</td></tr>";
	$whole_cat_code_end .= "</tbody></table><br>";

	$no_cat_code_start = "<div id='ic_area_cat1'>";
	$no_cat_code_end = "</div>";
	if ((!$language_pack_id) || ($language_pack_id == 1))
	{
		$no_cat_code_end .= wizard_button_new("../images/new_item_button_old.png",speak("Add ".$qtitle_item),"onclick='add_new_item(1)'",122,30,"#006700");
	}
	?>
	<script language="javascript">
		function click_delete(cat, itm, delinfo)
		{
			document.getElementById("icrow_" + cat + "_" + itm).style.display = "none";
			if(itm==0) // deleting a category
			{
				document.getElementById("icrow_delinfo_" + cat + "_" + itm).innerHTML = document.getElementById("ic_" + cat + "_category").value;
				document.getElementById("ic_area_cat" + cat).style.display = "none";
				document.getElementById("ic_cat" + cat + "_addbtn").style.display = "none";
			}
			else
			{
				setdval = "deleted";//document.getElementById("ic_" + cat + "_<?php echo $inventory_primefield?>_" + itm).value;
				document.getElementById("icrow_delinfo_" + cat + "_" + itm).innerHTML = setdval;
			}
			document.getElementById("icrow_del_" + cat + "_" + itm).style.display = "block";
			document.getElementById("ic_" + cat + "_delete_" + itm).value = 1;
		}

		function click_undelete(cat,itm)
		{
			document.getElementById("icrow_del_" + cat + "_" + itm).style.display = "none";
			document.getElementById("icrow_" + cat + "_" + itm).style.display = "block";
			document.getElementById("ic_" + cat + "_delete_" + itm).value = 0;
			if(itm==0) // undeleting a category
			{
				document.getElementById("ic_area_cat" + cat).style.display = "block";
				document.getElementById("ic_cat" + cat + "_addbtn").style.display = "block";
			}
		}

		function add_new_category()
		{
			cat_count++;
			item_count[cat_count] = 0;

			new_cat_code = "<?php jsvar($whole_cat_code_start . $whole_cat_code_end)?>";
			new_cat_code = new_cat_code.replace(/\(cat_row_code\)/ig,"<?php jsvar(cat_row_code($qtitle_category, $cfields, "", "", false, $cinfo))?>");
			new_cat_code = new_cat_code.replace(/\(cat_count\)/ig,cat_count);			
			new_cat_code = new_cat_code.replace(/\(cat\)/ig,cat_count);
			append_html_to_id("ic_area", new_cat_code);
			//document.getElementById("ic_area").innerHTML += new_cat_code;
		}

		function add_new_item(cat)
		{
			item_count[cat]++;

			new_item_code = "";
			//if(item_count[cat] > 1) new_item_code += "<br>";
			new_item_code += "<?php jsvar(item_row_code($qdbfields, $details_column, $send_details, $inventory_primefield, $field)); ?>";
			new_item_code = new_item_code.replace(/\(cat\)/ig,cat);
			new_item_code = new_item_code.replace(/\(item\)/ig,item_count[cat]);
			append_html_to_id("ic_area_cat" + cat, new_item_code);
		}

		function append_html_to_id(areaid, newhtml)
		{
			var t = document.createElement('div');
			t.innerHTML = newhtml;
			document.getElementById(areaid).appendChild(t);
			//document.getElementById(areaid).innerHTML += newhtml;
			return;
		}

		movefrom_cat = "";
		movefrom_item = "";
		function moveitem_mousedown(mcat,mitem)
		{
			movefrom_cat = mcat;
			movefrom_item = mitem;
			return false;
		}

		function moveitem_mouseup(mcat,mitem)
		{
			moveto_cat = mcat;
			moveto_item = mitem;

			if(movefrom_cat==moveto_cat)
			{
				var n_container = document.getElementById("icrow_" + movefrom_cat + "_" + movefrom_item).parentNode;
				var n_from = document.getElementById("icrow_" + movefrom_cat + "_" + movefrom_item);
				var n_to = document.getElementById("icrow_" + moveto_cat + "_" + moveto_item);

				if(n_to.offsetTop > n_from.offsetTop)
				{
					if(n_container.lastChild==n_to)
						n_container.appendChild(n_to);
					else
						n_container.insertBefore(n_from,n_to.nextSibling);
				}
				else
					n_container.insertBefore(n_from,n_to);

				var str = "";
				var setorder = 0;
				for(ic = 0; ic < n_container.childNodes.length; ic++)
				{
					var icnm = n_container.childNodes[ic];
					if(icnm.id.indexOf("del") > 1)
					{
					}
					else
					{
						str += icnm.id + "\n";
						if(document.getElementById(icnm.id + "_order")!=undefined)
						{
							setorder++;
							document.getElementById(icnm.id + "_order").value = setorder;
						}
					}
				}
				//alert(setorder + " / " + n_container.childNodes.length + "\n" + str);
			}

			//alert("movefrom: " + movefrom_cat + " - " + movefrom_item + "\nmoveto: " + moveto_cat + " - " + moveto_item);
		}

		function append_html_to_id_v1(areaid, newhtml)
		{				
			var id_vals = new Array();
			var tag_stack = new Array();
			var cmd = "";
			var props = new Array();
			var prop_value = "";
			var str = "";
			var debug = "";
			var in_tag = false;
			var tag_spaces = 0;
			var tag_level = 0;
			var in_symbol = "";
			var escape_next = false;
			var oldhtml = document.getElementById(areaid).innerHTML;

			for(var n=0; n<oldhtml.length; n++)
			{
				chr = oldhtml.charAt(n);
				if(chr=="'" || chr=="\"")
				{
					if(escape_next)
					{
						str += chr;
						escape_next = false;
					}
					else if(in_symbol==chr)
					{
						str += chr;
						in_symbol = "";
					}
					else if(in_symbol=="")
					{
						str += chr;
						in_symbol = chr;
						prop_value = "";
					}
					else
					{
						prop_value += chr;
					}
				}
				else if(escape_next)
				{
					str += "\\" + chr;
				}
				else if(chr=="\\")
				{
					escape_next = true;
				}
				else
				{
					if(in_symbol=="")
					{
						if(in_tag)
						{
							if(chr==">")
							{
								if(tag_spaces==0)
								{
									cmd = str;
								}
								else if(tag_spaces > 0)
								{
									propparts = str.split("=");
									propname = propparts[0];
									if(propparts.length > 1)
									{
										if(propparts[1].indexOf("'") > -1 || propparts[1].indexOf('"') > -1)
										{
										}
										else
										{
											prop_value = propparts[1];
										}
										props[props.length] = new Array(propname,prop_value);
									}
								}
								if(cmd!="")
								{
									if(cmd.charAt(0)!="/")
									{
										// Opening Tag
										var elem = document.createElement(cmd);
										var elem_id = "";
										for(m=0; m<props.length; m++)
										{
											if(props[m][0]=="id")
											{
												elem_id = props[m][1];
											}
											//elem.setAttribute(props[m][0],props[m][1]);
										}
										if(elem_id!="")
										{
											id_vals.push(new Array(elem_id, ""));
										}
										tag_stack.pop(new Array(cmd, elem));
										if(tag_stack.length > 1)
										{
											var parent_elem = tag_stack[tag_stack.length - 2];
											//parent_elem.appendChild(elem);
										}
										else
										{
											var parent_elem = document.getElementById(areaid);
											//parent_elem.appendChild(elem);
										}
										tag_level++;
									}
									show_spaces = "";
									for(m=0; m<tag_level; m++)
										show_spaces += "    ";
									debug += show_spaces + cmd;
									for(m=0; m<props.length; m++)
									{
										debug += " " + props[m][0] + "=" + props[m][1];
									}
									debug += "\n";

									if(cmd=="input" || cmd.charAt(0)=="/")
									{
										// Closing Tag
										tag_stack.pop();
										tag_level--;
									}
								}
								str = "";
								in_tag = false;
							}
							else if(chr==" ")
							{
								if(tag_spaces==0)
								{
									cmd = str;
								}
								else if(tag_spaces > 0)
								{
									propparts = str.split("=");
									propname = propparts[0];
									if(propparts.length > 1)
									{
										if(propparts[1].indexOf("'") > -1 || propparts[1].indexOf('"') > -1)
										{
										}
										else
										{
											prop_value = propparts[1];
										}
										props[props.length] = new Array(propname,prop_value);
									}
								}
								tag_spaces++;
								str = "";
							}
							else
							{
								str += chr;
							}
						}
						else if(chr=="<")
						{
							if(str!="")
							{
								// Text node
								var elem = document.createTextNode(str);
								if(tag_stack.length > 1)
								{
									var parent_elem = tag_stack[tag_stack.length - 2];
									//parent_elem.appendChild(elem);
								}
								else
								{
									var parent_elem = document.getElementById(areaid);
									//parent_elem.appendChild(elem);
								}

								debug += str + "\n";
							}
							str = "";
							cmd = "";
							props = new Array();
							tag_spaces = 0;
							in_tag = true;
						}
						else
						{
							str += chr;
						}
					}
					else
					{
						prop_value += chr;
					}
				}
			}

			for(i=0; i<id_vals.length; i++)
			{
				id_set = id_vals[i][0];
				id_val = document.getElementById(id_set).value;
				id_vals[i][1] = id_val;
				//alert(id_set + " - ");
			}
			document.getElementById(areaid).innerHTML += newhtml;
			for(i=0; i<id_vals.length; i++)
			{
				id_set = id_vals[i][0];
				id_val = id_vals[i][1];
				document.getElementById(id_set).value = id_val;
			}

			return;
			//alert(newhtml);
			//alert(debug);
			//document.body.appendChild(tag_stack[0][1]);
			//document.getElementById(areaid).innerHTML += newhtml;
		}

		function open_dialog_box(dwidth, dheight, src, postvars)
		{
			if(postvars)
			{
				str = "<iframe id='dframe' name='dframe' width='" + dwidth + "' height='" + dheight + "' allowtransparency='true' frameborder='0' src=''></iframe>";
				str += "<form name='dialog_post' method='post' action='" + src + "' target='dframe'>";
				postvars = postvars.split("&");
				for(v=0; v<postvars.length; v++)
				{
					pv = postvars[v].split("=");
					if(pv.length>1)
					{
						pv_name = pv[0];
						pv_value = pv[1];
						str += "<br><input type='hidden' name='" + pv_name + "' value='" + pv_value + "'>";
					}
				}
				str += "</form>";
				show_info(str);
				document.dialog_post.submit();
			}
			else
				show_info("<iframe id='dframe' width='" + dwidth + "' height='" + dheight + "' allowtransparency='true' frameborder='0' src='" + src + "'></iframe>");
		}

		function open_picture_editor(setdir, dwidth, dheight, pictureid, filename, picture2, picture3, misc_content)
		{
			durl = "resources/form_dialog.php?mode=set_inventory_picture&dir=" + setdir + "&updateid=" + pictureid + "&main_picture=" + filename;
			if(picture2) durl += "&picture_2=" + picture2;
			if(picture3) durl += "&picture_3=" + picture3;
			if(misc_content) durl += "&misc_content=" + misc_content;
			open_dialog_box(dwidth, dheight, durl);
		}

		function open_special_editor(dwidth, dheight, item_name, item_value, item_title, row_name, extra1, extra2)
		{
			open_dialog_box(dwidth, dheight, "resources/form_dialog.php?mode=set_special&item_value=" + item_value + "&item_name=" + item_name + "&item_title=" + item_title + "&rowname=" + row_name + "&extra1=" + extra1 + "&extra2=" + extra2);
		}

		function set_inv_picture(pictureid, filename, displayfile, picture2, picture3, misc_content)
		{
			var imgcode = "<img src='" + displayfile + "' width='16' height='16' />";
			document.getElementById(pictureid).value = filename;
			document.getElementById(pictureid + "_display").innerHTML = imgcode;
			if(picture2)
			{
				pictureid_2 = pictureid.replace(/image/ig,"image2");
				document.getElementById(pictureid_2).value = picture2;
			}
			if(picture3)
			{
				pictureid_3 = pictureid.replace(/image/ig,"image3");
				document.getElementById(pictureid_3).value = picture3;
			}
			if(misc_content)
			{
				pictureid_misc = pictureid.replace(/image/ig,"misc_content");
				document.getElementById(pictureid_misc).value = misc_content;
			}
			info_visibility();
		}

		function set_special_extra_value(set_itemid, set_itemvalue)
		{
			document.getElementById(set_itemid).value = set_itemvalue;
		}

		function set_special_value(set_itemid, show_itemvalue, set_itemvalue)
		{
			document.getElementById(set_itemid).value = set_itemvalue;

			show_itemid = set_itemid + "_display";

			if(show_itemvalue=="") set_itemcolor = "#aaaaaa";
			else set_itemcolor = "#1c591c";

			document.getElementById(show_itemid).value = show_itemvalue;
			document.getElementById(show_itemid).style.color = set_itemcolor;
			info_visibility();
		}
	</script>
	<?php
	$field_code = "<div id='ic_area'>";
	$cat_count = 0;
	$item_count_list = "0";

	if($use_categories)
	{
		$category_filter_code = "";
		if($category_filter_by!="")
		{
			$cfval = array();
			if(is_array($category_filter_value))
			{
				$cfval = $category_filter_value;
			}
			else
			{
				$cfval[] = $category_filter_value;
			}
			$category_filter_code = " and ";
			if(count($cfval) > 1) $category_filter_code .= "(";
			for($x=0; $x<count($cfval); $x++)
			{
				if($x > 0) $category_filter_code .= " or ";
				$category_filter_code .= "`$category_filter_by`='".str_replace("'","''",$cfval[$x])."'";
			}
			if(count($cfval) > 1) $category_filter_code .= ")";
			if($category_filter_extra!="")
			{
				$category_filter_code .= " and " . $category_filter_extra;
			}
		}
		$inventory_filter_code = "";
		if($inventory_filter_by!="")
		{
			$inventory_filter_code = " and `$inventory_filter_by`='".str_replace("'","''",$inventory_filter_value)."'";
		}
		//echo "select * from $category_tablename where _deleted!=1".$category_filter_code." order by `$category_primefield` asc";
		$category_query = mlavu_query("select * from `poslavu_MAIN_db`.`$category_tablename` where `_deleted`!='1'".$category_filter_code." order by `$category_orderby` asc");
		while($category_read = mysqli_fetch_assoc($category_query))
		{
			$cat_count++;
			$setname = $category_read[$category_primefield];
			$setid = $category_read['id'];
			$matchfield = $category_read[$category_fieldname];

			$ccode = $whole_cat_code_start . "(inventory)" . $whole_cat_code_end;
			$ccode = str_replace("(cat_row_code)",cat_row_code($qtitle_category,$cfields,$setname,$setid,$category_read,$cinfo), $ccode);
			$ccode = str_replace("(cat_count)",$cat_count, $ccode);
			$ccode = str_replace("(cat)",$cat_count, $ccode);

			$inv_count = 0;
			$icode = "";
			if($datasource=="comma delimited" || substr(strtolower($datasource),0,12)=="delimited by")
			{
				if(substr(strtolower($datasource),0,12)=="delimited by")
				{
					$delimiter = trim(substr($datasource,12));
				}
				else
				{
					$delimiter = ",";
				}
				$parse_rows = explode($delimiter,$category_read[$category_fieldname]);
				$data = array();
				for($i=0; $i<count($parse_rows); $i++)
				{
					$inv_count++;

					$send_vals = array();
					$send_vals[$inventory_primefield] = $parse_rows[$i];
					//echo "<br>" . $parse_rows[$i];
					$item_code = item_row_code($qdbfields, $details_column, $send_details, $inventory_primefield, $field, $send_vals);
					$item_code = str_replace("(cat)",$cat_count, $item_code);
					$item_code = str_replace("(item)",$inv_count, $item_code);
					$icode .= $item_code;
				}
			}
			else
			{
				$qvars = array();
				$qvars['inventory_fieldname'] = $inventory_fieldname;
				$qvars['inventory_primefield'] = $inventory_primefield;
				$qvars['inventory_orderby'] = $inventory_orderby;
				$qvars['matchfield'] = $matchfield;
				$inventory_query = mlavu_query("select * from `poslavu_MAIN_db`.`$inventory_tablename` where `_deleted`!=1".$inventory_filter_code." and `[inventory_fieldname]`='[matchfield]'".$inventory_select_condition." order by `[inventory_orderby]` asc", $qvars);
				while($inventory_read = mysqli_fetch_assoc($inventory_query))
				{
					$inv_count++;

					$item_code = item_row_code($qdbfields, $details_column, $send_details, $inventory_primefield, $field, $inventory_read, $inventory_allow_reorder);
					$item_code = str_replace("(cat)",$cat_count, $item_code);
					$item_code = str_replace("(item)",$inv_count, $item_code);
					$icode .= $item_code;
				}
			}


			$ccode = str_replace("(inventory)",$icode,$ccode);
			if($item_count_list!="") $item_count_list .= ",";
			$item_count_list .= $inv_count;
			$field_code .= $ccode;
			$field_code .= "&nbsp;<input type='submit' value='Save' style='width:120px'><br><br><br>";

		}
	}
	else
	{
		if($datasource=="database")
		{
			$data = array();
			$inventory_query = mlavu_query("select * from `poslavu_MAIN_db`.`$inventory_tablename` where `_deleted`!=1".$inventory_select_condition." order by `[1]` asc", $inventory_primefield);
			while($inventory_read = mysqli_fetch_assoc($inventory_query))
			{
				$data[] = $inventory_read;
			}
		}
		else if($datasource=="comma delimited" || substr(strtolower($datasource),0,12)=="delimited by")
		{
			if(substr(strtolower($datasource),0,12)=="delimited by")
			{
				$delimiter = trim(substr($datasource,12));
			}
			else
			{
				$delimiter = ",";
			}
			$parse_rows = explode($delimiter,$data);
			$data = array();
			for($i=0; $i<count($parse_rows); $i++)
			{
				$datarow = array();
				$row_vals = array();
				$parse_row = $parse_rows[$i];
				$parse_cols = explode("(",$parse_row);
				for($c=0; $c<count($parse_cols); $c++)
				{
					$parse_col = $parse_cols[$c];
					$parse_col = explode(")",$parse_col);
					$parse_col = $parse_col[0];
					$row_vals[$c] = $parse_col;
				}
				for($x=0; $x<count($datafields); $x++)
				{
					$datafield = $datafields[$x];
					$datavalue = (isset($row_vals[$x]))?$row_vals[$x]:FALSE;
					$datarow[$datafield] = trim($datavalue);
				}
				$datarow['id'] = ($i + 1);
				$data[] = $datarow;
			}
		}

		$inv_count = 0;
		$icode = "";
		for($i=0; $i<count($data); $i++)
		{
			$inventory_read = $data[$i];
			$inv_count++;

			$item_code = item_row_code($qdbfields, $details_column, $send_details, $inventory_primefield, $field, $inventory_read, $inventory_allow_reorder);
			$item_code = str_replace("(cat)",1, $item_code);
			$item_code = str_replace("(item)",$inv_count, $item_code);
			$item_code = str_replace("(item_id)", $data[$i]['id'], $item_code);
			$icode .= $item_code;
		}
		$field_code .= $no_cat_code_start;
		$field_code .= $icode;
		$field_code .= $no_cat_code_end;
		$item_count_list = $inv_count . "," . $inv_count;
	}
	$field_code .= "</div>";
	if($use_categories)
		$field_code .= wizard_button_new("../images/new_category_button.png",speak("Add ".$qtitle_category),"onclick='add_new_category()'",144,30,"#000067");
	//$field_code .= "<a style='cursor:pointer; color:#0000aa' onclick='add_new_category()'><img src='images/new_category_button.png' border='0' /></a>";

	echo "<script language='javascript'>";
	echo "cat_count = ".$cat_count."; ";
	echo "item_count = new Array($item_count_list); ";
	echo "</script>";

	return $field_code;
	//$field_code .= "<input type='hidden' name='(name)' value='(value)'>";
	//$validate_code .= "if(document.sform.$field->name.value=='') alert('Category count is ' + cat_count); else ";
}

function delete_poslavu_dbrow($tablename, $id)
{
	if($id!="")
	{
		$query = "update `poslavu_MAIN_db`.`$tablename` set `_deleted`=1 where `id=[1]";
		mlavu_query($query, $id);
	}
}

function update_poslavu_dbrow($tablename, $fields, $id)
{
	global $language_pack_id;

	$insert_cols = "";
	$insert_vals = "";
	$update_code = "";

	for($i = 0; $i < count($fields); $i++)
	{
		$col = $fields[$i][0];
		$val = str_replace("'","''",$fields[$i][1]);
		if ($insert_cols != "") $insert_cols .= ", ";
		if ($insert_vals != "") $insert_vals .= ", ";
		if ($update_code != "") $update_code .= ", ";
		$insert_cols .= "`".$col."`";
		$insert_vals .= "'".$val."'";
		$update_code .= "`".$col."`='".$val."'";
		if ($col == 'word_or_phrase')
		{
			$word_or_phrase = $fields[$i][1];
		}
		if ($col == 'used_by')
		{
			$used_by = $fields[$i][1];	
		}
	}

	if ($id != "")
	{
		$query = "UPDATE `poslavu_MAIN_db`.`".$tablename."` set ".$update_code." WHERE `id` = '[1]'";

		mlavu_query($query, $id);
		$rows_affected = ConnectionHub::getConn('poslavu')->affectedRows();
		if ($rows_affected)
		{
			$message= "Hello, A Language pack was updated. The query was: ". $query. " The id was: $id. This query was run by: ".$_SESSION['posadmin_loggedin']. " The time was: ". date("Y-m-d H:i:s");

			if ($_SESSION['posadmin_access']=='language_pack')
			{
				mail("nate@poslavu.com", $subject, $message);
				mail("tiffany@poslavu.com", $subject, $message);
				mail("niallsc@poslavu.com", $subject, $message);
			}
		}

		$newid = $id;			

		if ($language_pack_id == 1)
		{
			$update_english = mlavu_query("UPDATE `poslavu_MAIN_db`.`language_dictionary` SET `word_or_phrase` = '[1]', `used_by` = '[2]' WHERE `english_id` = '[3]'", $word_or_phrase, $used_by, $id);
		}
	}
	else
	{
		$ok_to_add = TRUE;
		if ($language_pack_id == 1)
		{
			$check_for_repeat = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `word_or_phrase` COLLATE latin1_general_cs = '[1]'", $word_or_phrase);
			if (mysqli_num_rows($check_for_repeat) > 0)
			{
				$ok_to_add = FALSE;
			}
		}

		if ($ok_to_add)
		{
			$query = "INSERT INTO `poslavu_MAIN_db`.`".$tablename."` (".$insert_cols.") VALUES (".$insert_vals.")";
			mlavu_query($query);
			if (ConnectionHub::getConn('poslavu')->affectedRows())
			{
				$subject= 'New Language Pack Entry';
				$message= "Hello, A Translation was added. The query was: ". $query. " This query was run by: ".$_SESSION['posadmin_loggedin']. " The time was: ". date("Y-m-d H:i:s");
				if ($_SESSION['posadmin_access']=='language_pack')
				{
					mail("nate@poslavu.com", $subject, $message);
					mail("tiffany@poslavu.com", $subject, $message);
					mail("niallsc@poslavu.com", $subject, $message);
				}
			}

			$newid = mlavu_insert_id();

			if ($language_pack_id == 1)
			{
				$get_packs = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`language_packs` WHERE `id` != '1'");
				while ($pack = mysqli_fetch_assoc($get_packs))
				{
					$check_translation = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `word_or_phrase` COLLATE latin1_general_cs = '[1]' AND `pack_id`= '[2]'", $word_or_phrase, $pack['id']);
					if (@mysqli_num_rows($check_translation) > 0)
					{
						$translation = mysqli_fetch_assoc($check_translation);
						$set_english_id = mlavu_query("UPDATE `poslavu_MAIN_db`.`language_dictionary` SET `english_id` = '[1]', `used_by` = '[2]' WHERE `id` = '[3]'", $newid, $used_by, $translation['id']);
					}
					else
					{ 
						$add_translation = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`language_dictionary` (`pack_id`, `word_or_phrase`, `english_id`, `used_by`) VALUES ('[1]', '[2]', '[3]', '[4]')", $pack['id'], $word_or_phrase, $newid, $used_by);
					}
				}
			}
			else if ($language_pack_id == FALSE)
			{
				$get_words_and_phrases = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`language_dictionary` WHERE `pack_id` = '1'");
				while ($extract = mysqli_fetch_assoc($get_words_and_phrases))
				{
					$extract['new_pack_id'] = $newid;
					mlavu_query("INSERT INTO `poslavu_MAIN_db`.`language_dictionary` (`pack_id`, `word_or_phrase`, `english_id`, `used_by`) VALUES ('[new_pack_id]', '[word_or_phrase]', '[id]', '[used_by]')", $extract);
				}
			}

		}
	}

	return $newid;
}
?>
<script type="text/javascript" language="javascript">

function uploadLanguagePack() {
	var file = document.getElementById('file').value;
	if (file != '') {
		var languageName = "<?=$languageName?>";
		var confrm = confirm('Are you sure to upload translation file for language pack "' + languageName + '" ?');
		if (confrm == true) {
			document.getElementById("upload_language_form").submit();
			document.getElementById
		}
		return true;
	}
	alert ("Please select valid csv or json file.");
}

</script>