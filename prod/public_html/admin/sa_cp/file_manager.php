<?php
	require_once("/home/poslavu/private_html/rs_upload.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php"); // needed for lavuExec
	
	$dir = $_GET['dir'];
	//echo $dir;
	$reldir = str_replace("/home/poslavu/public_html/","http://www.poslavu.com/",$dir);
	//$reldir = str_replace($_SERVER['DOCUMENT_ROOT'],"",$dir);
	$mode = (isset($_GET['mode']))?$_GET['mode']:"files";
	$callback_function = (isset($_GET['callback_function']))?$_GET['callback_function']:"";
	$setwidth = (isset($_GET['setwidth']))?$_GET['setwidth']:"";
	$setheight = (isset($_GET['setheight']))?$_GET['setheight']:"";
	$itemfocus = (isset($_GET['itemfocus']))?$_GET['itemfocus']:"";
	//$convert = "/usr/local/bin/convert";
	$convert = "convert";
	
	$rootdir = (isset($_GET['rootdir']))?$_GET['rootdir']:$dir;
?>

<style>
	.dir_display {
		font-family:Arial, Helvetica, sans-serif;
		font-size:10px;
	}
	a:link, a:visited {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:12px;
		color:#000066;
		text-decoration:none;
	}
	a:hover {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size:12px;
		color:#0000CC;
		text-decoration:none;
	}
</style>

<body style="background:transparent">
	<?php
		if($mode=="edit_css_file")
		{			
			$edit_file = (isset($_GET['edit_file']))?$_GET['edit_file']:"";
			$filename = $dir . "/" . $edit_file;
			$file_ext = pathinfo($filename);
			$file_ext = $file_ext['extension'];
			if($file_ext=="css"||$file_ext=="txt")
			{
				if(isset($_POST['file_content']))
				{
					$fp = fopen($filename,"w");
					fwrite($fp,$_POST['file_content']);
					fclose($fp);
					$mode = "files";
				}
				else
				{
					echo "<form method='post' action='' name='edit_file'>";			
					echo "<a href='file_manager.php?mode=files&dir=$dir&rootdir=$rootdir'><< Back to File List</a><br>";
					$fp = fopen($filename,"r");
					$str = fread($fp,filesize($filename));
					fclose($fp);
					
					echo "<textarea rows='20' cols='110' name='file_content'>$str</textarea>";
					echo "<input type='button' value='Save' onclick='document.edit_file.submit();' />";
					echo "</form>";
				}
			}
			
			echo "</body>";
			if($mode=="edit_css_file")
				exit();
		}
	?>
    
	<?php if($mode=="files" || $mode=="pictures")
		{ ?>
    <form name="upload" method="post" enctype="multipart/form-data" action="file_manager.php?dir=<?php echo $dir?>&mode=<?php echo $mode?>&rootdir=<?php echo $rootdir?>">
        <input type="file" name="picture" onChange="document.upload.submit()"/>    	
    </form>
	<?php
		}
		else if($mode=="upload_screenshot")
		{
			if(isset($_POST['designpath']) || isset($_GET['designpath']))
			{
				if(isset($_POST['designpath']))
				{
					$designname = $_POST['designname'];
					$designpath = $_POST['designpath'];
					$designurl = $_POST['designurl'];
					$designpath_full = $_POST['designpath_full'];
					$designpath_thumb = $_POST['designpath_thumb'];
				}
				else if(isset($_GET['designpath']))
				{
					$designname = $_GET['designname'];
					$designpath = $_GET['designpath'];
					$designurl = $_GET['designurl'];
					$designpath_full = $_GET['designpath_full'];
					$designpath_thumb = $_GET['designpath_thumb'];
				}
				
				if(isset($_FILES['picture']))
				{
					$basename = basename($_FILES['picture']['name']);
					
					$option = (isset($_POST['option']))?$_POST['option']:"";
					if($option=="(option)") $option = "";
					if($option=="")
					{
						$option_list = explode(",",$_POST['option_list']);
						for($i=0; $i<count($option_list); $i++)
						{
							$opt = trim($option_list[$i]);
							if($opt!="" && strpos($basename,"_".$opt))
								$option = $opt;
						}
					}
					
					if($option!="")
					{
						$designpath = str_replace(".jpg","_".$option.".jpg",$designpath);
						$designurl = str_replace(".jpg","_".$option.".jpg",$designurl);
						$designpath_full = str_replace(".jpg","_".$option.".jpg",$designpath_full);
						$designpath_thumb = str_replace(".jpg","_".$option.".jpg",$designpath_thumb);
					}
				
					if(rs_move_uploaded_file($_FILES['picture']['tmp_name'], $designpath_full))
					{
						chmod($designpath_full,0775);
						$convertcmd = "convert ".lavuShellEscapeArg($designpath_full)." -thumbnail '400x300>' ".lavuShellEscapeArg($designpath);
						lavuExec($convertcmd);
						chmod($designpath,0775);
						$convertcmd = "convert ".lavuShellEscapeArg($designpath)." -thumbnail '200x150>' ".lavuShellEscapeArg($designpath_thumb);
						lavuExec($convertcmd);
						chmod($designpath_thumb,0775);
					}
				}
				$thumb_script = "";
				$thumb_script .= "sthumb_enlarged = new Array(); ";
				$thumb_script .= "sthumb_pic_small = new Array(); ";
				$thumb_script .= "sthumb_pic_regular = new Array(); ";
				
				$dfile_list = array();
				$thumb_list = "<table>";
				$dp = opendir(str_replace(basename($designpath),"",$designpath_thumb));
				while($dread = readdir($dp))
				{
					if(strpos($dread,".jpg") && strpos($dread,$designname)!==false)
					{
						$dfile_list[] = $dread;
					}
				}
				sort($dfile_list);
				for($i=0; $i<count($dfile_list); $i++)
				{
					$dread = $dfile_list[$i];
					$ssid = $i + 1;
					$fname_split = explode("_",$dread,3);
					if(count($fname_split)>2)
					{
						$dread_output = str_replace(".jpg","",$fname_split[2]);
					}
					else $dread_output = "(no option)";
					$turl = str_replace(basename($designpath),"",$designurl)."thumb/";
					$thumb_url = $turl.$dread."?abc=".rand(10000,99999);
					$regular_url = str_replace("thumb/","",$thumb_url);
					$thumb_list .= "<td align='center' valign='top' style='cursor:pointer' onclick='click_screenshot($ssid)'>";
					$thumb_list .= "$dread_output<br>";
					$thumb_list .= "<img src=\"$thumb_url\" border=\"0\" id='screenthumb_$ssid' name='screenthumb_$ssid' />";
					$thumb_list .= "</td>";
					
					$thumb_script .= "sthumb_enlarged[$ssid] = false; ";
					$thumb_script .= "sthumb_pic_small[$ssid] = '$thumb_url'; ";
					$thumb_script .= "sthumb_pic_regular[$ssid] = '$regular_url'; ";
				}
				
				$thumb_list .= "</table>";
				$thumb_list .= "<script language='javascript'>";
				$thumb_list .= $thumb_script;
				$thumb_list .= "function click_screenshot(sid) { ";
				$thumb_list .= "  if(sthumb_enlarged[sid]) { ";
				$thumb_list .= "    document.getElementById('screenthumb_' + sid).src = sthumb_pic_small[sid]; ";
				$thumb_list .= "    sthumb_enlarged[sid] = false; ";
				$thumb_list .= "  } ";
				$thumb_list .= "  else { ";
				$thumb_list .= "    document.getElementById('screenthumb_' + sid).src = sthumb_pic_regular[sid]; ";
				$thumb_list .= "    sthumb_enlarged[sid] = true; ";
				$thumb_list .= "  } ";
				$thumb_list .= "} ";
				$thumb_list .= "</script>";
				echo $thumb_list;
			}
			echo "</body>";
			exit();
		}
		else if($mode=="process_picture")
		{
			$picture = $_GET['picture'];
			echo "Picture: $picture";
			echo "<br>Item focus: $itemfocus";
			echo "<br>Width: $setwidth";
			echo "<br>Height: $setheight";

			$basename = basename($picture);
			$basename = str_replace("&","_and_",$basename);
			$basename = str_replace(" ","_",$basename);
			$basename = str_replace("%20","_",$basename);
			
			if(substr($itemfocus,strlen($itemfocus)-1,1)!="/")
				$itemfocus .= "/";
			$fullfile = $itemfocus . "fullsize/" . $basename;
			$thumbfile = $itemfocus . "fullsize/thumbs/" . $basename;
			$itemfile = $itemfocus . $basename;
			
			copy($picture, $fullfile);
			
			$convertcmd = "convert ".lavuShellEscapeArg($picture)." -thumbnail '60x60>' ".lavuShellEscapeArg($thumbfile);
			lavuExec($convertcmd);
			
			$convertcmd = "convert ".lavuShellEscapeArg($picture)." -resize ".lavuShellEscapeArg($setwidth."x".$setheight)."> ".lavuShellEscapeArg($itemfile);
			lavuExec($convertcmd);
			
			echo "<script language='javascript'>";
			echo "window.parent.".$callback_function."('".dir_relative($itemfile)."','".dir_relative($picture)."'); ";
			echo "</script>";
		}
		
		if(isset($_POST['new_folder']) && $_POST['new_folder']!="thumbs")
		{
			$newdir = $dir . "/" . $_POST['new_folder'];
			if(!is_dir($newdir))
				mkdir($newdir,0775);	
		}
		
		if(!is_dir($dir))
		{
			mkdir($dir, 0775);
		}
		
		if(isset($_FILES['picture']))
		{
			$basename = basename($_FILES['picture']['name']);
			$uploadfile = $dir . "/" . $basename;
			if(rs_move_uploaded_file($_FILES['picture']['tmp_name'], $uploadfile))
			{
				chmod($uploadfile,0775);
				if($mode=="pictures")
				{
					$thumbdir = $dir . "/thumbs";
					if(!is_dir($thumbdir))
					{
						mkdir($thumbdir, 0775);
					}
					$thumbfile = $thumbdir . "/" . $basename;
					$convertcmd = "convert ".lavuShellEscapeArg($uploadfile)." -thumbnail '60x60>' ".lavuShellEscapeArg($thumbfile);
					lavuExec($convertcmd);
				}
			}
		}
				
		if(isset($_GET['remove']))
		{
			$remdir = $dir . "/" . $_GET['remove'];
			if(is_dir($remdir))
			{
				$del_count = 0;
				$dh  = opendir($remdir);
				while (false !== ($filename = readdir($dh))) {
					if($filename!="." && $filename!=".." && $filename!="thumbs") { //  && strpos($filename,".")!=false
						$del_count++;
					}
				}
				if($del_count < 1) {
					if(is_dir($remdir . "/thumbs"))
						rmdir($remdir . "/thumbs");
					rmdir($remdir);
				}
				else
					echo "Cannot delete: This directory contains files<br><br>";
			}
			else
			{
				if($mode=="pictures")
				{
					$thumb_file = thumb_dir($dir, "full") . "/".$_GET['remove'];
					unlink($thumb_file);
				}
				if(is_file($dir . "/" . $_GET['remove']));
					unlink($dir . "/" . $_GET['remove']);
			}
		}
		
		$files = array();
		$dh  = opendir($dir);
		while (false !== ($filename = readdir($dh))) {
			if($filename!="." && $filename!=".." && $filename!="thumbs") //  && strpos($filename,".")!=false
				$files[] = $filename;
		}
		sort($files);
		
		if($dir!=$rootdir)
		{
			echo "<table>";
			echo "<td valign='top'>";
			echo "<b>" . substr($dir, (strrpos($dir, "/") + 1)) . "</b>";
			echo "</td>";
			echo "<td width='60'>&nbsp;</td>";
			echo "<td valign='top'>";
			echo "<a href='file_manager.php?dir=$rootdir&mode=$mode'>(back to main picture library)</a><br><br>";
			echo "</td>";
			echo "</table>";
		}
		
		$col = 1;
		$maxcol = 8;
		echo "<table><tr>";
		for($i=0; $i<count($files); $i++)
		{			
			if($mode=="pictures" || $mode=="choose_picture")
			{
				echo "<td>";
				
				$count = $i;
				$thumb_file = thumb_dir($dir) . "/" .$files[$i];
				$full_file = $dir . "/" . $files[$i];

				if(is_dir($full_file))
					$item_display = "<table cellspacing=0 cellpadding=0><td align='center' valign='middle' width='60' height='60' (click) class='dir_display'>".substr($full_file, (strrpos($full_file,"/") + 1))."</td></table>";
				else
					$item_display = "<img src='$thumb_file' border='0' (click) />";
				
				if($mode=="pictures")
				{	
					echo "<table cellspacing=0 cellpadding=0><td width='60' height='60' bgcolor='#ffffff' style='border:solid 1px #888888' align='left' valign='top' onmouseover='document.getElementById(\"del_$count\").style.visibility = \"visible\"' onmouseout='document.getElementById(\"del_$count\").style.visibility = \"hidden\"'>";
					echo "<div style='position:relative'>";
					echo "<table width='100%' height='100%' cellspacing=0 style='table-layout:fixed'><td width='60' height='60' align='center' valign='middle'>";
					$click_code = "";
					if(is_dir($full_file))
						$click_code = "onclick='window.location = \"file_manager.php?dir=$full_file&mode=$mode&rootdir=$rootdir\"' style='cursor:pointer'";
					echo str_replace("(click)",$click_code,$item_display);
					echo "</td></table>";
					//------------------------ delete button -----------------------------//
					echo "<div style='position:absolute; left:48px; top:0px; visibility:hidden' id='del_$count'>";
					echo "<table><td style='border:solid 1px black; font-size:10px; color:#ffffff; cursor:pointer' bgcolor='#aa0000' onclick='window.location = \"file_manager.php?dir=$dir&mode=$mode&rootdir=$rootdir&remove=".$files[$i]."\"'>x</td></table>";
					echo "</div>";
					
					echo "</div>";
					echo "</td></table>";
				}
				else if($mode=="choose_picture")
				{
					echo "<table cellspacing=0 style='table-layout:fixed; border:solid 1px #888888' bgcolor='#ffffff'><td width='60' height='60' align='center' valign='middle'>";
					$code = "style='cursor:pointer' onclick='";
					if(is_dir($full_file))
						$code .= "window.location = \"file_manager.php?dir=$full_file&mode=$mode&rootdir=$rootdir&setwidth=$setwidth&setheight=$setheight&itemfocus=$itemfocus&callback_function=$callback_function\"";
					else
						$code .= "window.location = \"file_manager.php?callback_function=$callback_function&mode=process_picture&dir=$dir&picture=$full_file&itemfocus=$itemfocus&setwidth=$setwidth&setheight=$setheight&rootdir=$rootdir\"";
					$code .= "'";
					echo str_replace("(click)",$code,$item_display);
					//if($callback_function!="")
					//	echo "onclick='window.parent.".$callback_function."(\"".dir_relative($full_file)."\")'";
					echo "</td></table>";
				}
				
				echo "</td>";
				$col++;
				if($col > $maxcol)
				{
					echo "</tr><tr>";
					$col = 1;
				}
			}
			else
			{
				$full_file = $dir . "/" . $files[$i];
				if(is_dir($full_file))
				{
					$click_path = "file_manager.php?dir=$full_file&rootdir=$rootdir&mode=files";
					$click_target = "";
					$use_icon = "images/icon_folder.jpg";
				}
				else
				{
					$click_path = $reldir."/".$files[$i];
					$click_target = "_blank";
					$img_types = array("jpg","bmp","gif","png","jpe","jpeg","tif","tiff");
					$use_icon = false;
					for($n=0; $n<count($img_types); $n++)
					{
						if(strpos(strtolower($files[$i]),strtolower($img_types[$n]))!==false)
						{
							$use_icon = "images/icon_picture.jpg";
						}
					}
					if(!$use_icon)
						$use_icon = "images/icon_blank.jpg";
				}
				
				echo "<td><a href='$click_path' target='$click_target'><img src='$use_icon' border='0' /></a></td>";
				echo "<td>";
				echo "<a href='$click_path' target='$click_target'>" . $files[$i] . "</a>";
				echo "</td><td width='20'>&nbsp;</td><td><a href='file_manager.php?dir=$dir&mode=$mode&remove=".$files[$i]."&rootdir=$rootdir'><font color='red'><b>x</b></font></a></td>";
				$fsplit = explode(".",$files[$i]);
				if(count($fsplit)>1)
				{
					$fext = $fsplit[count($fsplit)-1];
					if($fext=="css")
					{
						echo "<td width='20'>&nbsp;</td>";
						echo "<td><a href='file_manager.php?mode=edit_css_file&edit_file=".$files[$i]."&dir=$dir&rootdir=$rootdir'>(edit)</a></td>";
					}
				}
				echo "</tr><tr>";
			}
		}
		echo "</tr></table>";
		
		?>
        <script language="javascript">
			function validate_new_folder() {
				if(document.addnew.new_folder.value=="") {
					alert("Please type in the name of the folder you would like to create");
					return false;
				}
				else {
					return true;
				}
			}
		</script>
        <?php
		
		//if($mode=="pictures")
		//{
			echo "<form name='addnew' method='post' onsubmit='return validate_new_folder()' action='file_manager.php?dir=$dir&rootdir=$rootdir&mode=$mode'>";
			echo "<table>";
			echo "<td><input type='text' name='new_folder'></td>";
			echo "<td><input type='submit' value='Add New Folder'></td>";
			echo "</table>";
			echo "</form>";
		//}
				
		function dir_relative($dir) {
			return str_replace($_SERVER['DOCUMENT_ROOT'],"",$dir);
		}
		
		function thumb_dir($dir,$type="relative") {
			$reldir = dir_relative($dir);
			if($type=="relative")
				return $reldir . "/thumbs"; 
			else return $_SERVER['DOCUMENT_ROOT'] . $reldir . "/thumbs";
		}
    ?>
</body>
