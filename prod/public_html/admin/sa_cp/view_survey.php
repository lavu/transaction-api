<form name="which_survey_form" id="which_survey_form" method="get" action="index.php">

<!--
<div class="leftForm"><label>Select Survey To View</label></div>
<div class="rightForm">
	<select onchange="selectSurvey();" name="which_survey" id="which_survey">
			<option value=""></option>
			<?php
			//	$survey_ids_res = mlavu_query("SELECT survey_id,question FROM `poslavu__db`.survey WHERE setting='survey_title' ORDER BY survey_id DESC");
			//	while($survey_id_row = mysqli_fetch_assoc($survey_ids_res)){
			//		echo "<option value='".$survey_id_row['survey_id']."'>".$survey_id_row['question']."</option>";
			//	}//while
			?>
			
			
	</select>
</div>

<div class="clearForm"></div>
-->
<div class="leftForm"></div>
<div class="rightForm">
 
 </div>
 <div class="clearForm"></div>
 <input type="hidden" name="mode" id="mode" value="view_customer_survey"/>
</form>

<script type="text/javascript">
function selectSurvey(){
		document.getElementById("which_survey_form").submit();
}//selectSurvey()

function thankYou(){
		alert("Your feedback is greatly appreciated. Thank you!");
		return true;
}//thankYou()

</script>
<?php

	/* functions here */
	function clean($input){
			
			$string = addslashes($input);
			
			return $string;
			
	}//clean

	/* Form submission stuff goes here */
	//$sub is short for submission
	//$should_submit = reqvar("submit_ok");
	if (isset($_POST)){
			//$sub_survey_id = $_POST['survey_id'];
			$sub_question_ids = $_POST['question_ids'];
			$sub_question_ids_arr = explode(",",$sub_question_ids);
			$sub_answers = "";
			for ($t=0;$t<count($sub_question_ids_arr)*1-1;$t++){
					/* These are actually answers, not questions */
					$sub_question_name = "question_".$sub_question_ids_arr[$t];
					$sub_question_val = clean($_POST[$sub_question_name]);
					$sub_answers .= $sub_question_val."||";
					//echo "quest: $sub_question_val<br/>";
			}
			//echo "answer: $sub_answers<br/>";
		//	$locationid = "11"; ///////////                             NEED TO SET LOCATION ID!!!!
			//echo "loc: $locationid<br/>";
			/* Remember when setting="survey_answer", then the questions field actually holds answers delimited by || */
			$data_name = admin_info("dataname");
			$resInsert = mlavu_query("INSERT INTO `poslavu__db`.`survey` (setting,location_id,survey_id,question,data_name) VALUES ('survey_answer','$locationid','$survey_id','$sub_answers','$data_name')");
			//print_r($_POST);
			unset($_POST);
	}//if
?>

<style type="text/css">
.leftForm{
	float:left;
	width:250px;

	text-align:right;
	margin-right:10px;
	margin-left:50px;
}
.rightForm{
	float:left;
	width:200px;
	
	text-align:left;
}
.clearForm{
	clear:both;
}
</style>

<?php
/* php functions */

function drawInput($type,$id,$options){
		/* Put the right type of HTML input in the form */
		if ($type == "text"){
				$input_str = "<textarea name='question_".$id."' id='question_".$id."' rows='10' cols='40'></textarea>";
				return $input_str;
		}//if
		
		if ($type == "dropdown"){
				$input_str = "<select name='question_".$id."' id='question_".$id."'>";
				$input_str .="<option value=''></option>";
				
				$options_arr = explode(",",$options);
				for ($x=0;$x<count($options_arr);$x++){
						$input_str .= "<option value='".$options_arr[$x]."'>".$options_arr[$x]."</option>";
				}//fo
				$input_str .= "</select>";
				return $input_str;
		}//if
}//drawInput()

?>


<form name="survey_answer_form" method="post" action="" onsubmit="return thankYou();">
<?php

//$survey_id = "s_1";// $survey_id = "s_2"; //SET THE SURVEY_ID!!!!

/* Get the instructions */

$resTitle = mlavu_query("SELECT * FROM `poslavu__db`.`survey` WHERE setting='survey_title' AND question!='' AND survey_id='$survey_id'");
$titleRow = mysqli_fetch_assoc($resTitle);
$the_title = $titleRow['question'];//where setting='survey_instructions' question field is the instructions
	echo "<h2>".$the_title."</h2>";

$resInstruct = mlavu_query("SELECT * FROM `poslavu__db`.`survey` WHERE setting='survey_instructions' AND question!='' AND survey_id='$survey_id'");
$instructRow = mysqli_fetch_assoc($resInstruct);
$the_instructions = $instructRow['question'];//where setting='survey_instructions' question field is the instructions
	echo "<div style='width:600px;'>".$the_instructions."</div>";


$res0 = mlavu_query("SELECT * FROM `poslavu__db`.`survey` WHERE setting='survey_question' AND survey_id='$survey_id'");
$question_ids = "";//so we don't have to query the database again for efficiency's sake

while ($row0 = mysqli_fetch_assoc($res0)){
		$question = html_entity_decode($row0['question']);
		$type = $row0['type'];
		$id = $row0['id'];
		$question_ids .= $id.",";
		$options = $row0['options'];
		echo "
			<div class='leftForm'>$question</div>
			<div class='rightForm'>".drawInput($type,$id,$options)."</div>
			<div class='clearForm'></div>
		";
		
}//while


?>
<div class="leftForm">
	<input type="hidden" name="survey_id" value="<?php echo $survey_id;?>"/>
	<input type="hidden" name="question_ids" value="<?php echo $question_ids;?>"/>
</div>
<div class="rightForm">
<input type="submit" value="Submit" />
</div>
<div class="clearForm"></div>
</form>

<?php
	/* This form likes to make unnecessary submissions to database. Gotta clean the garbage */
	mlavu_query("DELETE FROM `poslavu__db`.`survey` WHERE question=''");
?>