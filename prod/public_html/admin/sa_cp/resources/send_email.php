<?php
	
	//function send_email_with_images($email_to, $email_subject, $email_body, $email_from="noreply@poslavu.com", $email_bcc="") 
	require_once(dirname(__FILE__).'/../billing/send_email.php');
	
	// sends an email through sendgrid from poslavu
	// eliminates the "via web1-...." in the email header
	// @$wt_to: either a to string seperated by commas, or an array of emails to send this to
	// @$s_from: the email to be sending from (can only be one email address)
	// @$s_subject: the subject string
	// @$s_body: the body string
	// @$a_extras: an array of value(one of the values from http://sendgrid.com/docs/API_Reference/Web_API/mail.html => value)
	// @return: array('type'=>'error', 'response'=>'no "to" email address'), array('type'=>'error', 'response'=>'no "from" email address'), array('type'=>'error', 'response'=>'no body and no subject'), or array('type'=>'response', 'response'=>the response from sendgrid)
	function send_email_through_sendgrid($wt_to, $s_from, $s_subject, $s_body, $a_extras) {
		$a_to_addresses = array();
		$to_str = '';
		
		// check the "from" address string
		$s_from = trim($s_from);
		if ($s_from == '')
			return array('type'=>'error', 'response'=>'no "from" email address');
		
		// check the body/subject
		$s_subject = trim($s_subject);
		$s_body = trim($s_body);
		if ($s_subject == '' && $s_body == '')
			return array('type'=>'error', 'response'=>'no body and no subject');
		
		// find the "to" addresses string
		if (is_array($wt_to))
			$to_parts = $wt_to;
		else
			$to_parts = explode(",",$wt_to);
		if(count($to_parts) > 1)
		{
			for($n=0; $n<count($to_parts); $n++)
			{
				$sc_parts = explode(";",$to_parts[$n]);
				for($z=0; $z<count($sc_parts); $z++)
				{
					$inner_email = trim($sc_parts[$z]);
					if($inner_email != "")
					{
						$a_to_addresses[] = urlencode(trim($inner_email));
					}
				}
			}
		}
		else
		{
			$a_to_addresses = array(urlencode(trim($wt_to)));
		}
		
		// check the "to" addresses
		if (count($a_to_addresses) > 1) {
			$to_str = 'to[]='.implode('&to[]=', $a_to_addresses).'&';
		} else if (count($a_to_addresses) == 0) {
			return array('type'=>'error', 'response'=>'no "to" email address');
		} else {
			$to_str = 'to='.$a_to_addresses[0].'&';
		}
		
		$url = "https://sendgrid.com/api/mail.send.json";
		$vars = "api_user=ticket@poslavu.com&api_key=balilavu&".$to_str."subject=".urlencode($s_subject)."&html=".urlencode($s_body)."&from=".urlencode($s_from);
		
		foreach($a_extras as $k=>$v) {
			$vars .= '&'.urlencode(trim($k)).'='.urlencode(trim($v));
		}
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		$response = curl_exec($ch);
		curl_close ($ch);
		
		return array('type'=>'response', 'response'=>$response);
		//log_email_response("SendGrid Response",$response);
	}
	
	// checks a single email for correct syntax
	// if the syntax is incorrect, it sets $b_error_sending_email and $s_error_message and returns FALSE
	// @return: TRUE if the syntax is good, FALSE otherwise
	function check_valid_email_syntax($s_email, &$b_error_sending_email, &$s_error_message) {
		$s_email = trim($s_email);
		
		// check for good email syntax
		if (preg_match('/.+\@.+\..+/', $s_email) !== 1) {
			$b_error_sending_email = TRUE;
			$s_error_message = '"'.$s_email.'" is not in a valid email format.';
			return FALSE;
		}
		
		return TRUE;
	}
	
?>