<?php
	if(can_access("customers")) {

		ini_set("display_errors","1");
		require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
		
		$fields = array();
		$fields[] = array("title", "Title");
		$fields[] = array("city", "City");
		$fields[] = array("state", "State");
		$fields[] = array("country", "Country");
		$fields[] = array("gateway", "Gateway");
		$fields[] = array("net_path", "Net Path");
		$filter = "";
		
		$filter .= "(`restaurant_locations`.`disabled`='' or `restaurant_locations`.`disabled`='0')";
		
		echo "<style>";
		echo ".label { text-align:right; color:#999999; vertical-align:top; } ";
		echo "</style>";
		echo "<form name='form1' method='post' action='index.php?mode=client_search&search=1'>";
		echo "<table cellpadding=12><td style='border:solid 1px black' valign='top'>";
				
		echo "<div style='float:left'>";
		echo "<table>";
		echo "<tr><td colspan='4'>Search Client Locations:</td></tr>";
		echo "<tr><td>&nbsp;</td></tr>";
		echo "<tr>";
		foreach ($fields as $field) {
			echo "<td class='label' style='padding-left:10px'>".ucwords($field[1]).": </td>";
			echo "<td><input type='text' name='".$field[0]."' size='16' value='".$_POST[$field[0]]."'></td>";
			if (!empty($_POST[$field[0]])) {
				if ($filter != "") { $filter .= " AND"; }
				$filter .= " `restaurant_locations`.`".$field[0]."` LIKE '%[".$field[0]."]%'";
			}
		}
		echo "<td style='padding-left:10px'><input type='submit' value='Search'></td>";
		echo "</tr>";
		echo "</table>";
		echo "</form>";

		echo "</div>";
		echo "</table>";
		
		echo "<div style='float:left; padding-left:10px; width:1200px;'>";
		echo "<br><br>";
		
		if (!empty($_GET['search'])) {
		
			if ($filter == "") {
			
				echo "Please fill out at least one field to perform search...";
			
			} else {

				$heading_shown = false;
				/*$get_restaurants = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants`");
				while ($c_info = mysqli_fetch_assoc($get_restaurants)) {
				
					$account_type = "Enabled";
					if ($c_info['dev'] == "1") {
						$account_type = "Dev";
					} else if ($c_info['disabled'] > 0) {
						$account_type = "Disabled";
					} else if ($c_info['demo'] == "1") {
						$account_type = "Demo";
					} else if ($c_info['test'] == "1") {
						$account_type = "Test";
					} else if ($c_info['reseller'] == "1") {
						$account_type = "Reseller";
					}
										
					$_POST['data_name'] = $c_info['data_name'];
					$check_locations = mlavu_query("SELECT * FROM `poslavu_[data_name]_db`.`locations` WHERE $filter ORDER BY `title` ASC", $_POST);*/


					//$check_locations = mlavu_query("SELECT * from `poslavu_MAIN_db`.`restaurant_locations` WHERE $filter ORDER BY `title` ASC",$_POST);
					mlavu_select_db("poslavu_MAIN_db");
					$check_locations = mlavu_query("SELECT `restaurant_locations`.*, `restaurants`.`dev` AS `dev`, `restaurants`.`last_activity` AS `r_last_activity` FROM `restaurant_locations` LEFT JOIN `restaurants` ON `restaurants`.`data_name` = `restaurant_locations`.`dataname` WHERE $filter ORDER BY `restaurant_locations`.`title` ASC",$_POST);
					if (mysqli_num_rows($check_locations) > 0) {
						echo mysqli_num_rows($check_locations) . " records found:<br>";
					
						if ($heading_shown == false) {
							$heading_shown = true;
							echo "<table cellspacing=1 cellpadding=5 style='border:solid 1px black'>";
							echo "<tr bgcolor='#66FF99'>";
							echo "<td align='center' valign='bottom'>Company</td>";
							echo "<td align='center' valign='bottom'>Dataname</td>";
							echo "<td align='center' valign='bottom'>Location</td>";
							echo "<td align='center' valign='bottom'>City</td>";
							echo "<td align='center' valign='bottom'>State</td>";
							echo "<td align='center' valign='bottom'>Country</td>";
							echo "<td align='center' valign='bottom'>Email</td>";
							echo "<td align='center' valign='bottom'>Phone</td>";
							echo "<td align='center' valign='bottom'>Type</td>";
							//echo "<td align='center' valign='bottom'>License</td>";
							//echo "<td align='center' valign='bottom'>Hosting</td>";
							echo "<td align='center' valign='bottom'>Net Path</td>";
							echo "<td align='center' valign='bottom'>Gateway</td>";
							echo "<td align='center' valign='bottom'>Transtype</td>";
							echo "<td align='center' valign='bottom'>Last Activity</td>";
							echo "<td align='center' valign='bottom'>Dev</td>";
							echo "</tr>";
						}
	
						while ($l_info = mysqli_fetch_assoc($check_locations)) {
							echo "<tr onmouseover='this.bgColor = \"#CCDDFF\"' onmouseout='this.bgColor = \"#FFFFFF\"' onclick='window.location = \"index.php?mode=manage_customers&submode=details&rowid=".$l_info['restaurantid']."\"' style=\"cursor:pointer;\">";
							//echo "<td>".$c_info['company_name']."</td>";
							//echo "<td>".$c_info['data_name']."</td>";
							echo "<td>".$l_info['title']."</td>";
							echo "<td>".$l_info['dataname']."</td>";
							
							echo "<td>".$l_info['title']."</td>";
							echo "<td>".$l_info['city']."</td>";
							echo "<td>".$l_info['state']."</td>";
							echo "<td>".$l_info['country']."</td>";
							//echo "<td>".((!empty($c_info['email']))?$c_info['email']:$l_info['email'])."</td>";
							//echo "<td>".((!empty($c_info['phone']))?$c_info['phone']:$l_info['phone'])."</td>";
							echo "<td>".((!empty($l_info['email']))?$l_info['email']:$l_info['email'])."</td>";
							echo "<td>".((!empty($l_info['phone']))?$l_info['phone']:$l_info['phone'])."</td>";
							
							//echo "<td>".$account_type."</td>";
							echo "<td>".$l_info['product_level']."</td>";
							
							//echo "<td>".$c_info['license_status']."</td>";
							//echo "<td>".$c_info['hosting_status']."</td>";
							echo "<td>".$l_info['net_path']." (".$l_info['use_net_path'].")</td>";
							echo "<td>".$l_info['gateway']."</td>";
							echo "<td>".$l_info['cc_transtype']."</td>";

							$activity_color = "#222222";
							if ($l_info['r_last_activity'] >= date("Y-m-d H:i:s", (time() - 432000))) $activity_color = "#588a3a; font-weight:bold;";
							echo "<td><span style='font-size:11px; color:$activity_color;'>".$l_info['r_last_activity']."</span></td>";
							echo "<td>".$l_info['dev']."</td>";
							echo "</tr>";
						}
					}
				//}
				
				if ($heading_shown == true) {
					echo "</table><br><br>";
				} else {
					echo "No locations found matching your search criteria...";
				}
			}
		}
	
		echo "</div>";
	}
?>