<?php
	require_once(dirname(__FILE__). '/../cp/areas/white_list.php');

	if($in_lavu)
	{
		$maindb = "poslavu_MAIN_db";
		$login_message = "";

		$fwd_page = "";
		$fwd_action = "";
		if(isset($_GET['fwd']))
		{
			$fwd_page = $_GET['fwd'];
			$fwd_action = "?fwd=" . $fwd_page;
		}

		if(isset($_POST['username']) && isset($_POST['password']))
		{
			function update_cp_login_status($success,$username)
			{
				global $maindb;
				if($success)
					$set_field = "succeeded";
				else
					$set_field = "failed";

				if (!$success && $username == "andy") {
					file_put_contents(dirname(__FILE__)."/stuff.txt", print_r($_SERVER, TRUE), FILE_APPEND);
				}

				$ipaddress = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];  // work-around for load balancer proxy
				$mints = time() - (60 * 15);
				$currentts = time();
				$currentdate = date("Y-m-d H:i:s");
				$userVar = mysqli_real_escape_string($username);

				$li_query = mlavu_query("select * from `$maindb`.`login_log` where `ipaddress`='$ipaddress' and `$set_field`>'0' and `ts`>='$mints' and `users`='$userVar'");
				if(mysqli_num_rows($li_query))
				{
					$li_read = mysqli_fetch_assoc($li_query);
					$li_id = $li_read['id'];
					mlavu_query("update `$maindb`.`login_log` set `ts`='$currentts', `date`='$currentdate', `$set_field`=`$set_field`+1 where `id`='$li_id'",$username);
				}
				else
				{
					mlavu_query("insert into `$maindb`.`login_log` (`ts`,`date`,`$set_field`,`ipaddress`,`users`) values ('$currentts','$currentdate','1','$ipaddress','[1]')",$username);
				}
			}

			function get_cp_login_count($field, $username)
			{
				global $maindb;
				$userVar = mysqli_real_escape_string($username);

				$ipaddress = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];  // work-around for load balancer proxy
				$mints = time() - (60 * 15);

				$li_query = mlavu_query("select * from `$maindb`.`login_log` where `ipaddress`='$ipaddress' and `$field`>'0' and `ts`>='$mints' and `users`='$userVar'");
				if(mysqli_num_rows($li_query))
				{
					$li_read = mysqli_fetch_assoc($li_query);
					return $li_read[$field];
				}
				else return false;
			}

			$failed_login_attempts = get_cp_login_count("failed", $POST['username']);
			if($failed_login_attempts >= 10 && !isset($whiteList[$ipaddress]) && !$isLocalIP)
			{
				update_cp_login_status(false,$_POST['username']);
				$login_message = "Exceeded Max Login Attempts";

				if($failed_login_attempts==100 || $failed_login_attempts ==10)
				{
					if($failed_login_attempts==100)
						$msubject = "Attack Detected: SA";
					else
						$msubject = "Exceeded Login Attempts: SA";

					mail("cp_notifications@lavu.com","$msubject - PosLavu","$msubject\nusername: " . $_POST['username'] . "\npassword: " . $_POST['password'] . "\nip address: " . $_SERVER['HTTP_X_FORWARDED_FOR'],"From: security@poslavu.com");  // work-around for load balancer proxy
				}
			}
			else
			{
				$account_query = mlavu_query("select * from `poslavu_MAIN_db`.`accounts` where `username`='[1]' and (`password`=PASSWORD('[2]') or `password`=OLD_PASSWORD('[2]'))",$_POST['username'],$_POST['password']);
				if(mysqli_num_rows($account_query))
				{
					$account_read = mysqli_fetch_assoc($account_query);
					$loggedin = $account_read['username'];
					$loggedin_fullname = trim($account_read['firstname'] . " " . $account_read['lastname']);
					$loggedin_email = $account_read['email'];
					$loggedin_access = $account_read['access'];
					$loggedin_lavu_admin = $account_read['lavu_admin'];
					$_SESSION['posadmin_loggedin'] = $loggedin;
					$_SESSION['posadmin_fullname'] = $loggedin_fullname;
					$_SESSION['posadmin_email'] = $loggedin_email;
					$_SESSION['posadmin_access'] = $loggedin_access;
					$_SESSION['posadmin_lavu_admin'] = $loggedin_lavu_admin;
					$_SESSION['posadmin_password'] = $_POST['password'];

					update_cp_login_status(true,$_POST['username']);

					if(isset($_POST['stay_logged_in']))
					{
						$autokey = session_id() . rand(1000,9999);
						setcookie("poslavu_sacp_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
						//mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
					}

					if($fwd_page=="")
					{
						if($loggedin=="corey" || $loggedin=="j.edwards" || $loggedin=="josh" || $loggedin=="ohad" || $loggedin=="mirza" || $loggedin=="andy")
						{
							$fwd_page = "index.php?mode=intrep";
						}
						else if($loggedin=="b.orourke")
						{
							$fwd_page = "index.php?mode=retention_info";
						}
						else if($loggedin=="brad")
						{
							$fwd_page = "index.php?mode=mtools";
						}
						else
						{
							$fwd_page = "index.php";
						}
					}

					echo "<script language='javascript'>";
					echo "window.location.replace('$fwd_page'); ";
					echo "</script>";
					exit();
				}
				else
				{
					update_cp_login_status(false,$_POST['username']);
					$login_message = "Invalid Account or Password";
				}

				/*$cust_query = mlavu_query("select * from `$maindb`.`customer_accounts` where `username`='[1]'",$_POST['username']);
				if(mysqli_num_rows($cust_query))
				{
					$cust_read = mysqli_fetch_assoc($cust_query);
					$custid = $cust_read['id'];
					$dataname = $cust_read['dataname'];
					$rdb = "poslavu_".$dataname."_db";

					$user_query = mlavu_query("select * from `$rdb`.`users` where `access_level`>='3' and `username`='[1]' and (`password`=PASSWORD('[2]') or `password`=OLD_PASSWORD('[2]'))",$_POST['username'],$_POST['password']);
					if(mysqli_num_rows($user_query))
					{
						$rest_query = mlavu_query("select * from `$maindb`.`restaurants` where `data_name`='[1]'",$dataname);
						if(mysqli_num_rows($rest_query))
						{
							$rest_read = mysqli_fetch_assoc($rest_query);
							if($rest_read['disabled']=="1")
								$company_disabled = true;
							else
								$company_disabled = false;
							$companyid = $rest_read['id'];
						}
						else
						{
							$company_disabled = true;
							$companyid = 0;
						}

						if($company_disabled)
						{
							update_cp_login_status(false,$_POST['username']);
							$login_message = "Invalid Account or Password";
						}
						else
						{
							$user_read = mysqli_fetch_assoc($user_query);
							admin_login($dataname,$user_read['id'],$user_read['username'],trim($user_read['f_name']." ".$user_read['l_name']),$user_read['email'], $companyid);
							update_cp_login_status(true,$_POST['username']);

							if(isset($_POST['stay_logged_in']))
							{
								$autokey = session_id() . rand(1000,9999);
								setcookie("poslavu_cp_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
								mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
							}

							echo "<script language='javascript'>";
							echo "window.location.replace('index.php'); ";
							echo "</script>";
							exit();
						}
					}
					else
					{
						update_cp_login_status(false,$_POST['username']);
						$login_message = "Invalid Account or Password";
					}
				}
				else
				{
					update_cp_login_status(false,$_POST['username']);
					$login_message = "Invalid Account or Password";
				}*/
			}
		}

		echo "<table cellpadding=16><td style='border: solid 2px #bbbbbb' bgcolor='#f6f6f6'>";
		echo "<form name='login' method='post' action='$fwd_action'>";
		echo "<table>";
		if($login_message!="")
			echo "<tr><td colspan='2' align='center'><b>$login_message</b></td></tr>";
		echo "<tr><td class='form_row_title'>Username</td><td><input type='text' name='username'></td></tr>";
		echo "<tr><td class='form_row_title'>Password</td><td><input type='password' name='password'></td></tr>";
		echo "<tr><td>&nbsp;</td><td><input class='form_submit_button' type='submit' value='Submit'></td></tr>";
		//echo "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
		//echo "<tr><td>&nbsp;</td><td><input type='checkbox' name='stay_logged_in'> Stay Logged In</td></tr>";
		echo "</table>";
		echo "</form>";
		echo "</td></table>";
		echo "<script language='javascript'>";
		echo "document.login.username.focus(); ";
		echo "</script>";
	}
?>
