<?php
require_once(dirname(__FILE__) . "/report_functions.php");
require_once (dirname(__FILE__) . "/../lib/paginator.class.php");
if(function_exists("resource_path"))
{
	require_once(resource_path() . "/chain_functions.php");
}
else
{
	require_once("/home/poslavu/public_html/admin/cp/resources/chain_functions.php");
}

/**
 * Displays pagination links on page
 * @param Paginator $pages
 * @return html
 */
function displayPagination($pages){
	$paginationLiks="";
	$allLinks = $pages->display_pages();
	if(strlen($allLinks)>10){
		$paginationLiks="<div style=\"display: table; margin-right: 20px; margin-left: auto;\">";
		$paginationLiks .=$allLinks;
		$paginationLiks .=$pages->display_items_per_page();
		$paginationLiks .="</div>";
	}
	return $paginationLiks;
}

/**
 * Provides object which we can use for Pagination functionality
 * @param string $selectQuery
 * @return Paginator
 */
function getPaginationInfo($selectQuery, $noOfRecordsPerPage, $page){
	if (!isset($noOfRecordsPerPage)) $noOfRecordsPerPage = 100;
	if (!isset($page)) $page = 1;
	$itemsPerPage = $noOfRecordsPerPage;
	$queryResult = mrpt_query($selectQuery);
	$totalCount = 0;
	while ($resultCount = mysqli_fetch_assoc($queryResult)) {
		$totalCount = (int)$resultCount["totalCount"];
	}
	$currentPage = $page;
	$offset = ($currentPage - 1) * $itemsPerPage + 1;
	$pages = new Paginator($currentPage, 7, $itemsPerPage);
	$pages->items_total = $totalCount;
	$pages->paginate($currentPage, $noOfRecordsPerPage);
	return $pages;
}

/*$allow_multi_location = false;
if(isset($_GET['test_multi_location'])) { $_SESSION['test_multi_location'] = $_GET['test_multi_location']; }
if(isset($_SESSION['test_multi_location']) && $_SESSION['test_multi_location']==1) $allow_multi_location = true;*/
$allow_multi_location = true;

function ast_replace($fieldname,$str)
{
	$str = str_replace("**","((asterisk))",$str);
	$str = str_replace("*",$fieldname,$str);
	$str = str_replace("((asterisk))","*",$str);
	return $str;
}

function export_report($filename,$str)
{
	$str = (string)$str;
	$known_mime_types=array(
		"pdf" => "application/pdf",
		"txt" => "text/plain",
		"html" => "text/html",
		"htm" => "text/^html",
		"exe" => "application/octet-stream",
		"zip" => "application/zip",
		"doc" => "application/msword",
		"xls" => "application/vnd.ms-excel",
		"ppt" => "application/vnd.ms-powerpoint",
		"gif" => "image/gif",
		"png" => "image/png",
		"jpeg"=> "image/jpg",
		"jpg" =>  "image/jpg",
		"php" => "text/plain",
		"csv" => "text/csv"
	);
	$file_extension = strtolower(substr(strrchr($filename,"."),1));
	$mime_type = (isset($known_mime_types[$file_extension]))?$known_mime_types[$file_extension]:$file_extension;

	if ($file_extension == 'xls') {
	   $str = mb_convert_encoding($str, 'UTF-16LE', 'UTF-8');
	   $str = chr(255).chr(254). $str;
    } else {
		$str =  chr(239).chr(187).chr(191). $str;
	}

	header("Content-Type: $mime_type; charset=UTF-8");
	header('Content-Disposition: attachment; filename="'.$filename.'"');
	header("Content-Transfer-Encoding: binary");
	header('Accept-Ranges: bytes');

	header("Cache-control: private");
	header('Pragma: private');
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Content-Length: ".strlen($str));

	echo $str;
}

function rs_start_val($val,$def="")
{
	if(isset($_SESSION['rselector_'.$val])) return $_SESSION['rselector_'.$val];
	else return $def;
}
function set_rs_start_val($val,$set)
{
	$_SESSION['rselector_'.$val] = $set;
}
function rs_getvar($var,$def)
{
	//return (isset($_GET[$var]))?$_GET[$var]:$def;
	$setval = (isset($_GET[$var]))?$_GET[$var]:rs_start_val($var,$def);
	set_rs_start_val($var,$setval);
	return $setval;
}

function display_money($val, $loc_info, $ts=",") {

	$sym = trim($loc_info['monitary_symbol']);
	$lor = $loc_info['left_or_right'];
	$dp = $loc_info['disable_decimal'];
	$dc = $loc_info['decimal_char'];
	$ts	= $dc == "." ? "," : ".";

		if ($sym == "")
			return number_format(round($val, $dp), $dp, $dc, $ts);
		else if ($lor == "right")
			return number_format(round($val, $dp), $dp, $dc, $ts).$sym;
		else
			return $sym.number_format(round($val, $dp), $dp, $dc, $ts);
}

function reportsTableHeaderJSTostr() {
	return <<<JAVASCRIPT
	<script type='text/javascript'>
	function createReportHeaderRows() {
		window.reportHeaderRows = [];

		var findTableHeader = function(jtable) {
			var jhead = jtable.find("thead");
			if (jhead.length > 0) {
				var jtr = jhead.find("tr");
				if (jtr.length > 0) {
					return jtr;
				}
			}

			var rowIsHeader = function(jtr) {
				var children = jtr.children();
				var retval = false;
				for(var i = 0; i < children.length; i++) {
					var child = children[i];
					if (child.tagName.toLowerCase() == "th") {
						retval = true;
						break;
					}
				}
				return retval;
			}

			var jtrs = jtable.find("tr");
			for(var i = 0; i < jtrs.length; i++) {
				var jtr = $(jtrs[i]);
				if (rowIsHeader(jtr)) {
					return jtr;
				}
			}

			return null;
		};

		var findTableTitle = function(k) {
			var jtitles = $("b.reportTitle");
			var jreports = $(".report");
			if (jreports.length < jtitles.length) {
				return $(jtitles[k+1]).text();
			}
			if (jtitles.length >= k) {
				return $(jtitles[k]).text();
			}
			return "";
		};

		var createHeaderRow = function(k, table_element) {

			// find the header tr row
			var jtable = $(table_element);
			var jtr = findTableHeader(jtable);
			if (jtr == null) {
				return;
			}

			// create the header row
			var title = findTableTitle(k);
			var left = jtr.offset().left+"px";
			var jheaderRowTable = $("<table cellpadding=4><thead><tr></tr></thead></table>")
			var jheaderRow = $("<div class='headerRow' style='border-bottom:1px solid #999; position:fixed; top:0; left:"+left+"; background-color:#f3f3f3; overflow-y:hidden;'></div>");
			jheaderRow.append(jheaderRowTable);
			var jheaderRowTableRow = jheaderRowTable.find("tr");
			var addHeader = function(k, th) {
				jth = $(th);
				jth_clone = jth.clone();
				jth_clone.css({ width:(parseInt(jth.width()) + "px"), height:(parseInt(jth.height()) + "px") });
				jheaderRowTableRow.append(jth_clone);
			}
			$.each(jtr.find("td"), addHeader);
			$.each(jtr.find("th"), addHeader);
			jtitle = null;
			if (title != "") {
				jtitle = $("<div style='position:fixed; top:0px; left:0px; background-color:white; border:1px solid #999; border-top:none; border-right:none; padding: 4px;'><table style='vertical-align:middle; height:100%;'><tr><td><span style='font-style:italic;'>"+title+"</span></td></tr></table></div>");
			}

			// add to the list of header rows
			var top = parseInt(jtable.offset().top);
			var bottom = top+parseInt(jtable.height());
			reportHeaderRows.push({ table:jtable, headerRow:jheaderRow, visible:false, top:top, bottom:bottom, title:jtitle });
		};

		var sortHeaderRows = function(a, b) {
			return a.top - b.top;
		};

		var displayHeaderRows = function() {
			var headerRowToDisplay = function() {
				var retval = -1;
				var windowScroll = parseInt($(window).scrollTop());
				var doShow = function(k, headerRow) {
					if (headerRow.top < windowScroll && headerRow.bottom-42 > windowScroll) {
						retval = k;
					}
				}
				$.each(reportHeaderRows, doShow);
				return retval;
			}

			var hideHeaderRows = function(rowIndex) {
				var hideRow = function(k, row) {
					if (k == rowIndex) {
						return;
					}
					if (row.visible == true) {
						row.table.find(".headerRow").remove();
					}
					row.visible = false;
				}
				$.each(reportHeaderRows, hideRow);
			}

			var showHeaderRow = function(row, rowIndex) {
				jheaderRow = row.headerRow.clone();
				row.table.append(jheaderRow);
				reportHeaderRows[rowIndex].visible = true;

				if (row.title !== null) {
					var jtitle = row.title.clone();
					jheaderRow.append(jtitle);
					var height = parseInt(jheaderRow.height()) - parseInt(jtitle.css("padding-top")) - parseInt(jtitle.css("padding-bottom")) + "px";
					var width = parseInt(jtitle.width());
					var leftOffset = parseInt(jheaderRow.offset().left);
					var left = leftOffset - parseInt(jtitle.css("padding-right")) - width - 4 + "px";
					jtitle.css({ height:height, left:left, opacity:0 });
					jtitle.animate({ opacity:1 }, 300);
				}
			}

			var rowIndex = headerRowToDisplay();
			if (rowIndex == -1) {
				hideHeaderRows();
				return;
			}
			var row = reportHeaderRows[rowIndex];
			if (row.visible) {
				hideHeaderRows(rowIndex);
				return;
			}
			showHeaderRow(row, rowIndex);
			hideHeaderRows(rowIndex);
		};

		var tables = $("#main_content_area").find("table.report");
		$.each(tables, createHeaderRow);
		reportHeaderRows.sort(sortHeaderRows);
		$(window).scroll(displayHeaderRows);
	}
	setTimeout(function() {
		$(document).ready(createReportHeaderRows);
	}, 500);
	</script>
JAVASCRIPT;
}

function show_report($dbname,$maindb,$date_mode,$rurl,$reportid,$special=false,$set_locationid=false,$is_combo_rep=false,$return_query=false,$b_second_part_of_multi_report=FALSE)
{
	//$DEBUG = "$dbname\n$maindb\n$date_mode\n$rurl\n$reportid\n$special\n$set_locationid\n$is_combo_rep\n\n\n\n";
	if($special=="debug") $debug = true; else $debug = false;
	if(strpos($special,"export")!==false) $export = true; else $export = false;
	if(substr($special,0,10)=="subreport:")
	{
		$use_subreport = true;
		$subreport = substr($special,10);
	}
	else $use_subreport = false;

	// report options - controled

	require_once(dirname(__FILE__).'/../cp/areas/reports/graphData.php');
	$b_draw_pretty_graphs = TRUE;

	//if (lavuDeveloperStatus()) {
	//	$b_draw_multi_locations = TRUE;
	//	echo reportsTableHeaderJSTostr();
	//}

	// whether to draw multiple locations as separate reports or as a single report
	if (isset($_GET['sep_loc'])) {
		$b_separate_locations = ($_GET['sep_loc'] == '0') ? FALSE : TRUE;
		set_rs_start_val('draw_multi_reports_separate_locations', $b_separate_locations);
	} else {
		$b_separate_locations = rs_getvar('draw_multi_reports_separate_locations', TRUE);
	}

	//--------------------------------------------- Multi Location Code -------------------------------------//
	if(isset($_GET['multireport'])) {
		$showing_multi_report = trim($_GET['multireport']);
		if ((int)$showing_multi_report == 0)
			$showing_multi_report = FALSE;
		set_rs_start_val('draw_multi_reports_multireport', $showing_multi_report);
	} else {
		$showing_multi_report = rs_getvar('draw_multi_reports_multireport', FALSE);
	}

	// testing/debugging (can be deleted)
	$mpcount = 0;
	foreach($_GET as $k=>$v)
		if ($k == 'multireport')
			$mpcount++;
	if ( ($showing_multi_report == "0" || $showing_multi_report === 0 || $mpcount > 1) && $showing_multi_report !== FALSE)
		error_log(__FILE__.' error with multireport: '.gettype($showing_multi_report).', getvars:'.print_r($_GET,TRUE));
	if ($showing_multi_report == "0" || $showing_multi_report == 0) {
		$showing_multi_report = FALSE;
		set_rs_start_val('draw_multi_reports_multireport',$showing_multi_report);
	}

	global $allow_multi_location;
	$selector_str = "";
	$multi_location_ids = "";
	if($allow_multi_location)
	{
		$cr_dataname = $dbname;
		$cr_dataname = substr($cr_dataname,8);
		$cr_dataname = substr($cr_dataname,0,strlen($cr_dataname) - 3);

		if(isset($_GET['mode']) && substr($_GET['mode'],0,7)=="reports")
		{

			// get the chain reporting groups
			require_once(dirname(dirname(__FILE__)).'/sa_cp/manage_chain_functions.php');
			$cr_groups = CHAIN_USER::get_user_reporting_groups();
			$a_chain_locations = getChainLocations($cr_dataname, FALSE, FALSE);

			// check that the currently selected group is one of the groups this user has access to
			// -1 means "all locations for this restaurant"
			if ($showing_multi_report !== FALSE && $showing_multi_report !== '-1') {
				$b_found = FALSE;
				$first_cr_group = NULL;
				foreach($cr_groups as $cr_group) {
					$first_cr_group = $cr_group;
					if ((int)$cr_group['id'] == (int)$showing_multi_report) {
						$b_found = TRUE;
						break;
					}
				}
				if (!$b_found)
					$showing_multi_report = ($first_cr_group !== NULL) ? $first_cr_group['id'] : FALSE;
			}

			$cr_group_name = "";
			if( (count($cr_groups) > 0 || count($a_chain_locations) > 1) )
			{
				// find the group name and associated accounts
				foreach($cr_groups as $cr_group)
				{
					if((int)$showing_multi_report == (int)$cr_group['id'])
					{
						$cr_group_name = $cr_group['name'];
						$multi_location_ids = $cr_group['contents'];
					}
				}

				// create a getvar string
				$uri = $_SERVER['QUERY_STRING'];
				$uri = str_ireplace(array("&multireport=0","&multireport=".$showing_multi_report), "", $uri);

				// create a dropdown select box for selecting the reporting group
				$selector_str .= "<select onchange='if(this.value==\"\") window.location = \"index.php?$uri&multireport=0\"; else window.location = \"index.php?$uri&multireport=\" + this.value'>";
				$selector_str .= "<option value=''>Showing Current Location</option>";
				if (count($a_chain_locations) > 1) {
					$s_selected = ($showing_multi_report == '-1') ? "selected" : "";
					$selector_str .= "<option value='-1' $s_selected>Current Account - All Locations</option>";
				}
				if (count($cr_groups) > 0) {
					foreach($cr_groups as $cr_group)
					{
						$groupid = $cr_group['id'];
						$groupname = $cr_group['name'];
						$s_selected = ((int)$groupid == (int)$showing_multi_report) ? "selected" : "";
						$selector_str .= "<option value='$groupid' $s_selected>$groupname</option>";
					}
				}
				$selector_str .= "</select>";

				// create a dropdown with an option to view all locations in one report or
				// in separate reports
				if (@$b_draw_multi_locations && $showing_multi_report != FALSE && $showing_multi_report !== '-1') {
					$s_group_selected = ($b_separate_locations) ? "" : "selected";
					$s_separate_selected = ($b_separate_locations) ? "selected" : "";
					$sep_uri = str_replace(array('&sep_loc=0','&sep_loc=1'),'',$uri);
					$sep_uri = str_replace(array('sep_loc=0&','sep_loc=1&'),'',$sep_uri);
					$sep_uri = str_replace(array('sep_loc=0','sep_loc=1'),'',$sep_uri);
					$selector_str .= "<select onchange='window.location = \"index.php?$sep_uri&sep_loc=\"+this.value'>
						<option value='0' $s_group_selected>Group Locations</option>
						<option value='1' $s_separate_selected>Separate Locations</option>
					</select>";
				}
			}
		}
	}

	if(strpos($date_mode,"|")) $selector_str = "";
	//-------------------------------------------------------------------------------------------------------//

	$show_extra_functions = false;
	if(isset($_GET['test48']))
		$show_extra_functions = true;


	if(!$export && ! $return_query)
	{
		createWhiteBox();
	}

	if($set_locationid)
		$locationid = $set_locationid;
	else if(sessvar_isset("locationid"))
		$locationid = sessvar("locationid");
	else
		$locationid = false;

	$date_start = "";
	$date_end = "";
	$time_start = "";
	$time_end = "";
	$setdate = '';
	$set_date_end = '';
	$set_date_start = '';
	$day_duration = '';

	$primary_table = "";
	$join_code = "";
	$select_code = "";
	$select_outer_code = "";
	$where_code = "";
	$stitles = array();
	$sdisplay = array();
	$smonetary = array();
	$ssum = array();
	$ssumop = array();
	$sgraph = array();
	$sbranches = array();
	$salign = array();
	$scount = 0;

	$groupid = "";
	$dateid = "";
	$timeid = "";
	$groupby = "";

	$s_header = "";
	$s_body = "";
	$s_footer = "";
	$s_graph = "";
	$output = "";
	$export_output = "";
	$export_row = "\r\n";
	$export_col = "\t";
	if($special=="export:xls") $export_ext = "xls";
	else if($special=="export:csv") { $export_ext = "csv"; $export_col = ","; }
	else $export_ext = "txt";

	$branchto = (isset($_GET['branch']))?$_GET['branch']:"";
	$branch_title = (isset($_GET['branch_title']))?$_GET['branch_title']:"";
	$branch_col = (isset($_GET['branch_col']))?$_GET['branch_col']:"";
	$branch_fieldname = "";
	$repeat_foreach = "";

	if($reportid=="select_date")
		$date_selector = true;
	else
		$date_selector = false;

	if($date_selector)
	{
		$date_select_type = "Day Range";
	}
	else
	{
		$report_query = mrpt_query("select * from `$maindb`.`reports` where id='$reportid'");
		if(mysqli_num_rows($report_query))
		{
			$report_read = mysqli_fetch_assoc($report_query);
			$groupid = $report_read['groupid'];
			$dateid = $report_read['dateid'];
			$locid = $report_read['locid'];
			$timeid = $report_read['timeid'];
			$group_operation = $report_read['group_operation'];
			$order_operation = $report_read['order_operation'];
			$orderby = explode(" ",$report_read['orderby']);
			$date_select_type = $report_read['date_select_type'];
			$report_title = $report_read['title'];
			$repeat_foreach = $report_read['repeat_foreach'];
			$extra_functions = $report_read['extra_functions'];
			$set_unified = $report_read['unified'];

			if(isset($_GET['test'])) echo "date mode: $set_unified <br>";
					if($set_unified=="No") $date_mode = "single";

			if($report_read['report_type']=="Combo")
				$date_mode = "combo|" . $date_mode;

			if(count($orderby) > 1)
			{
				$orderby_id = trim($orderby[0]);
				$orderby_dir = trim($orderby[1]);
			}
			else
			{
				$orderby_id = 0;
				$orderby_dir = "asc";
			}
			$orderby = "";

			if($use_subreport)
			{
				$s_header .= "<b>" . $subreport . "</b>";
			}
			else
			{
				$s_header .= "Report: ";
				if($is_combo_rep)
					$s_header .= "<a href='?mode=reports_id_".$report_read['id']."'><b class='reportTitle'>" . $report_read['title'] . "</a>";
				else{
					$s_header .= "<b class='reportTitle'>" . $report_read['title'];
					if( $report_read['title']=='Credit Card Batches')
						$s_header.="</b><br/> This report is available for customers processing with Mercury Payment Systems and who manually settle the batch.";
				}
				if($branch_col!="") $s_header .= ": " . $branch_title;
				$s_header .= "</b>";

				$s_header .= "&nbsp;" . $selector_str;
			}
			$s_header .= "<br><br>";
		}
	}

	$explicit_date_end = false;
	$rep_type = "Regular";
	if(substr($date_mode,0,5)=="auto|")
	{
		$date_parts = explode("|",$date_mode);
		$date_mode = $date_parts[1];
		$date_start = $date_parts[2];
		$time_start = $date_parts[3];
		$date_end = $date_parts[4];
		$time_end = $date_parts[5];
		$date_select_type = "auto";
		$rep_type = "SubReport";

		$set_date_start = $date_start;
		$set_date_end = $date_end;
		$set_time_start = $time_start;
		$set_time_end = $time_end;
		//if(isset($_GET['test'])) echo "SETDATE:".$set_date_start . " - " . $set_date_end;

		$explicit_date_end = true;
	}
	if(substr($date_mode,0,6)=="combo|")
	{
		$date_parts = explode("|",$date_mode);
		$date_mode = $date_parts[1];
		$rep_type = "Combo";
		$date_select_type = "Day Range";
	}
	//if(isset($_GET['test'])) echo "date mode: $date_mode <br>";

	$change_group_from = "";
	$change_group_to = "";
	$change_group_id = "";
	$reporturl = "$rurl&reportid=$reportid&rmode=view";
	//------------------------------------ date/time selection here -----------------------------------//
	if($date_select_type=="Year")
	{
		$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y");
		$s_header .= "<select onchange='window.location = \"$reporturl&setdate=\" + this.value'>";
		for($i=date("Y")-8; $i<date("Y")+8; $i++)
		{
			$sv = $i;
			$s_header .= "<option value='$sv'";
			if($sv==$setdate) $s_header .= " selected";
			$s_header .= ">".date("Y",mktime(0,0,0,0,0,$i+1))."</option>";
		}
		$s_header .= "</select>";
		$set_date_start = $setdate . "-01-01";
		$set_date_end = $setdate . "-12-31";
		$set_time_start = "00:00:00";
		$set_time_end = "24:00:00";
	}
	else if($date_select_type=="Month")
	{
		$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y-m");
		$s_header .= "<select onchange='window.location = \"$reporturl&setdate=\" + this.value'>";
		for($i=date("Y")-2; $i<date("Y")+2; $i++)
		{
			for($n=1; $n<=12; $n++)
			{
				if($n<10) $m = "0".$n; else $m = $n;
				$sv = $i . "-" . $m;
				$s_header .= "<option value='$sv'";
				if($sv==$setdate) $s_header .= " selected";
				$s_header .= ">".date("M Y",mktime(0,0,0,$n+1,0,$i))."</option>";
			}
		}
		$s_header .= "</select>";
		$set_date_start = $setdate . "-01";
		$set_date_end = $setdate . "-31";
		$set_time_start = "00:00:00";
		$set_time_end = "24:00:00";
	}
	else if($date_select_type=="Day" || $date_select_type=="Day Range")
	{
		//$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y-m-d");
		//$day_duration = (isset($_GET['day_duration']))?$_GET['day_duration']:1;
		//$stime = (isset($_GET['stime']))?$_GET['stime']:0;

		if(date("H") < 7) $default_setdate = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 1,date("Y")));
		else $default_setdate = date("Y-m-d");

		$setdate = rs_getvar("setdate",$default_setdate);
		$day_duration = rs_getvar("day_duration",1);
		$stime = rs_getvar("stime",0);

		$s_header .= "<script type=\"text/javascript\" src=\"resources/datepickercontrol.js\"></script>";
		$s_header .= "<link type=\"text/css\" rel=\"stylesheet\" href=\"resources/datepickercontrol.css\">";
		$s_header .= "<script language='javascript'>";
		$s_header .= "DatePickerControl.onSelect = function(inputid) { ";
		$s_header .= "   window.location = '$reporturl&day_duration=$day_duration&stime=$stime&setdate=' + document.getElementById(inputid).value; ";
		$s_header .= "} ";
		$s_header .= "</script>";

		$set_date_start = $setdate;
		$date_start_ts = mktime(0,0,0,substr($set_date_start,5,2),substr($set_date_start,8,2),substr($set_date_start,0,4));

		// based on date format setting
		$date_setting = "date_format";
		$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
		if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
			$date_format = array("value" => 2);
		}else
			$date_format = mysqli_fetch_assoc( $location_date_format_query );

		switch ($date_format['value']){
			case 1:$show_date_format = date("d/m/Y",$date_start_ts);
			break;
			case 2:$show_date_format = date("m/d/Y",$date_start_ts);
			break;
			case 3:$show_date_format = date("Y/m/d",$date_start_ts);
			break;
		}
		$s_header .= "<input type=\"hidden\" id=\"DPC_TODAY_TEXT\" value=\"today\">";
		$s_header .= "<input type=\"hidden\" id=\"DPC_BUTTON_TITLE\" value=\"Open calendar...\">";
		$s_header .= "<input type=\"hidden\" id=\"DPC_MONTH_NAMES\" value=\"['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']\">";
		$s_header .= "<input type=\"hidden\" id=\"DPC_DAY_NAMES\" value=\"['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']\">";
		$s_header .= "<input type=\"text\" name=\"edit1\" id=\"edit1\" datepicker=\"true\" datepicker_format=\"YYYY-MM-DD\" value=\"".$show_date_format."\">";
		//echo "date: ".$date_start_ts."<br>";
		if($date_select_type=="Day Range")
		{
			$s_header .= "<select id=\"toDateSelect\" onchange='window.location = \"$reporturl&setdate=$setdate&stime=$stime&day_duration=\" + this.value'>";
			for($i=1; $i<=365; $i++)
			{
				$s_header .= "<option value='$i'";
				if($day_duration==$i) $s_header .= " selected";
				$s_header .= ">";
				switch ($date_format['value']){
					case 1:$s_header .= "to " . date("d/m/Y",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($i - 1),date("Y",$date_start_ts))) . " ($i day";
					break;
					case 2:$s_header .= "to " . date("m/d/Y",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($i - 1),date("Y",$date_start_ts))) . " ($i day";
					break;
					case 3:$s_header .= "to " . date("Y/m/d",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($i - 1),date("Y",$date_start_ts))) . " ($i day";
					break;
				}
				//$s_header .= "to " . date("m/d/Y",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($i - 1),date("Y",$date_start_ts))) . " ($i day";
				if($i > 1) $s_header .= "s";
				$s_header .= ")";
				$s_header .= "</option>";
			}
			$s_header .= "</select>";
		}
		else $day_duration = 1;

		$set_date_end = date("Y-m-d",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($day_duration - 1),date("Y",$date_start_ts)));
		$set_time_start = "00:00:00";
		$set_time_end = "24:00:00";

		$set_nextdate = date("Y-m-d",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($day_duration),date("Y",$date_start_ts)));
		$s_header .= "<input type='button' value='Next >>' onclick='window.location = \"$reporturl&setdate=$set_nextdate&stime=$stime&day_duration=$day_duration\"'>";

		/*$s_header .= " End of Day: <select name='stime' onchange='window.location = \"$reporturl&setdate=$setdate&day_duration=$day_duration&stime=\" + this.value'>";
		for($t=0; $t<=900; $t+=100)
		{
			$hour = (($t - ($t % 100)) / 100);
			$min = ($t % 100);
			if($min < 10) $min = "0" . $min;
			if($t==0) $show_time = "12:00am";
			else $show_time = $hour . ":" . $min . "am";
			$s_header .= "<option value='$t'";
			if($stime==$t) $s_header .= " selected";
			$s_header .= ">$show_time</option>";
		}
		$s_header .= "</select>";*/
	}

	$date_reports_by = "opened";
	$rep_time_query = mrpt_query("select * from `$maindb`.`report_parts` where `reportid`='[1]' and `id`='[2]'",$reportid,$dateid);
	if(mysqli_num_rows($rep_time_query))
	{
		preg_match("/poslavu_(.*)_db/", $dbname, $data_name);
		lavu_connect_dn($data_name[1],$dbname);
		$rep_time_read = mysqli_fetch_assoc($rep_time_query);
		//if(isset($_GET['testing'])) echo "<br>found report open/close: ".$rep_time_read['fieldname']."-".$date_select_type."<br>";
		if(($rep_time_read['fieldname']=="opened"||$rep_time_read['fieldname']=="closed") && ($date_select_type=="Year"||$date_select_type=="Month"||$date_select_type=="Day"||$date_select_type=="Day Range"||$date_select_type=="auto"))
		{
			if(isset($_GET['date_reports_by']))
			{
				$cfg_query = rpt_query("select * from `[1]`.`config` where `location`='[2]' and `type`='special' and `setting`='report config'",$dbname,$locationid);
				if(mysqli_num_rows($cfg_query))
				{
					$cfg_read = mysqli_fetch_assoc($cfg_query);
					lavu_query("update `[1]`.`config` set `value`='[2]' where `id`='[3]'",$dbname,$_GET['date_reports_by'],$cfg_read['id']);
				}
				else
					lavu_query("insert into `[1]`.`config` (`location`,`type`,`setting`,`value`) values ('[2]','special','report config','[3]')",$dbname,$locationid,$_GET['date_reports_by']);
			}

			$cfg_query = rpt_query("select * from `[1]`.`config` where `location`='[2]' and `type`='special' and `setting`='report config'",$dbname,$locationid);
			if(mysqli_num_rows($cfg_query))
			{
				$cfg_read = mysqli_fetch_assoc($cfg_query);
				$date_reports_by = ConnectionHub::getConn('rest')->escapeString($cfg_read['value']);
				if (isset($_GET['testing2'])) echo "date reports by: $date_reports_by\n<br />";
			}

			if($date_select_type!="auto")
			{
				$s_header .= " <select name='date_reports_by' onchange='window.location = \"$reporturl&date_reports_by=\" + this.value'>";
				$s_header .= "<option value='opened'".($date_reports_by=="opened"?" selected":"").">Report using orders OPENED on this date</option>";
				$s_header .= "<option value='closed'".($date_reports_by=="closed"?" selected":"").">Report using orders CLOSED on this date</option>";
				$s_header .= "</select>";
			}
		}
	}

	//
	$apply_stime = false;
	if($locationid)
	{
		preg_match("/poslavu_(.*)_db/", $dbname, $data_name);
		lavu_connect_dn($data_name[1],$dbname);
		$location_query = rpt_query("select * from `$dbname`.`locations` where `id`='[1]'",$locationid);
		if(mysqli_num_rows($location_query))
		{
			$location_read = mysqli_fetch_assoc($location_query);
			$stime = $location_read['day_start_end_time'];
			if(is_numeric($stime))
			{
				$stime_mins = $stime % 100;
				$stime_hours = ($stime - $stime_mins) / 100;
				if($stime_hours < 10) $stime_hours = "0".$stime_hours;
				if($stime_mins < 10) $stime_mins = "0".$stime_mins;
				$apply_stime = $stime_hours . ":" . $stime_mins . ":00";
			}
			else
			{
				$apply_stime = false;
			}
		}
	}

	// wrap it in a "header" tag (for processing later on)
	$output = $s_header;

	if($apply_stime)
	{
		$add_to_date_end = 1;
		if(($date_mode!="unified") || $explicit_date_end)
		{
			$add_to_date_end = 0;
		}
		$date_end_ts = mktime(0,0,0,substr($set_date_end,5,2),substr($set_date_end,8,2),substr($set_date_end,0,4));
		$set_date_end = date("Y-m-d",mktime(0,0,0,date("m",$date_end_ts),date("d",$date_end_ts) + $add_to_date_end,date("Y",$date_end_ts)));
		$set_time_start = $apply_stime;
		$set_time_end = $apply_stime;
	}
	if($date_selector)
	{
		echo $s_header;
		$ret = array();
		$ret['date_start'] = $set_date_start;
		$ret['time_start'] = $set_time_start;
		$ret['date_end'] = $set_date_end;
		$ret['time_end'] = $set_time_end;
		$ret['date_reports_by'] = $date_reports_by;
		$ret['locationid'] = $locationid;

		return $ret;
	}
	//if(isset($_GET['test'])) echo "STIME: $apply_stime ";

	//

	// reports that are actually a compilation of other reports
	// eg: reports --> general --> summary
	if($rep_type=="Combo")
	{
		if($report_read['title']=="Balance Summary")
		{
			$date_start = "$set_date_start $set_time_start";
			$date_end = "$set_date_end $set_time_end";

			//if(isset($_GET['testing1'])) echo "PROCESS FIX from $date_start to $date_end<br>";
			//process_fix_all($date_reports_by,$date_start,$date_end,$locationid);
		}

		echo $s_header;
		echo "<hr>";
		$rep_count = 0;
		$rep_query = mrpt_query("select `report_parts`.`prop1` as `prop1`, `report_parts`.`id` as `id`, `reports`.`title` as `title` from `$maindb`.`report_parts` LEFT JOIN `$maindb`.`reports` ON `report_parts`.`prop1`=`reports`.`id` where `report_parts`.`reportid`='$reportid' and `report_parts`.`type`='report' order by `_order` asc, id asc");
		while($rep_read = mysqli_fetch_assoc($rep_query))
		{
			if($rep_count > 0) echo "<hr>";
			$date_str = $set_date_start . "|" . $set_time_start . "|" . $set_date_end . "|" . $set_time_end;
			$debug_combo_report = false;//(isset($_GET['test']))?true:false;
			$use_report_id = $rep_read['prop1'];

			show_report($dbname,$maindb,"auto|".$date_mode."|".$date_str,$rurl,$use_report_id,$debug_combo_report,$locationid,true);
			$rep_count++;
		}
		return;

	// reports that are actually a compilation of other reports in some other weird way
	} else if($repeat_foreach!="" && !$use_subreport) {
		echo $s_header;
		echo "<hr>";
		$fe_parts = explode(".",$repeat_foreach);
		if(count($fe_parts)>1)
		{
			$fe_table = str_replace("`","",$fe_parts[0]);
			$fe_table = str_replace("'","",$fe_table);
			$fe_col = str_replace("`","",$fe_parts[1]);
			$fe_col = str_replace("'","",$fe_col);

			// get the row from report_parts containing the `date_table`.`date_column` to match dates to
			$where_code = "";
			$dt_query = mrpt_query("select * from `poslavu_MAIN_db`.`report_parts` where `id`='[1]'",$dateid);
			if(mysqli_num_rows($dt_query))
			{
				$dt_read = mysqli_fetch_assoc($dt_query);
				$date_start = "$set_date_start $set_time_start";
				$date_end = "$set_date_end $set_time_end";
				$date_col = "`".$dt_read['tablename'] . "`.`".$dt_read['fieldname']."`";
				$where_code = " where $date_col >= '$date_start' and $date_col <= '$date_end'";
			}

			if (substr($fe_table, 0, 1) == '-') {
				$where_code = '';
				$fe_table = substr($fe_table, 1);
			}
			preg_match("/poslavu_(.*)_db/", $dbname, $data_name);
			lavu_connect_dn($data_name[1],$dbname);
			$fe_query = rpt_query("select DISTINCT(`$fe_col`) as `fe_value` from `$dbname`.`$fe_table`".$where_code);
			while($fe_read = mysqli_fetch_assoc($fe_query))
			{
				$date_str = $set_date_start . "|" . $set_time_start . "|" . $set_date_end . "|" . $set_time_end;
				//echo "dbname: " . $dbname . "<br>maindb: " . $maindb . "<br>date_mode: " . $date_mode . "<br>date_str: " . $date_str . "<br>rurl: " . $rurl . "<br>reportid: " . $reportid . "<br>subreport: " . $fe_read['fe_value'] . "<br>locationid: " . $locationid . "<br>";
				show_report($dbname,$maindb,"auto|".$date_mode."|".$date_str,$rurl,$reportid,"subreport:".$fe_read['fe_value'],$locationid,false);
			}
		}
		return;
	}

	//

	// old position of apply_stime and date_selector snippets

	//

	if($set_unified=="Yes") $date_mode = "unified"; else if($set_unified=="No") $date_mode = "mixed";
	//Date Start and Date End are the fields that govern the time range that the reports should be pulled from.
	if($date_mode=="unified")
	{
		$date_start = "$set_date_start $set_time_start";
		$date_end = "$set_date_end $set_time_end";
	}
	else
	{
		$date_start = "$set_date_start";
		$date_end = "$set_date_end";
		$time_start = $set_time_start;
		$time_end = $set_time_end;
	}

//	error_log(__FILE__ . " " . __LINE__ . ": Date Start ~" .$date_start."~");
//	error_log(__FILE__ . " " . __LINE__ . ": Date End ~" .$date_end."~");
//	error_log(__FILE__ . " " . __LINE__ . ": Time Start ~" .$time_start."~");
//	error_log(__FILE__ . " " . __LINE__ . ": Time End ~" .$time_end."~");


	if(!isset($locationid))
	{
		/*if(isset(reqvar("loc_id")) $locationid = reqvar("loc_id");
		else if(isset(reqvar("locationid")) $locationid = reqvar("locationid");
		else*/ $locationid = 0;
	}

	$join_after = array();
	$tables_joined = array();
	$rep_query = mrpt_query("select * from `$maindb`.`report_parts` where `reportid`='$reportid' order by `_order` asc, `id` asc");
	$tableInfo=array();
	$valuesArray= array();
	$categoriesArray= array();
	$included_snames = array();
	//Added by Brian D. and Leif G.
	$specialFieldRows = array();
	//
	// normal reports (only have one report per report)
	while($rep_read = mysqli_fetch_assoc($rep_query))
	{

		$set_table = $rep_read['tablename'];
		$set_field = $rep_read['fieldname'];
		$set_prop1 = $rep_read['prop1'];
		$set_prop2 = $rep_read['prop2'];
		$set_prop3 = $rep_read['prop3'];
		$set_prop4 = $rep_read['prop4'];
		$set_type = $rep_read['type'];
		$set_operation = $rep_read['operation'];

		//LP-4463,in order table card_paid field refund details already calculated,here again calculating.
		if($set_field!='card_paid'){
		    $set_opargs = $rep_read['opargs'];
		}


		$set_filter = $rep_read['filter'];
		$set_label = $rep_read['label'];
		$use_graph = $rep_read['graph'];
		$use_branch = $rep_read['branch'];
		$use_monetary = $rep_read['monetary'];
		$use_sumop = $rep_read['sum'];
		$use_align = $rep_read['align'];
		$partid = $rep_read['id'];
		$hide = ($rep_read['hide']=="1")?true:false;



		if($use_branch=="1")
		{
			$show_label = $set_label;
			if($show_label=="") $show_label = $set_field;
			$sbranches[] = array($set_table,$set_field,$partid,$show_label);
		}

		if($primary_table=="")
		{
			$primary_table = $set_table;
			$tables_joined[] = $set_table;
		}
		else $tables_joined[] = $primary_table;

		if($set_type=="join")
		{
			$set_table_joined = false;
			$set_table2_joined = false;

			for($x=0; $x<count($tables_joined); $x++)
			{
				if($tables_joined[$x]==$set_table)
					$set_table_joined = true;
				if($tables_joined[$x]==$set_prop1)
					$set_table2_joined = true;
			}
			if($set_table_joined)
			{
				// Added because some time we required custom condition in join table
				if ($set_operation == 'custom' && $set_opargs != "" && $set_prop2 != "" && $set_field == "")
				{
					$fcode = "`$set_prop1`";
					$join_code .= " LEFT JOIN ".$fcode." ON $set_opargs = `$set_prop1`.`$set_prop2` ";
				} else if ($set_operation == 'custom' && $set_opargs != "" && $set_prop2 == "" && $set_field != "")
				{
					$fcode = "`$set_prop1`";
					$join_code .= " LEFT JOIN ".$fcode." ON `$set_table`.`$set_field` = $set_opargs ";
				}
				else
				{
					$set_prop_joined = false;
					for($x=0; $x<count($tables_joined); $x++)
					{
						if($tables_joined[$x]==$set_prop1)
							$set_prop_joined = true;
					}
					if(!empty($set_prop3) && !empty($set_prop4)){
						$fcode = $set_prop3 . "[DBNAME].". $set_prop4 ." `$set_prop1`";
					}
					else if($set_prop_joined)
					{
						$fcode = "`$set_prop1`";
					}
					else
					{
						$tables_joined[] = $set_prop1;
						$fcode = from_table($set_prop1,"[DBNAME]");
					}
					$join_code .= " LEFT JOIN ".$fcode." ON `$set_table`.`$set_field` = `$set_prop1`.`$set_prop2`";
				}
			}
			else if($set_table2_joined)
			{
				if ($set_operation == 'custom' && $set_opargs != "" && $set_prop2 != "" && $set_field == "")
				{
					$fcode = "`$set_table`";
					$join_code .= " LEFT JOIN ".$fcode." ON $set_opargs = `$set_prop1`.`$set_prop2` ";
				} else if ($set_operation == 'custom' && $set_opargs != "" && $set_prop2 == "" && $set_field != "")
				{
					$fcode = "`$set_table`";
					$join_code .= " LEFT JOIN ".$fcode." ON `$set_table`.`$set_field` = $set_opargs ";
				}
				else
				{

					$tables_joined[] = $set_table;
					$fcode = from_table($set_table,"[DBNAME]");
					$join_code .= " LEFT JOIN ".$fcode." ON `$set_prop1`.`$set_prop2` = `$set_table`.`$set_field`";
				}
			}
			else
			{
				//$tables_joined[] = $set_table;
				$fcode = from_table($set_table,"[DBNAME]");
				$join_after[$set_prop1] = " LEFT JOIN ".$fcode." ON `$set_prop1`.`$set_prop2` = `$set_table`.`$set_field`";
				/*if(isset($_GET['test']))
				{
					echo "<br>join after $set_prop1 - " . $join_after[$set_prop1] . "<br>";
				}*/
			}
			if(isset($join_after[$set_table]) && $join_after[$set_table]!="")
			{
				$tables_joined[] = $set_table;
				$join_code .= $join_after[$set_table];
				$join_after[$set_table] = "";
			}
		}
		else if($set_type=="select")
		{
			$sname = "`$set_table`.`$set_field`";
			if ((int)$reportid == 25 && $sname == "`super_groups`.`title`") {
				$sname = "if(super_groups.title != '', super_groups.title, 'No Super Group')";
			}
			if((int)$reportid == 131 && $sname == "`alternate_payment_totals`.`tip`"){
				$sname = "`orders`.`other_tip`";
			}

			if ($dbname == "poslavu_test_pizza_sho_db") error_log("sname: ".$sname);

			$alias_list[] = array($sname,"`field".$scount."`");

			$prime_sname = $sname;
			if($set_filter!="")
			{
				if($where_code!="") $where_code .= " and ";
				$set_filter = str_replace("'","''",$set_filter);
				$where_code .= ast_replace($sname,$set_filter);//str_replace("*",$sname,$set_filter);
			}
			if($dateid==$partid)
			{
				$date_sname = $sname;
				if($set_field=="opened" || $set_field=="closed")
				{
					$date_sname = "`$set_table`.`$date_reports_by`";
				}

				if($where_code!="") $where_code .= " and ";
				if(isset($_GET['testing'])) echo "date_sname: $date_sname - $date_reports_by<br>";
				if (((int)$reportid == 131 || (int)$reportid == 6) && (int)$showing_multi_report != 0) {
					$explodeStartDate = explode(" ", $date_start);
					$explodeEndDate = explode(" ", $date_end);
					$startDate = $explodeStartDate[0].'[startTime]';
					$endDate = $explodeEndDate[0].'[endTime]';
					$where_code .= "$date_sname >= '$startDate' and $date_sname <= '$endDate'";
				} else {
					$where_code .= "$date_sname >= '$date_start' and $date_sname <= '$date_end'";
				}
			}
			if($timeid==$partid)
			{
				if($where_code!="") $where_code .= " and ";
				$where_code .= "$sname >= '$time_start' and $sname <= '$time_end'";
			}
			if($locid==$partid && $showing_multi_report !== '-1')
			{
				if($where_code!="") $where_code .= " and ";
				$where_code .= "$sname = '$locationid'";
			}
			if($set_operation!="")
			{
				if($set_operation=="custom")
				{

					if (substr($set_opargs, 0, 12) == "[get_fields:") {

						//error_log("opargs: ".$set_opargs);
						$gf_parts = explode(":", trim($set_opargs, "[]"));
						//error_log(print_r($get_field_parts, true));

						if ($gf_parts[1] == "config") {
							preg_match("/poslavu_(.*)_db/", $dbname, $data_name);
							lavu_connect_dn($data_name[1],$dbname);
							$gf = rpt_query("SELECT `value`, `value2`, `value4`, `type` FROM `$dbname`.`config` WHERE `setting` = '[1]' AND `location` = '[2]' AND (`value10` * 1) > '2' AND `value11` = '[3]' ORDER BY `value10` ASC", $gf_parts[2], $locationid, $reportid);
							$gf_count = mysqli_num_rows($gf);

							$cnt = 0;
							while ($gf_info = mysqli_fetch_assoc($gf)) {
								//error_log(print_r($gf_info, true));

								$cnt++;
								$sname = "`$set_table`.`".$gf_info['value2']."`";
								$hide = false;
								$set_label = ucwords(str_replace("_", " ", $gf_info['value']));
								$use_monetary = "0";
								$use_sumop = "0";
								$use_graph = "0";
								$use_align = (($gf_info['value4']=="number")?"center":"left");
								$ssum[] = 0;

								if ($cnt < $gf_count) {

									if ($select_code!="")
										$select_code .= ",";
									$select_code .= "`$set_table`.`".$gf_info['value2']."` AS `field$scount`";
									$scount++;
									$sdisplay[] = !$hide;
									$stitles[] = $set_label;
									$smonetary[] = "0";
									$ssumop[] = "0";
									$sgraph[] = "0";
									$salign[] = $use_align;
									$ssum[] = 0;
								}
							}
						}

					} else if ($set_opargs!="")
						$sname = ast_replace($sname,$set_opargs);//str_replace("*",$sname,$set_opargs);

				}
				else
				{
					$sname = strtoupper($set_operation) . "(" . $sname;
					if($set_opargs!="")
					{
						$set_opargs = str_replace("'","''",$set_opargs);
						$sname .= "," . $set_opargs;
					}
					$sname .= ")";
				}
			}

			if($branchto!="")
			{
				if($branchto==$partid)
				{
					/*if($group_operation && $group_operation!="")
						$groupby = " group by " . ast_replace($prime_sname,$group_operation);//str_replace("*",$prime_sname,$group_operation);
					else*/
						$groupby = " group by `field$scount` ";
						$change_group_to = count($stitles);
						if(isset($_GET['test3'])) echo "CHANGE GROUP TO: $change_group_to<br>";
				}
				else if($groupid==$partid)
				{
					$change_group_from = count($stitles);
					if(isset($_GET['test3'])) echo "CHANGE GROUP FROM: $change_group_from<br>";
				}
			}
			else
			{
				if($groupid==$partid)
				{
					if($group_operation && $group_operation!="")
						$groupby = " group by " . ast_replace($prime_sname,$group_operation);//str_replace("*",$prime_sname,$group_operation);
					else
						$groupby = " group by `field$scount` ";
						$change_group_id = count($stitles);
				}
			}

			if($branch_col=="field".$scount)
			{
				$branch_fieldname = $sname;
			}

			$sdisplay[] = !$hide;
			if($set_label!="")
				$stitles[] = $set_label;
			else
				$stitles[] = $set_field;
			$smonetary[] = $use_monetary;
			$ssumop[] = $use_sumop;
			$sgraph[] = $use_graph;
			$salign[] = $use_align;
			$ssum[] = 0;

			if($select_code!="")
			$select_code .= ",";
			$select_code .= "$sname AS `field$scount`";
			$included_snames[] = $sname;

			// -1 is "all locations for this restaurant"
			if(($showing_multi_report && (!$b_draw_multi_locations || !$b_separate_locations)) && $showing_multi_report !== '-1')
			{
				if(!isset($fields_used)) $fields_used = array();
				if(!isset($fields_used[$set_field]))
				{
					$select_code .= ",$sname AS `$set_field`";
					$fields_used[$set_field] = 1;
				}
			}

			if($select_outer_code!="") $select_outer_code .= ",";
			if(strpos(strtolower($sname),"sum(")!==false || strpos(strtolower($sname),"count(")!==false)
				$select_outer_code .= "sum(`innerquery`.`field$scount`) AS `field$scount`";
			else
				$select_outer_code .= "`innerquery`.`field$scount` AS `field$scount`";

			if($orderby_id==$partid)
			{
				if($order_operation && $order_operation!="")
					$orderby = " order by " . ast_replace($prime_sname,$order_operation) . " " . $orderby_dir;
				else
					$orderby = " order by `field$scount` ".$orderby_dir;
			}
			$scount++;
		}
		//Added by Brian D. and Leif G.
		else if($set_type == 'special'){
			$specialFieldRows[] = $rep_read;
		}
		//
	}//End while loop that pulls report parts one at a time.
	if ((int)$reportid==6 || (int)$reportid==131 || (int)$reportid==7 || (int)$reportid==24) {
		$select_code .= ", sum(orders.cash_tip) AS `field$scount` ";
		$select_outer_code .= ", sum(`innerquery`.`field$scount`) AS `field$scount`";
	}
	if ((int)$reportid==131) {
		$select_outer_code .= ", sum(`innerquery`.`field$scount`) AS `field$scount`";
	}
	foreach($specialFieldRows as $currSpecialField){
		$stitles[] = $currSpecialField['label'];
		$scount++;
	}

	if($repeat_foreach!="" && $use_subreport)
	{
		$fe_parts = explode(".",$repeat_foreach);
		if(count($fe_parts)>1)
		{
			$fe_table = str_replace("`","",$fe_parts[0]);
			$fe_table = str_replace("'","",$fe_table);
			$fe_table = str_replace("-","",$fe_table);
			$fe_col = str_replace("`","",$fe_parts[1]);
			$fe_col = str_replace("'","",$fe_col);
			$fe_value = str_replace("'","''",$subreport);

			if($where_code!="") $where_code .= " and ";
			$where_code .= "`$fe_table`.`$fe_col` = '$fe_value'";
		}
	}
	if($branchto!="" && $branch_col!="")
	{
		$branch_title = str_replace("'","''",$branch_title);
		if($where_code!="") $where_code .= " and ";
		$where_code .= "$branch_fieldname = '$branch_title'";
	}
	$change_bank_report_tab = mrpt_query("SELECT `title` FROM `poslavu_MAIN_db`.`reports` WHERE `id` = '[1]'", $reportid);
	$changeBankTab = mysqli_fetch_assoc($change_bank_report_tab);
	$changeFlag = 0;
	if($changeBankTab['title'] == "Change Bank"){
		$changeFlag = 1;
		$where_code .= "`change_bank`.`created_date` >= '".$date_start."' AND `change_bank`.`created_date` <= '".$date_end."'ORDER BY `change_bank`.`created_date` ASC";
	}

	$bank_deposit_report_tab = mrpt_query("SELECT `title` FROM `poslavu_MAIN_db`.`reports` WHERE `id` = '[1]'", $reportid);
	$bankDepositTab = mysqli_fetch_assoc($bank_deposit_report_tab);
	$depoFlag = 0;
	if($bankDepositTab['title'] == "Bank Deposit"){
		$depoFlag= 1;
		$where_code .= "`bank_deposit`.`created_date` >= '".$date_start."' AND `bank_deposit`.`created_date` <= '".$date_end."'ORDER BY `bank_deposit`.`created_date` DESC";
	}
	if($where_code!=""){
			$where_code = "where ".$where_code;
	}

	if((int)$reportid == 44){
		$where_code .= " and `order_contents`.`combo_id`=0 and `order_contents`.`is_combo`=0 ";
	}

	if ((int)$reportid == 148){
		$where_code .= " and `order_contents`.`combo_id` != 1 and `order_contents`.`is_combo`=0 ";
	}

/* 	$process_ag_data = false;
	if(strpos($select_code . " " . $where_code,"ag_cash") || strpos($select_code . " " . $where_code,"ag_card"))
		$process_ag_data = true;
	if($process_ag_data)
	{
		lavu_select_db($dbname);
		if(isset($_GET['view_ag_process'])) echo "View AG process:<br>";
		process_ag_data($date_reports_by,$date_start,$date_end,$locationid);
	} */
	if(isset($_GET['explain']))
	{
		process_explain_mismatches($date_reports_by,$date_start,$date_end,$locationid);
	}
	// show reports from multiple locations
	// -1 is "all locations for this restaurant"
	if($showing_multi_report && $showing_multi_report !== '-1')
	{
		$inner_selects = "";
		$multi_db_list = array($dbname);

		if(count($multi_location_ids) > 0)
		{
			$multi_db_list = array();
			$multi_ids = $multi_location_ids;

			foreach($multi_ids as $n=>$v)
			{
				$mid = trim($multi_ids[$n]);
				$mrest_query = mrpt_query("select `data_name`,`id` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$mid);
				if(mysqli_num_rows($mrest_query))
				{
					$mrest_read = mysqli_fetch_assoc($mrest_query);
					$multi_db_list[] = "poslavu_" . $mrest_read['data_name'] . "_db";
				}
			}
			if(count($multi_db_list) < 1)
				$multi_db_list = array($dbname);

			$inner_selects = "";
			for($n=0; $n<count($multi_db_list); $n++)
			{
				$multi_dbname = $multi_db_list[$n];
				if (((int)$reportid == 131 || (int)$reportid == 6 ) && (int)$showing_multi_report != 0) {
					$dayStartEndTimeQuery = rpt_query("SELECT `day_start_end_time` FROM `$multi_dbname`.`locations` WHERE `id`='[1]'", $locationid);
					$timeValue = mysqli_fetch_assoc($dayStartEndTimeQuery);
					$stime = $timeValue['day_start_end_time'];
					if (is_numeric($stime)) {
						$stime_mins = $stime % 100;
						$stime_hours = ($stime - $stime_mins) / 100;
						if ($stime_hours < 10) {
							$stime_hours = "0".$stime_hours;
						}
						if ($stime_mins < 10) {
							$stime_mins = "0".$stime_mins;
						}
						$apply_stime = " ".$stime_hours . ":" . $stime_mins . ":00";
					}
				}

				if($select_code !="") {
					$sel1_code = "select '[DBNAME]' as `cc`, " . $select_code . " from ".from_table($primary_table,"[DBNAME]").$join_code.$where_code.$groupby;
				} else {
					$sel1_code = "select '[DBNAME]' as `cc` " . $select_code . " from ".from_table($primary_table,"[DBNAME]").$join_code.$where_code.$groupby;
				}
				$sel1_code = str_replace("[DBNAME]",$multi_dbname,$sel1_code);
				$sel1_code = str_replace("[startTime]",$apply_stime,$sel1_code);
				$sel1_code = str_replace("[endTime]",$apply_stime,$sel1_code);

				if($inner_selects!="")
					$inner_selects .= " UNION ALL ";
				$inner_selects .= "(" . $sel1_code . ")";
			}
			if($showing_multi_report){
				if($select_outer_code != "") {
					$query_start = "select `innerquery`.`cc` as `cc`," .$select_outer_code." from (".$inner_selects.") AS `innerquery`";
				} else {
					$query_start = "select `innerquery`.`cc` as `cc`" .$select_outer_code." from (".$inner_selects.") AS `innerquery`";
				}
				}
				if((int)$reportid==148){
					$selectQuery =" select count( distinct concat(`field16`,`field15`)) as totalCount from (".$inner_selects.") AS `innerquery`";
					$pages = getPaginationInfo($selectQuery, $_GET['ipp'], $_GET['page']);
				}

			else{
				$query_start = "select $select_outer_code from (".$inner_selects.") AS `innerquery`";
			}

		}

	}
	else
	{
		if((int)$reportid==148){
			$selectQuery =" select count( distinct concat(`order_contents`.`item_id`,`order_contents`.`options`)) as totalCount from ".from_table($primary_table,"[DBNAME]").$join_code.$where_code;
			$selectQuery = str_replace("[DBNAME]", $dbname, $selectQuery);
			$pages = getPaginationInfo($selectQuery, $_GET['ipp'], $_GET['page']);
		}
		if($primary_table) {
		$query_start = "select SQL_NO_CACHE $select_code from ".from_table($primary_table,"[DBNAME]").$join_code.$where_code;
		$query_start = str_replace("[DBNAME]",$dbname,$query_start);
		}
	}

	if (@$b_draw_multi_locations && @$b_separate_locations) // suppressing notice:undefined offset in error log
	{
		if($primary_table) {
		$query_start = "select SQL_NO_CACHE $select_code from ".from_table($primary_table,"[DBNAME]").$join_code.$where_code;
		$query_start = str_replace("[DBNAME]",$dbname,$query_start);
	}
	}

	if (isset($_GET['yourmom3']))
		echo $query_start;

	//---DEBUG---
	if (isset($_GET['yourmom1'])) echo "query start, -1 replacement step: $query_start\n<br />";
	if(isset($_GET['show_query8']))
		echo $query_start . "<br>";
	//$query_start = "select SQL_NO_CACHE $select_code from ".from_table($primary_table).$join_code.$where_code;
	$query_start = str_replace("(selected_date)",$setdate,$query_start);
	$set_stime = $stime;
	$stime_mins = $set_stime % 100;
	$stime_hours = ($set_stime - $stime_mins) / 100;
	if($stime_hours < 10) $stime_hours = "0" . $stime_hours;
	if($stime_mins < 10) $stime_mins = "0" . $stime_mins;
	$set_stime = $stime_hours . ":" . $stime_mins . ":00";
	$query_start = str_replace("(day_start_end)",$set_stime,$query_start);
	$groupby = str_replace("(day_start_end)",$set_stime,$groupby);
	$query_start = str_replace("(open_close)",$date_reports_by,$query_start);
	$groupby = str_replace("(open_close)",$date_reports_by,$groupby);
	$sdate_parts = explode("-",$setdate);
	if(count($sdate_parts) > 2)
	{
		$nextday = date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + 1,$sdate_parts[0]));
	}
	else $nextday = "";
	$query_start = str_replace("(next_day)",$nextday,$query_start);

	//Added by leif for special queries
//	error_log(__FILE__ . " " . __LINE__ . "Group By: " . $groupby);
	preg_match("~field\\d+~", $groupby, $groupByForSpecialQueries);
	$groupByForSpecialQueries = $groupByForSpecialQueries[0];
	//end of added code.

	if(isset($_GET['test2'])) echo "(selected_date) = $setdate<br>(next_day) = $nextday<br>(day_start_end) = $set_stime<br>";
	/*if($primary_table=="orders" || $primary_table=="schedule")
	{
		$second_query = str_replace("`schedule`","`schedule_archive`",str_replace("`orders`","`orders_archive`",$query_start));
		$query_start .= " UNION ALL " . $second_query;
		> '(selected_date) (day_start_end)' && < '(next_day) (day_start_end)'
	}*/
	$orderby='';

	if( $_GET['mode'] != "reports_id_124" && $_GET['mode'] != "reports_id_23" && $_GET['mode'] != 'reports_id_156' && $_GET['mode'] != 'reports_id_157' && $_GET['mode'] != 'reports_id_37' && $_GET['mode'] != 'reports_id_87' && $_GET['mode'] != "reports_id_135" && $_GET['mode'] != "reports_id_70" && $_GET['mode'] != "reports_id_68" )
	{
		if(isset($_GET['order_by_col']) && $_GET['order_by_col']!='' )
		{
			$orderby = " order by `" . $_GET['order_by_col'] . "` ".$_GET['ordermode']." ";
			if( $_GET['ordermode']=='asc')
				$_GET['ordermode']='desc';
			else
				$_GET['ordermode']= 'asc';


			/*
			if($orderby=="")
				$orderby = " order by `" . $_GET['order_by_col'] . "` * 1 asc ";
			else
				$orderby = "order by `" . $_GET['order_by_col'] . "` asc ";
				//$orderby = str_replace("order by","order by `" . $_GET['order_by_col'] . "` * 1 asc, `" . $_GET['order_by_col'] . "` asc,",$orderby);
			*/
		}
	}
	else
	{
		if(isset($_GET['reportId']))
		{
				if( $_GET['reportId'] == $reportid && isset($_GET['order_by_col']) && $_GET['order_by_col']!='')
				{
						$orderby = " order by `" . $_GET['order_by_col'] . "` ".$_GET['ordermode']." ";
						if( $_GET['ordermode']=='asc')
							$_GET['ordermode']='desc';
						else
							$_GET['ordermode']= 'asc';
				}
		}
		else if(isset($_GET['subreport']))
		{
                if( $_GET['subreport'] == $subreport && isset($_GET['order_by_col']) && $_GET['order_by_col']!='')
                {
                        $orderby = " order by `" . $_GET['order_by_col'] . "` ".$_GET['ordermode']." ";
                        if( $_GET['ordermode']=='asc')
                                $_GET['ordermode']='desc';
                        else
                                $_GET['ordermode']= 'asc';
                }
		}
		else
		{
			$orderby = "";
		}
	}

	for($n=0; $n<count($alias_list); $n++)
	{
		// -1 is "all locations for this account"
		if(($showing_multi_report && (!$b_draw_multi_locations || !$b_separate_locations)) && $showing_multi_report !== '-1')
		{
			$orderby = str_replace($alias_list[$n][0],$alias_list[$n][1],$orderby);
			$groupby = str_replace($alias_list[$n][0],$alias_list[$n][1],$groupby);
		}
	}

	if (isset($_GET['yourmom0']))
		echo "start: $query_start\n<br />group by: $groupby\n<br />order by: $orderby\n<br />";

	if((int)$reportid==148){
		if($export) {
		   $query = $query_start . $groupby.$orderby;
		} else {
			$query = $query_start . $groupby.$orderby.$pages->limit;
		}
	} else if((int)$reportid==44){
		$query = $query_start . $groupby;
	} else if (((int)$reportid == 131 || (int)$reportid == 6 ) && (int)$showing_multi_report != 0) {
		$query = $query_start . " group by `server` ". $orderby;
	} else {
		$query = $query_start . $groupby.$orderby;
	}

	if(isset($_GET['test44'])) echo $query . "<br><br>";

	if($return_query)
	{
		for($i = ($scount-1); $i >=0; $i--)
		{
			$query = str_replace('field'.$i, $stitles[$i], $query);
		}
		return $query;
	}
	//////////////////////////////////////////////////////
	// draw the table header row
	if($change_group_from=="") $change_group_from = 999999;
	if($debug) $s_body .= $query . "<br><br>";
	lavu_select_db($dbname);
	//$table_class = (lavuDeveloperStatus()) ? "class='report'" : "";
	if($changeFlag==1){
	    $s_body.= "<table><tr><td colspan='5' style='text-align: left;'><b>Final Amount in Change Bank</b></td></tr>";
	    $s_body.= "<tr><td colspan='5'  style='text-align: left;'>Total Variance over day</td></tr></table>";
	}
	$table_class = "";
	$s_body .= "<table cellpadding=4 {$table_class} width='50%' id=\"reportTable\"><tbody id=\"reportTableBody\">";
	$s_body .= "<tr>";

	//Added by Brian D and Leif G.
	// 1.) Run special queries uzsing str_replace for templates
	foreach($specialFieldRows as $currSpecialFieldReportPart){
		$stitles[] = $currSpecialFieldReportPart["label"];
		$sdisplay[] = "1";
	}
	// Customized array because we are adding two new columns for "Sales by Item with Mods" report
	if((int)$reportid == 44) {
		array_splice($ssumop, 18, 0, '0');
		array_splice($ssumop, 19, 0, '0');
		array_splice($smonetary, 18, 0, '0');
		array_splice($smonetary, 19, 0, '0');
		array_splice($sdisplay, 18, 0, '1');
		array_splice($sdisplay, 19, 0, '1');
		array_splice($stitles, 18, 0, 'Product Code');
		array_splice($stitles, 19, 0, 'Tax Code');
	}
	if((int)$reportid == 48){
		$firstPartArray = array_slice($stitles, 0, 6);
		$secondPartArray = array_slice($stitles, 6);
		$stitles = array_merge($firstPartArray, array('Product Code'), array('Tax Code'),  $secondPartArray);
		array_splice($sdisplay, 8, 0, '1');
		array_splice($sdisplay, 9, 0, '1');
		array_splice($smonetary, 7, 0, '0');
		array_splice($smonetary, 8, 0, '0');
		array_splice($smonetary, 10, 0, '1');
	}
	if((int)$reportid == 69){
		$firstPartArray = array_slice($stitles, 0, 7);
		$secondPartArray = array_slice($stitles, 7);
		$stitles = array_merge($firstPartArray, array('Product Code'), array('Tax Code'),  $secondPartArray);
		array_splice($sdisplay, 9, 0, '1');
		array_splice($sdisplay, 10, 0, '1');
		array_splice($smonetary, 8, 0, '0');
		array_splice($smonetary, 9, 0, '0');
	}

	// based on date format setting
	$date_setting = "date_format";
	$location_date_format_query = rpt_query( "SELECT `location`, `setting`, `value` FROM `config` WHERE `setting` = '[1]'",  $date_setting);
	if( !$location_date_format_query || !mysqli_num_rows($location_date_format_query)){
		$date_format = array("value" => 2);
	}else
		$date_format = mysqli_fetch_assoc( $location_date_format_query );

	if($report_read['title'] == 'Change Bank' && $changeFlag == 1){
		array_splice($smonetary, 4, 0, '1');
	}
	if($report_read['title'] == 'Bank Deposit' && $depoFlag == 1){
		array_splice($smonetary, 1, 0, '1');
		array_splice($smonetary, 5, 0, '1');
	}
	$skip = 0;
	for($i=0; $i<count($stitles); $i++)
	{
		if( @$sgraph[$i] && @$sdisplay[$i]){ // suppressing notice:undefined offset in error log
			if(isset($_GET['nialls_test']))
				echo print_r($sdisplay['i'],1);

			array_push($categoriesArray, $stitles[$i]);
		}
		if($sdisplay[$i])
		{
			$s_body .= "<th syle='border-bottom:solid 1px black;' align='center'";
			if(isset($_GET['mode']))
			{
				$query_str = $_SERVER['QUERY_STRING'];
 				if($stitles[$i] == "Product Code" || $stitles[$i] == "Tax Code"){
					$s_body .= "";
					$skip++;
				}else{
					$key = $i;
					if( $skip !== 0 )
					{
						$key -= $skip;
					}
					if( $_GET['mode'] == "reports_id_124" || $_GET['mode'] == "reports_id_23" || $_GET['mode'] == "reports_id_70" || $_GET['mode'] == "reports_id_68" )
					{
							if(isset($_GET['ordermode']))
							{
								$s_body .= " onclick='window.location = \"?{$query_str}&reportId=".$reportid."&ordermode=".$_GET['ordermode']."&order_by_col=field".$key."\"'";
							} else {
								$s_body .= " onclick='window.location = \"?{$query_str}&reportId=".$reportid."&ordermode=asc&order_by_col=field".$key."\"'";
							}
					}
					else if($_GET['mode'] == "reports_id_37" || $_GET['mode'] == "reports_id_87" || $_GET['mode'] == "reports_id_135")
					{
						if(isset($_GET['ordermode']))
						{
							$s_body .= " onclick='window.location = \"?{$query_str}&subreport=".$subreport."&ordermode=".$_GET['ordermode']."&order_by_col=field".$key."\"'";
						}
						else
						{
							$s_body .= " onclick='window.location = \"?{$query_str}&subreport=".$subreport."&ordermode=asc&order_by_col=field".$key."\"'";
						}
					}
                    else
                    {
						if( isset($_GET['ordermode'])) {
							$s_body .= " onclick='window.location = \"?{$query_str}&ordermode=".$_GET['ordermode']."&order_by_col=field".$key."\"'";
						} else {
							$s_body .= " onclick='window.location = \"?{$query_str}&ordermode=asc&order_by_col=field".$key."\"'";
						}
                    }
				}
			}
			$s_body .= "><b><font style='cursor:s-resize;'>";
			if($change_group_from == $i)
			{
				$s_body .= $stitles[$change_group_to];
			}else if($stitles[$i] == "Opened (DD/MM/YY)"){
				$split_header = explode(" ", $stitles[$i]);
				$change_header = explode("/", $split_header[1]);
				switch ($date_format['value']){
					case 1:$stitles[$i] = $split_header[0]." (".trim($change_header[0], "()")."/".trim($change_header[1], "()")."/".trim($change_header[2], "()").")";
					break;
					case 2:
					$stitles[$i] = $split_header[0]." (".trim($change_header[1], "()")."/".trim($change_header[0], "()")."/".trim($change_header[2], "()").")";
					break;
					case 3:
					$stitles[$i] = $split_header[0]." (".trim($change_header[2], "()")."/".trim($change_header[1], "()")."/".trim($change_header[0], "()").")";
					break;
					}
					$s_body .= $stitles[$i];
			}
			else
			{
				$s_body .= $stitles[$i];
			}
			$s_body .= "</font></b></th>";
			$export_output .= $stitles[$i] . $export_col;
		}
	}
	//if(isset($_GET['nialls_test']))
	//	echo "The stitles are: ".print_r($categoriesArray,1);
	if(trim($extra_functions)!="" && $show_extra_functions)
	{
		//extra_functions
		require_once(dirname(__FILE__) . "/report_extra_functions.php");

		$extra_function_list = explode(",",$extra_functions);
		for($x=0; $x<count($extra_function_list); $x++)
		{
			$function_name = trim($extra_function_list[$x]);

			$s_body .= "<td syle='border-bottom:solid 1px black;' align='center'><b><font>";
			$s_body .= run_extra_report_function($function_name,false,false,"title");
			$s_body .= "</td>";
		}
	}
	$s_body .= "</tr>";
	$graph_vals = array();

	// debug
	if( isset($_GET['yourmom'])){
		echo "Dataname:".$cr_dataname."<br>";
		//echo $query;
	}

	if(strpos($cr_dataname, 'green_beans_co') !== false){
		//echo "<br>Is a green_beans account... performing query string replace.<br>";
		$query = preg_replace("/and `orders`.`location_id` = '[0-9]'/", '', $query, -1);
		$query = preg_replace("/and `cc_transactions`.`loc_id` = '[0-9]'/", '', $query, -1);

		//$query = preg_replace("/`orders`.`location_id` = `cc_transactions`.`loc_id` OR /", '', $query, -1);
		$rep = 'and (`orders`.`location_id` = `cc_transactions`.`loc_id` OR `cc_transactions`.`order_id` = "Paid In" OR `cc_transactions`.`order_id` = "Paid Out")';
		$query = str_replace($rep,'',$query);

		$query = preg_replace("/and `order_contents`.`loc_id` = `orders`.`location_id`/", '', $query, -1);
		$query = preg_replace("/`cash_data`.`loc_id` = '[0-9]' and /", '', $query, -1);
		$query = preg_replace("/`orders`.`location_id` = '[0-9]'.*?and/", '', $query, -1);
		//echo "<br><br>Replacement Query:<br>".$query."<br><br>";
	}

	$mode = $_GET['mode'];
	$report_array = explode("_", $mode);
	$report_query = mrpt_query("select * from `reports` where `id`='[1]'",$report_array[2]);
	$current_report_read = mysqli_fetch_assoc($report_query);
	$start_date = explode(" ", $date_start);
	$end_date = explode(" ", $date_end);
	$date_selected = $_SESSION['rselector_setdate'];
	if("Cash Sales" == $current_report_read['title'] && $date_selected != ''){
		// lp-2507
		$s_body .= "<tr>";
		$s_body .= "<td colspan='$scount' align='center'><b>Date</b></td>";
		$s_body .= "<td>&nbsp;</td>";
		$s_body .= "<td colspan='$scount' align='center'><b>Cash Sales</b></td>";
		$s_body .= "</tr>";
		$sales_query = rpt_query("SELECT date(datetime) as saleDate, if(count(*) > 0, sum( IF(cc_transactions.action='Refund', -1*cc_transactions.total_collected, cc_transactions.total_collected) ), '0.00') as sales_total_collected FROM cc_transactions WHERE pay_type = 'Cash' and voided = 0 and `datetime` >= '".$date_start."' and `datetime` < '".$date_end."' and cc_transactions.voided = '0' and cc_transactions.action != 'Void' and cc_transactions.action NOT LIKE '%issue%' and cc_transactions.action NOT LIKE '%reload%' and cc_transactions.process_data NOT LIKE 'LOCT%' and cc_transactions.action != '' and cc_transactions.loc_id='1' group by date(datetime)");
		$rowsCount=mysqli_num_rows($sales_query);
		if ($rowsCount > 0) {
			while ($sales_total_collected = mysqli_fetch_assoc($sales_query)) {
				$saleDate = $sales_total_collected['saleDate'];
				$cashAmount = $sales_total_collected['sales_total_collected'];
				$show_colval = display_money($cashAmount, $location_read);
				$change_format = explode("-", $saleDate);
				switch ($date_format['value']) {
					case 1:$saleDate = $change_format[2]."-".$change_format[1]."-".$change_format[0];
					break;
					case 2:$saleDate = $change_format[1]."-".$change_format[2]."-".$change_format[0];
					break;
					case 3:$saleDate = $change_format[0]."-".$change_format[1]."-".$change_format[2];
					break;
				}
				$s_body .= "<tr>";
				$s_body .= "<td align='center'>".$saleDate."</td>";
				$s_body .= "<td align='center'>&nbsp;</td>";
				$s_body .= "<td align='center'>".$show_colval."</td>";
				$s_body .= "</tr>";
			}
		} else {
			$s_body .= "<tr>";
			$s_body .= "<td colspan='3' align='center'>No Record Found</td>";
			$s_body .= "</tr>";
		}
		// lp-2507 end
	}
	// must use mlavu query if accessing more than just the current account
	// -1 is "all locations for this restaurant"
	// here we will start sending the reports V1 to the replica, this file is the
	// one I have verified that is executed when using reports version 1
	if($showing_multi_report === FALSE || $showing_multi_report !== '-1')
	{
		$result_query = rpt_query($query);
	}
	if ($result_query === FALSE)
	{
		$result_query = mrpt_query($query); // use_mlavu
	}

	// debug
	if(isset($_GET['show_query8']))
	{
		echo "num rows: " . mysqli_num_rows($result_query) . "<br>";
		echo "error: " . mlavu_dberror() . "<br>";
	}

	//Added by Brian D. and Leif G.
	//Tailor the query to the current report.
	$specialParts = setBranchingVarsForSpecialReportParts($reportid, @$currSpecialFieldReportPart, $groupByForSpecialQueries); // suppressing notice:undefined offset in error log
	//Build the query rows for normal queries and put them in an array.
	$fullyBuiltQueryResultRowsArray = buildOriginalQueryArray($result_query);
	if(isset($_GET['order_by_col']) && $_GET['order_by_col']!=''){
		usort($fullyBuiltQueryResultRowsArray, 'reorder' );
	}
	//Modify Action Log result
	if ((int)$reportid == 30) {
		$fullyBuiltQueryResultRowsArray = modifyActionLogReports($fullyBuiltQueryResultRowsArray);
	}
	//TODO If statement to determine if cash tips are eod vs per-item and then call different fields holding different queries?
	//Perform the special query to obtain the results of the special query.

	$fullyBuiltQueryResultRowsArray = performSpecialQueryPullSpecialResultsAndAppendToOriginalQueryResult($fullyBuiltQueryResultRowsArray, $specialFieldRows, $reportid, $date_start, $date_end, $specialParts, $dbname);
	if((int)$reportid == 48 or (int)$reportid == 69) {
		$fullyBuiltQueryResultRowsArray = getProductAndTaxCodeModifierUsage($fullyBuiltQueryResultRowsArray, $dbname, $showing_multi_report, $reportid);
	}
	if((int)$reportid == 44){
		$fullyBuiltQueryResultRowsArray = getProductAndTaxCodeSalesByItem($fullyBuiltQueryResultRowsArray, $dbname, $showing_multi_report);
	}
	//echo "<pre>"; print_r($stitles); echo "</pre>";
	$smonetary[] = "1";     //This will only work for one special query so if you're implementing multiple queries you'll have to deal with changing this.
	$ssumop[] = "1";        //This will only work for one special query so if you're implementing multiple queries you'll have to deal with changing this.
	$salign[] = "center";   //This will only work for one special query so if you're implementing multiple queries you'll have to deal with changing this.
	///End adding stuff...
	array_splice($salign, 8, 0, 'center');
	array_splice($salign, 9, 0, 'center');
	$reportQuery = mrpt_query("SELECT `title` FROM `reports` WHERE `id` = '[1]'", $reportid);
	$reportRead = mysqli_fetch_assoc($reportQuery);
	if (trim($reportRead['title']) == 'Server Sales w/Tips') {
		array_splice($salign, 13, 0, 'left');
	}
	$change_sum = 0;
	$deposit_sum = 0;
	$lavu_sum = 0;
	$recentChangeBankTime="";
	$changeCount = mysqli_num_rows($result_query);
	if($result_query !== FALSE && $result_query !== NULL && mysqli_num_rows($result_query))
	{

//		while($result_read = mysqli_fetch_assoc($result_query))
//		{
		foreach($fullyBuiltQueryResultRowsArray as $result_read){
			$export_output = substr($export_output, 0, -1).$export_row;
			$s_body .= "<tr>";
			if((int)$reportid == 48 or (int)$reportid == 69 or (int)$reportid == 44) {
				$scount = count($result_read);
			}


			for($i=0; $i<$scount; $i++)
			{
				if($i == 4 && $changeFlag == 1){
					$change_sum += $result_read['field'.$i];
					$firstChangeEntry = $fullyBuiltQueryResultRowsArray[0]['field'.$i];
					$lastChangeEntry= $fullyBuiltQueryResultRowsArray[($changeCount - 1)]['field'.$i];
				}
				if($i == 1 && $depoFlag == 1){
					$deposit_sum += $result_read['field'.$i];
				}
				if($i == 5 && $depoFlag == 1){
					$lavu_sum += $result_read['field'.$i];
				}
				// Adding hardcoded becuase need to overide existing value without affecting other report
				// At the time of order processing inserting in `order_content`.'after_discount' value without
				// deducting included tax
				if ($i == 23 && (int)$reportid == 148) {
					$result_read['field'.$i] = $result_read['field21'] - $result_read['field22'];
				}
				if ($i == 24 && (int)$reportid == 44) {
					$result_read['field'.$i] = $result_read['field22'] - $result_read['field23'];
				}
				if($sdisplay[$i])
				{
					// get the value of this column
					if($change_group_from==$i)
					{
						$show_colval = @$result_read['field'.$change_group_to];
					}
					else
					{
						$show_colval = @$result_read['field'.$i];
//						error_log(__FILE__ . " " . __LINE__ . " Show_Colval: " . $show_colval);
					}

					if($reportid == 150){
                      if(is_numeric($show_colval)){
                       $ex_colval = "` ".$show_colval;
                      }
                      else{
                      	$ex_colval = $show_colval;
                      }

					}
					else{
					  $ex_colval = $show_colval;
					  $ex_colval = preg_replace("/[\n\t]/", "", $ex_colval);
					}

					if ($smonetary[$i] == "1") {

						$ex_colval = (string)display_money($show_colval, $location_read, "");
						$show_colval = display_money($show_colval, $location_read);
					} else if( is_numeric( $show_colval ) ){
						$disable_decimal = isset($location_read['disable_decimal'])?$location_read['disable_decimal']:2;

						if($reportid == 150){
						  $show_colval = $show_colval;
						}
						else{
						 $show_colval = round($show_colval*1, $disable_decimal );
						}


					}

					if( $export_ext == 'csv' && (strstr($ex_colval, '"') || strstr($ex_colval, "\n") || strstr($ex_colval, "\r") || strstr($ex_colval, ","))){
						$ex_colval = str_ireplace('"', "\\\"", $ex_colval);
						$ex_colval = "\"{$ex_colval}\"";
					}

					if( @$sgraph[$i]){ // suppressing notice:undefined offset in error log
						if ($b_draw_pretty_graphs) {
							$s_tableinfo = str_replace(array($location_read['monitary_symbol'], ' '), '', $ex_colval);
							$s_tableinfo = str_replace(array($location_read['decimal_char']), '.', $s_tableinfo);
							if (preg_match('/-?[0-9]+\.[0-9]+/', $s_tableinfo) === 1)
								$s_tableinfo = (float)$s_tableinfo;
							else if (preg_match('/-?[0-9]+/', $s_tableinfo) === 1)
								$s_tableinfo = (int)$s_tableinfo;
						} else {
							$s_tableinfo = str_replace(array($location_read['monitary_symbol'], ' ', $location_read['decimal_char']), '', $ex_colval);
						}
						array_push ($tableInfo, $s_tableinfo);
					}

					if( $stitles[$i]=="Opened" || $stitles[$i]=="Closed" || $stitles[$i]=="Order Opened"
							|| $stitles[$i]=="Order Closed" || $stitles[$i]=="Local Time"
							|| $stitles[$i]=="Server Time" || $stitles[$i]=="Reopened"
							|| $stitles[$i]=="Reclosed" || $stitles[$i]=="Void Time"
							|| $stitles[$i]=="Created" || $stitles[$i]=="Last Activity" || $stitles[$i] == "Date/Time" || $stitles[$i]=="Merge Time"){
						if(preg_match('/^[a-zA-Z .]+$/', $ex_colval)){
							$ex_colval = $ex_colval;
						}else{
							$split_date = explode(" ", $ex_colval);
							$change_format = explode("-", $split_date[0]);
							switch ($date_format['value']){
								case 1:$ex_colval = @$change_format[2]."-".@$change_format[1]."-".@$change_format[0]." ".@$split_date[1];
								break;
								case 2:$ex_colval = @$change_format[1]."-".@$change_format[2]."-".@$change_format[0]." ".@$split_date[1];
								break;
								case 3:$ex_colval = @$change_format[0]."-".@$change_format[1]."-".@$change_format[2]." ".@$split_date[1];
								break;
							}
						}
					}else if( $stitles[$i] == "Opened (DD/MM/YY)" || $stitles[$i] == "Opened (YY/MM/DD)" || $stitles[$i] == "Opened (MM/DD/YY)" ){
						$split_date = explode(" ", $ex_colval);
						$change_format = explode("/", $split_date[0]);
						switch ($date_format['value']){
							case 1:$ex_colval = $change_format[0]."/".$change_format[1]."/".$change_format[2]." ".$split_date[1]." ".$split_date[2];
							break;
							case 2:$ex_colval = $change_format[1]."/".$change_format[0]."/".$change_format[2]." ".$split_date[1]." ".$split_date[2];
							break;
							case 3:$ex_colval = $change_format[2]."/".$change_format[1]."/".$change_format[0]." ".$split_date[1]." ".$split_date[2];
							break;
						}
					}
					// export code
					$export_output .= $ex_colval . $export_col;

					// add it to the body
					$s_body .= "<td";
					if((($stitles[$i]=="order_id" || strtolower($stitles[$i])=="order id")) && ($result_read['field'.$i] != "") && (substr($result_read['field'.$i], 0, 4) != "Paid") && ($result_read['field'.$i] != "0"))
					{
					    $dataName = (isset($result_read['cc'])) ? $result_read['cc'] :"";
					    $s_body .= " style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999; color:#000088;' onclick='showWhiteBox(\"Order ID: ".$result_read['field'.$i]."<br><iframe src=\\\"index.php?show_page=reports/order_details&order_id=".$result_read['field'.$i]."&datanameGrp=".$dataName."\\\" width=\\\"100%\\\" height=\\\"100%\\\" frameborder=\\\"0\\\"></iframe>\")'";
					}else{
						$s_body .= " style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999'";
					}
					$s_body .= " align='".$salign[$i]."'>";
					if (($show_colval=="") || ((($stitles[$i]=="order_id") || strtolower($stitles[$i])=="order id") && (($result_read['field'.$i] == "") || ($result_read['field'.$i] == "0"))))
						$s_body .= "&nbsp;";
					else if( $stitles[$i]=="Opened" || $stitles[$i]=="Closed" || $stitles[$i]=="closed" || $stitles[$i]=="datetime" || $stitles[$i]=="Order Opened"
							|| $stitles[$i]=="Order Closed" || $stitles[$i]=="Local Time"
							|| $stitles[$i]=="Server Time" || $stitles[$i]=="Reopened"
							|| $stitles[$i]=="Reclosed" || $stitles[$i]=="Void Time"
							|| $stitles[$i]=="Created" || $stitles[$i]=="Last Activity" || $stitles[$i] == "Date/Time" || $stitles[$i] == "time_out" || $stitles[$i]=="Merge Time"){
						if(preg_match('/^[a-zA-Z .]+$/', $show_colval)){
							$s_body .= $show_colval;
						}else{
							$split_date = explode(" ", $show_colval);
							$change_format = explode("-", $split_date[0]);
							switch ($date_format['value']){
								case 1:$show_colval = @$change_format[2]."-".@$change_format[1]."-".@$change_format[0]." ".@$split_date[1];
								break;
								case 2:$show_colval = @$change_format[1]."-".@$change_format[2]."-".@$change_format[0]." ".@$split_date[1];
								break;
								case 3:$show_colval = @$change_format[0]."-".@$change_format[1]."-".@$change_format[2]." ".@$split_date[1];
								break;
							}
							$recentChangeBankTime=$show_colval;
							$s_body .= $show_colval;
						}
					}else if( $stitles[$i] == "Opened (DD/MM/YY)" || $stitles[$i] == "Opened (YY/MM/DD)" || $stitles[$i] == "Opened (MM/DD/YY)" ){
						$split_date = explode(" ", $show_colval);
						$change_format = explode("/", $split_date[0]);
						switch ($date_format['value']){
							case 1:$show_colval = @$change_format[0]."/".@$change_format[1]."/".@$change_format[2]." ".@$split_date[1]." ".@$split_date[2];
							break;
							case 2:$show_colval = @$change_format[1]."/".@$change_format[0]."/".@$change_format[2]." ".@$split_date[1]." ".@$split_date[2];
							break;
							case 3:$show_colval = @$change_format[2]."/".@$change_format[1]."/".@$change_format[0]." ".@$split_date[1]." ".@$split_date[2];
							break;
						}
						$s_body .= $show_colval;
					}else
						$s_body .= $show_colval;
					$s_body .= "</td>";

					if(@$ssumop[$i]=="1" || @$ssumop[$i]=="2") // suppressing notice:undefined offset in error log
					{
						if(strpos(@$result_read['field'.$i],"$")!==false)
							$ssumop[$i] = "2";

						$addnum = @$result_read['field'.$i];
						if($location_read['decimal_char']==",")
						{
							$addnum = str_replace(",",".",$addnum);
						}
						else
						{
							$addnum = str_replace(",","",$addnum);
						}
						$addnum = str_replace("$","",$addnum);

						@$ssum[$i] = @$ssum[$i] * 1 + (@$addnum * 1); // suppressing notice:undefined offset in error log
						if(isset($_GET['poopies']))
						{
							echo "sum: + " . $result_read['field'.$i] . " = " . $ssum[$i] . "<br>";
						}
					}
					if(@$sgraph[$i]=="1") // suppressing notice:undefined offset in error log
					{
						$graph_vals[] = (str_replace("$","",str_replace(",","",$result_read['field'.$i])) * 1);
					}
				}
			}
			if(trim($extra_functions)!="" && $show_extra_functions)
			{
				//extra_functions
				require_once(dirname(__FILE__) . "/report_extra_functions.php");

				$extra_function_list = explode(",",$extra_functions);
				for($x=0; $x<count($extra_function_list); $x++)
				{
					$function_name = trim($extra_function_list[$x]);

					$s_body .= "<td style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999' align='center'>";
					$s_body .= run_extra_report_function($function_name,$result_read,$stitles);
					$s_body .= "</td>";
				}
			}

			if($change_group_id!="")
			{
				if($rep_type!="SubReport")
				{
					for($i=0; $i<count($sbranches); $i++)
					{
						$s_body .= "<td><a href='$reporturl&setdate=$setdate&stime=$stime&day_duration=$day_duration&branch=".$sbranches[$i][2]."&branch_title=".urlencode($result_read['field'.$change_group_id])."&branch_col=field".$change_group_id."'>(" . $sbranches[$i][3] . ")</a></td>";
					}
				}
			}
			$s_body .= "</tr>";
		}

		if($changeFlag == 1){//LP-4186
		    $lastRecord=$fullyBuiltQueryResultRowsArray[$changeCount-1];

		    $changeBankTop="Final Amount in Change Bank (".$recentChangeBankTime.") : ".display_money($lastRecord['field4'],$location_read);
		    $changeBankVariance='Total Variance over day(+/-): '.display_money(($lastChangeEntry - $firstChangeEntry), $location_read);

		    $s_body =str_replace("Final Amount in Change Bank",$changeBankTop,$s_body);
		    $s_body =str_replace("Total Variance over day",$changeBankVariance,$s_body);

		}
		$export_output = substr($export_output, 0, -1);
		$export_totals = "";
		for($i=0; $i<$scount; $i++)
		{
			if($sdisplay[$i])
			{
				if($export_totals != "")
				{
					$export_totals .= $export_col;
				}

				$s_body .= "<td";
				if (@$smonetary[$i] == "1" && @$ssumop[$i]=="1") { // suppressing notice:undefined offset in error log
					$s_body .= " align='".$salign[$i]."'><b>".display_money($ssum[$i], $location_read)."</b>";
					$temp = display_money($ssum[$i], $location_read);
					if($export_ext == "csv" && strstr($temp, ","))
						$export_totals .= '"' . $temp . '"';
					else
						$export_totals .= $temp;
				}

				else if(@$ssumop[$i]=="1") // suppressing notice:undefined offset in error log
				{
					$s_body .= " align='".$salign[$i]."'><b>".$ssum[$i]."</b>";
					// $export_totals .= $ssum[$i];
					$temp = $ssum[$i];
					if($export_ext == "csv" && strstr($temp, ","))
						$export_totals .= '"' . $temp . '"';
					else
						$export_totals .= $temp;
				}
				else if(@$ssumop[$i]=="2") // suppressing notice:undefined offset in error log
				{
					$s_body .= " align='".$salign[$i]."'><b>$".number_format($ssum[$i],2,".",",")."</b>";

					$temp = number_format($ssum[$i],2,".",",");
					if($export_ext == "csv" && strstr($temp, ","))
						$export_totals .= '"' . $temp . '"';
					else
						$export_totals .= $temp;
				}
				/* else if($i == 0 && $changeFlag == 1){
					$s_body .= " align='".$salign[$i]."'><b>Final amount entered in Change Bank: </b>";
				}
				else if($i == 4 && $changeFlag == 1){
					$s_body .= " align='".$salign[$i]."'><b>".display_money($change_sum, $location_read)."</b>";
				} */
				else if($i == 0 && $depoFlag == 1){
					$s_body .= " align='".$salign[$i]."'><b>Final amount deposited into Bank: </b>";
				}
				else if($i == 1 && $depoFlag == 1){
					$s_body .= " align='".$salign[$i]."'><b>".display_money($deposit_sum, $location_read)."</b>";
				}
				else if($i == 5 && $depoFlag == 1){
					$s_body .= " align='".$salign[$i]."'><b>".display_money($lavu_sum, $location_read)."</b>";
				}
				else
				{
					$s_body .= ">&nbsp;";
					$export_totals .= " ";
				}
				$s_body .= "</td>";
			}
		}
		$export_output .= $export_row . $export_totals;

	}
	$s_body .= "</tbody></table>";
	if($changeFlag == 1 && $changeCount>0){
	    $s_body .= '<br><div id="depositVal" style="padding:0px;margin:0px;">Total Variance over day(+/-): '.display_money(($firstChangeEntry - $lastChangeEntry), $location_read).'</div>';
	}
	if($depoFlag == 1 && $changeCount>0){
	    $s_body .= '<br><br><div id="depositVal" style="padding:0px;margin:0px;">Difference between the entered deposit and the Lavu calculated deposit(+/-): '.display_money(($lavu_sum - $deposit_sum), $location_read).'</div>';
	}
	$s_body .= '<div id="show_new_graph" style="padding:0px;margin:0px;"></div>';

	$output .= $s_body;

	// give the user the option to export this report
	if($rep_type!="SubReport")
	{

		if (count($tableInfo) > 0 && $b_draw_pretty_graphs) {
			$s_graph .= "<br /><div id='graph' style='width:600px;height:300px; margin:0 auto;'></div>";
			$s_chart_retval = chart($categoriesArray,$tableInfo);
			$s_graph .= $s_chart_retval;
			$s_graph .= "<br />";
		} else {

			if(count($graph_vals) > 0)
			{
				$graph_val_str = "";
				for($i=0; $i<count($graph_vals); $i++)
				{
					if($graph_val_str!="") $graph_val_str .= ",";
					$graph_val_str .= $graph_vals[$i];
				}
				$s_footer .= "<img src='resources/show_graph.php?width=480&height=360&data=".$graph_val_str."'>";
			}
		}

		if($showing_multi_report) $mrcode = "&multireport=".$showing_multi_report;
		else $mrcode = "";

		$query_str = $_SERVER['QUERY_STRING'];

		$s_footer .= "<br><br><a href='index.php?{$query_str}&reportid={$reportid}&export_type=txt&widget=reports/export";
		if(isset($_GET['order_by_col'])) $s_footer .= "&order_by_col=".$_GET['order_by_col'];
		$s_footer .= "' class='exportLink'>Export To Tab Delimited \".txt\" file</a>";

		$s_footer .= "<br><br><a href='index.php?{$query_str}&reportid={$reportid}&export_type=xls&widget=reports/export";
		if(isset($_GET['order_by_col'])) $s_footer .= "&order_by_col=".$_GET['order_by_col'];
		$s_footer .= "' class='exportLink'>Export To Tab Delimited \".xls\" file</a>";

		$s_footer .= "<br><br><a href='index.php?{$query_str}&reportid={$reportid}&export_type=csv&widget=reports/export";
		if(isset($_GET['order_by_col'])) $s_footer .= "&order_by_col=".$_GET['order_by_col'];
		$s_footer .= "' class='exportLink'>Export To Comma Delimited \".csv\" file</a>";
		$s_footer .= "<br><br><br><br>";
	}

	$output = $s_graph . $output;
	if((int)$reportid==148 && $pages->num_pages > 1){
		$output .= '<input type="hidden" name="total_pages" id="total_pages" value="'.$pages->num_pages.'"/><a href="' . $_SERVER['REQUEST_URI'].'" title="Next posts" rel="next" id="nextPage"><img src="images/kpr_activity.gif"/></a>';
	}
	$output .= $s_footer;

	$b_do_the_multi_report_thing = ($showing_multi_report && $showing_multi_report !== '-1' && count($multi_db_list) > 1 && !$b_second_part_of_multi_report && ($b_draw_multi_locations && $b_separate_locations));

	// export the report
	if($export) {
		export_report("report_".date("Y-m-d H:i").".".$export_ext,$export_output);
	} else {
		if ($b_second_part_of_multi_report || $b_do_the_multi_report_thing) {
			if( ! class_exists ("MAIN_DB_Interface")){

			}
			$a_locations= ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('locations', NULL, TRUE,
				array('databasename'=>$dbname, 'selectclause'=>'`title` AS `location_name`'));
			$s_location_name = $a_locations[0]['location_name'];
			$s_draw_dataname = (function_exists('is_lavu_admin') && is_lavu_admin()) ? "<font style='font-weight:bold;'>dataname: $cr_dataname</font> (only visible to lavu admins)<br />" : "";
			if (!$b_second_part_of_multi_report)
				echo "<br />$s_header<br /><br />
					<h2>$s_location_name</h2>$s_draw_dataname<br />
					$s_body<br />";
			else
				echo "<h2>$s_location_name</h2>$s_draw_dataname<br />
					$s_body<br />";
		} else {
			echo "<br>".$output;
		}
	}

	// draw the report for other restaurants in the chain reporting group
	// don't draw the header and footer
	if ($b_do_the_multi_report_thing) {
		foreach($multi_db_list as $s_database) {
			if ($dbname == $s_database)
				continue;
			//show_report($dbname,$maindb,$date_mode,$rurl,$reportid,$special=false,$set_locationid=false,$is_combo_rep=false,$return_query=false,$b_second_part_of_multi_report=FALSE);
			show_report($s_database,$maindb,$date_mode,$rurl,$reportid,$special,$set_locationid,$is_combo_rep,$return_query,TRUE);
		}
		echo $s_footer;
	}

	if((int)$reportid==148 && $pages->num_pages > 1 && !$export){

		echo <<<JAVASCRIPT
		<script type="text/javascript" src="scripts/jquery.infinitescroll.js"></script>
		<script type="text/javascript">
			(function ($){
					var page = 2;
					var noOfRecords = 100;
					var targetUrl = $('a#nextPage').attr('href');
					targetUrl = targetUrl.replace('index.php','ajax_show_report.php');
					targetUrl = updateQueryStringParameter(targetUrl, 'page', page);
					targetUrl = updateQueryStringParameter(targetUrl, 'ipp', noOfRecords);

					$('a#nextPage').attr('href', targetUrl);

					$('body').yofinity({
		                buffer: 500,
						debug: false,
						type: 'post',
		                navSelector: 'a#nextPage',
		                success: function (link, response, text){
							var returnedData = JSON.parse(link);

							var responseContent = returnedData.content;
							if (responseContent.indexOf('No Records found') > -1) {
		                         text.attr('href', 'error');
							} else {
								$('table#reportTable tr:last').remove();
								$('div#show_new_graph').html(returnedData.graph);
								$('#reportTableBody').append(responseContent);
								url = text.attr('href');
								key = 'page';
								value = page;
								newUrl = updateQueryStringParameter(url, key, value);
								newUrl = newUrl.replace('index.php','ajax_show_report.php');
		                        page++;
								targetUrl = updateQueryStringParameter(newUrl, 'page', page);
								text.attr('href', targetUrl);

								//update urls for exporting here
								$('a.exportLink').each(function(index, element){
									linkUrl = $(this).attr('href');
									key = 'ipp';
									value = noOfRecords*(page-1);
									newLinkUrl = updateQueryStringParameter(linkUrl, key, value);
									$(this).attr('href', newLinkUrl);
								});

		                    }

							if($('input#total_pages').val() < page ) {
								text.remove();
							}
		                },
		                error: function (link, response, text){
		                    response.remove();
		                }
		            });
				})(jQuery);


				function updateQueryStringParameter(uri, key, value) {
				  var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
				  if( value === undefined ) {
				  	if (uri.match(re)) {
						return uri.replace(re, '$1$2');
					} else {
						return uri;
					}
				  } else {
				  	if (uri.match(re)) {
				  		return uri.replace(re, '$1' + key + "=" + value + '$2');
					} else {
				     var hash =  '';
				     if( uri.indexOf('#') !== -1 ){
				        hash = uri.replace(/.*#/, '#');
				        uri = uri.replace(/#.*/, '');
				     }
					    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
						return uri + separator + key + "=" + value + hash;
					}
				  }
				}
		</script>
JAVASCRIPT;
	}

}
//Added by Brian D. and Leif G.
$specialParts = array();

//This should be the only function you'd need to edit to get a new special report working.
function setBranchingVarsForSpecialReportParts($reportid, $currSpecialFieldReportPart, $groupByForSpecialQueries){
//	error_log(__FILE__ . " " . __LINE__ . " Group by for special queries: " . print_r($groupByForSpecialQueries,1));
	global $specialParts;
	$specialParts = array();
	$specialParts['AS_IS'] = false;
	$specialParts['group_by_clause']= "";
	$specialParts['group_field_replace'] = "";
	$specialParts['group_by_field_key']= "";
	$specialParts['special_query_group_key'] = "";

	if($reportid == 24){ //Reports that don't need field replacements.
		$specialParts['AS_IS'] = true;
	}else {
		if (isset($_GET['branch_col'])) { //Branch link has been clicked; Not all reports will have this.
			$specialParts['group_by_clause'] = $currSpecialFieldReportPart["prop4"];
			if ($reportid == '131' || $reportid == '6') {
				$specialParts['group_field_replace'] = '`server_id`';
				$specialParts['special_query_group_key'] = "register_name";
			}
		} else { //Branch link is not clicked
			$specialParts['group_by_clause'] = $currSpecialFieldReportPart["prop3"];
			if ($reportid == '131' || $reportid == '6') {
				$specialParts['group_field_replace'] = '`server_id`';
				$specialParts['special_query_group_key'] = "server_id";
			} else if ($reportid == '7') {
				$specialParts['group_field_replace'] = '`cashier_id`';
				$specialParts['special_query_group_key'] = "cashier_id";
			}
		}
	}
	$specialParts['group_by_field_key'] = $groupByForSpecialQueries;
	return $specialParts;
}

/*Builds the original Query Array which holds queries for each row of the report*/
function buildOriginalQueryArray($result_query){
	$fullyBuiltQueryResultRows = array();
	while($currFinalResultRow = mysqli_fetch_assoc($result_query)){
		$fullyBuiltQueryResultRows[] = $currFinalResultRow;
	}
	return $fullyBuiltQueryResultRows;
}



/*Loops over the 'special' report parts, refines the query for that report part, then calls the query.
	The function then creates */
function performSpecialQueryPullSpecialResultsAndAppendToOriginalQueryResult($fullyBuiltQueryResultRows, $specialFieldRows, $reportid, $date_start, $date_end, $specialParts, $dbname){
	$isDailyTotalQuery = "SELECT `value` from `".$dbname."`.`config` WHERE `setting`='cash_tips_as_daily_total'";
	$isDailyTotal = rpt_query($isDailyTotalQuery);
	$isDailyTotal = mysqli_fetch_assoc($isDailyTotal);
	global $specialParts;

	$specialParts['dbname'] = $dbname;

	if($isDailyTotal['value'] == '1') {
		$isDailyTotalMode = true;
	}else{
		$isDailyTotalMode = false;
	}

	foreach($specialFieldRows as $currSpecialFieldReportPart){
		$specialQuery = refineSpecialDataQuery($currSpecialFieldReportPart["prop1"], $reportid, $date_start, $date_end, $specialParts, $isDailyTotalMode);
		$specialResultArr = createArrayFromQueryResult(rpt_query($specialQuery));
		//error_log(__FILE__ . " " . __LINE__ ." Special Query Results: " . print_r($specialResultArr,1));
		foreach($specialResultArr as $currSpecialResultRow){
			$fullyBuiltQueryResultRows = appendCurrentSpecialResultRowValueToOriginalQueryResult($fullyBuiltQueryResultRows, $currSpecialResultRow, $specialParts, $reportid);
		}
	}
	return $fullyBuiltQueryResultRows;
}
/*refineSpecialDataQuery appends the where clause to the end of the select statement, so it is not needed in the report_parts database.*/
function refineSpecialDataQuery($qry, $report_id, $start_time, $end_time, $specialParts, $isDailyTotal) {
	global $specialParts;
	$specialParts['datewhere'] = " WHERE `orders`.`closed` >= '$start_time' AND `orders`.`closed` <= '$end_time'";
	$qry .= $specialParts['datewhere'];

	if ($isDailyTotal) {
		$specialParts['777'] = " AND `orders`.`order_id` LIKE '777%'";
		$qry .= $specialParts['777'];
	} else {
		$specialParts['777'] = " AND `orders`.`order_id` NOT LIKE '777%'";
		$qry .= $specialParts['777'];
	}

	//Add to the switch statement for each new report using special fields
	switch ($report_id) {
		case '6':
		case '7':
		case '131':
			$qry = str_replace('[group_field]',$specialParts['group_field_replace'], $qry);
			break;
		case '24':
			break;
		default:
			break;
	}
	if (!$specialParts['AS_IS']) {
		$qry .= " " . $specialParts['group_by_clause'];
	}
//	error_log(__FILE__ . __LINE__ . " Special Query: " . $qry);
	return $qry;
}
/*Simple helper method to easily create an associative array from the result of a query*/
function createArrayFromQueryResult($queryResult){
	$specialResultArr = array();
	while($curr = mysqli_fetch_assoc($queryResult)){ $specialResultArr[] = $curr; }
	return $specialResultArr;
}

/*  Inputs
 *  $fullyBuiltQueryResultRows = the set of all rows of normal (not special) queries.
 *  $currSpecialResultRow      = A single row of special query
 *  $specialParts              = An array of fields relating to the special query which hold desired values for the query.
 *
 * */
function appendCurrentSpecialResultRowValueToOriginalQueryResult($fullyBuiltQueryResultRows, $currSpecialResultRow, $specialParts, $reportid){
	$fullyBuiltQueryResultRowsIndex = 0;
	global $specialParts;

	if(isset($_GET['branch_col'])) { //register branch report rows with special type only
		$server_id = $fullyBuiltQueryResultRows[$fullyBuiltQueryResultRowsIndex][$_GET['branch_col']];
		$branch_query = "Select SUM(`cash_tip`) as `cash_tip`, `register_name` from `" . $specialParts['dbname'] . "`.`orders` " . $specialParts['datewhere'] . " AND `server_id` = '" . $server_id . "' ".$specialParts['777'] ." GROUP BY `orders`.`register_name`";
//		error_log(__FILE__ . " " . __LINE__ . " Branch Query: " . $branch_query);
		$branch_query_result = mrpt_query($branch_query);
		$branch_result = array();
		while ($branch_row = mysqli_fetch_row($branch_query_result)) {
			$branch_result[] = $branch_row;
		}
		foreach ($fullyBuiltQueryResultRows as $reportQueryRow) {
			foreach ($reportQueryRow as $queryFieldKey => $queryFieldValue) {
				foreach ($branch_result as $branch_row) {
//					error_log("Branch row: " . $branch_row[1]);
//					error_log("Query Field Value: " . $queryFieldValue);
					if ($branch_row[1] == $queryFieldValue) {
						$size = count($fullyBuiltQueryResultRows[$fullyBuiltQueryResultRowsIndex]);
						$fullyBuiltQueryResultRows[$fullyBuiltQueryResultRowsIndex]["field" . $size] = $branch_row[0];
						break;
					}
				}
			}
			$fullyBuiltQueryResultRowsIndex++;
		}
//			error_log(__FILE__ . " " . __LINE__ . " Fully built query result rows : " . print_r($fullyBuiltQueryResultRows,1));
	}else{ //Non-Branch Code Block
		foreach ($fullyBuiltQueryResultRows as $reportQueryRow) {
			foreach ($reportQueryRow as $queryFieldKey => $queryFieldValue) {
				if (!$specialParts['AS_IS']) {
					if ($queryFieldKey == $specialParts['group_by_field_key']) {
						if ($reportQueryRow[$specialParts['group_by_field_key']] == $currSpecialResultRow[$specialParts['special_query_group_key']]) { //These if statements are nested to break out of loop every time the first if statement is seen rather than loop through the whole array even if the second statement returns false. using && will not fix this.
							$indexedSpecialVals = array_values($currSpecialResultRow);
							$specialValue = $indexedSpecialVals[0];
							$size = count($fullyBuiltQueryResultRows[$fullyBuiltQueryResultRowsIndex]);
							$fullyBuiltQueryResultRows[$fullyBuiltQueryResultRowsIndex]["field" . $size] = $specialValue;
						}
						break;
					}
				} else {
					foreach ($currSpecialResultRow as $value) {
						//Report ID 24 is for the Payments Report. The if statement makes sure cash tips lines up with the cash row.
						if ($reportid == 24 && strpos($fullyBuiltQueryResultRows[$fullyBuiltQueryResultRowsIndex]["field16"], 'Cash') !== FALSE && strpos($fullyBuiltQueryResultRows[$fullyBuiltQueryResultRowsIndex]["field18"], 'Sale') !== FALSE) {
							$fieldNum = count($fullyBuiltQueryResultRows[0])-1;
							$field = "field" . $fieldNum;
							$fullyBuiltQueryResultRows[$fullyBuiltQueryResultRowsIndex][$field] = $value;
						}
					}
					break;
				}
			}

			$fullyBuiltQueryResultRowsIndex++;
		}
	}
//	error_log(__FILE__ . " " . __LINE__ . " Fully built query result rows : " . print_r($fullyBuiltQueryResultRows,1));
//	error_log(__FILE__ . " " . __LINE__ . " Group By Field Key: " . $specialParts['group_by_field_key']);
	return $fullyBuiltQueryResultRows;
}

//Get Product and Tax code for "Modifiers Usage" report
function getProductAndTaxCodeModifierUsage($fullyBuiltQueryResultRows, $dbname, $showing_multi_report, $reportid){
	$productMergeArray = array(); $taxMergeArray = array();
	foreach ($fullyBuiltQueryResultRows as $key => $reportQueryRow) {
		if ($showing_multi_report) {
			$dbname = $reportQueryRow['cc'];
			unset($fullyBuiltQueryResultRows[$key]['cc']);
		}
		$modifier_title = ($reportid == 48 ? $reportQueryRow['field5'] : $reportQueryRow['field6']);
		$modifier_type = ($reportid == 48 ? $reportQueryRow['field1'] : $reportQueryRow['field4']);
		$mod_query = "SELECT `mod_id` from `".$dbname."`.`modifiers_used` WHERE title = '".mlavu_query_encode($modifier_title)."' AND type = '".$modifier_type."' LIMIT 1";
		$mod_query_result = mrpt_query($mod_query);
		$mod_row = mysqli_fetch_row($mod_query_result);
		$modifier_table = ($reportid == 48 ? "forced_modifiers" : "modifiers");
		$product_tax_query = "SELECT `product_code`, `tax_code` from `".$dbname."`.`".$modifier_table."` WHERE id = '".$mod_row[0]."' AND (product_code != '' OR tax_code != '')";
		$product_tax_query_result = mrpt_query($product_tax_query);
		$product_tax_result = array();
		$productCodeArr = array();
		$taxCodeArr = array();
		while ($product_tax_row = mysqli_fetch_row($product_tax_query_result)) {
			($product_tax_row[0]) ? $productCodeArr[] = $product_tax_row[0] : '';
			($product_tax_row[1]) ? $taxCodeArr[] = $product_tax_row[1] : '';
		}
		$afterIndex = ($reportid == 48 ? 5 : 6);
		$newVal = ($reportid == 48) ? array('field6' => implode(", ",$productCodeArr)) : array('field7' => implode(", ",$productCodeArr));
		$firstPartArray = array_slice($fullyBuiltQueryResultRows[$key], 0, $afterIndex+1);
		$secondPartArray = array_slice($fullyBuiltQueryResultRows[$key], $afterIndex+1);
		foreach($secondPartArray as $key1 => $intVal){
			$key_val = substr($key1, 0, 5);
			$key_int = substr($key1, 5);
			$key_int++;
			$productMergeArray[$key_val.$key_int] = $intVal;
		}
		$fullyBuiltQueryResultRows[$key] = array_merge($firstPartArray, $newVal, $productMergeArray);
		$afterIndex = ($reportid == 48 ? 6 : 7);
		$newVal = ($reportid == 48) ? array('field7' => implode(", ",$taxCodeArr)) : array('field8' => implode(", ",$taxCodeArr));
		$firstPartArray = array_slice($fullyBuiltQueryResultRows[$key], 0, $afterIndex+1);
		$secondPartArray = array_slice($fullyBuiltQueryResultRows[$key], $afterIndex+1);
		foreach($secondPartArray as $key1 => $intVal){
			$key_val = substr($key1, 0, 5);
			$key_int = substr($key1, 5);
			$key_int++;
			$taxMergeArray[$key_val.$key_int] = $intVal;
		}
		$fullyBuiltQueryResultRows[$key] = array_merge($firstPartArray, $newVal, $taxMergeArray);
	}
	return $fullyBuiltQueryResultRows;
}

//End of added code
/*Get Product and Tax code for "Sales by Item" report*/
function getProductAndTaxCodeSalesByItem($fullyBuiltQueryResultRows, $dbname, $showing_multi_report){
	$productMergeArray = array(); $taxMergeArray = array();
	foreach ($fullyBuiltQueryResultRows as $key => $reportQueryRow) {
		if ($showing_multi_report) {
			$dbname = $reportQueryRow['cc'];
			unset($fullyBuiltQueryResultRows[$key]['cc']);
		}
		$id=$reportQueryRow['field9'];
		if ($id) {
			$product_tax_query = "SELECT `product_code`, `tax_code` from `".$dbname."`.`menu_items` WHERE id=$id";
		}
		$product_tax_query_result = mrpt_query($product_tax_query);
		$product_tax_result = array();
		$productCodeArr = array();
		$taxCodeArr = array();
		while ($product_tax_row = mysqli_fetch_row($product_tax_query_result)) {
			($product_tax_row[0]) ? $productCodeArr[] = $product_tax_row[0] : '';
			($product_tax_row[1]) ? $taxCodeArr[] = $product_tax_row[1] : '';
		}
		$afterIndex = 18;
		$newVal = array('field18' => implode(", ",$productCodeArr));
		$firstPartArray = array_slice($fullyBuiltQueryResultRows[$key], 0, $afterIndex+1);
		$secondPartArray = array_slice($fullyBuiltQueryResultRows[$key], $afterIndex);
		foreach($secondPartArray as $key1 => $intVal){
			$key_val = substr($key1, 0, 5);
			$key_int = substr($key1, 5);
			$key_int++;
			$productMergeArray[$key_val.$key_int] = $intVal;
		}
		$fullyBuiltQueryResultRows[$key] = array_merge($firstPartArray, $newVal, $productMergeArray);
		$afterIndex = 19;
		$newVal = array('field19' => implode(", ",$taxCodeArr));
		$firstPartArray = array_slice($fullyBuiltQueryResultRows[$key], 0, $afterIndex+1);
		$secondPartArray = array_slice($fullyBuiltQueryResultRows[$key], $afterIndex);
		foreach($secondPartArray as $key1 => $intVal){
			$key_val = substr($key1, 0, 5);
			$key_int = substr($key1, 5);
			$key_int++;
			$taxMergeArray[$key_val.$key_int] = $intVal;
		}
		$fullyBuiltQueryResultRows[$key] = array_merge($firstPartArray, $newVal, $taxMergeArray);
	}
	return $fullyBuiltQueryResultRows;
}

//Modify the result for Report Action Log report id = 30
function modifyActionLogReports($fullyBuiltQueryResultRowsArray) {
	//json object is required for offline inventory functionality, So here overwrite json string with new message for report id 30
	foreach ($fullyBuiltQueryResultRowsArray as $key => $resultArray) {
		if ($resultArray['field7'] == 'Inventory Process Order') {
			$fullyBuiltQueryResultRowsArray[$key]['field8'] = 'Reserved Inventory Item';
		}
		if ($resultArray['field7'] == 'Inventory Deliver Order') {
			$fullyBuiltQueryResultRowsArray[$key]['field8'] = 'Deliverd Inventory Item';
		}
	}
	return $fullyBuiltQueryResultRowsArray;
}

function reorder( $report_row1, $report_row2 ){
	$orderby = $_GET['order_by_col'];
	$direction = isset( $_GET['ordermode'] ) ? $_GET['ordermode'] : 'asc';

	if( !isset( $report_row1[$orderby] ) || !isset( $report_row2[$orderby] ) ){
		return 0;
	}

	$value1 = $report_row1[$orderby];
	$value2 = $report_row2[$orderby];

	if( $value1 == $value2 ){
		return 0;
	}

	if( $direction == 'asc' ){
		if( $value1 < $value2){
			return -1;
		} else {
			return 1;
		}
	} else {
		if( $value1 < $value2){
			return 1;
		} else {
			return -1;
		}
	}
	return 0;
}
?>
