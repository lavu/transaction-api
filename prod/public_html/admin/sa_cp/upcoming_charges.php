<?php
	if(can_access("customers")) 
	{
		ini_set("display_errors","1");
		require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
		$baseurl = "?mode=upcoming_charges";
		
		function ts_plus_days($d)
		{
			return time() + (60 * 60 * 24 * $d);
		}
		
		function show_charges_for_date($tmts)
		{
			global $baseurl;
			
			$str = "";
			$tmdate = date("Y-m-d",$tmts);
			
			$str .= "<tr><td colspan='20' align='center' bgcolor='#777777' style='color:#ffffff; font-weight:bold'>";
			$str .= "Charges Scheduled For ".date("m/d/Y",$tmts);
			$str .= "</td></tr>";
			$charge_query = mlavu_query("select * from `poslavu_MAIN_db`.`billing_profiles` where `start_date`='[1]' and `type`='license' and (`active`='1' or `arb_status`='active')",$tmdate);
			while($charge_read = mysqli_fetch_assoc($charge_query))
			{
				$data_name = $charge_read['dataname'];
				$notes = $charge_read['notes'];
				$rest_id = $charge_read['restaurantid'];
				$charge_id = $charge_read['id'];
				$save_btn_id = "save_btn_" . $charge_id;
				$notes_id = "notes_" . $charge_id;
				
				$charge_email = "";
				$charge_phone = "";
				$charge_company = $data_name;
				$charge_city = "";
				$charge_state = "";
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$data_name);
				if(mysqli_num_rows($signup_query))
				{
					$signup_read = mysqli_fetch_assoc($signup_query);
					$charge_email = $signup_read['email'];
					$charge_phone = $signup_read['phone'];
					$charge_company = $signup_read['company'];
					$charge_city = $signup_read['city'];
					$charge_state = $signup_read['state'];
					
					if($charge_email!="")
					{
						$lead_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_leads` where `email`='[1]' and `city`!='' order by id desc limit 1",$charge_email);
						if(mysqli_num_rows($lead_query))
						{
							$lead_read = mysqli_fetch_assoc($lead_query);
							$charge_city = $lead_read['city'];
							$charge_state = $lead_read['state'];
						}
					}
				}
				
				$cp_link = "<a style='font-size:10px; color:#000088' href='index.php?mode=manage_customers&submode=details&rowid=$rest_id&loginmode=cp&cplogin=$rest_id&ccode=$data_name' target='_blank'>(Control Panel)</a>";
				
				$str .= "<tr>";
				$str .= "<td valign='top'>" . trim($charge_read['first_name'] . " " . $charge_read['last_name']) . "<br>$charge_city $charge_state</td><td>&nbsp;</td>";
				$str .= "<td valign='top'>xxxxxxxxxxxx" . $charge_read['last_four'] . "<br><a target='_new' href='https://admin.poslavu.com/sa_cp/billing/index.php?dn=$data_name' style='font-size:10px; color:#000088'>(Billing)</a></td><td>&nbsp;</td>";
				$str .= "<td valign='top'>$" . number_format($charge_read['amount'],2) . "</td><td>&nbsp;</td>";
				$str .= "<td valign='top'>$charge_company<br>$cp_link</td><td>&nbsp;</td>";
				$str .= "<td valign='top'>$charge_email<br>$charge_phone</td>";
				$str .= "<td valign='top'>";
				$str .= "<textarea id='$notes_id' rows='2' cols='60' style='font-size:10px' onkeypress='document.getElementById(\"$save_btn_id\").style.display = \"block\"'>$notes</textarea>";
				$str .= "<div id='$save_btn_id' style='display:none'><input type='button' value='Save' style='font-size:10px; color:#008800' onclick='window.location = \"$baseurl&charge_id=$charge_id&save_notes=\" + document.getElementById(\"$notes_id\").value'></div>";
				$str .= "</td>";
				$str .= "</tr>";
			}
			
			return $str;
		}
		
		if(isset($_GET['save_notes']))
		{
			$save_notes = $_GET['save_notes'];
			$charge_id = $_GET['charge_id'];
			
			mlavu_query("update `poslavu_MAIN_db`.`billing_profiles` set `notes`='[1]' where `id`='[2]'",$save_notes,$charge_id);
			echo "<script language='javascript'>";
			echo "window.location.replace('$baseurl'); ";
			echo "</script>";
			exit();
		}
		
		echo "<table cellspacing=0 cellpadding=4>";
		echo show_charges_for_date(ts_plus_days(1));
		echo show_charges_for_date(ts_plus_days(2));
		echo show_charges_for_date(ts_plus_days(3));
		echo show_charges_for_date(ts_plus_days(4));
		echo show_charges_for_date(ts_plus_days(5));
		echo show_charges_for_date(ts_plus_days(6));
		echo show_charges_for_date(ts_plus_days(7));
		echo show_charges_for_date(ts_plus_days(8));
		echo "</table>";
	}
?>
	
