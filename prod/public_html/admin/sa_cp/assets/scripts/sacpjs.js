function showSelector(selector_type, company_id, loc_id, type, vars) {

	var url = "inner_selector.php";
	var id_no = company_id;
	if (selector_type == "loc") {
		id_no = loc_id;
	}
	var params = " selector_type=" + selector_type + "&company_id=" + company_id + "&loc_id=" + loc_id + "&type=" + type; 	
	if (vars != "") {
		params += "&" + vars;
	}
	var http_request = false;
	
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		
		if (http_request.overrideMimeType) {
			http_request.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) { // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
		
	if (!http_request) {
		alert('Giving up :( Cannot create an XMLHTTP instance');
		return false;
	}

	http_request.onreadystatechange = function() { alertContents(http_request, selector_type, type, id_no); };
	http_request.open('POST', url, true);

	http_request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//http_request.setRequestHeader("Content-length", params.length); // causes js error: Refused to set unsafe header "Content-length"
	//http_request.setRequestHeader("Connection", "close"); // causes js error: Refused to set unsafe header "Connection"

	http_request.send(params);
}

function alertContents(http_request, selector_type, type, id_no) {
		
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			textinput = http_request.responseText;
			textsplit = textinput.split('-->');
			var divid = "div" + selector_type + type+ id_no;
						
			showtxt = textinput.split("<RUN_CODE>");
			if (showtxt.length > 1) {
				displaytxt = showtxt[0];
				showtxt = showtxt[1].split("</RUN_CODE>");
				if (showtxt.length > 1) {
					displaytxt += showtxt[1];
					document.getElementById(divid).innerHTML = displaytxt;
				}
			} else {
				document.getElementById(divid).innerHTML = textinput;
			}
			//initializeMaps();
			cmdtxt = textinput.split("<RUN_CODE>");
			if(cmdtxt.length>1) {
				cmdtxt = cmdtxt[1].split("</RUN_CODE>");
				cmdtxt = cmdtxt[0];
				eval(cmdtxt);
			}
		} else {
			alert('There was a problem loading the requested info.');
		}
	}
}