<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 5/07/19
 * Time: 6:15 PM
 */
require_once("../cp/resources/gt_functions.php");
require_once("../cp/resources/language_dictionary.php");

$filename = $_FILES["file"]["tmp_name"];
$pack_id	= reqvar('pack_id');

$path_parts = pathinfo($_FILES["file"]["name"]);
$extension = $path_parts['extension'];

if ($extension == 'csv' || $extension == 'resjson' || $extension == 'json') {
    if ($_FILES["file"]["size"] > 0) {
        list($existEnglishPhrase, $tagEnglishDictionaryDetails) = getExistLanguageData(1);
        list($existLanguagePhrase, $tagPackLanguageDictionaryDetails) = getExistLanguageData($pack_id);
        $msg = 2;
        if ($extension == 'csv') {
            $file = fopen($filename, "r");
            $newLanguagedata = array();
            $i = 0;
            while (($getData = fgetcsv($file, 100000, ",")) !== false) {
                if ($i == 0 && (trim(strtolower($getData[0])) != 'word_or_phrase' || trim(strtolower($getData[1])) != 'translation' || trim(strtolower($getData[2])) != 'used_by' || trim(strtolower($getData[3])) != 'tag')) {
                    $msg = 1;
                    break;
                }
                if ($i > 0 ) {
                    insertUpdateLanguageDictionary ($pack_id, $getData, $existEnglishPhrase, $tagEnglishDictionaryDetails, $existLanguagePhrase, $tagPackLanguageDictionaryDetails);
                }
                $i++;
            }
        } else {
            $getJsonData = file_get_contents($filename);
            $dataArray = json_decode($getJsonData, true);
            if (!empty($dataArray)) {
                foreach ($dataArray as $data) {
                    if (!isset($data['word_or_phrase']) || !isset($data['translation']) || !isset($data['used_by']) || !isset($data['tag'])) {
                        $msg = 1;
                        break;
                    }
                    $languageDictionaryData[0] = $data['word_or_phrase'];
                    $languageDictionaryData[1] = $data['translation'];
                    $languageDictionaryData[2] = $data['used_by'];
                    $languageDictionaryData[3] = $data['tag'];
                    insertUpdateLanguageDictionary ($pack_id, $languageDictionaryData, $existEnglishPhrase, $tagEnglishDictionaryDetails, $existLanguagePhrase, $tagPackLanguageDictionaryDetails);
                }
            } else {
                if ($getJsonData != '') {
                    $bom = pack('H*', 'EFBBBF');
                    $getJsonData = preg_replace("/^$bom/", '', $getJsonData);
                    $jsonDataArr = json_decode($getJsonData, true);
                    if (!empty($jsonDataArr)) {
                        foreach ($jsonDataArr as $keyword => $translation) {
                            $languageDictionaryData[0] = $keyword;
                            $languageDictionaryData[1] = $translation;
                            $languageDictionaryData[2] = '';
                            $languageDictionaryData[3] = '';
                            insertUpdateLanguageDictionary ($pack_id, $languageDictionaryData, $existEnglishPhrase, $tagEnglishDictionaryDetails, $existLanguagePhrase, $tagPackLanguageDictionaryDetails);
                        }
                    }
                }
            }
        }
        deleteAllLanguagePack($pack_id, 1);
    }
} else {
    $msg = 3;
}
$url = $_SERVER['HTTP_REFERER'];
if (strpos($url, '&msg') != false) {
    $lastAmpPosition = strrpos($url, '&');
    $url =substr($url, 0, $lastAmpPosition);
}
$showMsg = ($msg) ? '&msg='.$msg : '';
header('Location: '. $url.$showMsg);
/**
 * This functiion is used to get existing language dictionary data from DB based on given pack id
 * @param Integer $pack_id language pack id
 * @return Array $wordPhrase, $tagDictionaryDetails array of word_of_phrase and id and tag and id
 */
function getExistLanguageData($pack_id) {
    $lang_query = mlavu_query("SELECT * FROM `language_dictionary` WHERE pack_id = '" . $pack_id . "'");
    $wordPhrase = array();
    $tagDictionaryDetails = array();
    while ($data = mysqli_fetch_assoc($lang_query)) {
        $wordPhrase[$data['word_or_phrase']] = $data['id'];
        if ($data['tag'] != '') {
            $tagDictionaryDetails[$data['tag']] = $data['id'];
        }
    }
    return array ($wordPhrase, $tagDictionaryDetails);
}

/**
 * This functiion is used to insert and update language dictionary records into Main DB language_dictionary table
 * @param Integer $pack_id language pack id
 * @param Array $languageDictionaryData arary of new language data
 * @param Array $existEnglishPhrase array of existingEnglish with id
 * @param Array $tagEnglishDictionaryDetails array of existingEnglish with tag and id
 * @param Array $existLanguagePhrase array of existingLanguage with id
 * @param Array $tagPackLanguageDictionaryDetails array of existingLanguage with tag and id
 * @return boolean true
 */
function insertUpdateLanguageDictionary ($pack_id, $languageDictionaryData, $existEnglishPhrase, $tagEnglishDictionaryDetails, $existLanguagePhrase, $tagPackLanguageDictionaryDetails) {
    if (!empty($languageDictionaryData)) {
        $utc_datetime = UTCDateTime(true);
        $wordOrPhrase = trim($languageDictionaryData[0]);
        $translation = trim($languageDictionaryData[1]);
        $usedBy = trim($languageDictionaryData[2]);
        $tag = trim($languageDictionaryData[3]);
        if ($languageDictionaryData[0] != '') {
            if ($tag != '') {
                if (isset($tagEnglishDictionaryDetails[$tag]) && $tagEnglishDictionaryDetails[$tag] != '') {
                    $existEnglishId = $tagEnglishDictionaryDetails[$tag];
                    mlavu_query("UPDATE `language_dictionary` SET `word_or_phrase` = '[1]', `used_by` = '[2]', `tag` = '[3]', `modified_date` = '[4]' WHERE  `id` = '" . $existEnglishId . "' AND `pack_id` = 1", $wordOrPhrase, $usedBy, $tag, $utc_datetime, null, null, false);
                    if (isset($tagPackLanguageDictionaryDetails[$tag]) && $tagPackLanguageDictionaryDetails[$tag] != '') {
                        mlavu_query("UPDATE `language_dictionary` SET `word_or_phrase` = '[1]', `translation` = '[2]', `used_by` = '[3]', `tag` = '[4]', `modified_date` = '[5]' WHERE  `id` = '" . $tagPackLanguageDictionaryDetails[$tag] . "' AND `pack_id` = '[6]'", $wordOrPhrase, $translation, $usedBy, $tag, $utc_datetime, $pack_id, false);
                    } else {
                        mlavu_query("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `translation`, `english_id`, `used_by`, `created_date`, `modified_date`, `tag`) VALUES ('[1]', '[2]', '[3]', $existEnglishId, '[4]', '[5]', '[5]', '[6]')", $pack_id, $wordOrPhrase, $translation, $usedBy, $utc_datetime, $tag, false);
                    }
                } else {
                    mlavu_query("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `english_id`, `used_by`, `created_date`, `modified_date`, `tag`) VALUES (1, '[1]', 0, '[2]', '[3]', '[3]', '[4]')", $wordOrPhrase, $usedBy, $utc_datetime, $tag, null, null, false);
                    $englishId = mlavu_insert_id();
                    mlavu_query("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `translation`, `english_id`, `used_by`, `created_date`, `modified_date`, `tag`) VALUES ('[1]', '[2]', '[3]', $englishId, '[4]', '[5]', '[5]', '[6]')", $pack_id, $wordOrPhrase, $translation, $usedBy, $utc_datetime, $tag, false);
                }
            } else {
                if (isset($existEnglishPhrase[$wordOrPhrase]) && $existEnglishPhrase[$wordOrPhrase] != '') {
                    $existEnglishId = $existEnglishPhrase[$wordOrPhrase];
                    if (isset($existLanguagePhrase[$wordOrPhrase]) && $existLanguagePhrase[$wordOrPhrase] != '') {
                        mlavu_query("UPDATE `language_dictionary` SET `translation` = '[1]', `used_by` = '[2]', `tag` = '[3]', `modified_date` = '[4]' WHERE  `id` = '" . $existLanguagePhrase[$wordOrPhrase] . "' AND `pack_id` = '[5]'", $translation, $usedBy, $tag, $utc_datetime, $pack_id, null, false);
                    } else {
                        mlavu_query("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `translation`, `english_id`, `used_by`, `created_date`, `modified_date`, `tag`) VALUES ('[1]', '[2]', '[3]', $existEnglishId, '[4]', '[5]', '[5]', '[6]')", $pack_id, $wordOrPhrase, $translation, $usedBy, $utc_datetime, $tag, false);
                    }
                } else {
                    mlavu_query("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `english_id`, `used_by`, `created_date`, `modified_date`, `tag`) VALUES (1, '[1]', 0, '[2]', '[3]', '[3]', '[4]')", $wordOrPhrase, $usedBy, $utc_datetime, $tag, null, null, false);
                    $englishId = mlavu_insert_id();
                    mlavu_query("INSERT INTO `language_dictionary` (`pack_id`, `word_or_phrase`, `translation`, `english_id`, `used_by`, `created_date`, `modified_date`, `tag`) VALUES ('[1]', '[2]', '[3]', $englishId, '[4]', '[5]', '[5]', '[6]')", $pack_id, $wordOrPhrase, $translation, $usedBy, $utc_datetime, $tag, false);
                }
            }
        }
    }
    return true;
}
