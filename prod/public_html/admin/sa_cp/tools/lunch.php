<?php


	if(isset($submode))
	{
		$cmd = (isset($_GET['cmd']))?$_GET['cmd']:"";
		$today = date("Y-m-d");
		$today_dir = dirname(__FILE__) . "/storage/$today";
		$other_dir = dirname(__FILE__) . "/storage/other";
		
		if($cmd=="submit_lunch_order")
		{
			$accountid = (isset($_POST['accountid']))?$_POST['accountid']:"";
			$togo = (isset($_POST['togo']))?$_POST['togo']:"";
			$set_phone = (isset($_POST['set_phone']))?$_POST['set_phone']:"x";
			$content = (isset($_POST['content']))?$_POST['content']:"";
			$account_file = $today_dir . "/$accountid" . ".txt";
			$phone_file = $other_dir . "/txt_$accountid" . ".txt";
			
			$str = $togo . "|" . $content;
			
			$fp = fopen($account_file,"w");
			fwrite($fp,$str);
			fclose($fp);
			
			if($set_phone!="x")
			{
				$fp = fopen($phone_file,"w");
				fwrite($fp,$set_phone);
				fclose($fp);
			}

			echo "[---START---]";
			echo "result=submitted the lunch order for account $accountid";
			echo "&accountid=$accountid";
			echo "[---END---]";
			exit();
		}
		else
		{
			if(!is_dir($today_dir))
			{
				mkdir($today_dir,0755);
			}
			
			$lunch_list_restaurant = "";
			$lunch_list_togo = "";
			
			$maxcols = 4;
			$col = 1;
			$baseurl = "lunch.php?submode=lunch";
			//$baseurl = "index.php?mode=manage_customers&submode=lunch";
			
			echo "<script language='javascript' src='/sa_cp/tools/ajax_functions.js'></script>\n";
			echo "<script language='javascript'>\n";
			echo "	function submit_lunch_order_callback(response) {\n";
			echo "		var vars = ajax_get_internal_vars(response);\n";
			echo "		var accountid = vars['accountid'];\n";
			echo "		if(accountid=='location') {\n";
			echo "			window.location = \"$baseurl&updated=1\";\n";
			echo "		} else {\n";
			echo "			document.getElementById('container_' + accountid).bgColor = '#aaccaa';\n";
			echo "		}\n";
			echo "	}\n";
			echo "</script>";
			echo "<style>";
			echo "body, table { font-size:12px; font-family:Arial,Verdana; } ";
			echo "</style>";
			
			function read_last_send_file($last_send_file)
			{
				if(is_file($last_send_file) && filesize($last_send_file) > 0)
				{
					$fp = fopen($last_send_file,"r");
					$last_send_str = fread($fp,filesize($last_send_file));
					fclose($fp);
				}
				else $last_send_str = "";
				
				return $last_send_str;
			}
			
			function build_lunch_link($baseurl,$today_dir)
			{
				$accountid = "location";
				$account_file = $today_dir . "/$accountid" . ".txt";
				if(is_file($account_file))
				{
					$fp = fopen($account_file,"r");
					$fstr = fread($fp,filesize($account_file));
					fclose($fp);
					
					$vals = explode("|",$fstr);
					$set_link = $vals[1];
					$set_name = $vals[0];
				}
				else
				{
					$set_link = "";
					$set_name = "";
				}
				
				$loggedin_username = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:"";
				
				if(($set_name=="" && $set_link=="") || isset($_GET['new']))
				{
					$esc_set_name = str_replace("\"","&quot;",$set_name);
					$esc_set_link = str_replace("\"","&quot;",$set_link);
					
					echo "<table>";
					echo "<tr><td align='right'>Location Name:</td><td><input type='text' id='set_name' value=\"$esc_set_name\" /></td></tr>";
					echo "<tr><td align='right'>Location Link:</td><td><input type='text' id='set_link' value=\"$esc_set_link\" /></td></tr>";
					echo "<tr><td>&nbsp;</td><td><input type='button' value='Save Location' onclick='ajax_get_response(\"$baseurl&cmd=submit_lunch_order\",\"accountid=$accountid&content=\" + document.getElementById(\"set_link\").value + \"&togo=\" + (document.getElementById(\"set_name\").value),\"submit_lunch_order_callback\");' /></td></tr>";
					echo "</table>";
				}
				else
				{
					echo "Location: <b>$set_name</b>";
					if(trim($set_link)!="")
					{
						if(strpos($set_link,"http")===false)
							$set_link = "http://" . $set_link;
						echo " <a href='$set_link' target='_blank'>(Vist Website)</a>";
						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					}
					echo " <a href='$baseurl&new=1'>(update location / link)</a>";
					
					echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					
					if($loggedin_username=="corey" || $loggedin_username=="andy" || $loggedin_username=="rafael" || $loggedin_username=="ag" || $loggedin_username=="nsinton" || $loggedin_username=="lucas" || $loggedin_username=="misty")
					{
						$messages = array();
						$messages[1] = "Today lunch will be at \"" . $set_name . "\".  Please place your order now";
						$messages[2] = "Today lunch will be family style at \"" . $set_name . "\".  Please enter \"Yes\" if you will be joining us";
						$messages[3] = "Time to leave for Lunch!";
						
						if(isset($_GET['send_message']) && isset($messages[$_GET['send_message']]))
						{
							global $other_dir;
							$send_message = $messages[$_GET['send_message']];
							echo "<br><br>";
							echo "Message Selected: " . $send_message . "<br>";
							
							//------------------------- begin send message --------------------//
							$account_query = mlavu_query("select * from `poslavu_MAIN_db`.`accounts` order by firstname asc, lastname asc");
							while($account_read = mysqli_fetch_assoc($account_query))
							{
								$accountid = $account_read['id'];
								if($account_read['access']=="all" || strpos($account_read['access'],"customers")!==false || strpos($account_read['access'],"lunch")!==false)
								{
									$phone_file = $other_dir . "/txt_$accountid" . ".txt";
									$last_send_file = $other_dir . "/sent_$accountid" . ".txt";
									
									if(is_file($phone_file) && filesize($phone_file) > 0)
									{
										$fp = fopen($phone_file,"r");
										$pstr = fread($fp,filesize($phone_file));
										fclose($fp);
									}
									else
									{
										$pstr = "";
									}
									$set_phone = $pstr;
																											
									if(strpos($set_phone,"@")!==false && strpos($set_phone,".")!==false)
									{
										$set_send_str = date("Y-m-d") . "_" . $_GET['send_message'] . "_" . strlen($send_message);
										$last_send_str = read_last_send_file($last_send_file);
										$firstname = trim($account_read['firstname']);
										$fullname = trim($account_read['firstname'] . " " . $account_read['lastname']);
										
										if(trim($set_send_str)!=trim($last_send_str)) // prevent duplicate messages
										{
											$fp = fopen($last_send_file,"w");
											fwrite($fp,$set_send_str);
											fclose($fp);
											
											$last_send_str = read_last_send_file($last_send_file);
											if($last_send_str==$set_send_str) // make sure this message is logged
											{
												echo "Sending to $set_phone ($fullname)....<br>";
												mail($set_phone,"Lunch",$send_message,"From: lunch@poslavu.com");
											}
											else
											{
												echo "Cannot write log file for $set_phone ($fullname) - data $last_send_file - $last_send_str != $set_send_str<br>";
											}
										}
										else
										{
											echo "Already sent to $set_phone ($fullname)<br>";
										}
									}
								}
							}
							//-------------------------- end send message --------------------//
							
							echo "<br><input type='button' value='Continue >>' onclick='window.location = \"$baseurl\"' /><br><br>";
							exit();
						}
						else
						{
							//echo "<input type='button' style='font-size:10px' value='Send Lunch Message \"place your order\"' />";
							foreach($messages as $mkey => $mval)
							{
								if(trim($mval)!="")
								{
									echo "<input type='button' style='font-size:10px' value='$mval' onclick='window.location = \"$baseurl&send_message=$mkey\"'/>&nbsp;&nbsp;&nbsp;&nbsp;";
								}
							}
						}
					}
				}
				echo "<br><br>";
				
				echo "<hr>";
			}
			
			echo build_lunch_link($baseurl,$today_dir);
			
			echo "<table>";
			echo "<tr>";
			$account_query = mlavu_query("select * from `poslavu_MAIN_db`.`accounts` order by firstname asc, lastname asc");
			while($account_read = mysqli_fetch_assoc($account_query))
			{
				$accountid = $account_read['id'];
				if($account_read['access']=="all" || strpos($account_read['access'],"customers")!==false || strpos($account_read['access'],"lunch")!==false)
				{
					if($col > $maxcols) 
					{
						$col = 1;
						echo "</tr><tr><td>&nbsp;</td></tr><tr>";
					}
					
					$account_file = $today_dir . "/$accountid" . ".txt";
					if(is_file($account_file))
					{
						$fp = fopen($account_file,"r");
						$fstr = fread($fp,filesize($account_file));
						fclose($fp);
						
						$vals = explode("|",$fstr);
						$set_content = $vals[1];
						$checked = $vals[0];
						$color = "#aaccaa";
					}
					else
					{
						$set_content = "";
						$checked = "";
						$color = "#eeeeee";
					}
					
					$phone_file = $other_dir . "/txt_$accountid" . ".txt";
					if(is_file($phone_file) && filesize($phone_file) > 0)
					{
						$fp = fopen($phone_file,"r");
						$pstr = fread($fp,filesize($phone_file));
						fclose($fp);
					}
					else
					{
						$pstr = "";
					}
					$set_phone = $pstr;
					
					echo "<td valign='top' bgcolor='$color' style='border:solid 1px #aaaaaa' id='container_$accountid' bgcolor='#ffffff'>";
					echo "<table width='100%' cellsapcing=0 cellpadding=0><tr><td align='left'>";
					
					//Lols  
					$firstName = $account_read['firstname'];
					$firstName = str_replace('Rafael', "R4ph-  Stub Step (Coat's bane)", $firstName);
					
					$ask_for_phone = true;
					
					echo $firstName . " " . $account_read['lastname'];
					echo "</td><td align='right'>";
					echo "to-go<input type='checkbox' id='togo_$accountid'";
					if($checked=="1") echo " checked";
					echo ">";
					echo "</td></table>";
					echo "<textarea rows='2' cols='32' id='content_$accountid'>$set_content</textarea>";
					if($ask_for_phone) echo "<table width='100%' cellspacing=0 cellpadding=0><tr><td align='right' style='color:#668866; font-size:10px'>Txt Msg Email <a style='cursor:pointer' onclick='alert(\"Verizon: phonenumber@vtext.com\\n\\nATT: phonenumber@mms.att.net\\n\\nT-Mobile: phonenumber@tmomail.net\\n\\nSprint: phonenumber@messaging.sprintpcs.com\")'>(?)</a><input type='text' id='phone_$accountid' value='$set_phone' style='color:#778877; font-size:10px'></td></tr></table>";
					echo "<table width='100%' cellspacing=0 cellpadding=0><tr><td align='right'><input style='font-size:9px' type='button' value='Save' onclick='document.getElementById(\"container_$accountid\").bgColor = \"#aaaaaa\"; ajax_get_response(\"$baseurl&cmd=submit_lunch_order\",\"accountid=$accountid&content=\" + document.getElementById(\"content_$accountid\").value";
					if($ask_for_phone) echo " + \"&set_phone=\" + document.getElementById(\"phone_$accountid\").value";
					echo " + \"&togo=\" + (document.getElementById(\"togo_$accountid\").checked?\"1\":\"0\"),\"submit_lunch_order_callback\");' /></td></tr></table>";
					//foreach($account_read as $key => $val) echo $key . "=$val<br>";
					echo "</td>";
					echo "<td width='10'>&nbsp;</td>";
					$col++;
					
					if(trim($set_content)!="")
					{
						$lstr = "<br><br>";
						$lstr .= $account_read['firstname'] . " " . $account_read['lastname'] . " - ";
						$lstr .= str_replace("\n","<br>",$set_content);
						
						if($checked=="1")
							$lunch_list_togo .= $lstr;
						else
							$lunch_list_restaurant .= $lstr;
					}
				}
			}
			echo "</tr>";
			echo "</table>";
			
			if(trim($lunch_list_restaurant)!="")
			{
				echo "<hr>";
				echo "<b>------------ For at the Location: ------------</b>";
				echo $lunch_list_restaurant;
			}
			if(trim($lunch_list_togo)!="")
			{
				echo "<hr>";
				echo "<b>--------------- To-Go Orders: ---------------</b>";
				echo $lunch_list_togo;
			}
		}
	}
?>
