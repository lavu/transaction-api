<?php
	$ajcode = 	"AGJRIGAbn43t8ghagan4gA45j";
	if(isset($_POST['ajcode']) && $_POST['ajcode']==$ajcode)
	{
		if(isset($_POST['restid']))
		{
			$restid = $_POST['restid'];
			if(isset($_POST['setstate']))
			{
				require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
				$maindb = "poslavu_MAIN_db";
				
				$setstate = $_POST['setstate'];
				$success = false;
				$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$_POST['restid']);
				if(mysqli_num_rows($rest_query))
				{
					$rest_read = mysqli_fetch_assoc($rest_query);
					$data_name = $rest_read['data_name'];
					
					if($data_name!="")
					{
						$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$data_name);
						if(mysqli_num_rows($signup_query))
						{
							$signup_read = mysqli_fetch_assoc($signup_query);
							$signup_id = $signup_read['id'];
							
							$success = mlavu_query("update `poslavu_MAIN_db`.`signups` set `state`='[1]' where `id`='[2]' and `dataname`='[3]'",$setstate,$signup_id,$data_name);
							if($success)
							{
								$message = "Success";
							}
							else
							{
								$message = "Failed To Update Database";
							}
						}
						else
						{
							$message = "No Signup Record";
						}
					}
					else
					{
						$message = "No Dataname";
					}
				}
				else
				{
					$message = "No Restaurant Record";
				}
				
				echo "success=$success&restid=$restid&setstate=$setstate&message=$message";
			}
		}
		exit();
	}

	$can_contine = false;
	if(can_access("technical"))
		$can_continue = true;
	
	$calling_ip = (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
	
	//error_log("cip: ".$calling_ip);
		
	//if(($_SERVER['REMOTE_ADDR']=="74.95.18.90" || $_SERVER['REMOTE_ADDR']=="71.177.73.26" || $_SERVER['REMOTE_ADDR'] == "67.42.135.99" || $_SERVER['REMOTE_ADDR']=="174.56.118.101" || $_SERVER['REMOTE_ADDR']=="50.130.130.9" || $_SERVER['REMOTE_ADDR']=="209.181.115.24" || $_SERVER['REMOTE_ADDR']=="76.113.74.243" || $_SERVER['REMOTE_ADDR']=="166.248.18.254" || $_SERVER['REMOTE_ADDR']=="71.177.73.26") && can_access("technical"))
	// Pelican 64.58.147.242
	if (in_array($calling_ip, array("216.243.114.158", "216.243.114.114", "74.95.18.90", "76.113.74.243", "64.58.147.242","63.157.188.170","73.26.62.194")) && can_access("technical"))
		$can_continue = true;
	else if ($submode=="general_tools" && isset($_GET['tool']) && $_GET['tool']=="uncaptured" && isset($_GET['catdog']) && $_GET['catdog']=="hamstertruck")
		$can_continue = true;
	else
	{
		if($can_continue)
		{
		}
		else
		{
			echo "..";
			exit();
		}
	}
		
	function get_local_load_level() // for the load level
	{
		$lockfname = "/home/poslavu/private_html/loadlevel.txt";
		$fp = fopen($lockfname,"r");
		$lockout_status = fread($fp,filesize($lockfname));
		fclose($fp);
		if($lockout_status!="" && is_numeric($lockout_status))
		{
			return $lockout_status;
		}
		else return "";
	}
	
	function get_license_type_by_payment_amount($amount)
	{
		$lic_amounts = array();
		$lic_amounts[] = array(895,"license","silver");
		$lic_amounts[] = array(716,"license","silver");
		$lic_amounts[] = array(1495,"license","gold");
		$lic_amounts[] = array(3495,"license","pro");
		$lic_amounts[] = array(2495,"license","pro");
		$lic_amounts[] = array(600,"upgrade","gold");
		$lic_amounts[] = array(1000,"upgrade","pro");
		$lic_amounts[] = array(1600,"upgrade","pro");
		$lic_amounts[] = array(2000,"upgrade","pro");
		$lic_amounts[] = array(2600,"upgrade","pro");
		$lic_amounts[] = array(4500,"buyin","");
		$lic_amounts[] = array(9000,"buyin","");
		$lic_amounts[] = array(29.00,"hosting","silver");
		$lic_amounts[] = array(29.95,"hosting","silver");
		$lic_amounts[] = array(39.00,"hosting","silver");
		$lic_amounts[] = array(39.95,"hosting","silver");
		$lic_amounts[] = array(49.00,"hosting","gold");
		$lic_amounts[] = array(49.95,"hosting","gold");
		$lic_amounts[] = array(79.00,"hosting","gold");
		$lic_amounts[] = array(79.95,"hosting","gold");
		$lic_amounts[] = array(99.00,"hosting","pro");
		$lic_amounts[] = array(99.95,"hosting","pro");
		$lic_amounts[] = array(149.00,"hosting","pro");
		$lic_amounts[] = array(149.95,"hosting","pro");
		$distro_amounts = array(0,20,25,30,35,40,50);
		$non_distro_amounts = array(0,10,15,20,25,30);
		
		$type_found = "";
		$level_found = "";
		for($n=0; $n<count($lic_amounts); $n++)
		{
			if($lic_amounts[$n][1]=="license" || $lic_amounts[$n][1]=="upgrade")
			{
				for($m=0; $m<count($distro_amounts); $m++)
				{
					$check_amount = $lic_amounts[$n][0] * 1 - ($lic_amounts[$n][0] * ($distro_amounts[$m] / 100));
					if(number_format($amount,2)==number_format($check_amount,2))
					{
						$type_found = $lic_amounts[$n][1];
						$level_found = (isset($lic_amounts[$n][2]))?$lic_amounts[$n][2]:"";
					}
				}
				for($m=0; $m<count($non_distro_amounts); $m++)
				{
					$check_amount = $lic_amounts[$n][0] * 1 - ($lic_amounts[$n][0] * ($non_distro_amounts[$m] / 100));
					if(number_format($amount,2)==number_format($check_amount,2))
					{
						$type_found = $lic_amounts[$n][1];
						$level_found = (isset($lic_amounts[$n][2]))?$lic_amounts[$n][2]:"";
					}
				}
			}
			else
			{
				$check_amount = $lic_amounts[$n][0] * 1;
				if(number_format($amount,2)==number_format($check_amount,2))
				{
					$type_found = $lic_amounts[$n][1];
					$level_found = (isset($lic_amounts[$n][2]))?$lic_amounts[$n][2]:"";
				}
			}
		}
		return array("type"=>$type_found,"level"=>$level_found);
	}
	
	function update_restaurant_location_data($rest_read,$loc_read)
	{
		$rsp = "";
		$restid = $rest_read['id'];
		$dataname = $rest_read['data_name'];
		$dbname = "poslavu_".$dataname."_db";
		
		$flist1 = array("title","address","city","state","zip","country","timezone","phone","email","website","manager");
		$flist2 = array("taxrate","net_path","use_net_path","component_package","component_package2","product_level","gateway","component_package_code");
		$flist3 = array("lavu_togo_name");
		$flist = array_merge($flist1,$flist2);

		$setlist = array();
		$setlist[] = array("dataname",$dataname);
		$setlist[] = array("restaurantid",$restid);
		$setlist[] = array("locationid",$loc_read['id']);
		$setlist[] = array("last_activity",$rest_read['last_activity']);
		$setlist[] = array("created",$rest_read['created']);
		$setlist[] = array("disabled",$rest_read['disabled']);
		
		for($n=0; $n<count($flist); $n++)
		{
			$fieldname = $flist[$n];
			$setlist[] = array($fieldname,$loc_read[$fieldname]);
		}
		for($n=0; $n<count($flist3); $n++)
		{
			$flist_name = $flist3[$n];
			$config_query = mlavu_query("select `value` from `[1]`.`config` where `type`='location_config_setting' and `setting`='[2]' and `location`='[3]'",$dbname,$flist_name,$loc_read['id']);
			if(mysqli_num_rows($config_query))
			{
				$config_read = mysqli_fetch_assoc($config_query);
				$setlist[] = array($flist_name,$config_read['value']);
			}
		}
	
		$conn = ConnectionHub::getConn('poslavu');

		$field_code = "";
		$value_code = "";
		$update_code = "";
		$update_vals = array();
		for($n=0; $n<count($setlist); $n++)
		{
			$fieldname = $setlist[$n][0];
			$fieldvalue = $setlist[$n][1];
			
			if($field_code!="") $field_code .= ",";
			if($value_code!="") $value_code .= ",";
			if($update_code!="") $update_code .= ",";
			
			$update_vals[$fieldname] = $fieldvalue;
			$field_code .= "`$fieldname`";
			$value_code .= "'[$fieldname]'";
			$update_code .= "`$fieldname`='[$fieldname]'";
		}
		
		$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurant_locations` where `dataname`='[1]' and `restaurantid`='[2]' and `locationid`='[3]'",$dataname,$restid,$loc_read['id']);
		if(mysqli_num_rows($exist_query))
		{
			$exist_read = mysqli_fetch_assoc($exist_query);
			$update_vals['existid'] = $exist_read['id'];
			mlavu_query("update `poslavu_MAIN_db`.`restaurant_locations` set " . $update_code . " where `id`='[existid]'",$update_vals);
			$rsp .= "<font color='#000088'>Updating " . $dataname . " - location: " . $loc_read['id'] . " - " . $loc_read['title'] . "</font><br>";
		}
		else
		{
			mlavu_query("insert into `poslavu_MAIN_db`.`restaurant_locations` ($field_code) values ($value_code)",$update_vals);
			$rsp .= "<font color='#008800'>Inserting into " . $dataname . " - location: " . $loc_read['id'] . " - " . $loc_read['title'] . "</font><br>";
		}
		return $rsp;
	}
	
	if($submode=="general_tools" && $can_continue)
	{
		$tool = (isset($_GET['tool']))?$_GET['tool']:"";
		$cmd = (isset($_GET['cmd']))?$_GET['cmd']:"";
		
		if($tool=="mixed_versions")
		{
			$last_activity = date("Y-m-d H:i:s",time() - (60 * 60 * 24 * 4));
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `last_activity`>'[1]' order by id asc",$last_activity);
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
				
				echo $rdb . "<br>";
			}
		}
		else if($tool=="grant_chain")
		{
			$restid = "24073";
			$master_dataname = "coccinelle_sys";
			
			require_once("/home/poslavu/public_html/inc/usermgr.php");
			$user = "usr_pl_" . $restid;
			$rdb = "poslavu_" . $master_dataname . "_db";
			
			$result = grant_db_user($user,$rdb);
			if($result) echo "Success<br>"; else echo "Failed<br>";

			$ins = "insert into `config` (`location`,`setting`,`value`,`type`) values ('1','chain_master_menu','coccinelle_sys','location_config_setting')";
			$upd = "update `config` set `value`='coccinelle_sys' where `setting`='chain_master_menu'";
			//https://admin.poslavu.com/sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=grant_chain
		}
		else if($tool=="edit_coms")
		{
			require_once(dirname(__FILE__) . "/edit_coms/view_comlist.php");
		}
		else if($tool=="restactivity")
		{
			$day_updating = date("Y-m-d");
			$datetime_updating = date("Y-m-d H:i:s");
			
			function restdata_number($n)
			{
				$str = number_format($n,2);
				$str = str_replace(",","",$str);
				return $str;
			}
			
			function last_restdata_id_updated_recently($type)
			{
				global $day_updating;
				global $datetime_updating;
				
				$last_query = mlavu_query("select * from `poslavu_MAIN_db`.`restdata` where `type`='summary - [1]' and left(`updated_datetime`,10)='[2]' order by `data2` * 1 desc limit 1",$type,$day_updating);
				if(mysqli_num_rows($last_query))
				{
					$last_read = mysqli_fetch_assoc($last_query);
					$last_id = $last_read['data2'];
					if(is_numeric($last_id)) return $last_id;
				}
				return 0;
			}
			
			function last_restdata_info_updated(&$rest_read,$type)
			{
				$fid = "R" . $rest_read['data_name'] . "S" . $type;
				$fexist_query = mlavu_query("select * from `poslavu_MAIN_db`.`restdata` where `range`='[1]' order by `updated_datetime` desc limit 1",$fid);
				if(mysqli_num_rows($fexist_query))
				{
					$fexist_read = mysqli_fetch_assoc($fexist_query);
					return $fexist_read['updated_datetime'];
				}
				else
				{
					$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`restdata` where `dataname`='[1]' and `type`='summary - [2]' order by `updated_datetime` desc limit 1",$rest_read['data_name'],$type);
					if(mysqli_num_rows($exist_query))
					{
						$exist_read = mysqli_fetch_assoc($exist_query);
						return $exist_read['updated_datetime'];
					}
					else
					{
						return "0000-00-00 00:00:00";
					}
				}
			}
			
			function update_restdata_info(&$rest_read,$type,$range,$data1,$data2="",$data_long="")
			{
				global $day_updating;
				global $datetime_updating;

				$vars = array();
				$vars['dataname'] = $rest_read['data_name'];
				$vars['restid'] = $rest_read['id'];
				$vars['type'] = $type;
				$vars['range'] = $range;
				$vars['data'] = $data1;
				$vars['data2'] =  $data2;
				$vars['data_long'] = $data_long;
				$vars['day_updating'] = $day_updating;
				$vars['datetime_updating'] = $datetime_updating;
				
				$exist_id = 0;
				$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`restdata` where `dataname`='[dataname]' and `type`='[type]' and `range`='[range]' and left(`updated_datetime`,10)='[day_updating]'",$vars);
				if(mysqli_num_rows($exist_query))
				{
					$exist_read = mysqli_fetch_assoc($exist_query);
					$exist_id = $exist_read['id'];
				}
								
				if($exist_id)
				{
					$vars['id'] = $exist_id;
					mlavu_query("update `poslavu_MAIN_db`.`restdata` set `data`='[data]', `data2`='[data2]', `data_long`='[data_long]', `updated_datetime`='[datetime_updating]' where `id`='[id]'",$vars);
					return array("action"=>"updated","id"=>$exist_id);
				}
				else
				{
					mlavu_query("insert into `poslavu_MAIN_db`.`restdata` (`dataname`,`restid`,`type`,`range`,`data`,`data2`,`data_long`,`updated_datetime`) values ('[dataname]','[restid]','[type]','[range]','[data]','[data2]','[data_long]','[datetime_updating]')",$vars);
					return array("action"=>"inserted","id"=>mlavu_insert_id());
				}
			}
			
			$count_types = array("inserted","updated","error");
			
			echo "<table cellspacing=0 cellpadding=4>";
			echo "<tr>";
			echo "<td style='border-bottom:solid 1px black' colspan='4'>&nbsp;</td>";
			for($n=0; $n<count($count_types); $n++)
			{
				echo "<td style='border-bottom:solid 1px black'>" . ucwords($count_types[$n]) . "</td>";
			}
			echo "</tr>";
			
			$page_limit = 400;
			$dtypes = array("daily sales","daily transactions");
			$last_id = last_restdata_id_updated_recently($dtypes[0]);
			for($n=1; $n<count($dtypes); $n++)
			{
				$check_last_id = last_restdata_id_updated_recently($dtypes[$n]);
				if($check_last_id < $last_id) $last_id = $check_last_id;
			}
			
			echo "Last Id: $last_id<br>";
			if(isset($_GET['next']))
			{
				echo "<script type='text/javascript'>";
				echo "	allow_auto_continue = 'yes'; ";
				echo "	function stop_auto_continue() { ";
				echo "		allow_auto_continue = 'no'; ";
				echo "		document.getElementById('auto_continue_text').innerHTML = 'Stopping Auto Continue...'; ";
				echo "	} ";
				echo "</script>";
				echo "<div id='auto_continue_text'>Auto Continue Enabled <input type='button' value='Turn Off' onclick='stop_auto_continue()' /></div><br><br>";
			}
			//$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='gold_st_cafe' order by id asc limit 400");
			$ncount = 0;
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `id`>'[1]' order by id asc limit [2]",$last_id,$page_limit);
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
				$ncount++;
				
				for($d=0; $d<count($dtypes); $d++)
				{
					$counts = array();
					$dtype = $dtypes[$d];
					$rest_last_update = last_restdata_info_updated($rest_read,$dtype);
					$rest_last_update_day = substr($rest_last_update,0,10);
					
					if($dtype=="daily sales")
					{
						$order_query = mlavu_query("select count(*) as `count`, sum(`total`) as `total`, left(`opened`,10) as `day` from `[1]`.`orders` where left(`opened`,10)>='[2]' group by `day` order by `day` asc",$rdb,$rest_last_update_day);
					}
					else if($dtype=="daily transactions")
					{
						$order_query = mlavu_query("select count(*) as `count`, sum(`amount`) as `total`, left(`datetime`,10) as `day` from `[1]`.`cc_transactions` where left(`datetime`,10)>='[2]' and `action`='Sale' group by `day` order by `day` asc",$rdb,$rest_last_update_day);
					}
					else 
					{
						$order_query = false;
					}
					//echo $rdb . " " . $dtype . " last update day: " . $rest_last_update_day . " (rows checked: ".mysqli_num_rows($order_query).")<br>";
					
					while($order_read = mysqli_fetch_assoc($order_query))
					{
						$uresult = update_restdata_info($rest_read,$dtype,$order_read['day'],$order_read['count'],restdata_number($order_read['total']));
						if($uresult['id'] < 1) $count_type = "error";
						else $count_type = $uresult['action'];
						
						if(!isset($counts[$count_type])) $counts[$count_type] = 0;
						$counts[$count_type]++;
						
						//echo "Update Daily Sales: " . $order_read['day'] . " - " . $order_read['count'] . " / " . restdata_number($order_read['total']) . " (".$uresult['action'].")<br>";
					}
					
					$total_count = 0;
					echo "<tr>";
					echo "<td>" . $ncount . "</td>";
					echo "<td>" . $rest_read['data_name'] . "</td>";
					echo "<td>" . $rest_last_update_day . "</td>";
					echo "<td style='border-right:solid 1px #aaaaaa'>" . $dtype . "</td>";
					for($n=0; $n<count($count_types); $n++)
					{
						echo "<td>" . $counts[$count_types[$n]] . "</td>";
						$total_count += $counts[$count_types[$n]];
					}
					echo "</tr>";
					update_restdata_info($rest_read,"summary - ".$dtype,"R".$rest_read['data_name']."S".$dtype,$total_count,$rest_read['id']);
				}
			}
			echo "</table>";
			
			$randnumber = rand(100000,999999);
			if(isset($_GET['next']))
			{
				echo "<script type='text/javascript'>";
				echo "	setTimeout('auto_continue()',2000); ";
				echo "	function auto_continue() { ";			
				echo "		if(allow_auto_continue=='yes') { ";
				echo "			window.location = '/sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=restactivity&r=$randnumber&next=1'; ";
				echo "		} ";
				echo "	} ";
				echo "</script>";
			}
		}
		else if($tool=="customerlocations")
		{
			ini_set("display_errors",1);
			$mode = "csv";//"table";
			$ccols = array("restaurantid","dataname","title","city","state","zip","country");
			$rcols = array("text:distro","username","company","city","state","postal_code","country");
			if($mode=="table") echo "<table>"; else echo "<textarea rows='40' cols='120'>";
			
			function output_location_row($loc_read,$mode,&$cols)
			{
				if($mode=="table") echo "<tr>";
				for($n=0; $n<count($cols); $n++)
				{
					if($mode=="table") echo "<td>"; else echo "\"";
					if(substr($cols[$n],0,5)=="text:") echo substr($cols[$n],5);
					else echo str_replace("\"","",str_replace("\n","",$loc_read[$cols[$n]]));
					if($mode=="table") echo "</td>"; else echo "\",";
				}
				if($mode=="table") echo "</tr>"; else echo "\n";				
			}
			
			$loc_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurant_locations` where `dataname`!='' order by id asc");
			while($loc_read = mysqli_fetch_assoc($loc_query))
			{
				output_location_row($loc_read,$mode,$ccols);
			}
			
			$reseller_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`!='' order by id asc");
			while($reseller_read = mysqli_fetch_assoc($reseller_query))
			{
				output_location_row($reseller_read,$mode,$rcols);
			}
			
			if($mode=="table") echo "</table>"; else echo "</textarea>";
		}
		else if($tool=="pointsaug15")
		{
			$min_datetime = "2015-08-16 00:00:00";
			$max_datetime = date("Y-m-d H:i:s");
			
			$total_past_balance = 0;
			
			$exportstr = "";
			
			echo "<table>";
			$points_query = mlavu_query("select username,credits from `poslavu_MAIN_db`.`resellers` where `isCertified`='1' and `username`!='' and `credits`!='' order by `credits` * 1 desc");
			while($points_read = mysqli_fetch_assoc($points_query))
			{
				$rname = $points_read['username'];
				$current_points = $points_read['credits'];
				$awarded = 0;
				
				$award_query = mlavu_query("select sum(`credits_applied`) as `credits_applied` from `poslavu_MAIN_db`.`reseller_billing_log` where `resellername`='[1]' and `datetime`>='[2]' and `datetime`<='[3]'",$rname,$min_datetime,$max_datetime);
				if(mysqli_num_rows($award_query))
				{
					$award_read = mysqli_fetch_assoc($award_query);
					$awarded = $award_read['credits_applied'];
				}
				
				$purchase_query = mlavu_query("select sum(`value`) as `purchased` from `poslavu_MAIN_db`.`reseller_licenses` where resellername='[1]' and `notes` LIKE 'PURCHASED WITH POINTS%' and `purchased`>='[2]' and `purchased`<='[3]'",$rname,$min_datetime,$max_datetime);
				if(mysqli_num_rows($purchase_query))
				{
					$purchase_read = mysqli_fetch_assoc($purchase_query);
					$purchased = $purchase_read['purchased'];
				}
				
				$aug15points = ($current_points * 1) - ($awarded * 1) + ($purchased * 1);
				$total_past_balance += ($aug15points * 1);
				
				echo "<tr>";
				echo "<td align='right'>" . $rname . "</td>";
				echo "<td>" . $current_points . "</td>";
				echo "<td>- " . $awarded . "</td>";
				echo "<td>+ " . $purchased . "</td>";
				echo "<td>= " . $aug15points . "</td>";
				echo "</tr>";
				
				$exportstr .= $rname . "\t" . $current_points . "\t-\t" . $awarded . "\t+\t" . $purchased . "\t=\t" . $aug15points . "\n";
			}
			echo "</table>";
			echo "<br>Total Past Balance " . $total_past_balance . "<br>";
			
			echo "<br>";
			echo "<textarea rows='20' cols='120'>";
			echo $exportstr;
			echo "</textarea>";
		}
		else if($tool=="firstpay")
		{
			for($m=36; $m>=0; $m--)
			{
				$first_count = 0;
				$min_datetime = date("Y-m-d H:i:s",mktime(0,0,0,date("m")-$m,1,date("Y")));
				$max_datetime = date("Y-m-d H:i:s",mktime(date("H"),date("i"),59,date("m")-$m,date("d"),date("Y")));
				$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `datetime`>='[1]' and `datetime`<='[2]' and `match_restaurantid`!='' and `x_type`='auth_capture' and x_response_code='1' order by `datetime` desc",$min_datetime,$max_datetime);
				while($pay_read = mysqli_fetch_assoc($pay_query))
				{
					$restid = $pay_read['match_restaurantid'];
					$past_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `datetime`<'[1]' and `match_restaurantid`='[2]' and x_type='auth_capture' and x_response_code='1'",$pay_read['datetime'],$restid);
					if(mysqli_num_rows($past_query))
					{
						$past_read = mysqli_fetch_assoc($past_query);
						if($past_read['count']==0)
						{
							$first_count++;
						}
					}
				}
				echo "First Payment Count (".date("m/Y",mktime(0,0,0,date("m")-$m,1,date("Y")))."): " . $first_count . "<br>";
			}
		}
		else if($tool=="highlow")
		{
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `notes` NOT LIKE '%AUTO-DELETED%' and `data_name`!='' and `lavu_lite_server`='' order by `id` asc");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				$dataname = $rest_read['data_name'];
				$restaurantid = $rest_read['id'];
				
				$rdb = "poslavu_" . $dataname . "_db";
				
				$done = true;
				$type_query = mlavu_query("show columns from `[1]`.`ingredients`",$rdb);
				while($type_read = mysqli_fetch_assoc($type_query))
				{
					if($type_read['Field']=="low" && $type_read['Type']!='varchar(25)') $done = false;
					if($type_read['Field']=="high" && $type_read['Type']!='varchar(25)') $done = false;
				}
				
				if($done)
				{
					echo "<font style='color:#000088'>already done: " . $dataname . "</font><br>";
				}
				else
				{
					$scs = mlavu_query("alter table `[1]`.`ingredients` change `high` `high` varchar(25) default '' not null",$rdb);
					if($scs)
					{
						$scs = mlavu_query("alter table `[1]`.`ingredients` change `low` `low` varchar(25) default '' not null",$rdb);
						if($scs)
						{
							mlavu_query("update `[1]`.`ingredients` set `high`='' where `high`='0'",$rdb);
							mlavu_query("update `[1]`.`ingredients` set `low`='' where `low`='0'",$rdb);
							echo "<font style='color:#008800'>updating: " . $dataname . "</font><br>";
						}
					}
					if(!$scs)
					{
						echo "<font style='color:#880000'>unable to update: " . $dataname . "</font><br>";
					}
				}
			}
		}
		else if($tool=="fix_parts")
		{
			$fix_parts = true;
			require_once(dirname(__FILE__) . "/fix_parts.php");
		}
		else if($tool=="receivables")
		{
			if(!function_exists("get_due_prop"))
			{
				function get_due_prop($type,$due_info)
				{
					if(strtolower($type)=="due")
						$due_parts = explode("Due since",$due_info);
					else
					{
						$due_parts = explode(strtolower($type) . ": $",strtolower($due_info));
						if(count($due_parts) < 2)
						{
							$due_parts = explode(strtolower($type) . ":",strtolower($due_info),2);
						}
					}
			
					if(count($due_parts) > 1)
					{
						$due_parts = explode("|",$due_parts[1]);
						return trim(str_replace(",","",$due_parts[0]));
					}
					else return "";
				}
			}
			
			$min_activity = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 14,date("Y")));
			$min_payment_datetime = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 45,date("Y")));
			$max_created = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 60,date("Y")));
			$min_order_datetime = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 14,date("Y")));
			
			$t1_code = "";
			$t2_code = "";
			$t1_count = 0;
			$t2_count = 0;
			
			$tdue = 0;
			$tcredit = 0;
			
			echo "<table>";
			echo "<tr><td>&nbsp;</td><td>Owed</td><td width='20'>&nbsp;<td>Credit</td></tr>";
			//echo "select * from `poslavu_MAIN_db`.`restaurants` where `last_activity`>'[1]' and `created`<'[2]' limit 20<br>";
			$recent_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `last_activity`>'[1]' and `created`<'[2]' and `dev`!='1' and `demo`!='1' and `disabled`!='1' and `disabled`!='2' and `disabled`!='3' and `licensed_for_locations` < 2 and `created`>='2012-01-01 00:00:00'",$min_activity,$max_created);
			while($recent_read = mysqli_fetch_assoc($recent_query))
			{
				$dataname = $recent_read['data_name'];
				$restaurantid = $recent_read['id'];
				$include_row = false;
				$rowbgcolor = "#ffffff";
				$owes = false;
				
				$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[1]' or `restaurantid`='[2]'",$dataname,$restaurantid);
				if(mysqli_num_rows($status_query))
				{
					$status_read = mysqli_fetch_assoc($status_query);
					$due_info = $status_read['due_info'];
					$total_due = get_due_prop("Total",$due_info);
					$total_unapplied = get_due_prop("Unapplied Payments",$due_info);
					
					if($total_due > 0 || $total_unapplied > 0)
					{
						$rdb = "poslavu_" . $dataname . "_db";
						$recent_order_query = mlavu_query("select `id` from `[1]`.`orders` where `opened`>='[2]' limit 1",$rdb,$min_order_datetime);
						if(!mysqli_num_rows($recent_order_query))
						{
							$recent_orders = false;
						}
						else
						{
							$recent_orders = true;
						}
						
						if($recent_orders)
						{
							$tdue += ($total_due * 1);
							$tcredit += ($total_unapplied * 1);
							echo "<tr><td>$dataname</td><td>$total_due</td><td>&nbsp;</td><td>$total_unapplied</td>";
							echo "<td><a href='https://admin.poslavu.com/sa_cp/billing/index.php?dn=$dataname' target='_blank'>(Billing)</a></td>";
							echo "</tr>";
						}
					}					
				}
			}
			echo "</table>";
			
			echo "<br><br>Total Due: $tdue";
			echo "<br><br>Total Credits: $tcredit";
		}
		else if($tool=="check_recent_hosting")
		{
			$min_activity = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 14,date("Y")));
			$min_payment_datetime = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 45,date("Y")));
			$max_created = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 60,date("Y")));
			$min_order_datetime = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 14,date("Y")));
			
			$t1_code = "";
			$t2_code = "";
			$t1_count = 0;
			$t2_count = 0;
			
			//echo "select * from `poslavu_MAIN_db`.`restaurants` where `last_activity`>'[1]' and `created`<'[2]' limit 20<br>";
			$recent_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `last_activity`>'[1]' and `created`<'[2]' and `dev`!='1' and `demo`!='1' and `disabled`!='1' and `disabled`!='2' and `disabled`!='3'",$min_activity,$max_created);
			while($recent_read = mysqli_fetch_assoc($recent_query))
			{
				$dataname = $recent_read['data_name'];
				$restaurantid = $recent_read['id'];
				$include_row = false;
				$rowbgcolor = "#ffffff";
				$owes = false;
				
				$rowstr = "";
				$rowstr .= "<tr bgcolor='[rowbgcolor]'>";
				$rowstr .= "<td>[count]</td>";
				$rowstr .= "<td>" . $dataname . "</td>";
				$rowstr .= "<td>[current_package]</td>";
				$payment_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_amount`*1<'200'*1 and `x_response_code`='1' and `datetime` > '[2]'",$restaurantid,$min_payment_datetime);
				if(!$payment_query)
				{
					$rowstr .= mlavu_dberror();
				}
				else
				{
					$rowstr .= "<td>";
					if(mysqli_num_rows($payment_query))
					{
						$rowstr .= "<font style='color:#008800'>Payment Found</font>";
					}
					else
					{
						$rowstr .= "<font style='color:#880000'>Payment Not Found</font>";
						$include_row = true;
						
						$rdb = "poslavu_" . $dataname . "_db";
						$recent_order_query = mlavu_query("select `id` from `[1]`.`orders` where `opened`>='[2]' limit 1",$rdb,$min_order_datetime);
						if(!mysqli_num_rows($recent_order_query))
						{
							$include_row = false;
						}
					}
					$rowstr .= "</td>";
					$rowstr .= "<td>";
					
					$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[1]' or `restaurantid`='[2]'",$dataname,$restaurantid);
					if(mysqli_num_rows($status_query))
					{
						$status_read = mysqli_fetch_assoc($status_query);
						$due_info = $status_read['due_info'];
						if(strpos($due_info,"Total: $")!==false && strpos($due_info,"Total: $0")===false)
						{
							$rowbgcolor = "#cccccc";
							$owes = true;
						}
						
						$rowstr .= " " . $due_info;
					}
					$rowstr .= "</td>";
				}
				$rowstr .= "</tr>";
				
				$rowstr = str_replace("[rowbgcolor]",$rowbgcolor,$rowstr);
				
				if($include_row) 
				{
					$package_query = mlavu_query("select `package` from `poslavu_MAIN_db`.`signups` where `dataname`='[1]' and `dataname`!=''",$dataname);
					if(mysqli_num_rows($package_query))
					{
						$package_read = mysqli_fetch_assoc($package_query);
						$current_package = $package_read['package'];
					}
					else $current_package = "no signup record";
					$rowstr = str_replace("[current_package]",$current_package,$rowstr);
					
					if($owes)
					{
						$t1_count++;
						$rowstr = str_replace("[count]",$t1_count,$rowstr);
						$t1_code .= $rowstr;
					}
					else
					{
						$t2_count++;
						$rowstr = str_replace("[count]",$t2_count,$rowstr);
						$t2_code .= $rowstr;
					}
				}
			}
			echo "<table cellspacing=0 cellpadding=4>";
			echo $t1_code;
			echo "</table>";
			echo "<br><br>";
			echo "<table cellspacing=0 cellpadding=4>";
			echo $t2_code;
			echo "</table>";
		}
		else if($tool=="universal_lls_update")
		{
			echo "<br>";
			return;
			
			$itlist = array();
			$itlist[] = "orders: opened,closed,order_id,ioid,repoened_datetime,reclosed_datetime";
			$itlist[] = "order_contents: order_id";
			$itlist[] = "cc_transactions: order_id,datetime,internal_id";
			
			function get_last_time_lls_synced($cust_read)
			{
				$desc = "Unknown";
				$elapsed_sync = -1;
				$lastsync_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='last sync request'", $cust_read['data_name']);
				if(mysqli_num_rows($lastsync_query))
				{
					$lastsync_read = mysqli_fetch_assoc($lastsync_query);
					//echo "<br>Last Sync: " . $lastsync_read['value'];
					
					$lastsync_time = $lastsync_read['value'];
					if(strpos($lastsync_time,"/"))
					{
						$lastsync_parts = explode("/",$lastsync_time);
						$lastsync_time = $lastsync_parts[0];
						$lastsync_interval = $lastsync_parts[1];
					}
					else $lastsync_interval = 60;
					//echo "<br>Last Sync TS: " . $lastsync_time;
					//echo "<br>Last Sync Interval: " . $lastsync_interval;
					//$lastsync_ts = create_timestamp_from_datetime($lastsync_read['value']);
					
					$elapsed_sync = time() - $lastsync_time * 1;
					if($elapsed_sync >= 60)
					{
						if($elapsed_sync >= 60 * 60 * 24)
						{
							$days_ago = floor($elapsed_sync / (60 * 60 * 24));
							$time_ago = $days_ago . " day";
							if($days_ago > 1) $time_ago .= "s";
							$time_ago .= " ago";
						}
						else if($elapsed_sync >= 60 * 60)
						{
							$hours_ago = floor($elapsed_sync / (60 * 60));
							$time_ago = $hours_ago . " hour";
							if($hours_ago > 1) $time_ago .= "s";
							$time_ago .= " ago";
						}
						else
						{
							$minutes_ago = floor($elapsed_sync / 60);
							$time_ago = $minutes_ago . " minute";
							if($minutes_ago > 1) $time_ago .= "s";
							$time_ago .= " ago";
						}
						$desc = "Last Time Synced: " . date("Y-m-d h:i:s a",$lastsync_read['value']) . " (".$time_ago.")";

						if($elapsed_sync > 240)
							$desc = "<font color='#880000'>$desc</font>";
						else
							$desc = "<font color='#888800'>$desc</font>";
					}
					else
					{
						$desc = "Synced " . $elapsed_sync . " second";
						if($elapsed_sync!=1) $desc .= "s";
						$desc .= " ago";
						$desc = "<font color='#008800'>$desc</font>";
					}
				}
				
				return array("elapsed"=>$elapsed_sync,"description"=>$desc);
			}
			
			function find_in_file_structure($rest_read,$term)
			{
				$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
				$fexist_query = mlavu_query("select count(*) as `count` from `[1]`.`config` where setting='local_file_structure' and value_long LIKE '%[2]%'",$rdb,$term);
				if(mysqli_num_rows($fexist_query))
				{
					$fexist_read = mysqli_fetch_assoc($fexist_query);
					$fexist_count = $fexist_read['count'];
				}
				else $fexist_count = 0;
				
				return $fexist_count;
			}
			
			//find_file_by_prop($rest_read,"sync_exec.php","mindate","2013-04-25 00:00:00");
			function find_file_by_prop($rest_read,$term,$prop_type,$prop_val)
			{
				$fexist = false;
				$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
				$fexist_query = mlavu_query("select * from `[1]`.`config` where setting='local_file_structure'",$rdb);
				if(mysqli_num_rows($fexist_query))
				{
					$fexist_read = mysqli_fetch_assoc($fexist_query);
					$fstr = $fexist_read['value_long'];
					$fstr_parts = explode($term . " ",$fstr);
					if(count($fstr_parts) > 1)
					{
						$fstr_parts = explode("\n",$fstr_parts[1]);
						$fstr_parts = explode("|",$fstr_parts[0]);
						
						for($n=0; $n<count($fstr_parts); $n++)
						{
							$prop_size = trim($fstr_parts[1]) * 1;
							$prop_datetime = trim($fstr_parts[2]);
							
							if($prop_type=="mindate")
							{
								if($prop_datetime >= $prop_val)
									return true;
							}
							else if($prop_type=="datetime")
							{
								if($prop_datetime==$prop_val)
									return true;
							}
							else if($prop_type=="size")
							{
								if($prop_size==$prop_val)
									return true;
							}
						}
					}
				}
				
				return $fexist;
			}
			
			$done_count = 0;
			$lls_count = 0;
			$process_limit = 400;
			$process_count = 0;
			$lls_query = mlavu_query("select * from `poslavu_MAIN_db`.`local_servers` where `encryption_type`='ioncube' and `last_checked_in` > '[1]' order by `data_name` asc",mktime(0,0,0,4,1,2013));
			while($lls_read = mysqli_fetch_assoc($lls_query))
			{
				$company_dataname = $lls_read['data_name'];
				$company_locationid = $lls_read['location_id'];
				$company_database = "poslavu_" . $company_dataname . "_db";
				
				if($process_count < $process_limit)
				{
					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `created` >= '2011-10-01 00:00:00' and `notes` NOT LIKE '%AUTO-DELETED%'",$company_dataname);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						$process_count++;
						
						$arguments = $company_database . "|" . implode(";", $itlist);
						
						$lls_count++;
						echo $lls_count . ": " . substr($rest_read['created'],0,10) . " update for " . $company_dataname;
						echo " (" . floor((time() - $lls_read['last_checked_in']) / 60 / 60 / 24) . " days since last checkin) ";
						
						//--------------------------------- begin check writable -------------------------------//
						/*$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
						$fexist_query = mlavu_query("select count(*) as `count` from `[1]`.`config` where setting='local_file_structure' and value_long LIKE '%sync_exec_n1%'",$rdb);
						if(mysqli_num_rows($fexist_query))
						{
							$fexist_read = mysqli_fetch_assoc($fexist_query);
							$fexist_count = $fexist_read['count'];
						}
						else $fexist_count = 0;
		
						if($fexist_count > 0)
						{
							echo " <b><font color='#008800'>Writable</font></b>";
						}
						else
						{
							// To check ability to write local files
							schedule_msync($company_dataname,$company_locationid,"copy remote file","lls-ioncube/local/sync_exec.php","../local/sync_exec_n1.php");
							schedule_msync($company_dataname,$company_locationid,"read file structure");
						}*/
						//---------------------------------- end check writable ------------------------------------//
						$lttsync = get_last_time_lls_synced($rest_read);
						echo " " . $lttsync['description'];
						
						$ion_count = find_in_file_structure($rest_read,"ioncube");
						$zen_count = find_in_file_structure($rest_read,"zend");
						$structure_count = find_in_file_structure($rest_read,"-local");
						//$sync_exec_date_found = find_in_file_structure($rest_read,"2013-04-25 15:09:02");
						//$sync_exec_size_found = find_in_file_structure($rest_read,"61104");
						$new_sync_exec_found = find_file_by_prop($rest_read,"/poslavu-local/local/sync_exec.php","mindate","2013-04-25 00:00:00");
						
						//To direct sync_exec traffic to api.poslavu.com
						/*if($structure_count < 1)
						{
							echo " (cannot confirm ioncube - local structure not present)";
							schedule_msync($company_dataname,$company_locationid,"read file structure");
						}
						else if($ion_count < 1)
						{
							echo " (cannot confirm ioncube)";
						}
						else if($zen_count > 0)
						{
							echo " (cannot confirm ioncube - zendguard is present)";
						}
						else if($ion_count > 0 && $zen_count < 1)*/
						if (1) {
							//echo " (ioncube)<br>";
							/*$sched_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='api sync scheduled'",$rest_read['data_name']);
							if(mysqli_num_rows($sched_query))
							{
								if($new_sync_exec_found)
								{
									echo " (done)";
									$done_count++;
									
									//echo " - resyncing health_check";
									//schedule_msync($company_dataname,$company_locationid,"copy remote file","lls-ioncube/local/health_check.php","../local/health_check.php");
									//schedule_msync($company_dataname,$company_locationid,"read file structure");
								}
								else
								{
									echo " (scheduled)";
								}
							}
							else*/
							if (1) {
								if($lttsync['elapsed'] > 60)
								{
									echo " (cannot schedule - lls is offline)";
								}
								else
								{
									echo " <b>(scheduling)</b>";
									//schedule_msync($company_dataname,$company_locationid,"run local query","devices","TRUNCATE `devices`");
									schedule_msync($company_dataname,$company_locationid,"sync database");
									//schedule_msync($company_dataname,$company_locationid,"copy remote file","lls-ioncube/cp/resources/rfix_functions.php","../cp/resources/rfix_functions.php");
									//schedule_msync($company_dataname,$company_locationid,"copy remote file","lls-ioncube/local/health_check.php","../local/health_check.php");
									//schedule_msync($company_dataname,$company_locationid,"copy remote file","lls-ioncube/local/sync_exec.php","../local/sync_exec.php");
									//schedule_msync($company_dataname,$company_locationid,"read file structure");
									//schedule_msync($company_dataname,$company_locationid,"api sync scheduled");
								}
							}
						}
						echo "<br><br>";				
						
						//For indexing
						//schedule_msync($company_dataname,$company_locationid,"copy remote file","lls-ioncube/local/health_check.php","../local/health_check.php");
						//schedule_lls_special_command($company_dataname,$company_locationid,"index_tables",$arguments);
					}
				}
			}
			echo "Amount Done: " . $done_count . "<br><br>";
			
			/*if(function_exists("schedule_msync"))
			{
				echo "schedule_msync exists<br>";
			}
			else echo "schedule_msync does not exist<br>";
			
			if(function_exists("schedule_lls_special_command"))
			{
				echo "schedule_lls_special_command exists<br>";
			}
			else echo "schedule_lls_special_command does not exist<br>";*/
			/*$company_dataname = $cust_read['data_name'];
			$company_database = "poslavu_".$company_dataname."_db";
		
			$itlist = array();
			$itlist[] = "orders: opened,closed,order_id,ioid,repoened_datetime,reclosed_datetime";
			$itlist[] = "order_contents: order_id";
			$itlist[] = "cc_transactions: order_id,datetime,internal_id";
		
			$arguments = $company_database . "|" . implode(";", $itlist);
			schedule_msync($cust_read['data_name'],$locserv_read['location'],"copy remote file","lls-ioncube/local/health_check.php","../local/health_check.php");
			schedule_lls_special_command($cust_read['data_name'],$locserv_read['location'],"index_tables",$arguments);*/
		}
		else if($tool=="centralize_created")
		{
			$rest_query = mlavu_query("select `id`,`data_name`,`created` from `poslavu_MAIN_db`.`restaurants` where `created`!='' and `last_activity`!='' and `data_name`!='' order by `id` asc");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				$created = $rest_read['created'];
				$dataname = $rest_read['data_name'];
				$restid = $rest_read['id'];
				
				$success = mlavu_query("update `poslavu_MAIN_db`.`restaurant_locations` set `created`='[1]' where `dataname`='[2]' and `restaurantid`='[3]'",$created,$dataname,$restid);
				if($success)
				{
					echo "<font style='color:#008800'>set $dataname to $created</font><br>";
				}
				else
				{
					echo "<font style='color:#880000'>failed: $dataname</font><br>";
				}
			}
		}
		else if($tool=="read_med_customer_usage")
		{
			$total_customers = 0;
			$most_customers = 0;
			$most_customer_dataname = "";
			$total_locations = 0;
			
			$most_customer_dataname = "";//rustic_burger1";
			
			if($most_customer_dataname=="")
			{
				$month_ago = date("Y-m-d H:i:s",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
				$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and (`disabled`='' or `disabled`='0') and `lavu_lite_server`='' and `notes` NOT LIKE '%AUTO-DELETED%' and `last_activity` > '[1]'",$month_ago);
				while($rest_read = mysqli_fetch_assoc($rest_query))
				{				
					$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
					$customer_query = mlavu_query("select count(*) as `count` from `[1]`.`med_customers`",$rdb);
					if(mysqli_num_rows($customer_query))
					{
						$customer_read = mysqli_fetch_assoc($customer_query);
						$customer_count = $customer_read['count'];
						
						if($customer_count > 100)
						{
							$total_locations++;
							$total_customers += $customer_count * 1;
							if($customer_count * 1 > $most_customers * 1)
							{
								$most_customers = $customer_count;
								$most_customer_dataname = $rest_read['data_name'];
							}
						}
					}
				}
				
				echo "Only including locations that our active within one month<br>";
				echo "Locations with customers: ".number_format($total_locations,0)."<br>";
				echo "Total customers: ".number_format($total_customers,0)."<br>";
				echo "Most customers in one location: ".number_format($most_customers,0)." ($most_customer_dataname)<br>";
				echo "Average customers per location: " . number_format(($total_customers / $total_locations),2) . "<br>";
			}
			
			$data_usage = 0;
			$data_str = "";
			$customer_query = mlavu_query("select * from `[1]`.`med_customers`","poslavu_" . $most_customer_dataname . "_db");
			while($customer_read = mysqli_fetch_assoc($customer_query))
			{
				$create_str = "";
				foreach($customer_read as $key => $val)
				{
					if($val!="")
					{
						$data_usage += strlen($key) * 1 + 1 + strlen($val) * 1;
						if($create_str!="") $create_str .= ",";
						$create_str .= "\"" . $key . "\":\"" . str_replace("\n","",str_replace("\"","",$val)) . "\"";
					}
				}
				if($data_str!="") $data_str .= ",\n";
				$data_str .= "{".$create_str."}";
			}
			echo "Data Usage (estimated): " . number_format($data_usage,2) . "<br>";
			echo "Data Usage (estimated again): " . number_format(strlen($data_str),2) . "<br>";
			
			echo "<br><br>";
			
			echo "<textarea rows='40' cols='120'>$data_str</textarea><br><br>";
			echo "<script type='text/javascript'>";
			echo "var customers = new Array(\n" . $data_str . "\n);";
			//echo "alert('customers loaded: ' + customers.length); ";
			echo "</script>";
		}
		else if($tool=="active_in_month")
		{
			if(!isset($_SESSION['can_include_list']))
			{
				$_SESSION['can_include_list'] = array();
			}
			
			if(isset($_GET['month'])) $check_month = $_GET['month']; else $check_month = "";
			
			echo "<br><br>";
			echo "<table cellspacing=0 cellpadding=20><tr><td>";
			echo "<select onchange='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=active_in_month&month=\" + this.value'>";
			echo "<option value=''>---Select Month---</option>";
			for($m=0; $m<32; $m++)
			{
				$smonth = date("Y-m",mktime(0,0,0,date("m") - $m,1,date("Y")));
				echo "<option value='$smonth'";
				if($smonth==$check_month) echo " selected";
				echo ">$smonth</option>";
			}
			echo "</select>";
			echo "<br><br>";
			
			if($check_month > "2000-01" && $check_month < "3000-01")
			{
				$min_orders_to_include = 1;
				
				$total_count = 0;
				$with_orders_count = 0;
				$with_payments_count = 0;
				$with_payment_or_order_count = 0;
				$require_payment_to_include = true;
				$rest_id_list = '';
				
				$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `lavu_lite_server`='' and `notes` NOT LIKE '%AUTO-DELETED%'");
				while($rest_read = mysqli_fetch_assoc($rest_query))
				{
					if($require_payment_to_include)
					{
						$rstr = "R" . $rest_read['id'];
						if(isset($_SESSION['can_include_list'][$rstr]))
						{
							if($_SESSION['can_include_list'][$rstr]=="yes")
							{
								$can_include = true;
							}
							else
							{
								$can_include = false;
							}
						}
						else
						{
							$can_include = false;
							$paid_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_amount` * 1 > 8",$rest_read['id']);// and `x_type`='auth_capture' and `x_response_code`='1'",$rest_read['id']);
							if(mysqli_num_rows($paid_query))
							{
								$paid_read = mysqli_fetch_assoc($paid_query);
								if($paid_read['count'] > 0)
								{
									$can_include = true;
								}
							}
							if(!$can_include)
							{
								$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `restaurantid`='[1]'",$rest_read['id']);
								if(mysqli_num_rows($lic_query))
								{
									$can_include = true;
								}
							}
							
							if($can_include)
							{
								$setstr = "yes";
							}
							else
							{
								$setstr = "no";
							}
							$_SESSION['can_include_list'][$rstr] = $setstr;
						}
					}
					else
					{
						$can_include = true;
					}
					
					if($can_include)
					{
						$pcountstr = "pcount_".$check_month."_" . $rest_read['data_name'];
						if(isset($_SESSION['can_include_list'][$pcountstr]))
						{
							$found_payment_count = $_SESSION['can_include_list'][$pcountstr];
						}
						else
						{
							$paid_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_amount` * 1 > 8 and `x_response_code`='1' and `x_type`='auth_capture' and `date`>='[2]-01' and `date`<='[2]-31'",$rest_read['id'],$check_month);
							if(mysqli_num_rows($paid_query))
							{
								$paid_read = mysqli_fetch_assoc($paid_query);
								$paid_count = $paid_read['count'];
								
								$found_payment_count = $paid_count;
								$_SESSION['can_include_list'][$pcountstr] = $paid_count;
							}
							else
							{
								$found_payment_count = 0;
							}
						}
						
						if($found_payment_count > 0)
						{
							$with_payments_count++;
						}
						
						$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
						$ocountstr = "count" . $min_orders_to_include . "_" . $check_month . "_" . $rest_read['data_name'];
						
						if(isset($_SESSION['can_include_list'][$ocountstr]))
						{
							$order_count = $_SESSION['can_include_list'][$ocountstr];
						}
						else
						{
							$order_count = 0;
							$order_count_query = mlavu_query("select sum(`data`) as `count` from `poslavu_MAIN_db`.`restdata` where `dataname`='[1]' and `range`>='[2]-01' and `range`<='[2]-31'",$rest_read['data_name'],$check_month);
							if(mysqli_num_rows($order_count_query))
							{
								$order_count_read = mysqli_fetch_assoc($order_count_query);
								$order_count = $order_count_read['count'];
								//echo $rest_read['data_name'] . " - " . $order_count . "<br>";
							}
							
							/*$order_count_query = mlavu_query("select count(*) as `count` from `[1]`.`orders` where `closed` >= '[2]-01 00:00:00' and `closed` <= '[2]-31 24:00:00'",$rdb,$check_month);
							if(mysqli_num_rows($order_count_query))
							{
								$order_count_read = mysqli_fetch_assoc($order_count_query);
								$order_count = $order_count_read['count'];
							}*/
							$_SESSION['can_include_list'][$ocountstr] = $order_count;
						}
							
						if($order_count >= $min_orders_to_include)
						{
							$found_orders = true;
						}
						else
						{
							$found_orders = false;
						}
						
						if($found_orders)
						{
							$with_orders_count++;
						}
						
						if($found_payment_count > 0 || $found_orders)
						{
							$with_payment_or_order_count++;
							$rest_id_list = empty($rest_id_list) ? $rest_read['id'] : $rest_id_list .", ". $rest_read['id'];
						}
						$total_count++;
					}
				}
				
				echo "Date: " . $check_month . "<br>";
				echo "Total Count: $total_count<br>";
				echo "With Orders Count: $with_orders_count<br>";
				echo "With Payments Count: $with_payments_count<br>";
				echo "With Payments or Orders Count: $with_payment_or_order_count<br>";
				echo "<br><br>Restaurant IDs: $rest_id_list<br>";
			}
			
			echo "</td></tr></table>";
		}
		else if($tool=="active_by_month")
		{
			require_once("/home/poslavu/public_html/admin/sa_cp/billing/payment_profile_functions.php");
			ini_set("memory_limit","800M");
			set_time_limit(600);
			
			$output_str = true;
			$min_orders_to_include = 1;
			$require_payment_to_include = true;
			$include_dataname = true;
			$include_lifetime = true;
			
			$total_accounts_active = 0;
			$total_still_active = 0;
			$total_no_longer_active = 0;
			$total_months_active = 0;
			$total_months_active_for_still_active_only = 0;
			$total_months_active_for_no_longer_active_only = 0;
			$total_owed = 0;
			
			$start_ts = time();
			$months_active = array();
			$months = array();
			for($m=1; $m<60; $m++)
			{
				$months[] = date("Y-m",mktime(0,0,0,date("m") - $m,date("d"),date("Y")));
			}
			
			$str = "";
			$str .= "Id";
			if($include_dataname) $str .= ",Dataname";
			$due_str = $str;
			$due_str .= ",Owed";
			if($include_lifetime) $str .= ",\"First\",\"Last\",\"Span\"";
			for($m=0; $m<count($months); $m++)
			{
				$str .= ",".$months[$m];
				$months_active[$months[$m]] = 0;
			}
			
			
						
			//$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and (`disabled`='' or `disabled`='0') and `lavu_lite_server`='' and `notes` NOT LIKE '%AUTO-DELETED%' and `last_activity` > '[1]'",$month_ago);
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `lavu_lite_server`='' and `notes` NOT LIKE '%AUTO-DELETED%'");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				if($require_payment_to_include)
				{
					$can_include = false;
					$paid_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_amount` * 1 > 8",$rest_read['id']);// and `x_type`='auth_capture' and `x_response_code`='1'",$rest_read['id']);
					if(mysqli_num_rows($paid_query))
					{
						$paid_read = mysqli_fetch_assoc($paid_query);
						if($paid_read['count'] > 0)
						{
							$can_include = true;
						}
					}
					if(!$can_include)
					{
						$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `restaurantid`='[1]'",$rest_read['id']);
						if(mysqli_num_rows($lic_query))
						{
							$can_include = true;
						}
					}
				}
				else
				{
					$can_include = true;
				}
				
				if($can_include)
				{
					$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
					if($output_str)
					{
						$addstr = "";
						$addstr .= "\n";
						$addstr .= $rest_read['id'];
						if($include_dataname) $addstr .= ",\"".str_replace("\"","",str_replace("\n","",$rest_read['data_name']))."\"";
						$str .= $addstr;
						$due_str .= $addstr;
					}
					
					$first_order_month = substr($rest_read['created'],0,7);
					$last_order_month = $first_order_month;
					
					$order_counts = array();
					$order_count_query = mlavu_query("select count(*) as `count`, left(`closed`,7) as `month` from `[1]`.`orders` group by `month` order by `month` desc",$rdb);
					if(mysqli_num_rows($order_count_query))
					{
						while($order_count_read = mysqli_fetch_assoc($order_count_query))
						{
							$order_month = trim($order_count_read['month']);
							if($order_month!="")
							{
								$order_counts[$order_month] = $order_count_read['count'] * 1;
								if($order_counts[$order_month] * 1 >= $min_orders_to_include)
								{
									if(isset($months_active[$order_month])) 
									{
										$months_active[$order_month]++;
										if($order_month > $last_order_month) $last_order_month = $order_month;
									}
								}
							}
						}
						
						$count_year = substr($first_order_month,0,4);
						$count_month = substr($first_order_month,5,2);
						$last_month_found = false;
						$find_month_counter = 0;
						while(!$last_month_found)
						{
							if(date("Y-m",mktime(0,0,0,$count_month + $find_month_counter,1,$count_year)) >= $last_order_month)
							{
								$last_month_found = true;
							}
							else
							{
								$find_month_counter++;
								if($find_month_counter > 4000) $last_month_found = true;
							}
						}
						if($find_month_counter < 4000) $lifetime = $find_month_counter; else $lifetime = 0;
						if($lifetime > 1)
						{
							$total_accounts_active ++;
							$total_months_active += $lifetime;
							if($last_order_month >= date("Y-m",mktime(0,0,0,date("m") - 2,1,date("Y")))) 
							{
								$total_still_active ++;
								$total_months_active_for_still_active_only += $lifetime;
								
								$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
								if(mysqli_num_rows($status_query))
								{
									$status_read = mysqli_fetch_assoc($status_query);
									$due_info = $status_read['due_info'];
									
									$due_total = get_due_prop("total",$due_info);
									$due_license = get_due_prop("license",$due_info);
									$due_hosting = get_due_prop("hosting",$due_info);
									$due_since = get_due_prop("due",$due_info);
									$paid_last_15 = get_due_prop("paid last 15",$due_info);
									$paid_last_31 = get_due_prop("paid last 31",$due_info);
									$last_payment_made = get_due_prop("last payment made",$due_info);
									if($due_total!="" && $due_total * 1 > 0)
									{
										$due_total = str_replace("$","",$due_total);
										$due_total = str_replace(",","",$due_total);
										$due_total = $due_total * 1;
										$total_owed += $due_total;
									}
									if($output_str)
									{
										if($due_total=="") $due_total = 0;
										$due_str .= "," . number_format($due_total,2);
									}
								}
							}
							else
							{
								$total_no_longer_active ++;
								$total_months_active_for_no_longer_active_only += $lifetime;
							}
						}
						
						if($output_str)
						{
							if($include_lifetime) 
							{
								$addstr = ",\"$first_order_month\",\"$last_order_month\",\"$lifetime months\"";
								$str .= $addstr;
							}
							for($m=0; $m<count($months); $m++)
							{
								if(isset($order_counts[$months[$m]]))
								{
									$str .= ",".$order_counts[$months[$m]];
								}
								else $str .= ",0";
							}
						}
					}
				}
			}
			
			$end_ts = time();
			$ts_elapsed = ($end_ts - $start_ts);
			$seconds = $ts_elapsed % 60;
			$minutes = ($ts_elapsed - $seconds) / 60;
			if($minutes < 10) $minutes = "0" . $minutes;
			if($seconds < 10) $seconds = "0" . $seconds;

			echo "Elapsed: " . $minutes . ":" . $seconds . "<br>";
			echo "Avg Lifetime for All: " . number_format($total_months_active / $total_accounts_active,2) . " months<br>";
			echo "Still Active: " . $total_still_active . " / " . $total_accounts_active . " ";
			echo "(Avg " . number_format($total_months_active_for_still_active_only / $total_still_active,2) . " months)<br>";
			echo "No Longer Active: " . $total_no_longer_active . " / " . $total_accounts_active . " ";
			echo "(Avg " . number_format($total_months_active_for_no_longer_active_only / $total_no_longer_active,2) . " months)<br>";
			echo "Total Owed by Active: $" . number_format($total_owed,2) . "<br>";
			
			if($output_str)
			{
				echo "<textarea rows=40 cols=120>";
				echo $str;
				echo "</textarea>";
				echo "<br><br>";
				echo "<textarea rows=40 cols=120>";
				echo $due_str;
				echo "</textarea>";
			}
			
			for($m=0; $m<count($months); $m++)
			{
				if(isset($months_active[$months[$m]]))
				{
					echo "<br>" . $months[$m] . " - " . $months_active[$months[$m]];
					if(isset($months_active[$months[$m + 1]]))
					{
						$locs_added = $months_active[$months[$m]] - $months_active[$months[$m + 1]];
						if($locs_added >= 0)
						{
							echo " (+" . $locs_added . ") ";
						}
						else
						{
							echo " (-" . abs($locs_added) . ") ";
						}
					}
				}
			}
			exit();
		}
		else if($tool=="monthlytrans")
		{
			$start_year = 2011;
			$start_month = 2;
			$month_ts = mktime(0,0,0,$start_month,1,$start_year);
			
			$month_list = array();
			while($month_ts < time())
			{
				$month_list[] = date("Y-m",$month_ts);
				$start_month++;
				if($start_month > 12)
				{
					$start_year++;
					$start_month = 1;
				}
				$month_ts = mktime(0,0,0,$start_month,1,$start_year);
			}
			
			$last_restid = "---";
			$last_months = array();
			$sum_query = mlavu_query("select format(sum(`x_amount`),2) as `total`,`match_restaurantid`, left(`date`,7) as `month` from `poslavu_MAIN_db`.`payment_responses` where `x_type`='auth_capture' and `x_amount` * 1 >= 350 and `x_response_code`='1' and ((`date`>='2000-01-01' and `date`<'2014-02-01') or (`date`>='2014-02-01' and `date`<'2014-08-15' and `batch_status`!='N') or (`date`>='2014-08-15' and `date`<='2020-12-31')) group by concat('M',match_restaurantid,'_D',left(`date`,7)) order by `match_restaurantid` * 1 asc, `match_restaurantid` asc, `month` asc");
			$exstr = "";
			//$tstr = "";
			//$tstr .= "<table cellspacing=0 cellpadding=2>";
			//$tstr .= "<tr>";
			//$tstr .= "<td>&nbsp;</td>";
			$exstr = "\t";
			for($m=0; $m<count($month_list); $m++)
			{
				//$tstr .= "<td>";
				//$tstr .= $month_list[$m];
				//$tstr .= "</td>";
				$exstr .= $month_list[$m] . "\t";
			}
			//$tstr .= "</tr>";
			$exstr .= "\n";
			while($sum_read = mysqli_fetch_assoc($sum_query))
			{
				$restid = $sum_read['match_restaurantid'];
				$month = $sum_read['month'];
				if($restid!=$last_restid)
				{
					if($last_restid!="---")
					{
						//$tstr .= "<tr>";
						//$tstr .= "<td align='right'>";
						//$tstr .= $last_restid;
						//$tstr .= "</td>";
						$exstr .= $last_restid . "\t";
						for($m=0; $m<count($month_list); $m++)
						{
							//$tstr .= "<td style='border:solid 1px black'>";
							if(isset($last_months[$month_list[$m]]))
							{
								//$tstr .= $last_months[$month_list[$m]];
								$exstr .= $last_months[$month_list[$m]];
							}
							else 
							{
								//$tstr .= "&nbsp;";
							}
							//$tstr .= "</td>";
							$exstr .= "\t";
						}
						//$tstr .= "</tr>";
						$exstr .= "\n";
					}
					$last_months = array();
				}
				$last_months[$month] = $sum_read['total'];
				$last_restid = $restid;
			}
			//$tstr .= "</table>";
			
			/*$exstr = $tstr;
			$exstr = str_replace("</tr>","\n",$exstr);
			$exstr = str_replace("</td>","\t",$exstr);
			$exstr = strip_tags($exstr);*/
			
			echo "<textarea rows='40' cols='120'>";
			echo $exstr;
			echo "</textarea>";
			echo "<br><br><br>...";
		}
		else if($tool=="read_cc_users")
		{
			$daterange = (isset($_GET['daterange']))?$_GET['daterange']:"";
			if($daterange=="" && isset($_SESSION['daterange'])) $daterange = $_SESSION['daterange'];
			if($daterange=="") $daterange = date("Y-m");
			$_SESSION['daterange'] = $daterange;
			
			$filter = (isset($_GET['filter']))?$_GET['filter']:"";			
			if($filter=="None") $filter = "";
			
			echo "<br><br>";
			echo "<select name='daterange_select' onchange='window.location = \"/sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=read_cc_users&filter=$filter&daterange=\" + this.value'>";
			for($i=0; $i<=60; $i++)
			{
				$dateset = date("Y-m",mktime(0,0,0,date("m")-$i,1,date("Y")));
				echo "<option value='$dateset'";
				if($dateset==$daterange) echo " selected";
				echo ">$dateset</option>";
			}
			for($i=0; $i<=5; $i++)
			{
				$dateset = date("Y",mktime(0,0,0,date("m"),1,date("Y")-$i));
				echo "<option value='$dateset'";
				if($dateset==$daterange) echo " selected";
				echo ">--- $dateset ---</option>";
			}
			echo "</select>";
			echo "<br><br>";
			
			//Heartland, BridgePay, EVO Snap & Moneris
			$filters_available = array("None","Mercury","BridgePay","Heartland","EVO Snap","Moneris","Other","Mercury Single Terminal","Single Terminal","Gold","Pro and Platinum","All");
			echo "<table cellspacing=0 cellpadding=12>";
			echo "<tr>";
			for($i=0; $i<count($filters_available); $i++)
			{
				$f = $filters_available[$i];
				if($filter==$f)
				{
					$setcolor = "#eeeeee";
				}
				else
				{
					$setcolor = "#cccccc";
				}
				echo "<td align='center' style='border:solid 1px #aaaaaa; cursor:pointer' bgcolor='$setcolor' onclick='window.location = \"/sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=read_cc_users&filter=$f\"'>$f</td>";
				
			}
			echo "</tr>";
			echo "</table>";
			
			$all_trans_total = 0;
			$all_trans_count_total = 0;
			
			//$month_ago = date("Y-m-d H:i:s",mktime(0,0,0,date("m")-1,date("d"),date("Y")));
			//$month_ago_description = "past month";
			//$max_month = "2040-01-01 00:00:00";
			
			//$month_ago = "2014-01-00 00:00:00";
			//$max_month = "2014-12-32 00:00:00";
			//$month_ago_description = "2014";

			$daterange_parts = explode("-",$daterange);
			if(count($daterange_parts)==2)
			{
				$daterange_year = $daterange_parts[0];
				$daterange_month = $daterange_parts[1];
				
				$month_ago = date("Y-m-d H:i:s",mktime(0,0,0,$daterange_month,1,$daterange_year));
				$max_month = date("Y-m-d H:i:s",mktime(23,59,59,$daterange_month,31,$daterange_year));
				$month_ago_description = "$daterange_month/$daterange_year";
			}
			else if(count($daterange_parts)==1)
			{
				$daterange_year = $daterange_parts[0];
				
				$month_ago = date("Y-m-d H:i:s",mktime(0,0,0,1,1,$daterange_year));
				$max_month = date("Y-m-d H:i:s",mktime(23,59,59,12,31,$daterange_year));
				$month_ago_description = "$daterange_year";
			}
			else
			{
				$month_ago = date("Y-m-d H:i:s",mktime(0,0,0,date("m")-6,date("d"),date("Y")));
				$max_month = "2040-01-01 00:00:00";
				$month_ago_description = "past 6 months";
			}
			
			$mercury_locs = array();
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and (`disabled`='' or `disabled`='0') and `lavu_lite_server`='' and `notes` NOT LIKE '%AUTO-DELETED%' and `last_activity` > '[1]'",$month_ago);
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				//Heartland, BridgePay, EVO Snap & Moneris
				$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
				if(strpos($filter,"Mercury")!==false)
				{
					$loc_query = mlavu_query("select * from `[1]`.`locations` where `gateway` LIKE '%merc%' and `monitary_symbol`='"."$"."' and (`country` LIKE '%USA%' or `country` LIKE '%united states%')",$rdb);
				}
				else if(strpos($filter,"BridgePay")!==false)
				{
					$loc_query = mlavu_query("select * from `[1]`.`locations` where `gateway` LIKE '%tgate%' and `monitary_symbol`='"."$"."' and (`country` LIKE '%USA%' or `country` LIKE '%united states%')",$rdb);
				}
				else if(strpos($filter,"Heartland")!==false)
				{
					$loc_query = mlavu_query("select * from `[1]`.`locations` where `gateway` LIKE '%heart%' and `monitary_symbol`='"."$"."' and (`country` LIKE '%USA%' or `country` LIKE '%united states%')",$rdb);
				}
				else if(strpos($filter,"EVO Snap")!==false)
				{
					$loc_query = mlavu_query("select * from `[1]`.`locations` where `gateway` LIKE '%evo%' and `monitary_symbol`='"."$"."' and (`country` LIKE '%USA%' or `country` LIKE '%united states%')",$rdb);
				}
				else if(strpos($filter,"Moneris")!==false)
				{
					$loc_query = mlavu_query("select * from `[1]`.`locations` where `gateway` LIKE '%moneris%' and `monitary_symbol`='"."$"."' and (`country` LIKE '%USA%' or `country` LIKE '%united states%')",$rdb);
				}
				else if(strpos($filter,"Other")!==false)
				{
					$loc_query = mlavu_query("select * from `[1]`.`locations` where `gateway` NOT LIKE '%moneris%' and `gateway` NOT LIKE '%evo%' and `gateway` NOT LIKE '%heart%' and `gateway` NOT LIKE '%tgate%' and `gateway` NOT LIKE '%merc%' and `monitary_symbol`='"."$"."' and (`country` LIKE '%USA%' or `country` LIKE '%united states%')",$rdb);
				}
				else if($filter=="All")
				{
					$loc_query = mlavu_query("select * from `[1]`.`locations` where (`country` LIKE '%USA%' or `country` LIKE '%united states%')",$rdb);// where `gateway` LIKE '%merc%'",$rdb);
				}
				else if($filter!="")
				{
					$loc_query = mlavu_query("select * from `[1]`.`locations` where `monitary_symbol`='"."$"."' and (`country` LIKE '%USA%' or `country` LIKE '%united states%')",$rdb);// where `gateway` LIKE '%merc%'",$rdb);
				}
				else $loc_query = false;
				
				if($loc_query && mysqli_num_rows($loc_query))
				{
					$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$rest_read['data_name']);
					if(mysqli_num_rows($signup_query))
					{
						$signup_read = mysqli_fetch_assoc($signup_query);
						$package = trim(strtolower($signup_read['package']));
						$can_include = false;
						
						if(strpos($filter,"Single Terminal")!==false)
						{
							if($package=="1" || $package=="11" || $package=="21" || $package=="8" || $package=="88" || $package=="silver" || strpos($package,"88")!==false || strpos($package,"silver")!==false)
							{
								$can_include = true;
							}
						}
						else if(strpos($filter,"Gold")!==false)
						{
							if($package=="2" || $package=="12" || $package=="22" || $package=="gold" || strpos($package,"gold")!==false)
							{
								$can_include = true;
							}
						}
						else if(strpos($filter,"Pro")!==false || strpos($filter,"Platinum")!==false)
						{
							if($package=="3" || $package=="13" || $package=="23" || $package=="pro" || strpos($package,"pro")!==false || $package=="platinum" || strpos($package,"platinum")!==false)
							{
								$can_include = true;
							}
						}
						else
						{
							$can_include = true;
						}
						
						if($can_include)
						{
							while($loc_read = mysqli_fetch_assoc($loc_query))
							{
								$loc_id = $loc_read['id'];
								$sess_varid = "total_".$rest_read['data_name']."_" . $loc_id;
											
								if(1==2 && isset($_SESSION[$sess_varid]))
								{
									$str_parts = explode("|",$_SESSION[$sess_varid]);
									if(count($str_parts) > 1)
									{
										$trans_total = $str_parts[0];
										$trans_count = $str_parts[1];
									}
								}
								else
								{
									$trans_total = 0;
									$trans_query = mlavu_query("select count(*) as `count`, sum(`amount`) as `amount` from `[1]`.`cc_transactions` where `refunded`!='1' and `action`='Sale' and `datetime`>'[2]' and `datetime`<='[3]' and `loc_id`='[4]'",$rdb,$month_ago,$max_month,$loc_id);
									if(mysqli_num_rows($trans_query))
									{
										$trans_read = mysqli_fetch_assoc($trans_query);
										$trans_total = $trans_read['amount'];
										$trans_count = $trans_read['count'];
										
									}
								}
								
								if($trans_total * 1 > 200 && $trans_total < 2000000)
								{
									$all_trans_total += $trans_total * 1;
									$all_trans_count_total += $trans_count * 1;
									$mercury_locs[] = array_merge($loc_read,array('total_cc_transactions'=>$trans_total,"trans_count"=>$trans_count));
									$_SESSION[$sess_varid] = $trans_total . "|" . $trans_count;
								}
								//$mercury_locs[] = array_merge($loc_read,array("total_cc_transactions"=>$trans_total));
								//echo $rest_read['data_name'] . "<br>";
							}
						}
					}
				}
			}
			
			echo "<br><br>";
			if(count($mercury_locs) > 0)
			{
				echo "USA Locations: " . count($mercury_locs) . "<br>";
				echo "Transaction Count for $filter Locations for $month_ago_description: " . number_format($all_trans_count_total,0) . "<br>";
				echo "All Transactions for $filter Locations for $month_ago_description: $" . number_format($all_trans_total,2) . "<br>";
				echo "Average Transactions per $filter Location for $month_ago_description: $" . number_format(($all_trans_total / count($mercury_locs)),2) . "<br>";
				echo "<br>";
				echo "<table cellspacing=0 cellpadding=4>";
				for($i=0; $i<count($mercury_locs); $i++)
				{
					echo "<tr><td align='right'>" . $mercury_locs[$i]['title'] . "</td><td align='right'>" . number_format($mercury_locs[$i]['trans_count'],0) . "</td><td align='right'>$" . number_format($mercury_locs[$i]['total_cc_transactions'],2) . "</td></tr>";
				}
				echo "</table>";
			}
			else
			{
				echo "Nothing Found<br>";
			}
			echo "<br>";
		}
		else if($tool=="centralize_locations")
		{
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and (`disabled`='' or `disabled`='0') and `lavu_lite_server`='' and `notes` NOT LIKE '%AUTO-DELETED%' and `created` > '2013-07-01 00:00:00'");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
				$loc_query = mlavu_query("select * from `[1]`.`locations`",$rdb);
				while($loc_read = mysqli_fetch_assoc($loc_query))
				{
					update_restaurant_location_data($rest_read,$loc_read);
					echo $rest_read['data_name'] . "<br>";
				}
			}
		}
		else if($tool=="batches")
		{			
			require_once(dirname(__FILE__) . "/batches.php");
			analyze_batch_data();
		}
		else if($tool=="uncaptured")
		{
			require_once(dirname(__FILE__) . "/batches.php");
			create_uncaptured_transaction_interface();
			//analyze_uncaptured_data();
		}
		else if($tool=="mtest")
		{
			echo "mtest<br>";
		}
		else if($tool=="monthly_payments")
		{			
			require_once(dirname(__FILE__) . "/monthly.php");
			output_monthly_reports();
		}
		else if($tool=="transferauth")
		{
			require_once(dirname(__FILE__) . "/transferauth.php");
			run_transferauth();
		}
		else if($tool=="butter")
		{
			if(isset($_GET['authlist']) && $_GET['authlist']==1)
			{
				echo "<textarea rows='80' cols='80'>";
				$total = 0;
				$rsp_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_type`='auth_capture' or `x_type`='credit' order by `date` asc, `time` asc");
				while($rsp_read = mysqli_fetch_assoc($rsp_query))
				{
					if($rsp_read['x_amount'] * 1 > 2.0)
					{
						$restid = $rsp_read['match_restaurantid'];
						$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]' and `distro_code`!=''",$restid);
						if(mysqli_num_rows($rest_query))
						{
							$rest_read = mysqli_fetch_assoc($rest_query);
							$distro_query = mlavu_query("select `id` from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$rest_read['distro_code']);
							if(mysqli_num_rows($distro_query))
							{
								$distro_read = mysqli_fetch_assoc($distro_query);
								$distroid = $distro_read['id'];
							}
						}
						else
						{
							$distroid = 0;
						}
						
						if(!is_numeric($restid))
						{
							$restid = "0";
						}
						if(!is_numeric($distroid))
						{
							$distroid = "0";
						}
						
						echo $rsp_read['date'] . " " . $rsp_read['time'] . ", " . $rsp_read['x_amount'] . ", " . $rsp_read['x_type'] . ", " . $restid . ", " . $distroid . "\n";
						if($rsp_read['x_type']=="auth_capture") $total += $rsp_read['x_amount'] * 1;
						else if($rsp_read['x_type']=="credit") $total -= $rsp_read['x_amount'] * 1;
					}
				}
				echo "</textarea>";
				echo "<br>$" . number_format($total,2,".",",") . "<br>";
				exit();
			}
			
			//-------------------------------------------- main butter start --------------------------------//
			$distro_mode = false;
			
			//$distro_mode = true;
			//$distro_code_query = mlavu_query("select distinct `distro_code` as `distro_code` from `poslavu_MAIN_db`.`restaurants` where `distro_code`!='' order by `id` asc");
			//while($distro_code_read = mysqli_fetch_assoc($distro_code_query))
			if(true)
			{
				if($distro_mode)
				{
					$main_query = "select * from `poslavu_MAIN_db`.`restaurants` where `created`!='' and `last_activity`!='' and `data_name`!='' and `distro_code`='[distro_code]' order by `id` asc";
					$main_vars = array("distro_code"=>$distro_code_read['distro_code']);
					$main_title = $distro_code_read['distro_code'];
				}
				else
				{
					$main_query = "select * from `poslavu_MAIN_db`.`restaurants` where `created`!='' and `last_activity`!='' and `data_name`!='' and `created` > '2010-06-01 00:00:00' order by `id` asc";
					$main_vars = array();
					$main_title = "View All";
				}
				
				$last_act = array();
				$created = array();
				$str = "";
				
				$rest_query = mlavu_query($main_query,$main_vars);
				if(mysqli_num_rows($rest_query))
				{
					$str .= "<br><br><b>" . $main_title . "</b><hr>";
				}
								
				while($rest_read = mysqli_fetch_assoc($rest_query))
				{
					$la = substr($rest_read['last_activity'],0,7);
					$cr = substr($rest_read['created'],0,7);
					$cr2 = substr($rest_read['created'],0,10);
					$dn = $rest_read['data_name'];
					$auto_deleted = strpos($rest_read['notes'],"AUTO-DELETED");
					$rdb = "poslavu_" . $dn . "_db";
								
					$disabled = false;
					if($rest_read['disabled']!="" && $rest_read['disabled']!=0)
						$disabled = true;
					
					if($rest_read['lavu_lite_server']!="") $lavu_lite_account = true;
					else $lavu_lite_account = false;
					
					if($lavu_lite_account)
						$min_pay_amount = 20;
					else
						$min_pay_amount = 300;
					
					$pay_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `x_type`='auth_capture' and `x_amount` > [1] and `match_restaurantid`='[2]'",$min_pay_amount,$rest_read['id']);
					if(mysqli_num_rows($pay_query))
					{
						$pay_read = mysqli_fetch_assoc($pay_query);
						$pcount = $pay_read['count'];
						
						if($pcount < 1 && !$lavu_lite_account)
						{
							$lic_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`reseller_licenses` where `restaurantid`='[1]'",$rest_read['id']);
							if(mysqli_num_rows($lic_query))
							{
								$lic_read = mysqli_fetch_assoc($lic_query);
								$pcount += $lic_read['count'] * 1;
							}
						}
						
						$_16_days_ago = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") - 16,date("Y")));
						$_16_days_ago2 = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 16,date("Y")));
						
						//echo $rest_read['data_name'] . " - " . $pcount . " - " . $cr . " - " . $_16_days_ago . "<br>";
						if($pcount > 0 || $cr2 > $_16_days_ago2)
						{
							$last_order_found = false;
							if(!$auto_deleted && !$lavu_lite_account)
							{
								$last_order_query = mlavu_query("select `closed` from `[1]`.`orders` where `closed` > '2000-00-00 00:00:00' and `closed` < '2020-00-00 00:00:00' order by `closed` desc limit 1",$rdb);
								if(mysqli_num_rows($last_order_query))
								{
									$last_order_read = mysqli_fetch_assoc($last_order_query);
									$last_order_found = true;
									$la = substr($last_order_read['closed'],0,7);
								}
							}
							
							if(!isset($last_act[$la])) $last_act[$la] = array();
							if(!isset($created[$cr])) $created[$cr] = array();
							if($disabled)
								$last_act[$la][] = $dn;
							$created[$cr][] = $dn;
						}
					}
				}
				
				/*$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `cost` > 100 order by `purchased` asc");
				if(mysqli_num_rows($lic_query))
				{
					$lic_read = mysqli_fetch_assoc($lic_query);
					$purchased = $lic_read['purchased'];
					$cr = substr($purchased,0,7);
					$dn = "Reseller:  " . $lic_read['resellername'] . "\nRestaurantId: " . $lic_read['restaurantid'];
					
					if($cr!="")
					{
						if(!isset($created[$cr])) $created[$cr] = array();
						$created[$cr][] = $dn;
					}
				}*/
				
				$total_active = 0;
				$total_used = 0;
				$str .= "<br>";
				$str .= "<table>";
				foreach($created as $key => $val)
				{
					$str .= "<tr>";
					$str .= "<td>created - " . $key . " = <font style='color:#008800'><b>" . count($val) . "</b></font></td>";
					$last_act_count = 0;
					if(isset($last_act[$key]))
						$last_act_count = count($last_act[$key]);
					$str .= "<td>disabled - " . $last_act_count . "</td>";
					
					$last_total_active = $total_active;
					$total_active += (count($val) * 1) - ($last_act_count * 1);
					$total_used += (count($val) * 1);
					$str .= "<td>change - +" . ($total_active - $last_total_active) . "</td>";
					$str .= "<td>active - " . $total_active . "</td>";
					$str .= "<td>used - " . $total_used . "</td>";
					$str .= "</tr>";
				}
				$str .= "</table>";
				
				if($total_used > 0)
				{
					echo $str;
				}
				/*echo "<br>";
				foreach($last_act as $key => $val)
				{
					echo "last activity - " . $key . " = " . count($val) . "<br>";
				}*/
			}
		}
		else if($tool=="resize_signatures")
		{
			$sig_filter = (isset($_GET['sig_filter']))?$_GET['sig_filter']:"";
			$start = (isset($_GET['start']))?$_GET['start']:"0";
			$span = 1;
			
			$rows = 0;
			$cols = 0;
			$maxcols = 6;
			
			$start_sig = (isset($_GET['start_sig']))?$_GET['start_sig']:"0";
			$sig_span = 100;
			$auto_continue_sigs = false;
			
			$current_load_level = get_local_load_level();
			
			echo "Resize signatures:<br><br>";
			echo "Current load level: " . $current_load_level . "<br><br>";
			
			if($current_load_level > 14)
			{
				echo "Load level to high to continue";
				
				$next_url = "/sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start&start_sig=$start_sig&sig_filter=$sig_filter";
				echo "<script language='javascript'>";
				echo "function continue_tool() { window.location = '$next_url'; } ";
				echo "setTimeout('continue_tool()',6000); ";
				echo "</script>";
				exit();
			}
			
			$min_ftime = time() - (60 * 60 * 24 * 90);
			echo "Min Datetime for Signatures: " . date("m/d/Y",$min_ftime) . "<br><br>";
			
			$sig_r_query = "";
			if($sig_filter!="")
			{
				$sig_r_query = "and `data_name`='[1]' ";
			}
			$r_query = mlavu_query("select `data_name` from poslavu_MAIN_db.restaurants where img_disk_space > 20 ".$sig_r_query."order by id asc limit $start,$span", $sig_filter);
			//$r_query = mlavu_query("select `data_name` from poslavu_MAIN_db.restaurants order by id asc limit $start,$span");
			while($r_read = mysqli_fetch_assoc($r_query))
			{
				$dataname = $r_read['data_name'];
				if(trim($dataname)!="")
				{
					echo $dataname . "<br>";
					$dataname_dir = "/mnt/poslavu-files/images/".$dataname."/signatures/";
					
					if(is_dir($dataname_dir))
					{
						$ftotal = 0;
						$ltotal = 0;
						
						$before_count = $start_sig;
						
						//echo "ssh poslavu@10.182.103.248 mkdir /home/poslavu/deleted_signatures/$dataname<br>";
						//echo exec("ssh poslavu@10.182.103.248 mkdir /home/poslavu/deleted_signatures/$dataname");
						
						echo "<table><tr>";
						
						$continue = true;
						$dp = opendir($dataname_dir);
						while($continue)
						{
							if($fname = readdir($dp))
							{
								$entire_fname = $dataname_dir . $fname;
								
								if(strpos($fname,".jpg"))
								{
									if($before_count > 0)
									{
										$before_count--;
									}
									else
									{
										//$dest_fname = "/home/poslavu/deleted_signatures/".$dataname."/$fname";
										
										//rename($entire_fname,$dest_fname);
										//echo $entire_fname . " - " . date("m/d/Y",$ftime) ."<br>";
										//exec("scp poslavu@10.182.103.248:$entire_fname $dest_fname");
										
										$img_size = getimagesize($entire_fname);
										if(is_array($img_size) && count($img_size) > 1)
										{
											$ftime = filemtime($entire_fname);
											if($ftime < $min_ftime)
											{
												$set_width = 128;
												$set_height = 24;
											}
											else
											{
												$set_width = 256;
												$set_height = 47;
											}
											
											if($img_size[0] > $set_width)
											{
												$ltotal++;
												$convert_cmd = "convert '$entire_fname' -thumbnail '".$set_width."x".$set_height.">' '$entire_fname'";
												//echo $convert_cmd . "<br>";
												//-------exec($convert_cmd);
												
												echo "<td>".$img_size[0]."x".$img_size[1]."<br><img src='/images/$dataname/signatures/$fname' /></td>";
												$cols++;
												if($cols==$maxcols) 
												{
													$cols = 0;
													echo "</tr><tr>";
												}
											}
										}
										
										$ftotal++;
									}
								}
							}
							else $continue = false;
							
							if($ftotal >= $sig_span) $continue = false;
						}
						
						echo "</tr></table>";
						
						if($ftotal > 0)
							$auto_continue_sigs = true;
						//if($ltotal > $ftotal / 10 || $ltotal > 0)
						//	$auto_continue_sigs = true;
						echo "Processed signatures " . ($start_sig) . " - " . ($start_sig + $ftotal) . " ($ltotal large)<br>";
						//echo "Signature count: started at $start_sig $ftotal records ($ltotal large)<br>";
					}
					else echo "No dir<br>";
				}
			}

			$start_next_sig = $start_sig + $sig_span;
			echo "<br><br><input type='button' value='Next Signatures >>' onclick='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start&start_sig=$start_next_sig&sig_filter=$sig_filter\"' />";
			
			$start_next = $start + $span;
			echo "<br><br><input type='button' value='Next >>' onclick='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start_next&sig_filter=$sig_filter\"' />";

			if($auto_continue_sigs)
			{
				$next_url = "index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start&start_sig=$start_next_sig&sig_filter=$sig_filter";
			}
			else if($ftotal < $sig_span - ($sig_span / 4))
			{
				$next_url = "index.php?mode=manage_customers&submode=general_tools&tool=resize_signatures&start=$start_next&sig_filter=$sig_filter";
			}
			else
			{
				$next_url = "";
			}
			
			if($next_url!="")
			{
				echo "<script language='javascript'>";
				echo "function continue_tool() { window.location = '$next_url'; } ";
				echo "setTimeout('continue_tool()',2000); ";
				echo "</script>";
			}
		}
		else if($tool=="unused_images")
		{
			if(isset($_GET['clear_img_disk_space']))
			{
				echo "zeroing out<br>";
				mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `img_disk_space`=''");
			}
			function get_dir_space($dir)
			{
				if(is_dir($dir))
				{
					$str = //------exec("du $dir");
					$str = explode($dir,$str);
					$kused = $str[0];
					$mused = $kused / 1000;
					return trim($mused);
				}
				else return 0;
			}
			
			$cond = "`img_disk_space`=''";
			
			$tdspace = 0;
			$r_query = mlavu_query("select `data_name` from poslavu_MAIN_db.restaurants where $cond limit 20");
			while($r_read = mysqli_fetch_assoc($r_query))
			{
				$dataname = $r_read['data_name'];
				if(trim($dataname)!="")
				{
					$dataname_dir = "/mnt/poslavu-files/images/".$dataname."/";
					$dataname_space = get_dir_space($dataname_dir);
					
					mlavu_query("update poslavu_MAIN_db.restaurants set `img_disk_space`='[1]' where `data_name`='[2]'",$dataname_space,$dataname);
					$tdspace += $dataname_space;
					echo $dataname . ": " . $dataname_space . "<br>";
				}
			}
			echo "Total Space: $tdspace<br>";
			
			echo "<br>";
			$count_query = mlavu_query("select count(*) as `count` from poslavu_MAIN_db.restaurants where $cond");
			if(mysqli_num_rows($count_query))
			{
				$count_read = mysqli_fetch_assoc($count_query);
				echo "<br>Left: " . $count_read['count'] . "<br>";
			}
			
			if(isset($_GET['step'])) $step = $_GET['step']; else $step = 1;
			$next_step = $step + 1;
			
			echo "<script language='javascript'>";
			echo "function continue_tool() { window.location = 'index.php?mode=manage_customers&submode=general_tools&tool=unused_images&step=$next_step'; } ";
			echo "setTimeout('continue_tool()',1000); ";
			echo "</script>";
			//echo "Unused images<br>";
		}
		else if($tool=="dropoffs")
		{
			$rslist = array();
			
			if(isset($_GET['start'])) $start_date = $_GET['start']; else $start_date = date("Y-m-") . "01";
			if(isset($_GET['end'])) $end_date = $_GET['end']; else $end_date = date("Y-m-") . "31";
			
			echo "<br><br>Date Range: <b>" . $start_date . " - " . $end_date . "</b><br><br>";
			
			$start_dt = "$start 00:00:00";
			$end_dt = "end 24:00:00";
			
			$disabled_count = 0;
			$paid_count = 0;
			$ps_query = mlavu_query("select distinct(`match_restaurantid`) from `poslavu_MAIN_db`.`payment_responses` where x_type='auth_capture' and x_amount > 500 and x_response_code=1 and datetime > '[1]' and datetime < '[2]' and `match_restaurantid`!=''",$start_dt,$end_dt);
			while($ps_read = mysqli_fetch_assoc($ps_query))
			{
				$rid = $ps_read['match_restaurantid'];
				$rslist[$rid] = "1";
			}
			$cr_query = mlavu_query("select distinct(`match_restaurantid`) from `poslavu_MAIN_db`.`payment_responses` where x_type='credit' and x_amount > 500 and x_response_code=1 and datetime > '[1]' and datetime < '[2]' and `match_restaurantid`!=''",$start_dt,$end_dt);
			while($cr_read = mysqli_fetch_assoc($cr_query))
			{
				$rid = $cr_read['match_restaurantid'];
				$rslist[$rid] = "0";
			}
			$lc_query = mlavu_query("select `restaurantid` from `poslavu_MAIN_db`.`payment_status` where `license_applied`!='' and `trial_start` > '[1]' and `trial_start` < '[2]'",$start_dt,$end_dt);
			while($lc_read = mysqli_fetch_assoc($lc_query))
			{
				$rid = $lc_read['restaurantid'];
				$rslist[$rid] = "1";
			}
			
			foreach($rslist as $rid => $rstatus)
			{
				if($rstatus=="1")
				{
					$paid_count++;
					$r_query = mlavu_query("select `disabled` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$rid);
					if(mysqli_num_rows($r_query))
					{
						$r_read = mysqli_fetch_assoc($r_query);
						
						if($r_read['disabled']=="1"||$r_read['disabled']=="2"||$r_read['disabled']=="3")
							$disabled_count++;
					}
				}
			}
			echo "Total Paid: " . mysqli_num_rows($ps_query) . "<br>";
			echo "Credited: " . mysqli_num_rows($cr_query) . "<br>";
			echo "Licensed: " . mysqli_num_rows($lc_query) . "<br><br>";
			echo "Paid: " . $paid_count . "<br>";
			echo "Disabled and paid: $disabled_count";
		}
		else if($tool=="packages")
		{
			if($cmd=="set_package")
			{
				require_once(dirname(__FILE__) . "/dnbilling.php");
				
				$r_dataname = (isset($_POST['r_dataname']))?$_POST['r_dataname']:"";
				$containerid = (isset($_POST['containerid']))?$_POST['containerid']:"";
				$r_level = (isset($_POST['r_level']))?$_POST['r_level']:"";
				$level_names = array("1"=>"Silver","2"=>"Gold","3"=>"Platinum","42"=>"Demo");
				$r_display_level = (isset($level_names[$r_level]))?$level_names[$r_level]:"Unknown";
				
				echo "[---START---]";
				if($r_dataname!="" && $r_level!="" && $r_display_level!="Unknown" && $r_display_level!="")
				{
					//--------------------------------------- Set Package -------------------------------------//
					$svars = array();
					$svars['package'] = $r_level;
					$svars['dataname'] = $r_dataname;
					$svars['status'] = "new";
					$svars['source'] = "superadmin adjustment";
					$svars['date'] = date("Y-m-d");
					$svars['min_due_date'] = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + 10,date("Y")));
					$svars['collection_mode'] = "on";

					/*echo "display=" . urlencode("dataname: $r_dataname\npackage: $r_level");
					echo "&containerid=" . urlencode($containerid);
					exit();*/

					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[dataname]'",$svars);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						$svars['restaurantid'] = $rest_read['id'];
					}
					else
					{
						echo "display=" . urlencode("Unable to find restaurant record") . "&containerid=" . urlencode($containerid);
						exit();
					}
					
					if($r_level==42) // demo account
					{
						mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `notes`=concat(`notes`,'---SET TO DEMO---'), `demo`='1' where `data_name`='[dataname]'",$svars);
						echo "display=" . urlencode("Set to Demo Status") . "&containerid=" . urlencode($containerid);
						exit();
					}
					if($r_display_level=="Demo" || $r_level=="42")
					{
						// safeguard
						exit();
					}

					$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[dataname]'",$svars);
					if(mysqli_num_rows($signup_query))
					{
						$signup_read = mysqli_fetch_assoc($signup_query);
						$svars['signupid'] = $signup_read['id'];
						$signup_success = mlavu_query("update `poslavu_MAIN_db`.`signups` set `package`='[package]' where `id`='[signupid]' limit 1",$svars);
					}
					else
					{
						$signup_success = mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`package`,`status`,`dataname`,`source`,`date`) values ('[package]','[status]','[dataname]','[source]','[date]')",$svars);
					}
					//--------------------------- Set Minimum Due Date in Payment Status ----------------------//
					$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[dataname]'",$svars);
					if(mysqli_num_rows($status_query))
					{
						$status_read = mysqli_fetch_assoc($status_query);
						$svars['statusid'] = $status_read['id'];
						$status_success = mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `min_due_date`='[min_due_date]',`collection_mode`='[collection_mode]' where `id`='[statusid]' limit 1",$svars);
					}
					else
					{
						$status_success = mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`,`min_due_date`,`collection_mode`) values ('[restaurantid]','[dataname]','[min_due_date]','[collection_mode]')",$svars);
					}
					//-----------------------------------------------------------------------------------------//
					
					if($signup_success && $status_success)
					{
						echo "display=" . urlencode("Set package for " . $r_dataname . " to " . $r_display_level);
						echo urlencode(" <a href='https://admin.poslavu.com/sa_cp/billing/index.php?dn=$r_dataname' target='_blank'>Billing</a>");
						run_billing_for_dataname($r_dataname);
					}
					else
						echo "display=" . urlencode("Error inserting or updating records");
					echo "&containerid=" . urlencode($containerid);
				}
				else
				{
					echo "display=" . urlencode("Unable to set package, error occurred") . "&containerid=" . urlencode($containerid);
				}
				echo "[---END---]";
				exit();
			}

			// for testing only
			//mlavu_query("delete from `poslavu_MAIN_db`.`payment_status` where `dataname`='17edison' limit 1");
			//mlavu_query("update `poslavu_MAIN_db`.`signups` set `package`='' where `dataname`='17edison' limit 1");
			//-------------------------------------------------------------------------------------------------------//

			$baseurl = "index.php?mode=manage_customers&submode=general_tools&tool=packages";
			$counts = array();
			$counts['all'] = array("total"=>0,"no_signup"=>0,"no_package"=>0,"trial"=>0,"expired_trial"=>0,"distro_licensed"=>0,"possible_demo"=>0);
			$counts['recent'] = $counts['all'];
			
			$needs_billing = array();
			
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where (`disabled`='' or `disabled`='0') and `notes` NOT LIKE '%AUTO_DELETED%' and `demo`!='1' and `dev`!='1' and `lavu_lite_server`='' order by (`data_name` LIKE '17edison') desc, id asc");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				$signup_exists = false;
				$payment_status_exists = false;
				$package = "";
				$license_type = "";
				$trial_start = "";
				$trial_end = "";
				
				$in_trial = false;
				$expired_trial = false;
				$billable = false;
				$possible_demo = false;
				
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$rest_read['data_name']);
				if(mysqli_num_rows($signup_query))
				{
					$signup_read = mysqli_fetch_assoc($signup_query);
					$package = $signup_read['package'];
					$signup_exists = true;
				}
				
				$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[1]'",$rest_read['data_name']);
				if(mysqli_num_rows($status_query))
				{
					$status_read = mysqli_fetch_assoc($status_query);
					$license_type = $status_read['license_type'];
					$trial_start = $status_read['trial_start'];
					$trial_end = $status_read['trial_end'];
					$payment_status_exists = true;
				}

				if($trial_start!="" && $trial_end!="" && $license_type=="")
				{
					if($trial_end < date("Y-m-d H:i:s")) $expired_trial = true;
					else $in_trial = true;
				}
				if(!$in_trial && !$expired_trial) $billable = true;
				if(strpos(strtolower($rest_read['data_name']),"demo")!==false) $possible_demo = true;
				
				$add_list = array();
				$add_list[] = "total";
				if($license_type!="") $add_list[] = 'distro_licensed';
				if($in_trial) $add_list[] = 'trial';
				if($expired_trial) $add_list[] = 'expired_trial';
				if($possible_demo) $add_list[] = 'possible_demo';
				if($billable) $add_list[] = 'billable';
				if(!$signup_exists && $billable) 
				{
					$add_list[] = 'no_signup';
					$needs_billing[] = array("rest_read"=>$rest_read,"signup_read"=>array(),"type"=>"no_signup");
				}
				else if($package=="" && $billable) 
				{
					$add_list[] = 'no_package';
					$needs_billing[] = array("rest_read"=>$rest_read,"signup_read"=>$signup_read,"type"=>"no_package");
				}
				
				for($n=0; $n<count($add_list); $n++)
				{
					$counts['all'][$add_list[$n]]++;
				}
				
				if($rest_read['last_activity'] > date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")-7,date("Y"))))
				{
					for($n=0; $n<count($add_list); $n++)
					{
						$counts['recent'][$add_list[$n]]++;
					}
				}
			}
			
			function show_counts($vars,$title)
			{
				echo "<br>";
				foreach($vars as $key => $val)
				{
					echo $title . " " . ucfirst(str_replace("_"," ",$key)) . ": " . $val . "<br>";
				}
			}
			
			echo "<script language='javascript' src='/sa_cp/tools/ajax_functions.js'></script>\n";
			echo "<script language='javascript'>\n";
			echo "  buttons_locked = false;\n";
			echo "	function set_package_callback(response) {\n";
			echo "		var vars = ajax_get_internal_vars(response);\n";
			echo "		var containerid = vars['containerid'];\n";
			echo "		var rsp = unescape(vars['display']).replace(/\+/g,' ');\n";
			echo "		document.getElementById(containerid).innerHTML = rsp;\n";
			echo "		buttons_locked = false;\n";
			echo "	}\n";
			echo "	function set_package(r_dataname, r_level, containerid) {\n";
			echo "		if(buttons_locked) { alert('Cannot assign yet, still waiting for response from previous request'); }\n";
			echo "		else {\n";
			echo "			buttons_locked = true;\n";
			echo "			document.getElementById(containerid).innerHTML = 'waiting for response......'; ";
			echo "			ajax_get_response(\"$baseurl&cmd=set_package\",\"r_dataname=\" + r_dataname  + \"&r_level=\" + r_level + \"&containerid=\" + containerid,\"set_package_callback\");\n";
			echo "		}\n";
			echo "	}\n";
			echo "</script>\n";
			
			echo "<table><tr><td valign='top'>";
			show_counts($counts['all'],"Total");
			echo "</td><td width='20'>&nbsp;</td><td valign='top'>";
			show_counts($counts['recent'],"Recent");
			echo "</td></tr></table>";
			
			$billing_count = 50;			
			$page_number = (isset($_GET['page']))?$_GET['page']:1;
			$page_count = ceil(count($needs_billing) / $billing_count);
			$billing_offset = ($page_number - 1) * $billing_count;
										
			echo "<br>";
			if($page_number > 1) echo "<a href='$baseurl&page=".($page_number - 1)."'><<</a> ";
			echo "Page $page_number / $page_count";
			if($page_number < $page_count) echo " <a href='$baseurl&page=".($page_number + 1)."'>>></a>";
			echo "<br>";
			echo "<table>";
			for($i=0; $i<$billing_count; $i++)
			{
				$offset = $i + $billing_offset;
				
				if(isset($needs_billing[$offset]))
				{
					$need_type = $needs_billing[$offset]['type'];
					$rest_read = $needs_billing[$offset]['rest_read'];
					if($rest_read['last_activity'] > date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")-7,date("Y"))))
					{
						$rdb = "poslavu_" . $rest_read['data_name'] . "_db";
						$last_order_query = mlavu_query("select * from `[1]`.`orders` where `closed` < '2040-01-01 00:00:00' order by `closed` desc limit 1",$rdb);
						if(mysqli_num_rows($last_order_query))
						{
							$last_order_read = mysqli_fetch_assoc($last_order_query);
							$last_order_closed = $last_order_read['closed'];
						}
						else $last_order_closed = "no orders found";
						
						$last_payment_date = "";
						$last_payment_amount = "";
						$last_payment_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_type`='auth_capture' and `x_response_code`='1'",$rest_read['id']);
						if(mysqli_num_rows($last_payment_query))
						{
							$last_payment_read = mysqli_fetch_assoc($last_payment_query);
							$last_payment_date = $last_payment_read['datetime'];
							$last_payment_amount = $last_payment_read['x_amount'];
						}
						
						if($last_order_closed > date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")-4,date("Y"))))
							$color = "#008800";
						else
							$color = "#000000";
						
						$payment_status = "";
						if($last_payment_date=="")
							$payment_status = "no payment found";
						else
							$payment_status = "last payment <b>$last_payment_amount</b> on <b>$last_payment_date</b>";
						echo "<tr><td>" . $need_type . " - <font style='color: $color'>" . $rest_read['company_name'] . " - " . $last_order_closed . "</font> - " . $payment_status;
						$r_dataname = $rest_read['data_name'];
						$containerid = "package_container_$r_dataname";
						echo "</td><td>";
						echo "<div id='$containerid'>";
						echo "<input type='button' style='font-size:10px' value='Silver' onclick='set_package(\"$r_dataname\",\"1\",\"$containerid\")' />";
						echo "<input type='button' style='font-size:10px' value='Gold' onclick='set_package(\"$r_dataname\",\"2\",\"$containerid\")' />";
						echo "<input type='button' style='font-size:10px' value='Platinum' onclick='set_package(\"$r_dataname\",\"3\",\"$containerid\")' />";

						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' style='font-size:10px' value='Demo Account' onclick='set_package(\"$r_dataname\",\"42\",\"$containerid\")' />";
						echo "</div>";
						echo "</td>";
						echo "<td>";
						
						echo "</td></tr>";
					}
				}
			}
			echo "</table>";
		}
		else if($tool=="state_locations" || $tool=="distinct_states" || $tool=="export_state_locations" || $tool=="state_by_transactions")
		{
			if(substr($tool,0,7)=="export_")
			{
				$tool = substr($tool,7);
				$export_mode = true;
			}
			else
			{
				$export_mode = false;
			}
			
			function csv_col($str)
			{
				$str = str_replace("\n","",$str);
				$str = str_replace("\"","",$str);
				if(strpos($str,",")!==false)
				{
					return "\"$str\"";
				}
				return $str;
			}
			
			function get_state_list()
			{
				return "AL:Alabama,
				AK:Alaska,
				AZ:Arizona,
				AR:Arkansas,
				CA:California,
				CO:Colorado, 
				CT:Connecticut, 
				DE:Delaware, 
				DC:District Of Columbia, 
				FL:Florida, 
				GA:Georgia, 
				HI:Hawaii, 
				ID:Idaho, 
				IL:Illinois, 
				IN:Indiana, 
				IA:Iowa, 
				KS:Kansas, 
				KY:Kentucky, 
				LA:Louisiana, 
				ME:Maine, 
				MD:Maryland, 
				MA:Massachusetts, 
				MI:Michigan, 
				MN:Minnesota, 
				MS:Mississippi, 
				MO:Missouri, 
				MT:Montana, 
				NE:Nebraska, 
				NV:Nevada, 
				NH:New Hampshire, 
				NJ:New Jersey, 
				NM:New Mexico, 
				NY:New York, 
				NC:North Carolina, 
				ND:North Dakota, 
				OH:Ohio, 
				OK:Oklahoma, 
				OR:Oregon, 
				PA:Pennsylvania, 
				RI:Rhode Island, 
				SC:South Carolina, 
				SD:South Dakota, 
				TN:Tennessee, 
				TX:Texas, 
				UT:Utah, 
				VT:Vermont, 
				VA:Virginia, 
				WA:Washington, 
				WV:West Virginia, 
				WI:Wisconsin, 
				WY:Wyoming";
			}
			
			$state_list = array();
			$state_list_assoc = array();
			$state_list_str = get_state_list();
			$state_list_parts = explode(",",$state_list_str);
			for($n=0; $n<count($state_list_parts); $n++)
			{
				$state_parts = explode(":",$state_list_parts[$n]);
				if(count($state_parts) > 1)
				{
					$state_abbrev = trim($state_parts[0]);
					$state_name = trim($state_parts[1]);
					
					//echo $state_abbrev . " - " . $state_name . "<br>";
					$state_list[] = array($state_abbrev,$state_name);
					$state_list_assoc[strtolower($state_abbrev)] = $state_abbrev;
					$state_list_assoc[strtolower($state_name)] = $state_abbrev;
				}
			}
			
			if(isset($_GET['month']))
			{
				$use_month = $_GET['month'];
			}
			else $use_month = "0";
			
			$month_arr = array("","January","February","March","April","May","June","July","August","September","October","November","December","2011","2012","2013","2014","2015");
			echo "<center>";				
			echo "<br><br>";
			
			$year = 2015;
			if(isset($_GET['setyear']))
			{
				$_SESSION['stateyear'] = $_GET['setyear'];
			}
			if(isset($_SESSION['stateyear'])) $year = $_SESSION['stateyear'];
			
			$special_mode = "";
			if(isset($_GET['specialmode']))
			{
				//$_SESSION['specialmode'] = $_GET['specialmode'];
				$special_mode = $_GET['specialmode'];
			}
			//if(isset($_SESSION['specialmode'])) $special_mode = $_SESSION['specialmode'];
			
			echo "Primary Year: $year&nbsp;&nbsp;&nbsp;&nbsp;";
			echo "<select onchange='window.location = \"?mode=manage_customers&submode=general_tools&tool=$tool";
			if(isset($_GET['state'])) echo "&state=" . $_GET['state'];
			echo "&setyear=\" + this.value'><option value=''>---Change Year---</option><option value='2015'>2015</option><option value='2014'>2014</option><option value='2013'>2013</option><option value='2012'>2012</option><option value='2011'>2011</option><option value='2010'>2010</option></select>";
			echo "<br><br>";
			
			echo "<select onchange='window.location = \"?mode=manage_customers&submode=general_tools&tool=$tool";
			if(isset($_GET['state'])) echo "&state=" . $_GET['state'];
			echo "&month=\" + this.value'>";
			for($m=0; $m<count($month_arr); $m++)
			{
				echo "<option value='$m'";
				if($m==$use_month) echo " selected";
				echo ">" . $month_arr[$m] . "</option>";
			}
			echo "</select>";
			if($tool!="state_by_transactions") echo "<br><br>";
			 
			if($use_month > 12 && is_numeric($month_arr[$use_month]))
			{
				$year = $month_arr[$use_month];
				if($year > 2000 && $year < 3000)
				{
					$filter_number = "";
					$filter_name = "(Year of $year)";
					$min_datetime = $year . "-01-01 00:00:00";
					$max_datetime = $year . "-12-31 24:00:00";
				}
				else
				{
					exit();
				}
			}
			else if($use_month!="0" && $use_month!="")
			{
				$filter_number = $use_month;
				$filter_name = "(".$month_arr[$filter_number * 1].")";//"(March)";
				if($filter_number < 10) $filter_number = "0" . ($filter_number * 1);
				$min_datetime = $year . "-".$filter_number."-01 00:00:00";
				$max_datetime = $year . "-".$filter_number."-31 24:00:00";		
			}
			else
			{
				exit();
			}
			
			if($tool=="state_by_transactions")
			{
				ini_set("memory_limit","1200M");
				
				// Used for testing
				$exclude_datanames = array("lavu_inc","17edison","demo_lavu_ls","test_pizza_sho","praefkeyconcep","test");
				
				$select_condition1 = "";
				$select_condition2 = "";
				for($ex=0; $ex<count($exclude_datanames); $ex++)
				{
					$ex_dataname = $exclude_datanames[$ex];
					if($select_condition1 != "") $select_condition1 .= " and ";
					if($select_condition2 != "") $select_condition2 .= " and ";
					$select_condition1 .= " `data_name`!='".$conn->escapeString($ex_dataname)."'";
					$select_condition2 .= " `dataname`!='".$conn->escapeString($ex_dataname)."'";
				}
				
				$rest_list = array();
				$rest_query = mlavu_query("select `data_name`,`company_name`,`chain_id`,`distro_code`,`id` from `poslavu_MAIN_db`.`restaurants`");
				while($rest_read = mysqli_fetch_assoc($rest_query))
				{
					$rest_list[$rest_read['id']] = $rest_read;
				}
				
				$loc_list = array();
				$loc_query = mlavu_query("select `restaurantid`,`city`,`state`,`country` from `poslavu_MAIN_db`.`restaurant_locations`");
				while($loc_read = mysqli_fetch_assoc($loc_query))
				{
					$loc_list[$loc_read['restaurantid']] = $loc_read;
				}
				
				$signup_list = array();
				$signup_query = mlavu_query("select `package`,`dataname`,`city`,`state`,`country`,`ipaddress` from `poslavu_MAIN_db`.`signups`");
				while($signup_read = mysqli_fetch_assoc($signup_query))
				{
					$signup_list[$signup_read['dataname']] = $signup_read;
				}
				
				$us_total = 0;
				$us_count = 0;
				$non_us_total = 0;
				$non_us_count = 0;
				$n = 0;
				
				$special_list = array();
				$state_list = array();
				$state_info = array();
				
				function is_not_valid_state_input($str)
				{
					if($str=="" || $str=="-" || $str=="--" || $str=="." || is_numeric(str_replace(" ","",str_replace("-","",$str))) || $str=="!") return true;
					else return false;
				}
				
				$all_states_total = 0;
				$not_included_list = array();		
				$non_included_total = 0;
				$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1' and `datetime`>='[2]' and `datetime`<='[3]'",$rest_id,$min_datetime,$max_datetime);
				while($pay_read = mysqli_fetch_assoc($pay_query))
				{
					$n++;
					$loc_total = 0;
					$trans_count = 0;
					$rf_count = 0;
					$state = trim(strtolower($pay_read['x_state']));
					$otype = $pay_read['match_restaurantid'];
					if(isset($pay_read['x_country']))
					{
						$set_country = trim(strtolower($pay_read['x_country']));
					}
					else
					{
						$set_country = "";
					}
					
					$odistro = "";
					if(substr($otype,0,3)=="DS_") { $odistro = trim(strtolower(substr($otype,3))); }
					$rest_dataname = "";
					if(isset($rest_list[$otype])) { $rest_dataname = trim($rest_list[$otype]['data_name']); }
					
					$should_include = true;
					for($ex=0; $ex<count($exclude_datanames); $ex++)
					{
						$ex_dataname = $exclude_datanames[$ex];
						if(trim(strtolower($ex_dataname))==trim(strtolower($rest_dataname)))
						{
							$should_include = false;
						}
					}
					if($odistro=="martin")
					{
						$should_include = false;
					}
					
					if(isset($rest_dataname) && $rest_dataname!="")
					{
						$read_signup_state = trim(strtolower($signup_list[$rest_dataname]['state']));
						if(substr(strtolower($read_signup_state),0,2)=="o:")
						{
							$state = substr($read_signup_state,2);
						}
					}
					
					if(is_not_valid_state_input($state))
					{
						if(isset($loc_list[$otype]))
						{
							$state = trim(strtolower($loc_list[$otype]['state']));
							$set_country = trim(strtolower($loc_list[$otype]['country']));
							if(is_not_valid_state_input($state))
							{
								$state = trim(strtolower($loc_list[$otype]['country']));
							}
						}
						if(is_not_valid_state_input($state) && isset($rest_list[$otype]))
						{
							$rest_dataname = trim($rest_list[$otype]['data_name']);
							if($rest_dataname!="")
							{
								$state = trim(strtolower($signup_list[$rest_dataname]['state']));
								$set_country = trim(strtolower($signup_list[$rest_dataname]['country']));
								if(is_not_valid_state_input($state))
								{
									$state = trim(strtolower($signup_list[$rest_dataname]['country']));
								}
								if(is_not_valid_state_input($state))
								{
									$pay_read['x_city'] = $pay_read['x_city'] . " " . $rest_dataname;
									//foreach($signup_list[$rest_dataname] as $skey => $sval) echo $skey . " = " . $sval . "<br>";
									$signupip = $signup_list[$rest_dataname]['ipaddress'];
									if(strlen($signupip) > 6 && is_numeric(str_replace(".","",$signupip)))
									{
										$pay_read['x_city'] = $pay_read['x_city'] . " <a href='http://whatismyipaddress.com/ip/$signupip' target='_blank'>($signupip)</a>";
									}
									//$state = "no state in signup: $rest_dataname";
								}
							}
							//else $state = "no dataname: $otype";
						}
						if(is_not_valid_state_input($state))
						{
							if($odistro=="bank") $state = "thailand";
							else if($odistro=="vikingpos") $state = "norway";
							else if($odistro=="tmax") $state = "panama";
							else if($odistro=="newage") $state = "barbados";
							else if($odistro=="coccinelle") $state = "denmark";
							else if($odistro=="softwareexpress") $state = "mexico";
							else if($odistro=="kenji") $state = "hong kong";
							else if($odistro=="c4b") $state = "united kingdom";
							else if($odistro=="centregrafic") $state = "spain";
							else if($odistro=="redamber") $state = "united kingdom";
							else if($odistro=="dressed") $state = "hong kong";
							//$pay_read['x_city'] .= " " . $otype;
						}

						if(is_not_valid_state_input($state))
						{


						}
						else if(isset($rest_dataname) && $rest_dataname!="")
						{
							if(isset($state_list_assoc[strtolower($state)])) 
							{
								$set_country = "USA";
								$set_state = $state;
							}
							else
							{
								$set_country = $state;
								$set_state = "";
							}
							$set_rest_dataname = "mem_" . $rest_dataname;

							$find_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_location_actual` where `data_name`='[1]'",$set_rest_dataname);
							if(mysqli_num_rows($find_query))
							{
								mlavu_query("update `poslavu_MAIN_db`.`customer_location_actual` set `administrative_area_level_1`='[1]', `country`='[2]' where `data_name`='[3]'",$set_state,$set_country,$set_rest_dataname);
							}
							else
							{
								mlavu_query("insert into `poslavu_MAIN_db`.`customer_location_actual` (`administrative_area_level_1`, `country`, `data_name`) values ('[1]','[2]','[3]')",$set_state,$set_country,$set_rest_dataname);
							}
						}

					}
					
					if($pay_read['x_response_code']!="1")
					{
						$pamount_add = 0;
					}
					else if($pay_read['x_type']=="credit")
					{
						$pamount_add = $pay_read['x_amount'] * -1;
					}
					else
					{
						$pamount_add = $pay_read['x_amount'] * 1;
					}
					
					if($should_include)
					{
						if($otype=="") $otype = "blank";
						if($pay_read['x_type']=="credit")
						{
							$loc_total -= ($pay_read['x_amount'] * 1);
							$rf_count++;
						}
						else
						{
							$loc_total += ($pay_read['x_amount'] * 1);
							$trans_count++;
						}
						
						if(isset($state_list_assoc[trim(strtolower($state))]))
						{
							$state = $state_list_assoc[trim(strtolower($state))];
							$us_total += ($loc_total * 1);
							$us_count += 1;
							//echo $n . ": <font style='color:#008800'>" . $state_read['city'] . " " . $state_read['state'] . "</font><br>";
							$set_country = "USA";
						}
						else
						{
							$non_us_total += ($loc_total * 1);
							$non_us_count += 1;
							//echo $n . ": " . $state_read['city'] . " " . $state_read['state'] . "<br>";
						}
						if($set_country=="") $set_coutry = $state;
						
						if($special_mode=="data")
						{
							$product_tier = "";
							$company_name = "";
							$chain_id = "";
							$rest_id = "";
							$invoice_distro = $odistro;
							if($rest_dataname!="")
							{
								$signup_package = $signup_list[$rest_dataname]['package'];
								if(is_numeric($signup_package))
								{
									$product_mod = $signup_package % 10;
									if($product_mod==1) $product_tier = "silver";
									else if($product_mod==2) $product_tier = "gold";
									else if($product_mod==3) $product_tier = "pro";
									else if($product_mod==8) $product_tier = "lavu88";
									else $product_tier = "#" . $signup_package;
								}
								else
								{
									$prodcut_tier = $signup_package;
								}
								$company_name = $rest_list[$otype]['company_name'];
								$chain_id = $rest_list[$otype]['chain_id'];
								$rest_id = $rest_list[$otype]['id'];
								if($invoice_distro=="")
								{
									$invoice_distro = $rest_list[$otype]['distro_code'];
								}
							}
							if($product_tier=="")
							{
								$product_tier_info = get_license_type_by_payment_amount($pay_read['x_amount'] * 1);
								$product_tier = $product_tier_info['level'];
							}
							
							$paid_yes_no = "yes";
							if($pay_read['batch_status']=="N" && $pay_read['date']>="2014-02-01" && $pay_read['date']<"2014-08-15")
							{
								$paid_yes_no = "no";
							}
														
							$special_entry = array();
							$special_entry['customer_name'] = $company_name;
							$special_entry['chain_id'] = $chain_id;
							$special_entry['customer_id'] = $rest_id;
							$special_entry['direct_or_reseller'] = ($invoice_distro=="")?"direct":"reseller";
							$special_entry['reseller_name'] = $invoice_distro;
							$special_entry['invoice_id'] = $pay_read['invoices_applied_to'];
							$special_entry['invoice_date'] = $pay_read['date'];
							$special_entry['subscription_date'] = $pay_read['date'];
							$special_entry['country'] = $set_country;
							$special_entry['state'] = $state;
							$special_entry['product_tier'] = $product_tier;
							$special_entry['rev_type'] = $pay_read['payment_type'];
							$special_entry['memo'] = $pay_read['batch_action'];
							$special_entry['paid_yes_no'] = $paid_yes_no;
							$special_entry['amount'] = $pamount_add;
							
							$special_list[] = $special_entry;
						}
						
						if(!isset($state_info[$state])) 
						{
							$state_list[] = $state;
							$state_info[$state] = array("count"=>0,"rcount"=>0,"amount"=>0,"transactions"=>array());
						}
						$state_info[$state]['count']+=$trans_count;
						$state_info[$state]['rcount']+=$rf_count;
						$state_info[$state]['amount']+=$loc_total;
						$state_info[$state]['transactions'][] = $pay_read;
						
						$all_states_total += $pamount_add;
					}
					else
					{
						$not_included_list[] = $pay_read;
						$non_included_total += $pamount_add;
						
						$paid_yes_no = "yes";
						if($pay_read['batch_status']=="N" && $pay_read['date']>="2014-02-01" && $pay_read['date']<"2014-08-15")
						{
							$paid_yes_no = "no";
						}
						$special_entry = array();
						$special_entry['customer_name'] = "not connected";
						$special_entry['chain_id'] = "";
						$special_entry['customer_id'] = "";
						$special_entry['direct_or_reseller'] = "";
						$special_entry['reseller_name'] = "";
						$special_entry['invoice_id'] = $pay_read['invoices_applied_to'];
						$special_entry['invoice_date'] = $pay_read['date'];
						$special_entry['subscription_date'] = $pay_read['date'];
						$special_entry['country'] = $pay_read['x_country'];
						$special_entry['state'] = $pay_read['x_state'];
						$special_entry['product_tier'] = $product_tier;
						$special_entry['rev_type'] = $pay_read['payment_type'];
						$special_entry['memo'] = $pay_read['batch_action'];
						$special_entry['paid_yes_no'] = $paid_yes_no;
						$special_entry['amount'] = $pamount_add;
						
						$special_list[] = $special_entry;
					}					
					
				}
				echo "<br><br>States Total: " . number_format($all_states_total,2) . "<br>";
				echo "Not Included Total: " . number_format($non_included_total,2) . "<br>";
				echo "All Total: " . number_format($all_states_total * 1 + $non_included_total * 1,2) . "<br>";
				echo "<br><br>";
				
				function add_subtotal_for_list($list)
				{
					if(count($list) > 0)
					{
						$total = 0;
						for($i=0; $i<count($list); $i++)
						{
							if($list[$i]['x_type']=="credit")
								$total += $list[$i]['x_amount'] * -1;
							else
								$total += $list[$i]['x_amount'] * 1;
						}
						$list[] = array("x_first_name"=>"Subtotal","x_last_name"=>"","x_description"=>"","x_amount"=>$total,"sort_notes"=>"");
					}
					return $list;
				}
				
				sort($state_list);
				$state_selected = (isset($_GET['state']))?$_GET['state']:"";
				$view_txn_mode = (isset($_GET['view_txn_mode']))?$_GET['view_txn_mode']:"";
				echo "<select onchange='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=state_by_transactions&month=$use_month&view_txn_mode=$view_txn_mode&state=\" + this.value'>";
				echo "<option value=''>Choose State</option>";
				for($n=1; $n<=2; $n++)
				{
					if($n==1) echo "<option value='ALLUSA'".($state_selected=="ALLUSA"?" selected":"").">-----------------( USA )---------------</option>";
					else if($n==2) echo "<option value='ALLINTL'".($state_selected=="ALLINTL"?" selected":"").">-----------------( INTL )---------------</option>";
					foreach($state_list as $state_key => $state_value)
					{
						if(($n==1 && isset($state_list_assoc[strtolower($state_value)]))||($n==2 && !isset($state_list_assoc[strtolower($state_value)])))
						{
							echo "<option value=\"".str_replace("\"","&quot;",$state_value)."\"";
							if($state_selected==$state_value && $state_selected!="") echo " selected";
							echo ">";
							echo $state_value;
							echo " (" . count($state_info[$state_value]['transactions']) . ") ";
							echo "</option>";
						}
					}
				}
				echo "<option value='ALL'".($state_selected=="ALL"?" selected":"").">----------( View ALL )-----------</option>";
				echo "</select>";
				
				//echo "<select onchange='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=state_by_transactions&month=$use_month&state=$state_selected&view_txn_mode=\" + this.value'>";
				//echo "<option value='list'".($view_txn_mode=="list"?" selected":"").">Transaction List</option>";
				//echo "<option value='invoices'".($view_txn_mode=="invoices"?" selected":"").">View Invoices</option>";
				//echo "</select>";
				
				if($state_selected=="")
				{
					exit();
				}
				
				if($special_mode=="data")
				{
					$datastr = "";
					//echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px black'>";
					for($i=0; $i<count($special_list); $i++)
					{
						$entry_line = $special_list[$i];
						if($i==0)
						{
							//echo "<tr>";
							$linestr = "";
							foreach($entry_line as $key => $val)
							{
								if($linestr!="") $linestr .= ",";
								$linestr .= "\"".str_replace("\"","",ucwords(str_replace("_"," ",$key)))."\"";
								//echo "<td align='center' bgcolor='#dddddd'>".ucwords(str_replace("_"," ",$key))."</td>";
							}
							$datastr .= $linestr . "\n";
							//echo "</tr>";
						}
						//echo "<tr>";
						$linestr = "";
						foreach($entry_line as $key => $val)
						{
							if($linestr!="") $linestr .= ",";
							$linestr .= "\"".str_replace("\"","",$val)."\"";
							//echo "<td align='center' style='border-right:solid 1px #777777'>$val</td>";
						}
						$datastr .= $linestr .= "\n";
						//echo "</tr>";
					}
					//echo "</table>";
					
					echo "<textarea rows='40' cols='120'>";
					echo $datastr;
					echo "</textarea>";
					exit();
				}
				
				$all_invoice_list = array();
				echo "<script type='text/javascript' src='resources/ajaxlib.js'></script>";
				echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #cccccc'>";
				foreach($state_list as $state_key => $state_value)
				{
					//if(1)//strlen($state_value)==2 && strtoupper($state_value)==$state_value && !is_numeric($state_value))
					if($state_selected=="ALL" || strtolower($state_selected)==strtolower($state_value) || ($state_selected=="ALLUSA" && isset($state_list_assoc[strtolower($state_value)])) || ($state_selected=="ALLINTL" && !isset($state_list_assoc[strtolower($state_value)])))
					{
						echo "<tr bgcolor='#ddddee'>";
						echo "<td align='right' style='border-top:solid 1px black'>State: <b>$state_value</b></td>";
						echo "<td style='border-top:solid 1px black'>&nbsp;</td>";
						echo "<td style='border-top:solid 1px black'>&nbsp;</td>";
						echo "<td style='border-top:solid 1px black'>&nbsp;</td>";//$state_info[$state_value]['count'] . "</td>"; 
						//echo "<td>$" . number_format($state_info[$state_value]['amount'],2) . "</td>";
						echo "<td style='border-top:solid 1px black'>&nbsp;</td>";
						echo "<td style='border-top:solid 1px black'>&nbsp;</td>";
						echo "<td style='border-top:solid 1px black'>&nbsp;</td>"; // added
						echo "</tr>";
						
						$directlist = array();
						$distrolist = array();
						$otherlist = array();
						$translist = $state_info[$state_value]['transactions'];
						for($i=0; $i<count($translist); $i++)
						{
							$trans = $translist[$i];
							if(substr($trans['match_restaurantid'],0,3)=="DS_")
							{
								$trans['sort_notes'] = "Distro: " . substr($trans['match_restaurantid'],3);
								$distrolist[] = $trans;
							}
							else if(is_numeric($trans['match_restaurantid']))
							{
								$trans['sort_notes'] = "Direct";
								$directlist[] = $trans;
							}
							else
							{
								$trans['sort_notes'] = "Other";
								$otherlist[] = $trans;
							}
						}
												
						$directlist = add_subtotal_for_list($directlist);
						$distrolist = add_subtotal_for_list($distrolist);
						$otherlist = add_subtotal_for_list($otherlist);
						$sorted_translist = array_merge($directlist,$distrolist,$otherlist);
						
						for($i=0; $i<count($sorted_translist); $i++)
						{
							$trans = $sorted_translist[$i];
							$matchid = $trans['match_restaurantid'];
							$match_dataname = "";
							$match_disabled = "";
							
							if($trans['x_first_name']=="Subtotal") $bgcolor = "#eeeeee"; else $bgcolor = "#ffffff";
							if($trans['x_type']=="credit") $show_amount = "-$" . number_format($trans['x_amount'],2);
							else $show_amount = "$" . number_format($trans['x_amount'],2);
							
							echo "<tr bgcolor='$bgcolor'>";
							echo "<td align='right'>";
							if($matchid!="")
							{
								if(isset($rest_list[$matchid]))
								{
									$rest_info = $rest_list[$matchid];
									$match_dataname = $rest_info['data_name'];
									$match_disabled = $rest_info['disabled'];
									echo $rest_info['company_name'];
								}
								else
								{
									if(substr($matchid,0,3)!="DS_")
										echo "Id: " . $matchid;
								}
							}
							else
							{
								echo $trans['x_first_name'] . " " . $trans['x_last_name'];
							}
							echo "</td>";
							
							$date_parts = explode("-",$trans['date']);
							if(count($date_parts) > 2)
							{
								$date_year = $date_parts[0];
								$date_month = $date_parts[1];
								$date_day = $date_parts[2];
								
								$show_date = $date_month . "/" . $date_day . "/" . $date_year;
							}
							else $show_date = $trans['date'];
							//echo "<td>" . $trans['x_description'] . "</td>";
							echo "<td>" . $trans['x_city'] . "</td>";
							echo "<td>" . $show_date . "</td>";
							echo "<td>" . $show_amount . "</td>";
							echo "<td>" . $trans['sort_notes'] . "</td>";
							
							$iapplied = array();
							$invoices_applied_to = explode("|",trim($trans['invoices_applied_to']));
							for($n=0; $n<count($invoices_applied_to); $n++)
							{
								$iapplied_parts = explode(" to ",$invoices_applied_to[$n]);
								if(count($iapplied_parts) > 1)
								{
									$iapplied_amount = trim($iapplied_parts[0]);
									$iapplied_invoiceid = trim($iapplied_parts[1]);
									
									if(is_numeric($iapplied_amount) && is_numeric($iapplied_invoiceid))
									{
										$iapplied[] = array("amount"=>$iapplied_amount,"invoiceid"=>$iapplied_invoiceid);
									}
								}
							}
							
							echo "<td>";
							if($match_dataname!="")
							{
								$invoice_found = false;
								for($n=0; $n<count($iapplied); $n++)
								{
									if($invoice_found) echo "<br>";
									echo "<a target='_blank' href='https://admin.poslavu.com/sa_cp/billing/invoicepdf.php?dataname=$match_dataname&invoiceid=".$iapplied[$n]['invoiceid']."'>Invoice #".$iapplied[$n]['invoiceid']."</a>";
									$invoice_found = true;
									$all_invoice_list[] = $iapplied[$n]['invoiceid'];
								}
								if(!$invoice_found)
								{
									if($match_disabled!="" && $match_disabled!="0")
									{
										echo "disabled($match_disabled) ";
									}
									//echo "responseid: " . $trans['id'];
									echo "<a target='_blank' href='https://admin.poslavu.com/sa_cp/billing/invoicepdf.php?dataname=$match_dataname&invoiceid=P".$trans['id']."|'>Invoice #P".$trans['id']."</a>";
									echo "<br><a href='https://admin.poslavu.com/sa_cp/billing/index.php?dn=$match_dataname' target='_blank' style='color:#880000'>---View Billing Page---</a>";
									$all_invoice_list[] = "P" . $trans['id'];
								}
							}
							echo "</td>";
							
							echo "<td>";
							if($trans['x_first_name']!="Subtotal" && $trans['x_first_name']!="Total" && ((is_not_valid_state_input($state_value) && is_numeric($matchid)) || !isset($state_list_assoc[strtolower($state_value)])))
							{
								echo "<div id='state_container_$matchid'><input type='button' value='Set State' onclick='set_state(\"$matchid\",\"allow_input\")' /></div>";
							}
							else
							{
								echo "&nbsp;";
							}
							echo "</td>";
							echo "</tr>";
						}
						
						echo "<tr bgcolor='#dddddd'>";
						echo "<td align='right'>$state_value Total</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>$" . number_format($state_info[$state_value]['amount'],2) . "</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>"; // Added
						echo "</tr>";
						//echo "<tr><td>&nbsp;</td></tr>";
					}
				}
				echo "</table>";
				
				if(isset($all_invoice_list))
				{
					function output_partial_invoice_link($invoice_part_size,$all_invoices_part,$start_invoice)
					{
						if(count($all_invoices_part) > 0)
						{
							$all_invoice_link_template = "<a target='_blank' href='https://admin.poslavu.com/sa_cp/billing/invoicepdf.php?dataname=view_all_invoices&invoiceid=[invoice_ids]'>[title]</a>";
							$part_invoices_link = $all_invoice_link_template;
							$part_invoices_link = str_replace("[invoice_ids]",implode("|",$all_invoices_part),$part_invoices_link);
							$part_invoices_link = str_replace("[title]","Invoices " . $start_invoice . " - " . ($start_invoice + count($all_invoices_part) - 1),$part_invoices_link);
							return $part_invoices_link;
						}
						else return "";
					}
					
					if(count($all_invoice_list) > 0)
					{
						echo "<br><br>";
						echo "<table cellpadding=12 bgcolor='#eeeeee' width='360' style='border:solid 1px #777777'>";
						echo "<tr><td align='center' bgcolor='#777777' style='color:#ffffff; font-weight:bold'>Multi-Invoices</td></tr>";
						echo "<tr><td align='center'>";
						echo "<table>";
						$invoice_part_size = 20;
						$start_invoice = 1;
						$all_invoices_part = array();
						for($n=0; $n<count($all_invoice_list); $n++)
						{
							$all_invoices_part[] = $all_invoice_list[$n];
							if(count($all_invoices_part) >= $invoice_part_size)
							{
								echo "<tr><td>" . output_partial_invoice_link($invoice_part_size,$all_invoices_part,$start_invoice) . "</td></tr>";
								$start_invoice += $invoice_part_size;
								$all_invoices_part = array();
							}
						}
						echo "<tr><td>" . output_partial_invoice_link($invoice_part_size,$all_invoices_part,$start_invoice) . "</td></tr>";
						echo "</table>";
						echo "</td></tr>";
						echo "</table>";
						echo "<br><br>";
						echo "<input type='button' value='Data' onclick='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=state_by_transactions&month=$use_month&view_txn_mode=$view_txn_mode&state=$state_selected&specialmode=data\"' />";
						echo "<br><br>";
					}
				}
				echo "</center>";
				
				echo "<script type='text/javascript'>";
				echo "	function set_state(setid,setcmd) { ";
				echo "		state_container = document.getElementById('state_container_' + setid); ";
				echo "		if(setcmd=='allow_input') { ";
				echo "			state_container.innerHTML = '<input type=\"text\" id=\"state_input_' + setid + '\" /><input type=\"button\" value=\"update\" onclick=\"set_state(\'' + setid + '\',\'update\')\" />'; ";
				echo "		} ";
				echo "		else if(setcmd=='update') { ";
				echo "			var setval = document.getElementById('state_input_' + setid).value; ";
				echo "			make_ajax_call('/sa_cp/tools/general_tools.php','ajcode=$ajcode&restid=' + setid + '&setstate=' + escape(setval),'state_updated'); ";
				//echo "			alert('update ' + setid + ' to ' + setval); ";
				echo "		} ";
				echo "	} ";
				echo "	function state_updated(rsp) { ";
				echo "		var success = rsp['success']; ";
				echo "		var message = rsp['message']; ";
				echo "		var setstate = rsp['setstate']; ";
				echo "		var restid = rsp['restid']; ";
				echo "		if(message=='Success') { ";
				echo "			document.getElementById('state_container_' + restid).innerHTML = '<font color=\"#008800\">' + setstate + '</font>'; ";
				echo " 		} ";
				echo "		else { ";
				echo "			alert('Failed to Update: ' + rsp['message']); ";
				echo "		} ";
				echo "	} ";
				echo "</script>";
				exit();
			}
			else if($tool=="distinct_states")
			{				
				$us_total = 0;
				$us_count = 0;
				$non_us_total = 0;
				$non_us_count = 0;
				
				$connects_found = array();
				
				echo $min_datetime . " - " . $max_datetime . "<br>";
				$n = 0;
				$state_query = mlavu_query("select `restaurant_locations`.`restaurantid`, `restaurant_locations`.`state` as `state`,`restaurant_locations`.`city` as `city` from `poslavu_MAIN_db`.`restaurant_locations` left join `poslavu_MAIN_db`.`restaurants` on `restaurant_locations`.`restaurantid`=`restaurants`.`id` where `restaurants`.`demo`!='1' and `restaurants`.`dev`!='1' and `restaurant_locations`.`title` NOT LIKE '%demo%' and `restaurant_locations`.`title` NOT LIKE '%test%' and `restaurant_locations`.`title` NOT LIKE '%default%' and `restaurant_locations`.`title` NOT LIKE '%lavu%' and `restaurant_locations`.`title` NOT LIKE '%sqa%' order by `restaurants`.`id` desc, `restaurant_locations`.`id` desc limit 40000");
				while($state_read = mysqli_fetch_assoc($state_query))
				{
					$rest_id = $state_read['restaurantid'];
					$connects_found[$rest_id] = 1;
					
					$loc_total = 0;
					$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1' and `datetime`>='[2]' and `datetime`<='[3]'",$rest_id,$min_datetime,$max_datetime);
					while($pay_read = mysqli_fetch_assoc($pay_query))
					{
						if($pay_read['x_type']=="credit")
						{
							$loc_total -= ($pay_read['x_amount'] * 1);
						}
						else
						{
							$loc_total += ($pay_read['x_amount'] * 1);
						}
					}
					
					if($loc_total != 0)
					{
						$n++;
						if(isset($state_list_assoc[trim(strtolower($state_read['state']))]))
						{
							$us_total += ($loc_total * 1);
							$us_count += 1;
							echo $n . ": <font style='color:#008800'>" . $state_read['city'] . " " . $state_read['state'] . "</font><br>";
						}
						else
						{
							$non_us_total += ($loc_total * 1);
							$non_us_count += 1;
							echo $n . ": " . $state_read['city'] . " " . $state_read['state'] . "<br>";
						}
					}
				}
				
				$loc_total = 0;
				$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid` LIKE 'DS%' and (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1' and `datetime`>='[2]' and `datetime`<='[3]'",$rest_id,$min_datetime,$max_datetime);
				while($pay_read = mysqli_fetch_assoc($pay_query))
				{
					$connects_found[$pay_read['match_restaurantid']] = 1;
					if($pay_read['x_type']=="credit")
					{
						$loc_total -= ($pay_read['x_amount'] * 1);
					}
					else
					{
						$loc_total += ($pay_read['x_amount'] * 1);
					}
				}
				$distro_total = $loc_total;

				$other_types = array();
				$loc_total = 0;
				$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1' and `datetime`>='[2]' and `datetime`<='[3]'",$rest_id,$min_datetime,$max_datetime);
				while($pay_read = mysqli_fetch_assoc($pay_query))
				{
					$otype = $pay_read['match_restaurantid'];
					if($otype=="") $otype = "blank";
					if(!isset($connects_found[$otype]))
					{
						if(!isset($other_types[$otype])) $other_types[$otype] = 1;
						else $other_types[$otype]++;
						//$connects_found[$pay_read['match_restaurantid']] = 1;
						if($pay_read['x_type']=="credit")
						{
							$loc_total -= ($pay_read['x_amount'] * 1);
						}
						else
						{
							$loc_total += ($pay_read['x_amount'] * 1);
						}
					}
				}
				$blank_total = $loc_total;
				
				echo "<br>";
				echo "Total Locations with Payments for All US States ".$filter_name.": " . $us_count . "<br>";
				echo "Total Amount for All US States ".$filter_name.": $" . number_format($us_total,2) . "<br>";
				echo "Total Locations with Payments for All Non US States ".$filter_name.": " . $non_us_count . "<br>";
				echo "Total Amount for All Non US States ".$filter_name.": $" . number_format($non_us_total,2) . "<br>";
				echo "Total Locations with Payments ".$filter_name.": " . ($us_count + $non_us_count) . "<br>";
				echo "Distro Amount ".$filter_name.": $" . number_format($distro_total,2) . "<br>";
				echo "Other Amount ".$filter_name.": $" . number_format($blank_total,2) . "<br>";
				echo "Total Amount ".$filter_name.": $" . number_format($us_total + $non_us_total + $distro_total + $blank_total,2) . "<br>";
				echo "<br><br><br><br>";
				foreach($other_types as $okey => $oval) 
				{
					$odataname = "";
					$odemo = "";
					$odev = "";
					$orloc = "";
					if(is_numeric($okey))
					{
						$rest_query = mlavu_query("select `data_name`,`id`,`demo`,`dev` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]' limit 1",$okey);
						if(mysqli_num_rows($rest_query))
						{
							$rest_read = mysqli_fetch_assoc($rest_query);
							$odataname = $rest_read['data_name'];
							if($rest_read['demo']=="1") $odemo = "DEMO";
							if($rest_read['dev']=="1") $odev = "DEV";

							$rest_query = mlavu_query("select `id` from `poslavu_MAIN_db`.`restaurant_locations` where `restaurantid`='[1]' limit 1",$okey);
							if(mysqli_num_rows($rest_query))
							{
							}
							else
							{
								$orloc = " (NOT IN MAIN LIST)";
							}
						}
					}
					//echo "$okey x $oval = $odataname <b>$odemo $odev $orloc</b><br>";
				}
				echo "<br><br><br><br>";
				exit();
			}
			
			//$state_list[] = array("nm","new mexico");
			//$state_list[] = array("tx","texas");
			$output = "";
			$export_output = "";
			$all_state_total = 0;
			$all_state_count = 0;
			$states_with_locs = 0;
			$states_repped = array();
			//$filter_name = "(March)";
			//$filter_number = "03";
			//$filter_name = "(June)";
			//$filter_number = "06";
			
			for($s=0; $s<count($state_list); $s++)
			{
				$state_arr = $state_list[$s];
				$show_state_name = "";
				$state_cond = "";
				$show_state_cond = "";
				for($n=0; $n<count($state_arr); $n++)
				{
					if($show_state_name=="" && trim($state_arr[$n])!="")
					{
						$show_state_name = trim($state_arr[$n]);
					}
					if($state_cond != "") $state_cond .= " or ";
					$state_cond .= "`restaurant_locations`.`state` LIKE '".$conn->escapeString(trim($state_arr[$n]))."' or `restaurant_locations`.`state` LIKE '".$conn->escapeString(trim($state_arr[$n]))." ' or `restaurant_locations`.`state` LIKE ' ".$conn->escapeString(trim($state_arr[$n]))."'";
					if(strlen($state_arr[$n]) > 3)
					{
						$state_cond .= " or `restaurant_locations`.`state` LIKE '%".$conn->escapeString(trim($state_arr[$n]))."%'";
					}
					if($show_state_cond != "") $show_state_cond .= " or ";
					$show_state_cond .= "\"" . $state_arr[$n] . "\"";
				}
				if($state_cond!="")
				{
					//$output .="<br><br>Condition: $show_state_cond<br>";
					
					$all_year_total = 0;
					$first_year = "";
					for($year=2014; $year<=2014; $year++)
					{
						$rests_processed = array();
						
						$loc_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurant_locations` left join `poslavu_MAIN_db`.`restaurants` on `restaurant_locations`.`restaurantid`=`restaurants`.`id` where ($state_cond) and `restaurants`.`demo`!='1' and `restaurants`.`dev`!='1' and `restaurant_locations`.`title` NOT LIKE '%demo%' and `restaurant_locations`.`title` NOT LIKE '%test%' and `restaurant_locations`.`title` NOT LIKE '%default%' and `restaurant_locations`.`title` NOT LIKE '%lavu%' and `restaurant_locations`.`title` NOT LIKE '%sqa%' order by `restaurants`.`id` desc, `restaurant_locations`.`id` desc limit 1000");
						//$output .="Locations records found: " . mysqli_num_rows($loc_query) . "<br>";
						
						$rfound = 0;
						$all_total = 0;
						$rfound_paid = 0;
						$loc_info = "";
						while($loc_read = mysqli_fetch_assoc($loc_query))
						{
							$rest_id = $loc_read['restaurantid'];
							//$output .=$rest_id . "<br>";
							
							if(isset($rests_processed[$rest_id]))
							{
								$rests_processed[$rest_id]++;
							}
							else
							{
								$loc_total = 0;
								$rests_processed[$rest_id] = 1;
								$rfound++;
												
								$min_datetime = $year . "-".$filter_number."-01 00:00:00";
								$max_datetime = $year . "-".$filter_number."-31 24:00:00";		
								$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1' and `datetime`>='[2]' and `datetime`<='[3]'",$rest_id,$min_datetime,$max_datetime);
								while($pay_read = mysqli_fetch_assoc($pay_query))
								{
									if($pay_read['x_type']=="credit")
									{
										$loc_total -= ($pay_read['x_amount'] * 1);
									}
									else
									{
										$loc_total += ($pay_read['x_amount'] * 1);
									}
								}
								
								if($loc_total != 0)
								{
									//$loc_info .= $loc_read['restaurantid'] . " ";
									$loc_info .= $loc_read['title'] . " " . $loc_read['city'] . " " . $loc_read['state'] . " ";
									
									$show_loc_amount = "";
									if($loc_total < 0) $show_loc_amount .= "-";
									else $show_loc_amount .= "+";
									$show_loc_amount .= " $" . number_format($loc_total,2);
									
									$loc_info .= $show_loc_amount . "<br>";
									$export_output .= csv_col($show_state_name) . "," . csv_col($loc_read['city']) . "," . csv_col($loc_read['title']) . "," . csv_col($show_loc_amount) . "\n";
									
									$all_total += $loc_total;
									$rfound_paid++;
								}
							}
						}
						
						/*$distro_total = 0;
						$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_state`='[4]' and `match_restaurantid` LIKE 'DS%' and (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1' and `datetime`>='[2]' and `datetime`<='[3]'",$rest_id,$min_datetime,$max_datetime,$state_arr[0],$state_arr[1]);
						while($pay_read = mysqli_fetch_assoc($pay_query))
						{
							if($pay_read['x_type']=="credit")
							{
								$distro_total -= ($pay_read['x_amount'] * 1);
							}
							else
							{
								$distro_total += ($pay_read['x_amount'] * 1);
							}							
						}
						if($distro_total != 0)
						{
							$loc_info .= "Distro";
							if($distro_total < 0) $loc_info .= "-";
							else $loc_info .= "+";
							$loc_info .= " $" . number_format($distro_total,2) . "<br>";
							$all_total += $distro_total;
						}*/
						
						//$output .="Distinct Locations found: " . $rfound . "<br>";
						if($all_total > 0)
						{
							$output .="<br>";
							if($first_year != "") $output .="<hr>";
							$output .="Year: $year $filter_name <b>".$show_state_cond."</b><br>";
							$output .="Distinct Locations with Payments: " . $rfound_paid . "<br>";
							$output .="Total Amount: $" . number_format($all_total,2) . "<br>";
							$output .="<br>";
							$output .="<table cellpadding=6><tr><td bgcolor='#eeeeee' style='border:solid 1px #bbbbbb'>$loc_info</td></tr></table>";
							if($first_year=="") $first_year = $year;
							$all_year_total += ($all_total * 1);
							$all_state_total += ($all_total * 1);
							$all_state_count += ($rfound_paid * 1);
							
							if(!isset($states_repped[$show_state_cond]))
							{
								$states_repped[$show_state_cond] = 1;
							}
							else
							{
								$states_repped[$show_state_cond] += 1;
								$output .="<b>Warning: State Already Represented: $show_state_cond</b><br>";
							}
							$states_with_locs += 1;
							
							$export_output .= csv_col($show_state_name) . ",Total,".csv_col($rfound_paid . " Locations").",," . csv_col("$" . number_format($all_total,2)) . "\n\n";
						}
					}
					//$output .="<br><hr>";
					//$output .="Total Amount for All Years $filter_name: $" . number_format($all_year_total,2) . "<br>";
					//$output .="<br><br>";
				}
			}
			$output .="<br><hr>";
			$output .="Total Locations with Payments for All States ".$filter_name.": " . $all_state_count . "<br>";
			$output .="Total Amount for All States ".$filter_name.": $" . number_format($all_state_total,2) . "<br>";
			$output .="Number of States Represented ".$filter_name.": " . $states_with_locs . "<br>";
			$output .= "<br><br><br><br>";
			$export_output .= ",Total - All,".csv_col($all_state_count . " Locations").",,," . csv_col("$" . number_format($all_state_total,2)) . "\n";
			
			if($export_mode)
			{
				//header('Content-disposition: attachment; filename=state_locations.txt');
				//header('Content-Type:text/txt');
				echo "<textarea rows='40' cols='120'>$export_output</textarea>";
				exit();
			}
			else
			{
				echo $output;
			}
		}
	}	
	else
	{
		echo "....|";
	}
?>
