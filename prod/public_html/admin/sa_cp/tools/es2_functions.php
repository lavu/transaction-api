<?php
	function read_tag_value($str,$tag,$def="")
	{
		$str_parts = explode("<" . $tag . ">",$str);
		if(count($str_parts) > 1)
		{
			$inner_parts = explode("</" . $tag . ">",$str_parts[1]);
			if(count($inner_parts) > 1)
			{
				return trim($inner_parts[0]);
			}
		}
		return $def;
	}
	
	function e2_api_create_new_password($length=12)
	{
		$str = "";
		for($i=0; $i<$length; $i++)
		{
			$n = rand(0,61);
			if($n < 10) // 0-9
			{
				$setn = 48 + $n;
			}
			else if($n<=35) // 10-35
			{
				$setn = 55 + $n;
			}
			else if($n<=61) // 36-61
			{
				$setn = 61 + $n;
			}
			$str .= chr($setn);
		}
		return $str;
	}
	
	function process_v2_create_site($folder,$start_username,$start_password,$set_company_name,$model_type="",$settings=array())
	{
		$setting_str = "";
		foreach($settings as $skey => $sval) $setting_str .= "<setting>" . $skey . "<value>" . urlencode($sval);
		
		$setupkey = "testkey";
		$product = "v1created";
		$setup_folder = "default_general";
		
		if($model_type!="") $product = $model_type;
		$postvars = "key=boq4g8GBhhw5g8hgABDF74grAz5&processdb=create_new_site&folder=$folder&product=$product&setup_folder=$setup_folder&setupkey=$setupkey&start_settings=" . $setting_str;
		$postvars .= "&set_admin_usr=$start_username";
		$postvars .= "&set_admin_pwd=$start_password";
		$postvars .= "&setting_company_name=$set_company_name";
		
		$set_url = "https://manage.ershost.com/import/process.php";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $set_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$response = curl_exec($ch);
		curl_close($ch);
		
		$val = read_tag_value($response,"success","0");
		if($val=="1") return true;
		else return false;	
	}
	
	function get_folder_availability_from_server($folder)
	{
		$postvars = "key=boq4g8GBhhw5g8hgABDF74grAz5&special_action=check_availability&foldername=$folder";
		$set_url = "https://manage.ershost.com/import/process.php";
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $set_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$response = curl_exec($ch);
		curl_close($ch);
		
		$val = read_tag_value($response,"available","0");
		if($val=="1") return true;
		else return false;	
	}
?>