function send_ajax_request(send_url,dictionary_data) {
	send_ajax_request_retval = "";
	async = false;
	
	$.ajax({
		type: "POST",
		async: async,
		cache: false,
		url: send_url,
		data: dictionary_data,
		success: function(data) {
			send_ajax_request_retval = data;
		},
		error: function(xhr, ajaxOptions, thrownError) {
			send_ajax_request_retval = xhr.status+": "+thrownError;
		}
	});
	
	return send_ajax_request_retval;
}

function ajax_get_response(send_url,send_content,send_callback) {
    var xmlHttpReq = false;
    var self = this;
    // Mozilla/Safari
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
        self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq.open('POST', send_url, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState == 4) {
            var response = (self.xmlHttpReq.responseText);
			eval(send_callback + "(response); ");
        }
    }
    self.xmlHttpReq.send(send_content);
}

function ajax_get_internal_response(response) 
{
	var rsp = response.split('[---START---]');
	if(rsp.length > 1) {
		rsp = rsp[1].split('[---END---]');
		rsp = rsp[0];
		return rsp;
	}
	return '';
}

function ajax_get_vars(response) {
	var vars = new Array();
	var rsp = response;
	rsp = rsp.split('&');
	for(var i=0; i<rsp.length; i++) {
		var innerrsp = rsp[i].split('=');
		if(innerrsp.length > 1) {
			vars[innerrsp[0]] = innerrsp[1];
		}
	}
	return vars;
}

function ajax_get_internal_vars(response) {
	var rsp = ajax_get_internal_response(response);
	return ajax_get_vars(rsp);
}

function find_and_eval_script_tags(estr,script_start,script_end) {
	var xhtml = estr.split(script_start);
	for(var n=1; n<xhtml.length; n++)
	{
		var xhtml2 = xhtml[n].split(script_end);
		if(xhtml2.length > 1)
		{
			run_xhtml = xhtml2[0];
			eval(run_xhtml);
		}
	}
}

function eval_javascript_tags(evstr) {
	find_and_eval_script_tags(evstr,"<" + "script language=\"javascript\"" + ">","<" + "/script>");
	find_and_eval_script_tags(evstr,"<" + "script language='javascript'" + ">","<" + "/script>");
}

function parse_ajax_response(containerid,rspstr) {
	eval_javascript_tags(rspstr);
	document.getElementById(containerid).innerHTML = rspstr;
}
