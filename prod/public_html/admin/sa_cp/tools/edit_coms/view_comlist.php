<?php
	if(can_access("technical") && $tool=="edit_coms")
	{
		
		require_once(dirname(__FILE__).'/shared_view_functions.php');
		
		function update_com_options($update_code, $page_path) {
			mlavu_query("update `poslavu_MAIN_db`.`component_packages` set $update_code where `id`='[id]'",$_POST);
			echo "<script language='javascript'>window.location.replace('$page_path')</script>";
			exit();
		}
		
		$page_path = "index.php?mode=manage_customers&submode=general_tools&tool=edit_coms";
		$com_mode = getvar("com_mode","menu");
		
		if($com_mode=="menu")
		{
			$found_blank_locid = false;
			echo "<table cellpadding=4>";
			$com_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_packages` order by dev_locid desc");
			while($com_read = mysqli_fetch_assoc($com_query))
			{
				// com_read keys => id, title, layout_ids, parent, package_code, order_addons, notes, disabled
				$com_id = $com_read['id'];
				
				if(!$found_blank_locid && $com_read['dev_locid']=="")
				{
					echo "<tr><td colspan='2'><hr></td></tr>";
					$found_blank_locid = true;
				}
				
				echo "<tr><td valign='top' align='right'>";
				echo $com_read['title'];
				echo "<br><a href='$page_path&com_mode=view&com_id=$com_id' style='color: #000088; font-size:10px'>(View / Edit Layout)</a>";
				echo "</td>";
				echo "<td valign='top'>";
				echo "<textarea rows='2' cols='32' id='com_notes_$com_id' onfocus='com_notes_focus(\"$com_id\")' onblur='com_notes_blur(\"$com_id\")'>".$com_read['notes']."</textarea>";
				echo "</td></tr>";
			}
			echo "</table>";
			
			//---------------------------------------- ajax stuff ----------------------------------------//
			echo "<script language='javascript' src='/dev/components/lavukart/lib/ajax_functions.js'></script>";
			echo "<script language='javascript'>";
			echo "function com_notes_focus(com_id) { ";
			echo "	com_notes_start = document.getElementById('com_notes_' + com_id).value; ";
			echo "} ";
			echo "function com_notes_blur(com_id) { ";
			echo "	com_notes_current = document.getElementById('com_notes_' + com_id).value; ";
			echo "	if(com_notes_start != com_notes_current) { ";
			echo "		document.getElementById(\"com_notes_\" + com_id).style.backgroundColor = \"#eeeeee\"; ";
			echo "		ajax_call(\"/sa_cp/tools/edit_coms/com_save.php?com_id=\" + escape(com_id) + \"&set_notes=\" + escape(com_notes_current)); ";
			echo "	} ";
			echo "} ";
			echo "function ajax_received(rsp) { ";
			echo "	if(rsp['success']=='1') { ";
			echo "		document.getElementById(\"com_notes_\" + rsp['com_id']).style.backgroundColor = \"#ddffdd\"; ";
			echo "	} ";
			//echo " alert(rsp['success']); ";
			//echo "  alert(unescape(rsp['msg'].replace(/\+/ig,'%20'))); ";
			echo "} ";
			echo "</script>";
			
			//echo "<input type='button' value='eval function test' onclick='ajax_call(\"/sa_cp/tools/edit_coms/comstuff.php\")' /><br><br>";
			//-------------------------------------------------------------------------------------------//
		}
		else if($com_mode=="view")
		{
			function draw_edit_combox($component_read,$set_title="")
			{
				$cid = $component_read['id'];
				if(is_numeric($cid))
					$comz = (1000 + $cid);
				else
					$comz = 1000;
				$cname = $component_read['name'];
				if($set_title=="") $set_title = $component_read['title'];
				
				if(isset($_GET['tst']))
					foreach($component_read as $key => $val) echo $key . " = " . $val . "<br>";
				
				$str = "";
				$str .= "<div style='position:relative'>";
				$str .= "<nobr><a style='cursor:pointer' onclick='document.getElementById(\"edit_combox_$cid\").style.display = \"block\"'>" . $set_title . "</a></nobr>";

				$str .= "<div id='edit_combox_$cid' style='position:absolute; left:-240px; top:0px; display:none; z-index:$comz'>";
				$str .= "<table style='border:solid 1px black' bgcolor='#eeeeee' cellspacing=0 cellpadding=4>";
				$str .= "<tr><td bgcolor='#888888' style='color:#ffffff' align='right'>";
				$str .= "<a style='font-weight:bold; color:#ffffff; cursor:pointer' onclick='document.getElementById(\"edit_combox_$cid\").style.display = \"none\"'>(X)</a>";
				$str .= "</td></tr>";
				$str .= "<tr><td width='400' height='200' valign='top' align='center'>";
				$str .= "<form method='post' id='com_form_$cid' action=''>";
				$str .= "<input type='hidden' name='edit_com_id' value='$cid'>";
				$str .= "<input type='hidden' name='edit_com_action_$cid' id='edit_com_action_$cid' value=''>";
				$str .= "<input type='hidden' name='edit_com_vars_$cid' id='edit_com_vars_$cid' value=''>";
				$str .= "<table>";
				$str .= "<tr><td align='right'>Name</td><td align='left'><input type='text' size='30' name='edit_com_name' value=\"".str_replace("\"","&quot;",$component_read['name'])."\"></td></tr>";
				$str .= "<tr><td align='right'>Title</td><td align='left'><input type='text' size='30' name='edit_com_title' value=\"".str_replace("\"","&quot;",$component_read['title'])."\"></td></tr>";
				$str .= "<tr><td align='right'>Source</td><td align='left'><input type='text' size='42' style='font-size:10px' name='edit_com_filename' value=\"".str_replace("\"","&quot;",$component_read['filename'])."\"></td></tr>";
				$str .= "<tr><td align='right'>Scroll</td><td align='left'><input type='checkbox' name='edit_com_scroll' ".($component_read['allow_scroll']=="1"?" checked":"")."></td></tr>";
				$str .= "<tr><td>&nbsp;</td><td align='left'>";
				if($cid=="new")
				{
					$str .= "<input type='submit' value='Create' style='font-size:10px'>";
					$str .= "&nbsp;-or-&nbsp;";
					$str .= "<select style='font-size:9px' onchange='document.getElementById(\"edit_com_action_$cid\").value = \"add_com\"; document.getElementById(\"edit_com_vars_$cid\").value = \"add_com_id=\" + this.value; document.getElementById(\"com_form_$cid\").submit();'>";
					$str .= "<option value=''>Add Existing Component</option>";
					$comp_query = mlavu_query("select * from `poslavu_MAIN_db`.`components` order by `title` asc");
					while($comp_read = mysqli_fetch_assoc($comp_query))
					{
						$compid = $comp_read['id'];
						$comptitle = $comp_read['title'];
						
						$str .= "<option value='$compid'>$comptitle</option>";
					}
					$str .= "</select>";
				}
				else
				{
					$str .= "<input type='submit' value='Update' style='font-size:10px'>";
					$str .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
					$str .= "<a style='color:#aa0000; cursor:pointer; font-size:10px' onclick='if(confirm(\"Are you sure you want to remove this component?\")) { document.getElementById(\"edit_com_action_$cid\").value = \"remove\"; document.getElementById(\"com_form_$cid\").submit(); }'>(remove)</a>";
				}
				$str .= "</td></tr>";
				
				$str .= "</table>";
				$str .= "</form>";
				$str .= "</td></tr>";
				$str .= "</table>";
				$str .= "</div>";
				
				$str .= "</div>";
				return $str;
			}
			
			function update_layout_cols($ctab,$edit_com_id,$edit_com_action="remove",$edit_com_vars=false)
			{
				$layout_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_layouts` where `id`='[1]' and `_deleted`!='1'",$ctab);
				if(mysqli_num_rows($layout_query))
				{
					$layout_read = mysqli_fetch_assoc($layout_query);
					$lc_ids = explode(",",trim($layout_read['c_ids']));
					$lc_xs = explode(",",trim($layout_read['c_xs']));
					$lc_ys = explode(",",trim($layout_read['c_ys']));
					$lc_ws = explode(",",trim($layout_read['c_ws']));
					$lc_hs = explode(",",trim($layout_read['c_hs']));
					$lc_zs = explode(",",trim($layout_read['c_zs']));
					
					$c_ids = "";
					$c_xs = "";
					$c_ys = "";
					$c_ws = "";
					$c_hs = "";
					$c_zs = "";
					for($n=0; $n<count($lc_ids); $n++)
					{
						$include_col = false;
						$set_id = $lc_ids[$n];
						$set_x = $lc_xs[$n];
						$set_y = $lc_ys[$n];
						$set_w = $lc_ws[$n];
						$set_h = $lc_hs[$n];
						$set_z = $lc_zs[$n];
						
						if($edit_com_action=="remove")
						{
							if($lc_ids[$n]!=$edit_com_id) $include_col = true;
							else $include_col = false;
							
						}
						if($edit_com_action=="update")
						{
							$include_col = true;
							
							if($set_id==$edit_com_id)
							{
								$set_x = $edit_com_vars['x'];
								$set_y = $edit_com_vars['y'];
								$set_w = $edit_com_vars['w'];
								$set_h = $edit_com_vars['h'];
								$set_z = $edit_com_vars['z'];
							}
						}
						
						if($include_col)
						{
							if($c_ids!="") $c_ids .= ","; $c_ids .= $set_id;
							if($c_xs!="") $c_xs .= ","; $c_xs .= $set_x;
							if($c_ys!="") $c_ys .= ","; $c_ys .= $set_y;
							if($c_ws!="") $c_ws .= ","; $c_ws .= $set_w;
							if($c_hs!="") $c_hs .= ","; $c_hs .= $set_h;
							if($c_zs!="") $c_zs .= ","; $c_zs .= $set_z;
						}
					}
					$c_vars = array("ids"=>$c_ids,"xs"=>$c_xs,"ys"=>$c_ys,"ws"=>$c_ws,"hs"=>$c_hs,"zs"=>$c_zs,"id"=>$layout_read['id']);
					mlavu_query("update `poslavu_MAIN_db`.`component_layouts` set `c_ids`='[ids]', `c_xs`='[xs]', `c_ys`='[ys]', `c_ws`='[ws]', `c_hs`='[hs]', `c_zs`='[zs]' where `id`='[id]'",$c_vars);
				}
			}
			
			function get_urlstring_val($str,$var)
			{
				$vars = array();
				$str_parts = explode("&",$str);
				for($i=0; $i<count($str_parts); $i++)
				{
					$part_parts = explode("=",$str_parts[$i]);
					if(count($part_parts) > 1)
					{
						$vars[trim($part_parts[0])] = trim($part_parts[1]);
					}
				}
				
				if(isset($vars[$var])) return $vars[$var]; else return "";
			}
									
			$com_id = getvar("com_id");
			$ctab = getvar("ctab");
			if(isset($_POST['edit_com_id']))
			{
				$edit_com_id = postvar("edit_com_id");
				$edit_com_name = postvar("edit_com_name");
				$edit_com_title = postvar("edit_com_title");
				$edit_com_filename = postvar("edit_com_filename");
				$edit_com_action = postvar("edit_com_action_".$edit_com_id);
				$edit_com_scroll = "";
				if(isset($_POST['edit_com_scroll'])) $edit_com_scroll = "1";
				
				//foreach($_POST as $key => $val) echo $key . " = " . $val . "<br>";
				
				if($edit_com_action=="update_coords")
				{
					$edit_com_id_list = explode("|",$edit_com_id);
					
					for($n=0; $n<count($edit_com_id_list); $n++)
					{
						$edit_com_id = trim($edit_com_id_list[$n]);
						$edit_com_vars = postvar("edit_com_vars_".$edit_com_id);
						if(is_numeric($edit_com_id) && trim($edit_com_vars)!="")
						{
							$set_x = get_urlstring_val($edit_com_vars,"x");
							$set_y = get_urlstring_val($edit_com_vars,"y");
							$set_w = get_urlstring_val($edit_com_vars,"w");
							$set_h = get_urlstring_val($edit_com_vars,"h");
							$set_z = get_urlstring_val($edit_com_vars,"z");
							$set_fmod_id = get_urlstring_val($edit_com_vars,"fmod_id");
							$set_layout_id = get_urlstring_val($edit_com_vars,"layout_id");
							
							echo "ctab: $ctab<br>";
							echo "layout_id: $set_layout_id<br>";
							echo "edit_com_id: $edit_com_id<br>";
							echo "set_x: $set_x<br>";
							echo "set_y: $set_y<br>";
							echo "set_w: $set_w<br>";
							echo "set_h: $set_h<br>";
							echo "set_z: $set_z<br>";
							echo "set_fmod_id: $set_fmod_id<br>";
							echo "<br>";
							
							if($ctab!="") $use_layout_id = $ctab; else $use_layout_id = $set_layout_id;
							
							update_layout_cols($use_layout_id,$edit_com_id,"update",array("x"=>$set_x,"y"=>$set_y,"w"=>$set_w,"h"=>$set_h,"z"=>$set_z));
						}
					}
				}
				else if($edit_com_action=="remove")
				{
					if(is_numeric($edit_com_id) && $edit_com_id > 0)
					{
						update_layout_cols($ctab,$edit_com_id,"remove");
					}
				}
				else if($edit_com_id=="new" || $edit_com_action=="add_com")
				{
					if($edit_com_action=="add_com")
					{
						$edit_com_vars = postvar("edit_com_vars_".$edit_com_id);
						
						$add_com_id = get_urlstring_val($edit_com_vars,"add_com_id");
						if(is_numeric($add_com_id) && $add_com_id > 0)
						{
							$new_com_id = $add_com_id;
						}
					}
					else if($edit_com_id=="new")
					{
						mlavu_query("insert into `poslavu_MAIN_db`.`components` (`name`, `title`, `filename`, `allow_scroll`) values ('[1]','[2]','[3]','[4]')",$edit_com_name,$edit_com_title,$edit_com_filename,$edit_com_scroll);
						$new_com_id = mlavu_insert_id();
					}
					else
					{
						$new_com_id = 0;
					}
					
					if(is_numeric($new_com_id) && $new_com_id > 0)
					{
						$layout_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_layouts` where `id`='[1]' and `_deleted`!='1'",$ctab);
						if(mysqli_num_rows($layout_query))
						{
							$layout_read = mysqli_fetch_assoc($layout_query);
							$c_ids = trim($layout_read['c_ids']);
							$c_xs = trim($layout_read['c_xs']);
							$c_ys = trim($layout_read['c_ys']);
							$c_ws = trim($layout_read['c_ws']);
							$c_hs = trim($layout_read['c_hs']);
							$c_zs = trim($layout_read['c_zs']);
							if($c_ids!="") $c_ids .= ","; $c_ids .= $new_com_id;
							if($c_xs!="") $c_xs .= ","; $c_xs .= "0";
							if($c_ys!="") $c_ys .= ","; $c_ys .= "0";
							if($c_ws!="") $c_ws .= ","; $c_ws .= "0";
							if($c_hs!="") $c_hs .= ","; $c_hs .= "0";
							if($c_zs!="") $c_zs .= ","; $c_zs .= "0";
							
							//echo "Com Packages: " . $package_read['id'] . " to $layout_ids<br>";
							$c_vars = array("ids"=>$c_ids,"xs"=>$c_xs,"ys"=>$c_ys,"ws"=>$c_ws,"hs"=>$c_hs,"zs"=>$c_zs,"id"=>$layout_read['id']);
							mlavu_query("update `poslavu_MAIN_db`.`component_layouts` set `c_ids`='[ids]', `c_xs`='[xs]', `c_ys`='[ys]', `c_ws`='[ws]', `c_hs`='[hs]', `c_zs`='[zs]' where `id`='[id]'",$c_vars);
						}
					}
				}
				else if($edit_com_id!="" && is_numeric($edit_com_id))
				{
					mlavu_query("update `poslavu_MAIN_db`.`components` set `name`='[1]', `title`='[2]', `filename`='[3]', `allow_scroll`='[4]' where `id`='[5]'",$edit_com_name,$edit_com_title,$edit_com_filename,$edit_com_scroll,$edit_com_id);
				}
			}
			
			$package_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_packages` where `id`='[1]' and `disabled`!='1'",$com_id);
			if(mysqli_num_rows($package_query))
			{
				// id, title, layout_ids, parent, package_code, order_addons, notes, disabled
				$package_read = mysqli_fetch_assoc($package_query);
				$package_title = $package_read['title'];
				
				$addon_info = explode("|o|",get_addon_info($package_read));
				if(count($addon_info) > 1)
				{
					$addon_id = trim($addon_info[0]);
					$addon_title = trim($addon_info[1]);
				}
				else
				{
					$addon_id = 0;
					$addon_title = "";
				}
			
				$ctab = getvar("ctab");
				$view_mode = getvar("view_mode","");
				if($view_mode=="" && isset($_SESSION['com_view_mode']))
					$view_mode = $_SESSION['com_view_mode'];
				$_SESSION['com_view_mode'] = $view_mode;
				$mod_mode = getvar("mod_mode","");
				if($mod_mode=="" && isset($_SESSION['mod_mode']))
					$mod_mode = $_SESSION['mod_mode'];
				$_SESSION['mod_mode'] = $mod_mode;
				
				if(substr($mod_mode,0,5)=="fmod_")
				{
					$fmod_id = substr($mod_mode,5);
				}
				else $fmod_id = "";
				
				$dev_selector = "";
				$dev_selector .= "<select style='font-size:10px' onchange='window.location = \"$page_path&com_mode=$com_mode&com_id=$com_id&ctab=$ctab&view_mode=\" + this.value'>";
				$dev_selector .= "<option value='dev'".($view_mode=="dev"?" selected":"").">View Dev Coms</option>";
				$dev_selector .= "<option value='live'".($view_mode=="live"?" selected":"").">View Live Coms</option>";
				$dev_selector .= "</select>";

				$mod_selector = "";
				$mod_selector .= "<select style='font-size:10px' onchange='window.location = \"$page_path&com_mode=$com_mode&com_id=$com_id&ctab=$ctab&mod_mode=\" + this.value'>";
				$mod_selector .= "<option value='addon'".($mod_mode=="addon"?" selected":"").">Order Add-On</option>";
				if($package_read['dev_account']!="")
				{
					$rdb = "poslavu_" . $package_read['dev_account'] . "_db";
					$flist_query = mlavu_query("select * from `[1]`.`forced_modifier_lists` where `type`='custom'",$rdb);
					while($flist_read = mysqli_fetch_assoc($flist_query))
					{
						$flist_name = $flist_read['title'];
						$flist_id = $flist_read['id'];
						
						$fmod_query = mlavu_query("select * from `[1]`.`forced_modifiers` where `list_id`='[2]'",$rdb,$flist_id);
						while($fmod_read = mysqli_fetch_assoc($fmod_query))
						{
							$fmode = 'fmod_'.$fmod_read['id'];
							$mod_selector .= "<option value='$fmode'".($mod_mode==$fmode?" selected":"").">".$flist_name."</option>";
						}
					}
				}
				$mod_selector .= "</select>";
			
				echo com_navbar("View Component Package: $package_title",$dev_selector);
				echo "<table><tr><td valign='top'>";
				echo com_options($package_read,$page_path . "&com_mode=$com_mode&com_id=$com_id");
				echo "</td><td width='20'>&nbsp;</td><td valign='top'>";
								
				$lstr = "";
				$lstr .= "<div style='position:relative'>";
				$at_addon = false;
				$layouts = explode(",",$package_read['layout_ids']);
				for($n=0; $n<count($layouts); $n++)
				{
					$layout_id = $layouts[$n];
					if($layout_id==0) $layout_id = $addon_id;
					
					if((isset($_GET['ctab']) && $_GET['ctab']==$layout_id) || (!isset($_GET['ctab']) && $n==0))
					{
						if($layout_id==$addon_id) $at_addon = true;
						$layout_read = array();
						if($fmod_id!="")
						{
							$layout_read = get_custom_modifier_layout($package_read,$fmod_id);
						}
						else
						{
							$layout_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_layouts` where `id`='[1]'",$layout_id);
							if(mysqli_num_rows($layout_query))
							{
								$layout_read = mysqli_fetch_assoc($layout_query);
							}
						}
						if(isset($layout_read['id'])) $layout_id = $layout_read['id']; else $layout_id = "";
						
						if(isset($layout_read['c_ids']) && $layout_read['c_ids']!="")
						{	
							$lstr .= "<table cellspacing=0 cellpadding=2>";
							$lstr .= "<tr>";
							$lstr .= "<td align='right' style='border-bottom:solid 1px black'>";
							
							$lstr .= draw_edit_combox(array("id"=>"new","name"=>"","title"=>"","filename"=>"","allow_scroll"=>""),"Component");
							//$lstr .= "Component";
							$lstr .= "</td>";
							$lstr .= "<td align='center' style='border-bottom:solid 1px black'>X</td>";
							$lstr .= "<td align='center' style='border-bottom:solid 1px black'>Y</td>";
							$lstr .= "<td align='center' style='border-bottom:solid 1px black'>W</td>";
							$lstr .= "<td align='center' style='border-bottom:solid 1px black'>H</td>";
							$lstr .= "<td align='center' style='border-bottom:solid 1px black'>Z</td>";
							$lstr .= "</tr>";
							
							$c_ids = explode(",",$layout_read['c_ids']);
							$c_xs = explode(",",$layout_read['c_xs']);
							$c_ys = explode(",",$layout_read['c_ys']);
							$c_ws = explode(",",$layout_read['c_ws']);
							$c_hs = explode(",",$layout_read['c_hs']);
							$c_zs = explode(",",$layout_read['c_zs']);
							
							for($n=0; $n<count($c_ids); $n++)
							{
								$component_read = array();
								$component = trim($c_ids[$n]);
								if(strpos($component,".php"))
								{
									$component_read = array("name"=>trim($c_ids[$n]));
								}
								else
								{
									$component_query = mlavu_query("select * from `poslavu_MAIN_db`.`components` where `id`='[1]'",$component);
									if(mysqli_num_rows($component_query))
									{
										$component_read = mysqli_fetch_assoc($component_query);
									}
								}
								
								if(isset($component_read['name']) && $component_read['name']!="")
								{
									$cid = $c_ids[$n];
									$comz = (1000 + $cid);
									$cname = $component_read['name'];
									
									$lstr .= "<tr>";
									$lstr .= "<td align='right'>";
									
									if(is_numeric($cid))
									{
										$lstr .= draw_edit_combox($component_read);
									}
									else
									{
										$lstr .= $cid;
									}
									
									$lstr .= "</td>";
									$lstr .= "<td align='center'><input type='text' name='x_coord_$cid' id='x_coord_$cid' value='".$c_xs[$n]."' size='2' onkeyup='update_com_coords(\"$cid\",\"$cname\",\"container\",\"left\",this.value)'></td>";
									$lstr .= "<td align='center'><input type='text' name='y_coord_$cid' id='y_coord_$cid' value='".$c_ys[$n]."' size='2' onkeyup='update_com_coords(\"$cid\",\"$cname\",\"container\",\"top\",this.value)'></td>";
									$lstr .= "<td align='center'><input type='text' name='w_coord_$cid' id='w_coord_$cid' value='".$c_ws[$n]."' size='2' onkeyup='update_com_coords(\"$cid\",\"$cname\",\"iframe\",\"width\",this.value)'></td>";
									$lstr .= "<td align='center'><input type='text' name='h_coord_$cid' id='h_coord_$cid' value='".$c_hs[$n]."' size='2' onkeyup='update_com_coords(\"$cid\",\"$cname\",\"iframe\",\"height\",this.value)'></td>";
									$lstr .= "<td align='center'><input type='text' name='z_coord_$cid' id='z_coord_$cid' value='".$c_zs[$n]."' size='2' onkeyup='update_com_coords(\"$cid\",\"$cname\",\"container\",\"zIndex\",this.value)'></td>";
									$lstr .= "<td align='center'><input type='button' value='Save' style='display:none; font-size:10px' id='com_coord_save_$cid' onclick='document.getElementById(\"edit_com_action_$cid\").value = \"update_coords\"; document.getElementById(\"edit_com_vars_$cid\").value = \"x=\" + document.getElementById(\"x_coord_$cid\").value + \"&y=\" + document.getElementById(\"y_coord_$cid\").value + \"&w=\" + document.getElementById(\"w_coord_$cid\").value + \"&h=\" + document.getElementById(\"h_coord_$cid\").value + \"&z=\" + document.getElementById(\"z_coord_$cid\").value + \"&layout_id=$layout_id&fmod_id=$fmod_id\"; document.getElementById(\"com_form_$cid\").submit()'></td>";
									$lstr .= "</tr>";
								}
							}
							$lstr .= "</table>";
						}
					}
				}
				$lstr .= "</div>";
				
				if($at_addon)
				{
					echo $mod_selector;
				}
				echo $lstr; // Layout Editor - show after the mod selector
				
				echo "<script language='javascript'>";
				echo "function update_com_coords(comid,cname,ctarget,coordtype,setval) { ";
				echo "	document.getElementById('com_coord_save_' + comid).style.display = 'block'; ";
				//echo "	alert(ctarget + ' com_container_' + comid); ";
				echo "	if(ctarget=='container') ";
				echo "		var elem = document.getElementById('com_container_' + comid); ";
				echo "	else ";
				echo "		var elem = document.getElementById('com_' + cname); ";
				echo "	eval(\"elem.style.\" + coordtype + \" = '\" + setval + \"px'\"); ";
				//echo "	alert(comid + ' - ' + coordtype + ' - ' + setval); ";
				echo "} ";
				echo "</script>";
				
				echo "</td></tr></table>";
				
				if(trim($package_read['dev_account'])!="" && trim($package_read['dev_comcode'])!="")
				{
					require_once(dirname(__FILE__) . "/view_coms.php");
					echo "<table width='100%'><tr><td align='center' width='100%'>";
					view_coms($package_read,$page_path . "&com_mode=$com_mode&com_id=$com_id",$view_mode,$fmod_id);
					echo "</td></tr></table>";
				}
				else
				{
					echo "&nbsp;&nbsp;Please set up development account credentials to edit this component package<br><br>";
				}
			}
		}
	}
?>
