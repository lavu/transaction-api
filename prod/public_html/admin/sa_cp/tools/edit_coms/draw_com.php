<?php
	session_start();
	//ini_set("display_errors","1");
	//require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	function com_path()
	{
		if(isset($_SESSION['com_view_mode']) && $_SESSION['com_view_mode']=="live")
			return "/components";
		else
			return "/dev/components";
	}
	if(!function_exists("getvar"))
	{
		function getvar($var,$def="")
		{
			return (isset($_GET[$var]))?$_GET[$var]:$def;
		}
	}
	
	
	// cc, dn, tab_name, loc_id, server_id, server_name
	if(isset($_GET['usecc'])) $cc = $_GET['usecc'];
	else if(isset($_GET['cc'])) $cc = $_GET['cc'];
	if(isset($_GET['uselocid'])) $loc_id = $_GET['uselocid'];
	else $loc_id = 1;
	
	$dn = getvar("dn", "ersdemo");
	$tab_name = getvar("tab_name");
	$server_id = 0;
	$server_name = "com_view";
	$_GET['usecc'] = $usecc;
	$_GET['uselocid'] = $loc_id;
	$_GET['cc'] = $cc;
	$_GET['dn'] = $dn;
	$_GET['tab_name'] = $tab_name;
	$_GET['loc_id'] = $loc_id;
	$_GET['server_id'] = $server_id;
	$_GET['server_name'] = $server_name;
	
	$main_comvars = "cc={$cc}&dn={$dn}&tab_name=$tab_name&loc_id={$loc_id}&server_id={$server_id}&server_name={$server_name}";
	
	ob_start();
	?>
		<script language="javascript">
			String.prototype.trim = function () {
				return this.replace(/^\s*/, "").replace(/\s*$/, "");
			}
			
			function do_com_cmd(str)
			{
				vars = new Array();
				url_split = str.split("?");
				if(url_split.length > 1)
					url_vars = url_split[1];
				else
					url_vars = "";
				str = url_split[0];
				parts = str.split("&");
				for(i=0; i<parts.length; i++)
				{
					part = parts[i].split("=");
					if(part.length > 1)
					{
						set_key = part[0].trim();
						set_val = part[1].trim();
						vars[set_key] = set_val;
					}
				}
				
				if(url_vars!="") url_vars += "&";
				url_vars += "<?php echo "usecc=" . $cc . "&uselocid=" . $loc_id; ?>";
				
				cmd = vars["cmd"];
				if(cmd=="show_pit_setup")
				{
					//_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/select_track.php&c_name=select_track_wrapper;select_track&dims=112,200,423,374;112,200,423,374
					cmd = "create_overlay";
					vars["f_name"] = "lavukart/newevent_wrapper.php;lavukart/select_grid.php";
					vars["c_name"] = "select_grid_wrapper;select_grid";
					vars["dims"] = "112,200,423,374;112,200,423,374";
					vars["scroll"] = "0;0";
					url_vars = "locid=<?php echo $loc_id?>";
				}
				if(cmd=="create_overlay")
				{
					f_name = vars["f_name"].split(";");
					c_name = vars["c_name"].split(";");
					dims = vars["dims"].split(";");
					if(typeof(vars["scroll"])!="undefined")
					{
						scrollable = vars["scroll"].split(";");
						scroll_exists = true;
					}
					else
					{
						scrollable = "";
						scroll_exists = false;
					}
					
					for(i=0; i<f_name.length; i++)
					{
						dimlist = dims[i].split(",");
						if(dimlist.length > 3)
						{
							set_left = dimlist[0];
							set_top = dimlist[1];
							set_width = dimlist[2];
							set_height = dimlist[3];
							set_zindex = (php_zindex * 1 + 1000);
							if(scroll_exists) set_scroll = scrollable[i];
							else set_scroll = "0";
							//alert(set_zindex);
							window.parent.create_overlay(f_name[i],c_name[i],set_left,set_top,set_width,set_height,set_scroll,set_zindex,url_vars);
						}
					}
					window.parent.record_overlay_group(c_name);
				}
				else if(cmd=="close_overlay")
				{
					if(typeof(vars["f_name"])!="undefined")
					{
						f_name = vars["f_name"].split(";");
						c_name = vars["c_name"].split(";");
						
						for(i=0; i<f_name.length; i++)
						{
							window.parent.component_load_new(c_name[i],f_name[i],url_vars);
						}
					}
					window.parent.close_overlay("php_self");
				}
				else if(cmd=="load_url")
				{
					if(typeof(vars["f_name"])!="undefined")
					{
						f_name = vars["f_name"].split(";");
						c_name = vars["c_name"].split(";");
						
						for(i=0; i<f_name.length; i++)
						{
							window.parent.component_load_new(c_name[i],f_name[i],url_vars);
							window.parent.component_set_visibility(c_name[i],'visible');
						}
					}
				}
				else if(cmd=="send_js")
				{
					c_name = vars["c_name"];
					c_js = vars["js"];
					
					window.parent.component_run_script(c_name,c_js);
				}
				else if(cmd=="make_hidden" || cmd=="make_visible")
				{
					if(typeof(vars["c_name"])!="undefined")
					{
						c_name = vars["c_name"].split(";");
						
						for(i=0; i<c_name.length; i++)
						{
							window.parent.component_set_visibility(c_name[i],cmd.substring(5));
						}
					}
				}
				else if(cmd =='set_scrollable' ){

				}
				else if( cmd == 'confirm' ){
					var c_name = vars['c_name'];
					var title = vars['title'];
					var message = decodeURIComponent( vars['message'] );
					var js_cmd = vars['js_cmd'];

					if( confirm( message ) ){
						window.parent.component_run_script( c_name, js_cmd );
					}
				}
				else
				{
					alert("unknown command: " + cmd);
				}
			}
		</script>
	<?php
	$com_script_code = ob_get_contents();
	ob_end_clean(); 	
	
	global $b_dev_components;
	$b_dev_components = FALSE;
	if(isset($_GET['drawmode']) && $_GET['drawmode']=="draw_component")
	{
		$fname = $_GET['fname'];
		$com_dirname = dirname($fname);
		$b_dev_components = TRUE;
		
		/*$cc = $_GET['cc'];
		$dn = $_GET['dn'];
		$tab_name = $_GET['tab_name'];
		$loc_id = $_GET['loc_id'];
		$server_id = $_GET['server_id'];
		$server_name = $_GET['server_name'];*/
		$self = $_GET['self'];
		$zindex = $_GET['zindex'];
		$vars = $main_comvars;
		
		$gvars = "";
		foreach($_GET as $gkey => $gval)
		{
			if(trim($gkey)!="" && strpos($vars,trim($gkey)."=")===false)
			{
				if($gvars=="") $gvars .= "?"; else $gvars .= "&";
				$gvars .= trim($gkey) . "=" . trim($gval);
			}
		}
		
		/*$ch = curl_init("http://admin.poslavu.com/components/".$fname.$gvars);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);*/
		ob_start();
		
		$var_parts = explode("&",$vars);
		for($n=0; $n<count($var_parts); $n++)
		{
			$var_part = explode("=",$var_parts[$n]);
			if(count($var_part) > 1)
			{
				$var_key = trim($var_part[0]);
				$var_val = trim($var_part[1]);
				if($var_key!="")
				{
					$_POST[$var_key] = $var_val;
					$_REQUEST[$var_key] = $var_val;
				}
			}
		}
		$var_parts = explode("&",$gvars);
		for($n=0; $n<count($var_parts); $n++)
		{
			$var_part = explode("=",$var_parts[$n]);
			if(count($var_part) > 1)
			{
				$var_key = trim($var_part[0]);
				if(strlen($var_key) > 0 && substr($var_key,0,1)=="?")
					$var_key = substr($var_key,1);
				$var_val = trim($var_part[1]);
				if($var_key!="")
				{
					$_GET[$var_key] = $var_val;
					$_POST[$var_key] = $var_val;
					$_REQUEST[$var_key] = $var_val;
					//echo $var_key . " = " . $var_val . "<br>";
				}
			}
		}
		require_once("/home/poslavu/public_html/admin".com_path()."/".$fname);
		
		$output = ob_get_contents();
		ob_end_clean();
		
		$com_script_code = str_replace("php_self",$self,$com_script_code);
		$com_script_code = str_replace("php_zindex",$zindex,$com_script_code);
		
		if(strpos($output,"<head>")===false)
			$output = $com_script_code . "\n" . $output;
		else
			$output = str_replace("<head>","<head>\n" . $com_script_code,$output);
		
		$output = str_replace("/images/","-----images-----",$output);
		$output = str_replace("images/",com_path() . "/$com_dirname/images/",$output);
		$output = str_replace("-----images-----","/images/",$output);
		$output = str_replace("'".basename($fname)."?","'draw_com.php?drawmode=draw_component&usecc={$cc}&fname={$fname}&self={$self}&zindex={$zindex}&",$output);
		$output = str_replace("\"".basename($fname)."?","\"draw_com.php?drawmode=draw_component&usecc={$cc}&fname={$fname}&self={$self}&zindex={$zindex}&",$output);
		
		$output = str_replace("'race_scores.php?","'".com_path()."/$com_dirname/race_scores.php?",$output);
		$output = str_replace("\"race_scores.php?","\"".com_path()."/$com_dirname/race_scores.php?",$output);
		
		function parse_cmd($str,$leading,$type)
		{
			//$str_sep = strpos($str,$leading);
			$rc = $leading;
			if($type=="a")
			{
				$ac = "href=".$leading;
			}
			else
			{
				$ac = $leading;
			}
			$ldone = false;
			$in_quote = true;
			$lnk = "";
			$oplead = "";
			if($leading=="'") $oplead = '"';
			else if($leading=='"') $oplead = "'";
			for($n=0; $n<strlen($str); $n++)
			{
				$ch = $str[$n];
				if($ldone)
				{
					$rc .= $ch;
					$ac .= $ch;
				}
				if($ch==$leading)
					$in_quote = !$in_quote;
				if(!$ldone && !$in_quote && ($ch==";" || $ch==">" || $ch=="/" || ($ch=="\"" && $leading!="\"") || ($ch=="'" && $leading!="'")))
				{
					$rc .= "#".$leading.$ch;
					$ac .= "#".$leading;
					
					$inner_code = "";
					$l_parts = explode("_DO:",$lnk);
					for($lp=0; $lp<count($l_parts); $lp++)
					{
						if($lp > 0) $inner_code .= "; ";
						$inner_code .= "do_com_cmd(".$leading.$l_parts[$lp];
						if(count($l_parts) > $lp + 1)
							$inner_code .= $leading;
						$inner_code .= ")";
					}
					if($type=="a")
					{
						$ac .= " onclick=".$oplead.$inner_code.$oplead;
					}
					else
					{
						$ac .= "; ".$inner_code;
					}
					
					$ac .= $ch;
					$ldone = true;
				}
				if(!$ldone)
					$lnk .= $ch;
			}
			return array($ac,$rc,$lnk);
			//return $leading . "#" . $leading . substr($str,$str_sep + 1);
		}
		$output = preg_replace("/href\s?=\s?\'_DO:/", "href='_DO:", $output);
		$output = preg_replace("/href\s?=\s?\"_DO:/", "href=\"_DO:", $output);
		//$output = preg_replace("/location\s?=\s?\'_DO:/", "location='_DO:", $output);
		//$output = preg_replace("/location\s?=\s?\"_DO:/", "location=\"_DO:", $output);

		$output_parts = explode("href='_DO:",$output);
		$output = "";
		$output .= $output_parts[0];
		for($i=1; $i<count($output_parts); $i++)
		{
			$pc = parse_cmd($output_parts[$i],"'","a");
			$output .= $pc[0];
		}

		$output_parts = explode('href="_DO:',$output);
		$output = "";
		$output .= $output_parts[0];
		for($i=1; $i<count($output_parts); $i++)
		{
			$pc = parse_cmd($output_parts[$i],'"',"a");
			$output .= $pc[0];
		}
		
		$output_parts = explode("'_DO:",$output);
		$output = "";
		$output .= $output_parts[0];
		for($i=1; $i<count($output_parts); $i++)
		{
			$pc = parse_cmd($output_parts[$i],"'","js");
			$output .= $pc[0];
		}

		$output_parts = explode('"_DO:',$output);
		$output = "";
		$output .= $output_parts[0];
		for($i=1; $i<count($output_parts); $i++)
		{
			$pc = parse_cmd($output_parts[$i],'"',"js");
			$output .= $pc[0];
		}
		$output = preg_replace("/window\.location\s*=\s*('#'|\"#\")/", "", $output);
		$output = preg_replace("/location\s*=\s*('#'|\"#\")/", "", $output);
		$output = preg_replace("/href\s*=\s*('#'|\"#\")/", "", $output);

		//echo htmlspecialchars( $output );
		//exit();

		/*$output = str_replace("window.location = '#';","",$output);
		$output = str_replace('window.location = "#";',"",$output);
		$output = str_replace("location = '#';","",$output);
		$output = str_replace('location = "#";',"",$output);
		$output = str_replace("href='#'","",$output);
		$output = str_replace('href="#"',"",$output);*/
		echo $output;	
	}
?>