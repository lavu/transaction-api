<?php
	
	if(!function_exists("getvar"))
	{
		function getvar($var,$def="")
		{
			return (isset($_GET[$var]))?$_GET[$var]:$def;
		}
	}
	if(!function_exists("postvar"))
	{
		function postvar($var,$def="")
		{
			return (isset($_POST[$var]))?$_POST[$var]:$def;
		}
	}
	
	function get_addon_info($package_read)
	{
		$addon_info = $package_read['order_addons'];
		if(trim($addon_info)=="") $addon_info = "19|o|Customer|o|1|o|1"; // editing fmods doesn't work unless an addon is defined
		return $addon_info;
	}
	
	function com_navbar($title,$extra_code="&nbsp;")
	{
		global $page_path;
		$str = "";
		$str .= "<br>";
		$str .= "<table style='border: solid 1px #777777' bgcolor='#eeeeee' cellpadding=4>";
		$str .= "<tr>";
		$str .= "<td width='140'><input type='button' value='<< Back' style='font-size:10px' onclick='window.location = \"$page_path\"'></td>";
		$str .= "<td width='280' align='center'>$title</td>";
		$str .= "<td width='140' align='right'>$extra_code</td>";
		$str .= "</tr>";
		$str .= "</table>";
		$str .= "<br>";
		return $str;
	}
	
	function com_options($com_read,$page_path)
	{
		global $page_path;
		$com_fields = array();
		$com_fields[] = array("Development Account Dataname","component_packages","dev_account");
		$com_fields[] = array("Development Account Comcode","component_packages","dev_comcode");
		$com_fields[] = array("Development Account Location","component_packages","dev_locid");
		
		$fstr = "";
		$update_code = "";
		
		$fstr .= "<form name='update_com_package' method='post' action='$page_path'>";
		$fstr .= "<input type='hidden' name='posted'>";
		$fstr .= "<input type='hidden' name='id' value='".$com_read['id']."'>";
		$fstr .= "<table>";
		for($i=0; $i<count($com_fields); $i++)
		{
			$field_title = $com_fields[$i][0];
			$field_table = $com_fields[$i][1];
			$field_col = $com_fields[$i][2];
			$field_value = (isset($com_read[$field_col]))?$com_read[$field_col]:"";
			
			$field_escaped = ConnectionHub::getConn('poslavu')->escapeString($field_col);
			if($update_code!="") $update_code .= ",";
			$update_code .= "`$field_escaped`='[$field_escaped]'";
			$fstr .= "<tr><td align='right'>$field_title</td><td><input type='text' name='$field_col' value=\"".str_replace("\"","&quot;",$field_value)."\"></td></tr>";
		}
		$fstr .= "<tr><td align='right'>&nbsp;</td><td><input type='button' value='Submit' onclick='document.update_com_package.submit()'></td></tr>";
		$fstr .= "</table>";
		$fstr .= "</form>";
		
		if(isset($_POST['posted']) && isset($com_read['id']) && $com_read['id']==$_POST['id'])
		{
			update_com_options($update_code, $page_path);
		}
		
		echo $fstr;
	}
	
	function get_custom_modifier_layout($package_read,$fmod_id)
	{
		$layout_read = array();
		
		$rdb = "poslavu_" . $package_read['dev_account'] . "_db";
		$fmod_query = mlavu_query("select * from `[1]`.`forced_modifiers` where `id`='[2]'",$rdb,$fmod_id);
		if(mysqli_num_rows($fmod_query))
		{
			$fmod_read = mysqli_fetch_assoc($fmod_query);
			$set_c_ids = "";
			if($fmod_read['extra']!="") $set_c_ids .= $fmod_read['extra3'] . "/" . $fmod_read['extra'] . ".php";
			if($fmod_read['extra2']!="")
			{
				if($set_c_ids!="") $set_c_ids .= ",";
				$set_c_ids .= $fmod_read['extra3'] . "/" . $fmod_read['extra2'] . ".php";
			}
			
			$layout_read['c_ids'] = $set_c_ids;
			$layout_read['c_xs'] = "275,300";
			$layout_read['c_ys'] = "70,125";
			$layout_read['c_ws'] = "474,426";
			$layout_read['c_hs'] = "598,485";
			$layout_read['c_zs'] = "1,2";
			if($fmod_read['extra5']!="")
			{
				$extra5_parts = explode("|",$fmod_read['extra5']);
				if(count($extra5_parts) > 1)
				{
					$l1_parts = explode("x",$extra5_parts[0]);
					$l2_parts = explode("x",$extra5_parts[1]);
					if(count($l1_parts) > 3 && count($l2_parts) > 3)
					{
						$layout_read['c_xs'] = trim($l1_parts[0]) . "," . trim($l2_parts[0]);
						$layout_read['c_ys'] = trim($l1_parts[1]) . "," . trim($l2_parts[1]);
						$layout_read['c_ws'] = trim($l1_parts[2]) . "," . trim($l2_parts[2]);
						$layout_read['c_hs'] = trim($l1_parts[3]) . "," . trim($l2_parts[3]);
					}
				}
			}
		}
		
		return $layout_read;
	}

?>
