<?php
	function view_coms($package_read,$page_path,$set_view_mode,$fmod_id=0,$a_draw_parts=NULL)
	{
		
		global $view_mode;
		$view_mode = $set_view_mode;
		
		if(is_array($package_read))
		{
			$package_found = true;
		}
		else if(is_numeric($package_read))
		{
			$package_found = false;
			$package_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_packages` where `id`='[1]' and `disabled`!='1'",$com_id);
			if(mysqli_num_rows($package_query))
			{
				// id, title, layout_ids, parent, package_code, order_addons, notes, disabled
				$package_read = mysqli_fetch_assoc($package_query);
				$package_found = true;
			}
		}
		
		$addon_info = explode("|o|",get_addon_info($package_read));
		if(count($addon_info) > 1)
		{
			$addon_id = trim($addon_info[0]);
			$addon_title = trim($addon_info[1]);
		}
		else
		{
			$addon_id = 0;
			$addon_title = "";
		}
		
		$pos_component_properties = array("title"=>"POS","id"=>$addon_id,"background"=>"../../sa_cp/tools/edit_coms/images/pos_background.png");
		if($package_found)
		{
			$tablist = array();
			$layout_ids = explode(",",$package_read['layout_ids']);
			for($l=0; $l<count($layout_ids); $l++)
			{
				$layout_id = $layout_ids[$l];
				
				if($layout_id==0)
				{
					$tablist[] = $pos_component_properties;
				}
				else
				{
					$layout_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_layouts` where `id`='[1]'",$layout_id);
					if(mysqli_num_rows($layout_query))
					{
						$layout_read = mysqli_fetch_assoc($layout_query);
						$tablist[] = $layout_read;
					}
				}
			}
			
			$ctab = getvar("ctab","-first-");
			$z_index_setup = "z_index_list = new Array(); \n";
			
			if (!isset($coms)) $coms = NULL;
			if (!isset($layout_read)) $layout_read = NULL;
			if (!isset($background_image)) $background_image = NULL;
			
			echo "<script language='javascript' src='/sa_cp/tools/edit_coms/draw_common.js'></script>";
			echo "<table cellspacing=0 cellpadding=0 width='1024'>";
			ob_start();
			view_coms_tabs($tablist, $ctab, $page_path, $pos_component_properties, $addon_id, $fmod_id, $package_read, $coms, $layout_read, $background_image);
			$s_tab_contents = ob_get_contents();
			ob_end_clean();
			$s_banner_contents = "<tr><td height='88' style='background: URL(http://admin.poslavu.com".com_path()."/lavukart/images/bgtop.jpg); background-repeat:repeat-x; background-position: bottom middle'>&nbsp;</td></tr>";
			ob_start();
			view_coms_body($coms, $background_image, $package_read, $z_index_setup);
			$s_body_contents = ob_get_contents();
			ob_end_clean();
			if ($a_draw_parts === NULL || !isset($a_draw_parts['tabs']) || $a_draw_parts['tabs'] == TRUE)
				echo $s_tab_contents;
			if ($a_draw_parts === NULL || !isset($a_draw_parts['banner']) || $a_draw_parts['banner'] == TRUE)
				echo $s_banner_contents;
			if ($a_draw_parts === NULL || !isset($a_draw_parts['body']) || $a_draw_parts['body'] == TRUE)
				echo $s_body_contents;
		}
	}
	
	function view_coms_body(&$coms, &$background_image, &$package_read, &$z_index_setup) {
		echo "<tr>";
		echo "<td style='width:1024px; height:620px' background='http://admin.poslavu.com".com_path()."/$background_image' align='left' valign='top'>";
		
		echo "<div style='position:relative' id='stage' name='stage'>";
		
		for($i=0; $i<count($coms); $i++)
		{
			$id = $coms[$i]['id'];
			$left = $coms[$i]['x'] . "px";
			$top = $coms[$i]['y'] . "px";
			$width = $coms[$i]['w'] . "px";
			$height = $coms[$i]['h'] . "px";
			$zindex = ($coms[$i]['z'] * 1000) + 1;
			$type = $coms[$i]['type'];
			
			echo "<div style='position:absolute; left:$left; top:$top; z-index:$zindex' id='com_container_$id'>";
			
			$com_read = array();
			if(strpos($id,".php"))
			{
				$id_parts = explode(".php",$id);
				$id_parts = explode("/",$id_parts[0]);
				$id_name = $id_parts[count($id_parts) - 1];
				$id_filename = $id;
				
				$com_read = array();
				$com_read['name'] = $id_name;
				$com_read['filename'] = $id_filename;
				if(strpos(strtolower($id_name),"wrapper"))
					$com_read['allow_scroll'] = "0";
				else
					$com_read['allow_scroll'] = "1";
				$com_read['title'] = ucfirst(str_replace("_"," ",$id_name));
			}
			else
			{
				$com_query = mlavu_query("select * from `poslavu_MAIN_db`.`components` where `id`='[1]'",$id);
				if(mysqli_num_rows($com_query))
				{
					$com_read = mysqli_fetch_assoc($com_query);
				}
			}
			
			if(isset($com_read['name']))
			{
				$com_name = $com_read['name'];
				$com_filename = $com_read['filename'];
				$allow_scroll = $com_read['allow_scroll'];
				$com_type = $com_read['type'];
				$com_title = $com_read['title'];
				
				if($allow_scroll=="1") $scroll_mode = "auto"; else $scroll_mode = "no";
				
				$comname = "com_".$com_name;
				$z_index_setup .= "z_index_list['".$com_name."'] = $zindex; \n";
				$vars = "fname=$com_filename&zindex=$zindex&self=$com_name";
				$usecc = $package_read['dev_account'] . "_key_" . $package_read['dev_comcode'];
				$loc_id = $package_read['dev_locid'];
				echo "<iframe allowtransparency='true' frameborder='0' scrolling='{$scroll_mode}' style='width:{$width}; height:{$height}' name='{$comname}' id='$comname' src='/sa_cp/tools/edit_coms/draw_com.php?drawmode=draw_component&usecc={$usecc}&uselocid={$loc_id}&tab_name={$comname}&{$vars}'></iframe>";
			}
			echo "</div>";
		}
		echo "</div>";
		
		echo "</div>";
		echo "</td>";
		echo "</tr>";
		echo "</table>";
		echo "<script language='javascript'>";
		echo $z_index_setup;
		echo "</script>";
	}
	
	function view_coms_tabs(&$tablist, &$ctab, &$page_path, &$pos_component_properties, &$addon_id, &$fmod_id, &$package_read, &$coms, &$layout_read, &$background_image) {
		echo "<tr><td height='50' valign='bottom' align='left'>";
		
		echo "<table cellspacing=0 cellpadding=0>";
		for($t=0; $t<count($tablist); $t++)
		{
			if(!is_numeric($ctab) && ($ctab=="-first-" || $ctab=="")) $ctab = $tablist[$t]['id'];
			make_tab(ucfirst($tablist[$t]['title']),$tablist[$t]['id'],$ctab,$page_path);
		}
		echo "</table>";
		
		$coms = array();
		$layout_read = false;
		
		if($ctab==0)
		{
			$layout_read = $pos_component_properties;
		}
		else
		{
			$layout_query = mlavu_query("select * from `poslavu_MAIN_db`.`component_layouts` where `id`='[1]'",$ctab);
			if(mysqli_num_rows($layout_query))
			{
				$layout_read = mysqli_fetch_assoc($layout_query);
			}
			
			if($ctab==$addon_id)
			{
				$layout_read['background'] = $pos_component_properties['background'];
				if($fmod_id!=0)
				{
					$fmod_read = get_custom_modifier_layout($package_read,$fmod_id);
					foreach($fmod_read as $fkey => $fval)
					{
						$layout_read[$fkey] = $fval;
					}
				}
			}
		}
		
		if($layout_read)
		{		
			$background_image = $layout_read['background'];
			$c_ids = explode(",",$layout_read['c_ids']);
			$c_xs = explode(",",$layout_read['c_xs']);
			$c_ys = explode(",",$layout_read['c_ys']);
			$c_ws = explode(",",$layout_read['c_ws']);
			$c_hs = explode(",",$layout_read['c_hs']);
			$c_zs = explode(",",$layout_read['c_zs']);
			$c_types = explode(",",$layout_read['c_types']);
			
			for($i=0; $i<count($c_ids); $i++)
			{
				$placement = 0;
				for($n=0; $n<count($coms); $n++)
				{
					if($c_zs[$i] > $c_zs[$n])
						$placement = $n + 1; 
				}
				for($n=count($coms); $n>$placement; $n--)
				{
					$coms[$n] = $coms[$n-1];
				}
				$insert_com = array();
				$insert_com['id'] = $c_ids[$i];
				$insert_com['x'] = $c_xs[$i];
				$insert_com['y'] = $c_ys[$i];
				$insert_com['w'] = $c_ws[$i];
				$insert_com['h'] = $c_hs[$i];
				$insert_com['z'] = $c_zs[$i];
				$insert_com['type'] = $c_types[$i];
				
				$coms[$placement] = $insert_com;
			}
		}
					
		echo "</td></tr>";
	}
	
	function com_path()
	{
		global $view_mode;
		if($view_mode=="live")
			return "/components";
		else
			return "/dev/components";
	}

	function make_tab($title,$tnum,$selected,$page_path)
	{
		$tabfile = "tab1.png";
		if($selected==$tnum)
			$tabfile = "tab2.png";
		echo "<td width='195' height='45' style='cursor:pointer; color:#ffffff; font-weight:bolde; font-family:Arial; font-size:16px; background: URL(http://admin.poslavu.com".com_path()."/lavukart/images/$tabfile); background-position:bottom middle; background-repeat:no-repeat' align='center' valign='middle' onclick='window.location = \"$page_path&ctab=".$tnum."\"'>".$title."</td>";
	}
?>
