<?php
	require_once(dirname(__FILE__) . "/../../cp/resources/lavuquery.php");
	
	echo "<table>";
	$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `cost`>0 and `resellername`!='martin' order by `purchased` asc");
	while($lic_read = mysqli_fetch_assoc($lic_query))
	{
		$purchased = $lic_read['purchased'];
		$cost = $lic_read['cost'];
		$reseller = $lic_read['resellername'];
		
		echo "<tr>";
		echo "<td>$reseller</td>";
		echo "<td>$purchased</td>";
		echo "<td>$cost</td>";
		echo "<td>";
		
		$purchased_parts = explode(" ",$purchased);
		if(count($purchased_parts) > 1)
		{
			$date_parts = explode("-",$purchased_parts[0]);
			$time_parts = explode(":",$purchased_parts[1]);
			$year = $date_parts[0];
			$month = $date_parts[1];
			$day = $date_parts[2];
			$hour = $time_parts[0];
			$min = $time_parts[1];
			$sec = $time_parts[2];
		
			$min_datetime = date("Y-m-d H:i:s",mktime($hour,$min - 1,$sec,$month,$day,$year));
			$max_datetime = date("Y-m-d H:i:s",mktime($hour,$min + 5,$sec,$month,$day,$year));
			$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `datetime`>='[1]' and `datetime`<'[2]' and `x_response_code`='1' and (`x_amount` * 1) LIKE ('[3]' * 1)",$min_datetime,$max_datetime,$cost);
			if(mysqli_num_rows($pay_query))
			{
				echo "Found";
			}
			else echo "x";
		}
		
		echo "</td>";
		echo "</tr>";
	}
	echo "</table>";
?>
