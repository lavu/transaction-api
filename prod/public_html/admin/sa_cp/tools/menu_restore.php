<form method='POST' action=''>
<label for='dataname_input'>Dataname:</label>
<input type='text' id='dataname_input' name='dataname_input_name'/>
</form>


<?php
	
	ini_set('display_errors',1);
	$DUMP_DIRECTORY_PATH = dirname(__FILE__).'/../dbbackup/';

	//echo "POST:<pre>".print_r($_POST,1)."</pre>";
	//if(!can_access("customers")){
	//	return;
	//}

/*
	if(empty($_POST['dataname_input_name'])){
		return;
	}
*/


	$allSQLDumpPathsForDataname = getAllPathsForDataname();
	$pathsSortedByTime = getTableDatanameDumpsSortedByTime($allSQLDumpPathsForDataname);
	if( empty($_POST['perform_restore']) || $_POST['perform_restore'] != 'go'){
		createRestoreDisplay($pathsSortedByTime);
	}else{
		performMenuRestoration($pathsSortedByTime);
	}
	
	

	function getAllPathsForDataname(){
		global $DUMP_DIRECTORY_PATH;
		$filesInDumpDirectoryArr = scandir($DUMP_DIRECTORY_PATH);
		$sqlDumpFilesForDataname = array();
		foreach($filesInDumpDirectoryArr as $currPath){
			$basename = basename($currPath);
			if(strpos($basename.'.', $_POST['dataname_input_name']) === 0){
				$sqlDumpFilesForDataname[] = $currPath;
			}
		}
		return $sqlDumpFilesForDataname;
	}

	function getTableDatanameDumpsSortedByTime($pathsArr){
		$tablesDumpsSortedByTime = array();
		foreach($pathsArr as $currPath){
			$basename = basename($currPath);
			$nameParts = explode('.',$basename);
			$dataName = $nameParts[0];
			$table = $nameParts[1];
			$time = $nameParts[2];
			if(!is_array($tablesDumpsSortedByTime[$time])){ $tablesDumpsSortedByTime[$time] = array(); }
			$tablesDumpsSortedByTime[$time][$table] = $currPath;
		}
		return $tablesDumpsSortedByTime;
	}

	function createRestoreDisplay($pathsSortedByTime){
		$times = array_keys($pathsSortedByTime);
		$datesParalledToTimes = array();
		foreach($times as $currUnixTime){
			$datesParalledToTimes[] = date('Y-m-d H:i:s', $currUnixTime);
		}
		echo "<form action='' method='POST'>";
		echo "<input type='hidden' name='dataname' value='".$_POST['dataname_input_name']."'/>";
		echo "<input type='hidden' name='perform_restore' value='go'/>";
		echo "<select id='time_for_restore_select' name='date_select_name'>";
		for($i = 0; $i < count($datesParalledToTimes); $i++){
			echo "<option value='".$times[$i]."'>".$datesParalledToTimes[$i]."</option>";
		}
		echo "</select>";
		echo "<input type='submit'/>";
		echo "</form>";
	}

	function performMenuRestoration(){
		require_once(dirname(__FILE__).'/../../../../private_html/myinfo.php');
		$usr = my_poslavu_username();
		$pw = my_poslavu_password();
		$hst = my_poslavu_host();
		$dn = $_POST['dataname'];
		global $DUMP_DIRECTORY_PATH;
		$menuTableList = array("forced_modifiers",
			"forced_modifier_groups",
			"forced_modifier_lists",
			"ingredients",
			"ingredient_categories",
			"menus",
			"menu_categories",
			"menu_groups",
			"menu_items",
			"modifiers",
			"modifier_categories",
			"super_groups");
		$sqlDumpPathsArr = array();
		foreach($menuTableList as $currTable){
			$sqlDumpPathsArr[] = $DUMP_DIRECTORY_PATH.$_POST['dataname'].'.'.$currTable.'.'.$_POST['date_select_name'].'.sql';
		}

		foreach($sqlDumpPathsArr as $currSQLDumpPath){
			$shellLine = "mysql -h $hst -u $usr -p$pw poslavu_{$dn}_db < $currSQLDumpPath";
			echo "<br>Loading mysql from dump:$currSQLDumpPath output:".shell_exec($shellLine);
		}
	}

?>