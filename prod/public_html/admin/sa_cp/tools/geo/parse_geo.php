<?php
	ini_set("memory_limit","600M");
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	
	function convert_ip_address_to_ip_number($ip)
	{
		$ip_parts = explode(".",$ip);
		if(count($ip_parts)==4)
		{
			$num1 = (256 * 256 * 256 * $ip_parts[0]);
			$num2 = (256 * 256 * $ip_parts[1]);
			$num3 = (256 * $ip_parts[2]);
			$num4 = (1 * $ip_parts[3]);
			
			return $num1 + $num2 + $num3 + $num4;
		}
		else return -1;
	}
	
	function get_ip_address_country($ip)
	{
		$ip_num = convert_ip_address_to_ip_number($ip);
		
		if(is_numeric($ip_num) && $ip_num >= 0)
		{
			$block_query = mlavu_query("select * from `poslavu_MAIN_db`.`ap_blocks` where `begin_ip_num`<='[1]' and `end_ip_num`>='[1]'",$ip_num);
			if(mysqli_num_rows($block_query))
			{
				$block_read = mysqli_fetch_assoc($block_query);
				return array("code"=>$block_read['country_code'],"name"=>$block_read['country_name'],"range"=>$block_read['begin_ip'] . "-".$block_read['end_ip']);
			}
			else return array("code"=>"NA","name"=>"Not Found");
		}
		return array("code"=>"","name"=>"");
	}
	
	ini_set("display_errors","1");
	
	$mode = (isset($_GET['mode']))?$_GET['mode']:"";
	
	if($mode=="import_from_source")
	{
		import_all_ip_blocks_from_source_file();
	}
	else
	{
		//$cinfo = get_ip_address_country($_SERVER['REMOTE_ADDR']);
		//echo "You are in " . $cinfo['name'] . " (".$cinfo['range'].")";
		$last_connect = "2013-12-01 00:00:00";
		
		$country_list = array();
		$not_found_list = array();
		$recent_ip_query = mlavu_query("select distinct(`ipaddress`) as `ip`,`dataname` from `poslavu_MAIN_db`.`ap_connects` where `last_connect` > '[1]'",$last_connect);
		while($recent_ip_read = mysqli_fetch_assoc($recent_ip_query))
		{
			$ip = $recent_ip_read['ip'];
			$ip_country = get_ip_address_country($ip);
			$ip_dataname = $recent_ip_read['dataname'];
			
			if(isset($country_list[$ip_country['code']]))
			{
				$country_list[$ip_country['code']]['count']++;
				$country_list[$ip_country['code']]['iplist'][] = $ip;
				$country_list[$ip_country['code']]['dataname_list'][$ip_dataname] = 1;
			}
			else
			{
				$country_list[$ip_country['code']] = array("name"=>$ip_country['name'],"count"=>1,"iplist"=>array($ip),"dataname_list"=>array($ip_dataname=>1));
			}
		}
		
		foreach($country_list as $key => $val)
		{
			$country_list[$key]['count'] = count($country_list[$key]['dataname_list']);
		}
		
		$ordered_country_list = array();
		foreach($country_list as $key => $val)
		{
			$new_country_list = array();
			$added = false;
			for($n=0; $n<count($ordered_country_list); $n++)
			{
				if($val['count'] * 1 >= $ordered_country_list[$n]['count'] * 1 && !$added)
				{
					$added = true;
					$new_country_list[] = $val;
				}
				$new_country_list[] = $ordered_country_list[$n];
			}
			if(!$added)
			{
				$new_country_list[] = $val;
			}
			$ordered_country_list = $new_country_list;
		}
		
		for($n=0; $n<count($ordered_country_list); $n++)
		{
			$val = $ordered_country_list[$n];
			echo $val['name'] . " - " . $val['count'] . "<br>";
			/*for($i=0; $i<count($val['iplist']); $i++)
			{
				$ip = $val['iplist'][$i];
				$ip_num = convert_ip_address_to_ip_number($ip);
				$ip_info = get_ip_address_country($ip);
				$ip_country = $ip_info['name'];
				echo "---------------- " . $ip . " ($ip_num)<br>";
			}*/
		}
	}
	
	function import_all_ip_blocks_from_source_file()
	{
		$filename = dirname(__FILE__) . "/GeoIP-108.csv";
		$fp = fopen($filename,"r");
		$gstr = fread($fp,filesize($filename));
		fclose($gstr);
		
		$gstr = explode("\n",$gstr);
		
		echo "<table>";
		for($i=0; $i<count($gstr); $i++)
		{
			echo "<tr>";
			$gline = explode(",",str_replace("\"","",$gstr[$i]));
			if(count($gline)>=5)
			{
				$begin_ip = trim($gline[0]);
				$end_ip = trim($gline[1]);
				$begin_ip_num = trim($gline[2]);
				$end_ip_num = trim($gline[3]);
				$country_code = trim($gline[4]);
				$country_name = trim($gline[5]);
				
				if($begin_ip_num!="" && $end_ip_num!="" && is_numeric($begin_ip_num) && is_numeric($end_ip_num))
				{
					echo "<td>" . $country_name . "</td><td>" . $begin_ip . "</td><td>" . $end_ip . "</td>";
					//echo "<td>" . convert_ip_address_to_ip_number($begin_ip) . "/" . $begin_ip_num . "</td>";
					//echo "<td>" . convert_ip_address_to_ip_number($end_ip) . "/" . $end_ip_num . "</td>";
					echo "<td>";
					$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`ap_blocks` where `begin_ip_num`='[1]' and `end_ip_num`='[2]'",$begin_ip_num,$end_ip_num);
					if(mysqli_num_rows($exist_query))
					{
						echo "<font style='color:#000088'>Already exists</font>";
					}
					else
					{
						$success = mlavu_query("insert into `poslavu_MAIN_db`.`ap_blocks` (`begin_ip`,`end_ip`,`begin_ip_num`,`end_ip_num`,`country_code`,`country_name`) values ('[1]','[2]','[3]','[4]','[5]','[6]')",$begin_ip,$end_ip,$begin_ip_num,$end_ip_num,$country_code,$country_name);
						if($success)
							echo "<font style='color:#008800'>Insert Success</font>";
						else
							echo "<font style='color:#880000'>Failed to Insert</font>";
					}
					echo "</td>";
				}
			}
			echo "</tr>";
		}
		echo "</table>";
	}
?>
