<?php
	ini_set('display_errors', 'On');
	ini_set('memory_limit', '128M');
	
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once('/home/poslavu/public_html/admin/sa_cp/show_report.php');
	
	function errorOut($msg)
	{
		echo "No Results Found: " . $msg . "<br />";
		exit();
	}
	
	function error($msg)
	{
		echo "No Results Found: " . $msg . "<br />";
	}
	
	function debugPrint($var)
	{
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}
	
	function checkDuplicates($order_id)
	{
		$modifiersList = Array();
		{
			$modifiersUsedQuery = "SELECT * FROM `modifiers_used` WHERE `order_id`='" . $order_id . "'";
			$modifiersUsedResult = lavu_query($modifiersUsedQuery);
			
			if(!mysqli_num_rows($modifiersUsedResult))
			{
				error("No Modifiers Found for order_id: " . $order_id);
				return;
			}
			
			while($modifier = mysqli_fetch_assoc($modifiersUsedResult))
				$modifiersList[] = $modifier;
		}	
		
		$duplicatemodifierIDs = Array();
		{
			$fieldList = Array(
								'loc_id',
								'order_id',
								'mod_id',
								'qty',
								'type',
								'row',
								'cost',
								'unit_cost');
								
			for($i = 0; $i < count($modifiersList); $i++)
			{
				for($j = $i + 1; $j < count($modifiersList); $j++)
				{
					$var = true;
					foreach($fieldList as $k => $field)
						$var = $var && $modifiersList[$i][$field] == $modifiersList[$j][$field];
						
					if($var)
						$duplicatemodifierIDs[$modifiersList[$j]['id']] = $modifiersList[$j]['id'];
				}
			}
		}
		{
			global $orderidArray;
			global $duplicateNumArray;
			global $nonduplicateNumArray;
			$idFilters = "";
			
			foreach($duplicatemodifierIDs as $key => $id)
			{
				if($idFilters != "")
					$idFilters = $idFilters . ", ";
				
				$idFilters = $idFilters . "'$id'";
			}
			
			if(count($duplicatemodifierIDs))
			{
				//debugPrint($duplicatemodifierIDs);
				
				$deleteQuery = "DELETE FROM `modifiers_used` WHERE `id` IN (" . $idFilters . ")";
				echo $deleteQuery;
				exit();
				lavu_query($deleteQuery);
				//echo $deleteQuery . "<br />";
			}
			else
			{
				//echo $order_id . "| No Duplicates!";
			}
			
			$orderidArray[] = $order_id;
			$duplicateNumArray[] = count($duplicatemodifierIDs);
			$nonduplicateNumArray[] = count($modifiersList) - count($duplicatemodifierIDs);
		}
		
	}
	
	
	if(!isset($_REQUEST['dataname']))
		errorOut("Please Specify a 'dataname' in GET or POST");
		$dataname = $_REQUEST['dataname'];
	$orderidArray = Array();
	$duplicateNumArray = Array();
	$nonduplicateNumArray = Array();
	
	lavu_connect_dn($dataname, 'poslavu_' . $dataname . '_db');
	
	$orderids = Array();
	{
		$ordersQuery = "SELECT `order_id` FROM `orders`";
		$ordersResults = lavu_query($ordersQuery);
		if(!mysqli_num_rows($ordersResults))
			errorOut("Found No Results for Order ID Query");
			
		
		while ($order = mysqli_fetch_assoc($ordersResults))
			$orderids[] = $order['order_id'];
	}
	
	for($i = 0; $i < count($orderids); $i++)
	{
		checkDuplicates($orderids[$i]);
	}
		
	lavu_close_db();
	{
		echo "<table><thead><tr><th>order_id</th><th>duplicates</th><th>non-duplicates</th></tr></thead><tbody>";
		$a = 0;
		$b = 0;
		for($i = 0; $i < count($orderidArray); $i++)
		{
			$a += $duplicateNumArray[$i];
			$b += $nonduplicateNumArray[$i];
			echo "<tr>";
				echo "<td>" . $orderidArray[$i] . "</td>";
				echo "<td>" . $duplicateNumArray[$i] . "</td>";
				echo "<td>" . $nonduplicateNumArray[$i] . "</td>";
			echo "</tr>";
		}
		
		echo "<tr>";
				echo "<td></td>";
				echo "<td><b>" . $a . "</b></td>";
				echo "<td><b>" . $b . "</b></td>";
			echo "</tr>";
		echo "</tbody><table>";
		
		echo "<br />";
		echo "Delted $a Entries";
	}
	
	
?>