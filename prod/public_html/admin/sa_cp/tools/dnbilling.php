<?php
	require_once(dirname(__FILE__) . "/../billing/payment_profile_functions.php");
	require_once(dirname(__FILE__) . "/../billing/new_process_payments.php");
	require_once('/home/poslavu/public_html/admin/distro/reseller_licenses_functions.php');
	require_once(dirname(__FILE__) . "/../billing/package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();

	function run_billing_for_dataname($dataname)
	{
		global $o_package_container;

		$details = "";
		$str = "";
		$processed_count;
		$total_count;
		$min_datetime = "2008-01-01 00:00:00";
		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
		while($rest_read = mysqli_fetch_assoc($rest_query))
		{
			$total_count++;

			$p_area = "";
			$dataname = $rest_read['data_name'];
			$restid = $rest_read['id'];
			$created_datetime = $rest_read['created'];
			$created_month = "";
			$dt_parts = explode(" ",$created_datetime);
			$dt_parts = explode("-",$dt_parts[0]);
			if(count($dt_parts) > 2)
			{
				$created_month = date("M d Y",mktime(0,0,0,$dt_parts[1],$dt_parts[2],$dt_parts[0]));
			}
			else $created_month = $created_datetime;

			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]' or `restaurantid`='[2]'",$dataname,$restid);
			if(mysqli_num_rows($signup_query))
			{
				$signup_read = mysqli_fetch_assoc($signup_query);

				$signupid = $signup_read['id'];
				$package = $signup_read['package'];
				$signup_status = $signup_read['status'];
				$trial_end = "";
				$license_applied = "";

				$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$restid);
				if(mysqli_num_rows($status_query))
				{
					$status_read = mysqli_fetch_assoc($status_query);
					$license_applied = $status_read['license_applied'];
					if($status_read['trial_end']!="" && $status_read['license_applied']=="")
					{
						$trial_end = $status_read['trial_end'];
					}

					if($status_read['license_type']!="" && $signup_read['package']=="")
					{
						$set_package = $o_package_container->get_level_by_attribute('name', $status_read['license_type']);
						$a_base_packages = array_merge(
							$o_package_container->get_levels_of_similar_licenses(1),
							$o_package_container->get_levels_of_similar_licenses(2),
							$o_package_container->get_levels_of_similar_licenses(3)
						);
						if (!in_array($set_package, $a_base_packages))
							error_log('package '.$status_read['license_type'].' not recognized for restaurant '.$dataname.' in '.__FILE__);
						$set_package = RESELLER_LICENSES::choose_package_from_payment_status_read($set_package, $status_read);

						mlavu_query("update `poslavu_MAIN_db`.`signups` set `package`='[4]' where `dataname`='[1]' and `restaurantid`='[2]' and `id`='[3]' and `package`='' limit 1",$dataname,$restid,$signupid,$set_package);
						$package = $set_package;
					}
				}

				if($package=="")
				{
					//echo $total_count . ") $created_month $dataname - no package".$nl;
					//echo $dataname . " Trial Ends: " . $trial_end . " ($license_applied)<br>";
				}
				else
				{
					$processed_count++;

					$details .= $dataname . " - " . $created_datetime . $nl;
					$pp_result = process_payments("process", $dataname, $package, $signup_status);
					$details .= "Due: " . $pp_result['all_bills_due'] . $nl;
					$details .= "Info: " . $pp_result['due_info'] . $nl;
					$details .= "------------------------------------------------" . $nl;
					$str .= ".";
					//echo $total_count . ") $created_month $dataname - $package".$nl;
					//echo $pp_result['due_info'] . $nl . $nl;
				}
			}
			else
			{
				//echo $total_count . ") $created_month $dataname - no signup record".$nl;
			}
		}
		//echo "Processed " . $processed_count . "/" . $total_count . $nl . $str . $nl . $nl;
		//echo $details . $nl;
	}
?>