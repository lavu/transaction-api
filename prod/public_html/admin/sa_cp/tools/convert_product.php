<?php


    $location_row;//Need this to communicate between two functions.
	function get_product_list()
	{
		$products = array();
		$products[] = array("name" => "Remove",
							"comcode" => "",
							"model" => "test_pizza_sho",
							"packageid" => 0,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "",
							"config" => "",
							"available" => false
						);

		$products[] = array("name" => "Customer Management",
							"comcode" => "customer",
							"model" => "test_pizza_sho",
							"packageid" => 15,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "med_customers, med_action_log, lk_waivers",
							"config" => "GetCustomerInfo, reg_app_bypass_sign_and_waivers, has_registrar_fields_been_initialized",
							"available" => true
						);

		$products[] = array("name" => "LavuKart",
							"comcode" => "lavukart",
							"model" => "p2r_dallas",
							"packageid" => 4,
							"packageid2" => 6,
							"packageid3" => 7,
							"tables" => "lk_%, lk_rooms:fill, lk_tracks:fill, lk_event_types:fill, lk_event_sequences:fill, lk_waivers:fill",
							"available" => false
						);

		$products[] = array("name" => "Pizza",
							"comcode" => "pizza",
							"model" => "default_pizza_",
							"packageid" => 13,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "med_customers, med_action_log, lk_waivers",
							"config" => "GetCustomerInfo, reg_app_bypass_sign_and_waivers, has_registrar_fields_been_initialized",

							"special_field1" => array("name"=>"forced_mod_list","table"=>"forced_modifier_lists","where"=>"`title`='Pizza Mod'"),
							"special_field2" => array("name"=>"forced_mod_item","table"=>"forced_modifiers","where"=>"`title`='Pizza Mods'","link_field"=>"list_id","link_to"=>"forced_mod_list"),
							"special_field3" => array("name"=>"pizza_fmod_1_list","table"=>"forced_modifier_lists","where"=>"`title`='PIZZA - Crust Type'","config_insert"=>array("setting"=>"pizza_mods","value"=>"Crust Type","value6"=>"9","match"=>array("id","value2"))),
							"special_field4" => array("name"=>"pizza_fmod_1","table"=>"forced_modifiers","where"=>"","link_field"=>"list_id","link_to"=>"pizza_fmod_1_list","image_field"=>"extra5","image_dir"=>"fmods"),
							"special_field5" => array("name"=>"pizza_fmod_2_list","table"=>"forced_modifier_lists","where"=>"`title`='PIZZA - Custom Toppings'","config_insert"=>array("setting"=>"pizza_mods","value"=>"Custom Toppings","value6"=>"10","match"=>array("id","value2"))),
							"special_field6" => array("name"=>"pizza_fmod_2","table"=>"forced_modifiers","where"=>"","link_field"=>"list_id","link_to"=>"pizza_fmod_2_list","image_field"=>"extra5","image_dir"=>"fmods"),
							"special_field7" => array("name"=>"pizza_fmod_3_list","table"=>"forced_modifier_lists","where"=>"`title`='PIZZA - Large Toppings'"),
							"special_field8" => array("name"=>"pizza_fmod_3","table"=>"forced_modifiers","where"=>"","link_field"=>"list_id","link_to"=>"pizza_fmod_3_list","image_field"=>"extra5","image_dir"=>"fmods"),
							"special_field9" => array("name"=>"pizza_fmod_4_list","table"=>"forced_modifier_lists","where"=>"`title`='PIZZA - Portion (Large)'"),
							"special_field10" => array("name"=>"pizza_fmod_4","table"=>"forced_modifiers","where"=>"","link_field"=>"list_id","link_to"=>"pizza_fmod_4_list","image_field"=>"extra5","image_dir"=>"fmods"),
							"special_field11" => array("name"=>"pizza_fmod_5_list","table"=>"forced_modifier_lists","where"=>"`title`='PIZZA - Portion (Med)'"),
							"special_field12" => array("name"=>"pizza_fmod_5","table"=>"forced_modifiers","where"=>"","link_field"=>"list_id","link_to"=>"pizza_fmod_5_list","image_field"=>"extra5","image_dir"=>"fmods"),
							"special_field13" => array("name"=>"pizza_fmod_6_list","table"=>"forced_modifier_lists","where"=>"`title`='PIZZA - Portion (Sm)'"),
							"special_field14" => array("name"=>"pizza_fmod_6","table"=>"forced_modifiers","where"=>"","link_field"=>"list_id","link_to"=>"pizza_fmod_6_list","image_field"=>"extra5","image_dir"=>"fmods"),
							"special_field15" => array("name"=>"pizza_fmod_7_list","table"=>"forced_modifier_lists","where"=>"`title`='PIZZA - Size'","config_insert"=>array("setting"=>"pizza_mods","value"=>"Size","value6"=>"8","match"=>array("id","value2"))),
							"special_field16" => array("name"=>"pizza_fmod_7","table"=>"forced_modifiers","where"=>"","link_field"=>"list_id","link_to"=>"pizza_fmod_7_list","image_field"=>"extra5","image_dir"=>"fmods"),

							"available" => true
						);

		$products[] = array("name" => "LaserTag",
							"comcode" => "ers",
							"model" => "ersdemo",
							"packageid" => 8,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "lk_waivers:fill, med_customers, med_action_log",
							"config" => "GetCustomerInfo, reg_app_bypass_sign_and_waivers",
							"special_field1" => array("name"=>"forced_mod_list","table"=>"forced_modifier_lists","where"=>"`title`='Customer Mod'"),
							"special_field2" => array("name"=>"forced_mod_item","table"=>"forced_modifiers","where"=>"`title`='get_customer'","link_field"=>"list_id","link_to"=>"forced_mod_list"),
							"available" => false
						);

		$products[] = array("name" => "Modern",
							"comcode" => "hotel",
							"model" => "modern_mission",
							"packageid" => 21,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "lk_waivers:fill, med_customers, med_action_log",
							"config" => "GetCustomerInfo, reg_app_bypass_sign_and_waivers",
							"special_field1" => array("name"=>"forced_mod_list","table"=>"forced_modifier_lists","where"=>"`title`='Customer Mod'"),
							"special_field2" => array("name"=>"forced_mod_item","table"=>"forced_modifiers","where"=>"`title`='get_customer'","link_field"=>"list_id","link_to"=>"forced_mod_list"),
							"available" => false
						);

		$products[] = array("name" => "Hotel",
							"comcode" => "hotel",
							"model" => "ersdemo",
							"packageid" => 3,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "lk_waivers:fill, med_customers, med_action_log",
							"config" => "GetCustomerInfo, reg_app_bypass_sign_and_waivers",
							"special_field1" => array("name"=>"forced_mod_list","table"=>"forced_modifier_lists","where"=>"`title`='Customer Mod'"),
							"special_field2" => array("name"=>"forced_mod_item","table"=>"forced_modifiers","where"=>"`title`='get_customer'","link_field"=>"list_id","link_to"=>"forced_mod_list"),
							"available" => false
						);

		$products[] = array("name" => "Medical",
							"comcode" => "medical",
							"model" => "devmed",
							"packageid" => 10,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "med_customers, med_action_log",
							"config" => "GetCustomerInfo, reg_app_bypass_sign_and_waivers",
							"available" => false
						);

		$products[] = array("name" => "Fitness",
							"comcode" => "fitness",
							"model" => "lavu_fit_demo",
							"packageid" => 20,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "med_customers, med_action_log, lk_waivers, med_billing, forced_modifier_groups:fill, forced_modifier_lists:fill, users",
							"config" => "GetCustomerInfo, reg_app_bypass_sign_and_waivers, has_registrar_fields_been_initialized",
							"special_field1" => array("name"=>"forced_mod_list","table"=>"forced_modifier_lists","where"=>"`title`='Customer Mod'"),
							"special_field2" => array("name"=>"forced_mod_item","table"=>"forced_modifiers","where"=>"`title`='get_customer'","link_field"=>"list_id","link_to"=>"forced_mod_list"),
							"available" => true
						);

		$products[] = array("name" => "Tithe",
							"comcode" => "tithe",
							"model" => "lavu_tithe",
							"packageid" => 0,
							"packageid2" => 22,
							"packageid3" => 0,
							"tables" => "med_customers, med_action_log, med_billing",
							"config" => "GetCustomerInfo, has_registrar_fields_been_initialized",
							"available" => true
						);

		$products[] = array("name" => "SE",
							"comcode" => "posse",
							"model" => "default_restau",
							"packageid" => 0,
							"packageid2" => 0,
							"packageid3" => 0,
							"tables" => "",  // TO DO : see if we need to copy kiosk-specific tables
							"config" => "",
							"available" => true
						);

		return $products;
	}

	/*Creating Table: lk_action_log Success
	Creating Table: lk_categories Success
	Creating Table: lk_checklist_catergories Success
	Creating Table: lk_customers Success
	Creating Table: lk_customers_temp Success
	Creating Table: lk_event_schedules Success
	Creating Table: lk_event_sequences Success
	Creating Table: lk_event_types Success
	Creating Table: lk_events Success
	Creating Table: lk_group_events Success
	Creating Table: lk_karts Success
	Creating Table: lk_laps Success
	Creating Table: lk_monitor Success
	Creating Table: lk_parts Success
	Creating Table: lk_parts_received Success
	Creating Table: lk_race_log Success
	Creating Table: lk_race_results Success
	Creating Table: lk_rooms Success
	Creating Table: lk_sales_reps Success
	Creating Table: lk_service Success
	Creating Table: lk_service_complete Success
	Creating Table: lk_service_info Success
	Creating Table: lk_service_types Success
	Creating Table: lk_source_list Success
	Creating Table: lk_tracks Success
	Creating Table: lk_waivers Success	*/
	function get_product_info($name)
	{
		$products = get_product_list();

		for($i=0; $i<count($products); $i++)
		{
			if(strtolower($products[$i]['name'])==strtolower($name))
			{
				return $products[$i];
			}
		}
		return false;
	}

	function copy_poslavu_image_file($filename_from,$filename_to)
	{
		$str = "";
		if(is_file($filename_from) && !is_file($filename_to))
		{
			if(!is_dir(dirname($filename_to)))
			{
				mkdir(dirname($filename_to),0755);
			}
			if(is_dir(dirname($filename_to)))
			{
				$str .= "copying to $filename_to<br>";
				copy($filename_from,$filename_to);
			}
		}
		return $str;
	}

	function escape_but_allow_percentage_sign($str)
	{
		$str = str_replace("%","percentagesign",$str);
		$str = ConnectionHub::getConn('poslavu')->escapeString($str);
		$str = str_replace("percentagesign","%",$str);
		return $str;
	}

	function display_query_success($success,$output_success="Success",$output_failed="Failed",$include_br=true)
	{
		$str = "";
		if($success) $str .= " <font style='color:#008800'>$output_success</font>";
		else
		{
			$str .= " <font style='color:#880000'>$output_failed</font>";
			$str .= "<br>" . mlavu_dberror() . "<br>";
		}
		if($include_br) $str .= "<br>";
		return $str;
	}

	function display_query_for_debug($query,$vars)
	{
		foreach($vars as $key => $val)
		{
			$query = str_replace("[".$key."]",ConnectionHub::getConn('poslavu')->escapeString($val),$query);
		}
		return $query;
	}

	function get_query_record_count($query,$var1="",$var2="",$var3="",$var4="")
	{
		$record_query = mlavu_query("select count(*) as `count` from ".$query,$var1,$var2,$var3,$var4);
		if(mysqli_num_rows($record_query))
		{
			$record_read = mysqli_fetch_assoc($record_query);
			$record_count = $record_read['count'];
		}
		else $record_count = 0;

		return $record_count;
	}

	function insert_all_records_into_table_from_table($source_db,$source_table,$dest_db,$dest_table,$where_code,$where_vars)
	{
	    global $location_row;
		if(is_array($where_code))
		{
			$set_code_from_old = $where_code[2];
			$where_code_old = $where_code[1];
			$where_code = $where_code[0];
			foreach($set_code_from_old as $skey => $sval)
				echo $skey . " change to " . $sval . "<br>";
			echo "where_code: " . $where_code . "<br>";
			echo "where code old: " . $where_code_old . "<br>";
		}
		else
		{
			$set_code_from_old = array();
			$where_code_old = $where_code;
		}
		if($where_code_old=="")
			$where_code_old = $where_code;

		$vars = $where_vars;
		$vars['source_db'] = $source_db;
		$vars['source_table'] = $source_table;
		$vars['dest_db'] = $dest_db;
		$vars['dest_table'] = $dest_table;

		$query = "delete from `[dest_db]`.`[dest_table]` where ".$where_code;
		$success = mlavu_query($query,$vars);
		echo display_query_success($success,"-Cleared-","-Failed to Clear-",false);

		$record_query = mlavu_query("select * from `[source_db]`.`[source_table]` where ".$where_code_old,$vars);

		while($record_read = mysqli_fetch_assoc($record_query))
		{
			$insert_code = "";
			$insert_vals = "";
			foreach($record_read as $key => $val)
			{
			


				if(isset($set_code_from_old[$key]))
				{
				
					$val = $set_code_from_old[$key];
					$record_read[$key] = $val;
				}

				if($key!="id")
				{
					$escaped_key = ConnectionHub::getConn('poslavu')->escapeString($key);
					if($insert_code!="") $insert_code .= ",";
					$insert_code .= "`$escaped_key`";
					if($insert_vals!="") $insert_vals .= ",";
					$insert_vals .= "'[".$escaped_key."]'";
				}
			}
			$read_vars = array_merge($vars,$record_read);

			if(isset($read_vars['menu_id']) && !empty($location_row['menu_id']) ){
    			$read_vars['menu_id'] = $location_row['menu_id'];
			}

			$query = "insert into `[dest_db]`.`[dest_table]` ($insert_code) values ($insert_vals)";
			$success = mlavu_query($query,$read_vars);
			echo display_query_success($success,"-S-","-F-",false);
		}
	}

	function create_associated_config_setting($sfield_name, $ci_info, $exist_read, $ins_vars, $loc_id) {

		if (is_array($ci_info)) {

			$ins_vars['loc_id'] = $loc_id;
			$ins_keys = array("loc_id");
			$mfield = "";
			$cfield = "";
			foreach($ci_info as $key => $val) {
				if ($key == "match") {
					$mfield = $val[0];
					$cfield = $val[1];
					$ins_vars[$cfield] = $exist_read[$mfield];
					$ins_keys[] = $cfield;
				} else {
					$ins_vars[$key] = $val;
					$ins_keys[] = $key;
				}
			}

			$check_config = mlavu_query("SELECT `id` FROM `[1]`.`config` WHERE `location` = '[2]' AND `setting` = '[3]' AND `[4]` = '[5]'", $ins_vars['new_database'], $loc_id, $ins_vars['setting'], $cfield, $exist_read[$mfield]);
			if ($check_config) {
				if (mysqli_num_rows($check_config)) echo "Config setting already exists for $sfield_name : ".$exist_read['id']."<br>";
				else {
					$qfields = "";
					$qvalues = "";
					foreach ($ins_keys as $key) {
						if ($qfields != "") { $qfields .= ", "; $qvalues .= ", "; }
						$qfields .= "`".$key."`";
						$qvalues .= "'".$ins_vars[$key]."'";
					}
					$query = "INSERT INTO `[new_database]`.`config` (`location`, `setting`, `value`, `value2`) VALUES ('[loc_id]', '[setting]', '[value]', '[value2]')";
					$ci_query = mlavu_query($query, $ins_vars);
					if ($ci_query) echo "Config setting (".$ins_vars['setting'].") added for $sfield_name : ".$exist_read['id']."<br>";
					else echo "Failed to add config setting (".$ins_vars['setting'].") for $sfield_name : ".$exist_read['id']."<br>";
				}
			} else echo "Error checking for config setting: ".mlavu_dberror();
		}
	}

	function update_account_product($rowid,$product_name)
	{ 
		$conn = ConnectionHub::getConn('poslavu');
    	global $location_row;//Added by Brian D. Needed for specific location information when copying databases.
		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$rowid);
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			$company_name = $rest_read['company_name'];
			$data_name = $rest_read['data_name'];
			$rest_db = "poslavu_" . $data_name . "_db";
			$prodinfo = get_product_info($product_name);
				
			echo "<br>Convert Product for $company_name to " . $prodinfo['name'] . "<br><br>";
			$loc_query = mlavu_query("select * from `[1]`.`locations` WHERE `_disabled` = '0'",$rest_db);
			if(mysqli_num_rows($loc_query) > 1)
			{
				echo "Cannot convert a multi-location account<br><br>";
			}
			else if(mysqli_num_rows($loc_query) < 1)
			{
				echo "No Locations can be found<br><br>";
			}
			else
			{
				$loc_read = mysqli_fetch_assoc($loc_query);
				$loc_id = $loc_read['id'];
				$location_row = $loc_read;//Added by Brian D. When copying databases, some fields need to be overridden by the location row.
				$prodinfo_string = array();
				foreach($prodinfo as $key => $val)
				{
					if(is_string($val) || is_numeric($val))
					{
						$prodinfo_string[$key] = $val;
					}
				}

				$vars = $prodinfo_string;
				$vars['rest_db'] = $rest_db;
				$vars['id'] = $loc_id;
				$query = "update `[rest_db]`.`locations` set `component_package_code`='[comcode]', `component_package`='[packageid]', `component_package2`='[packageid2]', `component_package3`='[packageid3]' where `id`='[id]'";

				$success = mlavu_query($query,$vars);
				if($success)
				{
					echo "Location data updated<br><br>";

					//lavu_connect_dn($data_name,$rest_db);
					//lavu_connect_byid($rowid,$rest_db);

					$model_db = "poslavu_" . $prodinfo['model'] . "_db";
					$tables = explode(",",$prodinfo['tables']);
					for($i=0; $i<count($tables); $i++)
					{
						$table_parts = explode(":",$tables[$i]);
						$tablesearch = trim($table_parts[0]);
						if(count($table_parts) > 1)
						{
							$tableaction = trim($table_parts[1]);
						}
						else $tableaction = "";

						$query = "show tables from `[1]` like '".escape_but_allow_percentage_sign($tablesearch)."'";
						$table_query = mlavu_query($query,$model_db);
						while($table_read = mysqli_fetch_array($table_query))
						{
							$tablename = $table_read[0];

							//echo "Checking table: $tablename<br>";
							$exist_query = mlavu_query("show tables from `[1]` like '[2]'",$rest_db,$tablename);
							if(mysqli_num_rows($exist_query))
							{
								echo $tablename . " already exists<br>";
							}
							else
							{
								$create_query = mlavu_query("show create table `[1]`.`[2]`",$model_db,$tablename);
								if(mysqli_num_rows($create_query))
								{
									echo "Creating Table: <b>$tablename</b>";
									$create_read = mysqli_fetch_array($create_query);
									if(is_string($create_read[1]))
									{
										// Needed lavu_query() to create the table  in the restaurant's database but it wasn't working so instead
										// of calling lavu_connect_byid() before lavu_query(), we had to call mysql_select_db() and use mlavu_query().
										// But we put the selected database back to poslavu_MAIN_db afterwards since it's more likely we'll query
										// the main database rather than the restaurant database.
										$conn = ConnectionHub::getConn('poslavu');
										$conn->selectDB($rest_db);
										$success = mlavu_query($create_read[1]);
										echo display_query_success($success);
										$conn->selectDB("poslavu_MAIN_db");
									}
								}
							}

							if($tableaction=="fill")
							{
								$record_count = get_query_record_count("`[1]`.`[2]`",$rest_db,$tablename);
								if($record_count < 1)
								{
									//echo "insert into `$rest_db`.`$tablename` select * from `$model_db`.`$tablename`<br>";
									echo "filling $tablename ...";
									$success = mlavu_query("insert into `[1]`.`[2]` select * from `[3]`.`[4]`",$rest_db,$tablename,$model_db,$tablename);
									echo display_query_success($success);
								}
								else
								{
									echo "$tablename already has records<br>";
								}
							}
						}
						//echo escape_but_allow_percentage_sign($tablesearch);
					}

					$config_settings = (isset($prodinfo['config']))?$prodinfo['config']:"";
					$config_settings = explode(",",$config_settings);

					for($i=0; $i<count($config_settings); $i++)
					{
						$config_setting = trim($config_settings[$i]);
						$record_count = get_query_record_count("`[1]`.`config` where `setting`='[2]'",$rest_db,$config_setting);
						if($record_count < 1)
						{
							echo "adding config settings: $config_setting ...<br>";
							insert_all_records_into_table_from_table($model_db,"config",$rest_db,"config","`setting`='[value]'",array("value"=>$config_setting));
							echo "<br>";
						}
						else
						{
							echo "config settings $config_setting already present<br>";
						}
					}

					//"special_field1" => array("name"=>"forced_mod_list","table"=>"forced_modifier_lists","where"=>"`title`='Customer Mod'"),
					//"special_field2" => array("name"=>"forced_mod_item","table"=>"forced_modifiers","where"=>"`title`='get_customer'","link_field"=>"list_id","link_to"=>"forced_mod_list"));

					$new_field_ids = array();
					$old_field_ids = array();
					for($i=1; $i<=20; $i++)
					{
						if(isset($prodinfo['special_field'.$i]))
						{
							$sfield_info = $prodinfo['special_field'.$i];
							$sfield_name = (isset($sfield_info['name']))?$sfield_info['name']:"";
							$sfield_table = (isset($sfield_info['table']))?$sfield_info['table']:"";
							$sfield_where = (isset($sfield_info['where']))?$sfield_info['where']:"";
							$sfield_link_field = (isset($sfield_info['link_field']))?$sfield_info['link_field']:"";
							$sfield_link_to = (isset($sfield_info['link_to']))?$sfield_info['link_to']:"";
							$sfield_image_field = (isset($sfield_info['image_field']))?$sfield_info['image_field']:"";
							$sfield_image_dir = (isset($sfield_info['image_dir']))?$sfield_info['image_dir']:"";
							$sfield_config_insert = (isset($sfield_info['config_insert']))?$sfield_info['config_insert']:"";

							$ins_vars = array();
							$ins_vars['new_database'] = $rest_db;
							$ins_vars['source_database'] = $model_db;
							$ins_vars['tablename'] = $sfield_table;

							$sfield_set_old = array();
							$sfield_where_old = $sfield_where;
							if($sfield_link_field!="")
							{
								if(isset($new_field_ids[$sfield_link_to]))
								{
									if($sfield_where!="") $sfield_where .= " and ";
									if($sfield_where_old!="") $sfield_where_old .= " and ";

									$sfield_where .= "`".$conn->escapeString($sfield_link_field)."`='".$conn->escapeString($new_field_ids[$sfield_link_to])."'";
									$sfield_where_old .= "`".$conn->escapeString($sfield_link_field)."`='".$conn->escapeString($old_field_ids[$sfield_link_to])."'";
									$sfield_set_old[$sfield_link_field] = $new_field_ids[$sfield_link_to];
								}
								else
								{
									$sfield_where .= " and `id`='0'";
									$sfield_where_old .= " and `id`='0'";
								}
							}

							$source_querystr = "select * from `[source_database]`.`[tablename]` where " . $sfield_where_old;
							$source_query = mlavu_query($source_querystr,$ins_vars);
							if(mysqli_num_rows($source_query))
							{
								$source_read = mysqli_fetch_assoc($source_query);
								$old_field_ids[$sfield_name] = $source_read['id'];
							}

							$sel_query = "select * from `[new_database]`.`[tablename]` where " . $sfield_where;
							echo $sel_query . "<br>";
							$exist_query = mlavu_query($sel_query,$ins_vars);
							if(mysqli_num_rows($exist_query))
							{
								$exist_read = mysqli_fetch_assoc($exist_query);
								$new_field_ids[$sfield_name] = $exist_read['id'];
								echo "Row exists in $sfield_table for $sfield_name : " . $exist_read['id'] . "<br>";
								create_associated_config_setting($sfield_name, $sfield_config_insert, $exist_read, $ins_vars, $loc_id);
							}
							else
							{
								echo "No Rows found:<br>";
								echo "<hr>";
								echo "model_db: $model_db<br>sfield_table: $sfield_table<br>rest_db: $rest_db<br>sfield_where: $sfield_where<br>sfield_where_old: $sfield_where_old<br>sfield_set_old: $sfield_set_old<br>";
								

								insert_all_records_into_table_from_table($model_db,$sfield_table,$rest_db,$sfield_table,array($sfield_where,$sfield_where_old,$sfield_set_old),array());
								$exist_query = mlavu_query($sel_query,$ins_vars);
								while($exist_read = mysqli_fetch_assoc($exist_query))
								{
									$new_field_ids[$sfield_name] = $exist_read['id'];
									echo "Row added in $sfield_table for $sfield_name : " . $exist_read['id'] . "<br>";
									create_associated_config_setting($sfield_name, $sfield_config_insert, $exist_read, $ins_vars, $loc_id);
								}
							}

							if($sfield_image_field!="" && $sfield_image_dir!="")
							{
								$source_query = mlavu_query($source_querystr,$ins_vars);
								while($source_read = mysqli_fetch_assoc($source_query))
								{
									$source_basename = $source_read[$sfield_image_field];
									$filename_dir = "/mnt/poslavu-files/images/[dataname]/" . $sfield_image_dir;
									$filename = $filename_dir . "/" . $source_basename;
									$filename_main = $filename_dir . "/main/" . $source_basename;
									$filename_full = $filename_dir . "/full/" . $source_basename;

									$filename_from = str_replace("[dataname]",$prodinfo['model'],$filename);
									$filename_main_from = str_replace("[dataname]",$prodinfo['model'],$filename_main);
									$filename_full_from = str_replace("[dataname]",$prodinfo['model'],$filename_full);

									$filename_to = str_replace("[dataname]",$data_name,$filename);
									$filename_main_to = str_replace("[dataname]",$data_name,$filename_main);
									$filename_full_to = str_replace("[dataname]",$data_name,$filename_full);

									echo copy_poslavu_image_file($filename_from,$filename_to);
									echo copy_poslavu_image_file($filename_main_from,$filename_main_to);
									echo copy_poslavu_image_file($filename_full_from,$filename_full_to);

								}
							}
						}
					}
				}
				else echo "Unable to update location data<br><br>";
			}
		}
	}

	if(isset($_GET['direct']))
	{
		ini_set('display_errors', 1);
		echo "Marker1";
		//$rowid = urlvar("rowid","");
		//$set_product = urlvar("product","");
		$rowid = isset($_GET['rowid']) ? $_GET['rowid'] : '';
		$set_product = isset($_GET["product"]) ? $_GET["product"] : '';
		$submode = "convert_product";
		echo "Marker2";
		require_once(dirname(__FILE__) . "/../../cp/resources/core_functions.php");
		if(isset($submode) && $submode=="convert_product" && $rowid!="")
		{
			if(in_array(strtolower($set_product), array("customer", "hotel", "lasertag", "lavukart", "medical", "pizza", "fitness","modern")))
			{
				echo "About to convert product with rowid:$rowid to product: $set_product";
				update_account_product($rowid,$set_product);
			}
			else echo "No Product specified for $rowid<br>";
		}
	}
