<?php
	function create_monthly_payment_chart($title,$start_year,$start_month,$min_amount,$max_amount,$color,$color2,$divide_by,$by_day=false,&$passback_totals=false,$start_day=false,&$passback_refund_totals=false)
	{		
		$m = 0;
		$done = false;
		
		$show_change_values = (isset($_GET['show_change_values']) && !isset($_GET['by_day']) && in_array($title, array("Recurring (all txn less than or equal to 300)", "Silver", "Gold", "Platinum")));
		$last_month_value = 0;
				
		echo "<table cellspacing=0 cellpadding=2 style='border:solid 1px #d4d4d4' bgcolor='#f4f4f4'>";
		echo "<tr style='background-color:#aaaaaa'>";
		echo "<td colspan='4'>&nbsp;</td>";
		echo "<td colspan='6' align='left'><table cellpadding=4><tr><td><b><font style='color:#ffffff'>$title</font></b></td></tr></table></td>";
		echo "</tr>";
		while(!$done)
		{
			echo "<tr>";
			echo "<td>&nbsp;</td>";
			
			if($by_day)
			{
				$dts = mktime(0,0,0,$start_month,1 + $m,$start_year);
				$date_day = date("Y-m-d",$dts);
				
				$month_min = $date_day . " 00:00:00";
				$month_max = $date_day . " 24:00:00";
				
				$show_date = $date_day . " <font style='color:$color'>" . date("D",$dts) . "</font>";
			}
			else
			{
				$date_month = date("Y-m",mktime(0,0,0,($start_month + $m),1,$start_year));
				
				$month_min = $date_month . "-00 00:00:00";
				$month_max = $date_month . "-32 00:00:00";
				
				$show_date = $date_month;
			}
			
			if($title=="Silver")// || $title=="Non-Recurring")
			{
				$or_condition = " or `x_amount` * 1=88";
			}
			else
			{
				$or_condition = "";
			}
			
			$filter_payments_that_cover = (isset($_GET['no_collection']))?" AND `batch_action` NOT LIKE 'used to cover%'":"";
									
			$pay_query = mlavu_query("select count(*) as `count`, sum(`x_amount` * 1) as `total` from `poslavu_MAIN_db`.`payment_responses` where `datetime`>'[1]' and `datetime`<'[2]' and ((`x_amount` * 1 >= '[3]' * 1 and `x_amount` * 1 < '[4]')".$or_condition.") and `x_response_code`='1' and `x_type`='auth_capture'".$filter_payments_that_cover,$month_min,$month_max,$min_amount,$max_amount);
			if(mysqli_num_rows($pay_query))
			{
				$pay_read = mysqli_fetch_assoc($pay_query);
				
				$pay_count = $pay_read['count'];
				$pay_total = $pay_read['total'];
				
				// Check for Lavu 88
				$type_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`reseller_licenses` where type='Lavu88' and `purchased`>'[1]' and `purchased`<'[2]' and `resellername`!='distrotest' and `resellername`!='martin' and `resellername`!='tomlavu'",$month_min,$month_max);
				if(mysqli_num_rows($type_query))
				{
					$type_read = mysqli_fetch_assoc($type_query);
					$set_count = $type_read['count'];
					$pay_count += $set_count;
				}
				// End Check for Lavu 88

				// Check for Lavu Free
				$type_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`reseller_licenses` where type LIKE '%FreeLavu%' and `purchased`>'[1]' and `purchased`<'[2]' and `resellername`!='distrotest' and `resellername`!='martin' and `resellername`!='tomlavu'",$month_min,$month_max);
				if(mysqli_num_rows($type_query))
				{
					$type_read = mysqli_fetch_assoc($type_query);
					$set_count = $type_read['count'];
					$pay_count += $set_count;
				}
				// End Check for Lavu Free
				
				if(date("Y-m")==$date_month && !$by_day)
				{
					$estimated_count = ($pay_count / date("d")) * 30;
					$estimated_total = ($pay_total / date("d")) * 30;
				}
				
				if (is_array($passback_totals)) $passback_totals[] = number_format($pay_total,0,".","");
				
				echo "<td>" . $show_date . "</td>";
				echo "<td align='right'><b>" . number_format($pay_count,0) . "</b></td>"; 
				$refund_total = 0;
				if(isset($_GET['payments'])) 
				{
					$refund_query = mlavu_query("select count(*) as `count`, sum(`x_amount` * 1) as `total` from `poslavu_MAIN_db`.`payment_responses` where `datetime`>'[1]' and `datetime`<'[2]' and ((`x_amount` * 1 >= '[3]' * 1 and `x_amount` * 1 < '[4]')".$or_condition.") and `x_response_code`='1' and `x_type`='credit'".$filter_payments_that_cover,$month_min,$month_max,$min_amount,$max_amount);
					if(mysqli_num_rows($refund_query))
					{
						$refund_read = mysqli_fetch_assoc($refund_query);
						$refund_total = $refund_read['total'];
					}
					echo "<td align='right'><font style='color:#008800'>" . number_format($pay_total,0) . "</font></td>"; 
					echo "<td align='right'><font style='color:#880000'>-" . number_format($refund_total,0) . "</font></td>"; 
					echo "<td align='right'><font style='color:#000088'>" . number_format(($pay_total * 1 - $refund_total * 1),0) . "</font></td>"; 
				}
				if (is_array($passback_refund_totals)) $passback_refund_totals[] = number_format($refund_total,0,".","");
				echo "<td align='left'>";
				echo "<table cellpadding=0 cellspacing=0".(isset($_GET['show_change_values'])?" style='display:inline-table'":"")."><tr><td style='width:" . floor($pay_count / $divide_by) . "px; height:16px; background-color:$color'>&nbsp;</td>";
				if($estimated_count > $pay_count)
				{
					echo "<td style='width:" . floor(($estimated_count - $pay_count) / $divide_by) . "px; height:16px; background-color:$color2'>&nbsp;</td>";
				}
				echo "</tr></table>";

				if ($show_change_values && $m>0 && $date_month!=date("Y-m")) {
					$change_value = ($pay_count - $last_month_value);
					$cv_color = "#3E8653";
					$cv_symbol = "+";
					if ($change_value == 0) {
						$cv_color = "#000000";
						$cv_symbol = "";
					} else if ($change_value < 0) {
						$cv_color = "#B70000";
						$cv_symbol = "-";
					}
					echo "<font size='-2' color='".$cv_color."'><b> ".$cv_symbol." ".abs($change_value)."</b></font>";
				}		

				echo "</td>";	
			}
			
			$last_month_value = $pay_count;
			
			if($by_day)
			{
				if($date_day >= date("Y-m-d") || $date_day < "2000-01-01")
				{
					$done = true;
				}
			}
			else
			{
				if($date_month >= date("Y-m") || $date_month < "2000-01")
				{
					$done = true;
				}
			}
			$m++;
			echo "<td>&nbsp;</td>";
			echo "</tr>";
		}
		echo "</table>";
	}
	
	function get_freelavu_count($date_month)
	{
		if(strlen($date_month)==10)
		{
			$month_min = $date_month . " 00:00:00";
			$month_max = $date_month . " 24:00:00";
		}
		else
		{
			$month_min = $date_month . "-00 00:00:00";
			$month_max = $date_month . "-31 24:00:00";
		}
		
		// Check for Lavu Free
		$type_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`reseller_licenses` where type LIKE '%FreeLavu%' and `purchased`>'[1]' and `purchased`<'[2]' and `resellername`!='distrotest' and `resellername`!='martin' and `resellername`!='tomlavu'",$month_min,$month_max);
		if(mysqli_num_rows($type_query))
		{
			$type_read = mysqli_fetch_assoc($type_query);
			$set_count = $type_read['count'];
			return $set_count;
		}
		else return 0;
		// End Check for Lavu Free
	}

	function get_lavu88_first_payment_count($date_month)
	{
		$month_count = 0;
		$month_totals = array();
		$last_month = "";
		$rests_found = array();
		if(strlen($date_month)==10)
		{
			$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where left(`applied`,10)='[1]' and `type`='Lavu88'",$date_month);
		}
		else
		{
			$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where left(`applied`,7)='[1]' and `type`='Lavu88'",$date_month);
		}
		while($lic_read = mysqli_fetch_assoc($lic_query))
		{
			$restid = $lic_read['restaurantid'];
			$pay_month = substr($lic_read['applied'],0,strlen($date_month));
			if(!isset($rests_found[$restid]))
			{
				if(!isset($month_totals[$pay_month])) $month_totals[$pay_month] = 0;
				$month_totals[$pay_month]++;
				//if($date_month==$pay_month)
				//	echo "LIC 88 - " . $lic_read['applied'] . "<br>";
			}
			$rests_found[$restid] = 1;
		}
		
		$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_amount` * 1 = 88 and `x_type`='auth_capture' and `x_response_code`='1' order by date asc");
		while($pay_read = mysqli_fetch_assoc($pay_query))
		{
			$restid = $pay_read['match_restaurantid'];
			if(!isset($rests_found[$restid]))
			{
				if(strlen($date_month)==10)
				{
					$pay_month = substr($pay_read['date'],0,10);
				}
				else
				{
					$pay_month = substr($pay_read['date'],0,7);
				}
				if($last_month != $pay_month)
				{
					$month_count = 0;
				}
				
					$prestid = $pay_read['match_restaurantid'];
					if($prestid!="")
					{
						$prev_query = mlavu_query("select id from poslavu_MAIN_db.payment_responses where match_restaurantid='[1]' and `datetime`<'[2]' and `x_response_code`='1'",$prestid,$pay_read['datetime']);
						if(mysqli_num_rows($prev_query) < 1)
						{		
							$month_count++;
							//if($date_month==$pay_month)
							//	echo "PAY 88 - ".$pay_read['datetime']." - ".$pay_read['match_restaurantid']."<br>";
							if(!isset($month_totals[$pay_month])) $month_totals[$pay_month] = 0;
							$month_totals[$pay_month]++;
							$last_month = $pay_month;
						}
					}
			}
			$rests_found[$restid] = 1;
		}
		if(isset($month_totals[$date_month])) return $month_totals[$date_month];
		else return "";
	}

	function get_month_payment_breakdown($date_month)
	{
		$lic_amounts = array();
		$lic_amounts[] = array(895,"license",1);
		$lic_amounts[] = array(716,"license",1);
		$lic_amounts[] = array(1495,"license",2);
		$lic_amounts[] = array(3495,"license",3);
		$lic_amounts[] = array(2495,"license",2);
		$lic_amounts[] = array(600,"upgrade",1);
		$lic_amounts[] = array(1000,"upgrade",1);
		$lic_amounts[] = array(1600,"upgrade",2);
		$lic_amounts[] = array(2000,"upgrade",2);
		$lic_amounts[] = array(2600,"upgrade",2);
		$lic_amounts[] = array(4500,"buyin");
		$lic_amounts[] = array(9000,"buyin");
		$distro_amounts = array(0,20,25,30,35,40,50);
		$non_distro_amounts = array(0,10,15,20,25,30);
		
		$totals = array();
		
		if(strlen($date_month)==10)
		{
			$month_min = $date_month . " 00:00:00";
			$month_max = $date_month . " 24:00:00";
		}
		else
		{
			$month_min = $date_month . "-00 00:00:00";
			$month_max = $date_month . "-32 00:00:00";
		}
		$min_amount = 300;
		$max_amount = 20000;
		$new_terminals = 0;
		
		$totals['new_terminals'] = "";
		$totals['lavu88_licenses_created'] = "";
		$totals['lavufree'] = "";
		/*$type_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`reseller_licenses` where type='Lavu88' and `purchased`>'[1]' and `purchased`<'[2]' and `resellername`!='distrotest' and `resellername`!='martin' and `resellername`!='tomlavu'",$month_min,$month_max);
		if(mysqli_num_rows($type_query))
		{
			$type_read = mysqli_fetch_assoc($type_query);
			$set_count = $type_read['count'];
			if($set_count < 1) $set_count = "";
			$totals['lavu88_licenses_created'] = $set_count;
			//$totals['license'] = $set_count;
			//$totals['distributor_license'] = $set_count;
		}*/
		$totals['lavu88_licenses_created'] = get_lavu88_first_payment_count($date_month);
		$totals['lavufree'] = get_freelavu_count($date_month);
		
		if(is_numeric($totals['lavu88_licenses_created'])) $new_terminals+=$totals['lavu88_licenses_created'];
		if(is_numeric($totals['lavufree'])) $new_terminals+=$totals['lavufree'];
		
		if(strlen($date_month)==10)
		{
			$point_query = mlavu_query("select count(*) as `count`,left(purchased,10) as pdate from poslavu_MAIN_db.reseller_licenses where notes LIKE '%points%' and left(purchased,10)='[1]'",$date_month);
			
			$point_breakdown_query = mlavu_query("select `value` from poslavu_MAIN_db.reseller_licenses where notes LIKE '%points%' and left(purchased,10)='[1]'",$date_month);
		}
		else
		{
			$point_query = mlavu_query("select count(*) as `count`,left(purchased,7) as pdate from poslavu_MAIN_db.reseller_licenses where notes LIKE '%points%' and left(purchased,7)='[1]'",$date_month);
			$point_breakdown_query = mlavu_query("select `value` from poslavu_MAIN_db.reseller_licenses where notes LIKE '%points%' and left(purchased,7)='[1]'",$date_month);
		}
		if(mysqli_num_rows($point_query))
		{
			$point_read = mysqli_fetch_assoc($point_query);
			$point_purchases = $point_read['count'];
			
			while($point_breakdown_read = mysqli_fetch_assoc($point_breakdown_query))
			{
				$pointval = $point_breakdown_read['value'];
				if($pointval * 1 < 1000)
					$new_terminals++;
				else if($pointval * 1 <= 2000)
					$new_terminals+=2;
				else if($pointval * 1 > 2000)
					$new_terminals+=3;
				else
					$new_terminals++;
			}
		}
		else $point_purchases = 0;
		if($point_purchases < 1) $point_purchases = "";
		$totals['purchased_with_points'] = $point_purchases;
					
		$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `datetime`>'[1]' and `datetime`<'[2]' and ((`x_amount` * 1 >= '[3]' * 1 and `x_amount` * 1 < '[4]') or x_description LIKE 'POSLavu Payment L%' and x_amount * 1 > 0) and `x_response_code`='1' and `x_type`='auth_capture'",$month_min,$month_max,$min_amount,$max_amount);
		//echo $month_min . " - $month_max ($min_amount - $max_amount) = " . mysqli_num_rows($pay_query) . "<br>";
		while($pay_read = mysqli_fetch_assoc($pay_query))
		{
			//foreach($pay_read as $key => $val) echo $key . " = " . $val . "<br>";
			$pdesc = $pay_read['x_description'];
			$mtype = $pay_read['match_type'];
			$amount = $pay_read['x_amount'];
			$cstr = "";
			$ptype = "?";
			$psubtype = "?";
			
			if($mtype=="billing profile match")
			{
				$ptype = "license";
				$psubtype = "restaurant";
			}
			else
			{
				$desc_split = explode("POSLavu Auth RDS",$pdesc);
				if(count($desc_split) > 1)
				{
					$psubtype = "distributor";
					$type_found = "";
					for($n=0; $n<count($lic_amounts); $n++)
					{
						for($m=0; $m<count($distro_amounts); $m++)
						{
							$check_amount = $lic_amounts[$n][0] * 1 - ($lic_amounts[$n][0] * ($distro_amounts[$m] / 100));
							$cstr .= $check_amount . " ";
							if(number_format($amount,2)==number_format($check_amount,2))
							{
								$type_found = $lic_amounts[$n][1];
								if(isset($lic_amounts[$n][2])) $new_terminals += $lic_amounts[$n][2]; else $new_terminals += 1;
							}
						}
					}
					if($type_found!="") $ptype = $type_found;
					else $ptype = "other";
				}
				else
				{
					$desc_split = explode("POSLavu Auth R",$pdesc);
					if(count($desc_split) > 1)
					{
						$psubtype = "restaurant";
						$type_found = "";
						for($n=0; $n<count($lic_amounts); $n++)
						{
							for($m=0; $m<count($non_distro_amounts); $m++)
							{
								$check_amount = $lic_amounts[$n][0] * 1 - ($lic_amounts[$n][0] * ($non_distro_amounts[$m] / 100));
								if(number_format($amount,2)==number_format($check_amount,2))
								{
									$type_found = $lic_amounts[$n][1];
									if(isset($lic_amounts[$n][2])) $new_terminals += $lic_amounts[$n][2]; else $new_terminals += 1;
								}
							}
						}
						if($type_found!="") $ptype = $type_found;
						else $ptype = "other";
					}
				}
			}
			
			if($ptype=="?")
			{
				$type_found = "";
				for($n=0; $n<count($lic_amounts); $n++)
				{
					for($m=0; $m<count($distro_amounts); $m++)
					{
						$check_amount = $lic_amounts[$n][0] * 1 - ($lic_amounts[$n][0] * ($distro_amounts[$m] / 100));
						$cstr .= $check_amount . " ";
						if(number_format($amount,2)==number_format($check_amount,2))
						{
							if(isset($lic_amounts[$n][2])) $new_terminals += $lic_amounts[$n][2]; else $new_terminals += 1;
							$type_found = $lic_amounts[$n][1];
							if($distro_amounts[$m] * 1==0)
							{
								$psubtype = "restaurant";
							}
							else
							{
								$psubtype = "distributor";
							}
						}
					}
				}
				if($type_found!="") $ptype = $type_found;
				else $ptype = "other";
			}
			
			if($ptype=="other")
			{
				if($amount * 1 >= 0)
				{
					$prestid = $pay_read['match_restaurantid'];
					if($prestid!="")
					{
						$prev_query = mlavu_query("select id from poslavu_MAIN_db.payment_responses where match_restaurantid='[1]' and `datetime`<'[2]' and `x_response_code`='1'",$prestid,$pay_read['datetime']);
						if(mysqli_num_rows($prev_query) < 1)
						{
							//echo $pay_read['datetime'] . " - " . $pay_read['x_amount'] . "<br>";
							$ptype = "invoiced";
							$new_terminals++;
						}
					}
				}
			}
			
			//echo $psubtype . " - " . $ptype . " - " . $amount;
			//if($ptype=="other") echo " - from " . $pdesc;//echo "<br><font style='color:#888888'>(checked against " . $cstr . ")</font>";
			//echo "<br>";
			
			$totals[$ptype]++;
			$totals[$psubtype . "_" . $ptype]++;
			//echo $mtype . " - " . $pdesc . " - " . $pay_read['x_amount'] . "<br>";
		}
		$totals['new_terminals'] = $new_terminals;
		return $totals;
		//foreach($totals as $key => $val) echo $key . " = " . $val . "<br>";
	}

	function get_estimated_amount($amount,$day,$total_days)
	{
		$amount = (($amount / $day) * $total_days);
		$amount = $amount + .5;
		$amount = floor($amount);
		
		return $amount;
	}
	
	function output_payment_breakdown($start_year,$start_month,$start_day=false,$bar_mult=1)
	{
		$m = 0;
		$done = false;
		
		echo "<table cellspacing=0>";
		echo "<tr style='background-color:#aaaaaa'>";
		echo "<td>&nbsp;</td>";

		echo "<td colspan='2'>";
		
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Lic - Self</td></tr></table>";
		
		echo "</td><td colspan='2'>";

		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Lic - Distro</td></tr></table>";
		
		echo "</td><td colspan='2'>";
		
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Points</td></tr></table>";
		
		echo "</td><td colspan='2'>";
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Upgrades</td></tr></table>";
		
		echo "</td><td colspan='2'>";
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Invoiced</td></tr></table>";
		
		echo "</td><td colspan='2'>";
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Other</td></tr></table>";
		
		echo "</td><td colspan='2'>";
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Buy-Ins</td></tr></table>";

		echo "</td><td colspan='2'>";
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Lavu88</td></tr></table>";

		echo "</td><td colspan='2'>";
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Lavu Free</td></tr></table>";

		echo "</td><td colspan='2'>";
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>New Customers</td></tr></table>";

		echo "</td><td colspan='2'>";
		echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>New Terminals</td></tr></table>";
		//echo "</td><td colspan='2'>";
		//echo "<table cellpadding=4><tr><td style='color:#ffffff; font-weight:bold'>Unknown</td></tr></table>";

		$count_direction = "up";
		echo "</td>";
		echo "</tr>";
		while(!$done)
		{
			$trprops = "";
			if($start_day)
			{
				if($count_direction=="up")
					$date_ts = mktime(0,0,0,$start_month,($start_day + $m),$start_year);
				else
					$date_ts = mktime(0,0,0,date("m"),(date("d") - $m),date("Y"));
				$date_month = date("Y-m-d",$date_ts);
				$date_dow = date("D",$date_ts);
				
				if($date_dow=="Sat" || $date_dow=="Sun") $trprops = " bgcolor='#eeeeee'";
			}
			else
			{
				if($count_direction=="up")
					$date_ts = mktime(0,0,0,($start_month + $m),1,$start_year);
				else
					$date_ts = mktime(0,0,0,(date("m") - $m),1,$start_year);
				$date_month = date("Y-m",$date_ts);
				$date_dow = "";
			}
			echo "<tr".$trprops.">";

			$totals = get_month_payment_breakdown($date_month);
			$licenses = (isset($totals['license']))?$totals['license']:"";
			$self_licenses = (isset($totals['restaurant_license']))?$totals['restaurant_license']:"";
			$distro_licenses = (isset($totals['distributor_license']))?$totals['distributor_license']:"";
			$upgrades = (isset($totals['upgrade']))?$totals['upgrade']:"";
			$with_points = (isset($totals['purchased_with_points']))?$totals['purchased_with_points']:"";
			$invoiced = (isset($totals['invoiced']))?$totals['invoiced']:"";
			$other = (isset($totals['other']))?$totals['other']:"";
			$buyins = (isset($totals['buyin']))?$totals['buyin']:"";
			$unknown = (isset($totals['unknown']))?$totals['unknown']:"";
			$lavu88 = (isset($totals['lavu88_licenses_created']))?$totals['lavu88_licenses_created']:0;
			$lavufree = (isset($totals['lavufree']))?$totals['lavufree']:0;
			
			$new_terminals = (isset($totals['new_terminals']))?$totals['new_terminals']:0;
				
			echo "<td>" . $date_month;
			if(strlen($date_month)==10)
			{
				echo " " . $date_dow;
			}
			echo "</td>";
			
			//$new_terminals = ($lavu88 + $self_licenses + $distro_licenses + $with_points + $lavufree + $invoiced);
			
			$estimated_total_licenses = ($self_licenses + $distro_licenses);
			$estimated_self_licenses = $self_licenses;
			$estimated_distro_licenses = $distro_licenses;
			$estimated_total_lavu88 = $lavu88;
			$estimated_total_upgrades = $upgrades;
			$estimated_with_points = $with_points;
			$estimated_lavufree = $lavufree;
			$estimated_total_new_accounts = ($lavu88 + $self_licenses + $distro_licenses + $with_points + $lavufree + $invoiced);
			$estimated_total_new_terminals = $new_terminals;
			
			$month_days = 31;
			if($date_month == date("Y-m"))
			{
				$current_day = date("d");
				$estimated_self_licenses = get_estimated_amount($estimated_self_licenses,$current_day,$month_days);
				$estimated_distro_licenses = get_estimated_amount($estimated_distro_licenses,$current_day,$month_days);
				$estimated_total_licenses = get_estimated_amount($estimated_total_licenses,$current_day,$month_days);
				$estimated_total_lavu88 = get_estimated_amount($estimated_total_lavu88,$current_day,$month_days);
				$estimated_lavufree = get_estimated_amount($lavufree,$current_day,$month_days);
				$estimated_total_upgrades = get_estimated_amount($estimated_total_upgrades,$current_day,$month_days);
				$estimated_with_points = get_estimated_amount($estimated_with_points,$current_day,$month_days);
				$estimated_total_new_accounts = get_estimated_amount($estimated_total_new_accounts,$current_day,$month_days);
				$estimated_total_new_terminals = get_estimated_amount($estimated_total_new_terminals,$current_day,$month_days);
			}
			
			//foreach($totals as $tkey => $tval) echo $date_month . " $tkey = $tval<br>";
			if($unknown!="") $other += $unknown;
			echo value_and_bar($self_licenses,2 * $bar_mult,"#44aa44",$estimated_self_licenses,"#44cc44");
			echo value_and_bar($distro_licenses,2 * $bar_mult,"#44aa44",$estimated_distro_licenses,"#44cc44");
			//echo value_and_bar(($self_licenses + $distro_licenses),2 * $bar_mult,"#44aa44",$estimated_total_licenses,"#44cc44");
			echo value_and_bar($with_points,2 * $bar_mult,"#448888",$estimated_with_points,"#44cccc");
			echo value_and_bar($upgrades,2 * $bar_mult,"#4444aa",$estimated_total_upgrades,"#8888cc");
			echo value_and_bar($invoiced,2 * $bar_mult,"#aa4620");
			echo value_and_bar($other,2 * $bar_mult,"#aa4620");
			echo value_and_bar($buyins,4 * $bar_mult,"#44aaaa");
			echo value_and_bar($lavu88,2 * $bar_mult,"#448888",$estimated_total_lavu88,"#44cccc");
			echo value_and_bar($lavufree,2 * $bar_mult,"#448888",$estimated_lavufree,"#44cccc");
			echo value_and_bar(($lavu88 + $self_licenses + $distro_licenses + $with_points + $lavufree + $invoiced),2 * $bar_mult,"#44aa88",$estimated_total_new_accounts,"#44ccaa");
			echo value_and_bar(($new_terminals),2 * $bar_mult,"#77ddaa",$estimated_total_new_terminals,"#77ffcc");
			//echo value_and_bar($unknown,2,"#228822");
			
			//echo "<td>" . $licenses . "</td>";
			//echo "<td>" . $upgrades . "</td>";
			//echo "<td>" . $other . "</td>";
			//echo "<td>" . $buyins . "</td>";

			if(strlen($date_month)==10)
			{
				if(($count_direction=="up" && ($date_month >= date("Y-m-d") || $date_month < "2000-01")) ||
				 ($count_direction!="up" && ($date_month <= date("Y-m-d",mktime(0,0,0,$start_month,date("d"),$start_year)) || $date_month < "2000-01-01" || $date_month > "2400-01-01")))
				{
					$done = true;
				}
			}
			else
			{
				if(($count_direction=="up" && ($date_month >= date("Y-m") || $date_month < "2000-01")) ||
				 ($count_direction!="up" && ($date_month <= date("Y-m",mktime(0,0,0,$start_month,date("d"),$start_year)) || $date_month < "2000-01" || $date_month > "2400-01")))
				{
					$done = true;
				}
			}
			$m++;
			echo "</tr>";
		}
		echo "</table>";
	}
	
	function value_and_bar($val,$mult,$color,$val2=0,$color2="")
	{
		$str = "";
		$str .= "<td align='right'><b>";
		//if($val2 > $val) $str .= $val2;
		//else $str .= $val;
		if(is_numeric($val) && $val * 1 > 0)
		{
			$str .= $val;
		}
		else $str .= "&nbsp;";
		$str .= "</b></td>"; 
		
		//echo "<td align='right'><font style='color:#008800'>" . number_format($pay_total,0) . "</font></td>"; 
		$str .= "<td align='left'>";
		$str .= "<table cellpadding=0 cellspacing=0><tr><td style='width:" . floor($val * $mult / 2) . "px; height:16px; background-color:$color'>&nbsp;</td>";
		if($val2 > $val)
		{
			$str .= "<td style='width:" . floor((($val2 - $val) * $mult) / 2) . "px; height:16px; background-color:$color2'>&nbsp;</td>";
		}
		$str .= "</tr></table>";
		$str .= "</td>";
		return $str;
	}
	
	function create_recurring_non_recurring_sum_chart($recurring_totals, $non_recurring_totals, $recurring_refund_totals, $non_recurring_refund_totals) {

		$max_total = 0;
		for ($t = 0; $t < count($recurring_totals); $t++) {
			$max_total = max($max_total, ($recurring_totals[$t] + $non_recurring_totals[$t]));
		}
	
		echo "<table cellspacing=0 cellpadding=2 style='border:solid 1px #d4d4d4' bgcolor='#f4f4f4'>";
		echo "<tr style='background-color:#aaaaaa'>";
		echo "<td colspan='8' align='center'><table cellpadding=4><tr><td><b><font style='color:#ffffff'>Recurring + Non-Recurring</font></b></td></tr></table></td>";
		echo "</tr>";
		for ($t = 0; $t < count($recurring_totals); $t++) {
			echo "<tr>";
			echo "<td width='1px'>&nbsp;</td>";
	
			$total = ($recurring_totals[$t] + $non_recurring_totals[$t]);
			$refund_total = ($recurring_refund_totals[$t] + $non_recurring_refund_totals[$t]);
			echo "<td align='right'><font style='color:#008800'>" . number_format($total,0) . "</font></td>"; 
			echo "<td align='right'><font style='color:#880000'>-" . number_format($refund_total,0) . "</font></td>"; 
			echo "<td align='right'><font style='color:#000088'>" . number_format(($total * 1 - $refund_total * 1),0) . "</font></td>"; 
			
			$bar_width = max(1, floor(($total / $max_total) * 150));
			echo "<td align='left'>";
			echo "<table cellpadding=0 cellspacing=0".(isset($_GET['show_change_values'])?" style='display:inline-table'":"")."><tr><td style='width:" . $bar_width . "px; height:16px; background-color:#3e8653'>&nbsp;</td>";

			echo "</tr></table>";
			echo "</td>";
		
			echo "<td>&nbsp;</td>";
			echo "</tr>";
		}
		echo "</table>";
	}
	
	function output_monthly_reports()
	{		
		$view_all = "";
		if(isset($_GET['view_all'])) $view_all = $_GET['view_all'];
		
		if(isset($_GET['by_day'])) 
		{
			$by_day = true;
			$start_year = 2014;
			$start_month = 1;
			$div_more = 1;
			$div_less = 0.2;
		}
		else
		{
			$by_day = false;
			if($view_all=="yes")
			{
				$start_year = 2013;
				$start_month = 8;
			}
			else if($view_all=="recent")
			{
				$days_back = (isset($_GET['days']))?$_GET['days']:30;
				$rts = mktime(0,0,0,date("m"),date("d")-$days_back,date("Y"));
				$start_year = date("Y",$rts);
				$start_month = date("m",$rts);
				$start_day = date("d",$rts);
			}
			else if($view_all=="day")
			{
				$start_year = 2014;
				$start_month = date("m");
				$start_day = date("d");
				$start_month-=2;
				if($start_month<=0) $start_month += 12;
			}
			else
			{
				$start_year = 2012;
				$start_month = 8;
			}
			$div_more = 5;
			$div_less = 1;
		}
		
		$recurring_totals = array();
		$recurring_refund_totals = array();
		$non_recurring_totals = array();
		$non_recurring_refund_totals = array();
		
		// Overview of types sold
		if(!$by_day)
		{
			echo "<br><br>";
			if($view_all=="recent")
			{
				$mts = mktime(0,0,0,date("m")-1,date("d"),date("Y"));
				$mstart_year = date("Y",$mts);
				$mstart_month = date("m",$mts);
				$mstart_day = date("d",$mts);
				output_payment_breakdown($mstart_year,$mstart_month);
			}
			else
			{
				output_payment_breakdown($start_year,$start_month);
			}
			echo "<br><br>";
		}
		
		if($view_all=="yes")
		{
			// Paid License Breakdown
			echo "<table>";
			echo "<tr><td valign='top'>";
			create_monthly_payment_chart("New Silver",$start_year,$start_month,300,900,"#4444aa","#8888aa",$div_less,$by_day);
			echo "</td><td valign='top'>";
			create_monthly_payment_chart("New Gold",$start_year,$start_month,900,1500,"#44aa44","#88aa88",$div_less,$by_day);
			echo "</td><td valign='top'>";
			create_monthly_payment_chart("New Platinum",$start_year,$start_month,1500,60000,"#aa4444","#aa8888",$div_less,$by_day);
			echo "</tr></tr></table>";
			echo "<br><br>";
			
			// Recurring vs Non-Recurring
			echo "<table>";
			echo "<tr><td valign='top'>";
			create_monthly_payment_chart("Recurring (all txn less than or equal to 300)",$start_year,$start_month,0,300,"#4444aa","#8888aa",$div_more,$by_day,$recurring_totals,false,$recurring_refund_totals);
			echo "</td><td>&nbsp;</td><td valign='top'>";
			create_monthly_payment_chart("Non-Recurring (all txn greater than 300)",$start_year,$start_month,300,60000,"#44aa44","#88aa88",$div_less,$by_day,$non_recurring_totals,false,$non_recurring_refund_totals);
			//echo "</td><td>&nbsp;</td><td valign='top'>";
			//create_monthly_payment_chart("Other",2011,8,5000,600000,"#44aaaa","#88aaaa",1);
			if (isset($_GET['payments'])) {
				echo "</td><td>&nbsp;</td><td valign='top'>";
				create_recurring_non_recurring_sum_chart($recurring_totals, $non_recurring_totals, $recurring_refund_totals, $non_recurring_refund_totals);
			}
			echo "</td></tr>";
			echo "</table>";
			
			echo "<br><br>";
			echo "<table>";
			echo "<tr><td valign='top'>";
			create_monthly_payment_chart("Silver",$start_year,$start_month,0,45,"#4444aa","#8888aa",$div_more,$by_day);
			echo "</td><td valign='top'>";
			create_monthly_payment_chart("Gold",$start_year,$start_month,45,90,"#44aa44","#88aa88",$div_more,$by_day);
			echo "</td><td valign='top'>";
			create_monthly_payment_chart("Platinum",$start_year,$start_month,90,300,"#aa4444","#aa8888",$div_more,$by_day);
			echo "</tr></tr></table>";
		
			echo "<br><br>";
		}
		else if($view_all=="day" || $view_all==="recent")
		{
			output_payment_breakdown($start_year,$start_month,$start_day,8);
		}
		else
		{
			echo "<table>";
			echo "<tr><td valign='top'>";
			create_monthly_payment_chart("Recurring (all txn less than 300)",$start_year,$start_month,0,300,"#4444aa","#8888aa",$div_more,$by_day,$recurring_totals,false,$recurring_refund_totals);
			echo "</td><td>&nbsp;</td><td valign='top'>";
			create_monthly_payment_chart("Non-Recurring (all txn greater or equal to 300)",$start_year,$start_month,300,60000,"#44aa44","#88aa88",$div_less,$by_day,$non_recurring_totals,false,$non_recurring_refund_totals);
			
			if (isset($_GET['payments'])) {
				echo "</td><td>&nbsp;</td><td valign='top'>";
				create_recurring_non_recurring_sum_chart($recurring_totals, $non_recurring_totals, $recurring_refund_totals, $non_recurring_refund_totals);
			}
			echo "</td><td>&nbsp;</td><td valign='top'>";
			echo "&nbsp;";
			echo "</td></tr>";
			echo "</table>";
			//echo "<input type='button' value='View All' onclick='window.location = \"index.php?mode=manage_customers&submode=general_tools&tool=monthly_payments&view_all=yes\"' />";
			echo "<br><br>";
		}
		
		echo "<br><br>";
	}
?>