<?php


//error_reporting(E_ALL);
//ini_set('display_errors', '1');
if(can_access("customers")){
require_once(dirname(__FILE__).'/../../../private_html/myinfo.php');
$usr = my_poslavu_username();
$pw = my_poslavu_password();
$hst = my_poslavu_host();	
class DataTransfer{
	public $tablenames = array(
		"menu" => array(
			"forced_modifiers",
			"forced_modifier_groups",
			"forced_modifier_lists",
			"ingredients",
			"ingredient_categories",
			"menus",
			"menu_categories",
			"menu_groups",
			"menu_items",
			"modifiers",
			"modifier_categories",
			"super_groups"
		)
	);
	
	public function backup($data_name,$tables){
		global $usr, $pw, $hst;
		$backup_time = time();
		foreach($tables as $table){
			$output = array();
			$return_var = 1;
			$backupFile = "/home/poslavu/public_html/admin/sa_cp/dbbackup/$data_name.$table.".$backup_time.".sql";
			exec("mysqldump -h  $hst -u $usr -p$pw poslavu_".$data_name."_db $table > $backupFile",$output,$return_var);
			if ($return_var != 0){
				var_dump($output);
				die("<p>Error Backing up $table</p>");
			}else{
				echo "<br/>Dumped $table";
			}
		}
	}
	
	public function copy($from,$to,$tables){
		global $usr, $pw, $hst;
		$this->backup($to,$tables);
		$link = ConnectionHub::getConn('poslavu');
		foreach($tables as $table){
			$link->query('write',"TRUNCATE TABLE `poslavu_".$to."_db`.`$table`") or die("Query Error :".mlavu_dberror());
			$link->query('write',"INSERT INTO `poslavu_".$to."_db`.`$table` SELECT * FROM `poslavu_".$from."_db`.`$table`") or die("Query Error :".mlavu_dberror());
			echo mlavu_dberror();
		}
			$link->query('write',"update `poslavu_".$from."_db`.`locations` f, `poslavu_".$to."_db`.`locations` t set t.menu_id = f.menu_id") or die("<br/>Query Error:".mlavu_dberror());
	}
	
	public function isValidDataName($data_name){
		
	}
}

$dt = new DataTransfer();
if (isset($_REQUEST['to_data_name'])){

	$dt->copy($_REQUEST['from_data_name'],$_REQUEST['to_data_name'],$dt->tablenames['menu']);
}
?>

<form  method="post" action="">
		<div class="form_description">
			<h2>Data Transfer</h2> 
			<p>This tool will transfer the menu and modifiers(not settings or table layout) of one account to another. The old menu of the location being transferred to will be overwritten, but will be backed up first in case of a mistake. This version will probably only work for accounts with only one location.</p>
		</div>						
			<ul >
			
					<li id="li_1" >
		<label class="description" for="element_1">From:</label>
		<div>
			<input id="element_1" name="from_data_name" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>		<li id="li_2" >
		<label class="description" for="element_2">To:</label>
		<div>
			<input id="element_2" name="to_data_name" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>
			
					<li class="buttons">
			    <input type="hidden" name="form_id" value="413482" />
			    
				<input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
		</li>
			</ul>
		</form>
		
		<?php }?>

<button onclick="window.location='?mode=menu_restore';">Restore menu</button>
