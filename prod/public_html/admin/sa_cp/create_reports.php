<?php
	if($loggedin)
	{
		require_once dirname(dirname(__FILE__)) . '/cp/resources/core_functions.php';
		ini_set("display_errors","1");
		//require_once("../cp/resources/lavuquery.php");

		/*function urlvar($var,$def=false)
		{
			return (isset($_GET[$var]))?$_GET[$var]:$def;
		}*/
		require_once("report_functions.php");
		function get_rep_info($reportid)
		{
			$rep_type = "Regular";
			$rep_query = mlavu_query("select * from `reports` where id='[1]'",$reportid);
			if(mysqli_num_rows($rep_query))
			{
				$rep_read = mysqli_fetch_assoc($rep_query);
				$rep_type = $rep_read['report_type'];
				$rep_com_package = $rep_read['component_package'];
			}
			$rep_info = array();
			$rep_info['type'] = $rep_type;
			$rep_info['com_package'] = $rep_com_package;
			return $rep_info;
		}

		$date_mode = "unified";
		$use_rest = "demo_brian_d";//test_pizza_sho";
		//$use_rest= 'lavu_inc';

		$dbname = "poslavu_".$use_rest."_db";
		$maindb = "poslavu_MAIN_db";
		$use_loc_id = 1;
		//$use_loc_id='9';
		mlavu_select_db($maindb);
		$find_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$use_rest);
		if(mysqli_num_rows($find_query))
		{
			$find_read = mysqli_fetch_assoc($find_query);
			$restid = $find_read['id'];
		}
		else
		{
			echo "cant find $use_rest";
			exit();
		}
		//lavu_select_db($maindb);
		lavu_connect_byid($restid,$dbname);
		$dbinfo = get_database_info($dbname);
		$tables = $dbinfo['tables'];
		$fields = $dbinfo['fields'];

		$rurl = "index.php?mode=create_reports";

		$reportid = urlvar("reportid");
		$rmode = urlvar("rmode");
		if($reportid)
		{
			if(isset($_POST['report_data']))
			{
				$setvars = array();
				$rdata = explode("&",$_POST['report_data']);
				for($i=0; $i<count($rdata); $i++)
				{
					$ritem = explode("=",$rdata[$i]);
					if(count($ritem) > 1 && trim($ritem[0])!="")
					{
						$setvars[trim($ritem[0])] = trim($ritem[1]);
					}
				}
				/*echo "posted: <br>";
				foreach($setvars as $key => $val)
				{
					echo "<br>" . $key . " = " . $val;
				}*/
				$updatets = date("YmdHis");

				mlavu_query("delete from `report_parts` where `reportid`='$reportid' and `type`='join' order by `_order` asc");
				$join_count = $setvars['join_count'];
				for($i=0; $i<$join_count; $i++) {
					$from_table = $setvars['join_table_from_' . ($i + 1)];
					$from_field = $setvars['join_field_from_' . ($i + 1)];
					$to_table = $setvars['join_table_to_' . ($i + 1)];
					$to_field = $setvars['join_field_to_' . ($i + 1)];
					//131 = Server Sales w/tips, temporary fix to prevent duplicate payment aggregation
					if ($reportid == 131 && $to_table == 'alternate_payment_totals') {
						$subSelectPartOne = "(Select `order_id`, sum(`tip`) AS `tip` from ";
						$subSelectPartTwo = "`alternate_payment_totals` group by `order_id`)";
						mlavu_query("insert into `report_parts` (`reportid`,`type`,`tablename`,`fieldname`,`prop1`,`prop2`, `prop3`, `prop4`) values ('$reportid','join','$from_table','$from_field','$to_table','$to_field','$subSelectPartOne','$subSelectPartTwo')");

					} else if (($reportid == 134 || $reportid == 2) && $to_table == 'order_contents') {
						$subSelectPartOne = "(Select `order_id`, sum(`subtotal_with_mods`) AS `subtotal_with_mods`, sum(`itax`) as `itax` from ";
						$subSelectPartTwo = "`order_contents` where `void` = 0 group by `order_id` )";
						mlavu_query("insert into `report_parts` (`reportid`,`type`,`tablename`,`fieldname`,`prop1`,`prop2`, `prop3`, `prop4`) values ('$reportid','join','$from_table','$from_field','$to_table','$to_field','$subSelectPartOne','$subSelectPartTwo')");

					} else {
						mlavu_query("insert into `report_parts` (`reportid`,`type`,`tablename`,`fieldname`,`prop1`,`prop2`) values ('$reportid','join','$from_table','$from_field','$to_table','$to_field')");
					}
				}

				$select_count = $setvars['select_count'];
				for($i=0; $i<$select_count; $i++)
				{
					$sel_table = $setvars['select_table_'.($i + 1)];
					$sel_field = $setvars['select_field_'.($i + 1)];

					$ex_query = mlavu_query("select * from `report_parts` where `reportid`='$reportid' and `type`='select' and `tablename`='$sel_table' and `fieldname`='$sel_field'");
					if(mysqli_num_rows($ex_query))
					{
						$ex_read = mysqli_fetch_assoc($ex_query);
						$rpartid = $ex_read['id'];
						mlavu_query("update `report_parts` set `updatets`='$updatets' where id='$rpartid'");
					}
					else
					{
						mlavu_query("insert into `report_parts` (`reportid`,`type`,`tablename`,`fieldname`,`updatets`) values ('$reportid','select','$sel_table','$sel_field','$updatets')");
					}
				}
				mlavu_query("delete from `report_parts` where `reportid`='$reportid' and `type`='select' and `updatets`!='$updatets'");
				echo "<script language='javascript'>window.location.replace('$rurl&reportid=$reportid&rmode=details');</script>";
			}
			else if($rmode=="details")
			{
				if(isset($_POST['posted']))
				{
					$report_title = (isset($_POST['report_title']))?$_POST['report_title']:"";
					$radio_date = (isset($_POST['radio_date']))?$_POST['radio_date']:"";
					$radio_locid = (isset($_POST['radio_locid']))?$_POST['radio_locid']:"";
					$radio_group = (isset($_POST['radio_group']))?$_POST['radio_group']:"";
					$orderby = (isset($_POST['orderby']))?$_POST['orderby']:"";
					$set_group_operation = (isset($_POST['group_operation']))?$_POST['group_operation']:"";
					$set_order_operation = (isset($_POST['order_operation']))?$_POST['order_operation']:"";
					$date_select_type = (isset($_POST['date_select_type']))?$_POST['date_select_type']:"";
					$set_repeat_foreach = (isset($_POST['repeat_foreach']))?$_POST['repeat_foreach']:"";
					$set_extra_functions = (isset($_POST['extra_functions']))?$_POST['extra_functions']:"";
					$set_unified = (isset($_POST['unified']))?$_POST['unified']:"";

					$vars = array();
					$vars['report_title'] = $report_title;
					$vars['radio_date'] = $radio_date;
					$vars['radio_locid'] =$radio_locid;
					$vars['radio_group'] = $radio_group;
					$vars['orderby'] = $orderby;
					$vars['set_group_operation'] = $set_group_operation;
					$vars['set_order_operation'] = $set_order_operation;
					$vars['date_select_type'] = $date_select_type;
					$vars['repeat_foreach'] = $set_repeat_foreach;
					$vars['extra_functions'] = $set_extra_functions;
					$vars['set_unified'] = $set_unified;
					$vars['reportid'] = $reportid;

					mlavu_query("update `reports` set `title`='[report_title]', `dateid`='[radio_date]', `locid`='[radio_locid]', `groupid`='[radio_group]', `orderby`='[orderby]', `group_operation`='[set_group_operation]', `order_operation`='[set_order_operation]', `date_select_type`='[date_select_type]', `repeat_foreach`='[repeat_foreach]', `extra_functions`='[extra_functions]', `unified`='[set_unified]' where `id`='[reportid]'",$vars);
					$rep_query = mlavu_query("select * from `report_parts` where `reportid`='[1]' and `type`='select' order by `tablename` asc",$reportid);
					while($rep_read = mysqli_fetch_assoc($rep_query))
					{
						$set_table = $rep_read['tablename'];
						$set_field = $rep_read['fieldname'];
						$set_prop1 = $rep_read['prop1'];
						$set_prop2 = $rep_read['prop2'];
						$set_type = $rep_read['type'];
						$partid = $rep_read['id'];

						$set_operation = $_POST['operation_'.$partid];
						$set_opargs = str_replace("'","''",$_POST['opargs_'.$partid]);
						$set_filter = str_replace("'","''",$_POST['filter_'.$partid]);
						$set_label = str_replace("'","''",$_POST['label_'.$partid]);
						$set_order = str_replace("'","''",$_POST['_order_'.$partid]);
						$set_hide = str_replace("'","''",$_POST['hide_'.$partid]);
						if(isset($_POST['monetary_'.$partid])) $set_monetary = "1";
						else $set_monetary = "0";
						if(isset($_POST['sum_'.$partid])) $set_sum = "1";
						else $set_sum = "0";
						if(isset($_POST['graph_'.$partid])) $set_graph = "1";
						else $set_graph = "0";
						if(isset($_POST['branch_'.$partid])) $set_branch = "1";
						else $set_branch = "0";
						$set_align = $_POST['align_'.$partid];

						mlavu_query("update `report_parts` set `operation`='$set_operation', `opargs`='$set_opargs', `filter`='$set_filter', `hide`='$set_hide', `monetary`='$set_monetary', `sum`='$set_sum', `graph`='$set_graph', `branch`='$set_branch', `align`='$set_align', `label`='$set_label', `_order`='$set_order' where id='$partid'");
					}

					echo "<script language='javascript'>window.location.replace('$rurl&reportid=$reportid&rmode=view');</script>";
					//echo "<br><br><input type='button' value='View Report' onclick='window.location = \"$rurl&reportid=$reportid&rmode=view\"'>";
				}
				else
				{
					echo "<br><br><a href='$rurl'>(View All Reports)</a>";
					echo "<br><br>";

					$report_query = mlavu_query("select * from `reports` where id='$reportid'");
					if(mysqli_num_rows($report_query))
					{
						$report_read = mysqli_fetch_assoc($report_query);
						$groupid = $report_read['groupid'];
						$dateid = $report_read['dateid'];
						$locid = $report_read['locid'];
						$orderby = $report_read['orderby'];
						$set_group_operation = $report_read['group_operation'];
						$set_order_operation = $report_read['order_operation'];
						$date_select_type = $report_read['date_select_type'];
						$set_repeat_foreach = $report_read['repeat_foreach'];
						$set_extra_functions = $report_read['extra_functions'];
						$set_unified = $report_read['unified'];

						echo "Report: " . $report_read['title'];
						echo "<br><a href='$rurl&reportid=$reportid'><< Fields</a> <b>(Details)</b><br><br>";
					}

					echo "<script language='javascript'>";
					echo "function clearRadio(btngroup) { ";
					echo "  for(x=0; x<btngroup.length; x++) { ";
					echo "     btngroup[x].checked = false; ";
					echo "  } ";
					echo "} ";
					echo "</script>";

					echo "<form name='form1' method='post' action='$rurl&reportid=$reportid&rmode=details'>";
					echo "<input type='text' name='report_title' value=\"".str_replace("\"","&quot;",$report_read['title'])."\">";
					echo "<input type='hidden' name='posted' value='1'>";
					echo "<table cellpadding=4>";
					echo "<tr>";
					echo "<td align='center'>Order</td>";
					echo "<td align='center'>Display</td>";
					echo "<td align='center'>Date</td>";
					echo "<td align='center'>LocationId</td>";
					echo "<td align='center'>Group</td>";
					echo "<td align='center'>Select Operation</td>";
					echo "<td></td><td></td>";
					echo "<td align='center'>Label</td>";
					echo "<td align='center'>Where Filter</td>";
					echo "<td align='center'>Asc</td>";
					echo "<td align='center'>Desc</td>";
					echo "<td align='center'>Money</td>";
					echo "<td align='center'>Sum</td>";
					echo "<td align='center'>Graph</td>";
					echo "<td align='center'>Branch</td>";
					echo "<td align='center'>Alignment</td>";
					echo "</tr>";
					$rep_query = mlavu_query("select * from `report_parts` where `reportid`='$reportid' and `type`='select' order by `_order` asc, `tablename` asc");
					while($rep_read = mysqli_fetch_assoc($rep_query))
					{
						$set_table = $rep_read['tablename'];
						$set_field = $rep_read['fieldname'];
						$set_prop1 = $rep_read['prop1'];
						$set_prop2 = $rep_read['prop2'];
						$set_type = $rep_read['type'];
						$set_operation = $rep_read['operation'];
						$set_opargs = $rep_read['opargs'];
						$set_filter = $rep_read['filter'];
						$set_label = $rep_read['label'];
						$set_monetary = $rep_read['monetary'];
						$set_sum = $rep_read['sum'];
						$set_graph = $rep_read['graph'];
						$set_branch = $rep_read['branch'];
						$partid = $rep_read['id'];
						$_order = $rep_read['_order'];
						$hide = $rep_read['hide'];
						$set_align = $rep_read['align'];

						echo "<tr>";
						echo "<td><input type='text' name='_order_$partid' value='$_order' size='2'></td>";
						echo "<td><input type='checkbox' name='display_$partid' onchange='if(this.checked) document.form1.hide_$partid.value = \"0\"; else document.form1.hide_$partid.value = \"1\"'";
						if($hide!="1")
						{
							echo " checked";
						}
						echo "><input type='hidden' name='hide_$partid' value='$hide'></td>";
						echo "<td align='center'><input type='radio' name='radio_date' value='$partid'".(($partid==$dateid)?" checked":"")."></td>";
						echo "<td align='center'><input type='radio' name='radio_locid' value='$partid'".(($partid==$locid)?" checked":"")."></td>";
						echo "<td align='center'><input type='radio' name='radio_group' value='$partid'".(($partid==$groupid)?" checked":"")."></td>";
						echo "<td>";
						/*echo "<select name='operation_$partid'>";
						$options = array("","sum","count","avg","format","left","right","custom");
						for($i=0; $i<count($options); $i++)
						{
							echo "<option value='".$options[$i]."'";
							if($set_operation==$options[$i])
								echo " selected";
							echo ">".$options[$i]."</option>";
						}
						echo "</select>";*/
						echo "<input type='hidden' name='operation_$partid' value='custom'>";
						echo "<input type='text' name='opargs_$partid' value=\"".str_replace("\"","&quot;",$set_opargs)."\" size='32'>";
						echo "</td>";
						echo "<td style='font-size:10px; color:#aaaaaa' align='right'>" . $set_table . "</td>";
						echo "<td>" . $set_field . "</td>";
						echo "<td><input type='text' name='label_$partid' value=\"".str_replace("\"","&quot;",$set_label)."\" size='8'></td>";
						echo "<td><input type='text' name='filter_$partid' value=\"".str_replace("\"","&quot;",$set_filter)."\" size='16'></td>";
						echo "<td>";
						echo "<input type='radio' name='orderby' value='$partid asc'";
						if($orderby=="$partid asc")
							echo " checked";
						echo ">";
						echo "</td><td>";
						echo "<input type='radio' name='orderby' value='$partid desc'";
						if($orderby=="$partid desc")
							echo " checked";
						echo ">";
						echo "</td>";
						echo "<td align='center'>";
						echo "<input type='checkbox' name='monetary_$partid'";
						if($set_monetary=="1") echo " checked";
						echo ">";
						echo "</td>";
						echo "<td align='center'>";
						echo "<input type='checkbox' name='sum_$partid'";
						if($set_sum=="1") echo " checked";
						echo ">";
						echo "</td>";
						echo "<td align='center'>";
						echo "<input type='checkbox' name='graph_$partid'";
						if($set_graph=="1") echo " checked";
						echo ">";
						echo "</td>";
						echo "<td align='center'>";
						echo "<input type='checkbox' name='branch_$partid'";
						if($set_branch=="1") echo " checked";
						echo ">";
						echo "</td>";
						echo "<td align='center'>";
						echo "<select name='align_$partid'>";
						echo "<option value='left'";
						if($set_align=="left") echo " selected";
						echo ">Left</option>";
						echo "<option value='center'";
						if($set_align=="center") echo " selected";
						echo ">Center</option>";
						echo "<option value='right'";
						if($set_align=="right") echo " selected";
						echo ">Right</option>";
						echo "</select>";
						echo "</td>";
						echo "<td style='color:#bbbbbb'>" . $rep_read['id'] . "</td>";
						echo "</tr>";
					}
					echo "<tr>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td><input type='button' style='font-size:9px' onclick='clearRadio(document.form1.radio_date)' value='clear'></td>";
					echo "<td><input type='button' style='font-size:9px' onclick='clearRadio(document.form1.radio_locid)' value='clear'></td>";
					echo "<td><input type='button' style='font-size:9px' onclick='clearRadio(document.form1.radio_group)' value='clear'></td>";
					echo "</tr>";
					echo "<tr><td colspan='5' align='right'>Group Operation</td>";
					echo "<td><input type='text' name='group_operation' value=\"".str_replace("\"","&quot;",$set_group_operation)."\" size='32'></td>";
					echo "</tr>";
					echo "<tr><td colspan='5' align='right'>Order Operation</td>";
					echo "<td><input type='text' name='order_operation' value=\"".str_replace("\"","&quot;",$set_order_operation)."\" size='32'></td>";
					echo "<tr><td colspan='5' align='right'>Repeat For Each</td>";
					echo "<td><input type='text' name='repeat_foreach' value=\"".str_replace("\"","&quot;",$set_repeat_foreach)."\" size='32'></td>";
					echo "<td></td></tr>";

					echo "<tr><td colspan='5' align='right'>Extra Functions</td>";
					echo "<td><input type='text' name='extra_functions' value=\"".str_replace("\"","&quot;",$set_extra_functions)."\" size='32'></td>";
					echo "<td></td></tr>";

					echo "<tr><td colspan='5' align='right'>Date Select Type</td>";
					echo "<td><select name='date_select_type'>";
					$date_selectlist = array("","Day","Day Range","Month","Year");
					for($d=0; $d<count($date_selectlist); $d++)
						echo "<option value='".$date_selectlist[$d]."'".(($date_selectlist[$d]==$date_select_type)?" selected":"").">".$date_selectlist[$d]."</option>";
					echo "</select></td>";
					echo "<td></td></tr>";
					echo "<tr><td colspan='5' align='right'>Date Unified</td>";
					echo "<td><select name='unified'>";
					$date_selectlist = array("","Yes","No");
					for($d=0; $d<count($date_selectlist); $d++)
						echo "<option value='".$date_selectlist[$d]."'".(($date_selectlist[$d]==$set_unified)?" selected":"").">".$date_selectlist[$d]."</option>";
					echo "</select></td>";
					echo "<td></td></tr>";
					echo "</table>";
					echo "<input type='submit' value='Submit'>";
					echo "</form>";
					//echo "<br><br><input type='button' value='View Report' onclick='window.location = \"$rurl&reportid=$reportid&rmode=view\"'>";
				}
			}
			else if($rmode=="require_modules")
			{
				mlavu_query("UPDATE `reports` SET `required_modules`='".$_GET['required_modules']."' WHERE `id`='".$reportid."'");
				echo "<script type='text/javascript'>window.history.back();</script>";
			}
			else if($rmode=="view")
			{
				echo "<a href='$rurl'>(View All Reports)</a>";
				echo "<br><br>";

				//function sessvar_isset($var)
				//{
				//	return false;
				//}
				$rep_info = get_rep_info($reportid);
				require_once("show_report.php");
				if($rep_info['type']=="Combo")
				{
					echo "<a href='$rurl&reportid=$reportid'><< Sub Reports</a> <b>(View)</b><br><br>";
					show_report($dbname,$maindb,$date_mode,$rurl,$reportid,true,$use_loc_id);
				}
				else
				{
					echo "<a href='$rurl&reportid=$reportid'><< Fields</a> - <a href='$rurl&reportid=$reportid&rmode=details'><< Details</a> <b>(View)</b><br><br>";
					show_report($dbname,$maindb,$date_mode,$rurl,$reportid,true,$use_loc_id);
				}
				//echo "<br><br><br><br>".$query;
			}
			else
			{
				$rep_info = get_rep_info($reportid);
				$rep_type = $rep_info['type'];
				$rep_com_package = $rep_info['com_package'];

				if($rep_type=="Combo")
				{
					//------------------- FOR COMBO REPORTS -----------------------//
					echo "<a href='$rurl'>(View All Reports)</a>";
					echo "<br><br>";
					echo "<table>";
					echo "<td>Report: <b>" . $rep_read['title'] . "</b></td><td width='40'>&nbsp;</td>";
					echo "</table>";

					if(isset($_GET['add_sub_report']))
					{
						mlavu_query("insert into `$maindb`.`report_parts` (`reportid`,`type`,`prop1`) values ('[1]','report','[2]')",$reportid,$_GET['add_sub_report']);
					}
					else if(isset($_GET['remove_part']))
					{
						mlavu_query("delete from `$maindb`.`report_parts` where `id`='[1]'",$_GET['remove_part']);
					}
					$change_partid = "";
					if(isset($_GET['change_part']))
					{
						$change_partid = $_GET['change_part'];
						if(isset($_GET['change_part_to']))
						{
							$change_part_to = $_GET['change_part_to'];
							//echo "Change $change_partid to $change_part_to<br>";
							$part_query = mlavu_query("select * from `report_parts` where `id`='[1]'",$change_partid);
							if(mysqli_num_rows($part_query))
							{
								$part_read = mysqli_fetch_assoc($part_query);
								echo $part_read['id'] . ":" . $part_read['prop1'] . "<br>";
								mlavu_query("update `report_parts` set `prop1`='[1]' where `id`='[2]' limit 1",$change_part_to,$change_partid);
							}
							$change_partid = "";
						}
					}
					echo "<table>";
					$rep_query = mlavu_query("select `reports`.`id` as `repid`,`report_parts`.`prop1` as `prop1`, `report_parts`.`id` as `id`, `reports`.`title` as `title` from `$maindb`.`report_parts` LEFT JOIN `$maindb`.`reports` ON `report_parts`.`prop1`=`reports`.`id` where `report_parts`.`reportid`='$reportid' and `report_parts`.`type`='report' order by `_order` asc, id asc");
					while($rep_read = mysqli_fetch_assoc($rep_query))
					{
						echo "<tr>";
						if($rep_read['id']==$change_partid)
						{
							echo "<td>";//change: " . $rep_read['title'] . "</td>";
							echo "<select onchange='window.location = \"index.php?mode=create_reports&reportid=$reportid&change_part=".$rep_read['id']."&change_part_to=\" + this.value'>";
							if(isset($_GET['tt']))
								$crep_query = mlavu_query("select * from `$maindb`.`reports` where `report_type`!='Combo'");
							else $crep_query = mlavu_query("select * from `$maindb`.`reports` where `report_type`!='Combo' and (`component_package`='0' or `component_package`='[1]')",$rep_com_package);
							while($crep_read = mysqli_fetch_assoc($crep_query))
							{
								echo "<option value='".$crep_read['id']."'";
								if($rep_read['repid']==$crep_read['id']) echo " selected";
								echo ">".$crep_read['title']."</option>";
							}
							echo "</select>";
							echo "</td>";
							echo "<td>&nbsp;</td>";
						}
						else
						{
							echo "<td>" . $rep_read['title'] . "</td>";
							echo "<td><a href='index.php?mode=create_reports&reportid=$reportid&change_part=".$rep_read['id']."' style='color:#008800'>(change)</a></td>";
						}
						echo "<td><a href='index.php?mode=create_reports&reportid=$reportid&remove_part=".$rep_read['id']."' style='color:#880000'>(remove)</a></td>";
						echo "</tr>";
					}
					echo "<tr><td>";
					echo "<select name='add_report' onchange='window.location = \"index.php?mode=create_reports&reportid=$reportid&add_sub_report=\" + this.value'>";
					if(isset($_GET['tt']))
						$rep_query = mlavu_query("select * from `$maindb`.`reports` where `report_type`!='Combo'");
					else $rep_query = mlavu_query("select * from `$maindb`.`reports` where `report_type`!='Combo' and (`component_package`='0' or `component_package`='[1]')",$rep_com_package);
					echo "<option value=''>Add New Sub-Report</option>";
					while($rep_read = mysqli_fetch_assoc($rep_query))
					{
						echo "<option value='".$rep_read['id']."'>".$rep_read['title']."</option>";
					}
					echo "</select>";
					echo "</td></tr>";
					echo "</table>";
					echo "<br><br><input type='button' value='View Combo Report' onclick='window.location = \"index.php?mode=create_reports&reportid=$reportid&rmode=view\"' />";
				}
				else
				{
					//--------------------- FOR NORMAL REPORTS ----------------------//
					$init_script = "";
					$rep_query = mlavu_query("select * from `$maindb`.`report_parts` where `reportid`='$reportid' order by `_order` asc, id asc");
					while($rep_read = mysqli_fetch_assoc($rep_query))
					{
						$set_table = $rep_read['tablename'];
						$set_fieldname = $rep_read['fieldname'];
						$set_prop1 = $rep_read['prop1'];
						$set_prop2 = $rep_read['prop2'];
						$set_type = $rep_read['type'];
						if($set_type=="join")
						{
							$init_script .= "focus_table = \"$set_table\";\n";
							$init_script .= "focus_field = \"$set_fieldname\";\n";
							$init_script .= "field_join('$set_prop1','$set_prop2');\n";
						}
						else if($set_type=="select")
						{
							$init_script .= "field_select('$set_table','$set_fieldname');\n";
						}
					}

					$new_table = urlvar("new_table");
					$remove_table = urlvar("remove_table");
					if($new_table || $remove_table)
					{
						$rep_query = mlavu_query("select * from `reports` where id='[1]'",$reportid);
						if(mysqli_num_rows($rep_query))
						{
							$rep_read = mysqli_fetch_assoc($rep_query);

							$tlist = $rep_read['tables'];
							$table_in = (strpos($tlist,"|".$new_table."|")!==false);
							if($table_in && $remove_table)
							{
								$tlist = str_replace("|" . $remove_table . "|","",$tlist);
							}
							else if(!$table_in && $new_table)
							{
								$tlist .= "|" . $new_table . "|";
							}
							mlavu_query("update `reports` set `tables`='[1]' where id='[2]'",$tlist,$reportid);
						}
					}

					$rep_query = mlavu_query("select * from `reports` where id='[1]'",$reportid);
					if(mysqli_num_rows($rep_query))
					{
						$rep_read = mysqli_fetch_assoc($rep_query);

						echo "<a href='$rurl'>(View All Reports)</a>";
						echo "<br><br>";
						echo "<table>";
						echo "<td>Report: <b>" . $rep_read['title'] . "</b></td><td width='40'>&nbsp;</td>";
						echo "<td id='mode_join' style='border-bottom:solid 1px black; cursor:pointer;' onclick='set_rmode(\"join\")'>Join</td><td width='16'>&nbsp;</td>";
						echo "<td id='mode_select' style='cursor:pointer;' onclick='set_rmode(\"select\")'>Select</td><td width='16'>&nbsp;</td>";
						//echo "<td id='mode_group' style='cursor:pointer;' onclick='set_rmode(\"group\")'>Group</td><td width='16'>&nbsp;</td>";
						echo "<td width='24'>&nbsp;</td>";
						echo "<td><input type='button' value='Submit' onclick='submit_rdata()'></td>";
						echo "</table>";
						echo "<br><br>";

						$report_tables = array();
						$tlist = explode("|",$rep_read['tables']);
						for($i=0; $i<count($tlist); $i++)
							if($tlist[$i]!="")
								$report_tables[] = $tlist[$i];

						echo "<table style='border:solid 1px #555555'>";
						for($i=0; $i<count($report_tables); $i++)
						{
							$tablename = $report_tables[$i];
							echo "<td valign='top'>";
							echo "<table><td width='200' bgcolor='#dddddd' align='center'>";
							echo $tablename;
							echo "&nbsp;&nbsp;";
							echo "<a style='cursor:pointer' onclick='if(confirm(\"Remove $tablename?\")) window.location = \"$rurl&reportid=$reportid&remove_table=$tablename\"'><font color='#bb0000' style='font-size:10px'>(x)</font></a>";
							echo "</td></table>";
							$field_names = $dbinfo['fields'][$tablename][0];
							$field_types = $dbinfo['fields'][$tablename][1];

							echo "<table>";
							for($n=0; $n<count($field_names); $n++)
							{
								$fieldname = $field_names[$n];
								$fieldid = $tablename . "_" . $fieldname;
								$statusid = $fieldid . "_status";
								echo "<tr>";
								echo "<td id='$statusid'>&nbsp;</td>";
								echo "<td id='$fieldid' style='cursor:pointer' onselectstart='return false;' onmousedown='field_mousedown(\"$tablename\",\"$fieldname\"); return false;' onmouseup='field_mouseup(\"$tablename\",\"$fieldname\"); return false;' onclick='field_click(\"$tablename\",\"$fieldname\")'>";
								echo $fieldname;
								echo "</td>";
								echo "</tr>";
							}
							echo "</table>";

							echo "</td>";
						}
						echo "<td valign='top'>";
						echo "<table><td width='200' bgcolor='#dddddd'>&nbsp;</td></table>";
						echo "<br>Add New Table:";
						echo "<br>";
						echo "<select name='new_table' onchange='window.location = \"$rurl&reportid=$reportid&new_table=\" + this.value'>";
						echo "<option value=''></option>";
						for($i=0; $i<count($tables); $i++)
						{
							echo "<option value='".$tables[$i]."'>" . $tables[$i] . "</option>";
						}
						echo "</select>";
						echo "</td>";
						echo "</table>";
						?>

						<form name='send_data' method='post' action='<?php echo $rurl?>&reportid=<?php echo $reportid?>'>
							<input type='hidden' name='report_data' />
						</form>

						<script language="javascript">
							fmode = "join";
							focus_table = "";
							focus_field = "";
							colors = new Array("#008800","#0000ff","#ff0000","#ff00ff","#ffff00","#00ffff","#007700","#000077","#770000","#007777");
							joins = new Array();
							selects = new Array();

							function field_click(tname,fname)
							{
							}
							function field_mousedown(tname,fname)
							{
								focus_table = tname;
								focus_field = fname;
							}
							function field_mouseup(tname,fname)
							{
								if(fmode=="join")
								{
									field_join(tname,fname);
								}
								else if(fmode=="select")
								{
									field_select(tname,fname);
								}
								else if(fmode=="group")
								{
									field_group(tname,fname);
								}
							}
							function field_select(tname,fname)
							{
								fieldid = tname + "_" + fname;
								current_val = document.getElementById(fieldid).bgColor;
								if(current_val=="#aabbff" || current_val=="#AABBFF" || current_val=="aabbff" || current_val=="AABBFF")
								{
									set_val = "#ffffff";
									saction = "remove";
								}
								else
								{
									set_val = "#aabbff";
									saction = "add";
								}

								sfound = false;
								old_selects = selects;
								selects = new Array();
								for(n=0; n<old_selects.length; n++)
								{
									if(old_selects[n][0]==tname && old_selects[n][1]==fname)
									{
										sfound = true;
										if(saction!="remove")
											selects.push(old_selects[n]);
									}
									else selects.push(old_selects[n]);
								}
								if(!sfound)
								{
									if(saction=="add")
										selects.push(new Array(tname,fname));
								}
								document.getElementById(fieldid).bgColor = set_val;
							}
							function submit_rdata()
							{
								setstr = "reportid=<?php echo $reportid?>";
								join_count = 0;
								joinstr = "";
								for(n=0; n<joins.length; n++)
								{
									if(joins[n][0]!="")
									{
										join_count++;
										joinstr += "&join_table_from_" + join_count + "=" + joins[(join_count * 1 - 1)][0];
										joinstr += "&join_field_from_" + join_count + "=" + joins[(join_count * 1 - 1)][1];
										joinstr += "&join_table_to_" + join_count + "=" + joins[(join_count * 1 - 1)][2];
										joinstr += "&join_field_to_" + join_count + "=" + joins[(join_count * 1 - 1)][3];
									}
								}
								setstr += "&join_count=" + join_count;
								setstr += joinstr;

								setstr += "&select_count=" + selects.length;
								for(n=0; n<selects.length; n++)
								{
									setstr += "&select_table_" + (n * 1 + 1) + "=" + selects[n][0];
									setstr += "&select_field_" + (n * 1 + 1) + "=" + selects[n][1];
								}
								document.send_data.report_data.value = setstr;
								document.send_data.submit();
							}
							function field_group(tname,fname)
							{
							}
							function field_join(tname,fname)
							{
								if(tname!=focus_table)
								{
									new_join = new Array(focus_table,focus_field,tname,fname);
									join_index = 0;
									found = false;
									for(i=0; i<joins.length; i++)
									{
										if(joins[i][0]=="" && !found)
										{
											join_index = i;
											joins[i] = new_join;
											found = true;
										}
									}
									if(!found)
									{
										joins.push(new_join);
										join_index = joins.length - 1;
									}

									fromid = focus_table + "_" + focus_field + "_status";
									toid = tname + "_" + fname + "_status";

									bgcolor = colors[join_index];
									boxcode = "<table><td bgcolor='" + bgcolor + "' width='16' height='16' style='color:#ffffff; cursor:pointer' onclick='if(confirm(\"Remove Join?\")) remove_join(\"" + join_index + "\")'>join</td></table>";

									document.getElementById(fromid).innerHTML = boxcode;
									document.getElementById(toid).innerHTML = boxcode;
									//alert("link " + focus_table + "-" + focus_field + " to " + tname + "-" + fname);
								}
							}
							function remove_join(join_index)
							{
								jfrom_table = joins[join_index][0];
								jfrom_field = joins[join_index][1];
								jto_table = joins[join_index][2];
								jto_field = joins[join_index][3];

								fromid = jfrom_table + "_" + jfrom_field + "_status";
								toid = jto_table + "_" + jto_field + "_status";

								document.getElementById(fromid).innerHTML = "&nbsp;";
								document.getElementById(toid).innerHTML = "&nbsp;";
								joins[join_index] = new Array("","","","");
							}
							function set_rmode(setmode)
							{
								fmode = setmode;
								modes = new Array("join","select");//,"group");
								for(i=0; i<modes.length; i++)
								{
									if(fmode==modes[i]) setstyle = "solid 1px black";
									else setstyle = "none";

									document.getElementById("mode_" + modes[i]).style.borderBottom = setstyle;
								}
							}
							<?php echo $init_script; ?>
						</script>

						<?php
					}
				}
			}
		}
		else
		{
			$new_report = urlvar("new_report");
			$new_report_type = urlvar("new_report_type");
			$set_display = urlvar("set_display");
			$set_displayid = urlvar("set_displayid");
			$copy_report = urlvar("ccopy_report");
			$comp_package = urlvar("comp_package");
			$set_categoryid = urlvar("set_categoryid");
			$set_reportid = urlvar("set_reportid");

			if($new_report)
			{
				mlavu_query("insert into `reports` (`title`,`component_package`,`report_type`) values ('[1]','[2]','[3]')",$new_report,$comp_package,$new_report_type);
			}
			if($set_displayid)
			{
				mlavu_query("update `reports` set `display`='[1]' where id='[2]'",$set_display,$set_displayid);
			}
			if($set_categoryid)
			{
				mlavu_query("update `reports` set `categoryid`='[1]' where id='[2]'",$set_categoryid,$set_reportid);
			}
			if($copy_report)
			{
				$cp_query = mlavu_query("select * from `reports` where `id`='[1]'",$copy_report);
				if(mysqli_num_rows($cp_query))
				{
					$cp_read = mysqli_fetch_assoc($cp_query);
					echo "Copying Report " . $cp_read['title'] . "<br>";
					$vals = $cp_read;
					$vals['title'] .= " copy";
					$vals['display'] = "";

					mlavu_query("insert into `reports` (`title`,`tables`,`dateid`,`timeid`,`groupid`,`group_operation`,`orderby`,`order_operation`,`date_select_type`,`display`,`report_type`,`component_package`) values ('[title]','[tables]','[dateid]','[timeid]','[groupid]','[group_operation]','[orderby]','[order_operation]','[date_select_type]','[display]','[report_type]','[component_package]')",$vals);
					$part_query = mlavu_query("select * from `report_parts` where `reportid`='[1]'",$copy_report);
					$new_reportid = mlavu_insert_id();
					while($part_read = mysqli_fetch_assoc($part_query))
					{
						$vals = $part_read;
						$vals['reportid'] = $new_reportid;
						mlavu_query("insert into `report_parts` (`type`,`prop1`,`prop2`,`prop3`,`prop4`,`tablename`,`fieldname`,`operation`,`opargs`,`filter`,`hide`, `monetary`,`sum`,`graph`,`updatets`,`label`,`branch`,`align`,`reportid`,`_order`) values ('[type]','[prop1]','[prop2]','[prop3]','[prop4]','[tablename]','[fieldname]','[operation]','[opargs]','[filter]','[hide]','[monetary]','[sum]','[graph]','[updatets]','[label]','[branch]','[align]','[reportid]','[_order]')",$vals);
					}
				}
			}

			$rcat_list = array();
			$rcat_query = mlavu_query("select * from `poslavu_MAIN_db`.`report_categories` order by `id` asc");
			while($rcat_read = mysqli_fetch_assoc($rcat_query))
			{
				$rcat_list[] = $rcat_read;
			}

			$last_comp_package = "";
			echo "<table cellpadding=4 style='border:solid 1px black'>";
			$rep_query = mlavu_query("select * from `reports` order by `component_package` asc, `categoryid` asc, `title` asc");
			while($rep_read = mysqli_fetch_assoc($rep_query))
			{
				$comp_package_list = explode(",", $rep_read['component_package']);
				$comp_package = $comp_package_list[0];
				if($comp_package!=$last_comp_package)
				{
					echo "<tr><td colspan='8' bgcolor='#888888' style='color:white; font-weight:bold' align='center'>";
					if($comp_package==0)
						echo "Universal Reports";
					else
					{
						$cquery = mlavu_query("select * from `component_packages` where id='[1]'",$comp_package);
						if(mysqli_num_rows($cquery))
						{
							$cread = mysqli_fetch_assoc($cquery);
							echo $cread['title'] . " Reports";
						}
					}
					echo "</td></tr>";
					$last_comp_package = $comp_package;
				}
				echo "<tr><td width='240' onmouseover='this.bgColor = \"#ccddee\"' onmouseout='this.bgColor = \"#ffffff\"' onclick='window.location = \"$rurl&reportid=".$rep_read['id']."\"'>" . $rep_read['title'] . "</td>";
				echo "<td>";
				echo "<select onchange='window.location = \"$rurl&set_displayid=".$rep_read['id']."&set_display=\" + this.value'>";
				echo "<option value='0'></option>";
				echo "<option value='1'".(($rep_read['display']=="1")?" selected":false).">Display</option>";
				echo "</select>";
				echo "</td>";
				echo "<td>";

				echo "<select onchange='window.location = \"$rurl&set_reportid=".$rep_read['id']."&set_categoryid=\" + this.value'>";
				echo "<option value=''></option>";
				for($z=0; $z<count($rcat_list); $z++)
				{
					echo "<option value='".$rcat_list[$z]['id']."'";
					if($rcat_list[$z]['id']==$rep_read['categoryid']) echo " selected";
					echo ">".$rcat_list[$z]['title']."</option>";
				}
				echo "</select>";
				echo "</td>";

				$required_modules_id = "required_modules_".$rep_read['id'];
				echo "<td style='background-color:#eee;'><input type='text' size='25' value='".$rep_read['required_modules']."' id='$required_modules_id'><input type='button' value='Set Modules' onclick='window.location=\"$rurl&reportid=".$rep_read['id']."&rmode=require_modules&required_modules=\"+encodeURIComponent(document.getElementById(\"$required_modules_id\").value)'>";
				echo "<td><a style='cursor:pointer' onclick='if(confirm(\"Copy?\")) window.location = \"$rurl&ccopy_report=".$rep_read['id']."\"'>Copy</a></td>";
				echo "<td>";
				echo "<input type='button' value='View' onclick='window.location = \"$rurl&reportid=".$rep_read['id']."&rmode=view\"'>";
				echo "</td>";
				echo "</tr>";
			}
			echo "</table>";

			echo "<br><br><table cellpadding=4 style='border:solid 1px black'><td valign='middle'>";
			echo "<input type='radio' name='new_report_type' id='new_report_type' value='Regular'>Regular";
			echo "<br><input type='radio' name='new_report_type' id='new_report_type_combo' value='Combo'>Combo";
			echo "</td><td valign='middle'>";
			echo "<input type='text' id='new_report' name='new_report'>";
			echo "<select id='comp_package'>";
			echo "<option value='0'>Universal Report</option>";
			$cquery = mlavu_query("select * from `component_packages` where `parent`='0' order by `title` asc",$comp_package);
			while($cread = mysqli_fetch_assoc($cquery))
			{
				echo "<option value='".$cread['id']."'>".$cread['title']."</option>";
			}
			echo "</select>";
			echo "<input type='button' onclick='set_report_type = \"Regular\"; if(document.getElementById(\"new_report_type_combo\").checked) set_report_type = \"Combo\"; setval = document.getElementById(\"new_report\").value; if(setval==\"\") alert(\"Please provide the name of your new report\"); else window.location = \"$rurl&new_report=\" + setval + \"&new_report_type=\" + set_report_type + \"&comp_package=\" + document.getElementById(\"comp_package\").value;' value=
			'Create New Report'>";
			echo "</td></table>";

			if(isset($_GET['new_report_category']))
			{
				mlavu_query("insert into `poslavu_MAIN_db`.`report_categories` (`title`) values ('[1]')",$_GET['new_report_category']);
			}
			if(isset($_GET['update_report_category']))
			{
				mlavu_query("update `poslavu_MAIN_db`.`report_categories` set `title`='[1]', `_order`='[2]' where `id`='[3]'",$_GET['set_value'],$_GET['set_order'],$_GET['update_report_category']);
			}
			echo "<br><br>Report Categories:";
			echo "<table>";
			$rcat_query = mlavu_query("select * from `poslavu_MAIN_db`.`report_categories` order by `id` asc");
			while($rcat_read = mysqli_fetch_assoc($rcat_query))
			{
				$catid = $rcat_read['id'];
				echo "<tr><td><input type='text' id='category_".$catid."' value=\"" . str_replace("\"","&quot;",$rcat_read['title']) . "\"></td><td><input type='text' size='3' value='".$rcat_read['_order']."' id='category_order_".$catid."'></td><td><input type='button' value='Update' onclick='window.location = \"index.php?mode=create_reports&update_report_category=$catid&set_value=\" + document.getElementById(\"category_".$catid."\").value + \"&set_order=\" + document.getElementById(\"category_order_".$catid."\").value'></td></tr>";
			}
			echo "<tr><td><input type='text' id='new_category'></td><td><input type='button' value='Create New Category' onclick='window.location = \"index.php?mode=create_reports&new_report_category=\" + document.getElementById(\"new_category\").value'></td></tr>";
			echo "</table>";
		}

		/*$tablename = urlvar("tablename");
		if($tablename)
		{
			echo "<input type='button' value='Back to Table List' onclick='window.location = \"index.php?mode=create_reports\"' /><br><br>";
			echo "Table <b>$tablename</b>:<br>";
			$field_names = $dbinfo['fields'][$tablename][0];
			$field_types = $dbinfo['fields'][$tablename][1];

			for($i=0; $i<count($field_names); $i++)
			{
				echo "<br>".$field_names[$i];
			}
		}
		else
		{
			for($i=0; $i<count($tables); $i++)
			{
				echo "<br><a href='index.php?mode=create_reports&tablename=".$tables[$i]."'>" . $tables[$i] . "</a>";
			}
		}*/
	}
?>
