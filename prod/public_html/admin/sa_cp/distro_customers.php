<?php
	if(can_access("customers"))
	{
		require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
		
		echo "<table cellpadding=6>";
		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `distro_code`!='' order by `distro_code` asc");
		while($rest_read = mysqli_fetch_assoc($rest_query))
		{
			echo "<tr>";
			echo "<td>" . $rest_read['distro_code'] . "</td>";
			echo "<td>" . $rest_read['company_name'] . "</td>";
			echo "<td>" . $rest_read['data_name'] . "</td>";
			
			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]' or `dataname`='[2]'",$rest_read['id'],$rest_read['data_name']);
			if(mysqli_num_rows($status_query))
			{
				$status_read = mysqli_fetch_assoc($status_query);
				
				echo "<td>" . $status_read['trial_start'] . "</td>";
				echo "<td>&nbsp;</td>";
				echo "<td>" . $status_read['trial_end'] . "</td>";
				echo "<td>" . $status_read['trial_license_paid'] . "</td>";
				echo "<td>" . $status_read['license_type'] . "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";
	}
?>
