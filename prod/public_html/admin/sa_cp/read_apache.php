<?php
	function get_active_apache_processes()
	{
		$fname = "/home/poslavu/private_html/apfullstat.txt";
		//if($mode!="cloud-status")
			//exec("/usr/sbin/apachectl fullstatus > $fname");
		
		$astr = "";
		if(is_file($fname))
		{
			$fp = fopen($fname,"r");
			$astr = fread($fp,filesize($fname));
			fclose($fp);
		}
		
		if($astr!="")
		{
			$alines = explode("\n",$astr);
			
			//echo "lines: " . count($alines);
			//echo "<br><br>";
			
			$process_list = array();
			for($n=0; $n<count($alines); $n++)
			{
				$str = $alines[$n];
				
				$parts = array();
				$current_str = "";
				for($i=0; $i<strlen($str); $i++)
				{
					$chr = substr($str,$i,1);
					if($chr==" " && trim($current_str)!="")
					{
						$parts[] = trim($current_str);
						$current_str = "";
					}
					else if($chr!=" ")
					{
						$current_str .= $chr;
					}
				}
				
				if(count($parts)==14)
				{
					$a_status = $parts[3];
					$a_seconds = $parts[5];
					$a_ipaddress = $parts[10];
					$a_vhost = $parts[11];
					$a_request_type = $parts[12];
					$a_request = $parts[13];
					$a_http = $parts[14];
					
					/*if($a_vhost=="admin.poslavu.com")
					{
						echo $a_seconds . " seconds - " . $a_ipaddress . " " . $a_request . "<br>";
					}*/
					$process_list[] = $parts;
				}
			}
			
			$process_list_order = array();
			$process_list_added = array();
			$process_list_done = false;
			while(count($process_list_order) < count($process_list) && !$process_list_done)
			{
				$highest = 0;
				$highest_index = "";
				for($i=0; $i<count($process_list); $i++)
				{
					$a_seconds = $process_list[$i][5];
					if(!isset($process_list_added[$i]))
					{
						if($a_seconds > $highest || $highest_index=="")
						{
							$highest_index = $i;
							$highest = $a_seconds;
						}
					}
				}
				
				if($highest_index=="")
				{
					$process_list_done = true;
				}
				else
				{
					$process_list_order[] = $highest_index;
					$process_list_added[$highest_index] = true;
				}
			}
					
			$pstr = "";	
			$pstr .= "<table>";
			$pcount = 0;
			for($i=0; $i<count($process_list_order); $i++)
			{
				$parts = $process_list[$process_list_order[$i]];
				$a_status = $parts[3];
				$a_seconds = $parts[5];
				$a_ipaddress = $parts[10];
				$a_vhost = $parts[11];
				$a_request_type = $parts[12];
				$a_request = $parts[13];
				$a_http = $parts[14];
								
				$allow_continue = false;
				if(strpos($a_vhost,"poslavu.com"))
					$allow_continue = true;
				if(isset($_GET['ers']))
					$allow_continue = true;
				if($a_ipaddress=="localhost" || $a_ipaddress=="127.0.0.1")
					$allow_continue = false;
				
				if($allow_continue)
				{
					$a_gateway = "";
					$a_dataname = "";
					$ll_query = mlavu_query("select * from `poslavu_MAIN_db`.`login_log` where `ipaddress`='[1]' order by id desc limit 1",$a_ipaddress);
					if(mysqli_num_rows($ll_query))
					{
						$ll_read = mysqli_fetch_assoc($ll_query);
						$a_remote_users = $ll_read['users'];
						
						$dn_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$a_remote_users);
						if(mysqli_num_rows($dn_query))
						{
							$dn_read = mysqli_fetch_assoc($dn_query);
							$a_dataname = $dn_read['dataname'];
							
							if(trim($a_dataname)!="")
							{
								$a_db = "poslavu_" . $a_dataname . "_db";
								$lc_query = mlavu_query("select * from `[1]`.`locations` where `gateway`!=''",$a_db);
								while($lc_read = mysqli_fetch_assoc($lc_query))
								{
									$a_gateway = $lc_read['gateway'];
								}
							}
						}
					}
					
					if($a_status=="_") $a_show_status = "Waiting for Connection";
					else if($a_status=="S") $a_show_status = "Starting up";
					else if($a_status=="R") $a_show_status = "Reading Request";
					else if($a_status=="W") $a_show_status = "Sending Reply";
					else if($a_status=="K") $a_show_status = "Keepalive (read)";
					else if($a_status=="D") $a_show_status = "DNS Lookup";
					else if($a_status=="C") $a_show_status = "Closing connection";
					else if($a_status=="L") $a_show_status = "Logging";
					else if($a_status=="G") $a_show_status = "Gracefully finishing";
					else if($a_status=="I") $a_show_status = "Idle cleanup of worker";
					else if($a_status==".") $a_show_status = "Open slot with no current process";
					else $a_show_status = "Unknown action: $a_status";
					
					$display_row = true;
					if($a_status==".") $display_row = false;
					if(strpos($a_request,"ers.php")!==false && !isset($_GET['ers'])) $display_row = false;
					
					if($display_row)
					{
						$pstr .= "<tr>";
						if(isset($_GET['ers'])) $pstr .= "<td class='aproc'>" . $a_vhost . "</td><td>&nbsp</td>";
						$pstr .= "<td class='aproc'>" . $a_seconds . " seconds</td><td>&nbsp;</td>";
						$pstr .= "<td class='aproc'>" . $a_show_status . "</td><td>&nbsp;</td>";
						$pstr .= "<td class='aproc'>" . $a_ipaddress . "</td><td>&nbsp;</td>";
						$pstr .= "<td class='aproc'>" . $a_request . "</td><td>&nbsp;</td>";
						$pstr .= "<td class='aproc'>" . $a_dataname . "</td><td>&nbsp;</td>";
						$pstr .= "<td class='aproc'>" . $a_gateway . "</td><td>&nbsp;</td>";
						$pstr .= "</tr>";
						$pcount++;
					}
				}
			}
			$pstr .= "</table>";
			
			/*echo "<b>Apache Processes: " . $pcount . "</b>";
			echo "<style>";
			echo ".aproc { color:#004400; } ";
			echo "</style>";
			echo $pstr;*/
			//echo "process list: " . count($process_list) . "<br>";
			return array("count"=>$pcount,"details"=>$pstr);
		}
	}
?>