<?php
	session_start();
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/admin/sa_cp/presponse_functions.php");
	require_once(dirname(__FILE__).'/../manage/can_access.php');
	$maindb = "poslavu_MAIN_db";
	
	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;

	function confirm_restaurant_shard($data_name)
	{
		$str = "";
		$letter = substr($data_name,0,1);
		if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
			$letter = $letter;
		else
			$letter = "OTHER";
		$tablename = "rest_" . $letter;
		
		$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
		if(mysqli_num_rows($mainrest_query))
		{
			$str .= $letter . ": " . $data_name . " exists";
			$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
			if(mysqli_num_rows($rest_query))
			{
				$rest_read = mysqli_fetch_assoc($rest_query);
				$str .= " - set company id to " . $rest_read['id'];
				$success = mlavu_query("update `poslavu_MAINREST_db`.`$tablename` set `companyid`='[1]' where `data_name`='[2]'",$rest_read['id'],$rest_read['data_name']);
				if($success) $str .= " <font color='green'>success</font>";
				else $str .= " <font color='red'>failed</font>";
			}
		}
		else
		{
			$str .= $letter . ": " . $data_name . " inserting";
			
			$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
			if(mysqli_num_rows($rest_query))
			{
				$rest_read = mysqli_fetch_assoc($rest_query);
				
				$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
				if($success) $str .= " <font color='green'>success</font>";
				else $str .= " <font color='red'>failed</font>";
			}
		}
		return $str;
	}
	
	if(can_access("technical"))
	{
		$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` order by data_name asc");

		while($rest_read = mysqli_fetch_assoc($rest_query))
		{
			$data_name = $rest_read['data_name'];
			$str = confirm_restaurant_shard($data_name);
			echo $str . "<br>";
		}
		
		/*function update_shard_table_op($tablename)
		{
			$query = "ALTER TABLE `poslavu_MAINREST_db`.`$tablename` ADD INDEX(companyid)";
			$success = mlavu_query($query);
			
			echo $tablename . " index companyid ";
			if($success) echo " <font color='green'>success</font>";
			else echo " <font color='red'>failed</font>";
			echo "<br>";
		}
		
		for($i=ord("a"); $i<=ord("z"); $i++)
		{
			$letter = chr($i);
			$tablename = "rest_" . $letter;
			update_shard_table_op($tablename);
		}
		update_shard_table_op("rest_OTHER");*/
	}

	/*for($i=ord("r"); $i<=ord("z"); $i++)
	{
		$letter = chr($i);
		$tablename = "rest_" . $letter;
		
		$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where LEFT(`data_name`,1)='[1]' order by data_name asc",$letter);
		
		while($rest_read = mysqli_fetch_assoc($rest_query))
		{
			$data_name = $rest_read['data_name'];
			
			confirm_restaurant_shard($data_name);
			
			echo "<br>";
		}
		
		//$query = "CREATE TABLE  `poslavu_MAINREST_db`.`rest_".$letter."` (`data_name` VARCHAR( 255 ) NOT NULL ,`data_access` VARCHAR( 255 ) NOT NULL ,`jkey` VARCHAR( 255 ) NOT NULL ,`last_activity` VARCHAR( 255 ) NOT NULL ,`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY) ENGINE = MYISAM";
		//$query = "ALTER TABLE `poslavu_MAINREST_db`.`$tablename` ADD INDEX(data_name); ";
		//echo "doin nutin: " . $letter;

		//$success = mlavu_query($query);
		
		//if($success) echo " <font color='green'>success</font>";
		//else echo " <font color='red'>failed</font>";
		//echo "<br>";
	}*/
