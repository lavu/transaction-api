<?php
require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
$dataname = isset($_REQUEST["dataname"]) ? $_REQUEST["dataname"] : "";
$pos_db_name = "poslavu_{$dataname}_db";
$datetimeQuery = mlavu_query("SELECT DATE_FORMAT(TIMESTAMP(NOW()), '%Y%m%d%H%i%S') as 'time'");
$datetime = mysqli_fetch_assoc($datetimeQuery);
$time = $datetime['time'];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Old Inventory Ingredients</title>
</head>
<body>
<div>
	<img src = 'images/lavu.png' style="padding-left: 10px; float: left" width="50px">
	<img src = 'images/lavu.png' style="padding-right: 10px; float: right" width="50px">
<h1 style="color:#99BC08;text-shadow:1px 1px 0px black; margin-left: 70px;">Old Inventory Ingredients</h1>
<hr WIDTH="98%" COLOR="#99BC08" SIZE="2">
</div>

<div id = "indent" style="margin-left: 10px;">

<?php
if (!empty($dataname))
{
	echo "<h2 style='color:#99BC08;'>Dataname: $dataname</h2>";
	echo "<a href='#download'>Download file at the bottom</a><br><br>";
	echo "<table border = '1' bgcolor = '#494b4c' margin-left: 10px;>
	<tr bgcolor = '#99BC08'><td><strong>Category Name</strong></td>
	<td><strong>Item Name</strong></td>
	<td><strong>Ingredient Name</strong></td>
	<td><strong>Ingredient Quantity</strong></td>
	</tr>";

	$exportData = "Category Name,Item Name,Ingredient Name,Ingredient Quantity\n";
	$sql1 = mlavu_query("SELECT mi.name as 'menu_item_name',mi.ingredients as 'ingredients' ,mc.name as 'menu_category_name' from `$pos_db_name`.`menu_items` mi join `$pos_db_name`.`menu_categories` mc on mc.id=mi.category_id where 'ingredients' != '' order by mc.id asc");

	if ($sql1 === false)
	{
		echo "<h4 style = 'color:red'>ERROR: Invalid dataname: ".$dataname;
	}

	while ($data = mysqli_fetch_assoc($sql1))
	{
		$menuCategoryName = $data['menu_category_name'];
		$menuItemName = $data['menu_item_name'];
		$ingredientsRaw = $data['ingredients'];
		$ingredientsList = explode(",",$ingredientsRaw);
		echo "<tr bgcolor = 'white'><td>".$menuCategoryName."</td><td>".$menuItemName."</td>";
		$exportData .= $menuCategoryName.",".$menuItemName.",";
		foreach ($ingredientsList as $ingredientList)
		{
			$ingredientParts = explode(" x ",$ingredientList);
			$ingredientId = $ingredientParts[0];
			$ingredientQuantity = isset($ingredientParts[1]);
			$ingredientName = '';
			$sql2 = mlavu_query("SELECT title as 'ingredient_name' from `$pos_db_name`.`ingredients` where id = '$ingredientId'");
			while ($data2 = mysqli_fetch_assoc($sql2))
			{
				$ingredientName = $data2['ingredient_name'];
			}
			if ($ingredientList === $ingredientsList[0])
			{
				echo "<td>".$ingredientName."</td><td>".$ingredientQuantity."</td></tr>";
				$exportData .= $ingredientName.",".$ingredientQuantity."\n";
			}
			else
			{
				echo "<tr bgcolor = 'white'><td></td><td></td><td>".$ingredientName."</td><td>".$ingredientQuantity."</td></tr>";
				$exportData .= ",,".$ingredientName.",".$ingredientQuantity."\n";
			}
		}
	}
	echo "</table>";
}

$exportDataFinal = str_replace("'", "", $exportData);
$fileName = $dataname."_".$time;

echo <<<HTML
	<br>
	<div id = "download">
	<form action='/sa_cp/exporter.php' method = 'POST'>
		<input type='hidden' name ='Content-Type' value ='application/excel'>
		<input type='hidden' name ='fileName' value ='$fileName.csv'>
		<input type='hidden' name ='Content' value ='$exportDataFinal'>
		<button style="background-color:#99BC08;"><strong>Download</strong></button>
	</form>
	</div>
	<br>
	<br>
HTML;
?>
</div>
</body>
</html>