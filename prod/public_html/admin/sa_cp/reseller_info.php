<?php
	if(can_access("customers")) {

		ini_set("display_errors","1");
		require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
		
		$fields = array();
		$fields[] = array("f_name", "First name");
		$fields[] = array("l_name", "Last name");
		$fields[] = array("company", "Company");
		$fields[] = array("city", "City");
		$fields[] = array("state", "State");
		$fields[] = array("country", "Country");
		$fields[] = array("isCertified", "Certified?");
		$filter = "";
		
		echo "<style>";
		echo ".label { text-align:right; color:#999999; vertical-align:top; } ";
		echo "tr:hover {background:#eee;}";
		echo "</style>";
		echo "<a href='index.php?mode=reseller_info&action=new'>Add a Reseller</a>";
		echo "<form name='form1' method='post' action='index.php?mode=reseller_info&search=1'>";
		echo "<table cellpadding=12><td style='border:solid 1px black' valign='top'>";
				
		echo "<div style='float:left'>";
		echo "<table>";
		echo "<tr><td colspan='4'>Search Resellers:</td></tr>";
		echo "<tr><td>&nbsp;</td></tr>";
		echo "<tr>";
		foreach ($fields as $field) {
			if($field[0]=='isCertified'){
				
				// isCertified field
				echo "<td><input type='checkbox' name='".$field[0]."' value='1' ". (isset($_POST[$field[0]])?'checked':'') ."></td>";
				echo "<td class='label' style='padding-left:10px'>".ucwords($field[1])." </td>";
			
			} else {
			
				// all other fields
				echo "<td class='label' style='padding-left:10px'>".ucwords($field[1]).": </td>";
				echo "<td><input type='text' name='".$field[0]."' size='16' value='".$_POST[$field[0]]."'></td>";
			}
			if (!empty($_POST[$field[0]])) {
				if ($filter != "") { $filter .= " AND"; }
				$filter .= " `".$field[0]."` LIKE '%[".$field[0]."]%'";
			}
		}
		echo "<td style='padding-left:10px'><input type='submit' value='Search'></td>";
		echo "</tr>";
		echo "</table>";
		echo "</form>";

		echo "</div>";
		echo "</table>";
		
		echo "<div style='float:left; padding-left:10px; width:1200px;'>";
		echo "<br><br>";
		
		if (!empty($_GET['search'])) {
		
			if ($filter == "") {
			
				echo "Please fill out at least one field to perform search...";
			
			} else {
			
				$get_resellers = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`resellers` WHERE $filter ORDER BY `l_name`, `f_name` ASC", $_POST);
				if (mysqli_num_rows($get_resellers) > 0) {
				
					echo "<table cellspacing=1 cellpadding=5 style='border:solid 1px #444'>";
					echo "<tr bgcolor='#B4CDCD'>";
					echo "<th>Name</th>";
					echo "<th>Company</th>";
					echo "<th>Address</th>";
					echo "<th>City</th>";
					echo "<th>State</th>";
					echo "<th>Country</th>";
					echo "<th>Postal Code</th>";
					echo "<th>Phone</th>";
					echo "<th>Email</th>";
					echo "<th>Website</th>";
					echo "<th>Info</th>";
					echo "<th>Signup Date</th>";
					echo "<th>isCertified?</th>";
					echo "<td>&nbsp;</th>";
					echo "</tr>";
				
					while($rinfo = mysqli_fetch_assoc($get_resellers)) {
						echo "<tr>";
						echo "<td>".$rinfo['f_name']." ".$rinfo['l_name']."</td>";
						echo "<td>".$rinfo['company']."</td>";
						echo "<td>".$rinfo['address']."</td>";
						echo "<td>".$rinfo['city']."</td>";
						echo "<td>".$rinfo['state']."</td>";
						echo "<td>".$rinfo['country']."</td>";
						echo "<td>".$rinfo['postal_code']."</td>";
						echo "<td>".$rinfo['phone']."</td>";
						echo "<td>".$rinfo['email']."</td>";
						echo "<td><a href='http://".$rinfo['website']."' target='_blank'>".$rinfo['website']."</a></td>";
						echo "<td>".$rinfo['notes']."</td>";
						echo "<td>".substr($rinfo['signup_date'], 0, 10)."</td>";
						echo "<td align='center'>".(($rinfo['isCertified']) ? 'Yes' : 'No')."</td>";
						echo "<td><a href='index.php?mode=reseller_info&action=edit&id=".$rinfo['id']."'>edit</a></td>";
						echo "</tr>";
					}
					echo "</table>";
				
				} else {
					echo "No resellers found matching your search criteria...";
				}
			}
		}
		$conn = ConnectionHub::getConn('poslavu');

		if(isset($_GET['action']) && $_GET['action'] == 'new') {
		
	  
			if (isset($_POST['submitted'])) { 
			
				foreach($_POST AS $key => $value) { $_POST[$key] = $conn->escapeString($value); } 
				
				$login_fields = "";
				$login_values = "";
				$set_username = $_POST['distro_username'];
				$set_password = $_POST['distro_password'];
				
				$sql = "INSERT INTO `poslavu_MAIN_db`.`resellers` ( `f_name` ,  `l_name` ,  `company` ,  `address` ,  `city` ,  `state` ,  `country` ,  `postal_code` ,  `phone` ,  `email` ,  `website` ,  `isCertified`, `notes`".$login_fields.") VALUES(  '{$_POST['f_name']}' ,  '{$_POST['l_name']}' ,  '{$_POST['company']}' ,  '{$_POST['address']}' ,  '{$_POST['city']}' ,  '{$_POST['state']}' ,  '{$_POST['country']}' ,  '{$_POST['postal_code']}' ,  '{$_POST['phone']}' ,  '{$_POST['email']}' ,  '{$_POST['website']}',  '{$_POST['isCertified']}', '{$_POST['notes']}'".$login_values.") "; 

				mlavu_query($sql) or die(mlavu_dberror()); 
				echo "<h2 style='color:#6A8455'>Successfully Added Reseller</h2>"; 
				//echo "<a href='index.php?mode=reseller_info'>Back To Listing</a>"; 
			} 
		
			echo "<h3>New Reseller</h3>";
			echo "<form action='index.php?mode=reseller_info&action=new' method='POST'>"; 
			echo "<table>";
			echo "<tr><td align='right'><b>First Name:</b></td><td><input type='text' name='f_name'/></td></tr>";
			echo "<tr><td align='right'><b>Last Name:</b></td><td><input type='text' name='l_name'/></td></tr>";
			echo "<tr><td align='right'><b>Company:</b></td><td><input type='text' name='company'/></td></tr>"; 
			echo "<tr><td align='right'><b>Address:</b></td><td><input type='text' name='address'/></td></tr>"; 
			echo "<tr><td align='right'><b>City:</b></td><td><input type='text' name='city'/></td></tr>";
			echo "<tr><td align='right'><b>State:</b></td><td><input type='text' name='state' size='2' maxlength='2'/></td></tr>"; 
			echo "<tr><td align='right'><b>Country:</b></td><td><input type='text' name='country'/></td></tr>"; 
			echo "<tr><td align='right'><b>Postal Code:</b></td><td><input type='text' name='postal_code'/></td></tr>"; 
			echo "<tr><td align='right'><b>Phone:</b></td><td><input type='text' name='phone'/></td></tr>"; 
			echo "<tr><td align='right'><b>Email:</b></td><td><input type='text' name='email'/></td></tr>"; 
			echo "<tr><td align='right' valign='top'><b>Additional Info:</b></td><td valign='top'><textarea name='notes' rows='20' cols='40'></textarea></td></tr>"; 
			echo "<tr><td align='right'><b>Website:</b></td><td valign='top'><input type='text' name='website'/></td></tr>"; 
			//echo "<p><b>Signup Date:</b><input type='text' name='signup_date'/>"; 
			echo "<tr><td align='right'><b>IsCertified:</b></td><td><select name='isCertified'><option value=''></option><option value='1'>Yes</option><option value='0'>No</option></select></td></tr>"; 
			
			//echo "<tr><td align='right'><b>Username:</b></td><td valign='top'><input type='text' name='distro_username' value='' /></td></tr>";
			//echo "<tr><td align='right'><b>Password:</b></td><td valign='top'><input type='text' name='distro_password' value='' /></td></tr>";
			//echo "<tr><td align='right'><b>License Commision (percent):</b></td><td valign='top'><input type='text' name='distro_license_com' value='' /></td></tr>";
	
			echo "<tr><td align='right'></td><td><input type='submit' value='Add Reseller' /><input type='hidden' value='1' name='submitted' /></td></tr>";
			echo "</table>"; 
			echo "</form>"; 

		
		} // end new 
		if (isset($_GET['action']) && $_GET['action'] == 'edit') {
		
	
			if (isset($_GET['id']) ) { 
			
				$id = (int) $_GET['id']; 
				if (isset($_POST['submitted'])) { 
					foreach($_POST AS $key => $value) { $_POST[$key] = $conn->escapeString($value); } 
					
					$extra_update_code = "";
					$set_username = $_POST['distro_username'];
					$set_password = $_POST['distro_password'];
					$set_distro_license_com = $_POST['distro_license_com'];
					
					$confirm_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username` LIKE '[1]' and `id`!='[2]'",$set_username,$id);
					if(mysqli_num_rows($confirm_query))
					{
					}
					else
					{
						$extra_update_code .= ", `username` = '".$conn->escapeString($set_username)."'";
					}
					if($set_password!="")
					{
						$extra_update_code .= ", `password`=PASSWORD('".$conn->escapeString($set_password)."')";
					}
					$extra_update_code .= ", `distro_license_com` = '".$conn->escapeString($set_distro_license_com)."'";
					
					$sql = "UPDATE `poslavu_MAIN_db`.`resellers` SET  `f_name` =  '{$_POST['f_name']}' ,  `l_name` =  '{$_POST['l_name']}' ,  `company` =  '{$_POST['company']}' ,  `address` =  '{$_POST['address']}' ,  `city` =  '{$_POST['city']}' ,  `state` =  '{$_POST['state']}' ,  `country` =  '{$_POST['country']}' ,  `postal_code` =  '{$_POST['postal_code']}' ,  `phone` =  '{$_POST['phone']}' ,  `email` =  '{$_POST['email']}' ,  `website` =  '{$_POST['website']}' ,  `isCertified` =  '{$_POST['isCertified']}', `notes` = '{$_POST['notes']}'".$extra_update_code." WHERE `id` = '$id' "; 
					mlavu_query($sql) or die(mlavu_dberror()); 
					echo (ConnectionHub::getConn('poslavu')->affectedRows()) ? "<h2 style='color:#6A8455'>Successfully Updated Reseller</h2> " : "Nothing changed. <br />"; 
					//echo "<a href='list.php'>Back To Listing</a>"; 
				} 
				$row = mysqli_fetch_array ( mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`resellers` WHERE `id` = '$id' ")); 
			
				echo "<h3>Edit Reseller</h3>";
				echo "<form action='index.php?mode=reseller_info&action=edit&id=".$id."' method='POST'>";
				
				echo "<table>";
				echo "<tr><td align='right'><b>First Name:</b></td><td valign='top'><input type='text' name='f_name' value='". stripslashes($row['f_name']) ."' /></td></tr>";
				echo "<tr><td align='right'><b>Last Name:</b></td><td valign='top'><input type='text' name='l_name' value='". stripslashes($row['l_name']) ."' /></td></tr>";
				echo "<tr><td align='right'><b>Company:</b></td><td valign='top'><input type='text' name='company' value='". stripslashes($row['company']) ."' /></td></tr>"; 
				echo "<tr><td align='right'><b>Address:</b></td><td valign='top'><input type='text' name='address' value='". stripslashes($row['address']) ."' /></td></tr>";
				echo "<tr><td align='right'><b>City:</b></td><td valign='top'><input type='text' name='city' value='". stripslashes($row['city']) ."' /></td></tr>"; 
				echo "<tr><td align='right'><b>State:</b></td><td valign='top'><input type='text' name='state' value='". stripslashes($row['state']) ."' /></td></tr>"; 
				echo "<tr><td align='right'><b>Country:</b></td><td valign='top'><input type='text' name='country' value='".stripslashes($row['country']) ."' /></td></tr>";
				echo "<tr><td align='right'><b>Postal Code:</b></td><td valign='top'><input type='text' name='postal_code' value='". stripslashes($row['postal_code']) ."' /></td></tr>";
				echo "<tr><td align='right'><b>Phone:</b></td><td valign='top'><input type='text' name='phone' value='". stripslashes($row['phone']) ."' /></td></tr>"; 
				echo "<tr><td align='right'><b>Email:</b></td><td valign='top'><input type='text' name='email' value='". stripslashes($row['email']) ."' /></td></tr>"; 
				echo "<tr><td align='right' valign='top'><b>Additional Info:</b></td><td valign='top'><textarea name='notes' rows='20' cols='40'>". stripslashes($row['notes']) ."</textarea></td></tr>"; 
				echo "<tr><td align='right'><b>Website:</b></td><td valign='top'><input type='text' name='website' value='". stripslashes($row['website']) ."' /></td></tr>";
				echo "<tr><td align='right'><b>IsCertified:</b></td><td valign='top'><select name='isCertified'><option value=''></option><option value='1' ".($row['isCertified']==1?'selected':'').">Yes</option><option value='0' ".($row['isCertified']==0?'selected':'').">No</option></select></td></tr>"; 

				echo "<tr><td align='right'><b>Username:</b></td><td valign='top'><input type='text' name='distro_username' value='". stripslashes($row['username']) ."' /></td></tr>";
				echo "<tr><td align='right'><b>Password:</b></td><td valign='top'><input type='text' name='distro_password' value='' /></td></tr>";
				echo "<tr><td align='right'><b>License Commision (percent):</b></td><td valign='top'><input type='text' name='distro_license_com' value='".stripslashes($row['distro_license_com'])."' /></td></tr>";
	
				echo "<tr><td align='right'></td><td><input type='submit' value='Update Reseller' /><input type='hidden' value='1' name='submitted' /></td></tr>"; 
				echo "</table>";
				
				echo "</form>";

		
			}
		} // end edit
	
		echo "</div>";
	}
?>
