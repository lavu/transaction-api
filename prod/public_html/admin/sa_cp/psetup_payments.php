<?php
	if(can_access("technical"))
	{
		//$pay_info = array();
		/*function set_pay_info($response_text)
		{
			global $pay_info;
			$pay_info = parse_payment_response($response_text);
			return $pay_info;
		}*/
		//require_once(dirname(__FILE__) . "/presponse_functions.php");
		
		/*function get_pay_info($pay_info,$type,$def="")
		{
			//global $pay_info;
			return (isset($pay_info[$type]))?$pay_info[$type]:$def;
		}
		function show_col($val)
		{
			if($val=="") $val = "&nbsp;";
			return "<td valign='top'>$val</td>";
		}*/
		
		/*function fill_response_fields()
		{
			$str = "";
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where (`x_response_code`='' and `x_type`='') or `datetime`='' order by id desc");
			while($response_read = mysqli_fetch_assoc($response_query))
			{
				$response_text = $response_read['response'];
				$datetime = trim($response_read['date'] . " " . $response_read['time']);
				$ts = $response_read['ts'];
				$pay_info = parse_payment_response($response_text);
				//set_pay_info($response_text);
				
				$x_description = get_pay_info($pay_info,"x_description");
				$x_amount = get_pay_info($pay_info,"x_amount");
				$x_type = get_pay_info($pay_info,"x_type");
				$x_subscriptionid = get_pay_info($pay_info,"x_subscription_id");
				
				$x_first_name = get_pay_info($pay_info,"x_first_name");
				$x_last_name = get_pay_info($pay_info,"x_last_name");
				$x_company = get_pay_info($pay_info,"x_company");
				$x_city = get_pay_info($pay_info,"x_city");
				$x_state = get_pay_info($pay_info,"x_state");
				$x_phone = get_pay_info($pay_info,"x_phone");
				$x_email = get_pay_info($pay_info,"x_email");
				
				$x_account_number = get_pay_info($pay_info,"x_account_number");
				$x_card_type = get_pay_info($pay_info,"x_card_type");
				$x_response_code = get_pay_info($pay_info,"x_response_code");
				if($x_response_code=="1") $x_show_response = "<font color='#008800'>accepted</font>";
				else $x_show_response = "<font color='#880000'>declined</font>";
				
				$flist = array();
				$flist["x_description"] = $x_description;
				$flist["x_amount"] = $x_amount;
				$flist["x_type"] = $x_type;
				$flist["x_subscriptionid"] = $x_subscriptionid;
				$flist["x_first_name"] = $x_first_name;
				$flist["x_last_name"] = $x_last_name;
				$flist["x_company"] = $x_company;
				$flist["x_city"] = $x_city;
				$flist["x_state"] = $x_state;
				$flist["x_phone"] = $x_phone;
				$flist["x_email"] = $x_email;
				$flist["x_account_number"] = $x_account_number;
				$flist["x_card_type"] = $x_card_type;
				$flist["x_response_code"] = $x_response_code;
				$flist["datetime"] = $datetime;
				
				$update_code = "";
				foreach($flist as $key => $val)
				{
					if($update_code!="") $update_code .= ",";
					$update_code .= "`$key`='[$key]'";
				}
				$flist["id"] = $response_read['id'];
				$success = mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set " . $update_code . " where `id`='[id]'",$flist);
				$str .= "updated payment response data " . $response_read['id'];
				if($success) $str .= "(success)"; else $str .= "(failed) <br>$update_code";
				$str .= "<br>";
			}
			return $str;
		}*/
		
		/*function get_restaurant_data($dataname,$field="")
		{
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
			if(mysqli_num_rows($rest_query))
			{
				$rest_read = mysqli_fetch_assoc($rest_query);
				if($field!="")
					return $rest_read[$field];
				else
					return $rest_read;
			}
			else return false;
		}
		
		function get_restaurant_id($dataname)
		{
			return get_restaurant_data($dataname,"id");
		}
		
		function get_subscription_signup($res_read)
		{
			$signup_read = array();
			$find_method = "no";
			
			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `arb_license_id`='[1]' or `arb_hosting_id`='[1]'",$res_read['x_subscriptionid']);
			if(mysqli_num_rows($signup_query))
			{
				$signup_read = mysqli_fetch_assoc($signup_query);
				$find_method = "match";
			}
			else
			{
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `firstname`='[1]' and `lastname`='[2]' and `city`='[3]' and `city`!='' and `firstname`!=''",$res_read['x_first_name'],$res_read['x_last_name'],$res_read['x_city']);
				if(mysqli_num_rows($signup_query))
				{
					$signup_read = mysqli_fetch_assoc($signup_query);
					$find_method = "name_city";
				}
			}
			
			$signup_read['find_method'] = $find_method;
			return $signup_read;
		}

		function set_response_restaurant_id($response_id,$rest_id,$match_type)
		{
			$success = mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `match_restaurantid`='[1]', `match_type`='[2]' where `id`='[3]'",$rest_id,$match_type,$response_id);
			return $success;
		}
		
		function get_payment_request_info($payment_request_id)
		{
			$payreq_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_request` where `id`='[1]'",$payment_request_id);
			if(mysqli_num_rows($payreq_query))
			{
				$payreq_read = mysqli_fetch_assoc($payreq_query);
				return $payreq_read;
			}
			else return false;
		}
		
		function get_payment_request_rest_id($payment_request_id)
		{
			$payreq_read = get_payment_request_info($payment_request_id);
			if($payreq_read['set_restid']!="" && is_numeric($payreq_read['set_restid']))
				return $payreq_read['set_restid'];
			else
				return $payreq_read['restaurantid'];
		}
		
		function get_signup_by_name_and_city($res_read)
		{
			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `match_type`!='manual' and `firstname`='[1]' and `lastname`='[2]' and `city`='[3]' and `city`!='' and `firstname`!=''",$res_read['x_first_name'],$res_read['x_last_name'],$res_read['x_city']);
			if(mysqli_num_rows($signup_query))
			{
				$signup_read = mysqli_fetch_assoc($signup_query);
				return $signup_read;
			}
			else return false;
		}
		
		function get_response_by_name_and_card($res_read)
		{
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_type`!='manual' and `match_restaurantid`!='' and `x_account_number`!='' and `x_first_name`!='' and `x_last_name`!='' and `x_account_number`='[1]' and `x_first_name`='[2]' and `x_last_name`='[3]'",$res_read['x_account_number'],$res_read['x_first_name'],$res_read['x_last_name']);
			if(mysqli_num_rows($response_query))
			{
				$response_read = mysqli_fetch_assoc($response_query);
				return $response_read;
			}
			else return false;
		}

		function get_response_by_name_and_company($res_read)
		{
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_type`!='manual' and `match_restaurantid`!='' and `x_company`!='' and `x_first_name`!='' and `x_last_name`!='' and `x_company`='[1]' and `x_first_name`='[2]' and `x_last_name`='[3]'",$res_read['x_company'],$res_read['x_first_name'],$res_read['x_last_name']);
			if(mysqli_num_rows($response_query))
			{
				$response_read = mysqli_fetch_assoc($response_query);
				return $response_read;
			}
			else return false;
		}*/

		function process_system_payments($type)
		{
			global $loggedin_fullname;
			global $loggedin;
			
			$str = "processing: <br>";
			
			// for manual set vars
				$update_responseid = "";
				$set_restid = "";
				if(isset($_GET['update_resp']))
				{
					$update_responseid = $_GET['update_resp'];
					$set_restid = $_GET['set_value'];		
				}
			// end code for manual set vars
			
			if($type=="findgap")
			{
				$lastdate = "";
				$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` order by date asc, time asc");
				while($res_read = mysqli_fetch_assoc($res_query))
				{
					$nextdate = $res_read['date'];
					if($lastdate!="" && $nextdate!=$lastdate)
					{
						$ldate_parts = explode("-",$lastdate);
						$ndate_parts = explode("-",$nextdate);
						
						$datediff = mktime(0,0,0,$ndate_parts[1],$ndate_parts[2],$ndate_parts[0]) - mktime(0,0,0,$ldate_parts[1],$ldate_parts[2],$ldate_parts[0]);
						$daydiff = ($datediff / 60 / 60 / 24);
						if($daydiff > 1)
							echo $daydiff . " " . $lastdate . " to " . $nextdate . "<br>";
					}
					
					$lastdate = $nextdate;
				}
			}
			else if($type=="subscriptions")
			{
				$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`!='' and `x_response_code`='1' and `x_subscriptionid`!=''");
				while($res_read = mysqli_fetch_assoc($res_query))
				{
					$signup_read = get_subscription_signup($res_read);
					if($signup_read['find_method']!="no")
					{						
						$rest_id = get_restaurant_id($signup_read['dataname']);
						if($rest_id)
						{
							$success = set_response_restaurant_id($res_read['id'],$rest_id,"subscription");
							$str .= "found " . $signup_read['firstname'] . " " . $signup_read['lastname'] . " - set response " . $res_read['id'] . " to " . $rest_id . "<br>";
						}
					}
					else $str .= "not found: " . $res_read['id'] . "<br>";
				}
			}
			else if($type=="payment_links")
			{
				$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='' and `x_response_code`='1' and LEFT(`x_description`,17)='POSLavu Payment L'");
				while($res_read = mysqli_fetch_assoc($res_query))
				{
					$payment_request_id = substr($res_read['x_description'],17);
					$rest_id = get_payment_request_rest_id($payment_request_id);
					
					if($rest_id)
					{
						$success = set_response_restaurant_id($res_read['id'],$rest_id,"payment link");
						$str .= $res_read['x_description'] . " - $payment_request_id - set response " . $res_read['id'] . " to " . $rest_id . "<br>";
					}
				}
			}
			else if($type=="voids")
			{
				function get_rsp_var($var,$response_str)
				{
					$str_parts = explode($var . " =",$response_str);
					if(count($str_parts) > 1)
					{
						$str_parts = explode("\n",$str_parts[1]);
						return trim($str_parts[0]);
					}
					else return "";
				}
				
				$vres_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='' and `x_response_code`='1' and `x_type`='void' order by id desc limit 2");
				while($vres_read = mysqli_fetch_assoc($vres_query))
				{
					$response_str = $vres_read['response'];
					$x_trans_id = get_rsp_var("x_trans_id",$response_str);
					$x_account_number = get_rsp_var("x_account_number",$response_str);
					
					echo "x_trans_id: $x_trans_id<br>";
					echo "x_account_number: $x_account_number<br>";
					echo "<br>";
					
					$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_type`='auth_capture' and `x_account_number`='[1]' order by id desc limit 20",$x_account_number);
					while($res_read = mysqli_fetch_assoc($res_query))
					{
						$check_x_trans_id = get_rsp_var("x_trans_id",$res_read['response']);
						if($check_x_trans_id==$x_trans_id)
						{
							$success = mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `x_type`='auth_capture_voided' where `id`='[1]' limit 1",$res_read['id']);
							if($success)
								echo "Voided: ".$res_read['id']."<br><br>";
							//echo "---------------FOUND--------------<br><br>";
						}
						//echo str_replace("\n","<br>",$res_read['response']) . "<br><br>";
					}
					
					echo "<hr>";
				}
			}
			else if($type=="refunds")
			{
				$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='' and `x_response_code`='1' and `x_type`='credit' order by id desc");
				while($res_read = mysqli_fetch_assoc($res_query))
				{
					$prev_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_amount`>='[x_amount]' and `x_card_type`='[x_card_type]' and `x_account_number`='[x_account_number]' and `ts`<'[ts]' and `x_first_name`='[x_first_name]' and `x_last_name`='[x_last_name]' and `match_restaurantid`!='' order by x_amount asc",$res_read);
					if(mysqli_num_rows($prev_query))
					{
						$prev_read = mysqli_fetch_assoc($prev_query);
						$rest_id = $prev_read['match_restaurantid'];
						
						if($rest_id)
						{
							$success = set_response_restaurant_id($res_read['id'],$rest_id,"refund match");
							$str .= "FOUND MATCH - ".$res_read['x_amount']." " . $prev_read['match_restaurantid'] . "<br>";
						}
					}
					else 
					{
						if($res_read['id']==$update_responseid && $set_restid!="")
						{
							$rest_id = $set_restid;
							$find_method = "manual";
							
							$success = set_response_restaurant_id($res_read['id'],$rest_id,$find_method);
							$str .= "manual set - ".$res_read['datetime'] . " - " . $res_read['x_amount']." - " . $res_read['x_account_number'] . " " . $res_read['x_first_name'] . " " . $res_read['x_last_name']. "<br>";
						}
						else
						{
							$set_input_id = "input_set_restid_" . $res_read['id'];
							$str .= "<input style='font-size:10px' type='text' value='' id='$set_input_id' name='$set_input_id' /><input style='font-size:10px' type='button' value='update' onclick='window.location = \"psetup.php?type=refunds&update_resp=".$res_read['id']."&set_value=\" + document.getElementById(\"$set_input_id\").value' />&nbsp;";
							$str .= "not found - ".$res_read['datetime'] . " - " . $res_read['x_amount']." - " . $res_read['x_account_number'] . " " . $res_read['x_first_name'] . " " . $res_read['x_last_name']. "<br>";
						}
					}
				}
			}
			else if($type=="requests")
			{
				$request_query = mlavu_query("select * from `payment_request` where dataname='' and `license_paid`='paid'");
				while($request_read = mysqli_fetch_assoc($request_query))
				{
					$rest_id = $request_read['restaurantid'];
					if($request_read['set_restid']!="" && $request_read['set_restid'] > 0) $rest_id = $request_read['set_restid'];
					
					$rest_query = mlavu_query("select data_name as data_name from restaurants where id='[1]'",$rest_id);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						$dataname = $rest_read['data_name'];
						
						mlavu_query("update `payment_request` set `dataname`='[1]' where `id`='[2]'",$dataname,$request_read['id']);
					}
				}
				
				echo "<table>";
				$request_query = mlavu_query("select * from `payment_request` where license_amount!='' and `license_paid`='paid' order by license_amount * 1 desc, responseid asc, id asc");
				while($request_read = mysqli_fetch_assoc($request_query))
				{
					$line_style = "";
					if($request_read['responseid']=="")
					{
						$marked = false;
						$match_query = mlavu_query("select * from `payment_responses` where `match_restaurantid`='[1]' and `x_response_code`='1' and (`x_amount` * 1)=('[2]' * 1)",$request_read['restaurantid'],$request_read['license_amount']);
						if(mysqli_num_rows($match_query))
						{
							$match_read = mysqli_fetch_assoc($match_query);
							$marked = mlavu_query("update `payment_request` set `responseid`='[1]' where `id`='[2]'",$match_read['id'],$request_read['id']);
							$line_style = "bgcolor='#ccddff'";
						}
					}
					else
					{
						$line_style = "bgcolor='#ccddff'";
					}
						
					echo "<tr ".$line_style.">";
					echo "<td valign='top' width='120'>" . $request_read['license_amount'];
					if($marked) echo " <b>(marked)</b>";
					echo "</td>";
					echo "<td valign='top' width='100'>" . $request_read['license_paid'] . "</td>";
					echo "<td valign='top' width='100'>" . $request_read['dataname'] . "</td>";
					echo "<td valign='top' width='200'>" . $request_read['action'] . "</td>";
					echo "<td valign='top' width='200'>" . $request_read['email'] . "</td>";
					echo "<td valign='top' width='800'>";
					echo "<font style='font-size:9px'>" . $request_read['notes'] . "</font>";
					echo "</td>";
					echo "</tr>";
				}
				echo "</table>";
			}
			else if($type=="status")
			{
				$month_ago = date("Y-m-d",mktime(0,0,0,date("m") - 1,date("d"),date("Y"))) . " 00:00:00";
				
				$license_pay = array();
				$license_nopay = array();
				$license_demo_nopay = array();
				$hosting_pay = array();
				$hosting_nopay = array();
				$hosting_demo_nopay = array();
				$all_hosting_total = 0;
				$all_hosting_collected = 0;
				$signup_status_counts = array();
				
				$str = "";
				$dstr = "";
				$dstr .= "<table>";
				$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `disabled`!='1' and `demo`!='1' and `dev`!='1' and `last_activity`>='[1]' and `created` < '[1]' order by id asc",$month_ago);
				while($rest_read = mysqli_fetch_assoc($rest_query))
				{
					//--------------------- LICENSE
					$total_paid = 0;
					$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit') and (`x_amount` * 1 > 300) and `x_response_code`='1' order by `x_amount` * 1 desc, id desc",$rest_read['id'],$month_ago);
					if(mysqli_num_rows($res_query))
					{
						while($res_read = mysqli_fetch_assoc($res_query))
						{
							if($res_read['x_type']=="credit")
								$total_paid -= ($res_read['x_amount'] * 1);
							else
								$total_paid += ($res_read['x_amount'] * 1);
						}
					}
					
					$contains_demo_in_name = (strpos(strtolower($rest_read['company_name']),"demo")!==false);
					
					if($total_paid >= 500)
						$license_pay[] = $res_read['company_name'];
					else if($contains_demo_in_name)
						$license_demo_nopay[] = $res_read['company_name'];
					else
						$license_nopay[] = $res_read['company_name'];
					$total_license_paid = $total_paid;
						
					//-------------------- HOSTING
					/*$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `x_type`='auth_capture' and (`x_amount` * 1 < 120) and (`x_amount` * 1 > 8) and `datetime`>='[2]' and `x_response_code`='1' order by `x_amount` * 1 desc, id desc",$rest_read['id'],$month_ago);
					if(mysqli_num_rows($res_query))
					{
						$res_read = mysqli_fetch_assoc($res_read);
						
						$hosting_pay[] = $res_read['company_name'];
					}
					else
					{
						if(strpos(strtolower($rest_read['company_name']),"demo")!==false)
							$hosting_demo_nopay[] = $res_read['company_name'];
						else
							$hosting_nopay[] = $res_read['company_name'];
					}*/
					
					$hosting_is_current = false;
					$total_hosting_paid = 0;
					$highest_hosting_amount = 0;
					$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit') and (`x_amount` * 1 < 120) and (`x_amount` * 1 > 8) and `x_response_code`='1' order by `x_amount` * 1 desc, id desc",$rest_read['id'],$month_ago);
					while($res_read = mysqli_fetch_assoc($res_query))
					{
						if($res_read['x_type']=="auth_capture")
						{
							if($res_read['datetime'] >= $month_ago)
								$hosting_is_current = true;
						}
						
						if($res_read['x_type']=="credit")
							$total_hosting_paid -= $res_read['x_amount'];
						else
							$total_hosting_paid += $res_read['x_amount'];
						
						if($res_read['x_amount'] * 1 > $highest_hosting_amount)
							$highest_hosting_amount = $res_read['x_amount'];
					}
					
					if($highest_hosting_amount > 80) $hosting_type = "platinum";
					else if($highest_hosting_amount > 40) $hosting_type = "gold";
					else if($highest_hosting_amount > 20) $hosting_type = "silver";
					else $hosting_type = "";
					
					$signup_status = $rest_read['license_status'];
					$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$rest_read['data_name']);
					if(mysqli_num_rows($signup_query))
					{
						$signup_read = mysqli_fetch_assoc($signup_query);
						$hosting_type_id = $signup_read['package'];
						
						if($hosting_type_id==1) $hosting_type = "silver";
						else if($hosting_type_id==2) $hosting_type = "gold";
						else if($hosting_type_id==3) $hosting_type = "platinum";
					}
					
					$total_license_is_paid = false;
					if($hosting_type=="silver" && $total_license_paid > 700)
						$total_license_is_paid = true;
					else if($hosting_type=="gold" && $total_license_paid > 1100)
						$total_license_is_paid = true;
					else if($hosting_type=="platinum" && $total_license_paid > 2700)
						$total_license_is_paid = true;
					
					if($contains_demo_in_name)
					{
					}
					else if($signup_status=="Dev" || $signup_status=="Waived" || $signup_status=="Allow")
					{
						if(!isset($signup_status_counts[$signup_status]))
							$signup_status_counts[$signup_status] = 0;
						$signup_status_counts[$signup_status]++;
					}
					else
					{
						if($hosting_type=="silver") $hosting_fee = "29.95";
						else if($hosting_type=="gold") $hosting_fee = "49.95";
						else if($hosting_type=="platinum") $hosting_fee = "99.95";
						else $hosting_fee = "49.95";
						
						$created_date_parts = explode("-",substr($rest_read['created'],0,10));
						$created_date_ts = mktime(0,0,0,$created_date_parts[1],$created_date_parts[2],$created_date_parts[0]);
						
						$hosting_months = $rest_read['created'] . " - " . (ceil((time() - $created_date_ts) / 60 / 60 / 24 / 30) - 1) . " months";
						$total_hosting_amount = $hosting_fee * (ceil((time() - $created_date_ts) / 60 / 60 / 24 / 30) - 1);
						
						if($total_hosting_amount==$total_hosting_paid && $total_license_is_paid)
						{
						}
						else
						{
							$turn_on_due_date = false;
							$dstr .= "<tr onmouseover='this.bgColor = \"#ddeeff\"' onmouseout='this.bgColor = \"#ffffff\"' onclick='window.open(\"psetup.php?type=details&typeid=".$rest_read['id']."&typename=".$rest_read['data_name']."\",\"pay_details\",\"width=800,height=750,toolbar=0,scrollbars=1,resizable=1\")'>";
							$dstr .= "<td>";
							
							$statuscolor = "#dddddd";
							$statusstr = "&nbsp;";
							$extra_statusstr = "&nbsp;";
							$status_due_date = "";
							$set_due_date_to = "";
							$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
							while($status_read = mysqli_fetch_assoc($status_query))
							{
								$status_id = $status_read['id'];
								if($status_read['ok']=="1")
								{
									$statuscolor = "#008800";
									$statusstr = "OK";
								}
								else if($status_read['hosting_action']!="" && $status_read['collect']!="")
								{
									$statuscolor = "#0000cc";
									$statusstr = "H + C";
								}
								else if($status_read['hosting_action']!="")
								{
									$statuscolor = "#0088cc";
									$statusstr = "Hosting";
								}
								else if($status_read['collect']!="")
								{
									$statuscolor = "#00cccc";
									$statusstr = "Collect";
								}
								
								if($status_read['collection_active']=="1") $extra_statusstr = "!";
								
								if($status_read['hosting_taken']) $extra_statusstr .= "H";
								if($status_read['collection_taken']) $extra_statusstr .= "C";
								
								if($status_read['collection_active']=="1" && $status_read['collection_due_date']=="" && $status_read['ok']!="1")
								{
									if($status_read['hosting_action']!="" && !$status_read['hosting_taken'])
										$turn_on_due_date = true;
									if($status_read['collect']!="" && !$status_read['collection_taken'])
										$turn_on_due_date = true;
									if($turn_on_due_date)
									{
										$set_due_date_to = "(auto set disabled)";//date("Y-m-d",mktime(0,0,0,date("m"),date("d") + 10,date("Y")));
										//mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `collection_due_date`='[1]' where `restaurantid`='[2]' and `collection_due_date`='' and `id`='[3]' limit 1",$set_due_date_to,$rest_read['id'],$status_id);
									}
								}
								$status_due_date = $status_read['collection_due_date'];
							}
							$dstr .= "<table><td bgcolor='$statuscolor' align='center' style='border:solid 1px #777777; width:120px; color:#ffffff'>";
							$dstr .= "<b>" . $statusstr . "</b>";
							$dstr .= "</td></table>";
							
							$dstr .= "</td>";
							$dstr .= "<td>";
							$dstr .= "<b>" . $extra_statusstr . "</b>";
							$dstr .= "</td>";
							$dstr .= "<td align='right'>" . substr($rest_read['company_name'],0,24) . "</td>";
							$dstr .= "<td>" . $hosting_type . "</td>";
							$dstr .= "<td>&nbsp;</td>";
							$dstr .= "<td>" . $hosting_months . "</td>";
							$dstr .= "<td>&nbsp;</td>";
							$dstr .= "<td>$" . number_format($total_hosting_amount,2) . "</td>";
							$dstr .= "<td>&nbsp;</td>";
							$dstr .= "<td>$" . number_format($total_hosting_paid,2) . "</td>";
							$dstr .= "<td>" . $status_due_date . "</td>";
							if($turn_on_due_date)
							{
								$dstr .= "<td>Turn On Due Date - $set_due_date_to</td>";
							}
							$dstr .= "</tr>";
							
							$all_hosting_total += $total_hosting_amount * 1;
							$all_hosting_collected += $total_hosting_paid * 1;
							
							if($hosting_is_current)
								$hosting_pay[] = $rest_read['company_name'];
							else if(strpos(strtolower($rest_read['company_name']),"demo")!==false)
								$hosting_demo_nopay[] = $rest_read['company_name'];
							else
								$hosting_nopay[] = $rest_read['company_name'];
						}
					}
				}
				$dstr .= "</table>";
				$dstr .= "<br>All Hosting Total: " . number_format($all_hosting_total,2);
				$dstr .= "<br>All Hosting Collected: " . number_format($all_hosting_collected,2);
				$dstr .= "<br>Due: " . number_format(($all_hosting_total - $all_hosting_collected),2);
				
				$str .= "<style>";
				$str .= "table { font-family:Verdana,Arial; font-size:10px;} ";
				$str .= "</style>";
				
				$str .= $dstr;
				$str .= "<hr><b>Licenses</b><br>";
				$str .= "<br>total accounts 1 month old: " . (count($license_pay) + count($license_nopay) + count($license_demo_nopay));
				$str .= "<br>license paid: " . count($license_pay);
				$str .= "<br>not paid: " . count($license_nopay);
				$str .= "<br>not paid & has demo in the name: " . count($license_demo_nopay);
				$str .= "<br>";
				
				$str .= "<hr><b>Hosting</b><br>";
				$str .= "<br>total accounts 1 month old: " . (count($hosting_pay) + count($hosting_nopay) + count($hosting_demo_nopay));
				$str .= "<br>hosting paid within last month: " . count($hosting_pay);
				$str .= "<br>not paid within last month: " . count($hosting_nopay);
				$str .= "<br>not paid within last month & has demo in the name: " . count($hosting_demo_nopay);
				$str .= "<br>";
				
				$str .= "<hr><b>Other</b><br>";
				foreach($signup_status_counts as $key => $val)
				{
					$str .= "<br>" . $key . ": " . $val;
				}
			}
			else if($type=="details")
			{
				$p_companyid = $_GET['typeid'];
				$p_dataname = $_GET['typename'];
				$p_clear_out = (isset($_GET['clear_out']))?$_GET['clear_out']:"0";
				$p_area = "billing_info";
				
				$fwd_vars = "type=details&typeid=".$p_companyid."&typename=".$p_dataname;
				$p_type = "lavuadmin";
				$str = "";
				
				require_once("/home/poslavu/public_html/admin/cp/areas/billing.php");
								
				$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$p_companyid);
				while($rest_read = mysqli_fetch_assoc($rest_query))
				{
					$str .= "<br>Last Activity: " . $rest_read['last_activity'];
					$str .= "<table cellspacing=0 cellpadding=0><tr>";
					$str .= "<td>Daily Use Over Last Two Weeks: </td>";
					
					if($p_clear_out=="1")
					{
						$set_history = "";
						$set_history .= "-------------------------------------------------\n";
						$set_history .= "Clear Out: By " . $loggedin_fullname . "($loggedin) at " . date("Y-m-d H:i:s") . " from " . $_SERVER['REMOTE_ADDR'] . "\n";
						$collection_status_id = "";
						$collect_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
						if(mysqli_num_rows($collect_query))
						{
							$collect_read = mysqli_fetch_assoc($collect_query);
							$collection_status_id = $collect_read['id'];
							foreach($collect_read as $key => $val)
							{
								if($key!="history")
									$set_history .= $key . ": " . $val . "\n";
							}
						}
						//echo "<textarea rows='36' cols='80'>$set_history</textarea>";
						
						$cvars = array();
						$cvars['ok'] = "";
						$cvars['hosting_action'] = "";
						$cvars['hosting_amount'] = "";
						$cvars['hosting_start'] = "";
						$cvars['collect'] = "";
						$cvars['collection_active'] = "";
						$cvars['status_notes'] = "";
						
						$cvars['restaurantid'] = $rest_read['id'];
						$cvars['dataname'] = $rest_read['data_name'];
						$cvars['id'] = $collection_status_id;
						
						$cvars['collection_due_date'] = "";
						$cvars['collection_extend_days'] = "";
						$cvars['collection_minimum'] = "";
						
						$cvars['hosting_taken'] = "";
						$cvars['collection_taken'] = "";
						
						if($collection_status_id!="")
						{
							$set_cvars = "";
							foreach($cvars as $key => $val)
							{
								$inskey = $key;
								$inskey = str_replace("'","",$inskey);
								$inskey = str_replace("`","",$inskey);
								if($set_cvars!="") $set_cvars .= ", ";
								$set_cvars .= "`".$inskey."`='[".$inskey."]'";
							}
							$cvars['history'] = $set_history;
							mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `history`=concat('[history]\n',`history`), " . $set_cvars . " where `id`='[id]' and `dataname`='[dataname]' limit 1",$cvars);
						}
						echo "<br><b>CLEARING OUT AND WRITING TO HISTORY...</b><br>";
						echo "<br><br>";
						echo "<input type='button' value='Continue >>' onclick='window.location = \"psetup.php?$fwd_vars\"' />";
						exit();
					}
					
					$restdb = "poslavu_" . $rest_read['data_name'] . "_db";
					for($n=0; $n<14; $n++)
					{
						$bordercolor = "#777777";
						$bgcolor = "#dddddd";
						
						$nday = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - $n,date("Y")));
						$nday_start = $nday . " 00:00:00";
						$nday_end = $nday . " 24:00:00";
						$ocount_query = mlavu_query("select count(*) as `count` from `$restdb`.`orders` where (`opened`>='[1]' and `opened`<='[2]') or (`closed`>='[1]' and `closed`<='[2]')",$nday_start,$nday_end);
						if(mysqli_num_rows($ocount_query))
						{
							$ocount_read = mysqli_fetch_assoc($ocount_query);
							$ocount = $ocount_read['count'];
						}
						else $ocount = 0;
						
						if($ocount > 0)
						{
							$bordercolor = "#008800";
							$bgcolor = "#77ff77";
						}
						
						$str .= "<td>";
						$str .= "<table><tr><td style='border:solid 1px $bordercolor; width:20px; height:16px; text-align:center; vertical-align:middle' bgcolor='$bgcolor'>$ocount</td></tr></table>";
						$str .= "</td>";
					}
					
					$str .= "</tr></table>";
					$str .= "<br><textarea rows='8' cols='80'>" . $rest_read['notes'] . "</textarea>";
					
					$post_message = "";
					if(isset($_POST['posted']))
					{
						$set_hosting = $_POST['hosting'];
						$set_new_amount = $_POST['new_amount'];
						if($set_hosting!="")
							$set_hosting_start = $_POST['hosting_start'];
						else
							$set_hosting_start = "";
						$set_collect = $_POST['collect'];
						$set_ok = (isset($_POST['confirm_ok']))?1:0;
						$set_status_notes = $_POST['status_notes'];
						
						$set_collection_active = (isset($_POST['collection_active']))?1:0;
						
						$set_collection_due_date = (isset($_POST['collection_due_date']))?$_POST['collection_due_date']:"";
						$set_collection_extend_days = (isset($_POST['collection_extend_days']))?$_POST['collection_extend_days']:"";
						$set_collection_minimum = (isset($_POST['collection_minimum']))?$_POST['collection_minimum']:"";

						/*$str .= "<br>";
						$str .= "<br>set hosting: " . $set_hosting;
						$str .= "<br>set new amount: " . $set_new_amount;
						$str .= "<br>set hosting start: " . $set_hosting_start;
						$str .= "<br>collect: " . $set_collect;
						$str .= "<br>ok: " . $set_ok;
						$str .= "<br>";*/

						$vars = array();
						$vars['ok'] = $set_ok;
						$vars['hosting_action'] = $set_hosting;
						$vars['hosting_amount'] = $set_new_amount;
						$vars['hosting_start'] = $set_hosting_start;
						$vars['collect'] = $set_collect;
						$vars['collection_active'] = $set_collection_active;
						$vars['status_notes'] = $set_status_notes;
						$vars['restaurantid'] = $rest_read['id'];
						$vars['dataname'] = $rest_read['data_name'];
						
						$vars['collection_due_date'] = $set_collection_due_date;
						$vars['collection_extend_days'] = $set_collection_extend_days;
						$vars['collection_minimum'] = $set_collection_minimum;
						
						$datetime = date("m/d/Y h:i:s a");
						
						$collect_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
						if(mysqli_num_rows($collect_query))
						{
							$collect_read = mysqli_fetch_assoc($collect_query);
							$vars['id'] = $collect_read['id'];
							$success = mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `ok`='[ok]', `hosting_action`='[hosting_action]', `hosting_start`='[hosting_start]', `hosting_amount`='[hosting_amount]', `collect`='[collect]', `collection_active`='[collection_active]', `status_notes`='[status_notes]', `collection_due_date`='[collection_due_date]', `collection_extend_days`='[collection_extend_days]', `collection_minimum`='[collection_minimum]' where `id`='[id]'",$vars);
						}
						else
						{
							$success = mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`ok`,`hosting_action`,`hosting_amount`,`hosting_start`,`collect`,`collection_active`,`status_notes`,`collection_due_date`,`collection_extend_days`,`collection_minimum`,`restaurantid`,`dataname`) values ('[ok]','[hosting_action]','[hosting_amount]','[hosting_start]','[collect]','[collection_active]','[status_notes]','[collection_due_date]','[collection_extend_days]','[collection_minimum]','[restaurantid]','[dataname]')",$vars);
						}
						if($success) $post_message = "<font color='green'><b>Update Succeeded: $datetime</b></font><br>";
						else $post_message = "<font color='red'><b>Update Failed: $datetime</b></font><br>";
						
						//ok
						//collect
						//hosting_action
						//hosting_start
						//hosting_amount
						//restaurantid
						//dataname
						//id
						//status_notes
						
					}
					
					$collect_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
					if(mysqli_num_rows($collect_query))
					{
						$collect_read = mysqli_fetch_assoc($collect_query);
						
						$set_hosting = $collect_read['hosting_action'];
						$set_hosting_start = $collect_read['hosting_start'];
						$set_new_amount = $collect_read['hosting_amount'];
						$set_ok = $collect_read['ok'];
						$set_collect = $collect_read['collect'];
						$set_collection_active = $collect_read['collection_active'];
						$set_status_notes = $collect_read['status_notes'];
						
						$set_hosting_taken = $collect_read['hosting_taken'];
						$set_collection_taken = $collect_read['collection_taken'];
						$set_collection_extend_days = $collect_read['collection_extend_days'];
						$set_collection_due_date = $collect_read['collection_due_date'];
						$set_collection_minimum = $collect_read['collection_minimum'];
						
						$collection_history = $collect_read['history'];
					}
					else
					{
						$set_hosting = "";
						$set_hosting_start = "";
						$set_new_amount = "";
						$set_ok = "";
						$set_collect = "";
						$set_collection_active = "";
						$set_status_notes = "";
						
						$set_hosting_taken = "";
						$set_collection_taken = "";
						$set_collection_extend_days = "";
						$set_collection_due_date = "";
						$set_collection_minimum = "";
						
						$collection_history = "";
					}
					if($set_collection_extend_days=="") $set_collection_extend_days = 14;
					
					$str .= "<table><tr><td bgcolor='#999999' style='color:#ffffff' width='600' align='center'><b>" . $rest_read['company_name'] . "</b></td></tr></table>";
					
					$str .= $post_message;
					$str .= "<form name='collect' method='post' action='psetup.php?$fwd_vars'>";
					$str .= "<input type='hidden' name='posted' value='1'>";
					
					if($set_hosting_taken=="1") $str .= "<b><font color='#008800'>Hosting has been scheduled</font></b><br>";
					if($set_collection_taken=="1") $str .= "<b><font color='#008800'>Payment has been collected</font></b><br>";
					
					$str .= "<table>";
					
					$found_hosting_option = false;
					$str .= "<tr><td align='right'>Change Hosting:</td><td><select name='hosting'>";
					$str .= "<option value=''></option>";
					$str .= "<option value='new_hosting'".($set_hosting=="new_hosting"?" selected":"").">Create New Hosting</option>";
					for($n=0; $n<count($active_subscriptions); $n++)
					{
						$asubid = $active_subscriptions[$n];
						$str .= "<option value='$asubid'".($set_hosting=="$asubid"?" selected":"").">Replace $asubid</option>";
						if($set_hosting==$asubid) $found_hosting_option = true;
					}
					if(count($active_subscriptions) > 1)
					{
						$str .= "<option value='replace_all'".($set_hosting=="replace_all"?" selected":"").">Replace All Hosting</option>";
					}
					if(!$found_hosting_option && $set_hosting!="" && $set_hosting!="new_hosting" && $set_hosting!="replace_all")
					{
						$show_set_hosting = $set_hosting;
						if($set_hosting=="replace_all") $show_set_hosting= "Replace All Hosting";
						else if(is_numeric($set_hosting)) $show_set_hosting = "Replace " . $set_hosting . " (x)";
						$str .= "<option value='$set_hosting' selected>$show_set_hosting</option>";
					}
					$str .= "</select></td><td>Hosting Amount:<input type='text' name='new_amount' value=\"".str_replace("\"","&quot;",$set_new_amount)."\" /></td>";
					$str .= "<td><select name='hosting_start'>";
					
					$hdate_found = false;
					$hstr = "";
					for($n=1; $n<=63; $n++)
					{
						$hdate = mktime(0,0,0,date("m"),date("d") + $n,date("Y"));
						$hdate_full = date("Y-m-d",$hdate);
						$hdate_short = date("M d",$hdate);
						$hstr .= "<option value='$hdate_full'";
						if($hdate_full==$set_hosting_start)
						{
							$hdate_found = true;
							$hstr .= " selected";
						}
						$hstr .= ">$hdate_short</option>";
					}
					if(!$hdate_found && $set_hosting_start!="")
					{
						$seths_parts = explode("-",$set_hosting_start);
						$set_hosting_short = date("M d",mktime(0,0,0,$seths_parts[1],$seths_parts[2],$seths_parts[0]));
						$hstr = "<option value='$set_hosting_start' checked>$set_hosting_short</option>" . $hstr;
					}
					$hstr = "<option value=''>--- choose date ---</option>" . $hstr;
					$str .= $hstr;
					
					$str .= "</select></td></tr>";
					$str .= "<tr><td align='right'>Money to Collect:</td><td><input type='text' name='collect' value=\"".str_replace("\"","&quot;",$set_collect)."\" /></td>";
					$str .= "<td rowspan='2' colspan='2'><textarea rows='4' cols='48' name='status_notes'>$set_status_notes</textarea></td>";
					$str .= "</tr>";
					$str .= "<tr><td align='right'>Confirm OK:</td><td><input type='checkbox' name='confirm_ok'".($set_ok=="1"?" checked":"")." /></td></tr>";
					
					$str .= "<table width='680' cellspacing=0 cellpadding=4 style='border:solid 1px #bbbbbb' bgcolor='#eeeeee'><tr><td align='left' valign='top'>";
					$str .= "<input type='button' value='Submit' onclick='document.collect.submit()'>";
					$str .= "</td><td align='right' valign='top'>";
					
					$str .= "<table cellspacing=0>";
					$str .= "<tr><td align='right'>Collection Active:</td><td><input type='checkbox' name='collection_active'".($set_collection_active=="1"?" checked":"")." /></td></tr>";
					$str .= "<tr><td align='right'>Collection Minimum:</td><td><input type='text' name='collection_minimum' value='".$set_collection_minimum."' /></td></tr>";
					$str .= "<tr><td align='right'>Collection Due Date:</td><td>";
					
					// collection due date
					//$str .= "<input type='text' name='collection_due_date' value='".$set_collection_due_date."' />";
					$option_date_found = false;
					$str .= "<select name='collection_due_date'>";
					$str .= "<option value=''></option>";
					for($n=-1; $n<=30; $n++)
					{
						$set_due_date_ts = mktime(0,0,0,date("m"),date("d") + $n,date("Y"));
						$set_due_date_option = date("Y-m-d",$set_due_date_ts);
						
						$show_option = date("m/d",$set_due_date_ts) . " ";
						if($n==0) $show_option .= "(Today)";
						else if($n==1) $show_option .= "(in 1 Day)";
						else if($n > 1) $show_option .= "(in $n Days)";
						
						$str .= "<option value='$set_due_date_option'";
						if($set_collection_due_date==$set_due_date_option)
						{
							$str .= " selected";
							$option_date_found = true;
						}
						$str .= ">$show_option</option>";
					}
					if($set_collection_due_date!="" && !$option_date_found)
						$str .= "<option value='$set_collection_due_date'>$set_collection_due_date</option>";
					$str .= "</select>";
					
					$str .= "</td></tr>";
					$str .= "<tr><td align='right'>Collection Extend On Payment:</td><td>";
					
					// collection extend date
					$str .= "<select name='collection_extend_days'>";
					for($n=1; $n<30; $n++)
						$str .= "<option value='$n'".(($set_collection_extend_days==$n)?" selected":"").">$n Days</option>";
					$str .= "</select>";
					
					$str .= "</td></tr>";
					$str .= "</table>";
					
					$str .= "</td></tr></table>";
					
					$str .= "</td></tr>";
					
					//$str .= "<tr><td>&nbsp;</td><td><input type='button' value='Submit' onclick='document.collect.submit()'></td></tr>";
					$str .= "</table>";
					$str .= "</form>";
					$str .= "<br><br><input type='button' value='Clear out Collection Activity' onclick='if(confirm(\"Are you sure you want to do this $loggedin_fullname?\")) window.location = \"psetup.php?clear_out=1&$fwd_vars\"' />";
					$str .= "<br><br>Clear History:<br>";
					$str .= "<textarea rows='42' cols='88'>" . $collection_history . "</textarea>";
				}
			}
			else if($type=="capture" || $type=="decline")
			{				
				$response_search = "`x_response_code`='1'";
				if($type=="decline")
					$response_search = "`x_response_code`!='1'";
				$res_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='' and $response_search and `x_type`='auth_capture' order by `x_amount` * 1 desc, id desc");
				while($res_read = mysqli_fetch_assoc($res_query))
				{
					$rest_id = false;
					$signup_read = false;
					$find_method = "";
					
					if($find_method=="" && $res_read['id']==$update_responseid && $set_restid!="")
					{
						$rest_id = $set_restid;
						$find_method = "manual";
						//$str .= "<br>find_method: $find_method rest_id: $rest_id ";
					}
					
					if($find_method=="" && $res_read['x_subscriptionid']!="")
					{
						$signup_read = get_subscription_signup($res_read);
						if($signup_read)
							$find_method = "subscription match";
					}
					
					if($find_method=="" && $res_read['x_city']!="")
					{
						$signup_read = get_signup_by_name_and_city($res_read);
						if($signup_read)
							$find_method = "name city match";
					}
										
					if(!$rest_id && $find_method!="" && is_array($signup_read) && isset($signup_read['dataname']))
					{
						$rest_id = get_restaurant_id($signup_read['dataname']);
					}
					
					if(!$rest_id && substr($res_read['x_description'],0,17)=="POSLavu Payment L")
					{
						$payment_request_id = substr($res_read['x_description'],17);
						$rest_id = get_payment_request_rest_id($payment_request_id);
						$find_method = "payment request match";
					}						
										
					if(!$rest_id && substr($res_read['x_description'],0,14)=="POSLavu Auth R")
					{
						$rest_id = substr($res_read['x_description'],14);
						$find_method = "payment collect match";
					}
										
					if(!$rest_id && $res_read['x_account_number']!="" && $res_read['x_first_name']!="" && $res_read['x_last_name']!="")
					{
						$find_method = "";
						$match_read = get_response_by_name_and_card($res_read);
						if($match_read && is_array($match_read) && $match_read['match_restaurantid']!="")
						{
							$rest_id = $match_read['match_restaurantid'];
							$find_method = "name card match";
						}
					}

					if(!$rest_id && $res_read['x_company']!="" && $res_read['x_first_name']!="" && $res_read['x_last_name']!="")
					{
						$find_method = "";
						$match_read = get_response_by_name_and_company($res_read);
						if($match_read && is_array($match_read) && $match_read['match_restaurantid']!="")
						{
							$rest_id = $match_read['match_restaurantid'];
							$find_method = "name company match";
						}
					}
						
					if($rest_id && $find_method!="")
					{
						$success = set_response_restaurant_id($res_read['id'],$rest_id,$find_method);
						$str .= "found signup ($find_method): " . $res_read['datetime'] . " - " . $res_read['x_amount'] . " - " . $res_read['x_description'] . " - " . $res_read['x_first_name'] . " - " . $res_read['x_last_name']. " " . $res_read['x_city'] . "<br>";
					}
					else if($res_read['x_amount'] < 5)
					{
						$success = set_response_restaurant_id($res_read['id'],0,"small transaction");
						$str .= "small transaction: " . $res_read['datetime'] . " - " . $res_read['x_amount'] . "<br>";
					}
					else
					{
						$set_input_id = "input_set_restid_" . $res_read['id'];
						$str .= "<input style='font-size:10px' type='text' value='' id='$set_input_id' name='$set_input_id' /><input style='font-size:10px' type='button' value='update' onclick='window.location = \"psetup.php?update_resp=".$res_read['id']."&set_value=\" + document.getElementById(\"$set_input_id\").value' />&nbsp;";
						$str .= "couldn't find: " . $res_read['datetime'] . " - " . $res_read['x_company'] . " " . $res_read['x_first_name'] . " " . $res_read['x_last_name'] . " " . trim($res_read['x_card_type'] . " " . $res_read['x_account_number']) . " - " . $res_read['x_amount'] . " " . trim($res_read['x_first_name'] . " " . $res_read['x_last_name'] . " " . $res_read['x_company']). " " . $res_read['x_description'] . "<br>";
						
						$find_date = substr($res_read['datetime'],0,10);
						
						/*$signup_query = mlavu_query("select * from signups where `date`='[1]' and `dataname`!='' order by `company` asc",$find_date);
						while($signup_read = mysqli_fetch_assoc($signup_query))
						{
							$rest_query = mlavu_query("select * from `restaurants` where `data_name`='[1]'",$signup_read['dataname']);
							if(mysqli_num_rows($rest_query))
							{
								$rest_read = mysqli_fetch_assoc($rest_query);
								//$str .= $rest_read['id'] . ")))";
								$str .= $signup_read['company'] . " ";
								
								if($rest_read['disabled']=="1") $str .= "<font color='red'>disabled</font> ";
								else $str .= "<font color='green'>enabled</font> ";
								$paid_max_query = mlavu_query("select * from `payment_responses` where `match_restaurantid`='[1]' and x_response_code='1' and `x_amount`!='' and `x_amount` > 0 order by `x_amount` * 1 desc limit 1",$rest_read['id']);
								if(mysqli_num_rows($paid_max_query))
								{
									$paid_max_read = mysqli_fetch_assoc($paid_max_query);
									$str .= $paid_max_read['x_amount'];
								}
								else
								{
									$str .= "<b>No payments found</b> ";
								}
								if($rest_read['notes']!="") $str .= " - <font style='font-size:9px'>".$rest_read['notes'] ."</font>";
								//if($signup_read['notes']!="") $str .= " - <font style='font-size:9px'>".$rest_read['notes'] ."</font>";
								$str .= "<br>";
							}
						}*/
						
						$rqstr = "";
						$find_date_parts = explode("-",substr($res_read['datetime'],0,10));
						$find_date_min_ts = mktime(0,0,0,$find_date_parts[1] - 1,$find_date_parts[2],$find_date_parts[0]);
						$find_date_min = date("Y-m-d",$find_date_min_ts) . " 00:00:00";
						$find_date_max_ts = mktime(0,0,0,$find_date_parts[1],$find_date_parts[2] + 7,$find_date_parts[0]);
						$find_date_max = date("Y-m-d",$find_date_max_ts) . " 00:00:00";
						
						/*if($res_read['x_amount'] > 100)
						{
							$rest_query = mlavu_query("select * from `restaurants` where `created`>='[1]' and `created`<='[2]' and `data_name`!='' order by `company_name` asc",$find_date_min,$find_date_max);
							while($rest_read = mysqli_fetch_assoc($rest_query))
							{
								$request_query = mlavu_query("select * from `payment_request` where `restaurantid`='[1]' and `license_amount`='[2]' or `hosting_amount`='[2]'",$rest_read['id'],$res_read['x_amount']);
								if(mysqli_num_rows($request_query))
								{
									$request_read = mysqli_fetch_assoc($request_query);
									$rqstr .= $rest_read['company_name'] . " - " . $request_read['action'] . " " . $request_read['notes'] . " " . $request_read['email'];
									$rqstr .= "<br>";
								}
								//$rqstr .= $rest_read['company_name'] . ", ";
							}
						}
						
						if($rqstr!="")
						{
							$str .= "<div style='border:solid 1px #bbbbbb'>";
							$str .= $rqstr;
							$str .= "</div>";
						}*/
					}
				}
			}
			
			return $str;
		}
		
		echo fill_response_fields();
		//echo process_system_payments("subscriptions"); // 22 not found
		//echo process_system_payments("payment_links");
		echo process_system_payments((isset($_GET['type']))?$_GET['type']:"capture");
		//echo process_system_payments("refunds");
		
		function v2_setup_payments()
		{
			echo "<table>";
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` order by id desc limit 400");
			while($response_read = mysqli_fetch_assoc($response_query))
			{
				$response_text = $response_read['response'];
				$datetime = trim($response_read['date'] . " " . $response_read['time']);
				$ts = $response_read['ts'];
				$pay_info = parse_payment_response($response_text);
				//set_pay_info($response_text);
				
				$x_description = get_pay_info($pay_info,"x_description");
				$x_amount = get_pay_info($pay_info,"x_amount");
				$x_type = get_pay_info($pay_info,"x_type");
				$x_subscriptionid = get_pay_info($pay_info,"x_subscription_id");
				
				$x_first_name = get_pay_info($pay_info,"x_first_name");
				$x_last_name = get_pay_info($pay_info,"x_last_name");
				$x_company = get_pay_info($pay_info,"x_company");
				$x_city = get_pay_info($pay_info,"x_city");
				$x_state = get_pay_info($pay_info,"x_state");
				$x_phone = get_pay_info($pay_info,"x_phone");
				$x_email = get_pay_info($pay_info,"x_email");
				
				$x_account_number = get_pay_info($pay_info,"x_account_number");
				$x_card_type = get_pay_info($pay_info,"x_card_type");
				$x_response_code = get_pay_info($pay_info,"x_response_code");
				if($x_response_code=="1") $x_show_response = "<font color='#008800'>accepted</font>";
				else $x_show_response = "<font color='#880000'>declined</font>";
				
				if($x_response_code=="1")
				{
					$flist = array();
					$flist["x_description"] = $x_description;
					$flist["x_amount"] = $x_amount;
					$flist["x_type"] = $x_type;
					$flist["x_subscriptionid"] = $x_subscriptionid;
					$flist["x_first_name"] = $x_first_name;
					$flist["x_last_name"] = $x_last_name;
					$flist["x_company"] = $x_company;
					$flist["x_city"] = $x_city;
					$flist["x_state"] = $x_state;
					$flist["x_phone"] = $x_phone;
					$flist["x_email"] = $x_email;
					$flist["x_account_number"] = $x_account_number;
					$flist["x_card_type"] = $x_card_type;
					$flist["x_response_code"] = $x_response_code;
					
					if($response_read['x_response_code']=="" && $response_read['x_type']=="")
					{
						$update_code = "";
						foreach($flist as $key => $val)
						{
							if($update_code!="") $update_code .= ",";
							$update_code .= "`$key`='[$key]'";
						}
						$flist["id"] = $response_read['id'];
						$success = mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set " . $update_code . " where `id`='[id]'",$flist);
						echo "updated payment response data " . $response_read['id'];
						if($success) echo "(success)"; else echo "(failed) <br>$update_code";
						echo "<br>";
					}
					
					$flist['ts'] = $ts;
					$flist["datetime"] = $datetime;
					
					$display_trans = false;
					$extra_info = "";
					if($x_type=="credit")
					{
						//$display_trans = true;
						$prev_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_amount`>='[x_amount]' and `x_card_type`='[x_card_type]' and `x_account_number`='[x_account_number]' and `ts`<'[ts]' and `x_first_name`='[x_first_name]' and `x_last_name`='[x_last_name]' order by x_amount asc",$flist);
						if(mysqli_num_rows($prev_query))
						{
							$extra_info = "found: " . mysqli_num_rows($prev_query);
							while($prev_read = mysqli_fetch_assoc($prev_query))
							{
								$extra_info .= "<br>" . $prev_read['x_amount'] . " " . $prev_read['date'] . " " . $prev_read['time'] . " " . $prev_read['x_first_name'] . " " . $prev_read['x_last_name'];
							}
							//$extra_info = "original transaction found";
						}
						else
						{
							$extra_info = "match not found";
							$extra_info .= "<br>amount: " . $flist['x_amount'] . "<br>card_type: " . $flist['x_card_type'] . "<br>account_number: " . $flist['x_account_number'];
							$extra_info .= "<br>" . output_pay_info_contents($pay_info);
						}
					}
					else if($x_type=="auth_capture" && $x_subscriptionid!="")
					{
						//$display_trans = true;
						$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `arb_license_id`='[1]' or `arb_hosting_id`='[1]'",$x_subscriptionid);
						if(mysqli_num_rows($signup_query))
						{
							$signup_read = mysqli_fetch_assoc($signup_query);
							$extra_info = "signup found<br>" . $signup_read['firstname'] . " " . $signup_read['lastname'];
						}
						else 
						{
							$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `firstname`='[1]' and `lastname`='[2]' and `city`='[3]' and `city`!='' and `firstname`!=''",$x_first_name,$x_last_name,$x_city);
							if(mysqli_num_rows($signup_query))
							{
								$signup_read = mysqli_fetch_assoc($signup_query);
								$extra_info = "Name Match!";
							}
							else
							{
								//$display_trans = true;
								$extra_info = "signup not found";
							}
						}
					}
					else if($x_type=="auth_capture" && $x_amount * 1 > 1)
					{					
						if(strpos($x_description,"POSLavu Payment L")!==false)
						{
						}
						else
						{
							//$display_trans = true;
							$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `firstname`='[1]' and `lastname`='[2]' and `city`='[3]' and `city`!='' and `firstname`!=''",$x_first_name,$x_last_name,$x_city);
							if(mysqli_num_rows($signup_query))
							{
								$signup_read = mysqli_fetch_assoc($signup_query);
								$extra_info = "Name Match!";
							}
							else
							{
								$display_trans = true;
								
								$lastfour = "";
								if(strlen($x_account_number) > 4)
								{
									$lastfour = substr($x_account_number,strlen($x_account_number) - 4);
								}
								
								$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `card_number`='[1]' and `card_number`!='' and `dataname`!=''",$lastfour);
								if(mysqli_num_rows($signup_query))
								{
									$signup_read = mysqli_fetch_assoc($signup_query);
									$extra_info = "CC MATCH! - " . $signup_read['dataname'];
								}
								else
									$extra_info = "payment: signup not found - $lastfour";
							}
						}
					}
					
					//if($x_type!="auth_only") $display_trans = true;
					
					if($display_trans)
					{
						echo "<tr>";
						echo show_col($datetime);
						echo show_col($x_type);
						echo show_col($x_show_response);
						echo show_col($x_amount);
						echo show_col($x_subscriptionid);
						echo show_col($x_description);
						echo show_col($x_first_name);
						echo show_col($x_last_name);
						echo show_col($x_city);
						echo show_col($s_state);
						echo show_col($x_account_number);
						echo "</tr>";
						
						if($extra_info!="")
						{
							echo "<tr>";
							echo "<td colspan='12' align='center'>";
							echo $extra_info;
							echo "</td>";
							echo "</tr>";
						}
					}
				}
			}
			echo "</table>";
		}
	}
?>
