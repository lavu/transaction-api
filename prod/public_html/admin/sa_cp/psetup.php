<?php
	session_start();
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/admin/sa_cp/presponse_functions.php");
	require_once(dirname(__FILE__).'/../manage/can_access.php');
	$maindb = "poslavu_MAIN_db";
	
	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;
	$loggedin_fullname = (isset($_SESSION['posadmin_fullname']))?$_SESSION['posadmin_fullname']:false;
	
	if(can_access("technical"))
	{
		/*function parse_payment_response($response)
		{
			$vars = array();
			$response = explode("\n",$response);
			for($i=0; $i<count($response); $i++)
			{
				$line = explode("=",$response[$i]);
				if(count($line) > 1)
				{
					$vars[trim($line[0])] = trim($line[1]);
				}
			}
			return $vars;
		}*/
		function output_pay_info_contents($pay_info)
		{
			$str = "";
			$str .= "<table cellspacing=0 cellpadding=2>";
			foreach($pay_info as $key => $val)
			{
				$str .= "<tr><td align='right' style='border:solid 1px black'>$key</td><td>" . str_replace("<","&lt;",$val) . "</td></tr>";
			}
			$str .= "</table>";
			$str .= "<hr>";
			
			return $str;
		}
		function descriptor_restid($pay_info)
		{
			$xdesc = $pay_info['x_description'];
			if(substr($xdesc,0,17)=="POSLavu Payment L")
			{
				$reqid_parts = explode("Payment L",$xdesc);
				if(count($reqid_parts) > 1)
				{
					$reqid = $reqid_parts[1];
					$request_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_request` where `id`='[1]'",$reqid);
					if(mysqli_num_rows($request_query))
					{
						$request_read = mysqli_fetch_assoc($request_query);
						$restid = $request_read['restaurantid'];
						return $restid;
					}
				}
			}
			return "";
		}
		
		$mode = (isset($_GET['mode']))?$_GET['mode']:"assign";
		if($mode=="assign")
		{
			require_once("psetup_payments.php");
		}
		else if($mode=="test_connector")
		{
			require_once(dirname(__FILE__) . "/presponse_functions.php");
			$response_id = 14610;
			echo "Response:<br>" . connect_response_to_restaurant($response_id);
		}
		else if($mode=="locations")
		{
			function update_restaurant_location_data($rest_read,$loc_read)
			{
				$rsp = "";
				$restid = $rest_read['id'];
				$dataname = $rest_read['data_name'];
				$dbname = "poslavu_".$dataname."_db";
				
				$flist1 = array("title","address","city","state","zip","country","timezone","phone","email","website","manager");
				$flist2 = array("taxrate","net_path","use_net_path","component_package","component_package2","product_level","gateway");
				$flist3 = array("lavu_togo_name");
				$flist = array_merge($flist1,$flist2);
	
				$setlist = array();
				$setlist[] = array("dataname",$dataname);
				$setlist[] = array("restaurantid",$restid);
				$setlist[] = array("locationid",$loc_read['id']);
				$setlist[] = array("last_activity",$rest_read['last_activity']);
				$setlist[] = array("disabled",$rest_read['disabled']);
				
				for($n=0; $n<count($flist); $n++)
				{
					$fieldname = $flist[$n];
					$setlist[] = array($fieldname,$loc_read[$fieldname]);
				}
				for($n=0; $n<count($flist3); $n++)
				{
					$flist_name = $flist3[$n];
					$config_query = mlavu_query("select `value` from `[1]`.`config` where `type`='location_config_setting' and `setting`='[2]' and `location`='[3]'",$dbname,$flist_name,$loc_read['id']);
					if(mysqli_num_rows($config_query))
					{
						$config_read = mysqli_fetch_assoc($config_query);
						$setlist[] = array($flist_name,$config_read['value']);
					}
				}
				
				$field_code = "";
				$value_code = "";
				$update_code = "";
				$update_vals = array();
				for($n=0; $n<count($setlist); $n++)
				{
					$fieldname = $setlist[$n][0];
					$fieldvalue = $setlist[$n][1];
					
					if($field_code!="") $field_code .= ",";
					if($value_code!="") $value_code .= ",";
					if($update_code!="") $update_code .= ",";
					
					$update_vals[$fieldname] = $fieldvalue;
					$field_code .= "`$fieldname`";
					$value_code .= "'[$fieldname]'";
					$update_code .= "`$fieldname`='[$fieldname]'";
				}
				
				$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurant_locations` where `dataname`='[1]' and `restaurantid`='[2]' and `locationid`='[3]'",$dataname,$restid,$loc_read['id']);
				if(mysqli_num_rows($exist_query))
				{
					$exist_read = mysqli_fetch_assoc($exist_query);
					$update_vals['existid'] = $exist_read['id'];
					mlavu_query("update `poslavu_MAIN_db`.`restaurant_locations` set " . $update_code . " where `id`='[existid]'",$update_vals);
					$rsp .= "<font color='#000088'>Updating " . $dataname . " - location: " . $loc_read['id'] . " - " . $loc_read['title'] . "</font><br>";
				}
				else
				{
					mlavu_query("insert into `poslavu_MAIN_db`.`restaurant_locations` ($field_code) values ($value_code)",$update_vals);
					$rsp .= "<font color='#008800'>Inserting into " . $dataname . " - location: " . $loc_read['id'] . " - " . $loc_read['title'] . "</font><br>";
				}
				return $rsp;
			}
			
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` order by id asc");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				$restid = $rest_read['id'];
				$dataname = $rest_read['data_name'];
				$dbname = "poslavu_".$dataname."_db";
				
				$loc_query = mlavu_query("select * from `[1]`.`locations` order by id asc",$dbname);
				while($loc_read = mysqli_fetch_assoc($loc_query))
				{
					echo update_restaurant_location_data($rest_read,$loc_read);
				}
			}
		}
		else if($mode=="count")
		{
			$str = "";
			$str .= "<table>";
			// payment_profile: type, restaurantid, dataname, subscriptionid, profile_start, amount, last_charge, active, id
			/*----------- payment_history -----------
			datetime
			subscriptionid
			profileid
			amount
			result
			notes
			id*/
			//$profile_query = mlavu_query("select type, count(*) from `poslavu_MAIN_db`.`payment_profile` group by type");
			$recent_payment_count = 0;
			$payment_decline_count = 0;
			$profile_count = 0;
			$month_ago = date("Y-m-d",mktime(0,0,0,date("m")-1,date("d"),date("Y"))) . " 00:00:00";
			echo "month ago: $month_ago <br>";
			$profile_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_profile` where `type`='hosting'");
			while($profile_read = mysqli_fetch_assoc($profile_query))
			{
				/*foreach($profile_read as $pkey => $pval)
				{
					$str .= "<tr style='border-bottom:solid 1px black'><td align='right'>" . $pkey . "</td><td>" . $pval . "</td></tr>";
				}*/
				$hosting_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_history` where `profileid`='[1]' and `result`='1' and `datetime` >= '$month_ago'",$profile_read['id']);
				$recent_payment_count += mysqli_num_rows($hosting_query);
				
				$hosting_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_history` where `profileid`='[1]' and `result`='2' and `datetime` >= '$month_ago'",$profile_read['id']);
				$payment_decline_count += mysqli_num_rows($hosting_query);
				
				$profile_count++;
			}
			$str .= "</table>";
			echo "<br>Profile Count: $profile_count<br>";
			echo "<br>Recent Hosting Payment: $recent_payment_count<br>";
			echo "<br>Decline Count: $payment_decline_count<br>";
			//echo $str;
		}
		else if($mode=="list")
		{
			$rid_counts = array();
			$rid_info = array();
			$rid_counts[516] = 0;
			//requestid=$reqid&set_restid=			
			if(isset($_GET['requestid']) && isset($_GET['set_restid']))
			{
				mlavu_query("update `poslavu_MAIN_db`.`payment_request` set `set_restid`='[1]' where `id`='[2]'",$_GET['set_restid'],$_GET['requestid']);
			}
			
			$preq_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` order by id desc");
			while($preq_read = mysqli_fetch_assoc($preq_query))
			{
				$display_row = false;
				$str = "";
				
				$str .= "<table>";
				foreach($preq_read as $key => $val)
				{
					if($key=="response")
					{
						$pay_info = parse_payment_response($val);
						$xdesc = $pay_info['x_description'];
						if(substr($xdesc,0,17)=="POSLavu Payment L")
						{
							$reqid_parts = explode("Payment L",$xdesc);
							if(count($reqid_parts) > 1)
							{
								$reqid = $reqid_parts[1];
								$request_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_request` where `id`='[1]'",$reqid);
								if(mysqli_num_rows($request_query))
								{
									$request_read = mysqli_fetch_assoc($request_query);
									$restid = $request_read['restaurantid'];
									$xd = $restid;
																		
									if(!isset($rid_counts[$xd])) $rid_counts[$xd] = 0;
									$rid_counts[$xd]++;
									if(!isset($rid_info[$xd])) $rid_info[$xd] = array();
									$rid_info[$xd][] = array_merge($pay_info,array("payment_request_id"=>$request_read['id'],"payment_response_id"=>$preq_read['id'],"set_restid"=>$request_read['set_restid'],"notes"=>$request_read['notes']));
									
									$display_row = true;
									foreach($pay_info as $pkey => $pval)
									{
										$str .= "<tr style='border-bottom:solid 1px black'><td align='right'>" . $pkey . "</td><td>" . $pval . "</td></tr>";
									}
								}
							}
						}
					}
					else
					{
						$str .= "<tr style='border-bottom:solid 1px black'><td align='right'>" . $key . "</td><td>" . $val . "</td></tr>";
					}
				}
				$str .= "</table>";
				$str .= "<hr>";
				
				if($display_row) 
				{
					//echo $str;
				}
			}
			
			$rcount = 0;
			echo "<table>";
			foreach($rid_counts as $key => $val)
			{
				echo "<tr>";
				$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$key);
				if(mysqli_num_rows($rest_query))
				{
					$rest_read = mysqli_fetch_assoc($rest_query);
					
					echo "<td valign='top'><u>" . $rest_read['company_name'] . " (" . $rest_read['id'] . ")</u></td>";
					// = <b>" . $val . "</b><br>";
					echo "<td valign='top'>";
					echo "<table>";
					for($n=0; $n<count($rid_info[$key]); $n++)
					{
						$pay_info = $rid_info[$key][$n];
						$rcount++;
						$reqid = $pay_info['payment_request_id'];
						
						echo "<tr>";
						echo "<td><input type='text' style='font-size:9px' size='4' value='".$pay_info['set_restid']."' id='update_restid_$rcount' /><input type='button' value='update' style='font-size:9px' onclick='window.location = \"psetup.php?requestid=$reqid&set_restid=\" + document.getElementById(\"update_restid_$rcount\").value' /></td>";
						
						if($pay_info['x_response_code']==1) $style = " bgcolor='#008800' style='color:#ffffff; font-weight:bold'"; else $style = "";
						$style = "valign='top' " . $style;
						//echo "<td $style>" . $pay_info['x_response_code'] . "</td>";
						echo "<td $style>" . $pay_info['x_type'] . "</td>";
						echo "<td $style>" . $pay_info['x_amount'] . "</td>";
						echo "<td $style>" . $pay_info['date'] . "</td>";
						echo "<td $style>" . $pay_info['x_response_reason_text'] . "</td>";
						echo "<td $style>" . $pay_info['x_card_type'] . "</td>";
						echo "<td $style>" . $pay_info['x_account_number'] . "</td>";
						echo "<td $style>" . $pay_info['notes'] . "</td>";
						//echo "<td $style>" . $pay_info['x_company'] . "</td>";
						//echo "<td $style>" . trim($pay_info['x_first_name'] . " " . $pay_info['x_last_name']) . "</td>";
						echo "</tr>";
					}
					echo "</table>";
					echo "</td>";
				}
				echo "</tr>";
			}
			echo "</table>";
		}
		else if($mode=="process") //613 dakota 4-8 - bread
		{
			$limit_code = "";
			//$limit_code = " limit 200";
		
			$no_signup_count = 0;
			$no_signup_ids = array();
			$no_signup_info = array();
			$preq_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `restaurantid`='' order by id desc" . $limit_code);
			while($preq_read = mysqli_fetch_assoc($preq_query))
			{
				$responseid = $preq_read['id'];
				$datetime = $preq_read['date'] . " " . $preq_read['time'];
				$pay_info = parse_payment_response($preq_read['response']);

				$card_type = (isset($pay_info['x_card_type']))?$pay_info['x_card_type']:"";
				$last_four = (isset($pay_info['x_account_number']))?$pay_info['x_account_number']:"";
				
				$restaurantid = "";
				$subscriptionid = "";
				$row_output = "";
				$extra_msg = "";
				if($pay_info['x_type']=="auth_only")
				{
				}
				else if($pay_info['x_response_code']!=1)
				{
				}
				else if(!isset($pay_info['x_subscription_id'])) // PROCESS PAYMENTS WITH NO SUBSCRIPTION IDS
				{
					$restaurantid = descriptor_restid($pay_info);
					$row_output = $preq_read['date'] . " <font color='blue'>(no subscription)</font> <b>" . $pay_info['x_type'] . "</b> " . $pay_info['x_amount'] . " " . $pay_info['x_description'] . ($pay_info['x_response_code']==2?" <font color='red'>failed</font>":"");
					
					if((isset($pay_info['x_type'])) && $pay_info['x_type']=="credit")
					{
						$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_history` where `card_type`='[1]' and `last_four`='[2]' and `restaurantid`!='' and `datetime`<'[3]'",$card_type,$last_four,$preq_read['date'] . " 24:00:00");
						if(mysqli_num_rows($rest_query))
						{
							$rest_read = mysqli_fetch_assoc($rest_query);
							
							mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `match`='matched by cc - credit', `restaurantid`='[1]' where `id`='[2]' and `restaurantid`=''",$rest_read['restaurantid'],$preq_read['id']);
							//$row_output .= " - <b>found rest(".$rest_read['id'].")</b> - " . $pay_info['x_type'];
						}
					}
					$row_output .= output_pay_info_contents($pay_info);
				}
				else // PROCESS PAYMENTS WITH SUBSCRIPTION IDS
				{
					$dataname = "";
					$subscriptionid = $pay_info['x_subscription_id'];
					$amount = $pay_info['x_amount'];
					$active = 1;
					
					//echo output_pay_info_contents($pay_info);
					$row_output = "<font color='green'>(subscription: $subscriptionid)</font> <b>" . $pay_info['x_type'] . "</b> " . $pay_info['x_amount'] . " " . $pay_info['x_description'] . ($pay_info['x_response_code']==2?" <font color='red'>failed</font>":"");
					
					if($subscriptionid != "")
					{
						$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `arb_license_id`='[1]' or `arb_hosting_id`='[1]'",$subscriptionid);
						if(mysqli_num_rows($signup_query))
						{
							$signup_read = mysqli_fetch_assoc($signup_query);
							$dataname = $signup_read['dataname'];
							
							if($signup_read['arb_license_id']==$subscriptionid) $type = "license";
							else if($signup_read['arb_hosting_id']==$subscriptionid) $type = "hosting";
							
							$restaurantid = "";
							$profile_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_profile` where `subscriptionid`='[1]'",$subscriptionid);
							if(mysqli_num_rows($profile_query))
							{
								$profile_read = mysqli_fetch_assoc($profile_query);
								$profileid = $profile_read['id'];
								$restaurantid = $profile_read['restaurantid'];
							}
							else
							{
								$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
								if(mysqli_num_rows($rest_query))
								{
									$rest_read = mysqli_fetch_assoc($rest_query);
									$restaurantid = $rest_read['id'];
								}
								
								$vars = array();
								$vars['type'] = $type;
								$vars['restaurantid'] = $restaurantid;
								$vars['subscriptionid'] = $subscriptionid;
								$vars['dataname'] = $dataname;
								$vars['profile_start'] = "";
								$vars['amount'] = $amount;
								$vars['active'] = $active;
								$vars['last_charge'] = "";
								
								echo "<br>creating profile for " . $dataname . " - " .$type . " - " . $subscriptionid . " - " . $amount;
								mlavu_query("insert into `poslavu_MAIN_db`.`payment_profile` (`type`,`restaurantid`,`subscriptionid`,`dataname`,`profile_start`,`amount`,`active`,`last_charge`) values ('[type]','[restaurantid]','[subscriptionid]','[dataname]','[profile_start]','[amount]','[active]','[last_charge]')",$vars);
								
								$profile_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_profile` where `subscriptionid`='[1]'",$subscriptionid);
								if(mysqli_num_rows($profile_query))
								{
									$profile_read = mysqli_fetch_assoc($profile_query);
									$profileid = $profile_read['id'];
								}							
							}
							
							/*----------- payment_history -----------
							datetime
							subscriptionid
							profileid
							restaurantid
							amount
							result
							notes
							id*/
							
							$payment_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_history` where `subscriptionid`='[1]' and `datetime`='[2]' and `amount`='[3]'",$subscriptionid,$datetime,$amount);
							if(mysqli_num_rows($payment_query))
							{
								$payment_read = mysqli_fetch_assoc($payment_query);
								/*mlavu_query("update `poslavu_MAIN_db`.`payment_history` set `restaurantid`='[1]' where `id`='[2]'",$restaurantid,$payment_read['id']);
								echo "<br>setting restaurantid to " . $restaurantid . " for " . $payment_read['id'];*/
								//mlavu_query("update `poslavu_MAIN_db`.`payment_history` set `responseid`='[1]',`card_type`='[2]',`last_four`='[3]' where `id`='[4]'",$responseid,$card_type,$last_four,$payment_read['id']);
								//echo "<br>setting responseid to " . $responseid . " for " . $payment_read['id'] . " ($card_type $last_four)";
							}
							else
							{
								$vars['subscriptionid'] = $subscriptionid;
								$vars['restaurantid'] = $restaurantid;
								$vars['datetime'] = $datetime;
								$vars['profileid'] = $profileid;
								$vars['amount'] = $amount;
								$vars['result'] = $pay_info['x_response_code'];
								$vars['responseid'] = $responseid;
								$vars['card_type'] = $card_type;
								$vars['last_four'] = $last_four;
																
								$set_notes = "";
								if(isset($pay_info['x_account_number'])) $set_notes .= "cc# " . $pay_info['x_account_number'];
								$vars['notes'] = $set_notes;
								
								mlavu_query("insert into `poslavu_MAIN_db`.`payment_history` (`subscriptionid`,`restaurantid`,`datetime`,`profileid`,`amount`,`result`,`responseid`,`card_type`,`last_four`) values ('[subscriptionid]','[restaurantid]','[datetime]','[profileid]','[amount]','[result]','[responseid]','[card_type]','[last_four]')",$vars);
								echo "<br>adding payment for " . $dataname . " - " .$datetime . " - " . $amount . " - " . $set_notes;
							}							
						}
						else 
						{
							$no_signup_count++;
							$no_signup_info[$subscriptionid] = $pay_info;
							if(isset($no_signup_ids[$subscriptionid]))
								$no_signup_ids[$subscriptionid]++;
							else
								$no_signup_ids[$subscriptionid] = 1;
							$extra_msg = "No signup found";
						}
					}
				}
				if($restaurantid!="" || $subscriptionid!="")
				{
					$row_output = "<font color='green'>Restaurant Id: <b>" . $restaurantid . "</b></font> " . $row_output;
					mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `restaurantid`='[1]', `subscriptionid`='[2]' where `id`='[3]'",$restaurantid,$subscriptionid,$responseid);
				}
				if($row_output!="")
				{
					echo "<br>";
					if($extra_msg!="") echo $extra_msg . " - ";
					echo $row_output;
				}
			}
			
			$matched = 0;
			echo "<br>no signup count: " . $no_signup_count;
			echo "<br>unique signup count: " . count($no_signup_ids) . "<br>";
			$no_signup_output = "";
			foreach($no_signup_ids as $key => $val)
			{
				$spay_info = $no_signup_info[$key];
				$card_type = $spay_info['x_card_type'];
				$last_four = $spay_info['x_account_number'];
				
				$no_signup_output .= "<br>Signup $key - <b>$val</b>";
				$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_history` where `card_type`='[1]' and `last_four`='[2]' and `restaurantid`!=''",$card_type,$last_four);
				if(mysqli_num_rows($rest_query))
				{
					$rest_read = mysqli_fetch_assoc($rest_query);
					
					$no_signup_output .= " (FOUND: " . $rest_read['restaurantid'].")";
					mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `match`='matched by cc', `restaurantid`='[1]' where `subscriptionid`='[2]' and `restaurantid`=''",$rest_read['restaurantid'],$key);
					$no_signup_output .= " (matched in db)";
					$matched++;
				}
				else $no_signup_output .= " ... cant find restaurant";
			}
			echo "<br>can match: " . $matched;
			echo "<br>cannot match: " . (count($no_signup_ids) - $matched);
			echo "<br>" . $no_signup_output;
		}
	}
	
	if(1==2)
	{

		function parse_payment_response($response)
		{
			$vars = array();
			$response = explode("\n",$response);
			for($i=0; $i<count($response); $i++)
			{
				$line = explode("=",$response[$i]);
				if(count($line) > 1)
				{
					$vars[trim($line[0])] = trim($line[1]);
				}
			}
			return $vars;
		}
		
		$sum_capture = 0;
		$sum_refund = 0;
		$sum_declined = 0;
		$daily_totals = array();
		
		$mindate = "3000-01-01";
		$maxdate = "1000-01-01";
		
		function add_daily_total($daily_totals,$day,$amount)
		{
			if(isset($daily_totals[$day]))
				$daily_totals[$day] += ($amount * 1);
			else
				$daily_totals[$day] = 0 + ($amount * 1);
			return $daily_totals;
		}
		
		echo "<style>";
		echo ".style_capture { ";
		echo "  color: #ffffff; ";
		echo "} ";
		echo ".style_general { ";
		echo "  color: #000000; ";
		echo "} ";
		echo "</style>";
		echo "<table cellpadding=3>";
		$pr_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` order by `date` desc, `time` desc limit 100");
		while($pr_read = mysqli_fetch_assoc($pr_query))
		{
			if($mindate > $pr_read['date']) $mindate = $pr_read['date'];
			if($maxdate < $pr_read['date']) $maxdate = $pr_read['date'];
			
			$dt = $pr_read['date'];
			if(isset($daily_totals[$dt]))
			{
				$daily_totals[$dt] += $day_total;
			}
			
			//echo "$mindate > " . $pr_read['date'] . "<br>";
			
			$pay_info = parse_payment_response($pr_read['response']);
			echo "<table cellspacing=0 cellpadding=2>";
			foreach($pay_info as $key => $val)
			{
				echo "<tr><td align='right' style='border:solid 1px black'>$key</td><td>" . str_replace("<","&lt;",$val) . "</td></tr>";
			}
			echo "</table>";
			echo "<hr>";
			
			if($pay_info['x_type']=="auth_capture") 
			{
				if($pay_info['x_response_code']=="1")
				{
					$sum_capture += ($pay_info['x_amount'] * 1);
					$daily_totals = add_daily_total($daily_totals,$dt,($pay_info['x_amount'] * 1));
					$bgcolor = "#338833";
					
					if(isset($_GET['t4']))
					{
						if(isset($pay_info['x_subscription_id']) && $pay_info['x_subscription_id']!="")
						{
							$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `arb_license_id`='[1]'",$pay_info['x_subscription_id']);
							if(mysqli_num_rows($signup_query))
							{
								$signup_read = mysqli_fetch_assoc($signup_query);
								//echo "<tr><td colspan='4' bgcolor='#dddddd'>signup: " . $signup_read['company'] . "</td></tr>";
								$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$signup_read['dataname']);
								if(mysqli_num_rows($rest_query))
								{
									$rest_read = mysqli_fetch_assoc($rest_query);
									//echo "<tr><td colspan='4' bgcolor='#dddddd'>restaurant: " . $rest_read['company_name'] . "</td></tr>";
									$rest_id = $rest_read['id'];
									if($rest_read['license_status']=="")
									{
										$refund_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `response` LIKE '%[1]%' and `response` LIKE '%x_type = credit%'",$pay_info['x_account_number']);
										if(mysqli_num_rows($refund_query))
										{
											//echo "payment found with refund - " . $pay_info['x_first_name'] . "<br>";
										}
										else
										{
											mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `license_status`='Paid' where `id`='[1]'",$rest_id);
											echo "payment updated - " . $rest_read['company_name'] . "<br>";
										}
									}
									if($rest_read['package_status']=="")
									{
										$set_status = "";
										if($pay_info['x_amount']=="895.00")
											$set_status = "Silver";
										else if($pay_info['x_amount']=="1495.00")
											$set_status = "Gold";
										else if($pay_info['x_amount']=="3495.00")
											$set_status = "Platinum";
										mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `package_status`='[1]' where `id`='[2]'",$set_status,$rest_id);
										echo "status updated - " . $rest_read['company_name'] . " - " . $set_status . "<br>";
									}
								}
							}
						}
					}
				}
				else
				{
					$sum_decline += ($pay_info['x_amount'] * 1);
					$bgcolor = "#883333";
				}
				$class = "style_capture";
			}
			else if($pay_info['x_type']=="credit")
			{
				$sum_refund += ($pay_info['x_amount'] * 1);
				$daily_totals = add_daily_total($daily_totals,$dt,($pay_info['x_amount'] * -1));
				$bgcolor = "#333388";
				$class = "style_capture";
			} 
			else 
			{
				$bgcolor = "#ffffff";
				$class = "style_general";
			}
			/*echo "<tr bgcolor='$bgcolor'>";
			echo "<td class='$class'>" . $pr_read['date'] . "</td>";
			echo "<td class='$class'>" . $pr_read['time'] . "</td>";
			echo "<td class='$class'>" . $pay_info['x_company'] . "</td>";
			echo "<td class='$class'>" . trim($pay_info['x_first_name'] . " " . $pay_info['x_last_name']) . "</td>";
			echo "<td class='$class'>";
			if($pay_info['x_type']=="auth_capture" && $pay_info['x_response_code']=="1")
				echo "<b>" . $pay_info['x_amount'] . "</b>";
			else
				echo $pay_info['x_amount'];
			echo "</td>";
			//echo "<td>" . $pay_info['x_response_code'] . "</td>";
			echo "<td class='$class'>" . $pay_info['x_response_reason_text'] . "</td>";
			echo "<td class='$class'>" . $pay_info['x_type'] . "</td>";
			echo "<td class='$class'><a style='cursor:pointer' onclick='if(document.getElementById(\"details_".$pr_read['id']."\").style.display == \"table-row\") document.getElementById(\"details_".$pr_read['id']."\").style.display = \"none\"; else document.getElementById(\"details_".$pr_read['id']."\").style.display = \"table-row\"'>(details)</a></td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td colspan='20' id='details_".$pr_read['id']."' style='display:none'>";
			echo trim(str_replace("x_","<br>x_",$pr_read['response']),"<br>");
			echo "</td>";
			echo "</tr>";*/
		}
		echo "</table>";
	}
?>
