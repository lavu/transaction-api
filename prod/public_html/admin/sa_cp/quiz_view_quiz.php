
<form name="which_quiz_form" id="which_quiz_form" method="get" action="index.php">


<div class="leftForm"><label>Select quiz To View</label></div>
<div class="rightForm">
	<select onchange="selectquiz();" name="which_quiz" id="which_quiz">
			<option value=""></option>
			<?php
				$quiz_ids_res = mlavu_query("SELECT quiz_id,question FROM `poslavu__db`.quiz WHERE setting='quiz_title' ORDER BY quiz_id DESC");
				while($quiz_id_row = mysqli_fetch_assoc($quiz_ids_res)){
					echo "<option value='".$quiz_id_row['quiz_id']."'>".$quiz_id_row['question']."</option>";
				}//while
			?>
			
			
	</select>
</div>

<div class="clearForm"></div>

<div class="leftForm"></div>
<div class="rightForm">
 
 </div>
 <div class="clearForm"></div>
 <input type="hidden" name="mode" id="mode" value="view_quiz"/>
</form>

<script type="text/javascript">
function selectquiz(){
		document.getElementById("which_quiz_form").submit();
}//selectquiz()

function thankYou(){
		//alert("Your feedback is greatly appreciated. Thank you!");
		//return false;
		return true;//only use this for the customer actually taking the survey
}//thankYou()

</script>

<?php

	/* functions here */
	function clean($input){
			
			$string = addslashes($input);
			
			return $string;
			
	}//clean

	/* Form submission stuff goes here */
	//$sub is short for submission
	$quiz_id = $_GET['which_quiz'];
	if (isset($quiz_id)){
			$quiz_link = "http://admin.poslavu.com/public/quiz/index.php?quiz_id=".$quiz_id;
			echo "<p>This quiz is available for public use at <a target='_blank' href='$quiz_link'>$quiz_link</a></p>";
	}//if
	//$should_submit = reqvar("submit_ok");
	if (isset($_POST)){
			
			/* Record the proper answers */
			$question_ids = $_POST['question_ids'];
			$question_id_arr = explode(",",$question_ids);
			for ($i=0;$i<count($question_id_arr)*1-1;$i++){
					/* The answers selected on this form represent the correct answers for the quiz. They must be a multiple choice option if they are to be auto graded */
					$cur_id = $question_id_arr[$i];
					$cur_question_id = "question_".$cur_id;
					$cur_question_val = $_POST[$cur_question_id];
					//echo "correct_answer: $cur_question_val<br>";
					/* Does the entry for the correct answer exist in the database?*/
					$sql_query = "SELECT * FROM poslavu__db.quiz WHERE quiz_id='$quiz_id' AND setting='quiz_answer' AND question_id='$cur_id'";
					$res = mlavu_query($sql_query);
					if (mysqli_num_rows($res)*1 < 1){
							$sql_query = "INSERT INTO poslavu__db.quiz (setting,quiz_id,question_id) VALUES ('quiz_answer','$quiz_id','$cur_id') ";
							mlavu_query($sql_query);
					}//if
					$sql_query = "UPDATE poslavu__db.quiz SET question='$cur_question_val' WHERE setting='quiz_answer' AND quiz_id='$quiz_id' AND question_id='$cur_id' ";
					mlavu_query($sql_query);
					
			}//for
			
			//echo "<br>";
			$sub_question_ids = $_POST['question_ids'];
			$sub_question_ids_arr = explode(",",$sub_question_ids);
			$sub_answers = "";
			
			for ($t=0;$t<count($sub_question_ids_arr)*1-1;$t++){
					/* These are actually answers, not questions */
					$sub_question_name = "question_".$sub_question_ids_arr[$t];
					$sub_question_val = clean($_POST[$sub_question_name]);
					$sub_answers .= $sub_question_val."||";
					//echo "quest: $sub_question_val<br/>";
			}
			
			/* Remember when setting="quiz_answer", then the questions field actually holds answers delimited by || */
			//$data_name = "bob";//admin_info("dataname");
			
			//$resInsert = mlavu_query("INSERT INTO `poslavu__db`.`quiz` (setting,location_id,quiz_id,question,data_name) VALUES ('quiz_answer','$locationid','$quiz_id','$sub_answers','$data_name')");
			
			unset($_POST);
	}//if
	
?>

<style type="text/css">
.leftForm{
	float:left;
	width:250px;

	text-align:right;
	margin-right:10px;
	margin-left:50px;
}
.rightForm{
	float:left;
	width:200px;
	
	text-align:left;
}
.clearForm{
	clear:both;
}
</style>
<?php
/* php functions */

function drawInput($type,$id,$options){
		/* Put the right type of HTML input in the form */
		if ($type == "text"){
				$input_str = "<textarea name='question_".$id."' id='question_".$id."' rows='10' cols='40'></textarea>";
				return $input_str;
		}//if
		
		if ($type == "dropdown"){
				$input_str = "<select name='question_".$id."' id='question_".$id."'>";
				$input_str .="<option value=''></option>";
				
				$options_arr = explode(",",$options);
				for ($x=0;$x<count($options_arr);$x++){
						$input_str .= "<option value='".$options_arr[$x]."'>".$options_arr[$x]."</option>";
				}//fo
				$input_str .= "</select>";
				return $input_str;
		}//if
}//drawInput()

?>


<form name="select_correct_answer_form" method="post" action="" >
<?php

//$quiz_id = "s_1";// $quiz_id = "s_2"; //SET THE quiz_ID!!!!

/* Get the instructions */

$resTitle = mlavu_query("SELECT * FROM `poslavu__db`.`quiz` WHERE setting='quiz_title' AND question!='' AND quiz_id='$quiz_id'");
$titleRow = mysqli_fetch_assoc($resTitle);
$the_title = $titleRow['question'];//where setting='quiz_instructions' question field is the instructions
echo "<h2>".$the_title."</h2>";

$resInstruct = mlavu_query("SELECT * FROM `poslavu__db`.`quiz` WHERE setting='quiz_instructions' AND question!='' AND quiz_id='$quiz_id'");
$instructRow = mysqli_fetch_assoc($resInstruct);
$the_instructions = $instructRow['question'];//where setting='quiz_instructions' question field is the instructions
	echo "<div style='width:600px;'>".$the_instructions."</div>";


$res0 = mlavu_query("SELECT * FROM `poslavu__db`.`quiz` WHERE setting='quiz_question' AND quiz_id='$quiz_id'");
$question_ids = "";//so we don't have to query the database again for efficiency's sake
echo "
			<div class='leftForm' style='font-weight:700;'>Question</div>
			<div class='rightForm' style='font-weight:700;'>Correct Answer</div>
			<div class='clearForm'></div>
";
while ($row0 = mysqli_fetch_assoc($res0)){
		$question = html_entity_decode($row0['question']);
		$type = $row0['type'];
		$id = $row0['id'];
		$question_ids .= $id.",";
		$options = $row0['options'];
		echo "
			<div class='leftForm'>$question</div>
			<div class='rightForm'>".drawInput($type,$id,$options)."</div>
			<div class='clearForm'></div>
		";
		
}//while


?>
<div class="leftForm">
	<input type="hidden" name="quiz_id" value="<?php echo $quiz_id;?>"/>
	<input type="hidden" name="question_ids" value="<?php echo $question_ids;?>"/>
</div>
<div class="rightForm">
<input  type="submit" value="Submit" />
</div>
<div class="clearForm"></div>
</form>

<?php

	$sql_query = "SELECT question,question_id FROM `poslavu__db`.`quiz` WHERE setting='quiz_answer' AND quiz_id='$quiz_id' ";
	$answer_res = mlavu_query($sql_query);
	//$answer_arr = array();
	echo "<script type='text/javascript'>";
	while ($row = mysqli_fetch_assoc($answer_res)){
			$answer = $row['question'];//yes the answers are stored in the question column :( 
			$field_id = "question_".$row['question_id'];
			echo "document.getElementById('$field_id').value = '$answer';";
			//$answer_arr[$question_id] = $answer;
			
	}//while
	echo "</script>";

	/* This form likes to make unnecessary submissions to database. Gotta clean the garbage */
	mlavu_query("DELETE FROM `poslavu__db`.`quiz` WHERE question=''");
?>