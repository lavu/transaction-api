<?php

	session_start();

	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;

	if (!function_exists("can_access")) {
		require_once(dirname(__FILE__).'/../manage/can_access.php');
	}
	
	$submode = (isset($_REQUEST['submode']))?$_REQUEST['submode']:false;

	if (can_access("customers")) {

		ini_set("display_errors","1");

		echo"<style>
			.bar_lllg_mid {  background-image:url(../images/bar_lllg_mid.png); height:15px; }
			.bar_dllg_mid {  background-image:url(../images/bar_dllg_mid.png); height:15px; }
			.bar_db_left { background-image:url(../images/bar_db_left.png); width:8px; height:15px; }
			.bar_db_mid {  background-image:url(../images/bar_db_mid.png); height:15px; }
			.bar_db_right {  background-image:url(../images/bar_db_rightt.png); width:8px; height:15px; }
			.bar_lb_left { background-image:url(../images/bar_lb_left.png); width:8px; height:15px; }
			.bar_lb_mid {  background-image:url(../images/bar_lb_mid.png); height:15px; }
			.bar_lb_right {  background-image:url(../images/bar_lb_rightt.png); width:8px; height:15px; }
			.bar_dg_left { background-image:url(../images/bar_dg_left.png); width:8px; height:15px; }
			.bar_dg_mid {  background-image:url(../images/bar_dg_mid.png); height:15px; }
			.bar_dg_right {  background-image:url(../images/bar_dg_rightt.png); width:8px; height:15px; }
			.bar_lg_left { background-image:url(../images/bar_lg_left.png); width:8px; height:15px; }
			.bar_lg_mid {  background-image:url(../images/bar_lg_mid.png); height:15px; }
			.bar_lg_right {  background-image:url(../images/bar_lg_rightt.png); width:8px; height:15px; }
			.bar_o_left { background-image:url(../images/bar_o_left.png); width:8px; height:15px; }
			.bar_o_mid {  background-image:url(../images/bar_o_mid.png); height:15px; }
			.bar_o_right {  background-image:url(../images/bar_o_rightt.png); width:8px; height:15px; }
			.bar_y_left { background-image:url(../images/bar_y_left.png); width:8px; height:15px; }
			.bar_y_mid {  background-image:url(../images/bar_y_mid.png); height:15px; }
			.bar_y_right {  background-image:url(../images/bar_y_rightt.png); width:8px; height:15px; }
			.count_label { color:#111111; font-size:11px; vertical-align:middle; text-align:left; padding:0px 0px 1px 10px; }
			.count_label2 { color:#111111; font-size:11px; vertical-align:middle; text-align:right; padding:0px 10px 1px 0px; }
			.retention { color:#111111; font-size:18px; vertical-align:middle; text-align:center; padding:8px 8px 8px 8px; }
			.year_link { color:#0000CC; font-size:18px; }
			.clist_link { color:#000066; font-size:10px; cursor:pointer; }
			a.clist_link:visted { color:#000066; }
			a.clist_link:hover { color:#0066CC; }
			.condensed_info_table { color:#111111; font-family:Verdana,Arial; font-size:12px; }
			.cit_header { text-align:center; padding: 2px 5px 2px 5px; border-bottom:1px solid #000066; }
			.cit_cell { text-align:left; padding: 2px 5px 1px; } 
			.cit_cell_cntr { text-align:center; padding: 2px 5px 1px; } 
			div.line { transform-origin: 0 100%; height: 3px; background: #000000;
		}
		</style>";

		$display = "";

		if (isset($_REQUEST['cids'])) {
					
			require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
			$maindb = "poslavu_MAIN_db";
			
			$get_company_info = mlavu_query("SELECT `id`, `company_name`, `data_name`, `created`, `last_activity`, `lavu_lite_server` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` IN ([1]) ORDER BY `last_activity` DESC", $_REQUEST['cids']);
			if (mysqli_num_rows($get_company_info) > 0) {
				echo "<table class='condensed_info_table' cellspacing='0' cellpadding='1'>";
				
				$header = " align='center' style='border-bottom:1px solid #000066'";
				echo "<tr><td class='cit_header'><b>ID</b></td><td class='cit_header'><b>Company</b></td><td class='cit_header'><b>Dataname</b></td><td class='cit_header'><b>Created</b></td><td class='cit_header'><b>Last Activity</b></td><td class='cit_header'><b>Last Order</b></td><td class='cit_header'><b>Order Count</b></td><td class='cit_header'><b>Tx Count</b></td></tr>";
				while ($info = mysqli_fetch_assoc($get_company_info)) {
				
					$last_order_datetime = "n/a";
					$order_count = 0;
					$transaction_count = 0;
				
					$is_lavu_lite = !empty($info['lavu_lite_server']);
					
					if ($is_lavu_lite) {
					
						require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
							
						$lavu_code = lc_encode("account_snapshot:".$info['data_name'].":".date("Ymd"));
		
						$vars = "lc=$lavu_code&data_name=".$info['data_name'];
	
						// https doesn't work for some reason: 
						//
						// curl_error: SSL certificate problem, verify that the CA cert is OK. Details:\nerror:14090086:SSL routines:SSL3_GET_SERVER_CERTIFICATE:certificate verify failed					
								
						$url = 'http://admin.lavulite.com/sa_cp/account_activity_snapshot.php'; 
						
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($ch);					
						curl_close ($ch);
						
						$resp_parts = explode("|", $response);
						if ($resp_parts[0] == "1") {
							$last_order_datetime = $resp_parts[1];
							$order_count = $resp_parts[2];
							$transaction_count = $resp_parts[3];
						}
					
					} else {
					
						$use_db = "poslavu_".$info['data_name']."_db";
						
						$get_orders = mlavu_query("SELECT `closed` FROM `$use_db`.`orders` ORDER BY `closed` DESC");
						$order_count = mysqli_num_rows($get_orders);
						if ($order_count > 0) {
							$o_info = mysqli_fetch_assoc($get_orders);
							$last_order_datetime = $o_info['closed'];
						}
						
						$get_transaction_count = mlavu_query("SELECT COUNT(`id`) FROM `$use_db`.`cc_transactions` WHERE `action` != ''");
						$transaction_count = mysqli_result($get_transaction_count, 0);
					}
				
					$onclick = "window.open(\"http://admin.poslavu.com/sa_cp/index.php?mode=manage_customers&submode=details&rowid=".$info['id']."\");";
					if ($is_lavu_lite) $onclick = "window.open(\"http://admin.poslavu.com/manage/index.php?goToAccount=".$info['data_name']."\");";
					echo "<tr onmouseover='this.bgColor=\"#CCDDFF\"' onmouseout='this.bgColor=\"#FFFFFF\"' style='cursor:pointer' onclick='".$onclick."'>";
					echo "<td class='cit_cell'>".$info['id']."</td>";
					echo "<td class='cit_cell'>".$info['company_name']."</td>";
					echo "<td class='cit_cell'>".$info['data_name']."</td>";
					echo "<td class='cit_cell_cntr'>".$info['created']."</td>";
					echo "<td class='cit_cell_cntr'>".$info['last_activity']."</td>";
					echo "<td class='cit_cell_cntr'>".$last_order_datetime."</td>";
					echo "<td class='cit_cell_cntr'>".$order_count."</td>";
					echo "<td class='cit_cell_cntr'>".$transaction_count."</td>";
					echo "</tr>";
				}
				echo "</table>";
			}
			
		} else if ($submode == "history") {
		
			$display = "";
			$year = (isset($_REQUEST['year']))?$_REQUEST['year']:false;
		
			$to_breakdown = "Yearly";
			$title = "Overall Counts and Retention History";
			$stat_type = "recent_n_retention_history_all_years";
			if ($year) {
				$to_breakdown = "Weekly";
				$title = "Counts and Retention History for ".$year;
				$stat_type = "recent_n_retention_history_".$year;
			}
			
			function setMinMax($value, $key, &$mins, &$maxs) {
			
				if (isset($mins[$key])) $mins[$key] = min($value, $mins[$key]); else $mins[$key] = $value;
				if (isset($maxs[$key])) $maxs[$key] = max($value, $maxs[$key]); else $maxs[$key] = $value;
			}
			
			echo "<br><a href='index.php?mode=account_counts&year=".$year."'>Back to ".$to_breakdown." Breakdown</a><br>";
			
			$get_stats = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`stats` WHERE `type` = '[1]' AND `ts` >= '1377183888' ORDER BY `id` ASC", $stat_type);
			if (mysqli_num_rows($get_stats) > 0) {
			
				$all_recent = array();
				$all_retention = array();
				$pl_recent = array();
				$pl_retention = array();
				$ll_recent = array();
				$ll_retention = array();
				
				$mins = array();
				$maxs = array();
				
				$precent_key = "POS Lavu Recent Counts";
				$pretention_key = "POS Lavu Retention Ratio";
				$lrecent_key = "Lavu Lite Recent Counts";
				$lretention_key = "Lavu Lite Retention Ratio";
				$arecent_key = "Total Recent Counts";
				$aretention_key = "Average Retention Ratio";
				
				while ($info = mysqli_fetch_assoc($get_stats)) {
					$ts = $info['ts'];
					setMinMax($ts, "ts", $mins, $maxs);
					$all_recent[$ts] = $info['value'];
					setMinMax($info['value'], $arecent_key, $mins, $maxs);
					$all_retention[$ts] = $info['value2'];
					setMinMax($info['value2'], $aretention_key, $mins, $maxs);
					$pl_recent[$ts] = $info['value3'];
					setMinMax($info['value3'], $precent_key, $mins, $maxs);
					$pl_retention[$ts] = $info['value4'];
					setMinMax($info['value4'], $pretention_key, $mins, $maxs);
					$ll_recent[$ts] = $info['value5'];
					setMinMax($info['value5'], $lrecent_key, $mins, $maxs);
					$ll_retention[$ts] = $info['value6'];
					setMinMax($info['value6'], $lretention_key, $mins, $maxs);
				}
								
				$charts = array();
				$charts[$precent_key] = $pl_recent;
				$charts[$pretention_key] = $pl_retention;
				$charts[$lrecent_key] = $ll_recent;
				$charts[$lretention_key] = $ll_retention;
				$charts[$arecent_key] = $all_recent;
				$charts[$aretention_key] = $all_retention;
				
				?>
				
				<script language='javascript'>
				
					function drawLine(chart, x1, y1, x2, y2) {
					
						var c = document.getElementById(chart);
						var ctx = c.getContext("2d");
						ctx.strokeStyle="blue";
						ctx.moveTo(x1,y1);
						ctx.lineTo(x2,y2);
						ctx.lineWidth = 1;
						ctx.stroke();
					}
					
				</script>
				
				<?php
				
				$display .= "<table>";
				$keys = array_keys($charts);
				$coords = array();
				foreach ($keys as $key) {
				
					$display .= "<tr><td>&nbsp;</td></tr>";
					$display .= "<tr><td align='center'>".$key."</td></tr>";
					
					$is_ratio = (in_array($key, array($aretention_key, $pretention_key, $lretention_key)));
					
					$pmult = ($is_ratio)?100:1;
					
					$x_tics = 10;
					$y_tics = 10;

					$x_range = ($maxs['ts'] - $mins['ts']);
					$x_step = ($x_range / $x_tics);
					
					//error_log($key);
					error_log("mins: ".print_r($mins, true));
					//error_log(" - ");
					error_log("maxs: ".print_r($maxs, true));
					//error_log(" - ");
					
					$y_range = (($maxs[$key] - $mins[$key]) * $pmult);
					$y_step = 10;
					if (!$is_ratio) {
						/*if ($y_range < 20) $y_step = 1;
						else if ($y_range < 50) $y_step = 5;
						else if ($y_range < 100) $y_step = 10;
						else if ($y_range < 500) $y_step = 50;
						else if ($y_range < 1000) $y_step = 100;
						else if ($y_range < 5000) $y_step = 500;
						else if ($y_range < 10000) $y_step = 1000;*/
						$y_step = ceil($y_range / 10);
						if ($y_step < 1) $y_step = 1;
						else if ($y_step < 5) $y_step = 5;
						else if ($y_step < 10) $y_step = 10;
						else if ($y_step < 20) $y_step = 20;
						else if ($y_step < 25) $y_step = 25;
						else if ($y_step < 40) $y_step = 40;
						else if ($y_step < 50) $y_step = 50;
						else if ($y_step < 100) $y_step = 100;
						else if ($y_step < 200) $y_step = 200;
						else if ($y_step < 250) $y_step = 250;
						else if ($y_step < 500) $y_step = 500;
						else if ($y_step < 1000) $y_step = 1000;
						else if ($y_step < 2000) $y_step = 2000;
						else if ($y_step < 2500) $y_step = 2500;
						else if ($y_step < 5000) $y_step = 5000;
						else if ($y_step < 10000) $y_step = 10000;
						else if ($y_step < 20000) $y_step = 20000;
						else if ($y_step < 25000) $y_step = 25000;
						else if ($y_step < 50000) $y_step = 50000;
					}
					
					error_log("y_range: ".$y_range);
					error_log("y_step: ".$y_step);
					
					$base_y = ($is_ratio)?0:(floor($mins[$key] / $y_step) * $y_step);
					//$y_range = ($maxs[$key] - $base_y);
					$y_range = ($y_step * 10);

					error_log("base_y: ".$base_y);
					error_log("y_range: ".$y_range);
										
					//error_log($key." x_range: ".$x_range." x y_range: ".$y_range." (base_y: ".$base_y." ... y_step: ".$y_step.")");
					
					$chartW = 1000;
					$chartH = 300;
								
					$display .= "<tr><td align='center'>";
					$display .= "<table cellspacing='0' cellpadding='0'>";
					for ($i = 0; $i < ($y_tics + 1); $i++) {
						$adj = ($i == 0)?10:0;
						$display .= "<tr><td style='border-right:2px solid #333333;' height='".(($chartH / $y_tics) + $adj)."px' align='right' valign='bottom'>".round((($y_tics - $i) * $y_step) + $base_y).($is_ratio?"%":"")." _</td>";
						if ($i == 0) {
							$display .= "<td rowspan='".($y_tics + 1)."' width='".(($chartW / ($x_tics - 1)) / 2)."px'>&nbsp;</td>";
							$display .= "<td rowspan='".($y_tics + 1)."' width='".$chartW."px' valign='bottom'>";
							$display .= "<canvas id='".str_replace(" ", "_", $key)."_chart' width='".$chartW."px' height='".$chartH."px'></canvas>";
							$display .= "</td>";
							$display .= "<td rowspan='".($y_tics + 1)."' width='".(($chartW / ($x_tics - 1)) / 2)."px'>&nbsp;</td>";
						}
						$display .= "</tr>";
					}
					$display .= "<tr><td style='border-right:2px solid #333333;' height='10px'></td><td colspan='3'style='border-bottom:2px solid #333333;'></td></tr>";
					$display .= "<tr><td>&nbsp;</td>";
					$display .= "<td colspan='3'><table cellspacing='0' cellpadding='0'><tr>";
					for ($i = 0; $i < ($x_tics + 1); $i++) {
						$display .= "<td align='center' valign='top' width='".($chartW / $x_tics)."px'>|<br>".date("M j, Y", floor($mins['ts'] + ($x_step * $i)))."</td>";
					}
					$display .= "</tr></table></td>";
					$display .= "</tr>";
					$display .= "</table>";
					$dipslay .= "</td></tr>";
				
					$display .= "<tr><td align='center'>";
					/*$display .= "<table cellspacing='0' cellpadding='6' border='1'>";
					$tss = array_keys($charts[$key]);
					foreach ($tss as $ts) {
						$display_value = $charts[$key][$ts];
						if (strstr($key, "Ratio")) $display_value = number_format(($display_value * 100), 2)."%";
						$display .= "<tr><td align='center'>".date("Y-m-d", $ts)."</td><td align='center'>".$display_value."</td></tr>";
					}
					$display .= "</table>";
					$display .= "</td></tr>";*/
					$display .= "<tr><td>&nbsp;</td></tr>";
					
					$ts_keys = array_keys($charts[$key]);
					$coord_step = (count($ts_keys) > 50)?floor(count($ts_keys) / 50):1;
					$coords[$key] = array();
					
					//error_log("count: ".count($ts_keys)." ... coord_step: ".$coord_step);
										
					$last_xy = "";
					for ($i = 0; $i < count($ts_keys); $i += $coord_step) {
						$ts = $ts_keys[$i];
						$x = round((($ts - $mins['ts']) / $x_range) * $chartW);
						$val = $charts[$key][$ts];
						if ($is_ratio) $y = ($chartH - round($val * $chartH));
						else $y = ($val==0 || $y_range==0)?$chartH:($chartH - round((($val - $base_y) / $y_range) * $chartH));
						if (is_array($last_xy)) $coords[$key][] = array($x, $y, $last_xy[0], $last_xy[1]);					
						$last_xy = array($x, $y);
					}
				}
				$display .= "<tr><td>&nbsp;</td></tr></table>";
				
				$display .= "<script language='javascript'>";			
				foreach ($coords as $key => $val) {
					foreach ($val as $xsnys) {
						$display .= "drawLine('".str_replace(" ", "_", $key)."_chart', ".$xsnys[0].", ".$xsnys[1].", ".$xsnys[2].", ".$xsnys[3].");";
					}
				}
				$display .= "</script>";
				
			} else $display = "No historical data found.";			
		
			echo "<table width='100%'>
				<tr>
					<td align='center'>
						<table cellspacing='0' cellapdding='0'>
							<tr><td align='center'><b>".$title."</b></td></tr>
							<tr><td>&nbsp;</td></tr>
							<tr><td align='center'>".$display."</td></tr>
						</table>
					</td>
				</tr>
			</table>";
			
			
		} else {
		
			$year = (isset($_REQUEST['year']))?$_REQUEST['year']:false;
					
			$total_demo = 0;
			$total_resellers = 0;
			$total_paid = 0;
			$total_enabled = 0;
			$total_trial = 0;
			$total_recent = 0;
			$total_created = 0;
			$total_lavu_lite_recent = 0;
			$total_lavu_lite_count = 0;
		
			$recent_check = date("Y-m-d H:i:s", (time() - 432000));
			$max_count = 0;
			
			if ($year) {
			
				$title = $year." Weekly Count Breakdown";
				$leap_year = 0;
				//if (($year % 4) == 0) { $leap_year = 86401; }
				$week = 0;
				$last_week = 0;
				$low_n = 1;
				$high_n = 365;
				$inc_n = 7;
				$yd = 0;
			
			} else {
		
				$low_n = 2010;
				$high_n = date("Y");;
				$inc_n = 1;
				$title = "Yearly Account Count Breakdown ($low_n - $high_n)";
				$yd = 150;
			}
						
			$counts_array = array();
			for ($n = $low_n; $n <= $high_n; $n += $inc_n) {
				
				if ($year) {
					$week++;
					if ($week <= 52) {
						$key = mktime(0, 0, 0, 1, $n, $year);
						$counts_array[$key] = "";
						if ($week == 52) { $last_week = 86400; }
						$start_date = date("Y-m-d H:i:s", $key);
						$end_date = date("Y-m-d H:i:s", ($key + 604799 + $last_week + $leap_year));
					} else {
						break;
					}
				} else {
					$key = $n;
					$counts_array[$key] = "";
					$start_date = $n."-01-01 00:00:00";
					$end_date = date("Y-m-d H:i:s", mktime(0, 0, -1, 1, 1, ($n + 1)));
				}
		
				$demo_count = 0;
				$reseller_count = 0;
				$paid_count = 0;
				$enabled_count = 0;
				$trial_count = 0;
				$recent_count = 0;
				$lavu_lite_recent = 0;
				$lavu_lite_count = 0;
		
				$get_count = mlavu_query("SELECT `poslavu_MAIN_db`.`restaurants`.`id` AS `company_id`,`poslavu_MAIN_db`.`restaurants`.`disabled` AS `disabled`, `poslavu_MAIN_db`.`restaurants`.`demo` AS `demo`, `poslavu_MAIN_db`.`restaurants`.`reseller` AS `reseller`, `poslavu_MAIN_db`.`restaurants`.`license_status` AS `license_status`, `poslavu_MAIN_db`.`restaurants`.`lavu_lite_server` AS `lavu_lite_server`, `poslavu_MAIN_db`.`restaurants`.`last_activity` AS `last_activity`, `poslavu_MAIN_db`.`restaurants`.`signupid` as `signupid`, `poslavu_MAIN_db`.`signups`.`arb_license_start` as `arb_license_start` FROM `poslavu_MAIN_db`.`restaurants` LEFT JOIN `poslavu_MAIN_db`.`signups` ON `poslavu_MAIN_db`.`restaurants`.`signupid` = `poslavu_MAIN_db`.`signups`.`id` WHERE `poslavu_MAIN_db`.`restaurants`.`created` >= '[1]' AND `poslavu_MAIN_db`.`restaurants`.`created` < '[2]' AND `poslavu_MAIN_db`.`restaurants`.`test` != '1'", $start_date, $end_date);
				$full_count = mysqli_num_rows($get_count);
				$total_created += $full_count;
				$active_pl_ids = array();
				$active_ll_ids = array();
				while ($cinfo = mysqli_fetch_assoc($get_count)) {
					if ($cinfo['disabled']!=1 && $cinfo['disabled']!=2 && $cinfo['disabled']!=3) {
						if ($cinfo['demo'] == 1) {
							$demo_count++;
							$total_demo++;
						} else if ($cinfo['reseller'] == 1) {
							$reseller_count++;
							$total_resellers++;
						} else if (substr($cinfo['license_status'], 0, 4) == "Paid") {
							$paid_count++;
							$total_paid++;
							$enabled_count++;
							$total_enabled++;
						} else {
							$enabled_count++;
							$total_enabled++;
						}
						
						if (($cinfo['signupid'] > 0) && ($cinfo['arb_license_start'] >= date("Y-m-d"))) {
							$trial_count++;
							$total_trial++;
						}
					}
					
					if ($cinfo['last_activity'] >= $recent_check) {
						$recent_count++;
						$total_recent++;
						$active_pl_ids[] = $cinfo['company_id'];
					}
					
					if ($cinfo['lavu_lite_server'] != "") {
						$lavu_lite_count++;
						$total_lavu_lite_count++;
						if ($cinfo['last_activity'] >= $recent_check) {
							$lavu_lite_recent++;
							$total_lavu_lite_recent++;
							$active_ll_ids[] = $cinfo['company_id'];
						}
					}
				}
				
				if ($full_count > $max_count) $max_count = $full_count;
							
				$counts_array[$key] = array($week, $demo_count, $reseller_count, $paid_count, $enabled_count, $trial_count, $recent_count, $full_count, $lavu_lite_recent, $lavu_lite_count, implode(",", $active_pl_ids), implode(",", $active_ll_ids));
			}
			
			$ratio = ((665 + $yd) / $max_count);
			$max_total = max(array($total_demo, $total_resellers, $total_paid, $total_enabled, $total_recent, $total_created, $total_lavu_lite_recent, $total_lavu_lite_count));
			$totals_ratio = (765 / $max_total);
			
			$keys = array_keys($counts_array);
			foreach ($keys as $key) {
				$week = $counts_array[$key][0];
				$demo_count = $counts_array[$key][1];
				$reseller_count = $counts_array[$key][2];
				$paid_count = $counts_array[$key][3];
				$enabled_count = $counts_array[$key][4];
				$trial_count = $counts_array[$key][5];
				$recent_count = $counts_array[$key][6];
				$full_count = $counts_array[$key][7];
				$ll_recent = $counts_array[$key][8];
				$ll_count = $counts_array[$key][9];
				$display_retention = "";
				if ($full_count > 0) {
					$retention_ratio = 0;
					if ($full_count != 0) $retention_ratio = ($recent_count / $full_count);
					$pl_retention_ratio = 0;
					if (($full_count - $ll_count) != 0) $pl_retention_ratio = (($recent_count - $ll_recent) / ($full_count - $ll_count));
					$ll_retention_ratio = 0;
					if ($ll_count != 0) $ll_retention_ratio = ($ll_recent / $ll_count);
					$display_retention = number_format(($retention_ratio * 100), 2)."%<br><font size='-2'>PL ".number_format(($pl_retention_ratio * 100), 2)."%</font><br><font size='-2'>LL ".number_format(($ll_retention_ratio * 100), 2)."%</font>";
				}
				if ($year) {
					$last_week = 0;
					if ($week == 52) { $last_week = 86400; }
					$display_date = date("n/j/Y", $key)." to ".date("n/j/Y", ($key + 604799 + $last_week + $leap_year));
					$display .= "<tr>
						<td width='70px' align='center'>Week $week</td>
						<td width='180px' align='right'>$display_date</td>
						<td width='730px' align='left'>";
				} else {
					$display .= "<tr>
						<td width='100px' align='center' colspan='2'> <a class='year_link' href='index.php?mode=account_counts&year=$key'>$key</a> </td>
						<td width='880px' align='left'>";
				}
			
				$data_array = array();
				$data_array[] = array($demo_count, "y");
				$data_array[] = array($reseller_count, "o");
				$data_array[] = array($paid_count, "lg");
				$data_array[] = array($enabled_count, "dg");
				$data_array[] = array($recent_count, "lb");
				$data_array[] = array($full_count, "db");
				$data_array[] = array($ll_recent, "lllg");
				$data_array[] = array($ll_count, "dllg");
			
				$cnt = 0;
				foreach ($data_array as $data) {
					$cnt++;
					$display .= "<table cellspacing='0' cellpadding='0'>";
					$left_bar = "";
					$right_bar = "";
					if ($data[0] > 0) {
						//$left_bar = "<td class='bar_".$data[1]."_left'>&nbsp;</td>";
						//$right_bar = "<td class='bar_".$data[1]."_right'>&nbsp;</td>";
					}
					$extra_code = "";
					if (($data[1] == "dg") && ($trial_count > 0)) $extra_code = " ($trial_count in trial)";
					if (($data[1]=="lb" || $data[1]=="lllg") && $year && $data[0]>0) {
						$cid_list = $counts_array[$key][10];
						if ($data[1] == "lllg") $cid_list = $counts_array[$key][11];
						$extra_code = " &nbsp;&nbsp;<a class='clist_link' onclick='showCompanyListPopup(\"".$cid_list."\")'>List Companies</a>";	
					}
					if ($cnt == 7) $display .= "<tr><td height='7px'></td><tr>";
					$display .= "<tr>$left_bar<td class='bar_".$data[1]."_mid' width='".(int)($data[0] * $ratio)."px'>&nbsp;</td>$right_bar<td class='count_label'><nobr>".$data[0].$extra_code."</nobr></td></tr>
							</table>";
				}
				
				$display .= "</td><td class='retention'>$display_retention</td>
				</tr>";
			}
	
		
			$data_array = array();
			$data_array[] = array($total_demo, "y", "Enabled Demo Accounts");
			$data_array[] = array($total_resellers, "o", "Enabled Resellers");
			$data_array[] = array($total_paid, "lg", "Paid Clients");
			$data_array[] = array($total_enabled, "dg", "Enabled Clients");
			$data_array[] = array($total_recent, "lb", "Active Within 5 Days");
			$data_array[] = array($total_created, "db", "Accounts Created");
			$data_array[] = array($total_lavu_lite_recent, "lllg", "Lavu Lite Active Within 5 Days");
			$data_array[] = array($total_lavu_lite_count, "dllg", "Lavu Lite Created");
			
			$top_display = "";
			
			$retention_ratio = ($data_array[4][0] / $data_array[5][0]);
			$pl_retention_ratio = 0;
			$pl_retention_ratio = (($data_array[4][0] - $data_array[6][0]) / ($data_array[5][0] - $data_array[7][0]));
			$ll_retention_ratio = 0;
			if ($data_array[7][0] != 0) $ll_retention_ratio = ($data_array[6][0] / $data_array[7][0]);
			
			$top_display .= "<table>";
			$top_display .= "<tr><td align='left'>";
			foreach ($data_array as $data) {
				$top_display .= "<table cellspacing='0' cellpadding='0'>";
				$left_bar = "";
				$right_bar = "";
				if ($data[0] > 0) {
					//$left_bar = "<td class='bar_".$data[1]."_left'>&nbsp;</td>";
					//$right_bar = "<td class='bar_".$data[1]."_right'>&nbsp;</td>";
				}
				$extra_code = "";
				if (($data[2] == "Enabled Clients") && ($total_trial > 0)) $extra_code = " ($total_trial in trial)";
				$top_display .= "<tr><td class='count_label2' width='180px'>".$data[2]."</td>$left_bar<td class='bar_".$data[1]."_mid' width='".(int)($data[0] * $totals_ratio)."px'>&nbsp;</td>$right_bar<td class='count_label'>".$data[0]." Total".$extra_code."</td></tr>";
				//if ($data[2] == "Accounts Created") $top_display .= "<tr><td colspan='2' class='retention'><br><b>".number_format(($retention_ratio * 100), 2)."%</b> Overall Retention</td></tr>";
				$top_display .= "</table><br>";
			}
			$top_display .= "</td></tr>";
			$top_display .= "<tr><td class='retention'><b>".number_format(($retention_ratio * 100), 2)."%</b> Overall Retention<br><font size='-1'>POSLavu ".number_format(($pl_retention_ratio * 100), 2)."% &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Lavu Lite ".number_format(($ll_retention_ratio * 100), 2)."%</font></td></tr>";
			$top_display .= "<tr><td align='right' style='padding:0px 60px 0px 0px;'><a href='index.php?mode=account_counts&submode=history&year=".$year."'>History</a></td></tr>";
			$top_display .= "</table><br>";
			
			if ($year) echo "<br><a href='index.php?mode=account_counts'>Back to Yearly Breakdown</a><br>";
			
			echo "<table width='100%'>
				<tr>
					<td align='center'>
						<table cellspacing='0' cellapdding='0'>
							<tr><td align='center'><b>$title</b><br>(Excludes accounts marked as test accounts)</td></tr>
							<tr><td>&nbsp;</td></tr>
							<tr><td align='left'>$top_display</td></tr>
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td>
									<table cellspacing='2' cellpadding='5' width='1000px' bordercolor='#CCCCCC' border='1'><tr><td colspan='3'></td><td align='center'>Retention</td></tr>$display</table>
								</td>
							</tr>
							<tr><td>&nbsp;</td></tr>
						</table>
					</td>
				</tr>
			</table>";
			
			$stat_type = "recent_n_retention_history_".($year?$year:"all_years");
			$check_stats = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`stats` WHERE `type` = '[1]' AND `ts` > '[2]'", $stat_type, (time() - 43200));
			if (mysqli_num_rows($check_stats) < 1) {
				$vars = array();
				$vars['type'] = $stat_type;
				$ts = time();
				$vars['datetime'] = date("Y-m-d H:i:s", $ts);
				$vars['ts'] = $ts;
				$vars['value'] = $total_recent;
				$vars['value2'] = $retention_ratio;
				$vars['value3'] = ($total_recent - $total_lavu_lite_recent);
				$vars['value4'] = $pl_retention_ratio;
				$vars['value5'] = $total_lavu_lite_recent;
				$vars['value6'] = $ll_retention_ratio;
				$record_stats = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`stats` (`type`, `datetime`, `ts`, `value`, `value2`, `value3`, `value4`, `value5`, `value6`) VALUES ('[type]', '[datetime]', '[ts]', '[value]', '[value2]', '[value3]', '[value4]', '[value5]', '[value6]')", $vars);
			}
	
		?>
		
			<script language='javascript'>
			
				function showCompanyListPopup(cids) {
			
					var UserWidth = 0, UserHeight = 0;
					if (typeof(parent.window.innerWidth) == 'number') {
						//Non-IE
						UserWidth = parent.window.innerWidth;
						UserHeight = parent.window.innerHeight;
					} else if (parent.document.documentElement && (parent.document.documentElement.clientWidth || parent.document.documentElement.clientHeight)) {
						//IE 6+ in 'standards compliant mode'	
						UserWidth = parent.document.documentElement.clientWidth;
						UserHeight = parent.document.documentElement.clientHeight;
					} else if (parent.document.body && (parent.document.body.clientWidth || parent.document.body.clientHeight)) {
						//IE 4 compatible
						UserWidth = parent.document.body.clientWidth;
						UserHeight = parent.document.body.clientHeight;
					}
			
					var ScrollTop = document.body.scrollTop;
					if (ScrollTop == 0) {
						if (window.pageYOffset) ScrollTop = window.pageYOffset;
						else ScrollTop = (document.body.parentElement)?document.body.parentElement.scrollTop:0;
					}
			
					document.getElementById("company_list_iframe").src = "account_counts.php?cids=" + cids;	
					document.getElementById("company_list_popup").style.left = ((UserWidth / 2) - 462) + "px";
					document.getElementById("company_list_popup").style.top = (ScrollTop + 75) + "px";
					document.getElementById("company_list_popup").style.display = 'inline';
				}
			
				function hideCompanyList() {
					document.getElementById("company_list_popup").style.display = 'none';
				}
			
			</script>
			
			<div id='company_list_popup' style='position:absolute; left:0px; top:0px; z-index:200; display:none;'>
				<table cellspacing='0' cellpadding='0'>
					<tr>
						<td style='background:URL(images/popup_bg.png); width:924px; height:471px' align='left' valign='top'>
							<table cellspacing='0' cellpadding='0'>
								<tr><td colspan='3' height='22px'>&nbsp;</td></tr>
								<tr>
									<td width='21px'>&nbsp;</td>
									<td height='20px' align='right' style='background:URL(images/popup_bar.png);'><a onclick='hideCompanyList();' style='cursor:pointer; color:#EEEEEE;'><b>X</b></a> &nbsp; </td>
									<td width='25px'>&nbsp;</td>
								</tr>
								<tr>
									<td width='21px'>&nbsp;</td>
									<td align='left'><div id='iframe_scroller' style='width:879px; height:405px; overflow:hidden; margin:0px 0px 0px 0px; padding:0px 0px 0px 0px;'><iframe id='company_list_iframe' width='875px' height='401px' frameborder='1' scrolling='yes' src='' style='width:875px; height:401px;'></iframe></div></td>
									<td width='25px'>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			
		<?php

		}	
}
	
?>
