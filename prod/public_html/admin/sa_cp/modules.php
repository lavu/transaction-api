<?php
	ini_set("display_errors", "1");
	
	/**
	 * Modules that can be assigned in sa_cp
	 **/
	$available_modules = array(
		"lavutogo",
		"employees.classes",
		"employees.payrates",
		"employees.userschedules",
		"financial.discount.groups",
		"reports.alldiscounts",
		"reports.paymentswithtaxes",
		"reports.batchdetails",
		"dining.mealperiods",
		"dining.revenuecenters",
	);
	
	/**
	 * Modules that everyone has access to (CORE FUNCTIONALITY)
	 **/
	$core_modules = new Module();
	
	/**
	 * Modules that Silver has Access to (CORE TO SILVER)
	 **/
	$silver_modules = new Module();
	
	/**
	 * Modules that Gold has Access to (CORE TO GOLD) 
	 **/
	$gold_modules = new Module("lavutogo");
	
	/**
	 * Modules that Platinum has Access to (CORE TO PLATINUM)
	 **/
	$platinum_modules = new Module("lavutogo");
	
	/**
	 * Development Modules
	 **/
	$dev_modules = new Module("lavutogo");
	
	/**
	 * Moduels that Lavu Local Server has Access to (CORE TO LLS)
	 **/
	$lavu_local_server_modules = new Module("-lavutogo");	
	//Module
	//Example: "test,test1,payments,payments.orders,payments.orders.*"
	//
	//Result:
	// "test"
	// "Test1"
	// "payments" ->
	//		"orders" ->
	//			"*" //special character meaning everything

	/**
	 * The Module Class
	 * 
	 * The sole purpose of a Module class, is to determine access when provided a Module Path.
	 * You can think of a Module Path as being similiar to a Package as Represented in Java.
	 *
	 * Example:
	 * foo.bar.test.blah
	 *
	 * When given a Module Path to test against using Module->hasModule($str), the function will
	 * return true, as in it has the Module, or false, meaning that it does not.
	 *
	 * To build a Module, you merely have to provide it with a comma deliniated string that will
	 * account for all the Modules that this particular Module should encompass.  Each entry should
	 * then be period deliniated to signify the levels of the heirarchy.  Finally, and optional symbol
	 * (+ or -) can be prepended to each entry to either specify that it is enabled, or disabled.
	 *  NOTE: 	'+', '-', '.', ',', and '*' are reserved characters in this path construction, and as such
	 *			are not to be included in any Module Path name.
	 *				+: enable
	 *				-: disable
	 *				,: separates list of Module Paths
	 *				.: separates the heirarchy of Modules 
	 * 				*: specifies that this, and lower in the heirarchy
	 *
	 * Since the Modules are based on a heirarchy, if a parent Module is disabled, then all following
	 * Modules are also disabled.
	 **/
	class Module
	{
		private $moduleName = "";
		private $enabled = true;
		private $modules = array();
		
		function __construct($mods = "", $depth = 0)
		{
			$this->modules = array();
			$this->moduleName = "";
			$this->enabled = true;
			$ms = explode(",", $mods);
			
			if(count($ms) > 1)
			{
				$this->moduleName = "Base";
				foreach($ms as $key => $val)
				{
					$newMod = new Module($val, $depth + 1);
					if(isset($this->modules[$newMod->getModName()]))
					{
						$this->modules[$newMod->getModName()] = Module::mergeModules(array($this->modules[$newMod->getModName()], $newMod));
					}
					else
						$this->modules[$newMod->getModName()] = $newMod;
				}
			}
			else
			{
				if(($depth == 0))
				{
					$this->moduleName = "Base";
					if($mods != "")
					{
						$newMod = new Module($mods, $depth + 1);
						$this->modules[$newMod->getModName()] = $newMod;
					}
				}
				else
				{
					//. Delimited?
					$pos = strpos($mods, ".");
					
					if($pos===FALSE)
					{ //No . in this string
						
						$setenabled = true;
						if(substr($mods,0,1) == "-")
						{
							$setenabled = false;
							$mods = substr($mods,1);
						}
						else if(substr($mods,0,1) == "+")
						{
							$mods = substr($mods,1);
						}
						$this->moduleName = $mods;
						$this->enabled = $setenabled;
					}
					else
					{
						$this->moduleName = substr($mods, 0, $pos);
						$setenabled = true;
						if(substr($this->moduleName, 0, 1) == "-")
						{
							$setenabled = false;
							$this->moduleName = substr($this->moduleName, 1);
						}
						else if(substr($this->moduleName, 0, 1) == "+")
						{
							$this->moduleName = substr($this->moduleName, 1);
						}
						
						$this->enabled = $setenabled;
						
						
						$newMod = new Module(substr($mods, $pos + 1), $depth + 1);
						$this->modules[$newMod->getModName()] = $newMod;
					}
				}
			}
		}
		
		public function addModule($modName)
		{
			if(strpos($modName,",") !== FALSE)
				return;
				
			$newMod = new Module($modName, 1);
			$this->modules[$newMod->getModName()] = $newMod;
		}
		
		/**
		 * Returns whether this Module is enabled, or disabled
		 **/
		public function enabled()
		{
			return $this->enabled;
		}
		
		/**
		 * Returns whether this Module object contains the Module specified by $modname.
		 *
		 * ARGUMENTS:
		 * $modname => string:: Specifies a single Module Name
		 * 
		 * RETURNS:
		 * True if the Module, or wildcard, is found and enabled False otherwise.
		 **/
		public function hasModule($modname)
		{
			if(isset($this->modules["*"]))
			{
				if($this->modules["*"]->enabled)
					return true;
				else
					return false;
			}
				
			$pos = strpos($modname, ".");
			if($pos === FALSE)
			{
				//echo $modname."<br />";
				//echo "<pre style='text-align: left;'>".print_r($this->modules,true)."</pre>";
				
				return (isset($this->modules[$modname]) && $this->modules[$modname]->enabled());
			}
			else
			{
				$module = substr($modname, 0, $pos);
				$next = substr($modname, $pos + 1);
				return isset($this->modules[$module]) && ($this->modules[$module]->enabled()) && ($this->modules[$module]->hasModule($next));
			}
		}
		
		/**
		 * Returns the name of the current Module
		 **/
		public function getModName()
		{
			return $this->moduleName;
		}
		
		/**
		 * Returns a Combination Representing whether or not the Module is enabled, concatenated with it's Module Name.
		 *
		 * RETURNS
		 * + for enabled
		 * - for enabled
		 * Module Name
		 * EXAMPLE
		 * +example
		 * OR
		 * -example
		 **/
		public function getEnabledModName()
		{
			$str = $this->enabled ? "+" : "-";
			$str .= $this->getModName();
			
			return $str;
		}
		
		/**
		 * Takes an Array of Modules, and Merges them into one Module that it Returns.
		 * This function will favor the later Modules in the List, for enabling/disabling
		 * rather than the first one encountered.
		 *
		 * RETURNS
		 * Merged Module
		 * 
		 **/
		public static function mergeModules(array $mods, Module $result = NULL)
		{
			if(!$result)
				$result = $mods[0];//new Module();
				
			foreach($mods as $modules)
			{
				if(count($modules->modules))
					foreach($modules->modules as $key => $module)
					{
						if(!isset($result->modules[$key]))
							$result->modules[$key] = new Module($module->getModName(),1);
						if($result->modules[$key]->enabled != $module->enabled && $module->enabled != "")
							$result->modules[$key]->enabled = $module->enabled;//Overrides Enabled Setting if it exists
						Module::mergeModules(array($module), $result->modules[$key]);
					}	
			}
			
			
			return $result;
		}
	}

	function getActiveModules($moduleStr, $package, $isLLS)
	{
		global $core_modules;
		$mods = array($core_modules);
		if($package == "" || strpos("Silver", $package) !== FALSE || $package == "1")
		{
			global $silver_modules;
			$mods[] = $silver_modules;
		}
		else if(strpos("Gold", $package) !== FALSE || $package == "2")
		{
			global $gold_modules;
			$mods[] = $gold_modules;
		}
		else if(strpos("Platinum", $package) !== FALSE || $package == "3")
		{
			global $platinum_modules;
			$mods[] = $platinum_modules;
		}
		else if(strpos("Dev", $package) !== FALSE || $package == "24")
		{
			global $dev_modules;
			$mods[] = $dev_modules;
		}
		else
		{
		}
		
		if($isLLS)
		{
			global $lavu_local_server_modules;
			$mods[] = $lavu_local_server_modules;
		}
		
		$mods[] = new Module($moduleStr);
		
		//echo "<pre>".print_r($mods,true)."</pre>";
		return Module::mergeModules($mods);
	}
	
?>