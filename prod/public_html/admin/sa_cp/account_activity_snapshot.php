<?php

	//error_log("account_activity_snapshot.php");
	
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

	$lc = explode(":", lc_decode($_POST['lc']));
	if ($lc[0]=="account_snapshot" && $lc[1]==$_POST['data_name'] && $lc[2]==date("Ymd")) {

		//error_log("account_activity_snapshot: ".print_r($_POST, true));

		$use_db = "poslavu_".$_POST['data_name']."_db";
		
		$last_order_datetime = "n/a";
		$order_count = 0;
		$transaction_count = 0;
		
		$get_orders = mlavu_query("SELECT `closed` FROM `$use_db`.`orders` ORDER BY `closed` DESC");
		$order_count = mysqli_num_rows($get_orders);
		if ($order_count > 0) {
			$o_info = mysqli_fetch_assoc($get_orders);
			$last_order_datetime = $o_info['closed'];
		}
		
		$get_transaction_count = mlavu_query("SELECT COUNT(`id`) FROM `$use_db`.`cc_transactions` WHERE `action` != ''");
		$transaction_count = mysqli_result($get_transaction_count, 0);
		
		echo "1|".$last_order_datetime."|".$order_count."|".$transaction_count;
	}
	
?>