<?php
	if($argc > 1)
	{
		$fname = $argv[1];
		echo "\nremoving doublespacing in file $fname ...\n";
		$fp = fopen($fname,"r");
		$str = fread($fp,filesize($fname));
		fclose($fp);
		
		$str = str_replace("\n\n","\n",$str);
		$fp = fopen($fname,"w");
		fwrite($fp,$str);
		fclose($fp);
	}
	else
	{
		echo "\nusage: remove_doublespacing [filename]\n";
	}
?>
