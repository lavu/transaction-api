<?php

	/********************************************************************************
	 * This file does all of the "admin" tasks associated with chain management.
	 * Such tasks include joining chains, removing chains, and deleting chains
	 * or any other task not related to /cp/areas/chain_management
	 * 
	 * Author: Benjamin G. Bean
	 * benjamin@poslavu.com
	 ********************************************************************************/
	
	if(!function_exists("lavu_query"))
	{
		require_once(dirname(dirname(__FILE__)) . '/cp/resources/lavuquery.php');
	}
	require_once(dirname(dirname(__FILE__)).'/cp/objects/json_decode.php');
	require_once(dirname(dirname(__FILE__)).'/sa_cp/billing/payment_profile_functions.php');
	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";
	
	// $s_proposed_chain_name should be the company name that is spawning the chain
	// returns an unused chain name on success or empty string on error
	// rules:
	//     returned name isn't longer than 97 characters, plus an index
	//     if the name ends in a number and contains other characters, the number at the end is dropped
	//     if a similar name already exists, an index is added to the name
	function find_unused_chain_name($s_proposed_chain_name) {
		global $maindb;
		$s_proposed_chain_name = substr($s_proposed_chain_name, 0, 97);
		
		// determine if the name ends in an integer
		$a_index_counter_matches = array();
		preg_match("/\.[0-9]*$/", $s_proposed_chain_name, $a_index_counter_matches);
		if (strlen($a_index_counter_matches[0]) > 0) {
			$s_match = (string)$a_index_counter_matches[0];
			$a_parts = explode($s_match, $s_proposed_chain_name);
			unset($a_parts[count($a_parts)-1]);
			$s_proposed_chain_name = implode($s_match, $a_parts);
		}
		
		// check that the name doesn't exist
		$chain_exists_query = mlavu_query("SELECT `name` FROM `[maindb]`.`restaurant_chains` WHERE `name` LIKE '[proposed_name]%'",
			array("maindb"=>$maindb, "proposed_name"=>$s_proposed_chain_name));
		if ($chain_exists_query === FALSE) {
			error_log("Bad query chain_exists_query in /sa_cp/manage_customers_functions.php");
			return "bad query";
		}
		if (mysqli_num_rows($chain_exists_query) == 0)
		// the name doesn't exist
			return $s_proposed_chain_name;
			
		// the name exists, create a new one
		$a_exists_names = array();
		while ($a_exists_name = mysqli_fetch_assoc($chain_exists_query))
			$a_exists_names[] = $a_exists_name['name'];
		for ($i = 1; $i < 10000; $i++) {
			if (!in_array($s_proposed_chain_name.'.'.$i, $a_exists_names))
				return $s_proposed_chain_name.'.'.$i;
		}
		return "conflicting names";
	}
	
	// $s_loggedin_user should be either one of the lavu users, or 'customer:'.$username
	// returns 'duplicate chain exists', 'bad restaurant id', 'failed to create chain', or 'success|'.mlavu_insert_id()
	function create_new_restaurant_chain($s_new_chain_name, $i_restaurant_id, $s_loggedin_user) {
		
		$check_chains = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurant_chains` WHERE `name` = '[1]'", $s_new_chain_name);
		if (mysqli_num_rows($check_chains) > 0)
			return 'duplicate chain exists';
		
		$vars = array();
		$vars['name'] = $s_new_chain_name;
		$vars['primary_restaurantid'] = $i_restaurant_id;
		$vars['created'] = date("Y-m-d H:i:s");
		$vars['created_by'] = $s_loggedin_user;
		$vars['history'] = $s_history;
		
		// make sure the restaurant exists		
		$a_restaurant = get_restaurant_from_id($vars['primary_restaurantid']);
		if (count($a_restaurant) == 0)
			return 'bad restaurant id';
		
		// perform the query
		$s_insert_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToInsertClause($vars);
		$create_chain = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`restaurant_chains` $s_insert_clause", $vars);
		
		// check that the query succeeded
		if ($create_chain) {
			$i_chain_id = mlavu_insert_id();
			insert_into_chain_history($i_chain_id, array('created'=>array($vars['created']=>$a_restaurant['data_name'])));
			return 'success|'.$i_chain_id;
		} else {
			return 'failed to create chain';
		}
	}
	
	// returns the json decoded value of the chain history (can be NULL or an object), or FALSE upon failure
	// if the chain history is left blank, the chain id is used to read the restaurant_chains table
	function get_chain_history($i_chain_id, $s_chain_history = '') {
		global $maindb;
		
		// the history wasn't passed in, so use the chain id to get it
		if ($s_chain_history == '') {
			$a_chain = get_chain_by_id($i_chain_id);
			if (count($a_chain) == 0)
				return FALSE;
			$s_chain_history = $a_chain['history'];
		}
		
		// parse the history
		return Json::unencode($s_chain_history);
	}
	
	// when given an id it will return an array of values from `poslavu_MAIN_db`.`restaurant_chains`
	// or array() upon failure
	function get_chain_by_id($i_chain_id) {
		$a_chains = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurant_chains', array('id'=>$i_chain_id), TRUE);
		if (count($a_chains) == 0)
			return array();
		return $a_chains[0];
	}
	
	// inserts new history information into the chain
	// 
	// the $a_new_chain_history should be in the form
	//     array('actionname'=>array('datetime'=>value))
	//   eg
	//     array('created'=>array('2013-04-16 15:13:06'=>'17edison'))
	// if the action name already exists, the new date and value will be added to it
	// if the date already exists, nothing will be changed
	// 
	// returns the number of insertions made, or
	//     -1 if the mysql query failed
	//     -2 if the previous chain history wasn't obtained
	function insert_into_chain_history($i_chain_id, $a_new_chain_history) {
		global $maindb;
		$o_history = get_chain_history($i_chain_id, '');
		$i_insertions_made = 0;
		
		// check that the chain history was obtained
		if ($o_history === FALSE)
			return -2;
		if ($o_history === NULL)
			$o_history = new stdClass();
		
		// insert the action type
		foreach($a_new_chain_history as $actionname=>$a_action) {
			if (!isset($o_history->$actionname))
				$o_history->$actionname = new stdClass();
			
			// insert the value at that date (if the date doesn't already exist)
			foreach($a_action as $s_datetime=>$wt_value) {
				if (isset($o_history->$actionname->$s_datetime))
					continue;
				$o_history->$actionname->$s_datetime = $wt_value;
				$i_insertions_made++;
			}
		}
		
		// save the data
		$s_saveval = json_encode($o_history);
		mlavu_query("UPDATE `[maindb]`.`restaurant_chains` SET `history`='[history]' WHERE `id`='[id]'",
			array('maindb'=>$maindb, 'history'=>$s_saveval, 'id'=>$i_chain_id));
		if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
			return -1;
		
		return $i_insertions_made;
	}
	
	// updates the history of the given chain
	// sets the created and joined actions for live chains, and deleted action for dead chains
	// returns TRUE upon success or FALSE upon failure
	function update_chain_histories($i_chain_id) {
		// get the chain and make sure it exists
		$a_chain = get_chain_by_id($i_chain_id);
		if (count($a_chain) == 0)
			return FALSE;
		$a_primary_restaurant = get_restaurant_from_id($a_chain['primary_restaurantid']);
		if (count($a_primary_restaurant) == 0)
			return FALSE;
		
		// get the current chain history
		$o_history = get_chain_history($i_chain_id, $a_chain['history']);
		if (($o_history) == NULL) {
			$o_history = new stdClass();
		}
		
		// check if the chain is deleted
		if ($a_chain['_deleted'] != '0') {
			if (isset($o_history->deleted))
				return TRUE;
			insert_into_chain_history($i_chain_id, array('deleted'=>array(date('Y-m-d H:i:s')=>$a_primary_restaurant['data_name'])));
			return TRUE;
		}
		
		// insert created and joined data
		$joindate = $a_chain['created'];
		insert_into_chain_history($i_chain_id, array('created'=>array($joindate=>$a_primary_restaurant['data_name'])));
		$a_restaurants = get_restaurants_by_chain($i_chain_id);
		foreach($a_restaurants as $a_restaurant) {
			if ($a_restaurant['data_name'] == $a_primary_restaurant['data_name'])
				continue;
			$a_datanames_joined = $o_history->joined;
			if (count($a_datanames_joined) > 0) {
				foreach($a_datanames_joined as $s_datatime=>$s_dataname)
					if ($s_dataname == $a_restaurant['data_name'])
						continue 2;
				insert_into_chain_history($i_chain_id, array('joined'=>array($joindate=>$a_restaurant['data_name'])));
			}
		}
		return TRUE;
	}
	
	// $s_chain_id should be either a numeric string or ""
	//     if numeric: assigns a chain to a restaurant
	//     if empty string: removes a chain from a restaurant
	function assign_chain_to_restaurant($s_chain_id, $i_restaurant_id) {
		global $maindb;
		
		$check_chains = mlavu_query("SELECT `id`, `name` FROM `[maindb]`.`restaurant_chains` WHERE `id` = '[id]'", 
			array('id'=>$s_chain_id, 'maindb'=>$maindb));
		if ((mysqli_num_rows($check_chains) > 0) || $s_chain_id=="") {
			$chain_info = mysqli_fetch_assoc($check_chains);
			$cust_read['chain_id'] = $s_chain_id;
			$update = mlavu_query("UPDATE `[maindb]`.`restaurants` SET `chain_id` = '[chain_id]', `chain_name` = '[chain_name]' WHERE `id` = '[id]'",
				array('maindb'=>$maindb, 'chain_id'=>$s_chain_id, 'chain_name'=>$chain_info['name'], 'id'=>$i_restaurant_id));
			if (!$update) echo "<script langauge='javascript'>alert(\"Error: failed to update restaurant chain...\");</script>";

			if ($update) addCustomUnitsToChainAccount($s_chain_id);
		}
	}
	
	// returns mysqli_fetch_assoc on the matching restaurant_chains table
	//     eg array(id=>id, name=>name, primary_restaurantid=>primary_restaurantid, _deleted=>_deleted, created=>created, created_by=>created_by)
	// @$b_check_primary, if true tries to find a chain based on the restaurants.id = restaurant_chains.primary_restaurantid
	// returns an empty array on failure or if the restaurant has no chain
	$a_get_restaurant_chain_cached_values = array();
	function get_restaurant_chain($s_dataname, $b_check_primary = FALSE, $a_restaurant = NULL) {
		global $maindb;
		global $a_get_restaurant_chain_cached_values;
		$cached = &$a_get_restaurant_chain_cached_values;
		
		if (	isset($cached[$s_dataname]) &&
				isset($cached[$s_dataname][$b_check_primary.'']) )
			return $cached[$s_dataname][$b_check_primary.''];
		
		if (strlen(trim($s_dataname)) == 0 && $a_restaurant === NULL)
			return array();
		
		if ($a_restaurant === NULL)
			$a_restaurant = get_restaurant_from_dataname($s_dataname);
		if (count($a_restaurant) == 0)
			return array();
		
		// get the chain by the chain_id in the chain_id is set
		$where_clause = "WHERE `id`='[id]' AND `_deleted`!='1'";
		// or get the chain by the restaurant id as the primary chain restaurant id
		if ((!isset($a_restaurant['chain_id']) || $a_restaurant['chain_id'] == '') && $b_check_primary) {
			$where_clause = "WHERE `primary_restaurantid`='[primary_restaurantid]' AND `_deleted`!='1'";
		}
		
		$s_query_string = "SELECT * FROM `[maindb]`.`restaurant_chains` ".$where_clause;
		$a_query_vars = array('maindb'=>$maindb, 'id'=>$a_restaurant['chain_id'], 'primary_restaurantid'=>$a_restaurant['id']);
		$chain_query = mlavu_query($s_query_string, $a_query_vars);// maindb

		if ($chain_query === FALSE) {
			error_log('bad chain query in '.__FILE__);
			return array();
		}
		if (mysqli_num_rows($chain_query) == 0)
			return array();
		$chain_read = mysqli_fetch_assoc($chain_query);
		mysqli_free_result($chain_query);
		
		// cache the value for future lookups
		$cached[$s_dataname][$b_check_primary.''] = $chain_read;
		return $chain_read;
	}
	
	// get the restaurant ids of a reporting group either by name or by id
	// @return: an array of restaurant ids, or an empty array() on failure
	function get_restaurant_ids_by_reporting_group($s_crg_name, $i_crg_id = 0) {
		
		// get the chain reporting group (crg)
		$a_lookvars = array('name'=>$s_crg_name);
		if ($i_crg_id != 0)
			$a_lookvars = array('id'=>$i_crg_id);
		$a_crgs = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('chain_reporting_groups', $a_lookvars, TRUE, array('selectclause'=>'contents'));
		
		// check that the crg exists
		if (count($a_crgs) == 0)
			return array();
		$a_crg = $a_crgs[0];
		
		// return all contents
		return explode(',', $a_crg['contents']);
	}
	
	// returns * from the restaurants table for each linked restaurant as an array
	// or array() upon failure
	$a_get_restaurants_by_chain_cached_values = array();
	function get_restaurants_by_chain($i_chain_id) {
		global $maindb;
		global $a_get_restaurants_by_chain_cached_values;
		$cached = &$a_get_restaurants_by_chain_cached_values;
		
		if (isset($cached[$i_chain_id]))
			return $cached[$i_chain_id];
		
		// get all restaurants that are a part of this chain
		$a_retval = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('chain_id'=>$i_chain_id), TRUE);
		
		// cache results for fast lookups in the future
		$cached[$i_chain_id] = $a_retval;
		
		return $a_retval;
	}
	
	// searches first for a chain associated with the given username
	// searches second for a chain whose primary restaurant id matches that of $s_dataname
	// returns an empty array if the chain can't be found
	function get_chain_by_username($s_username, $s_dataname) {
		global $maindb;
		
		$chain_name_query = mlavu_query("SELECT * FROM ((SELECT `dataname` FROM `[maindb]`.`customer_accounts` WHERE `username`='[username]')) `cdb` LEFT JOIN ((SELECT * FROM `[maindb]`.`restaurants`)) `rdb` ON (`cdb`.`dataname`=`rdb`.`data_name`)",
			array('maindb'=>$maindb, 'username'=>$s_username));
		if ($chain_name_query === FALSE) {
			error_log("bad chain name query in ".__FILE__);
			return array();
		}
		if (mysqli_num_rows($chain_name_query) == 0) {
			return array();
		}
		$a_chain = mysqli_fetch_assoc($chain_name_query);
		mysqli_free_result($chain_name_query);
		if (!isset($a_chain['name']) || $a_chain['name'] == '') {
			$a_chain = get_restaurant_chain($s_dataname, TRUE);
			if (count($a_chain) == 0)
				return array();
		}
		return $a_chain;
	}
	
	// returns an array of reporting groups, where each entry is a row from the `[maindb]`.`chain_reporting_groups` table
	// or returns an empty array upon failure
	$a_get_chain_reporting_groups_cached_values = array();
	function get_chain_reporting_groups($i_chain_id, $a_all_chain_restaurants) {
		global $a_get_chain_reporting_groups_cached_values;
		$cached = &$a_get_chain_reporting_groups_cached_values;
		
		if (	isset($cached[$i_chain_id]) &&
				isset($cached[$i_chain_id][print_r($a_all_chain_restaurants,TRUE)]))
			return $cached[$i_chain_id][print_r($a_all_chain_restaurants,TRUE)];
		
		$a_retval = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('chain_reporting_groups', array('chainid'=>$i_chain_id), TRUE);
		foreach($a_retval as $k=>$v) {
			if ($v['_deleted'] != '' && $v['_deleted'] != '0')
				unset($a_retval[$k]);
		}
		foreach($a_retval as $k=>$v)
			$a_retval[$k]['contents'] = explode(',', $a_retval[$k]['contents']);
		
		// cache the result for fast lookups in the future
		$cached[$i_chain_id][print_r($a_all_chain_restaurants,TRUE)] = $a_retval;
		
		return $a_retval;
	}
	
	// for functions related to chains and users
	class CHAIN_USER {
		
		// gets the username and dataname and returns any errors in $s_error (or '' if no error)
		// @return: TRUE on success, FALSE on failure
		public static function get_username_dataname(&$s_username, &$s_dataname, &$s_error) {
			
			// load the username/dataname
			$s_dataname = ($s_dataname != '') ? $s_dataname : admin_info('dataname');
			$s_username = ($s_username != '') ? $s_username : admin_info('username');
			
			// check that the username/dataname is set
			if ($s_username === FALSE || $s_username == '') {
				$s_error = 'username not found';
				return FALSE;
			}
			if ($s_dataname === FALSE || $s_dataname == '') {
				$s_error = 'dataname not found';
				return FALSE;
			}
			
			// get the username and dataname if it is an admin user (access level 4) logged in through another account
			if (strpos($s_username, $s_dataname.':') === 0) {
				$s_username = substr($s_username, strlen($s_dataname.':'));
				$s_dataname = admin_info('root_dataname');
			}
			
			// success!
			$s_error = '';
			return TRUE;
		}
		
		// gets the user with $s_username from poslavu_$s_dataname_db and sets it in $a_user
		// returns any errors in $s_error
		// @return: TRUE on success, FALSE otherwise
		private static function get_user($s_dataname, $s_username, &$a_user, &$s_error) {
			if ($s_username === 'poslavu_admin') {
				$a_fullname = explode(' ', admin_info('fullname'));
				$a_user = array(
					'id'=>0,
					'company_code'=>admin_info('companyid'),
					'username'=>$s_username,
					'f_name'=>$a_fullname[0],
					'l_name'=>$a_fullname[1],
					'email'=>admin_info('email'),
					'access_level'=>4,
					'PIN'=>'1234',
					'loc_id'=>1,
					'phone'=>admin_info('phone'),
					'lavu_admin'=>1,
					'_deleted'=>0,
					'created_date'=>date('Y-m-d H:i:s'),
					'active'=>1,
					'clocked_in'=>1,
					'lavu_admin'=>1,
				);
				return TRUE;
			}
			
			$a_users = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('users', array('username'=>$s_username), TRUE,
				array('databasename'=>'poslavu_'.$s_dataname.'_db'));
			if (count($a_users) === 0) {
				$s_error = 'user not found';
				return FALSE;
			}
			$a_user = array_merge(array('lavu_admin'=>1), $a_users[0]);
			return TRUE;
		}
		
		// returns all restaurant rows that this user has access to,
		// as determined by their access_level and reporting_groups
		// see get_user_reporting_groups for arguments ($s_username, $s_dataname, $s_error)
		// @return: an array of all restaurant rows as arrays, or the restaurant of the associated admin_info('dataname') on failure
		public static function get_user_restaurants_access($s_username = '', $s_dataname = '', &$s_error = NULL) {
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
			
			// if level 4 just return all locations!
			if(function_exists("sessvar"))
			{
				if(sessvar('admin_access_level')>=4)
				{
					$current_rest_read = $dbapi->getAllInTable('restaurants', array('data_name'=>admin_info('dataname')), TRUE);
					if(isset($current_rest_read[0]) && isset($current_rest_read[0]['chain_id']) && $current_rest_read[0]['chain_id']!="")
					{
						return $dbapi->getAllInTable('restaurants', array('chain_id'=>$current_rest_read[0]['chain_id']), TRUE);
					}
				}
			}
			$a_reporting_groups = self::get_user_reporting_groups($s_username, $s_dataname, $s_error);
			if (count($a_reporting_groups) == 0)
				return $dbapi->getAllInTable('restaurants', array('data_name'=>admin_info('dataname')), TRUE);
			
			// get the restaurant ids
			$a_ids = array();
			foreach($a_reporting_groups as $a_crg) {
				$ids = get_restaurant_ids_by_reporting_group('', $a_crg['id']);
				foreach($ids as $s_id)
					$a_ids[$s_id] = $s_id;
			}
			unset($ids);
			unset($a_reporting_groups);
			if (count($a_ids) == 0)
				return $dbapi->getAllInTable('restaurants', array('data_name'=>admin_info('dataname')), TRUE);
			
			// load the current user's restaurant first
			$s_companyid = admin_info('root_companyid');
			if (isset($a_ids[$s_companyid]))
				unset($a_ids[$s_companyid]);
			$a_this_restaurant = $dbapi->getAllInTable('restaurants', array('id'=>$s_companyid), TRUE);
		
			$a_other_restaurants = array();
			if (count($a_ids)) {
				// get the other restaurants
				$s_in_clause = $dbapi->arrayToInClause($a_ids);
				$a_other_restaurants = $dbapi->getAllInTable('restaurants', NULL, TRUE,
					array('whereclause'=>"WHERE `id` ".$s_in_clause));
			}
			
			// combine the restaurants
			$a_restaurants = array();
			if (count($a_this_restaurant) > 0)
				$a_restaurants = $a_this_restaurant;
			if (count($a_other_restaurants) > 0)
				foreach($a_other_restaurants as $a_restaurant)
					$a_restaurants[] = $a_restaurant;
			
			// release memory and check that any restaurants were loaded
			unset($a_this_restaurant);
			unset($a_other_restaurants);
			if (count($a_restaurants) == 0)
				return $dbapi->getAllInTable('restaurants', array('data_name'=>admin_info('dataname')), TRUE);
			
			// success!
			return $a_restaurants;
		}
		
		// get the reporting groups of the given user
		// @$s_username: the username, or the current user if $s_username == ''
		// @$s_dataname: the dataname, or the current dataname if $s_dataname == ''
		// @$error: a string to be set with the reason for returning empty array (empty string if no error)
		// @return: an array of the reporting_group_ids, or an empty array() on failure
		public static function get_user_reporting_groups($s_username = '', $s_dataname = '', &$s_error = NULL) {
			$s_error = '';
			
			// get the dataname and user and username, and check that they are set
			if (!self::get_username_dataname($s_username, $s_dataname, $s_error))
				return array();
			$a_user = array();
			if (!self::get_user($s_dataname, $s_username, $a_user, $s_error))
				return array();
			
			// check the access level of the user
			if ((int)$a_user['access_level'] < 3) {
				$s_error = 'not high enough access_level';
				return array();
			}
			
			// get the chain for the dataname
			$a_restaurant_chain = get_restaurant_chain($s_dataname);
			$b_part_of_chain = (count($a_restaurant_chain) !== 0) ? TRUE : FALSE;
			if ($b_part_of_chain) {
				// get the chain reporting groups
				$i_chain_id = (int)$a_restaurant_chain['id'];
				$a_chain_reporting_groups = get_chain_reporting_groups($i_chain_id, get_restaurants_by_chain($i_chain_id));
				if (count($a_chain_reporting_groups) == 0) {					
					$s_error = 'no reporting groups for this chain';
					return array();
				}
			} else {
				$s_error = 'no associated chain';
				return array();
			}
			
			// return all reporting groups if the user has a high enough access_level
			if ($a_user['access_level'] >= 4)
			{
				return $a_chain_reporting_groups;
			}
			
			// load the reporting group ids and remove any that are zeroes
			$a_reporting_group_ids = Json::unencode($a_user['reporting_groups']);
			if (is_array($a_reporting_group_ids) && count($a_reporting_group_ids) > 0) {
				$a_to_remove = array();
				foreach($a_reporting_group_ids as $k=>$v)
					if ((int)$v == 0)
						$a_to_remove[] = $k;
				foreach($a_to_remove as $k)
					unset($a_reporting_group_ids[$k]);
			}
			
			// get the user reporting groups if it is json
			if ((is_object($a_reporting_group_ids) || is_array($a_reporting_group_ids)) && count($a_reporting_group_ids)) {
				if (is_object($a_reporting_group_ids)) $a_reporting_group_ids = (array) $a_reporting_group_ids;
				$a_newvals = array();
				foreach($a_reporting_group_ids as $s_id) {
					foreach($a_chain_reporting_groups as $a_crg) {
						if ((int)$s_id == (int)$a_crg['id']) {
							$a_newvals[] = $a_crg;
							break;
						}
					}
				}
				$a_reporting_groups = $a_newvals;
				unset($a_reporting_group_ids);
				unset($a_newvals);
			}
			
			// get the user reporting groups if it isn't json
			if ($a_reporting_group_ids === NULL || count($a_reporting_group_ids) === 0) {
				if ($a_user['reporting_groups'] === '') {
					$a_reporting_groups = array();
				} else {
					foreach($a_chain_reporting_groups as $a_crg)
						if ((int)$a_user['reporting_groups'] == (int)$a_crg['id'])
							$a_reporting_groups = array($a_crg);
				}
			}
			
			return $a_reporting_groups;
		}
		
		// given an array of reporting group ids, grants a level 3 user access of the given reporting groups
		// @$s_username: the username, or the current user if $s_username == ''
		// @$s_dataname: the dataname, or the current dataname if $s_dataname == ''
		// @return: 'success', 'not high enough access_level', 'dataname not found', ''username not found', 'user not found', or 'failed to update'
		public static function set_user_reporting_groups($a_reporting_group_ids, $s_username = '', $s_dataname = '') {
			$s_error = 'success';
			
			// get the dataname and user and username, and check that they are set
			if (!self::get_username_dataname($s_username, $s_dataname, $s_error))
				return $s_error;
			$a_user = array();
			if (!self::get_user($s_dataname, $s_username, $a_user, $s_error))
				return $s_error;
			
			// check the access level of the user
			if ((int)$a_user['access_level'] < 3) {
				$s_error = 'not high enough access_level';
				return array();
			}
			
			$b_user_mlavu = ConnectionHub::getConn('poslavu')->getDBAPI()->userMlavuQuery($s_dataname);
			$a_query_vars = array('dataname'=>$s_dataname, 'reporting_groups'=>json_encode($a_reporting_group_ids), 'username'=>$s_username);
			if ($b_user_mlavu)
				$s_success = mlavu_query("UPDATE `poslavu_[database]_db`.`users` SET `reporting_groups`='[reporting_groups]' WHERE `username`='[username]'", $a_query_vars); // use_mlavu
			else
				$s_success = lavu_query("UPDATE `users` SET `reporting_groups`='[reporting_groups]' WHERE `username`='[username]'", $a_query_vars);
			
			if ($s_success)
				return 'success';
			return 'failed to update';
		}
		
		public static function user_can_access_chains($s_chain_access, &$a_reporting_group_access = NULL) {
			return user_can_access_chains($s_chain_access, $a_reporting_group_access);
		}
	}
	
	// checks that the logged in user has the right access level to access chains
	// for the moment, a user must have access level 4 or be "admin_loggedin()"
	// @$a_reporting_group_access: reporting groups this user has access to
	// @return: "success" or "print error[*note*]".error_message
	function user_can_access_chains($s_chain_access, &$a_reporting_group_access = NULL) {
		$s_retval = 'print error[*note*]Your user doesn\'t have access to this functionality.';
		
		// start the session, if necessary
		if (!isset($_SESSION))
			session_start();
		
		// check if admin
		if (admin_info('lavu_admin')) {
			$a_chain = get_restaurant_chain(admin_info('dataname'));
			if (count($a_chain) > 0) {
				$a_chain_restaurants = get_restaurants_by_chain($a_chain['id']);
				$a_reporting_group_access = get_chain_reporting_groups($a_chain['id'], $a_chain_restaurants);
			} else {
				$a_reporting_group_access = array();
			}
			return "success";
		}
		
		// get the dataname and username, and check that they are set
		$b_success = CHAIN_USER::get_username_dataname($s_username, $s_dataname, $s_error);
		if (!$b_success && strstr($s_error, 'username'))
			return 'print error[*note*]Cannot find your username. Refresh the page and try again.';
		if (!$b_success && strstr($s_error, 'dataname'))
			return 'print error[*note*]Cannot find your restaurant\'s dataname. Refresh the page and try again.';
		if (!$b_success)
			return 'print error[*note*]'.$s_error;
		
		// get the user and check that they exist
		lavu_connect_dn($s_dataname,'poslavu_'.$s_dataname.'_db');
		$user_query_string = "select * from `poslavu_[dataname]_db`.`users` where `_deleted`!='1' and `username`='[username]'";
		$user_query_vars = array('username'=>$s_username, 'dataname'=>$s_dataname);
		$user_query = lavu_query($user_query_string, $user_query_vars);
		if (mysqli_num_rows($user_query) == 0)
			return 'print error[*note*]Your user could not be found. Refresh the page and try again.';
		$user_read = mysqli_fetch_assoc($user_query);
		mysqli_free_result($user_query);
		
		// check the user's permissions
		switch($s_chain_access) {
			case 'create_chain':
			case 'join_chain':
			case 'remove_chain':
				if (in_array($user_read['access_level'], array('4')))
					$s_retval = "success";
				break;
			case 'sync_menu':
				if (in_array($user_read['access_level'], array('3', '4')))
					$s_retval = "success";
				break;
		}
		
		if ($s_retval === 'success')
			$a_reporting_group_access = CHAIN_USER::get_user_reporting_groups($s_username, $s_dataname);
		
		return $s_retval;
	}
	
	// remove a restaurant from a chain
	// return any errors in $s_error
	// @return: TRUE on success, FALSE on failure
	function remove_restaurant_from_chain($s_dataname, &$s_error = NULL) {
		global $maindb;
		$s_error = '';
		
		// get the chain id from the restaurant
		$restaurant_query = mlavu_query("SELECT `chain_id`,`chain_name`,`id` FROM `[maindb]`.`restaurants` WHERE `data_name`='[data_name]'",
			array('maindb'=>$maindb, 'data_name'=>$s_dataname));
		if ($restaurant_query === FALSE) {
			error_log('bad restaurant query in '.__FILE__);
			$s_error = 'This restaurant doesn\'t have a chain associated with it';
			return FALSE;
		}
		if (mysqli_num_rows($restaurant_query) == 0) {
			$s_error = 'This restaurant doesn\'t have a chain associated with it';
			return FALSE;
		}
		$a_restaurant = mysqli_fetch_assoc($restaurant_query);
		$s_chain_id = $a_restaurant['chain_id'];
		if ($s_chain_id == '') {
			$s_error = 'This restaurant doesn\'t have a chain associated with it';
			return FALSE;
		}
		
		// remove the chain from the restaurant
		$a_update_vars = array('chain_id'=>'', 'chain_name'=>'');
		$s_updateClause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToUpdateClause($a_update_vars);
		$success = mlavu_query("UPDATE `[maindb]`.`restaurants` ".$s_updateClause." WHERE `data_name`='[data_name]'",
			array_merge(array('maindb'=>$maindb, 'data_name'=>$s_dataname), $a_update_vars));
		if ($success === FALSE) {
			$s_error = 'Updating the database failed. Please try again.';
			return FALSE;
		}
		
		// remove the restaurant from the reporting groups
		$a_chain_restaurants = get_restaurants_by_chain($s_chain_id);
		$a_crgs = get_chain_reporting_groups($s_chain_id, $a_chain_restaurants);
		foreach($a_crgs as $a_crg) {
			$a_restaurant_ids = $a_crg['contents'];
			foreach($a_restaurant_ids as $k=>$s_rest_id) {
				if ((int)$s_rest_id == (int)$a_restaurant['id']) {
					unset($a_restaurant_ids[$k]);
					$s_restaurant_ids = implode(',', $a_restaurant_ids);
					$s_mark_as_deleted = (count($a_restaurant_ids) == 0) ? ",`_deleted`='1'" : "";
					$s_query_string = "UPDATE `[database]`.`chain_reporting_groups` SET `contents`='[contents]'".$s_mark_as_deleted." WHERE `id`='[id]'";
					$a_query_vars = array('database'=>$maindb, 'contents'=>$s_restaurant_ids, 'id'=>$a_crg['id']);
					mlavu_query($s_query_string, $a_query_vars); //maindb
					break;
				}
			}
		}
		
		// remove the restaurant from the chain_replications tables
		$a_restaurants = get_restaurants_by_chain($s_chain_id);
		foreach($a_restaurants as $a_other_restaurant) {
			lavu_connect_dn($a_other_restaurant['data_name'],'poslavu_'.$a_other_restaurant['data_name'].'_db');
			$s_query_string = "DELETE FROM `poslavu_[other_dataname]_db`.`chain_replications` WHERE `restaurant_syncto_dataname`='[dataname]'";
			$a_query_vars = array('other_dataname'=>$a_other_restaurant['data_name'], 'dataname'=>$s_dataname);
			lavu_query($s_query_string, $a_query_vars);
		}
		lavu_connect_dn($s_dataname,'poslavu_'.$s_dataname.'_db');
		$s_query_string = "DELETE FROM `poslavu_[dataname]_db`.`chain_replications`";
		$a_query_vars = array('dataname'=>$s_dataname);
		lavu_query($s_query_string, $a_query_vars);
		
		insert_into_chain_history(
			$s_chain_id, 
			array( 'removed'=>array(date('Y-m-d H:i:s')=>$s_dataname) )
		);
		
		update_restaurant_chain_check_if_deleted($s_chain_id);


		loyaltree_update(array('id'=>"0"), $s_dataname);
		return TRUE;
	}
	
	// deletes a chain if there are no more restaurants in it
	function update_restaurant_chain_check_if_deleted($s_chainid) {
		global $maindb;
		
		// check for any other restaurants with the given chain id
		// mark the chain as deleted if there are no other restaurants with that chain id
		$a_other_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('chain_id'=>$s_chainid), TRUE);
		if (count($a_other_restaurants) == 0) {
			mlavu_query("UPDATE `[maindb]`.`restaurant_chains` SET `_deleted`='1' WHERE `id`='[id]'",
				array('maindb'=>$maindb, 'id'=>$s_chainid));
			insert_into_chain_history(
				$s_chainid,
				array( 'deleted'=>array(date('Y-m-d H:i:s')=>$s_dataname) )
			);
		}
	}
	
	// syncs all menu data from $s_dataname to the other restaurants in its chain
	// runs setDefaultSyncSettings() and then syncFromRestaurant() from the file 'chain_management'
	// checks that the restaurant is part of a chain, that the logged in user has level 4 access, and that there are reporting groups for this chain
	// @$b_capture_stdout: to use or not to use ob_start and ob_get_contents and ob_end_clean
	// @return: TRUE if the data was synced, FALSE if the restaurant is not part of a chain or there are no reporting groups in the chain or the user logged in doesn't have access
	function syncChainDatabases($s_dataname, $b_capture_stdout = TRUE) {
		
		// check that the logged in user has access
		$s_user_has_access = user_can_access_chains("sync_menu", $a_reporting_group_access);
		if ($s_user_has_access !== 'success')
			return FALSE;
		
		// get the restaurant's chain and
		// check that it is part of a chain
		$a_chain = get_restaurant_chain($s_dataname);
		if (count($a_chain) == 0)
			return FALSE;
		
		// get the restaurants in this chain and active restaurants
		$a_restaurant = get_restaurant_from_dataname($s_dataname);
		$a_restaurants = get_restaurants_by_chain($a_chain['id']);
		
		// remove inactive restaurants
		$a_to_remove = array();
		foreach($a_restaurants as $k=>$a_other_restaurant)
			if (strstr($a_other_restaurant['notes'],'AUTO_DELETED'))
				$a_to_remove[$k] = $a_other_restaurant['data_name'];
		if (count($a_to_remove) > 0) {
			foreach($a_to_remove as $k=>$s_rest_dataname) {
				error_log('removing restaurant '.$s_rest_dataname);
				remove_restaurant_from_chain($s_rest_dataname);
				unset($a_restaurants[$k]);
			}
		}
		
		// get the reporting groups of this restaurant
		$a_crgs = get_chain_reporting_groups($a_chain['id'], $a_restaurants);
		
		// check that there are any reporting groups (also verifies that this restaurant is part of a chain)
		if (count($a_crgs) == 0 || count($a_reporting_group_access) == 0)
			return FALSE;
		
		// capture output
		if (!class_exists('syncFunc'))
			require_once('/home/poslavu/public_html/admin/cp/areas/chain_management.php');
		if ($b_capture_stdout) {
			ob_start();
		}
		
		// sync the menus
		syncFunc::setDefaultSyncSettings($a_restaurant['id'], $a_reporting_group_access);
		if (count($a_crgs) != count($a_reporting_group_access)) {
			foreach($a_reporting_group_access as $a_crg)
				syncFunc::syncFromRestaurant($s_dataname, $a_crg['name'], FALSE);
		} else {
			syncFunc::syncFromRestaurant($s_dataname, $a_crg['name'], TRUE);
		}
		
		// capture output
		if ($b_capture_stdout) {
			$str_dumpval = ob_get_contents();
			ob_end_clean();
		}
		
		return TRUE;
	}
	
	// get the reporting groups that a menu item is allowed to sync to
	// @$s_selected_chain_id: the id of the reporting group currently being used for this item
	// @$i_no_crg: integer value of the default "no chain reporting group" option
	// @$i_parent_indicator: integer value of the reporting group that indicates a parent reporting group
	// @$s_parent_name: name of the parent reporting group option (eg "default menu category")
	// @$b_has_access: set to TRUE if the user has access to this menu item's reporting groups, FALSE otherwise
	// @return: a string represtinging the options to a "select"
	// see also: getReportingGroupOptionsForDimensionalForm
	function getChainReportingGroupOptions($s_selected_chain_id, $s_dataname, $i_no_crg = 0, $i_parent_indicator = FALSE, $s_parent_name = '', &$b_has_access = NULL) {
		$a_chain = get_restaurant_chain($s_dataname);
		$a_restaurants = get_restaurants_by_chain($a_chain['id']);
		$a_all_crgs = get_chain_reporting_groups($a_chain['id'], $a_restaurants);
		$a_crgs = $a_all_crgs;
		$a_user_crgs = CHAIN_USER::get_user_reporting_groups();
		$b_user_limited_options = FALSE;
		
		// compare the user and restaurant reporting groups
		$a_to_remove = array();
		foreach($a_crgs as $k=>$a_crg) {
			$b_found = FALSE;
			foreach($a_user_crgs as $a_user_crg) {
				if ((int)$a_user_crg['id'] == (int)$a_crg['id']) {
					$b_found = TRUE;
					break;
				}
			}
			if (!$b_found) {
				$b_user_limited_options = TRUE;
				$a_to_remove[] = $k;
			}
		}
		foreach($a_to_remove as $k)
			unset($a_crgs[$k]);
		
		$s_retval = '';
		
		// find the reporting group that this user has access to
		$b_has_access = FALSE;
		$s_groups_user_has_access_to = '';
		foreach($a_crgs as $a_crg) {
			$selected = '';
			if ((int)$a_crg['id'] == (int)$s_selected_chain_id) {
				$b_has_access = TRUE;
				$selected = 'SELECTED';
			}
			$s_groups_user_has_access_to .= '<option value="'.$a_crg['id'].'" '.$selected.'>'.$a_crg['name'].'</option>';
		}
		if ((int)$s_selected_chain_id <= 0)
			$b_has_access = TRUE;
		
		// draw the default options that this user has access to ("no reporting group"/"default parent reporting group")
		if (!$b_user_limited_options || $b_has_access == TRUE || (int)$i_no_crg == (int)$s_selected_chain_id) {
			$s_retval .= '<option value="'.$i_no_crg.'">Chain Group</option>';
			if ($i_parent_indicator !== FALSE) {
				$s_retval .= '<option value="'.$i_parent_indicator.'">'.$s_parent_name.'</option>';
			}
		}
		
		// draw the reporting group options that this user has access to
		$s_retval .= $s_groups_user_has_access_to;
		
		// the user doesn't have access to any of the reporting groups, so only display the currently selected value
		if (!$b_has_access && $b_user_limited_options) {
			foreach($a_all_crgs as $a_crg) {
				if ((int)$a_crg['id'] == (int)$s_selected_chain_id) {
					$s_retval = '<option value="'.$s_selected_chain_id.'">'.$a_crg['name'].'</option>';
					break;
				}
			}
		}
		
		return $s_retval;
	}
	
	// returns the values in $a_chain_groups and $a_chain_items, to be used for eg:
	// $cfields[] = array("chain_reporting_group", "chain_reporting_group", speak("Chain Reporting Group"), "select_exclusive", $a_chain_groups);
	// NOTE: only returns the crgs that the current user has access to (users with access level 4 have access to all crgs)
	// @return: TRUE or FALSE, if it's a part of a chain or not
	// see also: getChainReportingGroupOptions
	function getReportingGroupOptionsForDimensionalForm($data_name, &$a_chain_groups, &$a_chain_items) {
		$a_restaurant_chain = get_restaurant_chain($data_name);
		$b_part_of_chain = (count($a_restaurant_chain) !== 0) ? TRUE : FALSE;
		if ($b_part_of_chain) {
			
			// get the chain reporting groups
			$i_chain_id = (int)$a_restaurant_chain['id'];
			$a_all_crgs = get_chain_reporting_groups($i_chain_id, get_restaurants_by_chain($i_chain_id));
			$a_crgs = $a_all_crgs;
			$a_user_crgs = CHAIN_USER::get_user_reporting_groups('', '', $s_error);
			
			// compare the user and restaurant reporting groups
			$a_to_remove = array();
			foreach($a_crgs as $k=>$a_crg) {
				$b_found = FALSE;
				if (is_array($a_user_crgs) && count($a_user_crgs) > 0) {
					foreach($a_user_crgs as $a_user_crg) {
						if ((int)$a_user_crg['id'] == (int)$a_crg['id']) {
							$b_found = TRUE;
							break;
						}
					}
				}
				if (!$b_found)
					$a_to_remove[] = $k;
			}
			foreach($a_to_remove as $k)
				unset($a_crgs[$k]);
			
			// put the groups into arrays for the category and item level
			// -2 is no reporting group for items, 0 is category default
			// -1 is all locations
			// 0 is no reporting group for categories
			$a_chain_groups = array(array('Chain Group',0));
			$a_chain_items = array(array('Default Chain Group',0), array('No Chain Group', -2));
			foreach($a_crgs as $a_chain_reporting_group) {
				$i_reporting_group_id = (int)$a_chain_reporting_group['id'];
				$a_chain_item = array();
				$a_chain_item = array($a_chain_reporting_group['name'], $i_reporting_group_id);
				$a_chain_groups[] = $a_chain_item;
			}
		}
		return $b_part_of_chain;
	}
	
	// checks if the given dataname is part of a chain
	function getIsDataNameInChain($data_name) {
		$a_restaurant_chain = get_restaurant_chain($data_name);
		$b_part_of_chain = (count($a_restaurant_chain) !== 0) ? TRUE : FALSE;
		return $b_part_of_chain;
	}

	// If new restaurant added into chain restaurant then custom units will also add into new chain restaurant
	function addCustomUnitsToChainAccount ($chainId, $customUnitMeasuresDetails = array(), $currentDataname = '') {
		$chainRestaurants = get_restaurants_by_chain($chainId);
		if (!empty($chainRestaurants)) {
            if (empty($customUnitMeasuresDetails)) {
                foreach ($chainRestaurants as $restaurantDetails) {
                    if ($restaurantDetails['data_name'] != '' && $restaurantDetails['data_name'] != $currentDataname) {
                        $chainDb = "poslavu_" . $restaurantDetails['data_name'] . "_db";
                        lavu_connect_dn($restaurantDetails['data_name'], $chainDb);
                        $customUnitMeasureQuery = 'SELECT * FROM `' . $chainDb . '`.`custom_units_measure` WHERE _deleted = 0';
                        $customUnitMeasureQuery = lavu_query($customUnitMeasureQuery);
                        if (mysqli_num_rows($customUnitMeasureQuery) > 0) {
                            while ($customUnitRead = mysqli_fetch_assoc($customUnitMeasureQuery)) {
                                $customUnitMeasuresDetails[$customUnitRead['symbol']] = $customUnitRead;
                            }
                        }

                    }
                }
            }

            foreach ($chainRestaurants as $restaurantDetails) {
                if ($restaurantDetails['data_name'] != '' && $restaurantDetails['data_name'] != $currentDataname) {
                    $chainDb = "poslavu_" . $restaurantDetails['data_name'] . "_db";
                    lavu_connect_dn($restaurantDetails['data_name'], $chainDb);
					foreach ($customUnitMeasuresDetails as $customSymbol => $customDetails) {
						$customUnitMeasureQuery = "SELECT * FROM `" . $chainDb . "`.`custom_units_measure` WHERE `symbol` = '[1]'";
						$customUnitMeasureQuery = lavu_query($customUnitMeasureQuery, $customSymbol);
						if (mysqli_num_rows($customUnitMeasureQuery) > 0 ) {
							$customUnitRead = mysqli_fetch_assoc($customUnitMeasureQuery);
							$id = $customUnitRead['id'];
							$updateCustomUnitQuery = 'UPDATE `' . $chainDb . '`.`custom_units_measure` SET `categoryID` = [1], `unitID` = [2], `conversion` = [3], `_deleted` = ' . $customDetails['_deleted'] . ' WHERE id = "' . $id . '"';
							lavu_query($updateCustomUnitQuery, $customDetails['categoryID'], $customDetails['unitID'], $customDetails['conversion']);
                            lavu_query("UPDATE `" . $chainDb . "`.`inventory_unit` SET `_deleted` = '" . $customDetails['_deleted'] . "' WHERE `unitID` = '" . $id . "'");
						} else {
							$insertCustomUnitQuery = "INSERT INTO `" . $chainDb . "`.`custom_units_measure` (`name`, `symbol`, `categoryID`, `unitID`, `conversion`) VALUES ('[1]', '[2]', [3], [4], [5])";
							lavu_query($insertCustomUnitQuery, $customDetails['name'], $customDetails['symbol'], $customDetails['categoryID'], $customDetails['unitID'], $customDetails['conversion']);
                            $customId = lavu_insert_id();
                            lavu_query("INSERT INTO `" . $chainDb . "`.`inventory_unit` (`unitID`,`categoryID`) VALUES ('[1]', '[2]')", $customId, 3);
						}
					}
                }
            }
        }
	}
	/**
	* This function will mark chanined gift card(duplicate) and update with lesser amount
	*
	* @param string chainid
	* @param string dataname.
	* @param  array chainedDataname
	*/
	function addRemoveChainedGiftCard( $chainid, $dataname, $chainedDataname = array() ) {
		if (empty($chainedDataname)) {
			$chainedRestaurants =get_restaurants_by_chain($chainid);	#get all chained restaurants
			foreach( $chainedRestaurants as $restaurants ){
				if ($dataname == $restaurants['data_name'] ){
					continue;
				}
				$chainedDataname[] = $restaurants['data_name'];
			}
			#read all gift card details of chained restaurants
			$existingChainCardSql = mlavu_query('SELECT `inactive`, `chainid`, `name`, `balance`,`id`, `created_datetime`, `points`, `expires`, `history`, `dataname`  FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted` = 0 AND (`chainid` = "[1]" OR `dataname` IN ("'.implode('","', $chainedDataname).'"))', $chainid);
		}else {
			#read all gift card details of chained restaurants to remove duplicate gift card from existing chain
			$existingChainCardSql = mlavu_query('SELECT `inactive`, `chainid`, `name`, `balance`,`id`, `created_datetime`, `points`, `expires`, `history`, `dataname`  FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted` = 0 AND (`dataname` IN ("'.implode('","', $chainedDataname).'"))');
		}		

		#read gift card details of new dataname being chained
		$newChainCardSql = mlavu_query('SELECT `inactive`, `chainid`, `name`, `balance`,`id`, `created_datetime`, `points`, `expires`, `history`, `dataname`  FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `deleted` = 0 AND `dataname`="[1]"', $dataname);

		if (mysqli_num_rows($existingChainCardSql) > 0 && mysqli_num_rows($newChainCardSql) > 0) {
			#new chanin location's gift card details
			while ($newChainCardDetails = mysqli_fetch_assoc($newChainCardSql) ) {
				$duplicateInactiveInChain = 0;
				$duplicateGiftCard = 0;
				#existing chanin location's gift card details
				while ($existingChainCardDetails = mysqli_fetch_assoc($existingChainCardSql)) {

					#check duplicate gift card and take minimum balance if duplicate card is exist
					if ($newChainCardDetails['name'] == $existingChainCardDetails['name']) {
						$duplicateGiftCard = 1;
						if ($existingChainCardDetails['inactive'] == 1) {
							mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET deleted = 1 WHERE `id` = '[1]'", $existingChainCardDetails['id']);
							$duplicateInactiveInChain = 1;
							break;
						}					
						if ($newChainCardDetails['balance'] < $existingChainCardDetails['balance']) {
							$newBalance = $newChainCardDetails['balance'];
							if ($newChainCardDetails['points'] < $existingChainCardDetails['points']) {
								$newPoints = $newChainCardDetails['points'];
							}else {
								$newPoints = $existingChainCardDetails['points'];
							}
							#marke gift card as deleted of new dataname being chained
							mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET deleted = 1 WHERE `id` = '[1]'", $newChainCardDetails['id']);

							#update lavu gift history before updating balane of gift card
							updateLavuGiftHistory("Joined Chain", $newBalance, $newPoints, $existingChainCardDetails, $chainid ); 
							
							#update new balance(minimium balance of duplicate card) in existing chained dataname's gift card
							mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET balance = '".$newBalance."', points='".$newPoints."'  WHERE `id` = '".$existingChainCardDetails['id']."'");
						}else { 
							#marke gift card as deleted of new dataname being chained. We are considering minimum balance during merge
							mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET deleted = 1 WHERE `id` = '[1]'",$newChainCardDetails['id']);
						}
					}
				}
				if ($duplicateGiftCard && !$duplicateInactiveInChain && $newChainCardDetails['inactive'] == 1) {
					mlavu_query("UPDATE `poslavu_MAIN_db`.`lavu_gift_cards` SET deleted = 1 WHERE `id` = '[1]'", $newChainCardDetails['id']);
				}
				mysqli_data_seek($existingChainCardSql, 0); #reset existing chain gift card resultset
			}
		}		
	}
	/**
	* This function will create gift change history if chain is being created
	* @param string $action.
	* @param string $chainid
	* @param string $newBalance
	* @param array  $giftCardInfo
	*/
	function updateLavuGiftHistory($action, $newBalance, $newPoints, $giftCardInfo, $chainid="" ) {
		$select_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`lavu_gift_cards` WHERE `name`='[1]' AND `dataname`='[2]'", $giftCardInfo['name'], $giftCardInfo['dataname']);

		if (mysqli_num_rows($select_query) > 0) {
			$cardRead = mysqli_fetch_assoc($select_query);
			$currentAmount = floatval($cardRead['balance']);
			$currentPoints = floatval($cardRead['points']);
			if (empty($currentAmount) || is_nan($currentAmount)) $currentAmount = 0;
			if (empty($currentPoints) || is_nan($currentPoints)) $currentPoints = 0;

			$histVars = array();
			$histVars['chainid'] = $chainid;
			$histVars['dataname'] = $giftCardInfo['dataname'];
			$histVars['name'] = $giftCardInfo['name'];
			$histVars['datetime'] = date("Y-m-d H:i:s");
			$histVars['card_id'] = $cardRead['id'];
			$histVars['action'] = $action;
			$histVars['previous_points'] = $currentPoints;
			$histVars['previous_balance'] = $currentAmount;
			$histVars['point_amount'] = $cardRead['point_amount'];
			$histVars['balance_amount'] = $newBalance;
			$histVars['new_points'] = $newPoints;
			$histVars['new_balance'] = $newBalance;
			$histVars['item_id_list'] = "";

			#Created gift card history
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`lavu_gift_history` (`chainid`, `dataname`, `name`, `card_id`, `action`, `previous_points`, `previous_balance`, `point_amount`, `balance_amount`, `new_points`, `new_balance`, `item_id_list`, `datetime`) VALUES ('[chainid]', '[dataname]', '[name]', '[card_id]', '[action]', '[previous_points]', '[previous_balance]', '[point_amount]', '[balance_amount]', '[new_points]', '[new_balance]', '[item_id_list]', '[datetime]')", $histVars);
		}
	}

?>
