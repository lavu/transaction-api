<?php

	require_once(dirname(__FILE__) . "/report_functions.php");
	require_once(resource_path() . "/chain_functions.php");
	
	$allow_multi_location = false;
	if(isset($_GET['test_multi_location'])) { $_SESSION['test_multi_location'] = $_GET['test_multi_location']; }
	if(isset($_SESSION['test_multi_location']) && $_SESSION['test_multi_location']==1) $allow_multi_location = true;
			
	function ast_replace($fieldname,$str)
	{
		$str = str_replace("**","((asterisk))",$str);
		$str = str_replace("*",$fieldname,$str);
		$str = str_replace("((asterisk))","*",$str);
		return $str;
	}
	
	function export_report($filename,$str)
	{
		$known_mime_types=array(
			"pdf" => "application/pdf",
			"txt" => "text/plain",
			"html" => "text/html",
			"htm" => "text/html",
			"exe" => "application/octet-stream",
			"zip" => "application/zip",
			"doc" => "application/msword",
			"xls" => "application/vnd.ms-excel",
			"ppt" => "application/vnd.ms-powerpoint",
			"gif" => "image/gif",
			"png" => "image/png",
			"jpeg"=> "image/jpg",
			"jpg" =>  "image/jpg",
			"php" => "text/plain"
		);
		$file_extension = strtolower(substr(strrchr($filename,"."),1));
		$mime_type = (isset($known_mime_types[$file_extension]))?$known_mime_types[$file_extension]:$file_extension;
		
		/*if(strpos($str,"&#8364;")!==false && $file_extension=="xls")
		{
			$mime_type = "text/plain";
			$str = "........" . $str;
		}*/
		
		header('Content-Type: ' . $mime_type);
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		header("Content-Transfer-Encoding: binary");
		header('Accept-Ranges: bytes');
		
		header("Cache-control: private");
		header('Pragma: private');
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		
		header("Content-Length: ".strlen($str));
		echo $str;
	}
	
	function rs_start_val($val,$def="")
	{
		if(isset($_SESSION['rselector_'.$val])) return $_SESSION['rselector_'.$val];
		else return $def;
	}
	function set_rs_start_val($val,$set)
	{
		$_SESSION['rselector_'.$val] = $set;
	}
	function rs_getvar($var,$def)
	{
		//return (isset($_GET[$var]))?$_GET[$var]:$def;
		$setval = (isset($_GET[$var]))?$_GET[$var]:rs_start_val($var,$def);
		set_rs_start_val($var,$setval);
		return $setval;
	}
	
	function display_money($val, $loc_info, $ts=",") {
	
		$sym = $loc_info['monitary_symbol'];
		$lor = $loc_info['left_or_right'];
		$dp = $loc_info['disable_decimal'];
		$dc = $loc_info['decimal_char'];
	
			if ($sym == "")
				return number_format(round($val, $dp), $dp, $dc, $ts);
			else if ($lor == "right")
				return number_format(round($val, $dp), $dp, $dc, $ts)." ".$sym;
			else
				return $sym." ".number_format(round($val, $dp), $dp, $dc, $ts);
	}
	
	function show_report($dbname,$maindb,$date_mode,$rurl,$reportid,$special=false,$set_locationid=false,$is_combo_rep=false,$return_query=false)
	{
		//$DEBUG = "$dbname\n$maindb\n$date_mode\n$rurl\n$reportid\n$special\n$set_locationid\n$is_combo_rep\n\n\n\n";
		
		//--------------------------------------------- Multi Location Code -------------------------------------//
		if(isset($_GET['multireport']))
			$showing_multi_report = $_GET['multireport'];
		else
			$showing_multi_report = false;

		global $allow_multi_location;
		$selector_str = "";
		$multi_location_ids = "";
		if($allow_multi_location)
		{
			$cr_dataname = $dbname;
			$cr_dataname = substr($cr_dataname,8);
			$cr_dataname = substr($cr_dataname,0,strlen($cr_dataname) - 3);

			if(isset($_GET['mode']) && substr($_GET['mode'],0,7)=="reports")
			{
				$cr_groups = CHAIN_USER::get_user_reporting_groups();
				$cr_group_name = "";
				for($n=0; $n<count($cr_groups); $n++)
				{
					if($showing_multi_report==$cr_groups[$n]['id'])
					{
						$cr_group_name = $cr_groups[$n]['name'];
						$multi_location_ids = $cr_groups[$n]['contents'];
					}
				}
				
				if(count($cr_groups) > 0)
				{
					if($showing_multi_report)
					{				
						$uri = $_SERVER['QUERY_STRING'];
						
						$uri = str_ireplace("&multireport=".$showing_multi_report, "", $uri);
						$selector_str = "Showing $cr_group_name <a href='index.php?" . $uri . "'>(show only current location)</a><br>";
					}
					else
					{
						$uri = $_SERVER['QUERY_STRING'];
						$selector_str .= "<select onchange='window.location = \"index.php?$uri&multireport=\" + this.value'>";
						$selector_str .= "<option value=''>Showing Current Location</option>";
						for($n=0; $n<count($cr_groups); $n++)
						{
							$groupid = $cr_groups[$n]['id'];
							$groupname = $cr_groups[$n]['name'];
							$selector_str .= "<option value='$groupid'>$groupname</option>";
						}
						$selector_str .= "</select>";
						//echo "<a href='index.php?".$uri."&multireport=1'>(show all locations)</a><br>";
					}
				}
			}
		}
		//-------------------------------------------------------------------------------------------------------//
		
		$show_extra_functions = false;
		if(isset($_GET['test48']))
			$show_extra_functions = true;
		
		if($special=="debug") $debug = true; else $debug = false;
		if(strpos($special,"export")!==false) $export = true; else $export = false;
		if(substr($special,0,10)=="subreport:") 
		{
			$use_subreport = true;
			$subreport = substr($special,10);
		}
		else $use_subreport = false;
		
		if(!$export && ! $return_query)
		{
			createWhiteBox();
		}
		
		if($set_locationid)
			$locationid = $set_locationid;
		else if(sessvar_isset("locationid"))
			$locationid = sessvar("locationid");
		else
			$locationid = false;
		
		$date_start = "";
		$date_end = "";
		$time_start = "";
		$time_end = "";
		
		$primary_table = "";
		$join_code = "";
		$select_code = "";
		$select_outer_code = "";
		$where_code = "";
		$stitles = array();
		$sdisplay = array();
		$smonetary = array();
		$ssum = array();
		$ssumop = array();
		$sgraph = array();
		$sbranches = array();
		$salign = array();
		$scount = 0;
		
		$groupid = "";
		$dateid = "";
		$timeid = "";
		$groupby = "";
		
		$output = "";
		$export_output = "";
		$export_row = "\r\n";
		$export_col = "\t";
		if($special=="export:xls") $export_ext = "xls";
		else if($special=="export:csv") { $export_ext = "csv"; $export_col = ","; }
		else $export_ext = "txt";
				
		$branchto = (isset($_GET['branch']))?$_GET['branch']:"";
		$branch_title = (isset($_GET['branch_title']))?$_GET['branch_title']:"";
		$branch_col = (isset($_GET['branch_col']))?$_GET['branch_col']:"";
		$branch_fieldname = "";
		$repeat_foreach = "";
		
		if($reportid=="select_date")
			$date_selector = true;
		else
			$date_selector = false;
			
		if($date_selector)
		{
			$date_select_type = "Day Range";
		}
		else
		{
			$report_query = mlavu_query("select * from `$maindb`.`reports` where id='$reportid'");
			if(mysqli_num_rows($report_query))
			{
				$report_read = mysqli_fetch_assoc($report_query);
				$groupid = $report_read['groupid'];
				$dateid = $report_read['dateid'];
				$locid = $report_read['locid'];
				$timeid = $report_read['timeid'];
				$group_operation = $report_read['group_operation'];
				$order_operation = $report_read['order_operation'];
				$orderby = explode(" ",$report_read['orderby']);
				$date_select_type = $report_read['date_select_type'];
				$report_title = $report_read['title'];
				$repeat_foreach = $report_read['repeat_foreach'];
				$extra_functions = $report_read['extra_functions'];
				$set_unified = $report_read['unified'];
				
				if(isset($_GET['test'])) echo "date mode: $set_unified <br>";
						if($set_unified=="No") $date_mode = "single";
				
				if($report_read['report_type']=="Combo")
					$date_mode = "combo|" . $date_mode;
				
				if(count($orderby) > 1)
				{
					$orderby_id = trim($orderby[0]);
					$orderby_dir = trim($orderby[1]);
				}
				else
				{
					$orderby_id = 0;
					$orderby_dir = "asc";
				}
				$orderby = "";
				
				if($use_subreport) 
				{
					$output .= "<b>" . $subreport . "</b>";
				}
				else
				{
					$output .= "Report: ";
					if($is_combo_rep)
						$output .= "<a href='?mode=reports_id_".$report_read['id']."'><b>" . $report_read['title'] . "</a>";
					else
						$output .= "<b>" . $report_read['title'];
					if($branch_col!="") $output .= ": " . $branch_title;
					$output .= "</b>";
					
					$output .= "&nbsp;" . $selector_str;
				}
				$output .= "<br><br>";
			}
		}
				
		$explicit_date_end = false;
		$rep_type = "Regular";
		if(substr($date_mode,0,5)=="auto|")
		{
			$date_parts = explode("|",$date_mode);
			$date_mode = $date_parts[1];
			$date_start = $date_parts[2];
			$time_start = $date_parts[3];
			$date_end = $date_parts[4];
			$time_end = $date_parts[5];
			$date_select_type = "auto";
			$rep_type = "SubReport";
			
			$set_date_start = $date_start;
			$set_date_end = $date_end;
			$set_time_start = $time_start;
			$set_time_end = $time_end;
			//if(isset($_GET['test'])) echo "SETDATE:".$set_date_start . " - " . $set_date_end;
			
			$explicit_date_end = true;
		}
		if(substr($date_mode,0,6)=="combo|")
		{
			$date_parts = explode("|",$date_mode);
			$date_mode = $date_parts[1];
			$rep_type = "Combo";
			$date_select_type = "Day Range";
		}
		//if(isset($_GET['test'])) echo "date mode: $date_mode <br>";
		
		$change_group_from = "";
		$change_group_to = "";
		$change_group_id = "";
		$reporturl = "$rurl&reportid=$reportid&rmode=view";
		//------------------------------------ date/time selection here -----------------------------------//
		if($date_select_type=="Year")
		{
			$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y");
			$output .= "<select onchange='window.location = \"$reporturl&setdate=\" + this.value'>";
			for($i=date("Y")-8; $i<date("Y")+8; $i++)
			{
				$sv = $i;
				$output .= "<option value='$sv'";
				if($sv==$setdate) $output .= " selected";
				$output .= ">".date("Y",mktime(0,0,0,0,0,$i+1))."</option>";
			}
			$output .= "</select>";
			$set_date_start = $setdate . "-01-01";
			$set_date_end = $setdate . "-12-31";
			$set_time_start = "00:00:00";
			$set_time_end = "24:00:00";
		}
		else if($date_select_type=="Month")
		{
			$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y-m");
			$output .= "<select onchange='window.location = \"$reporturl&setdate=\" + this.value'>";
			for($i=date("Y")-2; $i<date("Y")+2; $i++)
			{
				for($n=1; $n<=12; $n++)
				{
					if($n<10) $m = "0".$n; else $m = $n;
					$sv = $i . "-" . $m;
					$output .= "<option value='$sv'";
					if($sv==$setdate) $output .= " selected";
					$output .= ">".date("M Y",mktime(0,0,0,$n+1,0,$i))."</option>";
				}
			}
			$output .= "</select>";
			$set_date_start = $setdate . "-01";
			$set_date_end = $setdate . "-31";
			$set_time_start = "00:00:00";
			$set_time_end = "24:00:00";
		}
		else if($date_select_type=="Day" || $date_select_type=="Day Range")
		{
			//$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y-m-d");
			//$day_duration = (isset($_GET['day_duration']))?$_GET['day_duration']:1;
			//$stime = (isset($_GET['stime']))?$_GET['stime']:0;
			
			if(date("H") < 7) $default_setdate = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 1,date("Y")));
			else $default_setdate = date("Y-m-d");
			
			$setdate = rs_getvar("setdate",$default_setdate);
			$day_duration = rs_getvar("day_duration",1);
			$stime = rs_getvar("stime",0);
			
			$output .= "<script type=\"text/javascript\" src=\"resources/datepickercontrol.js\"></script>";
			$output .= "<link type=\"text/css\" rel=\"stylesheet\" href=\"resources/datepickercontrol.css\">";
			$output .= "<script language='javascript'>";
			$output .= "DatePickerControl.onSelect = function(inputid) { ";
			$output .= "   window.location = '$reporturl&day_duration=$day_duration&stime=$stime&setdate=' + document.getElementById(inputid).value; ";
			$output .= "} ";
			$output .= "</script>";
							
			$set_date_start = $setdate;
			$date_start_ts = mktime(0,0,0,substr($set_date_start,5,2),substr($set_date_start,8,2),substr($set_date_start,0,4));
			
			$output .= "<input type=\"hidden\" id=\"DPC_TODAY_TEXT\" value=\"today\">";
			$output .= "<input type=\"hidden\" id=\"DPC_BUTTON_TITLE\" value=\"Open calendar...\">";
			$output .= "<input type=\"hidden\" id=\"DPC_MONTH_NAMES\" value=\"['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']\">";
			$output .= "<input type=\"hidden\" id=\"DPC_DAY_NAMES\" value=\"['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']\">";
			$output .= "<input type=\"text\" name=\"edit1\" id=\"edit1\" datepicker=\"true\" datepicker_format=\"YYYY-MM-DD\" value=\"".date("m/d/Y",$date_start_ts)."\">";
			if($date_select_type=="Day Range")
			{
				$output .= "<select onchange='window.location = \"$reporturl&setdate=$setdate&stime=$stime&day_duration=\" + this.value'>";
				for($i=1; $i<=365; $i++)
				{
					$output .= "<option value='$i'";
					if($day_duration==$i) $output .= " selected";
					$output .= ">";
					$output .= "to " . date("m/d/Y",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($i - 1),date("Y",$date_start_ts))) . " ($i day";
					if($i > 1) $output .= "s";
					$output .= ")";
					$output .= "</option>";
				}
				$output .= "</select>";
			}
			else $day_duration = 1;
			
			$set_date_end = date("Y-m-d",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($day_duration - 1),date("Y",$date_start_ts)));
			$set_time_start = "00:00:00";
			$set_time_end = "24:00:00";
			
			$set_nextdate = date("Y-m-d",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($day_duration),date("Y",$date_start_ts)));
			$output .= "<input type='button' value='Next >>' onclick='window.location = \"$reporturl&setdate=$set_nextdate&stime=$stime&day_duration=$day_duration\"'>";
			
			/*$output .= " End of Day: <select name='stime' onchange='window.location = \"$reporturl&setdate=$setdate&day_duration=$day_duration&stime=\" + this.value'>";
			for($t=0; $t<=900; $t+=100)
			{
				$hour = (($t - ($t % 100)) / 100);
				$min = ($t % 100);
				if($min < 10) $min = "0" . $min;
				if($t==0) $show_time = "12:00am";
				else $show_time = $hour . ":" . $min . "am";
				$output .= "<option value='$t'";
				if($stime==$t) $output .= " selected";
				$output .= ">$show_time</option>";
			}
			$output .= "</select>";*/
		}
		
		$date_reports_by = "opened";
		$rep_time_query = mlavu_query("select * from `$maindb`.`report_parts` where `reportid`='[1]' and `id`='[2]'",$reportid,$dateid);
		if(mysqli_num_rows($rep_time_query))
		{
			$rep_time_read = mysqli_fetch_assoc($rep_time_query);
			//if(isset($_GET['testing'])) echo "<br>found report open/close: ".$rep_time_read['fieldname']."-".$date_select_type."<br>";
			if(($rep_time_read['fieldname']=="opened"||$rep_time_read['fieldname']=="closed") && ($date_select_type=="Year"||$date_select_type=="Month"||$date_select_type=="Day"||$date_select_type=="Day Range"||$date_select_type=="auto"))
			{
				if(isset($_GET['date_reports_by']))
				{
					$cfg_query = mlavu_query("select * from `[1]`.`config` where `location`='[2]' and `type`='special' and `setting`='report config'",$dbname,$locationid);
					if(mysqli_num_rows($cfg_query))
					{
						$cfg_read = mysqli_fetch_assoc($cfg_query);
						mlavu_query("update `[1]`.`config` set `value`='[2]' where `id`='[3]'",$dbname,$_GET['date_reports_by'],$cfg_read['id']);
					}
					else
						mlavu_query("insert into `[1]`.`config` (`location`,`type`,`setting`,`value`) values ('[2]','special','report config','[3]')",$dbname,$locationid,$_GET['date_reports_by']);
				}
				
				$cfg_query = mlavu_query("select * from `[1]`.`config` where `location`='[2]' and `type`='special' and `setting`='report config'",$dbname,$locationid);
				if(mysqli_num_rows($cfg_query))
				{
					$cfg_read = mysqli_fetch_assoc($cfg_query);
					$date_reports_by = $cfg_read['value'];
				}
				
				if($date_select_type!="auto")
				{
					$output .= " <select name='date_reports_by' onchange='window.location = \"$reporturl&date_reports_by=\" + this.value'>";
					$output .= "<option value='opened'".($date_reports_by=="opened"?" selected":"").">Report using orders OPENED on this date</option>";
					$output .= "<option value='closed'".($date_reports_by=="closed"?" selected":"").">Report using orders CLOSED on this date</option>";
					$output .= "</select>";
				}
			}
		}
		
		//
		$apply_stime = false;
		if($locationid)
		{
			$location_query = mlavu_query("select * from `$dbname`.`locations` where `id`='[1]'",$locationid);
			if(mysqli_num_rows($location_query))
			{
				$location_read = mysqli_fetch_assoc($location_query);
				$stime = $location_read['day_start_end_time'];
				if(is_numeric($stime))
				{
					$stime_mins = $stime % 100;
					$stime_hours = ($stime - $stime_mins) / 100;
					if($stime_hours < 10) $stime_hours = "0".$stime_hours;
					if($stime_mins < 10) $stime_mins = "0".$stime_mins;
					$apply_stime = $stime_hours . ":" . $stime_mins . ":00";
				}
				else
				{
					$apply_stime = false;
				}
			}
		}

		if($apply_stime)
		{
			$add_to_date_end = 1;
			if(($date_mode!="unified") || $explicit_date_end)
			{
				$add_to_date_end = 0;
			}
			$date_end_ts = mktime(0,0,0,substr($set_date_end,5,2),substr($set_date_end,8,2),substr($set_date_end,0,4));
			$set_date_end = date("Y-m-d",mktime(0,0,0,date("m",$date_end_ts),date("d",$date_end_ts) + $add_to_date_end,date("Y",$date_end_ts)));
			$set_time_start = $apply_stime;
			$set_time_end = $apply_stime;
		}
		if($date_selector)
		{
			echo $output;
			$ret = array();
			$ret['date_start'] = $set_date_start;
			$ret['time_start'] = $set_time_start;
			$ret['date_end'] = $set_date_end;
			$ret['time_end'] = $set_time_end;
			$ret['date_reports_by'] = $date_reports_by;
			$ret['locationid'] = $locationid;
			
			return $ret;
		}
		//if(isset($_GET['test'])) echo "STIME: $apply_stime ";

		//

		if($rep_type=="Combo")
		{
			if($report_read['title']=="Balance Summary")
			{
				$date_start = "$set_date_start $set_time_start";
				$date_end = "$set_date_end $set_time_end";
				
				//if(isset($_GET['tt'])) echo "PROCESS FIX from $date_start to $date_end<br>";
				process_fix_all($date_reports_by,$date_start,$date_end,$locationid);				
			}
			
			echo $output;
			echo "<hr>";
			$rep_count = 0;
			$rep_query = mlavu_query("select `report_parts`.`prop1` as `prop1`, `report_parts`.`id` as `id`, `reports`.`title` as `title` from `$maindb`.`report_parts` LEFT JOIN `$maindb`.`reports` ON `report_parts`.`prop1`=`reports`.`id` where `report_parts`.`reportid`='$reportid' and `report_parts`.`type`='report' order by `_order` asc, id asc");
			while($rep_read = mysqli_fetch_assoc($rep_query))
			{
				if($rep_count > 0) echo "<hr>";
				$date_str = $set_date_start . "|" . $set_time_start . "|" . $set_date_end . "|" . $set_time_end;
				//if(isset($_GET['test'])) echo $dbname . "<br>" . $maindb . "<br>" . "auto:".$date_mode.":".$date_str . "<br>" . $rurl . "<br>" . $rep_read['prop1'] . "<br>" . $special . "<br><br>";
				$debug_combo_report = false;//(isset($_GET['test']))?true:false;
				$use_report_id = $rep_read['prop1'];
				if($dbname=="poslavu_the_fat_cow_db")
				{
					if($use_report_id==24) $use_report_id = 133;
				}
				
				show_report($dbname,$maindb,"auto|".$date_mode."|".$date_str,$rurl,$use_report_id,$debug_combo_report,$locationid,true);
				$rep_count++;
			}
			return;
		}
		else if($repeat_foreach!="" && !$use_subreport)
		{
			echo $output;
			echo "<hr>";
			$fe_parts = explode(".",$repeat_foreach);
			if(count($fe_parts)>1)
			{
				$fe_table = str_replace("`","",$fe_parts[0]);
				$fe_table = str_replace("'","",$fe_table);
				$fe_col = str_replace("`","",$fe_parts[1]);
				$fe_col = str_replace("'","",$fe_col);
				
				$where_code = "";
				$dt_query = mlavu_query("select * from `poslavu_MAIN_db`.`report_parts` where `id`='[1]'",$dateid);
				if(mysqli_num_rows($dt_query))
				{
					$dt_read = mysqli_fetch_assoc($dt_query);
					$date_start = "$set_date_start $set_time_start";
					$date_end = "$set_date_end $set_time_end";
					$date_col = "`".$dt_read['tablename'] . "`.`".$dt_read['fieldname']."`";
					$where_code = " where $date_col >= '$date_start' and $date_col <= '$date_end'";
				}
				
				if (substr($fe_table, 0, 1) == '-') {
					$where_code = '';
					$fe_table = substr($fe_table, 1);
				}
				
				$fe_query = mlavu_query("select DISTINCT(`$fe_col`) as `fe_value` from `$dbname`.`$fe_table`".$where_code);
				while($fe_read = mysqli_fetch_assoc($fe_query))
				{
					$date_str = $set_date_start . "|" . $set_time_start . "|" . $set_date_end . "|" . $set_time_end;
					//echo "dbname: " . $dbname . "<br>maindb: " . $maindb . "<br>date_mode: " . $date_mode . "<br>date_str: " . $date_str . "<br>rurl: " . $rurl . "<br>reportid: " . $reportid . "<br>subreport: " . $fe_read['fe_value'] . "<br>locationid: " . $locationid . "<br>";
					show_report($dbname,$maindb,"auto|".$date_mode."|".$date_str,$rurl,$reportid,"subreport:".$fe_read['fe_value'],$locationid,false);
				}
			}
			return;
		}
				
		//
		
		// old position of apply_stime and date_selector snippets
		
		//
		
		if($set_unified=="Yes") $date_mode = "unified"; else if($set_unified=="No") $date_mode = "mixed";
		if($date_mode=="unified")
		{
			$date_start = "$set_date_start $set_time_start";
			$date_end = "$set_date_end $set_time_end";
		}
		else
		{
			$date_start = "$set_date_start";
			$date_end = "$set_date_end";
			$time_start = $set_time_start;
			$time_end = $set_time_end;
		}
		
		if(!isset($locationid))
		{
			/*if(isset(reqvar("loc_id")) $locationid = reqvar("loc_id");
			else if(isset(reqvar("locationid")) $locationid = reqvar("locationid");
			else*/ $locationid = 0;
		}
		
		$join_after = array();
		$tables_joined = array();
		$rep_query = mlavu_query("select * from `$maindb`.`report_parts` where `reportid`='$reportid' order by `_order` asc, `id` asc");
		while($rep_read = mysqli_fetch_assoc($rep_query))
		{
			$set_table = $rep_read['tablename'];
			$set_field = $rep_read['fieldname'];
			$set_prop1 = $rep_read['prop1'];
			$set_prop2 = $rep_read['prop2'];
			$set_type = $rep_read['type'];
			$set_operation = $rep_read['operation'];
			$set_opargs = $rep_read['opargs'];
			$set_filter = $rep_read['filter'];
			$set_label = $rep_read['label'];
			$use_graph = $rep_read['graph'];
			$use_branch = $rep_read['branch'];
			$use_monetary = $rep_read['monetary'];
			$use_sumop = $rep_read['sum'];
			$use_align = $rep_read['align'];
			$partid = $rep_read['id'];
			$hide = ($rep_read['hide']=="1")?true:false;
			
			if($use_branch=="1")
			{
				$show_label = $set_label;
				if($show_label=="") $show_label = $set_field;
				$sbranches[] = array($set_table,$set_field,$partid,$show_label);
			}
			
			if($primary_table=="")
			{
				$primary_table = $set_table;
				$tables_joined[] = $set_table;
			}
			else $tables_joined[] = $primary_table;
						
			if($set_type=="join")
			{
				$set_table_joined = false;
				$set_table2_joined = false;
				
				for($x=0; $x<count($tables_joined); $x++)
				{
					if($tables_joined[$x]==$set_table)
						$set_table_joined = true;
					if($tables_joined[$x]==$set_prop1)
						$set_table2_joined = true;
				}
				if($set_table_joined)
				{
					$set_prop_joined = false;
					for($x=0; $x<count($tables_joined); $x++)
					{
						if($tables_joined[$x]==$set_prop1)
							$set_prop_joined = true;
					}
					if($set_prop_joined)
					{
						$fcode = "`$set_prop1`";
					}
					else
					{
						$tables_joined[] = $set_prop1;
						$fcode = from_table($set_prop1,"[DBNAME]");
					}
					$join_code .= " LEFT JOIN ".$fcode." ON `$set_table`.`$set_field` = `$set_prop1`.`$set_prop2`";
				}
				else if($set_table2_joined)
				{
					$tables_joined[] = $set_table;
					$fcode = from_table($set_table,"[DBNAME]");
					$join_code .= " LEFT JOIN ".$fcode." ON `$set_prop1`.`$set_prop2` = `$set_table`.`$set_field`";
				}
				else
				{
					//$tables_joined[] = $set_table;
					$fcode = from_table($set_table,"[DBNAME]");
					$join_after[$set_prop1] = " LEFT JOIN ".$fcode." ON `$set_prop1`.`$set_prop2` = `$set_table`.`$set_field`";
					/*if(isset($_GET['test']))
					{
						echo "<br>join after $set_prop1 - " . $join_after[$set_prop1] . "<br>";
					}*/
				}
				if(isset($join_after[$set_table]) && $join_after[$set_table]!="")
				{
					$tables_joined[] = $set_table;
					$join_code .= $join_after[$set_table];
					$join_after[$set_table] = "";
				}
			}
			else if($set_type=="select")
			{
				$sname = "`$set_table`.`$set_field`";
				
				$alias_list[] = array($sname,"`field".$scount."`");
				
				$prime_sname = $sname;
				if($set_filter!="")
				{
					if($where_code!="") $where_code .= " and ";
					$set_filter = str_replace("'","''",$set_filter);
					$where_code .= ast_replace($sname,$set_filter);//str_replace("*",$sname,$set_filter);
				}
				if($dateid==$partid)
				{
					$date_sname = $sname;
					if($set_field=="opened" || $set_field=="closed")
					{
						$date_sname = "`$set_table`.`$date_reports_by`";
					}
					
					if($where_code!="") $where_code .= " and ";
					if(isset($_GET['testing'])) echo "date_sname: $date_sname - $date_reports_by<br>";
					$where_code .= "$date_sname >= '$date_start' and $date_sname <= '$date_end'";
				}
				if($timeid==$partid)
				{
					if($where_code!="") $where_code .= " and ";
					$where_code .= "$sname >= '$time_start' and $sname <= '$time_end'";
				}
				if($locid==$partid)
				{
					if($where_code!="") $where_code .= " and ";
					$where_code .= "$sname = '$locationid'";
				}
				if($set_operation!="")
				{
					if($set_operation=="custom")
					{
						if($set_opargs!="")
							$sname = ast_replace($sname,$set_opargs);//str_replace("*",$sname,$set_opargs);
					}
					else
					{
						$sname = strtoupper($set_operation) . "(" . $sname;
						if($set_opargs!="")
						{
							$set_opargs = str_replace("'","''",$set_opargs);
							$sname .= "," . $set_opargs;
						}
						$sname .= ")";
					}
				}
				
				if($branchto!="")
				{
					if($branchto==$partid)
					{
						/*if($group_operation && $group_operation!="")
							$groupby = " group by " . ast_replace($prime_sname,$group_operation);//str_replace("*",$prime_sname,$group_operation);
						else*/
							$groupby = " group by `field$scount` ";
							$change_group_to = count($stitles);
							if(isset($_GET['test3'])) echo "CHANGE GROUP TO: $change_group_to<br>";
					}
					else if($groupid==$partid)
					{
						$change_group_from = count($stitles);
						if(isset($_GET['test3'])) echo "CHANGE GROUP FROM: $change_group_from<br>";
					}
				}
				else
				{
					if($groupid==$partid)
					{
						if($group_operation && $group_operation!="")
							$groupby = " group by " . ast_replace($prime_sname,$group_operation);//str_replace("*",$prime_sname,$group_operation);
						else
							$groupby = " group by `field$scount` ";
						$change_group_id = count($stitles);
					}
				}
				
				if($branch_col=="field".$scount)
				{
					$branch_fieldname = $sname;
				}
				
				$sdisplay[] = !$hide;
				if($set_label!="")
					$stitles[] = $set_label;
				else
					$stitles[] = $set_field;
				$smonetary[] = $use_monetary;
				$ssumop[] = $use_sumop;
				$sgraph[] = $use_graph;
				$salign[] = $use_align;
				$ssum[] = 0;
				
				if($select_code!="") $select_code .= ",";
				$select_code .= "$sname AS `field$scount`";
				
				if($select_outer_code!="") $select_outer_code .= ",";
				if(strpos(strtolower($sname),"sum(")!==false || strpos(strtolower($sname),"count(")!==false)
					$select_outer_code .= "sum(`innerquery`.`field$scount`) AS `field$scount`";
				else
					$select_outer_code .= "`innerquery`.`field$scount` AS `field$scount`";
					
				if($orderby_id==$partid)
				{
					if($order_operation && $order_operation!="")
						$orderby = " order by " . ast_replace($prime_sname,$order_operation) . " " . $orderby_dir;
					else
						$orderby = " order by `field$scount` ".$orderby_dir;
				}
				$scount++;
			}
		}
		
		if($repeat_foreach!="" && $use_subreport)
		{
			$fe_parts = explode(".",$repeat_foreach);
			if(count($fe_parts)>1)
			{
				$fe_table = str_replace("`","",$fe_parts[0]);
				$fe_table = str_replace("'","",$fe_table);
				$fe_table = str_replace("-","",$fe_table);
				$fe_col = str_replace("`","",$fe_parts[1]);
				$fe_col = str_replace("'","",$fe_col);
				$fe_value = str_replace("'","''",$subreport);
				
				if($where_code!="") $where_code .= " and ";
				$where_code .= "`$fe_table`.`$fe_col` = '$fe_value'";
			}
		}
		if($branchto!="" && $branch_col!="")
		{
			$branch_title = str_replace("'","''",$branch_title);
			if($where_code!="") $where_code .= " and ";
			$where_code .= "$branch_fieldname = '$branch_title'";
		}
		if($where_code!="")
			$where_code = "where ".$where_code;
			
		$process_ag_data = false;
		if(strpos($select_code . " " . $where_code,"ag_cash") || strpos($select_code . " " . $where_code,"ag_card"))
			$process_ag_data = true;
		if($process_ag_data)
		{
			lavu_select_db($dbname);
			if(isset($_GET['view_ag_process'])) echo "View AG process:<br>";
			process_ag_data($date_reports_by,$date_start,$date_end,$locationid);
		}
		if(isset($_GET['explain']))
		{
			process_explain_mismatches($date_reports_by,$date_start,$date_end,$locationid);
		}
		
		if($showing_multi_report)
		{
			$inner_selects = "";
			$multi_db_list = array($dbname);
			
			if(trim($multi_location_ids)!="")
			{
				$multi_db_list = array();
				$multi_ids = explode(",",$multi_location_ids);
				for($n=0; $n<count($multi_ids); $n++)
				{
					$mid = trim($multi_ids[$n]);
					$mrest_query = mlavu_query("select `data_name`,`id` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$mid);
					if(mysqli_num_rows($mrest_query))
					{
						$mrest_read = mysqli_fetch_assoc($mrest_query);
						$multi_db_list[] = "poslavu_" . $mrest_read['data_name'] . "_db";
					}
				}
				if(count($multi_db_list) < 1)
					$multi_db_list = array($dbname);
					
				$inner_selects = "";
				for($n=0; $n<count($multi_db_list); $n++)
				{
					$multi_dbname = $multi_db_list[$n];
					$sel1_code = "select '[DBNAME]' as `cc`, " . $select_code . " from ".from_table($primary_table,"[DBNAME]").$join_code.$where_code.$groupby;
					$sel1_code = str_replace("[DBNAME]",$multi_dbname,$sel1_code);
					
					if($inner_selects!="")
						$inner_selects .= " UNION ALL ";
					$inner_selects .= "(" . $sel1_code . ")";
				}
				if($showing_multi_report){
					$query_start = "select `innerquery`.`cc` as `cc`, $select_outer_code from (".$inner_selects.") AS `innerquery`";
				}
				else{
					$query_start = "select $select_outer_code from (".$inner_selects.") AS `innerquery`";
				}
			}
		}
		else
		{
			$query_start = "select SQL_NO_CACHE $select_code from ".from_table($primary_table,"[DBNAME]").$join_code.$where_code;
			$query_start = str_replace("[DBNAME]",$dbname,$query_start);
		}
		
		//---DEBUG---
		//echo $query_start . "<br>";
		//$query_start = "select SQL_NO_CACHE $select_code from ".from_table($primary_table).$join_code.$where_code;
		$query_start = str_replace("(selected_date)",$setdate,$query_start);
		$set_stime = $stime;
		$stime_mins = $set_stime % 100;
		$stime_hours = ($set_stime - $stime_mins) / 100;
		if($stime_hours < 10) $stime_hours = "0" . $stime_hours;
		if($stime_mins < 10) $stime_mins = "0" . $stime_mins;
		$set_stime = $stime_hours . ":" . $stime_mins . ":00";
		$query_start = str_replace("(day_start_end)",$set_stime,$query_start);
		$groupby = str_replace("(day_start_end)",$set_stime,$groupby);
		$query_start = str_replace("(open_close)",$date_reports_by,$query_start);
		$groupby = str_replace("(open_close)",$date_reports_by,$groupby);
		$sdate_parts = explode("-",$setdate);
		if(count($sdate_parts) > 2)
		{
			$nextday = date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + 1,$sdate_parts[0]));
		}
		else $nextday = "";
		$query_start = str_replace("(next_day)",$nextday,$query_start);
		
		if(isset($_GET['test2'])) echo "(selected_date) = $setdate<br>(next_day) = $nextday<br>(day_start_end) = $set_stime<br>";
		/*if($primary_table=="orders" || $primary_table=="schedule")
		{
			$second_query = str_replace("`schedule`","`schedule_archive`",str_replace("`orders`","`orders_archive`",$query_start));
			$query_start .= " UNION ALL " . $second_query; 
			> '(selected_date) (day_start_end)' && < '(next_day) (day_start_end)'
		}*/
		if(isset($_GET['order_by_col']))
		{
			if($orderby=="") $orderby = " order by `" . $_GET['order_by_col'] . "` * 1 asc, `" . $_GET['order_by_col'] . "` ";
			else $orderby = str_replace("order by","order by `" . $_GET['order_by_col'] . "` * 1 asc, `" . $_GET['order_by_col'] . "` asc,",$orderby);
		}
		
		for($n=0; $n<count($alias_list); $n++)
		{
			if($showing_multi_report)
			{
				$orderby = str_replace($alias_list[$n][0],$alias_list[$n][1],$orderby);
				$groupby = str_replace($alias_list[$n][0],$alias_list[$n][1],$groupby);
			}
		}
		
		$query = $query_start . $groupby.$orderby;
		if(isset($_GET['test44'])) echo $query . "<br><br>";
		
		if($return_query)
		{
			for($i = ($scount-1); $i >=0; $i--)
			{
				$query = str_replace('field'.$i, $stitles[$i], $query);
			}
			return $query;
		}
		
		//////////////////////////////////////////////////////
		if($change_group_from=="") $change_group_from = 999999;
		if($debug) $output .= $query . "<br><br>";			
		lavu_select_db($dbname);
		$output .= "<table cellpadding=4>";
		$output .= "<tr>";
		for($i=0; $i<count($stitles); $i++)
		{
			if($sdisplay[$i])
			{
				$output .= "<td syle='border-bottom:solid 1px black;' align='center'";
				if(isset($_GET['mode']))
				{
 					$output .= " onclick='window.location = \"?mode=".$_GET['mode']."&order_by_col=field".$i."\"'";
 				}
				$output .= "><b><font style='cursor:s-resize;'>";
				if($change_group_from==$i)
				{
					$output .= $stitles[$change_group_to];
				}
				else
				{
					$output .= $stitles[$i];
				}
				$output .= "</font></b></td>";
				$export_output .= $stitles[$i] . $export_col;
			}
		}
		if(trim($extra_functions)!="" && $show_extra_functions)
		{
			//extra_functions
			require_once(dirname(__FILE__) . "/report_extra_functions.php");
			
			$extra_function_list = explode(",",$extra_functions);
			for($x=0; $x<count($extra_function_list); $x++)
			{
				$function_name = trim($extra_function_list[$x]);
				
				$output .= "<td syle='border-bottom:solid 1px black;' align='center'><b><font>";
				$output .= run_extra_report_function($function_name,false,false,"title");
				$output .= "</td>";
			}
		}
		$output .= "</tr>";

		$graph_vals = array();
		
		// here we will start sending the reports V1 to the replica, however I have 
		// not being able to trigger this file yet, however since this is definitely 
		// part of the V1 reports we are sending it to the replica either way
		if($showing_multi_report){
			$result_query = mlavu_query($query, "[_empty_]", "[_empty_]", "[_empty_]", "[_empty_]", "[_empty_]", "[_empty_]", TRUE, TRUE); // use_mlavu
		}
		else
		{
			$result_query = lavu_query($query, "[_empty_]", "[_empty_]", "[_empty_]", "[_empty_]", "[_empty_]", "[_empty_]", TRUE, TRUE);
		}
		if(mysqli_num_rows($result_query))
		{
			while($result_read = mysqli_fetch_assoc($result_query))
			{
				$export_output = substr($export_output, 0, -1).$export_row;
				$output .= "<tr>";
				for($i=0; $i<$scount; $i++)
				{
					if($sdisplay[$i])
					{
						$output .= "<td";
						if((($stitles[$i]=="order_id" || strtolower($stitles[$i])=="order id")) && ($result_read['field'.$i] != "") && (substr($result_read['field'.$i], 0, 4) != "Paid") && ($result_read['field'.$i] != "0"))
							$output .= " style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999; color:#000088;' onclick='showWhiteBox(\"Order ID: ".$result_read['field'.$i]."<br><iframe src=\\\"index.php?show_page=reports/order_details&order_id=".$result_read['field'.$i]."\\\" width=\\\"100%\\\" height=\\\"100%\\\" frameborder=\\\"0\\\"></iframe>\")'";
						else
							$output .= " style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999'";
						if($change_group_from==$i)
						{
							$show_colval = $result_read['field'.$change_group_to];
						}
						else
						{
							$show_colval = $result_read['field'.$i];
						}
						$output .= " align='".$salign[$i]."'";
						$ex_colval = $show_colval;
						if ($smonetary[$i] == "1") {
							$ex_colval = display_money($show_colval, $location_read, "");
							$show_colval = display_money($show_colval, $location_read);
							//$output .= " align='right'";
						}
						if (is_numeric($show_colval)) {
							//$output .= " align='center'";
						}
						$output .= ">";
						if (($show_colval=="") || ((($stitles[$i]=="order_id") || strtolower($stitles[$i])=="order id") && (($result_read['field'.$i] == "") || ($result_read['field'.$i] == "0")))) $output .= "&nbsp;"; else $output .= $show_colval;
						$export_output .= $ex_colval . $export_col;
						
						$output .= "</td>";
						if($ssumop[$i]=="1" || $ssumop[$i]=="2")
						{
							if(strpos($result_read['field'.$i],"$")!==false)
								$ssumop[$i] = "2";
							$ssum[$i] = $ssum[$i] * 1 + (str_replace("$","",str_replace(",","",$result_read['field'.$i])) * 1);
						}
						if($sgraph[$i]=="1")
						{
							$graph_vals[] = (str_replace("$","",str_replace(",","",$result_read['field'.$i])) * 1);
						}
					}
				}
				
				if(trim($extra_functions)!="" && $show_extra_functions)
				{
					//extra_functions
					require_once(dirname(__FILE__) . "/report_extra_functions.php");
					
					$extra_function_list = explode(",",$extra_functions);
					for($x=0; $x<count($extra_function_list); $x++)
					{
						$function_name = trim($extra_function_list[$x]);
						
						$output .= "<td style='padding-left:12px; padding-right:12px; border-bottom:solid 1px #999999' align='center'>";
						$output .= run_extra_report_function($function_name,$result_read,$stitles);
						$output .= "</td>";
					}
				}
				
				if($change_group_id!="")
				{
					if($rep_type!="SubReport")
					{
						for($i=0; $i<count($sbranches); $i++)
						{
							$output .= "<td><a href='$reporturl&setdate=$setdate&stime=$stime&day_duration=$day_duration&branch=".$sbranches[$i][2]."&branch_title=".urlencode($result_read['field'.$change_group_id])."&branch_col=field".$change_group_id."'>(" . $sbranches[$i][3] . ")</a></td>";
						}
					}
				}
				$output .= "</tr>";
			}
			$export_output = substr($export_output, 0, -1);
			$export_totals = "";
			for($i=0; $i<$scount; $i++)
			{
				if($sdisplay[$i])
				{
					if($export_totals != "")
					{
						$export_totals .= $export_col;
					}
					$output .= "<td";
					if ($smonetary[$i] == "1" && $ssumop[$i]=="1") {
						$output .= " align='".$salign[$i]."'><b>".display_money($ssum[$i], $location_read)."</b>";
						
						$temp = display_money($ssum[$i], $location_read);
						if($export_ext == "csv" && strstr($temp, ","))
							$export_totals .= '"' . $temp . '"';
						else
							$export_totals .= $temp;
						
					}
					else if($ssumop[$i]=="1")
					{
						$output .= " align='".$salign[$i]."'><b>".$ssum[$i]."</b>";
						$export_totals .= $ssum[$i];
						
						$temp = $ssum[$i];
						if($export_ext == "csv" && strstr($temp, ","))
							$export_totals .= '"' . $temp . '"';
						else
							$export_totals .= $temp;
					}
					else if($ssumop[$i]=="2")
					{
						$output .= " align='".$salign[$i]."'><b>$".number_format($ssum[$i],2,".",",")."</b>";
						
						$temp = number_format($ssum[$i],2,".",",");
						if($export_ext == "csv" && strstr($temp, ","))
							$export_totals .= '"' . $temp . '"';
						else
							$export_totals .= $temp;
					}
					else
					{
						$output .= ">&nbsp;";
						$export_totals .= " ";
					}
					$output .= "</td>";
				}
			}
			$export_output .= $export_row . $export_totals;
		}
		else
		{
			$output .= "<tr>";
			$output .= "<td colspan='$scount' align='center'>No Records found</td>";
			$output .= "</tr>";
		}
		$output .= "</table>";
		
		if($rep_type!="SubReport")
		{
			if(count($graph_vals) > 0)
			{
				$graph_val_str = "";
				for($i=0; $i<count($graph_vals); $i++)
				{
					if($graph_val_str!="") $graph_val_str .= ",";
					$graph_val_str .= $graph_vals[$i];
				}
				$output .= "<img src='resources/show_graph.php?width=480&height=360&data=".$graph_val_str."'>";
			}
			
			if($showing_multi_report) $mrcode = "&multireport=1";
			else $mrcode = "";
			
			//mail("ck@poslavu.com","reportid",$reportid);
			$output .= "<br><br><a href='index.php?reportid=$reportid&setdate=$setdate".$mrcode."&day_duration=$day_duration&stime=$stime&export_type=txt&widget=reports/export";
			if(isset($_GET['order_by_col'])) $output .= "&order_by_col=".$_GET['order_by_col'];		
			$output .= "'>Export To Tab Delimited \".txt\" file</a>";

			$output .= "<br><br><a href='index.php?reportid=$reportid&setdate=$setdate".$mrcode."&day_duration=$day_duration&stime=$stime&export_type=xls&widget=reports/export";
			if(isset($_GET['order_by_col'])) $output .= "&order_by_col=".$_GET['order_by_col'];	
			$output .= "'>Export To Tab Delimited \".xls\" file</a>";

			$output .= "<br><br><a href='index.php?reportid=$reportid&setdate=$setdate".$mrcode."&day_duration=$day_duration&stime=$stime&export_type=csv&widget=reports/export";
			if(isset($_GET['order_by_col'])) $output .= "&order_by_col=".$_GET['order_by_col'];		
			$output .= "'>Export To Comma Delimited \".csv\" file</a>";		
			$output .= "<br><br><br><br>";
		}
				
		if($export)
		{
			/*for($i=0; $i<strlen($export_output); $i++)
			{
			}*/
			//$mail_output = $export_output;
			//$mail_output = str_replace("\t","[tab]",$mail_output);
			//$mail_output = str_replace("\n","[newline]",$mail_output);
			//mail("corey@poslavu.com","xls tmp",$mail_output,"From: info@poslavu.com");
			export_report("report_".date("Y-m-d H:i").".".$export_ext,$export_output);
		}
		else
			echo "<br>".$output;
	}
?>