<?php
	if(can_access("customers")) 
	{
		ini_set("display_errors","0");
		require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
		$rmode = (isset($_GET['rmode']))?$_GET['rmode']:"recent-sales";
		if($infonav_section=="finance")
		{
			$baseurl = "?mode=intrep";
			$basearea = "intrep";
			$rmode_list = array("recent-sales"=>"Recent Sales","revenue"=>"Revenue Total","reseller-activity"=>"Reseller Activity","customer-counts"=>"Customer Counts","gateway-usage"=>"Gateway Usage","past-due"=>"Past Due","lead-status"=>"Lead Status","new-customers"=>"New Customers","booked-revenue"=>"Booked Revenue","mixed_versions"=>"Mixed Versions");
		}
		else if($infonav_section=="marketing")
		{
			$baseurl = "?mode=mtools";
			$basearea = "mtools";
			$rmode_list = array("recent-sales"=>"Recent Sales","gateway-usage"=>"Gateway Usage","lead-status"=>"Lead Status","new-customers"=>"New Customers","mixed_versions"=>"Mixed Versions");
		}
		else
		{
			exit();
		}
		
		echo "<table cellpadding=12><tr><td>"; // start encasing page table		
		echo "<table cellspacing=0 cellpadding=6>";
		echo "<tr>";
		foreach($rmode_list as $rkey => $rval)
		{
			$modestr = $rkey;
			if($modestr==$rmode)
			{
				$tdcode = "bgcolor='#eeeeee' style='border:solid 1px black; color:#222222; font-size:10px'";
			}
			else
			{
				$tdcode = "bgcolor='#cccccc' style='border:solid 1px #aaaaaa; color:#555555; font-size:10px; cursor:pointer' onclick='window.location = \"/sa_cp/index.php?mode=$basearea&rmode=$modestr\"'";
			}
			echo "<td $tdcode>&nbsp;&nbsp;&nbsp;&nbsp;$rval&nbsp;&nbsp;&nbsp;&nbsp;</td>";
		}
		echo "</tr>";
		echo "</table><br>";
		
		function create_submode_nav($navarr,$rmode)
		{
			global $basearea;
			$rsubmode = (isset($_GET['rsubmode']))?$_GET['rsubmode']:$navarr[0];
			echo "<table cellspacing=0 cellpadding=6>";
			echo "<tr>";
			echo "<td width='40'>&nbsp;</td>";
			for($i=0; $i<count($navarr); $i++)
			{
				$navlink = $navarr[$i];
				$navtitle = ucwords(str_replace("-"," ",$navlink));
				
				if($navlink==$rsubmode)
				{
					$tdcode = "bgcolor='#eeffee' style='border:solid 1px black; color:#222222; font-size:10px'";
				}
				else
				{
					$tdcode = "bgcolor='#ccddcc' style='border:solid 1px #aaaaaa; color:#555555; font-size:10px; cursor:pointer' onclick='window.location = \"/sa_cp/index.php?mode=$basearea&rmode=$rmode&rsubmode=$navlink\"'";
				}
				echo "<td $tdcode>&nbsp;&nbsp;&nbsp;&nbsp;$navtitle&nbsp;&nbsp;&nbsp;&nbsp;</td>";			
			}
			echo "</tr>";
			echo "</table><br>";
			return $rsubmode;
		}
		
		$rmode_filename = dirname(__FILE__) . "/coninfo_areas/".str_replace("..","",$rmode).".php";
		if(is_file($rmode_filename))
		{
			require_once($rmode_filename);
		}
		echo "</td></tr></table>"; // close out encasing page table
	}
?>
