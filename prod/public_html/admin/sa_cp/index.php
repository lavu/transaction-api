<?php
	require_once(__DIR__."/../cp/resources/session_functions.php");
	standarizeSessionDomain();
	session_start();
?>
<!DOCTYPE html>
<html lang="en">

  <head>
	<title>Lavu Employee Control Panel</title>
	<style type="text/css">
		body {
			margin:0;
			padding:0;
			font-family:Verdana,Arial;
			font-size:12px;

		}

	</style>
	<!-- <link href="assets/styles/bootstrap.min.css" rel="stylesheet"> -->
	<script type="text/javascript" src="assets/scripts/sacpjs.js"></script>

  </head>
  <body>

<?php
	if(isset($_GET['mode']) && $_GET['mode']=="create_account")
	{
	}
	else
	{
		require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
		$maindb = "poslavu_MAIN_db";
	}

	if($_SERVER['REMOTE_ADDR'] == '69.41.14.215'){
		error_log("ERROR STRANGE ACCESS ".print_r($_REQUEST,1));
		return;
	}
	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posadmin_fullname']))?$_SESSION['posadmin_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posadmin_email']))?$_SESSION['posadmin_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;
	if($loggedin_access=="") $loggedin_access = "";
	$loggedin_lavu_admin = (isset($_SESSION['posadmin_lavu_admin']))?$_SESSION['posadmin_lavu_admin']:0;
	//exit();
	if( ($_SESSION['posadmin_loggedin'] == 'nate') && ('0401' == date('md')) ){
		echo '';
	}else if( (!empty($_SESSION['posadmin_loggedin'])) && ('0401' == date('md')) ){
		echo "<script>document.getElementsByTagName('body')[0].style.webkitFilter = 'sepia(0.8)';</script>";
	}

	require_once(dirname(__FILE__)."/../manage/can_access.php");

	function account_loggedin()
	{
		global $loggedin;
		return $loggedin;
	}

	function get_tag_value($tag,$str)
	{
		$str_parts = explode("<".$tag.">",$str);
		if(count($str_parts) > 1)
		{
			$parts = explode("</".$tag.">",$str_parts[1]);
			if(count($parts) > 1)
			{
				return $parts[0];
			}
			else
			{
				$parts = explode("</",$str_parts[1]);
				return $parts[0];
			}
		}
		else return false;
	}

	function get_temp_username($try_username)
	{
		$start_try = $try_username;
		$use_username = "";
		$old_dataname = "";
		$user_exists = false;
		$trys = 0;
		while($use_username=="")
		{
			$act_query = mlavu_query("select `id`, `type`, `dataname` from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$try_username);
			if(mysqli_num_rows($act_query))
			{
				$act_read = mysqli_fetch_assoc($act_query);
				if($act_read['type']=="lavuadmin")
				{
					$use_username = $try_username;
					$old_dataname = $act_read['dataname'];
					$user_exists = $act_read['id'];
				}
				else
				{
					$trys++;
					$try_username = $start_try . $trys;
				}
			}
			else $use_username = $try_username;
		}
		$return_info = array();
		$return_info['use_username'] = $use_username;
		$return_info['user_exists'] = $user_exists;
		$return_info['old_dataname'] = $old_dataname;
		return $return_info;
	}

	function list_customers()
	{
		require_once(__DIR__."/billing/authnet_functions.php");
		$get_arb = false;
		$get_arb_ids = "all";

		if(isset($_GET['disable']))
		{
			mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `disabled`='1' where id='[1]'",$_GET['disable']);
		}
		else if(isset($_GET['enable']))
		{
			mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `disabled`='' where id='[1]'",$_GET['enable']);
		}
		else if(isset($_GET['extend_billing']) && isset($_GET['signupid']))
		{
			$add_days = $_GET['extend_billing'];
			$signupid = $_GET['signupid'];
			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where id='[1]'",$signupid);
			if(mysqli_num_rows($signup_query))
			{
				$signup_read = mysqli_fetch_assoc($signup_query);

				function add_days_to_date($sdate,$days)
				{
					$sdate_parts = explode("-",$sdate);
					if(count($sdate_parts) > 2)
					{
						return date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + $days,$sdate_parts[0]));
					}
					else return false;
				}

				$arb_license_id = $signup_read['arb_license_id'];
				$arb_hosting_id = $signup_read['arb_hosting_id'];
				$arb_license_newdate = add_days_to_date($signup_read['arb_license_start'],$add_days);
				$arb_hosting_newdate = add_days_to_date($signup_read['arb_hosting_start'],$add_days);

				$extend_arbs = array();
				$extend_arbs[] = array($arb_license_id,$arb_license_newdate,"arb_license_start");
				$extend_arbs[] = array($abr_hosting_id,$arb_hosting_newdate,"arb_hosting_start");

				for($i=0; $i<count($extend_arbs); $i++)
				{
					$arbid = $extend_arbs[$i][0];
					$edate = $extend_arbs[$i][1];
					$set_field = $extend_arbs[$i][2];

					$vars['refid'] = 16;
					$vars['subscribeid'] = $arbid;
					$vars['start_date'] = $edate;
					$extend_info = manage_recurring_billing("update_date");
					if($extend_info['success'])
					{
						mlavu_query("update `poslavu_MAIN_db`.`signups` set `$set_field`='[1]' where `id`='[2]'",$edate,$signupid);
					}
				}
			}
		}
		else if(isset($_GET['cancel_billing']))
		{
			$cancel_arbs = explode(",",$_GET['cancel_billing']);
			for($i=0; $i<count($cancel_arbs); $i++)
			{
				$arbid = $cancel_arbs[$i];

				$vars['refid'] = 16;
				$vars['subscribeid'] = $arbid;
				$cancel_info = manage_recurring_billing("delete",$vars);

				//echo $arbid . "<br>";
			}
			$get_arb = true;
			$get_arb_ids = $_GET['cancel_billing'];
		}
		else if(isset($_GET['get_arb']) && $_GET['get_arb']=="1")
		{
			$get_arb = true;
		}

		/*$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups`");
		while($signup_read = mysqli_fetch_assoc($signup_query))
		{
			$dataname = $signup_read['dataname'];
			if($dataname!="")
			{
				mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `signupid`='[1]' where `data_name`='[2]' and (`signupid`='' or `signupid`='0')",$signup_read['id'],$dataname);
			}
		}*/

		// log into customer accounts via super admin
		if(isset($_GET['cplogin']) && isset($_GET['ccode']) && isset($_GET['loginmode']))
		{
			$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]' and `company_code`='[2]'",$_GET['cplogin'],$_GET['ccode']);
			if(mysqli_num_rows($cust_query))
			{
				$cust_read = mysqli_fetch_assoc($cust_query);

				require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

				if($_GET['loginmode']=="create_temp")
				{
					global $loggedin;

					$username_info = get_temp_username($loggedin);
					$use_username = $username_info['use_username'];
					$user_exists = $username_info['user_exists'];
					$old_dataname = $username_info['old_dataname'];

					$vars = array();
					$vars['username'] = $use_username;
					$vars['password'] = rand(11111,99999);
					$vars['dataname'] = $_GET['ccode'];
					$vars['restaurant'] = $_GET['cplogin'];
					$vars['type'] = "lavuadmin";
					$vars['userid'] = $user_exists;

					$vars['f_name'] = "LavuAdmin";
					$vars['l_name'] = $use_username;
					$vars['email'] = "info@poslavu.com";
					$vars['access_level'] = 4;
					$vars['PIN'] = $vars['password'];
					$vars['quick_serve'] = 0;
					$hi = $_GET['ccode'];
					//mail("ck@poslavu.com", $hi,"");
					$new_db = "poslavu_" . $_GET['ccode'] . "_db";
					if($user_exists)
					{
						$old_db = "poslavu_" . $old_dataname . "_db";
						mlavu_query("delete from `$old_db`.`users` where `username`='[username]'",$vars);
						mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `username`='[username]' ,`dataname`='[dataname]',`restaurant`='[restaurant]',`type`='[type]' where `id`='[userid]'",$vars);
					}
					else
					{
						mlavu_query("insert into `poslavu_MAIN_db`.`customer_accounts` (`username`,`dataname`,`restaurant`,`type`) values ('[username]','[dataname]','[restaurant]','[type]')",$vars);
					}

					$use_pin = "";
					$try_pin = $vars['password'];
					while($use_pin=="")
					{
						$pin_query = mlavu_query("select `id` from `$new_db`.`users` where `PIN`='[1]'",$try_pin);
						if(mysqli_num_rows($pin_query))
						{
							$try_pin = rand(11111,99999);
						}
						else
						{
							$use_pin = $try_pin;
						}
					}
					$vars['password'] = $use_pin;
					$vars['PIN'] = $use_pin;

					$is_LLS = false;
					$get_loc_info = mlavu_query("SELECT `id`, `use_net_path`, `net_path` FROM `$new_db`.`locations` LIMIT 1");
					if (mysqli_num_rows($get_loc_info) > 0) {
						$location_info = mysqli_fetch_assoc($get_loc_info);
						if ((int)$location_info['use_net_path']>0 && !strstr($location_info['net_path'], "poslavu.com")) {
							$is_LLS = true;
						}
					}

					if ($is_LLS){
						$vars['_use_tunnel'] = '1';
						mlavu_query("insert into `$new_db`.`users` (`company_code`,`username`,`f_name`,`l_name`,`password`,`email`,`access_level`,`PIN`,`quick_serve`, `lavu_admin`, `active`, `_use_tunnel`) values ('[dataname]','[username]','[f_name]','[l_name]',PASSWORD('[password]'),'[email]','[access_level]','[PIN]','[quick_serve]','1', '1', '[_use_tunnel]')",$vars);
						if(!mlavu_insert_id()){
							mlavu_query("insert into `$new_db`.`users` (`company_code`,`username`,`f_name`,`l_name`,`password`,`email`,`access_level`,`PIN`,`quick_serve`, `lavu_admin`, `active`) values ('[dataname]','[username]','[f_name]','[l_name]',PASSWORD('[password]'),'[email]','[access_level]','[PIN]','[quick_serve]','1', '1')",$vars);
						}
						$exist_query = mlavu_query("select * from ".$new_db.".`config` where `setting`='[1]' and `location`='[2]' and `value`='[3]' and `value_long`='[4]'","table updated",$location_info['id'],"users","");
						if(mysqli_num_rows($exist_query)){
							;
						}
						else {
							mlavu_query("insert into ".$new_db.".`config` (`setting`,`location`,`value`,`value_long`,`type`) values('[1]','[2]','[3]','[4]','log')","table updated",$location_info['id'],"users","");
						}
					}
					else{
						mlavu_query("insert into `$new_db`.`users` (`company_code`,`username`,`f_name`,`l_name`,`password`,`email`,`access_level`,`PIN`,`quick_serve`, `lavu_admin`, `active`) values ('[dataname]','[username]','[f_name]','[l_name]',PASSWORD('[password]'),'[email]','[access_level]','[PIN]','[quick_serve]','1', '1')",$vars);
					}

					echo "Temp Account for ".$_GET['ccode'].":";
					echo "<br>username: " . $vars['username'];
					echo "<br>password & PIN: " . $vars['password'];
					echo "<script language='javascript'>";
					echo "window.resizeTo(360,320); ";
					echo "</script>";
				}
				else if($_GET['loginmode']=="cp")
				{
					$dataname = $cust_read['company_code'];
					$userid = 1000;
					$username = "poslavu_admin";
					$full_username = "POSLavu Admin";
					$email = "info@poslavu.com";
					$companyid = $cust_read['id'];
					admin_login($dataname, $userid ,$username, $full_username, $email, $companyid, "", "", "4");

					echo "<script language='javascript'>";
					echo "window.location.replace('/cp/index.php'); ";
					echo "</script>";
				}
				exit();
			}
		}

		// create customer listing tabs
		$submode = (isset($_GET['submode']))?$_GET['submode']:"signups";
		$tablist = array("Signups","All Enabled","Disabled Signups","All Disabled");

		if(isset($_GET['notabs']))
		{
		}
		else
		{
			echo "<table>";
			for($i=0; $i<count($tablist); $i++)
			{
				$set_submode = strtolower(str_replace(" ","_",$tablist[$i]));
				if($submode==$set_submode)
					$bgcolor = "#ffffff";
				else
					$bgcolor = "#dddde2";
				echo "<td style='padding:6px; border:solid 2px #777777; cursor:pointer' onclick='window.location = \"index.php?mode=customers&submode=".$set_submode."\"' bgcolor='$bgcolor'>".$tablist[$i]."</td>";
			}
			echo "</table>";
		}

		function poslavu_package_name($packageid)
		{
			if($packageid==1) return "Silver";
			else if($packageid==2) return "Gold";
			else if($packageid==3) return "Platinum";
			else if($packageid==24) return "Dev";
			else return $packageid;
		}

		function show_signup_row($signup_read,$get_arb=false,$get_arb_ids="all")
		{
			if(!is_array($signup_read) && is_numeric($signup_read))
			{
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `id`='[1]'",$signupid);
				if(mysqli_num_rows($signup_query))
				{
					$signup_read = mysqli_fetch_assoc($signup_query);
				}
			}

			if(is_array($signup_read) && !is_numeric($signup_read))
			{
				$arb_active_ids = array();
				//$signup_read = mysqli_fetch_assoc($signup_query);
				echo "<td style='padding-right:6px'>".$signup_read['company']."</td>";
				echo "<td style='padding-right:6px'>".poslavu_package_name($signup_read['package'])."</td>";
				echo "<td style='padding-right:6px'>".$signup_read['firstname']." ".$signup_read['lastname']."</td>";
				echo "<td style='padding-right:6px'>".$signup_read['email']."</td>";
				echo "<td style='padding-right:6px'>".$signup_read['phone']."</td>";

				if(isset($signup_read['arb_license_id']) && trim($signup_read['arb_license_id'])!="")
				{
					$sdate = $signup_read['arb_license_start'];
					$sdate_parts = explode("-",$sdate);
					if(count($sdate_parts) > 2)
					{
						echo "<td>";
						echo date("m/d/Y",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2],$sdate_parts[0]));
						echo "</td><td>";
						if(($get_arb && $get_arb_ids=="all") || ($get_arb && strpos($get_arb_ids,$signup_read['arb_license_id'])!==false) || ($get_arb && strpos($get_arb_ids,$signup_read['arb_hosting_id'])!==false) || $signup_read['arb_license_status']=="")
						{
							echo "reading:";
							$vars['refid'] = 16;
							$vars['subscribeid'] = $signup_read['arb_license_id'];
							$hosting_info = manage_recurring_billing("info",$vars);
							if($hosting_info['success'])
							{
								$hosting_status = get_tag_value("Status",$hosting_info['response']);
								mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_license_status`='[1]' where id='[2]'",$hosting_status,$signup_read['id']);
								if($hosting_status=="expired") $hosting_status = "charged";
								else if($hosting_status=="active") $arb_active_ids[] = $signup_read['arb_license_id'];
								echo " - " . $hosting_status;
							}
							else
								echo " - not found";
							echo "&nbsp;";
							echo "</td>";
							echo "<td>";

							if(isset($signup_read['arb_hosting_id']) && trim($signup_read['arb_hosting_id'])!="")
							{
								$vars['refid'] = 16;
								$vars['subscribeid'] = $signup_read['arb_hosting_id'];
								$hosting_info = manage_recurring_billing("info",$vars);
								if($hosting_info['success'])
								{
									$hosting_status = get_tag_value("Status",$hosting_info['response']);
									mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_hosting_status`='[1]' where id='[2]'",$hosting_status,$signup_read['id']);
									if($hosting_status=="expired") $hosting_status = "charged";
									else if($hosting_status=="active") $arb_active_ids[] = $signup_read['arb_hosting_id'];
									echo " - " . $hosting_status;
								}
								else
									echo " - not found";
							}
						}
						else
						{
							$hosting_status = $signup_read['arb_license_status'];
							if($hosting_status=="expired") $hosting_status = "charged";
							else if($hosting_status=="active") $arb_active_ids[] = $signup_read['arb_license_id'];
							echo " - " . $hosting_status;

							echo "&nbsp;";
							echo "</td>";
							echo "<td>";

							$hosting_status = $signup_read['arb_hosting_status'];
							if($hosting_status=="expired") $hosting_status = "charged";
							else if($hosting_status=="active") $arb_active_ids[] = $signup_read['arb_hosting_id'];
							echo " - " . $hosting_status;
						}
						echo "&nbsp;";
						echo "</td>";
					}
					else echo "<td colspan='3'>&nbsp;</td>";
				}
				else echo "<td colspan='3'>&nbsp;</td>";

				if(count($arb_active_ids) > 0)
				{
					$mode = (isset($_GET['mode']))?$_GET['mode']:"customers";
					$submode = (isset($_GET['submode']))?$_GET['submode']:"signups";
					echo "<td><a style='cursor:pointer; color:#880000' onclick='if(confirm(\"Cancel Billing for ".str_replace("\"","",str_replace("'","",$signup_read['company']))."? Ids: ".join(",",$arb_active_ids)."?\")) window.location = \"index.php?mode=$mode&submode=$submode&cancel_billing=".join(",",$arb_active_ids)."\"'>(cancel billing)</a></td>";
					if($signup_read['arb_license_status']=="active")
						echo "<td><a style='cursor:pointer; color:#008800' onclick='if(confirm(\"Extend Trial by 5 days?\")) window.location = \"index.php?mode=$mode&submode=$submode&extend_billing=5&signupid=".$signup_read['id']."\"'>(extend 5 days)</a></td>";
				}
				else echo "<td>&nbsp;</td>";
			}
			else echo "<td colspan='8'>&nbsp;</td>";
		}

		// find out if a temp account exists for this administrator
		global $loggedin;
		if($loggedin!="" && $loggedin)
		{
			$username_info = get_temp_username($loggedin);
			$use_username = $username_info['use_username'];
			$user_exists = $username_info['user_exists'];
			$old_dataname = $username_info['old_dataname'];

			if(isset($_GET['remove_temp_account']) && $_GET['remove_temp_account']=="1")
			{
				$old_db = "poslavu_".$old_dataname."_db";
				mlavu_query("delete from `$old_db`.`users` where `username`='[1]'",$use_username);
				mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `dataname`='' where `username`='[1]'",$use_username);
				$old_dataname = "";
			}

			if($old_dataname!="")
			{
				$mode = (isset($_GET['mode']))?$_GET['mode']:"customers";
				$submode = (isset($_GET['submode']))?$_GET['submode']:"";
				$old_db = "poslavu_".$old_dataname."_db";
				$pn_query = mlavu_query("select * from `$old_db`.`users` where `username`='[1]'",$use_username);
				if(mysqli_num_rows($pn_query))
				{
					$pn_read = mysqli_fetch_assoc($pn_query);
					$pn_pin = $pn_read['PIN'];
					echo "Current Temp Account: $old_dataname - $pn_pin <a href='index.php?mode=$mode&submode=$submode&remove_temp_account=1'>(remove)</a><br><br>";
				}
			}
		}

		echo "<table>";
		if($submode=="signups" || $submode=="disabled_signups")
		{
			if($submode=="disabled_signups")
				$disabled_filter = true;
			else
				$disabled_filter = false;

			function output_signup_row($signup_read,$submode,$disabled=false,$get_arb=false,$get_arb_ids="all")
			{
				$cust_query = mlavu_query("SELECT * from `poslavu_MAIN_db`.`restaurants` where `company_code`='[1]'",$signup_read['dataname']);
				if(mysqli_num_rows($cust_query))
				{
					$cust_read = mysqli_fetch_assoc($cust_query);
					if( ((!$disabled && $cust_read['disabled']!="1") || ($disabled && $cust_read['disabled']=="1")) && can_access('toggle_acct_enabled'))
					{
						echo "<tr>";
						show_signup_row($signup_read,$get_arb,$get_arb_ids);

						if($disabled)
						{
							$enable_title = "(enable)";
							$enable_action = "enable";
						}
						else
						{
							$enable_title = "(disable)";
							$enable_action = "disable";
						}
						echo "<td><a style='cursor:pointer; color:#008800' onclick='if(confirm(\"".ucfirst($enable_action)." ".str_replace("'","",$cust_read['company_name'])."?\")) window.location = \"index.php?mode=customers&submode=$submode&$enable_action=".$cust_read['id']."\"'>$enable_title</a></td>";
						//echo "<td><a style='cursor:pointer; color:#008800' onclick='if(confirm(\"Disable ".$cust_read['company_name']."?\")) window.location = \"index.php?mode=customers&disable=".$cust_read['id']."\"'>(disable)</a></td>";
						echo "</tr>";
					}
				}
			}

			echo "<tr><td colspan='8'><input type='button' value='Load ARB Info from Authorize.net' onclick='window.location = \"index.php?mode=customers&submode=$submode&get_arb=1\"'></td></tr>";

			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `arb_license_start`='' or `arb_hosting_start`=''");
			while($signup_read = mysqli_fetch_assoc($signup_query))
			{
				$sdate = $signup_read['date'];
				$sdate_parts = explode("-",$sdate);
				if(count($sdate_parts) > 2)
				{
					$license_start = date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + 14,$sdate_parts[0]));
					$hosting_start = date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] + 30,$sdate_parts[0]));

					mlavu_query("update `poslavu_MAIN_db`.`signups` set `arb_license_start`='[1]', `arb_hosting_start`='[2]' where `id`='[3]'",$license_start,$hosting_start,$signup_read['id']);
					//echo "applying: $license_start $hosting_start<br>";
				}
			}

			$sdate = date("Y-m-d");
			$sdate_parts = explode("-",$sdate);
			if(count($sdate_parts) > 2)
			{
				//$due_date = date("Y-m-d",mktime(0,0,0,$sdate_parts[1],$sdate_parts[2] - 14,$sdate_parts[0]));
				echo "<tr><td colspan='12' bgcolor='#888888' style='color:#ffffff; font-weight:bold' align='center'>New Signups</td></tr>";
				$signup_query = mlavu_query("SELECT * from `poslavu_MAIN_db`.`signups` where `arb_hosting_id`!='' and `arb_license_start`>'[1]' order by `date` asc",$sdate);
				while($signup_read = mysqli_fetch_assoc($signup_query))
				{
					output_signup_row($signup_read,$submode,$disabled_filter,$get_arb,$get_arb_ids);
				}

				echo "<tr><td colspan='12' bgcolor='#888888' style='color:#ffffff; font-weight:bold' align='center'>Existing Signups</td></tr>";
				$signup_query = mlavu_query("SELECT * from `poslavu_MAIN_db`.`signups` where `arb_hosting_id`!='' and `arb_license_start`<='[1]' order by `date` asc",$sdate);
				while($signup_read = mysqli_fetch_assoc($signup_query))
				{
					output_signup_row($signup_read,$submode,$disabled_filter,$get_arb,$get_arb_ids);
				}
			}
		}
		else if($submode=="all_enabled" || $submode=="all_disabled")
		{
			if($submode=="all_disabled")
			{
				$cond = "`disabled`=1";
				$enable_title = "(enable)";
				$enable_action = "enable";
			}
			else
			{
				$cond = "`disabled`!=1";
				$enable_title = "(disable)";
				$enable_action = "disable";
			}

			$cust_query = mlavu_query("SELECT * from `poslavu_MAIN_db`.`restaurants` where $cond ORDER BY `company_name` ASC");
			if(mysqli_num_rows($cust_query))
			{
				while($cust_read = mysqli_fetch_assoc($cust_query))
				{
					$dbname = "poslavu_".$cust_read['company_code']."_db";

					$user_query = mlavu_query("SELECT * from `$dbname`.`users` where `access_level`>=3");
					if(mysqli_num_rows($user_query))
					{
						echo "<tr>";
						echo "<td style='padding-right:6px'><a href='index.php?mode=customers&submode=$submode&loginmode=cp&cplogin=".$cust_read['id']."&ccode=".$cust_read['company_code']."' target='_blank'>(cp login)</a></td>";
						echo "<td style='padding-right:6px'><a href='index.php?mode=customers&submode=$submode&loginmode=create_temp&cplogin=".$cust_read['id']."&ccode=".$cust_read['company_code']."' target='_blank'>(create temp account)</a></td>";
						echo "<td style='padding-right:6px'>".$cust_read['company_name']."</td>";
						echo "<td style='padding-right:6px'>".$cust_read['company_code']."</td>";
						echo "<td style='padding-right:6px'>".$cust_read['email']."</td>";
						echo "<td style='padding-right:6px'>".$cust_read['phone']."</td>";

						//$signupid = $cust_read['signupid'];
						//show_signup_row($signupid);

						echo "<td><a style='cursor:pointer; color:#008800' onclick='if(confirm(\"".ucfirst($enable_action)." ".str_replace("'","",$cust_read['company_name'])."?\")) window.location = \"index.php?mode=customers&submode=$submode&$enable_action=".$cust_read['id']."\"'>$enable_title</a></td>";

						/*$is_demo = ($cust_read['demo']=="1");
						if($is_demo) echo "<td style='padding-right:6px'><a style='cursor:pointer' onclick='if(confirm(\"Are you sure you want to make this account live?\")) window.location = \"index.php?mode=customers&golive=".$cust_read['id']."&companycode=".$cust_read['company_code']."\"'><font color='#77aa77'>(go live)</font><a></td><td><font color='#ccaa88'>demo</font></td>";
						else echo "<td style='padding-right:6px'>&nbsp;</td><td><font color='#008800'><b>Live</b></font></td>";*/
						echo "</tr>";
					}
				}
			}
			else
			{
				echo "<tr><td>No Customers found</td></tr>";
			}
		}
		echo "</table>";
	}

	function list_customers2()
	{
		if(isset($_GET['golive'])&&isset($_GET['companycode']))
		{
			$company_code = $_GET['companycode'];
			$company_id = $_GET['golive'];
			$company_db = "poslavu_".$company_code."_db";

			$company_query = mlavu_query("SELECT * from `poslavu_MAIN_db`.`restaurants` where `company_code`='[1]' and `id`='[2]'",$company_code,$company_id);
			if(mysqli_num_rows($company_query))
			{
				$company_read = mysqli_fetch_assoc($company_query);
				if($company_read['demo']=="0")
					echo "<br><br><b>Site $company_code is already live</b><br><br>";
				else
				{
					$new_title = $company_read['company_name'];
					mlavu_query("delete from `[1]`.`locations` where `title`='Demo West Side'",$company_db);
					$address_query = "";//", `address`='', `city`='', `state`='', `zip`='', `phone`='', `website`=''";
					mlavu_query("update `[1]`.`locations` set `title`='[2]'".$address_query." where `title`='Demo East Side'",$company_db,$new_title);
					mlavu_query("update `[1]`.`users` set `access_level`='3' where `id`='1'",$company_db);
					mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` set `demo`='0', `licensed_for_locations`='1' where `company_code`='[1]' and `id`='[2]'",$company_code,$company_id);
					// copy the demo images from the DEMO_DEFAULT directories

					$directories = array();
					$directories[] = array("../images/".$company_code."/items","../images/DEMO_DEFAULT/items");
					$directories[] = array("../images/".$company_code."/items/full","../images/DEMO_DEFAULT/items/full");
					$directories[] = array("../images/".$company_code."/items/main","../images/DEMO_DEFAULT/items/main");
					$directories[] = array("../images/".$company_code."/categories","../images/DEMO_DEFAULT/categories");
					$directories[] = array("../images/".$company_code."/categories/full","../images/DEMO_DEFAULT/categories/full");
					$directories[] = array("../images/".$company_code."/categories/main","../images/DEMO_DEFAULT/categories/main");

					foreach ($directories as $dir) {
						$file_list = array();
						$dh = opendir($dir[1]) or die("Failed to open images directory...");
						while (!(($file = readdir($dh)) === false)) {
							$file_list[] = $file;
						}
						closedir($dh);
						foreach ($file_list as $file) {
							if (is_file($dir[1]."/".$file)) {
								copy($dir[1]."/".$file, $dir[0]."/".$file);
							}
						}
					}

					echo "<br><br><b><font color='#008800'>Site has gone live: " . $_GET['companycode'] . "</font></b><br><br>";
				}
			}
		}

		echo "<table>";
		$cust_query = mlavu_query("SELECT * from `poslavu_MAIN_db`.`restaurants` ORDER BY `company_name` ASC");
		if(mysqli_num_rows($cust_query))
		{
			while($cust_read = mysqli_fetch_assoc($cust_query))
			{
				echo "<tr>";
				echo "<td style='padding-right:6px'>".$cust_read['company_name']."</td>";
				echo "<td style='padding-right:6px'>".$cust_read['company_code']."</td>";
				$is_demo = ($cust_read['demo']=="1");
				if($is_demo) echo "<td style='padding-right:6px'><a style='cursor:pointer' onclick='if(confirm(\"Are you sure you want to make this account live?\")) window.location = \"index.php?mode=customers&golive=".$cust_read['id']."&companycode=".$cust_read['company_code']."\"'><font color='#77aa77'>(go live)</font><a></td><td><font color='#ccaa88'>demo</font></td>";
				else echo "<td style='padding-right:6px'>&nbsp;</td><td><font color='#008800'><b>Live</b></font></td>";
				echo "</tr>";
			}
		}
		else
		{
			echo "<tr><td>No Customers found</td></tr>";
		}
		echo "</table>";
	}

	function adhoc_repository()
	{
		$dr = opendir("/home/poslavu/adhoc");
		while(($file = readdir($dr))!==false)
		{
			if($file!="." && $file!="..")
			{
				echo "<br><br>".$file;
				echo "<br><textarea rows='4' cols='80'></textarea>";
			}
		}
		closedir($dr);
	}

	function get_database_info($dbname)
	{
		$table_list = array();
		$field_list = array();
		$table_count = 0;
		$field_count = 0;
		// creates array
		// dbinfo['tables'] = table_list
		// dbinfo['fields'] = field_list[tablename][0] = array of field names
		//                                         [1][fieldname] = associative array of field info

		$table_query = mlavu_query("SHOW tables from `$dbname`", null, null, null, null, null, null, false);
		while($table_read = mysqli_fetch_row($table_query))
		{
			$tablename = $table_read[0];
			$table_count++;

			$table_list[] = $tablename;
			$field_list[$tablename] = array(array(),array());
			$field_query = mlavu_query("SHOW columns from `[1]`.`[2]`", $dbname, $tablename, null, null, null, null, false);
			while($field_read = mysqli_fetch_row($field_query))
			{
				$fieldname = $field_read[0];
				$field_count++;

				$field_info = array();
				$field_info['name'] = $fieldname;
				$field_info['type'] = $field_read[1];
				$field_info['null'] = $field_read[2];
				$field_info['key'] = $field_read[3];
				$field_info['default'] = $field_read[4];
				$field_info['extra'] = $field_read[5];

				$field_list[$tablename][0][] = $fieldname;
				$field_list[$tablename][1][$fieldname] = $field_info;
			}
		}

		$dbinfo = array();
		$dbinfo['tables'] = $table_list;
		$dbinfo['fields'] = $field_list;
		$dbinfo['table_count'] = $table_count;
		$dbinfo['field_count'] = $field_count;

		return $dbinfo;
	}

	function sqlite_create_tables()
	{
		$dev_db_info = get_database_info('poslavu_DEV_db');
		$dev_tables = $dev_db_info['tables'];
		$dev_fields = $dev_db_info['fields'];

		$str = "";
		for($i=0; $i<count($dev_tables); $i++)
		{
			$tablename = $dev_tables[$i];
			$field_list = $dev_fields[$tablename][0];
			$field_info = $dev_fields[$tablename][1];

			// CREATE TABLE
			$tcode = "";
			for($n=0; $n<count($field_list); $n++)
			{
				$fieldname = $field_list[$n];
				$field = $field_info[$fieldname];
				$fieldtype = $field['type'];
				$fieldkey = $field['key'];
				if(trim($fieldkey)!="") $usekey = true; else $usekey = false;

				if(strpos($fieldtype,"int")!==false) $fieldtype = "INTEGER";
				else if(strtolower($fieldtype)=="datetime") $fieldtype = "VARCHAR(80)";
				else $fieldtype = strtoupper($fieldtype);

				if($tcode!="") $tcode .= ",\n";
				$tcode .= "`$fieldname` $fieldtype";
				if($usekey) $tcode .= " PRIMARY KEY ASC";
			}
			$tcode = "CREATE TABLE `$tablename` ($tcode);\n";
			$str .= $tcode;

			// POPULATE RECORDS
			$record_query = mlavu_query("select * from `poslavu_DEV_db`.`[1]`",$tablename);
			while($record_read = mysqli_fetch_assoc($record_query))
			{
				$insert_keys = "";
				$insert_vals = "";
				for($n=0; $n<count($field_list); $n++)
				{
					$fieldname = $field_list[$n];
					if($insert_keys!="") $insert_keys .= ",";
					if($insert_vals!="") $insert_vals .= ",";
					$insert_keys .= "`$fieldname`";
					$insert_vals .= "'".str_replace("'","''",$record_read[$fieldname])."'";
				}
				$rcode = "INSERT INTO `$tablename` ($insert_keys) values ($insert_vals);\n";
				$str .= $rcode;
			}
			$str .= "\n";
		}
		echo "<textarea rows='50' cols='100'>$str</textarea>";
	}

	function show_database_difference()
	{
		return sync_database_structure(false);
	}

	function sync_accounts()
	{
		$get_data_names = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants`");
		if (mysqli_num_rows($get_data_names) > 0)
		{
			while ($company_info = mysqli_fetch_array($get_data_names))
			{
				$db = "poslavu_".$company_info['data_name']."_db";
				$dataname = $company_info['data_name'];
				$restaurant = $company_info['id'];
				$user_query = mlavu_query("select * from `$db`.`users`");
				while($user_read = mysqli_fetch_assoc($user_query))
				{
					$username = $user_read['username'];

					$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$username);
					if(mysqli_num_rows($exist_query))
					{
						mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `dataname`='[1]', `restaurant`='[2]' where `username`='[3]'",$dataname,$restaurant,$username);
					}
					else
					{
						mlavu_query("insert into `poslavu_MAIN_db`.`customer_accounts` (`username`,`dataname`,`restaurant`) values ('[1]','[2]','[3]')",$username,$dataname,$restaurant);
					}
				}
			}
		}
		return "<br>syncing accounts...<br>";
	}

	function fix_groupids()
	{
		$str = "";
		$get_data_names = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants`");
		if (mysqli_num_rows($get_data_names) > 0)
		{
			while ($company_info = mysqli_fetch_array($get_data_names))
			{
				$db = "poslavu_".$company_info['data_name']."_db";
				$group_query = mlavu_query("update `$db`.`menu_categories` set `group_id`='1' where `group_id`='0'");
				if($group_query)
				{
					$str .= "<br>Fixed groupids for " . $db;
				}
			}
		}
		return $str;
	}

	function sa_create_new_password($length=12)
	{
		$str = "";
		for($i=0; $i<$length; $i++)
		{
			$n = rand(0,61);
			if($n < 10) // 0-9
			{
				$setn = 48 + $n;
			}
			else if($n<=35) // 10-35
			{
				$setn = 55 + $n;
			}
			else if($n<=61) // 36-61
			{
				$setn = 61 + $n;
			}
			$str .= chr($setn);
		}
		return $str;
	}

	function perform_op3()
	{
		$rdb = "poslavu_p2r_new_jersey_db";
		$sdate = "2011-01-04 00:00:00";
		$edate = "2011-01-04 24:00:00";

		echo "<table border='1' cellpadding=4>";
		$order_query = mlavu_query("SELECT * FROM `$rdb`.`orders` where `opened`>='[1]' and `opened`<='[2]'",$sdate,$edate);
		while($order_read = mysqli_fetch_assoc($order_query))
		{
			$contents_subtotal = 0;
			$contents_modify_price = 0;
			$contents_forced_price = 0;
			$contents_total = 0;
			$contents_query = mlavu_query("select * from `$rdb`.`order_contents` where `order_id`='[1]'",$order_read['order_id']);
			while($contents_read = mysqli_fetch_assoc($contents_query))
			{
				$contents_subtotal += $contents_read['subtotal'];
				$contents_modify_price += $contents_read['modify_price'] * $contents_read['quantity'];
				$contents_forced_price += $contents_read['forced_modifiers_price'] * $contents_read['quantity'];
				$contents_total = $contents_subtotal + $contents_modify_price + $contents_forced_price;
			}

			if(abs(($order_read['subtotal'] * 1) - ($contents_total * 1)) > 0.01)
				$bgcolor = "#ffaaaa";
			else
				$bgcolor = "aaffaa";

				echo "<tr>";
				echo "<td valign='top'>" . $order_read['id'] . "</td>";
				echo "<td valign='top' bgcolor='$bgcolor'>" . $order_read['subtotal'] . "</td>";
				echo "<td valign='top'>" . $contents_subtotal . "</td>";
				echo "<td valign='top'>" . $contents_modify_price . "</td>";
				echo "<td valign='top'>" . $contents_forced_price . "</td>";
				echo "<td valign='top' bgcolor='$bgcolor'>" . $contents_total . "</td>";
				echo "</tr>";
		}
		echo "</table>";
	}

	function perform_op()
	{
		$str = "";
		return;
		$get_data_names = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants`");
		if (mysqli_num_rows($get_data_names) > 0)
		{
			while ($company_info = mysqli_fetch_array($get_data_names))
			{
				$db = "poslavu_".$company_info['data_name']."_db";

				$success = mlavu_query("ALTER TABLE `$db`.`admin_action_log` CHANGE `id` `id` INT( 11 ) NOT NULL AUTO_INCREMENT");
				$str .= "set auto_increment for `$db`.`admin_action_log` : ";
				if($success)
					$str .= "<font color='green'>success</font>";
				else
					$str .= "<font color='red'>failed</font>";
				$str .= "<br>";
			}
		}

		return $str;

		if(isset($_POST['search']))
		{
			$str .= "searching for " . $_POST['search'] . ": <br><br>";
			$get_data_names = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants`");
			if (mysqli_num_rows($get_data_names) > 0)
			{
				while ($company_info = mysqli_fetch_array($get_data_names))
				{
					$db = "poslavu_".$company_info['data_name']."_db";

					$find_query = mlavu_query("select * from `$db`.`users` where `f_name` LIKE '%[1]%' or `l_name` LIKE '%[1]%' or `email` LIKE '%[1]%' or `username` LIKE '%[1]%'",$_POST['search']);
					while($find_read = mysqli_fetch_assoc($find_query))
					{
						$str .= "found in " . $db . ": ";
						$str .= "<b>" . $find_read['f_name'] . " " . $find_read['l_name'] . " " . $find_read['email'] . "</b>";
						$str .= "<br>";
					}
				}
			}
		}
		else
		{
			$str .= "<form name='search' method='post' action='index.php?mode=perform_op'>";
			$str .= "Search: <input type='text' name='search'>";
			$str .= "<input type='submit' value='Submit'>";
			$str .= "</form>";
		}

		return $str;
	}

	function create_jkeys_for_all()
	{
		//------------- old op stuff --------------//
		$str = "Checking/Creating Jkeys:<br><br>";
		return "JKey checker has checked out of the house so you better check yourself before you jek yourself.......... Boooooyah!!!!";

		require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

		$str .= "<table style='border:solid 1px black' cellpadding=4>";
		$rest_query = mlavu_query("select `id`,`data_name`, `jkey` as `jkey_plain`, AES_DECRYPT(`jkey`,'".integration_keystr2()."') as `jkey` from `poslavu_MAIN_db`.`restaurants`");
		while($rest_read = mysqli_fetch_assoc($rest_query))
		{
			$str .= "<tr style='border-bottom:solid 1px black'>";
			$str .= "<td>" . $rest_read['data_name'] . "</td>";
			$str .= "<td>" . $rest_read['jkey'] . "</td>";

			if($rest_read['jkey_plain']=="")
			{
				$set_password = sa_create_new_password(20);
				$success = mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `jkey`=AES_ENCRYPT('$set_password','".integration_keystr2()."') where id='".$rest_read['id']."'");
				if($success)
				{
					$str .= "<td><font color='green'>success</font> - " . $rest_read['data_name'] . " - " . $set_password . "</td>";
				}
				else
				{
					$str .= "<td><font color='red'>failed</font> - " . $rest_read['data_name'] . " - " . $set_password . "</td>";
				}
			}
			else $set_password = $rest_read['jkey'];

			$str .= "</tr>";
		}
		$str .= "</table>";
		return $str;
	}

	function create_data_access_for_all()
	{
		//------------- old op stuff --------------//
		$phname = "system_placeholder_68293154";
		$str = "Special Op:<br><br>";

		require_once("/home/poslavu/public_html/inc/usermgr.php");
		$str .= "<table style='border:solid 1px black' cellpadding=4>";
		$rest_query = mlavu_query("select `id`,`data_name`, `data_access` as `data_access_plain`, AES_DECRYPT(`data_access`,'lavupass') as `data_access` from `poslavu_MAIN_db`.`restaurants` where data_name=''");
		while($rest_read = mysqli_fetch_assoc($rest_query))
		{
			$str .= "<tr style='border-bottom:solid 1px black'>";
			$str .= "<td>" . $rest_read['data_name'] . "</td>";
			$str .= "<td>" . $rest_read['data_access'] . "</td>";

			if($rest_read['data_access_plain']=="")
			{
				$set_password = sa_create_new_password();
				$success = mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `data_access`=AES_ENCRYPT('$set_password','lavupass') where id='".$rest_read['id']."'");
				/*if($success)
				{
					$str .= "<br><font color='green'>success</font> - " . $rest_read['data_name'] . " - " . $set_pwd;
				}
				else
				{
					$str .= "<br><font color='red'>failed</font> - " . $rest_read['data_name'] . " - " . $set_pwd;
				}*/
			}
			else $set_password = $rest_read['data_access'];

			$set_username = "usr_pl_" . $rest_read['id'];
			$user_db = "poslavu_" . trim($rest_read['data_name']) . "_db";

			if(db_user_exists($set_username))
			{
				$str .= "<td>user " . $set_username . " exists</td>";
			}
			else
			{
				$str .= "<td>user " . $set_username . " does not exist</td>";

				$success = create_db_user($set_username,$set_password);
				if($success)
				{
					$str .= "<td>";
					$str .= "<font color='green'>success</font> - created " . $set_username;
					$gsuccess = grant_db_user($set_username,$user_db);
					if($gsuccess)
						$str .= " - <font color='green'>Granted</font> access to db";
					else
						$str .= " - <font color='red'>Failed</font> to grant access to db";
					$str .= "</td>";
				}
				else
				{
					$str .= "<td><font color='red'>failed</font> - could not create " . $set_username . "</td>";
				}
			}
			$str .= "</tr>";
		}
		$str .= "</table>";
	}

	function check_force_mod_groups()
	{
		$str = "Checking force mod groups....<br>";
		/*$get_data_names = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants`");
		if (mysqli_num_rows($get_data_names) > 0)
		{
			while ($company_info = mysqli_fetch_array($get_data_names))
			{
				if($company_info['data_name']=="coreys_mega_mosque")
				{
					$db = "poslavu_".$company_info['data_name']."_db";
					$mod_group_query = mlavu_query("select * from `$db`.`forced_modifier_groups` where `title`='$phname'");
					if(mysqli_num_rows($mod_group_query) < 1)
					{
						mlavu_query("insert into `$db`.`forced_modifier_groups` (`title`) values ('$phname')");
						$str .= "<br>Addded placeholder mod group for " . $db;
					}
				}
			}
		}*/
		return  $str;
	}

	function migrate_modifiers()
	{
		$modlists = array();

		$modlist_query = mlavu_query("select * from `poslavu_DEV_db`.`forced_modifier_lists` where _deleted!='1'");
		while($modlist_read = mysqli_fetch_assoc($modlist_query))
		{
			$modlist_id = $modlist_read['id'];
			$mod_query = mlavu_query("select * from `poslavu_DEV_db`.`forced_modifiers` where _deleted!='1' and `list_id`='[1]' order by id asc",$modlist_id);
			$newlist = "";
			while($mod_read = mysqli_fetch_assoc($mod_query))
			{
				$newlist .= "[".$mod_read['id']."]";
			}
			$modlists[] = $newlist;
		}

		for($i=0; $i<count($modlists); $i++)
		{
			echo "list $i - " . $modlists[$i] . "<br>";
		}

		$cat_query = mlavu_query("select * from `poslavu_DEV_db`.`menu_items` where `options1`!='' or `options2`!='' or `options3`!=''");
		while($cat_read = mysqli_fetch_assoc($cat_query))
		{
			echo "<br>options for " . $cat_read['name'];
			for($i=1; $i<=3; $i++)
			{
				$mlist_items = array();
				$mlist_titles = array();
				$option = $cat_read['options'.$i];
				$options = explode(",",$option);
				for($n=0; $n<count($options); $n++)
				{
					$opt = trim($options[$n]);
					if($opt!="")
					{
						$opt_query = mlavu_query("select * from `poslavu_DEV_db`.`forced_modifiers` where `title`='".str_replace("'","''",$opt)."'");
						if(mysqli_num_rows($opt_query))
						{
							$opt_read = mysqli_fetch_assoc($opt_query);
							$mlist_items[] = $opt_read['id'];
						}
						else $mlist_items[] = 0;
						$mlist_titles[] = $opt;
					}
				}
				if(count($mlist_items) > 0)
				{
					sort($mlist_items);
					$mlist_str = "";
					for($m=0; $m<count($mlist_items); $m++)
						$mlist_str .= "[".$mlist_items[$m]."]";
					echo "<br>option $i - " . $mlist_str;
					$list_found = false;
					for($m=0; $m<count($modlists); $m++)
					{
						if($modlists[$m]==$mlist_str) $list_found = true;
						//else echo "<br>NEQ: " . $modlists[$m] . " - " . $mlist_str;
					}
					if($list_found)
					{
					}
					else if(1==1)
					{
						echo "<br>not found";
					}
					else
					{
						$success = mlavu_query("insert into `poslavu_DEV_db`.`forced_modifier_lists` (`title`) values ('".str_replace("'","''","Mod List ".$cat_read['id'])."')");
						if($success)
						{
							$new_modlist = "";
							$use_listid = ConnectionHub::getConn('poslauv')->insertID();
							for($x=0; $x<count($mlist_titles); $x++)
							{
								$success = mlavu_query("insert into `poslavu_DEV_db`.`forced_modifiers` (`title`,`list_id`) values ('".str_replace("'","''",$mlist_titles[$x])."','$use_listid')");
								$new_modlist .= "[".ConnectionHub::getConn('poslavu')->insertID()."]";
							}
							echo "<br>created new modlist: " . $new_modlist;
							$modlists[] = $new_modlist;
						}
					}
				}
			}
			echo "<br>";
		}
	}

	function clear_logs()
	{
		$offset = (isset($_GET['offset']))?$_GET['offset']:0;
		$osize = 80;

		echo "Clearing Logs #" . $offset . " to " . ($offset + $osize);
		$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` limit ".$offset.",".$osize);
		if(mysqli_num_rows($company_query))
		{
			while($company_read = mysqli_fetch_assoc($company_query))
			{
				//clear_logs_for_dataname($company_read['data_name'],"/mnt/poslavu-files/archive/thu_8_logs/company/",0,0);
				clear_logs_for_dataname($company_read['data_name'],"/mnt/poslavu-files/logs/company/",14,90);
				clear_logs_for_dataname($company_read['data_name'],"/home/poslavu/logs/company/",1,8);
			}

			echo "<script language='javascript'>";
			echo "function next_logs() { ";
			echo "  window.location = 'index.php?mode=clear_logs&offset=".($offset + $osize)."'; ";
			echo "} ";
			echo "setTimeout('next_logs()', 2500); ";
			echo "</script>";
		}
		else echo "<br>Done!";
	}

	function clear_logs_for_dataname($company_dataname,$root_dir,$short_days,$long_days)
	{
		if($company_dataname!="")
		{
			$earliest_log_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - $short_days,date("Y")));
			$earliest_gateway_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - $long_days,date("Y")));
			echo "<br><br>Clearing Logs for <b><font color='#000088'>$company_dataname</font></b> Before <b>".$earliest_log_date."</b> (gateway before ".$earliest_gateway_date."):";
			echo "<div style='width:800px; height:100px; overflow:auto'>";
			$flist = array();
			//$cdir = "/home/poslavu/logs/company/".$company_dataname;
			$cdir = $root_dir . $company_dataname;
			echo "dir: $cdir<br>";
			if(is_dir($cdir))
			{
				$logdir_fp = opendir($cdir);
				while($read_logfile = readdir($logdir_fp))
				{
					$flist[] = $read_logfile;
				}

				sort($flist);
				for($i=0; $i<count($flist); $i++)
				{
					$sep = strrpos($flist[$i],"_");
					if($sep)
					{
						$ftype = substr($flist[$i],0,$sep);
						$frest = substr($flist[$i],$sep + 1);

						$dotpos = strpos($frest,".");
						if($dotpos) $fdate = substr($frest,0,$dotpos);
						else $fdate = $frest;

						$date_parts = explode("-",$fdate);
						if(count($date_parts) > 1)
						{
							$year = $date_parts[0];
							$month = $date_parts[1];
							if(count($date_parts) > 2)
								$day = $date_parts[2];
							else
								$day = 1;
						}
						$logdate = date("Y-m-d",mktime(0,0,0,$month,$day,$year));
						if($ftype=="gateway" && $logdate >= $earliest_gateway_date)
							$keep_log = true;
						else if($logdate >= $earliest_log_date)
							$keep_log = true;
						else if($ftype!="gateway" && $ftype!="json_log" && $ftype!="mysql" && $flist[$i]!="json_log" && $flist[$i]!="auto_correct" && $flist[$i]!="gateway_vars" && $ftype!="auto_correct")
							$keep_log = true;
						else
							$keep_log = false;
						if($keep_log) {$setcolor = "#008800";$phrase = "Keeping";}
						else {$setcolor = "#880000";$phrase = "Removing";}

						echo "<br><font color='$setcolor'>$phrase</font> - " . $ftype . ": " . $fdate;
						if(!$keep_log)
						{
							$log_fullpath = $cdir . "/" . $flist[$i];
							echo " <font color='#999999' style='font-size:10px'>" . $log_fullpath . "</font>";
							unlink($log_fullpath);
						}
					}
				}
			}
			echo "</div>";
		}
	}

	function convert_tables_to_innodb($company_database)
	{
		$str = "";
		$db_tables = array();
		$table_query = mlavu_query("SHOW table status from `[1]`",$company_database);
		while($table_read = mysqli_fetch_row($table_query))
		{
			$table_name = $table_read[0];
			$table_engine = $table_read[1];
			$db_tables[] = array("name"=>$table_name,"engine"=>$table_engine);
		}
		$str .= "Convert $company_database to InnoDB...<br><br>";

		for($i=0; $i<count($db_tables); $i++)
		{
			$table_name = $db_tables[$i]['name'];
			$table_engine = $db_tables[$i]['engine'];
			$str .= $table_name . " - " . $table_engine;

			if($table_engine=="MyISAM" && $company_database!="mysql")
			{
				$success = mlavu_query("alter table `[1]`.`[2]` ENGINE=INNODB",$company_database,$table_name);
				if($success) $str .= " <font color='#008800'><b>(Innodb transfer success)</b></font>";
				else $str .= " <font color='#880000'><b>(Innodb transfer failed)</b></font>";
			}
			$str .= "<br>";
		}
		return $str;
	}

	function setup_tables_for_indexing($rdb,$index_table_list)
	{
		$str = "";
		for($i=0; $i<count($index_table_list); $i++)
		{
			$tinfo = explode(":",$index_table_list[$i]);
			if(count($tinfo) > 1)
			{
				$tname = trim($tinfo[0]);
				$tfields = explode(",",$tinfo[1]);

				$str .= "<br>--------checking " . $rdb . " - " . $tname . "------";

				$tindexes = array();
				$ix_query = mlavu_query("show indexes from `[1]`.`[2]`",$rdb,$tname);
				while($ix_read = mysqli_fetch_assoc($ix_query))
				{
					$tcolname = $ix_read['Column_name'];
					$tindexes[$tcolname] = true;
					$str .= "<br>found $tcolname";
				}

				for($n=0; $n<count($tfields); $n++)
				{
					$tcolname = trim($tfields[$n]);
					if(!isset($tindexes[$tcolname]))
					{
						$str .= "<br>adding index for $tcolname";
						$success = mlavu_query("alter table `[1]`.`[2]` ADD INDEX(`[3]`)",$rdb,$tname,$tcolname);
						if ($success) {
							$str .= "&nbsp;<font color='green'>success</font>"; 
						} else {
							$str .= "&nbsp;<font color='red'>failed - ".mlavu_dberror()."</font>";
						}
					}
				}
			}
		}
		return $str;
	}

	function generateIndexList($use_dataname)
	{
		$itlist = array(
			"action_log: order_id,time,server_time,user_id,ialid,ioid",
			"callback_sessions: order_identifier",
			"cash_data: setting",
			"cc_transactions: order_id,datetime,internal_id,ioid,transaction_id,auth_code",
			"config: setting,type,location",
			"devices: UUID",
			"device_messages: ts,datetime,read,central_id",
			"forced_modifier_86: forced_modifierID,inventoryItemID,unitID",
			"ingredient_usage: orderid,itemid,ingredientid",
			"kds_bumped_items: bumped_ticket_id",
			"kds_bumped_tickets: loc_date_time,order_id",
			"med_action_log: customer_id,order_id,restaurant_time,device_time",
			"med_customers: f_name,l_name,field1,field2,field3",
			"menuitem_86: inventoryItemID,menuItemID,unitID",
			"modifier_86: inventoryItemID,modifierID,unitID",
			"modifiers_used: order_id,ioid",
			"orders: opened,closed,order_id,reopened_datetime,reclosed_datetime,ioid,active_device,last_modified,serial_no,last_mod_ts,pushed_ts",
			"order_contents: order_id,item_id,icid,device_time,ioid,server_time",
			"send_log: order_id,islid,ioid",
			"split_check_details: order_id,ioid"
		);

		if ($use_dataname=="pole_position_raceway" || substr($use_dataname, 0, 4)=="p2r_")
		{
			$itlist[] = "lk_customers: l_name,f_name,event_id,racer_name,last_activity";
		}

		return $itlist;
	}

	function optimize_indexes($upage=1,$udataname="")
	{
		$umode = (isset($_GET['umode']))?$_GET['umode']:"";

		$itlist = generateIndexList($udataname);

		$str = "";
		/*if($umode=="innodb")
			$limit_mult = 50;
		else
			$limit_mult = 5;*/
		$limit_mult = 5;
		$limit_str = " limit " . (($upage - 1) * $limit_mult) . ", " . ($limit_mult);

		$filter_version_3 = (isset($_GET['version_3']))?"`version_number` = '3' AND ":"";

		if($udataname!="")
		{
			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$udataname);
		}
		else
		{
			$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where ".$filter_version_3."`notes` NOT LIKE '%AUTO-DELETED%' AND `lavu_lite_server` = ''".$limit_str);
		}
		$records_returned = mysqli_num_rows($company_query);

		$count_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`restaurants` WHERE ".$filter_version_3."`notes` NOT LIKE '%AUTO-DELETED%' AND `lavu_lite_server` = ''");
		if(mysqli_num_rows($count_query))
		{
			$count_read = mysqli_fetch_assoc($count_query);
			$total_record_count = $count_read['count'];
		}
		else $total_record_count = "?";

		if(is_numeric($upage))
		{
			$str .= "<br>processing " . (($upage - 1) * $limit_mult) . " to " . ($upage * $limit_mult) . " out of " . $total_record_count . " records<br>";
		}

		$innodb_status_file = "/home/poslavu/private_html/innodb_status.txt";
		if($umode=="innodb")
		{
			$innodb_status_file = "/home/poslavu/private_html/innodb_status.txt";
			$fp = fopen($innodb_status_file,"r");
			$innodb_status_str = fread($fp,filesize($innodb_status_file));
			fclose($innodb_status_str);
		}

		while($company_read = mysqli_fetch_assoc($company_query))
		{
			$company_db_name = "poslavu_".$company_read['data_name']."_db";
			$str .= "<br><b>" . $company_read['data_name'] . "</b><br>";
			if($umode=="innodb")
			{
				$process_this_db = true;
				$company_mysql_log = "/home/poslavu/logs/company/".$company_read['data_name']."/mysql_".date("Y-m-d").".log";
				if(is_file($company_mysql_log))
				{
					$file_ts = filemtime($company_mysql_log);
					$minutes_ago = floor((time() - $file_ts) / 60);
					$str .= "Last mysql activity: " . date("m/d/Y h:i a",$file_ts) . " ($minutes_ago minutes ago)<br>";

					if($minutes_ago < 60) {$process_this_db = false;$str .= "<br>Skipping due to recent activity<br>";}
				}
				else
					$str .= "No mysql activity today<br>";
				if($process_this_db)
				{
					// $innodb_count;
					if(strpos($innodb_status_str,"|" . $company_read['data_name'] . "|")!==false)
					{
						$str .= "Already Processed";
					}
					else
					{
						$str .= convert_tables_to_innodb($company_db_name);

						$fp = fopen($innodb_status_file,"a");
						fwrite($fp,"|".$company_read['data_name']."|*".$company_read['id']."*");
						fclose($fp);
					}
				}
			}
			else
				$str .= setup_tables_for_indexing($company_db_name,$itlist);
		}

		if($records_returned * 1 > $limit_mult / 2)
		{
			$next_upage = ($upage * 1) + 1;
			if($next_upage > $upage)
			{
				echo "<script language='javascript'>";
				echo "function goto_next_upage() { ";
				echo "  window.location = 'index.php?mode=optimize_indexes&upage=$next_upage&umode=$umode'; ";
				echo "} ";
				echo "setTimeout(\"goto_next_upage()\",2000); ";
				echo "</script>";
			}
		}

		return $str;
	}

	function show_replication_status($rep_type="db2")
	{
		/*$str = "";
		$rep_query = mlavu_query("select * from `poslavu_MAIN_db`.`updatelog` where `type`='rep db1'");
		if(mysqli_num_rows($rep_query))
		{
			$rep_read = mysqli_fetch_assoc($rep_query);
			$rep_parts = explode("\n",$rep_read['details']);
			$rep_str = $rep_parts[0];
			if($rep_str=="Replication Active")
			{
				$str .= "<font color='#00aa00'><b>$rep_str</b></font>";
			}
			else
			{
				$str .= "<font color='#aa0000'><b>$rep_str</b></font>";
			}
		}*/
		if($rep_type=="db2")
			$rep_type = "db1";
		else if($rep_type=="db3")
			$rep_type = "db3 db1";

		$str = "";
		$rep_query = mlavu_query("select * from `poslavu_MAIN_db`.`updatelog` where `type`='repstatus [1]'",$rep_type);
		if(mysqli_num_rows($rep_query))
		{
			$rep_read = mysqli_fetch_assoc($rep_query);
			$rep_parts = explode("Seconds_Behind_Master=",$rep_read['details']);
			if(count($rep_parts) > 1) { $rep_parts = explode("\n",$rep_parts[1]); $seconds_behind = trim($rep_parts[0]); } else $seconds_behind = "?";

			if(is_numeric($seconds_behind) && $seconds_behind < 1)
			{
				$str .= "<font color='#00aa00'><b>Replication Caught Up</b></font>";
			}
			else
			{
				if(is_numeric($seconds_behind))
				{
					$seconds = $seconds_behind % 60;
					$minutes = ($seconds_behind - $seconds) / 60;
					$hours = ($minutes - ($minutes % 60)) / 60;
					$minutes = $minutes % 60;
					if($seconds < 10) $seconds = "0" . ($seconds * 1);
					if($minutes < 10) $minutes = "0" . ($minutes * 1);
					if($hours < 10) $hours = "0" . ($hours * 1);

					if($hours > 0)
						$show_seconds_behind = $hours . ":" . $minutes . ":" . $seconds . " hours";
					else if($minutes > 0)
						$show_seconds_behind = $minutes . ":" . $seconds . " minutes";
					else
					{
						$show_seconds_behind = $seconds . " second";
						if($seconds * 1  != 1) $show_seconds_behind .= "s";
					}
				}
				else $show_seconds_behind = $seconds_behind;
				$str .= "<font color='#aa0000'><b>$show_seconds_behind Behind</b></font>";
			}
		}
		return $str;
	}

	function companyDatabaseExists($company_db_name)
	{
		$exist_query = mlavu_query("SHOW DATABASES LIKE '".$company_db_name."'");
		if (!$exist_query)
		{
			error_log(__FUNCTION__." failed: ". mlavu_dberror());
			return false;
		}

		return (mysqli_num_rows($exist_query) > 0);
	}

	function sync_database_structure($make_changes=true, $component_package=false, $model_db="DEV", $dbt_file="core_tables.dbt", $upage=1)
	{
		//echo "<br><br>Temporarily disabled...";
		//exit();

		//echo "Sync database structure is unavailable during the weekend to minimize load.";
		//return;

		echo "model db: $model_db <br>component package: $component_package<br>dbt file: $dbt_file <br>make changes: ".($make_changes?"yes":"no")."<br>page: " . $upage . "<br><br>";
		$rep_status = show_replication_status("db2");
		echo "replication: " . $rep_status . "<br>";

		$start_ts = (isset($_GET['start_ts']))?$_GET['start_ts']:time();
		$total_pr = (isset($_GET['total_pr']))?$_GET['total_pr']:0;

		$dev_db_info = get_database_info('poslavu_'.$model_db.'_db');
		$dev_tables = $dev_db_info['tables'];
		$dev_fields = $dev_db_info['fields'];

		$limit_mult = 1;
		$limit_str = " ORDER BY `data_name` ASC limit " . (($upage - 1) * $limit_mult) . ", " . ($limit_mult);
		$fp = fopen($dbt_file,"r");
		$coredata = fread($fp, filesize($dbt_file));
		fclose($fp);

		$core_tables = array();
		$coredata = explode("}",$coredata);
		for($i=0; $i<count($coredata); $i++)
		{
			$corerow = explode("{",$coredata[$i]);
			$corerow = trim($corerow[0]);
			if($corerow!="")
			{
				$core_tables[] = $corerow;
			}
		}

		$log = "";
		if($make_changes)
		{
			if($component_package)
			{
				$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `data_name`!='[1]' and `notes` NOT LIKE '%AUTO-DELETED%' and `lavu_lite_server`=''".$limit_str,$model_db);
			}
			else
			{
				//echo "select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `notes` NOT LIKE '%AUTO-DELETED%' and `lavu_lite_server`=''".$limit_str;
				$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `notes` NOT LIKE '%AUTO-DELETED%' and `lavu_lite_server`=''".$limit_str);
			}
		}
		else
		{
			ini_set("display_errors","1");
			if($component_package)
			{
				$compfound = false;
				$find_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='[1]'".$limit_str,$model_db);
				while($find_read = mysqli_fetch_assoc($find_query))
				{
					if(!$compfound)
					{
						$company_db_name = "poslavu_".$find_read['data_name']."_db";
						//echo " " . $company_db_name . " " . $find_read['id'] . "++";
						$comp_query = mlavu_query("select * from `$company_db_name`.`locations` where `component_package`='".str_replace("'","''",$component_package)."'");
						if(mysqli_num_rows($comp_query))
						{
							$compfound = true;
							$check_company = true;
							$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' limit 1",$find_read['data_name']);
						}
					}
				}
			}
			else
				$company_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`!='[1]' limit 1",$model_db);
		}

		$count_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`restaurants` where `data_name`!='' and `notes` NOT LIKE '%AUTO-DELETED%' and `lavu_lite_server`=''");
		if(mysqli_num_rows($count_query))
		{
			$count_read = mysqli_fetch_assoc($count_query);
			$total_record_count = $count_read['count'];
		}
		else $total_record_count = "?";

		$records_returned = mysqli_num_rows($company_query);

		$start_record_number = (($upage - 1) * $limit_mult);
		$end_record_number = ($upage * $limit_mult);
		$records_left_count = $total_record_count - $end_record_number;
		echo "processing " . $start_record_number . " to " . $end_record_number . " out of " . $total_record_count . " records<br>";

		if($total_pr > 0)
		{
			$avg_time_per_site = number_format((time() - $start_ts) / $total_pr / $limit_mult,2,".","");
			$time_to_finish = number_format($avg_time_per_site * $records_left_count,0,"","");
			$ttf_seconds = $time_to_finish % 60;
			$ttf_hours_and_minutes = ($time_to_finish - $ttf_seconds) / 60;
			$ttf_minutes = $ttf_hours_and_minutes % 60;
			$ttf_hours = ($ttf_hours_and_minutes - $ttf_minutes) / 60;

			$show_time_to_finish = "";
			if($ttf_hours > 0)
			{
				$show_time_to_finish .= $ttf_hours . " hour";
				if($ttf_hours * 1 !=1 ) $show_time_to_finish .= "s";
				$show_time_to_finish .= " and " . $ttf_minutes . " minute";
				if($ttf_minutes * 1 != 1) $show_time_to_finish .= "s";
			}
			else
			{
				$show_time_to_finish .= $ttf_minutes . " minute";
				if($ttf_minutes * 1 != 1) $show_time_to_finish .= "s";
			}

			//$show_time_to_finish = $ttf_hours . ":" . $ttf_minutes . ":" . $ttf_seconds;

			echo "<br>Time per Site: " . $avg_time_per_site . " seconds<br>";
			echo "<br>Time to finish: " . $show_time_to_finish . " ($records_left_count sites left)<br>";
			//echo "<br>processed: $total_pr<br>elapsed: " . (time() - $start_ts) . "<br>";
		}

		$waiting_for_rep = true;
		if(strpos($rep_status,"Replication Caught Up")!==false)
		{
			$waiting_for_rep = false;
		}
		if(strpos($rep_status,"seconds")!==false)
		{
			$waiting_for_rep = false;
		}
		if(strpos($rep_status,"minutes")!==false)
		{
			echo $rep_status . "<br>";
			$nsplit = explode(" ",trim(strip_tags($rep_status)));
			if(count($nsplit) > 1)
			{
				$nsplit = explode(":",$nsplit[0]);
				$minsrep = trim($nsplit[0]);
				if(is_numeric($minsrep) && $minsrep * 1 < 2)
				{
					$waiting_for_rep = false;
				}
			}
		}

		//if(strpos($rep_status,"Replication Caught Up")===false && strpos($rep_status,"seconds")===false)
		if($waiting_for_rep)
		{
			echo "<br>Waiting for replication to catch up<br>";
			echo "<script language='javascript'>";
			echo "function goto_current_upage() { ";

			/*if(isset($_GET['comps']))
			{
				$from_db = $_GET['from'];
				$dbt_file = $_GET['dbt'];
				$comps = $_GET['comps'];
				echo "  window.location = 'index.php?mode=sync_database_structure&upage=$next_upage'; ";
			}*/
			$current_upage = (isset($_GET['upage']))?$_GET['upage']:1;
			echo "  window.location = 'index.php?mode=sync_database_structure&upage=$current_upage&start_ts=$start_ts&total_pr=$total_pr'; ";
			echo "} ";
			echo "setTimeout(\"goto_current_upage()\",20000); "; // replication status only updates once a minute so no need to refresh often
			echo "setTimeout(\"goto_current_upage()\",60000); ";
			echo "setTimeout(\"goto_current_upage()\",120000); ";
			echo "setTimeout(\"goto_current_upage()\",240000); ";
			echo "</script>";
			exit();
		}

		/*if(!isset($_GET['process_now']))
		{
			while($company_read = mysqli_fetch_assoc($company_query))
			{
				$company_db_name = "poslavu_".$company_read['data_name']."_db";
				echo $company_db_name . "<br>";
			}
			echo "<input type='button' value='continue >>' onclick='window.location = \"index.php?mode=sync_database_structure&upage=$upage&process_now=1\"' />";
			exit();
		}*/

		//$syncmode = "indexing";
		if($syncmode=="indexing")
		{
			while($company_read = mysqli_fetch_assoc($company_query))
			{
				echo optimize_indexes("dataname",$company_read['data_name']);
			}
		}
		else
		{
			while ($company_read = mysqli_fetch_assoc($company_query))
			{
				$company_db_name = "poslavu_".$company_read['data_name']."_db";
				if (!companyDatabaseExists($company_db_name))
				{
					echo "<br>no database found for ".$company_read['data_name']."<br>";
					$log .= "<br>Database does not exist: ".$company_db_name."<br>";
					continue;
				}

				$company_db_info = get_database_info($company_db_name);
				$company_tables = $company_db_info['tables'];
				$company_fields = $company_db_info['fields'];

				echo "<br>checking ".$company_read['data_name']."<br>";

				if (!$make_changes)
				{
					$check_company = true;
				}
				else
				{
					$check_company = false;
					if ($component_package)
					{
						$comp_query = mlavu_query("select * from `$company_db_name`.`locations` where `component_package`='".str_replace("'","''",$component_package)."'");
						if (mysqli_num_rows($comp_query))
						{
							$check_company = true;
						}
					}
					else
					{
						$check_company = true;
					}
				}

				if ($check_company)
				{
					for ($i=0; $i<count($dev_tables); $i++)
					{
						$tablename = $dev_tables[$i];
						$process_table = false;
						for($n=0; $n<count($core_tables); $n++)
						{
							if($core_tables[$n]==$tablename)
								$process_table = true;
						}

						if($process_table)
						{
							//echo "<br>" . $tablename;
							if(!isset($company_fields[$tablename])) // create new table
							{
								$fieldlist = $dev_fields[$tablename][0];
								$fields = $dev_fields[$tablename][1];

								//CREATE TABLE `poslavu_DEV_db`.`test_table` (
								//`t1` VARCHAR( 255 ) NOT NULL ,
								//`t2` INT NOT NULL ,
								//`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY
								//) ENGINE = MYISAM

								$tcode = "";
								$icode = "";
								for($n=0; $n<count($fieldlist); $n++)
								{
									$fieldname = $fieldlist[$n];
									$fieldtype = $fields[$fieldname]['type'];
									$fieldnull = $fields[$fieldname]['null'];
									$fieldkey = $fields[$fieldname]['key'];
									if(trim($fieldnull)!="") $notnull = true; else $notnull = false;
									if(trim($fieldkey)=="PRI") $usekey = true; else $usekey = false;
									if(trim($fieldkey)=="MUL") $mulkey = true; else $mulkey = false;

									if($tcode!="") $tcode .= ", ";
									$tcode .= "`$fieldname` $fieldtype";
									if($notnull) $tcode .= " NOT NULL";
									if($usekey) $tcode .= " AUTO_INCREMENT PRIMARY KEY";
									if($mulkey) $icode .= ", INDEX ($fieldname)";
								}
								$tcode .= $icode;
								$query = "CREATE TABLE `$company_db_name`.`$tablename` ($tcode) ENGINE = INNODB";
								if($make_changes)
								{
									$str = "<br><font color='blue'>Executing:</font> $query";
									$success = mlavu_query($query);
									if($success) $str .= " <b><font color='green'>(Success)</font></b>";
									else $str .= " <b><font color='red'>(Failed)</font></b>";
									$str .= "<br>";
									echo $str;
									$log .= $str;
								}
								else echo "<br><br>" . $query;
							}
							else
							{
								$fieldlist = $dev_fields[$tablename][0];
								$fields = $dev_fields[$tablename][1];

								for($n=0; $n<count($fieldlist); $n++)
								{
									$fieldname = $fieldlist[$n];
									$fieldtype = $fields[$fieldname]['type'];
									if(!isset($company_fields[$tablename][1][$fieldname])) // create new column
									{
										$query = "ALTER TABLE `$company_db_name`.`$tablename` ADD `$fieldname` $fieldtype NOT NULL";
										if($make_changes)
										{
											$str = "<br><font color='blue'>Executing:</font> $query";
											$success = mlavu_query($query);
											if($success) $str .= " <b><font color='green'>(Success)</font></b>";
											else $str .= " <b><font color='red'>(Failed)</font></b>";
											$str .= "<br>";
											echo $str;
											$log .= $str;
										}
										else echo "<br><br>" . $query;
									}
									else
									{
										// check column type
										$company_fieldtype = $company_fields[$tablename][1][$fieldname]['type'];
										if($fieldtype!=$company_fieldtype)
										{
											$query = "ALTER TABLE `$company_db_name`.`$tablename` CHANGE `$fieldname` `$fieldname` $fieldtype NOT NULL";

											if(strpos($fieldtype,"varchar(25)--term-to-ignore")!==false)
											{
												echo "<br><br><font color='#aaaaaa'>Ignoring: $query</font>";
											}
											else if($make_changes)
											{
												$str = "<br><font color='blue'>Executing:</font> $query";
												$success = mlavu_query($query);
												if($success) $str .= " <b><font color='green'>(Success)</font></b>";
												else $str .= " <b><font color='red'>(Failed)</font></b>";
												$str .= "<br>";
												echo $str;
												$log .= $str;
											}
											else echo "<br><br>" . $query;
										}
									}
								}
							}
						}
					}
				}
			}
			mlavu_query("insert into `poslavu_MAIN_db`.`updatelog` (`type`,`date`,`details`,`account`) values ('database sync page $upage','".date("Y-m-d H:i:s")."','".str_replace("'","''",$log)."','".str_replace("'","''",account_loggedin())."')");
		}

		if($records_returned * 1 > $limit_mult / 2)
		{
			$next_upage = ($upage * 1) + 1;
			if($next_upage > $upage)
			{
				echo "<script language='javascript'>";
				echo "function goto_next_upage() { ";

				if(isset($_GET['comps']))
				{
					$from_db = $_GET['from'];
					$dbt_file = $_GET['dbt'];
					$comps = $_GET['comps'];
					echo "  window.location = 'index.php?mode=sync_database_structure&upage=$next_upage&start_ts=$start_ts&total_pr=".($total_pr + 1)."'; ";
				}
				echo "  window.location = 'index.php?mode=sync_database_structure&upage=$next_upage&start_ts=$start_ts&total_pr=".($total_pr + 1)."'; ";
				echo "} ";
				echo "setTimeout(\"goto_next_upage()\",2000); ";
				echo "setTimeout(\"goto_next_upage()\",60000); ";
				echo "setTimeout(\"goto_next_upage()\",120000); ";
				echo "setTimeout(\"goto_next_upage()\",240000); ";
				echo "</script>";
			}
		}
	}

	if(isset($_GET['logout'])) // logout
	{
		$_SESSION['posadmin_loggedin'] = false;
		$loggedin = false;

		foreach($_SESSION as $key=>$val)
		{
			if(substr($key, 0, 8) == 'posadmin')
				unset($_SESSION[$key]);
		}
	}
	/*else if(isset($_POST['user']) && isset($_POST['pass']) && !$loggedin) // login
	{
		$account_query = mlavu_query("select * from `poslavu_MAIN_db`.`accounts` where `username`='".str_replace("'","''",$_POST['user'])."' and (`password`=PASSWORD('".str_replace("'","''",$_POST['pass'])."') or `password`=OLD_PASSWORD('".str_replace("'","''",$_POST['pass'])."'))");
		if(mysqli_num_rows($account_query))
		{
			$account_read = mysqli_fetch_assoc($account_query);
			$loggedin = $account_read['username'];
			$loggedin_fullname = trim($account_read['firstname'] . " " . $account_read['lastname']);
			$loggedin_email = $account_read['email'];
			$loggedin_access = $account_read['access'];
			$_SESSION['posadmin_loggedin'] = $loggedin;
			$_SESSION['posadmin_fullname'] = $loggedin_fullname;
			$_SESSION['posadmin_email'] = $loggedin_email;
			$_SESSION['posadmin_access'] = $loggedin_access;
		}
		else
		{
			echo "Invalid Credentials<br><br>";
		}
	}*/

	if($loggedin) // logged in
	{
		$mode = (isset($_GET['mode']))?$_GET['mode']:"";
		$submode = (isset($_GET['submode']))?$_GET['submode']:"";

		// top navbar
		echo "<table cellpadding='10' style='background:#222; color:#EEE; border-bottom:2px solid #444;' width='100%'><td align='left'>";
		echo "Logged In: " . $loggedin_fullname;
		echo "</td><td align='right'>";
		echo "<a href='index.php?logout=1' style='color:#EEE;'>Logout</a>";
		echo "</td></table>";

		echo "<br><a href='index.php'>Back to Main Menu</a><br><br>";

		if($mode=="customers")
		{
			//echo "<br><a href='index.php'>Back to Main Menu</a><br><br>";
			echo list_customers();
		}
		else if($mode=="manage_customers")
		{
			if(!isset($_GET['notabs']))
			{
			}
			require_once("manage_customers.php");
		}
		else if($mode=="menu_manage")
		{
			require_once("data_transfer.php");
		}
		else if($mode=="menu_restore")
		{
			require_once("tools/menu_restore.php");
		}
		else if ($mode == "client_search")
		{
			require_once("client_search.php");
		}
		else if($mode=="upcoming_charges")
		{
			require_once("upcoming_charges.php");
		}
		else if($mode=="retention_info")
		{
			require_once("retention_info.php");
		}
		else if($mode=="coninfo" || $mode=="intrep")
		{
			$infonav_section = "finance";
			require_once("coninfo.php");
		}
		else if($mode=="mtools")
		{
			$infonav_section = "marketing";
			require_once("coninfo.php");
		}
		else if($mode=="daily_signups")
		{
			require_once("daily_signups.php");
		}
		else if ($mode == "reseller_info")
		{

			require_once("reseller_info.php");
		}
		else if($mode == "distro_customers")
		{

			require_once("distro_customers.php");
		}
		else if ($mode=="test_ecreate")
		{
			require_once(dirname(__FILE__) . "/tools/es2_functions.php");

			$newfolder = "hotel_test2";
			$rsp_available = get_folder_availability_from_server($newfolder);

			if($rsp_available)
			{
				echo "Folder $newfolder is available<br><br>";

				$settings = array();
				$settings['Control Panel Product'] = "Hotel";
				$settings['Poslavu Api Key'] = "This is the Key";
				$settings['Poslavu Api Token'] = "A Token Here";
				$site_created = process_v2_create_site($newfolder,"eadmin","abc123","Creation Test Hotel 2","hotel",$settings);
				if($site_created)
				{
					echo "Site Created!<br>";
				}
				else
				{
					echo "failed to create site...<br>";
				}
			}
			else
			{
				echo "Folder $newfolder is NOT available";
			}
		}
		else if ($mode=="create_account")
		{
			ini_set("display_errors",1);
			require_once(dirname(__FILE__) . "/tools/es2_functions.php");

			function confirm_restaurant_shard($data_name)
			{
				$str = "";
				$letter = substr($data_name,0,1);
				if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
					$letter = $letter;
				else
					$letter = "OTHER";
				$tablename = "rest_" . $letter;

				$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
				if(mysqli_num_rows($mainrest_query))
				{
					$str .= $letter . ": " . $data_name . " exists";
				}
				else
				{
					$str .= $letter . ": " . $data_name . " inserting";

					$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);

						$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
						if($success) $str .= " <font color='green'>success</font>";
						else $str .= " <font color='red'>failed</font>";
					}
				}
				return $str;
			}

			$model_list = array();
			$model_list['Bar/Lounge'] = array("model"=>"defaultbar",	"title"=>"Bar/Lounge");
			$model_list['Coffee Shop'] = array("model"=>"default_coffee",	"title"=>"Coffee Shop");
			$model_list['Diner'] = array("model"=>"apple_default",	"title"=>"Diner");
			$model_list['Original Sushi'] = array("model"=>"sushi_town",	"title"=>"Original Sushi");
			$model_list['Pizza Shop'] = array("model"=>"default_pizza_",	"title"=>"Pizza Shop");
			$model_list['Restaurant'] = array("model"=>"default_restau",	"title"=>"Restaurant");

			if(can_access("technical"))
			{
				$model_list['Laser Tag'] = array("model"=>"ersdemo",	"title"=>"Laser Tag",	"product"=>"lasertag");
				$model_list['Lavu Give'] = array("model"=>"lavu_tithe",	"title"=>"Lavu Give",	"product"=>"tithe");
				$model_list['Lavu Hospitality'] = array("model"=>"hotel_lavu",	"title"=>"Lavu Hospitality",	"product"=>"hotel");
				$model_list['Lavu Kart (P2R)'] = array("model"=>"p2r_syracuse",	"title"=>"Lavu Kart (P2R)",	"product"=>"lavukart");
				$model_list['Lavu Fit'] = array("model"=>"lavu_fit_demo",	"title"=>"Lavu Fit",	"product"=>"fitness");
			}

			ksort($model_list);

			ini_set("dislay_errors","1");
			if(isset($_POST['company']) && isset($_POST['email']))
			{
				$company = $_POST['company'];
				$email = $_POST['email'];
				$phone = $_POST['phone'];
				$firstname = $_POST['firstname'];
				$lastname = $_POST['lastname'];
				$address = $_POST['address'];
				$city = $_POST['city'];
				$state = $_POST['state'];
				$zip = $_POST['zip'];
				$countryInfo = explode('_', $_POST['country']);
				$country = $countryInfo[0];
				$countryCode = $countryInfo[1];
				$default_menu = $_POST['default_menu'];
				$override_default_menu = (isset($_POST['override_default_menu']))?$_POST['override_default_menu']:"";
				$account_type = $_POST['account_type'];

				require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
				$maindb = "poslavu_MAIN_db";

				if($override_default_menu!="")
				{
					$exist_query = mlavu_query("select `data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `notes` NOT LIKE '%AUTO-DELETED%' and `disabled`!=1 and `disabled`!=2 and `disabled`!=3 and `lavu_lite_server`=''",$override_default_menu);
					if(mysqli_num_rows($exist_query))
					{
						$exist_read = mysqli_fetch_assoc($exist_query);
						$default_menu = $exist_read['data_name'];
					}
					else
					{
						echo "Dataname not found: $override_default_menu<br>";
						exit();
					}
				}

				$set_com_product = "";
				foreach($model_list as $mkey => $mval)
				{
					if(is_array($mval) && isset($mval['model']) && $mval['model']==$default_menu && isset($mval['product']))
					{
						$set_com_product = $mval['product'];
					}
				}
				$package = $_POST['product'];
				$terminals = $_POST['terminals'];
				$package_info = array();
				if (!empty($package))
				{
					$package_info['package'] = $package;
				}
				
				if (!empty($terminals))
				{
					$package_info['ipads'] = $terminals;
					$package_info['ipods'] = $terminals * 5;
				}

				require_once("/home/poslavu/public_html/register/create_account.php");
				$result = create_new_poslavu_account($company,$email,$phone,$firstname,$lastname,$address,$city,$state,$zip,$default_menu,"",$account_type,$set_com_product,$package_info,$countryCode);
				$success = $result[0];
				if($success)
				{
					$username = $result[1];
					$password = $result[2];
					$dataname = $result[3];
					$company_id = $result[4];

					echo "<br>username: $username";
					echo "<br>password: $password";
					echo "<br>dataname: $dataname";

					if($default_menu=="hotel_lavu" || $default_menu=="default_hotel" || $default_menu=="hotel_default" || $default_menu=="modern_mission")
					{
						if(strpos($default_menu,"hotel")!==false)
						{
							$base_ers_netpath = "lavuhotel.com";
						}
						else
						{
							$base_ers_netpath = "ershost.com";
						}

						$edataname = str_replace("_","-",$dataname);
						$edataname_valid = false;
						$rcount = 2;
						while(!$edataname_valid && $rcount < 20)
						{
							$rsp_available = get_folder_availability_from_server($edataname);
							if($rsp_available)
							{
								$edataname_valid = true;
							}
							else
							{
								$edataname = $dataname . $rcount;
							}
							$rcount++;
						}
						if($edataname_valid)
						{
							$create_token = e2_api_create_new_password(20);
							$create_key = e2_api_create_new_password(20);

							$new_rdb = "poslavu_".$dataname."_db";
							mlavu_query("update `[1]`.`locations` set `api_token`=AES_ENCRYPT('$create_token','lavutokenapikey')",$new_rdb);
							mlavu_query("update `[1]`.`locations` set `api_key`=AES_ENCRYPT('$create_key','lavuapikey')",$new_rdb);

							$settings = array();
							$settings['Control Panel Product'] = "Hotel";
							$settings['Poslavu Dataname'] = $dataname;
							$settings['Poslavu Api Key'] = $create_key;
							$settings['Poslavu Api Token'] = $create_token;
							$site_created = process_v2_create_site($edataname,$username,$password,$company,"hotel",$settings);
							if($site_created)
							{
								//echo "<br>Integrated Site Created!<br>";
								$ersnetpath = "https://" . $edataname . "." . $base_ers_netpath;
								$erswebpath = "http://" . $edataname . "." . $base_ers_netpath . "/";
								$erscppath = $erswebpath . "cp/";

								echo "<br>Website: <a href='$erswebpath' target='_blank'>$erswebpath</a><br>";
								echo "<br>CP: <a href='$erscppath' target='_blank'>$erscppath</a><br>";

								$emsg = "dataname: $dataname\nfoldername: $edataname\nuser: $username\n$password\n\ncompany: $company\nfirstname: $firstname\nlastname: $lastname\nemail: $email\nphone: $phone\ncity: $city\nstate: $state\nzip: $zip";
								mail("corey@lavu.com,ag@lavu.com","Lavu Hospitality Account Created",$emsg,"From: hospitality@lavu.com");

								$exist_query = mlavu_query("select * from `[1]`.`config` where `setting`='ers_netpath'",$new_rdb);
								if(mysqli_num_rows($exist_query))
								{
									mlavu_query("update `[1]`.`config` set `value`='[2]' where `setting`='ers_netpath'",$new_rdb,$ersnetpath);
								}
								else
								{
									mlavu_query("insert into `[1]`.`config` (`setting`,`location`,`value`,`type`) values ('[2]','[3]','[4]','[5]')",$new_rdb,"ers_netpath",1,$ersnetpath,"location_config_setting");
								}
								mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `dev`='1' where `data_name`='[1]' limit 1",$dataname);
							}
							else
							{
								echo "<br>failed to create integrated site...<br>";
							}
						}
						else
						{
							echo "<br>Error: Could not create Integrated Account";
							exit();
						}
					}

					$credvars['username'] = $username;
					$credvars['dataname'] = $dataname;
					$credvars['password'] = $password;
					$credvars['company'] = $company;
					$credvars['email'] = $email;
					$credvars['phone'] = $phone;
					$credvars['firstname'] = $firstname;
					$credvars['lastname'] = $lastname;
					$credvars['address'] = $address;
					$credvars['city'] = $city;
					$credvars['state'] = $state;
					$credvars['zip'] = $zip;
					$credvars['country'] = $country;
					$credvars['default_menu'] = $default_menu;

					mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`username`, `dataname`, `password`, `company`, `email`, `phone`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `status`, `default_menu`) values ('[username]', '[dataname]', AES_ENCRYPT('[password]', 'password'), '[company]', '[email]', '[phone]', '[firstname]', '[lastname]', '[address]', '[city]', '[state]', '[zip]', '[country]', 'manual', '[default_menu]')",$credvars);
					confirm_restaurant_shard($dataname);

					//LP-7274
					//mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`username`, `dataname`, `password`, `company`, `email`, `phone`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `status`, `default_menu`) values ('[username]', '[dataname]', AES_ENCRYPT('[password]', 'password'), '[company]', '[email]', '[phone]', '[firstname]', '[lastname]', '[address]', '[city]', '[state]', '[zip]', '[country]', 'manual', '[default_menu]')",$credvars);
					mlavu_query("INSERT INTO poslavu_".$dataname."_db.`config` ( `location`, `setting`, `type`, `value`, `value2` ) VALUES ( '[1]', 'business_country', 'location_config_setting', '[2]', '[3]')", $company_id, $country, $countryCode);
				}
				else echo "Failed to create account";
			}
			else
			{
				//../lib/create_demo_account.php?m=2' method='POST'>
				require_once(__DIR__.'/../cp/resources/chain_functions.php');
				$countryCodes = getCountryListOption();
				
				echo "<form action='index.php?mode=create_account' id='create_account_form' method='POST'>
					<table cellspacing='0' cellpadding='5'>
						<tr>
						<td align='right'>Package:</td>
						<td align='left'>
							<input type='radio' name='product' id='package_lavu' value='Lavu' checked><label for='package_lavu'>Lavu</label>
							<input type='radio' name='product' id='package_lavuse' value='LavuSE'><label for='package_lavuse'>SE</label>
						</td>
					</tr>
					<tr>
						<td align='right'>Terminals:</td>
						<td align='left'><input type='number' name='terminals' size='2' value='1'></td>
					</tr>
						<tr><td align='right'>Default Menu:</td><td align='left'><select name='default_menu'>";

							foreach ($model_list as $key => $val) {
								$model_dataname = $val['model'];
								$model_title = $val['title'];
								echo "<option value='$model_dataname'>$model_title</option>";
							}

				echo "		</select></td></tr>
						<tr>
							<td align='right'>Account Type:</td>
							<td align='left'>
								<input type='radio' name='account_type' value='Client' checked> Client
								<input type='radio' name='account_type' value='Reseller'> Reseller
								<input type='radio' name='account_type' value='Demo'> Demo
								<input type='radio' name='account_type' value='Test'> Test
								<input type='radio' name='account_type' value='Dev'> Dev
							</td>
						</tr>
						<tr>
							<td align='right'>Custom Type:</td>
							<td align='left'><input type='text' name='override_default_menu' value=''></td>
						</tr>
						<tr><td align='right'>Company Name:</td><td align='left'><input type='text' name='company' size='30' /></td></tr>
						<tr><td align='right'>Email:</td><td align='left'><input type='text' name='email' size='30' /></td></tr>
						<tr><td align='right'>Phone:</td><td align='left'><input type='text' name='phone' size='30' /></td></tr>
						<tr><td align='right'>First Name:</td><td align='left'><input type='text' name='firstname' size='30' /></td></tr>
						<tr><td align='right'>Last Name:</td><td align='left'><input type='text' name='lastname' size='30' /></td></tr>
						<tr><td align='right'>Address:</td><td align='left'><input type='text' name='address' size='30' /></td></tr>
						<tr><td align='right'>* Country:</td><td align='left'>
						<select id='country' name='country'><option value=''>Select Country</option>";
						echo $countryCodes;
						echo "</select>
						</td></tr>
						<tr><td align='right'>City:</td><td align='left'><input type='text' name='city' size='30' /></td></tr>
						<tr><td align='right'>State:</td><td align='left'><input type='text' name='state' size='30' /></td></tr>
						<tr><td align='right'>Zip:</td><td align='left'><input type='text' name='zip' size='30' /></td></tr>";
					echo "<tr><td align='center' colspan='2'><input type='submit' value='Create Account' /></td></tr>
					</table>
				</form>";
			}
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if ($mode=="account_created")
		{
			if ($_REQUEST['s'] == 1) {
				echo "New account successfully created...<br><br>Username: ".$_REQUEST['new_un']."<br>Password: ".$_REQUEST['new_pw'];
				mail($_SESSION['posadmin_email'], "New POSLavu Account: ".$_REQUEST['new_co'], "New account successfully created...\n\nUsername: ".$_REQUEST['new_un']."\nPassword: ".$_REQUEST['new_pw'], "From: info@poslavu.com");
			} else if ($_REQUEST['s'] == 1) {
				echo "An error occurred while trying to create new account...";
			}
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="clear_logs")
		{
			clear_logs();
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="innodb_count")
		{
			$innodb_status_file = "/home/poslavu/private_html/innodb_status.txt";
			$fp = fopen($innodb_status_file,"r");
			$innodb_status_str = fread($fp,filesize($innodb_status_file));
			fclose($innodb_status_str);

			//echo $innodb_status_str . "<br>";
			$innodb_count = 0;
			$non_count = 0;
			$non_list = "";
			$convert_now = "";

			$non_list .= "<table>";
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants`");
			while($rest_read = mysqli_fetch_assoc($rest_query))
			{
				if(strpos($innodb_status_str,"|" . $rest_read['data_name'] . "|")!==false)
					$innodb_count++;
				else
				{
					//convert_tables_to_innodb($rest_read['data_name']);
					//echo "cant find " . $rest_read['data_name'] . "<br>";
					$non_list .= "<tr><td align='right'>".$rest_read['company_name']."</td><td>".$rest_read['data_name']."</td><td>";

					$order_count = 0;
					$order_count_query = mlavu_query("select count(*) as `count` from `poslavu_[1]_db`.`orders`",$rest_read['data_name']);
					if(mysqli_num_rows($order_count_query))
					{
						$order_count_read = mysqli_fetch_assoc($order_count_query);
						$order_count = $order_count_read['count'];
						$non_list .= $order_count;
					}
					else $non_list .= "&nbsp;";
					$non_list .= "</td><td>";

					$company_mysql_log = "/home/poslavu/logs/company/".$rest_read['data_name']."/mysql_".date("Y-m-d").".log";
					if(is_file($company_mysql_log))
					{
						$file_ts = filemtime($company_mysql_log);
						$minutes_ago = floor((time() - $file_ts) / 60);
						$non_list .= "$minutes_ago minutes ago";

						if($minutes_ago < 30 && $order_count > 40000) {$non_list .= "</td><td>xxxxxx";}
						else if($convert_now=="")
						{
							$convert_now = "poslavu_" . $rest_read['data_name'] . "_db";
							$convert_now_dataname = $rest_read['data_name'];
							$convert_now_id = $rest_read['id'];
						}
					}
					else
					{
						$convert_now = "poslavu_" . $rest_read['data_name'] . "_db";
						$convert_now_dataname = $rest_read['data_name'];
						$convert_now_id = $rest_read['id'];
					}

					$non_list .= "</td></tr>";
					$non_count++;
				}
			}
			$non_list .= "</table>";

			echo "Innodb count: $innodb_count<br>";
			echo "Non count: $non_count<br>";
			echo "<br>" . $non_list;

			if($convert_now!="")
			{
				echo "<br>" . convert_tables_to_innodb($convert_now);

				$fp = fopen($innodb_status_file,"a");
				fwrite($fp,"|".$convert_now_dataname."|*".$convert_now_id."*");
				fclose($fp);

				echo "<script language='javascript'>";
				echo "function goto_next_upage() { ";
				echo "  window.location = 'index.php?mode=innodb_count'; ";
				echo "} ";
				echo "setTimeout(\"goto_next_upage()\",2000); ";
				echo "</script>";
			}
		}
		else if($mode=="optimize_indexes")
		{
			echo "<br><a href='index.php'> << Back to Main Menu</a><br><br>";
			if(isset($_GET['upage']))
				$set_upage = $_GET['upage'] * 1;
			else
				$set_upage = 1;

			echo optimize_indexes($set_upage);
		}
		else if($mode=="sync_database_structure" || $mode=="show_database_structure_difference")
		{
			echo "<br><a href='index.php'> << Back to Main Menu</a><br><br>";
			if($mode=="show_database_structure_difference")
				$make_changes = false;
			else
				$make_changes = true;

			if(isset($_GET['upage']))
				$set_upage = $_GET['upage'] * 1;
			else
				$set_upage = 1;
			if(isset($_GET['comps']) && isset($_GET['from']) && isset($_GET['dbt']))
			{
				$from_db = $_GET['from'];
				$dbt_file = $_GET['dbt'];
				$comps = $_GET['comps'];

				//disabled temporarily
				echo sync_database_structure($make_changes,$comps,$from_db,$dbt_file,$set_upage);
				//exit();
			}
			else
			{
				// $make_changes=true, $component_package=false, $model_db="DEV", $dbt_file="core_tables.dbt", $upage=1
				//set_time_limit(0);

				//disabled temporarily
				echo sync_database_structure($make_changes, false, "DEV", "core_tables.dbt",$set_upage);
				//exit();
			}
		}
		else if($mode=="sqlite_sync")
		{
			echo sqlite_create_tables();
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="adhoc_repository")
		{
			//echo adhoc_repository();

/*
			echo "<table cellpadding=6 style='border:solid 2px #888888' bgcolor='#eeeeee' width='100%'><td align='left'>";
			echo "Logged In: " . $loggedin_fullname;
			echo "</td><td align='right'>";
			echo "<a href='index.php?logout=1'>(logout)</a>";
			echo "</td></table>";

			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
*/
			echo "<br><br><br><a href='http:/webv1.poslavu.com/adhoc/POSLavu_20120215-1a.zip' style='text-decoration:none'>POSLavu Client v2.0.2 - February 15, 2012 - Build 'A' - #1</b>";
			echo "<br><br><br><a href='http:/webv1.poslavu.com/adhoc/POSLavu_20120208-1c.zip' style='text-decoration:none'>POSLavu Client v2.0.2 - February 8, 2012 - Build 'C' - #1</b>";
		}
		else if($mode=="sync_accounts")
		{
			echo sync_accounts();
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="migrate_modifiers")
		{
			echo migrate_modifiers();
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="perform_op")
		{
			echo perform_op();
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="create_jkeys")
		{
			echo create_jkeys_for_all();
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="fix_groupids")
		{
			echo fix_groupids();
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="lavukart_tables")
		{
			require_once("config_tables.php");
			$content .= config_tables("/home/poslavu/public_html/admin/cp/components/lavukart/tables.dbt","poslavu_DEVKART_db");
			echo $content;
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="create_reports")
		{
			echo "<table cellpadding=6 width='100%'><td width='100%' bgcolor='#dddddd' align='left' style='border:solid 1px #888888'><input type='button' onclick='window.location = \"index.php\"' style='font-size:10px' value='<< Back to Main Menu'></td></table><br><br>";
			require_once("create_reports.php");
		}
		else if($mode=="signup_history")
		{
			function parse_payment_response($response)
			{
				$vars = array();
				$response = explode("\n",$response);
				for($i=0; $i<count($response); $i++)
				{
					$line = explode("=",$response[$i]);
					if(count($line) > 1)
					{
						$vars[trim($line[0])] = trim($line[1]);
					}
				}
				return $vars;
			}

			echo "signup history: <br>";
			echo "<table>";
			$last_date = "";
			$details = "";
			$captures = array();
			$signup_count = 0;
			$capture_count = 0;
			$capture_names = array();

			$date_scounts = array();
			$date_sinfo = array();
			$ds_query = mlavu_query("select restaurants.created as created,company_name,data_name,distro_code,last_activity,status,left(`restaurants`.`created`,10) as `date_created` from `poslavu_MAIN_db`.`restaurants` left join `poslavu_MAIN_db`.`signups` on `restaurants`.`data_name`=`signups`.`dataname` order by created desc limit 5000");
			//echo "count: " . mysqli_num_rows($ds_query) . "<br>";
			while($ds_read = mysqli_fetch_assoc($ds_query))
			{
				//$set_count = $ds_read['count'];
				//if($set_count > 100) $set_count = 0;
				//$date_scounts[$ds_read['date_created']] = $set_count;
				//echo $ds_read['date_created'] . " = " . $ds_read['count'] . "<br>";
				$date_created = $ds_read['date_created'];
				if(!isset($date_scounts[$date_created])) $date_scounts[$date_created] = 1;
				else $date_scounts[$date_created]++;

				if(!isset($date_sinfo[$date_created])) $date_sinfo[$date_created] = array();
				$date_sinfo[$date_created][] = $ds_read;

				$date_created . "----" . $date_scounts[$date_created] . "<br>";
			}

			$pr_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where LEFT(`x_description`,14)!='POSLavu Auth R' order by `date` desc, `time` desc");
			while($pr_read = mysqli_fetch_assoc($pr_query))
			{
				$pay_info = parse_payment_response($pr_read['response']);
				if($pay_info['x_type']=="auth_capture")
				{
					$captures[] = array(trim($pay_info['x_first_name'] . " " . $pay_info['x_last_name']),$pr_read['date']);
				}
				else if($pay_info['x_type']=="auth_only")
				{
					if($last_date!=$pr_read['date'] && $last_date!="")
					{
						//echo "<br><b>" . $last_date . " - " . $signup_count . "</b>";//<br>".$details;
						echo "<tr><td>";
						if($last_date==date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 15,date("Y"))))
							echo "<b>" . $last_date . "</b>";
						else
							echo $last_date;
						echo "</td><td>";
						$date_id = $last_date;
						$date_id = str_replace("_","",$date_id);
						$date_id = str_replace("-","",$date_id);

						echo "<table>";
						for($i=0; $i<$signup_count; $i++)
						{
							$signup_captured = $capture_names[$i][1];
							$fullname = str_replace("\"","&quot;",$capture_names[$i][0]);
							$set_status = $capture_names[$i][3];

							if($signup_captured)//$capture_count > $i)
								$bgcolor = "#77dd77";
							else
								$bgcolor = "#bbbbcc";
							echo "<td width='20' height='12' bgcolor='$bgcolor' style='border:solid 1px #888899' title=\"$fullname\" alt=\"$fullname\" align='center' valign='middle'>";
							if($set_status!="")
								echo $set_status;
							else
								echo "&nbsp;";
							echo "</td>";
						}
						if(isset($date_scounts[$last_date]))
						{
							for($i=($signup_count - 1); $i<$date_scounts[$last_date]; $i++)
							{
								$fullname = "distro";
								$set_status = ".";
								$bgcolor = "#eeeeee";
								echo "<td width='20' height='12' bgcolor='$bgcolor' style='border:solid 1px #aabbaa' title=\"$fullname\" alt=\"$fullname\" align='center' valign='middle'>";
								if($set_status!="")
									echo $set_status;
								else
									echo "&nbsp;";
								echo "</td>";
							}
						}
						echo "<td><a style='cursor:pointer; color:#cccccc' onclick='document.getElementById(\"row_info_$date_id\").style.display = \"table-row\"'>(view details)</a></td>";
						echo "</table>";
						echo "</td></tr>";
						echo "<tr id='row_info_$date_id' style='display:none'><td colspan='20'>";

						echo "<table cellpadding=6 cellspacing=6><tr><td style='border:solid 1px #aaaaaa'>";
						echo "<div style='color:#777777; font-size:10px'>";
						for($n=0; $n<count($date_sinfo[$last_date]); $n++)
						{
							$rread = $date_sinfo[$last_date][$n];
							echo $rread['company_name'];
							if($rread['distro_code']=="" && $rread['status']=="manual") echo " - <font style='color:#888800'>Internal</font>";
							else if($rread['distro_code']=="") echo " - <font style='color:#008800'>Self Signup</font>";
							else echo " - <font style='color:#000088'>Distro: " . $rread['distro_code'] . "</font>";
							echo "<br>";
						}
						echo "</div>";
						echo "</tr></tr></table>";

						echo "</td></tr>";

						$signup_count = 0;
						$capture_count = 0;
						$capture_names = array();
						$details = "";
					}
					if($pay_info['x_response_code']=="1" && $pay_info['x_first_name']!="" && $pay_info['x_last_name']!="Fiala" && (strtolower($pay_info['x_last_name'])!="lim" || strtolower($pay_info['x_first_name'])!="andy") && $pay_info['x_amount'] * 1 > 2)
					{
						$fullname = trim($pay_info['x_first_name'] . " " . $pay_info['x_last_name']);
						$pay_date = $pr_read['date'];
						$pay_time = $pr_read['time'];

						$details .= $fullname . "<br>";
						$capture_found = false;
						for($n=0; $n<count($captures); $n++)
						{
							if($captures[$n][0]==$fullname)
							{
								$capture_found = true;
							}
						}

						$set_status = "";
						$cs_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `date`<='[1]' and `time`<='[2]' order by `date` desc, `time` desc limit 1",$pay_date,$pay_time);
						if(mysqli_num_rows($cs_query))
						{
							$cs_read = mysqli_fetch_assoc($cs_query);
							$rs_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `company_code`='[1]'",$cs_read['dataname']);
							if(mysqli_num_rows($rs_query))
							{
								$rs_read = mysqli_fetch_assoc($rs_query);
								if($rs_read['disabled']=="1")
									$set_status = "x";//$cs_read['firstname'];
								else
								{
									$last_act_parts = explode(" ",$rs_read['last_activity']);
									//$set_status = $last_act_parts[0];
									if($last_act_parts[0]==date("Y-m-d"))
										$set_status = "<font color='#009900'>c</font>";
									else
									{
										$last_act_date = explode("-",$last_act_parts[0]);
										$days_since_use = (mktime(0,0,0,date("m"),date("d"),date("Y")) - mktime(0,0,0,$last_act_date[1],$last_act_date[2],$last_act_date[0])) / 24 / 60 / 60;
										$days_since_use = floor($days_since_use);
										$set_status = "<font color='#88aa88'>$days_since_use</font>";
									}
								}
							}
							else $set_status = "<font color='#000088'>?</font>";
						}
						else $set_status = "<font color='#000088'>?</font>";

						if($capture_found)
						{
							$capture_names[] = array($fullname,true,$datetime,$set_status);
							$capture_count++;
						}
						else
							$capture_names[] = array($fullname,false,$datetime,$set_status);
						$signup_count++;
					}

					while($last_date > $pr_read['date'] && $last_date >= "2010-01-01" && $last_date <= "2020-01-01")
					{
						$date_split = explode("-",$last_date);
						$date_year = $date_split[0];
						$date_month = $date_split[1];
						$date_day = $date_split[2];

						$last_date = date("Y-m-d",mktime(0,0,0,$date_month,$date_day - 1,$date_year));
						if($last_date > $pr_read['date']) 
						{
							echo "<tr><td>";
							if($last_date==date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 15,date("Y"))))
								echo "<b>" . $last_date . "</b>";
							else
								echo $last_date;
							echo "</td><td>";
							$date_id = $last_date;
							$date_id = str_replace("_","",$date_id);
							$date_id = str_replace("-","",$date_id);

							echo "<table>";
							for($i=0; $i<$date_scounts[$last_date]; $i++)
							{
								$fullname = "?";
								$set_status = ".";
								$bgcolor = "#eeeeee";
								echo "<td width='20' height='12' bgcolor='$bgcolor' style='border:solid 1px #aabbaa' title=\"$fullname\" alt=\"$fullname\" align='center' valign='middle'>";
								if($set_status!="")
									echo $set_status;
								else
									echo "&nbsp;";
								echo "</td>";
							}
							echo "</table>";
							//echo "<tr><td>" . $last_date . "</td><td>&nbsp;</td></tr>";
						}
					}
					$last_date = $pr_read['date'];
				}
			}
			echo "</table>";
		}
		else if($mode=="view_customer_survey")//this actually just display one particular survey, yes it was dumb to set it up that way :(
		{
				$survey_id = "s_2";//
				$locationid = "145"; ///////////
				require_once(dirname(__FILE__)."/view_survey.php");
		}//else if
		else if($mode=="view_customer_survey_support")
		{
				//$survey_id = "s_2";//
				//$locationid = "145"; ///////////
				$survey_id = $_GET['which_survey'];//"s_3";//
				$locationid = "145"; ///////////
				require_once(dirname(__FILE__)."/display_survey.php");
		}//else if
		else if($mode=="show_survey_answers")
		{
				require_once(dirname(__FILE__)."/show_survey_answers.php");
		}//else if

		else if($mode=="create_customer_survey")
		{
				/* This else-if section written by Chris Kirby */
				require_once(dirname(__FILE__)."/survey.php");
				echo "<form onSubmit='return sureSubmit();' name='create_survey' method='post' action=''>";

				echo "
						<div class='main' id='main'>";
						echo "<textarea name='title' id='title' style='width:360px; height:50px;'>Title</textarea><br/><br/>";
						echo "<textarea name='instructions' id='instruction' style='width:360px; height:150px;'>Instructions</textarea><br/><br/>
						<div style='clear:both;'></div>
								<div class='toolbar'>
										<img src='images/btn_add_plus.png' onclick='addEmptyRow();' width='48' height='48'>
								</div><!--toolbar-->
								<div class='panel' id='panel'>
											<div class='row header'>
																<div class='rowElement re1'>Question</div>
																<div class='rowElement re2'>Input Type</div>
																<div class='rowElement re3'>Options</div>
																<div class='rowElement re4'>Deactivate</div>
											</div>
											<div class='clearRow'></div>

				";
				echo "
								</div><!--panel-->
				";
				echo "

								<div style='clear:both;'></div>";
								echo "<input type='submit' style='margin-top:5px;' value='Create Survey'/>
											<input type='hidden' name='num_rows' id='num_rows' value='0'/>
											</form>
											<div class='clearRow'></div>
								</div><!--main-->";


						echo "<script type='text/javascript'>addEmptyRow();</script>";/* make sure the first one gets put on the page */

		}//else if
		else if($mode=="view_quiz")
		{
				//$quiz_id = "q_3";//
				$locationid = "145"; ///////////
				require_once(dirname(__FILE__)."/quiz_view_quiz.php");
		}//else if

		else if($mode=="create_quiz")
		{
				/* This else-if section written by Chris Kirby */
				require_once(dirname(__FILE__)."/quiz.php");
		}//else if
		else if($mode=="payment_history")
		{
			require_once("assign_restaurant_to_payment.php");
			function parse_payment_response($response)
			{
				$vars = array();
				$response = explode("\n",$response);
				for($i=0; $i<count($response); $i++)
				{
					$line = explode("=",$response[$i]);
					if(count($line) > 1)
					{
						$vars[trim($line[0])] = trim($line[1]);
					}
				}
				return $vars;
			}

			$sum_capture = 0;
			$sum_refund = 0;
			$sum_declined = 0;
			$daily_totals = array();

			$mindate = "3000-01-01";
			$maxdate = "1000-01-01";

			function add_daily_total($daily_totals,$day,$amount)
			{
				if(isset($daily_totals[$day]))
					$daily_totals[$day] += ($amount * 1);
				else
					$daily_totals[$day] = 0 + ($amount * 1);
				return $daily_totals;
			}

			if(isset($_GET['wayback']))
				$min_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 360,date("Y")));
			else
				$min_date = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 60,date("Y")));

			if (!isset($_GET['get_single_row_id']))
				if (!isset($_GET['lastname']))
					$pr_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' order by `date` desc, `time` desc",$min_date);
				else
					$pr_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' AND `x_last_name` = '[2]' order by `date` desc, `time` desc",$min_date,$_GET['lastname']);
			else
				$pr_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `date`>='[1]' AND `id` = '[2]' order by `date` desc, `time` desc",$min_date,$_GET['get_single_row_id']);
			echo "<br><a href='index.php'>Back to Main Menu</a><br><br>";
			echo "<style>";
			echo ".style_capture { ";
			echo "  color: #ffffff; ";
			echo "} ";
			echo ".style_general { ";
			echo "  color: #000000; ";
			echo "} ";
			echo "</style>";
			echo "<table cellpadding=3>";
			while($pr_read = mysqli_fetch_assoc($pr_query))
			{
				if($mindate > $pr_read['date']) $mindate = $pr_read['date'];
				if($maxdate < $pr_read['date']) $maxdate = $pr_read['date'];

				$dt = $pr_read['date'];
				if(isset($daily_totals[$dt]))
				{
					$daily_totals[$dt] += $day_total;
				}

				//echo "$mindate > " . $pr_read['date'] . "<br>";

				$pay_info = parse_payment_response($pr_read['response']);
				if($pay_info['x_type']=="auth_capture")
				{
					if($pay_info['x_response_code']=="1")
					{
						$sum_capture += ($pay_info['x_amount'] * 1);
						$daily_totals = add_daily_total($daily_totals,$dt,($pay_info['x_amount'] * 1));
						$bgcolor = "#338833";

						if(isset($_GET['t4']))
						{
							if(isset($pay_info['x_subscription_id']) && $pay_info['x_subscription_id']!="")
							{
								$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `arb_license_id`='[1]'",$pay_info['x_subscription_id']);
								if(mysqli_num_rows($signup_query))
								{
									$signup_read = mysqli_fetch_assoc($signup_query);
									//echo "<tr><td colspan='4' bgcolor='#dddddd'>signup: " . $signup_read['company'] . "</td></tr>";
									$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$signup_read['dataname']);
									if(mysqli_num_rows($rest_query))
									{
										$rest_read = mysqli_fetch_assoc($rest_query);
										//echo "<tr><td colspan='4' bgcolor='#dddddd'>restaurant: " . $rest_read['company_name'] . "</td></tr>";
										$rest_id = $rest_read['id'];
										if($rest_read['license_status']=="")
										{
											$refund_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `response` LIKE '%[1]%' and `response` LIKE '%x_type = credit%'",$pay_info['x_account_number']);
											if(mysqli_num_rows($refund_query))
											{
												//echo "payment found with refund - " . $pay_info['x_first_name'] . "<br>";
											}
											else
											{
												mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `license_status`='Paid' where `id`='[1]'",$rest_id);
												echo "payment updated - " . $rest_read['company_name'] . "<br>";
											}
										}
										if($rest_read['package_status']=="")
										{
											$set_status = "";
											if($pay_info['x_amount']=="895.00")
												$set_status = "Silver";
											else if($pay_info['x_amount']=="1495.00")
												$set_status = "Gold";
											else if($pay_info['x_amount']=="3495.00")
												$set_status = "Platinum";
											mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `package_status`='[1]' where `id`='[2]'",$set_status,$rest_id);
											echo "status updated - " . $rest_read['company_name'] . " - " . $set_status . "<br>";
										}
									}
								}
							}
						}
					}
					else
					{
						$sum_decline += ($pay_info['x_amount'] * 1);
						$bgcolor = "#883333";
					}
					$class = "style_capture";
				}
				else if($pay_info['x_type']=="credit")
				{
					$sum_refund += ($pay_info['x_amount'] * 1);
					$daily_totals = add_daily_total($daily_totals,$dt,($pay_info['x_amount'] * -1));
					$bgcolor = "#333388";
					$class = "style_capture";
				}
				else
				{
					$bgcolor = "#ffffff";
					$class = "style_general";
				}
				echo "<tr bgcolor='$bgcolor'>";
				echo "<td>";

				$mrestid = $pr_read['match_restaurantid'];
				if(is_numeric($mrestid))
				{
					echo "<a href='index.php?mode=manage_customers&submode=details&rowid=$mrestid' target='_blank' style='color:#aabbff'><b>(manage account)</b></a>";
				}
				else if($mrestid!="" && trim($mrestid) != "&nbsp;")
				{
					echo "<font color='#ffffff'>" . $mrestid . "</font>";
				}
				else if($pr_read['x_type'] == "auth_capture" || $pr_read['x_type'] == "credit")
				{
					echo "<a href='#' id='assign_restaurant_".$pr_read['id']."' onclick='change_attached_restaurant_payment_ajax(this.id,".$pr_read['id'].");' style='color:#aabbff'><b>(assign restaurant)</b></a>";
				}
				else
				{
					echo "&nbsp;";
				}

				echo "</td>";
				echo "<td class='$class'>" . $pr_read['date'] . "</td>";
				echo "<td class='$class'>" . $pr_read['time'] . "</td>";
				echo "<td class='$class'>" . trim($pay_info['x_company'] . " " . $pay_info['x_account_number']) . "</td>";
				echo "<td class='$class'>" . trim($pay_info['x_first_name'] . " " . $pay_info['x_last_name']) . "</td>";
				echo "<td class='$class'>";
				if($pay_info['x_type']=="auth_capture" && $pay_info['x_response_code']=="1")
					echo "<b>" . $pay_info['x_amount'] . "</b>";
				else
					echo $pay_info['x_amount'];
				echo "</td>";
				//echo "<td>" . $pay_info['x_response_code'] . "</td>";
				echo "<td class='$class'>" . $pay_info['x_response_reason_text'] . "</td>";
				echo "<td class='$class'>" . $pay_info['x_type'] . "</td>";
				echo "<td class='$class'><a style='cursor:pointer' onclick='if(document.getElementById(\"details_".$pr_read['id']."\").style.display == \"table-row\") document.getElementById(\"details_".$pr_read['id']."\").style.display = \"none\"; else document.getElementById(\"details_".$pr_read['id']."\").style.display = \"table-row\"'>(details)</a></td>";
				echo "</tr>";
				echo "<tr>";
				echo "<td colspan='20' id='details_".$pr_read['id']."' style='display:none'>";
				echo trim(str_replace("x_","<br>x_",$pr_read['response']),"<br>");
				echo "</td>";
				echo "</tr>";
			}
			echo "</table>";

			$min_date_parts = explode("-",$mindate);
			$min_date_ts = mktime(0,0,0,$min_date_parts[1],$min_date_parts[2],$min_date_parts[0]);
			$max_date_parts = explode("-",$maxdate);
			$max_date_ts = mktime(0,0,0,$max_date_parts[1],$max_date_parts[2],$max_date_parts[0]);
			$date_diff = ($max_date_ts - $min_date_ts) / 24 / 60 / 60;

			if(isset($_GET['sum']))
			{
				echo "<br><br>Days: $date_diff ($mindate - $maxdate)";
				$sum_total = $sum_capture - $sum_refund;
				echo "<br>Total Capture: ".number_format($sum_capture,2,".",",");
				echo "<br>Total Refunds: ".number_format($sum_refund,2,".",",");
				echo "<br>total Declined: ".number_format($sum_decline,2,".",",");
				echo "<br>Sum (Capture - Refunds): ".number_format($sum_total,2,".",",");
				echo "<br>Sum Per Day: " . number_format($sum_total / $date_diff,2,".",",");

				echo "<br>";
				echo "<table>";
				$check_date = $maxdate;
				$last_check_date = $check_date;
				$week_counter = 0;
				$week_total = 0;
				$all_weeks = 0;
				$week_end = "";
				while($check_date >= $mindate && $mindate > "2000-01-01")
				{
					$last_check_date = $check_date;
					if($week_counter==0) $week_end = $check_date;
					$week_counter++;

					if(isset($daily_totals[$check_date]))
						$week_total += $daily_totals[$check_date] * 1;

					if($week_counter > 6)
					{
						echo "<tr><td>" . $last_check_date . " to " . $week_end . ": </td><td align='right'>$" . number_format($week_total,2,".",",") . "</td><td align='right'>$" . number_format($week_total / $week_counter,2,".",",") . "</td></tr>";
						$all_weeks += $week_total * 1;
						$week_counter = 0;
						$week_total = 0;
					}
					$dparts = explode("-",$check_date);
					$check_date_ts = mktime(0,0,0,$dparts[1],$dparts[2]-1,$dparts[0]);
					$check_date = date("Y-m-d",$check_date_ts);
				}
				if($week_total != 0)
				{
					echo "<tr><td>" . $last_check_date . " to " . $week_end . ": </td><td align='right'>$" . number_format($week_total,2,".",",") . "</td><td align='right'>$" . number_format($week_total / $week_counter,2,".",",") . "</td></tr>";
					$all_weeks += $week_total * 1;
				}
				echo "</table>";
				//echo "<br>All Weeks: " . number_format($all_weeks,2,".",",");
				/*foreach($daily_totals as $key => $val)
				{
					echo "<br>" . $key . ": " . $val;
				}*/
			}
		}
		else if ($mode == "account_counts")
		{
/*
			echo "<table cellpadding=6 style='border:solid 2px #888888' bgcolor='#eeeeee' width='100%'><td align='left'>";
			echo "Logged In: " . $loggedin_fullname;
			echo "</td><td align='right'>";
			echo "<a href='index.php?logout=1'>(logout)</a>";
			echo "</td></table>";

			echo "<br><a href='index.php'>Back to Main Menu</a><br><br>";
*/
			require_once("account_counts.php");
		}
		else if ($mode == "server_assignment_counts")
		{
			require_once("server_assignment_counts.php");
		}
		else if($mode=="tutorials")
		{
			if(isset($_GET['delete_category']))
			{
				mlavu_query("delete from `poslavu_MAIN_db`.`tutorial_categories` where id=".$_GET['delete_category']);
			}
			if(isset($_GET['delete_item']))
			{
				mlavu_query("delete from `poslavu_MAIN_db`.`tutorials` where id=".$_GET['delete_item']);
			}
			if(isset($_POST['new_category']))
			{
				$cat_query = mlavu_query("select * from `poslavu_MAIN_db`.`tutorial_categories` order by _order asc");
				while($cat_read = mysqli_fetch_assoc($cat_query))
				{
					$cat_name = $_POST['cat_'.$cat_read['id']];
					$cat_order = $_POST['cat_order_'.$cat_read['id']];
					mlavu_query("update `poslavu_MAIN_db`.`tutorial_categories` set `title`='[1]', `_order`='[2]' where id='[3]'",$cat_name,$cat_order,$cat_read['id']);
					$item_query = mlavu_query("select * from `poslavu_MAIN_db`.`tutorials` where `category`='[1]' order by _order asc",$cat_read['id']);
					while($item_read = mysqli_fetch_assoc($item_query))
					{
						$ivars = array();
						$ivars['name'] = $_POST['item_'.$item_read['id']];
						$ivars['order'] = $_POST['item_order_'.$item_read['id']];
						$ivars['path'] = $_POST['item_path_'.$item_read['id']];
						$ivars['id'] = $item_read['id'];
						if($ivars['name']!="")
							mlavu_query("update `poslavu_MAIN_db`.`tutorials` set `title`='[name]', `_order`='[order]', `path`='[path]' where id='[id]'",$ivars);
					}
					$new_item = (isset($_POST['new_item_'.$cat_read['id']]))?$_POST['new_item_'.$cat_read['id']]:"";
					$new_item_path = (isset($_POST['new_item_path_'.$cat_read['id']]))?$_POST['new_item_path_'.$cat_read['id']]:"";
					if($new_item!="")
					{
						mlavu_query("insert into `poslavu_MAIN_db`.`tutorials` (`title`,`category`,`path`) values ('[1]','[2]','[3]')",$new_item,$cat_read['id'],$new_item_path);
					}
				}
				$new_category = $_POST['new_category'];
				if($new_category!="")
				{
					//echo "insert into `poslavu_MAIN_db`.`tutorial_categories` (`title`) values ('[1]') - $new_category";
					mlavu_query("insert into `poslavu_MAIN_db`.`tutorial_categories` (`title`) values ('[1]')",$new_category);
				}
				if(isset($_POST['set_default']))
				{
					$set_default = $_POST['set_default'];
					if($set_default!="")
					{
						mlavu_query("delete from `poslavu_MAIN_db`.`tutorials` where `category`='0' and `title`='default'");
						mlavu_query("insert into `poslavu_MAIN_db`.`tutorials` (`category`,`title`,`path`) values ('0','default','[1]')",$set_default);
					}
				}
			}

			$use_default = 0;
			$def_query = mlavu_query("select * from `poslavu_MAIN_db`.`tutorials` where `category`='0' and `title`='default'");
			if(mysqli_num_rows($def_query))
			{
				$def_read = mysqli_fetch_assoc($def_query);
				$use_default = $def_read['path'];
			}

			echo "<table><td valign='top'>";
			echo "<form name='form1' method='post' action='index.php?mode=tutorials'>";
			echo "<table cellpadding=2>";
			$cat_query = mlavu_query("select * from `poslavu_MAIN_db`.`tutorial_categories` order by _order asc");
			while($cat_read = mysqli_fetch_assoc($cat_query))
			{
				echo "<tr><td bgcolor='#888888' style='padding-left:8px; padding-right:8px'><input name='cat_".$cat_read['id']."' type='text' value=\"" . $cat_read['title'] . "\"><input name='cat_order_".$cat_read['id']."' type='text' value='".$cat_read['_order']."' style='width:18px'> <a style='color:red;cursor:pointer;font-size:8px' onclick='if(confirm(\"Are you sure you want to delete this entire category?\")) window.location = \"index.php?mode=tutorials&delete_category=".$cat_read['id']."\"'>(x)</a></td></tr>";
				$item_query = mlavu_query("select * from `poslavu_MAIN_db`.`tutorials` where `category`='[1]' order by _order asc",$cat_read['id']);
				while($item_read = mysqli_fetch_assoc($item_query))
				{
					echo "<tr><td style='padding-left:20px; padding-right:8px'>";
					echo "<input type='radio' name='set_default' value='".$item_read['id']."'".(($use_default==$item_read['id'])?" checked":"").">";
					echo "<input name='item_".$item_read['id']."' type='text' value=\"" . $item_read['title'] . "\"><input name='item_path_".$item_read['id']."' type='text' value=\"" . $item_read['path'] . "\"><input name='item_order_".$item_read['id']."' type='text' value='".$item_read['_order']."' style='width:18px'> <a style='color:red;cursor:pointer;font-size:8px' onclick='if(confirm(\"Are you sure you want to delete this item?\")) window.location = \"index.php?mode=tutorials&delete_item=".$item_read['id']."\"'>(x)</a></td></tr>";
				}
				echo "<tr><td style='padding-left:20px; padding-right:8px'><input style='color:#777777' type='text' value=\"\" name='new_item_".$cat_read['id']."'><input style='color:#777777' type='text' value=\"\" name='new_item_path_".$cat_read['id']."'></td></tr>";
			}
			echo "<tr><td bgcolor='#cccccc' style='padding-left:8px; padding-right:8px'><input type='text' name='new_category' value=\"\" style='color:#777777'></td></tr>";
			echo "</table>";
			echo "<br><input type='submit' value='Submit Changes'>";
			echo "</form>";
			echo "</td><td valign='top' align='center'>";
			echo "<table><td>File Manager for: docs/</td></table>";
			echo "<br><iframe src='file_manager.php?dir=/home/poslavu/public_html/tutorials/docs' width='420' height='480' />";
			echo "</td></table>";
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
		else if($mode=="manage_alerts")
		{
/*
			echo "<table cellpadding=6 style='border:solid 2px #888888' bgcolor='#eeeeee' width='100%'><td align='left'>";
			echo "Logged In: " . $loggedin_fullname;
			echo "</td><td align='right'>";
			echo "<a href='index.php?logout=1'>(logout)</a>";
			echo "</td></table>";

			echo "<br><a href='index.php'>Back to Main Menu</a><br><br>";
*/
			require_once(dirname(__FILE__) . "/manage_alerts.php");
		}
		else if ($mode == "auto_close_orders")
		{
			require_once(dirname(__FILE__) . "/tools/auto_close_orders.php");
		}
		else if ($mode == "view_sync_order")
		{
			require_once(dirname(__FILE__) . "/tools/view_sync_order.php");
		}
		else if ($mode == "testgw")
		{
			require_once(dirname(__FILE__) . "/tools/testgw.php");
		}
		else if ($mode == "user_restore")
		{
			require_once(dirname(__FILE__) . "/user_restore.php");
		}
		else if ($mode == "revert_inv_1")
		{
			require_once(dirname(__FILE__) . "/revert_inv_1.php");
		}
		else if ($mode == "old_inventory_ingredients")
		{
			require_once(dirname(__FILE__) . "/old_inventory_ingredients.php");
		}
		else if ($mode == "language_pack")
		{
/*
			echo "<table cellpadding=6 style='border:solid 2px #888888' bgcolor='#eeeeee' width='100%'><td align='left'>";
			echo "Logged In: " . $loggedin_fullname;
			echo "</td><td align='right'>";
			echo "<a href='index.php?logout=1'>(logout)</a>";
			echo "</td></table>";

			echo "<br><a href='index.php'>Back to Main Menu</a><br><br>";
*/
			require_once("language_pack.php");
		}
		else if ($mode == "stats")
		{
			$script_path = dirname(__FILE__) . "/stats/".$submode.".php";
			error_log($script_path);
			if (file_exists($script_path)) {
				error_log("exists");
				require_once(dirname(__FILE__) . "/stats/".$submode.".php");
			}
		}
		else if ($mode == "view_restaurants_without_signups")
		{
			$s_filters_all = $_GET['filter'];
			echo "<form type='GET'>Filter: <input type='textbox' name='filter' value='".$s_filters_all."'><input type='hidden' name='mode' value='view_restaurants_without_signups'><input type='submit'></form>";
			echo "<br /><br />";
			$a_filters = explode("|", $s_filters_all);

			$restaurant_ids_query = mlavu_query("SELECT `id`,`data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `last_activity` > '[1]'", date("Y-m-d 00:00:00", strtotime("-7 days")) );
			$signups_ids_query = mlavu_query("SELECT `dataname` FROM `poslavu_MAIN_db`.`signups`");
			$a_signups_ids = array();
			while ($row = mysqli_fetch_assoc($signups_ids_query))
				$a_signups_ids[] = $row['dataname'];
			// check one against the other
			$total = 0;
			$a_missing_ids = array();
			while ($row = mysqli_fetch_assoc($restaurant_ids_query)) {
				if ($row['data_name'] != '' && !in_array($row['dataname'], $a_signups_ids)) {
					foreach($a_filters as $s_filter)
						if (stripos($row['data_name'], $s_filter) !== FALSE)
							continue 2;
					echo "Restaurant with id ".$row['id']." (".$row['data_name'].") missing in signups table.<br />";
					$a_missing_ids[] = $row['id'];
					$total++;
				}
			}
			echo "<br />$total restaurants<br />";
			echo "ids: `id`='".implode("' or `id`='", $a_missing_ids)."'";
		}

		else // main menu
		{
/*
			echo "<table cellpadding=6 style='border:solid 2px #888888' bgcolor='#eeeeee' width='100%'><td align='left'>";
			echo "Logged In: " . $loggedin_fullname;
			echo "</td><td align='right'>";
			echo "<a href='index.php?logout=1'>(logout)</a>";
			echo "</td></table>";
*/

			if(can_access("customers"))
			{
				//echo "<br><br><a href='index.php?mode=customers'>Customers</a>";
				echo "<br><br>";
				echo "<table cellpadding=4 style='border:solid 2px #dddddd' bgcolor='#eeeeee'><tr><td align='center'>";
				echo "<a href='index.php?mode=manage_customers'><font color='#555555' style='text-deocoration:none; font-size:16px'>Customer Management</font></a>";// (keep out - danger: guard dogs)</a>";
				echo "</td></tr>";
				echo "<tr><td align='center'><a href='index.php?mode=manage_customers'><img src='images/lavu_caveman.png' border='0' /></a></td></tr>";
				echo "</table>";

				echo "<br><br><a href='index.php?mode=upcoming_charges'>Upcoming License Charges</a>";

				echo "<br><br><a href='index.php?mode=client_search'>Client Location Search</a>";
				echo "<br><br><a href='index.php?mode=reseller_info'>Manage Resellers / Distributors</a>";
				echo "<br><br><a href='index.php?mode=distro_customers'>View Distro Customer Accounts</a>";
				echo "<br><br><a href='index.php?mode=payment_history'>Payment History</a>";
				//echo "<br><br><a href='index.php?mode=create_quiz'>Create Quiz</a>";
				//echo "<br><br><a href='index.php?mode=view_quiz'>View Quiz</a>";
				//echo "<br><br><a href='index.php?mode=create_customer_survey'>Create Customer Survey</a>";
				echo "<br><br><a href='index.php?mode=show_survey_answers'>View Customer Survey Answers</a>";
				//echo "<br><br><a href='index.php?mode=view_customer_survey_support'>View Customer Survey</a>";
				echo "<br><br><a href='index.php?mode=signup_history'>Signup History</a>";
				echo "<br><br><a href='index.php?mode=account_counts'>Account Counts</a>";
				echo "<br><br><a href='index.php?mode=server_assignment_counts'>Server Assignment Counts</a>";
				echo " <br><br><a href= './messages'> New Backend Messages</a>";
				echo "<br><br><a href='index.php?mode=manage_customers&submode=general_tools&tool=edit_coms'>Edit Coms</a>";

			}
			if (can_access("create_account"))
				echo "<br><br><a href='index.php?mode=create_account'>Create Account</a>";

			if(can_access("tutorials"))
			{
				echo "<br><br><br><a href='index.php?mode=tutorials'>Tutorials</a>";
			}
			if(can_access("language_pack"))
			{
				echo "<br><br><br><a href='index.php?mode=language_pack'>Language Pack</a>";
			}
			if (can_access("adhoc")) {
				echo "<br><br><br><a href='index.php?mode=adhoc_repository'>Ad Hoc in Testing</a>";
			}
			if(can_access("reports")) {
				echo "<br><br><a href='index.php?mode=create_reports'>Create Reports</a>";
			}
			if(can_access("technical"))
			{
				echo "<br><br><br><a href='index.php?mode=sync_database_structure'>Sync Database Structure</a> <a href='index.php?mode=show_database_structure_difference'>(display current difference)</a>";
				echo "<br><br><br><a href='index.php?mode=optimize_indexes'>Optimize Indexes</a>";
				echo "<br><br><br><a href='index.php?mode=clear_logs'>Clear Logs</a>";
				//echo "<br><br><a href='index.php?mode=sync_accounts'>Sync Customer Accounts to Main Account Table</a>";
				//echo "<br><br><a href='index.php?mode=migrate_modifiers'>Migrate Forced Modifiers</a>";
				//echo "<br><br><a href='index.php?mode=fix_groupids'>Fix Group Ids</a>";
				echo "<br><br><a href='index.php?mode=create_jkeys'>Create Jkeys</a>";//create_jkeys_for_all()
				echo "<br><br><a href='index.php?mode=perform_op'>Perform Important Operation</a>";

				echo "<br><br><br><br><hr>";
				echo "<h3>Lavu Kart</h3>";
				echo "<a href='index.php?mode=lavukart_tables'>Initialize LavuKart Database Structure</a>";
				//echo "<br><br><a href='index.php?mode=sqlite_sync'>Sqlite Sync</a>";
				$compvars = "comps=1&from=DEVKART&dbt=/home/poslavu/public_html/admin/cp/components/lavukart/tables.dbt";
				echo "<br><br><a href='index.php?mode=sync_database_structure&$compvars'>Sync Database Structure</a> <a href='index.php?mode=show_database_structure_difference&$compvars'>(display current difference)</a>";
			}

			//<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~<>~-~
			//   L A V U    L O C A L    S E R V E R
			echo "<hr>";

			if(can_access('billing_payment_status')){
                 echo "<h3>Lavu Local Server Authorization</h3>";
                 require_once('/home/poslavu/public_html/admin/manage/misc/LLSFunctions/lls_authorization.php');
			     echoLLSAuthorizationWebCode();//For allowing/disallowing data_name's from using LLS.
			     echo "<hr>";
			}
			if(can_access('customers')){
    			require_once(dirname(__FILE__).'/index_lls.php');
			}
		}
	}
	else // login screen
	{
		$in_lavu = true;
		require_once("login.php");
		/*echo "<form name='login' method='post' action='index.php'>";
		echo "<table>";
		echo "<tr><td align='right'>Username</td><td><input type='text' name='user'></td></tr>";
		echo "<tr><td align='right'>Password</td><td><input type='password' name='pass'></td></tr>";
		echo "<tr><td></td><td><input type='submit' value='Submit'></td></tr>";
		echo "</table>";
		echo "</form>";
		echo "<script language='javascript'>document.login.user.focus();</script>";
		*/
	}
?>
</body>
	<!-- Load scripts after body for best performance -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" ></script>
	<script type="text/javascript" src="assets/scripts/bootstrap.min.js" ></script>

	<script type="text/javascript" src="/cp/scripts/select2-4.0.3.min.js"></script>
	<link href="/cp/css/select2-4.0.3.min.css" rel="stylesheet" />
	<script type="text/javascript">
	$('select[name^=\'country\']').select2();
	$('.select2-container').css({'width':'20em', 'backgroud-color':'#fff', 'border':'1px solid #ccc'});
	$('.select2-container--default .select2-selection--single').attr('style', 'max-width:23em !important');

	$('#create_account_form').on('submit', function() {
		 var country = $('#country').val();

		 if (country == '') {
			alert('Please select country name');
			return false;
		}

	});
	</script>
</html>
