<script language="javascript">
	ajax_callback_function = "ajax_callback";
	ajax_render_id = "";
	
	function xmlhttpPost(strURL,querystr) 
	{
		var xmlHttpReq = false;
		var self = this;
		// Mozilla/Safari
		if (window.XMLHttpRequest) 
		{
			self.xmlHttpReq = new XMLHttpRequest();
		}
		// IE
		else if (window.ActiveXObject) 
		{
			self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
		}
		self.xmlHttpReq.open('POST', strURL, true);
		self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		self.xmlHttpReq.onreadystatechange = function() 
		{
			if (self.xmlHttpReq.readyState == 4) 
			{
				process_ajax_callback(self.xmlHttpReq.responseText);
			}
		}
		self.xmlHttpReq.send(querystr);
	}
		
	function process_ajax_callback(str)
	{
		var sep = "<!--" + "AJAX" + " " + "RESPONSE" + "-->";
		if(str.indexOf(sep) >= 0)
			start_pos = str.indexOf(sep) + sep.length;
		else
			start_pos = 0;
		eval(ajax_callback_function + "(str.substring(start_pos))");
	}
	
	function get_ajax_response(render_area,post_info)
	{
		xmlhttpPost(render_area,post_info);
		//xmlhttpPost("?render_frame=" + render_area.replace(/\?/ig,"&"),post_info);
	}
	
	function render_ajax_response(str)
	{
		document.getElementById(ajax_render_id).innerHTML = str;
		
		var x = document.getElementById(ajax_render_id).getElementsByTagName("script");   
	    for(var i=0;i<x.length;i++)  
	    {
			eval(x[i].text);
	    }
	}
	
	function ajax_link(render_id,render_area,post_info)
	{
		if(!post_info) post_info = "";
		ajax_callback_function = "render_ajax_response";
		ajax_render_id = render_id;
		get_ajax_response(render_area,post_info);
	}
	
	// example for ajax callback
	//function ajax_callback(str)
	//{
	//	alert("callback response " + str);
	//}
	//get_ajax_response("store.calendar?cinfo=test");
	
	// example for ajax link
	//ajax_link("container","store.calendar?cinfo=test");
</script>
