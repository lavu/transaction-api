<?php
	ini_set("display_errors","1");
	require_once("../cp/resources/lavuquery.php");
	
	function urlvar($var,$def=false)
	{
		return (isset($_GET[$var]))?$_GET[$var]:$def;
	}
	
	$date_mode = "unified";
	$dbname = "poslavu_DEV_db";
	$dbinfo = get_database_info($dbname);
	$tables = $dbinfo['tables'];
	$fields = $dbinfo['fields'];
		
	$rurl = "index.php?mode=create_reports";
	//lavu_select_db($dbname);
	
	$reportid = urlvar("reportid");
	$rmode = urlvar("rmode");
	if($reportid)
	{
		if(isset($_POST['report_data']))
		{
			$setvars = array();
			$rdata = explode("&",$_POST['report_data']);
			for($i=0; $i<count($rdata); $i++)
			{
				$ritem = explode("=",$rdata[$i]);
				if(count($ritem) > 1 && trim($ritem[0])!="")
				{
					$setvars[trim($ritem[0])] = trim($ritem[1]);
				}
			}
			/*echo "posted: <br>";
			foreach($setvars as $key => $val)
			{
				echo "<br>" . $key . " = " . $val;
			}*/
			$updatets = date("YmdHis");
			
			lavu_query("delete from `report_parts` where `reportid`='$reportid' and `type`='join' order by `_order` asc");
			$join_count = $setvars['join_count'];
			for($i=0; $i<$join_count; $i++)
			{
				$from_table = $setvars['join_table_from_'.($i + 1)];
				$from_field = $setvars['join_field_from_'.($i + 1)];
				$to_table = $setvars['join_table_to_'.($i + 1)];
				$to_field = $setvars['join_field_to_'.($i + 1)];
				
				lavu_query("insert into `report_parts` (`reportid`,`type`,`tablename`,`fieldname`,`prop1`,`prop2`) values ('$reportid','join','$from_table','$from_field','$to_table','$to_field')");
			}
			
			$select_count = $setvars['select_count'];
			for($i=0; $i<$select_count; $i++)
			{
				$sel_table = $setvars['select_table_'.($i + 1)];
				$sel_field = $setvars['select_field_'.($i + 1)];
				
				$ex_query = lavu_query("select * from `report_parts` where `reportid`='$reportid' and `type`='select' and `tablename`='$sel_table' and `fieldname`='$sel_field'");
				if(mysqli_num_rows($ex_query))
				{
					$ex_read = mysqli_fetch_assoc($ex_query);
					$rpartid = $ex_read['id'];
					lavu_query("update `report_parts` set `updatets`='$updatets' where id='$rpartid'");
				}
				else
				{
					lavu_query("insert into `report_parts` (`reportid`,`type`,`tablename`,`fieldname`,`updatets`) values ('$reportid','select','$sel_table','$sel_field','$updatets')");
				}
			}
			lavu_query("delete from `report_parts` where `reportid`='$reportid' and `type`='select' and `updatets`!='$updatets'");
			echo "<script language='javascript'>window.location.replace('$rurl&reportid=$reportid&rmode=details');</script>";
		}
		else if($rmode=="details")
		{
			if(isset($_POST['posted']))
			{
				$radio_date = (isset($_POST['radio_date']))?$_POST['radio_date']:"";
				$radio_group = (isset($_POST['radio_group']))?$_POST['radio_group']:"";
				$orderby = (isset($_POST['orderby']))?$_POST['orderby']:"";
				$set_group_operation = (isset($_POST['group_operation']))?str_replace("'","''",$_POST['group_operation']):"";
				$set_order_operation = (isset($_POST['order_operation']))?str_replace("'","''",$_POST['order_operation']):"";
				$date_select_type = (isset($_POST['date_select_type']))?str_replace("'","''",$_POST['date_select_type']):"";
				
				lavu_query("update `reports` set `dateid`='$radio_date', `groupid`='$radio_group', `orderby`='$orderby', `group_operation`='$set_group_operation', `order_operation`='$set_order_operation', `date_select_type`='$date_select_type' where `id`='$reportid'");
				$rep_query = lavu_query("select * from `report_parts` where `reportid`='$reportid' and `type`='select' order by `tablename` asc");
				while($rep_read = mysqli_fetch_assoc($rep_query))
				{
					$set_table = $rep_read['tablename'];
					$set_field = $rep_read['fieldname'];
					$set_prop1 = $rep_read['prop1'];
					$set_prop2 = $rep_read['prop2'];
					$set_type = $rep_read['type'];
					$partid = $rep_read['id'];
					
					$set_operation = $_POST['operation_'.$partid];
					$set_opargs = str_replace("'","''",$_POST['opargs_'.$partid]);
					$set_filter = str_replace("'","''",$_POST['filter_'.$partid]);
					$set_order = str_replace("'","''",$_POST['_order_'.$partid]);
					$set_hide = str_replace("'","''",$_POST['hide_'.$partid]);
					if(isset($_POST['sum_'.$partid])) $set_sum = "1";
					else $set_sum = "0";
					
					lavu_query("update `report_parts` set `operation`='$set_operation', `opargs`='$set_opargs', `filter`='$set_filter', `hide`='$set_hide', `sum`='$set_sum', `_order`='$set_order' where id='$partid'");
				}
				
				echo "<script language='javascript'>window.location.replace('$rurl&reportid=$reportid&rmode=view');</script>";
				//echo "<br><br><input type='button' value='View Report' onclick='window.location = \"$rurl&reportid=$reportid&rmode=view\"'>";
			}
			else
			{
				echo "<a href='$rurl'>(View All Reports)</a>";
				echo "<br><br>";
			
				$report_query = lavu_query("select * from `reports` where id='$reportid'");
				if(mysqli_num_rows($report_query))
				{
					$report_read = mysqli_fetch_assoc($report_query);
					$groupid = $report_read['groupid'];
					$dateid = $report_read['dateid'];
					$orderby = $report_read['orderby'];
					$set_group_operation = $report_read['group_operation'];
					$set_order_operation = $report_read['order_operation'];
					$date_select_type = $report_read['date_select_type'];
					
					echo "Report: " . $report_read['title'];
					echo "<br><a href='$rurl&reportid=$reportid'><< Fields</a> <b>(Details)</b><br><br>";
				}
								
				echo "<script language='javascript'>";
				echo "function clearRadio(btngroup) { ";
				echo "  for(x=0; x<btngroup.length; x++) { ";
				echo "     btngroup[x].checked = false; ";
				echo "  } ";
				echo "} ";
				echo "</script>";
				
				echo "<form name='form1' method='post' action='$rurl&reportid=$reportid&rmode=details'>";
				echo "<input type='hidden' name='posted' value='1'>";
				echo "<table cellpadding=4>";
				echo "<tr>";
				echo "<td align='center'>Order</td>";
				echo "<td align='center'>Display</td>";
				echo "<td align='center'>Date</td>";
				echo "<td align='center'>Group</td>";
				echo "<td align='center'>Select Operation</td>";
				echo "<td></td><td></td>";
				echo "<td align='center'>Where Filter</td>";
				echo "<td align='center'>Asc</td>";
				echo "<td align='center'>Desc</td>";
				echo "<td align='center'>Sum</td>";
				echo "</tr>";
				$rep_query = lavu_query("select * from `report_parts` where `reportid`='$reportid' and `type`='select' order by `_order` asc, `tablename` asc");
				while($rep_read = mysqli_fetch_assoc($rep_query))
				{
					$set_table = $rep_read['tablename'];
					$set_field = $rep_read['fieldname'];
					$set_prop1 = $rep_read['prop1'];
					$set_prop2 = $rep_read['prop2'];
					$set_type = $rep_read['type'];
					$set_operation = $rep_read['operation'];
					$set_opargs = $rep_read['opargs'];
					$set_filter = $rep_read['filter'];
					$set_sum = $rep_read['sum'];
					$partid = $rep_read['id'];
					$_order = $rep_read['_order'];
					$hide = $rep_read['hide'];
					
					echo "<tr>";
					echo "<td><input type='text' name='_order_$partid' value='$_order' size='2'></td>";
					echo "<td><input type='checkbox' name='display_$partid' onchange='if(this.checked) document.form1.hide_$partid.value = \"0\"; else document.form1.hide_$partid.value = \"1\"'";
					if($hide!="1")
					{
						echo " checked";
					}
					echo "><input type='hidden' name='hide_$partid' value='$hide'></td>";
					echo "<td align='center'><input type='radio' name='radio_date' value='$partid'".(($partid==$dateid)?" checked":"")."></td>";
					echo "<td align='center'><input type='radio' name='radio_group' value='$partid'".(($partid==$groupid)?" checked":"")."></td>";
					echo "<td>";
					/*echo "<select name='operation_$partid'>";
					$options = array("","sum","count","avg","format","left","right","custom");
					for($i=0; $i<count($options); $i++) 
					{
						echo "<option value='".$options[$i]."'";
						if($set_operation==$options[$i])
							echo " selected";
						echo ">".$options[$i]."</option>";
					}
					echo "</select>";*/
					echo "<input type='hidden' name='operation_$partid' value='custom'>";
					echo "<input type='text' name='opargs_$partid' value=\"".str_replace("\"","&quot;",$set_opargs)."\" size='32'>";
					echo "</td>";
					echo "<td style='font-size:10px; color:#aaaaaa' align='right'>" . $set_table . "</td>";
					echo "<td>" . $set_field . "</td>";
					echo "<td><input type='text' name='filter_$partid' value=\"".str_replace("\"","&quot;",$set_filter)."\" size='32'></td>";
					echo "<td>";
					echo "<input type='radio' name='orderby' value='$partid asc'";
					if($orderby=="$partid asc")
						echo " checked";
					echo ">";
					echo "</td><td>";
					echo "<input type='radio' name='orderby' value='$partid desc'";
					if($orderby=="$partid desc")
						echo " checked";
					echo ">";
					echo "</td>";
					echo "<td>";
					echo "<input type='checkbox' name='sum_$partid'";
					if($set_sum=="1") echo " checked";
					echo ">";
					echo "</td>";
					echo "</tr>";
				}
				echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo "<td>&nbsp;</td>";
				echo "<td><input type='button' style='font-size:9px' onclick='clearRadio(document.form1.radio_date)' value='clear'></td>";
				echo "<td><input type='button' style='font-size:9px' onclick='clearRadio(document.form1.radio_group)' value='clear'></td>";
				echo "</tr>";
				echo "<tr><td colspan='4' align='right'>Group Operation</td>";
				echo "<td><input type='text' name='group_operation' value=\"".str_replace("\"","&quot;",$set_group_operation)."\" size='32'></td>";
				echo "</tr>";
				echo "<tr><td colspan='4' align='right'>Order Operation</td>";
				echo "<td><input type='text' name='order_operation' value=\"".str_replace("\"","&quot;",$set_order_operation)."\" size='32'></td>";
				echo "<td></td></tr>";
				echo "<tr><td colspan='4' align='right'>Date Select Type</td>";
				echo "<td><select name='date_select_type'>";
				$date_selectlist = array("","Day","Day Range","Month","Year");
				for($d=0; $d<count($date_selectlist); $d++)
					echo "<option value='".$date_selectlist[$d]."'".(($date_selectlist[$d]==$date_select_type)?" selected":"").">".$date_selectlist[$d]."</option>";
				echo "</select></td>";
				echo "<td></td></tr>";
				echo "</table>";
				echo "<input type='submit' value='Submit'>";
				echo "</form>";
				//echo "<br><br><input type='button' value='View Report' onclick='window.location = \"$rurl&reportid=$reportid&rmode=view\"'>";
			}
		}
		else if($rmode=="view")
		{	
			echo "<a href='$rurl'>(View All Reports)</a>";
			echo "<br><br>";
			
			$date_start = "";
			$date_end = "";
			$time_start = "";
			$time_end = "";
			
			$primary_table = "";
			$join_code = "";
			$select_code = "";
			$where_code = "";
			$stitles = array();
			$sdisplay = array();
			$scount = 0;
			
			$groupid = "";
			$dateid = "";
			$timeid = "";
			$groupby = "";
			
			$report_query = lavu_query("select * from `reports` where id='$reportid'");
			if(mysqli_num_rows($report_query))
			{
				$report_read = mysqli_fetch_assoc($report_query);
				$groupid = $report_read['groupid'];
				$dateid = $report_read['dateid'];
				$timeid = $report_read['timeid'];
				$group_operation = $report_read['group_operation'];
				$order_operation = $report_read['order_operation'];
				$orderby = explode(" ",$report_read['orderby']);
				$date_select_type = $report_read['date_select_type'];
				
				if(count($orderby) > 1)
				{
					$orderby_id = trim($orderby[0]);
					$orderby_dir = trim($orderby[1]);
				}
				else
				{
					$orderby_id = 0;
					$orderby_dir = "asc";
				}
				$orderby = "";
				
				echo "<br>Report: " . $report_read['title'] . "<br>";
				echo "<a href='$rurl&reportid=$reportid'><< Fields</a> - <a href='$rurl&reportid=$reportid&rmode=details'><< Details</a> <b>(View)</b><br><br>";
			}
			$reporturl = "$rurl&reportid=$reportid&rmode=view";
			//------------------------------------ date/time selection here -----------------------------------//
			if($date_select_type=="Year")
			{
				$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y");
				echo "<select onchange='window.location = \"$reporturl&setdate=\" + this.value'>";
				for($i=date("Y")-8; $i<date("Y")+8; $i++)
				{
					$sv = $i;
					echo "<option value='$sv'";
					if($sv==$setdate) echo " selected";
					echo ">".date("Y",mktime(0,0,0,0,0,$i+1))."</option>";
				}
				echo "</select>";
				$set_date_start = $setdate . "-01-01";
				$set_date_end = $setdate . "-12-31";
				$set_time_start = "00:00:00";
				$set_time_end = "24:00:00";
			}
			else if($date_select_type=="Month")
			{
				$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y-m");
				echo "<select onchange='window.location = \"$reporturl&setdate=\" + this.value'>";
				for($i=date("Y")-2; $i<date("Y")+2; $i++)
				{
					for($n=1; $n<=12; $n++)
					{
						if($n<10) $m = "0".$n; else $m = $n;
						$sv = $i . "-" . $m;
						echo "<option value='$sv'";
						if($sv==$setdate) echo " selected";
						echo ">".date("M Y",mktime(0,0,0,$n+1,0,$i))."</option>";
					}
				}
				echo "</select>";
				$set_date_start = $setdate . "-01";
				$set_date_end = $setdate . "-31";
				$set_time_start = "00:00:00";
				$set_time_end = "24:00:00";
			}
			else if($date_select_type=="Day" || $date_select_type=="Day Range")
			{
				$setdate = (isset($_GET["setdate"]))?$_GET['setdate']:date("Y-m-d");
				$day_duration = (isset($_GET['day_duration']))?$_GET['day_duration']:1;
				
				echo "<script type=\"text/javascript\" src=\"sa_resources/datepickercontrol.js\"></script>";
				echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"sa_resources/datepickercontrol.css\">";
				echo "<script language='javascript'>";
				echo "DatePickerControl.onSelect = function(inputid) { ";
				echo "   window.location = '$reporturl&day_duration=$day_duration&setdate=' + document.getElementById(inputid).value; ";
				echo "} ";
				echo "</script>";
 								
				$set_date_start = $setdate;
				$date_start_ts = mktime(0,0,0,substr($set_date_start,5,2),substr($set_date_start,8,2),substr($set_date_start,0,4));
				
				echo "<input type=\"hidden\" id=\"DPC_TODAY_TEXT\" value=\"today\">";
				echo "<input type=\"hidden\" id=\"DPC_BUTTON_TITLE\" value=\"Open calendar...\">";
				echo "<input type=\"hidden\" id=\"DPC_MONTH_NAMES\" value=\"['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']\">";
				echo "<input type=\"hidden\" id=\"DPC_DAY_NAMES\" value=\"['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']\">";
				echo "<input type=\"text\" name=\"edit1\" id=\"edit1\" datepicker=\"true\" datepicker_format=\"YYYY-MM-DD\" value=\"".date("m/d/Y",$date_start_ts)."\">";
				if($date_select_type=="Day Range")
				{
					echo "<select onchange='window.location = \"$reporturl&setdate=$setdate&day_duration=\" + this.value'>";
					for($i=1; $i<=30; $i++)
					{
						echo "<option value='$i'";
						if($day_duration==$i) echo " selected";
						echo ">";
						echo "to " . date("m/d/Y",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($i - 1),date("Y",$date_start_ts))) . " ($i day";
						if($i > 1) echo "s";
						echo ")";
						echo "</option>";
					}
					echo "</select>";
				}
				else $day_duration = 1;
				
				$set_date_end = date("Y-m-d",mktime(0,0,0,date("m",$date_start_ts),date("d",$date_start_ts) + ($day_duration - 1),date("Y",$date_start_ts)));
				$set_time_start = "00:00:00";
				$set_time_end = "24:00:00";
			}
			
			if($date_mode=="unified")
			{
				$date_start = "$set_date_start $set_time_start";
				$date_end = "$set_date_end $set_time_end";
			}
			else
			{
				$date_start = "$set_date_start";
				$date_end = "$set_date_end";
				$time_start = $set_time_start;
				$time_end = $set_time_end;
			}
			
			$tables_joined = array();
			
			$rep_query = lavu_query("select * from `report_parts` where `reportid`='$reportid' order by `_order` asc");
			while($rep_read = mysqli_fetch_assoc($rep_query))
			{
				$set_table = $rep_read['tablename'];
				$set_field = $rep_read['fieldname'];
				$set_prop1 = $rep_read['prop1'];
				$set_prop2 = $rep_read['prop2'];
				$set_type = $rep_read['type'];
				$set_operation = $rep_read['operation'];
				$set_opargs = $rep_read['opargs'];
				$set_filter = $rep_read['filter'];
				$partid = $rep_read['id'];
				$hide = ($rep_read['hide']=="1")?true:false;
				
				if($primary_table=="")
				{
					$primary_table = $set_table;
					$tables_joined[] = $set_table;
				}
				if($set_type=="join")
				{
					$set_table_joined = false;
					for($x=0; $x<count($tables_joined); $x++)
					{
						if($tables_joined[$x]==$set_table)
							$set_table_joined = true;
					}
					if($set_table_joined)
					{
						$tables_joined[] = $set_prop1;
						$join_code .= " LEFT JOIN `$set_prop1` ON `$set_table`.`$set_field` = `$set_prop1`.`$set_prop2`";
					}
					else
					{
						$tables_joined[] = $set_table;
						$join_code .= " LEFT JOIN `$set_table` ON `$set_prop1`.`$set_prop2` = `$set_table`.`$set_field`";
					}
				}
				else if($set_type=="select")
				{
					$sname = "`$set_table`.`$set_field`";
					$prime_sname = $sname;
					if($set_filter!="")
					{
						if($where_code!="") $where_code .= " and ";
						$set_filter = str_replace("'","''",$set_filter);
						$where_code .= str_replace("*",$sname,$set_filter);
					}
					if($dateid==$partid)
					{
						if($where_code!="") $where_code .= " and ";
						$where_code .= "$sname >= '$date_start' and $sname <= '$date_end'";
					}
					if($timeid==$partid)
					{
						if($where_code!="") $where_code .= " and ";
						$where_code .= "$sname >= '$time_start' and $sname <= '$time_end'";
					}
					if($set_operation!="")
					{
						if($set_operation=="custom")
						{
							if($set_opargs!="")
								$sname = str_replace("*",$sname,$set_opargs);
						}
						else
						{
							$sname = strtoupper($set_operation) . "(" . $sname;
							if($set_opargs!="")
							{
								$set_opargs = str_replace("'","''",$set_opargs);
								$sname .= "," . $set_opargs;
							}
							$sname .= ")";
						}
					}
					if($groupid==$partid)
					{
						if($group_operation && $group_operation!="")
							$groupby = " group by " . str_replace("*",$prime_sname,$group_operation);
						else
							$groupby = " group by `field$scount` ";
					}					
					
					$sdisplay[] = !$hide;
					$stitles[] = $set_field;
					if($select_code!="") $select_code .= ",";
					$select_code .= "$sname AS `field$scount`";
					if($orderby_id==$partid)
					{
						if($order_operation && $order_operation!="")
							$orderby = " order by " . str_replace("*",$prime_sname,$group_operation) . " " . $orderby_dir;
						else
							$orderby = " order by `field$scount` ".$orderby_dir;
					}
					$scount++;
				}
			}
			if($where_code!="")
				$where_code = "where ".$where_code;
			$query = "select $select_code from `$primary_table`".$join_code.$where_code.$groupby.$orderby;
			//echo $query . "<br><br>";			
			lavu_select_db("poslavu_DEV_db");
			echo "<table>";
			echo "<tr>";
			for($i=0; $i<count($stitles); $i++)
			{
				if($sdisplay[$i])
					echo "<td syle='border-bottom:solid 1px black' align='center'><b>".$stitles[$i]."</b></td>";
			}
			echo "</tr>";
			$result_query = lavu_query($query);
			while($result_read = mysqli_fetch_assoc($result_query))
			{
				echo "<tr>";
				for($i=0; $i<$scount; $i++)
				{
					if($sdisplay[$i])
						echo "<td width='180'>".$result_read['field'.$i]."</td>";
				}
				echo "</tr>";
			}
			echo "</table>";
			//echo $query;
		}
		else
		{
			$init_script = "";
			$rep_query = lavu_query("select * from `report_parts` where `reportid`='$reportid' order by `_order` asc");
			while($rep_read = mysqli_fetch_assoc($rep_query))
			{
				$set_table = $rep_read['tablename'];
				$set_fieldname = $rep_read['fieldname'];
				$set_prop1 = $rep_read['prop1'];
				$set_prop2 = $rep_read['prop2'];
				$set_type = $rep_read['type'];
				if($set_type=="join")
				{
					$init_script .= "focus_table = \"$set_table\";\n";
					$init_script .= "focus_field = \"$set_fieldname\";\n";
					$init_script .= "field_join('$set_prop1','$set_prop2');\n";
				}
				else if($set_type=="select")
				{
					$init_script .= "field_select('$set_table','$set_fieldname');\n";
				}
			}
			
			$new_table = urlvar("new_table");
			$remove_table = urlvar("remove_table");
			if($new_table || $remove_table)
			{
				$rep_query = lavu_query("select * from `reports` where id='[1]'",$reportid);
				if(mysqli_num_rows($rep_query))
				{
					$rep_read = mysqli_fetch_assoc($rep_query);
					
					$tlist = $rep_read['tables'];
					$table_in = (strpos($tlist,"|".$new_table."|")!==false);
					if($table_in && $remove_table)
					{
						$tlist = str_replace("|" . $remove_table . "|","",$tlist);
					}
					else if(!$table_in && $new_table)
					{
						$tlist .= "|" . $new_table . "|";
					}
					lavu_query("update `reports` set `tables`='[1]' where id='[2]'",$tlist,$reportid);
				}
			}
			
			$rep_query = lavu_query("select * from `reports` where id='[1]'",$reportid);
			if(mysqli_num_rows($rep_query))
			{
				$rep_read = mysqli_fetch_assoc($rep_query);
				
				echo "<a href='$rurl'>(View All Reports)</a>";
				echo "<br><br>";
				echo "<table>";
				echo "<td>Report: <b>" . $rep_read['title'] . "</b></td><td width='40'>&nbsp;</td>";
				echo "<td id='mode_join' style='border-bottom:solid 1px black; cursor:pointer;' onclick='set_rmode(\"join\")'>Join</td><td width='16'>&nbsp;</td>";
				echo "<td id='mode_select' style='cursor:pointer;' onclick='set_rmode(\"select\")'>Select</td><td width='16'>&nbsp;</td>";
				//echo "<td id='mode_group' style='cursor:pointer;' onclick='set_rmode(\"group\")'>Group</td><td width='16'>&nbsp;</td>";
				echo "<td width='24'>&nbsp;</td>";
				echo "<td><input type='button' value='Submit' onclick='submit_rdata()'></td>";
				echo "</table>";
				echo "<br><br>";
				
				$report_tables = array();
				$tlist = explode("|",$rep_read['tables']);
				for($i=0; $i<count($tlist); $i++)
					if($tlist[$i]!="") 
						$report_tables[] = $tlist[$i];
					
				echo "<table style='border:solid 1px #555555'>";
				for($i=0; $i<count($report_tables); $i++)
				{
					$tablename = $report_tables[$i];
					echo "<td valign='top'>";
					echo "<table><td width='200' bgcolor='#dddddd' align='center'>";
					echo $tablename;
					echo "&nbsp;&nbsp;";
					echo "<a style='cursor:pointer' onclick='if(confirm(\"Remove $tablename?\")) window.location = \"$rurl&reportid=$reportid&remove_table=$tablename\"'><font color='#bb0000' style='font-size:10px'>(x)</font></a>";
					echo "</td></table>";
					$field_names = $dbinfo['fields'][$tablename][0];
					$field_types = $dbinfo['fields'][$tablename][1];
					
					echo "<table>";
					for($n=0; $n<count($field_names); $n++)
					{
						$fieldname = $field_names[$n];
						$fieldid = $tablename . "_" . $fieldname;
						$statusid = $fieldid . "_status";
						echo "<tr>";
						echo "<td id='$statusid'>&nbsp;</td>";
						echo "<td id='$fieldid' style='cursor:pointer' onselectstart='return false;' onmousedown='field_mousedown(\"$tablename\",\"$fieldname\"); return false;' onmouseup='field_mouseup(\"$tablename\",\"$fieldname\"); return false;' onclick='field_click(\"$tablename\",\"$fieldname\")'>";
						echo $fieldname;
						echo "</td>";
						echo "</tr>";
					}
					echo "</table>";
					
					echo "</td>";
				}
				echo "<td valign='top'>";
				echo "<table><td width='200' bgcolor='#dddddd'>&nbsp;</td></table>";
				echo "<br>Add New Table:";
				echo "<br>";
				echo "<select name='new_table' onchange='window.location = \"$rurl&reportid=$reportid&new_table=\" + this.value'>";
				echo "<option value=''></option>";
				for($i=0; $i<count($tables); $i++)
				{
					echo "<option value='".$tables[$i]."'>" . $tables[$i] . "</option>";
				}
				echo "</select>";
				echo "</td>";
				echo "</table>";
				?>
				
				<form name='send_data' method='post' action='<?php echo $rurl?>&reportid=<?php echo $reportid?>'>
					<input type='hidden' name='report_data' />
				</form>
				
				<script language="javascript">
					fmode = "join";
					focus_table = "";
					focus_field = "";
					colors = new Array("#008800","#0000ff","#ff0000","#ff00ff","#ffff00","#00ffff","#007700","#000077","#770000","#007777");
					joins = new Array();
					selects = new Array();
					
					function field_click(tname,fname)
					{
					}
					function field_mousedown(tname,fname)
					{
						focus_table = tname;
						focus_field = fname;
					}
					function field_mouseup(tname,fname)
					{
						if(fmode=="join")
						{
							field_join(tname,fname);
						}
						else if(fmode=="select")
						{
							field_select(tname,fname);
						}
						else if(fmode=="group")
						{
							field_group(tname,fname);
						}
					}
					function field_select(tname,fname)
					{
						fieldid = tname + "_" + fname;
						current_val = document.getElementById(fieldid).bgColor;
						if(current_val=="#aabbff" || current_val=="#AABBFF" || current_val=="aabbff" || current_val=="AABBFF")
						{
							set_val = "#ffffff";
							saction = "remove";
						}
						else
						{
							set_val = "#aabbff";
							saction = "add";
						}
						
						sfound = false;
						old_selects = selects;
						selects = new Array();
						for(n=0; n<old_selects.length; n++)
						{
							if(old_selects[n][0]==tname && old_selects[n][1]==fname)
							{
								sfound = true;
								if(saction!="remove")
									selects.push(old_selects[n]);
							}
							else selects.push(old_selects[n]);
						}
						if(!sfound)
						{
							if(saction=="add")
								selects.push(new Array(tname,fname));
						}
						document.getElementById(fieldid).bgColor = set_val;
					}
					function submit_rdata()
					{
						setstr = "reportid=<?php echo $reportid?>"; 
						join_count = 0;
						joinstr = "";
						for(n=0; n<joins.length; n++)
						{
							if(joins[n][0]!="")
							{
								join_count++;
								joinstr += "&join_table_from_" + join_count + "=" + joins[(join_count * 1 - 1)][0];
								joinstr += "&join_field_from_" + join_count + "=" + joins[(join_count * 1 - 1)][1];
								joinstr += "&join_table_to_" + join_count + "=" + joins[(join_count * 1 - 1)][2];
								joinstr += "&join_field_to_" + join_count + "=" + joins[(join_count * 1 - 1)][3];
							}
						}
						setstr += "&join_count=" + join_count;
						setstr += joinstr;
						
						setstr += "&select_count=" + selects.length;
						for(n=0; n<selects.length; n++)
						{
							setstr += "&select_table_" + (n * 1 + 1) + "=" + selects[n][0];
							setstr += "&select_field_" + (n * 1 + 1) + "=" + selects[n][1];
						}
						document.send_data.report_data.value = setstr;
						document.send_data.submit();
					}
					function field_group(tname,fname)
					{
					}
					function field_join(tname,fname)
					{
						if(tname!=focus_table)
						{
							new_join = new Array(focus_table,focus_field,tname,fname);
							join_index = 0;
							found = false;
							for(i=0; i<joins.length; i++)
							{
								if(joins[i][0]=="" && !found)
								{
									join_index = i;
									joins[i] = new_join;
									found = true;
								}
							}
							if(!found)
							{
								joins.push(new_join);
								join_index = joins.length - 1;
							}
							
							fromid = focus_table + "_" + focus_field + "_status";
							toid = tname + "_" + fname + "_status";
							
							bgcolor = colors[join_index];
							boxcode = "<table><td bgcolor='" + bgcolor + "' width='16' height='16' style='color:#ffffff; cursor:pointer' onclick='if(confirm(\"Remove Join?\")) remove_join(\"" + join_index + "\")'>join</td></table>";
							
							document.getElementById(fromid).innerHTML = boxcode;
							document.getElementById(toid).innerHTML = boxcode;
							//alert("link " + focus_table + "-" + focus_field + " to " + tname + "-" + fname);
						}
					}
					function remove_join(join_index)
					{
						jfrom_table = joins[join_index][0];
						jfrom_field = joins[join_index][1];
						jto_table = joins[join_index][2];
						jto_field = joins[join_index][3];
	
						fromid = jfrom_table + "_" + jfrom_field + "_status";
						toid = jto_table + "_" + jto_field + "_status";
	
						document.getElementById(fromid).innerHTML = "&nbsp;";
						document.getElementById(toid).innerHTML = "&nbsp;";
						joins[join_index] = new Array("","","","");
					}
					function set_rmode(setmode)
					{
						fmode = setmode;
						modes = new Array("join","select");//,"group");
						for(i=0; i<modes.length; i++)
						{
							if(fmode==modes[i]) setstyle = "solid 1px black";
							else setstyle = "none";
							
							document.getElementById("mode_" + modes[i]).style.borderBottom = setstyle;
						}
					}
					<?php echo $init_script; ?>
				</script>
				
				<?php
			}
		}
	}
	else
	{
		$new_report = urlvar("new_report");
		$set_display = urlvar("set_display");
		$set_displayid = urlvar("set_displayid");
		if($new_report)
		{
			lavu_query("insert into `reports` (`title`) values ('[1]')",$new_report);
		}
		if($set_displayid)
		{
			lavu_query("update `reports` set `display`='[1]' where id='[2]'",$set_display,$set_displayid);
		}
		
		echo "<table cellpadding=4 style='border:solid 1px black'>";
		$rep_query = lavu_query("select * from `reports` order by `title` asc");
		while($rep_read = mysqli_fetch_assoc($rep_query))
		{
			echo "<tr><td width='240' onmouseover='this.bgColor = \"#ccddee\"' onmouseout='this.bgColor = \"#ffffff\"' onclick='window.location = \"$rurl&reportid=".$rep_read['id']."\"'>" . $rep_read['title'] . "</td>";
			echo "<td>";
			echo "<select onchange='window.location = \"$rurl&set_displayid=".$rep_read['id']."&set_display=\" + this.value'>";
			echo "<option value='0'></option>";
			echo "<option value='1'".(($rep_read['display']=="1")?" selected":false).">Display</option>";
			echo "</select>";
			echo "</td>";
			echo "</tr>";
		}
		echo "</table>";
		echo "<br><input type='text' id='new_report' name='new_report'><input type='button' onclick='setval = document.getElementById(\"new_report\").value; if(setval==\"\") alert(\"Please provide the name of your new report\"); else window.location = \"$rurl&new_report=\" + setval;' value=
		'Create New Report'>";
	}

	/*$tablename = urlvar("tablename");
	if($tablename)
	{
		echo "<input type='button' value='Back to Table List' onclick='window.location = \"index.php?mode=create_reports\"' /><br><br>";
		echo "Table <b>$tablename</b>:<br>";
		$field_names = $dbinfo['fields'][$tablename][0];
		$field_types = $dbinfo['fields'][$tablename][1];
		
		for($i=0; $i<count($field_names); $i++)
		{
			echo "<br>".$field_names[$i];
		}
	}
	else
	{	
		for($i=0; $i<count($tables); $i++)
		{
			echo "<br><a href='index.php?mode=create_reports&tablename=".$tables[$i]."'>" . $tables[$i] . "</a>";
		}
	}*/
?>
