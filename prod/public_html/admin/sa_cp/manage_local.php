<?php
	if(isset($_GET['locid']))
	{
		$lastsync_time = 0;
		$lastsync_interval = 0;
		if(!isset($dataname)) $dataname = "";
		if(!isset($loc_id)) $loc_id = 0;
		
		function show_time_till_next_sync($dataname,$loc_id,$fwd_link="")
		{
			global $lastsync_time;
			global $lastsync_interval;
			
			$lastsync_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='last sync request'", $dataname);
			if(mysqli_num_rows($lastsync_query))
			{
				$lastsync_read = mysqli_fetch_assoc($lastsync_query);
				//echo "<br>Last Sync: " . $lastsync_read['value'];
				
				$lastsync_time = $lastsync_read['value'];
				if(strpos($lastsync_time,"/"))
				{
					$lastsync_parts = explode("/",$lastsync_time);
					$lastsync_time = $lastsync_parts[0];
					$lastsync_interval = $lastsync_parts[1];
				}
				else $lastsync_interval = 60;
				
				$lastsync_fin_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='last sync finished' order by value desc limit 1", $dataname);
				while($lastsync_fin_read = mysqli_fetch_assoc($lastsync_fin_query))
				{
					$lastsync_fin_time = $lastsync_fin_read['value'];
					if(strpos($lastsync_fin_time,"/"))
					{
						$lastsync_fin_parts = explode("/",$lastsync_fin_time);
						$lastsync_fin_time = $lastsync_fin_parts[0];
						$lastsync_fin_interval = $lastsync_fin_parts[1];
					}
					else $lastsync_fin_interval = 60;
					
					if($lastsync_fin_time >= $lastsync_time)
					{
						$lastsync_time = $lastsync_fin_time;
						$lastsync_interval = $lastsync_fin_interval;
					}
				}
				//echo "<br>Last Sync TS: " . $lastsync_time;
				//echo "<br>Last Sync Interval: " . $lastsync_interval;
				//$lastsync_ts = create_timestamp_from_datetime($lastsync_read['value']);
				
				$elapsed_sync = time() - $lastsync_time * 1;
				if($elapsed_sync >= 60)
				{
					if($elapsed_sync >= 60 * 60 * 24)
					{
						$days_ago = floor($elapsed_sync / (60 * 60 * 24));
						$time_ago = $days_ago . " day";
						if($days_ago > 1) $time_ago .= "s";
						$time_ago .= " ago";
					}
					else if($elapsed_sync >= 60 * 60)
					{
						$hours_ago = floor($elapsed_sync / (60 * 60));
						$time_ago = $hours_ago . " hour";
						if($hours_ago > 1) $time_ago .= "s";
						$time_ago .= " ago";
					}
					else
					{
						$minutes_ago = floor($elapsed_sync / 60);
						$time_ago = $minutes_ago . " minute";
						if($minutes_ago > 1) $time_ago .= "s";
						$time_ago .= " ago";
					}
					echo "Last Time Synced: " . date("Y-m-d h:i:s a",$lastsync_read['value']) . " (".$time_ago.")";
				}
				else
				{
					$until_next_sync = ($lastsync_interval - $elapsed_sync);
					if($until_next_sync < 0) $until_next_sync = 0;
					$next_sync_id = "next_sync_" . $locserv_read['id'];
					$sync_left_display = "display_" . $next_sync_id;
					$sync_left_var = "var_" . $next_sync_id;
					$sync_left_function = "function_" . $next_sync_id;
					echo "<table cellspacing=0 cellpadding=0><tr><td style='color:#000088'>Next Sync Expected:&nbsp;</td><td><div style='color:#000088' id='$sync_left_display'><b>" . $until_next_sync . "</b></div></td><td style='color:#000088'>&nbsp;seconds</td></tr></table>";
					echo "<script language='javascript'>";
					echo " $sync_left_var = ".$until_next_sync."; ";
					echo " function ".$sync_left_function."() { ";
					echo "  if($sync_left_var > 0) $sync_left_var = $sync_left_var - 1; ";
					echo "  document.getElementById('$sync_left_display').innerHTML = '<b>' + ".$sync_left_var." + '</b>'; ";
					echo "  if($sync_left_var > 0) { ";
					echo "    setTimeout('".$sync_left_function."()', 1000); ";
					echo "  } ";
					if($fwd_link!="")
					{
						echo "   else { ";
						echo "      document.getElementById('$sync_left_display').innerHTML = '<b>Running Command...</b>'; ";
						echo "      setTimeout('fwd_sync_page()', 2000); ";
						echo "   } ";
						echo " } ";
						echo " function fwd_sync_page() { ";
						echo "    window.location = \"$fwd_link\"; ";
						echo " } ";
					}
					else echo " } ";
					
					echo " setTimeout('".$sync_left_function."()', 1000); ";
					echo "</script>";
				}
			}
		}

		function show_pending_sync_cmds($dataname,$loc_id)
		{
			$pending_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `location`='[2]' and (LEFT(`setting`,9)='sync_cmd:' or `setting`='copy remote file' or `setting`='health check' or `setting`='remove local file' or `setting`='upload signature' or `setting`='read file structure' or `setting`='run local query' or `setting`='sync database' or `setting`='retrieve local file' or (`setting`='retrieved_local_file' and `value5`='write'))", $dataname,$loc_id);
			if(mysqli_num_rows($pending_query))
			{
				echo "<br><table bgcolor='#8888AA' cellpadding=4>";
				echo "<tr><td bgcolor='#000088' style='color:#ffffff' align='center' colspan='3'>Pending Actions</td></tr>";
				while($pending_read = mysqli_fetch_assoc($pending_query))
				{
					if($pending_read['setting']=="retrieved_local_file" && $pending_read['value5']=="write")
					{
						$show_setting = "update retrieved file";
						$show_value = $pending_read['value'];
					}
					else
					{
						$show_setting = $pending_read['setting'];
						$show_value = $pending_read['value'];
						$show_value_long = $pending_read['value_long'];
					}
					echo "<tr><td bgcolor='#ffffff'>".$show_setting."</td><td bgcolor='#ffffff'>".$show_value."</td><td bgcolor='#ffffff'>".$show_value_long."</td></tr>";
				}
				echo "</table>";
			}
		}

		function countdown_till_command($description)
		{
			global $dataname;
			global $loc_id;
			global $submode;
			global $rowid;
			
			echo "<br><br><b>" . $description . "</b><br>";
			show_time_till_next_sync($dataname,$loc_id,"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$loc_id");
			
			exit();
		}
		
		function execute_sync_cmd($dataname,$loc_id,$cmd)
		{
			if($cmd=='remote_control')
			{
				$set_remote_control = urlvar("set_remote_control","");
				
				schedule_msync($dataname,$loc_id,"sync_cmd:remote control",$set_remote_control);
				$desc = "Set Remote Control Mode: ";
				if($set_remote_control==1) $desc .= "On"; else $desc .= "Off";
				
				countdown_till_command($desc);
			}
		}
		
		$loc_id = $_GET['locid'];
		$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$rowid);
		if(mysqli_num_rows($cust_query))
		{
			$cust_read = mysqli_fetch_assoc($cust_query);
			
			$dataname = $cust_read['data_name'];
			$kart_component_present = false;
			$comp_present_query = mlavu_query("select id from `poslavu_[1]_db`.`locations` where `component_package`=1",$cust_read['data_name']);
			if(mysqli_num_rows($comp_present_query))
			{
				$kart_component_present = true;
			}
			
			if(isset($_GET['cmd']))
			{
				execute_sync_cmd($dataname,$loc_id,$_GET['cmd']);
			}
			
			$basepath = "n/a";
			$paths = array();
			$locserv_query = mlavu_query("select * from `poslavu_[1]_db`.`config` where `setting`='local_file_structure'", $cust_read['data_name']);
			while($locserv_read = mysqli_fetch_assoc($locserv_query))
			{
				//echo "<br><b>Local Server File Structure - Location " . $locserv_read['location'] . "</b> ";
				echo "<br><br>";
				echo "<table><tr><td valign='top'>";
				echo "<b>Local Server Management</b> ";
				echo "<a href='?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$loc_id'>(refresh)</a>";
				//echo "<br>Ip Address: " . $locserv_read['value'];
				//echo "<br>Last Read: " . $locserv_read['value2'];
				echo "</td><td width='25'>&nbsp;</td><td valign='top'>";
				echo show_time_till_next_sync($dataname,$loc_id);
				echo "</td></tr></table>";
					
				if($lastsync_interval > 10) $ls_management = false;
				else $ls_management = true;
				
				if($ls_management)
				{
					echo "<br><input type='button' value='Turn off Remote Control mode' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$loc_id&cmd=remote_control&set_remote_control=0\"' />";
				}
				else
				{
					echo "<br><input type='button' value='Turn on Remote Control mode' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&locid=$loc_id&cmd=remote_control&set_remote_control=1\"' />";
				}

				echo show_pending_sync_cmds($dataname,$loc_id);
			}
		}
	}
?>
