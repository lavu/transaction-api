<?php
	function get_pay_info($pay_info,$type,$def="")
	{
		return (isset($pay_info[$type]))?$pay_info[$type]:$def;
	}
	
	function show_col($val)
	{
		if($val=="") $val = "&nbsp;";
		return "<td valign='top'>$val</td>";
	}
		
	function parse_payment_response($response)
	{
		$vars = array();
		$response = explode("\n",$response);
		for($i=0; $i<count($response); $i++)
		{
			$line = explode("=",$response[$i]);
			if(count($line) > 1)
			{
				$vars[trim($line[0])] = trim($line[1]);
			}
		}
		return $vars;
	}
	
	function fill_response_fields($response_id=false,$fill_field="")
	{
		$str = "";
		if($response_id=="fill_field")
		{
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` order by id desc");
			$response_id = false;
		}
		else if($response_id)
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `id`='[1]'",$response_id);
		else
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where (`x_response_code`='' and `x_type`='') or `datetime`='' order by id desc");

		while($response_read = mysqli_fetch_assoc($response_query))
		{
			$response_text = $response_read['response'];
			$datetime = trim($response_read['date'] . " " . $response_read['time']);
			$ts = $response_read['ts'];
			$pay_info = parse_payment_response($response_text);
			//set_pay_info($response_text);
			
			$x_description = get_pay_info($pay_info,"x_description");
			$x_amount = get_pay_info($pay_info,"x_amount");
			$x_type = get_pay_info($pay_info,"x_type");
			$x_subscriptionid = get_pay_info($pay_info,"x_subscription_id");
			
			$x_first_name = get_pay_info($pay_info,"x_first_name");
			$x_last_name = get_pay_info($pay_info,"x_last_name");
			$x_company = get_pay_info($pay_info,"x_company");
			$x_city = get_pay_info($pay_info,"x_city");
			$x_state = get_pay_info($pay_info,"x_state");
			$x_phone = get_pay_info($pay_info,"x_phone");
			$x_email = get_pay_info($pay_info,"x_email");
			
			$x_trans_id = get_pay_info($pay_info,"x_trans_id");
			
			$x_account_number = get_pay_info($pay_info,"x_account_number");
			$x_card_type = get_pay_info($pay_info,"x_card_type");
			$x_response_code = get_pay_info($pay_info,"x_response_code");
			if($x_response_code=="1") $x_show_response = "<font color='#008800'>accepted</font>";
			else $x_show_response = "<font color='#880000'>declined</font>";
			
			$flist = array();
			if($fill_field!="")
			{
				$flist[$fill_field] = get_pay_info($pay_info,$fill_field);
			}
			else
			{
				$flist["x_description"] = $x_description;
				$flist["x_amount"] = $x_amount;
				$flist["x_type"] = $x_type;
				$flist["x_subscriptionid"] = $x_subscriptionid;
				$flist["x_first_name"] = $x_first_name;
				$flist["x_last_name"] = $x_last_name;
				$flist["x_company"] = $x_company;
				$flist["x_city"] = $x_city;
				$flist["x_state"] = $x_state;
				$flist["x_phone"] = $x_phone;
				$flist["x_email"] = $x_email;
				$flist["x_trans_id"] = $x_trans_id;
				$flist["x_account_number"] = $x_account_number;
				$flist["x_card_type"] = $x_card_type;
				$flist["x_response_code"] = $x_response_code;
				$flist["datetime"] = $datetime;
			}
			
			$update_code = "";
			foreach($flist as $key => $val)
			{
				if($update_code!="") $update_code .= ",";
				$update_code .= "`$key`='[$key]'";
			}
			$flist["id"] = $response_read['id'];
			$success = mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set " . $update_code . " where `id`='[id]'",$flist);
			$str .= "updated payment response data " . $response_read['id'];
			if($success) 
			{
 				$str .= "(success)"; 
			}
			else 
			{
				$str .= "(failed) <br>$update_code";
			}
			$str .= "<br>";
		}
		return $str;
	}
	
	function get_restaurant_data($dataname,$field="")
	{
		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			if($field!="")
				return $rest_read[$field];
			else
				return $rest_read;
		}
		else return false;
	}
	
	function get_restaurant_id($dataname)
	{
		return get_restaurant_data($dataname,"id");
	}
	
	function get_billing_profile_signup($res_read)
	{
		$bprof_query = mlavu_query("select * from `poslavu_MAIN_db`.`billing_profiles` where `subscriptionid`='[1]'",$res_read['x_subscriptionid']);
		if(mysqli_num_rows($bprof_query))
		{
			$bprof_read = mysqli_fetch_assoc($bprof_query);
			mlavu_query("update `poslavu_MAIN_db`.`billing_profiles` set `last_pay_date`='[1]' where `id`='[2]'",date("Y-m-d"),$bprof_read['id']);
			
			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where (`dataname`='[1]' and `dataname`!='') or (`restaurantid`='[2]' and `restaurantid`!='')",$bprof_read['dataname'],$bprof_read['restaurantid']);
			if(mysqli_num_rows($signup_query))
			{
				$signup_read = mysqli_fetch_assoc($signup_query);
				$find_method = "match";
				
				$signup_read['find_method'] = $find_method;
				return $signup_read;
			}
			else return false;	
		}
		else return false;
	}
	
	function get_subscription_signup($res_read)
	{
		$signup_read = array();
		$find_method = "no";
		
		$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `arb_license_id`='[1]' or `arb_hosting_id`='[1]'",$res_read['x_subscriptionid']);
		if(mysqli_num_rows($signup_query))
		{
			$signup_read = mysqli_fetch_assoc($signup_query);
			$find_method = "match";
		}
		else
		{
			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `firstname`='[1]' and `lastname`='[2]' and `city`='[3]' and `city`!='' and `firstname`!=''",$res_read['x_first_name'],$res_read['x_last_name'],$res_read['x_city']);
			if(mysqli_num_rows($signup_query))
			{
				$signup_read = mysqli_fetch_assoc($signup_query);
				$find_method = "name_city";
			}
		}
		
		$signup_read['find_method'] = $find_method;
		return $signup_read;
	}

	function set_response_restaurant_id($response_id,$rest_id,$match_type)
	{
		$success = mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `match_restaurantid`='[1]', `match_type`='[2]' where `id`='[3]'",$rest_id,$match_type,$response_id);
		return $success;
	}
	
	function get_payment_request_info($payment_request_id)
	{
		$payreq_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_request` where `id`='[1]'",$payment_request_id);
		if(mysqli_num_rows($payreq_query))
		{
			$payreq_read = mysqli_fetch_assoc($payreq_query);
			return $payreq_read;
		}
		else return false;
	}
	
	function get_payment_request_rest_id($payment_request_id)
	{
		$payreq_read = get_payment_request_info($payment_request_id);
		if($payreq_read['set_restid']!="" && is_numeric($payreq_read['set_restid']))
			return $payreq_read['set_restid'];
		else
			return $payreq_read['restaurantid'];
	}
	
	function get_signup_by_name_and_city($res_read)
	{
		$s_query_string = "select * from `poslavu_MAIN_db`.`signups` where `status`!='manual' and `firstname`='[firstname]' and `lastname`='[lastname]' and `city`='[city]' and `city`!='' and `firstname`!=''";
		$a_query_vars = array('firstname'=>$res_read['x_first_name'], 'lastname'=>$res_read['x_last_name'], 'city'=>$res_read['x_city']);
		$signup_query = mlavu_query($s_query_string, $a_query_vars);
		
		if ($signup_query === FALSE) {
			foreach($a_query_vars as $k=>$v)
				$s_query_string = str_replace("[$k]", $v, $s_query_string);
			error_log($s_query_string);
		}
		
		if ($signup_query) {
			if(mysqli_num_rows($signup_query))
			{
				$signup_read = mysqli_fetch_assoc($signup_query);
				return $signup_read;
			}
			else return FALSE;
		} else {
			error_log("presponse_functions: ".mlavu_dberror());
			return FALSE;
		}
	}

	function get_signup_by_id($signup_id)
	{
		$s_query_string = "select * from `poslavu_MAIN_db`.`signups` where `id` != '' AND `id` = '[signup_id]'";
		$a_query_vars = array('signup_id'=>$signup_id);
		$signup_query = mlavu_query($s_query_string, $a_query_vars);
		
		if ($signup_query === FALSE) {
			foreach($a_query_vars as $k=>$v)
				$s_query_string = str_replace("[$k]", $v, $s_query_string);
			error_log("presponse_functions: ".mlavu_dberror() ." ". $s_query_string);
		}
		else if (mysqli_num_rows($signup_query))
		{
			$signup_read = mysqli_fetch_assoc($signup_query);
			return $signup_read;
		}

		return false;
	}
	
	function get_response_by_name_and_card($res_read)
	{
		$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_type`!='manual' and `match_restaurantid`!='' and `x_account_number`!='' and `x_first_name`!='' and `x_last_name`!='' and `x_account_number`='[1]' and `x_first_name`='[2]' and `x_last_name`='[3]'",$res_read['x_account_number'],$res_read['x_first_name'],$res_read['x_last_name']);
		if(mysqli_num_rows($response_query))
		{
			$response_read = mysqli_fetch_assoc($response_query);
			return $response_read;
		}
		else return false;
	}

	function get_response_by_name_and_company($res_read)
	{
		$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_type`!='manual' and `match_restaurantid`!='' and `x_company`!='' and `x_first_name`!='' and `x_last_name`!='' and `x_company`='[1]' and `x_first_name`='[2]' and `x_last_name`='[3]'",$res_read['x_company'],$res_read['x_first_name'],$res_read['x_last_name']);
		if(mysqli_num_rows($response_query))
		{
			$response_read = mysqli_fetch_assoc($response_query);
			return $response_read;
		}
		else return false;
	}
	
	function connect_response_to_restaurant($response_id)
	{
		$rest_id = false;
		$signup_read = false;
		$find_method = "";
		$str = "";

		$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `id`='[1]'",$response_id);
		if(mysqli_num_rows($response_query))
		{
			$res_read = mysqli_fetch_assoc($response_query);
		
			if($res_read['x_type']=="auth_capture")
			{
				if(!$rest_id && substr($res_read['x_description'],0,17)=="POSLavu Payment L") // moved from line 309 (from just beneath marker 1) - CF
				{
					$payment_request_id = substr($res_read['x_description'],17);
					$rest_id = get_payment_request_rest_id($payment_request_id);
					$find_method = "payment request match";
				}			
				
				if(!$rest_id && substr($res_read['x_description'],0,14)=="POSLavu Auth R")
				{
					$rest_id = substr($res_read['x_description'],14);
					$find_method = "payment collect match";
				} // end moved block (from just beneath marker 1) - CF

				if(!$rest_id && substr($res_read['x_description'],0,14)=="POSLavu Auth S")
				{
					$signup_id = substr($res_read['x_description'],14);
					$signup_read = get_signup_by_id($signup_id);
					if ($signup_read && !empty($signup_read['dataname']))
						$rest_id = get_restaurant_id($signup_read['dataname']);
					if ($rest_id)
						$find_method = "payment signup match";
				}
									
				if($find_method=="" && $res_read['x_subscriptionid']!="")
				{
					$signup_read = get_billing_profile_signup($res_read);
					if($signup_read)
						$find_method = "billing profile match";
				}
				
				if($find_method=="" && $res_read['x_subscriptionid']!="")
				{
					$signup_read = get_subscription_signup($res_read);
					if($signup_read)
						$find_method = "subscription match";
				}
				
				if($find_method=="" && $res_read['x_city']!="")
				{
					$signup_read = get_signup_by_name_and_city($res_read);
					if($signup_read)
						$find_method = "name city match";
				}
									
				if(!$rest_id && $find_method!="" && is_array($signup_read) && isset($signup_read['dataname']))
				{
					$rest_id = get_restaurant_id($signup_read['dataname']);
				}
				
				// marker 1 - moved some stuff from here (its above) - CF
				
				if(!$rest_id && $res_read['x_account_number']!="" && $res_read['x_first_name']!="" && $res_read['x_last_name']!="")
				{
					$find_method = "";
					$match_read = get_response_by_name_and_card($res_read);
					if($match_read && is_array($match_read) && $match_read['match_restaurantid']!="")
					{
						$rest_id = $match_read['match_restaurantid'];
						$find_method = "name card match";
					}
				}
		
				if(!$rest_id && $res_read['x_company']!="" && $res_read['x_first_name']!="" && $res_read['x_last_name']!="")
				{
					$find_method = "";
					$match_read = get_response_by_name_and_company($res_read);
					if($match_read && is_array($match_read) && $match_read['match_restaurantid']!="")
					{
						$rest_id = $match_read['match_restaurantid'];
						$find_method = "name company match";
					}
				}
					
				if($rest_id && $find_method!="")
				{
					$success = set_response_restaurant_id($res_read['id'],$rest_id,$find_method);
					$str .= "found signup ($find_method): " . $res_read['datetime'] . " - " . $res_read['x_amount'] . " - " . $res_read['x_description'] . " - " . $res_read['x_first_name'] . " - " . $res_read['x_last_name']. " " . $res_read['x_city'] . "<br>";
				}
				else if($res_read['x_amount'] < 5)
				{
					$success = set_response_restaurant_id($res_read['id'],0,"small transaction");
					$str .= "small transaction: " . $res_read['datetime'] . " - " . $res_read['x_amount'] . "<br>";
				}
			}
			else if($res_read['x_type']=="credit")
			{
				$prev_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_amount`>='[x_amount]' and `x_card_type`='[x_card_type]' and `x_account_number`='[x_account_number]' and `ts`<'[ts]' and `x_first_name`='[x_first_name]' and `x_last_name`='[x_last_name]' and `match_restaurantid`!='' order by x_amount asc",$res_read);
				if(mysqli_num_rows($prev_query))
				{
					$prev_read = mysqli_fetch_assoc($prev_query);
					$rest_id = $prev_read['match_restaurantid'];
					
					if($rest_id)
					{
						$success = set_response_restaurant_id($res_read['id'],$rest_id,"refund match");
						$str .= "FOUND MATCH - ".$res_read['x_amount']." " . $prev_read['match_restaurantid'] . "<br>";
					}
				}
			}
		}
		
		return $str;
	}
?>
