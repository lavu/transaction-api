<?php

// For details related to Lavu's logging system, refer to http://wiki.lavu/Server_Logs

require_once(__DIR__."/../cp/resources/core_functions.php");

ini_set('memory_limit','64M');

$lc			= explode(":", lc_decode($_REQUEST['lc']));
$dataname	= reqvar("dn", FALSE);

if ($lc[0]!="cloud_connect" || $lc[1]!=$dataname || $lc[2]!=date("Ymd"))
{
	echo "Error: request validation failed...";
	exit();
}

$type		= reqvar("t", FALSE);
$date		= reqvar("d", FALSE);
$search_for	= reqvar("s", "");
$loc_id		= reqvar("l", "0");
$dcf		= reqvar("dcf", FALSE);

$displaying_certain_file = (($type=="device_console" || $type=="gateway_debug") && $dcf);

if (!$dataname || !$type || (!$date && !$displaying_certain_file))
{
	echo "Error: incomplete request...";
	exit();
}

$base_path = "/home/poslavu/logs/company/".$dataname;

// Determine whether or not the log should be found in the operational or debug directory
switch ($type)
{
	case "gateway":
	case "gateway_vars":
	case "json_vars":
	case "mysql":

		$base_path .= "/operational";
		break;

	case "device_console":
	case "sync_conflicts":

		$base_path .= "/debug";
		break;

	case "gateway_debug":

		$base_path .= "/debug/gateway_debug";
		break;

	default:
		break;
}

if ($dcf) // Single file retrieval only for Device Console or Gateway Debug logs
{
	if (file_exists($base_path."/".$dcf))
	{
		echo "<a style='cursor:pointer; color:#0033CC;' onclick='history.go(-1);'><< Back to list</a><br><br>";

		$log_file = fopen($base_path."/".$dcf, "r");
		while (($log_line = fgets($log_file, 4096)) !== FALSE)
		{
			echo nl2br(str_replace("<", "&lt;", $log_line));
		}

		if (!feof($log_file))
		{
			echo "Error: unexpected fgets() fail\n";
		}

		fclose($log_file);

		echo "<br><br>";
	}
	else
	{
		echo "Error: cannot find ".$dcf."<br><br>";
	}

	echo "<a style='cursor:pointer; color:#0033CC;' onclick='history.go(-1);'><< Back to list</a><br><br>";

	exit();
}

switch ($type)
{
	case "error":

		$has_lines		= FALSE;
		$error_log_path	= "/home/poslavu/logs/error_log_tail";

		if (file_exists($error_log_path))
		{
			$lines = file($error_log_path);
			if (is_array($lines))
			{
				foreach ($lines as $line)
				{
					if ($search_for=="" || strstr($line, $search_for) || substr($line, 0, 21)=="ERROR WATCH LAST RAN:")
					{
						$has_lines = TRUE;
						echo $line."\n";
					}
				}
			}
		}

		if (!$has_lines)
		{
			echo "No error log tail to retrieve\n";
		}

		break;

	case "device_console":

		if ($dir = opendir($base_path))
		{
			$found_match	= FALSE;
			$t				= 0;
			$f				= 0;
			$file_list		= array( 'name' => array(), 'time' => array() );

			while (FALSE !== ($file = readdir($dir)))
			{
				if (strstr($file, "device_console") && strstr($file, str_replace("-", "", $date)))
				{
					$found_match = TRUE;
					$file_list['time'][$t++] = filemtime($base_path."/".$file);
					$file_list['name'][$f++] = $file;
				}
			}

			closedir($dir);
			asort($file_list['time']);
			asort($file_list['name']);

			if ($found_match)
			{
				foreach ($file_list['time'] as $key => $ftime)
				{
					$filename	= $file_list['name'][$key];
					$File		= escapeshellarg($base_path."/".$filename);
					$header		= `head -4 $File`;
					$first_line	= `head -7 $File | tail -n 1`;
					$last_line	= `tail -n 1 $File`;

					$first_time	= substr($first_line, 0, 19);
					$last_time	= substr($last_line, 0, 19);

					echo $filename."|***|".$first_time."|***|".$last_time."|***|".trim(str_replace("\n", " &nbsp;&nbsp; ", $header), " &nbsp;")."\n";
				}
			}
			else
			{
				echo "No device console logs found for ".$date."...";
			}
		}
		else
		{
			echo "Error: unable to open directory - ".$base_path." (".$dataname.")...";
			if (!is_dir($base_path))
			{
				mkdir($base_path, 0755, TRUE);
			}
		}

		break;

	case "gateway_debug":

		if ($dir = opendir($base_path))
		{
			$found_match	= FALSE;
			$file_list		= array();
			$test_list		= array();
			$search_for_loc	= ($loc_id != 0)?$loc_id."_":"";

			while (FALSE !== ($file = readdir($dir)))
			{
				$test_list[] = $file;
				$test_list[] = array( $date, strstr($file, str_replace("-", "", $date)), $loc_id, $search_for_loc, (substr($file, 0, (strlen((string)$loc_id) + 1))==$search_for_loc) );
				if (strstr($file, str_replace("-", "", $date)) && ($loc_id=="0" || substr($file, 0, (strlen((string)$loc_id) + 1))==$search_for_loc))
				{
					$found_match = TRUE;
					$file_list[] = $file;
				}
			}

			sort($file_list);
			closedir($dir);

			if ($found_match)
			{
				for ($f = (count($file_list) - 1); $f >= 0; $f--)
				{
					echo $file_list[$f]."\n";
				}
			}
			else
			{
				echo "No gateway debug logs found for ".$date."...";
			}
		}
		else
		{
			echo "Error: unable to open directory - ".$base_path." (".$dataname.")...";
			if (!is_dir($base_path))
			{
				mkdir($base_path, 0755, TRUE);
			}
		}

		break;

	default:

		$search_for_loc	= ($loc_id != 0)?"LOCATION: ".$loc_id:"";
		$file_name		= $type."_".$date.".log";
		$full_path		= $base_path."/".$file_name;

		if (!file_exists($full_path) && strstr($base_path, "/operational"))
		{ // Attempt to copy the operational log file back over from the log server at Lavu HQ
			copyLogFileFromArchive($file_name, $full_path, $dataname);
		}

		if (file_exists($full_path))
		{
			$temp_string = "";

			$log_file = fopen($full_path, "r");
			while (($log_line = fgets($log_file, 4096)) !== FALSE)
			{
				if ((($search_for == "") || strstr($log_line, $search_for)) && (($search_for_loc == "") || strstr($log_line, $search_for_loc)))
				{
					$temp_string .= $log_line;
				}
			}

			if (!feof($log_file))
			{
				echo "Error: unexpected fgets() fail";
			}
			else
			{
				echo $temp_string;
			}

			fclose($log_file);
		}
		else
		{
			echo "Error: file not found...";
		}

		break;
}

function copyLogFileFromArchive($file_name, $full_path, $dataname)
{
	$url	= "http://gman.lavu.com:5288/grab_operational_log_file.php";
	$vars	= "file_name=".$file_name."&dataname=".$dataname."&web_container=".gethostname();

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);

	$response = curl_exec($ch);

	if (empty($response))
	{
		return;
	}

	if (substr($response, 0, 8) == "Error - ")
	{
		error_log(__FUNCTION__." - Failed to retrieve ".$file_name." - ".$response);
		return;
	}

	$log_file = fopen($full_path, "w");
	if ($log_file)
	{
		error_log("writing to file ".$full_path);
		fputs($log_file, $response);
		fclose($log_file);
	}
}