<?php

/**
 *
 * This page uses wkhtmltopdf to create a PDF of invoice.php, effectively generating an invoice PDF.
 *
 * It works by passing through the dataname and invoice ID to invoice.php while calling wkhtmltopdf on it and then displaying the resulting PDF.
 * 
 * This was all based on the Activation Code PDF for BlueStar Activation Codes (DEV branch LavuBackend/bluestar/pdf.php).
 *
 */

$debug = (strpos($_SERVER["SERVER_NAME"], 'localhost') !== false);

/** Entry Point Code **/

$pdfPath = writePdf($_REQUEST);
debugLog( "pdfPath={$pdfPath}" );  //debug
if ( !empty($pdfPath) ) outputPdf($pdfPath);
exit;


/** Functions **/

function debugLog($msg)
{
	global $debug;

	if ( $debug ) error_log($msg);
}

function logMsg($msg)
{
	error_log($msg);
}

function writePdf($args)
{
	$dataname = isset($args['dataname']) ? $args['dataname'] : '';
	$invoiceid = isset($args['invoiceid']) ? $args['invoiceid'] : '';

	$multipleInvoicesDetected = (strpos($invoiceid, '|') !== false);

	// Make sure required arguments are present.
	if ( empty($invoiceid) ) die(__FILE__ ." received empty invoiceid!");
	if ( empty($dataname) && !$multipleInvoicesDetected ) die(__FILE__ ." received empty dataname!");

	// Strip the invoice number prefix of "77-" that will be present from Invoice PDF links in CP/V2.
	if ( substr($invoiceid, 0, 3) === '77-' ) $invoiceid = substr($invoiceid, 3);

	// Build URL for web page to convert to PDF using wkpdftohtml.
	// (Did things this way because I didn't think wkhtmltopdf accepted relative URLs.)
	$http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
	$host = $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
	$url = "{$http}://{$host}/sa_cp/billing/invoice.php?dataname={$dataname}&invoiceid={$invoiceid}";

	//debugLog("url={$url}");
error_log(__FILE__ ." DEBUG: url={$url}");  //debug 2015-03-17

	$pdfDir = '/tmp/invoicepdfs';
	$pdfName = "{$dataname}-invoice-{$invoiceid}.pdf";
	if ( $multipleInvoicesDetected ) {
		$invoiceids = explode('|', $invoiceid);
		$first_invoiceid = $invoiceids[0];
		$last_invoiceid = $invoiceids[ count($invoiceids) - 1 ];
		$pdfName = "{$dataname}-invoice-{$first_invoiceid}_{$last_invoiceid}.pdf";
	}
	$pdfPath = "{$pdfDir}/{$pdfName}";

	//debugLog( "pdfPath={$pdfPath}" );  //debug

	// Bail out if the PDF already exists - in part to guard against infinite loop stuff from happening again.
	if ( file_exists( $pdfPath ) ) {
		//NOTE : we could return $pdfPath right here to avoid regenerating an existing invoice
	}

	//debugLog( "file_exists({$pdfPath})=". file_exists( $pdfPath ) );  //debug

	// Create the dir to house the PDF, if it doesn't already exist.
	if ( !file_exists( $pdfDir ) ) {
		if ( !mkdir( $pdfDir, 0775, true ) ) {
			die("Couldn't create directory: $pdfDir");
		}
	}

	//debugLog( "file_exists({$pdfDir})=". file_exists( $pdfDir ) );  //debug

	// Create the waiver PDF using the command-line tool wkhtmltopdf.
	$cmd = "/usr/local/bin/wkhtmltopdf \"{$url}\" {$pdfPath}";  // Use the full path to wkhtmltopdf to avoid hard-to-track-down problems.
	$output = array();

	//debugLog( 'DEBUG: cmd='. $cmd );  // debug

	exec( $cmd, $output );  // using lavuExec() here breaks wkhtmltopdf because it escapes the '?' character needed for the URL querystring

	//debugLog( "DEBUG: ". print_r( $output, 1 ) );  // debug

	// Return the path to the PDF if it was successfully created.
	return (file_exists( $pdfPath )) ? $pdfPath : '';
}

function outputPdf($pdfPath)
{
	if (file_exists($pdfPath))
	{
		$baseName = basename($pdfPath);
		$fileSize = filesize($pdfPath);

	    header('Content-Description: File Transfer');
	    header('Content-Type: application/pdf');
	    header("Content-Disposition: inline; filename={$baseName}");
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
	    header("Content-Length: {$fileSize}");

	    //ob_clean();  // never called ob_start() so was receiving PHP Notice from this line.
	    flush();
	    readfile($pdfPath);
	}
}

?>
