<?php

// Display Usage Info - if they didn't supply the correct command line paramters

if ( $argc < 3 || in_array( $argv[1], array( '-h', '-?', '--help', '-help' ) ) ) {
	echo <<<USAGE
Usage: php {$argv[0]} [RUNDATE] [DATANAME]...
Run the customers_billing process to create ARB invoices for the supplied list of datanames.

RUNDATE can be formatted as 'yyyy-mm-dd' or 'today' - or anything else recognized by PHP's strtotime().

DATANAME is a space delimited list of one or more lower-case datanames.

USAGE;

	exit;
}


// Setup

$dev = strstr(dirname(__FILE__), '/dev/') ? 'dev/' : '';

require_once( "/home/poslavu/public_html/admin/{$dev}components/fitness/RecurringBilling.php" );


// Create invoices

$runDate = strtotime( $argv[1] );
$runDateStr = date('Y-m-d', $runDate);
$targetDayOfMonth = date('j', $runDate);
$targetDayOfWeek = date('N', $runDate);

if ( $runDateStr == '1969-12-31' ) {
	echo "Invalid date specified\n";
	exit;
}


echo "Creating customers_billing ARB invoices for runDate={$runDateStr} (targetDayOfMonth={$targetDayOfMonth} targetDayOfWeek={$targetDayOfWeek})\n";  // debug

$arbUtil = new RecurringBilling();
$invoicesCreated = array();
$invoicesErrors = array();
$argv_size = count( $argv );

for ($i = 2; $i < $argv_size; $i++)
{
	$dataname = strtolower( $argv[$i] );

	echo "Processing {$dataname}\n";  // debug

	// Connect to that dataname's database.
	// (Note: assumes they have a database user named after their dataname!)
	lavu_connect_dn($dataname, "poslavu_{$dataname}_db");

	// Get list of ARBs who's invoice day of the month is today:
	// * bill_frequency = monthly, bill_event_offset = <today's date of the month>
	// * bill_frequency = weekly, bill_event_offset = <today's day number of the week>
	// * bill_frequency = bi-weekly, bill_event_offset = <today's day number of the week>
	$arbQuery = lavu_query("select * from `med_billing` where status = 'active' and ((`bill_frequency` = 'monthly' and bill_event_offset = [targetDayOfMonth]) or (`bill_frequency` = 'weekly' and bill_event_offset = [targetDayOfWeek]))", array('targetDayOfMonth' => $targetDayOfMonth, 'targetDayOfWeek' => $targetDayOfWeek));
	if ($arbQuery === FALSE) die(__FILE__ . " couldn't get ARBs for dataname={$dataname}:". lavu_dberror());

	///echo "mysqli_num_rows(arbQuery)=" . mysqli_num_rows($arbQuery) ."\n";  // debug

	if (mysqli_num_rows($arbQuery) == 0) {
		echo "...none found.\n";
		continue;
	}

	while ($arb = mysqli_fetch_assoc($arbQuery)) {
		// Create invoice for this billing profile.
		$invoiceNumber = $arbUtil->createInvoiceForBillingProfile($arb);

		if ($invoiceNumber != '') {
			echo "...created invoice #{$invoiceNumber} for arbId={$arb['id']}\n";

			if (!isset($invoicesCreated[$dataname]))  $invoicesCreated[$dataname] = array();
			$invoicesCreated[$dataname][] = $invoiceNumber;

			// Pay created invoices
			echo "Paying invoices for bill date {$targetDayOfMonth}\n";
			$arbUtil->payInvoice( $invoiceNumber, $arb );
		}
		else {
			echo "...could not create invoice for arbId={$arb['id']}\n";
			$invoicesErrors[$dataname][] = $arb['id'];  // Accumulate the list of arbId's for each dataname that threw errors.
		}
	}
}


// Send email for accumulated errors

if (!empty($invoicesErrors)) {
	mail("tom@lavu.com", "Invoice Creation Errors from ". __FILE__, print_r($invoicesErrors, 1), "From:customers_billing@poslavu.com" );
}

?>
