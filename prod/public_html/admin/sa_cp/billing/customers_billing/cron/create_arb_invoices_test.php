<?php

// Display Usage Info - if they didn't supply the correct command line paramters

if ( $argc < 2 || in_array( $argv[1], array( '-h', '-?', '--help', '-help' ) ) ) {
	echo <<<USAGE
Usage: php {$argv[0]} [DATANAME] [ARBID]
Run the customers_billing process to create ARB invoices for the supplied list of ARB IDs.

DATANAME is the lower-case dataname.

ARBID is an integer representing the med_billing table ID of the ARB.

USAGE;

	exit;
}


// Setup

$dev = strstr(dirname(__FILE__), '/dev/') ? 'dev/' : '';

require_once( "/home/poslavu/public_html/admin/{$dev}components/fitness/RecurringBilling.php" );


// Create invoices

$dataname = $argv[1];
$arbId = $argv[2];

echo "Creating customers_billing ARB invoices for dataname={$dataname} ARB={$arbId}\n";

$arbUtil = new RecurringBilling();
$invoicesCreated = array();
$invoicesErrors = array();
$argv_size = count( $argv );

echo "Processing {$dataname}\n";  // debug

// Connect to that dataname's database.
// (Note: assumes they have a database user named after their dataname!)
lavu_connect_dn($dataname, "poslavu_{$dataname}_db");

// Create invoice for this billing profile.
$arbUtil->createInvoicesForBillingProfileId($arbId);

echo "...created invoice #{$invoiceNumber} for arbId={$arb['id']}\n";

if (!isset($invoicesCreated[$dataname]))  $invoicesCreated[$dataname] = array();
$invoicesCreated[$dataname][] = $invoiceNumber;

//// Pay created invoices
//echo "Paying invoices for bill date {$targetDayOfMonth}\n";
//$arbUtil->payInvoice( $invoiceNumber, $arb );


// Send email for accumulated errors

if (!empty($invoicesErrors)) {
	mail("tom@lavu.com", "Invoice Creation Errors from ". __FILE__, print_r($invoicesErrors, 1), "From:customers_billing@poslavu.com" );
}

?>
