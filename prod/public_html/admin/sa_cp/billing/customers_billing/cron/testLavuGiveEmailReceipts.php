<?php

// Display Usage Info - if they didn't supply the correct command line paramters

if ( $argc < 2 || in_array( $argv[1], array( '-h', '-?', '--help', '-help' ) ) ) {
	echo <<<USAGE
Usage: php {$argv[0]} [DATANAME] [ARBID]
Test ARB email receipts by running the customers_billing process to create ARB invoices for the supplied ARB ID and then pay them to trigger email receipts.

DATANAME is the lower-case dataname.

ARBID is an integer representing the med_billing table ID of the ARB.

USAGE;

	exit;
}


// Setup

$dev = strstr(dirname(__FILE__), '/dev/') ? 'dev/' : '';

error_log( __FILE__ ."dev={$dev}");  //debug

require_once( "/home/poslavu/public_html/admin/{$dev}components/tithe/lib/LavuGiveArb.php" );


// Create invoices

$dataname = $argv[1];
$arbId = $argv[2];

echo "Creating customers_billing ARB invoices for dataname={$dataname} ARB={$arbId}\n";

$arbUtil = new LavuGiveArb();
$invoicesCreated = array();
$invoicesErrors = array();
$argv_size = count( $argv );

echo "Processing {$dataname}\n";  // debug

// Connect to that dataname's database.
// (Note: assumes they have a database user named after their dataname!)
lavu_connect_dn($dataname, "poslavu_{$dataname}_db");

// Create invoice for this billing profile.
$invoicesCreated = $arbUtil->createInvoicesForBillingProfileId($arbId);
$invoiceNumber = (isset($invoicesCreated)) ? $invoicesCreated[0] : '';

if ( empty($invoiceNumber) ) {
	die( "...failed to create invoice for {$dataname}\n" );
}

echo "...created invoice #{$invoiceNumber} for {$dataname}\n";

// Pay created invoices
echo "Paying invoices for {$dataname}\n";
$arbUtil->payInvoicesForBillingProfileId( $dataname, $arbId );


// Send email for accumulated errors

if (!empty($invoicesErrors)) {
	mail("tom@lavu.com", "Invoice Creation Errors from ". __FILE__, print_r($invoicesErrors, 1), "From:customers_billing@poslavu.com" );
}

?>
