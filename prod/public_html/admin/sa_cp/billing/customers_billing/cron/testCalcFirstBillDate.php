<?php

// Display Usage Info - if they didn't supply the correct command line paramters

if ( $argc < 5 || $argc > 6 || in_array( $argv[1], array( '-h', '-?', '--help', '-help' ) ) ) {
	echo <<<USAGE
Usage: php {$argv[0]} [DATANAME] [REFDATE] [STARTDATE] [BILLFREQUENCY] [BILLEVENTOFFSET]
Test the RecurringBilling function calculateNextBillDate() with the supplied command-line arguments.

DATANAME in lower-case.

REFDATE is the date from which the next bill date should be calcuated - in the YYYY-mm-dd format.

STARTDATE is the ARB start date - in the YYYY-mm-dd format.

BILLFREQUENCY should be daily|monthly|weekly|bi-weekly|yearly.

BILLEVENTOFFSET is an integer representing the day of the month for BILLFREQUENCY=monthly|yearly or the day of the week for BILLFREQUENCY=weekly|bi-weekly.

USAGE;

	exit;
}


$dev = strstr(dirname(__FILE__), '/dev/') ? 'dev/' : '';

require_once( "/home/poslavu/public_html/admin/{$dev}components/fitness/RecurringBilling.php" );

$dataname = $argv[1];

lavu_connect_dn( $dataname, "poslavu_{$dataname}_db" );

$arbUtil = new RecurringBilling();
$refDate = $argv[2];
$startDate = $argv[3];
$billFrequency = $argv[4];
$billEventOffset = $argv[5];
$nextBillDate = $arbUtil->calculateNextBillDate( $refDate, $startDate, $billFrequency, $billEventOffset );

echo "{$nextBillDate}\n";

?>