<?php
/**
 *
 * This page uses Zuora Integration to retrieve a Zuora-hosted invoice PDF, save it to the Lavu web sefver file system, and send it back to the user for displaying.
 *
 * It works by using the Zuora SOAP API to get the Base64 encoded binary contents of the PDF and then sending that data back to the browser, which interprets it as a PDF.
 * 
 * This was all based on the Activation Code PDF for BlueStar Activation Codes (DEV branch LavuBackend/bluestar/pdf.php).
 *
 */

// Check for required args: dataname, zai (Zuora AccountId) and zii (Zuora InvoiceId)
if ( empty($_REQUEST['dataname']) || empty($_REQUEST['zai']) || empty($_REQUEST['zii']) )
{
	error_log(__FILE__ ." accessed without all required request parameters: dataname, zai, zii");
	die();
}

require_once(dirname(__FILE__).'/../../../inc/billing/zuora/ZuoraIntegration.php');

$zInt = new ZuoraIntegration();
$pdf = $zInt->getInvoicePDF(array('zAccountId' => $_REQUEST['zai'], 'zInvoiceId' => $_REQUEST['zii']));
if ( $pdf !== null )
{
	header('Content-Type: application/pdf');
	echo base64_decode($pdf);
}

exit;
