<?php

	//require_once(dirname(__FILE__) . "/../functions.php");
	session_start();
	require_once(dirname(__FILE__)."/../../../cp/resources/lavuquery.php");
	require_once(dirname(__FILE__)."/../../../distro/reseller_licenses_functions.php");
	require_once(dirname(__FILE__)."/../new_process_payments.php");
	require_once(dirname(__file__)."/../payment_profile_functions.php");

	$maindb = "poslavu_MAIN_db";

	require_once(dirname(__FILE__)."/../package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();

	ini_set("display_errors","1");

	/* NOTES

		: possible values for restaurants.disabled :
		1 -
		2 - client disabled it themselves
		3 - automatically disabled no package clients with no activity since 2012-06-01

	*/
	$origin='sa_cp';
	if(isset($_REQUEST['origin']))
		$origin=$_REQUEST['origin'];
	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	if($loggedin=="")
		$loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posadmin_fullname']))?$_SESSION['posadmin_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posadmin_email']))?$_SESSION['posadmin_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;
	if($loggedin_access=="") $loggedin_access = "";
	//exit();

	require_once(dirname(__FILE__)."/../../../manage/can_access.php");

	function account_loggedin()
	{
		global $loggedin;
		return $loggedin;
	}

	if(!account_loggedin())
	{
		exit();
	}
	if(!can_access("customers"))
	{
		echo "You do not have access to this area";
		exit();
	}

	/*--------------------------------------------------------------------------------------------------------------*/
	// BILLING ALERTS

	function show_billing_status_alert() // close duplicate function in /areas/billing.php
	{
		global $p_companyid;
		global $p_dataname;
		global $p_collection_lockout;

		$str = "";
		if(admin_loggedin())
		{
			$show_alert = false;
			$show_trial_alert = false;
			$collection_due_date_message = "";
			$show_trial_message = "";
			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$p_companyid);
			while($status_read = mysqli_fetch_assoc($status_query))
			{
				if($status_read['trial_end']!="" && $status_read['license_applied']=="")
				{
					$show_trial_alert = true;
					$show_trial_message = "";

					$trial_due_parts = explode(" ",$status_read['trial_end']);
					$trial_due_date = $trial_due_parts[0];

					$due_parts = explode("-",$trial_due_date);
					if(count($due_parts) > 2)
					{
						$current_ts = time();
						$due_ts = mktime(0,0,0,$due_parts[1],$due_parts[2],$due_parts[0]);
						$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
						if($diff_days > 1)
							$show_trial_message = "<b>$diff_days</b> Days left";
						else if($diff_days==1)
							$show_trial_message = "<b>$diff_days</b> Day left";
						else if($diff_days==0)
							$show_trial_message = "<b>Today</b> is your last day!";
						else
						{
							$show_trial_message = "Please contact your distributor to resume use of POSLavu";
							$p_collection_lockout = true;
						}
					}
				}
				else if($status_read['ok']=="1")
				{
				}
				else if($status_read['collection_active']!="1")
				{
				}
				else if($status_read['hosting_action']!="" && $status_read['hosting_taken']!="1")
				{
					$show_alert = true;
				}
				else if($status_read['collect']!="" && $status_read['collection_taken']!="1")
				{
					$show_alert = true;
				}

				$needs_to_collect = false;
				if($status_read['collect']!="" && $status_read['collection_taken']!="1")
					$need_to_collect = true;
				else if($status_read['hosting_action']!="" && $status_read['hosting_taken']!="1")
					$need_to_collect = true;

				$collection_due_date = $status_read['collection_due_date'];
				if($collection_due_date!="" && $need_to_collect)//$status_read['collection_taken']!="1")
				{
					$due_parts = explode("-",$collection_due_date);
					if(count($due_parts) > 2)
					{
						$current_ts = time();
						$due_ts = mktime(0,0,0,$due_parts[1],$due_parts[2],$due_parts[0]);
						$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
						if($diff_days > 1)
							$collection_due_date_message = "<b>$diff_days</b> Days left to use POSLavu without resolution";
						else if($diff_days==1)
							$collection_due_date_message = "<b>$diff_days</b> Day left to use POSLavu without resolution";
						else if($diff_days==0)
							$collection_due_date_message = "<b>Today</b> is your last day left to use POSLavu without resolution";
						else
						{
							$collection_due_date_message = "Please resolve your billing issues to resume use of POSLavu";
							$p_collection_lockout = true;
						}
					}
				}
			}

			if($p_collection_lockout)
				$show_alert = true;

			if($show_trial_alert)
			{
				$str .= "<table cellpadding=8 style='border:solid 1px #bbbbbb' bgcolor='#f6f6f6' width='100%'><tr><td valign='middle'>&nbsp;</td><td valign='middle' align='center'>";
				$str .= "<b>Evaluation Mode</b>: $show_trial_message";
				$str .= "</td></tr></table><br><br>";
			}
			else if($show_alert)
			{
				$str .= "<table cellpadding=8 style='border:solid 1px #bbbbbb' bgcolor='#f6f6f6'><tr><td valign='middle'><img src='/cp/images/little_lavu.png' /></td><td valign='middle' align='center'>";
				$str .= "<b>Important</b>: There is a billing issue with your account.<br><a href='index.php?mode=billing'>Please click here for more information</a><br><br>";
				$str .= $collection_due_date_message;
				$str .= "</td></tr></table><br><br>";
			}
		}
		return $str;
	}

	function get_collection_lockout_status() // duplicate function in /areas/billing.php
	{
		global $p_collection_lockout;
		if(!isset($p_collection_lockout)) $p_collection_lockout = false;
		if($p_collection_lockout==true) return "locked";
		else return "allow";
	}

	/*--------------------------------------------------------------------------------------------------------------*/
	// PAYMENT PROFILES

	require_once(dirname(__FILE__)."/../payment_profile_functions.php");

	//

	$dn = (isset($_GET['dn']))?$_GET['dn']:"";

	$title = "Billing";
	if ($dn != "") {
		$get_company_name = mlavu_query("SELECT `id`, `company_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dn);
		$info = mysqli_fetch_assoc($get_company_name);
		$title .= " - ".$info['company_name']." ($dn)";
		$restaurantid = $info['id'];
	}

	echo "<head><title>$title</title></head>";
?>
	<style type="text/css">
		.page_title { font-family:Arial, Helvetica, sans-serif; font-size:20px; }
		.section_header { font-family:Arial, Helvetica, sans-serif; font-size:17px; }
		.table_header { font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#006600; padding:1px 5px 1px 5px; }
		.table_cell1 { font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000000; padding:3px 5px 2px 5px; border-top:1px solid #CCCCCC; }
		.table_cell2 { font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; padding:7px 8px 2px 5px; border-top:1px solid #CCCCCC; }
		.info_label { font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#336633; }
		.info_value { font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#000000; }
		.info_value2 { font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#111111; }
		.info_value3 { font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#222222; }
	</style>
<?php

	if(isset($_GET['mode']) && $_GET['mode']=="admin")
	{
		require_once(dirname(__FILE__)."/../billing_admin.php");
	}
	else if($dn=="")
	{
		global $origin;
		global $o_package_container;
		$page = (isset($_REQUEST['page']))?$_REQUEST['page']:"1";
		$search_for = (isset($_REQUEST['search_for']))?$_REQUEST['search_for']:"";
		$search_field = (isset($_REQUEST['search_field']))?$_REQUEST['search_field']:"";
		//$order_by = (isset($_REQUEST['order_by']))?$_REQUEST['order_by']:"id";
		//$order_dir = (isset($_REQUEST['order_dir']))?$_REQUEST['order_dir']:"ASC";
		$package_filter = (isset($_REQUEST['pfilt']))?$_REQUEST['pfilt']:"All";
		$status_filter = (isset($_REQUEST['sfilt']))?$_REQUEST['sfilt']:"overdue";

		echo "<body>";
		/***

		MODIFICATION HERE !!!!!!!

		***/

		if($origin!='sa_cp')
			echo "<form name='form1' method='post' action='/../../sa_cp/billing/index.php'>";
		else
			echo "<form name='form1' method='post' action='index.php'>";

		echo "<table cellpadding=12><tr><td style='border:solid 1px black' valign='top'>";
		echo "<table>";

		$search_fields = array(array("company_name,data_name","Company Name/Dataname"),array("created","Date Created"),array("last_activity","Date Last Active"));
		$a_package_names = array("All");
		$a_package_names = array_merge($a_package_names, $o_package_container->get_not_lavulite_names());
		$payment_status = array(array("all","All"),array("paid","Paid on Time"),array("overdue","Overdue"));

		echo "<tr>
			<td colspan='4'>
				<table>
					<tr>
						<td class='info_label' style='padding:1px 8px 1px 8px;'>Search for </td>
						<td><input type='text' name='search_for' size='30' value='$search_for'></td>
						<td class='info_label' style='padding:1px 8px 1px 8px;'>in </td>
						<td>
							<select name='search_field'>";
		foreach ($search_fields as $field) {
			$selected = "";
			if ($search_field == $field[0]) $selected = " SELECTED";
			echo "<option value='".$field[0]."'$selected>".$field[1]."</option>";
		}
		echo "		</select>
						</td>
						<td class='info_label' style='padding:1px 5px 1px 12px;'>Package:</td>
						<td>
							<select name='pfilt'>";
		foreach ($a_package_names as $package) {
			$selected = "";
			if ($package_filter == $package) $selected = " SELECTED";
			echo "<option value='".$package."'$selected>".$package."</option>";
		}
		echo "		</select>
						</td>
						<td class='info_label' style='padding:1px 5px 1px 12px;'>Payment status:</td>
						<td>
							<select name='sfilt'>";
		foreach ($payment_status as $status) {
			$selected = "";
			if ($status_filter == $status[0]) $selected = " SELECTED";
			echo "<option value='".$status[0]."'$selected>".$status[1]."</option>";
		}
		echo "		</select>
						</td>
						<td style='padding-left:10px'>
							<input type='hidden' name='page' id='results_page' value='$page'>
							<input type='submit' value='Search'>
						</td>
					</tr>
				</table>
			</td>
		</tr>";
		echo "</table></td></tr></table>";
		echo "</form>";

		$status_found = 0;
		$signup_found = 0;
		$signup_packages = array();
		$no_package_list = array();
		$has_package_list = array();

		$left_join = "";
		$filter = "";
		if ($search_for != "") {
			if ($search_field != "") {
				$search_fields = explode(",", $search_field);
				$ff = "";
				foreach ($search_fields as $field) {
					if ($ff != "") $ff .= " OR ";
					$ff .= "`$maindb`.`restaurants`.`$field` LIKE '%$search_for%'";
				}
				$filter .= " AND ($ff)";
			}
		}
		if ($package_filter != "All") {
			$left_join = " LEFT JOIN `$maindb`.`payment_status` ON `$maindb`.`payment_status`.`restaurantid` = `$maindb`.`restaurants`.`id`";
			$filter .= " AND (`$maindb`.`restaurants`.`package_status` = '$package_filter' OR `$maindb`.`payment_status`.`current_package` = '".$o_package_container->get_level_by_attribute('name',$package_filter)."')";
			/*if ($package_filter == "Silver") $filter .= " AND (`$maindb`.`restaurants`.`package_status` = 'Silver' OR `$maindb`.`payment_status`.`current_package` = '1')";
			if ($package_filter == "Gold") $filter .= " AND (`$maindb`.`restaurants`.`package_status` = 'Gold' OR `$maindb`.`payment_status`.`current_package` = '2')";
			if ($package_filter == "Platinum") $filter .= " AND (`$maindb`.`restaurants`.`package_status` = 'Platinum' OR `$maindb`.`payment_status`.`current_package` = '3')";*/
		}
		if ($status_filter != "all") {
			$left_join = " LEFT JOIN `$maindb`.`payment_status` ON `$maindb`.`payment_status`.`restaurantid` = `$maindb`.`restaurants`.`id`";
			if ($status_filter == "paid") $filter .= " AND `$maindb`.`payment_status`.`due_info` NOT LIKE '%Due since%' AND `$maindb`.`payment_status`.`due_info` != ''";
			if ($status_filter == "overdue") $filter .= " AND `$maindb`.`payment_status`.`due_info` LIKE '%Due since%'";
		}

		$get_count = mlavu_query("select COUNT(`$maindb`.`restaurants`.`id`) from `$maindb`.`restaurants`$left_join where (`$maindb`.`restaurants`.`disabled`!='1' and `$maindb`.`restaurants`.`disabled`!='2' and `$maindb`.`restaurants`.`disabled`!='3') and `$maindb`.`restaurants`.`demo`!='1' and `$maindb`.`restaurants`.`dev`!='1'$filter");
		$total_count = mysqli_result($get_count, 0);

		$rest_query = mlavu_query("select `restaurants`.*, `billing_profiles`.`package` AS `profile_package`, `billing_profiles`.`active` AS `profile_active` from `$maindb`.`restaurants`$left_join LEFT JOIN `$maindb`.`billing_profiles` ON `$maindb`.`billing_profiles`.`restaurantid` = `$maindb`.`restaurants`.`id` where (`$maindb`.`restaurants`.`disabled`!='1' and `$maindb`.`restaurants`.`disabled`!='2' and `$maindb`.`restaurants`.`disabled`!='3') and `$maindb`.`restaurants`.`demo`!='1' and `$maindb`.`restaurants`.`dev`!='1'$filter GROUP BY `restaurants`.`id`");
		if(($rest_query->num_rows) < 1 && $search_for!="")
		{
			$rest_query = mlavu_query("select `restaurants`.*, `billing_profiles`.`package` AS `profile_package`, `billing_profiles`.`active` AS `profile_active` from `$maindb`.`restaurants`$left_join LEFT JOIN `$maindb`.`billing_profiles` ON `$maindb`.`billing_profiles`.`restaurantid` = `$maindb`.`restaurants`.`id` where `restaurants`.`data_name`='[1]'",$search_for);
		}

		while($rest_read = mysqli_fetch_assoc($rest_query))
		{
			$package = (!empty($rest_read['profile_package']))?$rest_read['profile_package']:"";

			$license_applied = "";
			$license_upgrades = array();
			$stuff = array();
			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]' or `dataname`='[2]'",$rest_read['id'],$rest_read['data_name']);
			if($status_query->num_rows)
			{
				$status_found++;

				$status_read = mysqli_fetch_assoc($status_query);
				$license_applied = $status_read['license_applied'];
				RESELLER_LICENSES::get_extra_upgrades_from_upgrade_string($status_read['extra_upgrades_applied'], $status_read['extra_reseller_ids'], $license_upgrades, $stuff);
				if (!is_numeric($package)) $package = $status_read['current_package'];
				$due_info = $status_read['due_info'];
			}
			else
			{
				$due_info = "n/a";
			}

			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `restaurantid`='[1]' or `dataname`='[2]'",$rest_read['id'],$rest_read['data_name']);
			if($signup_query->num_rows)
			{
				$signup_found++;
				$signup_read = mysqli_fetch_assoc($signup_query);
				if(!is_numeric($package))
				{
					$package = $signup_read['package'];
				}

				if(!is_numeric($package))
					$package = scrape_package($package, $license_applied, $rest_read['data_name'], $rest_read['id']);
				$package = RESELLER_LICENSES::choose_upgrades_for_packages($package, $license_upgrades);

				$has_billing_profile = ($rest_read['profile_active']=="1")?true:false;

				if($package=="")
				{
					$no_package_list[] = array($rest_read['data_name'],$rest_read['company_name'],$rest_read['created'],$signup_read['status'],$due_info,$rest_read['id'],$rest_read['last_activity'],$has_billing_profile);
					$package = "none";
				}
				else if(!is_numeric($package) || $package=="24")
					$package = "other";
				else
				{
					$has_package_list[] = array($rest_read['data_name'],$rest_read['company_name'],$rest_read['created'],$signup_read['status'],$package,$due_info,$rest_read['id'],$rest_read['last_activity'],$has_billing_profile);
				}

				if(!isset($signup_packages[$package]))
					$signup_packages[$package] = 0;
				$signup_packages[$package]++;
			}
		}

		echo "Status found: $status_found<br>";
		echo "Signup found: $signup_found<br>";
		echo "<br><span class='section_header'><u>Packages:</u></span><br>";

		if (count($has_package_list) > 0) {

			foreach($signup_packages as $key => $val)
				echo "<br>" . packageToStr($key) . ": " . $val;

			echo "<br><br>";
			echo "<table cellspacing='0' cellpadding='3'>";
			for($i=0; $i<count($has_package_list); $i++)
			{
				$pp_dataname = $has_package_list[$i][0];
				$pp_package = $has_package_list[$i][4];
				$pp_signup_status = $has_package_list[$i][3];
				$pp_due_info = $has_package_list[$i][5];
				if(isset($_GET['process_payments']))
				{
					$pp_result = process_payments($pp_dataname,$pp_package,$pp_signup_status);
					$pp_due_info = $pp_result['due_info'];
				}

				if ($i == 0) {
					echo "<tr>";
					echo "<td></td><td class='table_header'><b>ID</b></td>";
					echo "<td class='table_header'><b>Company</b></td>";
					echo "<td class='table_header'><b>Dataname</b></td>";
					echo "<td class='table_header'><b>Created</b></td>";
					echo "<td class='table_header'><b>Last Activity</b></td>";
					echo "<td class='table_header'><b>Signup</b></td>";
					echo "<td class='table_header'><b>Package</b></td>";
					echo "<td class='table_header'><b>Due</b></td>";
					echo "</tr>";
				}

				$activity_color = "#444444;";
				if ($has_package_list[$i][7] >= date("Y-m-d H:i:s", (time() - 432000))) $activity_color = "#336633; font-weight:bold;";

				$no_profile = "";
				if (!$has_package_list[$i][8]) $no_profile = "<font color='#CC0000' size='-2'>*</font>";

				echo "<tr onmouseover='this.bgColor = \"#ccddee\"' onmouseout='this.bgColor = \"#ffffff\"' style='cursor:pointer;' onclick='window.open(\"index.php?dn=".$has_package_list[$i][0]."&package=".$has_package_list[$i][4]."&signup_status=".$has_package_list[$i][3]."\",\"\",\"width=1024,height=600,scrollbars=yes,resizable=yes,toolbar=no\")'><td style='padding:0px 0px 0px 0px;'>".$no_profile."</td><td class='table_cell1'>".$has_package_list[$i][6]."</td><td class='table_cell1'>" . $has_package_list[$i][1] . "</td><td class='table_cell1'>" . $has_package_list[$i][0] . "</td><td class='table_cell1'>" . $has_package_list[$i][2] . "</td><td class='table_cell1' style='color:$activity_color'>".$has_package_list[$i][7]."</td><td class='table_cell1'>" . $has_package_list[$i][3] . "</td><td class='table_cell1'>".packageToStr($has_package_list[$i][4])."</td><td class='table_cell1'>".str_replace(array("|Due since","|"), array("<br>Due since"," - "), $pp_due_info)."</td></tr>";
			}
			echo "</table>";

		} else {

			echo "<br><table><tr><td align='center' width='400' class='info_value'>No Accounts Found Matching Search Criteria</td></tr></table>";
		}

		echo "<br><br>";
		if (count($no_package_list) > 0) {
			echo "<br><span class='section_header'><u>No Package Specified:</u></span><br><br>";
			echo "<table cellspacing='0' cellpadding='3'>";
			for($i=0; $i<count($no_package_list); $i++)
			{
				if ($i == 0) {
					echo "<tr>";
					echo "<td></td><td class='table_header'><b>ID</b></td>";
					echo "<td class='table_header'><b>Company</b></td>";
					echo "<td class='table_header'><b>Dataname</b></td>";
					echo "<td class='table_header'><b>Created</b></td>";
					echo "<td class='table_header'><b>Last Activity</b></td>";
					echo "<td class='table_header'><b>Signup</b></td>";
					echo "</tr>";
				}

				$activity_color = "#444444;";
				if ($no_package_list[$i][6] >= date("Y-m-d H:i:s", (time() - 432000))) $activity_color = "#336633; font-weight:bold;";

				$no_profile = "";
				if (!$no_package_list[$i][7]) $no_profile = "<font color='#CC0000' size='-2'>*</font>";

				echo "<tr onmouseover='this.bgColor = \"#ccddee\"' onmouseout='this.bgColor = \"#ffffff\"' style='cursor:pointer;' onclick='window.open(\"index.php?dn=".$no_package_list[$i][0]."&package=0&signup_status=".$no_package_list[$i][3]."\",\"\",\"width=925,height=600,scrollbars=yes,resizable=yes,toolbar=no\")'><td style='padding:0px 0px 0px 0px;'>".$no_profile."</td><td class='table_cell1'>" . $no_package_list[$i][5] . "</td><td class='table_cell1'>" . $no_package_list[$i][1] . "</td><td class='table_cell1'>" . $no_package_list[$i][0] . "</td><td class='table_cell1'>" . $no_package_list[$i][2] . "</td><td class='table_cell1' style='color:$activity_color'>".$no_package_list[$i][6]."</td><td class='table_cell1'>" . $no_package_list[$i][3] . "</td></tr>";
			}
			echo "</table>";
		}
		echo "</body>";
	}
	else
	{
		//------------------------------------------------ Billing for Individual Account -------------------------------------------------//

		if (!isset($load_billing_individual_accounts))
			$load_billing_individual_accounts = TRUE;
		require_once(dirname(__FILE__)."/../procareas/billing_for_individual_accounts.php");
	}
?>
