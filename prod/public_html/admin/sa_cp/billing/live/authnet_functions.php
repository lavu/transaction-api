<?php
	function authnet_info_original($type)
	{
		$info = array();
		$info['login'] = "8wvM4Z5T";
		$info['transkey'] = "8x4cPL589rhWuW5X";
		$info['version'] = 1;
		if(isset($info[$type])) return $info[$type]; else return "";
	}
	
	function authnet_info($type)
	{
		$info = array();
		$info['login'] = "8yN2Rr5E";
		$info['transkey'] = "6PSd425pe3J4qVcL";  //was 39v6L64PVwne3F6C before 2016-03-02 and 5V966uCktkX24y9x before that
		$info['version'] = 2;
		if(isset($info[$type])) return $info[$type]; else return "";
	}

	function process_creditcard_authorize($card_number, $card_expiration, $card_code, $amount, $description, $first_name="", $last_name="", $address="", $city="", $state="", $zip="", $company_name="", $email="", $phone="",$charge=false)
	{
		global $payment_settings;
		$api_login = authnet_info("login");
		$api_key = authnet_info("transkey");

		$post_url = "https://secure.authorize.net/gateway/transact.dll";
		if($charge)
			$authmode = "AUTH_CAPTURE";
		else
			$authmode = "AUTH_ONLY";

		$post_values = array(
			"x_login"			=> $api_login,
			"x_tran_key"		=> $api_key,

			"x_version"			=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "FALSE",

			"x_type"			=> $authmode,
			"x_method"			=> "CC",
			"x_card_num"		=> $card_number, //"4111111111111111",
			"x_card_code"		=> $card_code,
			"x_exp_date"		=> $card_expiration, //"0115",

			"x_amount"			=> $amount, //"19.99",
			"x_description"		=> $description, //"Sample Transaction",

			"x_first_name"		=> $first_name, //"John",
			"x_last_name"		=> $last_name, //"Doe",
			"x_address"			=> $address, //"1234 Street",
			"x_city"			=> $city,
			"x_state"			=> $state, //"WA",
			"x_zip"				=> $zip, //"98004"

			"x_phone"			=> $phone,
			"x_email"			=> $email,
			"x_company"			=> $company_name
		);

		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );

		$request = curl_init($post_url); // initiate curl object
		curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
		$post_response = curl_exec($request); // execute curl post and store results in $post_response
		curl_close($request); // close curl object

		$response_array = explode($post_values["x_delim_char"],$post_response);
		$response_code = $response_array[0];
		if(isset($_GET['test'])) echo "<br>".$response_array[3]."<br>";
		if($response_code==1) return "Approved";
		else if($response_code==2) return "Declined";
		else if($response_code==3) return $response_array[3];
		else if($response_code==4) return "Held for Review";
		else return "Unknown Response";
	}

	/**
	 * Class for processing recurring payments via ARB API Authorize.net
	 * Original code by John Conde: http://www.merchant-account-services.org/blog/
	 * with a few changes by Alexis Bellido: http://www.ventanazul.com/webzine/articles/authorize-recurring-billing-php-class
	 */

	class AuthnetARB {
		var $login;
		var $transkey;
		var $test;

		var $params   = array();
		var $success   = false;
		var $error    = true;

		var $url;
		var $xml;
		var $response;
		var $resultCode;
		var $code;
		var $text;
		var $subscrId;

		function __construct($login, $transkey, $test) {
			$this->login = $login;
			$this->transkey = $transkey;
			$this->test = $test;

			$subdomain = ($this->test) ? 'apitest' : 'api';
			$this->url = "https://" . $subdomain . ".authorize.net/xml/v1/request.api";
		}

		function getString() {
			if (!$this->params) {
				return (string) $this;
			}

			$output  = "";
			$output .= '<table summary="Authnet Results" id="authnet">' . "\n";
			$output .= '<tr>' . "\n\t\t" . '<th colspan="2"><b>Outgoing Parameters</b></th>' . "\n" . '</tr>' . "\n";

			foreach ($this->params as $key => $value) {
				$output .= "\t" . '<tr>' . "\n\t\t" . '<td><b>' . $key . '</b></td>';
				$output .= '<td>' . $value . '</td>' . "\n" . '</tr>' . "\n";
			}

			$output .= '</table>' . "\n";
			return $output;
		}

		function process($retries = 3) {
			$count = 0;
			while ($count < $retries)
			{
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $this->url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
				curl_setopt($ch, CURLOPT_HEADER, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $this->xml);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$this->response = curl_exec($ch);
				$this->parseResults();
				if ($this->resultCode === "Ok") {
			  $this->success = true;
			  $this->error   = false;
			  break;
				} else {
			  $this->success = false;
			  $this->error   = true;
			  break;
				}
				$count++;
			}
			curl_close($ch);
		}

		function createAccount() {
			$this->xml = "<?xml version='1.0' encoding='utf-8'?>
						  <ARBCreateSubscriptionRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
							  <merchantAuthentication>
								  <name>" . $this->login . "</name>
								  <transactionKey>" . $this->transkey . "</transactionKey>
							  </merchantAuthentication>
							  <refId>" . $this->params['refID'] ."</refId>
							  <subscription>
								  <name>". $this->params['subscrName'] ."</name>
								  <paymentSchedule>
									  <interval>
										  <length>". $this->params['interval_length'] ."</length>
										  <unit>". $this->params['interval_unit'] ."</unit>
									  </interval>
									  <startDate>" . $this->params['startDate'] . "</startDate>
									  <totalOccurrences>". $this->params['totalOccurrences'] . "</totalOccurrences>
									  <trialOccurrences>". $this->params['trialOccurrences'] . "</trialOccurrences>
								  </paymentSchedule>
								  <amount>". $this->params['amount'] ."</amount>
								  <trialAmount>" . $this->params['trialAmount'] . "</trialAmount>
								  <payment>
									  <creditCard>
										  <cardNumber>" . $this->params['cardNumber'] . "</cardNumber>
										  <expirationDate>" . $this->params['expirationDate'] . "</expirationDate>
									  </creditCard>
								  </payment>
								  <billTo>
									  <firstName>". $this->params['firstName'] . "</firstName>
									  <lastName>" . $this->params['lastName'] . "</lastName>
									  <address>" . htmlspecialchars($this->params['address']) . "</address>
									  <city>" . $this->params['city'] . "</city>
									  <state>" . $this->params['state'] . "</state>
									  <zip>" . $this->params['zip'] . "</zip>
									  <country>" . $this->params['country'] . "</country>
								  </billTo>
							  </subscription>
						  </ARBCreateSubscriptionRequest>";
			$this->process();
		}

		function updateStartDate() {
			$this->xml = "<?xml version='1.0' encoding='utf-8'?>
						  <ARBUpdateSubscriptionRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
							  <merchantAuthentication>
								  <name>" . $this->login . "</name>
								  <transactionKey>" . $this->transkey . "</transactionKey>
							  </merchantAuthentication>
							  <refId>" . $this->params['refID'] ."</refId>
							  <subscriptionId>" . $this->params['subscrId'] . "</subscriptionId>
							  <subscription>
								  <paymentSchedule>
									  <startDate>" . $this->params['startDate'] . "</startDate>
								  </paymentSchedule>
							  </subscription>
						  </ARBUpdateSubscriptionRequest>";
			$this->process();
		}

		function updateAmount() {
			$this->xml = "<?xml version='1.0' encoding='utf-8'?>
						  <ARBUpdateSubscriptionRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
							  <merchantAuthentication>
								  <name>" . $this->login . "</name>
								  <transactionKey>" . $this->transkey . "</transactionKey>
							  </merchantAuthentication>
							  <refId>" . $this->params['refID'] ."</refId>
							  <subscriptionId>" . $this->params['subscrId'] . "</subscriptionId>
							  <subscription>
								  <amount>". $this->params['amount'] ."</amount>
							  </subscription>
						  </ARBUpdateSubscriptionRequest>";
			$this->process();
		}

		function updateAccount() {
			$this->xml = "<?xml version='1.0' encoding='utf-8'?>
						  <ARBUpdateSubscriptionRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
							  <merchantAuthentication>
								  <name>" . $this->login . "</name>
								  <transactionKey>" . $this->transkey . "</transactionKey>
							  </merchantAuthentication>
							  <refId>" . $this->params['refID'] ."</refId>
							  <subscriptionId>" . $this->params['subscrId'] . "</subscriptionId>
							  <subscription>
								  <payment>
									  <creditCard>
										  <cardNumber>" . $this->params['cardNumber'] . "</cardNumber>
										  <expirationDate>" . $this->params['expirationDate'] . "</expirationDate>
									  </creditCard>
								  </payment>
							  </subscription>
						  </ARBUpdateSubscriptionRequest>";
			$this->process();
		}

		function getInfo() {
			$this->xml = "<?xml version='1.0' encoding='utf-8'?>
						  <ARBGetSubscriptionStatusRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
							<merchantAuthentication>
								<name>" . $this->login . "</name>
								<transactionKey>" . $this->transkey . "</transactionKey>
							</merchantAuthentication>
							<refId>" . $this->params['refID'] . "</refId>
							<subscriptionId>" . $this->params['subscrId'] . "</subscriptionId>
							</ARBGetSubscriptionStatusRequest>";
			$this->process();
		}

		function deleteAccount() {
			$this->xml = "<?xml version='1.0' encoding='utf-8'?>
						  <ARBCancelSubscriptionRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
							  <merchantAuthentication>
								  <name>" . $this->login . "</name>
								  <transactionKey>" . $this->transkey . "</transactionKey>
							  </merchantAuthentication>
							  <refId>" . $this->params['refID'] ."</refId>
							  <subscriptionId>" . $this->params['subscrId'] . "</subscriptionId>
						  </ARBCancelSubscriptionRequest>";
			$this->process();
		}

		function parseResults() {
			$this->resultCode = $this->substring_between($this->response,'<resultCode>','</resultCode>');
			$this->code = $this->substring_between($this->response,'<code>','</code>');
			$this->text = $this->substring_between($this->response,'<text>','</text>');
			$this->subscrId = $this->substring_between($this->response,'<subscriptionId>','</subscriptionId>');
		}

		function substring_between($haystack,$start,$end) {
			if (strpos($haystack,$start) === false || strpos($haystack,$end) === false) {
				return false;
			} else {
				$start_position = strpos($haystack,$start)+strlen($start);
				$end_position = strpos($haystack,$end);
				return substr($haystack,$start_position,$end_position-$start_position);
			}
		}

		function setParameter($field = "", $value = null) {
			$field = (is_string($field)) ? trim($field) : $field;
			$value = (is_string($value)) ? trim($value) : $value;

			if (!is_string($field)) {
				return;
				//die("setParameter() arg 1 for $field must be a string or integer: " . gettype($field) . " given.");
			}

			if (!is_string($value) && !is_numeric($value) && !is_bool($value)) {
				$value = "";
				//die("setParameter() arg 2 for $field must be a string, integer, or boolean value: " . gettype($value) . " given.");
			}

			if (empty($field)) {
				return;
				//die("setParameter() for $field requires a parameter field to be named.");
			}

			if ($value === "") {
				//die("setParameter() for $field requires a parameter value to be assigned: $field");
			}

			$this->params[$field] = $value;
		}

		function isSuccessful() {
			return $this->success;
		}

		function isError() {
			return $this->error;
		}

		function getResponse() {
			return $this->text;
		}

		function getRawResponse() {
			return $this->response;
		}

		function getResultCode() {
			return $this->resultCode;
		}

		function getSubscriberID() {
			return $this->subscrId;
		}
	}

	//-------------------------------------------------------------------------------------------//
	function manage_recurring_billing($action="create",$set=false)
	{
		// Code Block 1 for Authnet Version Start ----------------------//
		$authnet_version = 2;
		if(is_array($set) && isset($set['subscribeid']))
		{
			if(trim($set['subscribeid'])=="" || substr($set['subscribeid'],0,7)=="chase2_")
			{
				$set['subscribeid'] = str_replace("chase2_","",$set['subscribeid']);
				$authnet_version = 2;
			}
			else
			{
				$authnet_version = 1;
			}
		}
		
		if($authnet_version==2)
		{
			$login = authnet_info("login");
			$transkey = authnet_info("transkey");
			$test = FALSE;
		}
		else
		{
			$login = authnet_info_original("login");
			$transkey = authnet_info_original("transkey");
			$test = FALSE;
		}
		// Code Block 1 for Authnet Version End ----------------------//

		$arb = new AuthnetARB($login, $transkey, $test);

		if($action=="authorize")
		{
			$result = process_creditcard_authorize($set['card_number'], $set['card_date'], $set['card_code'], $set['amount'], $set['description'], $set['first_name'], $set['last_name'], $set['address'], $set['city'], $set['state'], $set['zip'], $set['company_name'], $set['email'], $set['phone'],false);
			return $result;
		}
		else if($action=="charge")
		{
			$result = process_creditcard_authorize($set['card_number'], $set['card_date'], $set['card_code'], $set['amount'], $set['description'], $set['first_name'], $set['last_name'], $set['address'], $set['city'], $set['state'], $set['zip'], $set['company_name'], $set['email'], $set['phone'],true);
			return $result;
		}
		else if($action=="create")
		{
			$arb->setParameter('interval_length',	substr( $set['interval_length'],0,3 ));
			$arb->setParameter('interval_unit',		$set['interval_unit']);
			$arb->setParameter('startDate',			$set['start_date']);
			$arb->setParameter('totalOccurrences',	substr( $set['total_occurrences'],0,4 ));
			$arb->setParameter('trialOccurrences',	substr( $set['trial_occurrences'],0,2 ));
			$arb->setParameter('trialAmount',		substr( $set['trial_amount'],0,15 ));

			$arb->setParameter('amount',			substr( $set['amount'],0,15 ));
			$arb->setParameter('refID',				substr( $set['refid'],0,20 ));
			$arb->setParameter('cardNumber',		substr( $set['card_number'],0,16 ));
			$arb->setParameter('expirationDate',	$set['card_date']);

			$arb->setParameter('firstName',			substr( $set['first_name'],0,50 ));
			$arb->setParameter('lastName',			substr( $set['last_name'],0,50 ));
			$arb->setParameter('address',			substr( $set['address'],0,60 ));
			$arb->setParameter('city',				substr( $set['city'],0,40 ));
			$arb->setParameter('state',				substr( $set['state'],0,2 ));
			$arb->setParameter('zip',				substr( $set['zip'],0,20 ));
			if(isset($set['country']) && $set['country']!="") {
				$arb->setParameter('country',		substr( $set['country'],0,60 ));
			}

			$arb->setParameter('subscrName',		substr( $set['description'],0,50 ));
			$arb->createAccount();
		}
		else if($action=="update")
		{
			$arb->setParameter('refID',				substr( $set['refid'],0,20 ));
			$arb->setParameter('subscrId',			substr( $set['subscribeid'],0,13 ));
			$arb->setParameter('cardNumber',		substr( $set['card_number'],0,16 ));
			$arb->setParameter('expirationDate',	$set['card_date']);
			$arb->updateAccount();

			if($arb->isSuccessful())
			{
				return "Approved";
			}
			else return "Declined";
		}
		else if($action=="update_start")
		{
			$arb->setParameter('refID',				substr( $set['refid'],0,20 ));
			$arb->setParameter('subscrId',			substr( $set['subscribeid'],0,13 ));
			$arb->setParameter('startDate',			$set['start_date']);
			$arb->updateStartDate();
		}
		else if($action=="update_amount")
		{
			$arb->setParameter('refID',				substr( $set['refid'],0,20 ));
			$arb->setParameter('subscrId',			substr( $set['subscribeid'],0,13 ));
			$s_amount = number_format((float)$set['amount'], 2, '.', '');
			$arb->setParameter('amount',			substr( $s_amount,0,15 ));
			$arb->updateAmount();
		}
		else if($action=="info")
		{
			$arb->setParameter('refID',				substr( $set['refid'],0,20 ));
			$arb->setParameter('subscrId',			substr( $set['subscribeid'],0,13 ));
			$arb->getInfo();
		}
		else if($action=="delete")
		{
			$arb->setParameter('refID',				substr( $set['refid'],0,20 ));
			$arb->setParameter('subscrId',			substr( $set['subscribeid'],0,13 ));
			$arb->deleteAccount();
		}

		if ($arb->isSuccessful())
		{
			$subscribeid = $arb->getSubscriberID();
			// Code Block 2 for Authnet Version Start ----------------------//
			if($authnet_version=="2")
			{
				$subscribeid = "chase2_" . str_replace("chase2_","",$subscribeid);
			}
			// Code Block 2 for Authnet Version End ----------------------//
			
			$result = array();
			$result['success'] = true;
			$result['id'] = $subscribeid;
			$result['response'] = $arb->getRawResponse();
			return $result;
		}
		else
		{
			$result = array();
			$result['success'] = false;
			$result['error'] = $arb->isError();
			$result['response'] = $arb->getResponse();
			return $result;
		}
	}
?>
