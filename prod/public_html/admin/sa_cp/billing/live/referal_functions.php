<?php

require_once(dirname(__FILE__)."/../package_levels_object.php");
require_once(dirname(__FILE__)."/../payment_profile_functions.php");
require_once(dirname(__FILE__)."/../procareas/procpay_functions.php");
require_once(dirname(__FILE__)."/../../../../register/authnet_functions.php");
require_once(dirname(__FILE__)."/../../../cp/resources/core_functions.php");
require_once(dirname(__FILE__)."/../../../cp/resources/json.php");
if (!isset($o_package_container))
	$o_package_container = new package_container();
if (!isset($maindb))
	$maindb = "poslavu_MAIN_db";

// updates or inserts a waived invoice to the given account with $s_dataname
// @$i_next_billdate: the billing date to create the waived invoice for
// @$s_dataname: the dataname of the account
// @$s_restaurantid: id of the acount
// @$i_package: package level of the account
// @$a_reverts: the array to be added to if the update/insert succeeds
// @$i_invoice_id: the id of the invoice updated/inserted will be returned here
// @return: TRUE on success, FALSE otherwise
function redeem_referral_code_create_waived_invoice($now, &$a_restaurant, $s_user, $s_full_username, $s_dataname_referree, $s_dataname_referrer, $i_next_billdate, $s_dataname, $s_restaurant_id, $i_package, &$a_reverts, &$i_invoice_id = NULL) {

	$b_database_updated = FALSE;

	// create or update the waived invoice
	$o_package_container = new package_container;
	$a_where_vars = array('due_date'=>date('Y-m-d',$i_next_billdate), 'dataname'=>$s_dataname, 'type'=>'Hosting');
	$a_where_vars2 = array('due_date'=>date('Y-m-d',$i_next_billdate), 'dataname'=>$s_dataname, 'type'=>'hosting');
	$a_insert_vars = array_merge($a_where_vars, array('restaurantid'=>$s_restaurant_id, 'amount'=>$o_package_container->get_recurring_by_attribute('level', $i_package), 'waived'=>'1'));
	$a_original_row = array();
	$a_where_vars_array = array($a_where_vars, $a_where_vars2);
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
	$s_success = $dbapi->insertOrUpdate('poslavu_MAIN_db', 'invoices', $a_insert_vars, $a_where_vars_array, $a_original_row);
	if (substr($s_success, 0, 1) == 'i') {

		// the invoice was created
		$s_insert_id = (int)trim(substr($s_success, 1));
		if ($s_insert_id != 0) {
			$i_invoice_id = $s_insert_id;
			$a_reverts[] = array('string'=>"DELETE FROM `poslavu_MAIN_db`.`invoices` WHERE `id`='[id]'", 'vars'=>array('id'=>$s_insert_id));
			$b_database_updated = TRUE;
		}
	} else if (substr($s_success, 0, 1) == 'u') {

		// the invoice was updated
		$i_num_affected_rows = (int)trim(substr($s_success, 1));
		if ($i_num_affected_rows != 0) {
			$i_invoice_id = $a_original_row['id'];
			$s_update_clause = $dbapi->arrayToUpdateClause($a_original_row);
			$a_reverts[] = array('string'=>"UPDATE `poslavu_MAIN_db`.`invoices` $s_update_clause WHERE `id`='[id]'", 'vars'=>$a_original_row);
			$b_database_updated = TRUE;
		}
	}

	// insert into the billing log that waived invoice was created
	if ($b_database_updated) {
		$a_insert_vars = array('datetime'=>$now, 'dataname'=>$s_dataname, 'restaurantid'=>$a_restaurant['id'], 'username'=>$s_user, 'fullname'=>$s_full_username, 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'action'=>'waived invoice for referral program', 'details'=> 'JSON: '.LavuJson::json_encode(array('program'=>'referral', 'referrer'=>$s_dataname_referrer, 'referree'=>$s_dataname_referree, 'success'=>'TRUE', 'comment'=>'Created waived invoice for '.date('Y-m-d',$i_next_billdate).'.')), 'billid'=>$i_invoice_id);
		$s_insert_clause = $dbapi->arrayToInsertClause($a_insert_vars);
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` $s_insert_clause", $a_insert_vars);
	}

	return $b_database_updated;
}

// helper function for redeem_referral_code
// updates authorize.net
// @return: TRUE on success, FALSE otherwise
function redeem_referral_code_authorize_dot_net($a_billing_profiles, $a_billing_profiles_referrer, $i_amount_referree, $i_amount_referrer, $a_revert, $now, $s_dataname, $s_dataname_referrer, $a_restaurant, $a_restaurant_referrer, $s_user, $s_full_username, $i_invoice_id_referree, $i_invoice_id_referrer, $b_award_free_month_to_referree, $b_award_free_month_to_referrer) {
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

	// try to update authorize.net for the referree
	if (count($a_billing_profiles) > 0 && $b_award_free_month_to_referree) {

		// do a call to authorize.net and return false if it doesn't succeed
		$o_authnet = new AuthnetARB(1,2,3);
		$s_reference_id = 11; // just what it has always been, because why not?
		$s_subscribe_id = $a_billing_profiles['subscriptionid'];
		$a_update_vars = array('refid'=>$s_reference_id, 'subscribeid'=>$s_subscribe_id, 'amount'=>$i_amount_referree);
		$a_result = manage_recurring_billing("update_amount", $a_update_vars);
		if ($a_result['success'] == FALSE)
			return FALSE;

		// insert into the billing log that the ARB was pushed back
		$i_skipped_billdate = strtotime(date('Y-m-d',$i_next_billdate_referree).' -1 month');
		$a_insert_vars = array('datetime'=>$now, 'dataname'=>$s_dataname, 'restaurantid'=>$a_restaurant['id'], 'username'=>$s_user, 'fullname'=>$s_full_username, 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'action'=>'Change ARB amount', 'details'=> 'JSON: '.LavuJson::json_encode(array('program'=>'referral', 'referrer'=>$s_dataname_referrer, 'referree'=>$s_dataname, 'success'=>'TRUE', 'comment'=>'Changed the amount of ARB subscription id '.$s_subscribe_id.' to '.$i_amount_referree.'.')), 'billid'=>$i_invoice_id_referree);
		$s_insert_clause = $dbapi->arrayToInsertClause($a_insert_vars);
		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` $s_insert_clause", $a_insert_vars);
	}

	// try to update authorize.net for the referrer
	if (count($a_billing_profiles_referrer) > 0 && $b_award_free_month_to_referrer) {

		// do a call to authorize.net and email tom@lavu.com if it doesn't succeed
		$o_authnet = new AuthnetARB(1,2,3);
		$s_reference_id = 11; // just what it has always been, because why not?
		$s_subscribe_id = $a_billing_profiles_referrer['subscriptionid'];
		$a_update_vars = array('refid'=>$s_reference_id, 'subscribeid'=>$s_subscribe_id, 'amount'=>$i_amount_referrer);
		$a_result = manage_recurring_billing("update_amount", $a_update_vars);
		$i_skipped_billdate = strtotime(date('Y-m-d',$i_next_billdate_referrer).' -1 month');
		if ($a_result['success'] == FALSE) {
			mail('tom@lavu.com', 'failed to update ARB for referrer', 'The restaurant '.$s_dataname.' successfully updated their ARB but the ARB for their referrer, '.$s_dataname_referrer.', could not be updated. Please be sure to update the amount of their ARB to '.$i_amount_referrer." manually.\n\nSubscription id: {$s_subscribe_id}\n\nResponse:\n".print_r($a_result,TRUE)."\n\n".__FILE__);

			// insert into the billing log that ARB update failed
			$a_insert_vars = array('datetime'=>$now, 'dataname'=>$s_dataname_referrer, 'restaurantid'=>$a_restaurant_referrer['id'], 'username'=>$s_user, 'fullname'=>$s_full_username, 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'action'=>'Change ARB amount', 'details'=> 'JSON: '.LavuJson::json_encode(array('program'=>'referral', 'referrer'=>$s_dataname_referrer, 'referree'=>$s_dataname, 'success'=>'FALSE', 'comment'=>'Could not change the amount of ARB subscription id '.$s_subscribe_id, 'response'=>print_r($a_result,TRUE))), 'billid'=>$i_invoice_id_referrer);
			$s_insert_clause = $dbapi->arrayToInsertClause($a_insert_vars);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` $s_insert_clause", $a_insert_vars);
		} else {

			// insert into the billing log that the ARB was pushed back
			$a_insert_vars = array('datetime'=>$now, 'dataname'=>$s_dataname_referrer, 'restaurantid'=>$a_restaurant_referrer['id'], 'username'=>$s_user, 'fullname'=>$s_full_username, 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'action'=>'Change ARB amount', 'details'=> 'JSON: '.LavuJson::json_encode(array('program'=>'referral', 'referrer'=>$s_dataname_referrer, 'referree'=>$s_dataname, 'success'=>'TRUE', 'comment'=>'Changed the amount of ARB subscription id '.$s_subscribe_id.' to '.$i_amount_referrer.'.')), 'billid'=>$i_invoice_id_referrer);
			$s_insert_clause = $dbapi->arrayToInsertClause($a_insert_vars);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` $s_insert_clause", $a_insert_vars);
		}
	}

	return TRUE;
}

// looks at the billing profiles table and waived invoices to find the next bill date
// @$s_dataname: dataname of the restaurant
// @$i_package: package level of the restaurant
// @$s_created_date: created date of the restaurant, based on `restaurants`.`created`
// @$a_signup: the restaurant's row from the signups table
// @$a_billing_profiles: the billing profiles of the restaurant
// @return: the next billing date
function redeem_referral_code_get_next_bill_date($s_dataname, $i_package, $s_created_date, $i_restaurant_id, &$a_signup, &$a_billing_profiles) {

	// try to find any hosting entries from the billing_profiles table
	$i_next_billdate = billing_get_next_bill_date($s_dataname, $i_package, $s_created_date, $i_restaurant_id, $a_signup, $a_billing_profiles, TRUE);
	return strtotime("+1 month", $i_next_billdate);
}

// helper function for redeem_referral_code_reset_arb()
// looks for a log created by redeem_referral_code_set_reset_arb_amount()
// @$s_dataname: the restaurant's dataname to search
// @$s_date: searches for entries that apply on or after this date, or the current date if empty string
// @return: an array(array('dataname'=>dataname, 'subscriptionid'=>subscriptionid, 'date'=>date, 'amount'=>dollar.cents)...) for each one found ordered by date desc, or an empty array() if not found
function redeem_referral_code_check_reset_arb($s_dataname, $s_date = "") {
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
	if ($s_date == '') $s_date = date('Y-m-d');
	$a_entries = $dbapi->getAllInTable('config', array('type'=>'ARB', 'setting'=>'change amount on or after date', 'value'=>$s_date), TRUE,
		array('databasename'=>'poslavu_'.$s_dataname.'_db', 'whereclause'=>"WHERE `type`='[type]' AND `setting`='[setting]' AND `value` <= '[value]' AND `_deleted`='0'", 'selectclause'=>'`id`,`value3` AS `subscriptionid`,`value2` AS `amount`,`value` AS `date`', 'orderbyclause'=>'ORDER BY `date` DESC'));
	foreach($a_entries as $k=>$v)
		$a_entries[$k]['dataname'] = $s_dataname;
	return $a_entries;
}

// looks for referral code reset arb entries and resets the arb to the most resent entry
// deletes all entries that apply on or before $s_date
// @$s_dataname: the restaurant's dataname to search
// @$s_date: searches for entries that apply on or after this date, or the current date if empty string
// @return: either an array(array('subscriptionid'=>subscriptionid, 'amount'=>amount)...) for each subscription id affected, or empty array()
function redeem_referral_code_reset_arb($s_dataname, $s_date = "") {
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

	// get the date and the entries, and set some basic variables
	if ($s_date == '') $s_date = date('Y-m-d');
	$a_entries = redeem_referral_code_check_reset_arb($s_dataname, $s_date);
	$a_affected = array();

	// reset the ARB
	if (count($a_entries) > 0) {

		// get restaurant information
		$a_restaurants = $dbapi->getAllInTable("restaurants", array("data_name"=>$s_dataname), TRUE, array("selectclause"=>"`id`"));
		$i_restaurant_id = $a_restaurants[0]['id'];

		$a_processed_subscription_ids = array();
		$o_authnet = new AuthnetARB(1,2,3);
		foreach($a_entries as $a_entry) {

			// delete the given entry
			mlavu_query("UPDATE `[database]`.`config` SET `_deleted`='1' WHERE `id`='[id]'", array('id'=>$a_entry['id']));

			// get the subscription information and make sure it hasn't already been processed
			$f_amount = $a_entry['amount'];
			$s_subscription_id = $a_entry['subscriptionid'];
			if (in_array($s_subscription_id, $a_processed_subscription_ids)) {
				continue;
			}
			$a_processed_subscription_ids[] = $s_subscription_id;
			$s_reference_id = 11; // just what it has always been, because why not?
			$a_subscriptions = $dbapi->getAllInTable("billing_profiles", array("subscriptionid"=>$s_subscription_id, "dataname"=>$s_dataname, "active"=>"1", "arb_status"=>"active"), TRUE, array("limitclause"=>"LIMIT 1"));

			// check that the subscription is still active
			if (count($a_subscriptions) == 0) {
				continue;
			}
			$a_subscription = $a_subscriptions[0];

			// update the ARB with authorize.net
			$a_update_vars = array('refid'=>$s_reference_id, 'subscribeid'=>$s_subscription_id, 'amount'=>$f_amount);
			$a_result = manage_recurring_billing("update_amount", $a_update_vars);
			$a_affected[] = array('subscriptionid'=>$s_subscription_id, 'amount'=>$f_amount);

			// update the local ARB record
			// and create a log of the transaction
			$query = mlavu_query("UPDATE `poslavu_MAIN_db`.`billing_profiles` SET `amount`='[amount]' WHERE `active`='1' AND `arb_status`='active' AND `subscriptionid`='[subscriptionid]'", array('amount'=>$f_amount, 'subscriptionid'=>$s_subscription_id));
			$a_insert_vars = array("datetime"=>date("Y-m-d H:i:s"), "dataname"=>$s_dataname, "restaurantid"=>$i_restaurant_id, "username"=>"billing", "fullname"=>"billing automaton", "ipaddress"=>$_SERVER['REMOTE_ADDR'], "action"=>"unwaived invoice for referral program", "details"=>'JSON: {"program":"referral","from":"'.$a_subscription['amount'].'","to":"'.$f_amount.'","comment":"reset arb back to original amount","subscriptionid":"'.$s_subscription_id.'"}');
			$s_insertclause = $dbapi->arrayToInsertClause($a_insert_vars);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` {$s_insertclause}", $a_insert_vars);
		}
	}

	// delete the other entries
	mlavu_query("UPDATE `[database]`.`config` SET `_deleted`='1' WHERE `type`='[type]' AND `setting`='[setting]' AND `value` <= '[date]'",
		array('database'=>'poslavu_'.$s_dataname.'_db', 'type'=>'ARB', 'setting'=>'change amount on or after date', 'date'=>$s_date));

	return $a_affected;
}

// updates a setting in the config table to remind billing to reset the ARB amount of the restaurant
// @$s_subscription_id: subscription id to update
// @$s_dataname: dataname of the restaurant to be affected
// @$s_date: date after or on which the ARB is to be reset
// @$a_reverts: the queries to perform to undo any changes made in this function
// @return: TRUE on success, FALSE on failure
function redeem_referral_code_set_reset_arb_amount($s_subscription_id, $s_dataname, $s_date, &$a_reverts) {
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

	// get the monthly hosting amount of the restaurant
	$s_amount = get_current_montly_hosting($s_dataname);

	// try to insert/update the config table
	$a_original_row = array();
	$a_where_vars = array('type'=>'ARB', 'setting'=>'change amount on or after date', 'value3'=>$s_subscription_id);
	$a_update_vars = array_merge($a_where_vars, array('value'=>$s_date, 'value2'=>$s_amount));
	$s_success = ConnectionHub::getConn('poslavu')->setValue($a_where_vars, $a_update_vars, 'config', 'poslavu_'.$s_dataname.'_db');

	// check for success and update $a_reverts
	if (strpos($s_success, 'u') == 0) {
		$s_update_clause = $dbapi->arrayToUpdateClause($a_original_row);
		$a_reverts[] = array('string'=>"UPDATE `poslavu_MAIN_db`.`config` $s_update_clause", 'vars'=>$a_original_row);
		return TRUE;
	}
	if (strpos($s_success, 'i') == 0) {
		$i_inserted = (int)substr($s_success, 1);
		$a_reverts[] = array('string'=>"UPDATE `poslavu_MAIN_db`.`config` SET `_deleted`='1' WHERE `id`='$i_inserted'", 'vars'=>NULL);
		return TRUE;
	}
	return FALSE;
}

?>
