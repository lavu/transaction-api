<?php

	$s_full_filename = realpath(dirname(__FILE__)."/../make_billing_change.php");
	$mbc_code = "";
	ob_start();
	if (!in_array($s_full_filename, get_included_files()))
		require_once($s_full_filename);
	$mbc_code = ob_get_contents();
	ob_end_clean();
	return $mbc_code;

?>