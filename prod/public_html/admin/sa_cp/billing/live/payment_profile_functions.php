<?php

	global $o_emailAddressesMap;
	require_once(dirname(__FILE__)."/../../../distro/reseller_licenses_functions.php");
	require_once(dirname(__FILE__)."/../../../manage/globals/email_addresses.php");
	require_once(dirname(__file__)."/../package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();
	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";

	function get_due_prop($type,$due_info)
	{
		if(strtolower($type)=="due")
			$due_parts = explode("Due since",$due_info);
		else
		{
			$due_parts = explode(strtolower($type) . ": $",strtolower($due_info));
			if(count($due_parts) < 2)
			{
				$due_parts = explode(strtolower($type) . ":",strtolower($due_info),2);
			}
		}

		if(count($due_parts) > 1)
		{
			$due_parts = explode("|",$due_parts[1]);
			return trim(str_replace(",","",$due_parts[0]));
		}
		else return "";
	}

	// attempts to find the most reliable signup address in the signups table
	// this is because some data in the signups table somehow got duplicated
	// returns an empty array() on failure
	function get_signup_by_id_dataname($s_restaurant_id, $s_restaurant_dataname) {

		if (strlen($s_restaurant_id)==0 && strlen($s_restaurant_dataname)==0) {
			return array();
		}

		$where_clause = "";
		if (strlen($s_restaurant_id) > 0) {
			$where_clause = "`restaurantid`='[company_id]'";
		}
		if (strlen($s_restaurant_dataname) > 0) {
			if (!empty($where_clause)) {
				$where_clause .= " OR ";
			}
			$where_clause .= "`dataname`='[dataname]'";
		}

		$w_vars = array(
			"company_id"	=> $s_restaurant_id,
			"dataname"		=> $s_restaurant_dataname
		);

		$signup_read = array();
		$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where ".$where_clause." ORDER BY `id` DESC", $w_vars);
		if ($signup_query === FALSE) {
			mysqli_free_result($signup_query);
			return array();
		}

		if (mysqli_num_rows($signup_query) == 0) {
			mysqli_free_result($signup_query);
			return array();
		}
		$most_reliable_read = NULL;
		while ($temp_signup_read = mysqli_fetch_assoc($signup_query)) {
			if ($most_reliable_read === NULL) {
				unset($most_reliable_read);
				$most_reliable_read = $temp_signup_read;
			}
			if ($temp_signup_read['dataname'] == $s_restaurant_dataname) {
				unset($most_reliable_read);
				$most_reliable_read = $temp_signup_read;
			}
			if ($temp_signup_read['restaurantid'] == $s_restaurant_id && $temp_signup_read['dataname'] == $s_restaurant_dataname) {
				unset($most_reliable_read);
				$most_reliable_read = $temp_signup_read;
				unset($temp_signup_read);
				break;
			}
			unset($temp_signup_read);
		}
		if ($most_reliable_read !== NULL)
			$signup_read = $most_reliable_read;
		unset($most_reliable_read);
		mysqli_free_result($signup_query);
		return $signup_read;
	}

	/**
	 * Computes the package that the restaurant currently has
	 * @param  integer $rest_id    The id of the restaurant from the `restaurants` table
	 * @param  integer $isLavuLite 0 for normal accounts, 1 for lavulite accounts
	 * @return array               array("package"=>package level, "signup_status"=>signup status)
	 *     package level: one of "none", "", 4=>lavu lite, 24=>testing (not used),
	 *         or an numeric where the tens place is the generation of packages and the ones place is 1=>silver, 2=>gold, 3=>silver
	 *         eg 2 => Gold, 11 => Silver 2, 23 => Platinum 3
	 *     signup status: one of "new", "manual", or ""
	 *         where "new" is a self-signup through the signups page, "manual" is a distro created account, and "" is a lavulite account
	 */
	function find_package_status($rest_id, $isLavuLite=0)
	{

		global $o_package_container;
		$package = "";
		$signup_status = "";
		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$rest_id);

		if(mysqli_num_rows($rest_query))

		{

			$rest_read = mysqli_fetch_assoc($rest_query);

			$license_applied = "";
			$license_upgrades = array();
			$stuff = array();
			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]' or `dataname`='[2]'",
			$rest_read['id'],$rest_read['data_name']);
			if(mysqli_num_rows($status_query))
			{
				$status_read = mysqli_fetch_assoc($status_query);
				$license_applied = $status_read['license_applied'];
				require_once(dirname(__FILE__).'/../../../distro/reseller_licenses_functions.php');
				RESELLER_LICENSES::get_extra_upgrades_from_upgrade_string($status_read['extra_upgrades_applied'], $status_read['extra_reseller_ids'], $license_upgrades, $stuff);
				$package = $status_read['current_package'];
				$due_info = $status_read['due_info'];
				unset($status_read);
			}else{
				$due_info = "n/a";
			}
			mysqli_free_result($status_query);

			$signup_read = get_signup_by_id_dataname($rest_read['id'],$rest_read['data_name']);
			/*if ($rest_read['data_name'] == '17edison')
				error_log("signup read: ".print_r($signup_read, TRUE));*/

			if($isLavuLite)
				$package=$o_package_container->get_level_by_attribute('name', 'Lite');

			if(count($signup_read) > 0){//mysqli_num_rows($signup_query)){

				if(trim($signup_read['package'])!="" && !$isLavuLite) $package = trim($signup_read['package']);
				/*if(!is_numeric($package))
				{
					$package = $signup_read['package'];

				}*/

				// find the package based on licenses applied and upgrades applied
				// only check the license applied if the current package is invalid
				// only apply the upgrades if they are of higher value the the current package
				if(!is_numeric($package))
					$package = scrape_package($package, $license_applied, $rest_read['data_name'], $rest_read['id']);
				if(!$isLavuLite)
					$package = RESELLER_LICENSES::choose_upgrades_for_packages($package, $license_upgrades);

				if($package=="")
				{
					//$no_package_list[] = array($rest_read['data_name'],$rest_read['company_name'],$rest_read['created'],$signup_read['status'],$due_info);
					$package = "none";
					$signup_status = $signup_read['status'];
				}
				else if(!is_numeric($package) || $package=="24")
					$package = "other";
				else
				{
					$signup_status = $signup_read['status'];
					//$has_package_list[] = array($rest_read['data_name'],$rest_read['company_name'],$rest_read['created'],$signup_read['status'],$package,$due_info);
				}

				/*if(!isset($signup_packages[$package]))
					$signup_packages[$package] = 0;
				$signup_packages[$package]++;*/
			}else{ //"This means that the signup query failed that means i messed up stuff

				if($isLavuLite){
					//echo "im in her";
					//echo "the rest id is".$rest_id;
					$usernameArray = mysqli_fetch_assoc(mlavu_query("select `email` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'", $rest_read['id']));
					if(isset($usernameArray['email']) && trim($usernameArray['email']) !== ''){
						// check if the signup exists
						$query_id = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`signups` WHERE `username`='[1]'", $usernameArray['username']);
						$b_exists = TRUE;
						if ($query_id === FALSE || $query_id === NULL)
							$b_exists = FALSE;
						else if (mysqli_num_rows($query_id) == 0)
							$b_exists = FALSE;
						if (!$b_exists)
							mlavu_query("INSERT INTO `poslavu_MAIN_db`.`signups` (`restaurantid`,`dataname`,`username`,`package`) VALUES ('[restaurantid]','[dataname]','[username]','[package]')",
								array('restaurantid'=>$rest_read['id'], 'dataname'=>$rest_read['data_name'], "username"=>$rest_read['id'], "package"=>$o_package_container->get_level_by_attribute('name', 'Lite')));
						// update the signup
						$query_result = mlavu_query("update `poslavu_MAIN_db`.`signups` SET `restaurantid`= '[1]',`dataname`='[2]' where `username`='[3]' && `username` != '' ", $rest_read['id'], $rest_read['data_name'], $usernameArray['username']);
						return find_package_status($rest_id, $isLavuLite);
						mysqli_free_result($query_result);
					}
				}
			}
		}
		unset($rest_read);
		unset($signup_read);
		mysqli_free_result($rest_query);

		//echo "the package is ". $package. " the signup_status is ". $signup_status;

		return array("package"=>$package,"signup_status"=>$signup_status);
	}

	function determinePackage($amount, $type) {
		global $o_package_container;

		if ($type=="" && $amount<250) $type = "hosting";
		else if ($type=="") $type = "license";

		$package = 0;

		if (strtolower($type) == "license") {
			$a_license_values = $o_package_container->get_baselevel_min_values(1,1000);
			arsort($a_license_values);
			for ($i = 0; $i < count($a_license_values); $i++)
				if ($amount >= $a_license_values[$i])
					$new_package = $o_package_container->get_level_by_attribute('min_value', $a_license_values[$i]);
			if ($new_package !== 0) $package = $new_package;
			/*if ($amount >= 2400) $package = 3;
			else if ($amount > 1000) $package = 2;
			else if ($amount > 500) $package = 1;*/

		} else if (strtolower($type) == "hosting") {
			$a_license_values = $o_package_container->get_baselevel_min_recurring(1,1000);
			arsort($a_license_values);
			for ($i = 0; $i < count($a_license_values); $i++)
				if ($amount >= $a_license_values[$i])
					$new_package = $o_package_container->get_level_by_attribute('min_recurring', $a_license_values[$i]);
			if ($new_package !== 0) $package = $new_package;
			/*if ($amount >= 80) $package = 3;
			else if ($amount > 40) $package = 2;
			else if ($amount > 10) $package = 1;*/
		}

		return $package;
	}

	function packageToStr($package, $default="") {
		global $o_package_container;
		$s_package_name = $o_package_container->get_plain_name_by_attribute('level',$package);
		if ($s_package_name != '')
			return $s_package_name;
		else
			return $default;
		/*switch ($package) {
			case 1 : return "Silver"; break;
			case 2 : return "Gold"; break;
			case 3 : return "Platinum"; break;
			default : return $default; break;
		}*/
	}

	/**
	 * Get the subscription information for an account based on the signup information and restaurant id.
	 * Gets information from the billing_profiles table.
	 * @param  mysql query resource $signup_read Used to match billing_profiles based on license id/hosting id.
	 * @param  integer              $p_companyid Used to match billing_profiles based on restaurantid.
	 * @return array                             An array of subscription info, in the form:
	 *     array(
	 *         'subscription_ids'=>array(
	 *             array(`subscriptionid`, `start_date`, "", `package`),
	 *             ...
	 *         ),
	 *         'ids_used'=>array(
	 *             `subscriptionid`=>index in the previous 'subscription_ids' array,
	 *             ...
	 *         )
	 *     )
	 */
	function get_signup_subscription_list($signup_read,$p_companyid)
	{

		// get subscription information based on the signup license id and signup hosting id
		$subscription_ids = array();
		$ids_used = array();
		if($signup_read)
		{
			$license_id = $signup_read['arb_license_id'];
			$hosting_id = $signup_read['arb_hosting_id'];
			if($license_id!="") {
				$subscription_ids[] = array($license_id,$signup_read['arb_license_start'],"",0);
				$ids_used[$license_id] = (count($subscription_ids) - 1);
			}
			if($hosting_id!="") {
				$subscription_ids[] = array($hosting_id,$signup_read['arb_hosting_start'],"",0);
				$ids_used[$hosting_id] = (count($subscription_ids) - 1);
			}
		}

		// get subscription information based on matching the restaurant id to the billing profile
		$bp_query = mlavu_query("select * from `poslavu_MAIN_db`.`billing_profiles` where `restaurantid`='[1]'",$p_companyid);
		while($bp_read = mysqli_fetch_assoc($bp_query))
		{
			$profid = $bp_read['subscriptionid'];
			if(!isset($ids_used[$profid]))
			{
				$subscription_ids[] = array($profid,$bp_read['start_date'],"",$bp_read['package']);
				$ids_used[$profid] = (count($subscription_ids) - 1);
			}
		}

		return array("subscription_ids"=>$subscription_ids,"ids_used"=>$ids_used);
	}

	/**
	 * Looks for a payment hostlink and billing profile with the given subscription id
	 * Creates a new billing profile if one doesn't exist with the subscription id, or nothing if one does exist
	 * Creates a new payment hostlin if one doesn't exist with the subscription id, or updates the old hostlink if one does exist
	 * Helper function of create_new_hosting_profile()
	 * @param  integer $p_companyid         ID of the account's `restaurants` row
	 * @param  string  $p_dataname          Dataname of the account
	 * @param  string  $hostlink_type       "license" or "hosting"
	 * @param  integer $subscriptionid      The subscription id provided from authorize.net
	 * @param  string  $subscription_start  The date that the subscription starts charging on
	 * @param  double  $subscription_amount The amount that the subscription is for
	 * @param  integer $package             The package level of the account
	 * @param  string  $firstname           The first name on the credit card
	 * @param  string  $lastname            The last name on the credit card
	 * @param  string  $card_number         The card number
	 * @return none                         N/A
	 */
	function insert_hostlink($p_companyid,$p_dataname,$hostlink_type,$subscriptionid,$subscription_start,$subscription_amount,$package=0,$firstname="",$lastname="",$card_number="")
	{

		// insert or update the payment hostlink to reflect the proper restaurantid, dataname, and package
		// insert a new one if none exist with the same subscription id
		$hlink_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_hostlink` where `profileid`='[subscriptionid]'", array('subscriptionid'=>$subscriptionid));
		if(mysqli_num_rows($hlink_query))
		{
			$result = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_hostlink` SET `restaurantid`='[restaurantid]', `dataname`='[dataname]', `package`='[package]' WHERE `profileid`='[subscriptionid]' AND (`restaurantid`='' OR `dataname`='' OR `package`=0)", array('restaurantid'=>$p_companyid, 'dataname'=>$p_dataname, 'package'=>$package, 'subscriptionid'=>$subscriptionid));
		}
		else
		{
			if ($package == 0) $package = determinePackage($subscription_amount, $hostlink_type);

			$vars = array();
			$vars['restaurantid'] = $p_companyid;
			$vars['dataname'] = $p_dataname;
			$vars['profileid'] = $subscriptionid;
			$vars['profiletype'] = $hostlink_type;
			$vars['profilestart'] = $subscription_start;
			$vars['profileamount'] = $subscription_amount;
			$vars['package'] = $package;
			$q_fields = "";
			$q_values = "";
			$keys = array_keys($vars);
			foreach ($keys as $key) {
				if ($q_fields != "") { $q_fields .= ", "; $q_values .= ", "; }
				$q_fields .= "`$key`";
				$q_values .= "'[$key]'";
			}
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`payment_hostlink` ({$q_fields}) VALUES ({$q_values})", $vars);
		}

		// get the signup id
		$signup_read = get_signup_by_id_dataname($p_companyid, $p_dataname);
		if(count($signup_read) > 0)
		{
			$p_signupid = $signup_read['id'];
		}
		else $p_signupid = "";

		// find a billind profile with the given subscription id
		// insert a new one if none exist
		$bprof_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_profiles` WHERE `subscriptionid`='[subscriptionid]'", array('subscriptionid'=>$subscriptionid));
		if(mysqli_num_rows($bprof_query))
		{
		}
		else
		{
			$last_four = substr($card_number,strlen($card_number) - 4);

			$vars = array();
			$vars['create_date'] = date('Y-m-d H:i:s');
			$vars['subscriptionid'] = $subscriptionid;
			$vars['type'] = $hostlink_type;
			$vars['start_date'] = $subscription_start;
			$vars['last_name'] = $lastname;
			$vars['first_name'] = $firstname;
			$vars['last_four'] = $last_four;
			$vars['amount'] = $subscription_amount;
			$vars['package'] = $package;
			$vars['restaurantid'] = $p_companyid;
			$vars['dataname'] = $p_dataname;
			$vars['signupid'] = $p_signupid;
			$vars['last_pay_date'] = "";
			$vars['active'] = "1";
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_profiles` (`create_date`, `subscriptionid`, `type`, `start_date`, `last_name`, `first_name`, `last_four`, `amount`, `package`, `restaurantid`, `dataname`, `signupid`, `last_pay_date`, `active`) VALUES ('[create_date]', '[subscriptionid]', '[type]', '[start_date]', '[last_name]', '[first_name]', '[last_four]', '[amount]', '[package]', '[restaurantid]', '[dataname]', '[signupid]', '[last_pay_date]', '[active]')",$vars);
		}
	}

	/**
	 * Finds all billing profiles associated with an account,
	 * updates their active status if inconsistent with authorize.net,
	 * and draws a table of the billing profiles.
	 * @param  string  $p_area                       "billing_info" or "customer_billing_info", if customer draw only active profiles
	 * @param  string  $p_dataname                   The account dataname
	 * @param  integer $p_companyid                  The account restaurant id
	 * @param  boolean $allow_cc_change              If TRUE draws the option to change the credit card info
	 * @param  boolean $allow_cancel_profile         If TRUE draws the option to cancel the account
	 * @param  boolean $b_draw_advanced_billing_info If TRUE draws the advanced information for each billing profile
	 */
	function list_billing_profiles($p_area, $p_dataname, $p_companyid, $allow_cc_change=false, $allow_cancel_profile=false, $b_draw_advanced_billing_info = FALSE) {
		require_once(dirname(__FILE__) . "/../../../../register/authnet_functions.php");

		global $p_area;
		global $found_signup_record;
		global $found_active_sub;

		// set some values
		$found_signup_record = true;
		$found_active_sub = false;

		// find any billing profiles
		// quit the function if no billing profiles can be found
		$filter_active = ($p_area == "billing_info")?"":" AND (`active` = '1' or `arb_status`='active')";
		$check_billing_profiles = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_profiles` WHERE (`dataname` = '[2]')$filter_active ORDER BY `start_date` DESC", $p_companyid, $p_dataname);
		if (mysqli_num_rows($check_billing_profiles) <= 0) {
			echo "<tr><td align='center' class='info_value' style='padding:4px 50px 4px 50px;'>No active billing profiles found for this account</td></tr>";
			return;
		}
		$found_active_sub = true;

		// draw the header for the billing profiles table
		echo "<tr><td align='center'><table cellspacing=0 cellpadding=4>";
		echo "<tr>";
		echo "<td class='table_header'>Subscription ID</td><td></td>";
		echo "<td class='table_header'>Card</td><td></td>";
		echo "<td class='table_header'>Amount</td><td></td>";
		echo "<td class='table_header'>Type</td><td></td>";
		echo "<td class='table_header'>Status</td><td></td>";
		echo "<td class='table_header'>Start Date</td><td></td>";
		echo "<td class='table_header'>Last Pay Date</td><td></td>";
		echo "<td class='table_header'>Next Pay Date</td>";
		echo "</tr>";

		while ($profile_info = mysqli_fetch_assoc($check_billing_profiles)) {

			// for lavu admins
			// draw the alert that pops up with the full billing profile info
			$s_advanced_billing_info = '';
			if ($b_draw_advanced_billing_info)
				$s_advanced_billing_info = " onclick=\"alert('".str_replace(array("\n","\r","\n\r"), '\\n', print_r($profile_info,TRUE))."');\"";

			// get the automatic recurring billing status of the billing profile
			// queries authorize.net
			if($profile_info['active']=="1" || $profile_info['arb_status']=="")
			{
				$sub_info = manage_recurring_billing("info",array("refid"=>"15", "subscribeid"=>$profile_info['subscriptionid']));
				if (isset($sub_info['response'])) {
					$status_find = explode("<status>",$sub_info['response']);
					if (count($status_find) > 1) {
						$status_find = explode("</status>", $status_find[1]);
						$substatus = $status_find[0];
					}
				} else {
					$substatus = "unknown";
				}
			} else {
				$substatus = $profile_info['arb_status'];
			}

			// update the database if it's `arb_status` field is inaccurate
			$upvars = array();
			if ($substatus!="active" && $substatus!="unknown" && $profile_info['active']=="1") {
				$upvars['active'] = "0";
				$profile_info['active'] = "0";
			}
			if ($substatus != $profile_info['arb_status']) $upvars['arb_status'] = $substatus;
			if (count($upvars) > 0) {
				$q_update = "";
				$keys = array_keys($upvars);
				foreach ($keys as $key) {
					if ($q_update != "") $q_update .= ", ";
					$q_update .= "`$key` = '[$key]'";
				}
				$upvars['id'] = $profile_info['id'];
				$update_profile = mlavu_query("UPDATE `poslavu_MAIN_db`.`billing_profiles` SET $q_update WHERE `id`  = '[id]' limit 1", $upvars);
			}

			// set some styles
			$bgcolor = "#dddddd";
			$show_status = "Not Active";
			$show_next_pay_date = "";
			$next_pay_date = "";
			$cellstyle = " style='border-bottom:solid 1px #ffffff'";

			// get the next pay date for the billing profile
			get_next_pay_date($profile_info, $next_pay_date, $bgcolor, $show_status, $show_next_pay_date);

			// get the cancel/change cc buttons
			$change_cc_code = "";
			if ($profile_info['active']=="1" && $p_area=="customer_billing_info" && strtolower($profile_info['type'])=='hosting' && $allow_cc_change) {
				$change_cc_code = "<input type='button' value='Change CC' onclick='window.location = \"index.php?mode=billing&change_cc=".$profile_info['subscriptionid']."\"'>";
			}
			$cancel_profile_code = "";
			$change_amount_code = "";
			if ($profile_info['active']=="1" && $p_area=="billing_info" && $allow_cancel_profile)
			{
				$cancel_profile_code = "<input type='button' value='Cancel Profile' onclick='return make_billing_change(false,\"\",\"Cancel Billing Profile\",\"?dn=".$p_dataname."&cancel_profile=".$profile_info['subscriptionid']."\",\"\",\"\",\"\")'>";
				$change_amount_code = "<input type='button' value='Change Amount' onclick='var newAmount = prompt(\"Enter new billing profile amount\", \"{$profile_info['amount']}\"); return newAmount && make_billing_change(false,\"\",\"Change Billing Profile Amount to \"+newAmount,\"?dn=".$p_dataname."&change_profile=".$profile_info['subscriptionid']."&amount=\"+newAmount,\"\",\"\",\"\")'>";
			}

			// draw the billing profile row
			echo "<tr bgcolor='$bgcolor'>";
			echo "<td $cellstyle $s_advanced_billing_info>" . $profile_info['subscriptionid'] . "</td><td $cellstyle>&nbsp;</td>";
			echo "<td $cellstyle>" . $profile_info['last_four'] . "</td><td $cellstyle>&nbsp;</td>";
			echo "<td $cellstyle>$" . number_format($profile_info['amount'], 2) . "</td><td $cellstyle>&nbsp;</td>";
			echo "<td $cellstyle>" . ucwords($profile_info['type']) . "</td><td $cellstyle>&nbsp;</td>";
			echo "<td $cellstyle>" . ucwords($show_status) . "</td><td $cellstyle>&nbsp;</td>";
			echo "<td $cellstyle>" . $profile_info['start_date'] . "</td><td $cellstyle>&nbsp;</td>";
			echo "<td $cellstyle>" . $profile_info['last_pay_date'] . "</td><td $cellstyle>&nbsp;</td>";
			echo "<td $cellstyle>" . $show_next_pay_date . "</td><td $cellstyle>&nbsp;</td>";
			if($change_cc_code!="")
				echo "<td $cellstyle>" . $change_cc_code . "</td><td $cellstyle>&nbsp;</td>";
			if($cancel_profile_code!="")
				echo "<td $cellstyle>" . $cancel_profile_code . "</td>";
			if($change_amount_code!="")
				echo "<td $cellstyle>" . $change_amount_code . "</td>";
			echo "<td $cellstyle></td>";
			echo "</tr>";

		}
		echo "</table></td></tr>";
	}

	// get the next pay day for an account based on the provided billing_profile
	// @$profile_info: the row from billing_profile
	// @$others: updated by this function
	function get_next_pay_date(&$profile_info, &$next_pay_date, &$bgcolor, &$show_status, &$show_next_pay_date) {
		if ($profile_info['active'] == "1") {
			$bgcolor = "#99ff99";
			$show_status = "Active";
			if (strtolower($profile_info['type']) == "hosting") {
				$add_m = 0;
				if (!empty($profile_info['last_pay_date'])) {
					$date_parts = explode("-" ,$profile_info['last_pay_date']);
					$add_m = 1;
				} else $date_parts = explode("-" ,$profile_info['start_date']);
				$next_pay_date = date("Y-m-d", mktime(0, 0, 0, ($date_parts[1] + $add_m), $date_parts[2], $date_parts[0]));
				$ndate_color = "#000000";
				if ($next_pay_date < date("Y-m-d")) $ndate_color = "#660000";
				$show_next_pay_date = "<font color='$ndate_color'>$next_pay_date</font>";
			}
		}
	}

	/**
	 * Update the signups record to reflect the new hosting profile and create a new hosting/billing_profiles
	 * @param  integer $restaurantid   ID of the account's `restaurants` row
	 * @param  string  $p_dataname     Dataname of the account
	 * @param  double  $hosting_amount The amount that the subscription is for
	 * @param  string  $hosting_start  The date that the subscription starts charging on
	 * @param  array   $card_info      Information on the credit card
	 *     including at least "code", "expiration", "number", "first_name", "last_name", "subscribeid", "refid"
	 * @param  array   $customer_info  Information about the customer belonging to the credit card
	 *     including at least "first_name" and "last_name"
	 * @param  string  $card_number    The card number
	 * @param  boolean $b_accept_8484  Allows the test credit card if TRUE, does not allow otherwise
	 * @return array                   One of
	 *     array("success"=>TRUE, "hostingid"=>subscription id from authorize.net)
	 *     array("success"=>FALSE, "response"=>"Invalid hosting amount")
	 *     array("success"=>FALSE, "response"=>response from authorize.net)
	 */
	function create_new_hosting_profile($restaurantid,$dataname,$hosting_amount,$hosting_start,$card_info,$customer_info,$card_number,$b_accept_8484 = FALSE)
	{

		// get the credit card authorization variables
		$vars = build_auth_vars($card_info,$customer_info);

		// check that the input is valid
		if(!$hosting_amount!="" || $hosting_amount <= 0)
			return array("success"=>FALSE, "response"=>"Invalid hosting amount");

		// sanitize the input
		if($hosting_start=="" || $hosting_start <= date("Y-m-d"))
		{
			$start_ts = mktime(0,0,0,date("m"),date("d") + 1,date("Y"));
			$hosting_start = date("Y-m-d",$start_ts);
		}
		if(trim($vars['first_name'])=="")
			$vars['first_name'] = admin_info("fullname");
		if(trim($vars['last_name'])=="")
			$vars['last_name'] = "Cust";
		if(trim($vars['company_name'])=="")
			$vars['company_name'] = admin_info("company_name");

		// set some variables to create a billing profile with authorize.net
		$vars['refid'] = 16;
		$vars['start_date'] = $hosting_start;
		$vars['total_occurrences'] = 9999;
		$vars['amount'] = $hosting_amount;
		$vars['description'] = "POSLavu Hosting R" . $restaurantid;
		$b_accept_test_cc = $b_accept_8484 && $card_number == '8484' && isset($card_info['code']) && $card_info['code'] == '8484' && isset($card_info['expiration']) && $card_info['expiration'] == '0213' && ($_SERVER['REMOTE_ADDR'] == '74.95.18.90' || $_SERVER['REMOTE_ADDR'] == '173.10.247.198');

		global $signup_read;
		if ( isset($signup_read['source']) && strpos($signup_read['source'], '_3mo8') !== false ) {
			global $o_emailAddressesMap;

			list($signup_date, $signup_timestamp) = explode(' ', $signup_read['signup_timestamp']);
			$hosting_start_ts = strtotime($hosting_start);
			$first_3mo8_hosting_invoice_ts  = strtotime($signup_date);
			$second_3mo8_hosting_invoice_ts = strtotime('+1 month',  $first_3mo8_hosting_invoice_ts);
			$third_3mo8_hosting_invoice_ts  = strtotime('+2 months', $first_3mo8_hosting_invoice_ts);

			// after first hosting invoices but before second
			if ( $hosting_start_ts >= $first_3mo8_hosting_invoice_ts  &&  $hosting_start_ts <= $second_3mo8_hosting_invoice_ts ) {
				$vars['trial_amount'] = 8;
				$vars['trial_occurrences'] = 2;
			}
			// after second hosting invoice but before third
			else if ( $hosting_start_ts >= $second_3mo8_hosting_invoice_ts  &&  $hosting_start_ts <= $third_3mo8_hosting_invoice_ts ) {
				$vars['trial_amount'] = 8;
				$vars['trial_occurrences'] = 1;
			}

			$br = "\n";
			$o_emailAddressesMap->sendEmailToAddress("billing_notifications", "3mo8 cc change", "dataname={$dataname}{$br}signup_date={$signup_date}{$br}hosting_start={$hosting_start}{$br}first_3mo8_hosting_invoice=". date('Y-m-d', $first_3mo8_hosting_invoice_ts) ."{$br}second_3mo8_hosting_invoice=". date('Y-m-d', $second_3mo8_hosting_invoice_ts) ."{$br}third_3mo8_hosting_invoice=". date('Y-m-d', $third_3mo8_hosting_invoice_ts) ."{$br}vars[trial_amount]=". $vars['trial_amount'] ."{$br}vars[trial_occurrences]=". $vars['trial_occurrences'], "From: billing@poslavu.com");
		}

		// send a request to authorize.net and store the returned result
		if ($b_accept_test_cc) {
			$hosting_result = array('success'=>TRUE, 'id'=>'0', 'response'=>'testing - Ben Bean');
		} else {
			$hosting_result = manage_recurring_billing("create",$vars);
		}

		// update the signups record to reflect the new hosting profile and create a new hosting/billing_profiles
		if($hosting_result['success'])
		{
			$a_signup_data = get_signup_by_id_dataname($restaurantid, $dataname);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `arb_hosting_id`='[1]', `arb_hosting_start`='[2]' WHERE `id`='[3]'",$hosting_result['id'],$hosting_start,$a_signup_data['id']);
			insert_hostlink($restaurantid,$dataname,"hosting",$hosting_result['id'],$hosting_start,$hosting_amount,determinePackage($hosting_amount,"hosting"),$customer_info['first_name'],$customer_info['last_name'],$card_number);
			return array("success"=>true,"hostingid"=>$hosting_result['id']);
		}
		else
		{
			return array("success"=>false,"response"=>$hosting_result['response']);
		}
	}

	function lavulite_communicate_disabled_status($dataname)
	{
		$post_url = "https://admin.lavulite.com/mainconnect/pstatus.php";

		$d_query = mlavu_query("select `disabled` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
		if(mysqli_num_rows($d_query))
		{
			$d_read = mysqli_fetch_assoc($d_query);
			$site_disabled = $d_read['disabled'];
		}
		else $site_disabled = 0;
		$set_disabled_info = $site_disabled;

		$post_string = "rcount=1&rid1=".urlencode($rest_id)."&dn1=".urlencode($dataname)."&disabled1=".urlencode($set_disabled_info);

		//mail("corey@poslavu.com","sending","post_url: $post_url\npost_string: $post_string\n","From:pstatus@poslavu.com");
		$ch = curl_init($post_url); // initiate curl object
		curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		$post_response = curl_exec($ch); // execute curl post and store results in $post_response
		//echo "post response: $post_response<br>";
		//echo curl_error($ch) . "<br>";
		curl_close($ch); // close curl object
	}

	function scrape_package($s_package_current, $license_applied, $s_dataname, $s_id) {
		$package = $s_package_current;
		global $o_package_container;

		// check for reseller licenses applied
		if(!is_numeric($package) && $license_applied!="" && is_numeric($license_applied))
		{
			$new_package_name = RESELLER_LICENSES::get_package_name_by_license_id($license_applied);
			if ($new_package_name !== '')
				if (in_array($new_package_name, $o_package_container->get_not_lavulite_names()))
					$package = $o_package_container->get_level_by_attribute('name', $new_package_name)*1;
		}

		// check for payment requests granting the license
		if(!is_numeric($package))
		{
			$request_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_request` where `dataname`='[1]' and LEFT(`action`,5)='Grant'",$s_dataname);
			if(mysqli_num_rows($request_query))
			{
				$request_read = mysqli_fetch_assoc($request_query);
				$s_action_type = $request_read['action'];
				$s_type = $o_package_container->get_mapped_license_name($s_action_type);
				if (in_array($s_type, $o_package_container->get_not_lavulite_names()))
					$package = $o_package_container->get_level_by_attribute('name', $s_type)*1;
			}
		}

		// check for license payments on record
		if(!is_numeric($package))
		{
			$amount_paid = 0;
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit') and `x_amount` * 1 > 300",$s_id);
			while($response_read = mysqli_fetch_assoc($response_query))
			{
				if($response_read['x_type']=="credit")
					$amount_paid -= $response_read['x_amount'] * 1;
				else
					$amount_paid += $response_read['x_amount'] * 1;
			}

			$a_license_values = $o_package_container->get_baselevel_min_values(1,1000);
			arsort($a_license_values);
			foreach($a_license_values as $i_license_value) {
				if ($amount_paid >= $i_license_value) {
					$package = $o_package_container->get_level_by_attribute('min_recurring', $i_license_value);
					break;
				}
			}
			if ($new_package !== 0) $package = $new_package;
		}

		// check for hosting payment requests
		if(!is_numeric($package))
		{
			$request_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_request` where `dataname`='[1]' and `hosting_amount` * 1 > 10",$s_dataname);
			if(mysqli_num_rows($request_query))
			{
				$request_read = mysqli_fetch_assoc($request_query);
				$hosting_amount = $request_read['hosting_amount'] * 1;
				$a_license_values = $o_package_container->get_baselevel_min_recurring(1,1000);
				arsort($a_license_values);
				foreach($a_license_values as $i_license_value) {
					if ($hosting_amount >= $i_license_value) {
						$package = $o_package_container->get_level_by_attribute('min_recurring', $i_license_value);
						break;
					}
				}
				if ($new_package !== 0) $package = $new_package;
			}
		}

		// check for hosting or auth_only payments
		if(!is_numeric($package))
		{
			$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='auth_only') and `x_amount` *1 > 10 and `x_amount` * 1 < 110 order by id desc",$s_id);
			if(mysqli_num_rows($response_query))
			{
				$response_read = mysqli_fetch_assoc($response_query);
				$auth_amount = $response_read['x_amount'];

				if($auth_amount >= 80)
					$package = 3;
				else if($auth_amount > 40)
					$package = 2;
				else if($auth_amount > 10)
					$package = 1;
			}
		}

		return $package;
	}

	// upgrades a distributor's customer's licensing from their current license to their new license
	// applies an invoice to the account and also applies the upgrade to the account
	// @return one of "success", "upgrade already applied", "license is not an upgrade", "restaurant dataname doesn't exist", "payment status doesn't exist", "payment status couldn't be updated", "upgrade doesn't exists", or "upgrade not applicable to this account"
	function apply_upgrade($s_dataname, $i_reseller_license_id, $s_current_package = NULL) {
		global $maindb;
		global $o_package_container;

		// get the license data
		$license_query_string = "SELECT * FROM `[maindb]`.`reseller_licenses` WHERE `id`='[license_id]' LIMIT 1";
		$license_query = mlavu_query($license_query_string, array("maindb"=>$maindb, "license_id"=>$i_reseller_license_id));
		if (!$license_query)
			return "upgrade doesn't exist";
		if (!mysqli_num_rows($license_query))
			return "upgrade doesn't exist";
		$a_license_data = mysqli_fetch_assoc($license_query);
		if ($o_package_container->get_package_status($a_license_data['type']) != "upgrade")
			return "license is not an upgrade";
		$a_upgrade_type_parts = $o_package_container->get_upgrade_old_new_names($a_license_data['type']);
		$s_upgrade_from = $a_upgrade_type_parts[0];
		$s_upgrade_to = $a_upgrade_type_parts[1];

		// check that the license hasn't yet been applied
		if ($a_license_data['applied'] != '')
			return "upgrade already applied";

		// apply the upgrade to the payment_status table
		$a_extra_upgrades_resellers = get_extra_upgrades_and_resellers_and_id($s_dataname);
		$a_extra_upgrades_applied = $a_extra_upgrades_resellers['extra_upgrades'];
		$a_extra_reseller_ids = $a_extra_upgrades_resellers['extra_resellers'];
		$i_payment_status_id = (int)$a_extra_upgrades_resellers['id'];
		if (in_array($a_license_data['id'], $a_extra_upgrades_applied))
			return "upgrade already applied";
		$a_extra_upgrades_applied[] = $a_license_data['id'];
		$a_extra_reseller_ids[] = $a_license_data['resellerid'];
		$s_extra_upgrades_string = implode('|', $a_extra_upgrades_applied);
		$s_extra_reseller_string = implode('|', $a_extra_reseller_ids);
		$s_payment_status_string = "UPDATE `[maindb]`.`payment_status` SET `extra_upgrades_applied`='[extra_upgrades_applied]',`extra_reseller_ids`='[extra_reseller_ids]' WHERE `id`='[id]'";
		$a_payment_status_vars = array("maindb"=>$maindb, "extra_upgrades_applied"=>$s_extra_upgrades_string, "extra_reseller_ids"=>$s_extra_reseller_string, "id"=>$i_payment_status_id);
		mlavu_query($s_payment_status_string, $a_payment_status_vars);
		if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
			return "payment status couldn't be updated";

		// check that upgrade is possible
		$s_upgrade_ok = authorize_upgrade_account_to_new_license($s_dataname, $s_upgrade_from, $s_upgrade_to, $s_current_package);
		if ($s_upgrade_ok != "upgrade ok")
			return $s_upgrade_ok;

		// apply the upgrade and the invoice to the restaurant
		$s_retval = upgrade_account_to_new_license($s_dataname, $s_upgrade_from, $s_upgrade_to, "distro:".$a_license_data['resellername'], $a_license_data['value'], TRUE);
		if (strpos($s_retval, '|')) {
			$a_retval_parts = explode('|', $s_retval);
			$s_retval = $a_retval_parts[1];
		}

		// remove the license from the distributor
		if ($s_retval == "success") {
			$s_restaurant_id = "0";
			$s_restaurant_query_string = "SELECT `id` FROM `[maindb]`.`restaurants` WHERE `data_name`='[data_name]' LIMIT 1";
			$a_restaurant_query_vars = array("maindb"=>$maindb, "data_name"=>$s_dataname);
			$restaurant_query = mlavu_query($s_restaurant_query_string, $a_restaurant_query_vars);
			if ($restaurant_query)
				if (mysqli_num_rows($restaurant_query))
					$a_restaurant_data = mysqli_fetch_assoc($restaurant_query);
			if(count($a_restaurant_data))
				$s_restaurant_id = $a_restaurant_data['id'];
			$s_reseller_licenses_string = "UPDATE `[maindb]`.`reseller_licenses` SET `applied`='[date]',`restaurantid`='[restaurantid]' WHERE `id`='[license_id]'";
			$a_reseller_licenses_vars = array("maindb"=>$maindb, "date"=>date("Y-m-d h:m:i"), "restaurantid"=>$s_restaurant_id, "license_id"=>$i_reseller_license_id);
			mlavu_query($s_reseller_licenses_string, $a_reseller_licenses_vars);
		}
		return $s_retval;
	}

	// returnes "upgrade ok" or an error message
	function authorize_upgrade_account_to_new_license($s_dataname, $s_upgrade_from, $s_upgrade_to, $s_current_package = NULL) {
		global $maindb;
		global $o_package_container;

		// check that the restaurant exists and get it's id
		$restaurant_query_string = "SELECT `id` FROM `[maindb]`.`restaurants` WHERE `data_name`='[data_name]' LIMIT 1";
		$restaurant_query_vars = array("maindb"=>$maindb, "data_name"=>$s_dataname);
		$restaurant_query = mlavu_query($restaurant_query_string, $restaurant_query_vars);
		if (!$restaurant_query)
			return "restaurant dataname doesn't exist";
		if (!mysqli_num_rows($restaurant_query))
			return "restaurant dataname doesn't exist";
		$a_restaurant_data = mysqli_fetch_assoc($restaurant_query);
		$s_restaurant_id = $a_restaurant_data['id'];

		// get the restaurant's current package (assume that they're not lavulite)
		// and check that the upgrade is applicable
		if ($s_current_package === NULL) {
			$a_package_data = find_package_status($s_restaurant_id);
			$s_current_package = $a_package_data['package'];
		}
		$s_current_package = packageToStr($s_current_package);
		$s_temp_upgrade_from = preg_replace('/[0-9]/', '' , $s_upgrade_from);
		$s_temp_current_package = preg_replace('/[0-9]/', '' , $s_current_package);
		if ($s_temp_current_package !== $s_temp_upgrade_from) {
			error_log('upgrade from '.$s_upgrade_from.' to '.$s_upgrade_to.', but the current package is '.$s_current_package);
			return "upgrade not applicable to this account";
		}

		return "upgrade ok";
	}

	// applies the upgrade
	// inserts/updates the invoice log
	function upgrade_account_to_new_license($s_dataname, $s_upgrade_from, $s_upgrade_to, $s_username, $i_upgrade_cost, $b_ignore_signup=FALSE) {
		global $maindb;
		global $o_package_container;
		global $o_emailAddressesMap;

		$umsg = "dataname: $s_dataname\nupgrade from: $s_upgrade_from\nupgrade to: $s_upgrade_to\nusername: $s_username\nupgrade cost: $i_upgrade_cost\nipaddress: " . $_SERVER['REMOTE_ADDR'] . "\ndatetime: " . date("Y-m-d H:i:s");
		if ($o_emailAddressesMap->sendEmailToAddress("upgrade_success_emails","Upgrade for $s_dataname",$umsg,"From: upgrades@poslavu.com"))
		    error_log("successfully sent email");
		else
			error_log("failed to send email about upgrade: {$umsg}");

		// get restaurant/package data
		$i_current_package = $o_package_container->get_level_by_attribute('name', $s_upgrade_from);
		$i_new_package = $o_package_container->get_level_by_attribute('name', $s_upgrade_to);
		$restaurant_query = mlavu_query("SELECT * FROM `[maindb]`.`restaurants` WHERE `data_name`='[dataname]'",
			array("maindb"=>$maindb, "dataname"=>$s_dataname));
		if (!$restaurant_query)
			return "failure|database returned false for restaurant";
		if (mysqli_num_rows($restaurant_query) == 0)
			return "failure|restaurant not found";
		$a_restaurant = mysqli_fetch_assoc($restaurant_query);
		$s_restaurant_id = $a_restaurant['id'];
		$s_restaurant_email = '';
		$s_restaurant_company_name = $a_restaurant['company_name'];
		$s_restaurant_distro_code = $a_restaurant['distro_code'];
		if (isset($a_restaurant['email']))
			$s_restaurant_email = $a_restaurant['email'];

		// get the user full name to insert into the billing log data
		$s_user_fullname = '';
		if (strpos($s_username, "restaurant:") == 0) {
			$a_db_username = explode("restaurant:", $s_username);
			$s_db_username = $a_db_username[1];
		} else if (strpos($s_username, "distro:") == 0) {
			$a_db_username = explode("distro:", $s_username);
			$s_db_username = $a_db_username[1];
		} else {
			$s_db_username = $s_username;
		}
		if (strlen($s_db_username)) {
			$s_safe_dataname = ConnectionHub::getConn('poslavu')->escapeString($s_dataname);
			$user_query_string = "SELECT `f_name`,`l_name` FROM `[database]`.`users` WHERE `username`='[username]'";
			$user_query_vars = array("database"=>"poslavu_".$s_safe_dataname."_db", "username"=>$s_db_username);
			error_log("ben_dev, billing: database(".$user_query_vars['database'].") username(".$user_query_vars['username'].")");
			$user_query = mlavu_query($user_query_string, $user_query_vars);
			if ($user_query)
				if ($a_user = mysqli_fetch_assoc($user_query))
					$s_user_fullname = $a_user['f_name']." ".$a_user['l_name'];
		}

		// apply the license
		// get the signup
		$a_signup_data = get_signup_by_id_dataname($s_restaurant_id, $s_dataname);
		if(!$b_ignore_signup) {
			if (count($a_signup_data) == 0)
				return "failure|signup not found";
			if ((int)$a_signup_data['package'] != $i_current_package)
				return "failure|signup package level doesn't match current level";
		}

		// change the license
		ob_start();
		$make_change_success = change_package($s_dataname, $s_dataname, $i_new_package, $s_restaurant_id);
		$contents_to_discard = ob_get_contents();
		ob_end_clean();
		if (!$make_change_success)
			return "failure|unable to change package";

		// apply the invoice
		// check if a previous invoice exists
		$invoice_query = mlavu_query("SELECT * FROM `[maindb]`.`invoices` WHERE `dataname`='[dataname]' AND `due_date`='[due_date]' AND `type`='License'",
			array("maindb"=>$maindb, "dataname"=>$s_dataname, "due_date"=>date("Y-m-d")));
		$a_invoice = array();
		if($invoice_query)
			if(mysqli_num_rows($invoice_query))
				$a_invoice = mysqli_fetch_assoc($invoice_query);
		// update it
		error_log(print_r($a_invoice, TRUE));
		if (count($a_invoice) > 0 && $i_upgrade_cost > 0) {
			$i_current_amount = (float)$a_invoice['amount'];
			$i_new_amount = $i_current_amount+$i_upgrade_cost;
			error_log("current amount: ".$i_current_amount." upgrade cost:".$i_upgrade_cost." new cost:".$i_new_amount);
			$s_query_string = "UPDATE `[maindb]`.`invoices` SET `amount`='[new_amount]' WHERE `id`='[id]'";
			$a_query_vars = array("maindb"=>$maindb, "new_amount"=>$i_new_amount, "id"=>$a_invoice['id']);
			mlavu_query($s_query_string, $a_query_vars);
		// insert it
		} else if ($i_upgrade_cost > 0) {
			$s_query_string = "INSERT INTO `[maindb]`.`invoices` (`due_date`,`dataname`,`restaurantid`,`amount`,`type`) VALUES ('[duedate]','[dataname]','[restaurantid]','[amount]','[type]')";
			$a_query_vars = array("maindb"=>$maindb, "duedate"=>date("Y-m-d"), "dataname"=>$s_dataname, "restaurantid"=>$s_restaurant_id, "amount"=>$i_upgrade_cost, "type"=>"License");
			mlavu_query($s_query_string, $a_query_vars);
		}

		// email the distributor
		$i_distro_commission = 0;
		if ($s_restaurant_distro_code != '') {
			$a_reseller = get_reseller_from_distro_code($s_restaurant_distro_code);
			require_once(dirname(__FILE__)."/../../../distro/beta/distro_data_functions.php");
			$i_distro_commission = get_distro_commision($a_reseller);
			$s_distro_email = $a_reseller['email'];
			$s_full_name = $a_reseller['f_name'].' '.$a_reseller['l_name'];
			$a_license_upgrade_parts = array($s_upgrade_from, $s_upgrade_to);
		}

		// update the signups table
		if (count($a_signup_data) > 0) {
			mlavu_query("UPDATE `[maindb]`.`signups` SET `package`='[new_package]' WHERE `id`='[id]'",
				array("maindb"=>$maindb, "new_package"=>$i_new_package, "id"=>$a_signup_data['id']));
		}

		// make the billing log
		if (count($a_signup_data) > 0) {
			$s_invoice_id = isset($a_invoice['id']) ? $a_invoice['id'] : '';
			mlavu_query("INSERT INTO `[maindb]`.`billing_log` (`datetime`,`dataname`,`restaurantid`,`username`,`fullname`,`ipaddress`,`action`,`details`,`billid`) VALUES
				('[datetime]','[dataname]','[restaurantid]','[username]','[fullname]','[ipaddress]','[action]','[details]','[billid]')",
				array("maindb"=>$maindb, "datetime"=>date("Y-m-d h:m:i"), "dataname"=>$s_dataname, "restaurantid"=>$s_restaurant_id, "username"=>$s_username, "fullname"=>$s_user_fullname, "ipaddress"=>$_SERVER['REMOTE_ADDR'], "action"=>'Package was Changed to '.$s_upgrade_to, "details"=>"upgraded from ".$s_upgrade_from." to ".$s_upgrade_to.", distro commission $i_distro_commission, distributor $s_restaurant_distro_code", "billid"=>$i_invoice_id));
		}

		return "success";
	}

	// updates the signups, payment_status table, restaurant table, and locations table to reflect the new package level
	// pushes the new package to the LLS (or prepares the accout for when the LLS is created)
	// @$dataname: dataname of the restaurant
	// @$dn: same as the dataname (I don't know why the same data is required twice)
	// @$i_new_package: new package level
	// @$restaurantid: id of the restaurant
	// @return: TRUE on success, FALSE on failure
	function change_package($dataname, $dn, $i_new_package, $restaurantid) {
		global $o_package_container;

		// update the signups table
		$a_signup_data = get_signup_by_id_dataname($restaurantid, $dataname);
		$i_old_package = (int)$a_signup_data['package'];
		$success = mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `package`='[1]' WHERE `dataname`='[2]' ORDER BY `id` DESC", $i_new_package, $dataname);
		// create a row for the signup, if it doesn't exist
		if ( ($i_old_package != $i_new_package) && (!$success || ConnectionHub::getConn('poslavu')->affectedRows() == 0) ) {
			$exists_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[1]' ORDER BY `id` DESC", $dataname);
			if ($exists_query) {
				if (mysqli_num_rows($exists_query)) {
					$restaurant_data_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $dataname);
					if (mysqli_num_rows($restaurant_data_query)) {
						$restaurant_data = mysqli_fetch_assoc($restaurant_data_query);
						$action_log_query = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`action_log` WHERE `time` != '' LIMIT 1", $dataname);
						$first_action_time = date("Y-m-d")." 00:00:00";
						if ($action_log_query) {
							if (mysqli_num_rows($action_log_query)) {
								$action_log = mysqli_fetch_assoc($action_log_query);
								$first_action_time = date("Y-m-d", strtotime($action_log['time']))." 00:00:00";
							}
						}

						$a_vars = array('package'=>$i_new_package, 'status'=>'', 'company'=>$restaurant_data['company_name'], 'firstname'=>$dataname, 'lastname'=>$dataname, 'email'=>$restaurant_data['email'], 'phone'=>$restaurant_data['phone'], 'dataname'=>$dataname, 'restaurantid'=>$restaurantid);
						mlavu_query("INSERT INTO `poslavu_MAIN_db`.`signups` (`package`,`status`,`company`,`firstname`,`lastname`,`email`,`phone`,`dataname`,`restaurantid`) VALUES ('[package]','[status]','[company]','[firstname]','[lastname]','[email]','[phone]','[dataname]','[restaurantid]')", $a_vars);
					}
				}
			}
		}
		if ( ($i_old_package != $i_new_package) && (!$success || ConnectionHub::getConn('poslavu')->affectedRows() == 0) ) {
			echo "Can't update package. Missing signups data. Please contact customer service.<br>";
			return FALSE;
		}

		$pstat_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname` = '[1]' order by id asc limit 1",$dataname);
		if (mysqli_num_rows($pstat_query)) {
			$success = mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `current_package` = '[1]' where `dataname` = '[2]'", $i_new_package, $dataname);
			if (!$success) {
				echo "Can't update package<br>";
				return FALSE;
			} else {
				echo "Updated package to ". $o_package_container->get_printed_name_by_attribute('level', $i_new_package) ."<br>";
			}
		} else {
			$success = mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`dataname`,`restaurantid`,`current_package`) values ('[1]','[2]','[3]')", $dataname, $restaurantid, $i_new_package);
			if($success) {
				echo "Success: $dataname $restaurantid $i_new_package<br>";
			} else {
				echo "Failed to create payment status record<br>";
				return FALSE;
			}
		}

		$update_main_record = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `package_status` = '[1]' WHERE `data_name` = '[2]'", packageToStr($i_new_package), $dn);
		$update_location_record = mlavu_query("UPDATE `poslavu_[1]_db`.`locations` SET `product_level` = '[2]'", $dn, $i_new_package);

		require_once(dirname(__FILE__)."/../../../manage/misc/LLSFunctions/schedule_msync.php");
		LLSFunctionsScheduleMsync::setPackage($dataname, $i_new_package);

		return TRUE;
	}

	// removes a distributor's license from the restaurant in the payment_status table
	// also removes the restaurantid and applied fields from the reseller_licenses table
	// returns:
	//     "success" or a number of error messages
	function remove_distro_license_from_restaurant($i_license_id, $s_remove_reason) {
		global $maindb;
		global $o_package_container;

		$i_license_id = (int)$i_license_id;

		// get the license status
		$license_query = mlavu_query("SELECT * FROM `[maindb]`.`reseller_licenses` WHERE `id`='[id]'",
			array("maindb"=>$maindb, "id"=>$i_license_id));
		if ($license_query === FALSE)
			return "bad license query";
		if (mysqli_num_rows($license_query) == 0)
			return "license not found";
		$a_license_read = mysqli_fetch_assoc($license_query);
		$i_restaurant_id = (int)$a_license_read['restaurantid'];
		$s_license_type = $a_license_read['type'];
		$i_reseller_id = (int)$a_license_read['resellerid'];
		$s_reseller_name = $a_license_read['resellername'];
		$s_license_notes = $a_license_read['notes'];

		// get the restaurant status
		$restaurant_query = mlavu_query("SELECT * FROM `[maindb]`.`restaurants` WHERE `id`='[id]'",
			array("maindb"=>$maindb, "id"=>$i_restaurant_id));
		if ($restaurant_query === FALSE)
			return "bad restaurant query";
		if (mysqli_num_rows($restaurant_query) == 0)
			return "restaurant not found";
		$restaurant_read = mysqli_fetch_assoc($restaurant_query);
		$s_restaurant_dataname = $restaurant_read['data_name'];
		$i_restaurant_id = (int)$restaurant_read['id'];

		// get the payment_status status
		$payment_status_query = mlavu_query("SELECT * FROM `[maindb]`.`payment_status` WHERE `dataname`='[dataname]'",
			array("maindb"=>$maindb, "dataname"=>$s_restaurant_dataname));
		if ($payment_status_query === FALSE)
			return "bad payment_status query";
		if (mysqli_num_rows($payment_status_query) == 0)
			return "payment_status not found";
		$payment_status_read = mysqli_fetch_assoc($payment_status_query);
		$i_payment_status_id = (int)$payment_status_read['id'];
		$i_license_applied = (int)$payment_status_read['license_applied'];

		// get extra upgrades and resellers
		$a_extra_upgrades_applied = array();
		$a_extra_reseller_ids = array();
		RESELLER_LICENSES::get_extra_upgrades_from_upgrade_string($payment_status_read['extra_upgrades_applied'], $payment_status_read['extra_reseller_ids'], $a_extra_upgrades_applied, $a_extra_reseller_ids);

		// remove the license from the account
		$i_max_license_level = 0;
		$b_is_upgrade = FALSE;
		$a_other_extra_upgrades_ids = array(); // ids of the other upgrades to remove
		if ($i_license_applied == $i_license_id) {
			// license is a license, not an upgrade
			$i_max_license_level = (int)$o_package_container->get_level_by_attribute('name', $s_license_type);
			foreach($a_extra_upgrades_applied as $k=>$i_upgrade_id) {
				$license_query = mlavu_query("SELECT * FROM `[maindb]`.`reseller_licenses` WHERE `id`='[id]'",
					array("maindb"=>$maindb, "id"=>$i_upgrade_id));
				if ($license_query === FALSE)
					continue;
				if (mysqli_num_rows($license_query) == 0)
					continue;
				$a_license_read = mysqli_fetch_assoc($license_query);
				$s_license_name = $a_license_read['type'];
				$a_upgrade_names = $o_package_container->get_upgrade_old_new_names($s_license_name);
				$s_upgrade_to = $a_upgrade_names[1];
				$i_upgrade_level = (int)$o_package_container->get_level_by_attribute('name', $s_upgrade_to);
				$i_max_license_level = max($i_max_license_level, $i_upgrade_level);
				$s_upgrade_notes = $a_license_read['notes'];
				$a_extra_upgrades_applied[$k] = array($i_upgrade_id, $s_upgrade_notes);
			}
			error_log('payment status id');
			error_log($i_payment_status_id);
			error_log($maindb);
			mlavu_query("UPDATE `[maindb]`.`payment_status` SET `license_applied`='',`license_resellerid`='',`extra_upgrades_applied`='',`extra_reseller_ids`='' WHERE `id`='[id]'",
				array("maindb"=>$maindb, "id"=>$i_payment_status_id));
		} else {
			// license is an upgrade
			$b_is_upgrade = TRUE;
			$a_removed_upgrades = array();
			$i_license_level = RESELLER_LICENSES::get_upgrade_level_by_license_id($i_license_id);
			foreach($a_extra_upgrades_applied as $k=>$i_upgrade_id) {
				// get information about the license
				$i_upgrade_level = RESELLER_LICENSES::get_upgrade_level_by_license_id($i_upgrade_id);
				// if the license is equivelant to or of greater value than $i_license_id, remove it
				if (RESELLER_LICENSES::choose_package_for_packages($i_license_level, $i_upgrade_level) == $i_upgrade_level) {
					$a_upgrade = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_licenses', array('id'=>$i_upgrade_id), TRUE,
						array('selectclause'=>'notes'));
					$a_removed_upgrades[] = array('id'=>$i_upgrade_id, 'notes'=>$a_upgrade[0]['notes']);
					unset($a_extra_upgrades_applied[$k]);
					unset($a_extra_reseller_ids[$k]);
				}
			}
			$s_extra_upgrades_applied = implode('|', $a_extra_upgrades_applied);
			$s_extra_reseller_ids = implode('|', $a_extra_reseller_ids);
			mlavu_query("UPDATE `[maindb]`.`payment_status` SET `extra_upgrades_applied`='[extra_upgrades]',`extra_reseller_ids`='[extra_resellers]' WHERE `id`='[id]'",
				array("maindb"=>$maindb, "id"=>$i_payment_status_id, "extra_upgrades"=>$s_extra_upgrades_applied, "extra_resellers"=>$s_extra_reseller_ids));
		}
		if (ConnectionHub::getConn('poslavu')->affectedRows() == 0) {
			error_log("failed to remove the license $i_license_id from $s_restaurant_dataname");
			return "failed to remove the license $i_license_id from $s_restaurant_dataname";
		}

		// remove the package from the signups table
		// don't actually remove the package (whoops)
		/*$a_signup = get_signup_by_id_dataname($i_restaurant_id, $s_restaurant_dataname);
		if (count($a_signup) > 0)
			mlavu_query("UPDATE `[maindb]`.`signups` SET `package`='' WHERE `id`='[id]'",
				array('maindb'=>$maindb, 'id'=>$a_signup['id']));*/

		// give the license back to the reseller
		if (!$b_is_upgrade && $i_max_license_level > 0) {
			// create the new (combined) license
			$s_new_license_name = $o_package_container->get_plain_name_by_attribute('level', $i_max_license_level);
			$i_new_license_value = $o_package_container->get_value_by_attribute('level', $i_max_license_level);
			$a_insert_vars = array('maindb'=>$maindb, 'type'=>$s_new_license_name,'resellerid'=>$i_reseller_id,'resellername'=>$s_reseller_name,'cost'=>0,'value'=>$i_new_license_value,'purchased'=>date("Y-m-d H:m:i"));
			mlavu_query("INSERT INTO `[maindb]`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`cost`,`value`,`purchased`) VALUES ('[type]','[resellerid]','[resellername]','[cost]','[value]','[purchased]')",
				$a_insert_vars);
			$was_inserted_query = mlavu_query("SELECT `id`,`notes` FROM `[maindb]`.`reseller_licenses` WHERE `type`='[type]' AND `resellerid`='[resellerid]' AND `resellername`='[resellername]' AND `cost`='[cost]' AND `value`='[value]' AND `purchased`='[purchased]'",
				$a_insert_vars);
			if ($was_inserted_query === FALSE)
				return "removed license from restaurant, failed to give license back to distributor";
			if (mysqli_num_rows($was_inserted_query) == 0)
				return "removed license from restaurant, failed to give license back to distributor";
			$i_new_license_id = (int)mlavu_insert_id();

			// get all licenses to revoke
			$a_all_licenses = array(array($i_license_id, $s_license_notes));
			if (count($a_extra_upgrades_applied) > 0)
				$a_all_licenses = array_merge($a_extra_upgrades_applied, $a_all_licenses);

			// mark the old licenses as revoked
			$a_failed_ids = array();
			foreach($a_all_licenses as $a_license) {
				$i_upgrade_id = $a_license[0];
				$s_notes = $a_license[1];
				$s_new_notes_message = 'combined ([new_license_id]), license revoked by [admin_user], previously restaurant [restaurant_id], reason: [remove_reason]';
				if (trim($s_notes) == '')
					$s_notes = $s_new_notes_message;
				else
					$s_notes .= '|'.$s_new_notes_message;
				mlavu_query("UPDATE `[maindb]`.`reseller_licenses` SET `restaurantid`='-1',`notes`='$s_notes' WHERE `id`='[id]'",
					array("maindb"=>$maindb, "id"=>$i_upgrade_id, "new_license_id"=>$i_new_license_id, "admin_user"=>account_loggedin(), "restaurant_id"=>$i_restaurant_id, "remove_reason"=>$s_remove_reason));
				if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
					$a_failed_ids[] = $i_upgrade_id;
			}
			if (count($a_failed_ids) > 0)
				return "removed license from restaurant, applied license to distributor, failed to update license status for upgrades and previous license (ids ".implode(', ', $a_failed_ids).")";
		} else {
		// give the upgrades back to the user
			foreach($a_removed_upgrades as $a_removed_upgrade) {
				$id = $a_removed_upgrade['id'];
				$a_temp_license = array('notes'=>$a_removed_upgrade['notes']);
				$s_new_note = 'Upgrade removed by '.account_loggedin().' on '.date('Y-m-d H:i:s');
				RESELLER_LICENSES::apply_note($a_temp_license, $s_new_note);
				$s_query_string = "UPDATE `[maindb]`.`reseller_licenses` SET `restaurantid`='',`applied`='',notes='[notes]' WHERE `id`='[id]'";
				$a_query_vars = array("maindb"=>$maindb, 'notes'=>$a_temp_license['notes'], 'id'=>$id);
				mlavu_query($s_query_string, $a_query_vars);
			}
		}

		foreach($a_other_extra_upgrades_ids as $i_id)
			remove_distro_license_from_restaurant($i_id, $s_remove_reason);

		// return sucecss
		return "success";
	}

	// returns the restaurant as an array or returns an empty array() on failure
	function get_restaurant_from_dataname($s_dataname) {
		global $maindb;

		$restaurant_query = mlavu_query("SELECT * FROM `[maindb]`.`restaurants` WHERE `data_name`='[dataname]'",
			array("maindb"=>$maindb, "dataname"=>$s_dataname));
		if ($restaurant_query === FALSE)
			return array();
		if (mysqli_num_rows($restaurant_query) == 0)
			return array();
		$a_retval = mysqli_fetch_assoc($restaurant_query);
		mysqli_free_result($restaurant_query);
		return $a_retval;
	}

	// returns the restaurant as an array or returns an empty array() on failure
	function get_restaurant_from_id($i_restaurant_id) {
		global $maindb;

		$restaurant_query = mlavu_query("SELECT * FROM `[maindb]`.`restaurants` WHERE `id`='[id]'",
			array("maindb"=>$maindb, "id"=>$i_restaurant_id));
		if ($restaurant_query === FALSE)
			return array();
		if (mysqli_num_rows($restaurant_query) == 0)
			return array();
		$a_retval = mysqli_fetch_assoc($restaurant_query);
		mysqli_free_result($restaurant_query);
		return $a_retval;
	}

	// returns the distro as an array or returns an empty array() on failure
	function get_reseller_from_distro_code($s_distro_code) {
		global $maindb;

		$distro_query = mlavu_query("SELECT * FROM `[maindb]`.`resellers` WHERE `username`='[distro_code]'",
			array("maindb"=>$maindb, "distro_code"=>$s_distro_code));
		if ($distro_query === FALSE)
			return array();
		if (mysqli_num_rows($distro_query) == 0)
			return array();
		$a_retval = mysqli_fetch_assoc($distro_query);
		mysqli_free_result($distro_query);
		return $a_retval;
	}

	// gets the currently prescribed montly hosting amount
	// @$s_dataname: the dataname to get the monthly hosting for
	// @$b_second_loop: used for internal purposes, don't touch
	// @return: the upcoming monthly hosting amount, or FALSE if it fails for some reason
	function get_current_montly_hosting($s_dataname, $b_second_loop = FALSE) {
		global $billing_props;

		if (isset($billing_props) && is_array($billing_props) && isset($billing_props['monthly_amount']) && isset($billing_props['dataname_processed']) && $billing_props['dataname_processed'] == $s_dataname) {
			return (float)$billing_props['monthly_amount'];
		}
		if ($b_second_loop)
			return FALSE;

		ob_start();
		require_once(dirname(__FILE__).'/../new_process_payments.php');
		$a_retval = process_payments('customer_billing_info', $s_dataname);
		$billing_props = $a_retval['props'];
		$s_trash = ob_get_contents();
		ob_end_clean();

		return get_current_montly_hosting($s_dataname, TRUE);
	}

	/**
	 * Predicts the next billing date for an account
	 * @param  string  $s_dataname         The dataname of the account
	 * @param  integer $i_package          The package level of the account (eg 13)
	 * @param  string  $s_created_date     The date the account was created on
	 * @param  integer $i_restaurant_id    The if of the account's `poslavu_MAIN_db`.`restaurants` row
	 * @param  array   $a_signup           The signup row of the account
	 * @param  array   $a_billing_profiles Filled with the billing profiles of the account (might as well pass in NULL)
	 * @return integer                     The datetime of the next billing date, eg strtotime('2013-01-01 00:00:00')
	 */
	function billing_get_next_bill_date($s_dataname, $i_package, $s_created_date, $i_restaurant_id, &$a_signup, &$a_billing_profiles, $b_skip_upcoming_waived_invoice = FALSE) {
		$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
		// get the first hosting date
		$a_props = array('created'=>date('Y-m-d',strtotime($s_created_date)));
		$a_prop_parts = get_setup_props($a_props, $i_package, $a_signup['status']);
		$i_next_billdate = strtotime($a_prop_parts['first_hosting']);
		$i_skipped_hosting_date = 0;
		$i_skipped_billdate = 0;

		// get the billing profiles
		$a_billing_profiles = $dbapi->getAllInTable('billing_profiles', array('dataname'=>$s_dataname, 'type'=>'hosting', 'arb_status'=>'active', 'active'=>'1'), TRUE,
			array('whereclause'=>"WHERE `dataname`='[dataname]' AND `type` LIKE '[type]' AND `arb_status`='[arb_status]' AND `active`='[active]'"));
		if (count($a_billing_profiles) > 0) {

			$i_selected_billing_profile_key = NULL;

			// get the next billing date based off of the billing profile(s)
			$a_similar_billing_profiles = array();
			foreach($a_billing_profiles as $k=>$a_billing_profile) {

				// choose a default key
				if ($i_selected_billing_profile_key === NULL)
					$i_selected_billing_profile_key = $k;

				// choose the next key if it is newer
				$s_billdate = '';
				$s_other = NULL;
				get_next_pay_date($a_billing_profile, $s_billdate, $s_other, $s_other, $s_other);
				$i_billdate = strtotime($s_billdate);
				if ($i_skipped_billdate < $i_billdate) {
					$i_skipped_billdate = $i_billdate;
					$i_selected_billing_profile_key = $k;
				}
				$a_similar_billing_profiles[] = $k;
			}

			// find the billing profile with the most recent payment
			$a_last_four = array();
			foreach($a_similar_billing_profiles as $k)
				$a_last_four[] = $a_billing_profiles[$k]['last_four'];
			$a_last_payment_responses = $dbapi->getAllInTable('payment_responses', array('match_restaurantid'=>$i_restaurant_id), TRUE,
				array('whereclause'=>"WHERE `match_restaurantid`='[match_restaurantid]' AND RIGHT(`x_account_number`,4) ".$dbapi->arrayToInClause($a_last_four,TRUE)." AND `x_response_code`='1' AND `match_type`='billing profile match'", 'orderbyclause'=>'ORDER BY `date` DESC', 'limitclause'=>'LIMIT 1', 'selectclause'=>'RIGHT(`x_account_number`,4) AS `last_four`'));
			if (count($a_last_payment_responses) > 0) {

				// a matching payment response was found
				// match it to a billing profile in $i_selected_billing_profile_key
				foreach($a_billing_profiles as $k=>$a_billing_profile) {
					if ($a_billing_profile['last_four'] == $a_last_payment_responses[0]['last_four'])
						$i_selected_billing_profile_key = $k;
				}
			}

			// choose only the most recent billing profile
			$a_billing_profiles = $a_billing_profiles[$i_selected_billing_profile_key];
		}

		// get the invoices
		$a_invoices = $dbapi->getAllInTable("invoices", array("dataname"=>$s_dataname, "type"=>"hosting"), TRUE, array("whereclause"=>"WHERE `dataname`='[dataname]' AND `type` LIKE '[type]'", "orderbyclause"=>"ORDER BY `due_date` ASC", "limitclause"=>"LIMIT 1"));
		if (count($a_invoices) > 0) {
			$i_skipped_hosting_date = strtotime($a_invoices[0]['due_date']);
		}

		// increment the skipped billing date up until it's within the last month
		// and set the next billing date based upon the skipped billing date +1 month
		if ($i_skipped_hosting_date > strtotime("2009-01-01"))
			$i_next_billdate = strtotime(date('Y-m-d', $i_skipped_hosting_date).' +1 month');
		else if ($i_skipped_billdate > strtotime("2009-01-01"))
			$i_next_billdate = $i_skipped_billdate;
		while ($i_next_billdate <= time()) {
			$i_next_billdate = strtotime(date('Y-m-d',$i_next_billdate).' +1 month');
		}

		// look for already created waived invoices
		// id, due_date, dataname, restaurantid, amount, waived, type, responseid, distro_points_applied, distro_code_applied_to
		if ($b_skip_upcoming_waived_invoice) {
			$a_invoices = $dbapi->getAllInTable('invoices', array('dataname'=>$s_dataname, 'type'=>'Hosting', 'waived'=>'1', 'due_date'=>date('Y-m-d', $i_next_billdate)), TRUE, array('selectclause'=>'`id`'));
			while (count($a_invoices) > 0) {
				$i_next_billdate = strtotime(date('Y-m-d', $i_next_billdate).' +1 month');
				$a_invoices = $dbapi->getAllInTable('invoices', array('dataname'=>$s_dataname, 'type'=>'Hosting', 'waived'=>'1', 'due_date'=>date('Y-m-d', $i_next_billdate)), TRUE, array('selectclause'=>'`id`'));
			}
		}

		return $i_next_billdate;
	}

?>
