<?php
	global $b_is_manage;
	$b_is_manage = (isset($_GET['origin']) && $_GET['origin']=="ua_cp") || (strpos($_SERVER['REQUEST_URI'], '/manage/') !== FALSE);
?>

<script type="text/javascript">
	inventory_window_exists = false;

	function set_info_location2()
	{
		if(inventory_window_exists)
		{
			iwinpos = findPos2(document.getElementById("billing_table"));
			var topy = iwinpos[1] - 25;
			var leftx = iwinpos[0];
			var widthx = document.getElementById("billing_table").offsetWidth;
			var areawidth = document.getElementById("info_area_table").offsetWidth;

			var top = window.parent.document.body.scrollTop - 150;
			if(top < topy)
				gotoy = topy + 25;
			else
				gotoy = top + 25;

			gotox = 0 + (widthx / 2) - (areawidth / 2);

			if(document.getElementById("info_window_bg").style.visibility=="visible")
			{
				document.getElementById("info_window_bg").style.top = gotoy + "px";
				document.getElementById("info_window_bg").style.left = gotox + "px";
			}
			if(document.getElementById("info_window").style.visibility=="visible")
			{
				document.getElementById("info_window").style.top = gotoy + "px";
				document.getElementById("info_window").style.left = gotox + "px";
			}
		}
	}

	function findPos2(obj) {
		var curleft = curtop = 0;
		if (obj.offsetParent) {
			do {
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
		}
		return new Array(curleft,curtop);
	}


	function show_info2(content)
	{

		content = unescape(content);
		content = content.replace(/\+/g, " ");
		document.getElementById("info_area").innerHTML = content;
		info_visibility2("on");
		<?php echo ($b_is_manage ? "set_info_location2();" : ""); ?>
	}

	function info_visibility2(setvis)
	{
		infowin = document.getElementById("info_window");
		infowinbg = document.getElementById("info_window_bg");
		if(infowin.style.visibility=="hidden" || setvis=="on")
		{
			infowin.style.visibility = "visible";
			infowinbg.style.visibility = "visible";
		}
		else
		{
			infowin.style.visibility = "hidden";
			infowinbg.style.visibility = "hidden";
		}
	}
</script>


<?php
	//-------------------------- floating info window -----------------------------------//

	if (!function_exists("transparency2")) {

		function transparency2($opacity)
		{
			return "style='filter:alpha(opacity=".$opacity.");-moz-opacity:.".$opacity.";opacity:.".$opacity."'";
		}
	}

	$dialog_iframe_size = array(555,448);
	function create_info_layer2($dwidth=false, $dheight=false)
	{
		global $dialog_iframe_size;
		global $b_is_manage;
		if($dwidth) $dialog_iframe_size[0] = $dwidth;
		if($dheight) $dialog_iframe_size[1] = $dheight;

		$str = "
		<style>
			.close_btn, .close_btn a:link, .close_btn a:visited, .close_btn a:hover {
				font-family:Arial, Helvetica, sans-serif;
				font-size:18px; color:#aa7777;
			}
		</style>

		<div style='position:absolute; left:240px; top:100px; z-index:2240; visibility:hidden' id='info_window_bg' name='info_window_bg'>
			<table width='$dwidth' height='".($dheight + 40)."' id='info_area_bgtable' style='border:solid 2px #888888; background-color:#EEEEBB; color:#111;' ".transparency2(90).">
				<tr>
					<td align='center' valign='top'>&nbsp;</td>
				</tr>
			</table>
		</div>

		<div style='position:absolute; left:240px; top:100px; z-index:2280; visibility:hidden' id='info_window' name='info_window'>
			<table width='$dwidth' height='$dheight' id='info_area_table'>
				<tr>
					<td align='center' valign='top'>
						<table width='100%'>
							<tr>
								<td width='100%' align='right' class='close_btn'>
									<a onclick='info_visibility2()' style='cursor:pointer; color:#444444'>
										<font size='-2'>CLOSE</font>
										<b>X</b>
									</a>
								</td>
							</tr>
						</table>
						<div id='info_area' name='info_area'>&nbsp;</div>
					</td>
				</tr>
			</table>
		</div>
		<script type='text/javascript'>
			inventory_window_exists = true;
			".($b_is_manage ? "window.parent.window.onscroll = set_info_location2;" : "")."
		</script>";

		$str = str_replace(array("\n\r", "\r\n", "\r"), "\n", $str);

		return $str;
	}
?>
