 <?php
	function dtadd($dt,$amount=1,$type="days",$amount2=0,$type2="")
	{
		$type = strtolower($type);
		$type2 = strtolower($type2);
		$plus_d = 0;
		$plus_m = 0;
		$plus_y = 0;
		if ($amount2>0 && $type2!="") {
			if ($type2 == "days") $plus_d = $amount2;
			if ($type2 == "month") $plus_m = $amount2;
			if ($type2 == "year") $plus_y = $amount2;
		}
		$dt_parts = explode("-",$dt);
		if(count($dt_parts) > 2)
		{
			if(substr($type,0,4)=="year")
				$dt_ts = mktime(0,0,0,($dt_parts[1] + $plus_m),($dt_parts[2] + $plus_d),($dt_parts[0] + $amount + $plus_y));
			else if(substr($type,0,5)=="month")
				$dt_ts = mktime(0,0,0,($dt_parts[1] + $amount + $plus_m),($dt_parts[2] + $plus_d),($dt_parts[0] + $plus_y));
			else
				$dt_ts = mktime(0,0,0,($dt_parts[1] + $plus_m),($dt_parts[2] + $amount + $plus_d),($dt_parts[0] + $plus_d));

			return date("Y-m-d",$dt_ts);
		}
		return $dt;
	}

	// function find_package_status() moved to payment_profile_functions.php

	/*--------------------------------------------------------------------------------------------------------------*/
	// BILLS AND PAYMENTS

	function process_payments($p_area, $dataname, $package="", $signup_status="")
	{
		$lavu_admin = false;
		if ($p_area == "billing_info") $lavu_admin = true;

		$output = "";

		$collection_mode = "";
		$custom_monthly_hosting = "";
		$auto_waive_until = "";
		$custom_max_ipads = "";
		$tablesideMaxIpads = "";
		$custom_max_ipods = "";
		$force_allow_tabs_n_tables = "0";

		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			$rest_id = $rest_read['id'];
			$rest_created = substr($rest_read['created'],0,10);

			if($package=="")
			{
				$fps_info = find_package_status($rest_id);
				$package = $fps_info['package'];
				$signup_status = $fps_info['signup_status'];
				//echo $fps_info['package'] . " - " . $fps_info['signup_status'] . "<br>";
			}

			$license_applied = 0;
			$license_value = 0;
			$license_date_paid = "";
			$status_record_exists = false;
			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
			if(mysqli_num_rows($status_query))
			{
				$status_read = mysqli_fetch_assoc($status_query);
				$collection_mode = $status_read['collection_mode'];
				$custom_monthly_hosting = $status_read['custom_monthly_hosting'];
				$auto_waive_until = $status_read['auto_waive_until'];
				$custom_max_ipads = $status_read['custom_max_ipads'];
				$tablesideMaxIpads = $status_read['tableside_max_ipads'];
				$custom_max_ipods = $status_read['custom_max_ipods'];
				$custom_max_kds = $status_read['custom_max_kds'];
				$force_allow_tabs_n_tables = $status_read['force_allow_tabs_n_tables'];
				$license_applied = $status_read['license_applied'];
				$status_record_exists = true;
				//$output .= "LICFOUND!<br>";

				if($license_applied!="" && is_numeric($license_applied))
				{
					$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `id`='[1]'",$license_applied);
					if(mysqli_num_rows($lic_query))
					{
						$lic_read = mysqli_fetch_assoc($lic_query);
						if($lic_read['type']=="Silver")
							$package = 1;
						else if($lic_read['type']=="Gold")
							$package = 2;
						else if($lic_read['type']=="Platinum")
							$package = 3;
						$license_value = $lic_read['value'];
						$license_date_paid = $lic_read['purchased'];
					}
				}
			}
		}
		else
		{
			return;
		}

		if($package==1)
		{
			$lic_amount = 895;
			$mon_amount = 29.95;
		}
		else if($package==2)
		{
			$lic_amount = 1495;
			$mon_amount = 49.95;
		}
		else if($package==3)
		{
			$lic_amount = 3495;
			$mon_amount = 99.95;
		}

		if (isset($_REQUEST['set_custom_monthly_hosting']) && is_numeric($_REQUEST['set_custom_monthly_hosting'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_monthly_hosting` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_custom_monthly_hosting'], $rest_read['id']);
			if ($update) $custom_monthly_hosting = $_REQUEST['set_custom_monthly_hosting'];
		}

		if (isset($_REQUEST['set_auto_waive_until'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `auto_waive_until` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_auto_waive_until'], $rest_read['id']);
			if ($update) $auto_waive_until = $_REQUEST['set_auto_waive_until'];
		}

		if (isset($_REQUEST['set_custom_max_ipads']) && is_numeric($_REQUEST['set_custom_max_ipads'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_max_ipads` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_custom_max_ipads'], $rest_read['id']);
			if ($update) $custom_max_ipads = $_REQUEST['set_custom_max_ipads'];
		}

		if (isset($_REQUEST['set_tableside_max_ipads']) && is_numeric($_REQUEST['set_tableside_max_ipads'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_max_ipads` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_tableside_max_ipads'], $rest_read['id']);
			if ($update){
				$tablesideMaxIpads = $_REQUEST['set_tableside_max_ipads'];
			} 
		}

		if (isset($_REQUEST['set_custom_max_ipods']) && is_numeric($_REQUEST['set_custom_max_ipods'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_max_ipods` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_custom_max_ipods'], $rest_read['id']);
			if ($update) $custom_max_ipods = $_REQUEST['set_custom_max_ipods'];
		}
		if (isset($_REQUEST['set_custom_max_kds']) && is_numeric($_REQUEST['set_custom_max_kds'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_max_kds` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_custom_max_ipods'], $rest_read['id']);
			if ($update) $custom_max_kds = $_REQUEST['set_custom_max_kds'];
		}
		if (isset($_REQUEST['set_custom_max_kiosk']) && is_numeric($_REQUEST['set_custom_max_kiosk'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_max_kiosk` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_custom_max_ipods'], $rest_read['id']);
			if ($update) $custom_max_kds = $_REQUEST['set_custom_max_kiosk'];
		}
		if (isset($_REQUEST['set_force_allow_tabs_n_tables'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `force_allow_tabs_n_tables` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_force_allow_tabs_n_tables'], $rest_read['id']);
			if ($update) $force_allow_tabs_n_tables = $_REQUEST['set_force_allow_tabs_n_tables'];
		}
		if (isset($_REQUEST['set_collection_mode'])) {
			$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `collection_mode` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_collection_mode'], $rest_read['id']);
			if ($update) $collection_mode = $_REQUEST['set_collection_mode'];
		}

		if ($custom_monthly_hosting!="" && is_numeric($custom_monthly_hosting)) $mon_amount = $custom_monthly_hosting;

		$props = array();
		$props['license_amount'] = $lic_amount;
		$props['monthly_amount'] = $mon_amount;
		$props['created'] = $rest_created;

		if($signup_status=="new")
		{
			$props['license_billed'] = dtadd($props['created'],14,"days");
			$props['first_hosting'] = dtadd($props['created'],44,"days");
		}
		else
		{
			$props['license_billed'] = dtadd($props['created'],7,"days");
			$props['first_hosting'] = dtadd($props['created'],37,"days");
		}

		$payment_type_list = array("Hosting","License","General","Other");
		if(isset($_GET['set_payment_type']) && isset($_GET['payment_id']))
		{
			$set_payment_type = $_GET['set_payment_type'];
			$payment_id = $_GET['payment_id'];

			mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `payment_type`='[1]' where `id`='[2]' limit 1",$set_payment_type,$payment_id);
		}

		if(isset($_GET['set_custom_payment_type']))
		{
			$scp_type = $_GET['set_custom_payment_type'];
			$scp_date = $_GET['set_custom_payment_date'];
			$scp_amount = $_GET['set_custom_payment_amount'];
			$scp_datetime = $scp_date . " 00:00:00";
			$scp_response_code = "1";

			$vars = array();
			$vars['datetime'] = $scp_datetime;
			$vars['amount'] = $scp_amount;
			$vars['x_type'] = $scp_type;
			$vars['match_restaurantid'] = $rest_id;
			$vars['payment_type'] = "";
			$vars['x_response_code'] = $scp_response_code;

			$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `datetime`='[datetime]' and `x_amount`='[amount]' and `x_type`='[x_type]' and `match_restaurantid`='[match_restaurantid]'",$vars);
			if(mysqli_num_rows($exist_query))
			{
				$exist_read = mysqli_fetch_assoc($exist_query);
				//mlavu_query("delete from `poslavu_MAIN_db`.`payment_responses` where id='[2]' limit 1",$scp_response_code,$exist_read['id']);
				//mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `x_response_code`='[1]' where id='[2]' limit 1",$scp_response_code,$exist_read['id']);
				//$output .= "found<br>";
			}
			else
			{
				//$output .= "couldn't find<br>";
				mlavu_query("insert into `poslavu_MAIN_db`.`payment_responses` (`payment_type`,`datetime`,`x_amount`,`x_type`,`match_restaurantid`,`x_response_code`) values ('[payment_type]','[datetime]','[amount]','[x_type]','[match_restaurantid]','[x_response_code]')",$vars);
			}
		}
		else if(isset($_GET['set_custom_invoice_type']))
		{
			$sci_type = $_GET['set_custom_invoice_type'];
			$sci_date = $_GET['set_custom_invoice_date'];
			$sci_amount = $_GET['set_custom_invoice_amount'];

			$vars = array();
			$vars['due_date'] = $sci_date;
			$vars['amount'] = $sci_amount;
			$vars['type'] = $sci_type;
			$vars['dataname'] = $dataname;
			$vars['waived'] = "";

			$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[dataname]' and `type`='[type]' and `due_date`='[due_date]'",$vars);
			if(mysqli_num_rows($exist_query))
			{
				$exist_read = mysqli_fetch_assoc($exist_query);
				$output .= "<b>Invoice already exists for $sci_type on $sci_date</b><br><br>";
			}
			else
			{
				$output .= "<b>Creating new invoice for $sci_type on $sci_date</b>";
				$success = mlavu_query("insert into `poslavu_MAIN_db`.`invoices` (`dataname`,`amount`,`type`,`waived`,`due_date`) values ('[dataname]','[amount]','[type]','[waived]','[due_date]')",$vars);
				if($success) $output .= " (success)";
				else $output .= " (failed)";
				$output .= "<br><br>";
			}
		}

		$payments = array();
		if($license_value > 0)
		{
			$payments[] = array($license_date_paid,$license_value,"License",true,0,"license",array());
		}
		$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit' or `x_type`='Misc' or `x_type`='Paypal' or `x_type`='Check' or `x_type`='Adjust') order by `datetime` asc, `id` asc",$rest_id);
		while($response_read = mysqli_fetch_assoc($response_query))
		{
			if($response_read['x_type']=="credit")
				$amount_paid = $response_read['x_amount'] * 1;
			else
				$amount_paid = $response_read['x_amount'] * 1;
			if($response_read['x_response_code']=="1")
				$approved = true;
			else
				$approved = false;

			$pay_type = $response_read['payment_type'];
			if($pay_type=="")
			{
				if(abs($amount_paid) > 500) $pay_type = "License";
				else $pay_type = "Hosting";

				mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `payment_type`='[1]' where `id`='[2]'",$pay_type,$response_read['id']);
			}

			$date_paid = substr($response_read['datetime'],0,10);

			$transaction_voided = false;
			if($response_read['x_trans_id']!="")
			{
				$void_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_trans_id`='[1]' and `x_type`='void'",$response_read['x_trans_id']);
				if(mysqli_num_rows($void_query))
				{
					$transaction_voided = true;
				}
			}

			if($transaction_voided)
			{
			}
			else
			{
				$payments[] = array($date_paid,$amount_paid,$pay_type,$approved,$response_read['id'],$response_read['x_type'],$response_read);
			}
		}

		/*$payments[] = array("2011-08-22","1495.95","License",true);
		$dt = $props['first_hosting'];
		while($dt < date("Y-m-d") && $rcount < 10000)
		{
			$payments[] = array($dt,"49.95","Hosting",rand() % 3);

			$dt = dtadd($dt,1,"month");
			$rcount++;
		}*/

		if(isset($_GET['invoice_id']))
		{
			$invoice_id_list = explode(",",$_GET['invoice_id']);

			for($n=0; $n<count($invoice_id_list); $n++)
			{
				$invoice_id = $invoice_id_list[$n];
				if(isset($_GET['set_invoice_waived']))
				{
					$set_invoice_waived = $_GET['set_invoice_waived'];
					if($set_invoice_waived=="remove")
						mlavu_query("delete from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `id`='[2]' limit 1",$dataname,$invoice_id);
					else
						mlavu_query("update `poslavu_MAIN_db`.`invoices` set `waived`='[1]' where `id`='[2]' limit 1",$set_invoice_waived,$invoice_id);
				}
				else if(isset($_GET['set_invoice_amount']))
				{
					$set_invoice_amount = str_replace(",","",$_GET['set_invoice_amount']);
					mlavu_query("update `poslavu_MAIN_db`.`invoices` set `amount`='[1]' where `id`='[2]' limit 1",$set_invoice_amount,$invoice_id);
				}
			}
		}

		$billable = array();
		$billable[] = array($props['license_billed'],$lic_amount,"License");
		//foreach($props as $key => $val) $output .= $key . " = " . $val . "<br>";
		mlavu_query("delete from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `amount`=''",$dataname);
		$dt = $props['first_hosting'];
		$last_hosting_bill_date = $props['first_hosting'];
		$rcount = 1;
		while($dt <= date("Y-m-d") && $rcount < 10000)
		{
			$invoice_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `due_date`='[2]'",$dataname,$dt);
			if(mysqli_num_rows($invoice_query))
			{
				$invoice_read = mysqli_fetch_assoc($invoice_query);
				$invoice_waived = $invoice_read['waived'];
				$invoice_id = $invoice_read['id'];

				$bill_amount = $invoice_read['amount'];

				//foreach($invoice_read as $key => $val) $output .= $key . " = " . $val . "<br>";
			}
			else
			{
				$invoice_id = 0;
				$bill_amount = $props['monthly_amount'];
			}
			$billable[] = array($dt,$bill_amount,"Hosting",false);

			$lastdt = $dt;
			$last_hosting_bill_date = $dt;
			$dt = dtadd($dt,1,"month");
			$rcount++;

			//------------------------- Look for non standard invoices ----------------------//
			$cust_invoice_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `id`!='[1]' and `dataname`='[2]' and `due_date`>='[3]' and `due_date`<'[4]'",$invoice_id,$dataname,$lastdt,$dt);
			if(mysqli_num_rows($cust_invoice_query))
			{
				$cust_invoice_read = mysqli_fetch_assoc($cust_invoice_query);
				$cust_invoice_waived = $cust_invoice_read['waived'];
				$cust_invoice_id = $cust_invoice_read['id'];
				$cust_bill_amount = $cust_invoice_read['amount'];
				$cust_invoice_date = $cust_invoice_read['due_date'];
				$cust_bill_type = $cust_invoice_read['type'];

				//$output .= "<br>FOUND CUSTOM INVOICE FOR $cust_bill_amount<br>";
				$billable[] = array($cust_invoice_date,$cust_bill_amount,$cust_bill_type,false);
			}
			//-------------------------------------------------------------------------------//
		}
		//$billable[4][3] = true; // waived
		//$billable[0][3] = true;
		$output .= '<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"> </script>';
		$output .= "<script language='javascript'>";

		$output .= "  function edit_invoice_amount(invoice_index) { ";

		$output .= "   $('#invoice_amount_' + invoice_index).hide(); ";
		$output .= "   $('#edit_invoice_amount_' + invoice_index).show(); ";

		$output .= "  } ";
		$output .= "  function enter_pressed(mfield,e) { ";
		$output .= "    var keycode; ";
		$output .= "    if (window.event) keycode = window.event.keyCode; ";
		$output .= "    else if (e) keycode = e.which; ";
		$output .= "    else keycode = 0; ";
		$output .= "    if (keycode == 13) ";
		$output .= "       return true; ";
		$output .= "    else return false; ";
		$output .= "  } ";
		$output .= "</script>";

		$bid_prefix = "";
		$pid_prefix = "";
		if (!$lavu_admin) {
			$bid_prefix = "77-";
			$pid_prefix = "88-";
		}

		$total_credits = array();
		$credits = array();
		$credits_left_by_index = array();
		$track_display_of_credits = array();

		$paid_last_two_weeks = 0;
		$last_payment_made = "";
		$paid_last_four_weeks = 0;
		$two_weeks_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 15,date("Y")));
		$four_weeks_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 31,date("Y")));

		$paystr = "";
		$paystr .= "<u>Payment List:</u><br>";
		$paystr .= "<table>";
		$payview_js = "";
		$payment_amounts = array();
		$payments_applied = array();
		$payments_code_array = array();
		for($i=0; $i<count($payments); $i++)
		{
			$this_paystr = "";
			$payments_applied[$i] = 0;
			$payment_date = $payments[$i][0];
			$payment_amount = $payments[$i][1];
			$payment_amounts[] = $payment_amount;
			$ptval_selected = $payments[$i][2];
			$payment_approved = $payments[$i][3];
			$payment_id = $payments[$i][4];
			$payment_xtype = $payments[$i][5];

			$amount_color = "#999999";
			if ($payment_xtype=="credit") {
				if ($payment_approved=="1") {
					$amount_color = "#556699";
					if($payment_date >= $two_weeks_ago) $paid_last_two_weeks -= $payment_amount * 1;
					if($payment_date >= $four_weeks_ago) $paid_last_four_weeks -= $payment_amount * 1;
				}
				$rowstyle = "style='color:#556699;'";
				$amount_style = "style='color:".$amount_color."; padding:1px 95px 1px 1px;'";
				$label = "Credit";
			} else {
				if ($payment_approved=="1") {
					$amount_color = "#004400";
					if($payment_date >= $two_weeks_ago) $paid_last_two_weeks += $payment_amount * 1;
					if($payment_date >= $four_weeks_ago) $paid_last_four_weeks += $payment_amount * 1;
					if($last_payment_made=="" || $payment_date > $last_payment_made) $last_payment_made = $payment_date;
				}
				$rowstyle = "style='color:#004400'";
				$amount_style = "style='color:".$amount_color."'";
				$label = "Payment";
			}

			if($p_area=="billing_info")
			{
				if($payment_approved) $show_payment_approved = "<font style='color:#008800'>(Approved)</font>";
				else $show_payment_approved = "<font style='color:#880000'>(Declined)</font>";

				$pay_read = $payments[$i][6];

				$payinfo = "";
				$payinfo .= "<table>";
				$payinfo .= "<tr><td align='right'>Date Processed:</td><td>$payment_date</td></tr>";
				$payinfo .= "<tr><td align='right'>Amount:</td><td>$payment_amount $show_payment_approved</td></tr>";
				$payinfo .= "<tr><td align='right'>Type:</td><td>$payment_xtype</td></tr>";
				if(isset($pay_read['response']))
				{
					$payinfo .= "<tr><td align='right'>Card Type:</td><td>".$pay_read['x_card_type']."</td></tr>";
					$payinfo .= "<tr><td align='right'>Last Four:</td><td>".$pay_read['x_account_number']."</td></tr>";
					$payinfo .= "<tr><td align='right'>SubscriptionId:</td><td>".$pay_read['x_subscriptionid']."</td></tr>";
					$payinfo .= "<tr><td align='right'>Match Type:</td><td>".$pay_read['match_type']."</td></tr>";
					$payinfo .= "<tr><td align='right'>Match Notes:</td><td>".$pay_read['match_notes']."</td></tr>";
					$payinfo .= "<tr><td align='right' valign='top'>More Info:</td><td><input type='button' value='View' onclick='show_payment_info_".$i."()' /></td></tr>";

					$payview_js .= "function show_payment_info_".$i."() { ";
					$payview_js .= "  alert('".str_replace("\n","\\n",str_replace("'","&apos;",$pay_read['response']))."')";
					$payview_js .= "} ";
				}
				$payinfo .= "</table>";

				$onclick_str = " onclick='show_info2(\"".str_replace("'","\\\"",str_replace("\"","\\\"",$payinfo))."\")'";
			}
			else
				$onclick_str = "";

			$this_paystr .= "<tr style='font-family:Arial, Helvetica, sans-serif; font-size:13px;'>";
			$this_paystr .= "<td $rowstyle><nobr>$label " . ($i + 1) . "&nbsp;</nobr></td>";
			$this_paystr .= "<td $rowstyle><nobr><font size='-1' color='#555555'>ID: ".$pid_prefix.$payment_id."</font>&nbsp;</nobr></td>";
			$this_paystr .= "<td $rowstyle><nobr>$payment_date</nobr></td>";
			$this_paystr .= "<td $amount_style align='right'".$onclick_str."><nobr><b>$" . number_format($payment_amount, 2) . "</b><payment_row_credit_$i></nobr></td>";
			//$this_paystr .= "<td $rowstyle><nobr><payment_row_credit_$i></nobr></td>";

			if($payment_xtype=="credit" && $payment_approved=="1")
			{
				if(!isset($total_credits[$ptval_selected]))
					$total_credits[$ptval_selected] = 0;
				$total_credits[$ptval_selected] += $payment_amount * 1;
				$credits[] = array($i, $payment_amount);
				$credits_left_by_index[$i] = $payment_amount;
				$track_display_of_credits[$i] = array(0, 0, -1); // display applied, last bill id, last display index
			}

			$this_paystr .= "<td $rowstyle>";
			//$this_paystr .= "<select onchange='window.location = \"index.php?dn=$dataname&package=$package&signup_status=$signup_status&payment_id=$payment_id&set_payment_type=\" + this.value'>";
			if ($lavu_admin && $payment_id > 0) {
//$this_paystr .= "<select onchange='return make_billing_change(this,\"$ptval_selected\",\"Change Payment Type\",\"index.php?dn=$dataname&package=$package&signup_status=$signup_status&payment_id=$payment_id&set_payment_type=\" + this.value,\"\",\"\",\"".$payment_id."\")'>";
$this_paystr .= "<select onchange='return make_billing_change(this,\"$ptval_selected\",\"Change Payment Type\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&payment_id=$payment_id&set_payment_type=\" + this.value,\"\",\"\",\"".$payment_id."\")'>";

				for ($n = 0; $n < count($payment_type_list); $n++) {
					$ptval = $payment_type_list[$n];
					$this_paystr .= "<option value='$ptval'";
					if($ptval==$ptval_selected) $this_paystr .= " selected";
					$this_paystr .= ">".$ptval."</option>";
				}
				$this_paystr .= "</select>";
			} else $this_paystr .= "($ptval_selected)&nbsp;";
			$this_paystr .= "</td>";

			if ($lavu_admin) $this_paystr .= "<td $rowstyle> $payment_xtype</td>";
			$this_paystr .= "<td $rowstyle>";
			if($payment_approved=="1") $this_paystr .= "<font color='#008800'> approved</font>";
			else $this_paystr .= "<font color='#cc0000'> declined</font>";
			$this_paystr .= "</td>";
			$this_paystr .= "</tr>";

			$paystr .= $this_paystr;
			$payments_code_array[] = $this_paystr;
		}
		$paystr .= "</table>";

		if($payview_js!="")
		{
			$output .= "<script language='javascript'>".$payview_js."</script>";
		}

		if ($lavu_admin) {
			//--------------------------------------- Create a custom payment --------------------------------//
			$custom_payment_date_options = "";
			for($i=0; $i<count($billable); $i++)
			{
				$dt = $billable[$i][0];
				$custom_payment_date_options = "<option value='$dt'>$dt</option>" . $custom_payment_date_options;
			}

			$custom_payment_date_options = $custom_payment_date_options . "<option value='' disabled>--------</option>";
			for($n=7; $n<90; $n+=7)
			{
				$ndt = date("Y-m-d",mktime(0,0,0,date("m"),date("d") + $n,date("Y")));
				$custom_payment_date_options = $custom_payment_date_options . "<option value='$ndt'>$ndt</option>";
			}

			$dt = date("Y-m-d");
			$custom_payment_date_option = "<option value='$dt'>$dt</option>" . $custom_payment_date_options;

			$custom_payment_string .= "<span class='info_label'>Add Custom Payment:</span> <select name='custom_payment_type' id='custom_payment_type'><option value='Misc'>Misc</option><option value='Paypal'>Paypal</option><option value='Check'>Check</option><option value='Adjust'>Adjust</option></select>";
			$custom_payment_string .= "<select name='custom_payment_date' id='custom_payment_date'>$custom_payment_date_options</select>";
			//$custom_payment_string .= " <input type='text' name='custom_payment_amount' id='custom_payment_amount' onKeyPress='if(enter_pressed(this,event)) window.location = \"index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_payment_type=\" + document.getElementById(\"custom_payment_type\").value + \"&set_custom_payment_date=\" + document.getElementById(\"custom_payment_date\").value + \"&set_custom_payment_amount=\" + document.getElementById(\"custom_payment_amount\").value'><br>";
			//$custom_payment_string .= " <input type='text' name='custom_payment_amount' id='custom_payment_amount' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"\",\"Add Custom Payment\",\"index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_payment_type=\" + document.getElementById(\"custom_payment_type\").value + \"&set_custom_payment_date=\" + document.getElementById(\"custom_payment_date\").value + \"&set_custom_payment_amount=\" + document.getElementById(\"custom_payment_amount\").value,\"\",\"\",\"\")'><br>";
			$custom_payment_string .= " <input type='text' name='custom_payment_amount' id='custom_payment_amount' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"\",\"Add Custom Payment\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_payment_type=\" + document.getElementById(\"custom_payment_type\").value + \"&set_custom_payment_date=\" + document.getElementById(\"custom_payment_date\").value + \"&set_custom_payment_amount=\" + document.getElementById(\"custom_payment_amount\").value,\"\",\"\",\"\")'><br>";
		}

		//echo "<br>CLBI: ".print_r($credits_left_by_index, true);

		//$tied_credits = array();
		$payments_with_credits = array();
		$tied_credit_info = array(); // credit amount, credit (index), applied to payment (index), amount applied

		$credits_left_arr = $total_credits;
		for($i=0; $i<count($payments); $i++)
		{
			$pamount = $payments[$i][1];
			$ptype = $payments[$i][2];
			$pxtype = $payments[$i][5];
			$papproved = $payments[$i][3];

			if(isset($credits_left_arr[$ptype]))
				$credits_left = $credits_left_arr[$ptype];
			else
				$credits_left = 0;
			if($pxtype=="auth_capture" && $papproved=="1")
			{
				$rpstr = "";
				$original_amount = $pamount;
				$applied_amount = 0;
				if($pamount <= $credits_left)
				{
					$credits_left -= $pamount;
					$pamount = 0;
					$payments[$i][1] = $pamount;
					$rpstr = "&nbsp;<font color='#556699'>-$".number_format($original_amount,2)." = <b>$0.00</b></font>";
					$applied_amount = $original_amount;
					//$rpstr = "Payment " . ($i + 1) . " ($".number_format($original_amount,2).") canceled out by credits";
				}
				else if(number_format($credits_left,2) > 0)
				{
					$pamount -= $credits_left;
					$subtracted = $credits_left;
					$credits_left = 0;
					$payments[$i][1] = $pamount;
					$rpstr = "&nbsp;<font color='#556699'>-$".number_format($subtracted,2)." = <b>$".number_format($pamount,2)."</b></font>";
					$applied_amount = $subtracted;
					//$rpstr = "Payment " . ($i + 1) . " reduced from $".number_format($original_amount,2)." to $".number_format($pamount,2)." by credits<br>";
				}

				if ($applied_amount > 0) {
					for ($cr = 0; $cr < count($credits); $cr++) {
						$this_credit = $credits[$cr];
						$cr_i = $this_credit[0];
						if (number_format($credits_left_by_index[$cr_i], 2) > 0) {
							$this_applied = $applied_amount;
							if ($credits_left_by_index[$cr_i] < $applied_amount) $this_applied = $credit_left;
							$applied_amount -= $this_applied;
							$credits_left_by_index[$cr_i] -= $this_applied;
							if (!in_array($i, $payments_with_credits)) $payments_with_credits[] = $i;
							$this_credit[2] = $i;
							$this_credit[3] = $this_applied;
							//$tied_credits[] = $cr_i;
							$tied_credit_info[] = $this_credit;
							$payments_code_array[$cr_i] = str_replace("color:#556699;", "color:#797e8d; font-size:12px;", $payments_code_array[$cr_i]);
							if (number_format($applied_amount, 2) < 0.01) break;
						}
					}
				}
				if($rpstr!="")
				{
					$paystr = str_replace("<payment_row_credit_$i>",$rpstr,$paystr);
					$payments_code_array[$i] = str_replace("<payment_row_credit_$i>",$rpstr,$payments_code_array[$i]);
				}
			}
			$credits_left_arr[$ptype] = $credits_left;
		}


		//echo "<br>tied credits: ".print_r($tied_credits, true);
		//echo "<br>payments with credits: ".print_r($payments_with_credits, true);
		//echo "<br>tied credit info: ".print_r($tied_credit_info, true);

		//$output .= $paystr;

		$a_bill_is_shown = false;
		$bills_due_list = array();
		$bill_types_due = array();
		$all_bills_due = 0;
		$earliest_past_due = "";
		$tied_declines = array();
		$tied_credited = array();
		$tied_payments = array();

		//$grand_total_due = 0;
		//$grand_total_applied = 0;

		$over_payment = false;

		$checked_list_js = "";
		$lic_bill_count = 0;
		$output .='<link rel="stylesheet" type="text/css" href="/sa_cp/billing/billingStyle.css" />';

		$output .= "<br><span class='section_header' style='font:bold 32px verdana; color: #cfcfc4;'><u>Bills and Payments</u></span><br>";
		$output .= "<button onclick='$(\"[id^=ac-]\").click()'>Expand All </div>";
		$output .= "<tr><td>The dataname is: $dataname</td></tr>";
		$output .= "</table>";


		$output.='<section class="ac-container" width:"700px">';

		$counter=0;
		for($i=0; $i<count($billable); $i++)
		{
			$payments_tied_to_this_bill = array();
			$amounts_applied = array();

			$bill_date = $billable[$i][0];
			$bill_amount = $billable[$i][1];
			$bill_type = $billable[$i][2];
			$bill_waived = $billable[$i][3];

			$output .= '<div><input id="ac-'.$counter.'" name="accordion-1" type="checkbox" />';
			$approved= false;
			for($n=0; $n<count($payments); $n++){
				if($payments[$n][3]=='approved')
					$approved=true;
			}


			$due_str = "";
			$show_actual_bill_amount = true;

			if($bill_date <= date("Y-m-d"))
			{
				//echo "select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `due_date`='[2]' and `type`='[3]' ,$dataname,$bill_date,$bill_type";

				$invoice_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `due_date`='[2]' and `type`='[3]'",$dataname,$bill_date,$bill_type);
				if(mysqli_num_rows($invoice_query))
				{
					$invoice_read = mysqli_fetch_assoc($invoice_query);
					$invoice_waived = $invoice_read['waived'];
					$invoice_id = $invoice_read['id'];

					$bill_amount = str_replace(",","",$invoice_read['amount']);
				}
				else
				{
					if ($bill_date <= $auto_waive_until) $bill_waived = "1";
					mlavu_query("insert into `poslavu_MAIN_db`.`invoices` (`dataname`,`amount`,`type`,`waived`,`due_date`) values ('[1]','[2]','[3]','[4]','[5]')",$dataname,$bill_amount,$bill_type,$bill_waived,$bill_date);
					$invoice_id = mlavu_insert_id();
					$invoice_waived = "";
					//$invoice_id = mlavu_insert_id();
					//$invoice_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `id`='[1]'",$invoice_id);
					//$invoice_read = mysqli_fetch_assoc($invoice_query);
				}
				$bill_due = $bill_amount;
				if($invoice_waived=="1")
				{
					$bill_waived = true;
					$bill_due = 0;
				}

				if( $bill_waived)
					$output.='<label for="ac-'.$counter.'"><table width="100%"><td>'.$bill_date.'</td> <td style="text-align:right"> $'.$bill_amount.' √ </td></table></label>';
				else
					$output.='<label for="ac-'.$counter.'"><table width="100%"><td>'.$bill_date.'</td><td <td id="bill'.$i.'" style="text-align:right"> $'.$bill_amount.' X </td></table></label>';

					$output.='<article class="ac-small">';
					$output .= "<center><table id='billingTable-".$counter."' ><tr><td valign='top' style='padding:1px 20px 1px 10px;'>";

			$counter++;
				//$grand_total_due += $bill_due;

				$a_bill_is_shown = true;

				$output .= "<table cellspacing=0 cellpadding=0>";

				$output .= "<tr>";
				//$output .= "<tr style='font:11px verdana; color: #a5a09c;'><td>Bill Date</td><td> Bill Amount</td><td>PAID</td></tr>";
				if ($lavu_admin) {
					$output .= "<td><input type='checkbox' name='invoice_checkbox_".$invoice_id."' id='invoice_checkbox_".$invoice_id."' style= 'display:block'></td>";
					$checked_list_js .= "  if(document.getElementById('invoice_checkbox_".$invoice_id."').checked) {if(cstr!='') cstr += ','; cstr += '$invoice_id';} ";
				}
				$output .= "<td colspan='4'>&nbsp;<span class='info_value'><font color='#000066'>" . $bill_date . "</font></span> &nbsp;&nbsp;&nbsp; <span class='info_value3'>Bill ID: ".$bid_prefix.$invoice_id."</span></td></tr>";
				$output .= "<tr>";
				if ($lavu_admin) $output .= "<td></td>";
				$output .= "<td>";
				if ($lavu_admin) {
					$output .= "<div id='invoice_amount_$i' style='display:block'><a class='info_value' style='cursor:pointer' onclick='edit_invoice_amount($i)'>$" . number_format((float)$bill_amount, 2) . "</a></div>";
					//$output .= "<div id='edit_invoice_amount_$i' style='display:none'><input id='set_invoice_amount_$i' type='text' value='" . number_format((float)$bill_amount, 2) . "' size='8' onKeyPress='if(enter_pressed(this,event)) window.location = \"index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=$invoice_id&set_invoice_amount=\" + document.getElementById(\"set_invoice_amount_$i\").value' /></div>";
					//$output .= "<div id='edit_invoice_amount_$i' style='display:none'><input id='set_invoice_amount_$i' type='text' value='" . number_format((float)$bill_amount, 2) . "' size='8' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".number_format((float)$bill_amount, 2)."\",\"Change Invoice Amount from $".number_format((float)$bill_amount, 2)." to $\" + parseFloat(document.getElementById(\"set_invoice_amount_$i\").value.replace(/,/ig,\"\")).toFixed(2),\"index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=$invoice_id&set_invoice_amount=\" + document.getElementById(\"set_invoice_amount_$i\").value,\"\",\"".$invoice_id."\",\"\");' /></div>";
					$output .= "<div id='edit_invoice_amount_$i' style='display:none'><input id='set_invoice_amount_$i' type='text' value='" . number_format((float)$bill_amount, 2) . "' size='8' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".number_format((float)$bill_amount, 2)."\",\"Change Invoice Amount from $".number_format((float)$bill_amount, 2)." to $\" + parseFloat(document.getElementById(\"set_invoice_amount_$i\").value.replace(/,/ig,\"\")).toFixed(2),\"../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=$invoice_id&set_invoice_amount=\" + document.getElementById(\"set_invoice_amount_$i\").value,\"\",\"".$invoice_id."\",\"\");' /></div>";
					///../../sa_cp/billing/index.php
				} else {
					if (strtolower($bill_type)=="license" && (float)$bill_amount!=895 && (float)$bill_amount!=1495 && (float)$bill_amount!=3495) {
						$show_actual_bill_amount = false;
						$output .= "<span class='info_value'>".packageToStr(determinePackage($bill_amount, "license"))."</span>";
					} else $output .= "<span class='info_value'>$".number_format((float)$bill_amount, 2)."</span>";
				}
				$output .= "</td>";
				if ($show_actual_bill_amount) $output .= "<td>&nbsp;<span class='info_value2'>for</span>&nbsp;</td>";
				else $output .= "<td></td>";
				$output .= "<td><span class='info_value'>" . $bill_type . "</span></td>";

				$current_value = ($invoice_waived=="1")?"Waived":"Active";

				$output .= "<td valign='bottom'>&nbsp;";
				//$output .= "<select onchange='window.location = \"index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=$invoice_id&set_invoice_waived=\" + this.value'>";
				if ($lavu_admin) {
					//$output .= "<select onchange='return make_billing_change(this,\"$invoice_waived\",\"Change Invoice Waived Status\",\"index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=$invoice_id&set_invoice_waived=\" + this.value,\"\",\"".$invoice_id."\",\"\")'>";
					$output .= "<select onchange='return make_billing_change(this,\"$invoice_waived\",\"Change Invoice Waived Status\",\"../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=$invoice_id&set_invoice_waived=\" + this.value,\"\",\"".$invoice_id."\",\"\")'>";

					$output .= "<option value=''>Active</option>";
					$output .= "<option value='1'".(($invoice_waived=="1")?" selected":"").">Waived</option>";

					if($bill_type=="License")
						$lic_bill_count++;
					if($bill_type=="Other" || ($bill_type=="License" && $lic_bill_count > 1))
						$output .= "<option value='remove'>(remove)</option>";
					$output .= "</select>";
				} else $output .= "- ".$current_value;
				$output .= "</td>";
				$output .= "</table>";

				$total_applied_str = "";

				if($bill_waived)
				{
					$due_str = "<span class='info_value'>Waived</span>";
				}
				else
				{
					$total_applied = 0;
					for($n=0; $n<count($payments); $n++)
					{
						$payment_id = ($n + 1);
						$payment_date = $payments[$n][0];
						$payment_amount = $payments[$n][1];
						$payment_type = $payments[$n][2];
						$payment_approved = $payments[$n][3];
						$payment_xtype = $payments[$n][5];

						if($bill_paid < $bill_amount)
						{
							$payment_can_apply_to_bill = false;
							if(($bill_type==$payment_type || $payment_type=="General") && (($payment_xtype=="auth_capture" || $payment_xtype=='Misc' || $payment_xtype=='Paypal' || $payment_xtype=='Check' || $payment_xtype=='Adjust') || $payment_xtype==strtolower($bill_type)))
								$payment_can_apply_to_bill = true;

							if($payment_approved && $payments_applied[$n] < $payment_amount && $payment_can_apply_to_bill)
							{
								$payment_left = $payment_amount - $payments_applied[$n];

								if(number_format($payment_left,2) > 0 && number_format($bill_due,2) > 0)
								{
									if($payment_left > $bill_due)
									{
										$amount_applied = $bill_due;
									}
									else
									{
										$amount_applied = $payment_left;
									}

									$payments_applied[$n] += (str_replace(",","",number_format($amount_applied, 2)) * 1);
									$bill_due -= $amount_applied;
									$total_applied += $amount_applied;
									$applied_payment_count++;

									//$output .= "<span class='info_value'><font color='#000044'>Payment " . $payment_id . " ($".number_format($amount_applied,2).")</font> <font style='color:#7788aa; font-size:11px'>$payment_date</font></span><br>";

									$payments_tied_to_this_bill[] = $payment_id;
									if ($amount_applied<$payment_amount) {
										$amounts_applied[$payment_id] = array($amount_applied, $payment_amount);
										if ($i==(count($billable) - 1) && (float)($payments_applied[$n] * 1)<(float)($payment_amount * 1)) $over_payment = array(($payment_id - 1), $payment_amount, ($payment_amount - $payments_applied[$n]));
									}

									//$debug = false;
									//if ($invoice_id>=34 && $invoice_id<36) $debug = true;

									//if ($debug) echo "<br><br>$invoice_id BEFORE1: ".print_r($payments_tied_to_this_bill, true);
									//if ($debug) echo "<br>$invoice_id BEFORE2: ".print_r($track_display_of_credits, true);

									if (in_array($n, $payments_with_credits)) {
										$credited = 0;
										for ($cr = 0; $cr < count($tied_credit_info); $cr++) {
											$cr_info = $tied_credit_info[$cr];
											if ($cr_info[2] == $n) {
												$payments_tied_to_this_bill[] = ($cr_info[0] + 1);
												$new_index = (count($payments_tied_to_this_bill) - 1);

												//if ($debug) echo "<br><br>new_index: $new_index";

												$credited += $cr_info[3];
												$tdoc = $track_display_of_credits[$cr_info[0]];

												//if ($debug) echo "<br><br>tdoc: ".print_r($tdoc, true);

												$tdoc[0] += $cr_info[3];
												if ($tdoc[1] == $invoice_id) {
													unset($payments_tied_to_this_bill[$tdoc[2]]);
													$new_index--;
												}

												//if ($debug) echo "<br><br>new_index: $new_index";

												$tdoc[1] = $invoice_id;
												$tdoc[2] = $new_index;
												$track_display_of_credits[$cr_info[0]] = $tdoc;
											}
										}

										//echo "<br><br>credited: $credited, amount_applied: $amount_applied";

										if ($credited>=$amount_applied || $amount_applied<=$payment_left) $tied_credited[] = $payment_id;
									}

									//if ($debug) echo "<br>AFTER1: ".print_r($payments_tied_to_this_bill, true);
									//if ($debug) echo "<br>AFTER2: ".print_r($track_display_of_credits, true);
									//if ($debug) echo "<br>AFTER3: ".print_r($tied_credited, true);

									if ($bill_due == 0) {
										if ($bill_type == "License") $license_is_paid = true;
										break;
									} else if ($bill_type == "License") $license_is_paid = false;
								}
							}
							else if (!$payment_approved && $payment_can_apply_to_bill)
							{
								if (!in_array($payment_id, $tied_declines)) {
									$payments_tied_to_this_bill[] = $payment_id;
									$tied_declines[] = $payment_id;
								}
							}
							else if (in_array($n, $payments_with_credits))
							{

								//if ($debug) echo "<br><br>$invoice_id BEFORE1: ".print_r($payments_tied_to_this_bill, true);
								//if ($debug) echo "<br>$invoice_id BEFORE2: ".print_r($track_display_of_credits, true);

								if (!in_array($payment_id, $tied_credited)) {
									$payments_tied_to_this_bill[] = $payment_id;
									$tied_credited[] = $payment_id;
									for ($cr = 0; $cr < count($tied_credit_info); $cr++) {
										$cr_info = $tied_credit_info[$cr];
										if ($cr_info[2] == $n) {
											$payments_tied_to_this_bill[] = ($cr_info[0] + 1);
											$new_index = (count($payments_tied_to_this_bill) - 1);

											//if ($debug) echo "<br><br>new_index: $new_index";

											$tdoc = $track_display_of_credits[$cr_info[0]];

											//if ($debug) echo "<br><br>tdoc: ".print_r($tdoc, true);

											$tdoc[0] += $cr_info[3];
											if ($tdoc[1] == $invoice_id) {
												unset($payments_tied_to_this_bill[$tdoc[2]]);
												$new_index--;
											}

											//if ($debug) echo "<br><br>new_index: $new_index";

											$tdoc[1] = $invoice_id;
											$tdoc[2] = $new_index;
											$track_display_of_credits[$cr_info[0]] = $tdoc;
										}
									}
								}

								//if ($debug) echo "<br>AFTER1: ".print_r($payments_tied_to_this_bill, true);
								//if ($debug) echo "<br>AFTER2: ".print_r($track_display_of_credits, true);
								//if ($debug) echo "<br>AFTER3: ".print_r($tied_credited, true);

							}
							else
							{
								//$output .= "cannot apply payment " . $payment_id . " - approved: $payment_approved | $payments_applied[$n] < $payment_amount - payment_can_apply: $payment_can_apply_to_bill $payment_xtype<br>";
							}
						}
						else $output .= " $package - $bill_paid / $bill_amount ";
					}
					if($bill_due=='0'){
						$due_str.="<script type=text/javascript>$('#bill".$i."').html(\"$ $bill_amount √\") </script>";
					}
					//$grand_total_applied += $total_applied;
					if (count($payments_tied_to_this_bill)>1 && $show_actual_bill_amount) $total_applied_str = "<tr style='font-family:Arial, Helvetica, sans-serif; font-size:13px;'><td style='color:#004400; border-top:1px solid #307130'><nobr><b></b></nobr></td><td colspan='2' style='border-top:1px solid #307130'>&nbsp;</td><td style='color:#004400; border-top:1px solid #307130;' align='right'><b>$".number_format($total_applied, 2)."</b></td><td colspan='4' style='border-top:1px solid #307130'>&nbsp;</td></tr>";

					if ($license_is_paid && !strstr($rest_read['license_status'], "Paid")) $update_license_status = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `license_status` = 'Paid' WHERE `id` = '[1]'", $rest_id);
					if (!$license_is_paid && strstr($rest_read['license_status'], "Paid")) $update_license_status = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `license_status` = 'Due' WHERE `id` = '[1]'", $rest_id);
				}

				$bill_due = str_replace(",","",number_format($bill_due,2)) * 1;
				if($bill_due > 0)
				{
					$due_str .= "<span class='info_value'><font style='font-size:11px; color:#880000'>Due: $".number_format($bill_due,2)."</font></span>";
					$bills_due_list[] = array("invoice_id"=>$invoice_id,"amount"=>$bill_amount,"paid"=>$bill_paid,"amount_due"=>$bill_due,"type"=>$bill_type,"date_due"=>$bill_date);
					if ($bill_date < date("Y-m-d")) {
						if(!isset($bill_types_due[$bill_type]))
							$bill_types_due[$bill_type] = 0;
						$bill_types_due[$bill_type] += $bill_due;
						$all_bills_due += $bill_due;
						if($earliest_past_due=="" || $bill_date < $earliest_past_due) $earliest_past_due = $bill_date;
					}
				}
				//$output .= "No Payments Applied";
				$output .= "<br>";
			}

			$output .= "</td><td valign='top' style='border-left:1px solid #888888; padding:1px 10px 1px 25px;'><table cellspacing='0' cellpadding='2'>";
			foreach ($payments_tied_to_this_bill as $payment_id) {
				if (isset($amounts_applied[$payment_id]) && $show_actual_bill_amount) {
					$payment_amount = $amounts_applied[$payment_id][1];
					$applied_amount = $amounts_applied[$payment_id][0];
					$output .= str_replace("$".number_format($payment_amount, 2), "</b>(Out of<b> $".number_format($payment_amount, 2)."</b>) <b>$".number_format($applied_amount, 2), $payments_code_array[($payment_id - 1)]);
				} else if (!$show_actual_bill_amount) {
					$payment_amount = $payment_amounts[($payment_id - 1)];
					$output .= str_replace("$".number_format($payment_amount, 2), "Paid", $payments_code_array[($payment_id - 1)]);
				} else $output .= $payments_code_array[($payment_id - 1)];
				if (!in_array($payment_id, $tied_payments)) $tied_payments[] = $payment_id;
			}
			$output .= $total_applied_str;
			if ($due_str != "") $output .= "<tr><td colspan='2'></td><td colspan='2' align='right'><table cellpadding='2'><td style='border:1px solid #000000'>$due_str</td></table></td><tr>";
			$output .= "</table><br></td></tr>";

			if (!$a_bill_is_shown && $i==(count($billable) - 1)) {
				$output .= "<tr><td colspan='2' class='info_value' style='padding:4px 50px 4px 50px'>No bills found for this account</td></tr>";
				if (count($payments) == 0) $output .= "<tr><td colspan='2' class='info_value' style='padding:4px 60px 4px 50px'>No payments found for this account</td></tr>";
			}

			$output .= "<tr><td colspan='2'><hr style='height: 1px; margin:2px 0px 2px 0px;'></td></tr></table>";
			$output.='</article>
				</div>';

			/*$output.="
			<script type='text/javascript'>
			currentHeight=$('.ac-small').height();
			alert($('#billingTable-".$counter."').height());
			if( currentHeight+30> currentHeight)

				$('.ac-small').height( $('#billingTable-".$counter."').height());
			</script>";
		*/
		}//END OF FOR LOOP
		$output.= '</div></section>';

		//echo "<tr><td colspan=10'><b>grand_total_due: $grand_total_due - grand_total_applied: $grand_total_applied = ".($grand_total_due - $grand_total_applied)."</b></td></tr>";




		$other_payments = false;
		if ($over_payment) {
			$output .= "<table><tr><td class='section_header' valign='top' align='right'><font size='-1'>Other Payments:</font></td><td valign='top' style='border-left:1px solid #888888; padding:1px 10px 1px 25px;'><table>";
			$output .= str_replace("$".number_format($over_payment[1], 2), "</b>(Out of<b> $".number_format($over_payment[1], 2)."</b>) <b>$".number_format($over_payment[2], 2), $payments_code_array[$over_payment[0]]);
			$other_payments = true;
		}
		for($n=0; $n<count($payments); $n++) {
			if (!in_array($n + 1, $tied_payments)) {
				if (!$other_payments) $output .= "<table><tr><td class='section_header' valign='top' align='right'><font size='-1'>Other Payments:</font></td><td valign='top' style='border-left:1px solid #888888; padding:1px 10px 1px 25px;'><table>";
				$output .= $payments_code_array[$n];
				$other_payments = true;
			}
		}
		if ($other_payments) $output .= "</table></td></tr><tr><td colspan='2'><hr style='height: 1px; margin:2px 0px 2px 0px;'></td></tr>";
		$output .= "</table>";

		if ($lavu_admin) {
			$output .= "<script language='javascript'>";
			$output .= "  function get_checked_list() { ";
			$output .= "    cstr = ''; ";
			$output .= $checked_list_js;
			$output .= "    return cstr; ";
			$output .= "  } ";
			$output .= "</script>";

			$mbc_code = "";
			ob_start();
			require_once(dirname(__FILE__) . "/../make_billing_change.php");
			$mbc_code = ob_get_contents();
			ob_end_clean();
			$output .= $mbc_code;

			//----------------------------------------- Apply to Checked -------------------------------------------------//
			//$output .= "<br><span class='info_label'>Apply to checked:</span> <input type='text' name='checkbox_amount' id='checkbox_amount' onKeyPress='if(enter_pressed(this,event)) window.location = \"index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=\" + get_checked_list() + \"&set_invoice_amount=\" + document.getElementById(\"checkbox_amount\").value'>";
			$output .= "<br><span class='info_label'>Apply to checked:</span> <input type='text' name='checkbox_amount' id='checkbox_amount' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"\",\"Change Invoice Amounts to $\" + parseFloat(document.getElementById(\"checkbox_amount\").value).toFixed(2),\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=\" + get_checked_list() + \"&set_invoice_amount=\" + document.getElementById(\"checkbox_amount\").value,\"\",get_checked_list(),\"\")'>";
			//$output .= "<select onchange='window.location = \"index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=\" + get_checked_list() + \"&set_invoice_waived=\" + this.value'>";
			$output .= "<select onchange='return make_billing_change(this,\"$current_value\",\"Change Invoice Status\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=\" + get_checked_list() + \"&set_invoice_waived=\" + this.value,\"\",get_checked_list(),\"\")'>";
			$output .= "<option value=''></option>";
			$output .= "<option value=''>Active</option>";
			$output .= "<option value='1'>Waived</option>";
			$output .= "</select>";
			//------------------------------------------------------------------------------------------------------------//

			//--------------------------------------- Create a custom invoice --------------------------------//
			$custom_invoice_date_options = $custom_payment_date_options;

			$invstr = "";
			$invstr .= "<span class='info_label'>Add Custom Invoice:</span> <select name='custom_invoice_type' id='custom_invoice_type'><option value='License'>License</option><option value='Other'>Other</option></select>";
			$invstr .= "<select name='custom_invoice_date' id='custom_invoice_date'>$custom_invoice_date_options</select>";
			//$invstr .= " <input type='text' name='custom_invoice_amount' id='custom_invoice_amount' onKeyPress='if(enter_pressed(this,event)) window.location = \"index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_invoice_type=\" + document.getElementById(\"custom_invoice_type\").value + \"&set_custom_invoice_date=\" + document.getElementById(\"custom_invoice_date\").value + \"&set_custom_invoice_amount=\" + document.getElementById(\"custom_invoice_amount\").value'><br>";
			$invstr .= " <input type='text' name='custom_invoice_amount' id='custom_invoice_amount' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"\",\"Add Custom Invoice\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_invoice_type=\" + document.getElementById(\"custom_invoice_type\").value + \"&set_custom_invoice_date=\" + document.getElementById(\"custom_invoice_date\").value + \"&set_custom_invoice_amount=\" + document.getElementById(\"custom_invoice_amount\").value,\"\",\"\",\"\")'><br>";
			$output .= "<br><br>" . $invstr;
			//-----------------------------------------------------------------------------------------------//

			$output .= "<br>".$custom_payment_string;

			//----- Set custom monthly hosting amount -----//

			$output .= "<br>";
			$output .= "<span class='info_label'>Custom Monthly Hosting Amount:</span> ";
			$output .= "<input type='text' name='custom_monthly_hosting' id='custom_monthly_hosting' value='".$custom_monthly_hosting."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$custom_monthly_hosting."\",\"Set Custom Monthly Hosting\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_monthly_hosting=\" + document.getElementById(\"custom_monthly_hosting\").value,\"\",\"\",\"\")'>";

			//----- Set custom auto waive until -----//

			$output .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";
			$output .= "<span class='info_label'>Automatically Waive Until:</span> ";
			$output .= "<select onchange='return make_billing_change(this,\"$auto_waive_until\",\"Set Auto Waive Until Date\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_auto_waive_until=\" + this.value,\"\",\"\",\"\")'>";
			$output .= "<option value=''></option>";
			$fdate = dtadd($last_hosting_bill_date, 1, "month");
			while ($fdate < dtadd(date("Y-m-d"), 1, "year", 2, "month")) {
				$selected = "";
				if ($fdate == $auto_waive_until) $selected = " SELECTED";
				$output .= "<option value='$fdate'$selected>$fdate</option>";
				$fdate = dtadd($fdate, 1, "month");
			}

			$output .= "</select><br>";

			//----- Set custom device limits / Tab/Table mode exception setting-----//

			$output .= "<br>";
			$output .= "<span class='info_label'>Custom iPad Limit:</span> ";
			$output .= "<input type='text' name='custom_max_ipads' id='custom_max_ipads' size='6' value='".$custom_max_ipads."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$custom_max_ipads."\",\"Set Custom iPad Limit to \" + (((document.getElementById(\"custom_max_ipads\").value * 1) > 0) ? (document.getElementById(\"custom_max_ipads\").value * 1) : \"none\"),\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_max_ipads=\" + document.getElementById(\"custom_max_ipads\").value,\"\",\"\",\"\")'>";
			$output .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";

			$output .= "<span class='info_label'>Tableside iPad Limit:</span> ";
			$output .= "<input type='text' name='tableside_max_ipads' id='tableside_max_ipads' size='6' value='".$tablesideMaxIpads."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$tablesideMaxIpads."\",\"Set Tableside iPad Limit to \" + (((document.getElementById(\"tableside_max_ipads\").value * 1) > 0) ? (document.getElementById(\"tableside_max_ipads\").value * 1) : \"none\"),\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_tableside_max_ipads=\" + document.getElementById(\"tableside_max_ipads\").value,\"\",\"\",\"\")'>";
			$output .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";

			$output .= "<span class='info_label'>Custom iPhone/iPod Limit:</span> ";
			$output .= "<input type='text' name='custom_max_ipods' id='custom_max_ipods' size='6' value='".$custom_max_ipods."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$custom_max_ipods."\",\"Set Custom iPhone/iPod Limit to \" + (((document.getElementById(\"custom_max_ipods\").value * 1) > 0) ? (document.getElementById(\"custom_max_ipods\").value * 1) : \"none\"),\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_max_ipods=\" + document.getElementById(\"custom_max_ipods\").value,\"\",\"\",\"\")'>";
			$output .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";

			$checked = "";
			$setTo = "1";
			if ($force_allow_tabs_n_tables) {
				$checked = " CHECKED";
				$setTo = "0";
			}
			$output .= "<span class='info_label'>Force Allow Tab/Table Mode:</span> ";
			$output .= "<input type='checkbox' name='force_allow_tabs_n_tables' id='force_allow_tabs_n_tables' onchange='return make_billing_change(this,\"$force_allow_tabs_n_tables\",\"Set Force Allow Tab/Table Mode to ".(($setTo=="0")?"No":"Yes")."\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_force_allow_tabs_n_tables=$setTo\",\"\",\"\",\"\")'$checked>";
			$output .= "<br>";

			if($p_area=="billing_info")
			{
				$output .= "<br>";
				$output .= "<span class='info_label'>Collection Mode:</span> ";
				$output .= "<select name='collection_mode' id='collection_mode' onchange='return make_billing_change(this,\"$collection_mode\",\"Change Collection Mode: \" + document.getElementById(\"collection_mode\").value,\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_collection_mode=\" + document.getElementById(\"collection_mode\").value)'>";
				$output .= "<option value=''>Use default collection rules</option>";
				$output .= "<option value='on'".($collection_mode=="on"?" selected":"").">Collection On</option>";
				$output .= "<option value='off'".($collection_mode=="off"?" selected":"").">Do not collect</option>";
				$output .= "</select>";
				$output .= "<br>";
			}

			//$output .= "<input type='text' name='custom_max_ipods' id='custom_max_ipods' size='6' value='".$custom_max_ipods."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$custom_max_ipods."\",\"Set Custom iPhone/iPod Limit to \" + (((document.getElementById(\"custom_max_ipods\").value * 1) > 0) ? (document.getElementById(\"custom_max_ipods\").value * 1) : \"none\"),\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_max_ipods=\" + document.getElementById(\"custom_max_ipods\").value,\"\",\"\",\"\")'>";

			//$output .= "<input type='text' name='auto_waive_until' id='auto_waive_until' value='".$auto_waive_until."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$auto_waive_until."\",\"Set Custom Monthly Hosting\",\"index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_auto_waive_until=\" + document.getElementById(\"auto_waive_until\").value,\"\",\"\",\"\")'><br>";
		}

		/*for($i=0; $i<count($bills_due_list); $i++)
		{
			foreach($bills_due_list[$i] as $key => $val)
			{
				$output .= $key . " = " . $val . "<br>";
			}
			$output .= "<br>";
		}*/
		$duestr = "";
		$set_due_info = "";
		foreach($bill_types_due as $key => $val)
		{
			$duestr .= "<span class='info_label'>Due for " . $key . ":</span> <span class='info_value'>$" . number_format($val,2)."</span> &nbsp;&nbsp;";
			$set_due_info .= $key . ": $" . number_format($val,2) . "|";
		}
		$duestr .= "<span class='info_label'>Total Due:</span> <span class='info_value'>$" . number_format($all_bills_due,2) ."</span>";
		$set_due_info .= "Total: $" . number_format($all_bills_due,2);
		if($earliest_past_due!="")
			$set_due_info .= "|Due since " . $earliest_past_due;

		$set_due_info .= "|Paid last 15: $" . number_format($paid_last_two_weeks,2);
		$set_due_info .= "|Paid last 31: $" . number_format($paid_last_four_weeks,2);
		$set_due_info .= "|Last Payment Made: " . $last_payment_made;

		$output = "&nbsp;<br>".$duestr."<br>".$output."<br>".$duestr."<br><br>";

		if (!$status_record_exists) {
			$status_exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_id);
			if (mysqli_num_rows($status_exist_query)) $status_record_exists = true;
		}

		if ($status_record_exists) mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `due_info`='[1]' where `restaurantid`='[2]' limit 1",$set_due_info,$rest_id);
		else {
			$success = mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`,`due_info`) values ('[1]','[2]','[3]')",$rest_id,$dataname,$due_info);
			if (!$success) {
				echo "<br>Failed to create new payment_status for $rest_id - $dataname<br>$due_info";
				exit();
			}
		}

		return array("output"=>$output,"bill_types_due"=>$bill_types_due,"all_bills_due"=>$all_bills_due,"due_info"=>$set_due_info);
	}
?>
