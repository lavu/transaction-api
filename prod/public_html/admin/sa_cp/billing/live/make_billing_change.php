<?php
	function js_setvar($varname,$varvalue)
	{
		echo "var $varname = \"".str_replace("\"","&quot;",$varvalue)."\"; ";
	}

	global $loggedin;
	global $loggedin_fullname;
	global $in_lavu;

	if(!$loggedin && !$in_lavu)
	{
		echo "not loggedin";
		exit();
	}
?>

<script language="javascript">
	<?php
		js_setvar("current_datetime",date("Y-m-d H:i:s"));
		js_setvar("show_datetime",date("m/d h:i a"));
		js_setvar("current_loggedin",$loggedin);
		js_setvar("current_loggedin_fullname",$loggedin_fullname);
	?>
</script>
<script src='/sa_cp/billing/make_billing_change.js' type='text/javascript'></script>

<?php
	require_once(dirname(__FILE__)."/../floating_window.php");
	$fwheight = (isset($set_fwheight))?$set_fwheight:250;
	echo create_info_layer2(400,$fwheight);
?>
