<?php

	if (!$load_billing_individual_accounts)
		return;

	function verify_fields_for_payment_responses()
	{
		$varchar_cols = array("payment_type");

		$presp_fields = array();
		$presp_query = mlavu_query("show columns from `poslavu_MAIN_db`.`payment_responses`");
		while($presp_read = mysqli_fetch_assoc($presp_query))
		{
			$fieldname = $presp_read['Field'];
			$presp_fields[$fieldname] = true;
		}

		for($i=0; $i<count($varchar_cols); $i++)
		{
			$fieldname = $varchar_cols[$i];
			if(!isset($presp_fields[$fieldname]))
			{
				mlavu_query("alter table `poslavu_MAIN_db`.`payment_responses` add `[1]` varchar(255) not null default ''",$fieldname);
				echo "Added Field: $fieldname<br>";
			}
		}
	}

	function verify_invoice_table()
	{
		$invoice_query = mlavu_query("show tables from `poslavu_MAIN_db` LIKE 'invoices'");
		if(mysqli_num_rows($invoice_query))
		{
			return true;
		}
		else
		{
			echo "creating invoice table....<br>";
			mlavu_query("CREATE TABLE `poslavu_MAIN_db`.`invoices` (`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT, `due_date` VARCHAR(255), `dataname` VARCHAR(255), `restaurantid` VARCHAR(255), `amount` VARCHAR(255), `waived` VARCHAR(255), `type` VARCHAR(255), `responseid` VARCHAR(255))");
		}
	}

	function verify_billing_log_table()
	{
		$exist_query = mlavu_query("show tables from `poslavu_MAIN_db` LIKE 'billing_log'");
		if(mysqli_num_rows($exist_query))
		{
			//echo "invoice table exists!<br>";
			return true;
		}
		else
		{
			echo "creating billing log table....<br>";
			$success = mlavu_query("CREATE TABLE `poslavu_MAIN_db`.`billing_log` (`id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT, `datetime` VARCHAR(255), `dataname` VARCHAR(255), `restaurantid` VARCHAR(255), `username` VARCHAR(255), `fullname` VARCHAR(255), `ipaddress` VARCHAR(255), `action` VARCHAR(255), `details` TEXT, `target` VARCHAR(255), `billid` VARCHAR(255), `paymentid` VARCHAR(255))");
			if($success)
			{
				$sc1 = mlavu_query("ALTER TABLE `poslavu_MAIN_db`.`billing_log` ADD INDEX(dataname)");
				$sc2 = mlavu_query("ALTER TABLE `poslavu_MAIN_db`.`billing_log` ADD INDEX(restaurantid)");
				$sc3 = mlavu_query("ALTER TABLE `poslavu_MAIN_db`.`billing_log` ADD INDEX(billid)");
				$sc4 = mlavu_query("ALTER TABLE `poslavu_MAIN_db`.`billing_log` ADD INDEX(paymentid)");
				if($sc1) echo "created index 1....<br>";
				if($sc2) echo "created index 2....<br>";
				if($sc3) echo "created index 3....<br>";
				if($sc4) echo "created index 4....<br>";
			}
		}
	}

	$ip_mp_output = "";
	require_once(dirname(__FILE__)."/../../procareas/monthly_parts.php");
	require_once(dirname(__FILE__)."/../../procareas/invoice_parts.php");
	if (InvoiceParts::check_edit($ip_mp_output)) echo "<script type='text/javascript'>window.location = '?dn={$dn}';</script>";
	if (MonthlyInvoiceParts::check_edit($ip_mp_output)) echo "<script type='text/javascript'>window.location = '?dn={$dn}';</script>";
	verify_invoice_table();
	verify_fields_for_payment_responses();
	verify_billing_log_table();

	echo "<body>";

	echo $ip_mp_output;

	$p_area = "billing_info";
	global $b_do_quit_after_loading_billing_info;
	$b_do_quit_after_loading_billing_info = TRUE;
	echo "<script type='text/javascript' src='/manage/js/jquery/js/jquery-1.9.0.js'></script>";
	if ($_SERVER['REMOTE_ADDR'] == '74.95.18.90')
		ob_start();
	require_once(dirname(__FILE__)."/../../billing_info.php");
	if ($_SERVER['REMOTE_ADDR'] == '74.95.18.90') {
		$s_contents = ob_get_contents();
		ob_end_clean();
		echo $s_contents;
	}
	echo "<script type='text/javascript' src='/manage/js/jquery/development-bundle/ui/jquery-ui.custom.js'></script>";
	echo "<script type='text/javascript' src='/manage/js/jquery/development-bundle/ui/jquery.ui.widget.js'></script>";
	echo "<script type='text/javascript' src='/manage/js/jquery/development-bundle/ui/jquery.ui.dialog.js'></script>";

	echo "</body>";

?>