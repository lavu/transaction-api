<?php
/******************************************************************************************
 * A helper file for parse_bills.php.
 * Probably the only useful function in here that can be used from another file is award_points_for_bills_give_points()
 *
 * author: Benjamin Bean (benjamin@poslavu.com)
 ******************************************************************************************/

require_once(dirname(__FILE__)."/../../package_levels_object.php");
require_once(dirname(__FILE__)."/../../../../cp/objects/deviceLimitsObject.php");
require_once(dirname(__FILE__)."/../../../../manage/globals/email_addresses.php");
if (!isset($o_package_container) || $o_package_container === NULL)
	$o_package_container = new package_container();

global $o_emailAddressesMap;

// adds a bill to a_all_bills_status
// removes any bills without the same dataname
// ignore bills with a null dataname
$a_all_bills_status = array();
function collect_info_on_bills($invoice_id, $dataname, $bill_due, $bill_waived, $bill_type, $bill_amount, $bill_date, $bill_points_applied, $bill_distro_code_applied, $bill_cmsn_applied=0.00) {
	global $a_all_bills_status;

	if ($dataname == '')
		return;
	if (count($a_all_bills_status) > 0)
		foreach($a_all_bills_status as $k=>$a_bill_status)
			if ($a_bill_status['dataname'] != $dataname)
				unset($a_all_bills_status[$k]);

	$a_all_bills_status[] = array('id'=>(int)$invoice_id, 'dataname'=>$dataname, 'due'=>(float)$bill_due, 'waived'=>(boolean)$bill_waived, 'type'=>$bill_type, 'amount'=>(float)$bill_amount, 'date'=>$bill_date, 'points'=>(int)$bill_points_applied, 'cmsn'=>(float)$bill_cmsn_applied, 'distro_code'=>$bill_distro_code_applied);
}

// check through every bill in $a_all_bills_status (which should only contain bills for the current dataname)
// and awards points if it meets all criteria
function award_points_for_bills() {
	global $a_all_bills_status;
	global $o_package_container;
	global $maindb;
	global $dataname;
	global $o_device_limits;
	global $o_emailAddressesMap;

	$i_pre_cutoff_ts = strtotime("141 days ago");//5 days ago");
	$i_post_cutoff_ts = strtotime("+141 days");//5 days");
	$a_recurring_points_packages = $o_package_container->get_recurring_points_packages();
	$a_recurring_points_package_levels = array();

	if (count($a_all_bills_status) == 0)
		return;

	foreach($a_recurring_points_packages as $o_package) {
		$i_level = $o_package->get_level();
		$a_recurring_points_package_levels[$i_level] = $i_level;
	}

	foreach($a_all_bills_status as $a_bill_status) {
		// check ALL the conditions
		if ($a_bill_status['waived']) {
			continue;
		}
		if ($a_bill_status['due'] > 0) {
			continue;
		}
		if ($a_bill_status['points'] != 0) {
			continue;
		}
		if ($a_bill_status['distro_code'] != '') {
			continue;
		}
		if (strtolower($a_bill_status['type']) != 'hosting') {
			continue;
		}
		$i_bill_ts = strtotime($a_bill_status['date']);
		if ($i_bill_ts < $i_pre_cutoff_ts) {
			continue;
		}
		if ($i_bill_ts > $i_post_cutoff_ts) {
			continue;
		}

		// find the restaurant/distro
		require_once(dirname(__FILE__)."/../../../../distro/distro_data_functions.php");
		$a_distro = get_distro_from_array_distrocode_dataname(array(), '', $a_bill_status['dataname']);
		$a_restaurant = get_restaurant_from_dataname($a_bill_status['dataname']);
		if (count($a_distro) == 0) {
			award_points_for_bills_log_no_distro($a_bill_status, $a_restaurant, $a_distro);
			continue;
		}

		// find the package and make sure that it's a points package
		$a_package = find_package_status($a_restaurant['id']);
		if (!in_array((int)$a_package['package'], $a_recurring_points_package_levels)) {
			continue;
		}

		// check that the distro is good to get points
		if ($a_distro['username'] == '' || $a_distro['username'] == 'zephyrhardware' || $a_distro['username'] == 'century') {
			continue;
		}

		// get the number of points to be applied
		if ((int)$a_bill_status['id'] > 582733) {
			$a_points = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("invoice_parts", array("invoice_id"=>$a_bill_status['id']), TRUE, array('selectclause'=>'SUM(`distro_points`) AS `distro_points`'));
			$i_points_to_award = (count($a_points) > 0) ? $a_points[0]['distro_points'] : 0;
			$i_package_points = $o_package_container->get_recurring_points_by_attribute('level', (int)$a_package['package']);
			if ($i_points_to_award != $i_package_points && $i_points_to_award > 0) {
				error_log("possible mismatch of points for bill");
				$o_emailAddressesMap->sendEmailToAddress("billing_errors", "possible mismatch of points for bill", "Dataname: ".$a_bill_status['dataname']."\nPackage: ".$a_package['package']."\nPoints: {$i_points_to_award}\nPredicted Points: {$i_package_points}");
			}
		} else {
			$i_points_to_award = $o_package_container->get_recurring_points_by_attribute('level', (int)$a_package['package']);
		}
		if ($i_points_to_award == 0) {
			continue;
		}

		// check if the distro is in good standing
		if (!award_points_for_bills_check_good_standing($a_distro, $a_bill_status, $a_restaurant)) {
			continue;
		}

		// check the logs
		$billing_log_query = mlavu_query("SELECT `id` FROM `[maindb]`.`billing_log` WHERE `action`='[action]' AND (`details` LIKE '[details]%' OR `details` LIKE '[details2]%') LIMIT 1",
			array("maindb"=>$maindb, "action"=>'apply distro points', "details"=>'invoice id '.$a_bill_status['id'].": applied {$i_points_to_award} points to distro", "details2"=>'invoice id '.$a_bill_status['id'].': could not apply distro points'));
		if ($billing_log_query === FALSE) {
			error_log("bad billing log query (/sa_cp/billing/procareas/parse_bills.php");
			continue;
		}
		if (mysqli_num_rows($billing_log_query) > 0) {
			continue;
		}
		// award the points
		if ($a_distro['username'] == 'tmax') {
			error_log("old: ".(int)$a_distro['credits'].", to give: $i_points_to_award");
		}
		$i_new_distro_points = (int)$a_distro['credits']+(int)$i_points_to_award;
		award_points_for_bills_give_points($i_new_distro_points, (int)$a_distro['credits'], $i_points_to_award, $a_distro['username'], $a_distro['id'], $a_bill_status['id'], $a_bill_status['dataname'], $a_restaurant['id']);
	}
}

// entirely for the purpose of logging and updating the invoice
function award_points_for_bills_log_no_distro($a_bill_status, $a_restaurant, $a_distro) {
	global $maindb;

	mlavu_query("UPDATE `[maindb]`.`invoices` SET `distro_code_applied_to`='N/A' WHERE `id`='[id]'",
		array("maindb"=>$maindb, "id"=>$a_bill_status['id']));
	mlavu_query("INSERT INTO `[maindb]`.`billing_log` (`datetime`,`dataname`,`restaurantid`,`username`,`fullname`,`action`,`details`) VALUES ('[datetime]','[dataname]','[restaurantid]','[username]','[fullname]','[action]','[details]')",
		array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'dataname'=>$a_bill_status['dataname'], 'restaurantid'=>$a_restaurant['id'], 'username'=>'award_points_for_bills.php', 'fullname'=>'award_points_for_bills.php', 'action'=>'apply distro points', 'details'=>'could not apply distro points (distro not found)'));
	mlavu_query("INSERT INTO `[maindb]`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]')",
		array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$a_distro['id'], 'resellername'=>$a_distro['username'], 'dataname'=>$a_bill_status['dataname'], 'action'=>'apply distro points', 'details'=>'could not apply distro points (distro not found)'));
}

// checks for good standing and/or bad survey response time and returns TRUE or FALSE
// affects the invoice and applies logs if FALSE
function award_points_for_bills_check_good_standing($a_distro, $a_bill_status, $a_restaurant) {
	global $maindb;

	$s_distro_good_standing = distro_in_good_standing_with_client($a_distro, '', $a_bill_status['dataname']);

	if (in_array($s_distro_good_standing, array('bad_no_distro', 'bad_restaurant', '25days_old', 'non_points_package')))
		return FALSE;
	return TRUE;

	if (!in_array($s_distro_good_standing, array('good','3months_old','did_not_answer_survey','bad_survey_response_time'))) {
		if (in_array($s_distro_good_standing, array('bad_interaction','bad_satisfaction'))) {
			mlavu_query("UPDATE `[maindb]`.`invoices` SET `distro_code_applied_to`='[distro_code]' WHERE `id`='[id]'",
				array("maindb"=>$maindb, "distro_code"=>$a_distro['username'], "id"=>$a_bill_status['id']));
			mlavu_query("INSERT INTO `[maindb]`.`billing_log` (`datetime`,`dataname`,`restaurantid`,`username`,`fullname`,`action`,`details`) VALUES ('[datetime]','[dataname]','[restaurantid]','[username]','[fullname]','[action]','[details]')",
				array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'dataname'=>$a_bill_status['dataname'], 'restaurantid'=>$a_restaurant['id'], 'username'=>'award_points_for_bills.php', 'username'=>'award_points_for_bills.php', 'action'=>'apply distro points', 'details'=>'could not apply distro points ('.$s_distro_good_standing.')'));
			mlavu_query("INSERT INTO `[maindb]`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]')",
				array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$a_distro['id'], 'resellername'=>$a_distro['username'], 'dataname'=>$a_bill_status['dataname'], 'action'=>'apply distro points', 'details'=>'could not apply distro points ('.$s_distro_good_standing.')'));
		}
		return FALSE;
	}

	return TRUE;
}

// actually gives the points to the distributor
// returns FALSE or TRUE
function award_points_for_bills_give_points($i_new_distro_points, $i_old_distro_points, $i_points_to_award, $distro_code, $distro_id, $invoice_id, $s_dataname, $i_restaurant_id) {
	global $maindb;
	global $b_dont_assign_points_just_testing;
	$conn = ConnectionHub::getConn('poslavu');
	$dbapi = $conn->getDBAPI();

	if (isset($b_dont_assign_points_just_testing) && $b_dont_assign_points_just_testing == TRUE) {
		$a_restaurants = $dbapi->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
		$a_package = find_package_status($a_restaurants[0]['id']);
		$i_package = $a_package['package'];
		$a_invoices = $dbapi->getAllInTable('invoices', array('id'=>$invoice_id), TRUE);
		$i_amount = $a_invoices[0]['amount'];
		$a_insertvars = array('setting'=>'find_sum_of_distro_points', 'type'=>'testing', 'value'=>$distro_code, 'value2'=>$i_points_to_award, 'value3'=>"($s_dataname,$i_package,$i_amount)");
		$s_insert_clause = $dbapi->arrayToInsertClause($a_insertvars);
		$s_query_string = "INSERT INTO `poslavu_17edison_db`.`config` $s_insert_clause";
		mlavu_query($s_query_string, $a_insertvars);
		//return;
	}

	$s_query_string = "UPDATE `[maindb]`.`invoices` SET `distro_points_applied`='[i_points_to_award]',`distro_code_applied_to`='[distro_code]' WHERE `id`='[id]'";
	$a_query_vars = array("maindb"=>$maindb, "i_points_to_award"=>$i_points_to_award, "distro_code"=>$distro_code, "id"=>$invoice_id);
	mlavu_query($s_query_string, $a_query_vars);
	if ($conn->affectedRows() == 0) {
		error_log("billing: could not update invoice to award points (invoice id ".$invoice_id.")");
		error_log($dbapi->drawQuery($s_query_string, $a_query_vars));
		return FALSE;
	}

	$s_query_string = "UPDATE `[maindb]`.`resellers` SET `credits`='[new_credits]' WHERE `username`='[distro_code]'";
	$a_query_vars = array("maindb"=>$maindb, "new_credits"=>$i_new_distro_points, "distro_code"=>$distro_code);
	mlavu_query($s_query_string, $a_query_vars);
	if ($conn->affectedRows() == 0) {
		mlavu_query("UPDATE `[maindb]`.`invoices` SET `distro_points_applied`='0',`distro_code_applied_to`='' WHERE `id`='[id]'",
			array("maindb"=>$maindb, "id"=>$invoice_id));
		error_log("billing: could not award points to distro (distro code ".$distro_code.", invoice id ".$invoice_id.")");
		error_log($dbapi->drawQuery($s_query_string, $a_query_vars));
		return FALSE;
	}

	mlavu_query("INSERT INTO `[maindb]`.`billing_log` (`datetime`,`dataname`,`restaurantid`,`username`,`fullname`,`action`,`details`,`billid`,`target`) VALUES ('[datetime]','[dataname]','[restaurantid]','[username]','[fullname]','[action]','[details]','[billid]','[target]')",
		array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'dataname'=>$s_dataname, 'restaurantid'=>$i_restaurant_id, 'username'=>'award_points_for_bills.php', 'fullname'=>'award_points_for_bills.php', 'action'=>'apply distro points', 'details'=>"invoice id {$invoice_id}: applied {$i_points_to_award} points to distro {$distro_code}", 'billid' => $invoice_id, 'target' => $i_points_to_award));
	$s_query_string = "INSERT INTO `[maindb]`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`,`credits_applied`,`invoiceid`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]','[credits_applied]','[invoiceid]')";
	$a_query_vars = array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$distro_id, 'resellername'=>$distro_code, 'dataname'=>$s_dataname, 'action'=>'apply distro points', 'details'=>'applied points to distro '.$distro_code, 'credits_applied'=>$i_points_to_award, 'invoiceid'=>$invoice_id);
	$query = mlavu_query($s_query_string, $a_query_vars);
	if ($query === FALSE)
		mail("tom@lavu.com", "failed to insert distro billing log", "{$s_query_string}\n\n".print_r($a_query_vars,TRUE));

	return TRUE;
}

?>
