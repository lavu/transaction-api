<?php
	require_once(dirname(__FILE__) . "/../../../../../../private_html/ZuoraConfig.php");

	function dtadd($dt,$amount=1,$type="days",$amount2=0,$type2="")
	{
		$type = strtolower($type);
		$type2 = strtolower($type2);
		$plus_d = 0;
		$plus_m = 0;
		$plus_y = 0;
		if ($amount2>0 && $type2!="") {
			if ($type2 == "days") $plus_d = $amount2;
			if ($type2 == "month") $plus_m = $amount2;
			if ($type2 == "year") $plus_y = $amount2;
		}
		$dt_parts = explode("-",$dt);
		if(count($dt_parts) > 2)
		{
			if(substr($type,0,4)=="year")
				$dt_ts = mktime(0,0,0,($dt_parts[1] + $plus_m),($dt_parts[2] + $plus_d),($dt_parts[0] + $amount + $plus_y));
			else if(substr($type,0,5)=="month")
				$dt_ts = mktime(0,0,0,($dt_parts[1] + $amount + $plus_m),($dt_parts[2] + $plus_d),($dt_parts[0] + $plus_y));
			else
				$dt_ts = mktime(0,0,0,($dt_parts[1] + $plus_m),($dt_parts[2] + $amount + $plus_d),($dt_parts[0] + $plus_d));

			return date("Y-m-d",$dt_ts);
		}
		return $dt;
	}

	// helper function for read_payment_responses_get_array
	function read_payment_responses_get_array_choose_function(&$a_license_values, &$a_license_dates, &$custom_monthly_hosting, &$license_value, &$license_date_paid, &$rest_id) {
		if (count($a_license_values) > 0 || count($a_license_dates) > 0) // if there are fields `extra_upgrades_applied` or `extra_reseller_ids` set in `payment_status`
			return read_payment_responses_get_array($a_license_values,$a_license_dates,$rest_id,$custom_monthly_hosting);
		else
			return read_payment_responses_get_array($license_value,$license_date_paid,$rest_id,$custom_monthly_hosting);
	}

	// processes payments from `payment_responses` and from `reseller_licenses`
	// @$wt_license_value value of the reseller license (if account was created by a distro/distributor)
	//        ($wt_license_value,$wt_license_date_paid) can be either (integer,string) or (array,array)
	// @$wt_license_date_paid date on the reseller license
	// @$s_rest_id
	// @$i_custom_monthly_hosting to help determine which payments are hosting payments
	// @return: an array of arrays from read_payment_responses_from_db, in the form
	//     array(
	//         array($date_paid,$amount_paid,$pay_type,$approved,$response_read['id'],$response_read['x_type'],$response_read),
	//         ...
	//     )
	function read_payment_responses_get_array($wt_license_value,$wt_license_date_paid,$s_rest_id,$i_custom_monthly_hosting=-1) {

		// check if the license/date vars are arrays
		$b_date_isarray = is_array($wt_license_date_paid);
		$b_license_isarray = is_array($wt_license_value);

		// account for arrays
		if ($b_license_isarray && $b_date_isarray) {
			if (count($wt_license_value) != count($wt_license_date_paid))
				mail("tom@lavu.com", "payment for $s_rest_id, date and license counts don't match", "wt_license_value:\n".print_r($wt_license_value,TRUE)."\n\nwt_license_date_paid:\n".print_r($wt_license_date_paid,TRUE));
			$i_count = min(count($wt_license_value), count($wt_license_date_paid));
			$a_retval = array();
			for($i = 0; $i < $i_count; $i++) {
				$a_nextval = read_payment_responses_from_db($wt_license_value[$i], $wt_license_date_paid[$i], -1);
				$a_retval = array_merge($a_retval, $a_nextval);
			}
			$a_nextval = read_payment_responses_from_db(0, date("Y-m-d h:m:i"), $s_rest_id, $i_custom_monthly_hosting);
			$a_retval = array_merge($a_retval, $a_nextval);
			return $a_retval;
		} else if ($b_license_isarray) {
			mail("tom@lavu.com", "payment for $s_rest_id with single date and multiple licenses", "wt_license_value:\n".print_r($wt_license_value,TRUE)."\n\nwt_license_date_paid:\n".$wt_license_date_paid);
			$wt_license_value = $wt_license_value[0];
		} else if ($b_date_isarray) {
			mail("tom@lavu.com", "payment for $s_rest_id with single license and multiple dates", "wt_license_value:\n".$wt_license_value."\n\nwt_license_date_paid:\n".print_r($wt_license_date_paid,TRUE));
			$wt_license_date_paid = $wt_license_date_paid[0];
		}

		// license and date are not arrays
		$i_license_value = $wt_license_value;
		$s_license_date_paid = $wt_license_date_paid;
		$a_nextval = read_payment_responses_from_db($i_license_value,$s_license_date_paid,$s_rest_id,$i_custom_monthly_hosting);
		return $a_nextval;
	}

	// kept for posterity (because this is php and I don't know what other files depend on this function prototype
	function read_payment_responses_from_db($license_value,$license_date_paid,$rest_id,$custom_monthly_hosting=-1)
	{
		$payments = array();
		if($license_value > 0)
		{
			$payments[] = array($license_date_paid,$license_value,"License",true,0,"license",array());
		}
		$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit' or `x_type`='Misc' or `x_type`='Paypal' or `x_type`='Check' or `x_type`='Adjust') order by `datetime` asc, `id` asc",$rest_id);
		while($response_read = mysqli_fetch_assoc($response_query))
		{
			if($response_read['x_type']=="credit")
				$amount_paid = $response_read['x_amount'] * 1;
			else
				$amount_paid = $response_read['x_amount'] * 1;
			if($response_read['x_response_code']=="1")
				$approved = true;
			else
				$approved = false;

			$pay_type = $response_read['payment_type'];
			if($pay_type=="")
			{
				if(abs($amount_paid) > 250) $pay_type = "General";
				else $pay_type = "Hosting";
				// check if the value is the same as the custom monthly hosting (primarily for p2r stuff)
				if((int)$custom_monthly_hosting !== -1) {
					if (abs($amount_paid) == (int)$custom_monthly_hosting) {
						$pay_type = "Hosting";
					}
				}

				mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `payment_type`='[1]' where `id`='[2]'",$pay_type,$response_read['id']);
			}

			$date_paid = substr($response_read['datetime'],0,10);

			$transaction_voided = false;
			if($response_read['x_trans_id']!="")
			{
				$void_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_trans_id`='[1]' and `x_type`='void'",$response_read['x_trans_id']);
				if(mysqli_num_rows($void_query))
				{
					$transaction_voided = true;
				}
			}

			if($transaction_voided)
			{
			}
			else
			{
				$payments[] = array($date_paid,$amount_paid,$pay_type,$approved,$response_read['id'],$response_read['x_type'],$response_read);
			}
		}

		return $payments;
	}

	function load_billable_invoices($props,$lic_amount,$dataname)
	{
		// initialize some variables
		$billable = array();
		$main_license_id = 0;
		if($lic_amount > 0) {
			$billable[] = array($props['license_billed'],$lic_amount,"License");

			// Bug #2733 - querying for first license invoice so we can get the ID of it and prevent us from pulling it during non-standard invoice query.  For upgrades, the main License invoice contained the 
			// old package level's license amount, so we couldn't always use $lic_amount to find the main license amount.  So, instead, we query for it using the date but we're also order by ID in case there is
			// an upgrade License invoice that happened on the same day as the main License invoice - since the main invoice should always be created first.
			$result = mlavu_query("select `id` from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `due_date`='[2]' and `type`='License' order by `id`", $dataname, $props['license_billed']);
			if ($result === false) die("{$dataname} got error during query: ". mlavu_dberror());
			$main_license_invoice = mysqli_fetch_assoc($result);

			$main_license_id = $main_license_invoice['id'];
		}

		// remove null invoices
		mlavu_query("delete from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `amount`=''",$dataname);

		// check that the first month's hosting isn't stupid
		if (strtotime($props['first_hosting']) < strtotime('2010-01-01')) {
			mail('tom@lavu.com', 'bad first hosting date', "First hosting: ".$props['first_hosting']."\nDataname: {$dataname}");
			return array("billable"=>$billable,"last_hosting_bill_date"=>$props['first_hosting']);
		}

		// Look for any Hardware invoices that pre-date the first Hosting invoice
		// CR 6705 - Moved code block before while loop to fix ordering for Hardware invoices that pre-date first Hosting invoice
		$hwq = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname`='[dataname]' AND `type`='Hardware' AND `due_date` < '[first_hosting]' AND `id`!='[main_license_id]' ORDER BY `due_date`, `id`", array("dataname"=>$dataname, "first_hosting"=>$props['first_hosting'], "main_license_id"=>$main_license_id));
		if ($hwq !== false && mysqli_num_rows($hwq) > 0) {
			while ($hwinvoice = mysqli_fetch_assoc($hwq)) {
				$billable[] = array($hwinvoice['due_date'], $hwinvoice['amount'], $hwinvoice['type']);
			}
		}

		// Go through every month starting with the first_hosting invoice through the Zuora go live date
		$dt = $props['first_hosting'];
		$dt_ts = strtotime($dt);
		$last_hosting_bill_date = $props['first_hosting'];
		if(isset($_GET['show_first_hosting'])) echo "First Hosting Date: $dt<br>";
		$rcount = 1;
		while($dt_ts <= ZuoraConfig::GOLIVE_DATE_TS && $rcount < 80)  // 80 = number of months since first recorded invoice on 2010-05-08
		{

			// check for specifically hosting and non-hosting invoices
			$b_before_oct_13 = (strtotime($dt) < strtotime("2013-10-01 00:00:00"));
			$s_hosting_check = ($b_before_oct_13) ? "" : "AND `type`='Hosting'";
			$s_other_check = ($b_before_oct_13) ? "" : "AND `type`!='Hosting'";

			// find monthly invoices
			$invoice_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[dataname]' and `due_date`='[due_date]' {$s_hosting_check}", array("dataname"=>$dataname,"due_date"=>$dt));
			if(mysqli_num_rows($invoice_query))
			{

				// the invoice exists, load it from the database
				$invoice_read = mysqli_fetch_assoc($invoice_query);
				$invoice_waived = $invoice_read['waived'];
				$invoice_id = $invoice_read['id'];
				$bill_amount = $invoice_read['amount'];

				$billable[] = array($dt, $bill_amount, "Hosting", false);  // FIX for $0 invoices
			}
			else
			{

				// the invoice doesn't exist but should, indicate it as such
				$invoice_id = 0;
				$bill_amount = $props['monthly_amount'];

				// Lavu 88 3 months for $8 promo
				global $signup_read;
				if ( strpos($signup_read['source'], '_3mo8') !== false && $dt >= $props['first_hosting'] && $dt < dtadd($props['first_hosting'], 3, 'month') ) {
					$bill_amount = '8';
				}

				// Free Lavu 88 packages (17, 20) and Mercury Gold (18) can have $0 hosting invoices
				$has_nontrial_free_lavu_license = ($signup_read['package'] == '17' || $signup_read['package'] == '18' || $signup_read['package'] == '20');

				if (($bill_amount != '' && $bill_amount !== 0 && $bill_amount !== 0.0) || $has_nontrial_free_lavu_license) $billable[] = array($dt, $bill_amount, "Hosting", false);  // FIX for $0 invoices - take 4
			}

			// get the next month's date
			$lastdt = $dt;
			$last_hosting_bill_date = $dt;
			$dt = dtadd($dt,1,"month");
			$dt_ts = strtotime($dt);
			$rcount++;

			//------------------------- Look for non standard invoices ----------------------//
			// Bug #2734 - excluding License invoices to fix duplicate License issue.
			// Bug #3459 - only excluding main License invoices to we don't exclude License Upgrade invoices.
			$cust_invoice_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `id`!='[invoice_id]' and `id`!='[main_license_id]' and `dataname`='[dataname]' and `due_date`>='[last_date]' and `due_date`<'[date]' {$s_other_check} order by `due_date`, `id`",array("invoice_id"=>$invoice_id,"dataname"=>$dataname,"last_date"=>$lastdt,"date"=>$dt,"main_license_id"=>$main_license_id));
			if(mysqli_num_rows($cust_invoice_query))
			{
				while ( $cust_invoice_read = mysqli_fetch_assoc($cust_invoice_query) ) {
					$cust_invoice_waived = $cust_invoice_read['waived'];
					$cust_invoice_id = $cust_invoice_read['id'];
					$cust_bill_amount = $cust_invoice_read['amount'];
					$cust_invoice_date = $cust_invoice_read['due_date'];
					$cust_bill_type = $cust_invoice_read['type'];

					//$output .= "<br>FOUND CUSTOM INVOICE FOR $cust_bill_amount<br>";
					$billable[] = array($cust_invoice_date,$cust_bill_amount,$cust_bill_type,false);
				}
			}
			//-------------------------------------------------------------------------------//
		}

		return array("billable"=>$billable,"last_hosting_bill_date"=>$last_hosting_bill_date);
	}

	// determines when the first license should be billed and the first hosting created
	// @$package: the package level of the restaurant
	// @$signup_status: the signup_status of the restaurant (eg 'new', 'manual')
	function get_setup_props(&$props, $package, $signup_status) {
		$prop_parts = array();

		// Use the signups row from setup_vars.php to check if this is a Lavu 88 Staple|NewEgg|Other Bundle account.
		global $signup_read;
		if ( !isset($props['signup_source']) && isset($signup_read['source']) ) $props['signup_source'] = $signup_read['source'];

 		if ($package=="4")  // Lavu Lite
		{
			$prop_parts['license_billed'] = dtadd($props['created'],0,"days");
			$prop_parts['first_hosting'] = dtadd($props['created'],14,"days");
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 14;
		}
		// Lavu 88 3 months for $8 promotion - $8 bill for the first 3 months (starts immediately) and then reverts to the normal $88 for hosting
		else if ($package == "8" && isset($props['signup_source']) && ($props['signup_source'] == 'staples_lavu88_3mo8' || $props['signup_source'] == 'newegg_lavu88_3mo8' || $props['signup_source'] == 'other_lavu88_3mo8'))
		{
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 0;
			$prop_parts['license_billed'] = dtadd($props['created'], $prop_parts['license_billing_add_days'], "days");
			$prop_parts['first_hosting']  = dtadd($props['created'], $prop_parts['first_hosting_add_days'],   "days");
		}
		// Lavu 88 Staples|NewEgg|Other Bundle promotion - free trial for 30 days
		else if ($package == "8" && isset($props['signup_source']) && ($props['signup_source'] == 'staples_lavu88' || $props['signup_source'] == 'newegg_lavu88' || $props['signup_source'] == 'other_lavu88'))
		{
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 30;
			$prop_parts['license_billed'] = dtadd($props['created'], $prop_parts['license_billing_add_days'], "days");
			$prop_parts['first_hosting']  = dtadd($props['created'], $prop_parts['first_hosting_add_days'],   "days");
		}
		else if ($package == "8")  // Lavu 88
		{
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 14;
			$prop_parts['license_billed'] = dtadd($props['created'], $prop_parts['license_billing_add_days'], "days");
			$prop_parts['first_hosting']  = dtadd($props['created'], $prop_parts['first_hosting_add_days'],   "days");
		}
		else if ($package == "9")  // Lavu Give
		{
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 14;
			$prop_parts['license_billed'] = dtadd($props['created'], $prop_parts['license_billing_add_days'], "days");
			$prop_parts['first_hosting']  = dtadd($props['created'], $prop_parts['first_hosting_add_days'],   "days");
		}
		else if ($package == "16" || $package == "20")  // Lavu 88 Trial, EvoSnapFreeLavu88
		{
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 14;  // was 14 but that resulted in invoices having the date of the last trial day
			$prop_parts['license_billed'] = dtadd($props['created'], $prop_parts['license_billing_add_days'], "days");
			$prop_parts['first_hosting']  = dtadd($props['created'], $prop_parts['first_hosting_add_days'],   "days");
		}
		else if ( ($package == "18") || ($package == "19") ) {  // Mercury Gold, Mercury Pro
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 30;
			$prop_parts['license_billed'] = dtadd($props['created'], $prop_parts['license_billing_add_days'], "days");
			$prop_parts['first_hosting'] = dtadd($props['created'], $prop_parts['first_hosting_add_days'], "days");
		}
		else if ( ($package == "25" || $package == "26") && (strtotime($props['created']) >= ZuoraConfig::GOLIVE_DATE_TS) )  // Lavu, Lavu Enterprise
		{
			// Josh Edwards wanted to shorten the trial period to 14 days (instead of the 44 days Josh Bennett originally designated) - which is actually what we advertise on lavu.com.
			// However, we could only do it for accounts created after the Zuora go-live date otherwise it would've retroactively created new first_hosting invoices for existing Lavu|LavuEnterprise accounts.
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 14;
			$prop_parts['license_billed'] = dtadd($props['created'], $prop_parts['license_billing_add_days'], "days");
			$prop_parts['first_hosting']  = dtadd($props['created'], $prop_parts['first_hosting_add_days'],   "days");
		}
		else if($signup_status=="new")
		{
			$prop_parts['license_billed'] = dtadd($props['created'],14,"days");
			$prop_parts['first_hosting'] = dtadd($props['created'],44,"days");
			$prop_parts['license_billing_add_days'] = 14;
			$prop_parts['first_hosting_add_days'] = 44;
		}
		// LP-726 -- Dan from Customer Care wanted to make Distro-created accounts have 14 day trial periods
		else if($signup_status=="manual" && strtotime($props['created']) >= strtotime('2017-02-24')) {
			$prop_parts['license_billed'] = dtadd($props['created'],0,"days");
			$prop_parts['first_hosting'] = dtadd($props['created'],14,"days");
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 14;
		}
		else if($signup_status=="manual" && strtotime($props['created']) >= strtotime('2013-06-25')) {
			$prop_parts['license_billed'] = dtadd($props['created'],0,"days");
			$prop_parts['first_hosting'] = dtadd($props['created'],30,"days");
			$prop_parts['license_billing_add_days'] = 0;
			$prop_parts['first_hosting_add_days'] = 30;
		}
		else
		{
			$prop_parts['license_billed'] = dtadd($props['created'],7,"days");
			$prop_parts['first_hosting'] = dtadd($props['created'],37,"days");
			$prop_parts['license_billing_add_days'] = 7;
			$prop_parts['first_hosting_add_days'] = 37;
		}

		return $prop_parts;
	}

	function common_billing_javascripts_tostr() {
		$retval = "";

		ob_start();
		?>
		<script language='javascript' src='/sa_cp/tools/ajax_functions.js'></script>
		<script type="text/javascript" src="/manage/js/jquery/js/jquery-1.9.0.js"></script>
		<script language='javascript'>
			function edit_invoice_amount(invoice_index) {
				$('#invoice_amount_' + invoice_index).hide();
				$('#edit_invoice_amount_' + invoice_index).show();
			}
			function enter_pressed(mfield,e) {
				var keycode;
				if (window.event)
					keycode = window.event.keyCode;
				else if (e)
					keycode = e.which;
				else
					keycode = 0;
				if (keycode == 13)
					return true;
				else
					return false;
			}
		</script>
		<?php
		$retval .= ob_get_contents();
		ob_end_clean();

		return $retval;
	}

	function dataname_in_billing_by_parts_datanames($dataname) {
		if (in_array($dataname, array(
		    'test_pizza_sho',
		    'example',
		    'uncle_joes_piz',
		)));
			return TRUE;
		return FALSE;
	}

	// function find_package_status() moved to payment_profile_functions.php

?>
