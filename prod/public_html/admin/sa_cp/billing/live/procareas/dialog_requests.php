<?php
	session_start();
	require_once(dirname(__FILE__)."/../../../../cp/resources/core_functions.php");
	require_once(dirname(__FILE__)."/../../payment_profile_functions.php");
	require_once(dirname(__FILE__)."/../../new_process_payments.php");
	require_once(dirname(__FILE__)."/../../../../../inc/billing/zuora/ZuoraUtils.php");
	require_once(dirname(__FILE__)."/../../../../../inc/billing/zuora/ZuoraWebhookEventProcessor.php");
	$maindb = "poslavu_MAIN_db";

	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posadmin_fullname']))?$_SESSION['posadmin_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posadmin_email']))?$_SESSION['posadmin_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;
	if($loggedin_access=="") $loggedin_access = "";

	if($loggedin)
	{

		$mode = postvar("mode");
		$paymentid = postvar("paymentid");
		$subscriptionid = postvar("subscriptionid");
		$containerid = postvar("containerid");
		$callback = postvar("callback");
		$connect = postvar("connect");
		$ismanage = postvar("ismanage");
		error_log($ismanage);

		if($mode=="move_payment")
		{
			$show_input = true;
			if($connect!="")
			{
				$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `id`='[1]'",$paymentid);
				if(mysqli_num_rows($pay_query))
				{
					$pay_read = mysqli_fetch_assoc($pay_query);
					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `data_name`!=''",$connect);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						$restaurantid = $rest_read['id'];
						$record_data = $loggedin . "|" . $loggedin_fullname . "|" . date("Y-m-d H:i:s") . "|" . $_SERVER['REMOTE_ADDR'] . "|" . $pay_read['x_amount'] . "|" . $pay_read['x_type'] . "|" . $pay_read['x_response_code'];

						mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `history_notes`=concat(`history_notes`,'(moving)',`match_type`,'|',`match_restaurantid`,'|[1]','(moved)'), `match_type`='payment moved', `match_restaurantid`='[2]' where `id`='[3]' limit 1",$record_data,$restaurantid,$paymentid);
						echo "move $paymentid to $connect (restaurantid is $restaurantid) !<br>";
						if ($ismanage === "yes") {
							echo "<input type='button' value='Refresh Page' onclick='window.parent.window.$(\"#billingTab\").click();' /><br>";
						} else {
							echo "<input type='button' value='Refresh Page' onclick='window.location.reload()' /><br>";
						}
						$show_input = false;
					}
					else
					{
						echo "data_name $connect not found<br>";
					}
				}
			}

			if($show_input)
			{
				echo "move to dataname: <input type='text' id='move_to_dataname'>";
				echo "<input type='button' value='Move' onclick='ajax_get_response(\"/sa_cp/billing/procareas/dialog_requests.php\",\"mode=$mode&containerid=$containerid&callback=$callback&paymentid=$paymentid&subscriptionid=$subscriptionid&ismanage=$ismanage&connect=\" + document.getElementById(\"move_to_dataname\").value,\"$callback\");' />";
			}
		}
		else if($mode=="subscription_match")
		{

			if($subscriptionid!="" && $paymentid!="")
			{
				echo "<font color='#000088'><b>$subscriptionid</b></font>";
				if($connect!="")
				{
					$pay_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `id`='[1]'",$paymentid);
					if(mysqli_num_rows($pay_query))
					{
						$pay_read = mysqli_fetch_assoc($pay_query);
						$restaurantid = $pay_read['match_restaurantid'];

						echo "<br>";
						$connect_ids = explode(",",$connect);
						for($n=0; $n<count($connect_ids); $n++)
						{
							$connectid = $connect_ids[$n];
							mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `match_type`='dialog subscriptionid match',`match_restaurantid`='[1]' where `id`='[2]' and `match_restaurantid`='' limit 1",$restaurantid,$connectid);
							echo "connect: $connectid to $restaurantid<br>";
						}
						if ($ismanage === "yes") {
							echo "<input type='button' value='Refresh Page' onclick='window.parent.window.$(\"#billingTab\").click();' /><br>";
						} else {
							echo "<input type='button' value='Refresh Page' onclick='window.location.reload()' /><br>";
						}
					}
				}
				else
				{
					$match_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_subscriptionid`='[1]' and `id`!='[2]' and `match_restaurantid`='' and `restaurantid`=''",$subscriptionid,$paymentid);
					if(mysqli_num_rows($match_query))
					{
						//echo "<br>(" . mysqli_num_rows($match_query) . " matches:)";
						echo "<script language='javascript'>";
						echo "set_connect_transactions = function(sid,schecked) { ";
						echo "	sterm_vals = document.getElementById('connect_transactions').value.split(','); ";
						echo "	sterm_value = ''; ";
						echo "	for(var n=0; n<sterm_vals.length; n++) { ";
						echo "    if(sterm_vals[n]!='' && sterm_vals[n]!=sid) { if(sterm_value!='') sterm_value += ','; sterm_value += sterm_vals[n]; } ";
						echo "  } ";
						echo "  if(schecked) { ";
						echo "  	if(sterm_value!='') sterm_value += ','; sterm_value += sid; ";
						echo "	} ";
						echo "  document.getElementById('connect_transactions').value = sterm_value; ";
						echo "} ";
						echo "</script>";
						echo "<table style='border:solid 1px #555555'>";
						while($match_read = mysqli_fetch_assoc($match_query))
						{
							$sid = $match_read['id'];

							echo "<tr>";
							echo "<td style='border-bottom:solid 1px #999999'>";
							echo "<input type='checkbox' onclick='set_connect_transactions(\"$sid\",this.checked)'> ";
							echo $match_read['datetime'] . " " . trim($match_read['x_first_name'] . " " . $match_read['x_last_name']) . " " . $match_read['x_account_number'];
							if($match_read['x_response_code']==1) echo " <font color='#008800'>approved</font>"; else echo " <font color='#880000'>declined</font>";
							echo "</td>";
							echo "</tr>";
						}
						echo "<tr>";
						echo "<td>";
						echo "<input type='hidden' name='connect_transactions' id='connect_transactions' value=''><br>";
						echo "<input type='button' value='Connect Checked Transactions' onclick='ajax_get_response(\"/sa_cp/billing/procareas/dialog_requests.php\",\"mode=$mode&containerid=$containerid&callback=$callback&paymentid=$paymentid&subscriptionid=$subscriptionid&ismanage=$ismanage&connect=\" + document.getElementById(\"connect_transactions\").value,\"$callback\");'>";
						echo "</td>";
						echo "</tr>";
						echo "</table>";
					}
					else
					{
						echo "<br>(no matches)";
					}
				}
				//echo "paymentid: $paymentid<br>subscriptionid: $subscriptionid<br>";
			}
		}
		// LP-328 -- Added import, sync, and due_info buttons and AccountId links for Zuora
		else if ($mode == 'zuora_import')
		{
			//error_log( __FILE__ ." DEBUG: in {$mode} block" );  //debug

			$dataname      = postvar('dataname');
			$start_date    = postvar('start_date');
			$package_level = postvar('package_level');
			$args = array('dataname' => $dataname, 'start_date' => $start_date, 'package_level' => $package_level);
			//error_log( __FILE__ ." DEBUG: in {$mode} block with args=". json_encode($args) );  //debug

			if ( empty($dataname) ) {
				error_log(__FILE__ ." is missing required parameters for mode={$mode}: dataname={$dataname}");
				$response = array('success' => '0', 'error_msg' => 'missing req args');
				echo http_build_query($response);
				exit;
			}

			$zInt = new ZuoraIntegration();

			// LP-403 -- Check for an existing Zuora account that just isn't recorded in `signups`.`zAccountId`
			$zAccount = $zInt->getAccount($args);

			$existing_account = ($zAccount !== null);
			$multiple_accounts = is_array($zAccount);

			// No start_date or package_level means we should not create a Subscription when creating the new account, which means using a different method.
			$new_account_without_sub = (empty($start_date) || empty($package_level));


			// LP-403 -- Check if the dataname has multiple accounts (which will cause much breakage with Zuora Integration) so we can display an error
			if ( $multiple_accounts )
			{
				$response['success'] = '0';
				$response['error_msg'] = 'Multiple accounts';
				error_log( __FILE__ ." ERROR: Could not Import dataname={$dataname} in Zuora: ". $response['error_msg'] );
				echo http_build_query($response);
				exit;
			}
			else if ( $existing_account )
			{
				//error_log( __FILE__ ." DEBUG: ...had existing Account zAccountId={$zAccount->Id}" );  //debug

				$zAccountId = $zAccount->Id;
				$zRatePlanChargeId = '';
				$zBillToContactId = $zAccount->BillToId;
				$zSoldToContactId = $zAccount->SoldToId;
				//error_log(__FILE__ ." DEBUG: ...zAccountId={$zAccountId} zBillToContactId={$zBillToContactId} zSoldToContactId={$zSoldToContactId} zRatePlanChargeId={$zRatePlanChargeId}");  //debug
			}
			else if ( $new_account_without_sub )
			{
				$results = $zInt->createAccountWithContacts($args);
				//error_log( __FILE__ ." DEBUG: ...importAccount results==". json_encode($results) );  //debug

				$zAccountId = $results[0];
				$zRatePlanChargeId = '';
				$zBillToContactId = $results[1];
				$zSoldToContactId = $results[2];
				//error_log(__FILE__ ." DEBUG: ...zAccountId={$zAccountId} zBillToContactId={$zBillToContactId} zSoldToContactId={$zSoldToContactId} zRatePlanChargeId={$zRatePlanChargeId}");  //debug
			}
			else
			{
				$results = $zInt->importAccount($args);
				//error_log( __FILE__ ." DEBUG: ...importAccount results==". json_encode($results) );  //debug

				$zAccountId = $results[0];
				$zRatePlanChargeId = $results[1];
				//error_log(__FILE__ ." DEBUG: ...zAccountId={$zAccountId} zRatePlanChargeId={$zRatePlanChargeId}");  //debug
			}

			// Zuora Integration returns the AccountId and RatePlanChargeId on successful syncs, so this seemed like a good opportunity to set it on `signups` records that do not have it populated.
			$response = array('dataname' => $dataname);
			if ( !empty($zAccountId) ) {
				$response['success'] = '1';
				$response['zAccountId'] = $zAccountId;
				$response['zRatePlanChargeId'] = $zRatePlanChargeId;

				if ( $existing_account || $new_account_without_sub ) {
					// Importing account with contacts only will result in zAccountId and zContactIds but we only save the zAccountId
					$update_sql = "UPDATE `poslavu_MAIN_db`.`signups` SET `zAccountId`='[zAccountId]' WHERE `dataname`='[dataname]' AND `dataname`!='' LIMIT 1";
				}
				else {
					// Importing account with subscription will result in zRatePlanChargeId
					$update_sql = "UPDATE `poslavu_MAIN_db`.`signups` SET `zAccountId`='[zAccountId]', `zRatePlanChargeId`='[zRatePlanChargeId]' WHERE `dataname`='[dataname]' AND `dataname`!='' LIMIT 1";
				}

				mlavu_query($update_sql, $response);
				$aff = ConnectionHub::getConn('poslavu')->affectedRows();
				if ($aff < 1) {
					error_log(__FILE__ .' got MySQL Error: '. mlavu_dberror());
					$args['error_msg'] = mlavu_dberror();
				}
				$results['updated_signups'] = $aff;
			}
			else {
				$response['success'] = '0';
				$response['error_msg'] = 'Failed';
			}

			error_log( __FILE__ ." DEBUG: ...http_build_query(response)=". http_build_query($response) );  //debug
			echo http_build_query($response);
		}
		else if ($mode == 'zuora_sync')
		{
			//error_log( __FILE__ ." DEBUG: in {$mode} block" );  //debug

			$dataname   = postvar('dataname');
			$args = array('dataname' => $dataname);
			//error_log( __FILE__ ." DEBUG: ...args=". json_encode($args) );  //debug

			if ( empty($dataname) ) {
				error_log(__FILE__ ." is missing required parameters for mode={$mode}: dataname={$dataname}");
				exit;
			}

			$zInt = new ZuoraIntegration();
			$synced = $zInt->syncAccount($args);
			//error_log( __FILE__ ." DEBUG: ...syncAccount synced={$synced}" );  //debug

			$response = array('success' => ($synced ? '1' : '0'));

			error_log( __FILE__ ." DEBUG: ...http_build_query(response)=". http_build_query($response) );  //debug
			echo http_build_query($response);
		}
		else if ($mode == 'zuora_dueinfo')
		{
			//error_log( __FILE__ ." DEBUG: in {$mode} block" );  //debug

			$dataname = postvar('dataname');
			$zAccountId = postvar('zAccountId');

			if ( empty($dataname) || empty($zAccountId) ) {
				error_log(__FILE__ ." is missing required parameters for mode={$mode}: dataname={$dataname} zAccountId={$zAccountId}");
				$response = array('success' => '0', 'error_msg' => 'missing req args');
				echo http_build_query($response);
				exit;
			}

			$zUtils = new ZuoraUtils();
			$accountUtils = new LavuAccountUtils();

			$args = array('dataname' => $dataname, 'zAccountId' => $zAccountId);
			//error_log( __FILE__ ." DEBUG: ...args=". json_encode($args) );  //debug

			try
			{
				$due_info = $zUtils->getDueInfoArray($args);
				//error_log( __FILE__ ." DEBUG: ...due_info=". json_encode($due_info) );  //debug

				$response = $accountUtils->setDueInfo($dataname, $due_info);
				//error_log( __FILE__ ." DEBUG: ...http_build_query(response)=". http_build_query($response) );  //debug
			}
			catch ( Exception $e )
			{
				error_log("Zuora Due Info button failed while getting/setting due_info array: ". $e->getMessage());
				$response = array('success' => '0', 'error_msg' => $e->getMessage());
			}

			echo http_build_query($response);
		}
		else if ($mode == 'zuora_newpaymeth')
		{
			//error_log( __FILE__ ." DEBUG: in {$mode} block" );  //debug

			$dataname = postvar('dataname');
			$zAccountId = postvar('zAccountId');
			$zPaymentMethodId = postvar('zPaymentMethodId');

			if ( empty($dataname) || empty($zAccountId) || empty($zPaymentMethodId) ) {
				error_log(__FILE__ ." is missing required parameters for mode={$mode}: dataname={$dataname} zAccountId={$zAccountId} zPaymentMethodId={$zPaymentMethodId}");
				$response = array('success' => '0', 'error_msg' => 'missing_args');
				echo http_build_query($response);
				exit;
			}

			$zInt = new ZuoraIntegration();

			$args = array('zAccountId' => $zAccountId, 'zPaymentMethodId' => $zPaymentMethodId, 'DefaultPaymentMethodId' => $zPaymentMethodId);
			//error_log( __FILE__ ." DEBUG: ...args=". json_encode($args) );  //debug

			$response = array('success' => false, 'error_msg' => '');

			$zPaymentMethod = $zInt->getPaymentMethodById($args);

			// Basic error checking before we run the update
			if ( $zPaymentMethod === null )
			{
				$response['error_msg'] = 'not_found';
			}
			else if ( !empty($zPaymentMethod->AccountId) )
			{
				$response['error_msg'] = 'already_assigned';
			}
			else
			{
				try
				{
					$update_ok = $zInt->updateAccount($args);
					$response['success'] = $update_ok;
					unset($response['error_msg']);
				}
				catch ( Exception $e )
				{
					$response['error_msg'] = $e->getMessage();
				}
			}

			if ( !empty($response['error_msg']) )
			{
				error_log("Zuora PaymentMethod Associate button failed while updating account: ". $response['error_msg']);
			}

			//error_log( __FILE__ ." DEBUG: ...http_build_query(response)=". http_build_query($response) );  //debug
			echo http_build_query($response);
		}
		else if ($mode == 'salesforce_sync')
		{
			//error_log( __FILE__ ." DEBUG: in salesforce_sync block" );  //debug

			require_once(dirname(__FILE__)."/../../procareas/SalesforceIntegration.php");
			require_once(dirname(__FILE__)."/../../../../../register/lib/utilities.php");

			$dataname = postvar('dataname');
			if (empty($dataname)) {
				error_log(__FILE__ ." got blank dataname for mode={$mode}");
				exit;
			}

			$reseller_leads = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `data_name`='[1]'", $dataname);
			if ($reseller_leads === false) error_log(__FILE__ .' got MySQL Error: '. mlavu_dberror());
			$reseller_lead = mysqli_fetch_assoc($reseller_leads);

			$account_info = SalesforceIntegration::getAccountInfo($dataname);

			$combo_array = array_merge($reseller_lead, $account_info);

			// "id" has to be reseller_lead.id, so unset it if there wasn't a reseller_leads row
			if (empty($reseller_lead['id'])) unset($combo_array['id']);

			$combo_array['salesforceid'] = postvar('salesforce_id');

			//error_log( __FILE__ ." DEBUG: dataname={$dataname} combo_array=". print_r($combo_array, true) );  //debug

			$response = send_to_salesforce($combo_array);

			//error_log( __FILE__ ." DEBUG: http_build_query(response)=". http_build_query($response) );  //debug

			$conn = ConnectionHub::getConn('poslavu');

			// The Salesforce response returns the Salesforce Opportunity ID on successful syncs, so this seemed like a good opportunity to set it on `signups` records that do not have it populated.
			if ( $response['success'] && !postvar('salesforce_id') ) {
				$result = mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `salesforce_id`='[salesforce_id]' WHERE (`salesforce_id`='' OR `salesforce_id` is null) AND `dataname`!='' AND `dataname`='[dataname]' LIMIT 1", array('salesforce_id' => $response['id'], 'dataname' => $dataname));
				if ($conn->affectedRows() < 1) error_log(__FILE__ .' got MySQL Error: '. mlavu_dberror());
			}

			// The Salesforce response returns the Salesforce Opportunity ID on successful syncs, so this seemed like a good opportunity to set it on `signups` records that do not have it populated.
			if ( $response['success'] && !postvar('salesforce_opportunity_id') ) {
				$result = mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `salesforce_opportunity_id`='[salesforce_opportunity_id]' WHERE (`salesforce_opportunity_id`='' OR `salesforce_opportunity_id` is null) AND `dataname`!='' AND `dataname`='[dataname]' LIMIT 1", array('salesforce_opportunity_id' => $response['opportunity_id'], 'dataname' => $dataname));
				if ($conn->affectedRows() < 1) error_log(__FILE__ .' got MySQL Error: '. mlavu_dberror());
			}

			echo http_build_query($response);
		}
	}
?>
