<?php

require_once(dirname(__FILE__) .'/../../../../cp/resources/lavuquery.php');
require_once(dirname(__FILE__) .'/../../../../sa_cp/billing/procareas/procpay_functions.php');  // for get_setup_props()
require_once(dirname(__FILE__) .'/../../../../sa_cp/billing/package_levels_object.php');

global $o_package_container;

class SalesforceIntegration
{
	/**
	 * Hook used by billing code to integrate with Salesforce.  Called at all points in the billing code during which the "stage"
	 * would change to insert an unprocessed row into the Salesforce integration table for the new time the data syncer runs.
	 *
	 * @param string $dataname dataname 
	 * @param string $event name of the event, preferably in all lowercase with underscores instead of spaces
	 * @param string $details additional info about the event
	 *
	 * @access public
	 * @static
	 */
	public static function createSalesforceEvent($dataname, $event, $details)
	{
		$event_id = '';

		$sql = "INSERT INTO `poslavu_MAIN_db`.`billing_salesforce_events` (createtime, dataname, event, details) VALUES (now(), '[dataname]', '[event]', '[details]')";
		$vals = array('dataname' => $dataname, 'event' => $event, 'details' => $details);
		$result = mlavu_query($sql, $vals);
		if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());  // should be emailErrorAndDie() in production, logError in DEV

		$event_id = mlavu_insert_id();

		if (empty($event_id)) self::emailError(__FILE__ ." ". __METHOD__ ." counldn't create `billing_salesforce_events` row for dataname={$dataname} event={$event} details={$details}");

		return $event_id;
	}

	/**
	 * DEPRECATED - use createSalesforceEvent() instead.  Kept for backwards compatibility with existing billing code.
	 * Hook used by billing code to integrate with Salesforce.  Called at all points in the billing code during which the "stage"
	 * would change to insert an unprocessed row into the Salesforce integration table for the new time the data syncer runs.
	 *
	 * @param string $dataname dataname 
	 * @param string $stage 
	 *
	 * @access public
	 * @static
	 *
	 * @deprecated
	 */
	public static function logSalesforceEvent($dataname, $stage)
	{
		return self::createSalesforceEvent($dataname, 'stage_change', $stage);
	}

	/**
	 * Similar to createSalesforceEvent() except it queries the database to make sure there isn't already an event of that stage with today's date.
	 *
	 * @param string $dataname dataname
	 * @param string $event name of the event, all lowercase with underscores instead of spaces
	 * @param string $details additional info about the event
	 *
	 * @access public
	 * @static
	 */
	public static function createSalesforceEventNoDupes($dataname, $event, $details)
	{
		$event_id = '';

		// We only insert an event row for this stage if we don't already have one from today.

		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_salesforce_events` WHERE `dataname` = '[dataname]' AND `event` = '[event]' AND `details` = '[details]' AND date(`createtime`) = '[current_date']", array('dataname' => $dataname, 'event' => $event, 'details' => $details, 'current_date' => date('Y-m-d')));
		if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());

		if ( mysqli_num_rows($result) === 0 ) {
			$event_id = self::createSalesforceEvent($dataname, $event, $details);
		}
		else {
			$existing_event_row = mysqli_fetch_assoc($result);
			$event_id = $existing_event_row['id'];
		}

		return $event_id;
	}

	/**
	 * Used by Salesforce data syncing process to retrieve the latest billing data for that dataname provided.
	 *
	 * @param string $dataname dataname
	 *
	 * @access public
	 * @static
	 */
	public static function getAccountInfo($dataname, $stage='')
	{
		global $o_package_container;

		$account_info = array();

		if (empty($dataname)) return $account_info;

		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[dataname]'", array('dataname' => $dataname));
		if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
		$restaurant = mysqli_fetch_assoc($result);

		if (!mysqli_num_rows($result)) return $account_info;

		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname` = '[dataname]'", array('dataname' => $dataname));
		if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
		$signup = mysqli_fetch_assoc($result);

		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_status` WHERE `restaurantid` = '[restaurantid]'", array('restaurantid' => $restaurant['id']));
		if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
		$payment_status = mysqli_fetch_assoc($result);

		lavu_connect_dn($dataname, true);
		$result = lavu_query("SELECT * FROM `poslavu_{$dataname}_db`.`locations` WHERE `_disabled` != '1' ORDER BY `id` DESC");
		if ( $result === false ) self::emailError(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". lavu_dberror());  // Changed to emailError() so it wouldn't die for accounts that were AUTO-DELETED before they could sync.
		$location = mysqli_fetch_assoc($result);
		$locations = $result;

		if (!isset($o_package_container)) $o_package_container = new package_container();

		$props = array('created' => $restaurant['created'], 'signup_source' => $signup['source']);
		$setup_props = get_setup_props( $props, $signup['package'], $signup['status'] );

		$account_info['CloseDate'] = self::getCloseDate($signup, $payment_status, $setup_props);
		$account_info['EnteredCC'] = self::getEnteredCC($dataname);
		$account_info['Description'] = self::getDescription($dataname, $payment_status);
		$account_info['LicenseType'] = self::getLicenseType($dataname, $signup);
		$account_info['LicenseDate'] = self::getLicenseDate($signup, $setup_props);
		$account_info['HostingDate'] = self::getHostingDate($setup_props);
		$account_info['CancelledDate'] = self::getCancelledDate($payment_status);
		$account_info['NumLocations'] = self::getNumLocations($locations);
		$account_info['TotalTerminals'] = self::getTotalTerminals($payment_status, $signup);
		$account_info['LicensingFee'] = self::getLicensingFee($payment_status, $signup);
		$account_info['DistroPoints'] = self::getDistroPoints($restaurant);
		$account_info['MonthlyFee'] = self::getMonthlyFee($dataname, $signup, $payment_status);
		$account_info['PromoCode'] = self::getPromoCode($signup);
		$account_info['Modules'] = self::getModules($restaurant);
		$account_info['MoPointsEarned'] = self::getMoPointsEarned($signup);
		$account_info['Stage'] = self::getStage($dataname, $stage, $account_info, $restaurant, $signup, $payment_status);

		return $account_info;
	}

	/**
	 * Determine the value that should be selected in Salesforce for the Stage (aka Stage Name) drop-down for the specified dataname.
	 *
	 * Below are the valid values for SalesforceIntegration to send:
	 *
	 *   Cancelled
	 *   Cancelled - Refund
	 *   Cancelled - Trial
	 *   Closed Won
	 *   Prospecting
	 *   Seasonal
	 *   Trial - CC
	 *   Trial - No CC
	 *   Trial - Reseller
	 *
	 * @param string $dataname dataname
	 * @param string $stage dataname
	 * @param array $account_info dataname
	 *
	 * @access private
	 * @static
	 */
	public static function getStage($dataname, $stage = '', $account_info = false, $restaurant = false, $signup = false, $payment_status = false)
	{
		if ( empty($dataname) ) {
			error_log( __FILE__ ." ". __METHOD__ ."() called with blank dataname" );
			return $stage;
		}

		if ( $restaurant === false ) {
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[dataname]'", array('dataname' => $dataname));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			$restaurant = mysqli_fetch_assoc($result);
		}

		if ( $signup === false ) {
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname` = '[dataname]'", array('dataname' => $dataname));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			$signup = mysqli_fetch_assoc($result);
		}

		if ( $payment_status === false ) {
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_status` WHERE `restaurantid` = '[restaurantid]'", array('restaurantid' => $restaurant['id']));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			$payment_status = mysqli_fetch_assoc($result);
		}

		if ( $account_info === false ) {
			$account_info = self::getAccountInfo($dataname);
		}

		// Use any passed-in value for stage instead of running our series of checks.
		if ( empty($stage) )
		{
			$trial = (empty($payment_status['license_applied']) && !empty($payment_status['trial_start']));

			if ( empty($account_info['CancelledDate']) && empty($account_info['CloseDate']) )
			{
				$stage = 'Prospecting';

				$signup_status = isset($signup['status']) ? $signup['status'] : '';

				if ( $signup_status == 'manual' ) {
					$stage = 'Trial - Reseller';
				}
				else if ( $signup_status == 'new' ) {
					$stage = 'Trial - CC';

					$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_leads` WHERE (`data_name` != '' AND `data_name` = '[dataname]') OR (`signupid` != '' and `signupid` = '[signupid]') ORDER BY `id` DESC", array('dataname' => $dataname, 'signupid' => $signup['id']));
					if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
					$reseller_lead = mysqli_fetch_assoc($result);

					if (isset($reseller_lead['lead_source']) && strpos( strtolower($reseller_lead['lead_source']), 'nocc') !== false) $stage = 'Trial - No CC';
				}
			}
			else if ( empty($account_info['CancelledDate']) && !empty($account_info['CloseDate']) )
			{
				$stage = 'Closed Won';

			}
			else if ( !empty($account_info['CancelledDate']) )
			{
				$stage = 'Cancelled';

				if ( $trial ) {
					$stage = 'Cancelled - Trial';
				}
				else if ( $payment_status['cancel_reason'] == 'My business is seasonal' ) {
					$stage = 'Seasonal';
				}
				else {
					// Check for Refund / Trial
					$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_responses` WHERE `payment_type` = 'Hosting' AND `match_restaurantid` = '[match_restaurantid]' AND `x_type` IN('credit', 'void', 'auth_capture_voided') AND `x_response_code` = '1' AND (`batch_status`in('', 'Y') OR (`batch_status`!='' AND `batch_action` LIKE '%covered by%')) ORDER BY `datetime`", array('match_restaurantid' => $payment_status['restaurantid']));
					if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
					$refund_issued = (mysqli_num_rows($result) > 0);

					if ($refund_issued) $stage = 'Cancelled - Refund';
				}
			}
		}

		return $stage;
	}

	/**
	 * Utility function for removing the timestamp portion of a date in the format of "YYYY-mm-dd hh:mm:ss".
	 *
	 * @param string $some_date Date to remove timestamp from
	 *
	 * @access private
	 * @static
	 */
	private static function removeTimestamp($some_date)
	{
		// Split off the timestamp, if it has one.
		if (strpos($some_date, ' ') !== false) {
			$pieces = explode(' ', $some_date);
			$some_date = $pieces[0];
		}

		return $some_date;
	}

	/**
	 * Utility method that will simply log the passed-in error message.
	 *
	 * @param string $msg Error message
	 *
	 * @access private
	 * @static
	 */
	private static function logError($msg)
	{
		error_log($msg);
	}

	/**
	 * Utility method that will log then email the passed-in error message.
	 *
	 * @param string $msg Error message
	 *
	 * @access private
	 * @static
	 */
	private static function emailError($msg)
	{
		error_log($msg);
		mail("tom@lavu.com", "SalesforceIntegration Error", $msg);
	}

	/**
	 * Utility method that will log then email the passed-in error message and then do a PHP die().
	 *
	 * @param string $msg Error message
	 *
	 * @access private
	 * @static
	 */
	private static function emailErrorAndDie($msg)
	{
		error_log($msg);
		mail("tom@lavu.com", "SalesforceIntegration Error", $msg);
		die($msg . "\n");
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "This should be the same date that License Fee is applied (or the Lavu 88 hosting fee)."  But Josh later said he would like to see the projected close date if they haven't closed yet.
	 *
	 * @param array $signup Associative array of the signups table row
	 * @param array $payment_status Associative array of the payment_status table row
	 * @param array $setup_props Associative array returned by procpay_functions.php's get_setup_props()
	 *
	 * @access private
	 * @static
	 */
	private static function getCloseDate($signup, $payment_status, $setup_props)
	{
		global $o_package_container;
	
		$close_date = '';

		$package_name = $o_package_container->get_printed_name_by_attribute('level', $signup['package']);

		// Distro license
		if ( !empty($payment_status['license_applied']) ) {
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` WHERE `id` = '[id]'", array('id' => $payment_status['license_applied']));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			$reseller_license = mysqli_fetch_assoc($result);
			$close_date = $reseller_license['applied'];
		}
		// Lavu 88 and Free Lavu 88 - Trial signups
		else if ( $package_name == 'Lavu 88' || $package_name == 'Lavu 88 Trial' ) {
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_responses` WHERE `payment_type` IN('Hosting', 'General') AND `match_restaurantid` = '[match_restaurantid]' AND `match_restaurantid` != '' AND `x_type` = 'auth_capture' AND `x_response_code` = '1' AND (`batch_status` IN('', 'Y') OR (`batch_status`!='' AND `batch_action` LIKE '%covered by%')) ORDER BY `datetime`", array('match_restaurantid' => $payment_status['restaurantid']));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			$payment_response = mysqli_fetch_assoc($result);
			if (isset($payment_response['date'])) $close_date = $payment_response['date'];
		}
		// All other signups
		else {
			// Consider the date of their first payment_response of type = license as the close date.
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_responses` WHERE `payment_type` = 'License' AND `match_restaurantid` = '[match_restaurantid]' AND `match_restaurantid` != '' AND `x_type` = 'auth_capture' AND `x_response_code` = '1' AND (`batch_status`in('', 'Y') OR (`batch_status`!='' AND `batch_action` LIKE '%covered by%')) ORDER BY `datetime`", array('match_restaurantid' => $payment_status['restaurantid']));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			$payment_response = mysqli_fetch_assoc($result);
			$close_date = $payment_response['date'];
		}

		return self::removeTimestamp($close_date);
	}

	/**
	 * Whether or not the account has entered a credit card, based on whether they have one or more billing_profiles.
	 *
	 * @param string $dataname Dataname
	 *
	 * @access private
	 * @static
	 */
	private static function getEnteredCC($dataname)
	{
		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_profiles` WHERE `dataname` = '[dataname]'", array('dataname' => $dataname));
		if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());

		return (mysqli_num_rows($result) > 0);
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "Note: the Description field on the Opportunity page should reflect the notes that the client enters into the system when they cancel their account (if any)."
	 *
	 * @param string $dataname Dataname
	 * @param array $signup Associative array of the payment_status table row
	 *
	 * @access private
	 * @static
	 */
	private static function getDescription($dataname, $payment_status)
	{
		$description = '';

		if ( !empty($payment_status['cancel_date']) ) {
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_log` WHERE `dataname` = '[dataname]' AND `action` = 'cancel account' AND DATE(`datetime`) = DATE('[cancel_date]')", array('dataname' => $dataname, 'cancel_date' => $payment_status['cancel_date']));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			$billing_log = mysqli_fetch_assoc($result);
			$description = $billing_log['details'];
		}

		return $description;
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "This should filter in the type of license that the customer signed up for, if you can standardize them to import matching the following license types:
	 * POS - L88
	 * POS - Silver
	 * POS - Gold
	 * POS - Pro
	 * EVO - Silver
	 * Hosp - Standard
	 * Hosp - Executive
	 * Hosp - Presidential
	 * Fit - Silver
	 * Fit - Gold
	 * Fit - Pro
	 * Give"
	 *
	 * @param string $dataname Dataname
	 * @param array $signup Associative array of the signups table row
	 *
	 * @access private
	 * @static
	 */

	private static function getLicenseType($dataname, $signup)
	{
		global $o_package_container;
		global $o_emailAddressesMap;

		$license_type = '';

		$package_name = $o_package_container->get_printed_name_by_attribute('level', $signup['package']);
		switch ( $package_name ) {
			// Accounts at the "Trial - Reseller" stage aren't going to have package names, so we had to add a case for them.
			case '':
				break;
			case 'Lavu 88':
				$license_type = 'POS - L88';
				break;
			case 'Silver':
				$license_type = 'POS - Silver';
				break;
			case 'Gold':
				$license_type = 'POS - Gold';
				break;
			case 'Pro':
			case 'Platinum':
				$license_type = 'POS - Pro';
				break;
			case 'Hospitality':
				$license_type = 'Hosp - Standard';
				break;
			case 'Hospitality Executive':
				$license_type = 'Hosp - Executive';
				break;
			case 'Hospitality Presidential':
				$license_type = 'Hosp - Presidential';
				break;
			case 'Fit Silver':
				$license_type = 'Fit - Silver';
				break;
			case 'Fit Gold':
				$license_type = 'Fit - Gold';
				break;
			case 'Fit Pro':
				$license_type = 'Fit - Pro';
				break;
			case 'Give':
				$license_type = 'Give';
				break;
			case 'Gold Trial':
				$license_type = 'Trial - Gold';
				break;
			case 'Pro Trial':
				$license_type = 'Trial - Pro';
				break;
			case 'Lavu 88 Trial':
				$license_type = 'Trial - L88';
				break;
			case 'Mercury Gold':
			case 'Free Gold Mercury':
				$license_type = 'Free - Gold - Merc';
				break;
			case 'Mercury Pro':
			case 'Free Pro Mercury':
				$license_type = 'Free - Pro - Merc';
				break;
			case 'Mercury Free Lavu 88':
			case 'Free Lavu 88 Mercury':
				$license_type = 'Free - L88 - Merc';
				break;
			case 'EVO Snap Free Lavu 88':
			case 'Free Lavu 88 EVOSnap':
				$license_type = 'Free - L88 - Evo';
				break;
			case 'Lavu':
				$license_type = 'POS - Lavu';
				break;
			case 'Lavu Enterprise':
				$license_type = 'POS - LavuEnterprise';
				break;
			default:
				$license_type = $package_name;
				self::emailError(__FILE__ ." ". __METHOD__ ." couldn't get license type because of unknown package name '{$package_name}' for dataname={$dataname} and stage={$stage}");
				break;
		}

		return $license_type;
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "License Invoice Date"
	 *
	 * @param array $signup Associative array of the signups table row
	 * @param array $setup_props Associative array returned by procpay_functions.php's get_setup_props()
	 *
	 * @access private
	 * @static
	 */
	private static function getLicenseDate($signup, $setup_props)
	{
		// Lavu 88 and Free Lavu 88 Trial don't have licenses, so they shouldn't have a License Date.
		if ( $signup['package'] == '8' || $signup['package'] == '16' )
			return '';
		else
			return $setup_props['license_billed'];
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "Initial Hosting Payment Date"
	 *
	 * @param array $setup_props Associative array returned by procpay_functions.php's get_setup_props()
	 *
	 * @access private
	 * @static
	 */
	private static function getHostingDate($setup_props)
	{
		return $setup_props['first_hosting'];
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "Should reflect the day the customer cancelled service."
	 *
	 * @param array $payment_status Associative array of the payment_status table row
	 *
	 * @access private
	 * @static
	 */
	private static function getCancelledDate($payment_status)
	{
		return self::removeTimestamp($payment_status['cancel_date']);  // TO DO : do a query for cancelled accounts and make sure this field is populated for all of them
	}

	/**
	 * Number of non-deleted locations.
	 *
	 * @param array $locations PHP MySQL results handle for the locations query
	 *
	 * @access private
	 * @static
	 */
	private static function getNumLocations($locations)
	{
		return ($locations === false) ? 0 : mysqli_num_rows($locations);
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "Provide total # of terminals in use"
	 *
	 * @param array $payment_status Associative array of the payment_status table row
	 * @param array $signup Associative array of the signups table row
	 *
	 * @access private
	 * @static
	 */
	private static function getTotalTerminals($payment_status, $signup)
	{	
		global $o_package_container;

		return empty($payment_status['custom_max_ipads']) ? $o_package_container->get_ipad_limt_by_attribute('level', (int)$signup['package']) : $payment_status['custom_max_ipads'];
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "Total License Fee" - Josh stated that he would like to see the to be the amount the customer paid for the license but not if it was purchased with points.
	 *
	 * @param array $payment_status Associative array of the payment_status table row
	 * @param array $signup Associative array of the signups table row
	 *
	 * @access private
	 * @static
	 */
	private static function getLicensingFee($payment_status, $signup)
	{
		global $o_package_container;

		$licensing_fee = '';

		// No package level yet - but we have to return a non-blank value to prevent Salesforce from substituting weird values.
		if ( $signup['package'] === 'none' || empty($signup['package']) ) {
			$licensing_fee = 0.00;
		}
		// Distro license
		else if ( !empty($payment_status['license_applied']) ) {
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` WHERE `id` = '[id]'", array('id' => $payment_status['license_applied']));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			$reseller_license = mysqli_fetch_assoc($result);
			if (strpos($reseller_license['notes'], 'PURCHASED WITH POINTS') === false) $licensing_fee = floatval($reseller_license['cost']);

			// Upgrade license
			if ( !empty($payment_status['extra_upgrades_applied']) ) {
				$upgrade_result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` WHERE `id` = '[id]'", array('id' => $payment_status['extra_upgrades_applied']));
				if ( $upgrade_result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
				$reseller_upgrade_license = mysqli_fetch_assoc($upgrade_result);
				if (strpos($reseller_upgrade_license['notes'], 'PURCHASED WITH POINTS') === false) $licensing_fee += floatval($reseller_upgrade_license['cost']);
			}
		}
		else {
			// Loop over non-reseller license payments because they might have paid for an upgrade and we'd like to tally that, as well.
			$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_responses` WHERE `payment_type` = 'License' AND `match_restaurantid` = '[match_restaurantid]' AND `match_restaurantid` != '' AND `x_type` = 'auth_capture' AND `x_response_code` = '1' AND (`batch_status`in('', 'Y') OR (`batch_status`!='' AND `batch_action` LIKE '%covered by%')) ORDER BY `datetime`", array('match_restaurantid' => $payment_status['restaurantid']));
			if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
			while ( $payment_response = mysqli_fetch_assoc($result) ) {
				$licensing_fee += floatval($payment_response['x_amount']);
			}
		}

		return $licensing_fee;
	}	

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "If points were used to purchase a license, display the total number of points used (i.e. 1500 points = 1 gold license)."
	 *
	 * @param array $signup Associative array of the signups table row
	 *
	 * @access private
	 * @static
	 */
	private static function getDistroPoints($restaurant)
	{
		$distro_points = '';

		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` WHERE `restaurantid` = '[restaurantid]' AND `notes` LIKE '%PURCHASED WITH POINTS%'", array('restaurantid' => $restaurant['id']));
		if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
		while ( $reseller_license = mysqli_fetch_assoc($result) ) {
			$distro_points += intval($reseller_license['value']);
		}

		return $distro_points;
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "Total Monthly Hosting Fee (including modules, additional terminals, etc.)"
	 *
	 * @param array $signup Associative array of the signups table row
	 *
	 * @access private
	 * @static
	 */
	private static function getMonthlyFee($dataname, $signup, $payment_status)
	{
		global $o_package_container;

		$monthly_fee = '';

		$package_obj = $o_package_container->get_package_by_attribute('level', $signup['package']);
		if ( isset($package_obj) ) {
			$monthly_fee = $package_obj->get_recurring();
		}

		$result = mlavu_query( "SELECT * FROM `poslavu_MAIN_db`.`monthly_parts` WHERE `dataname` = '[dataname]' AND `mp_start_day` <= '[current_date]' AND (`mp_end_day` = '' OR `mp_end_day` > '[current_date]') ORDER BY `id`", array('dataname' => $dataname, 'current_date' => date('Y-m-d') ) );
		if ( $result === false ) self::emailErrorAndDie(__FILE__ ." ". __METHOD__ ." encountered MySQL error: ". mlavu_dberror());
		while ( $monthly_part = mysqli_fetch_assoc($result) ) {
			$monthly_fee += floatval($monthly_part['amount']);
		}

		// This should override anything else.
		if ( !empty($payment_status['custom_monthly_hosting']) ) {
			$monthly_fee = floatval($payment_status['custom_monthly_hosting']);
		}

		return $monthly_fee;
	}
	
	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "Promo from Sign up form or from Mercury?"
	 *
	 * @param array $signup Associative array of the signups table row
	 *
	 * @access private
	 * @static
	 */
	private static function getPromoCode($signup)
	{
		return $signup['promo'];
	}

	/**
	 * Listing of modules for that account, taken directly from the database value.
	 *
	 * @param array $restaurant Associative array of the restaurants table row
	 *
	 * @access private
	 * @static
	 */
	private static function getModules($restaurant)
	{
		return $restaurant['modules'];
	}

	/**
	 * From "CP to Salesforce Integration - Customer" Google doc (https://docs.google.com/a/lavu.com/document/d/1R3r-XSrxxSr1Anyto6VOHB5bkhGGgWxDucg2uLBsngI/edit?usp=sharing):
	 *
	 * "Should reflect the number of points earned per month (i.e. 40 points = Lavu 88 or Gold) - Note: this should not be an aggregate total."
	 *
	 * @param array $signup Associative array of the signups table row
	 *
	 * @access private
	 * @static
	 */
	private static function getMoPointsEarned($signup)
	{
		global $o_package_container;

		$mo_points_earned = '';

		$package_obj = $o_package_container->get_package_by_attribute('level', $signup['package']);
		if ( isset($package_obj) ) {
			$mo_points_earned = $package_obj->get_recurring_points();
		}

		return $mo_points_earned;
	}

}
