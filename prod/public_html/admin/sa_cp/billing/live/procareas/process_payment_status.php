<?php
	function getPipeThenColonDelimFields($delim_str) {
		foreach ( explode('|', $delim_str) as $keyval )
		{
			list($key, $val) = explode(': ', $keyval, 2);
			$fields[$key] = $val;
		}
		return $fields;
	}

	// Get existing due_info string so we can break it apart into its component pieces in an array
	$due_info = $status_read['due_info'];

	// Fix inconsistency with the one label in due_info that didn't end with a colon and remove dollar sign and comma characters so number_format() will work
	$due_info = str_replace('Due since ', 'Due since: ', $due_info);
	$due_info = str_replace(array('$', ','), '', $due_info);

	// Put pipe-delimited due_info components into an array
	$due_info_components = getPipeThenColonDelimFields($due_info);

	// A pipe character in front of Total means we will have bill_type_due array elements, so get them
	$bill_types_due = array();
	if ( strpos($due_info, '|Total:') !== false )
	{
		list($bill_types_due_str, $due_info_after_total) = explode('|Total:', $due_info);
		$bill_types_due = getPipeThenColonDelimFields($bill_types_due_str);
	}

	// Put due_info components used for duestr + output into vars so they can be initialized
	$all_bills_due        = isset($due_info_components['Total']) ? $due_info_components['Total'] : 0.00;
	$earliest_past_due    = isset($due_info_components['Due since']) ? $due_info_components['Due since'] : '';
	$paid_last_two_weeks  = isset($due_info_components['Paid last 15']) ? $due_info_components['Paid last 15'] : 0.00;
	$paid_last_four_weeks = isset($due_info_components['Paid last 31']) ? $due_info_components['Paid last 31'] : 0.00;
	$last_payment_made    = isset($due_info_components['Last Payment Made']) ? $due_info_components['Last Payment Made'] : '';
	$other_payments_total = isset($due_info_components['Unapplied Payments']) ? $due_info_components['Unapplied Payments'] : '';

	$duestr = "";
	foreach($bill_types_due as $key => $val)
	{
		$duestr .= "<span class='info_label'>Due for " . $key . ":</span> <span class='info_value'>$" . number_format($val,2)."</span> &nbsp;&nbsp;";
	}
	$duestr .= "<span class='info_label'>Total Due:</span> <span class='info_value'>$" . number_format($all_bills_due,2) ."</span>";
	$output = "&nbsp;<br>".$duestr."<br>".$output."<br>".$duestr."<br><br>";
