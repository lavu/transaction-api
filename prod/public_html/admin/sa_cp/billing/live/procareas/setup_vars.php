<?php
	if(!isset($p_area))
	{
		exit();
	}

	require_once(dirname(__FILE__)."/../../package_levels_object.php");
	require_once(dirname(__FILE__)."/../../referal_functions.php");

	// check for referal updates, first
	redeem_referral_code_reset_arb($dataname, date("Y-m-d", strtotime("+1 days")));

	if (!isset($o_package_container))
		$o_package_container = new package_container();

	$lavu_admin = false;
	if ($p_area == "billing_info") $lavu_admin = true;

	$output = "";

	$collection_mode = "";
	$min_due_date = "";
	$custom_monthly_hosting = "";
	$auto_waive_until = "";
	$custom_max_ipads = "";
	$tablesideMaxIpads = "";
	$custom_max_ipods = "";
	$custom_max_kds = "";
	$custom_max_kiosk = "";
	$force_allow_tabs_n_tables = "0";
	$bill_types_due = "";
	$all_bills_due = "";
	$set_due_info = "";
	$custom_payment_plan = "";
	$annual_agreement = "";
	$f_last_monthly_hosting_cost = 0;
	$ltg_status_mode = 0;

	$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
	if(mysqli_num_rows($rest_query))
	{
		$rest_read = mysqli_fetch_assoc($rest_query);
		$rest_id = $rest_read['id'];
		$rest_created = substr($rest_read['created'],0,10);

		if($package=="")
		{
			$fps_info = find_package_status($rest_id);
			$package = $fps_info['package'];
			$signup_status = $fps_info['signup_status'];
			//echo $fps_info['package'] . " - " . $fps_info['signup_status'] . "<br>";
		}

		$license_applied = 0;
		$license_value = 0;
		$license_date_paid = "";
		$status_record_exists = false;
		$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
		if(mysqli_num_rows($status_query))
		{
			$status_read = mysqli_fetch_assoc($status_query);
			$collection_mode = $status_read['collection_mode'];
			$min_due_date = $status_read['min_due_date'];
			$custom_monthly_hosting = $status_read['custom_monthly_hosting'];
			$auto_waive_until = $status_read['auto_waive_until'];
			$custom_max_ipads = $status_read['custom_max_ipads'];
			$tablesideMaxIpads = $status_read['tableside_max_ipads'];
			$custom_max_ipods = $status_read['custom_max_ipods'];
			$custom_max_kds = $status_read['custom_max_kds'];
			$custom_max_kiosk = $status_read['custom_max_kiosk'];
			$force_allow_tabs_n_tables = $status_read['force_allow_tabs_n_tables'];
			$license_applied = $status_read['license_applied'];
			$extra_upgrades_applied = '';
			$extra_upgrades_applied = $status_read['extra_upgrades_applied'];
			$custom_payment_plan = $status_read['custom_payment_plan'];
			$annual_agreement = $status_read['annual_agreement'];
			$ltg_status_mode = $status_read['lavutogo_payment'];
			$status_record_exists = true;
			//$output .= "LICFOUND!<br>";

			// get the main distro license
			if($license_applied!="" && is_numeric($license_applied))
			{
				$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `id`='[1]'",$license_applied);
				if(mysqli_num_rows($lic_query))
				{
					$lic_read = mysqli_fetch_assoc($lic_query);
					$new_package = $o_package_container->get_level_by_attribute('name', $lic_read['type']);
					if ($new_package !== 0 && $o_package_container->compare_package_by_level((int)$new_package, (int)$package) == 1)
						$package = $new_package;
					$license_value = $lic_read['value'];
					$license_date_paid = $lic_read['purchased'];
				}
			}

			// get other distro upgrades that have been applied
			$a_license_values = array();
			$a_license_dates = array();
			if(strlen($extra_upgrades_applied) > 0) {
				$a_extra_distro_upgrades = explode("|", $extra_upgrades_applied);
				$a_license_values[] = $license_value;
				$a_license_dates[] = $license_date_paid;
				foreach($a_extra_distro_upgrades as $s_upgrade_id) {
					$upgrade_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `id`='[1]'",$s_upgrade_id);
					if(mysqli_num_rows($upgrade_query))
					{
						$a_upgrade_read = mysqli_fetch_assoc($upgrade_query);
						$a_upgrade_names = $o_package_container->get_upgrade_old_new_names($a_upgrade_read['type']);
						$s_upgrade_to = $a_upgrade_names[1];
						$new_package = $o_package_container->get_level_by_attribute('name', $s_upgrade_to);
						if ($new_package !== 0 && $o_package_container->compare_package_by_level((int)$new_package, (int)$package) == 1)
							$package = $new_package;
						$a_license_values[] = $a_upgrade_read['value'];
						$a_license_dates[] = $a_upgrade_read['purchased'];
					}
				}
			}
		}
		
		// record package_info as system_setting in location's config table for device limits enforcement
		$sysset_package_name = $o_package_container->get_printed_name_by_attribute('level', $package);
		$sysset_ipod_limit = max($custom_max_ipods, $o_package_container->get_ipod_limt_by_attribute('level', $package));
		$sysset_ipad_limit = max($custom_max_ipads, $o_package_container->get_ipad_limt_by_attribute('level', $package));
		$syssetTablesideIpadLimit = max($tablesideMaxIpads, $o_package_container->getTablesideIpadLimtByAttribute('level', $package));

		// give lee-way for trial accounts
		if ($sysset_ipod_limit < 1) $sysset_ipod_limit = 20;
		if ($sysset_ipad_limit < 1) $sysset_ipad_limit = 20;
		if ($syssetTablesideIpadLimit < 1) $syssetTablesideIpadLimit = 20;
		
	}
	else
	{
		return;
	}

	$new_lic_amount = $o_package_container->get_value_by_attribute('level', $package);
	if ($new_lic_amount != 0) $lic_amount = $new_lic_amount;
	$new_mon_amount = $o_package_container->get_recurring_by_attribute('level', $package);
	if ($new_mon_amount !== 0) $mon_amount = $new_mon_amount;
	/*if($package==1)
	{
		$lic_amount = 895;
		$mon_amount = 29.95;
	}
	else if($package==2)
	{
		$lic_amount = 1495;
		$mon_amount = 49.95;
	}
	else if($package==3)
	{
		$lic_amount = 3495;
		$mon_amount = 99.95;
	}
	else if($package==4)
	{
		$lic_amount = 0;
		$mon_amount = 39.00;
	}*/

	// Discounted Licenses w/Special Licenses - it is possible for Lavu to grant distros the ability to buy discounted licenses so we need
	// to adjust the cost of the License invoice accordingly to prevent it from being a manual process that happens after-the-fact.

	// Bug #3168: Needed to create discounted license invoices for Distro special licenses.
	if ( $license_applied != "" && is_numeric($license_applied) && $license_value !== 0) {
	        $lic_amount = $license_value;
	}

	// Discounted Licenses w/Promo Codes - it is sometimes necessary to adjust license cost due to a license discount from a promo code for companies
	// like Mercury, BlueStar, and Staples. (Note: staples_lavu88 promotion also has code in procpay_function.php's get_setup_props())

	global $signup_read;
	if ( !isset($signup_read) ) {
		$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'", $dataname);
		if ($signup_query !== false) $signup_read = mysqli_fetch_assoc($signup_query);
	}

	if ( !empty($signup_read['promo']) ) {
		require_once( dirname( __FILE__ ) .'/../../../../distro/beta/promo_codes.php' );

		$promo_code = $signup_read['promo'];
		$license_name = strtolower( $o_package_container->get_printed_name_by_attribute('level', $package) );
		$license_discount = 0.00;
		$check_unclaimed_codes_only = false;
		$promo_code_valid = check_promo_code($promo_code, $license_name, $license_discount, $check_unclaimed_codes_only);

		// Adjust the license cost if the code checks out.
		if ( $promo_code_valid && $license_discount > 0.00 ) {
			// Percentage discount
			if ( floatval($license_discount) < 1.00 ) {
				$lic_amount = floatval($lic_amount) - (floatval($lic_amount) * $license_discount);
			}
			// Flat discount amount
			else if ( floatval($license_discount) >= 1.00 ) {
				$lic_amount = floatval($lic_amount) - floatval($license_discount);
			}
		}

		//error_log( "lic_amount={$lic_amount} license_name={$license_name} license_discount={$license_discount} promo_code_valid={$promo_code_valid}");  //debug
	}

?>