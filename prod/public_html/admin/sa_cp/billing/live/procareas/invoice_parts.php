<?php

	require_once(dirname(__FILE__)."/../../procareas/monthly_parts.php");
	require_once(dirname(__FILE__)."/../../../../cp/resources/json.php");

	define('UNKNOWN', 0);
	define('DISTRIBUTOR', 1);
	define('USER', 2);
	define('LAVUADMIN', 3);

	class InvoiceParts {

		// creates an invoice part
		// @$i_part_id: if > 0, loads the row from `poslavu_MAIN_db`.`invoice_parts` to fill this object with
		// @$a_db_row: if $i_part_id == 0 and not NULL, uses this row data to fill this object with
		function __construct($i_part_id = 0, $a_db_row = NULL) {

			// create the history
			$o_history = self::create_standard_history();

			$this->dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

			// create the object
			$this->db_values = new stdClass();
			$db = $this->db_values;
			$db->id = 0;
			$db->invoice_id = 0;
			$db->monthly_parts_id = 0;
			$db->dataname = "";
			$db->description = "";
			$db->due_date = "";
			$db->amount = 0.0;
			$db->pro_rate = FALSE;
			$db->waived = FALSE;
			$db->promo = FALSE;
			$db->bundled = FALSE;
			$db->standard_monthly_billing = FALSE;
			$db->json_history = LavuJson::json_encode($o_history);
			$db->distro_points = 0;
			$db->distro_cmsn = 0.0;
			$db->package = "";
			$db->salesforce_quote_id = "";
			$db->salesforce_quotelineitem_id = "";

			// fill in the object
			if ($i_part_id > 0) {

				// load from the database
				$a_db_rows = $this->dbapi->getAllInTable('invoice_parts', array('id'=>$i_part_id), TRUE);
				$a_db_row = array();
				if (count($a_db_rows) > 0)
					$a_db_row = $a_db_rows[0];
			}
			if (is_array($a_db_row)) {

				// load from the array
				foreach($a_db_row as $k=>$v)
					if (isset($this->db_values->$k))
						$this->db_values->$k = $v;
			}

			// enforce some types upon some values
			$this->enforce_types();
		}

		// Supposed to be used to save after creation.
		public function save_to_db() {
			$a_update_vars = (array)$this->db_values;
			unset($a_update_vars['id']);
			$a_where_vars = array('dataname'=>$this->db_values->dataname, 'description'=>$this->db_values->description, 'due_date'=>$this->db_values->due_date);
			$this->dbapi->insertOrUpdate('poslavu_MAIN_db', 'invoice_parts', $a_update_vars, $a_where_vars);
		}

		// enforces the integer type upon some fields
		private function enforce_types() {
			foreach(array('id','invoice_id','monthly_parts_id','pro_rate','waived','promo','bundled','standard_monthly_billing','distro_points','distro_cmsn') as $s_name)
				$this->db_values->$s_name = (int)$this->db_values->$s_name;
			foreach(array('amount') as $s_name)
				$this->db_values->$s_name = (double)$this->db_values->$s_name;
		}

		// get boolean values
		// returns TRUE if the value exists and !== 0, FALSE otherwise
		public function is($s_name) {
			if (isset($this->db_values->$s_name) && $this->db_values->$s_name !== 0)
				return TRUE;
			return FALSE;
		}

		// updates an invoice_part
		// if it exists in the database, updates it there
		// if it applies to a invoice, it updates the cost of the invoice based on its constituent parts
		// @$a_db_values: an array of column/value pairs, eg array('amount'=>'49.00')
		// @$b_update_invoice: if false, won't update the invoice
		// @$s_details: if null no reason is provided, otherwise it is treated as a string
		// @return: 'data_updated' (for just this object), 'invoice_part_updated' (for this object and the database), or 'invoice_part_and_invoice_updated' (for this object, the database, and the invoice)
		public function update($a_db_values, $b_update_invoice = TRUE, $s_details = NULL) {

			// copy the values and update the history
			$a_history_updates = array();
			$i = 1;
			foreach($a_db_values as $s_key=>$wt_value) {
				if (!isset($this->db_values->$s_key))
					continue;
				$a_history_update = array('field'=>$s_key, 'prev'=>$this->db_values->$s_key, 'new'=>$wt_value);
				$a_history_updates["update_{$i}"] = $a_history_update;
				$this->db_values->$s_key = $wt_value;
				$i++;
			}
			$a_reason = $s_details === NULL ? array() : array('bc_details'=>$s_details);
			$a_history_updates = array_merge($a_history_updates, $a_reason);
			$a_updates = array(
				'action'=>'changed_values',
				'details'=>$a_history_updates
				);
			self::update_history($a_updates, $this->db_values->json_history);

			// enforce types
			$this->enforce_types();
			if ($this->db_values->id <= 0)
				return 'data_updated';

			// save to database
			$a_db_values = (array)$this->db_values;
			$s_update_clause = $this->dbapi->arrayToUpdateClause($a_db_values);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`invoice_parts` {$s_update_clause} WHERE `id`='[id]'", $a_db_values);
			if ($this->db_values->invoice_id <= 0 || !$b_update_invoice)
				return 'invoice_part_updated';

			// update invoice
			MonthlyInvoiceParts::UpdateInvoiceByParts($this->db_values->invoice_id);
			return 'invoice_part_and_invoice_updated';
		}

		// loads all invoice parts that match the given invoice id and creates a InvoiceParts object for them
		// @$i_invoice_id: id of the invoice to load parts for
		// @return: an array(InvoiceParts 1, InvoiceParts 2, ...)
		public static function getInvoicePartsForInvoice($i_invoice_id) {

			// load the parts from the database
			$a_parts = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('invoice_parts', array('invoice_id'=>$i_invoice_id), TRUE);
			if (count($a_parts) == 0)
				return array();

			// create an InvoiceParts for each row in the db
			$a_retval = array();
			foreach($a_parts as $a_row) {
				$a_retval[] = new InvoiceParts(0, $a_row);
			}
			return $a_retval;
		}

		// used by billing/index.php to make edits and updates to invoice parts
		// does a http redirect if there is an edit
		// @$ip_mp_output: if there is anything to be echo'd, it will be appended to this variable
		// @return: FALSE if there is nothing to edit or the user doesn't have accesses, TRUE if an edit is performed
		public static function check_edit(&$ip_mp_output) {

			// check if there's an edit and check user permissions
			if (!isset($_GET['edit_invoice_parts']) || !isset($_GET['action']) || !isset($_GET['dn']))
				return FALSE;
			if (!function_exists('can_access') || !can_access('technical')) {
				echo "<b>You don't have access to edit invoice parts.</b>";
				return FALSE;
			}

			// edit the invoice part
			$b_edit = FALSE;
			if ($_GET['action'] = 'create_new' && isset($_GET['invoice_id'])) {
				$a_invoices = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('invoices', array('id'=>$_GET['invoice_id']), TRUE, array('selectclause'=>'`due_date`'));
				$a_db_values = array('invoice_id'=>$_GET['invoice_id'], 'dataname'=>$_GET['dn'], 'due_date'=>$a_invoices[0]['due_date']);
				$o_invoice_part = new InvoiceParts(0, $a_db_values);
				$o_invoice_part->save_to_db();
				$b_edit = TRUE;
			} else if ($_GET['action']= 'change_invoice_part' && isset($_GET['part_id']) && isset($_GET['name']) && isset($_GET['value'])) {
				$o_invoice_part = new InvoiceParts($_GET['part_id'], NULL);
				$a_update_vars = array($_GET['name']=>$_GET['value']);
				if ($_GET['name'] == 'invoice_id') $s_old_invoice_id = $o_invoice_part->db_values->invoice_id;
				$o_invoice_part->update($a_update_vars, TRUE, (isset($_REQUEST['bc_details']) ? $_REQUEST['bc_details'] : ''));
				$b_edit = TRUE;
			}

			// check for pro rating
			// if there is pro rating, reduce the amount based on the amount of time the feature was used for
			$db = $o_invoice_part->db_values;
			$mp = NULL;
			if ((int)$db->monthly_parts_id > 0) {
				$mp = new MonthlyInvoiceParts($db->monthly_parts_id);
				$mp = $mp->db_values;
			}
			if ($mp !== NULL && (int)$db->pro_rate == 1 && $_GET['name'] == 'pro_rate' && strtotime($mp->mp_start_day) > strtotime($db->due_date." -1 month")) {
				$i_start_date = strtotime($mp->mp_start_day);
				$date = strtotime($db->due_date);
				$i_pro_rate_days = 0;
				while ($i_start_date < $date && $i_pro_rate_days < 22) {
					$i_start_date = strtotime(date("Y-m-d", $date)." +1 days");
					$i_pro_rate_days++;
				}
				$o_invoice_part->db_values->amount = InvoiceParts::pro_rate((int)$mp->amount, $i_pro_rate_days);
				$o_invoice_part->save_to_db();
			} else if ($mp !== NULL && (int)$db->pro_rate == 0 && $_GET['name'] == 'pro_rate') {
				$o_invoice_part->db_values->amount = $mp->amount;
				$o_invoice_part->save_to_db();
			}

			// update the invoice amount
			if (intval($db->invoice_id) !== -1) MonthlyInvoiceParts::UpdateInvoiceByParts($db->invoice_id);
			if (!empty($s_old_invoice_id)) MonthlyInvoiceParts::UpdateInvoiceByParts($s_old_invoice_id);

			// redirect
			if ($b_edit)
				header("Location: ?dn=".$_GET['dn']);

			return $b_edit;
		}

		// get the amount that should be charged to the customer based on
		//     the number of days the feature has been used for
		// 0.25 for 1-7 days, 0.50 for 8-14 days, 0.75 for 15-21 days, full for 22+ days
		// @$i_monthly_part_amount: the full cost of the feature, assuming no pro rate
		// @$i_pro_rate_days: the number of days that the feature has been used for
		public static function pro_rate($i_monthly_part_amount, $i_pro_rate_days) {
			if ($i_pro_rate_days < 8)
				return $i_monthly_part_amount*1/4;
			if ($i_pro_rate_days < 15)
				return $i_monthly_part_amount*2/4;
			if ($i_pro_rate_days < 22)
				return $i_monthly_part_amount*3/4;
			return $i_monthly_part_amount;
		}

		// creates a new invoice part with some properties of a monthly part template
		// and checks for pro_rating and sets some other invoice part properties
		// @$db: an object containing values from a monthly_part->db_values
		// @$i_today: the date of the invoice for this invoice part
		// @$s_date: same as $i_today, but in "Y-m-d" format
		// @$s_dataname: the dataname to apply the invoice part to
		// @$i_invoice_id: the invoice id to apply the invoice part to
		// @$i_waived: 1 or 0
		// @$s_creation_details: the details of why it was created
		// @return: an InvoiceParts object with all of its properties set
		public static function createNewInvoicePartWithProperties($db, $i_today, $s_date, $s_dataname, $i_invoice_id, $i_waived, $s_creation_details = "", $s_package) {

			// create the desired invoice part
			// and copy matching values from $this->db_values to the invoice part
			$o_invoice_part = new InvoiceParts();
			foreach($db as $s_key=>$wt_value) {
				if (!isset($o_invoice_part->db_values->$s_key))
					continue;
				if (strpos($s_key, 'mp_') === 0)
					continue;
				$o_invoice_part->db_values->$s_key = $wt_value;
			}

			// check for pro rating
			// if there is pro rating
			//     find the extra charge based on the amount of time the feature was used for
			//     and add it to the initial amount
			if ((int)$db->pro_rate == 1 && strtotime($db->mp_start_day) > strtotime(date("Y-m-d", $i_today)." -1 month")) {
				$i_start_date = strtotime($db->mp_start_day);
				$date = $i_today;
				$i_pro_rate_days = 0;
				while ($i_start_date < $date && $i_pro_rate_days < 22) {
					$i_start_date = strtotime(date("Y-m-d", $date)." +1 days");
					$i_pro_rate_days++;
				}
				$o_invoice_part->db_values->amount += self::pro_rate((int)$db->amount, $i_pro_rate_days);
			}

			// set special values on the invoice part
			$o_invoice_part->db_values->id = 0;
			$o_invoice_part->db_values->monthly_parts_id = $db->id;
			$o_invoice_part->db_values->due_date = $s_date;
			$o_invoice_part->db_values->dataname = $s_dataname;
			$o_invoice_part->db_values->invoice_id = $i_invoice_id;
			$o_invoice_part->db_values->waived = $i_waived;
			$o_invoice_part->db_values->package = $s_package;

			$o_history = LavuJson::json_decode($o_invoice_part->db_values->json_history);
			foreach($o_history as $s_date=>$o_history_part) {
				if ($o_history_part->action == "created") {
					$o_history_part->details = $s_creation_details;
					break;
				}
			}
			$o_invoice_part->db_values->json_history = LavuJson::json_encode($o_history);

			return $o_invoice_part;
		}

		/********************************************************************************
		 * History object functions
		 ********************************************************************************/

		// create a standard history object
		public static function create_standard_history($s_action = 'created', $s_details = '') {
			$o_history = new stdClass();
			$s_datetime = date("Y-m-d H:i:s");
			$o_userstats = self::get_user_stats();
			$o_history->$s_datetime = new stdClass();
			$o_history->$s_datetime->action = $s_action;
			$o_history->$s_datetime->details = $s_details;
			$o_history->$s_datetime = (object) array_merge((array)$o_history->$s_datetime, (array)$o_userstats);

			return $o_history;
		}

		// get the status of the user, for the history
		public static function get_user_stats() {

			global $loggedin;
			global $lavu_admin;

			// create the object
			$o_retval = new stdClass();
			$o_retval->username = "";
			$o_retval->usertype = UNKNOWN;

			// set it's values
			if (isset($loggedin)) {
				$o_retval->username = $loggedin;
				$o_retval->usertype = USER;
			}
			if (isset($lavu_admin)) {
				$o_retval->usertype = LAVUADMIN;
			}

			return $o_retval;
		}

		// update the history of the invoice
		// @$a_update: an array representing the update, in the form:
		//     array('action'=>string, 'details'=>string)
		// @$s_json_history: the json string of the history, also used as the return value
		// @return: TRUE on success or FALSE on failure
		public static function update_history(&$a_update, &$s_json_history) {

			// check the input
			if (!is_array($a_update) || !isset($a_update['action']) || !isset($a_update['details']))
				return FALSE;
			if (!is_string($s_json_history))
				return FALSE;

			// get some variables
			$o_history = LavuJson::json_decode($s_json_history, FALSE);
			$o_new_history = self::create_standard_history($a_update['action'], $a_update['details']);

			// update the history
			$o_history = (object) array_merge((array)$o_history, (array)$o_new_history);

			// return the new history
			$s_json_history = LavuJson::json_encode($o_history);
			return TRUE;
		}
	}

?>
