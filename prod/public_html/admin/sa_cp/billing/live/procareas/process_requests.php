<?php
	if(!isset($p_area))
	{
		exit();
	}

	require_once(dirname(__FILE__)."/../../procareas/monthly_parts.php");
	require_once(dirname(__FILE__)."/../../procareas/procpay_functions.php");
	require_once(dirname(__FILE__)."/../../../../cp/objects/deviceLimitsObject.php");
	require_once(dirname(__FILE__)."/../../../../cp/resources/core_functions.php");
	require_once(dirname(__FILE__)."/../../../../../inc/billing/LavuAccountUtils.php");
	require_once(__DIR__."/../../../../../inc/billing/zuora/ZuoraIntegration.php");

	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";

	if(!function_exists("get_first_invoices"))
	{
		// gets the first invoice of each type
		function get_first_invoices($dataname) {
			global $maindb;
			$a_retval = array();

			$first_invoices_query = mlavu_query("SELECT * FROM `[maindb]`.`invoices` WHERE `dataname`='[dataname]' ORDER BY `id` ASC", array("maindb"=>$maindb, "dataname"=>$dataname));
			if (!$first_invoices_query)
				return array();
			if (mysqli_num_rows($first_invoices_query) < 1)
				return array();
			while ($a_invoice = mysqli_fetch_assoc($first_invoices_query))
				if (!isset($a_retval[$a_invoice['type']]))
					$a_retval[$a_invoice['type']] = $a_invoice['due_date'];

			return $a_retval;
		}

		// used explicitly in this file
		function get_days_between_created_and_day($s_due_date, $s_created) {
			$t_due_date = strtotime($s_due_date);
			if (strpos($s_created, ' ')) {
				$s_created_parts = explode(' ', $s_created);
				$s_created = $s_created_parts[0];
			}
			$t_created = strtotime($s_created);
			$i_days = ($t_due_date-$t_created)/(60*60*24);
			// round
			$f_days = $i_days - floor($i_days);
			if ($f_days != 0.0) {
				if ($f_days >= 0.5)
					$i_days = ceil($i_days);
				else
					$i_days = floor($i_days);
			}
			return $i_days;
		}

		// change the custom payment amount
		function change_custom_payment_amount($i_payment_id, $s_bc_reason, $i_new_amount) {
			global $maindb;
			global $loggedin;
			global $loggedin_fullname;

			if (!function_exists('arrayToInsertClause')) {
				function arrayToInsertClause($indexed_array) {
					$a_inserts = array();
					$a_values = array();
					foreach($indexed_array as $k=>$v) {
						$nk = ConnectionHub::getConn('poslavu')->escapeString($k);
						$a_inserts[] = '`'.$nk.'`';
						$a_values[] = '\'['.$nk.']\'';
					}
					return '('.implode(',',$a_inserts).') VALUES ('.implode(',',$a_values).')';
				}
			}

			if ($i_payment_id < 0)
				{ error_log("a"); return; };

			$payment_query = mlavu_query("SELECT `x_type`,`match_restaurantid`,`x_amount` FROM `[maindb]`.`payment_responses` WHERE `id`='[id]'",
				array('maindb'=>$maindb, 'id'=>$i_payment_id));
			if ($payment_query === FALSE) {
				error_log('bad payment response query in '.__FILE__);
				return;
			}
			if (mysqli_num_rows($payment_query) == 0)
				{ error_log("b"); return; };
			$a_payment = mysqli_fetch_assoc($payment_query);

			if (strtolower($a_payment['x_type']) != 'misc')
				{ error_log("c"); return; };
			error_log('id: '.$i_payment_id.' restaurant id: '.$a_payment['match_restaurantid']);
			$restaurant_query = mlavu_query("SELECT * FROM `[maindb]`.`restaurants` WHERE `id`='[id]'",
				array('maindb'=>$maindb, 'id'=>$a_payment['match_restaurantid']));
			mlavu_query("UPDATE `[maindb]`.`payment_responses` SET `x_amount`='[x_amount]' WHERE `id`='[id]'",
				array('maindb'=>$maindb, 'x_amount'=>$i_new_amount, 'id'=>$i_payment_id));
			error_log('rows affected: '.ConnectionHub::getConn('poslavu')->affectedRows());
			if ($restaurant_query === FALSE || mysqli_num_rows($restaurant_query) == 0)
				{ error_log("d"); return; };
			$a_restaurant = mysqli_fetch_assoc($restaurant_query);

			$insert_vars = array(
				'datetime'=>date('Y-m-d H:i:s'),
				'dataname'=>$a_restaurant['data_name'],
				'restaurantid'=>$a_restaurant['id'],
				'username'=>$loggedin,
				'fullname'=>$loggedin_fullname,
				'ipaddress'=>$_SERVER['SERVER_ADDR'],
				'action'=>'Change Custom Payment',
				'details'=>$s_bc_reason.' (old value "'.$a_payment['x_amount'].'" new value "'.$i_new_amount.'")',
				'paymentid'=>$i_payment_id);
			$insert_clause = arrayToInsertClause($insert_vars);
			$insert_string = "INSERT INTO `[maindb]`.`billing_log` $insert_clause";
			$insert_vars = array_merge(array('maindb'=>$maindb), $insert_vars);
			mlavu_query($insert_string, $insert_vars);
		}
	}

	// When changing the package or the device limits, the HTML elements for the POS package_info render
	// before the actual update is performed, so we need to update the already-rendered entities.
	function updatePackageAndDeviceLimitInUI($package_info) {
		$package_name = empty($package_info['package_name']) ? '' : htmlspecialchars($package_info['package_name']);
		$ipad_limit = empty($package_info['ipad_limit']) ? '' : htmlspecialchars($package_info['ipad_limit']);
		$ipod_limit = empty($package_info['ipod_limit']) ? '' : htmlspecialchars($package_info['ipod_limit']);
		$tableside_limit = empty($package_info['tableside_limit']) ? '' : htmlspecialchars($package_info['tableside_limit']);

		echo <<<HTML
			<script>
			var pos_package = document.getElementById('pos_package');
			var pos_ipads   = document.getElementById('pos_ipads');
			var pos_ipods   = document.getElementById('pos_ipods');
			var pos_tableside   = document.getElementById('pos_tableside');

			if (pos_package     != null) pos_package.innerHTML = "{$package_name}";
			if (pos_ipads       != null) pos_ipads.innerHTML   = "{$ipad_limit}";
			if (pos_ipods       != null) pos_ipods.innerHTML   = "{$ipod_limit}";
			if (pos_tableside   != null) pos_tableside.innerHTML   = "{$tableside_limit}";
			</script>
HTML;
	}

	$changed_package_or_device_limits = false;

	if (isset($_REQUEST['set_ltg_status'])) {
		$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `lavutogo_payment` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_ltg_status'], $rest_read['id']);
		if ($update) {
			$ltg_status_mode = $_REQUEST['set_ltg_status'];
		}

		require_once(dirname(__FILE__)."/../../../../../inc/olo/config.php");
		$ltgStatusQuery = mlavu_query("SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $rest_read['id']);
		$ltgStatusVal = mysqli_fetch_assoc($ltgStatusQuery);
		$restDataName = 'poslavu_'.$ltgStatusVal['data_name'].'_db';
		$configQuery = mlavu_query("SELECT `id`, `value2` FROM `$restDataName`.`config` WHERE `location` = '[1]' AND `type` = 'location_config_setting' AND `setting`= '[2]'", '1', 'use_lavu_togo_2');
		$enable = 1;
		if (mysqli_num_rows($configQuery)>0) {
			if ($_REQUEST['set_ltg_status'] == LTG_MONTHLY || $_REQUEST['set_ltg_status'] == LTG_ANNUAL || $_REQUEST['set_ltg_status'] == PROMO || $_REQUEST['set_ltg_status'] == DEMO ) {
				$getConfigData = mysqli_fetch_assoc($configQuery);
				$extJson = json_decode($getConfigData['value2'], 1);
				$details->fname = $extJson['fname'];
				$details->lname = $extJson['lname'];
				$details->dob = $extJson['dob'];
				$updateConfig = mlavu_query("UPDATE `$restDataName`.`config` SET `value` = '1' WHERE `location` = '1' AND `type` = 'location_config_setting' AND `setting`= 'use_lavu_togo_2'");
			} else {
				$enable = 0;
				$updateConfig = mlavu_query("UPDATE `$restDataName`.`config` SET `value` = '0' WHERE `location` = '1' AND `type` = 'location_config_setting' AND `setting`= 'use_lavu_togo_2'");
			}
		} else {
			$createConfig = mlavu_query("INSERT INTO `$restDataName`.`config` (`location`, `type`, `setting`, `value`) VALUES ('1', 'location_config_setting', 'use_lavu_togo_2', '1')");
		}

		require_once(dirname(__FILE__)."/../../../../cp/areas/extensions/extensions_functions.php");
		$get_extension_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`extensions` WHERE `_deleted` = '0' and `title` = 'Lavu To Go'");
		$get_extension = mysqli_fetch_assoc($get_extension_query);
		$ext_id = $get_extension['id'];
		$resp['info'] = array('description' => "",'error' => "", 'status' => "success");
		addRemoveModule("extensions.ordering.lavutogo", $ext_id, $resp, $enable);
		adminActionLogExtensionChange("Lavu To Go", $enable);
	  if ($_REQUEST['set_ltg_status'] != PROMO && $_REQUEST['set_ltg_status'] != DEMO ) {
		if ($_REQUEST['set_ltg_status'] != '0') {
			if ($_REQUEST['set_ltg_status'] == LTG_ANNUAL) {
				$details->pack = PACK_ANNUAL;
				$zuoraPackage = ZUORA_LTG_ANNUAL;
			} elseif ($_REQUEST['set_ltg_status'] == LTG_MONTHLY) {
				$details->pack = PACK_MONTHLY;
				$zuoraPackage = ZUORA_LTG_MONTHLY;
			}
			try {
				$zuoraConnect = new ZuoraIntegration();
				$zuoraResponse = $zuoraConnect->subscribeToLavuToGo([
					'dataname' => $ltgStatusVal['data_name'],
					'package' => $zuoraPackage
				]);
			} catch(Exception $e) {
				$zuoraResponse = false;
				$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
			font-size: 13px;'>Dear Billing Team,<br/><br/>Location: <b>".$ltgStatusVal['data_name']."</b>(data name),<br/><br/>Lavu To Go has been enabled for this account and there is not an active Zuora account associated with this account.<br/><br/> Please ensure this is an internal account or collect payment from the customer.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
				$subject = "Lavu To Go V2 Enabled for ".$ltgStatusVal['data_name'];
			} 

			$detailsJSON = json_encode($details);
			$update_config_query = lavu_query("UPDATE `$restDataName`.`config` SET `value2` = '$detailsJSON' WHERE `location` = '1' AND `type` = 'location_config_setting' AND `_deleted` = '0' and `setting` = 'use_lavu_togo_2'");
		} else if($enable === 0) {
			try {
				$zuoraConnect = new ZuoraIntegration();
				$zuoraResponse = $zuoraConnect->cancelLavuToGoSubscriptions([
					'dataname' => $ltgStatusVal['data_name'],
					'cancel_reason' => 'Disabling Lavu To Go Subscription'
				]);
			} catch(Exception $e) {
				$zuoraResponse = false;
				$message = "<font style='font-family:Verdana,Arial,Helvetica,sans-serif;
			font-size: 13px;'>Dear Billing Team,<br/><br/>Location: <b>".$ltgStatusVal['data_name']."</b>(data name),<br/><br/>Lavu To Go has been enabled for this account and there is not an active Zuora account associated with this account.<br/><br/> Please ensure we stop billing payment from this customer.<br/><br/>Regards,<br/>Lavu Customer Care</font>";
				$subject = "Lavu To Go V2 Disabled for ".$ltgStatusVal['data_name'];
			} 
		}
		if($zuoraResponse == false) {
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
			mail(ZUORA_BILLING_EMAIL, $subject, $message, $headers);
		}
	  }
	}

	if (isset($_REQUEST['set_custom_monthly_hosting'])) {
		$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_monthly_hosting` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_custom_monthly_hosting'], $rest_read['id']);
		if ($update) $custom_monthly_hosting = $_REQUEST['set_custom_monthly_hosting'];
	}

	if (isset($_REQUEST['set_auto_waive_until'])) {
		$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `auto_waive_until` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_auto_waive_until'], $rest_read['id']);
		if ($update) $auto_waive_until = $_REQUEST['set_auto_waive_until'];
	}

	if (isset($_REQUEST['set_custom_payment_plan'])) {
		$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `custom_payment_plan` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_custom_payment_plan'], $rest_read['id']);
		if ($update) $custom_payment_plan = $_REQUEST['set_custom_payment_plan'];
		$query = mlavu_query("SELECT `min_due_date` FROM `poslavu_MAIN_db`.`payment_status` WHERE `restaurantid`='[1]'", $rest_read['id']);
		if ($query !== FALSE && mysqli_num_rows($query) > 0 && ($read = mysqli_fetch_assoc($query)))
			if ($read['min_due_date'] == '')
				mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `min_due_date`='[1]' WHERE `restaurantid`='[2]'", date('Y-m-d'), $rest_read['id']);
	}

	if (isset($_REQUEST['set_annual_agreement'])) {
		$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `annual_agreement` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_annual_agreement'], $rest_read['id']);
		if ($update) $annual_agreement = $_REQUEST['set_annual_agreement'];
	}

	if (isset($_REQUEST['set_custom_max_ipads']) && is_numeric($_REQUEST['set_custom_max_ipads'])) {
		if ($o_device_limits->setCustomiPhoneiPadKDSLimit(-1, $_REQUEST['set_custom_max_ipads'], -1, -1, $_REQUEST['dn']))
			$custom_max_ipads = $_REQUEST['set_custom_max_ipads'];
		$sysset_ipad_limit = $custom_max_ipads;
		$changed_package_or_device_limits = true;
	}
	if (isset($_REQUEST['set_tableside_max_ipads']) && is_numeric($_REQUEST['set_tableside_max_ipads'])) {
		if ($o_device_limits->setCustomiPhoneiPadKDSLimit(-1, -1, -1, -1, $_REQUEST['dn'],$_REQUEST['set_tableside_max_ipads'])){
			$tablesideMaxIpads = $_REQUEST['set_tableside_max_ipads'];
			$syssetTablesideIpadLimit = $tablesideMaxIpads;
		}
		$changed_package_or_device_limits = true;
	}
	if (isset($_REQUEST['set_custom_max_ipods']) && is_numeric($_REQUEST['set_custom_max_ipods'])) {
		if ($o_device_limits->setCustomiPhoneiPadKDSLimit($_REQUEST['set_custom_max_ipods'], -1, -1, -1, $_REQUEST['dn']))
			$custom_max_ipods = $_REQUEST['set_custom_max_ipods'];
		$sysset_ipod_limit = $sysset_ipod_limit;
		$changed_package_or_device_limits = true;
	}
	if (isset($_REQUEST['set_custom_max_kds']) && is_numeric($_REQUEST['set_custom_max_kds'])) {
		if ($o_device_limits->setCustomiPhoneiPadKDSLimit(-1, -1, $_REQUEST['set_custom_max_kds'], -1, $_REQUEST['dn']))
			$custom_max_kds = $_REQUEST['set_custom_max_kds'];
		$sysset_kds_limit = $sysset_kds_limit;
		$changed_package_or_device_limits = true;
	}
	if (isset($_REQUEST['set_custom_max_kiosk']) && is_numeric($_REQUEST['set_custom_max_kiosk'])) {
		if ($o_device_limits->setCustomiPhoneiPadKDSLimit(-1, -1, -1, $_REQUEST['set_custom_max_kiosk'], $_REQUEST['dn']))
			$custom_max_kiosk = $_REQUEST['set_custom_max_kiosk'];
		$changed_package_or_device_limits = true;
	}
	if (isset($_REQUEST['set_base_ipad_limit']) && is_numeric($_REQUEST['set_base_ipad_limit'])) {
		$a_success = $o_device_limits->setBaseiPadLimit($_REQUEST['set_base_ipad_limit'], $_REQUEST['dn']);
		$custom_max_ipads = $a_success['customiPadLimit'];
		$changed_package_or_device_limits = true;
	}
	if (isset($_REQUEST['set_force_allow_tabs_n_tables'])) {
		$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `force_allow_tabs_n_tables` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_force_allow_tabs_n_tables'], $rest_read['id']);
		if ($update) $force_allow_tabs_n_tables = $_REQUEST['set_force_allow_tabs_n_tables'];
	}
	if (isset($_REQUEST['set_collection_mode'])) {
		$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `collection_mode` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_collection_mode'], $rest_read['id']);
		if ($update) $collection_mode = $_REQUEST['set_collection_mode'];
	}
	if (isset($_REQUEST['set_min_due_date'])) {
		$update = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `min_due_date` = '[1]' WHERE `restaurantid` = '[2]'", $_REQUEST['set_min_due_date'], $rest_read['id']);
		if ($update) $min_due_date = $_REQUEST['set_min_due_date'];
	}
	if (isset($_POST['change_custom_payment_amount'])) {
		$i_payment_id = (int)$_POST['payment_id'];
		$s_bc_reason = $_POST['bc_reason'];
		$i_new_amount = (float)$_POST['new_amount'];
		change_custom_payment_amount($i_payment_id, $s_bc_reason, $i_new_amount);
	}
	if (function_exists('can_access') && can_access('billing_payment_status') && isset($_GET['change_trial_period']) && isset($_GET['amount']) && isset($_GET['addsub']) && isset($_GET['trial_end']) && isset($_GET['id'])) {
		$s_trial_end = $_GET['trial_end'];
		$s_trial_id = $_GET['id'];
		$s_amount = $_GET['amount'];
		$s_addsub = ($_GET['addsub'] == 'plus') ? '+' : '-';
		if (empty($s_trial_end)) {
			$s_new_trial_end_ts = '';
			$s_new_trial_end = '';
		} else {
			$s_new_trial_end_ts = strtotime($s_trial_end.' '.$s_addsub.$s_amount);
			$s_new_trial_end = date('Y-m-d', $s_new_trial_end_ts);
		}
		if ($_GET['change_trial_period'] == '1') {
			// LP-2559 - Change app lockout date in POS db whenever we change the trial_end date
			$dbargs = array('database' => $maindb, 'dataname' => $dataname, 'trial_end' => $s_new_trial_end, 'trial_end_ts' => $s_new_trial_end_ts, 'id' => $s_trial_id);
			mlavu_query("UPDATE `[database]`.`payment_status` SET `trial_end`='[trial_end]' WHERE `id`='[id]'", $dbargs);
			mlavu_query("UPDATE `poslavu_[dataname]_db`.`config` SET `value`='[trial_end_ts]', `value2`='[trial_end]' WHERE `type`='system_setting' AND `setting`='payment_status' LIMIT 1", $dbargs);
			$_GET['change_trial_period'] = '0';
		}
		echo "<form id='reload_billing_status' method='GET' action='#'><input type='hidden' name='dn' value='".$_GET['dn']."'></form><script type='text/javascript'>document.getElementById('reload_billing_status').submit()</script>";
	}

	// Update the POS db with the newly updated package level or terminal limits
	if ($changed_package_or_device_limits) {
		$accountUtils = new LavuAccountUtils();
		$new_values = $accountUtils->setPosPackageInfo($dataname);
		updatePackageAndDeviceLimitInUI($new_values);
	}

	if ($custom_monthly_hosting!="" && is_numeric($custom_monthly_hosting)) $mon_amount = $custom_monthly_hosting;

	if (!function_exists('get_setup_props') || !function_exists('get_first_invoices') || !function_exists('get_days_between_created_and_day'))
		return;

	$props = array();
	$props['license_amount'] = $lic_amount;
	$props['monthly_amount'] = $mon_amount;
	$props['created'] = $rest_created;
	$prop_parts = get_setup_props($props, $package, $signup_status);
	$props['license_billed'] = $prop_parts['license_billed'];
	$props['first_hosting'] = $prop_parts['first_hosting'];
	$props['dataname_processed'] = $dataname;

	$a_first_invoices = get_first_invoices($dataname);
	if (isset($a_first_invoices['License'])) {
		$i_days = get_days_between_created_and_day($a_first_invoices['License'], $props['created']);
		//error_log("days between license and created: $i_days");
		$props['license_billed'] = dtadd($props['created'],$i_days,"days");
	}
	if (isset($a_first_invoices['Hosting'])) {
		$i_days = get_days_between_created_and_day($a_first_invoices['Hosting'], $props['created']);
		//error_log("days between hosting and created: $i_days");
		$props['first_hosting'] = dtadd($props['created'],$i_days,"days");
	}

	$payment_type_list = array("Hosting","License","General","Other");
	if(isset($_GET['set_payment_type']) && isset($_GET['payment_id']))
	{
		$set_payment_type = $_GET['set_payment_type'];
		$payment_id = $_GET['payment_id'];

		mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `payment_type`='[1]' where `id`='[2]' limit 1",$set_payment_type,$payment_id);
	}

	if(isset($_GET['set_custom_payment_type']))
	{
		$scp_type = $_GET['set_custom_payment_type'];
		$scp_date = $_GET['set_custom_payment_date'];
		$scp_amount = $_GET['set_custom_payment_amount'];
		$scp_datetime = $scp_date . " 00:00:00";
		$scp_response_code = "1";

		$vars = array();
		$vars['datetime'] = $scp_datetime;
		$vars['amount'] = $scp_amount;
		$vars['x_type'] = $scp_type;
		$vars['match_restaurantid'] = $rest_id;
		$vars['payment_type'] = "";
		$vars['x_response_code'] = $scp_response_code;

		$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `datetime`='[datetime]' and `x_amount`='[amount]' and `x_type`='[x_type]' and `match_restaurantid`='[match_restaurantid]'",$vars);
		if(mysqli_num_rows($exist_query))
		{
			$exist_read = mysqli_fetch_assoc($exist_query);
		}
		else
		{
			mlavu_query("insert into `poslavu_MAIN_db`.`payment_responses` (`payment_type`,`datetime`,`x_amount`,`x_type`,`match_restaurantid`,`x_response_code`) values ('[payment_type]','[datetime]','[amount]','[x_type]','[match_restaurantid]','[x_response_code]')",$vars);
		}
	}
	else if(isset($_GET['set_custom_invoice_type']))
	{
		$sci_type = $_GET['set_custom_invoice_type'];
		$sci_date = $_GET['set_custom_invoice_date'];
		$sci_amount = $_GET['set_custom_invoice_amount'];

		$vars = array();
		$vars['due_date'] = $sci_date;
		$vars['amount'] = $sci_amount;
		$vars['type'] = $sci_type;
		$vars['dataname'] = $dataname;
		$vars['restaurantid'] = $rest_read['id'];
		$vars['waived'] = "";

		$exist_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[dataname]' and `type`='[type]' and `due_date`='[due_date]'",$vars);
		if(mysqli_num_rows($exist_query))
		{
			$exist_read = mysqli_fetch_assoc($exist_query);
			$output .= "<b>Invoice already exists for $sci_type on $sci_date</b><br><br>";
		}

		else
		{
			$output .= "<b>Creating new invoice for $sci_type on $sci_date</b>";
			$success = mlavu_query("insert into `poslavu_MAIN_db`.`invoices` (`dataname`,`restaurantid`,`amount`,`type`,`waived`,`due_date`) values ('[dataname]','[restaurantid]','[amount]','[type]','[waived]','[due_date]')",$vars);
			if($success) $output .= " (success)";
			else $output .= " (failed)";
			$output .= "<br><br>";

			// create an invoice by parts
			if (strtolower($vars['type']) == 'hosting') {
				$i_invoice_id = mlavu_insert_id();
				MonthlyInvoiceParts::ApplyInvoicePartsToInvoice($vars['dataname'], $i_invoice_id);
			}
		}
	}

	if(isset($_GET['invoice_id']))
	{
		$invoice_id_list = explode(",",$_GET['invoice_id']);

		for($n=0; $n<count($invoice_id_list); $n++)
		{

			// get the invoice id and its type
			$invoice_id = trim($invoice_id_list[$n]);
			$b_is_hosting = FALSE;
			$query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`invoices` WHERE `id`='[id]' AND `type` LIKE 'hosting'", array('id'=>$invoice_id));
			if ($query !== FALSE && mysqli_num_rows($query) > 0)// && dataname_in_billing_by_parts_datanames($dataname))
				$b_is_hosting = TRUE;

			// guarantee that this invoice has parts (for universal editing)
			if ($b_is_hosting)
				MonthlyInvoiceParts::ApplyInvoicePartsToInvoice($dataname, $invoice_id);

			if(isset($_GET['set_invoice_waived']) && $b_is_hosting)
			{

				// waive or remove an invoice (please for the love of everything holy NEVER REMOVE AN INVOICE, JUST WAIVE IT)
				$set_invoice_waived = $_GET['set_invoice_waived'];
				if($set_invoice_waived=="remove") {
					mlavu_query("DELETE FROM `poslavu_MAIN_db`.`invoices_parts` WHERE `invoice_id` = '[invoice_id]'", array('invoice_id'=>$invoice_id));
					mlavu_query("DELETE FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname`='[1]' AND `id`='[2]' LIMIT 1", $dataname, $invoice_id);
				} else {
					mlavu_query("UPDATE `poslavu_MAIN_db`.`invoice_parts` SET `waived`='[1]' WHERE `invoice_id`='[2]' AND `standard_monthly_billing`='1' LIMIT 1", $set_invoice_waived, $invoice_id);
				}
			}
			else if(isset($_GET['set_invoice_amount']) && $b_is_hosting)
			{

				// changes the amount of an invoice
				$set_invoice_amount = str_replace(",","",$_GET['set_invoice_amount']);
				mlavu_query("UPDATE `poslavu_MAIN_db`.`invoice_parts` SET `amount`='[1]' WHERE `invoice_id`='[2]' AND `standard_monthly_billing`='1' LIMIT 1", $set_invoice_amount, $invoice_id);
			}
			else if(isset($_GET['set_invoice_waived']))
			{

				// waive or remove an invoice (please for the love of everything holy NEVER REMOVE AN INVOICE, JUST WAIVE IT)
				$set_invoice_waived = $_GET['set_invoice_waived'];
				if($set_invoice_waived=="remove")
					mlavu_query("delete from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `id`='[2]' limit 1",$dataname,$invoice_id);
				else
					mlavu_query("update `poslavu_MAIN_db`.`invoices` set `waived`='[1]' where `id`='[2]' limit 1",$set_invoice_waived,$invoice_id);
			}
			else if(isset($_GET['set_invoice_amount']))
			{

				// changes the amount of an invoice
				$set_invoice_amount = str_replace(",","",$_GET['set_invoice_amount']);
				mlavu_query("update `poslavu_MAIN_db`.`invoices` set `amount`='[1]' where `id`='[2]' limit 1",$set_invoice_amount,$invoice_id);
			}

			// update invoice parts for monthly hosting invoices
			if ($b_is_hosting)
				MonthlyInvoiceParts::UpdateInvoiceByParts($invoice_id);
		}
	}

	// Change amount on billing profile with Authorize.net
	if (isset($_REQUEST['change_profile']) && isset($_REQUEST['amount']))
	{
		$change_vars = array();
		$change_vars['refid'] = 11;
		$change_vars['subscribeid'] = $_REQUEST['change_profile'];
		$change_vars['amount'] = $_REQUEST['amount'];
		$change_vars['dataname'] = $_REQUEST['dn'];

		$resp = manage_recurring_billing('update_amount', $change_vars);
		error_log(__FILE__ ." DEBUG: resp=". print_r($resp, true));  //debug

		if (isset($resp['success']) && $resp['success']) {
			mlavu_query("update `poslavu_MAIN_db`.`billing_profiles` set `amount`='[amount]' where `dataname`='[dataname]' and `subscriptionid`='[subscribeid]' and `subscriptionid`!='' limit 1", $change_vars);
		}
		else {
			error_log(__FILE__ ." ERROR: failed to update billing profile amount for dataname={$change_vars['dataname']} subscriptionid={$change_vars['subscribeid']} amount={$change_vasr['amount']}");
		}
	}

?>
