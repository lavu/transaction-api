<?php

$over_payment = false;
$checked_list_js = "";
$lic_bill_count = 0;

$web_path = ""; // used to be ../../sa_cp/billing/index.php
//$showInvoicePdf = (strstr($_SERVER['REQUEST_URI'], '/cp/') === false);
$showInvoicePdf = true;

global $zInt;
global $zAccountId;
global $o_emailAddressesMap;
require_once(dirname(__FILE__)."/../../procareas/award_points_for_bills.php");
require_once(dirname(__FILE__)."/../../procareas/award_cmsn_for_bills.php");
require_once(dirname(__FILE__)."/../../procareas/monthly_parts.php");
require_once(dirname(__FILE__)."/../../procareas/procpay_functions.php");
require_once(dirname(__FILE__)."/../../procareas/SalesforceIntegration.php");
require_once(dirname(__FILE__)."/../../package_levels_object.php");
require_once(dirname(__FILE__)."/../../../../manage/globals/email_addresses.php");
if (!isset($o_package_container))
	$o_package_container = new package_container();

if (empty($zAccountId) && !empty($signup_read['zAccountId']))
	$zAccountId = $signup_read['zAccountId'];

if (!class_exists("BillingParseBills")) {
	class BillingParseBills {

		// set the waive status of the standard_monthly_hosting to either 'waived' or 'active'
		// for 'hosting' invoices with invoice parts display the waived/active status based on the invoice parts
		// @$invoice_waived: the waived status of the invoice
		// @$web_path: the redirect path for updating the waived status
		// @$dataname, $package, $signup_status: used for the redirect
		// @$invoice_id: id of the invoice to modify
		// @$bill_type: the type of the bill (eg Hosting, License, Other)
		// @$lic_bill_count: the count of license bills found so far, incremented if $bill_type == "License"
		public static function SetInvoiceWaivedStatus($i, $invoice_waived, $web_path, $dataname, $package, $signup_status, $invoice_id, $bill_type, &$lic_bill_count, $bill_date='') {

			// determine if invoice is hosting and has parts and the monthly_hosting part is waived
			$s_edit_link = "";
			$s_alert_has_parts = "";
			$bill_type_lc = strtolower($bill_type);
			if ( ($bill_type_lc == "hosting" || $bill_type_lc == "hardware") && (int)$invoice_id > 0) {
				$query = mlavu_query("SELECT `waived`, `standard_monthly_billing` FROM `poslavu_MAIN_db`.`invoice_parts` WHERE `invoice_id`='[id]'", array('id'=>$invoice_id));
				if ($query !== FALSE && mysqli_num_rows($query) > 0) {
					$query_read = mysqli_fetch_assoc($query);
					$invoice_waived = $query_read['waived'];
					$s_alert_has_parts = "alert(\"This invoice has invoice parts. You can waive the standard monthly recurring here or by clicking on edit parts.\");--newline--";
					$s_edit_link = "<a style='color:blue; text-decoration:underline; position:absolute; left:24px; cursor:default;' onclick='Edit.InvoiceParts.openLink($(\".billing_change_log\")[0], {$invoice_id});' >edit parts</a>--newline--";
				}
				else if ($bill_type_lc == "hardware" && mysqli_num_rows($query) === 0) {
					$s_edit_link .= "<a style='color:blue; text-decoration:underline; position:absolute; left:24px; cursor:default;' onclick='Edit.InvoiceParts.openLinkForQuote(\"{$dataname}\", \"{$invoice_id}\", \"{$bill_date}\")'>create parts</a>";
				}
			}

			// check for permission
			global $b_has_technical;
			if (!$b_has_technical) {
				return ($invoice_waived ? "Waived--newline--" : "Active");
			}

			// draw the option
			$retval = "";
			$retval .= "<select onchange='
				{$s_alert_has_parts}
				return make_billing_change(
					this,
					\"$invoice_waived\",
					\"Change Invoice Waived Status\",
					\"$web_path?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=$invoice_id&set_invoice_waived=\" + this.value,
					\"\",
					\"".$invoice_id."\",
					\"\"
				);'>--newline--";
			$retval .= "<option value=''>Active</option>--newline--";
			$retval .= "<option value='1'".(($invoice_waived == "1") ? " SELECTED" : "").">Waived</option>--newline--";
			if($bill_type=="License")
				$lic_bill_count++;
			if($bill_type=="Other" || ($bill_type=="License" && $lic_bill_count > 1))
				$retval .= "<option value='remove'>(remove)</option>--newline--";
			$retval .= "</select>--newline--";
			$retval .= $s_edit_link;
			$retval = self::SanitizeReturnedValue($retval);
			return $retval;
		}

		public static function CustomInvoiceAmount($i, $bill_amount, $web_path, $dataname, $package, $signup_status, $invoice_id, $bill_type) {

			// check permissions

			// get the invoice by parts amount
			$i_invoice_parts_amount = (int)$bill_amount;
			$s_alert_has_parts = "";
			if ((int)$invoice_id > 0 && strtolower($bill_type) === 'hosting') {
				$query = mlavu_query("SELECT `amount` FROM `poslavu_MAIN_db`.`invoice_parts` WHERE `invoice_id`='[id]' AND `standard_monthly_billing`='1'", array('id'=>$invoice_id));
				if ($query !== FALSE && mysqli_num_rows($query) > 0) {
					$query_read = mysqli_fetch_assoc($query);
					$i_invoice_parts_amount = (int)$query_read['amount'];
					$s_alert_has_parts = "alert(\"This invoice has invoice parts. You can edit the standard monthly recurring here or by clicking on edit parts.\");";
				}
			}

			// check for permission
			global $b_has_technical;
			if (!$b_has_technical) {
				return "<div id='invoice_amount_{$i}' style='display:block;'><a class='info_value'>$".number_format((float)$bill_amount, 2)."</a></div>";
			}

			// create the html for the option
			$retval = "";
			$retval .= "
				<div id='invoice_amount_$i' style='display:block'>--newline--
					<a class='info_value' style='cursor:pointer' onclick='{$s_alert_has_parts} edit_invoice_amount($i)'>$" . number_format((float)$bill_amount, 2) . "</a>--newline--
				</div>--newline--
				<div id='edit_invoice_amount_$i' style='display:none'>--newline--
					<input id='set_invoice_amount_$i' type='text' value='" . number_format((float)$i_invoice_parts_amount, 2) . "' size='8' --newline-- onKeyPress='
						if(enter_pressed(this,event)) { return make_billing_change(
							this,
							\"".number_format((float)$i_invoice_parts_amount, 2)."\",
							\"Change Invoice Amount from $".number_format((float)$i_invoice_parts_amount, 2)." to $\" + parseFloat(document.getElementById(\"set_invoice_amount_$i\").value.replace(/,/ig,\"\")).toFixed(2),
							\"$web_path?dn=$dataname&package=$package&signup_status=$signup_status&invoice_id=$invoice_id&set_invoice_amount=\" + document.getElementById(\"set_invoice_amount_$i\").value,
							\"\",
							\"".$invoice_id."\",
							\"\"
						);}'--newline--
					/>--newline--
				</div>--newline--";
			$retval = self::SanitizeReturnedValue($retval);
			return $retval;
		}

		public static function CommonJavascript() {
			$s_retval = str_replace(array("\n","\r","\n\r"), "", "
				<script type='text/javascript'>
					window.EditInvoicePartsAppendText = function(win, left, top, w, h) {
						var invoicePartsHelp = $('#invoicePartsHelpContainer').html();
						return \"
						<div id='editInvoiceParts'>
							<div style='position:fixed; left:0; top:0; width:\"+win.width+\"px; height:\"+win.height+\"px; background-color:rgba(0,0,0,0.4);'></div>
							<div class='container' style='position:fixed; left:\"+left+\"px; top:\"+top+\"px; width:\"+w+\"px; height:\"+h+\"px; background-color:white; border:1px solid black; padding:15px; border-radius:5px;'>
								<div style='display:table; margin:0 auto; padding:9px; text-align:center'>
									<b>Edit Invoice Parts</b><span style='width:20px;height:1px;'> </span>\"+invoicePartsHelp+\"<br />
									<em>Only change one field at a time.  Hit Enter to save.</em>
								</div>
								<div id='editInvoicePartsCloseButton' style='position:absolute; top:15px; right:15px; font-size:20px; font-weight:bold; background-color:lightgray; border-radius:3px; padding:5px; width:20px; text-align:center; cursor:pointer;' onclick='Edit.InvoiceParts.closeLink();'>X</div>
								<div class='content_container' style='overflow-y:auto; margin:0 auto; height:\"+(h-40)+\"px;'>
									<div class='content' style='overflow-x:auto; display:table; margin:0 auto;'></div>
								</div>
							</div>
						</div>
						\";
					};
					window.billingReflexiveURL = '?dn={$dataname}';
				</script>");
			$s_retval .= "<script type='text/javascript' src='/manage/js/lavugui.js'></script>";
			$s_retval .= "<script type='text/javascript' src='/sa_cp/billing/js/Edit_InvoiceParts.js'></script>";
			if(!isset($_SESSION['processing_all_bills']))
				$s_retval .= "<div id='invoicePartsHelpContainer' style='display:none;'>".help_mark_tostring(889)."</div>";
			return $s_retval;
		}

		// replaces newlines to make javascript funcitonable
		// replaces --newline-- with a newline
		public static function SanitizeReturnedValue($retval) {
			$retval = str_replace("--newline--", "\n",
				str_replace(array("\n","\r","\n\r"), "",
					$retval)
				);
			return $retval;
		}

		public static function getPackage($rest_id) {

			// get the package
			$a_package = find_package_status($rest_id);
			return $a_package['package'];
		}

		public static function drawBill($a) {
			$retval = "";
			$retval .= "</td>\n";
			$retval .= "<td>".$a["s_for_span"]."</td>\n";
			$retval .= "<td><span class='info_value'>".$a["bill_type"]."</span></td>\n";
			$retval .= "<td valign='bottom'>&nbsp;\n";
			if ($a["lavu_admin"]) {

				// draw the option to waive this invoice to the lavu admin
				$retval .= BillingParseBills::SetInvoiceWaivedStatus($a["i"], $a["invoice_waived"], $a["web_path"], $a["dataname"], $a["package"], $a["signup_status"], $a["invoice_id"], $a["bill_type"], $a["lic_bill_count"], $a['bill_date']);
			} else {

				// draw the bill status to the user
				$retval .= "- ".$a["current_value"];
			}
			$retval .= "</td>\n";
			$retval .= "</tr>\n";
			$retval .= "</table>\n";
			return $retval;
		}
	}

	global $o_billingParseBills;
	if (!isset($o_billingParseBills))
		$o_billingParseBills = new BillingParseBills();
}

global $b_has_technical;
$output .= BillingParseBills::CommonJavascript();

if ($dataname == 'tigay_restaura' || $dataname == 'the_food_compa') {
	error_log(print_r($billable, TRUE));
}

//------------------------- GET UNCAPTURED CODE BLOCK START ------------------------------------//
// This part is here to reduce the payment by the amount it has covered (so it doesn't get applied to normal invoices) //
if(!isset($get_uncaptured_enabled)) $get_uncaptured_enabled = false;
$total_uncaptured = 0;
$uncaptured_txn_list = array();
for($n=0; $n<count($payments); $n++)
{
	$payment_id = ($n + 1);
	$payment_date = $payments[$n][0];
	$payment_amount = $payments[$n][1];
	$payment_type = $payments[$n][2];
	$payment_approved = $payments[$n][3];
	$payment_xtype = $payments[$n][5];
	$batch_status = "";
	$batch_action = "";
	if(isset($payments[$n][6]['batch_status'])) $batch_status = $payments[$n][6]['batch_status'];
	if(isset($payments[$n][6]['batch_action'])) $batch_action = $payments[$n][6]['batch_action'];
	if(substr($batch_action,0,14)=="used to cover ")
	{
		$batch_action_parts = explode("rsp:",$batch_action);
		if(count($batch_action_parts) > 1)
		{
			$batch_action_parts = explode(",",$batch_action_parts[1]);
			for($m=0; $m<count($batch_action_parts); $m++)
			{
				$batch_cover_id = trim($batch_action_parts[$m]);
				if(is_numeric($batch_cover_id))
				{
					$cover_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `id`='[1]'",$batch_cover_id);
					if(mysqli_num_rows($cover_query))
					{
						$cover_read = mysqli_fetch_assoc($cover_query);
						$payments_applied[$n] += $cover_read['x_amount'];
						if(isset($_GET['ut'])) echo "COVER AMOUNT: " . $cover_read['x_amount'] . " - payment used: " . $payments_applied[$n] . "<br>";
					}
				}
			}
		}
	}
}
//------------------------- GET UNCAPTURED CODE BLOCK END ------------------------------------//

$counter=0;
$a_type_counts = array();
$payment_invoice_list = array();
$license_paid_today = false;
for($i=0; $i<count($billable); $i++)
{

	// initialize payments variables for checking if this bill is paid
	$payments_tied_to_this_bill = array();
	$amounts_applied = array();

	// get stats on this bill
	$bill_date = $billable[$i][0];
	$bill_amount = $billable[$i][1];
	$bill_type = $billable[$i][2];
	$bill_waived = $billable[$i][3];

	// initialize some variables for drawing this bill
	$due_str = "";
	$show_actual_bill_amount = true;
	if (!isset($a_type_counts[$bill_type]))
		$a_type_counts[$bill_type] = 0;
	$a_type_counts[$bill_type]++;

	// check for any approved payments
	$approved= false;
	for($n=0; $n<count($payments); $n++){
		if($payments[$n][3]=='approved')
			$approved=true;
	}

	if($bill_date <= date("Y-m-d"))
	{

		// get the bill from the given date
		$bill_cmsn_applied = 0.0;
		$bill_points_applied = 0;
		$bill_distro_code_applied = '';
		$invoice_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `due_date`='[2]' and `type`='[3]'",$dataname,$bill_date,$bill_type);
		if(mysqli_num_rows($invoice_query))
		{

			// the invoice exists, load it from the database
			$invoice_read = mysqli_fetch_assoc($invoice_query);
			$invoice_waived = $invoice_read['waived'];
			$invoice_id = $invoice_read['id'];
			$bill_cmsn_applied = (float)$invoice_read['distro_cmsn_applied'];
			$bill_points_applied = $invoice_read['distro_points_applied'];
			$bill_distro_code_applied = $invoice_read['distro_code_applied_to'];
			$bill_amount = str_replace(",","",$invoice_read['amount']);
		}
		else
		{
			$i_restaurant_package = (int)BillingParseBills::getPackage($rest_id);

			// the invoice doesn't exist, create it
			if ($bill_type !== 'Hosting') {//!dataname_in_billing_by_parts_datanames($dataname) || ) {
				mlavu_query("insert into `poslavu_MAIN_db`.`invoices` (`dataname`,`amount`,`type`,`waived`,`due_date`,`package`) values ('[1]','[2]','[3]','[4]','[5]','[6]')",$dataname,$bill_amount,$bill_type,$bill_waived,$bill_date,$i_restaurant_package);
				$invoice_id = mlavu_insert_id();
				$invoice_waived = "";
			} else {
				$i_points_to_award = $o_package_container->get_recurring_points_by_attribute('level', $i_restaurant_package);
				$f_cmsn_to_award = $o_package_container->get_recurring_cmsn_by_attribute('level', $i_restaurant_package);
				if ($bill_date <= $auto_waive_until) $bill_waived = "1";
				$a_monthly_parts = NULL;
				$s_end_date = date("Y",strtotime("+15 years")).'-01-01';
				MonthlyInvoiceParts::load_monthly_parts($dataname, '2013-01-01', $s_end_date, $a_monthly_parts);

				// Lavu 88 3 months for $8 promo
				global $signup_read;
				if ( strpos($signup_read['source'], '_3mo8') !== false && floatval($bill_amount) == '8.00') $i_points_to_award = 0;

				if (TRUE) {
					$a_restaurant = get_restaurant_from_dataname($dataname);
					$a_package = find_package_status($a_restaurant['id']);
					if ((int)$a_package['package'] != $i_restaurant_package) {
						$s_values = "bill_amount: {$bill_amount}\nbill_date: {$bill_date}\ndataname: {$dataname}\nrest_id: {$rest_id}\na_monthly_parts: ".str_replace("\n", "...", print_r($a_monthly_parts,TRUE))."\nbill_waived: {$bill_waived}\ni_points_to_award: {$i_points_to_award}";
						$o_emailAddressesMap->sendEmailToAddress("billing_errors", "wronge restaurant id for account", "Restaurant id: ".$a_restaurant['id']."\nRestaurant package: ".$a_package['package']."\nValues: ".$s_values);
					}
				}
				$invoice_id = MonthlyInvoiceParts::create_new_invoice($bill_amount, $bill_date, $dataname, $rest_id, $a_monthly_parts, $bill_waived, $i_points_to_award, "calculated package: {$i_package_level}", $signup_read['package'], $f_cmsn_to_award);
				$a_invoices = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('invoices', array('id'=>$invoice_id), TRUE);
				$bill_amount = $a_invoices[0]['amount'];
				$bill_date = $a_invoices[0]['due_date'];
				$dataname = $a_invoices[0]['dataname'];
				$rest_id = $a_invoices[0]['restaurantid'];
				$invoice_waived = $a_invoices[0]['waived'];
			}
		}

		// get the amount due for the bill, ignoring waived bills
		$a_bill_ids_drawn[] = (int)$invoice_id;
		$bill_due = $bill_amount;
		//------------------------- GET UNCAPTURED CODE BLOCK START ------------------------------------//
		// track uncaptured amount
		$bill_uncaptured_due = 0;
		//------------------------- GET UNCAPTURED CODE BLOCK END ------------------------------------//
		if($invoice_waived=="1")
		{
			$bill_waived = true;
			$bill_due = 0;
		}

		// get the last monthly hosting cost of the location
		// should be the total of all invoice parts
		global $f_last_monthly_hosting_cost;
		if (strtolower(trim($bill_type)) == "hosting")
			$f_last_monthly_hosting_cost = $bill_amount;

		$output .= "<table id='billingTable-".$counter."' ><tr><td valign='top' width='280'>\n";
		$counter++;

		$a_bill_is_shown = true;

		// draw the bill information cell
		$output .= "<table cellspacing=0 cellpadding=0>\n";
		$output .= "<tr>\n";
		if ($lavu_admin && $b_has_technical) {
			$output .= "<td><input type='checkbox' name='invoice_checkbox_".$invoice_id."' id='invoice_checkbox_".$invoice_id."' style= 'display:block'></td>\n";
			$checked_list_js .= "  if(document.getElementById('invoice_checkbox_".$invoice_id."').checked) {if(cstr!='') cstr += ','; cstr += '$invoice_id';}\n";
		}
		$output .= "<td colspan='4'>&nbsp;<span class='info_value'><font color='#000066'>" . $bill_date . "</font></span> &nbsp;&nbsp;&nbsp; <span class='info_value3'>Bill ID: ".$bid_prefix.$invoice_id."</span></td></tr>\n";
		$output .= "<tr>\n";
		if ($lavu_admin) $output .= "<td></td>\n";
		$output .= "<td>\n";
		if ($lavu_admin && $b_has_technical) {

			// give the option to edit the invoice amount
			$output .= BillingParseBills::CustomInvoiceAmount($i, $bill_amount, $web_path, $dataname, $package, $signup_status, $invoice_id, $bill_type);
		} else {

			// draw the invoice amount to the user
			$a_license_values = $o_package_container->get_not_lavulite_values();
			if (strtolower($bill_type)=="license" && !in_array((float)$bill_amount,$a_license_values)) {

				// draw as a license invoice
				$show_actual_bill_amount = false;
				if (DEV) {
					$i_restaurant_package = (int)BillingParseBills::getPackage($rest_id);
					$output .= "<span class='info_value'>".$o_package_container->get_printed_name_by_attribute('level', (int)$i_restaurant_package)."</span>\n";
				} else {
					$output .= "<span class='info_value'>".packageToStr(determinePackage($bill_amount, "license"))."</span>\n";
				}
			} else {

				// draw as some other kind of invoice
				$output .= "<span class='info_value'>$".number_format((float)$bill_amount, 2)."</span>\n";
			}
		}

		// some status variables to draw to the user
		$s_for_span = ($show_actual_bill_amount ? "&nbsp;<span class='info_value2'>for</span>&nbsp;" : "");
		$current_value = ($invoice_waived=="1")?"Waived":"Active";

		// draw invoice information
		$a_immutables = array("s_for_span"=>$s_for_span, "bill_type"=>$bill_type, "lavu_admin"=>$lavu_admin, "b_has_technical"=>$b_has_technical, "i"=>$i, "invoice_waived"=>$invoice_waived, "web_path"=>$web_path, "dataname"=>$dataname, "package"=>$package, "signup_status"=>$signup_status, "invoice_id"=>$invoice_id, "lic_bill_count"=>$lic_bill_count, "current_value"=>$current_value, "bill_date"=>$bill_date);
		$output .= BillingParseBills::drawBill($a_immutables);

		$total_applied_str = "";

		if($bill_waived)
		{
			$due_str = "<span class='info_value'>Waived</span>\n";
		}
		else
		{
			// match payments to bills and draw the matches
			$total_applied = 0;
			for($n=0; $n<count($payments); $n++)
			{
				$payment_id = ($n + 1);
				$payment_date = $payments[$n][0];
				$payment_amount = $payments[$n][1];
				$payment_type = $payments[$n][2];
				$payment_approved = $payments[$n][3];
				$payment_xtype = $payments[$n][5];
				if($bill_paid < $bill_amount)
				{
					$payment_can_apply_to_bill = false;
					if(($bill_type==$payment_type || $payment_type=="General") && (($payment_xtype=="auth_capture" || $payment_xtype=='Misc' || $payment_xtype=='Paypal' || $payment_xtype=='Check' || $payment_xtype=='Adjust') || $payment_xtype==strtolower($bill_type)))
						$payment_can_apply_to_bill = true;

					if($payment_approved && $payments_applied[$n] < $payment_amount && $payment_can_apply_to_bill)
					{
						$payment_left = $payment_amount - $payments_applied[$n];

						if(number_format($payment_left,2) > 0 && number_format($bill_due,2) > 0)
						{
							if($payment_left > $bill_due)
							{
								$amount_applied = $bill_due;
							}
							else
							{
								$amount_applied = $payment_left;
							}

							$payments_applied[$n] += (str_replace(",","",number_format($amount_applied, 2)) * 1);
							$bill_due -= $amount_applied;
							$total_applied += $amount_applied;
							$applied_payment_count++;

							//------------------------- GET UNCAPTURED CODE BLOCK START ------------------------------------//
							// Count uncaptured transactions that haven't been covered yet //
							if($get_uncaptured_enabled)
							{
								$batch_status = "";
								$batch_action = "";
								if(isset($payments[$n][6]['batch_status'])) $batch_status = $payments[$n][6]['batch_status'];
								if(isset($payments[$n][6]['batch_action'])) $batch_action = $payments[$n][6]['batch_action'];
								if($batch_status=="N" && $batch_action=="" && $payment_date >= "2014-02-01" && $payment_date < "2014-08-15")
								{
									$bill_uncaptured_due += $amount_applied;
									$total_uncaptured += $amount_applied;
									$uncaptured_txn_list[] = array("pid"=>$payment_id,"info"=>$payments[$n]);
								}
							}
							//------------------------- GET UNCAPTURED CODE BLOCK END ------------------------------------//

							//$output .= "<span class='info_value'><font color='#000044'>Payment " . $payment_id . " ($".number_format($amount_applied,2).")</font> <font style='color:#7788aa; font-size:11px'>$payment_date</font></span><br>";

							$payments_tied_to_this_bill[] = $payment_id;
							if ($amount_applied<$payment_amount) {
								$amounts_applied[$payment_id] = array($amount_applied, $payment_amount);
								if ($i==(count($billable) - 1) && (float)($payments_applied[$n] * 1)<(float)($payment_amount * 1)) $over_payment = array(($payment_id - 1), $payment_amount, ($payment_amount - $payments_applied[$n]));
							}

							if (in_array($n, $payments_with_credits)) {
								$credited = 0;
								for ($cr = 0; $cr < count($tied_credit_info); $cr++) {
									$cr_info = $tied_credit_info[$cr];
									if ($cr_info[2] == $n) {
										$payments_tied_to_this_bill[] = ($cr_info[0] + 1);
										$new_index = (count($payments_tied_to_this_bill) - 1);

										$credited += $cr_info[3];
										$tdoc = $track_display_of_credits[$cr_info[0]];

										$tdoc[0] += $cr_info[3];
										if ($tdoc[1] == $invoice_id) {
											unset($payments_tied_to_this_bill[$tdoc[2]]);
											$new_index--;
										}

										$tdoc[1] = $invoice_id;
										$tdoc[2] = $new_index;
										$track_display_of_credits[$cr_info[0]] = $tdoc;
									}
								}

								if ($credited>=$amount_applied || $amount_applied<=$payment_left) $tied_credited[] = $payment_id;
							}
							if ($bill_due == 0) {
								if ($bill_type == "License") {
									$license_is_paid = true;
									if ($payment_date === date('Y-m-d')) $license_paid_today = true;  // Added for Salesforce integration
								}
								if ($dataname != 'milk_and_honey') {
									break;
								}
							} else if ($bill_type == "License") {
								$license_is_paid = false;
							}
						}
					}
					else if (!$payment_approved && $payment_can_apply_to_bill)
					{
						if (!in_array($payment_id, $tied_declines)) {
							$payments_tied_to_this_bill[] = $payment_id;
							$tied_declines[] = $payment_id;
						}
					}
					else if (in_array($n, $payments_with_credits))
					{

						if (!in_array($payment_id, $tied_credited)) {
							$payments_tied_to_this_bill[] = $payment_id;
							$tied_credited[] = $payment_id;
							for ($cr = 0; $cr < count($tied_credit_info); $cr++) {
								$cr_info = $tied_credit_info[$cr];
								if ($cr_info[2] == $n) {
									$payments_tied_to_this_bill[] = ($cr_info[0] + 1);
									$new_index = (count($payments_tied_to_this_bill) - 1);

									$tdoc = $track_display_of_credits[$cr_info[0]];

									$tdoc[0] += $cr_info[3];
									if ($tdoc[1] == $invoice_id) {
										unset($payments_tied_to_this_bill[$tdoc[2]]);
										$new_index--;
									}

									$tdoc[1] = $invoice_id;
									$tdoc[2] = $new_index;
									$track_display_of_credits[$cr_info[0]] = $tdoc;
								}
							}
						}

					}
				}
				else $output .= " $package - $bill_paid / $bill_amount ";
			}

			// check if the bill has been fully paid and there is more than one payment used for this bill and
			// draw success to the user!
			if (count($payments_tied_to_this_bill)>1 && $show_actual_bill_amount) {

				$total_applied_str = "
				<tr style='font-family:Arial, Helvetica, sans-serif; font-size:13px;'>
					<td style='color:#004400; border-top:1px solid #307130'>
						<nobr><b></b></nobr>
					</td>
					<td colspan='2' style='border-top:1px solid #307130'>
						&nbsp;
					</td>
					<td style='color:#004400; border-top:1px solid #307130;' align='right'>
						<b>$".number_format($total_applied, 2)."</b>
					</td>
					<td colspan='4' style='border-top:1px solid #307130'>
						&nbsp;
					</td>
				</tr>\n";
			}

			// check if the bill has been paid
			if (count($payments_tied_to_this_bill) >= 1 && $bill_due <= 0) {

				// check if this can be a referral account, this is the first paid hosting bill on the account, and it is a referral account
				if (strtotime($rest_read['created']) > strtotime('2013-07-01') && $a_type_counts['Hosting'] == 1 && $bill_type == "Hosting") {
					if (isset($in_lavu))
						$old_in_lavu = $in_lavu;
					if (!isset($in_lavu) || !$in_lavu)
						$in_lavu = TRUE;
					$b_dont_draw_referrals = TRUE;
					require_once(dirname(__FILE__)."/../../../../cp/areas/refer_somebody.php");
					$s_promo = get_promo_code($dataname);
					if ($s_promo !== "" && !strpos($s_promo, "used:") !== 0) {

						// attempt to apply the promo code
						$s_results = redeem_referral_code($dataname, 'billing', 'billing automaton');
					}
					if (isset($old_in_lavu)) {
						$in_lavu = $old_in_lavu;
						unset($old_in_lavu);
					} else {
						unset($in_lavu);
					}
				}
			}

			if ($license_is_paid && !strstr($rest_read['license_status'], "Paid")) $update_license_status = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `license_status` = 'Paid' WHERE `id` = '[1]'", $rest_id);
			if (!$license_is_paid && strstr($rest_read['license_status'], "Paid")) $update_license_status = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `license_status` = 'Due' WHERE `id` = '[1]'", $rest_id);
		}

		// check if the bill hasn't been comopletely paid and
		// draw the failure to the user
		//------------------------- GET UNCAPTURED CODE BLOCK START ------------------------------------//
		// Make sure the bill shows an unpaid amount //
		if(isset($bill_uncaptured_due) && $bill_uncaptured_due > 0)
		{
			$bill_due += $bill_uncaptured_due;
		}
		//------------------------- GET UNCAPTURED CODE BLOCK END ------------------------------------//

		$bill_due = str_replace(",","",number_format($bill_due,2)) * 1;
		if($bill_due > 0)
		{
			$due_str .= "<span class='info_value'><font style='font-size:11px; color:#880000'>Due: $".number_format($bill_due,2)."</font></span>";
			$bills_due_list[] = array("invoice_id"=>$invoice_id,"amount"=>$bill_amount,"paid"=>$bill_paid,"amount_due"=>$bill_due,"type"=>$bill_type,"date_due"=>$bill_date);

			$is_first_license_due_now = false;
			if($bill_type=="License" && $bill_date==substr($rest_read['created'],0,10) && $bill_date!="")
				$is_first_license_due_now = true;

			if ($bill_date < date("Y-m-d") || $is_first_license_due_now) {
				if(!isset($bill_types_due[$bill_type]))
					$bill_types_due[$bill_type] = 0;
				$bill_types_due[$bill_type] += $bill_due;
				$all_bills_due += $bill_due;
				if($earliest_past_due=="" || $bill_date < $earliest_past_due) $earliest_past_due = $bill_date;
			}
		}
		collect_info_on_bills($invoice_id, $dataname, $bill_due, $bill_waived, $bill_type, $bill_amount, $bill_date, $bill_points_applied, $bill_distro_code_applied, $bill_cmsn_applied);

		$output .= "<br>";
	}

	$invoicePdfHtml = ($showInvoicePdf && $a_bill_is_shown) ? "<td width='40' valign='top'><a target='_blank' href='/sa_cp/billing/invoicepdf.php?dataname={$dataname}&invoiceid={$invoice_id}'><img src='/sa_cp/images/invoicepdf/pdficon_large.png' width='32' height='32' /></a></td>" : '';
	$output .= "</td>{$invoicePdfHtml}<td valign='top' style='border-left:1px solid #888888;'><table cellspacing='0' cellpadding='2' width='550'>\n";
	foreach ($payments_tied_to_this_bill as $payment_id)
	{
		if (isset($amounts_applied[$payment_id]) && $show_actual_bill_amount)
		{
			$payment_amount = $amounts_applied[$payment_id][1];
			$applied_amount = $amounts_applied[$payment_id][0];
			$output .= str_replace("$".number_format($payment_amount, 2), "</b>(Out of<b> $".number_format($payment_amount, 2)."</b>) <b>$".number_format($applied_amount, 2), $payments_code_array[($payment_id - 1)]);
		}
		else if (!$show_actual_bill_amount)
		{
			$payment_amount = $payment_amounts[($payment_id - 1)];
			$output .= str_replace("$".number_format($payment_amount, 2), "Paid", $payments_code_array[($payment_id - 1)]);
		}
		else $output .= $payments_code_array[($payment_id - 1)];

		if (!in_array($payment_id, $tied_payments)) $tied_payments[] = $payment_id;

		//-------------- Start Code for recording which invoices each payment is connected to ------------------//
		$payment_response_id = $payments[($payment_id - 1)][4];
		if(isset($amounts_applied[$payment_id]))
		{
			$payment_amount = $amounts_applied[$payment_id][1];
			$applied_amount = $amounts_applied[$payment_id][0];
		}
		else
		{
			$payment_amount = $payment_amounts[($payment_id - 1)];
			$applied_amount = $payment_amount;
		}
		if(!isset($payment_invoice_list[$payment_response_id])) $payment_invoice_list[$payment_response_id] = array();
		$payment_invoice_list[$payment_response_id][] = array("invoice_id"=>$invoice_id,"invoice_date"=>$bill_date,"invoice_amount"=>$bill_amount,"invoice_type"=>$bill_type,"payment_applied"=>$applied_amount,"payment_amount"=>$payment_amount,"payment_number"=>$payment_id);
		//------------------------------ End code for recording which invoices each payment is connected to ------------------//
	}

	$output .= $total_applied_str;
	if ($due_str != "") $output .= "<tr><td colspan='2'></td><td colspan='2' align='right'><table cellpadding='2'><td style='border:1px solid #000000'>$due_str</td></table></td><tr>\n";
	$output .= "</table><br></td></tr>\n";

	$colspan = ($showInvoicePdf && $a_bill_is_shown) ? '3' : '2';
	if (!$a_bill_is_shown && $i==(count($billable) - 1)) {
		$output .= "<tr><td colspan='{$colspan}' class='info_value' style='padding:4px 20px 4px 250px'>No bills found for this account</td></tr>\n";
		if (count($payments) == 0) $output .= "<tr><td colspan='{$colspan}' class='info_value' style='padding:4px 20px 4px 20px'>No payments found for this account</td></tr>\n";
	}

	$output .= "<tr><td colspan='{$colspan}'><hr style='height: 1px; margin:2px 0px 2px 0px;'></td></tr></table>\n";

}//END OF FOR LOOP

// Display Zuora invoices and Payments

function zObjsSortByInvoiceDate($zObjA, $zObjB) { return strtotime($zObjA->InvoiceDate) - strtotime($zObjB->InvoiceDate); }
function zObjsSortByEffectiveDate($zObjA, $zObjB) { return strtotime($zObjA->EffectiveDate) - strtotime($zObjB->EffectiveDate); }

if ( $zAccountId )
{
	$zIntArgs = array('zAccountId' => $zAccountId);

	// LP-345 -- Added support for hiding reseller invoices on customer accounts
	$zInvoices = $zInt->getInvoicesNotFlaggedHidden($zIntArgs);
	usort($zInvoices, 'zObjsSortByInvoiceDate');

	//error_log("DEBUG: ...zInvoices=". print_r($zInvoices, true));  //debug

/*	$zPayments = $zInt->getPayments($zIntArgs);
	//error_log("DEBUG... zPayments=". print_r($zPayments, true));  //debug

	$zInvoicePayments = $zInt->getInvoicePaymentsForInvoices(array('zAccountId' => $zAccountId, 'zInvoices' => $zInvoices));
	//error_log("DEBUG... zInvoicePayments=". print_r($zInvoicePayments, true));  //debug

	$payment_counter = count($payments) + 1;
	$zPaymentIdsToPaymentNum = array();
	$zInvoiceIdToPaymentIds  = array();

	// Map which Zuora payments go to which Zuora invoice and make it look like the applied amounts are the actual payment amounts 
	foreach ( $zPayments as $zPayment )
	{
		foreach ( $zInvoicePayments as $zInvoicePayment )
		{
			if ( $zInvoicePayment->PaymentId == $zPayment->Id )
			{
				$zPayment->AppliedInvoiceAmount = $zInvoicePayment->Amount;
				$zInvoiceIdToPaymentIds[$zInvoicePayment->InvoiceId][] = $zPayment;
			}
		}
	}
*/
	// Draw Zuora invoices and payments
	$tempInvoices = array();
	foreach ( $zInvoices as $zInvoice){
			$tempInvoices[$zInvoice->InvoiceNumber] = $zInvoice;
	}
	$zInvoices = $tempInvoices;
	krsort($zInvoices);
	foreach ( $zInvoices as $zInvoice)
	{
		$invoice_amount = number_format((float) $zInvoice->Amount, 2);
		$invoice_status = empty($zInvoice->Balance) ? 'PAID' : '';

		// Display Zuora invoice
		$output .= <<<HTML
		<!-- Table for each Invoice + Payments -->
		<table id='billingTable-{$counter}' >
		<tr>
			<td valign='top' width='280'>
				<table cellspacing=0 cellpadding=0>
				<tr>
					<td colspan='4' nowrap>&nbsp;<span class='info_value'><font color='#000066'>{$zInvoice->InvoiceDate}</font></span> <span class='info_value3'>Bill ID: {$zInvoice->InvoiceNumber}</span></td></tr>
				<tr>
					<td colspan='2' align=right>&nbsp;<span class='info_value2'>\${$invoice_amount} for</span>&nbsp;</td>
					<td><span class='info_value'>Hosting</span></td>
					<td valign='bottom'>&nbsp; {$invoice_status}</td>
				</tr>
				</table>
			</td>
			<td width='40' valign='top'><a target='_blank' href='/sa_cp/billing/zinvoicepdf.php?dataname={$dataname}&zai={$zAccountId}&zii={$zInvoice->Id}'><img src='/sa_cp/images/invoicepdf/pdficon_large.png' width='32' height='32' /></a></td>
			<td valign='top' style='border-left:1px solid #888888;'>
				<table cellspacing="0" cellpadding="2" width="550">
HTML;
/*
		// Sort payments about to be displayed by EffectiveDate
		$zPaymentsForInvoice = $zInvoiceIdToPaymentIds[$zInvoice->Id];
		usort($zPaymentsForInvoice, 'zObjsSortByEffectiveDate');
		//error_log("DEBUG: ...zInvoiceId={$zInvoice->Id} got ". count($zPaymentsForInvoice) ." payments");  //debug

		if ( count($zPaymentsForInvoice) === 0 )
		{
			$output .= <<<HTML
				<!-- No Payment table -->
				<tbody><tr><td width="85%">&nbsp;</td><td style="border:1px solid #000000" nowrap><span class="info_value"><font style="font-size:11px; color:#880000">Due: \${$zInvoice->Balance}</font></span></td><td width="5%"></td></tr></tbody>
HTML;
		}

		// Display Zuora payments for this Zuora invoice
		foreach ( $zPaymentsForInvoice as $zPayment )
		{
			// Make sure we re-use the same payment_num for zPayments we've already processed (since they pay multiple zInvoices)
			if ( isset($zPaymentIdsToPaymentNum[$zPayment->Id]) )
			{
				$payment_num = $zPaymentIdsToPaymentNum[$zPayment->Id];
			}
			else
			{
				$payment_num = $payment_counter;
				$zPaymentIdsToPaymentNum[$zPayment->Id] = $payment_num;
				$payment_counter++;
			}

			$payment_amount = number_format((float) $zPayment->Amount, 2);

			$output .= <<<HTML
				<!-- Payment table -->
				<tbody><tr style="font-family:Arial, Helvetica, sans-serif; font-size:13px;">
					<td style="color:#004400;"><nobr>Payment {$payment_num}&nbsp;</nobr></td>
					<td style="color:#004400;"><nobr><font size="-1" color="#555555">ID: {$zPayment->PaymentNumber}</font>&nbsp;</nobr></td>
					<td style="color:#004400;"><nobr><font size="-1" color="#555555"></font>&nbsp;</nobr></td>
					<td style="color:#004400;"><nobr>{$zInvoice->EffectiveDate}</nobr></td><td style="color:#004400"><nobr></nobr></td>
					<td style="color:#004400;" align="right"><nobr><b>\${$payment_amount}</b><payment_row_credit_{$payment_num}></payment_row_credit_{$payment_num}></nobr></td>
					<td style="color:#004400;">General</td>
					<td style="color:#004400;"> auth_capture</td>
					<td style="color:#004400;"><font color="#008800"> approved</font></td>
				</tr></tbody>
HTML;
		}
*/
		$output .= <<<HTML
				</table>
			</td>
		</tr>
		<tr>
		  <td colspan='4'><hr style='height: 1px; margin:2px 0px 2px 0px;'></td>
		</tr>
		</table>
HTML;
		$counter++;
	}
}

//----------------- Begin code to record invoices payment connections ------------------------//
if(isset($payment_invoice_list))
{
	foreach($payment_invoice_list as $pikey => $pival)
	{
		$iapply_str = "";
		//echo "<hr>";
		foreach($pival as $pnum => $pval)
		{
			if($iapply_str!="") $iapply_str .= "|";
			$iapply_str .=  $pval['payment_applied'] . " to " . $pval['invoice_id'];
			//$payment_invoice_list[$payment_response_id] = array("invoice_id"=>$invoice_id,"invoice_date"=>$bill_date,"invoice_amount"=>$bill_amount,"invoice_type"=>$bill_type,"payment_applied"=>$applied_amount,"payment_amount"=>$payment_amount,"payment_number"=>$payment_id);
			//echo "invoice: " . $pval['invoice_id'] . " - " . $pval['invoice_date'] . " - " . $pval['invoice_type'] . "<br>";
			//echo "payment: " . $pikey . " " . $pval['payment_applied'] . " / " . $pval['payment_amount'] . "<br>";
			//echo "<br>";
		}
		//echo "update `poslavu_MAIN_db`.`payment_responses` set `invoices_applied_to`='$iapply_str' where `id`='$pikey'<br>";
		mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `invoices_applied_to`='[1]' where `id`='[2]'",$iapply_str,$pikey);
	}
}
//----------------- End code to record invoices payment connections ------------------------//

//------------------------- GET UNCAPTURED CODE BLOCK START ------------------------------------//
// Apply new payment to cover uncaptured transactions (if it is for the correct amount) //
if($get_uncaptured_enabled)
{
	if(!function_exists("collection_display_date"))
	{
		function collection_display_date($d)
		{
			$tparts = explode(" ",$d);
			if(count($tparts) > 1) $t = $tparts[1]; else $t = "";
			$d = $tparts[0];
			$dparts = explode("-",$d);
			if(count($dparts) > 2)
			{
				$year = $dparts[0];
				$month = $dparts[1];
				$day = $dparts[2];
				
				$ts = mktime(0,0,0,$month,$day,$year);
				if(date("Y")!=date("Y",$ts))
					return date("F jS Y",$ts);
				else
					return date("F jS",$ts);
			}
			else return $d;
		}
	}
	if($total_uncaptured * 1 > 0 && count($uncaptured_txn_list) > 0)
	{
		$uncaptured_paid_covered = false;
		$uncaptured_first_date = "";
		$uncaptured_last_date = "";
		$two_days_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-2,date("Y")));
		$two_days_later = date("Y-m-d",mktime(0,0,0,date("m"),date("d")+2,date("Y")));
		for($n=0; $n<count($payments); $n++)
		{
			$payment_id = ($n + 1);
			$payment_date = $payments[$n][0];
			$payment_amount = $payments[$n][1];
			$payment_type = $payments[$n][2];
			$payment_approved = $payments[$n][3];
			$payment_responseid = $payments[$n][4];
			$payment_xtype = $payments[$n][5];
			$batch_status = "";
			$batch_action = "";
						
			$payment_uncaptured = false;
			if(isset($payments[$n][6]['batch_status'])) $batch_status = $payments[$n][6]['batch_status'];
			if(isset($payments[$n][6]['batch_action'])) $batch_action = $payments[$n][6]['batch_action'];
			if($batch_status=="N" && $payment_date >= "2014-02-01" && $payment_date < "2014-08-15")
			{
				$payment_uncaptured = true;
				if($uncaptured_first_date=="" || $uncaptured_first_date > $payment_date) $uncaptured_first_date = $payment_date;
				if($uncaptured_last_date=="" || $uncaptured_last_date < $payment_date) $uncaptured_last_date = $payment_date;
			}
			if($payment_approved && $payments_applied[$n] * 1 < $payment_amount * 1 && $payment_xtype=="auth_capture" && !$payment_uncaptured && $payment_date >= $two_days_ago && $payment_date < $two_days_later)
			{
				$payment_amount_left = $payment_amount * 1 - $payments_applied[$n] * 1;
				if(isset($_GET['ttt']))
				{
					echo "Payment Approved: $payment_approved<br>";
					echo "Payment Applied: " . $payments_applied[$n] . " < $payment_amount<br>";
					echo "Payment Xtype: " . $payment_xtype . "<br>";
					echo "Payment Uncaptured: " . $payment_uncaptured . "<br>";
					echo "Total Uncaptured: " . $total_uncaptured . "<br>";
					echo "Payment Amount Left: " . $payment_amount_left . "<br>";
					if(number_format($total_uncaptured,2)==number_format($payment_amount_left,2)) { echo "Condition True"; } else { echo "Condition False"; }
				}
				if(number_format($total_uncaptured,2)==number_format($payment_amount_left,2))
				{
					//echo "Payment: $payment_id has $payment_amount_left to apply:<br>";
					$ut_payments_covered = "";
					$ut_payments_covered_rsps = "";
					for($ut=0; $ut<count($uncaptured_txn_list); $ut++)
					{
						$ut_id = $uncaptured_txn_list[$ut]['pid'];
						$ut_amount = $uncaptured_txn_list[$ut]['info'][1];
						$ut_type = $uncaptured_txn_list[$ut]['info'][2];
						$ut_approved = $uncaptured_txn_list[$ut]['info'][3];
						$ut_responseid = $uncaptured_txn_list[$ut]['info'][4];
						$ut_xtype = $uncaptured_txn_list[$ut]['info'][5];
						
						$set_batch_action = "covered by payment " . $payment_id . " rsp:" . $payment_responseid;
						$utsuccess = mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `batch_action`='[1]' where `id`='[2]'",$set_batch_action,$ut_responseid);
						if($utsuccess)
						{
							if($ut_payments_covered!="") $ut_payments_covered .= ",";
							else $ut_payments_covered .= "used to cover ";
							if($ut_payments_covered_rsps!="") $ut_payments_covered_rsps .= ",";
							else $ut_payments_covered_rsps .= "rsp:";
							$ut_payments_covered .= $ut_id;
							$ut_payments_covered_rsps .= $ut_responseid;
						}
						//echo $ut_id . " - " . $ut_responseid . " - " . $ut_amount . " - " . $ut_type . " - " . $ut_approved . " - " . $ut_xtype . "<br>";
					}
					if($ut_payments_covered!="")
					{
						$ut_payments_covered = trim($ut_payments_covered . " " . $ut_payments_covered_rsps);
						$utsuccess = mlavu_query("update `poslavu_MAIN_db`.`payment_responses` set `batch_action`='[1]' where `id`='[2]'",$ut_payments_covered,$payment_responseid);
						$uncaptured_paid_covered = true;
						
						mail("corey@lavu.com","Payment Coverage for $" . number_format($payment_amount_left,2),"Dataname: " . $dataname . "\nAmount: " . number_format($payment_amount_left,2) . "\nInfo: " . $ut_payments_covered,"From: payment@poslavu.com");
					}
					//echo "Payment: $payment_id - $payment_type - $payment_xtype - ".$payments_applied[$n]."/$payment_amount<br>";
				}
			}
		}
		if(!$uncaptured_paid_covered && $uncaptured_first_date!="" && count($uncaptured_txn_list) > 0)
		{
			global $account_past_due_msg;
			if($uncaptured_first_date!=$uncaptured_last_date && $uncaptured_last_date!="")
			{
				$account_past_due_msg = "";
				$account_past_due_msg .= "<table cellpadding=8 style='border:solid 1px #777777' bgcolor='#eeeeee'><tr><td align='center' valign='middle'><img src='/cp/images/blue_alert_icon.png' /></td><td width='600'>";
				$account_past_due_msg .= "Dear customer, records show that one or more invoiced payments were not drafted from your bank or credit card account as scheduled. These missed charges occurred between [FIRST DATE] and [LAST DATE], and although these payments were marked as approved they were never processed.<br><br>";
				$account_past_due_msg .= "To avoid this in the future we have switched to a new credit card processor. Please re-enter your credit or debit card information below to re-initiate your recurring billing and to process these missed charges.<br><br>";
				$account_past_due_msg .= "Thank you, and if you have any questions please email billing@lavuinc.com.";
				$account_past_due_msg .= "</td></tr></table>";
				
				$account_past_due_msg = str_replace("[FIRST DATE]",collection_display_date($uncaptured_first_date),$account_past_due_msg);
				$account_past_due_msg = str_replace("[LAST DATE]",collection_display_date($uncaptured_last_date),$account_past_due_msg);
			}
			else
			{
				$account_past_due_msg = "";
				$account_past_due_msg .= "<table cellpadding=8 style='border:solid 1px #777777' bgcolor='#eeeeee'><tr><td align='center' valign='middle'><img src='/cp/images/blue_alert_icon.png' /></td><td width='600'>";
				$account_past_due_msg .= "Dear customer, records show that an invoiced payment was not drafted from your bank or credit card account as scheduled. This missed charge occurred on [DATE], and although this payment was marked as approved it was never processed.<br><br>";
				$account_past_due_msg .= "To avoid this in the future we have switched to a new credit card processor. Please re-enter your credit or debit card information below to re-initiate your recurring billing and process the missed charge.<br><br>";
				$account_past_due_msg .= "Thank you, and if you have any questions please email billing@lavuinc.com.";
				$account_past_due_msg .= "</td></tr></table>";

				$account_past_due_msg = str_replace("[DATE]",collection_display_date($uncaptured_first_date),$account_past_due_msg);
			}
		}
		if(isset($_GET['testu'])) { echo "UPC: $uncaptured_paid_covered UFD: $uncaptured_first_date UC: " . count($uncaptured_txn_list) . "<br>"; echo "APDM: $account_past_due_msg<br>"; }
	}	
}
//------------------------- GET UNCAPTURED CODE BLOCK END ------------------------------------//

award_cmsn_for_bills();

if ($license_paid_today) SalesforceIntegration::logSalesforceEvent($dataname, 'Closed Won');

?>
