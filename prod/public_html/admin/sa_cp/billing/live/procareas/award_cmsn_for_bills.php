<?php
/******************************************************************************************
 * A helper file for parse_bills.php.
 * (Copied from award_points_for_bills.php)
 *
 *
 ******************************************************************************************/

require_once(dirname(__FILE__)."/../../package_levels_object.php");
require_once(dirname(__FILE__)."/../../../../cp/objects/deviceLimitsObject.php");
require_once(dirname(__FILE__)."/../../../../manage/globals/email_addresses.php");
if (!isset($o_package_container) || $o_package_container === NULL)
	$o_package_container = new package_container();

global $o_emailAddressesMap;

/* TO DO : uncomment this if we ever stop using award_points_for_bills.php
// adds a bill to a_all_bills_status
// removes any bills without the same dataname
// ignore bills with a null dataname
$a_all_bills_status = array();
function collect_info_on_bills($invoice_id, $dataname, $bill_due, $bill_waived, $bill_type, $bill_amount, $bill_date, $bill_points_applied, $bill_distro_code_applied, $bill_cmsn_applied=0.00) {
	global $a_all_bills_status;

	if ($dataname == '')
		return;
	if (count($a_all_bills_status) > 0)
		foreach($a_all_bills_status as $k=>$a_bill_status)
			if ($a_bill_status['dataname'] != $dataname)
				unset($a_all_bills_status[$k]);

	$a_all_bills_status[] = array('id'=>(int)$invoice_id, 'dataname'=>$dataname, 'due'=>(float)$bill_due, 'waived'=>(boolean)$bill_waived, 'type'=>$bill_type, 'amount'=>(float)$bill_amount, 'date'=>$bill_date, 'points'=>(int)$bill_points_applied, 'cmsn'=>$bill_cmsn_applied, 'distro_code'=>$bill_distro_code_applied);
}*/

// check through every bill in $a_all_bills_status (which should only contain bills for the current dataname)
// and awards cmsn if it meets all criteria
function award_cmsn_for_bills() {
	global $a_all_bills_status;
	global $o_package_container;
	global $maindb;
	global $dataname;
	global $o_device_limits;
	global $o_emailAddressesMap;

	$i_pre_cutoff_ts = strtotime("141 days ago");//5 days ago");
	$i_post_cutoff_ts = strtotime("+141 days");//5 days");
	$a_recurring_cmsn_packages = $o_package_container->get_recurring_cmsn_packages();
	$a_recurring_cmsn_package_levels = array();

	if (count($a_all_bills_status) == 0)
		return;

	foreach($a_recurring_cmsn_packages as $o_package) {
		$i_level = $o_package->get_level();
		$a_recurring_cmsn_package_levels[$i_level] = $i_level;
	}

	foreach($a_all_bills_status as $a_bill_status) {
		// check ALL the conditions
		if ($a_bill_status['waived']) {
			continue;
		}
		if ($a_bill_status['due'] > 0) {
			continue;
		}
		if ((float)$a_bill_status['cmsn'] !== 0.0) {
			continue;
		}
		if ($a_bill_status['distro_code'] != '') {
			continue;
		}
		// TO DO : see if this should be restored (seems like we'll want to keep it commented out for hardware or custom [and maybe even license] invoices)
		//if (strtolower($a_bill_status['type']) != 'hosting') {
		//	continue;
		//}
		$i_bill_ts = strtotime($a_bill_status['date']);
		if ($i_bill_ts < $i_pre_cutoff_ts) {
			continue;
		}
		if ($i_bill_ts > $i_post_cutoff_ts) {
			continue;
		}

		// find the restaurant/distro
		require_once(dirname(__FILE__)."/../../../../distro/distro_data_functions.php");
		$a_distro = get_distro_from_array_distrocode_dataname(array(), '', $a_bill_status['dataname']);
		$a_restaurant = get_restaurant_from_dataname($a_bill_status['dataname']);
		if (count($a_distro) == 0) {
			award_cmsn_for_bills_log_no_distro($a_bill_status, $a_restaurant, $a_distro);
			continue;
		}

		// find the package and make sure that it's a cmsn package
		$a_package = find_package_status($a_restaurant['id']);
		if (!in_array((int)$a_package['package'], $a_recurring_cmsn_package_levels)) {
			continue;
		}

		// check that the distro is good to get cmsn
		if ($a_distro['username'] == '' || $a_distro['username'] == 'zephyrhardware' || $a_distro['username'] == 'century') {
			continue;
		}

		// get the amount of cmsn to be applied
		if ((int)$a_bill_status['id'] > 582733) {
			$a_cmsn = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable("invoice_parts", array("invoice_id"=>$a_bill_status['id']), TRUE, array('selectclause'=>'SUM(`distro_cmsn`) AS `distro_cmsn`'));
			$f_cmsn_to_award = (count($a_cmsn) > 0) ? (float)$a_cmsn[0]['distro_cmsn'] : 0.00;
			$f_package_cmsn = $o_package_container->get_recurring_cmsn_by_attribute('level', (int)$a_package['package']);
			if ($f_cmsn_to_award !== $f_package_cmsn && $f_cmsn_to_award > 0) {
				error_log("possible mismatch of cmsn for bill");
				$o_emailAddressesMap->sendEmailToAddress("billing_errors", "possible mismatch of cmsn for bill", "Dataname: ".$a_bill_status['dataname']."\nPackage: ".$a_package['package']."\nCmsn: {$f_cmsn_to_award}\nPredicted Cmsn: {$f_package_cmsn}");
			}
		} else {
			$f_cmsn_to_award = $o_package_container->get_recurring_cmsn_by_attribute('level', (int)$a_package['package']);
		}
		if ($f_cmsn_to_award === 0.0 || $f_cmsn_to_award === 0) {
			continue;
		}

		// check if the distro is in good standing
		if (!award_cmsn_for_bills_check_good_standing($a_distro, $a_bill_status, $a_restaurant)) {
			continue;
		}

		// check the logs
		$billing_log_query = mlavu_query("SELECT `id` FROM `[maindb]`.`billing_log` WHERE `action`='[action]' AND (`details` LIKE '[details]%' OR `details` LIKE '[details2]%') LIMIT 1",
			array("maindb"=>$maindb, "action"=>'apply distro cmsn', "details"=>'invoice id '.$a_bill_status['id'].": applied {$f_cmsn_to_award} cmsn to distro", "details2"=>'invoice id '.$a_bill_status['id'].': could not apply distro cmsn'));
		if ($billing_log_query === FALSE) {
			error_log("bad billing log query (/sa_cp/billing/procareas/parse_bills.php");
			continue;
		}
		if (mysqli_num_rows($billing_log_query) > 0) {
			continue;
		}
		// award the cmsn
		if ($a_distro['username'] == 'tmax') {
			error_log("old: ".$a_distro['cmsn'].", to give: $f_cmsn_to_award");
		}
		$f_new_distro_cmsn = (float)$a_distro['cmsn']+$f_cmsn_to_award;
		award_cmsn_for_bills_give_cmsn($f_new_distro_cmsn, (float)$a_distro['cmsn'], $f_cmsn_to_award, $a_distro['username'], $a_distro['id'], $a_bill_status['id'], $a_bill_status['dataname'], $a_restaurant['id']);
	}
}

// entirely for the purpose of logging and updating the invoice
function award_cmsn_for_bills_log_no_distro($a_bill_status, $a_restaurant, $a_distro) {
	global $maindb;

	mlavu_query("UPDATE `[maindb]`.`invoices` SET `distro_code_applied_to`='N/A' WHERE `id`='[id]'",
		array("maindb"=>$maindb, "id"=>$a_bill_status['id']));
	mlavu_query("INSERT INTO `[maindb]`.`billing_log` (`datetime`,`dataname`,`restaurantid`,`username`,`fullname`,`action`,`details`) VALUES ('[datetime]','[dataname]','[restaurantid]','[username]','[fullname]','[action]','[details]')",
		array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'dataname'=>$a_bill_status['dataname'], 'restaurantid'=>$a_restaurant['id'], 'username'=>'award_cmsn_for_bills.php', 'fullname'=>'award_cmsn_for_bills.php', 'action'=>'apply distro cmsn', 'details'=>'could not apply distro cmsn (distro not found)'));
	mlavu_query("INSERT INTO `[maindb]`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]')",
		array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$a_distro['id'], 'resellername'=>$a_distro['username'], 'dataname'=>$a_bill_status['dataname'], 'action'=>'apply distro cmsn', 'details'=>'could not apply distro cmsn (distro not found)'));
}

// checks for good standing and/or bad survey response time and returns TRUE or FALSE
// affects the invoice and applies logs if FALSE
function award_cmsn_for_bills_check_good_standing($a_distro, $a_bill_status, $a_restaurant) {
	global $maindb;

	$s_distro_good_standing = distro_in_good_standing_with_client($a_distro, '', $a_bill_status['dataname']);

	if (in_array($s_distro_good_standing, array('bad_no_distro', 'bad_restaurant', '25days_old', 'non_cmsn_package')))
		return FALSE;
	return TRUE;

	if (!in_array($s_distro_good_standing, array('good','3months_old','did_not_answer_survey','bad_survey_response_time'))) {
		if (in_array($s_distro_good_standing, array('bad_interaction','bad_satisfaction'))) {
			mlavu_query("UPDATE `[maindb]`.`invoices` SET `distro_code_applied_to`='[distro_code]' WHERE `id`='[id]'",
				array("maindb"=>$maindb, "distro_code"=>$a_distro['username'], "id"=>$a_bill_status['id']));
			mlavu_query("INSERT INTO `[maindb]`.`billing_log` (`datetime`,`dataname`,`restaurantid`,`username`,`fullname`,`action`,`details`) VALUES ('[datetime]','[dataname]','[restaurantid]','[username]','[fullname]','[action]','[details]')",
				array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'dataname'=>$a_bill_status['dataname'], 'restaurantid'=>$a_restaurant['id'], 'username'=>'award_cmsn_for_bills.php', 'username'=>'award_cmsn_for_bills.php', 'action'=>'apply distro cmsn', 'details'=>'could not apply distro cmsn ('.$s_distro_good_standing.')'));
			mlavu_query("INSERT INTO `[maindb]`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]')",
				array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$a_distro['id'], 'resellername'=>$a_distro['username'], 'dataname'=>$a_bill_status['dataname'], 'action'=>'apply distro cmsn', 'details'=>'could not apply distro cmsn ('.$s_distro_good_standing.')'));
		}
		return FALSE;
	}

	return TRUE;
}

// actually gives the cmsn to the distributor
// returns FALSE or TRUE
function award_cmsn_for_bills_give_cmsn($f_new_distro_cmsn, $f_old_distro_cmsn, $f_cmsn_to_award, $distro_code, $distro_id, $invoice_id, $s_dataname, $i_restaurant_id) {
	global $maindb;
	global $b_dont_assign_cmsn_just_testing;
	$conn = ConnectionHub::getConn('poslavu');
	$dbapi = $conn->getDBAPI();

	if (isset($b_dont_assign_cmsn_just_testing) && $b_dont_assign_cmsn_just_testing == TRUE) {
		$a_restaurants = $dbapi->getAllInTable('restaurants', array('data_name'=>$s_dataname), TRUE);
		$a_package = find_package_status($a_restaurants[0]['id']);
		$i_package = $a_package['package'];
		$a_invoices = $dbapi->getAllInTable('invoices', array('id'=>$invoice_id), TRUE);
		$i_amount = $a_invoices[0]['amount'];
		$a_insertvars = array('setting'=>'find_sum_of_distro_cmsn', 'type'=>'testing', 'value'=>$distro_code, 'value2'=>$f_cmsn_to_award, 'value3'=>"($s_dataname,$i_package,$i_amount)");
		$s_insert_clause = $dbapi->arrayToInsertClause($a_insertvars);
		$s_query_string = "INSERT INTO `poslavu_17edison_db`.`config` $s_insert_clause";
		mlavu_query($s_query_string, $a_insertvars);
		//return;
	}

	$s_query_string = "UPDATE `[maindb]`.`invoices` SET `distro_cmsn_applied`='[f_cmsn_to_award]',`distro_code_applied_to`='[distro_code]' WHERE `id`='[id]'";
	$a_query_vars = array("maindb"=>$maindb, "f_cmsn_to_award"=>$f_cmsn_to_award, "distro_code"=>$distro_code, "id"=>$invoice_id);
	mlavu_query($s_query_string, $a_query_vars);
	if ($conn->affectedRows() == 0) {
		error_log("billing: could not update invoice to award cmsn (invoice id ".$invoice_id.")");
		error_log($dbapi->drawQuery($s_query_string, $a_query_vars));
		return FALSE;
	}

	$s_query_string = "UPDATE `[maindb]`.`resellers` SET `cmsn`='[new_cmsn]' WHERE `username`='[distro_code]'";
	$a_query_vars = array("maindb"=>$maindb, "new_cmsn"=>$f_new_distro_cmsn, "distro_code"=>$distro_code);
	mlavu_query($s_query_string, $a_query_vars);
	if ($conn->affectedRows() == 0) {
		mlavu_query("UPDATE `[maindb]`.`invoices` SET `distro_cmsn_applied`='0',`distro_code_applied_to`='' WHERE `id`='[id]'",
			array("maindb"=>$maindb, "id"=>$invoice_id));
		error_log("billing: could not award cmsn to distro (distro code ".$distro_code.", invoice id ".$invoice_id.")");
		error_log($dbapi->drawQuery($s_query_string, $a_query_vars));
		return FALSE;
	}

	mlavu_query("INSERT INTO `[maindb]`.`billing_log` (`datetime`,`dataname`,`restaurantid`,`username`,`fullname`,`action`,`details`,`billid`,`target`) VALUES ('[datetime]','[dataname]','[restaurantid]','[username]','[fullname]','[action]','[details]','[billid]','[target]')",
		array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'dataname'=>$s_dataname, 'restaurantid'=>$i_restaurant_id, 'username'=>'award_cmsn_for_bills.php', 'fullname'=>'award_cmsn_for_bills.php', 'action'=>'apply distro cmsn', 'details'=>"invoice id {$invoice_id}: applied {$f_cmsn_to_award} cmsn to distro {$distro_code}", 'billid' => $invoice_id, 'target' => $f_cmsn_to_award));
	$s_query_string = "INSERT INTO `[maindb]`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`,`cmsn_applied`,`invoiceid`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]','[cmsn_applied]','[invoiceid]')";
	$a_query_vars = array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$distro_id, 'resellername'=>$distro_code, 'dataname'=>$s_dataname, 'action'=>'apply distro cmsn', 'details'=>'applied cmsn to distro '.$distro_code, 'cmsn_applied'=>$f_cmsn_to_award, 'invoiceid'=>$invoice_id);
	$query = mlavu_query($s_query_string, $a_query_vars);
	if ($query === FALSE)
		mail("tom@lavu.com", "failed to insert distro billing log", "{$s_query_string}\n\n".print_r($a_query_vars,TRUE));

	return TRUE;
}

?>
