<?php
	if(!isset($p_area))
	{
		exit();
	}

	//------------------------- GET UNCAPTURED CODE BLOCK START ------------------------------------//
	// Determine if we want to collect uncaptured amounts //
	$get_uncaptured_enabled = false;
	if(isset($dataname))
	{
		/*$uc_dn_list = array("17edison","jaba","good_earth_foo","caffe_dolci","canteen","jupeter_enterp","beverly_hills_1","oblong_cafe","dove_island_lo1","pushcart_coffe1","pilar_cuban_ea","the_little_cra1","on_call","hillbilly_tea","hojas_premium_","lemonade_glute","cafesito1","ashish_enterpr1","ethical_bean","bad_art_bistro","sas_yofi_");
		
		if(in_array($dataname,$uc_dn_list))
		{
			$get_uncaptured_enabled = true;
			
			if(isset($_GET['testps']))
			{
				if(isset($status_read))
				{
					foreach($status_read as $skey => $sval) echo $skey . " = " . $sval . "<br>";
				}
			}
		}*/

		if(isset($status_read) && isset($status_read['uncaptured_amount']) && is_numeric($status_read['uncaptured_amount']))
		{
			$uncaptured_amount = $status_read['uncaptured_amount'] * 1;
			$first_uncaptured = $status_read['first_uncaptured'];
			
			if($uncaptured_amount > 0 && $uncaptured_amount < 100)
			{
				$get_uncaptured_enabled = true;
			}
			if($first_uncaptured >= "2014-06-01" && $first_uncaptured < "2014-08-15")
			{
				$get_uncaptured_enabled = true;
			}
			
		}
		for($i=0; $i<count($payments); $i++)
		{
			$pay_read = $payments[$i][6];
			if(substr($pay_read['batch_action'],0,7)=="used to" || substr($pay_read['batch_action'],0,10)=="covered by")
			{
				$get_uncaptured_enabled = true;
			}
			else if(isset($pay_read['batch_manual_collect']) && $pay_read['batch_manual_collect']=="collection_enabled") // Manually Enabled
			{
				$get_uncaptured_enabled = true;
			}
		}
	}
	//------------------------- GET UNCAPTURED CODE BLOCK END ------------------------------------//

	if(!function_exists("generate_change_custom_payment_button"))
	{
		function generate_change_custom_payment_button($payment_id) {

			global $b_has_technical;
			if (!$b_has_technical)
				return "";

			$form_id = 'change_payment_'.$payment_id;
			return '<form id="'.$form_id.'" method="POST">
				<input type="hidden" name="payment_id" value="'.$payment_id.'" />
				<input type="hidden" name="bc_reason" />
				<input type="hidden" name="new_amount" />
				<input type="hidden" name="change_custom_payment_amount" value="1" />
				<input type="button" onclick="
					if (confirm(\'Are you sure you dont just want to make a payment of a (negative/positive) amount?\')) {
						var amount = prompt(\'How many dollars should this payment be worth?\');
						if (amount !== null && /^[\d\.]+$/.test(amount)) {
							var reason = prompt(\'Please give a reason for this change:\');
							if (reason !== null && reason != \'\') {
								$(\'#'.$form_id.'\').find(\'input[name=bc_reason]\').val(reason);
								$(\'#'.$form_id.'\').find(\'input[name=new_amount]\').val(amount);
								$(\'#'.$form_id.'\').submit();
							} else {
								alert(\'Please enter a reason\');
							}
						} else {
							alert(\'Please enter a valid amount\');
						}
					}" value="Change Payment Amount" />
			</form>';
		}
	}

	global $b_has_technical;

	$total_credits = array();
	$credits = array();
	$credits_left_by_index = array();
	$track_display_of_credits = array();

	$paid_last_two_weeks = 0;
	$last_payment_made = "";
	$paid_last_four_weeks = 0;
	$two_weeks_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 15,date("Y")));
	$four_weeks_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d") - 31,date("Y")));

	$paystr = "";
	$paystr .= "<u>Payment List:</u><br>";
	$paystr .= "<table>";
	$payview_js = "";
	$payment_amounts = array();
	$payments_applied = array();
	$payments_code_array = array();
	for($i=0; $i<count($payments); $i++)
	{
		$this_paystr = "";
		$payments_applied[$i] = 0;
		$payment_date = $payments[$i][0];
		$payment_amount = $payments[$i][1];
		$payment_amounts[] = $payment_amount;
		$ptval_selected = $payments[$i][2];
		$payment_approved = $payments[$i][3];
		$payment_id = $payments[$i][4];
		$payment_xtype = $payments[$i][5];

		$amount_color = "#999999";
		if ($payment_xtype=="credit") {
			if ($payment_approved=="1") {
				$amount_color = "#556699";
				if($payment_date >= $two_weeks_ago) $paid_last_two_weeks -= $payment_amount * 1;
				if($payment_date >= $four_weeks_ago) $paid_last_four_weeks -= $payment_amount * 1;
			}
			$rowstyle = "style='color:#556699;'";
			$amount_style = "style='color:".$amount_color."; padding:1px 95px 1px 1px;'";
			$label = "Credit";
		} else {
			if ($payment_approved=="1") {
				$amount_color = "#004400";
				if($payment_date >= $two_weeks_ago) $paid_last_two_weeks += $payment_amount * 1;
				if($payment_date >= $four_weeks_ago) $paid_last_four_weeks += $payment_amount * 1;
				if($last_payment_made=="" || $payment_date > $last_payment_made) $last_payment_made = $payment_date;
			}
			$rowstyle = "style='color:#004400'";
			$amount_style = "style='color:".$amount_color."'";
			$label = "Payment";
		}

		if($p_area=="billing_info")
		{
			if($payment_approved) $show_payment_approved = "<font style='color:#008800'>(Approved)</font>";
			else $show_payment_approved = "<font style='color:#880000'>(Declined)</font>";

			$pay_read = $payments[$i][6];

			$payinfo = "<div style='width:380px; height:250px; overflow:auto'>";
			$payinfo .= "<table>";
			$payinfo .= "<tr><td align='right'>Date Processed:</td><td>$payment_date</td></tr>";
			$payinfo .= "<tr><td align='right'>Amount:</td><td>$payment_amount $show_payment_approved</td></tr>";
			$payinfo .= "<tr><td align='right'>Type:</td><td>$payment_xtype</td></tr>";
			$lastFour='';
			if(isset($pay_read['response']))
			{
				$lastFour= $pay_read['x_account_number'];
				$payinfo .= "<tr><td align='right'>Card Type:</td><td>".$pay_read['x_card_type']."</td></tr>";
				$payinfo .= "<tr><td align='right'>Last Four:</td><td>".$pay_read['x_account_number']."</td></tr>";
				$payinfo .= "<tr><td align='right' valign='top'>SubscriptionId:</td><td>";

				$subid = $pay_read['x_subscriptionid'];
				$paymentid = $pay_read['id'];
				if($subid!="")
				{
					$payinfo .= "<div id='subscription_container_$i'>";
					$payinfo .= "<a style='cursor:pointer' onclick='show_payment_info_$i(2)'>$subid</a>";
					$payinfo .= "</div>";
				}
				else
					$payinfo .= "&nbsp;";

				$payinfo .= "</td></tr>";
				$payinfo .= "<tr><td align='right'>Match Type:</td><td>".$pay_read['match_type']."</td></tr>";
				$payinfo .= "<tr><td align='right'>Match Notes:</td><td>".$pay_read['match_notes']."</td></tr>";
				$payinfo .= "<tr><td align='right' valign='top'>More Info:</td><td><input type='button' value='View' onclick='show_payment_info_$i(1)' /></td></tr>";


				$payinfo .= "<tr><td align='right' valign='top' colspan='2'";
				$payinfo .= "<div id='payment_move_container_$i'>";
				$payinfo .= "<a style='cursor:pointer' onclick='show_payment_info_$i(3)'>(move this payment)</a>";
				$payinfo .= "</div>";
				$payinfo .= "</td></tr>";

				$ismanage = (strpos($_SERVER['REQUEST_URI'], '/manage/') !== FALSE) ? "yes" : "no";
				$payview_js .= "
				function show_payment_info_$i(sptype) {
					if(sptype==1) {
						alert('".str_replace("\n","\\n",str_replace("'","&apos;",$pay_read['response']))."')
					} else if(sptype==2) {
						document.getElementById('subscription_container_$i').innerHTML = 'loading...';
						ajax_get_response('/sa_cp/billing/procareas/dialog_requests.php','mode=subscription_match&containerid=subscription_container_$i&callback=set_package_callback_$i&paymentid=$paymentid&subscriptionid=$subid&ismanage=$ismanage','set_package_callback_$i');
					} else if(sptype==3) {
						document.getElementById('payment_move_container_$i').innerHTML = 'loading...';
						ajax_get_response('/sa_cp/billing/procareas/dialog_requests.php','mode=move_payment&containerid=payment_move_container_$i&callback=move_payment_callback_$i&paymentid=$paymentid&subscriptionid=$subid&ismanage=$ismanage','move_payment_callback_$i');
					} else {
						alert('unknown sptype: ' + sptype);
					}
				}

				function set_package_callback_$i(response) {
					parse_ajax_response('subscription_container_$i',response);
				}

				function move_payment_callback_$i(response) {
					parse_ajax_response('payment_move_container_$i',response);
				}";
				$payview_js = str_replace(array("\n\r", "\r\n", "\r"), "\n", $payview_js);
			}
			$payinfo .= "</table>";
			$payinfo .= "</div>";

			$onclick_str = " onclick='show_info2(\"".str_replace("'","\\\"",str_replace("\"","\\\"",$payinfo))."\")'";
		}
		else {
			$pay_read = $payments[$i][6];
			$lastFour='';
			if(isset($pay_read['response']))
			{
				$lastFour= $pay_read['x_account_number'];
			}
			$onclick_str = "";
		}
		if (!$b_has_technical)
			$onclick_str = "";

		$this_paystr .= "<tr style='font-family:Arial, Helvetica, sans-serif; font-size:13px;'>";
		$this_paystr .= "<td $rowstyle><nobr>$label " . ($i + 1) . "&nbsp;</nobr></td>";
		$this_paystr .= "<td $rowstyle><nobr><font size='-1' color='#555555'>ID: ".$pid_prefix.$payment_id."</font>&nbsp;</nobr></td>";
		if($lavu_admin && $payment_id > 0 && isset($pay_read) && isset($pay_read['x_trans_id']) && trim($pay_read['x_trans_id'])!="") {
			$this_paystr .= "<td $rowstyle><nobr><font size='-1' color='#555555'>";
			$this_paystr .= "TransId: " . $pay_read['x_trans_id'];
			$this_paystr .= "</font>&nbsp;</nobr></td>";
		}
		$this_paystr .= "<td $rowstyle><nobr>$payment_date</nobr></td>";
		$this_paystr .= "<td $rowstyle><nobr>$lastFour</nobr></td>";
		$this_paystr .= "<td $amount_style align='right'".$onclick_str."><nobr><b>$" . number_format($payment_amount, 2) . "</b><payment_row_credit_$i></nobr></td>";
		//$this_paystr .= "<td $rowstyle><nobr><payment_row_credit_$i></nobr></td>";

		if($payment_xtype=="credit" && $payment_approved=="1")
		{
			if(!isset($total_credits[$ptval_selected]))
				$total_credits[$ptval_selected] = 0;
			$total_credits[$ptval_selected] += $payment_amount * 1;
			$credits[] = array($i, $payment_amount);
			$credits_left_by_index[$i] = $payment_amount;
			$track_display_of_credits[$i] = array(0, 0, -1); // display applied, last bill id, last display index
		}

		// draw the paytype (eg "(license)","(general)","(hosting)","(other)")
		// if a lavu admin and the payment is from the payment responses table, give the option to change it's payment type
		$this_paystr .= "<td $rowstyle>";
		if ($lavu_admin && $payment_id > 0 && $b_has_technical) {

			// draw the option to change the payment type for the lavu admin
			$this_paystr .= "<select onchange='return make_billing_change(this,\"$ptval_selected\",\"Change Payment Type\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&payment_id=$payment_id&set_payment_type=\" + this.value,\"\",\"\",\"".$payment_id."\")'>";
			for ($n = 0; $n < count($payment_type_list); $n++) {
				$ptval = $payment_type_list[$n];
				$this_paystr .= "<option value='$ptval'";
				if($ptval==$ptval_selected) $this_paystr .= " selected";
				$this_paystr .= ">".$ptval."</option>";
			}
			$this_paystr .= "</select>";
		} else {

			// draw the payment type
			$this_paystr .= "($ptval_selected)&nbsp;";
		}
		$this_paystr .= "</td>";

		$s_payment_xtype_string = ($lavu_admin ? "<td $rowstyle> $payment_xtype</td>" : "");
		$this_paystr .= "{$s_payment_xtype_string}<td $rowstyle>";
		
		if(isset($get_uncaptured_enabled) && $get_uncaptured_enabled)
		{
			//------------------------- GET UNCAPTURED CODE BLOCK START ------------------------------------//
			// Display uncaptured transactions to the customer, as well as if they have already been covered by a new payment //
			$batch_status = "";
			$batch_action = "";
			if(isset($payments[$i][6]['batch_status'])) $batch_status = $payments[$i][6]['batch_status'];
			if(isset($payments[$i][6]['batch_action'])) $batch_action = $payments[$i][6]['batch_action'];
			
			if($batch_status=="N" && $payment_date >= "2014-02-01" && $payment_date < "2014-08-15")
			{
				if($batch_action=="")
				{
					$this_paystr .= "<div style='border:solid 1px #880000; padding:4px; background-color:#ffdddd; text-align:center'><font color='#440000'> payment not drafted</font></div>";
				}
				else
				{
					$batch_action_description = "Covered by Later Payment";
					$batch_action_parts = explode("covered by payment",$batch_action);
					if(count($batch_action_parts) > 1)
					{
						$batch_action_parts = explode("rsp:",$batch_action_parts[1]);
						$batch_action_description = "Covered by Payment #" . trim($batch_action_parts[0]);
					}
					else
					{
						if(strpos($batch_action,"covered")!==false && strpos($batch_action,"rsp:")===false)
						{
							$batch_action_description = $batch_action;
						}
					}
					$this_paystr .= "<div style='border:solid 1px #008800; padding:4px; background-color:#ddffdd; text-align:center'><font color='#004400'> $batch_action_description</font></div>";
				}
			}
			else
			{
				$this_paystr .= ($payment_approved=="1" ? "<font color='#008800'> approved</font>" : "<font color='#cc0000'> declined</font>");
			}
			//------------------------- GET UNCAPTURED CODE BLOCK END ------------------------------------//
		}
		else
		{
			$this_paystr .= ($payment_approved=="1" ? "<font color='#008800'> approved</font>" : "<font color='#cc0000'> declined</font>");
		}
		
		if ($lavu_admin && strtolower($payment_xtype)=="misc") { // it's a custom payment
			$this_paystr .= generate_change_custom_payment_button($payment_id);
		}
		$this_paystr .= "</td>";
		$this_paystr .= "</tr>";

		$paystr .= $this_paystr;
		$payments_code_array[] = $this_paystr;
	}
	$paystr .= "</table>";

	if($payview_js!="")
	{
		$output .= "<script language='javascript'>".$payview_js."</script>";
	}

	if ($lavu_admin && $b_has_technical) {
		//--------------------------------------- Used when create a custom payment or invoice --------------------------------//
		$custom_payment_date_options = "";
		$custom_payment_dates_used = array();
		$invoice_day_of_the_month = date('n', strtotime($prop_parts['first_hosting']));
		for($i=0; $i<count($billable); $i++)
		{
			$dt = $billable[$i][0];
			if (isset($custom_payment_dates_used[$dt])) continue;
			else $custom_payment_dates_used[$dt] = 1;
			$custom_payment_date_options = "<option value='$dt'>$dt</option>" . $custom_payment_date_options;
		}

		$custom_payment_date_options = $custom_payment_date_options . "<option value='' disabled>Upcoming Bill Dates ----</option>";
		for($m=1; $m<=6; $m++)
		{
			$ndt = date("Y-m-d",mktime(0,0,0,date("m")+$m,$invoice_day_of_the_month,date("Y")));
			$custom_payment_date_options = $custom_payment_date_options . "<option value='$ndt'>$ndt</option>";
		}

		$dt = date("Y-m-d");
		$custom_payment_date_options = "<option value='$dt'>$dt</option><option value='' disabled>Past Bill Dates ----</option>" . $custom_payment_date_options;

		global $custom_payment_string;
		$custom_payment_string .= "<span class='info_label'>Add Custom Payment:</span> <select name='custom_payment_type' id='custom_payment_type'><option value='Misc'>Misc</option><option value='Paypal'>Paypal</option><option value='Check'>Check</option><option value='Adjust'>Adjust</option></select>";
		$custom_payment_string .= "<select name='custom_payment_date' id='custom_payment_date'>$custom_payment_date_options</select>";
		$custom_payment_string .= " <input type='text' name='custom_payment_amount' id='custom_payment_amount' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"\",\"Add Custom Payment\",\"/../../sa_cp/billing/index.php?dn=$dataname&package=$package&signup_status=$signup_status&set_custom_payment_type=\" + document.getElementById(\"custom_payment_type\").value + \"&set_custom_payment_date=\" + document.getElementById(\"custom_payment_date\").value + \"&set_custom_payment_amount=\" + document.getElementById(\"custom_payment_amount\").value,\"\",\"\",\"\")'><br>";
	}

	//echo "<br>CLBI: ".print_r($credits_left_by_index, true);

	//$tied_credits = array();
	$payments_with_credits = array();
	$tied_credit_info = array(); // credit amount, credit (index), applied to payment (index), amount applied

	$credits_left_arr = $total_credits;
	for($i=0; $i<count($payments); $i++)
	{
		$pamount = $payments[$i][1];
		$ptype = $payments[$i][2];
		$pxtype = $payments[$i][5];
		$papproved = $payments[$i][3];

		if(isset($credits_left_arr[$ptype]))
			$credits_left = $credits_left_arr[$ptype];
		else
			$credits_left = 0;
		if($pxtype=="auth_capture" && $papproved=="1")
		{
			$rpstr = "";
			$original_amount = $pamount;
			$applied_amount = 0;
			if($pamount <= $credits_left)
			{
				$credits_left -= $pamount;
				$pamount = 0;
				$payments[$i][1] = $pamount;
				$rpstr = "&nbsp;<font color='#556699'>-$".number_format($original_amount,2)." = <b>$0.00</b></font>";
				$applied_amount = $original_amount;
				//$rpstr = "Payment " . ($i + 1) . " ($".number_format($original_amount,2).") canceled out by credits";
			}
			else if(number_format($credits_left,2) > 0)
			{
				$pamount -= $credits_left;
				$subtracted = $credits_left;
				$credits_left = 0;
				$payments[$i][1] = $pamount;
				$rpstr = "&nbsp;<font color='#556699'>-$".number_format($subtracted,2)." = <b>$".number_format($pamount,2)."</b></font>";
				$applied_amount = $subtracted;
				//$rpstr = "Payment " . ($i + 1) . " reduced from $".number_format($original_amount,2)." to $".number_format($pamount,2)." by credits<br>";
			}

			if ($applied_amount > 0) {
				for ($cr = 0; $cr < count($credits); $cr++) {
					$this_credit = $credits[$cr];
					$cr_i = $this_credit[0];
					if (number_format($credits_left_by_index[$cr_i], 2) > 0) {
						$this_applied = $applied_amount;
						if ($credits_left_by_index[$cr_i] < $applied_amount) $this_applied = $credit_left;
						$applied_amount -= $this_applied;
						$credits_left_by_index[$cr_i] -= $this_applied;
						if (!in_array($i, $payments_with_credits)) $payments_with_credits[] = $i;
						$this_credit[2] = $i;
						$this_credit[3] = $this_applied;
						//$tied_credits[] = $cr_i;
						$tied_credit_info[] = $this_credit;
						$payments_code_array[$cr_i] = str_replace("color:#556699;", "color:#797e8d; font-size:12px;", $payments_code_array[$cr_i]);
						if (number_format($applied_amount, 2) < 0.01) break;
					}
				}
			}
			if($rpstr!="")
			{
				$paystr = str_replace("<payment_row_credit_$i>",$rpstr,$paystr);
				$payments_code_array[$i] = str_replace("<payment_row_credit_$i>",$rpstr,$payments_code_array[$i]);
			}
		}
		$credits_left_arr[$ptype] = $credits_left;
	}

?>
