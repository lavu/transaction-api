<?php
	require_once(dirname(__FILE__)."/../../../../manage/login.php");
	if (!account_loggedin()) {
		echo json_encode(array('success'=>'0', 'error_msg'=>'You must be logged in to manage.'));
		exit();
	}
	if (!can_access('technical')) {
		echo json_encode(array('success'=>'0', 'error_msg'=>'You don\'t have permission to edit invoice parts.'));
		exit();
	}

	require_once(dirname(__FILE__)."/../../procareas/monthly_parts.php");
	require_once(dirname(__FILE__)."/../../procareas/invoice_parts.php");
	require_once(dirname(__FILE__)."/../../../../cp/resources/lavuquery.php");
	require_once(dirname(__FILE__)."/../../../../cp/resources/json.php");
	require_once('/home/poslavu/private_html/salesforceCredentials.php');
	require_once(dirname(__FILE__).'/../../../../../register/lib/salesforce/SalesforceLogin.php');
	require_once(dirname(__FILE__).'/../../../../../register/lib/salesforce/SalesforceQuote.php');
	require_once(dirname(__FILE__).'/../../../../../register/lib/salesforce/SalesforceQuoteLineItem.php');


	class BillingEditInvoiceParts {

		public function getInvoiceParts() {

			$invoice_id = $_POST['invoice_id'];
			$a_invoices = InvoiceParts::getInvoicePartsForInvoice($invoice_id);
			echo json_encode(array('success'=>'1', 'success_msg'=>$a_invoices));
		}

		public function getPossibleInvoiceTypes() {

			$a_monthly_part_descriptions = $this->load_description_types();
			$a_invoice_descriptions = array();
			foreach($a_monthly_part_descriptions as $o_description)
				$a_invoice_descriptions[] = $o_description->name;
			$a_invoice_types = array_merge(array('Hosting', 'Other'), $a_invoice_descriptions);
			$a_retval = array();
			foreach($a_invoice_types as $s_invoice_type)
				$a_retval[] = array('value'=>$s_invoice_type, 'printed'=>$s_invoice_type);
			echo json_encode(array('success'=>'1', 'success_msg'=>$a_retval));
		}

		public function getPossibleHostingDates() {

			$s_dataname = $_POST['dataname'];
			$s_current_date = $_POST['current_date'];
			$a_invoices = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('invoices', array('dataname'=>$s_dataname, 'start'=>date('Y-m-01',strtotime($s_current_date.' -5 months')), 'end'=>$s_current_date), TRUE, array(
				'whereclause'=>"WHERE `dataname`='[dataname]' AND `due_date` >= '[start]' AND `due_date` <= '[end]'", 'selectclause'=>'`due_date`'));
			for ($i = 1; $i < 6; $i++)
				$a_invoices[] = array('due_date'=>date("Y-m-d",strtotime("{$s_current_date} +{$i} months")));
			$a_retval = array();
			foreach($a_invoices as $a_invoice)
				$a_retval[] = array('value'=>$a_invoice['due_date'], 'printed'=>$a_invoice['due_date']);
			echo json_encode(array('success'=>'1', 'success_msg'=>$a_retval));
		}

		public function getPossibleMonthlyParts() {
			$s_dataname = $_POST['dataname'];
			$s_current_date = $_POST['current_date'];
			$a_monthly_parts = NULL;
			MonthlyInvoiceParts::load_monthly_parts($s_dataname, $s_current_date, $s_current_date, $a_monthly_parts);
			MonthlyInvoiceParts::get_monthly_parts_that_apply_to_date($a_monthly_parts, $s_current_date, $a_monthly_parts);
			$a_retval = array(array('value'=>'0', 'printed'=>'Standard Monthly Billing'));
			foreach($a_monthly_parts as $k=>$o_monthly_part) {
				$a_retval[] = array('value'=>$o_monthly_part->db_values->id, 'printed'=>$o_monthly_part->db_values->description);
			}
			echo json_encode(array('success'=>'1', 'success_msg'=>$a_retval));
		}

		public function createQuoteLineItems($args) {
			$retval = array('success' => false, 'lineitems' => array(), 'error_msg' => '');
			if (empty($args['dataname']) || empty($args['invoice_id']) || empty($args['salesforce_quote_id'])) return $retval;

			$num_invoice_parts_created = 0;

			echo "Looking up quote in Salesforce (could take 10+ seconds)<br>";

			$sf_login = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
			$sf_quote_util = new SalesforceQuote($sf_login);
			$quote = $sf_quote_util->get_properties(array('Name', 'Id', 'Subtotal', 'TotalPrice', 'Tax_Amount__c', 'Tax_Rate__c', 'ShippingHandling', 'GrandTotal', 'LineItemCount'), $args['salesforce_quote_id']);

			if (!empty($quote)) {
				echo "...found Quote ID {$quote['Id']} in Salesforce<br>";

				$sf_quotelineitem_util = new SalesforceQuoteLineItem($sf_login);
				$quotelineitems = $sf_quotelineitem_util->getQuoteLineItems($args['salesforce_quote_id'], $sf_login);
				echo "...found Quote ". count($quotelineitems) ." line items<br>";

				// Manually add a line item for Shipping and/or Tax, to ensure totals are correct
				if (!empty($quote['ShippingHandling'])) {
					$quotelineitems[] = array('ProductName' => 'Shipping', 'Id' => '', 'TotalPrice' => $quote['ShippingHandling']);
					echo "...manually created shipping line item for ". $quote['ShippingHandling'] ."<br>";
				}
				if (!empty($quote['Tax_Amount__c'])) {
						$quotelineitems[] = array('ProductName' => 'Tax '. $quote['Tax_Rate__c'] .'%', 'Id' => '', 'TotalPrice' => $quote['Tax_Amount__c']);
				}

				foreach ($quotelineitems as $quotelineitem) {
					$invoice_part_dbvals = array(
						'id' => 0,
						'invoice_id' => $args['invoice_id'],
						'monthly_parts_id' => 0,
						'dataname' => $args['dataname'],
						'description' => $quotelineitem['ProductName'],
						'due_date' => $args['due_date'],
						'amount' => $quotelineitem['TotalPrice'],
						'pro_rate' => false,
						'waived' => false,
						'promo' => false,
						'bundled' => false,
						'standard_monthly_billing' => false,
						'json_history' => false,
						'distro_points' => 0,
						'distro_cmsn' => 0.0,
						'package' => $args['package'],
						'salesforce_quote_id' => $args['salesforce_quote_id'],
						'salesforce_quotelineitem_id' => $quotelineitem['Id'],
					);
					$new_invoice_part = new InvoiceParts(0, $invoice_part_dbvals);
					$new_invoice_part->save_to_db();
					$num_invoice_parts_created++;
				}
				echo "...created {$num_invoice_parts_created} invoice_parts<br>";

				MonthlyInvoiceParts::UpdateInvoiceByParts($args['invoice_id']); // updates the amount on the invoice
				echo "...updated total amount for invoice ID {$args['invoice_id']}<br>";
			}
			else {
				$retval['error_msg'] = 'Unable to get info for Quote ID '. $args['salesforce_quote_id'];
			}

			return $retval;
		}

		private function load_description_types() {

			return json_decode(file_get_contents(dirname(__FILE__)."/monthly_parts_descriptions.json"), FALSE);
		}

		private function errorAndDie($error_msg) {
			error_log(__FILE__ ." got error ". $error_msg);
			echo $error_msg ."\n";
			die($error_msg);
		}
	}

	class BillingEditMonthlyParts {
		public function getPossibleMonthlyDescriptions() {

			// get the possible invoice types
			global $o_tester;
			ob_start();
			$o_tester->getPossibleInvoiceTypes();
			$o_invoice_types = json_decode(ob_get_contents(), FALSE);
			ob_end_clean();

			// eliminate hosting types
			foreach($o_invoice_types->success_msg as $k=>$o_invoice_type)
				if (strtolower($o_invoice_type->value) == "hosting")
					unset($o_invoice_types->success_msg[$k]);
			echo json_encode($o_invoice_types);
		}
	}

	$o_tester = new BillingEditInvoiceParts();
	$o_monthly_tester = new BillingEditMonthlyParts();
	if (isset($_POST['action']) && $_POST['action'] == 'createQuoteLineItems')
		$o_tester->createQuoteLineItems($_POST);
	else if (isset($_POST['action']) && !isset($_GET['monthly']) && method_exists($o_tester, $_POST['action']))
		call_user_method($_POST['action'], $o_tester);
	else if (isset($_POST['action']) && isset($_GET['monthly']) && method_exists($o_monthly_tester, $_POST['action']))
		call_user_method($_POST['action'], $o_monthly_tester);
	else if (isset($_POST['action']))
		echo json_encode(array('success'=>'0', 'error_msg'=>"The action ".$_POST['action']." isn't recognized."));
	else
		echo json_encode(array('success'=>'0', 'error_msg'=>"'Action' must be set."));

?>
