<?php

	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";
	

	// looks for pending invoices from today and draws them
	if(!function_exists("draw_pending_invoices"))
	{
		function draw_pending_invoices($forced_dataname = '') {
			global $maindb;
			global $dataname;
			global $a_bill_ids_drawn;
			
			$s_retval = "";
			$s_dataname = "";
			$amount_color = "#004400";
			$rowstyle = "style='color:#004400'";
			$amount_style = "style='color:".$amount_color."'";
			
			if ($forced_dataname !== '') {
				$dataname = $forced_dataname;
			} else {
				if (function_exists("admin_info")) {
					$s_dataname = admin_info("dataname");
				} else if (isset($dataname)) {
					$s_dataname = $dataname;
				}
			}
			
			// get the invoices
			$s_other_invoices_query_string = "SELECT * FROM `[maindb]`.`invoices` WHERE `due_date`='[due_date]' AND `dataname`='[dataname]'";
			$a_other_invoices_query_vars = array("maindb"=>$maindb, "due_date"=>date("Y-m-d"), "dataname"=>$s_dataname);
			$other_invoices_query = mlavu_query($s_other_invoices_query_string, $a_other_invoices_query_vars);
			$a_other_invoices = array();
			if ($other_invoices_query) {
				while ($row = mysqli_fetch_assoc($other_invoices_query)) {
					if (isset($a_bill_ids_drawn))
						if (is_array($a_bill_ids_drawn))
							if (in_array((int)$row['id'], $a_bill_ids_drawn))
								continue;
					$a_other_invoices[] = $row;
				}
			}
			
			$b_other_invoices_exist = FALSE;
			if (count($a_other_invoices) > 0)
				$b_other_invoices = TRUE;
			
			// print the invoices
			if ($b_other_invoices)
				$s_retval .= "<table><tr><td class='section_header' valign='top' align='right'><font size='-1'>Pending Invoices:</font></td><td valign='top' style='border-left:1px solid #888888; padding:1px 10px 1px 25px;'><table>";
			
			foreach($a_other_invoices as $i=>$a_other_invoice) {
				$s_retval .= "<tr style='font-family:Arial, Helvetica, sans-serif; font-size:13px;'>";
				$s_retval .= "<td $rowstyle><nobr>Invoice " . ($i + 1) . "&nbsp;</nobr></td>";
				$s_retval .= "<td $rowstyle><nobr><font size='-1' color='#555555'>ID: ".$a_other_invoice['id']."</font>&nbsp;</nobr></td>";
				$s_retval .= "<td $rowstyle><nobr>".$a_other_invoice['due_date']."</nobr></td>";
				$s_retval .= "<td $amount_style align='right'><nobr><b>$" . number_format($a_other_invoice['amount'], 2) . "</b><payment_row_credit_$i></nobr></td>";
				$s_retval .= "<td $rowstyle>(".$a_other_invoice['type'].")</td>";
				$s_retval .= "<td $rowstyle><font color='#008800'> pending</font></td>";
			}
			if ($b_other_invoices)
				$s_retval .= "</table></td></tr><tr><td colspan='2'><hr style='height: 1px; margin: 2px 0px 2px 0px;'></td></tr></table>";
			return $s_retval;
		}
	}

	$other_payments = false;
	$other_payments_total = 0;
	if ($over_payment) {
		$output .= "<table><tr><td class='section_header' valign='top' align='right'><font size='-1'>Other Payments:</font></td><td valign='top' style='border-left:1px solid #888888; padding:1px 10px 1px 25px;'><table>";
		$output .= str_replace("$".number_format($over_payment[1], 2), "</b>(Out of<b> $".number_format($over_payment[1], 2)."</b>) <b>$".number_format($over_payment[2], 2), $payments_code_array[$over_payment[0]]);
		$other_payments = true;
		$other_payments_total += ($over_payment[2] * 1);
	}
	for($n=0; $n<count($payments); $n++) {
		if (!in_array($n + 1, $tied_payments)) {
			if (!$other_payments) $output .= "<table><tr><td class='section_header' valign='top' align='right'><font size='-1'>Other Payments:</font></td><td valign='top' style='border-left:1px solid #888888; padding:1px 10px 1px 25px;'><table>";
			$output .= $payments_code_array[$n];
			$other_payments = true;
			if(isset($payments[$n][1]) && isset($payments[$n][6]) && isset($payments[$n][6]['x_response_code']))
			{
				if($payments[$n][6]['x_response_code']=="1")
				{
					if($payments[$n][6]['x_type']=="auth_capture")
						$other_payments_total += $payments[$n][1];
					else if($payments[$n][6]['x_type']=="credit")
						$other_payments_total -= $payments[$n][1];
				}
			}
			if(isset($payments[$n][6]) && isset($payments[$n][6]['batch_action']) && substr($payments[$n][6]['batch_action'],0,7)=="used to")
			{
				$batch_action_parts = explode("rsp:",$payments[$n][6]['batch_action']);
				$payment_coverage_description = trim($batch_action_parts[0]);
				if($payment_coverage_description!="")
				{
					$payment_coverage_description = str_replace("used to cover","used to cover undrafted payment #",$payment_coverage_description);
					$payment_coverage_description = str_replace("payment payment","payment",$payment_coverage_description);
					$payment_coverage_description = ucwords($payment_coverage_description);
					$output .= "<tr><td>&nbsp;</td><td colspan='20'>$payment_coverage_description</td></tr>";
				}
			}
		}
	}
	if ($other_payments) $output .= "</table></td></tr><tr><td colspan='2'><hr style='height: 1px; margin:2px 0px 2px 0px;'></td></tr></table>";
	
	$output .= draw_pending_invoices($dataname);
?>