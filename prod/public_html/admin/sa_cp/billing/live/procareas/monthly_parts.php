<?php

	require_once(dirname(__FILE__)."/../../procareas/invoice_parts.php");
	require_once(dirname(__FILE__)."/../../../../cp/resources/json.php");

	// MonthlyInvoiceParts are meant to create a monthly part for every part to charge to the customer.
	// They are the monthly recurring charges that create new invoice parts.
	// Priority parts exist which take priority over regular parts. This is to create special pricing tiers between two times.
	class MonthlyInvoiceParts {

		// creates a montly invoice part
		// @$i_part_id: if > 0, loads the row from `poslavu_MAIN_db`.`monthly_parts` to fill this object with
		// @$a_db_row: if $i_part_id == 0 and not NULL, uses this row data to fill this object with
		function __construct($i_part_id = 0, $a_db_row = NULL) {
			MonthlyInvoiceParts::echo_function_name();

			$this->dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

			// create the history
			$o_history = InvoiceParts::create_standard_history();

			// create the object
			$this->check_for_previous_invoice_values = array();
			$this->db_values = new stdClass();
			$db = $this->db_values;
			$db->id = 0;
			$db->dataname = "";
			$db->description = "";
			$db->amount = 0.0;
			$db->pro_rate = FALSE;
			$db->waived = FALSE;
			$db->promo = FALSE;
			$db->bundled = FALSE;
			$db->distro_points = 0;
			$db->distro_cmsn = 0.0;
			$db->mp_json_history = LavuJson::json_encode($o_history);
			$db->mp_priority = 0.0;
			$db->mp_start_day = date("Y-m-d");
			$db->mp_end_day = date("Y-m-d");

			// fill in the object
			if ($i_part_id > 0) {

				// load from the database
				$a_db_rows = $this->dbapi->getAllInTable('monthly_parts', array('id'=>$i_part_id), TRUE);
				$a_db_row = array();
				if (count($a_db_rows) > 0)
					$a_db_row = $a_db_rows[0];
			}
			if (is_array($a_db_row)) {

				// load from the array
				foreach($a_db_row as $k=>$v)
					if (isset($this->db_values->$k))
						$this->db_values->$k = $v;
			}

			// enforce some types for some values
			$this->enforce_types();
		}

		// Supposed to be used to save after creation.
		public function save_to_db() {
			MonthlyInvoiceParts::echo_function_name();
			$a_update_vars = (array)$this->db_values;
			unset($a_update_vars['id']);
			$a_where_vars = array('dataname'=>$this->db_values->dataname, 'description'=>$this->db_values->description, 'id'=>$this->db_values->id);
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
			$dbapi->insertOrUpdate('poslavu_MAIN_db', 'monthly_parts', $a_update_vars, $a_where_vars);
		}

		// searches for invoice parts for the given invoice
		// if none exist and the invoice does, creates the invoice part
		// updates the date or invoice id, whichever is less reliable (defaults to the invoice id as the most reliable when not 0)
		// @return: TRUE if the invoice and invoice part exists, FALSE otherwise
		public function check_for_previous_invoice(&$s_date, $s_dataname, &$i_invoice_id, &$i_waived, $s_creation_details = "") {
			MonthlyInvoiceParts::echo_function_name();
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

			// check for previous values
			$s_loaded_val = "{$s_date}:{$s_dataname}:{$i_invoice_id}";
			if (isset($this->check_for_previous_invoice_values[$s_loaded_val]))
				return $this->check_for_previous_invoice_values[$s_loaded_val];

			// search for the invoice
			if ($i_invoice_id > 0) {
				$a_invoices = $dbapi->getAllInTable('invoices', array('id'=>$i_invoice_id, 'dataname'=>$s_dataname), TRUE);
			} else {
				$a_invoices = $dbapi->getAllInTable('invoices', array('due_date'=>$s_date, 'dataname'=>$s_dataname), TRUE);
			}

			// return if the invoice doesn't exist
			if (count($a_invoices) == 0) {
				$this->check_for_previous_invoice_values[$s_loaded_val] = FALSE;
				return FALSE;
			}

			// update the invoice id/date
			if ($i_invoice_id == 0)
				$i_invoice_id = $a_invoices[0]['id'];
			else
				$s_date = $a_invoices[0]['due_date'];

			// check for the invoice part
			$a_invoice_parts = $dbapi->getAllInTable('invoice_parts', array('invoice_id'=>$i_invoice_id), TRUE, array('whereclause'=>"WHERE `invoice_id`='[invoice_id]' AND `description` LIKE 'Hosting'"));
			if (count($a_invoice_parts) > 0) {
				$i_waived = 1;
				foreach($a_invoice_parts as $a_invoice_part)
					if ((int)$a_invoice_part['waived'] == 0)
						$i_waived = 0;
				return TRUE;
			}

			// create the invoice part
			$a_invoices['details'] = $s_creation_details;
			$this->create_invoice_part_for_existing_invoice($a_invoices[0]);
			return TRUE;
		}

		// create an invoice part for the given existing invoice
		// it's description will be equal to the existing invoice's type
		// @$a_invoice: the row from `invoices` in the db
		private function create_invoice_part_for_existing_invoice($a_invoice) {
			MonthlyInvoiceParts::echo_function_name();

			// create the invoice part
			$o_invoice_part = NULL;
			$this->_create_invoice_part($a_invoice['due_date'], $a_invoice['dataname'], $a_invoice['id'], FALSE, $o_invoice_part, FALSE, $a_invoice['package']);

			// update some values
			$o_invoice_part->db_values->monthly_parts_id = 0;
			$o_invoice_part->db_values->description = $a_invoice['type'];
			$o_invoice_part->db_values->amount = $a_invoice['amount'];
			$o_invoice_part->db_values->waived = $a_invoice['waived'];
			$o_invoice_part->db_values->distro_points = $a_invoice['distro_points'];
			$o_invoice_part->db_values->distro_cmsn = $a_invoice['distro_cmsn'];
			if (isset($a_invoice['details']) && $a_invoice['details'] != "")
				$o_invoice_part->db_values->details = $a_invoice['details'];
			if (trim(strtolower($a_invoice['type'])) == 'hosting')
				$o_invoice_part->db_values->standard_monthly_billing = 1;
			$o_invoice_part->db_values->package = $a_invoice['package'];

			$o_invoice_part->save_to_db();
		}

		// Creates an invioce part with this part as a template.
		// Should be called for this part once/month.
		// Creates a 'Hosting' invoice part for the current invoice at the current invoice amount if the part does not yet exist and the invoice is type 'Hosting'.
		// @$s_date: the date to create the invoice part for
		// @$s_dataname: the dataname of the account this invoice part should apply to
		// @$i_invoice_id: the id of the invoice this should apply to, autoset to the invoice on the day if 0
		// @$b_check_for_previous: if TRUE, checks for a 'hosting' invoice part for the given invoice and creates one if it doesn't yet exist
		// @$s_creation_details: the reason that the invoice part was created
		// @return: TRUE if this part's start day is <= $s_date and this part's end day >= $s_date, FALSE otherwise
		public function create_invoice_part($s_date, $s_dataname, $i_invoice_id = 0, $b_check_for_previous = TRUE, $s_creation_details = "Automatically generated from monthly part.", $b_waived = FALSE, $s_package) {
			MonthlyInvoiceParts::echo_function_name();
			$i_invoice_id = (int)$i_invoice_id;
			return $this->_create_invoice_part($s_date, $s_dataname, $i_invoice_id, $b_check_for_previous, $o_invoice_part, true, '', $b_waived, $s_package);
		}

		private function _create_invoice_part($s_date, $s_dataname, $i_invoice_id, $b_check_for_previous, &$o_invoice_part = NULL, $b_save = TRUE, $s_creation_details = "", $b_waived = FALSE, $s_package) {
			MonthlyInvoiceParts::echo_function_name();

			// check the date range
			$db = $this->db_values;
			$i_today = strtotime(date("Y-m-d", strtotime($s_date)));
			$i_start = strtotime($db->mp_start_day);
			$i_end = ($db->mp_end_day == "") ? $i_today : strtotime($db->mp_end_day);
			if ($i_start > $i_today || $i_end < $i_today)
				return FALSE;

			// create the hosting invoice part for the current invoice if such doesn't exist
			// and update the date, invoice_id, and waived values
			$i_waived = ($db->waived || $b_waived) ? 1 : 0;
			if ($b_check_for_previous)
				$this->check_for_previous_invoice($s_date, $s_dataname, $i_invoice_id, $i_waived, $s_creation_details);

			// the the new invoice part with all properties set
			$o_invoice_part = InvoiceParts::createNewInvoicePartWithProperties($db, $i_today, $s_date, $s_dataname, $i_invoice_id, $i_waived, $s_creation_details, $s_package);

			// save to db
			if ($b_save)
				$o_invoice_part->save_to_db();
		}

		public function update() {
			MonthlyInvoiceParts::echo_function_name();
			// this function PURPOSEFULLY DOES NOTHING
			// if you want to update the monthly invoice's amount because the parts changed, use UpdateInvoiceByParts()
			// if you want to update any other part of a montly part, instead outdate it and create a new monthly part
		}

		// sets the mp_end_day of the monthly_part and save it to the database (if the id > 0)
		// adds the outdate to the history
		// @$s_date: if not set it chooses date("Y-m-d", strtotime("-1 days"))
		public function outdate($s_date = "") {
			MonthlyInvoiceParts::echo_function_name();
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

			global $loggedin;
			global $lavu_admin;

			// sanitize the input
			if ($s_date == "") {
				$s_date = date("Y-m-d", strtotime("-1 days"));
				$prev_time = strtotime($this->db_values->mp_end_day);
				if ($prev_time > 0 && $prev_time < strtotime($s_date))
					return FALSE;
			} else {
				$s_date = date("Y-m-d", strtotime($s_date));
			}

			// Add a cancelled history event in the mp_json_history
			$mp_history = json_decode($this->db_values->mp_json_history, true);
			$mp_history[ date('Y-m-d H:i:s') ] = array('action' => 'cancelled', 'details' => $s_date, 'username' => $loggedin, 'usertype' => ($lavu_admin ? LAVUADMIN : USER));
			$this->db_values->mp_json_history = json_encode($mp_history);

			// update and save the monthly part
			$this->db_values->mp_end_day = $s_date;
			$a_update_vars = (array)$this->db_values;
			$s_update_clause = $dbapi->arrayToUpdateClause($a_update_vars);
			$result = mlavu_query("UPDATE `poslavu_MAIN_db`.`monthly_parts` {$s_update_clause} WHERE `id`='[id]' AND `dataname`='[dataname]'", $a_update_vars);
			if ($result === false) error_log(__FILE__ ." got MySQL error: ". mlavu_dberror());
		}

		// enforces the integer type upon some fields
		private function enforce_types() {
			MonthlyInvoiceParts::echo_function_name();
			foreach(array('id','pro_rate','waived','promo','bundled','distro_points') as $s_name)
				$this->db_values->$s_name = (int)$this->db_values->$s_name;
			foreach(array('amount','mp_priority','distro_cmsn') as $s_name)
				$this->db_values->$s_name = (double)$this->db_values->$s_name;
		}

		// update an invoice, setting its value equal to its constituent parts
		// @$i_invoice_id: id of the invoice to be updated
		// @return: TRUE if its constituent parts could be found, FALSE otherwise
		public static function UpdateInvoiceByParts($i_invoice_id) {
			self::echo_function_name();
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

			// get the sum of the parts
			$a_all_values = $dbapi->getAllInTable('invoice_parts', array('invoice_id'=>$i_invoice_id), TRUE, array('selectclause'=>'COUNT(*) AS `count`,SUM(`amount`) AS `sum`'));
			$a_normal_values = $dbapi->getAllInTable('invoice_parts', array('invoice_id'=>$i_invoice_id, 'waived'=>0), TRUE, array('selectclause'=>'COUNT(*) AS `count`,SUM(`amount`) AS `sum`'));
			$a_all_values = $a_all_values[0];
			$a_normal_values = $a_normal_values[0];
			$i_waived = (int)((int)$a_all_values['count'] > 0 && (int)$a_normal_values['count'] == 0);
			if (count($a_all_values) == 0)
				return FALSE;
			if ($i_waived)
				$f_sum = (double)$a_all_values['sum'];
			else
				$f_sum = (double)$a_normal_values['sum'];

			// save the sum
			mlavu_query("UPDATE `poslavu_MAIN_db`.`invoices` SET `amount`='[amount]',`waived`='[waived]' WHERE `id`='[id]'", array('id'=>$i_invoice_id, 'amount'=>$f_sum, 'waived'=>$i_waived));
			return TRUE;
		}

		// loads all monthly parts that apply between the two given dates
		// @$s_dataname: the dataname to load parts for
		// @$s_date1: the start date
		// @$s_date2: the end date
		// @$a_retval: used to return the set of montly parts loaded from the database as MonthlyInvoiceParts objects
		// @return: the number of rows loaded
		public static function load_monthly_parts($s_dataname, $s_date1, $s_date2, &$a_retval) {
			self::echo_function_name();
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

			// load from the database
			$a_db_rows = $dbapi->getAllInTable('monthly_parts', array('dataname'=>$s_dataname, 'start_date'=>$s_date1, 'end_date'=>$s_date2), TRUE, array('whereclause'=>"WHERE `dataname` = '[dataname]' AND ((`mp_start_day` <= '[end_date]' OR `mp_start_day` = '') AND (`mp_end_day` >= '[start_date]' OR `mp_end_day` = ''))"));
			if (count($a_db_rows) == 0)
				return 0;

			// convert to objects
			$a_retval = array();
			foreach($a_db_rows as $a_row)
				$a_retval[] = new MonthlyInvoiceParts(0, $a_row);
			return count($a_retval);
		}

		// given a set of monthly parts, returns the parts that apply to the given date, prioritizing parts of the same description based on their priority
		// @$a_monthly_parts: the entire set of monthly part (the haystack)
		// @$s_date: the date that these parts should apply to
		// @$a_retval: used to return the subset that apply
		// @return: the number of monthly parts that apply
		public static function get_monthly_parts_that_apply_to_date(&$a_monthly_parts, $s_date, &$a_retval) {
			self::echo_function_name();

			// get some variables
			$i_date = strtotime($s_date);

			// build the array of monthly parts that apply
			$a_pre_retval = array();
			if (count($a_monthly_parts) > 0) {
				foreach($a_monthly_parts as $o_monthly_part) {
					$i_start = ($o_monthly_part->db_values->mp_start_day == '') ? $i_date : strtotime($o_monthly_part->db_values->mp_start_day);
					$i_end = ($o_monthly_part->db_values->mp_end_day == '') ? $i_date : strtotime($o_monthly_part->db_values->mp_end_day);
					$f_priority = $o_monthly_part->db_values->mp_priority;
					$s_description = $o_monthly_part->db_values->description;
					if ($i_start > $i_date || $i_end < $i_date)
						continue;
					if (isset($a_pre_retval[$s_description]) && $a_pre_retval[$s_description]->db_values->mp_priority >= $f_priority)
						continue;
					$a_pre_retval[$s_description] = $o_monthly_part;
				}
			}

			// build the return array
			$a_retval = array();
			foreach($a_pre_retval as $o_monthly_part)
				$a_retval[$o_monthly_part->db_values->description] = $o_monthly_part;

			return count($a_retval);
		}

		// returns an empty MonthlyInvoiceParts object
		public static function emptyMonthlyPart() {
			self::echo_function_name();
			return new MonthlyInvoiceParts(0, array(
				'description'=>'Hosting',
				'amount'=>$a_create['monthly_recurring'],
				'waived'=>0,
				'distro_points'=>0,
				'distro_cmsn'=>0.0,
				));
		}

		// retrieve the hosting invoice part id (`description` LIKE 'hosting')
		// @$s_invoice_date: the date that the invoice occurs on
		// @$s_dataname: dataname of the account
		// @$a_create: creates the part if one doesn't exist and $a_create is not NULL, should contain the following values:
		//     monthly_recurring (int): the cost of the monthly recurring
		// @return: the id of the invoice part that matches the date and hosting type, or 0 if no invoice part is found/created
		public static function get_hosting_invoice_part($s_invoice_date, $s_dataname, $a_create = NULL) {
			self::echo_function_name();
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

			// try and find the invoice part id
			$a_invoice_parts = $dbapi->getAllInTable('invoice_parts', array('desc'=>'hosting', 'date'=>$s_invoice_date), TRUE, array('whereclause'=>"WHERE `description` LIKE '[desc]' AND `due_date`='[date]'"));
			if (count($a_invoice_parts) > 0)
				return $a_invoice_parts['id'];

			// it doesn't exist, return 0
			if ($a_create === NULL)
				return 0;

			// it doesn't exist, create it
			$o_monthly_part = self::emptyMonthlyPart();
			$i_waived = 0;
			$i_invoice_id = 0;
			if (check_for_previous_invoice($s_invoice_date, $s_dataname, $i_invoice_id, $i_waived)) {
				$a_invoice_parts = $dbapi->getAllInTable('invoice_parts', array('desc'=>'hosting', 'date'=>$s_invoice_date), TRUE, array('whereclause'=>"WHERE `description` LIKE '[desc]' AND `due_date`='[date]'"));
				if (count($a_invoice_parts) > 0)
					return $a_invoice_parts['id'];
			}
			create_invoice_part($s_invoice_date, $s_dataname, $i_invoice_id);
		}

		// create a new invoice with all of the constituent parts and the original monthly invoice cost
		// it is assumed that you're trying to create a monthly hosting invoice
		// @$f_amount: the monthly cost of the invoice
		// @$s_date: the date to create the invoice on
		// @$s_dataname: dataname of the account to create the invoice for
		// @$i_restaurant_id: id of the account to create the invoice for
		// @$a_monthly_parts: an array of MonthlyInvoiceParts
		// @$b_bill_waived: TRUE if the monthly hosting part of the bill should be waived, FALSE otherwise
		// @$i_points_to_award: the number of points to award to the distributor
		// @$s_creation_details: details about creating the invoice part, optional
		// @$s_package: package level integer string at the time the invoice was created
		// @$f_cmsn_to_award: the amount of cmsn to award to the distributor
		// @return: the id of the new invoice on success or the invoice already exists, 0 on failure of the database
		public static function create_new_invoice($f_amount, $s_date, $s_dataname, $i_restaurant_id, $a_monthly_parts, $b_bill_waived, $i_points_to_award = -1, $s_creation_details = "Automatically generated from monthly part.", $s_package = '', $f_cmsn_to_award = 0.00) {
			self::echo_function_name();
			$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

			// check to make sure that the invoice doesn't exist
			$a_invoices = $dbapi->getAllInTable('invoices', array("dataname"=>$s_dataname, "due_date"=>$s_date, "type"=>"Hosting"), TRUE, array('selectclause'=>'`id`'));
			if (count($a_invoices) > 0)
				return $a_invoices[0]['id'];

			// check for invoice parts
			$a_invoice_parts = $dbapi->getAllInTable('invoice_parts', array('dataname'=>$s_dataname, 'due_date'=>$s_date), TRUE);

			// get the monthly parts that need to be created
			$a_applicable_monthly = NULL;
			self::get_monthly_parts_that_apply_to_date($a_monthly_parts, $s_date, $a_applicable_monthly);

			// create the invoice
			$a_insert_vars = array('due_date'=>$s_date, 'dataname'=>$s_dataname, 'restaurantid'=>$i_restaurant_id, 'amount'=>$f_amount, 'type'=>'Hosting', 'package'=>$s_package);
			if ($b_bill_waived) $a_insert_vars['waived'] = '1';
			$s_insert_clause = $dbapi->arrayToInsertClause($a_insert_vars);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`invoices` {$s_insert_clause}", $a_insert_vars);
			$i_invoice_id = mlavu_insert_id();

			// update invoice parts to apply to the newly created invoice
			foreach($a_invoice_parts as $a_invoice_part) {
				mlavu_query("UPDATE `poslavu_MAIN_db`.`invoice_parts` SET `invoice_id`='[invoice_id]' WHERE `id`='[id]'", array('invoice_id'=>$i_invoice_id, 'id'=>$a_invoice_part['id']));
			}

			// create the new monthly parts
			$b_check_for_previous = TRUE;
			foreach($a_applicable_monthly as $o_monthly_part) {
				$b_invoice_part_exists = FALSE;
				foreach($a_invoice_parts as $a_invoice_part) {
					if ($a_invoice_part['description'] == $o_monthly_part->db_values->description) {
						$b_invoice_part_exists = TRUE;
						break;
					}
				}
				if ($b_invoice_part_exists) {
					continue;
				}
				$b_invoice_part_created = $o_monthly_part->create_invoice_part($s_date, $s_dataname, $i_invoice_id, $b_check_for_previous, $s_creation_details, $b_bill_waived, $s_package);
				$b_check_for_previous = FALSE;
			}

			// create the hosting invoice part if no monthly parts exist
			if ($b_check_for_previous) {
				$i_waived = NULL;
				$o_monthly_part = self::emptyMonthlyPart();
				$o_monthly_part->db_values->mp_start_day = $s_date;
				$o_monthly_part->db_values->mp_end_day = $s_date;
				$o_monthly_part->check_for_previous_invoice($s_date, $s_dataname, $i_invoice_id, $i_waived, $s_creation_details);
			}

			// update the hosting invoice part with the points to award
			if ($i_points_to_award >= 0) {
				$s_query_string = "UPDATE `poslavu_MAIN_db`.`invoice_parts` SET `distro_points`='[distro_points]' WHERE `invoice_id`='[invoice_id]' AND `description` LIKE 'hosting'";
				$a_query_vars = array('distro_points'=>$i_points_to_award, 'invoice_id'=>$i_invoice_id);
				mlavu_query($s_query_string, $a_query_vars);
			}

			// update the hosting invoice part with the cmsn to award
			if ($f_cmsn_to_award >= 0.0) {
				$s_query_string = "UPDATE `poslavu_MAIN_db`.`invoice_parts` SET `distro_cmsn`='[distro_cmsn]' WHERE `invoice_id`='[invoice_id]' AND `description` LIKE 'hosting'";
				$a_query_vars = array('distro_cmsn'=>$f_cmsn_to_award, 'invoice_id'=>$i_invoice_id);
				mlavu_query($s_query_string, $a_query_vars);
			}

			// update the invoice with the cost of all of its parts
			self::UpdateInvoiceByParts($i_invoice_id);
			return $i_invoice_id;
		}

		// checks if an invoice doesn't have invoice parts and creates parts if they can't be found
		// only applies to 'Hosting' invoices
		// @$s_dataname: dataname of the account
		// @$i_invoice_id: id of the invoice
		public static function ApplyInvoicePartsToInvoice($s_dataname, $i_invoice_id) {
			$o_monthly_part = MonthlyInvoiceParts::emptyMonthlyPart();
			$s_date = "";
			$i_waived = 0;
			$query = mlavu_query("SELECT `due_date` FROM `poslavu_MAIN_db`.`invoices` WHERE `id`='[invoice_id]'", array('invoice_id'=>$i_invoice_id));
			if ($query !== FALSE && mysqli_num_rows($query) > 0) {
				$query_read = mysqli_fetch_assoc($query);
				$s_date = $query_read['due_date'];
			}
			$o_monthly_part->db_values->mp_start_day = $s_date;
			$o_monthly_part->db_values->mp_end_day = $s_date;
			$o_monthly_part->check_for_previous_invoice($s_date, $s_dataname, $i_invoice_id, $i_waived);
		}

		// used by billing/index.php to make edits and updates to invoice parts
		// does a http redirect if there is an edit
		// @$ip_mp_output: if there is anything to be echo'd, it will be appended to this variable
		// @return: FALSE if there is nothing to edit or the user doesn't have accesses, TRUE if an edit is performed
		public static function check_edit(&$ip_mp_output) {

			// check if there's an edit and check user permissions
			if (!isset($_GET['edit_monthly_parts']) || !isset($_GET['action']) || !isset($_GET['dn']))
				return FALSE;
			if (!function_exists('can_access') || !can_access('technical')) {
				echo "<b>You don't have access to edit invoice parts.</b>";
				return FALSE;
			}

			// edit the invoice part
			$b_edit = FALSE;
			if ($_GET['action'] == 'create_part') {

				// load the getvars
				$b_edit = TRUE;
				$a_getvars = array();
				foreach($_GET as $k=>$v)
					if (strpos($k, 'mp_') === 0)
						$a_getvars[substr($k,3)] = $v;
				$a_getvars = array_merge($a_getvars, array('dataname'=>$_GET['dn']));

				// initialize the new monthly part
				$o_new_monthly_part = new MonthlyInvoiceParts(0, $a_getvars);
				$s_date = date("Y-m-d");
				$a_monthly_parts = NULL;

				// check for an existing monthly part that will disqualify the new part
				self::load_monthly_parts($a_getvars['dataname'], $s_date, $s_date, $a_monthly_parts);
				self::get_monthly_parts_that_apply_to_date($a_monthly_parts, $s_date, $a_monthly_parts);
				foreach($a_monthly_parts as $o_monthly_part) {
					if ($o_monthly_part->db_values->description == $o_new_monthly_part->db_values->description &&
					    (float)$o_monthly_part->db_values->mp_priority >= (float)$o_new_monthly_part->db_values->mp_priority) {
						$ip_mp_output .= "<b>The monthly part with id ".$o_monthly_part->db_values->id." takes priority over the monthly part you are trying to create. Please choose a different description, a different date range, or a higher priority.</b><br />";
						$b_edit = FALSE;
					}
				}

				// check that the dates are in the right order, or
				// that there isn't an end date
				if ($o_new_monthly_part->db_values->mp_end_day !== "" && strtotime($o_new_monthly_part->db_values->mp_end_day) < strtotime($o_new_monthly_part->db_values->mp_start_day)) {
					$ip_mp_output .= "<b>The monthly part start day must come on or before the end day. Please choose a different date range.</b>";
					$b_edit = FALSE;
				}

				// if the monthly part is ready to be created, save it to the db
				if ($b_edit)
					$o_new_monthly_part->save_to_db();
			} else if ( $_GET['action'] == 'cancel_part' && isset($_GET['part_id']) ) {

				$o_monthly_part = new MonthlyInvoiceParts($_GET['part_id'], NULL);
				$o_monthly_part->outdate((isset($_GET['date']) ? $_GET['date'] : ''));
				$b_edit = TRUE;
			}

			// redirect
			if ($b_edit)
				header("Location: ?dn=".$_GET['dn']);

			return $b_edit;
		}

		// creates an interface to view and edit monthly parts
		// @$a_monthly parts: an array of MonthlyInvoiceParts to draw
		// @return: an html string
		public static function interfaceToString($a_monthly_parts) {

			global $dataname;

			$a_db_values = array();
			if (count($a_monthly_parts) > 0) {
				foreach($a_monthly_parts as $o_monthly_part)
					$a_db_values[] = LavuJson::json_encode($o_monthly_part->db_values);
			}
			$s_db_values = "[".implode(",", $a_db_values)."]";
			$s_db_values = str_replace("\\", "\\\\", $s_db_values);
			$s_db_values = str_replace("\"", "\\\"", $s_db_values);
			$s_today = date("Y-m-d");
			$a_getvars = array();
			foreach($_GET as $k=>$v)
				if (strpos($k, 'mp_') === 0)
					$a_getvars[substr($k,3)] = $v;
			$s_getvars = LavuJson::json_encode($a_getvars);
			return "
				<div id='monthlyPartsInterface'></div>
				<script type='text/javascript' src='/sa_cp/billing/js/Edit_MonthlyParts.js'></script>
				<script type='text/javascript'>
					$(document).ready(function(){
						if (typeof(window.Account) == 'undefined')
							window.Account = {};
						window.Account.Dataname = \"{$dataname}\";

						// set some values
						Edit.MonthlyParts.dbValues = JSON.parse(\"{$s_db_values}\");
						Edit.MonthlyParts.today = \"{$s_today}\";
						Edit.MonthlyParts.dataname = \"{$dataname}\";

						// draw the interface
						var monthlyPartsRetval = Edit.MonthlyParts.partsToString();
						$('#monthlyPartsInterface').append(monthlyPartsRetval.string);

						// add event listeners
						$.each(monthlyPartsRetval.parts, function(k,v) {
							var jtr = Lavu.Gui.getParentDOM($(Lavu.Gui.getElement(v.draw_values[0].identifier_class)), 'tr');
							var jbutton = jtr.find('.monthly_parts_button');
							jbutton.click(v.onclick);
						});

						// update the creation row
						var jtr = Lavu.Gui.getParentDOM($('input.monthly_parts_button[value=Create]'), 'tr');
						$.each({$s_getvars}, function(k,v) {
							var jinput = jtr.find('[name='+k+']');
							if (jinput.length > 0) {
								if (jinput.attr('type') == 'checkbox') {
									if (v === '1' || v === 1 || v === true)
										jinput[0].checked = true;
									else
										jinput[0].checked = false;
								} else {
									jinput.val(v);
								}
							}
						});
					});
				</script>";
		}

		public static function echo_function_name() {
			return;
			$a_stacktrace = debug_backtrace();
			echo ".".$a_stacktrace[1]['function']."\n";
		}
	}

?>
