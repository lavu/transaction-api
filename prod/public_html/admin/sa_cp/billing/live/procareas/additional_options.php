<?php

require_once(dirname(__FILE__)."/../../../../cp/objects/deviceLimitsObject.php");
require_once(dirname(__file__)."/../../package_levels_object.php");
require_once(dirname(__FILE__)."/../../../../../inc/olo/config.php");
if (!isset($o_package_container))
	$o_package_container = new package_container();

if ($lavu_admin)
{
	if(!function_exists("get_reseller_license_stats"))
	{
		function get_reseller_license_stats($a_stats, $i_license_id) {
			if (!is_array($a_stats))
				return array();
			if (count($a_stats) == 0)
				return array();
			$a_stats_safe = array();
			foreach($a_stats as $s_stat)
				$a_stats_safe[] = ConnectionHub::getConn('poslavu')->escapeString($s_stat);
			$s_stats = '`'.implode('`,`', $a_stats_safe).'`';
			$license_stats_query = mlavu_query("SELECT [stats] FROM `[maindb]`.`reseller_licenses` WHERE `id`='[id]' LIMIT 1",
				array("stats"=>$s_stats, "maindb"=>"poslavu_MAIN_db", "id"=>$i_license_id));
			if (!$license_stats_query)
				return array();
			if (mysqli_num_rows($license_stats_query) == 0)
				return array();
			return mysqli_fetch_assoc($license_stats_query);
		}

		function draw_distributor_licenses_applied($dataname) {
			// find the payment_status row, if it exists
			$all_distro_licenses_query = mlavu_query("SELECT `license_applied`,`license_resellerid`,`extra_upgrades_applied`,`extra_reseller_ids` FROM `poslavu_MAIN_db`.`payment_status` WHERE `dataname`='[1]' LIMIT 1", $dataname);
			if (!$all_distro_licenses_query)
				return '';
			if (mysqli_num_rows($all_distro_licenses_query) == 0)
				return '';

			// get the licenses
			$all_distro_licenses_read = mysqli_fetch_assoc($all_distro_licenses_query);
			foreach($all_distro_licenses_read as $k=>$v)
				$all_distro_licenses_read[$k] = trim($v);
			$a_distro_licenses = array();
			if ($all_distro_licenses_read['license_applied'] != '') {
				$a_ls = get_reseller_license_stats(array('type','resellername','cost','value','applied','notes'), (int)$all_distro_licenses_read['license_applied']);
				$a_distro_licenses[] = array('license_or_upgrade'=>'License', 'lid'=>$all_distro_licenses_read['license_applied'], 'did'=>$all_distro_licenses_read['license_resellerid'], 'date'=>$a_ls['applied'], 'val'=>$a_ls['value'], 'dval'=>$a_ls['cost'], 'dist_name'=>$a_ls['resellername'], 'lname'=>$a_ls['type'], 'notes'=>$a_ls['notes']);
			}

			// the first is the distro license, further licenses are actually upgrades
			if ($all_distro_licenses_read['extra_upgrades_applied'] != '') {
				$a_extra_upgrades = explode('|', $all_distro_licenses_read['extra_upgrades_applied']);
				$a_extra_reseller_ids = explode('|', $all_distro_licenses_read['extra_reseller_ids']);
				foreach($a_extra_upgrades as $k=>$v) {
					$a_license_stats = get_reseller_license_stats(array('type','resellername','cost','value','applied','notes'), (int)$v);
					$a_distro_licenses[] = array('license_or_upgrade'=>'Upgrade', 'lid'=>$v, 'did'=>$a_extra_reseller_ids[$k], 'date'=>$a_license_stats['applied'], 'val'=>$a_license_stats['value'], 'dval'=>$a_license_stats['cost'], 'dist_name'=>$a_license_stats['resellername'], 'lname'=>$a_license_stats['type'], 'notes'=>$a_license_stats['notes']);
				}
			}

			// draw the licenses
			$s_td_style = ' style="padding-right:15px;"';
			$s_retval = '<div id="distro_licenses_applied_to_account_div">Distributor licenses applied to account:<br />
			<table style="text-align:left;"><tr><th'.$s_td_style.'>Type</th><th'.$s_td_style.'>License name</th><th'.$s_td_style.'>ID</th><th'.$s_td_style.'>Applied on</th><th'.$s_td_style.'>Distributor</th><th'.$s_td_style.'>Value</th><th'.$s_td_style.'>Cost to distributor</th><th'.$s_td_style.'>Action</th></tr>';
			$s_action_select = '<select class="remove_license_select" onchange="
					if (this.value != \' \') {
						var reason = prompt(\'Please Enter The Reason for Change.\',\'\');

						var loginfo = \'bc_action=[license_or_upgrade] was reassigned back to distributor (license id [license_id])&bc_details=\' + reason;
						if(reason)
							window.location = \'?dn='.$dataname.'&\' + loginfo + \'&license_id=[license_id]&license_action=\' + this.value;

					}"><option value=" ">&nbsp;</option><option value="remove_and_reassign_to_distributor">Reassign to Distributor</option>';
			foreach($a_distro_licenses as $a_license) {
				$i_license_id = (int)$a_license['lid'];
				$i_distro_id = (int)$a_license['did'];
				$a_applied_on_parts = explode(' ', $a_license['date']);
				$s_applied_on = $a_applied_on_parts[0];
				$s_value = '$'.number_format($a_license['val']*1, 2);
				$s_distributor_cost = (strpos($a_license['notes'], 'PURCHASED WITH POINTS') === false) ? '$'.number_format($a_license['dval']*1, 2) : $a_license['val']*1 . ' distro points';
				$s_license_name = $a_license['lname'];
				$s_distro_name = $a_license['dist_name'];
				$s_licenseupgrade = $a_license['license_or_upgrade'];
				$s_this_action_select = str_replace("[license_or_upgrade]", $s_licenseupgrade, str_replace("[license_id]", $i_license_id, $s_action_select));
				$s_retval .= '<tr>
					<td'.$s_td_style.'>'.$s_licenseupgrade.'</td>
					<td'.$s_td_style.'>'.$s_license_name.'</td>
					<td'.$s_td_style.'>'.$i_license_id.'</td>
					<td'.$s_td_style.'>'.$s_applied_on.'</td>
					<td'.$s_td_style.'>'.$s_distro_name.'</td>
					<td'.$s_td_style.'>'.$s_value.'</td>
					<td'.$s_td_style.'>'.$s_distributor_cost.'</td>
					<td'.$s_td_style.'>'.$s_this_action_select.'</td></tr>';
			}
			$s_retval .= '</table></div>';

			return $s_retval;
		}

		// used as a namespace for these functions
		class BillingAdditionalOptions {

			// these two functions are used by the rest of the class functions to create a redirect link back to the billing page
			public static function GetRedirectOptions($dataname, $package, $signup_status) {
				$o_redirect_vars = new stdClass();
				$o_redirect_vars->dataname = $dataname;
				$o_redirect_vars->package = $package;
				$o_redirect_vars->signup_status = $signup_status;
				return $o_redirect_vars;
			}
			public static function GetRedirectString($o_redirect_vars) {
				return "/../../sa_cp/billing/index.php?dn=".$o_redirect_vars->dataname."&package=".$o_redirect_vars->package."&signup_status=".$o_redirect_vars->signup_status;
			}

			// change the invoice amount for all checked invoices
			// finds checked invoices and applies a new amount to them
			// @$current_value: the current value of (the last?) invoice
			public static function ApplyToChecked($o_redirect_vars, $current_value) {
				$retval = "";
				$retval .= "<br><span class='info_label'>Apply to checked:</span> <input type='text' name='checkbox_amount' id='checkbox_amount' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"\",\"Change Invoice Amounts to $\" + parseFloat(document.getElementById(\"checkbox_amount\").value).toFixed(2),\"".self::GetRedirectString($o_redirect_vars)."&invoice_id=\" + get_checked_list() + \"&set_invoice_amount=\" + document.getElementById(\"checkbox_amount\").value,\"\",get_checked_list(),\"\")'>";
				$retval .= "<select onchange='return make_billing_change(this,\"$current_value\",\"Change Invoice Status\",\"".self::GetRedirectString($o_redirect_vars)."&invoice_id=\" + get_checked_list() + \"&set_invoice_waived=\" + this.value,\"\",get_checked_list(),\"\")'>";
				$retval .= "<option value=''></option>";
				$retval .= "<option value=''>Active</option>";
				$retval .= "<option value='1'>Waived</option>";
				$retval .= "</select>";
				return $retval;
			}

			// create a custom invoice (License/Other) on a given license date or a given hosting date
			// @$custom_payment_date_options: the dates a custom invoice can be created on (as html options)
			// @$custom_invoice_date_options: set to the value of $custom_payment_options
			// @$invstr: the create invoice html string (is appended to)
			public static function CreateCustomInvoice($custom_payment_date_options, &$custom_invoice_date_options, &$invstr, $o_redirect_vars) {
				$custom_invoice_date_options = $custom_payment_date_options;
				$retval = "";
				$invstr .= "<span class='info_label'>Add Custom Invoice:</span> <select name='custom_invoice_type' id='custom_invoice_type' onchange='checkForHwInvoice(this.options[this.selectedIndex].value)'><option value='License'>License</option><option value='Hosting'>Hosting</option><option value='Hardware'>Hardware</option><option value='Other'>Other</option></select>";
				$invstr .= "<select name='custom_invoice_date' id='custom_invoice_date'>$custom_invoice_date_options</select>";
				$invstr .= " <input type='text' name='custom_invoice_amount' id='custom_invoice_amount' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"\",\"Add Custom Invoice\",\"".self::GetRedirectString($o_redirect_vars)."&set_custom_invoice_type=\" + document.getElementById(\"custom_invoice_type\").value + \"&set_custom_invoice_date=\" + document.getElementById(\"custom_invoice_date\").value + \"&set_custom_invoice_amount=\" + document.getElementById(\"custom_invoice_amount\").value,\"\",\"\",\"\")'><br>";
				$retval .= "<br><br>" . $invstr;
				return $retval;
			}

			// create a custom payment, similar to a custom invoice
			// @$custom_payment_string: the html string, generated in a different file
			public static function CreateCustomPayment($custom_payment_string) {
				return "<br>".$custom_payment_string;
			}

			// set the custom monthly hosting amount (how much is owed invoiced for per month for basic hosting)
			// @$custom_monthly_hosting: the current custom monthly hosting
			public static function CustomMonthlyHostingAmount($custom_monthly_hosting, $o_redirect_vars) {
				$custom_monthly_hosting_formatted = empty($custom_monthly_hosting) ? "blank" : $custom_monthly_hosting;
				$retval = "";
				$retval .= "<br>";
				$retval .= "<span class='info_label'>Custom Monthly Hosting Amount:</span> ";
				$retval .= "<input type='text' name='custom_monthly_hosting' id='custom_monthly_hosting' value='".$custom_monthly_hosting."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"{$custom_monthly_hosting}\",\"Set Custom Monthly Hosting ({$custom_monthly_hosting_formatted} => \" + (document.getElementById(\"custom_monthly_hosting\").value ? document.getElementById(\"custom_monthly_hosting\").value : \"blank\") + \")\",\"".self::GetRedirectString($o_redirect_vars)."&set_custom_monthly_hosting=\" + document.getElementById(\"custom_monthly_hosting\").value,\"\",\"\",\"\")'>";
				return $retval;
			}

			// automatically waives the lavu monster until the given date
			// @$auto_waive_until: the current auto waive settings
			// @$recn: returns the count of date options to auto waive until
			// @$fdate: returns the last date option provided
			// @$last_hosting_bill_date: the date of the last hosting bill (by date)
			// @$selected: returns " SELECTED" if the last provided date option is by default selected
			public static function SetAutoWaiveUntil($auto_waive_until, $o_redirect_vars, &$recn, &$fdate, $last_hosting_bill_date, &$selected) {
				$s_from_date = date("Y-m-d H:i:s");
				$retval = "";
				$retval .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";
				$retval .= "<span class='info_label'>Automatically Waive Until:</span> ";
				$retval .= "<select onchange='return make_billing_change(this,\"$auto_waive_until\",\"Set Auto Waive Until Date ({$s_from_date}-\"+this.value+\")\",\"".self::GetRedirectString($o_redirect_vars)."&set_auto_waive_until=\" + this.value,\"\",\"\",\"\")'>";
				$retval .= "<option value=''></option>";
				$recn = 0;
				$fdate = dtadd($last_hosting_bill_date, 1, "month");

				if($fdate=="")
				{
					echo "Error: cannot find last hosting date<br>";
					exit();
				}
				while ($fdate < dtadd(date("Y-m-d"), 1, "year", 2, "month") && $recn < 9999) {
					$selected = "";
					if ($fdate == $auto_waive_until) $selected = " SELECTED";
					$retval .= "<option value='$fdate'$selected>$fdate</option>";
					$next_fdate = dtadd($fdate, 1, "month");
					if ($auto_waive_until < $next_fdate && $auto_waive_until > $fdate)
					{
						$retval .= "<option value='$auto_waive_until' SELECTED>$auto_waive_until</option>";
					}
					$fdate = $next_fdate;
					$recn++;
				}

				$retval .= "</select><br>";
				return $retval;
			}

			// sets the custom number of ipads/ipods allowed for the user
			// @$custom_max_ipads: the current custom ipad limit
			// @$custom_max_ipods: the current custom ipod limit
			// @$custom_max_kds: the current custom max kds limit
			public static function CustomDeviceLimits($custom_max_ipads, $tablesideMaxIpads, $custom_max_ipods, $custom_max_kds, $custom_max_kiosk, $o_redirect_vars, $i_package, $s_dataname) {
				global $o_package_container;

				$retval = "";
				$retval .= "<br>";
				$retval .= "<span class='info_label'>Custom iPad Limit:</span> ";
				$retval .= "<input type='text' name='custom_max_ipads' id='custom_max_ipads' size='6' value='".$custom_max_ipads."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$custom_max_ipads."\",\"Set Custom iPad Limit to \" + (((document.getElementById(\"custom_max_ipads\").value * 1) > -1) ? (document.getElementById(\"custom_max_ipads\").value * 1) : \"none\"),\"".self::GetRedirectString($o_redirect_vars)."&set_custom_max_ipads=\" + document.getElementById(\"custom_max_ipads\").value,\"\",\"\",\"\")'>";
				$retval .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";

				$retval .= "<span class='info_label'>Tableside iPad Limit:</span> ";
				$retval .= "<input type='text' name='tableside_max_ipads' id='tableside_max_ipads' size='6' value='".$tablesideMaxIpads."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$tablesideMaxIpads."\",\"Set Tableside iPad Limit to \" + (((document.getElementById(\"tableside_max_ipads\").value * 1) > -1) ? (document.getElementById(\"tableside_max_ipads\").value * 1) : \"none\"),\"".self::GetRedirectString($o_redirect_vars)."&set_tableside_max_ipads=\" + document.getElementById(\"tableside_max_ipads\").value,\"\",\"\",\"\")'>";
				$retval .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";

				$retval .= "<span class='info_label'>Custom iPhone/iPod Limit:</span> ";
				$retval .= "<input type='text' name='custom_max_ipods' id='custom_max_ipods' size='6' value='".$custom_max_ipods."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$custom_max_ipods."\",\"Set Custom iPhone/iPod Limit to \" + (((document.getElementById(\"custom_max_ipods\").value * 1) > -1) ? (document.getElementById(\"custom_max_ipods\").value * 1) : \"none\"),\"".self::GetRedirectString($o_redirect_vars)."&set_custom_max_ipods=\" + document.getElementById(\"custom_max_ipods\").value,\"\",\"\",\"\")'>";
				$retval .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";
				$retval .= "<span class='info_label'>Custom Max KDS:</span> ";
				$retval .= "<input type='text' name='custom_max_kds' id='custom_max_kds' size='6' value='".$custom_max_kds."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$custom_max_kds."\",\"Set Custom Max KDS Limit to \" + (((document.getElementById(\"custom_max_kds\").value * -1) > 0) ? (document.getElementById(\"custom_max_kds\").value * 1) : \"none\"),\"".self::GetRedirectString($o_redirect_vars)."&set_custom_max_kds=\" + document.getElementById(\"custom_max_kds\").value,\"\",\"\",\"\")'>";
				$retval .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";
				$retval .= "<span class='info_label'>Custom Max Kiosk:</span> ";
				$retval .= "<input type='text' name='custom_max_kiosk' id='custom_max_kiosk' size='6' value='".$custom_max_kiosk."' onKeyPress='if(enter_pressed(this,event)) return make_billing_change(this,\"".$custom_max_kiosk."\",\"Set Custom Max Kiosk Limit to \" + (((document.getElementById(\"custom_max_kiosk\").value * 1) > 0) ? (document.getElementById(\"custom_max_kiosk\").value * 1) : \"none\"),\"".self::GetRedirectString($o_redirect_vars)."&set_custom_max_kiosk=\" + document.getElementById(\"custom_max_kiosk\").value,\"\",\"\",\"\")'>";

				$retval .= "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";

				$retval .= self::baseiPadLimitInterfaceToString($o_package_container->get_ipad_limt_by_attribute("level", (int)$i_package), $s_dataname, $o_redirect_vars);
				$retval .= "<br />";
				return $retval;
			}

			// silver customers used to not be able to do anything but quickserves
			// this option gave the ability to use tabs mode and tables mode for silver accounts (we think)
			// ::ANTIQUATED::
			public static function SetForceAllowTabTableMode(&$checked, &$setTo, $force_allow_tabs_n_tables, $o_redirect_vars) {
				$checked = "";
				$setTo = "1";
				if ($force_allow_tabs_n_tables) {
					$checked = " CHECKED";
					$setTo = "0";
				}
				return "";
				$retval = "";
				$retval .= "<span class='info_label'>Force Allow Tab/Table Mode:</span> ";
				$retval .= "<input type='checkbox' name='force_allow_tabs_n_tables' id='force_allow_tabs_n_tables' onchange='return make_billing_change(this,\"$force_allow_tabs_n_tables\",\"Set Force Allow Tab/Table Mode to ".(($setTo=="0")?"No":"Yes")."\",\"".self::GetRedirectString($o_redirect_vars)."&set_force_allow_tabs_n_tables=$setTo\",\"\",\"\",\"\")'$checked>";
				$retval .= "<br>";
				return $retval;
			}

			// turn the collection mode to on/off/default for monthly recurring
			// @$collection_mode: the current collection mode
			public static function SetCollectionMode($collection_mode, $o_redirect_vars) {
				$retval = "";
				$retval .= "<br>";
				$retval .= "<span class='info_label'>Collection Mode:</span> ";
				$retval .= "<select name='collection_mode' id='collection_mode' onchange='return make_billing_change(this,\"$collection_mode\",\"Change Collection Mode: \" + document.getElementById(\"collection_mode\").value,\"".self::GetRedirectString($o_redirect_vars)."&set_collection_mode=\" + document.getElementById(\"collection_mode\").value)'>";
				$retval .= "<option value=''>Use default collection rules</option>";
				$retval .= "<option value='on'".($collection_mode=="on"?" selected":"").">Collection On</option>";
				$retval .= "<option value='off'".($collection_mode=="off"?" selected":"").">Do not collect</option>";
				$retval .= "</select>";
				$retval .= "<br>";
				return $retval;
			}

			// set the minimum due date that the account must be paid up by
			// @$min_due_date: the current minimum due date
			// @$a_options: returns the dates allowed for the minimum due date
			// @$b_min_dt_found: returns TRUE if the minimum due date is within the standard options, FALSE otherwise
			// @$i_time: returns the UTCTIME (int) of the last standard date option
			// @$min_dt: same as $i_time, but as a string
			// @$s_option: same as $i_time, but as an html option string
			public static function SetMinimumDueDate($min_due_date, $o_redirect_vars, &$a_options, &$b_min_dt_found, &$i_time, &$min_dt, &$s_option) {
				$retval = "";
				$retval .= "<br>";
				$retval .= "<span class='info_label'>Min Due Date:</span> ";
				$retval .= "<select name='min_due_date' id='min_due_date' onchange='return make_billing_change(this,\"$min_due_date\",\"Change Min Due Date: \" + document.getElementById(\"min_due_date\").value,\"".self::GetRedirectString($o_redirect_vars)."&set_min_due_date=\" + document.getElementById(\"min_due_date\").value)'>";
				$a_options = array();
				$a_options[0] = "<option value=''></option>";
				$b_min_dt_found = (trim($min_due_date) == '' ? TRUE : FALSE);
				for($i=0; $i<30; $i++)
				{
					$i_time = mktime(0,0,0,date("m"),date("d") + $i,date("Y"));
					$min_dt = date("Y-m-d",$i_time);
					$s_option = "<option value='$min_dt'";
					if($min_due_date==$min_dt) {
						$s_option .= " selected";
						$b_min_dt_found = TRUE;
					}
					$s_option .= ">$min_dt</option>";
					$a_options[$i_time] = $s_option;
				}
				if (!$b_min_dt_found)
					$a_options[strtotime($min_due_date)] = "<option value='{$min_due_date}' SELECTED>{$min_due_date}</option>";
				$retval .= implode($a_options);
				$retval .= "</select>";
				$retval .= "<br>";
				return $retval;
			}

			// possibly the most confusing setting in billing
			// set the minimum amount a user must pay, and a time period they must pay in
			// the setting should read amount/days, eg 149/30
			// allows a user to get caught back up on payment when they are behind without charging them all at once
			// @custom_payment_plan: the current custom payment plan
			// @$f_last_monthly_hosting_cost: cost of the previous month's hosting invoice
			public static function CustomPaymentPlan($custom_payment_plan, $o_redirect_vars, $f_last_monthly_hosting_cost) {
				$retval = "";
				$retval .= "<br>";
				$retval .= "<span class='info_label'>Custom Payment Plan:</span> ";
				$retval .= "<input type='text' name='custom_payment_plan' id='custom_payment_plan' size='6' value='".$custom_payment_plan."' onKeyPress='if(enter_pressed(this,event) && make_custom_payment_plan(this)) return make_billing_change(this,\"".$custom_payment_plan."\",\"Set Custom Payment Plan \" + document.getElementById(\"custom_payment_plan\").value,\"".self::GetRedirectString($o_redirect_vars)."&set_custom_payment_plan=\" + document.getElementById(\"custom_payment_plan\").value,\"\",\"\",\"\")'>";
				$retval .= "<br>";

				ob_start();
				?>
				<script type='text/javascript'>
					window.make_custom_payment_plan = function(input) {

						// init some variables
						var error_str = "The custom payment plan must be in the form 'cost per month/7 <= days <= 31'";
						var failed_str = "\n\nFailed to update values. Please enter valid input.";
						var value = input.value+"";

						// check the syntax
						if (!value.match(/[0-9\.]+\/(([7-9])|([1-9][0-9]+))/)) {
							alert(error_str+failed_str);
							return false;
						}

						// check that the payment amount is great enough
						var cost = parseFloat(value.split('/')[0]);
						var days = parseFloat(value.split('/')[1]);
						var perMonth = <?= (double)$f_last_monthly_hosting_cost ?>;
						if (cost/days*30 < perMonth) {
							alert("The cost per month divided by the days given to pay must be greater than or equal to "+perMonth+".\n\nThe given per month value is "+cost+"/"+days+"*30 = "+(cost/days*30)+"."+failed_str);
							return false;
						}

						return true;
					}
				</script>
				<?php
				$retval .= ob_get_contents();
				ob_end_clean();

				return $retval;
			}

			// possibly the most confusing setting in billing
			// set the minimum amount a user must pay, and a time period they must pay in
			// the setting should read amount/days, eg 149/30
			// allows a user to get caught back up on payment when they are behind without charging them all at once
			// @annual_agreement: the current custom payment plan
			// @$f_last_monthly_hosting_cost: cost of the previous month's hosting invoice
			public static function AnnualAgreement($annual_agreement, $o_redirect_vars, $f_last_monthly_hosting_cost) {
				$checked = ($annual_agreement) ? 'checked' : '';
				$checkbox_value = ($annual_agreement) ? '1' : '';
				$new_value_label = ($annual_agreement) ? '(blank)' : '1';
				$new_value = ($annual_agreement) ? '' : '1';
				$retval = "";
				$retval .= "<br>";
				$retval .= "<label class='info_label' for='annual_agreement'>Annual Agreement:</label> ";
				$retval .= "<input type='checkbox' name='annual_agreement' id='annual_agreement' {$checked} onclick='return make_billing_change(this, \"{$checkbox_value}\", \"Change Annual Agreement: {$new_value_label}\", \"".self::GetRedirectString($o_redirect_vars)."&set_annual_agreement={$new_value}\", \"\", \"\", \"\", \"{$new_value}\")'>";
				$retval .= "<br>";
				return $retval;
			}

			public static function ltgStatusPlan($ltg_status_mode, $o_redirect_vars, $modules, $dataname, $ltgBillingPackages) {
				$restaurantid = admin_info("companyid");
				$retval = "";
				$retval .= "<br>";
				$retval .= "<span class='info_label'>LTG Status:</span> ";
				$rdb = "poslavu_".$dataname."_db";
				$retval .= "<select name='ltg_status' id='ltg_status' onchange='return make_billing_change(this,\"$ltg_status_mode\",\"Change LTG Status: \" + document.getElementById(\"ltg_status\").options[this.value].innerHTML,\"".self::GetRedirectString($o_redirect_vars)."&set_ltg_status=\" + document.getElementById(\"ltg_status\").value, \"\", \"\", \"\", document.getElementById(\"ltg_status\").options[this.value].innerHTML)'>";
				foreach ($ltgBillingPackages as $ltgPkgKey => $ltgPkgVal) {
					$retval .= "<option value='$ltgPkgKey'".($ltg_status_mode=="$ltgPkgKey"?" selected":"").">$ltgPkgVal</option>";
				}
				$retval .= "</select>";
				$retval .= "<br>";
				return $retval;
			}

			public static function baseiPadLimitInterfaceToString($i_package_limit, $s_dataname, $o_redirect_vars) {
				global $o_device_limits;

				// get some initial values
				$i_base_limit = $o_device_limits->getBaseiPadLimit($i_package_limit, $s_dataname);

				// draw the interface
				$s_dom_interface = "Base iPad Limit: <input type='text' id='base_ipad_limit_field' value='{$i_base_limit}' onblur='change_ipad_limit(true);' onkeyup='change_ipad_limit(false);; if(event.which == 13) {submit_base_ipad_limit(this);}' />";
				ob_start();
				?>
				<script type="text/javascript">
					function submit_base_ipad_limit(element) {
						var jelement = $(element);
						if (parseInt(jelement.val()) == <?= $i_base_limit ?>) {
							alert("Please change the base iPad limit before saving it.");
						} else if (jelement.val() > 0) {
							make_billing_change(element, <?= $i_base_limit ?>, "Change base iPad limit from <?= $i_base_limit ?> to "+jelement.val(), "<?= self::GetRedirectString($o_redirect_vars) ?>&set_base_ipad_limit="+jelement.val(), "", "", "");
						} else {
							alert("The base iPad limit must be greater than 0.");
						}
					}

					window.ipad_limit_original_value = null;
					function change_ipad_limit(set_to_original_value) {

						// get the working elements
						var jBaseLimit = $("#base_ipad_limit_field");
						var jiPadLimit = $("#custom_max_ipads");

						// get the original value
						if (window.ipad_limit_original_value === null)
							window.ipad_limit_original_value = jiPadLimit.val();

						// set the value
						if (set_to_original_value)
							jiPadLimit.val(window.ipad_limit_original_value);
						else
							jiPadLimit.val(jBaseLimit.val());

						setTimeout(function() {
							startShakeElement(jiPadLimit);
						}, 1);
					}

					window.shakingElements = [];
					function startShakeElement(jelement) {

						stopShakeElement(jelement);
						window.shakingElements[jelement[0]] = { timeout:null, originalPosition:jelement.css('position'), originalLeft:jelement.css('left') }
						jelement.css({ position:'relative' });
						shakeElement(jelement, 5, 40);
					}
					function shakeElement(jelement, iterationsLeft, delay) {

						if (iterationsLeft == 0) {
							stopShakeElement(jelement);
							return;
						}

						var left = parseInt(jelement.css('left'));
						if (!(left < 1) && !(left > -1))
							left = 0;
						if (iterationsLeft % 2 == 0)
							left += 2;
						else
							left -= 2;
						newleft = (left+'px');
						jelement.css({ left:newleft });
						window.shakingElements[jelement[0]].timeout = setTimeout(function() {
							shakeElement(jelement, iterationsLeft-1, delay);
						}, delay);
					}
					function stopShakeElement(jelement) {

						if (!window.shakingElements[jelement[0]] || window.shakingElements[jelement[0]] == null)
							return;

						// unset everything
						jelement.css({ position:window.shakingElements[jelement[0]].originalPosition, left:window.shakingElements[jelement[0]].originalLeft });
						clearTimeout(window.shakingElements[jelement[0]].timeout);
						window.shakingElements[jelement[0]] = null;
					}
				</script>
				<?php
				$s_javascript_interface = ob_get_contents();
				ob_end_clean();

				return $s_dom_interface.$s_javascript_interface;
			}
		}
	}

	$additional_options_output .= "<script language='javascript'>";
	$additional_options_output .= "  function get_checked_list() { ";
	$additional_options_output .= "    cstr = ''; ";
	$additional_options_output .= $checked_list_js;
	$additional_options_output .= "    return cstr; ";
	$additional_options_output .= "  } ";
	$additional_options_output .= "</script>";

	$mbc_code = require_once(dirname(__FILE__)."/../../make_billing_change_code.php");
	///$additional_options_output .= $mbc_code;  // Commented this out because it was putting a random "1" character in the Additional Options section and it didn't seem to be needed since the make billing change code gets outputted in the chain of require'd files.

	//----------------- Remove Distributor Licenses -------------------//
	if(can_access("upgrades_and_licenses"))
		$additional_options_output .= draw_distributor_licenses_applied($dataname);
	//-----------------------------------------------------------------//

	$o_redirect_vars = BillingAdditionalOptions::GetRedirectOptions($dataname, $package, $signup_status);
	$additional_options_output .= BillingAdditionalOptions::ApplyToChecked($o_redirect_vars, $current_value);
	$invstr = "";
	$custom_invoice_date_options = array();
	$additional_options_output .= BillingAdditionalOptions::CreateCustomInvoice($custom_payment_date_options, $custom_invoice_date_options, $invstr, $o_redirect_vars);
	$additional_options_output .= BillingAdditionalOptions::CreateCustomPayment($custom_payment_string);
	$additional_options_output .= BillingAdditionalOptions::CustomMonthlyHostingAmount($custom_monthly_hosting, $o_redirect_vars);
	$recn = 0;
	$fdate = "";
	$selected = "";
	$additional_options_output .= BillingAdditionalOptions::SetAutoWaiveUntil($auto_waive_until, $o_redirect_vars, $recn, $fdate, $last_hosting_bill_date, $selected);
	$additional_options_output .= BillingAdditionalOptions::CustomDeviceLimits($custom_max_ipads, $tablesideMaxIpads, $custom_max_ipods, $custom_max_kds, $custom_max_kiosk, $o_redirect_vars, $package, $dataname, BillingAdditionalOptions::GetRedirectString($o_redirect_vars));
	$checked = "";
	$setTo = "";
	$additional_options_output .= BillingAdditionalOptions::SetForceAllowTabTableMode($checked, $setTo, $force_allow_tabs_n_tables, $o_redirect_vars);
	$modules_info_result = mlavu_query("SELECT `modules` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name` = '[1]'", $dataname );
	if (!$modules_info_result) {
		error_log("MySQL error Loading Modules: " . lavu_dberror());
	} else if (mysqli_num_rows( $modules_info_result)) {
		$module_info = mysqli_fetch_assoc($modules_info_result);
		$module_list = $module_info['modules'];
		mysqli_free_result($modules_info_result);
	}
	$module_lists = explode(",", $module_list);

	if($p_area=="billing_info")
	{
		$additional_options_output .= BillingAdditionalOptions::SetCollectionMode($collection_mode, $o_redirect_vars);
		$a_options = array();
		$b_min_dt_found = FALSE;
		$i_time = 0;
		$min_dt = "";
		$s_option = "";
		global $f_last_monthly_hosting_cost;
		$additional_options_output .= BillingAdditionalOptions::SetMinimumDueDate($min_due_date, $o_redirect_vars, $a_options, $b_min_dt_found, $i_time, $min_dt, $s_option);
		$additional_options_output .= BillingAdditionalOptions::CustomPaymentPlan($custom_payment_plan, $o_redirect_vars, $f_last_monthly_hosting_cost);
		$additional_options_output .= BillingAdditionalOptions::ltgStatusPlan($ltg_status_mode, $o_redirect_vars, $module_lists, $dataname, $ltgBillingPackages);
		$additional_options_output .= BillingAdditionalOptions::AnnualAgreement($annual_agreement, $o_redirect_vars);

		require_once(dirname(__FILE__)."/../../procareas/monthly_parts.php");
		$a_monthly_parts = NULL;
		MonthlyInvoiceParts::load_monthly_parts($dataname, $rest_read['created'], date("Y-m-d H:i:s"), $a_monthly_parts);
		$monthly_parts_output = MonthlyInvoiceParts::interfaceToString($a_monthly_parts);
	}

	$output .= $additional_options_output . $monthly_parts_output;

}

?>
