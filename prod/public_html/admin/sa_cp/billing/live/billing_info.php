<?php
if (!isset($maindb))
	$maindb = 'poslavu_MAIN_db';
require_once(dirname(__FILE__)."/../../../cp/resources/json.php");
require_once(dirname(__FILE__)."/../../../cp/resources/help_box.php");
require_once(dirname(__FILE__)."/../payment_profile_functions.php");
require_once("/home/poslavu/public_html/register/authnet_functions.php");
require_once(dirname(__FILE__)."/../../../../inc/billing/zuora/ZuoraIntegration.php");
require_once(dirname(__FILE__)."/../../../../inc/billing/LavuAccountUtils.php");

$a_payment_status = array();

if(!isset($isLavuLite)){
	if ($in_lavu) {
		$restaurantid = admin_info("companyid");
		$dataname = admin_info("dataname");
	}
	else $dataname = $_GET['dn'];
}

//echo "The dataname is: ". $dataname. "<br>";

global $signup_read;

if (!isset($signup_read)) {
	$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`!='' AND `dataname`='[1]'", $dataname);
	if ($signup_query === false) error_log(__FILE__ .' got MySQL error: '. mlavu_dberror());
	$signup_read = mysqli_fetch_assoc($signup_query);
}

if (!isset($a_payment_status) || count($a_payment_status) == 0) {
	$ps_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_status` WHERE `dataname`!='' AND `dataname`='[1]'", $dataname);
	if ($ps_query === false) error_log(__FILE__ .' got MySQL error: '. mlavu_dberror());
	$a_payment_status = mysqli_fetch_assoc($ps_query);
}

/**
* To check lockout status of location
* @param string $trialEnd,$licenseApplied,$currentPackage,$dataname
* @return lockout status of location as locked or allow
* Ticket: LP-10828 
*/
function getAccountLockoutStatus($trialEnd, $licenseApplied, $currentPackage, $dataname)
{

	// Find out if their license has been waived.  If so do not require trial end
	$licenseWaived = false;
	$waivedQuery = mlavu_query("SELECT COUNT(1) AS total FROM `poslavu_MAIN_db`.`invoices` WHERE `dataname`='[1]' AND `type`='License' AND `waived`='1'",$dataname);
	$waivedQueryResult = mysqli_fetch_assoc($waivedQuery);
	if($waivedQueryResult['total'] > 0){
		$licenseWaived = true;
	}
	$accountStatus = true;
	#first check before trial expiration check
	if($currentPackage == "" && $licenseApplied == "" && !$licenseWaived ){	
		#Trial expiration check
		if($trialEnd != ""){
			$trialDueParts = explode(" ",$trialEnd);
			$trialDueDate = $trialDueParts[0];
			$dueParts = explode("-",$trialDueDate);
			if(count($dueParts) > 2){
				$currentTs = time();
				$dueTs = mktime(0,0,0,$dueParts[1],$dueParts[2],$dueParts[0]);
				$diffDays = floor(($dueTs - $currentTs) / 60 / 60 / 24) + 1;
			}
			if($diffDays < 0){
				$accountStatus = false;
			}
		}else{
			$accountStatus = false;
		}
	}
 
	if($accountStatus == false){
		return "locked";
	}else{
		return "allow";
	} 		
}

// Zuora Integration
global $zInt;
global $zAccount;
global $zAccountId;
global $zPaymentMethod;
global $zPaymentMethodId;

// Zuora init
if ( !isset($zInt) )
{
	$zInt = new ZuoraIntegration();
}


function draw_trial_mode($s_dataname) {
	global $maindb;
	global $a_payment_status;

	// get and check the $a_payment_status
	$ps_query = mlavu_query("SELECT * FROM `[database]`.`payment_status` WHERE `dataname`='[dataname]'",
		array('database'=>$maindb, 'dataname'=>$s_dataname));
	if ($ps_query === FALSE || mysqli_num_rows($ps_query) === 0)
		return '';
	$a_payment_status = mysqli_fetch_assoc($ps_query);
	if (!is_array($a_payment_status) || !isset($a_payment_status['id']) || !isset($a_payment_status['trial_end']))
		return '';

	// get the trial end time and
	// return '' if `trial_end` == '' or `trial_end` == '0000-00-00 00:00:00'
	$s_trial_end = $a_payment_status['trial_end'];
	if ($s_trial_end == '' || $s_trial_end == '0000-00-00' || $s_trial_end == '0000-00-00 00:00:00')
		return '';

	// create some text for the user to see
	// make_billing_change(belement,bvalue_current,btitle,burl,target,billid,paymentid)
	$s_admin_retval = <<<HTML
	<input id='extend_trial_out_form' type='text' name='trial_end' size='12' maxlength='10' pattern='[0-9]{4}-[0-9]{2}-[0-9]{2}' value='{$s_trial_end}'>
	<input type='button' onclick='
		make_billing_change(
			document.getElementById("extend_trial_out_form"),
			"$s_trial_end",
			"Extend Trial Out To "+ document.getElementById("extend_trial_out_form").value,
			"?dn={$_GET["dn"]}&change_trial_period=1&addsub=plus&amount=0 days&trial_end="+ document.getElementById("extend_trial_out_form").value +"&id={$a_payment_status["id"]}",
			"",
			"",
			""
		);' value='Save'>
HTML;

	$s_retval = date("n/j/Y h:m:s a", strtotime($s_trial_end));
	if (function_exists('can_access') && can_access('billing_payment_status'))
		$s_retval = $s_admin_retval;
	return $s_retval;
}

// loads user options and checks if the user wants to use collapsables
function billing_info_user_wants_collapsables() {
return true;  // force everyone to use collapseable headers
	global $loggedin;

	// load from the database
	$accounts_query = mlavu_query("SELECT `options` FROM `poslavu_MAIN_db`.`accounts` WHERE `username`='[username]'", array('username'=>$loggedin));
	if ($accounts_query === FALSE || mysqli_num_rows($accounts_query) == 0)
		return FALSE;
	$accounts_read = mysqli_fetch_assoc($accounts_query);
	$s_options = trim($accounts_read['options']);
	if ($s_options == "")
		return FALSE;

	// interpret the options using a json decoder
	$o_options = LavuJson::json_decode($s_options, FALSE);

	// check for the setting
	if (isset($o_options->billing_collapsables) && $o_options->billing_collapsables == "on")
		return TRUE;
	return FALSE;
}

//ini_set("display_errors", "1");
if ($loggedin || $in_lavu) {

	function wrappedIDList($str) {

		if (strstr($str, ",")) {
			$list = explode(",", $str);
			$str = "<br>";
			for ($i = 0; $i < count($list); $i++) {
				$str .= $list[$i];
				if ($i < (count($list) - 1)) $str .= ", ";
				if ((($i + 1) % 4) == 0) $str .= "<br>";
			}
		}

		return $str;
	}

	function build_json_details_string($details) {
		$a_details = (array)LavuJson::json_decode(substr($details,6));
		$details = '';
		foreach(array('details','comment') as $k) {
			if (isset($a_details[$k])) {
				$details .= ' '.$a_details[$k];
				unset($a_details[$k]);
			}
		}
		$details = trim($details).' (';
		$a_new_details = array();
		foreach($a_details as $k=>$v) {
			$a_new_details[] = $k.': '.$v;
		}
		$details .= implode(', ', $a_new_details).')';
		return $details;
	}

	function displayBillingChangeLog($dataname, $companyid) {

		$managesa_cp = (strstr($_SERVER['REQUEST_URI'],"/manage/")) ? 'manage' : 'sa_cp';

		echo "<br><span class='section_header'><u>Billing Change Log:</u></span><br><br>";
		if ($managesa_cp == 'manage')
			echo "<style type='text/css'>
					table.billing_change_log td {
						border-right:1px solid #eee;
						border-top:1px solid #ccc;
						padding: 2px 5px;
					}
					table.billing_change_log td.table_header {
						border:none;
						color:green;
					}
					table.billing_change_log tr:nth-child(even) {
						/*background-color:#eee;*/
					}
				</style>";
		echo "<table class='billing_change_log' ".($managesa_cp == 'manage' ? "cellspacing='0'" : '').">";

		$get_log_entries = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_log` WHERE (`dataname` = '[1]' OR `restaurantid` = '[2]') AND `_deleted` = '0' ORDER BY `datetime` ASC", $dataname, $companyid);
		if ($get_log_entries !== FALSE && mysqli_num_rows($get_log_entries) > 0) {

			echo "<tr><td class='table_header'>Date/Time</td><td class='table_header'>User</td><td class='table_header'>IP Address</td><td class='table_header'>Action</td><td class='table_header'></td><td class='table_header' ".($managesa_cp == 'manage' ? '' : "width='180px'").">Details</td></tr>";
			$count = 0;
			while ($info = mysqli_fetch_assoc($get_log_entries)) {

				$details2 = "";
				if ($info['paymentid'] != "") $details2 .= "Payment ID: ".wrappedIDList($info['paymentid']);
				if ($info['billid'] != "") {
					if ($details2 != "") $details2 .= "<br>";
					$details2 .= "Bill ID: ".wrappedIDList($info['billid']);
				}

				$details = $info['details'];
				if (strpos($details, 'JSON: {') === 0)
					$details = build_json_details_string($details);

				echo "<tr><td class='table_cell2' valign='top'>".$info['datetime']."</td><td class='table_cell2' valign='top'>".$info['username']."</td><td class='table_cell2' valign='top'>".$info['ipaddress']."</td><td class='table_cell2' valign='top'><nobr>".$info['action']."</nobr></td><td class='table_cell2' valign='top'>".$details2."</td><td class='table_cell2' valign='top'>".nl2br($details)."</td></tr>";

				$count++;
			}

		} else echo "<tr><td align='center' class='info_value' style='padding:4px 50px 4px 50px;'>No billing changes found</td></tr>";

		echo "</table>";
	}

	// determine if the user has access to change things
	global $b_has_technical;
	$b_has_technical = FALSE;
	if (function_exists('can_access') && can_access('technical')) {
		$b_has_technical = TRUE;
	}

	if (function_exists('can_access') && can_access('billing_event_log')) {
		$has_billing_event_log_access = true;
	}

	require_once(dirname(__FILE__) . "/../new_process_payments.php");

	require_once(dirname(__file__)."/../package_levels_object.php");
	if (!isset($o_package_container)) {
		$o_package_container = new package_container();
	}

	echo '<script type="text/javascript" src="/manage/js/jquery/js/jquery-1.9.0.js"></script>';
	echo '<script type="text/javascript" src="/sa_cp/tools/ajax_functions.js"></script>';
	echo <<<JS
		<script>
			var zuora_action_import = null;
			var zuora_action_sync = null;
			var zuora_action_newpaymeth = null;
			var zuora_start_date = null;
			var zuora_result = null;
			var zuora_result2 = null;
			var zuora_result_newpaymeth = null;

			var salesforce_action = null;
			var salesforce_result = null;

			var waitingAnimationCounter = 0;
			var waitingAnimationInterval = null;

			function doZuoraImport(dataname, start_date, package_level) {
				ajax_get_response('/sa_cp/billing/live/procareas/dialog_requests.php','mode=zuora_import&dataname='+ dataname +'&start_date='+ start_date +'&package_level='+ package_level, 'afterZuoraImport');

				if (zuora_action_import == null) zuora_action_import = document.getElementById('zuora_action_import');
				if (zuora_result == null) zuora_result = document.getElementById('zuora_result');

				if (zuora_action_import != null) {
					zuora_action_import.disabled = true;
					waitingAnimationInterval = window.setInterval(waitingAnimationOnZuoraImportButton, 500);
				}

				if (zuora_result != null) {
					zuora_result.innerHTML = '';
				}
			}

			function afterZuoraImport(response) {
				var vars = ajax_get_vars(response);

				// Stop import button animation
				if (waitingAnimationInterval != null)
					window.clearInterval(waitingAnimationInterval);

				// Return import button back to normal
				if (zuora_action_import != null) {
					zuora_action_import.value = 'Import';
					zuora_action_import.disabled = false;
				}

				// Display message indicating success/failure of sync next to button
				if (zuora_result != null) {
					zuora_result.innerHTML = (vars['success'] == '1') ? 'success' : 'failure - ' + vars['error_msg'];
				}

				if (vars['success'] == '1') {
					var zuora_accountid_link = document.getElementById('zuora_accountid_link');
					if (vars['zAccountId'] && zuora_accountid_link != null) {
						// Update zAccountId shown
						zuora_accountid_link.innerHTML = vars['zAccountId'];

						// Update zAccountId link
						zuora_accountid_link.href += vars['zAccountId'];
					}

					if (zuora_action_import != null) {
						zuora_action_import.className = 'hidden';
					}

					zuora_start_date = document.getElementById('zuora_start_date');
					if (zuora_start_date != null) {
						zuora_start_date.className = 'hidden';
					}

					zuora_action_sync = document.getElementById('zuora_action_sync');
					if (zuora_action_sync != null) {
						zuora_action_sync.className = '';
					}
				}
			}

			function doZuoraSync(dataname) {
				ajax_get_response('/sa_cp/billing/live/procareas/dialog_requests.php','mode=zuora_sync&dataname='+ dataname, 'afterZuoraSync');

				if (zuora_action_sync == null) zuora_action_sync = document.getElementById('zuora_action_sync');
				if (zuora_result == null) zuora_result = document.getElementById('zuora_result');

				if (zuora_action_sync != null) {
					zuora_action_sync.disabled = true;
					waitingAnimationInterval = window.setInterval(waitingAnimationOnZuoraSyncButton, 500);
				}

				if (zuora_result != null) {
					zuora_result.innerHTML = '';
				}
			}

			function afterZuoraSync(response) {
				var vars = ajax_get_vars(response);

				// Stop sync button animation
				if (waitingAnimationInterval != null)
					window.clearInterval(waitingAnimationInterval);

				// Return sync button back to normal
				if (zuora_action_sync != null) {
					zuora_action_sync.value = 'Sync';
					zuora_action_sync.disabled = false;
				}

				// Display message indicating success/failure of sync next to button
				if (zuora_result != null) {
					zuora_result.innerHTML = (vars['success'] == '1') ? 'success' : 'failure - ' + vars['error_msg'];
				}
			}

			function doZuoraDueInfo(dataname) {
				var zuora_accountid_link = document.getElementById('zuora_accountid_link');
				var zAccountId = (zuora_accountid_link === null || !zuora_accountid_link.innerHTML) ? "" : zuora_accountid_link.innerHTML;

				ajax_get_response('/sa_cp/billing/live/procareas/dialog_requests.php','mode=zuora_dueinfo&dataname='+ dataname +'&zAccountId='+ zAccountId, 'afterZuoraDueInfo');

				if (zuora_action_dueinfo == null) zuora_action_dueinfo = document.getElementById('zuora_action_dueinfo');
				if (zuora_action_dueinfo != null) {
					zuora_action_dueinfo.disabled = true;
					waitingAnimationInterval = window.setInterval(waitingAnimationOnZuoraDueInfoButton, 500);
				}

				if (zuora_result2 == null) zuora_result2 = document.getElementById('zuora_result2');
				if (zuora_result2 != null) {
					zuora_result2.innerHTML = '';
				}
			}

			function afterZuoraDueInfo(response) {
				var vars = ajax_get_vars(response);

				// Stop sync button animation
				if (waitingAnimationInterval != null)
					window.clearInterval(waitingAnimationInterval);

				// Return sync button back to normal
				if (zuora_action_dueinfo != null) {
					zuora_action_dueinfo.value = 'Due Info';
					zuora_action_dueinfo.disabled = false;
				}

				// Display message indicating success/failure of sync next to button
				if (zuora_result2 != null) {
					zuora_result2.innerHTML = (vars['success'] == '1') ? 'success <button onclick="reloadBillingFrame()">reload</button>' : 'failure ' + vars['error_msg'];
				}
			}

			function doZuoraNewPayMeth(dataname, zAccountId, zPaymentMethodId) {

				ajax_get_response('/sa_cp/billing/live/procareas/dialog_requests.php','mode=zuora_newpaymeth&dataname='+ dataname +'&zAccountId='+ zAccountId +'&zPaymentMethodId='+ zPaymentMethodId, 'afterZuoraNewPayMeth');

				if (zuora_action_newpaymeth == null) zuora_action_newpaymeth = document.getElementById('zuora_action_newpaymeth');
				if (zuora_result_newpaymeth == null) zuora_result_newpaymeth = document.getElementById('zuora_result_newpaymeth');

				if (zuora_action_newpaymeth != null) {
					zuora_action_newpaymeth.disabled = true;
					waitingAnimationInterval = window.setInterval(waitingAnimationOnZuoraNewPayMethButton, 500);
				}

				if (zuora_result_newpaymeth != null) {
					zuora_result_newpaymeth.innerHTML = '';
				}
			}

			function afterZuoraNewPayMeth(response) {
				var vars = ajax_get_vars(response);

				// Stop sync button animation
				if (waitingAnimationInterval != null)
					window.clearInterval(waitingAnimationInterval);

				// Return sync button back to normal
				if (zuora_action_newpaymeth != null) {
					zuora_action_newpaymeth.value = 'Associate';
					zuora_action_newpaymeth.disabled = false;
				}

				// Display message indicating success/failure of sync next to button
				if (zuora_result_newpaymeth != null) {
					zuora_result_newpaymeth.innerHTML = (vars['success'] == '1') ? 'success <button onclick="reloadBillingFrame()">reload</button>' : 'failure: ' + vars['error_msg'];
				}
			}


			var billingIframe = null;
			function reloadBillingFrame() {
				window.location.reload(false);
			}

			function doSalesforceSync(dataname, salesforce_id, salesforce_opportunity_id) {
				ajax_get_response('/sa_cp/billing/live/procareas/dialog_requests.php','mode=salesforce_sync&dataname='+ dataname +'&salesforce_id='+ salesforce_id +'&salesforce_opportunity_id='+ salesforce_opportunity_id, 'afterSalesforceSync');

				if (salesforce_action == null) salesforce_action = document.getElementById('salesforce_action');
				if (salesforce_result == null) salesforce_result = document.getElementById('salesforce_result');

				if (salesforce_action != null) {
					salesforce_action.disabled = true;
					waitingAnimationInterval = window.setInterval(waitingAnimationOnSalesforceSyncButton, 500);
				}

				if (salesforce_result != null) {
					salesforce_result.innerHTML = '';
				}
			}

			function afterSalesforceSync(response) {
				var vars = ajax_get_vars(response);

				// Stop sync button animation
				if (waitingAnimationInterval != null)
					window.clearInterval(waitingAnimationInterval);

				// Return sync button back to normal
				if (salesforce_action != null) {
					salesforce_action.value = 'Sync';
					salesforce_action.disabled = false;
				}

				// Display message indicating success/failure of sync next to button
				if (salesforce_result != null) {
					salesforce_result.innerHTML = (vars['success'] == '1') ? 'success' : 'failure - ' + vars['error_msg'];
				}

				var salesforce_id_link = document.getElementById('salesforce_id_link');
				if (vars['success'] == '1' && salesforce_id_link != null) {
					salesforce_id_link.innerHTML = vars['id'];
					salesforce_id_link.href += vars['id'];
				}

				var salesforce_opportunity_id_link = document.getElementById('salesforce_opportunity_id_link');
				if (vars['success'] == '1' && salesforce_opportunity_id_link != null) {
					salesforce_opportunity_id_link.innerHTML = vars['opportunity_id'];
				}
			}

			function waitingAnimationOnZuoraImportButton() {
				waitingAnimationCounter = ++waitingAnimationCounter % 6;
				var ellipsis = '';
				switch (waitingAnimationCounter) {
					case 1:
					case 5:
						ellipsis = 'i';
						break;
					case 2:
					case 4:
						ellipsis = 'in';
						break;
					case 3:
						ellipsis = 'ing';
						break;
					default:
						ellipsis = '';
						break;
				}
				zuora_action_import.value = 'Import' + ellipsis;
			}

			function waitingAnimationOnZuoraSyncButton() {
				waitingAnimationCounter = ++waitingAnimationCounter % 6;
				var ellipsis = '';
				switch (waitingAnimationCounter) {
					case 1:
					case 5:
						ellipsis = 'i';
						break;
					case 2:
					case 4:
						ellipsis = 'in';
						break;
					case 3:
						ellipsis = 'ing';
						break;
					default:
						ellipsis = '';
						break;
				}
				zuora_action_sync.value = 'Sync' + ellipsis;
			}

			function waitingAnimationOnZuoraDueInfoButton() {
				waitingAnimationCounter = ++waitingAnimationCounter % 6;
				var ellipsis = '';
				switch (waitingAnimationCounter) {
					case 1:
					case 5:
						ellipsis = '.';
						break;
					case 2:
					case 4:
						ellipsis = '..';
						break;
					case 3:
						ellipsis = '...';
						break;
					default:
						ellipsis = '';
						break;
				}
				zuora_action_dueinfo.value = 'Due Info' + ellipsis;
			}

			function waitingAnimationOnZuoraNewPayMethButton() {
				waitingAnimationCounter = ++waitingAnimationCounter % 6;
				var ellipsis = '';
				switch (waitingAnimationCounter) {
					case 1:
					case 5:
						ellipsis = 'i';
						break;
					case 2:
					case 4:
						ellipsis = 'in';
						break;
					case 3:
						ellipsis = 'ing';
						break;
					default:
						ellipsis = '';
						break;
				}
				zuora_action_newpaymeth.value = 'Associat' + ellipsis;
			}

			function waitingAnimationOnSalesforceSyncButton() {
				waitingAnimationCounter = ++waitingAnimationCounter % 6;
				var ellipsis = '';
				switch (waitingAnimationCounter) {
					case 1:
					case 5:
						ellipsis = 'i';
						break;
					case 2:
					case 4:
						ellipsis = 'in';
						break;
					case 3:
						ellipsis = 'ing';
						break;
					default:
						ellipsis = '';
						break;
				}
				salesforce_action.value = 'Sync' + ellipsis;
			}

			function checkForHwInvoice(invoiceType) {
				if (invoiceType == 'Hardware') {
					var oldCustomInvoiceDate = document.getElementById('custom_invoice_date');
					var oldValue = oldCustomInvoiceDate.options[oldCustomInvoiceDate.selectedIndex].value;

					var newCustomInvoiceDate = document.createElement("input");
					newCustomInvoiceDate.setAttribute("type", "text");
					newCustomInvoiceDate.setAttribute("id", "custom_invoice_date");
					newCustomInvoiceDate.setAttribute("name", "custom_invoice_date");
					newCustomInvoiceDate.setAttribute("placeholder", "YYYY-mm-dd");
					newCustomInvoiceDate.setAttribute("value", oldValue);

					var nextSibling = oldCustomInvoiceDate.nextSibling;
					var parentElement = oldCustomInvoiceDate.parentNode;
					parentElement.removeChild(oldCustomInvoiceDate);
					parentElement.insertBefore(newCustomInvoiceDate, nextSibling);
				}
			}
		</script>
JS;

	if (isset($_GET['package'])) {
		$package = $_GET['package'];
		$signup_status = $_GET['signup_status'];
	} else {

		if(isset($isLavuLite))
			$status_info = find_package_status($restaurantid, true );//This is to check to see if we are lavulite
		else
			$status_info = find_package_status($restaurantid);

		$package = $status_info['package'];
		$signup_status = $status_info['signup_status'];
	}

	$set_package = (isset($_GET['set_package']))?$_GET['set_package']:"";
	$reassign_to_distributor_license_id = (isset($_GET['license_id']))?$_GET['license_id']:'';
	$reassign_to_distributor_license_action = (isset($_GET['license_action']))?$_GET['license_action']:'';
	if($package=="" && $set_package=="")
	{

		echo "Billing Info is not available for this account because the package level (Silver/Gold/Platinum) has not been specified<br>";
	}
	else
	{
		// get the restaurant and company id from the restaurants table
		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name` = '[1]'", $dataname);
		if(mysqli_num_rows($rest_query)) {
			$rest_read = mysqli_fetch_assoc($rest_query);
			$companyid = $rest_read['id'];
		} else {
			echo "Unable to load company info<br>";
			exit();
		}

		// get the current package from the payment status table
		$pstat_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[1]'",$dataname);
		if (mysqli_num_rows($pstat_query)) {
			$pstat_read = mysqli_fetch_assoc($pstat_query);
			if ($pstat_read['current_package'] != "") $package = $pstat_read['current_package'];
		}

		// log billing changes made through the make_billing_change() script
		if (isset($_REQUEST['bc_action']) && $b_has_technical) {
			$vars = array();
			$vars['dataname'] = $dataname;
			$vars['restaurantid'] = $restaurantid;
			$vars['username'] = $loggedin;
			$vars['fullname'] = $loggedin_fullname;;
			$vars['ipaddress'] = $_SERVER['REMOTE_ADDR'];
			$vars['action'] = $_REQUEST['bc_action'];
			$vars['details'] = $_REQUEST['bc_details'];
			$vars['target'] = $_REQUEST['bc_target'];
			$vars['billid'] = $_REQUEST['bc_billid'];
			$vars['paymentid'] = $_REQUEST['bc_paymentid'];
			$keys = array_keys($vars);
			$qfields = "";
			$qvalues = "";
			foreach ($keys as $key) {
				if ($qfields != "") { $qfields .= ", "; $qvalues .= ", "; }
				$qfields .= "`$key`";
				$qvalues .= "'[$key]'";
			}

			// Append an additional note about the payment_status.trial_end date being blanked out at the same time the package level was changed.
			if (isset($_GET['change_trial_period']) && isset($_GET['trial_end']) && $_GET['trial_end'] == '') $vars['action'] .= ', Trial End date cleared';

			$record_billing_change = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` ($qfields, `datetime`) VALUES ($qvalues, now())", $vars);
			global $record_billing_id;
			$record_billing_id = mlavu_insert_id();

		}
		$make_change_success = TRUE;

		// try to update the package
		if ($set_package != "" && $b_has_technical) {

			// LP-422 -- Create a Lavu reseller license for distro demo accounts that don't already have one
			if ($set_package == '25' && $signup_status == 'manual' && !empty($rest_read['distro_code']))
			{
				$dbargs = array('dataname' => $dataname, 'restaurantid' => $restaurantid, 'type' => 'Lavu', 'distro_code' => $rest_read['distro_code'], 'resellerid' => '');

				// First check for an existing reseller license
				$preexisting_licenses = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` WHERE `restaurantid`='[restaurantid]' AND `type`='[type]' AND `applied`!=''", $dbargs);
				if (mysqli_num_rows($preexisting_licenses) === 0)
				{
					// Get resellerid
					$resellers = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`resellers` WHERE `username`='[distro_code]'", $dbargs);
					if ($resellers !== false && mysqli_num_rows($resellers) > 0)
					{
						$reseller = mysqli_fetch_assoc($resellers);
						$dbargs['resellerid'] = $reseller['id'];
					}

					// Create and apply reseller_license
					$dbargs['applied'] = date("Y-m-d H:i:s");
					$insert_ok = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_licenses` (`type`, `resellerid`, `resellername`, `restaurantid`, `cost`, `value`, `purchased`, `applied`, `notes`, `promo_name`, `promo_expiration`, `cmsn`, `cmsn_applied`) VALUES ('[type]', '[resellerid]', '[distro_code]', '[restaurantid]', '0', '0', '[applied]', '[applied]', 'LAVU AUTO-CREATED', '', '0000-00-00 00:00:00', 0, 0)", $dbargs);
					if ($insert_ok)
					{
						$dbargs['reseller_license_id'] = mlavu_insert_id();
						mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `license_applied`='[reseller_license_id]', `license_type`='[type]', `license_resellerid`='[resellerid]', `trial_end`='' where `dataname`='[dataname]'", $dbargs);

						// Reload the billing screen to pick up the new package level and reseller license
						echo "<script>setTimeout(reloadBillingFrame(), 3000)</script>\n";
					}
				}
			}

			// Change the package
			require_once(dirname(__FILE__)."/../payment_profile_functions.php");
			$make_change_success = change_package($dataname, $dn, $set_package, $restaurantid);
			if (!$make_change_success) {
				$update_details_query = mlavu_query("SELECT `details` FROM `poslavu_MAIN_db`.`billing_log` WHERE `id`='[1]'",$record_billing_id);
				$a_update_details = mysqli_fetch_assoc($update_details_query);
				$s_update_details = $a_update_details['details'];
				$s_update_details .= " -- failed to change package --";
				mlavu_query("UPDATE `poslavu_MAIN_db`.`billing_log` SET `details`='[1]' WHERE `id`='[2]'", $s_update_details, $record_billing_id);
			}
			else
			{
				$accountUtil = new LavuAccountUtils();
				$accountUtil->setPosPackageInfo($dataname);
			}
		}

		// remove a license from the account and reassign it back to the distributor
		if ($reassign_to_distributor_license_action == 'remove_and_reassign_to_distributor' && $b_has_technical) {
			require_once(dirname(__FILE__)."/../payment_profile_functions.php");
			$bc_details = (isset($_GET['bc_details']))?$_GET['bc_details']:'';
			$s_response = remove_distro_license_from_restaurant((int)$reassign_to_distributor_license_id, $bc_details);
			if ($s_response == "success")
				echo "<b><font color='black'>Successfully removed the distributor license and gave the license back to the distributor</font></b>";
			else
				echo "<b><font color='black'>Error: $s_response</font></b>";
		}

		// add new notes to the restaurant
		if(isset($_GET['save_notes']) && isset($_POST['notes']) && $b_has_technical) {
			$s_note = $_POST['notes'];
			if (!$make_change_success)
				$s_note .= " FAILED";
			$success = mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `notes`='[1]' where `data_name`='[2]' limit 1", $s_note, $dataname);
			if($success) echo "<b><font color='#008800'>Updated Notes</font></b><br>";
			else echo "<b><font color='red'>Failed to update notes</font></b><br>";
		}

		// try and cancel billing profiles from the restaurant
		if(isset($_GET['cancel_profile']) && $b_has_technical)
		{

			// get the subscription id to be cancel
			$cancel_profile = $_GET['cancel_profile'];

			// check that the profile matches the restaurant id (and possibly override this check)
			$s_restaurant_code = "`restaurantid`='[restaurantid]' AND ";
			if (isset($_POST['cancel_regardless_of_restaurant_id_mismatch']) && $_POST['cancel_regardless_of_restaurant_id_mismatch'] == '1')
				$s_restaurant_code = '';

			// get the row of the billing profile to be canceled
			$query_string = "select * from `poslavu_MAIN_db`.`billing_profiles` where {$s_restaurant_code}`subscriptionid`='[subscriptionid]'";
			$query_vars = array('restaurantid'=>$companyid, 'subscriptionid'=>$cancel_profile);
			$prof_query = mlavu_query($query_string,$query_vars);
			if(mysqli_num_rows($prof_query))
			{

				// the profile restaurant id matches the actual restaurant id, or the check is being overridden
				// cancel the billing profile
				$prof_read = mysqli_fetch_assoc($prof_query);
				$profid = $prof_read['id'];

				echo "Canceling Profile: $cancel_profile (id $profid)<br>";
				$vars = array();
				$vars['refid'] = 16;
				$vars['subscribeid'] = $cancel_profile;
				$cancel_info = manage_recurring_billing("delete",$vars);
				if(isset($cancel_info['response']))
				{
					$msg_parts = explode("<text>",$cancel_info['response']);
					if(count($msg_parts) > 1)
					{
						$msg_parts = explode("</text>",$msg_parts[1]);
						if(count($msg_parts) > 1)
						{
							$msg = $msg_parts[0];
							echo "Response: <b>$msg</b><br>";
						}
					}
				}
			} else {

				// the restaurant id doesn't match
				// ask the user if they want to override the id
				$a_get_vars = array();
				foreach($_GET as $k=>$v)
					$a_get_vars[] = "{$k}={$v}";
				$a_post_vars = array();
				foreach($_POST as $k=>$v)
					$a_post_vars[] = "<input type='hidden' name='{$k}' value='".str_replace('\'', '"', $v)."' />";
				echo "
					<div style='padding-bottom:15px;'>
						<font style='font-weight:bold;'>Can't cancel profile (mismatched restaurantid).</font>
						<form name='bchange_form' method='post' action='?".implode('&', $a_get_vars)."' style='display:inline;'>
							".implode('', $a_post_vars)."
							<input type='hidden' name='cancel_regardless_of_restaurant_id_mismatch' value='1' />
							<input type='submit' value='Cancel Anyways' />
						</form>
					</div>";
			}
		}

		// check if this is manage or sa_cp
		// if sa_cp, force a minimum table width
		$managesa_cp = (strstr($_SERVER['REQUEST_URI'],"/manage/")) ? 'manage' : 'sa_cp';
		echo "<table id='billing_table' ".($managesa_cp == 'manage' ? '' : "width='900px'").">";

		$main_align = "center";
		if ($p_area == "billing_info") {
			$locations = mlavu_query("SELECT `id` FROM `poslavu_[1]_db`.`locations` WHERE `_disabled`='0' ORDER BY `id`", $dataname);
			if ($locations === false) error_log(__FILE__ .' got MySQL error: '. mlavu_dberror());
			$location = mysqli_fetch_assoc($locations);

			// POS db package_info - based on code from /lib/deviceValidations.php
			$pos_package_infos = mlavu_query("select `value` AS `package`, `value2` AS `ipods`, `value3` AS `ipads`, `value4` as `tableside` from `poslavu_[1]_db`.`config` WHERE `type`='system_setting' AND `location`='[2]' AND `location`!='' AND `setting`='package_info'", $dataname, $location['id']);
			if ($pos_package_infos === false) error_log(__FILE__ .' got MySQL error: '. mlavu_dberror());
			$pos_package_info = mysqli_fetch_assoc($pos_package_infos);
			$pos_package_level = isset($_REQUEST['set_package']) ? $_REQUEST['set_package'] : $signup_read['package'];

			// Get POS trial lockout date from row in config table (based on code from /lib/deviceValidations.php)
			$pos_payment_statuses = mlavu_query("select `value` AS `trial_end_ts`,`value2` AS `trial_end`  from `poslavu_{$dataname}_db`.`config` where `type`='system_setting' and `location`='[1]' and `setting`='payment_status'", $location['id']);
			if ($pos_payment_statuses === false) error_log(__FILE__ .' got MySQL error: '. mlavu_dberror());
			$pos_payment_status = mysqli_fetch_assoc($pos_payment_statuses);
			//$pos_trial_end_date = empty($pos_payment_status['trial_end_ts']) ? '' : date('Y-m-d', $pos_payment_status['trial_end_ts']);
			$pos_trial_end_date = $pos_payment_status['trial_end'];
			
			$package_name = $o_package_container->get_printed_name_by_attribute('level', $pos_package_level);
			$pos_package_mismatch = ($package_name != $pos_package_info['package']) ? 'background-color: #ffe6e6;' : '';
			$pos_ipads_mismatch = ($a_payment_status['custom_max_ipads'] != '' && $a_payment_status['custom_max_ipads'] != $pos_package_info['ipads']) ? 'background-color: #ffe6e6;' : '';
			$pos_ipods_mismatch = ($a_payment_status['custom_max_ipods'] != '' && $a_payment_status['custom_max_ipods'] != $pos_package_info['ipods']) ? 'background-color: #ffe6e6;' : '';
			$posTablesideMismatch = ($a_payment_status['tableside_max_ipads'] != '' && $a_payment_status['tableside_max_ipads'] != $pos_package_info['tableside']) ? 'background-color: #ffe6e6;' : '';
			$pos_trial_end_mismatch = (!empty($pos_trial_end_date) && $a_payment_status['trial_end'] != $pos_trial_end_date) ? 'background-color: #ffe6e6;' : '';
			$pos_trial_end_display_css = empty($pos_trial_end_date) ? 'none' : 'inline-block';

echo <<<HTML
			<tr><td class='page_title'>
			<div style='display:inline-block'>
				{$rest_read['company_name']}
				<pre style='font-size:.9em; margin-top:0px; margin-bottom:5px;' class='lavugreen'>{$dataname}</pre>
			</div>
			<div style='display:inline-block; vertical-align:top; margin-top:0px; margin-left:5em; margin-bottom:1em'>
				<figure style='display:inline-block; vertical-align:top; margin:0; border:1px solid lightgray; border-radius: 5px; text-align:center; padding:2px; {$pos_package_mismatch}'>
					<figcaption style='padding:3px; font-size:9px'>Package</figcaption>
					<span id="pos_package">{$pos_package_info['package']}</span>
				</figure>
				<figure style='display:inline-block; vertical-align:top; margin:0; border:1px solid lightgray; border-radius: 5px; text-align:center; padding:2px; {$pos_ipads_mismatch}'>
					<figcaption style='padding:3px; font-size:9px'>iPads</figcaption>
					<span id="pos_ipads">{$pos_package_info['ipads']}</span>
				</figure>
				<figure style='display:inline-block; vertical-align:top; margin:0; border:1px solid lightgray; border-radius: 5px; text-align:center; padding:2px; {$pos_ipods_mismatch}'>
					<figcaption style='padding:3px; font-size:9px'>iPods</figcaption>
					<span id="pos_ipods">{$pos_package_info['ipods']}</span>
				</figure>
				<figure style='display:inline-block; vertical-align:top; margin:0; border:1px solid lightgray; border-radius: 5px; text-align:center; padding:2px; {$posTablesideMismatch}'>
					<figcaption style='padding:3px; font-size:9px'>Tableside iPad</figcaption>
					<span id="pos_tableside">{$pos_package_info['tableside']}</span>
				</figure>
				<figure style='display:{$pos_trial_end_display_css}; vertical-align:top; margin:0; border:1px solid lightgray; border-radius: 5px; text-align:center; padding:2px; {$pos_trial_end_mismatch}'>
					<figcaption style='padding:3px; font-size:9px'>App Lockout Date</figcaption>
					<span id="pos_ipods">{$pos_trial_end_date}</span>
				</figure>
			</div>
			<div>
				<button style="background-color:lightgray;" onclick='window.open("/manage/misc/MainFunctions/custLogin.php?site=old&mode=login&custID={$restaurantid}&custCode={$dataname}&rmode=billing", "_blank"); window.focus();'>💰 CP Billing Page</span></button>
				<button style="background-color:lightgray;" onclick='window.open("/manage/misc/MainFunctions/custLogin.php?site=old&mode=login&custID={$restaurantid}&custCode={$dataname}&rmode=billing&cancel_step=info", "_blank"); window.focus();'>❌️ CP Cancel Page</span></button>
			</div>
			</td></tr>
HTML;
			$main_align = "left";
		}
		echo "<tr><td align='$main_align'>";
		echo "<table><tr><td valign='top'>";

		// draw the collapsable headers?
		$b_use_collapsables = $p_area == "billing_info" && billing_info_user_wants_collapsables();
		if ($b_use_collapsables) {
			$collapsable_header = "<div class='manage_grouping_header'>--title--</div>";
			$collapsable_body = "manage_grouping";
			$collapsable_hr = "display:none;";
			echo "<script type='text/javascript' src='/manage/js/collapsables.js'></script>";
			echo "
				<style type='text/css'>
					.manage_grouping_header {
						padding-top: 20px;
						font-weight: 500;
					}
					.manage_grouping_header.collapsed {
						border-bottom: 1px dotted lightgray;
					}
					.manage_grouping_header .collapsable {
						background-color: lightgray;
						width: 16px;
						height: 20px;
						border: 0px solid transparent;
						border-radius: 5px;
						font-size: 16px;
						text-align: center;
						padding: 3px 1px;
						display: inline-block;
						cursor: pointer;
						margin-right: 8px;
						transform: rotate(90deg);
						-ms-transform: rotate(90deg);
						-webkit-transform: rotate(90deg);
					}
					.manage_grouping_header.collapsed .collapsable {
						transform: rotate(0deg);
						-ms-transform: rotate(0deg);
						-webkit-transform: rotate(0deg);
					}
					.manage_grouping {
						border: 1px solid lightgray;
						border-radius: 5px;
						padding: 10px;
					}
					.collapsable_help {
						display: inline-table;
					}
					input:invalid {
						background-color: mistyrose;
					}
				</style>";
			echo "
				<script type='text/javascript'>
					$(document).ready(function(){
						create_collapsables();
						$.each($('.collapsable_help'), function(k,div) {
							$(div).click(function(){
								$(div).parent().click();
							});
						});
					});
				</script>";
		} else {
			$collapsable_header = "";
			$collapsable_body = "";
		}

		// draw the header information about the account (Before drawing the invoices)
		// AKA "Account Info and Notes"
		if ($p_area == "billing_info") {

			// draw the info window
			if ($b_has_technical) {
				require dirname(__FILE__)."/../make_billing_change_code.php";
				echo $mbc_code;
			}

			echo str_replace("--title--", "Account Info and Notes", $collapsable_header);

			echo <<<HTML
				<style>
				#billing_table {
						font-family: Arial;
				}
				.info_label {
					font-weight: bold;
				}
				.page_title {
					font-size: 14pt;
					font-weight: lighter;
				}
				.smallish {
					font-size: .9em;
				}
				.xsmallish {
					font-size: .8em;
				}
				.lavugreen {
					color: #accd52;
				}
				.hidden {
					display: none;
				}
				.red {
					color: red;
				}
				a.evergreen:link,
				a.evergreen:visited,
				a.evergreen:hover {
					color: #accd52;
				}
				.lavu_gui {
					width:100%;
				}
				</style>
HTML;

			echo "<div id='billing_info_stats_and_notes' class='{$collapsable_body}'>";

			echo "<table><tr><td valign='top'>";
			echo "<table id='accountInfo' style='min-width:300px' cellspacing='1' cellpadding='3'>";
			echo "<tr><td><span class='info_label'>Dataname</span><br> <span class='info_value'>{$dataname}</span></td>";
			echo "    <td><span class='info_label'>Restaurant ID</span><br> <span class='info_value'>{$restaurantid}</span></td></tr>";
			echo "<tr><td><span class='info_label'>Package</span><br> <span class='info_value'></span>";
			//MODIFICATION HERE!!

			$distro_account_confirmation = "true";
			if (isset($pstat_read) && isset($pstat_read['trial_start']) && trim($pstat_read['trial_start']) != '')
				$distro_account_confirmation = "confirm(\"This is a distributor account with a trial period. Are you sure you want to change their package?\")";

			global $quit_after_loading_billing_info;
			$quit_after_loading_billing_info = TRUE;

			$s_trial_text = draw_trial_mode($dataname);

			// draw the package level drop-down
			if ($b_has_technical) {

				// Additional js that blank out the trial end date when changing away from a Lavu 88 Trial package level.
				$lavu88trial_addl_js = ($package == '16') ? ' + "&change_trial_period=1&addsub=&amount=&trial_end=&id='. $a_payment_status['id'] .'"' : '';

				// provide the option to change the package level
				$a_select_options = $o_package_container->get_baselevel_names(1,1000);
				echo " <select id='change_package' onchange='
				var pck = \"\";
				var pck_num = this.value;
				for(var i = 0; i < this.children.length; i++)
				{
					if(this.children[i].selected)
					{
						pck=children[i].text;
						break;
					}
				}

				if ($distro_account_confirmation) {
					var reason = prompt(\"Please Enter The Reason for Change.\",\"\");
					var loginfo = \"bc_action=Package was Changed to \" + pck + \"&bc_details=\" + reason;
					if(reason) {
						window.location = \"?dn=$dataname&package=$package&signup_status=$signup_status&\" + loginfo + \"&set_package=\" + this.value{$lavu88trial_addl_js};
						oldAccountPackageValue = this.value;
					} else {
						this.value = oldAccountPackageValue;
					}
				} else {
					this.value = oldAccountPackageValue;
				}

				'>";
				// If $package is set to a legit value but signups.package isn't, then proceed with showing the rest of the Billing screen so the user can change the drop-down to match the reseller_license
				if (($signup_read['package'] == '' || $signup_read['package'] == 'none') && $package != '' && $package != 'none') $quit_after_loading_billing_info = false;

				echo "<option value=''>none</option>";
				foreach ($a_select_options as $s_name) {
					$i_level = $o_package_container->get_level_by_attribute('name',$s_name);
					$b_matches = ($signup_read['package'] == $i_level);
					if (isset($_REQUEST['set_package'])) $b_matches = ($_REQUEST['set_package'] == $i_level);
					if ($b_matches) $quit_after_loading_billing_info = FALSE;
					echo "<option value='$i_level'".($b_matches?" selected":"").">$i_level $s_name</option>";
				}
				echo "</select>
				<script type='text/javascript'>
					oldAccountPackageValue = document.getElementById('change_package').value;

					function toggleScrollableOverflow(elmnt) {
						elmnt.style.overflow = (elmnt.style.overflow.indexOf('hidden') > -1) ? 'auto' : 'hidden';
					}
				</script>";
			} else {

				// draw the package name
				$s_name = $o_package_container->get_plain_name_by_attribute('level', $package);
				echo "({$s_name})";
				if ($s_name !== "")
					$quit_after_loading_billing_info = FALSE;
			}

			echo "    <td><span class='info_label'>Source</span><br>";

			if($signup_status=="new") {
				echo "<span class='info_value'>Self Signup</span></td></tr>";
			}
			else if ($signup_status=="manual" && !empty($a_payment_status['trial_end'])) {
				echo "<span class='info_value'>Distro Account</span></td></tr>";
			}
			else if ($signup_status=="manual" && empty($a_payment_status['trial_end'])) {
				$account_type = '';
				if (!empty($rest_read['reseller'])) $account_type .= 'Reseller ';
				if (!empty($rest_read['dev'])) $account_type .= 'Dev ';
				if (!empty($rest_read['test'])) $account_type .= 'Test ';
				if (!empty($rest_read['demo'])) $account_type .= 'Demo ';
				echo "<span class='info_value'>Lavu {$account_type} Account</span></td></tr>";
			}

			$created_date = substr($rest_read['created'],0,10);
			$created_date_formatted = date('n/j/Y', strtotime($created_date));
			$cd_parts = explode("-",$created_date);
			$last_activity_date = substr($rest_read['last_activity'],0,10);
			$last_activity_date_formatted = date('n/j/Y', strtotime($last_activity_date));
			$la_parts = explode("-",$last_activity_date);
			$prev_activity_date = date("Y-m-d",mktime(0,0,0,$la_parts[1],$la_parts[2] - 1,$la_parts[0]));

			$ipads_active = 0;
			$ipods_active = 0;

			$rdb = "poslavu_" . $dataname . "_db";

			// Get the number of iThings recenly used
			if(!isset($isLavuLite)){
				$device_query = mlavu_query("select `model` from `[1]`.`devices` where `data_name`='[2]' and `lavu_admin` = '0' AND (LEFT(`last_checkin`,10)>='[3]' and LEFT(`last_checkin`,10)>='[4]')",$rdb,$dataname,$last_activity_date,$prev_activity_date);
				while ($device_query !== FALSE && $info = mysqli_fetch_assoc($device_query)) {
					switch ($info['model']) {
						case "iPad" : $ipads_active++; break;
						case "iPhone" : $ipods_active++; break;
						case "iPod touch" : $ipods_active++; break;
						default: break;
					}
				}

				echo "<tr><td valign='top'><span class='info_label'>Created</span><br><span class='info_value'>{$created_date_formatted}</span></td>";
				echo "    <td valign='top'><span class='info_label'>Last Activity</span><br><span class='info_value'>{$last_activity_date_formatted}</span></td></tr>";
				echo "<tr><td valign='top'><span class='info_label'>Active iPads</span><br><span class='info_value'>{$ipads_active}</span></td>";
				echo "    <td valign='top'><span class='info_label'>Active iPhones/iPods</span><br><span class='info_value'>{$ipods_active}</span></td></tr>";

				// If they do not have a Zuora account, pre-populate the next bill date for the import field
				if ( empty($signup_read['zAccountId']) )
				{
					$zInt->zDataExtractor->init($dataname);
					$bill_day_of_month = $zInt->zDataExtractor->getInvoiceDayOfMonth();
					$next_bill_date = sprintf('%d-%02d-%02d', date('Y'), date('m'), $bill_day_of_month);
					$today = date('Y-m-d');
					if ( strtotime($today) > strtotime($next_bill_date) )
					{
						$next_bill_date = date('Y-m-d', strtotime("{$next_bill_date} +1 month"));
					}
				}
				// Since they have a zAccountId, go ahead and populate their zAccount (currently only *needed* on this page for AccountNumber and DefaultPaymentMethod)
				else if ( empty($zAccount) )
				{
					$zAccount = $zInt->getAccount($signup_read);
				}

				// LP-328 -- Added import, sync, and due_info buttons and AccountId links for Zuora
				$zuora_url = ZuoraConfig::SANDBOX ? 'https://apisandbox.zuora.com/apps/CustomerAccount.do?method=view&id=' : 'https://www.zuora.com/apps/CustomerAccount.do?method=view&id=';
				$salesforce_url = DEV ? 'https://cs15.salesforce.com' : 'https://na42.salesforce.com';
				$zuora_env_icon = ZuoraConfig::SANDBOX ? '🚧' : '';
				$zuora_sync_button_css   = empty($signup_read['zAccountId']) ? 'hidden' : '';
				$zuora_import_button_css = empty($signup_read['zAccountId']) ? '' : 'hidden';
				$zuora_dueinfo_button_disabled = ($signup_read['package'] == '' || $signup_read['package'] == 'none') ? 'disabled' : '';
				$salesforce_button_css = empty($signup_read['salesforce_id']) ? '' : '';
				$s_trial_text = draw_trial_mode($dataname);

				echo <<<HTML
				<tr><td valign='top'><span class='info_label'>Zuora AccountId</span><br>
						<a target='_blank' id='zuora_accountid_link' class='xsmallish evergreen' href='{$zuora_url}{$signup_read['zAccountId']}' class='info_value'>{$signup_read['zAccountId']}</a><br>
						<input type='button' class='{$zuora_sync_button_css}' name='zuora_action_sync' id='zuora_action_sync' value='Sync' onclick='doZuoraSync("{$dataname}")'>
						<input type='button' class='{$zuora_import_button_css}' name='zuora_action_import' id='zuora_action_import' value='Import' onclick='doZuoraImport("{$dataname}", document.getElementById("zuora_start_date").value, document.getElementById("change_package").options[document.getElementById("change_package").selectedIndex].value)'> {$zuora_env_icon}
						<input type='text' size='10' maxlength='10' name='zuora_start_date' id='zuora_start_date' value='{$next_bill_date}' class='{$zuora_import_button_css}'>
						<span class='lavugreen smallish' style='font-style:italic' id='zuora_result'></span></td>
					<td valign='top' nowrap><span class='info_label'>Zuora Account Number</span><br>
						<span id='zuora_accountnumber_link' class='xsmallish info_value'>{$zAccount->AccountNumber}<br>
						<input type='button' name='zuora_action_dueinfo' id='zuora_action_dueinfo' value='Due Info' onclick='doZuoraDueInfo("{$dataname}")' {$zuora_dueinfo_button_disabled}> {$zuora_env_icon}
						<span class='lavugreen smallish' style='font-style:italic' id='zuora_result2'></td></tr>
				<tr><td valign='top'><span class='info_label'>Salesforce AccountId</span><br><a target='_blank' id='salesforce_id_link' class='smallish evergreen' href='{$salesforce_url}/{$signup_read['salesforce_id']}' class='info_value'>{$signup_read['salesforce_id']}</a><br>
						<input type='button' id='salesforce_action' class='{$salesforce_button_css}' name='salesforce_action' value='Sync' onclick='doSalesforceSync("{$dataname}", "{$signup_read['salesforce_id']}", "{$signup_read['salesforce_opportunity_id']}")'>
						<span class='lavugreen smallish' style='font-style:italic' id='salesforce_result'></span></td>
					<td valign='top' nowrap><span class='info_label'>Salesforce OpportunityId</span><br>
						<a target='_blank' id='salesforce_opportunity_id_link' class='smallish evergreen' href='{$salesforce_url}/{$signup_read['salesforce_opportunity_id']}' class='info_value'>{$signup_read['salesforce_opportunity_id']}</a></td></tr>
				<tr><td valign='top'><span class='info_label'>Distro Code</span><br><span class='info_value'>{$rest_read['distro_code']}</span></td>
					<td valign='top'><span class='info_label'>Trial Expires</span><br>{$s_trial_text}<span class='info_value'></span></td></tr>
HTML;
			}

			// draw local server and self signup status
			$using_local_server = false;
			$local_sever_text = '';
			if(!isset($isLavuLite)){  //lavulite cannot have local server.
				$ls_query = mlavu_query("SELECT * FROM `[1]`.`locations` WHERE `_disabled`='0' ORDER BY `id` DESC", $rdb);
				if($ls_query !== FALSE && mysqli_num_rows($ls_query))
				{
					$location = mysqli_fetch_assoc($ls_query);
					if ( ($location['use_net_path'] == '1' || $location['use_net_path'] == '2') && strpos($location['net_path'], 'poslavu.com') === false ) {
						$using_local_server = true;
						$local_sever_text = 'LLS (Local Server)';
					}
				}
			}

			echo "<td><span class='info_value2 lavugreen'>{$local_sever_text}</span></td><td><span class='info_value2'></span></td></tr>";

			/* UNCOMMENT FOR INVOICE TAXES // Added notification to AI&N section if account has taxes enabled.
			global $tax_part;
			$tax_part = InvoiceTax::getTaxPart($dataname, date('Y-m-d'));
			if ( !empty($tax_part) ) {
				echo "<tr><td><em class='info_value' style='color:green'>". bcmul($tax_part['tax_rate'], 100) ."% ". $tax_part['description'] ."</em></td></tr>";
			}*/

			if ( !empty($rest_read['disabled']) ) {
				echo "<tr><td colspan='2'><span class='info_value red'>ACCOUNT DISABLED</span></td></tr>";
				if (empty($a_payment_status['cancel_date'])) {
					echo "<tr><td colspan='2'><span class='info_value red'>ACCOUNT <span style='text-decoration:underline'>NOT</span> CANCELLED</span></td></tr>";
				}
			}

			if ( !empty($a_payment_status['cancel_date']) ) {
				$cancel_date = date('n/j/Y', strtotime($a_payment_status['cancel_date']));
				$cancel_reason = empty($a_payment_status['cancel_reason']) ? '(None)' : $a_payment_status['cancel_reason'];
				$cancel_notes = empty($a_payment_status['cancel_notes']) ? '' : "({$a_payment_status['cancel_notes']})";

				echo "<tr><td colspan='2'><span class='info_value red'>ACCOUNT CANCELLED</span></td></tr>";
				echo "<tr><td valign='top'><span class='info_label red'>Cancel Date</span><br><span class='info_value'>{$cancel_date}</span></td>";
				echo "    <td valign='top'><span class='info_label red'>Cancel Reason</span><br><span class='info_value smallish'>{$cancel_reason} {$cancel_notes}</span></td></tr>";
			}

			// Display POS BILLING MSG
			$accountLockoutStatus = getAccountLockoutStatus($a_payment_status['trial_end'],$a_payment_status['license_applied'],$a_payment_status['current_package'],$dataname);

			if ($accountLockoutStatus == "locked") {
				echo "<tr><td colspan='2' valign='top'><span class='info_value red' style='border:solid 1px; padding:1px'>POS APP LOCKOUT MSG</span></td></tr>";
			}

			echo "</tr></table>";
			echo "</td><td valign='top'>";

			$company_settings_rows = mlavu_query("select * from `poslavu_{$dataname}_db`.`config` where `type`='location_config_setting' and `location`='[1]' and `setting` LIKE 'business_%'", $location['id']);  // query taken from /cp/areas/company_settings.php
			if ($company_settings_rows === false) error_log(__FILE__ .' got MySQL error: '. mlavu_dberror());
			$company_settings = array();
			while ( $company_settings_row = mysqli_fetch_assoc($company_settings_rows) ) {
				$company_settings[ strtolower($company_settings_row['setting']) ] = $company_settings_row['value'];
			}

			$locationCityStateSeparator = (!empty($location['city']) && !empty($location['state'])) ? ', ' : '';
			$signupCityStateSeparator = (!empty($signup_read['city']) && !empty($signup_read['state'])) ? ', ' : '';
			$custSettCityStateSeparator = (!empty($company_settings['business_city']) && !empty($company_settings['business_state'])) ? ', ' : '';

			echo <<< HTML
					<table width="600" style="margin-bottom:15px">
						<tbody>
							<tr>
								<td width="200" class="lavugreen" style="white-space:nowrap">Location Address</td>
								<td width="200" class="lavugreen" style="white-space:nowrap">Signup Address</td>
								<td width="200" class="lavugreen" style="white-space:nowrap">Customer Settings Address</td>
							</tr>
							<tr>
								<td valign="top">{$location['title']}</td>
								<td valign="top">{$signup_read['company']}</td>
								<td valign="top">{$rest_read['company_name']}</td>
							</tr>
							<tr>
								<td valign="top">{$location['manager']}</td>
								<td valign="top">{$signup_read['firstname']} {$signup_read['lastname']}</td>
								<td valign="top">{$company_settings['business_primary_contact']}</td>
							</tr>
							<tr>
								<td valign="top">{$location['address']}</td>
								<td valign="top">{$signup_read['address']}</td>
								<td valign="top">{$company_settings['business_address']}</td>
							</tr>
							<tr>
								<td valign="top">{$location['city']}{$locationCityStateSeparator}{$location['state']} {$location['zip']}</td>
								<td valign="top">{$signup_read['city']}{$signupCityStateSeparator}{$signup_read['state']} {$signup_read['zip']}</td>
								<td valign="top">{$company_settings['business_city']}{$custSettCityStateSeparator}{$company_settings['business_state']} {$company_settings['business_zip']}</td>
							</tr>
							<tr>
								<td valign="top">{$location['country']}</td>
								<td valign="top">{$signup_read['country']}</td>
								<td valign="top">{$company_settings['business_country']}</td>
							</tr>
							<tr>
								<td valign="top">{$location['phone']}</td>
								<td valign="top">{$signup_read['phone']}</td>
								<td valign="top">{$rest_read['phone_alert']}</td>
							</tr>
						</tbody>
					</table>
HTML;

			// draw the notes
			// requires /manage/tabs/Notes.php to collect its data
			$notes_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurant_notes` WHERE `restaurant_id`='[1]' ORDER BY id DESC",$restaurantid);
			if($notes_query !== FALSE && mysqli_num_rows($notes_query))
			{
				echo "<div style='position:relative; border:solid 1px #dddddd; width:85%; height:150px; margin:0; overflow:hidden; background-color:#fcfcfc' onclick='toggleScrollableOverflow(this)' title='Click the Notes area to enable/disable scrolling.'>";
				require_once(dirname(__FILE__).'/../../../manage/tabs/Notes.php');
				echo AccountNotes::getNotesCore($restaurantid);
				echo "</div>";
			}

			// quit now if the restaurant doesn't have a package level assigned
			global $quit_after_loading_billing_info;
			global $b_do_quit_after_loading_billing_info;
			if (isset($quit_after_loading_billing_info) && $quit_after_loading_billing_info && isset($b_do_quit_after_loading_billing_info) && $b_do_quit_after_loading_billing_info) {
				require_once(dirname(__FILE__)."/../procareas/process_requests.php");
				echo "</td></tr><tr><td colspan='2' class='red' style='padding:10px; text-align:center; font-weight:bold;'>Billing can't be loaded until a package level is assigned.</td></tr></table>";
				return;
			}

			echo "</td></tr></table>";
			echo "</div></td></tr>";
			echo "<tr><td valign='top'>";
		}

		// get the output from processing billing
		$pp_result = process_payments($p_area, $dataname, $package, $signup_status);
		$due_string = $pp_result['due_string'];
		$additional_options = $pp_result['additional_options_output'];
		$monthly_parts = $pp_result['monthly_parts_output'];
		/* UNCOMMENT FOR INVOICE TAXES $tax_parts = $pp_result['tax_parts_output'];
		$payments_log = str_replace(array($additional_options, $monthly_parts, $due_string, $tax_parts), '', $pp_result['output']);*/
		$payments_log = str_replace(array($additional_options, $monthly_parts, $due_string), '', $pp_result['output']);
		$payments_log = $due_string . str_replace("<br><br><br>", "<br />", $payments_log);

		/***********************************************************************
		 *              S T A R T   B I L L I N G   A R E A                    *
		 **********************************************************************/

		// Account Addresses section
		if ($p_area == 'billing_info' && $b_has_technical) {

		}

		// draw the bills and payments
		echo str_replace("--title--", "Bills and Payments", $collapsable_header);
		echo "\n\n<div id='main_content_area' class='{$collapsable_body}'>";
			echo $payments_log;

			// draw the due string again, for some reason
			if ($p_area != 'billing_info')
				echo "<br />".$due_string;
		echo "</div>\n\n";

		// draw additional options
		if ($p_area == 'billing_info' && $b_has_technical) {

			// additional options
			$s_helptext = help_mark_tostring(890);
			echo str_replace("--title--", "Additional Options".($b_use_collapsables ? " <div class='collapsable_help'>{$s_helptext}</div>" : ""), $collapsable_header);
			echo "\n\n<div id='additional_options' class='{$collapsable_body}'>";
				if (!$b_use_collapsables) echo $s_helptext;
				echo $additional_options;

				// draw the due string again, for some reason
				echo "<br />".$due_string;
				echo "<br /><hr style='{$collapsable_hr}'>";
			echo "</div>\n\n";

			// monthly parts
			$s_helptext = help_mark_tostring(888);
			echo str_replace("--title--", "Monthly Parts".($b_use_collapsables ? " <div class='collapsable_help'>{$s_helptext}</div>" : ""), $collapsable_header);
			echo "\n\n<div id='monthly_parts' class='{$collapsable_body}'>";
				if (!$b_use_collapsables) echo $s_helptext;
				echo $monthly_parts;
			echo "</div>\n\n";

			/* UNCOMMENT FOR INVOICE TAXES // tax parts
			$s_helptext = '';  // do we want to create an entry in the `helpText` table for tax_parts?  if so, we'd probably want to fix the question mark image first.
			echo str_replace("--title--", "Tax Parts".($b_use_collapsables ? " <div class='collapsable_help'>{$s_helptext}</div>" : ""), $collapsable_header);
			echo "\n\n<div id='tax_parts' class='{$collapsable_body}'>";
				if (!$b_use_collapsables) echo $s_helptext;
				echo $tax_parts;
			echo "</div>\n\n";*/
		}

		// draw the billing profiles
		echo str_replace("--title--", "Billing Profiles", $collapsable_header);
		echo "<div id='billing_profiles_div' class='{$collapsable_body}'>";

			// get some cc info
			$due_total = get_due_prop("total",$pp_result['due_info']);
			if($due_total!="" && $due_total > 0)
				$allow_cc_change = false;
			else
				$allow_cc_change = true;

			// check permissions for canceling/changing the cc profiles
			if($loggedin && $b_has_technical)
				$allow_cancel_profile = true;
			if(!isset($allow_cc_change))
				$allow_cc_change = false;
			if(!isset($allow_cancel_profile))
				$allow_cancel_profile = false;

			// show the billing profiles
			$found_active_sub = false;
			$b_draw_advanced_billing_info = FALSE;
			if (function_exists('can_access') && can_access('billing_payment_status'))
				$b_draw_advanced_billing_info = TRUE;
			if ( $p_area == "billing_info" && ($b_has_technical || $has_billing_event_log_access) )
			{
				// draw the header
				echo "<br /><span class='section_header'><u>Billing Profiles:</u></span><br><br>";
				echo "<table>";
				$profile_list = list_billing_profiles($p_area, $dataname, $companyid, $allow_cc_change, $allow_cancel_profile, $b_draw_advanced_billing_info);
				echo "</table>";

				// show the Zuora PaymentMethods
				echo <<<HTML
				<br /><span class='section_header'><u>Active Payment Methods:</u></span><br><br>
				<table cellspacing=0 cellpadding=4>
				<tr>
				<td class='table_header'>Card Holder Name</td><td></td>
				<td class='table_header'>Card Last 4</td><td></td>
				<td class='table_header'>Type</td><td></td>
				<td class='table_header'>Exp</td><td></td>
				<td class='table_header'>Status</td><td></td>
				<td class='table_header'>Start Date</td><td></td>
				<td class='table_header'>Last Pay Date</td><td></td>
				<td class='table_header'>PaymentMethod Id</td><td></td>
				</tr>
HTML;
				foreach ( $zInt->getActivePaymentMethods(array('zAccountId' => $zAccountId)) as $zPaymentMethod )
				{

					$cc_mask_num = 'xxxx'. str_replace('*', '', $zPaymentMethod->CreditCardMaskNumber);
					$expir_date = (empty($zPaymentMethod->CreditCardExpirationMonth) || empty($zPaymentMethod->CreditCardExpirationYear)) ? '' : $zPaymentMethod->CreditCardExpirationMonth .'/'. $zPaymentMethod->CreditCardExpirationYear;
					$row_color = ($zPaymentMethod->PaymentMethodStatus == 'Active') ? '#99ff99' : '#f4f4f4';
					$pmCreatedDate = substr($zPaymentMethod->CreatedDate, 0, strpos($zPaymentMethod->CreatedDate, 'T'));
					$pmLastTransactionDateTime = substr($zPaymentMethod->LastTransactionDateTime, 0, strpos($zPaymentMethod->LastTransactionDateTime, 'T'));
					$default_indicator = ($zPaymentMethod->Id == $zAccount->DefaultPaymentMethodId) ? '(default)' : '';

					echo <<<HTML
				<tr bgcolor='{$row_color}'>
				<td style='border-bottom:solid 1px #ffffff'>{$zPaymentMethod->CreditCardHolderName}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
				<td style='border-bottom:solid 1px #ffffff'>{$cc_mask_num}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
				<td style='border-bottom:solid 1px #ffffff'>{$zPaymentMethod->CreditCardType}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
				<td style='border-bottom:solid 1px #ffffff'>{$expir_date}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
				<td style='border-bottom:solid 1px #ffffff'>{$zPaymentMethod->PaymentMethodStatus}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
				<td style='border-bottom:solid 1px #ffffff'>{$pmCreatedDate}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
				<td style='border-bottom:solid 1px #ffffff'>{$pmLastTransactionDateTime}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
				<td style='border-bottom:solid 1px #ffffff'>{$zPaymentMethod->Id}</td>
				<td style='background-color:#ffffff; border-bottom:solid 1px #ffffff'>{$default_indicator}</td>
				</tr>
HTML;
				}
				echo "</table>";

				echo <<<HTML
			<label for='zuora_newpaymeth_id'>Associate existing PaymentMethod Id:</label>
			<input type='text' size='36' maxlength='32' name='zuora_newpaymeth_id' id='zuora_newpaymeth_id'>
			<input type='button' name='zuora_action_newpaymeth' id='zuora_action_newpaymeth' value='Associate' onclick='doZuoraNewPayMeth("{$dataname}", document.getElementById("zuora_accountid_link").innerHTML, document.getElementById("zuora_newpaymeth_id").value)'> {$zuora_env_icon}
			<span class='lavugreen smallish' style='font-style:italic' id='zuora_result_newpaymeth'></span>
HTML;

			echo "</div>";

			echo "</td></tr>";
		}

		// draw the billing change log
 		if ( $p_area == "billing_info" && ($b_has_technical || $has_billing_event_log_access) ) {
			echo "<tr><td>";
			echo str_replace("--title--", "Billing Change Log", $collapsable_header);
			echo "<div id='billing_change_log_div' class='{$collapsable_body}'>";
			echo "<hr style='{$collapsable_hr}'>";
			displayBillingChangeLog($dataname, $companyid);
			echo "</div>";
			echo "</td></tr>";
		}

		echo "</table><br>";
	}

}

?>
