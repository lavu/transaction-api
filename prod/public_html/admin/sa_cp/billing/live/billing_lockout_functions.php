<?php

// Requires core_functions

if(!isset($p_companyid)) $p_companyid = admin_info("companyid");
if(!isset($p_dataname)) $p_dataname = admin_info("dataname");
if(!isset($p_collection_lockout)) $p_collection_lockout = false;


function get_collection_lockout_status()
{
	global $p_collection_lockout;
	if(!isset($p_collection_lockout)) $p_collection_lockout = false;
	if($p_collection_lockout==true) return "locked";
	else return "allow";
}


function show_billing_status_alert()
{
	global $p_companyid;
	global $p_dataname;
	global $p_collection_lockout;

	$str = "";
	if($p_dataname=="nice_people_funny_dog_crazy_cat_cool_monkey") // special cases
	{
		return "";
	}

	if(admin_loggedin())
	{
		$show_alert = false;
		$show_trial_alert = false;
		$collection_due_date_message = "";
		$show_trial_message = "";
		$collection_mode = "";

		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]' or (`data_name`='[2]' and `data_name`!='')",$p_companyid,$p_dataname);
		if($rest_query->num_rows)
		{
			$rest_read = $rest_query->fetch_assoc();
			$rest_created_parts = explode(" ",$rest_read['created']);
			$rest_created = $rest_created_parts[0];

			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$p_companyid);
			while($status_read = $status_query->fetch_assoc())
			{
				$collection_mode = $status_read['collection_mode'];

				// Find out if their license has been waived.  If so do not require trial end
				$license_waived = false;
				$waived_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `type`='License' and `waived`='1' order by id desc",$rest_read['data_name']);
				if($waived_query->num_rows)
				{
					$license_waived = true;
				}

				if($status_read['trial_end']!="" && 
				   $status_read['license_applied']=="" && 
				   $license_waived==false &&
				   empty($status_read['current_package']) )
				{
					//echo "Trial Active<br>";
					$show_trial_alert = true;
					$show_trial_message = "";

					$trial_due_parts = explode(" ",$status_read['trial_end']);
					$trial_due_date = $trial_due_parts[0];

					$due_parts = explode("-",$trial_due_date);
					if(count($due_parts) > 2)
					{
						$current_ts = time();
						$due_ts = mktime(0,0,0,$due_parts[1],$due_parts[2],$due_parts[0]);
						$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
						if($diff_days > 1)
							$show_trial_message = "<b>$diff_days</b> ". speak('Days left');
						else if($diff_days==1)
							$show_trial_message = "<b>$diff_days</b> ". speak('Day left');
						else if($diff_days==0)
							$show_trial_message = "<b>" . speak('Today') . "</b> ". speak('is your last day') . '!';
						else
						{
							$p_collection_lockout = true;
							$show_trial_message = speak("Please contact your specialist to resume use of Lavu POS");

							// See if this is a self-signup to display an alternate CP lockout message
							$signup_query = mlavu_query("select status from `poslavu_MAIN_db`.`signups` where `dataname`!='' and `dataname`='[1]'",$p_dataname);
							$signup_read = $signup_query->fetch_assoc();
							if (!empty($signup_read['status']) && $signup_read['status'] == 'new') {
								$show_trial_message = speak("Your trial has ended.")."<br><br><a href='index.php?mode=billing'>" . speak('Please click here to resume use of Lavu POS') . "</a><br>";
							}
						}
					}
				}

				if($show_trial_message=="")
				{
					$due_info = $status_read['due_info'];

					if(($rest_created > "2009-01-01" || $collection_mode=="on") && $collection_mode!="off")
					{
						$due_total = get_due_prop("total",$due_info);
						$due_license = get_due_prop("license",$due_info);
						$due_hosting = get_due_prop("hosting",$due_info);
						$due_since = get_due_prop("due",$due_info);
						$paid_last_15 = get_due_prop("paid last 15",$due_info);
						$paid_last_31 = get_due_prop("paid last 31",$due_info);
						$last_payment_made = get_due_prop("last payment made",$due_info);

						if($due_total!="" && $due_total * 1 > 0)
						{
							// UNCAPTURED BLOCK START
							$uncap_query = mlavu_query("select count(*) as `count` from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and `batch_status`='N' and `date` >= '2014-02-01' and `date` < '2014-08-15' and `batch_action`=''",$p_companyid);
							if($uncap_query->num_rows)
							{
								$uncap_read = $uncap_query->fetch_assoc();
								if($uncap_read['count'] > 0)
								{
									$uncap_min_date = "2014-10-15";
									if($rest_read['distro_code']=="boontech" || $rest_read['distro_code']=="tmax")
									{
										$uncap_min_date = "2014-10-15";
									}
									if(isset($_GET['test1'])) { echo "SMD: " . $status_read['min_due_date'] . "<br>"; }
									if(trim($status_read['min_due_date'])=="" || $status_read['min_due_date'] < $uncap_min_date)
									{
										$status_read['min_due_date'] = $uncap_min_date;
									}
								}
								if(isset($_GET['test1'])) { echo "UncapCount: ". $uncap_read['count'] . "<br>MinDate: " . $status_read['min_due_date']; }
							}
							// UNCAPTURED BLOCK END

							if(isset($_GET['btest']))
							{
								$str .= speak("Due Since") . ": $due_since<br>";
								$str .= speak("Due Total") . ": $due_total<br>";
								$str .= speak("Paid Last") . " 15: $paid_last_15<br>";
								$str .= speak("Paid Last") . " 31: $paid_last_31<br>";
								$str .= speak("Last Payment Made") . ": $last_payment_made<br>";
							}

							$use_paid_last = 15;
							if($use_paid_last==30)
							{
								$paid_last_str = $paid_last_30;
								$paid_last_num = 30;
							}
							else
							{
								$paid_last_str = $paid_last_15;
								$paid_last_num = 15;
							}

							if($due_total > 0 && ($paid_last_str=="" || $paid_last_str * 1 == 0 || $status_read['min_due_date']!=""))
							{
								$collection_due_date_message = "";
								$collection_due_date = $due_since;
								//if($collection_due_date < "2012-09-26") $collection_due_date = "2012-09-26";
								if($collection_due_date < "2014-03-20") $collection_due_date = "2014-03-20"; // For those with $0 invoice parts that are being fixed

								if($last_payment_made!="" && $status_read['min_due_date']=="")
								{
									$lpm_parts = explode("-",$last_payment_made);
									$lpm_due_ts = mktime(0,0,0,$lpm_parts[1],$lpm_parts[2] + $paid_last_num,$lpm_parts[0]);
									$lpm_due_date = date("Y-m-d",$lpm_due_ts);
									if($collection_due_date < $lpm_due_date) $collection_due_date = $lpm_due_date;
								}

								$due_parts = explode("-",$collection_due_date);
								if(count($due_parts) > 2)
								{
									$current_ts = time();
									$due_ts = mktime(0,0,0,$due_parts[1],$due_parts[2] + 5,$due_parts[0]);

									if(trim($status_read['min_due_date'])!="")
									{
										$mdue = explode("-",$status_read['min_due_date']);
										if(count($mdue) > 2)
										{
											$min_due_ts = mktime(0,0,0,$mdue[1],$mdue[2],$mdue[0]);
											if($min_due_ts > $due_ts) $due_ts = $min_due_ts;
										}
									}

									$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
									if($diff_days > 1)
										$collection_due_date_message = "<b>$diff_days</b> ". speak("Days left");
									else if($diff_days==1)
										$collection_due_date_message = "<b>$diff_days</b> ". speak("Day left to use POS Lavu");
									else if($diff_days==0)
										$collection_due_date_message = "<b>" . speak('Today') ."</b> ". speak('is your last day left to use POS Lavu without resolution');
									else
									{
										$collection_due_date_message = speak("Please resolve your billing issues to resume use of POS Lavu");
										$p_collection_lockout = true;
									}
								}
								$str .= "<table cellpadding=8 style='border:solid 1px #bbbbbb' bgcolor='#f6f6f6'><tr><td valign='middle'><img src='images/little_lavu.png' /></td><td valign='middle' align='center'>";
								$str .= "<b>". speak('Important') . "</b>: ". speak('There is a billing issue with your account.') . "<br><a href='index.php?mode=billing'>" . speak('Please click here for more information') . "</a><br><br>";
								$str .= $collection_due_date_message;
								$str .= "</td></tr></table>";
								//echo "Amount Due: $due_total<br>Due Since: $due_since<br>Last 15: $paid_last_15<br>Last 31: $paid_last_31<br>";
							}
						}
					}
				}
			}
		}
	}

	/** BlueStar logic **/

	// Restaurant database row
	$restaurant_results = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `id`='[1]'", $p_companyid);
	if ($restaurant_results === false) die(__METHOD__ ." had a query error: ". mlavu_dberror());
	$restaurant = $restaurant_results->fetch_assoc();

	// Reseller database row
	$reseller_results = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`resellers` WHERE `username`='[1]'", $restaurant['distro_code']);
	if ($reseller_results === false) die(__METHOD__ ." had a query error: ". mlavu_dberror());
	$reseller = $reseller_results->fetch_assoc();

	// Reseller database row
	$reseller_leads_results = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `data_name`='[1]'", $p_dataname);
	if ($reseller_leads_results === false) die(__METHOD__ ." had a query error: ". mlavu_dberror());
	$reseller_lead = $reseller_leads_results->fetch_assoc();

	// Logic provided by martin.
	$is_bluestar = ( strpos($reseller['access'], 'bluestar') !== false );

	if ($show_trial_alert && $is_bluestar)
	{
		$activation_code = empty($_REQUEST['activation_code']) ? '' : $_REQUEST['activation_code'];

		$str .= "<form name=\"activation_code_form\" method=\"post\" action=\"index.php?mode=billing\">
		<input type=\"hidden\" name=\"mode\" value=\"" . speak('billing') . "\" />
		<table cellpadding=\"8\" style=\"border:solid 1px #bbbbbb\" bgcolor=\"#f6f6f6\" width=\"100%\"><tr><td valign=\"middle\">&nbsp;</td><td valign=\"middle\" align=\"center\">
		<strong>" . speak('Evaluation Mode') . "</strong>: $show_trial_message</td></tr>
		<tr><td colspan=\"2\" align=\"middle\" style=\"font-size:.8em\"><strong>". speak('Activation Code') . "</strong>: <input type=\"text\" name=\"activation_code\" value=\"{$activation_code}\" /> <input type=\"submit\" name=\"activate\" value=\"" . speak('Activate') . "\" /><br>
		" . speak('You can enter the BlueStar&trade; Activation Code included in your activation kit to take your account out of Evaluation Mode.') . "</td></tr>
		</table><br><br>
		</form>";
	}
	else if ($show_trial_alert)
	{
		$str .= "<table cellpadding=8 style='border:solid 1px #bbbbbb' bgcolor='#f6f6f6' width='100%'><tr><td valign='middle'>&nbsp;</td><td valign='middle' align='center'>";
		$str .= "<b>" . speak('Evaluation Mode') . "</b>: $show_trial_message";
		$str .= "</td></tr></table><br><br>";
	}

	return $str;
}


function show_billing_status_alert_old()
{
	global $p_companyid;
	global $p_dataname;
	global $p_collection_lockout;

	$str = "";
	if(admin_loggedin())
	{
		$show_alert = false;
		$show_trial_alert = false;
		$collection_due_date_message = "";
		$show_trial_message = "";
		$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$p_companyid);
		while($status_read = $status_query->fetch_assoc())
		{
			if($status_read['trial_end']!="" && $status_read['license_applied']=="")
			{
				$show_trial_alert = true;
				$show_trial_message = "";

				$trial_due_parts = explode(" ",$status_read['trial_end']);
				$trial_due_date = $trial_due_parts[0];

				$due_parts = explode("-",$trial_due_date);
				if(count($due_parts) > 2)
				{
					$current_ts = time();
					$due_ts = mktime(0,0,0,$due_parts[1],$due_parts[2],$due_parts[0]);
					$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
					if($diff_days > 1)
						$show_trial_message = "<b>$diff_days</b> Days left";
					else if($diff_days==1)
						$show_trial_message = "<b>$diff_days</b> Day left";
					else if($diff_days==0)
						$show_trial_message = "<b>Today</b> is your last day!";
					else
					{
						$show_trial_message = "Please contract your distributor to resume use of POSLavu";
						$p_collection_lockout = true;
					}
				}
			}
			else if($status_read['ok']=="1")
			{
			}
			else if($status_read['collection_active']!="1")
			{
			}
			else if($status_read['hosting_action']!="" && $status_read['hosting_taken']!="1")
			{
				// disable old collection system
				//$show_alert = true;
			}
			else if($status_read['collect']!="" && $status_read['collection_taken']!="1")
			{
				// disable old collection system
				//$show_alert = true;
			}

			if(!$show_alert)
			{
				$due_info = $status_read['due_info'];
				$due_total = get_due_prop("total",$due_info);
				$due_since = get_due_prop("due",$due_info);

				if($due_total!="" && $due_since!="")
				{
					if($due_total * 1 > 0 && $due_since < date("Y-m-d"))
					{
						$created_query = mlavu_query("select `created` as `created` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$p_companyid);
						if($created_query->num_rows)
						{
							$create_read = $created_query->fetch_assoc();
							$create_date = substr($create_read['created'],0,10);


							$arbDateQuery = mlavu_query("select arb_license_start from poslavu_MAIN_db.signups where dataname= '".$p_dataname."'");

							$arbRead = $arbDateQuery->fetch_assoc();
							$arbStart = $arbRead['arb_license_start'];

							$createDateObject = strtotime($create_date);
							$arbStartDateObject = strtotime($arbStart);

							$createdPlusFifteen = strtotime('+14 day', $createDateObject);



							$lateAllowed = 0;
							if($arbStartDateObject > $createdPlusFifteen ){
								$lateAllowed = ($arbStartDateObject - $createdPlusFifteen)/24/60/60;
							}

							$dueSincePlusLateAllowed = date("Y-m-d",strtotime('+'.$lateAllowed.' day', strtotime($due_since)));

							if($_SERVER['REMOTE_ADDR']=="74.95.18.90")
							{
								//echo "duesincepluselateallowd: $dueSincePlusLateAllowed<br>";
								//echo "Due Total: $due_total<br>Due Since: $due_since<br>";
								//$show_alert = true;
							}
						}
					}
				}
			}

			$needs_to_collect = false;

			$collection_due_date = $status_read['collection_due_date'];
			if($collection_due_date!="" && $need_to_collect)//$status_read['collection_taken']!="1")
			{
				$due_parts = explode("-",$collection_due_date);
				if(count($due_parts) > 2)
				{
					$current_ts = time();
					$due_ts = mktime(0,0,0,$due_parts[1],$due_parts[2],$due_parts[0]);
					$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
					if($diff_days > 1)
						$collection_due_date_message = "<b>$diff_days</b> Days left to use POSLavu without resolution";
					else if($diff_days==1)
						$collection_due_date_message = "<b>$diff_days</b> Day left to use POSLavu without resolution";
					else if($diff_days==0)
						$collection_due_date_message = "<b>Today</b> is your last day left to use POSLavu without resolution";
					else
					{
						$collection_due_date_message = "Please resolve your billing issues to resume use of POSLavu";
						$p_collection_lockout = true;
					}
				}
			}
		}

		if($p_collection_lockout)
			$show_alert = true;

		if($show_trial_alert)
		{
			$str .= "<table cellpadding=8 style='border:solid 1px #bbbbbb' bgcolor='#f6f6f6' width='100%'><tr><td valign='middle'>&nbsp;</td><td valign='middle' align='center'>";
			$str .= "<b>Evaluation Mode</b>: $show_trial_message";
			$str .= "</td></tr></table><br><br>";
		}
		else if($show_alert)
		{
			$str .= "<table cellpadding=8 style='border:solid 1px #bbbbbb' bgcolor='#f6f6f6'><tr><td valign='middle'><img src='images/little_lavu.png' /></td><td valign='middle' align='center'>";
			$str .= "<b>Important</b>: There is a billing issue with your account.<br><a href='index.php?mode=billing'>Please click here for more information</a><br><br>";
			$str .= $collection_due_date_message;
			$str .= "</td></tr></table><br><br>";
		}
	}
	return $str;
}
