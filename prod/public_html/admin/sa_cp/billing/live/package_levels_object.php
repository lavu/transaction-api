<?php

	//if (class_exists('package_container'))
	//	return;

	$a_package_levels_object_global_var_packages = NULL;
	$a_package_levels_object_global_var_upgrades = NULL;
	$a_package_levels_object_global_var_attributes_to_package = NULL;

	/******************************************************************************************
	 * A centralized location for package information
	 * The best method for use is to just create a new package_container and then query it for information via its methods.
	 * To add new packages or upgrades, look at the init_packages() and init_upgrades() methods of the package_container class.
	 *
	 * Benjamin G. Bean
	 * benjamin@poslavu.com
	 ******************************************************************************************/

	class package_container {
		private $a_packages = array();
		private $a_attributes_to_package = array();
		private $a_upgrades = array();
		private $a_upgrade_equivalents = array();

		function __construct() {
			$this->init_packages();
			$this->init_upgrades();
			$this->init_upgrade_equivalents();
		}

		/******************************************************************************************
		 * P U B L I C   F U N C T I O N S (very specific)
		 ******************************************************************************************/

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		// examples:
		// get_printed_name_by_attribute('name', 'Gold')
		// get_printed_name_by_attribute('value', '1495')
		// get_printed_name_by_attribute('recurring', '49.95')
		// get_printed_name_by_attribute('min value', '1000')
		// get_printed_name_by_attribute('min recurring', '40')
		// get_printed_name_by_attribute('level', '2')
		public function get_printed_name_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return '';
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_printed_name();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_plain_name_by_attribute($s_attribute_name, $s_attribute_value) {
			/*if ($_SERVER['REMOTE_ADDR'] == '74.95.18.90')
				error_log($s_attribute_name.":".$s_attribute_value);*/
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return '';
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_plain_name();
		}

		public function get_plain_name_by_level($level) {
			$plain_name = '';
			$level_intval = intval($level);
			$a_packages = $this->get_packages();
			for ( $i = 0; $i < count($a_packages); $i++ ) {
				$i_level = $a_packages[$i]->get_level();

				if ( $i_level === $level_intval ) {
					$plain_name = $a_packages[$i]->get_plain_name();
					break;
				}
			}
			return $plain_name;
		}

		// Similar to get_plain_name_by_level() except it returns the plain/display name of the level's baselevel ancestor.
		public function get_baselevel_plain_name_by_level($level) {
			$plain_name = '';
			$a_packages = $this->get_packages();
			for ( $i = 0; $i < count($a_packages); $i++ ) {
				$i_level = $a_packages[$i]->get_level();

				if ( $i_level === intval($level) ) {
					$plain_name = $this->get_plain_name_by_level( $a_packages[$i]->get_baselevel() );
					break;
				}
			}
			return $plain_name;
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_value_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0.0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_value();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_recurring_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0.0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_recurring();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_recurring_points_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_recurring_points();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_recurring_cmsn_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0.0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_recurring_cmsn();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_level_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_level();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function is_points_package_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->is_points_package();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function is_cmsn_package_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->is_cmsn_package();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_ipod_limt_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_ipod_limit();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_ipad_limt_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return 0;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_ipad_limit();
		}

		// 
		/**
		* Get tableside ipad limit by attribue
		* Attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		* @param $s_attribute_name, $s_attribute_value
		* @return tableside ipad limit
		* Ticket: LP-10857
		*/
		public function getTablesideIpadLimtByAttribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])){
				return 0;
			} 
			$packageObject = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $packageObject->getTablesideIpadLimit();
		}

		// attribute name should be one of ['name', 'value', 'recurring', 'min value', 'min recurring', or 'level']
		public function get_package_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return NULL;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package;
		}

		// attribute name should be one of ['name', 'level']
		public function get_ipad_cost_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return null;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_ipad_cost();
		}

		// attribute name should be one of ['name', 'level']
		public function get_ipad_cmsn_by_attribute($s_attribute_name, $s_attribute_value) {
			if (!isset($this->a_attributes_to_package[$s_attribute_name][$s_attribute_value])) return null;
			$o_package = $this->a_attributes_to_package[$s_attribute_name][$s_attribute_value];
			return $o_package->get_ipad_cmsn();
		}

		// check if a package exists or upgrade exists by the given name
		// returns "package", "upgrade", or "none"
		public function get_package_status($s_package_name) {
			$s_package_name = trim($s_package_name);
			foreach($this->a_packages as $o_package)
				if ($o_package->get_plain_name() == $s_package_name)
					return "package";
			foreach($this->a_upgrades as $o_upgrade)
				if ($s_package_name == $o_upgrade->get_plain_name())
					return "upgrade";
			return "none";
		}

		// shortcut function for compare_package_by_level()
		public function compare_package_by_name($p1_name, $p2_name) {
			return $this->compare_package_by_level($p1_name, $p2_name, 'name');
		}

		// compares the packages and returns
		// -1 if the first package is of lesser value, 0 if the they are equal, or 1 if the second package is of lesser value
		// it does the comparison based on package value, first, and then by package recurring cost
		// returns: -1, 0, or 1
		public function compare_package_by_level($p1_level, $p2_level, $s_comparitor = 'level') {
			$i_cost_1 = $this->get_value_by_attribute($s_comparitor, $p1_level);
			$i_cost_2 = $this->get_value_by_attribute($s_comparitor, $p2_level);
			if ($i_cost_1 < $i_cost_2) {
				return -1;
			} else if ($i_cost_1 == $i_cost_2) {
				$i_recurring_1 = $this->get_recurring_by_attribute($s_comparitor, $p1_level);
				$i_recurring_2 = $this->get_recurring_by_attribute($s_comparitor, $p2_level);
				if ($i_recurring_1 < $i_recurring_2) {
					return -1;
				} else if ($i_recurring_1 == $i_recurring_2) {
					return 0;
				} else {
					return 1;
				}
			} else {
				return 1;
			}
		}

		// returns an array with the old and new names, or
		// returns an empty array
		public function get_upgrade_old_new_names($s_upgrade_name) {
			if (!strpos($s_upgrade_name, " to "))
				return array();
			return explode(" to ", $s_upgrade_name);
		}

		// returns the upgrade's printed name, or
		// empty string
		public function get_upgrade_printed_name($s_upgrade_name) {
			foreach($this->a_upgrades as $o_upgrade)
				if ($s_upgrade_name == $o_upgrade->get_plain_name())
					return $o_upgrade->get_printed_name();
			return '';
		}

		// pass in the names of the old and new packages for the upgrade
		// returns the cost, or -1 if the upgrade doesn't exist
		public function get_upgrade_cost_by_old_new_names($s_old_package_name, $s_new_package_name) {
			foreach($this->a_upgrades as $o_upgrade) {
				$s_upgrade_old_name = $o_upgrade->get_old_name();
				$s_upgrade_new_name = $o_upgrade->get_new_name();
				if ($s_upgrade_old_name == $s_old_package_name && $s_upgrade_new_name == $s_new_package_name)
					return (int)($o_upgrade->get_upgrade_cost());
			}
			return -1;
		}

		// returns a list of available packages available as upgrades from the current package
		public function get_available_upgrades($s_current_package_name, $b_full_name = FALSE) {
			$a_retval = array();
			foreach($this->a_upgrades as $o_upgrade)
				if ($o_upgrade->get_old_name() == $s_current_package_name)
					if($b_full_name)
						$a_retval[] = $o_upgrade->get_plain_name();
					else
						$a_retval[] = $o_upgrade->get_new_name();
			return $a_retval;
		}

		// returns TRUE if the upgrade matches the license or an equivalent matches
		// FALSE if there is not match
		public function upgrade_matches_license($i_upgrade_level, $i_license_level) {
			// sanitize the input
			$i_upgrade_level = (int)$i_upgrade_level;
			$i_license_level = (int)$i_license_level;

			// simple check if they match exactly
			if ($i_upgrade_level == $i_license_level)
				return TRUE;

			// advanced check if they match equivalently
			foreach($this->a_upgrades as $o_upgrade) {
				// check for matches between the upgrade level and the license level
				$i_from_level = $o_upgrade->get_old_package()->get_level();
				if ($i_from_level == $i_license_level) {
					foreach($this->a_upgrade_equivalents as $o_upgrade_equivalent) {
						if ($o_upgrade_equivalent->upgrades_match($i_from_level, $i_upgrade_level))
							return TRUE;
					}
				}
			}

			return FALSE;
		}

		/******************************************************************************************
		 * P U B L I C   F U N C T I O N S (less specific)
		 ******************************************************************************************/

		public function get_packages() {
			return $this->a_packages;
		}
		public function get_upgrades() {
			return $this->a_upgrades;
		}

		// returns the names for packages without 'lite' in their names (case insensitive)
		public function get_not_lavulite_names() {
			$a_retval = array();
			foreach($this->get_packages_not_lavulite() as $o_package)
				$a_retval[] = $o_package->get_plain_name();
			return $a_retval;
		}

		// returns the values for packages without 'lite' in their names (case insensitive)
		public function get_not_lavulite_values() {
			$a_retval = array();
			foreach($this->get_packages_not_lavulite() as $o_package)
				$a_retval[] = $o_package->get_value();
			return $a_retval;
		}

		// gets the packages with levels between $i_lower_level and $i_upper_level, inclusive
		public function get_baselevel_packages($i_lower_level=1, $i_upper_level=3) {
			$a_retval = array();
			foreach($this->a_attributes_to_package['level'] as $k=>$o_package) {
				if ((int)$k >= $i_lower_level && (int)$k <= $i_upper_level) {
					$a_retval[] = $o_package;
				}
			}
			return $a_retval;
		}

		// returns the names for packages without 'lite' in their names (case insensitive)
		public function get_upgrades_names() {
			$a_retval = array();
			foreach($this->get_upgrades() as $o_package)
				$a_retval[] = $o_package->get_plain_name();
			return $a_retval;
		}

		// returns the highest package names for the silver, gold, and platinum licenses
		// ranked by package level
		public function get_highest_base_package_names() {
			$a_highest = array(
				'Silver'      => array('Silver',1),
				'Gold'        => array('Gold',2),
				'Platinum'    => array('Platinum',3),
				'FitSilver'   => array('FitSilver',5),
				'FitGold'     => array('FitGold',6),
				'FitPro'      => array('FitPro',7),
				'Lavu88'      => array('Lavu88',8),
				'LavuGive'    => array('LavuGive',9),
				'Hospitality' => array('Hospitality',10),
			);
			$a_packages = $this->get_packages();
			for ($i = 0; $i < count($a_packages); $i++) {
				$i_level = $a_packages[$i]->get_level();
				$i_baselevel = $a_packages[$i]->get_baselevel();
				$s_name = $this->get_plain_name_by_attribute('level', $i_baselevel);
				if (isset($a_highest[$s_name]) && $i_level > $a_highest[$s_name][1])
					$a_highest[$s_name] = array($a_packages[$i]->get_plain_name(), $i_level);
			}
			return array(
				$a_highest['Silver'][0],
				$a_highest['Gold'][0],
				$a_highest['Platinum'][0],
				$a_highest['FitSilver'][0],
				$a_highest['FitGold'][0],
				$a_highest['FitPro'][0],
				$a_highest['Lavu88'][0],
				//$a_highest['LavuGive'][0],
				$a_highest['Hospitality'][0],
			);
		}

		// similar to get_highest_base_package_names() (where the returned array values are the highest level package names) except the returned array's keys are the base levels.
		public function get_highest_baselevel_package_names() {
			$a_highest = array();
			$a_packages = $this->get_packages();

			for ( $i = 0; $i < count($a_packages); $i++ ) {
				$i_level = $a_packages[$i]->get_level();
				$i_baselevel = $a_packages[$i]->get_baselevel();
				$i_highest_name = isset($a_highest[$i_baselevel]) ? $a_highest[$i_baselevel] : '';
				$i_highest_level = empty($i_highest_name) ? 0 : $this->get_level_by_attribute('name', $i_highest_name);

				if ( $i_level > $i_highest_level )
					$a_highest[$i_baselevel] = $a_packages[$i]->get_plain_name();
			}

			ksort($a_highest);  // Order the return array by its base levels.

			return $a_highest;
		}


		// similar to get_highest_baselevel_package_names() except the returned array's values are the highest package level numbers.
		public function get_highest_baselevel_package_levels() {
			$a_highest = array();
			$a_packages = $this->get_packages();

			for ( $i = 0; $i < count($a_packages); $i++ ) {
				$i_level = $a_packages[$i]->get_level();
				$i_baselevel = $a_packages[$i]->get_baselevel();
				$i_highest_level = isset($a_highest[$i_baselevel]) ? $a_highest[$i_baselevel] : 0;

				if ( $i_level > $i_highest_level )
					$a_highest[$i_baselevel] = $i_level;
			}

			ksort($a_highest);  // Order the return array by its base levels.

			return $a_highest;
		}


		// returns the names for packages with levels between lower_level and upper_level
		public function get_baselevel_names($i_lower_level=1, $i_upper_level=3) {
			$a_retval = array();
			foreach($this->get_baselevel_packages($i_lower_level, $i_upper_level) as $o_package)
				$a_retval[] = $o_package->get_plain_name();
			return $a_retval;
		}

		// returns the values for packages with levels between lower_level and upper_level
		public function get_baselevel_values($i_lower_level=1, $i_upper_level=3) {
			$a_retval = array();
			foreach($this->get_baselevel_packages($i_lower_level, $i_upper_level) as $o_package)
				$a_retval[] = $o_package->get_value();
			return $a_retval;
		}

		// returns the minimum values for packages with levels between lower_level and upper_level
		public function get_baselevel_min_values($i_lower_level=1, $i_upper_level=3) {
			$a_retval = array();
			foreach($this->get_baselevel_packages($i_lower_level, $i_upper_level) as $o_package)
				$a_retval[] = $o_package->get_min_value();
			return $a_retval;
		}

		// returns the minimum values for packages with levels between lower_level and upper_level
		public function get_baselevel_min_recurring($i_lower_level=1, $i_upper_level=3) {
			$a_retval = array();
			foreach($this->get_baselevel_packages($i_lower_level, $i_upper_level) as $o_package)
				$a_retval[] = $o_package->get_min_recurring();
			return $a_retval;
		}

		// returns the actual name for a package with the given mapped name
		public function get_mapped_license_name($s_mapped_name) {
			$i_package_level = 0;
			if($s_mapped_name=="Grant Silver License")
				$i_package_level = 1;
			if($s_mapped_name=="Grant Gold License")
				$i_package_level = 2;
			if($s_mapped_name=="Grant Platinum License")
				$i_package_level = 3;
			if ($i_package_level)
				return $this->get_plain_name_by_attribute('level', $i_package_level);
			return '';
		}

		// get all of the license levels that have a similar name to this license
		// eg, returns array(1,11,21) for 1 or "Silver"
		public function get_levels_of_similar_licenses($i_license_level, $s_license_name = "") {

			// verify the license level
			if ($i_license_level === 0) {
				$i_license_level = $this->get_level_by_attribute('name', $s_license_name);
			}
			if ($i_license_level === 0 || $this->get_level_by_attribute('level', $i_license_level) === 0) {
				return array();
			}

			$license_level_package = $this->a_attributes_to_package['level'][$i_license_level];
			$i_baselevel = $license_level_package->get_baselevel();
			$s_basename = $this->get_plain_name_by_attribute('level', $i_baselevel);

			// get all licenses that are similar
			$a_retval = array($i_baselevel);
			foreach($this->a_packages as $o_package) {
				if ($o_package->get_baselevel() === $i_baselevel && $o_package->get_plain_name() !== $s_basename) {
					$a_retval[] = $o_package->get_level();
				}
			}
			return $a_retval;
		}

		// gets all packages that should be generating recurring points
		public function get_recurring_points_packages() {
			$a_retval = array();
			foreach($this->a_packages as $o_package) {
				if ($o_package->is_points_package()) {
					$a_retval[] = $o_package;
				}
			}
			return $a_retval;
		}

		// gets all packages that should be generating recurring cmsn
		public function get_recurring_cmsn_packages() {
			$a_retval = array();
			foreach($this->a_packages as $o_package) {
				if ($o_package->is_cmsn_package()) {
					$a_retval[] = $o_package;
				}
			}
			return $a_retval;
		}

		/******************************************************************************************
		 * S E T   N E W   L I C E N S E S   A N D   U P G R A D E S   H E R E
		 ******************************************************************************************/

		private function init_packages() {
			global $a_package_levels_object_global_var_packages;
			if (!isset($a_package_levels_object_global_var_packages) || $a_package_levels_object_global_var_packages === NULL) {
				// key:
				//     1:  $s_name, the actual package name (what lavu users see) aka plain name
				//     2:  $s_display_name, what the end user sees
				//     3:  $i_license_cost, the license cost for this package
				//     4:  $i_recurring_cost, the monthly cost for this package
				//     5:  $i_recurring_points, the points awarded every month
				//     6:  $f_recurring_cmsn, the cmsn earned every month
				//     7:  $i_min_website_dollar_value, the minimum license cost to mark a license payment as this package
				//     8:  $i_min_recurring_cost, the minimum recurring cost to mark a recurring monthly payment as this package
				//     9:  $i_level, the package number associated with the package
				//    10:  $i_baselevel, the lowest ancestor package level to which it is a successor
				//    11:  $i_functional_baselevel, the base level the package functions like - even if it is not an ancestor
				//    12:  $b_points_package, should points be awarded for monthly payments on this package?
				//    13:  $b_cmsn_package, is cmsn earned for monthly payments on this package?
				//    14:  $i_ipod_limit, the number of ipods this package is limited to by default
				//    15:  $i_ipad_limit, the number of ipads this package is limited to by default
				//    16:  $f_ipad_cost, the additional monthly cost for each ipad past the package limit
				//    17:  $f_ipad_cmsn, the cmsn earned for each ipad past the package limit
				//                                1                    2                            3        4    5      6        7       8     9  10  11     12    13   14  15      16    17
				$this->a_packages[] = new package("Silver",            "Silver",                895.0,   29.95,   0,  0.00,     500,     10,    1,  1,  1, false, true,   1,  1,  20.00, 0.00);
				$this->a_packages[] = new package("Gold",              "Gold",                 1495.0,   49.95,   0,  0.00,    1000,     40,    2,  2,  2, false, true,  10,  2,  20.00, 0.00);
				$this->a_packages[] = new package("Platinum",          "Platinum",             3495.0,   99.95,   0,  0.00,    2400,     80,    3,  3,  3, false, true, 200, 10,  20.00, 0.00);

				$this->a_packages[] = new package("Lite",              "Lite",                    0.0,   39.0,    0,  0.00,  9000.0,   9000,    4,  4,  4, false, true,   0,  0,  20.00, 0.00); // we don't want customers to accidentally be marked as lavulite, so make their min values >9000

				$this->a_packages[] = new package("FitSilver",         "Fit Silver",            895.0,  149.00,  10,  2.50,     500,     10,    5,  5,  1, false, true,   1,  1,  20.00, 0.00);  // ** Silver clone - so keep its attributes in sync! **
				$this->a_packages[] = new package("FitGold",           "Fit Gold",             1495.0,  149.00,  40, 10.00,    1000,     40,    6,  6,  2, false, true,  10,  2,  20.00, 0.00);  // ** Gold clone   - so keep its attributes in sync! **
				$this->a_packages[] = new package("FitPro",            "Fit Pro",              2495.0,  149.00,  80, 20.00,    2400,     80,    7,  7,  3, false, true,  15,  3,  20.00, 0.00);  // ** Pro clone    - so keep its attributes in sync! **

				$this->a_packages[] = new package("Lavu88",            "Lavu 88",                 0.0,   88.00,  40, 10.00,       0,      0,    8,  8,  1, false, true,   1,  1,  20.00, 0.00);  // ** Silver clone - so keep its attributes in sync! **

				$this->a_packages[] = new package("LavuGive",          "Give",                  895.0,   39.00,  10,  2.50,     500,     10,    9,  9,  1, false, true,   1,  1,  20.00, 0.00);  // ** Silver clone - so keep its attributes in sync! **

				$this->a_packages[] = new package("Hospitality",       "Hospitality",          1995.0,  199.00,  50, 12.50,    1000,    199,   10, 10,  1, false, true,  10,  2,  20.00, 0.00);

				$this->a_packages[] = new package("Silver2",           "Silver",                895.0,   39.00,  10,  2.50,     500,     10,   11,  1,  1, false, true,   1,  1,  20.00, 0.00);
				$this->a_packages[] = new package("Gold2",             "Gold",                 1495.0,   79.00,  40, 10.00,    1000,     40,   12,  2,  2, false, true,  10,  2,  20.00, 0.00);
				$this->a_packages[] = new package("Platinum2",         "Platinum",             3495.0,  149.00,  80, 20.00,    2400,     80,   13,  3,  3, false, true, 200, 10,  20.00, 0.00);

				$this->a_packages[] = new package("GoldTrial",         "Gold Trial",           1495.0,   79.00,  40, 10.00,    1000,     40,   14, 14,  2, false, true,  10,   2, 20.00, 0.00);  // Based on Gold3
				$this->a_packages[] = new package("ProTrial",          "Pro Trial",            2495.0,  149.00,  80, 20.00,    2400,     80,   15, 15,  3, false, true,  15,   3, 20.00, 0.00);  // Based on Pro aka Platinum3
				$this->a_packages[] = new package("Lavu88Trial",       "Lavu 88 Trial",           0.0,    0.00,  40, 10.00,       0,      0,   16, 16,  1, false, true,   1,   1, 20.00, 0.00);

				$this->a_packages[] = new package("MercuryFreeLavu88", "Mercury Free Lavu 88",    0.0,    0.00,  40, 10.00,       0,      0,   17, 17,  1, false, true,   1,   1, 20.00, 0.00);
				$this->a_packages[] = new package("MercuryGold",       "Mercury Gold",         1495.0,    0.00,  40, 10.00,    1000,     40,   18, 18,  2, false, true,  10,   2, 20.00, 0.00);  // Based on Gold3
				$this->a_packages[] = new package("MercuryPro",        "Mercury Pro",          2495.0,   70.00,  80, 20.00,    2400,     80,   19, 19,  3, false, true,  15,   3, 20.00, 0.00);  // Based on Pro aka Platinum3

				$this->a_packages[] = new package("EvoSnapFreeLavu88", "EVO Snap Free Lavu 88",   0.0,    0.00,  40, 10.00,       0,      0,   20, 20,  1, false, true,   1,   1, 20.00, 0.00);

				$this->a_packages[] = new package("Silver3",           "Silver",                895.0,   39.00,  10,  2.50,     500,     10,   21,  1,  1, false, true,   1,   1, 20.00, 0.00);  // ** Changes to Silver should be echoed to Lavu88, LavuGive! **
				$this->a_packages[] = new package("Gold3",             "Gold",                 1495.0,   79.00,  40, 10.00,    1000,     40,   22,  2,  2, false, true,  10,   2, 20.00, 0.00);
				$this->a_packages[] = new package("Platinum3",         "Pro",                  2495.0,  149.00,  80, 20.00,    2400,     80,   23,  3,  3, false, true,  15,   3, 20.00, 0.00);

				// Don't use package level 24 - there are 15 old signup rows with it already

				$this->a_packages[] = new package("Lavu",              "Lavu",                   0.00,   79.00,   0, 10.00,       0,      0,   25, 25,  3, false, true,   5,   1, 69.00, 10.00);
				$this->a_packages[] = new package("LavuEnterprise",    "Lavu Enterprise",     2495.00,  276.00,   0, 40.00,       0,      0,   26, 26,  3, false, true,  15,   5, 20.00,  3.40);

				$this->a_packages[] = new package("LavuSE",            "LavuSE",                 0.00,   79.00,   0, 10.00,       0,      0,   27, 27,  3, false, true,   0,   0, 69.00, 10.00);  // TO DO : get base terminal amount

				$a_package_levels_object_global_var_packages = $this->a_packages;
			} else {
				$this->a_packages = $a_package_levels_object_global_var_packages;
			}
			$this->map_attributes_to_package();
		}

		private function init_upgrades() {
			global $a_package_levels_object_global_var_upgrades;
			if (!isset($a_package_levels_object_global_var_upgrades) || $a_package_levels_object_global_var_upgrades === NULL) {
				$this->a_upgrades[] = $this->create_generic_upgrade('Silver',    'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('Silver2',   'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('Silver3',   'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('Gold',      'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('Gold2',     'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('Gold3',     'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('Lavu88',    'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('FitSilver', 'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('FitGold',   'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('FitPro',    'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('LavuGive',  'Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('Lavu88Trial','Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('MercuryFreeLavu88','Lavu');
				$this->a_upgrades[] = $this->create_generic_upgrade('EvoSnapFreeLavu88','Lavu');
				$a_package_levels_object_global_var_upgrades = $this->a_upgrades;
			} else {
				$this->a_upgrades = $a_package_levels_object_global_var_upgrades;
			}
		}

		private function init_upgrade_equivalents() {
			global $a_package_levels_object_global_var_upgrade_equivalents;
			if (!isset($a_package_levels_object_global_var_upgrade_equivalents) || $a_package_levels_object_global_var_upgrade_equivalents === NULL) {
				$this->a_upgrade_equivalents[] = $this->create_upgrade_equivalent('Silver',  'Platinum3', 'Silver2', 'Platinum3');
				$this->a_upgrade_equivalents[] = $this->create_upgrade_equivalent('Silver',  'Platinum3', 'Silver3', 'Platinum3');
				$this->a_upgrade_equivalents[] = $this->create_upgrade_equivalent('Silver2', 'Platinum3', 'Silver3', 'Platinum3');
				$this->a_upgrade_equivalents[] = $this->create_upgrade_equivalent('Gold',    'Platinum3', 'Gold2',   'Platinum3');
				$this->a_upgrade_equivalents[] = $this->create_upgrade_equivalent('Gold',    'Platinum3', 'Gold3',   'Platinum3');
				$this->a_upgrade_equivalents[] = $this->create_upgrade_equivalent('Gold2',   'Platinum3', 'Gold3',   'Platinum3');
				$a_package_levels_object_global_var_upgrade_equivalents = $this->a_upgrade_equivalents;
			} else {
				$this->a_upgrade_equivalents = $a_package_levels_object_global_var_upgrade_equivalents;
			}
		}

		/******************************************************************************************
		 * P R I V A T E   F U N C T I O N S
		 ******************************************************************************************/

		// create upgrade equivalents (for use with 'upgrade_matches_license()')
		// returns an upgrade_equivalent object or NULL on failure
		private function create_upgrade_equivalent($s_from_a, $s_to_a, $s_from_b, $s_to_b) {
			$o_retval = NULL;
			foreach($this->a_upgrades as $o_upgrade) {
				if ($o_upgrade->get_old_name() == $s_from_a && $o_upgrade->get_new_name() == $s_to_a) {
					$o_a = $o_upgrade->get_old_package();
				}
				if ($o_upgrade->get_old_name() == $s_from_b && $o_upgrade->get_new_name() == $s_to_b) {
					$o_b = $o_upgrade->get_old_package();
				}
			}
			if (isset($o_a) && isset($o_b)) {
				$i_from_a = $o_a->get_level();
				$i_from_b = $o_b->get_level();
				$o_retval = new pc_upgrade_equivalent($i_from_a, $i_from_b);
			}
			return $o_retval;
		}

		private function get_packages_not_lavulite() {
			$a_retval = array();
			foreach($this->a_packages as $o_package)
				if (stripos($o_package->get_plain_name(), "lite") === FALSE)
					$a_retval[] = $o_package;
			return $a_retval;
		}

		private function create_generic_upgrade($s_package_name_old, $s_package_name_new, $i_upgrade_cost = -1) {
			$o_old_package = $this->get_package_by_name($s_package_name_old);
			$o_new_package = $this->get_package_by_name($s_package_name_new);
			if ($i_upgrade_cost == -1)
				$i_upgrade_cost = $o_new_package->get_value() - $o_old_package->get_value();
			$o_upgrade = new upgrade($o_old_package, $o_new_package, $i_upgrade_cost);
			return $o_upgrade;
		}

		private function get_package_by_name($s_name) {
			return $this->a_attributes_to_package['name'][$s_name];
		}

		private function map_attributes_to_package() {
			if (!isset($a_package_levels_object_global_var_attributes_to_package) || $a_package_levels_object_global_var_attributes_to_package == NULL) {
				foreach($this->a_packages as $o_package) {
					$this->a_attributes_to_package['name'][(string)$o_package->get_plain_name()] = $o_package;
					$this->a_attributes_to_package['value'][(string)$o_package->get_value()] = $o_package;
					$this->a_attributes_to_package['recurring'][(string)$o_package->get_recurring()] = $o_package;
					$this->a_attributes_to_package['min_value'][(string)$o_package->get_min_value()] = $o_package;
					$this->a_attributes_to_package['min_recurring'][(string)$o_package->get_min_recurring()] = $o_package;
					$this->a_attributes_to_package['level'][(string)$o_package->get_level()] = $o_package;
					$this->a_attributes_to_package['baselevel'][(string)$o_package->get_baselevel()] = $o_package;
				}
				$a_package_levels_object_global_var_attributes_to_package = $this->a_attributes_to_package;
			} else {
				$this->a_attributes_to_package = $a_package_levels_object_global_var_attributes_to_package;
			}
		}
	}

	class pc_upgrade_equivalent {
		private $i_from_a = 0;
		private $i_from_b = 0;

		function __construct($i_from_a, $i_from_b) {
			$this->i_from_a = $i_from_a;
			$this->i_from_b = $i_from_b;
		}

		public function upgrades_match($i_from_a, $i_from_b) {
			if ($i_from_a == $i_from_b || (
					($i_from_a == $this->i_from_a && $i_from_b == $this->i_from_b) ||
					($i_from_a == $this->i_from_b && $i_from_b == $this->i_from_a)
				)
			) {
				return TRUE;
			}
			return FALSE;
		}
	}

	// a container of information for packages
	class package {
		private $s_name = "";
		private $s_display_name = "";
		private $i_license_cost = 0;
		private $i_recurring_cost = 0;
		private $i_recurring_points = 0;
		private $f_recurring_cmsn = 0.00;
		private $i_min_website_dollar_value = 0;
		private $i_min_recurring_cost = 0;
		private $i_level = 0; // eg 1 for silver, 2 for gold, 3 for platinum, 4 for lavulite, 5 for fitsilver, 6 for fitgold, 7 for fitpro, 8 for lavu88, 9 for lavugive, 10 for hospitality, etc.
		private $i_baselevel = 0;
		private $i_functional_baselevel = 0;
		private $b_points_package = FALSE;
		private $b_cmsn_package = FALSE;
		private $i_ipod_limit = 0;
		private $i_ipad_limit = 0;
		private $tablesideIpadLimit = 0;
		private $f_ipad_cost = 0.00;
		private $f_ipad_cmsn = 0.00;

		function __construct($s_name, $s_display_name, $i_license_cost, $i_recurring_cost, $i_recurring_points, $f_recurring_cmsn, $i_min_website_dollar_value, $i_min_recurring_cost, $i_level, $i_baselevel, $i_functional_baselevel, $b_points_package, $b_cmsn_package, $i_ipod_limit, $i_ipad_limit, $f_ipad_cost, $f_ipad_cmsn,$tablesideIpadLimit=0) {
			$this->s_name = $s_name;
			$this->i_license_cost = $i_license_cost;
			$this->i_recurring_cost = $i_recurring_cost;
			$this->i_recurring_points = $i_recurring_points;
			$this->f_recurring_cmsn = $f_recurring_cmsn;
			$this->i_min_website_dollar_value = $i_min_website_dollar_value;
			$this->i_min_recurring_cost = $i_min_recurring_cost;
			$this->i_level = $i_level;
			$this->i_baselevel = $i_baselevel;
			$this->i_functional_baselevel = $i_functional_baselevel;
			$this->s_display_name = $s_display_name;
			$this->b_points_package = $b_points_package;
			$this->b_cmsn_package = $b_cmsn_package;
			$this->i_ipod_limit = $i_ipod_limit;
			$this->i_ipad_limit = $i_ipad_limit;
			$this->tablesideIpadLimit = $tablesideIpadLimit;
			$this->f_ipad_cost = $f_ipad_cost;
			$this->f_ipad_cmsn = $f_ipad_cmsn;
		}

		public function get_plain_name() {
			return $this->s_name;
		}

		public function get_printed_name() {
			return $this->s_display_name;
		}

		public function get_value() {
			return $this->i_license_cost;
		}

		public function get_recurring() {
			return $this->i_recurring_cost;
		}

		public function get_recurring_points() {
			return $this->i_recurring_points;
		}

		public function get_recurring_cmsn() {
			return $this->f_recurring_cmsn;
		}

		public function get_min_value() {
			return $this->i_min_website_dollar_value;
		}

		public function get_min_recurring() {
			return $this->i_min_recurring_cost;
		}

		public function get_level() {
			return $this->i_level;
		}

		public function get_baselevel() {
			return $this->i_baselevel;
		}

		public function get_functional_baselevel() {
			return $this->i_functional_baselevel;
		}

		public function is_points_package() {
			return $this->b_points_package;
		}

		public function is_cmsn_package() {
			return $this->b_cmsn_package;
		}

		public function get_ipod_limit() {
			return $this->i_ipod_limit;
		}

		public function get_ipad_limit() {
			return $this->i_ipad_limit;
		}
		/*
		  This function will return tableside ipad limit
		*/
		public function getTablesideIpadLimit() {
			return $this->tablesideIpadLimit;
		}


		public function get_ipad_cost() {
			return $this->f_ipad_cost;
		}

		public function get_ipad_cmsn() {
			return $this->f_ipad_cmsn;
		}

		private function create_printed_name() {
			if (strpos(' ', $this->s_name) === FALSE)
				return ucfirst($this->s_name);
			$a_name = explode(' ', $this->s_name);
			foreach($a_name as $k=>$v)
				$a_name[$k] = ucfirst($v);
			return implode(' ', $a_name);
		}
	}

	// a container of information for upgrades
	class upgrade {
		private $o_old_package = NULL;
		private $o_new_package = NULL;
		private $i_upgrade_cost = 0;

		function __construct($o_old_package, $o_new_package, $i_cost) {
			$this->o_old_package = $o_old_package;
			$this->o_new_package = $o_new_package;
			$this->i_upgrade_cost = $i_cost;
		}

		public function get_plain_name() {
			$s_old_name = $this->o_old_package->get_plain_name();
			$s_new_name = $this->o_new_package->get_plain_name();
			return "$s_old_name to $s_new_name";
		}

		public function get_printed_name() {
			$s_old_name = $this->o_old_package->get_printed_name();
			$s_new_name = $this->o_new_package->get_printed_name();
			return "$s_old_name to $s_new_name";
		}

		public function get_old_name() {
			return $this->o_old_package->get_plain_name();
		}

		public function get_new_name() {
			return $this->o_new_package->get_plain_name();
		}

		public function get_old_package() {
			return $this->o_old_package;
		}

		public function get_new_package() {
			return $this->o_new_package;
		}

		public function get_upgrade_cost() {
			return $this->i_upgrade_cost;
		}
	}

	/*class SilverPackage extends package {
		private static $__silverPackage = NULL;
		private function __construct(){
			;
		}

		public static function getSilverPackage() {
			if(!$__silverPackage){
				$__silverPackage = new SilverPackage();
			}
			return $__silverPackage;
		}
	};
	class Silver2Package : SilverPackage{};

	$obj instanceof SilverPackage;
	*/
?>
