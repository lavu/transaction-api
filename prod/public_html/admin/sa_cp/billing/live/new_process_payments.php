<?php
	//ini_set("display_errors","1");
	require_once(dirname(__FILE__) . "/../procareas/procpay_functions.php");
	require_once(dirname(__FILE__) . "/../../../manage/can_access.php");
	$a_bill_ids_drawn = array();

	function process_payments($p_area, $dataname, $package="", $signup_status="")
	{
		$output = "";
		$bill_types_due = "";
		$all_bills_due = "";
		$set_due_info = "";
		global $a_bill_ids_drawn;
		global $billing_props;

		if($dataname!="")
		{
			//------------------------------------------------------- Setup Important Variables
			require(dirname(__FILE__) . "/../procareas/setup_vars.php");
			//-------------------------------------------------- Process requests from user
			require(dirname(__FILE__) . "/../procareas/process_requests.php");
			//-------------------------------------------------- Read payments from the payment_responses table and apply distributor licenses
			$payments = read_payment_responses_get_array_choose_function($a_license_values, $a_license_dates, $custom_monthly_hosting, $license_value, $license_date_paid, $rest_id);
			//-------------------------------------------------- Read invoices from the db or indicate that they should be created if they do not exist
			$billable_info = load_billable_invoices($props,$lic_amount,$dataname);

			#Sort the billable invoices on due date
			$billableData = array();
			foreach($billable_info['billable'] as $billables){
				$billableData[$billables[0]] = $billables; 
			} 
			krsort($billableData);
	
			$billable = array_values($billableData);

			$last_hosting_bill_date = $billable_info['last_hosting_bill_date'];

			$output .= common_billing_javascripts_tostr();

			$bid_prefix = "";
			$pid_prefix = "";
			if (!$lavu_admin) {
				$bid_prefix = "77-";
				$pid_prefix = "88-";
			}

			//--------------------------------------------------- Loop through payments and credits and establish relationships
			require(dirname(__FILE__) . "/../procareas/parse_payments_and_credits.php");

			$a_bill_is_shown = false;
			$bills_due_list = array();
			$bill_types_due = array();
			$all_bills_due = 0;
			$earliest_past_due = "";
			$tied_declines = array();
			$tied_credited = array();
			$tied_payments = array();

			//----------------------------------------------------- Parse bills and create bill display output
			require(dirname(__FILE__) . "/../procareas/parse_bills.php");

			//----------------------------------------------------- Other Payments (payments that are not applied to bills)
			require(dirname(__FILE__) . "/../procareas/show_other_payments.php");
			//----------------------------------------------------- Additional Administrative Options (buttons and dropdowns)
			require(dirname(__FILE__) . "/../procareas/additional_options.php");
			//----------------------------------------------------- Update Payment status (such as due_info)
			require(dirname(__FILE__) . "/../procareas/process_payment_status.php");

			if(isset($_GET['showtags'])) $output = "<textarea rows='80' cols='200'>" . str_replace("<","&lt;",$output)."</textarea>";
		}

		/*if($dataname=="17edison")
		{
			echo "<textarea rows='20' cols='80'>".str_replace("<","&lt;",$output)."</textarea>";
		}*/

		$billing_props = $props;
		return array(
			"output"=>$output,
			"bill_types_due"=>$bill_types_due,
			"all_bills_due"=>$all_bills_due,
			"due_info"=>$set_due_info,
			"props"=>$props,
			"additional_options_output"=>$additional_options_output,
			"monthly_parts_output"=>$monthly_parts_output,
			"due_string"=>$duestr
		);
	}

	function recordDueInfoAsSystemSetting($rest_id, $dataname, $due_info) {
		
		require_once(dirname(__FILE__)."/../payment_profile_functions.php");
		
		// trial_status: 0 = not in trial, 1 = in trial, 2 = trial expired
		$sysset = array("due_ts"=>"", "due_date"=>"", "trial_status"=>"0");
		
		$get_status_info = mlavu_query("select `trial_end`, `license_applied`, `min_due_date`, `collection_mode`, `uncaptured_amount` from `poslavu_MAIN_db`.`payment_status` where `restaurantid` = '[1]'", $rest_id);
		if (mysqli_num_rows($get_status_info)) {
			$status_info = mysqli_fetch_assoc($get_status_info);	
		}

		$current_ts = time();
		$due_ts = 0;
		
		if (($status_info['uncaptured_amount'] * 1) == 0) {

			$augment_extension = 0;
			switch (date("N")) {
				case 5: $augment_extension = 3; break;
				case 6: $augment_extension = 2; break;
				case 7: $augment_extension = 1; break;
				default: break;	
			}

			if (isset($status_info['trial_end'])) {
		
				// Find out if their license has been waived
				$waived_query = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where `dataname`='[1]' and `type`='License' and `waived`='1' order by id desc", $dataname);
				$license_waived = (mysqli_num_rows($waived_query));
				
				if ($status_info['trial_end']!="" && $status_info['license_applied']=="" && !$license_waived) {
		
					$trial_due_parts = explode(" ", $status_info['trial_end']);
					$trial_due_date = $trial_due_parts[0];
	
					$due_parts = explode("-", $trial_due_date);
					if (count($due_parts) > 2) {
						
						$due_ts = mktime(0, 0, 0, $due_parts[1], $due_parts[2], $due_parts[0]);
	
						$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
						if ($diff_days <= -4) {
							$sysset['trial_status'] = ($diff_days >= 0)?"1":"2";
							$sysset['due_ts'] = ($current_ts + 1209600 + $augment_extension); // display alert for 14 days, starting at 4 days past trial expiration
							$sysset['due_date'] = date("Y-m-d", $sysset['due_ts']);
						}
					}
				}
	
				if ($sysset['trial_status'] == "0") {
					
					$min_due_date = $status_info['min_due_date'];
					
					$rest_created = "";
					$rest_query = mlavu_query("select `created` from `poslavu_MAIN_db`.`restaurants` where `data_name` = '[1]'", $dataname);
					if (mysqli_num_rows($rest_query)) {
						$rest_read = mysqli_fetch_assoc($rest_query);
						$rest_created_parts = explode(" ",$rest_read['created']);
						$rest_created = $rest_created_parts[0];
						if ($rest_created < "2012-01-01") $min_due_date = "2014-09-16";
					}
				
					$collection_mode = $status_info['collection_mode'];
					
					if (($rest_created > "2009-01-01" || $collection_mode=="on") && $collection_mode!="off") {
	
						$due_total = get_due_prop("total", $due_info);
						$due_license = get_due_prop("license", $due_info);
						$due_hosting = get_due_prop("hosting", $due_info);
						$due_since = get_due_prop("due", $due_info);
						$paid_last_15 = get_due_prop("paid last 15", $due_info);
						$paid_last_31 = get_due_prop("paid last 31", $due_info);
						$last_payment_made = get_due_prop("last payment made", $due_info);
						
						if ($due_total!="" && ($due_total * 1)>0) {
								
							$use_paid_last = 15;
							if ($use_paid_last == 31) {
								$paid_last_str = $paid_last_31;
								$paid_last_num = 31;
							} else {
								$paid_last_str = $paid_last_15;
								$paid_last_num = 15;
							}
		
							if ($due_total>0 && ($paid_last_str=="" || ($paid_last_str * 1)==0 || $min_due_date!="")) {
		
								$collection_due_date = ($due_since < "2014-03-20")?"2014-03-20":$due_since;
	
								if ($last_payment_made!="" && $min_due_date=="") {
									$lpm_parts = explode("-", $last_payment_made);
									$lpm_due_ts = mktime(0, 0, 0, $lpm_parts[1], ($lpm_parts[2] + $paid_last_num), $lpm_parts[0]);
									$lpm_due_date = date("Y-m-d", $lpm_due_ts);
									if ($collection_due_date < $lpm_due_date) $collection_due_date = $lpm_due_date;
								}
		
								$due_parts = explode("-", $collection_due_date);
								if (count($due_parts) > 2) {
									
									$due_ts = mktime(0, 0, 0, $due_parts[1], ($due_parts[2] + 5), $due_parts[0]);
		
									if (trim($min_due_date) != "") {
										$mdue = explode("-", $min_due_date);
										if (count($mdue) > 2) {
											$min_due_ts = mktime(0, 0, 0, $mdue[1], $mdue[2], $mdue[0]);
											if ($min_due_ts > $due_ts) $due_ts = $min_due_ts;
										}
									}
	
									$diff_days = floor(($due_ts - $current_ts) / 60 / 60 / 24) + 1;
									if ($diff_days <= -1) {
										$sysset['due_ts'] = ($current_ts + 2592000 + $augment_extension); // display alert for 30 days starting at 1 day past actual due_ts
										$sysset['due_date'] = date("Y-m-d", $sysset['due_ts']);
									}
								}
							}
						}
					}
				}
			}
		}
		
		//$sysset = array("due_ts"=>"", "due_date"=>"", "trial_status"=>"0"); // Overriding because clients are getting locked out that should not -CF
		
		$rdb = "poslavu_".$dataname."_db";
		$get_locations = mlavu_query("SELECT `id`, `use_net_path`, `net_path` FROM `".$rdb."`.`locations`");
		if ($get_locations) {
			if (mysqli_num_rows($get_locations) > 0) {
				while ($loc_info = mysqli_fetch_assoc($get_locations)) {
					$check_config = mlavu_query("SELECT `id`, `value6` FROM `".$rdb."`.`config` WHERE `location` = '[1]' AND `type` = 'system_setting' AND `setting` = 'payment_status'", $loc_info['id']);
					if (mysqli_num_rows($check_config) > 0) {
						$c_info = mysqli_fetch_assoc($check_config);
						$u_vars = array();
						if ($sysset['due_ts'] == "") {
							$u_vars['id'] = $c_info['id'];
							$u_vars['value'] = "";
							$u_vars['value2'] = "";
							$u_vars['value3'] = "0";
							$u_vars['value4'] = "";
							$u_vars['value5'] = "";
							$u_vars['value6'] = "0";
						} else if ($c_info['value6'] != $due_ts) {
							$u_vars['id'] = $c_info['id'];							
							$u_vars['value'] = $sysset['due_ts'];
							$u_vars['value2'] = $sysset['due_date'];
							$u_vars['value3'] = $sysset['trial_status'];
							$u_vars['value4'] = $current_ts;
							$u_vars['value5'] = date("Y-m-d H:i:s", $current_ts);
							$u_vars['value6'] = $due_ts;
						}
						if (count($u_vars) > 0) $update = mlavu_query("UPDATE `".$rdb."`.`config` SET `value` = '[value]', `value2` = '[value2]', `value3` = '[value3]', `value4` = '[value4]', `value5` = '[value5]', `value6` = '[value6]' WHERE `id` = '[id]'", $u_vars);					
					} else {
						$i_vars = array();
						$i_vars['location'] = $loc_info['id'];
						$i_vars['value'] = $sysset['due_ts'];
						$i_vars['value2'] = $sysset['due_date'];
						$i_vars['value3'] = $sysset['trial_status'];
						$i_vars['value4'] = $current_ts;
						$i_vars['value5'] = date("Y-m-d H:i:s", $current_ts);
						$i_vars['value6'] = $due_ts;
						$insert = mlavu_query("INSERT INTO `".$rdb."`.`config` (`location`, `type`, `setting`, `value`, `value2`, `value3`, `value4`, `value5`, `value6`) VALUES ('[location]', 'system_setting', 'payment_status', '[value]', '[value2]', '[value3]', '[value4]', '[value5]', '[value6]')", $i_vars);
					}

					if ((int)$loc_info['use_net_path']>0 && !strstr($loc_info['net_path'], "poslavu.com")) { // if lls, schedule config table sync
						$exist_query = mlavu_query("SELECT `id` FROM `".$rdb."`.`config` WHERE `setting` = 'table updated' AND `location` = '[1]' AND `value` = 'config'", $loc_info['id']);
						if (mysqli_num_rows($exist_query) == 0) mlavu_query("INSERT INTO `".$rdb."`.`config` (`setting`, `location`, `value`, `type`) VALUES ('table updated', '[1]', 'config', 'log')", $loc_info['id']);
					}
				}
			}
		} else error_log("billing show_billing_status_alert get_locations: ".mlavu_dberror());
	}

	function recordPackageInfoAsSystemSetting($dataname, $package_name, $ipod_limit, $ipad_limit,$tablesideIpadLimit=0) {
		$rdb = "poslavu_".$dataname."_db";
		$get_locations = mlavu_query("SELECT `id`, `use_net_path`, `net_path` FROM `".$rdb."`.`locations`");
		if ($get_locations) {
			if (mysqli_num_rows($get_locations) > 0) {
				while ($loc_info = mysqli_fetch_assoc($get_locations)) {
					$check_config = mlavu_query("SELECT `id` FROM `".$rdb."`.`config` WHERE `location` = '[1]' AND `type` = 'system_setting' AND `setting` = 'package_info'", $loc_info['id']);
					if (mysqli_num_rows($check_config) > 0) {
						$c_info = mysqli_fetch_assoc($check_config);
						$update = mlavu_query("UPDATE `".$rdb."`.`config` SET `value` = '[1]', `value2` = '[2]', `value3` = '[3]', `value4` = '[4]' WHERE `id` = '[5]'", $package_name, $ipod_limit, $ipad_limit, $tablesideIpadLimit, $c_info['id']);
					} else{
						$insert = mlavu_query("INSERT INTO `".$rdb."`.`config` (`location`, `type`, `setting`, `value`, `value2`, `value3`, `value4`) VALUES ('[1]', 'system_setting', 'package_info', '[2]', '[3]', '[4]', '[5]')", $loc_info['id'], $sysset_package_name, $sysset_ipod_limit, $sysset_ipad_limit, $syssetTablesideIpadLimit);
					}

					if ((int)$loc_info['use_net_path']>0 && !strstr($loc_info['net_path'], "poslavu.com")) { // if lls, schedule config table sync
						$exist_query = mlavu_query("SELECT `id` FROM `".$rdb."`.`config` WHERE `setting` = 'table updated' AND `location` = '[1]' AND `value` = 'config'", $loc_info['id']);
						if (mysqli_num_rows($exist_query) == 0) lavu_query("INSERT INTO `".$rdb."`.`config` (`setting`, `location`, `value`, `type`) VALUES ('table updated', '[1]', 'config', 'log')", $loc_info['id']);
					}
				}
			}
		} else error_log("process_payments setup_vars get_locations: ".lavu_dberror() . " " . mlavu_dberror());
	}
?>
