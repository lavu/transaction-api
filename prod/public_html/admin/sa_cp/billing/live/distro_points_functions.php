<?php

require_once(dirname(__FILE__)."/../../../manage/misc/AdvancedToolsFunctions/companyReportingExternalFunctions.php");

// returns an array with each entry like array('id'=>id, dataname'=>dataname, 'distro_points_applied'=>distro_points_applied, 'restaurant_name'=>restaurant name, 'billing_log_found'=>TRUE/FALSE)
function get_distro_points_explained_recurring($s_distro_code, &$a_invoices) {
	$s_query_string = NULL;
	$a_invoices = TRUE;
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
	$dbapi->getAllInTable('invoices', array('distro_code_applied_to'=>$s_distro_code), TRUE, array('selectclause'=>'`id`,`dataname`,`distro_points_applied`, date_format(`due_date`, "%c/%e/%Y") AS `formatted_date`'), $s_query_string, $a_invoices);

	// get the ids of the invoices
	$a_details_check = array();
	for($i = 0; $i < count($a_invoices); $i++) {
		$a_details_check[] = "`details` LIKE 'invoice id ".$a_invoices[$i]['id'].": applied points to distro %'";
	}

	// get a billing log for each of the given ids
	$a_billing_logs = array();
	if (count($a_details_check) > 0) {
		$s_details_clause = implode("OR ", $a_details_check);
		unset($a_details_check);
		$dbapi->getAllInTable('billing_log', NULL, TRUE, array('selectclause'=>"SUBSTRING(`details`,12,LOCATE(':',`details`)-12) AS `invoice_id`, date_format(`datetime`, '%c/%e/%Y') AS `formatted_date`", 'whereclause'=>"WHERE `username`='award_points_for_bills.php' AND ".$s_details_clause, 'collapse_arrays'=>TRUE), $s_query_string, $a_billing_logs);
	}

	// get the restaurant names
	foreach($a_invoices as $k=>$a_invoice) {

		// find the restaurant's name
		if (trim($a_invoice['dataname']) == '') {
			$a_invoices[$k]['restaurant_name'] = '';
		} else {
			$a_restaurants = $dbapi->getAllInTable('restaurants', array('data_name'=>$a_invoice['dataname']), TRUE,
				array('selectclause'=>'`company_name`'));
			if (count($a_restaurants) == 0)
				$a_invoices[$k]['restaurant_name'] = '';
			else
				$a_invoices[$k]['restaurant_name'] = $a_restaurants[0]['company_name'];
		}

		// determine if the billing log was found
		$a_invoices[$k]['billing_log_found'] = in_array($a_invoices[$k]['id'], $a_billing_logs);
	}
	unset($a_restaurants);
}

// returns an array with each entry like array('dataname'=>dataname, 'from'=>original license, 'to'=>new license, 'commission'=>distributor's commission at that time)
function get_distro_points_explained_upgrades($s_distro_code, &$a_upgrades) {
	global $o_statsReportingExternal;
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

	$o_statsReportingExternal->getListOfUpgradedAccounts($a_upgrades);
	$a_upgrades = $a_upgrades['user']['upgrades'];
	if ($s_distro_code == "zephyrhardware")
		$a_upgrades = array();
	foreach($a_upgrades as $k=>$a_upgrade) {
		if ($a_upgrade['distro_code'] != $s_distro_code)
			unset($a_upgrades[$k]);
	}

	// check the billing logs
	if (count($a_upgrades) > 0) {
		$a_billing_logs_dns = $dbapi->getAllInTable('billing_log', array('distro_code'=>$s_distro_code), TRUE, array('whereclause'=>"WHERE `username` LIKE 'restaurant:%' AND `action` LIKE 'Package was Changed to %' AND `details` LIKE '%distributor [distro_code]'", 'selectclause'=>'`dataname`, date_format(`datetime`, "%c/%e/%Y") AS `formatted_date`', 'collapse_arrays'=>TRUE));
		foreach($a_upgrades as $k=>$a_upgrade)
			if (!in_array($a_upgrade['dataname'], $a_billing_logs_dns))
				unset($a_upgrades[$k]);
	}

	// get the restaurant names
	foreach($a_upgrades as $k=>$a_upgrade) {
		if (trim($a_upgrade['dataname']) == '') {
			$a_upgrades[$k]['restaurant_name'] = '';
		} else {
			$a_restaurants = $dbapi->getAllInTable('restaurants', array('data_name'=>$a_upgrade['dataname']), TRUE,
				array('selectclause'=>'`company_name`'));
			if (count($a_restaurants) == 0)
				$a_upgrades[$k]['restaurant_name'] = '';
			else
				$a_upgrades[$k]['restaurant_name'] = $a_restaurants[0]['company_name'];
		}
	}
}

// returns an array with each entry like array('dataname'=>dataname, 'value'=>value, 'commission'=>commission)
function get_distro_points_explained_licenses($s_distro_code, &$a_licenses) {
	$s_query_string = '';
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
	$dbapi->getAllInTable('reseller_licenses', array('resellername'=>$s_distro_code), TRUE, array('selectclause'=>'`value`,`applied`,`restaurantid`,`notes`, date_format(`purchased`, "%c/%e/%Y") AS `formatted_date`', 'whereclause'=>"WHERE `resellername`='[resellername]' AND `notes` LIKE 'PURCHASED WITH POINTS, distro commission %'"), $s_query_string, $a_licenses);
	foreach($a_licenses as $k=>$a_license) {

		// get the commission of the distro
		$a_matches = array();
		preg_match('/[0-9]+/', str_replace('PURCHASED WITH POINTS, distro commission ', '', $a_license['notes']), $a_matches);
		$a_license['commission'] = $a_matches[0];

		// get the restaurant that the license was applied to
		if ($a_license['applied'] == '' || $a_license['applied'] == '0000-00-00 00:00:00' || $a_license['restaurantid'] == '') {
			$a_licenses[$k]['dataname'] = '';
			continue;
		}

		$a_restaurants = $dbapi->getAllInTable('restaurants', array('id'=>$a_license['restaurantid']), TRUE, array('selectclause'=>'`data_name`,`company_name` AS `restaurant_name`'));
		if (count($a_restaurants) == 0) {
			$a_licenses[$k]['dataname'] = '';
			continue;
		}
		$a_licenses[$k]['dataname'] = $a_restaurants[0]['data_name'];
		$a_licenses[$k]['restaurant_name'] = $a_restaurants[0]['restaurant_name'];
	}
}

// returns an array with each entry like array('dataname'=>dataname, 'distro_points_applied'=>distro_points_applied, 'details'=>details)
function get_distro_points_explained_reseller_billing_log($s_distro_code, &$a_billing_logs) {
	$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

	$a_billing_logs = $dbapi->getAllInTable('reseller_billing_log', array('resellername'=>$s_distro_code, 'action'=>'custom add distro points', 'action2'=>'buy-in add distro points'), TRUE, array('selectclause'=>'reseller_billing_log.*, date_format(`datetime`, "%c/%e/%Y") AS `formatted_date`', 'whereclause'=>"WHERE `resellername`='[resellername]' AND (`action`='[action]' OR `action`='[action2]')"));
	foreach($a_billing_logs as $k=>$a_application) {
		$a_billing_logs[$k]['distro_points_applied'] = (int)$a_application['credits_applied'];
		if (trim($a_application['dataname']) == '') {
			$a_billing_logs[$k]['restaurant_name'] = '';
		} else {
			$a_restaurants = $dbapi->getAllInTable('restaurants', array('data_name'=>$a_application['dataname']), TRUE,
				array('selectclause'=>'`company_name`'));
			if (count($a_restaurants) == 0) {
				$a_billing_logs[$k]['restaurant_name'] = '';
			} else {
				$a_billing_logs[$k]['restaurant_name'] = $a_restaurants[0]['company_name'];
				$a_billing_logs[$k]['details'] = str_replace($a_application['dataname'], $a_billing_logs[$k]['restaurant_name'], $a_billing_logs[$k]['details']);
			}
		}
	}
}

// see get_distro_username_from_array_distrocode for options for variables
// returns "success|numpointsapplied", "distributor deactivated", "apply_distro_credits error: could not find distributor", or "apply_distro_credits error: could not update mysql database"
function apply_distro_credits_for_transaction($i_transaction_cost, $a_distro, $s_distro_code = '', $s_dataname = '', $b_ignore_zephyr = FALSE, &$s_return_distro_code = '') {
	global $maindb;

	$a_distro = get_distro_from_array_distrocode_dataname($a_distro, $s_distro_code, $s_dataname);
	if (count($a_distro) == 0)
		return "apply_distro_credits error: could not find distributor";
	$s_return_distro_code = $a_distro['username'];
	if ($a_distro['access'] == "deactivated")
		return "distributor deactivated";
	$i_distro_commission = get_distro_commision($a_distro);
	$i_distro_points = (int)$a_distro['credits'];
	$s_distro_code = $a_distro['username'];
	$i_points_to_add = (int)$i_transaction_cost*$i_distro_commission/100;
	if ($i_points_to_add < 1)
		return "success|0";
	if ($b_ignore_zephyr && $s_distro_code == 'zephyrhardware')
		return "success|0";
	$i_new_points = $i_points_to_add + $i_distro_points;
	mlavu_query("UPDATE `[maindb]`.`resellers` SET `credits`='[credits]' WHERE `id`='[id]'",
		array("maindb"=>$maindb, "credits"=>$i_new_points, "id"=>$a_distro['id']));
	if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
		return "apply_distro_credits error: could not update mysql database";
	return "success|".$i_points_to_add;
}

?>
