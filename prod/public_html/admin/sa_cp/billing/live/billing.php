<?php
if(!isset($p_companyid)) $p_companyid = admin_info("companyid");
if(!isset($p_dataname)) $p_dataname = admin_info("dataname");
if(!isset($p_area)) $p_area = "";
if(!isset($p_collection_lockout)) $p_collection_lockout = false;

global $zInt;
global $zUtils;
global $zAccount;
global $zAccountId;
global $zPaymentMethod;
global $zPaymentMethodId;
global $zRatePlanCharge;
global $zRatePlanChargeId;
global $o_emailAddressesMap;
global $acctUtils;
require_once(dirname(__FILE__)."/../../../sa_cp/billing/payment_profile_functions.php");
require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
require_once(dirname(__FILE__)."/../../../sa_cp/billing/procareas/monthly_parts.php");
require_once(dirname(__FILE__)."/../../../sa_cp/billing/procareas/procpay_functions.php");
require_once(dirname(__FILE__)."/../../../sa_cp/billing/procareas/SalesforceIntegration.php");
require_once(dirname(__FILE__).'/../../../sa_cp/billing/billing_lockout_functions.php');
require_once(dirname(__FILE__)."/../../../sa_cp/billing/referal_functions.php");
require_once(dirname(__FILE__)."/../../../sa_cp/billing/authnet_functions.php");
require_once(dirname(__FILE__)."/../../../manage/globals/email_addresses.php");
require_once(dirname(__FILE__)."/../../../cp/objects/deviceLimitsObject.php");
require_once(dirname(__FILE__)."/../../../cp/resources/json.php");
require_once(dirname(__FILE__)."/../../../../inc/billing/zuora/ZuoraIntegration.php");
require_once(dirname(__FILE__)."/../../../../inc/billing/zuora/ZuoraUtils.php");
require_once(dirname(__FILE__)."/../../../../inc/billing/LavuAccountUtils.php");
global $o_package_container;
global $b_accept_8484;
global $o_device_limits;
if (!isset($o_package_container))
	$o_package_container = new package_container();
if (!isset($maindb))
	$maindb = "poslavu_MAIN_db";
$b_accept_8484 = TRUE;
$show_form = true;
global $account_past_due_msg;
$account_past_due_msg = "";

global $rest_read;
$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]' or `data_name`='[2]'",$p_companyid,$p_dataname);
if(mysqli_num_rows($rest_query)) $rest_read = mysqli_fetch_assoc($rest_query);

global $signup_read;
$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `restaurantid`='[1]' or `dataname`='[2]'",$p_companyid,$p_dataname);
if(mysqli_num_rows($signup_query)) $signup_read = mysqli_fetch_assoc($signup_query);

global $status_read;
$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]' or `dataname`='[2]'",$p_companyid,$p_dataname);
if(mysqli_num_rows($status_query)) $status_read = mysqli_fetch_assoc($status_query);

$setup_props = get_setup_props($rest_read, $signup_read['package'], $signup_read['status']);

// Zuora init
if ( !isset($zInt) )
{
	$zInt = new ZuoraIntegration();
}
// Zuora utils
if ( !isset($zUtils) )
{
	$zUtils = new ZuoraUtils($zInt);
}
// Lavu account utils
if ( !isset($acctUtils) )
{
	$acctUtils = new LavuAccountUtils();
}

$zAccountId = $signup_read['zAccountId'];
$zRatePlanChargeId = $signup_read['zRatePlanChargeId'];
$zPaymentMethodId = '';
$show_5min_message = false;
$trial_end_date = $status_read['trial_end'];

// Account exists in Zuora
if ( !empty($zAccountId) )
{
	$zAccount = $zInt->getAccount(array('zAccountId' => $zAccountId));

	$zPaymentMethodId = $zAccount->DefaultPaymentMethodId;

	// refid in the request meants customer just entered new credit card, so let it override the DefaultPaymentMethodId
	if ( !empty($_REQUEST['refid']) && $zPaymentMethodId != $_REQUEST['refid'] )
	{
		$zPaymentMethodId = $_REQUEST['refid'];

		// Update account with the newly created zPaymentMethodId as its DefaultPaymentMethod
		$update_ok = $zInt->updateAccount(array('zAccountId' => $zAccountId, 'DefaultPaymentMethodId' => $zPaymentMethodId, 'AutoPay' => 1));

		// Update our zAccount object, too
		if ( $update_ok )
		{
			// Close the old PaymentMethod, if it already has one
			$had_preexisting_pm = !empty($zAccount->DefaultPaymentMethodId);
			if ( $had_preexisting_pm )
			{
				$closed_old_pm = $zInt->cancelPaymentMethod(array('zAccountId' => $zAccountId, 'zPaymentMethodId' => $zAccount->DefaultPaymentMethodId));
			}

			// Update zAccount object without re-querying
			$zAccount->DefaultPaymentMethodId = $zPaymentMethodId;

			// LP-723 -- Send notification email when new PaymentMethod is created
			$o_emailAddressesMap->sendEmailToAddress('payment_notifications', "New Zuora PaymentMethod Created from CP Billing Page", "Dataname: {$p_dataname}\nPaymentMethodId: {$zPaymentMethodId}\nHad Pre-existing PaymentMethod: {$had_preexisting_pm}");

			// LP-1486 -- Make sure their trial end date is blanked out, which takes them out of trial mode
			// since they have a new or existing credit card on file by this point
			if ( !empty($trial_end_date) )
			{
				blankOutTrialEndDate(array('dataname' => $p_dataname));
			}
		}
	}

	if ( !empty($zPaymentMethodId) )
	{
		$zPaymentMethod = $zInt->getPaymentMethodById(array('zPaymentMethodId' => $zPaymentMethodId));
		if ( $zPaymentMethod !== null )
		{
			$zPaymentMethod->CreatedDate = explode('T', $zPaymentMethod->CreatedDate)[0];
			$zPaymentMethod->LastTransactionDateTime = explode('T', $zPaymentMethod->LastTransactionDateTime)[0];
		}
	}

	// Calculate monthly recurring cost
	$monthly_cost = $zUtils->getMonthlyRecurringAmount(array('zAccountId' => $zAccountId));
}
// Account not yet imported into Zuora
else
{
	// No Zuora Account but they just entered a credit card (aka PaymentMethod) so we'll create their Zuora account and subscription at the same time
	if ( !empty($_REQUEST['refid']) )
	{
		// Create new subscription for customer
		$zPaymentMethodId = $_REQUEST['refid'];
		$package_level = (int) $signup_read['package'];
		if (empty($package_level)) $package_level = 25;  // Default to Lavu package level
		$package_name = $o_package_container->get_plain_name_by_attribute('level', $package_level);  // LP-347 -- Fixed account creating failing while users were entering a CC due to package display name being used instead of package name
		$recurring_amount = $o_package_container->get_recurring_by_attribute('level', $package_level);
		$start_date = $setup_props['first_hosting'];
		$current_date = date('Y-m-d');

		// LP-723 -- Send notification email when new PaymentMethod is created
		$o_emailAddressesMap->sendEmailToAddress('payment_notifications', "New Zuora PaymentMethod Created from CP Billing Page", "Dataname: {$p_dataname}\nPaymentMethodId: {$zPaymentMethodId}\nHad Pre-existing PaymentMethod: 0");

		// LP-851 -- Zach wanted to show the 5 min message for new signups unlocking their locked trial accounts
		if ( !empty($trial_end_date) && strtotime($current_date) > strtotime($trial_end_date) )
		{
			$show_5min_message = true;
		}

		// If we've already passed the first_hosting date, just use today
		if ( strtotime($current_date) > strtotime($start_date) )
		{
			// If they have a trial extension, Finance said to start the subscription on that date
			if ( !empty($trial_end_date) && strtotime($trial_end_date) > strtotime($current_date) )
			{
				$start_date = $trial_end_date;
			}
			else
			{
				$start_date = $current_date;
			}
		}
		$response = $zInt->createHostingSubscriptionWithPaymentMethodId(array('dataname' => $p_dataname,  'package_level' => $package_level, 'package_name' => $package_name, 'start_date' => $start_date, 'first_hosting' => $start_date, 'billing_product_type' => 'Hosting', 'hosting_amount' => $recurring_amount, 'zPaymentMethodId' => $zPaymentMethodId), $signup_read);

		// Update signups table on success
		if ( isset($response->result) && isset($response->result->AccountId) )
		{
			$zAccountId = $response->result->AccountId;
			mlavu_query("UPDATE `poslavu_MAIN_db`.`signups` SET `zAccountId`='[3]' WHERE (`restaurantid`='[1]' OR `dataname`='[2]') AND `zAccountId`=''",$p_companyid, $p_dataname, $zAccountId);

			// Activate subscription
			$activation_response = $zInt->activateSubscription(array('zSubscriptionId' => $response->result->SubscriptionId));

			$update_ok = $zInt->updateAccount(array('zAccountId' => $zAccountId, 'AutoPay' => true));

			// Fetch other Zuora objects
			$zAccount = $zInt->getAccount(array('zAccountId' => $zAccountId));
			$zPaymentMethod = $zInt->getPaymentMethodById(array('zPaymentMethodId' => $zPaymentMethodId));
			if ( $zPaymentMethod !== null )
			{
				$zPaymentMethod->CreatedDate = explode('T', $zPaymentMethod->CreatedDate)[0];
				$zPaymentMethod->LastTransactionDateTime = explode('T', $zPaymentMethod->LastTransactionDateTime)[0];
			}

			$args = array('dataname' => $p_dataname, 'zAccountId' => $zAccountId, 'zPaymentMethodId' => $zPaymentMethodId);

			// Blank out trial_end date
			if ( !empty($trial_end_date) )
			{
				blankOutTrialEndDate($args);
			}

			// Run Due Info to update balance and unlock account
			$due_info_components = $zUtils->getDueInfoArray($args);
			$due_info = $acctUtils->getDueInfo($due_info_components);
			$due_info_updated = $zUtils->setDueInfo($p_dataname, $due_info_components);

			// Calculate monthly recurring cost
			$monthly_cost = $zUtils->getMonthlyRecurringAmount(array('zAccountId' => $zAccountId));
		}
	}
}

// Pay off any outstanding invoices if a new PaymentMethod was just created
if ( !empty($zAccount) && !empty($zPaymentMethodId) && !empty($zAccount->Balance) && !empty($_REQUEST['refid']) )
{
	$show_5min_message = true;

	$args = array('dataname' => $p_dataname, 'zAccountId' => $zAccountId, 'zPaymentMethodId' => $zPaymentMethodId);

	$zPayments = $zInt->payUnpaidInvoices($args);

	// Run Due Info to update balance and unlock account
	$due_info_components = $zUtils->getDueInfoArray($args);
	$due_info = $acctUtils->getDueInfo($due_info_components);
	$due_info_updated = $acctUtils->setDueInfo($p_dataname, $due_info_components);

	// Update Zuora account balance without re-querying, in case we need it
	$zAccount->Balance = $due_info_components['all_bills_due'];

	// SOMEDAY : display message about having paid $zPayment->Amount successfully
}

function blankOutTrialEndDate($args)
{
	$pstatus_update_ok = mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `trial_end`='' WHERE `dataname`='[dataname]' AND `dataname`!='' AND `trial_end`!='' LIMIT 1", $args);
	$posdb_update_ok = mlavu_query("UPDATE `poslavu_[dataname]_db`.`config` SET `value`='', `value2`='' WHERE `type`='system_setting' AND `setting`='payment_status' LIMIT 1", $args);

	return $pstatus_update_ok && $posdb_update_ok;
}

function dtadd1($dt,$amount=1,$type="days")
{
	$type = strtolower($type);
	$dt_parts = explode("-",$dt);
	if(count($dt_parts) > 2)
	{
		if(substr($type,0,4)=="year")
			$dt_ts = mktime(0,0,0,$dt_parts[1],$dt_parts[2],$dt_parts[0] + $amount);
		else if(substr($type,0,5)=="month")
			$dt_ts = mktime(0,0,0,$dt_parts[1] + $amount,$dt_parts[2],$dt_parts[0]);
		else
			$dt_ts = mktime(0,0,0,$dt_parts[1],$dt_parts[2] + $amount,$dt_parts[0]);

		return date("Y-m-d",$dt_ts);
	}
	return $dt;
}

function parse_expiration_date($card_expiration)
{
	$card_expiration = str_replace("/","",$card_expiration);
	$card_expiration = str_replace("-","",$card_expiration);
	$card_expiration = str_replace(" ","",$card_expiration);
	if(strlen($card_expiration)==5)
	{
		$card_expiration = substr($card_expiration,0,1) . substr($card_expiration,3,2);
	}
	else if(strlen($card_expiration)==6)
	{
		$card_expiration = substr($card_expiration,0,2) . substr($card_expiration,4,2);
	}
	while(strlen($card_expiration)<4)
	{
		$card_expiration = "0".$card_expiration;
	}
	return $card_expiration;
}

function get_current_arb_status($subscriptionid)
{
	$substatus = "unknown";
	$sub_info = manage_recurring_billing("info",array("refid"=>"15","subscribeid"=>$subscriptionid));
	if(isset($sub_info['response']))
	{
		$status_find = explode("<status>",$sub_info['response']);
		if(count($status_find) > 1)
		{
			$status_find = explode("</status>",$status_find[1]);
			$substatus = $status_find[0];
		}
	}
	return $substatus;
}

function get_company_billing_info($p_companyid,$dataname)
{
	$cust_info = array();
	$cust_info['company_name'] = "";
	$cust_info['first_name'] = "";
	$cust_info['last_name'] = "";
	$cust_info['address'] = "";
	$cust_info['city'] = "";
	$cust_info['state'] = "";
	$cust_info['zip'] = "";
	$cust_info['phone'] = "";
	$cust_info['email'] = "";

	$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `restaurantid`='[1]' or `dataname`='[2]'",$p_companyid,$dataname);
	if(mysqli_num_rows($signup_query))
	{
		$signup_read = mysqli_fetch_assoc($signup_query);
		$cust_info['company_name'] = $signup_read['company'];
		$cust_info['first_name'] = $signup_read['firstname'];
		$cust_info['last_name'] = $signup_read['lastname'];
		$cust_info['address'] = $signup_read['address'];
		$cust_info['city'] = $signup_read['city'];
		$cust_info['state'] = $signup_read['state'];
		$cust_info['zip'] = $signup_read['zip'];
		$cust_info['phone'] = $signup_read['phone'];
		$cust_info['email'] = $signup_read['email'];
	}
	else
	{
		$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$p_companyid);
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			$cust_info['company_name'] = $rest_read['company_name'];
			$cust_info['first_name'] = "";
			$cust_info['last_name'] = "";
			$cust_info['address'] = "";
			$cust_info['city'] = "";
			$cust_info['state'] = "";
			$cust_info['zip'] = "";
			$cust_info['phone'] = $rest_read['phone'];
			$cust_info['email'] = $rest_read['email'];

			$loc_query = mlavu_query("select * from `poslavu_[1]_db`.`locations` where `manager`!=''",$dataname);
			if(mysqli_num_rows($loc_query))
			{
				$loc_read = mysqli_fetch_assoc($loc_query);
				$manager_parts = explode(" ",$loc_read['manager'],2);
				if(count($manager_parts) > 1)
				{
					$manager_first_name = $manager_parts[0];
					$manager_last_name = $manager_parts[1];
				}
				else
				{
					$manager_first_name = $manager_parts[0];
					$manager_last_name = "";
				}

				$cust_info['first_name'] = $manager_first_name;
				$cust_info['last_name'] = $manager_last_name;
				$cust_info['city'] = $loc_read['city'];
				$cust_info['state'] = $loc_read['state'];
				$cust_info['zip'] = $loc_read['zip'];
				$cust_info['phone'] = $loc_read['phone'];
				$cust_info['email'] = $loc_read['email'];
			}
		}
	}

	return $cust_info;
}

function build_auth_vars($card_info,$customer_info)
{
	$vars = array();
	$vars['interval_length'] = 1;
	$vars['interval_unit'] = "months";
	$vars['trial_occurrences'] = 0;
	$vars['trial_amount'] = 0;
	$vars['card_number'] = $card_info['number'];
	$vars['card_date'] = parse_expiration_date($card_info['expiration']);
	$vars['card_code'] = $card_info['code'];
	$vars['first_name'] = $card_info['first_name'];
	$vars['last_name'] = $card_info['last_name'];
	//$vars['first_name'] = $customer_info['first_name'];
	//$vars['last_name'] = $customer_info['last_name'];
	$vars['address'] = $customer_info['address'];
	$vars['zip'] = $customer_info['zip'];
	$vars['city'] = $customer_info['city'];
	$vars['state'] = $customer_info['state'];
	$vars['country'] = "";
	$vars['email'] = $customer_info['email'];
	$vars['phone'] = $customer_info['phone'];
	$vars['company_name'] = $customer_info['company_name'];
	if(isset($card_info['subscribeid'])) $vars['subscribeid'] = $card_info['subscribeid'];
	if(isset($card_info['refid'])) $vars['refid'] = $card_info['refid'];

	return $vars;
}

function authorize_card_for_transaction($atype,$restaurantid,$dataname,$auth_amount,$card_info,$customer_info)
{
	$vars = build_auth_vars($card_info,$customer_info);
	$vars['amount'] = $auth_amount;
	$vars['description'] = "POSLavu Auth R" . $restaurantid;

	$license_auth_result = manage_recurring_billing($atype,$vars);
	if($license_auth_result=="Approved")
	{
		return array("success"=>true);
	}
	else
	{
		return array("success"=>false,"response"=>$license_auth_result);
	}
}

class CForm
{
	var $formcode;
	var $validate;
	var $insert_cols;
	var $insert_vals;
	var $update_cols;
	var $post_action;
	var $fields;
	var $suppress_submit;

	function __construct($set_fields,$set_post_action)
	{
		$this->formcode = "";
		$this->validate = array();
		$this->insert_cols = "";
		$this->insert_vals = "";
		$this->update_cols = "";
		$this->fields = $set_fields;
		$this->post_action = $set_post_action;
		$this->suppress_submit = $suppress_submit;

		$this->process();
	}

	function form_code()
	{
		return $this->formcode;
	}

	function process()
	{
		global $zInt;

		$this->formcode = "";
		$this->formcode .= $this->cvv_layer_code(20,20);
		$this->formcode .= "<form name='form1' method='post' action='".$this->post_action."'>";
		$this->formcode .= "<input type='hidden' name='posted' value='1'>";
		$this->formcode .= "<table cellpadding=1 cellspacing=1>";

		for($i=0; $i<count($this->fields); $i++)
		{
			$title = $this->fields[$i][0];
			$type = $this->fields[$i][1];
			$name = (isset($this->fields[$i][2]))?$this->fields[$i][2]:"";
			$required = (isset($this->fields[$i][3]))?$this->fields[$i][3]:false;
			$props = (isset($this->fields[$i][4]))?$this->fields[$i][4]:array();
			$attributes = (isset($this->fields[$i][5]))?$this->fields[$i][5]:"";
			$value = (isset($_POST[$name]))?$_POST[$name]:"";

			$include_in_db = false;

			$this->formcode .= "<tr>";
			if($type=="title")
			{
				$this->formcode .= "<td colspan='2'>&nbsp;</td><td class='tabhead' align='center'>".strtoupper($title)."</td>";
			}
			else if($type=="submit")
			{
				//$this->formcode .= "<td colspan='2'>&nbsp;</td><td><input type='button' value='$title' onclick='validate_form()' {$attributes}></td>";
			}
			else if($type=="set")
			{
				$include_in_db = true;
				$this->formcode .= "<input type='hidden' name='$name' value=\"".str_replace("\"","&quot;",$props)."\" {$attributes}>";
			}
			else
			{
				if($type!="ntext")
					$include_in_db = true;

				$this->formcode .= "<td align='right' class='tabtext'>";
				$this->formcode .= strtoupper($title);

				if($type=="card_code")
				{
					$include_in_db = false;
					$this->formcode .= " <a style='cursor:pointer; color:#888888' onclick='document.getElementById(\"cvv\").style.visibility = \"visible\"'>(What is this?)</a>";
				}
				else if($type=="card_expiration")
				{
					$this->formcode .= " <font color='#888888'>(mmYY)</font>";
				}

				$this->formcode .= "</td><td>";
				if($required) $this->formcode .= "<font color='#444477' style='font-size:14px'>*</font>";
				else $this->formcode .= "&nbsp;";
				$this->formcode .= "</td>";
				$this->formcode .= "<td>";
				if($type=="select")
				{
					$this->formcode .= "<select name='$name' style='width:240px' {$attributes}>";
					for($n=0; $n<count($props); $n++)
					{
						$prop = $props[$n];
						$this->formcode .= "<option value='$prop'";
						if($prop==$value) $this->formcode .= " selected";
						$this->formcode .= ">$prop</option>";
					}
					 $this->formcode .= "</select>";
				}
				else
				{
					$this->formcode .= "<input type='text' name='$name' value=\"".str_replace("\"","&quot;",$value)."\" style='width:240px' {$attributes}>";
				}
				if($type=="card_number")
				{
					if(strlen($value) > 4)
					{
						$value = substr($value,strlen($value)-4);
					}
				}
				$this->formcode .= "</td>";
				if($required)
				{
					$this->validate[] = "if(document.form1.$name.value=='') alert('Please provide a value for \"$title\"'); ";
				}
				if($type=="email")
				{
					$this->validate[] = "if(document.form1.$name.value!='' && (document.form1.$name.value.indexOf('@') < 1 || document.form1.$name.value.indexOf('.') < 1)) alert('Please provide a valid email address'); ";
				}
			}
			if($include_in_db)
			{
				if($this->insert_cols!="") $this->insert_cols .= ",";
				$this->insert_cols .= "`$name`";
				if($this->insert_vals!="") $this->insert_vals .= ",";
				$this->insert_vals .= "'".str_replace("'","''",$value)."'";
				if($this->update_cols!="") $this->update_cols .= ",";
				$this->update_cols .= "`$name`='".str_replace("'","''",$value)."'";
			}
			$this->formcode .= "</tr>";
		}
		$this->formcode .= "</table>";
		$this->formcode .= "</form>";

		$this->formcode .= "<script language='javascript'>";
		$this->formcode .= "function validate_form() { ";
		for($i=0; $i<count($this->validate); $i++)
		{
			$this->formcode .= $this->validate[$i] . " else ";
		}
		$this->formcode .= "  document.form1.submit(); ";
		$this->formcode .= "} ";
		$this->formcode .= "</script>";

	}

	function cvv_layer_code($x,$y)
	{
		$str = "";
		$str .= "<div style='position:absolute; left:".$x."px; top:".$y."px; visibility:hidden' id='cvv'>";
		$str .= "<table cellpadding=8 bgcolor='#ffffff' style='border:solid 2px #aaaaaa'>";
		$str .= "<tr><td align='right' bgcolor='#dddddd' onclick='document.getElementById(\"cvv\").style.visibility = \"hidden\"' style='font-family:Verdana, Arial; color:#888888; cursor:pointer'><b>Close X</b></td></tr>";
		$str .= "<tr><td><img src='images/cvv.jpg'></td></tr>";
		$str .= "</table>";
		$str .= "</div>";
		return $str;
	}
}

function get_num_devices(){
	return mysqli_num_rows(lavu_query("SELECT * FROM `devices` WHERE `inactive`='0'"));
}

function draw_upgrade_button($p_dataname, $o_package_container) {
	$s_retval = "";

	// get the restaurant id
	$i_restaurant_id = -1;
	$restaurant_id_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[data_name]'", array("data_name"=>$p_dataname));
	if ($restaurant_id_query)
		if ($a_restaurant_id = mysqli_fetch_assoc($restaurant_id_query))
			$i_restaurant_id = $a_restaurant_id['id'];

	// draw the upgrade options
	if ($i_restaurant_id > 0) {
		$a_package_level = find_package_status($i_restaurant_id); // from payment_profile_functions
		$i_package_level = $a_package_level['package'];
		$s_package_name = packageToStr($i_package_level);
		$a_available_upgrades = $o_package_container->get_available_upgrades($s_package_name);
		if (count($a_available_upgrades) > 0) {
			$printed_name = $o_package_container->get_printed_name_by_attribute('name', $s_package_name);
			if (stripos($printed_name, 'Lavu') === false) $printed_name = 'Lavu ' . $printed_name;
			$s_retval .= "<table id='upgrade_button_container' style='border:1px solid #999;background-color:#f4f4f4;width:'><tr><td>&nbsp;</td><td>Current License Level: <b>".$printed_name."</b></td><td width='200'>&nbsp;</td><td>";
			if(count($a_available_upgrades) > 0)
			{
				$s_retval .= "<form method='POST' action='index.php?mode=billing' name='upgradeAccount'>";
				$s_retval .= "<input type='hidden' name='mode' value='billing'>";
				$s_retval .= "<input type='hidden' name='upgrade_from' value='$s_package_name'>";
				$s_retval .= "<select name='upgrade_account' onchange='document.upgradeAccount.submit()'>";
				$s_retval .= "<option value=''>---- Choose a level to upgrade ----</option>";
				foreach($a_available_upgrades as $s_new_package_name) {
					$printed_name = $o_package_container->get_printed_name_by_attribute('name', $s_new_package_name);
					if (strpos($printed_name, 'Lavu') === false) $printed_name = "Lavu ". $printed_name;
					$s_retval .= "<option value='$s_new_package_name'>".$printed_name."</option>";
				}
				$s_retval .= "</select>";
				$s_retval .= "</form>";
			}
			else $s_retval .= "&nbsp;";
			$s_retval .= "</td>";
			$s_retval .= "<td>&nbsp;</td></tr></table>";
		}
	}
	return $s_retval;
}

function drawBillingSummary($dataname, $package_display_name, $monthly_cost, $num_terminals, $total_due, $service_agreement, $is_demo_account_in_trial_mode=false, $in_self_signup_trial_mode=false, $trial_end_date='', $ask_for_hosting) {
	if (empty($service_agreement)) $service_agreement = 'Month-to-Month';
	if (!empty($trial_end_date)) $trial_end_date = date("m/d/Y", strtotime($trial_end_date));

	echo <<<HTML
		<div class='upgrade_devices_container' style='margin-left:auto; margin-right:auto; border:1px solid #999; background-color:#f4f4f4; width:610px;'>
			<table width="95%" align="center">
			<tr>
				<td width="50%">Package: <strong>{$package_display_name}</strong></td>
				<td width="50%">Recurring Cost: <strong>\${$monthly_cost}</strong></td>
			</tr>
			<tr>
				<td>Terminals: <strong>{$num_terminals}</strong></td>
				<td>Billing Frequency: <strong>Monthly</strong></td>
			</tr>
HTML;
	if ($in_self_signup_trial_mode && !$ask_for_hosting) {
		echo <<<HTML
			<tr>
				<td>Trial Ends: <strong>{$trial_end_date}</strong></td>
				<td><a style='color:#aecd37' href='' onclick='loadHostedPage(); return false'\">Enter Credit Card Now</a></td>
			</tr>
			</table>
		</div>
HTML;
	}
	else if ( $is_demo_account_in_trial_mode) {
		echo <<<HTML
			<tr>
				<td>Trial Ends: <strong>{$trial_end_date}</strong></td>
				<td></td>
			</tr>
			</table>
		</div>
HTML;
	}
	else {
		echo <<<HTML
			<tr>
				<td>Total Due: <strong>{$total_due}</strong></td>
				<td>Service Agreement: <strong>{$service_agreement}</strong></td>
			</tr>
			</table>
		</div>
HTML;
	}
}

function echo_interface_manage_device_limit($package_level, $i_num_current_devices, $i_base_ipad_limit, $i_max_devices, $f_predicted_hosting, $f_cost_per_ipad) {
	if ($i_num_current_devices == $i_max_devices)
		return;
	$options = '';
	$orig_total_cost = $f_predicted_hosting;
	for ($i=$i_num_current_devices; $i<=$i_max_devices; $i++) {
		$num_terminals_1thru4 = min($i, 4);
		$num_terminals_after4 = max($i - 4, 0);

		// Platinum3 (Pro)
		if ( $package_level === 23 )
		{
			$i_start_charging = max($i_base_ipad_limit, $i_num_current_devices);
			$extra_cost = max($i - $i_start_charging, 0)*$f_cost_per_ipad;
			$new_total_cost = $f_predicted_hosting + $extra_cost;
		}
		// Lavu + LavuEnterprise
		else
		{
			$cost_per_terminal_1thru4 = $f_cost_per_ipad;
			$cost_per_terminal_after4 = 20.00;

			$cost_1thru4_terminals = $num_terminals_1thru4 * $cost_per_terminal_1thru4;
			$cost_after4_terminals = $num_terminals_after4 * $cost_per_terminal_after4;

			$new_total_cost = $cost_1thru4_terminals + $cost_after4_terminals;
			$extra_cost = $new_total_cost - $f_predicted_hosting;
		}

		$s_price = " +$".number_format($extra_cost, 2, '.', ',');
		$s_price .= " ($".number_format($new_total_cost, 2, '.', ',').")";
		$s_current = ($i == $i_num_current_devices) ? " (current)" : $s_price;
		$options.= "<option name= 'num_devices' value='$i'>$i{$s_current}</option>";
	}

	?>
	<script type='text/javascript'>
		function device_limit_mod_submit(num_ipads){
			var dataname = "<?= admin_info('dataname') ?>";
			var form = "<form action='index.php' method='GET'>";
			var values = {'mode':'billing', 'set_num_ipads_customer':num_ipads, 'dn':dataname};
			for(key in values) {
				form += "<input type='hidden' name='"+key+"' value='"+values[key]+"' />";
			}
			form += "</form>";
			$(form).submit();
		}

		function device_limit_mod_get_select_val() {
			return $('#upgrade_devices').find('select').val();
		}

		function device_limit_mod_draw_submit_button(select_element) {
			var jselect = $(select_element);
			var num_ipads = jselect.val();
			var jsubmit = jselect.siblings("input");

			if (num_ipads == <?= $i_num_current_devices ?>) {
				jsubmit.stop(true,true);
				jsubmit.hide(300);
			} else {
				jsubmit.stop(true,true);
				jsubmit.show(300);
			}
		}
	</script>
	<?php

}

class ExtractedBillingFunctions {

	/**
	 * Gets the authorization result for changing a customer's cc info for any reason.
	 * @param  array   $a_immutables             An array of values that aren't changed in this function, including:
	 *     doing_upgrade, ask_for_cc_change, ask_for_payment, card_info, customer_info, change_cc, p_companyid, p_dataname, doing_ipad_upgrade, i_new_device_limit, i_base_ipad_limit, f_cost_per_ipad, hosting_start
	 * @param  double  $collect_amount           Set to the recurring monthly cost of the new license
	 * @param  array   $auth_result              Either array(FALSE, $s_upgrade_ok) or the response from authorize_card_for_transaction()
	 * @param  boolean $doing_upgrade_success    If success is set to TRUE if the actual account upgrade succeeded
	 * @param  boolean $b_upgrade_update_hosting If success is set to TRUE, FALSE otherwise
	 * @param  double  $hosting_amount           If success is set to the value of collect_amountfor doing_upgrade and doing_ipad_upgrade
	 * @param  array   $card_info                The credit card information that was entered, 'subscribeid' and 'refid' are set for $ask_for_cc_change
	 * @return none                              N/A
	 */
	public static function get_auth_result($a_immutables, &$collect_amount, &$auth_result, &$doing_upgrade_success, &$b_upgrade_update_hosting, &$hosting_amount, &$card_info, &$b_upgrade_ipad_limit_update_hosting) {

		global $b_accept_8484;
		global $o_device_limits;

		$doing_upgrade = $a_immutables['doing_upgrade'];
		$ask_for_cc_change = $a_immutables['ask_for_cc_change'];
		$ask_for_payment = $a_immutables['ask_for_payment'];
		$customer_info = $a_immutables['customer_info'];
		$change_cc = $a_immutables['change_cc'];
		$p_companyid = $a_immutables['p_companyid'];
		$p_dataname = $a_immutables['p_dataname'];
		$package = $a_immutables['package'];
		$doing_ipad_upgrade = $a_immutables['doing_ipad_upgrade'];

		$b_accept_test_cc = $b_accept_8484 && $card_info['number'] == '8484' && $card_info['code'] == '8484' && $card_info['expiration'] == '0213' && ($_SERVER['REMOTE_ADDR'] == '74.95.18.90' || $_SERVER['REMOTE_ADDR'] == '173.10.247.198');
		$a_immutables['b_accept_test_cc'] = $b_accept_test_cc;

		if($doing_upgrade)
		{
			$a_immutables['card_info'] = $card_info;
			$a_immutables['b_accept_test_cc'] = $b_accept_test_cc;
			ExtractedBillingFunctions::doing_upgrade($a_immutables, $collect_amount, $auth_result, $doing_upgrade_success, $b_upgrade_update_hosting, $hosting_amount);
		}
		else if($ask_for_cc_change)
		{
			$card_info['subscribeid'] = $change_cc;
			$card_info['refid'] = 15;

			////$auth_result = authorize_card_for_transaction("update",$p_companyid,$p_dataname,$collect_amount,$card_info,$customer_info);
		}
		else if($ask_for_payment)
		{
			////$auth_result = authorize_card_for_transaction("charge",$p_companyid,$p_dataname,$collect_amount,$card_info,$customer_info);
		}
		else if ($doing_ipad_upgrade)
		{
			$i_new_device_limit = $a_immutables['i_new_device_limit'];
			$i_base_ipad_limit = $a_immutables['i_base_ipad_limit'];
			$f_cost_per_ipad = $a_immutables['f_cost_per_ipad'];
			$hosting_start = $a_immutables['hosting_start'];

			////$auth_result = authorize_card_for_transaction("authorize",$p_companyid,$p_dataname,$collect_amount,$card_info,$customer_info);
			$o_device_limits->setCustomiPhoneiPadKDSLimit(-1, $i_new_device_limit, -1, $p_dataname);
			self::updateMonthlyHostingForNewiPadLimit($i_new_device_limit, $p_dataname, $i_base_ipad_limit, $f_cost_per_ipad, $hosting_start, $package);
			if ($auth_result['success']) {
				$b_upgrade_ipad_limit_update_hosting = TRUE;
				$hosting_amount = $collect_amount;
			}
		}
		else
		{
			////$auth_result = authorize_card_for_transaction("authorize",$p_companyid,$p_dataname,$hosting_amount,$card_info,$customer_info);
		}
	}

	/**
	 * Helper function for get_auth_result()
	 * @param  integer $i_new_device_limit  The new device limit that the user has chosen (assumed to be greater than $i_base_ipad_limit)
	 * @param  string  $s_dataname          The dataname of the account to update the monthly hosting for
	 * @param  integer $i_base_ipad_limit   The number of iPads that aren't charged for (assumed to be greater than zero)
	 * @param  integer $f_cost_per_ipad     The recurring cost of upgrading to one more iPad
	 * @param  string  $s_next_hosting_date The date of the next month's hosting
	 * @return none                         N/A
	 */
	public static function updateMonthlyHostingForNewiPadLimit($i_new_device_limit, $s_dataname, $i_base_ipad_limit, $f_cost_per_ipad, $s_next_hosting_date, $package='') {
		global $o_package_container;
		$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
		// get some values
		$i_monthly_hosting_cost_for_ipads = max($i_new_device_limit - $i_base_ipad_limit, 0)*$f_cost_per_ipad;
		$s_limit_start_date = date("Y-m-d");
		$i_limit_start_date = strtotime($s_limit_start_date);

		// get the previous hosting date
		$s_last_hosting_date = date("Y-m-d", strtotime($s_next_hosting_date." -2 months"));
		while (date("Y-m-d", strtotime("{$s_last_hosting_date} +1 months")) != $s_next_hosting_date)
			$s_last_hosting_date = date("Y-m-d", strtotime("{$s_last_hosting_date} +1 days"));

		// get any previous monthly hosting parts related to iPad limiting
		$a_starting_monthly_parts = $dbapi->getAllInTable("monthly_parts", array("dataname"=>$s_dataname, "last_hosting_date"=>$s_last_hosting_date, "next_hosting_date"=>$s_next_hosting_date), TRUE, array("whereclause"=>"WHERE `dataname`='[dataname]' AND `mp_start_day` > '[last_hosting_date]' AND `mp_start_day` < '[next_hosting_date]' AND (`mp_end_day` >= '[next_hosting_date]' || `mp_end_day`='') AND `description`='Customer iPad Limit'", "limitclause"=>"LIMIT 1"));
		$a_existing_monthly_parts = $dbapi->getAllInTable("monthly_parts", array("dataname"=>$s_dataname, "last_hosting_date"=>$s_last_hosting_date, "next_hosting_date"=>$s_next_hosting_date), TRUE, array("whereclause"=>"WHERE `dataname`='[dataname]' AND `mp_start_day` <= '[last_hosting_date]' AND (`mp_end_day` >= '[next_hosting_date]' || `mp_end_day`='') AND `description`='Customer iPad Limit'", "limitclause"=>"LIMIT 1"));

		// alter an existing monthly part or create a new monthly part
		// there should either be 1 starting monthly part, 1 existing monthly part, or no monthly parts
		$i_priority = 0;
		if (count($a_existing_monthly_parts) > 0) {

			// outdate the existing part
			$o_monthly_part = new MonthlyInvoiceParts(0, $a_existing_monthly_parts[0]);
			$o_monthly_part->outdate();
			$o_monthly_part->save_to_db();
			$i_priority = (int)$a_existing_monthly_parts[0]['mp_priority']+1;
		} else if (count($a_starting_monthly_parts) > 0) {

			// outdate the starting part
			$o_monthly_part = new MonthlyInvoiceParts(0, $a_starting_monthly_parts[0]);
			$o_monthly_part->outdate();
			$o_monthly_part->save_to_db();
			$i_priority = (int)$a_starting_monthly_parts[0]['mp_priority']+1;
		}

		$f_per_terminal_cmsn = $o_package_container->get_ipad_cmsn_by_attribute('level', $package);
		$f_all_terminal_cmsn = ($f_per_terminal_cmsn !== null) ? (max($i_new_device_limit - $i_base_ipad_limit, 0)*$f_per_terminal_cmsn) : 0.00;

		// create a new monthly part
		$o_monthly_part = new MonthlyInvoiceParts(0, array("dataname"=>$s_dataname, "description"=>"Customer iPad Limit", "amount"=>$i_monthly_hosting_cost_for_ipads, "mp_start_day"=>$s_limit_start_date, "mp_end_day"=>"", "mp_priority"=>$i_priority, "distro_cmsn"=>$f_all_terminal_cmsn));
		$o_monthly_part->save_to_db();
	}

	/**
	 * Try and estimate the hosting on a given date
	 * @param  string  $s_date                   The date to estimate hosting on
	 * @param  string  $s_dataname               The dataname of the account to estimate hosting for
	 * @param  integer $i_package_level          The package level of the account
	 * @param  integer $i_custom_monthly_hosting The custom monthly hosting of the account if set, or -1 if not set
	 * @return double                            The estimated next month's hosting
	 */
	public static function predictMonthlyHosting($s_date, $s_dataname, $i_package_level, $i_custom_monthly_hosting) {
		$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();

		global $o_package_container;

		// get some initial values
		$i_retval = 0;
		$b_hosting_found = FALSE;

		// get the monthly parts that apply to the dataname/date
		// and apply the cost of the monthly parts
		$a_monthly_parts = NULL;
		MonthlyInvoiceParts::load_monthly_parts($s_dataname, "2009-01-01", "", $a_monthly_parts);
		MonthlyInvoiceParts::get_monthly_parts_that_apply_to_date($a_monthly_parts, $s_date, $a_monthly_parts);
		foreach($a_monthly_parts as $a_monthly_part) {
			if (strtolower($a_monthly_part['description']) == 'hosting')
				$b_hosting_found = TRUE;
			$i_retval += $a_monthly_part['amount'];
		}

		// get the invoice parts that apply to the dataname/date
		// and apply the invoice parts that have a different description than the monthly parts
		$a_invoice_parts = $dbapi->getAllInTable("invoice_parts", array("dataname"=>$s_dataname, "due_date"=>$s_date), TRUE);
		foreach($a_invoice_parts as $a_invoice_part) {

			// find any related monthly parts and quit if one is found
			$monthly_part_found = NULL;
			foreach($a_monthly_parts as $k=>$a_monthly_part) {
				if ($a_monthly_part['description'] == $a_invoice_part['description']) {
					$monthly_part_found = $k;
					break;
				}
			}
			if ($monthly_part_found !== NULL)
				break;

			// add the cost of the invoice part
			if (strtolower($a_invoice_part['description']) == 'hosting')
				$b_hosting_found = TRUE;
			$i_retval += $a_invoice_part['amount'];
		}

		// add the monthly hosting amount
		if (!$b_hosting_found) {
			if ($i_custom_monthly_hosting > -1) {
				$i_retval += $i_custom_monthly_hosting;
			} else {
				$i_retval += $o_package_container->get_recurring_by_attribute('level', $i_package_level);
			}
		}

		return $i_retval;
	}

	/**
	 * Echos out the display to ask for credit card information.
	 * @param  array    $a_immutables           An array of values that aren't changed in this function, including:
	 *     ask_for_cc_change, ask_for_upgrade, change_cc, profile_read, ask_for_hosting, hosting_action, show_hosting_start, show_hosting_amount, doing_upgrade, ask_for_payment, collection_minimum, collect_amount, current_license_level, new_license_level, status_notes, ask_for_ipad_upgrade, doing_ipad_upgrade, i_new_device_limit, collect_total
	 * @param  string   $ask_display            The string that gets echoed out (gets echoed as part of this function).
	 * @param  string   $extra_urlvars          URL vars to pass upon form submission.
	 * @param  array    $cancel_arbs            An array of subscription ids to be canceled.
	 * @param  resource $profile_query          The mysql resource of the query for a `billing_profile`.
	 * @param  array    $profile_read           The first row from the profile query.
	 * @param  string   $show_collection_amount The number format of $collect_total
	 * @return none                             N/A
	 */
	public static function echo_ask_display($a_immutables, &$ask_display, &$extra_urlvars, &$cancel_arbs, &$profile_query, &$profile_read, &$show_collection_amount) {

		global $o_package_container;
		global $account_past_due_msg;

		$ask_for_cc_change = $a_immutables['ask_for_cc_change'];
		$ask_for_upgrade = $a_immutables['ask_for_upgrade'];
		$profile_read = $a_immutables['profile_read'];
		$ask_for_hosting = $a_immutables['ask_for_hosting'];
		$hosting_action = $a_immutables['hosting_action'];
		$doing_upgrade = $a_immutables['doing_upgrade'];
		$ask_for_payment = $a_immutables['ask_for_payment'];
		$status_notes = $a_immutables['status_notes'];
		$ask_for_ipad_upgrade = $a_immutables['ask_for_ipad_upgrade'];
		$doing_ipad_upgrade = $a_immutables['doing_ipad_upgrade'];

		$ask_display = "";
		if($ask_for_cc_change)
			echo "<b>Change CC</b>";
		else if ($ask_for_upgrade || $doing_upgrade)
			echo "<b>Upgrade</b>";
		else
			echo "<b>Important:</b>";

		$extra_urlvars = "";
		if($ask_for_cc_change)
		{
			$change_cc = $a_immutables['change_cc'];
			echo "<br>Please enter the new credit card info below<br><br>";
			$extra_urlvars .= "&change_cc=".$change_cc;
			$ask_display .= "<tr><td align='right' style='color:#777777'>Subscription Id</td><td style='color:#000088'>" . $change_cc . "</td></tr>";
			$ask_display .= "<tr><td align='right' style='color:#777777'>Current Card</td><td style='color:#000088'>" . $profile_read['last_four'] . "</td></tr>";
			$ask_display .= "<tr><td align='right' style='color:#777777'>Amount</td><td style='color:#000088'>$" . number_format($profile_read['amount'],2) . "</td></tr>";
		}
		if($ask_for_hosting)
		{
			$show_hosting_start = $a_immutables['show_hosting_start'];
			$show_hosting_amount = $a_immutables['show_hosting_amount'];

			if($hosting_action=="new_hosting")
			{
				echo "<br>* Your account does not have any hosting payments scheduled.";
				echo "<br>To avoid interruption in service, please sign up for your monthly hosting payment below";
			}
			else
			{
				echo "<br>* Your monthly hosting payments may be incorrect or are scheduled after the billing date.";
				echo "<br>To avoid any interruption in service, please setup your proper monthly hosting payments below";

				if(is_numeric($hosting_action))
					$cancel_arbs[] = $hosting_action;
			}

			$ask_display .= "<tr><td align='right' style='color:#777777'>Recurring Hosting Start (next scheduled payment)</td><td style='color:#000088'>" . $show_hosting_start . "</td></tr>";
			$ask_display .= "<tr><td align='right' style='color:#777777'>Recurring Hosting Amount</td><td style='color:#000088'>" . $show_hosting_amount . " / month</td></tr>";
			echo "<br><br>";
		}
		if(($ask_for_hosting && $hosting_action == "replace_all") || $doing_upgrade || $doing_ipad_upgrade) {
			$p_companyid = $a_immutables['p_companyid'];
			$p_dataname = $a_immutables['p_dataname'];

			////// load all billing profiles, to be canceled
			////$profile_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_profiles` WHERE (`restaurantid`='[restaurantid]' OR `dataname`='[dataname]') AND `type` LIKE 'hosting' AND (`active`='1' or `arb_status`='active')", array('restaurantid'=>$p_companyid, 'dataname'=>$p_dataname));
			////while($profile_read = mysqli_fetch_assoc($profile_query))
			////{
			////	$cancel_arbs[] = $profile_read['subscriptionid'];
			////}
		}
		if($ask_for_payment)
		{
			$collection_minimum = $a_immutables['collection_minimum'];
			$collect_amount = $a_immutables['collect_amount'];
			$collect_total = $a_immutables['collect_total'];

			echo "<br>--Account Past Due--"; // Do not change it - it gets replaced with the appropriate message before rendering (this is inside of an output buffer)
			$account_past_due_msg = "";
			$account_past_due_msg .= "* Your account is either past due or requires payment.";
			$account_past_due_msg .= "<br>To avoid interruption in service, please pay below";

			if($collection_minimum!="" && is_numeric($collection_minimum) && $collection_minimum * 1 < $collect_total * 1)
			{
				$show_collection_total = "$" . number_format($collect_total,2);
				$show_collection_amount = "$" . number_format($collect_amount,2);

				$ask_display .= "<tr><td align='right' style='color:#777777'>Total Amount Due</td><td style='color:#000088'>" . $show_collection_total . "</td></tr>";
				$ask_display .= "<tr><td align='right' style='color:#777777'>Current Payment Amount (to charge now)</td><td style='color:#000088'><b>" . $show_collection_amount . "</b></td></tr>";
			}
			else
			{
				$ask_display .= "<tr><td align='right' style='color:#777777'>Amount To Pay Now</td><td style='color:#000088'>" . $show_collection_amount . "</td></tr>";
			}
			echo "<br><br>";
		}
		if ($ask_for_upgrade || $doing_upgrade) {
			$current_license_level = $a_immutables['current_license_level'];
			$new_license_level = $a_immutables['new_license_level'];
			$i_upgrade_cost = $o_package_container->get_upgrade_cost_by_old_new_names($current_license_level, $new_license_level);
			$i_recurring_cost = $o_package_container->get_recurring_by_attribute('name', $new_license_level);

			if ($ask_for_upgrade) {
				echo "<br />Upgrading from a Lavu ".$o_package_container->get_printed_name_by_attribute('name', $current_license_level)." license to a Lavu ".$o_package_container->get_printed_name_by_attribute('name', $new_license_level)." license.";
				echo "<br />Please fill in the information below to complete your upgrade.<br /><br />";
			}
			if ($i_upgrade_cost !== -1) {
				$ask_display .= "<tr><td align='right' style='color:#777777'>Upgrade Cost</td><td style='color:#008'>$".number_format($i_upgrade_cost,2)."</td></tr>";
				$ask_display .= "<tr><td align='right' style='color:#777777'>New Monthly Recurring</td><td style='color:#008'>$".number_format($i_recurring_cost,2)."</td></tr>";
				if ($doing_upgrade)
					$ask_display .= "<tr><td colspan='2' align='center'><a href='#' onclick='window.location = window.location;'>Reload</a></td></tr>";
			} else {
				$ask_display .= "<tr><td style='color:red;font-style:italic;'>Sorry, the upgrade package selected doesn't exist.</td><td></td></tr>";
			}
		}
		if ($ask_for_ipad_upgrade || $doing_ipad_upgrade) {
			$i_new_device_limit = $a_immutables['i_new_device_limit'];
			$collect_amount = $a_immutables['collect_amount'];

			echo "<br />Changing iPad device limit to {$i_new_device_limit}.";
			if ($ask_for_ipad_upgrade) {
				echo "<br />Please fill in the information below to complete these changes.<br /><br />";
			}
			$ask_display .= "<tr><td align='right' style='color:#777777'>New Monthly Recurring</td><td style='color:#008'>$".number_format($collect_amount,2)."</td></tr>";
		}

		if($status_notes!="")
			echo "<b>Additional Notes:</b><br>" . $status_notes . "<br><br>";

		echo "<table cellpadding=4>";
		echo $ask_display;
		echo "<tr><td colspan='2'>";
	}

	/**
	 * Manages "ask for payment" queries of a credit card that has passed authorizations.
	 * Extracted from the massive if statement below to try and make code flow more manageable.
	 * @param  array  $a_immutables An array of values that aren't changed in this function
	 * @param  string $show_message Parts of a message printed out to the customer
	 * @return none                 N/A
	 */
	public static function auth_result_success_ask_for_payment($a_immutables, &$show_message) {
		$collect_amount = $a_immutables["collect_amount"];
		$p_companyid = $a_immutables["p_companyid"];
		$p_area = $a_immutables["p_area"];
		$p_dataname = $a_immutables["p_dataname"];
		$show_collection_amount = $a_immutables["show_collection_amount"];
		$collect_total = $a_immutables["collect_total"];
		$collection_minimum = $a_immutables["collection_minimum"];
		$collection_due_date = $a_immutables["collection_due_date"];
		$collection_extend_days = $a_immutables["collection_extend_days"];
		$min_due_date = $a_immutables["min_due_date"];

		$show_message = "<font color='#008800'><b>Success!</b> <br>Your Payment of " . $show_collection_amount . " has been processed</font><br><br>";
		if($collection_minimum!="" && $collection_minimum * 1 < $collect_total * 1)
		{
			$new_collect_amount = $collect_total * 1 - $collection_minimum * 1;
			$new_due_date = "";
			if($collection_due_date!="" && $collection_extend_days!="")
			{
				$due_date_parts = explode("-",$collection_due_date);
				$new_due_date_ts = mktime(0,0,0,$due_date_parts[1],$due_date_parts[2] + $collection_extend_days,$due_date_parts[0]);
				$new_due_date = date("Y-m-d",$new_due_date_ts);
			}
			mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `collect`='[1]', `collection_due_date`='[2]' where `restaurantid`='[3]'",$new_collect_amount,$new_due_date,$p_companyid);

			$new_due_date = "";
			if($min_due_date!="" && $collection_extend_days!="")
			{
				$due_date_parts = explode("-",$min_due_date);
				$new_due_date_ts = mktime(0,0,0,$due_date_parts[1],$due_date_parts[2] + $collection_extend_days,$due_date_parts[0]);
				$new_due_date = date("Y-m-d",$new_due_date_ts);
			}
			mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `collect`='[1]', `min_due_date`='[2]' where `restaurantid`='[3]'",$new_collect_amount,$new_due_date,$p_companyid);
		}
		else
		{
			mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `collection_taken`='1' where `restaurantid`='[1]'",$p_companyid);
		}
	}

	/**
	 * Part 1 of processing a credit card that has passed authorizations.
	 * Extracted from the massive if statement below to try and make code flow more manageable.
	 * @param  array  $a_immutables An array of values that aren't changed in this function, including:
	 *     doing_upgrade, ask_for_cc_change, card_info, doing_upgrade_success, p_companyid, p_dataname, change_cc
	 * @param  string $show_message Parts of a message printed out to the customer
	 * @return none                 N/A
	 */
	public static function auth_result_success_part_1($a_immutables, &$show_message) {
		$doing_upgrade = $a_immutables['doing_upgrade'];
		$ask_for_cc_change = $a_immutables['ask_for_cc_change'];
		$card_info = $a_immutables['card_info'];
		$p_companyid = $a_immutables['p_companyid'];
		$p_dataname = $a_immutables['p_dataname'];
		$change_cc = $a_immutables['change_cc'];
		$doing_upgrade_success = $a_immutables['doing_upgrade_success'];

		if($doing_upgrade) {
			$show_message = "<nobr>$doing_upgrade_success</nobr>";
		}
		if($ask_for_cc_change)
		{
			$card_number = $card_info['number'];
			$last_four = substr($card_number,strlen($card_number) - 4);
			mlavu_query("update `poslavu_MAIN_db`.`billing_profiles` set `last_four`='[last_four]' where (`restaurantid`='[p_companyid]' or `dataname`='[p_dataname]') and `type` LIKE 'hosting' and (`active`='1' or `arb_status`='active') and `subscriptionid`='[change_cc]'", array('p_companyid'=>$p_companyid, 'p_dataname'=>$p_dataname, 'change_cc'=>$change_cc, 'last_four'=>$last_four));
			$show_message = "<nobr>Your Card has been changed to " . $last_four . " (last four)</nobr>";
		}
	}

	/**
	 * Part douce of processing a credit card that has passed authorizations.
	 * Extracted from the massive if statement below to try and make code flow more manageable.
	 * @param  array   $a_immutables    An array of values that aren't changed in this function, including:
	 *     doing_upgrade, ask_for_cc_change, ask_for_payment, card_info, active_last_four, b_upgrade_update_hosting, p_companyid, p_dataname, hosting_amount, hosting_start, cancel_arbs
	 * @param  boolean $ask_for_hosting Should this function ask for hosting? Might be set to FALSE within this function.
	 * @param  array   $customer_info   Information about the customer that entered their cc info
	 * @param  string  $show_message    Parts of a message printed out to the customer
	 * @param  boolean $show_form       If not supposed to ask for hosting or hosting was successfully updated, set to FALSE
	 * @return none                     N/A
	 */
	public static function auth_result_success_part_2($a_immutables, &$ask_for_hosting, &$customer_info, &$show_message, &$show_form) {

		global $b_accept_8484;

		$doing_upgrade = $a_immutables['doing_upgrade'];
		$ask_for_cc_change = $a_immutables['ask_for_cc_change'];
		$ask_for_payment = $a_immutables['ask_for_payment'];
		$card_info = $a_immutables['card_info'];
		$active_last_four = $a_immutables['active_last_four'];
		$b_upgrade_update_hosting = $a_immutables['b_upgrade_update_hosting'];
		$p_companyid = $a_immutables['p_companyid'];
		$p_dataname = $a_immutables['p_dataname'];
		$hosting_amount = $a_immutables['hosting_amount'];
		$hosting_start = $a_immutables['hosting_start'];
		$cancel_arbs = $a_immutables['cancel_arbs'];
		$b_upgrade_ipad_limit_update_hosting = $a_immutables['b_upgrade_ipad_limit_update_hosting'];

		$card_number_last_four = "";
		$cnlength = strlen($card_info['number']);
		if($cnlength > 4)
		{
			$card_number_last_four = substr($card_info['number'],$cnlength - 4);
		}
		if(trim($card_number_last_four)!="")
		{
			if($ask_for_hosting && isset($active_last_four[trim($card_number_last_four)]))
			{
				$active_last_four_amount = $active_last_four[trim($card_number_last_four)];
				if($active_last_four_amount * 1 == $hosting_amount * 1)
					$ask_for_hosting = false;
			}
		}

		$b_do_cancel_arbs = FALSE;
		if($ask_for_hosting || $b_upgrade_update_hosting || $b_upgrade_ipad_limit_update_hosting)
		{
			$customer_info['first_name'] = $card_info['first_name'];
			$customer_info['last_name'] = $card_info['last_name'];
			$result = create_new_hosting_profile($p_companyid,$p_dataname,$hosting_amount,$hosting_start,$card_info,$customer_info,$card_info['number'],$b_accept_8484);

			if ($b_upgrade_update_hosting) {
				$profiles_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_profiles` WHERE `dataname`='[dataname]' AND `amount`='[hosting_amount]' AND `start_date`='[hosting_start]'", array('dataname'=>$p_dataname, 'hosting_amount'=>$hosting_amount, 'hosting_start'=>$hosting_start));
				if ($profiles_query !== FALSE && mysqli_num_rows($profiles_query) > 0) {
					error_log(__FILE__.":".__LINE__."|possible duplicate billing profile created for {$p_dataname}");
					$s_card_info = LavuJson::json_encode($card_info);
					mail("tom@lavu.com", "possible duplicate billing profile created for {$p_dataname}", "dataname: {$p_dataname}\namount: {$hosting_amount}\n:start_date: {$hosting_start}\ncard info:\n{$s_card_info}");
				}
			}

			if($result['success'])
			{
				mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `hosting_taken`='1' where `restaurantid`='[1]'",$p_companyid);
				if(!$ask_for_payment || !isset($show_message))
					$show_message = "";
				$show_message .= "<font color='#008800'><b>Success!</b> <br>Your Hosting Payment has been scheduled</font>";
				if ($b_upgrade_update_hosting)
					$show_message = "";
				$show_form = false;

				$b_do_cancel_arbs = TRUE;
			}
			else
			{
				$show_message = "<font color='#880000'><b>** Error:</b><br>" . $result['response'] . "</font>";
			}
		}
		else if ($ask_for_cc_change)
		{
			$b_do_cancel_arbs = TRUE;
			$show_form = FALSE;
		}
		else
		{
			$show_form = false;
		}

		// cancel all the cancel ARBs
		if ($b_do_cancel_arbs) {
			for($n=0; $n<count($cancel_arbs); $n++)
			{
				$arb_status = get_current_arb_status($cancel_arbs[$n]);
				if($arb_status=="active")
				{
					$arbid = $cancel_arbs[$n];
					$vars = array();
					$vars['refid'] = 16;
					$vars['subscribeid'] = $arbid;
					$cancel_info = manage_recurring_billing("delete",$vars);
					mlavu_query("update `poslavu_MAIN_db`.`billing_profiles` set `active`='0' where `subscriptionid`='[1]' and `subscriptionid`!='' limit 1",$arbid);
				}
			}
		}
	}


	/**
	 * Tries to upgrade the client's account.
	 * Key:
	 *
	 * @param  array   $a_immutables             An array of values to be passed in that aren't going to be changed, including
	 *     card_info, customer_info, p_companyid, p_dataname, maindb, b_accept_test_cc
	 * @param  double  $collect_amount           Set to the recurring monthly cost of the new license
	 * @param  array   $auth_result              Either array(FALSE, $s_upgrade_ok) or the response from authorize_card_for_transaction()
	 * @param  boolean $doing_upgrade_success    If success is set to TRUE if the actual account upgrade succeeded
	 * @param  boolean $b_upgrade_update_hosting If success is set to TRUE, FALSE otherwise
	 * @param  double  $hosting_amount           If success is set to the value of collect_amount
	 * @return string                            One of 'success', 'upgrade not ok: $s_upgrade_ok', 'failed to charge card'
	 */
	public static function doing_upgrade($a_immutables, &$collect_amount, &$auth_result, &$doing_upgrade_success, &$b_upgrade_update_hosting, &$hosting_amount) {

		global $o_package_container;
		global $o_emailAddressesMap;
		global $b_accept_8484;

		// get the immutables
		$p_dataname = $a_immutables['p_dataname'];
		$p_companyid = $a_immutables['p_companyid'];
		$card_info = $a_immutables['card_info'];
		$customer_info = $a_immutables['customer_info'];
		$b_accept_test_cc = $a_immutables['b_accept_test_cc'];
		$maindb = 'poslavu_MAIN_db';

		// check that all internal values allow for an upgrade
		$s_upgrade_ok = authorize_upgrade_account_to_new_license($p_dataname, $_POST['upgrade_from'], $_POST['upgrade_account']);
		if ($s_upgrade_ok != "upgrade ok") {
			$auth_result = array("success"=>FALSE, "response"=>$s_upgrade_ok);
			return "upgrade not ok: {$s_upgrade_ok}";
		}

		// set some upgrade variables
		$i_upgrade_cost = (float)$o_package_container->get_upgrade_cost_by_old_new_names($_POST['upgrade_from'], $_POST['upgrade_account']);
		$collect_amount = (float)$o_package_container->get_recurring_by_attribute('name', $_POST['upgrade_account']);
		$i_old_package = (int)$o_package_container->get_level_by_attribute('name', $_POST['upgrade_from']);
		$i_new_package = (int)$o_package_container->get_level_by_attribute('name', $_POST['upgrade_account']);
		error_log("Upgrade cost: $i_upgrade_cost");

		// check that this card can be charged
		if ($b_accept_test_cc) {
			$auth_result = array('success'=>TRUE, 'response'=>'testing - Ben Bean');
		}
		else if ($i_upgrade_cost === 0.0) {
			$auth_result = array('success'=>TRUE, 'response'=>'No upgrade cost');
		}
		else {
			$auth_result = authorize_card_for_transaction("charge",$p_companyid,$p_dataname,$i_upgrade_cost,$card_info,$customer_info);
		}

		/* check if $auth_result is true
		    if so, then check that a licensing invoice doesn't already exist for today
		        if so, then add the value for the upgrade cost to the invoice
		        otherwise, create an invoice with the upgrade cost
		    upgrade the status of the account */
		error_log(print_r($auth_result,TRUE));
		if (!$auth_result['success'])
			return "failed to charge card";

		$s_username = admin_info("username");
		error_log("stuff: ".$p_dataname.":".$_POST['upgrade_from'].":".$_POST['upgrade_account'].":".$s_username);
		ob_start();
		// upgrade the account
		$doing_upgrade_success = upgrade_account_to_new_license($p_dataname, $_POST['upgrade_from'], $_POST['upgrade_account'], "restaurant:".$s_username, $i_upgrade_cost);
		error_log("ben_dev, billing: ".$doing_upgrade_success);

		/***********************************************************************
		 * update the billing profile and package level,
		 * and award points to the distributor
		 **********************************************************************/

		// update the session package level so CP knows about it
		set_sessvar('package', $i_new_package);

		// create the new billing profile
		$b_upgrade_update_hosting = TRUE;
		$hosting_amount = $collect_amount;


		// Need to blank out the payment_status trial end date to make the standard CP "Evaulation Mode: x Days" message go away for GoldTrial, ProTrial, Lavu88Trial package levels.
		if ( strpos($_POST['upgrade_from'], 'Trial') !== false ) {
			// update the package information
			mlavu_query("UPDATE `[maindb]`.`payment_status` SET `trial_end`='' WHERE `dataname`='[dataname]' AND `trial_end` != ''", array("maindb"=>$maindb, "dataname"=>$p_dataname));
			if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
				$o_emailAddressesMap->sendEmailToAddress("upgrade_failure_emails", "bad upgrade", "Failed to blank trial end date for ".$p_dataname." (upgrading from ".$i_old_package." to ".$i_new_package.")");
		}

		// find the billing profile
		error_log("ben_dev, billing: maindb(poslavu_MAIN_db) _maindb($maindb) dataname($p_dataname)");
		$billing_profile_query = mlavu_query("SELECT * FROM `[maindb]`.`billing_profiles` WHERE `dataname`='[dataname]' ORDER BY `id` DESC LIMIT 1", array("maindb"=>"poslavu_MAIN_db", "dataname"=>$p_dataname, "old_package"=>$i_old_package, "new_amount"=>$collect_amount));
		if ($billing_profile_query !== FALSE)
			if (mysqli_num_rows($billing_profile_query))
				$billing_profile_read = mysqli_fetch_assoc($billing_profile_query);
		if (count($billing_profile_read) > 0) {

			// update the billing profile
			$s_update_amount = "";
			if ((float)$billing_profile_read['amount'] < $collect_amount)
				$s_update_amount = ",`amount`='[new_amount]'";
			mlavu_query("UPDATE `[maindb]`.`billing_profiles` SET `package`='[new_package]'$s_update_amount WHERE `id`='[id]'",
				array("maindb"=>$maindb, "new_package"=>$i_new_package, "new_amount"=>$collect_amount, "id"=>$billing_profile_read['id']));
			if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
				$o_emailAddressesMap->sendEmailToAddress("upgrade_failure_emails", "bad upgrade", "Failed to update billing profile for ".$p_dataname." (upgrading from ".$i_old_package." to ".$i_new_package.")");

			// update the package information
			mlavu_query("UPDATE `[maindb]`.`signups` SET `package`='[new_package]' WHERE `dataname`='[dataname]'", array("maindb"=>$maindb, "new_package"=>$i_new_package, "dataname"=>$p_dataname));
			if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
				$o_emailAddressesMap->sendEmailToAddress("upgrade_failure_emails", "bad upgrade", "Failed to update signup package for ".$p_dataname." (upgrading from ".$i_old_package." to ".$i_new_package.")");

			// award points to the distributor
			require_once(dirname(__FILE__)."/../../../distro/beta/distro_data_functions.php");
			$s_distro_code = '';
			$apply_points_results = apply_distro_credits_for_transaction($i_upgrade_cost, array(), '', $p_dataname, TRUE, $s_distro_code);
			error_log($apply_points_results);
		}
		// Create a billing profile for accounts upgrading from a Trial package level since it's a credit card-less signup.
		else if (count($billing_profile_read) === 0 && strpos($_POST['upgrade_from'], 'Trial') !== false) {
			$result = create_new_hosting_profile($p_companyid,$p_dataname,$a_immutables['hosting_start'],$hosting_start,$card_info,$customer_info,$card_info['number'],$b_accept_8484);
			if (!$result['success'])
				return "failed to create billing profile";
		}
		else {
			$o_emailAddressesMap->sendEmailToAddress("upgrade_failure_emails", "Could not find billing profile for {$p_dataname}", "Doing upgrade for {$p_dataname}. Cost \${$i_upgrade_cost}. Could not find billing profile.");
		}
		$contents_to_discard = ob_get_contents();
		ob_end_clean();

		return "success";
	}
}


/**
 * Create a row in reseller_licenses for this BlueStar distro account
 */

function createBlueStarLicense($activation_code, $restaurant_id, $distro_id, $distro_name, $license_name)
{
	global $o_package_container;

	// Get how much the license is worth using its package name.  Even though we just set the license cost to $0, since BlueStar accounts don't pay for the license.
	$license_value = $o_package_container->get_value_by_attribute('name', $license_name);
	$collected_amount = '0';

	// Insert a new license into reseller_licenses table and create a distro history event for it.

	$vars = array(
		'type' => $license_name,
		'resellerid' => $distro_name,
		'resellername' => $distro_id,
		'restaurantid' => $restaurant_id,
		'cost' => '0',
		'value' => $license_value,
		'purchased' => date('Y-m-d H:i:s'),
		'applied' => date('Y-m-d H:i:s'),
		'notes' => "BlueStar Activation Code {$activation_code}",
		'promo_name' => 'bluestar_activation_code',
		'promo_expiration' => ''
	);

	mlavu_query("insert into `poslavu_MAIN_db`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`restaurantid`,`cost`,`value`,`purchased`,`applied`,`notes`,`promo_name`,`promo_expiration`) values ('[type]','[resellerid]','[resellername]','[restaurantid]','[cost]','[value]','[purchased]','[applied]','[notes]','[promo_name]','[promo_expiration]')", $vars);
	$license_id = mlavu_insert_id();

	add_to_distro_history("bought license", "type: {$license_name}\ncollected: {$collected_amount}\nvalue: {$license_value}", $distro_id, $distro_name);

	return $license_id;
}


/**
 * Apply the BlueStar license corresponding to the supplied license ID.
 */

function applyBlueStarLicense($activation_code, $dataname, $company_name, $restaurant_id, $payment_status_id, $distro_name, $distro_id, $license_name, $license_id, $created_date)
{
	global $o_package_container;

	// Code based on apply_license_step2() in /distro/beta/exec/apply_license.php (which I couldn't call directly due because there was executable code in apply_license.php).
	// Must run all of these update statementse to apply the license.

	$current_timestamp = date("Y-m-d H:i:s");

	mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_licenses` SET `restaurantid`='[1]', `applied`='[2]' WHERE `id`='[3]' ", $restaurant_id, $current_timestamp, $license_id);

	// BlueStar Lavu 88 licenses have special logic that auto-waives all invoices within 90 days (really, we say 88 so the 3rd month's invoice is not waived).
	if ($license_name == 'Lavu88') {
		$createdate_components = explode(' ', $created_date);
		$auto_waive_date = date( "Y-m-d", strtotime( $createdate_components[0] . " +88 days" ) );
		mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `license_applied`='[1]', `license_type`='[2]', `license_resellerid`='[3]', `auto_waive_until`='[4]' WHERE `id`='[5]'", $license_id, $license_name, $distro_id, $auto_waive_date, $payment_status_id);
	} else {
		mlavu_query("UPDATE `poslavu_MAIN_db`.`payment_status` SET `license_applied`='[1]', `license_type`='[2]', `license_resellerid`='[3]' WHERE `id`='[4]'", $license_id, $license_name, $distro_id, $payment_status_id);
	}

	mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `license_applied`='[date]', `_finished`='1', `_canceled`='0', `promo`='[promo]' WHERE `data_name`='[dataname]'", array('date' => $current_timestamp, 'dataname' => $dataname, 'promo' => $activation_code));

	// update the account package level
	$i_new_package = $o_package_container->get_level_by_attribute('name', $license_name);
	change_package($dataname, $dataname, $i_new_package, $restaurant_id);

	// add to the distro history
	add_to_distro_history("applied license","applyto: {$dataname}\ncompany_name: {$company_name}\nlicense_id: {$license_id}\nlicense_type: {$license_name}", $distro_id, $distro_name);

	SalesforceIntegration::logSalesforceEvent($dataname, 'Closed Won');

	return true;
}


/**
 * Decode the package base level from the BlueStar Activation Code and then finding the package name of the highest, current version of that package level (ex. 2 => Gold3).
 */

function getBlueStarPackageName($activation_code)
{
	global $o_package_container;

	// The 5th character of the BlueStar Activation Code is the package base level.
	$package_baselevel = substr($activation_code, 4, 1);

	// Get the mapping for the index of the array returned by package_container::get_highest_base_package_names() to our package baselevel.
	$current_package_index = '';
	switch ($package_baselevel) {
		case '1':
			$current_package_index = 0;
			break;
		case '2':
			$current_package_index = 1;
			break;
		case '3':
			$current_package_index = 2;
			break;
		case '8':
			$current_package_index = 6;  // 2014-11-17 tom fixed this index changing after the Fit licenses were deployed.
			break;
	}

	$current_package_names = $o_package_container->get_highest_base_package_names();

	return $current_package_names[$current_package_index];
}


/*******************************************************************************
 *                     E N D   O F   F U N C T I O N S                         *
 *         B E G I N N I N G   O F   E X E C U T A B L E   C O D E             *
 ******************************************************************************/

$dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();


if ($p_area!="billing_info" && $p_area!="customer_billing_info")
{
	return;
}

ob_start();

/** Stopped trying to redirect http to https on 2014-11-24 because it creates an infinite redirect loop now that SSL termination happens at the load balancer, making connections from there to our web server unencrypted.
if ($_SERVER['SERVER_PORT']!=443) // quit if not ssl
{
	$new_url = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	echo "<script language='javascript'>";
	echo "window.location.replace('$new_url'); ";
	echo "</script>";
	exit();
	$p_area = "fwd";
}*/

if(isset($_GET['type']) && isset($_GET['typeid']) && isset($_GET['typename'])) {
	$b_baseurl = "psetup.php?type=details&typeid=".$_GET['typeid']."&typename=".$_GET['typename'];
} else {
	$b_baseurl = "index.php?mode=billing";
}
$cancel_step = (isset($_GET['cancel_step']))?$_GET['cancel_step']:"";
$activation_code = empty($_REQUEST['activation_code']) ? '' : $_REQUEST['activation_code'];
//error_log("cancel_step: " . $cancel_step);

if($cancel_step!="")
{
	$tempinfostr = "";
	$tempinfostr .= "username=" . admin_info('username');
	
	mlavu_query("insert into `poslavu_MAIN_db`.`temp_info` (`dataname`,`type`,`datetime`,`request`,`info`) values ('[1]','[2]','[3]','[4]','[5]')",$p_dataname,"cancel_visit",date("Y-m-d H:i:s"),$cancel_step,$tempinfostr);
}

if($cancel_step=="callback")
{		
		if($form_posted){
			CPActionTracker::track_action($section . '/' . $mode, 'info_switch_callback_posted');
		}
		else {
			CPActionTracker::track_action($section . '/' . $mode, 'info_switch_callback');
		}
		$sendlist = array("corey@lavu.com","lucas@lavu.com","b.orourke@lavu.com","jon@lavu.com","anna@lavu.com");//"test@poslavu.com","corey@lavu.com","lucas@lavu.com","ag@lavu.com");
		$fieldtitles = array();
		$fieldtypes = array();
		$fieldprops = array();
		
		$fieldlist = array("Full Name","Phone","Email","Best Time","Reason");
		$fieldtitles['Phone'] = "Phone Number";
		$fieldtitles['Email'] = "Email Address";
		$fieldtitles['Best Time'] = "When is the best time to contact you? (please include timezone)";
		$fieldtypes['Reason'] = "select_info";
		$fieldprops['Reason'] = array("Just trying it out::Do you have any questions or comments while you think over your decision?","Connectivity / Hardware issues::Printers|Internet Speeds|Networking|Other peripherals","Lavu does not have the feature(s) I need::What feature(s) do you need, maybe we can help?","My business is seasonal::We hope you enjoy your break! When will you be coming back? Do you have any feedback concerning the Lavu software to improve your experience when you return?","Going Out of Business::We’re sorry to hear that, any additional feedback concerning the Lavu software?","Other::Do you have any feedback that would help Lavu better serve its customers?");
		
		$form_posted = false;
		$field_output = "";
		$field_validation = "";
		$posted_str = "";
		for($n=0; $n<count($fieldlist); $n++)
		{
			$fieldtitle = $fieldlist[$n];
			$ftype = "text";
			$fprops = array();
			$fieldname = strtolower(trim(str_replace(" ","",$fieldtitle)));
			if(isset($fieldtitles[$fieldtitle])) $fieldtitle = $fieldtitles[$fieldtitle];
			if(isset($fieldtypes[$fieldtitle])) $ftype = $fieldtypes[$fieldtitle];
			if(isset($fieldprops[$fieldtitle])) $fprops = $fieldprops[$fieldtitle];
			
			if($ftype=="select_info")
			{
				$jstr = "";
				$jstr .= " extra_info_list_$fieldname = new Array(); ";
				
				$istr = "";
				$istr .= "<select name='$fieldname' onchange='select_changed_$fieldname(this.value)'>";
				$istr .= "<option value=''></option>";
				for($p=0; $p<count($fprops); $p++)
				{
					$prop_parts = explode("::",$fprops[$p]);
					$prop_info_str = "";
					if(count($prop_parts) > 1) $prop_info_str = trim($prop_parts[1]);
					$prop_info_str_parts = explode("|",$prop_info_str);
					if(count($prop_info_str_parts) > 1)
					{
						$prop_info_str = "";
						for($s=0; $s<count($prop_info_str_parts); $s++)
						{
							$prop_info_str_name = $prop_info_str_parts[$s];
							if($prop_info_str!="") $prop_info_str .= "<br>";
							$prop_info_str .= "<input type=\"radio\" name=\"subval_$fieldname\" value=\"$prop_info_str_name\"> $prop_info_str_name";
						}
						$prop_info_str .= "";
					}
					else if($prop_info_str!="")
					{
						$prop_info_str .= "<br><textarea name=\"subval_$fieldname\" style=\"margin:8px\" rows=\"3\" cols=\"36\"></textarea>";
					}
					
					$prop_selection = trim($prop_parts[0]);
					$istr .= "<option value='$prop_selection'>$prop_selection</option>";
					$jstr .= " extra_info_list_".$fieldname."['$prop_selection'] = '$prop_info_str'; ";
				}
				$istr .= "</select>";
				$istr .= "<div id='select_extra_info_$fieldname' style='display:none'>";
				$istr .= "	<table cellspacing=6 cellpadding=6><tr><td>";
				$istr .= "	<div id='select_extra_info_content_$fieldname'></div>";
				$istr .= "	</td></tr></table>";
				$istr .= "</div>";
				$istr .= "<script type='text/javascript'>";
				$istr .= "	$jstr ";
				$istr .= "	function select_changed_$fieldname(val) { ";
				$istr .= "		var infostr_title = extra_info_list_".$fieldname."[val]; ";
				$istr .= "		if(infostr_title!='' && typeof infostr_title != 'undefined') { ";
				$istr .= "			document.getElementById('select_extra_info_content_$fieldname').innerHTML = infostr_title; ";
				$istr .= "			document.getElementById('select_extra_info_$fieldname').style.display = 'block'; ";
				$istr .= "		} ";
				$istr .= "		else { ";
				$istr .= "			document.getElementById('select_extra_info_$fieldname').style.display = 'none'; ";
				$istr .= "      } ";
				$istr .= "  } ";
				$istr .= "</script>";
			}
			else
			{
				$istr = "<input type='text' name='$fieldname'>";
			}
			
			$field_output .= "<tr><td align='right' valign='top'>$fieldtitle</td><td>$istr</td></tr>\n";
			if($field_validation!="") $field_validation .= " else ";
			$field_validation .= "if(document.switch_form.$fieldname.value==\"\") alert(\"Please provide a value for \\\"$fieldtitle\\\" to continue\"); ";
			if(isset($_POST[$fieldname]))
			{
				$posted_str .= $fieldtitle . ": " . $_POST[$fieldname] . "\n";
				if(isset($_POST['subval_'.$fieldname]))
				{
					$posted_str .= $fieldtitle . " (info): " . $_POST['subval_'.$fieldname] . "\n";
				}
				$form_posted = true;
			}
		}
		$field_validation .= " else if(document.switch_form.email.value.indexOf(\"@\") < 1 || document.switch_form.email.value.indexOf(\".\") < 1) alert(\"Please provide a valid email to continue\"); ";
		$field_validation .= " else document.switch_form.submit(); ";
		
		$submit_mouse_commands = "onmouseover='document.getElementById(\"try_three_btn\").style.backgroundColor = \"#d2e97a\"' onmouseout='document.getElementById(\"try_three_btn\").style.backgroundColor = \"#ffffff\"' onclick='$field_validation'";

		if(!$form_posted)
		{
			echo "<input type='button' value='<< Back' onclick='cp_track_action(\"billing/billing\", \"cancel_step_back_callback\"); window.location = \"$b_baseurl\"' />";
		}
		echo "<form name='switch_form' method='post' action=''>";
		echo "<br><br>";
		
		echo "<table cellpadding=12 cellspacing=0 bgcolor='#f2f6f2' style='border: solid 1px #849e20'>";
		echo "<tr><td width='480' align='center' style='color:#ffffff;' bgcolor='#849e20'>Account Cancellation</td></tr>";
		//echo "<tr><td width='320' align='center'>";
		//echo "We are sorry to hear that you wish to cancel your account at this time. Please fill out the form below and a Client Services Representative will contact you by the end of the next business day to confirm your account’s cancellation. Thank you.";
		//echo "</td></tr>";
		echo "<tr><td width='480' align='left' style='color:#558855'>";
		echo "  <table width='100%'><tr><td width='100%' align='center' style='color:#558855'>";
		if($form_posted)
		{
			$rest_query = mlavu_query("select * from poslavu_MAIN_db.restaurants where `data_name`='[1]'",$data_name);
			if(mysqli_num_rows($rest_query))
			{
				$rest_read = mysqli_fetch_assoc($rest_query);
				$total_paid = 0;
				$pay_query = mlavu_query("select x_amount, x_type from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1'",$rest_read['id']);
				while($pay_read = mysqli_fetch_assoc($pay_query))
				{
					if($pay_read['x_type']=="auth_capture") $total_paid += $pay_read['x_amount'] * 1;
					else if($pay_read['x_type']=="credit") $total_paid += $pay_read['x_amount'] * -1;
				}
				
				$package_level = "";
				$signup_query = mlavu_query("select * from poslavu_MAIN_db.signups where `dataname`='[1]'",$data_name);
				if(mysqli_num_rows($signup_query))
				{
					$signup_read = mysqli_fetch_assoc($signup_query);
					$p = $signup_read['package'];
					if($p * 1 % 10 == 1) $package_level = "Silver";
					else if($p * 1 % 10 == 2) $package_level = "Gold";
					else if($p * 1 == 3) $package_level = "Platinum";
					else if($p * 1 % 10 == 3) $package_level = "Pro";
					else if($p * 1 == 88) $package_level = "Lavu88";
					else if(trim($p) != "") $package_level = "#" . $p;
				}
				
				$posted_str .= "\n";
				$posted_str .= "Dataname: " . $rest_read['data_name'] . "\n";
				$posted_str .= "Company Name: " . $rest_read['company_name'] . "\n";
				$posted_str .= "Account Created: " . $rest_read['created'] . "\n";
				$posted_str .= "Package Level: " . $package_level . "\n";
				$posted_str .= "Total Paid: $" . number_format($total_paid,2) . "\n";
			}
			
			//echo "<textarea rows='20' cols='80'>$posted_str</textarea><br><br>";
			
			mlavu_query("insert into `poslavu_MAIN_db`.`temp_info` (`dataname`,`type`,`datetime`,`request`,`info`) values ('[1]','[2]','[3]','[4]','[5]')",$rest_read['data_name'],"cancel_callback",date("Y-m-d H:i:s"),$cancel_step,$posted_str);

			for($n=0; $n<count($sendlist); $n++)
			{
				mail($sendlist[$n],"Lavu Cancellation Callback",$posted_str,"From: lavucancel@poslavu.com");
			}
			echo "Thank You<br><br>If you are not contacted after completing this form by the end of the next business day, please email billing@poslavu.com.<br><br>";
		}
		else
		{
			echo "We are sorry to hear that you wish to cancel your account at this time. Please fill out the form below and a Client Services Representative will contact you by the end of the next business day to confirm your account’s cancellation.<br>Thank you.";
			echo "          <br>&nbsp;";
			echo "          <table celpadding=2>";
			echo "                  $field_output";
			echo "                  <tr><td>&nbsp;</td>";
			echo "                          <td><input id='try_three_btn' type='button' style='color:#000000; background-color:#ffffff; border-radius: 8px; border:solid 1px #849e20; padding:4px; cursor:pointer' value='Submit >>' $submit_mouse_commands /></td>";
			echo "                  </td></tr>";
			echo "          </table>";
		}
		echo "  </td></tr></table>";
		echo "</td></tr></table>";
		
		echo "</form>";
}
else if($cancel_step=="info_switch_three")
{		
	if($form_posted){
		CPActionTracker::track_action($section . '/' . $mode, 'info_switch_three_posted');
	}
	else {
		CPActionTracker::track_action($section . '/' . $mode, 'info_switch_three');
	}
		$sendlist = array("test@poslavu.com","corey@lavu.com","lucas@lavu.com","ag@lavu.com");
		$fieldlist = array("Full Name","Phone","Email");
		$form_posted = false;
		$field_output = "";
		$field_validation = "";
		$posted_str = "";
		for($n=0; $n<count($fieldlist); $n++)
		{
			$fieldtitle = $fieldlist[$n];
			$fieldname = strtolower(trim(str_replace(" ","",$fieldtitle)));
			$field_output .= "<tr><td align='right'>$fieldtitle</td><td><input type='text' name='$fieldname'></td></tr>\n";
			if($field_validation!="") $field_validation .= " else ";
			$field_validation .= "if(document.switch_form.$fieldname.value==\"\") alert(\"Please provide a value for \\\"$fieldtitle\\\" to continue\"); ";
			if(isset($_POST[$fieldname]))
			{
				$posted_str .= $fieldtitle . ": " . $_POST[$fieldname] . "\n";
				$form_posted = true;
			}
		}
		$field_validation .= " else if(document.switch_form.email.value.indexOf(\"@\") < 1 || document.switch_form.email.value.indexOf(\".\") < 1) alert(\"Please provide a valid email to continue\"); ";
		$field_validation .= " else document.switch_form.submit(); ";
		
		$submit_mouse_commands = "onmouseover='document.getElementById(\"try_three_btn\").style.backgroundColor = \"#d2e97a\"' onmouseout='document.getElementById(\"try_three_btn\").style.backgroundColor = \"#ffffff\"' onclick='$field_validation'";

		if(!$form_posted)
		{
			echo "<input type='button' value='<< Back' onclick='cp_track_action(\"billing/billing\", \"cancel_step_back2\"); window.location = \"$b_baseurl&cancel_step=info\"' />";
		}
		echo "<form name='switch_form' method='post' action=''>";
		echo "<br><br>";
		
		echo "<table cellpadding=12 cellspacing=0 bgcolor='#f2f6f2' style='border: solid 1px #849e20'>";
		echo "<tr><td width='320' align='center' style='color:#ffffff;' bgcolor='#849e20'>Obtain The Newest Lavu 3 Pre-Release</td></tr>";
		echo "<tr><td width='320' align='left' style='color:#558855'>";
		echo "  <table width='100%'><tr><td width='100%' align='center' style='color:#558855'>";
		if($form_posted)
		{
			for($n=0; $n<count($sendlist); $n++)
			{
			        mail($sendlist[$n],"Lavu 3 Request Save",$posted_str,"From: lavu3request@poslavu.com");
			}
			echo "          Your pre-release download of Lavu 3 will be prepared.  Our staff will contact you shortly.";
		}
		else
		{
			echo "          Please provide your best contact info below so that we can provide you with a pre-release download of the newest Lavu 3.  Our staff will contact you shortly.";
			echo "          <br>&nbsp;";
			echo "          <table celpadding=2>";
			echo "                  $field_output";
			echo "                  <tr><td>&nbsp;</td>";
			echo "                          <td><input id='try_three_btn' type='button' style='color:#000000; background-color:#ffffff; border-radius: 8px; border:solid 1px #849e20; padding:4px; cursor:pointer' value='Submit >>' $submit_mouse_commands /></td>";
			echo "                  </td></tr>";
			echo "          </table>";
		}
		echo "  </td></tr></table>";
		echo "</td></tr></table>";

		echo "</form>";
}
else if($cancel_step=="entry")
{
	echo "<input type='button' value='<< Back' onclick='cp_track_action(\"billing/billing\", \"cancel_step_back1\"); window.location = \"$b_baseurl\"' />";
	echo "<br><br>";
	echo "<table bgcolor='#eeeeee' style='border:solid 1px #aaaaaa' cellspacing=0 cellpadding=12><tr><td align='center' width='600'>";

	if(1)
	{
		if(isset($p_companyid) && is_numeric($p_companyid) && $p_companyid > 0)
		{
			$pay_exist_query = mlavu_query("select x_amount, x_type from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid`='[1]' and (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1'",$p_companyid);
			if(mysqli_num_rows($pay_exist_query))
			{
				$cancel_url_step = "callback";
			}
			else
			{
				$cancel_url_step = "info";
			}
		}
		else
		{
			$cancel_step = "info";
		}
		echo "<p style='width:60%' id='cancelBillingDisclaimer'>If you decide that you no longer wish to use your Lavu Account, it is your responsibility to cancel the account. If you do not cancel your account, your monthly recurring fee will continue to be charged until you complete the cancellation process.</p>";
		echo "<br>To cancel your account, please click <a href='".$b_baseurl."&cancel_step=".$cancel_url_step."'>here</a><br><br>";
	}
	else
	{
		// echo "<p style='width:60%' id='cancelBillingDisclaimer'>If you decide that you no longer wish to use your Lavu Account, it is your responsibility to cancel the account by clicking <strong>Cancel All Billing &amp; Disable Account</strong> below.</p>";
		// echo "<input type='button' value='Cancel All Billing & Disable Account' onclick='window.location = \"".$b_baseurl."&cancel_step=info\"'>";
	}
	echo "</td></tr></table>";
}
else if($cancel_step=="info")
{
	CPActionTracker::track_action($section . '/' . $mode, 'cancel_step_info');
	echo "<input type='button' value='<< Back' onclick='cp_track_action(\"billing/billing\", \"cancel_step_back1\"); window.location = \"$b_baseurl&cancel_step=entry\"' />";
	echo "<br><br>";
	echo "<b><font color='#770000'>*** Important ***</font></b>";
	echo "<br><br>";
	echo "<table style='border:solid 1px #777777' bgcolor='#eeeeee' cellpadding=20><tr><td align='center'>";
	echo "Canceling your billing will also disable your account.<br>Once your account is disabled you will no longer be able to operate POSLavu";
	echo "</td></tr></table>";
	echo "<br><br>";

	echo "<form name='cancel_form' method='post' action='$b_baseurl&cancel_step=confirm'>";
	echo "<input type='hidden' name='confirm_cancel' id='confirm_cancel' value=''>";
	echo "<input type='hidden' name='confirm_radio' id='confirm_radio' value=''>";
	echo "<table cellpadding=4><td style='border-bottom:solid 1px #555588; color:#555566' align='center'>If you are canceling your account, we're interested in knowing why.  <br>We appreciate your feedback.</td></table>";
	echo "<table>";
	$network_issue_string = "Connectivity, Network or Internet Issues";
	$reasons = array("Just trying it out","My business is seasonal",$network_issue_string,"Planning on signing up later","Some Features I would like are not present","Trouble with hardware","I'm getting out of the restaurant business","I don't know","It's not you it's me","Other Reasons");
	for($n=0; $n<count($reasons); $n++)
	{
		$reason = $reasons[$n];
		if($reason==$network_issue_string)// && $_SERVER['REMOTE_ADDR']=="216.243.114.158"
		{
				echo "<tr><td><input type='radio' name='reason' value=\"$reason\" onclick='document.getElementById(\"confirm_radio\").value = this.value; document.getElementById(\"connect_row\").style.display = \"table-row\"'></td><td>$reason</td></tr>";
				echo "<tr style='display:table-row' id='connect_row'><td>&nbsp;</td><td valign='top' align='left' valign='top'>";

				// description table
				echo "<table cellpadding=12 cellspacing=0 bgcolor='#f2f6f2' style='border: solid 1px #849e20'>";
				echo "<tr><td width='320' align='center' style='color:#ffffff;' bgcolor='#849e20'>New Version of Lavu 3 available for Pre-Release</td></tr>";
				echo "<tr><td width='320' align='left' style='color:#558855'>";

				$try_mouse_commands = "onmouseover='document.getElementById(\"try_three_btn\").style.backgroundColor = \"#d2e97a\"' onmouseout='document.getElementById(\"try_three_btn\").style.backgroundColor = \"#ffffff\"' onclick='window.location = \"$b_baseurl&cancel_step=info_switch_three\"'";
				echo "  If you would like to install a newer version 3 app instead of canceling, please click the following button:";
				echo "  <br><br><table width='100%' cellspacing=0 cellpadding=0><tr><td width='100%' align='center'>";
				echo "          <table cellspacing=0 cellpadding=4><tr>";
				echo "                  <td valign='middle'><img src='/cp/images/Lavu_Monster_Icon.png' style='cursor:pointer' width='32' border='0' $try_mouse_commands /></td>";
				echo "                  <td valign='middle'><input id='try_three_btn' type='button' style='color:#000000; background-color:#ffffff; border-radius: 8px; border:solid 1px #849e20; padding:8px; cursor:pointer' value='Switch To the Newest Lavu 3 Version >>' $try_mouse_commands /></td>";
				echo "          </tr></table>";
				echo "  </td></tr></table>";

				echo "</td></tr></table>";
				// end description table

				echo "</td></tr>";
		}
		else
		{
			echo "<tr><td><input type='radio' name='reason' value=\"$reason\" onclick='document.getElementById(\"confirm_radio\").value = this.value'></td><td>$reason</td></tr>";
		}
	}
	echo "</table>";
	echo "Notes";
	echo "<br><textarea rows='4' cols='48' name='notes'></textarea>";

	echo "<br><br>";
	echo "<input type='button' value='Continue Cancellation and Disable your Account >>' onclick='if(document.getElementById(\"confirm_radio\").value==\"\") { alert(\"Please select a reason from the dropdown to continue\"); } else {cp_track_action(\"billing/billing\", \"cancel_step_continue\"); if(confirm(\"This will immediately disable the backend and stop all POS activity.  Are you sure?\")) {document.getElementById(\"confirm_cancel\").value = \"" . (($p_companyid * 44) + 9) . "\"; document.cancel_form.submit();}}' />";
	echo "</form>";
}
else if($cancel_step=="confirm")
{
	if(isset($_POST['confirm_cancel']))
	{
		CPActionTracker::track_action($section . '/' . $mode, 'cancel_step_final');
		$confirm_cancel = $_POST['confirm_cancel'];
		$decoded_confirm_cancel = (($confirm_cancel - 9) / 44);
		$reason = $_POST['reason'];
		$notes = $_POST['notes'];

		if($decoded_confirm_cancel==$p_companyid)
		{

			$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname`='[1]'",$p_dataname);
			if(mysqli_num_rows($signup_query))
			{
				$signup_read = mysqli_fetch_assoc($signup_query);
			}
			else $signup_read = false;

			$a_debug_subscription_info = array();

			$slist_info = get_signup_subscription_list($signup_read,$p_companyid);
			$subscription_ids = $slist_info['subscription_ids'];
			$canceled_arbs_str = "";
			for($n=0; $n<count($subscription_ids); $n++)
			{
				$a_debug_subscription_info[$subscription_ids[$n][0]] = array('subscriptionid'=>$subscription_ids[$n][0], 'start_date'=>$subscription_ids[$n][1], 'package'=>$subscription_ids[$n][3]);

				$sid = $subscription_ids[$n][0];
				$arb_status = get_current_arb_status($sid);
				if($arb_status=="active")
				{
					if($canceled_arbs_str!="")
						$canceled_arbs_str .= ",";
					$canceled_arbs_str .= $sid;

					$vars = array();
					$vars['refid'] = 16;
					$vars['subscribeid'] = $sid;
					$cancel_info = manage_recurring_billing("delete",$vars);
					if (!$cancel_info['success'])
						$o_emailAddressesMap->sendEmailToAddress('cancel_profile_failures', 'failed to cancel billing profile upon account cancelation', print_r($cancel_info,TRUE));
					mlavu_query("UPDATE `poslavu_MAIN_db`.`billing_profiles` SET `active`='0' WHERE `subscriptionid`='[1]' AND `subscriptionid`!='' LIMIT 1",$sid);
					echo "<br>canceling $sid";

					$a_debug_subscription_info[$subscription_ids[$n][0]]['cancel_info'] = $cancel_info;
				}

				$a_debug_subscription_info[$subscription_ids[$n][0]]['arb_status'] = $arb_status;
			}

			$query = mlavu_query("SELECT `subscriptionid`,`start_date`,`package` FROM `poslavu_MAIN_db`.`billing_profiles` WHERE `restaurantid`='[restaurantid]' OR `dataname`='[dataname]'", array('restaurantid'=>$p_companyid, 'dataname'=>$p_dataname));
			$a_reads = array();
			while($query !== FALSE && ($row = mysqli_fetch_assoc($query))) {
				$id = $row['subscriptionid'];
				if (!isset($a_debug_subscription_info[$id]))
					$a_debug_other_subscriptions = $row;
			}

			$a_signup_info = $dbapi->getAllInTable('signups', array('dataname'=>$p_dataname), TRUE);
			if (count($a_signup_info) == 0)
				$a_signup_info = array(array('success'=>'false'));
			$a_cancel_info = array(
				'dataname'=>$p_dataname,
				'restaurantid'=>$p_companyid,
				'canceled subscriptions'=>$a_debug_subscription_info,
				'other subscriptions'=>$a_debug_other_subscriptions,
				'signup'=>$a_signup_info[0]
			);
			$o_emailAddressesMap->sendEmailToAddress('account_cancelations', 'account canceled', "information about the canceled account:\n\n".print_r($a_cancel_info, TRUE));
			if ($p_dataname == 'test_pizza_sho')
				return;

			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$p_companyid);
			if(mysqli_num_rows($status_query))
			{
				$status_read = mysqli_fetch_assoc($status_query);
			}
			else
			{
				mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`) values ('[1]','[2]')",$p_companyid,$p_dataname);
				$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$p_companyid);
				if(mysqli_num_rows($status_query))
				{
					$status_read = mysqli_fetch_assoc($status_query);
				}
				else $status_read = false;
			}

			if(!$status_read || $status_read['canceled']!="1")
			{
				$cvars = array();
				$cvars['canceled'] = 1;
				$cvars['canceled_arbs'] = $canceled_arbs_str;
				$cvars['cancel_date'] = date("Y-m-d H:i:s");
				$cvars['cancel_info'] = admin_info("fullname") . "|" . $_SERVER['REMOTE_ADDR'];
				$cvars['cancel_reason'] = $reason;
				$cvars['cancel_notes'] = $notes;
				$cvars['restaurantid'] = $p_companyid;
				$cvars['history'] = "cancel_date: " . $cvars['cancel_date'] . "\ncancel_info: " . $cvars['cancel_info'] . "\ncancel_reason: " . $cvars['cancel_reason'] . "\ncancel_notes: " . $cvars['cancel_notes'] . "\n\n";
				mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `canceled`='[canceled]', `canceled_arbs`='[canceled_arbs]', `cancel_date`='[cancel_date]', `cancel_info`='[cancel_info]', `cancel_reason`='[cancel_reason]', `cancel_notes`='[cancel_notes]' where `restaurantid`='[restaurantid]'", $cvars);
			}
			mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `disabled`='2' where `id`='[1]'",$p_companyid);

			// create a record in the billing log
			$a_insert_vars = array('datetime'=>date('Y-m-d H:i:s'), 'dataname'=>$p_dataname, 'restaurantid'=>$p_companyid, 'username'=>admin_info("username"), 'fullname'=>admin_info("fullname"), 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'action'=>'cancel account', 'details'=>$reason.' ('.$notes.')');
			$s_insert_clause = $dbapi->arrayToInsertClause($a_insert_vars);
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`billing_log` $s_insert_clause", $a_insert_vars); // cancel account

			$reason_with_notes = empty($notes) ? $notes : $reason .'-'. $notes;
			$response = $zInt->cancelAccount(array('zAccountId' => $zAccountId, 'cancel_reason' => $reason_with_notes, 'cancel_date' => date('Y-m-d')));

			// Salesforce integration - adapted the $show_trial_alert logic here to determine if this is a distro-created trial account being cancelled or just a vanilla cancellation.
			$salesforce_event_name = empty($status_read['license_applied']) && !empty($status_read['trial_start']) ? 'Cancelled - Trial' : 'Cancelled';
			if ($reason == 'My business is seasonal') $salesforce_event_name = 'Seasonal';
			SalesforceIntegration::logSalesforceEvent($p_dataname, $salesforce_event_name);

			echo "<br><br>Cancellation Complete.  Your account will now be disabled. <br><br>Thank you for using POSLavu.";
			admin_logout();
		}
	}
}
// BlueStar activation code submitted for distro account in trial mode
else if ( empty($cancel_step) && !empty($activation_code) )
{
	require_once( dirname(__FILE__) .'/../../../distro/beta/promo_codes.php' );
	require_once( dirname(__FILE__) .'/../../../distro/beta/exec/distro_user_functions.php' );

	$license_name = '';
	$license_discount = '';
	$activation_code_ok = check_promo_code($activation_code, $license_name, $license_discount, true);

	// Good activation code
	if ( $activation_code_ok ) {

		// Restaurant database row
		$restaurant_results = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `id`='[1]'", $p_companyid);
		if ($restaurant_results === false) die(__METHOD__ ." had a query error: ". mlavu_dberror());
		$restaurant = mysqli_fetch_assoc($restaurant_results);

		// Reseller database row
		$reseller_results = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`resellers` WHERE `username`='[1]'", $restaurant['distro_code']);
		if ($reseller_results === false) die(__METHOD__ ." had a query error: ". mlavu_dberror());
		$reseller = mysqli_fetch_assoc($reseller_results);

		// Signups database row
		$signups_results = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[1]'", $p_dataname);
		if ($signups_results === false) die(__METHOD__ ." had a query error: ". mlavu_dberror());
		$signup = mysqli_fetch_assoc($signups_results);

		// Payment Status database row
		$payment_status_results = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`payment_status` WHERE `restaurantid`='[1]'", $p_companyid);
		if ($payment_status_results === false) die(__METHOD__ ." had a query error: ". mlavu_dberror());
		$payment_status = mysqli_fetch_assoc($payment_status_results);

		// Vars used in functions
		$restaurant_id = $restaurant['id'];
		$company_name = $restaurant['company_name'];
		$distro_id = $reseller['id'];
		$distro_name = $reseller['username'];
		$signup_id = $signup['id'];
		$payment_status_id = $payment_status['id'];
		$license_cost = '0';
		$create_date = $restaurant['created'];
		$package_name = getBlueStarPackageName($activation_code);

		// Functions that do most of the work
		$activation_code_taken = process_promo_code($activation_code, $distro_name, $package_name, $license_cost, $signup_id);
		$license_id = createBlueStarLicense($activation_code, $restaurant_id, $distro_id, $distro_name, $package_name);
		$license_applied_ok = applyBlueStarLicense($activation_code, $p_dataname, $company_name, $restaurant_id, $payment_status_id, $distro_name, $distro_id, $package_name, $license_id, $create_date);

		if ( $license_applied_ok ) {
			echo "<h2>Activation Code Accepted</h2>
			<p style=\"font-size:.8em; margin-bottom: 20px\">Your account has been activated.</p>
			<input type=\"button\" value=\"Proceed\" onclick=\"window.location = 'index.php?mode=home'\" />";
		}
		else {
			echo "<h2>Problem with Activation Code</h2>
			<form name=\"activation_code_form\" method=\"post\" action=\"index.php?mode=billing\">
			<input type=\"hidden\" name=\"mode\" value=\"billing\" />
			<table cellpadding=\"8\" style=\"border:solid 1px #bbbbbb\" bgcolor=\"#f6f6f6\" width=\"100%\"><tr><td valign=\"middle\">&nbsp;</td><td valign=\"middle\" align=\"center\">
			The Activation Code you entered was valid but our systems were not able to activate your account at this time.  Please contact Lavu Support for assistance.</td></tr>
			</table><br><br>
			</form>";
		}

	}
	// Bad activation code
	else {

		echo "<h2>Problem with Activation Code</h2>
		<form name=\"activation_code_form\" method=\"post\" action=\"index.php?mode=billing\">
		<input type=\"hidden\" name=\"mode\" value=\"billing\" />
		<table cellpadding=\"8\" style=\"border:solid 1px #bbbbbb\" bgcolor=\"#f6f6f6\" width=\"100%\"><tr><td valign=\"middle\">&nbsp;</td><td valign=\"middle\" align=\"center\">
		The Activation Code you entered appears to be invalid or has already been used.  Please verify you have entered the code correctly and try again.</td></tr>
		<tr><td colspan=\"2\" align=\"middle\" style=\"font-size:.8em\"><strong>Activation Code</strong>: <input type=\"text\" name=\"activation_code\" value=\"$activation_code\" /> <input type=\"submit\" name=\"activate\" value=\"Activate\" /></td></tr>
		</table><br><br>
		</form>";
	}
}
else
{
	if($p_area=="customer_billing_info")
	{
		CPActionTracker::track_action($section . '/' . $mode, 'billing_info');
		//global $p_companyid;
		//global $p_dataname;

		$str = "";
		if(admin_loggedin())
		{
			#if an account is locked out the show this alert message
			if(sessvar_isset('lockout_status') && sessvar('lockout_status')=="locked"){
				echo <<<HTML
				<script>$('#mainTable > table > tbody  .menu').find('td:eq(1)').hide();</script>
				<div class='upgrade_devices_container' style='margin-left:auto; margin-right:auto; border:1px solid #ebccd1; background-color:#f2dede; width:750px;color: #a94442;'>
					<table width="95%" align="center">
						<tr>
							<td width="100%"><strong>{$p_dataname},</strong> your account has been locked because your payment was either declined or the card on file has expired. Reenter your card information below to process payment and resume use of the POS.</td>
						</tr>
						<tr>
							<td>If you feel you have reached this message in error please call 1.855.767.5288 and choose the "billing" option.</td>
						</tr>
					</table>
				</div>
HTML;
			}

			$show_alert = false;
			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$p_companyid);
			if(mysqli_num_rows($status_query))
			{
				$status_read = mysqli_fetch_assoc($status_query);
				echo "
					<style type='text/css'>
						a.greenlink { color:#aecd37; }
						a.greenlink:hover {	color:gray;	}
					</style>
					<div style='position:relative; left:15px; text-align:left; color:gray;'>
						Have a question about your billing? <a href='?mode=assistance_support&department=billing' class='greenlink'>Click Here</a>
					</div>
					<br />";

				$hosting_action = $status_read['hosting_action'];
				$hosting_taken = $status_read['hosting_taken'];
				$hosting_amount = $status_read['hosting_amount'];
				$hosting_start = $status_read['hosting_start'];
				$collect_amount = $status_read['collect'];
				$collection_taken = $status_read['collection_taken'];
				$status_notes = $status_read['status_notes'];
				$show_hosting_amount = "$" . number_format((float)$hosting_amount,2);
				$show_collection_amount = "$" . number_format((float)$collect_amount,2);

				// Load the pre-computed minimum due date
				// This includes the custom payment plan
				// The minimum due date is re-computed in auth_result_success_ask_for_payment()
				$collection_due_date = $status_read['collection_due_date'];
				$collection_extend_days = $status_read['collection_extend_days'];
				$collection_minimum = $status_read['collection_minimum'];
				$custom_payment_plan = $status_read['custom_payment_plan'];
				$annual_agreement = $status_read['annual_agreement'];
				$min_due_date = $status_read['min_due_date'];
				$cpp_parts = explode("/",$custom_payment_plan);
				$cpp_amount = trim($cpp_parts[0]);
				if($cpp_amount!="")
				{
					if(count($cpp_parts) > 1)
					{
						$cpp_days = trim($cpp_parts[1]);
					}
					else
					{
						$cpp_days = 14;
					}
					if(!is_numeric($cpp_days)) $cpp_days = 14;

					$collection_minimum = $cpp_amount;
					$collection_extend_days = $cpp_days;
				}

				// load the restaurant's current package level and signup status
				$pckstatus = find_package_status($p_companyid);
				$signup_package = $pckstatus['package'];

				// which automatic recurring billing `billing_profiles` need to be canceled
				// used for upgrades and such
				$cancel_arbs = array();

				// Lookup amount of payments made to hosting through recurring billing within the last month
				$active_last_four = array();

				// get $active_last_four and $last_four (of the credit card)
				// the $min_datetime_ts and $min_datetime (each for one month ago)
				// and $per_month_total (the total payments received or credited to a subscription id)
				$per_month_total = 0;
				$profile_query = mlavu_query("select * from `poslavu_MAIN_db`.`billing_profiles` where (`restaurantid`='[1]' or `dataname`='[2]') and `type` LIKE 'hosting' and (`active`='1' or `arb_status`='active')",$p_companyid,$p_dataname);
				while($profile_read = mysqli_fetch_assoc($profile_query))
				{
					$last_four = trim($profile_read['last_four']);
					$min_datetime_ts = mktime(0,0,0,date("m") - 1,date("d"),date("Y"));
					$min_datetime = date("Y-m-d",$min_datetime_ts) . " 00:00:00";

					$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where (`x_type`='auth_capture' or `x_type`='credit') and `x_response_code`='1' and `x_subscriptionid`='[1]' and `datetime`>'[2]'",$profile_read['subscriptionid'],$min_datetime);
					while($response_read = mysqli_fetch_assoc($response_query))
					{
						if($response_read['x_type']=="auth_capture")
						{
							$per_month_total += ($response_read['x_amount'] * 1);
							if($last_four!="")
							{
								$active_last_four[$last_four] = 1;
							}
						}
						else if($response_read['x_type']=="credit")
							$per_month_total -= ($response_read['x_amount'] * 1);
					}

					$response_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `x_type`='auth_capture' and x_response_code='1' and `datetime`>'[1]' and `match_restaurantid`='[2]' and `x_account_number` LIKE '%XXXX[3]%'",$min_datetime,$p_companyid,$last_four);
					while($response_read = mysqli_fetch_assoc($response_query))
					{
						$profile2_query = mlavu_query("select * from `poslavu_MAIN_db`.`billing_profiles` where (`restaurantid`='[1]' or `dataname`='[2]') and `type` LIKE 'hosting' and (`active`='1' or `arb_status`='active') and `last_four`='[3]'",$p_companyid,$p_dataname,$last_four);
						if(mysqli_num_rows($profile2_query))
						{
							$profile2_read = mysqli_fetch_assoc($profile2_query);
							$active_last_four[$last_four] = $profile2_read['amount'];
						}
					}
				}

				// display information about the active last four
				if(isset($_GET['show_active_last_four']))
				{
					foreach($active_last_four as $key => $val)
					{
						echo "Active Last Four: " . $key . " = " . $val . "<br>";
					}
				}

				// get the restaurant's row from `poslavu_MAIN_db`.`restaurants`
				$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$p_companyid);
				if(mysqli_num_rows($rest_query))
				{
					$rest_read = mysqli_fetch_assoc($rest_query);
					$rest_created = $rest_read['created'];
				} else {
					exit();
				}

				// get the current monthly hosting cost and $signup_read
				$i_custom_monthly_hosting = -1;
				$monthly_hosting = 49.95;
				$signup_query = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `restaurantid`='[1]' or `dataname`='[2]'",$p_companyid,$p_dataname);
				if(mysqli_num_rows($signup_query))
				{
					$signup_read = mysqli_fetch_assoc($signup_query);
					$monthly_hosting = (float)$o_package_container->get_recurring_by_attribute('level', $signup_package);
					if($status_read['custom_monthly_hosting']!="" && $status_read['custom_monthly_hosting'] * 1 > 0 || isset($b_testing_get_next_bill_date))
					{
						$i_custom_monthly_hosting = (int)$status_read['custom_monthly_hosting'];
						$monthly_hosting = $status_read['custom_monthly_hosting'];
					}
				}

				// get the hosting_start date (based on (in priority order) invoices, billing profiles, and the restaurant created day)
				$a_signups_vars = array("status"=>"");
				if (isset($signup_read))
					$a_signups_vars = $signup_read;
				$a_billing_profiles = NULL;
				$i_next_bill_date = billing_get_next_bill_date($p_dataname, (int)$signup_package, $rest_created, $p_companyid, $a_signups_vars, $a_billing_profiles);
				$hosting_start = date("Y-m-d", $i_next_bill_date);
				$hosting_parts = explode("-",$hosting_start);
				$show_hosting_start = $hosting_parts[1] . "/" . $hosting_parts[2] . "/" . $hosting_parts[0];

				// load the due info from get_due_prop($due_info) and display the due total if an amount is due
				if(!$ask_for_payment)
				{
					$due_info = $status_read['due_info'];
					$due_total = get_due_prop("total",$due_info);
					$due_license = get_due_prop("license",$due_info);
					$due_hosting = get_due_prop("hosting",$due_info);
					$due_other = get_due_prop("other",$due_info);
					$due_since = get_due_prop("due",$due_info);

					if($due_total!="" && $due_since!="")
					{
						if($due_total * 1 > 0 && $due_since <= date("Y-m-d"))
						{
							$ask_for_payment = true;

							$auto_ask_for_hosting = true;
							if($auto_ask_for_hosting) // Added so we can switch people over when they pay owed money
							{
								$ask_for_hosting = true;
								$hosting_action = "replace_all";
								$hosting_amount = $monthly_hosting;
								$show_hosting_amount = "$" . number_format($hosting_amount,2);
								$hosting_taken = 0;
							}

							$collect_amount = $due_total;

							$due_type_count = 0;
							$status_notes = "Our Billing system indicates that the following amount(s) are due: ";
							if($due_license > 0)
							{
								$due_type_count++;
								$status_notes .= "<br>License: $" . number_format($due_license,2);
							}
							if($due_hosting > 0)
							{
								$due_type_count++;
								$status_notes .= "<br>Hosting: $" . number_format($due_hosting,2);
							}
							if($due_other > 0)
							{
								$due_type_count++;
								$status_notes .= "<br>Other: $" . number_format($due_other,2);
							}
							if($due_type_count > 1)
								$status_notes .= "<br>Total: $" . number_format($due_total,2);
						}
					}
				}

				// get the package and base ipad limits
				$i_package_ipad_limit = $o_package_container->get_ipad_limt_by_attribute('level', (int)$signup_package);
				$i_package_ipod_limit = $o_package_container->get_ipod_limt_by_attribute('level', (int)$signup_package);
				$i_base_ipad_limit = $o_device_limits->getBaseiPadLimit($i_package_ipad_limit, $p_dataname);
				$a_old_device_limits = $o_device_limits->getDeviceLimitsByDataname($p_dataname);
				$i_old_device_limit = $a_old_device_limits['iPad_max'];
				$f_cost_per_ipad = $o_device_limits->getCostPeriPad(array('current_package' => '' + $signup_package, 'custom_max_ipads' => '' + $i_old_device_limit, 'custom_max_ipods' => '' + $i_package_ipod_limit, 'annual_agreement' => $annual_agreement));

				$package_level_name = $o_package_container->get_plain_name_by_attribute('level', $signup_package);

				// setting up hosting for those that owe money
				if($per_month_total < $monthly_hosting && $ask_for_payment)
				{
					$hosting_amount = $monthly_hosting;

					// Bug #2731: Added Custom iPad Limit to hosting calcuation so new ARB will be for the correct amount.  Took iPad calculation from section for $collect_amount below.
					if ($package_level_name == 'Platinum3' || $package_level_name == 'Lavu' || $package_level_name == 'LavuEnterprise') {
						$hosting_amount = $monthly_hosting + Max($i_old_device_limit - $i_base_ipad_limit,0)*$f_cost_per_ipad;
					}

					$show_hosting_amount = "$" . number_format($hosting_amount,2);
					$hosting_taken = 0;
					$ask_for_hosting = true;
				}

				// check if the user is trying to upgrade the account
				$ask_for_upgrade = false;
				if(isset($_POST['upgrade_account']) && isset($_POST['upgrade_from']))
				{
					$ask_for_upgrade = true;
					$current_license_level = $_POST['upgrade_from'];
					$new_license_level = $_POST['upgrade_account'];
				}
				// second step of upgrading
				$doing_upgrade = FALSE;
				$b_upgrade_update_hosting = FALSE;
				if(isset($_POST['upgrade_with_cc_info'])) {
					$doing_upgrade = TRUE;
					$ask_for_upgrade = FALSE;
				}

				// check if the user wants to change their cc info for a subscription id
				if(isset($_GET['change_cc']))
				{
					$change_cc = $_GET['change_cc'];
					$profile_query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`billing_profiles` WHERE (`restaurantid`='[restaurantid]' OR `dataname`='[dataname]') AND `type` LIKE 'hosting' AND (`active`='1' or `arb_status`='active') AND `subscriptionid`='[change_cc]'", array('restaurantid'=>$p_companyid, 'dataname'=>$p_dataname, 'change_cc'=>$change_cc));
					if(mysqli_num_rows($profile_query))
					{
						$profile_read = mysqli_fetch_assoc($profile_query);
						$ask_for_cc_change = true;
						$ask_for_hosting = false;
						$ask_for_payment = false;
						$ask_for_upgrade = false;
					}
				}

				// Added hook for Free Lavu users comming in to enter their credit card information instead of agreeing to setting up integrated payments to keep Free Lavu free.
				$enter_cc = (isset($_REQUEST['enter_cc'])) ? '1' : '';
				if ( $enter_cc && $package_level_name == 'Lavu88Trial' && !isset($_POST['posted'])) {
					$current_license_level = $package_level_name;
					$new_license_level = 'Lavu88';  // TO DO : figure out the highest baselevel package cousin for the Free Lavu package

					$ask_for_hosting = true;
					$hosting_amount = $o_package_container->get_recurring_by_attribute('name', $new_license_level);
					$show_hosting_amount = "$" . number_format($hosting_amount,2);
				}

				// check if the user is trying to upgrade the ipad limit for the account
				$ask_for_ipad_upgrade = FALSE;
				$doing_ipad_upgrade = FALSE;
				$i_new_device_limit = 0;
				$f_predicted_hosting = ExtractedBillingFunctions::predictMonthlyHosting($hosting_start, $p_dataname, $signup_package, $i_custom_monthly_hosting);
				if(isset($_GET['set_num_ipads_customer']) || isset($_POST['set_num_ipads_customer_with_cc_info']) && !$ask_for_hosting && !$ask_for_payment && !$ask_for_upgrade) {
					$i_new_device_limit = (isset($_POST['set_num_ipads_customer_with_cc_info'])) ? (int)$_POST['set_num_ipads_customer_with_cc_info'] : (int)$_GET['set_num_ipads_customer'];
					if ($i_old_device_limit < $i_new_device_limit) {
						if(isset($_POST['set_num_ipads_customer_with_cc_info'])) {
							$doing_ipad_upgrade = TRUE;
						} else {
							$ask_for_ipad_upgrade = TRUE;
						}
						$ask_for_cc_change = FALSE;
					}
					$collect_amount = $f_predicted_hosting + Max($i_new_device_limit - $i_base_ipad_limit,0)*$f_cost_per_ipad;
				}

				// update the collection total variables
				if($ask_for_payment && $collection_minimum!="" && is_numeric($collection_minimum) && $collection_minimum * 1 < $collect_amount * 1)
				{
					$collect_total = $collect_amount;
					$show_collection_total = $show_collection_amount;
					$collect_amount = $collection_minimum;
				}
				$show_collection_amount = "$" . number_format((float)$collect_amount,2);

				// check if the user is trying to make changes to their account or
				// the restaurant owes us money
				if($ask_for_hosting || $ask_for_payment || $ask_for_cc_change || $ask_for_upgrade || $doing_upgrade || $ask_for_ipad_upgrade || $doing_ipad_upgrade)
				{

					// print a form to ask for credit card information
					$ask_display = "";
					$extra_urlvars = "";
					if (!isset($cancel_arbs)) $cancel_arbs = array();
					if (!isset($profile_query)) $profile_query = FALSE;
					if (!isset($profile_read)) $profile_read = array();
					if (!isset($collect_total)) $collect_total = 0;
					if (!isset($show_collection_amount)) $show_collection_amount = "";
					$a_immutables = array('ask_for_cc_change'=>$ask_for_cc_change, 'ask_for_upgrade'=>$ask_for_upgrade, 'change_cc'=>$change_cc, 'profile_read'=>$profile_read, 'ask_for_hosting'=>$ask_for_hosting, 'hosting_action'=>$hosting_action, 'show_hosting_start'=>$show_hosting_start, 'show_hosting_amount'=>$show_hosting_amount, 'doing_upgrade'=>$doing_upgrade, 'ask_for_payment'=>$ask_for_payment, 'collection_minimum'=>$collection_minimum, 'collect_amount'=>$collect_amount, 'current_license_level'=>$current_license_level, 'new_license_level'=>$new_license_level, 'status_notes'=>$status_notes, 'ask_for_ipad_upgrade'=>$ask_for_ipad_upgrade, 'doing_ipad_upgrade'=>$doing_ipad_upgrade, 'i_new_device_limit'=>$i_new_device_limit, 'collect_total'=>$collect_total, 'p_companyid'=>$p_companyid, 'p_dataname'=>$p_dataname);
					ExtractedBillingFunctions::echo_ask_display($a_immutables, $ask_display, $extra_urlvars, $cancel_arbs, $profile_query, $profile_read, $show_collection_amount);
					////echo "<table cellpadding=4>";
					////echo "<tr><td colspan='2'>";

					// create the fields for the credit card form
					$fields = array();
					////$fields[] = array("Credit Card Info","title","",FALSE,array(),"autocomplete='off'");
					////$fields[] = array("First Name","text","first_name",true,array(),"autocomplete='off'");
					////$fields[] = array("Last Name","text","last_name",true,array(),"autocomplete='off'");
					////$fields[] = array("Card Number","card_number","card_number",true,array(),"autocomplete='off'");
					////$fields[] = array("Card Expiration","card_expiration","card_expiration",true,array(),"autocomplete='off'");
					////$fields[] = array("CVV Code","card_code","card_code",true,array(),"autocomplete='off'");
					////$fields[] = array("Card on File", 'card_on_file', '', false, array());
					if ($ask_for_upgrade && !isset($_POST['posted'])) {
						$fields[] = array("Upgrade", "set", "upgrade_with_cc_info");
						$fields[] = array("Upgrade", "set", "upgrade_from", true, $current_license_level);
						$fields[] = array("Upgrade", "set", "upgrade_account", true, $new_license_level);
					}
					if ($ask_for_ipad_upgrade && !isset($_POST['posted'])) {
						$fields[] = array("Set Num iPads", "set", "set_num_ipads_customer_with_cc_info", TRUE, $_GET['set_num_ipads_customer']);
					}
					if ($package_level_name == 'Lavu88Trial' && !isset($_POST['posted'])) {
						// "Upgrade" the package level to the non-Free version of the license since the user just wants to pay with their credit card.
						$fields[] = array("Upgrade", "set", "upgrade_with_cc_info");
						$fields[] = array("Upgrade", "set", "upgrade_from", true, $current_license_level);
						$fields[] = array("Upgrade", "set", "upgrade_account", true, $new_license_level);
					}
					$show_form = true;

					if(isset($_POST['posted']))
					{
						// the form has already beed submitted and posted, check the credit card information and make billing changes
						$show_message = "";

						$card_info = array();
						$card_info['number'] = $_POST['card_number'];
						$card_info['expiration'] = $_POST['card_expiration'];
						$card_info['code'] = $_POST['card_code'];
						$card_info['first_name'] = $_POST['first_name'];
						$card_info['last_name'] = $_POST['last_name'];

						$customer_info = get_company_billing_info($p_companyid,$p_dataname);

						// get the credit card authorizations
						if (!isset($b_upgrade_ipad_limit_update_hosting)) $b_upgrade_ipad_limit_update_hosting = FALSE;
						if (!isset($doing_upgrade_success)) $doing_upgrade_success = FALSE;
						$a_immutables = array('doing_upgrade'=>$doing_upgrade, 'ask_for_cc_change'=>$ask_for_cc_change, 'ask_for_payment'=>$ask_for_payment, 'customer_info'=>$customer_info, 'change_cc'=>$change_cc, 'p_companyid'=>$p_companyid, 'p_dataname'=>$p_dataname, 'doing_ipad_upgrade'=>$doing_ipad_upgrade, 'i_new_device_limit'=>$i_new_device_limit, 'i_base_ipad_limit'=>$i_base_ipad_limit, 'f_cost_per_ipad'=>$f_cost_per_ipad, 'hosting_start'=>$hosting_start, 'maindb'=>$maindb, 'package'=>$signup_package);
						ExtractedBillingFunctions::get_auth_result($a_immutables, $collect_amount, $auth_result, $doing_upgrade_success, $b_upgrade_update_hosting, $hosting_amount, $card_info, $b_upgrade_ipad_limit_update_hosting);

						if($auth_result['success'])
						{
							if (!isset($ask_for_hosting)) $ask_for_hosting = FALSE;

							if($ask_for_payment)
							{

								$a_immutables = array("collect_amount"=>$collect_amount, "p_companyid"=>$p_companyid, "p_area"=>$p_area, "p_dataname"=>$p_dataname, "show_collection_amount"=>$show_collection_amount, "collect_total"=>$collect_total, "collection_minimum"=>$collection_minimum, "collection_due_date"=>$collection_due_date, "collection_extend_days"=>$collection_extend_days, "min_due_date"=>$min_due_date);
								////ExtractedBillingFunctions::auth_result_success_ask_for_payment($a_immutables, $show_message);
							} else {

								$a_immutables = array('doing_upgrade'=>$doing_upgrade, 'ask_for_cc_change'=>$ask_for_cc_change, 'card_info'=>$card_info, 'p_companyid'=>$p_companyid, 'p_dataname'=>$p_dataname, 'change_cc'=>$change_cc, 'doing_upgrade_success'=>$doing_upgrade_success);
								////ExtractedBillingFunctions::auth_result_success_part_1($a_immutables, $show_message);
							}

							$a_immutables = array('doing_upgrade'=>$doing_upgrade, 'ask_for_cc_change'=>$ask_for_cc_change, 'ask_for_payment'=>$ask_for_payment, 'card_info'=>$card_info, 'active_last_four'=>$active_last_four, 'b_upgrade_update_hosting'=>$b_upgrade_update_hosting, 'p_companyid'=>$p_companyid, 'p_dataname'=>$p_dataname, 'hosting_amount'=>$hosting_amount, 'hosting_start'=>$hosting_start, 'cancel_arbs'=>$cancel_arbs, 'b_upgrade_ipad_limit_update_hosting'=>$b_upgrade_ipad_limit_update_hosting);
							////ExtractedBillingFunctions::auth_result_success_part_2($a_immutables, $ask_for_hosting, $customer_info, $show_message, $show_form);
							if ($b_upgrade_ipad_limit_update_hosting) {
								$o_emailAddressesMap->sendEmailToAddress('billing_notifications', "Account just updated iPad limit", "The account {$p_dataname} just updated their iPad limit. They updated from {$i_old_device_limit} to {$i_new_device_limit}, with a base limit of {$i_base_ipad_limit}, for a new monthly hosting of {$hosting_amount}.");
							}

							SalesforceIntegration::createSalesforceEvent($p_dataname, 'stage_change', 'Closed Won');
						}
						else
						{
							$show_message = "<font color='#880000'><b>* Error:</b> ".$auth_result['response']."</p></font>";
							if (isset($doing_upgrade) && $doing_upgrade) {
								$fields[] = array("Upgrade", "set", "upgrade_with_cc_info");
								$fields[] = array("Upgrade", "set", "upgrade_from", true, $_POST["upgrade_from"]);
								$fields[] = array("Upgrade", "set", "upgrade_account", true, $_POST["upgrade_account"]);
							}
							if (isset($doing_ipad_upgrade) && $doing_ipad_upgrade) {
								$fields[] = array("Set Num iPads", "set", "set_num_ipads_customer_with_cc_info", TRUE, $_POST['set_num_ipads_customer_with_cc_info']);
							}
						}

						if($show_message!="")
						{
							echo "<table width='100%' cellpadding=6><td width='100%' align='center'>";
							echo $show_message;
							if(!$show_form)
							{
								echo "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php?mode=billing\"' />";
							}
							echo "</td></tr></table>";
						}
					}

					if($show_form) {
						// draw the payment form
						$pay_form = new CForm($fields,"index.php?mode=billing".$extra_urlvars);
						echo $pay_form->form_code();
					}
					echo "</td></tr>";
					echo "</table><br><br>"; // credit card info table
				}

				// Draw the Billing Summary table
				$monthly_cost = number_format($monthly_cost, 2);
				$package_display_name = (empty($signup_read['package']) || $signup_read['package'] == 'none') ? 'Lavu (Demo Account)' : $o_package_container->get_printed_name_by_attribute('level', $signup_package);
				$has_service_agreement = !empty($status_read['annual_agreement']) ? '1 year' : '';
				$is_demo_account_in_trial_mode = ($status_read['trial_end']!="" && $status_read['license_applied']=="" && $license_waived==false);
				$is_self_signup_in_trial_mode = ($signup_read['status'] == 'new' && empty($zAccountId));
				$trial_end_date = empty($status_read['trial_end']) ? $setup_props['first_hosting'] : $status_read['trial_end'];
				drawBillingSummary($p_dataname, $package_display_name, $monthly_cost, $i_old_device_limit, $show_collection_amount, $has_service_agreement, $is_demo_account_in_trial_mode, $is_self_signup_in_trial_mode, $trial_end_date, false);

				// draw the zuora payment method
				echo "<table width='100%' cellpadding=6><td width='100%' align='center'>";
				echo "<br><b>Payment Method</b><br>";
				if ($zPaymentMethod === null)
				{
					echo "<table>
					<tr><td align='center' class='info_value' style='padding:4px 50px 4px 50px;'>No active card on file<br><a style='color:#aecd37' href='' onclick='loadHostedPage(); return false'\">Enter Credit Card Now</a></td></tr>
					</table>";
				}
				else
				{
					$cc_mask_num = empty($zPaymentMethod->CreditCardMaskNumber) ? '' : 'xxxx' . str_replace('*', '', $zPaymentMethod->CreditCardMaskNumber);
					$expir_date = (empty($zPaymentMethod->CreditCardExpirationMonth) || empty($zPaymentMethod->CreditCardExpirationYear)) ? '' : $zPaymentMethod->CreditCardExpirationMonth .'/'. $zPaymentMethod->CreditCardExpirationYear;
					$row_color = ($zPaymentMethod->PaymentMethodStatus == 'Active') ? '#99ff99' : '#f4f4f4';
					$pmCreatedDate = empty($zPaymentMethod->CreatedDate) ? '' : date('n/j/Y', strtotime($zPaymentMethod->CreatedDate));
					$pmLastTxnDate = empty($zPaymentMethod->LastTransactionDateTime) ? '' : date('n/j/Y', strtotime($zPaymentMethod->LastTransactionDateTime));

					echo "<table>
					<tr><td align='center'><table cellspacing=0 cellpadding=4>
					<tr>
					<td class='table_header'>Card Holder Name</td><td></td>
					<td class='table_header'>Card Last 4</td><td></td>
					<td class='table_header'>Type</td><td></td>
					<td class='table_header'>Exp</td><td></td>
					<td class='table_header'>Status</td><td></td>
					<td class='table_header'>Start Date</td><td></td>
					<td class='table_header'>Last Pay Date</td><td></td>
					<td></td>
					</tr>
					<tr bgcolor='{$row_color}'>
					<td style='border-bottom:solid 1px #ffffff'>{$zPaymentMethod->CreditCardHolderName}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
					<td style='border-bottom:solid 1px #ffffff'>{$cc_mask_num}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
					<td style='border-bottom:solid 1px #ffffff'>{$zPaymentMethod->CreditCardType}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
					<td style='border-bottom:solid 1px #ffffff'>{$expir_date}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
					<td style='border-bottom:solid 1px #ffffff'>{$zPaymentMethod->PaymentMethodStatus}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
					<td style='border-bottom:solid 1px #ffffff'>{$pmCreatedDate}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
					<td style='border-bottom:solid 1px #ffffff'>{$pmLastTxnDate}</td><td style='border-bottom:solid 1px #ffffff'>&nbsp;</td>
					<td style='background-color:#ffffff; border-bottom:solid 1px #ffffff'><a style='color:#aecd37' href='' onclick='loadHostedPage(); return false'\">Change Card</a></td>
					</tr>
					</table>";

					if ( $show_5min_message )
					{
						echo "<p style='color:red'><strong>Note:</strong> After updating your credit card information, it may take up to 5 minutes to take effect.</p>";
					}
				}
				echo "</td></tr></table>";

				// draw the payment method collection form
				$zInt->echoHpmHtmlDiv();
				$zInt->echoHpmJsInc();
				echo '<script>
						function callback(response) {
							if(response.success) {
								var redirectUrl = window.location.href+"&refid="+response.refId;
								window.location.replace(redirectUrl);
							} else {
								alert("errorcode="+response.errorCode + ", errorMessage="+response.errorMessage);
							}
						}
				';
				$signup_read['page_name'] = 'cp_billing';
				$signup_read['style']  = 'overlay';
				$signup_read['amount'] = $collect_amount;
				$signup_read['http_host'] = $_SERVER['HTTP_HOST'];  // LP-282 -- Added support for making HPMs work from various subdomain.domain combos
				$zInt->echoHpmJs($signup_read);
				echo '</script>';

				// we're not asking them for $$$
				// draw the upgrade button for silver and gold accounts
				// draw the device limit upgrade for pro accounts
				// check for referal reset rows in the config table
				if (!$ask_for_hosting && !$ask_for_payment && !$ask_for_ipad_upgrade && !$doing_ipad_upgrade && !isset($_POST['upgrade_from']) && isset($p_dataname) && $p_dataname!='') {

					// check for referal reset rows
					$a_updated = redeem_referral_code_reset_arb($p_dataname, date("Y-m-d"));
					if (count($a_updated) > 0) {
						echo "<script type='text/javascript'>window.location='?mode=billing';</script>";
					}

					// draw the upgrade button -- 2016-08-27: Disabled for now; will revisit post-Zuora go live
					///echo draw_upgrade_button($p_dataname, $o_package_container);

					// draw the device limit option
					$package_name = $o_package_container->get_plain_name_by_attribute('level', $signup_package);
					if($package_name == 'Platinum3' || $package_name == 'Lavu' || $package_name == 'LavuEnterprise') {
						$package_info = $o_device_limits->getDeviceLimitsByDataname(admin_info('dataname'));
						echo_interface_manage_device_limit((int)$signup_package, $i_old_device_limit, $i_base_ipad_limit, 10, $f_predicted_hosting, $f_cost_per_ipad);
					}
				}
			}
		}

		echo "<hr>";
	}

	$allow_cc_change = false;

	if($show_form)
	{
		if (($p_area=="billing_info" || $p_area=="customer_billing_info")) {

			require_once(dirname(__FILE__)."/../../../sa_cp/billing/billing_info.php");
		}
		
		//----------------------- Begin area to check if we need to reload the page --------------------------//
		// We need to reload the page if the total due is different than when the page load started
		// This prevents the collection form from asking them for the wrong amount - CF 9/19/14
		if(isset($due_total) && isset($p_companyid))
		{
			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$p_companyid);
			if(mysqli_num_rows($status_query))
			{
				$due_info_now = $status_read['due_info'];
				$due_total_now = get_due_prop("total",$due_info_now);
				
				$reloaded = 0;
				if(isset($_GET['reloaded'])) $reloaded = $_GET['reloaded'] * 1;
				$reloaded++;
				
				if($reloaded < 4)
				{
					if($due_total * 1 != $due_total_now * 1)
					{
						$output_str = ob_get_contents();
						ob_end_clean();
						$fwd_to_uri = $_SERVER['REQUEST_URI'];
						if(strpos($fwd_to_uri,"?")) $fwd_to_uri .= "&"; else $fwd_to_uri .= "?";
						$fwd_to_uri .= "reloaded=$reloaded";
						$fwd_to_uri = str_replace("reloaded=".$_GET['reloaded']."&","",$fwd_to_uri);
						echo "<script type='text/javascript'>";
						echo "window.location.replace('$fwd_to_uri'); ";
						echo "</script>";
						exit();
					}
					//echo "Total1: $due_total<br>Total2: $due_total_now<br>";
				}
			}
		}
		//----------------------- End area to check if we need to reload the page --------------------------//

		echo "<br><br>";
		if($p_area=="customer_billing_info") {
			// echo "<p style='width:60%' id='cancelBillingDisclaimer'>If you decide that you no longer wish to use your Lavu Account, it is your responsibility to cancel the account.  <strong>For more information click below</strong></p>";
			// echo "<input type='button' value='View Information regarding Account Cancellation' onclick='window.location = \"".$b_baseurl."&cancel_step=entry\"'>";
		}
	}
}
$output_str = ob_get_contents();
$output_str = str_replace("--Account Past Due--",$account_past_due_msg,$output_str);
ob_end_clean();
echo $output_str;

?>
