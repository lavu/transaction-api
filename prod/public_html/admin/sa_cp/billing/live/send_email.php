<?php
	// The email body should be plaintext with the images embedded as <--image-->path/to/image/on/server.png<--image style-->style="image style css"<--/image-->
	// eg image style: style="position: absolute; top: 20px; left: 20px; width: 97px; height: 105px;"
	function send_email_with_images($email_to, $email_subject, $email_body, $email_from="noreply@poslavu.com", $email_bcc="") {
		date_default_timezone_set("America/New_York");
		// Create a boundary string.  It needs to be unique
		$sep = sha1(date('r', time()));

		$boundary = "lavu-related-boundary";

		$email_headers = "From: $email_from\r\n";
		if ($email_bcc != "")
			$email_headers .= "Bcc: $email_bcc\r\n";
		// Add in our content boundary, and mime type specification:
		$email_headers .= "Content-Type: multipart/related;\r\n boundary=\"$boundary\"\r\n";

		// find the images and rebuild the body
		$original_email_body = $email_body;
		$a_images = array();
		$i_image_count = 0;
		$a_email_body_parts = explode('<--image-->', $email_body);
		$a_email_body_subparts = array();
		foreach($a_email_body_parts as $k=>$s_body_part) {
			if (strpos($s_body_part, '<--/image-->') === FALSE) {
		 		$a_email_body_subparts[] = $s_body_part;
		 		continue;
		 	}
			$i_image_count++;
			// path/to/image/on/server.png<--image style-->style="image style css"<--/image-->
			$a_image_parts = explode('<--/image-->', $s_body_part);
			// path/to/image/on/server.png<--image style-->style="image style css"
			$s_image_part = $a_image_parts[0];
			$a_email_body_subparts[] = $a_image_parts[1];
			// create the image
			$a_image_subparts = explode('<--image style-->', $s_image_part);
			$s_image_path = $a_image_subparts[0];
			$s_image_style = $a_image_subparts[1];
			$s_image_contents = chunk_split(base64_encode(file_get_contents($s_image_path)));
			$s_image_name = "image".$i_image_count;
			$a_images[] = array('path'=>$s_image_path, 'style'=>$s_image_style, 'contents'=>$s_image_contents, 'name'=>$s_image_name);
		}

		// create the email body
		$email_body = 'EOBODY
--'.$boundary.'
Content-Type: text/html; charset=ISO-8859-1
Content-Transfer-Encoding: 7bit

<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>';
		foreach($a_email_body_subparts as $index=>$s_body_part) {
			$email_body .= $s_body_part;
			if (count($a_images) <= $index)
				continue;
			$a_image = $a_images[$index];
			$email_body .= '<img src="cid:'.$a_image['name'].'"'.$a_image['style'].'></img>';
		}
		$email_body .= '
	</body>
</html>

';

		// create the images content
		foreach($a_images as $a_image) {
			$email_body .= '--'.$boundary.'
Content-Type: image/png
	 name="'.$a_image['filename'].'"
Content-Transfer-Encoding: base64
Content-ID: <'.$a_image['name'].'>
Content-Disposition: inline;
	 filename="'.$a_image['filename'].'"

'.$a_image['contents'].'
';
		}

		// end the email
		$email_body .= '--'.$boundary.'--
EOBODY';
		 mail($email_to, $email_subject, $email_body, $email_headers);
	}
?>