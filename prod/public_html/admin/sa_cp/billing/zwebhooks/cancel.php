<?php

require_once(dirname(__FILE__).'/../../../../inc/billing/zuora/ZuoraWebhookEventCapturer.php');

// Get the action from the extensionless filename
$action = basename(__FILE__, '.php');

// Get the POST'd XML payload to turn it into an array
$xml  = trim(file_get_contents('php://input'));

// Parse XML into SimpelXMLElement object so it can then be turned into an array
$xmlObj = simplexml_load_string($xml);
$data = array();
foreach ( $xmlObj->parameter as $parameter)
{
	$val = (string) $parameter;
	$key = (string) $parameter['name'];
	$data[$key] = $val;
}

// Use array to process payload data
$webhook = new ZuoraWebhookEventCapturer($action, $data);

exit;