	function make_billing_change(belement,bvalue_current,btitle,burl,target,billid,paymentid,bvalue_set)
	{
		var elmnt = document.getElementById("billing_table");
		elmnt.scrollIntoView();

		if (typeof bvalue_set == 'undefined')
			bvalue_set = belement.value;
		var bstr = "";
		if (btitle=="Add Custom Invoice" || btitle=="Add Custom Payment") bvalue_set = "$" + parseFloat(bvalue_set).toFixed(2);
		if (btitle.substring(0, 21)=="Set Custom iPad Limit" || btitle.substring(0, 28)=="Set Custom iPhone/iPod Limit") bvalue_set = "";
		if (btitle.substring(0, 19)=="Set Force Allow Tab") {
			bvalue_set = "";
			if (bvalue_current=="0") belement.checked = true;
			else belement.checked = false;
		}

		belement.value = bvalue_current;

		bstr += "<form name='bchange_form' method='post' action=\"" + burl + "\">";
		bstr += "<span style='font-family:Verdana,Arial; font-size:12px'>";
		bstr += "<b>" + btitle + "</b>";
		if (bvalue_set != "" && (typeof bvalue_set != 'undefined')) bstr += "<br>New Value: " + bvalue_set;
		bstr += "<br>at " + show_datetime + " by " + current_loggedin_fullname;
		bstr += "<br><br>";
		bstr += "<input type='hidden' name='bc_action' value='" + btitle + "'>";
		bstr += "<input type='hidden' name='bc_target' value='" + target + "'>";
		bstr += "<input type='hidden' name='bc_billid' value='" + billid +"'>";
		bstr += "<input type='hidden' name='bc_paymentid' value='" + paymentid + "'>";
		bstr += "<textarea rows='7' cols='36' id='bc_details' name='bc_details'></textarea>";
		bstr += "<br><br>";
		bstr += "<input type='button' value='Make Change' onclick='prevent_further_changes(); document.bchange_form.submit();'>";
		bstr += "<br><br>";
		bstr += "</span>";
		bstr += "</form>";

		show_info2(bstr);
		document.getElementById('bc_details').focus();
		document.getElementById('bc_details').value = "";
		//alert(btitle);
		//window.location = burl;

		return false;
	}

	function prevent_further_changes() {
		w = $("body").width();
		h = $("body").height();

		while ($("#dontdoanything").length > 0)
			$("#dontdoanything").remove()

		$($("div")[0]).append("<div id='dontdoanything' style='width:"+(w+200)+"px; height:"+(h+200)+"px; position:fixed; left:-100px; top:-100px; background-color:rgba(0,0,0,0.4); z-index:3000'></div>");
	}

	/*function update_hosting(cmpnyid, dtnm, crrnt_subid, crrnt_package, is_upgrade) {

		var str = "";
		var submit_title = "Submit New Credit Card Info";

		str += "<form name='update_hosting_form' method='post' action=\"\">";
		str += "<table>";
		if (is_upgrade) {
			str += "<tr><td colspan='2' align='center' style='padding:5px 1px 9px 1px;'>";
			if (crrnt_package==0) str += "Please select your package and enter your credit<br>card info to create your billing profile:";
			else str += "Please enter your credit card info to upgrade your package:";
			str += "</td></tr>";
			if (crrnt_package==0 || crrnt_package==1) {
				str += "<tr><td align='right'>Package:</td><td align='left'><select name='package' onchange='adjustHostingSubmit(this, " + crrnt_package + ");'>";
				if (crrnt_package==0) {
					str += "<option value='1'>Silver</option>";
					submit_title = "Create Silver Billing Profile";
				} else submit_title = "Upgrade to Gold";
				str += "<option value='2'>Gold</option><option value='3'>Platinum</option>";
				str += "</select></td></tr>";
			} else if (crrnt_package==2) {
				str += "<input type='hidden' name='package' value='3'>";
				submit_title = "Upgrade to Platinum";
			}
		} else {
			str += "<tr><td colspan='2' align='center' style='padding:5px 1px 9px 1px;'>Please enter your new credit card info below:</td></tr>";
			str += "<input type='hidden' name='package' value='" + crrnt_package + "'>";
		}
		str += "<tr><td align='right'>First Name:</td><td align='left'><input type='text' id='f_name' name='f_name' size='15'></td><tr>";
		str += "<tr><td align='right'>Last Name:</td><td align='left'><input type='text' name='l_name' size='15'></td><tr>";
		str += "<tr><td align='right'>Card Number:</td><td align='left'><input type='text' name='card_number' size='24'></td><tr>";
		str += "<tr><td align='right'>Expiration Date:</td><td align='left'><select name='exp_mo'>";
		str += "<option value=''></option><option value='01'>1 - Jan</option><option value='02'>2 - Feb</option><option value='03'>3 - Mar</option>";
		str += "<option value='04'>4 - Apr</option><option value='05'>5 - May</option><option value='06'>6 - Jun</option>";
		str += "<option value='07'>7 - Jul</option><option value='08'>8 - Aug</option><option value='09'>9 - Sep</option>";
		str += "<option value='10'>10 - Oct</option><option value='11'>11 - Nov</option><option value='12'>12 - Dec</option>";
		str += "</select><select name='exp_yr'>";
		str += "<option value=''></option><option value='2012'>2012</option><option value='2013'>2013</option><option value='2014'>2014</option>";
		str += "<option value='2015'>2015</option><option value='2016'>2016</option><option value='2017'>2017</option><option value='2018'>2018</option>";
		str += "<option value='2019'>2019</option><option value='2020'>2020</option><option value='2021'>2021</option><option value='2022'>2022</optoin>";
		str += "</select></td></tr>";
		str += "<tr><td align='right'>Security Code (CCV):</td><td align='left'><input type='text' name='ccv' size='10'></td><tr>";
		str += "<tr><td colspan='2' style='padding:8px 1px 8px 1px;'><input type='checkbox' name='agree_tos' value='1' id='agree_tos' />By checking here you agree to our <a href='http://www.poslavu.com/en/tos' target='_blank' style='color:#3333CC; font-size:12px;'>Terms of Service</a></td></tr>";
		str += "<tr><td colspan='2' align='center'><input id='hosting_submit' type='button' value='" + submit_title + "' onclick='validateUpdateHosting();'></td></tr>";
		str += "</table>";
		str += "<br><br>";
		str += "</span>";
		str += "<input type='hidden' name='update_hosting' value='1'>";
		str += "<input type='hidden' name='companyid' value='" + cmpnyid + "'>";
		str += "<input type='hidden' name='dataname' value='" + dtnm + "'>";
		str += "<input type='hidden' name='current_subid' value='" + crrnt_subid + "'>";
		str += "<input type='hidden' name='current_package' value='" + crrnt_package + "'>";
		str += "</form>";

		show_info(str);
		document.getElementById('f_name').focus();
	}

	function adjustHostingSubmit(element, crrnt_package) {
		submit_btn = document.getElementById("hosting_submit");
		if (element.value==1) submit_btn.value = "Create Silver Billing Profile";
		if (element.value==2) {
			if (crrnt_package>0) submit_btn.value = "Upgrade to Gold";
			else submit_btn.value = "Create Gold Billing Profile";
		} else if (element.value==3) {
			if (crrnt_package>0) submit_btn.value = "Upgrade to Platinum";
			else submit_btn.value = "Create Platinum Billing Profile";
		}
	}

	function isEmpty(data) {
		for (var i=0; i<data.length; i++) {
			if (data.substring(i,i+1) != " ") return false;
		}
		return true;
	}

	function isValidCardNumber(cn) {
		return true;
	}

	function isValidCCV(ccv) {
		return true;
	}

	function validateUpdateHosting() {
		if (isEmpty(update_hosting_form.f_name.value)) {
			alert("Please enter your first name...");
			update_hosting_form.f_name.focus();
		} else if (isEmpty(update_hosting_form.l_name.value)) {
			alert("Please enter your last name...");
			update_hosting_form.l_name.focus();
		} else if (isEmpty(update_hosting_form.card_number.value)) {
			alert("Please enter your card number...");
			update_hosting_form.card_number.focus();
		} else if (!isValidCardNumber(update_hosting_form.card_number.value)) {
			alert("Please enter a valid card number...");
			update_hosting_form.card_number.focus();
		} else if (isEmpty(update_hosting_form.exp_mo.value)) {
			alert("Please select your card's expiration month...");
			update_hosting_form.exp_mo.focus();
		} else if (isEmpty(update_hosting_form.exp_yr.value)) {
			alert("Please select your card's expiration year...");
			update_hosting_form.exp_yr.focus();
		} else if (isEmpty(update_hosting_form.ccv.value)) {
			alert("Please enter your card's security code...");
			update_hosting_form.ccv.focus();
		} else if (!isValidCCV(update_hosting_form.ccv.value)) {
			alert("Please enter a valid security coce...");
			update_hosting_form.ccv.focus();
		} else if (!update_hosting_form.agree_tos.checked) {
			alert("Please check that you agree to our Terms of Service...");
			update_hosting_form.agree_tos.focus();
		} else {
			document.update_hosting_form.submit();
		}
	}*/
