if (typeof(Edit) == "undefined")
	Edit = {};
if (typeof(Edit.MonthlyParts) == "undefined")
	Edit.MonthlyParts = {};

Edit.MonthlyParts.editURL = '/sa_cp/billing/procareas/editInvoiceParts.php?monthly=1';
Edit.MonthlyParts.partsToString = function() {
	var dbValues = this.dbValues;
	var parts = [];
	var retval = "";
	var header = "";
	var past = "";
	var num_past_mps = 0;

	if (typeof(this.today) == 'undefined')
		this.today = '';
	if (typeof(this.dataname) == 'undefined')
		this.dataname = '';

	// get the monthly parts
	var part = null;
	$.each(dbValues, function(k, v) {
		part = new Edit.MonthlyParts.MonthlyPart(v, false);
		parts.push(part);
	});
	part = new Edit.MonthlyParts.MonthlyPart({ dataname:this.dataname, mp_start_day:this.today, mp_end_day:this.today, mp_priority:'0.0', distro_cmsn:'0.0' }, true);
	parts.push(part);

	// prepare the header
	header = "<table cellspacing='0'><tr>";
	$.each(parts[0].draw_values, function(k,draw_value) {
		if (draw_value.draw) {
			header += "<td style='padding:5px;'>"+draw_value.name+"</td>";
		}
	});
	header += "<td>Create/Cancel</td></tr>";

	// draw the past monthly parts
	past = "<a onclick='$(this).children().show(); $(this).children(\"[name=show_link]\").remove();'><font style='color:blue; text-decoration:underline; cursor:pointer;' name='show_link'>Show Past Monthly Parts (___num_past_mps___)</font><div style='display:none;'>"+header;
	$.each(parts, function(k, part) {
		if (part.isExpired()) {
			past += part.toStringToEdit();
			num_past_mps++;
		}
	});
	past += "</table></div></a><br /><br />";
	past = past.replace("___num_past_mps___", num_past_mps);
	if (num_past_mps > 0)
		retval += past;

	// draw the current/future monthly parts
	retval += header;
	$.each(parts, function(k, part) {
		if (!part.isExpired())
			retval += part.toStringToEdit();
	});
	retval += "</table>";

	return {string:retval, parts:parts};
};
Edit.MonthlyParts.possibleMonthlyDescriptions = null;
Edit.MonthlyParts.getPossibleMonthlyDescriptions = function() {
	if (this.possibleMonthlyDescriptions !== null)
		return this.possibleMonthlyDescriptions;
	possibleMonthlyDescriptions = {};
	$.ajax({
		url: Edit.MonthlyParts.editURL,
		async: false,
		cache: false,
		method: "POST",
		data: { action:'getPossibleMonthlyDescriptions' },
		success: function(m) {
			possibleMonthlyDescriptions = JSON.parse(m);
		}
	});
	this.possibleMonthlyDescriptions = possibleMonthlyDescriptions.success_msg;
	return this.possibleMonthlyDescriptions;
};
Edit.MonthlyParts.MonthlyPart = function(part, create_new) {
	this.db_values = part;
	this.create_new = create_new;
	this.draw_values = [
		{ name:'id',						disabled:true,	draw:true,	type:'text',		style: 'font-weight:bold;',	possible_values:[] },
		{ name:'dataname',					disabled:true,	draw:false,	type:'textbox',		style: '',					possible_values:[] },
		{ name:'description',				disabled:false,	draw:true,	type:'select',		style: '',					possible_values:Edit.MonthlyParts.getPossibleMonthlyDescriptions() },
		{ name:'amount',					disabled:false,	draw:true,	type:'number',		style: 'width:58px;',		possible_values:[] },
		{ name:'pro_rate',					disabled:false,	draw:true,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'waived',					disabled:false,	draw:true,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'promo',						disabled:false,	draw:true,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'bundled',					disabled:false,	draw:true,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'distro_points',				disabled:false,	draw:true,	type:'number',		style: 'width:58px;',		possible_values:[] },
		{ name:'distro_cmsn',				disabled:false,	draw:true,	type:'number',		style: 'width:58px;',		possible_values:[] },
		{ name:'mp_priority',				disabled:false,	draw:true,	type:'textbox',		style: '',					possible_values:[],	attributes:{size:7} },
		{ name:'mp_start_day',				disabled:false,	draw:true,	type:'textbox',		style: '',					possible_values:[],	attributes:{size:11} },
		{ name:'mp_end_day',				disabled:false,	draw:true,	type:'textbox',		style: '',					possible_values:[],	attributes:{size:11} },
		{ name:'mp_json_history',			disabled:false,	draw:true,	type:'history',		style: '',					possible_values:[] },
	];
};

Edit.MonthlyParts.MonthlyPart.prototype.constructor = Edit.MonthlyParts.MonthlyPart;
Edit.MonthlyParts.MonthlyPart.prototype = {};
Edit.MonthlyParts.MonthlyPart.prototype.toStringToEdit = function() {

	var retval = "";
	var i = 0;
	var tr_extra_style = "";
	var td_extra_style = "border:0px solid transparent; border-top:1px solid #aaa; padding:5px 10px;";
	retval += "<tr style='"+tr_extra_style+"'>";

	// add some convenience values to the the draw values
	for (i = 0; i < this.draw_values.length; i++) {
		this.draw_values[i].value = this.db_values[this.draw_values[i].name];
		this.draw_values[i].identifier_class = this.draw_values[0].value+'_'+this.draw_values[i].name;
		this.draw_values[i].dataname = this.draw_values[3].value;
		this.draw_values[i].part_id = this.draw_values[0].value;
		if (this.draw_values[i].name == "json_history")
			this.draw_values[i].value = JSON.stringify(this.draw_values[i].value);
	}

	// determine if canceled
	var canceled = false;
	for (var timestamp in this.db_values.mp_json_history) {
		var description = this.db_values.mp_json_history[timestamp];
		if (description.action == 'canceled') {
			canceled = true;
			break;
		}
	}
	this.canceled = canceled;

	// draw each draw value
	var create_new = this.create_new;
	$.each(this.draw_values, function(k,v) {
		var disabled = v.disabled;
		if (!create_new)
			v.disabled = true;
		if (v.draw) {
			i++;
			if(v.type == 'history' && v.value)
				v.value = '"'+v.value.replace(/\\/g, '\\\\').replace(/"/g, '\\"')+'"';
			retval += "<td style='"+td_extra_style+"'>"+Lavu.Gui.toString(v)+"</td>";
		}
		v.disabled = disabled;
	});

	var buttonval = "";
	var onclick = function(){};
	var me = this;
	var reference = me.draw_values[4];
	if (this.create_new) {

		// the function to call upon clicking "Create" (as in create new monthly part)
		onclick = function(event) {
			var element = Lavu.Gui.getElement(reference.identifier_class);
			var jtr = Lavu.Gui.getParentDOM($(element), "tr");
			var bc_action = 'Create New Monthly Invoice Part';
			var burl = "?dn="+Edit.MonthlyParts.dataname+"&edit_monthly_parts=1&action=create_part";
			var input_vals = Lavu.Gui.getInputsNamesValues(jtr);
			$.each(input_vals, function(k,v) {
				burl += "&mp_"+v.name+"="+v.value;
			});
			make_billing_change(element, reference.value, bc_action, burl, "", reference.part_id, "");
		};
		buttonval = "Create";
	} else {

		// the function to call upon clicking "Cancel" (as in cancel monthly part)
		onclick = function(event) {
			var element = Lavu.Gui.getElement(reference.identifier_class);
			var bc_action = 'Cancel Monthly Invoice Part '+reference.part_id;
			var burl = "?dn="+Edit.MonthlyParts.dataname+"&edit_monthly_parts=1&monthly=1&action=cancel_part&part_id="+reference.part_id;
			make_billing_change(element, reference.value, bc_action, burl, "", reference.part_id, "");
		};
		buttonval = "Cancel";
	}

	this.onclick = onclick;

	// draw the cancel button
	var button = "";
	if (this.canceled) {
		button = "<font style='color:lightgray;'>Canceled</font>";
	} else if (this.isExpired()) {
		button = "<font style='color:lightgray;'>Expired</font>";
	} else {
		button = "<input class='monthly_parts_button' type='button' value='"+buttonval+"' />";
	}
	retval += "<td style='"+td_extra_style+"'>"+button+"</td></tr>";

	return retval;
};
Edit.MonthlyParts.MonthlyPart.prototype.isExpired = function() {

	// get the parts of the expiration date
	var end_day = this.db_values.mp_end_day;
	var end_day_parts = end_day.split("-");
	$.each(end_day_parts, function(k,v){
		end_day_parts[k] = parseInt(v,10);
	});

	// subtract one from the month for javascript month format
	end_day_parts[1]--;

	// get date objects
	var expiration_date = new Date(end_day_parts[0], end_day_parts[1], end_day_parts[2], 0, 0, 0, 0);
	var today = new Date();
	today = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0, 0);

	// check if the expiration date has passed
	if (expiration_date.getTime() < today.getTime())
		return true;
	return false;
};

if (!window.getDataname) {
	window.getDataname = function() {
		if (window.Account && window.Account.Dataname)
			return window.Account.Dataname;
		return "";
	};
}