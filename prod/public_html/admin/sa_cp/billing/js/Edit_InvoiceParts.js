if (typeof(Edit) == "undefined")
	Edit = {};
if (typeof(Edit.InvoiceParts) == "undefined")
	Edit.InvoiceParts = {};

Edit.InvoiceParts.editURL = '/sa_cp/billing/procareas/editInvoiceParts.php';
Edit.InvoiceParts.openLink = function(append_element, invoice_id) {
	var w = 900;
	var win = { window: window, width: $(window).width(), height: $(window).height() };
	var left = win.width/2-w/2;
	var top = 40;
	var h = 430;

	// draw the overlay while its loading
	while($("#editInvoiceParts").length > 0)
		$("#editInvoiceParts").remove();
	$(append_element).append("<div id='editInvoiceParts' style='width:"+win.width+"px; height:"+win.height+"px; left:0; top:0; background-color:rgba(0,0,0,0.4); position:fixed;'></div>");

	setTimeout(function() {
		var parts = Edit.InvoiceParts.ajaxGetParts(invoice_id);

		// draw the actual interface
		while($("#editInvoiceParts").length > 0)
			$("#editInvoiceParts").remove();
		$(append_element).append(window.EditInvoicePartsAppendText(win, left, top, w, h));

		// scroll inside of manage
		var mewindow = window;
		var parentWindow = window.parent.window;
		if (parentWindow !== mewindow) {
			var scrollfunc = function() {
				var jeditInvoiceParts = mewindow.$("#editInvoiceParts");
				if (jeditInvoiceParts.length <= 0)
					return;
				var parentScroll = Math.max(0, parseInt(parentWindow.$(parentWindow).scrollTop(),10)-158);
				jeditInvoiceParts.find('.container').css("top", ((top+mewindow.$(mewindow).scrollTop()+parentScroll)+"px"));
			};
			parentWindow.$(parentWindow).scroll(scrollfunc);
			setTimeout(scrollfunc, 400);
			scrollfunc();
		}

		// give the browser time to draw the popup and
		// load the invoice parts
		setTimeout(function() {
			Edit.InvoiceParts.displayEditParts(parts.error_msg, parts.parts);
		}, 10);
	}, 10);

	// Close lightbox on Esc key
	$(document).keyup(function(e) {
		if (e.keyCode == 27) $('#editInvoicePartsCloseButton').click();
	});
};
Edit.InvoiceParts.closeLink = function() {
	while($("#editInvoiceParts").length > 0)
		$("#editInvoiceParts").remove();
};
Edit.InvoiceParts.ajaxGetParts = function(invoice_id) {
	var retval;
	$.ajax({
		url: Edit.InvoiceParts.editURL,
		async: false,
		cache: false,
		data: { action: 'getInvoiceParts', invoice_id: invoice_id },
		method: "POST",
		success: function(m) {
			var parts = JSON.parse(m);
			if (!parts.success_msg)
				parts.success_msg = [];
			if (!parts.error_msg)
				parts.error_msg = '';
			retval = {
				'parts': parts.success_msg,
				'error_msg': parts.error_msg
			};
		}
	});
	return retval;
};
Edit.InvoiceParts.displayEditParts = function(error_msg, parts) {
	var jcontent = $("#editInvoiceParts").find(".content");

	// check for errors
	if (error_msg !== '') {
		jcontent.html("");
		jcontent.append("<div style='font-weight:bold;'>"+error_msg+"</div>");
		return;
	}

	// draw the damned parts
	var inputPartsRows = [];
	var iparts = [];
	for (var i = 0; i < parts.length; i++) {
		ipart = new this.InvoicePart(parts[i]);
		iparts.push(ipart);
		inputPartsRows.push(ipart.toStringToEdit());
	}

	// draw the option to create a new part
	var create_new_part = "<tr><td colspan='8000' style='border:0px solid transparent;'><hr></td></tr><tr>";
	var burl = "?dn="+parts[0].db_values.dataname+"&edit_invoice_parts=1&action=create_new&invoice_id="+parts[0].db_values.invoice_id;
	var make_billing_change = "make_billing_change(this, 0, \"Create A New Invoice Part\", \""+burl+"\", \"\", "+parts[0].db_values.invoice_id+", \"\");";
	create_new_part += "<td colspan='8000' style='text-align:center; border:0px solid transparent;'><a href='#' onclick='"+make_billing_change+"' style='color:blue;'>Create New</a></td></tr>";

	jcontent.html("");
	jcontent.append("<table>"+inputPartsRows.join("")+create_new_part+"</table>");
	$.each(iparts, function(k,ipart) {
		$.each(ipart.draw_values, function(k2, draw_value) {
			var jelement = $(Lavu.Gui.getElement(draw_value.identifier_class));
			var jinput = Lavu.Gui.getInput(jelement);
			if (draw_value.onkeypress)
				jelement.keypress(draw_value.onkeypress);
			if (draw_value.onchange)
				jelement.change(draw_value.onchange);
		});
	});
};
Edit.InvoiceParts.createQuoteLineItems = function(dataname, invoiceid, salesforce_quote_id, due_date) {
	if (salesforce_quote_id == '' || salesforce_quote_id == null) return;
	$("#sfqi_loading").css("visibility", "visible");
	$.ajax({
		url: Edit.InvoiceParts.editURL,
		async: true,
		cache: false,
		method: "POST",
		data: {
			'action':'createQuoteLineItems',
			'dataname':dataname,
			'invoice_id':invoiceid,
			'salesforce_quote_id':salesforce_quote_id,
			'package':$("#change_package").val(),
			'due_date':due_date
		},
		success: function(respTxt) {
			// Display results
			$("#quoteInvoicePartsResult").html(respTxt);
			window.location.reload();
		},
		error: function(respTxt) {
			// Display error
			$("#quoteInvoicePartsResult").html('<span style="color:red">ERROR: '+ respTxt +'</span>');
		}
	});
};
Edit.InvoiceParts.openLinkForQuote = function(dataname, invoiceid, due_date) {
	var lineItemsFromQuoteHtml = '';
	lineItemsFromQuoteHtml += "<span style='font-family:Verdana,Arial; font-size:12px'>";
	lineItemsFromQuoteHtml += "<b>Create Invoice Parts<br>From Salesforce Quote</b>";
	lineItemsFromQuoteHtml += "<br>Invoice ID " + invoiceid;
	lineItemsFromQuoteHtml += "<br><br>";
	lineItemsFromQuoteHtml += "<input type='text' name='sfqi' id='sfqi' size='20' placeholder='Salesforce Quote ID' style='text-align:center'>";
	lineItemsFromQuoteHtml += "<br><br>";
	lineItemsFromQuoteHtml += "<input type='button' value='Create invoice parts' onclick='Edit.InvoiceParts.createQuoteLineItems(\""+dataname+"\", \""+invoiceid+"\", $(\"#sfqi\").val(), \""+due_date+"\")'>";
	lineItemsFromQuoteHtml += "<br><br><img id='sfqi_loading' src='/distro/beta/images/loading_squares.gif' style='visibility:hidden'><br>";
	lineItemsFromQuoteHtml += "<span id='quoteInvoicePartsResult'></span>";
	lineItemsFromQuoteHtml += "</span>";
	show_info2(lineItemsFromQuoteHtml);  // from make_billing_change.js
};
Edit.InvoiceParts.possibleInvoiceTypes = null;
Edit.InvoiceParts.getPossibleInvoiceTypes = function() {
	if (this.possibleInvoiceTypes !== null)
		return this.possibleInvoiceTypes;
	possibleInvoiceTypes = {};
	$.ajax({
		url: Edit.InvoiceParts.editURL,
		async: false,
		cache: false,
		method: "POST",
		data: { action:'getPossibleInvoiceTypes' },
		success: function(m) {
			possibleInvoiceTypes = JSON.parse(m);
		}
	});
	this.possibleInvoiceTypes = possibleInvoiceTypes.success_msg;
	this.possibleInvoiceTypes.push({value:'',printed:''});
	return this.possibleInvoiceTypes;
};
Edit.InvoiceParts.possibleHostingDates = null;
Edit.InvoiceParts.getPossibleHostingDates = function(dataname, current_date) {
	if (this.possibleHostingDates !== null && this.possibleHostingDates.current_date == current_date)
		return this.possibleHostingDates.values;
	this.possibleHostingDates = {};
	possibleHostingDates = {};
	$.ajax({
		url: Edit.InvoiceParts.editURL,
		async: false,
		cache: false,
		method: "POST",
		data: { action:'getPossibleHostingDates', dataname:dataname, current_date:current_date },
		success: function(m) {
			possibleHostingDates = JSON.parse(m);
		}
	});
	this.possibleHostingDates.values = possibleHostingDates.success_msg;
	this.possibleHostingDates.values.push({value:'',printed:''});
	this.possibleHostingDates.current_date = current_date;
	return this.possibleHostingDates.values;
};
Edit.InvoiceParts.possibleMonthlyParts = null;
Edit.InvoiceParts.getPossibleMonthlyParts = function(dataname, current_date) {
	if (this.possibleMonthlyParts !== null && this.possibleMonthlyParts.current_date == current_date)
		return this.possibleMonthlyParts.values;
	this.possibleMonthlyParts = {};
	possibleMonthlyParts = {};
	$.ajax({
		url: Edit.InvoiceParts.editURL,
		async: false,
		cache: false,
		method: "POST",
		data: { action:'getPossibleMonthlyParts', dataname:dataname, current_date:current_date },
		success: function(m) {
			possibleMonthlyParts = JSON.parse(m);
		}
	});
	this.possibleMonthlyParts.values = possibleMonthlyParts.success_msg;
	this.possibleMonthlyParts.current_date = current_date;
	return this.possibleMonthlyParts.values;
};
Edit.InvoiceParts.InvoicePart = function(part) {
	this.db_values = part.db_values;
	this.draw_values = [
		{ name:'id',						disabled:'1',	draw:true,	type:'text',		style: 'font-weight:bold',	possible_values:[] },
		{ name:'dataname',					disabled:'1',	draw:false,	type:'textbox',		style: '',					possible_values:[] },
		{ name:'monthly_parts_id',			disabled:'1',	draw:false,	type:'select',		style: '',					possible_values:Edit.InvoiceParts.getPossibleMonthlyParts(this.db_values.dataname, this.db_values.due_date) },
		{ name:'description',				disabled:'',	draw:true,	type:'textbox',		style: 'width:100%',		possible_values:[] },
		{ name:'invoice_id',				disabled:'',	draw:true,	type:'textbox',		style: '',					possible_values:[] },
		{ name:'due_date',					disabled:'',	draw:true,	type:'select',		style: '',					possible_values:Edit.InvoiceParts.getPossibleHostingDates(this.db_values.dataname, this.db_values.due_date) },
		{ name:'amount',					disabled:'',	draw:true,	type:'textbox',		style: '',					possible_values:[] },
		{ name:'waived',					disabled:'',	draw:true,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'pro_rate',					disabled:'',	draw:false,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'promo',						disabled:'',	draw:false,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'bundled',					disabled:'',	draw:false,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'standard_monthly_billing',	disabled:'1',	draw:false,	type:'checkbox',	style: '',					possible_values:[] },
		{ name:'distro_points',				disabled:'1',	draw:true,	type:'number',		style: '',					possible_values:[] },
		{ name:'distro_cmsn',				disabled:'1',	draw:true,	type:'number',		style: '',					possible_values:[] },
		{ name:'json_history',				disabled:'',	draw:true,	type:'history',		style: '',					possible_values:[] },
		{ name:'salesforce_quote_id',		disabled:'',	draw:true,	type:'textbox',		style: '',					possible_values:[] },
		{ name:'salesforce_quotelineitem_id',disabled:'',	draw:true,	type:'textbox',		style: '',					possible_values:[] },
	];
};

Edit.InvoiceParts.InvoicePart.prototype.constructor = Edit.InvoiceParts.InvoicePart;
Edit.InvoiceParts.InvoicePart.prototype = {};
Edit.InvoiceParts.InvoicePart.prototype.toStringToEdit = function() {

	var retval = "";
	var i = 0;
	var extra_style = "border:0px solid transparent;";
	retval += "<tr><td colspan='8000' style='"+extra_style+"'><hr></td></tr><tr>";

	// add some convenience values to the the draw values
	for (i = 0; i < this.draw_values.length; i++) {
		this.draw_values[i].value = this.db_values[this.draw_values[i].name];
		this.draw_values[i].identifier_class = this.draw_values[0].value+'_'+this.draw_values[i].name;
		this.draw_values[i].bill_id = this.draw_values[4].value;
		this.draw_values[i].dataname = this.draw_values[1].value;
		this.draw_values[i].part_id = this.draw_values[0].value;
		if (this.draw_values[i].name == "json_history")
			this.draw_values[i].value = JSON.stringify(this.draw_values[i].value);
	}

	// draw each draw value
	i = 0;
	$.each(this.draw_values, function(k,v) {

		if (i % 3 === 0 && i !== 0)
			retval += "</tr><tr>";

		// the function to call upon changing the input
		var onchange = function(event) {
			if (v.type == 'textbox' || v.type == 'number')
				if (event.which != 13)
					return;
			var element = Lavu.Gui.getElement(v.identifier_class);
			var value = Lavu.Gui.getValue(v.identifier_class);
			var bc_action = 'Change Invoice Part '+v.name+' From '+v.value+' To '+value+', Invoice Part ID '+v.part_id;
			var burl = "?dn="+v.dataname+"&edit_invoice_parts=1&action=change_invoice_part&part_id="+v.part_id+"&name="+v.name+"&value="+value;
			Lavu.Gui.setValue(element, v.value);
			make_billing_change(element, v.value, bc_action, burl, "", v.bill_id, "", v.value);
		};
		if (v.type == 'textbox' || v.type == 'number')
			v.onkeypress = onchange;
		else
			v.onchange = onchange;
		if (v.draw) {
			i++;
			if (v.name == "description") {
				retval += "<td style='"+extra_style+"'> "+v.name+": </td><td colspan='3' style='"+extra_style+"'>"+Lavu.Gui.toString(v)+" </td>";
				i++;
			} else {
				retval += "<td style='"+extra_style+"'> "+v.name+": </td><td style='"+extra_style+"'>"+Lavu.Gui.toString(v)+" </td>";
			}
		}
	});

	return retval;
};