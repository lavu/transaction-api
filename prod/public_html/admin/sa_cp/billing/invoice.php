<?php

require_once(dirname(__FILE__) . "/../../cp/resources/core_functions.php");

$debug = (strpos($_SERVER["SERVER_NAME"], 'localhost') !== false);


// Required parameters.

$dataname = isset($_REQUEST['dataname']) ? $_REQUEST['dataname'] : '';
$invoiceid = isset($_REQUEST['invoiceid']) ? $_REQUEST['invoiceid'] : '';

$disableDatanameCheck = ($dataname == 'view_all_invoices');
$multipleInvoicesDetected = (strpos($invoiceid, '|') !== false);

if ( empty($invoiceid) ) die(__FILE__ ." received empty invoiceid!");
if ( empty($dataname) && !$disableDatanameCheck ) die(__FILE__ ." received empty dataname!");

// Check for 
$invoiceids = array();
if ( $multipleInvoicesDetected ) {
	$invoiceids = explode('|', $invoiceid);
}
else {
	$invoiceids[] = $invoiceid;
}

$content = '';

foreach ( $invoiceids as $invoiceid ) {

	if (empty($invoiceid)) continue;

	$paymentOnlyInvoiceDetected = (strpos(strtolower($invoiceid), 'p') !== false);

	debugLog("paymentOnlyInvoiceDetected={$paymentOnlyInvoiceDetected}");

	if ( $multipleInvoicesDetected ) {
		// Need to get the dataname from another source, since it's not passed in for the multiple invoices stuff.
		if ( $paymentOnlyInvoiceDetected ) {
			$payment_response_id = str_replace('p', '', strtolower($invoiceid));
			$results = mlavu_query("select match_restaurantid from `poslavu_MAIN_db`.`payment_responses` where id = '[1]'", $payment_response_id);
			if ( $results === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());
			$row = mysqli_fetch_assoc($results);
			$restaurantid = $row['match_restaurantid'];

			$restaurants = mlavu_query("select data_name from `poslavu_MAIN_db`.`restaurants` where `id` = '[1]'", $restaurantid);
			if ( $restaurants === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());
			$restaurant = mysqli_fetch_assoc($restaurants);

			$dataname = $restaurant['data_name'];

			$payment_rows = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `id` = '[1]'", $payment_response_id);
			if ( $payment_rows === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());
		}
		else {
			$results = mlavu_query("select dataname from `poslavu_MAIN_db`.`invoices` where id = '[1]'", $invoiceid);
			if ( $results === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());
			$row = mysqli_fetch_assoc($results);

			$dataname = $row['dataname'];

			$restaurants = mlavu_query("select id from `poslavu_MAIN_db`.`restaurants` where data_name = '[1]'", $dataname);
			if ( $restaurants === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());
			$restaurant = mysqli_fetch_assoc($restaurants);
			$restaurantid = $restaurant['id'];		

			$payment_rows = mlavu_query("select * from `poslavu_MAIN_db`.`payment_responses` where `match_restaurantid` = '[1]' and `x_response_code` = '1' and (`invoices_applied_to` like '% to [2]' or `invoices_applied_to` like '% to [2]|%')", $restaurantid, $invoiceid);
			if ( $payment_rows === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());
		}
	}

	// Database ops.

	lavu_connect_dn($dataname, "poslavu_{$dataname}_db");

	$locations = lavu_query("select * from `poslavu_{$dataname}_db`.`locations` order by `id` limit 1");
	if ( $locations === false ) error_log(__FILE__ ." encountered query error: ". lavu_dberror());  // Had to keep from dying here, since we later have a fallback check to use the signups table.
	$location = mysqli_fetch_assoc($locations);

	$invoices = mlavu_query("select * from `poslavu_MAIN_db`.`invoices` where id = '[1]' and `dataname` = '[2]'", $invoiceid, $dataname);
	if ( $invoices === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());
	$dbinvoice = mysqli_fetch_assoc($invoices);

	$invoice_parts = mlavu_query("select * from `poslavu_MAIN_db`.`invoice_parts` where `invoice_id` = '[1]' and `dataname` = '[2]'", $invoiceid, $dataname);
	if ( $invoice_parts === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());

	// Make sure the specified invoice exists and for that dataname.
	if ( mysqli_num_rows($invoices) === 0 && !$paymentOnlyInvoiceDetected ) {
		error_log(__FILE__ ." couldn't find invoice ID {$invoiceid} for dataname={$dataname}");
		return;
	}

	if ( empty($location) ) {
		$locations = mlavu_query("select * from `poslavu_MAIN_db`.`signups` where `dataname` = '[1]'", $dataname);
		if ( $locations === false ) die(__FILE__ ." encountered query error: ". mlavu_dberror());
		$location = mysqli_fetch_assoc($locations);
		$location['title'] = $location['company'];  // Map the `signups` fields to the `locations` fields.
	}


	// Data population.

	$current_date = date('n/j/Y g:i:s a T');

	$account = array(
		'name'    => $location['title'],
		'address' => $location['address'],
		'city'    => $location['city'],
		'state'   => $location['state'],
		'zip'     => $location['zip'],
		'phone'   => $location['phone'],
	);

	$invoice = array(
		'id'        => $invoiceid,
		'number'    => "77-{$invoiceid}",
		'date'      => date("n/j/Y", strtotime($dbinvoice['due_date'])),
		'amountdue' => formatMoney($dbinvoice['amount']),  // Calculated later, when we have all of the totals.
		'subtotal'  => formatMoney($dbinvoice['amount']),
		'tax'       => '0.00',
		'total'     => formatMoney($dbinvoice['amount']),
		'waived'    => $dbinvoice['waived'],
	);

	// Line items.
	if ( mysqli_num_rows($invoice_parts) >= 1 ) {
		$lineitems = array();
		while ( $invoice_part = mysqli_fetch_assoc($invoice_parts) ) {
			$lineitems[ $invoice_part['id'] ] = array(
				'id'       => $invoice_part['id'],
				'date'     => formatDate($invoice_part['due_date']),
				'item'     => $invoice_part['description'],
				'tax'      => '0.00',
				'amount'   => formatMoney($invoice_part['amount']),  // TO DO : waived?
				'currency' => 'USD',
				'waived'   => $invoice_part['waived'],
			);
		}
		$invoice['lineitems'] = $lineitems;
	}
	// When no `invoice_parts` rows exist for the invoice (for pre-October 2013 invoices), just synthesize one using the parent invoice info.
	else {
		$invoice['lineitems'][0] = array(
			'id'       => '0',
			'date'     => formatDate($dbinvoice['due_date']),
			'item'     => $dbinvoice['type'],
			'tax'      => '0.00',
			'amount'   => formatMoney($dbinvoice['amount']),  // TO DO : waived?
			'currency' => 'USD',
			'waived'   => '0',
		);
	}

	// Payments
	if ( mysqli_num_rows($payment_rows) >= 1 ) {
		$payments = array();
		while ( $payment = mysqli_fetch_assoc($payment_rows) ) {
			$partial_amount = ($paymentOnlyInvoiceDetected) ? $payment['x_amount'] : 0.00;
			foreach ( explode('|', $payment['invoices_applied_to']) as $parial_payments ) {
				$partials = explode(' to ', $parial_payments);
				if ( $partials[1] == $invoiceid ) {
					$partial_amount = $partials[0];
					break;
				}

			}
			$payments[ $payment['id'] ] = array(
				'id'       => $payment['id'],
				'date'     => formatDate($payment['date']),
				'item'     => $payment['payment_type'] .' Payment with '. $payment['x_card_type'] .' '. strtolower($payment['x_account_number']),
				'tax'      => '0.00',
				'amount'   => formatMoney($partial_amount),
				'currency' => 'USD'
			);

			// Need to borrow some values to fix invoices for Payment-only invoices.
			if ( $paymentOnlyInvoiceDetected ) {
				$invoice['date'] = date("n/j/Y", strtotime($payment['date']));
				$invoice['number'] = $invoiceid;
			}

		}
		$invoice['payments'] = $payments;
	}


	// Data Formatting

	$cityStateSeparator = empty($account['city']) ? '' : ', ';
	$title = ($multipleInvoicesDetected) ? '' : 'Invoice '. $invoice['id'];
	$footerClassSelector = ($multipleInvoicesDetected) ? '2' : '';


	// Loop through invoice items.

	$calculatedSubtotal = 0.00;
	$calculatedTotal = 0.00;
	$calculatedTax = 0.00;

	$lineItemHtml = '';

	foreach ( $invoice['lineitems'] as $lineitem )
	{
		$isWaived = ( $invoice['waived'] || (isset($lineitem['waived']) && $lineitem['waived'] == '1') );
		$waivedStatus = $isWaived ? ' **Waived**' : '';
		$amount = $isWaived ? 0.00 : floatval($lineitem['amount']);
		$tax = $isWaived ? 0.00 : floatval($lineitem['tax']);
		$calculatedSubtotal += $amount;
		$calculatedTax += $tax;

		$lineItemHtml .= <<<HTML

	<tr class="plain">
		<td>{$lineitem['date']}</td>
		<td class="maincell">{$lineitem['item']}{$waivedStatus}</td>
		<td class="right">{$lineitem['amount']}</td>
		<td>{$lineitem['currency']}</td>
	</tr>

HTML;

	}


	// Payments

	$calculatedTotalPayments = 0.00;

	$paymentLineItemsHtml = '';

	foreach ( $invoice['payments'] as $payment )
	{
		$amount = empty($payment['amount']) ? 0.00 : floatval($payment['amount']);
		$tax = $isWaived ? 0.00 : floatval($payment['tax']);
		$calculatedTotalPayments += $amount;

		$paymentLineItemsHtml .= <<<HTML

	<tr class="plain">
		<td>{$payment['date']}</td>
		<td class="maincell">{$payment['item']}</td>
		<td class="right">{$payment['amount']}</td>
		<td>{$payment['currency']}</td>
	</tr>

HTML;

	}


	// Calculate payment totals.

	$total_payments = formatMoney($calculatedTotalPayments);


	// We build the Payments area this way so it only shows for people with payments.
	if ( !empty($paymentLineItemsHtml) ) {
		$paymentsHtml = <<<HTML

	<h2>Payments</h2>

	<hr>

	<!-- Line Items -->
	<div class="lineitemsContainer">

		<table class="lineitems">
		<tbody>
		<tr>
			<td>Date</td>
			<td class="maincell">Payment</td>
			<td class="right">Amount</td>
			<td></td>
		</tr>
		{$paymentLineItemsHtml}
		</tbody>
		</table>

	</div>

	<hr>

	<!-- Total Payments -->
	<div class="totalsContainer">

		<table class="totals">
		<tbody>
		<tr>
			<td width="100"><strong>Total Payments</strong></td><td class="plain right">{$total_payments}</td>
		</tr>
		</tbody>
		</table>

	</div>

	<div class="clear"></div>

HTML;

	}


	// Calculate totals.

	$calculatedTotal = $calculatedSubtotal + $calculatedTax;
	$subtotal = formatMoney($calculatedSubtotal);
	$tax = formatMoney($calculatedTax);
	$total = formatMoney($calculatedTotal);
	$total_payments = formatMoney($calculatedTotalPayments);

	$invoice['amountdue'] = formatMoney($calculatedTotal - $calculatedTotalPayments);

	$content .= <<<HTML

<div class="wrapper">

	<header>

		<div class="accountInfo">
			<p>{$account['name']}</p>
			<p>{$account['address']}{$address2br}{$account['address2']}</p>
			<p>{$account['city']}{$cityStateSeparator}{$account['state']} {$account['zip']}</p>
			<p>{$account['phone']}</p>
		</div>

	</header>

	<h1>Invoice</h1>

	<table class="invoiceInfo">
	<tbody>
	<tr>
		<td width="100">Invoice date:</td>
		<td class="plain right">{$invoice['date']}</td>
	<tr>
	<tr>
		<td width="100">Invoice number:</td>
		<td class="plain right">{$invoice['number']}</td>
	<tr>
	<tr>
		<td width="100">Amount due:</td>
		<td class="plain right">{$invoice['amountdue']}</td>
	<tr>
	</tbody>
	</table>

	<hr>

	<!-- Line Items -->
	<div class="lineitemsContainer">

		<table class="lineitems">
		<tbody>
		<tr>
			<td>Date</td>
			<td class="maincell">Item</td>
			<td class="right">Amount</td>
			<td></td>
		</tr>
		{$lineItemHtml}
		</tbody>
		</table>

	</div>

	<hr>

	<!-- Totals -->
	<div class="totalsContainer">

		<table class="totals">
		<tbody>
		<tr>
			<td width="100">Subtotal</td><td class="plain right">{$subtotal}</td>
		</tr>
		<tr>
			<td width="100">Tax</td><td class="plain right">{$tax}</td>
		</tr>
		<tr>
			<td width="100"><strong>Total</strong></td><td class="plain right">{$total}</td>
		</tr>
		</tbody>
		</table>

	</div>

	<div class="clear"></div>

	<!-- Payments -->

	{$paymentsHtml}

</div>


<div class="footer2">
	<p>Lavu Inc. | 116 Central Ave SW Suite 300 | Albuquerque, NM 87102</p>
	<p>855-528-8457 | support@lavu.com</p>
	<p>Generated {$current_date}</p>
</div>

HTML;

}

// Functions

function formatMoney($amount)
{
	return empty($amount) ? '0.00' : number_format((float)$amount, 2, '.', '');
}

function formatDate($date)
{
	return empty($date) ? '' : date("n/j/Y", strtotime($date));
}

function debugLog($msg)
{
	global $debug;

	if ( $debug ) error_log($msg);
}

function logMsg($msg)
{
	error_log($msg);
}


// Display web page.

echo <<<HTML

<!DOCTYPE html>
<html lang="en">
<head>
	<title>{$title}</title>
	<style>
	html, body {
		font-family: Arial;
		font-size: 9pt;
		margin: 0px;
		padding: 0px;
		height: 100%;
	}
	h1 {
		margin-top: 50px;
		margin-bottom: 20px;
		color: #8b8b8b;
		font-size: 14pt;
		text-align: center;
		text-transform: uppercase;
	}
	h2 {
		margin-top: 40px;
		margin-bottom: 20px;
		color: #8b8b8b;
		font-size: 11pt;
		text-align: center;
		text-transform: uppercase;
	}
	header {
		margin-top:   35px;
		margin-right: 35px;
		margin-bottom: 20px;
		padding-top:  65px;
		padding-left: 65px;
		background: top right no-repeat url("/sa_cp/images/invoicepdf/lavu_logo_invoice.png");
	}
	td {
		white-space: nowrap;
		vertical-align: top;
	}
	.plain {
		color: black;
		font-size: 8pt;
	}
	.right {
		text-align: right;
	}
	.maincell {
		min-width: 100px;
	}
	.createDate {
		color: #8b8b8b;
		font-size: 8px;
		text-align: right;
	}
	.accountInfo p {
    	margin: 2px;
    	padding: 0px;
	}
	.wrapper {
		position: relative;
		margin-left: 15%;
		margin-right: 15%;
		page-break-before: always;  /* page break for pdfs */
	}
	.invoiceInfo {
		color: #8b8b8b;
		min-width: 200px;
		margin-bottom: 50px;
	}
	.accountInfo {
		color: #8b8b8b;	
		font-weight: bold;
		font-size: 9pt;
	}
	.lineitems {
		width: 90%;
	}
	.totals {
		min-width: 200px;
	}
	.lineitemsContainer {
		min-height: 60px;
		margin-left: auto;
		margin-right: auto;
	}
	.lineitemsContainer table,
	.totalsContainer table {
		color: #8b8b8b;	
		margin-top:  15px;
		margin-left: 65px;
		min-height: 75px
	}
	.totalsContainer table {
		float: right;
	}
	.clear {
		clear: both;
	}
	.hidden {
		display: none;
	}
	.footer {
		color: #8b8b8b;
		font-size: 9px;
		text-align: center;
		width: 100%;
		position:absolute;
		bottom:0;
		left:0;
	}
	.footer2 {
		color: #8b8b8b;
		font-size: 9px;
		text-align: center;
		width: 100%;
		clear: both;
		margin-top: 50px;
	}
	section {
		padding-bottom: 30px;
	}
	hr {
		border: 0px;
		margin: 0px;
		margin-left: auto;
		margin-right: auto;
		height: 1px;
		background: no-repeat url("/sa_cp/images/invoicepdf/divider.jpg") 50% 50%;
	}
	</style>
</head>
<body>

{$content}

</body>
</html>

HTML;

?>
