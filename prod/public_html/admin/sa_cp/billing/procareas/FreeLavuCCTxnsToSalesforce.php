<?php

/**
 * This populates/updates the "# of CC Transactions" section for Free Lavu accounts in Salesforce.
 * It is intended to be cron'd and run every day.
 */

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
require_once('/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php');


// Init

$sales_team_email = 'tom@lavu.com';  // TO DO : get real salesteam email from Josh
$developer_email = 'tom@lavu.com';
$send_to_salesforce = true;  // TO DO : base on command-line parameters

$txns = array();
if (!isset($o_package_container))  $o_package_container = new package_container();

mlavu_select_db('poslavu_MAIN_db');

// Get the datanames of the accounts with Free Lavu package levels.

$s_result = mlavu_query( "SELECT * FROM `signups` WHERE `package` IN('17', '18', '19', '20')" );
if ( $s_result === false ) die( mlavu_dberror() );

echo "Got ". mysqli_num_rows( $s_result ) ." datanames with Free Lavu package levels...\n";


// Iterate over the accounts.

while ( $signup = mysqli_fetch_assoc( $s_result ) )
{
	$dataname = $signup['dataname'];
	$package_level = $signup['package'];

	if ( empty($dataname) ) {
		error_log(__FUNCTION__ ." got empty dataname for signup=". print_r($signup, true));
		continue;
	}

	//$restaurant = getRestaurant($dataname);
	list($processor, $package_name, $txn_treshold) = getPaymentProcessorFields($package_level);

	lavu_connect_dn($dataname, "poslavu_{$dataname}_db");

	setAccountInfo($dataname, $package_level, $signup);
	setPaymentProcessor($dataname, $processor, $signup);
	setFirstTxnDate($dataname, $processor);
	setCreditCardTxns($dataname, $processor, $txn_treshold);
}

echo print_r($txns, true) . "\n";

updateSalesforce();

exit;


// Functions

/**
 * Get row from `restaurants` table
 */
function getRestaurant($dataname)
{
	$result = mlavu_query("SELECT * FROM `restaurants` WHERE `data_name` = '[1]'", $dataname);
	if ($result === false)  die( mlavu_dberror() );
	$restaurant = mysqli_fetch_assoc( $result );

	return $restaurant;
}

/**
 * Setup config info (intended processor name, txn tresholds, etc.) based on the package level.
 */
function getPaymentProcessorFields($package_level)
{
	switch ( $package_level )
	{
		case '17':
			$processor = 'Mercury';
			$package_name = 'MercuryFreeLavu88';
			$txn_treshold = 10000.00;
			break;
		case '18':
			$processor = 'Mercury';
			$package_name = 'MercuryGold';
			$txn_treshold = 10000.00;
			break;
		case '19':
			$processor = 'Mercury';
			$package_name = 'MercuryPro';
			$txn_treshold = 10000.00;
			break;
		case '20':
			$processor = 'EVO Snap';
			$package_name = 'EvoSnapFreeLavu88';
			$txn_treshold = 50.00;
			break;
		default:
			$msg = "Missing mapping for unrecognized package level: {$package_level}";
			error_log( $msg );
			die( $msg . "\n" );
			break;
	}

	return array($processor, $package_name, $txn_treshold);
}

/**
 * Set basic info on $txns array.
 */
function setAccountInfo($dataname, $package_level, $signup)
{
	global $txns;
	global $o_package_container;

	$package_name = $o_package_container->get_printed_name_by_attribute('level', $package_level);

	$txns[$dataname]['company'] = $signup['company'];
	$txns[$dataname]['license'] = $package_name;

	return $package_name;
}

/**
 * Query `locations` table in dataname's POS database for credit card processing gateway info and email Sales team with irregularities.
 */
function setPaymentProcessor($dataname, $intended_processor, $signup)
{
	global $txns;

	$result = lavu_query( "SELECT `gateway`, `integrateCC` FROM `poslavu_[dataname]_db`.`locations` WHERE `_disabled` IN('0', '') ORDER BY `id` desc LIMIT 1", array('dataname' => $dataname) );
	if ($result === false) die( lavu_dberror() );
	$location = mysqli_fetch_assoc( $result );
	$actual_processor = $location['gateway'];
	$cc_integration_enabled = ($location['integrateCC'] === '1');

	$txns[$dataname]['cc_integration_enabled'] = ($cc_integration_enabled) ? '1' : '0';
	$txns[$dataname]['processor'] = $actual_processor;
	if (!empty($actual_processor) && !$cc_integration_enabled)  $txns[$dataname]['processor'] .= " (Not enabled)";
	$txns[$dataname]['salesforce_data']['Payment_Processor__c'] = $txns[$dataname]['processor'];

	if ( $cc_integration_enabled && !empty($actual_processor) && $intended_processor !== $actual_processor ) {
		$txns[$dataname]['has_wrong_processor'] = '1';
		emailSalesTeam('has_wrong_processor', $signup);
	}

	$txns[$dataname]['salesforce_opportunity_id'] = $signup['salesforce_opportunity_id'];

	return $actual_processor;
}

/**
 * Get first credit card txn for each month for the dataname and update $txns array.
 */
function setFirstTxnDate($dataname, $processor)
{
	global $txns;

	$sql = "SELECT DATE_FORMAT(`datetime`, '%Y-%m-%d') AS `mmddyyyy` FROM `cc_transactions` WHERE `action`='Sale' AND `pay_type`='Card' AND `refunded`='0' AND `voided`='0' AND `gateway`='[processor]' ORDER BY `datetime` DESC LIMIT 1";
	if ($processor == 'Mercury')  $sql = "SELECT DATE_FORMAT(`datetime`, '%Y-%m-%d') AS `mmddyyyy` FROM `cc_transactions` WHERE `action`='Sale' AND `refunded`='0' AND `voided`='0' AND ((`pay_type`='Card' AND `gateway`='[processor]') OR (`pay_type`='Mercury')) ORDER BY `datetime` DESC LIMIT 1";

	$result = lavu_query($sql, array('processor' => $processor));

	if ($result === false) die( lavu_dberror() );
	$first_txn = mysqli_fetch_assoc( $result );
	$first_txn_date = $first_txn['mmddyyyy'];

	$txns[$dataname]['first_txn_date'] = $first_txn_date;
	$txns[$dataname]['salesforce_data']['First_Trans_Date__c'] = $first_txn_date;

	return $first_txn_date;
}

/**
 * Query `cc_transactions` table in dataname's POS database for credit card transaction data and update $txns array with data and flags.
 */
function setCreditCardTxns($dataname, $processor, $txn_treshold)
{
	global $txns;

	$txns[$dataname]['txns'] = array();

	$sql = "SELECT LEFT(`datetime`, 7) AS `yrmo`, DATE_FORMAT(`datetime`, '%M') AS `month`, DATE_FORMAT(`datetime`, '%b') AS `mon`, SUM(`amount`) AS `txn_amount`, count(*) AS `num_txn` FROM `cc_transactions` WHERE `action`='Sale' AND `pay_type`='Card' AND `refunded`='0' AND `voided`='0' AND `datetime` >= '2015-05-15' AND `gateway`='[processor]'  GROUP BY 1, 2, 3 ORDER BY 1";
	if ($processor == 'Mercury')  $sql = "SELECT LEFT(`datetime`, 7) AS `yrmo`, DATE_FORMAT(`datetime`, '%M') AS `month`, DATE_FORMAT(`datetime`, '%b') AS `mon`, SUM(`amount`) AS `txn_amount`, count(*) AS `num_txn` FROM `cc_transactions` WHERE `action`='Sale' AND `refunded`='0' AND `voided`='0' AND `datetime` >= '2015-05-15' AND ((`pay_type`='Card' AND `gateway`='[processor]') OR (`pay_type`='Mercury')) GROUP BY 1, 2, 3 ORDER BY 1";

	$result = lavu_query($sql, array('processor' => $processor));
	if ($result === false)  die(lavu_dberror());

	//echo "...dataname={$datname} got ". mysqli_num_rows($result) ." row(s) from `cc_transactions`\n";

	while ( $cctxn = mysqli_fetch_assoc($result) )
	{
		$month = strtolower($cctxn['month']);
		$mon = getSalesforceMonthAbbreviation($cctxn['mon']);

		$cctxn['reached_treshold'] = (floatval($cctxn['txn_amount']) > $txn_treshold) ? '1' : '0';
		$txns[$dataname]['txns'][ $cctxn['yrmo'] ] = $cctxn;
		$txns[$dataname]['salesforce_data']["Trans_{$mon}__c"] = $cctxn['num_txn'];
		$txns[$dataname]['salesforce_data']["Transactions_total_{$month}__c"] = $cctxn['txn_amount'];
	}
}

/**
 * Email sales team about account having different credit card processor than their license.
 */
function emailSalesTeam($event, $signup)
{
	global $sales_team_email;
	global $developer_email;

	switch ($event)
	{
		case 'has_wrong_processor':
			$subject = '';
			$message = '';
			break;

		default:
			error_log(__METHOD__ ." got unmapped event: {$event}");
			return;
	}

	if (!empty($sales_team_email)) mail($sales_team_email, $subject, $message);
	if (!empty($developer_email)) mail($developer_email, $subject, $message);
}

function getSalesforceMonthAbbreviation($month)
{
	switch ($month)
	{
		case 'Jun':
			$month_abbreviation = 'June';
			break;
		case 'Jul':
			$month_abbreviation = 'July';
			break;
		case 'Sep':
			$month_abbreviation = 'Sept';
			break;
		default:
			$month_abbreviation = $month;
			break;
	}

	return $month_abbreviation;
}

/**
 * Send collected credit card transaction data to Salesforce
 */
function updateSalesforce()
{
	global $txns;
	global $send_to_salesforce;
	global $developer_email;

	if (!$send_to_salesforce)  return false;

	require_once('/home/poslavu/private_html/salesforceCredentials.php');
	require_once('/home/poslavu/public_html/register/lib/salesforce/SalesforceLead.php');
	require_once('/home/poslavu/public_html/register/lib/salesforce/SalesforceAccount.php');
	require_once('/home/poslavu/public_html/register/lib/salesforce/SalesforceContact.php');
	require_once('/home/poslavu/public_html/register/lib/salesforce/SalesforceOpportunity.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

	echo "Sending credit card transaction data to Salesforce...\n";

	$success_datanames = array();
	$errored_datanames = array();

	$token_file_path = '/tmp/salesforce_token.txt';
	$file_exists = file_exists( $token_file_path );
	$file_not_empty = $file_exists && filesize($token_file_path);
	$is_token_fresh = $file_not_empty && ( time()  < ( filemtime( $token_file_path ) + 60 * 60 * 2 ) );
	$token = $is_token_fresh ? unserialize( file_get_contents($token_file_path ) ) : false;
	if ( !empty ( $token ) && !empty( $token['access_token'] ) ) {
		$credentials = new SalesforceLogin( $token );
	} else {
		$credentials = new SalesforceLogin(SFORCE_UNAME, SFORCE_PWD, SFORCE_CLIENTID, SFORCE_SECRET);
		file_put_contents( $token_file_path, serialize($credentials->get_all() ) );
	}

	$opportunity_meta_file = '/home/poslavu/private_html/salesforceOpportunityMeta.txt';
	$opportunity_meta = file_exists( $opportunity_meta_file ) ? file_get_contents( $opportunity_meta_file ) : null;
	$opportunity_meta = @unserialize( $opportunity_meta );
	$opportunity_handler = new SalesforceOpportunity( $credentials, $opportunity_meta );

	foreach ($txns as $dataname => $data)
	{
		if ( empty($data['salesforce_opportunity_id']) ) {
			$error_msg = "skipping sending Salesforce data for dataname={$dataname} due to blank Opportunity ID";
			echo "...{$error_msg}\n";
			error_log( $error_msg );
			$errored_datanames[$dataname] = $error_msg;
			continue;
		}

		try	{
			$response = $opportunity_handler->update_by_id( $data['salesforce_opportunity_id'], $data['salesforce_data'] );

			echo "...processed dataname={$dataname} company=". $data['company'] ." successfully\n";
			$success_datanames[$dataname] = array('salesforce_opportunity_id' => $data['salesforce_opportunity_id'], 'dataname' => $dataname, 'company' => $data['company'], 'response' => $response);
		}
		catch (Exception $e) {
			$error_msg = $e->getMessage();
			echo "...{$error_msg}\n";
			error_log( $error_msg );
			$errored_datanames[$dataname] = $error_msg;
			continue;
		}
	}

	echo "success_datanames: ". print_r($success_datanames, true) ."\n";  //debug
	echo "errored_datanames: ". print_r($errored_datanames, true) ."\n";  //debug

	if ( count($errored_datanames) > 0 ) {
		mail($developer_email, "ERRORs from ". basename(__FILE__), "Could not process '# of Transactions' data for these datanames with Free Lavu licenses: ". print_r($errored_datanames, true));
	}

	return true;
}
