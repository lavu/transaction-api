<?php

require_once('/home/poslavu/private_html/ZuoraConfig.php');
require_once('/home/poslavu/public_html/inc/billing/zuora/api/API.php');
require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

session_start();

// Do not load the page if they don't have a Lavu session
if (empty(sessvar('admin_dataname')))
{
	return;
}

$config = new stdClass();
$config->wsdl = ZuoraConfig::API_WSDL;

// Init Zuora API
$zApi = Zuora_API::getInstance($config);
$zApi->setLocation(ZuoraConfig::API_ENDPOINT);
$zApi->login(ZuoraConfig::API_USERNAME, ZuoraConfig::API_PASSWORD);

$env = ZuoraConfig::SANDBOX ? 'SANDBOX' : 'PRODUCTION';

date_default_timezone_set('America/Denver');

$queryTool = new ZuoraQueryTool($zApi);
$success = false;
$results = '';

if ( !empty($_REQUEST['queryMore']) )
{
	$success = $queryTool->runZoqlQueryMore($_REQUEST['zoql'], $_REQUEST['queryLocator']);
}
else if ( !empty($_REQUEST['zoql']) )
{
	// Fix bug where running carried over zoql query when viewing result set that has a queryLocator messes up record/paging count
	$_REQUEST['page'] = '';
	$_REQUEST['size'] = '';

	// Run zoql query
	$success = $queryTool->runZoql($_REQUEST['zoql']);
}

$results_class = $success ? '' : 'error';

?>

<html lang="en">
<head>
<title>ZOQL Query Tool</title>
</head>
<style>
html, body {
	font-family: Arial, Helvetica;
}
#querytool input {
	margin-bottom: 0px;
}
#querytool textarea {
	top:0;
	width: 80em;
	height: 4em;
}
#top {
}
#tips {
	font-style: italic;
}
#tips tt {
	color: blue;
	font-style: normal;
}
#querytool #submit {
	vertical-align: top;
	width: 10em;
	height: 4em;
	margin-top:0px;
	margin-bottom:0px;
}
#querytool #queryMore {
	width: 10em;
	height: 2em;
}
#results {
	color: blue;
}
.error pre, .red {
	color: red;
}
</style>
<script>
document.querytool.zoql.onkeyup = function(e){
  e = e || event;
  if (e.keyCode === 13 && !e.ctrlKey) {
    document.getElementById('querytool').submit();
  }
  return true;
 }
</script>
<body>

<p>Zuora <?= $env ?></p>

<form id="querytool" method="post" name="querytool">
	<div id="top">
		<input type="hidden" name="posted" value="1">
		<textarea name="zoql" placeholder="ZOQL goes here"><?= $queryTool->getLastQuery() ?></textarea>
		<input type="submit" name="submit" id="submit" value="Run Query">

		<p id="tips"><tt>SELECT * FROM</tt> = <span class="red">ERROR in Zuora_API::query: You have an error in your ZOQL syntax</span>
		so use fields from the <a href="https://knowledgecenter.zuora.com/DC_Developers/SOAP_API/E1_SOAP_API_Object_Reference">Zuora SOAP API Object_Reference</a>:<br>

		<tt>SELECT Id, Name, AllowInvoiceEdit, AutoPay, Balance, BillCycleDay, BillToId, CrmId, Currency, Dataname__c, DefaultPaymentMethodId, PaymentTerm, SoldToId, Status FROM <span class="red">Account</span> WHERE Dataname__c<br>
		SELECT Id, FirstName, LastName, Address1, Address2, City, State, PostalCode, Country, WorkPhone, WorkEmail, CreatedDate, AccountId FROM <span class="red">Contact</span> WHERE AccountId<br>
		SELECT Id, AccountId, Amount, Balance, DueDate, InvoiceDate, TargetDate, InvoiceNumber, IncludesOneTime, IncludesRecurring, IncludesUsage, LegacyID__c FROM <span class="red">Invoice</span> WHERE AccountId|Status = 'Posted'<br>
		SELECT Id, ChargeAmount, ChargeType, InvoiceId FROM <span class="red">InvoiceItem</span> WHERE InvoiceId|ChargeType|LegacyInvoiceID__c<br>
		SELECT Id, AccountId, AdjustmentDate, Amount, Comment, InvoiceId, InvoiceItemName, InvoiceNumber, LegacyInvoiceID__c, SourceId, SourceType, Type FROM <span class="red">InvoiceItemAdjustment</span> WHERE AccountId|InvoiceId<br>
		SELECT Id, Amount, CreatedDate, InvoiceId, PaymentId, RefundAmount FROM <span class="red">InvoicePayment</span> WHERE PaymentId<br>
		SELECT Id, AccountId, Amount, CreatedDate, EffectiveDate, PaymentNumber, ReferenceId FROM <span class="red">Payment</span> WHERE AccountId|EffectiveDate|Status = 'Processed'<br>
		SELECT Id, AccountId, Type, UseDefaultRetryRule FROM <span class="red">PaymentMethod</span> WHERE AccountId<br>
		SELECT Id, CreatedDate, Name, ProductRatePlanId, SubscriptionId FROM <span class="red">RatePlan</span> WHERE SubscriptionId<br>
		SELECT Id, CreatedDate, EffectiveStartDate, Name FROM <span class="red">RatePlanCharge</span> WHERE RatePlanId|Name<br>
		SELECT Id, Name, Version, Status, ContractEffectiveDate, InvoiceOwnerId, SubscriptionStartDate, TermEndDate, TermStartDate, TermType FROM <span class="red">Subscription</span> WHERE AccountId|TermStartDate|TermType = 'EVERGREEN'<br></tt>
	</div>

	<?= $queryTool->getMoreResultsButton() ?> <a href="#response">Jump to Response</a>

	<table id="results-table" border="1">
	<?= $queryTool->getResultsTable() ?>
	</table>

	<?= $queryTool->getMoreResultsButton() ?>

	<input type="hidden" name="queryLocator" value="<?= $queryTool->getQueryLocator(true) ?>">
	<input type="hidden" name="page" id="page" value="<?= $queryTool->getResultsPage() ?>">
	<input type="hidden" name="size" id="size" value="<?= $queryTool->getSize() ?>">

</form>

<div id="results" class="<?= $results_class ?>">
	<a name="response">Response:</a>
	<tt><pre><?= print_r($queryTool->getResponse(), true) ?></pre></tt>
</div>

</body>
</html>

<?php

class ZuoraQueryTool
{
	private $zApi;
	private $response;
	private $result;
	private $last_query;
	private $zQueryLocatorId;

	public function __construct($zApi)
	{
		$this->zApi = $zApi;
		$this->last_query = '';
		$this->results = '';
	}

	public function getResponse()
	{
		return $this->response;
	}

	public function getLastQuery()
	{
		return $this->last_query;
	}

	public function getSize()
	{
		$size = '';

		if ( isset($this->response) && isset($this->response->size) )
		{
			$size = $this->response->size;
		}

		return $size;
	}

	public function getResultsPage()
	{
		// Default to 1
		$page = 1;

		if ( !empty($_REQUEST['page']) )
		{
			// If we're coming in with a value, we'll always need to increment it because that means we're displaying the next page of results
			$page = (int) $_REQUEST['page'];
			$page++;
		}

		return ''+ $page;  // convert to string
	}

	public function getQueryLocator($html_escape = false)
	{
		$queryLocator = '';

		if ( isset($this->response) && isset($this->response->result) && isset($this->response->result->queryLocator) )
		{
			$queryLocator = $this->response->result->queryLocator;
			if ($html_escape) $queryLocator = htmlentities($queryLocator); 
		}

		return $queryLocator;
	}

	public function runZoql($zoql)
	{
		//error_log('In '. __METHOD__ ."() with zoql={$zoql}");  //debug

		if ( empty($zoql) ) {
			return false;
		}

		$this->last_query = trim($zoql);

		try {
			$this->response = $this->zApi->query($zoql);
		}
		catch ( Exception $e ) {
			$this->response = $e->getMessage();
			error_log(__FILE__ .": ". $e->getMessage());
			return false;
		}

		return true;
	}

	// Doesn't actually use zoql, just re-displays it in textarea
	public function runZoqlQueryMore($zoql, $queryLocator)
	{
		error_log('In '. __METHOD__ ."() with queryLocator={$queryLocator}");  //debug

		if ( empty($zoql) || empty($queryLocator) ) {
			return false;
		}

		$this->last_query = trim($zoql);

		try {
			$this->response = $this->zApi->queryMore($queryLocator);
		}
		catch ( Exception $e ) {
			$this->response = $e->getMessage();
			error_log(__FILE__ .": ". $e->getMessage());
			return false;
		}

		return true;
	}

	public function getResultsTable()
	{
		$table_html = '';

		if ( isset($this->response) && isset($this->response->result) ) {
			if ( $this->response->result->size === 1 )
			{
				$records[] = $this->response->result->records;
			}
			else if ( $this->response->result->size > 1 )
			{
				$records = $this->response->result->records;
			}
			else
			{
				$records = array();
			}

			// Get the exact column ordering from the zoql SELECT clause so we can retain that ordering when displaying results.
			preg_match('/SELECT\s+(\S.+\S)\s+FROM/i', $_REQUEST['zoql'], $matches);
			$columns = explode(',', $matches[1]);

			// Header row
			if ( count($records) >= 1 )
			{
				$table_html .= '<tr><td></td>';
				foreach ( $columns as $key ) {
					$key = trim($key);
					$table_html .= '<td>'. $key .'</td>';
				}
				$table_html .= '</tr>';
			}

			// Adjust record counter based on the page of the result set we're on
			$page_offset = $this->getResultsPage() - 1;
			$record_set_size = 2000;
			$i = 1 + ($page_offset * $record_set_size);

			foreach ( $records as $zObj ) {
				$data = $zObj->getData();
				$table_html .= '<tr>';

				// Table row of values for each result record
				$table_html .= '<td>'. $i .'</td>';
				foreach ( $columns as $key ) {
					$key = trim($key);
					$val = isset($data[$key]) ? $data[$key] : '-';
					$table_html .= '<td>'. $val .'</td>';
				}

				$table_html .= '</tr>';
				$i++;
			}
		}

		return (string) $table_html;
	}

	public function getMoreResultsButton()
	{
		$querymore_button_html = '';

		if ( isset($this->response) && isset($this->response->result) && empty($this->response->result->done) && isset($this->response->result->queryLocator) )
		{
			$querymore_button_html = '<input type="submit" name="queryMore" id="queryMore" value="Get more results >">';
		}

		return $querymore_button_html;
	}
}

?>