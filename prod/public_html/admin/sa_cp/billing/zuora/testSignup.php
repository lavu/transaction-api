<?php

$req = array('company' => '', 'package' => 'Lavu', 'default_menu' => 'default_restau', 'email' => '', 'url' => 'http://register.localhost/signup');
$opt = array('firstname' => '', 'firstname' => '', 'lastname' => '', 'address' => '', 'city' => '', 'state' => '', 'zip' => '');
$out = $_REQUEST + $req;
$out = $out + $opt;  // Prevets undefined index notices without destructively overwriting anything
$out_html = array_map('htmlentities', $out);

echo <<<HTML
	<html>
	<body>
	<form method="post">
		<input required type="text" name="url" id="url" placeholder="URL" size="50" value="{$out_html['url']}">
		<br>
		<br>
		<input type="text" name="package" id="package" placeholder="Package Name" value="{$out_html['package']}"> like Lavu88Trial
		<br>
		<input type="text" name="default_menu" id="default_menu" placeholder="Default Sample Menu" value="{$out_html['default_menu']}"> default_restau|defaultbar|default_pizza_|default_coffee
		<br>
		<input type="text" name="email" id="email" placeholder="Email Address" value="{$out_html['email']}"> like email@ddress.com
		<br>
		<input type="text" name="company" id="company" placeholder="Company Name" value="{$out_html['company']}">
		<br>
		<input type="text" name="firstname" id="firstname" placeholder="First Name" value="{$out_html['firstname']}">
		<br>
		<input type="text" name="lastname" id="lastname" placeholder="Last Name" value="{$out_html['lastname']}">
		<br>
		<input type="text" name="address" id="address" placeholder="Address" value="{$out_html['address']}">
		<br>
		<input type="text" name="city" id="city" placeholder="City" value="{$out_html['city']}">
		<br>
		<input type="text" name="state" id="state" placeholder="State" value="{$out_html['state']}">
		<br>
		<input type="text" name="zip" id="zip" placeholder="Zip" value="{$out_html['zip']}">
		<br>
		<br>
		<input type="submit" name="submit" value="submit">
	</form>
HTML;

if ( !empty($out['url']) )
{
	$url = isset($out['url']) ? $out['url'] : '';
	unset($out['url']);
	unset($out['submit']);
	unset($out['PHPSESSID']);

	$json = json_encode($out, JSON_PRETTY_PRINT);

	echo <<<HTML
		<h1>OUTGOING JSON to {$url}</h1>
		<pre>{$json}</pre>
HTML;

	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 60); 
	curl_setopt($curl, CURLOPT_TIMEOUT, 60);

	$json_response = curl_exec($curl);

	$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	//if ( $status != 201 ) {
	//    echo "<h3 style='color:red'>Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl) ."</h3>";
	//}
	curl_close($curl);

	//$response = json_decode($json_response, true);
	$response = json_decode($json_response, true);
	$json_response = json_encode($response, JSON_PRETTY_PRINT);

	echo <<<HTML
		<h1>RESPONSE JSON from {$url}</h1>
		<pre>{$json_response}</pre>
		</body>
		</html>
HTML;

}
