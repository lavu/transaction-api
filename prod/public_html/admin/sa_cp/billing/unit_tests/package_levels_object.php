<?php

	$in_lavu = TRUE;
	$inlavu = TRUE;
	require_once(dirname(__FILE__)."/../../../cp/resources/lavuquery.php");
	require_once(dirname(__FILE__)."/../payment_profile_functions.php");
	require_once(dirname(__FILE__)."/../procareas/monthly_parts.php");
	require_once(dirname(__FILE__)."/../procareas/invoice_parts.php");
	require_once(dirname(__FILE__)."/../new_process_payments.php");
	require_once(dirname(__FILE__)."/../package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();


	draw_styles();
	do_all_unit_tests();

	function do_all_unit_tests() {
		$a_unit_test_groups = get_unit_tests_all();

		global $o_package_container;
		$i_test_number = 0;

		foreach($a_unit_test_groups as $a_unit_test_group) {
			echo "<h2>Test #{$i_test_number}</h2>";
			echo "<table><tr><th>status</th><th>test</th><th>expected</th><th>actual</th><tr>";
			foreach($a_unit_test_group as $a_unit_test) {
				$s_test = $a_unit_test[1];
				$wt_expected_value = $a_unit_test[3];
				$wt_produced_value = call_user_method_array($a_unit_test[0], $o_package_container, $a_unit_test[2]);
				draw_and_test($s_test, $wt_expected_value, $wt_produced_value);
			}
			echo "</table>";
			$i_test_number++;
		}
	}

	function get_unit_tests_all() {
		global $o_package_container;
		$a_retval = array();
		$a_packages = $o_package_container->get_packages();
		foreach($a_packages as $a_package) {
			$a_unit_tests = get_unit_tests_by_package($a_package);
			$a_retval[] = $a_unit_tests;
		}
		return $a_retval;
	}

	function get_unit_tests_by_package($a_package) {
		$a_retval = array();
		$a_attributes = array(
			array("name", "get_plain_name"),
			array("value", "get_value"),
			array("recurring", "get_recurring"),
			array("min_value", "get_min_value"),
			array("min_recurring", "get_min_recurring"),
			array("level", "get_level")
		);

		foreach($a_attributes as $a_attribute) {
			$s_attribute_name = $a_attribute[0];
			$s_attribute_value = (string)call_user_method($a_attribute[1], $a_package);
			$a_retval[] = array("get_printed_name_by_attribute", "get_printed_name_by_attribute({$s_attribute_name})", array($s_attribute_name, $s_attribute_value), (string)call_user_method("get_printed_name", $a_package));
			$a_retval[] = array("get_plain_name_by_attribute", "get_plain_name_by_attribute({$s_attribute_name})", array($s_attribute_name, $s_attribute_value), (string)call_user_method("get_plain_name", $a_package));
			$a_retval[] = array("get_value_by_attribute", "get_value_by_attribute({$s_attribute_name})", array($s_attribute_name, $s_attribute_value), (double)call_user_method("get_value", $a_package));
			$a_retval[] = array("get_recurring_by_attribute", "get_recurring_by_attribute({$s_attribute_name})", array($s_attribute_name, $s_attribute_value), (double)call_user_method("get_recurring", $a_package));
			$a_retval[] = array("get_recurring_points_by_attribute", "get_recurring_points_by_attribute({$s_attribute_name})", array($s_attribute_name, $s_attribute_value), (int)call_user_method("get_recurring_points", $a_package));
			$a_retval[] = array("get_level_by_attribute", "get_level_by_attribute({$s_attribute_name})", array($s_attribute_name, $s_attribute_value), (int)call_user_method("get_level", $a_package));
			$a_retval[] = array("is_points_package_by_attribute", "is_points_package_by_attribute({$s_attribute_name})", array($s_attribute_name, $s_attribute_value), (boolean)call_user_method("is_points_package", $a_package));
		}

		$a_retval[] = array("get_package_status", "get_package_status", array($a_package->get_plain_name()), "package");
		//$a_retval[] = array("compare_package_by_name", array($p1_name, $p2_name), $wt_expected_value)
		//$a_retval[] = array("get_upgrade_old_new_names", array($s_upgrade_name), $wt_expected_value)
		//$a_retval[] = array("get_upgrade_printed_name", array($s_upgrade_name), $wt_expected_value)
		//$a_retval[] = array("get_upgrade_cost_by_old_new_names", array($s_old_package_name, $s_new_package_name), $wt_expected_value)
		//$a_retval[] = array("upgrade_matches_license", array($i_upgrade_level, $i_license_level), $wt_expected_value)

		return $a_retval;
	}

	function draw_styles() {
		?>
		<style type='text/css'>
			.passed {
				color: green;
			}
			.failed {
				color: red;
			}
		</style>
		<?php
	}

	function draw_and_test($s_test, $wt_expected_value, $wt_produced_value) {
		$b_passed = ($wt_expected_value === $wt_produced_value);
		$s_passed = $b_passed ? "passed" : "failed";
		$s_expected = "(".gettype($wt_expected_value).") {$wt_expected_value}";
		$s_produced = "(".gettype($wt_produced_value).") {$wt_produced_value}";

		echo "<tr><td class='{$s_passed}'>{$s_passed}</td><td>{$s_test}</td><td>{$s_expected}</td><td>{$s_produced}</td><tr>";
	}

?>