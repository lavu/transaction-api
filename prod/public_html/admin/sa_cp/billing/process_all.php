<?php
	session_start();

	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once(dirname(__FILE__) . "/payment_profile_functions.php");
	require_once(dirname(__FILE__) . "/new_process_payments.php");
	require_once(dirname(__FILE__)."/package_levels_object.php");
	$maindb = "poslavu_MAIN_db";
	if (!isset($o_package_container))
		$o_package_container = new package_container();

	//ini_set("display_errors","1");

	/* NOTES

		: possible values for restaurants.disabled :
		1 -
		2 -
		3 - automatically disabled no package clients with no activity since 2012-06-01

	*/
	$_SESSION['processing_all_bills'] = true;

	$loggedin = (isset($_SESSION['posadmin_loggedin']))?$_SESSION['posadmin_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posadmin_fullname']))?$_SESSION['posadmin_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posadmin_email']))?$_SESSION['posadmin_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;
	if($loggedin_access=="") $loggedin_access = "";

	if(empty($_SERVER['REMOTE_ADDR'])) $running_from_cmdline = true;
	else $running_from_cmdline = false;

	if($running_from_cmdline) $nl = "\n";
	else $nl = "<br>";

	$start_num = 0;
	$max_num = 100000;
	if(isset($argc) && $argc>=2 && is_numeric($argv[1]))
	{
		$start_num = $argv[1];
		if(isset($argv[2]) && is_numeric($argv[2]))
			$max_num = $argv[2];
	}
	echo "Processing with limit $start_num to $max_num\n";

	if (!$loggedin && !$running_from_cmdline)
	{
		return;
	}

	set_time_limit(6000);
	ini_set("memory_limit","800M");

	$details = "";
	$str = "";
	$processed_count;
	$total_count;
	$min_datetime = "2008-01-01 00:00:00";
	$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `created` >= '[1]' and `disabled`!='1' and `disabled`!='2' and `disabled`!='3' and `data_name`!='' and `notes` NOT LIKE '%AUTO-DELETED%' AND `lavu_lite_server` = '' order by `created` desc",$min_datetime);
	//if ($_SERVER['REMOTE_ADDR'] == '74.95.18.90')
		//$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `created` >= '[1]' and `disabled`!='1' and `disabled`!='2' and `disabled`!='3' and `data_name`!='' and `data_name`='brickyard_pizz1' and `notes` NOT LIKE '%AUTO-DELETED%' order by `created` desc",$min_datetime);

	$total_rest_count = mysqli_num_rows($rest_query);
	while($rest_read = mysqli_fetch_assoc($rest_query))
	{
		$total_count++;
		if($total_count < $start_num || $total_count > $max_num)
		{
			continue;
		}

		echo "process ".$total_count." (".$rest_read['id'].")... ";

		$pstr = "";
		$p_area = "";
		$dataname = $rest_read['data_name'];
		$restid = $rest_read['id'];
		$created_datetime = $rest_read['created'];
		$created_month = "";
		$dt_parts = explode(" ",$created_datetime);
		$dt_parts = explode("-",$dt_parts[0]);
		if(count($dt_parts) > 2)
		{
			$created_month = date("M d Y",mktime(0,0,0,$dt_parts[1],$dt_parts[2],$dt_parts[0]));
		}
		else $created_month = $created_datetime;

		$signup_read = get_signup_by_id_dataname($restid, $dataname);
		if (is_array($signup_read) && isset($signup_read['id'])) {
			$signupid = $signup_read['id'];
			$package = $signup_read['package'];
			$signup_status = $signup_read['status'];
			$trial_end = "";
			$license_applied = "";

			$pstr .= "-------------- Count: $total_count / $total_rest_count ($processed_count processed) ------------------" . $nl;
			$pstr .= "signupid: $signupid" . $nl . "package: " . $package . $nl;
			$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$restid);
			if(mysqli_num_rows($status_query))
			{
				$status_read = mysqli_fetch_assoc($status_query);
				$license_applied = $status_read['license_applied'];
				if($status_read['trial_end']!="" && $status_read['license_applied']=="")
				{
					$trial_end = $status_read['trial_end'];
				}

				if($status_read['license_type']!="" && $signup_read['package']=="")
				{
					$set_package = "";
					$set_package = $o_package_container->get_level_by_attribute('name', $status_read['license_type']);

					mlavu_query("update `poslavu_MAIN_db`.`signups` set `package`='[4]' where `dataname`='[1]' and `restaurantid`='[2]' and `id`='[3]' and `package`='' limit 1",$dataname,$restid,$signupid,$set_package);
					$package = $set_package;
				}
			}

			if($package=="")
			{
				$pstr .= $total_count . ") $created_month $dataname - no package".$nl;
				//echo $dataname . " Trial Ends: " . $trial_end . " ($license_applied)<br>";
			}
			else
			{
				$processed_count++;

				$details .= $dataname . " - " . $created_datetime . $nl;
				$pp_result = process_payments("process", $dataname, $package, $signup_status);
				$details .= "Due: " . $pp_result['all_bills_due'] . $nl;
				$details .= "Info: " . $pp_result['due_info'] . $nl;
				$details .= "------------------------------------------------" . $nl;
				$str .= ".";
				$pstr .= $total_count . ") $created_month $dataname - $package".$nl;
				$pstr .= $pp_result['due_info'] . $nl . $nl;
			}
		}
		else
		{
			$pstr .= $total_count . ") $created_month $dataname - no signup record".$nl;
		}

		$fp = fopen("/home/poslavu/private_html/tools/data/process_all.log","a");
		fwrite($fp,$pstr);
		fclose($fp);
	}
	echo "Processed " . $processed_count . "/" . $total_count . $nl . $str . $nl . $nl;
	echo $details . $nl;
	echo $nl;
?>
