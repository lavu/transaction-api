<?php
require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

$dataname = isset($_REQUEST["dataname"]) ? $_REQUEST["dataname"] : "";
$pos_db_name = "poslavu_{$dataname}_db";
$posted_reenabled_users = !empty($_REQUEST['reenable_users']);
$reenable_users = isset($_REQUEST['reenable_users']) ? $_REQUEST['reenable_users'] : array();

list($update_sql, $update_error, $updated_users) = restoreUsers($pos_db_name, $posted_reenabled_users, $reenable_users, $dataname);
?>

<!DOCTYPE html>
<html>
<head>
	<title>User Restore</title>
</head>
<body>
<div>
	<img src = 'images/lavu.png' style="padding-left: 10px; float: left" width="50px">
	<img src = 'images/lavu.png' style="padding-right: 10px; float: right" width="50px">
<h1 style="color:#99BC08;text-shadow:1px 1px 0px black; margin-left: 70px;">User Restore</h1>
<hr WIDTH="98%" COLOR="#99BC08" SIZE="2">
</div>
	<form method = 'POST' action = ''>
<!-- 		<label style="color:#99BC08; margin: 10px;">Dataname:</label> -->
		<input type = 'hidden' value = '<?= $dataname ?>' name = 'dataname' id = 'dataname' placeholder = 'Enter Dataname'>
<!-- 		<input type = 'Submit'> -->

		<input type = 'hidden' name = 'posted' value = '1'>
	</form>
	<br>
<?php
if (!empty($dataname))
{
	echo <<<HTML
	<div style="margin:10px;">
	<form method = 'POST' action = ''>
		<input type='hidden' name = 'posted' value = '1'>
		<input type='hidden' name = 'dataname' value = '$dataname'>
		<table border = '1' bgcolor = "#494b4c">
		<tr bgcolor = "#99BC08"><td><strong>ID</strong></td>
		<td><strong>Username</strong></td>
		<td><strong>First Name</strong></td>
		<td><strong>Last Name</strong></td>
		<td></td></tr>
HTML;

	$users = mlavu_query("SELECT f_name as 'firstName', l_name as 'lastName', username as 'username', id as 'id' FROM `$pos_db_name`.`users` where _deleted = 1 order by l_name asc");

	if ($users === false)
	{
		echo "<h4 style = 'color:red'>ERROR: Invalid dataname: ".$dataname;
	}

	while ($user = mysqli_fetch_assoc($users))
	{
		$f_name = $user['firstName'];
		$l_name = $user['lastName'];
		$username = $user['username'];
		$selected_user_id = $user['id'];
		$checked = '';

		echo "<tr bgcolor = 'white'><td>".$selected_user_id."</td><td>".$username."</td><td>".$f_name."</td><td>".$l_name."</td><td><label><input type='checkbox' $checked name = 'reenable_users[]' value = '{$selected_user_id}' id='reenable_user{$selected_user_id}'></label></td></tr>"; 
	}
	echo "</table>";
	echo "<br><input type = 'Submit' name = 'action' value = 'Restore User'></form>";
	echo "</div>";
}

echo $update_error;

if (!$update_error)
{
	returnRestoredUsernames($pos_db_name, $reenable_users);
}

?>

</body>
</html>

<?php

function restoreUsers($pos_db_name, $posted_reenabled_users, $reenable_users, $dataname)
{
	global $data_name;

	if($posted_reenabled_users)
	{
		$update_sql = '';
		$update_error = '';
		$updated_users = array();
		foreach($reenable_users as $selected_user_id)
		{
			// echo $selected_user."<br>";
			$sql = "UPDATE $pos_db_name.users set _deleted = 0 where id = $selected_user_id and _deleted = '1'";
			$update_sql .= $sql."<br>";
			$update_ok = mlavu_query("UPDATE $pos_db_name.users set _deleted = 0 where id = $selected_user_id and _deleted = '1'");
			$updated_users[$selected_user_id] = $update_ok;

			if(!$update_ok)
			{
				$data_name = $dataname;
				$conn = ConnectionHub::getConn('poslavu');

				error_log("conn = ".print_r($conn, true));
				$update_error .= "<h4 style = 'color:red'>ERROR: ".mysqli_error($conn)."</h4>";
			}
		}
		return array($update_sql, $update_error, $updated_users);
	}
}

function returnRestoredUsernames($pos_db_name, $reenable_users)
{
	if($reenable_users)
	{
		$checkUsers = array();
		echo "<br><table style='margin:10px;' border = '1' bgcolor = '#494b4c'><tr bgcolor = '#99BC08'><td><strong>Restored Usernames:</strong></td></tr>";
		foreach($reenable_users as $selected_user_id)
		{
			$findUsers = mlavu_query("SELECT username as 'username' from $pos_db_name.users where id = $selected_user_id");
			$restoredUsernamesResults = mysqli_fetch_assoc($findUsers);
			$checkUsers = $restoredUsernamesResults['username'];
			echo "<tr bgcolor= 'white'><td>".$checkUsers."</td></tr>";
		}
		echo "</table>";
	}
}