<?php
	require_once ('/home/poslavu/public_html/3rd_party/jpgraph/src/jpgraph.php');
	require_once ('/home/poslavu/public_html/3rd_party/jpgraph/src/jpgraph_bar.php');

	$datay1 = explode(",",$_GET['data']);
	//$datay1 = array(20,10,36);
	//$datay1=$graph_vals;
	
	// Create the graph.
	$graph = new Graph($_GET['width'],$_GET['height']);
	$graph->SetScale('textlin');
	$graph->SetMarginColor('white');
	
	// Setup title
	$graph->title->Set("");
	
	// Create the first bar
	$bplot = new BarPlot($datay1);
	$bplot->SetFillGradient('AntiqueWhite2','AntiqueWhite4:0.8',GRAD_VERT);
	$bplot->SetColor('darkred');
	
	$accbplot = new AccBarPlot(array($bplot));
	$graph->Add($accbplot);
	
	$graph->Stroke();
?>