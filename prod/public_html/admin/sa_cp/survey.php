<?php
	/* This form likes to make unnecessary submissions to database. Gotta clean the garbage */
	lavu_query("DELETE FROM survey WHERE question=''");
	
?>

<?php
	/* functions here */
function clean($input){
		
		$string = addslashes($input);
		
		return $string;
		
}//clean
?>


<?php



/* Process form submission */

/* Get most recent survey id and increment by one */
$res0 = lavu_query("SELECT survey_id FROM survey WHERE setting='survey_question' ORDER BY survey_id DESC");
$row0 = mysqli_fetch_assoc($res0);
//mysql_data_seek($res0, 0);

$max_survey_id = $row0['survey_id'];
$max_survey_id_arr = explode("_",$max_survey_id);
$cur_max_survey_id = $max_survey_id_arr[1];
$new_max_survey_id = $cur_max_survey_id*1 + 1;
$survey_id = "s_".$new_max_survey_id;

$title = clean($_POST['title']);
$resTitle = lavu_query("INSERT INTO survey (setting,survey_id,question) VALUES ('survey_title','$survey_id','$title')");
$instructions = clean($_POST['instructions']);
$resInst = lavu_query("INSERT INTO survey (setting,survey_id,question) VALUES ('survey_instructions','$survey_id','$instructions')");
//echo "maxId: $survey_id<br/>";

$num_rows = $_POST['num_rows'];
//echo "numRows: $num_rows<br/>";
for ($i=1;$i<=$num_rows*1;$i++){
		$question_name = "question_".$i;
		$question_val = clean($_POST[$question_name]);
		
		$type_name = "type_".$i;
		$type_val = clean($_POST[$type_name]);
		
		$delete_name = "delete_".$i;
		$delete_val = clean($_POST[$delete_name]);
		
		if ($type_val == "dropdown"){	
				$num_options_name = "num_options_".$i;
				$num_options_val = $_POST[$num_options_name];
				for ($q=1;$q<$num_options_val*1;$q++){
						$cur_option_name = "row_".$i."_option_".$q;
						$cur_option_val .= clean($_POST[$cur_option_name]).",";
				}//for
			//echo "ques: $cur_option_val<br/>";
		}//if
		
		//echo "in: $instructions<br/>";
		//lavu_query("INSERT INTO survey (setting,type,options,question,survey_id) VALUES ('survey_question','$type_val','$cur_options_val','$survey_id')");
		lavu_query("INSERT INTO survey (setting,type,options,question,survey_id) VALUES ('survey_question','$type_val','$cur_option_val','$question_val','$survey_id')");
		/* Resest any variables */
		$cur_option_val = "";
}//for


?>


<style type="text/css">

.rowElement{
	width: 650px;
	
	float:left;
}

.header{
	text-align:center;
}

.re1{
	width: 160px;
	
}
.re2{
	width: 100px;

}
.re3{
	width: 100px;
	
	text-align:center;
}
.re4{
	width: 100px;
	
}

.row{
	border-bottom:1px solid #000;
	
}

.clearRow{
	clear:both;
}

.toolbar{
	float:left;
	width:100px;
	
	text-align:center;
}

.panel{
	float:left;
	width:650px;
	border-left:3px solid #000;
	
}

.main{
	width:800px; 
	margin:auto;
	border:1px solid #999;
}

</style>

<?php


?>




<script type="text/javascript">

function sureSubmit(){
		/* Ask people if they want to submit the form */
		answer = confirm("Do you want to create the survey now? There is currently no way to edit it after submission.");
		if (answer){return true;}
		
		return false;
}

function addOption(index,row_num){
		/* if  user selects an HTML input that requires options, such as drop down, radio, etc, display text fields so the user can specify those options */
		//alert("row_num: " + row_num);
		//their_data = saveEntries();
		num_options_id = "num_options_"+index;
		num_options = document.getElementById(num_options_id).value;
		new_num_options = num_options*1 + 1;
		their_data = "";
		//alert(num_options);
		if (row_num*1 == num_options*1){
				name = "re3_"+index;
				their_data = saveEntries();
				add_option = document.getElementById(name).innerHTML;
				add_option += "<input type='text' name='row_"+index+"_option_"+new_num_options+"' id='row_"+index+"_option_"+new_num_options+"' style='width:85px;' onclick='addOption("+index+","+new_num_options+");' />";
				document.getElementById(name).innerHTML = add_option;
				writeEntries(their_data);//this needs to occur write after the innerHTML gets written back
				
				
				
				document.getElementById(num_options_id).value = new_num_options;//write this at the end so it isn't overwritten
				new_focus_id = "row_"+index+"_option_"+num_options;
				document.getElementById(new_focus_id).focus();
				//alert("their_data: " + their_data);
				
		}//if
		
		
		
		//alert(",: " + new_focus_id);
		//writeEntries(their_data);//put the data back
}//addOption()

function toAddOrNotToAdd(index,row_num){
		/* Do we need another option?*/
		//num_options_id = "num_options_"+index;
		//num_options = document.getElementById(num_options_id).value;
		
}//toAddOrNotToAdd()

function doWhat(type,index){
		/* This function decides if text fields need to be displayed in the re3 column, like if a user chose an html input option that requires options to be specified (dropdown, radio, etc) */
		
		if (type=="text"){
				name = "re3_"+index;
				document.getElementById(name).innerHTML = "";
		}//if
		
		if ( (type=="radio") || (type=="dropdown") ){
				addOption(index,0);
				return true;
		}//if
}//doWhat()

function deleteRow(row_num){
		/* row_num is the index. This function deletes the row by placing 'delete' in a hidden text field. Upon submitting the form, the row entry is 	  removed from the database. */
		answer = confirm("Do you wish to delete this row?");
		if (!answer){return false;}
		row_name = "row_num_"+row_num;
		document.getElementById(row_name).style.display = 'none';
		
		delete_name = "delete_"+row_num;
		//alert("delete row: " + delete_name);
		document.getElementById(delete_name).value = "delete";
}//deleteRow()

function saveEntries(){
		/* This function grabs all the data inputs before the form gets overwritten, then this function repastes data to the relevent window after rewriting has occured */
		var num_rows = document.getElementById("num_rows").value;
		var data = "";
		for (i=1;i<=num_rows;i++){
				
				question_name = "question_"+i;
				question_val = document.getElementById(question_name).value;
				data += question_val+"\,";
				
				input_name = "type_"+i;
				input_val = document.getElementById(input_name).value;
				data += input_val+"\,";
				
				num_options_name = "num_options_"+i;
				num_options_val = document.getElementById(num_options_name).value;
				data += num_options_val+",";
				//alert("numOptionsVal: " + num_options_val);
				if (num_options_val*1 != 0){
						//alert("here");
						//data += "\@";
						for (option_num=1;option_num<num_options_val*1;option_num++){
									
									cur_option_name = "row_"+i+"_option_"+option_num;
									
									cur_option_val = document.getElementById(cur_option_name).value;
									//alert("curOptionVal: " + cur_option_val);
									data += cur_option_val+"\#";
						}//for
						
						data += "\,";
						//get the options
				}//if
				
				delete_name = "delete_"+i;
				delete_val = document.getElementById(delete_name).value;
				data += "\,"+delete_val;
				data += "\|\|";
		}//for
		
		//alert("data: " + data);
		return data;
}//saveEntries

function writeEntries(data){
		/* This function grabs all the data inputs before the form gets overwritten, then this function repastes data to the relevent window after rewriting has occured */
		//alert("writeEntries: " + data);
		data_str = data.split("||");
		data_str_length = data_str.length;
		//alert("length: " + data_str_length);
		index = 1;
		for (i=0;i<data_str_length*1-1;i++){
				question = data_str[i];
				question_arr = question.split(",");
				question_len = question_arr.length;
				//for(j=0;j<question_len;j++){
						question_val = question_arr[0];
						question_name = "question_"+index;
						document.getElementById(question_name).value = question_val;
						//alert("index: " + index + "\nquestion: " + question_val);
						type_val = question_arr[1];
						input_name = "type_"+index;
						document.getElementById(input_name).value = type_val;
						
						if (type_val == "dropdown"){
								num_options_val = question_arr[2];
								num_options_name = "num_options_"+index;
								document.getElementById(num_options_name).value = num_options_val;
								
								options_str = question_arr[3];
								options_arr = options_str.split("#");
								o_index = 1;
								for (z=0;z<options_arr.length;z++){
										cur_option_name = "row_"+index+"_option_"+o_index;
										//alert("cur+option_name: " + cur_option_name);
										document.getElementById(cur_option_name).value = options_arr[z];
										o_index++;
								}//for
						}//if
						deleted_val = question_arr[4];
				//}//for
				//alert("question: " + question);
				index++;
		}//for
		//alert("data: " + data);
		return data;
}//writeEntries

function addEmptyRow(){
		/* Add a row for a user to enter another question. They can select any of the html input options */
 		their_data = saveEntries();
		index = document.getElementById("num_rows").value;//this is the number of rows;
		index++;
		//var rowString = "";
		whats_already_there = document.getElementById("panel").innerHTML;
		whats_already_there += "<div class='row' id='row_num_"+index+"'>";
		
		whats_already_there += "<div class='rowElement re1'>";
		whats_already_there += "<textarea style='width:150px; height:50px;'  name='question_"+index+"' id='question_"+index+"' ></textarea>";
		whats_already_there += "</div>";

		whats_already_there += "<div class='rowElement re2'>";
		whats_already_there += "<select name='type_"+index+"' id='type_"+index+"' onchange='doWhat(this.value,"+index+");'>";
		whats_already_there += "<option value='text'>Text</option>";
		//whats_already_there += "<option value='radio'>Radio</option>";
		whats_already_there += "<option value='dropdown'>Drop down</option>";
		whats_already_there += "</select>";
		whats_already_there += "</div>";
		
		whats_already_there += "<div class='rowElement re3' id='re3_"+index+"' >";
		whats_already_there += "<br/><input style='width:20px;' value='0' type='hidden' name='num_options_"+index+"' id='num_options_"+index+"'/>";
		whats_already_there += "</div>";
		
		whats_already_there += "<div class='rowElement re4'>";
		whats_already_there += "<span onclick='deleteRow("+index+");'>Delete</span>";
		whats_already_there += "<input type='hidden'  id='delete_"+index+"' name='delete_"+index+"'  style='width:20px;'/>";
		whats_already_there += "</div>";
		
		whats_already_there += "</div><!--row-->";
		
		whats_already_there += "<div class='clearRow'></div>";
		
		
		document.getElementById("panel").innerHTML = whats_already_there;
		
		document.getElementById("num_rows").value = index;
		writeEntries(their_data);//put the data back
}//addRow()

</script>

<?php
	/* This form likes to make unnecessary submissions to database. Gotta clean the garbage */
	lavu_query("DELETE FROM survey WHERE question=''");
?>