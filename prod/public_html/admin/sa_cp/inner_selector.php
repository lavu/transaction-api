<?php

	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");

	$selector_type = $_REQUEST['selector_type'];
	$company_id = $_REQUEST['company_id'];
	
	if ($selector_type == "comp") {
	
		if (isset($_REQUEST['make_change'])) {
		
			$q_vars = array();
			$q_vars['test'] = 0;
			$q_vars['demo'] = 0;
			$q_vars['reseller'] = 0;
			
			if ($_REQUEST['account_type'] != "Client") {
				$q_vars[$_REQUEST['account_type']] = 1;
			}
			
			$keys = array_keys($q_vars);
			$q_update = "";
			foreach ($keys as $key) {
				if ($q_update != "") { $q_update .= ", "; }
				$q_update .= "`$key` = '[$key]'";
			}
			$q_vars['id'] = $company_id;
			$make_change = mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET $q_update WHERE `id` = '[id]'", $q_vars);
		}
		
		if (isset($_REQUEST['type']) && ($_REQUEST['type'] != "")) {
			$company_info[$_REQUEST['type']] = 1;
		} else {
		
			$get_info = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $company_id);
			$company_info = mysqli_fetch_assoc($get_info);
		}	
	
		$c_check = array();
		$c_check[] = array("Test", "test");
		$c_check[] = array("Demo", "demo");
		$c_check[] = array("Reseller", "reseller");
		$c_check[] = array("Client", "");
		
		$display = "";
		
		$check_client = " CHECKED";
		foreach ($c_check as $check) {
			$checked = "";
			if ($company_info[$check[1]] == 1) {
				$checked = " CHECKED";
				$check_client = "";
			}
			if ($check[0] == "Client") {
				$checked = $check_client;
			}
			//$display .= "<td align='right' style='padding:0px 2px 0px 15px'><input name='account_type' type='radio' value='".$check[1]."'$checked onchange='document.getElementById(\"radio_panel\").submit();'></td><td align='left' style='padding:0px 10px 0px 0px'>".$check[0]."</td>";
			$display .= "<td align='right' style='padding:0px 2px 0px 15px'><input name='account_type' type='radio' value='".$check[1]."'$checked onchange='showSelector(\"comp\", \"$company_id\", \"0\", \"\", \"account_type=".$check[1]."&make_change=1\");'></td><td align='left' style='padding:0px 10px 0px 0px'>".$check[0]."</td>";
		}
		
	} else if ($selector_type == "loc") {

		$lavu_query_should_log = false;

		$loc_id = $_REQUEST['loc_id'];
		
		$get_data_name = mlavu_query("SELECT `data_name` FROM `poslavu_MAIN_db`.`restaurants` WHERE `id` = '[1]'", $company_id);
		$company_info = mysqli_fetch_assoc($get_data_name);
		$data_name = $company_info['data_name'];

		$table = "";
		$set_setting = "0";
		$setting_info = array();
		$type = $_REQUEST['type'];

		if (in_array($type, array("gateway_debug"))) {
		
			$table = "locations";	
			
		} else if (in_array($type, array("api_debug", "lds_debug"))) {
			
			$table = "config";
		}
		
		if (strlen($table) > 0) {
	
			if (isset($_REQUEST['make_change'])) {

				$set_setting = $_REQUEST['set_'.$type];
				if ($table == "locations") {

					$make_change = mlavu_query("UPDATE `poslavu_[1]_db`.`locations` SET `[2]` = '[3]' WHERE `id` = '[4]'", $data_name, $type, $set_setting, $loc_id);
	
				} else if ($table == "config") {
	
					$check_existing = mlavu_query("SELECT `id` FROM `poslavu_[1]_db`.`config` WHERE `location` = '[2]' AND `type` = 'location_config_setting' AND `setting` = '[3]'", $data_name, $loc_id, $type);
					if (mysqli_num_rows($check_existing) > 0) {
						$info = mysqli_fetch_assoc($check_existing);
						$update_exisiting = mlavu_query("UPDATE `poslavu_[1]_db`.`config` SET `value` = '[2]' WHERE `id` = '[3]'", $data_name, $set_setting, $info['id']);	
					} else {
						$create_setting = mlavu_query("INSERT INTO `poslavu_[1]_db`.`config` (`location`, `type`, `setting`, `value`) VALUES ('[2]', 'location_config_setting', '[3]', '[4]')", $data_name, $loc_id, $type, $set_setting);
					}
				}
			}
			
			if ($table == "locations") {
				
				$get_info = mlavu_query("SELECT `[1]` FROM `poslavu_[2]_db`.`locations` WHERE `id` = '[3]'", $type, $data_name, $loc_id);
				$setting_info = mysqli_fetch_assoc($get_info);
				$set_setting = $setting_info[$type];
				
			} else if ($table == "config") {
				
				$set_setting = "0";
				$get_info = mlavu_query("SELECT `value` AS `[1]` FROM `poslavu_[2]_db`.`config` WHERE `location` = '[3]' AND `type` = 'location_config_setting' AND `setting` = '[4]'", $type, $data_name, $loc_id, $type);
				if (mysqli_num_rows($get_info) > 0) {
					$setting_info = mysqli_fetch_assoc($get_info);
					$set_setting = $setting_info[$type];	
				}
			}
		}
		
		$display = "";
		$checked = "";
		if ($set_setting == "1") {
			$set_setting = "0";
			$checked = " CHECKED";		
		} else {
			$set_setting = "1";
		}
		
		$display .= "<td align='right' style='padding:0px 2px 0px 15px'><input type='checkbox' value='".(($set_setting=="1")?"0":"1")."'$checked onchange='showSelector(\"loc\", \"$company_id\", \"$loc_id\", \"".$type."\", \"set_".$type."=".$set_setting."&make_change=1\");'></td><td align='left' style='padding:0px 10px 0px 0px'>".$check[0]."</td>";
	}
	
?>

<html>
	<head>
		<style type="text/css">
			body, table { background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; }
		</style>
	</head>

	<body>
		<form id='radio_panel' action='' method='post'>
			<table>
				<tr><?php echo $display; ?></tr>
			</table>
			<input type='hidden' name='make_change' value='1'>
			<input type='hidden' name='type' value='<?php echo $type; ?>'>
			<input type='hidden' name='selector_type' value='<?php echo $selector_type; ?>'>
		</form>
	</body>
</html>