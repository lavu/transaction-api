<?php	
	ini_set("display_errors","1");
	
	function return_field_size($field)
	{
		$fsize = "regular";
		$small_types = array("TINYINT");
		$long_types = array("textarea","htmlarea","checkbox","list","filelist","log","hidden_log","list_parenedit","hidden_long","price_modifier","modifier_exceptions","superadmin_htmlsetting","superadmin_textsetting","setupadmin_html","directive","funjump_order_options");
		for($i=0; $i<count($small_types); $i++)
		{
			if($field==$small_types[$i])
				$fsize = "small";
		}
		for($i=0; $i<count($long_types); $i++)
		{
			if($field==$long_types[$i])
				$fsize = "long";
		}
		return $fsize;
	}
	
	function check_field_on_table($name, $type, $table_name, $cmdline=false)
	{
		global $table_prefix;
		$str = "";
		
		if(strpos($table_name,"`")===false)
			$table_name = "`".$table_name."`";
				
		if($type!="title" && $type!="submit" && $type!="command")
		{
			$found=0;
			$fsize = return_field_size($type);
			$current_fieldtype = "";
			$field_query = mlavu_query("SHOW columns FROM $table_name");
			while($field_read = mysqli_fetch_assoc($field_query))
			{
				if($field_read['Field']==$name) 
				{
					$found=1;
					$current_fieldtype = $field_read['Type'];
				}
			}
			if(!$found)
			{
				/*else if($type=="textarea"||$type=="htmlarea"||$type=="checkbox"||$type=="list"||$type=="filelist"||$type=="log"||$type=="hidden_log"||$type=="list_parenedit"||$type=="hidden_long"||$type=="price_modifier"||$type=="modifier_exceptions"||$type=="superadmin_htmlsetting"||$type=="superadmin_textsetting"||$type=="superadmin_html"||$type=="directive"||$type=="funjump_order_options")*/
				$set_query = "ALTER TABLE $table_name ADD `$name` ";
				if($fsize=="small")
					$set_query .= "TINYINT NOT NULL;";
				else if($fsize=="long")
					$set_query .= "TEXT NOT NULL;";
				else
					$set_query .= "VARCHAR(255) NOT NULL;";
				mlavu_query($set_query);
				
				if($cmdline) $str .= "\n";
				$str .= "Column added to ".$table_name.": $name";
				
				if($table_name==$table_prefix . "orders")
				{
					mlavu_query(str_replace("`".$table_prefix."orders`","`".$table_prefix."orders_archive`",$set_query));
					$str .= " (added to archive also)";
				}
				else if($table_name==$table_prefix . "schedule")
				{
					mlavu_query(str_replace("`".$table_prefix."schedule`","`".$table_prefix."schedule_archive`",$set_query));
					$str .= " (added to archive also)";
				}
				if(!$cmdline) $str .= "<br>";
			}
			else if($found)// && isset($_GET['test']))
			{
				$fset['small'] = "tinyint(4)";
				$fset['regular'] = "varchar(255)";
				$fset['long'] = "text";
				$fset['huge'] = "blob";
				
				$change_type = false;
				if($current_fieldtype!=$fset[$fsize])
				{
					if($fsize=="huge")
						$change_type = true;
					else if($fsize=="long" && $current_fieldtype!=$fset['huge'])
					{
						$queryset = "TEXT";
						$change_type = true;
					}
					else if($fsize=="regular" && $current_fieldtype!=$fset['long'] && $current_fieldtype!=$fset['huge'])
					{
						$queryset = "VARCHAR( 255 )";
						$change_type = true;
					}
					else if($fsize=="small" && $current_fieldtype!=$fset['regular'] && $current_fieldtype!=$fset['long'] && $current_fieldtype!=$fset['huge'])
						$change_type = true;
					if($change_type)
					{
						$alter_cmd = "ALTER TABLE $table_name CHANGE `$name` `$name`";
						$use_charset = "CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL";
						$query = "$alter_cmd $queryset $use_charset";
						//echo "<br>".$name . " - change to " . $fset[$fsize] . " from " . $current_fieldtype;
						mlavu_query($query);
						//echo $query . "<br>";
					}
				}
			}
		}
		return $str;
	}
	
	function check_table($table_name, $fields, $index_title, $index_type, $cmdline=false, $db="")
	{		
		$str = "";
		if($table_name!="")
		{
		   $found=0;
		   if($db=="")
		   {
			   $table_query=mlavu_query("SHOW TABLES");
		   }
		   else
		   {
			   mlavu_query("CREATE DATABASE IF NOT EXISTS `$db`");
			   $table_query=mlavu_query("SHOW TABLES IN `$db`");
		   }
		   
		   while($table_read=mysqli_fetch_row($table_query))
			{if($table_read[0]=="$table_name") $found=1;}
		   if($db=="")
		   {
			   $table_name = "`".$table_name."`";
		   }
		   else
		   {
			   $table_name = "`".$db."`.`".$table_name."`";
		   }
		   
		   if($found)
			{
				for($x = 0; $x < count($fields); $x++)
				{
					$name = $fields[$x][$index_title];
					$type = $fields[$x][$index_type];
	
					$str .= check_field_on_table($name, $type, $table_name);
				}
				$str .= check_field_on_table("_deleted", "TINYINT", $table_name, $cmdline);
				return $str;
			}
		   else
			{
				$create_string="CREATE TABLE $table_name (";
				for($x = 0; $x < count($fields); $x++)
					{
						$name = $fields[$x][$index_title];
						$type = $fields[$x][$index_type];
						if($type=="textarea"||$type=="htmlarea"||$type=="checkbox"||$type=="list"||$type=="filelist"||$type=="log"||$type=="hidden_log"||$type=="list_parenedit"||$type=="hidden_long"||$type=="price_modifier"||$type=="superadmin_htmlsetting"||$type=="superadmin_textsetting"||$type=="superadmin_html"||$type=="directive"||$type=="funjump_order_options")
							$create_string.=" `$name` TEXT NOT NULL,";
						else if($type!="title" && $type!="submit" && $type!="command")
							$create_string.=" `$name` VARCHAR(255) NOT NULL,";
					}
				$create_string.=" `_order` INT NOT NULL, `_deleted` TINYINT NOT NULL, `id` INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(`id`));";
				
				mlavu_query($create_string);
				if($cmdline) $str .= "\n"; else $str .= "<b>";
				$str .= "Table Creation executed at ".date("Y/m/d H:i:s").": ".$table_name."<br>";
				if(!$cmdline) $str .= "</b><br><br>";
				return $str;
			}
		}
		return false;
	}

	function check_table_config($table_name, $tfields, $db)
	{
		$index_title = 0;
		$index_type = 1;
		
		$fields = array();
		for($i=0; $i<count($tfields); $i++)
		{
			if(count($tfields[$i]) > 1)
			{
				$field_setname = $tfields[$i][0];
				$ftype = strtolower($tfields[$i][1]);
				if($ftype=="long"||$ftype=="large") $field_settype = "textarea";
				else if($ftype=="short"||$ftype=="small") $field_settype = "TINYINT";
				else $field_settype = "text";
				$fields[] = array($field_setname,$field_settype);
			}
		}
		
		return check_table($table_name, $fields, $index_title, $index_type, false, $db);
	}

	function config_tables($config_file, $db)
	{
		$output = "";
		
		$fp = fopen($config_file,"r");
		if($input = fread($fp,filesize($config_file)))
		{
			fclose($fp);
			$tables = array();
			
			$parts = explode("}",$input);
			for($i=0; $i<count($parts) - 1; $i++)
			{
				$tinfo = explode("{",$parts[$i]);
				if(count($tinfo) > 1)
				{
					$tname = trim($tinfo[0]);
					$fields = array();
					$final_parts = array();
					$field_parts = explode(Chr(13),$tinfo[1]);
					for($x=0; $x<count($field_parts); $x++)
					{
						$fparts = explode(Chr(10),$field_parts[$x]);
						for($z=0; $z<count($fparts); $z++)
						{
							if($fparts[$z]!="") $final_parts[] = $fparts[$z];
						}
					}
					$field_parts = $final_parts;
					for($n=0; $n<count($field_parts); $n++)
					{
						$field_info = trim($field_parts[$n]);
						if($field_info!="")
						{
							$field_info = explode(":",$field_info);
							if(count($field_info) > 1)
								$field_type = trim($field_info[1]);
							else
								$field_type = "regular";
							$field_name = trim($field_info[0]);
							
							$fields[] = array($field_name,$field_type);
						}
					}
					$tables[] = array($tname,$fields);
				}
			}
			
			for($i=0; $i<count($tables); $i++)
			{
				$tablename = $tables[$i][0];
				$fields = $tables[$i][1];
				
				$output .= check_table_config($tablename, $fields, $db);
				/*for($n=0; $n<count($fields); $n++)
				{
					$field = $fields[$n];
					$fieldname = $field[0];
					$fieldtype = $field[1];
				}*/
			}
		}
		else $output .= "<br>Error: could not open table configuration file<br>";
		
		return $output;
	}
?>
