<?php
	session_start();
	$loggedin_access = (isset($_SESSION['posadmin_access']))?$_SESSION['posadmin_access']:false;
	if (!function_exists("can_access")) {
		require_once(dirname(__FILE__).'/../manage/can_access.php');
	}
	if (can_access("customers")) {
		set_time_limit(0);
		ini_set('display_errors',0);
		echo "These counts include all accounts marked as active within the past 5 days:";
		$web2 = 0;
		$web2_empty = 0;
		$lavuCloud = 0;
		$cloud1 = 0;
		$cloud3 = 0;
		$dcloud = 0;
		$lls = 0;
		$lavu_lite = 0;
		$stacked = 0;
		$get_restaurants = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `notes` NOT LIKE '%AUTO-DELETED%' AND `last_activity` >= '".date("Y-m-d H:i:s", (time() - 432000))."' AND `data_name` != 'DEV'");
		while ($company_info = mysqli_fetch_assoc($get_restaurants)) {
			if ($company_info['lavu_lite_server'] != ''){
				$lavu_lite++;
			}else {
				$get_locations = mlavu_query("SELECT `use_net_path`, `net_path` FROM `poslavu_".$company_info['data_name']."_db`.`locations` WHERE `_disabled` = '0'");
				if (mysqli_num_rows($get_locations) > 1){
					$stacked++;
				}
				while ($loc_info = mysqli_fetch_assoc($get_locations)) {
					if ($loc_info['use_net_path'] > 0) {
						if (strstr($loc_info['net_path'], "dcloud")) $dcloud++;
						else if (strstr($loc_info['net_path'], "cloud.")) $lavuCloud++;
						else if (strstr($loc_info['net_path'], "cloud1")) $cloud1++;
						else if (strstr($loc_info['net_path'], "cloud3")) $cloud3++;
						else if (strstr($loc_info['net_path'], "admin")) $web2++;
						else $lls++; 
					} else {
						$web2_empty++;
					}
				}
			}
		}
		echo "<br><br>&nbsp;WEB2: ".$web2;
		echo "<br>&nbsp;WEB2 (use_net_path = 0): ".$web2_empty;
		echo "<br>&nbsp;LAVU-CLOUD: ".$lavuCloud;
		echo "<br>&nbsp;CLOUD1: ".$cloud1;
		echo "<br>&nbsp;CLOUD3: ".$cloud3;
		echo "<br>&nbsp;DCLOUD: ".$dcloud;
		echo "<br>&nbsp;LLS: ".$lls;
		echo "<br>&nbsp;Lavu Lite: ".$lavu_lite;
		echo "<br>&nbsp;Stacked Locations: ".$stacked;
	}
?>
