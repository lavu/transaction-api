<?php
	
	function process_fix($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		process_fix_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		fix_blank_taxes_for_order_contents($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		process_fix_voided_contents_tax($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		process_fix_order_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		// process_fix_auto_gratuity($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		process_fix_itax($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
		//process_fix_tax($date_reports_by,$date_start,$date_end,$locationid,$order_id,$special_query);
	}
	
	function process_fix_all($date_reports_by,$date_start,$date_end,$locationid)
	{
		return process_fix($date_reports_by,$date_start,$date_end,$locationid);
	}

	function process_fix_order($order_id,$locationid="")
	{
		return process_fix("","","",$locationid,$order_id);
	}

	$explain_done = false;
	function process_explain_mismatches($date_reports_by,$date_start,$date_end,$locationid)
	{
		global $explain_done;
		if($explain_done)
		{
			return;
		}
		else
		{
			$explain_done = true;
		}
		
		//process_fix_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid);
		process_ag_data($date_reports_by,$date_start,$date_end,$locationid);
		
		$order_query = lavu_query("select * from `orders` where `[1]` >= '[2]' and `[1]` < '[3]' and `location_id`='[4]' and `void`!='1'",$date_reports_by,$date_start,$date_end,$locationid);
		while($order_query !== FALSE && $order_read = mysqli_fetch_assoc($order_query))
		{
			$idiscount = $order_read['idiscount_amount'];
			$subtotal = numformat($order_read['subtotal'] - $order_read['itax']);
			$discount = $order_read['discount'];
			$tax = $order_read['tax'] - $order_read['itax'];
			$total_before_ag = $order_read['total'] - $order_read['gratuity'];
			$ag_total = $order_read['gratuity'];
			$ag_card = $order_read['ag_card'];
			$ag_cash = $order_read['ag_cash'];
			$total = $order_read['total'];
			$cash_paid = $order_read['cash_applied'];
			$card_paid = $order_read['card_paid'];
			$alt_paid = $order_read['alt_paid'] + $order_read['gift_certificate'];
			$cash_tip = $order_read['cash_tip'];
			$card_tip = $order_read['card_gratuity'];
			$order_id = $order_read['order_id'];
			$order_opened = $order_read['opened'];
			$order_closed = $order_read['closed'];
			
			$extra_order_info = " <font color='#bbbbbb'>($order_id opened: $order_opened closed: $order_closed)</font>";
			
			$content_query = lavu_query("select sum(`tax_amount` + `itax`) as `tax`,sum((`price` * `quantity`) - `itax`) as `item_total`, sum((`modify_price` + `forced_modifiers_price`) * `quantity`) as `modifiers`, sum(`idiscount_amount`) as `idiscount`, `discount_amount` as `check_discount`, sum(`after_discount`) as `after_discount`, sum(`subtotal_with_mods` - `idiscount_amount` - `itax`) as `subtotal`, sum(`after_discount` + `discount_amount` - `itax`) as `subtotal2`, sum(`subtotal_with_mods` - `discount_amount` + `tax_amount` - `idiscount_amount`) as `total` from `order_contents` where `order_id`='[1]' and `loc_id`='[2]'",$order_read['order_id'], $locationid);
			if($content_query !== FALSE && mysqli_num_rows($content_query))
			{
				$content_read = mysqli_fetch_assoc($content_query);
				$contents_tax = $content_read['tax'];
				$contents_item_total = $content_read['item_total'];
				$contents_modifiers = $content_read['modifiers'];
				$contents_idiscount = $content_read['idiscount'];
				$contents_subtotal = numformat($content_read['subtotal']);
				$contents_check_discount = $content_read['check_discount'];
				$contents_after_discount = $content_read['after_discount'];
				$contents_total = $content_read['total'];

				if($contents_subtotal != $subtotal)
				{
					echo "Content Mismatch: contents:$contents_subtotal = order:$subtotal $extra_order_info<br>";
				}
			}
			
			$trans_totals = array();
			$trans_totals['cash'] = 0;
			$trans_totals['card'] = 0;
			$trans_totals['gc'] = 0;
			$trans_totals['other'] = 0;
			
			/*$trans_query = lavu_query("select `pay_type`,`action`,`voided`,`card_type`, IF(`voided`='1',0,IF(`action`='Refund', (0 - sum(`total_collected` + `tip_amount`)), sum(`total_collected` + `tip_amount`))) as `amount` from `cc_transactions` where `order_id`='[1]' group by concat(`pay_type`,' ',`card_type`,`action`,`voided`,IF(`cc_transactions`.`order_id` LIKE 'Paid%','PaidInOut','')) as `line_group`",$order_read['order_id']);
			while($trans_read = mysqli_fetch_assoc($trans_query))
			{
				$pay_amount = numformat($trans_read['amount']);
				$pay_type = $trans_read['pay_type'];
				if($pay_type=="Cash") $use_type = "cash";
				else if($pay_type=="Card") $use_type = "card";
				else if($pay_type=="Gift Certificate") $use_type = "gc";
				else $use_type = "Other";
				$trans_totals[$use_type] += $pay_amount;
			}
			
			if($trans_totals['cash']!=$cash_paid) echo "Pay Mismatch: Cash:" . $trans_totals['cash'] . " order:$cash_paid $extra_order_info<br>";
			if($trans_totals['card']!=$card_paid) echo "Pay Mismatch: Card:" . $trans_totals['card'] . " order:$card_paid $extra_order_info<br>";
			//if($trans_totals['gc']!=$gift_paid) echo "Pay Mismatch: GC:" . $trans_totals['gc'] . " order:$gift_paid $extra_order_info<br>";
			if($trans_totals['other']!=$alt_paid) echo "Pay Mismatch: Other:" . $trans_totals['other'] . " order:$alt_paid $extra_order_info<br>";*/
		}
	}
	
	function process_fix_transaction_to_order_mismatch($date_reports_by,$date_start,$date_end,$locationid)
	{
		$query = "select orders.order_id, orders.card_paid, format(sum(IF(`cc_transactions`.`action`='Refund',0-cc_transactions.total_collected,cc_transactions.total_collected)),2) as `added`, format(abs(sum(IF(cc_transactions.action='Refund',0-cc_transactions.total_collected,cc_transactions.total_collected)) - orders.card_paid),2) as `offset` from orders left join cc_transactions on orders.order_id = cc_transactions.order_id where `orders`.`[1]` >= '[2]' and orders.opened <= '2012-10-31 04:00:00' and cc_transactions.pay_type='Card' and cc_transactions.voided < 1 and cc_transactions.action != 'Void' and cc_transactions.action != '' group by orders.order_id order by offset desc limit 10;";
	}
	
	function process_fix_auto_gratuity($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		if($special_query!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`order_id` = '[5]' and `location_id`='[4]')";
		else
			$where_cond = "(`[1]`>'[2]' and `[1]`<'[3]' and location_id='[4]')";
			
		$query = "select order_id,subtotal,discount,gift_certificate,tax,gratuity,format((subtotal - discount + tax + gratuity),2) as `added`,total,opened,closed,cash_paid,cash_applied,card_paid from orders where $where_cond and format((subtotal - discount + tax + gratuity),2)!=format(`total`,2) and void < 1";
		
		$order_query = lavu_query($query,$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		while($order_query !== FALSE && $order_read = mysqli_fetch_assoc($order_query))
		{
			$amount_paid = $order_read['cash_applied'] + $order_read['card_paid'] + $order_read['gift_certificate'] + $order_read['alt_paid'];
			if(isset($_GET['tt'])) echo "FIX AUTO GRAT: " . $order_read['order_id'] . " - added (order fields): " . number_format($order_read['added'],2) . " current order total: ".number_format($order_read['total'],2) . " paid: ".number_format($amount_paid,2)." current_ag: " . number_format($order_read['gratuity'],2) . "<br>";
			if(number_format($amount_paid * 1,2)==number_format($order_read['total'],2))
			{
				$diff = str_replace(",","",number_format($order_read['total'] * 1 - ($order_read['subtotal'] - $order_read['discount'] + $order_read['tax']) * 1,2));
				if($diff * 1 >= 0)
				{
					if(isset($_GET['tt'])) echo "applying gratuity: ".$diff." as fix<br>";
					lavu_query("update `orders` set `gratuity`='[1]',`ag_cash`='',`ag_card`='',`ag_other`='' where `order_id`='[2]' and `location_id`='[3]' limit 2",$diff,$order_read['order_id'],$locationid);
				}
				else if($diff * 1 < 0 && ($order_read['discount'] * 1 == 0 || $order_read['discount']=="")) // If difference is negative make it a discount instead
				{
					if(isset($_GET['tt'])) echo "applying discount: ".abs($diff)." as fix<br>";
					lavu_query("update `orders` set `discount`='[1]' where `order_id`='[2]'",abs($diff),$order_read['order_id']); 
				}
				//echo "Paid: $amount_paid Total: " . $order_read['total'] . " Diff: ".$diff."<br>";
			}
			else if(number_format(abs($amount_paid * 1 - $order_read['added']),2) < .08)
			{
				if(isset($_GET['tt'])) echo "Total for " . $order_read['order_id'] . " should be " . number_format($order_read['added'],2) . " (it is currently ".number_format($order_read['total'],2).")<br>";
				lavu_query("update `orders` set `total`='[1]' where `order_id`='[2]' and `location_id`='[3]' limit 2",$order_read['added'],$order_read['order_id'], $locationid);
			}
			else if(trim((number_format($order_read['total'],2) + number_format($order_read['gratuity'],2)))==trim(number_format($amount_paid,2)) && number_format($amount_paid,2) > 0)
			{
					if(isset($_GET['tt'])) echo "fix by setting total to added + ag<br>";
					$set_total = trim(number_format($amount_paid,2));
					$set_order_id = $order_read['order_id'];
					lavu_query("update `orders` set `total`='[1]' where `order_id`='[2]'",$set_total,$set_order_id);
			}
			else
			{
					if(isset($_GET['tt'])) 
					{
						echo "not sure how to fix<br>";
						echo "total + ag = " . (number_format($order_read['total'],2) + number_format($order_read['gratuity'],2)) . "<br>";
						echo "amount_paid = " . number_format($amount_paid,2) . "<br>";
					}
			}
		}
	}

	function process_fix_tax($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query=""){
		$params = array(
			'loc_id' => $locationid,
			'date_reports_by' => $date_reports_by,
			'date_start' => $date_start,
			'date_end' => $date_end,
			'order_id' => $order_id
		);

		global $location_info;
		$no_of_decimals = $location_info['disable_decimal']*1;
		$where_cond = "";

		if(trim($special_query)!=""){
			$where_cond = "({$special_query})";
		} else if($order_id!=""){
			$where_cond = "(`orders`.`order_id` ='[order_id]' AND `orders`.`location_id`='[loc_id]')";
		} else {
			$where_cond = "(`[date_reports_by]`>'[date_start]' AND `[date_reports_by]`<'[date_end]' AND `orders`.`location_id`='[loc_id]')";
		}

		$query = "SELECT `orders`.`id`, `order_id`, ROUND(SUM(`order_contents`.`tax_amount`),{$no_of_decimals}) as `tax_amount`, `orders`.`tax` as `tax` FROM `orders` JOIN `order_contents` USING (`order_id`) WHERE {$where_cond} GROUP BY `order_id` HAVING ROUND(SUM(`order_contents`.`tax_amount`),{$no_of_decimals}) != ROUND(`orders`.`tax`,{$no_of_decimals})";
		if( isset($_GET['test45']) ){
			$query_test = $query;
			foreach( $params as $key => $param ){
				$query_test = str_ireplace("[{$key}]", $param, $query_test);
			}

			echo $query_test;
		}
		$order_details_query = lavu_query($query, $params);
		if( $order_details_query !== FALSE && mysqli_num_rows( $order_details_query ) ){
			if(isset($_GET['test45'])){
				echo "Num Rows: " . mysqli_num_rows( $order_details_query );
			}
			echo "Tax MisMatch<br />";
			while( $order_details = mysqli_fetch_assoc( $order_details_query )) {
				$update_query = "UPDATE `orders` SET `tax` = [tax_amount] WHERE `id` = [id]";
				lavu_query( $update_query, $order_details );
			}
		}
	}

	function process_fix_itax($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query=""){
		$params = array(
			'loc_id' => $locationid,
			'date_reports_by' => $date_reports_by,
			'date_start' => $date_start,
			'date_end' => $date_end,
			'order_id' => $order_id
		);
		global $location_info;
		$no_of_decimals = $location_info['disable_decimal']*1;
		$where_cond = "";

		if(trim($special_query)!=""){
			$where_cond = "({$special_query})";
		} else if($order_id!=""){
			$where_cond = "(`orders`.`order_id` ='[order_id]' AND `order_contents`.`loc_id`='[loc_id]')";
		} else {
			$where_cond = "(`[date_reports_by]`>'[date_start]' AND `[date_reports_by]`<'[date_end]' AND `order_contents`.`loc_id`='[loc_id]')";
			
		}

		$query = "SELECT `order_contents`.* FROM `order_contents` LEFT JOIN `orders` ON `orders`.`order_id` = `order_contents`.`order_id` WHERE {$where_cond} AND `order_contents`.`itax`*1 != 0 AND ((`order_contents`.`itax_rate`*1 != (`order_contents`.`tax_rate1`*1 + `order_contents`.`tax_rate2`*1 + `order_contents`.`tax_rate3`*1)) OR (`order_contents`.`itax`*1 != (`order_contents`.`tax1`*1 + `order_contents`.`tax2`*1 + `order_contents`.`tax3`*1)))";

		$order_contents_query = lavu_query($query, $params);
		if($order_contents_query && mysqli_num_rows($order_contents_query) ){
			echo "iTax Mismatch<br />";
			while( $order_contents_query !== FALSE && $order_contents_read = mysqli_fetch_assoc($order_contents_query) ){
				if($order_contents_read['tax_amount']*1 == 0){
					// Just update the order_contents tax rates/ tax dollars, and don't update the prices, or the actual tax field
					$itax_rate = $order_contents_read['tax_rate1'] + $order_contents_read['tax_rate2'] + $order_contents_read['tax_rate3'];
					$itax = round($itax_rate * $order_contents_read['after_discount'], $no_of_decimals );
					$itax_acc = $itax;
					if ($itax_rate == 0.0) {
						$tax_percentage1 = 0;
						$tax_percentage2 = 0;
						$tax_percentage3 = 0;
					} else {
						$tax_percentage1 = $order_contents_read['tax_rate1'] / $itax_rate;
						$tax_percentage2 = $order_contents_read['tax_rate2'] / $itax_rate;
						$tax_percentage3 = $order_contents_read['tax_rate3'] / $itax_rate;
					}
					$tax1 = round( $tax_percentage1 * $itax, $no_of_decimals );
					$itax_acc -= $tax1;
					$tax2 = round( $tax_percentage2 * $itax, $no_of_decimals );
					$itax_acc -= $tax2;
					$tax3 = round( $tax_percentage3 * $itax, $no_of_decimals );
					$itax_acc -= $tax3;

					if($itax_acc > 0){
						$itax1 += $itax_acc;
					}

					$order_content_id = $order_contents_read['id'];

					$tax_params = array(
						"tax1" => $tax1,
						"tax2" => $tax2,
						"tax3" => $tax3,
						"itax" => $itax,
						"itax_rate" => $itax_rate,
						"id" => $order_content_id
					);

					lavu_query("UPDATE `order_contents` SET `tax1`='[tax1]', `tax2`='[tax2]', `tax3`='[tax3]', `itax_rate`='[itax_rate]', `itax`='[itax]' WHERE `id`='[id]' LIMIT 1", $tax_params);

				} else {
					///TODO MAKE THIS FIX STUFF
				}
			}
		}
	}
	
	function test_manual_tax_calculation($date_reports_by,$date_start,$date_end,$locationid)
	{
		$total_tax = 0;
		$contents_tax_total = 0;
		echo "Test manual for: $date_start to $date_end - $date_reports_by - $locationid<br>";
		$order_query = lavu_query("select `order_id`,`tax` from `orders` where `location_id`='[4]' and `[1]`>='[2]' and `[1]`<='[3]' and void < 1 order by order_id asc",$date_reports_by,$date_start,$date_end,$locationid);
		if($order_query)
		{
			while( $order_query !== FALSE && $order_read = mysqli_fetch_assoc($order_query))
			{
				//echo "tax for " . $order_read['order_id'] . ": " . $order_read['tax'] . "<br>";
				$total_tax += ($order_read['tax'] * 1);
				
				$contents_tax = 0;
				$contents_query = lavu_query("select `tax_amount` from `order_contents` where `order_id`='[1]' and `loc_id`='[3]'and `quantity`>0",$order_read['order_id'],$locationid);
				while($contents_query !== FALSE && $contents_read = mysqli_fetch_assoc($contents_query))
				{
					$contents_tax += $contents_read['tax_amount'] * 1;
				}
				
				$diff = abs(($contents_tax * 1) - ($order_read['tax'] * 1));
				if($diff >= 0.01)
				{
					echo "Tax mismatch: " . $order_read['order_id'] . " - $contents_tax - ". $order_read['tax'] . "<br>";
				}
				else if($diff > 0.001)
				{
					echo "Partial Tax mismatch: " . $order_read['order_id'] . " - $contents_tax - ". $order_read['tax'] . "<br>";
				}
				$contents_tax_total += $contents_tax;
			}
		}
		else
		{
			lavu_dberror();
		}
		echo "Total Tax: $total_tax<br>";
		echo "Contents Tax: $contents_tax_total<br>";
	}
	
	function process_fix_voided_contents_tax($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		//if(isset($_GET['tt'])) test_manual_tax_calculation($date_reports_by,$date_start,$date_end,$locationid);
		
		if($special_query!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[5]' and `orders`.`location_id`='[5]' and `order_contents`.`loc_id` = `orders`.`location_id`)";
		else
			$where_cond = "(`orders`.`[1]` >= '[2]' and `orders`.`[1]` < '[3]' and `orders`.`location_id`='[4]' and `order_contents`.`loc_id` = `orders`.`location_id`)";
		
		$mm_query = lavu_query("select orders.order_id, orders.tax as `order_tax`,  sum(order_contents.tax_amount) as `contents_tax`, abs(orders.tax * 1 - sum(`order_contents`.`tax_amount`)) as `offset` from orders left join order_contents on orders.order_id=order_contents.order_id where $where_cond and `orders`.`void` < 1 and `order_contents`.`quantity` > 0 group by `orders`.`order_id` order by `offset` desc limit 20",$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		while( $mm_query !== FALSE && $mm_read = mysqli_fetch_assoc($mm_query))
		{
			if($mm_read['offset'] >= 0.01)
			{
				fix_order_based_on_order_contents("tax mismatch",$mm_read['order_id'],$date_reports_by,$date_start,$date_end,$locationid);
			}
		}
		$mm_query = lavu_query("select orders.order_id, orders.tax as `order_tax`,  sum(order_contents.tax_amount) as `contents_tax`, abs(orders.tax * 1 - sum(`order_contents`.`tax_amount`)) as `offset`, sum(`order_contents`.`quantity`) as `contents_qty` from orders left join order_contents on orders.order_id=order_contents.order_id where $where_cond and `orders`.`void` < 1 group by `orders`.`order_id` order by `contents_qty` asc limit 20",$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		while( $mm_query !== FALSE && $mm_read = mysqli_fetch_assoc($mm_query))
		{
			if($mm_read['contents_qty'] < 1)
			{
				fix_order_based_on_order_contents("tax mismatch",$mm_read['order_id'],$date_reports_by,$date_start,$date_end,$locationid);
			}
		}
	}
	
	function process_fix_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		if($special_query!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(order_id = '[3] AND `loc_id`='[4]')";
		else
			$where_cond = "(server_time >= '[1]' and server_time <= '[2]')";
		
		$success = lavu_query("update `order_contents` set `subtotal_with_mods`=(`quantity` * (`price` + `forced_modifiers_price` + `modify_price`)) where void < 1 and $where_cond and `item`!='SENDPOINT' and ((`quantity` * (`price` + `forced_modifiers_price` + `modify_price`)) != `subtotal_with_mods`) limit 2000",$date_start,$date_end,$order_id, $locationid);
	}
	
	function fix_blank_taxes_for_order_contents($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		if($special_query!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[5]' AND `location_id`='[4]')";
		else
			$where_cond = "(`orders`.`[1]`>='[2]' and `orders`.`[1]`<='[3]' and `orders`.`location_id`='[4]')";
			
		$content_query = lavu_query("select `order_contents`.`id` as `content_id` from `orders` left join `order_contents` on `orders`.`order_id`=`order_contents`.`order_id` where `order_contents`.`apply_taxrate`!='' and `order_contents`.`tax_amount`='' and $where_cond",$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		while( $content_query !== FALSE && $content_read = mysqli_fetch_assoc($content_query))
		{
			lavu_query("update `order_contents` set `tax_amount`=(`quantity` * (`price` + `forced_modifiers_price` + `modify_price`) * `apply_taxrate`) where `id`='[1]'",$content_read['content_id']);
		}
	}
	
	function fix_order_based_on_order_contents($type,$order_id,$date_reports_by,$date_start,$date_end,$locationid)
	{
		$content_query = lavu_query("select sum(`tax_amount` + `itax`) as `tax`,sum(`tax_amount`) as `tax_amount`,sum(`itax`) as `itax`,sum((`price` * `quantity`) - `itax`) as `item_total`, sum((`modify_price` + `forced_modifiers_price`) * `quantity`) as `modifiers`, sum(`idiscount_amount`) as `idiscount`, `discount_amount` as `check_discount`, sum(`after_discount`) as `after_discount`, sum(`subtotal_with_mods` - `idiscount_amount`) as `subtotal`, sum(`after_discount` + `discount_amount` - `itax`) as `subtotal2`, sum(`subtotal_with_mods` - `discount_amount` + `tax_amount` - `idiscount_amount`) as `total` from `order_contents` where `order_id`='[1]' and `loc_id`='[2]' and `quantity` > 0",$order_id, $locationid);
		if($content_query !== FALSE && mysqli_num_rows($content_query))
		{
			$content_read = mysqli_fetch_assoc($content_query);
			$contents_tax = $content_read['tax'];
			$contents_item_total = $content_read['item_total'];
			$contents_modifiers = $content_read['modifiers'];
			$contents_idiscount = $content_read['idiscount'];
			$contents_subtotal = numformat($content_read['subtotal']);
			$contents_check_discount = $content_read['check_discount'];
			$contents_after_discount = $content_read['after_discount'];
			$contents_total = $content_read['total'];
			$contents_tax_amount = $content_read['tax_amount'];
			$contents_itax = $content_read['itax'];

			if($contents_subtotal=="") $contents_subtotal = 0;
			if($contents_total=="") $contents_total = 0;
			if($contents_tax_amount=="") $contents_tax_amount = 0;
			if($contents_itax=="") $contents_itax = 0;
		}
		else
		{
			$contents_subtotal = 0;
			$contents_total = 0;
			$contents_tax_amount = 0;
			$contents_itax = 0;
		}
			
		//echo "MM Content Mismatch: contents:$contents_subtotal = order:$subtotal $extra_order_info<br>";
		$vars = array();
		$vars['order_id'] = $order_id;
		$vars['subtotal'] = $contents_subtotal;
		$vars['total'] = $contents_total;
		$vars['tax'] = $contents_tax_amount;
		$vars['itax'] = $contents_itax;
		$vars['location_id'] = $locationid;
		$query = "update `orders` set `subtotal`='[subtotal]', `tax`='[tax]', `itax`='[itax]', `total`='[total]' where `order_id`='[order_id]' AND `location_id`='[location_id]' limit 2";
		
		if(isset($_GET['test45'])){
			$query_copy = $query;
			foreach( $vars as $key => $param ){
				$query_copy = str_ireplace("[{$key}]", $param, $query_copy);
			}

			echo $query_copy . "<br />";
		}
		$order_query = lavu_query("select * from `orders` where `order_id`='[1]'",$order_id);
		if($order_query !== FALSE && mysqli_num_rows($order_query))
		{
			$order_read = mysqli_fetch_assoc($order_query);
			
			//if(isset($_GET['tt'])) echo "Checking Order: $order_id - $contents_tax_amount - ".$order_read['tax']."<br>";
			if(abs($order_read['tax'] - $contents_tax_amount) > 0.005 || abs($order_read['subtotal'] - $contents_subtotal) > 0.01 || abs($order_read['itax'] - $contents_itax) > 0.01)
			{
				$testing = false;
				if($testing)
				{
					if(isset($_GET['tt']))
					{
						echo "<hr><b>$type</b><br>";
						$display_query = $query;
						foreach($vars as $key => $val) 
						{
							$display_query = str_replace("[".$key."]",ConnectionHub::getConn('rest')->escapeString($val),$display_query);
							echo $key . " = " . $order_read[$key] . "<br>";
						}
						echo $display_query . "<br><br>";
					}
				}
				else if($vars['order_id']!="")
				{
					//echo "Running query: " . $query . "<br>";
					$success = lavu_query($query,$vars);
				}
			}
		}
	}
	
	function process_fix_order_subtotal_mismatches($date_reports_by,$date_start,$date_end,$locationid,$order_id="",$special_query="")
	{
		if($special_query!="")
			$where_cond = "($special_query)";
		else if($order_id!="")
			$where_cond = "(`orders`.`order_id` = '[5]' and `orders`.`location_id`='[4]' and `orders`.`location_id` = `order_contents`.`loc_id`)";
		else
			$where_cond = "(`orders`.`[1]` >= '[2]' and `orders`.`[1]` < '[3]' and `orders`.`location_id`='[4]' and `orders`.`location_id` = `order_contents`.`loc_id`)";
			
		$mm_query = lavu_query("select orders.order_id as `order_id`, orders.subtotal as `order_subtotal`, `orders`.`tax` as `order_tax`, sum(`order_contents`.`tax_amount`) as `contents_tax`,  sum((order_contents.price + order_contents.forced_modifiers_price + order_contents.modify_price) * order_contents.quantity - order_contents.idiscount_amount - order_contents.itax) as `contents_subtotal`, abs(orders.subtotal * 1 - sum((order_contents.price + order_contents.forced_modifiers_price + order_contents.modify_price) * order_contents.quantity - order_contents.idiscount_amount - order_contents.itax)) as `offset`, abs(orders.tax * 1 - sum(order_contents.tax_amount)) as `tax_offset` from orders left join order_contents on orders.order_id=order_contents.order_id where $where_cond and `orders`.`void` < 1 group by `orders`.`order_id` order by `offset` desc, `tax_offset` desc limit 20",$date_reports_by,$date_start,$date_end,$locationid,$order_id);
		while($mm_query !== FALSE && $mm_read = mysqli_fetch_assoc($mm_query))
		{
			if($mm_read['offset'] >= 0.01)
			{
				//echo $mm_read['order_id'] . ": " . $mm_read['order_subtotal'] . "/" . $mm_read['contents_subtotal'] . "<br>";
				$subtotal = $mm_read['order_subtotal'];
				
				fix_order_based_on_order_contents("subtotal mismatch",$mm_read['order_id'],$date_reports_by,$date_start,$date_end,$locationid);
			}
			if($mm_read['tax_offset'] >= 0.005)
			{
				if(isset($_GET['tt'])) echo "Found tax mismatch for order " . $mm_read['order_id'] . ": " . number_format($mm_read['tax_offset'],2) . " ".number_format($mm_read['order_tax'],2)."/".number_format($mm_read['contents_tax'],2)."<br>";
				fix_order_based_on_order_contents("subtotal mismatch",$mm_read['order_id'],$date_reports_by,$date_start,$date_end,$locationid);
			}
		}
	}
	
	function process_ag_data($date_reports_by,$date_start,$date_end,$locationid)
	{
		global $location_info;
		if( (!isset($location_info['ag_calculation_scheme'])) || $location_info['ag_calculation_scheme'] == '1' ){
			$ag_query = lavu_query("select `id`,`order_id`,`cash_applied`,`gratuity`,`card_paid`,`ag_cash`,`ag_card` from `orders` where `[1]` >= '[2]' and `[1]` < '[3]' and `location_id`='[4]' and `gratuity` > 0 and `gratuity`!='' and ((`cash_applied` > 0 and `ag_cash`='') or (`card_paid` > 0 and `ag_card`=''))",$date_reports_by,$date_start,$date_end,$locationid);
			while($ag_query !== FALSE && $ag_read = mysqli_fetch_assoc($ag_query)) {
				$cash_applied = $ag_read['cash_applied'];
				$use_ag = $ag_read['gratuity'];
				$use_cash_paid = ($cash_applied);
				$use_card_paid = $ag_read['card_paid'];
				$ag_cash = 0;
				$ag_card = 0;
				if($use_cash_paid >= $use_ag)
				{
					$ag_cash = $use_ag;
				}
				else
				{
					$ag_cash = $use_cash_paid;
					$use_ag -= $ag_cash * 1;
					if($use_card_paid >= $use_ag)
					{
						$ag_card = $use_ag;
					}
					else
					{
						$ag_card = $use_card_paid;
						$use_ag -= $ag_card * 1;
					}
				}
				$success = lavu_query("update `orders` set `ag_cash`='[1]', `ag_card`='[2]' where `id`='[3]'",$ag_cash,$ag_card,$ag_read['id']);
				if(isset($_GET['view_ag_process']))
				{
					if($success) echo "Updated ag_cash to $ag_cash and ag_card to $ag_card<br>";
					else echo "Unable to update: ".$ag_read['order_id']."<br>";
				}
			}
			if(isset($_GET['view_ag_process'])) echo "To Process: " . mysqli_num_rows($ag_query) . " ($date_reports_by $date_start $date_end $locationid)<br>";
		} else {
			process_fix_auto_gratuity($date_reports_by,$date_start,$date_end,$locationid);
		}
	}
	
	if (!function_exists("numformat")) {
		
		function numformat($num) {
		
			global $decimal_places;
			
			if (!isset($decimal_places)) $decimal_places = 2;

			return number_format($num, $decimal_places, ".", "");
		}
	}
	
?>
