<?php
	/******************************************************************
	Contains all the methods that handle the DB queries.
	*******************************************************************/
	class messagesDB{
		/******************************************************************
		Handles connection to the DB trough the custom inhouse DB libraries
		
		Params:		---			---
		
		Returns: 	---
		*******************************************************************/
		function __construct(){
			
			if (!isset($_SESSION)) {
				session_start();
			}
			
			ini_set("display_errors",1);
			//echo dirname(__FILE__).'<br/><br/>';
			//require_once($_SERVER['DOCUMENT_ROOT']."/cp/resources/core_functions.php");
			require_once(dirname(__FILE__)."/../../cp/resources/core_functions.php");
			
			$maindb = "poslavu_MAIN_db";
			$data_name = sessvar("admin_dataname");
			$in_lavu = true;
			lavu_auto_login();
		
			$rdb = admin_info("database");
			lavu_connect_byid(admin_info("companyid"),$rdb);
		}
		
		function updateEntry($title, $type, $content, $id){
				
				$query = "UPDATE poslavu_MAIN_db.messages set title=\"[1]\", type=\"[2]\", content=\"[3]\" WHERE id=\"[4]\"";
				$result = mlavu_query($query, $title, $type, $content, $id);
				return $result;
				
		}//updateEntry()
		
		function insertNewMessage($title, $type, $content){
				
				$query = "INSERT INTO poslavu_MAIN_db.messages (title, type, content) VALUES (\"[1]\",\"[2]\",\"[3]\")";
				$res = mlavu_query($query,$title,$type,$content);
				return $res;
		}//insertNewMessage()
		
		function getMessageInfo($id){
			$query = "SELECT * FROM poslavu_MAIN_db.messages WHERE id='".$id."'";//
			$result = mlavu_query($query);
		
			return $result;
		}//queryDB()
		
		function saveNewMessage($title, $type, $content){
			return mlavu_query('insert into poslavu_MAIN_db.messages (title, type, content) values ("'.$title.'". '.$type.', "'.$content.'")');
		}
		
		function getMessage($id){
			$result = array();
			
			$query = mlavu_query('select * from poslavu_MAIN_db.messages where id='.$id);
			
			while ($read = mysqli_fetch_assoc($query)) {
				$result[] = $read;
			}
			
			return $result;
		}
		
		function getMesagges(){
			/* Security !? */
			$result = array();
			
			$query = mlavu_query('select * from poslavu_MAIN_db.messages order by timestamp desc');
        	
			while ($read = mysqli_fetch_assoc($query)) {
				$result[] = $read;
			}
		
			return $result;
		}
		function deleteMessage($id){
			$query = mlavu_query("Delete from poslavu_MAIN_db.messages where `id`= '[1]' ", $id);
			if(mlavu_dberror())
				return mlavu_dberror();
			else
				return "success";

		}
		
		
	}
?>
