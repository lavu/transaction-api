<?php

	$rowid = urlvar("rowid");
	$search_log = (isset($_REQUEST['log_type']))?$_REQUEST['log_type']:"";
	$lls = (isset($_REQUEST['search_lls']))?$_REQUEST['search_lls']:"0";
	$lls_checked = "";
	if ($lls == "1") $lls_checked = " checked";
	$date = (isset($_REQUEST['date']))?$_REQUEST['date']:"";
	$loc_id = (isset($_REQUEST['loc_id']))?$_REQUEST['loc_id']:"0";
	$search_for = (isset($_REQUEST['search_for']))?$_REQUEST['search_for']:"";

	$log_types = array("Admin Actions", "Device Console", "Gateway", "Gateway Comm Vars", "Gateway Debug", "JSON Comm Vars", "MySQL", "Order Sum Corrections");
	$dates = array();
	for ($d = 0; $d < 90; $d++) {
		$dates[] = date("Y-m-d", time() - ($d * 86400));
	}

	$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$rowid);
	if(mysqli_num_rows($cust_query)) {
		$cust_read = mysqli_fetch_assoc($cust_query);
		
		echo "<style>";
		echo ".label { text-align:right; color:#999999; vertical-align:top; } ";
		echo "</style>";
		echo "<br><a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";
		echo "<form name='form1' method='post' action='index.php?mode=manage_customers&submode=old_search_logs&rowid=$rowid'>";
		echo "<table cellpadding=12><td style='border:solid 1px black' valign='top'>";
		
		echo "<div style='float:left; display:block;'>";
		echo "<table>";
		echo "<tr><td class='label label-info'>Company Name: </td><td width='200'>" . $cust_read['company_name'] . "</td><td class='label label-info'>Data Name: </td><td>" . $cust_read['data_name'] . "</td></tr>";
		echo "<tr><td>&nbsp;</td></tr>";
		
		$locations = array();
		$loc_id_lookup = array();
		$loc_query = mlavu_query("SELECT `id`, `title` FROM `poslavu_[1]_db`.`locations` ORDER BY `title` ASC", $cust_read['data_name']);
		if (mysqli_num_rows($loc_query)) {
			while ($loc_read = mysqli_fetch_assoc($loc_query)) {
				$locations[] = array($loc_read['id'], $loc_read['title']);
				$loc_id_lookup[$loc_read['id']] = $loc_read['title'];
			}
		}
		
		echo "<tr>
			<td colspan='4'>
				<table>
					<tr>
						<td class='label label-info'>Log to search: </td>
						<td>
							<select name='log_type'>";
		foreach ($log_types as $log) {
			$selected = "";
			if ($log == $search_log) {
				$selected = " SELECTED";
			}
			echo "<option value='$log'$selected>$log</option>";
		}
		echo "		</select>
						</td>
						<td class='label label-info'>LLS: </td>
						<td>
							<input name='search_lls' type='checkbox' value='1'$lls_checked>
						</td>
						<td class='label label-info' style='padding-left:10px'>Date: </td>
						<td>
							<select name='date'>";
		$todays_date = false;
		for ($d = 0; $d < count($dates); $d++) {
			$selected = "";
			if ($date == $dates[$d]) {
				$selected = " SELECTED";
				if ($d == 0) $todays_date = true;
			}
			echo "<option value='".$dates[$d]."'$selected>".$dates[$d]."</option>";
		}
		echo "		</select>
						</td>
						<td class='label label-info' style='padding-left:10px'>Location: </td>
						<td>
							<select name='loc_id'>
								<option value='0'>All Locations</option>";
		foreach ($locations as $l) {
			$selected = "";
			if ($loc_id == $l[0]) {
				$selected = " SELECTED";
			}
			echo "<option value='".$l[0]."'$selected>".$l[1]."</option>";
		}	
		echo "		</select>
						</td>
						<td class='label label-info' style='padding-left:10px'>Search string: </td>
						<td><input type='text' name='search_for' size='30' value='$search_for'></td>
						<td style='padding-left:10px'><input type='submit' value='Search'></td>
					</tr>
				</table>
			</td>
		</tr>";
		echo "</table>";
		echo "</form>";

		echo "<span style='font-size: 11px; color:#666666;'>(LLS currently searchable only when LSVPN is turned on)</span>";
						
		echo "</div>";
		echo "</table>";
		
		echo "<div style='float:left; padding-left:10px; width:1200px;'>";
		echo "<br><br>";

		if ($search_log == "Admin Actions") {
		
			if ($lls == "1") echo "Admin Actions log not available for LLS search...<br><br>";
			else {
		
				$filter = "`server_time` >= '$date 00:00:00' AND `server_time` <= '$date 23:59:59'";				
				if ($loc_id != 0) $filter .= " AND `loc_id` = '$loc_id'";
				if ($search_for != "") $filter .= " AND (`action` LIKE '%".$search_for."%' OR `user` LIKE '%".$search_for."%' OR `data` LIKE '%".$search_for."%' OR `detail1` LIKE '%".$search_for."%' OR `detail2` LIKE '%".$search_for."%' OR `detail3` LIKE '%".$search_for."%')";
				
				$get_actions = mlavu_query("SELECT * FROM `poslavu_[1]_db`.`admin_action_log` WHERE $filter ORDER BY `server_time` ASC", $cust_read['data_name']);
				if (@mysqli_num_rows($get_actions) > 0) {
				
					echo "<br><table cellspacing=0 cellpadding=5 style='border:solid 1px black'>";
					echo "<tr bgcolor='#EDEDED'>";
					echo "<td align='center' valign='bottom'>Server Time</td>";
					echo "<td align='center' valign='bottom'>Local Time</td>";
					echo "<td align='center' valign='bottom'>Location</td>";
					echo "<td align='center' valign='bottom'>Action</td>";
					echo "<td align='center' valign='bottom'>User</td>";
					echo "<td align='center' valign='bottom'>User ID</td>";
					echo "<td align='center' valign='bottom'>IP Address</td>";
					echo "<td align='center' valign='bottom'>Order ID</td>";
					echo "<td align='center' valign='bottom' colspan='4'>Details</td>";
					echo "</tr>";

					while($log_info = mysqli_fetch_assoc($get_actions)) {
						echo "<tr>";
						echo "<td valign='top'>".$log_info['server_time']."</td>";
						echo "<td valign='top'>".$log_info['time']."</td>";
						echo "<td valign='top'>".$loc_id_lookup[$log_info['loc_id']]."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['action'])."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['user'])."</td>";
						echo "<td valign='top'>".$log_info['user_id']."</td>";
						echo "<td valign='top'>".$log_info['ipaddress']."</td>";
						echo "<td valign='top'>".$log_info['order_id']."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['data'])."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['detail1'])."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['detail2'])."</td>";
						echo "<td valign='top'>".str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $log_info['detail3'])."</td>";
						echo "</tr>";
					}
					echo "</table><br><br>";
										
				} else {
					echo "<br><br>No log entries found matching your search criteria...";
				}
			}
			
		} else if ($search_log == "Device Console") {
		
			if ($lls == "1") echo "Device Console logs not available for LLS search...<br><br>";
			else {

				$dcf = (!empty($_REQUEST['dcf']))?$_REQUEST['dcf']:false;
				$path = "/home/poslavu/logs/company/".$cust_read['data_name']; 
				
				if ($dcf) {
				
					if (file_exists($path."/".$dcf)) {

						echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=$search_log&date=$date\"'><< Back to list</a><br><br>";
	
						$log_file = fopen($path."/".$dcf, "r");
						while (($log_line = fgets($log_file, 4096)) !== false) {
							echo nl2br($log_line);
						}

						if (!feof($log_file)) {
							echo "Error: unexpected fgets() fail\n";
						}
						fclose($log_file);
						
						echo "<br><br>";
					
					} else {
					
						echo "Error: cannot find ".$dcf."<br><br>";
					}
					
					echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=$search_log&date=$date\"'><< Back to list</a><br><br>";
				
				} else {
			
					if ($dir = opendir($path)) {
		
						$found_match = false;
	
						$t = 0;
						$f = 0;
						$file_list = array();
						$file_list['name'] = array();
						$file_list['time'] = array();
	
						while (false !== ($file = readdir($dir))) {
							if (strstr($file, "device_console") && strstr($file, str_replace("-", "", $date))) {
								$found_match = true;
								$file_list['time'][$t++] = filemtime($path."/".$file);
								$file_list['name'][$f++] = $file;
							}
						}
				
						closedir($dir);
						asort($file_list['time']);
						asort($file_list['name']);
							
						if ($found_match) {
						
							foreach ($file_list['time'] as $key=>$ftime) {
			
								$filename = $file_list['name'][$key];
								//$File = fopen($path."/".$file, "r");
								$File = escapeshellarg($path."/".$filename);
								$header = `head -5 $File`;
								$first_line = `head -7 $File | tail -n 1`;
								$last_line = `tail -n 1 $File`;
								
								$first_time = substr($first_line, 0, 19);
								$last_time = substr($last_line, 0, 19);
		
								echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=$search_log&date=$date&dcf=$filename\"'>".$filename."</a> - <b>$first_time</b> to <b>$last_time</b> - ";
								echo trim(str_replace("\n", " &nbsp;&nbsp; ", $header), " &nbsp;")."<br>";
							}

						} else {
							echo "No device console logs found for ".$date;
						}
						echo "<br><br>";
		
					} else {
	
						echo "Error: unable to open directory (".$cust_read['data_name'].")...<br><br>";
					}
				}
			}
			
		} else if ($search_log == "Gateway Debug") {
		
			if ($lls == "1") echo "Gateway Debug logs not available for LLS search...<br><br>";
			else {
			
				$gdf = (!empty($_REQUEST['gdf']))?$_REQUEST['gdf']:false;
				$path = "/home/poslavu/logs/company/".$cust_read['data_name']."/gateway_debug"; 
				
				if ($gdf) {
				
					if (file_exists($path."/".$gdf)) {

						echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=$search_log&date=$date\"'><< Back to list</a><br><br>";
	
						$log_file = fopen($path."/".$gdf, "r");
						while (($log_line = fgets($log_file, 4096)) !== false) {
							echo nl2br(str_replace(array("<", " "), array("&lt;", "&nbsp;"), $log_line));
						}

						if (!feof($log_file)) {
							echo "Error: unexpected fgets() fail\n";
						}
						fclose($log_file);
						
						echo "<br><br>";
					
					} else {
					
						echo "Error: cannot find ".$gdf."<br><br>";
					}
					
					echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=$search_log&date=$date\"'><< Back to list</a><br><br>";
				
				} else {
			
					if ($dir = opendir($path)) {
		
						$found_match = false;
	
						$file_list = array();
						
						$search_for_loc = "";
						if ($loc_id != 0) $search_for_loc = $loc_id."_";
	
						while (false !== ($file = readdir($dir))) {
							if (strstr($file, str_replace("-", "", $date)) && ($loc_id==0 || substr($file, 0, (strlen((string)$loc_id) + 1))==$search_for_loc)) {
								$found_match = true;
								$file_list[] = $file;
							}
						}
				
						sort($file_list);
						closedir($dir);

						if ($found_match) {
						
							for ($f = (count($file_list) - 1); $f >= 0; $f--) {
							
								$filename = $file_list[$f];
			
								echo "<a style='cursor:pointer; color:#0033CC;' onclick='window.location = \"?mode=manage_customers&submode=$submode&rowid=$rowid&log_type=$search_log&date=$date&gdf=$filename\"'>".$filename."</a>";
								echo trim(str_replace("\n", " &nbsp;&nbsp; ", $header), " &nbsp;")."<br>";
							}

						} else {
							echo "No device console logs found for ".$date;
						}
						echo "<br><br>";
		
					} else {
	
						echo "Error: unable to open directory (".$cust_read['data_name'].")...<br><br>";
					}
				}				
			}

		} else {
		
			$append_date = "";
			$search_for_loc = "";
			if ($loc_id != 0) $search_for_loc = "LOCATION: ".$loc_id;

			if ($search_log == "Gateway") {
				
				$file_root = "gateway";
				$ext = ".log";
				if ($date != "") $append_date = "_".$date;
				
			} else if ($search_log == "Gateway Comm Vars") {
			
				$file_root = "gateway_vars";
				$ext = "";
				if ($date != "") $append_date = "_".$date;

			} else if ($search_log == "JSON Comm Vars") {

				$file_root = "json_log";
				$ext = "";
				if ($date != "") $append_date = "_".$date;

			} else if ($search_log == "MySQL") {

				$file_root = "mysql";
				$ext = ".log";
				if ($date != "") $append_date = "_".$date;

			} else if ($search_log == "Order Sum Corrections") {

				$file_root = "auto_correct";
				$ext = ".log";
				if ($date != "") $append_date = "_".$date;
			}

			if ($lls == "1") {
			
				$url = "http://poslavu.com/lsvpn/local/search_logs.php";
				$vars = "cc=".$cust_read['data_name']."&search=".$search_for."&type=".$file_root."&date=".$date;
			
				echo "<iframe src='".$url."?".$vars."' frameborder='1' width='1200px' height='1000px'></iframe><br><br>";
			
				/*$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($ch);
				
				if(curl_errno($ch)){
					echo 'Curl error: ' . curl_error($ch);
				}
				
				print_r(curl_getinfo($ch));

				curl_close($ch);

				echo $response."<br><br>";*/
			
			} else {

				$temp_string = "";
	
				$file_path = "/home/poslavu/logs/company/".$cust_read['data_name']."/".$file_root.$append_date.$ext;
				if (!file_exists($file_path)) $file_path = "/mnt/poslavu-files/logs/company/".$cust_read['data_name']."/".$file_root.$append_date.$ext;
				
				if (file_exists($file_path)) {
	
					$log_file = fopen($file_path, "r");
					while (($log_line = fgets($log_file, 4096)) !== false) {
	
						if (($search_log == "Gateway") && ($date != "")) {
	
							if (strstr($log_line, $date)) {
								$temp_string .= $log_line;
								$log_line = fgets($log_file, 4096);
								if ((($search_for == "") || strstr($log_line, $search_for)) && (($search_for_loc == "") || strstr($log_line, $search_for_loc))) {
									$temp_string .= $log_line;
									while (($log_line = fgets($log_file, 4096)) !== false) {
										$temp_string .= $log_line;
										if ($log_line == "\n") {
											break;
										}
									}

									/*$replace_this = array();
									$replace_this[] = "<";
									$replace_this[] = $date;
									$replace_this[] = $search_for_loc;
									$replace_this[] = $search_for;
									
									$with_this = array();
									$with_this[] = "&lt;";
									$with_this[] = "<span style='background-color:#FFFF66'><b>$date</b></span>";
									$with_this[] = "<span style='background-color:#FFFF66'><b>$search_for_loc</b></span>";
									$with_this[] = "<span style='background-color:#FFFF66'><b>$search_for</b></span>";

									echo nl2br(str_replace($replace_this, $with_this, $temp_string));*/
									
									$temp_string = nl2br(str_replace("<", "&lt;", $temp_string));
									$temp_string = str_replace($date, "<span style='background-color:#FFFF66'><b>$date</b></span>", $temp_string);
									$temp_string = str_replace($search_for_loc, "<span style='background-color:#FFFF66'><b>$search_for_loc</b></span>", $temp_string);
									$temp_string = str_replace("SUCCEEDED", "<span style='color:#009933'><b>{SUCCEEDED}</b></font>", $temp_string);
									$temp_string = str_replace("FAILED", "<span style='color:#990000'>{FAILED}</font>", $temp_string);
									echo str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", $temp_string);
									
								}
								$temp_string = "";
							}
		
						} else {
			
							if ((($search_for == "") || strstr($log_line, $search_for)) && (($search_for_loc == "") || strstr($log_line, $search_for_loc))) {
								$temp_string .= $log_line;
								while (($log_line = fgets($log_file, 4096)) !== false) {
									if ($log_line == "\n") {
										break;
									} else {
										$temp_string .= $log_line;
									}
								}
								$temp_string = str_replace($search_for_loc, "<span style='background-color:#FFFF66'><b>$search_for_loc</b></span>", nl2br($temp_string));
								$temp_string = str_replace("{SUCCEEDED}", "<font color='#0B2F00'>{SUCCEEDED}</font>", $temp_string);
								$temp_string = str_replace("{FAILED}", "<font color='#990000'><b>{FAILED}</b></font>", $temp_string);
								echo str_replace($search_for, "<span style='background-color:#FFFF66'><b>$search_for</b></span>", nl2br($temp_string));
								$temp_string = "";
								echo "<br>";
							}
						}
					}
					
					if (!feof($log_file)) {
						echo "Error: unexpected fgets() fail\n";
					}
					fclose($log_file);

				} else {

					echo "Error: file not found...";
				}
			}
		}
		
		echo "<input type='button' onclick='document.form1.submit()' value='Refresh'></a><br><br>";
		echo "<a href='index.php?mode=manage_customers&submode=details&rowid=$rowid'><< Company Details</a><br><br>";
		echo "<br><a href='index.php'>Back to Main Menu</a><br><br>";

		echo "</div>";
	}	
?>