<?php

require_once('/home/poslavu/private_html/myinfo.php');

// Do not instrument this high throughput transaction that is skewing the application average response time
if (extension_loaded('newrelic') && function_exists('newrelic_ignore_transaction')) { // Ensure PHP agent is available
    newrelic_ignore_transaction();
}

$strMySQLHost = my_poslavu_host();
$strMySQLUser = my_poslavu_username();
$strMySQLPassword = my_poslavu_password();

# Try to make a database connection to the current production master
$resourceDBHandle = @mysqli_connect($strMySQLHost,$strMySQLUser,$strMySQLPassword);

# Try to open a test file on the mounted file system to make sure it's working
$handleFile = fopen("/mnt/poslavu-files/images/hap-test-file","r");

# If we can't make a database connection or open the file send an error which will have HAProxy take this node out of rotation
if((!$resourceDBHandle) OR (!$handleFile))
{
        header("HTTP/1.1 503 Service Unavailable");
}
else
{
        header("HTTP/1.1 200 OK");
}
?>