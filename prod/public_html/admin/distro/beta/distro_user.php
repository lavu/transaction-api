<?php
	//session_start();
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/payment_profile_functions.php");
	require_once('/home/poslavu/public_html/admin/cp/objects/json_decode.php');
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();
	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";
	ini_set("memory_limit","128M");

	define('UNKNOWN', 0);
	define('DISTRIBUTOR', 1);
	define('USER', 2);
	define('LAVUADMIN', 3);
/**
user.php
author:Martin Tice

This class represents a distro user

**/

	class distro_user {

		function __construct() {
			$this->dbapi = ConnectionHub::getConn('poslavu')->getDBAPI();
		}

		private $leads = array();
		private $quotes = array();
		private $quotes_lineitems = array();
		private $accounts = array();
		private $info = array();//get_reseller_billing info
		private $loc_notes = array();
		private $loc_contacts = array();
		private $licenses = array();
		private $account_labels_options = "";
		private $lead_notes = array();// notes for leads
		private $all_unassigned = array(); // Leads w/o distributors
		private $all_in_progress = array();// started as leads, have NOT applied license
		private $all_resellers = array();// all resellers
		//private $reseller_username_to_id = array(); //key->username value->id;
		private $all_processed = array();//started as lead, applied license
		private $o_special = NULL;

		//holds all of the leads that are not test accounts.
		private $all_leads = array();
		//holds all of the leads with data_names in the restaurant table.
		private $leads_with_demos = array();

		public function get_lead_notes(){
			return $this->lead_notes;
		}
		public function get_leads(){
			return $this->leads;
		}
		public function get_quotes(){
			return $this->quotes;
		}
		public function get_quotes_lineitems() {
			return $this->quote_lineitems;
		}
		public function get_accounts(){
			return $this->accounts;
		}
		public function get_info(){
			return $this->info;
		}
		public function get_loc_notes(){
			//echo ' distro_user->get_loc_notes <br />'.var_dump($this->loc_notes);
			return $this->loc_notes;
		}
		public function get_loc_contacts(){
			return $this->loc_contacts;
		}
		public function get_licenses(){
			return $this->licenses;
		}
		public function get_account_labels_options(){
			return $this->account_labels_options;
		}
		public function get_all_resellers(){
			return $this->all_resellers;
		}
		public function get_all_processed(){
			return $this->all_processed;
		}
		public function get_all_lead_entries(){
			return $this->all_leads;
		}
		public function get_leads_with_demos(){
			return $this->leads_with_demos;
		}

/* These not made yet.. Don't know if need them.
		public function set_new_lead($lead){}
		public function set_lead_attribute($lead, $attr, $value){}
		public function set_new_account($account){}
		public function set_account_attr($account, $attr, $value){}
*/

		public function get_lead($data_name){return $this->leads[$data_name];}
		public function get_account($data_name){return $this->accounts[$data_name];}

		public function get_lead_attr($data_name, $attr){return $this->leads[$data_name][$attr];}
		public function set_lead_attr($data_name, $attr, $value){$this->leads[$data_name][$attr] = $value;}
		public function get_account_attr($data_name, $attr){return $this->accounts[$data_name][$attr];}

		public function get_info_attr($attr){return $this->info[$attr];}
		public function set_info_attr($attr, $value){$this->info[$attr] = $value;}
		public function get_all_unassigned_leads(){return $this->all_unassigned;}
		public function get_all_in_progress(){return $this->all_in_progress;}

		// does the distro accept leads that are only mercury accounts? (from try.poslavu.com)
		// returns TRUE or FALSE
		public function get_accepts_mercury() {
			return ($this->o_special->mercuryAcceptsList == '1') ? TRUE : FALSE;
		}
		public function set_accepts_mercury($b_accepts) {
			$s_saveval = $b_accepts ? '1' : '0';
			$this->o_special->mercuryAcceptsList = $s_saveval;
			$this->save_specials();
		}

		// special licenses are licenses that can only be applied to restaurants that start with a specific dataname,
		// for a specific set of licenses, for a certain quantity of restaurants, at a certain percentage discount

		// checks that the special licenses exist for this user
		private function check_special_licenses() {
			if (!isset($this->o_special->special_licenses)) {
				$this->o_special->special_licenses = new stdClass();
				$this->o_special->special_licenses->increment_key = 0;
			}
		}
		// gets the special licenses that are applicable to the given dataname
		// @return: an array(id1, id2, ...)
		public function get_applicable_special_licenses($s_dataname) {
			$a_retval = array();
			global $b_first_special_license_check;
			foreach(get_object_vars($this->get_special_licenses(0)) as $s_spec_id=>$o_special_license) {
				if (strpos($s_dataname, $o_special_license->prefix) === 0) {
					foreach($this->licenses as $s_id=>$a_license) {
						if ((string)$this->license_is_special($s_id) == (string)$s_spec_id) {
							$a_retval[] = $s_id;
						}
					}
				}
			}
			if (!isset($b_first_special_license_check) && strpos($s_dataname, 'pizza_amore') === 0) {
				error_log($s_dataname.": ".print_r($a_retval,TRUE));
			}
			return $a_retval;
		}
		// determine if the given license is a special license
		// @$s_id: the license id
		// @return: 0 if the license can't be found or is not special, or the special license id if it is special
		public function license_is_special($s_id) {
			if (!isset($this->licenses[$s_id]))
				return 0;
			$a_license = $this->licenses[$s_id];
			$s_notes = $a_license['notes'];
			if (strpos($s_notes, "special license id ") !== 0 && strpos($s_notes, "|special license id ") === FALSE)
				return 0;
			$a_spec_id = explode("special license id ", $s_notes);
			$a_spec_id = explode("|", $a_spec_id[1]);
			return (int)$a_spec_id[0];
		}
		// returns special licenses availabe for puchase that have quantity > 0 and aren't deleted
		// @$i_min_quantity: the minimum number of licenses left to give
		// @$b_check_deleted: check that the special licenses haven't been deleted
		// @$i_count: used to return the number of licenses contained
		// @return: object(0=>object('notes'=>notes, 'prefix'=>dataname prefix, 'original_quantity'=>int, 'quantity'=>int, 'discount_percentage'=>int, 'lic_types'=>array(), 'deleted'=>1/0, 'history'=>object('created'=>object(),'updated'=>object())), 1=>object(...), ...)
		public function get_special_licenses($i_min_quantity = 1, $b_check_deleted = TRUE, &$i_count = 0) {

			// get the special licenses
			$a_licenses = (array)$this->o_special->special_licenses;
			$o_licenses = new stdClass();

			// iterate through them, adding ones that have quantity > 0
			$i_count = 0;
			foreach($a_licenses as $index=>$o_special_licenses) {
				if (!isset($o_special_licenses->history))
					continue;
				if ($o_special_licenses->quantity < $i_min_quantity)
					continue;
				if ($o_special_licenses->deleted != '0' && $b_check_deleted)
					continue;
				$o_licenses->$index = $o_special_licenses;
				$i_count++;
			}

			return $o_licenses;
		}
		// creates a new set of specific licenses for the user
		// @$s_username: name of the current user/distributor/admin
		// @$i_user_type: should be one of UNKNOWN, USER, DISTRIBUTOR, or LAVUADMIN
		public function create_special_licenses($s_notes, $s_prefix, $i_quantity, $i_discount_percentage, $s_discount_type, $s_discount_comment, $a_lic_typenames, $s_username, $i_user_type) {
			$this->check_special_licenses();
			$o_special_license = new stdClass();
			$o_special_license->notes = (string)$s_notes;
			$o_special_license->prefix = (string)$s_prefix;
			$o_special_license->quantity = (int)$i_quantity;
			$o_special_license->original_quantity = (int)$i_quantity;
			$o_special_license->discount_percentage = (int)$i_discount_percentage;
			$o_special_license->discount_type = $s_discount_type;
			$o_special_license->discount_comment = $s_discount_comment;
			$o_special_license->lic_types = (array)$a_lic_typenames;
			$o_special_license->history = new stdClass();
			$o_special_license->history->created = new stdClass();
			$o_special_license->history->created->datetime = date('Y-m-d H:i:s');
			$o_special_license->history->created->username = $s_username;
			$o_special_license->history->created->usertype = $i_user_type;
			$o_special_license->history->updated = new stdClass();
			$o_special_license->deleted = '0';
			$i_count = (int)$this->o_special->special_licenses->increment_key + 1;
			$this->o_special->special_licenses->$i_count = $o_special_license;
			$this->o_special->special_licenses->increment_key = (string)$i_count;
			$this->save_special_licenses();
		}
		// updates a special licenses to save it to the database
		public function save_special_licenses($o_special_licenses = NULL) {
			if ($o_special_licenses !== NULL) {
				foreach($o_special_licenses as $k=>$v) {
					$this->o_special->special_licenses->$k = $v;
				}
			}
			$this->save_specials();
		}

		public function get_availability(){
			$days = array();
			$days['monday'] =  $this->o_special->monday;
			$days['tuesday'] =  $this->o_special->tuesday;
			$days['wednesday'] =  $this->o_special->wednesday;
			$days['thursday'] =  $this->o_special->thursday;
			$days['friday'] =  $this->o_special->friday;
			$days['saturday'] =  $this->o_special->saturday;
			$days['sunday'] =  $this->o_special->sunday;
			return $days;
		}

		//sets a string describing availability; key = day of week,value = description
		public function set_availability($avail){
			$this->o_special->monday = (isset($avail['monday']))? $avail['monday'] : '';
			$this->o_special->tuesday = (isset($avail['tuesday']))? $avail['tuesday'] : '';
			$this->o_special->wednesday = (isset($avail['wednesday']))? $avail['wednesday'] : '';
			$this->o_special->thursday = (isset($avail['thursday']))? $avail['thursday'] : '';
			$this->o_special->friday = (isset($avail['friday']))? $avail['friday'] : '';
			$this->o_special->saturday = (isset($avail['saturday']))? $avail['saturday'] : '';
			$this->o_special->sunday = (isset($avail['sunday']))? $avail['sunday'] : '';
			save_specials();

		}

		// gets/sets the number of allowed devices
		public function num_allowed_admin_devices($i_count = -1) {
			if (!isset($this->o_special->num_allowed_admin_devices))
				$this->o_special->num_allowed_admin_devices = 1;
			if ($i_count == -1)
				return (int)$this->o_special->num_allowed_admin_devices;
			$this->o_special->num_allowed_admin_devices = (int)$i_count;
			$this->save_specials();
		}
		public function allowed_admin_devices($a_device_list = NULL) {

			// get or set the number of allowed devices
			$i_allowed = $this->num_allowed_admin_devices();
			if (!isset($this->devices) || !isset($this->devices->allowed_admin_devices)) {

				// load the devices
				if (!isset($this->devices))
					$this->devices = new stdClass();
				$a_devices = $this->dbapi->getAllInTable('reseller_devices', array('reseller_id'=>$this->get_info_attr('id'), 'type'=>'admin_device'), TRUE, array('selectclause'=>'`UUID_min` AS `UUID`', 'collapse_arrays'=>TRUE));
				$this->devices->allowed_admin_devices = $a_devices;
				foreach($this->devices->allowed_admin_devices as $k=>$v)
					if (strlen($v) == 0)
						unset($this->devices->allowed_admin_devices[$k]);
				$i_count = count($this->devices->allowed_admin_devices);
				if ($i_allowed < 1)
					$this->devices->allowed_admin_devices = array();
				else if ($i_count < $i_allowed)
					$this->devices->allowed_admin_devices = array_fill($i_count, $i_allowed-$i_count, '00000000-0000');
			}

			// save changes to the allowed devices
			if ($a_device_list !== NULL) {
				$this->devices->allowed_admin_devices = $a_device_list;
				if (!isset($this->o_special->admin_device_register_times))
					$this->o_special->admin_device_register_times = array();
				$this->o_special->admin_device_register_times[] = date("Y-m-d H:i:s");
			}

			// set the allowed device list to exactly as many devices as are allowed
			$i_count = count($this->devices->allowed_admin_devices);
			if ($i_count > $i_allowed) {
				$a_devices = array();
				$i = 0;
				foreach($this->devices->allowed_admin_devices as $wt_device) {
					$i++;
					if ($i > $i_allowed) break;
					$a_devices[] = $wt_device;
				}
				$this->devices->allowed_admin_devices = $a_devices;
			} else if ($i_count < $i_allowed) {
				$this->devices->allowed_admin_devices = array_merge(
				    $this->devices->allowed_admin_devices,
					array_fill($i_count, $i_allowed-$i_count, '00000000-0000')
				);
			}

			// save
			if ($a_device_list !== NULL) {
				$this->save_specials();
				if ($s_reason_for_changing == "" || $s_reason_for_changing == "Reason For Changing")
					$s_reason_for_changing = "no reason provided for the change";
				foreach($this->devices->allowed_admin_devices as $s_uuid_min) {
					$a_update_vars = array('reseller_id'=>$this->get_info_attr('id'), 'UUID_min'=>$s_uuid_min, 'type'=>'admin_device');
					$this->dbapi->insertOrUpdate('poslavu_MAIN_db', 'reseller_devices', $a_update_vars, $a_update_vars);
				}
			}

			return $this->devices->allowed_admin_devices;
		}
		public function draw_allowed_admin_devices() {

			// get the allowed devices list
			$a_devices = $this->allowed_admin_devices();
			if (count($a_devices) == 0)
				return '';

			// build the string
			$s_retval = '<table>';
			$s_starttag = '<tr><td class="edit_name">Device --index--: </td><td class="edit_value"><input type="hidden" name="type" value="textbox" /><input type="hidden" name="index" value="--index--" /><input type="hidden" name="name" value="--index--" /><font class="value">';
			$s_endtag = '</font></td></tr>';
			$s_retval .= $s_starttag;
			$s_retval .= implode($s_endtag.$s_starttag, $a_devices);
			$s_retval .= $s_endtag.'</table>';
			$i = 1;
			while (strpos($s_retval, "--index--") !== FALSE) {
				$s_retval = preg_replace("/--index--/", $i, $s_retval, 3);
				$i++;
			}

			return $s_retval;
		}

		// get or set if the user has a custom commission
		// @$i_value: 0 or 1, or -1 to retrieve (not set)
		public function use_custom_commission($i_value = -1, $s_username = "", $s_reason_for_changing = "") {
			if (!isset($this->o_special->commission))
				$this->o_special->commission = new stdClass();
			if ($i_value === 0 || $i_value === 1) {

				// get user information
				$id = $this->get_info_attr('id');
				$username = $this->get_info_attr('username');
				$i_old_is_custom_commission = $this->o_special->commission->is_custom;
				$i_commission = $this->get_info_attr("distro_license_com");

				// set and save the new values
				$this->o_special->commission->is_custom = $i_value;
				$this->save_specials();

				// create a billing log
				if ($s_reason_for_changing == "")
					$s_reason_for_changing = "no reason for changing provided";
				mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`,`ipaddress`,`username`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]','[ipaddress]','[username]')",
					array('datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$id, 'resellername'=>$username, 'dataname'=>'', 'action'=>'change distributor use custom commission', 'details'=>"Changing distributor use custom commission from {$i_old_is_custom_commission} to {$i_value}, current commission {$i_commission}, reason for changing: {$s_reason_for_changing}", 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'username'=>$s_username));
			} else {
				if (!isset($this->o_special->commission->is_custom))
					$this->o_special->commission->is_custom = 0;
				$i_value = $this->o_special->commission->is_custom;
			}
			return (int)$i_value;
		}
		// sets the new commission for the reseller
		// @$i_new_commission: the new commission (as an integer percentage, eg 20 for 20%)
		public function update_commission($i_new_commission, $s_username = "", $s_reason_for_changing = "") {

			// get user information
			$id = $this->get_info_attr('id');
			$username = $this->get_info_attr('username');
			$i_is_custom_commission = $this->o_special->commission->is_custom;
			$i_old_commission = $this->get_info_attr("distro_license_com");

			// set and save the new values
			$this->set_info_attr("distro_license_com", $i_new_commission);
			mlavu_query("UPDATE `poslavu_MAIN_db`.`resellers` SET `distro_license_com`='[commission]' WHERE `username`='[username]' AND `id`='[id]' AND `username`!=''",
				array('username'=>$username, 'id'=>$id, 'commission'=>$i_new_commission));

			// create a billing log
			if ($s_reason_for_changing == "")
				$s_reason_for_changing = "no reason for changing provided";
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`,`ipaddress`,`username`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]','[ipaddress]','[username]')",
				array('datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$id, 'resellername'=>$username, 'dataname'=>'', 'action'=>'change distributor commission', 'details'=>"Changing distributor commission from {$i_old_commission} to {$i_new_commission}, use custom commission {$i_is_custom_commission}, reason for changing: {$s_reason_for_changing}", 'ipaddress'=>$_SERVER['REMOTE_ADDR'], 'username'=>$s_username));
		}
		// gets all of the rows from reseller_billing_log related to changing
		// this user's commissions
		public function get_previous_commissions() {

			// get some values
			$username = $this->get_info_attr('username');
			$id = $this->get_info_attr('id');

			// get the commissions db rows
			global $a_distro_user_get_previous_commissions;
			if (!isset($a_distro_user_get_previous_commissions) ||
			    !isset($a_distro_user_get_previous_commissions[$username])) {
				if (!isset($a_distro_user_get_previous_commissions))
					$a_distro_user_get_previous_commissions = array();
				$a_distro_user_get_previous_commissions[$username] =
					$this->dbapi->getAllInTable('reseller_billing_log', array('resellerid'=>$id, 'resellername'=>$username, 'action'=>'change distributor commission'), TRUE);
			}

			return $a_distro_user_get_previous_commissions[$username];
		}
		// gets the user's previous commissions and returns them in an html format
		// @$b_is_admin: if TRUE, prints a bit of extra data
		// @return: the commissions in an html format
		public function previous_commissions_toString($b_is_admin = FALSE) {

			// get/set some values
			$a_commissions = $this->get_previous_commissions();
			$s_retval = "";

			// create a row for every commission
			foreach($a_commissions as $a_commission) {

				// draw the extra info
				if ($b_is_admin) {
					$s_retval .= "<a style='color:blue; text-decoration:underline;' onclick='alert(\"";
					foreach($a_commission as $k=>$v) {
						$s_retval .= str_replace(array("'",'"'), "", $k);
						$s_retval .= ": ";
						$s_retval .= str_replace(array("'",'"'), "", $v);
						$s_retval .= "\\n";
					}
					$s_retval .= "\");'>";
				}

				// get and draw the commission
				$a_commission_parts = explode(" from ", $a_commission['details']);
				$a_commission_parts = explode(" to ", $a_commission_parts[1]);
				$s_retval .= $a_commission_parts[0];

				// draw the extra info
				if ($b_is_admin)
					$s_retval .= "</a><br />";
			}

			return $s_retval;
		}

		// creates/saves the specials json object
		private function create_specials() {
			$this->o_special = json_decode($this->info['special_json']);
			// set the defaults if the values don't already exist
			$a_specials_defaults = array('mercuryAcceptsList'=>'0');
			$b_save = FALSE;
			foreach($a_specials_defaults as $k=>$v) {
				if (!isset($this->o_special->$k)) {
					$this->o_special->$k = $v;
					$b_save = TRUE;
				}
			}
			if ($b_save)
				$this->save_specials();
		}
		private function save_specials() {
			global $maindb;
			$s_special = json_encode($this->o_special);
			mlavu_query("UPDATE `[maindb]`.`resellers` SET `special_json`='[special_json]' WHERE `id`='[id]'",
				array("maindb"=>$maindb, "special_json"=>$s_special, "id"=>$this->info['id']));
		}

		/*private function update_auto_licenses(){

			$q_vars = array();
			$q_vars['signup_db'] = '`poslavu_MAIN_db`.`signups`';
			$q_vars['leads_db'] = '`poslavu_MAIN_db`.`reseller_leads`';
			$q_vars['none'] = 'none';
			$signup_query = mlavu_query("select * from [signup_db] where `package`!='[none]'", $q_vars);
			while($signup = mysqli_fetch_assoc($signup_query)){

				$package= $signup['package'];
				if($package != ''){
					$q_vars['data_name'] = $signup['dataname'];
					$q_vars['license_applied'] = $signup['signup_timestamp'];
					$q_vars['email'] = $signup['email'];
					$q_vars['company_name'] = $signup['company'];
					mlavu_query()
				}

			}

		}//update_auto*/


		/**
		This function sets all of a given distributers leads and accounts into
		their respective arrays
		**/
		public function set_leads_accounts($user_name, $range=''){
			global $o_package_container;
			///error_log("count of leads: ".count($this->leads));
			///error_log("count of accounts: ".count($this->accounts));

			//error_log("set_leads_accounts");
			$dbr = '`poslavu_MAIN_db`.`restaurants`';
			$d_code = '`distro_code`';
			$dname_loc = '`dataname`';

			$dbr_loc = '`poslavu_MAIN_db`.`restaurant_locations`';
			$loc_fields = '`address`,`city`,`state`,`zip`,`country`,`manager`';

			$db_paystat = '`poslavu_MAIN_db`.`payment_status`';
			$lead_status = '`lead_status`';
			$trial_end = '`trial_end`';



			//echo "leads:".count($this->leads).", accounts:".count($this->accounts)."<br />";
			//
			//
			//query to get info from restaurants table

			//echo 'range:: '.$range.'<br />';

			if($range == ''){
				$restaurants_query_string = "select * from [restaurantsdb] where [distrocode]='[username]' order by `created` desc limit 100";
			}else{
				$restaurants_query_string = "select * from [restaurantsdb] where [distrocode]='[username]' order by `created` desc limit ".ConnectionHub::getConn('poslavu')->escapeString($range);
			}
			$restaurants_query_vars = array("restaurantsdb"=>$dbr,"distrocode"=>$d_code,"username"=>$user_name);
			$_SESSION['update_accounts'] = '';


			$restaurants_query = mlavu_query($restaurants_query_string, $restaurants_query_vars)
					or die(mlavu_dberror("disto_user->set_leads_accounts_restaurants_query error"));

			///error_log("a.0.0");
			///display_diff_mem_usage();
			//goes through each distro's restaurants and builds all of the arrays
			while($entry = mysqli_fetch_assoc($restaurants_query)){
				///display_diff_mem_usage();
				///error_log("cost of restaurant");
				$dname = $entry['data_name'];
				$d_id = $entry['id'];

				//
				//
				//query to get info from restaurant_locations table
				$loc_query = mlavu_query("select [1] from [2] where [3]='[4]'",$loc_fields,$dbr_loc,$dname_loc,$dname) or die(mlavu_dberror("disto_user->set_leads_accounts_res_loc_query error"));
				$d_loc_fields = mysqli_fetch_assoc($loc_query);
				mysqli_free_result($loc_query);


				//
				//
				//query to get information from payment_status table
				$lead_stat_query = mlavu_query("select * from [1] where [2]='[3]'",$db_paystat, $dname_loc, $dname) or
				die(mlavu_dberror("disto_user->set_leads_accounts_pay_status_query error"));
				$d_lead_stat = mysqli_fetch_assoc($lead_stat_query);
				mysqli_free_result($lead_stat_query);


				//
				//
				//query to get information from restaurant_notes table
				$db_notes = '`poslavu_MAIN_db`.`restaurant_notes`';
				$n_r_id = '`restaurant_id`';
				$n_s_ninja = '`support_ninja`';
				//echo 'query::<br />'."select * from $db_notes where $n_r_id='$d_id' and $n_s_ninja='DISTRO_NOTE'<br /><br />";
				$res_notes_query = mlavu_query("select * from [1] where [2]='[3]' and [4]='DISTRO_NOTE'",$db_notes, $n_r_id, $d_id,$n_s_ninja) or
				die(mlavu_dberror("disto_user->set_leads_accounts_r_notes_query error"));

				while($d_notes_stat = mysqli_fetch_assoc($res_notes_query)){// load all notes into loc_notes
					//echo '<br /><br /><br /><br /><br />'.var_dump($d_notes_stat);
					//foreach($d_notes_stat as $nkey=>$nvalue){
					$mdate = $d_notes_stat['created_on'];
					$mmessage = $d_notes_stat['message'];
					$this->loc_notes[$dname][$mdate] = $mmessage;
						//echo 'NOTE INFO::::<br />dname= '.$dname.'<br /> key= '.$mdate.' <br />value= '.$mmessage.'<br /><br />';
					//}
					unset($mdate);
					unset($mmessage);
					unset($d_notes_stat);
				}//end while loop
				mysqli_free_result($res_notes_query);
				///display_diff_mem_usage();
				///error_log("aaaaaaaa");
				//
				//
				//query to get information from signups table
				$db_signups = '`poslavu_MAIN_db`.`signups`';
				$s_r_id = '`restaurantid`';
				//echo 'query::<br />'."select * from $db_signups where $s_r_id='$d_id'<br /><br />";
				//select * from `poslavu_MAIN_db`.`signups` where `restaurantid`='[1]' or `dataname`='[2]'
				$signups_query = mlavu_query("select * from [1] where [2]='[3]' or `dataname`='[4]'",$db_signups, $s_r_id, $d_id,$dname) or
				die(mlavu_dberror("disto_user->set_leads_accounts_signups_query error"));
				///display_diff_mem_usage();
				///error_log("bbbbbbbb");
				$p_level = '';
				$signup_timestamp = '';
				if($signups_stat = mysqli_fetch_assoc($signups_query)){//
					//echo '$signups_stat[dataname] = '.$signups_stat['dataname'].'<br />';//'<br /><br /><br /><br /><br />'.var_dump($signups_stat);
					$first = $signups_stat['firstname'];
						$last = $signups_stat['lastname'];
						$signup_id = $signups_stat['id'];
					/*	$p_level = $signups_stat['package'];
						$signup_timestamp = $signups_stat['signup_timestamp'];
						$d_name = $signups_stat['dataname'];
						$rest_company = $signups_stat['company'];
						$rest_email = $signups_stat['email'];
						//echo 'pl::'.$package_level.'<br />';
						$leads_db = '`poslavu_MAIN_db`.`reseller_leads`';
					if($p_level != '' && $p_level != 'none'){
						//mlavu_query("update [1] set `license_applied`='[2]' where `company_name`='[3]' and `email`='[4]' limit 1", $leads_db, $signup_timestamp, $rest_company, $rest_email);
						if($rest_company == 'Shiki'){
						//error_log("not a bug ben:::: ".$rest_company.', '.$rest_email.', '.$p_level);
						}
					}*/
					foreach($signups_stat as $ckey=>$cvalue){
						$this->loc_contacts[$dname][$signup_id][$ckey] = $cvalue;
					}
					$other_contacts_field = $signups_stat['other_contacts'];
					//echo strlen($other_contacts_field).'<br />';
					if(strpos($other_contacts_field, ':*:')){
						//echo "oc";
						$other_contacts = explode('|',$other_contacts_field);
						//echo count($other_contacts);
						$other_count = 1;
						foreach($other_contacts as $contact){
							//echo $c_info[0].", ".$c_info[1].", ".$c_info[2].", ".$c_info[3].", ".$c_info[4]."<br />";
							$o_id = $signup_id.'_'.$other_count;
							$c_info = explode(':*:', $contact);
							if($c_info[0] != ''){
								$this->loc_contacts[$dname][$o_id]['firstname'] = $c_info[0];
								$this->loc_contacts[$dname][$o_id]['lastname'] = $c_info[1];
								$this->loc_contacts[$dname][$o_id]['phone'] = $c_info[2];
								$this->loc_contacts[$dname][$o_id]['email'] = $c_info[3];
								$this->loc_contacts[$dname][$o_id]['contact_notes'] = $c_info[4];
								$other_count++;
							}
						}
					}
					//echo 'CONTACT INFO::::<br />dname= '.$dname.'<br /> key= '.$ckey.' <br />value= '.$cvalue.'<br /><br />';
					//}
				}//end while loop
				//fill in the accounts information
				$this->accounts[$dname] = $entry;
				//information from restaurant_location table
				if($d_loc_fields != null)
					$this->accounts[$dname] = array_merge($d_loc_fields, $this->accounts[$dname]);
				//information from payment_status table
				if($d_lead_stat != null)
					$this->accounts[$dname] = array_merge($d_lead_stat, $this->accounts[$dname]);
				if($p_level != ''){
					// set the package level to what was found (aka $a_package_data['package'])
					$this->accounts[$dname]['package'] = $p_level;
				}else{
					// set the package level because the package level wasn't previously found
					$a_package_data = find_package_status($entry['id']);
					$this->accounts[$dname]['package'] = $a_package_data['package'];
					if ($signup_id != '' && is_numeric($signup_id)) {
						mlavu_query("UPDATE [signupsdb] SET `package`='[package]' WHERE `id`='[signupid]'",
							array("signupsdb"=>$db_signups,"package"=>$a_package_data['package'],"signupid"=>$signup_id));
					}
				}
			}//while($entry = mysqli_fetch_assoc($restaurants_query))

			// free some memory
			unset($entry);
			unset($d_loc_fields);
			unset($d_lead_stat);

			// load data from the payment_responses table
			$this->insert_payment_responses_information_into_accounts();

			//query to get info from reseller_leads table
			$leads_query_string = "select * from `poslavu_MAIN_db`.`reseller_leads` where `distro_code`='[username]' AND `_finished` != '1' AND `_canceled` != '1' order by created desc";
			$leads_query_vars = array("username"=>$user_name);
			//bed_dev_display_query($leads_query_string, $leads_query_vars);
			$leads_query = mlavu_query($leads_query_string, $leads_query_vars)
					or die(mlavu_dberror("disto_user->set_leads_accounts_restaurants_query error"));

			///error_log("a.0.1");
			///display_diff_mem_usage();
			//goes through each distro's restaurants and builds all of the arrays
			while($a_lead = mysqli_fetch_assoc($leads_query)){
				$i_lead_id = $a_lead['id'];
				$s_lead_status = "finish";
				if (!isset($this->leads[$i_lead_id])) $this->leads[$i_lead_id] = array();
				$this->leads[$i_lead_id]['already_made_demo_account'] = FALSE;

				//error_log("a.0.1.0");
				//display_diff_mem_usage();
				// get the lead's status
				$a_dates_names = array();
				$a_dates_names["license_applied"]='apply_license';
				$a_dates_names["made_demo_account"]='demo_account';
				$a_dates_names["contacted"]='contact';
				$a_dates_names["accepted"]='accept';
				foreach($a_dates_names as $k=>$v) {
					$s_date_value = preg_replace('/[^1-9]/','',$a_lead[$k]);
					if ($k == "made_demo_account" && $s_date_value != '')
						$this->leads[$i_lead_id]['already_made_demo_account'] = TRUE;
					if ($s_date_value == '') // the date has been set
						$s_lead_status = $v;
				}

				//error_log("a.0.1.1");
				//display_diff_mem_usage();
				// get the lead's level
				if ($this->leads[$i_lead_id]['already_made_demo_account'] && strlen($a_lead['data_name'])) {
					$s_dataname = $a_lead['data_name'];
					$restaurant_query = mlavu_query("SELECT `id` FROM `[maindb]`.`restaurants` WHERE `data_name`='[data_name]' LIMIT 1",
						array("maindb"=>"poslavu_MAIN_db", "data_name"=>$s_dataname));
					$signups_query = mlavu_query("SELECT `status` FROM `[maindb]`.`signups` WHERE `dataname`='[data_name]' LIMIT 1",
						array("maindb"=>"poslavu_MAIN_db", "data_name"=>$s_dataname));
					if ($restaurant_query)
						if (mysqli_num_rows($restaurant_query))
							$a_restaurant = mysqli_fetch_assoc($restaurant_query);
					if ($signups_query)
						if (mysqli_num_rows($signups_query))
							$a_signup = mysqli_fetch_assoc($signups_query);
					if (count($a_restaurant) > 0) {
						$i_restaurant_id = (int)$a_restaurant['id'];
						if ($i_restaurant_id > 0) {
							if ($a_signup['status'] == 'new')
								$a_package_data = find_package_status($i_restaurant_id);
							else
								$a_package_data['package'] = '12'; // a default in case the account was created manually
							$s_package = $a_package_data['package'];
							if (is_numeric($s_package))
								$this->leads[$i_lead_id]['level'] = $o_package_container->get_plain_name_by_attribute('level', $s_package);
						}
					}
				}

				//error_log("a.0.1.2");
				//display_diff_mem_usage();
				//get the notes on the lead
				$a_temp_notes = explode('|*note*|',$a_lead['notes']);
				foreach($a_temp_notes as $s_note){// load all notes into lead_notes
						$a_note_parts = explode('|*date*|',$s_note);
						$mmessage = $a_note_parts[0];
						$mdate = $a_note_parts[1];
						$this->lead_notes[$i_lead_id][$mdate] = $mmessage;
						//$a_lead['notes'] = '';
				}

				//error_log("a.0.1.2");
				//display_diff_mem_usage();
				// fill in leads array
				foreach($a_lead as $key=>$value){
					$this->leads[$i_lead_id][$key] = $value;
				}
				$this->leads[$i_lead_id]['lead_status'] = $s_lead_status;
			}//while($a_lead = mysqli_fetch_assoc($leads_query))
		}//set_leads_accounts


		/**
		This function sets all of a given distributor quotes into
		their respective arrays
		**/
		public function set_quotes($distro_code, $range=''){
			//error_log(__FILE__ . " DEBUG: distro_code={$distro_code} count of quotes: ".count($this->quotes));  //debug
			//error_log(__FILE__ .' DEBUG: In distro_user->set_quotes()');  //debug

			//query to get reseller_quotes from database table
			$quotes_query_sql = "SELECT * FROM `poslavu_MAIN_db`.`reseller_quotes` WHERE `distro_code`='[distro_code]' AND `_deleted` != '1' ORDER BY `id` DESC";
			$quotes_query_vars = array("distro_code" => $distro_code);
			$quotes_query = mlavu_query($quotes_query_sql, $quotes_query_vars)
					or die(mlavu_dberror("disto_user->set_quotes error"));

			//goes through each distro's quotes and builds all of the arrays
			while ( $reseller_quote = mysqli_fetch_assoc($quotes_query) ){
				$id = $reseller_quote['id'];

				if (!isset($this->quotes[$id])) $this->quotes[$id] = array();

				// fill in quotes array
				foreach($reseller_quote as $key => $value){
					$this->quotes[$id][$key] = $value;
				}

				//error_log("a.0.1.2");
				//display_diff_mem_usage();
			}

			mysqli_free_result($quotes_query);
		}//set_quotes

		public function set_quotes_lineitems($distro_code, $reseller_quote_id){
			//error_log("count of quotes_lineitems: ".count($this->quotes_lineitems));  //debug
			//error_log('In distro_user->set_quotes_lineitems()');  //debug

			//query to get reseller_quotes_lineitems from database table
			$quotes_lineitems_query_sql = "SELECT * FROM `poslavu_MAIN_db`.`reseller_quotes_lineitems` WHERE `distro_code`='[distro_code]' AND `reseller_quote_id` = '[reseller_quote_id]' AND `_deleted` != '1' ORDER BY `line_num` DESC";
			$quotes_lineitems_query_vars = array("distro_code" => $distro_code, 'reseller_quote_id' => $reseller_quote_id);
			$quotes_lineitems_query = mlavu_query($quotes_lineitems_query_sql, $quotes_lineitems_query_vars)
					or die(mlavu_dberror("disto_user->set_quotes error"));

			//goes through each distro's quotes_lineitems and builds all of the arrays
			while ( $reseller_quote_lineitems = mysqli_fetch_assoc($quotes_lineitems_query) ){
				if (!isset($this->quotes_lineitems[$reseller_quote_id])) $this->quotes_lineitems[$reseller_quote_id] = array();

				// fill in quotes_lineitems array
				foreach($reseller_quote_lineitems as $key => $value){
					$this->quotes_lineitems[$reseller_quote_id][$key] = $value;
				}

				//error_log("a.0.1.2");
				//display_diff_mem_usage();
			}

			mysqli_free_result($quotes_lineitems_query);
		}//set_quotes_lineitems


		// gets information from the payment_responses table and inserts it into the account
		// searches specifically for a valid license payment and sets $a_account['has_license_payment'] = TRUE or FALSE, depending
		private function insert_payment_responses_information_into_accounts() {

			require_once(dirname(__FILE__).'/account_functions.php');
			if (count($this->accounts) == 0)
				return;

			// get a list of account ids to datanames
			$a_ids_to_dns = array();
			foreach($this->accounts as $data_name=>$a_account)
				$a_ids_to_dns[$a_account['id']] = $data_name;

			// default to TRUE
			foreach($a_ids_to_dns as $id=>$data_name)
				$this->accounts[$data_name]['has_license_payment'] = TRUE;

			// query the database for payment_responses that have x_response_code = 1 and payment_type = license
			$a_ids = array();
			foreach($a_ids_to_dns as $id=>$dataname)
				$a_ids[] = $id;
			$a_paid_ids = get_accounts_with_paid_licenses($a_ids);

			// set the data in the accounts
			foreach($a_paid_ids as $id=>$b_paid) {
				$data_name = $a_ids_to_dns[$id];
				$this->accounts[$data_name]['has_license_payment'] = $b_paid;
			}

			unset($a_ids);
			unset($a_paid_ids);
			unset($a_response);
		}

		//fills in all_unassigned leads
		public function set_all_unassigned($options = array()){

			$merc_ok_only = isset($options['merc_ok_only']) ? $options['merc_ok_only'] : false;

			//error_log("set_all_unassigned");
			//echo "unass:".count($this->all_unassigned)."<br />";
			//if(count($this->all_unassigned)){return;}

			//all unfinished
			$qu_all_leads = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_leads` where `distro_code` != 'martin' and `_finished` != '1' and `_canceled` != '1' order by `id` desc");
			while($lead_entry = mysqli_fetch_assoc($qu_all_leads)){
				if ($merc_ok_only && abs($lead_entry['merc_ok']) !== 1) continue;
				$id = $lead_entry['id'];
				foreach($lead_entry as $key=>$value){
					if($lead_entry['distro_code'] != ''){
						$this->all_in_progress[$id][$key] = $value;
					}
				}
			}

			//not contacted
			$qu = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_leads` where `contacted`='0000-00-00 00:00:00'");
			while($a_lead = mysqli_fetch_assoc($qu)){
				if ($merc_ok_only && abs($lead_entry['merc_ok']) !== 1) continue;
				$id = $a_lead['id'];
				foreach($a_lead as $key=>$value){
					//$this->all_unassigned[$key] = $value;
					//if($key != 'id'){
						$this->all_unassigned[$id][$key]= $value;
					//}

				}

			}

			$qu_processed = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_leads` where `distro_code` != 'martin' and `_finished` = '1'");
			while($a_proc = mysqli_fetch_assoc($qu_processed)){
				if ($merc_ok_only && abs($lead_entry['merc_ok']) !== 1) continue;
				$id = $a_proc['id'];
				foreach($a_proc as $key=>$value){
					//$this->all_unassigned[$key] = $value;
					if($a_proc['email'] != 'corey@poslavu.com' && $a_proc['license_applied'] != '0000-00-00 00:00:00'){
						$this->all_processed[$id][$key]= $value;
					}

				}

			}

			//declined
			$qu_d = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_leads` where `_declined`='1'");
			while($d_lead = mysqli_fetch_assoc($qu_d)){
				if ($merc_ok_only && abs($lead_entry['merc_ok']) !== 1) continue;
				$id = $d_lead['id'];
				foreach($d_lead as $key=>$value){
					//$this->all_unassigned[$key] = $value;
					//if($key != 'id'){
						$this->all_unassigned[$id][$key]= $value;
					//}

				}

			}
		}//set_all_unassigned

		//fills in all_resellers
		public function set_all_resellers(){
			//error_log("set_all_resellers");
			//echo "resellers:".count($this->all_resellers)."<br />";
			if(count($this->all_resellers)){
				//echo "already set<br />";
				return;
			}
			$q_vars = array();
			$q_vars['r_db'] = '`poslavu_MAIN_db`.`resellers`';
			$q_result = mlavu_query("SELECT * FROM [r_db] ORDER BY `company` ASC",$q_vars);

			while($entry = mysqli_fetch_assoc($q_result)){
				//$username = $entry['username'];
				if($entry['username'] != '' && $entry['access'] != 'deactivated'){
					$username = $entry['username'];
					//echo $username.'<br />';
					foreach($entry as $key=>$value){
						$this->all_resellers[$username][$key] = $value;
					}
				}
			}
			//ksort($this->all_resellers);

			//echo count($this->all_resellers);
		}//set_all_resellers


		public function set_all_mercury_resellers(){
			//error_log("set_all_resellers");
			//echo "resellers:".count($this->all_resellers)."<br />";
			if(count($this->all_resellers)){
				//echo "already set<br />";
				return;
			}
			$q_vars = array();
			$q_vars['r_db'] = '`poslavu_MAIN_db`.`resellers`';
			$q_result = mlavu_query("SELECT * FROM [r_db] ORDER BY `company` ASC",$q_vars);

			while($entry = mysqli_fetch_assoc($q_result)){
				//$username = $entry['username'];
				if($entry['username'] != '' && $entry['access'] != 'deactivated'){
					$username = $entry['username'];
					//echo $username.'<br />';
					foreach($entry as $key=>$value){
						$this->all_resellers[$username][$key] = $value;
					}
				}
			}
			//ksort($this->all_resellers);

			//echo count($this->all_resellers);
		}//set_all_resellers


		public function set_all_lead_entries(){
		 $fields = array();
		 $fields['db_leads'] = '`poslavu_MAIN_db`.`reseller_leads`';
		 $fields['test_lead'] = '0';

		 $lead_q = mlavu_query("select * from [db_leads] where `test_lead`='[test_lead]' and `bad_info`='0'", $fields);
		 echo mlavu_dberror();
		 while($entry = mysqli_fetch_assoc($lead_q)){
		 	$id = $entry['id'];
		 	foreach($entry as $key=>$value){
				$this->all_leads[$id][$key] = $value;
		 	}
		 }

		}//set_all_lead_entries
		public function set_all_leads_with_demos(){

			$fields = array();
			$fields['db_leads'] = '`poslavu_MAIN_db`.`reseller_leads`';
			$fields['db_rest'] = '`poslavu_MAIN_db`.`restaurants`';
			$fields['test_lead'] = '0';

			$lead_q = mlavu_query("select [db_rest].`data_name`,[db_rest].`id`, [db_rest].`disabled`  from [db_leads], [db_rest] where [db_leads].`test_lead`='[test_lead]' and [db_leads].`data_name` != '' and [db_leads].`data_name`=[db_rest].`data_name`", $fields);
			//echo mlavu_dberror();
			while($entry = mysqli_fetch_assoc($lead_q)){
		 		$id = $entry['id'];
		 		//echo '['.$id.']['.$entry['data_name'].']--['.$id.']['.$entry['disabled'].']';
				$this->leads_with_demos[$id]['data_name'] = $entry['data_name'];
				$this->leads_with_demos[$id]['id'] = $entry[$id];
				$this->leads_with_demos[$id]['disabled'] = $entry['disabled'];
		 	}

		}//set_all_leads_with_demos
		//public function set_info($info){}

		/**
		This function sets all of a given distributers personal information
		**/
		public function set_all_info($user_name, $b_check_distro_exists = FALSE){
			//error_log("set_all_info");
				//echo "info:".count($this->info)."<br />";
				$distributer = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$user_name);
				if (($distributer === FALSE || mysqli_num_rows($distributer) == 0) && $b_check_distro_exists)
					return FALSE;

				if($entry = mysqli_fetch_assoc($distributer)){
					foreach($entry as $key=>$value){
						$this->info[$key] = $value;
						//echo '</br>user=='.$key.'=>'.$value;
					}
				}
				$this->create_specials();

				$db_reseller_licenses = '`poslavu_MAIN_db`.`reseller_licenses`';
				$resellername = '`resellername`';
				$restaurantid = '`restaurantid`';

				//echo "select * from $db_reseller_licenses where $resellername='$user_name' and $restaurantid=''";
				$lic_query = mlavu_query("select * from [reseller_licenses_db] where [resellername]='[username]' and [restid]=''",
					array("reseller_licenses_db"=>$db_reseller_licenses, "resellername"=>$resellername, "username"=>$user_name, "restid"=>$restaurantid));
					//echo "select * from `poslavu_MAIN_db`.`reseller_licenses` where `resellerid`='[1]' and `restaurantid`=''",$distro_read['id'];
				if ($lic_query !== FALSE) {
					while($lic_read = mysqli_fetch_assoc($lic_query))
					{
						if($lic_read['id'] !== ''){
							$id_key = $lic_read['id'];
							foreach($lic_read as $key=>$value){
								if($key !== 'id'){
									$this->licenses[$id_key][$key]=$value;
									//echo 'licenses['.$id_key.']['.$key.']='.$value.'<br>';
								}
							}
						}
							//echo 'licenses['.$user_name.']['.$key.']='.$value.'<br>';
							//echo '<br>----------------------<br>';
						/*$license_type=$lic_read['type'];
						if( $lic_read['type']!='Platinum'){
							echo "<tr><td align='right'>";
								echo "<table cellspacing=0 cellpadding=0><tr><td align='right' style='cursor:pointer' onclick='
								if(document.getElementById(\"upgrade_".$lic_read['id']."\").style.display==\"block\")
									document.getElementById(\"upgrade_".$lic_read['id']."\").style.display = \"none\";
								else
									document.getElementById(\"upgrade_".$lic_read['id']."\").style.display = \"block\"'>
									".$license_type."
								</td><td><div id='upgrade_".$lic_read['id']."' style='display:none'>&nbsp;
								<a href='index.php?mode=upgrade_license&applyto=".$lic_read['data_name']."&license_type=".$lic_read['type']."&resturant_id=".$lic_read['id']."'>
								(upgrade license)</a></div></td></tr></table>";

							echo "</td><td width='6'>&nbsp;</td><td class='datetime'>".display_datetime($lic_read['purchased'])."</td></tr>";

						}else
							echo "<tr><td align='right'>" . $lic_read['type'] . "</td><td width='6'>&nbsp;</td><td class='datetime'>".display_datetime($lic_read['purchased'])."</td></tr>";*/

					}

				}
				$this->set_form_array_options();
		}

		public function set_form_array_options() {
			global $s_new_account_label_name_options;
			require(dirname(__file__)."/form_requirements.php");
			$this->account_labels_options = $s_new_account_label_name_options;
		}



	}



