<?php
	ini_set("display_errors",1);
	
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

	require_once(dirname(__FILE__)."/distro_user.php");
	require_once(dirname(__FILE__)."/distro_tab_container_test.php");
	
	$distrojs = '<script type="text/javascript" src="./distro_scripts/distro_test.js"></script>';
	if (isset($_GET['ben']))
		$distrojs = '<script type="text/javascript" src="./distro_scripts/distro_ben.js"></script>';
	
	$header = '<!DOCTYPE html><html><head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/dnew_test.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery.tooltip.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery.calendars.picker.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/appointment.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery-ui-1.10.0.custom.css" />
		<script type="text/javascript">'.file_get_contents("/home/poslavu/public_html/admin/distro/beta/distro_scripts/lavuLog.js").'</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.corner.js"></script>
		'.$distrojs.'
		<script type="text/javascript" src="./distro_scripts/jquery.easytabs.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/acc_simple.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery-ui-1.10.0.custom.js"></script>
		<script type="text/javascript" src="/manage/js/lavugui.js"></script>
		<script type="text/javascript" src="/manage/js/lavuform.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.flot.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.flot.time.js"></script>
		<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="./distro_scripts/excanvas.min.js"></script><![endif]-->
		<script type="text/javascript" src="./distro_scripts/jquery.validate.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery-tooltip/jquery.tooltip.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.form.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.plus.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.picker.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.picker.ext.js"></script>
		<script type="text/javascript" src="./ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="./ckeditor/config.js"></script>
		<title>DISTROCLASSTESTER</title>
		</head>
				';
	echo $header;
	
	
	$user = new distro_user();

	$user_name = 'martin';
	//echo 'not martin == '.var_dump($_GET);//$_REQUEST['notmartin'].'<br />';
	if(isset($_GET['notmartin'])){
		$user_name = $_GET['notmartin'];
		echo $user_name.'---<br />';
	}
	$user->set_leads_accounts($user_name);
	$user->set_all_info($user_name);
	
	 $is_admin = $user->get_info_attr('access');
	 //echo $is_admin .'<br />';
	 	
	new tab_container($user, $is_admin);
