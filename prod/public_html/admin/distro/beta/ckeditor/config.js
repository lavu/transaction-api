/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

/*CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};*/
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl =      '/distro/beta/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = '/distro/beta/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = '/distro/beta/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl =      '/distro/beta/kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = '/distro/beta/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = '/distro/beta/kcfinder/upload.php?type=flash';
    config.width = 500;
    config.toolbar = [
        ["Source", "-", "New Page"], 
        ["Cut", "Copy", "Paste", "PasteText", "PasteFromWord", "-", "Undo", "Redo"],
        ["Bold", "Italic", "Underline", "RemoveFormat"],
        ["NumberedList", "BulletedList"],
        ["Link", "Unlink", "Styles", "Image", "Font", "TextColor"]
    ];
};