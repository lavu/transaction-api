/**
* distro.js
* Author: Martin Tice
* Property of Lavu inc.

This file is for the distro portal at admin.poslavu.com/distro/beta/main_layout.php. (eventually admin.poslavu.com/distro/)
*****/

var global_apply_license_do_reload = false;
var global_demo_account_created_reload = false;
var global_assign_lead_do_reload = false;
var global_add_news_do_reload = false;
var global_add_lead_note_do_reload = false;

/*
*function to handle the autocomplete of lead search
*
*/
$(function() {
	var username = $('#distro_username').val();
	$.ui.autocomplete.prototype._renderItem = function( ul, item){
		//lavuLog('herro?');
		var term = this.term.split(' ').join('|');
		var re = new RegExp("(" + term + ")", "gi");
		var t = item.label.replace(re,"<b>$1</b>");
		//lavuLog('term='+term+', re='+re+', t='+t);
		return $( "<li></li>" )
		.data( "item.autocomplete", item )
			.append( "<a>" + t + "</a>" )
			.appendTo( ul );
	};//$.ui.autocomplete.prototype._renderItem = function(ul, item)
	$("#inProg_searchInput").autocomplete({
		source: "exec/inProg_search.php?distro_code="+ username,
		minLength: 2,
		async:false,
		focus: function( event, ui) {
			var val = 0;
			if(typeof(ui.item.value) !=='undefined'){
				val= ui.item.value.split(" (")[1].split(")")[0];
				//lavuLog("search focus val:: "+val);
			}else
				return;
			//lavuLog( val);
			$.ajax({
				url: "exec/searchLeadTemp.php",
				data:{ action:val,username:$('#distro_username').val()},
				type: "post",
				success: function (str){
					var username = $('#distro_username').val();
					lavuLog('searchInput success:: '+username);
					$("#in_prog_lead_desc").show();
					$('#inProg_lead_display').html('');
					$('#inProg_lead_display').html(str);
					$('#inProg_lead_display').show();
				}
			});//ajax
		},
		select: function(event, ui){
		    $("#inProg_lead_desc").show();
		    attach_accordion();
		    attach_accordion_events();
			//$("#inProg_lead_display").html('');
			//event.keyCode= 0;
			var username = $('#distro_username').val();
			lavuLog('searchInput select:'+username);
			//$(".rowResult").css("display", "");
			var val;
			if( typeof $("#inProg_searchInput").val() === "undefined")
				val= $("#inProg_searchInput").val().split(" (")[1].split(")")[0];
			else
				val= $(".floatingTempResult").find('.cCode').html();
			selected=true;
			//populateResults(val);
		}
	});//inProg_searchInput.autocomplete()
	$("#specialist_searchInput").autocomplete({
		source:  "exec/specialist_search.php",
		minLength: 2,
		async:false,
		focus: function( event, ui) {
			var val = 0;
			if(typeof(ui.item.value) !=='undefined'){
				val= ui.item.value.split(" (")[1].split(")")[0];
				//lavuLog("search Specialist focus val:: "+val);
			}else
				return;
			//lavuLog( val);
			
			$.ajax({
				url: "exec/searchSpecialistTemp.php",
				data:{ action:'singleSearch',did:val, access:'admin'},
				type: "post",
				success: function (str){
					//lavuLog(str);
					$("#reseller_desc").show();
					$('#specialist_display').html('');
					$('#specialist_display').html(str);
					$('#specialist_display').show();
				}
			});//ajax
		},
		select: function(event, ui){
		    $("#specialist_lead_desc").show();
		    attach_accordion();
		    attach_accordion_events();
		    //lavuLog('specialist search select');
		}
	});//specialist_searchInput.autocomplete()]
	$("#quote_accountlead_searchInput").autocomplete({
		source: function(request, response) {
            $.ajax({
                url: "exec/quote_accountlead_search.php",
                dataType: "json",
                data: {
                    term: request.term,
                    distro_code: $("#new_quote_form #distro_code").val(),
                    search_accounts: ($("#new_quote_form #search_accounts:checked") ? $("#new_quote_form #search_accounts:checked").val() : ''),
                    search_leads: ($("#new_quote_form #search_leads:checked") ? $("#new_quote_form #search_leads:checked").val() : ''),
                    search_quotes: ($("#new_quote_form #search_quotes:checked") ? $("#new_quote_form #search_quotes:checked").val() : ''),
                },
                success: function(data) {
                    response(data);
                }
            });
        },
		minLength: 2,
		async: false,
		/*focus: function(event, ui) {
			var autocomp;
			if (typeof(ui.item.value) !=='undefined')
				autocomp = ui.item.value.split("^");
			return;
		},*/
		select: function(event, ui) {
			var vers = '';
			if (ui.item.label.slice(0, 5) == "QUOTE") {
				var vernum = '2';
				var regex = /\s\(?[vV](\d+)\)?$/;
				var matches = ui.item.quote_name.match(regex);
				if (matches != null && matches[1] != null) vernum = parseInt(matches[1]) + 1;
				vers = ' V' + vernum;
				ui.item.quote_name = (ui.item.quote_name == "") ? ui.item.company_name + vers : ui.item.quote_name.replace(regex, '') + vers;
				$("#current_pos").val(ui.item.current_pos);
				$("#current_processor").val(ui.item.current_processor);
				$("#requested_processor").val(ui.item.requested_processor);
			}
			$("#quote_accountlead_searchInput").val(ui.item.company_name);
			$("#dataname").val(ui.item.dataname);
			$("#reseller_lead_id").val(ui.item.reseller_lead_id);
			$("#restaurantid").val(ui.item.restaurantid);
			$("#signupid").val(ui.item.signupid);
			$("#salesforce_id").val(ui.item.salesforce_id);
			$("#salesforce_opportunity_id").val(ui.item.salesforce_opportunity_id);
			$("#quote_name").val(ui.item.quote_name);
			$("#company").val(ui.item.company_name);
			$("#firstname").val(ui.item.firstname);
			$("#lastname").val(ui.item.lastname);
			$("#address").val(ui.item.address);
			$("#city").val(ui.item.city);
			$("#state").val(ui.item.state);
			$("#zip").val(ui.item.zip);
			if (ui.item.country != "") $("#country").val(ui.item.country);
			$("#phone").val(ui.item.phone);
			$("#email").val(ui.item.email);
		}
	});//quote_accountlead_searchInput.autocomplete()]
	$("#associated_account_reseller").click( function() {
		$.ajax( {
			url: 'exec/quote_responder.php',
			data: {
				action: 'get',
				item: 'reseller',
				distro_code: $('#distro_code').val(),
			},
			success: function(data) {
				var distro = JSON.parse(data);
				$("#dataname").val(distro.demo_account_dataname);
				$("#reseller_lead_id").val(distro.reseller_lead_id);
				$("#restaurantid").val(distro.restaurantid);
				$("#signupid").val(distro.signupid);
				$("#salesforce_id").val(distro.salesforce_id);
				$("#salesforce_opportunity_id").val(distro.salesforce_opportunity_id);
				$("#quote_name").val(distro.company);
				$("#company").val(distro.company);
				$("#firstname").val(distro.f_name);
				$("#lastname").val(distro.l_name);
				$("#address").val(distro.address);
				$("#city").val(distro.city);
				$("#state").val(distro.state);
				$("#zip").val(distro.postal_code);
				$("#phone").val(distro.phone);
				$("#email").val(distro.email);
				$("#same_billing_address").prop('checked', true);
				setBillingAddress(true);
			}
		} );
	} );
	$("#associated_account_existing").click( function() {
		$("#dataname").val('');
		$("#reseller_lead_id").val('');
		$("#restaurantid").val('');
		$("#signupid").val('');
		$("#salesforce_id").val('');
		$("#salesforce_opportunity_id").val('');
		$("#quote_name").val('');
		$("#company").val('');
		$("#firstname").val('');
		$("#lastname").val('');
		$("#address").val('');
		$("#city").val('');
		$("#state").val('');
		$("#zip").val('');
		$("#phone").val('');
		$("#email").val('');
		$("#billing_firstname").val('');
		$("#billing_lastname").val('');
		$("#billing_address").val('');
		$("#billing_city").val('');
		$("#billing_state").val('');
		$("#billing_zip").val('');
		$("#billing_country").val('');
		$("#same_billing_address").prop('checked', false);
		setBillingAddress(false);
	} );
	$("#associated_account_new").click( function() {
		$("#dataname").val('');
		$("#reseller_lead_id").val('');
		$("#restaurantid").val('');
		$("#signupid").val('');
		$("#salesforce_id").val('');
		$("#salesforce_opportunity_id").val('');
		$("#quote_name").val('');
		$("#company").val('');
		$("#firstname").val('');
		$("#lastname").val('');
		$("#address").val('');
		$("#city").val('');
		$("#state").val('');
		$("#zip").val('');
		$("#phone").val('');
		$("#email").val('');
		$("#billing_firstname").val('');
		$("#billing_lastname").val('');
		$("#billing_address").val('');
		$("#billing_city").val('');
		$("#billing_state").val('');
		$("#billing_zip").val('');
		$("#billing_country").val('');
		$("#same_billing_address").prop('checked', false);
		setBillingAddress(false);
	} );
	$("#quote_pdf_form_searchInput").autocomplete({

		source: function(request, response) {
			$.ajax({
				url: "exec/quote_accountlead_search.php",
				dataType: "json",
				data: {
					term: request.term,
					distro_code: $("#quote_select_account_distro_code").val(),
					search_accounts: "1",
					search_leads: "",
					search_quotes: "",
				},
				success: function(data) {
					response(data);
				}
			});
		},
		minLength: 2,
		async: false,
		select: function(event, ui) {
			$("#quote_pdf_form_searchInput").val(ui.item.company_name);
			$("#quote_pdf_dataname").val(ui.item.dataname);
			$("#quote_pdf_restaurantid").val(ui.item.restaurantid);
			$("#quote_pdf_signupid").val(ui.item.signupid);
			$("#submit_add_quote_pdf").attr('disabled', false);
		}
	}); //quote_select_account_searchInput.autocomplete()]
	var dcode = $('#account_search_required_data').attr('dcode');
	var did = $('#account_search_required_data').attr('did');
	$("#account_searchInput").autocomplete({
			source:  "exec/account_search.php?dcode="+dcode+"&did="+did,
			minLength: 2,
			async:false,
			focus: function( event, ui) {
				var val = 0;
				if(typeof(ui.item.value) !=='undefined'){
					val= ui.item.value.split(" (")[1].split(")")[0];
					lavuLog("search focus val:: "+val);
				}else
					return;
				lavuLog(val);
				var dcode = $('#account_search_required_data').attr('dcode');
				var did = $('#account_search_required_data').attr('did');
				var daccess = $('#account_search_required_data').attr('daccess');
				$.ajax({
					url: "exec/searchAccountTemp.php",
					data:{ action:'focus_search',rid:val,access:'admin', distro_code:dcode,distro_access:daccess,distro_id:did},
					type: "post",
					success: function (str){
						//lavuLog("searchAccountTemp ajax success:: "+str);
						$("#accnts_desc").show();
						$('#accounts_display').html('');
						$('#accounts_display').html(str);
						$('#accounts_display').show();
					}
				});//ajax
	            
			},
			select: function(event, ui){
			    $("#inProg_lead_desc").show();
			    attach_accordion();
			     attach_accordion_events();
				//$("#inProg_lead_display").html('');
				//event.keyCode= 0;
				//$(".rowResult").css("display", "");
				var val;
			}
	});//account_searchInput.autocomplete()
});//search thing..
/*
*
*
*/
function attach_accordion(){
	$('.acc_head:even').css('background-color','#f1f5ee');
	//$('.acc_quote_content:even').css('background-color','#f1f5ee');
	$('.acc_arrow').click(function() {
		lavuLog("acc_butt_clicked");
		//REMOVE THE ON CLASS FROM ALL BUTTONS
		$(this).parent().removeClass('on');
		  
		//NO MATTER WHAT WE CLOSE ALL OPEN SLIDES
	 	$('.accordionContent').slideUp('normal');

		//IF THE NEXT SLIDE WASN'T OPEN THEN OPEN IT
		if($(this).parent().next().is(':hidden') == true) {
		
			//CHANGE LEFT ARROW TO DOWN ARROW
			$(this).attr('src', './images/arrow_down.png');
			//ADD THE ON CLASS TO THE BUTTON
			$(this).parent().addClass('on');
			  
			//OPEN THE SLIDE
			$(this).parent().next().slideDown('normal');
		 } else{
		 	//CHANGE DOWN ARRROW TO LEFT ARROW
			 $(this).attr('src', './images/arrow_left.png');			 
		 }
		  
	 });
	$('.accordionButton').mouseover(function() {
		$(this).addClass('over');
		
	//ON MOUSEOUT REMOVE THE OVER CLASS
	}).mouseout(function() {
		$(this).removeClass('over');										
	});	
	$('.accordionContent').hide();
}//attach_accordion()
function attach_accordion_events(){

	$('.need_follow_up12').css('background-color','yellow');
	$('.need_follow_up24').css('background-color','orange');
	$('.need_follow_up48').css('background-color','red');
	//remove_lead_event
	$('.remove_lead').click(function(){
		var l_id = $(this).attr('l_id');
		lavuLog("remove:: "+l_id);
		var conf = confirm("Are you sure you want to remove this lead?");
		if(conf){
			$.ajax("./exec/remove_lead.php", {
				type: "POST",
				data: {
            		leadid: l_id
            	},
            	cache: false,
            	async: false,
            	success: function(data){
		        	lavuLog("removed:"+data);
		        	window.location.reload();
		        }
		   });//ajax
		}
	});//.remove_lead.click()
	//remove_quote.click() event
	$('.remove_quote').click(function(){
		var q_id = $(this).attr('q_id');
		var dn = $(this).attr('dn');
		var sfqi = $(this).attr('sfqi');
		lavuLog("remove: "+q_id+" sfqi="+sfqi);
		var conf = confirm("Are you sure you want to remove this quote?");
		if(conf){
			$.ajax("./exec/remove_quote.php", {
				type: "POST",
				data: {
            		reseller_quote_id: q_id,
            		dataname: dn,
            		salesforce_quote_id: sfqi
            	},
            	cache: false,
            	async: false,
            	success: function(data){
		        	lavuLog("removed:"+data);
		        	window.location.reload();
		        }
		   });//ajax
		}
	});//.remove_quote.click()
	//jab_distro_event
	$('.jab_distro').click(function(){
		var bus_name = $(this).attr('b_name');
		var bus_email = $(this).attr('email');
		var bus_phone = $(this).attr('phone');
		var contact = $(this).attr('contact');
		var reseller = $(this).attr('reseller');
		//lavuLog(bus_name+bus_email+bus_phone+contact);
		$.ajax({
			type:"POST",
			url:"./exec/jab_distro.php",
			data:{
				company:bus_name,
				email:bus_email,
				phone:bus_phone,
				name:contact,
				reseller:reseller
			},
			cache:false,
			success:function(msg){
				lavuLog("jabby!! "+msg);
				alert(" jab was sent!");
				
			},
			error:function(msg){
				lavuLog("jabby error!! "+msg);
				alert(" jab was sent!");
			}
		});//post
	});//jab_distro.click
	//assign_lead
	$('.assign_lead').click(function(){
		lavuLog("open assign lead form");
		sort_distro_options();
		var jlead_id_container = $(this).parent().siblings("[name=lead_on_lead_id]");
		var lead_id = jlead_id_container.val();
		lavuLog(lead_id);

		$('#assign_lead_lead_id').val(lead_id);
		//apply_license_form();
		assign_lead_form();
	});//assign_lead
	//notes_icon.click()
	$('.notes_icon').click(function(){
		var lead_id = $(this).attr('comp_id');
		var reseller = $(this).attr('reseller');
		var note_string = $(this).attr('notes');
		//note_string = note_string.replace("__+__", "\n");
		//note_string = note_string.replace("_____", '"');
		//var contact= $(this).attr('contact');
		//var contacted= $(this).attr('contacted');
		$('#lead_note_previous').val(note_string);
		var each_note = note_string.split("*|*");
		var note_date = '';
		var note_title = '';
		var note_content = '';

		/*var div_for_contact_note = "<div class='lead_note_date'><span class='lead_note_display_title'>DATE:</span>"+contacted+"</div><div class='lead_note_title'><span class='lead_note_display_title'>TITLE:</span> CONTACT NOTE</div><div class='lead_note_content'><span class='lead_note_display_title'>CONTENT:</span><br />"+contact+"</div>";*/


		var display_all_notes = '<div style="text-align:center; font:20px Verdana;color:#439789;">PREVIOUS NOTES:</div>';

		each_note.forEach(function(entry){
			var split_note = entry.split(':*:');
			note_date = split_note[0];
			note_title = split_note[1];
			note_content = split_note[2];
			if(note_date != ''){
				lavuLog("DATE:  "+note_date+", title:"+note_title+", content:"+note_content);
				display_all_notes = display_all_notes.concat("<div class='lead_note_date'><span class='lead_note_display_title'>DATE:</span>"+note_date+"</div><div class='lead_note_title'><span class='lead_note_display_title'>TITLE:</span> "+note_title+"</div><div class='lead_note_content'><span class='lead_note_display_title'>CONTENT:</span><br />"+note_content+"</div>");
			}
		});
		//lavuLog(":::"+display_all_notes);
		$('#lead_notes_display').html(display_all_notes);
		//lavuLog('lead_id='+lead_id+', reseller='+reseller);
		$('#lead_note_reseller').val(reseller);
		$('#lead_note_lead_id').val(lead_id);
		add_lead_note_form();
	});//.notes_icon
	//unassign.click()
	$('.unassign').click(function(){
		var lead_id = $(this).attr('lead_id');
	    var action = 'unassign';
	    var thisid = $(this).attr('id');
		$.ajax({
			type:"POST",
			url:"./exec/time_milestones.php",
			data:{lead_id:lead_id,action:action},
			success:function(msg){
				lavuLog(" success!! "+msg+",  #un_lead_head_"+thisid);
				//$('body').replaceWith(msg);
				//window.location.reload();
				$('.acc_head:even').css('background-color','#f1f5ee');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up12');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up24');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up48');
				$('.need_follow_up12').css('background-color','yellow');
				$('.need_follow_up24').css('background-color','orange');
				$('.need_follow_up48').css('background-color','red');
				window.location.reload();	
			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
		});//ajax
	});//unassign.click();
	$('.allow_mercury').click(function(){
		var lead_id = $(this).attr('lead_id');
	    var action = 'mercury_permission_toggle';
	    var thisid = $(this).attr('id');
	    var current_permission = $(this).attr('permission');
	    
	    $.ajax({
			type:"POST",
			url:"./exec/time_milestones.php",
			data:{lead_id:lead_id,action:action,current:current_permission},
			success:function(msg){
				lavuLog(" success!! "+msg+",  #"+thisid);
				//$('body').replaceWith(msg);
				//window.location.reload();
				/*
				$('.acc_head:even').css('background-color','#f1f5ee');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up12');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up24');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up48');
				$('.need_follow_up12').css('background-color','yellow');
				$('.need_follow_up24').css('background-color','orange');
				$('.need_follow_up48').css('background-color','red');
				window.location.reload();
				*/

				current_permission = $('#'+thisid).attr('permission');
				lavuLog(current_permission);
				
				if(current_permission == 'allowed' ){
					$('#'+thisid).text('NO MERC');
					$('#'+thisid).attr('permission', 'notallowed');
					$('#'+thisid).css('color','red');
				}else{
					$('#'+thisid).text('MERC OK');
					$('#'+thisid).attr('permission', 'allowed');
					$('#'+thisid).css('color','red');
					$('#'+thisid).css('color','#0684b5');
				}

				var username = $('#distro_username').val();
				var distro_id = $('#distro_id').val();
				var from_mercury_perm = (current_permission == 'allowed') ? 'MERC OK' : 'NO MERC';
				var to_mercury_perm = (current_permission != 'allowed') ? 'MERC OK' : 'NO MERC';

				lavuLog('Logging distro (username = '+username+', id='+distro_id +') that changed Mercury permission: '+ from_mercury_perm +' => '+ to_mercury_perm );

				$.ajax({
					type:"POST",
					url:"./exec/add_to_history.php",
					data:{
						action:"toggled lead mercury perm",
						details:"Changed Mercury permission for lead "+ lead_id +": "+ from_mercury_perm +" => "+ to_mercury_perm,
						reseller_id:distro_id,
						reseller_name:username
					},
					cache:false,
					success:function(msg){
						lavuLog("success::"+msg);
					},
					error:function(msg){
						lavuLog("error"+msg);
					}
				});//ajax
			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
		});//ajax

	});//allow_mercury.click
	$('.lead_sort_by').click(function(){
		var sort_by = $(this).attr('sort_by');
		console.log('sort by choice:: '+sort_by);
		sort_in_prog_leads_by_choice(sort_by);
	});//lead_sort_by.click
	$('.d_level_stars').click(function(){
		lavuLog("d_level_stars click!");
		var span_id = "#"+$(this).attr('val');
		lavuLog('show '+span_id);
		$(this).hide();
		$(span_id).show();
	});
	$('.change_d_level_select').change(function(){
		var form_id = "#"+$(this).attr('val');
		var prompt_id = form_id+"_prompt";
		$(this).parent().parent().hide();
		$(prompt_id).show();
		$('#submit_changes').show();
		$.ajax({
			type:"POST",
			url:"./exec/edit_reseller.php",
			data: $(form_id).serialize(),
			cache: false,
			success:function(msg){
				lavuLog("success!!"+msg);
				if(msg === '')
				{
					$(prompt_id).html('<img src="./images/stars_0.png" alt="not_set" />');
				}else if(msg == 'agree')
				{
					$(prompt_id).html('<span style="color:red;">AGREE</span>');
				}else if(msg == 'ingram')
				{
					$(prompt_id).html('<span style="color:red;">INGRAM</span>');
				}else if(msg == 'bluestar')
				{
					$(prompt_id).html('<span style="color:blue;">BLUESTAR</span>');
				}else
				{
					$(prompt_id).html('<img src="./images/stars_'+msg+'.png" alt="not_set" />');
				}
			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
		});//ajax
	});
	$('.remove_reseller').click(function(){
		var user = $(this).attr('username');
		var a_user = "#activate_"+user;
		lavuLog("uname::"+user);
		$(this).hide();
		$(a_user).show();
		$.ajax({
			type:"POST",
			url:"./exec/edit_reseller.php",
			data:{
				remove_reactivate:"remove",
				username:user
			},
			cache:false,
			success:function(msg){
				lavuLog("success::"+msg);
				$('#submit_changes').show();
			},
			error:function(msg){
				lavuLog("error"+msg);
			}
		});//ajax
	});//remove_reseller.click()
	$('.activate_reseller').click(function(){
		$(this).hide();
		var user = $(this).attr('username');
		var d_user = "#remove_"+user;
		$(d_user).show();
		$.ajax({
			type:"POST",
			url:"./exec/edit_reseller.php",
			data:{
				remove_reactivate:"activate",
				username:user
			},
			cache:false,
			success:function(msg){
				lavuLog("success::"+msg);
				$('#submit_changes').show();
			},
			error:function(msg){
				lavuLog("error"+msg);
			}
		});//ajax
	});//activate_reseller.click()
	$('#submit_changes').click(function(){
		$(this).hide();
		window.location.reload();
	});
	$('.upgrade_license_btn').click(function(){
		lavuLog("open apply account form");
		var account_dataname = $(this).parent().parent().children("input[name=dataname]").val();
		var available_upgrades = $(this).siblings("input[name='available_upgrades']").val();
		$("#apply_license_form").find("input[name=apply_license_dname]").val(account_dataname);
		$("#apply_license_form").find("input[name=available_licenses_to_apply]").val(available_upgrades);
		$("#apply_license_form").find("input[name=licenseupgrade_string]").val("an upgrade");
		apply_license_form();
	});
	$('.apply_license_btn').click(function(){
		lavuLog("open apply account form:: "+$(this).attr('val'));
		var dname = $(this).attr('val');
		var license_level = $(this).html().toLowerCase();
		var available_licenses = $(this).siblings("input[name='available_licenses']").val();
		var javailable_special_licenses = $(this).siblings("input[name='available_special_licenses']");
		var available_special_licenses = ((javailable_special_licenses.length > 0 && javailable_special_licenses.val() !== '') ? javailable_special_licenses.val() : '');
		license_level = license_level.charAt(0).toUpperCase() + license_level.slice(1);
		lavuLog("license level:: "+license_level);
		lavuLog("available licenses:: "+available_licenses);
		$('#apply_license_dname').val(dname);
		$('#apply_license_company_name').text(dname);
		$("select[name=apply_lic_available_select]").val(license_level);
		$("#apply_license_form").find("input[name=available_licenses_to_apply]").val(available_licenses);
		$("#apply_license_form").find("input[name=available_special_licenses_to_apply]").val(available_special_licenses);
		$("#apply_license_form").find("input[name=licenseupgrade_string]").val("a license");
		apply_license_form();
	});
	$('.input_remove_lead').click(function(){
		lavuLog("checkbox clicked");
		$('.remove_all_selected_leads').show();
	});//remove_lead_early.click
	$('.remove_all_selected_leads').click(function(){
		remove_selected_leads();
	});
	$('#resource_dl_edit_start').click(function(){
		resource_dl_show_edit();
	});//resource_dl_edit_start
	$('#resource_dl_edit_finish').click(function(){
		resource_dl_edit_apply();
		resource_dl_hide_edit();
	});//dl_edit_finish
	$('#resource_dl_edit_cancel').click(function(){
		resource_dl_hide_edit();
	});//dl_edit_cancel
	$('#resources_tab_top_thing').click(function(){

		var username = $('#distro_username').val();
		var distro_id = $('#distro_id').val();
		lavuLog('Distro username = '+username+', id='+distro_id);
		/*
		$values['action'] = '';
		$values['details'] = '';
		$values['reseller_id'] = '';
		$values['reseller_name'] = '';
		$values['b_update'] = FALSE;
		*/
		$.ajax({
			type:"POST",
			url:"./exec/add_to_history.php",
			data:{
				action:"New Resources section visited",
				details:username+" visited the new resources section",
				reseller_id:distro_id,
				reseller_name:username
			},
			cache:false,
			success:function(msg){
				//lavuLog("success::"+msg);
			},
			error:function(msg){
				//lavuLog("error"+msg);
			}
		});//ajax
	
	});//resources_tab_top_thing.click
}//attach_accordion_click_events()
function filter_inProgSearch(){
	//var filters = $('#inProg_filter_form').serialize();
	//lavuLog("kick me");
	$.ajax({
		url: "exec/searchLeadTemp.php",
		data:$('#inProg_filter_form').serialize() +"&username="+ $('#distro_username').val(),
		type: "post",
	}).done(function(str){
		//lavuLog('boob '+str);
		$('#inProg_lead_display').html('');
		$('#inProg_lead_display').html(str);
		$('#inProg_lead_display').show();
		$('#in_prog_lead_desc').show();
		attach_accordion();
		attach_accordion_events();
	});//ajax
}//filter_inProgSearch()
function filter_specialistSearch(){
	lavuLog("account Search");
	$.ajax({
		url: "exec/searchSpecialistTemp.php",
		data:$('#specialist_filter_form').serialize(),
		type: "post",
	}).done(function(str){
		//lavuLog('searchAccountTemp response:: '+str);
		$('#specialist_display').html('');
		$('#specialist_display').html(str);
		$('#specialist_display').show();
		$('#reseller_desc').show();
		attach_accordion();
		attach_accordion_events();
	});//ajax
}//filter_specialistSearch()
function filter_accountSearch(){
	lavuLog("account Search");
	$.ajax({
		url: "exec/searchAccountTemp.php",
		data:$('#account_filter_form').serialize(),
		type: "post",
	}).done(function(str){
		//lavuLog('searchAccountTemp response:: '+str);                
		$('#account_array').html('');
		$('#account_array').html(str);
		$('#account_array').show();
		attach_accordion();                
		attach_accordion_events();
	});//ajax
}//filter_account_search()
function apply_license_form(){
	var jlicenseform = $("#apply_license_form");
	var s_available_licenses = jlicenseform.find("input[name=available_licenses_to_apply]").val();
	var s_available_special_licenses = jlicenseform.find("input[name=available_special_licenses_to_apply]").val();
	var s_licenseupgrade_string = jlicenseform.find("input[name=licenseupgrade_string]").val();
	jlicenseform.find("input[name=available_licenses_to_apply]").val('');
	while (jlicenseform.find("font.remove_me").length > 0)
		jlicenseform.find("font.remove_me").remove();
	// copy the old select
	if (jlicenseform.find("select[name=apply_lic_available_select_backup]").length > 0) {
		while (jlicenseform.find("select[name=apply_lic_available_select]").length > 0) {
			jlicenseform.find("select[name=apply_lic_available_select]").remove();
			lavuLog("removing select");
		}
		duplicate_jelement_and_change_name(jlicenseform.find("select[name=apply_lic_available_select_backup]"), "apply_lic_available_select");
		jlicenseform.find("select[name=apply_lic_available_select]").show();
	}
	var b_disable_select = false;
	// check if only certain licenses are available
	lavuLog(s_available_licenses);
	if (s_available_licenses !== '' || s_available_special_licenses !== '') {
		// remove previous backups
		while (jlicenseform.find("select[name=apply_lic_available_select_backup]").length > 0) {
			jlicenseform.find("select[name=apply_lic_available_select_backup]").remove();
			lavuLog("removing backup");
		}
		// create and hide the backup
		duplicate_jelement_and_change_name(jlicenseform.find("select[name=apply_lic_available_select]"), "apply_lic_available_select_backup");
		jlicenseform.find("select[name=apply_lic_available_select_backup]").hide();
		// remove invalid options
		var i = 0;
		var jselect = jlicenseform.find("select[name=apply_lic_available_select]");
		var a_select_options = jselect.children();
		var a_available_licenses = s_available_licenses.split('|');
		for (i = 0; i < a_available_licenses.length; i++)
			a_available_licenses[i] = a_available_licenses[i].replace(/[0-9]/g, '');
		lavuLog("available licenses: ");
		lavuLog(a_available_licenses);
		for (i = 0; i < a_select_options.length; i++) {
			var s_select_option_val = $(a_select_options[i]).val();
			s_select_option_val = s_select_option_val.replace(/[0-9]/g, '');
			lavuLog("select option: "+s_select_option_val);
			if (jQuery.inArray(s_select_option_val, a_available_licenses) < 0)
				$(a_select_options[i]).remove();
		}
		if (jselect.children().length === 0) {
			b_disable_select = true;
		}
		// get licenses that are permitted by id as a_avail_lic
		var json_id = s_licenseupgrade_string.match("upgrade") ? "distro_upgrades_json" : "distro_licenses_json";
		var a_licenses = JSON.parse($('#'+json_id).val().replace(/'/g, '"'));
		var special_text = (s_available_special_licenses !== '' ? 'special licensing ' : '');
		var a_lic_ids = (s_available_special_licenses !== '' ? s_available_special_licenses.split(',') : s_available_licenses.split('|'));
		lavuLog(a_lic_ids);
		var a_avail_lics = [];
		if (a_lic_ids.length > 0) {
			jselect.children().remove();
			$.each(a_lic_ids, function(k, s_id) {
				var i_id = parseInt(s_id, 10);
				if (a_licenses[i_id]) {
					a_licenses[i_id].id = i_id;
					a_avail_lics.push(a_licenses[i_id]);
				}
			});
		}
		// if only certain licenses are available, then show only those licenses
		if (a_avail_lics.length > 0) {
			// sort them by name
			var sortByName = function(a,b) {
				var aName = a.printed_name;
				var bName = b.printed_name;
				if (aName == bName)
					return 0;
				if (aName == 'Platinum')
					return -1;
				if (bName == 'Platinum')
					return 1;
				if (aName == 'Gold')
					return -1;
				if (bName == 'Gold')
					return 1;
				return (aName < bName ? -1 : (aName > bName ? 1 : 0));
			};
			a_avail_lics.sort(sortByName);
			// remove all options
			jselect.children().remove();
			// create the new options
			$.each(a_avail_lics, function(k,v) {
				jselect.append("<option value='"+v.id+"'>"+v.printed_name+" ("+special_text+"#"+(250000+v.id)+")</option>");
			});
			// tell the apply license script that it should be accepting an id
			jselect.siblings('input[name=is_id]').val(1);
			b_disable_select = (jselect.children().length === 0);
		}
	} else {
		b_disable_select = true;
	}
	// tell the user that they must purchase a license/upgrade
	if (b_disable_select) {
		jlicenseform.find("select[name=apply_lic_available_select]").remove();
		$("#select_arrow_bg_apply_license").append('<font class="remove_me" style="font-style:italic;">You must first purchase '+s_licenseupgrade_string+' from the licenses tab.</font>');
	}
	// display the form
	$(".opaque_modal_bg").css('height','3000px');
	$(".opaque_modal_bg").css('width', '100%');
	$(".opaque_modal_bg").css("display","block");
	$("#apply_license_form_div").css("display","block");
}//apply_license_form()
function done_apply_license_form(){
	lavuLog('done_apply_license_form()');
	$('#apply_license_form').show();
	$('#success_apply_license_form').html(' ');
	$('#success_apply_license_form').hide();
	$('#ok_app_license').hide();
	$(".opaque_modal_bg").css("display","none");
	$("#apply_license_form_div").css("display","none");
	if (global_apply_license_do_reload){
		window.location.reload();
	}
}//done_apply_license_form()
function add_lead_note_form(){
		$(".opaque_modal_bg").css('height','3000px');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
        $('#lead_note_form').show();
		$("#lead_note_form_div").css("display","block");
}//add_lead_note_form()
function add_quote_pdf_form(hideSelectAccount){
		$("#quote_pdf_form")[0].reset();
		$(".opaque_modal_bg").css('height','3000px');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
        $('#quote_pdf_form').show();
		$("#quote_pdf_form_div").css("display","block");
		if (hideSelectAccount) {
			$("#quote_pdf_form_searchInput_label").hide();
			$("#quote_pdf_form_searchInput").hide();
			$("#submit_add_quote_pdf").prop('disabled', false);
			$("#quote_pdf_form_searchInput").prop('required', false);
		}
		else {
			$("#quote_pdf_form_searchInput_label").show();
			$("#quote_pdf_form_searchInput").show();
			$("#submit_add_quote_pdf").prop('disabled', true);
			$("#quote_pdf_form_searchInput").prop('required', true);
		}
}//add_quote_pdf_form()
function done_add_lead_note_form(){
	lavuLog('done_lead_note_form()');
	$('#lead_note_form').show();
    $('#success_lead_note_form').html(' ');
    $('#success_lead_note_form').hide();
    $('#ok_add_lead_note').hide();
	$(".opaque_modal_bg").css("display","none");
	$("#lead_note_form_div").css("display","none");
	if (global_add_lead_note_do_reload){
		window.location.reload();
	}
}//done_add_lead_note_form()
function done_add_quote_pdf_form(){
	lavuLog('done_add_quote_pdf_form()');
	$('#quote_pdf_form').show();
    $('#success_quote_pdf_form').html(' ');
    $('#success_quote_pdf_form').hide();
    $('#ok_add_quote_pdf').hide();
	$(".opaque_modal_bg").css("display","none");
	$("#quote_pdf_form_div").css("display","none");
	if (global_assign_lead_do_reload){
		window.location.reload();
	}
}//done_add_quote_pdf_form()
function assign_lead_form(){
	$(".opaque_modal_bg").css('height','3000px');
	$(".opaque_modal_bg").css('width', '100%');
	$(".opaque_modal_bg").css("display","block");
	$("#assign_lead_form_div").css("display","block");
}//assign_lead_form()
function done_assign_lead_form(){
	lavuLog('done_assign_lead_form()');
	$('#assign_lead_form').show();
    $('#success_assign_lead_form').html(' ');
    $('#success_assign_lead_form').hide();
    $('#ok_assign_lead').hide();
	$(".opaque_modal_bg").css("display","none");
	$("#assign_lead_form_div").css("display","none");
	if (global_assign_lead_do_reload){
		window.location.reload();
	}
}//done_assign_lead_form()
function sort_in_prog_leads_by_choice(choice){
		var in_prog_heads = $('.in_prog_head');
		var in_prog_contents = $('.in_prog_content');
		var head_stats = in_prog_heads.map(function(_,o){
				return {
					st : $(o).attr(choice),
					id : $(o).attr('id'),
					o : o
				};
			}).get();
			head_stats.sort(function(o1, o2){
				return o1.st > o2.st ? 1 :  o1.st < o2.st ? -1 : 0;
			});//head_stats.sort
			//console.log(head_stats.toString());
			head_stats.map(function(entry){
				//console.log("array :: "+entry.id.replace('head', 'content')+", "+entry.st);
				//console.log(entry.o);
				$('#'+entry.id).remove();
				$('.in_progress_wrapper').append(entry.o);
				var temp_cont = $('#'+entry.id.replace('head', 'content'));
				$('#'+entry.id.replace('head', 'content')).remove();
				$('.in_progress_wrapper').append(temp_cont);


			});
}//sort_in_prog_leads_by_choice

//
//resource stuff
//
function parse_resource_files(){
	$('.resources_home_btn').hide();
	$('.resources_category_section').hide();
	$('.resources_home_btn').css('margin','auto');
	$('.resources_home_btn').css('top','58px');
	$('.resources_home_btn').css('padding','7px');
	$('#resources_home_section_wrapper').show();
	$('.first_subsection_closed_arrow').show();
	$('.first_subsection_open_arrow').hide();
	$('.first_subsection_closed_arrow_featured').hide();
	$('.first_subsection_open_arrow_featured').show();
	$('.empty_section_message').hide();
	$('#allFeatured_content').show();
	//lavuLog("Parsing resource files...");
	$.ajax({
		url:"./exec/parse_resources.php",
		type:"POST",
		data:{'resource_action':"parse_all"},
        success:function(msg){
	        //lavuLog("parse resources response::  "+msg);
	        //parse_common_ajax_responses(msg);
	        
	        var file_json = $.parseJSON(msg);

	        //lavuLog("sales string:: "+file_json.sales);

	        //$('#sales_resources_contents').html('');
	        $('#sales_resources_contents').html(file_json.sales);

	        //$('#maketing_resources_contents').html('');
	        $('#marketing_resources_contents').html(file_json.marketing);

	       // $('#featured_resources_contents').html('');
	        $('#featured_resources_contents').html(file_json.featured);

	        $('#support_resources_contents').html(file_json.support);

	        //
	        //
	        $('#salesResellerAgreements_content').html('');
	        //lavuLog("salesResellerAgreements_content length= "+file_json.salesResellerAgreements_content.length);
	        if(file_json.salesResellerAgreements_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_reselleragreements').show();
	        }
	        $('#salesResellerAgreements_content').html(file_json.salesResellerAgreements_content);
	        //$('#salesResellerAgreements_content').show();

	        $('#salesUserGuides_content').html('');
	        if(file_json.salesUserGuides_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_userguides').show();
	        }
	        //lavuLog("salesUserGuides_content length= "+file_json.salesUserGuides_content.length);
	        $('#salesUserGuides_content').html(file_json.salesUserGuides_content);

	        $('#salesCookbook_content').html('');
	        if(file_json.salesCookbook_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_cookbook').show();
	        }
	        //lavuLog("salesCookbook_content length= "+file_json.salesCookbook_content.length);
	        $('#salesCookbook_content').html(file_json.salesCookbook_content);

	        $('#salesComparisonCharts_content').html('');
	        if(file_json.salesComparisonCharts_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_comparisoncharts').show();
	        }
	        //lavuLog("salesComparisonCharts_content length= "+file_json.salesCookbook_content.length);
	        $('#salesComparisonCharts_content').html(file_json.salesComparisonCharts_content);

	        $('#salesDefeatList_content').html('');
	        if(file_json.salesDefeatList_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_defeatlist').show();
	        }
	        //lavuLog("salesDefeatList_content length= "+file_json.salesDefeatList_content.length);
	        $('#salesDefeatList_content').html(file_json.salesDefeatList_content);

	        $('#salesNicheSellingPoints_content').html('');
	        if(file_json.salesNicheSellingPoints_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_nichesellingpointspoints').show();
	        }
	        //lavuLog("salesnichesellingpoints_content length= "+file_json.salesBusinessVerticalSalesPoints_content.length);
	        $('#salesNicheSellingPoints_content').html(file_json.salesNicheSellingPoints_content);

	        $('#salesMaintainence_content').html('');
	        if(file_json.salesMaintainence_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_maintainence').show();
	        }
	        //lavuLog("file_json.salesMaintainence_content length= "+file_json.salesMaintainence_content.length);
	        $('#salesMaintainence_content').html(file_json.salesMaintainence_content);

	        $('#salesVideos_content').html('');
	        if(file_json.salesVideos_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_videos_s').show();
	        }
	        //lavuLog("file_json.salesVideos_content length= "+file_json.salesVideos_content.length);
	        $('#salesVideos_content').html(file_json.salesVideos_content);

	        //$('#salesNotDefined_content').html('');
	        //if(file_json.salesResellerAgreements_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	//$('.empty_reselleragreements').show();
	        //}
	        $('#salesNotDefined_content').html(file_json.salesNotDefined_content);

	        //
	        //
	        $('#marketingFeaturesList_content').html('');
	        if(file_json.marketingFeaturesList_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_featureslist').show();
	        }
	        //lavuLog("file_json.salesVideos_content length= "+file_json.salesVideos_content.length);
	        $('#marketingFeaturesList_content').html(file_json.marketingFeaturesList_content);
	        //
	        $('#marketingGraphics_content').html('');
	        if(file_json.marketingGraphics_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_graphics').show();
	        }
	        $('#marketingGraphics_content').html(file_json.marketingGraphics_content);
	        //$('#marketingGraphics_content').show();
	        //
	        $('#marketingLogos_content').html('');
	        if(file_json.marketingLogos_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_logos').show();
	        }
	        $('#marketingLogos_content').html(file_json.marketingLogos_content);
	        //$('#marketingLogos_content').show();
	        //
	        $('#marketingHowToLavu_content').html('');
	        if(file_json.marketingHowToLavu_content == undefined){
	        	lavuLog("howtolavu is empty");
	        	$('.empty_howtolavu').show();
	        }
	        $('#marketingHowToLavu_content').html(file_json.marketingHowToLavu_content);
	        //
	        $('#marketingMarketingText_content').html('');
	        if(file_json.marketingMarketingText_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_marketingtext').show();
	        }
	        $('#marketingMarketingText_content').html(file_json.marketingMarketingText_content);
	        //
	        $('#marketingPhotos_content').html('');
	        if(file_json.marketingPhotos_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_photos').show();
	        }
	        $('#marketingPhotos_content').html(file_json.marketingPhotos_content);
	        //
	        $('#marketingPricing_content').html('');
	        if(file_json.marketingPricing_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_pricing').show();
	        }
	        $('#marketingPricing_content').html(file_json.marketingPricing_content);
	        //
	        $('#marketingPrintMedia_content').html('');
	        if(file_json.marketingPrintMedia_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_printmedia').show();
	        }
	        $('#marketingPrintMedia_content').html(file_json.marketingPrintMedia_content);
	        //
	        $('#marketingTestimonials_content').html('');
	        if(file_json.marketingTestimonials_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_testimonials').show();
	        }
	        $('#marketingTestimonials_content').html(file_json.marketingTestimonials_content);
	        //
	        $('#marketingVideos_content').html('');
	        if(file_json.marketingVideos_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_videos_m').show();
	        }
	        $('#marketingVideos_content').html(file_json.marketingVideos_content);
	        //
	        $('#marketingWebsiteComponents_content').html('');
	        if(file_json.marketingWebsiteComponentss_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_websitecomponents').show();
	        }
	        $('#marketingWebsiteComponents_content').html(file_json.marketingWebsiteComponents_content);
	        //
	        //$('#marketingNotDefined_content').html('');
	        //if(file_json.salesResellerAgreements_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	//$('.empty_reselleragreements').show();
	        //}
	       // $('#marketingNotDefinedcontent').html(file_json.marketingNotDefined_content);

	        //
	        //featured
	        $('#allFeatured_content').html('');
	        if(file_json.allFeatured_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_allFeatured').show();
	        }
	        $('#allFeatured_content').html(file_json.allFeatured_content);
	        //$('#featuredBusinessVerticalSalesPoints_content').show();

	        // SUPPORT

	        // How-To
	        $('#supportHowTo_content').html('');
	        if(file_json.supportHowTo_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_supporthowto').show();
	        }
	        $('#supportHowTo_content').html(file_json.supportHowTo_content);

	        // Resources
	        $('#supportResources_content').html('');
	        if(file_json.supportResources_content == undefined){
	        	//lavuLog("cookbook is empty");
	        	$('.empty_supportresources').show();
	        }
	        $('#supportResources_content').html(file_json.supportResources_content);

	        $('#supportNotDefined_content').html(file_json.supportNotDefined_content);

        },
        error:function(msg){
	        //lavuLog("display_sales_lead_display() error:: "+msg);
        }
    });//ajax
}//parse_resource_files()
function done_resource_upload_form(){
	lavuLog('done_resource_upload_form');
	$(".opaque_modal_bg").stop(true,true);
	$('#success_lead_form').hide();
	// $('#ok_new_lead').hide();	
	//$(".opaque_modal_bg").clone().prop("class", "opaque_modal_bg_2").insertAfter($("#account_form_div"));
	$(".opaque_modal_bg").css({display:"none"});
	$("#resource_upload_form_div").css("display","none");
}//done_resource_upload_form()
function resource_upload_form(){
	lavuLog('resource_upload_form');
	$('.sales_sub_category').prop('disabled',true);
	$('.marketing_sub_category').prop('disabled',true);
	$('.support_sub_category').prop('disabled',true);
	$(".opaque_modal_bg").css('height','3000px');
	$(".opaque_modal_bg").css('width', '100%');
	$(".opaque_modal_bg").css("display","block");
	var opacity = $(".opaque_modal_bg").css("opacity");
	$(".opaque_modal_bg").css("opacity",0.1);
	$(".opaque_modal_bg").animate({opacity:opacity},300,function(){
		$("#resource_upload_form_div").css("display","block");
	});
}//resource_upload_form()
function resourceFileUpload(){
      if (window.File && window.FileReader && window.FileList && window.Blob) {
      } else {
      	alert('The File APIs are not fully supported in this browser.');
          return;
      }   
      input = document.getElementById('resource_file');
      if (!input) {
          alert("Um, couldn't find the fileinput element.");
      }
      else if (!input.files) {
          alert("This browser doesn't seem to support the `files` property of file inputs.");
      }
      else if (!input.files[0]) {
          alert("Please select a file before clicking 'Load'");               
      }
      else {
      	file = input.files[0];
      	var fr = new FileReader();
      	fr.readAsDataURL(file);
      	fr.onload = function(evt){
      		var contents = evt.target.result;
		  /*alert( "Got the file.n" 
			 +"name: " + file.name + "n"
			 +"type: " + file.type + "n"
			 +"size: " + file.size + " bytesn"
			 );*/
		//document.getElementById('logo_preview').appendChild(document.createTextNode(fr.result));
		var logo_username = $('#logo_username').attr('value');
		//encodeURIComponent(fr.result)
		$.ajax({
		  	type: "POST",
		  	url: "./exec/upload_resource.php",
		  	data: {submit_logo:'oh_yea',logo_file:fr.result,username:logo_username},
		  	cache: false,
		  	contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		  	success: function(msg){
			  	//lavuLog("edit_incentive response:: "+msg);
			  	var result = msg.split('|||');
			  	if(result[0]=='success'){
			  	$("#logo_preview").html('');
				  	$("#logo_preview").html("<strong 'style = color:#aecd37;'>SUCCESS!! "+result[1]+"</strong>");
				  	$('#image_uploaded_reload').show();
			  	}else{
				  	$("#logo_preview").html("<strong 'style = color:red;'>FAILED:: "+result[1]+"</strong>");
			  	}
			}
		});//ajax*/
	};//onload
    }
}//resourceFileUpload()
function resourceDownloadView(resource, filename, dl_path,title,subtitle,subsection,filesize,description,section,date_created){
	//$('.sales_sub_category').prop('disabled',true);
	//$('.marketing_sub_category').prop('disabled',true);
	//lavuLog('src== '+$('#resource_download_iframe').attr('src')+", new src== "+resource);
	lavuLog('section== '+section+", subsection="+subsection+", date_created="+date_created);
	//var file_info = eval(json_info);
	var downloadAttrSupported = ("download" in document.createElement("a"));
	lavuLog("downloadAttrSupported == "+downloadAttrSupported);
	if(!downloadAttrSupported){
		$('#resource_download_iframe').attr('src', resource);
		$('#resource_download_click').attr('onclick', "");
		//$('#resource_download_click').attr('onclick', "downloadWithHeaders(\""+dl_path+"\");");
		$('#resource_download_click').attr('href', "."+dl_path);
		//$('#resource_download_click').attr('download', "."+dl_path);
		$('#resource_download_click').attr('target', '_blank');
		$('#resource_download_click').text('');
		$('#resource_download_click').text(filename);
	}else{
		lavuLog("resource::"+resource+", filename::"+filename+", dl_path::"+dl_path);
		$('#resource_download_click').attr('onclick', "");
		$('#resource_download_click').attr('target', '');
		$('#resource_download_iframe').attr('src', resource);
		$('#resource_download_click').attr('href', "."+dl_path);
		$('#resource_download_click').attr('download', "."+dl_path);
		$('#resource_download_click').text('');
		$('#resource_download_click').text(filename);
	}
	var option_to_hide = '.edit_dl_select_'+section;
	var subsection_nospace = subsection.split(" ").join("_");
	var option_to_select = 'option[value='+subsection_nospace+']';

	lavuLog('to hide:: '+option_to_hide);
	lavuLog('to select:: '+option_to_select);
	lavuLog('to select:: '+subsection_nospace);

	$(option_to_select).attr('selected','selected');
	$('.edit_dl_select_sales').prop('disabled',false);
	$('.edit_dl_select_marketing').prop('disabled',false);
	$(option_to_hide).prop('disabled',true);

	$('.edit_dl_select_sales').show();
	$('.edit_dl_select_marketing').show();
	//$(option_to_hide).hide();

	$('input[name=dl_view_section]').val(section);
	$('#resource_download_title').html(title);
	$('input[name=dl_edit_title]').val(title);

	$('#resource_download_subtitle').html(subtitle);
	$('input[name=dl_edit_subtitle]').val(subtitle);

	$('#resource_download_subsection').html(subsection);
	$('input[name=dl_edit_subsection]').val(subsection);

	$('#resource_download_filesize').html(filesize);

	$('#resource_download_description').html(description);
	$('input[name=dl_edit_description]').val(description);

	$('input[name=dl_view_date_created]').val(date_created);

	$(".opaque_modal_bg").css('height','3000px');
	$(".opaque_modal_bg").css('width', '100%');
	$(".opaque_modal_bg").css("display","block");
	var opacity = $(".opaque_modal_bg").css("opacity");
	$(".opaque_modal_bg").css("opacity",0.1);
	$(".opaque_modal_bg").animate({opacity:opacity},300,function(){
		$("#resource_download_div").css("display","block");
	});
}//resourceDownloadView
function close_resource_download(){
	$("#resource_download_div").hide();
	$(".opaque_modal_bg").css("display","none");
}//close_resource_download
function update_sub_section_drop_down(category){
	var sub_category = "."+category+"_sub_category";
	lavuLog("get rid of:: ".sub_category);
	$('.marketing_sub_category').prop('disabled',true);
	$('.sales_sub_category').prop('disabled',true);
	$('.support_sub_category').prop('disabled',true);
	$(sub_category).prop('disabled',false);
}//update_sub_section_drop_down(category)
function duplicate_jelement_and_change_name(jelement, s_name) {
		var jcontainer = jelement.parent();
		jelement.clone().appendTo(jcontainer);
		jelement.prop("name", s_name);
	}
function remove_selected_leads(){
	lavuLog("remove_selected_leads");
	//var jlead_id_container = $(this).parent().siblings("[name=lead_on_lead_id]");
	var selected = $('.input_remove_lead:checked');
	var deleted_id_string = "";
	var username = $('.remove_all_selected_leads').attr('username');
	var res_id = $('.remove_all_selected_leads').attr('res_id');

	$.each(selected,function(i,v){
		lavuLog(i+": "+v.getAttribute('lead_id'));
		deleted_id_string += v.getAttribute('lead_id')+", ";
		//lavuLog(deleted_id_array.toString());
		//$(this).parent().siblings("[name=lead_on_lead_id]");
	});
	//var jsonString = JSON.stringify(deleted_id_array);
	//lavuLog("JSON:: "+jsonString);
	var conf = confirm("Are you sure you want to remove these leads?");
		if(conf){
			$.ajax("./exec/remove_lead.php", {
				type: "POST",
				data: {
            		lead_ids: deleted_id_string,
            		username:username,
            		res_id:res_id,
            		to_call:"remove_multiple"
            	},
            	cache: false,
            	async: false,
            	success: function(data){
		        	lavuLog("removed: "+data);
		        	window.location.reload();
		        }
		   });//ajax
		}
	$('.remove_all_selected_leads').hide();
}//remove_selected_leads
function resource_dl_show_edit(){
	$('.dl_view').hide();
	$('#resource_dl_edit_finish').show();
	$('#resource_dl_edit_cancel').show();
	$('#resource_dl_edit_start').hide();
	$('.dl_edit').show();
}//resource_dl_show_edit
function resource_dl_hide_edit(){
	$('.dl_view').show();
	$('#resource_dl_edit_finish').hide();
	$('#resource_dl_edit_cancel').hide();
	$('#resource_dl_edit_start').show();
	$('.dl_edit').hide();
}//resource_dl_show_edit
function resource_dl_edit_apply(){
	//
	var title = $('input[name=dl_edit_title]').val();
	var section = $('input[name=dl_view_section]').val();
	var subtitle = $('input[name=dl_edit_subtitle]').val();
	var subsection = $('select[name=dl_edit_subsection] > option:selected').val();
	var description = $('input[name=dl_edit_description]').val();
	var featured = $('input[name=dl_edit_featured]').is(':checked');
	var remove = $('input[name=dl_edit_remove]').is(':checked');
	var filename = $('#resource_download_click').text();
	var date_created = (typeof $('input[name=dl_view_date_created]').val() === "undefined") ? "" : $('input[name=dl_view_date_created]').val();
	var edit_resource = 'edit_resource';
	lavuLog("title="+title+", subtitle="+subtitle+", subsection="+subsection+", description="+description+", filename="+filename+", remove="+remove+", date_created="+date_created);

	$.ajax({
			url:"./exec/upload_resource.php",
			type:"POST",
			data:{
			 'resource_action':edit_resource,
			 'title':title,
			 'section':section,
			 'subtitle':subtitle,
			 'subsection':subsection,
			 'description':description,
			 'featured':featured,
			 'remove':remove,
			 'filename':filename,
			 'date_created':date_created
			},
			success:function(msg){
				//lavuLog("resource edit response:: "+msg);
				var updated_attrs = msg.split('||:||');
				lavuLog('resource edit response::'+updated_attrs[0]+', '+updated_attrs[1]);
				if(updated_attrs[0] == 'SUCCESS'){
					lavuLog(title+', '+subtitle+', '+updated_attrs[1]+', '+description);
					$('#resource_download_title').text(title);
					$('#resource_download_subtitle').text(subtitle);
					$('#resource_download_subsection').text(updated_attrs[1]);
					$('#resource_download_description').text(description);
					alert('SUCCESS! Changes have been made. Changes will appear on page reload.');		
					parse_resource_files();	
					//window.location.reload(true);  // Tried to prevent user from having to reload but it didn't work.
				}else{
					alert('error editing resource');
				}

				/*
				$('#resource_download_title').val(title);
				$('#resource_download_subtitle').val(subtitle);
				$('#resource_download_subsection').val(updated_attrs[1]);
				$('#resource_download_description').val(description);
	            */
				//parse_common_ajax_responses(msg);
				//window.location.reload();
			},
			error:function(msg){
				lavuLog("resource edit error::"+msg);
			}
		});//ajax

}//resource_dl_edit_apply()
function sort_distro_options(){
		var options = $('select#all_reseller_select option');
		var arr = options.map(function(_, o) {
			return {
				t: $(o).text(),
				v: o.value
			};
		}).get();
		arr.sort(function(o1, o2) {
			return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
		});
		options.each(function(i, o) {
			//lavuLog(i);
			o.value = arr[i].v;
			$(o).text(arr[i].t);
		});
	}//sort_distro_options
$(document).ready(function(){
	//ALL CONTENT
	$('#tab-container').easytabs();
	// ACCORDIAN TOP COLORINGS
	$('.acc_head:even').css('background-color','#f1f5ee');
	//$('.acc_quote_content:even').css('background-color','#f1f5ee');
	//$('.acc_head:odd').css('background-color','#f3f3f3');
	$('.acc_edit_detail:even').css('background-color','#f3f3f3');
	//$('.acc_edit_detail:odd').css('background-color','#f3f3f3');
	$('.accnt_detail:even').css('background-color','#f3f3f3');
	$('.need_follow_up').css('background-color','pink');
	$('.need_follow_up12').css('background-color','yellow');
	$('.need_follow_up24').css('background-color','orange');
	$('.need_follow_up48').css('background-color','red');
	//$('.accnt_detail:odd').css('background-color','#f1f5ee');


	//
	//ACCOUNT TAB THINGS
	//
	//

	//HIDE ALL DONE ICONS AND FORMS
	$('.acc_done_button').hide();
	$('.acc_edit_details').hide();
	$('.loc_add_detail').hide();
	$('.show_merc').hide();
	//$('.distro_report_table_divshow_merc').html('');
	//$('.distro_report_table_divshow_merc').hide();
	//$('#account_form_div').hide();
	//$('#apply_license_form_div').hide();

	function parse_common_ajax_responses(response_string) {
		if (response_string.split('|')[0] == 'reload')
			window.location.reload();
		if (response_string.split('|')[0] == 'alert')
			alert(response_string.split('|')[1]);
	}

	$('img.accept_lead_confirm').click(function(){
		lavuLog('--company accepted:: '+$('#p_view_username').text());//$('.accordionButton.on').children('span.lead_on_comp_name').text());

		var lead_id = $('.accordionButton.on').children('input[name=lead_on_lead_id]').val();
		var distro_username = $('#p_view_username').text();
		$.post("./exec/lead_progress.php", {
			progress:"lead_accepted",
			leadid: lead_id,
			username: distro_username
		}).done(function(data){
			parse_common_ajax_responses(data);
		});
	});
	$('img.decline_lead_confirm').click(function(){
		lavuLog('--company accepted:: '+$('#p_view_username').text());//$('.accordionButton.on').children('span.lead_on_comp_name').text());

		var lead_id = $('.accordionButton.on').children('input[name=lead_on_lead_id]').val();
		var distro_username = $('#p_view_username').text();
		$.post("./exec/lead_progress.php", {
			progress:"lead_declined",
			leadid: lead_id,
			username: distro_username
		}).done(function(data){
			parse_common_ajax_responses(data);
		});
	});

	$('div.contact_level_submit').click(function(){
		var contact_form = $(this).attr('contact_form');
		lavuLog("herro? "+contact_form);
		$.ajax({
			url:"./exec/lead_progress.php",
			type:"POST",
			data:$(contact_form).serialize(),
			success:function(msg){
				lavuLog("lead_progres:: "+msg);
				parse_common_ajax_responses(msg);
				//window.location.reload();
			},
			error:function(msg){
				lavuLog("lead_progres2"+msg);
			}
		});//ajax

	});

	$('div.quote_select_account').click(function() {
		// Show lightbox
		$(".opaque_modal_bg").css('height','3000px');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
		var opacity = $(".opaque_modal_bg").css("opacity");
		$(".opaque_modal_bg").css("opacity",0.1);
		$(".opaque_modal_bg").animate({opacity:opacity},300,function(){
			$("#quote_select_account_form_div").css("display","block");
		});

/*		var quoteid = $(this).attr('quoteid');
		var dc = $(this).attr('dc');
		var status = $(this).attr('s');
		//lavuLog("quote_place_order clicked quoteid="+quoteid+" dc="+dc+" status="+status);
		if (status == "ordered") return;
		$.ajax({
			url: "./exec/quote_responder.php",
			type: "POST",
			data: {
        		action: 'set',
        		item: 'quote_order',
        		distro_code: dc,
        		reseller_quote_id: quoteid,
        		status: 'ordered'
        	},
			success: function(data) {
				var resp = $.parseJSON(data);
				if (resp != null && resp.success) {
					// change circle to blue
					$("#quote_row_circ_"+quoteid).attr("src", "./images/circ_blue.png");
					// change Status text
					$("#quoteinfo_status_"+quoteid).html("Ordered");
					$("#quote_place_order_"+quoteid).attr("s", "ordered");
					$("#quote_place_order_"+quoteid).toggleClass("quote_place_order quote_place_order_deactivated");
					$("#quote_payment_link_"+quoteid).attr('href', resp.payment_link);
					$("#quote_payment_link_"+quoteid).html('Payment Link');
				}
			},
			error: function(msg) {
				lavuLog("quote: set-quote_status error: "+msg);
			}
		});//ajax
*/
	});

	$(".quote_clone_quote").click( function() {
		// Show new quote lightbox
		new_quote_form();

		// Get quote info and fill-in fields
		var distro_code = $(this).attr('dc');
		var quoteid = $(this).attr('qid');
		$.ajax("./exec/quote_responder.php", {
			type: "POST",
			data: {
				action: 'get',
				item: 'reseller_quote',
				distro_code: distro_code,
				reseller_quote_id: quoteid
			},
			cache: false,
			async: true,
			success: function(data) {
				var quote = $.parseJSON(data);
				var vernum = '2';
				var regex = /\s\(?[vV](\d+)\)?$/;
				var matches = quote.quote_name.match(regex);
				if (matches != null && matches[1] != null) vernum = parseInt(matches[1]) + 1;
				vers = ' V' + vernum;
				quote.quote_name = (quote.quote_name == "") ? quote.company + vers : quote.quote_name.replace(regex, '') + vers;
				$("#quote_accountlead_searchInput").val(quote.company);
				$("#current_pos").val(quote.current_pos);
				$("#current_processor").val(quote.current_processor);
				$("#requested_processor").val(quote.requested_processor);
				$("#notes").val(quote.notes);
				$("#customer_notes").val(quote.customer_notes);
				$("#dataname").val(quote.dataname);
				$("#reseller_lead_id").val(quote.reseller_lead_id);
				$("#restaurantid").val(quote.restaurantid);
				$("#signupid").val(quote.signupid);
				$("#salesforce_id").val(quote.salesforce_id);
				$("#salesforce_opportunity_id").val(quote.salesforce_opportunity_id);
				$("#quote_name").val(quote.quote_name);
				$("#company").val(quote.company);
				$("#firstname").val(quote.firstname);
				$("#lastname").val(quote.lastname);
				$("#address").val(quote.address);
				$("#city").val(quote.city);
				$("#state").val(quote.state);
				$("#zip").val(quote.zip);
				$("#country").val(quote.country);
				$("#phone").val(quote.phone);
				$("#email").val(quote.email);
				$("#same_billing_address").prop('checked', (quote.same_billing_address == '1'));
				$("#billing_firstname").val(quote.billing_firstname);
				$("#billing_lastname").val(quote.billing_lastname);
				$("#billing_address").val(quote.billing_address);
				$("#billing_city").val(quote.billing_city);
				$("#billing_state").val(quote.billing_state);
				$("#billing_zip").val(quote.billing_zip);
				$("#billing_country").val(quote.billing_country);
				
				//LP-4505
				var pay_options=(quote.payment_options).split(',');
				var num_pay_options = pay_options.length;		
				for (var j = 0; j < num_pay_options; j++) {
					var pay_opt=pay_options[j];
					$("#payment_option_"+pay_opt).prop('checked',true);
				}
				$("#payment_link_options_1").prop('checked', (quote.pay_link_option=='1'));
				//End of LP-4505

				// Clone quote line items
				var num_line_items = quote.lineitems.length;
				for (var i = 0; i < num_line_items; i++) {
					ctrlnum = i;
					if (i == 0) ctrlnum = "";
					if (i != 0) addNewQuoteLineItem();

					// Have to do this weirdness because we don't have the exact value we're re-selecting
					// but it will also help if two drop-down options contain the same text
					$("#product"+ctrlnum+" option:contains("+ quote.lineitems[i].product +")").each(function(){
						if ($(this).text() == quote.lineitems[i].product) {
							$(this).attr('selected', 'selected');
							return false;
						}
						return true;
					});
					$("#qty"+ctrlnum).val(quote.lineitems[i].qty);
					$("#unit_price"+ctrlnum).val(quote.lineitems[i].unit_price);
					if (quote.lineitems[i].from_inventory == "1") $("#from_inventory"+ctrlnum).attr('checked', true);
					checkLineNumForQuoteTotal(ctrlnum);
				}
				$("#line_num").val(num_line_items - 1);
			}
		} ); //ajax
	} );

	$('img.demo_account_requested').click(function(){

		var lead_id = $('.accordionButton.on').children('input[name=lead_on_lead_id]').val()+"";
		var distro_username = $('#p_view_username').text();
		lavuLog('lead_id:: '+lead_id+', username:: '+distro_username);
		$('input[name=new_acc_address]').val(lead_info[lead_id]['address']);
		$('input[name=new_acc_state]').val(lead_info[lead_id]['state']);
		$('input[name=new_acc_first_name]').val(lead_info[lead_id]['firstname']);
		$('input[name=new_acc_email]').val(lead_info[lead_id]['email']);
		$('input[name=new_acc_comp_name]').val(lead_info[lead_id]['company_name']);
		$('input[name=new_acc_city]').val(lead_info[lead_id]['city']);
		$('input[name=new_acc_zip]').val(lead_info[lead_id]['zip']);
		$('input[name=new_acc_last_name]').val(lead_info[lead_id]['lastname']);
		$('input[name=new_acc_phone]').val(lead_info[lead_id]['phone']);
		lavuLog(lead_info[lead_id]['promo']);
		$('input[name=promo]').val(lead_info[lead_id]['promo']);
		$('input[name=new_acc_has_lead]').val('TRUE');
		new_account_form();
		var haslead = $('input[name=new_acc_has_lead]').val();
		lavuLog('has lead?:: '+haslead);
	});

	// called once the create new account field has been completed
	function demo_account_created() {

		var lead_id = $('.accordionButton.on').children('input[name=lead_on_lead_id]').val()+"";
		var distro_username = $('#p_view_username').text();
		var lead_data_name = lead_info[lead_id]['data_name'];

		$.ajax("./exec/lead_progress.php", {
			type: "POST",
			data: {
				progress:"demo_created",
				leadid: lead_id,
				username: distro_username,
				dataname: lead_data_name
			},
			cache: false,
			async: false,
			success: function(data){
				if (data.split('|')[0] == 'reload')
					global_demo_account_created_reload = true;
				else
					global_demo_account_created_reload = false;
				if (data.split('|')[0] == 'alert')
					alert(data.split('|')[0]);
			}
		});//ajax
	}

	//GREEN + BUTTON IN EACH MANAGE ACCOUNT DETAIL TO ADD A NOTE OR CONTACT
	//opens up edit section, click again to close
	$('img.acc_add_detail').click(function(){
		lavuLog("add detail");

		//SLIDES ALL ADD DETAIL SECTIONS UP ON CLICK.
		$('.loc_add_detail').slideUp('normal');
		//THIS HANDLES THE OPENING OF EDIT AREA IF NOT OPEN ALREADY
		if($(this).parent().parent().siblings().children('.loc_add_detail').is(':hidden')) {
			lavuLog("add_detail was hidden");
			$(this).parent().parent().siblings().children('.loc_add_detail').slideDown('normal');
		}
		//$(this).parent().parent().siblings().children('.loc_add_detail').show();
	});

	//WORD-WRAP EMAILS ON "@"
	function wordWrapEmails() {
		var emails = $(".emailSpan");
		$.each(emails, function(k,v) {
			v = $(v);
			var cw = v.width();
			var pw = v.parent().width();
			if (cw > pw) {
				v.text(v.text().replace(/@/g,'@ '));
			}
		});
	}
	wordWrapEmails();

	//looks for more leads that initially put in c.c. info and have had their cards charged
	$('#look_for_finished').click(function(){
		var run_query = 'update_leads';
		lavuLog('look for finished::');
		$.ajax({
			url:"./exec/look_for_finished.php",
			type:"POST",
			data:{
				update:run_query
			},
			success:function(string){
				lavuLog("response:: "+string);
				window.location.reload();

			}
		});//ajax

	});//look_for_finished

	//LOGIC TO DO IF ADD NEW NOTE SUBMIT IS PRESSED
	$('.submit_new_note').click(function(){
		var note_id = "#"+$(this).attr('val');//val();
		var note = $(note_id).val();
		var dataname = $(note_id).attr('dataname');
		var url_string = "function=add_note&dataname="+dataname+"&note="+note;
		lavuLog('submit new note::'+url_string);
		$.ajax({
			url:"./exec/accounts_update.php",
			type:"POST",
			data:url_string,
			success:function(string){
				lavuLog("response:: "+string);
				window.location.reload();

			}
		});//ajax


		$(this).parents('.loc_add_detail').hide();
	});//submit_new_note

	//LOGIC TO DO IF ADD NEW CONTACT SUBMIT IS PRESSED
	$('.submit_new_contact').click(function(){

		var form_id = '#'+$(this).attr('val');
		//function="add_contact"',
		//$(form_id).serialize(),
		lavuLog('submit new contact, '+form_id);
		$(this).parents('.loc_add_detail').hide();
		var url_string = "function=add_contact&dataname=shitheel&note=shitpoopbuttaidsmeth";
		$.ajax({
			url:"./exec/accounts_update.php",
			type:"POST",
			data:$(form_id).serialize(),
			success:function(string){
				lavuLog("add contact success::"+string);
				window.location.reload();
			},
			error:function(string){
				lavuLog("pee fart");
			}

		});//ajax
	});


	//CLOSES EDIT SECTION FOR ADD NOTE OR CONTACT IF CANCEL IS PRESSED
	$('.loc_cancel_add_detail').click(function(){
		lavuLog('cancel_add_loc_detail');
		$(this).parents('.loc_add_detail').hide();
	});

	/*$('#next_start').click(function(){
		var range = $(this).attr('range');
		var curr_accnts = $('#account_acc_wrapper').html();
		var username = $(this).attr('username');
		var acc_heads = $('.acc_count').length;
		lavuLog('account number: '+acc_heads);
		/*$.ajax({
			type:"POST",
			url:"./load_more_accounts.php",
			data:{func:'next_50',range:range,curr:curr_accnts,username:username},
			success:function(msg){
				lavuLog("success!!"+msg);
				//$('body').replaceWith(msg);
				//window.location.reload();
				if(msg === ''){
				$(prompt_id).html('<img src="./images/stars_0.png" alt="not_set" />');
				}else{
				$(prompt_id).html('<img src="./images/stars_'+msg+'.png" alt="not_set" />');
				}
			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
		});//ajax
	});*/

	//tooltip things
	$('.distro_tip').tooltip({
		bodyHandler:function(){
			var tipid= $(this).attr('tipid');
			return $(tipid).html();
		}
	});
	//mark as seen
	//$('.').
	$('.unassign').click(function(){
	    //$('.acc_head:even').css('background-color','#f1f5ee');
		var lead_id = $(this).attr('lead_id');
	    var action = 'unassign';
	    var thisid = $(this).attr('id');
		$.ajax({
			type:"POST",
			url:"./exec/time_milestones.php",
			data:{lead_id:lead_id,action:action},
			success:function(msg){
				lavuLog(" success!! "+msg+",  #un_lead_head_"+thisid);
				//$('body').replaceWith(msg);
				//window.location.reload();
				$('.acc_head:even').css('background-color','#f1f5ee');
				$('.need_follow_up12').css('background-color','yellow');
				$('.need_follow_up24').css('background-color','orange');
				$('.need_follow_up48').css('background-color','red');
				window.location.reload();


			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
	    });//ajax
	});//unassign.click();

	//OPENS THE RESPECTIVE EDIT VIEW OF ACOUNT DETAIL SECTION
	$('.acc_edit_button').click(function(){
		lavuLog('acc_edit_clicked');
		$(this).hide();
		$(this).siblings('.acc_done_button').show();
		$(this).parent().siblings().children('.accnt_view').hide();
		$(this).parent().siblings().children('.acc_edit_details').show();
	});
	//CLOSES TEH RESPECTIVE EDIT VIEW OF ACCOUNT DETAIL SECTION
	$('.acc_done_button').click(function(){
		lavuLog('acc_done_clicked');
		//if($())
		$(this).hide();
		$(this).siblings('.acc_edit_button').show();

		$(this).parent().siblings().children('.acc_edit_details').hide();
		$(this).parent().siblings().children('.accnt_view').show();
	});


	$('.acc_note_input').change(function(){
		var changed_id = "#"+$(this).attr('id')+"_changed";
		lavuLog(changed_id);

	});

	//hover to show hidden things.
	var hover_width = '';
	/*$('.hover_flow').hover(function(){
		hover_width = $(this).css('width');
		//console.log(this.scrollWidth);
		if(this.offsetWidth < this.scrollWidth){
			console.log(this.offsetWidth+" < "+ this.scrollWidth);
			$(this).css('width','300');
			//$(this).css('background','#aecd37');
		}
		else{
			//your element don't have overflow
		}
	},function(){
		$(this).css('width',hover_width);
		//$(this).css('background','white');
	});*/

	//HANDLES DISPLAY OF NEW ACCOUNT FORM MODAL
	$('#account_create_account').click(function(){
		lavuLog("open new account form");
		$('input[name=new_acc_has_lead]').val('FALSE');
		new_account_form();
	});
	$("#cancel_new_account").click(function(){
		lavuLog("cancel new account form");
		done_new_account_form();
	});
	$("#cancel_new_lead").click(function(){
		lavuLog("cancel new lead form");
		done_new_lead_form();
	});
	$("#cancel_new_quote").click(function(){
		//lavuLog("cancel new quote form");
		done_new_quote_form();
	});
	$("#cancel_quote_select_account").click(function(){
		//lavuLog("cancel new quote form");
		done_quote_select_account_form();
	});
	$("#ok_new_account").click(function(){
		lavuLog("cancel new account form");
		done_new_account_form();
	});
	$("#ok_new_lead").click(function(){
		lavuLog("cancel new lead form");
		done_new_lead_form();
	});
	$("#ok_new_quote").click(function(){
		lavuLog("cancel new quote form");
		done_new_quote_form();
	});
	$("#ok_quote_select_account").click(function(){
		lavuLog("cancel quote select account form");
		done_quote_select_account_form();
	});
	$("#ok_app_license").click(function(){
		lavuLog("cancel new account form");
		done_apply_license_form();
	});
	$('#buy_using_points').click(function(){
		lavuLog('show points form');
		$(this).hide();
		$('#hide_using_points').show();
		$('#license_points_form_div').show();
	});
	$('#hide_using_points').click(function(){
		$(this).hide();
		$('#license_points_form_div').hide();
		$('#buy_using_points').show();
	});
	$("#ok_add_news_btn").click(function(){
		lavuLog("cancel new account form");
		done_add_news_form();
	});
	$("#ok_add_lead_note_btn").click(function(){
		lavuLog("cancel new account form");
		done_add_lead_note_form();
	});
	$("#ok_add_quote_pdf_btn").click(function(){
		lavuLog("cancel quote pdf form");
		done_add_quote_pdf_form();
	});
	$("#submit_assign_lead").click(function(){
		lavuLog("submit assign lead");
		done_assign_lead_form();
	});

	$('.show_radio').attr('disabled',false);
	$('.license_pts_radio').click(function(){
		lavuLog('show_submit_pts');
		$('#submit_pts_license').show();
		$('#cancel_pts_license').show();
	});

	$('#cancel_pts_license').click(function(){
		$('#submit_pts_license').hide();
		$(this).hide();
	});
	//HANDLES DISPLAY OF APPLY ACCOUNT FORM MODAL
	$('.apply_license_btn').click(function(){
		lavuLog("open apply account form:: "+$(this).attr('val'));
		var dname = $(this).attr('val');
		var license_level = $(this).html().toLowerCase();
		var available_licenses = $(this).siblings("input[name='available_licenses']").val();
		var javailable_special_licenses = $(this).siblings("input[name='available_special_licenses']");
		var available_special_licenses = ((javailable_special_licenses.length > 0 && javailable_special_licenses.val() !== '') ? javailable_special_licenses.val() : '');
		license_level = license_level.charAt(0).toUpperCase() + license_level.slice(1);
		lavuLog(license_level);
		lavuLog(available_licenses);
		$('#apply_license_dname').val(dname);
		$('#apply_license_company_name').text(dname);
		$("select[name=apply_lic_available_select]").val(license_level);
		$("#apply_license_form").find("input[name=available_licenses_to_apply]").val(available_licenses);
		$("#apply_license_form").find("input[name=available_special_licenses_to_apply]").val(available_special_licenses);
		$("#apply_license_form").find("input[name=licenseupgrade_string]").val("a license");
		apply_license_form();
	});
	$("#cancel_apply_license").click(function(){
		lavuLog("cancel apply account form");
		done_apply_license_form();
	});


	//HANDLES DISPLAY OF NEW LEAD FORM MODAL
	$('.new_lead_btn').click(function(){
		lavuLog("open new lead form:: ");

		new_lead_form();
	});

	//HANDLES DISPLAY OF NEW LEAD FORM MODAL
	$('.new_quote_btn').click(function(){
		//lavuLog("open new quote form:: ");
		new_quote_form();
	});

	//HANDLES DISPLAY OF APPLY ACCOUNT FORM MODAL
	$('.upgrade_license_btn').click(function(){
		lavuLog("open apply account form");
		var account_dataname = $(this).parent().parent().children("input[name=dataname]").val();
		var available_upgrades = $(this).siblings("input[name='available_upgrades']").val();
		lavuLog("found available upgrades: "+available_upgrades);
		$("#apply_license_form").find("input[name=apply_license_dname]").val(account_dataname);
		$("#apply_license_form").find("input[name=available_licenses_to_apply]").val(available_upgrades);
		$("#apply_license_form").find("input[name=available_special_licenses_to_apply]").val("");
		$("#apply_license_form").find("input[name=licenseupgrade_string]").val("an upgrade");
		apply_license_form();
	});
	$("#cancel_apply_license").click(function(){
		lavuLog("cancel apply account form");
		//done_apply_license_form();
	});

	//HANDLES DISPLAY OF ADD NEWS FORM MODAL
	$('#add_news').click(function(){
		lavuLog("open apply account form");
		$('#submit_add_news').show();
		$('#ok_save_news_changes').hide();
		add_news_form();
	});
	$("#cancel_add_news").click(function(){
		lavuLog("cancel apply account form");
		done_add_news_form();
	});
	//EDIT NEWS FUNCTIONS
	$('.edit_news_entry').click(function(){
		var entry_number = $(this).attr('entry');
		var title = $('#entry_title'+entry_number).text();
		var body = $('#entry_body'+entry_number).text();
		$('#add_news_title').val(title);
		$('#add_news_content').text(body);
		$('#ok_save_news_changes').attr('news_entry', entry_number);
		$('#ok_delete_news_entry').attr('news_entry', entry_number);
		$('#submit_add_news').hide();
		$('#ok_save_news_changes').show();
		$('#ok_delete_news_entry').show();
		add_news_form();


	});//edit_entry.click()

	//submit edit news changes
	$('#ok_save_news_changes').click(function(){
		var new_title = $('#add_news_title').val();
		var new_content = $('#add_news_content').text();
		var entry_number = $(this).attr('news_entry');
		$('#entry_title'+entry_number).text(new_title);
		$('#entry_body'+entry_number).text(new_content);
		done_add_news_form();
		update_news_file();

	});//ok_save_news_changes.click()
	$('#ok_delete_news_entry').click(function(){
		var new_title = $('#add_news_title').val();
		var new_content = $('#add_news_content').text();
		var entry_number = $(this).attr('news_entry');
		$('#entry_title'+entry_number).text('@@@@@');
		$('#entry_body'+entry_number).text('@@@@@');
		//lavuLog('delete::'+entry_number+'== '+$('#entry_title'+entry_number).text()+', '+$('#entry_body'+entry_number).text());
		$('#news_entry'+entry_number).hide();
		$('#edit_news_entry'+entry_number).hide();
		done_add_news_form();
		update_news_file();
	});//ok_delete_news_changes

	function update_news_file(){
		var all_news = "";
		$('.news_entry').each(function(){
			all_news = all_news.concat($(this).find('.entry_title').text());
			//lavuLog($(this).find('.entry_title').text());
			all_news = all_news.concat(':*:');
			all_news = all_news.concat($(this).find('.entry_date').text());
			all_news = all_news.concat(':*:');
			all_news = all_news.concat($(this).find('.entry_body').text());
			all_news = all_news.concat('|');
			//lavuLog('----news_entry::'+all_news);

		});//news_entry.each()
		//lavuLog(all_news);
		$.ajax({
			type:"POST",
			url:"./distro_news/update_news.php",
			data: {whole_file:all_news},
			cache: false,
			success:function(msg){
				lavuLog("success!!"+msg);
				//if(msg === ''){
				//$(prompt_id).html('<img src="./images/stars_0.png" alt="not_set" />');
				//}else{
				//$(prompt_id).html('<img src="./images/stars_'+msg+'.png" alt="not_set" />');
				//}
			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
		});//ajax


	}//update_news_file()
	//EDIT FEATURED DISTRIBUTOR
	$('.featured_entry_edit').focus(function(){$('.edit_featured_entry').show()});//incentive_edit_focus()
	$('.edit_featured_entry').click(function(){

		var title = $('.featured_entry_title').html();
		var body = $('.featured_entry_body').html();

		lavuLog("title::"+title);
		lavuLog("body::"+body);

		$.ajax({
				 type: "POST",
				 url: "./exec/edit_featured.php",
				 data: {edit_current_featured:'oh_yea',title:title,body:body},
				 cache: false,
				 success: function(msg){
				 lavuLog("edit_featured response:: "+msg);

				 }
            });//ajax


	});//edit_featured_entry.click()

	$('.notes_icon').click(function(){
		var lead_id = $(this).attr('comp_id');
		var reseller = $(this).attr('reseller');
		var note_string = $(this).attr('notes');
		//note_string = note_string.replace("__+__", "\n");
		//note_string = note_string.replace("_____", '"');
		//var contact= $(this).attr('contact');
		//var contacted= $(this).attr('contacted');
		$('#lead_note_previous').val(note_string);
		var each_note = note_string.split("*|*");
		var note_date = '';
		var note_title = '';
		var note_content = '';

		/*var div_for_contact_note = "<div class='lead_note_date'><span class='lead_note_display_title'>DATE:</span>"+contacted+"</div><div class='lead_note_title'><span class='lead_note_display_title'>TITLE:</span> CONTACT NOTE</div><div class='lead_note_content'><span class='lead_note_display_title'>CONTENT:</span><br />"+contact+"</div>";*/


		var display_all_notes = '<div style="text-align:center; font:20px Verdana;color:#439789;">PREVIOUS NOTES:</div>';

		each_note.forEach(function(entry){
			var split_note = entry.split(':*:');
			note_date = split_note[0];
			note_title = split_note[1];
			note_content = split_note[2];
			if(note_date !== ''){
				lavuLog("DATE:  "+note_date+", title:"+note_title+", content:"+note_content);
				display_all_notes = display_all_notes.concat("<div class='lead_note_date'><span class='lead_note_display_title'>DATE:</span>"+note_date+"</div><div class='lead_note_title'><span class='lead_note_display_title'>TITLE:</span> "+note_title+"</div><div class='lead_note_content'><span class='lead_note_display_title'>CONTENT:</span><br />"+note_content+"</div>");
			}
		});
		//lavuLog(":::"+display_all_notes);
		$('#lead_notes_display').html(display_all_notes);
		//lavuLog('lead_id='+lead_id+', reseller='+reseller);
		$('#lead_note_reseller').val(reseller);
		$('#lead_note_lead_id').val(lead_id);
		add_lead_note_form();


	});//.notes_icon
	$("#cancel_add_lead_note").click(function(){
		lavuLog("cancel lead note form");
		done_add_lead_note_form();
	});


	$('.pdf_icon').click(function(){
		var quote_id = $(this).attr('q_id');
		var distro_code = $(this).attr('dc');
		var dataname = $(this).attr('dn');
		var restaurantid = $(this).attr('rid');
		var signupid = $(this).attr('sid');

		var pdf_date = '';
		var pdf_title = '';
		var pdf_content = '';

		var quote_pdf_display = '<div style="text-align:center; font:20px Verdana;color:#439789;">UPLOAD PDF:</div>';

		//lavuLog(":::"+display_all_notes);
		$('#quote_pdf_display').html(quote_pdf_display);
		//lavuLog('lead_id='+lead_id+', reseller='+reseller);
		$('#quote_pdf_distro_code').val(distro_code);
		$('#quote_pdf_reseller_quote_id').val(quote_id);
		$('#quote_pdf_dataname').val(dataname);
		$('#quote_pdf_restaurantid').val(restaurantid);
		$('#quote_pdf_signupid').val(signupid);
		add_quote_pdf_form( (dataname && restaurantid && signupid) );
	});//.pdf_icon

	$("#cancel_add_quote_pdf").click(function(){  // cancel_add_lead_note => cancel_add_quote_pdf
		lavuLog("cancel quote pdf form");
		done_add_quote_pdf_form();
	});

	//HANDLES DISPLAY OF ASSIGN LEADS FORM MODAL
	$('.assign_lead').click(function(){
		lavuLog("open assign lead form");
		sort_distro_options();
		var jlead_id_container = $(this).parent().siblings("[name=lead_on_lead_id]");
		var lead_id = jlead_id_container.val();
		lavuLog(lead_id);

		$('#assign_lead_lead_id').val(lead_id);
		//apply_license_form();
		assign_lead_form();
	});
	$("#cancel_assign_lead").click(function(){
		lavuLog("cancel assign lead");
		//done_apply_license_form();
		done_assign_lead_form();
	});


	//sorts the option fields in the assign lead drop down by name
	function sort_distro_options(){
		var options = $('select#all_reseller_select option');
		var arr = options.map(function(_, o) {
			return {
				t: $(o).text(),
				v: o.value
			};
		}).get();
		arr.sort(function(o1, o2) {
			return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
		});
		options.each(function(i, o) {
			//lavuLog(i);
			o.value = arr[i].v;
			$(o).text(arr[i].t);
		});
	}//sort_distro_options
	
	function sort_report_table(row_class, table_body){
		var tablelength = $('#'+table_body+' tbody').children().length;
		var classlength = $('tr.'+row_class).length;
		//lavuLog('bootie:: '+row_class+', '+table_body+', '+tablelength+', '+classlength);
		//$('#'+table_body+' tbody').children().length;
		//$('tr.'+row_class).hide();
		$('.total_entry').hide();

		$('tr.'+row_class).each(function(){
			//lavuLog('bootie:: '+this);
			$('#'+table_body+' tbody').prepend(this);
			$(this).show();
		});

		
		//$('#'+table_body+' tbody').html('');
		$('tr.'+row_class).css('background-color', 'pink');
		
	}//sort_report_table

	//
	//RESOURCES TAB THINGS
	//
	$('.res_drop_down').hide();

	$('#res_print_choice').click(function(){
		lavuLog('res_print_choice clicked');
		//slide all drop downs up
		$('.res_drop_down').slideUp('normal');
		if($('#res_print_drop').is(':hidden')) {
			lavuLog("res_print_drop was hidden");
			$('#res_print_drop').slideDown('normal');
		}
	});
	$('#res_web_choice').click(function(){
		lavuLog('res_web_choice clicked');
		//slide all drop downs up
		$('.res_drop_down').slideUp('normal');
		if($('#res_web_drop').is(':hidden')) {
			lavuLog("res_web_drop was hidden");
			$('#res_web_drop').slideDown('normal');
		}
	});

	$('.res_request').click(function(){
		$('.resource_section_container').hide();
		var section = "#"+$(this).attr('res');
		$(section).show();
		lavuLog(section);

	});

	$('.jab_distro').click(function(){
		var bus_name = $(this).attr('b_name');
		var bus_email = $(this).attr('email');
		var bus_phone = $(this).attr('phone');
		var contact = $(this).attr('contact');
		var reseller = $(this).attr('reseller');
		//lavuLog(bus_name+bus_email+bus_phone+contact);
		/*$.ajax({
			type:"POST",
			url:"./exec/edit_reseller.php",
			data: $(form_id).serialize(),
			cache: false,
			success:function(msg){
				lavuLog("success!!"+msg);
				if(msg === ''){
				$(prompt_id).html('<img src="./images/stars_0.png" alt="not_set" />');
				}else{
				$(prompt_id).html('<img src="./images/stars_'+msg+'.png" alt="not_set" />');
				}
			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
		});//ajax*/
		$.ajax({
			type:"POST",
			url:"./exec/jab_distro.php",
			data:{
				company:bus_name,
				email:bus_email,
				phone:bus_phone,
				name:contact,
				reseller:reseller
			},
			cache:false,
			success:function(msg){
				lavuLog("jabby!! "+msg);
				alert(" jab was sent!");

			},
			error:function(msg){
				lavuLog("jabby error!! "+msg);
				alert(" jab was sent!");
			}
		});//post


	});//jab_distro.click

	$('.mark_as_test').click(function(){
		var rest_id = $(this).attr('rest_id');
		var mark_as_test_function = 'mark_as_test';
		$.ajax({
			type:"POST",
			url:"./exec/make_test.php",
			data:{
				making_function:mark_as_test_function,
				lead_id:rest_id
			},
			cache:false,
			success:function(msg){
				lavuLog("success::"+msg);
			},
			error:function(msg){
				lavuLog("error"+msg);
			}
		});

	});//mark_as_test.click

	//new account form display
	function new_account_form(){
		$(".opaque_modal_bg").css('height','100%');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
		var opacity = $(".opaque_modal_bg").css("opacity");
		$(".opaque_modal_bg").css("opacity",0.1);
		$(".opaque_modal_bg").animate({opacity:opacity},300,function(){
			$("#account_form_div").css("display","block");
			//$('input[name=new_acc_has_lead]').val('FALSE');
		});
	}
	function done_new_account_form(){
		$(".opaque_modal_bg").stop(true,true);
		$('#new_account').show();
		$('#success_account_form').hide();
		$('#ok_new_account').hide();
		//$(".opaque_modal_bg").clone().prop("class", "opaque_modal_bg_2").insertAfter($("#account_form_div"));
		$(".opaque_modal_bg").css({display:"none"});
		$("#account_form_div").css("display","none");
		/*var opacity = $(".opaque_modal_bg").css("opacity");
		$(".opaque_modal_bg_2").css({display:"block",opacity:opacity,height:window.innerHeight});
		$(".opaque_modal_bg_2").animate({opacity:0},30000,function() {
			$(".opaque_modal_bg_2").remove();
		});*/
		if (global_demo_account_created_reload)
			window.location.reload();
	}
	//new lead form displahy
	function new_lead_form(){
		$(".opaque_modal_bg").css('height','3000px');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
		var opacity = $(".opaque_modal_bg").css("opacity");
		$(".opaque_modal_bg").css("opacity",0.1);
		$(".opaque_modal_bg").animate({opacity:opacity},300,function(){
			$("#new_lead_form_div").css("display","block");
		});
	}
	function done_new_lead_form(){
		$(".opaque_modal_bg").stop(true,true);
		$('#new_lead_form').show();
		$('#success_lead_form').hide();
		$('#ok_new_lead').hide();
		//$(".opaque_modal_bg").clone().prop("class", "opaque_modal_bg_2").insertAfter($("#account_form_div"));
		$(".opaque_modal_bg").css({display:"none"});
		$("#new_lead_form_div").css("display","none");
		/*var opacity = $(".opaque_modal_bg").css("opacity");
		$(".opaque_modal_bg_2").css({display:"block",opacity:opacity,height:window.innerHeight});
		$(".opaque_modal_bg_2").animate({opacity:0},30000,function() {
			$(".opaque_modal_bg_2").remove();
		});*/
		if (global_demo_account_created_reload)
			window.location.reload();
	}
	//new quote form display
	function new_quote_form(){
		$(".opaque_modal_bg").css('height','3000px');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
		var opacity = $(".opaque_modal_bg").css("opacity");
		$(".opaque_modal_bg").css("opacity",0.1);
		$(".opaque_modal_bg").animate({opacity:opacity},300,function(){
			$("#new_quote_form_div").css("display","block");
		});

		// Blank out quote fields
		$("#product_0").val('');
		$("#qty_0").val('1');
		$("#unit_price_0").val('');
		$("#your_cost_0").html('');
		document.getElementById('product_info_0').value='';
		var line_num = parseInt($("#line_num").val(), 10);
		for ( var i = 1; i <= line_num; i++ ) {
			$("#quote_line_item" + i).remove();
		}
		$("#new_quote_form")[0].reset();
		$("#line_num").val('0');
		$("#quote_total").html('');
		$("#reseller_cost_total").html('');
		$("#reseller_lead_id").val('');
		$("#dataname").val('');
		$("#restaurantid").val('');
		$("#signupid").val('');
		$("#salesforce_id").val('');
		$("#salesforce_opportunity_id").val('');
		/*$("#current_pos").val('');
		$("#current_processor").val('');
		$("#requested_processor").val('');
		$("#restaurantid").val('');
		$("#reseller_lead_id").val('');
		$("#salesforce_id").val('');
		$("#salesforce_opportunity_id").val('');
		$("#quote_name").val('');
		$("#company").val('');
		$("#firstname").val('');
		$("#lastname").val('');
		$("#address").val('');
		$("#city").val('');
		$("#state").val('');
		$("#zip").val('');
		$("#phone").val('');
		$("#email").val('');
		$("#same_billing_address").prop('checked', false);
		$("#billing_firstname").val('');
		$("#billing_lastname").val('');
		$("#billing_address").val('');
		$("#billing_city").val('');
		$("#billing_state").val('');
		$("#billing_zip").val('');*/

	}
	function done_new_quote_form(){
		$(".opaque_modal_bg").stop(true,true);
		$('#new_quote_form').show();
		$('#success_quote_form').hide();
		$('#ok_new_quote').hide();
		//$(".opaque_modal_bg").clone().prop("class", "opaque_modal_bg_2").insertAfter($("#account_form_div"));
		$(".opaque_modal_bg").css({display:"none"});
		$("#new_quote_form_div").css("display","none");
		/*var opacity = $(".opaque_modal_bg").css("opacity");
		$(".opaque_modal_bg_2").css({display:"block",opacity:opacity,height:window.innerHeight});
		$(".opaque_modal_bg_2").animate({opacity:0},30000,function() {
			$(".opaque_modal_bg_2").remove();
		});*/
		if (global_demo_account_created_reload)
			window.location.reload();
	}

	function done_quote_select_account_form(){
		$(".opaque_modal_bg").stop(true,true);
		$('#quote_select_account_form').show();
		$('#quote_select_account_form_div').hide();
		$(".opaque_modal_bg").css({display:"none"});
		$("#quote_select_account_form_div").css("display","none");
		if (global_demo_account_created_reload)
			window.location.reload();
	}

	// appends a duplicate of the given jelement and changes the name of the old element to the given name
	function duplicate_jelement_and_change_name(jelement, s_name) {
		var jcontainer = jelement.parent();
		jelement.clone().appendTo(jcontainer);
		jelement.prop("name", s_name);
	}

	// jcheckbox: a jquery element representing the checkbox
	// jcontainer: a jquery element whose html is to be changed, or null
	// a_vals: an array with indices true/false if the checkbox is checked or not
	//		the html of jcontainer will be replaced with the values from a_vals
	checkbox_checked_update_element = function(jcheckbox, jcontainer, a_vals) {
		var checked = jcheckbox.prop("checked");
		var a_checked = {true:"checked",false:""};
		jcheckbox.val(a_checked[checked]);
		jcontainer.html(a_vals[jcheckbox.prop('checked')]);
	};

	//open/close apply license form
	function apply_license_form(){
		var jlicenseform = $("#apply_license_form");
		var s_available_licenses = jlicenseform.find("input[name=available_licenses_to_apply]").val();
		var s_available_special_licenses = jlicenseform.find("input[name=available_special_licenses_to_apply]").val();
		var s_licenseupgrade_string = jlicenseform.find("input[name=licenseupgrade_string]").val();
		jlicenseform.find("input[name=available_licenses_to_apply]").val('');
		while (jlicenseform.find("font.remove_me").length > 0)
			jlicenseform.find("font.remove_me").remove();
		// copy the old select
		if (jlicenseform.find("select[name=apply_lic_available_select_backup]").length > 0) {
			while (jlicenseform.find("select[name=apply_lic_available_select]").length > 0) {
				jlicenseform.find("select[name=apply_lic_available_select]").remove();
				lavuLog("removing select");
			}
			duplicate_jelement_and_change_name(jlicenseform.find("select[name=apply_lic_available_select_backup]"), "apply_lic_available_select");
			jlicenseform.find("select[name=apply_lic_available_select]").show();
		}
		var b_disable_select = false;
		// check if only certain licenses are available
		lavuLog(s_available_licenses);
		if (s_available_licenses !== '' || s_available_special_licenses !== '') {
			// remove previous backups
			while (jlicenseform.find("select[name=apply_lic_available_select_backup]").length > 0) {
				jlicenseform.find("select[name=apply_lic_available_select_backup]").remove();
				lavuLog("removing backup");
			}
			// create and hide the backup
			duplicate_jelement_and_change_name(jlicenseform.find("select[name=apply_lic_available_select]"), "apply_lic_available_select_backup");
			jlicenseform.find("select[name=apply_lic_available_select_backup]").hide();
			// remove invalid options
			var i = 0;
			var jselect = jlicenseform.find("select[name=apply_lic_available_select]");
			var a_select_options = jselect.children();
			var a_available_licenses = s_available_licenses.split('|');
			for (i = 0; i < a_available_licenses.length; i++)
				a_available_licenses[i] = a_available_licenses[i].replace(/[0-9]/g, '');
			lavuLog("available licenses: ");
			lavuLog(a_available_licenses);
			for (i = 0; i < a_select_options.length; i++) {
				var s_select_option_val = $(a_select_options[i]).val();
				s_select_option_val = s_select_option_val.replace(/[0-9]/g, '');
				lavuLog("select option: "+s_select_option_val);
				if (jQuery.inArray(s_select_option_val, a_available_licenses) < 0)
					$(a_select_options[i]).remove();
			}
			if (jselect.children().length === 0) {
				b_disable_select = true;
			}
			// get licenses that are permitted by id as a_avail_lic
			var json_id = s_licenseupgrade_string.match("upgrade") ? "distro_upgrades_json" : "distro_licenses_json";
			var a_licenses = JSON.parse($('#'+json_id).val().replace(/'/g, '"'));
			var special_text = (s_available_special_licenses !== '' ? 'special licensing ' : '');
			var a_lic_ids = (s_available_special_licenses !== '' ? s_available_special_licenses.split(',') : s_available_licenses.split('|'));
			lavuLog(a_lic_ids);
			var a_avail_lics = [];
			if (a_lic_ids.length > 0) {
				jselect.children().remove();
				$.each(a_lic_ids, function(k, s_id) {
					var i_id = parseInt(s_id, 10);
					if (a_licenses[i_id]) {
						a_licenses[i_id].id = i_id;
						a_avail_lics.push(a_licenses[i_id]);
					}
				});
			}
			// if only certain licenses are available, then show only those licenses
			if (a_avail_lics.length > 0) {
				// sort them by name
				var sortByName = function(a,b) {
					var aName = a.printed_name;
					var bName = b.printed_name;
					if (aName == bName)
						return 0;
					if (aName == 'Platinum')
						return -1;
					if (bName == 'Platinum')
						return 1;
					if (aName == 'Gold')
						return -1;
					if (bName == 'Gold')
						return 1;
					return (aName < bName ? -1 : (aName > bName ? 1 : 0));
				};
				a_avail_lics.sort(sortByName);
				// remove all options
				jselect.children().remove();
				// create the new options
				$.each(a_avail_lics, function(k,v) {
					jselect.append("<option value='"+v.id+"'>"+v.printed_name+" ("+special_text+"#"+(250000+v.id)+")</option>");
				});
				// tell the apply license script that it should be accepting an id
				jselect.siblings('input[name=is_id]').val(1);

				b_disable_select = (jselect.children().length === 0);
			}
		} else {
			b_disable_select = true;
		}
		// tell the user that they must purchase a license/upgrade
		if (b_disable_select) {
			jlicenseform.find("select[name=apply_lic_available_select]").remove();
			$("#select_arrow_bg_apply_license").append('<font class="remove_me" style="font-style:italic;">You must first purchase '+s_licenseupgrade_string+' from the licenses tab.</font>');
		}
		// display the form
		$(".opaque_modal_bg").css('height','3000px');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
		$("#apply_license_form_div").css("display","block");
	}//apply_license_form
	function done_apply_license_form(){
		lavuLog('done_apply_license_form()');
		$('#apply_license_form').show();
		$('#success_apply_license_form').html(' ');
		$('#success_apply_license_form').hide();
		$('#ok_app_license').hide();
		$(".opaque_modal_bg").css("display","none");
		$("#apply_license_form_div").css("display","none");
		if (global_apply_license_do_reload){
			window.location.reload();
		}
	}

	//open/close add news form
	function add_news_form(){
		$(".opaque_modal_bg").css('height','3000px');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
		$("#news_form_div").css("display","block");
	}
	function show_agreement_form(){
		$(".opaque_modal_bg").css('height','3000px');
		$(".opaque_modal_bg").css('width', '100%');
		$(".opaque_modal_bg").css("display","block");
		$("#distro_agreement_div").css("display","block");
		lavuLog('show agreement form displayed?');
	}
	function close_d_agreement(){
		$("#distro_agreement_div").hide();
		$(".opaque_modal_bg").css("display","none");
		lavuLog('agreement form submitted');
	}
	function done_add_news_form(){
		lavuLog('done_news_form()');
		$('#news_form').show();
		$('#success_news_form').html(' ');
		$('#success_news_form').hide();
		$('#ok_add_news').hide();
		$(".opaque_modal_bg").css("display","none");
		$("#news_form_div").css("display","none");
		if (global_add_news_do_reload){
			window.location.reload();
		}
	}

	//reseller things
	$('.d_level_stars').click(function(){
		lavuLog("d_level_stars click!");
		var span_id = "#"+$(this).attr('val');
		lavuLog('show '+span_id);
		$(this).hide();
		$(span_id).show();
	});
	$('#show_agreement_btn').click(function(){
		lavuLog('show agreement clicked');
		show_agreement_form();
	});

	$('.change_d_level_select').change(function(){
		var form_id = "#"+$(this).attr('val');
		var prompt_id = form_id+"_prompt";
		$(this).parent().parent().hide();
		$(prompt_id).show();
		$('#submit_changes').show();
		$.ajax({
			type:"POST",
			url:"./exec/edit_reseller.php",
			data: $(form_id).serialize(),
			cache: false,
			success:function(msg){
				//lavuLog("success!!"+msg);
				if(msg === '')
				{
					$(prompt_id).html('<img src="./images/stars_0.png" alt="not_set" />');
				}else if(msg == 'agree')
				{
					$(prompt_id).html('<span style="color:red;">AGREE</span>');
				}else if(msg == 'ingram'){
					$(prompt_id).html('<span style="color:red;">INGRAM</span>');
				}else if(msg == 'bluestar'){
					lavuLog("success!!"+msg);
					$(prompt_id).html('<span style="color:blue;">BLUESTAR</span>');
				}else
				{
					lavuLog("success stars == !! "+msg);
					$(prompt_id).html('<img src="./images/stars_'+msg+'.png" alt="not_set" />');
				}
			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
		});//ajax
	});
	$('.remove_reseller').click(function(){
		var user = $(this).attr('username');
		var a_user = "#activate_"+user;
		lavuLog("uname::"+user);
		$(this).hide();
		$(a_user).show();
		$.ajax({
			type:"POST",
			url:"./exec/edit_reseller.php",
			data:{
				remove_reactivate:"remove",
				username:user
			},
			cache:false,
			success:function(msg){
				lavuLog("success::"+msg);
				$('#submit_changes').show();
			},
			error:function(msg){
				lavuLog("error"+msg);
			}
		});//ajax
	});//remove_reseller.click()
	$('.activate_reseller').click(function(){
		$(this).hide();
		var user = $(this).attr('username');
		var d_user = "#remove_"+user;
		$(d_user).show();
		$.ajax({
			type:"POST",
			url:"./exec/edit_reseller.php",
			data:{
				remove_reactivate:"activate",
				username:user
			},
			cache:false,
			success:function(msg){
				lavuLog("success::"+msg);
				$('#submit_changes').show();
			},
			error:function(msg){
				lavuLog("error"+msg);
			}
		});//ajax
	});//activate_reseller.click()
	$('#submit_changes').click(function(){
		$(this).hide();
		window.location.reload();
	});//submit_chanages.click()


	//ARCHIVE ACTIVATE!!!
	//
	//
	$('.load_archive').click(function(){
			$('.to_archive_head').each(function(index){

			//var pookie = this;
			$('#archive_lead_desc').after($(this).next('.to_archive_body'));
			$('#archive_lead_desc').after($(this));
			//lavuLog($(this).next('.to_archive_body'));
			//lavuLog();
		});
	});//load_archive.click()!!

	//MY INFO TAB THINGS
	//

	//control my_tab edit icon things
	$('div#p_edit').hide();
	$('div#a_edit').hide();
	$('div#c_edit').hide();
	$('div#l_edit').hide();
	$('div#add_edit').hide();
	$('span#p_done_icon').hide();
	$('span#a_done_icon').hide();
	$('span#c_done_icon').hide();
	$('span#l_done_icon').hide();
	$('span#add_done_icon').hide();
	$('#p_edit_icon').click(function(){
		lavuLog('p_edit clicked');
		$('#submit_personal').val('Submit Personal Changes');
		$('div#p_edit').show();
		$('div#p_view').hide();
		$('span#p_edit_icon').hide();
		$('span#p_done_icon').show();

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});
	$('#a_edit_icon').click(function(){
		lavuLog('a_edit clicked');
		$('#submit_availability').val('Submit Availability Changes');
		$('div#a_edit').show();
		$('div#a_view').hide();
		$('span#a_edit_icon').hide();
		$('span#a_done_icon').show();

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});
	$('#c_edit_icon').click(function(){
		lavuLog('c_edit clicked');
		$('#submit_company').val('Submit Company Changes');
		$('div#c_edit').show();
		$('div#c_view').hide();
		$('span#c_edit_icon').hide();
		$('span#c_done_icon').show();

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});
	$('#l_edit_icon').click(function(){
		lavuLog('l_edit clicked');
		$('#submit_logo').val('Submit Logo Change');
		$('div#l_edit').show();
		$('span#l_edit_icon').hide();
		$('span#l_done_icon').show();

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});
	$('#add_edit_icon').click(function(){
		lavuLog('add_edit clicked');
		$('#submit_notes').val('Submit Notes');
		$('div#add_edit').show();
		$('div#add_view').hide();
		$('span#add_edit_icon').hide();
		$('span#add_done_icon').show();

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});

	$('span#p_done_icon').click(function(){
		lavuLog('p_done clicked');

		//$(".p_field").each(function() {
			//alert($(this).val());
		//});
		//p_edit_change();
		if($('div#p_field_submit_message').html() !== ''){
			lavuLog('reset here');
			window.location.reload();
		}
		//$('div#p_field_submit_message').html('');
		$('div#p_view').show();
		$('div#p_edit').hide();
		$('span#p_done_icon').hide();
		$('span#p_edit_icon').show();
		$('#submit_personal').val('');

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});
	$('span#a_done_icon').click(function(){
		lavuLog('a_done clicked');

		//$(".p_field").each(function() {
			//alert($(this).val());
		//});
		//p_edit_change();
		if($('div#a_field_submit_message').html() !== ''){
			lavuLog('reset here');
			window.location.reload();
		}
		//$('div#p_field_submit_message').html('');
		$('div#a_view').show();
		$('div#a_edit').hide();
		$('span#a_done_icon').hide();
		$('span#a_edit_icon').show();
		$('#submit_availability').val('');

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});
	$('span#c_done_icon').click(function(){
		lavuLog('c_done clicked');
		if($('div#c_field_submit_message').html() !== ''){
			lavuLog('reset here');
			window.location.reload();
		}

		$('div#c_view').show();
		$('div#c_edit').hide();
		$('span#c_done_icon').hide();
		$('span#c_edit_icon').show();
		$('#submit_company').val('');

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});
	$('span#l_done_icon').click(function(){
		lavuLog('c_done clicked');

		if($('div#l_field_submit_message').html() !== ''){
			lavuLog('reset here');
			window.location.reload();
		}

		$('div#l_edit').hide();
		$('span#l_done_icon').hide();
		$('span#l_edit_icon').show();
		$('#submit_logo').val('');

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});
	$('span#add_done_icon').click(function(){
		lavuLog('add_done clicked');

		if($('div#add_field_submit_message').html() !== ''){
			lavuLog('reset here');
			window.location.reload();
		}

		$('div#add_view').show();
		$('div#add_edit').hide();
		$('span#add_done_icon').hide();
		$('span#add_edit_icon').show();
		$('#submit_notes').val('');

		//$('#p_edit').css('display','block');
		//$('#p_view').css('display','none');
		/*$('.p_field').each(function(){
			marker = $('<span />').insertBefore(this);
			$(this).detach().attr('type', 'text').insertAfter(marker);//.focus();
			marker.remove();
		});*/
	});

    //handles the rich text editing of the elements with the class 'contenteditable'
	CKEDITOR.on('instanceCreated', function(event){
		var editor = event.editor,
			element = editor.element;
			//lavuLog('ckeditor ids::'+element.getAttribute('id')+', cn::'+element.getAttribute('class'));
			if(element.is('h3')){
				lavuLog('ckeditor ids::'+element.getAttribute('id'));
				editor.on( 'configLoaded', function() {

					// Remove unnecessary plugins to make the editor simpler.
					editor.config.removePlugins = 'colorbutton,find,flash,font,' +
						'forms,iframe,image,newpage,removeformat,' +
						'smiley,specialchar,stylescombo,templates';

					// Rearrange the layout of the toolbar.
					editor.config.toolbarGroups = [
						{ name: 'editing',		groups: [ 'basicstyles', 'links' ] },
						{ name: 'undo' },
						{ name: 'clipboard',	groups: [ 'selection', 'clipboard' ] },
						{ name: 'about' }
					];
				});

			}

	});//CKEDITOR.on(instanceCreated)

	///report tab things
	$('#list_a').change(function(){
		$('#list_b').prop('disabled', false);
		$('#report_choose_submit').attr("choice", $('#list_a').val());
		$('#tab_report_link').hide();
	});//list_a.change()
	$('#list_b').change(function(){
		$('#list_state').prop('disabled', false);
	});//list_b.change()
	$('#list_state').change(function(){
		$('#report_finished_choices').show();
		
	});//list_state.change()

	$('.incentive_entry_start').calendarsPicker({
		onSelect: start_incentive}
	);//({calendar: $.calendars.instance('persian'), onSelect: showDate});
	$('.incentive_entry_end').calendarsPicker({
		onSelect: end_incentive}
	);//({calendar: $.calendars.instance('persian'), onSelect: showDate});
	function start_incentive(date){
		$('.incentive_entry_start').text(date);
	}
	function end_incentive(date){
		$('.incentive_entry_end').text(date);
	}

	$('.incentive_entry_edit').focus(function(){
		lavuLog('boobie');
		$('.edit_incentive_entry').show()
	});//incentive_edit_focus()
	$('.edit_incentive_entry').click(function(){
		var start = $('#incentive_entry_start0').val();
		var end = $('#incentive_entry_end0').val();
		var title = $('.incentive_entry_title').html();
		var body = $('.incentive_entry_body').html();

		lavuLog("title::"+title+", start::"+start+", end::"+end);
		lavuLog("body::"+body);

		$.ajax({
				 type: "POST",
				 url: "./exec/edit_incentive.php",
				 data: {edit_current_incentive:'oh_yea',start:start,end:end,title:title,body:body},
				 cache: false,
				 success: function(msg){
				 lavuLog("edit_incentive response:: "+msg);

				 }
            });//ajax


	});//edit_incentive_entry.click()

	//report things
	$('.distro_report_table tr:even').css('background-color','#f1f5ee');
	$('.distro_report_table tr:odd').css('background-color','#f3f3f3');
	$('div.see_report').hover(function(){
		$(this).css('text-decoration', 'underline');
		$(this).css('color', 'blue');
	},function(){
		$(this).css('text-decoration', 'none');
		$(this).css('color', 'black');
	});//see_report.hover
	$('div.see_report').click(function(){
		var report = $(this).attr('report_id');
		var report_start = $(this).attr('start_rows');
		var table_to_sort = 'table_try_total';
		lavuLog('boogers:: '+report_start);
		$('.distro_report_table tr:even').css('background-color','#f1f5ee');
		$('.distro_report_table tr:odd').css('background-color','#f3f3f3');
		sort_report_table(report_start, table_to_sort);
		//var thisparentclass = $(this).parent().attr('class');
		//var sort_by_attr = $(this).parent('.ppc_table_row').attr('ppc_row');
		//lavuLog('see_report::: '+report);
		//$('div.distro_report_table_div').hide();
		//lavuLog('opening:'+report);
		$(report).show();
		//$('.distro_report_table_divshow_merc').hide();
		
	});//see_report.click


	$('#merc_time_table tbody td.see_time_report').live( 'hover', function(){
		lavuLog('please??!!');
		$(this).css('text-decoration', 'underline');
		$(this).css('color', 'blue');
	},function(){
		$(this).css('text-decoration', 'none');
		$(this).css('color', 'black');
	});//see_report.hover
	$('td.see_time_report').live('click', function(){
		var report = $(this).attr('report_id');
		//var report_start = $(this).attr('start_rows');
		//var table_to_sort = 'table_try_total';
		lavuLog('boogers:: '+report);
		$('.distro_report_table tr:even').css('background-color','#f1f5ee');
		$('.distro_report_table tr:odd').css('background-color','#f3f3f3');
		//sort_report_table(report_start, table_to_sort);
		//var thisparentclass = $(this).parent().attr('class');
		$('.total_entry').hide();
		$('#table_time_range_div').show();
		$('tr.time_row').hide();
		$('tr.'+report).show();
		//var sort_by_attr = $(this).parent('.ppc_table_row').attr('ppc_row');
		//lavuLog('see_report::: '+report);
		//$('div.distro_report_table_div').hide();
		//lavuLog('opening:'+report);
		//$(report).show();	
	});//see_report.click
	//Rounded corners
	$('#top').corner();
	//$().corner();

	$("#d_agree_form").validate({
		rules:{
			d_agreed_check:"required"
		},
		submitHandler:function(form){
			$.ajax({
				type: "POST",
				url: "./exec/edit_reseller.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					close_d_agreement();
			},
			error: function(msg){
				lavuLog( "d_agree_form Error: "+msg);
			}
		});//ajax

		}
	});
	
	$('#date_start').calendarsPicker({
		onSelect: showDate}
	);//({calendar: $.calendars.instance('persian'), onSelect: showDate});
	$('#date_end').calendarsPicker({
		onSelect: showDate2}
	);//({calendar: $.calendars.instance('persian'), onSelect: showDate});
	$('#date_start_mercury').calendarsPicker({
		onSelect: showDate_merc}
	);//({calendar: $.calendars.instance('persian'), onSelect: showDate});
	$('#date_end_mercury').calendarsPicker({
		onSelect: showDate2_merc}
	);//({calendar: $.calendars.instance('persian'), onSelect: showDate});
	
	function showDate(date) {
		// $('#choice').html('<b>'+date+'</b>');
		$('#date_start').val(date+" 00:00:00");
	}
	function showDate2(date) {
		// $('#choice').html('<b>'+date+'</b>');
		 $('#date_end').val(date+" 00:00:00");
	}
	function showDate_merc(date) {
		// $('#choice').html('<b>'+date+'</b>');
		$('#date_start_mercury').val(date+" 00:00:00");
	}
	function showDate2_merc(date) {
		// $('#choice').html('<b>'+date+'</b>');
		 $('#date_end_mercury').val(date+" 00:00:00");
	}
	
	$('#report_finished_choices').click(function(){
		lavuLog("mode_select::"+$("input[name='mode_select']:checked").val()+", l_a:: "+$('#list_a').val()+", l_b:: "+$('#list_b').val()+", l_s:: "+$('#list_state').val()+', date::'+$('#date_start').val()+', '+$('#date_end').val());
			$('#loading_report').show();
			$('#total_unit_count').html("");
			$('#total_unit_count').show();
		$.ajax({
				 type: "POST",
				 url: "./exec/distro_reports.php",
				 data: $('#request_report_form').serialize(),
				 cache: false,
				 success: function(msg){
					 $('#loading_report').hide();
				 	//lavuLog("herro?"+msg);
				 	if($('#report_choose_submit').attr('choice')=='distros'){
					 	build_distro_view_from_json(msg);
				 	}else if($('#report_choose_submit').attr('choice')=='licenses_by_unit'){
				 		//lavuLog('return_report'+msg);
					 	build_l_unit_view_from_json(msg);
				 	}else if($("input[name='mode_select']:checked").val() == 'total')
				 	{
				 		//lavuLog('return_report'+msg);
					 	build_license_view_from_json(msg);
				 	}else{
				 		build_active_view_from_json(msg);
				 	}
					 $('#tab_report_link').show();
				 	}
            });//ajax
	});//report_finished_choices.click
	
	function build_distro_view_from_json(report_json){
		//lavuLog( "Form Submitted: "+report_json);
					$('#total_count_div').html("");
					 //$('#report_view').html(msg);
					var jsonr = eval(report_json);
					 //$("#report_view").append("<div>total count::"+jsonr[0].r_total+"</div>");
					// lavuLog(json.stringify(jsonr));
					 //add thead
					 
					 //add tbody
					 //`f_name`, `l_name`, `username`, `company`, `city`, `state`, `username`
					 $("#distro_report_table_display thead").html("<tr><th>FIRST NAME</th><th>LAST NAME</th><th>COMPANY</th><th>CITY</th><th>STATE</th><th>USERNAME</th></tr>");
					 $("#distro_report_table_display tbody").html("");
					 var table_body = "";
					$.each(jsonr,function(i, item){
						//lavuLog(item.username);
						table_body = table_body.concat("<tr><td>"+item.f_name+"</td><td>"+item.l_name+"</td><td>"+item.company+"</td><td>"+item.city+"</td><td>"+item.state+"</td><td>"+item.username+"</td></tr>");
					});
					//lavuLog(table_body);
					$("#distro_report_table_display tbody").html(table_body);
					$('.distro_report_table tr:even').css('background-color','#f1f5ee');
					$('.distro_report_table tr:odd').css('background-color','#f3f3f3');
					$("#report_table_div").show();
					$("#report_view").prepend("<div id='total_count_div'>total::  "+jsonr.length+"</div>");
	}//buld_distro_view_from_json()
	function build_license_view_from_json(report_json){
					$('#total_count_div').html("");
					 //$('#report_view').html(msg);
					 //lavuLog(report_json);
					var jsonr = eval(report_json);
					 //$("#report_view").append("<div>total count::"+jsonr[0].r_total+"</div>");
					 //lavuLog(json.stringify(jsonr));
					 //add thead
					 
					 //add tbody
					 //`f_name`, `l_name`, `username`, `company`, `city`, `state`, `username`
					 $("#distro_report_table_display thead").html("<tr><th>RESTAURANT ID</th><th>STATE</th><th>AMOUNT</th><th>TIMESTAMP</th></tr>");
					 $("#distro_report_table_display tbody").html("");
					 var table_body = "";
					 //lavuLog('jsonr.length::'+jsonr.length);
					 var total = 0;
					$.each(jsonr,function(i, item){
						//lavuLog("count::"+total);
						if( item.x_type =='credit'){
							total++;
							//lavuLog("do not count:: id:: "+item.r_id+" type:: "+item.x_type+",  total:: "+total);
						}else{
							table_body = table_body.concat("<tr><td>"+item.r_id+"</td><td>"+item.x_state+"</td><td>"+item.x_amount+"</td><td>"+item.datetime+"</td></tr>");
						}
					});//each
					//lavuLog(table_body);
					$("#distro_report_table_display tbody").html(table_body);
					$('.distro_report_table tr:even').css('background-color','#f1f5ee');
					$('.distro_report_table tr:odd').css('background-color','#f3f3f3');
					$("#report_table_div").show();
					var net = jsonr.length-total;
					$("#report_view").prepend("<div id='total_count_div'>gross::  "+jsonr.length+"  <span>credit:: "+total+"</span><span>  ACTUAL:: "+net+"</span></div>");
	}//buld_license_view_from_json()
	function build_active_view_from_json(report_json){
					$('#total_count_div').html("");
					 //$('#report_view').html(msg);
					var jsonr = eval(report_json);
					 //$("#report_view").append("<div>total count::"+jsonr[0].r_total+"</div>");
					 //lavuLog(json.stringify(jsonr));
					 //add thead
					 
					 //add tbody
					 //`f_name`, `l_name`, `username`, `company`, `city`, `state`, `username`
					 $("#distro_report_table_display thead").html("<tr><th>DATA NAME</th></tr>");
					 $("#distro_report_table_display tbody").html("");
					 var table_body = "";
					 //lavuLog('jsonr.length::'+jsonr.length);
					 var total = 0;
					$.each(jsonr,function(i, item){
						//lavuLog("count::"+total);
						table_body = table_body.concat("<tr><td>"+item.data_name+"</td></tr>");
					});//each
					//lavuLog(table_body);
					$("#distro_report_table_display tbody").html(table_body);
					$('.distro_report_table tr:even').css('background-color','#f1f5ee');
					$('.distro_report_table tr:odd').css('background-color','#f3f3f3');
					$("#report_table_div").show();
					var net = jsonr.length-total;
					$("#report_view").prepend("<div id='total_count_div'>gross::  "+jsonr.length+"  <span>credit:: "+total+"</span><span>  ACTUAL:: "+net+"</span></div>");
	}//buld_active_view_from_json()
	function build_l_unit_view_from_json(report_json){
		$('#total_count_div').html("");
		var jsonr = eval(report_json);
		var r_units = new Array();
		$("#distro_report_table_display thead").html("");
		$("#distro_report_table_display tbody").html("");
		$("#distro_report_table_display thead").html("<tr><th>RESELLER</th><th>TOTAL UNITS</th><th>LAST 30 DAYS</th><th>SILVER</th><th>GOLD</th><th>PLATINUM</th></tr>");
		var table_body = "";
		var unit_count = 0;
		var unit_count_recent = 0;
		$.each(jsonr,function(i, item){
			var reseller = item.resellername;
			var count_silver = parseInt(item.Silver);
			var count_gold = parseInt(item.Gold);
			var count_platinum = parseInt(item.Platinum);
			
			var count_silver_recent = parseInt(item.Silver_recent);
			var count_gold_recent = parseInt(item.Gold_recent);
			var count_platinum_recent = parseInt(item.Platinum_recent);
			if(!isNaN(count_silver))
			{
				var unit_silver = count_silver / 4;
			}else
			{
				var unit_silver = 0;
			}
			if(!isNaN(count_silver_recent))
			{
				var unit_silver_recent = count_silver_recent / 4;
			}else
			{
				var unit_silver_recent = 0;
			}
			//
			if(!isNaN(count_gold))
			{
				var unit_gold = count_gold / 2;
			}else
			{
				var unit_gold = 0;
			}
			if(!isNaN(count_gold_recent))
			{
				var unit_gold_recent = count_gold_recent / 2;
			}else
			{
				var unit_gold_recent = 0;
			}
			
			if(!isNaN(count_platinum))
			{
				var unit_platinum = count_platinum;
			}else
			{
				var unit_platinum = 0;
			}
			if(!isNaN(count_platinum_recent))
			{
				var unit_platinum_recent = count_platinum_recent;
			}else
			{
				var unit_platinum_recent = 0;
			}
			
			//var unit_gold = count_gold / 2;
			//var unit_platinum = count_platinum;
			var total_units = unit_silver + unit_gold + unit_platinum;
			
			//var unit_silver_recent = count_silver_recent / 4;
			//var unit_gold_recent = count_gold_recent / 2;
			//var unit_platinum_recent = count_platinum_recent;
			var total_units_recent = unit_silver_recent + unit_gold_recent + unit_platinum_recent;
			unit_count += total_units;
			unit_count_recent += total_units_recent;
			
			table_body = table_body.concat("<tr><td>"+reseller+"</td><td>"+total_units+"</td><td>"+total_units_recent+"</td><td>"+unit_silver+"</td><td>"+unit_gold+"</td><td>"+unit_platinum+"</td></tr>");
			
			$("#distro_report_table_display tbody").html(table_body);
					$('.distro_report_table tr:even').css('background-color','#f1f5ee');
					$('.distro_report_table tr:odd').css('background-color','#f3f3f3');
					$("#report_table_div").show();			
		});//.each()
		
		$("#total_unit_count").html("Total count:: "+unit_count+", Recent count:: "+unit_count_recent);
		
	}//buld_l_unit_view_from_json()

	// Validate the license page promo code after they finish typing in it.
	function validatePromoCode(){
		var licenseDropDown = document.getElementById("license_license");
		var promo_code = document.getElementById("promo_code");

		// Don't validate the promo code until one is entered and they've selected a license, as well.
		if (licenseDropDown.selectedIndex == 0 || promo_code.value == "" || promo_code.value.length < 1) {
			$('#promo_code_response_message').empty();
			return;
		}

		var selectedLicense = licenseDropDown.options[licenseDropDown.selectedIndex].value;
		selectedLicense = selectedLicense.split(" - ")[0];

		var postData = $('#promo_code').serializeArray();
		postData.push({ name : "license_name", value : selectedLicense });

		$.ajax({
			type: "POST",
			url: "./exec/check_promo_code.php",
			data: postData,
			cache: false,
			success: function(msg){
				var result = msg.split('|');
				lavuLog('r[0]='+result[0]+', r[1]='+result[1]);
				if(result[0] == "success" || result[0] == "failure") {
					$('#promo_code_response_message').empty();
					var p;
					if (result[0] == "failure") {
						p = '<p style="color:red;font-size:.9em">';
					} else {
						p = '<p style="color:#439789;font-size:.9em">';
					}
					$('#promo_code_response_message').html(p+ result[1] +"</p>");
					$('#promo_code_response_message').show();
				}
			},
			error: function(msg){
				lavuLog( "Error: ");
			}
		});//ajax
	}
	$('#promo_code').blur(validatePromoCode);
	$('#license_license').change(validatePromoCode);  // Listen for them to change the license drop-down, too, since they could change the discount amount.
	$('#submit_buy_new_license').click(validatePromoCode);  // This is to make sure the discount message is displayed before the page changes to show them the charged amount.
	
	// all of the form validations
	$('#new_license').validate({
		rules:{
			license_license:"required",
			card_number:{
				required:false
			},
			license_card_exp_m:{
				required:false
			},
			license_card_exp_y:{
				required:false
			},
			card_ccv:{
				required:false
			}
		},
		submitHandler: function(form){
			$('#license_response_message').stop(true,true);
			$('#license_response_message').css({color:'gray',opacity:0});
			$('#license_response_message').html("Processing...");
			$('#license_response_message').show();
			$('#license_response_message').animate({opacity:1}, 300);
			$.ajax({
				type: "POST",
				url: "./exec/buy_license.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Data Saved: ");
					var result = msg.split('|');
					lavuLog('r[0]='+result[0]+', r[1]='+result[1]);
					if(result[0] == "success"){
						$('#licenses_form').hide();
						$('#licenses_prices').hide();
						$('#license_response_message').stop(true,true);
						$('#license_response_message').css({color:'black',opacity:0});
						$('#license_response_message').html(result[1]);
						$('#license_response_message').show();
						$('#license_response_message').animate({opacity:1}, 300);
						$('#show_l_form').show();
					}
					else{
						//$('#licenses_form').hide();
						$('#license_response_message').stop(true,true);
						$('#license_response_message').css({color:'red',opacity:0});
						$('#license_response_message').html(result[1]);
						$('#license_response_message').show();
						$('#license_response_message').animate({opacity:1}, 300);
						//$('#show_l_form').show();
					}
				},
				error: function(msg){
					lavuLog( "Error: ");
				}
			});//ajax
		}
	});

	/* Validation for Zip code */
	$("#new_acc_zip").on("keypress keyup", function (event) {
		var inputtxt = $(this).val();
		var keyCode = (event.which) ? event.which : event.keyCode;

		//Regex for Valid Characters i.e. Alphabets and Numbers.
		var regex = /^[A-Za-z0-9]+$/;
		//Validate TextBox value against the Regex.
		var enteredValue = String.fromCharCode(keyCode);
		var lastValue = inputtxt.charAt(inputtxt.length-1);
		var isValid = regex.test(enteredValue);

		if ((inputtxt.trim() == '' || lastValue == ' ' || lastValue == '-') && (keyCode == 45 || keyCode == 32)) {
		  return false;
		} else if (!isValid && keyCode != 32 && keyCode != 45) {
			return false;
		}
		return true;
	 });
	 /* Validation for Phone Number */
	 $("#new_acc_phone").on("keypress keyup", function (event) {
		var inputtxt = $(this).val();
		var keyCode = (event.which) ? event.which : event.keyCode;
		var lastValue = inputtxt.charAt(event.target.selectionStart-2);

		if(inputtxt.trim() == '' && (keyCode == 40 || keyCode == 43)) {
			return true;
		} else if ((inputtxt.trim() == '' || lastValue == ' ' || lastValue == '-' || lastValue == '+' ||  lastValue == '.' || lastValue == '(' || lastValue == ')') && (keyCode == 40 || keyCode == 41 || keyCode == 43 || keyCode == 45 || keyCode == 46 || keyCode == 32 ) ) {
		 return false;
		} else if ((keyCode < 48 || keyCode > 57) && keyCode != 40 && keyCode != 41 && keyCode != 43 && keyCode != 45 && keyCode != 46 && keyCode != 32 ) {
			return false;
		}
		return true;
	 });

	$('#new_account').validate({
		rules:{
			starter_menu:"required",
			new_acc_address:{
				required:false
			},
			new_acc_state:{
				required:false
			},
			new_acc_first_name:{
				required:false
			},
			new_acc_email:{
				required:false
			},
			new_acc_comp_name:{
				required:false
			},
			new_acc_city:{
				required:false
			},
			new_acc_zip:{
				required:false
			},
			new_acc_last_name:{
				requred:false
			},
			new_acc_phone:{
				required:false
			},
			new_acc_has_lead:{
				required:false
			},
			distro_username:{
				required:false
			},
			distro_id:{
				required:false
			},
			distro_email:{
				required:false
			},
			promo:{
				required:false
			}
		},
		submitHandler: function(form){
			$('#success_account_form').stop(true,true);
			$('#success_account_form').css('color','gray');
			$('#success_account_form').css({'opacity':0, 'padding-bottom':'10px'});
			$('#submit_new_account').hide();
			$('#success_account_form').html('Processing...');
			$('#success_account_form').show();
			$('#success_account_form').animate({opacity:1},300);
			$.ajax({
				type: "POST",
				url: "./exec/create_account.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Data Saved: ");
					var result = msg.split('|');
					lavuLog('r[0]='+result[0]+', r[1]='+result[1]);
					if(result[0] == "success"){
						lavuLog('success create');
						$('#new_account').hide();
						$('#success_account_form').stop(true,true);
						$('#success_account_form').css('color','black');
						$('#success_account_form').css({'opacity':0, 'padding-bottom':'20px'});
						$('#success_account_form').html('');
						$('#success_account_form').append(result[1]);
						$('#submit_new_account').show();
						$('#success_account_form').show();
						$('#success_account_form').animate({opacity:1},300);
						$('#ok_new_account').show();
						var lead_id = $('.accordionButton.on').children('input[name=lead_on_lead_id]').val()+"";
						if (lead_info[lead_id]) {
							if ($("input[name=new_account_data_name]") && $("input[name=new_account_data_name]").length > 0) {
								var dataname = $("input[name=new_account_data_name]").val();
								lavuLog("dataname: "+dataname);
								lavuLog("lead id: "+lead_id);
								lead_info[lead_id]['data_name'] = dataname;
								lavuLog("Lead info: "+lead_info[lead_id]);
								$("input[name=new_account_data_name]").remove();
							} else {
								lavuLog("dataname not found");
							}
							if ($('li.tab.active').children('a').prop('href').split('#')[1] == 'leads_tab') {
								demo_account_created();
							}
						}
						global_demo_account_created_reload = true;
					}
					else{
						lavuLog('fail');
						$('#success_account_form').stop(true,true);
						$('#success_account_form').html(result[1]);
						$('#success_account_form').css('color','red');
						$('#success_account_form').css('opacity',0);
						$('#submit_new_account').show();
						$('#success_account_form').show();
						$('#success_account_form').animate({opacity:1},300);
						/*
						$('#licenses_form').hide();
						$('#license_response_message').text("message fail!");
						$('#license_response_message').show();
						$('#show_l_form').show();
						*/
					}

			},
			error: function(msg){
				lavuLog( "Error: ");
			}
		});
		}
	});
	$('#new_lead_form').validate({
		rules:{
			new_lead_address:{
				required:true
			},
			new_lead_state:{
				required:true
			},
			new_lead_first_name:{
				required:false
			},
			new_lead_email:{
				required:false
			},
			new_lead_comp_name:{
				required:true
			},
			new_lead_city:{
				required:true
			},
			new_lead_zip:{
				required:true
			},
			new_lead_last_name:{
				requred:false
			},
			new_lead_phone:{
				required:true
			}
		},
		submitHandler: function(form){
			$('#success_lead_form').stop(true,true);
			$('#success_lead_form').css('color','gray');
			$('#success_lead_form').css({'opacity':0, 'padding-bottom':'10px'});
			$('#success_lead_form').html('Processing...');
			$('#success_lead_form').show();
			$('#success_lead_form').animate({opacity:1},300);
			$.ajax({
				type: "POST",
				url: "./exec/new_lead.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Data Saved: ");
					var result = msg.split('|');
					lavuLog('r[0]='+result[0]+', r[1]='+result[1]);
					if(result[0] == "success"){
						lavuLog('success create');
						$('#new_lead_form').hide();
						$('#success_lead_form').stop(true,true);
						$('#success_lead_form').css('color','black');
						$('#success_lead_form').css({'opacity':0, 'padding-bottom':'20px'});
						$('#success_lead_form').html(result[1]);
						$('#success_lead_form').show();
						$('#success_lead_form').animate({opacity:1},300);
						$('#ok_new_lead').show();
						//var lead_id = $('.accordionButton.on').children('input[name=lead_on_comp_id]').val();

						global_demo_account_created_reload = true;
					}
					else{
						lavuLog('fail');
						$('#success_lead_form').stop(true,true);
						$('#success_lead_form').html(result[1]);
						$('#success_lead_form').css('color','red');
						$('#success_lead_form').css('opacity',0);
						$('#success_lead_form').show();
						$('#success_lead_form').animate({opacity:1},300);
						/*
						$('#licenses_form').hide();
						$('#license_response_message').text("message fail!");
						$('#license_response_message').show();
						$('#show_l_form').show();
						*/
					}

			},
			error: function(msg){
				lavuLog( "Error: ");
			}
		});
		}
	});
/*	$('#new_quote_form').validate({
		rules:{
			firstname:{
				required:true
			},
			lastname:{
				requred:true
			},
			company:{
				required:true
			},
			address:{
				required:true
			},
			state:{
				required:true
			},
			city:{
				required:true
			},
			zip:{
				required:true
			},
			phone:{
				required:false
			},
			email:{
				required:false
			}
		},
		submitHandler: function(form){
*/
	$("#account_filter_form").submit(function(event){
		filter_accountSearch();
		event.preventDefault();
	});
	$('#new_quote_form').submit(function(event){
			event.preventDefault();
			
			//LP-4505 Validation for payment methods...
			var payment_method=0;
			if($('input[name=payment_option_1]:checked').val() == 1 && payment_method==0){
				payment_method=1;
				}
			else if($('input[name=payment_option_2]:checked').val() == 2 && payment_method==0){
				payment_method=1;
				}
			else if($('input[name=payment_option_3]:checked').val() == 3 && payment_method==0){
				payment_method=1;
				}
			if (payment_method==0) {
				$('#success_quote_form').css('color','red');
				$('#success_quote_form').html('Please Check at least one option for "How will this quote be paid?"');
				$('#success_quote_form').show();
				return false;
				}
			//end of Validation for payment methods...LP-4505

			// Validate that they chose an account
			if ( $('input[name=associated_account]:checked').val() == "existing" && $('#dataname').val() == '' && $('#reseller_lead_id').val() == '' ) {
				$('#success_quote_form').css('color','red');
				$('#success_quote_form').html('The account you have selected is not valid');
				$('#success_quote_form').show();
				return false;
			}

			// Fix for Safari and other browsers that don't recognize the required attribute
			var reqFields = ["quote_name", "company", "firstname", "lastname", "address", "city", "state", "zip", "country", "phone", "email", "billing_firstname", "billing_lastname", "billing_address", "billing_city", "billing_state", "billing_zip", "billing_country"];
			for (var i = 0; i < reqFields.length; i++) {
				if ($.trim($('#'+ reqFields[i]).val()).length == 0) {
					$('#success_quote_form').css('color','red');
					$('#success_quote_form').html('Please fill out all required fields');
					$('#success_quote_form').show();
					$('#'+ reqFields[i]).focus();
					return false;
				} 
			}

			var reqProductFields = ["product_", "qty_", "unit_price_"];
			for (var i = 0; i <= parseInt($('#line_num').val()); i++) {
				//var x = (i == 0) ? '' : i;
				var x = i;
				for (var j = 0; j < reqProductFields.length; j++) {
					if ($.trim($('#'+ reqProductFields[j] + i).val()).length === 0) {
						$('#success_quote_form').css('color','red');
						$('#success_quote_form').html('Please fill out all required fields HERE');
						$('#success_quote_form').show();
						$('#'+ reqProductFields[i]).focus();
						return false;
					}
				}
			}

			if ( !checkAllLinesForQuoteTotal() ) {
				return false;
			}

			$('#quote_loading_squares').css('display', 'block');
			$('#submit_new_quote').prop('disabled', true);
			$('#success_quote_form').stop(true,true);
			$('#success_quote_form').css('color','gray');
			$('#success_quote_form').css({'opacity':0, 'padding-bottom':'10px'});
			$('#success_quote_form').html('Processing...');
			$('#success_quote_form').show();
			$('#success_quote_form').animate({opacity:1},300);
			$.ajax({
				type: "POST",
				url: "./exec/new_quote.php",
				/*data: $(form).serialize(),*/
				data: $("#new_quote_form").serialize(),
				cache: false,
				success: function(msg){
					$('#submit_new_quote').prop('disabled', false);
					lavuLog( "Data Saved: ");
					var result = msg.split('|');
					lavuLog('r[0]='+result[0]+', r[1]='+result[1]);
					if(result[0].indexOf("success") > -1){
						lavuLog('success create');
						$('#quote_loading_squares').css('display', 'none');
						$('#new_quote_form').hide();
						$('#success_quote_form').stop(true,true);
						$('#success_quote_form').css('color','black');
						$('#success_quote_form').css({'opacity':0, 'padding-bottom':'20px'});
						$('#success_quote_form').html(result[1]);
						$('#success_quote_form').show();
						$('#success_quote_form').animate({opacity:1},300);
						$('#ok_new_quote').show();
						//var lead_id = $('.accordionButton.on').children('input[name=lead_on_comp_id]').val();

						global_demo_account_created_reload = true;
					}
					else{
						lavuLog('fail');
						$('#quote_loading_squares').css('display', 'none');
						$('#success_quote_form').stop(true,true);
						$('#success_quote_form').html(result[1]);
						$('#success_quote_form').css('color','red');
						$('#success_quote_form').css('opacity',0);
						$('#success_quote_form').show();
						$('#success_quote_form').animate({opacity:1},300);
						/*
						$('#licenses_form').hide();
						$('#license_response_message').text("message fail!");
						$('#license_response_message').show();
						$('#show_l_form').show();
						*/
					}

			},
			error: function(msg){
				lavuLog( "Error: "+msg);
				var result = (msg != null) ? msg.split('|') : ['', ''];
				$('#quote_loading_squares').css('display', 'none');
				$('#submit_new_quote').prop('disabled', false);
				$('#success_quote_form').stop(true,true);
				$('#success_quote_form').html(result[1]);
				$('#success_quote_form').css('color','red');
				$('#success_quote_form').css('opacity',0);
				$('#success_quote_form').show();
				$('#success_quote_form').animate({opacity:1},300);
			}
		});
	});
	$('#assign_lead_form').validate({
		rules:{
			all_reseller_select:{
				required:false
			},
			assign_lead_lead_id:{
				required:false
			}
		},
		submitHandler:function(form){
			$('#success_assign_lead_form').stop(true,true);
			$('#success_assign_lead_form').css('color','gray');
			$('#success_assign_lead_form').css({'opacity':0, 'padding-bottom':'10px'});
			$('#success_assign_lead_form').html('Processing...');
			$('#success_assign_lead_form').show();
			$('#success_assign_lead_form').animate({opacity:1},300);
			$.ajax({
				type: "POST",
				url: "./exec/assign_lead.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Data Saved: ");
					var result = msg.split('|');
					lavuLog('r[0]='+result[0]+', r[1]='+result[1]);
					if(result[0] == "success"){
						lavuLog('success create');
						$('#new_lead_form').hide();
						$('#success_assign_lead_form').stop(true,true);
						$('#success_assign_lead_form').css('color','black');
						$('#success_assign_lead_form').css({'opacity':0, 'padding-bottom':'20px'});
						$('#success_assign_lead_form').html(result[1]);
						$('#success_assign_lead_form').show();
						$('#success_assign_lead_form').animate({opacity:1},300);
						$('#ok_assign_btn').show();
						//var lead_id = $('.accordionButton.on').children('input[name=lead_on_lead_id]').val();
						window.location.reload();
						global_assign_lead_do_reload = true;
					}
					else{
						lavuLog('fail');
						$('#success_assign_lead_form').stop(true,true);
						$('#success_assign_lead_form').html(result[1]);
						$('#success_assign_lead_form').css('color','red');
						$('#success_assign_lead_form').css('opacity',0);
						$('#success_assign_lead_form').show();
						$('#success_assign_lead_form').animate({opacity:1},300);
						/*
						$('#licenses_form').hide();
						$('#license_response_message').text("message fail!");
						$('#license_response_message').show();
						$('#show_l_form').show();
						*/
					}

			},
			error: function(msg){
				lavuLog( "Error: ");
			}
		});
		}
	});
	$('#apply_license_form').validate({
		rules:{
			apply_lic_available_select:"required",
			distro_id:{
				required:false
			},
			is_id:{
				required:false
			},
			apply_license_dname:{
				required:false
			}
		},
		submitHandler: function(form){
			$('#success_apply_license_form').stop(true,true);
			$('#success_apply_license_form').css('color','gray');
			$('#success_apply_license_form').css({'opacity':0, 'padding-bottom':'10px'});
			$('#success_apply_license_form').html('Processing...');
			$('#success_apply_license_form').show();
			$('#success_apply_license_form').animate({opacity:1},300);
			$.ajax({
				type: "POST",
				url: "./exec/apply_license.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Data Saved: ");
					var result = msg.split('|');
					lavuLog('r[0]='+result[0]+', r[1]='+result[1]);
					if(result[0] == "success"){
						var lead_id = $('.accordionButton.on').children('input[name=lead_on_lead_id]').val();
						var lead_data_name = result[2];
						var distro_username = $('#p_view_username').text();
						$.ajax({
							type: "POST",
							url: "./exec/lead_progress.php",
							data: {
								progress: 'license_applied',
								leadid: lead_id,
								username: distro_username,
								leaddataname: lead_data_name
							},
							cache: false,
							async: false,
							success: function(msg) {
								var s_cmd = msg.split('|')[0];
								if (s_cmd == 'reload') {
									global_apply_license_do_reload = true;
								}
							}
						});
						lavuLog('success-app-lic');
						$('#apply_license_form').hide();
						$('#success_apply_license_form').stop(true,true);
						$('#success_apply_license_form').css('color','black');
						$('#success_apply_license_form').css({'opacity':0, 'padding-bottom':'20px'});
						$('#success_apply_license_form').html(result[1]);
						$('#success_apply_license_form').show();
						$('#success_apply_license_form').animate({opacity:1},300);
						$('#ok_app_license').show();
					}
					else{
						lavuLog('fail');
						$('#success_apply_license_form').stop(true,true);
						$('#success_apply_license_form').html(result[1]);
						$('#success_apply_license_form').css('color','red');
						$('#success_apply_license_form').css('opacity',0);
						$('#success_apply_license_form').show();
						$('#success_apply_license_form').animate({opacity:1},300);
						lavuLog('fail');
					}

			},
			error: function(msg){
				lavuLog( "Error: ");
			}
		});
		}
	});
	$('#my_info_form').validate({
		rules:{
			starter_menu:"required",
			new_acc_address:{
				required:false
			},
		},//rules
		submitHandler: function(form){
			$.ajax({
				type: "POST",
				url: "./exec/my_info_update.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Form Submitted: "+msg);
					var result = msg.split('|');
					lavuLog('r[0]='+result[0]+', r[1]='+result[1]+', r[2]='+result[2]+', r[3]='+result[3]+', r[4]='+result[4]);

					if(result[0]=="personal"){
						$('div#p_field_submit_message').html(result[1]);
						$('#p_view_f_name').text(result[2]);
						$('#p_view_l_name').text(result[3]);
						$('#p_view_username').text(result[4]);
						$('div#p_view').show();
						$('div#p_edit').hide();
						//$('span#p_done_icon').hide();
						//$('span#p_edit_icon').show();
						$('#submit_personal').val('');
					}
					if(result[0] == 'availability'){
						lavuLog('availability changed');
						$('#a_view_monday').text(result[1]);
						$('#a_view_tuesday').text(result[2]);
						$('#a_view_wednesday').text(result[3]);
						$('#a_view_thursday').text(result[4]);
						$('#a_view_friday').text(result[5]);
						$('#a_view_saturday').text(result[6]);
						$('#a_view_sunday').text(result[7]);
						$('div#a_view').show();
						$('div#a_edit').hide();
						$('#submit_availability').val('');
					}
					if(result[0]=="company"){
						$('div#c_field_submit_message').html(result[1]);
						$('#c_view_comp').text(result[2]);
						$('#c_view_add').text(result[3]);
						$('#c_view_city').text(result[4]);
						$('#c_view_state').text(result[5]);
						$('#c_view_zip').text(result[6]);
						$('#c_view_country').text(result[7]);
						$('#c_view_phone').text(result[8]);
						$('#c_view_email').text(result[9]);
						$('#c_view_web').text(result[10]);
						$('div#c_view').show();
						$('div#c_edit').hide();
						//$('span#p_done_icon').hide();
						//$('span#p_edit_icon').show();
						$('#submit_company').val('');
					}

					if(result[0]=="notes"){
						$('div#add_field_submit_message').html(result[1]);
						$('#add_edit_textarea').text(result[2]);
						$('div#add_view').show();
						$('div#add_edit').hide();
					}

				}
			});//ajax
		}

	});
	$('#news_form').validate({
		rules:{

			add_news_title:{
				required:false
			},
			add_news_content:{
				required:false
			}
		},
		submitHandler: function(form){
			$('#success_news_form').stop(true,true);
			$('#success_news_form').css('color','gray');
			$('#success_news_form').css({'opacity':0, 'padding-bottom':'10px'});
			$('#success_news_form').html('Processing...');
			$('#success_news_form').show();
			$('#success_news_form').animate({opacity:1},300);
			$.ajax({
				type: "POST",
				url: "./exec/add_news.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Data Saved: ");
					var result = msg.split('|');
					lavuLog('r[0]='+result[0]+', r[1]='+result[1]);
					if(result[0] == 'success'){
						$('#news_form').hide();
						$('#success_news_form').stop(true,true);
						$('#success_news_form').css('color','black');
						$('#success_news_form').css({'opacity':0, 'padding-bottom':'20px'});
						$('#success_news_form').html(result[1]);
						$('#success_news_form').show();
						$('#success_news_form').animate({opacity:1},300);
						$('#ok_add_news').show();
						global_add_news_do_reload = true;
					}
					else{
						lavuLog('fail');
						$('#success_news_form').stop(true,true);
						$('#success_news_form').html(result[1]);
						$('#success_news_form').css('color','red');
						$('#success_news_form').css('opacity',0);
						$('#success_news_form').show();
						$('#success_news_form').animate({opacity:1},300);
						lavuLog('fail');
					}

			},
			error: function(msg){
				lavuLog( "Error: ");
			}
		});
		}
	});
	/*$('.loc_add_note_form').validate({
		submitHandler: function(form){
			$.ajax({
				type: "POST",
				url: "./exec/accounts_update.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Form Submitted: "+msg);

				}
			});//ajax
		}
	});*/
	//for image upload
	$('#license_points_form').validate({
		submitHandler: function(form){
			$.ajax({
				type: "POST",
				url: "./exec/buy_license_points.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "Data Saved: "+msg);
					//window.location.reload();
					var a_msg_parts = msg.split('|');
					var success = (a_msg_parts[0] == 'success');
					var note = a_msg_parts[1];
					if (success) {
						window.location.reload();
					} else {
						jlabel = $("#buy_license_with_points_label");
						jlabel.html(note);
						jlabel.css({ opacity: 0 });
						jlabel.animate({ opacity: 1 }, 500);
					}
			},
			error: function(msg){
				lavuLog( "Error: ");
			}
		});
		}
	});//license_points_form.validate
	$('#resource_upload_form').validate({
		rules:{
			new_resource_title:{
				required:false
			},
			new_resource_sub_title:{
				required:false
			},
			resource_description:{
				required:false
			},
			section_for_resource:{
				required:false
			},
			new_lead_city:{
				required:false
			},
			resource_section:{
				required:false
			},
			resource_sub_section:{
				requred:false
			},
			new_lead_phone:{
				required:false
			}
		},
		submitHandler: function(form){
        	/*
        	$('#success_lead_form').stop(true,true);
    		$('#success_lead_form').css('color','gray');
    		$('#success_lead_form').css({'opacity':0, 'padding-bottom':'10px'});
        	$('#success_lead_form').html('Processing...');
        	$('#success_lead_form').show();
        	$('#success_lead_form').animate({opacity:1},300);
        	*/
        	if (window.File && window.FileReader && window.FileList && window.Blob) {
        	} else {
        		alert('The File APIs are not fully supported in this browser.');
        		return;
        	}   
        	input = document.getElementById('resource_file');
        	if (!input) {
        		alert("Um, couldn't find the fileinput element.");
        	}
        	else if (!input.files) {
        		alert("This browser doesn't seem to support the `files` property of file inputs.");
        	}
        	else if (!input.files[0]) {
        		alert("Please select a file before clicking 'SUBMIT RESOURCE'");               
        	}
        	else {
        		file = input.files[0];
        		var fr = new FileReader();
        		fr.readAsDataURL(file);
        		fr.onload = function(evt){
				var contents = evt.target.result;
				/*
				alert( "Got the file.n" 
					+"name: " + file.name + "n"
					+"type: " + file.type + "n"
					+"size: " + file.size + " bytesn"
				);
	            */
				//document.getElementById('logo_preview').appendChild(document.createTextNode(fr.result));
				//var logo_username = $('#logo_username').attr('value');
				//encodeURIComponent(fr.result)
				//{submit_logo:'oh_yea',logo_file:fr.result,username:logo_username}
				var title = $('#new_resource_title').val();
				title = title.replace(' ','_');
				var sub_title = $('#new_resource_sub_title').val();
				//sub_title = sub_title.replace(' ','_');
				var description = $('#resource_description').val();
				var section = $('input[name=section_for_resource]:checked').val();
				var sub_section = $('#resource_sub_section').val();
				var filename = file.name;
				filename = filename.replace(/\\s+/g,'_');
				filename = filename.replace(' ','_');
				var filesize = file.size;
				var filetype = file.type;
				var featured = "";
				var homescreen = "";
				if($('#new_resource_featured:checked').val()=='featured'){
					var featured = 'featured';
				}
				if($('#new_resource_home:checked').val()=='home'){
					var homescreen = 'home';
				}
				//var type=$('#resource_file_type').val();
				$.ajax({
				  	type: "POST",
				  	url: "./exec/upload_resource.php",
				  	data: {
				  		title:title,
				  		sub_title:sub_title,
				  		description:description,
				  		section:section,
				  		sub_section:sub_section,
				  		filesize:filesize,
				  		filetype:filetype,
				  		filename:filename,
				  		featured:featured,
				  		homescreen:homescreen,
				  		resource:fr.result
				  	},
				  	cache: false,
				  	contentType: "application/x-www-form-urlencoded;charset=UTF-8",
				  	success: function(msg){
					  	lavuLog("submit resource upload response:: "+msg);
					  	
					  	var result = msg.split('|||');
					  	lavuLog("success!!"+result[0]+'--');
					  	if(result[0]=='SUCCESS'){
					  		lavuLog("success!!");
					  		$("#success_resource_form").html('');
						  	$("#success_resource_form").html("<strong 'style = color:#aecd37;'>SUCCESS!! "+result[1]+"</strong>");
						  	$("#success_resource_form").show();
						  	$('#resource_upload_form').hide();
						  	$('#ok_resource_buttons').show();
						  	$('#resource_upload_preview').html('');
						  	$('#resource_upload_preview').html('<img style="width:200px;" src="'+result[2]+'" alt="resource preview upload view">');
						  	$('#resource_upload_preview').show();
					  	}else{
						  	$("#success_resource_form").html("<strong 'style = color:red;'>FAILED:: "+result[1]+"</strong>");
						  	$("#success_resource_form").show();
						  	$('#resource_upload_form').hide();
						  	$('#ok_resource_buttons').show();
					  	}
					  	
					},
					error: function(msg){
						lavuLog("submit resource upload response ERROR:: "+msg);
						  	$("#success_resource_form").html("<strong 'style = color:red;'>ERROR:: "+msg+"</strong>");
						  	$("#success_resource_form").show();
						  	$('#resource_upload_form').hide();
						  	$('#ok_resource_buttons').show();
					}
				});//ajax*/
			};//onload
        }		
		}
	});//resource_upload_form.validate();
	$('#ok_resource_buttons').click(function(){
		$("#success_resource_form").html('');
		//$("#success_resource_form").html("<strong 'style = color:#aecd37;'>SUCCESS!! "+result[1]+"</strong>");
		$("#success_resource_form").hide();
		$('#resource_upload_form').show();
		$('#ok_resource_buttons').hide();
		$('#resource_upload_preview').hide();
		$(".opaque_modal_bg").css("display","none");
		$("#resource_upload_form_div").css("display","none");
	});//ok_resource_buttons
	$('#lead_note_form').validate({
		rules:{
			add_lead_note_title:{
				required:true
			},
			add_lead_note_content:{
				required:true
			}
		},
		submitHandler: function(form){
			$.ajax({
				type: "POST",
				url: "./exec/add_lead_note.php",
				data: $(form).serialize(),
				cache: false,
				success: function(msg){
					lavuLog( "lead_note_added: "+msg);
					//window.location.reload();
					var a_msg_parts = msg.split('|');
					var success = a_msg_parts[0];
					var note = a_msg_parts[1];
					if (success) {
						$('#lead_note_form').hide();
						$('#lead_notes_display').hide();
						$('#success_lead_note_form').stop(true,true);
						$('#success_lead_note_form').css('color','black');
						$('#success_lead_note_form').css({'opacity':0, 'padding-bottom':'20px'});
						$('#success_lead_note_form').html(a_msg_parts[1]);
						$('#success_lead_note_form').show();
						$('#success_lead_note_form').animate({opacity:1},300);
						$('#ok_add_lead_note').show();
						global_add_lead_note_do_reload = true;
					} else {

					}
			},
			error: function(msg){
				lavuLog( "Error: ");
			}
		});
		}
	});//lead_note_form.validate
	$('#quote_pdf_form').validate({
		rules:{
			quote:{
				required:true
			}
		},
		submitHandler: function(form){
			if (window.File && window.FileReader && window.FileList && window.Blob) {
			} else {
				alert('The File APIs are not fully supported in this browser.');
			  return;
			}   
			input = document.getElementById('quote');
			if (!input) {
			  alert("Couldn't find the fileinput element.");
			}
			else if (!input.files) {
			  alert("This browser doesn't seem to support the `files` property of file inputs.");
			}
			else if (!input.files[0]) {
			  alert("Please select a file before clicking 'Upload'");               
			}
			else {
				file = input.files[0];
				var fr = new FileReader();
				fr.readAsDataURL(file);
				fr.onload = function(evt){
					var contents = evt.target.result;
					var filename = file.name;
					var filesize = file.size;
					var filetype = file.type;
					var data = $(form).serializeArray();
					data.push({name:'filetype', value:filetype});
					data.push({name:'filename', value:filename});
					data.push({name:'filedata', value:fr.result});
					data.push({name:'action', value:'set'});
					data.push({name:'item', value:'pdf_and_payment_request'});
					$.ajax({
						type: "POST",
						url: "./exec/quote_responder.php",
						data: data,
						cache: false,
					  	contentType: "application/x-www-form-urlencoded;charset=UTF-8",
						success: function(msg){
							lavuLog( "quote_pdf_added: "+msg);
							//window.location.reload();
			        		var response = $.parseJSON(msg);
							if (response.success) {
								$('#quote_pdf_form').hide();
								$('#quote_pdf_display').hide();
								$('#success_quote_pdf_form').stop(true,true);
								$('#success_quote_pdf_form').css('color','black');
								$('#success_quote_pdf_form').css({'opacity':0, 'padding-bottom':'20px'});
								$('#success_quote_pdf_form').html('Your Quote PDF has been uploaded successfully.');
								$('#success_quote_pdf_form').show();
								$('#success_quote_pdf_form').animate({opacity:1},300);
								$('#ok_add_quote_pdf').show();
								global_assign_lead_do_reload = true;
							} else {

							}
						},
						error: function(msg){
							lavuLog( "Error: ");
						}
					});
				}
			}
		}
	});//quote_pdf_form.validate
	$('#show_l_form').click(function(){
		window.location.reload();
	});
	$('.remove_lead').click(function(){
		var l_id = $(this).attr('l_id');
		var r_username= $(this).attr('r_username');
		lavuLog("remove:: "+l_id+", reseller="+r_username);
		var conf = confirm("Are you sure you want to remove this lead?");
		if(conf){
			$.ajax("./exec/remove_lead.php", {
				type: "POST",
				data: {
					leadid: l_id,
					r_username:r_username
				},
				cache: false,
				async: false,
				success: function(data){
					lavuLog("removed:"+data);
					window.location.reload();
				}
			});//ajax
		}
	});//.remove_lead.click()
	$('#proc_tab').click(function(){
		//lavuLog('make_graph');
		draw_graph();
	});//proc_tab.click
	$('#display_graph').click(function(){
			$(this).hide();
			draw_graph();
		});
	function draw_graph(){
		$('#display_graph').hide();
		var d1 = [];
		var d2 = [];
		//var week_splits = 1;
		var week_splits = [];
		var week_counts = [];
		var week_counts_demo = [];
		for (var i = 0; i <14; i++) {
			week_counts[i] = 0;
			week_counts_demo[i] = 0;
		}
		var w_1 = new Date("August 01, 2013 00:00:00");
		//lavuLog(w_1);
		//week_splits.push(w_1);
		for (var i = 0; i < 14; i++) {
			var new_d = new Date(w_1.setDate(w_1.getDate()+7));
			//lavuLog(w_1);
			week_splits.push(new_d);
		}
		// Split timestamp into [ Y, M, D, h, m, s ]
		var created_stamps = $(".created_stamp");
		var count = 1;
		$.each(created_stamps, function(k,v) {
			v = $(v);
			var t = v.text().split(/[- :]/);
			var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
			for(var i = 0;i<14;i++){
				if(d < week_splits[i]){
					week_counts[i]++;
					//lavuLog('boob:: '+', '+i+', '+week_splits[i]);
					i=30;
				}
			}
			d1.push([d, count]);
			//lavuLog('boob::  ['+d+', '+count+']');
			count++;
			
		});//.each(created_stamps, function(k,v) yo)
		
		//lavuLog(week_counts);
		
		w_1.getDate()
		var demo_stamps = $(".demo_stamp");
		count = 1;
		$.each(demo_stamps, function(k,v) {
			v = $(v);
			var t = v.text().split(/[- :]/);
			var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);
			for(var i = 0;i<14;i++){
				if(d < week_splits[i]){
					week_counts_demo[i]++;
					//lavuLog('boob:: '+', '+i+', '+week_splits[i]);
					i=30;
				}
			}
			d2.push([d, count]);
			count++;
			//lavuLog('boob::  '+time);
		});//.each
		var d3 = [];
		var d4 = [];
		//lavuLog(week_counts_demo);
		for (var i = 0; i <14; i++) {
			d3.push([week_splits[i],week_counts[i]]);
			d4.push([week_splits[i],week_counts_demo[i]]);
			//lavuLog('weiner::  ['+week_splits[i]+', '+week_counts_demo[i]+']');
		}
		//var t = "2010-06-09 13:12:01".split(/[- :]/);

		// Apply each element to the Date function
	   //var d = new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]);

		//alert(d);
		//lavuLog(d1);
		//lavuLog(d3);
		//{label: "demo", data: d2, color:"blue"},
		$.plot("#graphalaphigus",[
			{label: "converted", data: d1, color:"green"},
			{label: "demo by week", data: d4, color:"blue"},
			{label: "conv. by week", data: d3, color:"red"}
			], 
			{legend:{
				position:"nw"
			},
			series: {
				lines: { show: true },
				points: { show: true }
			}, 
			xaxis:{
				minTickSize: [7, "day"],
				mode:"time"
				}
		});
		}//draw_graph()


	display_account_nav();
	function display_account_nav(){
		var len = $('.acc_count').length;
		var prev_range = $('#prev_start').attr('range');
		lavuLog('prev_range:: '+prev_range);
		if(len === 0){
			$('#next_start').hide();
		}else if(len < 50){
			$('#next_start').hide();
		}
		if(prev_range == '#'){
				$('#prev_start').hide();
		}

	}
	function refreshPage() {
		lavuLog("auto refreshed");
		window.location = location.href;
	}
	window.onload = setupRefresh;

	function setupRefresh() {
		lavuLog("setupRefresh");
		setTimeout(refreshPage, 2400000);
	}
$('#mercury_time_range_submit').click(function(){
	var start_time = $('#date_start_mercury').val();
	var end_time = $('#date_end_mercury').val();
	var access = $('#distro_username').val();
	lavuLog("mercury time range:: "+start_time+", to:: "+end_time+", "+access);
	$.ajax({
		type: "POST",
		url: "./exec/distro_reports.php",
		data: {start_time_mercury:start_time,
			end_time_mercury:end_time,
			dr_function:"view_mercury_range",
			access:access
		},
		cache: false,
		success: function(msg){
		 //$('#loading_report').hide();
			//lavuLog("merc_time:: "+msg);
		   build_mercury_time_range_display(msg);
		 //$('#tab_report_link').show();
		}
    });//ajax
});//mercury_time_range_submit.click()
function build_mercury_time_range_display(leads_json, source){
	var jsonr = eval(leads_json);
	var display_body = "";

	var converted = 0;
	var converted_try = 0;

	var demo_account = 0;
	var demo_account_try = 0;

	var contacted = 0;
	var contacted_try = 0;

	var dead = 0;
	var dead_try = 0;

	var total = 0;
	var total_try = 0;
	source = 'mercury';
	//var display_class = '';


	$.each(jsonr,function(i, item){
	//display_class='';
	 total++;
	 if(item.lead_source == source){
		 	total_try++;
	 	}
	 if(item.license_applied != '0000-00-00 00:00:00')
	 {//converted
	 	converted++;
	 	if(item.lead_source == source){
		 	converted_try++;
		 	$('#table_time_range').append('<tr class=" time_row time_conv_source"><td>'+item.company_name+'</td><td>'+item.state+'</td><td>'+item.lastname+', '+item.firstname+'</td><td>'+item.phone+'</td><td>'+item.distro_code+'</td></tr>');
	 	}
		 
	 }else if(item.made_demo_account != '0000-00-00 00:00:00')
	 {//demo_account
		 demo_account++;
		 if(item.lead_source == source){
		 	demo_account_try++;
		 	$('#table_time_range').append('<tr class=" time_row time_demo_source"><td>'+item.company_name+'</td><td>'+item.state+'</td><td>'+item.lastname+', '+item.firstname+'</td><td>'+item.phone+'</td><td>'+item.distro_code+'</td></tr>');
	 	}
	 }else if(item.contacted != '0000-00-00 00:00:00')
	 {//contacted
		 contacted++;
		 if(item.lead_source == source){
		 	contacted_try++;
		 	$('#table_time_range').append('<tr class="time_row time_contacted_source"><td>'+item.company_name+'</td><td>'+item.state+'</td><td>'+item.lastname+', '+item.firstname+'</td><td>'+item.phone+'</td><td>'+item.distro_code+'</td></tr>');
	 	}
	 }
	 if(item._canceled == '1')
	 {
		 dead++;
		 if(item.lead_source == source){
		 	dead_try++;
		 	$('#table_time_range').append('<tr class="time_row time_dead_source"><td>'+item.company_name+'</td><td>'+item.state+'</td><td>'+item.lastname+', '+item.firstname+'</td><td>'+item.phone+'</td><td>'+item.distro_code+'</td></tr>');
	 	}
	 }
		//lavuLog(item.username);	
	})//each(i, item)
	 //lavuLog('t_ice:: '+converted_try);
	 var converted_pct = Math.round(converted/total*100);
	 var converted_pct_try = Math.round(converted_try/total_try*100);
	 
	 var demo_pct = Math.round(demo_account/total*100);
	 var demo_pct_try = Math.round(demo_account_try/total_try*100);
	 
	 var contacted_pct = Math.round(contacted/total*100);
	 var contacted_pct_try = Math.round(contacted_try/total_try*100);
	 
	 var dead_pct = Math.round(dead/total*100);
	 var dead_pct_try = Math.round(dead_try/total_try*100);
	 
	$("#merc_time_table").html("<thead><th>Developer</th><th class='merc_raw_th'>Leads</th><th class='merc_raw_th'>Dead</th><th>Dead %</th><th class='merc_raw_th'>Made Contact</th><th>Contact %</th><th class='merc_raw_th'>Demo Account</th><th>Demo Account %</th><th class='merc_raw_th'>License Applied</th><th>License %</th></thead><tr><td>Lavu inc.</td><td>"+total+"</td><td>"+dead+"</td><td>"+dead_pct+"</td><td>"+contacted+"</td><td>"+contacted_pct+"</td><td>"+demo_account+"</td><td>"+demo_pct+"</td><td>"+converted+"</td><td>"+converted_pct+"</td></tr><tr><td>Try campaign</td><td>"+total_try+"</td><td class='see_time_report' report_id='time_dead_source'>"+dead_try+"</td><td class='see_time_report' report_id='time_dead_source'>"+dead_pct_try+"</td><td class='see_time_report' report_id='time_contacted_source'>"+contacted_try+"</td><td class='see_time_report' report_id='time_contacted_source'>"+contacted_pct_try+"</td><td class='see_time_report' report_id='time_demo_source'>"+demo_account_try+"</td><td class='see_time_report' report_id='time_demo_source'>"+demo_pct_try+"</td><td class='see_time_report' report_id='time_conv_source'>"+converted_try+"</td><td class='see_time_report' report_id='time_conv_source'>"+converted_pct_try+"</td></tr>");
	$("#merc_time_table_div").show();
	 lavuLog(total_try+", "+converted_try+", "+converted_pct_try+"%");
}//build_mercury_time_range_display()
//
//FORM PROCESSING FUNCTIONS NOT USED RIGHT NOW
//
function p_edit_change(values){

	$.post('../beta/form_process.php', function(data){
			alert(JSON.stringify(data));
	});
}
$('.view_reviews').click(function(){
	var u_name = $('#new_lead_distro_username').val();
	console.log("datanames for "+u_name+"::");
	var dnames = new Array();
	$('input[name="dataname"]').each(function(){
		//console.log("dataname=  "+$(this).val());
		dnames.push($(this).val());
	});//each input dataname
	dnames.forEach(function(entry) {
		//console.log(entry);
	});
		
	$.ajax({
		url:"./exec/find_reviews.php",
		type:"POST",
		data:{'json':JSON.stringify(dnames),'distro_code':u_name}
	}).done(function(response){

		//console.log(response);
		display_survey_stats(response);
	});//$.ajax.done();	
	});//$('#view_reviews').click()
	$('.li_accounts_tab').click(function(){
		var u_name = $('#new_lead_distro_username').val();
		console.log("datanames for "+u_name+"::");
		var dnames = new Array();
		$('input[name="dataname"]').each(function(){
			console.log("dataname=  "+$(this).val());
			dnames.push($(this).val());
		});//each input dataname
		dnames.forEach(function(entry) {
			//console.log(entry);
		});
		
		$.ajax({
			url:"./exec/find_reviews.php",
			type:"POST",
			data:{'json':JSON.stringify(dnames),'distro_code':u_name}
		}).done(function(response){

			//console.log(response);
			display_survey_stats(response);
		});//$.ajax.done();	
	});//$('#view_reviews').click()
	function display_survey_stats(responses_json){
			//console.log("responses::"+ responses_json);
			var jsonr = eval(responses_json);
					 
					 $("#distro_report_table_display thead").html("<tr><th>RESTAURANT ID</th><th>STATE</th><th>AMOUNT</th><th>TIMESTAMP</th></tr>");
					 $("#distro_report_table_display tbody").html("");
					 var table_body = "";
					 //lavuLog('jsonr.length::'+jsonr.length);
					 var total = 0;
					$.each(jsonr,function(i, item){
						//console.log("booger:: .acc_"+item.dataname+' said:: '+item.value_long);
						$('.tooltip_'+item.dataname).remove();
						$('.tooltip_'+item.dataname).parent().remove();
						var tip_top = item.value_long;
						var tip_res = '';
						if(item.value_long == ''){
							tip_top = 'Didn\'t leave comment';
						}else{
							tip_top = item.value_long.replace(/(\r\n|\n|\r)/gm,"");
						}
						if(item.value5 != ''){
							tip_res='<span style="padding-top:4px;display:inline-block;width:21px;" class="review_'+item.value5+'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+item.value5+'</span>';
						}
						$('.acc_'+item.dataname).append('<span class="review_span_wrapper"><a style="padding-top:6px;padding-left:38px;" href="#" title="'+tip_top+'" class="tooltip tooltip_'+item.dataname+'"><span title="More" style="padding-top:4px;display:inline-block;width:21px;" class="review_'+item.value4+'">'+item.value4+'</span>'+tip_res+'</a></span>');

					});
	}//display_survey_stats


	$('.sales_lead_display_time_segment_selection').click(function(){
		$('.sales_lead_display_time_segment_selection').css('font-weight','100');
		$('#sales_lead_display_time_choice').html('');
		$(this).css('font-weight','bold');
		var new_choice = $(this).text();
		lavuLog('choice == '+new_choice);
		$('#sales_lead_display_time_choice').html('MONTH - '+new_choice);
	});//$('.sales_lead_display_time_segment_selection').click()
	$('.resources_home_section_header').click(function(){
		var section_to_open = '#'+$(this).attr('section_to_go');
		lavuLog("opening... "+section_to_open);
		$('.resources_home_btn').show();
		$('#resources_home_section_wrapper').hide();
		$('.resources_category_section').hide();
		$('.resources_home_btn').css('margin','0 0 0 10px');
		$('.resources_home_btn').css('top','43px');
		$('.resources_home_btn').css('padding','7px');
		$('.resources_section_main_head').css('margin-left','55px');
		$(section_to_open).show();
	});//resources_subsection_head.click
	$('.view-all-arrow').click(function(){
		var section_to_open = '#'+$(this).attr('section_to_go');
		lavuLog("opening... "+section_to_open);
		$('.resources_home_btn').show();
		$('#resources_home_section_wrapper').hide();
		$('.resources_category_section').hide();
		$('.resources_home_btn').css('margin','0 0 0 10px');
		$('.resources_home_btn').css('top','43px');
		$('.resources_home_btn').css('padding','7px');
		$('.resources_section_main_head').css('margin-left','55px');
		$(section_to_open).show();
	});//view-all-arrow.click
	$('.resources_home_btn').click(function(){
		
		parse_resource_files();
	});//resources_home_btn.click()
	parse_resource_files();
	$('.aallow_mercury').click(function(){
		var lead_id = $(this).attr('lead_id');
	    var action = 'mercury_permission_toggle';
	    var thisid = $(this).attr('id');
	    var current_permission = $(this).attr('permission');
	    
	    $.ajax({
			type:"POST",
			url:"./exec/time_milestones.php",
			data:{lead_id:lead_id,action:action,current:current_permission},
			success:function(msg){
				lavuLog(" success!! "+msg+",  #"+thisid);
				//$('body').replaceWith(msg);
				//window.location.reload();
				/*
				$('.acc_head:even').css('background-color','#f1f5ee');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up12');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up24');
				$('#un_lead_head_'+thisid).removeClass('need_follow_up48');
				$('.need_follow_up12').css('background-color','yellow');
				$('.need_follow_up24').css('background-color','orange');
				$('.need_follow_up48').css('background-color','red');
				window.location.reload();

				*/
				current_permission = $('#'+thisid).attr('permission');
				lavuLog(current_permission);
				/*
				if(current_permission == 'allowed' ){
					$('#'+thisid).text('NO MERC');
					$('#'+thisid).attr('permission', 'notallowed');
					$('#'+thisid).css('color','red');
				}else{
					$('#'+thisid).text('OK MERC');
					$('#'+thisid).attr('permission', 'allowed');
					$('#'+thisid).css('color','red');
					$('#'+thisid).css('color','#0684b5');
				}
				*/

			},
			error:function(msg){
				lavuLog("error!!"+msg);
			}
		});//ajax

	});//allow_mercury.click

	// .get_qli.click() event
	$('.get_qli').click(function() {
		var dc = $(this).attr('dc');
		var q_id = $(this).attr('q_id');
		lavuLog("get_qli: dc="+ dc +" q_id="+q_id);
		$.ajax("./exec/quote_responder.php", {
			type: "POST",
			data: {
        		action: 'get',
        		item: 'reseller_quote',
        		distro_code: dc,
        		reseller_quote_id: q_id
        	},
        	cache: false,
        	async: true,
        	success: function(data) {
        		var quote = $.parseJSON(data);
				$('#quoteinfo_table_'+ q_id +' .quoteinfo_status').html(quote.status.charAt(0).toUpperCase() + quote.status.slice(1));
				$('#quoteinfo_table_'+ q_id +' .quoteinfo_quote_name').html(quote.quote_name);
				$('#quoteinfo_table_'+ q_id +' .quoteinfo_current_pos').html(quote.current_pos);
				$('#quoteinfo_table_'+ q_id +' .quoteinfo_current_processor').html(quote.current_processor);
				$('#quoteinfo_table_'+ q_id +' .quoteinfo_requested_processor').html(quote.requested_processor);
				$('#quoteinfo_table_'+ q_id +' .quoteinfo_notes').html(quote.notes);
				$('#quoteinfo_table_'+ q_id +' .quoteinfo_customer_notes').html(quote.customer_notes);

				if (quote.lineitems != null) {
	        		// Clear any existing rows in the table
	        		$('#lineitems_table_' + q_id +' tr').remove();

					var $header_row = $('<tr style="border-bottom:1px solid black">').append(
		        			$('<td class="hdr" style="padding:1px; vertical-align:top white-space:nowrap">').text('PRODUCT'),
		        			$('<td class="hdr" style="padding:1px; vertical-align:top white-space:nowrap">').text('QTY'),
		        			$('<td class="hdr" style="padding:1px; vertical-align:top white-space:nowrap; text-align:right">').text('UNIT PRICE')
		        		).appendTo('#lineitems_table_'+ q_id);
		        	$.each( quote.lineitems, function(i, rec) {
		        		var $tr = $('<tr>').append(
		        			$('<td style="padding:5px; vertical-align:top white-space:nowrap">').text(rec.product),
		        			$('<td style="padding:5px; vertical-align:top white-space:nowrap">').text(rec.qty),
		        			$('<td style="padding:5px; vertical-align:top white-space:nowrap; text-align:right">').text(rec.unit_price)
		        		).appendTo('#lineitems_table_'+ q_id);
		        	} );
					var $total_price_row = $('<tr style="border-top:1px solid black">').append(
		        			$('<td class="hdr" style="padding:1px; vertical-align:top white-space:nowrap; text-align:right">').text('TOTAL'),
		        			$('<td style="padding:5px; vertical-align:top white-space:nowrap">').text(''),
		        			$('<td style="padding:5px; vertical-align:top white-space:nowrap; text-align:right">').text(quote.total_price)
		        		).appendTo('#lineitems_table_'+ q_id);
					var $total_cost_row = $('<tr style="border-top:1px solid black">').append(
		        			$('<td class="hdr" style="padding:1px; vertical-align:top white-space:nowrap; text-align:right">').text('YOUR COST'),
		        			$('<td style="padding:5px; vertical-align:top white-space:nowrap">').text(''),
		        			$('<td style="padding:5px; vertical-align:top white-space:nowrap; text-align:right">').text(quote.total_cost)
		        		).appendTo('#lineitems_table_'+ q_id);
	        	}
	        }
	   } ); //ajax
	}); //.get_qli.click()	

	attach_accordion_events();
});//document.ready();
