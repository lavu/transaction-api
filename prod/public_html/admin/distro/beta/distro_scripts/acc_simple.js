/***********************************************************************************************************************
DOCUMENT: includes/javascript.js
DEVELOPED BY: Ryan Stemkoski
COMPANY: Zipline Interactive
EMAIL: ryan@gozipline.com
PHONE: 509-321-2849
DATE: 3/26/2009
UPDATED: 3/25/2010
DESCRIPTION: This is the JavaScript required to create the accordion style menu.  Requires jQuery library
NOTE: Because of a bug in jQuery with IE8 we had to add an IE stylesheet hack to get the system to work in all browsers. I hate hacks but had no choice :(.
************************************************************************************************************************/
$(document).ready(function() {
	 
	 
	 //ACCORDION BUTTON ACTION (ON CLICK DO THE FOLLOWING)
//	$('.expand_account_btn').click(function() {
	$('.acc_arrow').click(function() {	
		lavuLog("acc_butt_clicked");
		//REMOVE THE ON CLASS FROM ALL BUTTONS
		$(this).parent().removeClass('on');
		  
		//NO MATTER WHAT WE CLOSE ALL OPEN SLIDES
	 	$('.accordionContent').slideUp('normal');
   
		//IF THE NEXT SLIDE WASN'T OPEN THEN OPEN IT
		if($(this).parent().next().is(':hidden') == true) {
		
			//CHANGE LEFT ARROW TO DOWN ARROW
			$(this).attr('src', './images/arrow_down.png');
			//ADD THE ON CLASS TO THE BUTTON
			$(this).parent().addClass('on');
			  
			//OPEN THE SLIDE
			$(this).parent().next().slideDown('normal');
		 } else{
		 	//CHANGE DOWN ARRROW TO LEFT ARROW
			 $(this).attr('src', './images/arrow_left.png');			 
		 }
		  
	 });
	$('.resources_subsection_head').click(function() {
		//lavuLog("resources_subsection_head_clicked");
		//REMOVE THE ON CLASS FROM ALL BUTTONS
		$(this).removeClass('on');

		//NO MATTER WHAT WE CLOSE ALL OPEN SLIDES
	 	$('.resources_subsection_content').slideUp('normal');
		//IF THE NEXT SLIDE WASN'T OPEN THEN OPEN IT
		if($(this).next().is(':hidden') == true) {

			//CHANGE LEFT ARROW TO DOWN ARROW
			//$(this).attr('src', './images/arrow_down.png');
			//ADD THE ON CLASS TO THE BUTTON
			$('.sub_section_arrow_open').hide();
			$('.sub_section_arrow_closed').css('display','inline-block');

			$(this).children('.sub_section_arrow_open').css('display','inline-block');
			$(this).children('.sub_section_arrow_closed').hide();
			$('.sub_section_arrow_open').attr('display','inline-block');
			//$(this).addClass('on');

			//OPEN THE SLIDE
			$(this).next().slideDown('normal');
		 } else{
		 	//CHANGE DOWN ARRROW TO LEFT ARROW
			// $(this).attr('src', './images/arrow_left.png');	
			$(this).children('.sub_section_arrow_open').hide();
			$(this).children('.sub_section_arrow_closed').css('display','inline-block');
			$('.sub_section_arrow_open').attr('display','inline-block');	 
		 }

	 });  
	
	/*** REMOVE IF MOUSEOVER IS NOT REQUIRED ***/
	
	//ADDS THE .OVER CLASS FROM THE STYLESHEET ON MOUSEOVER 
	$('.accordionButton').mouseover(function() {
		$(this).addClass('over');
		
	//ON MOUSEOUT REMOVE THE OVER CLASS
	}).mouseout(function() {
		$(this).removeClass('over');										
	});
	
	/*** END REMOVE IF MOUSEOVER IS NOT REQUIRED ***/
	
	
	/********************************************************************************************************************
	CLOSES ALL S ON PAGE LOAD
	********************************************************************************************************************/	
	$('.accordionContent').hide();

});
