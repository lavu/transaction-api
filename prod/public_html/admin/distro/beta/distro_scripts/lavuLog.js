/*
	Lavu log is a super simple logging function and all it does is 
	make sure that if you are on IE you do not get console.log outputs. 
	The reason for this is be cause we found that if the console is not open
	in IE than it will freeze the browser if you try to write to the console. so 
	with this in place we can output to any browser that isnt IE.
	-ncc 7/25/12

*/
function lavuLog(output){
	if( navigator.appName != 'Microsoft Internet Explorer')
		console.log(output);
}