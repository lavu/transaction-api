<?php
/*
require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
if (!isset($o_package_container))
	$o_package_container = new package_container();
*/

global $o_specialistTab;
$o_specialistTab = new SpecialistTabClass();

class SpecialistTabClass {

	function __construct() {
	}
	public function tabToStr($o_current_user) {
		$title ='
		<li class="tab"><a href="#resellers_tab">SPECIALISTS</a></li>
		';//

		$content ='<div id="resellers_tab" class="tab_section tab00">';
		$content .= '
		<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;cursor:pointer;">SPECIALISTS</div>
					<div style="border:1px solid red;float:right;text-align:center;width:200px;font:20px verdana;display:none;margin-top:22px;background:#aecd37;cursor:pointer;" id="submit_changes">SUBMIT CHANGES</div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
		</div>
		<div class="adjust_body">';
		
		$content .= '
		<div id="reseller_acc_wrapper">';
		$current_access = 'admin';
		$content .= self::get_specialist_search($current_access);
		$content .= '
		<div id="specialist_lead_desc" style="height:30px;margin:10px auto;">
			<span style="width:200px;">NAME</span>
			<span style="width:200px;">COMPANY</span>
			<span style="width:154px;">CITY-STATE</span>
			<span style="width:190px;">EMAIL</span>
			<span style="width:65px;">LOGIN</span>
			<span style="width:50px;">LEVEL</span>
			<span style="width:50px;">REMOVE</span>
			<br style="clear:left;" />
		</div>
		';
		$content .= '<div id="specialist_display"></div>';
		$content .= '</div><!--reseller div close 1-->';
		$content .= '</div><!--reseller div close 2-->';
		$content .= '</div><!--reseller div close 3-->';

		return array('title'=>$title, 'content'=>$content);
	}//tabToStr($o_current_user, &$a_resellers)
	private function get_specialist_search($current_access){
	    //echo $o_current_user->get_info_attr('access');
		$lead_search_content = '
		<div id="specialist_search_area" class="portal_search in_progress_search" style="display:inline-block;margin-left:60px;width:500px;height:30px;">
			<input type="text" id="specialist_searchInput" placeholder="Search For Specialist" name="specialist_Search" style="height:24px;width:380px;font-size:20px;z-index:100;" />
			<div id="specialist_submit" style="width:65px;border:1px solid black;padding:6px;float:left;cursor:pointer;" onclick="filter_specialistSearch();">SEARCH</div>
		</div>
		<div class="portal_search_filter" id="specialist_search_filter" style="text-align:left;margin-left:60px;">Add Filters to Search:
			<input type="checkbox" id="specialist_use_filters" onchange=\'if($("#specialist_filters").css("display")=="none") $("#specialist_filters").show();else $("#specialist_filters").hide();\' />
		</div>
		<table id="specialist_filters" class="portal_search_filter_table" style="border:1px solid black;border-collapse:collapse;display:none;margin-left:60px;">
			<form action="#" id="specialist_filter_form" method="post">
				<input type="hidden" id="filter_specialist_results" name="filter_specialist_results" value="filterSearch"/>
				<tbody style="display:table-row-group;vertical-align:middle;border-color:inherit;">
					<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Mercury Specialists:
								<input type="checkbox" id="specialist_filter_mercury" name="specialist_filter_mercury" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Ingram Specialists:
								<input type="checkbox" id="specialist_filter_ingram" name="specialist_filter_ingram" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Lavu Admins:
								<input type="checkbox" id="specialist_filter_admin" name="specialist_filter_admin" style="float:right;" />
							</small>
							<br />
						</td>
					</tr>
					<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Need to Agree:
								<input type="checkbox" id="specialist_filter_agree" name="specialist_filter_agree" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>3 stars:
								<input type="checkbox" id="specialist_filter_3" name="specialist_filter_3" style="float:right;" />
							</small>
							<br />
						</td>';
					if($current_access=='admin'){
							$lead_search_content .= '
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>2 stars:
									<input type="checkbox" id="specialist_filter_2" name="specialist_filter_2" style="float:right;" />
								</small>
								<br />
							</td>
						</tr>
						<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>1 star:
									<input type="checkbox" id="specialist_filter_1" name="specialist_filter_1" style="float:right;" />
								</small>
								<br />
							</td>
						</tr>';
					}else{
						$lead_search_content .= '
						<input type="hidden" id="specialist_filter_try" name="specialist_filter_try" value="mercury" />
						</tr>';
					}
					$lead_search_content .= '
					<tr style="display:none;">
						<td>
							<select name="specialist_filter_time" id="specialist_filter_time">
								<option value="">Select Time Range</option>
								<option value="-1 week">Past Week</option>
								<option value="-2 weeks">Past 2 Weeks</option>
								<option value="-1 month">Past 30 Days</option>
								<option value="-2 months">Past 60 Days</option>
								<option value="-3 months">Past 90 Days</option>
								<option value="-6 months">Past 6 Months</option>
								<option value="">All Time</option>
							</select>
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Canceled:
								<input type="checkbox" id="specialist_filter_canceled" name="specialist_filter_canceled" style="float:right;" />
							</small>
							<br />
						</td>
					</tr>
				</tbody>
			</form>
		</table>';
		return $lead_search_content;
	}//get_specialist_search()

}// class SpecialistTabClass