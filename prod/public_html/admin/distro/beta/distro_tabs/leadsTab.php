<?php

require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
if (!isset($o_package_container))
	$o_package_container = new package_container();

global $o_leadsTab;
$o_leadsTab = new LeadsTabClass();

class LeadsTabClass {

	function __construct() {
	}

	public function tabToStr($o_current_user, &$a_licenses_not_upgrades_not_special) {
		global $o_package_container;
		$uname = $o_current_user->get_info_attr('username');
		$d_id = $o_current_user->get_info_attr('id');
		$d_access = $o_current_user->get_info_attr('access');
		$new_lead_count = 8;//count_new_leads($leads);
		//<span><img src="./images/circ_leads.png"></span>	<span style="font-size:16px;text-align:center;">'.$new_lead_count.'</span>
		$title ='<li class="tab" style="width:110px;" onclick="get_unaccepted_leads(\''.$uname.'\');"><a href="#leads_tab">
			<span id="leads_tab_title_count" style="background-image:url(./images/circ_leads.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$new_lead_count.'
			</span>
			LEADS</a></li>
		';
				$content ='<div id="leads_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">NEW LEADS</div>
					<div id="unass_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;padding-top:25px;">
					<img src="./images/btn_create_lead.png" alt="btn_apply" class="new_lead_btn" style="cursor:pointer;" /></span>
					</div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
			// <div style="background-image:url(./images/lead_legend.png);"></div>
		$content .= '<div class="adjust_body">
			<div style="height:30px;width:950px;margin-bottom:20px;margin-left:8px;"><img src="./images/lead_legend.png" alt="llegend"></div>
			<div id="lead_search_required_data" dcode="'.$uname.'" did="'.$d_id.'" daccess="'.$d_access.'" style="display:none;"></div>
		';
		$current_access = 'admin';
        //beginning of accordian
		$content .= '<div id="lead_acc_wrapper">
		';
		$content .= '
		<div id="not_accepted_yet round_corners" style="background:#F0F0FA;border:1px solid #717074;margin-bottom:20px;">
			<div style="font:24px Verdana;color:#717069;padding-top:17px;width:950px;margin-bottom:10px;">WAITING TO BE ACCEPTED <span style="margin-left:20px;font:13px DIN;"><img src="./images/circ_green.png">=Already has demo account</span></div>
			<div class="not_accepted_yet_list"></div>
		</div>';
		$content .= '<div>'.self::get_lead_search($current_access).'</div>';
		$content .= '
		</div>
		<div id="lead_desc" style="display:none;">
			<span style="width:60px;">STATUS</span>
			<span style="width:153px;">BUSINESS NAME</span>
			<span style="width:180px;">LOCATION</span>
			<span style="width:169px;">EMAIL</span>
			<span style="width:137px;">NAME</span>
			<span style="width:120px;">PHONE</span>
			<span style="width:72px;">NOTES</span>
			<span style="width:50px;">REMOVE</span>
			<br style="clear:left;" />
		</div>
		<div id="leadTab_display" class="client_display">
		</div></div></div>
		';
		return array('title'=>$title, 'content'=>$content);
	}//tabToStr()
	private function get_lead_search($current_access){
	    //echo $o_current_user->get_info_attr('access');
		$lead_search_content = '

		<div id="lead_search_area" class="portal_search in_progress_search" style="display:inline-block;margin-left:60px;width:500px;height:30px;">
			<div style="margin:10px 0 20px 0;font:30px Verdana;color:#439789;width:750px;text-align:left;">SEARCH LEADS</div>
			<input type="text" id="lead_searchInput" placeholder="Lead Search" name="leadSearch" style="height:24px;width:380px;font-size:20px;z-index:100;" />
			<div id="lead_submit" style="width:65px;border:1px solid black;padding:6px;cursor:pointer;float:right;" onclick="filter_acceptedLeadSearch();">SEARCH</div>
		</div>
		<div class="portal_search_filter" id="lead_search_filter" style="text-align:left;margin-left:60px;">Add Filters to Search:
			<input type="checkbox" id="lead_use_filters" onchange=\'if($("#lead_filters").css("display")=="none") $("#lead_filters").show();else $("#lead_filters").hide();\' />
		</div>
		<table id="lead_filters" class="portal_search_filter_table" style="border:1px solid black;border-collapse:collapse;display:none;margin-left:60px;">
			<form action="#" id="lead_filter_form" method="post">
				<tbody style="display:table-row-group;vertical-align:middle;border-color:inherit;">
					<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Leads With Demos:
								<input type="checkbox" id="lead_filter_demo" name="lead_filter_demo" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Unaccepted Leads:
								<input type="checkbox" id="lead_filter_unacc" name="lead_filter_unacc" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Accepted leads:
								<input type="checkbox" id="lead_filter_acc" name="lead_filter_acc" style="float:right;" />
							</small>
							<br />
						</td>
					</tr>
					<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Contacted Leads:
								<input type="checkbox" id="lead_filter_cont" name="lead_filter_cont" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Sold Leads:
								<input type="checkbox" id="lead_filter_sold" name="lead_filter_sold" style="float:right;" />
							</small>
							<br />
						</td>';
						if($current_access=='admin'){
							$lead_search_content .= '
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>try.poslavu.com:
									<input type="checkbox" id="lead_filter_try" name="lead_filter_try" style="float:right;" />
								</small>
								<br />
							</td>
						</tr>
						<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>baripadpos.com:
									<input type="checkbox" id="lead_filter_barmicro" name="lead_filter_barmicro" style="float:right;" />
								</small>
								<br />
							</td>
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>pizzaipadpos.com:
									<input type="checkbox" id="lead_filter_pizzamicro" name="lead_filter_pizzamicro" style="float:right;" />
								</small>
								<br />
							</td>
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>lavuhospitality.com:
									<input type="checkbox" id="lead_filter_hosmicro" name="lead_filter_hosmicro" style="float:right;" />
								</small>
								<br />
							</td>
						</tr>';
					}else{
						$lead_search_content .= '
						<input type="hidden" id="lead_filter_try" name="lead_filter_try" value="mercury" />
						</tr>';
					}
					$lead_search_content .= '
					<tr>
						<td>
							<select name="lead_filter_time" id="lead_filter_time">
								<option value="">Select Time Range</option>
								<option value="-1 week">Past Week</option>
								<option value="-2 weeks">Past 2 Weeks</option>
								<option value="-1 month">Past 30 Days</option>
								<option value="-2 months">Past 60 Days</option>
								<option value="-3 months">Past 90 Days</option>
								<option value="-6 months">Past 6 Months</option>
								<option value="">All Time</option>
							</select>
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Canceled:
								<input type="checkbox" id="lead_filter_canceled" name="lead_filter_canceled" style="float:right;" />
							</small>
							<br />
						</td>
					</tr>
				</tbody>
			</form>
		</table>';
		return $lead_search_content;
	}//get_lead_search

	private function count_new_leads($leads){
		$count = 0;
		foreach($leads as $dname=>$attrs){
		$ls = isset($attrs['lead_status'])?$attrs['lead_status']:'accept';
			//echo 'lead_status::<br />'.$attrs['lead_status'].'<br /><br />';
			if($ls === 'accept'){
				//echo 'new lead::<br/>'.$attrs['company_name'].'<br /><br />';
				$count++;
			}
		}
		//echo 'total new::<br />'.$count.'<br /><br />';
		return $count;
	}//count_new_leads
	private function lead_color($stat, $div,$color_class){
	    //echo 'stat='.$stat.' div='.$div.' cc='.$color_class;
		if($stat === $div){
			//echo '  true';
			return $color_class;
		}
	}//lead_color
	private function lead_type_content($level, $data_name, $s_licenses_string, $i_lead_id, $o_current_user){
		if($level === 'accept'){
			return '<div style="margin:50px;font:12px Verdana;color:#717074;">
				<span style="margin-right:30px;">Do you accept this new account</span>
					<img src="./images/btn_accept.png" style="margin-right:10px;cursor:pointer;" class="accept_lead_confirm" alt="btn_accpt"/>
						<span class="accept_vert_div" style="border-right:1px solid #999;margin-right:10px;"></span>
					<img src="./images/btn_decline.png" style="cursor:pointer;" alt="btn_dec" class="decline_lead_confirm"/>
			</div>';
		}else if($level === 'contact'){
			$dname_nospace = str_replace(' ', '', $data_name);
			$dname_nospace = str_replace('\'', '', $dname_nospace);
			$dname_nospace = str_replace('"', '', $dname_nospace);
			$dname_nospace = str_replace('.', '', $dname_nospace);
			$dname_nospace = str_replace(',', '', $dname_nospace);
			$dname_nospace = str_replace('&', '', $dname_nospace);
			return '
			<div>
				<form id="contact_form_'.$dname_nospace.'" method="post" action="">
					<div>Enter description of how you contacted the customer</div>
					<div>
						<div style="margin:20px;">CONTACT METHOD::</div>
							<div style="width:150px;float:left;text-align:left;padding-top:25px;font:14px verdana;margin-left:255px;">
								<input type="radio" name="contact_method" value="phone" />PHONE<br />
								<input type="radio" name="contact_method" value="email" />EMAIL<br />
								<input type="radio" name="contact_method" value="face" />FACE TO FACE<br />
								<input type="radio" name="contact_method" value="other" />OTHER<br />
							</div>
						<textarea cols="410" rows="20" id="'.$dname_nospace.'_contact_text" name="contact_comment" style="width:225px;height:100px;float:left;margin:20px 10px 0px 20px 25px;background-color:#f1f5ee;">any comments here</textarea>
						<input type="hidden" name="company_name" value="'.$data_name.'" />
						<input type="hidden" name="leadid" value="'.$i_lead_id.'" />
						<input type="hidden" name="progress" value="contacted" />
						<div style="margin-left:10px;border:1px solid #717074;padding:10px;cursor:pointer;float:left;" class="contact_level_submit" contact_form="#contact_form_'.$dname_nospace.'" >Submit</div>
						<br style="clear:left;">
				</div>
				</div></form>
			';
		}else if($level === 'demo_account'){
			return '
			<div style="font:12px Verdana;color:#717074;margin:20px 10px 50px;">
			<span>Setup a Demo account for this prospect: </span><img src="./images/btn_createdemo.png" alt="btn_c_d" class="demo_account_requested" style="cursor:pointer;"/></div>';
		}else{
			$a_applicable_special_licenses = $o_current_user->get_applicable_special_licenses($data_name);
			$s_special_licenses = (count($a_applicable_special_licenses) == 0 ? '' : '<input type="hidden" name="available_special_licenses" value="'.implode(',', $a_applicable_special_licenses).'" />');
			return '<div style="font:12px Verdana;color:#717074;margin:40px">
			<span>Select License to Apply:</span>
				<img src="./images/i_gold_tiny.png" alt="img_g_t"/>
			<span style="color:#aecd37;cursor:pointer;" class="apply_license_btn" val="'.$data_name.'">GOLD</span>
				<img src="./images/i_platinum_tiny.png" alt="img_p_t"/>
			<span style="color:#aecd37;cursor:pointer;" class="apply_license_btn" val="'.$data_name.'">PLATINUM</span>
				<input type="hidden" name="available_licenses" id="available_licenses" value="'.$s_licenses_string.'" />
				'.$s_special_licenses.'
			</div>
			';
		}
	}//lead_type_content
}//LeadsTabClass