<?php

global $o_quotesTab;
$o_quotesTab = new QuotesTabClass();

//LP-4505,added for payment options.
global $payment_option_arr;

$o_quotesTab->getCategories();
$o_quotesTab->getCategoryProducts();

require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

class QuotesTabClass {

	public $categories;
	public $categoryJson;
	public $categoryProducts;
	public $categoryProductJson;
	public $products;
	public $productsJson;

	function __construct() {}

	function getCategoryProducts(){
		$result=mlavu_query("select * from reseller_products");
                while($rec=mysqli_fetch_assoc($result)){
                  $this->categoryProducts[$rec['category_id']][]=array("id"=>$rec['reseller_product_id'],"name"=>$rec['name'],"info"=>$rec['info'],"categoryId"=>$rec['category_id']);
		  $this->categoryProducts[0][]=array("id"=>$rec['reseller_product_id'],"name"=>$rec['name'],"info"=>$rec['info'],"categoryId"=>$rec['category_id']);
		}
		$this->categoryProductJson=json_encode($this->categoryProducts);	
	}

	function getCategories(){
                $result=mlavu_query("select * from reseller_product_category");
		$this->categories[0]=array("id"=>0,"name"=>"All");
                while($rec=mysqli_fetch_assoc($result)){
		  $this->categories[$rec['reseller_product_category_id']]=array("id"=>$rec['reseller_product_category_id'],"name"=>$rec['name']);		  
		}
		$this->categoryJson=json_encode($this->categories);
	}


	function getHtmlForTab() {
		return <<<HTML
			<li class="tab li_quotes_tab" style="width:110px;"><a href="#quotes_tab">QUOTES</a></li>
HTML;
	}

	function getHtmlForTabContent($distro_user) {
		$distro_code = $distro_user->get_info_attr('username');

		$lavu_distro_codes = array('lavusales505', 'bryansales', 'kalynnsales', 'joshsales', 'tanyasales', 'nate', 'tomlavu', 'j.wilcox');
		$is_internal_distro = in_array($distro_code, $lavu_distro_codes);
		$sflink_header_css = ($is_internal_distro) ? '' : 'display:none;';

		$content .= <<<HTML
			<div id="quotes_tab" class="tab_section tab00">
				<div class="tab_top" style="height:70px;">
					<div class="tab_top_accnts">
						<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">QUOTES & ORDERS</div>
						<div id="unass_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;padding-top:25px;">
							<img src="./images/btn_create_quote.png" alt="btn_apply" class="new_quote_btn" style="cursor:pointer;" />
						</div>
					</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>

				<div class="adjust_body">
					<div id="quote_desc">
						<span class="hdr" style="text-align:left; width:50px;">&nbsp;</span>
						<span class="hdr" style="text-align:left; width:140px;">QUOTE NAME</span>
						<span class="hdr" style="text-align:left; width:170px;">COMPANY</span>
						<span class="hdr" style="text-align:left; width:130px;">PRICE</span>
						<span class="hdr" style="text-align:left; width:170px;">COST</span>
						<span class="hdr" style="text-align:left; width:50px;">CLONE</span>
						<span class="hdr" style="text-align:left; width:40px;">PDF</span>
						<span class="hdr" style="text-align:left; width:50px;">REMOVE</span>
						<span class="hdr" style="text-align:left; width:50px;{$sflink_header_css}">LINK</span>
						<br style="clear:left;" />
					</div>
			<div id="quote_acc_wrapper">
HTML;

        //beginning of accordian
		$quotes = $distro_user->get_quotes();
		$payment_option_arr=array('1'=>'Financing','2'=>'Customer Credit Card','3'=>'My Credit Card');
		foreach ($quotes as $reseller_quote_id => $reseller_quote) {
			// Circle color
			$circle_color = 'orange';
			$status = strtolower($reseller_quote['status']);
			if ($status == 'approved') $circle_color = 'green';
			if ($status == 'paid') $circle_color = 'blue';
			if ($status == 'rejected') $circle_color = 'red';
			if ($reseller_quote['_finished'] == '1') $circle_color = 'black';
			
			

			// PDF icon
			$select_account_css = (!empty($reseller_quote['pdf_file']) && empty($reseller_quote['dataname'])) ? '' : '';
			$pdf_icon_display_css = empty($reseller_quote['pdf_file']) ? 'display:none' : '';
			$pdf_icon_onclick = empty($reseller_quote['pdf_file']) ? 'onclick="return false"' : '';
			$pdf_icon_class_css = empty($reseller_quote['pdf_file'])? '' : 'noclick';
			$sflink_icon_css = ($is_internal_distro && !empty($reseller_quote['salesforce_quote_id'])) ? '' : 'display:none';
			$payment_link_text = empty($reseller_quote['payment_link']) ? '' : 'Pay Quote (Auto-Saves CC for Monthly Hosting)';
			$payment_link_separator = empty($payment_link_text) ? '' : '<br>';
			
			 $payment_options='';
	         if(!empty($reseller_quote['payment_options'])){
	              $pay_opt=explode(',',$reseller_quote['payment_options']);
	    
                   foreach($pay_opt as $optNum){
                        $payment_options.=$payment_option_arr[$optNum];
                         $payment_options.="<br>";
                     }
	             }
	             
			  $Payment_link=($reseller_quote['pay_link_option'] == 1) ? 'Yes' : 'No';

   	        $content .= <<<HTML
				<div class="acc_head accordionButton" id="quote_row_{$reseller_quote_id}">
					<img class="acc_arrow get_qli" src="./images/arrow_left.png" dc="{$distro_code}" q_id="{$reseller_quote['id']}" style="float:left;" alt=""/>
					<span style="width:23px;"><img id="quote_row_circ_{$reseller_quote_id}" class="acc_status" src="./images/circ_{$circle_color}.png"></span>
					<input type="hidden" name="reseller_quote_id" value="{$reseller_quote['id']}" />
					<span style="width:130px;">{$reseller_quote['quote_name']}</span><div class="loc_head_div">|</div>
					<span style="width:160px;">{$reseller_quote['company']}</span><div class="loc_head_div">|</div>
					<span style="width:120px;">{$reseller_quote['total_price']}</span><div class="loc_head_div">|</div>
					<span style="width:160px;">{$reseller_quote['total_cost']}</span><div class="loc_head_div">|</div>
					<span style="width:40px;text-align:center;cursor:pointer;" qid="{$reseller_quote['id']}" dc="{$reseller_quote['distro_code']}" class="quote_clone_quote"><img src="./images/btn_clone.png" alt="Clone" width="32" height="32" /></span><div class="loc_head_div">|</div>
					<span class="pdf_icon{$pdf_icon_class_css}" dc="{$reseller_quote['distro_code']}" q_id="{$reseller_quote['id']}" dn="{$reseller_quote['dataname']}" rid="{$reseller_quote['restaurantid']}" sid="{$reseller_quote['signupid']}" style="width:30px;"><a target="_blank" href="{$reseller_quote['pdf_file']}" {$pdf_icon_onclick}><img src="./images/pdficon_large.png" style="{$pdf_icon_display_css}" alt="i_n"/></a></span><div class="loc_head_div">|</div>
					<span style="width:40px;text-align:center;cursor:pointer;" q_id="{$reseller_quote['id']}" dn="{$reseller_quote['dataname']}" sfqi="{$reseller_quote['salesforce_quote_id']}" class="remove_quote"><img src="./images/btn_decline.png" alt="big_X" /></span><div class="loc_head_div" style="{$sflink_icon_css}">|</div>
					<span style="width:40px;text-align:center;cursor:pointer;"><a target="_blank" href="https://na42.salesforce.com/{$reseller_quote['salesforce_quote_id']}"><img src="./images/salesforce_icon.png" style="{$sflink_icon_css}" alt=""></a></span>
					<br class="clear:left;">
				</div>
				<div class="acc_quote_content accordionContent" style="margin:0; padding:0 20px;font-size:12px">
					<div id="quoteinfo_table_{$reseller_quote_id}" style="float:left; width:20%; margin-top:1%; margin-left:2%">
						<p>
							<span class="hdr">Status</span><br><span class="quoteinfo_status" id="quoteinfo_status_{$reseller_quote_id}"></span>
						</p>
						<p>
							<span class="hdr">Current POS</span><br><span class="quoteinfo_current_pos"></span><br>
							<span class="hdr">Current Processor</span><br><span class="quoteinfo_current_processor"></span><br>
							<span class="hdr">Requested Processor</span><br><span class="quoteinfo_requested_processor"></span>
						</p>
						<p>
							<span class="hdr">Notes</span><br><span class="quoteinfo_notes"></span>
						</p>
					</div>
					<div style="float:left; width:20%; margin-top:1%; margin-bottom:2%">
						<div style="float:left">
							<p class="hdr">LOCATION ADDRESS</p>
							{$reseller_quote['company']}<br>
							{$reseller_quote['firstname']} {$reseller_quote['lastname']}<br>
							{$reseller_quote['address']}<br>
							{$reseller_quote['city']}, {$reseller_quote['state']} {$reseller_quote['zip']}<br>
							{$reseller_quote['country']}</p>
							<p class="hdr">BILLING ADDRESS</p>
							{$reseller_quote['billing_firstname']} {$reseller_quote['billing_lastname']}<br>
							{$reseller_quote['billing_address']}<br>
							{$reseller_quote['billing_city']}, {$reseller_quote['billing_zip']} {$reseller_quote['billing_state']} {$reseller_quote['billing_zip']}<br>
							{$reseller_quote['billing_country']}<br>
                            <p class="hdr">Payment options</p>
                             {$payment_options} <br>
                             <p class="hdr">Payment link required</p>
                             {$Payment_link}
						</div>
					</div>
					<div style="float:left; width:40%; margin-top:1%; margin-bottom:2%">
						<p class="hdr"></p>
						<table id="lineitems_table_{$reseller_quote_id}" style="width:100%">
							<tr><td colspan="3" id="lineitems_loading_{$reseller_quote_id}" align="left">Loading...</td></tr>
						</table>
						<p class="hdr" style="{$pdf_icon_display_css}">Payment Links</p>
						1. Click the payment link you wish to use<br>
						2. Make the payment or send the website address to the party responsible for payment<br>
						<a target="_blank" id="quote_payment_link_{$reseller_quote_id}" href="{$reseller_quote['payment_link']}">{$payment_link_text}</a>{$payment_link_separator}
						<!--div class="quote_select_account" style="{$select_account_css}" id="quote_place_order_{$reseller_quote_id}" dc={$distro_code} quoteid="{$reseller_quote_id}" s="{$status}">Select Account</div-->
					</div>
					<br style="clear:left;">
				</div>
HTML;

		}
		$content .= '</div></div></div>';//quote_acc_wrapper and quote tab close baby

		return $content;
	}

	function getHtmlForNewQuoteForm($distro_user, $is_admin = '') {
		//$user->get_info_attr('username');
		//echo 'get_new_lead_form is_admin:: '.$is_admin;
		$hide_from_specialists = 'hide_from_specialists';
		if($is_admin == 'admin')
		{
			$hide_from_specialists = '';
		}

		$distro_code = $distro_user->get_info_attr('username');
		$resellerid = $distro_user->get_info_attr('id');
		$reseller_company = htmlentities($distro_user->get_info_attr('company'));
		$reseller_name = htmlentities($distro_user->get_info_attr('f_name')) .' '. htmlentities($distro_user->get_info_attr('l_name'));
		$reseller_phone = htmlentities($distro_user->get_info_attr('phone'));
		$reseller_email = htmlentities($distro_user->get_info_attr('email'));
		$distro_license_com = $distro_user->get_info_attr('distro_license_com');

		// Create HTML for Account list (ordered by company name)
		$accounts = $distro_user->get_accounts();
		usort($accounts, 'cmpForSortingByCompanyName');
		$dataname_dropdown = '';
		foreach ($accounts as $i => $restaurant ) {
			$dataname_dropdown .= '<option value="'. $restaurant['dataname'] .'">'. $restaurant['company_name'] .'</option>';
		}
	
		echo "<script>
		var categoryOpt=".$this->categoryJson.";	
		var categoryProductOpt=".$this->categoryProductJson.";</script>";	
		$new_quote_form = <<<HTML
			<div id="new_quote_form_div" style="display:none;"><!--img id="quote_loading_squares" style="float:right; margin-left:25%" src="/distro/beta/images/loading_squares.gif"-->
				<div id="success_quote_form" style="display:none;text-align:center;"></div>
				<img id="quote_loading_squares" style="display:none;margin:0 auto;" src="/distro/beta/images/loading_squares.gif">
				<form id="new_quote_form" method="post" action="">
					<input type="hidden" name="status" value="new">
					<input type="hidden" name="distro_code" id="distro_code" value="{$distro_code}">
					<input type="hidden" name="resellerid" id="resellerid" value="{$resellerid}">
					<input type="hidden" name="reseller_company" id="reseller_company" value="{$reseller_company}">
					<input type="hidden" name="reseller_name" id="reseller_name" value="{$reseller_name}">
					<input type="hidden" name="reseller_phone" id="reseller_phone" value="{$reseller_phone}">
					<input type="hidden" name="reseller_email" id="reseller_email" value="{$reseller_email}">
					<input type="hidden" name="distro_license_com" id="distro_license_com" value="{$distro_license_com}">
					<input type="hidden" name="dataname" id="dataname">
					<input type="hidden" name="restaurantid" id="restaurantid">
					<input type="hidden" name="signupid" id="signupid">
					<input type="hidden" name="reseller_lead_id" id="reseller_lead_id">
					<input type="hidden" name="salesforce_id" id="salesforce_id">
					<input type="hidden" name="salesforce_opportunity_id" id="salesforce_opportunity_id">
					<div style="float:right">* required field</div>
					<div id="new_quote_form_title" style="font:18px Verdana;color:#717074;text-align:left;">Create New Quote</div>
					<div>
						<fieldset name="account_info" style="width:auto">
							<legend>1. Choose Account</legend>
							<input type="radio" name="associated_account" id="associated_account_existing" value="existing" checked><label for="associated_account_existing">Use an existing account</label>
							<input type="text" id="quote_accountlead_searchInput" placeholder="Start typing to search..." name="accountlead_Search" style="width:200px;" autofocus>
							Search in:
							<input type="checkbox" name="search_accounts" id="search_accounts" value="1" checked><label for="search_accounts">Accounts</label>
							<input type="checkbox" name="search_leads" id="search_leads" value="1"checked><label for="search_leads">Leads</label>
							<input type="checkbox" name="search_quotes" id="search_quotes" value="1"checked><label for="search_quotes">Quotes</label><br>
							<input type="radio" name="associated_account" id="associated_account_new" style="height:18px" value="new"><label for="associated_account_new">Create a new account</label><br>
							<input type="radio" name="associated_account" id="associated_account_reseller" style="height:18px" value="reseller"><label for="associated_account_reseller">This quote is for my inventory</label>
						</fieldset>
						<br clear="left">
						<fieldset name="quote_info" style="width:auto">
							<legend>2. Quote Info</legend>
							<input type="text" name="quote_name" id="quote_name" placeholder="* Quote Name" style="width:200px" required /><br>
							<input type="text" name="current_pos" id="current_pos" placeholder="Current POS" /><br>
							<input type="text" name="current_processor" id="current_processor" placeholder="Current Processor"><br>
							<div class="select_arrow_bg_no_float {$hide_from_specialists}" style="float:none;">
								<select name="requested_processor" id="requested_processor">
									<option value="">Processor Requested</option>
									<option value="Keep Current">Keep Current</option>
									<option value="Heartland">Heartland</option>
									<option value="Mercury">Mercury</option>
									<option value="Moneris">Moneris</option>
									<option value="Evosnap">Evosnap</option>
									<option value="Bridgepay">Bridgepay</option>
								</select><br>

							<textarea name="notes" id="notes" placeholder="Notes to Lavu Sales" maxlength="255" style="width:200px;height:4em;"></textarea>
							<textarea name="customer_notes" id="customer_notes" placeholder="Notes to Customer" maxlength="5000" style="width:200px;height:2em;"></textarea>
							</div>
						</fieldset>
						<fieldset name="shipping_address" style="float:left; width:auto; margin-left:20px;">
							<legend for="shipping_address">3. Shipping Address</legend>
							<input type="text" name="company" id="company" placeholder="* Company Name" required><br />

							<input type="text" name="firstname" id="firstname" size="20" style="width:69px" placeholder="* First Name" required>
							<input type="text" name="lastname" id="lastname" size="30" style="width:70px" placeholder="* Last Name" required><br>

							<input type="text" name="address" id="address" placeholder="* Address" required><br>

							<input type="text" name="city" id="city" placeholder="* City" required><br>

							<div class="select_arrow_bg_new_account">
								<select name="state" id="state" required>
									<option value="">* State / Province</option>
									<option value="AL">Alabama</option>
									<option value="AK">Alaska</option>
									<option value="AS">American Samoa</option>
									<option value="AZ">Arizona</option>
									<option value="AR">Arkansas</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DE">Delaware</option>
									<option value="DC">District of Columbia</option>
									<option value="FM">Federated States of Micronesia</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="GU">Guam</option>
									<option value="HI">Hawaii</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="IA">Iowa</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="ME">Maine</option>
									<option value="MH">Marshall Islands</option>
									<option value="MD">Maryland</option>
									<option value="MA">Massachusetts</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MS">Mississippi</option>
									<option value="MO">Missouri</option>
									<option value="MT">Montana</option>
									<option value="NE">Nebraska</option>
									<option value="NV">Nevada</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NY">New York</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="MP">Northern Mariana Islands</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PW">Palau</option>
									<option value="PA">Pennsylvania</option>
									<option value="PR">Puerto Rico</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VT">Vermont</option>
									<option value="VI">Virgin Islands</option>
									<option value="VA">Virginia</option>
									<option value="WA">Washington</option>
									<option value="WV">West Virginia</option>
									<option value="WI">Wisconsin</option>
									<option value="WY">Wyoming</option>
									<option disabled>--</option>
									<option value="AB">Alberta</option>
									<option value="BC">British Columbia</option>
									<option value="MB">Manitoba</option>
									<option value="NB">New Brunswick</option>
									<option value="NL">Newfoundland and Labrador</option>
									<option value="NS">Nova Scotia</option>
									<option value="NT">Northwest Territories</option>
									<option value="NU">Nunavut</option>
									<option value="ON">Ontario</option>
									<option value="PE">Prince Edward Island</option>
									<option value="QC">Quebec</option>
									<option value="SK">Saskatchewan</option>
									<option value="YT">Yukon</option>
								</select>
							</div><br>

							<input type="text" name="zip" id="zip" placeholder="* Zip" required><br>

							<select name="country" id="country" required>
								<option value="">* Country</option>
								<option value="US">US</option>
								<option value="CA">CA</option>
							</select><br>

							<input type="text" name="phone" id="phone" placeholder="* Phone" required>
							<!--input type="text" name="phone1" id="phone1" style="width:30px" />-
							<input type="text" name="phone2" id="phone1" style="width:30px"/>-
							<input type="text" name="phone3" id="phone2" style="width:50px"/--><br>

							<input type="text" name="email" id="email" placeholder="* Email" required><br>
						</fieldset>
						<fieldset name="billing_address" style="float:left; width:auto; margin-left:20px;">
							<legend for="billing_address">4. Billing Address</legend>
							<input type="checkbox" name="same_billing_address" id="same_billing_address" value="1" onclick="setBillingAddress(this.checked)">
							<label for="same_billing_address">Use Shipping Address</label><br>

							<input type="text" name="billing_firstname" id="billing_firstname" size="20" style="width:69px" placeholder="* First Name" required>
							<input type="text" name="billing_lastname" id="billing_lastname" size="30" style="width:70px" placeholder="* Last Name" required><br>

							<input type="text" name="billing_address" id="billing_address" placeholder="* Address" required><br>

							<input type="text" name="billing_city" id="billing_city" placeholder="* City" required><br>

							<div class="select_arrow_bg_new_account">
								<select name="billing_state" id="billing_state" required>
									<option value="">* State / Province</option>
									<option value="AL">Alabama</option>
									<option value="AK">Alaska</option>
									<option value="AS">American Samoa</option>
									<option value="AZ">Arizona</option>
									<option value="AR">Arkansas</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DE">Delaware</option>
									<option value="DC">District of Columbia</option>
									<option value="FM">Federated States of Micronesia</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="GU">Guam</option>
									<option value="HI">Hawaii</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="IA">Iowa</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="ME">Maine</option>
									<option value="MH">Marshall Islands</option>
									<option value="MD">Maryland</option>
									<option value="MA">Massachusetts</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MS">Mississippi</option>
									<option value="MO">Missouri</option>
									<option value="MT">Montana</option>
									<option value="NE">Nebraska</option>
									<option value="NV">Nevada</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NY">New York</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="MP">Northern Mariana Islands</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PW">Palau</option>
									<option value="PA">Pennsylvania</option>
									<option value="PR">Puerto Rico</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VT">Vermont</option>
									<option value="VI">Virgin Islands</option>
									<option value="VA">Virginia</option>
									<option value="WA">Washington</option>
									<option value="WV">West Virginia</option>
									<option value="WI">Wisconsin</option>
									<option value="WY">Wyoming</option>
									<option disabled>--</option>
									<option value="AB">Alberta</option>
									<option value="BC">British Columbia</option>
									<option value="MB">Manitoba</option>
									<option value="NB">New Brunswick</option>
									<option value="NL">Newfoundland and Labrador</option>
									<option value="NS">Nova Scotia</option>
									<option value="NT">Northwest Territories</option>
									<option value="NU">Nunavut</option>
									<option value="ON">Ontario</option>
									<option value="PE">Prince Edward Island</option>
									<option value="QC">Quebec</option>
									<option value="SK">Saskatchewan</option>
									<option value="YT">Yukon</option>
								</select>
							</div><br>

							<input type="text" name="billing_zip" id="billing_zip" placeholder="* Zip" /><br />

							<select name="billing_country" id="billing_country" required>
								<option value="">* Country</option>
								<option value="US">US</option>
								<option value="CA">CA</option>
							</select>

						</fieldset>

                        <!--LP-4505 -->
                        <fieldset name="pay_options" style="float:left; width:auto; margin-left:20px;">
                        <legend for="pay_options">5.How will this quote be paid? * </legend>
                         <input type="checkbox" name="payment_option_1" id="payment_option_1" value="1">
                        <label for="payment_option_1">Financing (Payments option shown on quote)</label><br>
                        <input type="checkbox" name="payment_option_2" id="payment_option_2" value="2">
                        <label for="payment_option_2">Customer Credit Card</label><br>
                        <input type="checkbox" name="payment_option_3" id="payment_option_3" value="3">
                        <label for="payment_option_3">My Credit Card(cannot be used for hosting)</label><br>
                        </fieldset>
                        <fieldset name="pay_link_option" style="float:left; width:auto; margin-left:20px;">
                        <legend for="pay_link_option">6. Payment link required?</legend>
                        <input type="radio" name="pay_link_option" id="payment_link_options_1" value="1">
                        <label for="payment_link_options_1">Yes</label>
                        <input type="radio" name="pay_link_option" id="payment_link_options_2"  value="0" checked>
                        <label for="payment_link_options_2">No</label>
                        </fieldset>
                         <!-- End of LP-4505 -->

                    <br clear="both">
					</div>
					<div id="quote_line_item">
						<table id="quote_line_items_table" border="0" style="width:100%; padding:0; border-spacing:1px;">
						<tr>
							<td>+/-</td>
							<td bgcolor="#d2d4d8"><label for="category">Category</label></td>
							<td bgcolor="#d2d4d8"><label for="product">Product</label></td>
							<td bgcolor="#d2d4d8"><label for="qty">Qty</label></td>
							<td bgcolor="#d2d4d8"  style="text-align:right"><label for="unit_cost">Quote Price</label></td>
							<td bgcolor="#d2d4d8" style="text-align:right"><label for="your_cost">Your Cost</label></td>
							<td bgcolor="#d2d4d8" style="text-align:right"><label for="msrp">MSRP</label></td>
							<td bgcolor="#d2d4d8" style="text-align:right"><label for="margin_pc">Margin %</label></td>
							<td bgcolor="#d2d4d8" style="text-align:right"><label for="margin_amount">Margin $</label></td>
							<td bgcolor="#d2d4d8">&nbsp;</td>
						</tr>
						<tr>
							<td><input type="button" value="+" onclick="addNewQuoteLineItem()"></td>
							<td><select name="category_0" id="category_0" style="width:100px; appearance:normal;" onchange="renderProductList(0)" required>
							<td>
							<input id="product_0" style="width:300px" name="product_0" onchange="clearUnitPrice('');" required>
							</td>
							<!--<td><select name="product" id="product" style="width:300px; appearance:normal;" onchange="clearUnitPrice(''); checkLineNumForQuoteTotal('')" required>-->
							//Reseller Product
								</select></td>
								<td><input type="text" name="qty_0" id="qty_0" maxlength="3" style="width:20px" value="1" onchange="checkLineNumForQuoteTotal(0)" required></td>
								<td style="text-align:right"><input type="text" name="unit_price_0" id="unit_price_0" maxlength="8" style="width:50px; text-align:right" onchange="checkLineNumForQuoteTotal(0)" required></td>
								<td style="text-align:right"><span id="your_cost_0">0.00</span></td>
								<td style="text-align:right"><span id="msrp_0">0.00</span></td>
								<td style="text-align:right"><span id="margin_pc_0"></span></td>
								<td style="text-align:right"><span id="margin_amount_0">0.00</span></td>
								<td style="text-align:right"><input type="checkbox" name="from_inventory_0" id="from_inventory_0" value="1" onclick="return checkLineNumForQuoteTotal(0)"><label id="from_inventory_label_0" for="from_inventory_0">from Inventory</label></td>
						</tr>
						<tr id="quote_lineitems_totals">
							<td><input type="button" value="+" onclick="addNewQuoteLineItem()"></td>
							<td>&nbsp;</td>
							<td align="right"><strong>Total</strong></td>
							<td></td>
							<td id="quote_total" style="text-align:right">0.00</td>
							<td id="reseller_cost_total" style="text-align:right">0.00</td>
							<td></td>
							<td id="margin_pc_total" style="text-align:right">0.00</td>
							<td id="margin_amount_total" style="text-align:right">0.00</td>
							<td></td>
						</tr>
						</table>
						<input type="hidden" name="line_num" id="line_num" value="0">
					</div></tr>
					<div id="new_quote_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_new_quote">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;width:160px" id="submit_new_quote" value="Submit" />
						<input type="hidden" id="product_info_0" name="product_info_0">
					</div>
				</form>
				<div id="ok_quote_buttons" style="text-align:center; margin-top:20px;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_new_quote">OK</span>
					</div>
			</div>
			<div id="quote_select_account_form_div" style="display:none;">
				<div id="success_quote_select_account_form" style="display:none;text-align:center;"></div>
				<form id="quote_select_account_form" method="post" action="">
					<input type="hidden" id="quote_select_account_distro_code" name="distro_code" value="{$distro_code}">
					<div id="quote_select_account_form_title" style="font:24px Verdana;color:#717074;text-align:left;">Select Demo Account</div>
					<div id="new_quote_form_required" style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Quotes need to be associated with a Demo Account before Payment Links will be generated</div>
					<div style="margin:20px auto">
						<input type="text" id="quote_select_account_searchInput" placeholder="Search for existing Demo Account..." name="quote_select_account_Search" style="height:30px;width:230px;" />
						<br>
						<div id="quote_select_account_company"></div>
					</div>
					<div id="quote_select_account_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_quote_select_account">Cancel</span>
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;cursor:pointer;width:160px" id="ok_quote_select_account">OK</span>
					</div>
				</form>
			</div>
<script>
var new_quote_form = document.getElementById("new_quote_form");
var line_num = document.getElementById("line_num");
var distro_license_com = document.getElementById("distro_license_com");
var product = document.getElementById("product");
var quote_line_items_table = document.getElementById("quote_line_items_table");
var quote_lineitems_totals = document.getElementById("quote_lineitems_totals");

        var categoryField = document.getElementById("category_0");
        // Clone existing product drop-down options 
        for (var key in categoryOpt){
                var newOption = document.createElement("option");
                newOption.text = categoryOpt[key].name;
                newOption.value = categoryOpt[key].id;
                categoryField.add(newOption);
        }
renderProductList(0);
var lineItems=[];
function renderProductList(index){
 $( function() {
	var prodList=[]
	var categoryObj=document.getElementById('category_'+index);
 	var categoryId=categoryObj.options[categoryObj.selectedIndex].value;
 	var products=categoryProductOpt[categoryId];

	//empty product field
	document.getElementById('product_'+index).value='';

 	for(var key in products){
		var item={};
		item["value"]=products[key].name;
		item["label"]=products[key].name;
		item["id"]=products[key].info;
		item["categoryId"]=products[key].categoryId;
		prodList.push(item);
 	}
    $( "#product_"+index ).autocomplete({
      source: prodList
    });
    $( "#product_"+index ).on( "autocompleteselect", 
	function(event,ui) {
		var item={};
		item["prodName"]=ui.item.value;
		item["prodInfo"]=ui.item.id;
		if(categoryId==0){
			categoryId=ui.item.categoryId;
		}
		item["categoryid"]=categoryId;
		lineItems[index]=[];
		lineItems[index].push(item);
		checkLineNumForQuoteTotal(index);
	} );
  } );
}

function createCategoryDOM(control_num){
        var categorySelectObj = document.createElement("select");
        categorySelectObj.id = "category_" + control_num;
        categorySelectObj.name = "category_" + control_num;
        categorySelectObj.required = true;
        categorySelectObj.style.width = "100px";
        categorySelectObj.onchange = function () { renderProductList(control_num) };
        // Clone existing product drop-down options
        for (var key in categoryOpt){
                var newOption = document.createElement("option");
                newOption.text = categoryOpt[key].name;
                newOption.value = categoryOpt[key].id;
                categorySelectObj.add(newOption);
        }
	return categorySelectObj;
}

function addNewQuoteLineItem() {
	// Get next control_num and increment line_num
	var control_num = parseInt(line_num.value) + 1; 
	line_num.value = control_num;

	// Create new div for all new elements to go in
	var newRow = document.createElement("tr");
	newRow.id = "quote_line_item"+ control_num;

	var newRemoveButtonTd = document.createElement("td");
	var newRemoveButton = document.createElement("input")
	newRemoveButton.type = "button";
	newRemoveButton.id = "remove" + control_num;
	newRemoveButton.name = "remove" + control_num;
	newRemoveButton.value = "X";
	newRemoveButton.onclick = function () {  removeQuoteLineItem(control_num); updateQuoteTotal(); };
	newRemoveButtonTd.appendChild(newRemoveButton);
	//newDiv.appendChild(document.createTextNode(' '));
	newRow.appendChild(newRemoveButtonTd);

	var categoryTdObj = document.createElement("td");
        categoryTdObj.appendChild(createCategoryDOM(control_num));
        newRow.appendChild(categoryTdObj);

	var newDivObj=document.createElement('div');
	var newInputObj=document.createElement('input');
	newInputObj.id="product_"+control_num;
	newInputObj.name="product_"+control_num;
	newInputObj.style.width="300px";
	newDivObj.appendChild(newInputObj);

	//to capture product info in hidden field
	var newInputObj=document.createElement('input');
        newInputObj.id="product_info_"+control_num;
        newInputObj.name="product_info_"+control_num;
        newInputObj.type="hidden";
        newDivObj.appendChild(newInputObj);
	newRow.appendChild(newDivObj);


	var newQtyEditTd = document.createElement("td");
	var newQtyEdit = document.createElement("input");
	newQtyEdit.type = "text";
	newQtyEdit.id = "qty_" + control_num;
	newQtyEdit.name = "qty_" + control_num;
	newQtyEdit.maxlength = "3";
	newQtyEdit.value = "1";
	newQtyEdit.required = true;
	newQtyEdit.style.width = "20px";
	newQtyEdit.onchange = function () {  checkLineNumForQuoteTotal(control_num) };
	newQtyEditTd.appendChild(newQtyEdit);
	newRow.appendChild(newQtyEditTd);

	var newUnitPriceEditTd = document.createElement("td");
	newUnitPriceEditTd.style.textAlign = 'right';

	var newUnitPriceEdit = document.createElement("input");
	newUnitPriceEdit.type = "text";
	newUnitPriceEdit.id = "unit_price_" + control_num;
	newUnitPriceEdit.name = "unit_price_" + control_num;
	newUnitPriceEdit.maxlength = "6";
	newUnitPriceEdit.required = true;
	newUnitPriceEdit.style.width = "50px";
	newUnitPriceEdit.style.textAlign = "right";
	newUnitPriceEdit.onchange = function () {  checkLineNumForQuoteTotal(control_num) };
	newUnitPriceEditTd.appendChild(newUnitPriceEdit);
	newRow.appendChild(newUnitPriceEditTd);

	var newYourCostSpanTd = document.createElement("td");
	newYourCostSpanTd.style.textAlign = 'right';

	var newYourCostSpan = document.createElement("span");
	newYourCostSpan.type = "text";
	newYourCostSpan.id = "your_cost_" + control_num;
	newYourCostSpan.innerHTML = "0.00 ";
	newYourCostSpanTd.appendChild(newYourCostSpan);
	newRow.appendChild(newYourCostSpanTd);

       var newMsrpSpanTd = document.createElement("td");
        newMsrpSpanTd.style.textAlign = 'right';

        var newMsrpSpan = document.createElement("span");
        newMsrpSpan.type = "text";
        newMsrpSpan.id = "msrp_" + control_num;
        newMsrpSpan.innerHTML = "0.00 ";
        newMsrpSpanTd.appendChild(newMsrpSpan);
        newRow.appendChild(newMsrpSpanTd);

        var newMarginPcSpanTd = document.createElement("td");
        newMarginPcSpanTd.style.textAlign = 'right';

        var newMarginPcSpan = document.createElement("span");
        newMarginPcSpan.type = "text";
        newMarginPcSpan.id = "margin_pc_" + control_num;
        newMarginPcSpanTd.appendChild(newMarginPcSpan);
        newRow.appendChild(newMarginPcSpanTd);


        var newMarginAmtSpanTd = document.createElement("td");
        newMarginAmtSpanTd.style.textAlign = 'right';

        var newMarginAmtSpan = document.createElement("span");
        newMarginAmtSpan.type = "text";
        newMarginAmtSpan.id = "margin_amount_" + control_num;
        newMarginAmtSpan.innerHTML = "0.00 ";
        newMarginAmtSpanTd.appendChild(newMarginAmtSpan);
        newRow.appendChild(newMarginAmtSpanTd);


	var newInventoryCheckboxTd = document.createElement("td");
	newInventoryCheckboxTd.style.textAlign = 'right';

	var newInventoryCheckbox = document.createElement("input");
	newInventoryCheckbox.type = "checkbox";
	newInventoryCheckbox.id = "from_inventory_" + control_num;
	newInventoryCheckbox.name = "from_inventory_" + control_num;
	newInventoryCheckbox.value = "1";
	newInventoryCheckbox.required = false;
	newInventoryCheckbox.style.width = "auto";
	newInventoryCheckbox.onchange = function () {  checkLineNumForQuoteTotal(control_num) };
	newInventoryCheckboxTd.appendChild(newInventoryCheckbox);

	var newInventoryCheckboxLabel = document.createElement("label");
	newInventoryCheckboxLabel.innerHTML = "from Inventory";
	newInventoryCheckboxLabel.htmlFor = "from_inventory_" + control_num;
	newInventoryCheckboxLabel.id = "from_inventory_label_" + control_num;
	newInventoryCheckboxTd.appendChild(newInventoryCheckboxLabel);
	newRow.appendChild(newInventoryCheckboxTd);

	// Place new div after line_num element
	quote_lineitems_totals.parentNode.insertBefore(newRow, quote_lineitems_totals);

	renderProductList(control_num);

	// Increment the counter
	line_num.value = control_num;
}

var controlsToChnageOnRemovedLineItem = {product:"selectedIndex", qty:"value", unit_price:"value", your_cost:"innerHTML", from_inventory:"checked", from_inventory:"checked"};

function removeQuoteLineItem(line_num_to_delete) {
	var last_line_num = parseInt(line_num.value);

	// Shift all subsequent rows down one
	for (var control_num = parseInt(line_num_to_delete); control_num <= last_line_num; control_num++) {
		// If we're processing the last row then we just remove it instead of cloning anything
		if (control_num == last_line_num) {
			// Remove the last row
			var row = document.getElementById("quote_line_item" + control_num);
			row.parentNode.removeChild(row);

			// Decrement the line_num counter since we deleted a row
			line_num.value -= 1;
		}
		// Clone next row onto current one
		else {
			var next_control_num = parseInt(control_num + 1);
			var nextRow = document.createElement("quote_line_item"+ next_control_num);

			if (nextRow != null) {
				for (var fieldName in controlsToChnageOnRemovedLineItem) {
					if (controlsToChnageOnRemovedLineItem.hasOwnProperty(fieldName)) {
						var propertyName = controlsToChnageOnRemovedLineItem[fieldName];
						var nextProductDropdown = document.getElementById(fieldName + next_control_num);
						var currProductDropdown = document.getElementById(fieldName + control_num);
						currProductDropdown[propertyName] = nextProductDropdown[propertyName];
					}
				}
			}
		}
	}
}

var forInventory = document.getElementById("associated_account_reseller");
var success_quote_form = document.getElementById('success_quote_form');

function checkLineNumForQuoteTotal(lineNum) {

	var product=lineItems[lineNum][0].prodName;

	var prodInfo=lineItems[lineNum][0].prodInfo;
	var category=categoryOpt[lineItems[lineNum][0].categoryid].name;

	document.getElementById('product_info_'+lineNum).value=category+'^'+product+prodInfo;

	var qtyEdit = document.getElementById("qty_" + lineNum);
	var unitPriceEdit = document.getElementById("unit_price_" + lineNum);
	var msrpObj = document.getElementById("msrp_" + lineNum);
	var yourCost = document.getElementById("your_cost_" + lineNum);
	var fromInventory = document.getElementById("from_inventory_" + lineNum);
	var fromInventoryLabel = document.getElementById("from_inventory_label_" + lineNum);
	var distroPoints = 0;


	var maxPrice = "";
	var resellerPrice = "";
	var pricebookEntryId = "";

	var needToUpdateTotal = false;

	if (product) {
		success_quote_form.innerHTML = '';

		var productVals = prodInfo.split('^', 5);
		pricebookEntryId = productVals[1];

		maxPrice = parseFloat(productVals[3]);
                maxPrice = isNaN(maxPrice)?0:maxPrice;
		resellerPrice = parseFloat(productVals[2]);
                resellerPrice = isNaN(resellerPrice)?0:resellerPrice;

		var isBillingLineItem = (category == 'SaaS');
		var isLicenseLineItem = (isBillingLineItem && (product.indexOf("License") > -1 || product.indexOf("Lavu 88") > -1 ));
		var isHostingLineItem = (isBillingLineItem && (product.indexOf("Lavu -") > -1));
		var isPriceCappedLineItem = (maxPrice && maxPrice > 0);
		var isPriceFixedLineItem = !isPriceCappedLineItem && (isBillingLineItem || category == 'iPads' || category == 'Apple Mac Mini' || (category == 'Networking' && product.indexOf("Apple") > -1) || category == 'Lavu Installation Services' || category == 'Lavu Site Survey Services' || category == 'Lavu Menu Setup Services' || category == 'Extended Care' || category == 'Extended Care 2 Year' || category == 'SITA (Spare in the Air Warranty)');

		msrpObj.innerHTML=maxPrice;

		// Remove characters in the price that will break things
		if (unitPriceEdit.value) unitPriceEdit.value = unitPriceEdit.value.replace(/[,$]/g, '');

		if (isPriceCappedLineItem) {
			if ( (unitPriceEdit.value != "") && (parseFloat(unitPriceEdit.value) > maxPrice) ) {
				success_quote_form.innerHTML = "Item " + lineNum +" price ("+ unitPriceEdit.value +") has exceed max price ("+ maxPrice +")";
				success_quote_form.style.color = 'red';
				success_quote_form.style.display = 'block';
				unitPriceEdit.focus();
				return false;
			}
			else if (unitPriceEdit.value == '') {
				unitPriceEdit.value == maxPrice;
				success_quote_form.style.color = 'black';
				success_quote_form.style.display = '';
			}
		}

		// Don't show the "from Inventory" checkbox for Billing or Services products.
		if (category == 'SaaS' || category.indexOf('Services') > -1) {
			fromInventory.checked = false;
			fromInventory.style.visibility = 'hidden';
			fromInventoryLabel.style.visibility = 'hidden';
		}
		else {
			fromInventory.style.visibility = 'visible';
			fromInventoryLabel.style.visibility = 'visible';
		}


		// Discount the price of a License based on distro commission
		if (isLicenseLineItem && distro_license_com.value != "" && !fromInventory.checked) {
			resellerPrice = (resellerPrice - ( ( parseInt( distro_license_com.value, 10) / 100 ) * resellerPrice ) ).toFixed(2);
		}

		if (forInventory.checked) {
			yourCost.innerHTML = resellerPrice;
		}
		else if (fromInventory.checked && !isHostingLineItem) {
			yourCost.innerHTML = "0.00";
			///unitPriceEdit.required = false;
		}
		else {
			yourCost.innerHTML = resellerPrice;
			unitPriceEdit.required = true;
		}
		needToUpdateTotal = true;

		// Don't allow the price to be changed for price-fixed products
		if (forInventory.checked) {
			unitPriceEdit.value = resellerPrice;
			unitPriceEdit.readOnly = true;
		}
		else if ( isBillingLineItem ) {
			if ( isLicenseLineItem ) {
				var licensePrice = resellerPrice;
				if ( product.indexOf("Lavu Enterprise") > -1 ) {
					licensePrice = '2495.00';
				}
				else if ( product.indexOf("Lavu Gold") > -1 ) {
					licensePrice = '1495.00';
				}
				else if ( product.indexOf("Lavu 88") > -1 ) {
					licensePrice = '0.00';
				}
				unitPriceEdit.value = licensePrice;
			}
			else {
				unitPriceEdit.value = resellerPrice;
			}
			unitPriceEdit.readOnly = true;
		}
		else if ( isPriceFixedLineItem ) {
			unitPriceEdit.value = resellerPrice;
			unitPriceEdit.readOnly = true;
		}
		else {
			unitPriceEdit.readOnly = false;
			unitPriceEdit.focus();
		}
	}

      //calculate margin
      var marginPc=0;
      var marginAmt=0;
      var qty = parseInt(qtyEdit.value);
      var unitPrice=parseFloat(unitPriceEdit.value);
      unitPrice = isNaN(unitPrice)?0:(qty * unitPrice);
      var yourCost=parseFloat(yourCost.innerHTML);
      yourCost = isNaN(yourCost)?0:(qty * yourCost);
      marginAmt = unitPrice - yourCost;
      marginAmt=(isNaN(marginAmt) || unitPrice==0)?0:marginAmt;
      marginPc = Math.round((marginAmt/unitPrice)*100);
      marginPc=(isNaN(marginPc) || unitPrice==0)?0:marginPc;
      $("#margin_pc_"+lineNum).text(marginPc);
      $("#margin_amount_"+lineNum).text(marginAmt);
      if ( product!=''  && qtyEdit != null && qtyEdit.value != "" && unitPriceEdit != null && unitPriceEdit.value != "" ) {
        needToUpdateTotal = true;
      }


	if (needToUpdateTotal) updateQuoteTotal();

	return true;
}

function clearUnitPrice(lineNum) {
	var unitPriceEdit = document.getElementById("unit_price" + lineNum);
	if (unitPriceEdit != null) unitPriceEdit.value = '';
}

function checkAllLinesForQuoteTotal() {
	var result = true;
	for (var i = 0; i < parseInt(line_num.value); i++) {
		//var ctrl_num = (i == 0) ? "" : i;
		result = result && checkLineNumForQuoteTotal(i);
	}
	return result;
}

var quote_total = document.getElementById("quote_total");
var reseller_cost_total = document.getElementById("reseller_cost_total");
function updateQuoteTotal() {
	//console.log("In updateQuoteTotal()");  //debug
	var subtotal = 0.00;
	var reseller_cost = 0.00;
	var lineNum = parseInt(line_num.value);
	for (var i = 0; i <= lineNum; i++ ) {
		//var x = (i == 0) ? "" : i;
		var x = i;
		var qtyEdit = document.getElementById("qty_" + x);
		var unitPriceEdit = document.getElementById("unit_price_" + x);
		var resellerPrice = document.getElementById("your_cost_" + x);
		if ( qtyEdit != null && qtyEdit.value != "" && unitPriceEdit != null && unitPriceEdit.value != "" ) {
			subtotal += parseFloat(qtyEdit.value) * parseFloat(unitPriceEdit.value);
		}
		if ( qtyEdit != null && qtyEdit.value != "" && resellerPrice != null && resellerPrice.value != "" ) {
			reseller_cost += parseFloat(qtyEdit.value) * parseFloat(resellerPrice.innerHTML);
		}
	}
	//console.log("...subtotal"+ subtotal);  //debug
	quote_total.innerHTML = subtotal.toFixed(2);
	reseller_cost_total.innerHTML = reseller_cost.toFixed(2);

	//set margin total
	var marginPcTotal=0;
	var marginAmtTotal=0;
	var quoteTotal=parseFloat(quote_total.innerHTML);
	var resellerCostTotal=parseFloat(reseller_cost_total.innerHTML);
	marginAmtTotal= quoteTotal - resellerCostTotal;
	marginAmtTotal=(parseInt(resellerCostTotal)==0 || parseInt(quoteTotal)==0)?0:marginAmtTotal;
	marginPcTotal=Math.round((marginAmtTotal/quoteTotal)*100);
	marginPcTotal=(parseInt(resellerCostTotal)==0 || parseInt(quoteTotal)==0)?0:marginPcTotal;

	$("#margin_pc_total").text(marginPcTotal);
	$("#margin_amount_total").text(marginAmtTotal);

}

var firstname = document.getElementById("firstname");
var lastname = document.getElementById("lastname");
var address = document.getElementById("address");
var city = document.getElementById("city");
var state = document.getElementById("state");
var zip = document.getElementById("zip");
var country = document.getElementById("country");
var billing_firstname = document.getElementById("billing_firstname");
var billing_lastname = document.getElementById("billing_lastname");
var billing_address = document.getElementById("billing_address");
var billing_city = document.getElementById("billing_city");
var billing_state = document.getElementById("billing_state");
var billing_zip = document.getElementById("billing_zip");
var billing_country = document.getElementById("billing_country");

function setBillingAddress(sameAsShippingChecked) {
	billing_firstname.required = !sameAsShippingChecked;
	billing_lastname.required = !sameAsShippingChecked;
	billing_address.required = !sameAsShippingChecked;
	billing_city.required = !sameAsShippingChecked;
	billing_state.required = !sameAsShippingChecked;
	billing_zip.required = !sameAsShippingChecked;
	billing_country.required = !sameAsShippingChecked;

	billing_firstname.readOnly = sameAsShippingChecked;
	billing_lastname.readOnly = sameAsShippingChecked;
	billing_address.readOnly = sameAsShippingChecked;
	billing_city.readOnly = sameAsShippingChecked;
	billing_state.readOnly = sameAsShippingChecked;
	billing_zip.readOnly = sameAsShippingChecked;
	billing_country.readOnly = sameAsShippingChecked;

	if ( sameAsShippingChecked ) {
		billing_firstname.value = firstname.value;
		billing_lastname.value = lastname.value;
		billing_address.value = address.value;
		billing_city.value = city.value;
		billing_state.value = state.value;
		billing_zip.value = zip.value;
		billing_country.value = country.value;
	}
}

</script>
HTML;

			/*
			<input type="hidden" name="distro_username" id="distro_username" value="'.$user->get_info_attr('username').'">
			<input type="hidden" name="distro_id" id="distro_id" value="'.$user->get_info_attr('id').'">
			<input type="hidden" name="distro_email" id="distro_email" value="'.$user->get_info_attr('email').'">
			*/

		return $new_quote_form;
	}//get_new_quote_form

}

