<?php


global $o_resourcesTab;
$o_resourcesTab = new ResourcesTabClass();

class ResourcesTabClass {

	function __construct() {
	}

	public function tabToStr($o_current_user) {
		$current_access = $o_current_user->get_info_attr('access');
		$title = <<< HTML
			<li class="tab" onclick="parse_resource_files();" id="resources_tab_top_thing">
				<a href="#resources_tab">RESOURCES</a>
			</li>
HTML;

		$content = <<< HTML
		<div id="resources_tab" class="tab_section tab00">
			<div id="res_tab_top" style="font:14px Verdana;color:#717074;">
HTML;

		if($current_access == 'admin'){
			$content .= <<< HTML
			<div id="resource_upload_button" style="color:#fff;width:115px;background:#8b8b8b;margin:auto;padding:5px;" onclick="resource_upload_form();">UPLOAD RESOURCE</div>
HTML;
		}
		$content .= '</div>';
		$content .= <<<HTML
		<div class="adjust_body" style="top:0px;">
			<div class="resources_home_btn" style="cursor:pointer;padding:7px;margin:auto;background:#8b8b8b;position:relative;top:58px;">
				<img src="./images/home_icon.png" style="width:20px;" alt="resources home button"/>
			</div>
		<div id="resources_home_section_wrapper">
			<div class="resources_home_row_wrapper">
				<div id="sales_resources" class="resources_home_section">
					<div id="sales_resources_header" class="resources_home_section_header" section_to_go="resources_section_sales" style="background:#aecd37;">
						<div>SALES</div>
					</div>
					<div id="sales_resources_contents" class="resources_home_section_content">
						<div style="margin:0 auto; width:60px; height:60px; padding:0; background-image:url('../beta/images/loading_60x.gif');">&nbsp;</div>
					</div>
					<div id="sales_resources_view_all" class="resources_home_section_view_all">
						<div style="cursor:pointer;" section_to_go="resources_section_sales" class="view-all-arrow">VIEW ALL <span><img style="cursor:pointer;" src="./images/more_arrow_gfx.png" alt="more arrow gfx" /><span></div>
					</div>
				</div>
				<div id="marketing_resources" class="resources_home_section">
					<div id="marketing_resources_header" class="resources_home_section_header" section_to_go="resources_section_marketing" style="background:#3382a9;">
						<div>MARKETING</div>
					</div>
					<div id="marketing_resources_contents" class="resources_home_section_content">
						<div style="margin:0 auto; width:60px; height:60px; padding:0; background-image:url('../beta/images/loading_60x.gif');">&nbsp;</div>
					</div>
					<div class="resources_home_section_view_all">
						<div style="cursor:pointer;" section_to_go="resources_section_marketing" class="view-all-arrow">VIEW ALL <span><img style="cursor:pointer;" src="./images/more_arrow_gfx.png"  alt="more arrow gfx" /><span></div>
					</div>
				</div>
			</div><!-- resources_home_row_wrapper-->
			<div class="resources_home_row_wrapper">
				<div id="featured_resources" class="resources_home_section">
					<div id="featured_resources_header" class="resources_home_section_header" section_to_go="resources_section_featured" style="background:#f68e20;">
						<div>FEATURED</div>
					</div>
					<div id="featured_resources_contents" class="resources_home_section_content">
						<div style="margin:0 auto; width:60px; height:60px; padding:0; background-image:url('../beta/images/loading_60x.gif');">&nbsp;</div>
					</div>
					<div id="sales_resources_view_all" class="resources_home_section_view_all">
						<div style="cursor:pointer;" class="view-all-arrow" section_to_go="resources_section_featured">VIEW ALL <span><img style="cursor:pointer;" src="./images/more_arrow_gfx.png" alt="more arrow gfx" /><span></div>
					</div>
				</div>
<!--
				<div id="featured_resources" class="resources_home_section">
					<div id="latest_resources_header" class="resources_home_section_header_no_go" section_to_go="resources_section_latest" style="background:RED;">
						<div>LATEST</div>
					</div>
					<div id="latest_resources_contents" class="resources_home_section_content">
						<div style="margin:0 auto; width:60px; height:60px; padding:0; background-image:url('../beta/images/loading_60x.gif');">&nbsp;</div>
					</div>
				</div>
-->
				<div id="support_resources" class="resources_home_section">
					<div id="latest_resources_header" class="resources_home_section_header" section_to_go="resources_section_support" style="background:RED;">
						<div>SUPPORT</div>
					</div>
					<div id="support_resources_contents" class="resources_home_section_content">
						<div style="margin:0 auto; width:60px; height:60px; padding:0; background-image:url('../beta/images/loading_60x.gif');">&nbsp;</div>
					</div>
					<div id="support_resources_view_all" class="resources_home_section_view_all">
						<div style="cursor:pointer;" class="view-all-arrow" section_to_go="resources_section_support">VIEW ALL <span><img style="cursor:pointer;" src="./images/more_arrow_gfx.png" alt="more arrow gfx" /><span></div>
					</div>
				</div>
				<!--<div id="resources_home_image" class="resources_home_section">
					<div style="margin:20px auto;">
						<img src="./images/LAVU_logo_360.jpg" alt="lavu logo" />
					</div>
					<div style="margin:20px auto;font:18px Verdana;color:#8b8b8b;text-align:center;">
						Introducing <span style="color:#aecd37;">LAVU HOSPITALITY</span>
					</div>
				</div>-->
			</div><!-- resources_home_row_wrapper-->
		</div><!-- resources_home_section_wrapper -->
		<div id="resources_section_sales" class="resources_category_section">
			<div class="resources_section_main_head" style="background:#aecd37;">SALES</div>
		<div class="resources_subsection_body_wrap">
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed first_subsection_closed_arrow" style="display:none;">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx" />
				</div>
				<div class="sub_section_arrow_open first_subsection_open_arrow" style="display:inline-block;">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Reseller Agreements</div>
				<div class="empty_section_message empty_reselleragreements" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="salesResellerAgreements_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed" >
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx" />
				</div>
				<div class="sub_section_arrow_open" >
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">User Guides</div>
				<div class="empty_section_message empty_userguides" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="salesUserGuides_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Cookbook</div>
				<div class="empty_section_message empty_cookbook" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="salesCookbook_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Comparison Charts</div>
				<div class="empty_section_message empty_comparisoncharts" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="salesComparisonCharts_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Defeat List</div>
				<div class="empty_section_message empty_defeatlist" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="salesDefeatList_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Niche Selling Points</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="salesNicheSellingPoints_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Maintainence</div>
				<div class="empty_section_message empty_maintainence" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="salesMaintainence_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_content" id="salesVideos_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Videos</div>
				<div class="empty_section_message empty_videos_s" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="salesNotDefined_content">
				
			</div><!-- resources_subsection_content-->
		</div><!-- resources_subsection_body_wrap -->
		</div><!-- resources_section_sales -->
		<div id="resources_section_marketing" class="resources_category_section">
			<div class="resources_section_main_head" style="background:#3382a9;">MARKETING</div>
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed first_subsection_closed_arrow" style="display:none;">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open first_subsection_open_arrow" style="display:inline-block;">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Graphics</div>
				<div class="empty_section_message empty_graphics" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingGraphics_content">
			</div><!-- resources_subsection_content-->

			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open" >
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Logos</div>
				<div class="empty_section_message empty_logos" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingLogos_content">
			</div><!-- resources_subsection_content-->

			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open" >
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Features List</div>
				<div class="empty_section_message empty_featureslist" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingFeaturesList_content">
			</div><!-- resources_subsection_content-->
				
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open" >
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Lead Generation</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingHowToLavu_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Marketing Text</div>
				<div class="empty_section_message empty_marketingtext" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingMarketingText_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open" >
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Photos</div>
				<div class="empty_section_message empty_photos" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingPhotos_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Pricing</div>
				<div class="empty_section_message empty_pricing" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingPricing_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open" >
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Print Media</div>
				<div class="empty_section_message empty_printmedia" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingPrintMedia_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Testimonials</div>
				<div class="empty_section_message empty_testimonials" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingTestimonials_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open" >
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Videos</div>
				<div class="empty_section_message empty_videos_m" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingVideos_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">Website Components</div>
				<div class="empty_section_message empty_websitecomponents" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="marketingWebsiteComponents_content">
				
			</div><!-- resources_subsection_content-->
			<div class="resources_subsection_content" id="marketingNotDefined_content">
				
			</div><!-- resources_subsection_content-->
			
		</div><!-- resources_section_marketing -->
		<div id="resources_section_featured" class="resources_category_section">
			<div class="resources_section_main_head" style="background:#f68e20;">FEATURED</div>
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed first_subsection_closed_arrow_featured" style="display:none;">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open first_subsection_open_arrow_featured" style="display:inline-block;">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">ALL FEATURED RESOURCES</div>
				<div class="empty_section_message empty_allFeatured" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="allFeatured_content">
				
			</div><!-- resources_subsection_content-->
		</div><!-- resources_section_featured -->

		<div id="resources_section_support" class="resources_category_section">
			<div class="resources_section_main_head" style="background:RED;">SUPPORT</div>

			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed first_subsection_closed_arrow" style="display:none;">
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
				</div>
				<div class="sub_section_arrow_open first_subsection_open_arrow" style="display:inline-block;">
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">HOW-TO</div>
				<div class="empty_section_message empty_supporthowto" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="supportHowTo_content">
			</div><!-- resources_subsection_content-->
				
			<div class="resources_subsection_head">
				<div class="sub_section_arrow_closed" >
					<img src="./images/more_arrow_gfx.png" alt="more arrow gfx" />
				</div>
				<div class="sub_section_arrow_open" >
					<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
				</div>
				<div class="resources_subsection_category_title">RESOURCES</div>
				<div class="empty_section_message empty_supportresources" style="display:inline-block;vertical-align:top;font:19px Verdana;margin-left:20px;">(coming soon)</div>
			</div><!--resources_subsection_head -->
			<div class="resources_subsection_content" id="supportResources_content">
			</div><!-- resources_subsection_content-->

			<div class="resources_subsection_content" id="supportNotDefined_content">			
			</div><!-- resources_subsection_content-->

		</div><!-- resources_section_support -->


		</div><!-- adjust_body-->
		</div><!-- resources_tab-->

HTML;

		return array('title'=>$title, 'content'=>$content);

	}
}
