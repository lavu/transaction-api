<?php
ini_set('memory_limit', '512M');
require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
require_once("/home/poslavu/public_html/admin/distro/beta/timing_functions.php");
if (!isset($o_package_container))
	$o_package_container = new package_container();

global $o_unassignedTab;
$o_unassignedTab = new UnassignedTabClass();

class UnassignedTabClass {

	function __construct() {
	}

	public function tabToStr($o_current_user, &$a_resellers, $options) {
		global $o_package_container;
		$o_current_user->set_all_unassigned($options);
		$unacc = $o_current_user->get_all_unassigned_leads();
	    //$in_progress = self::$o_current_user->get_all_in_progress();
	    //$processed = self::$o_current_user->get_all_processed();
		$current_access = $o_current_user->get_info_attr('access');
		$username = $o_current_user->get_info_attr('username');
		$userid = $o_current_user->get_info_attr('id');
		//echo 'mmmaaaaa:: '.$current_access.'<br />';
		$title = "";
		$j_decode = new Json();
		//count unaccepted leads
		$all_count = count($unacc);
		//echo $all_count;
		//echo 'tabToStr_test()';
		//count unassigned leads
		$unassigned = array();
		$merc_unass_count = 0;
	    // get some values

		foreach($unacc as $key=>$value){
			//echo 'assigned to :: '.$value['distro_code'].'<br />';
			//echo $value['_declined'].'<br />';
			//if($value['id'] == '10851'){echo 'cozy nut';}
			if(($value['distro_code'] == '' || $value['_declined']) && $value['_canceled'] == '0' ){
			//var_dump($value);
				//echo "found unassigned::".$value['id'].", Declined::".$value['_declined']."<br />";
				$unass_id = $value['id'];
				foreach($value as $att=>$val){
					//if($attrs['_canceled'] == '1'){
						$unassigned[$unass_id][$att] = $val;						
					//}

					//echo "unassigned[".$unass_id."][".$att."]=".$val."<br />";
				}
				/*if($current_access == 'mercury' && $value['lead_source'] == 'mercury'){
					$merc_unass_count++;
				}*/
			}
		}
		$unass_count = count($unassigned);
	    //echo "total unassigned::".$unass_count.", total unaccepted:: ".$all_count;
		
		
		function lead_color_u($stat, $div,$color_class){
	        //echo 'stat='.$stat.' div='.$div.' cc='.$color_class;
			if($stat === $div){
				return $color_class;
			}
			return '';
		}
		
		
		//different display for different distro levels
		if($current_access == 'mercury'){
			$title ='<li class="tab"><a href="#unacc_tab">UNASSIGNED</a></li>
			';
		}else{
			$title ='<li class="tab"><a href="#unacc_tab">
			<span style="background-image:url(./images/circ_leads.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$unass_count.'
			</span>
			UNASSIGNED</a></li>
			';
		}
		$content ='<div id="unacc_tab" class="tab_section tab00">';
		$content .= '
		<div class="tab_top" style="height:70px;">
			<div class="tab_top_accnts">
				<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">UNACCEPTED LEADS</div>';
				if($current_access == 'admin'){
					$content .= '
					<div id="leads_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;padding-top:25px;">
						<img src="./images/btn_create_lead.png" alt="btn_apply" class="new_lead_btn"/></span>
					</div>';
				}
				$content .= '
			</div>
			<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
		</div>';
		$content .= '<div class="adjust_body">';
		
		$merc_legend = '';
		if($current_access == 'admin'){
			$merc_legend = '<div style="height:30px;width:950px;margin:0 auto 20px;"><img src="./images/lead_legend_sponsor.png" alt="mlegends"></div>';
		}else{
			$merc_legend = '<div style="height:30px;width:950px;margin:0 auto 20px;"><img src="./images/lead_legend_sponsor_for_mercurysite.png" alt="mlegendm"></div>';
		}
		//if($current_access == 'admin'){
		$content .= '
		<div id="unacc_lead_desc">
			<div style="height:30px;width:950px;margin: 0 auto;"><img src="./images/lead_legend.png" alt="llegend"></div>
			'.$merc_legend.'
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:115px;">LOCATION</span>
			<span style="width:186px;">EMAIL</span>
			<span style="width:113px;">NAME</span>
			<span style="width:110px;">PHONE</span>
			<span style="width:84px;">D:H:M:S</span>
			<span style="width:54px;">ASSIGN</span>
			<span style="width:42px;">NOTES</span>
			<span style="width:34px;">REMOVE</span>
			<br style="clear:left;" />													
		</div>
		
		<div style="cursor:pointer;display:none;width:200px;border:1px solid black;padding:10px;margin-left:680px;margin-top:10px;margin-bottom:10px;font-size:14px;width:220px;background:red;" class="remove_all_selected_leads" username="'.$username.'" res_id="'.$userid.'" > REMOVE SELECTED LEADS</div>
		
		<div id="unassigned_lead_acc_wrapper">';
		//}

		//$content .= '<div id="unassigned_lead_acc_wrapper">';
		foreach($unassigned as $key=>$attrs){
			//if($attrs['id'] == '10363'){
				//echo("cozy nut:: ".$attrs['company_name']).'<br />';
				//continue;
			//}
			$lead_stat = 'accept';
			$lead_source = '';
		    //echo "herro?<br />";
			$accepts_mercury = $j_decode->decode($resellers[$attrs['distro_code']]['special_json']);
			$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
			if($attrs['lead_source'] == 'mercury'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}else if($attrs['lead_source'] == 'mercury_bleed'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini_bleed.png" alt="mm"/></span>';
			}
		    //echo $lead_stat.'==><br/> ';
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_accept';
			if($attrs['contacted'] != '0000-00-00 00:00:00') $circ_image_ref = 'circ_contact';
			if($attrs['made_demo_account'] != '0000-00-00 00:00:00') $made_demo_account =  TRUE; else $made_demo_account = FALSE;
			if ($made_demo_account) $circ_image_ref .= '_has_demo_account';
			if($attrs['declined_by'] != ''){
				$circ_image_ref = 'circ_black';
			} 
			if($attrs['created'] != '0000-00-00 00:00:00'){
				$birth = $attrs['created'];
				$age = time_from($birth);	
			}
		    //echo 'before_access = '.$current_access;
		    $time_values = get_lead_times($attrs);
	   //error_log('distro/beta/exec/searchLeadTemp.php time_values:: '.var_dump($time_values));
	//var_dump($time_values);
	//self::$current_user->set_lead_attr($attrs['data_name'], 'signup_step', $time_values['signups']);
	$total_age = $time_values['first'];
	$total_age_raw = $time_values['first_raw'];
	//$last_raw = $time_values['most_recent_raw'];
	//$assigned_raw = $time_values['assigned_raw'];
	//echo $total_age.', '.$total_age_raw.'<br />';
	//$total_exp = explode(" ", $total_age);
	//$number_age = intval($total_exp[0]);
	//$follow_up = '';
	//$follow_up_span = '';
	//$never_accepted = '';
	//<span style="width:70px;" class="hover_flow">'.$attrs['declined_by'].'</span><div class="loc_head_div">|</div>
			//<span style="width:70px;" class="hover_flow">'.$total_age.'</span><div class="loc_head_div">|</div>
			if($current_access == 'admin'){
				$content .= '
				<div class="acc_head accordionButton">
					<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
					<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
					<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
					<span style="width:120px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
					<span style="width:120px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
					<span style="width:175px;" class="hover_flow">'.$attrs['email'].'</span><div class="loc_head_div">|</div>
					<span style="width:95px;" class="hover_flow">'.$attrs['firstname'].' '.$attrs['lastname'].'</span><div class="loc_head_div">|</div>
					<span style="width:100px;" class="hover_flow">'.$attrs['phone'].'</span><div class="loc_head_div">|</div>
					<span style="width:80px;" class="hover_flow">'.$total_age.'</span><div class="loc_head_div">|</div>
					<span style="width:60px;" class="hover_flow"><img src="./images/btn_assign.png" alt="assign" style="height:23px;" class="assign_lead"/></span><div class="loc_head_div">|</div>
					<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:30px;"><img src="./images/i_notes.png" alt="i_n"/></span>
					'.$lead_source.'
					<span class="remove_lead_early" style="width:30px;"><input type="checkbox" class="input_remove_lead" lead_id="'.$attrs['id'].'"/></span>
					<br class="clear:left;">
				</div>';
			//if($current_access == 'mercury')
			}else {
			//echo 'mercury access='.$attrs['lead_source'].', '.$attrs['company_name'].', '.$attrs['distro_code'].'<br />';
				if($current_access == 'mercury' && $merc_dist && $attrs['lead_source'] != 'distro portal'){
					$content .= '
					<div class="acc_head accordionButton">
						<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
						<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
						<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
						<span style="width:150px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
						<span style="width:150px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
						<span style="width:70px;" class="hover_flow">'.$attrs['declined_by'].'</span><div class="loc_head_div">|</div>
						<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>							'.$lead_source.' 
						<br class="clear:left;">
					</div>		
					';
				}
			}
			$content .= '
			<div class="acc_lead_content accordionContent">
				<div class="lead_acc_content_head"><img src="./images/prog_bar_full.png" alt="p_b_f"/></div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color_u($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>	
			</div>';		
	    }//for each unassigned
	    $content .= '</div><!--unassigned_lead_acc_wrapper -->';//end of unassigned
	    $content .= '
	    <div style="margin:20px;text-align:center;font:30px verdana;color:#439789;">ALL IN-PROGRESS LEADS</div>';
	
	    if($current_access == 'admin'){
		$conten .='
		<div id="in_prog_lead_desc">
			<span style="width:50px;cursor:pointer;" class="lead_sort_by" sort_by="status">STATUS</span>
			<span style="width:115px;" class="lead_sort_by" sort_by="b_name">BUSINESS NAME</span>
			<span style="width:120px;">TOTAL AGE</span>
			<span style="width:100px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:105px;">REMIND</span>
			<span style="width:105px;">REMOVE</span>
			<span style="width:80px;">RESELLER</span>
			<span style="width:80px;">ASSIGN</span>
			<span>NOTES</span>													
		</div>';
	    }
	    if($current_access == 'mercury'){
		$conten .='
		<div id="in_prog_lead_desc">
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:120px;">TOTAL AGE</span>
			<span style="width:156px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:150px;">RESELLER</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>';
	    }
	    $content .= "<div class='in_progress_wrapper' style='text-align:left;'>";
	    $content .= $this->get_lead_search($current_access);
	    $content .= $this->get_inProg_lead_display($current_access);
	    $content .= '</div><!--in_progress_wrapper -->';//end of in_progress_wrapper
	    $content .= '</div><!--adjust_body -->';//end of adjust_body
	    $content .= '</div><!--unacc_tab -->';//end of adjust_body
	    // set the account range
	    // set to only load accounts within the range

	    // add content to tab_list
	    return array('title'=>$title, 'content'=>$content);
	}//tabToStr()
	public function tabToStr_test($o_current_user,&$a_resellers){
		global $o_package_container;
		$o_current_user->set_all_unassigned();
		$unacc = $o_current_user->get_all_unassigned_leads();
	    //$in_progress = self::$o_current_user->get_all_in_progress();
	    //$processed = self::$o_current_user->get_all_processed();
		$current_access = $o_current_user->get_info_attr('access');
		//echo 'mmmaaaaa:: '.$current_access.'<br />';
		$title = "";
		$j_decode = new Json();
		//count unaccepted leads
		$all_count = count($unacc);
		//echo $all_count;
		//echo 'tabToStr_test()';
		//count unassigned leads
		$unassigned = array();
		$merc_unass_count = 0;
	    // get some values

		foreach($unacc as $key=>$value){
			//echo 'assigned to :: '.$value['distro_code'].'<br />';
			//echo $value['_declined'].'<br />';
			if($value['distro_code'] == '' || $value['_declined']){
			//var_dump($value);
				//echo "found unassigned::".$value['id'].", Declined::".$value['_declined']."<br />";
				$unass_id = $value['id'];
				foreach($value as $att=>$val){
					$unassigned[$unass_id][$att] = $val;
					//echo "unassigned[".$unass_id."][".$att."]=".$val."<br />";
				}
				/*if($current_access == 'mercury' && $value['lead_source'] == 'mercury'){
					$merc_unass_count++;
				}*/
			}
		}
		$unass_count = count($unassigned);
	    //echo "total unassigned::".$unass_count.", total unaccepted:: ".$all_count;
		
		
		function lead_color_u($stat, $div,$color_class){
	        //echo 'stat='.$stat.' div='.$div.' cc='.$color_class;
			if($stat === $div){
				return $color_class;
			}
			return '';
		}
		
		
		//different display for different distro levels
		if($current_access == 'mercury'){
			$title ='<li class="tab"><a href="#unacc_tab">UNASSIGNED</a></li>
			';
		}else{
			$title ='<li class="tab"><a href="#unacc_tab">
			<span style="background-image:url(./images/circ_leads.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$unass_count.'
			</span>
			UNASSIGNED</a></li>
			';
		}
		$content ='<div id="unacc_tab" class="tab_section tab00">';
		$content .= '
		<div class="tab_top" style="height:70px;">
			<div class="tab_top_accnts">
				<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">UNACCEPTED LEADS</div>';
				if($current_access == 'admin'){
					$content .= '
					<div id="leads_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;padding-top:25px;">
						<img src="./images/btn_create_lead.png" alt="btn_apply" class="new_lead_btn"/></span>
					</div>';
				}
				$content .= '
			</div>
			<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
		</div>';
		$content .= '<div class="adjust_body">';
		
		$merc_legend = '';
		if($current_access == 'admin'){
			$merc_legend = '<div style="height:30px;width:950px;margin:0 auto 20px;"><img src="./images/lead_legend_sponsor.png" alt="mlegends"></div>';
		}else{
			$merc_legend = '<div style="height:30px;width:950px;margin:0 auto 20px;"><img src="./images/lead_legend_sponsor_for_mercurysite.png" alt="mlegendm"></div>';
		}
		//if($current_access == 'admin'){
		$content .= '
		<div id="unacc_lead_desc">
			<div style="height:30px;width:950px;margin: 0 auto;"><img src="./images/lead_legend.png" alt="llegend"></div>
			'.$merc_legend.'
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:115px;">LOCATION</span>
			<span style="width:186px;">EMAIL</span>
			<span style="width:113px;">NAME</span>
			<span style="width:110px;">PHONE</span>
			<span style="width:84px;">DECLINED</span>
			<span style="width:54px;">ASSIGN</span>
			<span style="width:42px;">NOTES</span>
			<span style="width:34px;">REMOVE</span>
			<br style="clear:left;" />													
		</div>
		
		<div style="cursor:pointer;display:none;width:200px;border:1px solid black;padding:10px;margin-left:680px;margin-top:10px;margin-bottom:10px;font-size:14px;width:220px;background:red;" class="remove_all_selected_leads"> REMOVE SELECTED LEADS</div>
		
		<div id="unassigned_lead_acc_wrapper">';
		//}

		//$content .= '<div id="unassigned_lead_acc_wrapper">';
		foreach($unassigned as $key=>$attrs){
			$lead_stat = 'accept';
			$lead_source = '';
		    //echo "herro?<br />";
			if($attrs['_canceled']){
				echo("skipping:: ".$attrs['company_name']);
				break;
			}
			$accepts_mercury = $j_decode->decode($resellers[$attrs['distro_code']]['special_json']);
			$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
			if($attrs['lead_source'] == 'mercury'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}else if($attrs['lead_source'] == 'mercury_bleed'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini_bleed.png" alt="mm"/></span>';
			}
		    //echo $lead_stat.'==><br/> ';
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_accept';
			if($attrs['contacted'] != '0000-00-00 00:00:00') $circ_image_ref = 'circ_contact';
			if($attrs['made_demo_account'] != '0000-00-00 00:00:00') $made_demo_account =  TRUE; else $made_demo_account = FALSE;
			if ($made_demo_account) $circ_image_ref .= '_has_demo_account';
			if($attrs['declined_by'] != ''){
				$circ_image_ref = 'circ_black';
			} 
			if($attrs['created'] != '0000-00-00 00:00:00'){
				$birth = $attrs['created'];
				$age = time_from($birth);	
			}
		    //echo 'before_access = '.$current_access;
			if($current_access == 'admin'){
				$content .= '
				<div class="acc_head accordionButton">
					<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
					<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
					<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
					<span style="width:120px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
					<span style="width:140px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
					<span style="width:175px;" class="hover_flow">'.$attrs['email'].'</span><div class="loc_head_div">|</div>
					<span style="width:95px;" class="hover_flow">'.$attrs['firstname'].' '.$attrs['lastname'].'</span><div class="loc_head_div">|</div>
					<span style="width:100px;" class="hover_flow">'.$attrs['phone'].'</span><div class="loc_head_div">|</div>
					<span style="width:70px;" class="hover_flow">'.$attrs['declined_by'].'</span><div class="loc_head_div">|</div>
					<span style="width:60px;" class="hover_flow"><img src="./images/btn_assign.png" alt="assign" style="height:23px;" class="assign_lead"/></span><div class="loc_head_div">|</div>
					<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:30px;"><img src="./images/i_notes.png" alt="i_n"/></span>
					'.$lead_source.'
					<span class="remove_lead_early" style="width:30px;"><input type="checkbox" class="input_remove_lead" lead_id="'.$attrs['id'].'"/></span>
					<br class="clear:left;">
				</div>';
			//if($current_access == 'mercury')
			}else {
			//echo 'mercury access='.$attrs['lead_source'].', '.$attrs['company_name'].', '.$attrs['distro_code'].'<br />';
				if($current_access == 'mercury' && $merc_dist && $attrs['lead_source'] != 'distro portal'){
					$content .= '
					<div class="acc_head accordionButton">
						<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
						<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
						<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
						<span style="width:150px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
						<span style="width:150px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
						<span style="width:70px;" class="hover_flow">'.$attrs['declined_by'].'</span><div class="loc_head_div">|</div>
						<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>							'.$lead_source.' 
						<br class="clear:left;">
					</div>		
					';
				}
			}
			$content .= '
			<div class="acc_lead_content accordionContent">
				<div class="lead_acc_content_head"><img src="./images/prog_bar_full.png" alt="p_b_f"/></div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color_u($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>	
			</div>';		
	    }//for each unassigned
	    $content .= '</div><!--unassigned_lead_acc_wrapper -->';//end of unassigned
	    $content .= '
	    <div style="margin:20px;text-align:center;font:30px verdana;color:#439789;">ALL IN-PROGRESS LEADS</div>';
	
	    if($current_access == 'admin'){
		$conten .='
		<div id="in_prog_lead_desc">
			<span style="width:50px;cursor:pointer;" class="lead_sort_by" sort_by="status">STATUS</span>
			<span style="width:115px;" class="lead_sort_by" sort_by="b_name">BUSINESS NAME</span>
			<span style="width:120px;">TOTAL AGE</span>
			<span style="width:100px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:105px;">REMIND</span>
			<span style="width:105px;">REMOVE</span>
			<span style="width:80px;">RESELLER</span>
			<span style="width:80px;">ASSIGN</span>
			<span>NOTES</span>													
		</div>';
	    }
	    if($current_access == 'mercury'){
		$conten .='
		<div id="in_prog_lead_desc">
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:120px;">TOTAL AGE</span>
			<span style="width:156px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:150px;">RESELLER</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>';
	    }
	    $content .= "<div class='in_progress_wrapper' style='text-align:left;'>";
	    $content .= $this->get_lead_search($current_access);
	    $content .= $this->get_inProg_lead_display($current_access);
	    $content .= '</div><!--in_progress_wrapper -->';//end of in_progress_wrapper
	    $content .= '</div><!--adjust_body -->';//end of adjust_body
	    $content .= '</div><!--unacc_tab -->';//end of adjust_body
	    // set the account range
	    // set to only load accounts within the range

	    // add content to tab_list
	    return array('title'=>$title, 'content'=>$content);
	}//tabToStr_test()
	private function get_lead_search($current_access){
	    //echo $o_current_user->get_info_attr('access');
		$lead_search_content = '
		<div id="inProg_search_area" class="portal_search in_progress_search" style="display:inline-block;margin-left:60px;width:500px;height:30px;">
			<input type="text" id="inProg_searchInput" placeholder="Lead Search" name="inProgSearch" style="height:24px;width:380px;font-size:20px;z-index:100;" />
			<div id="inProg_submit" style="width:65px;border:1px solid black;padding:6px;position:relative;bottom:30px;left:400px;cursor:pointer;" onclick="filter_inProgSearch();">SEARCH</div>
		</div>
		<div class="portal_search_filter" id="inProg_search_filter" style="text-align:left;margin-left:60px;">Add Filters to Search:
			<input type="checkbox" id="inProg_use_filters" onchange=\'if($("#inProg_filters").css("display")=="none") $("#inProg_filters").show();else $("#inProg_filters").hide();\' />
		</div>
		<table id="inProg_filters" class="portal_search_filter_table" style="border:1px solid black;border-collapse:collapse;display:none;margin-left:60px;">
			<form action="#" id="inProg_filter_form" method="post">
				<tbody style="display:table-row-group;vertical-align:middle;border-color:inherit;">
					<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Leads With Demos:
								<input type="checkbox" id="inProg_filter_demo" name="inProg_filter_demo" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Unaccepted Leads:
								<input type="checkbox" id="inProg_filter_unacc" name="inProg_filter_unacc" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Accepted leads:
								<input type="checkbox" id="inProg_filter_acc" name="inProg_filter_acc" style="float:right;" />
							</small>
							<br />
						</td>
					</tr>
					<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Contacted Leads:
								<input type="checkbox" id="inProg_filter_cont" name="inProg_filter_cont" style="float:right;" />
							</small>
							<br />
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Sold Leads:
								<input type="checkbox" id="inProg_filter_sold" name="inProg_filter_sold" style="float:right;" />
							</small>
							<br />
						</td>';
						if($current_access=='admin'){
							$lead_search_content .= '
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>try.poslavu.com:
									<input type="checkbox" id="inProg_filter_try" name="inProg_filter_try" style="float:right;" />
								</small>
								<br />
							</td>
						</tr>
						<tr style="display:table-row;vertical-align:inherit;border-color:inherit;">
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>baripadpos.com:
									<input type="checkbox" id="inProg_filter_barmicro" name="inProg_filter_barmicro" style="float:right;" />
								</small>
								<br />
							</td>
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>pizzaipadpos.com:
									<input type="checkbox" id="inProg_filter_pizzamicro" name="inProg_filter_pizzamicro" style="float:right;" />
								</small>
								<br />
							</td>
							<td style="border-bottom:1px solid black;overflow:auto;">
								<small>lavuhospitality.com:
									<input type="checkbox" id="inProg_filter_hosmicro" name="inProg_filter_hosmicro" style="float:right;" />
								</small>
								<br />
							</td>
						</tr>';
					}else{
						$lead_search_content .= '
						<input type="hidden" id="inProg_filter_try_login" name="inProg_filter_try_login" value="mercury_lands" />
						<!--<input type="hidden" id="inProg_filter_try" name="inProg_filter_try" value="mercury" />-->
						</tr>';
					}
					$lead_search_content .= '
					<tr>
						<td>
							<select name="inProg_filter_time" id="inProg_filter_time">
								<option value="">Select Time Range</option>
								<option value="-1 week">Past Week</option>
								<option value="-2 weeks">Past 2 Weeks</option>
								<option value="-1 month">Past 30 Days</option>
								<option value="-2 months">Past 60 Days</option>
								<option value="-3 months">Past 90 Days</option>
								<option value="-6 months">Past 6 Months</option>
								<option value="">All Time</option>
							</select>
						</td>
						<td style="border-bottom:1px solid black;overflow:auto;">
							<small>Canceled:
								<input type="checkbox" id="inProg_filter_canceled" name="inProg_filter_canceled" style="float:right;" />
							</small>
							<br />
						</td>
					</tr>
				</tbody>
			</form>
		</table>';
		return $lead_search_content;
	}//get_lead_search()
	private function get_inProg_lead_display($current_access){
		if($current_access == 'admin'){
			$inProg_lead_display_content .='
			<div id="in_prog_lead_desc" style="display:none;">
				<span style="width:50px;cursor:pointer;" class="lead_sort_by" sort_by="status">STATUS</span>
				<span style="width:115px;cursor:pointer;" class="lead_sort_by" sort_by="b_name">BUSINESS NAME</span>
				<span style="width:120px;cursor:pointer;" class="lead_sort_by" sort_by="total_age">TOTAL AGE</span>
				<span style="width:100px;cursor:pointer;" class="lead_sort_by" sort_by="last_age">TIME SINCE LAST</span>
				<span style="width:109px;cursor:pointer;" class="lead_sort_by" sort_by="assigned_age">ASSIGNED</span>
				<span style="width:105px;">REMIND</span>
				<span style="width:105px;">REMOVE</span>
				<span style="width:80px;">RESELLER</span>
				<span style="width:80px;">ASSIGN</span>
				<span>NOTES</span>													
			</div>';
		}else if($current_access == 'mercury'){
			$inProg_lead_display_content .='
			<div id="in_prog_lead_desc" style="display:none;">
				<span style="width:50px;cursor:pointer;" class="lead_sort_by" sort_by="status">STATUS</span>
				<span style="width:115px;cursor:pointer;" class="lead_sort_by" sort_by="b_name">BUSINESS NAME</span>
				<span style="width:120px;cursor:pointer;" class="lead_sort_by" sort_by="total_age">TOTAL AGE</span>
				<span style="width:100px;cursor:pointer;" class="lead_sort_by" sort_by="last_age">TIME SINCE LAST</span>
				<span style="width:109px;cursor:pointer;" class="lead_sort_by" sort_by="assigned_age">ASSIGNED</span>
				<span style="width:150px;">RESELLER</span>
				<span>NOTES</span>													
			</div>';
		}
		$inProg_lead_display_content .= '
		<div id="inProg_lead_display" class="client_display">
		</div><!--inProg_lead_display-->';
		return $inProg_lead_display_content;
	}//get_inProg_lead_display()
}//class UnassignedTabClass
