<?php

require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
if (!isset($o_package_container))
	$o_package_container = new package_container();

global $o_accountsTab;
$o_accountsTab = new AccountsTabClass();

class AccountsTabClass {

function __construct() {
}

public function tabToStr($o_current_user, &$a_licenses_not_upgrades_not_special) {
	global $o_package_container;
	// get some values
	$accounts = $o_current_user->get_accounts();
	$location_notes = $o_current_user->get_loc_notes();
	$location_contacts = $o_current_user->get_loc_contacts();
	$uname = $o_current_user->get_info_attr('username');
	if($uname=='distrotest'){
		//error_log('tabONE paramater2=='.print_r($a_licenses_not_upgrades_not_special));		
	}
	$range = $o_current_user->get_info_attr('account_display_range');
	$redirect = $o_current_user->get_info_attr('account_redirect');
	$user_email = $o_current_user->get_info_attr('email');
	$next_range_attr = '';
	$prev_range_attr = '';

	// set the account range
	// set to only load accounts within the range
	if($uname=="distrotest"){
		//print_r($accounts);
	}
	if($range != ''){
		$range_exploded = explode(",",$range);
		$pr_start = ((int)$range_exploded[0]*1)- 50;
		$pr_end = ((int)$range_exploded[1]*1) - 50;
		$nr_start = ((int)$range_exploded[0]*1) + 50;
		$nr_end = ((int)$range_exploded[1]*1 ) + 50;
		$prev_range_attr = $pr_start.', '.$pr_end;
		$next_range_attr = $nr_start.', '.$nr_end;
	}else{
		$range = '#';
		$prev_range_attr = '#';
		$next_range_attr = '50, 100';
	}

	if($redirect ==  ''){
		$redirect = './index.php?';
	}

	// add content to tab_list
	$title ='<li class="tab li_accounts_tab" style="width:110px;"><a href="#accounts_tab">ACCOUNTS</a></li>
	';

	// get the header of this tab
	$content ='
	<div id="accounts_tab" class="tab_section tab00">
		<div class="tab_top" style="height:70px;">
			<div class="tab_top_accnts">
				<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">MANAGE ACCOUNTS</div>
				<div id="accounts_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;">
					<div id="account_points" style="margin:5px;color:#b3b3b3;font:13px Helvetica;">&nbsp;</div>
					<div id="account_create_account" style="background-image:url(./images/btn_createaccount__.png);width:144px;height:22px;"></div>
				</div>
			</div>
				<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
			</div>
			<div id="account_nav" style="position:relative;top:100px;">

				<a href="'.$redirect.'change_account_range='.$prev_range_attr.'#accounts_tab" id="prev_start" range="'.$prev_range_attr.'" style="color:#aecd37;cursor:pointer;margin-right:20px;">prev 50</a>
				<a href="'.$redirect.'change_account_range='.$next_range_attr.'#accounts_tab" range="'.$next_range_attr.'" id="next_start"style="color:#aecd37;cursor:pointer;">next 50</a>

			</div>
			<div class="adjust_body" style="position:relative; top:115px;"><div id="accnts_desc">
				<span style="width:200px;text-align:left;margin-left:20px;">COMPANY</span>
			<span style="width:121px;">CREATED</span>
			<span style="width:191px;">GOOD UNTIL</span>
			<span style="width:192px;">LEVEL</span>
			<span style="width:75px;display:none;">LOGIN</span>
			<span style="width:20px;cursor:pointer;color:#aecd37;" class="view_reviews"><a style="color:#aecd37;" href="#" title="POS Lavu app reviews" class="tooltip"><span title="More">APP REVIEW</span></a></span>
			<span style="width:160px;cursor:pointer;color:#439789;" class="view_reviews"><a style="color:#aecd37;" href="#" title="Reseller reviews" class="tooltip"><span title="More" style="color:#439789;">YOUR REVIEW</span></a></span>
			</div>
			<div id="account_acc_wrapper">';

	//used as a place_filler to make id names unique in forms
	$contact_default_name = 0;

	foreach($accounts as $s_data_name=>$attrs){
		//logic to get created/good until fields and format them
		$trial_end = $attrs['trial_end'];
		$created = tab_container::display_datetime((string)$attrs['created'], "date");
		// get the licensing info for the account
		$license_applied = $attrs['license_applied'];
		$license_type = $attrs['license_type'];
		if (is_numeric($attrs['package'])) {
			$original_license = $license_applied;
			$license_applied = (int)$attrs['package'];
			$license_type = $o_package_container->get_plain_name_by_attribute('level', $attrs['package']);
		}
		// get applicable special licenses that can be applied to this account
		$a_applicable_special_licenses = $o_current_user->get_applicable_special_licenses($s_data_name);
		$s_special_licenses = (count($a_applicable_special_licenses) == 0 ? '' : '<input type="hidden" name="available_special_licenses" value="'.implode(',', $a_applicable_special_licenses).'" />');
		// get applicable licenses that can be applied to this account
		$a_licenses_strings = $a_licenses_not_upgrades_not_special;
		$level_display = tab_container::display_level($trial_end, $original_license, $license_type, $attrs['data_name'], $a_licenses_strings, $s_special_licenses, $o_current_user, $attrs['has_license_payment']);
		// get the "good_until" display string
		if($trial_end==""){
			$good_until = "n/a";
		}else{
			$good_until = tab_container::display_datetime((string)$trial_end,"date");
		}
		// get the dataname
		$dataname = str_replace(" ","",$attrs['data_name']);
		// build the account's specific display row
		$content .= '
		<div class="acc_head accordionButton acc_count acc_'.$dataname.'">
			<img class="acc_arrow expand_account_btn" src="./images/arrow_left.png" style="float:left;" alt="a_l"/>
			<span style="width:220px;"><a href="./exec/backend_account_login.php?loginto='.$dataname.'&loggedin='.$uname.'&email='.$user_email.'" target="_blank">'.$attrs['company_name'].'</a></span>
			<span style="width:150px;">'.$created.'</span>
			<span style="width:150px;">'.$good_until.'</span>
			<span style="width:160px;text-align:center;">'.$level_display.'</span>
			<span style="width:100px;text-align:center;display:none;"><a href="./exec/backend_account_login.php?loginto='.$dataname.'&loggedin='.$uname.'&email='.$user_email.'" target="_blank">login</a></span>
			<input type="hidden" name="dataname" value="'.$dataname.'" />
		</div>
			';
		// build the account's specific data row that the javascript uses to pull for everything
		$content .= '<div class="acc_accnt_content accordionContent">
		';
		$content .= $this->get_account_notes($s_data_name, $location_notes, $contact_default_name);
		$content .= $this->get_account_contacts($s_data_name, $location_contacts,$contact_default_name);
		$contact_default_name++;
		$content .= $this->get_account_location($attrs);
		$content .= '
		</div>';
	}//foreach($accounts as $s_data_name=>$att)

	// get the footer of this tab
	$content .= '
			</div>
		</div>
	</div>';

	return array('title'=>$title, 'content'=>$content);
}//tabToStr
public function tabTwoStr($o_current_user, &$a_licenses_not_upgrades_not_special){
	global $o_package_container;
	
	// get some values
	//$accounts = $o_current_user->get_accounts();
	//$location_notes = $o_current_user->get_loc_notes();
	//$location_contacts = $o_current_user->get_loc_contacts();
	$uname = $o_current_user->get_info_attr('username');
	if($uname=='distrotest'){
		//error_log('tabTWO paramater2=='.print_r($a_licenses_not_upgrades_not_special));		
	}

	$d_id = $o_current_user->get_info_attr('id');
	$d_access = $o_current_user->get_info_attr('access');        
	$range = $o_current_user->get_info_attr('account_display_range');
	$redirect = $o_current_user->get_info_attr('account_redirect');
	//echo 'account_redirect == '.$redirect.'<br />';
	$user_email = $o_current_user->get_info_attr('email');
	
	// set to only load accounts within the range
	if($redirect ==  ''){
		$redirect = './index.php?';
	}
	// add content to tab_list
	$title ='<li class="tab li_accounts_tab" style="width:110px;"><a href="#accounts_tab">ACCOUNTS</a></li>
	';
	$content ='
	<div id="accounts_tab" class="tab_section tab00">
		<div class="tab_top" style="height:70px;">
			<div class="tab_top_accnts">
				<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">MANAGE ACCOUNTS</div>
				<div id="accounts_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;">
					<div id="account_points" style="margin:5px;color:#b3b3b3;font:13px Helvetica;">&nbsp;</div>
					<div id="account_create_account" style="background-image:url(./images/btn_createaccount__.png);width:144px;height:22px;"></div>
				</div>
			</div>
				<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
		</div>
		<div class="adjust_body" style="position:relative; top:115px;">
			<div id="account_acc_wrapper">';
	$content .= $this->get_account_search($uname, $d_access, $d_id);
	$content .= '
				<div id="accnts_desc" style="display:none;">
					<span style="width:200px;text-align:left;margin-left:20px;">COMPANY</span>
					<span style="width:121px;">CREATED</span>
					<span style="width:191px;">GOOD UNTIL</span>
					<span style="width:192px;">LEVEL</span>
					<span style="width:75px;display:none;">LOGIN</span>
					<span style="width:20px;cursor:pointer;color:#aecd37;" class="view_reviews"><a style="color:#aecd37;" href="#" title="POS Lavu app reviews" class="tooltip"><span title="More">APP REVIEW</span></a></span>
					<span style="width:160px;cursor:pointer;color:#439789;" class="view_reviews"><a style="color:#aecd37;" href="#" title="Reseller reviews" class="tooltip"><span title="More" style="color:#439789;">YOUR REVIEW</span></a></span>
				</div>
				<div id="accounts_display"></div>
			</div>
		</div>
	</div>';

	return array('title'=>$title, 'content'=>$content);
}//tabTwoStr
public function populateTabArray($o_current_user,&$a_licenses_not_upgrades_not_special){
	global $o_package_container;
	// get user information
    
	$access = $o_current_user->get_info_attr('access');        
	$uname = $o_current_user->get_info_attr('username');
	$d_id = $o_current_user->get_info_attr('id');
	//get account array and range information
	$accounts = $o_current_user->get_accounts();
	$accountsSerialized = base64_encode(serialize($accounts));
	$location_notes = $o_current_user->get_loc_notes();
	$location_contacts = $o_current_user->get_loc_contacts();

	if($uname=='distrotest'){
		//error_log('tabONE paramater2=='.print_r($a_licenses_not_upgrades_not_special));		
	}	
	$redirect = $o_current_user->get_info_attr('account_redirect');
	$user_email = $o_current_user->get_info_attr('email');
	
	// add content to tab_list
	$title ='<li class="tab li_accounts_tab" style="width:110px;"><a href="#accounts_tab">ACCOUNTS</a></li>';

	$content = <<<HTML
	<div id="accounts_tab" class="tab_section tab00">
		<div class="tab_top" style="height:70px;">
			<div class="tab_top_accnts">
				<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">MANAGE ACCOUNTS</div>
				<div id="accounts_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;">
					<div id="account_points" style="margin:5px;color:#b3b3b3;font:13px Helvetica;">&nbsp;</div>
					<div id="account_create_account" style="background-image:url(./images/btn_createaccount__.png);width:144px;height:22px;"></div>
				</div>
			</div>
			<div class="tab_top_border" style="position:relative; top:-17px"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
			<div id="admin_search_ui">
				<form action="#" id="account_filter_form" method="post">
					<input type="hidden" id="account_filter_distro_code" name="account_filter_distro_code" value="{$uname}"/>
					<input type="hidden" id="account_filter_distro_id" name="account_filter_distro_id" value="{$d_id}"/>
					<input type="hidden" id="account_filter_accountarray" name="account_serialized" />
					<input type="hidden" id="account_filter_distro_action" name="account_filter_distro_action" value="userAccountSearch"/>
					<div id="account_search_area" class="portal_search in_progress_search" style="display:inline-block;margin-left:60px;width:800px;height:30px;">
						<div id="account_search_required_data" dcode="{$uname}" daccess="{$access}" did="{$d_id}" style="display:none;"></div>
						<input type="text" id="account_searchInput" placeholder="Search Account Name, Dataname, or Email" name="accountSearch" style="height:24px;width:440px;font-size:20px;z-index:100;" />
						<div id="account_submit" style="width:65px;border:1px solid black;padding:6px;position:relative;float:right;right:53px;cursor:pointer;" onclick="filter_accountSearch();">SEARCH</div>
					</div>
				</form>
			</div>
			<div class="adjust_body" style="position:relative; top:25px;">
				<div id="accnts_desc">
					<span style="width:200px;text-align:left;margin-left:20px;">COMPANY</span>
					<span style="width:121px;">CREATED</span>
					<span style="width:191px;">GOOD UNTIL</span>
					<span style="width:192px;">LEVEL</span>
					<span style="width:75px;display:none;">LOGIN</span>
					<span style="width:20px;cursor:pointer;color:#aecd37;" class="view_reviews"><a style="color:#aecd37;" href="#" title="POS Lavu app reviews" class="tooltip"><span title="More">APP REVIEW</span></a></span>
					<span style="width:160px;cursor:pointer;color:#439789;" class="view_reviews"><a style="color:#aecd37;" href="#" title="Reseller reviews" class="tooltip"><span title="More" style="color:#439789;">YOUR REVIEW</span></a></span>
				</div>
				<div id="account_array">
				</div>
			</div>
		</div>
	</div>
HTML;
	return array('title'=>$title, 'content'=>$content);
}

//notes section in each account dropdown
private function get_account_notes($data_name, $ln, $def_name){

	//restaraunt_notes entries where support_ninja = DISTRO_NOTE
	$ds_notes = $ln[$data_name];

	$notes = '
	<div id="notes_'.$data_name.'" >
		<div class="acc_drop_top">
			<span style="float:left;margin-left:20px;">
				<img src="./images/btn_addnew.png" class="acc_add_detail acc_add_note" style="cursor:pointer;" alt="btn_anew"/>
				<span style="margin-left:5px;">Notes</span>
			</span>
			<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
				<span>Edit</span>
				<img src="./images/icon_edit.png" alt="i_edit">
			</span>
			<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
				<span>Done</span>
				<img src="./images/check_green.png" alt="ch_grn">
			</span>
		</div>';//acc_drop_top
    $notes .= '<div class="accnt_detail_wrapper">';
    if($ds_notes){

    	foreach($ds_notes as $date=>$message){
        	$notes .= '
        	<div class="loc_note accnt_detail accnt_view">
        		<span class="loc_note_date">'.$date.'</span><span class="loc_note_note">'.$message.'</span>
        	</div>';
        }
    	$id = 0;

        $notes .= '
        <div class="acc_edit_details loc_note_form">
        	<form id="note_form_'.$data_name.'" >';
        foreach($ds_notes as $date=>$message){
        $message = trim($message);
        	$notes .= '
        	<div class="acc_edit_detail">
        		<span class="loc_note_date">'.$date.'</span>
        		<label for="n_'.$data_name.'_'.$id.'">NOTE:</label>
        		<input type="text" class="acc_note_input" id="n_'.$data_name.'_'.$id.'" name="n_'.$data_name.'_'.$id.'" value="'.$message.'"/>
        	</div>';//account_edit_detail
        	$id++;
        }
        $notes .= '<input type="hidden" id="add_note_hidden" name="add_note_hidden" value="" /></form></div>';//note_form and acc_edit_detail

    }else{
	    $notes .= '<div class="loc_note accnt_detail">NO NOTES FOUND</div>';
    }
    $notes .= '
    <div class="loc_add_detail loc_add_note accnt_detail">
    	<form class="loc_add_note_form">
    		<label for="note_to_add'.$def_name.'">Note:</label>
    		<input class="note_to_add_'.$data_name.' acc_note_input" dataname="'.$data_name.'" id="note_to_add'.$def_name.'" name="note_to_add'.$def_name.'" value="" />
    		<div class="loc_add_detail_submit_cancel">
    			<span class="loc_cancel_add_detail">Canel</span>
    			<span class="loc_submit_add_detail submit_new_note" val="note_to_add'.$def_name.'">Submit</span>
    		</div>
    	</form>
    </div>';//loc_add_note
	$notes .= '</div></div>';//whole note div and accnt_detail_wrapper
	return $notes;

}//get_account_notes

//contact section in each account dropdown
private function get_account_contacts($data_name, $lc,$def_name){

	//restaraunt_notes entries where support_ninja = DISTRO_NOTE
	$ds_contacts = $lc[$data_name];

	$contacts = '<div id="contacts_'.$data_name.'" class="loc_contacts">
	<div class="acc_drop_top">
		<span style="float:left;margin-left:20px;">
			<img src="./images/btn_addnew.png" class="acc_add_detail acc_add_contact" alt="btn_a_n"/>
			<span>Contacts</span>
		</span>
		<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
			<span>Edit</span>
			<img src="./images/icon_edit.png" class="acc_edit_contact" alt="i_e" />
		</span>
		<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
				<span>Done</span>
				<img src="./images/check_green.png" alt="ch_grn" />
		</span>
	</div>
    ';//closes loc_con_top

    $contacts .= '<div class="accnt_detail_wrapper">';
    if($ds_contacts){

    	foreach($ds_contacts as $id){
    		$name = $id['firstname'].' '.$id['lastname'];
        	$contacts .=
        	'<div class="loc_contact accnt_detail accnt_view">
        		<span class="loc_contact_firstname">'.$id['firstname'].'</span>
        		<span class="loc_contact_lastname" style="margin-right:40px;">'.$id['lastname'].'</span>
        		<span class="loc_contact_phone" style="margin-right:40px;">'.$id['phone'].'</span>
        		<span class="loc_contact_email" style="margin-right:40px;">'.$id['email'].'</span>
        		<span class="loc_contact_note">'.$id['contact_note'].'</span>
        	</div>';
        }

        $contacts .= '
        <div class="acc_edit_details loc_contact_form">
        	<form id="contact_form_'.$data_name.'" >';
        $id_add = 0;
        foreach($ds_contacts as $id){
        	$fname = ($id['firstname'] != null)?trim($id['firstname']).'_'.$id_add:$def_name."_".$id_add;
        	$lname = ($id['lastname'] != null)?trim($id['lastname']).'_'.$id_add: $def_name."_".$id_add;
        	$phone = ($id['phone'] != null)?trim($id['phone']).'_'.$id_add:$def_name."_".$id_add;
        	$email = ($id['email'] != null)?trim($id['email']).'_'.$id_add:$def_name."_".$id_add;
        	$cnote = ($id['contact_note'] != null)?trim($id['contact_note']).'_'.$id_add:$def_name."_".$id_add;

        	$contacts .= '
        	<div class="acc_edit_detail">
        		<label for="fname_'.$fname.'">First Name:</label>
        		<input type="text" id="fname_'.$fname.'" name="fname_'.$fname.'" value="'.$fname.'"/>

        		<label for="lname_'.$lname.'">Last Name:</label>
        		<input type="text" id="lname_'.$lname.'" name="lname_'.$lname.'" value="'.$lname.'"/>

        		<label for="phone_'.$fname.'">Phone:</label>
        		<input type="text" id="phone_'.$fname.'" name="phone_'.$fname.'" value="'.$phone.'"/>

        		<label for="email_'.$fname.'">Email:</label>
        		<input type="text" id="email_'.$fname.'" name="email_'.$fname.'" value="'.$email.'"/>

        		<label for="cnote_'.$fname.'">Note:</label>
        		<input type="text" id="cnote_'.$fname.'" name="cnote_'.$fname.'" value="'.$cnote.'"/>
        	</div>';//account_edit_detail
        	$id_add++;
        }
        $contacts .= '</form></div>';//note_form and acc_edit_detail
    }else{
	        $contacts .= '<div class="loc_note accnt_detail">NO CONTACTS FOUND</div>';
    }
	$contacts .= '
    <div class="loc_add_detail loc_add_contact accnt_detail">
    	<form class="loc_add_contact_form">
    		<label for="add_fname_'.$data_name.'">First Name:</label>
        	<input type="text" id="add_fname_'.$data_name.'" name="add_fname_'.$data_name.'" value=""/>
        	<label for="add_lname_'.$data_name.'">Last Name:</label>
        	<input type="text" id="add_lname_'.$data_name.'" name="add_lname_'.$data_name.'" value=""/>
        	<label for="add_phone_'.$data_name.'">Phone:</label>
        	<input type="text" id="add_phone_'.$data_name.'" name="add_phone_'.$data_name.'" value=""/>
        	<label for="add_email_'.$data_name.'">Email:</label>
        	<input type="text" id="add_email_'.$data_name.'" name="add_email_'.$data_name.'" value=""/>
        	<label for="add_cnote_'.$data_name.'">Note:</label>
        	<input type="text" id="add_cnote_'.$data_name.'" name="add_cnote_'.$data_name.'" value=""/>
    		<div class="loc_add_detail_submit_cancel">
    			<span class="loc_cancel_add_detail">Cancel</span>
    			<span class="loc_submit_add_detail submit_new_contact">Submit</span>
    		</div>
    	</form>
    </div>';//loc_add_contact
	$contacts .= '</div></div>';//closes loc_contacts and accnt_detail_wrapper
	return $contacts;

}//get_account_contacts

//location section in each account dropdown
private function get_account_location($loc_deets){
	global $o_package_container;

	$address = ($loc_deets['address'] != null)?trim($loc_deets['address']):"";
	$city = ($loc_deets['city'] != null)?trim($loc_deets['city']):"";
	$state = ($loc_deets['state'] != null)?trim($loc_deets['state']):"";
	$zip = ($loc_deets['zip'] != null)?trim($loc_deets['zip']):"";
	$loc = '
	<div class=loc_loc>
		<div class="acc_drop_top">
			<span style="float:left;margin-left:20px;">
				<span>Location</span>
			</span>
			<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
				<span>Edit</span>
				<img src="./images/icon_edit.png" class="acc_edit_location" alt="icon_edit" />
			</span>
			<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
				<span>Done</span>
				<img src="./images/check_green.png" alt="check_green" />
			</span>
		</div>';//acc_drop_top
	$loc .= '
	<div class="accnt_detail_wrapper">
		<div class="accnt_view">
			<div style="text-align:left;width:710px;margin:3px auto;font:10px Helvetica;color:#717074;letter-spacing:0.075em;">
				<div style="font:10px Verdana;">Address: '.$address.'</div>
				<div style="font:10px Verdana;"><span>'.$city.', </span><span>'.$state.', </span><span>'.$zip.'</span></div>
			</div>
		</div>';//accnt_view
    $loc .= '
        <div class="acc_edit_details loc_loc_form">
        	<form id="loc_form_'.$loc_deets['data_name'].'" >
        	<div class="acc_edit_detail">
        		<label for="loc_cont_address_'.$loc_deets['data_name'].'">Address:</label>
        		<input type="text" id="loc_cont_address_'.$loc_deets['data_name'].'" name="loc_cont_address_'.$loc_deets['data_name'].'" value="'.$address.'"/>
        	</div>
        	<div class="acc_edit_detail">
        		<label for="loc_cont_city_'.$loc_deets['data_name'].'">City:</label>
        		<input type="text" id="loc_cont_city_'.$loc_deets['data_name'].'" name="loc_cont_city_'.$loc_deets['data_name'].'" value="'.$city.'"/>
        	</div>
        	<div class="acc_edit_detail">
        		<label for="loc_cont_state_'.$loc_deets['data_name'].'">State:</label>
        		<input type="text" id="loc_cont_state_'.$loc_deets['data_name'].'" name="loc_cont_state_'.$loc_deets['data_name'].'" value="'.$state.'"/>
        	</div>
        	<div class="acc_edit_detail">
        		<label for="loc_cont_zip_'.$loc_deets['data_name'].'">Zip:</label>
        		<input type="text" id="loc_cont_zip_'.$loc_deets['data_name'].'" name="loc_cont_zip_'.$loc_deets['data_name'].'" value="'.$zip.'"/>
        	</div>';//account_edit_detail
        $loc .= '</form></div>';//loc_loc_form and acc_edit_details

	$loc .= '</div></div>';//closes accnt_detail and loc_loc
	return $loc;
}//get_account_locations
//returns the markup for the new account form
}
?>
