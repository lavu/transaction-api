<?php

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

$action = $_REQUEST['action'];
$item = $_REQUEST['item'];

switch ( $action .'-'. $item )
{
	case 'get-address':
		$dataname = $_REQUEST['dataname'];
		$distro_code = $_REQUEST['distro_code'];
		$address = getAddressForDataname($dataname, $distro_code);
		echo json_encode($address);
		break;

	default:
		error_log(__FILE__ ." got unmapped mode=");
		break;
}
exit;


// Functions

function getAddressForDataname($dataname, $distro_code)
{
	$address = array(
		'dataname' => $dataname,
		'distro_code' => $distro_code,
		'restaurantid' => '',
		'company' => '',
		'firstname' => '',
		'lastname' => '',
		'address' => '',
		'city' => '',
		'state' => '',
		'zip' => '',
		'country' => '',
		'phone' => '',
		'email' => '',
	);

	//error_log("DEBUG: dataname={$dataname} distro_code={$distro_code}");  //debug

	if ( $dataname && $distro_code ) {
		// First check that the dataname belongs to the provided distro_code.
		$restaurant_query = mlavu_query("SELECT `id`, `data_name`, `distro_code` FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[dataname]' AND `distro_code`='[distro_code]'", $address);
		if ($restaurant_query === false) error_log(__FILE__ ." got MySQL error for dataname={$dataname} distro_code={$distro_code}: ". mlavu_dberror());
		if ( mysqli_num_rows($restaurant_query) ) {
			$restaurant = mysqli_fetch_assoc($restaurant_query);
			$address['restaurantid'] = $restaurant['id'];
			// Try getting the address fields from the signup record first.
			$signup_query = mlavu_query("SELECT `company`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `phone`, `email` FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[dataname]'", $address);
			if ($signup_query === false) error_log(__FILE__ ." got MySQL error for distro_code={$distro_code} reseller_quote_id={$reseller_quote_id}: ". mlavu_dberror());
			if ( mysqli_num_rows($signup_query) ) {
				$signup = mysqli_fetch_assoc($signup_query);
				foreach ( $signup as $field => $val ) {
					if (!empty($signup[$field])) $address[$field] = $signup[$field];
				}
			}
			else {
				// Only attempt to get the address fields from the reseller_leads record if the signup record doesn't exist.
				$reseller_lead_query = mlavu_query("SELECT `company_name` AS `company`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `phone`, `email`  FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `data_name`='[dataname]'", $address);
				if ($reseller_lead_query === false) error_log(__FILE__ ." got MySQL error for distro_code={$distro_code} reseller_quote_id={$reseller_quote_id}: ". mlavu_dberror());
				$reseller_lead = mysqli_fetch_assoc($reseller_lead_query);
				foreach ( $reseller_lead as $field => $val ) {
					if (!empty($reseller_lead[$field])) $address[$field] = $reseller_lead[$field];
				}
			}
		} 
	}

	//error_log(__FILE__ ." DEBUG: address=". print_r($address, true));  //debug

	return $address;
}
