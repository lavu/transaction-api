<?php

ini_set("display_errors", 1);
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");

// We have to do the weird utf8 => base64 to decode the raw binary data POST'd via ajax that Martin developed for the resource uploads.
$raw_pdf_contents = utf8_decode($_POST['filedata']);
$base64_break = 'data:'. $_POST['filetype'] .';base64,';
$base64data = str_replace($base64_break, '', $raw_pdf_contents);
$decoded_pdf = base64_decode($base64data);

$distro_code = $_REQUEST['distro_code'];
$reseller_quote_id = $_REQUEST['reseller_quote_id'];

$filename = "Resources_new/quotes/{$distro_code}_{$reseller_quote_id}.pdf";
//$filename = "/tmp/{$distro_code}_{$reseller_quote_id}.pdf";
$wrote_file = file_put_contents("../{$filename}", $decoded_pdf);

if ( $wrote_file === false ) {
	echo 'failure|Could not save PDF to server';
	return;
}

mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_quotes` SET `quote`='[filename]', `status`='approved' WHERE `id`='[reseller_quote_id]' AND `distro_code`='[distro_code]' LIMIT 1", array('filename' => $filename, 'reseller_quote_id' => $reseller_quote_id, 'distro_code' => $distro_code));
if ( ConnectionHub::getConn('poslavu')->affectedRows() < 1 ) {
	echo 'failure|Database error '. mlavu_dberror();
}
else {
	echo 'success|PDF added successfully';
}
