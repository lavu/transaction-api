<?php
	if (getenv('DEV') == '1') {
		@define('DEV', 1);
		ini_set("display_errors","1");
		error_reporting(error_reporting() ^ E_NOTICE ^ E_DEPRECATED ^ E_WARNING);
	} else {
		@define('DEV', 0);
	}

	session_start();
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once(dirname(__file__)."/../distro_user.php");
	require_once("/home/poslavu/public_html/register/authnet_functions.php");
	require_once(dirname(__file__)."/distro_user_functions.php");
	require_once(dirname(__file__)."/../distro_data_functions.php");
	require_once("/home/poslavu/public_html/admin/cp/areas/billing.php");
	require_once(dirname(__file__)."/../promo_codes.php");

	// variables used
	$show_message = "";
	$card_info = array();
	$license_cost = 0;
	$license_value = 0;
	$license_info = array();
	$license_name = "";
	$loggedin = "";
	$customer_info = array();
	$license_types = array();

	// get the session variables
	$user = unserialize($_SESSION['distro_user']);
	$license_types = unserialize($_SESSION['license_types']);
	// parse the session variables
	$loggedin = $user->get_info_attr('username');
	$customer_info = $user->get_info();
	$user_access = $customer_info['access'];

	// get the post variables
	$license_type = $_POST['license_license'];
	$card_info['number'] = $_POST['card_number'];
	$card_info['expiration'] = $_POST['license_card_exp_m'].$_POST['license_card_exp_y'];
	$card_info['code'] = $_POST['card_ccv'];
	$s_special_license_id = isset($_POST['special_license_selection_radio']) ? $_POST['special_license_selection_radio'] : '0';

	// parse the post variables
	$license_info = explode("-",$license_type);
	$license_name = trim($license_info[0]);
	$license_seconary_info = trim($license_info[1]);
	$new_license_name = $license_name;

	// Validate the promo code, if they've entered one.
	if ( !empty($_REQUEST['promo_code']) && !check_promo_code($_REQUEST['promo_code'], $license_name, $license_discount) ) {
		error_and_exit("failure|Error: The promo code you entered is invalid or has already been used.", $loggedin);
	}


	// check that the license and the associated cost match one of the costs in the database
	for($i=0; $i<count($license_types); $i++)
	{
		if(strtolower($license_name)==strtolower($license_types[$i][0]))
		{
			$license_cost = $license_types[$i][1];
			$license_value = $license_types[$i][2];
			$s_promo_name = $license_types[$i][3];
			$s_promo_expiration = $license_types[$i][4];

			if ( $s_promo_name !== '' && strpos($license_name, '<*>'.$s_promo_name) == (strlen($license_name)-strlen('<*>'.$s_promo_name)) )
				$new_license_name = substr($license_name, 0, strlen($license_name)-strlen('<*>'.$s_promo_name));

			// check for special license pricing
			if ($s_special_license_id !== '0') {
				$o_special_licenses = $user->get_special_licenses();
				if (!isset($o_special_licenses->$s_special_license_id)) {
					error_and_exit("failure|Error: that special license pricing could not be found.", $loggedin);
				}
				$license_cost = (float)$license_cost * (100.0 - (float)$o_special_licenses->$s_special_license_id->discount_percentage) / 100.00;
				$license_value = (float)$license_value * (100.0 - (float)$o_special_licenses->$s_special_license_id->discount_percentage) / 100.00;
				$s_bought = date("Y-m-d H:i:s");
				$o_update = new stdClass();
				$o_update->action = 'bought license';
				$o_update->username = $loggedin;
				$o_update->usertype = DISTRIBUTOR;
				$o_special_licenses->$s_special_license_id->history->updated->$s_bought = $o_update;
				$o_special_licenses->$s_special_license_id->quantity = (string)((int)$o_special_licenses->$s_special_license_id->quantity - 1);
				$user->save_special_licenses($o_special_licenses);
			}
		}
	}
	$license_name = $new_license_name;

	// check for custom purchases (buy-ins, reseller conference costs, etc)
	$b_custom_purchase = FALSE;
	if (($license_cost <= 0 || $license_value <= 0) && $license_name == "Custom Purchase") {

		// check if the reported custom purchase belongs to this distributor
		require_once(dirname(__FILE__)."/../../../manage/misc/AdvancedToolsFunctions/distro_tools/custom_purchases.php");
		$o_distroToolsCustomPurchases->set_permission(TRUE);
		$a_custom_purchases = $o_distroToolsCustomPurchases->get_purchases($loggedin, TRUE, FALSE); // error_log
		error_log(print_r($a_custom_purchases,TRUE));
		error_log($license_seconary_info);
		$b_apply_commission = FALSE;
		foreach($a_custom_purchases['available'] as $a_custom_purchase) {
			if ((int)$license_seconary_info == (int)$a_custom_purchase['id'] && (int)$license_seconary_info > 0) {
				$b_custom_purchase = TRUE;
				$i_custom_purchase_id = (int)$a_custom_purchase['id'];
				$s_custom_purchase_name = $a_custom_purchase['name'];
				$license_cost = (int)$a_custom_purchase['amount'];
				$license_value = $license_cost;
				$b_apply_commission = ((int)$a_custom_purchase['apply_commission'] == 1) ? TRUE : FALSE;
			}
		}

		// apply the distributor's commission to the license cost, if appropriate
		if ($b_apply_commission) {
			$lpercent = DistroSessionStats::getLpercent();
			$license_cost = $license_cost * (100-$lpercent)/100;
		}

		// check if the custom purchase was found
		if (!$b_custom_purchase) {
			error_and_exit("failure|The purchase could not be completed. The purchase type can not be found.", $loggedin);
		}
	}

	$is_no_cost_license = ($license_name == 'Lavu' || strpos($license_name, 'Lavu88') !== false) && (strpos($license_name, ' to ') === false);  // should catch Lavu and all flavors of Lavu 88 (Lavu88, MercuryFreeLavu88, etc.) that aren't upgrade licenses
	$is_lavu_inside_sales = ($loggedin == 'lavusales505');

	// get the license input
	if( empty($license_name) || (($license_cost <= 0 || $license_value <= 0) && !$is_no_cost_license && !$is_lavu_inside_sales) )
	{   //error_log('distro/beta/exec/buy_license #114::'.$license_name);
		error_and_exit("failure|Please choose a valid license type.", $loggedin);
	}

	// Check promo code and adjust license cost, if applicable.
	$promo_code_accepted = 0;
	if ( !empty($_REQUEST['promo_code']) ) {
		$promo_code_accepted = process_promo_code($_REQUEST['promo_code'], $loggedin, $license_name, $license_cost);
	}

	// try to purchase a license of the given value
	$collect_amount = $license_cost;
	$b_using_testing_credit_card_credentials = FALSE;
	if($card_info['number']=="8484" && $card_info['expiration']=="0213" && $card_info['code']=="8484" && ($_SERVER['REMOTE_ADDR']=="74.95.18.90" || $_SERVER['REMOTE_ADDR']=='173.10.247.197'))
	{
		$auth_result = array();
		$auth_result['success'] = true;
		$auth_result['response'] = "Test Mode";
		$b_using_testing_credit_card_credentials = TRUE;
	}else if($user_access == "ingram")
	{
		$auth_result = array();
		$auth_result['success'] = true;
		$auth_result['response'] = "";
		//error_log( "DEBUG: Performing ingram micro cc auth for loggedin={$loggedin} user_access={$user_access}" );
	}else if($is_no_cost_license || $is_lavu_inside_sales){
		$auth_result = array();
		$auth_result['success'] = true;
		$auth_result['response'] = "";
	}else{
		$auth_result = authorize_card_for_transaction("charge","DS_" . $loggedin,"DS_" . $loggedin,$collect_amount,$card_info,$customer_info);
	}
	if(!$auth_result['success'])
	{
		// On failed credit card transactions, we need to free up accepted promo codes we just marked as taken since
		// the user may fix their credit card info and re-submit the same form information with the same promo code.
		if ( $promo_code_accepted ) {
			liberate_promo_code($_REQUEST['promo_code'], $loggedin);
		}

		error_and_exit("failure|Error: ".$auth_result['response'], $loggedin);
	}

	if ($b_custom_purchase) {

		// mark the custom purchase in the reseller_custom_purchases table
		$o_distroToolsCustomPurchases->make_purchase($i_custom_purchase_id, "Purchased through distributo portal for {$collect_amount}".($b_using_testing_credit_card_credentials ? ", using testing CC credentials" : ""), -1, $collect_amount);
		add_to_distro_history("bought custom purchase","type: $license_name\ncollected: $collect_amount\nname: $s_custom_purchase_name",$vars['resellerid'],$vars['resellername']);
	} else {
		$notes = ($s_special_license_id == '0' ? '' : 'special license id '.$s_special_license_id);
		$notes .= isset($_REQUEST['notes']) ? 'LAVU INTERNAL PURCHASE, distro commission 100 '. $_REQUEST['notes'] : '';

		// add the license to the reseller_licenses table
		$vars = array();
		$vars['type'] = $license_name;
		$vars['resellerid'] = $customer_info['id'];
		$vars['resellername'] = $loggedin;
		$vars['restaurantid'] = "";
		$vars['cost'] = $collect_amount;
		$vars['value'] = $license_value;
		$vars['purchased'] = date("Y-m-d H:i:s");
		$vars['applied'] = "";
		$vars['notes'] = $notes;
		$vars['promo_name'] = $s_promo_name;
		$vars['promo_expiration'] = $s_promo_expiration;
		$vars['cmsn'] = ($license_name == 'LavuEnterprise') ? '998' : '';
		mlavu_query("insert into `poslavu_MAIN_db`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`restaurantid`,`cost`,`value`,`purchased`,`applied`,`notes`,`promo_name`,`promo_expiration`,`cmsn`) values ('[type]','[resellerid]','[resellername]','[restaurantid]','[cost]','[value]','[purchased]','[applied]','[notes]','[promo_name]','[promo_expiration]','[cmsn]')",$vars);
		add_to_distro_history("bought license","type: $license_name\ncollected: $collect_amount\nvalue: $license_value\ncmsn: {$vars['cmsn']}",$vars['resellerid'],$vars['resellername']);
	}

	$show_form = false;
	if($user_access == 'ingram' || $is_no_cost_license || $is_lavu_inside_sales){
		echo "success|Your license request has been approved.<br />You must reload the page to see the effects of your purchase.</font><br><br>";
	}
	else if ($b_custom_purchase) {
		echo "success|Your Payment of $" . number_format($collect_amount * 1,2). " has been processed.<br />You must reload the page to see the effects of your purchase.</font><br><br>";
	} else {
		echo "success|Your Payment of $" . number_format($collect_amount * 1,2). " has been processed.<br />You must reload the page to see the new license.</font><br><br>";
	}

	function error_and_exit($msg, $loggedin) {
		error_log($msg);
		echo $msg;
		exit();
	}

?>
