<?php
/* An ajax file for updating the status of a lead
 * 
 * author: Martin and Ben
 */

ini_set("display_errors",1);
session_start();
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");

//var_dump($_POST);
$i_lead_id = (isset($_POST['leadid']))?$_POST['leadid']:'';
$s_lead_dataname = (isset($_POST['leaddataname']))?$_POST['leaddataname']:'';
$s_distro_code = $_POST['username'];
$s_progress = $_POST['progress'];

// accept/decline
if($s_progress == 'lead_accepted'){
	$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `accepted`='[1]' WHERE `id`='[2]'";
	
	mlavu_query($s_query_string, date("Y-m-d H:i:s"), $i_lead_id);
	if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
		echo 'reload|Lead Accepted';
	else
		echo 'alert|Lead id '.$i_lead_id.' not recognized/updated. Please try again or contact support.';
}
if($s_progress == 'lead_declined'){
	$s_query_string = "SELECT `declined_by` FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `id`='[2]'";
	$s_query = mlavu_query($s_query, $i_lead_id);
	$a_new_declined_by = array($s_distro_code);
	if ($s_query){
		if ($a_read = mysqli_fetch_assoc($s_query)) {
			$s_declined_by = $a_read['declined_by'];
			$a_new_declined_by = explode('|*distro_code*|');
			if (!in_array($s_distro_code, $a_new_declined_by))
				$a_new_declined_by[] = $s_distro_code;
		}
	}
	$s_new_declined_by = implode('|*distro_code*|',$a_new_declined_by);
	
	$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `_declined`='1',`declined_by`='[1]',`distro_code`='' WHERE `id`='[2]'";
	mlavu_query($s_query_string, $s_new_declined_by, $i_lead_id);
	$to = //'martin@poslavu.com';
	$subject = "LEAD DECLINED";
	$message = "A distributer declined a lead. username = ".$s_distro_code;
	mail($to, $subject, $message);
	if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
		echo 'reload|Lead Declined';
	else
		echo 'alert|Lead id '.$i_lead_id.' or disto '.$s_new_declined_by.' not recognized/updated. Please try again or contact support.';
} 

// contact
if($s_progress == 'contacted'){
	$s_comp_name = $_POST['company_name'];
	$s_contact_note = $_POST['contact_comment'];
	if($s_contact_note == 'any comments here'){
		$s_contact_note = 'contacted by: '.$_POST['contact_method'];
	}else{
		$s_contact_note = 'contacted by: '.$_POST['contact_method'].', note: '.$s_contact_note;
	}
	$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `contacted`='[1]', `contact_note`='[3]' WHERE `company_name`='[2]'";
	mlavu_query($s_query_string, date("Y-m-d H:i:s"), $s_comp_name, $s_contact_note);
	if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
		echo 'reload|Lead Contacted';
	else
		echo 'alert|fail. not recognized/updated. Please try again or contact support.';
}

// create demo account
if($s_progress == 'demo_created'){
	$s_data_name = $_POST['dataname'];
	$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `made_demo_account`='[1]',`data_name`='[2]' WHERE `company_name`='[3]'";
	mlavu_query($s_query_string, date("Y-m-d H:i:s"), $s_data_name, $i_lead_id);
	if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
		echo 'reload|Demo Created';
	else
		echo 'alert|Account '.$s_data_name.' or lead '.$i_lead_id.' not recognized/updated. Please try again or contact support.';
}

// apply a license
if($s_progress == 'license_applied'){
	if ($i_lead_id == '' && $s_lead_dataname != '') {
		$i_lead_id_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `data_name`='[1]'",$s_lead_dataname);
		if ($i_lead_id_query) {
			if ($i_lead_id_read = mysqli_fetch_assoc($i_lead_id_query)) {
				$i_lead_id = $i_lead_id_read['id'];
			}
		}
	}
	if ($i_lead_id != '') {
		$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `license_applied`='[1]',`_finished`='1' WHERE `id`='[2]'";
		mlavu_query($s_query_string, date("Y-m-d H:i:s"), $i_lead_id);
		if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
			echo 'reload|License applied';
		else
			echo 'alert|Lead '.$i_lead_id.' not recognized/updated. Please try again or contact support.';
	} else {
		echo 'reload|Not a lead';
	}
}
/*if(applied_license){
	
	`payment_status`.`lead_status` = ''
}
*/