<?php

require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');

$action = $_REQUEST['action'];
$item = $_REQUEST['item'];
$mode = $action .'-'. $item;

switch ( $mode )
{
	case 'get-reseller':
		$distro_code = $_REQUEST['distro_code'];
		$reseller = getReseller($distro_code);
		echo json_encode($reseller);
		break;

	case 'get-reseller_quote':
		$distro_code = $_REQUEST['distro_code'];
		$reseller_quote_id = $_REQUEST['reseller_quote_id'];
		$quote = getResellerQuote($distro_code, $reseller_quote_id);
		$line_items = getResellerQuotesLineitems($distro_code, $reseller_quote_id);
		$quote['lineitems'] = $line_items;
		echo json_encode($quote);
		break;

	case 'get-reseller_quotes_lineitems':
		$distro_code = $_REQUEST['distro_code'];
		$reseller_quote_id = $_REQUEST['reseller_quote_id'];
		$line_items = getResellerQuotesLineitems($distro_code, $reseller_quote_id);
		echo json_encode($line_items);
		break;

	case 'set-quote_order':
		$distro_code = $_REQUEST['distro_code'];
		$reseller_quote_id = $_REQUEST['reseller_quote_id'];
		$status = $_REQUEST['status'];
		$result = setQuoteAttributes($distro_code, $reseller_quote_id, array('status' => $status));
		if ($result['success']) $result['payment_link'] = createPaymentRequest($distro_code, $reseller_quote_id);
		echo json_encode($result);
		break;

	case 'set-pdf_and_payment_request':
		$distro_code = $_REQUEST['distro_code'];
		$reseller_quote_id = $_REQUEST['reseller_quote_id'];
		$file_data = $_REQUEST['filedata'];
		$file_type = $_REQUEST['filetype'];
		$dataname = $_REQUEST['dataname'];
		$restaurantid = $_REQUEST['restaurantid'];
		$signupid = $_REQUEST['signupid'];
		$result = uploadQuotePdf($distro_code, $reseller_quote_id, $file_data, $file_type, $dataname, $restaurantid, $signupid);
		if ($result['success']) {
			$result['payment_link'] = createPaymentRequest($distro_code, $reseller_quote_id);
			if (empty($result['payment_link'])) {
				$result['success'] = false;
				$result['error'] = "Could not create payment link";
			}
		}
 		echo json_encode($result);
		break;

	default:
		error_log(__FILE__ ." got unmapped mode={$mode}");
		break;
}
exit;


// Functions

function getReseller($distro_code) {
	$reseller = array();

	if ( $distro_code ) {
		$q = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`resellers` WHERE `username`='[1]'", $distro_code);
		if ($q === false) error_log(__FILE__ ." got MySQL error for distro_code={$distro_code}: ". mlavu_dberror());
		$reseller = mysqli_fetch_assoc($q);

		if ( !empty($reseller['demo_account_dataname']) ) {
			$rl = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `data_name`='[1]'", $reseller['demo_account_dataname']);
			if ($rl === false) error_log(__FILE__ ." got MySQL error for distro_code={$distro_code}: ". mlavu_dberror());
			$reseller_lead = mysqli_fetch_assoc($rl);

			$r = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`restaurants` WHERE `data_name`='[1]'", $reseller['demo_account_dataname']);
			if ($r === false) error_log(__FILE__ ." got MySQL error for distro_code={$distro_code}: ". mlavu_dberror());
			$restaurant = mysqli_fetch_assoc($r);

			$s = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`signups` WHERE `dataname`='[1]'", $reseller['demo_account_dataname']);
			if ($s === false) error_log(__FILE__ ." got MySQL error for distro_code={$distro_code}: ". mlavu_dberror());
			$signup = mysqli_fetch_assoc($s);
		}

		$reseller['reseller_lead_id'] = isset($reseller_lead['id']) ? $reseller_lead['id'] : '';
		$reseller['restaurantid'] = isset($restaurant['id']) ? $restaurant['id'] : '';
		$reseller['signupid'] = isset($signup['id']) ? $signup['id'] : '';
		$reseller['salesforce_id'] = isset($signup['salesforce_id']) ? $signup['salesforce_id'] : '';
		$reseller['salesforce_opportunity_id'] = isset($signup['salesforce_opportunity_id']) ? $signup['salesforce_opportunity_id'] : '';
	}

	return $reseller;
}

function getResellerQuote($distro_code, $reseller_quote_id)
{

	$quote = array();

	if ( $distro_code && $reseller_quote_id ) {
		$q = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_quotes` WHERE `distro_code`='[1]' AND `id`='[2]'", $distro_code, $reseller_quote_id);
		if ($q === false) error_log(__FILE__ ." got MySQL error for distro_code={$distro_code} reseller_quote_id={$reseller_quote_id}: ". mlavu_dberror());
		$quote = mysqli_fetch_assoc($q);
	}

	return $quote;
}

function getResellerQuotesLineitems($distro_code, $reseller_quote_id)
{

	$lineitems = array();

	if ( $distro_code && $reseller_quote_id ) {
		$result = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_quotes_lineitems` WHERE `distro_code`='[1]' AND `reseller_quote_id`='[2]' AND `_deleted`!='1'", $distro_code, $reseller_quote_id);
		if ($result === false) error_log(__FILE__ ." got MySQL error for distro_code={$distro_code} reseller_quote_id={$reseller_quote_id}: ". mlavu_dberror());
		while ( $lineitem = mysqli_fetch_assoc($result) ) {
			$lineitems[] = $lineitem;
		}

		if (mysqli_num_rows($result) < 1) {
			$lineitems[]['product'] = '(None)';
		}
	}
	else {
		$lineitems[]['product'] = '(Error loading quote)';
	}

	return $lineitems;
}

function setQuoteAttributes($distro_code, $reseller_quote_id, $attributes) {

	$success = false;

	if ( $distro_code && $reseller_quote_id ) {

		$update_clause = '';
		foreach ( $attributes as $key => $val ) {
			$update_clause .= empty($update_clause) ? "`{$key}`='[$key]'" : ", `{$key}`='[$key]'";
		}

		$attributes['distro_code'] = $distro_code;
		$attributes['reseller_quote_id'] = $reseller_quote_id;

		$q = mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_quotes` SET {$update_clause} WHERE `distro_code`='[distro_code]' AND `id`='[reseller_quote_id]'", $attributes);
		if ($q === false) error_log(__FILE__ ." got MySQL error: ". mlavu_dberror());

		$success = ($q !== false && ConnectionHub::getConn('poslavu')->affectedRows());
	}

	return array('success' => $success);
}

function createPaymentRequest($distro_code, $reseller_quote_id)
{
	$quote = getResellerQuote($distro_code, $reseller_quote_id);
	$reseller = getReseller($distro_code);

	if ( empty($quote['payment_link']) ) {
		$quote['reseller_quote_id'] = $reseller_quote_id;
		$quote['action'] = 'Zuora Quote Payment';
		$quote['payment_notes'] = "Payment for quote \"{$input['quote_name']}\"";
		$quote['key'] = rand(11111,99999) . rand(11111,99999) . rand(11111,99999);
		$quote['notify_email'] = $reseller['email'];
		$quote['shipping'] = '';
		$quote['payment_notes'] = "For quote '{$quote['quote_name']}'";
		$quote['createuser'] = $distro_code;

		mlavu_query("INSERT INTO `poslavu_MAIN_db`.`payment_request` (`createtime`,`createuser`,`email`,`dataname`,`restaurantid`,`signupid`,`reseller_quote_id`,`salesforce_quote_id`,`quote_pdf_filename`,`hardware_amount`,`license_amount`,`hosting_amount`,`package_level`,`points_amount`,`action`,`notes`,`key`,`notify_email`,`shipping`) VALUES (now(), '[createuser]', '[email]','[dataname]','[restaurantid]','[signupid]','[reseller_quote_id]','[salesforce_quote_id]','[quote_name]','[hardware_amount]','[license_amount]','[hosting_amount]','[package_level]','[points_amount]','[action]','[payment_notes]','[key]','[notify_email]','[shipping]')", $quote);
		if (mlavu_dberror()) error_log(__FILE__ ." got MySQL error for payment request for reseller_quote_id={$input['reseller_quote_id']}: ". mlavu_dberror());

		$register_url = 'https://register.poslavu.com'; 
		$payment_link = "{$register_url}/make_payment/?key={$quote['key']}&session=".session_id();
		$quote['payment_link'] = $payment_link;
		mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_quotes` SET `payment_link`='[payment_link]' WHERE `id`='[id]' AND `distro_code`='[distro_code]' AND `payment_link`=''", $quote);
		$updated_rows = ConnectionHub::getConn('poslavu')->affectedRows();

		//error_log("DEBUG: payment_link={$quote['payment_link']} updated_db={$updated_rows} id={$quote['id']} distro_code={$quote['distro_code']} mlavu_dberror=". mlavu_dberror());  //debug
	}
	else {
		$payment_link = $quote['payment_link'];
	}

	return $payment_link;
}

/**
 * We have to do the weird utf8 => base64 to decode the raw binary data POST'd via ajax that Martin developed for the resource uploads.
*/
function uploadQuotePdf($distro_code, $reseller_quote_id, $file_data, $file_type, $dataname, $restaurantid, $signupid)
{
	$results = array('success' => false);

	// We have to do the weird utf8 => base64 to decode the raw binary data POST'd via ajax that Martin developed for the resource uploads.
	$raw_pdf_contents = utf8_decode($file_data);
	$base64_break = "data:{$file_type};base64,";
	$base64data = str_replace($base64_break, '', $raw_pdf_contents);
	$decoded_pdf = base64_decode($base64data);

	$filename = "Resources_new/quotes/{$distro_code}_{$reseller_quote_id}.pdf";
	$wrote_file = file_put_contents("../{$filename}", $decoded_pdf);

	if ( $wrote_file === false ) {
		echo 'failure|Could not save PDF to server';
		return;
	}

	mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_quotes` SET `status`='approved', `pdf_file`='[filename]', `dataname`='[dataname]', `restaurantid`='[restaurantid]', `signupid`='[signupid]' WHERE `id`='[reseller_quote_id]' AND `distro_code`='[distro_code]' LIMIT 1", array('filename' => $filename, 'reseller_quote_id' => $reseller_quote_id, 'distro_code' => $distro_code, 'dataname' => $dataname, 'restaurantid' => $restaurantid, 'signupid' => $signupid));

	if ( ConnectionHub::getConn('poslavu')->affectedRows() < 1 ) {
		$msg = "Database error: ". mlavu_dberror();
		error_log(__FILE__ ." ". $msg);
		$results['success'] = false;
		$results['error'] = $msg;
	}
	else {
		$results['success'] = true;
		$results['filename'] = $filename;
	}

	return $results;
}
