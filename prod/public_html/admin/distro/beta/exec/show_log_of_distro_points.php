<?php
	$dont_actually_load_index = TRUE;
	ob_start();
	require_once(dirname(__FILE__).'/../index.php');
	$s_trash = ob_get_contents();
	ob_end_clean();
	//error_log('show_log_d_points post -'.isset($_POST['admin']).'-');
	if (!account_loggedin()) {
		if(!isset($_POST['admin'])){
			echo 'print error[*note*]You must be logged in to view this page.';
			exit();
		}
	}
	
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once(dirname(__FILE__)."/distro_user_functions.php");
	require_once(dirname(__FILE__)."/../distro_data_functions.php");
	
	if (!isset($maindb) || $maindb == '') $maindb = 'poslavu_MAIN_db';
	
	if (!function_exists('postvars')) {
		function postvars($s_name, $wt_default = FALSE) {
			return (isset($_POST[$s_name]) ? $_POST[$s_name] : $wt_default);
		}
	}
	
	$s_distro_code = postvars('distro_code');
	$i_width = (int)postvars('width');
	$i_height = (int)postvars('height');
	$is_admin = postvars('admin');
	
	// get the distro
	$a_resellers = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('resellers', array('username'=>$s_distro_code), TRUE);
	if ($s_distro_code === FALSE || trim($s_distro_code) == '' || count($a_resellers) == 0) {
		echo 'print error[*note*]The distributor "'.$s_distro_code.'" couldn\'t be found.';
		exit();
	}
	if ($s_distro_code !== account_loggedin() && (!isset($_SESSION['distro_logged_in_is_admin']) || $_SESSION['distro_logged_in_is_admin'] == FALSE) && !isset($_POST['admin'])) {
		echo 'print error[*note*]You don\'t have access to this account.';
		exit();
	}
	$a_reseller = $a_resellers[0];
	
	// get their points log and create a record in the distro history
	$a_points_log = get_distro_points_explained($s_distro_code);
	add_to_distro_history('User Checked Distro Points History','The user checked their distro points history. Current points: '.$a_reseller['credits'].', Predicted points: '.$a_points_log['predicted_points'],$a_reseller['id'],$s_distro_code,TRUE);
	
	// draw the points log
	$message = '[*note*]';
	$message .= '
	<style type="text/css">
		.distro_points_explanation_category {
			font-weight: bold;
		}
		.distro_points_explanation_item {
		}
	</style>
	<div style="width:'.$i_width.'px; height:'.$i_height.'px; overflow-y:auto; overflow-x:auto; border:0px none clear;">
		<table style="width:'.($i_width-50).'px;">';
	foreach($a_points_log['explanations'] as $s_explanation_name=>$a_explanations) {
		$message .= '
			<tr><td class="distro_points_explanation_category">'.$s_explanation_name.'</td>';
		if (count($a_explanations) == 0) {
			$message .= '<td class="distro_points_explanation_item" colspan="80">None</td></tr>';
		}
		$s_trow_code = '';
		foreach($a_explanations as $a_explanation) {
			$s_formatted_date = isset($a_explanation['formatted_date']) ? $a_explanation['formatted_date'] : '';
			$s_commission = $a_explanation['commission'];
			$s_points = $a_explanation['distro_points_applied'];
			$s_dataname = $a_explanation['dataname'];
			$s_restaurant_name = $a_explanation['restaurant_name'];
			$s_details = isset($a_explanation['details']) ? str_replace($s_distro_code, $a_reseller['f_name'].' '.$a_reseller['l_name'], $a_explanation['details']) : FALSE;
			if($is_admin != '' && $s_details != '') $s_restaurant_name = '<em>"'. ((strlen($s_details) < 27) ? substr($s_details, 0, 30) : substr($s_details, 0, 30) .'...') .'</em>"';
			$message .= $s_trow_code;
			$message .= '<td class="distro_points_explanation_item">'.$s_formatted_date.'</td>';
			$message .= '<td class="distro_points_explanation_item" '.((int)$s_points < 0 ? 'style="color:red"' : '').' align="center">'.$s_points.'</td>';
			$message .= '<td class="distro_points_explanation_item">'.($s_commission == 'N/A' ? '' : ' (@'.$s_commission.'%)').'</td>';
			$message .= '<td class="distro_points_explanation_item">'.$s_restaurant_name.'</td>';
			if($is_admin != '' && $s_details != '') 
				$message .= '<td class="distro_points_explanation_item">'.($s_details === FALSE ? '&nbsp;' : '<a style="cursor:pointer" onclick="alert(\''.$s_details.'\');">Full Details</a>').'</td>';
			$message .= '</tr>';
			$s_trow_code = '<tr><td>&nbsp;</td>';
		}
	}
	$message .= '
			<tr><td class="distro_points_explanation_category"></td><td class="distro_points_explanation_category">Total: </td><td class="distro_points_explanation_item" colspan="80">'.$a_points_log['predicted_points'].'</td></tr>
		</table>
	</div>';
	
	echo $message;

?>