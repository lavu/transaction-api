<?php

	// JSON JSON JSON JSON

	require_once(dirname(__FILE__)."/../../../cp/resources/json.php");

	// check that the user is logged in
	$dont_actually_load_index = TRUE;
	ob_start();
	require_once(dirname(__FILE__)."/../index.php");
	$s_trash = ob_get_contents();
	ob_end_clean();
	if (!account_loggedin()) {
		$a_retval = array("success"=>FALSE, "error_msg"=>"The server detects that you are no longer logged in.");
		echo LavuJson::json_encode($a_retval);
		return;
	}

	// set up the environment
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");
	if (!isset($maindb) || $maindb == '') $maindb = 'poslavu_MAIN_db';

	class DistroExecRegisterDevices {

		// registers the user's devices
		public function register_devices() {

			// get some values
			global $user;
			$a_values = array();
			foreach($_POST as $k=>$v)
				if (is_numeric($k) && (int)$k > 0)
					$a_values[] = $v;

			// saved the registered devices and exit
			$user->allowed_admin_devices($a_values);
			$this->return_success();
			return;
		}

		// returns a json encoded success message
		public function return_success() {

			$a_retval = array("success"=>TRUE, "success_msg"=>"");
			echo LavuJson::json_encode($a_retval);
			return;
		}
	}

	// check for actions
	if (isset($_REQUEST['action'])) {
		$o_test = new DistroExecRegisterDevices();
		if (method_exists($o_test, $_REQUEST['action'])) {

			// log in the user
			global $user;
			$user = new distro_user();
			$user->set_leads_accounts($loggedin);
			$user->set_all_info($loggedin);

			// call the action
			call_user_method($_REQUEST['action'], $o_test);
		} else {
			$a_retval = array("success"=>FALSE, "error_msg"=>"The requested action is not recognized.");
			echo LavuJson::json_encode($a_retval);
		}
	} else {
		$a_retval = array("success"=>FALSE, "error_msg"=>"Please provide an action.");
		echo LavuJson::json_encode($a_retval);
	}

?>