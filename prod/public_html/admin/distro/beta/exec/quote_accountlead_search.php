<?php

$term = $_REQUEST['term'];
$distro_code = $_REQUEST['distro_code'];

// Default Request var for jQuery autocomplete
if ( empty($_REQUEST['term']) ) {
	echo "No search term.  Please try again.";
	exit;
}

require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');

if ( $_REQUEST['search_accounts'] ) search('ACCOUNT', $_REQUEST);
if ( $_REQUEST['search_leads'] )    search('LEAD', $_REQUEST);
if ( $_REQUEST['search_quotes'] )   search('QUOTE', $_REQUEST);
//usort($search_results, 'cmpForSortingByCompanyName');

//error_log( "DEBUG: search_results=". print_r($search_results, true));  //debug

echo json_encode( $search_results );
exit;


// Functions ---

function echoErrorAndDie($msg) {
	global $term;
	global $distro_code;

	echo "Server error.  Please contact Lavu Sales.";
	error_log(__FILE__ ." encountered error with term={$term} distro_code={$distro_code}: {$msg}");
	die($msg);
}

function search($search_type, $vars) {
	global $search_results;

	if ( $search_type == 'ACCOUNT' ) {
		$sql = "SELECT r.`created`, `company_name` AS `quote_name`, `company_name`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, r.`phone`, r.`email`, s.`dataname`, '' AS `reseller_lead_id`, r.`id` AS `restaurantid`, s.`id` AS `signupid`, s.`salesforce_id`, s.`salesforce_opportunity_id` FROM `poslavu_MAIN_db`.`restaurants` r LEFT JOIN `poslavu_MAIN_db`.`signups` s ON (r.`data_name`=s.`dataname`) WHERE `distro_code`='[distro_code]' AND `company_name` LIKE '%[term]%'";
	}
	else if ( $search_type == 'LEAD' ) {
		$sql = "SELECT `created`, `company_name` AS `quote_name`, `company_name`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `phone`, `email`, `data_name` AS `dataname`, `id` AS `reseller_lead_id`, '' AS `restaurantid`, `signupid`, `salesforce_id`, '' AS `salesforce_opportunity_id` FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `distro_code`='[distro_code]' AND `company_name` LIKE '%[term]%' AND `_canceled`!='1'";
	}
	else {  // search_type == QUOTE
		$sql = "SELECT `createdtime` AS `created`, `quote_name`, `company` AS `company_name`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `phone`, `email`, `dataname`, `reseller_lead_id`, '' AS `restaurantid`, `signupid`, `salesforce_id`, `salesforce_opportunity_id` FROM `poslavu_MAIN_db`.`reseller_quotes` WHERE `distro_code`='[distro_code]' AND (`company` LIKE '%[term]%' OR `quote_name` LIKE '%[term]%') AND (`dataname` != '' OR `reseller_lead_id` != '') AND `_deleted`!='1'";
	}

	$q = mlavu_query($sql, $vars);

	if ($q == false) echoErrorAndDie(mlavu_dberror()); 

	while ( $row = mysqli_fetch_array($q) ) {
		$name_txt = ($search_type == 'QUOTE') ? $row['quote_name'] : $row['company_name'];
		$dataname_txt = empty($row['dataname']) ? '' : ' ('. $row['dataname'] .')';
		$created_txt = empty($row['created']) ? '' : ', created '. date( 'n/j/Y', strtotime( $row['created'] ) );
		$row['label'] = $search_type .': '. $name_txt . $dataname_txt . $created_txt;
		$row['value'] = $name_txt;
		$search_results[] = $row;
	}
}

function cmpForSortingByCompanyName($a, $b) {
	return strcmp($a['company_name'], $b['company_name']);
}
