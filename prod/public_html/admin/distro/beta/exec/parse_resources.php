<?php
error_reporting(E_ERROR);
require_once(dirname(__FILE__)."/../../../cp/resources/json.php");

if(isset($_POST['resource_action'])){
	$_POST['resource_action']();
}
function parse(){
	$json_resources = file_get_contents("../Resources_new/resources_attr.json");
	$json_a = LavuJson::json_decode($json_resources);
	return $json_a;
	//create_display($json_a);
	//error_log("distro/beta/exec/parse_resources.php:: json_count:: ".count($json_a));
	/*(foreach($json_a as $k=>$v){
		error_log("distro/beta/exec/parse_resources.php:: json_count::K=".$k);
		foreach($v as $k2=>$v2){
			error_log("distro/beta/exec/parse_resources.php:: json_count::V[filename]= ".$v2['filename']);
		}
	}*/
	//echo "turkleberry";

}
function parse_all(){
	$json_array = parse();
	create_display($json_array);
}//parse_all()
/*
	$json_resources = file_get_contents("../Resources_new/resources_attr.json");
	$json_a = LavuJson::json_decode($json_resources);
	error_log("distro/beta/exec/parse_resources.php:: json_count:: ".count($json_a));
*/
function create_display($a_file_json){
	$a_display = array();

	$s_sales_display = "";
	$s_marketing_display = "";

	$s_featured_display = "";
	$s_featured_inner = "";
	$s_k_inner_featured = "";

	$s_support_display = "";

	$new_date_cutoff = strtotime('-60 days');

	$a_file_json['latest'] = array_reverse($a_file_json['latest']);
	foreach($a_file_json as $k=>$v){
		//error_log("distro/beta/exec/parse_resources.php:: json_count::K=".$k);
		$s_display = "";
		$s_inner_display = "";
		$s_k_inner = "";


		if($k != 'featured'){
			foreach($v as $k2=>$v2){
				if($v2['homescreen'] != 'removed'){
					$s_temp = '';
					if($v2['subcategory']!=''){
						$sub_section_key = $k.str_replace(' ','',$v2['subcategory']).'_content';
						$sub_section_display = $v2['subcategory'];
					}else{
						$sub_section_key=$k.'NotDefined_content';
						$sub_section_display = 'Not Defined';
					}
					$date_created = empty($v2['date_created']) ? '' : $v2['date_created'];
					$new = (!empty($date_created) && $new_date_cutoff <= strtotime($date_created)) ? " <img style='vertical-align:top; max-height:18px' src='/distro/beta/images/newicon_small.png' border='0' />" : '';
					$onclick_string = '';
					if($v2['type']=='ai' || $v2['type']=='tif'|| $v2['type']=='ttf'|| $v2['type']=='psd'|| $v2['type']=='indd'|| $v2['type']=='lst'){
						$onclick_string = "onclick='resourceDownloadView(\"".$v2['thumbnail']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$k."\",\"".$date_created."\");'";
						if($k == 'latest'){
							$s_display .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"".$v2['thumbnail']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$v2['section']."\",\"".$date_created."\");'><span class='resources_".$v2['section']."_span'>".$v2['section']." - </span>".$v2['display']."{$new}</div>";
							$s_temp .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"".$v2['thumbnail']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$v2['section']."\",\"".$date_created."\");'><span class='resources_".$v2['section']."_span'>".$v2['section']." - </span>".$v2['display']."{$new}</div>";
						}else{
							$s_display .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"".$v2['thumbnail']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$k."\",\"".$date_created."\");'>".$v2['display']."{$new}</div>";
							$s_temp .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"".$v2['thumbnail']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$k."\",\"".$date_created."\");'>".$v2['display']."{$new}</div>";
						}
			  		//$s_display .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"".$v2['thumbnail']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$k."\");'>".$v2['display']."</div>";
					}else{
						$location = $k;
						if($v2['featured']=="featured"){
							$location = $v2['section'];
						}
						$onclick_string = "onclick='resourceDownloadView(\"./Resources_new/".$location."/".$v2['filename']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$k."\",\"".$date_created."\");'";
						if($k == 'latest'){
							$s_display .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"./Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$v2['section']."\",\"".$date_created."\");'><span class='resources_".$v2['section']."_span'>".$v2['section']." - </span>".$v2['display']."{$new}</div>";
							$s_temp .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"./Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$v2['section']."\",\"".$date_created."\");'><span class='resources_".$v2['section']."_span'>".$v2['section']." - </span>".$v2['display']."{$new}</div>";
						}else{
							$s_display .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"./Resources_new/".$location."/".$v2['filename']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$k."\",\"".$date_created."\");'>".$v2['display']."{$new}</div>";
							$s_temp .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"./Resources_new/".$location."/".$v2['filename']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$k."\",\"".$date_created."\");'>".$v2['display']."{$new}</div>";
						}
			  		//$s_display .= "<div class='resources_content_file_name' onclick='resourceDownloadView(\"./Resources_new/".$location."/".$v2['filename']."\",\"".$v2['filename']."\",\"/Resources_new/".$v2['section']."/".$v2['filename']."\",\"".$v2['title']."\",\"".$v2['sub_title']."\",\"".$v2['subcategory']."\",\"".$v2['filesize']."\",\"".$v2['description']."\",\"".$k."\");'>".$v2['display']."</div>";
					}
					if($v2['featured']=="featured"){
						$s_featured_display.= $s_temp;
			  		//error_log("==featured==a==a ".$v2['filename']);
					}
			  	    //error_log('mmmmmmmNNNNN::=:: '.$sub_section_key);
					$file_thumbnail = './Resources_new/thumbnails/about_xcode_tools_3.jpg';
					if($v2['thumbnail'] != ''){
						$file_thumbnail = $v2['thumbnail'];
					}
					$a_display[$sub_section_key] .= '<div class="resources_files_preview" '.$onclick_string.'><a class="resource_tooltip" href="#"><span>'.$v2['description'].'</span></a><div class="resources_file_preview_title">'.$v2['display'].'</div><div class="resources_file_preview_date">'.$v2['sub_title'].'</div><div class="resources_file_preview_type">'.$v2['type'].' :: '.$v2['size'].'</div><div class="resources_file_preview_view"><img src="'.$file_thumbnail.'" alt="file resource preview" style="width:200px;"/></div></div><!-- resource_file_preview -->';
					if($v2['featured'] == "featured"){
						$s_featured_inner .= '<div class="resources_files_preview" '.$onclick_string.'><a class="resource_tooltip" href="#"><span>'.$v2['description'].'</span></a><div class="resources_file_preview_title">'.$v2['display'].'</div><div class="resources_file_preview_date">'.$v2['sub_title'].'</div><div class="resources_file_preview_type">'.$v2['type'].' :: '.$v2['size'].'</div><div class="resources_file_preview_view"><img src="'.$file_thumbnail.'" alt="file resource preview" style="width:200px;"/></div></div><!-- resource_file_preview -->';'<div class="resources_files_preview" '.$onclick_string.'><a class="resource_tooltip" href="#"><span>'.$v2['description'].'</span></a><div class="resources_file_preview_title">'.$v2['display'].'</div><div class="resources_file_preview_date">'.$v2['sub_title'].'</div><div class="resources_file_preview_type">'.$v2['type'].' :: '.$v2['size'].'</div><div class="resources_file_preview_view"><img src="'.$file_thumbnail.'" alt="file resource preview" style="width:200px;"/></div></div><!-- resource_file_preview -->';
					}
			  	    //$a_display[$sub_section_key] .= '<div class="resources_files_preview" style="display:inline-block;vertical-align:top;border:1px solid #8b8b8b;margin-bottom:15px;padding:5px;" '.$onclick_string.'><div class="resources_file_preview_title">'.$v2['display'].'</div><div class="resources_file_preview_date">'.$v2['sub_title'].'</div><div class="resources_file_preview_type">'.$v2['type'].' :: '.$v2['size'].'</div><div class="resources_file_preview_view"><img src="'.$file_thumbnail.'" alt="file resource preview" style="width:200px;"/></div></div><!-- resource_file_preview -->';
			  	    /*
			  	    <div class="resources_subsection_head">
			  		<div class="sub_section_arrow_closed">
			  			<img src="./images/more_arrow_gfx.png" alt="more arrow gfx"/>
			  		</div>
			  		<div class="sub_section_arrow_open" >
			  			<img src="./images/green_down_button.png" style="width:25px;" alt="green down arrow"/>
			  		</div>
			  		<div class="resources_subsection_category_title">'.$sub_section_display.'</div>
			  	    </div><!-- resources_subsection_head -->
			  	    */
			  	    //$s_inner_display.= '<>';
			  	    //print_r($a_display[$v2['filename']]);
			  	    //error_log("distro/beta/exec/parse_resources.php:: json_count::V[filename]= ".$v2['filename']);
			  	    if(isset($v['featured']) && $v['featured'] == 'featured'){
			  		  //error_log($s_display);
			  	    }
			  	}else{
			  		//error_log('mmmmm   distro/beta/exec/parse_resources.php#118 file removed :: '.$v2['filename']);
			  	}//if not removed

		    }//all files set up display
	    }// if $k != featured
		//$s_k_inner = $k."_inner";
		//if($k == "latest"){
				//error_log("==latest==a==a ".$v2['filename']);
			//}
	    if($k != 'featured'){
	    	$a_display[$k] = $s_display;
	    }

		//$a_display[$s_k_inner] =$s_inner_display;
	}
	$a_display['featured'] = $s_featured_display;
	$a_display['allFeatured_content'] = $s_featured_inner;
	echo LavuJson::json_encode($a_display);
	//print_r($a_display);


	//return "Nialls drinks his own pee";
}
