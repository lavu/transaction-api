<?php

	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/register/create_account.php");
	require_once(dirname(__file__)."/../distro_user.php");
	require_once(dirname(__file__)."/distro_user_functions.php");
	$message = "";
	
	if (!isset($maindb))
		$maindb = "poslavu_MAIN_db";
	
	function getPost($s_post_name) {
		return (isset($_POST[$s_post_name])?$_POST[$s_post_name]:'');
	}
	
	// dependency of db_check_existance
	function _db_check_existance_string($s_database, $s_table, $s_field, $s_value) {
		$existance_query = mlavu_query("SELECT `[3]` FROM `[1]`.`[2]` WHERE `[3]`='[4]' LIMIT 1", $s_database, $s_table, $s_field, $s_value);
		if ($existance_query === FALSE)
			return FALSE;
		if (mysqli_num_rows($existance_query) > 0)
			return TRUE;
		return FALSE;
	}
	
	// returns FALSE if there isn't a row that matches $a_s_values or any of $a_s_values
	function db_check_existance($s_database, $s_table, $s_field, $a_s_values) {
		if (gettype($a_s_values) == 'string')
			return _db_check_existance_string($s_database, $s_table, $s_field, $a_s_values);
		if (gettype($a_s_values) == 'array')
			foreach($a_s_values as $s_value)
				if (!_db_check_existance_string($s_database, $s_table, $s_field, $s_value))
					return FALSE;
		return TRUE;
	}
	
	function assign_distro_to_lead($s_distro_code, $s_lead_id) {
		global $maindb;
		
		if (!db_check_existance($maindb, "resellers", "username", $s_distro_code))
			return "failed|That distributor ($s_distro_code) can't be found in the database";
		if (!db_check_existance($maindb, "reseller_leads", "id", $s_lead_id))
			return "failed|That lead ($s_lead_id) can't be found in the database.";
		$undecline_lead = '0';
		$time_assigned = date("Y-m-d H:i:s");
		//echo $time_assigned;
		
		$new_lead_source = no_merc_lead_update($s_lead_id);
		$update_query_vars = array('distro_code'=>$s_distro_code, '_declined'=>$undecline_lead, 'assigned_time'=>$time_assigned, 'accepted'=>"0000-00-00 00:00:00",'lead_source'=>$new_lead_source);
		$s_update_clause = ConnectionHub::getConn('poslavu')->getDBAPI()->arrayToUpdateClause($update_query_vars);
		//error_log('assign distro to lead query=='.$s_update_clause);
		$update_query_string = "UPDATE `[maindb]`.`reseller_leads`$s_update_clause WHERE `id`='[id]' limit 1";
		mlavu_query($update_query_string, array_merge(array('maindb'=>$maindb, 'id'=>$s_lead_id), $update_query_vars));
		
		if (ConnectionHub::getConn('poslavu')->affectedRows() > 0){
			email_notify_assigned($s_distro_code);
			add_distro_code_to_restaurant_table($s_distro_code, $s_lead_id);
			$r_assigner_id = getPost('assign_lead_res_id');
			$r_assigner_uname = getPost('assign_lead_res_usr');
			
			//info for entry in history
			$action = "lead assigned to specialist";
			$details = "$s_distro_code was assigned lead id# $s_lead_id by $r_assigner_uname at $time_assigned";
			add_to_distro_history($action, $details, $r_assigner_id, $r_assigner_uname);
			//error_log($r_assigner_uname." assigned lead id#$s_lead_id to $s_distro_code");
			return "success|The distributor was assigned to the lead by $r_assigner_uname.".$time_assigned;
		}else{
			return "failed|Assignment failed. Please try again.";
		}
	}
	function add_distro_code_to_restaurant_table($s_distro_code, $s_lead_id){
		global $maindb;
		
		//error_log("assign lead restaurant table update");
		$query_attrs = array();
		$query_attrs['lead_table'] = '`'.$maindb.'`.`reseller_leads`';
		$query_attrs['id'] = $s_lead_id;
			//error_log("lead_table== ".$query_attrs['lead_table']);
		$data_name_check = mlavu_query("select * from [lead_table] where `id`= '[id]' and `data_name` != ''", $query_attrs);
			//error_log("---q1:: select `data_name` from lead_table where `id`= '[id]' and `data_name` != ''");
			
			
		if(mysqli_num_rows($data_name_check) > 0){
			$q_results = mysqli_fetch_assoc($data_name_check);
			if(trim($q_results['data_name']) != ''){
				$query_attrs['res_table'] = '`'.$maindb.'`.`restaurants`';
				$query_attrs['data_name'] = $q_results['data_name'];
				$query_attrs['distro_code'] = $s_distro_code;
				//error_log("---q2:: update res_table set `distro_code`='[distro_code]' where `data_name`='[data_name]' and `data_name` != '' limit 1 ");
				mlavu_query("update [res_table] set `distro_code`='[distro_code]' where `data_name`='[data_name]' and `data_name` != '' limit 1 ", $query_attrs);
			}
		}
		/*if(_db_check_existance_string($maindb, "reseller_leads", "data_name", $s_distro_code)){
			$res_query_attrs = array();
			$res_query_attrs['restaurant_table'] = '`'.$maindb.'`.`restaurants`';
			$res_query_attrs['distro_code'] = $s_distro_code;
			$res_query_attrs['restaurant_table'] = 'restaurants';
			mlavu_query("update [1] set ")
		}*/
	}
	
	//send email to distributer assigned
	function email_notify_assigned($d_code){
		//echo "-----".$d_code;
		$q_fields = array();
		$q_fields['db'] = '`poslavu_MAIN_db`.`resellers`';
		$q_fields['username'] = $d_code;
		
		$q_string = "select * from [db] where `username`='[username]'";
		$q_results = mlavu_query($q_string, $q_fields);
		if($distro_info = mysqli_fetch_assoc($q_results)){
			//echo 'success|--'.$distro_info['email'].'--';
			$to = $distro_info['email'];
			$subject = "A NEW POSLAVU LEAD HAS BEEN ASSIGNED TO YOU";
			$message = "Please Log into https://admin.poslavu.com/distro/beta and go to the leads tab to view your new lead. Please select either accept or decline lead to continue process. Thank you.";
			$headers = 'From: resellers@poslavu.com' . "\r\n".'Reply-To: distributors@poslavu.com'."\r\n".'X-Mailer: PHP/'.phpversion();
			//error_log("DDDDDDDDDDD".$to."-----".$subject."-----");
			if ($distro_info['username'] != 'lavusales505') $mail_response = mail($to, $subject, $message, $headers);  // disabled per Josh on 2015-09-09
			$action = "assigned email";
			$details = "Lead assigned to Specialist:: ".$distro_info['username'].", email::".$to.", mail response code:: ".$mail_response."";
			add_to_distro_history($action, $details, $distro_info['id'], $distro_info['username']);
		}else{
			$action = "assigned email fail";
			$details = "Lead assigned to Specialist:: ".$q_fields['username'].", email:: failed";
			add_to_distro_history($action, $details, '', $q_fields['username']);
		}
		
	}
	function no_merc_lead_update($s_lead_id){
		$a_vars = array();
		$a_vars['db'] = '`poslavu_MAIN_db`.`reseller_leads`';
		$a_vars['id'] = $s_lead_id;
		$q_first = mlavu_query("select `lead_source` from [db] where `id`=[id]",$a_vars);
		$lead_source = '';
		if($row = mysqli_fetch_assoc($q_first)){
			$lead_source = $row['lead_source'];
		  //echo 'current source=='.$lead_source;
	    }
	    $lead_source = str_replace('_heartland_', '', $lead_source);
	    $lead_source .= getPost('heartland-lead');
	    $lead_source = str_replace('_no_merc', '', $lead_source);
	    //error_log('no merc lead update == '.$lead_source.'_no_merc');
	    return $lead_source.'_no_merc';
	}//no_merc_lead_update(lead_id)
	$s_distro_code = getPost('all_reseller_select');
	$s_lead_id = getPost('assign_lead_lead_id');
	
	if($s_distro_code == ''){
		echo 'failed|Please select a reseller.';
	}
	else if ($s_lead_id == '') {
		echo 'failed|No lead is selected.';
	}
	else {
		echo assign_distro_to_lead($s_distro_code, $s_lead_id);
	}
	
	
?>
