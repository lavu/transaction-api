<?php
require_once("/home/poslavu/public_html/admin/cp/resources/json.php");
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");

$datanames = LavuJson::json_decode($_POST['json']);
$s_in_clause_datanames = arrayToInClause($datanames, TRUE);

if($s_in_clause_datanames!=='IN ()'){
    $query_fields = array();
    $query_fields['survey_db'] = "`poslavu_MAIN_db`.`survey`";
    $query_fields['in_clause'] = $s_in_clause_datanames;
    
    $q_results = mlavu_query("select * from [survey_db] where `dataname` $s_in_clause_datanames", $query_fields);
    
    
    $a_q_results = array();
    while($row = mysqli_fetch_assoc($q_results)){
        $comment = str_replace(array("\r", "\n"), "", $row['value_long']);
        array_push($a_q_results,
            array(
                'dataname' => $row['dataname'],
                'setting' => $row['setting'],
                'date' => $row['value'],
                'value2' => $row['value2'],
                'value4' => $row['value4'],
                'value_long' => $comment,
                'value5' => $row['value5'],)
            );
    }
    echo LavuJson::json_encode($a_q_results);
}

function arrayToInClause(&$indexed_array, $b_use_value = FALSE) 
{
			$a_vals = array();
			if ($b_use_value) {
				foreach($indexed_array as $v)
					if ($v !== '')
						$a_vals[] = $v;
			} else {
				foreach($indexed_array as $k=>$v)
					if ($k !== '')
						$a_vals[] = $k;
			}
			if (count($a_vals) == 0)
				return 'IN ()';
			return 'IN (\''.implode('\',\'', $a_vals).'\')';
}
