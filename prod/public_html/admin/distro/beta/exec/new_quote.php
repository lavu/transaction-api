<?php

require_once( dirname(__FILE__) .'/../../../cp/resources/core_functions.php');
require_once( dirname(__FILE__) .'/../../../../register/create_account.php');

global $dataname, $reseller_lead_id, $signupid, $username, $password, $main_recordid;

$input = $_REQUEST;

$display_message = '';

$lavu_distro_codes = array('lavusales505', 'bryansales', 'kalynnsales', 'joshsales', 'tanyasales', 'tomlavu');

// Get the reseller row for the distro commission rate discount on licenses.
$q = mlavu_query("SELECT * FROM `resellers` WHERE `username`='[distro_code]'", $input);
if ($q !== false) $reseller = mysqli_fetch_assoc($q);
mysqli_free_result($q);

// Create a new demo account, if they've requested it, before we create the quote so we can associate them together.
if ( $input['associated_account'] == 'new' ) {
    $suppress_new_account_output = true;
    $_POST['new_acc_comp_name'] = $input['company'];
    $_POST['new_acc_email'] = empty($input['email']) ? '?' : $input['email'];  // not required for quote
    $_POST['new_acc_phone'] = empty($input['phone']) ? '?' : $input['phone'];  // not required for quote
    $_POST['new_acc_first_name'] = $input['firstname'];
    $_POST['new_acc_last_name'] = $input['lastname'];
    $_POST['new_acc_address'] = $input['address'];
    $_POST['new_acc_city'] = $input['city'];
    $_POST['new_acc_state'] = $input['state'];
    $_POST['new_acc_zip'] = $input['zip'];
    $_POST['starter_menu'] = 'default_restau';
    $_POST['distro_username'] = $input['distro_code'];
    $_POST['distro_id'] = $reseller['id'];
    $_POST['distro_email'] = $reseller['email'];
    $_POST['new_acc_has_lead'] = 'FALSE';
    require_once( dirname(__FILE__) .'/create_account.php');
    $input['dataname'] = $dataname;
    $input['signupid'] = $signupid;
    $input['reseller_lead_id'] = $reseller_lead_id;
    $input['restaurantid'] = $main_recordid;
    if ( empty($dataname) ) {
        echo 'failure|Error creating account';
        return;
    }
}

// Pre-insert value tweaks

$input['createdtime'] = date("Y-m-d H:i:s");
$input['updatedtime'] = $input['createdtime'];
$input['total_price'] = '0.00';
$input['total_cost'] = '0.00';
$input['license_amount'] = '0.00';
$input['hosting_amount'] = '0.00';
$input['hardware_amount'] = '0.00';
$input['points_amount'] = '';
$input['package_level'] = '';
$input['same_billing_address'] = isset($input['same_billing_address']) ? $input['same_billing_address'] : '';
$input['for_inventory'] = ($input['associated_account'] == 'reseller') ? '1' : '';

if ( !empty($input['same_billing_address']) ) {
    $input['billing_firstname'] = $input['firstname'];
    $input['billing_lastname'] = $input['lastname'];
    $input['billing_address'] = $input['address'];
    $input['billing_city'] = $input['city'];
    $input['billing_state'] = $input['state'];
    $input['billing_zip'] = $input['zip'];
}

$payment_options='';
$pay_option[]=($input['payment_option_1'] != '') ? $input['payment_option_1'] : '';
$pay_option[]=($input['payment_option_2'] != '') ? $input['payment_option_2'] : '';
$pay_option[]=($input['payment_option_3'] != '') ? $input['payment_option_3'] : '';

$cnt=0;
foreach($pay_option as $payinfo){
    if($payinfo!=''){
        if($cnt==0){
            $payment_options=$payinfo;
        }
          else{
             $payment_options=$payment_options.",".$payinfo;
         }
          $cnt++;
    }
}

$input['payment_options']=$payment_options;
                
$result = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_quotes` (`distro_code`, `resellerid`, `dataname`, `reseller_lead_id`, `restaurantid`, `signupid`, `quote_name`, `company`, `firstname`, `lastname`, `address`, `city`, `state`, `zip`, `country`, `email`, `phone`, `createdtime`, `status`, `current_pos`, `current_processor`, `requested_processor`, `notes`, `customer_notes`, `same_billing_address`, `billing_firstname`, `billing_lastname`, `billing_address`, `billing_city`, `billing_state`, `billing_zip`, `billing_country`, `for_inventory`,`payment_options`,`pay_link_option`) values('[distro_code]', '[resellerid]', '[dataname]', '[reseller_lead_id]', '[restaurantid]', '[signupid]', '[quote_name]', '[company]', '[firstname]', '[lastname]', '[address]', '[city]', '[state]', '[zip]', '[country]', '[email]', '[phone]', '[createdtime]', '[status]', '[current_pos]', '[current_processor]', '[requested_processor]', '[notes]', '[customer_notes]', '[same_billing_address]', '[billing_firstname]', '[billing_lastname]', '[billing_address]', '[billing_city]', '[billing_state]', '[billing_zip]', '[billing_country]', '[for_inventory]','[payment_options]','[pay_link_option]')", $input);
if ($result === false) {
    echo 'failed|'. mlavu_dberror();
    exit;
}
$input['reseller_quote_id'] = mlavu_insert_id();

$line_num = intval($input['line_num']);
$last_line_num = $line_num;

// Insert line items for productX, qtyX, unit_priceX

$quote_line_items = array();
for ($i = 0; $i <= $line_num; $i++) {
    // Make the X in productX blank for "product" since it's not "product0".
    $x = $i;

    // Make sure productX exists because they may have deleted a set of controls.
    if ( empty($input["product_{$x}"]) ) continue;

    // Overwrites 0 with blank
    $input['line_num'] = $x;
    
    // Get product by splitting delimited field
    $input['product'] = isset($input["product_info_{$x}"]) ? $input["product_info_{$x}"] : '';
    list($input['category'], $input['product'], $input['salesforce_pricebookentry_id'], $input['max_price'], $input['unit_cost']) = explode('^', $input['product'], 5);

    // Product type -- hardware vs. billing line item + type of billing line item
    $is_license_lineitem = false;
    $is_hosting_lineitem = false;
    $is_billing_lineitem = false;

    if (stripos($input['product'], 'License') !== false || $input['product'] === 'Lavu 88') {
        $input['product_type'] = 'license';
        $is_billing_lineitem = true;
        $is_license_lineitem = true;
    }
    else if ($input['category'] == 'POS Packages' && (stripos($input['product'], 'Terminal') !== false)) {
        $input['product_type'] = 'hosting';
        $is_billing_lineitem = true;
        $is_hosting_lineitem = true;
    }
    else {
        $input['product_type'] = 'hardware';
    }

    // Set package_level
    if ($is_hosting_lineitem && stripos($input['product'], 'Enterprise') === false) $input['package_level'] = '25';
    else if ($is_billing_lineitem && stripos($input['product'], 'Lavu Enterprise') !== false) $input['package_level'] = '26';
    else if ($is_billing_lineitem && stripos($input['product'], 'Gold') !== false) $input['package_level'] = '22';
    else if ($is_billing_lineitem && stripos($input['product'], 'Lavu 88') !== false) $input['package_level'] = '8';

    // Price of line item

    // Overwrite the values for control set prime so we can use the same insert statement each time.
    $input['qty']             = isset($input["qty_{$x}"])            ? $input["qty_{$x}"]            : '';
    $input['unit_price']      = isset($input["unit_price_{$x}"])     ? $input["unit_price_{$x}"]     : '';
    $input['from_inventory']  = (isset($input["from_inventory_{$x}"]) && empty($input['for_inventory'])) ? $input["from_inventory_{$x}"] : '';
    $input['points_purchase'] = (isset($input["from_inventory_{$x}"]) && $is_license_lineitem) ? $input["from_inventory_{$x}"] : '';

    // Make sure any licenses discounted from the distro commission rate don't look suspicious (implying that the distro_license_com value was tampered with in the web page from a web developer utility)
    if ( $is_license_lineitem && !empty($reseller['distro_license_com']) && empty($input['from_inventory']) && empty($input['points_purchase']) ) {
        $distro_license_com = 1.0 - (floatval($reseller['distro_license_com']) / 100.0);
        $unit_cost = floatval($input['unit_cost']);
        $license_price_discounted_by_cmsn = (floatval($distro_license_com) * floatval($unit_cost));
        if ( $license_price_discounted_by_cmsn !== floatval($input['unit_price']) ) {
            ///mail("orders@lavu.com", "Unexpected Discounted License Price in Quote", "Detected discounted License amount that did not match expected amount based on Specialists' commission rate:\n\nDistro Code: {$input['distro_code']}\nCommision Rate: {$reseller['distro_license_com']}\nQuote Name: {$input['quote_name']}\nReseller Quote ID: {$input['reseller_quote_id']}\nLicense: {$input['product']}\nUnit Price: {$input['unit_price']}\nExpected Price: {$license_price_discounted_by_cmsn}");
            ///$input['unit_price'] = $license_price_discounted_by_cmsn;
        }
    }

    // Adjust prices for line items being furnished from or purchase for the Specialists' own inventory.
    if ( $input['for_inventory'] == '1' ) {
        $input['unit_price'] = $input['unit_cost'];
    }
    else if ( $input['from_inventory'] == '1' ) {
        // Capture the unit_cost for Licenses paid for with points in points_amount before we overwrite unit_cost.
        if ( $input['points_purchase'] == '1' ) {
            $input['points_amount'] = intval($input['points_amount']) + intval($input['unit_cost']);
        }

        // No unit_cost when the Specialist is supplying the hardware from their own inventory - the line item is just here so it appears on the quote.
        $input['unit_cost'] = 0.00;
    }

    // Tally the categorized product
    $input[ $input['product_type'] . '_amount'] = floatval($input[ $input['product_type'] . '_amount'] ) + (floatval($input['qty']) * floatval($input['unit_price']));

    $result = mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_quotes_lineitems` (`distro_code`, `dataname`, `reseller_quote_id`, `createdtime`, `updatedtime`, `line_num`, `product_type`, `product`, `qty`, `unit_price`, `unit_cost`, `from_inventory`, `for_inventory`, `points_purchase`, `salesforce_pricebookentry_id`) values('[distro_code]', '[dataname]', '[reseller_quote_id]', '[createdtime]', '[updatedtime]', '[line_num]', '[product_type]', '[product]', '[qty]', '[unit_price]', '[unit_cost]', '[from_inventory]', '[for_inventory]', '[points_purchase]', '[salesforce_pricebookentry_id]')", $input);
    $reseller_quotes_lineitems_id = mlavu_insert_id();
    if ($result === false || !$reseller_quotes_lineitems_id ) {
        error_log(__FILE__ ." failed to insert reseller_quoet_lineitem=". print_r($input, true));
        $insert_failed = true;
        break;
    }

    // Collect line items for Salesforce integration
    $quote_line_items[$i] = array('product' => $input['product'], 'qty' => $input['qty'], 'unit_price' => $input['unit_price'], 'unit_cost' => $input['unit_cost'], 'from_inventory' => $input['from_inventory'], 'for_inventory' => $input['for_inventory'], 'points_purchase' => $input['points_purchase'], 'line_num' => $input['line_num'], 'reseller_quote_lineitems_id' => !$reseller_quotes_lineitems_id );

    // Email content + add totals
    $special = '';
    if ( $input['points_purchase'] == '1' ) $special .= ' **Points Purchase**';
    else if ( $input['from_inventory'] == '1' ) $special .= ' **From Inventory**';
    $input['lineitems_email_content'] .= $input['product'] .' Qty '. $input['qty'] .' Unit Price '. number_format($input['unit_price'], 2) . $special ."\n";
    $input['total_price'] = floatval($input['total_price']) + (floatval($input['qty']) * floatval($input['unit_price']));
    $input['total_cost'] = floatval($input['total_cost']) + (floatval($input['qty']) * floatval($input['unit_cost']));
}

// Output quote-creation error to Ajax response handler

if ( $insert_failed ) {
    echo 'failure|An error occurring when when processing your quote. Please report this error to Lavu Sales.';
    error_log(__FILE__ ." failed to insert row {$i}: ". mlavu_dberror() );
    exit;
}

// Update totals fields

$input['total_price'] = number_format($input['total_price'], 2, '.', '');
$input['total_cost'] = number_format($input['total_cost'], 2, '.', '');
$input['license_amount'] = number_format($input['license_amount'], 2, '.', '');
$input['hosting_amount'] = number_format($input['hosting_amount'], 2, '.', '');
$input['hardware_amount'] = number_format($input['hardware_amount'], 2, '.', '');

$payment_options='';
$pay_option_i[]=($input['payment_option_1'] != '') ? $input['payment_option_1'] : '';
$pay_option_i[]=($input['payment_option_2'] != '') ? $input['payment_option_2'] : '';
$pay_option_i[]=($input['payment_option_3'] != '') ? $input['payment_option_3'] : '';

$cnt=0;
foreach($pay_option_i as $payinfo){
    if($payinfo!=''){
        if($cnt==0){
            $payment_options=$payinfo;
        }
        else{
            $payment_options=$payment_options.",".$payinfo;
        }
        $cnt++;
    }
}
$input['payment_options']=$payment_options;

mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_quotes` SET `package_level`='[package_level]', `total_price`='[total_price]', `total_cost`='[total_cost]', `license_amount`='[license_amount]', `hosting_amount`='[hosting_amount]', `hardware_amount`='[hardware_amount]', `points_amount`='[points_amount]',`payment_options`='[payment_options]',`pay_link_option`='[pay_link_option]' WHERE `id` = '[reseller_quote_id]' AND `distro_code`='[distro_code]'", $input);

if ( ConnectionHub::getConn('poslavu')->affectedRows() < 0 ) {
    error_log(__FILE__ ." failed to update reseller_quotes ID {$input['reseller_quote_id']} with total_price={$input['total_price']} total_cost={$input['total_cost']}: ". mlavu_dberror());
}

// Send to Sales team
$input['payment_options_label']=getPaymentOptions($input['payment_options']);
$input['pay_link_option_label']=($input['pay_link_option']==1)?'Yes':'No';
$sent_to_salesteam = sendEmail($input);

if ( !$sent_to_salesteam ) {
    echo 'failure|Error when sending quote to Sales Team for approval. Please report this error to Lavu Sales.';
    error_log(__FILE__ ." failed to send quote to Salesfore for input=". print_r($input, true) );
    exit;
}

$success_msg = 'success|Your quote has been submitted for approval.';

if ( !empty($username) && !empty($password) ) {
    $success_msg .= "<p>And your new account has been created.</p><p>Username: {$username}<br>Password: {$password}</p>";
}

echo $success_msg;
exit;

function getPaymentOptions($val){
 $paymentOptions=array('1'=>'Financing','2'=>'Customer Credit Card','3'=>'My Credit Card');
 $vals=explode(',',$val);
 foreach($vals as $val){
  $paymentOption[]=$paymentOptions[$val];
 }
 return implode(',',$paymentOption);
}

// Functions

/**
 * The main try/catch logic block is taken from /home/poslavu/public_html/register/lib/utilities.php
 */
function sendEmail($input) {
    $special_section = '';
    if ($input['for_inventory'] == '1') $special_section = "**For Inventory Quote**";
    $quote_email_content .= <<<EMAIL

New Quote Request from {$input['distro_code']}

{$input['reseller_company']}
{$input['reseller_name']}
{$input['reseller_phone']}
{$input['reseller_email']}

{$special_section}

Shipping Address:
{$input['company']}
{$input['firstname']} {$input['lastname']}
{$input['address']}
{$input['city']}, {$input['state']} {$input['zip']}
{$input['country']}
{$input['phone']}
{$input['email']}

Billing Adress:
{$input['billing_firstname']} {$input['billing_lastname']}
{$input['billing_address']}
{$input['billing_city']}, {$input['billing_state']} {$input['billing_zip']}
{$input['billing_country']}

Dataname: {$input['dataname']}
Current POS: {$input['current_pos']}
Current Processor: {$input['current_processor']}
Processor Requested: {$input['requested_processor']}

Notes to Lavu Sales:
{$input['notes']}

Notes to Customer:
{$input['customer_notes']}

How will this quote be paid?
{$input['payment_options_label']}

Payment link required?
{$input['pay_link_option_label']}

{$input['lineitems_email_content']}
TOTAL: {$input['total_price']}

(Specialist Cost: {$input['total_cost']})

EMAIL;

    return mail("orders@lavu.com", "New Quote Request", $quote_email_content);
 }

function sanitize_email( $email ) {
    $email = trim( $email, ". \t\n\r\0\x0B" );
    $email = str_replace('@@', '@', $email);
    $email = strpos($email,'@') === false ? "$email@not.available" : $email;
    if ( function_exists('filter_var') ) {
        if ( !filter_var($email, FILTER_VALIDATE_EMAIL ) ) {
            $email = "not@vail.able";
        }
    } else {
        $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
        if (!preg_match($pattern, $email)) {
            $email = "not@vail.able";
        }
    }
    return $email;
}
