<?php
require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
require_once("/home/poslavu/public_html/admin/sa_cp/billing/payment_profile_functions.php");
require_once("/home/poslavu/public_html/admin/distro/beta/distro_tab_container.php");
require_once("/home/poslavu/public_html/admin/distro/beta/distro_user.php");
if (!isset($o_package_container)){
	$o_package_container = new package_container();
}
global $s_licenses_not_upgrades; // names of licenses the user has that aren't upgrades (as bar-delimeted string)
global $a_licenses_not_upgrades; // all distro licenses that aren't upgrades.
global $a_licenses_upgrades; // alll distro licenses that are upgrades.
global $a_licenses_not_upgrades_not_special; // ids of all licenses that aren't upgrades and aren't specially priced licenses
if(!isset($o_current_user)){
	$o_current_user = new distro_user();

	$user_name = $_POST['distro_code'];
	if($user_name == ''){
		$user_name = $_POST['account_filter_distro_code'];
	}
	//echo '-f-f-f-f-f-f'.$user_name.'<br />';//$_REQUEST['notmartin'].'<br />';
	$o_current_user->set_all_info($user_name);
}//o_current_user not set
else{
}//else
//print_r($_POST);
//error_log("distro/beta/exec/searchAccountTemp.php->start..".$_POST['action']);
if(isset($_POST['action'])){
	//echo 'boob action::'.$_POST['action'].'>>';
	$_POST['action']();
}else if(isset($_POST['account_filter_distro_action'])){
	//echo '||filter search:: action::'.$_POST['account_filter_distro_action'].', distro_code::'.$_POST['account_filter_distro_code'].', distro_id::'.$_POST['account_filter_distro_id'];
	$_POST['account_filter_distro_action']();
}

//searches for account info on hover
function focus_search(){
	//$post_vals = "rid=".$_POST['rid'].", access=".$_POST['access'].", distro_code=".$_POST['distro_code'].", distro_access=".$_POST['distro_access'].", did=".$_POST['did'];
	//echo $post_vals;
	$restaurantid = $_POST['rid'];
	$q_array = array();
	$q_array['restaurantsdb'] = '`poslavu_MAIN_db`.`restaurants`';
	$q_array['id'] = $restaurantid;
	$restaurants_query_string = "select * from [restaurantsdb] where `id`='[id]'";
	//	$_SESSION['update_accounts'] = '';
	$restaurants_query = mlavu_query($restaurants_query_string, $q_array) or die(mlavu_dberror("disto_user->set_leads_accounts_restaurants_query error"));
	
	if($entry = mysqli_fetch_assoc($restaurants_query)){
		set_account_info($entry);
	}else{
		echo "<div>no results found<div><br />";
	}
}//focus_search()
function filter_search(){
	$distroid = $_POST['account_filter_distro_id'];
	$distrocode = $_POST['account_filter_distro_code'];
        $account_searchString = $_POST['accountSearch'];
	
        $a_filters = getFilters();
        
	//echo '#70<br />'.print_r($a_filters).'<br />length:'.strlen($filter_length).', '.$a_filters['filter_length'].'<br />';
        //$accounts = $_POST['accounts'];
        //userAccountSearch($accounts, $account_searchString, $a_filters);
	build_accounts_array($distroid, $distrocode, $a_filters, $account_searchString);
}//filter_search
//
//
function getFilters(){
    $a_filters = array();

	//echo "filter search:: build_accounts_array(".$distroid.", ".$distrocode.", $a_filters)";
	$filter_length = '';
	$a_filters['distro_id'] = $distroid;
	$a_filters['distro_code'] = $distrocode;
	$a_filters['time_window'] = $_POST['account_tab_filter_time'];
	//$filter_length .= $a_filters['time_window'];
	$a_filters['silver'] = isset($_POST['account_filter_silver']) ? "Silver" : '';
	$filter_length .= $a_filters['silver'];
	$a_filters['gold'] = isset($_POST['account_filter_gold']) ? "Gold" : '';
	$filter_length .= $a_filters['gold'];
	$a_filters['pro'] = isset($_POST['account_filter_pro']) ? "Platinum" : '';
	$filter_length .= $a_filters['pro'];
	$a_filters['lavu88'] = isset($_POST['account_filter_lavu88']) ? "Lavu" : '';
	$filter_length .= $a_filters['lavu88'];
	$a_filters['trial'] = isset($_POST['account_filter_trial']) ? 'n/a' : '';
	$filter_length .= $a_filters['trial'];
	$a_filters['self'] = isset($_POST['account_filter_self']) ? "Self" : '';
	$filter_length .= $a_filters['self'];
	$a_filters['canceled'] = $_POST['account_filter_canceled'];
	$a_filters['filter_length'] = (strlen($filter_length) > 1) ? "filters" : 'all';
        
        return $a_filters;
}

/**
 * Gets all of the accounts for a reseller if no search text is specified or just ones
 * containing the search text in their account name, dataname, or email if it is specified.
 */
function getAccounts($distro_code, $search_text='') {

    $accounts = array();

    if (!empty($distro_code))
    {
        $sql = "SELECT r.`id`, r.`data_name`, s.`dataname`, r.`company_name`, r.`company_code`, r.`email`, r.`distro_code`, r.`created`, s.`package`, s.`address`, s.`city`, s.`state`, s.`zip`, ps.`trial_end`, ps.current_package AS `has_license_payment`, ps.`license_applied`, ps.`license_type` FROM `poslavu_MAIN_db`.`restaurants` r LEFT OUTER JOIN `signups` s ON (r.`data_name` = s.`dataname`) LEFT OUTER JOIN `payment_status` ps ON (r.`data_name` = ps.`dataname`) WHERE r.`distro_code`='[1]'";
        if (!empty($search_text)) $sql .= " AND (`data_name` LIKE '%[2]%' OR `company_name` LIKE '%[2]%' OR `email` LIKE '%[2]%')";
        $q = mlavu_query($sql, $distro_code, $search_text);

        if ($q === false) {
            error_log(__FILE__ ." got db error with distro_code={$distro_code} search_text={$search_text}: ". mlavu_dberror());
        }
        else if (mysqli_num_rows($q) > 0) {
            while ( $account = mysqli_fetch_assoc($q) ) {
                $accounts[$account['data_name']] = $account;
            }
        }
    }

    return $accounts;
}

//Searches distro_user->accounts instead of hitting the database
function userAccountSearch()
{
    $account_searchString = $_POST['accountSearch']; 
    $distrocode = $_POST['account_filter_distro_code']; 
    $a_filters = getFilters();
    $o_current_user = new distro_user();	
    if($distrocode == ''){
            $distrocode = $_POST['distro_code'];
    }
    $o_current_user->set_all_info($distrocode);
    
    
    $serializedAccounts = $_POST['account_serialized'];
    $unserializedAccounts = getAccounts($distrocode);
    $location_contacts = $o_current_user->get_loc_contacts();
    
    if(!empty($account_searchString)){
        $accountsToSearch = findAccount($account_searchString, $unserializedAccounts);
        create_account_display($accountsToSearch, $location_contacts, $a_filters);
    }
    else create_account_display($unserializedAccounts, $location_contacts, $a_filters);
}

function findAccount($accountSearchString, $accounts){
    $accountsMatch = array();
    $matches = array();
    
    // Check for autocompleted search string: (<Rest ID>) <Account Name> (<Dataname>) <Email>
    preg_match_all('/\((.*?)\)/', $accountSearchString, $matches);
    if(count($matches) >= 1 && isset($matches[1]) && isset($matches[1][1]))
    {  
        $idString = $matches[1][0];
        $companyCodeString = $matches[1][0];
        foreach($accounts as $index=>$account)
        {
            if($account['id'] == $idString ||
               $account['company_code'] == $companyCodeString)
               array_push($accountsMatch, $accounts[$index]);
        }        
    }
    else
    {   
        foreach($accounts as $index=>$account)
        {
            if(stripos($account['company_name'], $accountSearchString) !== FALSE) array_push($accountsMatch, $accounts[$index]);
            else if(stripos($account['email'], $accountSearchString) !== FALSE) array_push($accountsMatch, $accounts[$index]);
            else if(stripos($account['data_name'], $accountSearchString)!== FALSE) array_push($accountsMatch, $accounts[$index]);
        }
    }
    return $accountsMatch;
}
//notes section in each account dropdown
function get_account_notes($data_name, $ln, $def_name){

	//restaraunt_notes entries where support_ninja = DISTRO_NOTE
	$ds_notes = $ln[$data_name];

	$notes = '
	<div id="notes_'.$data_name.'" >
		<div class="acc_drop_top">
			<span style="float:left;margin-left:20px;">
				<img src="./images/btn_addnew.png" class="acc_add_detail acc_add_note" style="cursor:pointer;" alt="btn_anew"/>
				<span style="margin-left:5px;">Notes</span>
			</span>
			<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
				<span>Edit</span>
				<img src="./images/icon_edit.png" alt="i_edit">
			</span>
			<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
				<span>Done</span>
				<img src="./images/check_green.png" alt="ch_grn">
			</span>
		</div>';//acc_drop_top
    $notes .= '<div class="accnt_detail_wrapper">';
    if($ds_notes){

    	foreach($ds_notes as $date=>$message){
        	$notes .= '
        	<div class="loc_note accnt_detail accnt_view">
        		<span class="loc_note_date">'.$date.'</span><span class="loc_note_note">'.$message.'</span>
        	</div>';
        }
    	$id = 0;

        $notes .= '
        <div class="acc_edit_details loc_note_form">
        	<form id="note_form_'.$data_name.'" >';
        foreach($ds_notes as $date=>$message){
        $message = trim($message);
        	$notes .= '
        	<div class="acc_edit_detail">
        		<span class="loc_note_date">'.$date.'</span>
        		<label for="n_'.$data_name.'_'.$id.'">NOTE:</label>
        		<input type="text" class="acc_note_input" id="n_'.$data_name.'_'.$id.'" name="n_'.$data_name.'_'.$id.'" value="'.$message.'"/>
        	</div>';//account_edit_detail
        	$id++;
        }
        $notes .= '<input type="hidden" id="add_note_hidden" name="add_note_hidden" value="" /></form></div>';//note_form and acc_edit_detail

    }else{
	    $notes .= '<div class="loc_note accnt_detail">NO NOTES FOUND</div>';
    }
    $notes .= '
    <div class="loc_add_detail loc_add_note accnt_detail">
    	<form class="loc_add_note_form">
    		<label for="note_to_add'.$def_name.'">Note:</label>
    		<input class="note_to_add_'.$data_name.' acc_note_input" dataname="'.$data_name.'" id="note_to_add'.$def_name.'" name="note_to_add'.$def_name.'" value="" />
    		<div class="loc_add_detail_submit_cancel">
    			<span class="loc_cancel_add_detail">Canel</span>
    			<span class="loc_submit_add_detail submit_new_note" val="note_to_add'.$def_name.'">Submit</span>
    		</div>
    	</form>
    </div>';//loc_add_note
	$notes .= '</div></div>';//whole note div and accnt_detail_wrapper
	return $notes;
}//get_account_notes

//contact section in each account dropdown
function get_account_contacts($data_name, $lc,$def_name){

	//restaraunt_notes entries where support_ninja = DISTRO_NOTE
	$ds_contacts = $lc[$data_name];

	$contacts = '<div id="contacts_'.$data_name.'" class="loc_contacts">
	<div class="acc_drop_top">
		<span style="float:left;margin-left:20px;">
			<img src="./images/btn_addnew.png" class="acc_add_detail acc_add_contact" alt="btn_a_n"/>
			<span>Contacts</span>
		</span>
		<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
			<span>Edit</span>
			<img src="./images/icon_edit.png" class="acc_edit_contact" alt="i_e" />
		</span>
		<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
				<span>Done</span>
				<img src="./images/check_green.png" alt="ch_grn" />
		</span>
	</div>
    ';//closes loc_con_top

    $contacts .= '<div class="accnt_detail_wrapper">';
    if($ds_contacts){

    	foreach($ds_contacts as $id){
    		$name = $id['firstname'].' '.$id['lastname'];
        	$contacts .=
        	'<div class="loc_contact accnt_detail accnt_view">
        		<span class="loc_contact_firstname">'.$id['firstname'].'</span>
        		<span class="loc_contact_lastname" style="margin-right:40px;">'.$id['lastname'].'</span>
        		<span class="loc_contact_phone" style="margin-right:40px;">'.$id['phone'].'</span>
        		<span class="loc_contact_email" style="margin-right:40px;">'.$id['email'].'</span>
        		<span class="loc_contact_note">'.$id['contact_note'].'</span>
        	</div>';
        }

        $contacts .= '
        <div class="acc_edit_details loc_contact_form">
        	<form id="contact_form_'.$data_name.'" >';
        $id_add = 0;
        foreach($ds_contacts as $id){
        	$fname = ($id['firstname'] != null)?trim($id['firstname']).'_'.$id_add:$def_name."_".$id_add;
        	$lname = ($id['lastname'] != null)?trim($id['lastname']).'_'.$id_add: $def_name."_".$id_add;
        	$phone = ($id['phone'] != null)?trim($id['phone']).'_'.$id_add:$def_name."_".$id_add;
        	$email = ($id['email'] != null)?trim($id['email']).'_'.$id_add:$def_name."_".$id_add;
        	$cnote = ($id['contact_note'] != null)?trim($id['contact_note']).'_'.$id_add:$def_name."_".$id_add;

        	$contacts .= '
        	<div class="acc_edit_detail">
        		<label for="fname_'.$fname.'">First Name:</label>
        		<input type="text" id="fname_'.$fname.'" name="fname_'.$fname.'" value="'.$fname.'"/>

        		<label for="lname_'.$lname.'">Last Name:</label>
        		<input type="text" id="lname_'.$lname.'" name="lname_'.$lname.'" value="'.$lname.'"/>

        		<label for="phone_'.$fname.'">Phone:</label>
        		<input type="text" id="phone_'.$fname.'" name="phone_'.$fname.'" value="'.$phone.'"/>

        		<label for="email_'.$fname.'">Email:</label>
        		<input type="text" id="email_'.$fname.'" name="email_'.$fname.'" value="'.$email.'"/>

        		<label for="cnote_'.$fname.'">Note:</label>
        		<input type="text" id="cnote_'.$fname.'" name="cnote_'.$fname.'" value="'.$cnote.'"/>
        	</div>';//account_edit_detail
        	$id_add++;
        }
        $contacts .= '</form></div>';//note_form and acc_edit_detail
    }else{
	        $contacts .= '<div class="loc_note accnt_detail">NO CONTACTS FOUND</div>';
    }
	$contacts .= '
    <div class="loc_add_detail loc_add_contact accnt_detail">
    	<form class="loc_add_contact_form">
    		<label for="add_fname_'.$data_name.'">First Name:</label>
        	<input type="text" id="add_fname_'.$data_name.'" name="add_fname_'.$data_name.'" value=""/>
        	<label for="add_lname_'.$data_name.'">Last Name:</label>
        	<input type="text" id="add_lname_'.$data_name.'" name="add_lname_'.$data_name.'" value=""/>
        	<label for="add_phone_'.$data_name.'">Phone:</label>
        	<input type="text" id="add_phone_'.$data_name.'" name="add_phone_'.$data_name.'" value=""/>
        	<label for="add_email_'.$data_name.'">Email:</label>
        	<input type="text" id="add_email_'.$data_name.'" name="add_email_'.$data_name.'" value=""/>
        	<label for="add_cnote_'.$data_name.'">Note:</label>
        	<input type="text" id="add_cnote_'.$data_name.'" name="add_cnote_'.$data_name.'" value=""/>
    		<div class="loc_add_detail_submit_cancel">
    			<span class="loc_cancel_add_detail">Cancel</span>
    			<span class="loc_submit_add_detail submit_new_contact">Submit</span>
    		</div>
    	</form>
    </div>';//loc_add_contact
	$contacts .= '</div></div>';//closes loc_contacts and accnt_detail_wrapper
	return $contacts;
}//get_account_contacts

//location section in each account dropdown
function get_account_location($loc_deets){
	global $o_package_container;

	$address = ($loc_deets['address'] != null)?trim($loc_deets['address']):"";
	$city = ($loc_deets['city'] != null)?trim($loc_deets['city']):"";
	$state = ($loc_deets['state'] != null)?trim($loc_deets['state']):"";
	$zip = ($loc_deets['zip'] != null)?trim($loc_deets['zip']):"";
	$loc = '
	<div class=loc_loc>
		<div class="acc_drop_top">
			<span style="float:left;margin-left:20px;">
				<span>Location</span>
			</span>
			<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
				<span>Edit</span>
				<img src="./images/icon_edit.png" class="acc_edit_location" alt="icon_edit" />
			</span>
			<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
				<span>Done</span>
				<img src="./images/check_green.png" alt="check_green" />
			</span>
		</div>';//acc_drop_top
	$loc .= '
	<div class="accnt_detail_wrapper">
		<div class="accnt_view">
			<div style="text-align:left;width:710px;margin:3px auto;font:10px Helvetica;color:#717074;letter-spacing:0.075em;">
				<div style="font:10px Verdana;">Address: '.$address.'</div>
				<div style="font:10px Verdana;"><span>'.$city.', </span><span>'.$state.', </span><span>'.$zip.'</span></div>
			</div>
		</div>';//accnt_view
    $loc .= '
        <div class="acc_edit_details loc_loc_form">
        	<form id="loc_form_'.$loc_deets['data_name'].'" >
        	<div class="acc_edit_detail">
        		<label for="loc_cont_address_'.$loc_deets['data_name'].'">Address:</label>
        		<input type="text" id="loc_cont_address_'.$loc_deets['data_name'].'" name="loc_cont_address_'.$loc_deets['data_name'].'" value="'.$address.'"/>
        	</div>
        	<div class="acc_edit_detail">
        		<label for="loc_cont_city_'.$loc_deets['data_name'].'">City:</label>
        		<input type="text" id="loc_cont_city_'.$loc_deets['data_name'].'" name="loc_cont_city_'.$loc_deets['data_name'].'" value="'.$city.'"/>
        	</div>
        	<div class="acc_edit_detail">
        		<label for="loc_cont_state_'.$loc_deets['data_name'].'">State:</label>
        		<input type="text" id="loc_cont_state_'.$loc_deets['data_name'].'" name="loc_cont_state_'.$loc_deets['data_name'].'" value="'.$state.'"/>
        	</div>
        	<div class="acc_edit_detail">
        		<label for="loc_cont_zip_'.$loc_deets['data_name'].'">Zip:</label>
        		<input type="text" id="loc_cont_zip_'.$loc_deets['data_name'].'" name="loc_cont_zip_'.$loc_deets['data_name'].'" value="'.$zip.'"/>
        	</div>';//account_edit_detail
        $loc .= '</form></div>';//loc_loc_form and acc_edit_details

	$loc .= '</div></div>';//closes accnt_detail and loc_loc
	return $loc;
}//get_account_locations


//
//
function create_account_display($accounts, $location_contacts,$a_filters){
	//logic to get created/good until fields and format them
	
	//$content = '<div>start<br />';
	if (!isset($o_package_container)){
		$o_package_container = new package_container();
	}
	global $o_current_user;
	if(!isset($o_current_user)){
		error_log("distro/beta/exec->create_account_display:: $o_distro_user not set");

	}

	global $s_licenses_not_upgrades; // names of licenses the user has that aren't upgrades (as bar-delimeted string)
	global $a_licenses_not_upgrades; // all distro licenses that aren't upgrades.
	global $a_licenses_upgrades; // alll distro licenses that are upgrades.
	global $a_licenses_not_upgrades_not_special;

	$user_name = $_POST['distro_code'];
	//echo 'not martin == '.var_dump($_GET);//$_REQUEST['notmartin'].'<br />';
	$o_current_user->set_all_info($user_name);

	$licenses = $o_current_user->get_licenses();
	//echo(count($licenses));
	$a_licenses_not_upgrades = array();
	$a_licenses_upgrades = array();
	foreach($licenses as $s_id=>$a_license) {
		$s_license_name = $a_license['type'];
		if ($o_package_container->get_package_status($s_license_name) != "upgrade") {
			$a_license['printed_name'] = $o_package_container->get_printed_name_by_attribute('name', str_replace(" Promo", "", $a_license['type']));
			$a_license['is_special'] = $o_current_user->license_is_special($s_id);
			$a_licenses_not_upgrades[$s_id] = $a_license;
		} else {
			$a_license['printed_name'] = $o_package_container->get_upgrade_printed_name($a_license['type']);
			$a_license['is_special'] = $o_current_user->license_is_special($s_id);
			$a_licenses_upgrades[$s_id] = $a_license;
		}
	}

	// get a string representing all licenses (not upgrades)
	$s_licenses_not_upgrades = implode('|', $a_licenses_not_upgrades);

	// get a string representing all licenses (not upgrades, not special licenses)
	$a_licenses_not_upgrades_not_special = array('all'=>array(), 'Silver'=>array(), 'Gold'=>array(), 'Platinum'=>array());
	foreach($a_licenses_not_upgrades as $s_id=>$a_license) {
		if ($a_license['is_special'] != 0)
			continue;
		$a_licenses_not_upgrades_not_special['all'][] = $s_id;
		$a_licenses_not_upgrades_not_special[$a_license['printed_name']][] = $s_id;
	}
	foreach($a_licenses_not_upgrades_not_special as $k=>$v)
		$a_licenses_not_upgrades_not_special[$k] = implode('|', $v);

	$s_licenses_not_upgrades = $s_licenses_not_upgrades;
	$a_licenses_not_upgrades_not_special = $a_licenses_not_upgrades_not_special;


	foreach($accounts as $s_data_name=>$attrs){
		$trial_end = $attrs['trial_end'];
		$created = tab_container::display_datetime((string)$attrs['created'], "date");
		// get the licensing info for the account
		$license_applied = $attrs['license_applied'];
		$license_type = $attrs['license_type'];
		
		//error_log("distro/beta/exec/searchAccountTemp.php #286".$attrs['package']);
		if (is_numeric($attrs['package'])) {
			$original_license = $license_applied;
			$license_applied = (int)$attrs['package'];
			$license_type = $o_package_container->get_plain_name_by_attribute('level', $attrs['package']);
		}
		// get applicable special licenses that can be applied to this account
		//error_log("distro/beta/exec/searchAccountTemp#291:: foreach accounts s_data_name::".$s_data_name);
		$a_applicable_special_licenses = $o_current_user->get_applicable_special_licenses($s_data_name);
		$s_special_licenses = (count($a_applicable_special_licenses) == 0 ? '' : '<input type="hidden" name="available_special_licenses" value="'.implode(',', $a_applicable_special_licenses).'" />');
		// get applicable licenses that can be applied to this account
		$a_licenses_strings = $a_licenses_not_upgrades_not_special;

		//error_log("trial_end=$trial_end, original_license=$original_license, license_type=$license_type, data_name=".$attrs['data_name'].", a_licenses_string=$a_licenses_strings, s_special_licenses=$s_special_licenses, o_current_user=$o_current_user, has_license_payment=".$attrs['has_license_payment']);
		$level_display = tab_container::display_level($trial_end, $original_license, $license_type, $attrs['data_name'], $a_licenses_strings, $s_special_licenses, $o_current_user, $attrs['has_license_payment']);
		// get the "good_until" display string
		if(strpos($level_display, 'Silver') !== FALSE){
			//echo $_POST['silver'].'<br />';
			//echo '<br />::'.strpos($level_display, 'Silver').$level_display.'<br />';
		}
		//echo $level_display.'<br />';
		if($trial_end==""){
			$good_until = "n/a";
		}else{
			$good_until = tab_container::display_datetime((string)$trial_end,"date");
		}
		// get the dataname
		$dataname = str_replace(" ","",$attrs['data_name']);
		// build the account's specific display row
		if($dataname == 'distrotest'){
			//echo (var_dump($attrs));
		}
	/*
	   $$a_filters['distro_id'] = $distroid;
	   $a_filters['distro_code'] = $distrocode;
	   $a_filters['time_window'] = $_POST['account_tab_filter_time'];
	   $a_filters['silver'] = isset($_POST['account_filter_silver']) ? "Silver" : '';
	   $a_filters['gold'] = isset($_POST['account_filter_gold']) ? "Gold" : '';
	   $a_filters['pro'] = isset($_POST['account_filter_pro']) ? "Platinum" : '';
	   $a_filters['88'] = isset($_POST['account_filter_88']) ? "88" : '';
	   $a_filters['trial'] = sset($_POST['account_filter_trial']) ? 'btn_apply' : '';
	   $a_filters['self'] = isset($_POST['account_filter_self']) ? "Self" : '';
	   $a_filters['canceled'] = $_POST['account_filter_canceled'];
	*/
	   $include_account = FALSE;
	   $trial_boolean = ($a_filters['trial'] == 'n/a') ? TRUE : FALSE;
	   if($trial_boolean){
	   	//echo '<br />trial truelean<br />';
	   }
	/*
	   if($a_filters['self'] == "Self"){
	   		if(strpos($level_display, $a_filters['self']) !== FALSE){
	   			$include_account = TRUE;
	   			echo 'switch silver plus self<br />';
	   			break;
	   		}
	   	}
	*/
	   
	   	if($a_filters['self'] == "Self")
	   	{
	   		if(isset($_POST['account_filter_pro']))
	   		{
	   			$a_filters['pro'] = "Pro";
	   			//echo 'switch pro plus self<br />';
	   		}else if(isset($_POST['account_filter_gold']))
	   		{
	   			$a_filters['gold'] = "Self Signup - Gold";
	   			//echo 'switch pro plus self<br />';
	   		}else if(isset($_POST['account_filter_silver']))
	   		{
	   			$a_filters['silver'] = "Self Signup - Silver";
	   			//echo 'switch pro plus self<br />';
	   		}else if(isset($_POST['account_filter_88']))
	   		{
	   			$a_filters['lavu88'] = "Self Signup - Lavu 88";
	   			//echo 'switch pro plus self<br />';
	   		}else{
	   			$a_filters['pro'] = "Self";
	   		}
	    }//self signup string change
	   	if(strpos($level_display, 'btn_apply')){
	   		//echo '<br >'.$level_display.'<br>';	   		
	   	}


	   //Switch case
	   switch($level_display){
	   	case ($a_filters['filter_length'] == 'all'):
	   	//echo '<br />no filters<br />';
	   	$include_account = TRUE;
	   	break;
	   	case (strpos($level_display, $a_filters['silver']) !== FALSE):
	   	if($trial_boolean){
	   		if(strpos($good_until, $a_filters['trial']) === FALSE){
	   			//echo '<br />TRIAL SILVER<br />';
	   			$include_account = TRUE;
	   			break;
	   		}else{
	   			//echo '<br />NO TRIAL SILVER<br />';
	   			$include_account = FALSE;
	   			break;
	   		}
	   	}
	   	//echo '<br />switch silver<br />';
	   	$include_account = TRUE;
	   	break;
	   	case (strpos($level_display, $a_filters['gold']) !== FALSE):
	   	if($trial_boolean){
	   		if(strpos($good_until, $a_filters['trial']) === FALSE){
	   			//echo '<br />TRIAL SILVER<br />';
	   			$include_account = TRUE;
	   			break;
	   		}else{
	   			//echo '<br />NO TRIAL SILVER<br />';
	   			$include_account = FALSE;
	   			break;
	   		}
	   	}
	   	//echo '<br />switch gold<br />';
	   	$include_account = TRUE;
	   	break;
	   	case (strpos($level_display, $a_filters['pro']) !== FALSE):
	   	if($trial_boolean){
	   		if(strpos($good_until, $a_filters['trial']) === FALSE){
	   			//echo '<br />TRIAL SILVER<br />';
	   			$include_account = TRUE;
	   			break;
	   		}else{
	   			//echo '<br />NO TRIAL SILVER<br />';
	   			$include_account = FALSE;
	   			break;
	   		}
	   	}
	   	//echo '<br />switch pro<br />';
	   	$include_account = TRUE;
	   	case (strpos($level_display, $a_filters['lavu88']) !== FALSE):
	   	if($trial_boolean){
	   		if(strpos($good_until, $a_filters['trial']) === FALSE){
	   			//echo '<br />TRIAL SILVER<br />';
	   			$include_account = TRUE;
	   			break;
	   		}else{
	   			//echo '<br />NO TRIAL SILVER<br />';
	   			$include_account = FALSE;
	   			break;
	   		}
	   	}
	   	//echo '<br />switch 88<br />';
	   	$include_account = TRUE;
	   	break;
	   	case $trial_boolean:
	   	if(strpos($good_until, $a_filters['trial']) === FALSE){
	   		//echo '<br />TRIAL SILVER<br />';
	   		$include_account = TRUE;
	   		break;
	   	}else{
	   		//echo '<br />NO TRIAL SILVER<br />';
	   		$include_account = FALSE;
	   		break;
	   	}
	   	//$include_account = TRUE;
	   	break;

	   }//switch($level_display)
	   

	   //time range
	   if($include_account && (strlen($a_filters['time_window']) > 1)){
	   	//echo '<br />time window<br />';
	   	//echo '<br />'.$dataname.'<br />';
	   	$time_length = $a_filters['time_window'];
		$date_begin_time = date('Y-m-d',strtotime($time_length));

		$created_formated = substr($created,0,-2).'20'.substr($created, -2);
		$created_formated = str_replace('-', '/', $created_formated);
		//echo $created.', '.$created_formated.', '.strtotime($created_formated).'<br />';
		$date_created = strtotime($created_formated);
		$date_filter = strtotime($date_begin_time);

		if($date_filter > $date_created){
			$include_account = FALSE;
		}

		//echo 'filter < created='.($date_filter < $date_created).'<br />';
		//echo 'filter > created='.($date_filter > $date_created).'<br />';
		//echo '<br />filter choice:: '.$date1.', created:: '.$date2.'<br />';
		//$created_formated = $date_front.$date_back;
		//echo $date_begin_time.' < '.$created_formated.'<br />';
		
		//$date_now = date('Y-m-d H:i:s');
		//$s_time_filter = "and `created` < '$date_now' and `created` > '$date_begin_time'";
		

	   }
	 
	   if($include_account){
	   	//echo isset($_POST['account_filter_silver']) ? "Silver<br />" : 'No Silver<br />';
	   	$content .= '
	   	<div class="acc_head accordionButton acc_count acc_'.$dataname.'">
	   		<img class="acc_arrow expand_account_btn" src="./images/arrow_left.png" style="float:left;" alt="a_l"/>
	   		<span style="width:220px;"><a href="./exec/backend_account_login.php?loginto='.$dataname.'&loggedin='.$uname.'&email='.$user_email.'" target="_blank">'.$attrs['company_name'].'</a></span>
	   		<span style="width:150px;">'.$created.'</span>
	   		<span style="width:150px;">'.$good_until.'</span>
	   		<span style="width:160px;text-align:center;">'.$level_display.'</span>
	   		<span style="width:100px;text-align:center;display:none;"><a href="./exec/backend_account_login.php?loginto='.$dataname.'&loggedin='.$uname.'&email='.$user_email.'" target="_blank">login</a></span>
	   		<input type="hidden" name="dataname" value="'.$dataname.'" />
	   	</div>
	   	';
	   // build the account's specific data row that the javascript uses to pull for everything
	   	$content .= '<div class="acc_accnt_content accordionContent">
	   	';
	   	$content .= get_account_notes($dataname, $location_notes, $contact_default_name);
	   	$content .= get_account_contacts($s_data_name, $location_contacts,$contact_default_name);
	   	$contact_default_name++;
	   	$content .= get_account_location($attrs);
	   	$content .= '
	   </div>';
	}//FILTER CHECK STATEMENT
		
	}//foreach($accounts as $s_data_name=>$attrs)
	echo $content;  // TO DO : see if this needs div here
}//create_account_display

//
//->display_level:: 2013-11-22 00:00:00, , Silver2, Array, , Object id #36, 1,
//->display_level:: 2013-11-22 00:00:00, , Silver2, , , Object id #36, ,
function set_account_info($entry){
	$accounts = array();
	$a_query_params = array();
	$loc_notes = array();
	$loc_contacts = array();
	$dname = $entry['data_name'];	
	$d_id = $entry['id'];
	$a_query_params['restaurant_id'] = $d_id;
	$a_query_params['data_name'] = $dname;
	$a_query_params['db_restaurants'] = '`poslavu_MAIN_db`.`restaurants`';
	$a_query_params['db_rest_locations'] = '`poslavu_MAIN_db`.`restaurant_locations`';
	$a_query_params['db_payment_status'] = '`poslavu_MAIN_db`.`payment_status`';
	$a_query_params['distro_code'] = $entry['distro_code'];//$distro_code;
	//$a_query_params['distro_id'] = $distro_id;
	$a_query_params['loc_fields'] = '`address`,`city`,`state`,`zip`,`country`,`manager`';
	//print_r($a_query_params);
	//
	//
	//query to get info from restaurant_locations table
	$loc_query = mlavu_query("select [loc_fields] from [db_rest_locations] where `dataname`='[data_name]'",$a_query_params);// or die(mlavu_dberror("distro/beta/exec/searchAccountTemp->#402 error"));
	$d_loc_fields = mysqli_fetch_assoc($loc_query);
	mysqli_free_result($loc_query);
	//
	//
	//query to get information from payment_status table
	$lead_stat_query = mlavu_query("select * from [db_payment_status] where `dataname`='[data_name]'",$a_query_params);// or die(mlavu_dberror("distro/beta/exec/searchAccountTemp->#409"));
	$d_lead_stat = mysqli_fetch_assoc($lead_stat_query);
	mysqli_free_result($lead_stat_query);
	//
	//
	//query to get information from restaurant_notes table
	$db_notes = '`poslavu_MAIN_db`.`restaurant_notes`';
	$n_r_id = '`restaurant_id`';
	$n_s_ninja = '`support_ninja`';
	$a_query_params['db_rest_notes'] = $db_notes;
	$a_query_params['support_ninja'] = $n_s_ninja;
	//echo 'query::<br />'."select * from $db_notes where $n_r_id='$d_id' and $n_s_ninja='DISTRO_NOTE'<br /><br />";
	$res_notes_query = mlavu_query("select * from [db_rest_notes] where `restaurant_id`='[restaurant_id]' and [support_ninja]='DISTRO_NOTE'",$a_query_params) or die(mlavu_dberror("distro/bet/exec/searchAccountTemp->#422 error"));
	while($d_notes_stat = mysqli_fetch_assoc($res_notes_query)){// load all notes into loc_notes
		//echo '<br /><br /><br /><br /><br />'.var_dump($d_notes_stat);
		//foreach($d_notes_stat as $nkey=>$nvalue){
		$mdate = $d_notes_stat['created_on'];
		$mmessage = $d_notes_stat['message'];
		$loc_notes[$dname][$mdate] = $mmessage;
			//echo 'NOTE INFO::::<br />dname= '.$dname.'<br /> key= '.$mdate.' <br />value= '.$mmessage.'<br /><br />';
		//}
		unset($mdate);
		unset($mmessage);
		unset($d_notes_stat);
	}//end while loop
	mysqli_free_result($res_notes_query);
	//
	//
	//query to get information from signups table
	$db_signups = '`poslavu_MAIN_db`.`signups`';
	$s_r_id = '`restaurantid`';
	$a_query_params['db_signups'] = $db_signups;
	//echo 'query::<br />'."select * from $db_signups where $s_r_id='$d_id'<br /><br />";
	//select * from `poslavu_MAIN_db`.`signups` where `restaurantid`='[1]' or `dataname`='[2]'
	$signups_query = mlavu_query("select * from [db_signups] where `restaurantid`='[restaurant_id]' or `dataname`='[data_name]'",$a_query_params) or die(mlavu_dberror("distro/beta/exec->#445 error"));
	///display_diff_mem_usage();
	///error_log("bbbbbbbb");
	$p_level = '';
	$signup_timestamp = '';
	if($signups_stat = mysqli_fetch_assoc($signups_query)){//
		//echo '$signups_stat[dataname] = '.$signups_stat['dataname'].'<br />';//'<br /><br /><br /><br /><br />'.var_dump($signups_stat);
		$first = $signups_stat['firstname'];
		$last = $signups_stat['lastname'];
		$signup_id = $signups_stat['id'];
		//error_log("distro/beta/exec/searchAccountTemp:: #403 plevel=".$signups_stat['package']);
		/*	$p_level = $signups_stat['package'];
			$signup_timestamp = $signups_stat['signup_timestamp'];
			$d_name = $signups_stat['dataname'];
			$rest_company = $signups_stat['company'];
			$rest_email = $signups_stat['email'];
			//echo 'pl::'.$package_level.'<br />';
			$leads_db = '`poslavu_MAIN_db`.`reseller_leads`';
		if($p_level != '' && $p_level != 'none'){
			//mlavu_query("update [1] set `license_applied`='[2]' where `company_name`='[3]' and `email`='[4]' limit 1", $leads_db, $signup_timestamp, $rest_company, $rest_email);
			if($rest_company == 'Shiki'){
			//error_log("not a bug ben:::: ".$rest_company.', '.$rest_email.', '.$p_level);
			}
		}*/
		foreach($signups_stat as $ckey=>$cvalue){
			$loc_contacts[$dname][$signup_id][$ckey] = $cvalue;
		}
		$other_contacts_field = $signups_stat['other_contacts'];
		//echo strlen($other_contacts_field).'<br />';
		if(strpos($other_contacts_field, ':*:')){
			//echo "oc";
			$other_contacts = explode('|',$other_contacts_field);
			//echo count($other_contacts);
			$other_count = 1;
			foreach($other_contacts as $contact){
				//echo $c_info[0].", ".$c_info[1].", ".$c_info[2].", ".$c_info[3].", ".$c_info[4]."<br />";
				$o_id = $signup_id.'_'.$other_count;
				$c_info = explode(':*:', $contact);
				if($c_info[0] != ''){
					$loc_contacts[$dname][$o_id]['firstname'] = $c_info[0];
					$loc_contacts[$dname][$o_id]['lastname'] = $c_info[1];
					$loc_contacts[$dname][$o_id]['phone'] = $c_info[2];
					$loc_contacts[$dname][$o_id]['email'] = $c_info[3];
					$loc_contacts[$dname][$o_id]['contact_notes'] = $c_info[4];
					$other_count++;
				}
			}
		}
		//echo 'CONTACT INFO::::<br />dname= '.$dname.'<br /> key= '.$ckey.' <br />value= '.$cvalue.'<br /><br />';
		//}
	}//end if($signups_stat = mysqli_fetch_assoc($signups_query))
	//fill in the accounts information
	$accounts[$dname] = $entry;
	//information from restaurant_location table
	if($d_loc_fields != null){
		//error_log("distro/beta/searchAccountTemp->#444".count($d_loc_fields));
		$accounts[$dname] = array_merge($d_loc_fields, $accounts[$dname]);
	}
	//information from payment_status table
	error_log("distro/beta/exec/searchAccountTemp:: #451 plevel=".$p_level);
	if($d_lead_stat != null)
		$accounts[$dname] = array_merge($d_lead_stat, $accounts[$dname]);
	if($p_level != ''){
		// set the package level to what was found (aka $a_package_data['package'])
		error_log("distro/beta/exec/searchAccountTemp:: #455 plevel=".$p_level);
		$accounts[$dname]['package'] = $p_level;
	}else{
		// set the package level because the package level wasn't previously found
		$a_package_data = find_package_status($entry['id']);
		$accounts[$dname]['package'] = $a_package_data['package'];
		if ($signup_id != '' && is_numeric($signup_id)) {
			mlavu_query("UPDATE [signupsdb] SET `package`='[package]' WHERE `id`='[signupid]'",
				array("signupsdb"=>$db_signups,"package"=>$a_package_data['package'],"signupid"=>$signup_id));
		}
	}
	//echo "set_account_info made:: length:: ".print_r($accounts);
	create_account_display($accounts, $loc_contacts);
}//set_account_info($entry)

//builds an array of all distro's account information
function build_accounts_array($distro_id, $distro_code, $a_filters, $account_searchString){
	//global $o_package_container;
        //lavuLog("Building Array");
	$accounts = array();
	$loc_contacts = array();
	$loc_notes = array();
        //parsing $account_searchString for autocomplete flags
        preg_match_all('/\(.*?\)/', $account_searchString, $matches);
        if(count($matches) >= 1)
        {
            $idString = substr($matches[0][0], 1, -1);        
            $companyCodeString = substr($matches[0][1], 1, -1);
        }
        
	$a_query_params = array();
	$a_query_params['db_restaurants'] = '`poslavu_MAIN_db`.`restaurants`';
	$a_query_params['db_rest_locations'] = '`poslavu_MAIN_db`.`restaurant_locations`';
	$a_query_params['db_payment_status'] = '`poslavu_MAIN_db`.`payment_status`';
	$a_query_params['distro_code'] = $distro_code;
	$a_query_params['distro_id'] = $distro_id;
        $a_query_params['account_searchString'] = $account_searchString;
        $a_query_params['account_id'] = $idString;
        $a_query_params['account_company_code'] = $companyCodeString;
	$a_query_params['loc_fields'] = '`address`,`city`,`state`,`zip`,`country`,`manager`';
        $restaurants_query_string = ''; //initialize query string
	//and (`package_status`='Silver3' or `package_status`='Silver2' or `package_status`='Silver')
        
        if(!empty($idString))
        {
            $restaurants_query_string = "select * from [db_restaurants] where "
                    . "`distro_code`='[distro_code]'  and "
                    . "`distro_code`!='' and "
                    . "`id`='[account_id]'  or "
                    . "`company_code`='[account_company_code]' "
                    . " order by `created` desc";            
        }
        else if(empty($account_searchString))
        {
            $restaurants_query_string = "select * from [db_restaurants] where"
                    . " `distro_code`='[distro_code]' and "
                    . " `distro_code`!=''"
                    . " order by `created` desc";
        }
        else //search string isn't empty, attempt to search
        {
            $restaurants_query_string = "select * from [db_restaurants] where "
                    . "`distro_code`='[distro_code]'  and "
                    . "`distro_code`!='' and "
                    . "`company_name`='[account_searchString]' or "
                    . "`email`='[account_searchString]' or "
                    . "`company_code`='[account_searchString]'"
                    . " order by `created` desc";        
        }       
	//	$_SESSION['update_accounts'] = '';
	//error_log("distro/beta/exec/searchAccountTemp#482: select * from '`poslavu_MAIN_db`.`restaurants`' where `distro_code`='".$distro_code."'");
	$restaurants_query = mlavu_query($restaurants_query_string, $a_query_params);// or die(mlavu_dberror("disto_user->set_leads_accounts_restaurants_query error"));
	
	//$restaurants_query = mlavu_query($restaurants_query_string, $restaurants_query_vars) or die(mlavu_dberror("disto_user->set_leads_accounts_restaurants_query error"));
	while($entry = mysqli_fetch_assoc($restaurants_query)){
		//display_diff_mem_usage();
		//error_log("cost of restaurant");
		$dname = $entry['data_name'];
		$a_query_params['data_name'] = $dname;
		$d_id = $entry['id'];
		$a_query_params['restaurant_id'] = $d_id;
		//
	    //
	    //query to get info from restaurant_locations table
	    $loc_query = mlavu_query("select [loc_fields] from [db_rest_locations] where `dataname`='[data_name]'",$a_query_params);// or die(mlavu_dberror("distro/beta/exec/searchAccountTemp->#402 error"));
	    $d_loc_fields = mysqli_fetch_assoc($loc_query);
	    mysqli_free_result($loc_query);
	    //
	    //
	    //query to get information from payment_status table
	    $lead_stat_query = mlavu_query("select * from [db_payment_status] where `dataname`='[data_name]'",$a_query_params);// or die(mlavu_dberror("distro/beta/exec/searchAccountTemp->#409"));
	    $d_lead_stat = mysqli_fetch_assoc($lead_stat_query);
	    mysqli_free_result($lead_stat_query);
	    //
	    //
	    //query to get information from restaurant_notes table
	    $db_notes = '`poslavu_MAIN_db`.`restaurant_notes`';
	    $n_r_id = '`restaurant_id`';
	    $n_s_ninja = '`support_ninja`';
	    $a_query_params['db_rest_notes'] = $db_notes;
	    $a_query_params['support_ninja'] = $n_s_ninja;
	    //echo 'query::<br />'."select * from $db_notes where $n_r_id='$d_id' and $n_s_ninja='DISTRO_NOTE'<br /><br />";
	    $res_notes_query = mlavu_query("select * from [db_rest_notes] where `restaurant_id`='[restaurant_id]' and [support_ninja]='DISTRO_NOTE'",$a_query_params) or die(mlavu_dberror("distro/bet/exec/searchAccountTemp->#422 error"));
	    while($d_notes_stat = mysqli_fetch_assoc($res_notes_query)){// load all notes into loc_notes
		    //echo '<br /><br /><br /><br /><br />'.var_dump($d_notes_stat);
		    //foreach($d_notes_stat as $nkey=>$nvalue){
		    $mdate = $d_notes_stat['created_on'];
		    $mmessage = $d_notes_stat['message'];
		    $loc_notes[$dname][$mdate] = $mmessage;
			//echo 'NOTE INFO::::<br />dname= '.$dname.'<br /> key= '.$mdate.' <br />value= '.$mmessage.'<br /><br />';
		    //}
		    unset($mdate);
		    unset($mmessage);
		    unset($d_notes_stat);
	    }//end while loop
	    mysqli_free_result($res_notes_query);
	    //
	    //
	    //query to get information from signups table
	    $db_signups = '`poslavu_MAIN_db`.`signups`';
	    $s_r_id = '`restaurantid`';
	    $a_query_params['db_signups'] = $db_signups;
	    //echo 'query::<br />'."select * from $db_signups where $s_r_id='$d_id'<br /><br />";
	    //select * from `poslavu_MAIN_db`.`signups` where `restaurantid`='[1]' or `dataname`='[2]'
	    $signups_query = mlavu_query("select * from [db_signups] where `restaurantid`='[restaurant_id]' or `dataname`='[data_name]'",$a_query_params) or die(mlavu_dberror("distro/beta/exec->#445 error"));
	    ///display_diff_mem_usage();
	    ///error_log("bbbbbbbb");
	    $p_level = '';
	    $signup_timestamp = '';
	    if($signups_stat = mysqli_fetch_assoc($signups_query)){//
		    //echo '$signups_stat[dataname] = '.$signups_stat['dataname'].'<br />';//'<br /><br /><br /><br /><br />'.var_dump($signups_stat);
		    $first = $signups_stat['firstname'];
		    $last = $signups_stat['lastname'];
		    $signup_id = $signups_stat['id'];
		    /*	$p_level = $signups_stat['package'];
			    $signup_timestamp = $signups_stat['signup_timestamp'];
			    $d_name = $signups_stat['dataname'];
			    $rest_company = $signups_stat['company'];
			    $rest_email = $signups_stat['email'];
			    //echo 'pl::'.$package_level.'<br />';
			    $leads_db = '`poslavu_MAIN_db`.`reseller_leads`';
		    if($p_level != '' && $p_level != 'none'){
			    //mlavu_query("update [1] set `license_applied`='[2]' where `company_name`='[3]' and `email`='[4]' limit 1", $leads_db, $signup_timestamp, $rest_company, $rest_email);
			    if($rest_company == 'Shiki'){
			    //error_log("not a bug ben:::: ".$rest_company.', '.$rest_email.', '.$p_level);
			}
		    }*/
		    foreach($signups_stat as $ckey=>$cvalue){
			    $loc_contacts[$dname][$signup_id][$ckey] = $cvalue;
		    }
		    $other_contacts_field = $signups_stat['other_contacts'];
		    //echo strlen($other_contacts_field).'<br />';
		    if(strpos($other_contacts_field, ':*:')){
			    //echo "oc";
			    $other_contacts = explode('|',$other_contacts_field);
			    //echo count($other_contacts);
			    $other_count = 1;
			    foreach($other_contacts as $contact){
				    //echo $c_info[0].", ".$c_info[1].", ".$c_info[2].", ".$c_info[3].", ".$c_info[4]."<br />";
				    $o_id = $signup_id.'_'.$other_count;
				    $c_info = explode(':*:', $contact);
				    if($c_info[0] != ''){
					    $loc_contacts[$dname][$o_id]['firstname'] = $c_info[0];
					    $loc_contacts[$dname][$o_id]['lastname'] = $c_info[1];
					    $loc_contacts[$dname][$o_id]['phone'] = $c_info[2];
					    $loc_contacts[$dname][$o_id]['email'] = $c_info[3];
					    $loc_contacts[$dname][$o_id]['contact_notes'] = $c_info[4];
					    $other_count++;
				    }
			    }
		    }
		    //echo 'CONTACT INFO::::<br />dname= '.$dname.'<br /> key= '.$ckey.' <br />value= '.$cvalue.'<br /><br />';
		    //}
	    }//end if($signups_stat = mysqli_fetch_assoc($signups_query))
	    //fill in the accounts information
	    $accounts[$dname] = $entry;
	    //information from restaurant_location table
	    if($d_loc_fields != null){
		   // error_log("distro/beta/searchAccountTemp->#453".count($d_loc_fields));
		    $accounts[$dname] = array_merge($d_loc_fields, $accounts[$dname]);
	    }
	    //information from payment_status table
	    if($d_lead_stat != null)
		    $accounts[$dname] = array_merge($d_lead_stat, $accounts[$dname]);
	    if($p_level != ''){
		    // set the package level to what was found (aka $a_package_data['package'])
		    $accounts[$dname]['package'] = $p_level;
	    }else{
		    // set the package level because the package level wasn't previously found
		    $a_package_data = find_package_status($entry['id']);
		    $accounts[$dname]['package'] = $a_package_data['package'];
		    if ($signup_id != '' && is_numeric($signup_id)) {
			    mlavu_query("UPDATE [signupsdb] SET `package`='[package]' WHERE `id`='[signupid]'",
				    array("signupsdb"=>$db_signups,"package"=>$a_package_data['package'],"signupid"=>$signup_id));
		    }
	    }
	}//each restaurant while loop
	//$accounts = insert_payment_responses_information_into_accounts($accounts);
	create_account_display($accounts, $loc_contacts,$a_filters);
}//build_account_array
// gets information from the payment_responses table and inserts it into the account
		// searches specifically for a valid license payment and sets $a_account['has_license_payment'] = TRUE or FALSE, depending
function insert_payment_responses_information_into_accounts($accounts) {
	//require_once(dirname(__FILE__).'/../account_functions.php');
	if (count($accounts) == 0){
		error_log("insert_payment_responses == 0");
		return;
	}
	// get a list of account ids to datanames
	$a_ids_to_dns = array();
	foreach($accounts as $data_name=>$a_account)
		$a_ids_to_dns[$a_account['id']] = $data_name;
	// default to TRUE
	foreach($a_ids_to_dns as $id=>$data_name)
		$accounts[$data_name]['has_license_payment'] = TRUE;
	// query the database for payment_responses that have x_response_code = 1 and payment_type = license
	$a_ids = array();
	foreach($a_ids_to_dns as $id=>$dataname)
		$a_ids[] = $id;
	$a_paid_ids = get_accounts_with_paid_licenses($a_ids);
	// set the data in the accounts
	foreach($a_paid_ids as $id=>$b_paid) {
		$data_name = $a_ids_to_dns[$id];
		$accounts[$data_name]['has_license_payment'] = $b_paid;
	}
	unset($a_ids);
	unset($a_paid_ids);
	unset($a_response);
}
function get_accounts_with_paid_licenses($a_ids, $b_default = TRUE) {
		// escape the ids and create the query string
		foreach($a_ids as $k=>$id)
			$a_ids[$k] = ConnectionHub::getConn('poslavu')->escapeString($id);
		$s_in_clause = "IN ('".implode("','", $a_ids)."')";
		$s_query_str = "SELECT `match_restaurantid` FROM `poslavu_MAIN_db`.`payment_responses` WHERE `x_response_code`='1' AND `payment_type`='license' AND `match_restaurantid` {$s_in_clause}";
		
		// query the database, returning $b_default for every account if the query fails
		$response_query = mlavu_query($s_query_str);
		if ($response_query === FALSE || mysqli_num_rows($response_query) === 0) {
			$a_retval = array();
			foreach($a_ids as $id)
				$a_retval[(string)$id] = $b_default;
			unset($a_ids);
			mysqli_free_result($response_query);
			return $a_retval;
		}
		
		// check the results
		foreach($a_ids as $id)
			$a_retval[(string)$id] = FALSE;
		while ($a_response = mysqli_fetch_assoc($response_query)) {
			$a_retval[$a_response['match_restaurantid']] = TRUE;
		}
		return $a_retval;
	}
function build_accounts_array_backup($distro_id, $distro_code, $a_filters){
	//global $o_package_container;
	$accounts = array();
	$loc_contacts = array();
	$a_query_params = array();
	$a_query_params['db_restaurants'] = '`poslavu_MAIN_db`.`restaurants`';
	$a_query_params['db_rest_locations'] = '`poslavu_MAIN_db`.`restaurant_locations`';
	$a_query_params['db_payment_status'] = '`poslavu_MAIN_db`.`payment_status`';
	$a_query_params['distro_code'] = $distro_code;
	$a_query_params['distro_id'] = $distro_id;
	$a_query_params['loc_fields'] = '`address`,`city`,`state`,`zip`,`country`,`manager`';
	/*
	$dbr = '`poslavu_MAIN_db`.`restaurants`';
	$d_code = '`distro_code`';
	$dname_loc = '`dataname`';
	$dbr_loc = '`poslavu_MAIN_db`.`restaurant_locations`';
	$loc_fields = '`address`,`city`,`state`,`zip`,`country`,`manager`';
	$db_paystat = '`poslavu_MAIN_db`.`payment_status`';
	$lead_status = '`lead_status`';
	$trial_end = '`trial_end`';
	*/
	//$a_filters for time..
	if($range == ''){
		$restaurants_query_string = "select * from [restaurantsdb] where [distrocode]='[username]' order by `created` desc limit 100";
	}else{
		$restaurants_query_string = "select * from [restaurantsdb] where [distrocode]='[username]' order by `created` desc limit ".ConnectionHub::getConn('poslavu')->escapeString($range);
	}
	$restaurants_query_vars = array("restaurantsdb"=>$dbr,"distrocode"=>$d_code,"username"=>$user_name);
	//	$_SESSION['update_accounts'] = '';
	$restaurants_query = mlavu_query($restaurants_query_string, $restaurants_query_vars) or die(mlavu_dberror("disto_user->set_leads_accounts_restaurants_query error"));
	while($entry = mysqli_fetch_assoc($restaurants_query)){
		//display_diff_mem_usage();
		//error_log("cost of restaurant");
		$dname = $entry['data_name'];
		$a_query_params['data_name'] = $dname;
		$d_id = $entry['id'];
		$a_query_params['restaurant_id'] = $d_id;
		//
		//
		//query to get info from restaurant_locations table
		$loc_query = mlavu_query("select [loc_fields] from [db_rest_locations] where `dataname`='[data_name]'",$a_query_params);// or die(mlavu_dberror("distro/beta/exec/searchAccountTemp->#402 error"));
		$d_loc_fields = mysqli_fetch_assoc($loc_query);
		mysqli_free_result($loc_query);
		//
		//
		//query to get information from payment_status table
		$lead_stat_query = mlavu_query("select * from [db_payment_status] where `dataname`='[data_name]'",$a_query_params) or
		die(mlavu_dberror("distro/beta/exec/searchAccountTemp->#409"));
		$d_lead_stat = mysqli_fetch_assoc($lead_stat_query);
		mysqli_free_result($lead_stat_query);
		//
		//
		//query to get information from restaurant_notes table
		$db_notes = '`poslavu_MAIN_db`.`restaurant_notes`';
		$n_r_id = '`restaurant_id`';
		$n_s_ninja = '`support_ninja`';
		$a_query_params['db_rest_notes'] = $db_notes;
		$a_query_params['support_ninja'] = $n_s_ninja;
		//echo 'query::<br />'."select * from $db_notes where $n_r_id='$d_id' and $n_s_ninja='DISTRO_NOTE'<br /><br />";
		$res_notes_query = mlavu_query("select * from [db_rest_notes] where `restaurant_id`='[restaurant_id]' and [support_ninja]='DISTRO_NOTE'",$a_query_params) or die(mlavu_dberror("distro/bet/exec/searchAccountTemp->#422 error"));
		while($d_notes_stat = mysqli_fetch_assoc($res_notes_query)){// load all notes into loc_notes
			//echo '<br /><br /><br /><br /><br />'.var_dump($d_notes_stat);
			//foreach($d_notes_stat as $nkey=>$nvalue){
			$mdate = $d_notes_stat['created_on'];
			$mmessage = $d_notes_stat['message'];
			$this->loc_notes[$dname][$mdate] = $mmessage;
				//echo 'NOTE INFO::::<br />dname= '.$dname.'<br /> key= '.$mdate.' <br />value= '.$mmessage.'<br /><br />';
			//}
			unset($mdate);
			unset($mmessage);
			unset($d_notes_stat);
		}//end while loop
		mysqli_free_result($res_notes_query);
		//
		//
		//query to get information from signups table
		$db_signups = '`poslavu_MAIN_db`.`signups`';
		$s_r_id = '`restaurantid`';
		$a_query_params['db_signups'] = $db_signups;
		//echo 'query::<br />'."select * from $db_signups where $s_r_id='$d_id'<br /><br />";
		//select * from `poslavu_MAIN_db`.`signups` where `restaurantid`='[1]' or `dataname`='[2]'
		$signups_query = mlavu_query("select * from [db_signups] where `restaurantid`='[restaurant_id]' or `dataname`='[data_name]'",$a_query_params) or die(mlavu_dberror("distro/beta/exec->#445 error"));
		///display_diff_mem_usage();
		///error_log("bbbbbbbb");
		$p_level = '';
		$signup_timestamp = '';
		if($signups_stat = mysqli_fetch_assoc($signups_query)){//
			//echo '$signups_stat[dataname] = '.$signups_stat['dataname'].'<br />';//'<br /><br /><br /><br /><br />'.var_dump($signups_stat);
			$first = $signups_stat['firstname'];
			$last = $signups_stat['lastname'];
			$signup_id = $signups_stat['id'];
			/*	$p_level = $signups_stat['package'];
				$signup_timestamp = $signups_stat['signup_timestamp'];
				$d_name = $signups_stat['dataname'];
				$rest_company = $signups_stat['company'];
				$rest_email = $signups_stat['email'];
				//echo 'pl::'.$package_level.'<br />';
				$leads_db = '`poslavu_MAIN_db`.`reseller_leads`';
			if($p_level != '' && $p_level != 'none'){
				//mlavu_query("update [1] set `license_applied`='[2]' where `company_name`='[3]' and `email`='[4]' limit 1", $leads_db, $signup_timestamp, $rest_company, $rest_email);
				if($rest_company == 'Shiki'){
				//error_log("not a bug ben:::: ".$rest_company.', '.$rest_email.', '.$p_level);
				}
			}*/
			foreach($signups_stat as $ckey=>$cvalue){
				$this->loc_contacts[$dname][$signup_id][$ckey] = $cvalue;
			}
			$other_contacts_field = $signups_stat['other_contacts'];
			//echo strlen($other_contacts_field).'<br />';
			if(strpos($other_contacts_field, ':*:')){
				//echo "oc";
				$other_contacts = explode('|',$other_contacts_field);
				//echo count($other_contacts);
				$other_count = 1;
				foreach($other_contacts as $contact){
					//echo $c_info[0].", ".$c_info[1].", ".$c_info[2].", ".$c_info[3].", ".$c_info[4]."<br />";
					$o_id = $signup_id.'_'.$other_count;
					$c_info = explode(':*:', $contact);
					if($c_info[0] != ''){
						$this->loc_contacts[$dname][$o_id]['firstname'] = $c_info[0];
						$this->loc_contacts[$dname][$o_id]['lastname'] = $c_info[1];
						$this->loc_contacts[$dname][$o_id]['phone'] = $c_info[2];
						$this->loc_contacts[$dname][$o_id]['email'] = $c_info[3];
						$this->loc_contacts[$dname][$o_id]['contact_notes'] = $c_info[4];
						$other_count++;
					}
				}
			}
			//echo 'CONTACT INFO::::<br />dname= '.$dname.'<br /> key= '.$ckey.' <br />value= '.$cvalue.'<br /><br />';
			//}
		}//end while loop
		//fill in the accounts information
		$this->accounts[$dname] = $entry;
		//information from restaurant_location table
		if($d_loc_fields != null)
			$this->accounts[$dname] = array_merge($d_loc_fields, $this->accounts[$dname]);
		//information from payment_status table
		if($d_lead_stat != null)
			$this->accounts[$dname] = array_merge($d_lead_stat, $this->accounts[$dname]);
		if($p_level != ''){
			// set the package level to what was found (aka $a_package_data['package'])
			$this->accounts[$dname]['package'] = $p_level;
		}else{
			// set the package level because the package level wasn't previously found
			$a_package_data = find_package_status($entry['id']);
			$this->accounts[$dname]['package'] = $a_package_data['package'];
			if ($signup_id != '' && is_numeric($signup_id)) {
				mlavu_query("UPDATE [signupsdb] SET `package`='[package]' WHERE `id`='[signupid]'",
					array("signupsdb"=>$db_signups,"package"=>$a_package_data['package'],"signupid"=>$signup_id));
			}
		}
	}//each restaurant while loop
}//build_account_array_backup
function filter_stuff_query(){
	require_once("/home/poslavu/public_html/admin/manage/misc/AdvancedToolsFunctions/companyReporting.php");
	$reporting_object = new statsReporting();
}
