<?php
/*
	Used to log somebody into a clients back-end
*/

ini_set("display_errors",1);
session_start();
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
require_once(dirname(__FILE__).'/../distro_data_functions.php');
require_once('/home/poslavu/public_html/admin/sa_cp/billing/payment_profile_functions.php');
require_once("/home/poslavu/public_html/admin/cp/resources/session_functions.php");

$loggedin = (isset($_SESSION['posdistro_loggedin']))?$_SESSION['posdistro_loggedin']:false;
if($loggedin=="") $loggedin = false;
$loggedin_fullname = (isset($_SESSION['posdistro_fullname']))?$_SESSION['posdistro_fullname']:false;
if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
$loggedin_email = (isset($_SESSION['posdistro_email']))?$_SESSION['posdistro_email']:false;
if($loggedin_email=="") $loggedin_email = $loggedin;
$loggedin_access = (isset($_SESSION['posdistro_access']))?$_SESSION['posdistro_access']:false;
if($loggedin_access=="") $loggedin_access = "";
$loggedin_id = (isset($_SESSION['posdistro_id']))?$_SESSION['posdistro_id']:'';
$posdistro_percentage = (isset($_SESSION['posdistro_percentage']))?$_SESSION['posdistro_percentage']:'';
$posdistro_loggedin_admin = (isset($_SESSION['posdistro_loggedin_admin']))?$_SESSION['posdistro_loggedin_admin']:'';
$posdistro_password = (isset($_SESSION['posdistro_password']))?$_SESSION['posdistro_password']:'';



$loginto = (isset($_GET['loginto']))?$_GET['loginto']:"";

$loggedin = check_admin_logged_in($loggedin, $loginto);

echo "loginto=".$loginto.", loggedin=".$loggedin;

$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `distro_code`='[2]'",$loginto,$loggedin);
if(mysqli_num_rows($cust_query))
{

		$cust_read = mysqli_fetch_assoc($cust_query);

		$s_distrocode_login = check_admin_logged_in(distro_admin_info("loggedin"), $loginto);
		$dataname = $cust_read['company_code'];
		$userid = 1001;
		$username = "poslavu_distro_".$s_distrocode_login;
		$full_username = "POSLavu Distro " .$s_distrocode_login;
		$email = $_GET['email'];
		$companyid = $cust_read['id'];

		// LP-104 -- Empty bloated session and restore only barebones values for Distro Portal to make room for CP session
		// that could potentially have language pack (which could hit memcached 1 MB limit and blank the session entirely)
		$_SESSION = array();
		if ($loggedin) $_SESSION['posdistro_loggedin'] = $loggedin;
		if ($loggedin_fullname) $_SESSION['posdistro_fullname'] = $loggedin_fullname;
		if ($loggedin_email) $_SESSION['posdistro_email'] = $loggedin_email;
		if ($loggedin_access) $_SESSION['posdistro_access'] = $loggedin_access;
		if ($loggedin_id) $_SESSION['posdistro_id'] = $loggedin_id;
		if ($posdistro_percentage) $_SESSION['posdistro_percentage'] = $posdistro_percentage;
		if ($posdistro_loggedin_admin) $_SESSION['posdistro_loggedin_admin'] = $posdistro_loggedin_admin;
		if ($posdistro_password) $_SESSION['posdistro_password'] = $posdistro_password;

		admin_login($dataname, $userid, $username, $full_username, $email, $companyid, "", "", "4", "", "", "0", "0");

		$cpURL = "/cp/index.php?mode=home_home&redirect";

		echo "<script language='javascript'>";
		
		$data_string = json_encode([
			'username' => $loggedin,
			'password' => $posdistro_password,
			'dataname' => $loginto,
		]);

		$ch = curl_init($_SERVER['HUBQL_URL']."/loginReseller");
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string),
		]);
		$result = curl_exec($ch);
		curl_close($ch);
		$data = json_decode($result);

		if ($data->status === 200) {
			$cpDomain = getStandardDomain();
	
			echo '
				var expires = new Date(new Date().getTime() + '.$_SERVER['CP3_SESSION_EXPIRE_DAYS'].' * 24 * 60 * 60 * 1000);
				var cookie = "LAVU_TOKEN='.$data->token.';expires=" + expires.toGMTString() + ";path=/;domain='.$cpDomain.'";
				document.cookie = cookie;
			';

			if (boolval($cust_read['is_new_control_panel_active'])) {
				$cpURL = $_SERVER['CP3_URL']."/home";
			}

			echo "window.location.replace('".$cpURL."'); ";
		} else if ($data->status === 401) {
			echo "alert('".$data->message."'); window.close();";
		}

		echo "</script>";
}
			
function distro_admin_info($var){
	$sessvar_name = 'posdistro_'.$var;
	if(isset($_SESSION[$sessvar_name]))
		return $_SESSION[$sessvar_name];
	else
		return "";
}//distro_admin_info	
			
function add_to_distro_history($action,$details,$reseller_id,$reseller_name){
	$vars = array();
	$vars['action'] = $action;
	$vars['details'] = $details;
	$vars['reseller_id'] = $reseller_id;
	$vars['reseller_name'] = $reseller_name;
	$vars['datetime'] = date("Y-m-d H:i:s");

		mlavu_query("insert into `poslavu_MAIN_db`.`reseller_history` (`action`,`details`,`reseller_id`,`reseller_name`,`datetime`) values ('[action]','[details]','[reseller_id]','[reseller_name]','[datetime]')",$vars);
}//add_to_distro_history

function check_admin_logged_in($s_distrocode, $s_dataname) {
	if (!distro_is_admin($s_distrocode))
		return $s_distrocode;
	
	// must be an admin
	$a_restaurant = get_restaurant_from_dataname($s_dataname);
	if (count($a_restaurant) == 0)
		return '';
	return $a_restaurant['distro_code'];
}//check_admin_logged_in
