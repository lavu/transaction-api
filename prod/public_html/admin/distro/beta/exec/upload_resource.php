<?php

require_once(dirname(__FILE__)."/../../../cp/resources/lavuquery.php");
require_once(dirname(__FILE__)."/../../../cp/objects/json_decode.php");
require_once(dirname(__FILE__)."/../../../cp/resources/json.php");

//echo 'booger';
//print_r($_POST);
//error_log("upload resource php:: ");

if ( isset($_POST['resource']) ) {
	//error_log('distro/beta/exec/upload_resource.php#15');
	$filestuff = array();
	$raw_resource_string = utf8_decode($_POST['resource']);

	$base64_break = 'data:'.$_POST['filetype'].';base64,';
	$booger64data = str_replace($base64_break, '', $raw_resource_string);
	//echo 'break:: '.$base64_break.'>>>>'.$booger64data;
	//echo formatSizeUnits($_POST['filesize']);
	//$filestuff
	//echo 'before decoding';
	$decoded_resource = base64_decode($booger64data);
	$filename_nospace = $_POST['filename'];
	$_POST['filename'] = $filename_nospace;
	if(strpos($_POST['filetype'], 'image')===false){
		//echo 'not an --image--'.$_POST['filetype'];

		no_thumbnail_upload($decoded_resource);
		
	}else{
		//echo '--image--';
		no_thumbnail_upload($decoded_resource);
	}
	//echo 'decoded:: '.$decoded_resource;
	//echo '0=::__'.$bread_string_arr[0];//.'__||||1=::__'.$bread_string_arr[1];
	//$file_extension = getMimeType();
}//upload that bitch;
else if(isset($_POST['resource_action'])){
	//error_log('distro/beta/exec/upload_resource.php#40 resource_action='.$_POST['resource_action']);
	$_POST['resource_action']();
}

//fetches and returns the array for of the resource json
function get_existing_resource_json_tree(){
	$json_resources = file_get_contents("../Resources_new/resources_attr.json");
	$json_a = LavuJson::json_decode($json_resources);
	//create_display($json_a);
	return $json_a;
}//get_existing_resource_json_tree()

function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }
    return $bytes;
}//formatSizeUnits()
function no_thumbnail_upload($file){
	//get json. 
	//error_log("=======upload_resource=======file length:: ".count($file));
	$a_resources = get_existing_resource_json_tree();
	//$a_resources[section][filename][attribute]=value
	$filesize = formatSizeUnits($_POST['filesize']);
	$filename_split = explode('.',$_POST['filename']);
	$type_index = count($filename_split) - 1;
	$filetype = $filename_split[$type_index];
	$filename_nospace = str_replace(' ', '_', $_POST['filename']);
	$jpg_name = str_replace(' ', '_', $filename_split[0]);
	$file_subcategory = str_replace('_', ' ', $_POST['sub_section']);
	$date_created = empty($_POST['date_created']) ? date('Y-m-d H:i:s') : $_POST['date_created'];
	$date_modified = date("Y-m-d H:i:s");

	if(strpos($_POST['filetype'], 'image')===false){
		$thumbnail = "./Resources_new/thumbnails/".$jpg_name.'.jpg';
	}else{
		$thumbnail = "./Resources_new/".$_POST['section']."/".$filename_nospace;
	}

	$a_resource_to_add = array(
		$filename_nospace => array(
			"section"=>$_POST['section'],
			"subcategory"=>$file_subcategory,
			"title"=>$_POST['title'],
			"display"=>$_POST['title'],
			"sub_title"=>$_POST['sub_title'],
			"description"=>$_POST['description'],
			"filename"=>$filename_nospace,
			"filesize"=>$filesize,
			"type"=>$filetype,
			"size"=>$filesize,
			"homescreen"=>$_POST['homescreen'],
			"thumbnail"=>$thumbnail,
			"featured"=>$_POST['featured'],
			"date_created"=>$date_created,
			"date_modified"=>$date_modified
		)
	); //$a_resource_to_add[]

	//print_r($a_resources);
	$a_resources[$_POST['section']] = array_merge($a_resources[$_POST['section']], $a_resource_to_add);
	$a_resources['latest'] = array_merge($a_resources['latest'], $a_resource_to_add);
	//print_r($a_resources);
	$json_new_resources = LavuJson::json_encode($a_resources);

	write_resource_json($json_new_resources);
	//error_log('sales=>lavu_pos_data_kit.pdf=>filename='.$a_resources['sales']['lavu_pos_data_kit.pdf']['filename']);
	//error_log('sales=>'.$_POST['filename'].'=>filename='.$a_resource_to_add['sales']['lavu_pos_data_kit.pdf']['filename']); 
	$filename = "../Resources_new/".$_POST['section'].'/'.$_POST['filename'];
	//echo "file location:: ".$filename;
	//$section = $_POST['section'];
	//write file
	//file_exists($filename)
	if (file_exists($filename)){//remove old file, replace with new one
		//echo $username . "success||| Image uploaded||||../../images/logos/fullsize/$filename";
		
		//$remove_oldcmd = "rm -f ".$filename;
		//exec($remove_oldcmd);
		unlink($filename);
		
		//echo .$filename;
		//echo "file exits write response:: ".
		$result = file_put_contents($filename,$file);
		if($result>0){
			echo "SUCCESS|||The resource was uploaded successfully
			";//'TRUE:: distro/exec/upload_resource.php ::file exits file put response::'.$result;			
		}else{
			echo "FAIL|||The resource was not uploaded
			";//'FALSE:: distro/exec/upload_resource.php ::file exits file put response::'.$result;
		}
	}else{//add file
		$result = file_put_contents($filename,$file);
		error_log('---file does not exist ::'.$result);
	    if($result>0){
	    	//error_log('mmmmFILE SUCCESS!!');
	    	$s_return = "SUCCESS|||";
			//echo "SUCCESS|||The resource was uploaded successfully";//'TRUE:: distro/exec/upload_resource.php ::file exits file put response::'.$result;			
		}else{
			$s_return = "FAIL|||";
			//echo "FAIL|||The resource was not uploaded";//'FALSE:: distro/exec/upload_resource.php ::file exits file put response::'.$result;
		}
    }//add new file
    //error_log("filetype:: ".$_POST['filetype']);
    if(strpos($_POST['filetype'], 'image')===false){
    	if(strpos($_POST['filetype'], 'pdf')===false){
    		$filepath = "/home/poslavu/public_html/admin/distro/beta/Resources_new/".$_POST['section'].'/'.$_POST['filename'];
    	}else{
    		error_log("make thumbnail from pdf");
    		$filepath = "/home/poslavu/public_html/admin/distro/beta/Resources_new/".$_POST['section'].'/'.$_POST['filename'].'[0]';
    	}
    	$thumbnail_filepath = "/home/poslavu/public_html/admin/distro/beta/Resources_new/thumbnails/".$filename_split[0].'.jpg';
    	$thumbnail_link = "./Resources_new/thumbnails/".$filename_split[0].'.jpg';
    	$exec_string = "convert -density 100 -colorspace rgb ".lavuShellEscapeArg($filepath)." -scale 200x200 ".lavuShellEscapeArg($thumbnail_filepath);
    	//error_log("---exec string:: ".$exec_string);
    	lavuExec($exec_string);
    	$s_return = "SUCCESS|||"."Resource uploaded successfully!|||".$thumbnail_link;
    	/*
	   //exec('convert -density 100 -colorspace rgb /home/poslavu/public_html/admin/distro/beta/Resources/Documents/Inventory_Set_Up_Exercise.pdf[0] -scale 200x200 /home/poslavu/public_html/admin/distro/beta/Resources_new/thumbnails/test.jpg');
	   $result = '/home/poslavu/public_html/admin/distro/beta/Resources_new/thumbnails/test.jpg';
	   //exec("convert logo: /home/poslavu/public_html/admin/distro/beta/Resources_new/thumbnails/$im", $out, $rcode);
	   echo "Logo result::<br /><img src='$result' alt='testie' /><br /> code is code <br>";
	   */
    }//if filetype is NOT and image
    else{
    	$s_return = "SUCCESS|||"."Image Resource uploaded successfully!|||./Resources_new/".$_POST['section'].'/'.$_POST['filename'];
    	
    }
    //error_log('return value:: '.$s_return);
    echo $s_return;
}//no_thumbnail_upload()
function yes_thumbnail_upload($file){

	/*
	  //exec('convert -density 100 -colorspace rgb /home/poslavu/public_html/admin/distro/beta/Resources/Documents/Inventory_Set_Up_Exercise.pdf[0] -scale 200x200 /home/poslavu/public_html/admin/distro/beta/Resources_new/thumbnails/test.jpg');
	   $result = '/home/poslavu/public_html/admin/distro/beta/Resources_new/thumbnails/test.jpg';
	   //exec("convert logo: /home/poslavu/public_html/admin/distro/beta/Resources_new/thumbnails/$im", $out, $rcode);
	   echo "Logo result::<br /><img src='$result' alt='testie' /><br /> code is code <br>";
	*/
}//yes_thumbnail_upload()
function write_resource_json($resource_json){
	$fp = fopen("../Resources_new/resources_attr.json", 'w');
	$response = '';
	$response .= fwrite($fp, $resource_json);
	fclose($fp);

	//$a_resources = get_existing_resource_json_tree();
	//print_r($a_resources);
	/*
	   $json_resources = file_get_contents("../Resources_new/resources_attr.json");
	   $json_a = LavuJson::json_decode($json_resources);
	*/
	return $response;
}//write_resource_json()
function backup_resource_json(){
	$fp = fopen("../Resources_new/resources_attr.json", 'w');
	fwrite($fp, $resource_json);
	fclose($fp);

	$a_resources = get_existing_resource_json_tree();
	//print_r($a_resources);
	/*
	   $json_resources = file_get_contents("../Resources_new/resources_attr.json");
	   $json_a = LavuJson::json_decode($json_resources);
	*/
}//backup_resource_json
function edit_resource(){
	//echo 'edit resource function==>';
	$a_resources = get_existing_resource_json_tree();
	$a_sales_file = $a_resources['sales'][$_POST['filename']];
	$a_marketing_file = $a_resources['marketing'][$_POST['filename']];
	$a_support_file = $a_resources['support'][$_POST['filename']];
	//$a_featured_file = $a_resources['featured'][$_POST['filename']];
	$_featured = '';
	$_homescreen = '';
	if($_POST['featured']=='true'){
		//error_log('----=-----=----=-----FEATURED CLICKED'.$_POST['featured']);
		$_featured='featured';
	}
	if($_POST['remove']=='true'){
		error_log('----=-----=----=-----REMOVED CHECKED'.$_POST['remove']);
		$_homescreen='removed';
	}
	$current_section = str_replace('_',' ',$_POST['section']);
	$sub_section = str_replace('_',' ',$_POST['subsection']);
	//echo 'current section boobie '.$current_section;//print_r($_POST);
	$date_created = empty($_POST['date_created']) ? date('Y-m-d H:i:s') : $_POST['date_created'];
	$date_modified = date("Y-m-d H:i:s");

	if(count($a_sales_file) > 0){
		//error_log('distro/beta/exec/upload_resource.php#219 edit sales json');
		if($current_section == 'sales'){
			$a_sales_file['subcategory'] = $sub_section;
			//error_log("distro/beta/exec/upload_resource.php#224 post SALES sub CHANGE=".$sub_section.", file sub=".$a_sales_file['subcategory']);

		}
		$a_sales_resource_to_add = array($_POST['filename']=>array(
			"section"=>'sales',
			"subcategory"=>$a_sales_file['subcategory'],
			"title"=>$_POST['title'],
			"display"=>$_POST['title'],
			"sub_title"=>$_POST['subtitle'],
			"description"=>$_POST['description'],
			"filename"=>$_POST['filename'],
			"filesize"=>$a_sales_file['filesize'],
			"type"=>$a_sales_file['type'],
			"size"=>$a_sales_file['filesize'],
			"homescreen"=>$_homescreen,
			"thumbnail"=>$a_sales_file['thumbnail'],
			"featured"=>$_featured,
			"date_created"=>$date_created,
			"date_modified"=>$date_modified)
	   );//$a_resource_to_add[]
		//echo 's before'.print_r($a_resources['sales']);
		$a_resources['sales'] = array_merge($a_resources['sales'], $a_sales_resource_to_add);
		//echo 's after'.print_r($a_resources['sales']);
		//print_r($a_resources['sales']);
	}//sales array
	if(count($a_marketing_file) > 0){
		//error_log('distro/beta/exec/upload_resource.php#222 edit marketing json');
		if($current_section == 'marketing'){
			$a_marketing_file['subcategory'] = $sub_section;
			//error_log("distro/beta/exec/upload_resource.php#248 post MARKETING sub CHANGE=".$sub_section.", file sub=".$a_marketing_file['subcategory']);
		}
		$a_marketing_resource_to_add = array($_POST['filename']=>array(
			"section"=>'marketing',
			"subcategory"=>$a_marketing_file['subcategory'],
			"title"=>$_POST['title'],
			"display"=>$_POST['title'],
			"sub_title"=>$_POST['subtitle'],
			"description"=>$_POST['description'],
			"filename"=>$_POST['filename'],
			"filesize"=>$a_marketing_file['filesize'],
			"type"=>$a_marketing_file['type'],
			"size"=>$a_marketing_file['filesize'],
			"homescreen"=>$_homescreen,
			"thumbnail"=>$a_marketing_file['thumbnail'],
			"featured"=>$_featured,
			"date_created"=>$date_created,
			"date_modified"=>$date_modified)
	   );//$a_resource_to_add[]
		$a_resources['marketing'] = array_merge($a_resources['marketing'], $a_marketing_resource_to_add);
		//echo 'm after'.print_r($a_resources['marketing']);
	}//marketing array
	if(count($a_featured_file) > 0){
		//error_log('distro/beta/exec/upload_resource.php#225 edit featured json');
		if($current_section == 'featured'){
			error_log("distro/beta/exec/upload_resource.php#269 post FEATURED sub CHANGE=".$sub_section.", file sub=".$a_featured_file['subcategory']);
		}

		$a_featured_resource_to_add = array($_POST['filename']=>array(
			"section"=>'featured',
			"subcategory"=>$a_featured_file['subcategory'],
			"title"=>$_POST['title'],
			"display"=>$_POST['title'],
			"sub_title"=>$_POST['subtitle'],
			"description"=>$_POST['description'],
			"filename"=>$_POST['filename'],
			"filesize"=>$a_featured_file['filesize'],
			"type"=>$a_featured_file['type'],
			"size"=>$a_featured_file['filesize'],
			"homescreen"=>$_homescreen,
			"thumbnail"=>$a_featured_file['thumbnail'],
			"featured"=>$_featured,
			"date_created"=>$date_created,
			"date_modified"=>$date_modified)
	   );//$a_resource_to_add[]
		//echo 'f '.print_r($a_featured_resource_to_add);
	}//featured array

	if(count($a_support_file) > 0){
		error_log('DEBUG: in a_support_file block...');
		if($current_section == 'support'){
			$a_sales_file['subcategory'] = $sub_section;
			//error_log("distro/beta/exec/upload_resource.php#333 post SUPPORT sub CHANGE=".$sub_section.", file sub=".$a_sales_file['subcategory']);
		}
		$a_support_resource_to_add = array($_POST['filename']=>array(
			"section"=>'featured',
			"subcategory"=>$a_support_file['subcategory'],
			"title"=>$_POST['title'],
			"display"=>$_POST['title'],
			"sub_title"=>$_POST['subtitle'],
			"description"=>$_POST['description'],
			"filename"=>$_POST['filename'],
			"filesize"=>$a_support_file['filesize'],
			"type"=>$a_support_file['type'],
			"size"=>$a_support_file['filesize'],
			"homescreen"=>$_homescreen,
			"thumbnail"=>$a_support_file['thumbnail'],
			"featured"=>$_featured,
			"date_created"=>$date_created,
			"date_modified"=>$date_modified)
		); //$a_resource_to_add[]

		$a_resources['support'] = array_merge($a_resources['support'], $a_support_resource_to_add);

		//echo 'f '.print_r($a_support_resource_to_add);
	} //support array


	//print_r($a_resources);
	//$a_resources[$_POST['section']] = array_merge($a_resources[$_POST['section']], $a_resource_to_add);
	//echo print_r($a_resources);
	$json_new_resources = LavuJson::json_encode($a_resources);
	$response = write_resource_json($json_new_resources);
	if($response > 0){
		echo 'SUCCESS||:||'.$sub_section;
	}else{
		echo 'FAIL||:||'.$sub_section;
	}
	//echo 'write response =='.write_resource_json($json_new_resources);
	//echo echo 's after'.print_r($a_resources['sales']);
}//edit_resource

?>