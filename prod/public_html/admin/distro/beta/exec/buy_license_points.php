<?php
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
require_once("/home/poslavu/public_html/admin/sa_cp/billing/payment_profile_functions.php");
require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");
require_once(dirname(__file__)."/distro_user_functions.php");
require_once(dirname(__file__)."/../distro_data_functions.php");

if (!isset($o_package_container))
	$o_package_container = new package_container();

//var_dump($_POST);
$time_stamp = date("Y-m-d H:i:s");

// get the highest level names
$a_base_highest_names = $o_package_container->get_highest_base_package_names();
$a_base_highest_names[] = 'Platinum2';
$a_base_highest_names[] = 'Lavu';
$a_base_highest_names[] = 'LavuEnterprise';

$prices = array();
$prices['Lavu88 to Gold3'] = 1495.00;
$prices['Gold3 to Platinum3'] = 1000.00;
$prices['Lavu88 to Platinum3'] = 2495.00;
foreach($a_base_highest_names as $s_highest_name) {
	$prices[$s_highest_name] = (float)$o_package_container->get_value_by_attribute('name', $s_highest_name);
}

$points = array();
foreach($prices as $s_name=>$i_value) {
	$points[$s_name] = (int)$i_value;
}

$fields = array();
$fields['r_l_table']= '`poslavu_MAIN_db`.`reseller_licenses`';
$fields['r_table']= '`poslavu_MAIN_db`.`resellers`';
$fields['resellerid']= $_POST['reseller_id'];
$fields['resellername']= $_POST['reseller_name'];
$fields['cost']= '0';
$fields['value']= $prices[$_POST['pts_license_selected']];
$fields['notes'] = 'PURCHASED WITH POINTS, distro commission ';
$fields['type'] = $_POST['pts_license_selected'];
$fields['purchased'] = $time_stamp;
$fields['credits'] = strval((int)$_POST['reseller_points'] - (int)$prices[$_POST['pts_license_selected']]);

$a_distro = get_distro_from_array_distrocode_dataname(array(), $_POST['reseller_name']);
if (trim($_POST['pts_license_selected']) == '') {
	echo "fail|First select a license";
} else if (!isset($prices[$_POST['pts_license_selected']])) {
	echo "fail|License is not valid";
} else if ($a_distro['credits'] == $_POST['reseller_points'] && (int)$fields['credits'] >= 0) {
	$i_commission = get_distro_commision($a_distro);
	$fields['notes'] .= $i_commission;
	add_to_distro_licenses_table($fields);
	edit_distributor_credits($fields);
	echo "success|Successfully purchased license";
} else {
	echo "fail|Not enough credits";
}

function add_to_distro_licenses_table($fields){
	$query_string = "insert into [r_l_table] (`type`, `resellerid`, `resellername`, `cost`, `value`, `purchased`, `notes`) values('[type]', '[resellerid]', '[resellername]', '[cost]', '[value]', '[purchased]', '[notes]')";

	mlavu_query($query_string, $fields);
}

function edit_distributor_credits($fields){
	$query_string = "update [r_table] set `credits`='[credits]' where `username`='[resellername]' limit 1";
	mlavu_query($query_string, $fields);
}


//$action = 'buy license using points';
//$details = '';
//$reseller_id = '';
//$reseller_name = '';
//add_to_distro_history($action,$details,$reseller_id,$reseller_name);