<?php

global $o_emailAddressesMap;
require_once(dirname(__FILE__)."/../../../cp/resources/core_functions.php");
require_once(dirname(__FILE__)."/../../../cp/resources/lavuquery.php");
require_once(dirname(__FILE__)."/../../../sa_cp/billing/payment_profile_functions.php");
require_once(dirname(__FILE__)."/../../../sa_cp/billing/package_levels_object.php");
require_once(dirname(__FILE__)."/../../../sa_cp/billing/procareas/SalesforceIntegration.php");
require_once(dirname(__FILE__)."/../../../manage/globals/email_addresses.php");
require_once(dirname(__FILE__)."/../reseller_licenses_functions.php");
require_once(dirname(__FILE__)."/../account_functions.php");
require_once(dirname(__FILE__)."/distro_user_functions.php");

if (!isset($o_package_container))
	$o_package_container = new package_container();
if (!isset($maindb) || $maindb == '') $maindb = 'poslavu_MAIN_db';

$applyto = (isset($_POST['apply_license_dname']))?$_POST['apply_license_dname']:"";
$use_license_level = (isset($_POST['apply_lic_available_select']))?$_POST['apply_lic_available_select']:"";
$is_id = (isset($_POST['is_id']) && $_POST['is_id'] == '1') ? TRUE : FALSE;
$distro_id = (isset($_POST['distro_id']))?$_POST['distro_id']:"";

$message = apply_license_step1($applyto, $distro_id, $use_license_level, $is_id);
error_log($message);

// format for message: [success|fail]\|"report message"
echo $message;

// returns either '' or the distro_code
function get_distro_code_from_id($s_distro_id) {
	$a_query_vars = array('id'=>$s_distro_id);
	$a_distro_usernames = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('resellers', $a_query_vars, TRUE, array('selectclause'=>'`username`', 'collapse_arrays'=>TRUE));
	if (count($a_distro_usernames) == 0)
		return "";
	return $a_distro_usernames[0];
}

// returns either success|message or fail|message
function apply_license_step2($s_applyto_data_name, &$distro_read, $s_license_id, $b_applying_license_to_signup = FALSE, $cmsn = '') {
	global $maindb;
	global $o_package_container;
	global $o_emailAddressesMap;

	$current_license_id = "";
	$current_license_resellerid = "";
	$status_id = "";
	$s_current_license_type = "";
	$s_package = ''; // only used for upgrades
	$s_distro_code = $distro_read['username'];

	// get the restaurant and ensure that it exists
	$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_applyto_data_name, 'distro_code'=>$s_distro_code), TRUE);
	if (count($a_restaurants) == 0)
		return 'fail|The restaurant could not be found.';
	$a_restaurant = $a_restaurants[0];

	// get the signup and ensure that it exists
	$a_signups = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('signups', array('dataname'=>$s_applyto_data_name), TRUE);
	if (count($a_signups) == 0)
		return 'fail|The restaurant could not be found.';
	$a_signup = $a_signups[0];

	// get the restaurant payment status
	error_log($a_restaurant['id']." - ".$s_applyto_data_name);
	$a_payment_statuses = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('payment_status', array('restaurantid'=>$a_restaurant['id'], 'dataname'=>$s_applyto_data_name), TRUE);

	// the payment status row exists, get the currently applied license
	if(count($a_payment_statuses) > 0)
	{
		$a_payment_status = $a_payment_statuses[0];
		$current_license_id = $a_payment_status['license_applied'];
		$status_id = $a_payment_status['id'];
		$a_package_data = find_package_status($a_restaurant['id']);
		$s_package = $a_package_data['package'];
		if (is_numeric($s_package)) {
			$s_current_license_type = $o_package_container->get_plain_name_by_attribute('level', (int)$s_package);
		}
		$a_license_applied_type = RESELLER_LICENSES::get_license_from_id($current_license_id,$distro_read);
		$license_applied_type = $a_license_applied_type['type'];
	}

	// the payment status row does not exist, create it
	else
	{
		mlavu_query("insert into `[1]`.`payment_status` (`restaurantid`,`dataname`) values ('[2]','[3]')",$maindb,$a_restaurant['id'],$a_restaurant['data_name']);
		$status_id = mlavu_insert_id();
	}

	// find the license, returning errors related to the license if they are found
	$wt_lic_read = RESELLER_LICENSES::get_license_from_id($s_license_id, $distro_read);
	if (is_string($wt_lic_read)) // if successful, $wt_lic_read will be an array
		return $wt_lic_read;
	$lic_read = $wt_lic_read;
	$lic_read['upgrade_from_level'] = $o_package_container->get_level_by_attribute('name', $lic_read['upgrade_from']);
	if($lic_read['restaurantid'] != "")
		return 'fail|This license has already been applied';

	// applying a new license from the distro portal
	if($current_license_id=="" && // for applying licenses
			// for applying upgrades
			($s_package == '' || 'none' == strtolower($s_package) || $a_signup['status'] == 'new') && $lic_read['upgrade_from'] == '')
		{

		// run the queries to apply the license
		$s_datetime = date("Y-m-d H:i:s");
		mlavu_query("update `[1]`.`reseller_licenses` set `restaurantid`='[2]', `applied`='[3]' where `id`='[4]' ",$maindb,$a_restaurant['id'],$s_datetime,$s_license_id);
		mlavu_query("update `[1]`.`payment_status` set `license_applied`='[2]', `license_type`='[3]', `license_resellerid`='[4]' where `id`='[5]'",$maindb,$s_license_id,$lic_read['type'],$distro_read['id'],$status_id);
		$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `license_applied`='[date]',`_finished`='1',`_canceled`='0' WHERE `data_name`='[dataname]'";
		$a_query_vars = array('date'=>$s_datetime, 'dataname'=>$a_restaurant['data_name']);
		error_log(ConnectionHub::getConn('poslavu')->getDBAPI()->drawQuery($s_query_string, $a_query_vars));
		mlavu_query($s_query_string, $a_query_vars);

		// update the account package level
		$i_new_package = $o_package_container->get_level_by_attribute('name', $lic_read['type']);
		change_package($a_restaurant['data_name'], $a_restaurant['data_name'], $i_new_package, $a_restaurant['id']);

		// cancel the arb for the license payment
		if ($b_applying_license_to_signup) {
			require_once(dirname(__FILE__).'/../../../../register/authnet_functions.php');
			$subscriptionid = $a_signup['arb_license_id'];
			$sub_info = manage_recurring_billing("delete",array("refid"=>"15","subscribeid"=>$subscriptionid));
			if (!$sub_info['success']) {
				return 'fail|Our system failed to cancel the customer\'s license invoice. Please contact Lavu Support. License applied.';
			}
			$o_emailAddressesMap->sendEmailToAddress('billing_errors', 'cancelation success', print_r($sub_info,TRUE));
		}

		// add to the distro history
		add_to_distro_history("applied license","applyto: $s_applyto_data_name\ncompany_name: ".$a_restaurant['company_name']."\nlicense_id: $s_license_id\nlicense_type: ".$lic_read['type'] ."\ncmsn: $cmsn",$distro_read['id'],$distro_read['username']);

		SalesforceIntegration::logSalesforceEvent($s_applyto_data_name, 'Closed Won');

		// apply cmsn
		if (!empty($cmsn)) {
			// Award cmsn to specialist
			mlavu_query("UPDATE `[1]`.`resellers` SET `cmsn`=`cmsn`+'[2]' WHERE `username`='[3]' AND `username` != ''",$maindb, $cmsn, $s_distro_code);
			if (mlavu_affected_rows >= 0) {
				// Mark the reseller license as having paid out its commission
				mlavu_query("UPDATE `[1]`.`reseller_licenses` SET `cmsn_applied`='[2]' WHERE `id`='[3]' ",$maindb, $cmsn, $s_license_id);
				// Log history events
				mlavu_query("INSERT INTO `[maindb]`.`billing_log` (`datetime`,`dataname`,`restaurantid`,`username`,`fullname`,`action`,`details`,`billid`,`target`) VALUES ('[datetime]','[dataname]','[restaurantid]','[username]','[fullname]','[action]','[details]','[billid]','[target]')",
					array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'dataname'=>$s_applyto_data_name, 'restaurantid'=>$a_restaurant['id'], 'username'=>'award_cmsn_for_bills.php', 'fullname'=>'award_cmsn_for_bills.php', 'action'=>'apply distro cmsn', 'details'=>"reseller_license id {$s_license_id}: applied {$cmsn} cmsn to distro {$s_distro_code}", 'billid' => '', 'target' => $cmsn));
				mlavu_query("INSERT INTO `[maindb]`.`reseller_billing_log` (`datetime`,`resellerid`,`resellername`,`dataname`,`action`,`details`,`cmsn_applied`,`invoiceid`) VALUES ('[datetime]','[resellerid]','[resellername]','[dataname]','[action]','[details]','[cmsn_applied]','[invoiceid]')", 
					array("maindb"=>$maindb, 'datetime'=>date("Y-m-d H:i:s"), 'resellerid'=>$distro_read['id'], 'resellername'=>$s_distro_code, 'dataname'=>$s_applyto_data_name, 'action'=>'apply distro cmsn', 'details'=>'applied cmsn to distro '.$s_distro_code, 'cmsn_applied'=>$cmsn, 'invoiceid'=>''));

			}
		}

		// return success
		$s_license_level_print = $o_package_container->get_printed_name_by_attribute('name', $lic_read['type']);
		return 'success|'.$s_license_level_print.' license successfully applied to '.$a_restaurant['company_name'].'.|'.$s_applyto_data_name;

	// applying an upgrade from the distro portal
	} else if ($lic_read['upgrade_from'] != '' &&
		$o_package_container->upgrade_matches_license($lic_read['upgrade_from_level'], $s_package)) {

		// get the values of the upgrade
		$s_current_license_type_print = $o_package_container->get_printed_name_by_attribute('name', $s_current_license_type);
		$s_upgrade_from_print = $o_package_container->get_printed_name_by_attribute('name', $lic_read['upgrade_from']);
		$s_upgrade_to_print = $o_package_container->get_printed_name_by_attribute('name', $lic_read['upgrade_to']);

		// check that the upgrade is a valid upgrade and can be applied
		if ($s_current_license_type_print != $s_upgrade_from_print)
			return 'fail|This upgrade can only be applied to a '.$lic_read['upgrade_from'].' account';
		$i_upgrade_cost = (int)$o_package_container->get_upgrade_cost_by_old_new_names($lic_read['upgrade_from'], $lic_read['upgrade_to']);
		if ($i_upgrade_cost == -1)
			return 'fail|This upgrade doesn\'t exist or can\'t be processed';

		// apply the upgrade and return the success/failure
		$s_success_message = apply_upgrade($s_applyto_data_name, (int)$s_license_id, $s_package);
		if ($s_success_message == "success")
			return 'success|'.$s_upgrade_to_print.' upgrade successfully applied to '.$a_restaurant['company_name'];
		else
			return 'fail|'.ucfirst($s_success_message);

	// applying an upgrade/license failed
	} else {
		if ($lic_read['upgrade_from'] != '') {
			return 'fail|The restaurant must first have a license in order to apply an upgrade';
		}
 		if (!($s_package == '' || 'none' == strtolower($s_package) || $a_signup['status'] == 'new')) {
			return 'fail|The restaurant must not have a package level in order to receive a license. Please contact lavu support.';
		}
		return 'fail|The restaurant '.$a_restaurant['company_name'].' already has a license.';
	}
}

// returns either success|message or fail|message
function apply_license_step1($s_applyto_data_name, $s_distro_id, $s_license_name_or_id, $is_id) {

	global $maindb;
	global $o_package_container;

	$s_distro_code = get_distro_code_from_id($s_distro_id);
	if ($is_id) {
		$query = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`reseller_licenses` WHERE `id`='[id]'", array('id'=>$s_license_name_or_id));
		$reseller_license = ($query !== FALSE && mysqli_num_rows($query) == 1) ? mysqli_fetch_assoc($query) : array('id' => '', 'cmsn' => '', 'cmsn_applied' => '');
		$s_license_id =  $reseller_license['id'];
	} else {
		$s_license_id = RESELLER_LICENSES::get_license_id_from_level($s_distro_code, $s_license_name_or_id);
		$reseller_license = array('id' => $s_license_id, 'cmsn' => '', 'cmsn_applied' => '');
	}

	// find the distributor and ensure that they exist
	if($s_distro_code == "")
		return 'fail|The distributor is not provided. Please contact support.';
	$a_distros = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('resellers', array('username'=>$s_distro_code), TRUE);
	if (count($a_distros) == 0)
		return 'fail|Unable to find the distributor';
	$distro_read = $a_distros[0];
	$s_distro_code = $distro_read['username'];

	// check if the license is not an upgrade (is a license), and if the account is a signup, and if the the account does not yet have a license payment
	$b_applying_license_to_signup = FALSE;
	$wt_lic_read = RESELLER_LICENSES::get_license_from_id($s_license_id, $distro_read);
	if (is_string($wt_lic_read)) // if successful, $wt_lic_read will be an array
		return $wt_lic_read;
	$s_license_type = $o_package_container->get_package_status($wt_lic_read['type']);
	$a_signups = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('signups', array('dataname'=>$s_applyto_data_name), TRUE, array('selectclause'=>'`status`'));
	if ($s_license_type == 'package' && $a_signups['status'] == 'new') {
		$s_pay_status = account_has_paid_license(0, $s_applyto_data_name);
		if ($s_pay_status == 'paid')
			return 'fail|This account has already paid for a license.';
		if ($s_pay_status == 'has_license')
			return 'fail|This account already has a license applied to it.';
		$b_applying_license_to_signup = TRUE;
	}

	// check that the input is good
	if ($s_distro_code == '')
		return 'fail|Unable to find distributor.';

	// the license doesn't exist
	if ($s_license_id == '')
		return 'fail|Unable to find registered '.$s_license_level.' license for this distributor.';

	$free_lavu_license = ( preg_match('/^(Mercury|EvoSnap).+$/i', $wt_lic_read['type'], $regex_matches) );
	if ($free_lavu_license) {
		// Get Free Lavu payment processor names.
		$payment_processor_shortname = isset($regex_matches[1]) ? $regex_matches[1] : '';  // Mercury|EvoSnap
		$payment_processors_map = array('EvoSnap' => 'EVOSnap', 'Mercury' => 'Mercury');
		$payment_processor_for_license = isset($payment_processors_map[$payment_processor_shortname]) ? $payment_processors_map[$payment_processor_shortname] : $payment_processor_shortname;

		// Get current payment processor from account's locations table.
		ConnectionHub::getConn('rest')->selectDN($s_applyto_data_name);
		$locationquery = lavu_query("SELECT * FROM `locations` WHERE `_disabled` = '0' ORDER BY `id` DESC LIMIT 1");
		if ($locationquery === false)
			return "fail|Got database error when trying to get location data for {$s_applyto_data_name}: ". mlavu_dberror();

		$location = mysqli_fetch_assoc($locationquery);

		if ( strtolower($location['gateway']) != strtolower($payment_processor_for_license) )
			return "fail|Cannot apply {$s_license_level} license until payment processor gateway is set to {$payment_processor_for_license}.";
	}

	$cmsn = ($reseller_license['cmsn'] != '' && empty($reseller_license['cmsn_applied'])) ? $reseller_license['cmsn'] : '';

	// try to apply the license
	$apply_license_response = apply_license_step2($s_applyto_data_name, $distro_read, $s_license_id, $b_applying_license_to_signup, $cmsn);

	return $apply_license_response;
}

?>
