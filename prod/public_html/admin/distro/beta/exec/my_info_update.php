<?php

ini_set("display_errors",1);
session_start();
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
require_once('/home/poslavu/public_html/admin/cp/objects/json_decode.php');
require_once(dirname(__FILE__).'/../distro_data_functions.php');

//var_dump($_POST);

if(isset($_POST['submit_personal'])){
	$message = '';
	if($_POST['pPassword'] != ''){
		$message .= doPassUpdate();
	}
	if($_POST['pFirstName'] != $_POST['p_current_first'] || $_POST['pLastName'] != $_POST['p_current_last']){
		$q_vals = array();
		$q_vals['rsl_db'] = '`poslavu_MAIN_db`.`resellers`';
		$q_vals['f_name'] = '`f_name`';
		$q_vals['l_name'] = '`l_name`';
		$q_vals['u_name'] = '`username`';
		$q_vals['f_val'] = $_POST['pFirstName'];
		$q_vals['l_val'] = $_POST['pLastName'];
		$q_vals['u_curr'] = $_POST['p_current_username'];
		//echo var_dump($q_vals);
			//echo "update $q_vals['rsl_db'] set $q_vals['f_name']='$q_vals['f_val'] = $_POST['pFirstName'];', $q_vals['l_name']=$q_vals['l_val'], $q_vals['u_name']='$q_vals['u_val']' where $q_vals['u_name']=$q_vals['u_curr']";
		mlavu_query("update [rsl_db] set [f_name]='[f_val]', [l_name]='[l_val]' where [u_name]='[u_curr]' limit 1", $q_vals);

	}
	//if username is changed, must update restaurant table and leads table also
	
	
	
	$message .= '<span style="color:#aedc37;font:12px Verdana;">Your changes have been made. Press green done button to update page</span>';
	echo 'personal|'.$message.'|'.$_POST['pFirstName'].'|'.$_POST['pLastName'].'|'.$_POST['pUsername'];
	
	
}

if(isset($_POST['submit_availability'])){
	$avail_vals = array();
	$avail_vals['mon'] = (isset($_POST['aMonday']))? $_POST['aMonday'] : ' ';
	$avail_vals['tue'] = (isset($_POST['aTuesday']))? $_POST['aTuesday'] : ' ';
	$avail_vals['wed'] = (isset($_POST['aWednesday']))? $_POST['aWednesday'] : ' ';
	$avail_vals['thu'] = (isset($_POST['aThursday']))? $_POST['aThursday'] : ' ';
	$avail_vals['fri'] = (isset($_POST['aFriday']))? $_POST['aFriday'] : ' ';
	$avail_vals['sat'] = (isset($_POST['aSaturday']))? $_POST['aSaturday'] : ' ';
	$avail_vals['sun'] = (isset($_POST['aSunday']))? $_POST['aSunday'] : ' ';
	$avail_vals['u_curr'] = $_POST['p_current_username'];
	
	
	$a_distro = get_distro_from_array_distrocode_dataname(array(), $avail_vals['u_curr'], '');
	$o_special = create_specials($a_distro['special_json'], $a_distro['id']);
	$o_special->mercuryAcceptsList = ($values['mercuryAcceptsList'] == 'checked') ? '1' : '0';
	$o_special->monday = $avail_vals['mon'];
	$o_special->tuesday = $avail_vals['tue'];
	$o_special->wednesday = $avail_vals['wed'];
	$o_special->thursday = $avail_vals['thu'];
	$o_special->friday = $avail_vals['fri'];
	$o_special->saturday = $avail_vals['sat'];
	$o_special->sunday = $avail_vals['sun'];
	save_specials($o_special, $a_distro['id']);
	
	echo 'availability'.
		'|'.$avail_vals['mon'].
		'|'.$avail_vals['tue'].
		'|'.$avail_vals['wed'].
		'|'.$avail_vals['thu'].		
		'|'.$avail_vals['fri'].
		'|'.$avail_vals['sat'].
		'|'.$avail_vals['sun'];
		//var_dump($avail_vals);//'ahoy fat jenny';
};

if(isset($_POST['submit_company'])){
	$values = array();
	$values['message']='<span style="color:#aedc37;font:12px Verdana;">Your changes have been made. Press green done button to update page</span>';//
	$values['name'] = $_POST['cName'];
	$values['address'] = $_POST['cAddress'];
	$values['city'] = $_POST['cCity'];
	$values['state'] = $_POST['cState'];
	$values['zip'] = $_POST['cPostalCode'];
	$values['country'] = $_POST['cCountry'];
	$values['phone'] = $_POST['cPhone'];
	$values['email'] = $_POST['cEmail'];
	$values['web'] = $_POST['cWebsite'];
	$values['u_curr'] = $_POST['p_current_username'];
	$values['mercuryAcceptsList'] = $_POST['cAcceptsMercury'];
	
	$values['rsl_db'] = '`poslavu_MAIN_db`.`resellers`';
	
	mlavu_query("update [rsl_db] set `company`='[name]', `address`='[address]', `city`='[city]' , `state`='[state]', `country`='[country]', `postal_code`='[zip]', `phone`='[phone]', `email`='[email]', `website`='[web]' where `username`='[u_curr]' limit 1", $values);
	
	// save specials
	$a_distro = get_distro_from_array_distrocode_dataname(array(), $values['u_curr'], '');
	$o_special = create_specials($a_distro['special_json'], $a_distro['id']);
	$o_special->mercuryAcceptsList = ($values['mercuryAcceptsList'] == 'checked') ? '1' : '0';
	save_specials($o_special, $a_distro['id']);
	
	//var_dump($values);
	echo 'company|'.
		$values['message'].
		'|'.$values['name'].
		'|'.$values['address'].
		'|'.$values['city'].
		'|'.$values['state'].
		'|'.$values['zip'].
		'|'.$values['country'].
		'|'.$values['phone'].
		'|'.$values['email'].
		'|'.$values['web'];
}

if(isset($_POST['submit_logo'])){
	//$values = array();
	//$values['rsl_db'] = '`poslavu_MAIN_db`.`resellers`';
	
	//$values['message'] = '<span style="color:#aedc37;font:12px Verdana;">Your changes have been made. Press green done button to update page</span>';
	//echo 'image upload';
	//mlavu_query("update [rsl_db] set `notes`='[notes]' where `username`='[u_curr]'", $values);
	$booger = utf8_decode($_POST['logo_file']);
	//echo 'booger:: '.$booger;
	$break_string_arr = explode(';base64,', $booger);
	$mimetype= str_replace('data:image/','',$break_string_arr[0]);
	$booger64 = str_replace('data:image/jpeg;base64,', '', $booger);
	//echo 'boobs:: '.base64_decode($booger64);
	//$mimetype = getImageMimeType($booger64);
	//echo ',,,,,mimetype:: '.$mimetype;
	$should_be_image= base64_decode($break_string_arr[1]);
	//echo 'file sizer:==:'.strlen($should_be_image);
	if(strlen($should_be_image) > 150000){
		echo 'fail||| Image must be smaller than 1.5mb';
	}
	if($mimetype =='jpeg' || $mimetype=='jpg'){
		$filename= $_POST['username'].'.jpg';
		$filename_jpg= $_POST['username'].'.jpg';
	}else if($mimetype=='png'){
		$filename= $_POST['username'].'.png';
		$filename_png= $_POST['username'].'.png';
	}else{
		echo 'fail||| Must be of type *.jpg, *.jpeg, or *.png';
	}
	/*$should_be_image= base64_decode($break_string_arr[1]);
	$filename= $_POST['username'].'.jpg';*/
	$filename2= $_POST['username'].'.png';
	//echo ' ,;,  '.$filename;
	if (file_exists("../../images/logos/fullsize/".$filename_jpg)){
		    echo $username . "success||| Image uploaded||||../../images/logos/fullsize/$filename";
		    //$remove_oldcmd = "rm -f ".lavuShellEscapeArg("../../images/logos/fullsize/$filename_jpg");
		    //lavuExec($remove_oldcmd);
			unlink("../../images/logos/fullsize/$filename_jpg");
		   ///$remove_oldcmd = "rm -f ".lavuShellEscapeArg("../../images/logos/$filename_jpg");
		    //lavuExec($remove_oldcmd);
			unlink("../../images/logos/$filename_jpg");
		    //echo .$filename;
		    file_put_contents("../../images/logos/fullsize/".$filename,$should_be_image);
		    resizeAndSave($filename);
		    			
	}else if(file_exists("../../images/logos/fullsize/".$filename_png)) {
		echo $username . "success||| Image uploaded||||../../images/logos/fullsize/$filename";
		//$remove_oldcmd = "rm -f ../../images/logos/fullsize/".$filename_png;
		//exec($remove_oldcmd);
		unlink("../../images/logos/fullsize/$filename_png");
		//$remove_oldcmd = "rm -f ../../images/logos/".$filename_png;
		//exec($remove_oldcmd);
		unlink("../../images/logos/$filename_png");
		 
		file_put_contents("../../images/logos/fullsize/".$filename,$should_be_image);
		resizeAndSave($filename);
		    
    }else{
	    file_put_contents("../../images/logos/fullsize/".$filename,$should_be_image);
	    resizeAndSave($filename);
	    echo "success||| Image uploaded||||../../images/logos/fullsize/$filename";
    }
	
	//var_dump($_FILES);
	//echo $should_be_image.', ';
	//var_dump($_POST);
	//echo 'logo';
	//uploadImage();
}//if(isset($_POST['submit']))

if(isset($_POST['submit_notes'])){
	$values = array();
	$values['notes'] = $_POST['add_edit_textarea'];
	$values['u_curr'] = $_POST['p_current_username'];
	
	//var_dump($values);
	
	mlavu_query("update [rsl_db] set `notes`='[notes]' where `username`='[u_curr]' limit 1", $values);
	
	$values['message'] = '<span style="color:#aedc37;font:12px Verdana;">Your changes have been made. Press green done button to update page</span>';
	echo 'notes|'.$values['message'].'|'.$values['notes'];
}

function uploadImage(){
		$username= $_POST['pUsername'].'.png';
		$allowedExts = array("jpg", "jpeg", "gif", "png");
		$basename = basename( $_FILES["file"]["name"]);
		$extension = end(explode(".",$basename));
		
		if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/png") || 
		($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg")) && 
		($_FILES["file"]["size"] < 2000000) && in_array($extension, $allowedExts)){
			echo 'if long file';
			if ($_FILES["file"]["error"] > 0){
		    	echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
		    
		    }else{
		    	echo '  no file error';
		    	if (file_exists("../images/logos/fullsize/" . $username)){
		    		echo $username . "Logo already exists. Overwriting. ";
		    		move_uploaded_file($_FILES["file"]["tmp_name"],"../images/logos/fullsize/".$username);
		    		resizeAndSave($username);
		    		
		    	}else{
		    		move_uploaded_file($_FILES["file"]["tmp_name"],"../images/logos/fullsize/".$username);
		    		resizeAndSave($username);
		    		echo "Successfully uploaded your logo. Thank you!";
		    		
		    	}
		    }
		}
		
}
function resizeAndSave($filename){
	//echo "copyingDDD ../../images/logos/fullsize/".$filename."";
 	copy("../../images/logos/fullsize/".$filename."" , "../../images/logos/".$filename);
 	$inv_main_size= getimagesize("../../images/logos/fullsize/".$filename);
 	
 	$convertcmd = "convert ".lavuShellEscapeArg("../../images/logos/$filename")." -thumbnail '120x120 >' ".lavuShellEscapeArg("../../images/logos/$filename");
    lavuExec($convertcmd);
 }
 
function doPassUpdate(){
	//echo "username: ".$_POST['username'];
	if( $_POST['pPassword'] != $_POST['pConfirm'] ){
	
		return "<div style='color:red'>Your new passwords do not match please try again.  </div>";
		//updatePassword($_POST['username']);
	
	}else{
	
		mlavu_query("update `poslavu_MAIN_db`.`resellers` set `password`=PASSWORD('[1]') where `username`='[2]' and `password`= PASSWORD('[3]') limit 1", $_POST['pPassword'], $_POST['p_current_username'], $_POST['pCurrent']);
		if(ConnectionHub::getConn('poslavu')->affectedRows()){
			return "<p style='color:#aecd37;'>password successfully updated.</p>";
		}else{ 
			return "<div style='color:red'>current password is incorrect. please try again.</div>";
			//updatePassword($_POST['username']);
		}		
	}
}//doPassUpdate

// for loading and saving the special_json from the database
function create_specials($s_special_json, $distro_id) {
	$o_json_decoder = new Json();
	$o_special = $o_json_decoder->decode($s_special_json);
	// set the defaults if the values don't already exist
	$a_specials_defaults = array('mercuryAcceptsList'=>'0');
	$b_save = FALSE;
	foreach($a_specials_defaults as $k=>$v) {
		if (!isset($o_special->$k)) {
			$o_special->$k = $v;
			$b_save = TRUE;
		}
	}
	if ($b_save)
		save_specials($o_special, $distro_id);
	return $o_special;
}
function save_specials($o_special, $distro_id) {
	global $maindb;
	$s_special = json_encode($o_special);
	mlavu_query("UPDATE `[maindb]`.`resellers` SET `special_json`='[special_json]' WHERE `id`='[id]'",
		array("maindb"=>$maindb, "special_json"=>$s_special, "id"=>$distro_id));
}
function getBytesFromHexString($hexdata)
{
  for($count = 0; $count < strlen($hexdata); $count+=2)
    $bytes[] = chr(hexdec(substr($hexdata, $count, 2)));

  return implode($bytes);
}

function getImageMimeType($imagedata)
{
  $imagemimetypes = array( 
    "jpeg" => "FFD8", 
    "png" => "89504E470D0A1A0A", 
    "gif" => "474946",
    "bmp" => "424D", 
    "tiff" => "4949",
    "tiff" => "4D4D"
  );

  foreach ($imagemimetypes as $mime => $hexbytes)
  {
    $bytes = getBytesFromHexString($hexbytes);
    if (substr($imagedata, 0, strlen($bytes)) == $bytes)
      return $mime;
  }

  return NULL;
}

/*$encoded_string = "....";
$imgdata = base64_decode($encoded_string);*/
