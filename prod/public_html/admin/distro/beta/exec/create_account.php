<?php
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/register/create_account.php");
	require_once(dirname(__FILE__)."/../../../sa_cp/billing/procareas/SalesforceIntegration.php");
	require_once(dirname(__file__)."/../distro_user.php");
	require_once(dirname(__file__)."/distro_user_functions.php");
	$message = "";
	
	function confirm_restaurant_shard($data_name)
	{
		$str = "";
		$letter = substr($data_name,0,1);
		if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
			$letter = $letter;
		else
			$letter = "OTHER";
		$tablename = "rest_" . $letter;
		
		$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
		if(mysqli_num_rows($mainrest_query))
		{
			$str .= $letter . ": " . $data_name . " exists";
		}
		else
		{
			$str .= $letter . ": " . $data_name . " inserting";
			
			$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
			if(mysqli_num_rows($rest_query))
			{
				$rest_read = mysqli_fetch_assoc($rest_query);
				
				$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
				if($success) $str .= " <font color='green'>success</font>";
				else $str .= " <font color='red'>failed</font>";
			}
		}
		return $str;
	}
	
	$company = $_POST['new_acc_comp_name'];
	$email = $_POST['new_acc_email'];
	$phone = $_POST['new_acc_phone'];
	$firstname = $_POST['new_acc_first_name'];
	$lastname = $_POST['new_acc_last_name'];
	$address = $_POST['new_acc_address'];
	$city = $_POST['new_acc_city'];
	$state = $_POST['new_acc_state'];
	$zip = $_POST['new_acc_zip'];	
	$countryInfo = explode('_', $_POST['new_acc_country']);
	$country = $countryInfo[0];
	$countryCode = $countryInfo[1];
	$default_menu = $_POST['starter_menu'];
	$distro_username = $_POST['distro_username'];
	$distro_id = $_POST['distro_id'];
	$distro_email = $_POST['distro_email'];
	$islead = $_POST['new_acc_has_lead'];
	$username = "";
	$password = "";
	$dataname = "";
	$promo = isset($_POST['promo']) ? $_POST['promo'] : '';
	
	$a_required = array();
	$a_required['default Menu']='starter_menu';
	$a_required['company Name']='new_acc_comp_name';
	$a_required['email Address']='new_acc_email';
	$a_required['phone Number']='new_acc_phone';
	$a_required['first Name']='new_acc_first_name';
	$a_required['last Name']='new_acc_last_name';
	$a_required['address']='new_acc_address';
	$a_required['country']='new_acc_country';
	$a_required['city Name']='new_acc_city';
	$a_required['state/Province Name']='new_acc_state';
	$a_required['zip Code']='new_acc_zip';
	$a_missing = array();
	foreach($a_required as $k=>$v) {
		if (!isset($_POST[$v]) || trim($_POST[$v]) == '')
			$a_missing[] = ucfirst($k);
	}
	if (count($a_missing) == 0) {
		$maindb = "poslavu_MAIN_db";

		$package_info=array();
	
		$result = create_new_poslavu_account($company,$email,$phone,$firstname,$lastname,$address,$city,$state,$zip,$default_menu,"","Client","",$package_info,$countryCode);

		$success = $result[0];
		if($success)
		{
			$username = $result[1];
			$password = $result[2];
			$dataname = $result[3];
			
			/*echo "<br>username: $username";
			echo "<br>password: $password";
			echo "<br>dataname: $dataname";*/
			
			$credvars['username'] = $username;
			$credvars['dataname'] = $dataname;
			$credvars['password'] = $password;
			$credvars['company'] = $company;
			$credvars['email'] = $email;
			$credvars['phone'] = $phone;
			$credvars['firstname'] = $firstname;
			$credvars['lastname'] = $lastname;
			$credvars['address'] = $address;
			$credvars['city'] = $city;
			$credvars['state'] = $state;
			$credvars['zip'] = $zip;
			$credvars['default_menu'] = $default_menu;
			$credvars['islead'] = $islead;
			$credvars['promo'] = $promo;
			
			mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`username`,`dataname`,`password`,`company`,`email`,`phone`,`firstname`,`lastname`,`address`,`city`,`state`,`zip`,`status`,`default_menu`,`promo`) values ('[username]','[dataname]',AES_ENCRYPT('[password]','password'),'[company]','[email]','[phone]','[firstname]','[lastname]','[address]','[city]','[state]','[zip]','manual','[default_menu]','[promo]')",$credvars);
			$signupid = mlavu_insert_id();
			confirm_restaurant_shard($dataname);

			//enter into leads d.b. if lead doesn't exist
			if($islead == 'FALSE'){
				$created = date("Y-m-d H:i:s");
				$credvars['created'] = $created;
				$credvars['accepted'] = $created;
				$credvars['made_demo_account'] = $created;
				$credvars['assigned_time'] = $created;
				$credvars['demo_type'] = 'distro';
				$credvars['lead_source'] = 'distro portal_no_merc';
				$credvars['distro_code'] = $distro_username;
				$credvars['notes'] = $created.':*:Account Created:*:Made with Create Account Button*|*';
				
				//error_log(var_dump($credvars));
				
				mlavu_query("insert into `poslavu_MAIN_db`.`reseller_leads` (`address`,`city`,`company_name`,`data_name`,`distro_code`,`email`,`firstname`,`lastname`,`notes`,`phone`,`state`,`zip`,`accepted`,`made_demo_account`,`lead_source`,`assigned_time`,`created`,`demo_type`) values('[address]','[city]','[company]','[dataname]','[distro_code]','[email]','[firstname]','[lastname]','[notes]','[phone]','[state]','[zip]','[accepted]','[made_demo_account]','[lead_source]','[assigned_time]','[created]','[demo_type]') ",$credvars);
				$reseller_lead_id = mlavu_insert_id();
			}
			
			mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `distro_code`='[1]', `signupid`='[3]' where `data_name`='[2]' limit 1",$distro_username,$dataname,$signupid);
			
			$history_str = "";
			foreach($credvars as $key => $val) $history_str .= $key . ": " . $val . "\n";
			add_to_distro_history("created new account",$history_str,$distro_id,$distro_username);
			
			$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
			if(mysqli_num_rows($rest_query))
			{
				$rest_read = mysqli_fetch_assoc($rest_query);
				
				$stat_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[1]'",$dataname);
				if(mysqli_num_rows($stat_query))
				{
					$stat_read = mysqli_fetch_assoc($stat_query);
					$status_id = $stat_read['id'];
				}
				else
				{
					mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`) values ('[1]','[2]')",$rest_read['id'],$dataname);
					$status_id = mlavu_insert_id();
				}
				
				$trial_start = date("Y-m-d H:i:s");
				$trial_end = $result[5];
				mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `trial_start`='[1]', `trial_end`='[2]' where `id`='[3]'",$trial_start,$trial_end,$status_id);
			}

			SalesforceIntegration::logSalesforceEvent($dataname, 'Trial - Reseller');
		} else {
			$message = speak("Failed to create account.");
		}
	} else {
		foreach($a_missing as $k=>$v)
			$a_missing[$k] = speak($v);
		$message = speak("Please provide each of the following: ").implode(", ",$a_missing);
	}

	if ($suppress_new_account_output)
		return;

	if ($message == "") {
		$message = "success|";
		$message .= speak("New account successfully created!")."<br><br>Username: ".$username."<br>Password: ".$password."<input type='hidden' name='new_account_data_name' value='".$dataname."' />";
		mail($distro_email, "New POSLavu Account: ".$dataname, "New account successfully created...\n\nUsername: ".$username."\nPassword: ".$password, "From: noreply@lavuinc.com");
	} else {
		$message = "fail|".$message;
	}
	
	echo $message;
?>
