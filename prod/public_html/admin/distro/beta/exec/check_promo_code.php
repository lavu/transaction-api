<?php
	if (getenv('DEV') == '1') {
		@define('DEV', 1);
		ini_set("display_errors","1");
		error_reporting(error_reporting() & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
	} else {
		@define('DEV', 0);
	}

	require_once(dirname(__file__)."/../promo_codes.php");

	$msg = '';
	$license_discount = '';

	// Validate the promo code, if they've entered one.
	if ( !empty($_REQUEST['promo_code']) && check_promo_code($_REQUEST['promo_code'], $_REQUEST['license_name'], $license_discount) )
	{
		// Either add a $ or % to the discount amount, depending upon the value type.
		$license_discount_labelled = '';
		if (floatval($license_discount) < 1) {
			$license_discount_labelled = (string) (floatval($license_discount) * 100) . '%';
		}
		else {
			$license_discount_labelled = '$' . (string) $license_discount;
		}  

		$msg = "success|A {$license_discount_labelled} discount will be applied to the license cost above.";
	}
	else
	{
		$msg = "failure|Error: The promo code you entered is invalid or has already been used.";
	}

	echo $msg;
	exit();

?>