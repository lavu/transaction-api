<?php
/* An ajax file for updating the status of a lead
 * 
 * author: Martin and Ben
 */

//ini_set("display_errors",1);
session_start();
require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");

$i_lead_id = (isset($_POST['leadid']))?$_POST['leadid']:'';
$s_lead_dataname = (isset($_POST['leaddataname'])) ? $_POST['leaddataname'] : (isset($_POST['dataname']) ? $_POST['dataname'] : '');
$s_distro_code = $_POST['username'];
$s_progress = $_POST['progress'];

// accept/decline
switch($s_progress) {
	case 'lead_accepted':
		
		// try to update the lead
		$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `accepted`='[1]' WHERE `id`='[2]'";
		mlavu_query($s_query_string, date("Y-m-d H:i:s"), $i_lead_id);
		if (ConnectionHub::getConn('poslavu')->affectedRows() > 0) {
			
			// try to update the restaurant (if there is one) with the distro code
			$a_leads = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('reseller_leads', array('id'=>$i_lead_id), TRUE, array('selectclause'=>'`data_name`,`distro_code`'));
			if (count($a_leads) > 0) {
				$a_lead = $a_leads[0];
				if (trim($a_lead['data_name']) != '' && trim($a_lead['distro_code']) != '') {
					mlavu_query("UPDATE `poslavu_MAIN_db`.`restaurants` SET `distro_code`='[distro_code]' WHERE `data_name`='[data_name]'", $a_lead);
				}
			}
			
			echo 'reload|Lead Accepted';
		} else {
			echo 'alert|Lead id '.$i_lead_id.' not recognized/updated. Please try again or contact support.';
		}
		
		break;
		
	case 'lead_declined':
		
		// get previous declined information
		$s_query_string = "SELECT `declined_by` FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `id`='[2]'";
		$s_query = mlavu_query($s_query, $i_lead_id);
		$a_new_declined_by = array($s_distro_code);
		if ($s_query){
			if ($a_read = mysqli_fetch_assoc($s_query)) {
				$s_declined_by = $a_read['declined_by'];
				$a_new_declined_by = explode('|*distro_code*|');
				if (!in_array($s_distro_code, $a_new_declined_by))
					$a_new_declined_by[] = $s_distro_code;
			}
		}
		$s_new_declined_by = implode('|*distro_code*|',$a_new_declined_by);
		
		$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `_declined`='1',`declined_by`='[1]',`distro_code`='' WHERE `id`='[2]'";
		mlavu_query($s_query_string, $s_new_declined_by, $i_lead_id);
		$to = //'martin@poslavu.com';
		$subject = "LEAD DECLINED";
		$message = "A distributer declined a lead. username = ".$s_distro_code;
		mail($to, $subject, $message);
		if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
			echo 'reload|Lead Declined';
		else
			echo 'alert|Lead id '.$i_lead_id.' or disto '.$s_new_declined_by.' not recognized/updated. Please try again or contact support.';
		
		break;
		
	case 'contacted':
		
		// get extra information necessary for the "contacted" phase
		$s_comp_name = $_POST['company_name'];
		$s_contact_note = $_POST['contact_comment'];
		$to_remove = array("\r\n", "\n", "\r", '"');
		$replace = '';
		$s_contact_note = str_replace($to_remove, $replace, $s_contact_note);
		
		$s_contact_method = (isset($_POST['contact_method']))?$_POST['contact_method']:"none specified";
		
		//get any previous notes
		$db = '`poslavu_MAIN_db`.`reseller_leads`';
		$prev_notes_query_string = "select * from [1] where `company_name`='[2]' or `data_name`='[2]' limit 1";
		$prev_notes_query = mlavu_query($prev_notes_query_string, $db, $s_comp_name);
		$prev_note = '';
		if($prev_notes = mysqli_fetch_assoc($prev_notes_query)){
			$prev_note = $prev_notes['notes'];
		}
		
		// get the contacted meta information
		if($s_contact_note == 'any comments here'){
			$s_contact_note = 'contacted by: '.$s_contact_method;
		}else{
			$s_contact_note = 'contacted by: '.$s_contact_method.', note: '.$s_contact_note;
		}
		$contact_note_to_note_field = 'CONTACTED NOTE:*:'.$s_contact_note.':*:'.date("Y-m-d H:i:s").'*|*'.$prev_note;
		
		// update the lead
		if ($i_lead_id != '')
			$s_whereclause = "`id`='[id]'";
		else
			$s_whereclause = "`company_name`='[company_name]' or `data_name`='[data_name]'";
		$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `contacted`='[contacted]', `contact_note`='[contact_note]' , `notes`='[notes]' WHERE $s_whereclause limit 1";
		$a_query_vars = array('contacted'=>date("Y-m-d H:i:s"), 'company_name'=>$s_comp_name, 'data_name'=>$s_comp_name, 'id'=>$i_lead_id, 'contact_note'=>$s_contact_note, 'notes'=>$contact_note_to_note_field);
		
		mlavu_query($s_query_string, $a_query_vars);
		if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
			echo 'reload|Lead Contacted';
		else
			echo 'alert|fail. not recognized/updated. Please try again or contact support.';
		
		break;
		
	case 'demo_created':
		
		// update the lead
		if (!isset($s_data_name))
			$s_data_name = $s_lead_dataname;
		$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `made_demo_account`='[made_demo_account]',`data_name`='[data_name]' , `demo_type`= '[demo_type]' WHERE `id`='[id]'";
		mlavu_query($s_query_string, array('made_demo_account'=>date("Y-m-d H:i:s"), 'data_name'=>$s_data_name, 'id'=>$i_lead_id, 'demo_type'=>$demo_type));
		if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
			echo 'reload|Demo Created';
		else
			echo 'alert|Account '.$s_data_name.' or lead '.$i_lead_id.' not recognized/updated. Please try again or contact support.';
		
		break;

	case 'license_applied':
		
		// get the lead's id
		if ($i_lead_id == '' && $s_lead_dataname != '') {
			$i_lead_id_query = mlavu_query("SELECT `id` FROM `poslavu_MAIN_db`.`reseller_leads` WHERE `data_name`='[1]'",$s_lead_dataname);
			if ($i_lead_id_query) {
				if ($i_lead_id_read = mysqli_fetch_assoc($i_lead_id_query)) {
					$i_lead_id = $i_lead_id_read['id'];
				}
			}
		}
		
		// update the lead
		if ($i_lead_id != '') {
			$s_query_string = "UPDATE `poslavu_MAIN_db`.`reseller_leads` SET `license_applied`='[1]',`_finished`='1' WHERE `id`='[2]'";
			mlavu_query($s_query_string, date("Y-m-d H:i:s"), $i_lead_id);
			if (ConnectionHub::getConn('poslavu')->affectedRows() > 0)
				echo 'reload|License applied';
			else
				echo 'alert|Lead '.$i_lead_id.' not recognized/updated. Please try again or contact support.';
		} else {
			echo 'reload|Not a lead';
		}
		
		break;
}