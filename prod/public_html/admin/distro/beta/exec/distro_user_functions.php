<?php
	// if $b_update is TRUE, it will attempt to update a row with a matching action, reseller id, and reseller name
	// @return: TRUE if the update/insert succeeded, FALSE otherwise
	function add_to_distro_history($action,$details,$reseller_id,$reseller_name,$b_update=FALSE)
	{
		$vars = array();
		$vars['action'] = $action;
		$vars['details'] = $details;
		$vars['reseller_id'] = $reseller_id;
		$vars['reseller_name'] = $reseller_name;
		$vars['datetime'] = date("Y-m-d H:i:s");
		
		$b_update_failed = TRUE;
		$b_success = FALSE;
		
		if ($b_update) {
			$b_update_failed = FALSE;
			mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_history` SET `details`='[details]',`datetime`='[datetime]' WHERE `action`='[action]' AND `reseller_id`='[reseller_id]' AND `reseller_name`='[reseller_name]'", $vars);
			if (ConnectionHub::getConn('poslavu')->affectedRows() == 0)
				$b_update_failed = TRUE;
			else
				$b_success = TRUE;
		}
		
		if ($b_update_failed) {
			mlavu_query("INSERT INTO `poslavu_MAIN_db`.`reseller_history` (`action`,`details`,`reseller_id`,`reseller_name`,`datetime`) VALUES ('[action]','[details]','[reseller_id]','[reseller_name]','[datetime]')",$vars);
			if ((int) ConnectionHub::getConn('poslavu')->insertID() >= 1)
				$b_success = TRUE;
		}
		
		return $b_success;
	}
?>
