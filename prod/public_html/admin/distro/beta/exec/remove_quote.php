<?php
	ini_set("display_errors", 1);
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once('/home/poslavu/public_html/admin/cp/objects/json_decode.php');
	require_once(dirname(__file__)."/distro_user_functions.php");
	require_once(dirname(__file__) . "/../../../sa_cp/billing/procareas/SalesforceIntegration.php");
	//require_once(dirname(__FILE__)."/../../../cp/resources/json.php");
	//require_once("/home/poslavu/public_html/register/create_account.php");
	//require_once(dirname(__file__)."/../distro_user.php");
	//require_once(dirname(__file__)."/distro_user_functions.php");

	//var_dump($_POST);

	if ($_POST['to_call']=='remove_multiple') {
		remove_multiple();
	}
	else {
		$dataname = $_REQUEST['dataname'];
		$reseller_quote_id = $_REQUEST['reseller_quote_id'];
		$salesforce_quote_id = $_REQUEST['salesforce_quote_id'];

		mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_quotes` SET `_deleted`='1' WHERE `id`='[1]'", $reseller_quote_id);
		if ( ConnectionHub::getConn('poslavu')->affectedRows() > 0 ) {
			$fields['reseller_quote_id'] = '`poslavu_MAIN_db`.`reseller_quotes_lineitems`';
			mlavu_query("UPDATE `poslavu_MAIN_db`.`reseller_quotes_lineitems` SET `_deleted`='1' WHERE `reseller_quote_id` = '[1]'", $reseller_quote_id);
		}

		SalesforceIntegration::createSalesforceEvent( $dataname, 'update_quote', json_encode( array('QuoteId' => $salesforce_quote_id, 'Status' => 'Removed in Portal') ) );
	}
	exit;

	// Functions

	function remove_multiple() {
		//$a_ids = LavuJson::json_decode($json_resources);
		//echo("remove multiple leads response:: ".$_POST['lead_ids']);
		$a_ids = explode(", ", $_POST['reseller_quote_ids']);
		$s_in_clause = arrayToInClause($a_ids,TRUE);

		//print_r($a_ids);
		$fields = array();
		$fields['_canceled'] = '1';
		$fields['db'] = '`poslavu_MAIN_db`.`reseller_quotes`';
		$fields['id'] = $_POST['reseller_quote_id'];

		$q_string = "update [db] set `_canceled`='[_canceled]' WHERE `id` {$s_in_clause}";
		//echo $q_string;

		mlavu_query($q_string, $fields);

		//error_log('us');
		$action = 'cancelled quotes';
		$distro_code = $_POST['distro_code'];
		$details = 'username='.$distro_code.' unassigned leads with ids::'.$s_in_clause;
		$reseller_id = $_POST['res_id'];

		//error_log("exec/remove_lead#51 add_to_history(".$action.",".$details.",".$reseller_id.",".$reseller_name.")");
		
		add_to_distro_history($action,$details,$reseller_id,$distro_code);
	}

	function arrayToInClause(&$indexed_array, $b_use_value = FALSE) {
			$a_vals = array();
			if ($b_use_value) {
				foreach($indexed_array as $v)
					if ($v !== '')
						$a_vals[] = $v;
			} else {
				foreach($indexed_array as $k=>$v)
					if ($k !== '')
						$a_vals[] = $k;//mysqli_real_escape_string($k);
			}
			if (count($a_vals) == 0)
				return 'IN ()';
			return 'IN (\''.implode('\',\'', $a_vals).'\')';
	} //arrayToInClause
	