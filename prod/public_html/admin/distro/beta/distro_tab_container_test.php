<?php
	//session_start();

	require_once(dirname(__FILE__)."/distro_data_functions.php");
	//require_once(dirname(__FILE__)."/timing_functions_test.php");
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();

	global $o_accountsTab;
	global $o_unassignedTab;
	global $o_leadsTab;
	global $o_specialistTab;
	global $o_resourcesTab;
	require_once(dirname(__FILE__)."/distro_tabs/accountsTab.php");
	require_once(dirname(__FILE__)."/distro_tabs/unassignedTab.php");
	require_once(dirname(__FILE__)."/distro_tabs/leadsTab.php");
	require_once(dirname(__FILE__)."/distro_tabs/specialistTab.php");
	require_once(dirname(__FILE__)."/distro_tabs/resourcesTab.php");
/**
tab_container.php
author: Martin Tice
property of Lavu inc.

This file holds the classes for the tab contianer the user sees when logging in

**/
if (isset($_SESSION['update_accounts'])){
		//echo 'tab'.$_SESSION['update_accounts'];
	}

ini_set("display_errors",1);
ini_set("memory_limit","128M");

class tab_container{
	private static $current_user; // contains all information on user (distro_user object)
    private static $view_markup; // this holds all the markup to be returned.
    private static $tab_list; // holds the markup for the list of tab sections and relative links
    private static $tab_sections ;//holds markup for each tab added.
    private static $s_licenses_not_upgrades; // names of licenses the user has that aren't upgrades (as bar-delimeted string)
    private static $a_licenses_not_upgrades; // all distro licenses that aren't upgrades
    private static $a_licenses_upgrades; // alll distro licenses that are upgrades
    private static $a_licenses_not_upgrades_not_special; // ids of all licenses that aren't upgrades and aren't specially priced licenses

    public function __construct($user, $admin){
		global $o_package_container;
		//build the tab_container
		self::$current_user = $user;
		self::$tab_list = '<div id="tab-container" class="tab-container">
			<ul class="tabs">';
		self::$tab_sections = '<div id="panel-container">
		';

		$licenses = self::$current_user->get_licenses();
		self::$a_licenses_not_upgrades = array();
		self::$a_licenses_upgrades = array();
		foreach($licenses as $s_id=>$a_license) {
			$s_license_name = $a_license['type'];
			if ($o_package_container->get_package_status($s_license_name) != "upgrade") {
				$a_license['printed_name'] = $o_package_container->get_printed_name_by_attribute('name', str_replace(" Promo", "", $a_license['type']));
				$a_license['is_special'] = self::$current_user->license_is_special($s_id);
				self::$a_licenses_not_upgrades[$s_id] = $a_license;
			} else {
				$a_license['printed_name'] = $o_package_container->get_upgrade_printed_name($a_license['type']);
				$a_license['is_special'] = self::$current_user->license_is_special($s_id);
				self::$a_licenses_upgrades[$s_id] = $a_license;
			}
		}

		// get a string representing all licenses (not upgrades)
		$s_licenses_not_upgrades = implode('|', self::$a_licenses_not_upgrades);

		// get a string representing all licenses (not upgrades, not special licenses)
		$a_licenses_not_upgrades_not_special = array('all'=>array(), 'Silver'=>array(), 'Gold'=>array(), 'Platinum'=>array());
		foreach(self::$a_licenses_not_upgrades as $s_id=>$a_license) {
			if ($a_license['is_special'] != 0)
				continue;
			$a_licenses_not_upgrades_not_special['all'][] = $s_id;
			$a_licenses_not_upgrades_not_special[$a_license['printed_name']][] = $s_id;
		}
		foreach($a_licenses_not_upgrades_not_special as $k=>$v)
			$a_licenses_not_upgrades_not_special[$k] = implode('|', $v);

		self::$s_licenses_not_upgrades = $s_licenses_not_upgrades;
		self::$a_licenses_not_upgrades_not_special = $a_licenses_not_upgrades_not_special;

		if (self::$current_user->get_info_attr('username') == 'lavureports'){
			$this->build_reports_view();
		}
		else if(self::$current_user->get_info_attr('access') == 'ingram'){
			//echo '<br />ingram reseller<br />';
			$this->build_ingram_view();
		}else if($admin == 'admin'){
			$this->build_admin_view();
		}else if($admin == 'mercury'){
			$this->build_mercury_view();
		}else if ($admin == 'sales'){
			$this->build_sales_view();
		}else{
			$this->build_distributor_view();
		}
		/*
		if($admin){
			//build admin view
		}else{
			// build distributor view
		}*/
	}
    /*
	public function __construct($user, $admin){
		global $o_package_container;
		//build the tab_container
		self::$current_user = $user;
		self::$tab_list = '<div id="tab-container" class="tab-container">
			<ul class="tabs">';
		self::$tab_sections = '<div id="panel-container">
		';

		$licenses = self::$current_user->get_licenses();
		self::$a_licenses_not_upgrades = array();
		self::$a_licenses_upgrades = array();
		foreach($licenses as $s_id=>$a_license) {
			$s_license_name = $a_license['type'];
			if ($o_package_container->get_package_status($s_license_name) != "upgrade") {
				$a_license['printed_name'] = $o_package_container->get_printed_name_by_attribute('name', str_replace(" Promo", "", $a_license['type']));
				$a_license['is_special'] = self::$current_user->license_is_special($s_id);
				self::$a_licenses_not_upgrades[$s_id] = $a_license;
			} else {
				$a_license['printed_name'] = $o_package_container->get_upgrade_printed_name($a_license['type']);
				$a_license['is_special'] = self::$current_user->license_is_special($s_id);
				self::$a_licenses_upgrades[$s_id] = $a_license;
			}
		}
		//echo 'blast!!:: '.count ($a_licenses_not_upgrades);
		if(count($a_licenses_not_upgrades) > 0)
			$s_licenses_not_upgrades = implode('|', $a_licenses_not_upgrades);
		self::$s_licenses_not_upgrades = $s_licenses_not_upgrades;
		if (self::$current_user->get_info_attr('username') == 'lavureports'){
			$this->build_reports_view();
		}
		else if($admin == 'admin'){
			$this->build_admin_view();
		}else if($admin == 'mercury'){
			$this->build_mercury_view();
		}else if(self::$current_user->get_info_attr('access') == 'ingram'){
			echo '<br />ingram reseller<br />';
			$this->build_ingram_view();
		}else{
			self::$current_user->get_info_attr('access');
			$this->build_distributor_view();
		}
	}//__construct()
	*/

	// builds the fiew for the admin only
	private function build_admin_view(){
		
		$this->build_modal_content();
		$resellers = self::$current_user->get_all_resellers();
		$this->build_header_admin();
		//$this->add_news_tab();
		$this->add_leads_tab();
		//$this->add_unassigned_tab($resellers);
		$this->add_resellers_tab($resellers);
		//$this->add_accounts_tab();
		$this->add_resources_tab();
		//$this->add_archive_tab($resellers);
		$this->add_my_info_tab();
		$this->add_licenses_tab();
		//$this->add_reports_tab();
		//$this->add_processed_tab($resellers);
		if (self::$current_user->get_info_attr('username') == 'martin'){
			//$this->add_processed_tab($resellers);
			$this->add_reports_tab();
			//$this->add_accounts_tab();
		}
		self::$tab_list .= '</ul>';
		self::$tab_sections .= '</div></div>';
		echo self::$tab_list;
		echo self::$tab_sections;
	}//build_admin_view

	// builds the tab view for distributor only
	private function build_distributor_view(){
		$this->build_modal_content();
		$this->build_header();
		$this->add_news_tab();
		$this->add_leads_tab();
		$this->add_accounts_tab();
		$this->add_resources_tab();
		$this->add_licenses_tab();	
		$this->add_my_info_tab();					
		self::$tab_list .= '</ul>';
		self::$tab_sections .= '</div></div>';		
		echo self::$tab_list;
		echo self::$tab_sections;
	}//build_distributor_view

	private function build_reports_view(){
		
		$this->build_modal_content();
		$resellers = self::$current_user->get_all_resellers();
		$this->build_header_admin();
		$this->add_news_tab();
		//$this->add_leads_tab();
		//$this->add_unassigned_tab($resellers);
		//$this->add_resellers_tab($resellers);
		//$this->add_accounts_tab();
		//$this->add_resources_tab();
		//$this->add_archive_tab($resellers);
		$this->add_my_info_tab();
		//$this->add_licenses_tab();
		$this->add_reports_tab();
		$this->add_processed_tab($resellers);
		
		self::$tab_list .= '</ul>';
		self::$tab_sections .= '</div></div>';
		echo self::$tab_list;
		echo self::$tab_sections;
	}//build_reports_view

	//builds the tab view for the mercury men
	private function build_mercury_view(){
		//$this->build_header();(might do this before class instantiation)
		$this->build_modal_content();
		self::$current_user->set_all_resellers();
		$resellers = self::$current_user->get_all_resellers();
		$this->build_header_mercury();
		//$this->add_news_tab();
		//$this->add_leads_tab();
		$this->add_unassigned_tab($resellers);
		//$this->add_resellers_tab();
		//$this->add_accounts_tab();
		//$this->add_resources_tab();
		//$this->add_licenses_tab();
		$this->add_my_info_tab();
		$this->add_processed_tab($resellers);
		self::$tab_list .= '</ul>';
		self::$tab_sections .= '</div></div>';
		echo self::$tab_list;
		echo self::$tab_sections;
	}//build_mercury_view

	private function build_ingram_view(){
		$this->build_modal_content();
		$this->build_header_ingram();
		$this->add_news_tab();
		$this->add_leads_tab();
		$this->add_accounts_tab();
		$this->add_resources_tab();
		$this->add_licenses_tab();	
		$this->add_my_info_tab();					
		self::$tab_list .= '</ul>';
		self::$tab_sections .= '</div></div>';		
		echo self::$tab_list;
		echo self::$tab_sections;
	}//build_ingram_view

	//
	//
	//builds all of the content to be displayed as modal.
	// ex. forms, loading screens, etc.
	private function build_modal_content(){

		$is_admin = self::$current_user->get_info_attr('access');

		$res_id = self::$current_user->get_info_attr('id');
		$res_name = self::$current_user->get_info_attr('f_name').' '.self::$current_user->get_info_attr('l_name');
		$country = self::$current_user->get_info_attr('country');
		if($is_admin == 'admin'){
			//echo 'admin';
			$modal_content	= '<div class="opaque_modal_bg"></div>';
			self::$current_user->set_all_resellers();
		}else if($is_admin == 'agree' && $_GET['hide_agree'] != 'admin'){
			//echo 'SIGN AGREEMENT!!!  '.$is_admin.'</br>';
			$modal_content	= '<div class="opaque_modal_bg" style="display:block;"></div>';
		}else{
			$modal_content	= '<div class="opaque_modal_bg"></div>';
		}
			//$modal_content	= '<div class="opaque_modal_bg"></div>';

		$licenses = self::$current_user->get_licenses();
		$account_labels_options = self::$current_user->get_account_labels_options();
		$user = self::$current_user;
		$user_username = $user->get_info_attr('username');
		$resellers = $user->get_all_resellers();
		//var_dump($account_labels_options);

		//the background black opaque div.
		/*<label for="new_acc_email">Promo Code</label><br />
							<input type="text" name="promo" id="promo" />*/
		function get_new_account_form($account_labels_options,$user){
			$account_form = '
				<div id="account_form_div" style="display:none;">
					<div id="success_account_form" style="display:none;text-align:center;"></div>
					<form id="new_account" method="post" action="">
						<div id="new_account_form_title" style="font:24px Verdana;color:#717074;text-align:left;">Create Account</div>
						<div id="new_account_form_required" style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					<div>
						<fieldset>
							<label for="starter_menu">Default Menu*</label><br />
							<div class="select_arrow_bg_new_account">
								<select name="starter_menu" id="starter_menu">
									'.$account_labels_options.'
								</select>
							</div><br /><br />
							<label for="new_acc_address">Address*</label><br />
							<input type="text" name="new_acc_address" id="new_acc_address"/><br />
							<label for="new_acc_state">State/Province*</label><br />
							<input type="text" name="new_acc_state" id="new_acc_state" /><br />
							<label for="new_acc_first_name">First Name*</label><br />
							<input type="text" name="new_acc_first_name" id="new_acc_first_name" /><br />
							<label for="new_acc_email">Email*</label><br />
							<input type="text" name="new_acc_email" id="new_acc_email" /><br />
							
						</fieldset>
						<fieldset>
							<label for="new_acc_comp_name">Company Name*</label><br />
							<input type="text" name="new_acc_comp_name" id="new_acc_comp_name"/><br />
							<label for="new_acc_city">City*</label><br />
							<input type="text" name="new_acc_city" id="new_acc_city" /><br />
							<label for="new_acc_zip">Zip*</label><br />
							<input type="text" name="new_acc_zip" id="new_acc_zip" /><br />
							<label for="new_acc_last_name">Last Name*</label><br />
							<input type="text" name="new_acc_last_name" id="new_acc_last_name" /><br />
							<label for="new_acc_phone">Phone*</label><br />
							<input type="text" name="new_acc_phone" id="new_acc_phone" />
							<input type="hidden" name="distro_username" id="distro_username" value="'.$user->get_info_attr('username').'">
							<input type="hidden" name="distro_id" id="distro_id" value="'.$user->get_info_attr('id').'">
							<input type="hidden" name="distro_email" id="distro_email" value="'.$user->get_info_attr('email').'">
							<input type="hidden" name="promo" id="promo">
							<input type="hidden" name="new_acc_has_lead" id="new_acc_has_lead" value=""/>
						</fieldset>
						<br style="clear:left;">
					</div>
					<div id="new_acc_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_new_account">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;" id="submit_new_account" />
					</div>
				</form>
				<div id="ok_acc_buttons" style="text-align:center;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_new_account">OK</span>
					</div>
			</div>';//account_form_div

			return $account_form;
		}//get_new_account_form

		function get_new_lead_form($user_username, $is_admin = ''){
			//$user->get_info_attr('username');
			//echo 'get_new_lead_form is_admin:: '.$is_admin;
			$hide_from_specialists = 'hide_from_specialists';
			if($is_admin == 'admin')
			{
				$hide_from_specialists = '';
			}
			$new_lead_form = '
				<div id="new_lead_form_div" style="display:none;">
					<div id="success_lead_form" style="display:none;text-align:center;"></div>
					<form id="new_lead_form" method="post" action="">
						<div id="new_lead_form_title" style="font:24px Verdana;color:#717074;text-align:left;">Create New Lead</div>
						<div id="new_lead_form_required" style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					<div>
						<fieldset>
						<label for="select_accept_lead">Do You wish to accept this lead?</label><br />
						<div class="select_arrow_bg_new_account">
								<select name="select_accept_lead" id="select_accept_lead">
									<option value="0">Accept</option>
									<option value="1">Decline</option>
								</select>
						</div><br /><br />
							<label for="new_lead_address">Address*</label><br />
							<input type="text" name="new_lead_address" id="new_lead_address"/><br />
							<label for="new_lead_state">State/Province*</label><br />
							<input type="text" name="new_lead_state" id="new_lead_state" /><br />
							<label for="new_lead_first_name">First Name*</label><br />
							<input type="text" name="new_lead_first_name" id="new_lead_first_name" /><br />
							<label for="new_lead_email">Email*</label><br />
							<input type="text" name="new_lead_email" id="new_lead_email" /><br /><br /><br />
							';
							if($hide_from_specialists != 'hide_from_specialists')
							{
								$new_lead_form .='
								<label for="campaign_source">Select Source</label>
								<div class="select_arrow_bg_no_float '.$hide_from_specialists.' style="float:none;">
									<select name="source_campaign" id="source_campaign">
										<option value="none">None Specified</option>
										<option value="mercury">try.poslavu.com</option>
										<option value="CHD">CHD</option>
										<option value="lavu.com">lavu.com</option>
										<option value="software_advice">Software Advice</option>
										<option value="salesgenie">SalesGenie</option>
										<option value="orlando_mailer">Orlando Mailer</option>
										<option value="other">Other*</option>
									</select>
								</div><br />
								<label for="other_lead_source">*If Other, please specify</label><br />
								<input type="text" name="other_lead_source" id="other_lead_source" /><br />
								<label for="new_lead_by_phone">Lead called us?</label>
								<input type="checkbox" name="new_lead_by_phone" id="new_lead_by_phone" value="called_us" style="width:20px;"/>
								';
							}else if($user_username == 'distrotest')
							{
								$new_lead_form .='
								<label for="other_lead_source">*Please Specify Source of Lead</label><br />
								<input type="text" name="other_lead_source" id="other_lead_source" /><br />
								';
							}
							$new_lead_form .= '
							<br /><label for="new_lead_no_merc">NOT interested in Mercury</label>
							<input type="checkbox" name="new_lead_no_merc" id="new_lead_no_merc" value="no_merc" style="width:20px;"/>
						</fieldset>
						<fieldset>
							<label for="new_lead_comp_name">Company Name*</label><br />
							<input type="text" name="new_lead_comp_name" id="new_lead_comp_name"/><br />
							<label for="new_lead_city">City*</label><br />
							<input type="text" name="new_lead_city" id="new_lead_city" /><br />
							<label for="new_lead_zip">Zip*</label><br />
							<input type="text" name="new_lead_zip" id="new_lead_zip" /><br />
							<label for="new_lead_last_name">Last Name*</label><br />
							<input type="text" name="new_lead_last_name" id="new_lead_last_name" /><br />
							<label for="new_lead_phone">Phone*</label><br />
							<input type="text" name="new_lead_phone" id="new_lead_phone" />
							<input type="hidden" name="new_lead_distro_username" id="new_lead_distro_username" value="'.$user_username.'">
						</fieldset>
						<br style="clear:left;">
					</div>
					<div id="new_lead_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_new_lead">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;" id="submit_new_lead" />
					</div>
				</form>
				<div id="ok_lead_buttons" style="text-align:center;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_new_lead">OK</span>
					</div>
			</div>';//account_form_div

			/*
			<input type="hidden" name="distro_username" id="distro_username" value="'.$user->get_info_attr('username').'">
			<input type="hidden" name="distro_id" id="distro_id" value="'.$user->get_info_attr('id').'">
			<input type="hidden" name="distro_email" id="distro_email" value="'.$user->get_info_attr('email').'">
			*/

			return $new_lead_form;
		}//get_new_lead_form

		function get_apply_license_form($licenses, $user, $a_distro_nonupgrade_licenses, $a_distro_upgrade_licenses){
			global $o_package_container;
			$options = '';
			$gold = 0;
			$silver = 0;
			$platinum = 0;
			$none_available = '<div>No licences available to apply. Please purchase in licenses tab</div>';
			$s_distro_nonupgrade_licenses_json = str_replace('"', "'", str_replace("'", '', json_encode($a_distro_nonupgrade_licenses)));
			$s_distro_upgrade_licenses_json = str_replace('"', "'", str_replace("'", '', json_encode($a_distro_upgrade_licenses)));
			foreach($licenses as $key=>$value){
					$type = $value['type'];
					$printed_name = preg_replace('/[1-2]/', '', $type);
					$options .= '<option value="'.$type.'">'.$printed_name.'</option>';
					if($printed_name == 'Gold'){
						$gold++;
					}else if($printed_name == 'Silver'){
						$silver++;
					}else if($printed_name == 'Platinum'){
						$platinum++;
					}
			}
			if($gold > 0 || $silver > 0 || $platinum > 0){
				$none_available = '';
			}
			/*if($d_name == null){
				$d_name = 'Benwa Balls';
				$current = 'Gold';
			}*/
			//<div><span>Current License</span><span>'.$Gold.'</span></div>
			$apply_lic_form = '
			<div id="apply_license_form_div" style="display:none;">
				<div id="success_apply_license_form" style="display:none;text-align:center;"></div>
				<input type="hidden" name="distro_licenses_json" id="distro_licenses_json" value="'.$s_distro_nonupgrade_licenses_json.'" />
				<input type="hidden" name="distro_upgrades_json" id="distro_upgrades_json" value="'.$s_distro_upgrade_licenses_json.'" />
				<form id="apply_license_form" method="post" action="">
					<div style="font:24px Verdana;color:#717074;text-align:left;">Apply License</div>
					<div style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					<div>
						<fieldset>
							<legend>Apply License To:
								<span id="apply_license_company_name"></span>
								<input type="hidden" name="apply_license_dname" id="apply_license_dname" value="" />
								<input type="hidden" name="available_licenses_to_apply" id="available_licenses_to_apply" value="" />
								<input type="hidden" name="available_special_licenses_to_apply" id="available_special_licenses_to_apply" value="" />
								<input type="hidden" name="licenseupgrade_string" id="licenseupgrade_string" value="" />
							</legend>
							<label for="apply_lic_available_select">Apply:</label>
							<div id="select_arrow_bg_apply_license">
								<select name="apply_lic_available_select" id="apply_lic_available_select">
								'.$options.'
								</select>
								<input type="hidden" name="distro_id" id="distro_id" value="'.$user->get_info_attr('id').'">
								<input type="hidden" name="is_id" value="0" />
							</div>
						</fieldset>
						<fieldset>
							<legend>Available Licenses</legend>
							<div>'.$silver.' Silver</div>
							<div>'.$gold.' Gold</div>
							<div>'.$platinum.' Platinum</div>'.$none_available.'
						</fieldset>
					</div>
					<div id="apply_license_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;" id="cancel_apply_license">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;" id="submit_apply_license" />								</div>
				</form>
				<div id="ok_app_button" style="text-align:center;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_app_license">OK</span>
					</div>
			</div>';
			return $apply_lic_form;
		}//get_apply_license_form

		function get_assign_lead_form($resellers){
			//var_dump($resellers);
			$options = '<option value=""></option>';
			//echo count($resellers);
			$json_decode = new Json();
			foreach($resellers as $key=>$value){
				$accepts_mercury = $json_decode->decode($value['special_json']);
				$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
				$merc_logo= '';
				if($merc_dist){
					//$merc_logo='<img src="./images/i_merc_mini.png" alt="mm" />';
					$merc_logo = 'MERCURY';
					//echo $merc_logo.", ".$value['f_name'].$value['l_name'].'<br />';
				}
				//echo 'username::'.$value['username'].'<br/>';//'<option value="'.$value.'">'.$value['firstname'].' '.$value['lastname'].'</option><br />';
				if($value['access'] != 'agree' && $value['hasAgreed'] != '3'){
					$options .= '<option value="'.$key.'">'.$value['f_name'].' '.$value['l_name'].', username: '.$value['username'].','.$value['city'].','.$value['state'].','.$value['postal_code'].', '.$merc_logo.'</option>';
				}
			}

			$assign_lead_form = '
			<div id="assign_lead_form_div" style="display:none;">
				<div id="success_assign_lead_form" style="display:none;text-align:center;"></div>
				<form id="assign_lead_form" method="post" action="">
					<div style="font:24px Verdana;color:#717074;text-align:left;">Assign Lead</div>
					<div style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					<div>
						<fieldset>
							<legend>Assign Lead to Reseller</legend>
							<input type="hidden" id="assign_lead_lead_id" name="assign_lead_lead_id" value="" />
							<label for="all_reseller_select">*Choose Reseller:</label>
							<div id="select_arrow_bg_apply_license">
								<select name="all_reseller_select" id="all_reseller_select">
								'.$options.'
								</select>
						</fieldset>
					</div>
					<div id="assign_leads_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;" id="cancel_assign_lead">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;" id="submit_assign_lead" />								</div>
				</form>
				<div id="ok_assign_button" style="text-align:center;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_assign_btn">OK</span>
				</div>
			</div>';
			return $assign_lead_form;
		}//get_assign_lead_form

		function get_news_form(){
			$news_form = '
			<div id="news_form_div" style="display:none;">
				<div id="success_news_form" style="display:none;text-align:center;"></div>
					<form id="news_form" method="post" action="">
						<legend>ADD NEWS</legend>
						<div style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>


						<label for="add_news_title">*Title of News</label><br />
						<input type="text" name="add_news_title" id="add_news_title"/><br />

						<label for="add_news_content">*Content</label><br />
						<textarea name="add_news_content" id="add_news_content" style="width:300px;height:200px;"></textarea><br />

						<div id="add_news_buttons" style="text-align:right; margin-top:20px;">
							<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_add_news">Cancel</span>
							<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="submit_add_news" />
							<span style="margin:10px auto;border:1px solid #717074;padding:10px;cursor:pointer;display:none;" id="ok_save_news_changes">SAVE CHANGES</span>
							<span style="margin:10px auto;border:1px solid #717074;padding:10px;cursor:pointer;display:none;background:pink;" id="ok_delete_news_entry">DELETE ENTRY</span>
						</div>

					</form>
					<div id="ok_add_news" style="text-align:center;display:none;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;cursor:pointer;" id="ok_add_news_btn">OK</span>
					</div>
			</div>';
			return $news_form;

		}//get_news_form
		function get_lead_note_form(){
			$lead_note_form = '
			<div id="lead_note_form_div" style="display:none;">
				<div id="success_lead_note_form" style="display:none;text-align:center;"></div>
					<div style="float:left;width:350px;height:310px;">
					<form id="lead_note_form" method="post" action="">
						<legend>ADD NOTE</legend>
						<div style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>


						<label for="add_lead_note_title">*Title of Note</label><br />
						<input type="text" name="add_lead_note_title" id="add_lead_note_title"/><br />

						<label for="add_lead_note_content">*Content</label><br />
						<textarea name="add_lead_note_content" id="add_lead_note_content" style="width:300px;height:200px;"></textarea><br />
						<input type="hidden" name="lead_note_reseller" id="lead_note_reseller" value="" />
						<input type="hidden" name="lead_note_lead_id" id="lead_note_lead_id" value="" />
						<input type="hidden" name="lead_note_previous" id="lead_note_previous" value="" />
						<input type="hidden" name="function" id="lead_note_function" value="notes_icon_note" />
						<div id="add_lead_note_buttons" style="text-align:center; margin-top:20px;">
							<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_add_lead_note">Cancel</span>
							<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="submit_add_lead_note" />
						</div>

					</form>
					</div>
					<div id="lead_notes_display" style="float:left;width:350px;height:375px;overflow-y:scroll">
					</div>
					<div id="ok_add_lead_note" style="text-align:left;display:none;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;cursor:pointer;" id="ok_add_lead_note_btn">OK</span>
					</div>
			</div>';
			return $lead_note_form;

		}//get_lead_note_form
		function get_distro_agreement($is_admin, $res_id, $country, $res_name){
			//echo $is_admin.'</br>';
			$require_signin = '';
			$agreement = './Resources/Documents/Reseller_Agreement_Rev_9_10_2013_Domestic.pdf';
			if($is_admin == 'agree'){
				$require_signin = 'display:block;';
				//echo 'acess:: '.$is_admin.'<br />';
			}else{
				$require_signin = 'display:none;';
			}
			if($country != 'USA' && $country != 'United States'){
				//echo '$country:: '.$country.'<br />';
				$agreement = './Resources/Documents/Reseller_Agreement_Rev_9_10_2013_International.pdf';
			}


			$d_agree = '
			<div id="distro_agreement_div" style="'.$require_signin.'">
				<div style="font:24px verdana;">DISTRIBUTER AGREEMENT</div>
				<div id="success_d_agree" style="display:none;text-align:center;"></div>
					<div id="d_agree_pdf_div" style="height:375px;border:5px solid #f0f0ff;">
						<iframe src="'.$agreement.'" style="height:100%;width:100%;"></iframe>
					</div>
					<div id="d_agree_div" style="text-align:center;margin:10px;">
						<form id="d_agree_form" method="post" action="">
						<span>I have read and agreed to the above terms*</span>
							<input type="checkbox" id="d_agreed_check" name="d_agreed_check" />
							<input type="submit" style="margin:10px auto;border:1px solid #717074;padding:10px;cursor:pointer;" /><br />
							<input type="hidden" name="res_id" value="'.$res_id.'">
							<input type="hidden" name="signed_agreement" value="'.$res_id.'">
							<input type="hidden" name="res_name" value="'.$res_name.'">
							<p>* You are seeing this message because you must agree to the above lavu distributor conditions before being allowed to continue using the distributor portal.</p>
						</form>
					</div>
			</div>';
			return $d_agree;
		}//get_distro_agreement
		function get_resource_upload(){
			$resource_upload = '
				<div id="resource_upload_form_div" style="display:none;">
					<div id="success_resource_form" style="display:none;text-align:center;padding:60px;cursor:pointer;"></div>
					<form id="resource_upload_form" method="post" action="" enctype="multipart/form-data">
						<div id="resource_upload_form_title" style="font:24px Verdana;color:#717074;text-align:left;">Upload New Resource</div>
						<div id="resource_upload_form_required" style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					<div>
						<fieldset style="display:inline-block;vertical-align:top;">
						
						<label for="new_resource_title">Title*</label><br />
						<input type="text" name="new_resource_title" id="new_resource_title"/><br />
						<label for="new_resource_sub_title">Sub-Title*</label><br />
						<input type="text" name="new_resource_sub_title" id="new_resource_sub_title"/><br /><br />

						<label for="new_resource_featured">Featured</label>
						<input type="checkbox" name="new_resource_featured" id="new_resource_featured" value="featured"/><br />
						<label for="new_resource_home">Home Display</label>
						<input type="checkbox" name="new_resource_home" id="new_resource_featured" value="home"/><br />

						
						';
						/*<label for="resource_file_type">File Type Selection *</label><br />
						<div class="select_arrow_bg_new_account" style="width:55px;">
							<select name="resource_file_type" id="resource_file_type">
								<option value="notset">CHOOSE TYPE</option>
								<option value="data:image/png">PNG</option>
								<option value="data:image/jpeg">JPG</option>
								<option value="data:image/gif">GIF</option>
								<option value="data:application/pdf">PDF</option>
								<option value="data:text/plain">TXT</option>
							</select>
						</div><br /><br />
						*/
						$resource_upload .= '
						</fieldset>
						<fieldset style="display:inline-block;vertical-align:top;">
							<label for="resource_file">UPLOAD FILE</label>
							<input type="file" name="resource_file" id="resource_file"><br />
							<label for="resource_description">Description</label>
							<textarea id="resource_description" name="resource_description" style="width:300px;height:80px;"></textarea><br /><br /><br /><br />
							<label for="section_for_resource">Which section should resource be located in?</label><br />
						<span>Sales</span>
						<input type="radio" name="section_for_resource" id="section_for_resource" value="sales" style="width:20px;" onclick="update_sub_section_drop_down(\'sales\');"/><br />
						<span>Marketing</span>
						<input type="radio" name="section_for_resource" id="section_for_resource" value="marketing" style="width:20px;" onclick="update_sub_section_drop_down(\'marketing\');"/><br />
						<label for="resource_sub_section">Sub-Section Selection</label><br />
						<div class="select_arrow_bg_new_account">
							<select name="resource_sub_section" id="resource_sub_section">
								<option value="Reseller Agreements" class="sales_sub_category">Sales::Reseller Agreements</option>
								<option value="User Guides" class="sales_sub_category">Sales::User Guides</option>
								<option value="Cookbook" class="sales_sub_category">Sales::Cookbook</option>
								<option value="Comparison Charts" class="sales_sub_category">Sales::Comparison Charts</option>
								<option value="Reseller Agreements" class="sales_sub_category">Sales::Reseller Agreements</option>
								<option value="Defeat List" class="sales_sub_category">Sales::Defeat List</option>
								<option value="Business vertical sales points" class="sales_sub_category">Sales:: Business vertical sales points</option>
								<option value="Maintainence" class="sales_sub_category">Sales::Maintainence</option>
								<option value="Videos" class="sales_sub_category">Sales::Videos</option>
								<option value="Features List" class="marketing_sub_category">Marketing::Features List</option>
								<option value="Graphics" class="marketing_sub_category">Marketing::Graphics</option>
								<option value="How To Lavu" class="marketing_sub_category">Marketing::How To Lavu</option>
								<option value="Marketing Text" class="marketing_sub_category">Marketing::Marketing Text</option>
								<option value="Photos" class="marketing_sub_category">Marketing::Photos</option>
								<option value="Pricing" class="marketing_sub_category">Marketing::Pricing</option>
								<option value="Print Media" class="marketing_sub_category">Marketing::Print Media</option>
								<option value="Testimonials" class="marketing_sub_category">Marketing::Testimonials</option>
								<option value="Videos" class="marketing_sub_category">Marketing::Videos</option>
								<option value="Website Components" class="marketing_sub_category">Marketing::Website Components</option>

							</select>
						</div><br /><br />
						</fieldset>
					</div>
					<div id="resource_upload_buttons" style="margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="resource_upload_cancel" onclick="done_resource_upload_form();">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="submit_resource_upload" value="SUBMIT RESOURCE"/>
					</div>
				</form>
				<div id="ok_resource_buttons" style="text-align:center;display:none;height:30px;">
					<span style="margin:20px auto;border:1px solid #717074;padding:10px;cursor:pointer;" id="ok_resource_buttons">OK</span>
				</div>
				<div id="resource_upload_preview" style="text-align:center;display:none;">
				</div>
			</div>';
			return $resource_upload;
		}//get_resource_upload()
		function get_resource_download_view($is_admin){
			$require_signin = '';
			$agreement = './Resources/Documents/Reseller_Agreement_Rev_9_10_2013_Domestic.pdf';
			if($is_admin == 'agree'){
				$require_signin = 'display:block;';
				//echo 'acess:: '.$is_admin.'<br />';
			}else{
				$require_signin = 'display:none;';
			}
			if($country != 'USA' && $country != 'United States'){
				//echo '$country:: '.$country.'<br />';
				$agreement = './Resources/Documents/Reseller_Agreement_Rev_9_10_2013_International.pdf';
			}
			error_log('martinmartinmartinmartin get_resource_download_view');
			$d_agree = '
			<div id="resource_download_div" style="display:none;">
				<div>
					<div id="resource_download_name" style="font:24px verdana;display:inline-block;vertical-align:top;">RESOURCE VIEW</div>
					<div style="float:right;cursor:pointer;" onclick="close_resource_download();">
						<img src="./images/close_button_roll.png" alt="close resource download">
					</div>
				</div>
				<div id="resource_hidden_thing" style="display:none;text-align:center;"></div>
					<div id="resource_large_preview_div" style="height:375px;border:5px solid #f0f0ff;">
						<iframe src="" style="height:100%;width:100%;" id="resource_download_iframe" name="resource_download_iframe"></iframe>
					</div>
					<div>
						<div>Click to Download:: <a href="" id="resource_download_click" style="color:blue;" download="" name=""></a>
						</div>
					</div>
			</div>';
			return $d_agree;
		}//get_resource_download_view()

		$modal_content .= get_new_account_form($account_labels_options,$user);
		$modal_content .= get_apply_license_form($licenses, $user, self::$a_licenses_not_upgrades, self::$a_licenses_upgrades);
		$modal_content .= get_new_lead_form($user_username, $is_admin);
		$modal_content .= get_assign_lead_form($resellers);
		$modal_content .= get_news_form();
		$modal_content .= get_lead_note_form();
		//get_resource_download_view($is_admin);
		$is_admin = self::$current_user->get_info_attr('access');
		$modal_content .= get_resource_download_view($is_admin);
		if($is_admin != 'admin' && $is_admin != 'mercury'){
			//echo $is_admin;
			$modal_content .= get_distro_agreement($is_admin, $res_id, $country, $res_name);
		}else if($is_admin='admin'){
			$modal_content .= get_resource_upload();
		}
		echo $modal_content;
	}//build_modal_content


	//
	//
	//all of the functions that return tab contents
	private function add_news_tab(){
	
		$access = self::$current_user->get_info_attr("access");
		//echo $access;
		//gets the news from news.txt, wraps html around it, and displays
		function get_news($access){
			//echo $acces.' mmm';
			$readHandle=fopen("./distro_news/news_test.txt", 'r');
			$contents = fread($readHandle, filesize("./distro_news/news.txt"));
			fclose($readHandle);
			
			$cont=explode("|", $contents);
			//$contents=$cont[1];
			$news_entries = '';
			$count = 0;
			$edit_option = '';
			/*if($access == 'admin'){
				$edit_option = '<div class="edit_news_entry" entry="'.$count.'">edit</div>';
			}*/
			foreach($cont as $entry){
				if($access == 'admin'){
					$edit_option = '<div class="confirm_edit_news_entry" entry="'.$count.'">edit</div>';
				}
				$seperated_entry = explode(":*:", $entry);
				
				if($seperated_entry[0] != '@@@@@')
				{
					$news_entries .= $edit_option.'<div class="confirm_news_entry" id="news_entry'.$count.'">
						<div class="entry_title editable_news_part" id="entry_title'.$count.'" contenteditable="true"><h3>'.$seperated_entry[0].'</h3></div>
						<div class="entry_date " id="entry_date'.$count.'">'.$seperated_entry[1].'</div>
						<div class="entry_body editable_news_part" id="entry_body'.$count.'" contenteditable="true">'.$seperated_entry[2].'</div>					
						</div>';
				}
				$count++;
			//echo $count.'<br />';
			}
			
			
			return $news_entries;
			
		}//get_news($access)
		function get_featured_distributor($access){
			//echo $acces.' mmm';
			$readHandle=fopen("./distro_news/featured_test.txt", 'r');
			$contents = fread($readHandle, filesize("./distro_news/featured_test.txt"));
			fclose($readHandle);
			
			$cont=explode("|||||", $contents);
			//$contents=$cont[1];
			$incentive = '';
			$count = 0;
			$edit_option = '';
			
			
			$newest_entry = explode("::*::", $cont[0]);
			if($access=='admin'){
				$featured = '
				<div class="featured_entry" id="featured_entry'.$count.'" ><div class="edit_featured_entry" entry="'.$count.'" style="margin:20px;padding:10px;">CONFIRM CHANGES</div>
						<div style="height:70px;">
							<h3 class="featured_entry_title featured_entry_edit" id="featured_entry_title'.$count.'" contenteditable="true" style="margin-bottom:10px;">'.$newest_entry[0].'</h3>
						</div>
						<div class="featured_entry_body featured_entry_edit" id="featured_entry_body'.$count.'" contenteditable="true">'.$newest_entry[1].'</div>
					</div>';
			}else{
				
				
				$featured = '
				<div class="featured_entry" id="featured_entry'.$count.'" >
						<div style="height:70px;">
							<h3 class="featured_entry_title" id="featured_entry_title'.$count.'" style="margin-bottom:10px;">'.$newest_entry[0].'</h3>
						</div>
						<div class="featured_entry_body" id="featured_entry_body'.$count.'" >'.$newest_entry[1].'</div>
					</div>';
				
			}
					
			
			return $featured;
			
		}//get_featured_distributor($access)
		function get_old_news_letters($access){
			$files = '';
			$files = scandir('./newsletters/');
			$leg = str_replace("/", " -- ", $dir);
			//$list = '<fieldset style="text-align:left;margin:10px 0px;color:#999;padding:20px 10px;"><legend>'.$leg.'</legend><div style="float:left;width:400px;margin:10px;padding:20px;">';
			//$half = (count($files) / 2);
			$count = 0;
			$new_div = true;
				//$parts = explode('.', $file);
				//$ext = end($parts);
			//$image_types = array('jpg','png','jpeg');
				//$doc_types = array('doc','docx','txt');
				//$pdf_type = array('pdf');
				//$rtf_type = array('rtf');
			//echo '--'.$dir.',  half:'.$half."<br><br>";
			$list = '';
			foreach($files as $file){
				
					if ($file != "." && $file != "..") {
					//echo "<<br>";
					//if(is_dir)
						$arr = explode(".",$file);
						$ext = end($arr);
						$ext = strtolower($ext);
						
						/*if(in_array($ext, $image_types)){
							//echo 'img::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else if($ext=='pdf' || $ext=='PDF'){
							//echo 'pdf::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_pdf.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else{
							//echo 'doc::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_document.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}*/
						
						$list .= '<div>';
							$list .= '<a href="https://admin.poslavu.com/distro/beta/newsletters/'.$file.'" target="_blank">';
							$list .= $file;
						$list .= '</a>';
						$list .= '</div>';
					}
					$count++;
				
					//$new_div = false;
					//$list .='</div><div style="float:left;width:400px;margin:10px;padding:20px;">';
					
			}
		//return $images;
			return $list;
		};
		function get_top_distributors($access){
			$top_d = '
				<ol>
					<li>Dylon 30 sold</li>
					<li>Dylon 25 sold</li>
					<li>Dylon 18 sold</li>
					<li>Dylon 17 sold</li>
					<li>Dylon 10 sold</li>
				</ol>
			';
			return $top_d;
		};
		// add content to tab_list
		$title ='<li class="tab" style="width:60px;"><a href="#news_tab">NEWS</a></li>
		';

		// add content to tab_sections
		$content ='<div id="news_tab" class="tab_section tab00">';

		//top of of the tab
		$news_top = '
		<div class="tab_top">
			<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;margin-left:80px;">RESELLER NEWS</div>';
			if($access === 'admin'){
				$news_top .= '<div id="leads_top_right" style="text-align:right;width:150px;float:right;margin-right:60px;position:relative;padding-top:9px;">
						<div id="add_news" style="border:1px solid #999;padding:10px;width:100px;float:right;background:#aecd37;cursor:pointer;">ADD NEWS</div>
					</div>';
			}
		//$news_top .= '</div>';

		$news_top .= '<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="b_w_f" /></div>
			</div>
		';//end news_tab_top
		/*<div id="news_tab_top" class="tab_top">
				<div class="tab_top_top">
					<span style="font:24px Verdana;color:#717074;">DISTRO NEWS</span>
				</div>';*/
		//body of the tab
		$news_body = '
		<div id="left_news_content" style="float:left;">
			<div class="left_news_content_title" style="position:relative;top:75px;margin-left:50px;color:blue;text-align:left;">RECENT NEWS</div>
			<div id="news_content">';
		$news_body .= get_news($access).'</div>';

		$news_body .= '
			<div class="left_news_content_title" style="position:relative;top:75px;margin-left:50px;color:blue;text-align:left;">FEATURED DISTRIBUTOR</div>
			<div id="featured_distributor">';
		$news_body .= get_featured_distributor($access).'</div>';

		$news_body .= '
		</div>';//end left_content

		$news_body .= '
		<div id="right_news_content" style="float:left;width:200px;position:relative;top:120px;">
			<div class="right_news_content_title">OLD NEWS LETTERS</div>
			<div id="old_news_letters" style="height:300px; border:1px solid black; margin-bottom:30px;padding:5px;overflow-x:hidden;font:13px verdana;">
		';
		$news_body .= get_old_news_letters($access).'</div>';
		$news_body .= '
			<div class="right_news_content_title">TOP 5 DISTRIBUTORS OF LAST MONTH</div>
			<div id="get_top_distributors" style="height:300px; border:1px solid black; margin-bottom:30px;">
		';
		$news_body .= get_top_distributors($access).'</div>';

		$content .= $news_top;
		$content .= $news_body;


		$content .= '
		</div></div>';//all_content;

		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_news_tab
	private function add_leads_tab(){

		
		global $o_leadsTab;
		$a_retval = $o_leadsTab->tabToStr(self::$current_user, $resellers);
		self::$tab_list .= $a_retval['title'];
		self::$tab_sections .= $a_retval['content'];
		return ;

		$leads = self::$current_user->get_leads();
		// add content to tab_list
		function count_new_leads($leads){
			$count = 0;
			foreach($leads as $dname=>$attrs){
			$ls = isset($attrs['lead_status'])?$attrs['lead_status']:'accept';			
				//echo 'lead_status::<br />'.$attrs['lead_status'].'<br /><br />';
				if($ls === 'accept'){
					//echo 'new lead::<br/>'.$attrs['company_name'].'<br /><br />';
					$count++;
				}
			}
			//echo 'total new::<br />'.$count.'<br /><br />';
			return $count;
		}
		function lead_color($stat, $div,$color_class){
		//echo 'stat='.$stat.' div='.$div.' cc='.$color_class;
			if($stat === $div){
				//echo '  true';
				return $color_class;
			}
		}
		function lead_type_content($level, $data_name, $s_licenses_string){
			if($level === 'accept'){
				return '<div style="margin:50px;font:12px Verdana;color:#717074;">
					<span style="margin-right:30px;">Do you accept this new account</span>
						<img src="./images/btn_accept.png" style="margin-right:10px;cursor:pointer;" class="accept_lead_confirm" alt="btn_accpt"/>
							<span class="accept_vert_div" style="border-right:1px solid #999;margin-right:10px;"></span>
						<img src="./images/btn_decline.png" style="cursor:pointer;" alt="btn_dec" class="decline_lead_confirm"/>
				</div>';
			}else if($level === 'contact'){
				$dname_nospace = str_replace(' ', '', $data_name);
				$dname_nospace = str_replace('\'', '', $dname_nospace);
				$dname_nospace = str_replace('"', '', $dname_nospace);
				$dname_nospace = str_replace('.', '', $dname_nospace);
				$dname_nospace = str_replace(',', '', $dname_nospace);
				return '
				<div>
					<form id="contact_form_'.$dname_nospace.'" method="post" action="">
						<div>Enter discription of how you contacted the customer</div>
						<div>
							<div style="margin:20px;">CONTACT METHOD::</div>
								<div style="width:150px;float:left;text-align:left;padding-top:25px;font:14px verdana;margin-left:255px;">
									<input type="radio" name="contact_method" value="phone" />PHONE<br />
									<input type="radio" name="contact_method" value="email" />EMAIL<br />
									<input type="radio" name="contact_method" value="face" />FACE TO FACE<br />
									<input type="radio" name="contact_method" value="other" />OTHER<br />
								</div>
							<textarea cols="410" rows="20" id="'.$dname_nospace.'_contact_text" name="contact_comment" style="width:225px;height:100px;float:left;margin:20px 10px 0px 20px 25px;background-color:#f1f5ee;">any comments here</textarea>
							<input type="hidden" name="company_name" value="'.$data_name.'" />
							<input type="hidden" name="progress" value="contacted" />
							<div style="margin-left:10px;border:1px solid #717074;padding:10px;cursor:pointer;float:left;" class="contact_level_submit" contact_form="#contact_form_'.$dname_nospace.'" >Submit</div>
							<br style="clear:left;">

					</div>
					</div></form>
				';
				
			}else if($level === 'demo_account'){
				return '
				<div style="font:12px Verdana;color:#717074;margin:20px 10px 50px;">
				<span>Setup a Demo account for this prospect: </span><img src="./images/btn_createdemo.png" alt="btn_c_d" class="demo_account_requested" style="cursor:pointer;"/></div>';
				
			}else{
				return '<div style="font:12px Verdana;color:#717074;margin:40px">
				<span>Select License to Apply:</span>
					<img src="./images/i_gold_tiny.png" alt="img_g_t"/>
				<span style="color:#aecd37;cursor:pointer;" class="apply_license_btn" val="'.$data_name.'">GOLD</span>
					<img src="./images/i_platinum_tiny.png" alt="img_p_t"/>
				<span style="color:#aecd37;cursor:pointer;" class="apply_license_btn" val="'.$data_name.'">PLATINUM</span>
					<input type="hidden" name="available_licenses" id="available_licenses" value="'.$s_licenses_string.'" />
				</div>
				';
			}
		}
		
		$new_lead_count = count_new_leads($leads);
		//<span><img src="./images/circ_leads.png"></span>	<span style="font-size:16px;text-align:center;">'.$new_lead_count.'</span>	
		$title ='<li class="tab" style="width:110px;"><a href="#leads_tab">
			<span style="background-image:url(./images/circ_leads.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$new_lead_count.'
			</span>
			LEADS</a></li>
		';
	
		// add content to tab_sections 	
		$content ='<div id="leads_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">NEW LEADS</div>
					<div id="unass_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;padding-top:25px;">
					<img src="./images/btn_create_lead.png" alt="btn_apply" class="new_lead_btn" style="cursor:pointer;" /></span>
					</div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
			// <div style="background-image:url(./images/lead_legend.png);"></div>
		$content .= '<div class="adjust_body">
			
			<div style="height:30px;width:950px;margin-bottom:20px;margin-left:8px;"><img src="./images/lead_legend.png" alt="llegend"></div>
			<div id="lead_desc">
				<span style="width:60px;">STATUS</span>
				<span style="width:153px;">BUSINESS NAME</span>
				<span style="width:180px;">LOCATION</span>
				<span style="width:169px;">EMAIL</span>
				<span style="width:137px;">NAME</span>
				<span style="width:120px;">PHONE</span>
				<span style="width:32px;">NOTES</span>
				<span style="width:50px;">REMOVE</span>
				<br style="clear:left;" />														
			</div>
		';
	     //beginning of accordian

		$content .= '<div id="lead_acc_wrapper">
		';
		
		foreach($leads as $key=>$attrs){
			$lead_created_through_signup = FALSE;
			$lead_stat = isset($attrs['lead_status'])?$attrs['lead_status']:'accept';
			if(isset($attrs['already_made_demo_account'])) $made_demo_account =  $attrs['already_made_demo_account']; else $made_demo_account = FALSE;
			$lead_level = isset($attrs['level']) ? $attrs['level'] : '';
			if ($made_demo_account && in_array($lead_stat, array('accept', 'contact')))
				$lead_created_through_signup = TRUE;
			$lead_created_through_signup_message = '';
			//echo $lead_stat.'==><br/> ';
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_'.$lead_stat;
			if ($lead_created_through_signup) $circ_image_ref .= '_has_demo_account';
			if ($lead_created_through_signup) $lead_created_through_signup_message .= '<div style="margin:0 auto;padding:20px;font-size:12px;text-align:center;">Note: this account was created through the <a href="http://www.poslavu.com/en/ipad-pos-signup" target="_blank">POSLavu Signup Page</a> and already has a demo account.</div>';
			
			if($attrs['accepted'] == '0000-00-00 00:00:00'){
				$remove = '<span style="width:50px;text-align:center;></span>';
			}else{
				$remove = '<span style="width:50px;text-align:center;cursor:pointer;" l_id="'.$attrs['id'].'" class="remove_lead"><img src="./images/btn_decline.png" alt="big_X" /></span>';
			}
			$lead_source = '';
			if($attrs['lead_source'] == 'mercury'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}
			
			$content .= '<div class="acc_head accordionButton">
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="./images/circ_red.png" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:150px;" class="lead_on_comp_name">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:170px;">'.$attrs['zip'].' - '.$attrs['city'].'</span><div class="loc_head_div">|</div>
				<span style="width:170px;">'.$attrs['email'].'</span><div class="loc_head_div">|</div>
				<span style="width:120px;">'.$attrs['firstname'].' '.$attrs['lastname'].'</span><div class="loc_head_div">|</div>
				<span style="width:110px;">'.$attrs['phone'].'</span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" style="width:40px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$remove.'
				'.$lead_source.'
				<br class="clear:left;">
				</div>		
				';
			$content .= '<div class="acc_lead_content accordionContent">
				<div class="lead_acc_content_head">
					<img src="./images/prog_bar_full.png" alt="p_b_f"/>
				</div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>'.
				$lead_created_through_signup_message.
				lead_type_content($lead_stat, $attrs['data_name']?$attrs['data_name']:$attrs['company_name'], self::$s_licenses_not_upgrades).'</div>
			';
				
		}
		$content .= '</div></div></div>';//lead_acc_wrapper and leads tab close baby
	    //end of accordian
		
	    // just a list of values for $this->leads;	
	    /*	
		$content .= '	<ul id="lead_list" style="overflow:scroll;height:320px;">';
		foreach($leads as $key=>$attrs){
			$content .= '<li>NAME:'.'key:'.$key.'**'.$attrs['company_code'].'::attr=='.$attrs['zip'].'</li>';//.'::value=='.$value.'</li>';
			//$content .= '<li>NAME:'.$key.'::attr=='.$attrs['l_name'].'</li>';//.'::value=='.$value.'</li>';
			
			foreach($attrs as $attr=>$value){
				$content .= '<li>NAME:'.$key.'::attr=='.$attr.'::value=='.$value.'</li>
				';
			}
		}
		$content .= '</ul></div>';//all_content;
	    */
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_leads_tab
	private function add_accounts_tab(){
		global $o_package_container;

		$accounts = self::$current_user->get_accounts();
		$location_notes = self::$current_user->get_loc_notes();
		$location_contacts = self::$current_user->get_loc_contacts();
		$uname = self::$current_user->get_info_attr('username');
		$range = self::$current_user->get_info_attr('account_display_range');
		$redirect = self::$current_user->get_info_attr('account_redirect');
		$next_range_attr = '';
		$prev_range_attr = '';
		if($range != ''){

			$range_exploded = explode(",",$range);
			//var_dump($range_exploded);
			$pr_start = ((int)$range_exploded[0]*1)- 50;
			$pr_end = ((int)$range_exploded[1]*1) - 50;
			$nr_start = ((int)$range_exploded[0]*1) + 50;
			$nr_end = ((int)$range_exploded[1]*1 ) + 50;
			$prev_range_attr = $pr_start.', '.$pr_end;
			$next_range_attr = $nr_start.', '.$nr_end;
			//$range = './index.php?change_account_range='.$range.'#accounts_tab';
		}else{
			$range = '#';
			$prev_range_attr = '#';
			$next_range_attr = '50, 100';
			
		}
		if($redirect ==  ''){
			$redirect = './index.php?';
		}
	    //$location_contacts = self::$current_user->get_loc_contacts();
		//echo var_dump($location_notes['99distro_test3']).'<br />';
		//notes section in each account dropdown
		global $o_accountsTab;
		$a_retval = $o_accountsTab->tabTwoStr(self::$current_user, self::$a_licenses_not_upgrades_not_special);
		self::$tab_list .= $a_retval['title'];
		self::$tab_sections .= $a_retval['content'];
		return ;
	}//add_accounts_tab
	private function add_resources_tab(){

		global $o_resourcesTab;
		$a_retval = $o_resourcesTab->tabToStr(self::$current_user);
		self::$tab_list .= $a_retval['title'];
		self::$tab_sections .= $a_retval['content'];
		return ;
		$title ='<li class="tab"><a href="#resources_tab">RESOURCES</a></li>
		';
		function get_resource_files($dir){
			$files = '';
			$files = scandir('./Resources/'.$dir);
			$leg = str_replace("/", " -- ", $dir);
			$list = '<fieldset style="text-align:left;margin:10px 0px;color:#999;padding:20px 10px;"><legend>'.$leg.'</legend><div style="float:left;width:400px;margin:10px;padding:20px;">';
			$half = (count($files) / 2);
			$count = 0;
			$new_div = true;
				//$parts = explode('.', $file);
				//$ext = end($parts);
			$image_types = array('jpg','png','jpeg');
				//$doc_types = array('doc','docx','txt');
				//$pdf_type = array('pdf');
				//$rtf_type = array('rtf');
			//echo '--'.$dir.',  half:'.$half."<br><br>";
			foreach($files as $file){
				if($count <= $half){
					if ($file != "." && $file != "..") {
					//echo "<<br>";
					//if(is_dir)
						$arr = explode(".",$file);
						$ext = end($arr);
						$ext = strtolower($ext);
						
						if(in_array($ext, $image_types)){
							//echo 'img::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else if($ext=='pdf' || $ext=='PDF'){
							//echo 'pdf::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_pdf.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else{
							//echo 'doc::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_document.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}
						$list .= '<div>';
							$list .= $t_nail;//t_nail($file);
							$list .= '<a href="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" target="_blank">';
							$list .= $file;
						$list .= '</a>';
						$list .= '</div>';
					}
					$count++;
				}else if($new_div == true){
					$new_div = false;
					$list .='</div><div style="float:left;width:400px;margin:10px;padding:20px;">';
					if ($file != "." && $file != "..") {
					//echo "<<br>";
					//if(is_dir)
						$arr = explode(".",$file);
						$ext = end($arr);
						$ext = strtolower($ext);
						
						if(in_array($ext, $image_types)){
							//echo 'img::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else if($ext=='pdf' || $ext=='PDF'){
							//echo 'pdf::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_pdf.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else{
							//echo 'doc::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_document.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}
						$list .= '<div>';
							$list .= $t_nail;//t_nail($file);
							$list .= '<a href="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'">';
							$list .= $file;
						$list .= '</a>';
						$list .= '</div>';
					}
					$count++;
				}else{
					if ($file != "." && $file != "..") {
					//echo "<<br>";
					//if(is_dir)
						$arr = explode(".",$file);
						$ext = end($arr);
						$ext = strtolower($ext);
						
						if(in_array($ext, $image_types)){
							//echo 'img::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else if($ext=='pdf' || $ext=='PDF'){
							//echo 'pdf::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_pdf.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else{
							//echo 'doc::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_document.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}
						$list .= '<div>';
							$list .= $t_nail;//t_nail($file);
							$list .= '<a href="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'">';
							$list .= $file;
						$list .= '</a>';
						$list .= '</div>';
					}
					$count++;
				}
			}
			$list .= '</div></fieldset>';
		//return $images;
			return $list;
		}//get_resource_files
		
		//<span id="res_print_choice_">GRAPHICS</span><span class="r_h_div"> | </span>
		//<span id="res_web_choice_">LLS</span><span class="r_h_div"> | </span>
		
		$resources_top = '<div id="resources_tab_top" class="tab_top">
				<div id="res_tab_top" style="font:14px Verdana;color:#717074;" >
					<span id="doc_request" class="res_request" res="documents_content">DOCUMENTS</span><span class="r_h_div"> | </span>
					<span id="graphics_request" class="res_request" res="graphics_content">GRAPHICS</span><span class="r_h_div"> | </span>
					<span id="lls_request" class="res_request" res="lls_content">LLS</span><span class="r_h_div"> | </span>
					<span id="photos_request" class="res_request" res="photos_content">PHOTOS</span><span class="r_h_div"> | </span>
					<span id="print_request" class="res_request" res="print_content">PRINT</span><span class="r_h_div"> | </span>
					<span id="video_request" class="res_request" res="video_content">VIDEO</span><span class="r_h_div"> | </span>
					<span id="web_request" class="res_request" res="web_content">WEB</span>
				</div>
				<div class="res_drop_down" id="res_print_drop">
					<div class="res_drop_choice">
						brochures
					</div>
					<div class="res_drop_choice">
						logos
					</div>
					<div class="res_drop_choice">
						rackcards
					</div>
				</div>
				<div class="res_drop_down" id="res_web_drop">
					<div class="res_drop_choice">
						brochures
					</div>
					<div class="res_drop_choice">
						logos
					</div>
					<div class="res_drop_choice">
						rackcards
					</div>
				</div>				
				<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
			</div>
		';//end resources_tab_top
		
		//make the links. 
		
		
		// add content to tab_sections 	
		$content ='<div id="resources_tab" class="tab_section tab00">
		';
		$content .= $resources_top;
		
		$content .= '<div class="adjust_body">
		
					<div id="documents_content" class="resource_section_container" style="display:block;">
					<div class="resource_section_header" >DOCUMENT RESOURCES</div>
					'.get_resource_files('Documents').'
					</div>
					<div id="graphics_content" class="resource_section_container">
						<div class="resource_section_header">GRAPHICS RESOURCES</div>
						
							<div id="graphics_ads" class="inner_resources">'.get_resource_files('Graphics/ads').'</div>
						
						
							<div id="graphics_logos" class="inner_resources">'.get_resource_files('Graphics/Logos').'</div>
						
						
							<div id="graphics_misc" class="inner_resources">'.get_resource_files('Graphics/misc').'</div>
						
						
							<div id="graphics_products" class="inner_resources">'.get_resource_files('Graphics/products').'</div>
						
						
							<div id="graphics_screenshots" class="inner_resources">'.get_resource_files('Graphics/screenshots').'</div>
						
					</div>
					<div id="lls_content" class="resource_section_container">
					<div class="resource_section_header">LLS RESOURCES</div>
					'.get_resource_files('LLS').'
					</div>
					<div id="photos_content" class="resource_section_container">
					 	<div class="resource_section_header">PHOTO RESOURCES</div>
						<div id="photos_devices">'.get_resource_files('Photos/devices').'</div>
						<div id="photos_location">'.get_resource_files('Photos/location').'</div>
						<div id="photos_products">'.get_resource_files('Photos/products').'</div>
					</div>
					<div id="print_content" class="resource_section_container">
						<div class="resource_section_header">PRINT RESOURCES</div>
						<div id="print_banners">'.get_resource_files('Print/banners').'</div>
						<div id="print_brochures">'.get_resource_files('Print/brochures').'</div>
						<div id="print_logos">'.get_resource_files('Print/logos').'</div>
						<div id="print_product">'.get_resource_files('Print/product').'</div>
						<div id="print_rackcards">'.get_resource_files('Print/rackcards').'</div>
					</div>
					<div id="video_content" class="resource_section_container">
					<div class="resource_section_header">VIDEO RESOURCES</div>
					'.get_resource_files('Video').'
					</div>
					<div id="web_content" class="resource_section_container">
					<div class="resource_section_header">WEB RESOURCES</div>
					'.get_resource_files('Web').'
					</div>
			</div></div>
		';//all_content;
		self::$tab_list .= $title;
		self::$tab_sections .= $content;		
	}//add_resources_tab
	private function add_licenses_tab(){
		global $o_package_container;
		global $license_select;

		// get reseller info
		$my_info = self::$current_user->get_info();
		$my_licenses = self::$current_user->get_licenses();
		$points = (int)$my_info['credits'];
		$reseller_id = $my_info['id'];
		$reseller_name = $my_info['username'];
		$access = $my_info['access'];
		$lpercent = DistroSessionStats::getLpercent();

		// get license cost info
		$a_base_highest_names = $o_package_container->get_highest_base_package_names();
		foreach($a_base_highest_names as $k=>$v) {
			$i_level = $o_package_container->get_level_by_attribute('name', $v);
			$s_baselevel_name = $o_package_container->get_plain_name_by_attribute('level', $i_level % 10);
			$a_base_highest_names[$s_baselevel_name] = $v;
		}
		$license_values = array(
			"Silver"=>$o_package_container->get_value_by_attribute('name', $a_base_highest_names['Silver']),
			"Gold"=>$o_package_container->get_value_by_attribute('name', $a_base_highest_names['Gold']),
			"Platinum"=>$o_package_container->get_value_by_attribute('name', $a_base_highest_names['Platinum'])
		);

		$o_special_licenses = self::$current_user->get_special_licenses();
		foreach($o_special_licenses as $s_id=>$v)
			$o_special_licenses->$s_id->id = $s_id;
		$s_special_licenses = '<div id="buy_special_licenses" style="display:none;"><ul class="special_license_description"></ul><input type="hidden" name="available_special_licenses_json" value="{}" /></div>';
		$s_json = str_replace('"', "'", json_encode($o_special_licenses));
		$s_special_licenses = '<div id="buy_special_licenses" style="display:none;"><ul class="special_license_description"></ul><input type="hidden" name="available_special_licenses_json" value="'.$s_json.'" /></div>';

		$silver_pts = '';
		$gold_pts = '';
		$plat_pts = '';
		if($points >= $license_values['Silver']){
			$silver_points = 'show_radio';
		}if($points >= $license_values['Gold']){
			$gold_points = 'show_radio';
		}if($points >= $license_values['Platinum']){
			$plat_points = 'show_radio';
		}
		require(dirname(__file__).'/form_requirements.php');

		// make a list of licenses that can be purchased
		$license_options = '';

		if($access == 'ingram'){
			foreach($license_select as $a_option_vals){
				$option = $a_option_vals[0];
				$option_printed = $a_option_vals[1];

				$license_info = explode("-",$option_printed);
				$license_name = trim($license_info[0]);
				$license_name = str_replace("Silver", "ING_Silver", $license_name);
				$license_name = str_replace("Gold", "ING_Gold", $license_name);
				$license_name = str_replace("Pro", "ING_Pro", $license_name);
				$license_seconary_info = trim($license_info[1]);

				if($option !== ''){
					$license_options .= '<option value="'.$option.'">'.$license_name.'</option>';
				//$license_display .= '<div class="license_display">'.$option_printed.'</div>';
				} else {
					$license_options .= '<option value="&nbsp;" selected>&nbsp;</option>';
			    }
			}
		}else{
			foreach($license_select as $a_option_vals){
				$option = $a_option_vals[0];
				$option_printed = $a_option_vals[1];
				if($option !== ''){
					$license_options .= '<option value="'.$option.'">'.$option_printed.'</option>';
				} else {
					$license_options .= '<option value="&nbsp;" selected>&nbsp;</option>';
				}
			}
		}

		// add available custom purchases to the list of purchasable licenses
		require_once(dirname(__FILE__)."/../../manage/misc/AdvancedToolsFunctions/distro_tools/custom_purchases.php");
		$o_distroToolsCustomPurchases->set_permission(TRUE);
		$a_custom_purchases = $o_distroToolsCustomPurchases->get_purchases($my_info['username'], TRUE);
		foreach($a_custom_purchases['available'] as $a_custom_purchase) {
			$s_value = "Custom Purchase - ".$a_custom_purchase['id'];
			$i_amount = (int)$a_custom_purchase['amount'];
			$i_amount = ((int)$a_custom_purchase['apply_commission'] == 1) ? ($i_amount * (100-$lpercent) / 100) : $i_amount;
			$s_option_printed = $a_custom_purchase['type'].": ".$a_custom_purchase['name']." - $".number_format($i_amount, 2);
			$license_options .= "<option value=\"{$s_value}\">{$s_option_printed}</option>";
		}

		//$license_display .= '</div>';
		$license_display = '<div id="available_licenses"><div style="color:#439789;margin-bottom:20px;">AVAILABLE LICENSES::</div>';
			$gold = 0;
			$silver = 0;
			$platinum = 0;
			$upgrades = 0;
			$none_available = '<div>No licences available to apply. Please purchase above</div>';
			foreach($my_licenses as $key=>$value){
					$type = $value['type'];
					$purchased = $value['purchased'];
					$license_status = $o_package_container->get_package_status($type);
					if ($license_status == "upgrade") {
						$printed_name = $o_package_container->get_upgrade_printed_name($type);
					} else {
						$printed_name = $o_package_container->get_printed_name_by_attribute('name', $type);
					}
					if($access == 'ingram'){
						$printed_name = str_replace("Silver", "ING_Silver", $printed_name);
						$printed_name = str_replace("Gold", "ING_Gold", $printed_name);
						$printed_name = str_replace("Pro", "ING_Pro", $printed_name);
						$license_display .= '<div class="license_display"><span style="display:inline-block;width:120px;">'.$printed_name.'</span> <span style="color:black;">purchased: '.$purchased.'</span></div>';
					}else{
						$license_display .= '<div class="license_display"><span style="display:inline-block;width:120px;">'.$printed_name.'</span> <span style="color:black;">purchased: '.$purchased.'</span></div>';						
					}
					if ($license_status == 'upgrade') {
						$upgrades++;
					} else if($printed_name == 'Gold' || $printed_name == 'ING_Gold'){
						$gold++;
					}else if($printed_name == 'Silver' || $printed_name == 'ING_Silver'){
						$silver++;
					}else if($printed_name == 'Platinum' || $printed_name == 'Pro' || $printed_name == 'ING_Pro'){
						$platinum++;
					}
			}
			if($gold > 0 || $silver > 0 || $platinum > 0 || $upgrades > 0){
				$none_available = '';
			}
			$license_display .= $none_available.'</div>';

		//echo $license_display;
		$title ='<li class="tab" style="width:100px"><a href="#licenses_tab">LICENSES</a></li>
		';
		if($my_info['credits'] != ''){
			$lavu_points_display = $my_info['credits'];
		}else{
			$lavu_points_display = 'N/A';
		}
		$licenses_top_edit_points = '';
		$hide_css = '';
		if($access == 'ingram'){
			$hide_css = 'display:none;';
			$licenses_top = '
			<div id="licenses_tab_top" class="tab_top">
				<div class="tab_top_top">
					<span style="font:24px Verdana;color:#717074;">REQUESET NEW LICENSE</span>
				</div>
				<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="d_w_f"/></div>
			</div>';//end licenses_tab_top
		}else{
		$licenses_top = '
			<div id="licenses_tab_top" class="tab_top">
				<div class="tab_top_top">
					<span style="font:24px Verdana;color:#717074;">BUY NEW LICENSE</span>
					<span style="color:#B7BDB0;margin-left:388px;font-size:12px;">LAVU POINTS:</span>
					<span >'.$lavu_points_display.$licenses_top_edit_points.'</span>
				</div>
				<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="d_w_f"/></div>
			</div>';//end licenses_tab_top
		}
			/*<div id="licenses_points_top" style="text-align:right;">
						<span style="color:#B7BDB0;margin-right:100px;font-size:12px;">LAVU POINTS: </span>'.$my_info['credits'].'
					</div>*/
		// add content to tab_sections
		$buy_license_title = 'CREDIT CARD INFO';
		if($access == 'ingram'){
			$hide_from_ingram = 'display:none;';
			$buy_license_title = 'LICENSE REQUEST';
		}else{
			$hide_from_ingraom = '';
		}
		$content ='<div id="licenses_tab" class="tab_section tab00">';
		$content .= $licenses_top;
		$content .= '
			<div id="licenses_meat" class="adjust_body">
				<div id="license_response_message" style="display:none;"></div>
				<div id="show_l_form" style="display:none;color:blue;text-decoration:underline;cursor:pointer;margin-right:50px;"><br />Reload</div>
				<div id="licenses_form" >
				<form id="new_license" method="post" action="./exec/buy_license.php">
					<fieldset style="text-align:left;border:none;">
						<legend>'.$buy_license_title.'</legend>
						<p style="font:10px verdana;color:#717074;margin:0;">required fields*</p>
						<label for="license_license">License*</label>
						<div class="select_arrow_background">
						<select name="license_license" id="license_license" class="select_full" onchange="show_applicable_special_licenses(this.value);">
							'.$license_options.'
						</select>
						</div>
						<br />
						<div class="hide_from_ingram" style="'.$hide_from_ingram.'">
						<label for="card_number">Card Number*</label>
						<input type="text" id="card_number" name="card_number" />
						<br />
						<label for="license_card_exp_m">Card Expiration*</label>
						<span style="background:url(./images/sort_arrow.png) no-repeat right; display:inline-block; width:73px;margin-right:4px;">
						<select name="license_card_exp_m" id="license_card_exp_m" class="select_half">
							<OPTION VALUE="" SELECTED>Month
							<OPTION VALUE="01">Jan(01)
							<OPTION VALUE="02">Feb(02)
							<OPTION VALUE="03">Mar(03)
							<OPTION VALUE="04">Apr(04)
							<OPTION VALUE="05">May(05)
							<OPTION VALUE="06">June(06)
							<OPTION VALUE="07">July(07)
							<OPTION VALUE="08">Aug(08)
							<OPTION VALUE="09">Sep(09)
							<OPTION VALUE="10">Oct(10)
							<OPTION VALUE="11">Nov(11)
							<OPTION VALUE="12">Dec(12)
						</select>
						</span>
						<span style="background:url(./images/sort_arrow.png) no-repeat right; display:inline-block; width:73px;">
						<select name="license_card_exp_y" id="license_card_exp_y" class="select_half">
							 <OPTION VALUE="13">2013
							 <OPTION VALUE="14">2014
							 <OPTION VALUE="15">2015
							 <OPTION VALUE="16">2016
							 <OPTION VALUE="17">2017
							 <OPTION VALUE="18">2018
							 <OPTION VALUE="19">2019
							 <OPTION VALUE="20">2020
							 <OPTION VALUE="21">2021
							 <OPTION VALUE="22">2022
							 <OPTION VALUE="23">2023
							 <OPTION VALUE="24">2024
						</select>
						</span>
						<br />
						<label for="card_ccv">CCV*</label>
						<input type="text" id="card_ccv" name="card_ccv" />
						<br />
						</div>
						'.$s_special_licenses.'
						<div><input type="image" src="./images/btn_submit.png" id="submit_buy_new_license"style="padding: 20px 0px 0px 154px;" alt="btn_sub"/></div>
					</fieldset>
				</form>
				'.$license_display.'
				</div>
				<!--<div id="licenses_prices" style="'.$hide_css.'">-->
				<div id="licenses_prices" style="'.$hide_css.'">
				<p style="border-bottom:1px solid #d6d4d4;font:15px Verdana;color:#555555;padding:5px 4px;">LAVU LICENSES</p>
				<div id="license_points_form_div" style="margin:20px 0;border-bottom:1px solid #666;padding-bottom:15px;">
					<form id="license_points_form">
						<div>
							<input type="radio" class="license_pts_radio '.$silver_points.'" name="pts_license_selected" value="'.$a_base_highest_names['Silver'].'" DISABLED/>
							<img src="./images/seal_silver.png" alt="seal_s" />
							<span style="margin-right:10px;color:#7e7e73;letter-spacing:0.075em;position:relative;top:-9px;">'.$o_package_container->get_printed_name_by_attribute('name', $a_base_highest_names['Silver']).'</span>
							<span style="position:relative;top:-9px;">'.$license_values['Silver'].' Points</span>
						</div>

						<div>
							<input type="radio" class="license_pts_radio '.$gold_points.'"  name="pts_license_selected" value="'.$a_base_highest_names['Gold'].'" DISABLED/>
							<img src="./images/seal_gold.png" alt="seal_g"/>
							<span style="margin-right:10px;color:#7e7e73;letter-spacing:0.075em;position:relative;top:-9px;">'.$o_package_container->get_printed_name_by_attribute('name', $a_base_highest_names['Gold']).'</span>
							<span style="position:relative;top:-9px;">'.$license_values['Gold'].' Points</span>
						</div>

						<div>
							<input type="radio" class="license_pts_radio '.$plat_points.'" name="pts_license_selected" value="'.$a_base_highest_names['Platinum'].'" DISABLED/>
							<img src="./images/seal_plat.png" alt="seal_p" />
							<span style="margin-right:10px;color:#7e7e73;letter-spacing:0.075em;position:relative;top:-9px;">'.$o_package_container->get_printed_name_by_attribute('name', $a_base_highest_names['Platinum']).'</span>
							<span style="color:#38373;letter-spacing:0.05em;position:relative;top:-9px;">'.$license_values['Platinum'].' Points</span>
						</div>

						<input type="hidden" name="reseller_name" value="'.$reseller_name.'"/>
						<input type="hidden" name="reseller_id" value="'.$reseller_id.'"/>
						<input type="hidden" name="reseller_points" value="'.$points.'"/>

						<input type="submit" id="submit_pts_license" style="display:none;" />
						<span id="cancel_pts_license" style="padding:5px;display:none;margin-left:40px;width:20px;height:20px;border:1px solid #333;">CANCEL</span>
					</form>
				</div>
				<div id="buy_license_with_points_label">&nbsp;</div>
				<div>SELECT OPTION ABOVE TO APPLY POINTS TO LICENSE</div>
				</div>
				<br style="clear:left;">
			</div>

		</div>
		';//all_content;
		self::$tab_list .= $title;
		self::$tab_sections .= $content;

		/* new form thing.
		<form id="new_license">
						<fieldset>
							<legend>CREDIT CARD INFO</legend>
							<p style="font:10px verdana;color:#717074;margin:0;">required fields*</p>
							<label for="license_license">License*</label>
							<select type="text" name="license_license" id="license_license" class="new_lic_field" value="'.$my_info['f_name'].'" />
						</fieldset>
					</form>	*/
	}//add_licenses_tab
	private function add_my_info_tab(){
		//echo date('l').'<br />';
		//var_dump($days_of_week);
		//for($i = 0;$i<7;$i++){
		//	echo "dow:: days_of_week[".$i."] = ".$days_of_week[$i]
		//}
		
		function get_incentives($access){
			//echo $access.' mmm';
			$readHandle=fopen("./distro_news/incentives_test.txt", 'r');
			$contents = fread($readHandle, filesize("./distro_news/incentives_test.txt"));
			fclose($readHandle);
			
			$cont=explode("|||||", $contents);
			//$contents=$cont[1];
			$incentive = '';
			$count = 0;
			$edit_option = '';
			
			
			$newest_entry = explode("::*::", $cont[0]);
			if($access=='admin'){
				$incentive = '
				<div class="incentive_entry" id="incentive_entry'.$count.'" ><div class="edit_incentive_entry" entry="'.$count.'">CONFIRM CHANGES</div>
							<div style="height:70px;">
								<h3 class="incentive_entry_title incentive_entry_edit" id="incentive_entry_title'.$count.'" contenteditable="true" style="margin-bottom:10px;">'.$newest_entry[0].'</h3>
							</div>
							<div style="height:40px;">
							<div style="float:left;margin:0 50px;">
								<span class="incentive_start">START: </span><input type="text" class="incentive_entry_start incentive_entry_edit" id="incentive_entry_start'.$count.'" value="'.$newest_entry[1].'"/>
							</div>
							<div style="float:left;">
								<span class="incentive_end">END: </span><input type="text" class="incentive_entry_end incentive_entry_edit" id="incentive_entry_end'.$count.'" value="'.$newest_entry[2].'" />
							</div>
							</div>
						<div class="incentive_entry_body incentive_entry_edit" id="incentive_entry_body'.$count.'" contenteditable="true">'.$newest_entry[3].'</div>
					</div>';
			}else{
				
				
				$incentive = '
				<div class="incentive_entry" id="incentive_entry'.$count.'" >
							<div style="height:70px;">
								<h3 class="incentive_entry_title" id="incentive_entry_title'.$count.'" style="margin-bottom:10px;">'.$newest_entry[0].'</h3>
							</div>
							<div style="height:40px;">
							<div style="float:left;margin:0 50px;">
								<span class="incentive_start" >START: </span><span class="view_incentive_start" id="incentive_entry_start'.$count.'" style="font-weight:bold;color:red;">'.$newest_entry[1].'</span>
							</div>
							<div style="float:left;">
								<span class="incentive_end">END: </span><span class="view_incentive_end" id="incentive_entry_end'.$count.'" style="font-weight:bold;color:red;">'.$newest_entry[2].'</span>
							</div>
							</div>
						<div class="incentive_entry_body" id="incentive_entry_body'.$count.'" >'.$newest_entry[3].'</div>
					</div>';
				
			}
					
			
			return $incentive;
			
		}//get_incentives($access)
		
		$days_of_week = self::next_seven_days_of_week(date('l'));
		$access = self::$current_user->get_info_attr("access");
		//echo $access.':: poo<br />';
		$my_info = self::$current_user->get_info();
		$reseller_name = $my_info['username'];
		$s_accepts_mercury = self::$current_user->get_accepts_mercury() ? 'Yes' : 'No';
		$s_accepts_mercury_checked = self::$current_user->get_accepts_mercury() ? ' checked' : '';
		$s_availability = self::$current_user->get_availability();
		
		if ($_SERVER['REMOTE_ADDR'] == '74.95.18.90') {
			$s_registered_devices = self::$current_user->draw_allowed_admin_devices();
		}
		//var_dump($s_availability).'<br />';
		$certified = $my_info['isCertified']?" Certified":" Not Certified";
		// add content to tab_list
		$title ='<li class="tab" style="width:120px;"><a href="#my_info_tab">SALES TOOLS</a></li>
		';
	
		// add content to tab_sections 	
		$content ='<div id="my_info_tab" class="tab_section tab00">
		';
		/*<div id="licenses_tab_top" class="tab_top">
				<div class="tab_top_top">
					<span style="font:24px Verdana;color:#717074;">BUY NEW LICENSE</span>
					<span style="color:#B7BDB0;margin-left:388px;font-size:12px;">LAVU POINTS:</span>
					<span >'.$lavu_points_display.$licenses_top_edit_points.'</span>
				</div>
				<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="d_w_f"/></div>
			</div>';//end licenses_tab_top*/
		if($my_info['credits'] != ''){
			$lavu_points_display = $my_info['credits'];
		}else{
			$lavu_points_display = 'N/A';
		}
		//echo '--'.self::is_admin().'--';
		$licenses_top_edit_points = self::view_point_breakdown(self::is_admin(), $reseller_name);
		$hide_points  = '';
		if($access=='ingram'){
			$hide_points = 'display:none;';
		}
		$content  .= '
		<div id="my_info_tab_top" class="tab_top">
			<div class="tab_top_top">
				<span style="font:24px Verdana;color:#717074;">SALES TOOLS</span>
					<span style="color:#B7BDB0;margin-left:388px;font-size:12px;'.$hide_points.'">LAVU POINTS:</span>
					<span style='.$hide_points.'>'.$lavu_points_display.$licenses_top_edit_points.'</span>
			</div>
			<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="d_w_f"/></div>
		</div>
		';//end very top		
		
		if($access == 'ingram'){
			$content .= '<div class="adjust_body">
			<div id="my_info_no_form">
				<p style="font:13px Verdana;color#555;">
					
			    </p>
		    </div>
		    ';//my_info above form
		}else{
			$content .= '<div class="adjust_body">
			<div id="my_info_no_form">
				<p style="font:13px Verdana;color#555;">
					<span style="margin-right:60px;">Commission: '.get_distro_commision($my_info).'%</span>
					<span>Certification:'.$certified.'</span>
			    </p>
		    </div>
		    ';//my_info above form
		}
		
		$edit_icon = '<span>Edit</span><img src="./images/icon_edit.png" alt="icon_edit" />';
		$done_icon = '<span>Done</span><img src="./images/check_green.png" alt="check_green" />';
		
		//<label for="pCurrent">Current Password:</label>
		//		<input type="password" name="pCurrent" id="pCurrent" class="p_field" />
		//		<br />
		//Form that holds all...
		$content .= '
		<div style="float:left;width:650px;">
			<div class="my_sales_title">Incentives</div>
			<div id="my_sales_incentives" style="text-align:left;">
				<div>
				'.get_incentives($access).'
				</div>	
			</div>
			<div>===================</div>
			<div id="sales_tools_quick_links">
				<div class="my_sales_title">Sales Tools Quick Links</div>
				<div><span><img src="./images/btn_circ_help.png" class="circ_help" alt="icon_help" /></span><a href="./sales_tools_quick_links/CompetitionFeatures.pdf" target="_blank">POS Comparison Charts</a></div>
				<div><span><img src="./images/btn_circ_help.png" class="circ_help" alt="icon_help" /></span><a href="./sales_tools_quick_links/DefeatList.pdf" target="_blank">Defeat List</a></div>
			</div>
		</div>
		<div style="float:left;width:300px;">
		<form id="my_info_form" name="my_info_form" method="post" action="" enctype="multipart/form-data">
		<div class="vertical_fieldsets" style="float:left;width:280px;">
			<fieldset class="my_info_fieldset" id="my_info_fset_personal">
				<legend>Personal<span style="float:right;color:#aecd37;cursor:pointer;" id="p_edit_icon">'.$edit_icon.'</span>
				<span style="float:right;color:#aecd37;cursor:pointer;" id="p_done_icon">'.$done_icon.'</span>
				</legend>
				
				<div id="p_edit">
				<label for="pFirstName">First Name:</label>
				<input type="text" name="pFirstName" id="pFirstName" class="p_field" value="'.$my_info['f_name'].'" />
				<br />
				<label for="pLastName">Last Name:</label>
				<input type="text" name="pLastName" id="pLastName" class="p_field" value="'.$my_info['l_name'].'" />
				<br />
				<div><span class="view_label">Username:</span><span id="pUsername" name="pUsername">'.$my_info['username'].'</div>
				<br />
				<label for="pCurrent">Current Password:</label>
				<input type="password" name="pCurrent" id="pCurrent" class="p_field" />
				<br />
				<label for="pPassword">Update Password:</label>
				<input type="password" name="pPassword" id="pPassword" class="p_field" />
				<br />
				<label for="pConfirm">Confirm:</label>
				<input type="password" name="pConfirm" id="pConfirm" class="p_field" />
				<br />
				<input type="submit" value="" id="submit_personal" name="submit_personal" class="my_info_submit"/>
				<input type="hidden" name="p_current_username" class="p_field" value="'.$my_info['username'].'" />
				<input type="hidden" name="p_current_first" class="p_field" value="'.$my_info['f_name'].'" />
				<input type="hidden" name="p_current_last" class="p_field" value="'.$my_info['l_name'].'" />
				<input type="hidden" name="p_changed" id="p_changed" class="p_field" value="nochange" />
				<input type="hidden" name="p_identify" id="p_identify" class="p_field" value="p_identify" />				
				</div>
				
				<div id="p_view">
				<div><span class="view_label">First Name:</span><span id="p_view_f_name">'.$my_info['f_name'].'</span></div>
				<div><span class="view_label">Last Name:</span><span id="p_view_l_name">'.$my_info['l_name'].'</div>
				<div><span class="view_label">Username:</span><span id="p_view_username">'.$my_info['username'].'</div>
				<div>Update Password:</div>
				<div>Confirm:</div>
				<div id="p_field_submit_message"></div>
				</div>															
  			</fieldset>
  			<fieldset class="my_info_fieldset" id="my_info_fset_avail" style="float:left;">
  			<legend>Availability<span style="float:right;color:#aecd37;cursor:pointer;" id="a_edit_icon">'.$edit_icon.'</span>
				<span style="float:right;color:#aecd37;cursor:pointer;" id="a_done_icon">'.$done_icon.'</span>
				</legend>
				
				<div id="a_edit">';
				for($j = 0;$j<7;$j++){
					$content .= '<label for="a'.$days_of_week[$j].'">'.$days_of_week['d'.$j].' '.$days_of_week[$j].'</label>';
				/*<input type="text" name="a'.$days_of_week[$j].'" id="a'.$days_of_week[$j].'" class="a_field" value="9am-5pm '.$days_of_week[$j].'" />*/
					$content .= '<textarea name="a'.$days_of_week[$j].'" id="a'.$days_of_week[$j].'" class="a_field" placeholder="'.$s_availability[strtolower($days_of_week[$j])].'" style="width:242px;height:28px;">'.$s_availability[strtolower($days_of_week[$j])].'</textarea>
				<br />';
				}
				
				$content .= '<input type="submit" value="" id="submit_availability" name="submit_availability" class="my_info_submit"/>';
				for($j = 0;$j<7;$j++){
					$content .= '<input type="hidden" name="a_current_'.$days_of_week[$j].'" class="a_field" value="" />';
				}
				
				
				$content .= '<input type="hidden" name="a_changed" id="a_changed" class="a_field" value="nochange" />
				<input type="hidden" name="a_identify" id="a_identify" class="a_field" value="a_identify" />				
				</div>
				
				<div id="a_view">';
				for($j = 0;$j<7;$j++){
				//echo strtolower($days_of_week[$j]).'<br />';
					$content .= '<div style="margin-bottom:10px;"><span class="view_label">'.$days_of_week['d'.$j].' '.$days_of_week[$j].':</span><span id="a_view_'.strtolower($days_of_week[$j]).'">'.$s_availability[strtolower($days_of_week[$j])]. '</span></div>';
				}
				
				
				$content .= '<div id="a_field_submit_message"></div>
				</div>
  			</fieldset>
  		</div>
			<fieldset class="my_info_fieldset" id="my_info_fset_company">
				<legend>Company<span style="float:right;color:#aecd37;cursor:pointer;" id="c_edit_icon">'.$edit_icon.'</span>
				<span style="float:right;color:#aecd37;cursor:pointer;" id="c_done_icon">'.$done_icon.'</span>
				</legend>
				<div id="c_edit">
				<label for="cName">Name:</label>
				<input type="text" name="cName" id="cName" value="'.$my_info['company'].'" />
				<br />
				<label for="cAddress">Address:</label>
				<input type="text" name="cAddress" id="cAddress" value="'.$my_info['address'].'" />
				<br />
				<label for="cCity">City:</label>
				<input type="text" name="cCity" id="cCity" value="'.$my_info['city'].'" />
				<br />
				<label for="cState">State:</label>
				<input type="text" name="cState" id="cState" value="'.$my_info['state'].'" />
				<br />
				<label for="cPostalCode">Postal Code:</label>
				<input type="text" name="cPostalCode" id="cPostalCode" value="'.$my_info['postal_code'].'" />
				<br />
				<label for="cCountry">Country:</label>
				<input type="text" name="cCountry" id="cCountry" value="'.$my_info['country'].'" />
				<br />
				<label for="cPhone">Phone:</label>
				<input type="text" name="cPhone" id="cPhone" value="'.$my_info['phone'].'" />
				<br />
				<label for="cEmail">Email:</label>
				<input type="text" name="cEmail" id="cEmail" value="'.$my_info['email'].'" />
				<br />
				<label for="cWebsite">Website:</label>
				<input type="text" name="cWebsite" id="cWebsite"  value="'.$my_info['website'].'"/>
				<br />
				<label for="cAcceptsMercery">Mercury:</label>
				<input type="checkbox" name="cAcceptsMercury" id="cAcceptsMercury" value="'.$s_accepts_mercury_checked.'" onclick="checkbox_checked_update_element($(this),$(\'#c_view_accept_mercury\'),{true:\'Yes\',false:\'No\'});" '.$s_accepts_mercury_checked.'/>
				<br />
				<input type="submit" id="submit_company" name="submit_company" value="" class="my_info_submit"/>
				
				</div>
				
				<div id="c_view">
				<div><span class="view_label">Name:</span><span id="c_view_comp">'.$my_info['company'].'</span></div>
				<div><span class="view_label">Address:</span><span id="c_view_add">'.$my_info['address'].'</span></div>
				<div><span class="view_label">City:</span><span id="c_view_city">'.$my_info['city'].'</span></div>
				<div><span class="view_label">State:</span><span id="c_view_state">'.$my_info['state'].'</span></div>
				<div><span class="view_label">Postal Code:</span><span id="c_view_zip">'.$my_info['postal_code'].'</span></div>
				<div><span class="view_label">Country:</span><span id="c_view_country">'.$my_info['country'].'</span></div>
				<div><span class="view_label">Phone:</span><span id="c_view_phone">'.$my_info['phone'].'</span></div>
				<div><span class="view_label">Email:</span><span id="c_view_email">'.$my_info['email'].'</span></div>
				<div><span class="view_label">Website:</span><span id="c_view_web">'.$my_info['website'].'</span></div>
				<div><span class="view_label">Mercury: <a class="fancybox" id="mercury_description_help" rel="group" href="/cp/areas/table_setup_files/png/white.png"><img src="/cp/areas/table_setup_files/png/plus.png" al="+"></a></span><span id="c_view_accept_mercury">'.$s_accepts_mercury.'</span></div>
				<div style="display:none" id="mercury_description_help_displaytext">
					What is the Mercury Accept List?<br />
					<br />
					Mercury Payments has sponsored a PPC campaign which directs prospects to try.poslavu.com.<br />
					Leads generated through try.poslavu.com require a Mercury payment processing account. <br />
					Distributors on the Mercury Accept List will receive leads generated from this campaign.<br />
					These accounts are standard in all other ways including monthly residual Lavu Points.<br />
					<br />
					To join the Mercury Accept List, click (( EDIT )) and then check the Mercury checkbox.<br />
				</div>
				<div id="c_field_submit_message"></div>						
				</div>															
			</fieldset>';
			//registered device thingies.
			if ($_SERVER['REMOTE_ADDR'] == '74.95.18.90'){
			$content .= '
			<div class="edit_account_info registered_devices" style="padding:19px;">
				<legend>Registered Devices
					<span class="edit_icon">'.$edit_icon.'</span>
					<span class="done_icon">'.$done_icon.'</span>
				</legend>
				<input type="hidden" class="draw_info_form_submit_url" name="" value="/distro/beta/exec/distro_devices.php?action=register_devices" />
				<input type="hidden" class="draw_info_form_name" name="" value="registered_devices" />
				<fieldset class="my_info_fieldset draw_info_form_noedit" style="margin:5px 0 0 0; padding:0;">
					'.$s_registered_devices.'
					<font>
						<input type="hidden" name="attributes" value="{--class--:--submit--}" />
						<input type="hidden" name="type" value="button" />
						<input type="hidden" name="index" value="1000" />
						<font style="display:none;" class="value">Submit</font>
					</font>
				</fieldset>
				<fieldset class="my_info_fieldset draw_info_form_edit" style="margin:5px 0 0 0; padding:0; display:none;">
				</fieldset>
				<script type="text/javascript">
					$(document).ready(function() { setTimeout(function() {
						Lavu.Form.draw_info_form($(".edit_account_info.registered_devices"));
					}, 300); });
				</script>
			</div>';
			}
			$content .= '
			<div id="my_info_right">
				<fieldset class="my_info_fieldset" id="my_info_fset_logo">
					<legend>Current Logo<span style="float:right;color:#aecd37;cursor:pointer;" id="l_edit_icon">'.$edit_icon.'</span>
					<span style="float:right;color:#aecd37;cursor:pointer;" id="l_done_icon">'.$done_icon.'</span>
					</legend>';
					$logo_img = "no image uploaded";
					if(file_exists('../images/logos/fullsize/'.$my_info['username'].'.jpg')){
						$logo_img = '<img id="curr_logo_preview" src="../images/logos/fullsize/'.$my_info['username'].'.jpg" style="width:50px;" alt="logo" />';
					}else if(file_exists('../images/logos/fullsize/'.$my_info['username'].'.png')){
						$logo_img = '<img id="curr_logo_preview" src="../images/logos/fullsize/'.$my_info['username'].'.png" style="width:50px;" alt="logo" />';
					}
					$content .= $logo_img;
					//<input type="submit" value="" id="submit_logo" name="submit_logo" class="my_info_submit"/>
				$content .= '
					<div id="l_field_submit_message"></div>
					<div id="l_edit">
						<label for="lfile">Filename:</label>
						<input type="file" name="lfile" id="lfile"/>
						<input type="hidden" name="username" id="logo_username" value= '.$my_info['username'].'>
						<br/>
						<div id="logo_preview"></div>
						<div id="image_uploaded_reload" style="background-color:pink;border:1px solid black;display:none;cursor:pointer;margin-left:20px;margin-top:10px;padding:10px;">Click to reload and see results</div>
					</div>		
				</fieldset>
				<fieldset class="my_info_fieldset" id="my_info_fset_additional">
					<legend>Additional Info<span style="float:right;color:#aecd37;cursor:pointer;" id="add_edit_icon">'.$edit_icon.'</span>
						<span style="float:right;color:#aecd37;cursor:pointer;" id="add_done_icon">'.$done_icon.'</span>
					</legend>
					<div id="add_edit">
						<textarea cols="30" rows="6" id="add_edit_textarea" name="add_edit_textarea">'.$my_info['notes'].'</textarea>
						<input type="submit" value="" id="submit_notes" name="submit_notes" class="my_info_submit"/>
					</div>
					<div id="add_view">
					'.$my_info['notes'].'
					<div id="add_field_submit_message"></div>
					</div>				
				</fieldset>
			</div>
			</form>
			</div>
			<br style="clear:left;">
		</div></div>';//end of form
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_my_info_tab

	//
	//
	//////ADMIN SPECIFIC TABS
	private function add_unassigned_tab($resellers){
		
		global $o_unassignedTab;
		$a_retval = $o_unassignedTab->tabToStr(self::$current_user, $resellers);
		self::$tab_list .= $a_retval['title'];
		self::$tab_sections .= $a_retval['content'];
		return ;

		self::$current_user->set_all_unassigned();
		$unacc = self::$current_user->get_all_unassigned_leads();
		$in_progress = self::$current_user->get_all_in_progress();
		$processed = self::$current_user->get_all_processed();
		$current_access = self::$current_user->get_info_attr('access');
		//echo 'current_access===='.$current_access.'<br />';
		//var_dump($processed);
		//var_dump($unacc);
		//var_dump($resellers);
		$j_decode = new Json();
		//count unaccepted leads
		$all_count = count($unacc);
		//echo $all_count;
		
		//count unassigned leads
		$unassigned = array();
		$merc_unass_count = 0;
		//$is_mercury = false;
		 
		foreach($unacc as $key=>$value){
			//echo 'assigned to :: '.$value['distro_code'].'<br />';
			//echo $value['_declined'].'<br />';
			if($value['distro_code'] == '' || $value['_declined']){
			//var_dump($value);
				//echo "found unassigned::".$value['id'].", Declined::".$value['_declined']."<br />";
				$unass_id = $value['id'];
				foreach($value as $att=>$val){
					$unassigned[$unass_id][$att] = $val;
					//echo "unassigned[".$unass_id."][".$att."]=".$val."<br />";
				}
				/*if($current_access == 'mercury' && $value['lead_source'] == 'mercury'){
					$merc_unass_count++;
				}*/
			}
		}
		$unass_count = count($unassigned);
		//echo "total unassigned::".$unass_count.", total unaccepted:: ".$all_count;
		
		
		function lead_color_u($stat, $div,$color_class){
		//echo 'stat='.$stat.' div='.$div.' cc='.$color_class;
			if($stat === $div){
				//echo '  true';
				return $color_class;
			}
		}
		
		
		//different display for different distro levels
		if($current_access == 'mercury'){
			$title ='<li class="tab"><a href="#unacc_tab">UNASSIGNED</a></li>
		';
		}else{
		$title ='<li class="tab"><a href="#unacc_tab">
			<span style="background-image:url(./images/circ_leads.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$unass_count.'
			</span>
			UNASSIGNED</a></li>
		';
		}
		$content ='<div id="unacc_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">UNACCEPTED LEADS</div>
					';
		if($current_access == 'admin'){
		$content .= '<div id="leads_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;padding-top:25px;">
					<img src="./images/btn_create_lead.png" alt="btn_apply" class="new_lead_btn"/></span>
					</div>
					';
		}
		$content .= '</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
		$content .= '<div class="adjust_body">';
		
		$merc_legend = '';
		if($current_access == 'admin'){
			$merc_legend = '<div style="height:30px;width:950px;margin:0 auto 20px;"><img src="./images/lead_legend_sponsor.png" alt="mlegends"></div>';
		}else{
			$merc_legend = '<div style="height:30px;width:950px;margin:0 auto 20px;"><img src="./images/lead_legend_sponsor_for_mercurysite.png" alt="mlegendm"></div>';
		}
		//if($current_access == 'admin'){
		$content .= '<div id="unacc_lead_desc">
		    <div style="height:30px;width:950px;margin: 0 auto;"><img src="./images/lead_legend.png" alt="llegend"></div>
		    '.$merc_legend.'
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:160px;">LOCATION</span>
			<span style="width:156px;">EMAIL</span>
			<span style="width:113px;">NAME</span>
			<span style="width:110px;">PHONE</span>
			<span style="width:80px;">DECLINED BY</span>
			<span style="width:80px;">ASSIGN</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>
		<div id="unassigned_lead_acc_wrapper">
		';
		//}
		
		//$content .= '<div id="unassigned_lead_acc_wrapper">';
		foreach($unassigned as $key=>$attrs){
			$lead_stat = 'accept';
			$lead_source = '';
			//echo "herro?<br />";
			$accepts_mercury = $j_decode->decode($resellers[$attrs['distro_code']]['special_json']);
			$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
			
			if($attrs['lead_source'] == 'mercury'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}else if($attrs['lead_source'] == 'mercury_bleed'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini_bleed.png" alt="mm"/></span>';
			}
			//echo $lead_stat.'==><br/> ';
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_accept';
			if($attrs['contacted'] != '0000-00-00 00:00:00') $circ_image_ref = 'circ_contact';
			if($attrs['made_demo_account'] != '0000-00-00 00:00:00') $made_demo_account =  TRUE; else $made_demo_account = FALSE;
			if ($made_demo_account) $circ_image_ref .= '_has_demo_account';
			if($attrs['declined_by'] != ''){
				$circ_image_ref = 'circ_black';
			} 
			if($attrs['created'] != '0000-00-00 00:00:00'){
				$birth = $attrs['created'];
				$age = time_from($birth);
				
				//echo $attrs['company_name'].', '.$birth.'--'.$age.'<br />';
				
			}
			//echo 'before_access = '.$current_access;
			if($current_access == 'admin'){
	    //echo 'eat iitttt<br />';
			$content .= '<div class="acc_head accordionButton">
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:150px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;" class="hover_flow">'.$attrs['email'].'</span><div class="loc_head_div">|</div>
				<span style="width:95px;" class="hover_flow">'.$attrs['firstname'].' '.$attrs['lastname'].'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" class="hover_flow">'.$attrs['phone'].'</span><div class="loc_head_div">|</div>
				<span style="width:70px;" class="hover_flow">'.$attrs['declined_by'].'</span><div class="loc_head_div">|</div>
				<span style="width:60px;" class="hover_flow"><img src="./images/btn_assign.png" alt="assign" style="height:23px;" class="assign_lead"/></span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.'
				<br class="clear:left;">
				</div>		
				';
				//if($current_access == 'mercury')
			}else {
				//echo 'mercury access='.$attrs['lead_source'].', '.$attrs['company_name'].', '.$attrs['distro_code'].'<br />';
				if($current_access == 'mercury' && $merc_dist && $attrs['lead_source'] != 'distro portal'){
				//echo 'mercury access2<br/ >';
					$content .= '<div class="acc_head accordionButton">
						<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
						<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
						<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
						<span style="width:150px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
						<span style="width:150px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
						<span style="width:70px;" class="hover_flow">'.$attrs['declined_by'].'</span><div class="loc_head_div">|</div>
						<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>							'.$lead_source.' 
						<br class="clear:left;">
				</div>		
				';
				}
			}
			$content .= '<div class="acc_lead_content accordionContent">
				<div class="lead_acc_content_head"><img src="./images/prog_bar_full.png" alt="p_b_f"/></div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color_u($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>
				
			</div>';
			
				
		}//for each unassigned
		 
		 //in progress parts
		 
		//all in progress part
		//if($current_access == 'admin'){
			$content .= '</div>';
		//}
		$content .= '<div style="margin:20px;text-align:center;font:30px verdana;color:#439789;">ALL IN-PROGRESS LEADS</div>';
		if($current_access == 'admin'){
		$content .='
		<div id="in_prog_lead_desc">
			<span style="width:50px;cursor:pointer;" class="lead_sort_by" sort_by="status">STATUS</span>
			<span style="width:115px;" class="lead_sort_by" sort_by="b_name">BUSINESS NAME</span>
			<span style="width:120px;">TOTAL AGE</span>
			<span style="width:100px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:105px;">REMIND</span>
			<span style="width:105px;">REMOVE</span>
			<span style="width:80px;">RESELLER</span>
			<span style="width:80px;">ASSIGN</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>';
		}
		if($current_access == 'mercury'){
		$content .='<div id="in_prog_lead_desc">
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:120px;">TOTAL AGE</span>
			<span style="width:156px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:150px;">RESELLER</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>';
		 }
		//foreach($in_progress as $key=>$attrs)
		$no_archive = true;//indicates lead is not yet to be archived
		$content .= "<div class='in_progress_wrapper'>";
		foreach($in_progress as $key=>$attrs){
			//$lead_stat = 'accept';
			$lead_source = '';
			$lead_source = self::display_merc_logo($attrs, $resellers, $current_access, $j_decode);
			$can_expand = '<span style="width:20px;"></span>';
			/*if($attrs['lead_source'] == 'mercury'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}else if($attrs['lead_source'] == 'mercury_bleed'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini_bleed.png" alt="mm"/></span>';
			}*/
			$accepts_mercury = $j_decode->decode($resellers[$attrs['distro_code']]['special_json']);
			$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
			
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_accept';
			$sort_val = '1';
			if(preg_replace('/[1-9]/', '', $attrs['contacted']) != '0000-00-00 00:00:00') {
				if(preg_replace('/[1-9]/', '', $attrs['made_demo_account']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$sort_val = '4';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}else{
					$circ_image_ref = 'circ_demo_account';
					$lead_stat = 'demo_account';
					$sort_val = '3';
	
				}
			}else if(preg_replace('/[1-9]/', '', $attrs['accepted']) != '0000-00-00 00:00:00'){
				if(preg_replace('/[1-9]/', '', $attrs['made_demo_account']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$sort_val = '4';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
					 
				}else{
					$circ_image_ref = 'circ_contact';
					$lead_stat = 'contact';
					$sort_val = '2';
				}
			}else if(preg_replace('/[1-9]/', '', $attrs['made_demo_account'])!= '0000-00-00 00:00:00'){
				if(preg_replace('/[1-9]/', '', $attrs['accepted']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$sort_val = '4';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}else{
					$circ_image_ref = 'circ_accept_has_demo_account';
					$lead_stat = 'accept';
					$sort_val = '1';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}
			}else{
				$circ_image_ref = 'circ_accept';
				$sort_val = '1';
			}
			
			$time_values = get_lead_times($attrs);
			//self::$current_user->set_lead_attr($attrs['data_name'], 'signup_step', $time_values['signups']);
			$total_age = $time_values['first'];
			$total_exp = explode(" ", $total_age);
			$number_age = intval($total_exp[0]);
			$follow_up = '';
			$follow_up_span = '';
			$never_accepted = '';
			
			if($attrs['accepted'] == '0000-00-00 00:00:00' && (intval($time_values['assigned_hours']) > 12) && $attrs['need_follow_up']){
				//echo 'unassign:: '.$attrs['company_name'].'<br />';
				//echo 'follow up:: '.$attrs['company_name'].', hrs:: '.intval($time_values['assigned_hours']).'<br />';
				$follow_up = 'need_follow_up12';
				if(intval($time_values['assigned_hours']) > 24){
					$follow_up = 'need_follow_up24';					
				}
				if(intval($time_values['assigned_hours']) > 48){
					$follow_up = 'need_follow_up48';
				}
				//echo 'follow up:: '.$attrs['company_name'].', hrs:: '.intval($time_values['assigned_hours']).'<br />';
				//$follow_up_span = '<span class="unassign" id="followup'.$attrs['id'].'" lead_id="'.$attrs['id'].'">UNASSIGN</span>';
			};
			$follow_up_span = '<span class="unassign" id="followup'.$attrs['id'].'" lead_id="'.$attrs['id'].'">UNASSIGN</span>';
			//echo $assigned_time = '---assigned:: '.$time_values['assigned'].'<br />';
			
			/*$age = time_from($attrs['created']);
			$age_array = explode(' ', $age);
				$age_num = (int)$age_array[0];
				$age_unit = $age_array[1];
			if($attrs['accepted'] == '0000-00-00 00:00:00' && $attrs['assigned_time'] != '0000-00-00 00:00:00'){
					echo $attrs['created'].', age=::'.$age.', '.$attrs['company_name'].', '.$age_num.', '.$age_unit.'<br />';
			}*/
			
			$archive_class_head = '';
			$archive_class_body = '';
			if($no_archive && $number_age > 40){
				//echo 'start archive at:: '.$total_exp[0].','.$number_age.'<br />';
				//$no_archive = false;
				$archive_class_head = 'to_archive_head';
				$archive_class_body = 'to_archive_body';
				
			}
			$last_step_time = $time_values['most_recent'];
			$assigned_time = $time_values['assigned'];
				
			$last_step_time = str_replace("-", "", $last_step_time);
			$total_age = str_replace("-", "", $total_age);
			
			$distro_username = $attrs['distro_code'];
			$distro_phone = $resellers[$distro_username]['phone'];
			$distro_email = $resellers[$distro_username]['email'];
			$distro_company = $resellers[$distro_username]['company'];
			$distro_name = $resellers[$distro_username]['f_name'].' '.$resellers[$distro_username]['l_name'];
			$tooltip_id = $attrs['id'].'_'.$distro_username.'_content';
			
			$distro_tooltip = '
			<div id="'.$tooltip_id.'" style="display:none;">
				<div><strong>Name: </strong>'.$distro_name.'</div>
				<div><strong>Company: </strong>'.$distro_company.'</div>
				<div><strong>Phone: </strong>'.$distro_phone.'</div>
				<div><strong>Email: </strong>'.$distro_email.'</div>
			</div>';//$distro_tooltip
			
			/*$from_mercury = false;
			$to_mercury = '';
			$j_decode = new Json();
				$accepts_mercury = $j_decode->decode($resellers[$distro_username]['special_json']);
				$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
				//echo $merc_dist.'<br />';
				if($merc_dist){
					$from_mercury = true;
					$to_mercury = '<span style="margin-left:5px;"><img src="./images/i_mercdistro_lavulead_mini.png" alt="from_merc"></span>';
					//$merc_logo='<img src="./images/i_merc_mini.png" alt="mm" />';
					//echo $merc_dist.'||  '.$distro_username.'<br />';
				}*/
			
			
			if($current_access == 'admin'){
			$attrs_for_jab = 'b_name="'.$attrs['company_name'].'" email="'.$attrs['email'].'" phone="'.$attrs['phone'].'" contact="'.$attrs['firstname'].' '.$attrs['lastname'].'" reseller="'.$attrs['distro_code'].'"';
			$content .= '<div class="acc_head accordionButton in_prog_head '.$follow_up.' '.$archive_class_head.'" id="un_lead_head_'.$attrs['id'].'" status="'.$sort_val.'">
				'.$distro_tooltip.'
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:140px;" class="lead_on_comp_name">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" class="in_progress_total_age">'.$total_age.'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" >'.$last_step_time.'</span><div class="loc_head_div">|</div>
				<span style="width:100px;">'.$assigned_time.'</span><div class="loc_head_div">|</div>
				<span style="width:50px;text-align:center;cursor:pointer;" l_id="'.$attrs['id'].'" class="remove_lead"><img src="./images/btn_decline.png" alt="big_X" /></span><div class="loc_head_div">|</div>
				<span style="width:50px;text-align:center;" class="jab_distro" '.$attrs_for_jab.'><img src="./images/jab.png" alt="jab" /></span><div class="loc_head_div">|</div>
				<span style="width:110px;" class="distro_tip" tipid="#'.$tooltip_id.'">'.$attrs['distro_code'].'</span><div class="loc_head_div">|</div>
				<span style="width:60px;"><img src="./images/btn_assign.png" alt="assign" style="height:23px;" class="assign_lead"/></span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.$follow_up_span.'
				<br class="clear:left;">
				</div>		
				';
			}else if($current_access == 'mercury'){
				if($merc_dist && $attrs['lead_source'] != 'distro portal'){
				
			
			$content .= '<div class="acc_head accordionButton">
				'.$distro_tooltip.'
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:140px;" class="lead_on_comp_name">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;">'.$total_age.'</span><div class="loc_head_div">|</div>
				<span style="width:140px;" >'.$last_step_time.'</span><div class="loc_head_div">|</div>
				<span style="width:110px;">'.$assigned_time.'</span><div class="loc_head_div">|</div>
				
				<span style="width:130px;" class="distro_tip" tipid="#'.$tooltip_id.'">'.$attrs['distro_code'].'</span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.'
				<br class="clear:left;">
				</div>
				';
				}//if merc_dist and source != 'd p'
			}//if current_access=='mercury'
			$content .= '<div class="acc_lead_content accordionContent in_prog_content '.$archive_class_body.'" id="un_lead_content_'.$attrs['id'].'">
				<div class="lead_acc_content_head"><img src="./images/prog_bar_full.png" alt="p_b_f"/></div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color_u($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>
				<div style="margin-bottom:20px;">Lead Details:</div>
				<div class="in_progress_details" style="height:200px;">
					<div style="width:400px;float:left;text-align:left;margin:20px 5px 20px 50px;font:12px verdana;line-height:175%;">
						<div><span class="lead_detail_title">Location:</span><span>'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span></div>
						<div><span class="lead_detail_title">Name:</span><span>'.$attrs['firstname'].' '.$attrs['lastname'].'</span></div>
						<div><span class="lead_detail_title">Email:</span><span>'.$attrs['email'].'</span></div>
						<div><span class="lead_detail_title">Phone:</span><span>'.$attrs['phone'].'</span></div>
					</div>
					<div style="width:450px;float:left;font:12px verdana;line-height;175%;text-align:left;">
						<div><span style="font-weight:bold;color:black;">Contacted info:</span><span>'.$attrs['contact_note'].'</span></div>
					</div>
				</div>
				
			</div>';
		}//foreach($in_progress as $key=>$value)
		 
		$content .= '</div></div></div>';
		/*lead_type_content($lead_stat, $attrs['data_name']?$attrs['data_name']:$attrs['company_name']).'</div>*/
		
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//unassigned_tab
	private function add_resellers_tab($resellers){
		//self::$current_user->set_all_resellers();
		//$resellers = self::$current_user->get_all_resellers();
		//echo 'reseller_count:: '.count($resellers).'<br />';
		/*foreach($resellers as $key=>$value){
			echo $key.'<br />';
		}*/
		global $o_specialistTab;
		$a_retval = $o_specialistTab->tabToStr(self::$current_user);
		self::$tab_list .= $a_retval['title'];
		self::$tab_sections .= $a_retval['content'];
		return ;

		$title ='<li class="tab"><a href="#resellers_tab">RESELLERS</a></li>
		';
		
		$content ='<div id="resellers_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;cursor:pointer;">RESELLERS</div>
					<div style="border:1px solid red;float:right;text-align:center;width:200px;font:20px verdana;display:none;margin-top:22px;background:#aecd37;cursor:pointer;" id="submit_changes">SUBMIT CHANGES</div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
		$content .= '<div class="adjust_body"><div id="unacc_lead_desc">
			<span style="width:200px;">NAME</span>
			<span style="width:200px;">COMPANY</span>
			<span style="width:154px;">CITY-STATE</span>
			<span style="width:190px;">EMAIL</span>
			<span style="width:65px;">LOGIN</span>
			<span style="width:50px;">LEVEL</span>
			<span style="width:50px;">REMOVE</span>
			<br style="clear:left;" />													
		</div>
		';
		
		$content .= '
		<div id="account_acc_wrapper">';
		
		
		//get information for reseller accordians.
		
		
		foreach($resellers as $key=>$value){
			//echo 'info<br/>'.$resellers[$key]['f_name'].' '.$resellers[$key]['l_name'].', '.$resellers[$key]['username'].'<br />';
		//build head
			$username = $resellers[$key]['username'];
			//<img class="acc_arrow expand_account_btn" src="./images/arrow_left.png" style="float:left;" alt="a_l"/>\
			if($resellers[$key]['access'] == 'admin'){
				$level = 'ADMIN';
			}else if($resellers[$key]['access']=='' || $resellers[$key]['access']=='deactivated'){
				$level = '<img src="./images/stars_0.png" alt="not_set" />';
			}
			else if($resellers[$key]['access']=='agree'){
				$level = '<span style="color:red;">AGREE</span>';
			}else{
				$level = '<img src="./images/stars_'.$resellers[$key]['access'].'.png" alt="not_set" />';
			}
			$content .= '<div class="acc_head accordionButton">				
				<span style="width:180px;">'.$resellers[$key]['f_name'].' '.$resellers[$key]['l_name'].'</span>
				<span style="width:200px;">'.$resellers[$key]['company'].'</span>
				<span style="width:140px;">'.$resellers[$key]['city'].'-'.$resellers[$key]['state'].'</span>
				<span style="width:190px;">'.$resellers[$key]['email'].'</span>
				<span style="width:100px;text-align:center;"><a href="./view_reseller.php?function=login_reseller&username='.$username.'" target="_blank">'.$username.'</a></span>
				<span style="width:60px;color:#aecd37;"class="d_level_stars" val="span_d_level_'.$resellers[$key]['username'].'" id="form_d_level_'.$resellers[$key]['username'].'_prompt">'.$level.'</span>
				<span style="width:50px;display:none;" id="span_d_level_'.$resellers[$key]['username'].'">
					<form class="change_d_level" id="form_d_level_'.$resellers[$key]['username'].'">
					<select id="change_level_select'.$resellers[$key]['username'].'" name="change_level_select" val="form_d_level_'.$resellers[$key]['username'].'" class="change_d_level_select" style="width:48px;">
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="agree">require agreement</option>
					</select>
					<input type="hidden" name="distro_username" value="'.$resellers[$key]['username'].'">
				</form>
				</span>
				<span style="color:#aecd37;cursor:pointer;width:50px;" class="remove_reseller" id="remove_'.$resellers[$key]['username'].'" username="'.$resellers[$key]['username'].'">remove</span>
				<span style="color:red;cursor:pointer;width:50px;display:none;" id="activate_'.$resellers[$key]['username'].'" class="activate_reseller" username="'.$resellers[$key]['username'].'">activate</span>
				<br class="clear:left;">
				</div>';//acc_head
				
		//build content
			$content .= '<div class="acc_reseller_content accordionContent">';
			$content .= '</div>';//acc_resellser_content
		}
		$content .= '</div></div></div>';//account_acc_wrapper
		
		
		self::$tab_list .= $title;
		self::$tab_sections .= $content;	
	}//add_resellers_tab
	private function add_processed_tab($resellers){
		$processed = self::$current_user->get_all_processed();
		$access = self::$current_user->get_info_attr('access');
		$p_count = count($processed);
		
		$title ='<li class="tab" style="width:30px;"><a href="#proc_tab">
			<span style="background-image:url(./images/circ_complete.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$p_count.'
			</span></a></li>
		';
		
		$content ='<div id="proc_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">FINISHED LEADS</div>
					<div style="font:24px Verdana;color:#aecd37;float:left;padding-top:17px;" id="look_for_finished"></div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
		$content_no .= '<div class="adjust_body"><div id="proc_lead_desc" style="height:20px;margin:4px auto;">
			<span style="width:50px;">STATUS</span>
			<span style="width:125px;">BUSINESS NAME</span>
			<span style="width:160px;">LOCATION</span>
			<span style="width:150px;">EMAIL</span>
			<span style="width:113px;">NAME</span>
			<span style="width:110px;">PHONE</span>
			<span style="width:100px;">DISTRO</span>
			<span style="width:40px;">NOTES</span>
			<span>GAVE CC</span>
			<br style="clear:left;" />													
		</div>
		';
		
		$content_no .= '<div id="processed_lead_acc_wrapper" style="height:30%;overflow-y:scroll;">
		';
		$json_decode = new Json();
		
		$gave_cc = '';
		$show_merc = '';
			$merc_count = 0;		 
		/*
		foreach($processed as $key=>$attrs){
			//echo $attrs['demo_type'].'--<br />';
			//$json_decode = new Json();
			$spec = $resellers[$attrs['distro_code']]['special_json'];
			$accepts_mercury = $json_decode->decode($spec);
			$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
			
			if($attrs['demo_type'] == 'signups'){
			//echo 'signups--'.$attrs['company_name'].'<br />';
				$gave_cc = '<span style="width:23px;margin-left:5px;"><img class="acc_status" src="./images/i_cc_mini.png" alt="circ_blue" /></span>';
			}else{
				$gave_cc = '';
			}
			//
			
				$hide_from_merc = 'show_merc';

			if(($attrs['lead_source'] == 'mercury' || $merc_dist) && $access=='mercury'){
				$hide_from_merc = '';
				$merc_count++;
			}
			if($access=='admin'){
				$hide_from_merc = '';
			}
			//$signups = self::$current_user->get_lead_attr($attrs['data_name'], 'signup_step');
			//echo '00000000::'.$attrs['data_name'].', '.$signups.'<br />';
			$content .= '<div class="acc_head accordionButton '.$hide_from_merc.'">
				<span style="width:23px;"><img class="acc_status" src="./images/circ_blue.png" alt="circ_blue" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:150px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;" class="hover_flow">'.$attrs['email'].'</span><div class="loc_head_div">|</div>
				<span style="width:95px;" class="hover_flow">'.$attrs['firstname'].' '.$attrs['lastname'].'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" class="hover_flow">'.$attrs['phone'].'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" class="hover_flow">'.$attrs['distro_code'].'</span><div class="loc_head_div">|</div>
				
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.'
				'.$gave_cc.'
				<br class="clear:left;">
				</div>		
				';
				
		}//foreach processed()
		*/
		if($access == 'mercury'){
			$title ='<li class="tab" style="width:30px;"><a href="#proc_tab">
				<span style="background-image:url(./images/circ_complete.png);background-repeat:no-repeat;display:inline-block;width:21px;">
					'.$merc_count.'
				</span></a></li>
			';
		}
		//$content .= '</div>';
		self::$current_user->set_all_lead_entries();
		//self::$current_user->set_all_leads_with_demos();

		$all_lead_entries = self::$current_user->get_all_lead_entries();
		//$all_lead_entries = array_unique($all_lead_entries);
		$all_leads_with_demos = self::$current_user->get_leads_with_demos();
		//$all_lead_with_demos = array_unique($all_lead_with_demos);
		$content .= self::report_numbers($processed, $resellers, $all_lead_entries, $all_leads_with_demos, $access);

		$content .= '</div>';
		


		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_processed_tab
	private function add_archive_tab($resellers){
		$unacc = self::$current_user->get_all_unassigned_leads();
		$in_progress = self::$current_user->get_all_in_progress();
		//$processed = self::$current_user->get_all_processed();
		//echo 'current_access===='.$current_access.'<br />';
		//var_dump($processed);
		//var_dump($unacc);
		//var_dump($resellers);
		$j_decode = new Json();
		//count unaccepted leads
		//echo $all_count;
		
		
		function lead_color_arch($stat, $div,$color_class){
		//echo 'stat='.$stat.' div='.$div.' cc='.$color_class;
			if($stat === $div){
				//echo '  true';
				return $color_class;
			}
		}//lead_color_u()
		
		
		$title ='<li class="tab load_archive"><a href="#archive_tab">ARCHIVE</a></li>
		';
		$content ='<div id="archive_tab" class="tab_section tab00">';
		$content .= '
			<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">UNACCEPTED LEADS</div>';
		$content .= '
			</div>
				<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
			</div>';
		$content .= '<div class="adjust_body">';
		
		$content .= '<div style="margin:20px;text-align:center;font:30px verdana;color:#439789;">ALL LEADS OLDER THAN 40 DAYS</div>';
		
		$content .='
		<div id="archive_lead_desc">
			<div style="height:30px;width:950px;margin: 0 auto;"><img src="./images/lead_legend.png" alt="llegend"></div>
		    <div style="height:30px;width:950px;margin:0 auto 20px;"><img src="./images/lead_legend_sponsor.png" alt="mlegends"></div>
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:160px;">TOTAL AGE(Days Hrs:min:sec)</span>
			<span style="width:156px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:105px;">REMIND</span>
			<span style="width:80px;">RESELLER</span>
			<span style="width:80px;">ASSIGN</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div><div id="#archive_entry_div"></div>';
		//foreach($in_progress as $key=>$attrs)
		/*foreach($in_progress as $key=>$attrs){
			//$lead_stat = 'accept';
			$lead_source = '';
			$lead_source = self::display_merc_logo($attrs, $resellers, $current_access, $j_decode);
			$can_expand = '<span style="width:20px;"></span>';
			
			$accepts_mercury = $j_decode->decode($resellers[$attrs['distro_code']]['special_json']);
			$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
			
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_accept';
			if(preg_replace('/[1-9]/', '', $attrs['contacted']) != '0000-00-00 00:00:00') {
				if(preg_replace('/[1-9]/', '', $attrs['made_demo_account']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}else{
					$circ_image_ref = 'circ_demo_account';
					$lead_stat = 'demo_account';
	
				}
			}else if(preg_replace('/[1-9]/', '', $attrs['accepted']) != '0000-00-00 00:00:00'){
				if(preg_replace('/[1-9]/', '', $attrs['made_demo_account']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
					 
				}else{
					$circ_image_ref = 'circ_contact';
					$lead_stat = 'contact';
				}
			}else if(preg_replace('/[1-9]/', '', $attrs['made_demo_account'])!= '0000-00-00 00:00:00'){
				if(preg_replace('/[1-9]/', '', $attrs['accepted']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}else{
					$circ_image_ref = 'circ_accept_has_demo_account';
					$lead_stat = 'accept';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}
			}else{
				$circ_image_ref = 'circ_accept';
			}

			$time_values = get_lead_times($attrs);
			//self::$current_user->set_lead_attr($attrs['data_name'], 'signup_step', $time_values['signups']);
			$total_age = $time_values['first'];
			$last_step_time = $time_values['most_recent'];
			$assigned_time = $time_values['assigned'];
				
			$last_step_time = str_replace("-", "", $last_step_time);
			$total_age = str_replace("-", "", $total_age);
			
			$distro_username = $attrs['distro_code'];
			$distro_phone = $resellers[$distro_username]['phone'];
			$distro_email = $resellers[$distro_username]['email'];
			$distro_company = $resellers[$distro_username]['company'];
			$distro_name = $resellers[$distro_username]['f_name'].' '.$resellers[$distro_username]['l_name'];
			$tooltip_id = $attrs['id'].'_'.$distro_username.'_content';
			
			$distro_tooltip = '
			<div id="'.$tooltip_id.'" style="display:none;">
				<div><strong>Name: </strong>'.$distro_name.'</div>
				<div><strong>Company: </strong>'.$distro_company.'</div>
				<div><strong>Phone: </strong>'.$distro_phone.'</div>
				<div><strong>Email: </strong>'.$distro_email.'</div>
			</div>';//$distro_tooltip
			
			
			
			$attrs_for_jab = 'b_name="'.$attrs['company_name'].'" email="'.$attrs['email'].'" phone="'.$attrs['phone'].'" contact="'.$attrs['firstname'].' '.$attrs['lastname'].'" reseller="'.$attrs['distro_code'].'"';
			$content .= '<div class="acc_head accordionButton">
				'.$distro_tooltip.'
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:140px;" class="lead_on_comp_name">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:100px;">'.$total_age.'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" >'.$last_step_time.'</span><div class="loc_head_div">|</div>
				<span style="width:100px;">'.$assigned_time.'</span><div class="loc_head_div">|</div>
				<span style="width:50px;text-align:center;cursor:pointer;" l_id="'.$attrs['id'].'" class="remove_lead"><img src="./images/btn_decline.png" alt="big_X" /></span><div class="loc_head_div">|</div>
				<span style="width:50px;text-align:center;" class="jab_distro" '.$attrs_for_jab.'><img src="./images/jab.png" alt="jab" /></span><div class="loc_head_div">|</div>
				<span style="width:110px;" class="distro_tip" tipid="#'.$tooltip_id.'">'.$attrs['distro_code'].'</span><div class="loc_head_div">|</div>
				<span style="width:60px;"><img src="./images/btn_assign.png" alt="assign" style="height:23px;" class="assign_lead"/></span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.'
				<br class="clear:left;">
				</div>		
				';
			
			$content .= '<div class="acc_lead_content accordionContent">
				<div class="lead_acc_content_head"><img src="./images/prog_bar_full.png" alt="p_b_f"/></div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color_arch($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color_arch($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color_arch($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color_arch($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>
				<div style="margin-bottom:20px;">Lead Details:</div>
				<div class="in_progress_details" style="height:200px;">
					<div style="width:400px;float:left;text-align:left;margin:20px 5px 20px 50px;font:12px verdana;line-height:175%;">
						<div><span class="lead_detail_title">Location:</span><span>'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span></div>
						<div><span class="lead_detail_title">Name:</span><span>'.$attrs['firstname'].' '.$attrs['lastname'].'</span></div>
						<div><span class="lead_detail_title">Email:</span><span>'.$attrs['email'].'</span></div>
						<div><span class="lead_detail_title">Phone:</span><span>'.$attrs['phone'].'</span></div>
					</div>
					<div style="width:450px;float:left;font:12px verdana;line-height;175%;text-align:left;">
						<div><span style="font-weight:bold;color:black;">Contacted info:</span><span>'.$attrs['contact_note'].'</span></div>
					</div>
				</div>
				
			</div>';
		}//foreach($in_progress as $key=>$value)*/
		 
		$content .= '</div></div>';
		/*lead_type_content($lead_stat, $attrs['data_name']?$attrs['data_name']:$attrs['company_name']).'</div>*/
		
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//archive_tab
	private function add_reports_tab(){
		$unacc = self::$current_user->get_all_unassigned_leads();
		$in_progress = self::$current_user->get_all_in_progress();
		//$processed = self::$current_user->get_all_processed();
		//echo 'current_access===='.$current_access.'<br />';
		//var_dump($processed);
		//var_dump($unacc);
		//var_dump($resellers);
		$j_decode = new Json();
		//count unaccepted leads
		//echo $all_count;
		
		$states = self::get_state_input();
		$title ='<li class="tab"><a href="#reports_tab">REPORTS</a></li>
		';
		$content ='<div id="reports_tab" class="tab_section tab00">';
		$content .= '
			<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">REPORTS</div>';
		$content .= '
			</div>
				<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
			</div>';//tab_top
		$content .= '<div class="adjust_body">';
		
		$content .= '
		<div style="margin:20px;text-align:center;font:24px verdana; color:#439789; border-bottom:1px solid #aecd37;">
			<div id="report_header_desc">
			PLEASE BUILD DESIRED REPORT AND THEN CLICK SUBMIT												
		</div></div>';
		
		$content .= '
		<div style="margin:20px auto; ">
			<span class="report_header_span">REPORT TYPE</span>
			<span class="report_header_span">REPORT FILTER</span>
			<span class="report_header_span">STATE FILTER</span>
		</div>
		<form id="request_report_form" method="post" action="">
		<div id="report_selections">
			<div class="mode_choice">
				<label for="mode_select" >ACTIVE LICENSE MODE</label>
				<input type="radio" name="mode_select" value="active" checked/><br />
				<label for="mode_select" >TOTAL LICENSE MODE</label>
				<input type="radio" name="mode_select" value="total" />
			</div>
			<div class="report_select_div">
				<select class="report_select" id="list_a" name="list_a">
					<option value="">CHOOSE</option>
					<option value="distros">Distributors</option>
					<option value="licenses_total">All Licenses</option>
					<option value="1">Silver Licenses</option>
					<option value="2">Gold Licenses</option>
					<option value="3">Platinum Licenses</option>
					<option value="licenses_by_unit">License Units</option>
				</select>
			</div>

			<div class="report_select_div">
				<select class="report_select" id="list_b" name="list_b" disabled>
					<option value="">CHOOSE</option>
					<option value="NULL">--NULL--</option>
					<option value="NULL">All Specialist Levels</option>
					<option value="3">Preferred Specialist</option>
					<option value="2">Certified Specialist</option>
					<option value="1">Premiere Specialist</option>
					<option value="licenses_total">Licenses Total</option>
					<option value="licenses_by_unit">License Unit Breakdown</option>
				</select>
			</div>

			<div class="report_select_div">
				<select class="report_select" id="list_state" name="list_state" disabled>
				'.$states.'
				</select>
			</div>
			<div style="margin: 40px 250px; height:200px;">
				<label for="date_start">Start Date::</label>
				<input type="input" id="date_start" class="date_selection" name="date_start" value="-null-"/>
				<label for="date_end">End Date::</label>
				<input type="input" id="date_end" class="date_selection" name="date_end" value="-null-"/>
			</div>
			</form>
		</div>';//report_selections
		
		
		/*<div style="margin: 40px 250px; height:200px;">
				<div id="inlineDatepicker_start" style="float:left;margin-right:30px;"></div>
				<div id="inlineDatepicker_end" style="float:left;"></div>
		</div>*/
			
			
		$content .= '
		<div id="loading_report" style="display:none;">Loading...</div>
		<div id="report_finished_choices">
				<div class="report_submit_choice" id="report_choose_submit">Submit</div>
		</div>';
		/*<a href="./exec/tab_delimited_report.txt" target="_blank" id="tab_report_link" style="display:none;">tab delimited report</a>*/
			/*<div class="report_submit_choice" id="report_choose_export">Export</div>
				<div class="report_submit_choice" id="report_choose_graph">Graph</div>*/
		$content .= "
		<link rel='stylesheet' type='text/css' href='/manage/css/autoTable.css' />
		<style type='text/css'>
		div.autoTable {
			height: 800px;
			float:left;
		}
		table.autoTableContainer{float:left;}
	    </style>";
	    $content .= '
	    <div id="report_view">
	    	<div id="total_unit_count" style="display:none;"></div>
	    	<div id="report_table_div" style="display:none;">
	    		<table class="distro_report_table" id="distro_report_table_display">
	    			<thead>
	    				<tr>
	    					<th>head</th>
	    					<th>another head</th>
	    					<th>header tit</th>
	    				</tr>
	    			</thead>
	    			<tbody>
	    				<tr>
	    					<td>one</td>
	    					<td>two</td>
	    					<td>Third column value</td>
	    				</tr>
	    				<tr>
	    					<td>first column value</td>
	    					<td>second</td>
	    					<td>three</td>
	    				</tr>
	    				<tr>
	    					<td>first</td>
	    					<td>Second Column Value</td>
	    					<td>third</td>
	    				</tr>
	    			</tbody>
	    		</table>
	    	</div>
		</div>';//report_view

		$content .= '</div></div>';
		/*lead_type_content($lead_stat, $attrs['data_name']?$attrs['data_name']:$attrs['company_name']).'</div>*/

		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//reports_tab


	//end of tab content funcitions
	//
	//
	// returns header section (might do this before class gets built)
	private function build_header(){
		function get_redirect(){
			$rd = '';
			$rd .= '
			<div style="text-align:center;">
				<form method="post" action="https://admin.poslavu.com/distro/">
				<input type="hidden" name="old_redirect" value="old" />
				If you wish to return to the old reseller backend, please click <input type="submit" value="here" style="curser:pointer;" />
				</form>
			</div>';
			return $rd;
		}
		$a_lead_info_strings = $this->build_leads_info_array();
		$header = '<div id="distro_header">
			<div id="distro_header_middle">
				<img src="./images/logo_distro_icon.png" style="margin:10px;" alt="l_d_i"/>
				<span style="position: relative;left: 20px;bottom: 25px;">DISTRIBUTOR PORTAL</span>
				<span style="float:right; position: relative;right: 150px;top: 25px;">'.self::$current_user->get_info_attr('f_name').' '.self::$current_user->get_info_attr('l_name').'</span><span style="float:right; position: relative;right: -50px;top: 25px;"><a href="index.php?logout=1">(logout)</a></span>
			</div>
		</div>'.get_redirect().'
		<script type="text/javascript">
			lead_info=[];
			'.implode(';
			',$a_lead_info_strings).'
		</script>
		';
		echo $header;
		
	}//build_header
	private function build_leads_info_array(){
	
	
		// leads text
		$a_lead_info_strings = array();
		foreach(self::$current_user->get_leads() as $a_lead) {
			$a_lead_info_strings[] = 'lead_info['.$a_lead['id'].']=[]';
			foreach($a_lead as $k=>$v) {
				$a_lead_info_strings[] = 'lead_info['.$a_lead['id'].']["'.$k.'"]="'.$v.'"';
			}
		}
		return $a_lead_info_strings;
	}//build_leads_info_array
	private function build_header_admin(){
		function get_redirect_admin(){
			$rd = '';
			$rd .= '
			<div style="text-align:center;">
				<form method="post" action="https://admin.poslavu.com/distro/">
				<input type="hidden" name="old_redirect" value="old" />
				If you wish to return to the old reseller backend, please click <input type="submit" value="here" style="curser:pointer;" />
				</form>
				<div id="show_agreement_btn" style="width:20;height:20;cursor:pointer;border:1px solid black;background#f0f0f0;">show agreement</div>
			</div>';
			//onclick="lavuLog(\'BOING!\');"
			$div_for_campaign_breakdown = '
			<div id="toggle_sales_lead_display" style="cursor:pointer;width:300px;margin:10px auto;text-align:center;" onclick="display_sales_lead_display();">Show Sales Lead Display <span style="color:blue;">V</span></div>
			<div id="sales_lead_display" style="display:none;border:1px solid black;background:#efc;padding:10px;">
				<div id="sales_lead_display_header">
					<div id="sales_lead_display_header_left" style="display:inline-block;vertical-align:top;width:325px;">
						<div id="sales_lead_display_time_choice" style="display:inline-block; width:250px;">
							MONTH - JULY 06
						</div>
						<div style="display:inline-block;">
							ORIGIN
						</div>
					</div>
					<div id="sales_lead_display_header_right" style="display:inline-block;vertical-align:top;">
						<div class="sales_lead_campaign_name" style="display:inline-block;vertical-align:top;width:100px;text-align:center;">
							Total Signups
						</div>
						<div class="sales_lead_campaign_name" style="display:inline-block;vertical-align:top;width:100px;text-align:center;">
							Adwords Leads
						</div>
						<div class="sales_lead_campaign_name" style="display:inline-block;vertical-align:top;width:100px;text-align:center;">
							Facebook
						</div>
						<div class="sales_lead_campaign_name" style="display:inline-block;vertical-align:top;width:100px;text-align:center;">
							Microsites
						</div>
						<div class="sales_lead_campaign_name" style="display:inline-block;vertical-align:top;width:100px;text-align:center;">
							Other Ads
						</div>
						<div class="sales_lead_campaign_name" style="display:inline-block;vertical-align:top;width:100px;text-align:center;">
							No info
						</div>
					</div>
				</div>
				<div id="sales_lead_display_time" class="sales_lead_display_segment" style="width:325px;display:inline-block;border:1px solid black;background:#eee;">
					<div id="sales_lead_display_month" style="display:inline-block;vertical-align:top;width:160px;">
						<div class="sales_lead_display_time_segment_selection">WEEK</div>
						<div class="sales_lead_display_time_segment_selection">YEAR</div>
						<div class="sales_lead_display_time_segment_selection">JANUARY</div>
						<div class="sales_lead_display_time_segment_selection">FEBRUARY</div>
						<div class="sales_lead_display_time_segment_selection" style="font-weight:bold;">MARCH</div>
						<div class="sales_lead_display_time_segment_selection">APRIL</div>
						<div class="sales_lead_display_time_segment_selection">MAY</div>
						<div class="sales_lead_display_time_segment_selection">JUNE</div>
						<div class="sales_lead_display_time_segment_selection">JULY</div>
						<div class="sales_lead_display_time_segment_selection">AUGUST</div>
						<div class="sales_lead_display_time_segment_selection">SEPTEMBER</div>
						<div class="sales_lead_display_time_segment_selection">OCTOBER</div>
						<div class="sales_lead_display_time_segment_selection">NOVEMBER</div>
						<div class="sales_lead_display_time_segment_selection">DECEMBER</div>
					</div>
					<div id="sales_lead_display_level_key" style="display:inline-block;vertical-align:top;width:160px;">
						<div style="margin:20px 10px 20px 10px;">TOTAL LEADS</div>
						<div style="margin:10px 10px 10px 10px;">FREE TRIAL</div>
						<div style="margin:10px 10px 10px 10px;">UNRESOLVED</div>
						<div style="margin:30px 10px 10px 10px;">PAID ACCOUNTS</div>
					</div>
				</div>
				<div id="sales_lead_display_numbers" class="sales_lead_display_segment" style="width:620px;display:inline-block;border:1px solid black;background:#eee;">
					<div class="sales_lead_campain_numbers" style="display:inline-block;vertical-align:top;margin-right:50px;padding:10px;">
						<div class="sales_lead_campaign_total" style="color:#aecd37;font-size:31px;margin-bottom:15px;">340</div>
						<div style="margin-bottom:10px;">230</div>
						<div>127</div>
						<div class="sales_lead_campain_paid" style="color:red;font-size:31px;margin-top:15px;">103</div>
					</div>
					<div class="sales_lead_campain_numbers" style="display:inline-block;vertical-align:top;margin-right:50px;padding:10px;">
						<div class="sales_lead_campaign_total" style="color:#aecd37;font-size:31px;margin-bottom:15px;">11</div>
						<div style="margin-bottom:10px;">03</div>
						<div>08</div>
						<div class="sales_lead_campain_paid" style="color:red;font-size:31px;margin-top:15px;">00</div>
					</div>
					<div class="sales_lead_campain_numbers" style="display:inline-block;vertical-align:top;margin-right:50px;padding:10px;">
						<div class="sales_lead_campaign_total" style="color:#aecd37;font-size:31px;margin-bottom:15px;">03</div>
						<div style="margin-bottom:10px;">00</div>
						<div>02</div> 
						<div class="sales_lead_campain_paid" style="color:red;font-size:31px;margin-top:15px;">01</div>
					</div>
					<div class="sales_lead_campain_numbers" style="display:inline-block;vertical-align:top;margin-right:50px;padding:10px;">
						<div class="sales_lead_campaign_total" style="color:#aecd37;font-size:31px;margin-bottom:15px;">128</div>
						<div style="margin-bottom:10px;">45</div>
						<div>40</div>
						<div class="sales_lead_campain_paid" style="color:red;font-size:31px;margin-top:15px;">5</div>
					</div>
					
					<div class="sales_lead_campain_numbers" style="display:inline-block;vertical-align:top;margin-right:50px;padding:10px;">
						<div class="sales_lead_campaign_total" style="color:#aecd37;font-size:31px;margin-bottom:15px;">17</div>
						<div style="margin-bottom:10px;">04</div>
						<div>03</div>
						<div class="sales_lead_campain_paid" style="color:red;font-size:31px;margin-top:15px;">00</div>
					</div>
					<div class="sales_lead_campain_numbers" style="display:inline-block;vertical-align:top;padding:10px;">
						<div class="sales_lead_campaign_total" style="color:#aecd37;font-size:31px;margin-bottom:15px;">18</div>
						<div style="margin-bottom:10px;">07</div>
						<div>07</div>
						<div class="sales_lead_campain_paid" style="color:red;font-size:31px;margin-top:15px;">07</div>
					</div>
				</div>
			</div>
			';
			return $rd.$div_for_campaign_breakdown;
		}
	    //<div id="show_agreement_btn" style="width:20;height:20;cursor:pointer;border:1px solid black;background#f0f0f0;">show agreement</div>

		$a_lead_info_strings = $this->build_leads_info_array();
		//header text
		$header = '<div id="distro_header_admin" style="border-top:3px solid #aecd37;">
			<div id="distro_header_middle">
				<img src="./images/logo_distro_icon.png" style="margin:10px;" alt="l_d_i"/>
				<span style="position: relative;left: 20px;bottom: 25px;color:#aecd37;">ADMIN</span>
				<span style="float:right; position: relative;right: 150px;top: 25px;color:#aecd37;">'.self::$current_user->get_info_attr('f_name').' '.self::$current_user->get_info_attr('l_name').'</span><span style="float:right; position: relative;right: -50px;top: 25px;"><a href="index.php?logout=1">(logout)</a></span>
			</div>
		</div>'.get_redirect_admin().'
		<script type="text/javascript">
			lead_info=[];
			'.implode(';
			',$a_lead_info_strings).'
		</script>
		';
		echo $header;	
	}//build_header_admin
	private function build_header_mercury(){
		
	
		//header text
		$header = '<div id="distro_header_admin" style="border-top:3px solid #aecd37; background:#8d0714;height:75px;">
			<div id="distro_header_middle">
				<img src="./images/logos_merc_lavu.png" style="margin:10px;" alt="l_d_i"/>
				<span style="float:right; position: relative;right: 100px;top: 25px;"><a href="index.php?logout=1">(logout)</a></span>
			</div>
		</div>';
		echo $header;	
	}//build_header_mercury
	private function build_header_ingram(){
		
	
		//header text
		$header = '<div id="distro_header_admin" style="border-top:3px solid #aecd37; background:#c22a39;height:75px;">
			<div id="distro_header_middle">
			<img src="./images/ingrammicro.gif" style="margin:10px;" alt="ingramlogo"/>
				<span style="float:right; position: relative;right: 100px;top: 25px;"><a href="index.php?logout=1">(logout)</a></span>
			</div>
		</div>';
		echo $header;	
	}//build_header_mercury
	public static function display_datetime($dt,$type="both"){
		$dt_parts = explode(" ",$dt);
		if(count($dt_parts) > 1)
		{
			$d_parts = explode("-",$dt_parts[0]);
			$t_parts = explode(":",$dt_parts[1]);
			if(count($d_parts) > 2 && count($t_parts) > 2)
			{
				$ts = mktime($t_parts[0],$t_parts[1],$t_parts[2],$d_parts[1],$d_parts[2],$d_parts[0]);
				if($type=="date")
					return date("m-d-y",$ts);
				else if($type=="time")
					return date("h:i a",$ts);
				else
					return date("m-d-y h:i a",$ts);
			}
			else return $dt;
		}
		else return $dt;
	}//display_datetime
	//Used to determine what level to display for each Distro's restaurants
	public static function display_level($trial_end,$original_license,$current_package_name, $dname, $s_licenses_string=''){
		global $o_package_container;
		$s_retval = '';
		//echo 'trial end:: '.$trial_end.', orig_license:: '.$original_license.', curr_pack_name:: '.$current_package_name.', dname:: '.$dname.', s_lic_string:: '.$s_licenses_string.'<br />';
		if($trial_end=="" && $original_license==""){//alt signup
			$s_retval .= 'Alternative License';
		}else if($current_package_name!=""){//Lite,platinum,gold
			if(strpos($current_package_name,'Platinum') != 0){//Lite or Gold
				$s_retval .= $o_package_container->get_printed_name_by_attribute('name', $current_package_name);
			}else{
				$s_retval .= $o_package_container->get_printed_name_by_attribute('name', $current_package_name);
			}
		}else{//Apply License
			$s_retval .= '<img src="./images/btn_applylicense__.png" alt="btn_apply" class="apply_license_btn" val="'.$dname.'" />
			<input type="hidden" name="available_licenses" id="available_licenses" value="'.$s_licenses_string.'" />';
		}
		
		// check for upgrades
		$a_available_upgrades_for_package = $o_package_container->get_available_upgrades($current_package_name, TRUE);
		if (count($a_available_upgrades_for_package))
			$s_retval .= '<img src="./images/btn_upgrade.png" style="margin-left:10px;width:15px;" alt="btn_upg" class="upgrade_license_btn"/>
			<input type="hidden" name="available_upgrades" value="'.implode('|', $a_available_upgrades_for_package).'">';
		
		return $s_retval;
	}//display_level
	private static function display_merc_logo($attrs, $resellers, $access, $j_decode){
		$distro_code = $attrs['distro_code'];
		
		
		$accepts_mercury = $j_decode->decode($resellers[$distro_code]['special_json']);
		$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
		
		$to_mercury = false;
		if($merc_dist){$to_mercury = true;}
		
		
		$merc_logo = '';
		
		if($attrs['lead_source'] == 'mercury'){
			if($to_mercury){//from mercury, to mercury
				$merc_logo = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}else{//from mercury, not to mercury. sshhhhh!!!
				$merc_logo = '<span style="width:15px;"><img src="./images/i_trylavu_nomerc_mini.png" alt="shhh"/></span>';
				if($access == 'mercury'){//just show mercury the lavu icon
					$merc_logo = '<span style="width:15px;"><img src="./images/i_lavulead_mini.png" alt="mm"/></span>';
				}
			}
		}else{
			if($to_mercury){//not from mercury, but given to mercury distributor
				$merc_logo = '<span style="width:15px;"><img src="./images/i_mercdistro_lavulead_mini.png" alt="mm"/></span>';
			}
			else{//not a merc distributor, not from mercury
				$merc_logo = '<span style="width:15px;"><img src="./images/i_lavulead_mini.png" alt="mm"/></span>';
			}
		}
		return $merc_logo;
	}//display_merc_logo
	private static function report_numbers($processed, $resellers, $all_lead_entries,$all_leads_with_demos, $access ){
		$stats = array();
		$stats['total_leads'] = count($all_lead_entries);
		$stats['leads_with_demos'] = count($all_leads_with_demos);
		$stats['completed_leads'] = count($processed);
		//var_dump($stats).'<br />---------------<br />';
		$json_decode = new Json();
		$show_merc = '';
		if($access == 'mercury'){
			$show_merc = 'show_merc';
		}

		//find non-lavu leads
		$non_lavu = 0;
		$active = 0;
		$cancelled = 0;
		$from_mercury = 0;
		$to_mercury = 0;

		$count_non = 0;
		$count_contacted = 0;
		$count_converted = 0;
		$count_total = 0;

		$count_merc_non = 0;
		$count_merc_converted = 0;
		$count_merc_total = 0;

		$count_try_non = 0;
		$count_try_converted = 0;

		$count_try_total = 0;//all leads since ppc started
		$count_try_ppc_no_click = 0;//all leads since ppc, no info
		$count_try_ppc = 0;//all leads since ppc, have goog anal info

		$count_try_ppc_demo_total = 0;//all leads since ppc started (have demo)
		$count_try_ppc_no_click_demo = 0;//all leads since ppc, no info (have demo)
		$count_try_ppc_demo = 0;//all leads since ppc, have goog anal info (have demo)
		
		$count_try_ppc_contacted = 0;
		$count_try_ppc_no_click_contacted = 0;
		$count_merc_contacted = 0;
		$count_try_ppc_total_contacted = 0;

		$count_try_ppc_sold_total = 0;//all leads since ppc started (sold)
		$count_try_ppc_no_click_sold = 0;//all leads since ppc, no info (sold)
		$count_try_ppc_sold = 0;//all leads since ppc, have goog anal info (sold)



		$count_signups_demo = 0;
		$count_distro_demo = 0;

		//containers for the table info
		$non_lavu_table = '
		<div class="distro_report_table_div" id="table_non_lavu_div">
			<table class="distro_report_table" id="table_non_lavu">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		$converted_table = '
		<div class="distro_report_table_div" id="table_converted_div">
			<table class="distro_report_table" id="table_converted">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		$total_table = '
		<div class="distro_report_table_div" id="table_total_div">
			<table class="distro_report_table" id="table_total">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		$contacted_table = '
		<div class="distro_report_table_div" id="table_contacted_div">
			<table class="distro_report_table" id="table_contacted">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';

		//from try tables
		$try_non_lavu_table = '
		<div class="distro_report_table_div" id="table_try_non_lavu_div">
			<table class="distro_report_table" id="table_try_non_lavu">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		$try_converted_table = '
		<div class="distro_report_table_div" id="table_try_converted_div">
			<table class="distro_report_table" id="table_try_converted">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		$try_ppc_table = '
		<div class="distro_report_table_div" id="table_try_total_div">
			<table class="distro_report_table" id="table_try_total">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';

		//to merc tables
		$merc_non_lavu_table = '
		<div class="distro_report_table_div" id="table_merc_non_lavu_div">
			<table class="distro_report_table" id="table_merc_non_lavu">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		$merc_converted_table = '
		<div class="distro_report_table_div" id="table_merc_converted_div">
			<table class="distro_report_table" id="table_merc_converted">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		$merc_total_table = '
		<div class="distro_report_table_div" id="table_merc_total_div">
			<table class="distro_report_table" id="table_merc_total">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		$time_frame_table = '
		<div class="distro_report_table_div" id="table_time_frame_campaign_div">
		<table class="distro_report_table" id="table_time_frame_campaign">
				<thead>
					<th>Company Name</th>
					<th>Location</th>
					<th>Name</th>
					<th>Phone</th>
					<th>Distro</th>
				</thead>
				<tbody>
		';
		//var_dump($resellers).'----------------------------<br /><br />';
		foreach($all_lead_entries as $key=>$value){
			//echo $key.', '.$value.'<br />';
			$distro = $value['distro_code'];
			$special_json = $resellers[$distro]['special_json'];
			$accepts_mercury = $json_decode->decode($special_json);
			$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
			//echo $distro.', '.$special_json.', '.$merc_dist.'<br />';
			//if($value['distro_code']=='mykoreit'){
				//error_log('distro_tab_container  find ppc values company:: '.$value['distro_code']);
				$ppc_values = self::find_ppc_values($value['notes']);
				//error_log('distro_tab_container  find ppc values company:: '.strlen($ppc_values));
			//}

			//all of the totals
			$count_total++;
			$total_table .= '
				<tr>
					<td>'.$value['company_name'].'</td>
					<td>'.$value['city'].' '.$value['state'].'</td>
					<td>'.$value['firstname'].' '.$value['lastname'].'</td>
					<td>'.$value['phone'].'</td>
					<td>'.$value['distro_code'].'</td>
				</tr>';

			if($value['contacted']!= '0000-00-00 00:00:00')
			{
				$count_contacted++;
				$contacted_table .= '
				<tr>
					<td>'.$value['company_name'].'</td>
					<td>'.$value['city'].' '.$value['state'].'</td>
					<td>'.$value['firstname'].' '.$value['lastname'].'</td>
					<td>'.$value['phone'].'</td>
					<td>'.$value['distro_code'].'</td>
				</tr>';
				
			}//all contacted
			
			//leads coming from mercury
			if($value['lead_source'] == 'mercury' && $value['created'] > '2013-08-01 00:00:00')
			{
			$count_try_total++;
			if(strpos($ppc_values, 'not_found')===false)
			{
				$count_try_ppc++;

			}else
			{
				$count_try_ppc_no_click++;
			}
			if($value['made_demo_account'] != '0000-00-00 00:00:00')
			{//has demo
				$count_try_ppc_demo_total++;
				$try_ppc_table .= '
					<tr class="ppc_demo_total_row">
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td class="demo_stamp">'.$value['created'].'</td>
						<td>'.$value['phone'].'</td>
						<td>'.$value['distro_code'].'</td>
					</tr>';

				if(strpos($ppc_values, 'not_found')===false)
				{
					$count_try_ppc_demo++;
					$try_ppc_table .= '
					<tr class="ppc_demo_info_row ppc_demo_total_row">
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td class="demo_stamp">'.$value['created'].'</td>
						<td>'.$value['phone'].'</td>
						<td>'.$value['distro_code'].'</td>
					</tr>';
				}else
				{
					$count_try_ppc_no_click_demo++;
					$try_ppc_table .= '
					<tr class="ppc_demo_no_info_row ppc_demo_total_row">
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td class="demo_stamp">'.$value['created'].'</td>
						<td>'.$value['phone'].'</td>
					<td>'.$value['distro_code'].'</td>
					</tr>';
				}
				if($value['license_applied'] != '0000-00-00 00:00:00')
				{//sold
					$count_try_ppc_sold_total++;
					$try_ppc_table .= '
					<tr class="ppc_sold_all_row">
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td>'.$value['firstname'].' '.$value['lastname'].'</td>
						<td>'.$value['phone'].'</td>
					<td>'.$value['distro_code'].'</td>
					</tr>';
					if(strpos($ppc_values, 'not_found')===false)
					{
						$count_try_ppc_sold++;
						$try_ppc_table .= '
						<tr class="ppc_sold_info_row ppc_sold_total_row">
							<td>'.$value['company_name'].'</td>
							<td>'.$value['city'].' '.$value['state'].'</td>
							<td class="created_stamp">'.$value['created'].'</td>
							<td>'.$value['phone'].'</td>
							<td>'.$value['distro_code'].'</td>
						</tr>';
					}else
					{
						$count_try_ppc_no_click_sold++;
						$try_ppc_table .= '
						<tr class="ppc_sold_no_info_row ppc_sold_total_row">
							<td>'.$value['company_name'].'</td>
							<td>'.$value['city'].' '.$value['state'].'</td>
							<td class="created_stamp">'.$value['created'].'</td>
							<td>'.$value['phone'].'</td>
							<td>'.$value['distro_code'].'</td>
						</tr>';
					}
				}
			}
			if($value['contacted']!= '0000-00-00 00:00:00')
			{
				$count_try_ppc_total_contacted++;
				if(strpos($ppc_values, 'not_found')===false)
				{
					$count_try_ppc_no_click_contacted++;
					$try_ppc_table .= '
					<tr class="ppc_contact_no_info_row ppc_contact_total_row">
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td>'.$value['firstname'].' '.$value['lastname'].'</td>
						<td>'.$value['phone'].'</td>
						<td>'.$value['distro_code'].'</td>
					</tr>';
				}else
				{
					$count_try_ppc_contacted++;
					$try_ppc_table .= '
					<tr class="ppc_contact_info_row ppc_contact_total_row">
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td>'.$value['firstname'].' '.$value['lastname'].'</td>
						<td>'.$value['phone'].'</td>
						<td>'.$value['distro_code'].'</td>
					</tr>';
				}
			}else
			{//no demo

			}
			}//from try.poslavu.com
			if($merc_dist){
				$count_merc_total++;
				if($value['contacted'] != '0000-00-00 00:00:00')
				{
					$count_merc_contacted++;
					
				}
				$merc_total_table .= '
					<tr>
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td>'.$value['firstname'].' '.$value['lastname'].'</td>
						<td>'.$value['phone'].'</td>
						<td>'.$value['distro_code'].'</td>
					</tr>';


			}//merc_dist

			//non-lavu leads
			if($value['license_applied'] == '0000-00-00 00:00:00' && $value['made_demo_account'] != '0000-00-00 00:00:00'){
				$non_lavu++;
				$count_non++;

				//add to non_lavu
				$non_lavu_table .= '
				<tr>
					<td>'.$value['company_name'].'</td>
					<td>'.$value['city'].' '.$value['state'].'</td>
					<td>'.$value['firstname'].' '.$value['lastname'].'</td>
					<td>'.$value['phone'].'</td>
					<td>'.$value['distro_code'].'</td>
				</tr>';
				if($value['lead_source'] == 'mercury'){
					$count_try_non++;
					$try_non_lavu_table .= '
					<tr>
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td>'.$value['firstname'].' '.$value['lastname'].'</td>
						<td>'.$value['phone'].'</td>
						<td>'.$value['distro_code'].'</td>
					</tr>';
				}
				if($merc_dist){
					$count_merc_non++;
					$merc_non_lavu_table .='
					<tr>
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td>'.$value['firstname'].' '.$value['lastname'].'</td>
						<td>'.$value['phone'].'</td>
						<td>'.$value['distro_code'].'</td>
					</tr>';
				}
				//echo '--'.$value['data_name'].', '.$value['company_name'].', '.$value['id'].'<br />';
			}
			if($value['license_applied'] != '0000-00-00 00:00:00')
			{
				if($merc_dist)
				{
					$count_merc_converted++;
					$merc_converted_table.='
					<tr>
						<td>'.$value['company_name'].'</td>
						<td>'.$value['city'].' '.$value['state'].'</td>
						<td>'.$value['created'].'</td>
						<td>'.$value['phone'].'</td>
						<td>'.$value['distro_code'].'</td>
					</tr>';
				}
			}


			//total

		}
		foreach($all_leads_with_demos as $key=>$value){
			//echo $value['data_name'].', '.$key.', '.$value['disabled'].'<br />';
			if($value['disabled']){
				$cancelled++;
			}else{
				$active++;
			}

		}
		//converted
		/*
		foreach($processed as $key=>$value){
			$count_converted++;
			$distro = $value['distro_code'];
			$special_json = $resellers[$distro]['special_json'];
			$accepts_mercury = $json_decode->decode($special_json);
			$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
			//echo $value['lead_source'].'<br />';

			if($value['demo_type'] == 'signups'){
				$count_signups_demo++;
			}else{
				$count_distro_demo++;
			}

			$converted_table .= '
				<tr>
					<td>'.$value['company_name'].'</td>
					<td>'.$value['city'].' '.$value['state'].'</td>
					<td>'.$value['firstname'].' '.$value['lastname'].'</td>
					<td>'.$value['phone'].'</td>
					<td>'.$value['distro_code'].'</td>
				</tr>';
			if($value['lead_source'] == 'mercury'){
				$count_try_converted++;
				$try_converted_table .= '
				<tr>
					<td>'.$value['company_name'].'</td>
					<td>'.$value['city'].' '.$value['state'].'</td>
					<td>'.$value['firstname'].' '.$value['lastname'].'</td>
					<td>'.$value['phone'].'</td>
					<td>'.$value['distro_code'].'</td>
				</tr>';


			}
			if($merc_dist){
				$count_merc_converted++;
				$merc_converted_table .= '
				<tr>
					<td>'.$value['company_name'].'</td>
					<td>'.$value['city'].' '.$value['state'].'</td>
					<td>'.$value['firstname'].' '.$value['lastname'].'</td>
					<td>'.$value['phone'].'</td>
					<td>'.$value['distro_code'].'</td>
				</tr>';
			}
			<div class="see_report" report_id="#table_contacted_div">'.$count_contacted.'</div>
		}
		*/

		//<img src="./images/fakerealitybro.jpeg" alt="graphs bro" style="width:350px;height:225px;"/>
		$report_div = '
		<div id="distro_reports">
			<div style="text-align:left;margin:5px 20px 5px 20px;font:24px verdana;color:#439789;">REPORTING</div>
			<div style="float:left;width=200px;margin-top:100px;display:none;">
				<div id="bad_numnums" style="float:left;width:180px;margin:50px;"></div>
				<div id="bad_leads" campaign="mercury" style="cursor:pointer;with:100px;border:1px solid blue;background:pink;margin-left:75px;margin-top:200px;width:100px;margin-bottom:10px;">See Non-Counted</div>
			</div>
			<div id="display_graph" style="width:200px;margin:100px auto 0px auto;padding:10px;border:1px solid black;cursor:pointer">DISPLAY GRAPH</div>
			<div id="graphalaphigus" style="width:800px;height:300px;position:relative;top:75px;left:70px;">
			</div>
			
			<div id="summary_numbers">
				<div class="report_summary" style="text-align:left;margin-left:20px;">
					<strong>LEGEND:</strong>
					<div class="'.$show_merc.'">TOTAL::</div>
					<div>SINCE TRY PPC::</div>
					<div>PPC NO INFO::</div>
					<div>PPC INFO::</div>
					<div>TO MERCURY::</div>
				</div>
				
				<div class="report_summary"><strong>Total Leads:</strong>
					<div class="see_report '.$show_merc.'" report_id="#table_total_div">'.$count_total.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_total_row">'.$count_try_total.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_total_no_info_row">'.$count_try_ppc_no_click.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_total_info_row">'.$count_try_ppc.'</div>
					<div class="see_report" report_id="#table_merc_total_div">'.$count_merc_total.'</div>
				</div>
				<div class="report_summary"><strong>Made Contact:</strong>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_contact_total_row">'.$count_try_ppc_total_contacted.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_contact_no_info_row">'.$count_try_ppc_no_click_contacted.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_contact_info_row">'.$count_try_ppc_contacted.'</div>
					<div class="see_report" report_id="#table_try_total_div">'.$count_merc_contacted.'</div>
				</div>
				<div class="report_summary"><strong>Had Trial:</strong>
					<div class="see_report '.$show_merc.'" report_id="#table_non_lavu_div">'.$count_non.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_demo_total_row">'.$count_try_ppc_demo_total.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_demo_no_info_row">'.$count_try_ppc_no_click_demo.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_demo_info_row">'.$count_try_ppc_demo.'</div>
					<div class="see_report" report_id="#table_merc_non_lavu_div">'.$count_merc_non.'</div>
				</div>
				<div class="report_summary" style="border-right:1px solid gray;"><strong>Converted:</strong>
					<div class="see_report '.$show_merc.'" report_id="#table_converted_div">'.$count_converted.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_sold_total_row">'.$count_try_ppc_sold_total.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_sold_no_info_row">'.$count_try_ppc_no_click_sold.'</div>
					<div class="see_report" report_id="#table_try_total_div" start_rows="ppc_sold_info_row">'.$count_try_ppc_sold.'</div>
					<div class="see_report" report_id="#table_merc_converted_div">'.$count_merc_converted.'</div>
				</div>
				<div class="report_summary '.$show_merc.'" ><strong>Demo Type:</strong>
					<div>Gave CC info:'.$count_signups_demo.'</div>
					<div>Distro Demos:'.$count_distro_demo.'</div>
			</div>
			</div>
			<div style="margin: 45px auto 20px; height:100px;">
				<div style="margin:15px;">Narrow time to:: </div>
				<select>
					<option value="boob">boob</option>
					<option value="peepee">peepee</option>
					<option value="poopoo">poopoo</option>
				</select>
				<label for="date_start">Start Date::</label>
				<input type="input" id="date_start_mercury" class="date_selection" name="date_start_mercury" value="-null-"/>
				<label for="date_end">End Date::</label>
				<input type="input" id="date_end_mercury" class="date_selection" name="date_end_mercury" value="-null-"/>
				<div id="mercury_time_range_submit" style="padding:10px;border:1px solid black;cursor:pointer;margin:20px auto;width:100px;background:#aecd37;">Submitski</div>
			</div>
			<div id="merc_time_table_div" style="margin: 20px auto;display:none;">
				<table id="merc_time_table">
				</table>
			</div>
			
		';
		/*$report_div = '
		<div id="distro_reports">
			<div style="text-align:left;font:24px verdana;color:#439789;">REPORTING</div>
			<div id="summary_numbers">
				<div class="report_summary" style="text-align:left;margin-left:20px;">
					<strong>LEGEND:</strong>
					<div class="'.$show_merc.'">TOTAL::</div>
					<div>SINCE TRY PPC::</div>
					<div>PPC NO INFO::</div>
					<div>PPC INFO::</div>
					<div>TO MERCURY::</div>
				</div>
				<div class="report_summary"><strong>Total Leads:</strong>
					<div class="see_report '.$show_merc.'" report_id="#table_total_div">'.$count_total.'</div>
					<div class="see_report" report_id="#table_try_total_div">'.$count_try_total.'</div>
					<div class="see_report" report_id="#table_merc_total_div">'.$count_merc_total.'</div>
				</div>
				<div class="report_summary"><strong>Had Trial:</strong>
					<div class="see_report '.$show_merc.'" report_id="#table_non_lavu_div">'.$count_non.'</div>
					<div class="see_report" report_id="#table_try_non_lavu_div">'.$count_try_non.'</div>
					<div class="see_report" report_id="#table_merc_non_lavu_div">'.$count_merc_non.'</div>
				</div>
				<div class="report_summary" style="border-right:1px solid gray;"><strong>Converted:</strong>
					<div class="see_report '.$show_merc.'" report_id="#table_converted_div">'.$count_converted.'</div>
					<div class="see_report" report_id="#table_try_converted_div">'.$count_try_converted.'</div>
					<div class="see_report" report_id="#table_merc_converted_div">'.$count_merc_converted.'</div>
				</div>
				<div class="report_summary '.$show_merc.'" ><strong>Demo Type:</strong>
					<div>Gave CC info:'.$count_signups_demo.'</div>
					<div>Distro Demos:'.$count_distro_demo.'</div>
				</div>
			</div>
		';*/
		$total_table .= '</tbody></table></div>';
		$report_div .= $total_table;
		
		$contacted_table .= '</tbody></table></div>';
		$report_div .= $contacted_table;
		

		$try_ppc_table .= '</tbody></table></div>';
		$report_div .= $try_ppc_table;

		
		/*
		$try_total_table .= '</tbody></table></div>';
		$report_div .= $try_total_table;
		*/
		
		$merc_total_table .= '</tbody></table></div>';
		$report_div .= $merc_total_table;
		
		$converted_table .= '</tbody></table></div>';
		$report_div .= $converted_table;
		
		$try_converted_table .= '</tbody></table></div>';
		$report_div .= $try_converted_table;
		
		$merc_converted_table .= '</tbody></table></div>';
		$report_div .= $merc_converted_table;
		
		$non_lavu_table .= '</tbody></table></div>';
		$report_div .= $non_lavu_table;
		
		$try_non_lavu_table .= '</tbody></table></div>';
		$report_div .= $try_non_lavu_table;
		
		$merc_non_lavu_table .= '</tbody></table></div>';
		$report_div .= $merc_non_lavu_table;

		$time_frame_table .= '</tbody></table></div>';
		$report_div .= $merc_non_lavu_table;
		
		$report_div .= '</div>';
		return $report_div;


		/*
		echo '<br />total leads::'.$stats['total_leads'].'<br />';
		echo 'leads wif demos:: '.$stats['leads_with_demos'].'<br />';
		echo 'completed_leads:: '.$stats['completed_leads'].'<br />';
		echo 'non-lavu::'.$non_lavu.'<br />';
		echo 'active::'.$active.'<br />';
		echo 'cancelled::'.$cancelled.'<br />';
		*/

		//return $stats;
	}//report_numbers
	//for now, It will just return a string that lists all of the values
	private static function find_ppc_values($notes_field){
		//$values = array();
		$ppc_string = '';
		$notes_explode_1 = explode('*|*',$notes_field);
		foreach($notes_explode_1 as $note){
			if(strstr($note, 'medium::')){
				$ppc_string = strstr($note, 'medium::');
				return $ppc_string;
				//error_log('distro_tab_container.find_ppc_values:: '.$ppc_string);
				//$ppc_array = explode('::', $ppc_string);
				//error_log('distro_tab_container.find_ppc_values:: 0->'.$ppc_array[0].'=='.$ppc_array[1].', 2->'.$ppc_array[2].'=='.$ppc_array[3]);
				//error_log('distro_tab_container.find_ppc_values:: 4->'.$ppc_array[4].'=='.$ppc_array[5].', 6->'.$ppc_array[6].'=='.$ppc_array[7]);
			}
		}
		return $ppc_string;

	}//find_ppc_values
	private static function next_seven_days_of_week($today){
		$week_ahead = array();
		$week_ahead[0] = $today;
		$week_ahead['d0'] = date('m/d', mktime(0, 0, 0, date("m")  , date("d")+$i, date("Y")));
		//echo $today.'<br />';
		for($i=1; $i<7;$i++){
			$day = date('l', mktime(0, 0, 0, date("m")  , date("d")+$i, date("Y")));
			$daydate= date('m/d', mktime(0, 0, 0, date("m")  , date("d")+$i, date("Y")));
			//echo $day.'<br />';
			$week_ahead[$i] = $day;
			$week_ahead['d'.$i] = strtolower($daydate);
		}
		//echo date('l', mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))).'<br />';
		//echo date('l', mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))).'<br />';
		return $week_ahead;
	}//next_seven_days_of_week
	public static function is_admin() {
		return (isset($_SESSION['distro_logged_in_is_admin']) && $_SESSION['distro_logged_in_is_admin']);
	}
	public static function view_point_breakdown($admin, $reseller_name){
		
		if($admin ==''){
			//echo 'no admin<br />';
			$admin = 0;
		}
		//echo '-'.$admin.'-';
		$licenses_top_edit_points .= '
				 <a onclick="show_points_log();" style="cursor:pointer;">View</a>
				<script type="text/javascript">
					function show_points_log() {
						var jelement = $("#distro_points_show_points");
						var jparent = jelement.parent();
						var jhistory = jelement.find(".distro_points_history_container");
						var jerror = jhistory.find(".error");
						jelement.css({ top: (jparent.offset().top-jelement.height()/2), left: (jparent.position().left-jelement.width()/2) });
						jerror.html("");
						jelement.show();
						load_distro_points_history(jelement, jhistory, jerror);
					}
					function hide_points_log() {
						var jelement = $("#distro_points_show_points");
						jelement.hide();
					}
					function load_distro_points_history(jelement, jhistory, jerror) {
						$.ajax({
							cache: false,
							async: true,
							url: "./exec/show_log_of_distro_points.php",
							data: { distro_code: "'.$reseller_name.'", width: 600, height: 265, admin:'.$admin.' },
							type: "POST",
							success: function(message) {
								var parts = message.split("[*note*]");
								var command = parts[0];
								var note = parts[1];
								if (command=="print error") {
									jerror.html("");
									jerror.css({ color: "red", opacity: 0 });
									jerror.append(note);
									jerror.stop(true, true);
									jerror.animate({ opacity: 1 }, 300);
								} else {
									jhistory.html("");
									jhistory.css({ opacity: 0, margin: "auto", width: "auto" });
									jhistory.append(note);
									jhistory.stop(true, true);
									jhistory.animate({ opacity: 1 }, 300);
								}
							},
							error: function(a,b,c) {
								jerror.css({ color: "red", opacity: 0 });
								jerror.html("");
								jerror.append("Connection to the server failed. Please refresh and try again.");
								jerror.stop(true, true);
								jerror.animate({ opacity: 1 }, 300);
							},
						});
					}
					function show_applicable_special_licenses(license_sendval) {
						var license_type = license_sendval.split(" - ")[0];
						var jdiv = $("#buy_special_licenses");
						var a_spec_lics = JSON.parse(jdiv.find("input[name=available_special_licenses_json]").val().replace(/\'/g, \'"\'));
						while (jdiv.find("ul").length > 0)
							jdiv.find("ul").remove();
						var a_options = [{ "code":"<ul class=\'special_license_description\'><li>No Special License Pricing</li><li style=\'display:none;\' class=\'id\'><table><tr><td class=\'name\'>id</td><td class=\'value\'>0</td></tr></table></li></ul>", "id":"0", "discount":0 }];
						
						// remember the original price
						var original_license_price = 0;
						if (!window.original_license_pricing)
							window.original_license_pricing = {};
						if (!window.original_license_pricing[license_type]) {
							original_license_price = parseFloat(license_sendval.split(" - ")[1].split("$")[1].replace(/,/g, ""));
							window.original_license_pricing[license_type] = original_license_price;
						}
						original_license_price = window.original_license_pricing[license_type];
						
						// create the descriptions
						$.each(a_spec_lics, function(k,v) {
							var b_matches = false;
							$.each(v.lic_types, function(a,b) {
								if (b == license_type && license_type != "")
									b_matches = true;
							});
							if (b_matches) {
								var a_list_items = [];
								var o_display = {
									"prefix" : { "name":"Applicable Accounts", "value":(v.prefix+"...") },
									"discount_percentage" : { "name":"% Discount", "value":v.discount_percentage },
									"na": { "name":"Purchased", "value":(v.original_quantity-v.quantity) },
									"quantity" : { "name":"Remaining", "value":v.quantity }
								};
								$.each(o_display, function(attr,props) {
									a_list_items.push("<li><table><tr><td class=\'name\'>"+props.name+":</td><td class=\'value\'>"+props.value+"</td></tr></table></li>");
								});
								a_list_items.push("<li style=\'display:none;\' class=\'id\'><table><tr><td class=\'name\'>id</td><td class=\'value\'>"+v.id+"</td></tr></table></li>");
								a_options.push({ "code":"<ul class=\'special_license_description\'>"+a_list_items.join("")+"</ul>", "id":v.id, "discount":parseInt(o_display.discount_percentage.value) });
							}
						});
						
						// create the selection
						if (a_options.length == 1)
							return;
						var selection_text = "<ul class=\'special_license_selection\'>";
						$.each(a_options, function(k,v) {
							selection_text += "<li><table><tr><td><input type=\'radio\' name=\'special_license_selection_radio\' value=\'"+v.id+"\' style=\'width:auto;\' onchange=\'change_special_license_price("+original_license_price+","+v.discount+");\' /></td><td>"+v.code+"</td></tr></table></li>";
						});
						selection_text += "</ul>";
						jdiv.append(selection_text);
						jdiv.show();
						setTimeout(function() {
							$($("input[name=special_license_selection_radio]")[0]).click();
						}, 100);
					}
					function change_special_license_price(original_price, discount) {
						var jselect = $("#license_license");
						var options = jselect.find("option");
						var joption = null;
						for (var i = 0; i < options.length; i++) {
							if (options[i].value == jselect.val())
								joption = $(options[i]);
						}
						for (var i = 0; i < 2; i++) {
							var val = (i == 0 ? joption.val() : joption.html());
							var name = val.split(" - ")[0];
							var new_price = original_price*(100-discount)/100;
							val = name+" - $"+number_format(new_price);
							if (i == 0)
								joption.val(val);
							else
								joption.html(val);
						}
					}
					function number_format(value) {
						var dollars = parseInt((value+"").split(".")[0]);
						var cents = (value-dollars).toFixed(2);
						dollars = dollars+"";
						var dlength = dollars.length;
						var dstring = "";
						for (var i = 0; i < dlength; i++) {
							if (i%3 == 0 && i > 2)
								dstring = ","+dstring;
							dstring = dollars.substr(dlength-1-i, 1)+dstring;
						}
						return dstring + cents.substr(1,3);
					}
				</script>
				<style type="text/css">
					.popup_close_button {
						width:18px;
						height:18px;
						border:1px solid lightgray;
						background-color:gray;
						border-radius:20px;
						display:inline-block;
						background-image:url(\'/cp/images/close-16x16.png\');
					}
					.popup_close_button:hover {
						background-color:lightgray;
					}
					ul.special_license_selection {
						display: inline-block;
						list-style-type: none;
						margin:16px 0 0 16px;
						padding:0;
					}
					ul.special_license_selection li {
						display: block;
					}
					ul.special_license_description {
						display: inline-block;
						list-style-type: none;
						margin:0 0 0 16px;
						padding:0;
					}
					ul.special_license_description li {
						display: inline-block;
						font-size: 15px;
					}
					ul.special_license_description td.name {
						font-weight:normal;
					}
					ul.special_license_description td.value {
						font-weight:bold;
					}
				</style>
				<div style="display:none; width:600px; height:300px; border:1px solid black; position:absolute; top:0px; left:0px; padding:15px; background-color:white; color:black;" id="distro_points_show_points">
					<div onclick="hide_points_log();" style="width:75px; cursor:pointer;" onmouseover="$(this).children(\'div\').css({ \'background-color\':\'lightgray\', \'border-color\':\'gray\' });" onmouseout="$(this).children(\'div\').css({ \'background-color\':\'gray\', \'border-color\':\'lightgray\' });"><div class="popup_close_button">&nbsp;</div> <font class="popup_box_text">Close</font></div><br />
					<div style="margin:0 auto; width:60px;" class="distro_points_history_container">
						<div style="width:60px; height:80px;">&nbsp;</div>
						<div style="position:absolute; left:30px; top:50px;" class="error">There was an error loading this information. Please refresh and try again.</div>
						<div style="margin:0 auto; width:60px; height:60px; padding:0; background-image:url(\'/cp/images/loading_60x.gif\');">&nbsp;</div>
					</div>
				</div>';//license_top_edit_points;
				return $licenses_top_edit_points;
	}//view_point_breakdown
	private static function get_state_input(){
	   //<input type="text" name="info_request_state" id="info_request_state" placeholder="state"/><br />


	    /*<div class="select_arrow_bg" style="width:90px; margin:2px 0;height:24px;" >
		<select name="info_request_state" size="1" style="width:90px;" >
		<option value="">-- CANADA --</option>
		<option value="AB">Alberta</option>
		<option value="BC">British Columbia</option>
		<option value="MB">Manitoba</option>
		<option value="NB">New Brunswick</option>
		<option value="NF">Newfoundland and Labrador</option>
		<option value="NT">Northwest Territories</option>
		<option value="NS">Nova Scotia</option>
		<option value="NU">Nunavut</option>
		<option value="ON">Ontario</option>
		<option value="PE">Prince Edward Island</option>
		<option value="PQ">Quebec</option>
		<option value="SK">Saskatchewan</option>
		<option value="YT">Yukon Territory</option>*/
		$states = '
		<option selected value="">Choose State</option>
		<option value="ALL">All States</option>
		<option value="East">East Region</option>
		<option value="Midwest">Midwest Region</option>
		<option value="South">South Region</option>
		<option value="West">West Region</option>
		<option value="AL">AL</option>
		<option value="AK">AK</option>
		<option value="AZ">AZ</option>
		<option value="AR">AR</option>
		<option value="CA">CA</option>
		<option value="CO">CO</option>
		<option value="CT">CT</option>
		<option value="DE">DE</option>
		<option value="DC">DC</option>
		<option value="FL">FL</option>
		<option value="GA">GA</option>
		<option value="HI">HI</option>
		<option value="ID">ID</option>
		<option value="IL">IL</option>
		<option value="IN">IN</option>
		<option value="IA">IA</option>
		<option value="KS">KS</option>
		<option value="KY">KY</option>
		<option value="LA">LA</option>
		<option value="ME">ME</option>
		<option value="MD">MD</option>
		<option value="MA">MA</option>
		<option value="MI">MI</option>
		<option value="MN">MN</option>
		<option value="MS">MS</option>
		<option value="MO">MO</option>
		<option value="MT">MT</option>
		<option value="NE">NE</option>
		<option value="NV">NV</option>
		<option value="NH">NH</option>
		<option value="NJ">NJ</option>
		<option value="NM">NM</option>
		<option value="NY">NY</option>
		<option value="NC">NC</option>
		<option value="ND">ND</option>
		<option value="OH">OH</option>
		<option value="OK">OK</option>
		<option value="OR">OR</option>
		<option value="PA">PA</option>
		<option value="RI">RI</option>
		<option value="SC">SC</option>
		<option value="SD">SD</option>
		<option value="TN">TN</option>
		<option value="TX">TX</option>
		<option value="UT">UT</option>
		<option value="VT">VT</option>
		<option value="VA">VA</option>
		<option value="WA">WA</option>
		<option value="WV">WV</option>
		<option value="WI">WI</option>
		<option value="WY">WY</option>';
		return $states;
	}//get_state_input

	// //
	// //get/set license variables
	// //
	//public
}//class tab_container{}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



