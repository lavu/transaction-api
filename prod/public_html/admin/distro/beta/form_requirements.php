<?php
	session_start();

	// import a bunch of arrays
	require(dirname(__file__)."/form_arrays.php");
	require_once(dirname(__FILE__)."/distro_data_functions.php");
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();

	// for buying a new license
	$loggedin = DistroSessionStats::getLoggedin();
	$lpercent = DistroSessionStats::getLpercent();

	// day-related promotions
	// NOTE: promo names cannot contain any dashes (-) in them
	$i_license_commission = NULL;
	$i_upgrade_commission = NULL;
	$s_license_promotional_name = '';
	$s_upgrade_promotional_name = '';
	$s_license_promotional_expiration = '';
	$s_upgrade_promotional_expiration = '';
	$i_4july_promo = array('start'=>'2013-07-04', 'end'=>'2013-07-14', 'license_commission'=>50, 'promo_name'=>'J4 Promo');
	$a_day_promos = array($i_4july_promo);

	// find any promos for today
	$i_now = strtotime(date('Y-m-d'));
	foreach($a_day_promos as $a_day_promo) {
		if (strtotime($a_day_promo['start']) <= $i_now && strtotime($a_day_promo['end']) >= $i_now) {
			if (isset($a_day_promo['license_commission'])) {//} && ((int)$a_day_promo['license_commission'] > $i_license_commission || $i_license_commission === NULL)) {
				$i_license_commission = (int)$a_day_promo['license_commission'];
				$s_license_promotional_name = $a_day_promo['promo_name'];
				$s_license_promotional_expiration = $a_day_promo['end'];
			}
			if (isset($a_day_promo['upgrade_commission'])) {//} && ((int)$a_day_promo['upgrade_commission'] > $i_upgrade_commission || $i_upgrade_commission === NULL)) {
				$i_upgrade_commission = (int)$a_day_promo['upgrade_commission'];
				$s_upgrade_promotional_name = $a_day_promo['promo_name'];
				$s_upgrade_promotional_expiration = $a_day_promo['end'];
			}
		}
	}

	// get the string replacement arrays
	$s_silver_name = $o_package_container->get_printed_name_by_attribute('name', $a_highest_license_names_mapping['Silver']);
	$s_gold_name = $o_package_container->get_printed_name_by_attribute('name', $a_highest_license_names_mapping['Gold']);
	$a_replace_from = array($s_silver_name." to ", $s_gold_name." to ");
	$a_replace_to = array($s_silver_name." (or Silver) to ", $s_gold_name." (or Gold) to ");

	$license_select = array();//want this
	$license_select[] = "";
	for($i=0; $i<count($a_license_types); $i++)
	{
		$a_license_type = $a_license_types[$i];
		$s_package_type = $a_license_types[$i][0];
		$b_promo_applied = isset($a_license_types[$i]['promo_applied']) && $a_license_types[$i]['promo_applied'] == TRUE;
		$s_promo_name = '';
		$s_promo_printed_name = '';
		$s_promo_expiration = '';

		// calculate the commission based on the distro's current commission and any promos
		if ($o_package_container->get_package_status($s_package_type) == 'upgrade') {
			$lpercent_promo = $lpercent;
			if ($i_upgrade_commission !== NULL && $i_upgrade_commission > $lpercent_promo && !$b_promo_applied) {
				$a_license_types[] = array_merge($a_license_type, array('promo_applied'=>TRUE));
				$lpercent_promo = $i_upgrade_commission;
				$s_promo_name = trim($s_upgrade_promotional_name);
				$s_promo_printed_name = ' '.$s_promo_name;
				$s_promo_expiration = $s_upgrade_promotional_expiration;
			}
			$lic_discount = ($a_license_types[$i][2] * 1) * ($lpercent_promo * 1 / 100);
		} else {
			$lpercent_promo = $lpercent;
			if ($i_license_commission !== NULL && $i_license_commission > $lpercent_promo && !$b_promo_applied) {
				$a_license_types[] = array_merge($a_license_type, array('promo_applied'=>TRUE));
				$lpercent_promo = $i_license_commission;
				$s_promo_name = trim($s_license_promotional_name);
				$s_promo_printed_name = ' '.$s_promo_name;
				$s_promo_expiration = $s_license_promotional_expiration;
			}
			$lic_discount = ($a_license_types[$i][2] * 1) * ($lpercent_promo * 1 / 100);
		}
		$lic_amount = round($a_license_types[$i][2] * 1 - $lic_discount,2);

		// generate the creation/upgrade printed names
		$a_license_types[$i][0] = $s_package_type.($s_promo_name == '' ? '' : '<*>'.$s_promo_name);
		$a_license_types[$i][1] = $lic_amount;
		$a_license_types[$i][3] = $s_promo_name;
		$a_license_types[$i][4] = $s_promo_expiration;
		$s_option = $s_package_type.($s_promo_name == '' ? '' : '<*>'.$s_promo_name). " - $" . number_format($lic_amount * 1,2);
		$s_option_printed = '';
		if ($o_package_container->get_package_status($s_package_type) == 'upgrade') {
			$s_printed_name = $o_package_container->get_upgrade_printed_name($s_package_type);
			if ($b_implement_platinum3)
                               $s_printed_name = str_replace($a_replace_from, $a_replace_to, $s_printed_name);
			$s_option_printed = $s_printed_name . $s_promo_printed_name . " - $" . number_format($lic_amount * 1,2);
		} else {
			$s_printed_name = $o_package_container->get_printed_name_by_attribute('name', $s_package_type);
			$s_option_printed = $s_printed_name . $s_promo_printed_name . " - $" . number_format($lic_amount * 1,2);
		}
		$license_select[] = array(
			$s_option,
			$s_option_printed,
		);
	}

	if(!function_exists("add_license_select_row"))
	{
		function add_license_select_row($ltype,$rtype)
		{
			if($rtype=="type")
			{
				return array($ltype['name'],$ltype['cost'],$ltype['value']);
			}
			else if($rtype=="select")
			{
				$lstr = $ltype['name'] . " - $" . number_format($ltype['cost'],2);
				return array($lstr,$lstr);
			}
			else return false;
		}
	}

	/*

		//-------------------------------------- Start July 4 Promo --------------------------------//
		$merica_start = mktime(0,0,0,7,4,2013);
		$merica_end = mktime(0,0,0,7,15,2013);

		if(time() > $merica_start && time() < $merica_end){

			$ltype = array("name"=>"Silver J4 Promo","cost"=>"447.50","value"=>"895.00");
			$a_license_types[] = add_license_select_row($ltype,"type");
			$license_select[] = add_license_select_row($ltype,"select");

			$ltype = array("name"=>"Gold J4 Promo","cost"=>"747.50","value"=>"1495.00");
			$a_license_types[] = add_license_select_row($ltype,"type");
			$license_select[] = add_license_select_row($ltype,"select");

			$ltype = array("name"=>"Platinum J4 Promo","cost"=>"1747.50","value"=>"3495.00");
			$a_license_types[] = add_license_select_row($ltype,"type");
			$license_select[] = add_license_select_row($ltype,"select");
		}
		//---------------------------------End July 4 Promo--------------------------------//
		//-----------------------------------------------------------------------------------------//*/

	// license types array format:
	// 0 - license name
	// 1 - license cost
	// 2 - license value
	// 3 - promotional name
	// 4 - promotion expiration
	$license_types = $a_license_types;
	$_SESSION['license_types'] = serialize($license_types);

	// for creating a new accound
	$s_new_account_label_name_options = "";
	foreach($a_new_account_label_names as $k=>$v) {
		$s_new_account_label_name_options .= "<option value='".$k."'>".$v."</option>";
	}
