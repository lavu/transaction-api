<?php

	require_once(dirname(__FILE__) . "/../../cp/resources/core_functions.php");
	require_once( dirname(__FILE__) . "/../../sa_cp/billing/package_levels_object.php");

	if (!isset($o_package_container))
		$o_package_container = new package_container();

	// Function declarations

	/**
	 * Validates the supplied promo code.
	 * Note: this was taken from ~/public_html/register/step2.php
	 * @param  string  $promo_code            Promo code supplied by the user
	 * @param  string  $license_name          Package level name of license to be discounted
	 * @param  string  $license_discount      Discount amount saved in variable that is passed by reference
	 * @param  boolean $unclaimed_codes_only  Whether promo codes should be checked against
	 *
	 * @return bool                    returns boolean indicating whether promo code is valid and will be accepted in process_promo_code()
	 */
	function check_promo_code($promo_code, $license_name, &$license_discount, $unclaimed_codes_only = true)
	{
		global $o_package_container;
		$code_accepted = false;
		$license_discount = 0;
		$license_display_name = strtolower($o_package_container->get_printed_name_by_attribute('name', $license_name));

		// NRA 2014 mailer promo
		if ( preg_match('/nra\s*14/i', $promo_code) || preg_match('/14\s*nra/i', $promo_code) )  // alexis said she's like to accept a few alternate forms of this promo code.
		{
			$code_accepted = true;
			switch ($license_display_name) {
				case 'silver':
					$license_discount = 100;
					break;
				case 'gold':
					$license_discount = 200;
					break;
				case 'pro':
					$license_discount = 400;
					break;
				default:
					break;
			}
		}
		// Mercury 25% off license promo
		else if ( preg_match('/^.[mM]..[25]..[pPrR0mM0]/', $promo_code) )
		{
			$sql = '';
			if ($unclaimed_codes_only) {
				// When someone's about to claim a promo code, we only want to validate promo codes against unclaimed ones (signified by a "blank" claimed_date).
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`promo_codes` WHERE `promo_code` = '[promo_code]' and `claimed_date` = '0000-00-00'";
			} else {
				// If we're just validating promo codes and getting the discount amount (like when invoicing for Billing), then we don't care about the claimed date.
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`promo_codes` WHERE `promo_code` = '[promo_code]'";
			}

			$results = mlavu_query( $sql, array('promo_code' => $promo_code) );
			$num_rows = ($results === FALSE) ? 0 : mysqli_num_rows($results);

			if (DEV) error_log( "DEBUG: sql={$sql} promo_code={$promo_code} num_rows={$num_rows} mlavu_dberror=". mlavu_dberror());  //debug

			if ( $num_rows >= 1 ) {
				$code_accepted = true;
				$license_discount = .25;
			}
		}
		// BlueStar Activation Codes
		else if ( preg_match('/^..[bBlLUueE].[1238][uUcC]./', $promo_code) )
		{
			$sql = '';
			if ($unclaimed_codes_only) {
				// When someone's about to claim a promo code, we only want to validate promo codes against unclaimed ones (signified by a "blank" claimed_date).
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`promo_codes` WHERE `promo_code` = '[promo_code]' and `promo_name` like 'bluestar%' and `claimed_date` = '0000-00-00'";
			} else {
				// If we're just validating promo codes and getting the discount amount (like when invoicing for Billing), then we don't care about the claimed date.
				$sql = "SELECT * FROM `poslavu_MAIN_db`.`promo_codes` WHERE `promo_code` = '[promo_code]' and `promo_name` like 'bluestar%'";
			}

			$results = mlavu_query( $sql, array('promo_code' => $promo_code) );
			$num_rows = ($results === FALSE) ? 0 : mysqli_num_rows($results);

			if (DEV) error_log( "DEBUG: sql={$sql} promo_code={$promo_code} num_rows={$num_rows} mlavu_dberror=". mlavu_dberror());  //debug

			if ( $num_rows >= 1 ) {
				$code_accepted = true;
				$license_discount = 1.0;  // 100%
			}
		}

		return $code_accepted;
	}

	/**
	 * Validates and proesses the supplied promo code.  Will modify $license_cost to reflect the discounted amount.
	 * Note: this is a slightly different version of process_promo_code() found in ~/public_html/register/auth_account.php
	 * @param  string  $promo_code     Promo code supplied by the user
	 * @param  array   $license_cost   Array containing 'amount' key to be discounted for valid promo codes
	 * @param  string  $license_name   License name
	 * @param  string  $distro_name    Distro username
	 * @param  string  $signupid       Signup ID
	 * @return bool                    returns boolean indicating whether code was accepted
	 */
	function process_promo_code($promo_code, $distro_name = '', $license_name, &$license_cost, $signupid = '')
	{
		$license_discount = 0.00;

		$code_accepted = check_promo_code($promo_code, $license_name, $license_discount);

		if (DEV) error_log( "DEBUG: Before - license_cost={$license_cost} license_name={$license_name} license_display_name={$license_display_name}" );  //debug

		if ($code_accepted) {
			// Percentage discount - need to support a $license_discount of 1.0 because it represents a 100% license discount
			if (floatval($license_discount) <= 1.0) {
				$promo_code_update_sql = "UPDATE `poslavu_MAIN_db`.`promo_codes` SET `claimed_date` = now(), `distro_name` = '[distro_name]' , `signupid` = '[signupid]' WHERE `claimed_date` = '0000-00-00' and `promo_code` = '[promo_code]'";
				mlavu_query($promo_code_update_sql, array('promo_code' => $promo_code, 'signupid' => $signupid, 'distro_name' => $distro_name));
				$affected_rows = ConnectionHub::getConn('poslavu')->affectedRows();
				if (DEV) error_log( "DEBUG: promo_code_update_sql={$promo_code_update_sql} promo_code={$promo_code} distro_name={$distro_name} affected_rows={$affected_rows}" );  //debug
				if (DEV) error_log( "DEBUG: mlavu_dberror=". mlavu_dberror());  //debug
				if ( $affected_rows === 1 ) {
					$license_cost = floatval($license_cost) - (floatval($license_cost) * floatval($license_discount));
				}
				else {
					$code_accepted = false;
				}
			}
			// Dollar amount price reduction
			else {
				$license_cost = floatval($license_cost) - floatval($license_discount);
			}
		}

		if (!$code_accepted) error_log( __METHOD__ . " received bad promo code on Distro Portal License tab from distro_name={$distro_name}: promo_code={$promo_code} license_name={$license_name} license_cost={$license_cost}" );

		return $code_accepted;
	}


	/**
	 * Marks the supplied promo code as not taken.
	 * @param  string  $promo_code     Promo code supplied by the user
	 * @param  string  $distro_name    Distro username
	 * @return bool                    returns boolean indicating whether code was accepted
	 */
	function liberate_promo_code($promo_code, $distro_name)
	{
		$code_liberated = false;

		$promo_code_update_sql = "UPDATE `poslavu_MAIN_db`.`promo_codes` SET `claimed_date` = '0000-00-00', `distro_name` = '', `signupid` = '' WHERE `promo_code` = '[promo_code]' and `distro_name` = '[distro_name]' limit 2";
		mlavu_query($promo_code_update_sql, array('promo_code' => $promo_code, 'distro_name' => $distro_name));
		$affected_rows = ConnectionHub::getConn('poslavu')->affectedRows();

		if (DEV) error_log( "DEBUG: promo_code_update_sql={$promo_code_update_sql} promo_code={$promo_code} distro_name={$distro_name} affected_rows={$affected_rows}" );  //debug
		if (DEV) error_log( "DEBUG: mlavu_dberror=". mlavu_dberror());  //debug

		if ( $affected_rows === 1 ) {
			$code_liberated = true;
		}
		else if ( $affected_rows > 1 ) {
			$code_liberated = true;
			error_log( __METHOD__ . " unexpectedly marked multiple promo codes as unused for promo_code={$promo_code} distro_name={$distro_name}" );
		}

		return $code_liberated;
	}



?>
