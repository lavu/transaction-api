<?php

	require_once(dirname(__FILE__)."/../../sa_cp/billing/package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();
	if (!isset($maindb) || $maindb == '') $maindb = 'poslavu_MAIN_db';

	// returns an array ('extra_upgrades'=>array(...), 'extra_resellers'=>array(...), 'id'=>`payment_status`.`id`)
	function get_extra_upgrades_and_resellers_and_id($s_dataname) {
		global $maindb;
		$payment_status_query = mlavu_query("SELECT `id`,`extra_upgrades_applied`,`extra_reseller_ids` FROM `[maindb]`.`payment_status` WHERE `dataname`='[dataname]'",
			array("maindb"=>$maindb, "dataname"=>$s_dataname));
		if (!$payment_status_query)
			return "payment status doesn't exist";
		if (mysqli_num_rows($payment_status_query) == 0)
			return "payment status doesn't exist";
		$a_payment_status = mysqli_fetch_assoc($payment_status_query);
		if (strlen($a_payment_status['extra_upgrades_applied']))
			$a_extra_upgrades_applied = explode('|', $a_payment_status['extra_upgrades_applied']);
		else
			$a_extra_upgrades_applied = array();
		if (strlen($a_payment_status['extra_reseller_ids']))
			$a_extra_reseller_ids = explode('|', $a_payment_status['extra_reseller_ids']);
		else
			$a_extra_reseller_ids = array();
		return array('extra_upgrades'=>$a_extra_upgrades_applied, 'extra_resellers'=>$a_extra_reseller_ids, 'id'=>$a_payment_status['id']);
	}

	// function relevant to finding or setting data for reseller licenses
	class RESELLER_LICENSES {

		// splits the upgrades and licenses applied into arrays
		// @$s_upgrades: should be the string coming from `payment_status`.`extra_upgrades_applied`
		// @$s_reseller_ids: should be the string coming from `payment_status`.`extra_reseller_ids`
		// returns the values in $a_extra_upgrades_applied and $a_extra_reseller_ids
		public static function get_extra_upgrades_from_upgrade_string($s_upgrades, $s_reseller_ids, &$a_extra_upgrades_applied, &$a_extra_reseller_ids) {
			if ($s_upgrades != '') {
				$a_extra_upgrades_applied = explode('|', $s_upgrades);
				$a_extra_reseller_ids = explode('|', $s_reseller_ids);
				foreach($a_extra_upgrades_applied as $k=>$v)
					$a_extra_upgrades_applied[$k] = (int)$v;
			} else {
				$a_extra_upgrades_applied = array();
				$a_extra_reseller_ids = array();
			}
		}

		// returns the name of the package from `reseller_licenses`.`type` for the given id,
		// or '' if that id can't be found
		public static function get_package_name_by_license_id($license_applied) {
			$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `id`='[1]'",$license_applied);
			if(mysqli_num_rows($lic_query))
			{
				$lic_read = mysqli_fetch_assoc($lic_query);
				$s_type = $lic_read['type'];
				return $s_type;
			}
			return '';
		}

		// returns the level (int) of the package of a given reseller_license_id
		// returns 0 on error, or $license_applied is not a 'package' (aka, 'none' or 'upgrade')
		public static function get_package_level_by_license_id($license_applied) {
			global $o_package_container;

			if ($license_applied == '')
				return 0;
			$s_package_name = self::get_package_name_by_license_id($license_applied);
			if ($s_package_name == '')
				return 0;
			if ($o_package_container->get_package_status($s_package_name) != 'package')
				return 0;
			return $o_package_container->get_level_by_attribute('name', $s_package_name);
		}

		// returns the level (int) of the upgrade_to package of a given reseller_license_id
		// returns 0 on error, or $license_applied is not an 'upgrade'
		public static function get_upgrade_level_by_license_id($i_upgrade_id) {
			global $o_package_container;

			if ($i_upgrade_id == '')
				return 0;
			$s_package_name = self::get_package_name_by_license_id($i_upgrade_id);
			if ($s_package_name == '')
				return 0;
			if ($o_package_container->get_package_status($s_package_name) != 'upgrade')
				return 0;
			$a_upgrade_names = $o_package_container->get_upgrade_old_new_names($s_package_name);
			$i_retval = $o_package_container->get_level_by_attribute('name', $a_upgrade_names[1]);
			return $i_retval;
		}

		// returns the highest package from the list the (current package, package of applied_upgrades) as an integer or
		// returns teh value in $package if an error occurs
		public static function choose_upgrades_for_packages($package, $a_license_upgrades) {
			global $o_package_container;

			if (!is_array($a_license_upgrades) || count($a_license_upgrades) == 0)
				return $package;
			foreach($a_license_upgrades as $i_license_upgrade) {
				$s_upgrade_name = self::get_package_name_by_license_id($i_license_upgrade);
				if ($s_upgrade_name === '')
					continue;
				$a_upgrade_names = $o_package_container->get_upgrade_old_new_names($s_upgrade_name);
				$s_new_package_name = $a_upgrade_names[1];
				$new_package = $o_package_container->get_level_by_attribute('name', $s_new_package_name);
				if ($new_package === 0)
					continue;
				$package = self::choose_package_for_packages($package, $new_package);
			}

			return $package;
		}

		// returns the packages with greater value, as determined by package_levels_object->compare_package_by_level
		// be default, returns the value in $package1 if either of the given packages don't exist
		public static function choose_package_for_packages($p1_level, $p2_level) {
			global $o_package_container;

			if ($o_package_container->get_value_by_attribute('level', (int)$p2_level) != 0 &&
				$o_package_container->get_value_by_attribute('level', (int)$p1_level) != 0 &&
				$o_package_container->compare_package_by_level((int)$p2_level, (int)$p1_level) == 1)
				return $p2_level;
			return $p1_level;
		}

		// convenience function
		// returns the package with the greatest value of the given package, the `license_applied`, and the `extra_upgrades_applied`
		// if the licenses applied don't exist, it returns $package
		public static function choose_package_from_payment_status_read($package, $a_payment_status_read) {

			// compare to `license_applied`
			$i_license_level = self::get_package_level_by_license_id($a_payment_status_read['license_applied']);
			$package = self::choose_package_for_packages($package, $i_license_level);

			// compare to `extra_upgrades_applied`
			$a_extra_upgrades_applied = array();
			$a_extra_reseller_ids = array();
			self::get_extra_upgrades_from_upgrade_string($a_payment_status_read['extra_upgrades_applied'], $a_payment_status_read['extra_reseller_ids'], $a_extra_upgrades_applied, $a_extra_reseller_ids);
			$package = self::choose_upgrades_for_packages($package, $a_extra_upgrades_applied);

			return $package;
		}

		// returns either '' or the id of the license trying to be applied
		public static function get_license_id_from_level($s_distro_code, $s_license_name) {
			global $maindb;
			$s_retval = '';
			$license_id_query = mlavu_query("SELECT `id` FROM `[maindb]`.`reseller_licenses` WHERE `resellername`='[resellername]' AND `type`='[type]' ORDER BY `restaurantid`,`applied` ASC",
				array("maindb"=>$maindb, "resellername"=>$s_distro_code, "type"=>$s_license_name));
			if ($license_id_query) {
				while ($license_id_read = mysqli_fetch_assoc($license_id_query)) {
					$s_id = $license_id_read['id'];
					if ($s_id != '') {
						$s_retval = $s_id;
						if ($license_id_read['restaurantid'] == '' && $license_id_read['applied'] == '')
							break;
					}
				}
			}
			return $s_retval;
		}

		// given the row from `reseller_licenses`,
		// returns the notes as an array or
		// returns array() on error
		public static function get_notes($a_license) {
			// check for errors
			if (!is_array($a_license) || !isset($a_license['notes']))
				return array();

			// get the notes
			$a_notes = explode('|', $a_license['notes']);

			// remove empty notes
			$a_to_remove = array();
			foreach($a_notes as $k=>$v)
				if ($v === '')
					$a_to_remove[] = $k;
			foreach($a_to_remove as $k)
				unset($a_notes[$k]);

			return $a_notes;
		}

		// modifies the given license to add the given note to $a_license['notes']
		// returns TRUE on success or FALSE on error
		public static function apply_note(&$a_license, $s_note) {
			if (!is_array($a_license) || $s_note === '')
				return FALSE;
			$a_notes = self::get_notes($a_license);
			$a_notes[] = $s_note;
			$a_license['notes'] = implode('|', $a_notes);
			return TRUE;
		}

		// used to get the entry from `reseller_licenses` with the matching $s_license_id and `reseller_name`
		// returns the result as an array or
		// "fail|failure message"
		public static function get_license_from_id($s_license_id,$a_distro_read) {
			global $maindb;
			global $o_package_container;

			if($s_license_id!="")
			{
				$lic_query_string = "select * from `[maindb]`.`reseller_licenses` where `resellername`='[resellername]' and `id`='[id]'";
				$lic_query_vars = array("maindb"=>$maindb,"resellername"=>$a_distro_read['username'],"id"=>$s_license_id);
				$lic_query = mlavu_query($lic_query_string,$lic_query_vars);
				/*foreach($lic_query_vars as $k=>$v)
					$lic_query_string = str_replace("[$k]", $v, $lic_query_string);
				error_log($lic_query_string);*/
				if($lic_query !== FALSE && mysqli_num_rows($lic_query) > 0)
				{
					$lic_read = mysqli_fetch_assoc($lic_query);
					$s_package_status = $o_package_container->get_package_status($lic_read['type']);
					error_log("DEBUG: s_package_status={$s_package_status} lic_read[type]={$lic_read['type']} s_license_id={$s_license_id}");  //debug
					if ($s_package_status == "upgrade") {
						$a_upgrade = $o_package_container->get_upgrade_old_new_names($lic_read['type']);
						$lic_read['upgrade_from'] = $a_upgrade[0];
						$lic_read['upgrade_to'] = $a_upgrade[1];
					} else if ($s_package_status == "package") {
						$lic_read['upgrade_from'] = '';
						$lic_read['upgrade_to'] = '';
					} else {
						return 'fail|The license type could not be determined';
					}
					return $lic_read;
				}
				else
				{
					return 'fail|Unable to find the license for this distributor';
				}
			}
			else
			{
				return 'fail|Unable to find the license for this distributor at this level';
			}
		}

	}

?>
