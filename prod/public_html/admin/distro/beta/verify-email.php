<?php
function verifyEmail($toemail, $fromemail, &$details = null ) {
    $valid_email_regex = "#[a-z0-9!\#\\$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!\#\\$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?#iu";
	//echo $valid_email_regex;
    if ( !preg_match($valid_email_regex, $toemail) ) {
        $details[] = "Invalidly formatted email";
        return false;
    } //else return true;
	//TODO everything below doesn't work in PHP 5.1
    $email_arr = explode("@", $toemail);
    $domain = array_slice($email_arr, -1);
    $domain = $domain[0];
    // Trim [ and ] from beginning and end of domain string, respectively
    $domain = ltrim($domain, "[");
    $domain = rtrim($domain, "]");
    $details = array();
    if( "IPv6:" == substr($domain, 0, strlen("IPv6:")) ) {
        $domain = substr($domain, strlen("IPv6") + 1);
    }

    $mxhosts = array();
    getmxrr($domain, $mxhosts, $mxweight);

    if(!empty($mxhosts) ) {
    	$mx_ip = $mxhosts[array_search(min($mxweight), $mxhosts)];
    } else {
        $record_a = dns_get_record($domain, DNS_A);
        if( empty( $record_a ) ) {
            $record_a = dns_get_record($domain, DNS_AAAA);
        }
        if( !empty($record_a) )
            $mx_ip = $record_a[0]['ip'];
        else {
            $result   = false;
            $details[] = "No suitable MX records found.";

            return $result;
        }
    }

    $connect = @fsockopen($mx_ip, 25);
    $out = fgets($connect, 1024);
    $details = array();
    $result=false;
    $HTTP_HOST = "register.poslavu.com"; 
    if($connect){ 
        if(preg_match("/^220/", $out )){
            $send = "HELO $HTTP_HOST\r\n";
            fputs ($connect , $send ); 
            $out = fgets ($connect, 1024);
            $details[] = $out."\n";
            
            $send = "MAIL FROM: <$fromemail>\r\n";
            
            fputs ($connect , $send ); 
            $from = fgets ($connect, 1024);
            $details[] = $from."\n";
            
            $send = "RCPT TO: <$toemail>\r\n";
            fputs ($connect , $send ); 
            $to = fgets ($connect, 1024);
            $details[] = $to."\n";
            $send = "QUIT";
            fputs ($connect , $send);
            fclose($connect);

            if(!preg_match("/^250/", $from) || !preg_match("/^250/", $to)){
                $result = false; 
            }
            else{
                $result = true;
            }
        } 
    }
    else{
        $result = false;
        $details []= "Could not connect to server";
    }
    return $result;
}