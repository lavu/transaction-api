Lavu is implementing new pricing and paying cash residuals!:*:2015-11-02 16:10:15:*:Check your email! A new Lavu Newsletter has been sent out. Pick a webinar that will work best for you and let us know! You don't want to miss this!|Version 3.0.2:*:2015-08-17 08:00:35:*:We are excited to announce the release of the latest update to the Lavu app. Version 3.0.2 will be available in the App Store on August 16. 

Version 3.0.2 will include:
Numerous bug fixes
Performance improvements
Optimized stability

A bug was discovered during the testing of 3.0.2. For some accounts, line item menu prices are not displaying on iPhones and iPod Touches. Order totals are unaffected by this bug and displayed totals are accurate. iPad's and iPad mini's are unaffected. Printed receipts are also unaffected. 

Please remind customers not to update the app immediately before or during operating hours.|Specialist Alert!:*:2015-08-10 07:58:19:*:Apple will release their newest operating systems: OS X 10.11 and iOS 9, sometime next month. To ensure a smooth transition, please inform your customers 
NOT TO UPDATE devices using Lavu apps to iOS 9 or OS X 10.11 “El Capitan” at this time. To avoid accidental updates on iOS devices, we suggest turning off Automatic Updates on devices running Lavu apps. Go to Settings > iTunes and App Store > Automatic Downloads > toggle Apps and Updates to OFF. |Test Flight!:*:2015-03-30 08:32:09:*:If you are interested in participating in Test Flight, please email your Apple ID to specialists@poslavu.com. Check out and test Lavu 3.0!|Lavu Advance 2015:*:2015-03-19 08:54:35:*:Hello Lavu Specialists! A special thanks to those who joined us last weekend for our 3rd annual conference. We are very excited about Lavu 3.0! More information will be coming out via newsletters. Keep an eye out!|Connectivity:*:2015-01-16 20:55:11:*:We are aware of the issue that was affecting our customer’s abilities to connect to our cloud services.

The issue has been corrected and we are monitoring our services to prevent any other interruptions.

If your clients have not been able to connect, please have them “Reload Settings” on all of their devices to reconnect to the cloud at this time. If they continue to experience issues please have them contact Lavu Support at 1.855.528.8457 or by email at support@poslavu.com.

We will be providing additional information regarding this issue as soon as possible.
|Newsletters:*:2014-10-02 15:51:02:*:If you are not receiving the Specialists Newsletters, please email Specialists@poslavu.com to be put on the list! Make sure you do this ASAP!|@@@@@:*:2014-09-03 14:05:14:*:@@@@@|July 1st to August 31st Sales Incentive!:*:2014-09-02 17:50:57:*:We are now at the end of our sales incentive that ran from July 1st to August 31st! 

Sell 3 active licenses and receive 895 points!
Sell 5 active licenses and receive 1495 points!
Sell 8 active licenses and receive 2495 points!

If you have sold 3 or more licenses during this time, please email us with a list of the account names to whom you sold the licenses. This email should be sent to specialists@poslavu.com. These will be verified and points will be distributed with in 30 days of the email. 

Keep your eye out for further incentives coming soon!

|ATTENTION - Lavu Local Server Customers:*:2014-05-06 16:56:02:*:As we announced yesterday, POS Lavu version 2.3.6 is now in the App Store. Please DO NOT update your clients iOS devices with version 2.3.6 of the POS Lavu app until the clients have received the latest Lavu Local Server (LLS) update.

-How can you tell if client needs update?

If the Mac mini running Lavu Local Server software has been updated in the past TWO WEEKS, you require this LLS update to use POS Lavu version 2.3.6


|Version 2.3.5!:*:2014-02-13 17:36:39:*:Valentine’s Day is one of the busiest days, if not THE busiest day, for many restaurants. We are excited to inform you that the first week of our version 2.3.5 release has been a smooth roll out. So far, ⅔ of Lavu users who have updated to this version and have seen minimal issues, all of which have a minor workaround. “Version 2.3.5 has proven to be a very solid build” says Lucas Lisitza, our Lavu Support Manager.  Lucas has also made it known that Lavu will be staffed appropriately for the Valentine’s Day rush to assist with any unforeseen circumstances that may arise.  

Keep up the great work and Happy Valentine’s Day!
|Specialist Newsletter Clarification:*:2014-02-07 18:14:32:*:We would like to take a second to further explain the reason behind the remarks made in the latest Specialist Newsletter regarding the selling of competing POS solutions. We understand that businesses grow and their focus can change constantly.  Sometimes what was once a positive endeavor or profitable sales vertical for a company may no longer continue to be so. As we make headway in to 2014, we will be making some decisions on our behalf to ensure that we are following the best course possible to not only ensure positive growth for ourselves, but for our dedicated resellers as well. We understand that there are other solutions out there that play in our same market space and that it can be tempting to work with a few of them.  But we feel, that it would be in everyone’s best interest, to pick one and fully commit and back that solution. We also understand that that solution may not be us, and that’s ok. So please, take a moment to evaluate your partnership with Lavu and ask yourself if we are still a good fit for you.  There are a lot of good things coming down the pipe over the next month including a whole new reporting suite of tools and the next version (2.3.5) of POS Lavu.  We are going to be supplying our specialists with a few new opportunities and solutions that will enable sales into new verticals such as fitness/gym locations, hotels, and churches.  We think that 2014 can and will be a very lucrative year for all of us.  Thank you for your understanding, and we hope to see you all at our conference in April.
|Specialist Newsletters:*:2014-01-08 13:32:33:*:If you are not receiving the Specialists Newsletters, please email Specialists@poslavu.com to be put on the list! Make sure you do this ASAP!|Pizza Creator!:*:2013-12-04 18:13:16:*:Hello Specialists!

The New Lavu Pizza Creator will be coming out of beta release very soon! This will need to be enabled by user to configure. Please see Settings > Pizza Settings.

Help Center documentation will be available soon. 

-Specialist Team|Platinum Final Purchase Date:*:2013-11-13 11:26:36:*:Hello Specialists,

You may notice that we sent word out that the last day to purchase a Platinum license is the 19th of November.  As of that date, the only options will be: Silver, Gold, and Pro.  This is for new signups only and is for end users only.  As for you Specialists, we are still going to offer both license types until the January 7th date that we recently spoke about in our last newsletter.  Again, this is for new signups, looking to make the purchase on their own and will not affect your purchase dates outlined in the newsletter.  If you have any questions, please contact me (Kalynn) at: specialists@poslavu.com.

Regards,

Kalynn
|New Documents Added!:*:2013-10-11 13:14:46:*:Heads Up Specialist! We have added two more documents to further assist you! You will now find a  Users Guide for our software as well as a Setup Guide for the KDS Pro in the Sales Tools Tab! It's live now, check it out!|Newsletters:*:2013-09-11 15:50:19:*:If you are not receiving the Specialists Newsletters, please email Specialists@poslavu.com to be put on the list! Make sure you do this ASAP!|Note ALL LEADS:*:2013-09-10 15:01:17:*:Just a reminder to all of our Lavu Specialists- Please be sure to note ALL LEADS, this includes leads that you have declined. We need to make sure that any issues are addressed and that we are handling the leads in the most effective way. Thank you all for your continued awesomeness!!|Auto Close Bug:*:2013-08-30 17:59:51:*:In regards to a recent bug that was causing the orders to auto-close and clear the items from the order:                                             
                                                                                           
Our developers have been working at resolving this issue and, with the help of our dedicated clients and awesome resellers providing information as it was happening, they were able to pin point the problem, A change has been made that will take effect immediately with our clients using the cloud. Our LLS clients should have an update available next week.
Thank you for all your patience and hard work!! |TAKE NOTES on EVERY Lead!:*:2013-04-22 13:28:21:*:Be sure to add notes to every lead assigned to you, regardless of whether or not it became a successful conversion to Lavu.  The notes help us understand how we can improve our sales process and increase the quality of leads we assign.  

So take a few extra seconds to let us know how it's going with every lead.  A little communication goes a long way towards giving you more  and keeping everything more efficient between Lavu, our partners, and our awesome distros.|Removing Old Leadsaaa:*:2013-04-16 19:12:46:*:We wanted to remind everyone to use the "Remove" button to remove any old leads that did not work out. Please don't do this until you are sure that the lead will not be interested in Lavu. Thanks!|Mercury Campaign:*:2013-03-26 18:05:56:*:USA RESELLERS, DO YOU WANT TO RECEIVE HOT LEADS FROM THE MERCURY PPC CAMPAIGN?

We are updating the list of Distros who are open to receiving Free Leads from the Mercury/Lavu marketing campaign. Go to the My Info tab to set your Mercury status.

WHAT IS THIS TRY.LAVU CAMPAIGN ALL ABOUT?

We have built a lead generation campaign with our preferred processing partner, Mercury Payment Systems. Mercury is financing an ongoing Google Search Engine Pay Per Click marketing campaign to promote try.poslavu.com.

	•	Every lead that comes from this campaign will be assigned to a Distributor 

	•	Since Mercury is paying for these leads, we must offer Mercury as the processor. 


Some Distros have arrangements with other processors and may initially think they don't want an account that does not use their processing, but we would like to suggest that these are still paying customers (monthly fees/points) and good to accept either way. In fact, they may require less support since processing questions would direct to Mercury. Considering the cost, time, and energy it can take to gather leads, this is a great opportunity to build your customer base.


PS: All non-Mercury generated leads will still be sent to Distros regardless of your choice to be on this list.|Distro News & Updates:*:2013-03-21 00:02:49:*:Over the next few weeks we are going to be sending out a bunch of newsletters with important information and updates. As a distributor it is your job to keep up to date on the latest distributor program information.

We have noticed that there is a significant percentage of distributors that are not reading the information that we are sending out. Those who choose not to keep themselves up to date on the program may be asked to no longer participate in the program.

Please make sure that you are reading the newsletters, your email address is up to date, you check the mail at that address regularly and that you have any spam filters set to allow all mail from lavu.com and poslavu.com.

Thanks!|UPGRADES AND LEADS!!!:*:2013-03-08 12:39:48:*:By the end of today, Friday 3/8/2013, All Lavu distributors will be able to purchase licenses in the LICENSES tab, and apply them in the ACCOUNTS tab.   All distributors will also be ABLE TO APPLY THESE LICENSES TO UPGRADE ACCOUNTS!!! New leads will start to be assigned to distributers soon. 