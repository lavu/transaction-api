<?php
/***
* pre_login.php
* author: Martin Tice
* property of: LAVU inc.
*   This will be used to display initial information to potential new distributors.
*   The Idea is that There will be information on all 3 Types, (Local, Intnl, Sales only)
*   Along with respective agreement forms. 
*   
*   Once the forms are filled out, they will receive an email with a username and password. 
*
*****/
ini_set("display_errors", '1');



$head_tag = '<head>';
	$head_tag .= '<link rel="stylesheet" href="./distro_stylesheets/pre_login.css" type="text/css" />';
	$head_tag .= '<script type="text/javascript">'.file_get_contents("/home/poslavu/public_html/admin/distro/beta/distro_scripts/lavuLog.js").'</script>';
	$head_tag .= '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>';
	$head_tag .= '<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>';
	$head_tag .= '<script type="text/javascript" src="./distro_scripts/pre_login.js"></script>';
	$head_tag .= '<script type="text/javascript" src="./distro_scripts/jquery.corner.js"></script>';	
	$head_tag .= '<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/themes/smoothness/jquery-ui.css"></link>';
	
$head_tag .= '</head>';

$body_tag = '<div id="everything">'.get_everything().'</div>';//</div id="everything">

//
//echo everything++++++++++++++++++++++++
echo $head_tag;
echo $body_tag;
//+++++++++++++++++++++++++++++++++++++++
//

/**
*Returns all the markup that is not inside the <head> tag
*
**/
function get_everything(){
	$everything = '';
	$everything .= get_top();
	$everything .= get_explanation();
	$everything .= get_dist_choices();
	$everything .= get_form();
	return $everything;
};//get_everything()

/**
*Returns markup for header
**/
function get_top(){
	$header = '';
		$header .= '<div id="header_section">
		<div id="middle_top">
			<div id="logo">
				<img src="./images/logo_distro_medium.png">
			</div>
			<div id="title_top">
				<h1>Certified Lavu Distributor Signup</h1>
			</div>
		</div>';//middle_top
		$header .= '</div>';//header_section
		$header .= '<div style="background-image:url(./images/line_g.png);height:1px;"></div>';
		$header .= '<div style="background-image:url(./images/shadowline.png);width:960px;height:20px;margin:0 auto;"></div>';		
	return $header;
	
};//get_top()


function get_explanation(){
	$expl = '';
	$expl .= '<div id="explanation">
			<div id="instructions">
				<p><b>Place instructions on how to use this thing here.</b></p><br/>
				<p>Step 1: Pick one of these things.</p>
				<p>Step 2: Fill out form and agree to terms.</p>
				<p>Step 3: You will be given access to distro portal.</p>
				<p>Step 4: Follow further instructions when you log in.</p>
			</div>
		</div>';
	$expl .= '<div style="background-image:url(./images/line_g.png);height:1px;"></div>';
	$expl .= '<div style="background-image:url(./images/shadowline.png);width:960px;height:20px;margin:0 auto;"></div>';		
	return $expl;
}
/**
* box that holds container for each dist type
**/
function get_dist_choices(){
	$choices = '<div id="dist_choices">';
		$choices .= '<div id="first_choice" class="d_choice gray-grad">';
		$choices .= get_local();
		$choices .= '</div><div class="choice_divider"></div>';
		$choices .= '<div id="second_choice" class="d_choice gray-grad">';		
		$choices .= get_sales_only();
		$choices .= '</div><div class="choice_divider"></div>';		
		$choices .= '<div id="third_choice" class="d_choice gray-grad">';		
		$choices .= get_intnl();		
		$choices .= '</div><br style="clear: left;">';
	$choices .= '</div>';//<div id="dist_choices">
	return $choices;
};//get_dist_choices()


//////////////////////////////All DISTRO TYPE SECTIONS
/**
*local section
**/
function get_local(){
	//description of local distributor
	$local_desc = '<p class="relationship">
		Distributors have an ONGOING relationship with their customers</p>
		<p class="desc_par">
		Lavu Distributors are responsible for marketing of Lavu, sales, billing, and <span class="relationship">full ongoing support</span>
		for their customers. There is a minimum monthly sales requirement to insure active Distributors are part of our Network.<br/><br/>
		1. Must have adequate IT support for your clients<br/>
		2. Current business liscense<br/>
		3. Attend Certification training(required to become certified and listed on our Distributor referal list on our website)</p>';
		
	$local_desc .= 	'<p class="agreement_link">*For more info on other requirements, please refer to our <a target="_blank" href="/assets/documents/DomesticReseller.pdf">DISTRIBUTER/RESELLER AGREEMENT</a></p>';
	
	
	//button to choose local distributor
	$choose_local = '<button id="select_local" class="choose_button">SELECT</button>';
	$select_button = '<div id="select_local" class="select_button">SELECT</div>';	
	
	$local = '<div id="local_section" class="selection_content">';
	$local .= $select_button;
		$local .= '<div id="local_header" class="dist_type_header">';
			$local .= 'LOCAL DISTRIBUTOR';
		$local .= '</div>';//<div id="local_header">
		
		$local .= $local_desc;
		//$local .= $choose_local;
	$local .= '</div>';//<div id="local_section">
	return $local;
};//get_local()

/**
*sales only section
**/
function get_sales_only(){
		//description of sales distributor
	$sales_desc = '<p class="relationship">'.
		'Sales-only Resellers have a ONE-TIME relationship with their customers.</p>'.
		'<p class="desc_par">'.
		'Lavu Sales-only Resellers are responsible for marketing of Lavu, sales and billing, providing initial installation spec sheets,'.
		' and to connect customer to Lavu Distributors and Support Team. There is a minimum monthly sales requirement to insure active'.
		' Resellers are part of our Network.<br/><br/>'.
		'1. Current business liscense<br/>'.
		'2. Attend Certification training(required to become certified and listed on our Distributor referal list on our website)<br/><br/>'.
		'*For more info on other requirements, please refer to our <a target="_blank" href="/assets/documents/DomesticReseller.pdf">SALES-ONLY RESELLER AGREEMENT</a></p>';
	
	// OLD button to choose sales distributor
	//$choose_sales = '<button id="select_local" class="choose_button" style="bottom:20px;">Select Sales-Only</button>';
	//NEW button div
	$select_button = '<div id="select_sales" class="select_button">SELECT</div>';
	
	$sales = '<div id="sales_section" class="selection_content">';
	$sales .= $select_button;
	$sales .= '<div id="sales_header" class="dist_type_header">';
			$sales .= 'SALES-ONLY DISTRIBUTOR';
		$sales .= '</div>';//<div id="sales_header">	
		$sales .= $sales_desc;
		//$sales .= $choose_sales;
	$sales .= '</div>';//<div id="sales_section">
	return $sales;
};//get_sales_only()

/**
*local section
**/
function get_intnl(){
		//description of intnl distributor
	$intnl_desc = '<p class="desc_par">'.
		'Lavu is available Worldwide. We welcome qualified agents to become Certified Lavu Distributers / Resellers.'.
		'The requirements are similar to USA Distributors, but may have subtle differences based on location.<br/><br/>'.
		'**For more info on international requirements  <a target="_blank" href="/assets/documents/DomesticReseller.pdf">click here</a></p>';
	
	//button to choose intnl distributor
	//$choose_intnl = '<button id="select_local" class="choose_button">Select International</button>';
	$select_button = '<div id="select_intnl" class="select_button">SELECT</div>';
	
	$intnl = '<div id="intnl_section" class="selection_content">';
	$intnl .= '<div id="select_local" class="select_button">SELECT</div>';
	
	$intnl .= '<div id="intnl_header" class="dist_type_header">';
			$intnl .= 'INTERNATIONAL DISTRIBUTOR';
		$intnl .= '</div>';//<div id="intnl_header">	
		$intnl .= $intnl_desc;
		//$intnl .= $choose_intnl;
	$intnl .= '</div>';//<div id="intnl_section">
	return $intnl;
};// get_intnl()


/**
* Form for information and agreement. 
**/
function get_form(){
	$form = '<div id="form_all">';
		$form .= '<ul class="dist_form">';
			// name
			$form .= '<li>';
				$form .= '<label for="name" class="form_label">Full Name';
					$form .= '<span class="required_field">*</span>';
				$form .= '</label>';
				$form .= '<div id="full_name" class="form_input_container">';
					$form .= '<span class="form_sublabel_container">';
						$form .= '<input id="first_name" class="form_input" type="text" name="first_name" size="10">';
						$form .= '<label id="sublabel_first" class="form_sublabel"> First Name </label>';
					$form .= '</span>';
					$form .= '<span class="form_sublabel_container">';
						$form .= '<input id="last_name" class="form_input" type="text" name="last_name" size="15">';
						$form .= '<label id="sublabel_last" class="form_sublabel"> Last Name </label>';
					$form .= '</span>';
				$form .= '</div>';//full_name
			$form .= '</li>';// name 
			
			//email
			$form .= '<li>';
				$form .= '<label for="email" id="email_label" class="form_label"> E-mail';
					$form .= '<span class="required_field">*</span>';
				$form .= '</label>';
				$form .= '<div id="email_container" class="form_input_container">';
					$form .= '<input id="email" class="form_input" type="text" name="email" size="20" placeholder="ex: name@example.com">';
				$form .= '</div>';//email_container
			$form .= '</li>';//email
			
			//terms of service
			$form .= '<li>';
				$form .= '<label for="tos" class="form_label">Terms and Conditions';
					$form .= '<span class="required_field">*</span>';
				$form .= '</label>';
				$form .= '<div id="tos_container" class="form_input_container">';
						$form .= '<span class="form_checkbox" style="clear:left;">';
							$form .= '<input id="tos_check" class="form_checkbox_req" type="checkbox" value="I have read and agree to the Terms and Conditions" name="tos_check">';
							$form .= '<label for="tos_check">';
								$form .= 'I have read and agree to the ';
								$form .= '<a title="Distributer Terms" target="new" href="">Terms and Conditions</a>';
							$form .= '</label>';
						$form .= '</span>';//form_checkbox
						$form .= '<span class="clearfix"></span>';
					$form .= '</div>';//tos_container
			$form .= '</li>';//t.o.s.			
		
		$form .= '</ul>';		
	$form .= '</div>';//<div id="form_all">	
	return $form;
};//get_form()


