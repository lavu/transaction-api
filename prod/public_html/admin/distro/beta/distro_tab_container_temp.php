<?php
	//session_start();

	require_once(dirname(__FILE__)."/distro_data_functions.php");
	require_once(dirname(__FILE__)."/timing_functions.php");
	require_once("/home/poslavu/public_html/admin/sa_cp/billing/package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();
/**
tab_container.php
author: Martin Tice
property of Lavu inc.

This file holds the classes for the tab contianer the user sees when logging in

**/
if (isset($_SESSION['update_accounts'])){
		//echo 'tab'.$_SESSION['update_accounts'];
	}
 
ini_set("display_errors",1);

class tab_container{
    private static $current_user; // contains all information on user (distro_user object)
    private static $view_markup; // this holds all the markup to be returned.
    private static $tab_list; // holds the markup for the list of tab sections and relative links
    private static $tab_sections ;//holds markup for each tab added. 
    private static $s_licenses_not_upgrades; // names of licenses the user has that aren't upgrades (as bar-delimeted string)
    
	public function __construct($user, $admin){
		global $o_package_container;
		//build the tab_container
		self::$current_user = $user;
		self::$tab_list = '<div id="tab-container" class="tab-container">
			<ul class="tabs">';
		self::$tab_sections = '<div id="panel-container">
		';
		
		$licenses = self::$current_user->get_licenses();
		$a_licenses_not_upgrades = array();
		foreach($licenses as $a_license) {
			$s_license_name = $a_license['type'];
			if ($o_package_container->get_package_status($s_license_name) != "upgrade")
				$a_licenses_not_upgrades[$s_license_name] = $s_license_name;
		}
		$s_licenses_not_upgrades = implode('|', $a_licenses_not_upgrades);
		self::$s_licenses_not_upgrades = $s_licenses_not_upgrades;
		
		if($admin == 'admin'){
			$this->build_admin_view();
		}else if($admin == 'mercury'){
			$this->build_mercury_view();
		}else{
			$this->build_distributor_view();
		}
		/*
		if($admin){
			//build admin view
		}else{
			// build distributor view
		}*/
	}
	
	// builds the fiew for the admin only
	private function build_admin_view(){
		//$this->build_header();(might do this before class instantiation)
		$this->build_modal_content();
		$resellers = self::$current_user->get_all_resellers();
		$this->build_header_admin();
		$this->add_news_tab();
		$this->add_leads_tab();
		$this->add_unassigned_tab($resellers);
		$this->add_resellers_tab($resellers);
		$this->add_accounts_tab();
		$this->add_resources_tab();
		$this->add_licenses_tab();
		$this->add_my_info_tab();
		$this->add_processed_tab();
		self::$tab_list .= '</ul>';
		self::$tab_sections .= '</div></div>';
		echo self::$tab_list;
		echo self::$tab_sections;
	}//build_admin_view
	
	// builds the tab view for distributor only
	private function build_distributor_view(){
		$this->build_modal_content();
		$this->build_header();
		$this->add_news_tab();
		$this->add_leads_tab();
		$this->add_accounts_tab();
		$this->add_resources_tab();
		$this->add_licenses_tab();		
		$this->add_my_info_tab();					
		self::$tab_list .= '</ul>';
		self::$tab_sections .= '</div></div>';		
		echo self::$tab_list;
		echo self::$tab_sections;
	}//build_distributor_view
	
	//builds the tab view for the mercury men
	private function build_mercury_view(){
		//$this->build_header();(might do this before class instantiation)
		$this->build_modal_content();
		self::$current_user->set_all_resellers();
		$resellers = self::$current_user->get_all_resellers();
		$this->build_header_mercury();
		//$this->add_news_tab();
		//$this->add_leads_tab();
		$this->add_unassigned_tab($resellers);
		//$this->add_resellers_tab();
		//$this->add_accounts_tab();
		//$this->add_resources_tab();
		//$this->add_licenses_tab();
		$this->add_my_info_tab();	
		self::$tab_list .= '</ul>';
		self::$tab_sections .= '</div></div>';
		echo self::$tab_list;
		echo self::$tab_sections;
	}//build_mercury_view
	
	
	//
	//
	//builds all of the content to be displayed as modal.
	// ex. forms, loading screens, etc.
	private function build_modal_content(){
	
		$is_admin = self::$current_user->get_info_attr('access');
		if($is_admin == 'admin'){
			//echo 'admin';
			self::$current_user->set_all_resellers();
		}
		
		
		$licenses = self::$current_user->get_licenses();
		$account_labels_options = self::$current_user->get_account_labels_options();
		$user = self::$current_user;
		$user_username = $user->get_info_attr('username');
		$resellers = $user->get_all_resellers();
		//var_dump($account_labels_options);

		//the background black opaque div.
		$modal_content	= '<div class="opaque_modal_bg"></div>';
		 
		function get_new_account_form($account_labels_options,$user){
			$account_form = '
				<div id="account_form_div" style="display:none;">
					<div id="success_account_form" style="display:none;text-align:center;"></div>
					<form id="new_account" method="post" action="">
						<div id="new_account_form_title" style="font:24px Verdana;color:#717074;text-align:left;">Create Account</div>
						<div id="new_account_form_required" style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>				
					<div>
						<fieldset>
							<label for="starter_menu">Default Menu*</label><br />
							<div class="select_arrow_bg_new_account">
								<select name="starter_menu" id="starter_menu">
									'.$account_labels_options.'
								</select>
							</div><br /><br />
							<label for="new_acc_address">Address*</label><br />
							<input type="text" name="new_acc_address" id="new_acc_address"/><br />
							<label for="new_acc_state">State/Province*</label><br />
							<input type="text" name="new_acc_state" id="new_acc_state" /><br />
							<label for="new_acc_first_name">First Name*</label><br />
							<input type="text" name="new_acc_first_name" id="new_acc_first_name" /><br />
							<label for="new_acc_email">Email*</label><br />
							<input type="text" name="new_acc_email" id="new_acc_email" />										
						</fieldset>
						<fieldset>
							<label for="new_acc_comp_name">Company Name*</label><br />
							<input type="text" name="new_acc_comp_name" id="new_acc_comp_name"/><br />
							<label for="new_acc_city">City*</label><br />
							<input type="text" name="new_acc_city" id="new_acc_city" /><br />
							<label for="new_acc_zip">Zip*</label><br />
							<input type="text" name="new_acc_zip" id="new_acc_zip" /><br />
							<label for="new_acc_last_name">Last Name*</label><br />
							<input type="text" name="new_acc_last_name" id="new_acc_last_name" /><br />
							<label for="new_acc_phone">Phone*</label><br />
							<input type="text" name="new_acc_phone" id="new_acc_phone" />
							<input type="hidden" name="distro_username" id="distro_username" value="'.$user->get_info_attr('username').'">
							<input type="hidden" name="distro_id" id="distro_id" value="'.$user->get_info_attr('id').'">
							<input type="hidden" name="distro_email" id="distro_email" value="'.$user->get_info_attr('email').'">
						</fieldset>
						<br style="clear:left;">
					</div>
					<div id="new_acc_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_new_account">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;" id="submit_new_account" />
					</div>
				</form>
				<div id="ok_acc_buttons" style="text-align:center;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_new_account">OK</span>
					</div>
			</div>';//account_form_div
			
			return $account_form;
		}
		
		function get_new_lead_form($user_username){
			//$user->get_info_attr('username');
			$new_lead_form = '
				<div id="new_lead_form_div" style="display:none;">
					<div id="success_lead_form" style="display:none;text-align:center;"></div>
					<form id="new_lead_form" method="post" action="">
						<div id="new_lead_form_title" style="font:24px Verdana;color:#717074;text-align:left;">Create New Lead</div>
						<div id="new_lead_form_required" style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>				
					<div>
						<fieldset>
						<label for="select_accept_lead">Do You wish to accept this lead?</label><br />
						<div class="select_arrow_bg_new_account">
								<select name="select_accept_lead" id="select_accept_lead">
									<option value="0">Accept</option>
									<option value="1">Decline</option>
								</select>
						</div><br /><br />
							<label for="new_lead_address">Address*</label><br />
							<input type="text" name="new_lead_address" id="new_lead_address"/><br />
							<label for="new_lead_state">State/Province*</label><br />
							<input type="text" name="new_lead_state" id="new_lead_state" /><br />
							<label for="new_lead_first_name">First Name*</label><br />
							<input type="text" name="new_lead_first_name" id="new_lead_first_name" /><br />
							<label for="new_lead_email">Email*</label><br />
							<input type="text" name="new_lead_email" id="new_lead_email" />										
						</fieldset>
						<fieldset>
							<label for="new_lead_comp_name">Company Name*</label><br />
							<input type="text" name="new_lead_comp_name" id="new_lead_comp_name"/><br />
							<label for="new_lead_city">City*</label><br />
							<input type="text" name="new_lead_city" id="new_lead_city" /><br />
							<label for="new_lead_zip">Zip*</label><br />
							<input type="text" name="new_lead_zip" id="new_lead_zip" /><br />
							<label for="new_lead_last_name">Last Name*</label><br />
							<input type="text" name="new_lead_last_name" id="new_lead_last_name" /><br />
							<label for="new_lead_phone">Phone*</label><br />
							<input type="text" name="new_lead_phone" id="new_lead_phone" />
							<input type="hidden" name="new_lead_distro_username" id="new_lead_distro_username" value="'.$user_username.'">
						</fieldset>
						<br style="clear:left;">
					</div>
					<div id="new_lead_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_new_lead">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;" id="submit_new_lead" />
					</div>
				</form>
				<div id="ok_lead_buttons" style="text-align:center;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_new_lead">OK</span>
					</div>
			</div>';//account_form_div
			
			/*
			<input type="hidden" name="distro_username" id="distro_username" value="'.$user->get_info_attr('username').'">
			<input type="hidden" name="distro_id" id="distro_id" value="'.$user->get_info_attr('id').'">
			<input type="hidden" name="distro_email" id="distro_email" value="'.$user->get_info_attr('email').'">
			*/
			
			return $new_lead_form;
		}
		
		function get_apply_license_form($licenses, $user){
			global $o_package_container;
			
			$options = '';
			$gold = 0;
			$silver = 0;
			$platinum = 0;
			$none_available = '<div>No licences available to apply. Please purchase in licenses tab</div>';
			foreach($licenses as $key=>$value){
					$type = $value['type'];
					$printed_name = preg_replace('/[0-9]/', '', $type);
					$options .= '<option value="'.$type.'">'.$printed_name.'</option>';
					if($printed_name == 'Gold'){
						$gold++;
					}else if($printed_name == 'Silver'){
						$silver++;
					}else if($printed_name == 'Platinum'){
						$platinum++;
					}
			}
				if($gold > 0 || $silver > 0 || $platinum > 0){
					$none_available = '';
				}
			/*if($d_name == null){
				$d_name = 'Benwa Balls';
				$current = 'Gold';
			}*/
			//<div><span>Current License</span><span>'.$Gold.'</span></div>
			$apply_lic_form = '
			<div id="apply_license_form_div" style="display:none;">
				<div id="success_apply_license_form" style="display:none;text-align:center;"></div>
				<form id="apply_license_form" method="post" action="">
					<div style="font:24px Verdana;color:#717074;text-align:left;">Apply License</div>
					<div style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					<div>
						<fieldset>
							<legend>Apply License To:
								<span id="apply_license_company_name"></span>
								<input type="hidden" name="apply_license_dname" id="apply_license_dname" value="" />
								<input type="hidden" name="available_licenses_to_apply" id="available_licenses_to_apply" value="" />
								<input type="hidden" name="licenseupgrade_string" id="licenseupgrade_string" value="" />
							</legend>
							
							<label for="apply_lic_available_select">Apply:</label>
							<div id="select_arrow_bg_apply_license">
								<select name="apply_lic_available_select" id="apply_lic_available_select">
								'.$options.'
								</select>
								<input type="hidden" name="distro_id" id="distro_id" value="'.$user->get_info_attr('id').'">
							</div>
						</fieldset>
						<fieldset>						
							<legend>Available Licenses</legend>
							<div>'.$silver.' Silver</div>
							<div>'.$gold.' Gold</div>
							<div>'.$platinum.' Platinum</div>'.$none_available.'
							
						</fieldset>
					</div>
					<div id="apply_license_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;" id="cancel_apply_license">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;" id="submit_apply_license" />								</div>
				</form>
				<div id="ok_app_button" style="text-align:center;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_app_license">OK</span>
					</div>		
			</div>';
			return $apply_lic_form;
		}
		
		function get_assign_lead_form($resellers){
			//var_dump($resellers);
			$options = '<option value=""></option>';
			//echo count($resellers);
			$json_decode = new Json();
			foreach($resellers as $key=>$value){
				$accepts_mercury = $json_decode->decode($value['special_json']);
				$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
				$merc_logo= '';
				if($merc_dist){
					//$merc_logo='<img src="./images/i_merc_mini.png" alt="mm" />';
					$merc_logo = 'MERCURY';
					//echo $merc_logo.", ".$value['f_name'].$value['l_name'].'<br />';
				}
				
				//echo 'username::'.$value['username'].'<br/>';//'<option value="'.$value.'">'.$value['firstname'].' '.$value['lastname'].'</option><br />';
				$options .= '<option value="'.$key.'">'.$value['f_name'].' '.$value['l_name'].', username: '.$value['username'].','.$value['city'].','.$value['state'].','.$value['postal_code'].', '.$merc_logo.'</option>';
			}
		
			$assign_lead_form = '
			<div id="assign_lead_form_div" style="display:none;">
				<div id="success_assign_lead_form" style="display:none;text-align:center;"></div>
				<form id="assign_lead_form" method="post" action="">
					<div style="font:24px Verdana;color:#717074;text-align:left;">Assign Lead</div>
					<div style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					<div>
						<fieldset>
							<legend>Assign Lead to Reseller</legend>
							<input type="hidden" id="assign_lead_lead_id" name="assign_lead_lead_id" value="" />
							<label for="all_reseller_select">*Choose Reseller:</label>
							<div id="select_arrow_bg_apply_license">
								<select name="all_reseller_select" id="all_reseller_select">
								'.$options.'
								</select>
						</fieldset>
					</div>
					<div id="assign_leads_buttons" style="text-align:right; margin-top:20px;">
						<span style="margin-right:10px;border:1px solid #717074;padding:10px;" id="cancel_assign_lead">Cancel</span>
						<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;" id="submit_assign_lead" />								</div>
				</form>
				<div id="ok_assign_button" style="text-align:center;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;display:none;" id="ok_assign_btn">OK</span>
				</div>		
			</div>';
			return $assign_lead_form;
		}
		
		function get_news_form(){
			$news_form = '
			<div id="news_form_div" style="display:none;">
				<div id="success_news_form" style="display:none;text-align:center;"></div>
					<form id="news_form" method="post" action="">
						<legend>ADD NEWS</legend>
						<div style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					
					
						<label for="add_news_title">*Title of News</label><br />
						<input type="text" name="add_news_title" id="add_news_title"/><br />
					
						<label for="add_news_content">*Content</label><br />
						<textarea name="add_news_content" id="add_news_content" style="width:300px;height:200px;"></textarea><br />
					
						<div id="add_news_buttons" style="text-align:right; margin-top:20px;">
							<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_add_news">Cancel</span>
							<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="submit_add_news" />
						</div>
						
					</form>
					<div id="ok_add_news" style="text-align:center;display:none;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;cursor:pointer;" id="ok_add_news_btn">OK</span>
					</div>
			</div>';
			return $news_form;
			
		}
		function get_lead_note_form(){
			$lead_note_form = '
			<div id="lead_note_form_div" style="display:none;">
				<div id="success_lead_note_form" style="display:none;text-align:center;"></div>
					<div style="float:left;width:350px;height:310px;">
					<form id="lead_note_form" method="post" action="">
						<legend>ADD NOTE</legend>
						<div style="font:10px Verdana;color:#717074;text-align:left;margin-bottom:10px;">Required Fields*</div>
					
					
						<label for="add_lead_note_title">*Title of Note</label><br />
						<input type="text" name="add_lead_note_title" id="add_lead_note_title"/><br />
					
						<label for="add_lead_note_content">*Content</label><br />
						<textarea name="add_lead_note_content" id="add_lead_note_content" style="width:300px;height:200px;"></textarea><br />
						<input type="hidden" name="lead_note_reseller" id="lead_note_reseller" value="" />
						<input type="hidden" name="lead_note_lead_id" id="lead_note_lead_id" value="" />
						<input type="hidden" name="lead_note_previous" id="lead_note_previous" value="" />
						<input type="hidden" name="function" id="lead_note_function" value="notes_icon_note" />
						<div id="add_lead_note_buttons" style="text-align:center; margin-top:20px;">
							<span style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="cancel_add_lead_note">Cancel</span>
							<input type="submit" style="margin-right:10px;border:1px solid #717074;padding:10px;cursor:pointer;" id="submit_add_lead_note" />
						</div>
						
					</form>
					</div>
					<div id="lead_notes_display" style="float:left;width:350px;height:375px;overflow-y:scroll">
					</div>
					<div id="ok_add_lead_note" style="text-align:left;display:none;">
						<span style="margin:10px auto;border:1px solid #717074;padding:10px;cursor:pointer;" id="ok_add_lead_note_btn">OK</span>
					</div>
			</div>';
			return $lead_note_form;
			
		}
		
		
		$modal_content .= get_new_account_form($account_labels_options,$user);
		$modal_content .= get_apply_license_form($licenses, $user);
		$modal_content .= get_new_lead_form($user_username);
		$modal_content .= get_assign_lead_form($resellers);
		$modal_content .= get_news_form();
		$modal_content .= get_lead_note_form();
		echo $modal_content;	
	}//build_modal_content
	//
	//
	//all of the functions that return tab contents
	private function add_news_tab(){
	
		$access = self::$current_user->get_info_attr("access");
		//echo $access;
		//gets the news from news.txt, wraps html around it, and displays
		function get_news(){
		
			$readHandle=fopen("./distro_news/news.txt", 'r');
			$contents = fread($readHandle, filesize("./distro_news/news.txt"));
			fclose($readHandle);
			
			$cont=explode("|", $contents);
			//$contents=$cont[1];
			$news_entries = '';
						
			foreach($cont as $entry){
				$seperated_entry = explode(":*:", $entry);
				$news_entries .= '<div class="news_entry">
					<div class="entry_title">'.$seperated_entry[0].'</div>
					<div class="entry_date">'.$seperated_entry[1].'</div>
					<div class="entry_body">'.$seperated_entry[2].'</div>					
					</div>';				
			}
			
			return $news_entries;
			
		}		
		// add content to tab_list
		$title ='<li class="tab" style="width:60px;"><a href="#news_tab">NEWS</a></li>
		';	
		
		// add content to tab_sections 	
		$content ='<div id="news_tab" class="tab_section tab00">';
		
		//top of of the tab
		$news_top = '
		<div class="tab_top">
			<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;margin-left:80px;">RESELLER NEWS</div>';
			if($access === 'admin'){
				$news_top .= '<div id="leads_top_right" style="text-align:right;width:150px;float:right;margin-right:60px;position:relative;padding-top:9px;">
						<div id="add_news" style="border:1px solid #999;padding:10px;width:100px;float:right;background:#aecd37;cursor:pointer;">ADD NEWS</div>
					</div>';
			}
		//$news_top .= '</div>';		
		
		$news_top .= '<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="b_w_f" /></div>
			</div>
		';//end news_tab_top
		/*<div id="news_tab_top" class="tab_top">
				<div class="tab_top_top">
					<span style="font:24px Verdana;color:#717074;">DISTRO NEWS</span>
				</div>';*/
		//body of the tab
		$news_body = '
		<div id="news_content">';
		$news_body .= get_news();
			
		$news_body .= '
		</div>';//end news_body
		
		$content .= $news_top;
		$content .= $news_body;	
		
		
		$content .= '
		</div>';//all_content;
		
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_news_tab
	private function add_leads_tab(){
		$leads = self::$current_user->get_leads();
		// add content to tab_list
		function count_new_leads($leads){
			$count = 0;
			foreach($leads as $dname=>$attrs){
			$ls = isset($attrs['lead_status'])?$attrs['lead_status']:'accept';			
				//echo 'lead_status::<br />'.$attrs['lead_status'].'<br /><br />';
				if($ls === 'accept'){
					//echo 'new lead::<br/>'.$attrs['company_name'].'<br /><br />';
					$count++;
				}
			}
			//echo 'total new::<br />'.$count.'<br /><br />';
			return $count;
		}
		function lead_color($stat, $div,$color_class){
		//echo 'stat='.$stat.' div='.$div.' cc='.$color_class;
			if($stat === $div){
				//echo '  true';
				return $color_class;
			}
		}
		function lead_type_content($level, $data_name, $s_licenses_string){
			if($level === 'accept'){
				return '<div style="margin:50px;font:12px Verdana;color:#717074;">
					<span style="margin-right:30px;">Do you accept this new account</span>
						<img src="./images/btn_accept.png" style="margin-right:10px;cursor:pointer;" class="accept_lead_confirm" alt="btn_accpt"/>
							<span class="accept_vert_div" style="border-right:1px solid #999;margin-right:10px;"></span>
						<img src="./images/btn_decline.png" style="cursor:pointer;" alt="btn_dec" class="decline_lead_confirm"/>
				</div>';
			}else if($level === 'contact'){
				$dname_nospace = str_replace(' ', '', $data_name);
				$dname_nospace = str_replace('\'', '', $dname_nospace);
				$dname_nospace = str_replace('"', '', $dname_nospace);
				$dname_nospace = str_replace('.', '', $dname_nospace);
				$dname_nospace = str_replace(',', '', $dname_nospace);
				return '
				<div>
					<form id="contact_form_'.$dname_nospace.'" method="post" action="">
						<div>Enter discription of how you contacted the customer</div>
						<div>
							<div style="margin:20px;">CONTACT METHOD::</div>
								<div style="width:150px;float:left;text-align:left;padding-top:25px;font:14px verdana;margin-left:255px;">
									<input type="radio" name="contact_method" value="phone" />PHONE<br />
									<input type="radio" name="contact_method" value="email" />EMAIL<br />
									<input type="radio" name="contact_method" value="face" />FACE TO FACE<br />
									<input type="radio" name="contact_method" value="other" />OTHER<br />
								</div>
							<textarea cols="410" rows="20" id="'.$dname_nospace.'_contact_text" name="contact_comment" style="width:225px;height:100px;float:left;margin:20px 10px 0px 20px 25px;background-color:#f1f5ee;">any comments here</textarea>
							<input type="hidden" name="company_name" value="'.$data_name.'" />
							<input type="hidden" name="progress" value="contacted" />
							<div style="margin-left:10px;border:1px solid #717074;padding:10px;cursor:pointer;float:left;" class="contact_level_submit" contact_form="#contact_form_'.$dname_nospace.'" >Submit</div>
							<br style="clear:left;">

					</div>
					</div></form>
				';
				
			}else if($level === 'demo_account'){
				return '
				<div style="font:12px Verdana;color:#717074;margin:20px 10px 50px;">
				<span>Setup a Demo account for this prospect: </span><img src="./images/btn_createdemo.png" alt="btn_c_d" class="demo_account_requested" style="cursor:pointer;"/></div>';
				
			}else{
				return '<div style="font:12px Verdana;color:#717074;margin:40px">
				<span>Select License to Apply:</span>
					<img src="./images/i_gold_tiny.png" alt="img_g_t"/>
				<span style="color:#aecd37;cursor:pointer;" class="apply_license_btn" val="'.$data_name.'">GOLD</span>
					<img src="./images/i_platinum_tiny.png" alt="img_p_t"/>
				<span style="color:#aecd37;cursor:pointer;" class="apply_license_btn" val="'.$data_name.'">PLATINUM</span>
					<input type="hidden" name="available_licenses" id="available_licenses" value="'.$s_licenses_string.'" />
				</div>
				';
			}
		}
		
		$new_lead_count = count_new_leads($leads);
		//<span><img src="./images/circ_leads.png"></span>	<span style="font-size:16px;text-align:center;">'.$new_lead_count.'</span>	
		$title ='<li class="tab" style="width:110px;"><a href="#leads_tab">
			<span style="background-image:url(./images/circ_leads.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$new_lead_count.'
			</span>
			LEADS</a></li>
		';
	
		// add content to tab_sections 	
		$content ='<div id="leads_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">NEW LEADS</div>
					<div id="unass_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;padding-top:25px;">
					<img src="./images/btn_create_lead.png" alt="btn_apply" class="new_lead_btn" style="cursor:pointer;" /></span>
					</div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
		$content .= '<div class="adjust_body"><div id="lead_desc">
			<span style="width:60px;">STATUS</span>
			<span style="width:153px;">BUSINESS NAME</span>
			<span style="width:180px;">LOCATION</span>
			<span style="width:169px;">EMAIL</span>
			<span style="width:137px;">NAME</span>
			<span style="width:120px;">PHONE</span>
			<span style="width:72px;">NOTES</span>
			<span style="width:50px;">REMOVE</span>
			<br style="clear:left;" />														
		</div>
		';
//beginning of accordian

		$content .= '<div id="lead_acc_wrapper">
		';
		
		foreach($leads as $key=>$attrs){
			$lead_created_through_signup = FALSE;
			$lead_stat = isset($attrs['lead_status'])?$attrs['lead_status']:'accept';
			if(isset($attrs['already_made_demo_account'])) $made_demo_account =  $attrs['already_made_demo_account']; else $made_demo_account = FALSE;
			$lead_level = isset($attrs['level']) ? $attrs['level'] : '';
			if ($made_demo_account && in_array($lead_stat, array('accept', 'contact')))
				$lead_created_through_signup = TRUE;
			$lead_created_through_signup_message = '';
			//echo $lead_stat.'==><br/> ';
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_'.$lead_stat;
			if ($lead_created_through_signup) $circ_image_ref .= '_has_demo_account';
			if ($lead_created_through_signup) $lead_created_through_signup_message .= '<div style="margin:0 auto;padding:20px;font-size:12px;text-align:center;">Note: this account was created through the <a href="http://www.poslavu.com/en/ipad-pos-signup" target="_blank">POSLavu Signup Page</a> and already has a demo account.</div>';
			
			if($attrs['accepted'] == '0000-00-00 00:00:00'){
				$remove = '<span style="width:50px;text-align:center;></span>';
			}else{
				$remove = '<span style="width:50px;text-align:center;cursor:pointer;" l_id="'.$attrs['id'].'" class="remove_lead"><img src="./images/btn_decline.png" alt="big_X" /></span>';
			}
			$lead_source = '';
			if($attrs['lead_source'] == 'mercury'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}
			
			$content .= '<div class="acc_head accordionButton">
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="./images/circ_red.png" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:150px;" class="lead_on_comp_name">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:170px;">'.$attrs['zip'].' - '.$attrs['city'].'</span><div class="loc_head_div">|</div>
				<span style="width:170px;">'.$attrs['email'].'</span><div class="loc_head_div">|</div>
				<span style="width:120px;">'.$attrs['firstname'].' '.$attrs['lastname'].'</span><div class="loc_head_div">|</div>
				<span style="width:110px;">'.$attrs['phone'].'</span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" style="width:40px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$remove.'
				'.$lead_source.'
				<br class="clear:left;">
				</div>		
				';
			$content .= '<div class="acc_lead_content accordionContent">
				<div class="lead_acc_content_head">
					<img src="./images/prog_bar_full.png" alt="p_b_f"/>
				</div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>'.
				$lead_created_through_signup_message.
				lead_type_content($lead_stat, $attrs['data_name']?$attrs['data_name']:$attrs['company_name'], self::$s_licenses_not_upgrades).'</div>
			';
				
		}
		$content .= '</div></div></div>';//lead_acc_wrapper and leads tab close baby
//end of accordian
		
// just a list of values for $this->leads;	
/*	
		$content .= '	<ul id="lead_list" style="overflow:scroll;height:320px;">';
		foreach($leads as $key=>$attrs){
			$content .= '<li>NAME:'.'key:'.$key.'**'.$attrs['company_code'].'::attr=='.$attrs['zip'].'</li>';//.'::value=='.$value.'</li>';
			//$content .= '<li>NAME:'.$key.'::attr=='.$attrs['l_name'].'</li>';//.'::value=='.$value.'</li>';
			
			foreach($attrs as $attr=>$value){
				$content .= '<li>NAME:'.$key.'::attr=='.$attr.'::value=='.$value.'</li>
				';
			}
		}
		$content .= '</ul></div>';//all_content;
*/
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_leads_tab
	private function add_accounts_tab(){
		global $o_package_container;
		
		$accounts = self::$current_user->get_accounts();
		$location_notes = self::$current_user->get_loc_notes();
		$location_contacts = self::$current_user->get_loc_contacts();
		$uname = self::$current_user->get_info_attr('username');
		$range = self::$current_user->get_info_attr('account_display_range');
		$redirect = self::$current_user->get_info_attr('account_redirect');
		$next_range_attr = '';
		$prev_range_attr = '';
		if($range != ''){

			$range_exploded = explode(",",$range);
			//var_dump($range_exploded);
			$pr_start = ((int)$range_exploded[0]*1)- 50;
			$pr_end = ((int)$range_exploded[1]*1) - 50;
			$nr_start = ((int)$range_exploded[0]*1) + 50;
			$nr_end = ((int)$range_exploded[1]*1 ) + 50;
			$prev_range_attr = $pr_start.', '.$pr_end;
			$next_range_attr = $nr_start.', '.$nr_end;
			//$range = './index.php?change_account_range='.$range.'#accounts_tab';
		}else{
			$range = '#';
			$prev_range_attr = '#';
			$next_range_attr = '50, 100';
			
		}
		if($redirect ==  ''){
			$redirect = './index.php?';
		}
//		$location_contacts = self::$current_user->get_loc_contacts();
		//echo var_dump($location_notes['99distro_test3']).'<br />';
		//notes section in each account dropdown
		function get_account_notes($data_name, $ln, $def_name){
			//echo $data_name.'<br />';
			//restaraunt_notes entries where support_ninja = DISTRO_NOTE
			$ds_notes = $ln[$data_name];
			//var_dump($location_notes);
			//echo 'get_account_notes... dname='.$data_name.' note='.$location_notes[$data_name]['message'].'<br/>';
			//echo var_dump($ds_notes);
			
			$notes = '
			<div id="notes_'.$data_name.'" >
				<div class="acc_drop_top">
					<span style="float:left;margin-left:20px;">
						<img src="./images/btn_addnew.png" class="acc_add_detail acc_add_note" style="cursor:pointer;" alt="btn_anew"/>
						<span style="margin-left:5px;">Notes</span>
					</span>
					<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
						<span>Edit</span>
						<img src="./images/icon_edit.png" alt="i_edit">
					</span>
					<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
						<span>Done</span>
						<img src="./images/check_green.png" alt="ch_grn">
					</span>				
				</div>';//acc_drop_top
	        $notes .= '<div class="accnt_detail_wrapper">';
	        if($ds_notes){
	        	//echo var_dump($ds_notes).'<br />';
	        	//echo 'good '.$data_name.'<br />';

	        	foreach($ds_notes as $date=>$message){
		        	//echo 'get_account_notes date= <br /> date= '.$date.'<br /> message = '.$message;
		        	//$note_date = $note['created_on'];//self::display_datetime($note['created_on']);
	        		        
		        	$notes .= '
		        	<div class="loc_note accnt_detail accnt_view">
		        		<span class="loc_note_date">'.$date.'</span><span class="loc_note_note">'.$message.'</span>
		        	</div>';
		        }
	        	$id = 0;
		        $notes .= '
		        <div class="acc_edit_details loc_note_form">
		        	<form id="note_form_'.$data_name.'" >';
		        foreach($ds_notes as $date=>$message){
		        $message = trim($message);		        
		        	$notes .= '
		        	<div class="acc_edit_detail">
		        		<span class="loc_note_date">'.$date.'</span>
		        		<label for="n_'.$data_name.'_'.$id.'">NOTE:</label>
		        		<input type="text" class="acc_note_input" id="n_'.$data_name.'_'.$id.'" name="n_'.$data_name.'_'.$id.'" value="'.$message.'"/>
		        	</div>';//account_edit_detail
		        	$id++;
		        }
		        $notes .= '<input type="hidden" id="add_note_hidden" name="add_note_hidden" value="" /></form></div>';//note_form and acc_edit_detail
		        
		    }else{
			    $notes .= '<div class="loc_note accnt_detail">NO NOTES FOUND</div>';			    
		    }
		    $notes .= '
		    <div class="loc_add_detail loc_add_note accnt_detail">
		    	<form class="loc_add_note_form">
		    		<label for="note_to_add'.$def_name.'">Note:</label>
		    		<input class="note_to_add_'.$data_name.' acc_note_input" dataname="'.$data_name.'" id="note_to_add'.$def_name.'" name="note_to_add'.$def_name.'" value="" />
		    		<div class="loc_add_detail_submit_cancel">
		    			<span class="loc_cancel_add_detail">Cancel</span>
		    			<span class="loc_submit_add_detail submit_new_note" val="note_to_add'.$def_name.'">Submit</span>
		    		</div>
		    	</form>
		    </div>';//loc_add_note
			$notes .= '</div></div>';//whole note div and accnt_detail_wrapper
			return $notes;
			
		}
		//contact section in each account dropdown
		function get_account_contacts($data_name, $lc,$def_name){
			//echo $data_name.'<br />';
			//restaraunt_notes entries where support_ninja = DISTRO_NOTE
			$ds_contacts = $lc[$data_name];
			//var_dump($location_notes);
			//echo 'get_account_notes... dname='.$data_name.' note='.$location_notes[$data_name]['message'].'<br/>';
			//echo var_dump($ds_notes);
			
			$contacts = '<div id="contacts_'.$data_name.'" class="loc_contacts">
			<div class="acc_drop_top">
				<span style="float:left;margin-left:20px;">
					<img src="./images/btn_addnew.png" class="acc_add_detail acc_add_contact" alt="btn_a_n"/>
					<span>Contacts</span>
				</span>
				<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
					<span>Edit</span>
					<img src="./images/icon_edit.png" class="acc_edit_contact" alt="i_e" />
				</span>
				<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
						<span>Done</span>
						<img src="./images/check_green.png" alt="ch_grn" />
				</span>				
			</div>
	        ';//closes loc_con_top
	        $contacts .= '<div class="accnt_detail_wrapper">';	        
	        if($ds_contacts){
	        	//echo var_dump($ds_notes).'<br />';
	        	//echo 'good '.$data_name.'<br />';
	        	foreach($ds_contacts as $id){
	        		$name = $id['firstname'].' '.$id['lastname'];
		        	//echo 'contact::<br /> id= '.$id['id'].'<br /> name = '.$name.'<br /><br />';
		        	//$note_date = $note['created_on'];//self::display_datetime($note['created_on']);
	        		        
		        	$contacts .= 
		        	'<div class="loc_contact accnt_detail accnt_view">
		        		<span class="loc_contact_firstname">'.$id['firstname'].'</span>
		        		<span class="loc_contact_lastname" style="margin-right:40px;">'.$id['lastname'].'</span>
		        		<span class="loc_contact_phone" style="margin-right:40px;">'.$id['phone'].'</span>
		        		<span class="loc_contact_email" style="margin-right:40px;">'.$id['email'].'</span>
		        		<span class="loc_contact_note">'.$id['contact_note'].'</span>		        	
		        	</div>';
		        }
		        $contacts .= '
		        <div class="acc_edit_details loc_contact_form">
		        	<form id="contact_form_'.$data_name.'" >';
		        $id_add = 0;
		        foreach($ds_contacts as $id){
			        /*if($id['firstname'] != null){
				        echo '||'.$id['firstname']."||<br />";
			        }*/
		        	$fname = ($id['firstname'] != null)?trim($id['firstname']).'_'.$id_add:$def_name."_".$id_add;
		        	//echo 'CONTACT:: '.$def_name.'<br />=='.$fname.'==<br />';
		        	$lname = ($id['lastname'] != null)?trim($id['lastname']).'_'.$id_add: $def_name."_".$id_add;
		        	//echo '=='.$lname.'==<br />';
		        	$phone = ($id['phone'] != null)?trim($id['phone']).'_'.$id_add:$def_name."_".$id_add;
		        	//echo '=='.$phone.'==<br />';		        	
		        	$email = ($id['email'] != null)?trim($id['email']).'_'.$id_add:$def_name."_".$id_add;
		        	//echo '=='.$email.'==<br />';		        	
		        	$cnote = ($id['contact_note'] != null)?trim($id['contact_note']).'_'.$id_add:$def_name."_".$id_add;
		        	//echo '=='.$cnote.'==<br /><br />';		        	
		        		        			        			        			        	
		        	$contacts .= '
		        	<div class="acc_edit_detail">
		        		<label for="fname_'.$fname.'">First Name:</label>
		        		<input type="text" id="fname_'.$fname.'" name="fname_'.$fname.'" value="'.$fname.'"/>
		        		
		        		<label for="lname_'.$lname.'">Last Name:</label>
		        		<input type="text" id="lname_'.$lname.'" name="lname_'.$lname.'" value="'.$lname.'"/>
		        		
		        		<label for="phone_'.$fname.'">Phone:</label>
		        		<input type="text" id="phone_'.$fname.'" name="phone_'.$fname.'" value="'.$phone.'"/>
		        		
		        		<label for="email_'.$fname.'">Email:</label>
		        		<input type="text" id="email_'.$fname.'" name="email_'.$fname.'" value="'.$email.'"/>
		        		
		        		<label for="cnote_'.$fname.'">Note:</label>
		        		<input type="text" id="cnote_'.$fname.'" name="cnote_'.$fname.'" value="'.$cnote.'"/>
		        	</div>';//account_edit_detail
		        	$id_add++;
		        }
		        $contacts .= '</form></div>';//note_form and acc_edit_detail		        
		    }else{
			        $contacts .= '<div class="loc_note accnt_detail">NO CONTACTS FOUND</div>';			        
		    }		    
			$contacts .= '
		    <div class="loc_add_detail loc_add_contact accnt_detail">
		    	<form class="loc_add_contact_form">
		    		<label for="add_fname_'.$data_name.'">First Name:</label>
		        	<input type="text" id="add_fname_'.$data_name.'" name="add_fname_'.$data_name.'" value=""/>
		        	<label for="add_lname_'.$data_name.'">Last Name:</label>
		        	<input type="text" id="add_lname_'.$data_name.'" name="add_lname_'.$data_name.'" value=""/>
		        	<label for="add_phone_'.$data_name.'">Phone:</label>
		        	<input type="text" id="add_phone_'.$data_name.'" name="add_phone_'.$data_name.'" value=""/>
		        	<label for="add_email_'.$data_name.'">Email:</label>
		        	<input type="text" id="add_email_'.$data_name.'" name="add_email_'.$data_name.'" value=""/>
		        	<label for="add_cnote_'.$data_name.'">Note:</label>
		        	<input type="text" id="add_cnote_'.$data_name.'" name="add_cnote_'.$data_name.'" value=""/>
		    		<div class="loc_add_detail_submit_cancel">
		    			<span class="loc_cancel_add_detail">Cancel</span>
		    			<span class="loc_submit_add_detail submit_new_contact">Submit</span>
		    		</div>
		    	</form>
		    </div>';//loc_add_contact			
			$contacts .= '</div></div>';//closes loc_contacts and accnt_detail_wrapper 	
			return $contacts;
			
		}
		//location section in each account dropdown	
		function get_account_location($loc_deets){
			global $o_package_container;
			
			
			//($id['firstname'] != null)?trim($id['firstname']).'_'.$id:"";		
			$address = ($loc_deets['address'] != null)?trim($loc_deets['address']):"";
			$city = ($loc_deets['city'] != null)?trim($loc_deets['city']):"";
			$state = ($loc_deets['state'] != null)?trim($loc_deets['state']):"";
			$zip = ($loc_deets['zip'] != null)?trim($loc_deets['zip']):"";
			//var_dump($loc_deets);
			//echo '<br /><br />LOCATION::<br/>address = '.$address.'<br />city = '.$city.'<br />state= '.$state.'<br />zip= '.$zip.'<br /><br /><br />';
			$loc = '
			<div class=loc_loc>
				<div class="acc_drop_top">
					<span style="float:left;margin-left:20px;">
						<span>Location</span>
					</span>
					<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_edit_button">
						<span>Edit</span>
						<img src="./images/icon_edit.png" class="acc_edit_location" alt="icon_edit" />
					</span>
					<span style="float:right;margin-right:20px;cursor:pointer;" class="acc_done_button">
						<span>Done</span>
						<img src="./images/check_green.png" alt="check_green" />
					</span>				
				</div>';//acc_drop_top
			$loc .= '
			<div class="accnt_detail_wrapper">
				<div class="accnt_view">
					<div style="text-align:left;width:710px;margin:3px auto;font:10px Helvetica;color:#717074;letter-spacing:0.075em;">
						<div style="font:10px Verdana;">Address: '.$address.'</div>
						<div style="font:10px Verdana;"><span>'.$city.', </span><span>'.$state.', </span><span>'.$zip.'</span></div>
					</div>
				</div>';//accnt_view
		    $loc .= '
		        <div class="acc_edit_details loc_loc_form">
		        	<form id="loc_form_'.$loc_deets['data_name'].'" >
		        	<div class="acc_edit_detail">
		        		<label for="loc_cont_address_'.$loc_deets['data_name'].'">Address:</label>
		        		<input type="text" id="loc_cont_address_'.$loc_deets['data_name'].'" name="loc_cont_address_'.$loc_deets['data_name'].'" value="'.$address.'"/>
		        	</div>
		        	<div class="acc_edit_detail">
		        		<label for="loc_cont_city_'.$loc_deets['data_name'].'">City:</label>
		        		<input type="text" id="loc_cont_city_'.$loc_deets['data_name'].'" name="loc_cont_city_'.$loc_deets['data_name'].'" value="'.$city.'"/>
		        	</div>
		        	<div class="acc_edit_detail">
		        		<label for="loc_cont_state_'.$loc_deets['data_name'].'">State:</label>
		        		<input type="text" id="loc_cont_state_'.$loc_deets['data_name'].'" name="loc_cont_state_'.$loc_deets['data_name'].'" value="'.$state.'"/>
		        	</div>
		        	<div class="acc_edit_detail">
		        		<label for="loc_cont_zip_'.$loc_deets['data_name'].'">Zip:</label>
		        		<input type="text" id="loc_cont_zip_'.$loc_deets['data_name'].'" name="loc_cont_zip_'.$loc_deets['data_name'].'" value="'.$zip.'"/>
		        	</div>';//account_edit_detail
		        $loc .= '</form></div>';//loc_loc_form and acc_edit_details	
		        				
			$loc .= '</div></div>';//closes accnt_detail and loc_loc
			return $loc;
		}//get_account_locations
		//returns the markup for the new account form
				//
		//
		// add content to tab_list
		$title ='<li class="tab" style="width:110px;"><a href="#accounts_tab">ACCOUNTS</a></li>
		';
		
		$accounts_head = '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">MANAGE ACCOUNTS</div>
					<div id="accounts_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;">
						<div id="account_points" style="margin:5px;color:#b3b3b3;font:13px Helvetica;">LAVU POINTS:N/A</div>
						<div id="account_create_account" style="background-image:url(./images/btn_createaccount__.png);width:144px;height:22px;"></div>				
					</div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
				<div id="account_nav" style="position:relative;top:100px;">
								
								<a href="'.$redirect.'change_account_range='.$prev_range_attr.'#accounts_tab" id="prev_start" range="'.$prev_range_attr.'" style="color:#aecd37;cursor:pointer;margin-right:20px;">prev 50</a>
								<a href="'.$redirect.'change_account_range='.$next_range_attr.'#accounts_tab" range="'.$next_range_attr.'" id="next_start"style="color:#aecd37;cursor:pointer;">next 50</a>
							
				</div>
			';		
		
		$content ='<div id="accounts_tab" class="tab_section tab00">';
		$content .= $accounts_head;
		/*
		$content .= '<div id="account_top" style="height:60px;border-bottom:1px solid #999;">
			<div id="account_title" style="text-align:left;font:24px Verdana;color:#717074;float:left;width:220px;">Manage Accounts</div>
			<div id="accounts_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;">
				<div id="account_points" style="margin:5px;color:#b3b3b3;font:13px Helvetica;">LAVU POINTS:69</div>
				<div id="account_create_account" style="background-image:url(./images/btn_createaccount__.png);width:144px;height:22px;"></div>
			</div>
		</div>';
		*/
		
		$content .= '<div class="adjust_body" style="position:relative; top:115px;"><div id="accnts_desc">
			<span style="width:240px;text-align:left;margin-left:20px;">COMPANY</span>
			<span style="width:150px;">CREATED</span>
			<span style="width:150px;">GOOD UNTIL</span>
			<span style="width:150px;">LEVEL</span>
			<span style="width:150px;">LOGIN</span>
			<br style="clear:left;" />														
		</div>
		';
		/*$content .= '<div class="adjust_body" style="position:relative; top:115px;"><div id="accnts_desc">
			<span style="width:240px;text-align:left;margin-left:20px;">COMPANY</span><span><img src="./images/sort_arrow.png" alt="s_a" /></span>
			<span style="width:150px;">CREATED</span><span><img src="./images/sort_arrow.png" alt="s_a" /></span>
			<span style="width:150px;">GOOD UNTIL</span><span><img src="./images/sort_arrow.png" alt="s_a" /></span>
			<span style="width:150px;">LEVEL</span><span><img src="./images/sort_arrow.png" alt="s_a" /></span>
			<span style="width:150px;">LOGIN</span><span><img src="./images/sort_arrow.png" alt="s_a" /></span>
			<br style="clear:left;" />														
		</div>
		';*/
		
//beginning of accordian

		$content .= '<div id="account_acc_wrapper">
		';
		//used as a place_filler to make id names unique in forms
		$contact_default_name = 0;
		
		$user_email = self::$current_user->get_info_attr('email');
		foreach($accounts as $key=>$attrs){//key=account data name, attrs=attr[value]
			//logic to get created/good until fields and format them
			$trial_end = $attrs['trial_end'];
			$created = self::display_datetime((string)$attrs['created'], "date");
			
			$license_applied = $attrs['license_applied'];
			$license_type = $attrs['license_type'];
			if (is_numeric($attrs['package'])) {
				$original_license = $license_applied;
				$license_applied = (int)$attrs['package'];
				$license_type = $o_package_container->get_plain_name_by_attribute('level', $attrs['package']);
			}
			
			$level_display = self::display_level($trial_end,$original_license,$license_type, $attrs['data_name'], self::$s_licenses_not_upgrades);
						
			if($trial_end==""){
				$good_until = "n/a";
			}else{
				$good_until = self::display_datetime((string)$trial_end,"date");
			}
			$dataname = str_replace(" ","",$attrs['data_name']);
			//echo '<a href="./exec/backend_account_login?loginto='.$dataname.'" target="_blank">login</a>';
			$content .= '
			<div class="acc_head accordionButton acc_count">
				<img class="acc_arrow expand_account_btn" src="./images/arrow_left.png" style="float:left;" alt="a_l"/>				
				<span style="width:290px;">'.$attrs['company_name'].'</span>
				<span style="width:150px;">'.$created.'</span>
				<span style="width:150px;">'.$good_until.'</span>
				<span style="width:160px;text-align:center;">'.$level_display.'</span>
				<span style="width:160px;text-align:center;"><a href="./exec/backend_account_login.php?loginto='.$dataname.'&loggedin='.$uname.'&email='.$user_email.'" target="_blank">login</a></span>
				<input type="hidden" name="dataname" value="'.$key.'" />
				<br class="clear:left;">
			</div>		
				';
			$content .= '<div class="acc_accnt_content accordionContent">
			';
			$content .= get_account_notes($key, $location_notes, $contact_default_name);
			$content .= get_account_contacts($key, $location_contacts,$contact_default_name);
			$contact_default_name++;
			$content .= get_account_location($attrs);
			$content .= '
			</div>';//acc_accnt_content
			
			/*
			$content .= get_account_contacts($key);			
			$content .= get_account_location($key);					
			*/
		}
		$content .= '</div></div></div>';//'</div>'.get_new_account_form().'</div></div>';//lead_acc_wrapper
	
	/*
		// add content to tab_sections 	
		$content ='<div id="accounts_tab" class="tab_section tab00">
			<ul id="accounts_list" style="overflow:scroll;height:320px;">';
		foreach($accounts as $key=>$attrs){
			foreach($attrs as $attr=>$value){
				$content .= '<li>NAME:'.$key.'::attr=='.$attr.'::value=='.$value.'</li>
				';
			}
		}
		$content .= '</ul></div>
		';//all_content;*/
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_accounts_tab
	private function add_resources_tab(){
			$title ='<li class="tab"><a href="#resources_tab">RESOURCES</a></li>
		';
		function get_resource_files($dir){
			$files = '';
			$files = scandir('./Resources/'.$dir);
			$leg = str_replace("/", " -- ", $dir);
			$list = '<fieldset style="text-align:left;margin:10px 0px;color:#999;padding:20px 10px;"><legend>'.$leg.'</legend><div style="float:left;width:400px;margin:10px;padding:20px;">';
			$half = (count($files) / 2);
			$count = 0;
			$new_div = true;
				//$parts = explode('.', $file);
				//$ext = end($parts);
			$image_types = array('jpg','png','jpeg');
				//$doc_types = array('doc','docx','txt');
				//$pdf_type = array('pdf');
				//$rtf_type = array('rtf');
			//echo '--'.$dir.',  half:'.$half."<br><br>";
			foreach($files as $file){
				if($count <= $half){
					if ($file != "." && $file != "..") {
					//echo "<<br>";
					//if(is_dir)
						$arr = explode(".",$file);
						$ext = end($arr);
						$ext = strtolower($ext);
						
						if(in_array($ext, $image_types)){
							//echo 'img::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else if($ext=='pdf' || $ext=='PDF'){
							//echo 'pdf::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_pdf.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else{
							//echo 'doc::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_document.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}
						$list .= '<div>';
							$list .= $t_nail;//t_nail($file);
							$list .= '<a href="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" target="_blank">';
							$list .= $file;
						$list .= '</a>';
						$list .= '</div>';
					}
					$count++;
				}else if($new_div == true){
					$new_div = false;
					$list .='</div><div style="float:left;width:400px;margin:10px;padding:20px;">';
					if ($file != "." && $file != "..") {
					//echo "<<br>";
					//if(is_dir)
						$arr = explode(".",$file);
						$ext = end($arr);
						$ext = strtolower($ext);
						
						if(in_array($ext, $image_types)){
							//echo 'img::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else if($ext=='pdf' || $ext=='PDF'){
							//echo 'pdf::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_pdf.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else{
							//echo 'doc::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_document.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}
						$list .= '<div>';
							$list .= $t_nail;//t_nail($file);
							$list .= '<a href="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'">';
							$list .= $file;
						$list .= '</a>';
						$list .= '</div>';
					}
					$count++;
				}else{
					if ($file != "." && $file != "..") {
					//echo "<<br>";
					//if(is_dir)
						$arr = explode(".",$file);
						$ext = end($arr);
						$ext = strtolower($ext);
						
						if(in_array($ext, $image_types)){
							//echo 'img::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else if($ext=='pdf' || $ext=='PDF'){
							//echo 'pdf::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_pdf.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}else{
							//echo 'doc::'.$ext.'<br />';
							$t_nail = '<img src="https://admin.poslavu.com/distro/beta/images/t_document.png" style="width:30px;margin-right:20px;" alt="resource_preview"/>';
						}
						$list .= '<div>';
							$list .= $t_nail;//t_nail($file);
							$list .= '<a href="https://admin.poslavu.com/distro/beta/Resources/'.$dir.'/'.$file.'">';
							$list .= $file;
						$list .= '</a>';
						$list .= '</div>';
					}
					$count++;
				}
			}
			$list .= '</div></fieldset>';
		//return $images;
			return $list;
		}//get_resource_files
		
		//<span id="res_print_choice_">GRAPHICS</span><span class="r_h_div"> | </span>
		//<span id="res_web_choice_">LLS</span><span class="r_h_div"> | </span>
		
		$resources_top = '<div id="resources_tab_top" class="tab_top">
				<div id="res_tab_top" style="font:14px Verdana;color:#717074;" >
					<span id="doc_request" class="res_request" res="documents_content">DOCUMENTS</span><span class="r_h_div"> | </span>
					<span id="graphics_request" class="res_request" res="graphics_content">GRAPHICS</span><span class="r_h_div"> | </span>
					<span id="lls_request" class="res_request" res="lls_content">LLS</span><span class="r_h_div"> | </span>
					<span id="photos_request" class="res_request" res="photos_content">PHOTOS</span><span class="r_h_div"> | </span>
					<span id="print_request" class="res_request" res="print_content">PRINT</span><span class="r_h_div"> | </span>
					<span id="video_request" class="res_request" res="video_content">VIDEO</span>
				</div>
				<div class="res_drop_down" id="res_print_drop">
					<div class="res_drop_choice">
						brochures
					</div>
					<div class="res_drop_choice">
						logos
					</div>
					<div class="res_drop_choice">
						rackcards
					</div>
				</div>
				<div class="res_drop_down" id="res_web_drop">
					<div class="res_drop_choice">
						brochures
					</div>
					<div class="res_drop_choice">
						logos
					</div>
					<div class="res_drop_choice">
						rackcards
					</div>
				</div>				
				<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
			</div>
		';//end resources_tab_top
		
		//make the links. 
		
		
		// add content to tab_sections 	
		$content ='<div id="resources_tab" class="tab_section tab00">
		';
		$content .= $resources_top;
		
		$content .= '<div class="adjust_body">
		
					<div id="documents_content" class="resource_section_container" style="display:block;">
					<div class="resource_section_header" >DOCUMENT RESOURCES</div>
					'.get_resource_files('Documents').'
					</div>
					<div id="graphics_content" class="resource_section_container">
						<div class="resource_section_header">GRAPHICS RESOURCES</div>
						
							<div id="graphics_ads" class="inner_resources">'.get_resource_files('Graphics/ads').'</div>
						
						
							<div id="graphics_logos" class="inner_resources">'.get_resource_files('Graphics/Logos').'</div>
						
						
							<div id="graphics_misc" class="inner_resources">'.get_resource_files('Graphics/misc').'</div>
						
						
							<div id="graphics_products" class="inner_resources">'.get_resource_files('Graphics/products').'</div>
						
						
							<div id="graphics_screenshots" class="inner_resources">'.get_resource_files('Graphics/screenshots').'</div>
						
					</div>
					<div id="lls_content" class="resource_section_container">
					<div class="resource_section_header">LLS RESOURCES</div>
					'.get_resource_files('LLS').'
					</div>
					<div id="photos_content" class="resource_section_container">
					 	<div class="resource_section_header">PHOTO RESOURCES</div>
						<div id="photos_devices">'.get_resource_files('Photos/devices').'</div>
						<div id="photos_location">'.get_resource_files('Photos/location').'</div>
						<div id="photos_products">'.get_resource_files('Photos/products').'</div>
					</div>
					<div id="print_content" class="resource_section_container">
						<div class="resource_section_header">PRINT RESOURCES</div>
						<div id="print_banners">'.get_resource_files('Print/banners').'</div>
						<div id="print_brochures">'.get_resource_files('Print/brochures').'</div>
						<div id="print_logos">'.get_resource_files('Print/logos').'</div>
						<div id="print_product">'.get_resource_files('Print/product').'</div>
						<div id="print_rackcards">'.get_resource_files('Print/rackcards').'</div>
					</div>
					<div id="video_content" class="resource_section_container">
					<div class="resource_section_header">VIDEO RESOURCES</div>
					'.get_resource_files('Video').'
					</div>
			</div></div>
		';//all_content;
		self::$tab_list .= $title;
		self::$tab_sections .= $content;		
	}//add_resources_tab
	private function add_licenses_tab(){
		global $o_package_container;
		global $license_select;
		
		$my_info = self::$current_user->get_info();
		$my_licenses = self::$current_user->get_licenses(); 	
		$points = (int)$my_info['credits'];
		$reseller_id = $my_info['id'];
		$reseller_name = $my_info['username'];
		$license_values = array(
			"Silver2"=>$o_package_container->get_value_by_attribute('name', 'Silver2'),
			"Gold2"=>$o_package_container->get_value_by_attribute('name', 'Gold2'),
			"Platinum2"=>$o_package_container->get_value_by_attribute('name', 'Platinum2')
		);
		
		$silver_pts = '';
		$gold_pts = '';
		$plat_pts = '';
		if($points >= $license_values['Silver2']){
			$silver_points = 'show_radio';
		}if($points >= $license_values['Gold2']){
			$gold_points = 'show_radio';
		}if($points >= $license_values['Platinum2']){
			$plat_points = 'show_radio';
		}
		require(dirname(__file__).'/form_requirements.php');
		
		//error_log(print_r($license_select,TRUE));
		//echo var_dump($my_licenses);
		$license_options = '';
		//$license_display = '<div id="available_licenses"><div style="color:#439789;margin-bottom:20px;">AVAILABLE LICENSES</div>';
		foreach($license_select as $a_option_vals){
			$option = $a_option_vals[0];
			$option_printed = $a_option_vals[1];
			if($option !== ''){
				$license_options .= '<option value="'.$option.'">'.$option_printed.'</option>';
				//$license_display .= '<div class="license_display">'.$option_printed.'</div>';
			} else {
				$license_options .= '<option value="&nbsp;" selected>&nbsp;</option>';
			}
		}
		//$license_display .= '</div>';
		$license_display = '<div id="available_licenses"><div style="color:#439789;margin-bottom:20px;">AVAILABLE LICENSES::</div>';
			$gold = 0;
			$silver = 0;
			$platinum = 0;
			$none_available = '<div>No licences available to apply. Please purchase above</div>';
			foreach($my_licenses as $key=>$value){
					$type = $value['type'];
					$purchased = $value['purchased'];
					$printed_name = preg_replace('/[0-9]/', '', $type);
					$license_display .= '<div class="license_display"><span style="display:inline-block;width:120px;">'.$printed_name.'</span><span style="color:black;">purchased:: '.$purchased.'</span></div>';
					if($printed_name == 'Gold'){
						$gold++;
					}else if($printed_name == 'Silver'){
						$silver++;
					}else if($printed_name == 'Platinum'){
						$platinum++;
					}
			}
				if($gold > 0 || $silver > 0 || $platinum > 0){
					$none_available = '';
				}
			$license_display .= $none_available.'</div>';
		
		//echo $license_display;
		$title ='<li class="tab" style="width:100px"><a href="#licenses_tab">LICENSES</a></li>
		';
		if($my_info['credits'] != ''){
			$lavu_points_display = $my_info['credits'];
		}else{
			$lavu_points_display = 'N/A';
		}
		$licenses_top = '
			<div id="licenses_tab_top" class="tab_top">
				<div class="tab_top_top">
					<span style="font:24px Verdana;color:#717074;">BUY NEW LICENSE</span>
					<span style="color:#B7BDB0;margin-left:388px;font-size:12px;">LAVU POINTS:</span>
					<span >'.$lavu_points_display.'</span>									
				</div>
				<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="d_w_f"/></div>
			</div>';//end licenses_tab_top
			/*<div id="licenses_points_top" style="text-align:right;">
						<span style="color:#B7BDB0;margin-right:100px;font-size:12px;">LAVU POINTS: </span>'.$my_info['credits'].'
					</div>*/			
		// add content to tab_sections 
		
		$content ='<div id="licenses_tab" class="tab_section tab00">';
		$content .= $licenses_top;
		$content .= '
			<div id="licenses_meat" class="adjust_body">
				<div id="license_response_message" style="display:none;"></div>
				<div id="show_l_form" style="display:none;color:blue;text-decoration:underline;cursor:pointer;margin-right:50px;"><br />Open form up again</div>
				<div id="licenses_form" >
				<form id="new_license" method="post" action="./exec/buy_license.php">
					<fieldset style="text-align:left;border:none;">
						<legend>CREDIT CARD INFO</legend>
						<p style="font:10px verdana;color:#717074;margin:0;">required fields*</p>
						<label for="license_license">License*</label>
						<div class="select_arrow_background">
						<select name="license_license" id="license_license" class="select_full">
							'.$license_options.'														
						</select>
						</div>
						<br />
						<label for="card_number">Card Number*</label>
						<input type="text" id="card_number" name="card_number" />
						<br />
						<label for="license_card_exp_m">Card Expiration*</label>
						<span style="background:url(./images/sort_arrow.png) no-repeat right; display:inline-block; width:73px;margin-right:4px;">
						<select name="license_card_exp_m" id="license_card_exp_m" class="select_half">
							<OPTION VALUE="" SELECTED>Month
							<OPTION VALUE="01">Jan(01)
							<OPTION VALUE="02">Feb(02)
							<OPTION VALUE="03">Mar(03)
							<OPTION VALUE="04">Apr(04)
							<OPTION VALUE="05">May(05)
							<OPTION VALUE="06">June(06)
							<OPTION VALUE="07">July(07)
							<OPTION VALUE="08">Augu(08)
							<OPTION VALUE="09">Sep(09)
							<OPTION VALUE="10">Oct(10)
							<OPTION VALUE="11">Nov(11)
							<OPTION VALUE="12">Dec(12)														
						</select>
						</span>
						<span style="background:url(./images/sort_arrow.png) no-repeat right; display:inline-block; width:73px;">
						<select name="license_card_exp_y" id="license_card_exp_y" class="select_half">
							 <OPTION VALUE="13">2013
							 <OPTION VALUE="14">2014
							 <OPTION VALUE="15">2015
							 <OPTION VALUE="16">2016
							 <OPTION VALUE="17">2017
							 <OPTION VALUE="18">2018
							 <OPTION VALUE="19">2019
							 <OPTION VALUE="20">2020
							 <OPTION VALUE="21">2021
							 <OPTION VALUE="22">2022
							 <OPTION VALUE="23">2023
							 <OPTION VALUE="24">2024														
						</select>
						</span>						
						<br />						
						<label for="card_ccv">CCV*</label>
						<input type="text" id="card_ccv" name="card_ccv" />	
						<br />					
						<label for="apply_points">Apply  Points</label>	
						<input type="text" id="apply_points" name="apply_points" />
						<br />
						<div><input type="image" src="./images/btn_submit.png" id="submit_buy_new_license"style="padding: 20px 0px 0px 154px;" alt="btn_sub"/></div>																
					</fieldset>
				</form>
				'.$license_display.'
				</div>			
				<div id="licenses_prices">
				<p style="border-bottom:1px solid #d6d4d4;font:15px Verdana;color:#555555;padding:5px 4px;">LAVU LICENSES</p>
				<div id="license_points_form_div" style="margin:20px 0;border-bottom:1px solid #666;padding-bottom:15px;">
					<form id="license_points_form">
							<input type="radio" class="license_pts_radio '.$silver_points.'" name="pts_license_selected" value="Silver2" DISABLED/>
							<img src="./images/seal_silver.png" alt="seal_s" />
					<span style="margin-right:10px;color:#7e7e73;letter-spacing:0.075em;position:relative;top:-9px;">SILVER</span>
					<span style="position:relative;top:-9px;">'.$license_values['Silver2'].' Points</span>
					
							<input type="radio" class="license_pts_radio '.$gold_points.'"  name="pts_license_selected" value="Gold2" DISABLED/>
							<img src="./images/seal_gold.png" alt="seal_g"/>
					<span style="margin-right:10px;color:#7e7e73;letter-spacing:0.075em;position:relative;top:-9px;">GOLD</span>
					<span style="position:relative;top:-9px;">'.$license_values['Gold2'].' Points</span>
							
							<input type="radio" class="license_pts_radio '.$plat_points.'" name="pts_license_selected" value="Platinum2" DISABLED/>
							<img src="./images/seal_plat.png" alt="seal_p" />
					<span style="margin-right:10px;color:#7e7e73;letter-spacing:0.075em;position:relative;top:-9px;">PLATINUM</span>
					<span style="color:#38373;letter-spacing:0.05em;position:relative;top:-9px;">'.$license_values['Platinum2'].' Points</span>
					
					<input type="hidden" name="reseller_name" value="'.$reseller_name.'"/>
					<input type="hidden" name="reseller_id" value="'.$reseller_id.'"/>
					<input type="hidden" name="reseller_points" value="'.$points.'"/>
					
							
							<input type="submit" id="submit_pts_license" style="display:none;" />
							<span id="cancel_pts_license" style="padding:5px;display:none;margin-left:40px;width:20px;height:20px;border:1px solid #333;">CANCEL</span>
					</form>
				</div>
				<div id="buy_license_with_points_label">&nbsp;</div>
				<div>SELECT OPTION ABOVE TO APPLY POINTS TO LICENSE</div>								
				</div>
				<br style="clear:left;">
			</div>
		
		</div>
		';//all_content;
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
		
		/* new form thing.
		<form id="new_license">
						<fieldset>
							<legend>CREDIT CARD INFO</legend>
							<p style="font:10px verdana;color:#717074;margin:0;">required fields*</p>
							<label for="license_license">License*</label>
							<select type="text" name="license_license" id="license_license" class="new_lic_field" value="'.$my_info['f_name'].'" />
						</fieldset>
					</form>	*/	
	}//add_licenses_tab
	private function add_my_info_tab(){
		$my_info = self::$current_user->get_info();
		$s_accepts_mercury = self::$current_user->get_accepts_mercury() ? 'Yes' : 'No';
		$s_accepts_mercury_checked = self::$current_user->get_accepts_mercury() ? ' checked' : '';
		$certified = $my_info['isCertified']?" Certified":" Not Certified";
		// add content to tab_list
		$title ='<li class="tab" style="width:120px;"><a href="#my_info_tab">MY INFO</a></li>
		';
	
		// add content to tab_sections 	
		$content ='<div id="my_info_tab" class="tab_section tab00">
		';
		
		$content  .= '
		<div id="myinfo_tab_top" class="tab_top">
			<div class="tab_top_top">
				<span style="font:24px Verdana;color:#717074;">ACCOUNT INFORMATION</span>
			</div>
			<div class="tab_top_border"><img src="./images/divider_with_fade.png" alt="d_w_f"/></div>
		</div>
		';//end very top		
		
		$content .= '<div class="adjust_body">
		<div id="my_info_no_form">
			<p style="font:13px Verdana;color#555;">
				<span style="margin-right:60px;">Commission: '.get_distro_commision($my_info).'%</span>
				<span>Certification:'.$certified.'</span>
			</p>
		</div>
		';//my_info above form
		
		$edit_icon = '<span>Edit</span><img src="./images/icon_edit.png" alt="icon_edit" />';
		$done_icon = '<span>Done</span><img src="./images/check_green.png" alt="check_green" />';
		
		//<label for="pCurrent">Current Password:</label>
		//		<input type="password" name="pCurrent" id="pCurrent" class="p_field" />
		//		<br />
		//Form that holds all...
		$content .= '<form id="my_info_form" name="my_info_form" method="post" action="" enctype="multipart/form-data">
			<fieldset class="my_info_fieldset" id="my_info_fset_personal">
				<legend>Personal<span style="float:right;color:#aecd37;cursor:pointer;" id="p_edit_icon">'.$edit_icon.'</span>
				<span style="float:right;color:#aecd37;cursor:pointer;" id="p_done_icon">'.$done_icon.'</span>
				</legend>
				
				<div id="p_edit">
				<label for="pFirstName">First Name:</label>
				<input type="text" name="pFirstName" id="pFirstName" class="p_field" value="'.$my_info['f_name'].'" />
				<br />
				<label for="pLastName">Last Name:</label>
				<input type="text" name="pLastName" id="pLastName" class="p_field" value="'.$my_info['l_name'].'" />
				<br />
				<div><span class="view_label">Username:</span><span id="pUsername" name="pUsername">'.$my_info['username'].'</div>
				<br />
				<label for="pCurrent">Current Password:</label>
				<input type="password" name="pCurrent" id="pCurrent" class="p_field" />
				<br />
				<label for="pPassword">Update Password:</label>
				<input type="password" name="pPassword" id="pPassword" class="p_field" />
				<br />
				<label for="pConfirm">Confirm:</label>
				<input type="password" name="pConfirm" id="pConfirm" class="p_field" />
				<br />
				<input type="submit" value="" id="submit_personal" name="submit_personal" class="my_info_submit"/>
				<input type="hidden" name="p_current_username" class="p_field" value="'.$my_info['username'].'" />
				<input type="hidden" name="p_current_first" class="p_field" value="'.$my_info['f_name'].'" />
				<input type="hidden" name="p_current_last" class="p_field" value="'.$my_info['l_name'].'" />
				<input type="hidden" name="p_changed" id="p_changed" class="p_field" value="nochange" />
				<input type="hidden" name="p_identify" id="p_identify" class="p_field" value="p_identify" />				
				</div>
				
				<div id="p_view">
				<div><span class="view_label">First Name:</span><span id="p_view_f_name">'.$my_info['f_name'].'</span></div>
				<div><span class="view_label">Last Name:</span><span id="p_view_l_name">'.$my_info['l_name'].'</div>
				<div><span class="view_label">Username:</span><span id="p_view_username">'.$my_info['username'].'</div>
				<div>Update Password:</div>
				<div>Confirm:</div>
				<div id="p_field_submit_message"></div>
				</div>															
  			</fieldset>
			<fieldset class="my_info_fieldset" id="my_info_fset_company">
				<legend>Company<span style="float:right;color:#aecd37;cursor:pointer;" id="c_edit_icon">'.$edit_icon.'</span>
				<span style="float:right;color:#aecd37;cursor:pointer;" id="c_done_icon">'.$done_icon.'</span>
				</legend>
				<div id="c_edit">
				<label for="cName">Name:</label>
				<input type="text" name="cName" id="cName" value="'.$my_info['company'].'" />
				<br />
				<label for="cAddress">Address:</label>
				<input type="text" name="cAddress" id="cAddress" value="'.$my_info['address'].'" />
				<br />
				<label for="cCity">City:</label>
				<input type="text" name="cCity" id="cCity" value="'.$my_info['city'].'" />
				<br />
				<label for="cState">State:</label>
				<input type="text" name="cState" id="cState" value="'.$my_info['state'].'" />
				<br />
				<label for="cPostalCode">Postal Code:</label>
				<input type="text" name="cPostalCode" id="cPostalCode" value="'.$my_info['postal_code'].'" />
				<br />
				<label for="cCountry">Country:</label>
				<input type="text" name="cCountry" id="cCountry" value="'.$my_info['country'].'" />
				<br />
				<label for="cPhone">Phone:</label>
				<input type="text" name="cPhone" id="cPhone" value="'.$my_info['phone'].'" />
				<br />
				<label for="cEmail">Email:</label>
				<input type="text" name="cEmail" id="cEmail" value="'.$my_info['email'].'" />
				<br />
				<label for="cWebsite">Website:</label>
				<input type="text" name="cWebsite" id="cWebsite"  value="'.$my_info['website'].'"/>
				<br />
				<label for="cAcceptsMercery">Mercury:</label>
				<input type="checkbox" name="cAcceptsMercury" id="cAcceptsMercury" value="'.$s_accepts_mercury_checked.'" onclick="checkbox_checked_update_element($(this),$(\'#c_view_accept_mercury\'),{true:\'Yes\',false:\'No\'});" '.$s_accepts_mercury_checked.'/>
				<br />
				<input type="submit" id="submit_company" name="submit_company" value="" class="my_info_submit"/>
				
				</div>
				
				<div id="c_view">
				<div><span class="view_label">Name:</span><span id="c_view_comp">'.$my_info['company'].'</span></div>
				<div><span class="view_label">Address:</span><span id="c_view_add">'.$my_info['address'].'</span></div>
				<div><span class="view_label">City:</span><span id="c_view_city">'.$my_info['city'].'</span></div>
				<div><span class="view_label">State:</span><span id="c_view_state">'.$my_info['state'].'</span></div>
				<div><span class="view_label">Postal Code:</span><span id="c_view_zip">'.$my_info['postal_code'].'</span></div>
				<div><span class="view_label">Country:</span><span id="c_view_country">'.$my_info['country'].'</span></div>
				<div><span class="view_label">Phone:</span><span id="c_view_phone">'.$my_info['phone'].'</span></div>
				<div><span class="view_label">Email:</span><span id="c_view_email">'.$my_info['email'].'</span></div>
				<div><span class="view_label">Website:</span><span id="c_view_web">'.$my_info['website'].'</span></div>
				<div><span class="view_label">Mercury: <a class="fancybox" id="mercury_description_help" rel="group" href="/cp/areas/table_setup_files/png/white.png"><img src="/cp/areas/table_setup_files/png/plus.png" al="+"></a></span><span id="c_view_accept_mercury">'.$s_accepts_mercury.'</span></div>
				<div style="display:none" id="mercury_description_help_displaytext">
					What is the Mercury Accept List?<br />
					<br />
					Mercury Payments has sponsored a PPC campaign which directs prospects to try.poslavu.com.<br />
					Leads generated through try.poslavu.com require a Mercury payment processing account. <br />
					Distributors on the Mercury Accept List will receive leads generated from this campaign.<br />
					These accounts are standard in all other ways including monthly residual Lavu Points.<br />
					<br />
					To join the Mercury Accept List, click (( EDIT )) and then check the Mercury checkbox.<br />
				</div>
				<div id="c_field_submit_message"></div>						
				</div>
																			
			</fieldset>
			<div id="my_info_right">
				<fieldset class="my_info_fieldset" id="my_info_fset_logo">
					<legend>Current Logo<span style="float:right;color:#aecd37;cursor:pointer;" id="l_edit_icon">'.$edit_icon.'</span>
					<span style="float:right;color:#aecd37;cursor:pointer;" id="l_done_icon">'.$done_icon.'</span>
					</legend>';
					$logo_img = "no image uploaded";
					if(file_exists('../images/logos/fullsize/'.$my_info['username'].'.png')){
						$logo_img = '<img src="../images/logos/fullsize/'.$my_info['username'].'.png" style="width:50px;" alt="logo" />';
					}
					$content .= $logo_img;
				$content .= '
					<div id="l_field_submit_message"></div>
					<div id="l_edit">
					<label for="lfile">Filename:</label>
					<input type="file" name="lfile" id="lfile"/> 
					<input type="hidden" name="username" value= '.$my_info['username'].'>
					<br/>
					<input type="submit" value="" id="submit_logo" name="submit_logo" class="my_info_submit"/>
					</div>		
				</fieldset>
				<fieldset class="my_info_fieldset" id="my_info_fset_additional">
					<legend>Additional Info<span style="float:right;color:#aecd37;cursor:pointer;" id="add_edit_icon">'.$edit_icon.'</span>
						<span style="float:right;color:#aecd37;cursor:pointer;" id="add_done_icon">'.$done_icon.'</span>
					</legend>
					<div id="add_edit">
						<textarea cols="30" rows="6" id="add_edit_textarea" name="add_edit_textarea">'.$my_info['notes'].'</textarea>
						<input type="submit" value="" id="submit_notes" name="submit_notes" class="my_info_submit"/>
					</div>
					<div id="add_view">
					'.$my_info['notes'].'
					<div id="add_field_submit_message"></div>
					</div>				
				</fieldset>
			</div>
			</form>
			<br style="clear:left;">
		</div></div>';//end of form
		
/*			<ul id="my_info_list" style="overflow:scroll;height:320px;">';
		foreach($my_info as $key=>$value){
				$content .= '</br><li>::ATTR=='.$key.'::value=='.$value.'</li>
				';
		}
		$content .= '</ul>
*/		
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_my_info_tab
	
	
	//////ADMIN SPECIFIC TABS
	private function add_unassigned_tab($resellers){
		self::$current_user->set_all_unassigned();
		$unacc = self::$current_user->get_all_unassigned_leads();
		$in_progress = self::$current_user->get_all_in_progress();
		$processed = self::$current_user->get_all_processed();
		$current_access = self::$current_user->get_info_attr('access');
		//echo 'current_access===='.$current_access.'<br />';
		//var_dump($processed);
		//var_dump($unacc);
		//var_dump($resellers);
		
		//count unaccepted leads
		$all_count = count($unacc);
		//echo $all_count;
		
		//count unassigned leads
		$unassigned = array();
		$merc_unass_count = 0;
		$is_mercury = false;
		foreach($unacc as $key=>$value){
			//echo 'assigned to :: '.$value['distro_code'].'<br />';
			//echo $value['_declined'].'<br />';
			if($value['distro_code'] == '' || $value['_declined']){
			//var_dump($value);
				//echo "found unassigned::".$value['id'].", Declined::".$value['_declined']."<br />";
				$unass_id = $value['id'];
				foreach($value as $att=>$val){
					$unassigned[$unass_id][$att] = $val;
					//echo "unassigned[".$unass_id."][".$att."]=".$val."<br />";
				}
				/*if($current_access == 'mercury' && $value['lead_source'] == 'mercury'){
					$merc_unass_count++;
				}*/
			}
		}
		$unass_count = count($unassigned);
		//echo "total unassigned::".$unass_count.", total unaccepted:: ".$all_count;
		
		
		function lead_color_u($stat, $div,$color_class){
		//echo 'stat='.$stat.' div='.$div.' cc='.$color_class;
			if($stat === $div){
				//echo '  true';
				return $color_class;
			}
		}
		if($current_access == 'mercury'){
			$title ='<li class="tab"><a href="#unacc_tab">UNASSIGNED</a></li>
		';
		}else{
		$title ='<li class="tab"><a href="#unacc_tab">
			<span style="background-image:url(./images/circ_leads.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$unass_count.'
			</span>
			UNASSIGNED</a></li>
		';
		}
		$content ='<div id="unacc_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">UNACCEPTED LEADS</div>
					';
		if($current_access == 'admin'){
		$content .= '<div id="leads_top_right" style="text-align:right;width:150px;float:right;margin-right:20px;position:relative;padding-top:25px;">
					<img src="./images/btn_create_lead.png" alt="btn_apply" class="new_lead_btn"/></span>
					</div>
					';
		}
		$content .= '</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
		$content .= '<div class="adjust_body">';
		
		//if($current_access == 'admin'){
		$content .= '<div id="unacc_lead_desc">
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:160px;">LOCATION</span>
			<span style="width:156px;">EMAIL</span>
			<span style="width:113px;">NAME</span>
			<span style="width:110px;">PHONE</span>
			<span style="width:80px;">DECLINED BY</span>
			<span style="width:80px;">ASSIGN</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>
		<div id="unassigned_lead_acc_wrapper">
		';
		//}
		
		//$content .= '<div id="unassigned_lead_acc_wrapper">';
		foreach($unassigned as $key=>$attrs){
			$lead_stat = 'accept';
			$lead_source = '';
			if($attrs['lead_source'] == 'mercury'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}else if($attrs['lead_source'] == 'mercury_bleed'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini_bleed.png" alt="mm"/></span>';
			}
			//echo $lead_stat.'==><br/> ';
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_accept';
			if($attrs['contacted'] != '0000-00-00 00:00:00') $circ_image_ref = 'circ_contact';
			if($attrs['made_demo_account'] != '0000-00-00 00:00:00') $made_demo_account =  TRUE; else $made_demo_account = FALSE;
			if ($made_demo_account) $circ_image_ref .= '_has_demo_account';
			if($attrs['declined_by'] != ''){
				$circ_image_ref = 'circ_black';
			} 
			if($attrs['created'] != '0000-00-00 00:00:00'){
				$birth = $attrs['created'];
				$age = time_from($birth);
				//echo $attrs['company_name'].', '.$birth.'--'.$age.'<br />';
			}
			//echo 'before_access = '.$current_access;
			if($current_access == 'admin'){
//			echo 'eat iitttt<br />';
			$content .= '<div class="acc_head accordionButton">
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:150px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;" class="hover_flow">'.$attrs['email'].'</span><div class="loc_head_div">|</div>
				<span style="width:95px;" class="hover_flow">'.$attrs['firstname'].' '.$attrs['lastname'].'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" class="hover_flow">'.$attrs['phone'].'</span><div class="loc_head_div">|</div>
				<span style="width:70px;" class="hover_flow">'.$attrs['declined_by'].'</span><div class="loc_head_div">|</div>
				<span style="width:60px;" class="hover_flow"><img src="./images/btn_assign.png" alt="assign" style="height:23px;" class="assign_lead"/></span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.'
				<br class="clear:left;">
				</div>		
				';
				//if($current_access == 'mercury')
			}else {
				//echo 'mercury access='.$attrs['lead_source'].', '.$attrs['company_name'].', '.$attrs['distro_code'].'<br />';
				if($current_access == 'mercury'){
				//echo 'mercury access2<br/ >';
					$content .= '<div class="acc_head accordionButton">
						<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
						<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
						<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
						<span style="width:150px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
						<span style="width:150px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
						<span style="width:70px;" class="hover_flow">'.$attrs['declined_by'].'</span><div class="loc_head_div">|</div>
						<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>							'.$lead_source.' 
						<br class="clear:left;">
				</div>		
				';
				}
			}
			$content .= '<div class="acc_lead_content accordionContent">
				<div class="lead_acc_content_head"><img src="./images/prog_bar_full.png" alt="p_b_f"/></div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color_u($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>
				
			</div>';
			
				
		}//for each unassigned
		 
		 //in progress parts
		 
		//all in progress part
		//if($current_access == 'admin'){
			$content .= '</div>';
		//}
		$content .= '<div style="margin:20px;text-align:center;font:30px verdana;color:#439789;">ALL IN-PROGRESS LEADS</div>';
		if($current_access == 'admin'){
		$content .='<div id="unacc_lead_desc">
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:160px;">TOTAL AGE(Days Hrs:min:sec)</span>
			<span style="width:156px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:105px;">REMIND</span>
			<span style="width:80px;">RESELLER</span>
			<span style="width:80px;">ASSIGN</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>';
		}
		if($current_access == 'mercury'){
		$content .='<div id="unacc_lead_desc">
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:160px;">TOTAL AGE(Days Hrs:min:sec)</span>
			<span style="width:156px;">TIME SINCE LAST</span>
			<span style="width:109px;">ASSIGNED</span>
			<span style="width:150px;">RESELLER</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>';
		 }
		//foreach($in_progress as $key=>$attrs)
		foreach($in_progress as $key=>$attrs){
			//$lead_stat = 'accept';
			$lead_source = '';
			$can_expand = '<span style="width:20px;"></span>';
			if($attrs['lead_source'] == 'mercury'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini.png" alt="mm"/></span>';
			}else if($attrs['lead_source'] == 'mercury_bleed'){
				$lead_source = '<span style="width:15px;"><img src="./images/i_merc_mini_bleed.png" alt="mm"/></span>';
			}
			$color_accept = 'color_accept';
			$circ_image_ref = 'circ_accept';
			if(preg_replace('/[1-9]/', '', $attrs['contacted']) != '0000-00-00 00:00:00') {
				if(preg_replace('/[1-9]/', '', $attrs['made_demo_account']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}else{
					$circ_image_ref = 'circ_demo_account';
					$lead_stat = 'demo_account';
	
				}
			}else if(preg_replace('/[1-9]/', '', $attrs['accepted']) != '0000-00-00 00:00:00'){
				if(preg_replace('/[1-9]/', '', $attrs['made_demo_account']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
					 
				}else{
					$circ_image_ref = 'circ_contact';
					$lead_stat = 'contact';
				}
			}else if(preg_replace('/[1-9]/', '', $attrs['made_demo_account'])!= '0000-00-00 00:00:00'){
				if(preg_replace('/[1-9]/', '', $attrs['accepted']) != '0000-00-00 00:00:00'){
					$circ_image_ref = 'circ_apply_license';
					$lead_stat = 'apply_license';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}else{
					$circ_image_ref = 'circ_accept_has_demo_account';
					$lead_stat = 'accept';
					$can_expand = '<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>';
				}
			}else{
				$circ_image_ref = 'circ_accept';
			}
			
			$time_values = get_lead_times($attrs);
			
			$total_age = $time_values['first'];
			$last_step_time = $time_values['most_recent'];
			$assigned_time = $time_values['assigned'];
				
			$last_step_time = str_replace("-", "", $last_step_time);
			$total_age = str_replace("-", "", $total_age);
			
			$distro_username = $attrs['distro_code'];
			$distro_phone = $resellers[$distro_username]['phone'];
			$distro_email = $resellers[$distro_username]['email'];
			$distro_company = $resellers[$distro_username]['company'];
			$distro_name = $resellers[$distro_username]['f_name'].' '.$resellers[$distro_username]['l_name'];
			$tooltip_id = $attrs['id'].'_'.$distro_username.'_content';
			
			$distro_tooltip = '
			<div id="'.$tooltip_id.'" style="display:none;">
				<div><strong>Name: </strong>'.$distro_name.'</div>
				<div><strong>Company: </strong>'.$distro_company.'</div>
				<div><strong>Phone: </strong>'.$distro_phone.'</div>
				<div><strong>Email: </strong>'.$distro_email.'</div>
			</div>';//$distro_tooltip
			
			$from_mercury = false;
			$j_decode = new Json();
				$accepts_mercury = $j_decode->decode($resellers[$distro_username]['special_json']);
				$merc_dist = $accepts_mercury->{"mercuryAcceptsList"};
				//echo $merc_dist.'<br />';
				if($merc_dist){
					$from_mercury = true;
					//$merc_logo='<img src="./images/i_merc_mini.png" alt="mm" />';
					//echo $merc_dist.'||  '.$distro_username.'<br />';
				}
			
			
			if($current_access == 'admin'){
			$attrs_for_jab = 'b_name="'.$attrs['company_name'].'" email="'.$attrs['email'].'" phone="'.$attrs['phone'].'" contact="'.$attrs['firstname'].' '.$attrs['lastname'].'" reseller="'.$attrs['distro_code'].'"';
			$content .= '<div class="acc_head accordionButton">
				'.$distro_tooltip.'
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:140px;" class="lead_on_comp_name">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;">'.$total_age.'</span><div class="loc_head_div">|</div>
				<span style="width:140px;" >'.$last_step_time.'</span><div class="loc_head_div">|</div>
				<span style="width:110px;">'.$assigned_time.'</span><div class="loc_head_div">|</div>
				<span style="width:50px;text-align:center;" class="jab_distro" '.$attrs_for_jab.'><img src="./images/jab.png" alt="jab" /></span><div class="loc_head_div">|</div>
				<span style="width:110px;" class="distro_tip" tipid="#'.$tooltip_id.'">'.$attrs['distro_code'].'</span><div class="loc_head_div">|</div>
				<span style="width:60px;"><img src="./images/btn_assign.png" alt="assign" style="height:23px;" class="assign_lead"/></span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.'
				<br class="clear:left;">
				</div>		
				';
			}else if($current_access == 'mercury'){
				if($attrs['lead_source'] == 'mercury' || $from_mercury){
				
			
			$content .= '<div class="acc_head accordionButton">
				'.$distro_tooltip.'
				<img class="acc_arrow" src="./images/arrow_left.png" style="float:left;" alt="arr_l"/>
				<span style="width:23px;"><img class="acc_status" src="./images/'.$circ_image_ref.'.png" alt="'.$circ_image_ref.'" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:140px;" class="lead_on_comp_name">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;">'.$total_age.'</span><div class="loc_head_div">|</div>
				<span style="width:140px;" >'.$last_step_time.'</span><div class="loc_head_div">|</div>
				<span style="width:110px;">'.$assigned_time.'</span><div class="loc_head_div">|</div>
				
				<span style="width:130px;" class="distro_tip" tipid="#'.$tooltip_id.'">'.$attrs['distro_code'].'</span><div class="loc_head_div">|</div>
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.'
				<br class="clear:left;">
				</div>		
				';
				}
			}
			$content .= '<div class="acc_lead_content accordionContent">
				<div class="lead_acc_content_head"><img src="./images/prog_bar_full.png" alt="p_b_f"/></div>
				<div class="lead_acc_current">
					<div class="lead_level_text_boxes">
						<div class="lead_level_text '.lead_color_u($lead_stat,'accept','color_accept').'">Accept</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'contact','color_contact').'">Contact</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'demo_account','color_demo_account').'">Demo Account</div>
						<div class="lead_level_text '.lead_color_u($lead_stat,'apply_license','color_apply_license').'">Apply License</div>
						<br style="clear:left" />
					</div>
				</div>
				<div style="margin-bottom:20px;">Lead Details:</div>
				<div class="in_progress_details" style="height:200px;">
					<div style="width:400px;float:left;text-align:left;margin:20px 5px 20px 50px;font:12px verdana;line-height:175%;">
						<div><span class="lead_detail_title">Location:</span><span>'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span></div>
						<div><span class="lead_detail_title">Name:</span><span>'.$attrs['firstname'].' '.$attrs['lastname'].'</span></div>
						<div><span class="lead_detail_title">Email:</span><span>'.$attrs['email'].'</span></div>
						<div><span class="lead_detail_title">Phone:</span><span>'.$attrs['phone'].'</span></div>
					</div>
					<div style="width:450px;float:left;font:12px verdana;line-height;175%;text-align:left;">
						<div><span style="font-weight:bold;color:black;">Contacted info:</span><span>'.$attrs['contact_note'].'</span></div>
					</div>
				</div>
				
			</div>';
		}//foreach($in_progress as $key=>$value)
		 
		$content .= '</div></div>';
		/*lead_type_content($lead_stat, $attrs['data_name']?$attrs['data_name']:$attrs['company_name']).'</div>*/
		
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//unassigned_tab
	private function add_resellers_tab($resellers){
		//self::$current_user->set_all_resellers();
		//$resellers = self::$current_user->get_all_resellers();
		//echo 'reseller_count:: '.count($resellers).'<br />';
		/*foreach($resellers as $key=>$value){
			echo $key.'<br />';
		}*/
		$title ='<li class="tab"><a href="#resellers_tab">RESELLERS</a></li>
		';
		
		$content ='<div id="resellers_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;cursor:pointer;">RESELLERS</div>
					<div style="border:1px solid red;float:right;text-align:center;width:200px;font:20px verdana;display:none;margin-top:22px;background:#aecd37;cursor:pointer;" id="submit_changes">SUBMIT CHANGES</div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
		$content .= '<div class="adjust_body"><div id="unacc_lead_desc">
			<span style="width:200px;">NAME</span>
			<span style="width:200px;">COMPANY</span>
			<span style="width:154px;">CITY-STATE</span>
			<span style="width:190px;">EMAIL</span>
			<span style="width:65px;">LOGIN</span>
			<span style="width:50px;">LEVEL</span>
			<span style="width:50px;">REMOVE</span>
			<br style="clear:left;" />													
		</div>
		';
		
		$content .= '
		<div id="account_acc_wrapper">';
		
		
		//get information for reseller accordians.
		
		
		foreach($resellers as $key=>$value){
			//echo 'info<br/>'.$resellers[$key]['f_name'].' '.$resellers[$key]['l_name'].', '.$resellers[$key]['username'].'<br />';
		//build head
			$username = $resellers[$key]['username'];
			//<img class="acc_arrow expand_account_btn" src="./images/arrow_left.png" style="float:left;" alt="a_l"/>\
			if($resellers[$key]['access'] == 'admin'){
				$level = 'ADMIN';
			}else if($resellers[$key]['access']=='' || $resellers[$key]['access']=='deactivated'){
				$level = '<img src="./images/stars_0.png" alt="not_set" />';
			}
			else{
				$level = '<img src="./images/stars_'.$resellers[$key]['access'].'.png" alt="not_set" />';
			}
			$content .= '<div class="acc_head accordionButton">				
				<span style="width:200px;">'.$resellers[$key]['f_name'].' '.$resellers[$key]['l_name'].'</span>
				<span style="width:210px;">'.$resellers[$key]['company'].'</span>
				<span style="width:140px;">'.$resellers[$key]['city'].'-'.$resellers[$key]['state'].'</span>
				<span style="width:190px;">'.$resellers[$key]['email'].'</span>
				<span style="width:70px;text-align:center;"><a href="./view_reseller.php?function=login_reseller&username='.$username.'" target="_blank">login</a></span>
				<span style="width:60px;color:#aecd37;"class="d_level_stars" val="span_d_level_'.$resellers[$key]['username'].'" id="form_d_level_'.$resellers[$key]['username'].'_prompt">'.$level.'</span>
				<span style="width:50px;display:none;" id="span_d_level_'.$resellers[$key]['username'].'">
					<form class="change_d_level" id="form_d_level_'.$resellers[$key]['username'].'">
					<select id="change_level_select'.$resellers[$key]['username'].'" name="change_level_select" val="form_d_level_'.$resellers[$key]['username'].'" class="change_d_level_select">
						<option value=""></option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
					</select>
					<input type="hidden" name="distro_username" value="'.$resellers[$key]['username'].'">
				</form>
				</span>
				<span style="color:#aecd37;cursor:pointer;width:50px;" class="remove_reseller" id="remove_'.$resellers[$key]['username'].'" username="'.$resellers[$key]['username'].'">remove</span>
				<span style="color:red;cursor:pointer;width:50px;display:none;" id="activate_'.$resellers[$key]['username'].'" class="activate_reseller" username="'.$resellers[$key]['username'].'">activate</span>
				<br class="clear:left;">
				</div>';//acc_head
				
		//build content
			$content .= '<div class="acc_reseller_content accordionContent">';
			$content .= '</div>';//acc_resellser_content
		}
		$content .= '</div></div></div>';//account_acc_wrapper
		
		
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
		
	}//add_resellers_tab
	private function add_processed_tab(){
		$processed = self::$current_user->get_all_processed();
		$p_count = count($processed);
		
		$title ='<li class="tab" style="width:30px;"><a href="#proc_tab">
			<span style="background-image:url(./images/circ_complete.png);background-repeat:no-repeat;display:inline-block;width:21px;">
				'.$p_count.'
			</span></a></li>
		';
		
		$content ='<div id="proc_tab" class="tab_section tab00">';
		$content .= '<div class="tab_top" style="height:70px;">
				<div class="tab_top_accnts">
					<div style="font:24px Verdana;color:#717074;float:left;padding-top:17px;">FINISHED LEADS</div>
					<div style="font:24px Verdana;color:#aecd37;float:left;padding-top:17px;" id="look_for_finished">REFRESHING!!</div>
				</div>
					<div class="tab_top_border" style="position:relative; top:-17;"><img src="./images/divider_with_fade.png" alt="d_w_f" /></div>
				</div>
			';
		$content .= '<div class="adjust_body"><div id="unacc_lead_desc">
			<span style="width:50px;">STATUS</span>
			<span style="width:140px;">BUSINESS NAME</span>
			<span style="width:160px;">LOCATION</span>
			<span style="width:156px;">EMAIL</span>
			<span style="width:113px;">NAME</span>
			<span style="width:110px;">PHONE</span>
			<span style="width:110px;">DISTRO</span>
			<span>NOTES</span>
			<br style="clear:left;" />													
		</div>
		';
		
		$content .= '<div id="processed_lead_acc_wrapper" style="height:50%;">
		';
		
		foreach($processed as $key=>$attrs){
			
			$content .= '<div class="acc_head accordionButton">
				<span style="width:23px;"><img class="acc_status" src="./images/circ_blue.png" alt="circ_blue" /></span>
				<input type="hidden" name="lead_on_lead_id" value="'.$attrs['id'].'" />
				<span style="width:150px;" class="hover_flow">'.$attrs['company_name'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;"class="hover_flow">'.$attrs['city'].' '.$attrs['state'].', '.$attrs['zip'].'</span><div class="loc_head_div">|</div>
				<span style="width:150px;" class="hover_flow">'.$attrs['email'].'</span><div class="loc_head_div">|</div>
				<span style="width:95px;" class="hover_flow">'.$attrs['firstname'].' '.$attrs['lastname'].'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" class="hover_flow">'.$attrs['phone'].'</span><div class="loc_head_div">|</div>
				<span style="width:100px;" class="hover_flow">'.$attrs['distro_code'].'</span><div class="loc_head_div">|</div>
				
				<span class="notes_icon" reseller="'.$attrs['distro_code'].'" comp_id="'.$attrs['id'].'" notes="'.$attrs['notes'].'" contact="'.$attrs['contact_note'].'" contacted="'.$attrs['contacted'].'" style="width:23px;"><img src="./images/i_notes.png" alt="i_n"/></span>
				'.$lead_source.'
				<br class="clear:left;">
				</div>		
				';
				
		}
		
		$content .= '</div></div>';
		
		self::$tab_list .= $title;
		self::$tab_sections .= $content;
	}//add_processed_tab
	
	//end of tab content funcitions
	//
	//
	// returns header section (might do this before class gets built)
	private function build_header(){
		function get_redirect(){
			$rd = '';
			$rd .= '
			<div style="text-align:center;">
				<form method="post" action="https://admin.poslavu.com/distro/">
				<input type="hidden" name="old_redirect" value="old" />
				Welcome to the new Lavu Reseller backend! We are currently in Beta mode of this backend.<br />
				If you wish to return to the old reseller backend, please click <input type="submit" value="here" style="curser:pointer;" />
				</form>
			</div>';
			return $rd;
		}
		$a_lead_info_strings = $this->build_leads_info_array();
		$header = '<div id="distro_header">
			<div id="distro_header_middle">
				<img src="./images/logo_distro_icon.png" style="margin:10px;" alt="l_d_i"/>
				<span style="position: relative;left: 20px;bottom: 25px;">DISTRIBUTOR PORTAL</span>
				<span style="float:right; position: relative;right: 150px;top: 25px;">'.self::$current_user->get_info_attr('f_name').' '.self::$current_user->get_info_attr('l_name').'</span><span style="float:right; position: relative;right: -50px;top: 25px;"><a href="index.php?logout=1">(logout)</a></span>
			</div>
		</div>'.get_redirect().'
		<script type="text/javascript">
			lead_info=[];
			'.implode(';
			',$a_lead_info_strings).'
		</script>
		';
		echo $header;
		
	}//build_header
	private function build_leads_info_array(){
	
	
		// leads text
		$a_lead_info_strings = array();
		foreach(self::$current_user->get_leads() as $a_lead) {
			$a_lead_info_strings[] = 'lead_info['.$a_lead['id'].']=[]';
			foreach($a_lead as $k=>$v) {
				$a_lead_info_strings[] = 'lead_info['.$a_lead['id'].']["'.$k.'"]="'.$v.'"';
			}
		}
		return $a_lead_info_strings;
	}//build_leads_info_array
	private function build_header_admin(){
		function get_redirect_admin(){
			$rd = '';
			$rd .= '
			<div style="text-align:center;">
				<form method="post" action="https://admin.poslavu.com/distro/">
				<input type="hidden" name="old_redirect" value="old" />
				Welcome to the new Lavu Reseller backend! We are currently in Beta mode of this backend.<br />
				If you wish to return to the old reseller backend, please click <input type="submit" value="here" style="curser:pointer;" />
				</form>
			</div>';
			return $rd;
		}
	
		$a_lead_info_strings = $this->build_leads_info_array();
		//header text
		$header = '<div id="distro_header_admin" style="border-top:3px solid #aecd37;">
			<div id="distro_header_middle">
				<img src="./images/logo_distro_icon.png" style="margin:10px;" alt="l_d_i"/>
				<span style="position: relative;left: 20px;bottom: 25px;color:#aecd37;">ADMIN</span>
				<span style="float:right; position: relative;right: 150px;top: 25px;color:#aecd37;">'.self::$current_user->get_info_attr('f_name').' '.self::$current_user->get_info_attr('l_name').'</span><span style="float:right; position: relative;right: -50px;top: 25px;"><a href="index.php?logout=1">(logout)</a></span>
			</div>
		</div>'.get_redirect_admin().'
		<script type="text/javascript">
			lead_info=[];
			'.implode(';
			',$a_lead_info_strings).'
		</script>
		';
		echo $header;
		
	}//build_header_admin
	
	private function build_header_mercury(){
		
	
		//header text
		$header = '<div id="distro_header_admin" style="border-top:3px solid #aecd37; background:#0684b5;height:75px;">
			<div id="distro_header_middle">
				<img src="./images/logos_merc_lavu.png" style="margin:10px;" alt="l_d_i"/>
				<span style="float:right; position: relative;right: 100px;top: 25px;"><a href="index.php?logout=1">(logout)</a></span>
			</div>
		</div>';
		echo $header;
		
	}//build_header_mercury
	
	private static function display_datetime($dt,$type="both"){
		$dt_parts = explode(" ",$dt);
		if(count($dt_parts) > 1)
		{
			$d_parts = explode("-",$dt_parts[0]);
			$t_parts = explode(":",$dt_parts[1]);
			if(count($d_parts) > 2 && count($t_parts) > 2)
			{
				$ts = mktime($t_parts[0],$t_parts[1],$t_parts[2],$d_parts[1],$d_parts[2],$d_parts[0]);
				if($type=="date")
					return date("m-d-y",$ts);
				else if($type=="time")
					return date("h:i a",$ts);
				else
					return date("m-d-y h:i a",$ts);
			}
			else return $dt;
		}
		else return $dt;
	}//display_datetime
	
	//Used to determine what level to display for each Distro's restaurants
	private static function display_level($trial_end,$original_license,$current_package_name, $dname, $s_licenses_string=''){
		global $o_package_container;
		$s_retval = '';
		
		if($trial_end=="" && $original_license==""){//alt signup
			$s_retval .= 'Alternative License';
		}else if($current_package_name!=""){//Lite,platinum,gold
			if(strpos($current_package_name,'Platinum') != 0){//Lite or Gold
				$s_retval .= $o_package_container->get_printed_name_by_attribute('name', $current_package_name);
			}else{
				$s_retval .= $o_package_container->get_printed_name_by_attribute('name', $current_package_name);
			}
		}else{//Apply License
			$s_retval .= '<img src="./images/btn_applylicense__.png" alt="btn_apply" class="apply_license_btn" val="'.$dname.'" />
			<input type="hidden" name="available_licenses" id="available_licenses" value="'.$s_licenses_string.'" />';
		}
		
		// check for upgrades
		$a_available_upgrades_for_package = $o_package_container->get_available_upgrades($current_package_name, TRUE);
		if (count($a_available_upgrades_for_package))
			$s_retval .= '<img src="./images/btn_upgrade.png" style="margin-left:10px;width:15px;" alt="btn_upg" class="upgrade_license_btn"/>
			<input type="hidden" name="available_upgrades" value="'.implode('|', $a_available_upgrades_for_package).'">';
		
		return $s_retval;
	}//display_level
	
	/*private function color_level($lead_level,$stat){
			if(strpos($lead_stat,'accept')<0 && $stat=='accept'){
				return "color_accept";
			}else if(strpos($lead_stat,'contact')<0 && $stat=='contact'){
				return "color_contact";
			}else if(strpos($lead_stat,'demo')<0&& $stat=='demo'){
				return "color_demo_account";
			}else{
				return "color_apply_license";
			}
		}*/	
	
}




///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**other idea for tabs

// this is the class that represents the father of the tabs that can be added to the view
abstract class tab{

}

//these are the concrete tabs. 
class news_tab extends tab{
	// add content to tab_list
	private $title =
	'<li class="tab"><a href="">NEWS</a></li>
	';
	
	// add content to tab_sections 	
	private $content =
	'<div id="news_tab" class="">
	</div>
	';//all_content;
	
}
class leads_tab extends tab{

	private function get_leads(){
		//cycle through leads of distibutor and
		//  display.
		//
	}

}
class accounts_tab extends tab{

    // create account button
    // apply license button
    // lavu points display
    //accounts display
    
	private function get_accounts(){
		// cycle through accounts and build 
		// table for all of them.
	}
	
	
}
class resources_tab extends tab{
	
}
class my_info_tab extends tab{
	
}
*/


