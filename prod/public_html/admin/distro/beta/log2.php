<?php
/**
log2.php
author: Martin Tice
property of Lavu inc.

This is the login logic/functions for the NEW DISTRO PORTAL. 

**/


	ini_set("display_errors",1);
	session_start();
	require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	$maindb = "poslavu_MAIN_db";

	// Is a distro guy logged in
	$loggedin = (isset($_SESSION['posdistro_loggedin']))?$_SESSION['posdistro_loggedin']:false;
	if($loggedin=="") $loggedin = false;