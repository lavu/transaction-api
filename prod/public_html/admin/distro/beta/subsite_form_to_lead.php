<?php
ini_set("display_errors",1);
require_once("../../cp/resources/core_functions.php");
require_once("../../cp/resources/lavuquery.php");



//error_log(var_dump($_POST));
//error_log("++++++++++++++in correct error log".count($_POST));


/*if (isset($_POST["request_info"])){
	error_log('Requested info');
	//add_request_more_info();
}else if(isset($_POST["free_trial_page"])){
	error_log('GET STARTED');
	//add_get_started();
}*/

if(isset($_REQUEST['form'])){
	//error_log('--------'.$_REQUEST['form']);
	if(function_exists($_REQUEST['form'])){
		$_REQUEST['form']();
	}
	
}

function long_form(){

	/*require_once('https://admin.poslavu.com/distro/beta/exec/add_lead_from_try.php?form=long_form&name='.$r_fields['name'].'&email='.$r_fields['email'].'&phone='.$r_fields['phone'].'&b_name='.$r_fields['business_name'].'&address='.$r_fields['address'].'&city='.$r_fields['city'].'&state='.$r_fields['state']);*/
	$fields = array();
	$fields['db'] = '`poslavu_MAIN_db`.`reseller_leads`';
	$fields['firstname'] = $_REQUEST['firstname'];
	$fields['lastname'] = $_REQUEST['lastname'];
	$fields['company_name'] = str_replace("___", " ", $_REQUEST['b_name']);
	$fields['email'] = str_replace('___', ' ', $_REQUEST['email']);
	$fields['phone'] = str_replace('___', ' ', $_REQUEST['phone']);
	$fields['address'] = str_replace('___', ' ', $_REQUEST['address']);
	$fields['city'] = str_replace('___', ' ', $_REQUEST['city']);
	$fields['state'] = str_replace('___', ' ', $_REQUEST['state']);
	$fields['lead_source'] = $_REQUEST['lead_source'];
	$fields['created'] = date("Y-m-d H:i:s");
	$fields['contact_note'] = $REQUEST['contact_note'];
	
	//error_log('-------'.$fields['firstname'].', '.$fields['lastname'].', '.$fields['company_name'].', '.$fields['email'].', '.$fields['phone'].', '.$fields['address'].', '.$fields['city'].', '.$fields['state'].', '.$fields['lead_source']);
	
	$q_string ="insert into [db] (`firstname`, `lastname`, `address`, `city`, `state`,`company_name`, `email`, `phone`, `lead_source`, `created`, `contact_note`) values('[firstname]', '[lastname]', '[address]', '[city]', '[state]', '[company_name]', '[email]', '[phone]', '[lead_source]','[created]', '[contact_note]')";
	
	mlavu_query($q_string, $fields);
	
	error_log(mlavu_dberror());
	
	//mail()
	
	
}

function short_form(){

	$fields = array();
	$fields['db'] = '`poslavu_MAIN_db`.`reseller_leads`';
	$fields['firstname'] = $_REQUEST['firstname'];
	$fields['lastname'] = $_REQUEST['lastname'];
	$fields['email'] = str_replace('___', ' ', $_REQUEST['email']);
	$fields['phone'] = str_replace('___', ' ', $_REQUEST['phone']);
	$fields['lead_source'] = $_REQUEST['lead_source'];
	$fields['created'] = date("Y-m-d H:i:s");
	
	$q_string ="insert into [db] (`firstname`, `lastname`, `email`, `phone`, `lead_source`, `created` ) values('[firstname]', '[lastname]', '[email]', '[phone]', '[lead_source]','[created]')";
	
	mlavu_query($q_string, $fields);
	
	error_log(mlavu_dberror());
	
	echo $_REQUEST['callback'] . '(' . json_encode( array( 'success' => true ) ) . ')';
}
/*
+--------------+--------------+------+-----+---------+----------------+
| Field        | Type         | Null | Key | Default | Extra          |
+--------------+--------------+------+-----+---------+----------------+
| id           | int(11)      | NO   | PRI | NULL    | auto_increment | 
| product_name | varchar(255) | NO   |     | NULL    |                | 
| name         | varchar(255) | NO   |     | NULL    |                | 
| email        | varchar(255) | NO   |     | NULL    |                | 
| phone_number | varchar(255) | NO   |     | NULL    |                | 
+--------------+--------------+------+-----+---------+----------------+

*/

/**
* This is intended to be called to be called from 
* lavuhospitality.com using JSONP. If this function is called
* then 'hotels_waiting_list' was included as the form field
* in the request. 
*/
function hotels_waiting_list () {
    $fields = array();
    $fields['db'] = '`poslavu_MAIN_db`.`wait_list`';
    $fields['name']  = $_REQUEST['name'];
    $fields['email'] = $_REQUEST['email'];
    $fields['phone_number'] = $_REQUEST['phone'];
    $fields['product_name'] = 'hotels_microsite';
    $query  = "INSERT INTO [db] ";
    $query .= "(`name`, `email`, `phone_number`, `product_name`) ";
    $query .= "VALUES ('[name]', '[email]', '[phone_number]', ";
    $query .= "'[product_name]')";
    mlavu_query($query, $fields);
    echo $_REQUEST['callback'] . '(' . 
        json_encode( array( 'success' => true, "error" => mlavu_dberror() ) ) . 
    ')';
}

/**
* This is intended to be called to be called from 
* lavuhospitality.com using JSONP. If this function is called
* then 'hotels_waiting_list' was included as the form field
* in the request. 
*/
function foodtrucks_waiting_list () {
    $fields = array();
    $fields['db'] = '`poslavu_MAIN_db`.`wait_list`';
    $fields['name']  = $_REQUEST['name'];
    $fields['email'] = $_REQUEST['email'];
    $fields['phone_number'] = $_REQUEST['phone'];
    $fields['product_name'] = 'foodtrucks_microsite';
    $query  = "INSERT INTO [db] ";
    $query .= "(`name`, `email`, `phone_number`, `product_name`) ";
    $query .= "VALUES ('[name]', '[email]', '[phone_number]', ";
    $query .= "'[product_name]')";
    mlavu_query($query, $fields);
    echo $_REQUEST['callback'] . '(' . 
        json_encode( array( 'success' => true, "error" => mlavu_dberror() ) ) . 
    ')';
}

function waiting_list (){
    $fields = array();
    $fields['db'] = '`poslavu_MAIN_db`.`wait_list`';
    $fields['name']  = $_REQUEST['name'];
    $fields['email'] = $_REQUEST['email'];
    $fields['phone_number'] = $_REQUEST['phone'];
    $fields['product_name'] = $_REQUEST['origin'];
    $query  = "INSERT INTO [db] ";
    $query .= "(`name`, `email`, `phone_number`, `product_name`) ";
    $query .= "VALUES ('[name]', '[email]', '[phone_number]', ";
    $query .= "'[product_name]')";
    mlavu_query($query, $fields);
    error_log(mlavu_dberror());
    $fields = array();
	$fields['db'] = '`poslavu_MAIN_db`.`reseller_leads`';
	$fields['firstname'] = $_REQUEST['company_name'];
	$fields['email'] = str_replace('___', ' ', $_REQUEST['email']);
	$fields['phone'] = str_replace('___', ' ', $_REQUEST['phone']);
	$fields['lead_source'] = $_REQUEST['origin'];
	$fields['created'] = date("Y-m-d H:i:s");
	$q_string ="insert into [db] (`company_name`, `email`, `phone`, `lead_source`, `created` ) values('[company_name]', '[email]', '[phone]', '[lead_source]','[created]')";
	mlavu_query($q_string, $fields);
	
	error_log(mlavu_dberror());
	header('Content-type: application/javascript');
    echo $_REQUEST['callback'] . '(' . 
        json_encode( array( 'success' => true, "error" => mlavu_derror() ) ) . 
    ')';
}

function new_lavu_form() {
	$fields = array();
	$fields['db'] = '`poslavu_MAIN_db`.`reseller_leads`';
	$fields['company_name'] = str_replace("___", " ", $_REQUEST['b_name']);
	$fields['email'] = str_replace('___', ' ', $_REQUEST['email']);
	$fields['phone'] = str_replace('___', ' ', $_REQUEST['phone']);
	$fields['lead_source'] = $_REQUEST['lead_source'];
	$fields['created'] = date("Y-m-d H:i:s");
	
	//error_log('-------'.$fields['firstname'].', '.$fields['lastname'].', '.$fields['company_name'].', '.$fields['email'].', '.$fields['phone'].', '.$fields['address'].', '.$fields['city'].', '.$fields['state'].', '.$fields['lead_source']);
	
	$q_string ="insert into [db] (`company_name`, `email`, `phone`, `lead_source`, `created`) values('[company_name]', '[email]', '[phone]', '[lead_source]','[created]')";
	
	mlavu_query($q_string, $fields);
	
	error_log(mlavu_dberror());
	header('Content-type: application/javascript');
	echo $_REQUEST['callback'] . "({success:true})";
	//mail()
}
/**
 * Dummy function to sort two string by length, it is used
 * by split_name below
 * returns positive int if b is longer, 0 if equal and negative if a is
 * longer
 * @param $a string
 * @param $b string
 * @return int
 */
function sort_by_length ( $a, $b ){
    return strlen ( $b ) - strlen( $a );
}

/**
 * Splits a name in to first and last
 * Ex: if you call split_name("Mr Tester J. Testersson III Phd")
 *     it returns : ["Tester", "Testersson"]
 * @param $name string
 * @return array
 */ 
function split_name ( $name ) {
    $pieces = preg_split( "/[\s,]+/", $name );
    $pieces_copy = $pieces;
    usort( $pieces, "sort_by_length");
    $og_index1 = array_search( $pieces[0], $pieces_copy );
    $og_index2 = array_search( $pieces[1], $pieces_copy );
    if ( $og_index1 > $og_index2 ) {
        return array( $pieces[1], $pieces[0] );
    } else {
        return array( $pieces[0], $pieces[1] );
    }
}



function send_to_salesforce( $data ) {
    if ( !is_array( $data ) ) {
        parse_str( $data, $tmp );
        $data = $tmp;
    }
    $url = "https://webto.salesforce.com/servlet/servlet.WebToLead?";
    $data['encoding'] = "UTF-8";
    $data['oid'] = "00DF000000076Td";
    $curl_opts = array();
    $curl_opts[CURLOPT_RETURNTRANSFER] = 1;
    $mime = array( "Content-Type" => "application/x-www-form-urlencoded" );
    $curl_opts[CURLOPT_HTTPHEADER] = $mime;
    $curl_opts[CURLOPT_FOLLOWLOCATION] = 1 ;
    $curl_opts[CURLOPT_SSL_VERIFYPEER] = false;
    $curl_opts[CURLOPT_SSL_VERIFYHOST] = false;
    $curl_opts[CURLOPT_HEADER] = false;
    $curl_opts[CURLOPT_USERAGENT] = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)";
    $curl_opts[CURLOPT_URL] = $url . http_build_query( $data );
    $ch = curl_init();
    curl_setopt_array($ch, $curl_opts );
    return array(curl_exec( $ch ), curl_errno($ch), curl_error($ch) );
}

function lead_submit() {
	$jsonmime = "Content-Type: application/json";
	$jsmime = "Content-Type: application/javascript";
	// put lead in distro portal
	$e = str_replace( '___', ' ', trim( $_REQUEST['email'] ) );
	$p = str_replace( '___', ' ', trim( $_REQUEST['phone'] ) );
	$c = str_replace( "___", " ", trim( $_REQUEST['company'] ) );
	$n = !empty( $_REQUEST['name'] )? $_REQUEST['name'] : $_REQUEST['full_name'];
	$n = trim( $n );
    $is_js = !empty( $_REQUEST['jsonp'] );
	$mime = $is_js ? $js : $jsonmime;
	$name = split_name( $n );
	$f = $name[0];
	$l = $name[1];
	if ( empty( $e ) || empty( $p ) || ( empty( $c ) && empty( $n ) ) )  {
		$retval = "{success:false,errmsg:\"Missing arguments\"}";
		header($mime);
		$ret = $is_js ? $_REQUEST['callback'] . ".($retval)" : $retval;	
		echo $ret;
	}
	require "verify-email.php";
	if ( !verifyEmail( $e, 'info@poslavu.com', $details ) ) {
		$err_msg = implode( '||', $details );
		$retval = "{success:false,err_msg:\"$err_msg\"}";
		header( $mime );
		$ret = $is_js ? $_REQUEST['callback'] . ".($retval)" : $retval;
	    echo $ret;
	    exit;
	}
	$fields = array();
	$fields['db'] = '`reseller_leads`';
	$fields['company_name'] = $c;
	$fields['firstname'] = $f;
	$fields['lastname'] = $l;
	$fields['email'] = $e;
	$fields['phone'] = $p;
	$ref = !empty( $_REQUEST['referrer'] ) ? $_REQUEST['referrer'] : "";
	$fields['lead_source'] = "{$_REQUEST['lead_source']}_$ref";
	$fields['created'] = date("Y-m-d H:i:s");
	if ( !empty( $_REQUEST['session_id'] ) ) {
		$fields['session_id'] = $_REQUEST['session_id'];
	}
	//error_log('-------'.$fields['firstname'].', '.$fields['lastname'].', '.$fields['company_name'].', '.$fields['email'].', '.$fields['phone'].', '.$fields['address'].', '.$fields['city'].', '.$fields['state'].', '.$fields['lead_source']);
	
	$q_string ="insert into [db] (`company_name`, `email`, `phone`, `lead_source`, `created`) values('[company_name]', '[email]', '[phone]', '[lead_source]','[created]')";
	
	mlavu_query($q_string, $fields);
	//get the id from the inserted row
	$id = mlavu_insert_id();

	//prepare dat for salesforce
	$data = array();
	$data['00NF000000Aao6L'] = empty( $_REQUEST['00NF000000Aao6L'] )? $_REQUEST['level'] : $_REQUEST['00NF000000Aao6L'];
	$data['email'] = $e;
	$data['phone'] = $p;
	$data['company'] = $c;
	if ( !empty( $_REQUEST['zip'] ) ) {	$data['Zip_Postal_Code__c'] = $_REQUEST['zip']; }
	$data['first_name'] = $f;
	$data['last_name'] = $l;
	$data["Description"] = "dbid:$id";
	$data["Description__c"] = "dbid:$id";
	//insert in to salesforce
	if ( empty( $_SERVER['DEV'] ) ) send_to_salesforce( $data );
	// log errors and return
	error_log(mlavu_dberror());
	$retval = "{success:true,dbid:$id}";
	header( $mime );
	$ret = $is_js ? $_REQUEST['callback'] . ".($retval)" : $retval;	
	echo $ret;
}
