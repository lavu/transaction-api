<?php

	/**
	 * Defines all of the licenses and upgrades available for distributors to buy in the distro portal.
	 * ALL licenses and ALL upgrades are both stored in $a_license_types.
	 * Default menu builds are defined int $a_new_account_label_names.
	 */

	if (!isset($loggedin))
		$loggedin = $_SESSION['posdistro_loggedin'];
	require_once(dirname(__FILE__)."/../../sa_cp/billing/package_levels_object.php");
	if (!isset($o_package_container))
		$o_package_container = new package_container();

	// get the highest package level names
	$a_highest_license_names = array(25 => 'Lavu', 26 => 'LavuEnterprise');
	$a_highest_license_names_mapping = array();
	foreach($a_highest_license_names as $k=>$v) {
		$i_level = $o_package_container->get_level_by_attribute('name', $v);
		$s_baselevel_name = $o_package_container->get_plain_name_by_attribute('level', $i_level);
		$a_highest_license_names_mapping[$s_baselevel_name] = $v;
	}

	// for buying a new license
	$a_license_types = array();//want this
	$a_license_names = $a_highest_license_names;
	foreach($a_license_names as $s_license_name) {
		$i_license_value = $o_package_container->get_value_by_attribute('name', $s_license_name);
		$s_license_value = (string)$i_license_value;
		$a_license_types[] = array($s_license_name, $s_license_value, $s_license_value);
	}
 
	// for upgrading a license
	$a_licenses_to_upgrade_names = $a_highest_license_names;
	foreach($a_licenses_to_upgrade_names as $s_license_name) {
		$a_upgrade_full_name = $o_package_container->get_available_upgrades($s_license_name, TRUE);
		foreach($a_upgrade_full_name as $s_upgrade_full_name) {
			$s_upgrade_full_name_parts = $o_package_container->get_upgrade_old_new_names($s_upgrade_full_name);
			$s_upgrade_old_name = $s_upgrade_full_name_parts[0];
			$s_upgrade_new_name = $s_upgrade_full_name_parts[1];
			$s_upgrade_value = (string)$o_package_container->get_upgrade_cost_by_old_new_names($s_upgrade_old_name, $s_upgrade_new_name);
			$a_license_types[] = array($s_upgrade_full_name, $s_upgrade_value, $s_upgrade_value);
		}
	}

	// for creating a new account
	$a_new_account_label_names = array();
	if($loggedin=="robw") $a_new_account_label_names['ersdemo']='LaserTag';
	$a_new_account_label_names[' '] = '&nbsp;';
	$a_new_account_label_names['default_coffee']='Coffee Shop';
	$a_new_account_label_names['defaultbar']='Bar/Lounge';
	$a_new_account_label_names['default_pizza_']='Pizza Shop';
	$a_new_account_label_names['default_restau']='Restaurant';
	$a_new_account_label_names['sushi_town']='Original Sushi';
	$a_new_account_label_names['lavu_fit_demo']='Lavu Fit';
?>
