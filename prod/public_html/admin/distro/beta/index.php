<?php

	if (getenv('DEV') == '1') {
		@define('DEV', 1);
		ini_set("display_errors","1");
		error_reporting(error_reporting() & ~E_NOTICE & ~E_DEPRECATED & ~E_WARNING);
	} else {
		@define('DEV', 0);
               ini_set("display_errors","0");
                error_reporting(error_reporting() & ~E_NOTICE);
	}
	require_once("/home/poslavu/public_html/admin/cp/resources/session_functions.php");
	standarizeSessionDomain();


	//error_reporting(E_ALL);
	session_start();
	ini_set("display_errors",1);
	ini_set("memory_limit","128M");
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once("/home/poslavu/public_html/register/authnet_functions.php");
	require_once(dirname(__FILE__)."/distro_user.php");
	require_once(dirname(__FILE__)."/distro_tab_container.php");
	require_once(dirname(__FILE__)."/distro_data_functions.php");
	require_once(dirname(__FILE__)."/exec/distro_user_functions.php");

	if (isset($_SESSION['update_accounts'])){
		//echo 'index'.$_SESSION['update_accounts'];
	}
	$maindb = "poslavu_MAIN_db";
	$header = '<!DOCTYPE html><html><head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/dnew.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery.calendars.picker.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery.tooltip.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery-ui-1.10.0.custom.css" />
		<link rel="stylesheet" type="text/css" href="/cp/areas/table_setup_files/css/jquery.fancybox.css" media="screen" />
		<script type="text/javascript">'.file_get_contents("/home/poslavu/public_html/admin/distro/beta/distro_scripts/lavuLog.js").'</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.corner.js"></script>
		<script type="text/javascript" src="./distro_scripts/distro.js"></script>
		<script type="text/javascript" src="/manage/js/lavugui.js"></script>
		<script type="text/javascript" src="/manage/js/lavuform.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.flot.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.flot.time.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.easytabs.js"></script>
		<script type="text/javascript" src="./ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.plus.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.picker.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.picker.ext.js"></script>
		<script type="text/javascript" src="./distro_scripts/acc_simple.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery-ui-1.10.0.custom.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.validate.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery-tooltip/jquery.tooltip.min.js"></script>
		<script type="text/javascript" src="/cp/areas/table_setup_files/js/jquery.fancybox.pack.js"></script>
		<title>Lavu Specialist Portal</title>
		</head>';

	echo $header;

	if(isset($_GET['logout']) && $_GET['logout']==1)
	{
		add_to_distro_history("logged out","Logged Out",$_SESSION['posdistro_id'],$_SESSION['posdistro_loggedin']);
		$_SESSION['posdistro_loggedin'] = false;
		$_SESSION['posdistro_fullname'] = false;
		$_SESSION['posdistro_email'] = false;
		$_SESSION['posdistro_access'] =  false;
		$_SESSION['posdistro_id'] = false;
	}

	if (isset($_GET['admin_login']) && strpos($_SESSION['posdistro_access'],"admin") !== false){
		$account_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$_GET['admin_login']);
		if(mysqli_num_rows($account_query))
		{
			$account_read = mysqli_fetch_assoc($account_query);
			$loggedin = $account_read['username'];
			$loggedin_fullname = trim($account_read['f_name'] . " " . $account_read['l_name']);
			$loggedin_email = $account_read['email'];
			$loggedin_percentage = get_distro_commision($account_read);
			//echo '<br />'.$loggedin_percentage.'<br>';
			$_SESSION['posdistro_loggedin'] = $loggedin;
			$_SESSION['posdistro_fullname'] = $loggedin_fullname;
			$_SESSION['posdistro_email'] = $loggedin_email;
			$_SESSION['posdistro_percentage'] = $loggedin_percentage;
		}
	}

	$loggedin = (isset($_SESSION['posdistro_loggedin']))?$_SESSION['posdistro_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posdistro_fullname']))?$_SESSION['posdistro_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posdistro_email']))?$_SESSION['posdistro_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posdistro_access']))?$_SESSION['posdistro_access']:false;
	if($loggedin_access=="") $loggedin_access = "";
	//Admin Login

	// if($_SERVER['SERVER_PORT']!=443 && !DEV) {
	// 	header("Location: https://admin.poslavu.com/distro/");
	// }


	/***************************************************************************
	 *                  C H E C K   L O G I N   S T A T U S                    *
	 **************************************************************************/
	if($loggedin && !(isset($dont_actually_load_index) && $dont_actually_load_index = TRUE)) // logged in
	{
		$user = new distro_user();

		if(isset($_REQUEST['change_account_range'])){
			$range = $_REQUEST['change_account_range'];
			$user->set_info_attr('account_display_range',$_REQUEST['change_account_range']);
			$user->set_leads_accounts($loggedin,$range);
		}else{
			$user->set_leads_accounts($loggedin);
		}
		$user->set_quotes($loggedin);
		$user->set_all_info($loggedin);
		$loggedin_percentage = get_distro_commision(array(), $loggedin);

		$is_admin = $user->get_info_attr('access');
		if ($is_admin == 'admin') {
			$_SESSION['distro_logged_in_is_admin'] = TRUE;
		}

		// LP-166 -- Stopped serializing the distro_user object and storing it in a session, since it was contributed to resellers going over the 1 MB size limit for memcached sessions.

		$_SESSION['posdistro_percentage'] = $loggedin_percentage;

		new tab_container($user, $is_admin);
	}
	else if(isset($_GET['test_login']) && $_GET['test_login']=='test_login')
	{
		$in_lavu = true;
		require_once("distro_login_test.php");
	}
	else{
		$in_lavu = true;
		require_once("distro_login.php");
	}

	function account_loggedin()
	{
		global $loggedin;
		return $loggedin;
	}//returns $loggedin

	function distro_admin_info($var)
	{
		$sessvar_name = 'posdistro_'.$var;
		if(isset($_SESSION[$sessvar_name]))
			return $_SESSION[$sessvar_name];
		else
			return "";
	}

	function display_datetime($dt,$type="both")
	{
		$dt_parts = explode(" ",$dt);
		if(count($dt_parts) > 1)
		{
			$d_parts = explode("-",$dt_parts[0]);
			$t_parts = explode(":",$dt_parts[1]);
			if(count($d_parts) > 2 && count($t_parts) > 2)
			{
				$ts = mktime($t_parts[0],$t_parts[1],$t_parts[2],$d_parts[1],$d_parts[2],$d_parts[0]);
				if($type=="date")
					return date("m/d/y",$ts);
				else if($type=="time")
					return date("h:i a",$ts);
				else
					return date("m/d/y h:i a",$ts);
			}
			else return $dt;
		}
		else return $dt;
	}


	//can replace with distro_user->somefunctionprobably
	function get_reseller_billing_info($distro_code)
	{
		$cust_info = array();
		$cust_info['company_name'] = "";
		$cust_info['first_name'] = "";
		$cust_info['last_name'] = "";
		$cust_info['address'] = "";
		$cust_info['city'] = "";
		$cust_info['state'] = "";
		$cust_info['zip'] = "";
		$cust_info['phone'] = "";
		$cust_info['email'] = "";
		$cust_info['distro_id'] = "";

		$distro_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$distro_code);
		if(mysqli_num_rows($distro_query))
		{
			$signup_read = mysqli_fetch_assoc($distro_query);
			$cust_info['company_name'] = $signup_read['company'];
			$cust_info['first_name'] = $signup_read['f_name'];
			$cust_info['last_name'] = $signup_read['l_name'];
			$cust_info['address'] = $signup_read['address'];
			$cust_info['city'] = $signup_read['city'];
			$cust_info['state'] = $signup_read['state'];
			$cust_info['zip'] = $signup_read['postal_code'];
			$cust_info['phone'] = $signup_read['phone'];
			$cust_info['email'] = $signup_read['email'];
			$cust_info['distro_id'] = $signup_read['id'];
		}

		return $cust_info;
	}

	function create_account_area($mode)
	{
		if ($mode=="create_account")
		{
			echo "<a href='index.php'>Back to Main Menu</a><br><br>";

			function confirm_restaurant_shard($data_name)
			{
				$str = "";
				$letter = substr($data_name,0,1);
				if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
					$letter = $letter;
				else
					$letter = "OTHER";
				$tablename = "rest_" . $letter;

				$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
				if(mysqli_num_rows($mainrest_query))
				{
					$str .= $letter . ": " . $data_name . " exists";
				}
				else
				{
					$str .= $letter . ": " . $data_name . " inserting";

					$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);

						$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
						if($success) $str .= " <font color='green'>success</font>";
						else $str .= " <font color='red'>failed</font>";
					}
				}
				return $str;
			}

			ini_set("dislay_errors","1");
			if(isset($_POST['company']) && isset($_POST['email']))
			{
				$company = $_POST['company'];
				$email = $_POST['email'];
				$phone = $_POST['phone'];
				$firstname = $_POST['firstname'];
				$lastname = $_POST['lastname'];
				$address = $_POST['address'];
				$city = $_POST['city'];
				$state = $_POST['state'];
				$zip = $_POST['zip'];
				$default_menu = $_POST['default_menu'];

				//require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
				$maindb = "poslavu_MAIN_db";
				require_once("/home/poslavu/public_html/register/create_account.php");
				$result = create_new_poslavu_account($company,$email,$phone,$firstname,$lastname,$address,$city,$state,$zip,$default_menu,"");
				$success = $result[0];
				if($success)
				{
					$username = $result[1];
					$password = $result[2];
					$dataname = $result[3];

					echo "<br>username: $username";
					echo "<br>password: $password";
					echo "<br>dataname: $dataname";

					$credvars['username'] = $username;
					$credvars['dataname'] = $dataname;
					$credvars['password'] = $password;
					$credvars['company'] = $company;
					$credvars['email'] = $email;
					$credvars['phone'] = $phone;
					$credvars['firstname'] = $firstname;
					$credvars['lastname'] = $lastname;
					$credvars['address'] = $address;
					$credvars['city'] = $city;
					$credvars['state'] = $state;
					$credvars['zip'] = $zip;
					$credvars['default_menu'] = $default_menu;

					mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`username`,`dataname`,`password`,`company`,`email`,`phone`,`firstname`,`lastname`,`address`,`city`,`state`,`zip`,`status`,`default_menu`) values ('[username]','[dataname]',AES_ENCRYPT('[password]','password'),'[company]','[email]','[phone]','[firstname]','[lastname]','[address]','[city]','[state]','[zip]','manual','[default_menu]')",$credvars);
					confirm_restaurant_shard($dataname);

					mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `distro_code`='[1]' where `data_name`='[2]' limit 1",account_loggedin(),$dataname);

					$history_str = "";
					foreach($credvars as $key => $val) $history_str .= $key . ": " . $val . "\n";
					add_to_distro_history("created new account",$history_str,distro_admin_info('id'),distro_admin_info('loggedin'));

					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);

						$stat_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[1]'",$dataname);
						if(mysqli_num_rows($stat_query))
						{
							$stat_read = mysqli_fetch_assoc($stat_query);
							$status_id = $stat_read['id'];
						}
						else
						{
							mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`) values ('[1]','[2]')",$rest_read['id'],$dataname);
							$status_id = mlavu_insert_id();
						}

						$trial_length = 7;
						$trial_start = date("Y-m-d H:i:s");
						$trial_end = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") + $trial_length,date("Y")));
						mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `trial_start`='[1]', `trial_end`='[2]' where `id`='[3]'",$trial_start,$trial_end,$status_id);
					}
				}
				else echo "Failed to create account";
			}
			else
			{
				//../lib/create_demo_account.php?m=2' method='POST'>
				$creation_fields = array("company","email","phone","firstname","lastname","address","city","state","zip");
				echo "<script language='javascript'>";
				echo "function submit_create_account() { ";
				for($n=0; $n<count($creation_fields); $n++)
				{
					$crfield = $creation_fields[$n];
					echo " if(document.create_account.".$crfield.".value=='') alert('Please fill out the entire form'); else";
				}
				echo " document.create_account.submit(); ";
				echo "} ";
				echo "</script>";
				echo "<form action='index.php?mode=create_account' method='POST' name='create_account'>
					<table cellspacing='0' cellpadding='5'>
						<tr><td align='right'>Starter Menu:</td><td align='left'><select name='default_menu'>
							<option value='default_coffee'>Coffee Shop</option>
							<option value='defaultbar'>Bar/Lounge</option>
							<option value='default_pizza_'>Pizza Shop</option>
							<option value='default_restau'>Restaurant</option>
							<option value='sushi_town'>Original Sushi</option>
						</select></td></tr>
						<tr><td align='right'>Company Name:</td><td align='left'><input type='text' name='company' size='30' /></td></tr>
						<tr><td align='right'>Email:</td><td align='left'><input type='text' name='email' size='30' /></td></tr>
						<tr><td align='right'>Phone:</td><td align='left'><input type='text' name='phone' size='30' /></td></tr>
						<tr><td align='right'>First Name:</td><td align='left'><input type='text' name='firstname' size='30' /></td></tr>
						<tr><td align='right'>Last Name:</td><td align='left'><input type='text' name='lastname' size='30' /></td></tr>
						<tr><td align='right'>Address:</td><td align='left'><input type='text' name='address' size='30' /></td></tr>
						<tr><td align='right'>City:</td><td align='left'><input type='text' name='city' size='30' /></td></tr>
						<tr><td align='right'>State:</td><td align='left'><input type='text' name='state' size='30' /></td></tr>
						<tr><td align='right'>Zip:</td><td align='left'><input type='text' name='zip' size='30' /></td></tr>
						<tr><td align='center' colspan='2'><input type='button' value='Create Account' onclick='submit_create_account()' /></td></tr>
					</table>
				</form>";
			}
		}
		else if ($mode=="account_created")
		{
			if ($_REQUEST['s'] == 1) {
				echo "New account successfully created...<br><br>Username: ".$_REQUEST['new_un']."<br>Password: ".$_REQUEST['new_pw'];
				mail($_SESSION['posadmin_email'], "New POSLavu Account: ".$_REQUEST['new_co'], "New account successfully created...\n\nUsername: ".$_REQUEST['new_un']."\nPassword: ".$_REQUEST['new_pw'], "From: info@poslavu.com");
			} else if ($_REQUEST['s'] == 1) {
				echo "An error occurred while trying to create new account...";
			}
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";
		}
	}
	function updatePassword($username){
		echo "<table><tr><td><b>Update Password</b></td></tr>";
		echo "<form action ='index.php?mode=doPassUpdate' method=post>";
		echo "<tr><td> Current Password </td><td> <input type= 'password' name='current'> </td></tr>";
		echo "<tr><td> New Password </td> <td> <input type='password' name='new1'></td></tr>";
		echo "<tr><td> Password Again </td> <td> <input type='password' name= 'new2'> <input type= hidden value='".$username."' name=username></td></tr>";
		echo "<tr><td> <input type= submit></tr></td>";
		echo "</form>";
		echo "</table> ";

		echo "<br><br><a href='index.php'>Back to Main Menu</a><br><br>";

	}
	function doPassUpdate(){
		//echo "username: ".$_POST['username'];
		if( $_POST['new2'] != $_POST['new1'] ){

			echo "<div style='color:red'>Your new passwords do not match please try again.  </div>";
			updatePassword($_POST['username']);

		}else{

			mlavu_query("update `poslavu_MAIN_db`.`resellers` set `password`=PASSWORD('[1]') where `username`='[2]' and `password`= PASSWORD('[3]')", $_POST['new1'], $_POST['username'], $_POST['current']);
			if(ConnectionHub::getConn('poslavu')->affectedRows()){
				echo "password successfully updated.";
			}else{
				echo "<div style='color:red'>current password is incorrect. please try again.</div>";
				updatePassword($_POST['username']);
			}


		}
	}

	function updateInfo(){
		echo "<b><center>Info Updated</b>";
		echo "<br><br><a href='index.php'>Back to Main Menu</a><br><br>";
		mlavu_query("update `poslavu_MAIN_db`.`resellers` set `f_name`='".$_POST['f_name']."', l_name='".$_POST['l_name']."', company='".$_POST['company']."', address='".$_POST['address']."',
		city='".$_POST['city']."',state= '".$_POST['state']."', country= '".$_POST['country']."', postal_code = '".$_POST['postal_code']."',phone= '".$_POST['phone']."', email='".$_POST['email']."',
		website= '".$_POST['website']."',notes='".$_POST['notes']."' where id='".$_POST['distroID']."'");

	}
	function updateMessage(){

		$myVals= $_POST['message'];
		echo "New Message:".$myVals;

		$writeHandle=fopen("../distroMessages/messages.txt", 'r+');


		fputs($writeHandle,"|".$myVals ) or die( "couldnt write file") ;

		fclose($writeHandle);
	}

	function uploadImage(){
		$username= $_POST['username'].'.png';
		$allowedExts = array("jpg", "jpeg", "gif", "png");
		$extension = end(explode(".", $_FILES["file"]["name"]));

		if ((($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/png") ||
		($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/jpg")) &&
		($_FILES["file"]["size"] < 2000000) && in_array($extension, $allowedExts)){

			if ($_FILES["file"]["error"] > 0){
		    	echo "Return Code: " . $_FILES["file"]["error"] . "<br />";

		    }else{

		    	if (file_exists("../images/logos/fullsize/" . $username)){
		    		echo $username . "Logo already exists. Overwriting. ";
		    		move_uploaded_file($_FILES["file"]["tmp_name"],"../images/logos/fullsize/".$username);
		    		resizeAndSave($username);

		    	}else{
		    		move_uploaded_file($_FILES["file"]["tmp_name"],"../images/logos/fullsize/".$username);
		    		resizeAndSave($username);
		    		echo "Successfully uploaded your logo. Thank you!";

		    	}
		    }
		}
		else{
			echo "Invalid file Please try another logo.";
		}
 }

function resizeAndSave($filename){
 	copy("../images/logos/fullsize/".$filename."" , "../images/logos/".$filename);
 	$inv_main_size= getimagesize("../images/logos/fullsize/".$filename);

 	$convertcmd = "convert '../images/logos/$filename' -thumbnail '120x120 >' '../images/logos/$filename'";
    exec($convertcmd);
 }
