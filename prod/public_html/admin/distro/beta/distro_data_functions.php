<?php

global $o_emailAddressesMap;
require_once(dirname(__FILE__)."/../../sa_cp/billing/payment_profile_functions.php");
require_once(dirname(__FILE__)."/../../sa_cp/billing/package_levels_object.php");
require_once(dirname(__FILE__)."/../../sa_cp/billing/distro_points_functions.php");
require_once(dirname(__FILE__)."/../../manage/globals/email_addresses.php");

if (!isset($maindb))
	$maindb = "poslavu_MAIN_db";
if (!isset($o_package_container))
	$o_package_container = new package_container;
if (!function_exists('is_dev')) {
	function is_dev() {
		if(strpos($_SERVER['REQUEST_URI'],"v2/")!==false)
			return true;
		else
			return false;
	}
}

function distro_is_admin($s_distrocode) {
	$a_distro = get_distro_from_array_distrocode_dataname(array(), $s_distrocode);
	if ($a_distro['access'] != 'admin')
		return FALSE;
	return TRUE;
}

// returns the mysqli_fetch_assoc array from `[maindb]`.`resellers` or an empty array if the reseller can't be found
// @$a_distro, the array that is being searched for (or an empty array)
// @$s_distro_code, the distro code that is being searched for (or empty string)
// @$s_dataname, the restaurant dataname that should have a distributor
$get_distro_from_array_distrocode_dataname_last_distro = array();
function get_distro_from_array_distrocode_dataname($a_distro, $s_distro_code = '', $s_dataname = '') {
	global $maindb;

	if (count($a_distro) == 0 || !isset($a_distro['username'])) {
		// check if this distro is already loaded
		if (count($get_distro_from_array_distrocode_dataname_last_distro) > 0 && $s_distro_code) {
			if (isset($get_distro_from_array_distrocode_dataname_last_distro['username']))
				if ($get_distro_from_array_distrocode_dataname_last_distro['username'] == $s_distro_code)
					return $get_distro_from_array_distrocode_dataname_last_distro;
		}

		// load the distro
		if (isset($a_distro['username'])) {
			$a_distro = get_reseller_from_distro_code($a_distro['username']);
		} else if ($s_distro_code !== '') {
			$a_distro = get_reseller_from_distro_code($s_distro_code);
		} else if ($s_dataname !== '') {
			$restaurant_query = mlavu_query("SELECT `distro_code` FROM `[maindb]`.`restaurants` WHERE `data_name`='[dataname]'",
				array("maindb"=>$maindb, "dataname"=>$s_dataname));
			if ($restaurant_query === FALSE)
				return array();
			if (mysqli_num_rows($restaurant_query) == 0)
				return array();
			$restaurant_read = mysqli_fetch_assoc($restaurant_query);
			mysqli_free_result($restaurant_query);
			$s_distro_code = $restaurant_read['distro_code'];
			if (trim($s_distro_code) == '')
				return array();
			$a_distro = get_reseller_from_distro_code($s_distro_code);
		} else {
			return array();
		}
	}
	$get_distro_from_array_distrocode_dataname_last_distro = $a_distro;
	return $a_distro;
}

// @$a_distro can be either an array with 'username' set or
//     an empty array (and provide the $s_distro_code)
// returns the commission as a part of 100 (0-100)
// returns -1 on error
function get_distro_commision($a_distro, $s_distro_code = '', $s_dataname = '') {
	$a_distro = get_distro_from_array_distrocode_dataname($a_distro, $s_distro_code, $s_dataname);
	if (count($a_distro) == 0)
		return -1;
	$s_distro_license_com = $a_distro['distro_license_com'];
	return (int)str_replace('%', '', $s_distro_license_com);
}

// get the explanation for why a distributor has the points that he/she does
// @$s_distro_code: the distributor's name
// @return: an array(
//     'predicted_points' => how many points the distributor SHOULD have,
//     'explanations' => array(
//         explanations title => array( array('commission'=>distributor's commission, 'distro_points_applied'=>how many points were applied, 'dataname'=>dataname of the restaurant, 'restaurant_name'=>name of the restaurant, ['details'=>details]), ...),
//         ...
//     ),
// )
function get_distro_points_explained($s_distro_code) {
	global $o_package_container;


	// get the restaurants that belong to this distributor
	$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('distro_code'=>$s_distro_code), TRUE, array('selectclause'=>'`data_name`,`company_name` AS `restaurant_name`'));

	// get all the explanations
	$a_recurring = FALSE;
	$a_upgrades = FALSE;
	$a_licenses = FALSE;
	$a_billing_logs = FALSE;
	get_distro_points_explained_recurring($s_distro_code, $a_recurring);
	get_distro_points_explained_upgrades($s_distro_code, $a_upgrades);
	get_distro_points_explained_licenses($s_distro_code, $a_licenses);
	get_distro_points_explained_reseller_billing_log($s_distro_code, $a_billing_logs);

	// calculate the worth of the recurring
	foreach($a_recurring as $k=>$a_invoice) {
		$a_recurring[$k]['distro_points_applied'] = (int)$a_invoice['distro_points_applied'];
		$a_recurring[$k]['commission'] = 'N/A';
	}
	unset($a_invoice);

	// calculate the worth of the upgrades
	foreach($a_upgrades as $k=>$a_upgrade) {
		$from = $a_upgrade['from'];
		$to = $a_upgrade['to'];
		$i_commision = (int)$a_upgrade['commission'];
		$a_upgrade['commission'] = $i_commision;
		$value = $o_package_container->get_value_by_attribute('name', $to) - $o_package_container->get_value_by_attribute('name', $from);
		if ($value <= 0)
			break;
		$a_upgrades[$k]['distro_points_applied'] = $i_commision * $value / 100;
		$a_upgrades[$k]['details'] = str_ireplace(array('silver', 'gold', 'platinum', 'silver2','gold2','platinum2'), array('Lavu Silver', 'Lavu Gold', 'Lavu Platinum', 'Lavu Silver', 'Lavu Gold', 'Lavu Platinum'), $a_upgrades[$k]['details']);
	}
	unset($a_upgrade);

	// calculate the worth of the licenses
	foreach($a_licenses as $k=>$a_license) {
		$i_value = (int)$a_license['value'];
		$a_licenses[$k]['commission'] = 'N/A';
		$a_licenses[$k]['distro_points_applied'] = -$i_value;
	}
	unset($a_license);

	// calculate the worth from the billing logs
	foreach($a_billing_logs as $k=>$a_billing_log) {
		$a_billing_logs[$k]['commission'] = 'N/A';
	}
	unset($a_billing_log);

	// predict how many points the distro should have
	$i_prediction = 0;
	foreach(array($a_recurring, $a_upgrades, $a_licenses, $a_billing_logs) as $a_explanations) {
		foreach($a_explanations as $a_explanation) {
			$i_prediction += $a_explanation['distro_points_applied'];
		}
	}
	unset($a_explanations);
	unset($a_explanation);

	$a_retval = array(
		'predicted_points' => $i_prediction,
		'explanations' => array(
			'Recurring' => $a_recurring,
			'Upgrades' => $a_upgrades,
			'Licenses' => $a_licenses,
			'Other'=>$a_billing_logs,
		),
	);
	unset($a_recurring);
	unset($a_upgrades);
	unset($a_licenses);
	unset($a_billing_logs);
	return $a_retval;
}

// get an array of all ip address this distro has ever signed in with
// return the array of ips or an empty array if no rows could be found
function get_distro_ips($s_distro_code) {
	global $maindb;

	$distro_ips_query = mlavu_query("SELECT `ipaddress` FROM `[maindb]`.`reseller_connects` WHERE `resellername`='[resellername]'",
		array("maindb"=>$maindb, "resellername"=>$s_distro_code));
	if ($distro_ips_query === FALSE)
		return array();
	if (mysqli_num_rows($distro_ips_query) == 0)
		return array();
	$a_retval = array();
	while ($distro_ips_read = mysqli_fetch_assoc($distro_ips_query))
		$a_retval[] = $distro_ips_read['ipaddress'];

	mysqli_free_result($distro_ips_query);

	return $a_retval;
}

// returns 'good', 'bad_no_distro', 'bad_restaurant', '25days_old', '3months_old', 'non_points_package', 'did_not_answer_survey', 'bad_survey_response_time', 'bad_interaction', or 'bad_satisfaction'
// $a_distro can be an empty array and $s_distro_code can be an empty string, in which case the distro is found with $s_dataname
// if a distro is not provided or a distro can't be found, returns 'bad'
function distro_in_good_standing_with_client($a_distro, $s_distro_code, $s_dataname) {
	global $o_emailAddressesMap;

	$a_distro = get_distro_from_array_distrocode_dataname($a_distro, $s_distro_code, $s_dataname);
	if (count($a_distro) == 0)
		return 'bad_no_distro';

	// get the restaurant
	require_once(dirname(__FILE__).'/../../sa_cp/billing/payment_profile_functions.php');
	$a_restaurant = get_restaurant_from_dataname($s_dataname);
	if (count($a_restaurant) == 0)
		return 'bad_restaurant';

	// check if the account is younger than 25 days (can't yet see the survey)
	$i_created_ts = strtotime($a_restaurant['created']);
	$i_25days_ts = strtotime("25 days ago");
	$i_3months_ts = strtotime("3 months ago");
	$i_4months_ts = strtotime("4 months ago");
	if($i_created_ts >= $i_25days_ts)
		return '25days_old';

	// check the account level
	global $o_package_container;
	require_once(dirname(__FILE__).'/../../sa_cp/billing/package_levels_object.php');
	if (!isset($o_package_container))
		$o_package_container = new package_container();
	$a_package = find_package_status($a_restaurant['id']);
	$i_package = (int)$a_package['package'];
	if (!$o_package_container->is_points_package_by_attribute('level',$i_package))
		return 'non_points_package';

	// check if the account is younger than 3 months (assumed to be in good standing)
	if($i_created_ts >= $i_3months_ts && $i_created_ts <= $i_4months_ts)
		return '3months_old';

	// check for a survey
	$s_mailto = "survey_followups";
	$s_subject = "customer hasn't completed survey";
	$s_content = "The customer with dataname ".$s_dataname." hasn't answered a survey recently (in the last 3 months and 2 weeks). We need them to answer this survey in order to award points to their distributor for monthly recurring hosting. Please contact them to complete the survey (should be found on the customer's home page in the backend). If they can't find the survey, contact Benjamin Bean.";
	$s_from = "From: noreply@poslavu.com";
	$s_cp = (is_dev() ? 'v2' : 'cp');
	require_once(dirname(__FILE__).'/../../'.$s_cp.'/areas/survey.php');
	$a_surveys = get_most_recent_survey_results('satisfaction survey', $s_dataname, TRUE);
	if (count($a_survey) == 0) {
		//$o_emailAddressesMap->sendEmailToAddress($s_mailto, $s_subject, $s_content, $s_from);
		return 'did_not_answer_survey';
	}
	$a_survey = $a_surveys[0];
	$i_survey_ts = strtotime($a_survey['date']);
	$i_survey_distro_interaction = (int)$a_survey['distributor_interaction'];
	$i_survey_distro_satisfaction = (int)$a_survey['distro_satisfaction'];
	$i_2weeks_secs = time()-strtotime("2 weeks ago");
	if ($i_survey_ts < strtotime("3 months ago")-$i_2weeks_secs) {
		//$o_emailAddressesMap->sendEmailToAddress($s_mailto, $s_subject, $s_content, $s_from);
		return 'bad_survey_response_time';
	}

	// check the survey results
	if ($i_survey_distro_interaction == 1)
		return 'bad_interaction';
	if ($i_survey_distro_satisfaction == 1)
		return 'bad_satisfaction';

	return 'good';
}

// get the distributors and their licenses sold since a date
// only include distributors that sold a minimum and maximum of licenses in the range
// @$prev_date: the previous date, in Y-m-d H:i:s format (must be greater than this time)
// @$i_min: the minimum number of licenses sold
// @$i_max: the maximum number of licenses sold
// @$next_date: the next day, in Y-m-d H:i:s format (must be less or equal to this time)
// @return: an array(array('resellername'=>string, 'count'=>(string)int), ...)
function get_distributor_licenses_sold_since($prev_date, $i_min, $i_max, $next_date = "") {

	// sanitize input
	if ($next_date === "")
		$next_date = date("Y-m-d H:i:s", strtotime("+1 days"));

	// do the query
	$distro_lsold_query = mlavu_query("SELECT `username` AS `resellername`,COUNT(`license_id`) AS `count` FROM ((SELECT `username` FROM `poslavu_MAIN_db`.`resellers` WHERE `access` != 'deactivated')) `rdb` LEFT JOIN ((SELECT `resellername`,`id` AS `license_id` FROM `poslavu_MAIN_db`.`reseller_licenses` WHERE `applied` > '[prev_date]' AND `applied` <= '[next_date]')) `ldb` ON (`rdb`.`username` = `ldb`.`resellername`) GROUP BY `username`",
		array('prev_date'=>$prev_date, 'next_date'=>$next_date));

	// check if the query is bad
	if ($distro_lsold_query === FALSE) {
		error_log('bad distro licenses sold query in '.__FILE__);
		return array();
	}

	// load the rows from the array
	$a_distro_quantities = array();
	while ($row = mysqli_fetch_assoc($distro_lsold_query)) {
		if ($row['count'] >= $i_min && $row['count'] <= $i_max)
			$a_distro_quantities[] = $row;
	}

	return $a_distro_quantities;
}

class DistroSessionStats {

	public static function getLoggedin() {
		$loggedin = $_SESSION['posdistro_loggedin'];
		if (isset($_SESSION['admin_posdistro_loggedin'])) // for when logging into a distro's account as an admin
			$loggedin = $_SESSION['admin_posdistro_loggedin'];
		return $loggedin;
	}

	public static function getLpercent() {
		$lpercent = (int)$_SESSION['posdistro_percentage'];
		if (isset($_SESSION['admin_posdistro_percentage'])) // for when logging into a distro's account as an admin
			$lpercent = (int)$_SESSION['admin_posdistro_percentage'];
		return (int)$lpercent;
	}

}

if (isset($_GET['test_distro'])) {
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	echo "<pre>";
	print_r(get_distro_points_explained('freshplus'));
	echo "</pre>";
}

?>