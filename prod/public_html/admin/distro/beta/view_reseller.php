<?php
	session_start();
	ini_set("display_errors",1);
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	require_once(dirname(__FILE__)."/distro_user.php");
	require_once(dirname(__FILE__)."/distro_tab_container.php");
	
	$distrojs = '<script type="text/javascript" src="./distro_scripts/distro.js"></script>';
	if (isset($_GET['ben']))
		$distrojs = '<script type="text/javascript" src="./distro_scripts/distro.js"></script>';
	
	$header = '<!DOCTYPE html><html><head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/dnew.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery.calendars.picker.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery.tooltip.css" />
		<link rel="stylesheet" type="text/css" href="./distro_stylesheets/jquery-ui-1.10.0.custom.css" />
		<script type="text/javascript">'.file_get_contents("/home/poslavu/public_html/admin/distro/beta/distro_scripts/lavuLog.js").'</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.corner.js"></script>
		'.$distrojs.'
		<script type="text/javascript" src="./distro_scripts/jquery.easytabs.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery-tooltip/jquery.tooltip.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/acc_simple.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery-ui-1.10.0.custom.js"></script>
		<script type="text/javascript" src="/manage/js/lavugui.js"></script>
		<script type="text/javascript" src="/manage/js/lavuform.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.validate.min.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.plus.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.picker.js"></script>
		<script type="text/javascript" src="./distro_scripts/jquery.calendars.picker.ext.js"></script>
		<script type="text/javascript" src="./ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="./ckeditor/config.js"></script>	
		<title>VIEW RESELLER</title>
		</head>
				';
	echo $header;
	
	if(isset($_GET['function'])){
		if(function_exists($_GET['function'])){
			$_GET['function']($_GET['username']);
		}
	}
	
	if (isset($_SESSION['distro_logged_in_is_admin']) && $_SESSION['distro_logged_in_is_admin']) {
		if (isset($_POST['action']) && $_POST['action'] = 'add_remove_distro_points') {
			$i_points = (int)$_POST['add_remove_distro_points_points'];
			
		}
	}
	
	function login_reseller($username = null){
		$user = new distro_user();
	
		$user_name = $username;
		//echo $user_name;
		$user->set_info_attr('account_redirect','./view_reseller.php?function=login_reseller&username='.$user_name.'&');
		if(isset($_REQUEST['change_account_range'])){
			$range = $_REQUEST['change_account_range'];
			//echo 'v_r_range:: '.$range.'<br />';
			$user->set_info_attr('account_display_range',$range);
			//echo 'r_set:: '.$range.'<br />';
			$user->set_leads_accounts($user_name,$range);
		}else{
			$user->set_leads_accounts($user_name);
		}
		$user->set_quotes($user_name);
		$user->set_leads_accounts($user_name);
		$user->set_all_info($user_name);
	
		$is_admin = $user->get_info_attr('access');
		if ($is_admin == 'admin')
			$_SESSION['distro_logged_in_is_admin'] = TRUE;
		
		set_admin_session_vars($user);
	 	
		new tab_container($user, $is_admin);
		
	}
	
	function set_admin_session_vars($o_user) {
		$_SESSION['admin_posdistro_percentage'] = $o_user->get_info_attr('distro_license_com');
		$_SESSION['admin_posdistro_loggedin'] = $o_user->get_info_attr('name');
	}
