<?php
	
	// used to determine if the restaurants with the given ids have paid for their license
	// @$a_ids: an array of ids to query the database for
	// @$b_default: TRUE or FALSE (if the query fails)
	// @return: an array((string)id=>bool, ...)
	function get_accounts_with_paid_licenses($a_ids, $b_default = TRUE) {
		
		// escape the ids and create the query string
		foreach($a_ids as $k=>$id)
			$a_ids[$k] = ConnectionHub::getConn('poslavu')->escapeString($id);
		$s_in_clause = "IN ('".implode("','", $a_ids)."')";
		$s_query_str = "SELECT `match_restaurantid` FROM `poslavu_MAIN_db`.`payment_responses` WHERE `x_response_code`='1' AND `payment_type`='license' AND `match_restaurantid` {$s_in_clause}";
		
		// query the database, returning $b_default for every account if the query fails
		$response_query = mlavu_query($s_query_str);
		if ($response_query === FALSE || mysqli_num_rows($response_query) === 0) {
			$a_retval = array();
			foreach($a_ids as $id)
				$a_retval[(string)$id] = $b_default;
			unset($a_ids);
			mysqli_free_result($response_query);
			return $a_retval;
		}
		
		// check the results
		foreach($a_ids as $id)
			$a_retval[(string)$id] = FALSE;
		while ($a_response = mysqli_fetch_assoc($response_query)) {
			$a_retval[$a_response['match_restaurantid']] = TRUE;
		}
		return $a_retval;
	}
	
	// checks if the given account has paid for their license already or has a license applied to them
	// @$i_restaurant_id: id of the account (primary source)
	// @$s_data_name: dataname of the account, can be empty if the restaurant_id is supplied (secondary source)
	// @return: 'paid', 'has_license', 'cant_find_restaurant', or 'not_paid'
	function account_has_paid_license($i_restaurant_id, $s_data_name = '') {
		
		// get the dataname/id, whichever is missing
		if ($s_data_name == '') {
			
			// get the dataname
			$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('id'=>$i_restaurant_id), TRUE, array('selectclause'=>'`data_name`'));
			if (count($a_restaurants) == 0)
				return 'cant_find_restaurant';
			$s_data_name = $a_restaurants[0]['data_name'];
		} else {
			
			// get the id
			$a_restaurants = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('restaurants', array('data_name'=>$s_data_name), TRUE, array('selectclause'=>'`id`'));
			if (count($a_restaurants) == 0)
				return 'cant_find_restaurant';
			$i_restaurant_id = (int)$a_restaurants[0]['id'];
		}
		
		// check for an applied license
		$a_payment_statuses = ConnectionHub::getConn('poslavu')->getDBAPI()->getAllInTable('payment_status', array('dataname'=>$s_data_name), TRUE, array('selectclause'=>'`license_applied`'));
		if (count($a_payment_statuses) == 0)
			return 'not_paid';
		if ($a_payment_statuses[0]['license_applied'] !== '')
			return 'has_license';
		
		// check for a payment
		$a_paid = get_accounts_with_paid_licenses(array($i_restaurant_id), FALSE);
		if ($a_paid[(string)$i_restaurant_id])
			return 'paid';
		return 'not_paid';
	}
	
?>
