<?php

	global $o_emailAddressesMap;
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');
	require_once("/home/poslavu/public_html/admin/manage/globals/email_addresses.php");
	require_once(dirname(__FILE__).'/distro_user.php');

	if (!isset($maindb))
		$maindb = 'poslavu_MAIN_db';

	// returns an array('username'=>'unique_code')
	function get_distro_unique_codes() {
		global $maindb;

		$distro_query = mlavu_query('SELECT `f_name`,`l_name`,`username`,`password`,`id` FROM `[maindb]`.`resellers`',
			array('maindb'=>$maindb));
		if ($distro_query === FALSE) {
			error_log('bad query for distro unique codes (/distro/beta/test.php)');
			return array();
		}
		if (mysqli_num_rows($distro_query) == 0)
			return array();
		$a_retval = array();
		while ($distro_read = mysqli_fetch_assoc($distro_query)) {
			$s_distro_code = trim($distro_read['username']);
			$i_distro_id = (int)$distro_read['id'];
			if ($s_distro_code == '')
				continue;
			$s_all_vars = '';
			foreach($distro_read as $v)
				$s_all_vars .= $v;
			$s_unique_code = substr(md5($s_all_vars),16).$i_distro_id;
			$a_retval[$s_distro_code] = $s_unique_code;
		}
		return $a_retval;
	}

	// returns 'success', 'no distro', or 'no email'
	function generate_email($s_distro_code) {
		global $o_emailAddressesMap;

		$s_distro_code = trim($s_distro_code);
		if ($s_distro_code == '')
			return 'no distro';

		$o_distro = create_user($s_distro_code);
		$a_unique_codes = get_distro_unique_codes();
		$s_unique_code = $a_unique_codes[$s_distro_code];
		$s_email = trim($o_distro->get_info_attr('email'));

		if ($s_email == '') {
			error_log('distro '.$s_distro_code.' doesn\'t have an email address');
			return 'no email';
		}

		$a_replace_strings = array('full_user_name'=>$o_distro->get_info_attr('f_name').' '.$o_distro->get_info_attr('l_name'),
			'link'=>'https://admin.poslavu.com/distro/beta/mercury_list_emails.php?action=signup&distro_key='.$s_unique_code);

		$s_subject = 'Sign up for the Mercury List';
		$s_message = 'Dear [full_user_name],

This email has been automatically generated to inform you about the Mercury Accept List.

What is the Mercury Accept List?

Mercury Payments has sponsored a PPC campaign which directs prospects to try.poslavu.com.
Leads generated through try.poslavu.com require a Mercury payment processing account.
Distributors on the Mercury Accept List will receive leads generated from this campaign.
These accounts are standard in all other ways including monthly residual Lavu Points.



To join the Mercury Accept List, go to

[link]

or

1. Sign into the distributor portal
2. Go to the "My Info" tab
3. Click (( EDIT )) and then check the Mercury checkbox

Sincerely,
~Lavu';

		foreach($a_replace_strings as $k=>$v)
			$s_message = str_replace("[$k]", $v, $s_message);

		$break = "\r\n";
		mail($s_email, $s_subject, $s_message, 'From: noreply@poslavu.com'.$break.'Bcc: '.$o_emailAddressesMap->getEmailsForAddressToString("distro_portal_dev"));

		return 'success';
	}

	$create_user_users = array();
	function create_user($s_distro_code) {
		global $create_user_users;

		if (isset($create_user_users[$s_distro_code]))
			return $create_user_users[$s_distro_code];
		$o_distro = new distro_user();
		$o_distro->set_all_info($s_distro_code);
		$o_distro->set_leads_accounts($s_distro_code);
		$create_user_users[$s_distro_code] = $o_distro;

		return $o_distro;
	}

	function signup_distro($s_distro_key) {
		$a_keys = get_distro_unique_codes();
		$s_distro_code = '';
		foreach($a_keys as $k=>$v)
			if ($v == $s_distro_key)
				$s_distro_code = $k;

		if ($s_distro_code == '') {
			return '<body style="background-image:url(images/bg_distro_port.jpg)">
	<div style="margin:400px auto 0 auto;width:600px;padding:30px;border:1px solid gray;background-color:white;border-radius:15px;text-align:center;">
		<font style="color:black;">
			This distributor can not be found.<br />
		</font>
	</div>
</body>';
		}

		$o_distro = create_user($s_distro_code);
		$o_distro->set_accepts_mercury(TRUE);
		return '<body style="background-image:url(images/bg_distro_port.jpg)">
	<div style="margin:250px auto 0 auto;width:600px;padding:30px;border:1px solid gray;background-color:white;border-radius:15px;text-align:center;">
		<font style="color:black;">
			Thank you for signing up for the Mercury Accepts List.<br />
		</font>
	</div>
</body>';
	}

	function send_mass_emails() {
		global $maindb;
		$distro_query = mlavu_query("SELECT `username` FROM `[maindb]`.`resellers` WHERE `username`!='' AND `special_json` NOT LIKE '[special_json]' AND `email`!='' AND `access`!='deactivated'",
			array("maindb"=>$maindb, "special_json"=>'{"mercuryAcceptsList":"1"}'));
		if ($distro_query === FALSE) {
			error_log("bad query for mass emails (/distro/beta/mercury_list_emails.php)");
			return 'failed';
		}
		if (mysqli_num_rows($distro_query) == 0)
			return 'no distros';
		$a_distro_codes = array();
		while ($distro_read = mysqli_fetch_assoc($distro_query))
			$a_distro_codes[] = $distro_read['username'];

		$s_retval = '';
		foreach($a_distro_codes as $s_distro_code)
			generate_email($s_distro_code);

		return 'Successfully sent emails. For the love of God don\'t hit the refresh button.';
	}

	function get_get_var($s_name) {
		return isset($_GET[$s_name]) ? $_GET[$s_name] : '';
	}

	$s_action = get_get_var('action');
	$s_distro_key = get_get_var('distro_key');
	$s_distro_code = get_get_var('distro_code');

	switch($s_action) {
	case 'signup':
		echo signup_distro($s_distro_key);
		break;
	case 'send_email':
		echo generate_email($s_distro_code);
		break;
	case 'send_mass_emails':
		echo send_mass_emails();
		break;
	default:
		echo 'bad command "'.$s_action.'"';
	}
?>