<?php

global $o_distroReceipts;
$o_distroReceipts = new DistroReceiptsClass();

class DistroReceiptsClass {
	private $a_distros;

	public function getSettings($s_distro_name) {
		if (!isset($this->a_distros[$s_distro_name]) || $this->a_distros[$s_distro_name]->user === NULL)
		{
			return NULL;
		}
		$o_user = $this->a_distros[$s_distro_name]->user;
		return $o_user->get_receipts_settings();
	}

	public function setSettings($s_distro_name, $o_settings) {
		if (!isset($this->a_distros[$s_distro_name]) || $this->a_distros[$s_distro_name]->user === NULL)
		{
			return FALSE;
		}
		$o_user = $this->a_distros[$s_distro_name]->user;
		$o_user->set_receipts_settings($o_settings);
		return TRUE;
	}

	public function loadDistro($o_distro_user) {
		$distro_username = $o_distro_user->get_info_attr('username');
		$this->a_distros[$distro_username] = (object)array("user"=>$o_distro_user);
	}
}

?>