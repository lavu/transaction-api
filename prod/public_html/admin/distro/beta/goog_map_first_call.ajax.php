<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 'on');
	require_once('/home/poslavu/public_html/admin/cp/objects/json_decode.php');

	//error_log('herro?');
	$req_url = 'http://admin.poslavu.com/distributor_test.ajax.php'; 

	$resellers_json = file_get_contents($req_url);
	
	// decode json from server
	$resellers = json_decode($resellers_json, true);
	
	$resellers_with_coords = array();
	foreach($resellers as $reseller) {
		
		foreach($reseller as $res) {
			$address = str_replace(' ','+' ,$res['address']);
			$city = str_replace(' ','+' ,$res['city']);
			$state = str_replace(' ','+' ,$res['state']);
			$zip = str_replace(' ','+' ,$res['postal_code']);
			
			$addy = $address . ',+' . $city . ',+' . $state . ',+' . $zip;
			//echo $addy.'<br />';
			if ( !isAddressGeocoded($res['id']) ) {
				echo $addy.'<br />';
				// geocode address
				$coordz = geocode($addy);
				
				if ($coordz['lat'] != 0 && $coordz['lng'] != 0) { // google was able to geocode address!
					//echo 'SUCCESS::  ';
					var_dump($coordz).'<br />';
					// add geocoords to file
					$geo_data = $res['id']."|".$coordz['lat'].",".$coordz['lng'].PHP_EOL;
					//echo $geo_data;
					$fp = file_put_contents("geocoded.txt", $geo_data, FILE_APPEND | LOCK_EX);
				}else{
					echo 'FAIL:: ';
					var_dump($coordz).'<br />';
				}
				
				//echo "missing addy: ".$geo_data;
			} 
/*
			else {
			
				echo "addy geocoded".PHP_EOL;
			}
*/
			/*$geocoords = geocodedResellers($res['id']);
				
			$resellers_with_coords[] = array_merge($res, $geocoords);*/
		}				
	}
		
	// output json
	echo json_encode($resellers_with_coords);	
	
	// TODO rename function geocodedResellers() -- its lame
	//		also speed up algorithm (above) to only iterate file once.
	
	
	function geocodedResellers($id = 0) {
		//echo 'geocodedResellers<br />';
		$coords = array('lat' => 0, 'lng' => 0);
		$geocoded_file = file('geocoded.txt') or die('Could not open geocoded file');
		
		foreach($geocoded_file as $geo) {
		
			$exist_point = explode('|',$geo);			
			$exist_coords = explode(",",$exist_point[1]);
			
			if ( $id == $exist_point[0] ) {
			
				// set coords for that reseller
				$coords['lat'] = $exist_coords[0];
				$coords['lng'] = $exist_coords[1];
			
			}
		}	
		
		return $coords;
	}

	function isAddressGeocoded( $reseller_id = 0 ) {	
		//return false;
		$geocoded = false;
		$geocoded_file = file('geocoded.txt') or die('Could not open geocoded file');

		foreach($geocoded_file as $geo) {
		
			$exist_point = explode('|',$geo);
			
			if ( $reseller_id == $exist_point[0] ) {
			
				$geocoded = true;
			}
			
		}

		return $geocoded;
	}
	 
	function geocode($address){
	
		$address = urlencode($address);
		$url = "http://maps.googleapis.com/maps/api/geocode/json?address=$address&sensor=false";
		
		$coords = array('lat' => 0, 'lng' => 0);
		$delay = 0;
		
 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = json_decode(curl_exec($ch), true);
 
		// If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
		if ($response['status'] != 'OK') {
			return $coords;
    	}
 
    	//print_r($response);
    	$geometry = $response['results'][0]['geometry'];
 
    	$longitude = $geometry['location']['lat'];
    	$latitude = $geometry['location']['lng'];
    
    	$coords['lat'] = $longitude;
    	$coords['lng'] = $latitude;
    	return $coords;
    
	}

?>