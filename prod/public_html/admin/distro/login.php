<?php
require_once(dirname(__FILE__). '/../cp/areas/white_list.php');

	if($in_lavu)
	{
		$maindb = "poslavu_MAIN_db";
		$login_message = "";
		if(isset($_POST['username']) && isset($_POST['password']))
		{
			function update_cp_login_status($success,$username)
			{
				global $maindb;
				if($success)
					$set_field = "succeeded";
				else
					$set_field = "failed";
					
				$ipaddress = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];  // work-around for load balancer proxy
				$mints = time() - (60 * 15);
				$currentts = time();
				$currentdate = date("Y-m-d H:i:s");
				
				$li_query = mlavu_query("select * from `$maindb`.`login_log` where `ipaddress`='$ipaddress' and `$set_field`>'0' and `ts`>='$mints'");
				if(mysqli_num_rows($li_query))
				{
					$li_read = mysqli_fetch_assoc($li_query);
					$li_id = $li_read['id'];
					mlavu_query("update `$maindb`.`login_log` set `ts`='$currentts', `date`='$currentdate', `users`=CONCAT(`users`,',[1]'), `$set_field`=`$set_field`+1 where `id`='$li_id'",$username);
				}
				else
				{
					mlavu_query("insert into `$maindb`.`login_log` (`ts`,`date`,`$set_field`,`ipaddress`,`users`) values ('$currentts','$currentdate','1','$ipaddress','[1]')",$username);
				}
			}
			
			function get_cp_login_count($field)
			{
				global $maindb;
				
				$ipaddress = empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'] : $_SERVER['HTTP_X_FORWARDED_FOR'];  // work-around for load balancer proxy
				$mints = time() - (60 * 15);
				
				$li_query = mlavu_query("select * from `$maindb`.`login_log` where `ipaddress`='$ipaddress' and `$field`>'0' and `ts`>='$mints'");
				if(mysqli_num_rows($li_query))
				{
					$li_read = mysqli_fetch_assoc($li_query);
					return $li_read[$field];
				}
				else return false;
			}
			
			$failed_login_attempts = get_cp_login_count("failed");
			if($failed_login_attempts >= 10 && !isset($whiteList[$ipaddress]) && !$isLocalIP) 
			{
				update_cp_login_status(false,$_POST['username']);
				$login_message = "Exceeded Max Login Attempts";
				
				if($failed_login_attempts==100 || $failed_login_attempts ==10)
				{
					if($failed_login_attempts==100)
						$msubject = "Attack Detected: DST";
					else
						$msubject = "Exceeded Login Attempts: DST";
						
					mail("corey@meyerwind.com","$msubject - PosLavu Distro","$msubject\nusername: " . $_POST['username'] . "\npassword: " . $_POST['password'] . "\nip address: " . $_SERVER['REMOTE_ADDR'],"From: security@poslavu.com");
				}
			}
			else
			{
				$account_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]' and (`password`=PASSWORD('[2]') or `password`=OLD_PASSWORD('[2]'))",$_POST['username'],$_POST['password']);
				if(mysqli_num_rows($account_query))
				{
					$account_read = mysqli_fetch_assoc($account_query);
					$loggedin = $account_read['username'];
					$loggedin_fullname = trim($account_read['f_name'] . " " . $account_read['l_name']);
					$loggedin_email = $account_read['email'];
					$loggedin_access = $account_read['access'];
					if (strpos($loggedin_access,"admin") !== false){
						$_SESSION['posdistro_loggedin_admin'] = $loggedin;
					}
					$_SESSION['posdistro_loggedin'] = $loggedin;
					$_SESSION['posdistro_fullname'] = $loggedin_fullname;
					$_SESSION['posdistro_email'] = $loggedin_email;
					$_SESSION['posdistro_access'] = $loggedin_access;
					$_SESSION['posdistro_id'] = $account_read['id'];
					
					update_cp_login_status(true,$_POST['username']);
					
					if(isset($_POST['stay_logged_in']))
					{
						$autokey = session_id() . rand(1000,9999);
						setcookie("poslavu_distro_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
						//mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
					}
					
					add_to_distro_history("logged in","Logged In",$account_read['id'],$loggedin);
					
					echo "<script language='javascript'>";
					echo "window.location.replace('index.php'); ";
					echo "</script>";
					exit();
				}
				else
				{
					update_cp_login_status(false,$_POST['username']);
					$login_message = "Invalid Account or Password";
				}
			
				/*$cust_query = mlavu_query("select * from `$maindb`.`customer_accounts` where `username`='[1]'",$_POST['username']);
				if(mysqli_num_rows($cust_query))
				{
					$cust_read = mysqli_fetch_assoc($cust_query);
					$custid = $cust_read['id'];
					$dataname = $cust_read['dataname'];
					$rdb = "poslavu_".$dataname."_db";
					
					$user_query = mlavu_query("select * from `$rdb`.`users` where `access_level`>='3' and `username`='[1]' and (`password`=PASSWORD('[2]') or `password`=OLD_PASSWORD('[2]'))",$_POST['username'],$_POST['password']);
					if(mysqli_num_rows($user_query))
					{
						$rest_query = mlavu_query("select * from `$maindb`.`restaurants` where `data_name`='[1]'",$dataname);
						if(mysqli_num_rows($rest_query))
						{
							$rest_read = mysqli_fetch_assoc($rest_query);
							if($rest_read['disabled']=="1")
								$company_disabled = true;
							else
								$company_disabled = false;
							$companyid = $rest_read['id'];
						}
						else 
						{
							$company_disabled = true;
							$companyid = 0;
						}
						
						if($company_disabled)
						{
							update_cp_login_status(false,$_POST['username']);
							$login_message = "Invalid Account or Password";
						}
						else
						{
							$user_read = mysqli_fetch_assoc($user_query);
							admin_login($dataname,$user_read['id'],$user_read['username'],trim($user_read['f_name']." ".$user_read['l_name']),$user_read['email'], $companyid);
							update_cp_login_status(true,$_POST['username']);
							
							if(isset($_POST['stay_logged_in']))
							{
								$autokey = session_id() . rand(1000,9999);
								setcookie("poslavu_cp_login", $autokey, mktime(0,0,0,date("m")+1,date("d"),date("Y")));
								mlavu_query("update `poslavu_MAIN_db`.`customer_accounts` set `autokey`=AES_ENCRYPT('[1]','autokey') where `id`='[2]'",$autokey,$custid);
							}
							
							echo "<script language='javascript'>";
							echo "window.location.replace('index.php'); ";
							echo "</script>";
							exit();
						}
					}
					else
					{
						update_cp_login_status(false,$_POST['username']);
						$login_message = "Invalid Account or Password";
					}
				}
				else
				{
					update_cp_login_status(false,$_POST['username']);
					$login_message = "Invalid Account or Password";
				}*/
			}
		}
		
		echo "<table cellpadding=16><td style='border: solid 2px #bbbbbb' bgcolor='#f6f6f6'>";
		echo "<form name='login' method='post' action=''>";
		echo "<table>";
		if($login_message!="")
			echo "<tr><td colspan='2' align='center'><b>$login_message</b></td></tr>";
		echo "<tr><td class='form_row_title'>Username</td><td><input type='text' name='username'></td></tr>";
		echo "<tr><td class='form_row_title'>Password</td><td><input type='password' name='password'></td></tr>";
		echo "<tr><td>&nbsp;</td><td><input class='form_submit_button' type='submit' value='Submit'></td></tr>";
		//echo "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>";
		//echo "<tr><td>&nbsp;</td><td><input type='checkbox' name='stay_logged_in'> Stay Logged In</td></tr>";
		echo "</table>";
		echo "</form>";
		echo "</td></table>";
		echo "<script language='javascript'>";
		echo "document.login.username.focus(); ";
		echo "</script>";
	}
?>
