<?php
	session_start();
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	//require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
	require_once("/home/poslavu/public_html/register/authnet_functions.php");
	$maindb = "poslavu_MAIN_db";
	
	if(isset($_GET['logout']) && $_GET['logout']==1)
	{
		add_to_distro_history("logged out","Logged Out",$_SESSION['posdistro_id'],$_SESSION['posdistro_loggedin']);
		$_SESSION['posdistro_loggedin'] = false;
		$_SESSION['posdistro_fullname'] = false;
		$_SESSION['posdistro_email'] = false;
		$_SESSION['posdistro_access'] = false;
		$_SESSION['posdistro_id'] = false;
	}
	
	if (isset($_GET['admin_login']) && strpos($_SESSION['posdistro_access'],"admin") !== false){
		$account_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$_GET['admin_login']);
		if(mysqli_num_rows($account_query))
		{
			$account_read = mysqli_fetch_assoc($account_query);
			$loggedin = $account_read['username'];
			$loggedin_fullname = trim($account_read['f_name'] . " " . $account_read['l_name']);
			$loggedin_email = $account_read['email'];
			$_SESSION['posdistro_loggedin'] = $loggedin;
			$_SESSION['posdistro_fullname'] = $loggedin_fullname;
			$_SESSION['posdistro_email'] = $loggedin_email;
		}
	}
	
	$loggedin = (isset($_SESSION['posdistro_loggedin']))?$_SESSION['posdistro_loggedin']:false;
	if($loggedin=="") $loggedin = false;
	$loggedin_fullname = (isset($_SESSION['posdistro_fullname']))?$_SESSION['posdistro_fullname']:false;
	if($loggedin_fullname=="") $loggedin_fullname = $loggedin;
	$loggedin_email = (isset($_SESSION['posdistro_email']))?$_SESSION['posdistro_email']:false;
	if($loggedin_email=="") $loggedin_email = $loggedin;
	$loggedin_access = (isset($_SESSION['posdistro_access']))?$_SESSION['posdistro_access']:false;
	if($loggedin_access=="") $loggedin_access = "";
	//Admin Login
	
	
	if($_SERVER['SERVER_PORT']!=443) {
		header("Location: https://admin.poslavu.com/distro/");
	}


	function account_loggedin()
	{
		global $loggedin;
		return $loggedin;
	}
	
	function distro_admin_info($var)
	{
		$sessvar_name = 'posdistro_'.$var;
		if(isset($_SESSION[$sessvar_name]))
			return $_SESSION[$sessvar_name];
		else
			return "";
	}
	
	function add_to_distro_history($action,$details,$reseller_id,$reseller_name)
	{
		$vars = array();
		$vars['action'] = $action;
		$vars['details'] = $details;
		$vars['reseller_id'] = $reseller_id;
		$vars['reseller_name'] = $reseller_name;
		$vars['datetime'] = date("Y-m-d H:i:s");

		mlavu_query("insert into `poslavu_MAIN_db`.`reseller_history` (`action`,`details`,`reseller_id`,`reseller_name`,`datetime`) values ('[action]','[details]','[reseller_id]','[reseller_name]','[datetime]')",$vars);
	}
	
	function display_datetime($dt,$type="both")
	{
		$dt_parts = explode(" ",$dt);
		if(count($dt_parts) > 1)
		{
			$d_parts = explode("-",$dt_parts[0]);
			$t_parts = explode(":",$dt_parts[1]);
			if(count($d_parts) > 2 && count($t_parts) > 2)
			{
				$ts = mktime($t_parts[0],$t_parts[1],$t_parts[2],$d_parts[1],$d_parts[2],$d_parts[0]);
				if($type=="date")
					return date("m/d/y",$ts);
				else if($type=="time")
					return date("h:i a",$ts);
				else
					return date("m/d/y h:i a",$ts);
			}
			else return $dt;
		}
		else return $dt;
	}
	
	function get_reseller_billing_info($distro_code)
	{
		$cust_info = array();
		$cust_info['company_name'] = "";
		$cust_info['first_name'] = "";
		$cust_info['last_name'] = "";
		$cust_info['address'] = "";
		$cust_info['city'] = "";
		$cust_info['state'] = "";
		$cust_info['zip'] = "";
		$cust_info['phone'] = "";
		$cust_info['email'] = "";
		$cust_info['distro_id'] = "";
		
		$distro_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$distro_code);
		if(mysqli_num_rows($distro_query))
		{
			$signup_read = mysqli_fetch_assoc($distro_query);
			$cust_info['company_name'] = $signup_read['company'];
			$cust_info['first_name'] = $signup_read['f_name'];
			$cust_info['last_name'] = $signup_read['l_name'];
			$cust_info['address'] = $signup_read['address'];
			$cust_info['city'] = $signup_read['city'];
			$cust_info['state'] = $signup_read['state'];
			$cust_info['zip'] = $signup_read['postal_code'];
			$cust_info['phone'] = $signup_read['phone'];
			$cust_info['email'] = $signup_read['email'];
			$cust_info['distro_id'] = $signup_read['id'];
		}
		
		return $cust_info;
	}
	
	function buy_form($lpercent)
	{
		$loggedin = account_loggedin();
		$lpercent = str_replace("%","",$lpercent);
		
		$license_types = array();
		$license_types[] = array("Silver","895.00","895.00");
		$license_types[] = array("Gold","1495.00","1495.00");
		$license_types[] = array("Platinum","3495.00","3495.00");
		
		$license_select = array();
		$license_select[] = "";
		for($i=0; $i<count($license_types); $i++)
		{
			$lic_discount = ($license_types[$i][2] * 1) * ($lpercent * 1 / 100);
			$lic_amount = round($license_types[$i][2] * 1 - $lic_discount,2);
			//echo $license_types[$i][2] . " - " . $lic_discount . " - " . $lic_amount . "<br>";
			
			$license_types[$i][1] = $lic_amount;
			$license_select[] = $license_types[$i][0] . " - $" . number_format($license_types[$i][1] * 1,2);
		}
		if($loggedin=="zephyrhardware" || $loggedin=="martin")
		{
			$silver_promo_amount = 562.50;
			$license_types[] = array("Silver Promo",$silver_promo_amount,"625.00");
			$license_select[] = "Silver Promo - $" . number_format($silver_promo_amount,2);
		}
		
		$str = "";
		$fields = array();
		$fields[] = array("Credit Card Info","title");
		$fields[] = array("License","select","license_type",true,$license_select);
		$fields[] = array("Card Number","card_number","card_number",true);
		$fields[] = array("Card Expiration","card_expiration","card_expiration",true);
		$fields[] = array("CVV Code","card_code","card_code",true);
		
		$show_form = true;
		$pay_form = new CForm($fields,"index.php?mode=buy_license");
		if(isset($_POST['posted']))
		{
			$show_message = "";
			
			$card_info = array();
			$license_info = $_POST['license_type'];
			$license_info = explode("-",$license_info);
			$license_name = trim($license_info[0]);
			
			$license_cost = 0;
			$license_value = 0;
			for($i=0; $i<count($license_types); $i++)
			{
				if(strtolower($license_name)==strtolower($license_types[$i][0]))
				{
					$license_cost = $license_types[$i][1];
					$license_value = $license_types[$i][2];
				}
			}
			
			if($license_cost > 0 && $license_value > 0)
			{
				$collect_amount = $license_cost;
				$card_info['number'] = $_POST['card_number'];
				$card_info['expiration'] = $_POST['card_expiration'];
				$card_info['code'] = $_POST['card_code'];
				
				$str .= "Purchasing $license_name for $collect_amount dollars with a value of $license_value<br><br>";
				$customer_info = get_reseller_billing_info($loggedin);
				
				if($card_info['number']=="8484" && $card_info['expiration']=="8484" && $card_info['code']=="8484")
				{
					$auth_result = array();
					$auth_result['success'] = true;
					$auth_result['response'] = "Test Mode";
				}
				else
				{
					$auth_result = authorize_card_for_transaction("charge","DS_" . $loggedin,"DS_" . $loggedin,$collect_amount,$card_info,$customer_info);
				}
				
				if($auth_result['success'])
				{
					$show_message = "<font color='#008800'><b>Success!</b> <br>Your Payment of " . $show_collection_amount . " has been processed</font><br><br>";
					
					$vars = array();
					$vars['type'] = $license_name;
					$vars['resellerid'] = $customer_info['distro_id'];
					$vars['resellername'] = $loggedin;
					$vars['restaurantid'] = "";
					$vars['cost'] = $collect_amount;
					$vars['value'] = $license_value;
					$vars['purchased'] = date("Y-m-d H:i:s");
					$vars['applied'] = "";
					mlavu_query("insert into `poslavu_MAIN_db`.`reseller_licenses` (`type`,`resellerid`,`resellername`,`restaurantid`,`cost`,`value`,`purchased`,`applied`) values ('[type]','[resellerid]','[resellername]','[restaurantid]','[cost]','[value]','[purchased]','[applied]')",$vars);
					add_to_distro_history("bought license","type: $license_name\ncollected: $collect_amount\nvalue: $license_value",$vars['resellerid'],$vars['resellername']);
					//mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `collection_taken`='1' where `restaurantid`='[1]'",$p_companyid);
					
					$show_form = false;
				}
				else
				{
					$show_message = "<font color='#880000'><b>* Error:</b> ".$auth_result['response']."</p></font>";
				}							
				if($show_message!="")
				{
					$str .= "<table width='100%' cellpadding=6><td width='100%' align='center'>";
					$str .= $show_message;
					if(!$show_form)
					{
						$str .= "<br><br><input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
					}
					$str .= "</td></table>";
				}
			}
		}
		if($show_form)
			$str .= $pay_form->form_code();
		return $str;
	}
	
	function create_account_area($mode)
	{
		if ($mode=="create_account")
		{
			echo "<a href='index.php'>Back to Main Menu</a><br><br>";
			
			function confirm_restaurant_shard($data_name)
			{
				$str = "";
				$letter = substr($data_name,0,1);
				if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
					$letter = $letter;
				else
					$letter = "OTHER";
				$tablename = "rest_" . $letter;
				
				$mainrest_query = mlavu_query("select * from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
				if(mysqli_num_rows($mainrest_query))
				{
					$str .= $letter . ": " . $data_name . " exists";
				}
				else
				{
					$str .= $letter . ": " . $data_name . " inserting";
					
					$rest_query = mlavu_query("select `data_access`,`id`,`last_activity`,`jkey`,`data_name` from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$data_name);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						
						$success = mlavu_query("insert into `poslavu_MAINREST_db`.`$tablename` (`data_access`,`last_activity`,`jkey`,`data_name`,`companyid`) values ('[data_access]','[last_activity]','[jkey]','[data_name]','[id]')",$rest_read);
						if($success) $str .= " <font color='green'>success</font>";
						else $str .= " <font color='red'>failed</font>";
					}
				}
				return $str;
			}
			
			ini_set("dislay_errors","1");
			if(isset($_POST['company']) && isset($_POST['email']))
			{
				$company = $_POST['company'];
				$email = $_POST['email'];
				$phone = $_POST['phone'];
				$firstname = $_POST['firstname'];
				$lastname = $_POST['lastname'];
				$address = $_POST['address'];
				$city = $_POST['city'];
				$state = $_POST['state'];
				$zip = $_POST['zip'];
				$default_menu = $_POST['default_menu'];
				
				//require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
				$maindb = "poslavu_MAIN_db";
				require_once("/home/poslavu/public_html/register/create_account.php");
				$result = create_new_poslavu_account($company,$email,$phone,$firstname,$lastname,$address,$city,$state,$zip,$default_menu,"");
				$success = $result[0];
				if($success)
				{
					$username = $result[1];
					$password = $result[2];
					$dataname = $result[3];
					
					echo "<br>username: $username";
					echo "<br>password: $password";
					echo "<br>dataname: $dataname";
					
					$credvars['username'] = $username;
					$credvars['dataname'] = $dataname;
					$credvars['password'] = $password;
					$credvars['company'] = $company;
					$credvars['email'] = $email;
					$credvars['phone'] = $phone;
					$credvars['firstname'] = $firstname;
					$credvars['lastname'] = $lastname;
					$credvars['address'] = $address;
					$credvars['city'] = $city;
					$credvars['state'] = $state;
					$credvars['zip'] = $zip;
					$credvars['default_menu'] = $default_menu;
					
					mlavu_query("insert into `poslavu_MAIN_db`.`signups` (`username`,`dataname`,`password`,`company`,`email`,`phone`,`firstname`,`lastname`,`address`,`city`,`state`,`zip`,`status`,`default_menu`) values ('[username]','[dataname]',AES_ENCRYPT('[password]','password'),'[company]','[email]','[phone]','[firstname]','[lastname]','[address]','[city]','[state]','[zip]','manual','[default_menu]')",$credvars);
					confirm_restaurant_shard($dataname);
					
					mlavu_query("update `poslavu_MAIN_db`.`restaurants` set `distro_code`='[1]' where `data_name`='[2]' limit 1",account_loggedin(),$dataname);
					
					$history_str = "";
					foreach($credvars as $key => $val) $history_str .= $key . ": " . $val . "\n";
					add_to_distro_history("created new account",$history_str,distro_admin_info('id'),distro_admin_info('loggedin'));
					
					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]'",$dataname);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						
						$stat_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `dataname`='[1]'",$dataname);
						if(mysqli_num_rows($stat_query))
						{
							$stat_read = mysqli_fetch_assoc($stat_query);
							$status_id = $stat_read['id'];
						}
						else
						{
							mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`) values ('[1]','[2]')",$rest_read['id'],$dataname);
							$status_id = mlavu_insert_id();
						}
						
						$trial_length = 7;
						$trial_start = date("Y-m-d H:i:s");
						$trial_end = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d") + $trial_length,date("Y")));
						mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `trial_start`='[1]', `trial_end`='[2]' where `id`='[3]'",$trial_start,$trial_end,$status_id);
					}
				}
				else echo "Failed to create account";
			}
			else
			{
				//../lib/create_demo_account.php?m=2' method='POST'>
				$creation_fields = array("company","email","phone","firstname","lastname","address","city","state","zip");
				echo "<script language='javascript'>";
				echo "function submit_create_account() { ";
				for($n=0; $n<count($creation_fields); $n++)
				{
					$crfield = $creation_fields[$n];
					echo " if(document.create_account.".$crfield.".value=='') alert('Please fill out the entire form'); else";
				}
				echo " document.create_account.submit(); ";
				echo "} ";
				echo "</script>";
				echo "<form action='index.php?mode=create_account' method='POST' name='create_account'>
					<table cellspacing='0' cellpadding='5'>
						<tr><td align='right'>Starter Menu:</td><td align='left'><select name='default_menu'>
							<option value='default_coffee'>Coffee Shop</option>
							<option value='defaultbar'>Bar/Lounge</option>
							<option value='default_pizza_'>Pizza Shop</option>
							<option value='default_restau'>Restaurant</option>
							<option value='sushi_town'>Original Sushi</option>
						</select></td></tr>
						<tr><td align='right'>Company Name:</td><td align='left'><input type='text' name='company' size='30' /></td></tr>
						<tr><td align='right'>Email:</td><td align='left'><input type='text' name='email' size='30' /></td></tr>
						<tr><td align='right'>Phone:</td><td align='left'><input type='text' name='phone' size='30' /></td></tr>
						<tr><td align='right'>First Name:</td><td align='left'><input type='text' name='firstname' size='30' /></td></tr>
						<tr><td align='right'>Last Name:</td><td align='left'><input type='text' name='lastname' size='30' /></td></tr>
						<tr><td align='right'>Address:</td><td align='left'><input type='text' name='address' size='30' /></td></tr>
						<tr><td align='right'>City:</td><td align='left'><input type='text' name='city' size='30' /></td></tr>
						<tr><td align='right'>State:</td><td align='left'><input type='text' name='state' size='30' /></td></tr>
						<tr><td align='right'>Zip:</td><td align='left'><input type='text' name='zip' size='30' /></td></tr>
						<tr><td align='center' colspan='2'><input type='button' value='Create Account' onclick='submit_create_account()' /></td></tr>
					</table>
				</form>";
			}
		}
		else if ($mode=="account_created")
		{
			if ($_REQUEST['s'] == 1) {
				echo "New account successfully created...<br><br>Username: ".$_REQUEST['new_un']."<br>Password: ".$_REQUEST['new_pw'];
				mail($_SESSION['posadmin_email'], "New POSLavu Account: ".$_REQUEST['new_co'], "New account successfully created...\n\nUsername: ".$_REQUEST['new_un']."\nPassword: ".$_REQUEST['new_pw'], "From: info@poslavu.com");
			} else if ($_REQUEST['s'] == 1) {
				echo "An error occurred while trying to create new account...";
			}
			echo "<br><br><a href='index.php'>Back to Main Menu</a>";		
		}
	}
	
	if($loggedin) // logged in
	{
		require_once(dirname(__FILE__) . "/../cp/areas/billing.php");
		$mode = (isset($_GET['mode']))?$_GET['mode']:"";
		
		echo "<head>";
		echo "<title>POSLavu Distributor Admin</title>";
		echo "<style>";
		echo "  body, table { font-family:Verdana,Arial; font-size:12px; } ";
		echo "  a:link, a:visited { text-decoration:none; color:#000088; } ";
		echo "  a:hover { text-decoration:none; color:#4444aa; } ";
		echo "  .datetime { color:#008800; } ";
		echo "</style>";
		echo "</head>";
		echo "<body>";
		
		if($mode=="create_account" || $mode=="account_created")
		{
			create_account_area($mode);
		}
		else if($mode=="autologin")
		{
			$loginto = (isset($_GET['loginto']))?$_GET['loginto']:"";
			
			$cust_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `distro_code`='[2]'",$loginto,$loggedin);
			if(mysqli_num_rows($cust_query))
			{
				$cust_read = mysqli_fetch_assoc($cust_query);

				add_to_distro_history("autologin",$loginto . "\n" . $cust_read['company_name'],distro_admin_info('id'),distro_admin_info('loggedin'));
				
				$dataname = $cust_read['company_code'];
				$userid = 1001;
				$username = "poslavu_distro_".distro_admin_info("loggedin");
				$full_username = "POSLavu Distro " . distro_admin_info("loggedin");
				$email = "info@poslavu.com";
				$companyid = $cust_read['id'];
				admin_login($dataname, $userid, $username, $full_username, $email, $companyid, "", "", "4", "", "", "0", "0");
				
				echo "<script language='javascript'>";
				echo "window.location.replace('/cp/index.php'); ";
				echo "</script>";
			}
		}
		else
		{
			echo "<table width='100%' cellspacing=0 cellpadding=8 style='border:solid 1px #777777' bgcolor='#eeeeee'><tr><td width='50%' align='left'>";
			echo "Distributor Logged in: <b>" . $loggedin_fullname . "</b>";
			echo "</td><td width='50%' align='right'><a href='index.php?logout=1'>(logout)</a>";
			echo "</td></tr></table>";
			echo "<br>";
			echo "<br>";
			
			$distro_query = mlavu_query("select * from `poslavu_MAIN_db`.`resellers` where `username`='[1]'",$loggedin);
			if(mysqli_num_rows($distro_query))
			{
				$distro_read = mysqli_fetch_assoc($distro_query);
				$distro_credits = $distro_read['credits'];
				$distro_license_com = $distro_read['distro_license_com'];
				if($distro_credits=="") $distro_credits = 0;
				if($distro_license_com=="") $distro_license_com = 0;

				if($mode!="" && $mode!="menu")
				{
					echo "<a href='index.php'><< Back</a><br><br>";
				}
				
				if($mode=="buy_license")
				{
					echo "<b>Buy New License</b>";
					echo "<br><br>";
					echo buy_form($distro_license_com);
				}
				else if($mode=="apply_license")
				{
					$applyto = (isset($_GET['applyto']))?$_GET['applyto']:"";
					$use_license_id = (isset($_GET['use_license_id']))?$_GET['use_license_id']:"";
					
					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `data_name`='[1]' and `distro_code`='[2]'",$applyto,$loggedin);
					if(mysqli_num_rows($rest_query))
					{
						$rest_read = mysqli_fetch_assoc($rest_query);
						
						$license_applied = "";
						$license_resellerid = "";
						$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
						if(mysqli_num_rows($status_query))
						{
							$status_read = mysqli_fetch_assoc($status_query);
							$license_applied = $status_read['license_applied'];
							$license_resellerid = $status_read['license_resellerid'];
							$status_id = $status_read['id'];
						}
						else
						{
							mlavu_query("insert into `poslavu_MAIN_db`.`payment_status` (`restaurantid`,`dataname`) values ('[1]','[2]')",$rest_read['id'],$rest_read['data_name']);
							$status_id = mlavu_insert_id();
						}
						
						if($license_applied=="")
						{
							if($use_license_id!="")
							{
								$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `resellerid`='[1]' and `restaurantid`='' and `id`='[2]'",$distro_read['id'],$use_license_id);
								if(mysqli_num_rows($lic_query))
								{
									$lic_read = mysqli_fetch_assoc($lic_query);
									mlavu_query("update `poslavu_MAIN_db`.`reseller_licenses` set `restaurantid`='[1]', `applied`='[2]' where `id`='[3]'",$rest_read['id'],date("Y-m-d H:i:s"),$use_license_id);
									mlavu_query("update `poslavu_MAIN_db`.`payment_status` set `license_applied`='[1]', `license_type`='[2]', `license_resellerid`='[3]' where `id`='[4]'",$use_license_id,$lic_read['type'],$distro_read['id'],$status_id);
									
									add_to_distro_history("applied license","applyto: $applyto\ncompany_name: ".$rest_read['company_name']."\nlicense_id: $use_license_id\nlicense_type: ".$lic_read['type'],distro_admin_info('id'),distro_admin_info('loggedin'));
									
									echo "License Applied<br><br>";
									echo "<input type='button' value='Continue >>' onclick='window.location = \"index.php\"' />";
								}
							}
							else
							{
								echo "Apply License to: <b>" . $rest_read['company_name'] . "</b>";
								
								$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `resellerid`='[1]' and `restaurantid`=''",$distro_read['id']);
								echo "<input type='hidden' name='set_license_id' id='set_license_id' value=''>";
								echo "<table cellspacing=0 cellpadding=4>";
								while($lic_read = mysqli_fetch_assoc($lic_query))
								{
									echo "<tr>";
									echo "<td><input type='radio' name='use_license' value='".$lic_read['id']."' onclick='document.getElementById(\"set_license_id\").value = this.value'></td>";
									echo "<td align='left'>" . $lic_read['type'] . "</td>";
									echo "<td>".display_datetime($lic_read['purchased'])."</td>";
									echo "</tr>";
								}
								echo "</table>";
								echo "<input type='button' value='Apply >>' onclick='setlic = document.getElementById(\"set_license_id\").value; if(setlic==\"\") alert(\"Please choose the license you would like to apply\"); else if(confirm(\"Are you sure you want to apply this license?  Once applied you cannot remove it.\")) window.location = \"index.php?mode=apply_license&applyto=$applyto&use_license_id=\" + setlic'>";
							}
						}
					}
				}
				else
				{
					if (strpos($loggedin_access,"admin") !== false){
						echo "<p><a href=\"accounts.php\">Manage Accounts</a></p>";
					}
					echo "<table><tr><td valign='top'>";
					
					echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888'>";
					echo "<tr bgcolor='#eeeeee'><td align='center'><b>Companies</b></td><td width='6'>&nbsp;</td><td align='center'>Created</td><td width='6'>&nbsp;</td><td align='center'>Good Until</td><td colspan='6'>&nbsp;</td></tr>";
					$rest_query = mlavu_query("select * from `poslavu_MAIN_db`.`restaurants` where `distro_code`='[1]'",$loggedin);
					while($rest_read = mysqli_fetch_assoc($rest_query))
					{
						$license_applied = "";
						$license_resellerid = "";
						$trial_end = "";
						$actual_trial_end = "";
						$status_query = mlavu_query("select * from `poslavu_MAIN_db`.`payment_status` where `restaurantid`='[1]'",$rest_read['id']);
						if(mysqli_num_rows($status_query))
						{
							$status_read = mysqli_fetch_assoc($status_query);
							$license_applied = $status_read['license_applied'];
							$license_type = $status_read['license_type'];
							$license_resellerid = $status_read['license_resellerid'];
							$trial_end = $status_read['trial_end'];
							$actual_trial_end = $trial_end;
							if($license_applied!="")
								$trial_end = "";
						}
						
						if($trial_end=="")
							$good_until = "n/a";
						else
							$good_until = display_datetime((string)$trial_end,"date");
						
						echo "<tr>";
						echo "<td>" . $rest_read['company_name'] . "</td><td width='6'>&nbsp;</td>";
						echo "<td class='datetime'>" . display_datetime((string)$rest_read['created']) . "</td><td width='6'>&nbsp;</td>";
						echo "<td class='datetime'><b>" . $good_until . "</b></td><td width='6'>&nbsp;</td>";
						echo "<td><a href='index.php?mode=autologin&loginto=".$rest_read['data_name']."' target='_blank'>(login)</a></td><td width='6'>&nbsp;</td>";
						
						echo "<td>";
						if($actual_trial_end=="" && $license_applied=="") // not created by a distro
						{
							echo "<table cellspacing=0 cellpadding=0><tr><td style='cursor:pointer' onclick='if(document.getElementById(\"alt_signup_".$rest_read['id']."\").style.display==\"block\") document.getElementById(\"alt_signup_".$rest_read['id']."\").style.display = \"none\"; else document.getElementById(\"alt_signup_".$rest_read['id']."\").style.display = \"block\"'>Alternate Signup</td><td><div id='alt_signup_".$rest_read['id']."' style='display:none'>&nbsp;<a href='index.php?mode=apply_license&applyto=".$rest_read['data_name']."'>(apply license)</a></div></td></tr></table>";
						}
						else if($license_applied!="")
						{
							echo $license_type;
						}
						else
						{
							echo "<input type='button' value='Apply License' style='font-size:10px' onclick='window.location = \"index.php?mode=apply_license&applyto=".$rest_read['data_name']."\"' />";
						}
						echo "</td>";
						
						echo "</tr>";
					}
					echo "<tr><td colspan='4' align='left'><input type='button' value='Create New Account' onclick='window.location = \"index.php?mode=create_account\"' /></td></tr>";
					echo "</table>";
					
					echo "</td><td width='20'>&nbsp;</td><td valign='top'>";
					
					echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888'>";
					echo "<tr bgcolor='#eeeeee'><td align='center'><b>Licenses not Applied</b></td><td width='6'>&nbsp;</td><td align='center'>Purchased</td></tr>";
					$lic_query = mlavu_query("select * from `poslavu_MAIN_db`.`reseller_licenses` where `resellerid`='[1]' and `restaurantid`=''",$distro_read['id']);
					while($lic_read = mysqli_fetch_assoc($lic_query))
					{
						echo "<tr><td align='right'>" . $lic_read['type'] . "</td><td width='6'>&nbsp;</td><td class='datetime'>".display_datetime($lic_read['purchased'])."</td></tr>";
					}
					echo "<tr><td colspan='3' align='left'><input type='button' value='Buy New License' onclick='window.location = \"index.php?mode=buy_license\"' /></td></tr>";
					echo "</table>";
					
					echo "</td></tr></table>";
					
					echo "<br><br>";
					echo "<table cellspacing=0 cellpadding=4 style='border:solid 1px #888888'>";
					echo "<tr bgcolor='#eeeeee'><td align='left'><b>Resources</b></td><td width='6'>&nbsp;</td><td align='center'>&nbsp;</td></tr>";
					echo "<tr><td colspan='2' align='center'>";
					echo "<table cellpadding=4><tr><td>";
					echo "<li><a href='http://www.poslavu.com/assets/products/Lavu-Localserver-1.1beta-10.7-x86.dmg'>Download Local Server Package</a>";
					echo "</td></tr></table>";
					echo "</td></tr>";
					echo "</table>";
					//echo buy_form();
				}
			}
		}		
		echo "</body>";
	}
	else
	{
		$in_lavu = true;
		require_once("login.php");
	}
?>
